# PHP 5.3 Apache

PHP 5.3 [reached EOL](http://php.net/eol.php) on 14 Aug 2014 and thus, official docker support was [dropped](https://github.com/docker-library/php/pull/20). I still needed to run 5.3 so I built this image based on the latest official builds of PHP.

# What is PHP?

PHP is a server-side scripting language designed for web development, but which can also be used as a general-purpose programming language. PHP can be added to straight HTML or it can be used with a variety of templating engines and web frameworks. PHP code is usually processed by an interpreter, which is either implemented as a native module on the web-server or as a common gateway interface (CGI).

> [wikipedia.org/wiki/PHP](http://en.wikipedia.org/wiki/PHP)

![logo](https://raw.githubusercontent.com/docker-library/docs/master/php/logo.png)

# How to use this image.

## With Command Line

For PHP projects run through the command line interface (CLI), you can do the following.

run the commands to build and run the Docker image:

    ./start.sh

### To ssh into docker container

    docker exec -it {container_id} bash

### Installing modules

To install additional modules use a `Dockerfile` like this:

``` Dockerfile
FROM orsolin/php:5.3-apache

# Installs curl
RUN docker-php-ext-install curl

## Credits

A big credit to [helderco](https://github.com/helderco/docker-php-5.3) for the `fpm` version
of this image.

# License

View [license information](http://php.net/license/) for the software contained in this image.
