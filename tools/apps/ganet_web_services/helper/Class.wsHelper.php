<?php
	/*
	 * Created on Jul 8, 2008
	 */
	
	require_once 'Class.WebServiceIni.php';
	
	class wsHelper{
		
		static function isValidDomain($domain){
			return in_array($domain, WebServiceIni::getValidDomains());
		}
		
	} 
?>
