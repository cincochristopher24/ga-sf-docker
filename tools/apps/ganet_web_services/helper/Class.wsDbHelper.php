<?
	require_once 'Class.WebServiceIni.php';
	
	class wsDbHelper{
		
		static function getUserField($domain){
			
			switch( $domain ){
				case WebServiceIni::FACEBOOK:
					$field = 'facebookID';
					break;
				case WebServiceIni::MYSPACE:
					$field = 'myspaceID';
					break;
				case WebServiceIni::ORKUT:
					$field = 'orkutID';
					break;	
				case WebServiceIni::NING:
					$field = 'ningID';
					break;	
				case WebServiceIni::HI5:
					$field = 'hi5ID';
					break;	
				default:
					return false;	
			}
			return $field;
			
		}
	}
?>	