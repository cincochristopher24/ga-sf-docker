<? //used in the travelBio for opensocial
   //modefied by nash

	require_once 'Class.dbHandler.php';
	
	class MyTravelBioApp{
	
		static function hasMyTravelBioApp($userID){
			$conn = new dbHandler();		
			$conn->setDB('SocialApplication');
			$sql = "SELECT * FROM `tblTravelBioUserOS` WHERE `userID` = {$userID}";  
			$rs = new iRecordset($conn->execQuery($sql));
		//	while(!$rs->EOF()){
		//		return $rs->getTravelBioUserID();
		//	}
		//	return false;
			return $rs->retrieveRecordCount();
		}
		
		static function addUser($userID){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$bioUserID = self::addUserToTravelBioUser();
			if($bioUserID > 0){
				$sql = "INSERT INTO `tblTravelBioUserOS` SET 
						`travelBioUserID` = {$bioUserID},
						`userID` = {$userID},
						`dateAdded` = NOW()";
				$conn->execQuery($sql);
			}
			else {
				$sql = "INSERT INTO `tblTravelBioUserOS` SET 
						`userID` = {$userID},
						`dateAdded` = NOW()";
				$conn->execQuery($sql);
			}
 			return $conn->getLastInsertedID();
		}
		
		static function addUserToTravelBioUser(){
			$bioUserID = 0;
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql_1 = "INSERT INTO `tblTravelBioUser` SET `dateAdded` = NOW(), `numQuestionsDisplayed` = 0";
			$conn->execQuery($sql_1);
			$bioUserID = $conn->getLastInsertedID();
			//echo $bioUserID;
			return $bioUserID;
		}
		
		static function hasTravelBioID($userID){
			$conn = new dbHandler();		
			$conn->setDB('SocialApplication');
			$sql = "SELECT travelBioUserID from `tblTravelBioUserOS` where `userID` = {$userID}";
			$rs = new iRecordset($conn->execQuery($sql));
			while(!$rs->EOF()){
				return $rs->getTravelBioUserID();
			}
			return false;
		}
		
		static function updateTravelBioUserOS($travelerID,$userID){
			$conn = new dbHandler();		
			$conn->setDB('SocialApplication');
			$travelBioUserID = 0;
			$num_q = 0;
			$sql_getTravelID = "SELECT travelBioUserID, numQuestionsDisplayed FRom `tblTravelBioUser` where `travelerID` = {$travelerID} limit 1";
			$rs = new iRecordset($conn->execQuery($sql_getTravelID));
			while(!$rs->EOF()){
				$travelBioUserID = $rs->getTravelBioUserID();
				$num_q =$rs->getNumQuestionsDisplayed();
			$rs->moveNext();
			}
			
			$sql_update = "UPDATE tblTravelBioUserOS set `travelBioUserID` = {$travelBioUserID}, `numQuestionsDisplayed` = {$num_q} where `userID` = {$userID}";
			$conn->execQuery($sql_update);
			
		}
		
		static function addTravelerTravelBio($travelerID = 0){
			if( 0 <$travelerID ){
				$conn = new dbHandler();
				$conn->setDB('SocialApplication');
				$sql = "INSERT INTO tblTravelBioUser SET travelerID={$travelerID}, dateAdded=now(), numQuestionsDisplayed=0";
				$conn->execQuery($sql);
			}
		}
		
		static function createOsUser($params = array()){
			//echo "function called";
			if(is_array($params)){
				
				$userID = $params['userID'];
				$bioID = $params['newBioUserId'];
				$conn = new dbHandler();
				$conn->setDB('SocialApplication');
				$sql = "INSERT INTO `tblTravelBioUserOS` SET 
						`travelBioUserID` = {$bioID},
						`userID` = {$userID},
						`dateAdded` = NOW()";
				$conn->execQuery($sql);
			}
		}
	}
	
?>