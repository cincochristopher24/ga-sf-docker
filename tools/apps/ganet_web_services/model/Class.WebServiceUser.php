<?php
	/*
	 * Created on Jul 8, 2008
	 */
	 
	require_once 'utility/Class.dbHandler.php';
	require_once 'utility/Class.iRecordset.php';
	require_once 'ganet_web_services/helper/Class.wsDbHelper.php'; 
	require_once 'ganet_web_services/helper/Class.wsHelper.php';
//	require_once 'ganet_web_services/model/Class.MyTravelJournalApp.php';
	require_once 'ganet_web_services/model/Class.MyTravelBioApp.php';
	require_once 'Class.WebServiceIni.php';
	require_once 'Class.ToolMan.php';
	 
	class WebServiceUser{
	
		protected $id = 0,
				$travelerID = 0,
				$domain,
				$wsID,
				$name,
				$dotNetUsername;
				
		protected $conn;		
		
		function __construct($id, $domain){
			
			if( wsHelper::isValidDomain($domain) ){
				
				$this->domain = $domain;
				$this->wsID = $id;
				if( 0 < $id ){
					$field = wsDbHelper::getUserField($domain);
					$this->conn = new dbHandler();
					$this->conn->setDB('SocialApplication');
					$sql = "SELECT `username`, `userID`,`tbUser`.`travelerID` FROM tbUser LEFT JOIN Travel_Logs.tblTraveler ON `tbUser`.`travelerID` = `tblTraveler`.`travelerID` WHERE {$field} = {$id} LIMIT 1";
					$rs = new iRecordSet($this->conn->execQuery($sql));
					if( $rs->retrieveRecordCount() ){
						$this->id = $rs->getUserID();
						$this->travelerID = $rs->getTravelerID();
						$this->dotNetUsername = $rs->getUsername();
					}
					else
						$this->setID(0);
				}
				else
					$this->setID(0);
			}
			else{
				return false;
			}
			
		}
		
		function setID($id){
			$this->id = (int)$id;	
		}
		
		function setTravelerID($travelerID){
			$this->travelerID = (int)$travelerID;
		}
		
		function getDomain(){
			return $this->domain;
		}
		
		function getID(){
			return $this->id;
		}
		
		function getWsID(){
			return $this->wsID;
		}
		
		function getTravelerID(){
			return $this->travelerID;
		}
		
		function getDotNetUsername(){
			return $this->dotNetUsername;
		}
		
		function getMyTravelJournals(){
			return MyTravelJournal::getUserJournals($this->id);
		}
		
		function save(){
			$field = wsDbHelper::getUserField($this->domain);
			$sql = 	"INSERT INTO `tbUser` SET " .
					"{$field} = {$this->wsID}, " .
					"`userID` = {$this->id} " .
					"ON DUPLICATE KEY UPDATE " .
					"{$field} = $this->wsID";
			$this->conn->execQuery($sql);
			if( $id = $this->conn->getLastInsertedID() )
				$this->id = $id;	
			return $this->id;		
		}
		
		function addApp($appName){
			
			switch($appName){
				case WebServiceIni::MY_TRAVEL_MAP:
					break;
				case WebServiceIni::MY_TRAVEL_PLANS:
					break;
				case WebServiceIni::MY_TRAVEL_BIO:
					MyTravelBioApp::addUser($this->id);
					break;
				case WebServiceIni::MY_TRAVEL_JOURNALS:
					AbstractMyTravelJournalApp::addUser($this->id);
					break;
			}	
			
		}
		
		function hasApp($appName){
			
			$hasApp = false;
			
			switch($appName){
				case WebServiceIni::MY_TRAVEL_MAP:
					break;
				case WebServiceIni::MY_TRAVEL_PLANS:
					break;
				case WebServiceIni::MY_TRAVEL_BIO:
					$hasApp = MyTravelBioApp::hasMyTravelBioApp($this->id);
					break;
				case WebServiceIni::MY_TRAVEL_JOURNALS:
					$hasApp = AbstractMyTravelJournalApp::hasMyTravelJournalApp($this->id);
					break;
			}
			
			return $hasApp;
			
		}
		
		/**
		 * static functions
		 */
		
		static function addUser($wsID, $domain){
			$user = new WebServiceUser($wsID, $domain);
			$user->save();
			return $user->getID();
		}
		 
		static function isSynchronized($id, $domain){
		
			if( 0 < $id && wsHelper::isValidDomain($domain) ){
				$field = wsDbHelper::getUserField($domain);
				$conn = new dbHandler();
				$conn->setDB('SocialApplication');
				$sql = "SELECT `userID` FROM tbUser WHERE {$field} = {$id} AND `travelerID` <> 0 LIMIT 1 ";
				$rs = new iRecordSet($conn->execQuery($sql));
				if( $rs->retrieveRecordCount() ){
					return $rs->getUserID();
				}
			//	else return false;
			}
			return false;
			
		}
		
		static function synchronize($wsID, $domain, $email, $password){
			
			if( $travelerID = self::isGAUser($email, $password) ){
				$conn = new dbHandler();		
				$conn->setDB('SocialApplication');
				$field = wsDbHelper::getUserField($domain);
				
				$sql = "DELETE FROM `tbUser` WHERE {$field} = {$wsID} AND `travelerID` = 0";
				$conn->execQuery($sql);
				
				if($conn->getAffectedRows()){
					$sql = "UPDATE `tbUser` SET {$field} = {$wsID} WHERE `travelerID` = {$travelerID}";
					$conn->execQuery($sql);	
				}
							
				// get UserID
				$sql = "SELECT `userID` FROM `tbUser` WHERE `travelerID` = {$travelerID}";
				$rs = new iRecordset($conn->execQuery($sql));
				
				return $rs->getUserID();	
			}
			else
				return 0; 
			
		}
		
		
		static function unSynchronize($travelerID, $domain){
			$conn = new dbHandler();		
			$conn->setDB('SocialApplication');
			$field = wsDbHelper::getUserField($domain);
			$sql = "UPDATE `tbUser` set {$field} = 0 WHERE travelerID = {$travelerID}";
			$conn->execQuery($sql);	 
		}
		
		//verify the users account in goabroad.net
		static function isGAUser($email, $password){
			
			$conn = new dbHandler();
			$conn->setDB('Travel_Logs');
			$sql = "SELECT `travelerID` FROM `tblTraveler` WHERE `email` = " . ToolMan::makeSqlSafeString($email) . " AND `password` = " . ToolMan::makeSqlSafeString($password) .' limit 1';
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() ) 
				return $rs->getTravelerID(0);
			return false;	
			                                                                         
		}
		
		static function getWebServiceUserByTravelerID($id){
			
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT `userID` FROM `tbUser` WHERE `travelerID` = {$id}";
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() ) 
				return $rs->getUserID(0);
			return false;	
			
		}
		
		static function getTravelerIDByUserID($userID){
			
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT `travelerID` FROM `tbUser` WHERE `userID` = {$userID}";
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() ) 
				return $rs->getTravelerID();
			return false;
			
		}
		
		
		public function validateGAAccountWithEmail($email, $password) {
			
//			$advisorIds = self::getAdvisorIds();
			$ret = false;
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
		    $sql = "SELECT a.travelerID AS travelerID, a.isSuspended AS isSuspended, b.userID AS userID, c.groupID AS isAdvisor 
		            FROM Travel_Logs.tblTraveler AS a
		            LEFT JOIN SocialApplication.tbUser AS b ON a.travelerID = b.travelerID
					LEFT JOIN Travel_Logs.tblGrouptoAdvisor AS c ON c.travelerID = a.travelerID
		            WHERE (a.email=". ToolMan::makeSqlSafeString($email) ." AND a.password= ". ToolMan::makeSqlSafeString($password) .") ";
		//			AND a.travelerID NOT IN ('".implode("','",$advisorIds)."')";

		    $rs = new iRecordset($conn->execQuery($sql));
			while(!$rs->EOF()){
				$ret = array('travelerID' => $rs->getTravelerID(), 'isSuspended' => $rs->getIsSuspended(), 'userID' => $rs->getUserID(), 'isAdvisor' => $rs->getIsAdvisor() );
			$rs->moveNext();
			}
			return $ret;
		}
		
		static function getAdvisorIds(){
			$ids = array();
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = 'SELECT a.travelerID AS travelerID
			 		FROM Travel_Logs.tblGrouptoAdvisor AS a';
		    $rs = new iRecordset($conn->execQuery($sql));
			while(!$rs->EOF()){
				$ids[] = $rs->getTravelerID();
			$rs->moveNext();
			}
			return $ids;
		}
		
		/* 	function created by nash
			this is used to synchronize an account to other account
			@$newUser is an array of
				--- newTravelerId
				--- travelerID
				--- isSuspended
				--- userID
				of the new account that is to be sync
			@$userInfo is an array of
				--- oldUserID
				--- bioUserID
				--- domain
				of the account (OS)
		*/
		function reSynchronize($newUser = array(), $userInfo = array()){
			
			$conn = new dbHandler();		
			$conn->setDB('SocialApplication');
			$field = wsDbHelper::getUserField($userInfo['domain']);
			
			$osUserID   = $userInfo['osUserID'];
			$oldUserID  = $userInfo['oldUserID'];
		//	$bioUserID  = $userInfo['bioUserID'];
		//	$osBioUserID = $userInfo['osBioUserID'];
			
			$travelerID = $newUser['travelerID'];
		//	$newUserID  = $newUser['userID'];
		//	$newBioUserID = $newUser['newBioUserId'];
			
		//	$sql = "DELETE FROM `tbUser` WHERE {$field} = {$osUserID} AND `travelerID` = 0";
			$sql_1 = "UPDATE `tbUser` SET {$field} = 0 WHERE `userID` = {$oldUserID}";
			$conn->execQuery($sql_1);
			
//			if($conn->getAffectedRows()){
			$sql_2 = "UPDATE `tbUser` SET {$field} = {$osUserID} WHERE `travelerID` = {$travelerID}";
			$conn->execQuery($sql_2);	
			
		}
		
		function firstSynchronize($newUser = array(), $userInfo = array()){
			
			$field = wsDbHelper::getUserField($userInfo['domain']);
			
			$osUserID   = $userInfo['osUserID'];
		//	$oldUserID  = $userInfo['oldUserID'];
		//	$bioUserID  = $userInfo['bioUserID'];
	//		$osBioUserID = $userInfo['osBioUserID'];
			
			$travelerID = $newUser['travelerID'];
		//	$newUserID  = $newUser['userID'];
		//	$newBioUserID = $newUser['newBioUserId'];
			
			$conn = new dbHandler();		
			$conn->setDB('SocialApplication');
			$sql_delete = "DELETE FROM `tbUser` WHERE {$field} = {$osUserID}";
			$conn->execQuery($sql_delete);
			
			$sql_update = "UPDATE `tbUser` SET {$field} = {$osUserID} WHERE `travelerID` = {$travelerID}";
			$conn->execQuery($sql_update);
			
		}
		
		
		
	} 
	
?>
