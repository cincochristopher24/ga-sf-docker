<?php
/*
 * Created on Aug 24, 2006
 * InstantiationException - a subclass of Exception and occurs when an error occurs in instantiating a class
 * Author: Kerwin Gordo	
 */
 
 class InstantiationException extends Exception {
 	 
   public function __toString() {
      return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
   }
   	
 }
 
?>
