<?php
/*
 * Created on Aug 24, 2006
 *
 * SQLException - a subclass of Exception and occurs when an error occurs in a database access
 * Author: Kerwin Gordo	
 */
 
 class SQLException extends Exception {
 	  
   public function __toString() {
       return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
   }
   	
 }
 
?>
