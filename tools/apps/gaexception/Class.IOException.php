<?php
/*
 * Created on Aug 24, 2006
 *
 * IOException:	 a subclass of Exception and occurs when an I/O error occurs 
 * Author: Kerwin Gordo	
 */
 
 class IOException extends Exception {
 	  
   public function __toString() {
       return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
   }
   	
 }
 
?>
