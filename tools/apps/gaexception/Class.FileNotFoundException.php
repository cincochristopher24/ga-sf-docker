<?php
/**
 * Created on Aug 24, 2006
 *
 * FileNotFoundException: a subclass of IOException and occurs when a file is not found
 * Author: Kerwin Gordo	
 */
 
 class FileNotFoundException extends IOException {
 	  
   public function __toString() {
       return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
   }
   	
 } 
?>
