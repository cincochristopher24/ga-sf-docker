<?php
	 /****************************************
	  * Class.ganetTravelRepo.php
	  * 	- repository class for Travel 
	  *  May 5, 2009
	  */
	
	 require_once	'Cache/ganetCacheProvider.php';
	 require_once 	'Class.Connection.php';
	 require_once	'Class.ConnectionProvider.php';
	 require_once 	'Class.Recordset.php';
	 require_once 	'travellog/vo/Class.ganetValueObject.php';
	 
	 
	 class ganetTravelRepo {
	 	protected $cache	=	null;

	 	public function ganetTravelRepo(){
	 		$this->cache	=	ganetCacheProvider::instance()->getCache();
	 	}
	 	
	 	function retrieve(ganetValueObject $vo){
	 		
	 	}
	 	
	 	function getList(ganetValueObject $vo){
	 		/* main contents of $vo CONTEXT,FILTER which are assoc arrays and COMMAND which is a string */
	 		$context 	=	$vo->IsKeyExists('CONTEXT')?$vo->get('CONTEXT'):null;
			$filter 	=	$vo->IsKeyExists('FILTER')?$vo->get('FILTER'):null; 		
	 		$command 	=	$vo->IsKeyExists('COMMAND')?$vo->get('COMMAND'):'';
			
			$travels 	=	array();
			
			switch ($command) {
				case 'TRAVELER':
					$travels	=	$this->getTravelerTravels($context,$filter);
					break;		
				default:
					$travels	=	$this->getTravels($context);
					break;
			}		 		
 			return $travels;	
	 	}
	 	
	 	/*********************
	 	 * action/worker functions
	 	 */
	 	
	 	protected function getTravels($context){
	 		$found	=	false;
	 		$travels	=	array();
	 		if ($this->cache != null){
	 			$travelIDs	=	$context->get('TRAVEL_IDS');
	 			
	 			foreach($travelIDs as $travelID){
	 				$travel = $this->cache->get('Travel_' . $travelID);
	 				if (!$travel){
	 					$travel	=	new Travel($travelID);
	 				}
	 				$travels[]	=	$travel;
	 			}
	 			
	 		}
	 		
	 		return $travels;
	 	}
	 	
	 	protected function getTravelerTravels($context,$filter){
	 		
	 		$travelerID	=	$context->get('TRAVELER_ID');
	 		$isOwner	=	$context->get('IS_OWNER');
	 		$privacycriteria	=	$filter->get('PRIVACY_CRITERIA');
	 		$found = false;
	 		if ($this->cache != null){
	 			$keys	=	$this->cache->get('Traveler_Travels_Relation_' .  $travelerID);
	 			
	 			if ($keys){
	 				if (array_key_exists('Traveler_Travels_' . $privacycriteria . '_' . $isOwner . '_' . $travelerID,$keys)) {
	 					
	 					$travelIDs	=	$keys['Traveler_Travels_' . $privacycriteria . '_' . $isOwner . '_' . $travelerID];

	 					/*$vo	=	new ganetValueObject();
	 					$context	=	new ganetValueObject();
	 					$context->set('TRAVEL_IDS',$travelIDs);
	 					$vo->set('CONTEXT',$context);
	 					$travels	=	$this->getList($vo);*/
	 					$found	=	true;
	 					foreach($travelIDs as $travelID){
	 						if ($travel = $this->cache->get($travelID)){
	 							$travels[]	=	$travel;
	 						} else {
	 							// one of the keys is not found so we hit the db
	 							$found	=	false;
	 							$travels	=	array();
	 							break;
	 						}
	 					}
	 					
	 					
	 				} 
	 			}
	 		}
	 		
	 		if (!$found){
		 		// viewer is not owner of travels, get only pushed journals with published entries
				if (0 == $isOwner){
					$sqlQuery = "SELECT DISTINCT tblTravel.* " .
								"FROM tblTravel, tblTravelLog, tblTravelLink " .
								"WHERE tblTravelLog.travelID = tblTravel.travelID " .
								"AND tblTravelLink.travellinkID = tblTravel.travellinkID " .
								"AND tblTravel.deleted = 0 " .
								"AND tblTravelLink.reftype = 1 " .
								"AND tblTravelLink.`refID` = " . $travelerID;
					if (strlen($privacycriteria))
						$sqlQuery = $sqlQuery . " AND tblTravel.privacypref IN ( " . $privacycriteria . " )";
					$sqlQuery = $sqlQuery . " ORDER BY tblTravel.lastupdated DESC ";
				}
				else{
					$sqlQuery = "SELECT DISTINCT tblTravel.* " .
								"FROM tblTravel, tblTravelLink " .
								"WHERE tblTravelLink.travellinkID  = tblTravel.travellinkID " .
								"AND tblTravel.deleted = 0 " .
								"AND tblTravelLink.reftype = 1 " .
								"AND tblTravelLink.refID = " . $travelerID;
		
					if (strlen($privacycriteria))
						$sqlQuery = $sqlQuery . " AND tblTravel.privacypref IN ( " . $privacycriteria . " )";
					//$sqlQuery = $sqlQuery . " ORDER BY tblTravel.title DESC ";
					// modified by daf - 15Dec2008 ; change order from alphabetical to recently updated
					$sqlQuery = $sqlQuery . " ORDER BY tblTravel.lastupdated DESC ";
				}
				
				$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$rs-> Execute($sqlQuery);
		
				$travels = array();
				$travelIDs = array();
				// Instantiate class Travel for each travelID
				if (0 != $rs->Recordcount()){
					while ($travel = mysql_fetch_array($rs->Resultset())){
						try{
					      	// edited by K. Gordo (removed first param of the constructor)
							$tempTravel = new Travel (0,$travel);
							/*$tempTravel->setTravelID($travel['travelID']);
							$tempTravel->setLocationID($travel['locID']);
							$tempTravel->setTitle($travel['title']); 
							$tempTravel->setDescription($travel['description']); 
							$tempTravel->setRating($travel['rating']); 
							$tempTravel->setVotes($travel['votes']); 
							$tempTravel->setViews($travel['viewed']); 
							$tempTravel->setLastUpdated($travel['lastupdated']);
							$tempTravel->setPrimaryphoto($travel['primaryphoto']);
							$tempTravel->setPrivacyPref($travel['privacypref']); 
							$tempTravel->setPublish($travel['publish']);
							$tempTravel->setIsFeatured($travel['isfeatured']);
							$tempTravel->setEntryCount($travel['entryCount']); 
							$tempTravel->setTravelerID($travel['travelerID']);
							$tempTravel->setTravelLinkID($travel['travellinkID']);
							*/
							$travels[] = $tempTravel;
							$travelIDs[] =	'Travel_' . $travel['travelID'];	
							
						}
						catch (exception $e) {		
							throw $e;
						}
					}
					
					// set Traveler travels to cache
					if ($this->cache != null){
						// find in cache relation key 
						$keys	=	 $this->cache->get('Traveler_Travels_Relation_' . $travelerID);
						
						if (!$keys){
							$keys	=	array(); 
						}
						
						$keys['Traveler_Travels_' . $privacycriteria . '_' . $isOwner . '_' . $travelerID]	= $travelIDs;
						$this->cache->set('Traveler_Travels_Relation_' . $travelerID , $keys ,array('EXPIRE'=>86400) );
					}
				}
	 		}
			return $travels;
	 		
	 	}
	 	
	 	
	 	
	 	
	 } 	
?>