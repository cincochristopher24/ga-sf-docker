<?php
/*
 * Created on 04 6, 09
 * 
 * Author:
 * Purpose: 
 * 
 */
 
 
 require_once 	'Class.Connection.php';
 require_once 	'Class.Recordset.php';
 require_once 	'travellog/model2/dao/Class.ganetAbstractDao.php';
 require_once	'Class.dbHandler.php';
 require_once	'Class.SqlResourceIterator.php';
 require_once 	'Cache/ganetMemcache.php';
 
 
 class ganetAdminGroupDao {
	protected $cache	=	null;
	protected $cacheConnected = true;
	
	function ganetAdminGroupDao(){
	
	 	$this->cache = ganetCacheProvider::instance()->getCache();
		if ($this->cache == null)
			$this->cacheConnected = false;
 	
	}
	
	function retrieve($id){
		
	}
	function create($group){
		
	}
	
	function update($group){
	  $handler = new dbHandler();
      $Aname = $handler->makeSqlSafeString($group->getName());
      $Adescription = $handler->makeSqlSafeString($group->getDescription());
      $AgroupAccess = $handler->makeSqlSafeString($group->getGroupAccess());
      $AgroupID = $handler->makeSqlSafeString($group->getGroupID());
      $AapiFlag = $handler->makeSqlSafeString($group->getApiFlag());

     
      $sql = "UPDATE tblGroup SET name = $Aname, description = $Adescription, groupaccess = $AgroupAccess, apiFlag = $AapiFlag WHERE groupID = $AgroupID  " ;
      
      $rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
      $rs->Execute($sql);
	  if ($this->cacheConnected){
	  		$this->cache->delete('Group_' . $group->getGroupID());
	  		//$this->cache->set('Group_' . $group->getGroupID(),$group,array('EXPIRE'=>86400));																																																				
	  }
	}
	
	function delete($group){
		if ($this->cacheConnected){
	  		$this->cache->delete('Group_' . $group->getGroupID());
		}		
		$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		
		$safeGID = mysql_escape_string($group->getGroupID());
		
		// added by K. Gordo. Delete entries in tblGroupApprovedJournals with this groupID
		$sql = "DELETE FROM tblGroupApprovedJournals where groupID = '$safeGID' ";
		$rs->Execute($sql);	

		$sql = "DELETE FROM tblTraveltoGroup where groupID = '$safeGID' ";
		$rs->Execute($sql);	

		$sql = "DELETE FROM tblSendable WHERE sendableID = " . $group->getSendableID();
		$rs->Execute($sql);

    	$sql = "DELETE FROM tblGroup where groupID = '$safeGID' ";           // delete selected admin group
		$rs->Execute($sql);
	}
	
 }
 
 
?>
