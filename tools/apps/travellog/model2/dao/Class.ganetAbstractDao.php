<?php
/*
 * Created on 04 2, 09
 * 
 * Author:
 * Purpose: 
 * 
 */
 
	abstract class ganetAbstractDao {
		abstract function retrieve($id);
		abstract function create($entity);			// entity is the class to be persisted
		abstract function update($entity);
		abstract function delete($entity);
		function executeCommand(ganetValueObject $param){}	// execute an arbitrary data access command
															// param contains 'COMMAND' as string and 'CONTEXT'	
															// 		as another value object which might contain 
															// 		ids
	}
 	
?>
