<?php

	require_once('Class.ConnectionProvider.php');	
	require_once('Class.Recordset.php');
	
	class ganetGroupDao extends ganetAbstractDao {
		protected static $instance = null;
		protected $cache = null;
			
		public static function instance(){
			if (self::$instance == null) {
				self::$instance = new ganetGroupDao();
			} 
			return self::$instance;
		} 
		
		private function ganetGroupDao(){
			$this->cache = ganetCacheProvider::instance()->getCache();	
		}
		
		function retrieve($id){
			
		}
		function create($entity){
		}
		function update($entity){
			
		}
		function delete($entity){
			
		}
		function executeCommand(ganetValueObject $param){
			if (!$param->isKeyExists('COMMAND') || !$param->isKeyExists('CONTEXT') ){
				throw new Exception("Name not found in ganetGroupDao->executeCommand: param must contain COMMAND and CONTEXT" );
			}
			
			$command	=	 $param->get('COMMAND');
			switch ($command) {
				case 'ADD_MEMBER' :
					$this->addMember($param->get('CONTEXT'));
					break;
				case 'REMOVE_MEMBER' :
					$this->removeMember($param->get('CONTEXT'));
					break;	
				default:
					break;	
			}
			
		}
		
		
		/************************************
		 * command implementation functions
		 */
		protected function addMember($context){
			$rs 	=	ConnectionProvider::instance()->getRecordset();	
			
			if (!$context->isKeyExists('TRAVELER_ID') || !$context->isKeyExists('GROUP_ID') ){
				throw new Exception("Name not found in ganetGroupDao->addMember: context must contain TRAVELER_ID and GROUP_ID" );
			}
			
			$travID = $context->get('TRAVELER_ID');  
			$groupID = $context->get('GROUP_ID');
			
            $now  = date("Y-m-d H:i:s");
                         
            $sql = "INSERT into tblGrouptoTraveler (groupID, travelerID, adate) VALUES ('$groupID', '$travID', '$now')";
            $rs->Execute($sql);

            // invalidate cache for Group members keys
            $this->invalidateCacheKeys($groupID);	           
                     
		}
		
		protected function removeMember($context){
			$rs 	=	ConnectionProvider::instance()->getRecordset();
			
			$travID = $context->get('TRAVELER_ID');  
			$groupID = $context->get('GROUP_ID');
			
			$sql = "DELETE FROM tblGrouptoTraveler WHERE groupID = '$groupID' AND travelerID = '$travID' ";
            $rs->Execute($sql);  
            
             // invalidate cache for Group members keys
            $this->invalidateCacheKeys($groupID);	    
		}
		
		
		private function invalidateCacheKeys($groupID){
			if ($this->cache != null){
          		// get hash keys
				$hashKeys	=	$this->cache->get('Group_to_Traveler_hashes_' . $groupID);
				
				if ($hashKeys){
					//remove this hashKeys from cache
					foreach($hashKeys as $hashKey){
						$this->cache->delete('Group_to_Traveler_' . $groupID . '_' . $hashKey);		
					}
            	
				}
            }
		}
		
		
		
	}

?>