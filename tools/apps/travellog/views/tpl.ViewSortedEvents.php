<?
	require_once('Class.Template.php');
	$dateTpl = new Template;
	$dateTpl->set_path('travellog/views/');
	$dateTpl->setVars(array(
		'date' 		=> $event['date'],
		'timezone'	=> $event['timezone']
	));
?>

<?if(2 == $profile):?> <!-- GROUP HOME -->
	<li>
		<span class="meta date" id="<?php echo $event['containerClass']; ?>">
			<?php echo $dateTpl->fetch('tpl.EventMetaDate.php')?>
		</span>
		<h3><a href="<?=$event['eventLink']?>" ><?=$event['eventTitle']?></a></h3>
		<div class="details">							
			<? if (TRUE == $event['eventControls']) { ?>								
				<? if (0 == $event['eventContext']) {
				   		$urledit = "activity.php?action=edit&aID=" . $event['eventID'].'&context=0';
				   		$urldelete = "activity.php?action=remove&aID=" . $event['eventID'].'&context=0';
				   }
				   else {
				   		$urledit = "event.php?action=edit&eID=" . $event['eventID'].'&context=1';
				   		$urldelete = "event.php?action=remove&eID=" . $event['eventID'].'&context=1';
				   }
				?>								
				<a id="edit" href="/<?= $urledit ?>" >Edit</a> | <a id="delete" href="javascript:void(0);" onclick="CustomPopup.initialize('Delete Event ?','Are you sure you want to delete this event?','/<?= $urldelete ?>','Delete Event','1');CustomPopup.createPopup();" >Delete</a>
			<? } ?>

		</div>
	</li>
<?elseif(1 == $profile):?> <!-- TRAVELER PROFILE -->
	<li>
		<?
			if (0 == $event['eventContext']) {
				$editLink = 'event?action=edit&eID=' . $event['eventID'].'&context=0';
				$delLink = 'event?action=remove&eID=' . $event['eventID'].'&context=0';
			}
		
			if (1 == $event['eventContext']) {
				$editLink = '/event.php?action=edit&eID=' . $event['eventID'];
				$delLink = '/event.php?action=remove&eID=' . $event['eventID'];
			}
		
			// traveler event 
			if (2 == $event['eventContext']) {
				$editLink = '/calendar-event.php?action=edit&eID=' . $event['eventID'];
				$delLink = '/calendar-event.php?action=remove&eID=' . $event['eventID'];
			}
		?>
		<span class="meta date" id="<?php echo $event['containerClass']; ?>">
			<?php echo $dateTpl->fetch('tpl.EventMetaDate.php')?>
		</span>
		<h3><a href="<?= $event['eventLink']?>"><?=$event['eventTitle']?></a></h3>
	
		<? if (isset($event['eventControls']) && (2 == $event['eventContext'] && $privileged)) : ?>
			<div class="details"><a href="<?=$editLink?>">Edit</a> | <a href="javascript:void(0);" onclick="CustomPopup.initialize('Delete Event ?','Are you sure you want to delete this event?','<?=$delLink?>','Delete Event','1');CustomPopup.createPopup();">Delete</a></div>
		<? endif; ?>
		<div class="clear"></div>
	</li>
<?else:?>	<!-- GROUP/TRAVELER EVENTS TAB -->
	<li class="viewevents" rel="<?=$event['eventContext']?>-<?=$event['eventID']?>">
		<div class="meta date convertToLocalTime" id="<?php echo $event['containerClass']  ?>">
			<?php echo $dateTpl->fetch('tpl.EventMetaDate.php')?>
		</div>
	
		<h3><a href="javascript:void(0)"><?= stripslashes($event['eventTitle'])?></a></h3>
	</li>
<?endif;?>