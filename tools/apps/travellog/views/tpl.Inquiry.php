<?php
/**
* <b>Template Inquiry </b>
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
		<title><?= $maintitle ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta name="author" content="Aldwin S. Sabornido" />
</head>
		<body>
				<table cellpadding="0" cellspacing="15" border="1" width="80%">
						<tr>
								<td>
										<table cellpadding="5" cellspacing="0" border="0" width="100%">
												<tr>
														<td>Sent to:</td><td><?= $inquiry->getClientName() ?></td>
												</tr>
												<tr>
														<td>Listing:</td><td><?= $inquiry->getListingName() ?></td>
												</tr>
												<tr>
														<td>Sent On:</td><td><?= $inquiry->getDateCreated() ?></td>
												</tr>
												<tr>
														<td colspan="2" height="10"></td>
												</tr>
												<tr>
														<td colspan="2">
																	Message:<br>
																	<?= $inquiry->getMessage() ?>
														</td>
												</tr>
										</table>			
											
								</td>
						</tr>
				</table>
				<a href="/Inquiry.php">Back</a>
		</body>
</html>