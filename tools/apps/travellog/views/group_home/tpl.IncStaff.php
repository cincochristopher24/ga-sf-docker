<div class="section" id="staff_box" >
	<h2>
		<span>Admin</span>
		<?php if( $is_admin_logged || $show_staff_count ): ?>
			<span class="header_actions">
			<?php if ($is_admin_logged):  ?>
				<a href="/members.php?gID=<?php echo $gID; ?>&mode=4" >Add Admin</a> |
				<a href="/members.php?gID=<?php echo $gID; ?>&mode=2" >Manage Admin</a>
			<?php elseif ($show_staff_count): ?>
				<a href="/members.php?gID=<?php echo $gID; ?>&mode=2" >View All <?php echo $staff_count; ?> Staff</a>
			<?php endif; ?>
			</span>
		<?php endif; ?>
	</h2>
	<div id="member_box_content" style="overflow:auto;" >
		<?php if( 0 < $staff_count): ?>
			<ul id="members_container" class="elements users staff_new">
				<?php foreach($staff as $member): ?>
					<?php $profile = $member->getTravelerProfile();	?>
					<li>
						<a href="/<?php echo $profile->getUserName(); ?>" class="thumb">
							<img class="pic" src="<?php echo $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width="65" height="65" alt="User Profile Pic" title="View Profile" />
						</a>
						<div class="details">
							<a href="/<?php echo $profile->getUserName();?>" title="<?php echo $profile->getUserName();?>" class="username"> <?php echo $profile->getUserName(); ?> </a>
							<?php if ($is_admin_logged || $member->getPrivacyPreference()->canViewNames($logged_user_id)): ?>
								<?php if( 17 < strlen($profile->getFirstName()." ".$profile->getLastName()) ): ?>
									<em><?php echo substr($profile->getFirstName(),0,1); ?>. <?php echo $profile->getLastName(); ?></em>
								<?php else: ?>
									<em><?php echo $profile->getFirstName(); ?> <?php echo $profile->getLastName(); ?></em>
								<?php endif; ?>
							<?php endif; ?>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php if ($is_admin_logged): ?>
				<div class="header_actions"> 
					<a href="/messages.php?act=messageGroupStaff&amp;gID=<?php echo $gID; ?>">
						<?php if( $is_parent_group ) : ?>
							Send Message to Group Staff
						<?php else: ?>
							Send Message to Subgroup Staff
						<?php endif; ?> 
					</a>
				</div>
			<?php endif; ?>
		<?php elseif( $is_admin_logged ): ?>
			<div class="sg_description">
				<h3>Assign others to help you manage your group. </h3>
				<p>
					You may add additional admins who will have the same permissions/control over the entire group as does the group Owner . <br />
				</p>
				<p>
					Or an Admin/Owner can assign Staff control over specific subgroups only. Examples of this would be faculty or staff who are only involved in some programs.
				</p>
				<a class="button_v3 goback_button jLeft" href="/members.php?gID=<?php echo $gID; ?>&mode=4"><strong>Add Admin</strong></a>
			</div>
		<?php endif; ?>
	</div>
</div>