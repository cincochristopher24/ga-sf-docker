<?php
require_once("travellog/views/Class.AbstractView.php");
require_once("travellog/helper/Class.HelperFactory.php");
class EntryDescription extends AbstractView{
	function render(){
		$obj_helper_factory = HelperFactory::instance();
		$obj_helper         = $obj_helper_factory->create(HelperType::$JOURNAL_ENTRY);
		$this->obj_template->set( 'obj_helper', $obj_helper );
		
		return ( $this->contents['obj_entry']->getTravelLogID() )? $this->obj_template->fetch('tpl.EntryDescription.php'): NULL;
	}
}
?>
