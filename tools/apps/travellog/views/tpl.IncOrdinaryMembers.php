<div class="section" id="members_box">
	<h2>
		<span>Members</span>
		
			<? if ($isAdminLogged): /* can add only if the current logged user is the admin*/ ?>
				<span class="header_actions long_links">
				  <a href="/members.php?gID=<?=$grpID?>&mode=3" >Add Member</a> |
				  <a href="/members.php?gID=<?=$grpID?>" ><?= $grpMembersLinkLabel ?></a>
				</span>
			<? else : ?>
				<span class="header_actions">
				  <a href="/members.php?gID=<?=$grpID?>" ><?= $grpMembersLinkLabel ?></a>
				</span>
			<? endif; ?>
		
	</h2>
  <div id="statusPanel1" class="widePanel" style="display: none;">
	  <p class="loading_message">
			<span id="imgLoading">
			  <img alt="Loading" src="/images/loading.gif"/>
			</span>
			<em id="statusCaption">Loading data please wait...</em>
	  </p>
	</div>
		
	<div id="member_box_content">
	  <?= $grpMembersBoxView->render(GroupMembersBoxView::$MEMBERS) ?>
	</div>	
</div>
