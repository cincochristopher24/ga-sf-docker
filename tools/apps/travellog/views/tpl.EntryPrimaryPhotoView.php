<?php
	if($contents['obj_entry'] instanceof Article){
		$context = "article";		
		$contextID = $contents['obj_entry']->getArticleID();
		$primPhotoLink = $contents['obj_entry']->getPrimaryPhoto()->getPhotoLink('jentryheader');
	}else{
		$context = "travellog";
		$contextID = $contents['obj_entry']->getTravelLogID();
		$primPhotoLink = $contents['obj_entry']->getPrimaryPhoto();
	}	
?>
<?if( $contents['obj_entry']->getPrimaryPhotoID() ):?>
	<img id="primaryheader_photo" class="header_photo" src="<?=$primPhotoLink?>" border="0" />
<?elseif( $contents['show_admin_controls'] && $contents['obj_entry']->getCountPhotos() == 0 && $contents['obj_entry']->getTemplateType() == 0):?>
	<p id="primary_photo_upload_interface">
		There are no photos for this <?if($contents['obj_entry'] instanceof Article):?>article.<?else:?>journal entry.<?endif;?> <br />
		<? if( isset($contents['group_id']) && $contents['group_id'] ): ?>
			<? $manage_photo_link = "/collection.php?type=group&ID=".$contents['group_id']."&context=".$context."&genID=".$contextID."&action=3"; ?>
		<? else: ?>
			<? $manage_photo_link = "/collection.php?type=traveler&ID=".$contents['traveler_id']."&context=".$context."&genID=".$contextID."&action=3"; ?>
		<? endif; ?>
		<a id="no-photo-link" href="<? echo $manage_photo_link ?>">Upload Photos!</a>
	</p>
<?endif;?>