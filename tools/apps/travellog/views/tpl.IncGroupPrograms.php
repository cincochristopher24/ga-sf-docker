<?php
/*
 * Created on Oct 20, 2006
 * Author: Czarisse Daphne P. Dolina
 * Tpl.IncGroupPrograms.php - displays group alerts
 */
?>
<div class="section" id="program_itineraries"> 	
	<h2><span>Programs / Itineraries</span></h2>
		<div class="content">
			
				<? if (TRUE == $isAdminLogged) : ?><div class="header_actions"><a href="/program.php?gID=<?=$grpID ?>" class="add">	Add Program / Itinerary</a></div><? endif;  ?>
			
				<ul class="elements">
							<li>
								<?  for($idx=0;$idx<count($grpPrograms);$idx++){
			    						$eachprogram = $grpPrograms[$idx];
			    						$programsubgroups = $eachprogram->getSubgroups();
								?>				
								<h3>							
									<a href="/program.php?page=1@1&amp;action=view&amp;gID=<?= $grpID ?>&amp;pID=<?= $eachprogram->getProgramID() ?>" ><?= $eachprogram->getTitle();	?></a>
								</h3>
								<p class="for_groups">		
								<?   for($idx1=0;$idx1<count($programsubgroups);$idx1++){
	                							$eachprogramsubgroup = $programsubgroups[$idx1];
								?>
									<? if (0 < $idx1) { ?>
												,
									<? } ?>			
										<a href="<?=$eachprogramsubgroup->getFriendlyURL()?>"><?= $eachprogramsubgroup->getName();	?>	</a>																
								<? }  ?> 		
								</p>	
									<div class="actions">
									<? if (TRUE == $isAdminLogged) { ?>
											
										<a href="/program.php?action=edit&amp;pID=<?= $eachprogram->getProgramID() ?>">edit</a> | <a href="/program.php?action=delete&amp;pID=<?= $eachprogram->getProgramID() ?>" onclick="return confirm('Are you sure you want to delete this?')">delete</a>
									
									<? }  ?> 
									</div>											
								<? } ?>
							</li>
							<? if (0 == count($grpPrograms) && $isAdminLogged) { ?>
								<li>There are no programs added to this group.</li>
							<? } ?>	
					</ul>
					<div class="clear"></div>
			
				<? if (isset($grpProgramsViewLink) ) { ?>	
					<div class="foot_actions">	
					<a href="/program.php?action=view&amp;gID=<?= $grpID ?>">View All</a>
					</div>
				<? } ?>	
			
		</div>
		
		<div class="foot"></div>
</div>			
					