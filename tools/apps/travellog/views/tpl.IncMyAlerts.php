<?php
/*
 * Created on 10 6, 2006
 * @author Kerwin Gordo
 */
?>

<? if (0 < count($alerts)) : ?>
<div class="section" id="alerts" >
	<h2><span>Alerts</span></h2>
	<div class="content">
			
			<ul>
			<?foreach($alerts as $alert) : ?>
				<li>
					<div class="meta">
						<span class="destination">
						<? $destinations = $alert->getDestination();				  
						   foreach ($destinations as $dest){
						   	 if ($dest instanceof Group) 
						   	 	echo $dest->getName();					   	 
						   } 
						?>
						</span>
						<span class="date"><?=date('F j, Y',strtotime($alert->getCreated()))?></span>
					</div>
					<p><?= $alert->getText() ?></p>
				</li>
			<?endforeach;?>
			</ul>
	</div>
	<div class="foot"></div>	
</div>	
<? endif;?>

