<span id="gID1" style="display:none"><?= $groupID ?></span>
<span id="pID" style="display:none"><? if(isset($programID)) echo $programID ?></span>

<div class="area" id="main">
	<div class="area" id="area_left">
		<div class="section" id="quick_tasks">
			<h2>Itineraries</h2>
			<div class="content">
				<? if($ShowControls): ?>
					<ul class="actions">
						<li><a href="javascript:void(0)" onclick="manager.displayForm('mode=add&amp;gID=<?= $groupID ?>');" class="button">Create Itinerary</a></li>
						
					</ul>
				<? endif; ?>
				<? if(isset($obj_program_iterator)):
						$obj_program_iterator->rewind(); ?>
						<ul class="itineraries">
						<? while( $obj_program_iterator->valid() ):?>
							<li class="ptitle" id="ptitle<?=$obj_program_iterator->current()->getProgramID()?>">
								<a href="javascript:void(0)" onclick="manager.getDetail('<?=$obj_program_iterator->current()->getProgramID()?>','<?=$groupID?>')"><?=$obj_program_iterator->current()->getTitle()?></a>
								<? $obj_program_iterator->next(); ?>
							</li>
						<? endwhile; ?>
						</ul>
				<? endif; ?>
			
		
			<?
				if(isset($obj_program_paging)):
					if ( $obj_program_paging->getTotalPages() > 1 ):
						echo '<br /><br />';
						$obj_program_paging->getPrevious();
						echo '&nbsp;|&nbsp;';
						$obj_program_paging->getNext();
					endif;
				endif;
			?>
			</div>
		</div>
	</div>
	
	<div class="area" id="area_right">
		<div id="programdetails"></div>
	</div>
	<div class="clear"></div>
</div>