<?php
/**
 * Created on 09 25, 2006
 * @author Kerwin Gordo
 * Purpose: A view class (UI) to view comments. This class renders the comments in html so clients
 * 			will just have to invoke this class to display the comments
 */
 
 require_once("Class.Template.php");
 require_once("Class.Connection.php");
 require_once("Class.Recordset.php");
 require_once('travellog/model/Class.PhotoType.php');
 require_once('travellog/model/Class.GroupFactory.php');
 require_once('travellog/model/Class.Photo.php');
 require_once 'travellog/model/Class.PokeType.php';
 require_once 'travellog/views/Class.PokesView.php';
 require_once('travellog/model/Class.Article.php');
 
 
 
 class CommentsView 
 {
 	
 	
 	protected $comments = array();     		// array of comments 
 	
 	protected $mErrorLink;					// link to go when there is an error
 	protected $mLackingParamsLink;			// link to go when params are lacking or params are manipulated
 	protected $mContext = null;						// commenttype (photo,travellog,profile)
 	protected $mContextID = null;					// id for the current context (ex. photoID,travelerID,etc)
 	protected $mGenID	=0;						// id for the photocontext (travellogid for travellog, travelerid for traveler)
 	protected $mPhotoCat = '';					// photo category used only for comments on photo
 	protected $mTravelerID = 0;					// travelerID of the current traveler logged in
 	protected $mWithPaging = false;			// boolean, says whether this view will have paging or not
 	protected $mRowsPerPage = 20;				// no. of rows to display if mWithPaging is true
 	protected $mPage = 1;
 	protected $mOkToDelete = false;	
 	protected $mCanAddComment = true;			// switch to tell if the add comment textarea will be shown
 	protected $mEnvURL = 'index.php';			// url of the page containing this component. Used for redirection during login 								
 	protected $submitmode = "submit";		// submit this comment to ajax or basic submit/reload					
 	protected $jsName = null;				// name of Javascript function to call if submitmode eq ajax;
 	protected $ownerId = 0;				// travelerId/groupId of profile/photo/journal-entry
 	protected $ownerName = '';
 	protected $mMessage = "";

 	protected $mShowConfirmationMessageOnly=false;

 	function CommentsView() {
 	}
 	
 	
 	/**
 	 * sets the context of the comment
 	 * @param $context comment on either (photo,travellog and profile) values are are found on Class.CommentType.php
 	 * @param $contextID the unique id of the context of the comment. If photo is being commented, then contextID is the photoID 
 	 */
 	function setContext($context,$contextID) {
 		$this->mContext = $context;
 		$this->mContextID = $contextID;
 	}
 	
 	function setPhotoCategory($cat){
 		$this->mPhotoCat = $cat;	
 	}
 	
 	function setConfirmationMessage($msg){
 		$this->mMessage = $msg;
 	}
 	
 	function setGenID($genID){
 		$this->mGenID = $genID;	
 	}
 	
 	/**
 	 * set the travelerID of the logged user
 	 */
 	function setTravelerID($travelerID) {
 		$this->mTravelerID = $travelerID;
 	}

	/**
 	 * set the ownerID of the owner of the item commented on;
 	 */
 	function setOwnerID($ownerID) {
 		$this->ownerId = $ownerID;
 	}
 	
 	/**
 	 * set Environment URL (URL of the containing page)
 	 */
 	 function setEnvURL($url){
 	 	$this->mEnvURL = $url;
 	 }
 	
 	/**
 	 * set submit mode
 	 */
 	function setSubmitmode($submitmode = "submit"){
 		$this->submitmode = $submitmode;
 	}
 	
 	/**
 	 * Name of Javascript function
 	 */
 	function setJsName($jsName){
 		$this->jsName = $jsName;
 	} 	
 	 	
	function showConfirmationMessageOnly($show=true){
		$this->mShowConfirmationMessageOnly = $show;
	}
 	
 	/**
 	 * readComments - query the comments from the database
 	 */
 	 function readComments() {		 	 	
 	 	require_once('Cache/ganetCacheProvider.php');
 	 	$cache	=	ganetCacheProvider::instance()->getCache();
 	 	$key	=	'CommentsView_Comments_' . $this->mTravelerID . '_' . $this->mContext . '_' . $this->mContextID;

 	 	if ($cache == null || !($this->comments = $cache->get($key))){
 	 	
	 	 	switch ($this->mContext){ 	 		
	 	 		case CommentType::$PHOTO: 	 			
	 	 			try {
	 	 				$photo = new Photo(NULL,$this->mContextID); 	 				
	 	 				$this->comments = $photo->getComments();  	 				
	 	 				$this->checkPhotoOwner($photo);						// sets mOkToDelete
	 	 			}
					catch (Exception $exp) {}
	 	 			break;
	 	 		case CommentType::$TRAVELLOG: 	 			
	 	 			try {
	 	 				$travellog = new TravelLog($this->mContextID);
	 	 				$this->comments = $travellog->getComments();
	 	 				$this->ownerId = $travellog->getTravelerID();
	
	 	 				if ($this->mTravelerID == $this->ownerId){
	  						$this->mOkToDelete = true;
	  						
	  						if (0 == count($this->comments))
	  							$this->mCanAddComment = false;
	  							
	  						$this->setCommentsAsRead();  
	 	 				}
	 	 			}
					catch (Exception $exp){} 	 		
	 	 			break;
	 	 		case CommentType::$PROFILE:
	 	 			try {
	 	 				$traveler = new Traveler($this->mContextID);
	 	 				$this->comments = $traveler->getTravelerProfile()->getComments();
	 	 				$this->ownerId = $traveler->getTravelerID();
	
	 	 				if ($this->mTravelerID == $this->ownerId) {  						
	  						$this->mOkToDelete = true;  						
	  						
	  						if (0 == count($this->comments))
	  							$this->mCanAddComment = false;
	  						
	  						$this->setCommentsAsRead();  					
	 	 				}
	  				
	 	 			}
					catch (Exception $exp) {} 	
					break;
				case CommentType::$ARTICLE:
	 	 			try {
	 	 				$article = new Article($this->mContextID);
	 	 				$this->comments = $article->getComments();
	 	 				$this->ownerId = $article->getAuthorID();
	
	 	 				if ($this->mTravelerID == $this->ownerId){
	  						$this->mOkToDelete = true;
	
	  						if (0 == count($this->comments))
	  							$this->mCanAddComment = false;
	
	  						$this->setCommentsAsRead();  
	 	 				}
	 	 			}
					catch (Exception $exp){} 	 		
	 	 			break;	
	 			case CommentType::$VIDEO:
					try {
						$video = new VideoAlbumToVideo($this->mContextID);

						if( 0 == (int) $video->getParentAlbum()->getAlbumID() ){
							$video = new TravelLogToVideo($this->mContextID);
						}

						$this->comments = $video->getComments();
						$this->checkVideoOwner($video);						// sets mOkToDelete
					}
					catch (Exception $exp){} 	 		
					break;
	 	 	}
	 	 	
	 	 	if ($cache != null){
	 	 		$cache->set($key,$this->comments,array('EXPIRE'=>3600));
	 	 		
	 	 		// store keys for invalidation
	 			$hashesKey =	'CommentsView_hashes_' . $this->mContext . '_' . $this->mContextID;
	 			
	 			$hashKeys  = 	$cache->get($hashesKey);
	 			if ($hashKeys){
	 				if (!in_array($key,$hashKeys)) {
		 				$hashKeys[]	=	$key;
		 				$cache->set($hashesKey,$hashKeys,array('EXPIRE'=>3600));
		 			}
	 			}else{ 	
	 				$cache->set($hashesKey,array($key),array('EXPIRE'=>3600));	 
	 			}	
	 	 	}
	 	 	
 	 	} else {
 	 		//echo 'ReadComments from cache <br/>';
 	 	}
 	 	
 	 }
 	 
 	/**
 	 * Adds the javascript code needed by the shout-outs in the main template.
 	 * Called in fetchShoutOutPopUp() method.
 	 * 
 	 * @return void
 	 */
 	private function retrieveJavascriptDependent(){
    if (is_null($this->mContextID) || is_null($this->mContext)) {
      echo "Context not set!"; exit;	
    } 
    
    $captcha = <<<BOF
      <script type="text/javascript">
        var Captcha = new GaCaptcha({
		      'imageTagID' 	: 'captchaImg',
		      'handler'		: '/gaCaptcha/gaCaptcha.php'
	      });
        
        jQuery(document).ready(function(){
  	      Captcha.load();
        });
      </script>
BOF;
    
    
    $code = <<<BOF
      <script type="text/javascript">
		jQuery.noConflict();
        jQuery(document).ready(function(){
          Comment.resetPopUpVariables();
          Comment.setDivId("comments");
          Comment.setContextId($this->mContextID);
          Comment.setContext($this->mContext);
          Comment.setPhotoCat("photo");
        });
      </script>
BOF;
    $logged = (0 == $this->mTravelerID) ? false : true;

	Template::includeDependentJs("/min/f=js/prototype.js",array("top" => true));
    if (!$logged) {
      Template::includeDependentJs("/min/g=CommentsViewJs2",array("bottom" => true));
      Template::includeDependent($captcha,array("bottom" => true));
    }
    //Template::includeDependentJs("/js/jquery-1.1.4.pack.js", array("bottom" => true));
		Template::includeDependentJs("/min/f=js/comments-1.3.js",array("bottom" => true));
		Template::includeDependent($code,array("bottom" => true));
 	}
 	
 	/**
 	 * Renders the comments of a given traveler.
 	 * 
 	 * @return string
 	 */
 	function renderAjax() {
 		require_once('Cache/ganetCacheProvider.php');
 		$cache = ganetCacheProvider::instance()->getCache();
 		$key 	=	'CommentsView_' . $this->mTravelerID . '_' . $this->mContext . '_' . $this->mContextID . '_' . $this->mGenID . '_' . $this->mCanAddComment ;
 		$content = null;
 		
 		if ($cache != null)
 			$content = $cache->get($key);
 		
 		
 		//echo 'CommentsView_' . $this->mContext . '_' . $this->mContextID;	
 			
 		if ($content){
 			//echo 'Got (CommentsView->renderAjax) comments from cache <br/>';
 			return $content;
 		}
 		
 		$template = new Template();
 		if ($this->mOkToDelete)
 			$template->set("okToDelete",$this->mOkToDelete);
 		$template->set("comments",$this->comments);
 		$template->set("travelerID",$this->mTravelerID);
 		$template->set("context",$this->mContext);
 		$template->set("contextID",$this->mContextID);
 		$template->set("ownerID", $this->ownerId);
 		$template->set("canAddComment",$this->mCanAddComment);
 		$template->set("photoCat",$this->mPhotoCat);
 		$template->set("genID",$this->mGenID);
 		$template->set("envURL",$this->mEnvURL);
 		$template->set("submitmode",$this->submitmode);
 		$template->set("jsName",$this->jsName);
 		$template->set("message", $this->mMessage);
 		$template->set("networkName", "GoAbroad");
 		$template->set("showConfirmationMessageOnly",$this->mShowConfirmationMessageOnly);
		$template->set("directSignupAllowed",TRUE);

 		$content =	preg_replace("/(\s)+/"," ",$template->fetch('travellog/views/tpl.ViewAjaxComments.php')); 		
 		$hashesKey =	'CommentsView_hashes_' . $this->mContext . '_' . $this->mContextID;

		// cache if comments list only
 		if ($cache != null && strlen($this->mMessage) < 0){
 			$cache->set($key,$content,array('EXPIRE'=>3600));
 			
 			// store keys for invalidation
 			$hashesKey =	'CommentsView_hashes_' . $this->mContext . '_' . $this->mContextID;
 			
 			$hashKeys  = 	$cache->get($hashesKey);
 			if ($hashKeys){
 				if (!in_array($key,$hashKeys)) {
	 				$hashKeys[]	=	$key;
	 				$cache->set($hashesKey,$hashKeys,array('EXPIRE'=>3600));
	 			}
 			}else{ 	
 				$cache->set($hashesKey,array($key),array('EXPIRE'=>3600));	 
 			}	
	 			
 		}
 		
 		return $content;
 	}
 	
 	/**
 	 * fetches the shout-out pop-up template
 	 * 
 	 * @return string
 	 */
 	function fetchShoutOutPopUp(){
 		$this->retrieveJavascriptDependent();
 		$logged = (0 == $this->mTravelerID) ? false : true;
 		$tpl = new Template();
 		$tpl->set("isLogged",$logged);
 		return preg_replace("/(\s)+/"," ",$tpl->fetch('travellog/views/tpl.IncShoutOutPopUp.php'));
 	}
 	
 	
 	/**
 	 * renders the ajax comments to the page
 	 */
 	function renderPopupAjax() { 		 		
 		$template = new Template();
 		if ($this->mOkToDelete)
 			$template->set("okToDelete",$this->mOkToDelete);
 		$template->set("comments",$this->comments);
 		$template->set("travelerID",$this->mTravelerID);
 		$template->set("context",$this->mContext);
 		$template->set("contextID",$this->mContextID);
 		$template->set("canAddComment",$this->mCanAddComment);
 		$template->set("photoCat",$this->mPhotoCat);
 		$template->set("genID",$this->mGenID);
 		$template->set("envURL",$this->mEnvURL);
 		$template->set("submitmode",$this->submitmode);  
 		$template->set("jsName",$this->jsName);
		$template->set("showConfirmationMessageOnly",$this->mShowConfirmationMessageOnly);
 		return $template->fetch('travellog/views/tpl.ViewPopupAjaxComments.php'); 		 		 
 	}
 	
 	
 	/**
 	 * check the owner of the photo and determine if the owners id is the same as $this->mTravelerID to create a delete link
 	 * Note: this is only applicable if context is CommentType::$PHOTO
 	 * @param $photo photo object
 	 */
 	private function checkPhotoOwner($photo) {
		
		switch ($photo->getPhotoTypeID()) {
			case PhotoType::$PROFILE:					
				$tp = $photo->getContext();
				$this-> ownerId = $tp->getTravelerID();

				if ($this->mTravelerID == $this->ownerId){
					$this->mOkToDelete = true;
					
					if (0 == count($this->comments))
  						$this->mCanAddComment = false;
  							
					$this->setCommentsAsRead();		            
				}	                                     					
				break;					
			case PhotoType::$TRAVELLOG:					
				$tl = $photo->getContext();
				$ownerTravelerID = 0;
				if (get_class($tl->getOwner()) == 'Traveler'){
					$ownerTravelerID = $tl->getTravelerID();
					$this->ownerId = $ownerTravelerID;
				}
				else if (is_a($tl->getOwner(),'Group')){ 
					$ownerTravelerID = $tl->getOwner()->getAdministrator()->getTravelerID();
					$this->ownerId = $tl->getOwner->getGroupID();
				}
				
				if ($this->mTravelerID == $ownerTravelerID){
					$this->mOkToDelete = true;
					
					if (0 == count($this->comments))
  						$this->mCanAddComment = false;
					
					$this->setCommentsAsRead();		           	
				}
				break;					
			case PhotoType::$FUNGROUP:									
				$group = $photo->getContext();
				$this->ownerId = $group->getAdministrator()->getTravelerID();

				if ($this->mTravelerID == $this->ownerId){
					$this->mOkToDelete = true;
					
					if (0 == count($this->comments))
  						$this->mCanAddComment = false;
					
					$this->setCommentsAsRead();
				}
				break;				
			case PhotoType::$ADMINGROUP:									
				$group = $photo->getContext();
				$this->ownerId = $group->getAdministrator()->getTravelerID();

				if ($this->mTravelerID == $this->ownerId) {
					$this->mOkToDelete = true;
					
					if (0 == count($this->comments))
  						$this->mCanAddComment = false;
					
					$this->setCommentsAsRead();
				}
				break;					
			case PhotoType::$PHOTOALBUM:									
				$pAlbum = $photo->getContext();
				$gf = GroupFactory::instance();
				$group = $gf->create($pAlbum->getGroupID());
				$this->ownerId = $group->getAdministrator()->getTravelerID();

				if ($this->mTravelerID == $this->ownerId) { 
					$this->mOkToDelete = true;
					
					if (0 == count($this->comments))
  						$this->mCanAddComment = false;
					
					$this->setCommentsAsRead();							
				}
				break;				
			case PhotoType::$PROGRAM:									
				$program = $photo->getContext();
				$gf = GroupFactory::instance();
				$group = $gf->create($program->getGroupID());
				$this->ownerId = $group->getAdministrator()->getTravelerID();

				if ($this->mTravelerID == $this->ownerId) {
				 	$this->mOkToDelete = true;
				 	
				 	if (0 == count($this->comments))
  						$this->mCanAddComment = false;
				 	
				 	$this->setCommentsAsRead();
				}
				break;																								
									
		}
 	}

	/**
	 * check the owner of the video and determine if the owners id is the same as $this->mTravelerID to create a delete link
	 * Note: this is only applicable if context is CommentType::$VIDEO
	 * @param $video video object
	 */

	private function checkVideoOwner($video)
	{
		if( $video instanceof VideoAlbumToVideo ){
			$album = $video->getParentAlbum();

			if( 'traveler' == strtolower($album->getCreatorType()) ){
				$ownerTravelerID = $album->getCreatorID();
				$this->ownerId = $ownerTravelerID;
			}
			else{
				if( 'group' == strtolower($album->getCreatorType()) ){
					$groups = GroupFactory::instance()->create(array($album->getCreatorID()));
					$group = $groups[0];
				}
				else if( 'group_template' == strtolower($album->getCreatorType()) ){
					$groupTemplate = GanetGroupTemplatePeer::retrieveByPk($album->getCreatorID());
					$group = GroupFactory::instance()->create($groupTemplate->getParentGroupID());
				}

				$ownerTravelerID = $group->getAdministrator()->getTravelerID();
				$this->ownerId = $ownerTravelerID;
			}
		}
		else if( $video instanceof TravelLogToVideo ){
			$journalEntry = $video->getParentTravelLog();
			$owner = $journalEntry->getOwner();

			if( $owner instanceof Traveler ){
				$ownerTravelerID = $owner->getTravelerID();
			}
			else if( $owner instanceof Group ){
				$ownerTravelerID = $owner->getAdministrator()->getTravelerID();
			}
			
			$this->ownerId = $ownerTravelerID;
		}

		if( $this->mTravelerID == $ownerTravelerID ){
			$this->mOkToDelete = true;

			if( 0 == count($this->comments) ){
				$this->mCanAddComment = false;
			}

			$this->setCommentsAsRead();		           	
		}
	}
 	
 	/**********************************************************************************
 	 * function to mark the comments as read (the comments array set in this class)
 	 * 		note: this function should be called if the logged in user is the owner of
 	 *            the subject of the comment
 	 */
 	private function setCommentsAsRead() {
 		/********* Last Edited By: Cheryl Ivy Q. Go  25 February 2008 *********/
		/*** Ayaw labot para diri ma-update pati date kun na-set to isRead! ***/
 		if (0 < count($this->comments)){ 		 			
 			foreach ($this->comments as $comment){ 				
 				if (!$comment->getIsRead()) { 					
 					$comment->setIsRead(true); 					 
 					$comment->UpdateIsRead();
 				}
 			}
 		}	
 		
 	}
 	/**
 	 * function to determine if the logged traveler is the one looking at comments for his profile,
 	 * 	travellog and photos
 	 */
 	private function myself() {
 		
 	}

	/**
	* invalidates cache
	*/
 	function invalidateCacheEntry(){
		require_once('Cache/ganetCacheProvider.php');
 		$cache = ganetCacheProvider::instance()->getCache();
 		$key 	=	'CommentsView_' . $this->mTravelerID . '_' . $this->mContext . '_' . $this->mContextID . '_' . $this->mGenID . '_' . $this->mCanAddComment ;
		$cache->delete($key);
	}
 }
?>