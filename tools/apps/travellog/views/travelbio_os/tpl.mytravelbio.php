<div class="page_header" id="page_header">
	<?if($isOwner == "true") {?>
		<h3>Your TravelBio</h3>
	<?}
	else {?>
		<h3>You are viewing <?=$ownerName?>'s Travel Bio.<h3>
	<?}?>
</div>

<?php
if($travelBio->getAnswers()){
	$numQuestionsToDisplay = $travelBio->getNumOfQuestionsDisplayed();
	$numQ = count($profileQuestions);
	//echo "<br/>q2display=".$numQuestionsToDisplay;
	//echo "<br/>profileQ=".$numQ;
	if($numQuestionsToDisplay < $numQ){
		$numQ = $numQuestionsToDisplay;

	}
	//echo "<br/>new numQ=".$numQ;
	shuffle($profileQuestions);
	$ctr = 0;
	?>
		<div id="main" class="content">
			<p class="header_text" id="header_text">
	<?		if($viewPage == "canvas" and $isOwner == "true"): ?>
				Here is the list of your travelbio questions and answers. If you want to edit any of your answers please go to <em><a href="javascript:void(0);" onclick="bioTabs('editBio',2)">Update Bio</a></em> page.
	<?		endif; ?>
			</p>
			<ul>
	<? 	foreach($profileQuestions as $iQuestion){
			if(array_key_exists($iQuestion->getQuestionID(),$structAns)){	?>
				<li class="entry"><h4 class="question"><?=$iQuestion->getIntroduction()?></h4>
	<?		if($iQuestion->getQuestionType() == ProfileQuestionOs::$TEXT || $iQuestion->getQuestionType() == ProfileQuestionOs::$TEXTAREA){
				$val = (array_key_exists($iQuestion->getQuestionID(),$structAns)) ? $structAns[$iQuestion->getQuestionID()][0]->getValue() : ''; 
	//			$str .='<img class="answer_icon" src="http://images.goabroad.net/images/facebook/question.jpg"/><p class="answer">'. $val.'</p>'; ?>
				<p class="answer"><?=htmlspecialchars($val)?></p>
	<?		}
			elseif($iQuestion->getQuestionType() == ProfileQuestionOs::$CHECK_TEXTBOX){
				$textVal = '';
				foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){
					if($iChoice->isTypeCheckbox() && 'Others' != $iChoice->getValue()){
	//					$str .='<img class="answer_icon" src="http://images.goabroad.net/images/facebook/question.jpg"/><p class="answer">'. $iChoice->getValue().'</p>'; ?>
						<p class="answer"><?=$iChoice->getValue()?></p>
	<?				}
					elseif($iChoice->isTypeText()){
						$textVal = $iChoice->getValue(); 
					}
				}
				if(strlen($textVal)){
	//				$str .='<img class="answer_icon" src="http://images.goabroad.net/images/facebook/question.jpg"/><p class="answer">'. $textVal.'</p>'; ?>
					<p class="answer"><?=htmlspecialchars($textVal)?></p>
	<?			}
			} ?>
			</li>
	<?		if($numQ == ++$ctr) break;
		}
	} ?>
	</ul>
	<input type="hidden" id="formName" value="main">
	</div>
<?}
else {?>
	<div id="main" class="content">
		<p class="header_text" id="header_text">
			<?=$ownerName?> don't have any answer to the Travel Bio questions yet.
		</p>
	<div>
<?}
?>