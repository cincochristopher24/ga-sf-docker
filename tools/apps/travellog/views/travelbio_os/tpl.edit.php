<div class="page_header">
	<h3>Edit your TravelBio Answers</h3>
</div>

<?$submitForm = "http://www.goabroad.net/opensocial/myTravelBio/edit.php"?>
<div id="edit_page" class="content">	
		<p class="header_text">
			Unanswered questions will not show up on your profile, and by default, ALL of your answers will be shown. 
			To set the amount of space that MyTravelBio will take up on your profile, please go to <a href="javascript:void(0);" onclick="bioTabs('settings',4)" title="Go to settings">Settings</a>.
		</p>	
	
	<form action="<?=$submitForm?>" method="POST" name="editBio">		
		<ul>
			<?php 
				$ctr = 0;
				foreach($profileQuestions as $iQuestion):
			?>	
				<li class="entry">
				 	<label="<?= ++$ctr ?>">
						<input type="hidden" id="qType_<?=$ctr?>" value="<?=$iQuestion->getQuestionType()?>">
						<input type="hidden" id="qId_<?=$ctr?>" value="<?=$iQuestion->getQuestionID()?>">
						<h4 class="question"><?= $iQuestion->getQuestion() ?></h4>
												
						<div class="edit_item">																							
						<?php 
							if($iQuestion->getQuestionType() == ProfileQuestionOs::$TEXT): 
								$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
									? $structAns[$iQuestion->getQuestionID()][0]->getValue()
									:'';
						?>
							<input class="tbox" type="text" id="txt_<?=$iQuestion->getQuestionID()?>" name="txt_<?= $iQuestion->getQuestionID() ?>" value="<?= htmlentities($val) ?>" />
						<?php 
							elseif($iQuestion->getQuestionType() == ProfileQuestionOs::$TEXTAREA): 
								$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
									? $structAns[$iQuestion->getQuestionID()][0]->getValue()
									:'';
						?>
							<textarea class="tarea" cols="30" rows="5" id="txa_<?=$iQuestion->getQuestionID()?>" name="txa_<?= $iQuestion->getQuestionID() ?>"><?= htmlentities($val) ?></textarea>
						<?php elseif($iQuestion->getQuestionType() == ProfileQuestionOs::$CHECK_TEXTBOX): ?>				
							<ul class="checkbox_list">
								<?php
									
									$choices = $iQuestion->getChoices();
									$selections = array();
									$textVal = '';						
									if(array_key_exists($iQuestion->getQuestionID(),$structAns)){
										foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){							
											if($iChoice->isTypeCheckbox()){
												$selections[] = htmlentities($iChoice->getValue()); 
											}
											elseif($iChoice->isTypeText()){
												$textVal = htmlentities($iChoice->getValue()); 
											}
										}
									
			
									}
									// 8004594526914871759
									
									$i=0;
									foreach($choices as $iChoice):
								?>
									<li class="checkbox_list_item">
										<input id="chk_<?=$i ?>" type="checkbox" name="chk_<?= $iQuestion->getQuestionID() ?>[]" style="width:auto" value="<?= htmlentities($iChoice->getChoice()) ?>" <?= in_array($iChoice->getChoice(),$selections)?'checked="true"':'' ?>  />&nbsp; <label for="ans_<?=$i ?>"><?= $iChoice->getChoice() ?> </label>
									</li>
								<?php $i++; endforeach;?>					
								<li class="checkbox_list_item checkbox_list_item_text">
									<input class="short_tbox" type="text" id="txtchk_<?=$iQuestion->getQuestionID()?>" name="txtchk_<?= $iQuestion->getQuestionID() ?>" value="<?= htmlentities($textVal) ?>" />	
								</li>
							</ul>
						<?php endif; ?>
						</div>				
					</label>
				</li>
			<?php endforeach; ?>		
		</ul>
				
		<input type="hidden" id="ctr" name="ctr" value="<?=$ctr?>">
		<input type="hidden" id="i" name="i" value="<?=$i?>">
		
		<div><p style='color:#008800;font-weight:bold;margin: 20px 0 30px 0'><?=$message?></p></div>
		<div>
			<p> Done with your changes? Click the <em>Save Changes</em> button</p>
			<input type="button" class="button" onclick="updateBio('saveBio');" name="save" value="Save Changes" />
	 	</div>
	</form>
	<input type="hidden" id="formName" value="edit">
</div>