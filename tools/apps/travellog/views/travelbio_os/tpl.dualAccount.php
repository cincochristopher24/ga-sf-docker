<?php
$part = explode(".",$container);

$platform_name = "";
	if($part[0] == "myspace"){
		$platform_name = "MySpace";
	} else {
		$platform_name = ucwords($part[0]);
	}
?>
<div class="content">
<p class="first_p">
	We have detected that you already have answers to your Travel Bio on GoAbroad Network.
	Please choose which account has the answers that you wish to keep.<br />
</p><p>
	<strong>Note:</strong> Choosing cannot be undone. If you choose your GoAbroad.net answers, this will delete
	your answers on <?=$platform_name?> and change them to your GoAbroad.net answers. Likewise, if you choose your
	<?=$platform_name?> answers, your GoAbroad.net profile will then change to reflect your choice.<br />					
</p><p>
	If you wish to keep your profiles as they are, just click on <strong>Skip</strong>.
	<input type="hidden" id="travelerID" name="travelerID" value="<?=$travelerID?>" />
</p>
<br />				
<label>
		<input type="button" name="submit" onclick="updateBio('keepDotNet');" value="Keep my GoAbroad.net TravelBio" />
		<input type="button" name="submit" onclick="updateBio('keepOpenSocial');" value="Keep my <?=$platform_name?> TravelBio" />
		<input type="button" name="submit" onclick="bioTabs('settings')" value="Skip" />

</label>
</div>