<?php
	/*
	 * Class.StudentProfileViewsFactory.php
	 * Created on Nov 29, 2007
	 * created by marc
	 */
	
	require_once("Class.Constants.php");
	 
	 class StudentProfileViewsFactory{
	 	static $instance = NULL;
	
		private $views = array();
		
		static function getInstance(){ 
			if( self::$instance == NULL ) self::$instance = new StudentProfileViewsFactory; 
			return self::$instance;
		}
		
		function createView( $view = constants::VIEW_STUDENT_PROFILE_PAGE){
			
			switch($view){
				case constants::VIEW_STUDENT_PREFERENCE:
					if( !array_key_exists("ViewStudentPreference", $this->views) ){
						require_once("travellog/views/Class.ViewStudentPreference.php");
						$this->views["ViewStudentPreference"] = new ViewStudentPreference;
					}
					return $this->views["ViewStudentPreference"];
				
				case constants::VIEW_COUNTRY_PREFERENCE:
					if( !array_key_exists("ViewCountryPreference", $this->views) ){
						require_once("travellog/views/Class.ViewCountryPreference.php");
						$this->views["ViewCountryPreference"] = new ViewCountryPreference;
					}
					return $this->views["ViewCountryPreference"];	
				
				case constants::VIEW_REFERED_LISTINGS:
					if( !array_key_exists("ViewReferedListings", $this->views) ){
						require_once("travellog/views/Class.ViewReferedListings.php");
						$this->views["ViewReferedListings"] = new ViewReferedListings;
					}
					return $this->views["ViewReferedListings"];	
					
				case constants::VIEW_STUDENT_PREFERENCE_FORM:
					if( !array_key_exists("ViewStudentPreferenceForm", $this->views) ){
						require_once("travellog/views/Class.ViewStudentPreferenceForm.php");
						$this->views["ViewStudentPreferenceForm"] = new ViewStudentPreferenceForm;
					}
					return $this->views["ViewStudentPreferenceForm"];		
				
				case constants::VIEW_STUDENT_PROFILE_FORM:
					if( !array_key_exists("ViewStudentProfileForm", $this->views) ){
						require_once("travellog/views/Class.ViewStudentProfileForm.php");
						$this->views["ViewStudentProfileForm"] = new ViewStudentProfileForm;
					}
					return $this->views["ViewStudentProfileForm"];		
				
				case constants::PAGING:
					if( !array_key_exists("Paging", $this->views) ){
						require_once("travellog/views/travelers/Class.PagingListsView.php");
						$this->views["Paging"] = new PagingListsView;
					}
					return $this->views["Paging"];
					break;		
				
				case constants::VIEW_STUDENT_COMMUNICATIONS:
					if( !array_key_exists("ViewStudentCommunications", $this->views) ){
						require_once("travellog/views/Class.ViewStudentCommunications.php");
						$this->views["ViewStudentCommunications"] = new ViewStudentCommunications;
					}
					return $this->views["ViewStudentCommunications"];
					break;	
					
				case constants::VIEW_LISTING:
					if( !array_key_exists("ViewListing", $this->views) ){
						require_once("travellog/views/Class.ViewListing.php");
						$this->views["ViewListing"] = new ViewListing;
					}
					return $this->views["ViewListing"];
					break;		
				
				case constants::VIEW_CLIENT:
					if( !array_key_exists("ViewClient", $this->views) ){
						require_once("travellog/views/Class.ViewClient.php");
						$this->views["ViewClient"] = new ViewClient;
					}
					return $this->views["ViewClient"];
					break;				
				
				default:
					if( !array_key_exists("ViewStudentProfile", $this->views) ){
						require_once("travellog/views/Class.ViewStudentProfile.php");
						$this->views["ViewStudentProfile"] = new ViewStudentProfile;
					}
					return $this->views["ViewStudentProfile"];		
			}
		}
		
	 } 
?>
