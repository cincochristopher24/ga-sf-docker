
<?php


	Template::setMainTemplate($layoutMain);
	
	Template::includeDependentJs("/js/prototype.js"); 
	Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");
	Template::includeDependentJs ( "/js/jquery-1.1.4.pack.js"           );
	Template::includeDependentJs("/js/resourcefile.js"); 
		
	if($group){
		Template::setMainTemplateVar('page_location', $group);
		Template::setMainTemplateVar('group', $group);
	}
	 	
	$code =<<<BOF
	<script type = "text/javascript">
	jQuery.noConflict();
	
	jQuery("#resourcefiles").ready(function(){   
		ResourceFiles = new ResourceFile($genID,"$context",$loginID);
		ResourceFiles.loadResourcefileLists()		
		function page(s,p){ResourceFiles.page(s,p);}
	});
	
	</script>
BOF;

		Template::includeDependent($code);
?>		
	
	<? $subNavigation->show(); ?>
	
	
<div class="area_wrapper" id="content_wrapper">	
	<div class="area" id="top">
		<div class="section" id="resources_section">
			<h1><span><?= $headercaption; ?></span></h1>
			
			<div class="content">
				<!-- Ajax Container -->
				<div id="resourcefiles">
				
				</div> 
				
			</div>
			
			<div class="foot"></div>
		</div>
	</div>
</div>	

<div id="managefilelayer">

</div>


<div id="showedit" style="position:absolute;display: none;">
	<h1> Edit Title</h1>	
	<div id="editcaption" >
		loading...
		<div class="clear"> </div>
	</div>		
</div>

<div id="showpreference" class="resource_popbox" style="display: none;">
	<h1>Set Privacy Preference</h1>	
	<div id="privacypreference">
		loading...
	</div>		
</div>


<script type="text/javascript" charset="utf-8">
	new Draggable('showedit',{});
	new Draggable('showpreference',{});	 	  
</script>
