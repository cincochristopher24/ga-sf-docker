<?php
	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.GroupFactory.php';
	require_once 'travellog/model/Class.MembersPanel.php';
	require_once ('Class.Paging.php');
	require_once ('Class.ObjectIterator.php');
	require_once ('travellog/model/Class.Condition.php');
	require_once ('travellog/model/Class.FilterOp.php');
	require_once ('travellog/model/Class.FilterCriteria2.php');
	require_once ('travellog/views/member_staff/Class.InviteList.php');
	require_once ('travellog/views/member_staff/Class.EmailInviteList.php');
	require_once ('travellog/model/Class.AdminGroup.php');
	require_once ('travellog/model/Class.Traveler.php');

	class PendingMember{

		private $mGroup			= null;
		private $mIsAdminGroup 	= TRUE;
		private $loggedUserId	= 0;
		private $mLoggedUser	= NULL;
		private $mViewerType	= 6;
		
		const FETCH_REG_INVITES		=	1;
		const FETCH_NONREG_INVITES	=	2;
		const CANCEL_REG_INVITE		=	3;
		const CANCEL_NONREG_INVITE	=	4;
		const RESEND_REG_INVITE		=	5;
		const RESEND_NONREG_INVITE	=	6;
		const RESEND_GROUP_NONREG_INVITE = 7;
		
		private $mRegisteredInvites		= array();
		private $mRegisteredInvitesCount= 0;
		private $mNonRegisteredInvites	= array();
		private $mNonRegisteredInvitesCount	= 0;
		private $mFilterCriteria2 		= NULL;
		private $mPage					= 1;
		private $mRpp 					= 10;
		private $mPageClickAction 		= "";
		private $mSubGroupIDs			= array();
		private $mAction				= self::FETCH_REG_INVITES;
		private $mParams				= array();

		function setGroup($_group = ''){
			$this->mGroup = $_group;
			$this->mIsAdminGroup = ( 2 == $this->mGroup->getDiscriminator() )? TRUE : FALSE;
		}
		
		function setLoggedUser($user=NULL){
			$this->mLoggedUser = $user;
		}
		
		function setViewerType($viewerType=6){
			$this->mViewerType = $viewerType;
		}

		function retrieve(){
			$mGroupId = $this->mGroup->getGroupID();

			$mStafftoSG = new MembersPanel;
			$mStafftoSG->setGroup($this->mGroup,$this->mLoggedUser,$this->mViewerType);
			
			$this->initParams();
			
			$rowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			try{
				$ri_array = $mStafftoSG->getRegisteredInvites($rowsLimit);
				$this->mRegisteredInvites = $ri_array[0];
				$this->mRegisteredInvitesCount = $ri_array[1];
			}catch(exception $e){
			}
			try{
				$nri_array = $mStafftoSG->getNonRegisteredInvites($rowsLimit);
				$this->mNonRegisteredInvites = $nri_array[0];
				$this->mNonRegisteredInvitesCount = $nri_array[1];
			}catch(exception $e){
			}
			
			$subgroup_ids = ( $this->mIsAdminGroup )? $mStafftoSG->getSubGroupIDs() : array($this->mGroup->getGroupID());
			
			$cond = new Condition;
			$cond->setAttributeName('groupID');
			$cond->setOperation(FilterOp::$IN);
			$cond->setValue("(".implode(',',$subgroup_ids).")");
			$this->mFilterCriteria2 = new FilterCriteria2();
			$this->mFilterCriteria2->addCondition($cond);
		}
		
		function initParams(){
			$default = array(	"gID"		=>	0,
								"tID"		=>	0,
								"email"		=>	"",
								"action"	=>	self::FETCH_REG_INVITES,
								"page"		=>	1	);
			
			$this->mParams = array_merge($default,$this->extractParameters());
			$this->mPage = $this->mParams["page"];
			$this->mAction = $this->mParams["action"];
		}

		function render(){
			$gID = $this->mGroup->getGroupID();

			$code = <<<BOF
			<script type="text/javascript"> 
			//<![CDATA[
				jQuery(document).ready(function(){
					piManager.setGroupID($gID);
				});
			//]]>
			</script>
BOF;
			Template::includeDependent($code);
			
			if( 0 < $this->mRegisteredInvitesCount ){
				$this->renderRegisteredInvites();
			}
			if( 0 < $this->mNonRegisteredInvitesCount ){
				$this->renderNonRegisteredInvites();
			}
			if( 0 == $this->mRegisteredInvitesCount && 0 == $this->mNonRegisteredInvitesCount ){
				echo '
					<div id = "pending_members" class = "section">
						<h2><span>Pending Member Invitations</span></h2>
						<div class="section_small_details">
							<p class="help_text"> <span>No Pending Member Invitations</span>.</p>
						</div>	
						<div id = "pending_registered_invites" class="content">
						</div>
						<div class="foot"></div>
					</div>
				';
			}
		}
		
		function renderRegisteredInvites(){
			$tpl = new Template();
			$tpl->set("group_object",$this->mGroup);
			$tpl->set("pending",$this);
			$tpl->set("registeredInvitesCount",$this->mRegisteredInvitesCount);
			$tpl->out('travellog/views/member_staff/tpl.ViewRegisteredInvites.php');
		}
		
		function renderRegisteredInvitesList(){
			$this->mPageClickAction = "piManager.fetchRegisteredInvites";
			$paging = new Paging( $this->mRegisteredInvitesCount, $this->mPage, 'grpID=' . $this->mGroup->getGroupID(), $this->mRpp, false );
			$paging->setOnclick($this->mPageClickAction);
			$iterator = new ObjectIterator( $this->mRegisteredInvites, $paging );
			
			$tpl = new Template();
			$tpl->set("group_object",$this->mGroup);
			$tpl->set("isAdminGroup",$this->mIsAdminGroup);
			$tpl->set("registeredInvites",$this->mRegisteredInvites);
			$tpl->set("filter",$this->mFilterCriteria2);
			$tpl->set("paging",$paging);
			$tpl->set("viewerType",$this->mViewerType);
			$tpl->set("loggedUser",$this->mLoggedUser);
			$tpl->out('travellog/views/member_staff/tpl.ViewRegisteredInvitesList.php');
		}
		
		function renderNonRegisteredInvites(){
			$tpl = new Template();
			$tpl->set("group",$this->mGroup);
			$tpl->set("pending",$this);
			$tpl->set("nonRegisteredInvitesCount",$this->mNonRegisteredInvitesCount);
			$tpl->out('travellog/views/member_staff/tpl.ViewNonRegisteredInvites.php');
		}
		
		function renderNonRegisteredInvitesList(){
			$this->mPageClickAction = "piManager.fetchNonRegisteredInvites";
			$paging = new Paging( $this->mNonRegisteredInvitesCount, $this->mPage, 'grpID=' . $this->mGroup->getGroupID(), $this->mRpp, false );
			$paging->setOnclick($this->mPageClickAction);
			$iterator = new ObjectIterator( $this->mNonRegisteredInvites, $paging );
			
			$tpl = new Template();
			$tpl->set("group",$this->mGroup);
			$tpl->set("isAdminGroup",$this->mIsAdminGroup);
			$tpl->set("nonRegisteredInvites",$this->mNonRegisteredInvites);
			$tpl->set("filter",$this->mFilterCriteria2);
			$tpl->set("paging",$paging);
			$tpl->set("viewerType",$this->mViewerType);
			$tpl->set("loggedUser",$this->mLoggedUser);
			$tpl->out('travellog/views/member_staff/tpl.ViewNonRegisteredInvitesList.php');
		}
		
		private function cancelRegisteredInvite(){
			$traveler = new Traveler($this->mParams["tID"]);
			if( ($traveler instanceof Traveler) && ( ($this->mGroup instanceof AdminGroup) || ($this->mGroup instanceof Group) ) ){
				$this->mGroup->removeFromInviteList($traveler->getTravelerID());
				$action = "cancelled";
				$description = "membership invitation";
				$tpl = new Template();
				$tpl->set("traveler_name",$traveler->getFullName());
				$tpl->set("group_name",$this->mGroup->getName());
				$tpl->set("action",$action);
				$tpl->set("description",$description);
				$tpl->out("travellog/views/member_staff/tpl.ViewNotice.php");
			}else{
				$error = "";
				if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
					$error .= "<p>".$this->mParams["gID"]." is not a valid Group ID<p>";
				}
				if( !($traveler instanceof Traveler) ){
					$error .= "<p>".$this->mParams["tID"]." is not a valid Traveler ID<p>";
				}
				$tpl = new Template();
				$tpl->set("message",$error);
				$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
			}
		}
		
		private function cancelNonRegisteredInvite(){
			if( $this->mGroup instanceof AdminGroup || ($this->mGroup instanceof Group) ){
				$this->mGroup->removeFromEmailInviteList($this->mParams["email"]);
				$action = "cancelled";
				$description = "membership invitation";
				$tpl = new Template();
				$tpl->set("traveler_name",$this->mParams["email"]);
				$tpl->set("group_name",$this->mGroup->getName());
				$tpl->set("action",$action);
				$tpl->set("description",$description);
				$tpl->out("travellog/views/member_staff/tpl.ViewNotice.php");
			}else{
				$error = "";
				if( !($group instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
					$error .= "<p>".$this->mParams["gID"]." is not a valid Group ID<p>";
				}
				$tpl = new Template();
				$tpl->set("message",$error);
				$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
			}
		}
		
		private function resendRegisteredInvite(){
			$traveler = new Traveler($this->mParams["tID"]);
			if( ($traveler instanceof Traveler) && ( ($this->mGroup instanceof AdminGroup) || ($this->mGroup instanceof Group) ) ){
				require_once ('travellog/views/member_staff/Class.InviteMember.php');
				$customMessage = InviteMember::finalizeCustomMessageForGAMembers($this->mGroup,InviteMember::getDefaultMessageForGAMembers($this->mGroup),$traveler->getFirstName());
				$this->mGroup->setCustomMessage($customMessage);
				$this->mGroup->addInvites(array($traveler));
				$action = "resent";
				$description = "membership invitation";
				$tpl = new Template();
				$tpl->set("traveler_name",$traveler->getFullName());
				$tpl->set("group_name",$this->mGroup->getName());
				$tpl->set("action",$action);
				$tpl->set("description",$description);
				$tpl->out("travellog/views/member_staff/tpl.ViewNotice.php");
			}else{
				$error = "";
				if( !($group instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
					$error .= "<p>".$this->mParams["gID"]." is not a valid Group ID<p>";
				}
				if( !($traveler instanceof Traveler) ){
					$error .= "<p>".$this->mParams["tID"]." is not a valid Traveler ID<p>";
				}
				$tpl = new Template();
				$tpl->set("message",$error);
				$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
			}
		}
		
		private function resendNonRegisteredInvite(){
			if( $this->mGroup instanceof AdminGroup || $this->mGroup instanceof Group ){
				$email_array[] = trim($this->mParams["email"]);
				require_once ('travellog/views/member_staff/Class.InviteMember.php');	
				if( !$this->mGroup->isInEmailInviteList(trim($this->mParams["email"])) ){
					$this->mGroup->addEmailInvites($email_array);
					$customMessage = InviteMember::finalizeCustomMessageForNonGAMembers($this->mGroup,InviteMember::getDefaultMessageForNonGAMembers($this->mGroup),"",trim($this->mParams["email"]));
					$this->mGroup->setCustomMessage($customMessage);
				}else{
					$email = trim($this->mParams["email"]);
					$customMessage = $this->mGroup->getEmailInvitationMessagePerEmail($email);
					
					$name_array = $this->mGroup->getEmailInviteAttribute($email);
					$recipient = isset($name_array["firstname"]) ? $name_array["firstname"] : "";
					
					$customMessage = InviteMember::finalizeCustomMessageForNonGAMembers($this->mGroup, $customMessage, $recipient, $email);
					$this->mGroup->setCustomMessage($customMessage);
					
					if( '' == $this->mGroup->getCustomMessage() ){
						//$name_array = $this->mGroup->getEmailInviteAttribute($email);
						//$recipient = isset($name_array["firstname"])? $name_array["firstname"] : "";
						$customMessage = InviteMember::finalizeCustomMessageForNonGAMembers($this->mGroup,InviteMember::getDefaultMessageForNonGAMembers($this->mGroup),$recipient,$email);
						$this->mGroup->setCustomMessage($customMessage);
						$this->mGroup->addMessageToEmailInvite($email, $this->mGroup->addEmailInvitationMessage($this->mGroup->getCustomMessage()));
					}
				}
				if ( ! (strpos($_SERVER['SERVER_NAME'], '.local') > 0) ) {
					require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
					RealTimeNotifier::getInstantNotification(RealTimeNotifier::GROUP_EMAIL_INVITATION, 
						array(
							'EMAIL' 	=> trim($this->mParams["email"]),
							'GROUP'		=> $this->mGroup,
							'CUSTOM_MESSAGE' => $this->mGroup->getCustomMessage()
						))->send();						
				}
								
				$action = "resent";
				$description = "membership invitation";
				$tpl = new Template();
				$tpl->set("traveler_name",$this->mParams["email"]);
				$tpl->set("group_name",$this->mGroup->getName());
				$tpl->set("action",$action);
				$tpl->set("description",$description);
				$tpl->out("travellog/views/member_staff/tpl.ViewNotice.php");
			}else{
				$error = "";
				if( !($group instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
					$error .= "<p>".$this->mParams["gID"]." is not a valid Group ID<p>";
				}
				$tpl = new Template();
				$tpl->set("message",$error);
				$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
			}
		}
		
		private function resendGroupNonRegisteredInvite(){
			$error = "";
			if( $this->mGroup instanceof AdminGroup || $this->mGroup instanceof Group ){
				require_once ('travellog/views/member_staff/Class.InviteMember.php');
				
				$page = isset($this->mParams["page"]) ? $this->mParams["page"] : 1;
				$limit = isset($this->mParams["limit"]) ? $this->mParams["limit"] : 200;
				$row = ($page*$limit)-$limit;
				$rowsLimit = new RowsLimit($limit, $row);
				
				$mStafftoSG = new MembersPanel;
				$mStafftoSG->setGroup($this->mGroup,$this->mLoggedUser,$this->mViewerType);
				
				$nri_array = $mStafftoSG->getGroupNonRegisteredInvitesArray($rowsLimit);
				$emails = $nri_array[0]; 
				$count = $nri_array[1];
				$tempEmails = array();
				if ($emails){
					foreach ($emails as $iEmail){
						$email = $iEmail[0];
						$tempEmails[] = $email;
						$this->mGroup->setCustomMessage($this->mGroup->getEmailInvitationMessagePerEmail($email));	
						if( '' == $this->mGroup->getCustomMessage() ){
						    $name_array = $this->mGroup->getEmailInviteAttribute($email);
						    $recipient = $iEmail[1]." ".$iEmail[2];
						    $customMessage = InviteMember::finalizeCustomMessageForNonGAMembers($this->mGroup,InviteMember::getDefaultMessageForNonGAMembers($this->mGroup),$recipient,$email);
						    $this->mGroup->setCustomMessage($customMessage);
						    $this->mGroup->addMessageToEmailInvite($email, $this->mGroup->addEmailInvitationMessage($this->mGroup->getCustomMessage()));
						}
						try{
					    	if ( ! (strpos($_SERVER['SERVER_NAME'], '.local') > 0) ) {
								require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
						    	RealTimeNotifier::getInstantNotification(RealTimeNotifier::GROUP_EMAIL_INVITATION, 
						    		array(
						    			'EMAIL' 	=> $email,
						    			'GROUP'		=> $this->mGroup,
						    			'CUSTOM_MESSAGE' => $this->mGroup->getCustomMessage()
						    		))->send();
					    	}
					    }catch(Exception $e){
					    	$error = $e->getMessage();
					    }
					}
					
					try{
					    $action = "resent";
					    $description = "membership invitation";
					    $tpl = new Template();
					    $tpl->set("traveler_name",implode(", ", $tempEmails));
					    $tpl->set("group_name",$this->mGroup->getName());
					    $tpl->set("action",$action);
					    $tpl->set("description",$description);
					    $tpl->out("travellog/views/member_staff/tpl.ViewNotice.php");
					}catch(Exception $e){
					    $error = $e->getMessage();
					}
				}
				else{
					$error .= "End.";
				}
			}else{
				if( !($group instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
					$error .= "<p>".$this->mParams["gID"]." is not a valid Group ID<p>";
				}
			}
			if ($error){
				$tpl = new Template();
				$tpl->set("message", $error);
				$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
			}
		}
		
		function performAction(){
			switch($this->mAction){
				case self::FETCH_REG_INVITES	:	$this->renderRegisteredInvitesList(); break;
				case self::FETCH_NONREG_INVITES	:	$this->renderNonRegisteredInvitesList(); break;
				case self::CANCEL_REG_INVITE	:	$this->cancelRegisteredInvite(); break;
				case self::CANCEL_NONREG_INVITE	:	$this->cancelNonRegisteredInvite(); break;
				case self::RESEND_REG_INVITE	:	$this->resendRegisteredInvite(); break;
				case self::RESEND_NONREG_INVITE	:	$this->resendNonRegisteredInvite(); break;
				case self::RESEND_GROUP_NONREG_INVITE	:	$this->resendGroupNonRegisteredInvite(); break;
			}
		}
		
		function extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}
		
		private function calcOffset($page=1){
			return ($page-1) * $this->mRpp;
		}
	}