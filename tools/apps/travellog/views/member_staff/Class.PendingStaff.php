<?php
	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.GroupFactory.php';
	require_once 'travellog/model/Class.MembersPanel.php';

	class PendingStaff{

		private $mType			= 0;
		private $mGroup			= null;
		private $mTraveler		= null;
		private $mLoggedUser	= null;
		private $mChosenGroup	= null;
		private $mViewerType	= 0;
		private $loggedUserId	= 0;
		private $recordcount	= 0;
		private $arrGroupList	= array();
		private $arrTraveler 	= array();

		function setGroup($_group = ''){
			$this->mGroup = $_group;
		}
		function setTraveler($_traveler = null){
			$this->mTraveler = $_traveler;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}
		function setViewerType($_viewerType = 0){
			$this->mViewerType = $_viewerType;
		}
		function setChosenGroup($_group = null){
			$this->mChosenGroup = $_group;
		}

		function cancelPending(){
			if (0 < $this->mChosenGroup->getGroupID())
				$this->mChosenGroup->cancelAsPendingStaff($this->mTraveler->getTravelerID());
		}

		function inviteAgain($_travelerId = 0, $_groupId = 0){
			if (0 < $this->mChosenGroup->getGroupID()){
				$this->mChosenGroup->addAsPendingStaff($this->mTraveler);

				// send an email
				$editedMessage = "The administrator of the group " . $this->mGroup->getName() . " has invited you to join and be a staff of their group.";

				/****** EMAIL Notification *******/
					require_once 'travellog/model/Class.StaffEmailNotification.php';

					$mEmail = new StaffEmailNotification;
					$mEmail->setGroup($this->mChosenGroup);
					$mEmail->setTraveler($this->mTraveler);
					$mEmail->setMessage($editedMessage);
					$mEmail->retrieve();
					$mEmail->Send();
				/****** EMAIL Notification *******/
			}
		}

		function preparePendingStaffedGroup(){
			$mySubGroupList = array();

			if (1 == $this->mViewerType || 2 == $this->mViewerType){
				foreach($this->arrTraveler as $eachMember){
					$arrSubGroup = array();

					// get staffs assigned to parent group
					if ($this->mGroup->isPendingStaffToGroup($eachMember->getTravelerID())){
						$tempArr = array();
						$tId = $eachMember->getTravelerID();
						$tempArr['name'] = $this->mGroup->getName();
						$tempArr['groupId'] = $this->mGroup->getGroupID();
						$tempArr['date'] = MembersPanel::getInvitationDate($this->mGroup->getGroupID(), $tId);
						$arrSubGroup[] = $tempArr;
					}
					else{
						// get staffs assigned to sub groups
						foreach($this->arrGroupList as $eachGroup){
							$tempArr = array();
							if ($eachGroup->isPendingStaffToGroup($eachMember->getTravelerID())){
								$gId = $eachGroup->getGroupID();
								$tId = $eachMember->getTravelerID();
								$tempArr['name'] = $eachGroup->getName();
								$tempArr['groupId'] = $eachGroup->getGroupID();
								$tempArr['date'] = MembersPanel::getInvitationDate($gId, $tId);
								$arrSubGroup[] = $tempArr;
							}
						}
					}
					$mySubGroupList[$eachMember->getTravelerID()] = $arrSubGroup;
				}
			}
			else{
				$travelerId = $this->mLoggedUser->getTravelerID();

				foreach($this->arrTraveler as $eachMember){
					$arrSubGroup = array();

					// get staffs assigned to sub groups
					foreach($this->arrGroupList as $eachGroup){
						$tempArr = array();
						if ($eachGroup->isPendingStaffToGroup($eachMember->getTravelerID())){
							$gId = $eachGroup->getGroupID();
							$tId = $eachMember->getTravelerID();
							$tempArr['name'] = $eachGroup->getName();
							$tempArr['groupId'] = $eachGroup->getGroupID();
							$tempArr['date'] = MembersPanel::getInvitationDate($gId, $tId);
							$arrSubGroup[] = $tempArr;
						}
					}
					$mySubGroupList[$eachMember->getTravelerID()] = $arrSubGroup;
				}
			}

			return $mySubGroupList;
		}

		function prepareUnstaffedGroups(){
			$arrDisplayLink = array();

			foreach($this->arrTraveler as $eachMember){
				$travelerId = $eachMember->getTravelerID();
				// if he is already a superstaff or is invited as superstaff don't display link
				if ((!$this->mGroup->isStaff($travelerId)) && (!$this->mGroup->isPendingStaffToGroup($travelerId))){
						$arrUnStaffedGroup = array();
						foreach($this->arrGroupList as $eachGroup){
							if ((!$eachGroup->isStaff($travelerId)) && (!$eachGroup->isPendingStaffToGroup($travelerId)))
								$arrUnStaffedGroup[] = $eachGroup->getGroupID();
						}
						if (1 == $this->mViewerType || 2 == $this->mViewerType){
							
						}
							
						if (0 < count($arrUnStaffedGroup))
							$arrDisplayLink[$travelerId] = true;
						else{
							if (in_array($this->mViewerType, array(1, 2)))
								$arrDisplayLink[$travelerId] = true;
							else
								$arrDisplayLink[$travelerId] = false;
						}
				}
				else
					$arrDisplayLink[$travelerId] = false;
			}
			return $arrDisplayLink;
		}

		function retrieve(){
			$arrTravelers = array();

			if (1 == $this->mViewerType || 2 == $this->mViewerType){
				if ($this->mGroup->isParent()){
					$this->arrGroupList = $this->mGroup->getAllSubGroups();

					$mStafftoSG = new MembersPanel;
					$mStafftoSG->setGroup($this->mGroup);
					$tempTravelers = $mStafftoSG->getTravelersWithPendingInvitations();
					if (0 < count($tempTravelers))
						$arrTravelers = $tempTravelers;
				}
				else{
					$this->arrGroupList = array($this->mGroup);

					$mStafftoSG = new MembersPanel;
					$mStafftoSG->setGroup($this->mGroup);
					$tempTravelers = $mStafftoSG->getTravelersWithPendingInvitations(true);
					if (0 < count($tempTravelers))
						$arrTravelers = $tempTravelers;
				}
			}
			else{
				if ($this->mGroup->isParent()){
					$this->arrGroupList = Traveler::getStaffedGroups($this->mLoggedUser->getTravelerID(), array($this->mGroup->getGroupID()));
					foreach ($this->arrGroupList as $eachGroup){
						$mStafftoSG = new MembersPanel;
						$mStafftoSG->setGroup($eachGroup);
						$tempTravelers = $mStafftoSG->getTravelersWithPendingInvitations(true);
						if (0 < count($tempTravelers))
							$arrTravelers = array_merge($arrTravelers, $tempTravelers);
					}
				}
				else{
					$this->arrGroupList = array($this->mGroup);

					$mStafftoSG = new MembersPanel;
					$mStafftoSG->setGroup($this->mGroup);
					$tempTravelers = $mStafftoSG->getTravelersWithPendingInvitations(true);
					if (0 < count($tempTravelers))
						$arrTravelers = $tempTravelers;
				}				
			}

			if (0 < count($arrTravelers)){
				$arrTravelerId = array();

				foreach($arrTravelers as $traveler){
					$mTravelerId = $traveler->getTravelerID();
					if (!in_array($mTravelerId, $arrTravelerId))
						$this->arrTraveler[] = $traveler;
					$arrTravelerId[] = $traveler->getTravelerID();
				}
			}
		}

		function render(){
			$tpl = new Template();

			$tpl->set("group", 				$this->mGroup);
			$tpl->set("groupId",			$this->mGroup->getGroupID());
			$tpl->set("viewerType",			$this->mViewerType);
			$tpl->set("arrTraveler",		$this->arrTraveler);
			$tpl->set("loggedUserId",		$this->loggedUserId);
			if (0 < count($this->arrTraveler)){
				$tpl->set("pendingStaffList",	$this->preparePendingStaffedGroup());
				$tpl->set("displayLinkList",	$this->prepareUnstaffedGroups());
			}
			$tpl->out('travellog/views/member_staff/tpl.ViewPendingStaff.php');
		}
	}
?>