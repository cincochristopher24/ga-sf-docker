<?php
	/*
	 *	@author CheWyl
	 *	12 November 2008
	 */

	require_once 'Class.Template.php';

	class AssignStaff{

		private $mType				= 0;
		private $mSubGroupId		= 0;
		private $mViewerType		= 0;
		private $mEmails			= '';
		private $mGroup				= null;
		private $mLoggedUser		= null;
		private $mIsLogged			= false;
		private $arrError			= array();
		private $arrSubGroups		= array();

		function setType($_type = 0){
			$this->mType = $_type;
		}
		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setErrors($_arrError = array()){
			$this->arrError = $_arrError;
		}
		function setEmails($_emails = ''){
			$this->mEmails = $_emails;
		}
		function setIsLogged($_isLogged = false){
			$this->mIsLogged = $_isLogged;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}
		function setViewerType($_viewerType = 0){
			$this->mViewerType = $_viewerType;
		}
		function setSubGroupId($_subGroupId = 0){
			$this->mSubGroupId = $_subGroupId;
		}		

		function retrieve(){
			if (1 == $this->mViewerType || 2 == $this->mViewerType){
				if($this->mGroup->isSubGroup()){
					if (0 == $this->mSubGroupId)
						$this->mSubGroupId = $this->mGroup->getGroupID();
					$this->arrSubGroups = array($this->mGroup);
				}
				else{
					// get all sub groups
					$this->arrSubGroups = $this->mGroup->getAllSubGroups();
					if (0 == $this->mSubGroupId && 0 < count($this->arrSubGroups) && 2 == $this->mType && 0 == count($this->arrError))
						$this->mSubGroupId = $this->arrSubGroups[0]->getGroupID();
				}
			}
			else{
				$this->mType = 2;
				if($this->mGroup->isSubGroup())
					$this->arrSubGroups = array($this->mGroup);
				else{
					// get all staffed sub groups of regular staff
					$this->arrSubGroups = Traveler::getStaffedGroups($this->mLoggedUser->getTravelerID(), array($this->mGroup->getGroupID()));
				}
			}
		}

		function render(){
			$tpl = new Template();

			$tpl->set("type",			$this->mType);
			$tpl->set("group",			$this->mGroup);
			$tpl->set("emails",			$this->mEmails);
			$tpl->set("errors",			$this->arrError);
			$tpl->set("viewerType",		$this->mViewerType);
			$tpl->set("groupId",		$this->mGroup->getGroupID());
			$tpl->set("subGroupId",		$this->mSubGroupId);
			$tpl->set("subGroups",		$this->arrSubGroups);
			$tpl->out("travellog/views/member_staff/tpl.ViewAssignStaff.php");
		}
	}
?>