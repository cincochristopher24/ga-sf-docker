<?php
	/*
	 *	@author CheWyl
	 *	12 November 2008
	 */

	require_once 'Class.Template.php';
	require_once 'travellog/views/member_staff/Class.ViewAllStaff.php';
	require_once 'travellog/views/member_staff/Class.ViewSuperStaff.php';
	require_once 'travellog/views/member_staff/Class.ViewRegularStaff.php';
	require_once 'travellog/views/member_staff/Class.ViewAllRegularStaff.php';

	class ViewStaff{

		private $mPage					= 1;
		private $mRowsPerPage			= 10;
		private $mSubGroupId			= 0;
		private $mViewerType			= 0;
		private $mIsLogged				= false;
		private $mLoggedUser			= null;
		private $mGroup					= null;
		private $mAllStaff				= null;
		private $mRegStaff				= null;
		private $mSuperStaff			= null;
		private $mAllRegularStaff		= null;

		function setPage($_page = 1){
			$this->mPage = $_page;
		}
		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setSubGroupId($_subGroupId = 0){
			$this->mSubGroupId = $_subGroupId;
		}
		function setIsLogged($_isLogged = false){
			$this->mIsLogged = $_isLogged;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}
		function setViewerType($_viewerType = 0){
			$this->mViewerType = $_viewerType;
		}
		function setRowsPerPage($_rows = 10){
			$this->mRowsPerPage = $_rows;
		}

		function getSubGroup(){
			return $this->arrSubGroups[0];
		}

		function retrieve(){
			// if admin or super staff
			if (1 == $this->mViewerType || 2 == $this->mViewerType){
				if ($this->mGroup->isParent()){
					
					//if( 1 == $this->mViewerType ){
						// get all super staff
						$this->mSuperStaff = new ViewSuperStaff;
						$this->mSuperStaff->setPage($this->mPage);
						$this->mSuperStaff->setGroup($this->mGroup);
						$this->mSuperStaff->setLoggedUser($this->mLoggedUser);
						$this->mSuperStaff->retrieve();
					//}
					// retrieve regular staff
					$this->mRegStaff = new ViewRegularStaff;
					$this->mRegStaff->setPage($this->mPage);
					$this->mRegStaff->setGroup($this->mGroup);
					if (0 < $this->mSubGroupId){
						$mGroup = GroupFactory::instance()->create(array($this->mSubGroupId));
						$this->mRegStaff->setSubGroup($mGroup[0]);
					}
					$this->mRegStaff->setLoggedUser($this->mLoggedUser);
					$this->mRegStaff->retrieve();
				}
				else{
					// retrieve regular staff
					$this->mAllRegularStaff = new ViewAllRegularStaff;
					$this->mAllRegularStaff->setPage($this->mPage);
					$this->mAllRegularStaff->setGroup($this->mGroup);
					$this->mAllRegularStaff->setIsLogged($this->mIsLogged);
					$this->mAllRegularStaff->setLoggedUser($this->mLoggedUser);
					$this->mAllRegularStaff->setViewerType($this->mViewerType);
					$this->mAllRegularStaff->retrieve();
				}
			}
			else{
				if ($this->mGroup->isParent()){
					// retrieve regular staff
					$this->mAllStaff = new ViewAllStaff;
					$this->mAllStaff->setPage($this->mPage);
					$this->mAllStaff->setGroup($this->mGroup);
					$this->mAllStaff->setIsLogged($this->mIsLogged);
					$this->mAllStaff->setLoggedUser($this->mLoggedUser);
					$this->mAllStaff->setViewerType($this->mViewerType);
					if (0 < $this->mSubGroupId){
						$mGroup = GroupFactory::instance()->create(array($this->mSubGroupId));
						$this->mAllStaff->setChosenGroup($mGroup[0]);
					}
					$this->mAllStaff->retrieveAll();
				}
				else{
					// retrieve regular staff
					$this->mAllRegularStaff = new ViewAllRegularStaff;
					$this->mAllRegularStaff->setPage($this->mPage);
					$this->mAllRegularStaff->setGroup($this->mGroup);
					$this->mAllRegularStaff->setIsLogged($this->mIsLogged);
					$this->mAllRegularStaff->setLoggedUser($this->mLoggedUser);
					$this->mAllRegularStaff->setViewerType($this->mViewerType);
					$this->mAllRegularStaff->retrieve();
				}
			}
		}

		function render(){
			$tpl = new Template();

			$tpl->set("groupId",		$this->mGroup->getGroupID());
			$tpl->set("groupName",		$this->mGroup->getName());
			$tpl->set("viewerType",		$this->mViewerType);
			if (1 == $this->mViewerType || 2 == $this->mViewerType){
				if ($this->mGroup->isParent()){
					$tpl->set("superStaff",		$this->mSuperStaff);
					$tpl->set("regularStaff",	$this->mRegStaff);
				}
				else
					$tpl->set("allRegStaff",	$this->mAllRegularStaff);
			}
			else{
				if ($this->mGroup->isParent())
					$tpl->set("allStaff",	$this->mAllStaff);
				else
					$tpl->set("allRegStaff",	$this->mAllRegularStaff);
			}
			$tpl->out("travellog/views/member_staff/tpl.ViewStaff.php");
		}
	}
?>