<?php

	require_once ('Class.Template.php');
	require_once ('travellog/model/Class.Condition.php');
	require_once ('travellog/model/Class.FilterOp.php');
	require_once ('travellog/model/Class.FilterCriteria2.php');
	require_once ('travellog/model/Class.AdminGroup.php');
	require_once ('travellog/model/Class.Traveler.php');
	require_once ('travellog/views/member_staff/Class.EmailInviteList.php');
	require_once ('travellog/views/member_staff/Class.InviteList.php');
	require_once 'travellog/model/Class.EmailNotification.php';
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.Crypt.php');
	require_once('Class.ExcelCSVUtil.php');
	require_once('travellog/model/Class.PathManager.php');
	
	class InviteMember{
		
		//getting subgroups invoking getSubGroupsAndProjects in AdminGroup
		const DISPLAY_ALL			=	0;
		const DISPLAY_GROUP			=	1;
		const DISPLAY_PROJECT		=	2;
		
		//invitemembers action
		const DISPLAYFORM			=	0;
		const EVALUATE				=	1;
		const SENDINVITATION		=	2;
		const INVITE_DIFFERENT		=	3;
		
		private $mParams 			= array();
		private $mGroupObject 		= NULL;
		private $mIsAdminGroup 		= TRUE;
		private $mGroupsAndProjects = array();
		private $mSubGroupIDs 		= array();
		
		//for errors
		const NO_ERROR				=	0;
		const EMAIL_MISSING			=	1;
		const EMAIL_INVALID			=	2;
		const EMAIL_POST_INVALID	=	3;
		const EMAIL_IN_FILE_INVALID	=	4;
		const NO_VALID_EMAIL		=	5;
		
		private $mErrors			=	array();//list of email errors
		private $mEmailError		=	self::NO_ERROR;
		private $mAllEmailErrors	=	array();
		
		private $mLoggedUser		=	NULL;
		private $mViewerType		=	1;
		
		private $mExcelCSVUtil		=	NULL;
		
		function __construct(){
			$default = array("action" 		=>	self::DISPLAYFORM,
			 				"inviteID"		=>	array(),
							"invite_email"	=>	array(),
							"emails"		=>	"",
							"message"		=>	"",
							"message2"		=>	"",
							"assignToGroup"	=>	0	);
			$this->mParams = array_merge($default,$this->extractParameters());
		}
		
		function setLoggedUser($loggedUser=NULL){//always invoke setLoggedUser before setGroup
			$this->mLoggedUser = $loggedUser;
		}
		
		function setViewerType($viewerType=1){
			$this->mViewerType = $viewerType;
		}
		
		function setGroup($group_object){
			try{
				if( !$this->mLoggedUser instanceof Traveler ){
					echo "Expected mLoggedUser to be an instance of Traveler.";
					die;
				}
				
				$this->mGroupObject = $group_object;
				$this->mIsAdminGroup = ( 2 == $this->mGroupObject->getDiscriminator() )? TRUE : FALSE;
				
				if( $this->mIsAdminGroup ){ 
					$cond = new Condition;
					$cond->setAttributeName("name");
					$cond->setOperation(FilterOp::$ORDER_BY);
					$order = new FilterCriteria2();
					$order->addCondition($cond);
					
					$filter = NULL;
					if( 3 == $this->mViewerType && $this->mGroupObject->isParent() ){
						$staffedGroups = Traveler::getStaffedGroups($this->mLoggedUser->getTravelerID(),array($this->mGroupObject->getGroupID()));
						$staffedIDs = array();
						foreach($staffedGroups AS $sg){
							$staffedIDs[] = $sg->getGroupID();
						}
						unset($sg);
					
						$cond2 = new Condition;
						$cond2->setAttributeName("groupID");
						$cond2->setOperation(FilterOp::$IN);
						$cond2->setValue("(".implode(',', $staffedIDs).")");
						$filter = new FilterCriteria2;
						$filter->addBooleanOp("AND");
						$filter->addCondition($cond2);
					}

					$sp_array = $this->mGroupObject->getSubGroupsAndProjects(NULL,$order,self::DISPLAY_ALL,$filter);
					$this->mGroupsAndProjects = $sp_array[0];
					
					if( 3 > $this->mViewerType ){
						$this->mGroupsAndProjects = array_merge(array($this->mGroupObject),$this->mGroupsAndProjects);
						$this->mSubGroupIDs = array($this->mGroupObject->getGroupID());
					}
					foreach($this->mGroupsAndProjects AS $grp){
						$this->mSubGroupIDs[] = $grp->getGroupID();
					}
					unset($grp);
				}else{
					$this->mSubGroupIDs[] = $this->mGroupObject->getGroupID();
				}
				$this->mSubGroupIDs = array_unique($this->mSubGroupIDs);			
			}catch(exception $e){
				$this->renderError($e->getMessage());
			}
		}
		
		function performAction(){
			switch($this->mParams["action"]){
				case self::EVALUATE	:	$this->evaluateInvited(); break;
				case self::SENDINVITATION	:	$this->sendInvitation(); break;
				case self::INVITE_DIFFERENT	:	$this->inviteDifferent(); break;
				default				:	$this->renderInviteMembersForm(); break;
			}
		}
		
		private function renderInviteMembersForm(){
			try{
				$gID = $this->mGroupObject->getGroupID();
			
				$code = <<<BOF
				<script type="text/javascript"> 
				//<![CDATA[
					imManager.setGroupID($gID);
				//]]>
				</script>
BOF;
				Template::includeDependent($code);
			
				$tpl = new Template();
				$tpl->set("group_object",$this->mGroupObject);
				$tpl->set("isAdminGroup",$this->mIsAdminGroup);
				$tpl->set("groups",$this->mGroupsAndProjects);
				$tpl->set("emails",$this->mParams["emails"]);
				$tpl->set("assignToGroup",$this->mParams["assignToGroup"]);
				$tpl->set("emailError",$this->mEmailError);
				$tpl->set("errors",$this->mErrors);
				$tpl->out("travellog/views/member_staff/tpl.FrmInviteMembers.php");
			}catch(exception $e){
				$this->renderError($e->getMessage());
			}
		}
		
		private function evaluateInvited(){
			try{
				
				$travelers = $this->getTravelersByEmail();
			
				if( self::NO_VALID_EMAIL == $this->mEmailError || self::EMAIL_MISSING == $this->mEmailError ){
					$this->renderInviteMembersForm();
					return;
				}

				$gID = $this->mGroupObject->getGroupID();

				$code = <<<BOF
				<script type="text/javascript"> 
				//<![CDATA[
					imManager.setGroupID($gID);
				//]]>
				</script>
BOF;
				Template::includeDependent($code);

				if( "" == $this->mParams["message"] ){
					$this->mParams["message"] = self::getDefaultMessageForGAMembers($this->mGroupObject);
				}
				if( "" == $this->mParams["message2"] ){
					$this->mParams["message2"] = self::getDefaultMessageForNonGAMembers($this->mGroupObject);
				}

				$tpl = new Template();
				$tpl->set("group_object",$this->mGroupObject);
				$tpl->set("isAdminGroup",$this->mIsAdminGroup);
				$tpl->set("groups",$this->mGroupsAndProjects);
				$tpl->set("travelers",$travelers);
				$tpl->set("assignToGroup",$this->mParams["assignToGroup"]);
				$tpl->set("message",$this->mParams["message"]);
				$tpl->set("message2",$this->mParams["message2"]);
				$tpl->set("error",$this->mEmailError);
				$tpl->set("invalidEmails",$this->mErrors);
				$tpl->out("travellog/views/member_staff/tpl.ViewReviewInvitation.php");
			}catch(exception $e){
				$this->renderError($e->getMessage());
			}
		}
		
		private function getTravelersByEmail(){
			$travelers = array();
			$registered = array();
			$nonRegistered = array();
			$nonRegisteredNames = array();

			if( isset($_FILES["contact_data_file"]) ){
				$params = array("columnNames"	=>	array("email","firstname","lastname"),
								"CSVSeparator"	=>	",",
				 				"pathManager"	=>	new PathManager($this->mGroupObject),
								"postFile"		=>	$_FILES["contact_data_file"],
								"discardFile"	=>	TRUE	);
				$this->mExcelCSVUtil = ExcelCSVUtil::getInstance();
				try{
					$this->mExcelCSVUtil->initialize($params);
				}
				catch(exception $e){}
			}

			if( (!isset($this->mParams["emails"]) || "" ==  $this->mParams["emails"]) 
				&& (is_null($this->mExcelCSVUtil) || 
					(is_object($this->mExcelCSVUtil) && ExcelCSVUtil::INVALID_FILE_ERROR == $this->mExcelCSVUtil->getError())) ){
				$this->mEmailError = self::EMAIL_MISSING;
				//$this->renderError("Expected at least a valid email");
				//return $travelers;	
			}else{
				
				$order   = array("\r\n", "\n", "\r");
				$replace = ',';
				$emails = str_replace($order, $replace, $this->mParams["emails"]);
			
				$emails = explode(",",$emails);
				
				$valid_emails = array();
				
				$hasValidPostEmail = FALSE;
				foreach($emails AS $email){
					if( "" == trim($email) || in_array($email,$valid_emails) ){
						continue;
					}
					if( !self::validEmail(trim($email)) ){
						$this->mEmailError = self::EMAIL_INVALID;
						$this->mErrors[] = $email;
						if( !array_key_exists(self::EMAIL_POST_INVALID,$this->mAllEmailErrors) ){
							$this->mAllEmailErrors[] = self::EMAIL_POST_INVALID;
						}
						continue;
					}
					$valid_emails[] = $email;
					try{
						$traveler = Traveler::getTravelerByEmail(trim($email));
						//$travelers[] = ( $traveler instanceof Traveler )? $traveler : $email;
						if( $traveler instanceof Traveler ){
							$registered[] = $traveler;
						}else{
							$nonRegistered[] = trim($email);
							$nonRegisteredNames[] = "";
						}
					}catch(exception $e){
						$this->renderError($e->getMessage());
					}
				}
				unset($email);
				if( 0 < count($registered) || 0 < count($nonRegistered) ){
					$hasValidPostEmail = TRUE;
				}
				
				$hasValidEmailInFile = FALSE;
				$tempRegistered = array();
				$tempNonRegistered = array();
				if( is_object($this->mExcelCSVUtil) && !$this->mExcelCSVUtil->getError() ){
					try{
						$this->mExcelCSVUtil->reset();
						while($data=$this->mExcelCSVUtil->fetchRow()){
							$email = trim($data["email"]);
							if( "" != $email && !in_array($email,$valid_emails) ){
								if( !self::validEmail($email) ){
									$this->mEmailError = self::EMAIL_INVALID;
									$this->mErrors[] = $email." <i>(from file)</i>";
									if( !array_key_exists(self::EMAIL_IN_FILE_INVALID,$this->mAllEmailErrors) ){
										$this->mAllEmailErrors[] = self::EMAIL_IN_FILE_INVALID;
									}
									continue;
								}
								$valid_emails[] = $email;
								try{
									$traveler = Traveler::getTravelerByEmail($email);
									if( $traveler instanceof Traveler ){
										$tempRegistered[] = $traveler;
									}else{
										$tempNonRegistered[] = trim($email);
										if( isset($data["lastname"]) && "" != trim($data["lastname"]) && isset($data["firstname"]) && "" != trim($data["firstname"]) ){
											$nonRegisteredNames[] = trim($data["lastname"]).", ".trim($data["firstname"]);
										}elseif( isset($data["lastname"]) && "" != trim($data["lastname"]) ){
											$nonRegisteredNames[] = trim($data["lastname"]);
										}elseif( isset($data["firstname"]) && "" != trim($data["firstname"]) ){
											$nonRegisteredNames[] = trim($data["firstname"]);
										}else{
											$nonRegisteredNames[] = "";
										}
									}
								}catch(exception $e){
									$this->renderError($e->getMessage());
								}
							}
						}
					}catch(exception $e){}
				}
				if( 0 < count($tempRegistered) || 0 < count($tempNonRegistered) ){
					$hasValidEmailInFile = TRUE;
				}
				if( !$hasValidPostEmail && !$hasValidEmailInFile ){
					$this->mEmailError = self::NO_VALID_EMAIL;
				}
				$registered = array_merge($registered,$tempRegistered);
				$nonRegistered = array_merge($nonRegistered,$tempNonRegistered);
			}
			$travelers = array("registered"=>$registered , "nonRegistered"=>$nonRegistered, "nonRegisteredNames"=>$nonRegisteredNames);
			return $travelers;
		}
		
		private function sendInvitation(){
			$ids = $this->mParams["inviteID"];
			$emails = $this->mParams["invite_email"];
			$this->inviteRegistered($ids);
			$this->inviteNonRegistered($emails);
			ob_end_clean();
			header("location: /members.php?gID=".$this->mGroupObject->getGroupID()."&mode=5");
		}
		
		private function inviteRegistered($ids=array()){
			foreach($ids AS $id){
				$traveler = new Traveler($id);
				if( isset($this->mParams["group_select_$id"]) ){
					$group = new AdminGroup($this->mParams["group_select_$id"]);
				}else{
					$group = new AdminGroup($this->mParams["gID"]);
				}
				if( !$traveler->isSuspended() && ( $group instanceof AdminGroup || $group instanceof Group ) ){
					$group->setCustomMessage(self::finalizeCustomMessageForGAMembers($group,trim($this->mParams["message"]),$traveler->getFirstName()));
					$group->addInvites(array($traveler));
				}
			}
			unset($id);
		}
		
		private function inviteNonRegistered($emails=array()){
			$parentGroup = new AdminGroup($this->mParams["gID"]);
			$inviteList = $parentGroup->getEmailInviteList();
			foreach($emails AS $email){
				//$traveler = Traveler::getTravelerByEmail($email);
				if( isset($this->mParams["group_select_".trim(str_replace(".","_",$email))]) ){
					$group = new AdminGroup($this->mParams["group_select_".trim(str_replace(".","_",$email))]);
					$inviteList = $group->getEmailInviteList();
				}
				else{
					$group = $parentGroup;
				}
				if( ($group instanceof AdminGroup || $group instanceof Group) ){
					/*if( $traveler instanceof Traveler ){
						if( !$traveler->isSuspended() ){
							$group->setCustomMessage(self::finalizeCustomMessageForGAMembers($group,self::getDefaultMessageForGAMembers($group),$traveler->getFirstName()));
							$group->addInvites(array($traveler));
						}
					}else{
						$email_array = array();
						$email_array[] = trim($email);
						if( !$group->isInEmailInviteList($email) ){
							$recipient = "";
							$name_parts = array();
							if( "" != trim($this->mParams["name_text_".trim(str_replace(".","_",$email))]) ){
								$name_parts = explode(",",$this->mParams["name_text_".trim(str_replace(".","_",$email))]);
								if( 2 == sizeof($name_parts) ){
									$recipient = $name_parts[1];
								}else if( 1 == sizeof($name_parts) ){
									$recipient = $name_parts[0];
								}
							}
							$recipient = ("" == trim($recipient))? trim($email) : trim($recipient); 
							$customMessage = self::finalizeCustomMessageForNonGAMembers($group,trim($this->mParams["message2"]),$recipient,trim($email));
							$group->addEmailInvites($email_array);
							$group->setCustomMessage($customMessage);
							$group->addMessageToEmailInvite(trim($email), $group->addEmailInvitationMessage($customMessage));
							if( 2 == sizeof($name_parts) ){
								$group->addEmailInviteAttribute($email, $name_parts[1], $name_parts[0]);
							}else if( 1 == sizeof($name_parts) ){
								$group->addEmailInviteAttribute($email, $name_parts[0], "");
							}else{
								$group->addEmailInviteAttribute($email, "", "");
							}
						}
					}*/
					$email_array = array();
					$email_array[] = trim($email);
					if( !in_array($email, $inviteList) ){
					    $recipient = "";
					    $name_parts = array();
					    if( "" != trim($this->mParams["name_text_".trim(str_replace(".","_",$email))]) ){
					    	$name_parts = explode(",",$this->mParams["name_text_".trim(str_replace(".","_",$email))]);
					    	if( 2 == sizeof($name_parts) ){
					    		$recipient = $name_parts[1];
					    	}else if( 1 == sizeof($name_parts) ){
					    		$recipient = $name_parts[0];
					    	}
					    }
					    $recipient = ("" == trim($recipient))? trim($email) : trim($recipient); 
					    $customMessage = self::finalizeCustomMessageForNonGAMembers($group,trim($this->mParams["message2"]),$recipient,trim($email),$parentGroup);
					    $group->addEmailInvites($email_array);
					    $group->setCustomMessage($customMessage);
					    $group->addMessageToEmailInvite(trim($email), $group->addEmailInvitationMessage($customMessage));
					    if( 2 == sizeof($name_parts) ){
					    	$group->addEmailInviteAttribute($email, $name_parts[1], $name_parts[0]);
					    }else if( 1 == sizeof($name_parts) ){
					    	$group->addEmailInviteAttribute($email, $name_parts[0], "");
					    }else{
					    	$group->addEmailInviteAttribute($email, "", "");
					    }
					}
				}
			}
			unset($email);
		}
		
		private function inviteDifferent(){
			$invitesList = NULL;
			$nonInvitingGroups = array();
			$traveler = NULL;
			$email = NULL;
			$cond = new Condition;
			$cond->setAttributeName('groupID');
			$cond->setOperation(FilterOp::$IN);
			$cond->setValue("(".implode(',',$this->mSubGroupIDs).")");
			$filterCriteria2 = new FilterCriteria2();
			$filterCriteria2->addCondition($cond);
			$customMessage = "";
			if( isset($this->mParams["email"]) ){
				$email = $this->mParams["email"];
				$invitesList = EmailInviteList::retrieve(array("email"=>$email, "filterCriteria2"=>$filterCriteria2));
				$customMessage = self::getDefaultMessageForNonGAMembers($this->mGroupObject);
			}else if( isset($this->mParams["travID"]) ){
				$traveler = new Traveler($this->mParams["travID"]);
				if( !($traveler instanceof Traveler) ){
					$this->renderError("Invalid travelerID [".$this->mParams["travID"]."]");
					return;
				}
				$invitesList = InviteList::retrieve(array("travelerID"=>$this->mParams["travID"], "status"=>0, "filterCriteria2"=>$filterCriteria2));			
				$customMessage = self::getDefaultMessageForGAMembers($this->mGroupObject);
			}else{
				$this->renderError("Expected an email or a travelerID");
				return;
			}
			$invitingIDs = array();
			foreach($invitesList AS $invite){
				$invitingIDs[] = $invite->get("groupID");
			}
			unset($invite);
			$nonInvitingGroups = $this->getNonInvitingGroups($invitingIDs);
			try{
				$gID = $this->mGroupObject->getGroupID();

				$code = <<<BOF
				<script type="text/javascript"> 
				//<![CDATA[
					imManager.setGroupID($gID);
				//]]>
				</script>
BOF;
				Template::includeDependent($code);

				$tpl = new Template();
				$tpl->set("group_object",$this->mGroupObject);
				$tpl->set("valid_groups",$nonInvitingGroups);
				$tpl->set("invitesList",$invitesList);
				$tpl->set("traveler",$traveler);
				$tpl->set("email",$email);
				$tpl->set("message",$customMessage);
				$tpl->out("travellog/views/member_staff/tpl.ViewReviewInviteDifferent.php");
				
			}catch(exception $e){
				$this->renderError($e->getMessage());
			}
		}
		
		private function getNonInvitingGroups($invitingIDs){
			$nonInvitingGroups = array();
			/*if( !in_array($this->mGroupObject->getGroupID(),$invitingIDs) ){
				$nonInvitingGroups[] = $this->mGroupObject;
			}*/
			foreach($this->mGroupsAndProjects AS $grp){
				if( !in_array($grp->getGroupID(),$invitingIDs) ){
					$nonInvitingGroups[] = $grp;
				}
			}
			return $nonInvitingGroups;
		}
		
		private function renderError($message=""){
			$tpl = new Template();
			$tpl->set("message","Message from InviteMembersController: { ".$message." }");
			$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
		}
		
		function extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}
		
		static function validEmail($email){

		   /*$exp = "^[a-z\'0-9]+([._-][a-z\'0-9]+)*@([a-z0-9]+([._-][a-z0-9]+))+$";

		   if(eregi($exp,$email)){
		      if(checkdnsrr(array_pop(explode("@",$email)),"MX")){
		        return true;
		      }else{
		        return false;
		      }
		   }else{
		      return false;
		   } */   
		   if (!preg_match('/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i', $email)){
		   	return false;
		   }
		   return true;
		}
		
		public static function getDefaultMessageForGAMembers($group=NULL){
			if( !($group instanceof Group) && !($group instanceof AdminGroup) ){
				return "";
			}
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 	= SiteContext::getInstance();
			$siteName 		= $domainConfig->getEmailName();
			$siteUrl 		= "http://".$domainConfig->getServerName();
			
			$isCISNetwork = FALSE;
			
			$group_name = "[your_group_name]";
			if( 1 == $group->getDiscriminator() ){
				$group_name = stripslashes($group->getName());
			}else{
				if( 241 == $group->getGroupID() || 241 == $group->getParentID() ){
					$isCISNetwork = TRUE;
				}
			}
			
			
			if( $isCISNetwork ){
				$message="Hi [recipient],

This is an invitation to join the group $group_name. GoAbroad is a great resource for CIS students!

By joining, you will be able to:

- View the exciting travel journals of participants on CIS study abroad programs
- View photos of traveling students and post photos of your own adventures abroad
- Blog about your experiences and share your adventures with all of us back at home
- Connect with students who have been on or are going on your same program
- Link your travel blog and photos to your Facebook account!

CIS will be sending e-messages to you about your study abroad program through GoAbroad.com, including info about visas, flights, and excursions.

To accept this invitation, please follow this link:
[accept_link]

We hope that GoAbroad will help you to get more out of your CIS experience, by providing you with resources, tools and connections that will link you with your *new* community abroad, while helping you stay connected to your community at home.


Many thanks,
The GoAbroad Network Team and the CIS Program Team
$siteUrl


This e-mail was sent from an unattended mailbox. Please do not reply. Please send any questions using the form on $siteUrl/feedback.php";
				
			}else{
			
				$message = "Hi [recipient],

You have an invitation to join the group $group_name.

To accept this group invitation, please follow the link below:
[accept_link]

To deny this group invitation, please follow the link below:
[deny_link]

To view your other invites, follow the link below:
[other_invites_link]



Many thanks,


$siteName
$siteUrl



This e-mail was sent from an unattended mailbox. Please do not reply. Send any questions to $siteUrl/feedback.php";
			}
			return $message;
		}
		
		public static function finalizeCustomMessageForGAMembers($group=NULL,$message="",$recipient=""){
			if( !($group instanceof Group) && !($group instanceof AdminGroup) ){
				return $message;
			}
			
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 		= SiteContext::getInstance();
			//$siteName 		= $domainConfig->getEmailName();
			$siteUrl 		= $domainConfig->getServerName();
			
			$accept_link = "http://$siteUrl/membership.php?gID=".$group->getGroupID()."&mode=accept";
			$deny_link = "http://$siteUrl/membership.php?gID=".$group->getGroupID()."&mode=deny";
			$other_invites_link = "http://$siteUrl/membership.php?gID=".$group->getGroupID();
			
			$message = str_replace("[recipient]",$recipient,$message);
			$message = str_replace("[your_group_name]",$group->getName(),$message);
			$message = str_replace("[accept_link]",$accept_link,$message);
			$message = str_replace("[deny_link]",$deny_link,$message);
			$message = str_replace("[other_invites_link]",$other_invites_link,$message);
			//$message = str_replace("[your_site_name]",$siteName,$message);
			//$message = str_replace("[your_site_URL]",$siteUrl,$message);
			
			return $message;
		}
		
		public static function getDefaultMessageForNonGAMembers($group=NULL){
			if( !($group instanceof Group) && !($group instanceof AdminGroup) ){
				return "";
			}
			
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 		= SiteContext::getInstance();
			$siteName 		= in_array($_SERVER["SERVER_NAME"],array("www.goabroad.net","dev.goabroad.net","goabroad.net.local"))? "GoAbroad Network" : $domainConfig->getSiteName();
			$sender			= in_array($_SERVER["SERVER_NAME"],array("www.goabroad.net","dev.goabroad.net","goabroad.net.local"))? "The GoAbroad Network Team" : $domainConfig->getEmailName();
			$siteUrl 		= "http://".$domainConfig->getServerName();
			
			$isCISNetwork = FALSE;
			$isISVolunteersNetwork = FALSE;
			$isvGID = 1760;
			if( 2 == $group->getDiscriminator() && $group->isParent() ){
				$maingroup_name = GaString::makePossessive(stripslashes($group->getName()));
				$maingroup = stripslashes($group->getName());
				if( 241 == $group->getGroupID() ){
					$isCISNetwork = TRUE;
				}
				if($isvGID == $group->getGroupID() ){
					$isISVolunteersNetwork = TRUE;
				}
			}else{
				$maingroup_name = "[your_group_name]";
				$maingroup = "[your_group_name]";
				
				if( 2 == $group->getDiscriminator() && 241 == $group->getParentID() ){
					$isCISNetwork = TRUE;
				}
				if( 2 == $group->getDiscriminator() && $isvGID == $group->getParentID() ){
					$isISVolunteersNetwork = TRUE;
				}
			}
			
			$group_name = "[your_group_name]";
			if( 1 == $group->getDiscriminator() ){
				$group_name = stripslashes($group->getName());
				$maingroup = stripslashes($group->getName());
				$maingroup_name = GaString::makePossessive($group->getName());
			}
			
			if( $isCISNetwork ){
				$message = "Hi [recipient],

This is an invitation to join the group [your_group_name]. GoAbroad is a great resource for CIS students!

By joining, you will be able to:

- View the exciting travel journals of participants on CIS study abroad programs
- View photos of traveling students and post photos of your own adventures abroad
- Blog about your experiences and share your adventures with all of us back at home
- Connect with students who have been on or are going on your same program
- Link your travel blog and photos to your Facebook account!

CIS will be sending e-messages to you about your study abroad program through GoAbroad.com, including info about visas, flights, and excursions.

To accept this invitation, please follow this link:
[registration_link]

We hope that GoAbroad will help you to get more out of your CIS experience, by providing you with resources, tools and connections that will link you with your *new* community abroad, while helping you stay connected to your community at home.


Many thanks,
The GoAbroad Network Team and the CIS Program Team
$siteUrl


This e-mail was sent from an unattended mailbox. Please do not reply. Please send any questions using the form on $siteUrl/feedback.php";

			}elseif($isISVolunteersNetwork){
				$message = "Hi [recipient],

ISV invites you to join our brand new online community so you can connect with other ISV travellers and share your experience with the world.

Please create your account and join [your_group_name] so you can meet other students who have travelled with ISV and future ISV travellers. Our goal is to build a huge online community of our 20,000+ alumni so we can all stay in touch.

The ISV community page is amazing, by joining this page you will be able to start your own online blog/journal so your friends and family can keep track of what you are doing.  These journals have a stack of features and will allow you to:

- Upload photos and videos
- Share your experience in a much more in-depth way that short Facebook postings
- Find out what's new through news and [your_group_name] calendar events that will show up on your page. 
- Share your experiences with family and friends who may not be your friends on Facebook (because you may not always want grandma to see what you did on the weekend!)
- Stay connected with all ISV alumni
- Receive email broadcasts from ISV 
- Write about new travel experiences you are going to embark on
- Receive updates from your project organisations 
- Share with your community all your thoughts and feelings before, during and after your trip
- See other friends' photos and videos
- Join discussions with members through their Discussion Boards
- Help promote volunteering to the world at large
- Participate in surveys and evaluations conducted by ISV

This is a brand new feature ISV has created to support you better as a participant and past ISV traveller. Please login now and create your own profile and start posting. To join [your_group_name], please follow the link below:

[registration_link]


Many thanks,
The ISV Team
$siteUrl


This e-mail was sent from an unattended mailbox. Please do not reply. Send any questions to $siteUrl/feedback.php";
			}else{
				$message = "Hi [recipient],

Your friends at $group_name have invited you to join their group on the $siteName online community.

By joining their group, you will be able to:

- Find out what's new through News and $group_name calendar  events that will show up on your page. 
- Have access to $maingroup_name resource files, like program information, itineraries, forms and others
- Receive email broadcasts from $group_name
- Join discussions with members through their Discussion Boards
- Participate in surveys and evaluations conducted by $group_name

To join $group_name, please follow the link below:

[registration_link]

 

Many thanks,


$sender
$siteUrl



This e-mail was sent from an unattended mailbox. Please do not reply. Send any questions to $siteUrl/feedback.php";
			}

			return $message;
		}
		
		public static function finalizeCustomMessageForNonGAMembers($group=NULL,$message="",$recipient="",$email="",$parentGroup=NULL){
			if( !($group instanceof Group) && !($group instanceof AdminGroup) ){
				return $message;
			}
			
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 		= SiteContext::getInstance();
			//$siteName 		= $domainConfig->getEmailName();
			$siteUrl 		= $domainConfig->getServerName();
			
			$crypt = new Crypt();
			$urlParam = urlencode($crypt->encrypt('traveler&email='.$email));
			$registration_link = "http://$siteUrl/register.php?$urlParam";
			
			if( 2 == $group->getDiscriminator() && !$group->isParent() ){
				$your_maingroup_name = $parentGroup ? $parentGroup->getName() : $group->getParent()->getName();
			}else{
				$your_maingroup_name = $group->getName();
			}
			if( "" == $recipient ){
				$message = str_replace("[recipient]",$email,$message);
			}else{
				$message = str_replace("[recipient]",$recipient,$message);
			}
			
			if( !strpos($message,"[registration_link]") ){
				$message .= "
				
				
				
To join $your_maingroup_name, please follow the link below:
[registration_link]";
			}
			
			//$message = str_replace("[your_site_name]",$siteName,$message);
			$message = str_replace("[your_maingroup_name]",$your_maingroup_name,$message);
			$message = str_replace("[registration_link]",$registration_link,$message);
			$message = str_replace("[your_group_name]",$group->getName(),$message);
			//$message = str_replace("[your_site_URL]",$siteUrl,$message);
			
			return $message;
		}
	}