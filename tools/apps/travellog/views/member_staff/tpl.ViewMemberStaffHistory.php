<div class = "content">
	<? if (0 < count($arrHistory)) : ?>
		<? $ctr = 0; ?>
		<ul class = "users">
			<? foreach ($arrHistory as $indHistory) : ?>
				<?	$ctr++;
					/*$mGroup = $indHistory['group'];
					$mTraveler = $indHistory['traveler'];

					$mProfile		= $mTraveler->getTravelerProfile();
					$mTravelerId	= $mTraveler->getTravelerID();
					$mUserName		= $mTraveler->getUsername();
					$traveler_type	= $mTraveler->getTravelerType();
					$thumbNail		= $mProfile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$truncate_username = HtmlHelpers::truncateWord($mUserName, 15);
					//$invitation_date= MembersPanel::getInvitationDate($mGroup->getGroupID(),$mTravelerId);
					if ($invitation_date != MembersPanel::getInvitationDate($mGroup->getGroupID(),$mTravelerId)):
						$invitation_date = MembersPanel::getInvitationDate($mGroup->getGroupID(),$mTravelerId);
						echo date("D M d, Y",$invitation_date);
					endif;*/
					$username		= explode(" ",$indHistory);
					$user			= substr($username[1],7,-1);
					$mTraveler 		= new Traveler(Traveler::getTravelerIDByUsername($user));
					$mProfile		= $mTraveler->getTravelerProfile();
					$mTravelerId	= $mTraveler->getTravelerID();
					$mUserName		= $mTraveler->getUsername();
					$traveler_type	= $mTraveler->getTravelerType();
					$thumbNail		= $mProfile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$truncate_username = HtmlHelpers::truncateWord($mUserName, 15);
					$mGroup 		= $mTraveler->getGroups();
				?>
				<li>
					<a class = "thumb" title = "<?=$mUserName?>" href = "/<?=$mUserName?>"> 
						<img src = "<?=$thumbNail?>" width = "65" height = "65" alt = "Member Thumbnail"/>
					</a>
					<div class = "details overflow">
						<a class = "username" title = "<?=$mUserName?>" href = "/<?=$mUserName?>"></a><?=$indHistory?>
						<em><strong>Traveler Type: </strong><?=$traveler_type?></em>
						<? if(is_object($mGroup[0])):?><em><strong>Group: </strong><?=$mGroup[0]->getName()?></em><?endif;?>
						
					</div>
				</li>
				<? if ($ctr>=10): break; endif;?>
			<? endforeach; ?>
		</ul>
	<? else : ?>
		<span>No Record found.</span>
	<? endif; ?>
</div>