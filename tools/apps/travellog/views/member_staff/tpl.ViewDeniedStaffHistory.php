<div class = "content">
	<? if (0 < count($arrHistory)) : ?>
		<ul class = "users">
			<? foreach ($arrHistory as $indHistory) : ?>
				<?
					$mGroup = $indHistory['group'];
					$mTraveler = $indHistory['traveler'];

					$mProfile		= $mTraveler->getTravelerProfile();
					$mTravelerId	= $mTraveler->getTravelerID();
					$mUserName		= $mTraveler->getUsername();
					$traveler_type	= $mTraveler->getTravelerType();
					$thumbNail		= $mProfile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$truncate_username = HtmlHelpers::truncateWord($mUserName, 15);
				?>
				<li>
					<a class = "thumb" title = "<?=$mUserName?>" href = "/<?=$mUserName?>"> 
						<img src = "<?=$thumbNail?>" width = "65" height = "65" alt = "Member Thumbnail"/>
					</a>
					<div class = "details overflow">
						<a class = "username" title = "<?=$mUserName?>" href = "/<?=$mUserName?>"><?=$truncate_username?></a>
						<em><strong>Traveler Type: </strong><?=$traveler_type?></em>
						<em><strong>Group: </strong><?=$mGroup->getName()?></em>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
	<? else : ?>
		<span>No denied invitations for staff.</span>
	<? endif; ?>
</div>