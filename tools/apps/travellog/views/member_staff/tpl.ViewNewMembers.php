<div id="new_members" class="section">
	<h2> <span> Showing New Members </span> </h2>
	
	<div class="section_small_details"> 
		<form onsubmit="gmsManager.updateNewMembers(); return false;">
			<ul class="list_small_details">											
				<li>
					<label for="quick_count">Display Last </label>
					<select id="quick_count" name="quick_count">
						<? for($i=1; $i<=10; $i++ ){ ?>
							<option value="<?=$i?>" <?=($i==$qCount)? 'selected="selected"' : '' ?> ><?=$i?></option>
						<? } ?>
					</select>
					<select id="quick_range" name="quick_range">
						<option value="0" <?=(0==$qRange)? 'selected="selected"' : '' ?> >Day(s)</option>
						<option value="1" <?=(1==$qRange)? 'selected="selected"' : '' ?> >Week(s)</option>
						<option value="2" <?=(2==$qRange)? 'selected="selected"' : '' ?> >Month(s)</option>
						<option value="3" <?=(3==$qRange)? 'selected="selected"' : '' ?> >Year(s)</option>
					</select>
				</li>										

				<li>
					<label for="sort_by_filter">By Membership Date</label>
					<select id="sort_by_filter" name="sort_by_filter">
						<option value="0" <?=(0==$sortByFilter)? 'selected="selected"' : '' ?> >Ascending &uarr;</option>
						<option value="1" <?=(1==$sortByFilter)? 'selected="selected"' : '' ?> >Descending &darr;</option>
					</select>								
				</li>
			
				<li>
					<input type="submit" name="refresh_list" value="Refresh List" id="refresh_list" />
				</li>
			
				<li>
					<img id="loading_content" src="/images/loading_small.gif" style="display:none;" />
				</li>										
			</ul>										
		</form>
	</div>									
	<!-- END section_small_details -->									
	
	<div id="search_result_content" class="content">
		<? echo $controller->renderMemberSearchResultList(); ?>						
	</div>
		
	<!-- END content -->
	<div class="foot">
		
	</div>
	
</div><!-- END show_new_members -->