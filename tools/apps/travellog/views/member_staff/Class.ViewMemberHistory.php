<?php
/**
 * @author Mercy
 * 2008-11-21
 */
 
	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.MembersPanel.php';
	require_once 'travellog/model/updates/Class.GroupMembershipUpdates.php';

	class ViewMemberHistory{

		private $mType		= 1;
		private $mGroup		= null;
		private $arrHistory	= array();

		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		
		function setType($_type = 1){
			$this->mType = $_type;
		}

		function retrieve(){
			if (2 == $this->mType)
				$this->renderDenied();
			else
				$this->renderUpdates();
		}

		function renderDenied(){
			$arrDenied = MembersPanel::getDeniedMemberInvitations($this->mGroup);

			if (0 < count($arrDenied)){
				foreach($arrDenied as $each){
					try{
						$mGroup = GroupFactory::instance()->create( array($each['groupID']) );
					}
					catch(exception $e){
						$mGroup = null;
					}
					try{
						$mTraveler = new Traveler( $each['travelerID'] );
					}
					catch(exception $e){
						$mTraveler = null;
					}
					

					if ($mGroup && $mTraveler){
						$mData = array();
						$mData['traveler'] = $mTraveler;
						$mData['group'] = $mGroup[0];
						$this->arrHistory[] = $mData;
					}
				}
			}
		}

		function renderUpdates(){
			//$arrAccepted = MembersPanel::getAcceptedMemberInvitations($this->mGroup);
			$membership = new GroupMembershipUpdates($this->mGroup);
			$this->arrHistory = $membership->getUpdates();
			//$arrAccepted = $membership->getUpdates();
/*			if (0 < count($arrAccepted)){
				foreach($arrAccepted as $each){
					try{
						$mGroup = GroupFactory::instance()->create( array($each['groupID']) );
					}
					catch(exception $e){
						$mGroup = null;
					}
					try{
						$mTraveler = new Traveler( $each['travelerID'] );
					}
					catch(exception $e){
						$mTraveler = null;
					}

					if ($mGroup && $mTraveler){
						$mData = array();
						$mData['traveler'] = $mTraveler;
						$mData['group'] = $mGroup[0];
						$this->arrHistory[] = $mData;
					}
					
					$this->arrHistory[] = $each;
				}
			}*///var_dump($this->arrHistory);exit;
			//$this->arrHistory = $arrAccepted;
		}

		function render(){
			$tpl = new Template;

			$tpl->set("arrHistory",		$this->arrHistory);

			$tpl->out("travellog/views/member_staff/tpl.ViewMemberStaffHistory.php");
		}
	}
?>
