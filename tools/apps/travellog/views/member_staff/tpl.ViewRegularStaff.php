<? if (0 < count($arrRegularStaff)) : ?>
	<div class="section_small_details">
		<label for="other_group_selector"> In Group</label>
		<select name="other_group_selector" id="groupselect_regular_staff">
			<option value="0" <?if(0==$subGroupId):?>selected<?endif;?>>--All Groups--</option>
			<? foreach($arrGroups as $indSubGroup) : ?>
				<option
						value = "<?=$indSubGroup->getGroupID()?>"
						<?if($subGroupId==$indSubGroup->getGroupID()):?>selected<?endif;?>>
						<?=$indSubGroup->getName()?>
				</option>
			<? endforeach; ?>
		</select>
		<input
				type	= "button"
				name	= "btnRefreshList"
				id		= "btnRefreshList"
				value	= "Refresh List" />
		<span id="regularstaff_actions" class="actions">
			<? if ( 0 < $subGroupId ) : ?>
				<a href="members.php?gID=<?=$groupId?>&mode=4&type=2&cID=<?=$subGroupId?>">+ Add Staff</a>
			<? else : ?>
				<a href="members.php?gID=<?=$groupId?>&mode=4&type=2">+ Add Staff</a>
			<? endif; ?>
		</span>
	</div>
	<div class="content">
		<ul id="regularstaff_list" class="users member_list">
			<? foreach( $arrRegularStaff as $each ) : ?>
				<?
					$indStaff = $each['traveler'];
					$subGroupName = $each['groups'];

					$profile	= $indStaff->getTravelerProfile();
					$username	= $indStaff->getUsername();
					$travelerId	= $indStaff->getTravelerID();
					$email		= $profile->getEmail();
					$thumbNail	= $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$fullname	= $profile->getFirstName() . " " . $profile->getLastName();
					$designation_date = MembersPanel::getDesignationDate($travelerId, $subGroupId);
				?>
				<li class="staff">
					<span class="staff_type">
						Staff
					</span>
					<a class="thumb" href="/<?=$username?>"> 
						<img src="<?=$thumbNail?>" width="65" height="65" alt="Member Thumbnail"/>
					</a>				
					<div class="details overflow">
						<a href="/<?=$username?>" class="username" title="<?=$username?>">
							<?=$username?>
							<span class="full_name"><?=$fullname?></span>
						</a>
						<div class="designation_date">
							<span>
								Designation Date: 
								<span>
									<? if (0 < strlen($designation_date)) : ?>
										<?=$d->set($designation_date)->friendlyFormat()?>
									<? else : ?>
										<i>[Not Recorded]</i>
									<? endif; ?>
								</span>
							</span>						
						</div>

						<div class="designation_group">
							<span>Assigned to: <span>
							<? if ( 150 < strlen($subGroupName) ) : ?>
								<?=substr($subGroupName, 0, 150).'..'?>
							<? else : ?>
								<?=$subGroupName?>
							<? endif; ?>	
							</span> </span>
						</div>
						<? if ($loggedUser->getTravelerID() != $travelerId) : ?>
							<div class="actions">
								<a href="javascript:void(0)" class="manage_staff" id="manage_staff_<?=$travelerId?>">Manage Staff &rarr;</a> |
								<a href="messages.php?act=messageTraveler&id=<?=$travelerId?>"> Send Message &rarr; </a>
								<? if ( 0 < $subGroupId ) : ?>
									&nbsp;|
									<a class="remove_staff" href="javascript: void(0)" id="remove_staff_<?=$travelerId?>">Remove Staff</a>
									<input type="hidden" class="remove_staff_name_holder" id="remove_staff_name_holder_<?=$travelerId?>" value="<?=$username?>" />
								<? endif; ?>
							</div>
						<? endif; ?>
					</div>
				</li>
			<? endforeach; ?>
		</ul>		
		<? if ($paging->getTotalPages() > 1) : ?>
			<div class="pagination">
				<div class="page_markers">
					<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalCount?>
				</div>	
				<?$paging->getFirst()?>
				&nbsp;|&nbsp;
				<?$paging->getPrevious()?>
				&nbsp;&nbsp;
				<?$paging->getLinks()?>
				&nbsp;&nbsp;
				<?$paging->getNext()?>
				&nbsp;|&nbsp;
				<?$paging->getLast()?>
			</div>
		<? endif; ?>
	</div>
<? else : ?>
	<div class="section_small_details">
		<p class="help_text"> <span>You have not assigned staff for your groups in <?=$groupname?></span>.
			<span id="regularstaff_actions" class="actions">
				<a href="members.php?gID=<?=$groupId?>&mode=4&type=2">+ Add Staff</a>
			</span>
		</p>
	</div>
<? endif; ?>

<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$("#btnRefreshList").click(function(){
				var gId = $('option:selected', $('#groupselect_regular_staff')).val();
				StaffManager.searchStaff( gId );
			});
			$(".remove_staff").click(function(){
				var elId	= this.id;
				var elName	= this.className;
				var travelerId = elId.substr(elName.length+1);
				var username = $("#remove_staff_name_holder_"+travelerId).val();

				StaffManager.removeStaff(travelerId, username, "<?=str_replace('\'', '\&rsquot;', $subGroupName)?>");
			});
			$(".manage_staff").click(function(){
				// get travelerID of staff
				var elId	= this.id;
				var elName	= this.className;
				var travelerId = elId.substr(elName.length+1);
				var gId = $('option:selected', $('#groupselect_regular_staff')).val();
				StaffManager.manageStaff( travelerId, gId );
			})
		});
	})(jQuery);
</script>