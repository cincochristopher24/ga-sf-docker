<?php
	/*
	 *	@author CheWyl
	 *	12 November 2008
	 */

	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.AdminGroup.php';

	class ViewAccordion{
		private $mMode			= 1;
		private $mGroupId		= 0;
		private $mViewerType	= 0;
		private $mDiscriminator = 2;
		private $mIsLogged		= false;
		private $mGroup			= NULL;

		function setMode($_mode = 1){
			$this->mMode = $_mode;
		}
		function setGroupId($_groupId = 0){
			$this->mGroupId = $_groupId;
		}
		function setIsLogged($_isLogged = false){
			$this->mIsLogged = $_isLogged;
		}
		function setViewerType($_viewerType = 0){
			$this->mViewerType = $_viewerType;
		}
		function setDiscriminator($_discriminator=2){
			$this->mDiscriminator = $_discriminator;
		}
		function setGroup($group=NULL){
			$this->mGroup = $group;
		}

		function render(){
			if( 1 == $this->mDiscriminator && 1 != $this->mViewerType ){
				return;
			}
			$tpl = new Template();
			$mLocation = 'http://' . $_SERVER['HTTP_HOST'] . '/members.php?gID=' . $this->mGroupId;

			$tpl->set("location",	$mLocation);
			$tpl->set("mode",		$this->mMode);
			$tpl->set("groupId",	$this->mGroupId);
			$tpl->set("isLogged",	$this->mIsLogged);
			$tpl->set("viewerType", $this->mViewerType);
			$tpl->set("discriminator", $this->mDiscriminator);
			$tpl->set("group",		$this->mGroup);
			$tpl->out("travellog/views/member_staff/tpl.ViewAccordion.php");
		}
	}
?>