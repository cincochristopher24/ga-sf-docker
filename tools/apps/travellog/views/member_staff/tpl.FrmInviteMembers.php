<div id="invite_members" class="section">
	<h2>
		<span>Invite Members</span>			
	</h2> 

	<?/*div class="section_small_details"> 
		This is where you can invite your members.
	</div*/?>									
	
	<div class="content">
		<? if( InviteMember::NO_ERROR != $emailError ): ?>
			<div class="errors">
				<ul>
				<? if( InviteMember::EMAIL_MISSING == $emailError ): ?>
					<li>Please specify at least an email.</li>
				<? else: ?>
					<?	if( 0 < count($errors) ): ?>
						<li>You didn't specify a valid email address. Please verify the following:</li>
						<? foreach($errors AS $error): ?>
							<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=$error?></strong> is invalid.</li>
						<? endforeach; ?>
					<?	else: ?>
						<li>There are no valid email addresses in the file you selected.</li>
					<?	endif; ?>
				<? endif; ?>
				</ul>
				<div class="foot"></div>
			</div>
		<? endif; ?>
		<form action="/members.php?gID=<?=$group_object->getGroupID();?>&mode=3" id="invite_form" name="invite_form" enctype="multipart/form-data" method="post">
			
		<table id="members_invite_form">
			<tbody>
				<tr>
					<td class="left_side" >
						<label for="contact_data_file">
							Import email addresses from your contact list
						</label>
						<span class="label_small_details">
							You may select an <strong>Excel (.xls)</strong> or a <strong>CSV (.csv)</strong> file which contains the information of the persons you wish to invite.
							<br /><a href="javascript:void(0)" onclick="imManager.showContactDataFileRules(); return false;">Learn more &raquo;</a>
							<br />
							<br />
							<br />
						</span>
					</td>
					<td class="right_side">
						<input onchange="imManager.validateImportFile(this); return false;" type="file" id="contact_data_file" name="contact_data_file" />
					</td>
				</tr>
				<tr>
					<td class="left_side">
						 <label for="emails"> 
							Email Address
						 </label>
						<span class="label_small_details">
							You may also enter email addresses separated by commas.
						</span>											
					</td>
					<td class="right_side"> 
						<textarea id="emails" name="emails" class="members_invite_textarea" ><?=$emails?></textarea>
					</td>
				</tr>
				<?php
					$display = 'display:none;';
					if( $isAdminGroup && !$group_object->isSubGroup() ){
						$display = '';
					}
				?>
				<tr style="<?=$display?>">
					<td class="left_side">
						 <label for="assignToGroup"> 
							Assign to Group
						 </label>
						<span class="label_small_details">
							Where do you want to assign them?
						</span>											
					</td>
					<td class="right_side">
						<select id="assignToGroup" name="assignToGroup" class="_">
							<? foreach($groups AS $group){ ?>
								<? if( $group->getGroupID() == $assignToGroup ): ?>
									<option value="<?=$group->getGroupID()?>" selected="selected"><?=stripslashes($group->getName())?> <? if( 2 == $group->getDiscriminator() && $group->isParent() ): ?>(Main Group)<?endif;?></option>
								<? else: ?>
									<option value="<?=$group->getGroupID()?>"><?=stripslashes($group->getName())?> <? if( 2 == $group->getDiscriminator() && $group->isParent() ): ?>(Main Group)<?endif;?></option>
								<? endif;?>
							<? } ?>																																													
						</select>
					</td>
				</tr>		
				
				<tr>
					<td class="left_side"></td>
					<td class="right_side">
						<input type="button" name="Invite" value="Invite" id="button_invite" onclick="imManager.checkEmails();"/>
					</td>	
				</tr>
				
			</tbody>	
		</table>
			<input type="hidden" id="action" name="action" value="1" />
		</form>
	</div>
</div>