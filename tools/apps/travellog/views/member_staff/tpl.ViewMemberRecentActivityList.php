<?php if (0 < count($recentActivities)) : ?>
<ul id="search_result_list" class="users member_list" >
	<?php
	 	$isFirst = TRUE;
		$label_first = "";
		foreach($recentActivities as $executionDate => $update):
			if( $isFirst ){
				$label_first = " first";
				$isFirst = FALSE;
			}else{
				$label_first = "";
			}
		?>
		<?php $doerType = $update->getDoerType() ?>
		<li class="<?=(UpdateDoerType::REGULAR_STAFF == $doerType || UpdateDoerType::SUPER_STAFF == $doerType ? 'staff' : 'member')?><?=$label_first?>">
			<?php
				$doerLabel = '';
				if ( 3 > $viewerType ){
					$doerLabel = (UpdateDoerType::REGULAR_STAFF == $doerType || UpdateDoerType::SUPER_STAFF == $doerType)? UpdateDoerType::getLabel($doerType) : '';
				}else if( 3 == $viewerType ){
					$doerLabel = (UpdateDoerType::REGULAR_STAFF == $doerType || UpdateDoerType::SUPER_STAFF == $doerType)? 'Staff' : '';
				}
			?>
			<?/*<span class="staff_type"><?=$doerLabel?></span>*/?>
			<a class="thumb" title = "<?=$update->getDoer()->getName()?>" href = "<?=$update->getDoer()->getUrl()?>"> 
				<img src = "<?=$update->getDoer()->getProfilePicture()?>" width = "65" height = "65" alt = "Member Thumbnail"/>
			</a>
			<div class="details overflow">
				<a class="username" title = "<?=$update->getDoer()->getName()?>" href = "<?=$update->getDoer()->getUrl()?>">
					<?=HtmlHelpers::truncateWord($update->getDoer()->getName(), 15)?>
					<?php if( $update->getDoer()->isTraveler() ):?>
						<span class = "full_name"><?=$update->getDoer()->getFullName()?></span>
					<?php endif; ?>
				</a>
				<?=GaDateTime::descriptiveDifference($update->getExecutionDate(), GaDateTime::dbDateTimeFormat())?> Ago - <?=$update->getAction()?>
				<?//php if( UpdateAction::TRAVELER_MESSAGED_GROUP != $update->getRetrieverType()):?>
				<div class="actions">
					<a href="<?=$update->getContextLink()?>" >Go To <?=$update->getContextName()?></a>
				</div>
				<?//php endif; ?>
			</div>
		</li>
	<?php endforeach;?>
</ul>
<?php else : ?>
<div><strong>No recent activities.</strong></div>
<?php endif; ?>