<?php

	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");

	class InviteList{
		
		private $mFields	=	array(	"travelerID"	=>	0,
										"groupID"		=>	0,
										"status"		=>	0,
										"inviteDate"	=> "0000-00-00 00:00:00");
		
		public function __construct($data=array()){
			$this->mFields = array_merge($this->mFields,$data);
		}
		
		public function set($field,$val){
			$this->mFields[$field] = $val;
		}
		
		public function get($field){
			if( isset($this->mFields[$field]) ){
				return $this->mFields[$field];
			}
			return "";
		}
		
		public static function retrieve($params=array()){
			try{
				$db = new dbHandler();
				
				$default = array(	"travelerID"	=>	NULL,
									"groupID"		=>	NULL,
									"status"		=>	NULL,
									"filterCriteria2"	=>	NULL );
				$params = array_merge($default,$params);
				
				$where_expression = "";
				
				if( !is_null($params["groupID"]) ){
					$where_expression = " AND `tblInviteList`.`groupID` = ".$db->makeSqlSafeString($params["groupID"]);
				}
				if ( !is_null($params["filterCriteria2"]) ){

					$filterCondition = $params["filterCriteria2"]-> getConditions();
					$attribute = $filterCondition[0]-> getAttributeName();
					$value     = $filterCondition[0]-> getValue();

					switch($filterCondition[0]->getOperation()){

	    				case FilterOp::$EQUAL:
	    					$operator	= " = ";
	    					break;
	    				case FilterOp::$NOT_EQUAL:
	    					$operator   = " <> ";
	    					break;
						case FilterOp::$IN:			//added bu Jul, 2008-11-19, in this case, the list value must have already been placed inside parenthesis
							$operator	= " IN ";
							break;
	    				default:
	    					$operator   = " = ";
	    					break;
	    			}

					$where_expression = " AND " .$attribute . $operator . $value;
				}
				if( !is_null($params["travelerID"]) ){
					$where_expression .= " AND `tblInviteList`.`travelerID` = ".$db->makeSqlSafeString($params["travelerID"]);
				}
				if( !is_null($params["status"]) ){
					$where_expression .= " AND `tblInviteList`.`status` = ".$db->makeSqlSafeString($params["status"]);
				}
				
				$sql = "SELECT `tblInviteList`.*
						FROM `Travel_Logs`.`tblInviteList`
						WHERE 1=1 $where_expression";

				$rs = new iRecordset($db->execute($sql));
				$arr = array();
				foreach( $rs as $row ){
					$arr[] = new InviteList($row);
				}
				return $arr;
				
			}catch(exception $e){
				throw $e;
			}
		}
	}