<?php
	require_once 'Class.Template.php';
	require_once 'travellog/views/member_staff/Class.PendingMember.php';

	class ViewPendingMember{

		private $mGroup			= null;
		private $mPending		= null;
		private $mViewerType	=	6;
		private $mLoggedUser	= null;

		function setGroup($_group = ''){
			$this->mGroup = $_group;
		}
		
		function setViewerType($viewer_type=6){
			$this->mViewerType = $viewer_type;
		}

		function setLoggedUser($user=NULL){
			$this->mLoggedUser = $user;
		}
		

		function retrieve(){
			$this->mPending = new PendingMember;
			$this->mPending->setGroup($this->mGroup);
			$this->mPending->setViewerType($this->mViewerType);
			$this->mPending->setLoggedUser($this->mLoggedUser);
			$this->mPending->retrieve();
		}

		function render(){
			$this->mPending->render();
		}
	}