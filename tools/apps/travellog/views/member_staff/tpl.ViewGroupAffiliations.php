<?php
	$title = "";
	switch($action){
		case 2	:	$title = "Select groups where to assign <strong>".$traveler->getFullName()."</strong>";
					break;
		case 3	:	$title = $traveler->getFullName();
					$title .= ( 1 < sizeof($subgroups) )? " is now a member of the following groups:" : " is now a member of ";
					break;
		case 4	:	$title = "Manage group affiliations of <strong>".$traveler->getFullName()."</strong>";
					break;
		case 5	:	$title = "Group Affiliation Management Results for ".$traveler->getFullName();
					break;
	}
?>
<? if( 2 == $action || 4 == $action ): ?>
<table class="table_popbox">
	<tbody>
		<tr>
			<td class="popbox_topLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_topRight"></td>
		</tr>
		<tr>
			<td class="popbox_border"></td>
			<td class="popbox_content">

		<h4 id="popup_header" class="header"><strong><?=$title?></strong></h4>
		<div id="popup_confirm" class="confirm">
			<ul>
			<?	foreach($subgroups AS $group){ 
					$checked = "";
					if( $group->isMember($traveler) ){
						$checked = "checked='checked'";
					}
			?>
				<li>
					<input type="checkbox" name="group_checkbox" value="<?=$group->getGroupID()?>" <?=$checked?> />&nbsp;<?=stripslashes($group->getName())?>
				</li>
			<?	}
			 	unset($group); ?>
			</ul>
		</div>
		<div id="popup_buttons" class="buttons_box">
			<input type="button" class="prompt_button" value="OK" onclick="gmsManager.checkGroups(<?=$action?>); return false;" id="btnSubmit"/> 
			<input type="button" class="prompt_button" value="Cancel" onclick="gmsManager.removeThickbox(); return false;" id="btnCancel"/>
		</div>							
			</td>
			<td class="popbox_border"></td>
		</tr>
		<tr>
			<td class="popbox_bottomLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_bottomRight"></td>
		</tr>
	</tbody>
</table>
	
<? endif; ?>

<? if( 3 == $action ): ?>
	<div>
		<p>
			<?=$title?>
			<? if( 1 == sizeof($subgroups) ): ?>
				<?=stripslashes($subgroups[0]->getName())?>
			<? endif; ?>
			<? if( 1 < sizeof($subgroups) ): ?>
				<ul>
				<? foreach($subgroups AS $group){ ?>
					<li><?=stripslashes($group->getName())?></li>
				<? }
					unset($group); ?>
				</ul>
			<? endif; ?>
		</p>
	</div>
<? endif; ?>

<? if( 5 == $action ): ?>
	<div>
		<p>
			<?=$title?>
		</p>
		<? if( 0 < sizeof($assigned_to) ): ?>
			<p>
				Assigned to:
				<ul>
					<?	foreach($assigned_to AS $group){ ?>
						<li><?=stripslashes($group->getName())?></li>
					<?	}
						unset($group);	?>
				</ul>
			</p>
		<? endif; ?>
		<? if( 0 < sizeof($removed_from) ): ?>
			<p>
				Removed membership in:
				<ul>
					<?	foreach($removed_from AS $group){ ?>
						<li><?=stripslashes($group->getName())?></li>
					<?	}
						unset($group);	?>
				</ul>
			</p>
		<? endif; ?>
	</div>
<? endif; ?>