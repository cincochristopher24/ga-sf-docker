<? if (0 < count($arrTraveler)): ?>
	<div class="section_small_details">
		Displaying <?=count($arrTraveler)?> <?=(1==count($arrTraveler))?' Traveler ':'Travelers '?>
		with Pending Invitations
	</div>
	<div class="content">		
		<ul id="pending_members_list" class="users member_list">
			<?
				foreach($arrTraveler as $member) :
					$username = $member->getUsername();
					$profile = $member->getTravelerProfile();
					$travelerId = $member->getTravelerID();
					$traveler_type = $member->getTravelerType();
					$traveler_photo = $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$countryID = ($profile->getCity() != NULL ) ? $profile->getCity()->getCountry()->getCountryID() : 0;
					$truncate_username = HtmlHelpers::truncateWord($member->getUsername(), 15);
					$thumbNail = $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$fullname = $profile->getFirstName() . ' ' . $profile->getLastName();

					$myPendingSubGroupList = array();
					if (isset($pendingStaffList))
						$myPendingSubGroupList = $pendingStaffList[$member->getTravelerID()];
					if (isset($displayLinkList))
						$displayLink = $displayLinkList[$member->getTravelerID()];
			?>
				<li class="member">
					<a class="thumb" href="/<?= $profile->getUserName()?>">
						<img src="<?=$thumbNail?>" width="65" height="65" alt="<?=$profile->getUserName()?>"/>
					</a>
					<div class="details">
						<? if ( 1 == $member->isActive() ): ?>
							<a href="/<?=$profile->getUserName()?>" class="username" title="<?=$profile->getUserName()?>">
								<?=(strlen($profile->getUserName()) > 50) ? substr($profile->getUserName(), 0, 50).'...' : $profile->getUserName()?>
								<span class="full_name"><?=$fullname?></span>
							</a>
						<? else : ?>
							<?=(strlen($profile->getEmail()) > 50) ? substr($profile->getEmail(), 0, 50).'...' : $profile->getEmail()?>
						<? endif; ?>
						<div class="pending_alt_action">
							<? if ($displayLink) : ?>
								<a href="members.php?gID=<?=$groupId?>&mode=4&tID=<?=$travelerId?>&Invite">Invite to a different Group &rarr;</a>
							<? endif; ?>
						</div>
						<?
							foreach($myPendingSubGroupList as $pendingSubGroup) : 
								$datenow = time();
								$tMonth = date("m", strtotime($pendingSubGroup['date']));
								$tDay = date("d", strtotime($pendingSubGroup['date']));
								$tYear = date("Y", strtotime($pendingSubGroup['date']));
								$invitedate =  mktime(0, 0, 0, $tMonth, $tDay, $tYear);

								$datediff = floor(($datenow - $invitedate) / (60 * 60 * 24));
								if (1 == $datediff)
									$cntPending = 'pending for ' . $datediff . ' day';
								else if (1 < $datediff)
									$cntPending = 'pending for ' . $datediff . ' days';
								else
									$cntPending = 'newly invited';

								$grp_factory = GroupFactory::instance();
								$subGroup = $grp_factory->create(array($pendingSubGroup['groupId']));
						?>
							<div class="pending_on_group_list">
								<div class="pending_on_group">
									<?
										if (strlen($pendingSubGroup['name']) > 50)
											echo substr($pendingSubGroup['name'], 0, 50) . '...';
										else
											echo $pendingSubGroup['name'];
									?>
									<span> <?=$cntPending?> </span>
								</div>
								
								<span>
									<?$param = 'gID='.$groupId.'&mode=5&cID='.$pendingSubGroup['groupId'].'&tID='.$travelerId.'&viewerType='.$viewerType?>
									<a href="javascript:void(0)" onclick="StaffManager.inviteAgain('<?=$param?>', '<?=$username?>', '<?=str_replace("'", "&rsquot;", $pendingSubGroup['name'])?>')">Resend Invitation</a> |
								</span>
								<span>
									<?$param = 'gID='.$groupId.'&mode=5&cID='.$pendingSubGroup['groupId'].'&tID='.$travelerId.'&viewerType='.$viewerType?>
									<a class="negative_action" href="javascript:void(0)" onclick="StaffManager.cancelInvite('<?=$param?>', '<?=$username?>', '<?=str_replace("'", "&rsquot;", $pendingSubGroup['name'])?>')">Cancel Invitation</a>
								</span>
							</div>
						<? endforeach; ?>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
	</div>
<!-- END content -->	
<? else : ?>
	<div class="section_small_details">
		<p class="help_text"> <span>No Pending Staff Invitations</span></p>
	</div>
<? endif; ?>