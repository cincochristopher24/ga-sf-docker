<? if (0 < count($superStaff)) : ?>
	<div class = "section_small_details">
		The Admin
		<span class = "actions">
			<a href = "members.php?gID=<?=$groupId?>&mode=4&type=1">+ Add Admin</a>
		</span>
	</div>
	<div class = "content">
		<ul id = "superstaff_list" class = "users member_list">
			<? foreach ($superStaff as $indStaff) : ?>
				<?
					$profile	= $indStaff->getTravelerProfile();
					$username	= $indStaff->getUsername();
					$travelerId	= $indStaff->getTravelerID();
					$email		= $profile->getEmail();
					$thumbNail	= $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$fullname	= $profile->getFirstName() . " " . $profile->getLastName();
					$designation_date = MembersPanel::getDesignationDate($travelerId, $groupId);
				?>
				<li class = "staff">
					<span class = "staff_type"> 
						<?php echo ( $travelerId == $administratorID ) ? 'Owner/Admin' : 'Admin' ?>
					</span>
					<a class = "thumb" href = "/<?$username?>"> 
						<img src = "<?=$thumbNail?>" width = "65" height = "65" alt = "<?=$username?>"/>
					</a>
					<div class = "details">
						<a href = "/<?=$username?>" class = "username" title = "<?=$username?>">
							<?=$username?>
							<span class = "full_name"><?=$fullname?></span>
						</a>
						<div class = "designation_date">
							<span>
								Designation Date: 
								<span>
									<? if (0 < strlen($designation_date)) : ?>
										<?=$d->set($designation_date)->friendlyFormat()?>
									<? else : ?>
										<i>[Not Recorded]</i>
									<? endif; ?>
								</span>
							</span>						
						</div>

						<div class = "designation_group">
							<span>Assigned to: <span>All Groups</span> </span>
						</div>
						<? if ($loggedUser->getTravelerID() != $travelerId) : ?>
							<div class = "actions">
								<!--
								<a
									href	= "javascript:void(0)"
									onclick	= "StaffManager.manageSuperStaff('gID=<?=$groupId?>&mode=2&tID=<?=$travelerId?>');return false;">
									Manage Admin &rarr; 
								</a> |
								-->
								<a href = "messages.php?act=messageTraveler&id=<?=$travelerId?>"> Send Message &rarr; </a> |
								<a
									class	= "negative_action"
									href	= "javascript: void(0)"
									onclick = "StaffManager.removeSuperStaff('gID=<?=$groupId?>&mode=2&tID=<?=$travelerId?>', '<?=$username?>', '<?=str_replace("'", "&rsquot;", $groupName)?>')">
									Remove Admin
								</a>
							</div>
						<? else : ?>
							<div class="actions">This is YOU.</div>
						<? endif; ?>
					</div>
				</li>
			<? endforeach; ?>
		</ul>		
		<? if ($paging->getTotalPages() > 1) : ?>
			<div class = "pagination">
				<div class = "page_markers">
					<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalCount?>
				</div>	
				<?$paging->getFirst()?>
				&nbsp;|&nbsp;
				<?$paging->getPrevious()?>
				&nbsp;&nbsp;
				<?$paging->getLinks()?>
				&nbsp;&nbsp;
				<?$paging->getNext()?>
				&nbsp;|&nbsp;
				<?$paging->getLast()?>
			</div>
		<? endif; ?>
	</div>
<? else : ?>
	<div class = "section_small_details">
		<p class="help_text"> <span>You have not assigned an admin for your group yet</span>.
			<span class = "actions">
				<a href = "members.php?gID=<?=$groupId?>&mode=4&type=1">+ Add Admin</a>
			</span>	
		</p>
	</div>
<? endif; ?>