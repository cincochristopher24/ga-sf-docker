<p>
	<? if( "membership invitation" == $description ): ?>
		You have <strong><?=$action?></strong> the <strong><?=$description?></strong>
		sent to <strong><?=$traveler_name?></strong> in joining <strong><?=$group_name?></strong>.
	<? endif; ?>
	<? if( "join request" == $description ): ?>
		You have <strong><?=$action?></strong> the <strong><?=$description?></strong>
		of <strong><?=$traveler_name?></strong> in <strong><?=$group_name?></strong>.
	<? endif; ?>
</p>