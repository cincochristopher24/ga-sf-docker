<?php

	require_once ('Class.Template.php');
	require_once ('Class.Paging.php');
	require_once ('Class.ObjectIterator.php');
	require_once ('travellog/model/Class.Condition.php');
	require_once ('travellog/model/Class.FilterOp.php');
	require_once ('travellog/model/Class.FilterCriteria2.php');
	require_once ('travellog/model/Class.AdminGroup.php');
	require_once ('travellog/model/Class.Traveler.php');

	class JoinRequest{
		
		const DISPLAY_REQUESTS 	=	1;
		const FETCH_REQUESTS	=	2;
		const APPROVE_REQUEST	=	3;
		const DENY_REQUEST		=	4;
		
		//this variable is set in AbstractGroupMembersController if the logged in user is a staff 
		//and the current group is a parent group; the user is a staff in some subgroups
		private static $mStaffedSubgroupIDs = NULL;
		private static $mRequestArray = NULL;//note: $mRequestArray[0] is an array of requestlists; $mRequestArray[1] is the total number of requests
		
		private $mGroupObject = NULL;
		private $mRequestList = NULL;
		private $mRequestCount = 0;
		private $mSubGroups = array();
		private $mIsAdminGroup = TRUE;
		private $mLoggedUser = NULL;
		private $mViewerType = 1;
		
		private $mFilterCriteria2 = NULL;
		
		private $mPage = 1;
		private $mRpp = 25;
		
		private $mPageClickAction = "";
		
		function __construct(){
			$default = array("action" 		=>	self::DISPLAY_REQUESTS,
							"gID"			=>	0,
							"tID"			=>	0);
			$this->mParams = array_merge($default,$this->extractParameters());
			if( isset($this->mParams["page"]) ){
				$this->mPage = $this->mParams["page"];
			}
		}
		
		function setViewerType($viewerType=1){
			$this->mViewerType = $viewerType;
		}
		
		function setStaffedSubgroupIDs($subgroupIDs=array()){
			self::$mStaffedSubgroupIDs = $subgroupIDs;
		}
		function setRequestArray($requests=array()){
			self::$mRequestArray = $requests;
		}
		
		function setGroup($group_object){
			$this->mGroupObject = $group_object;
			$this->mIsAdminGroup = ( 2 == $this->mGroupObject->getDiscriminator() )? TRUE : FALSE;
			$rowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			
			$filter = NULL;
			
			$group_ids = array($this->mGroupObject->getGroupID());
			if( $this->mIsAdminGroup ){
				if( 3 == $this->mViewerType && $this->mIsAdminGroup && $this->mGroupObject->isParent() ){
					if( is_null(self::$mStaffedSubgroupIDs) ){
						$staffedGroups = Traveler::getStaffedGroups($this->mLoggedUser->getTravelerID(),array($this->mGroupObject->getGroupID()));
						$staffedIDs = array();
						foreach($staffedGroups AS $sg){
							$staffedIDs[] = $sg->getGroupID();
						}
						unset($sg);
					}else{
						$staffedIDs = self::$mStaffedSubgroupIDs;
					}
					
					$cond2 = new Condition;
					$cond2->setAttributeName("tblRequestList.groupID");
					$cond2->setOperation(FilterOp::$IN);
					$cond2->setValue("(".implode(',', $staffedIDs).")");
					$filter = new FilterCriteria2;
					$filter->addBooleanOp("AND");
					$filter->addCondition($cond2);
					$group_ids = $staffedIDs;
				}
		
				if( 3 > $this->mViewerType ){
					$sp_array = $this->mGroupObject->getSubGroupsAndProjects();
					$this->mSubGroups = $sp_array[0];
					
					$group_ids = array($this->mGroupObject->getGroupID());
					foreach($this->mSubGroups AS $sb){
						$group_ids[] = $sb->getGroupID();
					}
					unset($sb);
				}
			}else{
				$group_ids = array($this->mGroupObject->getGroupID());
			}
			
			if( is_null(self::$mRequestArray) ){
				$jr_array = $this->mGroupObject->getJoinRequests($rowsLimit,$filter);
			}else{
				$jr_array = self::$mRequestArray;
			}
			$this->mRequestList = $jr_array[0];
			$this->mRequestCount = $jr_array[1];
			
			$cond = new Condition;
			$cond->setAttributeName('groupID');
			$cond->setOperation(FilterOp::$IN);
			$cond->setValue("(".implode(',',$group_ids).")");
			$this->mFilterCriteria2 = new FilterCriteria2();
			$this->mFilterCriteria2->addCondition($cond);
		}
		
		function setLoggedUser($user=NULL){
			$this->mLoggedUser = $user;
		}
		
		function performAction(){
			switch($this->mParams["action"]){
				case self::DISPLAY_REQUESTS	:	$this->renderRequests(); break;
				case self::FETCH_REQUESTS	:	$this->renderRequestList(); break;
				case self::APPROVE_REQUEST	:	$this->approveJoinRequest(); break;
				case self::DENY_REQUEST		:	$this->denyJoinRequest(); break;
				default						:	$this->renderRequests(); break;
			}
		}
		
		function renderRequests(){
			$tpl = new Template();
			$tpl->set("group_object",$this->mGroupObject);
			$tpl->set("request_count",$this->mRequestCount);
			$tpl->set("controller",$this);
			$tpl->out("travellog/views/member_staff/tpl.ViewRequestToJoin.php");
		}
		
		function renderRequestList(){
			$this->mPageClickAction = "jrManager.fetchJoinRequests";
			$paging = new Paging( $this->mRequestCount, $this->mPage, 'grpID=' . $this->mGroupObject->getGroupID(), $this->mRpp );
			$paging->setOnclick($this->mPageClickAction);
			$iterator = new ObjectIterator( $this->mRequestList, $paging );
			
			$tpl = new Template();
			$tpl->set("group_object",$this->mGroupObject);
			$tpl->set("requesters",$this->mRequestList);
			$tpl->set("filter",$this->mFilterCriteria2);
			$tpl->set("paging",$paging);
			$tpl->set("loggedUser",$this->mLoggedUser);
			$tpl->out("travellog/views/member_staff/tpl.ViewRequestToJoinList.php");
		}
		
		private function approveJoinRequest(){
			$group = new AdminGroup($this->mParams["gID"]);
			$traveler = new Traveler($this->mParams["tID"]);
			if( $group instanceof AdminGroup && $traveler instanceof Traveler){
				$group->addMember($traveler);
				$traveler_name = $traveler->getFullName();
				$group_name = $group->getName();
				$action = "accepted";
				$description = "join request";
				$tpl = new Template();
				$tpl->set("traveler_name",$traveler_name);
				$tpl->set("group_name",$group_name);
				$tpl->set("action",$action);
				$tpl->set("description",$description);
				$tpl->out("travellog/views/member_staff/tpl.ViewNotice.php");
			}else{
				$error = "";
				if( !($group instanceof AdminGroup) ){
					$error .= "<p>".$this->mParams["gID"]." is not a valid Group ID<p>";
				}
				if( !($traveler instanceof Traveler) ){
					$error .= "<p>".$this->mParams["tID"]." is not a valid Traveler ID<p>";
				}
				$tpl = new Template();
				$tpl->set("message",$error);
				$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
			}
		}
		
		private function denyJoinRequest(){
			$group = new AdminGroup($this->mParams["gID"]);
			$traveler = new Traveler($this->mParams["tID"]);
			if( $group instanceof AdminGroup && $traveler instanceof Traveler){
				$group->denyRequest($traveler->getTravelerID());
				$traveler_name = $traveler->getFullName();
				$group_name = $group->getName();
				$action = "denied";
				$description = "join request";
				$tpl = new Template();
				$tpl->set("traveler_name",$traveler_name);
				$tpl->set("group_name",$group_name);
				$tpl->set("action",$action);
				$tpl->set("description",$description);
				$tpl->out("travellog/views/member_staff/tpl.ViewNotice.php");
			}else{
				$error = "";
				if( !($group instanceof AdminGroup) ){
					$error .= "<p>".$this->mParams["gID"]." is not a valid Group ID<p>";
				}
				if( !($traveler instanceof Traveler) ){
					$error .= "<p>".$this->mParams["tID"]." is not a valid Traveler ID<p>";
				}
				$tpl = new Template();
				$tpl->set("message",$error);
				$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
			}
		}
		
		function extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}	

		private function calcOffset($page=1){
			return ($page-1) * $this->mRpp;
		}
	}