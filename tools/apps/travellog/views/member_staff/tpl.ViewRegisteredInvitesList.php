<ul id = "pending_members_list" class = "users member_list">
	<? 	$isFirst = TRUE;
		foreach( $registeredInvites AS $traveler ) :
			$inviting_groups = $traveler->getGroupMembershipInvites($filter);
			$class = ($isFirst)? "member first" : "member";
			$isFirst = FALSE;
			$server = $_SERVER["SERVER_NAME"];
	?>
		<li class = "<?=$class?>">
			<a class = "thumb" href = "/<?=$traveler->getUserName()?>">
				<img src = "<?=$traveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width = "65" height = "65" alt = "<?=$traveler->getUserName()?>"/>
			</a>
			<div class = "details">
				
					<a href = "/<?=$traveler->getUserName()?>" class = "username" title = "<?=$traveler->getUserName()?>">
						<?=$traveler->getUserName()?>&nbsp;&nbsp;<span class = "full_name"><?=stripslashes($traveler->getFullName())?></span>
					</a>
					<div class="member_email">
						<?=$traveler->getEmail()?>
					</div>
				<? if( $isAdminGroup && $group_object->isParent() ): ?>
					<div class = "pending_alt_action">
						<a href = "http://<?=$server?>/members.php?gID=<?=$group_object->getGroupID()?>&mode=3&action=3&travID=<?=$traveler->getTravelerID()?>" >Invite to a different Group &rarr;</a>
					</div>
				<? endif; ?>
				<?
					foreach($inviting_groups as $group) :
						$params = array("travelerID"	=>	$traveler->getTravelerID(),
										"groupID"		=>	$group->getGroupID(),
										"status"		=>	0 );
						try{
							$invite_list_array = InviteList::retrieve($params);
						}catch(exception $e){
							continue;
						}
						
						if( 0 == sizeof($invite_list_array) ){
							continue;
						}
						$invite_list = $invite_list_array[0];
						$invitedate = $invite_list->get("inviteDate");
						$datenow = time();
						$tMonth = date("m", strtotime($invitedate));
						$tDay = date("d", strtotime($invitedate));
						$tYear = date("Y", strtotime($invitedate));
						$invitedate =  mktime(0, 0, 0, $tMonth, $tDay, $tYear);

						$datediff = floor(($datenow - $invitedate) / (60 * 60 * 24));
						if (1 == $datediff)
							$cntPending = 'pending for ' . $datediff . ' day';
						else if (1 < $datediff)
							$cntPending = 'pending for ' . $datediff . ' days';
						else
							$cntPending = 'newly invited';
				?>
					<div class = "pending_on_group_list">
						<div class="pending_on_group">
							<?
								if (strlen(stripslashes($group->getName())) > 50)
									echo substr(stripslashes($group->getName()), 0, 50) . '...';
								else
									echo stripslashes($group->getName());
							?>
							<span> <?=$cntPending?> </span>
						</div>
						<? if( $viewerType < 4 ): ?>
							<span>
								<a href = "javascript:void(0)" onclick = "piManager.resendRegisteredInvitation(<?=$traveler->getTravelerID()?>,'<?=$traveler->getFullName()?>',<?=$group->getGroupID()?>,'<?=str_replace("'","&rsquot;",$group->getName())?>'); return false;">Resend Invitation</a> |
							</span>
							<span>
								<a class = "negative_action" href = "javascript:void(0)" onclick = "piManager.cancelRegisteredInvitation(<?=$traveler->getTravelerID()?>,'<?=$traveler->getFullName()?>',<?=$group->getGroupID()?>,'<?=str_replace("'","&rsquot;",$group->getName())?>'); return false;">Cancel Invitation</a>
							</span>
						<? endif; ?>
					</div>
				<? endforeach; ?>
			</div>
		</li>
	<? endforeach; ?>
</ul>

<img id="loading_page_registered" src="/images/loading_small.gif" style="display:none;"/>&nbsp;
<? if ( $paging->getTotalPages() > 1 ):?>
	<? $start = $paging->getStartRow() + 1; 
	   $end = $paging->getEndRow(); 
	?>
	<? $paging->showPagination() ?>
<? endif; ?>