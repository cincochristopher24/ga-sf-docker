<?php
	require_once 'Class.Template.php';
	require_once 'travellog/views/member_staff/Class.SearchMember.php';

	class ViewSearchMember{

		private $mGroup			= null;
		private $mSearchMember	= null;
		private $mViewerType	=	6;
		private $mLoggedUser	= null;

		function setGroup($_group = ''){
			$this->mGroup = $_group;
		}
		
		function setViewerType($viewer_type=6){
			$this->mViewerType = $viewer_type;
		}

		function setLoggedUser($user=NULL){
			$this->mLoggedUser = $user;
		}

		function retrieve(){
			$this->mSearchMember = new SearchMember();
			$this->mSearchMember->setGroup($this->mGroup);
			$this->mSearchMember->setAccessedGroup($this->mGroup);
			$this->mSearchMember->setParams($_REQUEST);
			$this->mSearchMember->setViewerType($this->mViewerType);
			$this->mSearchMember->setLoggedUser($this->mLoggedUser);
		}

		function render(){
			$this->mSearchMember->renderSearchForm();
		}
	}