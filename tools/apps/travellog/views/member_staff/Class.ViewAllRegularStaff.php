<?php
	/*
	 *	@author CheWyl
	 *	12 November 2008
	 */

	require_once 'Class.Paging.php';
	require_once 'Class.Template.php';

	class ViewAllRegularStaff{

		private $mPage				= 1;
		private $mTotalCount		= 0;
		private $mViewerType		= 0;
		private $mRowsPerPage		= 10;
		private $mPaging			= null;
		private $mGroup				= null;
		private $mLoggedUser		= null;
		private $mIsLogged			= false;

		function setPage($_page = 1){
			$this->mPage = $_page;
		}
		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}
		function setIsLogged($_isLogged = false){
			$this->mIsLogged = $_isLogged;
		}
		function setViewerType($_viewerType = 0){
			$this->mViewerType = $_viewerType;
		}
		function setRowsPerPage($_rows = 10){
			$this->mRowsPerPage = $_rows;
		}

		function removeAsStaff($_travelerId = 0){
			if ($this->mGroup)
				$this->mGroup->removeStaff($_travelerId);
		}

		function retrieve(){
			// set rowslimit per page
			$mRowsLimit = new RowsLimit($this->mRowsPerPage, ($this->mPage-1));

			if($this->mGroup->isSubGroup()){
				// get all regular staff
				$this->arrAllStaff = $this->mGroup->getAllGroupStaff($mRowsLimit);
				// merge administrator to the staff list
				$this->arrAllStaff = array_merge(array($this->mGroup->getAdministrator()), $this->arrAllStaff);
				$this->mTotalCount = $this->mGroup->getTotalRecords() + 1;
				$mQueryString = "gID=" . $this->mGroup->getGroupID() . "&mode=2";
			}
			$this->mPaging = new Paging( $this->mTotalCount, $this->mPage, $mQueryString, $this->mRowsPerPage );
			$this->mPaging->setScriptName('/members.php');
		}

		function render(){
			$tpl = new Template();

			$tpl->set("d",				new GaDateTime);
			$tpl->set("group",			$this->mGroup);
			$tpl->set("paging",			$this->mPaging);
			$tpl->set("totalCount",		$this->mTotalCount);
			$tpl->set("viewerType",		$this->mViewerType);
			$tpl->set("arrStaff",		$this->arrAllStaff);
			$tpl->set("isLogged",		$this->mIsLogged);
			$tpl->set("loggedUser",		$this->mLoggedUser);
			$tpl->set("groupId",		$this->mGroup->getGroupID());
			$tpl->set("groupName",		$this->mGroup->getName());
			$tpl->set("administratorID",	$this->mGroup->getParent()->getAdministratorID());

			$tpl->out("travellog/views/member_staff/tpl.ViewAllRegularStaff.php");
		}
	}
?>