<? $server = $_SERVER["SERVER_NAME"]; ?>
<div id="invite_members" class="section">
	<h2>
		<span>Invite to a Different Group</span>			
	</h2> 								
	
	<div class="content">
		<form id="review_invitation_form" name="review_invitation_form" action="/members.php?gID=<?=$group_object->getGroupID()?>&mode=3" method="post">
		<table id="members_review_form">
			<tbody>

				<? if( $traveler instanceof Traveler ):
					$type = 1;
					$typeLabel = "";
				?>
				
				<tr class="registered">
					<td class="checkbox">
						<? if( 0 == $traveler->isSuspended() && 0 < sizeof($valid_groups) ): ?>
							<input type="checkbox" name="inviteID[]"value="<?=$traveler->getTravelerID()?>" checked="checked"/>
						<? endif; ?>
					</td>
					<td class="member_details">
						<div class="users overflow">
							<a href="http://<?=$server?>/<?=$traveler->getUsername()?>" title="View <?=$traveler->getUsername()?>'s Pictures" class="thumb" target="_blank">
								<img class="pic" src="<?=$traveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" alt="<?=$traveler->getUsername()?>" />													
							</a>	
							<div class="details overflow">
								<a href="http://<?=$server?>/<?=$traveler->getUsername()?>" class="username" title="View <?=$traveler->getUsername()?>'s Profile" target="_blank"> 
									<?=$traveler->getUsername()?> 
									<span class="full_name"> <?=$traveler->getFullName()?></span>
								</a>
								<div class="member_email">
									<?=$traveler->getEmail()?>
								</div>													
								<div class="hint">
									Existing GoAbroad.Net User
								</div>	
								<? if( 0 < sizeof($invitesList) ): ?>
								<div class="pending_on_group_list">
									<span class="hint">Has pending invitation in:</span>
									<? 	$isFirst = TRUE;
										foreach($invitesList AS $invite):
											$group = new AdminGroup($invite->get("groupID"));
											if( $isFirst ){
												echo stripslashes($group->getName());
											}else{
												echo ", ".stripslashes($group->getName());
											}
											if( 2 == $group->getDiscriminator() && $group->isParent() ){
												echo " (Main Group)";
											}
									 		$isFirst = FALSE;
										endforeach; ?>
								</div>
								<? endif; ?>
							</div>																						
							<!-- END details overflow -->												
							<div class="generic_form_input notopborder" id="group_designation">
								<? if( 0 == $traveler->isSuspended() && 0 < sizeof($valid_groups) ):	?>
									<label for="group_select"> Assign To:</label>
									<select name="group_select_<?=$traveler->getTravelerID()?>">
										<? 	$isFirst = TRUE;
											foreach($valid_groups AS $group){ ?>
												<? if( $isFirst ): ?>
													<option value="<?=$group->getGroupID()?>" selected="selected"><?=stripslashes($group->getName())?> <? if( 2 == $group->getDiscriminator() && $group->isParent() ): ?>(Main Group)<?endif;?></option>
												<? else: ?>
													<option value="<?=$group->getGroupID()?>" ><?=stripslashes($group->getName())?> <? if( 2 == $group->getDiscriminator() && $group->isParent() ): ?>(Main Group)<?endif;?></option>
												<? endif; ?>
										<? 
												$isFirst = FALSE;
											} 
											unset($group);
										?>														
									</select>
								<? else: ?>
										<? if( $traveler->isAdvisor() ): ?>
											<span class="hint">This email is used for an advisor group. It can't be invited as a member.</span>
										<? elseif( 0 == sizeof($valid_groups) ): ?>
											<span class="hint">You don't have any more groups where you can invite this traveler.</span>
										<? else: ?>
											<span class="hint">This traveler is currently suspended. It can't be invited as a member.</span>
										<? endif; ?>
								<? endif; ?>									
							</div>
																			
							<!-- END group_designation -->												
							
						</div>
						<!-- END users -->
					</td>
				</tr>
				<!-- END registered member -->
				
				<? endif; //not instance of Traveler ?> 
					
				<? if( !is_null($email) ): 
					$traveler = $email;
					$invite_entry = ( 0 < sizeof($invitesList) )? $invitesList[0] : new EmailInviteList();
					$type = 2;
					$typeLabel = "2";
				?>
				<tr class="non_registered">
					<td class="checkbox">
						<? if( 0 < sizeof($valid_groups) ): ?>
							<input type="checkbox" name="invite_email[]" value="<?=trim($traveler)?>" checked="checked"/>
						<? endif; ?>
					</td>
					<td class="member_details">
						<div class="users overflow">	
							<a class="thumb" href="" onclick="return false;">
								<img class="pic" alt="thumbnail" src="/images/default_images/user_thumb.gif"/>
							</a>												
							<div class="details overflow">																										
								<div class="nonregistered_email">	<?=$traveler?> 	<span class="full_name"> <?=$invite_entry->get("firstname")." ".$invite_entry->get("lastname")?></span></div>																										
								<div class="hint"> Not yet registered	</div>
								<? if( 0 < sizeof($invitesList) ): ?>
								<div class="pending_on_group_list">
									<span class="hint">Has pending invitation in:</span>
									<? 	$isFirst = TRUE;
										foreach($invitesList AS $invite):
											$group = new AdminGroup($invite->get("groupID"));
											if( $isFirst ){
												echo $group->getName();
											}else{
												echo ", ".$group->getName();
											}
											if( 2 == $group->getDiscriminator() && $group->isParent() ){
												echo " (Main Group)";
											}
									 		$isFirst = FALSE;
										endforeach; ?>
								</div>
								<? endif; ?>																																						
							</div>
							<!-- END details overflow -->							
							
							<div  class="generic_form_input notopborder" id="group_designation">
								<? if( 0 < sizeof($valid_groups) ): ?>
									<label for="group_select"> Assign To:</label>
									<select name="group_select_<?=trim(str_replace('.','_',$traveler))?>">
										<? 	$isFirst = TRUE;
											foreach($valid_groups AS $group){ ?>
												<? if( $isFirst ): ?>
													<option value="<?=$group->getGroupID()?>" selected="selected"><?=stripslashes($group->getName())?> <? if( 2 == $group->getDiscriminator() && $group->isParent() ): ?>(Main Group)<?endif;?></option>
												<? else: ?>
													<option value="<?=$group->getGroupID()?>" ><?=stripslashes($group->getName())?> <? if( 2 == $group->getDiscriminator() && $group->isParent() ): ?>(Main Group)<?endif;?></option>
												<? endif; ?>
										<? 
												$isFirst = FALSE;
											} 
											unset($group);
										?>													
									</select>
								<? else: ?>
									<span class="hint">You don't have any more groups where you can invite this email owner.</span>
								<? endif; ?>														
							</div>
							<!-- END group_designation -->	
							<?php
								$style = ( is_null($invite_entry->get("lastname")) )? "" : "display:none;";
								$name_value = ( is_null($invite_entry->get("lastname")) )? "" : $invite_entry->get("lastname").", ".$invite_entry->get("firstname");
							?>
							<div class="generic_form_input" id="name_nonregistered" style="<?=$style?>">
								<label for="name_text"> Name:</label> 
								<input class="textbox" type="text" name="name_text_<?=trim(str_replace('.','_',$traveler))?>" value="<?=$name_value?>" id="name_text_<?=trim($traveler)?>" /> <span class="hint"> (e.g Last Name, First Name)
							</div>
							<!-- END name_nonregistered -->																	
						</div>
						<!-- END users -->											
						
					</td>
				</tr>
				<!-- End non registered user -->
				<? endif; ?>			
				
			</tbody>	
		</table>
		<? if( 0 < sizeof($valid_groups) ): ?>
		<div id="msg_preview" class="section">
			<h2>
				<!--span> Message Preview </span--> 
				<span id="message_action<?=$typeLabel?>" class="actions">
					This is the message that will be sent in the invitation. Click on <a href="" onclick="imManager.editMessage(<?=$type?>); return false;">Edit Message</a> to modify your message.
				</span>
			</h2>
			
			<div class="content">
				<textarea id="default_message<?=$typeLabel?>" readonly="true" style="display:none;"><?=$message?></textarea>
				<div id="reg_link_missing" class="errors" style="display: none;">
					<ul>
						<li>It seems that the text [registration_link] was removed from the message. However, the text is necessary so the recipient can view the link. Our mail server will append it at the end of the notification to be sent. Or you may insert it back. It is recommended to copy the text below into the message:<br /><br /></li>
						<li>To join [your_group_name], please follow the link below:<br />[registration_link]</li>
					</ul>
					<div class="foot"></div>
				</div>
				<textarea id="message<?=$typeLabel?>" name="message<?=$typeLabel?>" readonly="true" style="overflow: scroll;overflow-y:scroll;overflow-x: hidden; overflow:-moz-scrollbars-vertical;"><?=$message?></textarea>
				<div id="edit_msg_action<?=$typeLabel?>" style="display:none;">
					<input type="button" name="name" value="Save Changes" onclick="imManager.saveEditMessage(<?=$type?>); return false;"/>
					<input type="button" name="name" value="Cancel" onclick="imManager.cancelEditMessage(<?=$type?>); return false;"/>										
				</div><!-- End edit_msg_action -->
													
				<!--p id="msg_preview_text">										 
				</p-->
			</div>
			<!-- END content -->
		</div><!-- END msg_preview -->
		<? endif; ?>
		<? if( (!is_null($email) || ( ($traveler instanceof Traveler) && 0 == $traveler->isSuspended())) && 0 < sizeof($valid_groups) ): ?>
		<div id="">
			<input type="button" id="send_invite_button" value="Send Invitation" onclick="imManager.continueInvitation();" />
			<input type="hidden" id="action" name="action" value="" />
		</div>
		<? else: ?>
		<div id="msg_preview" class="section">
			<h2>
				<span>Notice</span>
				<span class="actions"><a href="http://<?=$server?>/members.php?gID=<?=$group_object->getGroupID()?>&mode=5">Back to Pending Invitations Page</a></span>
			</h2>
			<div id="msg_preview_text" class="content">
				<p>Cannot continue sending invitation.</p>
			</div>
		</div>
		<? endif;?>
		
		</form>												
	</div>
	<!-- END content -->
</div>
<!-- END invite_members -->