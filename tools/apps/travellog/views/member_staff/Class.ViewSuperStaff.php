<?php
	/*
	 *	@author CheWyl
	 *	12 November 2008
	 */

	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.MembersPanel.php';
	require_once 'travellog/views/member_staff/Class.ViewRegularStaff.php';

	class ViewSuperStaff{

		private $mPage				= 1;
		private $mTotalCount		= 0;
		private $mRowsPerPage		= 10;
		private $mGroup				= null;
		private $mLoggedUser		= null;
		private $arrSuperStaff		= array();

		function setPage($_page = 1){
			$this->mPage = $_page;
		}
		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setRowsPerPage($_rows = 10){
			$this->mRowsPerPage = $_rows;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}

		function removeAsStaff($_travelerId = 0){
			if ($this->mGroup){
				// get all subgroups of parent group
				$arrGroupId = array($this->mGroup->getGroupID());
				$arrSubGroup = $this->mGroup->getAllSubGroups();

				foreach($arrSubGroup as $eachGroup)
					$arrGroupId[] = $eachGroup->getGroupID();

				MembersPanel::removeAsParentStaff($arrGroupId, $_travelerId);
			}
		}

		function retrieve(){
			$mQueryString = "";
			$mRowsLimit = new RowsLimit($this->mRowsPerPage, ($this->mPage-1));

			// get all super staff
			$this->arrSuperStaff = $this->mGroup->getAllGroupStaff($mRowsLimit);
			// merge administrator to the superstaff/admin list
			$this->arrSuperStaff = array_merge(array($this->mGroup->getAdministrator()), $this->arrSuperStaff);
			$this->mTotalCount = $this->mGroup->getTotalRecords()+1;

			$mQueryString = "gID=" . $this->mGroup->getGroupID() . "&mode=2";
			$this->mPaging = new Paging( $this->mTotalCount, $this->mPage, $mQueryString, $this->mRowsPerPage );
			$this->mPaging->setScriptName('/members.php');
			$this->mPaging->setOnclick('MembersManager.searchSuperStaff');
		}

		function render(){
			$tpl = new Template();

			$tpl->set("d",				new GaDateTime);
			$tpl->set("paging",			$this->mPaging);
			$tpl->set("totalCount",		$this->mTotalCount);
			$tpl->set("groupName",		$this->mGroup->getName());
			$tpl->set("groupId",		$this->mGroup->getGroupID());
			$tpl->set("loggedUser",		$this->mLoggedUser);
			$tpl->set("superStaff",		$this->arrSuperStaff);
			$tpl->set("administratorID",	$this->mGroup->getAdministratorID());
			$tpl->out("travellog/views/member_staff/tpl.ViewSuperStaff.php");
		}
	}
?>