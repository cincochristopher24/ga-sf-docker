<?php
	/*
	 *	@author CheWyl
	 *	12 November 2008
	 */

	require_once 'Class.Template.php';

	class ViewRegularStaff{

		private $mPage				= 1;
		private $mSubGroupId		= 0;
		private $mTotalCount		= 0;
		private $mRowsPerPage		= 10;
		private $mGroup				= null;
		private $mSubGroup			= null;
		private $mLoggedUser		= null;
		private $arrRegularStaff	= array();
		private $isRemove			= false;
		private $lstAssignedGroup	= '';

		function setPage($_page = 1){
			$this->mPage = $_page;
		}
		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setSubGroup($_subGroup = null){
			$this->mSubGroup = $_subGroup;
		}
		function setSubGroupId($_subGroupId = 0){
			$this->mSubGroupId = $_subGroupId;
		}
		function setRowsPerPage($_rows = 10){
			$this->mRowsPerPage = $_rows;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}

		function removeAsStaff($_travelerId = 0){
			if ($this->mSubGroup)
				$this->mSubGroup->removeStaff($_travelerId);
			$this->isRemove = true;
		}

		function retrieve(){
			$mQueryString = "";
			$mRowsLimit = new RowsLimit($this->mRowsPerPage, ($this->mPage-1));

			// get all staffed sub groups
			$this->arrSubGroups = $this->mGroup->getStaffedSubGroups();

			// if sub group is not defined/set
			if ( !$this->mSubGroup )
			{
				// get all regular staff
				$tempAllRegularStaff = $this->mGroup->getStaffofSubGroups();
				if ( 0 < $tempAllRegularStaff )
				{
					foreach ( $tempAllRegularStaff as $regStaff )
					{
						if ( !$regStaff->isGroupStaff($this->mGroup->getGroupID()) )
						{
							$arrTemp = array();
							$arrTemp['traveler'] = $regStaff;

							$arrGroup = array();
							$tempStaffedSubGroup = Traveler::getAllStaffedSubGroups( $regStaff->getTravelerID(), $this->mGroup->getGroupID() );
							foreach ( $tempStaffedSubGroup as $group )
							{
								$arrGroup[] = $group->getName();
							}
							$arrTemp['groups'] = implode(", ", $arrGroup);

							$this->arrRegularStaff[] = $arrTemp;
						}
					}
				}

				
			}
			else
			{
				$tempStaffedSubGroup = array();

				if (0 < count($this->arrSubGroups)){
					if ( !is_null($this->mSubGroup) )
					{
						// get all regular staff
						$tempStaffedSubGroup = $this->mSubGroup->getAllGroupStaff($mRowsLimit, true);

						if ( $this->isRemove && (0 == count($tempStaffedSubGroup)) )
						{
							$this->mSubGroup = $this->arrSubGroups[0];
						}
					}
					else
					{
						// get subgroup in the select box
						$mSubGroupId = (0 < $this->mSubGroupId) ? $this->mSubGroupId : $this->arrSubGroups[0]->getGroupID();
						$mSubGroup = GroupFactory::instance()->create( array($mSubGroupId) );
						$this->mSubGroup = $mSubGroup[0];
					}

					// get all regular staff
					$tempStaffedSubGroup = $this->mSubGroup->getAllGroupStaff($mRowsLimit, true);

					foreach ( $tempStaffedSubGroup as $regStaff )
					{
						$arrTemp = array();
						$arrTemp['traveler'] = $regStaff;
						$arrTemp['groups'] = $this->mSubGroup->getName();

						$this->arrRegularStaff[] = $arrTemp;
					}

					$this->mTotalCount = $this->mSubGroup->getTotalRecords();
					$mQueryString = "gID=" . $this->mGroup->getGroupID() . "&mode=2&cID=" . $this->mSubGroup->getGroupID();
				}
			}

			$this->mPaging = new Paging( $this->mTotalCount, $this->mPage, $mQueryString, $this->mRowsPerPage );
			$this->mPaging->setScriptName('/members.php');
			$this->mPaging->setOnclick('MembersManager.searchStaff');
		}

		function render(){
			$tpl = new Template();

			$tpl->set("d",					new GaDateTime);
			$tpl->set("paging",				$this->mPaging);
			$tpl->set("totalCount",			$this->mTotalCount);
			$tpl->set("arrGroups",			$this->arrSubGroups);
			$tpl->set("arrRegularStaff",	$this->arrRegularStaff);
			$tpl->set("groupId",			$this->mGroup->getGroupID());
			$tpl->set("groupname",			$this->mGroup->getName());
			$tpl->set("loggedUser",			$this->mLoggedUser);
			$tpl->set("subGroupId",			(!is_null($this->mSubGroup))?$this->mSubGroup->getGroupID():0);
			$tpl->out("travellog/views/member_staff/tpl.ViewRegularStaff.php");
		}
	}
?>