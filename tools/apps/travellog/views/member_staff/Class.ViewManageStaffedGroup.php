<?php
	/*
	 *	@author CheWyl
	 *	22 December 2008
	 */

	class ViewManageStaffedGroup{
		private $mGroup				= null;
		private $mTraveler			= null;
		private $mChosenGroup		= null;
		private $mIsSuperStaff		= false;
		private $arrAllSubGroups		= array();
		private $arrCheckedGroups		= array();
		private $arrStaffedSubGroups	= array();

		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setTraveler($_traveler = null){
			$this->mTraveler = $_traveler;
		}
		function setChosenGroup($_group = null){
			$this->mChosenGroup = $_group;
		}
		function setArrSubGroup($_arrSubgroup = array()){
			$this->arrCheckedGroups = $_arrSubgroup;
		}		
		function setIsSuperStaff($_isSuperStaff = false){
			$this->mIsSuperStaff = $_isSuperStaff;
		}

		function manage(){
			$arrGroupId		= array();
			$arrStaffedSG	= array();
			$arrNewGroupId	= array();

			// get staffed groups of traveler
			$this->arrStaffedSubGroups = Traveler::getAllStaffedSubGroups($this->mTraveler->getTravelerID(), $this->mGroup->getGroupID());

			// remove staffed subgroups that are not checked
			foreach ($this->arrStaffedSubGroups as $subgroup){
				$arrGroupId[] = $subgroup->getGroupID();
				if (!in_array($subgroup->getGroupID(), $this->arrCheckedGroups))
					$subgroup->removeStaff($this->mTraveler->getTravelerID());
			}
			unset($subgroup);

			// add checked subgroups that are not yet staffed
			foreach ($this->arrCheckedGroups as $id){
				if (!in_array($id, $arrGroupId))
					$arrNewGroupId[] = $id;
			}
			$arrNewGroup = GroupFactory::instance()->create($arrNewGroupId);

			// add as staff and send email notice
			foreach($arrNewGroup as $newGroup){
				$newGroup->addStaff($this->mTraveler->getTravelerID());
				
				//add this traveler as member of this group
				$newGroup->addMember($this->mTraveler);
				
				/****** EMAIL Notification *******/
					require_once 'travellog/model/Class.StaffEmailNotification.php';

					$mEmail = new StaffEmailNotification;
					$mEmail->setGroup($newGroup);
					$mEmail->setTraveler($this->mTraveler);

					$default = "The administrator of the group " . $this->mGroup->getName() .
							" has invited you to be a staff of their group " . $newGroup->getName() . ".";

					$mEmail->setMessage($default);
					$mEmail->retrieve();
					$mEmail->Send();
				/****** EMAIL Notification *******/
			}
		}

		function retrieve(){
			if ($this->mGroup && $this->mGroup->isParent()){
				// get all subgroups
				$this->arrAllSubGroups = $this->mGroup->getAllSubGroups();

				// get staffed groups of traveler
				$this->arrStaffedSubGroups = Traveler::getAllStaffedSubGroups($this->mTraveler->getTravelerID(), $this->mGroup->getGroupID());
			}
		}
		function render(){
			$mTemplate = new Template();
			
			$mTemplate->set("group",			$this->mGroup);
			$mTemplate->set("traveler",			$this->mTraveler);
			$mTemplate->set("chosenGroup",		$this->mChosenGroup);
			$mTemplate->set("isSuperStaff",		$this->mIsSuperStaff);
			$mTemplate->set("arrSubGroups",		$this->arrAllSubGroups);
			$mTemplate->set("arrStaffedSGs",	$this->arrStaffedSubGroups);

			$mTemplate->out("travellog/views/member_staff/tpl.ViewManageStaffedGroup.php");
		}
	}
?>