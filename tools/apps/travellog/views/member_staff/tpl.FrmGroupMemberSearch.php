<? if( $isAdminGroup ): ?>
	<div class="section" id="default_members_1">	
			<h2>
				<span><?=stripslashes($grpName)?></span>
				<span style="color:#ccc; text-transform: none; margin-left: 1em"><?=$grpType?></span>		
			</h2> 
			<? 
				$totalMembersCount = $grpMemberCount;
				if( 3 > $viewerType || $isSubGroup || (!$isSubGroup && 3 <= $viewerType) ){
					$grpMemberCount = $grpMemberCount - ($grpStaffCount+$grpSuperStaffCount);
				}
				
				$member_label = "Member";
				if( 1 < $grpMemberCount ){
					$member_label = "Members";
				}else if( 1 > $grpMemberCount ){
					$member_label = "";
				}
				if( 3 > $viewerType ){
					$sstaff_label = "Admin";
					$staff_label = "Staff";
				}else{
					$sstaff_label = "Staff";
					$staff_label = $sstaff_label;
				}
				$grp_label = "Subgroup";
				if( 1 < $grpSubGroupCount ){
					$grp_label = "Subgroups";
				}
				$prog_label = "Project/Program";
				if( 1 < $grpProjectCount ){
					$prog_label = "Projects/Programs";
				}
			?>
			<div class="section_small_details">
				<? 	if( 3 > $viewerType || (4 > $viewerType && $isSubGroup) ): ?>
					You currently have 
				<?	else: ?>
					<?	if( $isSubGroup ): ?>
						<?	if( 1 == $grpMemberCount && 1 > $grpStaffCount
							 	|| 1 > $grpMemberCount && 1 == $grpStaffCount ): ?>
							There is currently
						<?	else: ?>
							There are currently 
						<?	endif; ?>
					<?	else: ?>
						<?	if( 1 == $grpMemberCount && 1 > $grpSuperStaffCount && 1 > $grpStaffCount && 1 > $grpProjectCount+$grpSubGroupCount
							 	|| 1 > $grpMemberCount && 1 == $grpSuperStaffCount && 1 > $grpStaffCount && 1 > $grpProjectCount+$grpSubGroupCount 
								|| 1 > $grpMemberCount && 1 > $grpSuperStaffCount && 1 == $grpStaffCount && 1 > $grpProjectCount+$grpSubGroupCount 
								|| 1 > $grpMemberCount && 1 > $grpSuperStaffCount && 1 > $grpStaffCount && 1 == $grpProjectCount+$grpSubGroupCount ): ?>
							There is currently
						<?	else: ?>
							There are currently 
						<?	endif; ?>
					<?	endif; ?>
				<? endif; ?>
				<?php if( 1 > $grpMemberCount && 1 > ($grpStaffCount+$grpSuperStaffCount) ): ?>
					<strong>No Members</strong>
				<?php elseif( 0 < $grpMemberCount ): ?>
					<strong><?=$grpMemberCount?></strong>
				<?php endif; ?>
				<?	if( 3 > $viewerType || (4 > $viewerType && $isSubGroup) ): ?> 
					<?	if( 0 < $grpSuperStaffCount && 0 < $grpStaffCount ): ?>
						<?	if( !$isSubGroup && 0 < $grpProjectCount+$grpSubGroupCount ): ?>
							<?=$member_label?><?if(""!=$member_label):?>, <?endif;?><?=$grpSuperStaffCount?> Admin, <?=$grpStaffCount?> Staff
						<?	else: ?>
							<?=$member_label?><?if(""!=$member_label):?>, <?endif;?><?=$grpSuperStaffCount?> Admin and <?=$grpStaffCount?> Staff
						<?	endif; ?>
					<?	elseif( 0 < $grpSuperStaffCount ): ?>
						<?	if( !$isSubGroup && 0 < $grpProjectCount+$grpSubGroupCount ): ?>
							<?=$member_label?><?if(""!=$member_label):?>, <?endif;?><?=$grpSuperStaffCount?> Admin
						<?	else: ?>
							<?=$member_label?><?if(""!=$member_label):?> and <?endif;?><?=$grpSuperStaffCount?> Admin
						<?	endif; ?>
					<?	elseif( 0 < $grpStaffCount ): ?>
						<?	if( !$isSubGroup && 0 < $grpProjectCount+$grpSubGroupCount ): ?>
							<?=$member_label?><?if(""!=$member_label):?>, <?endif;?><?=$grpStaffCount?> Staff
						<?	else: ?>
							<?=$member_label?><?if(""!=$member_label):?> and <?endif;?><?=$grpStaffCount?> Staff
						<?	endif; ?>
					<?	elseif( 0 < $grpMemberCount ): ?>
						<?=$member_label?>
					<?	endif; ?>
				<?	else: ?>
					<?	if( 0 < $grpStaffCount ): ?>
						<?	if( 0 < $grpProjectCount+$grpSubGroupCount ): ?>
							<?=$member_label?><?if(""!=$member_label):?>, <?endif;?><?=$grpStaffCount?> Staff
						<?	else: ?>
							<?=$member_label?><?if(""!=$member_label):?> and <?endif;?><?=$grpStaffCount?> Staff
						<?	endif; ?>
					<?	elseif( 0 < $grpMemberCount ): ?>
						<?=$member_label?>
					<?	endif; ?>
				<?	endif; ?>
				<? if( !$isSubGroup && 0 < $grpProjectCount+$grpSubGroupCount ): ?>
					<?	if ( 0 < $grpProjectCount && 0 < $grpSubGroupCount ) : ?>
						,<strong> <?=$grpSubGroupCount?></strong> <?=$grp_label?> and
						<strong> <?=$grpProjectCount?></strong> <?=$prog_label?>
					<?	elseif( 0 < $grpProjectCount ): ?>
						and <strong> <?=$grpProjectCount?></strong> <?=$prog_label?>
					<?	elseif( 0 < $grpSubGroupCount ): ?>
						and <strong> <?=$grpSubGroupCount?></strong> <?=$grp_label?>
					<? endif; ?>
				<? endif; ?>
			</div>
			<!-- END section_small_details -->							

			<div class="content" style="overflow: auto;">	
				<? if( 0 < ($grpProjectCount + $grpSubGroupCount) ): ?>
					<div id="search_box" class="search_box">
						<form onsubmit="gmsManager.searchMembers(1); return false;">
							<ul class="search_box_list">
								<li>
									<strong> Search. Find <? if( 3 > $viewerType || (4 > $viewerType && $isSubGroup) ): ?>your<? endif; ?> Members</strong>
									<p class="find_text">
										Type in a name or email to search for a <?=stripslashes($grpName)?> member. Search results are shown below. <br />
									</p><br />																						
								</li>
								<li class="emphasize_form"> 
									<label for="keyword"> Member <span> (Name, Username, or Email)</span> </label>
									<input name="keyword" id="keyword_search" type="text" />											
								</li>
								<li class="emphasize_form">
									<label for="scope"> Search in</label>									
									<select name="#" id="scope_search" name="scope" onchange="gmsManager.toggleAffiliation(this.selectedIndex);">
										<option value="<?=$grpID?>" selected="selected"> Main Group </option>
										<? if( 0 < $grpSubGroupCount ): ?>
										<option value="-1"> Groups </option>
										<? endif; ?>
										<? if( 0 < $grpProjectCount ): ?>
										<option value="-2"> Projects/Programs </option>
										<? endif; ?>																														
									</select>
								</li>
								<? if( 0 < $grpSubGroupCount ): ?>
									<li class="emphasize_form" >
										<span id="group_combo" >
											<label for="#"> Select Group</label>									
											<select name="grpSubGroupID" id="groupfilter_search" disabled="true">
												<option value="0" selected="selected"> -- Select Group -- </option>
											<? foreach( $grpSubGroups AS $subGroup) {?>
												<option value="<?=$subGroup->getGroupID()?>"> <?=stripslashes($subGroup->getName())?> </option>
											<? }unset($subGroup); ?>					
											</select>
										</span>																						
									</li>
								<? endif; ?>
								<? if( 0 < $grpProjectCount ): ?>
									<li class="emphasize_form" >
										<span id="program_combo" style="<?=(0<$grpSubGroupCount)? 'display:none;' : '' ?>">
											<label for="#"> In Project/Program:</label>
											<select name="grpProjectID" id="programfilter_search" disabled="true">
												<option value="0" selected="selected"> -- Select Project/Program -- </option>
											<? foreach( $grpProjects AS $project ) {?>
												<option value="<?=$project->getGroupID()?>"> <?=stripslashes($project->getName())?> </option>
											<? }unset($project); ?>							
											</select>
										</span>												
									</li>
								<? endif; ?>
								<li class="emphasize_form" id="submit_box">
									<img id="loading_search" src="/images/loading_small.gif" style="display:none;"/>&nbsp;
									<input id="value" type="submit" name="Search" value="Search" />
								</li>
							</ul>

							<div id="details_text">
								<? if( 4 > $viewerType ): ?>
									<strong> More Options</strong>
									<p>
										Click on a link below to show the members or groups that you would like to manage. Search results are shown below.											
									</p>
								<? else: ?>
									<strong> More Options</strong>
									<p>
										Click on a link below to show the members or groups that you would like to view. Results are shown below.										
									</p>
								<? endif; ?>
								<br />
								<a href="javascript:void(0)" onclick="gmsManager.displayAllGroupsAndProjects(); return false;"> Show All Groups/ Programs Instead</a>
								<br />
								<? if( 3 > $viewerType ): ?>
									<a href="javascript:void(0)" onclick="gmsManager.showUnassignedMembers(); return false;"> Show Unassigned Members</a>
									<br />
									<a href="javascript:void(0)" onclick="gmsManager.showNewMembers(); return false;"> Show New Members </a>																													
									<br />
									<a href="javascript:void(0)" onclick="gmsManager.showMainGroupMembers(); return false;"> Show Members of the Main Group</a>										
									<br />												
									<br />									
									<!--a class="recent_act" href="javascript:void(0)" onclick="gmsManager.showRecentActivity(); return false;"> Show Recent Member Activity</a-->								
								<? else: ?>
									<a href="javascript:void(0)" onclick="gmsManager.showMainGroupMembers(); return false;"> Show Members of the Main Group</a>										
									<br />
								<? endif; ?>
							</div><!-- END #details_text -->
						</form>			
					</div>
					<!-- END search_box -->
				<? else: ?>
					<div id="search_box" class="search_box">
						<form onsubmit="gmsManager.searchMembers(2); return false;" >
							<ul class="search_box_list">
								<li class="emphasize_form">
									<label for="#"> Member <span> (Name, Username, or Email)</span> </label>
									<input id="keyword_search" name="memberName" type="textbox" value="<?=stripslashes($username)?>" />
									<input id="value" type="submit" name="Search" value="Search" />
									<img id="loading_search" src="/images/loading_small.gif" style="display:none;"/>
								</li>
							</ul>
							<div id="details_text">
								<strong> Search. Find Members</strong>
								<? if( 4 > $viewerType ): ?>
									<p>
										Click on a link below to show the members that you would like to manage. Or use the box at the left to specify the name of the member you want to find. Search results are shown below.											
									</p>
									<br />																		
									<!--a href="javascript:void(0)" onclick="gmsManager.showRecentActivity(); return false;"> Show Recent Member Activity</a-->	
									<br />										
									<a href="javascript:void(0)" onclick="gmsManager.showNewMembers(); return false;"> Show New Members </a>																			
								<? else: ?>
									<p>
										Use the box at the left to specify the name of the member you want to find. Search results are shown below.											
									</p>
								<? endif; ?>								
							</div><!-- END #details_text -->
						</form>
					</div>
				<? endif; ?>					
			</div>
			<!-- END Content -->
		
			<div class="foot"></div>																						
	</div>
<? else: ?>
	<div class="section" id="default_members_1">	
		<h2>
			<span><?=stripslashes($group->getName())?></span>
			<span style="color:#ccc; text-transform: none; margin-left: 1em">CLUB</span>
	
			<!-- <span class="header_actions">
				<div style="clear:both">
					<a href="/photoalbum.php?action=view&groupID=245">Manage Album</a> 
				</div>
			</span>	 -->			
		</h2>
		<div class="section_small_details">
			<? if( 4 > $viewerType ): ?>You currently have <? else: ?>Currently has <? endif; ?>
			<strong><?=$grpMemberCount?></strong> Members
		</div>
			<div class="content" style="overflow: auto;">	
				<div id="search_box" class="search_box">
					<form onsubmit="gmsManager.searchMembers(2); return false;">
						<ul class="search_box_list">
							<li class="emphasize_form">
								<label for="#"> Member <span> (Name, Username, or Email)</span> </label>
								<input id="keyword_search" name="memberName" type="textbox" value="<?=stripslashes($username)?>"/>
								<input id="value" type="submit" name="Search" value="Search" />
								<img id="loading_search" src="/images/loading_small.gif" style="display:none;"/>
							</li>
						</ul>
						<div id="details_text">
							<? if( 4 > $viewerType ): ?>
								<strong> Manage members</strong>
								<p>
									Click on a link below to show the members that you would like to manage. Or use the box at the left to specify the name of the member you want to find. Search results are shown below.											
								</p>
								<br />																		
								<!--a href="javascript:void(0)" onclick="gmsManager.showRecentActivity(); return false;"> Show Recent Member Activity</a-->	
								<br />										
								<a href="javascript:void(0)" onclick="gmsManager.showNewMembers(); return false;"> Show New Members </a>																																				
							<? else: ?>
								<strong> Search. Find members</strong>
								<p>
									Use the box at the left to specify the name of the member you want to find. Search results are shown below.											
								</p>
							<? endif; ?>
						</div><!-- END #details_text -->
					</form>
				</div>					
			</div>
			<!-- END Content -->
			<div class="foot"></div>																						
	</div>
<? endif; ?>
<div id="default_content">
	<?=$controller->performAction()?>
</div>