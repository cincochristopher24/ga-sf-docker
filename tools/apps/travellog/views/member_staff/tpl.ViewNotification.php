<?php

	if (1 == count($arrStaff)){
		$mDetail = $arrStaff[0];

		if (isset($mDetail['traveler'])){
			$tID	= $mDetail['traveler']->getTravelerID();
			$cID	= $mDetail['subgroupId'];
			$param	= "gID=" . $groupId . "&mode=4&cID=" . $cID . "&tID=" . $tID;
		}
		elseif (isset($mDetail['email'])){
			$cID	= $mDetail['subgroupId'];
			$param	= "gID=" . $groupId . "&mode=4&cID=" . $cID . "&email=" . $mDetail['email'];
		}
	}
	else
		$param = "gID=" . $groupId . "&mode=4";
?>

<div id = "view_message" class = "message_preview" style = "display: block"><?=$preview_message?></div>
<div id ="edit_message" style = "display: none">
	<textarea name = "msg_textarea" id = "msg_textarea"><?=preg_replace('/\<br(\s*)?\/?\>/i', "\n", $actual_message)?></textarea>
	<div id = "edit_msg_action">
		<input
			type	= "button"
			name	= "btnSaveEdit"
			id		= "btnSaveEdit"
			value	= "Save Edits"
			onclick	= "StaffManager.editMessage('<?=$param?>')" />
		<input type = "button" name = "btnCancelEdit" id = "btnCancelEdit" value = "Cancel Edit" onclick = "StaffManager.viewMessage()">
	</div>
</div>	
<input type = "hidden" name = "hdnMessage" id = "hdnMessage" value = "<?=$actual_message?>">