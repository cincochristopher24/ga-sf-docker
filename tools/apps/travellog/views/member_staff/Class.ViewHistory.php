<?php
/***
 * Created on 11 23, 08
 *
 * * @author 
 * Purpose: 
 */
 
 	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.MembersPanel.php';
	require_once 'travellog/factory/Class.FileFactory.php';
	
 	class ViewHistory {
		private $mGroupID	= 0;
		private $mMode		= null;
		private $mLoggedUser= null;
		
		const MEMBER_TYPE	= 1;
		const STAFF_TYPE	= 2;

		function setGroupId($_groupID = 0){
			$this->mGroupID = $_groupID;
		}
		
		function setMode($_mode = 1){
			$this->mMode = $_mode;
		}
		
		function setLoggedUser($_loggedUser = null){
			$this->mLoggedUser = $_loggedUser;
		}
		
		function getGroup(){
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('GroupFactory');
			$group = $file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($this->mGroupID));
			return $group[0];
		}
		
		function render() {
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('ViewStaffHistory',		array('path' => 'travellog/views/member_staff'));
			$file_factory->registerClass('ViewMemberHistory',		array('path' => 'travellog/views/member_staff'));
			$file_factory->registerClass('GroupMembershipUpdates', 	array('path' => 'travellog/model/updates'));
			$file_factory->registerClass('StaffUpdates', 			array('path' => 'travellog/model/updates'));
			$tpl = new Template;
			$tpl->set("groupId",$this->mGroupID);
			$tpl->set("mode",$this->mMode);
			
			if (in_array($this->mMode, array(2, 4))) {
				// call staff history
				$mHistory = $file_factory->getClass('StaffUpdates', array($this->getGroup()));
				$mHistory->prepareUpdates(
					array(
						'LOGGED_TRAVELER'	=> $this->mLoggedUser
					)
				);
				$tpl->set("type",self::STAFF_TYPE);
			}
			else {
				// call group membership updates
				$mHistory = $file_factory->getClass('GroupMembershipUpdates', array($this->getGroup()));
				$mHistory->prepareUpdates(
					array(
						'LOGGED_TRAVELER'	=> $this->mLoggedUser,
						'DENIED_INVITES_ONLY'	=> false
					)
				);
				$tpl->set("type",self::MEMBER_TYPE);
			}
			$tpl->set("history",$mHistory);
			$tpl->setTemplate("travellog/views/member_staff/tpl.IncViewMemberStaffHistory.php");
			return $tpl;
		}
 	}
?>