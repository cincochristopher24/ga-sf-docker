<? if (isset($superStaff)) : ?>
	<div id = "superstaff_group" class = "section">
		<h2>
			<span> Admin </span>
		</h2>
		<p id = "superstaff_LoadingIndicator" style = "display: none">
	        Please wait...
	    </p>
		<div id = "superstaff_maincontent">
			<?=$superStaff->render()?>
		</div>
	</div>
<? endif; ?>
<? if (isset($regularStaff)) : ?>
	<div id = "regularstaff_group" class = "section">
		<h2>
			<span>
				<?/* if (1 == $viewerType || 2 == $viewerType) : ?>
					Regular 
				<? endif */?>
				Staff
			</span> 
		</h2>
		<p id = "regularstaff_LoadingIndicator" style = "display: none">
	        Please wait...
	    </p>
		<div id = "regularstaff_maincontent">
			<?=$regularStaff->render()?>
		</div>
	</div>
<? endif; ?>
<? if (isset($allStaff)) : ?>
	<div id = "regularstaff_group" class = "section">
		<h2>
			<span>Staff</span> 
		</h2>
		<p id = "regularstaff_LoadingIndicator" style = "display: none">
	        Please wait...
	    </p>
		<div id = "regularstaff_maincontent">
			<?=$allStaff->renderAll()?>
		</div>
	</div>
<? endif; ?>
<? if (isset($allRegStaff)) : ?>
	<div id = "regularstaff_group" class = "section">
		<h2>
			<span><?=$groupName?></span>
			<span style = "color: #CCCCCC; margin-left: 1em; text-transform:none;">STAFF</span>
		</h2>
		<p id = "regularstaff_LoadingIndicator" style = "display: none">
	        Please wait...
	    </p>
		<div id = "regularstaff_maincontent">
			<?=$allRegStaff->render()?>
		</div>
	</div>
<? endif; ?>