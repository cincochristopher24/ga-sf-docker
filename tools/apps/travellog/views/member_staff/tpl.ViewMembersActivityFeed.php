<div id = "wide_column" class = "layout2">
	
	<div class="section" id="members_feed">
		<h2> Members Activity Feed</h2>
		<?if(!$showIndiFeed):?>
			<div class="section_small_details">
				Showing recent member activities.
				<input type="button" id="refreshBtn" value="Refresh Feed" onclick="window.location.reload();"/>
			</div>
			<div class="content">
				<ul class="users activity_feed">
					<!-- Start Feed -->
					<?foreach($feeds as $feed):?>
					<?	$doerType = $feed->getDoerType(); 
						$doerLabel = (UpdateDoerType::REGULAR_STAFF == $doerType || UpdateDoerType::SUPER_STAFF == $doerType)? 'Staff' : '';
					?>	<li class="feed">
							<a class="thumb" title = "<?=$feed->getDoer()->getName()?>" href = "<?=$feed->getDoer()->getUrl()?>"> 
								<img src = "<?=$feed->getDoer()->getProfilePicture()?>" width = "65" height = "65" alt = "Member Thumbnail"/>
							</a>						
							<div class="details">
								<?=$doerLabel?>
								<a class="username" href = "<?=$feed->getDoer()->getUrl()?>"><?=$feed->getDoer()->getName()?></a>
								<strong><?=$feed->getDoer()->getFullName()?></strong>
								<?=$feed->getAction()?>

								<div class="feed_timestamp"> <?=GaDateTime::descriptiveDifference($feed->getExecutionDate(), GaDateTime::dbDateTimeFormat())?> ago </div>
								<div class="feed_actions">
									<a href="<?=$feed->getContextLink()?>" >Go To <?=$feed->getContextName()?></a>
								</div>						
							</div>														
						</li>		
					<?endforeach;?>	
					<!-- End Feed -->																																						
				</ul>				
			</div>
		<?else:?>
			<div class="section_small_details">
				Showing <strong><?=$member->getUserName()?></strong>'s activity feed.
				<input type="button" id="refreshBtn" value="Refresh Feed" onclick="window.location.reload();"/>
			</div>
			<div class="content">
				<ul class="users member_list activity_feed">
					<!-- Start Member Info -->
					<? 
						$server = $_SERVER["SERVER_NAME"]; 
						$canView = $member->getPrivacyPreference()->canViewNames($member->getTravelerID());
						$isSuperStaff = $group->isSuperStaff($member->getTravelerID());
						if( $group->isParent() && $isSuperStaff ){
							$affiliations = array();
						}
					?>
					<li class="member first">
						<a class="thumb" href="http://<?=$server.'/'.$member->getUserName()?>"> 
							<img src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width="65" height="65" alt="<?=$member->getUserName()?>"/>
						</a>
						<div class="details">
						
							<a class="username" href="http://<?=$server.'/'.$member->getUserName()?>">
								<?=$member->getUserName()?>
								<? if( 4 > $viewerType || ( 3 < $viewerType && $canView ) ): ?>
									<span class="full_name"><?=$member->getFullName()?></span>
								<?endif;?>
							</a> 

							<div class="group_affiliations">
								<? if ( 0 < sizeof($affiliations) ): ?>
									<? if( $group->isParent() && !$isSuperStaff ): ?>
										<span> Group Affiliations: (<?=sizeof($affiliations)?>)</span>
										<ul id="affiliation_summary_<?=$member->getTravelerID()?>">
											<? for($j=0; $j<sizeof($affiliations) && 10 > $j; $j++ ){
													$grp_affiliation = $affiliations[$j];	
											?>
											<li> <a href="http://<?=$server.$grp_affiliation->getFriendlyURL()?>"><?=stripslashes($grp_affiliation->getName())?></a>
												<? if( $j < 9 && $j < sizeof($affiliations)-1 ): ?>
												,&nbsp;
												<? endif; ?>
												<? if( $j == 9 && 10 < sizeof($affiliations) ): ?>
												&nbsp;
												<i>(<a href="" onclick="gmsManager.hide('affiliation_summary_<?=$member->getTravelerID()?>'); gmsManager.show('affiliation_all_<?=$member->getTravelerID()?>'); return false;">
												more &darr; 
												</a>)</i>
												<? endif; ?>
											</li>

											<? } ?>													
										</ul>
										<? if( 10 < sizeof($affiliations) ): ?>
											<ul id="affiliation_all_<?=$member->getTravelerID()?>" style="display:none;">
												<? for($j=0; $j<sizeof($affiliations); $j++ ){
														$grp_affiliation = $affiliations[$j];	
												?>
												<li> <a href="http://<?=$server.$grp_affiliation->getFriendlyURL()?>"><?=stripslashes($grp_affiliation->getName())?></a>
													<? if( $j < sizeof($affiliations)-1 ): ?>
													,&nbsp;
													<? endif; ?>
													<? if( $j == sizeof($affiliations)-1 ): ?>
													&nbsp;
													<i>(<a href="" onclick="gmsManager.hide('affiliation_all_<?=$member->getTravelerID()?>'); gmsManager.show('affiliation_summary_<?=$member->getTravelerID()?>'); return false;">
													hide &uarr;
													</a>)</i>
													<? endif; ?>
												</li>
												<? } ?>
											</ul>
										<? endif; ?>
									<? endif; ?>
								<? else: ?>
									<? if( $group->isParent() && !$isSuperStaff ): ?>
										<span> Group Affiliations other than the Main Group: </span>
										<ul id="affiliation_summary_<?=$member->getTravelerID()?>">
											<li> None </li>
										</ul>
									<? endif; ?>
									<? if( $group->isParent() && $isSuperStaff ): ?>
										<span> Group Affiliations: All groups</span>
									<? endif; ?>
								<? endif; ?>
									<!--ul id="affiliation_summary_7561">
										<li> <a href="http://goabroad.net.local/groups/ProWorld-Service-Corps/subgroup/Peru-Cal-Poly-University">Peru Cal Poly University</a>,</li>
										<li> <a href="http://goabroad.net.local/groups/ProWorld-Service-Corps/subgroup/Belize-August-08-Interns">Belize August 08 Interns</a>,</li>				
										<li> <a href="http://goabroad.net.local/groups/ProWorld-Service-Corps/subgroup/Belize-_-Lily-Martinez">Belize - Lily Martinez</a></li>																								
									</ul-->
							</div>

							<div class="actions">
								<?	if( $group->isParent() ): ?>
									<? if( (0 < sizeof($affiliations) || !$group->isParent()) && !$isSuperStaff ): ?>
										<a href="javascript:void(0)" onclick="gmsManager.getGroupsToManage(<?=$member->getTravelerID()?>); return false;">Manage Group Affiliations</a> | 
									<? else:  ?>
										<? if( !$isSuperStaff ): ?>
											<a href="javascript:void(0)" onclick="gmsManager.getAssignToGroups(<?=$member->getTravelerID()?>); return false;">Assign to Group</a> | 
										<? endif; ?>
									<? endif; ?> 
								<?	endif; ?>
								<? if( !$loggedUser->isBlocked($member->getTravelerID()) ): ?>
									<a href="http://<?=$server?>/messages.php?act=messageTraveler&id=<?=$member->getTravelerID()?>" >Send Message &rarr;</a>
								<? endif; ?>
								<? if( 1 == $viewerType || ( 2 == $viewerType && !$isSuperStaff ) || ( !$isSuperStaff && !$group->isParent() && $group->isStaff($loggedUser->getTravelerID()) ) ): ?>
									| <a class="negative_action" href="javascript:void(0)" onclick="gmsManager.removeMember(<?=$member->getTravelerID()?>,'<?=$member->getFullName()?>','<?=str_replace("'","&rsquot;",$group->getName())?>'); return false;">Remove Membership</a>
								<? endif; ?>
							</div>

															
						</div>														
					</li>			
					<!-- End Member Info -->
					
					<!-- Start Feed -->
					<?foreach($feeds as $feed):?>
						<li class="feed individual_member">
											
							<div class="details">
						
								<?=$feed->getAction()?>
								<div class="feed_timestamp"> <?=GaDateTime::descriptiveDifference($feed->getExecutionDate(), GaDateTime::dbDateTimeFormat())?> ago </div>
							</div>														
						</li>
					<?endforeach;?>				
					<!-- End Feed -->
				</ul>
			</div>		
		<?endif;?>
		<?if(!count($feeds)):?>
			<div class="help_text">No member activity found. Check back later.</div>
		<?endif;?>
	<!-- members feed -->
	</div>
</div>
<!-- end wide column -->