<ul id = "pending_members_list" class = "users member_list">
	<? 	$isFirst = TRUE;
		foreach( $nonRegisteredInvites AS $email ) :
			try{
				$params = array("email"	=>	$email,
								"filterCriteria2"	=>	$filter);
				$emailInvites = EmailInviteList::retrieve($params);
			}catch(exception $e){
				continue;
			}
			if( 0 == sizeof($emailInvites) ){
				continue;
			}
			$inviting_groups = array();
			foreach( $emailInvites AS $invite ){
				try{
					$temp = new AdminGroup($invite->get("groupID"));
					if( $temp instanceof AdminGroup ){
						$inviting_groups[] = $temp;
					}
				}catch(exception $e){
				}
			}
			unset($invite);
			$class = ($isFirst)? "nonregistered first" : "nonregistered";
			$isFirst = FALSE;
			$owner = $emailInvites[0];
			$server = $_SERVER["SERVER_NAME"];
	?>
		<li class = "<?=$class?>">
			<a class="thumb" href="" onclick="return false;"> 
				<img class="pic" src="/images/default_images/user_thumb.gif" alt="thumbnail"/>
			</a>
		
			<div class="details overflow">
				<span class="email_nonregistered"><?=$owner->get('email')?>
					<span class="full_name"> <?=stripslashes($owner->get('firstname'))." ".stripslashes($owner->get('lastname'))?></span>
				</span>									
				<? if( 2 == $group->getDiscriminator() && !$group->isSubGroup() ): ?>
					<div class = "pending_alt_action">
						<a href = "http://<?=$server?>/members.php?gID=<?=$group->getGroupID()?>&mode=3&action=3&email=<?=$email?>" >Invite to a different Group &rarr;</a>
					</div>
				<? endif; ?>
				
				<? foreach($inviting_groups AS $ig): ?>
				
					<div class = "pending_on_group_list">
						<div class="pending_on_group">
							<?
								if (strlen(stripslashes($ig->getName())) > 50)
									echo substr(stripslashes($ig->getName()), 0, 50) . '...';
								else
									echo stripslashes($ig->getName());
							?>
						</div>
						<? if( $viewerType < 4 ): ?>
							<span>
								<a href = "javascript:void(0)" onclick = "piManager.resendNonRegisteredInvitation('<?=$owner->get('email')?>',<?=$ig->getGroupID()?>,'<?=str_replace("'","&rsquot;",$ig->getName())?>'); return false;">Resend Invitation</a> |
							</span>
							<span>
								<a class = "negative_action" href = "javascript:void(0)" onclick = "piManager.cancelNonRegisteredInvitation('<?=$owner->get('email')?>',<?=$ig->getGroupID()?>,'<?=str_replace("'","&rsquot;",$ig->getName())?>'); return false;">Cancel Invitation</a>
							</span>
						<? endif; ?>
					</div>
				
				<? endforeach; ?>																						
			</div>
			<!-- END details overflow -->
		</li>
	<? endforeach; ?>
</ul>

<img id="loading_page_nonregistered" src="/images/loading_small.gif" style="display:none;"/>&nbsp;
<? if ( $paging->getTotalPages() > 1 ):?>
	<? $start = $paging->getStartRow() + 1; 
	   $end = $paging->getEndRow(); 
	?>
	<? $paging->showPagination(); ?>
<? endif; ?>