<?php

	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");
	
	class EmailInviteList{
		
		private $mFields	=	array(	"groupID"	=>	NULL,
										"email"		=>	NULL,
										"emailInvitationMessageID"	=>	NULL,
										"firstname"	=>	NULL,
										"lastname"	=>	NULL );
		
		public function __construct($data=array()){
			$this->mFields = array_merge($this->mFields,$data);
		}

		public function set($field,$val){
			$this->mFields[$field] = $val;
		}

		public function get($field){
			return $this->mFields[$field];
		}

		public static function retrieve($params=array()){
			try{
				$db = new dbHandler();
				
				$default = array(	"groupID"	=>	NULL,
									"email"		=>	NULL,
									"emailInvitationMessageID"	=>	NULL,
									"firstname"	=>	NULL,
									"lastname"	=>	NULL,
									"filterCriteria2" 	=>	NULL,);
									
				$params = array_merge($default,$params);
				
				$where_expression = "";
				
				if( !is_null($params["groupID"]) ){
					$where_expression .= " AND `tblEmailInviteList`.`groupID` = ".$db->makeSqlSafeString($params["groupID"]);
				}
				
				if( !is_null($params["email"]) ){
					$where_expression .= " AND `tblEmailInviteList`.`email` = ".$db->makeSqlSafeString($params["email"]);
				}
				
				if ( !is_null($params["filterCriteria2"]) ){

					$filterCondition = $params["filterCriteria2"]-> getConditions();
					$attribute = $filterCondition[0]-> getAttributeName();
					$value     = $filterCondition[0]-> getValue();

					switch($filterCondition[0]->getOperation()){

	    				case FilterOp::$EQUAL:
	    					$operator	= " = ";
	    					break;
	    				case FilterOp::$NOT_EQUAL:
	    					$operator   = " <> ";
	    					break;
						case FilterOp::$IN:			//added bu Jul, 2008-11-19, in this case, the list value must have already been placed inside parenthesis
							$operator	= " IN ";
							break;
	    				default:
	    					$operator   = " = ";
	    					break;
	    			}

					$where_expression .= " AND " .$attribute . $operator . $value;
				}
				
				$sql = "SELECT `tblEmailInviteList`.*
						FROM `Travel_Logs`.`tblEmailInviteList`
						WHERE 1=1 $where_expression
						ORDER BY `tblEmailInviteList`.`lastname` DESC,
							`tblEmailInviteList`.`firstname` DESC";
				//echo "<p>".$sql."</p>";

				$rs = new iRecordset($db->execute($sql));
				$arr = array();
				foreach( $rs as $row ){
					$arr[] = new EmailInviteList($row);
				}
				return $arr;
			}catch(exception $e){
			}
		}
	}