<div id="all_groups_programs" class="section">
	<h2> <span> All Groups and Programs</span> </h2>
	
	<div class="section_small_details"> 
		<form onsubmit="gmsManager.updateAllGroupsAndProjects(); return false;" >
			<ul class="list_small_details">											
				<li>
					<label for="display_all_filter">Display </label>
					<select id="display_all_filter" name="display_all_filter">
						<option value="0" <?=( 0 == $displayMode)? 'selected="selected"' : ''?> >All</option>
						<option value="1" <?=( 1 == $displayMode)? 'selected="selected"' : ''?> >Groups</option>
						<option value="2" <?=( 2 == $displayMode)? 'selected="selected"' : ''?> >Programs</option>
					</select>
				</li>										

				<li>
					<label for="sort_by_filter">Sort By</label>
					<select id="sort_by_filter" name="sort_by_filter">
						<option value="0" <?=( 0 == $sortByFilter)? 'selected="selected"' : ''?> >Rank &uarr;</option>
						<option value="1" <?=( 1 == $sortByFilter)? 'selected="selected"' : ''?> >Rank &darr;</option>
						<option value="2" <?=( 2 == $sortByFilter)? 'selected="selected"' : ''?> >Name A to Z</option>
						<option value="3" <?=( 3 == $sortByFilter)? 'selected="selected"' : ''?> >Name Z to A</option>
					</select>								
				</li>
	
				<!--li>
					<label for="member_size_filter">Group Size</label>
					<select id="member_size_filter" name="member_size_filter">
						<option value="0">Ascending</option>
						<option value="1">Descending</option>
					</select>												
				</li-->
			
				<li>
					<input type="submit" name="refresh_list" value="Refresh List" id="refresh_list" />
				</li>
			
				<li>
					<img id="loading_content" src="/images/loading_small.gif" style="display:none;" />
				</li>										
			</ul>										
		</form>
	</div>									
	<!-- END section_small_details -->									
	
	<div class="content">
		
		<div id="main_group">
			<div class="listcategory_divider"> Main Group</div>
			<div class="group_name"> 
				<?=$group->getName()?> 
				<?php
					if( 3 > $viewerType || (3 <= $viewerType && !$isSubGroup) ){
						$grpMemberCount = $grpMemberCount - ($grpSuperStaffCount+$grpStaffCount);
					}
				?>
				<span class="member_size"> (<?=$grpMemberCount?> <?if(1<$grpMemberCount):?>Members<?else:?>Member<?endif;?>)</span> 
			</div>
			<? if( !is_null($loggedUser) && ( $group->getAdministratorID() == $loggedUser->getTravelerID() || $group->isSuperStaff($loggedUser->getTravelerID())) ): //|| $group->isRegularStaff($loggedUser->getTravelerID())) ): ?>
				<div class="actions">
					<a href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/messages.php?act=messageGroupStaff&gID=<?=$group->getGroupID()?>" > Message Staff &rarr; </a>
				</div>
			<? endif; ?>											
		</div>	
		<!-- END main group	 -->
		<div id="other_group_projects">
			<div class="listcategory_divider"> 
				<? switch($displayMode){
						case 0	:	$label = "OTHER GROUPS & PROJECTS/PROGRAMS "; break;
						case 1	:	$label = "OTHER GROUPS "; break;
						case 2	:	$label = "PROJECTS/PROGRAMS "; break;
						default	:	$label = "";
					}
				?>
			 <?=$label?> (<?=$totalrec?>)
			</div>
			<? echo $controller->renderSubGroupAndProjectList(); ?>
		</div>
		<!-- END other_group_projects -->
		
	</div>
	<!-- END content -->
	<div class="foot">
		
	</div>
	
</div><!-- END all_groups_programs -->
