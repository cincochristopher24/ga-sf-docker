<ul id="requesting_users_list" class="users member_list">
		<? $server = $_SERVER["SERVER_NAME"]; 
			for( $i=0; $i<sizeof($requesters); $i++){
			$traveler = $requesters[$i];
			$class = ( 0 == $i ) ? "member first" : "member";
			$groups_requested = $traveler->getGroupRequests($filter);
		?>
			<li class="<?=$class?>">
				<a class="thumb" href="http://<?=$server.'/'.$traveler->getUserName()?>"> 
					<img src="<?=$traveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width="65" height="65" alt="<?=$traveler->getUserName()?>"/>
				</a>
		
				<div class="details overflow">
					
						<a href="http://<?=$server.'/'.$traveler->getUserName()?>" class="username" title="<?=$traveler->getUserName()?>">
							<?=$traveler->getUserName()?>&nbsp;&nbsp;<span class="full_name"><?=stripslashes($traveler->getFullName())?></span>
						</a> 
																
					<div class="group_affiliation">
						<span> Is Requesting to Join: </span>
					</div>
					<? foreach($groups_requested AS $requested){ ?>
						<div class="group_affiliations">
							<ul>
								<li><strong><a href="http://<?=$server?><?=$requested->getFriendlyURL()?>"><?=stripslashes($requested->getName())?></a></strong></li>																																								
							</ul>
						</div>
						<!-- END group_affiliations -->
						<div class="actions">
							<a href="javascript:void(0)" onclick="jrManager.approveJoinRequest(<?=$traveler->getTravelerID()?>,'<?=str_replace("'","&rsquot;",$traveler->getFullName())?>',<?=$requested->getGroupID()?>,'<?=str_replace("'","&rsquot;",$requested->getName())?>'); return false;">Approve Request</a> |
							<? if( !$loggedUser->isBlocked($traveler->getTravelerID()) ): ?>
								<a href="http://<?=$server?>/messages.php?act=messageTraveler&id=<?=$traveler->getTravelerID()?>">Send Message &rarr;</a> |
							<? endif; ?>
							<a class="negative_action" href="javascript:void(0)" onclick="jrManager.denyJoinRequest(<?=$traveler->getTravelerID()?>,'<?=str_replace("'","&rsquot;",$traveler->getFullName())?>',<?=$requested->getGroupID()?>,'<?=str_replace("'","&rsquot;",$requested->getName())?>'); return false;">Deny Request</a>																												
						</div><!-- END actions -->
					<? } ?>																							
				</div>
				<!-- END details overflow -->
			</li>	
		<? } ?>																														
</ul>

<img id="loading_page" src="/images/loading_small.gif" style="display:none;"/>&nbsp;
<? if ( $paging->getTotalPages() > 1 ):?>
	<? $start = $paging->getStartRow() + 1; 
	   $end = $paging->getEndRow(); 
	?>
	<? $paging->showPagination() ?>
<? endif; ?>