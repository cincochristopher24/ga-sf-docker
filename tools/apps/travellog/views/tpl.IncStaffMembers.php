<div class="section" id="members_box" >
	<h2>
		<span>Staff</span>
			<span class="header_actions">
			  <? if ($isAdminLogged): /* can add only if the current logged user is the admin*/ ?>
				  <a href="/members.php?gID=<?=$grpID?>&mode=4" >Add Staff</a> |
				<? endif; ?>
				<a href="/members.php?gID=<?=$grpID?>&mode=2" ><?= $staffLinkLabel ?></a>
			</span>
	</h2>

	<div id="statusPanel1" class="widePanel" style="display: none;">
	  <p class="loading_message">
			<span id="imgLoading">
			  <img alt="Loading" src="/images/loading.gif"/>
			</span>
			
			<em id="statusCaption">Loading data please wait...</em>
		</p>
  </div>
		
	<div id="member_box_content" style="overflow:auto;" >
	  <?= $grpMembersBoxView->render(GroupMembersBoxView::$STAFF) ?>
	</div>	

</div>