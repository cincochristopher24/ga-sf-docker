<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
		<title><?echo $maintitle ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta name="author" content="Aldwin S. Sabornido" />
</head>
<body>
	<?echo $primaryphoto ?>
	<br />
	username: <?= $username ?>
	<br />
	<?
		echo $viewprofilelink. "&nbsp;&nbsp;&nbsp;" .$editprofilelink;
	?>
	<table border="1">
		<tr>
					<td colspan="5">
							<strong><?echo $travel->getTitle() ?></strong>&nbsp;<?echo  $editjournallink; ?><br />
							<?echo $addentrylink; ?>
							
					</td>
		</tr>
		<tr><td>Title</td><td>Summary</td><td>Date</td><td>Location</td><td>Actions</td></tr>
		<? $ctr = 0; ?>
		<? if (count($trips)): ?>
				<? foreach($trips as $trip): ?>
						<? $travellogs = $trip->getTravelLogs();
							 
							 if (count($travellogs)):
							 			 
							 foreach($travellogs as $travellog):
			    		?>
			    				 <?  $ctr+=1;  ?>
			    				 <? if ($ctr == 1):  ?>
							    			<tr>
													<td>
															<? 
																	if($travellog->getRandomPhoto())
																	{ 
																			echo '<img src="'.$travellog->getRandomPhoto()->getThumbnailPhotoLink(). '" /><br />';
																			echo 'caption:'.$travellog->getRandomPhoto()->getCaption().'<br>';
																	}
																	echo $travellog->getTitle(); 
															?>
													</td>
													<td><?= $travellog->getDescription(); ?></td>
													<td><?= $travellog->getLogDate(); ?></td>
													<td><?= $trip->getLocation()->getName(); ?></td>
													<td><a href="/travellog/admin/Photomanagement.php?action=add&travelerID=<?= $travellog->getTravellerID() ?>&cat=travellog&travellogID=<?= $travellog->getTravellogID() ?>">Add Photos</a>&nbsp;<a href="http://<?= $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].'?action=showedit&travellogID='.$travellog->getTravellogID(); ?>">Edit</a>&nbsp;<a href="http://<?= $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].'?action=delete&travellogID='.$travellog->getTravellogID(); ?>">Delete</a></td>
											</tr>
											<? if( count($travellog->getPhotos()) ): ?>
													<tr>
																<td colspan="5">
																			<? 
																					$photos = $travellog->getPhotos();
																					foreach($photos as $photo)
																					{
																							echo '<img src="'.$photo->getThumbnailPhotoLink(). '" />&nbsp;';	
																					}	  
																		   ?>
																			<br /><a href="/travellog/admin/">view all photos</a>
																</td>
													</tr>
											<? endif; ?>
								<? else: ?>
											<tr>
													<td colspan="2">
															<a href="/travellog/admin/Travellog.php/"><?= $travellog->getTitle(); ?></a> 
													</td>
													<td><?= $travellog->getDescription(); ?></td>
													<td><?= $travellog->getLogDate(); ?></td>
													<td><?= $trip->getLocation()->getName(); ?></td>
											</tr>
								<? endif; ?>	
						<? endforeach; endif; ?>
				<? endforeach; ?>
		<? endif; ?>
	</table>
	<?= $map->plotLocations($locationmarkers) ?>
	<?php include('travellog/views/Tpl.Footer.php'); ?>
</body>
</html>