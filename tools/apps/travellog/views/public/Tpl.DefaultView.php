<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?= $title ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="/travellog/css/adhoc.css" media="screen" />
		<!-- START - Google Analytics Tracker -->
		<script src  = "http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
		<script type    = "text/javascript">
			_uacct  = "UA-467553-10";
			urchinTracker();
		</script>
		<!-- END - Google Analytics Tracker -->
</head>
<body>

<?php include('travellog/views/Tpl.IncHeader.php'); ?>

			
<div id="wrapper1">
	<div id="wrapper2">
		<div id="maincol">
			<div id="centercol">
			<?= $content ?>
			</div>
		</div>
	</div>
</div>
<?php include('travellog/views/Tpl.Footer.php'); ?>
</body>
</html>
