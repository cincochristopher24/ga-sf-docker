<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Groups');
	$subNavigation->show();
?>

<div id="content_wrapper" style="padding-bottom: 10px">
	<div class="section" style="margin: 0">
		<h1><span>Group Membership Management</span></h1>
		<div class="content">
			<div class="actions">
				<div><?=$message ?></div>
				<a href="passport.php"><strong>My Passport</strong></a> |
				<a href="group.php?gID=<?=$grpID ?>"><strong>Proceed to <?= $grpName ?>'s HomePage</strong></a>
			</div>
		</div>
		<div class="foot"></div>
	</div>		
</div>