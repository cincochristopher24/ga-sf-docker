<?=$contents['contents']->render()?>
<div id="contentEditor" style="display:none">	
 <table class="table_popbox">
	<tbody>
		<tr>
			<td class="popbox_topLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_topRight"></td>
		</tr>
		<tr>
			<td class="popbox_border"></td>
			<td class="popbox_content">
				<h4 id="popup_header" class="header"><strong id="contentLabel"></strong></h4>
			  	<div id="popup_confirm" class="confirm">
					 <div class="popbox_help_text" style="display:none;"></div>
					 <textarea name="content" id="content" rows="14" cols="64"></textarea>
					 <span id="counter" style="display:none;"></span>
				</div>	
				<div id="popup_buttons" class="buttons_box">
					<input type="hidden" name="fieldname" id="fieldname" value="" class="prompt_button"/>
					<input type="button" name="save" id="save" value="Save" class="prompt_button"/>&nbsp;<input type="button" name="cancel" id="cancel" value="Cancel" class="prompt_button"/>
				</div>
			</td>
			<td class="popbox_border"></td>
		</tr>
		<tr>
			<td class="popbox_bottomLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_bottomRight"></td>
		</tr>
	</tbody>
 </table>
</div>