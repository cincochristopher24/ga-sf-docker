<?php 
	Template::includeDependentJs ("/js/thickbox.js", array('bottom'=> true));
	Template::includeDependentJs ( "/js/video.js", array('bottom'=> true));
?>
<table class="table_popbox">
	<tbody>
		<tr>
			<td class="popbox_topLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_topRight"></td>
		</tr>
		<tr>
			<td class="popbox_border"></td>
			<td class="popbox_content">
				<h4 class="header" id="popup_header"><strong>Add Video</strong></h4>
				<div class="confirm" id="popup_confirm">
					<div id = "loading_image" style = "display:none">
						<span> <center> <img src = "/images/loading.gif"/> </center> </span>
					</div>
				
					<div class = "formcont" id="formcont">
						<div style="" class="popbox_help_text">
							<p>
								To add a video, just enter the URL (e.g. http://www.youtube.com/watch?v=AOnYVeaETps), 
								and a title and caption for it, and then click <strong>Add</strong>.
							</p>
						</div>
						<div>	
							<div>
								URL:<span class = "required"> * </span>
							</div>
							<div>
								<div id = "urlError" style = "display: none" class = "required"> </div>
								<input type = "text" id = "txtVideoUrl" size = "40" value = "">
							</div>
						</div>
	
						<div>	
							<div>	
								Title: 
							</div>
							<div>
								<div id = "titleError" style = "display: none" class = "required"> Do not use + and # for video title. </div>
								<input type = "text" id = "txtVideoTitle" size = "40" value = ""> 
							</div>
						</div>
	
						<div>	
							<div>
								Caption: 
							</div>
							<div>
								<div id = "captionError" style = "display: none" class = "required"> Do not use +, \ and # for video caption. </div>
								<textarea id = "txaVideoCaption"></textarea>
							</div>
						</div>
					</div>
				</div>	
				<div class="buttons_box" id="popup_buttons">
					<?php if(isset($travelerID)): ?>
						<input type = "button" id = "btnSubmit" onClick = "Video.submit_form(0, 'journal', <?php echo $travelLogID ?>, <?php echo $travelID ?>, <?php echo $travelerID ?>, 'Traveler', '<?php echo $redirect ?>')" value = "Add" class="prompt_button"/>
					<?php else: ?>
						<input type = "button" id = "btnSubmit" onClick = "Video.submit_form(0, 'journal', <?php echo $travelLogID ?>, <?php echo $travelID ?>, <?php echo $groupID ?>, 'Group', '<?php echo $redirect ?>')" value = "Add" class="prompt_button"/>
					<?php endif; ?>	
					<input type = "button" id = "btnCancel" onClick = "Video.hideThickbox()" value = "Cancel" class="prompt_button">
				</div>
			</td>
			<td class="popbox_border"></td>
		</tr>
		<tr>
			<td class="popbox_bottomLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_bottomRight"></td>
		</tr>
	</tbody>
</table>