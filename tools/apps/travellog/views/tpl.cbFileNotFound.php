<?php
	$cobrandName    = $GLOBALS['CONFIG']->getSiteName();
	$cobrandAddress    = $GLOBALS['CONFIG']->getServerName();
	$cobrandLogo = 'custom/'.$GLOBALS['CONFIG']->getFolderName().'/images/white_logo.gif';
	$logoSrc 	 = is_file($cobrandLogo) ? $cobrandLogo : 'images/header_logo_transparent.gif';
?>
<html>
<head>
<title> File Not Found </title>
<style type="text/css">
@import url("http://images.goabroad.com/css/goabroad2.css");
body {
background-color: white;
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 11px;
line-height: 1.5em;
margin: 0;
padding: 0;
} 
h1 {
margin: 25px;
}
a {
font-weight: normal;
}
a img {
border-width: 0;
}
</style>
</head>
<body bgcolor = "#ffffff">
<table width = "90%" border = "0" cellpadding = "5" cellspacing = "5" align="center">
<tr>
<td align = "center">
<a href="http://<?=$cobrandAddress?>" ><img src="<?=$logoSrc?>" alt="<?=$cobrandAddress?> logo" title="<?=$cobrandAddress?>"></a>
<h1>File Not Found</h1> 
<p>We are unable to find the requested file. It is possible that the page has been renamed or moved.<br />

<p> Please report bad links to the <a href="mailto: error@goabroad.net"> webmaster</a>.
<br> 
Be sure to let us know what page you were referred from and what page you are trying to access. Thank You.
</td>
</tr>
<tr>
<td align="center">
&#169; Copyright  
&nbsp; 2006-2007 | 
<?=$cobrandName?> and GoAbroad.net 
<br> 
<a href="http://<?=$cobrandAddress?>/feedback.php"> Contact <?=$cobrandName?></a>
</td>
</tr>
</table>
</body>
</html>