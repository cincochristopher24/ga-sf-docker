<?php
	/*
	 * tpl.ViewForgotPasswordConfirmation.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
?>

<div id="content_wrapper">
  <div id="forgotpass_wrap">
	<div id="one_column_wrapper">
		<div id="forgot_password" class="section">
			<div class="head_left">			
				<div class="head_right"></div>
			</div>		 
				<div class="content">	
				<h1>Forgot your password?</h1>
				<br />
					<? if( $contents["sent"] ): ?>
						<h2>Login details sent</h2>
						<p>
							A message was sent to the email address you specified with your
							login information. Please use that information to <a href="login.php">login to your account</a>.
						</p>
					<? else: ?>
						<h2>Error in request</h2>
						<p>
							Sorry but an error occurred in your request. Please feel free to report 
							this error through our <a href='feedback.php'>feedback</a> form.
						</p>
					<? endif; ?>			
				   <div class="clear"></div>
				</div>
  	    <div class="foot"></div>
		<div class="clear"></div>
	  </div> 
	</div>
  </div>
</div>
