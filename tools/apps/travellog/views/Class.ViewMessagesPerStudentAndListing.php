<?php
	/*
	 * Class.ViewMessagesPerStudentAndListing.php
	 * Created on Dec 17, 2007
	 * created by marc
	 */
	
	require_once("travellog/views/Class.AbstractView.php");

	class ViewMessagesPerStudentAndListing extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewPageStudentCommunication.php');
		}
		
	} 
?>
