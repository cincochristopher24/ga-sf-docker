<? if ( strlen($grpInstitutionInfo->getAddress1()) || strlen($grpInstitutionInfo->getAddress2()) || strlen($grpInstitutionInfo->getAddress3()) ) { ?>
	<strong>Address : </strong>
	<?= $this->_e($grpInstitutionInfo->getAddress1()) ?>
	<?= $this->_e($grpInstitutionInfo->getAddress2()) ?>
	<?= $this->_e($grpInstitutionInfo->getAddress3()) ?>
	<br />
<? } ?>
<? if ( strlen($grpInstitutionInfo->getCity()) || strlen($grpInstitutionInfo->getState()) || strlen($grpInstitutionInfo->getZip()) || strlen($grpInstitutionInfo->getCountry()) ) { ?>
	<?= $this->_e($grpInstitutionInfo->getCity());?> 
	<? if (strlen($grpInstitutionInfo->getState())) { ?>
		<? echo ','; ?> 
	<? } ?>
	<?= $this->_e($grpInstitutionInfo->getState());?>
	<?= $this->_e($grpInstitutionInfo->getZip());?>
	<?= $this->_e($grpInstitutionInfo->getCountry());?>						
	<br />	
<? } ?>
<? if ( strlen($grpInstitutionInfo->getPhone()) ) { ?>
	<strong>Phone : </strong> <?= $this->_e($grpInstitutionInfo->getPhone());?>
	<br />
<? } ?>
<? if ( strlen($grpInstitutionInfo->getFax()) ) { ?>
	<strong>Fax : </strong> <?= $this->_e($grpInstitutionInfo->getFax());?>
	<br/>
<? } ?>	
<? if ( strlen($grpInstitutionInfo->getMission()) ) { ?>						
	<strong>Mission Statement : </strong>
	<?= $this->_e($grpInstitutionInfo->getMission());?>		
<? } ?>
