<? if ( $obj_paging->getTotalPages() > 1 ): ?>
	<div class="pagination">
		<? $obj_paging->getFirst() ?>
		|
		<? $obj_paging->getPrevious() ?>
		|
		<? $obj_paging->getLinks() ?>
		|
		<? $obj_paging->getNext() ?>
		|
		<? $obj_paging->getLast(); ?>
	</div>
<? endif; ?>

<div class="journal_header"><span>&nbsp;</span></div> 
<div class="content" id="travelers_list_main">

	<?
		$obj_iterator->rewind();
		while($obj_iterator->valid()):
			$currTravelerID = $obj_iterator->current()->getTravelerID(); 
			if ( $currTravelerID != $travelerID):?>
				
				<div class="section traveler">
					<div class="content">
						<div class="wrapper">
							<? if ( $isLogin ): ?>
								<div class="user_stat">
									<?
										$obj_pref = new PrivacyPreference( $currTravelerID ); 
										if ( $obj_iterator->current()->isOnline() && $obj_pref->canViewOnlineStatus($travelerID) ):
									?>
										<img src="/images/orb_online.gif" border="0" alt="" title="online" />
									<? else: ?>
										<img src="/images/orb_offline.gif" border="0" alt="" title="offline" />
									<? endif; ?>
								</div>
							<? endif; ?>
							<a href="/profile.php?action=view&amp;travelerID=<?=$currTravelerID?>"  title="View Profile" class="user_thumb"><img src="<?=$obj_iterator->current()->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink()?>" class="profile_photo_thumb" alt="<?=$obj_iterator->current()->getUsername()?>" height="37" width="37" /></a>	
							<h2>
								<? if (strlen($obj_iterator->current()->getUsername()) > 24): ?>
									<a href="/profile.php?action=view&amp;travelerID=<?=$currTravelerID?>" title="<?=$obj_iterator->current()->getUsername()?>"><?= substr($obj_iterator->current()->getUsername(),0, 23).'...' ?></a>
								<? else: ?>
									<a href="/profile.php?action=view&amp;travelerID=<?=$currTravelerID?>"><?=$obj_iterator->current()->getUsername()?></a>
								<? endif; ?>
							</h2>			
							<?php $travtype = $obj_iterator->current()->getTravelerType() ?>
							
								
							
							<!--?php if ($travtype): ?-->
							<p class="traveler_type">
								<?= $travtype ?> |
								<?php
									$count_journals        = $obj_iterator->current()->getCountTravelJournals();
									$count_journal_entries = $obj_iterator->current()->getCountTravelLogs();
									$journals              = ( $count_journals > 1)? 'Journals':'Journal';
									$entries               = ( $count_journal_entries > 1)? 'Entries':'Entry';
								?>
								<?=$count_journals?>  <?=$journals ?> : <?=$count_journal_entries?> <?=$entries ?>
							</p>
							<!--?php endif; ?-->
						</div>
						<div class="profile_info"> 
							<p><?= HtmlHelpers::truncateText($obj_iterator->current()->getTravelerProfile()->getInterests(), 25)?></p>
						</div> 
						<div class="actions">
							<a href="/profile.php?action=view&amp;travelerID=<?=$currTravelerID?>" title="<?=$obj_iterator->current()->getUsername()?>">view profile</a>
							<? if($isLogin && !$isAdvisor): ?>
								&nbsp;|&nbsp;<a href="/messages.php?act=messageTraveler&id=<?=$currTravelerID?>">send message</a>
								<? if (!$obj_friend->isFriend($currTravelerID) && !$obj_friend->isBlocked($currTravelerID) ):?>
								&nbsp;|&nbsp;<a href="/friend.php?action=request&amp;friendID=<?=$currTravelerID?>">request as friend</a>
								<?endif; ?>
							<? endif; ?>	
						</div>	
						<div class="clear"></div>
					</div>
				</div>
				
			<? endif;
			$obj_iterator->next();
		endwhile;
	?>
	
	<? if ( $obj_paging->getTotalPages() == 0): ?>
	
		<div class="containerbox">No records found!</div>
	
	<? endif; ?>

	<span id="selected" style="display:none;"><?= $selected ?></span>

	<div class="clear"></div>

</div>
<div class="journal_header2"><span>&nbsp;</span></div>
<? if ( $obj_paging->getTotalPages() > 1 ): ?>
	<div class="pagination">
		<? $obj_paging->getFirst() ?>
		|
		<? $obj_paging->getPrevious() ?>
		|
		<? $obj_paging->getLinks() ?>
		|
		<? $obj_paging->getNext() ?>
		|
		<? $obj_paging->getLast(); ?>
	</div>
	
	
<? endif; ?>