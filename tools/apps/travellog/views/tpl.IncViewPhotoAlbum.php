
<? if ($reccount): ?>

			<? for($x=$start; $x<$endrow; $x++): ?>				
							<? if (array_key_exists($x, $photoalbum)): ?>
									
										<?
											$creator 				= $photoalbum[$x]["creator"];
											$context 				= $photoalbum[$x]["context"];
											$genID					= $photoalbum[$x]["genID"];
											$title					= $photoalbum[$x]["title"];
											$numphotos			= $photoalbum[$x]["numphotos"];
											$publish				= $photoalbum[$x]["publish"];
											$primaryphoto		= $photoalbum[$x]["primaryphoto"];
										?>
								
										  <?if($context == "photoalbum"):?>
										  
													<? if($isAdminLogged || $numphotos>0): ?>
														
															<div class="photo_box">		
														
																	<!-- per album container- for ajax callback request-->
																	<div id="peralbum_<?= $genID?>">	
																		
																			<? if($numphotos >0	):?>
																				<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(<?= $primaryphoto->getPhotoid(); ?>,'gallery',<?= $genID?>,'<?= $context?>')">
																					<img id="listen_img_album_<?= $genID?>" src="<?=$primaryphoto->getPhotoLink('default')?>" class="photo_bg"/>
																				</a>
																			<? else: ?>
																					<img id="listen_img_album_<?= $genID?>" src="<?=$primaryphoto->getPhotoLink('default')?>" class="photo_bg"/>
																			<? endif; ?>
																			
																			
																			<!-- album title container- for ajax callback request-->
																			<div id="ptitle_<?= $genID?>">
																				<p class="photo_caption">
																					<?= $title?>
																					<br>
																					<span id="listen_caption_album_<?= $genID?>" style="font-size:10px;text-align:right">
																						<?= $numphotos?> <?= $numphotos>1?"photos":"photo" ?>
																					</span>
																				</p>			
																			</div>
																			<!-- end album title container- for ajax callback request-->
																			<div class="actions">
																				<? if ($isAdminLogged) : ?>
																					<? $ptitle = str_replace("'", " ", $title);?>
																						<a  href="javascript:void(0)" alt="Edit Title" title="Edit Title" onclick="photo.showedit(<?= $genID; ?>)">
																							Edit |
																						</a>
																						<!--
																						<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(<?= $primaryphoto->getPhotoid(); ?>,'gallery',<?= $genID?>,'<?= $context?>')">
																									Manage Photos	| 
																						</a>
																						-->
																						<a href="/photos.php?type=group&ID=<? echo $groupID;?>&context=photoalbum&genID=<?echo $genID ?>" >
																									Manage Photos	| 
																						</a>
																						
																						<a href="javascript:void(0)" onclick="photo.Delete(<?= $genID?> );">Delete</a>
																						
																				<? endif; ?>
																			</div>
																	</div>
																	<!-- end per album container- for ajax callback request-->
															
															</div>
													<? endif; ?>
							
										<?else:?>
												
												<? if ($loggedUserID == $creator || $publish) : ?>
													<div class="photo_box">		
															<!-- per album container- for ajax callback request-->
															<div id="peralbum_<?= $genID?>">	
																<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(<?= $primaryphoto->getPhotoid(); ?>,'gallery',<?= $genID?>,'<?= $context?>')">
																
																	<img src="<?=$primaryphoto->getPhotoLink('default')?>"/>
																</a>
																
																<!-- album title container- for ajax callback request-->
																<div id="ptitle_<?= $genID?>">
																	<p class="photo_caption">
																		<?= substr($title,0,20)?>
																		<br>
																		<span style="font-size:10px;text-align:right">
																			<?= $numphotos?> <?= $numphotos>1?"photos ":"photo " ?>(journal).	
																		</span>
																	</p>			
																</div>
																<!-- end album title container- for ajax callback request-->
																<div class="actions" id="journalalbum_<?= $genID?>">
																	<? if ($loggedUserID == $creator) : ?>
																		<a href="javascript:void(0)" onclick="GROUPPhotoAlbum.TOGpublish('<?= $genID?>','<?= $publish?0:1?>')">
																			<?if($publish):?>
																				Unpublish
																			<?else:?>
																				Publish
																			<?endif; ?>
																		</a>
																	<? endif; ?>
																
																</div>
															</div>
															<!-- end per album container- for ajax callback request-->
													
													</div>			
											
												<? endif; ?>
												
										<? endif; ?>
									  
										
								<? endif; ?>				
																																																					
																								
			<? endfor; ?>
			
			<div class="clear"></div>		
			
<? else: ?>
	<p class="help_text"> <span>No Photo Albums yet</span>.
	<? if($isAdminLogged): ?>
			<a href="/photoalbum.php?action=add&groupID=<? echo $groupID; ?>">Create new Photo Album?</a>
	<? endif; ?>
	</p>
<? endif; ?>							
							
<? if ($reccount): ?>
	<div class="pagination_cont">
		<? $page->showPagination() ?>
		<div class="pagebg_right"></div>
		<div class="clear"></div>
	</div>
<? endif; ?>
<div class="clear"></div>
				
			
			
  
  
