<?php
	require_once('travellog/model/Class.LocationFactory.php');
	$file_factory      = FileFactory::getInstance(); 
	$obj_factory       = LocationFactory::instance(); 
	$width             = ( isset( $contents['width']  ) )? $contents['width']: null; 
	$height            = ( isset( $contents['height'] ) )? $contents['height']: 300;
	$title             = ( isset($contents['title']) )? $contents['title'] : 'Map of Travel Journals';
	$querystring       = ( isset( $contents['querystring'] ) )? $contents['querystring'] . '&': 'action=All&';
	$col_markers       = array();

	$col_locationIDs = $contents['col_locations'];
	$locations = '';
	if( count($col_locationIDs) ){
		foreach($col_locationIDs as $obj){
			if($obj instanceOf Country){
				$obj_location = $obj;
				$locations .= '{';
				$locations .= '"name": "'.$obj_location->getName().'",'; 
				$locations .= '"icon": ["ga_markerTransparent", "ga_marker_shadowTransparent"],';  
				$locations .= '"posn": ['.$obj_location->getCoordinates()->getY().', '.$obj_location->getCoordinates()->getX().'],';
				$locations .= '"url" : "/journal.php?'.$querystring.'locationID='.$obj->getLocationID().'"';
				$locations .= '},';
			}
		} 
	} 
  	
    
	
	$jsCode = <<<BOF
		var map;
	    var mgr;
	    var icons = {};
	    var allmarkers = [];

	    var iconData = {
			"n_america": { width: 132, height: 35 },
		  	"ga_marker_shadowTransparent": { width: 35, height: 40 },  

		  	"asia": { width: 63, height: 35 },
		  	"house-shadow": { width: 35, height: 40 },

		  	"australia": { width: 100, height: 35 },
		  	"ga_marker_shadowTransparent": { width: 35, height: 40 },

		  	"s_america": { width: 132, height: 35 },
		  	"ga_marker_shadowTransparent": { width: 35, height: 40 },

		  	"africa": { width: 63, height: 35 },
		  	"ga_marker_shadowTransparent": { width: 35, height: 40 },

		  	"europe": { width: 77, height: 35 },
		  	"ga_marker_shadowTransparent": { width: 35, height: 40 },

		  	"ga_markerTransparent": { width: 15, height: 20 },
		  	"ga_marker_shadowTransparent": { width: 29, height: 20 }  
		};

		var officeLayer = [
		  {
		    "zoom": [0, 2], 
		    "places": [
		      {
		        "name": "North America",
		        "icon": ["n_america", "ga_marker_shadowTransparent"],
		        "posn": [52.908902047770255, -99.84375], 
		        "url" : ""
		      },
		      {
		        "name": "Asia",
		        "icon": ["asia", "ga_marker_shadowTransparent"],
		        "posn": [32.54681317351514, 91.40625],
		        "url" : ""
		      },
		      {
		        "name": "Australia",
		        "icon": ["australia", "ga_marker_shadowTransparent"],
		        "posn": [-15.284185114076433, 132.890625], 
		        "url" : ""
		      },
		      {
		        "name": "South America",
		        "icon": ["s_america", "ga_marker_shadowTransparent"],
		        "posn": [0.7031073524364909, -55.546875], 
		        "url" : ""
		      },
		      {
		        "name": "Africa",
		        "icon": ["africa", "ga_marker_shadowTransparent"],
		        "posn": [27.683528083787756, 14.765625], 
		        "url" : ""
		      },
		      {
		        "name": "Europe",
		        "icon": ["europe", "ga_marker_shadowTransparent"],
		        "posn": [54.57206165565852, 16.171875],  
		        "url" : ""
		      }
		    ]
		  },
		  {
		    "zoom": [3, 17],       
		    "places": [
				$locations	
		    ]
		  }
		];

	    function loadMap() {
	    	if (GBrowserIsCompatible()) {
		        map = new GMap2(document.getElementById("map"));  
		        map.addControl(new GLargeMapControl()); 
		        map.setCenter(new GLatLng(46.55886030311719, 0.703125), 1);    
		        map.disableDoubleClickZoom();  
		        mgr = new MarkerManager(map, {trackMarkers:true});
		        window.setTimeout(setupOfficeMarkers, 0);
	      	}
	    }

	    function getIcon(images) {
	    	var icon = null;
	      	if (images) {
	        	if (icons[images[0]]) {
	          		icon = icons[images[0]];
	        	} else {
					icon = new GIcon();
	  				icon.image = "/images/Gicons/" 
	      				+ images[0] + ".png";


	  				var size = iconData[images[0]];
		  			icon.iconSize = new GSize(size.width, size.height);
		  			icon.iconAnchor = new GPoint(size.width >> 1, size.height >> 1);
		  			icon.shadow = "/images/Gicons/" 
		      			+ images[1] + ".png";
		  			size = iconData[images[1]];
		  			icon.shadowSize = new GSize(size.width, size.height);
		  			icons[images[0]] = icon;
	        	}
	      	}
	      	return icon;
	    }

	    function setupOfficeMarkers() {
	    	allmarkers.length = 0;
	      	for (var i in officeLayer) {
	        	if( i < officeLayer.length){
		        	var layer = officeLayer[i];
		        	var markers = [];
		        	for (var j in layer["places"]) {
		          		if( j < layer["places"].length){
							var place = layer["places"][j];
			          		var icon = getIcon(place["icon"]);
			          		var title = place["name"];
			          		var url   = place["url"];
			          		var lat   = place["posn"][0];
							var lng  = place["posn"][1];
			          		var posn = new GLatLng(lat, lng);
			          		var marker = createMarker(posn,title,icon, url); 
			          		markers.push(marker);
			          		allmarkers.push(marker);
			          	} 
		        	}
		        	mgr.addMarkers(markers, layer["zoom"][0], layer["zoom"][1]);
		        }  
	      	}
	      	mgr.refresh();
	    }

	    function createMarker(posn, title, icon, url) {
	    	var marker = new GMarker(posn, {title: title, icon: icon });
	      	GEvent.addListener(marker, 'click', function() { 
		    	if( url.length )
			  		window.location = url;
			  	else{    
			      	map.setCenter(posn, 3); 
			  	}        	 
	      	});  
			return marker;
	    }

	    loadMap();   
BOF;
	echo $jsCode;
	
?>
