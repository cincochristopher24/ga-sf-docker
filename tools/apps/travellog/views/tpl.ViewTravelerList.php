<?php

	/*
	 * tpl.ViewTravelerList.php
	 * Created on Dec 6, 2006
	 * marc
	 */
	 
	 if( !isset($contents["displayOnline"]) )
	 	$contents["displayOnline"] = 0;
	 if( !isset($contents["displaySendMessage"]) )
	 	$contents["displaySendMessage"] = 0;	
	 if( !isset($contents["isAdminLogged"]) )
	 	$contents["isAdminLogged"] = false;	
	 $loggeduserID = $contents["travelerID"];	
	 $contents["viewMode"] = isset($contents["viewMode"]) ? $contents["viewMode"] : "traveler";	
	 
	 switch( $contents["viewMode"] ){
	 	case "group": 
	 		$spacer = "&nbsp;";
	 		// insert thumbnail class | image size here
	 		$thumbSizeWidth = 37; // width
	 		$thumbSizeHeight = 37; // height 
	 		// ul class
	 		$nULclass = "";
	 		$nLIclass = "";
	 		break;
	 	default:
	 		$spacer = "<br />";
	 		// insert thumbnail class | image size here
	 		$thumbSizeWidth = 37; // width
	 		$thumbSizeHeight = 37; // height
	 		// ul class
	 		$nULclass = "";
	 		$nLIclass = "";
	 		break;
	 }	
	 	
?>

	<ul class="travelers_container elements">	
		<? foreach( $contents["travelers"] as $traveler ):
				$nProfile = $traveler->getTravelerProfile();	
				try {
					$canViewOnlineStatus = $traveler->getPrivacyPreference()->canViewOnlineStatus($loggeduserID);
				}
				catch (Exception $e) {
					$canViewOnlineStatus = FALSE;	
				} ?>
				<li>
						<a href="profile.php?action=view&amp;travelerID=<?= $nProfile->getTravelerID(); ?>" class="thumb"><img src="<?=$nProfile->getPrimaryPhoto()->getThumbnailPhotoLink() ?>" width="<?= $thumbSizeWidth; ?>" height="<?= $thumbSizeHeight; ?>" /></a>
						<div class="details">
							<? if (strlen($nProfile->getUserName()) > 14): ?>
								<a href="<?= $nProfile->getUsername(); ?>" class="username" title="<?= $nProfile->getUserName(); ?>">  <?= substr($nProfile->getUserName(),0, 14); ?>...</a>
							<? else: ?>	
								<a href="<?= $nProfile->getUsername(); ?>" class="username"> <?= $nProfile->getUserName(); ?> </a>
							<?endif; ?>							
						
							
						<p><? switch( $contents["viewMode"] ): 
								case "group": 
									if( $canViewOnlineStatus && $traveler->isOnline() ): ?>
							   			online<?= $spacer; ?>
							   		<? endif;
							   		if( $contents["isAdminLogged"] && isset($contents["grpID"]) ): ?> 
							   			<a href="messages.php?act=messageGroupStaff&gID=<?= $contents["grpID"] ?>">Message Staff</a><?= $spacer; ?>
							   		<? endif;
							   		break;
							   	default: 
							   		$entryJournal = array();
							   		if( $traveler->getCountTravelLogs() != NULL && 0 < $traveler->getCountTravelLogs() ){
							   			if( 1 < $traveler->getCountTravelLogs() )
							   				$str = " Entries";
							   			else	
							   				$str = " Entry";
							   			$entryJournal[] = $traveler->getCountTravelLogs() . $str ;		
							   		}
							   		if( $traveler->getCountTravelJournals() != NULL && 0 < $traveler->getCountTravelJournals() ){
							   			if( 1 < $traveler->getCountTravelJournals() )
							   				$str = " Journals";
							   			else	
							   				$str = " Journal";
							   			$entryJournal[] = $traveler->getCountTravelJournals() . $str;		
							   		}
							   		echo implode(" | ",$entryJournal);	
						  endswitch; ?>		   			
						</p>			
						</div>
						<div class="clear"></div>		
				</li>
		<? endforeach; ?>
	</ul>		