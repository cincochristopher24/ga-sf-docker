<style>
div, p, h1, h2, h3, h4, img {
  margin: 0;
  padding: 0;
}

.clear {
  clear: both;
}

div#ganet_app_container {
  width: 483px;
  padding: 0 0 20px 80px;
}

div#top_level h3 {
  font-size: 12px;
  color: #4e76b2;
  width: 320px;
  padding: 0 0 0 5px;
}

div#top_level input.title {
  font-size: 11px;
  font-weight: bold;
  color: #4e76b2;
  width: 350px;
  padding: 0 0 0 5px;
  border: none;
}

div#top_level h4.top_level_date {
  float: right;
  font-size: 10px;
  color: #9ea0a2;
  padding: 1px 5px 0 0;
}

div#top_level input.top_level_date {
  float: right;
  font-size: 10px;
  font-weight: bold;
  color: #9ea0a2;
  padding: 1px 5px 0 0;
  border: 1px;
}

div#top_level {
  padding: 5px 0 10px 0;
  border-top: 1px solid #e6e6e6;
  background-color: #fafafa;
}

div#top_level p {
  font-family: Arial, Verdana, sans-serif;
  font-size: 11px;
  color: #777777;
  width: 320px;
  padding: 2px 0 0 5px;
}

div#top_level input.details {
  font-family: Arial, Verdana, sans-serif;
  font-size: 9px;
  color: #92a4be;
  width: 260px;
  padding: 0 0 0 5px;
  border: none;
}


div#destinations_container h4 {
  font-size: 12px;
  color: #60718b;
  float: left;
}

div#destinations_container input.country {
  font-size: 11px;
  font-weight: bold;
  color: #60718b;
  width: 143px;
  border: none;
}

div#destinations_container h4.sub_date {
  font-size: 10px;
  float: right;
  color: #ccd0d3;
  margin: 2px 0 0 0;
  text-align: right;
  padding: 0 5px 0 0;
}

div#destinations_container p.sub_date {
  font-size: 10px;
  float: right;
  color: #ccd0d3;
  margin: 2px 0 0 0;
  text-align: right;
  padding: 0 5px 0 0;
}

div#destinations_container input.sub_date {
  font-size: 10px;
  width: 108px;
  float: right;
  margin-top: -12px;
  color: #ccd0d3;
  border: none;
}

#travel_entry_wrapper {
  font-family: Arial, Helvetica, sans-serif;
  width: 483px;
  padding-bottom: 5px;
}

div#destinations_container img.bullet {
   padding: 0 10px 0 5px;
   margin-top: 2px;
   float: left;
}

div#travel_entry_wrapper img.delete_desc {
  margin: 2px 0 0 5px;
  float: left;

}

div#destinations_container a.show_dest, div#destinations_container a.hide_dest {
   font-family: Arial, Helvetica, sans-serif;
   float: right;
   text-align: right;
   color: #4e76b2;
   font-size: 9px;
   padding: 0 5px 0 0;
}

div#top_level img.arrow_icon, div#destinations_container img.arrow_icon {
   padding: 0 0 0 2px;
}

div#top_level div.actions {
   float: right;
   padding: 2px 5px 0 0;
}

div#top_level div.date_section {
  float: right;
  width: 150px;
  display: block;
}

div#destinations_container div.sublevel_destination {
  float: left;
  width: 483px;
  padding: 5px 0 0 0;
}

div#destinations_container p.destination_desc {
  width: 460px;
  font-size: 11px;
  margin-left: 22px;
  color: #60718B;
  border-top: 1px solid #e6e6e6;
  padding: 2px 0 0 0;
}

div#destinations_container input.destination_desc {
  width: 460px;
  font-size: 9px;
  margin-left: 22px;
  padding: 5px 0 0 0;
  color: #919dae;
  border: none;
}


div#ganet_app_container p#add_travel {
  font-family:Arial, Helvetica, sans-serif;
  font-size: 12px;
  font-weight: bold;
  color: #4e76b2;
  padding: 0 5px 5px 0;
  float: right;
}

div#ganet_app_container img#add_img {
  padding: 0 5px 0 0;
}

div.dashboard_header div.dh_titlebar h2 {
  margin: 0 0 0 70px !important;
}

div.past div#top_level {
  background-color: #e6e5e5;
}

div.past div#top_level h3 {
  color: #60718B;
}

div.past div#destinations_container {
  background-color: #fafafa;
}

div.standard_message h1 {
  padding: 5px;
}

div.standard_message h1 p {
  padding-top: 10px;
}

</style>

<?php
$showWishList = $ganetUsername ? true : false;

if(!isset($_REQUEST['fb_sig_in_profile_tab'])) 
	require_once('tpl.menu.php');

?>

<div id="ganet_app_container">
  <?php if (!$isOwner && !isset($_REQUEST['fb_sig_in_profile_tab']) ) : ?>
  	<h5 style="margin: 0 0 8px 0"><fb:name uid="<?php echo $_GET['vid'] ?>" possessive="true" /> travel list</h5>
  <?php endif ?>

  <?php if(isset($_POST['fb_sig_in_profile_tab'])) : ?>
  	<fb:visible-to-owner>
    <p id="add_travel">
      <img id="add_img" src="http://www.goabroad.net/facebook/mytravelplans/images/add.jpg"/>
  	  <a href="http://apps.facebook.com/travel-todo/index.php?action=add">add a travel itinerary</a>
    </p>
    </fb:visible-to-owner>	
  <?php endif; ?>


  <?php if ($isOwner) : ?>
    <p id="add_travel">
      <img id="add_img" src="http://www.goabroad.net/facebook/mytravelplans/images/add.jpg"/>
	  <a href="http://apps.facebook.com/travel-todo/index.php?action=add">add a travel itinerary</a>
    </p>
  <?php endif; ?>

  <div class="clear"></div>

  <!-- START LIST -->
  <?php if (count($travelItems))  {
  $pastClass = ''; $i = 0;
  foreach ($travelItems as $item) {
    $travelItemID = $item->getTravelWishID();
    $title = htmlentities($item->getTitle(), ENT_QUOTES);
    $strippedTitle = preg_replace("/[!@#$%^&*()\"']/", "", $item->getTitle());
    $details = $item->getDetails();
    $tripDateFrom = $item->getDateFrom();
    $tripDateTo = $item->getDateTo();
    $tripDateRange = $item->getDateRange();
    $destinations = $item->getDestinations();
	if($includePastTravelsSetting && $numPresentTravels == $i){ ?>
  		<div style="color: #60718B; font-weight: bold; padding: 5px 0 5px 5px">PAST TRAVELS</div>
	<?php $includePastTravelsSetting = false; $pastClass = "class='past'"; } ?>


  <div id="travel_entry_wrapper" <?php echo $pastClass ?> >

    <div id="top_level" <?php echo $pastClass ?> >
      <!-- DATE-RANGE -->
      <div class="date_section">
        <h4 class="top_level_date"><?php echo $tripDateRange; ?></h4>
        <div class="clear"></div>
        <div class="actions">
          <?php if ($isOwner) { ?>
          <!-- EDIT LINK -->
          <a href="http://apps.facebook.com/travel-todo/index.php?action=edit&travelItemID=<?php echo $travelItemID;?>" title="Edit">
            <img src="http://www.goabroad.net/images/goabroad_app/edit.gif" alt="[Edit]"/>
          </a>
          <!-- DELETE LINK -->
          <a href="#delete" title="Delete" onclick="new Dialog().showChoice('Confirm Delete', '<?php echo $strippedTitle; ?>').onconfirm = function() {
            document.setLocation('http://apps.facebook.com/travel-todo/index.php?action=delete&travelItemID=<?php echo $travelItemID; ?>');
            return false;}"
          >

            <img src="http://www.goabroad.net/images/goabroad_app/delete.gif" alt="[Delete]"/>
          </a>
          <?php } ?>
        </div><!-- end actions -->
        <div class="clear"></div>
      </div><!-- end date_section -->
      <!-- TITLE -->
      <h3><?php echo $title; ?></h3>
      <!-- DETAILS -->
      <p><?php echo htmlentities($details, ENT_QUOTES); ?></p>
    </div><!-- end top_level -->

    <div id="destinations_container" <?php echo $pastClass ?> >
    <?php
      foreach ($destinations as $d) {
        $travDestID = $d->getTravelWishToCountryID();
        $destTravItemID = $d->getTravelWishID();
        $destCountry = $d->getCountry()->getName();
        $destCountryID = $d->getCountry()->getCountryID();
        $destDateFrom = $d->getStartDate();
        $destDateTo = $d->getEndDate();
        $destDateRange = $d->getDateRange();
        $destDetails = $d->getDescription();

        $showFriendsLink = in_array($destCountryID,$friendsCountryIDs) && $pastClass == '' ? true : false;
    ?>
      <div id="rmvDest">
        <div class="sublevel_destination">
          <!-- COUNTRY -->
          <h4><img class="bullet" src="http://www.goabroad.net/facebook/mytravelplans/images/bullet.gif"/><?php echo $destCountry;?></h4>
        <?php if ($isOwner) : ?>
          <?php if (count($destinations) != 1 ) : ?>
          <a id="dest<?php echo $travDestID;?>" href="#rmvDest" title="Remove destination" onclick="new Dialog().showChoice('Remove Destination', '<?php echo $destCountry ?>').onconfirm = function() {
              rmvDest(document.getElementById('<?php echo 'dest'.$travDestID; ?>'), '<?php echo $destTravItemID; ?>', '<?php echo $travDestID; ?>','<?php echo $destCountryID ?>', <?php echo $pastClass == '' ? '0' : '1' ?>);
              document.getElementById('destIndicator<?php echo $destTravItemID; ?>').setStyle('display', 'block');}"
          >
            <img class="delete_desc" src="http://www.goabroad.net/facebook/mytravelplans/images/delete_dest.jpg" alt="delete">
          </a>
          <?php endif; ?>
        <?php endif; ?>
          <!-- DATE RANGE -->
          <h4 class="sub_date"><?php echo $destDateRange; ?></h4>
          <div class="clear"></div>
        </div><!-- end sublevel_destination -->
        <div class="clear"></div>
        <!-- DESTINATION DETAILS -->
        <p class="destination_desc"><?php echo $destDetails; ?></p>
        <?php if ($showFriendsLink && $isOwner) :?>
        	<a style="float: right;" href="http://apps.facebook.com/travel-todo/destinations.php?id=<?php echo $user ?>&country=<?php echo $destCountryID ?>" title="Friends with same destination">See who else is going</a>
        <?php endif; ?>
        <div class="clear"></div>
      </div><!-- end rmvDest -->
    <?php }//end destinations loop ?>
    </div><!-- end destinations_container -->
    <div class="clear"></div>
    <div id="destIndicator<?php echo $destTravItemID; ?>" style="display: none">
      <img style="margin: 5px 0 0 0; float: left;" src="http://www.goabroad.net/facebook/mytravelplans/images/loading.gif"><p style="float: left; margin: 10px 0 0 2px;">Loading...</p>
      <div style="clear: both;"></div>
    </div>
  </div><!-- end travel_entry_wrapper -->
  <?php $i++; if($itemsToDisplaySetting == $i) break;

	}//end travelItem loop ?>
  <!-- END LIST -->

<?php } else { ?>
  <!-- NO TRAVEL ITEMS -->
  <fb:explanation>
    <fb:message>You haven't listed your travels yet.</fb:message>
    List down your travel plans and find out who could be traveling your way!
    List down your past travels-- show how well traveled you are and help travelers who need tips about traveling to the places you've been to!
  </fb:explanation>
  <div style="clear: both;"></div>
<?php } ?>
</div><!-- end div ganet_app_container -->

<!-- WISHLIST -->
<?php if ($showWishList) : ?>
<div style="padding: 0 0 15px 0">
  <div id="wish" style="width: 463px; margin: 10px 0 10px 80px; border: 1px solid #e6e6e6; padding: 10px; background-color: #fafafa; color: #777777;" >
    <span style="color: #60718B; font-weight: bold;">Travel Wishlist</span> (<?php echo count($wishList['countryIDs']) .' : '. implode(', ',$wishList['countryNames']) ?>)
    <div id="indicator" style="display: none">
      <img style="margin: 5px 0 0 0; float: left;" src="http://www.goabroad.net/facebook/mytravelplans/images/loading.gif"><p style="float: left; margin: 10px 0 0 2px;">Loading...</p>
      <div style="clear: both;"></div>
    </div>
  </div>
<?php if ($isOwner) : ?>
  <a id="showEditForm" clicktoshow="editForm" clicktohide="showEditForm" style="padding: 0 0 0 470px; display: block; cursor: pointer">Edit Country List</a>
<?php endif; ?>
  <form style="display: none" id="editForm" action="index.php?action=editWish" method="post">
    <div style="border: 1px solid #e6e6e6; margin: 10px 0 0 80px; width: 483px; height: 350px; overflow: auto;">
      <input type="hidden" name="user" value="<?php echo $user; ?>"/>

    <?php foreach ($countryList as $ctry) {
      $id=$ctry->getCountryID(); ?>

      <input type="checkbox"
             name="wishList[]"
             value="<?php echo $id; ?>"
             id="c<?php echo $id; ?>"
         <?php
		 for($i=0; $i<count($wishList['countryIDs']); $i++) {
           if ($id == $wishList['countryIDs'][$i] ) { ?>
             checked="checked"
         <?php
             break;
           }//end if
         }//end foreach
         ?>
      ><label for="c<?php echo $id; ?>"><?php echo $ctry->getName(); ?></label><br/>
    <?php } ?>
    </div>
    <div style="padding: 10px 0 10px 80px; margin: 0 0 0 5px;">
      <input type="submit"
         class="inputbutton"
         clickrewriteurl="http://www.goabroad.net/facebook/mytravelplans/wishList.php"
         clickrewriteid="wish"
         clicktoshow="showEditForm, indicator"
         clicktohide="editForm"
         value="Save"/>
      &nbsp;
      <a style="padding: 1px 5px 1px 5px;" clicktohide="editForm" clicktoshow="showEditForm" href="http://apps.facebook.com/travel-todo">Cancel</a>
    </div>
  </form>
</div>
<?php endif; ?>
<!-- END WISHLIST -->

<?php if (isset($publishStream) && !is_null($publishStream)) : ?>
<script>
/*
var attachment = {
	'name': "Check out My Travel Plans!",
	'href': 'http://apps.facebook.com/travel-todo/index.php?id=</?php echo $user;?>',
	'description': "Planning on a trip? Share them with me with My Travel Plans.",
	'media': [{ 'type': 'image', 'src': 'http://bit.ly/dwqk9C', 'href': 'http://apps.facebook.com/travel-todo'}]
};
var callback = function(post_id, exception, data){
	alert('callback called')
	//document.setLocation('http://apps.facebook.com/my-travel-map/index.php?updatedMyTravelMap');
}
*/

Facebook.streamPublish(
	'<?php echo addcslashes($publishStream, "'"); ?>', 
	{
		'name': "Check out My Travel Plans!",
		'href': 'http://apps.facebook.com/travel-todo/index.php?id=<?php echo '$user';?>',
		'description': "Planning on a trip? Share them with your friends with My Travel Plans.",
		'media': [{ 'type': 'image', 'src': 'http://bit.ly/dwqk9C', 'href': 'http://apps.facebook.com/travel-todo'}]
	}, 
	"", 
	null, 
	"Let your friends know where you plan to go.",
	function(post_id, exception, data){
		/*document.setLocation('http://apps.facebook.com/my-travel-map/index.php?updatedMyTravelMap');*/
	}
);

</script>
<?php endif; ?>

<script>
function rmvDest(obj, itemID, travDestID, countryID, isPastTravel) {
  var ajax = new Ajax();
  ajax.responseType = Ajax.FBML;
  ajax.ondone = function(data) {
    if (data!='') {// edit conditional
      if (obj.getParentNode().getParentNode().getParentNode().getId() == 'destinations_container') {//sanity check
        obj.getParentNode().getParentNode().getParentNode().getParentNode().setInnerFBML(data);
      }// TODO: else construct containing error info
    } else {
      document.getElementById('destIndicator'+travDestID).setStyle('display', 'none');
      new Dialog().showMessage('Info', 'We cannot process your request at this time. Please try again later.');
    }
  }
  ajax.onerror = function() {
    document.getElementById('destIndicator'+travDestID).setStyle('display', 'none');
    new Dialog().showMessage('Info', "An error occurred while processing your request.");
  }

  ajax.requireLogin = 1;
  var queryParams = { "ajaxAction":"deleteDestination","travelItemID":itemID,"travelDestID":travDestID,"countryID":countryID,"isPastTravel":isPastTravel };

  ajax.post("http://www.goabroad.net/facebook/mytravelplans/ajax.php", queryParams);
}

</script>
