<form id="travelForm" action="index.php?action=save" style="padding: 15px 0 20px 20px;" method="post">
  <div id="ganet_goabroad_wrapper">
    <h2 style="padding: 0 0 20px 0; color: #3B5998;">
      <?php if ($action == 'add') : ?>
        Add a New Travel Itinerary
      <?php else: ?>
        Modify Travel Itinerary
      <?php endif; ?>
    </h2>
    <!-- TITLE -->
    <h4 style="padding: 0 0 5px 0">Give your itinerary a name:</h4>
    <input style="border: 1px solid #BDC7D8; width: 300px;" id="title" name="title" class="input_box" type="text" value="<?php echo htmlentities($title, ENT_QUOTES); ?>"/>
    <div id="titleError" style="display:none;"></div><!-- IE will allocate space even if divs are empty if we don't set style to none -->
    <div style="color: #3B5998; padding-top: 5px; width: 330px">Please name your trip, e.g. My World Tour in 2007, Exploring Greece, My Year Out</div>
    <!-- DETAILS -->
    <h4 style="padding: 10px 0 5px 0">Itinerary Description:</h4>
    <textarea style="margin: 0; width: 293px;" id="details" class="input_box" name="details" value="<?php echo htmlentities($details, ENT_QUOTES); ?>"><?php echo htmlentities($details, ENT_QUOTES); ?></textarea>
    <div id="detailsError" style="display:none;"></div>
    <div style="color: #3B5998; padding-top: 5px; width: 330px">Please write a few details about your trip, e.g. [My World Tour in 2007] My quest to set foot on every continent, [Exploring Greece] Satisfy my fascination with ancient temples, [My Year Out] I spent a fun year out before I started college.</div>
    <!-- DESTINATIONS -->
    <?php $divCount = 0; $currentDate = getDate(); ?>
    <?php if (empty($destinations)) : ?>
    <!-- ACTION = ADD -->
    <div id="destinations">
      <div id="<?php echo $divCount; ?>" style="margin: 0 0 2px 0; padding: 0 0 5px 0">
        <h4 style="padding: 10px 0 5px 0; width: 305px; margin: 0;" id="lblDestination">Select Destination Country:</h4>
        <select id="country" style="padding: 0px;" name="countries[]">
          <option value=""></option>
        <?php foreach ($ctryList as $ctry) : ?>
          <option value="<?php echo $ctry->getCountryID(); ?>">
            <?php echo $ctry->getName(); ?>
          </option>
        <?php endforeach; ?>
        </select>
        
        <div id="destinationsErrorSingle" style="display:none;"></div>        
        
        <!-- DESCRIPTION -->
        <h4 style="padding: 10px 0 5px 0">Destination Details:</h4>
        <textarea style="margin: 0; width: 293px;" class="input_box" name="description[]"></textarea>
        <!-- FROM DATE -->
        <h4 style="padding: 10px 0 5px 0">Travel Start Date:</h4>
        <select id="fromMonth" name="destFromMonth[]" onchange="updateEndDate(this);">
        <?php foreach ($monthArray as $index => $month) : ?>
          <option value="<?php echo $index ?>" <?php if ($index == $currentDate['mon']): ?> selected="selected"<?php endif; ?>>
            <?php echo $month; ?>
          </option>
        <?php endforeach; ?>
        </select>
        <select id="fromDay" name="destFromDay[]" onchange="updateEndDate(this);">
          <option value="00"></option>
          <?php for($i=1; $i<32; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if ($i == $currentDate['mday']): ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <select id="fromYear" name="destFromYear[]" onchange="updateEndDate(this);">
          <option value="0000"></option>
          <?php for($i=2000; $i<2031; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if ($i == $currentDate['year']): ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <div id="fromDateError" style="display:none;"></div>
        <div style="color: #3B5998; padding-top: 5px; width: 330px">Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</div>
        <!-- TO DATE -->
        <h4 style="padding: 10px 0 5px 0">Travel End Date:</h4>
        <select id="toMonth" name="destToMonth[]">
        <?php foreach ($monthArray as $index => $month) : ?>
          <option value="<?php echo $index ?>">
            <?php echo $month; ?>
          </option>
        <?php endforeach; ?>
        </select>
        <select id="toDay" name="destToDay[]">
          <option value="00"></option>
          <?php for($i=1; $i<32; $i++): ?>
          <option value="<?php echo $i; ?>">
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <select id="toYear" name="destToYear[]">
          <option value="0000"></option>
          <?php for($i=2000; $i<2031; $i++): ?>
          <option value="<?php echo $i; ?>">
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <div id="toDateError" style="display:none;"></div>
        <div style="color: #3B5998; padding-top: 5px;">Optional</div>
        <?php $divCount++;?>
      </div><!-- end divCount -->
    </div><!-- end id="destinations" -->
    <?php else : ?>
    <!-- ACTION = EDIT -->
    <div id="destinations">
    <?php
      foreach ($destinations as $d) {
        $travDestID = $d->getTravelWishToCountryID();
        $destTravItemID = $d->getTravelWishID();
        $destCountryID = $d->getCountry()->getCountryID();
        $destDetails = htmlentities($d->getDescription(), ENT_QUOTES);

        list($destFromYear, $destFromMonth, $destFromDay) = explode('-', $d->getStartDate());
        list($destToYear, $destToMonth, $destToDay) = explode('-', $d->getEndDate());
    ?>
      <div id="<?php echo $divCount; ?>" style="margin: 0 0 2px 0; padding: 0 0 5px 0">
        <?php if ($divCount == 0) { ?>
        <h4 style="padding: 10px 0 5px 0; width: 305px; margin: 0;" id="lblDestination">Destination:</h4>
        <?php } else { ?>
        <h4 style="padding: 10px 0 5px 0; width: 305px; border-top: 1px solid #e6e6e6; margin-top: 15px;" id="lblDestination">Destination:</h4>
        <?php } ?>
        <select id="country" style="padding: 0px;" name="countries[]">
          <option value=""></option>
        <?php foreach ($ctryList as $ctry) : ?>
          <option value="<?php echo $ctry->getCountryID(); ?>" <?php if ($ctry->getCountryID() == $destCountryID): ?> selected="selected"<?php endif; ?>>
            <?php echo $ctry->getName(); ?>
          </option>
        <?php endforeach; ?>
        </select>
        <?php if (count($destinations) > 1 && $divCount > 0) { ?>
        <a style="padding: 0 0 0 10px;" href="#rmvDest" onclick="rmvDest(this); return false;">Delete This Destination</a>
        <?php } ?>
        
        <div id="destinationsErrorSingle" style="display:none;"></div>
        
        <!-- DESCRIPTION -->
        <h4 style="padding: 10px 0 5px 0">Description:</h4>
        <textarea style="margin: 0; width: 293px;" class="input_box" name="description[]" value="<?php echo $destDetails; ?>"><?php echo $destDetails; ?></textarea>
        <!-- FROM DATE -->
        <h4 style="padding: 10px 0 5px 0">Travel Start Date:</h4>
        <select id="fromMonth" name="destFromMonth[]">
        <?php foreach ($monthArray as $index => $month) : ?>
          <option value="<?php echo $index ?>" <?php if ($index == $destFromMonth): ?> selected="selected"<?php endif; ?>>
            <?php echo $month; ?>
          </option>
        <?php endforeach; ?>
        </select>
        <select id="fromDay" name="destFromDay[]">
          <option value="00"></option>
          <?php for($i=1; $i<32; $i++): ?>
          <option value="<?php echo $i ?>" <?php if ($i == $destFromDay): ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <select id="fromYear" name="destFromYear[]">
          <option value="0000"></option>
          <?php for($i=2000; $i<2031; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if($i == $destFromYear) : ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <div id="fromDateError" style="display:none;"></div>
        <div style="color: #3B5998; padding-top: 5px; width: 330px">Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</div>
        <!-- TO DATE -->
        <h4 style="padding: 10px 0 5px 0">Travel End Date:</h4>
        <select id="toMonth" name="destToMonth[]">
        <?php foreach ($monthArray as $index => $month) : ?>
          <option value="<?php echo $index ?>" <?php if ($index == $destToMonth): ?> selected="selected"<?php endif; ?>>
            <?php echo $month; ?>
          </option>
        <?php endforeach; ?>
        </select>
        <select id="toDay" name="destToDay[]">
          <option value="00"></option>
          <?php for($i=1; $i<32; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if ($i == $destToDay): ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <select id="toYear" name="destToYear[]">
          <option value="0000"></option>
          <?php for($i=2000; $i<2031; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if($i == $destToYear) : ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <div id="toDateError" style="display:none;"></div>
        <div style="color: #3B5998; padding-top: 5px;">Optional</div>
        <?php $divCount++; ?>
      </div><!-- end divCount -->
      <?php }//end foreach ?>
    </div><!-- end destinations -->
    <?php endif; ?>
    <div id="destinationsError" style="display:none;"></div>

    <div style="margin: 20px 0 0 0">
    <?php if ($action == 'add') : ?>
      <input type="submit" class="inputbutton" style="padding: 2px 5px 1px 6px;" onclick="return validateForm(this)" value="Save" />
    <?php elseif ($action == 'edit') : ?>
      <input type="submit" class="inputbutton" style="padding: 2px 5px 1px 6px;" onclick="return validateForm(this)" value="Save" />
      <input type="hidden" name="travelItemID" value="<?php echo $travelItemID; ?>" />
    <?php endif; ?>
      <a style="padding: 2px 5px 1px 6px;" id="addDestLink" href="#addDest" class="inputbutton" onclick="addDest(); return false;">Add Second Destination</a>
      <script>
        var order = ['First', 'Second','Third','Fourth','Fifth','Sixth','Seventh','Eighth','Ninth', 'Tenth','Eleventh','Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth', 'Twenty First', 'Twenty Second', 'Twenty Third', 'Twenty Fourth', 'Twenty Fifth', 'Twenty Sixth', 'Twenty Seventh', 'Twenty Eight', 'Twenty Third', 'Thirtieth', 'Thirty First', 'Thirty Second'];

        // Convert numbers to words
        function numToWords(num2Convert) {
          var th = ['','Thousand','Million', 'Billion','Trillion'];
          var tha = ['','Thousandth','Millionth', 'Billionth','Trillionth'];

          var dg = ['Zero', 'One','Two','Three','Four', 'Five','Six','Seven','Eight','Nine'];
          var dga = ['Zero', 'First', 'Second','Third','Fourth','Fifth','Sixth','Seventh','Eighth','Ninth'];

          var tn = ['Ten','Eleven','Twelve','Thirteen', 'Fourteen','Fifteen','Sixteen', 'Seventeen','Eighteen','Nineteen'];
          var tna = ['Tenth','Eleventh','Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth'];

          var tw = ['Twenty','Thirty','Forty','Fifty', 'Sixty','Seventy','Eighty','Ninety'];
          var twa = ['Twentieth', 'Thirtieth', 'Fortieth', 'Fiftieth', 'Sixtieth', 'Seventieth', 'Eightieth', 'Ninetieth'];

          if (typeof num2Convert == 'number') num2Convert += '';

          var digitCount = num2Convert.length;
          if (digitCount > 15) return 'grabe ka';

          var digitsArray = num2Convert.split('');
          var words = '';
          var prefixTriad = false;

          for (var i = 0; i < digitCount; i++) {
            if ((digitCount-i)%3==2) {
              if (digitsArray[i] == '1') {
                words += i == digitCount - 2 ? tna[digitsArray[i+1]] : tn[digitsArray[i+1]] + ' ';
                i++;
                prefixTriad = true;
              } else if (digitsArray[i]!=0) {
                words += i==digitCount-2 && digitsArray[i+1] == '0' ? twa[digitsArray[i]-2] : tw[digitsArray[i]-2] + ' ';
                prefixTriad = true;
              }
            } else if (digitsArray[i]!=0) {
              words += i == digitCount - 1 ? dga[digitsArray[i]] : dg[digitsArray[i]] +' ';
              if ((digitCount-i)%3==0)
                words += i == digitCount - 3 && digitsArray[i+2] == '0' && digitsArray[i+1] == '0' ? 'Hundredth' : 'Hundred ';
              prefixTriad = true;
            }

            if ((digitCount-i)%3==1) {
              if (prefixTriad)
                //TODO: thousandths, millionths, etc.
                words += th[(digitCount-i-1)/3] + ' ';
              prefixTriad = false;
            }
          }

          return words.replace(/\s+/g,' ');
        }

        function setButtonLabel(count) {
          //var addDestButtonLabel = 'Add ' + order[count] + ' Destination';
          var addDestButtonLabel = 'Add ' + numToWords(count+1) + ' Destination';
          document.getElementById('addDestLink').setTextValue(addDestButtonLabel);
        }
        setButtonLabel(<?php echo $divCount; ?>);
      </script>
      <a style="padding: 2px 5px 1px 6px;" href="http://apps.facebook.com/travel-todo" >Back to My Travel Plans</a>
    </div>
  </div>
</form>

<fb:js-string var="ctryDD">
  <!-- COUNTRIES -->
  <h4 style="padding: 10px 0 5px 0; width: 305px; border-top: 1px solid #e6e6e6; margin-top: 15px;" id="lblDestination">Destination:</h4>
  <select id="country" style="padding: 0px;" name="countries[]">
    <option value=""></option>
  <?php foreach ($ctryList as $ctry) : ?>
    <option value="<?php echo $ctry->getCountryID(); ?>">
      <?php echo $ctry->getName(); ?>
    </option>
  <?php endforeach; ?>
  </select>
  <!-- REMOVE DESTINATIONS -->
  <a style="padding: 0 0 0 10px;" href="#rmvDest" onclick="rmvDest(this); return false;">Delete This Destination</a>
  <h4 style="padding: 10px 0 5px 0">Description:</h4>
  <!-- DETAILS -->
  <textarea style="margin: 0; width: 293px;" class="input_box" name="description[]"></textarea>
  <!-- FROM DATE -->
  <h4 style="padding: 10px 0 5px 0">Travel Start Date:</h4>
  <select id="fromMonth" name="destFromMonth[]"  onchange="updateEndDate(this);">
  <?php foreach ($monthArray as $index => $month) : ?>
    <option value="<?php echo $index ?>" <?php if ($index == $currentDate['mon']): ?> selected="selected"<?php endif; ?>>
      <?php echo $month; ?>
    </option>
  <?php endforeach; ?>
  </select>
  <select id="fromDay" name="destFromDay[]" onchange="updateEndDate(this);">
    <option value=""></option>
    <?php for($i=1; $i<32; $i++): ?>
    <option value="<?php echo $i ?>" <?php if ($i == $currentDate['mday']): ?> selected="selected"<?php endif; ?>>
      <?php echo $i; ?>
    </option>
    <?php endfor; ?>
  </select>
  <select id="fromYear" name="destFromYear[]" onchange="updateEndDate(this);">
    <option value=""></option>
    <?php for($i=2000; $i<2031; $i++): ?>
    <option value="<?php echo $i ?>" <?php if ($i == $currentDate['year']): ?> selected="selected"<?php endif; ?>>
      <?php echo $i; ?>
    </option>
    <?php endfor; ?>
  </select>
  <div id="fromDateError" style="display:none;"></div>
  <div style="color: #3B5998; padding-top: 5px; width: 330px">Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</div>
  <!-- TO DATE -->
  <h4 style="padding: 10px 0 5px 0">Travel End Date:</h4>
  <select id="toMonth" name="destToMonth[]">
  <?php foreach ($monthArray as $index => $month) : ?>
    <option value="<?php echo $index ?>">
      <?php echo $month; ?>
    </option>
  <?php endforeach; ?>
  </select>
  <select id="toDay" name="destToDay[]">
    <option value=""></option>
    <?php for($i=1; $i<32; $i++): ?>
    <option value="<?php echo $i; ?>">
      <?php echo $i; ?>
    </option>
    <?php endfor; ?>
  </select>
  <select id="toYear" name="destToYear[]">
    <option value=""></option>
    <?php for($i=2000; $i<2031; $i++): ?>
    <option value="<?php echo $i; ?>">
      <?php echo $i; ?>
    </option>
    <?php endfor; ?>
  </select>
  <div id="toDateError" style="display:none;"></div>
  <div style="color: #3B5998; padding-top: 5px;">Optional</div>
</fb:js-string>

<fb:js-string var="fromDateError">

</fb:js-string>

<script>
var count = <?php echo $divCount; ?>;

function addDest() {
  // EDIT: added workaround for setInnerFBML bug (known bug in facebook for selected browsers eg. Safari)
  // - ianne , 11/27/09
  // TODO: follow up on bug fix
  if(count == 1){
	var newCtryDD = document.createElement('div');
	newCtryDD.setStyle('margin', '0 0 2px 0');
	newCtryDD.setInnerFBML(ctryDD);
  }else{
    var newCtryDD = document.getElementById('1').cloneNode(true);	
  }
  newCtryDD.setId(count);

  count++;

  document.getElementById('destinations').appendChild(newCtryDD);

  var addDestButtonLabel = 'Add ' + numToWords(count + 1) + ' Destination';
  document.getElementById('addDestLink').setTextValue(addDestButtonLabel);
}

function rmvDest(obj) {
  //TODO: validateField(obj);
  document.getElementById('destinations').removeChild(obj.getParentNode());
  count--;

  var addDestButtonLabel = 'Add ' + numToWords(count + 1) + ' Destination';
  document.getElementById('addDestLink').setTextValue(addDestButtonLabel);
}

//TODO: this is dependent on the DOM structure which is very fragile
function updateEndDate(obj) {
  var id = obj.getId();
  var fromMonth, fromDay, fromYear, toMonth, toDay, toYear;
  if (id == 'fromMonth') {
    fromMonth = obj;
    fromDay = obj.getNextSibling();
    fromYear = obj.getNextSibling().getNextSibling();
  } else if (id == 'fromDay') {
    fromMonth = obj.getPreviousSibling();
    fromDay = obj;
    fromYear = obj.getNextSibling();
  } else if (id == 'fromYear') {
    fromMonth = obj.getPreviousSibling().getPreviousSibling();
    fromDay = obj.getPreviousSibling();
    fromYear = obj;
  }
  toMonth = fromYear.getNextSibling().getNextSibling().getNextSibling().getNextSibling();
  toDay = toMonth.getNextSibling();
  toYear = toDay.getNextSibling();

  toMonth.setSelectedIndex(fromMonth.getSelectedIndex());
  toDay.setSelectedIndex(fromDay.getSelectedIndex());
  toYear.setSelectedIndex(fromYear.getSelectedIndex());
}

function validateForm(obj) {
  var doSubmit = true;

  if (document.getElementById('title').getValue().length < 1) {
    document.getElementById('titleError').setTextValue('* The name of your itinerary cannot be left blank.').setStyle({color: 'brown', display: 'block'});
    doSubmit = false;
  } else {
    document.getElementById('titleError').setTextValue('').setStyle('display', 'none');
  }

  if (document.getElementById('details').getValue().length > 1000) {
    document.getElementById('detailsError').setTextValue('* Description cannot exceed more than 1000 characters.').setStyle({color: 'brown', display: 'block'});
    doSubmit = false;
  } else if (document.getElementById('details').getValue().length < 1) {
    document.getElementById('detailsError').setTextValue('* The description of your itinerary cannot be left blank.').setStyle({color: 'brown', display: 'block'});
    doSubmit = false;
  } else {
    document.getElementById('detailsError').setTextValue('').setStyle('display', 'none');
  }

  var slcBoxes = document.getElementById('destinations').getElementsByTagName('select');

  var fromMonth = [];
  var fromDay = [];
  var fromYear = [];
  var toMonth = [];
  var toDay = [];
  var toYear = [];
  var slcCountries = [];

  for (var i=0; i<slcBoxes.length; i++) {
    slcBoxId = slcBoxes[i].getId();
    if (slcBoxId == 'fromMonth') {
      fromMonth.push(slcBoxes[i]);
    } else if (slcBoxId == 'fromDay') {
      fromDay.push(slcBoxes[i]);
    } else if (slcBoxId == 'fromYear') {
      fromYear.push(slcBoxes[i]);
    } else if (slcBoxId == 'toMonth') {
      toMonth.push(slcBoxes[i]);
    } else if (slcBoxId == 'toDay') {
      toDay.push(slcBoxes[i]);
    } else if (slcBoxId == 'toYear') {
      toYear.push(slcBoxes[i]);
    } else if (slcBoxId == 'country') {
      slcCountries.push(slcBoxes[i]);
    }
  }

  var validFromDate, validToDate;
  var fm, fd, fd, fy, tm, td, ty;
  var fError, tError;

  for (i=0; i<count; i++) {
    validFromDate = false;
    validToDate = false;

    fm = fromMonth[i].getValue();
    fd = fromDay[i].getValue();
    fy = fromYear[i].getValue();
    tm = toMonth[i].getValue();
    td = toDay[i].getValue();
    ty = toYear[i].getValue();

    fError = fromYear[i].getNextSibling();
    tError = toYear[i].getNextSibling();

    //TODO: refactor!
    // FROM DATES
    if (fy == '0000' || fy == '') {
      if ((fm == '00' || fm == '0' || fm == '') && (fd == '00' || fd == '0' || fd == '')) {
        if (ty != '0000' && ty != '') {
          fError.setTextValue('* Missing start date').setStyle({color: 'brown', display: 'block'});
          doSubmit = false;
          var testDate = tm+' '+td+' '+ty;
          if (isDate(testDate, 'M d yyyy')) {
            tError.setTextValue('').setStyle('display', 'block');
            validToDate = true;
          }
//        } else {
//          fError.setTextValue('Debug').setStyle('display', 'block');
//          validToDate = true;
        }
      } else {
        fError.setTextValue('* Allowed travel dates are: month, day & year OR month & year OR year').setStyle({color: 'brown', display: 'block'});
        doSubmit = false;
      }

    } else if (fm != '00' && fm != '0' && fm != '') {
      if (fd != '00' && fd != '0' && fd != '') {//check date validity if all date fields are present
        var testDate = fm+' '+fd+' '+fy;
        if (isDate(testDate, 'M d yyyy')) {
          fError.setTextValue('').setStyle('display', 'block');
          validFromDate = true;
        } else {
          fError.setTextValue('* Allowed travel dates are: month, day & year OR month & year OR year').setStyle({color: 'brown', display: 'block'});
          doSubmit = false;
        }
      } else {//allow month-year combinations
        fError.setTextValue('').setStyle('display', 'block');
        validFromDate = true;
      }
    } else if ((fm == '00' || fm == '0' || fm == '') && (fd != '00' && fd != '0' && fd != '')) {
      fError.setTextValue('* Allowed travel dates are: month, day & year OR month & year OR year').setStyle({color: 'brown', display: 'block'});
      doSubmit = false;
    } else {
      fError.setTextValue('').setStyle('display', 'block');
      validFromDate = true;
    }

    // END DATES
    if (ty == '0000' || ty == '') {
      if ((tm == '00' || tm == '0' || tm == '') && (td == '00' || td == '0' || td == '')) {
        tError.setTextValue('').setStyle('display', 'block');
        validToDate = true;
      } else {
        tError.setTextValue('* Allowed travel dates are: month, day & year OR month & year OR year').setStyle({color: 'brown', display: 'block'});
        doSubmit = false;
      }
    } else if (tm != '00' && tm != '0' && tm != '') {
      if (td != '00' && td != '0' && td != '') {//check date validity if all date fields are present
        testDate = tm+' '+td+' '+ty;
        if (isDate(testDate, 'M d yyyy')) {
          // zero dates e.g. (0000-00-00) are considered valid; don't allow zero
          // from-dates paired with valid non-zero to-dates
          if ((fy == '0000' || fy == '') && (fm == '00' || fm == '0' || fm == '') && (fd == '00' || fd == '0' || fd == '')) {
            //this scenario is already covered on the from dates (?)
            //new Dialog().showMessage('Debug', 'TO date');
            fError.setTextValue('* Missing start date').setStyle({color: 'brown', display: 'block'});
            doSubmit = false;
          } else {
            tError.setTextValue('').setStyle('display', 'block');
            validToDate = true;
          }
        } else {
          tError.setTextValue('* Allowed travel dates are: month, day & year OR month & year OR year').setStyle({color: 'brown', display: 'block'});
          doSubmit = false;
        }
      } else {//allow month-year combinations
        tError.setTextValue('').setStyle('display', 'block');
        validToDate = true;
      }
    } else if ((tm == '00' || tm == '0' || tm == '') && (td != '00' && td != '0' && td != '')) {
      tError.setTextValue('* Allowed travel dates are: month, day & year OR month & year OR year').setStyle({color: 'brown', display: 'block'});
      doSubmit = false;
    } else {
      tError.setTextValue('').setStyle('display', 'block');
      validToDate = true;
    }

    if (validFromDate && validToDate) {

      validDates = compareDates(fy,ty,fm,tm,fd,td);

      if (validDates) {
        tError.setTextValue('').setStyle('display', 'block');
      } else if ((tm == '00' || tm == '0' || tm == '') && (td == '00' || td == '0' || td == '') && (ty == '0000' || ty == '')) {
        tError.setTextValue('').setStyle('display', 'block');
      } else {
        tError.setTextValue('* End date is earlier than Start date.').setStyle({color: 'brown', display: 'block'});
        doSubmit = false;
      }
    }
  }//end-for

  if (slcCountries.length == 1 && slcCountries[0].getValue().toString() == '') {
    document.getElementById('destinationsErrorSingle').setTextValue('* Destination is required.').setStyle({color: 'brown', display: 'block'});
    doSubmit = false;
  } else if (slcCountries.length == 1) {
    document.getElementById('destinationsError').setTextValue('').setStyle('display', 'none');
  } else {
    cloned = slcCountries.slice(0);
    duplicateCtry:
    for (var i=0; i<count; i++) {
      if (slcCountries[i]) {
//EDITS: Nov29-2007
        if (slcCountries[i].getValue() == '') {
          document.getElementById('destinationsError').setTextValue('* Please remove empty destinations.').setStyle({color: 'brown', display: 'block'});
          doSubmit = false;
          break;
        }
        for (var j=0; j<count; j++) {
//EDITS: Nov29-2007
          if (cloned[j]) {
            if ((slcCountries[i] == cloned[j]) && (i != j)) {
              document.getElementById('destinationsError').setTextValue('* Duplicate countries are not allowed').setStyle({color: 'brown', display: 'block'});
              doSubmit = false;
              break duplicateCtry;
            }
          }
        }
      }
      document.getElementById('destinationsError').setTextValue('').setStyle('display', 'none');
      document.getElementById('destinationsErrorSingle').setTextValue('').setStyle('display', 'none');      
    }
  }
	return doSubmit;
}

// TODO: reimplement
// Equal dates are considered valid as well as zero value dates (0000-00-00)
function compareDates(fromYear, toYear, fromMonth, toMonth, fromDay, toDay) {
  //filter out the illegal pair of dates
  if (fromYear > toYear) {
    return 0;
  } else if (fromYear == toYear) {
    if (parseInt(fromMonth) > parseInt(toMonth)) {
      return 0;
    } else if (fromMonth == toMonth) {
      if (parseInt(fromDay) > parseInt(toDay)) {
        return 0;
      }
    }
  }
  return 1;
}

function trim(stringToTrim) {
  return stringToTrim.replace(/^\s+|\s+$/g,"");
}

var MONTH_NAMES=['January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
var DAY_NAMES=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
function LZ(x) {return(x<0||x>9?"":"0")+x}

function isDate(val,format) {
  var date=getDateFromFormat(val,format);
  if (date==0) { return false; }
  return true;
}

function _isInteger(val) {
  var digits="1234567890";
  for (var i=0; i < val.length; i++) {
    if (digits.indexOf(val.charAt(i))==-1) { return false; }
    }
  return true;
  }
function _getInt(str,i,minlength,maxlength) {
  for (var x=maxlength; x>=minlength; x--) {
    var token=str.substring(i,i+x);
    if (token.length < minlength) { return null; }
    if (_isInteger(token)) { return token; }
  }
  return null;
}

function getDateFromFormat(val,format) {
  val=val+"";
  format=format+"";
  var i_val=0;
  var i_format=0;
  var c="";
  var token="";
  var token2="";
  var x,y;
  var now=new Date();
  var year=now.getYear();
  var month=now.getMonth()+1;
  var date=1;
  var hh=now.getHours();
  var mm=now.getMinutes();
  var ss=now.getSeconds();
  var ampm="";

  while (i_format < format.length) {
    // Get next token from format string
    c=format.charAt(i_format);
    token="";
    while ((format.charAt(i_format)==c) && (i_format < format.length)) {
      token += format.charAt(i_format++);
      }
    // Extract contents of value based on format token
    if (token=="yyyy" || token=="yy" || token=="y") {
      if (token=="yyyy") { x=4;y=4; }
      if (token=="yy")   { x=2;y=2; }
      if (token=="y")    { x=2;y=4; }
      year=_getInt(val,i_val,x,y);
      if (year==null) { return 0; }
      i_val += year.length;
      if (year.length==2) {
        if (year > 70) { year=1900+(year-0); }
        else { year=2000+(year-0); }
      }
    } else if (token=="MMM"||token=="NNN"){
      month=0;
      for (var i=0; i<MONTH_NAMES.length; i++) {
        var month_name=MONTH_NAMES[i];
        if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
          if (token=="MMM"||(token=="NNN"&&i>11)) {
            month=i+1;
            if (month>12) { month -= 12; }
            i_val += month_name.length;
            break;
            }
          }
        }
      if ((month < 1)||(month>12)){return 0;}
      }
    else if (token=="EE"||token=="E"){
      for (var i=0; i<DAY_NAMES.length; i++) {
        var day_name=DAY_NAMES[i];
        if (val.substring(i_val,i_val+day_name.length).toLowerCase()==day_name.toLowerCase()) {
          i_val += day_name.length;
          break;
          }
        }
      }
    else if (token=="MM"||token=="M") {
      month=_getInt(val,i_val,token.length,2);
      if(month==null||(month<1)||(month>12)){return 0;}
      i_val+=month.length;}
    else if (token=="dd"||token=="d") {
      date=_getInt(val,i_val,token.length,2);
      if(date==null||(date<1)||(date>31)){return 0;}
      i_val+=date.length;}
    else if (token=="hh"||token=="h") {
      hh=_getInt(val,i_val,token.length,2);
      if(hh==null||(hh<1)||(hh>12)){return 0;}
      i_val+=hh.length;}
    else if (token=="HH"||token=="H") {
      hh=_getInt(val,i_val,token.length,2);
      if(hh==null||(hh<0)||(hh>23)){return 0;}
      i_val+=hh.length;}
    else if (token=="KK"||token=="K") {
      hh=_getInt(val,i_val,token.length,2);
      if(hh==null||(hh<0)||(hh>11)){return 0;}
      i_val+=hh.length;}
    else if (token=="kk"||token=="k") {
      hh=_getInt(val,i_val,token.length,2);
      if(hh==null||(hh<1)||(hh>24)){return 0;}
      i_val+=hh.length;hh--;}
    else if (token=="mm"||token=="m") {
      mm=_getInt(val,i_val,token.length,2);
      if(mm==null||(mm<0)||(mm>59)){return 0;}
      i_val+=mm.length;}
    else if (token=="ss"||token=="s") {
      ss=_getInt(val,i_val,token.length,2);
      if(ss==null||(ss<0)||(ss>59)){return 0;}
      i_val+=ss.length;}
    else if (token=="a") {
      if (val.substring(i_val,i_val+2).toLowerCase()=="am") {ampm="AM";}
      else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {ampm="PM";}
      else {return 0;}
      i_val+=2;}
    else {
      if (val.substring(i_val,i_val+token.length)!=token) {return 0;}
      else {i_val+=token.length;}
      }
    }
  // If there are any trailing characters left in the value, it doesn't match
  if (i_val != val.length) { return 0; }
  // Is date valid for month?
  if (month==2) {
    // Check for leap year
    if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { // leap year
      if (date > 29){ return 0; }
      }
    else { if (date > 28) { return 0; } }
    }
  if ((month==4)||(month==6)||(month==9)||(month==11)) {
    if (date > 30) { return 0; }
    }
  // Correct hours value
  if (hh<12 && ampm=="PM") { hh=hh-0+12; }
  else if (hh>11 && ampm=="AM") { hh-=12; }
  var newdate=new Date(year,month-1,date,hh,mm,ss);
  return newdate.getTime();
}

</script>