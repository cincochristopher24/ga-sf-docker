<?php require_once('travellog/views/mytravelplans/facebook/tpl.menu.php'); ?>
<div class="box">

  <?php if($pastTravelsCount == $totalNumItems && $pastTravelsCount): ?>
  	<p class="box fontMed fontColor1"><strong>You currently have <span id="travelItemCount"><?php echo $pastTravelsCount; ?></span> Past <?php echo ($totalNumItems == 1) ? 'Travel' : 'Travels';?> </strong></p>
  <?php elseif($pastTravelsCount && $totalNumItems || !$pastTravelsCount): ?>
  	<p class="box fontMed fontColor1"><strong>You currently have <span id="travelItemCount"><?php echo $totalNumItems; ?></span> Travel <?php echo ($totalNumItems == 1) ? 'Plan' : 'Plans';?>

  	<?php if ($pastTravelsCount) :?> 
		and 
		<span id="pastTravelsCount">
			<?php echo $pastTravelsCount; ?> 
			Past <?php echo $pastTravelsCount == 1 ? 'Travel' : 'Travels'; ?>
		</span> 
	<?php endif;?>

  	</strong></p>
  <?php endif; ?>

  <div class="box helpText"> 
    You have currently set to display <span id="settingsCount"><?php echo $itemsToDisplaySetting; ?> travel <?php echo ($itemsToDisplaySetting == 1) ? 'plan' : 'plans';?></span> as default, please note that they are ordered in terms of time proximity. 
  </div	>

  <div class="box">	  
    <form action="settings.php" method="post">
      <div id="mtpSettings" style="display:block;">
        Display:<input id="inputSettings" name="inputSettings" type="text" value="<?php echo $itemsToDisplaySetting; ?>" style="width: 30px;" onkeypress="return numbersOnly(event)" maxlength="2" />
        <span id="FixThisHack">
          <input id="inputPast" name="inputPast" type="checkbox" value='off' <?php if ($includePastTravelsSetting) echo 'checked="true"' ?> />Include past travels&nbsp;
        </span>
      </div>  
  	  <p> <input type="submit" class="inputbutton" name='saveSettings' value="Save Settings"/> </p>
    </form>
  </div>	  
</div>  
