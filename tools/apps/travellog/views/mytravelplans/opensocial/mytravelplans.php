
<?php if($appViewType == 'preview'): ?>
	<!--preview section-->
	<div>
		<img src='http://goabroad.net/opensocial/mytravelplans/images/mtp_screenshot.gif' width="550px" align="List down your travel plans and find out who could be traveling your way!`
		List your past travels-- show how well traveled you are and help other 
		travelers who need tips about traveling to the places you've been to!" border="0" align="left"/>
	</div>
<?php else : ?>
	<!--canvas section-->
	<div id="ganet_app_container" class="box overflow">

	<?php if ($isOwner && $appViewType == 'canvas') : ?>
	    <a id="add_travel" href="javascript: void(0);" onclick="showAddForm()" class="right button">
	    	<img id="add_img" src="<?php echo $imgBaseUrl;?>add.jpg" border="0"/>	
	      	<strong>add a travel itinerary</strong>
	    </a>
	<?php endif; ?>

	<?php if(count($travelItems)) : 
	  $i = 0; $travelClass = 'clear box marginBottom10';
	  foreach ($travelItems as $item) :
	    $travelWishID = $item->getTravelWishID();
	    $title = htmlentities($item->getTitle(), ENT_QUOTES);
	    $details = $item->getDetails();
	    $tripDateFrom = $item->getDateFrom();
	    $tripDateTo = $item->getDateTo();
	    $tripDateRange = $item->getDateRange();
	    $destinations = $item->getDestinations();

		if($includePastTravelsSetting && $numPresentTravels == $i){ ?>
	  		<p><strong class="fontColor1"><em>PAST TRAVELS</em></strong></p>
		<?php $includePastTravelsSetting = false; $travelClass = 'pastTravel';  } ?>

		  <div id="travel_entry_wrapper" class="<?php echo $travelClass ?>" >
		    <div id="top_level" >
		      <!-- DATE-RANGE -->
		      <div class="date_section">
		        <p class="left paddingTopBottom10 fontColor4"><?php echo $tripDateRange; ?></p>
		        <div class="right paddingTopBottom10">

		          <?php if ($isOwner && $appViewType == 'canvas') : ?>

			          <!-- EDIT LINK -->
			          <a href="#headerLogo" onclick="showEditForm(<?php echo $travelWishID;?>)" title="Edit">
			            <img src="<?php echo $imgBaseUrl;?>edit.gif" alt="[Edit]" border="0"/>
			          </a>

			          <!-- DELETE LINK -->
			          <a href="#headerLogo" onclick="deleteTravelItinerary(<?php echo $travelWishID;?>, '<?php echo htmlentities($title, ENT_QUOTES);?>')" title="Delete">
			            <img src="<?php echo $imgBaseUrl;?>delete.gif" alt="[Delete]" border="0"/>
			          </a>

		          <?php endif; ?>
		        </div><!-- end actions -->
		      </div><!-- end date_section -->
		      <!-- TITLE -->
		      <h3 class="fontSuper clear fontColor3"><?php echo $title; ?></h3>
		      <!-- DETAILS -->
		      <p><?php echo htmlentities($details, ENT_QUOTES); ?></p>
		    </div><!-- end top_level -->

		    <div id="destinations_container">
		    <?php 
		      foreach ($destinations as $d) { 
		        $travDestID = $d->getTravelWishToCountryID();
		        $destTravItemID = $d->getTravelWishID();
		        $destCountry = $d->getCountry()->getName();
		        $destCountryID = $d->getCountry()->getCountryID();
		        $destDateFrom = $d->getStartDate();
		        $destDateTo = $d->getEndDate();
		        $destDateRange = $d->getDateRange();
		        $destDescription = $d->getDescription();				
        		$showFriendsLink = in_array($destCountryID,$friendsCountryIDs) ? true : false;

		    ?>
		      <div id="rmvDest" class="spBox padding10 marginTop10">
		        <div class="sublevel_destination clear">
		          <!-- COUNTRY -->
		          <div class="left"><img class="bullet" src="<?php echo $imgBaseUrl;?>bullet.gif" style="margin-right:5px;"/><strong ><?php echo $destCountry;?></strong></div>
		        <?php if ($isOwner && $appViewType == 'canvas') : ?>
		          <?php if (count($destinations) != 1 ) : ?>
				          <a id="dest<?php echo $travDestID;?>" href="#headerLogo" title="Remove destination" onclick="deleteTravelDestination(<?php echo $destTravItemID;?>, <?php echo $destCountryID;?>, '<?php echo $destCountry;?>')">
				            <img class="delete_desc" src="<?php echo $imgBaseUrl;?>delete_dest.jpg" alt="delete" border="0">
				          </a>
		          <?php endif; ?>
		        <?php endif; ?>
		          <!-- DATE RANGE -->
		          <div class="right fontColor4"><?php echo $destDateRange; ?></div>
		        </div><!-- end sublevel_destination -->
		        <div class="clear"></div>
		        <!-- DESTINATION DETAILS -->
		        <p class="destination_desc"><?php echo $destDescription; ?></p>
		        <?php if ($showFriendsLink && $numPresentTravels >= $i && $isOwner) :?>
		        <a style="float: right;font-size:10px; font-weight:700; margin-left:2px; margin-right:5px; text-decoration:underline; white-space:nowrap;" href="javascript: void(0);" onclick="showDestinations(<?php echo $destCountryID ?>)" title="Friends with same destination">See who else is going</a>
		        <?php endif; ?>
		        <div class="clear"></div>
		      </div><!-- end rmvDest -->
		    <?php }//end destinations loop ?>
		    </div><!-- end destinations_container -->
		    <div class="clear"></div>
		  </div><!-- end travel_entry_wrapper -->
		  <?php	$i++; 

		if('canvas' != $appViewType){ if(3 == $i) break;  } 
		if($itemsToDisplaySetting == $i) break;		

	  endforeach; 

	elseif(count($travelItems)==0) : $i=0; ?>

	  <p class="clear" style="padding:15px">	
	    You haven't listed your travels yet.
	    List down your travel plans and find out who could be traveling your way!
	    List down your past travels - show how well traveled you are and help travelers 
		who need tips about traveling to the places	you've been to!
	  </p> 

	   <?php if($isOwner && $appViewType == 'profile') : ?>
	   	<a id='profileLink' class='button right' href='javascript: void(0);' onclick='gotoCanvas()'>Add Your Travel Plans Now!</a>
	   <?php endif; ?>

	<?php endif; ?> 

	</div><!-- end div ganet_app_container -->
	
	<?php if($showWishList) : ?>
	  <!-- WISHLIST -->		
		<div class="box" style='padding-bottom:0px;'>

		  <div id="wish" class="padding10 borderGray bColor1">
		    <p><strong>Travel Wishlist</strong> (<?php  echo count($wishList['countryNames']); ?>): <?php echo implode(', ',$wishList['countryNames']) ?></p>
		  </div>

		<?php if ($isOwner && $appViewType == 'canvas') : ?>
		  <p id="add_travel" style="padding-bottom:10px;text-align:right;">
		    <a id="editLink" class="destination_desc" href="javascript: void(0);" onclick="showForm(); return false;">
		      <strong>Edit Country List</strong>
		    </a>
		  </p>
		<?php endif; ?>
		  <form style="display: none;" id="editForm" name="editForm" action="" method="GET">
		    <div style="border: 1px solid #e6e6e6; margin: 10px 0 0 10px; height: 350px; overflow: auto;">
		      <input type="hidden" name="user" value="<?php echo $user; ?>"/>
		    <?php foreach ($countryList as $ctry) {
		      $id=$ctry->getCountryID()
		    ?>
		      <input type="checkbox"
		             name="wishList"
		             value="<?php echo $id; ?>"
		             id="c<?php echo $id; ?>"
		         <?php
				 for($i=0; $i<count($wishList['countryIDs']); $i++) {
		           if ($id == $wishList['countryIDs'][$i] ) { ?>
		             checked="checked"
		         <?php
		             break;
		           }//end if
		         }//end foreach
		         ?>
		      ><label for="c<?php echo $id; ?>"><?php echo $ctry->getName(); ?></label><br/>
		    <?php } ?>
		    </div>
		    <div style="padding: 10px 0 10px 0; margin: 0 0 0 5px;">
		      <a style="padding: 1px 5px 1px 5px;" href="javascript: void(0);" onclick="editTravelWishList()">Save</a>
		      <a class="inputbutton" style="padding: 1px 5px 1px 5px;" href="javascript: void(0);" onclick="hideForm()"; return false;>Cancel</a>
		    </div>
		  </form>
		</div>
	  <!--END OF WISHLIST -->		
	<?php endif; ?> 

<?php endif; ?>
