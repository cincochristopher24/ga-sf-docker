<div class="box">
	<p class="box fontMed fontColor1">
		<strong>You currently have 
		<?php if($onGoingTravelsCount): ?>
			<span id="travelItemCount"><?php echo $onGoingTravelsCount; ?></span> Travel <?php echo ($onGoingTravelsCount > 1) ? 'Plans' : 'Plan';?>
		<?php endif; ?>
		<?php if($onGoingTravelsCount && $pastTravelsCount) echo 'and'; ?> 
		<?php if ($pastTravelsCount) :?> 
			<span id="pastTravelsCount"><?php echo $pastTravelsCount; ?> Past <?php echo $pastTravelsCount > 1 ? 'Travels' : 'Travel'; ?></span>
		<?php endif; ?>
		</strong>
   </p>

  <div class="box helpText"> 
    You have currently set to display <span id="settingsCount"><?php echo $itemsToDisplaySetting; ?> travel <?php echo ($itemsToDisplaySetting == 1) ? 'plan' : 'plans';?></span> as default, please note that they are ordered in terms of time proximity. 
  </div	>

  <?php if($isOwner && $appViewType == 'canvas') : ?>
	<div class="box">	  
	  <form id="mtpSettingsForm" name="mtpSettingsForm" action="widget.php" method="post">
	    <div id="mtpSettings" style="display:block;">
	      Display:<input id="inputSettings" name="inputSettings" type="text" value="<?php echo $itemsToDisplaySetting; ?>" style="width: 30px;" onkeypress="return numbersOnly(event)" maxlength="2" <?php if($appViewType != 'canvas') echo "disabled='true'"; ?> />
	      <span id="FixThisHack">
	        <input id="inputPast" name="inputPast" class="input_box" type="checkbox" <?php if ($includePastTravelsSetting) echo 'checked="true"' ?> <?php if($appViewType != 'canvas') echo "disabled='true'"; ?> />Include past travels&nbsp;
	      </span>
	    </div>  
	  </form>
	</div>	  
	  <a id="settingsLink" href="javascript:void(0);" onClick="saveSettings()" class="button fontMed">Save Settings</a>
  <?php elseif($appViewType == 'home'): ?>
	  <a href='javascript:void(0);' style='font-weight:bold' onclick='gotoCanvas()' class="button fontMed">Edit Settings Now!</a>
  <?php endif; ?>

</div>  
