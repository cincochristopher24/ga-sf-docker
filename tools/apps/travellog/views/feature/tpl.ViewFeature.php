<!--WHAT'S NEW PAGE redesigned by Nick on September 16, 2009-->

<div id="intro_container">
		<div>
			<h1 id="whatsnew_h1">Welcome to the all-new GoAbroad Network!</h1>
			<p id="whatsnew_p">We have updated our look and have added cool new features to GoAbroad Network.</p>
		</div>
</div>



<div id="content_wrapper">	
	<div class="section">
		<ul class="lineModel">			
			<li>
				<img src="/images/whatsnew_v2/one.jpg" />
        		<p id="one"><span class="sub_head">Improved Photo Section</span><br/>
        		We have an all-new photo uploading tool that lets you add photos so much faster than before! Now you can upload up to 60 photos at a time in one easy step.</p>      
    		</li>

	    	<li>
				<img src="/images/whatsnew_v2/two.jpg" />
        		<p id="two"><span class="sub_head">Bring your Journals and Traveler Profile to your Facebook Profile!</span><br/>
        		We have four Facebook applications that let you bring the traveler in you to your Facebook profile. These apps let you sync your journals, travel map and travel plans with your Facebook page. All you need to do is add our apps on Facebook and sync them with your GoAbroad Network profile. Whenever you make a change on one site or the other, the change reflects on both websites.</p>      
    	    </li>

	    	<li>
				<img src="/images/whatsnew_v2/three.jpg" />
        		<p id="three"><span class="sub_head">Add Videos to your Journals!</span><br/>
        		You may now embed your Google Video or YouTube videos in your journal entries. Simply create a journal entry, or go to one of your existing journals, and click on Add a Video.</p>      
    	    </li>
	   
	    	<li>
				<img src="/images/whatsnew_v2/four.jpg" />
        		<p id="four"><span class="sub_head">Send your Journal Entries as Postcards!</span><br/>
        		Let everyone know that you have a new journal entry by sending an e-postcard! After you're done with your new entry, we let you pick a photo, write a message, and we will then send it along to your family and friends.</p>      
    	    </li>

	    	<li>
				<img src="/images/whatsnew_v2/five.jpg" />
        		<p id="five"><span class="sub_head">Enhanced Journal Page</span><br/>
        		We have enhanced the journal page and gave it a new and improved format. With a bigger primary photo, an improved photo gallery and a generally cleaner look, the new format makes your journal entry even more enticing and easier to read!</p>      
    	    </li>

	    	<li class="EndZone">
				<img src="/images/whatsnew_v2/six.jpg" />
        		<p id="six"><span class="sub_head">Discussion Board</span><br/>
        		Interact with the travelers on your groups or clubs! Post topics for discussion or join discussions about your common interests. </p>      
    	    </li>
    	  </ul>  
</div>
