<div id="stepmaincontainer" class="section">

    <div class="content">
  
		<div class="box2">
			<p class="title">Thank you for registering on <span><?php echo $siteName ?></span></p>
			<p class="info">Now the fun begins! Here is how we suggest you proceed with your new Travel Journal adventures.</p>
		</div>
    
		<div id="stepsubcontainer">
			<div class="jcarousel-skin-tango jcarousel-container jcarousel-container-horizontal" style="display: block;">
				<div class="jcarousel-prev jcarousel-prev-horizontal jcarousel-prev-disabled jcarousel-prev-disabled-horizontal" style="display:block"></div>
				<div class="jcarousel-next jcarousel-next-horizontal" style="display: block"></div>
				<div class="jcarousel-clip jcarousel-clip-horizontal">
					<ul id="steps_carousel" class="jcarousel-list jcarousel-list-horizontal" style="overflow: hidden; width: 1110px; left: 0pt; display: block;">
						<li id="one">
							<p class="step_title">
								<span class="number"></span>
								<a href="/edittravelerprofile.php">Complete Your Profile</a>
							</p>
							<p class="step_detail">Give your Journal Entries a personality and open up opportunities to meet like minded travelers!</p>
						</li>
						<li id="two">
							<p class="step_title">
								<span class="number"></span>
								<a href="/addressbookmanagement.php?action=add">Create Your Address Book</a>
							</p>
							<p class="step_detail">Create your address book. Adding contact info of your friends and family will keep them updated whenever you post new Journal Entries and photos.</p>
						</li>
						<li id="three">
							<p class="step_title">	
								<span class="number"></span>
								<a href="/travel.php?action=add">Create Your Journals</a>
							</p>
							<p class="step_detail">Create a Journal Title and a short Description, Add a Journal Entry and upload Travel Photos.</p>
						</li>
						<li id="four">
							<p class="step_title">	
								<span class="number"></span>
								<a href="/group.php">Join and Create Groups</a>
							</p>
							<p class="step_detail">Join a group, start your own club or share your travel experiences while searching for answers to your questions. Get involved in the network!</p>
						</li>
						<li id="five">
							<p class="step_title">
								<span class="number"></span>
								<a href="/widget.php?travelerID=<?php echo $travelerID; ?>">Write Your Travel Bio</a>
							</p>
							<p class="step_detail">Share your most memorable travel experiences by answering quick, fun questions about your travels!</p>
						</li>
						<li id="six">
							<p class="step_title">
								<span class="number"></span>
                                <a href="/widget.php?travelerID=<?php echo $travelerID; ?>">Share Your Travel Plans</a> 
							</p>
							<p class="step_detail">Let others know where you've been, where you are, where you're going and what you're up to. List your travel plans and find out who could be traveling your way! </p>
						</li>
                        <li id="seven">
                            <p class="step_title">
                                <span class="number"></span>
                                <a href="/widget.php?travelerID=<?php echo $travelerID; ?>">Put Your Travels On the Map</a> 
                            </p>
                            <p class="step_detail">Point out the countries you've been to and show your travel map on your profile. If you have travel journals, this app will show how many journals, photos and tips you have for every country.</p>
                        </li>            
                    </ul>
				</div>
			</div>
		</div>
    
	</div>
  
</div>	
