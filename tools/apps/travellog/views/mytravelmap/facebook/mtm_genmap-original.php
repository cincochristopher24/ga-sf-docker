<?php
$countryList = $model['countryList'];
$mapEntries = $model['mapEntries'];
$entriesCount = count($mapEntries);
$user = $model['user'];

function parseToXML($htmlStr) {
  $xmlStr=str_replace('<','&lt;',$htmlStr);
  $xmlStr=str_replace('>','&gt;',$xmlStr);
  $xmlStr=str_replace('"','&quot;',$xmlStr);
  $xmlStr=str_replace("'",'&#39;',$xmlStr);
  $xmlStr=str_replace("&",'&amp;',$xmlStr);
  return $xmlStr;
}

?>

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>My Travel Map</title>

    <link type="text/facebook/mytravelmap/css" rel="Stylesheet" media="screen" href="http://www.goabroad.net/facebook/mytravelmap/css/travelMapBubble.css"/>

    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAADN6t8sswaCRGbhKv8-BC0xTfWdpSMA2WXJD7WFd_XyKADP-mYxSQwLFLL4KN4RJUZRLKVgX7vxapkg" type="text/javascript"></script>
    <script type="text/javascript">

    //<![CDATA[

    var iconBlue = new GIcon();
    iconBlue.image = 'http://www.goabroad.net/facebook/mytravelmap/images/pin_blue_20.gif';
    iconBlue.shadow = 'http://www.goabroad.net/facebook/mytravelmap/images/pin_shadow.gif';
    iconBlue.iconSize = new GSize(12, 20);
    iconBlue.shadowSize = new GSize(22, 20);
    iconBlue.iconAnchor = new GPoint(6, 20);
    iconBlue.infoWindowAnchor = new GPoint(5, 1);

    var iconRed = new GIcon();
    iconRed.image = 'http://www.goabroad.net/facebook/facebook/mytravelmap/images/corners/ga_markerTransparent.png';
    iconRed.shadow = 'http://www.goabroad.net/facebook/facebook/mytravelmap/images/corners/ga_marker_shadowTransparent.png';
    iconRed.iconSize = new GSize(12, 20);
    iconRed.shadowSize = new GSize(22, 20);
    iconRed.iconAnchor = new GPoint(6, 20);
    iconRed.infoWindowAnchor = new GPoint(5, 1);

    var customIcons = [];
    customIcons["withJournal"] = iconRed;
    customIcons["noJournal"] = iconBlue;

    function load() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.setCenter(new GLatLng(0, 0), 1);

      <?php foreach ($mapEntries as $entry) : ?>
        var point = new GLatLng(parseFloat(<?php echo $entry->getCountry()->getLatitude(); ?>),
                                parseFloat(<?php echo $entry->getCountry()->getLongitude(); ?>));
        var marker =
          createMarker(point,
                      '<?php echo parseToXML($entry->getCountry()->getName()); ?>',
                      '<?php echo $entry->getPhoto() ? $entry->getPhoto() : ''; ?>',
                      parseInt(<?php echo $entry->getJournalEntriesCount() ? $entry->getJournalEntriesCount() : 0; ?>),
                      parseInt(<?php echo $entry->getPhotosCount() ? $entry->getPhotosCount() : 0; ?>),
                      parseInt(<?php echo $entry->getTipsCount() ? $entry->getTipsCount() : 0; ?>),
                      'withJournal');
        map.addOverlay(marker);
      <?php endforeach; ?>
      }
    }

    function createMarker(point, country, photoUrl, journals, photos, tips, type) {

      var marker = new GMarker(point, {title: country, icon: customIcons[type], clickable: journals + photos + tips});

      var myHtml = '<div id="bubble_details_container"';
      myHtml += photos ? ' style="height: 75px;"><img id="photo" src="' + photoUrl + '" />' : '>';
      myHtml += '<div id="bubble_details">';
      myHtml += '<h2>' + country + '</h2>';
      if (journals) myHtml += '<p><span>' + journals + '</span>' + (journals==1 ? ' Journal Entry</p>' : ' Journal Entries</p>');
      if (photos) myHtml += '<p><span>' + photos + '</span>' + (photos==1 ? ' Travel Photo</p>' : ' Travel Photos</p>');
      if (tips) myHtml += '<p><span>' + tips + '</span>' + (tips==1 ? ' Travel Tip</p>' : ' Travel Tips</p>');
      myHtml += '<div style="clear: both;"></div></div></div>';

      GEvent.addListener(marker, 'click', function() {
        document.getElementById('country_info').innerHTML = myHtml;
      });

      return marker;
    }

    //]]>
    </script>
  </head>
  <body onload="load()" onunload="GUnload()">
    <div id="map" style="width: 510px; height: 480px"></div>
    <div id="country_info">Ctry Info</div>
  </body>
</html>