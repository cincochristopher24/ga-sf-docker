<?php
$ganetEmail =$model['email'];
$ganetUsername =$model['ganetUsername'];
$countryList = $model['countryList'];
$mapEntries = $model['mapEntries'];
$entriesCount = count($mapEntries);
$isSync = $model['isSync'];
$user = $model['user'];
$isOwnerUser =$model['isOwnerUser'];
$fbFirstName =isset($_GET['fbFirstName']) ? $_GET['fbFirstName'] : $model['fbFirstName'];

$traveledName = $isOwnerUser ? "your" : "$fbFirstName's";
?>

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>My Travel Map</title>

    <link type="text/css" rel="Stylesheet" media="screen" href="http://www.goabroad.net/facebook/mytravelmap/css/travelMapBubble.css"/>
    <link type="text/css" rel="Stylesheet" media="screen" href="http://www.goabroad.net/facebook/mytravelmap/css/mytravelmap.css"/>

    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAADN6t8sswaCRGbhKv8-BC0xTfWdpSMA2WXJD7WFd_XyKADP-mYxSQwLFLL4KN4RJUZRLKVgX7vxapkg" type="text/javascript"></script>
    <script type="text/javascript">

    //<![CDATA[

    var iconBlue = new GIcon();
    iconBlue.image = 'http://www.goabroad.net/facebook/mytravelmap/images/pin_blue_20.gif';
    iconBlue.shadow = 'http://www.goabroad.net/facebook/mytravelmap/images/pin_shadow.gif';
    iconBlue.iconSize = new GSize(12, 20);
    iconBlue.shadowSize = new GSize(22, 20);
    iconBlue.iconAnchor = new GPoint(6, 20);
    iconBlue.infoWindowAnchor = new GPoint(5, 1);

    var iconRed = new GIcon();
    iconRed.image = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_markerTransparent.png';
    iconRed.shadow = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_marker_shadowTransparent.png';
    iconRed.iconSize = new GSize(12, 20);
    iconRed.shadowSize = new GSize(22, 20);
    iconRed.iconAnchor = new GPoint(6, 20);
    iconRed.infoWindowAnchor = new GPoint(5, 1);

    var customIcons = [];
    customIcons["withJournal"] = iconRed;
    customIcons["noJournal"] = iconBlue;



    function load() {
      	if (GBrowserIsCompatible()) {
        	var map = new GMap2(document.getElementById("map"));
        	map.addControl(new GSmallMapControl());
        	map.setCenter(new GLatLng(0, 0), 1);

      		<?php 
        	$showHelpText = false;  
        	$entryHelper = new EntryHelper;

        	foreach ($mapEntries as $entry) {
          		if (!$showHelpText) {
          			$showHelpText = 
          	  			$entry->getJournalEntriesCount() ||
          	  			$entry->getPhotosCount() ||
          	  			$entry->getTipsCount(); 
          		}	    
		  		$link = '';
		  		if ($entry->getTravelLogID()) {
		  			$travelLog = new TravelLog($entry->getTravelLogID());
		  			$link = 'http://www.goabroad.net'.$entryHelper->getLink($travelLog);
		  		}
				?>
        		var point = new GLatLng(parseFloat(<?php echo $entry->getCountry()->getLatitude(); ?>),
                                parseFloat(<?php echo $entry->getCountry()->getLongitude(); ?>));
				var marker =
          		createMarker(point,
                      '<?php echo parseToXML($entry->getCountry()->getName()); ?>',
                      '<?php echo $entry->getPhoto() ? $entry->getPhoto() : ''; ?>',
                      parseInt(<?php echo $entry->getJournalEntriesCount() ? $entry->getJournalEntriesCount() : 0; ?>),
                      parseInt(<?php echo $entry->getPhotosCount() ? $entry->getPhotosCount() : 0; ?>),
                      parseInt(<?php echo $entry->getTipsCount() ? $entry->getTipsCount() : 0; ?>),
                      'withJournal', '<?php echo $link ?>');

            	map.addOverlay(marker);

      <?php } ?>
      	}
    }

    function createMarker(point, country, photoUrl, journals, photos, tips, type, link) {

      	var marker = new GMarker(point, {title: country, icon: customIcons[type], clickable: journals + photos + tips});
	  	var imgElement = '';
		var countryElement = '<h2>' + country + '</h2>';
      	var myHtml = '<div id="bubble_details_container">';
	  	if (photos) {
			if (link != '') {
				imgElement = '<a href="'+link+'" target="_blank"><img style="border: 1px solid #e6e6e6; float: left; margin-right: 10px" id="photo" src="' + photoUrl + '" /></a>';
				countryElement = '<a href="'+link+'" target="_blank"><h2>' + country + '</h2></a>';
			} else {
                                countryElement = '<h2>' + country + '</h2>';
				imgElement = '<img style="border: 1px solid #e6e6e6; float: left; margin-right: 10px" id="photo" src="' + photoUrl + '" />';
			}
	  	}
	  	myHtml += imgElement + '</div>';
//      myHtml += photos ? '<img style="border: 1px solid #e6e6e6; float: left; margin-right: 10px" id="photo" src="' + photoUrl + '" />' : '';

		myHtml += '<div id="bubble_details" style="float: left">';
		myHtml += countryElement;
		if (journals) myHtml += '<p><span>' + journals + '</span>' + (journals==1 ? ' Journal Entry</p>' : ' Journal Entries</p>');
		if (photos) myHtml += '<p><span>' + photos + '</span>' + (photos==1 ? ' Travel Photo</p>' : ' Travel Photos</p>');
		if (tips) myHtml += '<p><span>' + tips + '</span>' + (tips==1 ? ' Travel Tip</p>' : ' Travel Tips</p>');
		myHtml += '<div style="clear: both;"></div></div><div class="clear"></div></div>';

		GEvent.addListener(marker, 'click', function() {
		  	document.getElementById('country_info').innerHTML = myHtml;
		});

		return marker;
    }

   //]]>
    </script>
  </head>

  <body onload="load()" onunload="GUnload()">
    <div id="map" style="width: 500px; height: 480px; margin: 0 auto"></div>
    <?php if ($isSync && $entriesCount && $showHelpText) : ?>                                             
    <div id="country_info" style="border: 1px solid #e6e6e6; width: 492px; padding: 5px"><p id="helptext">Click on the blue balloon to view <?=$traveledName?> travel journals for that country.</p></div>
    <?php endif; ?>
  </body>
</html>
