<?php
$mapEntries = $model['mapEntries'];
$entriesCount = count($mapEntries);

$i=0;
$countries = '';
foreach ($mapEntries as $entry) {
  $i++;
  $countries .= $entriesCount == $i ? $entry->getCountry()->getName() : $entry->getCountry()->getName().', ';
}

$htmlFragment = <<< HereDoc
<span style="color: #60718B; font-weight: bold;">Countries</span>($entriesCount): $countries
<div id="indicator" style="display: none">
  <img style="margin: 5px 0 0 0; float: left;" src="http://www.goabroad.com/facebook/traveltodo/images/loading.gif"><p style="float: left; margin: 10px 0 0 2px;">Loading...</p>
    <div style="clear: both;"></div>
</div>
HereDoc;

echo $htmlFragment;
?>