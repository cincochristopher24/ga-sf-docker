<?php
$fbnarrow = <<< Style
  <fb:narrow>
  <style>
	.location_info {
	  background: #e4e4e4 url("http://www.goabroad.net/facebook/mytravelmap/images/location_info_bg.gif") repeat-x;
	  border:1px solid #dadada;
	  border-top-width:0;
	  padding:8px 29px 10px;
	  position:relative;
	}

	.location_info_extended {
	  padding:8px 8px 18px 8px;
	}

	.location_details {
	  color:#666;
	  margin-top:8px;
	}

	.location_info_extended .location_details {
	  margin:2px 0 -5px;
	  font-size:10px;
	  line-height:1.45em;
	}


	.location_navigation li{
	  position:absolute;
	  text-indent:-9999px;
	}

	.location_navigation .prev {
	  left:2px;
	}

	.location_navigation .next {
	  right:2px;
	}

	.location_info_extended .location_navigation li {
	  bottom:2px;
	  top:auto;
	}

	.location_info_extended .location_navigation .prev {
	  right:24px;
	  left:auto;
	}

	.location_info_extended .location_navigation .next {
	  right:6px;
	}

	.next_link, .prev_link{
	  display:block;
	  padding:2px;
	}

	.next_link span, .prev_link span{
	  height:14px;
	  width:14px;
	  background:transparent url("http://www.goabroad.net/facebook/mytravelmap/images/navigation_buttons.png") 0 0 no-repeat;
	  display:block;
	}

	.next_link span {
	  background-position:right top;
	}

	.prev_link:hover span{
	  background-position:0 -14px;
	}

	.next_link:hover span {
	  background-position:right -14px;
	}

	.journal_photo {
	  float:left;
	  border:2px solid #ddd;
	  margin-right:7px;
	}

	.location_flag {
	  float:left;
	  margin-right:7px;
	  width:24px;
	  height:12px;
	}

	.country_name {
	  margin:0;
	  font-size:13px;
	  color:#607199;
	}

	.location_info_extended .country_name {
	  margin-left:32px;
	  font-size:11px;
	}

	.refresh_icon {
	  position:relative;
	  top:3px;
	  left:3px;
	  width: 16px; height: 16px;
	}

	.mute {
	  color:#999 !important;
	}

	.round_button {
	  background:transparent url("http://www.goabroad.net/facebook/mytravelmap/images/button.gif") right top no-repeat;
	  color:#575757;
	  text-shadow: 1px 1px 0px #fff;
	  padding:3px 0 4px 0;
	  margin-left:15px;
	  text-decoration:none;
	  line-height:20px;
	}

	.round_button:hover {
	  text-decoration:none;
	}

	.round_button span {
	  padding:3px 10px 4px 10px;
	  background:transparent url("http://www.goabroad.net/facebook/mytravelmap/images/button.gif") left -20px no-repeat;
	  margin-left:-10px;
	}

	.markerPointer {
	  height:8px;
	  width:13px;
	  position:absolute;
	  z-index:6;
	  bottom:1px;
	}

	#container { 
		background: #ffffff url("http://www.goabroad.net/facebook/mytravelmap/images/world_map_facebook2.gif") no-repeat scroll 0 0; 
		color: #ffffff; 
		position: relative; 
		height: 226px; 
		width: 380px; 
	}
	
	#wallContainer { 
		background:#fff url("http://www.goabroad.net/facebook/mytravelmap/images/wallTabMap2.png") no-repeat scroll 0 0; 
		color:#fff; 
		height:108px;
		overflow:hidden;
		position:relative;
		width:184px; 
	}
	
	#container div { position: absolute; }
	#container img { border-style: none; }
	#wallContainer div { position: absolute;}
	#wallContainer img { border-style: none;}
	
	.top_links { 
		margin: 0 0 5px 0; 
		padding: 0 3px; 
		text-align: right; 
		font-weight: bold; 
	}
	
	.top_links_extended {font-weight:normal;}

	.wish { 
		border: 1px solid #e6e6e6; 
		margin: 5px 0 0; 
		padding: 8px 10px; 
		color: #777777; 
		font-size: 11px; 
		background-color: #fafafa; 
		line-height:1.4em;
	}
	.wish_extended {font-size:9px;padding:6px 7px;}
	.wish span { color: #60718b; font-weight: bold; }
	.wish_extended span {font-size:10px;}
	
	.bigger {
		color: #60718B;
		font-size: 12px;
		font-weight: bold;
	}
	
	.click_invitation_2 {
	  background-color:#fff;
	  padding:1px 5px;
	  position:absolute;
	  width:172px;
	  color:#474747;
	  z-index:5;
	  line-height:1.5em;
	  border:1px solid #859bb3;
	  height:1.5em;
	  overflow:hidden;
	}

	.click_invitation_2 p {
	  font-size:10px;
	  margin:0;
	}

	.click_invitation_2 img {
	  float:left;
	  margin-left:-4px;
	  line-height:0;
	}

	.imgTrans { filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0); }
	.blueMarker { width: 12px; height: 20px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinBlueSmall'); }
	.greenMarker { width: 12px; height: 20px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinBlueSmall'); }
	.yellowMarker { width: 12px; height: 20px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinYellowSmall'); }
	.shadowMarker { width: 10px; height: 17px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinShadowSmall'); }

Style;

// set the individual countries' markers position in our CSS
$wallMap = new Map(array('x'=>183, 'y'=>183));
$entryCount = 0;
$countries = '';
$preselectedCtry = '';
$defaultPointerLeft=null;
if ($mapEntries) :
  foreach ($mapEntries as $entry) {
    $shadowDivID = 'pinShadow_'.$entryCount;
    $markerDivID = 'pinBlue_'.$entryCount;
    $altMarkerDivID = 'pinYellow_'.$entryCount;

    $coordinates = $wallMap->lngLatToXYCoords($entry->getCountry()->getLongitude(), $entry->getCountry()->getLatitude(), 'walltab');

    $y = $coordinates['y'] + 4;
    $x = $coordinates['x'] - 1;
	
	if($entry->getCountry()->getCountryID() == 1322) {
		$x += 5;
	}

    $fbnarrow .= '#'.$shadowDivID. '{
      top: '.($y-1).'px;
      left: '.($x-1).'px;
      z-index: 0
    }';

    $x++;
    $fbnarrow .= '#'.$markerDivID. '{
      top: '.$y.'px;
      left: '.$x.'px;
      z-index: 5
    }';

    $y++;
    $fbnarrow .= '#'.$altMarkerDivID. '{
      top: '.$y.'px;
      left: '.$x.'px;
      z-index: 5
    }';
	
	if($entryCount == 0) $defaultPointerLeft = $x.'px';
    $entryCount++;
  }//end for-each $this->mapEntries
  $fbnarrow .='</style>';
  $fbnarrow .= <<< IE6Style
  <fb:user-agent includes="ie 6"> 
  <style type="text/css">
    .next_link span, .prev_link span{
  	  background-image:url("http://www.goabroad.net/facebook/mytravelmap/images/navigation_buttons.gif");
  	}
  	.markerPointer {
  	  bottom:-4px;
  	}
  </style>
  </fb:user-agent> 
IE6Style;
  $fbnarrow .= <<< Links
	<div class="top_links top_links_extended">
	  	<fb:visible-to-owner>
	  		<img class="refresh_icon" alt=" " src="http://www.goabroad.net/facebook/mytravelmap/images/icon-refresh.gif" />
			<a href="http://apps.facebook.com/my-travel-map/index.php?action=refreshProfile">Refresh</a>
		</fb:visible-to-owner>
		
		<img class="refresh_icon" alt=" " src="http://www.goabroad.net/facebook/mytravelmap/images/icon-view-larger2.gif" />
		<a href="http://apps.facebook.com/my-travel-map/index.php?id=$userID">View Larger Map</a>
	</div>
Links;

	// MAP DIV CONTAINER -->
	$fbnarrow .= '<div id="wallContainer" onclick="closeTipBox()">';

  	$isCtrySelected = false; //watch out for $entryCount
	$entryHelper = new EntryHelper;	
		$i = 0;
	$citiesCount=0;
	// $entryCount FOR LOOP -->
	foreach ($mapEntries as $mapEntry) :
		$citiesCount += count($mapEntry->getCountry()->getCityLocationList());
		$ctryName = str_replace("&",'&amp;',$mapEntry->getCountry()->getName());
		$flag = $mapEntry->getCountry()->getFlag();
		$ctryMarkup = $entryInfo = '';
		
		if($travellogID = $mapEntry->getTravelLogID()) {
			$journalEntries = $photoEntries = $travelTips = '';
			
			$travel = new Travel($mapEntry->getTravelID());
  			$travelLog = new TravelLog($travellogID);
			$entryTips = new EntryTips;
			$ts = count($entryTips->getList($travellogID));
			$js = $travel->hasAnyEntry();
			$photos = $travelLog->getPhotos();
			$ps = count($photos);

			$link = self::GOABROAD_DOTNET_URL.$entryHelper->getLink($travelLog);

        	$entries = $js < 2 ? "$js Journal Entry" : "$js Journal Entries";
			$journalEntries = "$entries <span class='mute'>|</span> <a href='$link' target='_blank'>read latest &raquo;</a><br />";

			if($ps) {
        		$photoEntries = $ps < 2 ? "$ps Photo" : "$ps Photos";
                $photo =  $photos[rand(0,$ps-1)]->getPhotoLink('thumbnail');
				$photosLink = self::GOABROAD_DOTNET_URL."/collection.php?type=traveler&ID=".$travelLog->getTravelerID()."&context=journal&genID=".$travelLog->getTravelID();
				$photoEntries = "$photoEntries <span class='mute'>|</span> <a href='$photosLink' target='_blank'>browse photos &raquo;</a><br />";
			}

			if($ts) {
            	$travelTips = $ts < 2 ? "$ts Travel Tip"   : "$ts Travel Tips";													
			}
			
		    $entryInfo = "<p class='location_details'>$journalEntries $photoEntries $travelTips </p>";
		}

     	$ctryMarkup .= <<< CtryMarkup
		<div id="country_details">
		  <img class="location_flag" src="$flag" alt="$ctryName" width="24"/>
		  <p class="country_name"><strong>$ctryName</strong></p>
		  $entryInfo
		</div>
CtryMarkup;

   		$ctryMarkup .= '<div style="clear: both"></div>';

      	$fbnarrow .= <<< FbjsString
        <fb:js-string var="entries.ctry$i">
            $ctryMarkup
        </fb:js-string>
FbjsString;
   		if (!$isCtrySelected) { $preselectedCtry = $ctryMarkup; }


        // MARKER'S SHADOW -->
      	$fbnarrow .= <<< Marker
        <div id="pinShadow_$i" class="shadowMarker">
          	<img width="22" src="$pinShadowSmall" class="imgTrans"/>
        </div>
Marker;
		// END OF MARKER'S SHADOW -->


       	// MAIN MARKERS -->
      	if (!$isCtrySelected) :
			$isCtrySelected = true;
        	$fbnarrow .= <<< Marker
          	<div id="pinBlue_$i" class="greenMarker" style="display:none" onclick="showCountryInfo($i, this)" onmouseover="accentMarker(this)" >
            	<img class="imgTrans" title="{$mapEntry->getCountry()->getName()}" alt="{$mapEntry->getCountry()->getName()}" src="$pinBlueSmall" />
          	</div>
          	<div id="pinYellow_$i" onmouseover="accentMarker(this)" class="yellowMarker">
            	<img class="imgTrans" title="{$mapEntry->getCountry()->getName()}" alt="{$mapEntry->getCountry()->getName()}" src="$pinYellowSmall" />
          	</div>
Marker;
     	else :
        	$fbnarrow .= <<< Marker
          	<div id="pinBlue_$i" onclick="showCountryInfo($i,this)" onmouseover="accentMarker(this)" class="greenMarker">
            	<img class="imgTrans" title="{$mapEntry->getCountry()->getName()}" alt="{$mapEntry->getCountry()->getName()}" src="$pinBlueSmall" />
          	</div>
          	<div id="pinYellow_$i" onmouseover="accentMarker(this)" style="display:none" class="yellowMarker">
            	<img class="imgTrans" title="{$mapEntry->getCountry()->getName()}" alt="{$mapEntry->getCountry()->getName()}" src="$pinYellowSmall" />
          	</div>
Marker;
     		endif;
		// END OF MAIN MARKERS -->

    	$countries .= $countries ? ', '.$mapEntry->getCountry()->getName() : $mapEntry->getCountry()->getName();
		$i++;
  	endforeach;
	// END OF $entryCount FOR LOOP -->	
		if(!is_null($defaultPointerLeft)) {
			$fbnarrow .= "<div id='pointer' class='markerPointer' style='left:$defaultPointerLeft'>
				<img src='http://www.goabroad.net/facebook/mytravelmap/images/arrow-up.gif'/>
			</div>";
		}
		
	$fbnarrow .= '<div class="click_invitation_2" id="click_invitation">
			<p>
				<strong>Tip:</strong>
				Click on the pins.
				<img src="http://www.goabroad.net/facebook/mytravelmap/images/click_invite_illustration_2.gif" />
				<a onclick="closeTipBox()">Close</a>
			</p>
		</div>';
	
	$fbnarrow .= "</div>";
	// END OF MAP DIV CONTAINER -->

	$fbnarrow .= <<< PrevNextButton
	<div class="location_info location_info_extended clearfix">
		<ul class="location_navigation">
	    	<li class="prev"><a class="prev_link" title="Previous" onclick="mapEntriesPrevNextControl('prev')"><span>Prev</span></a></li>
	    	<li class="next"><a class="next_link" title="Next" onclick="mapEntriesPrevNextControl('next')"><span>Next</span></a></li>
	  	</ul>
PrevNextButton;

		// COUNTRY JOURNALS INFO -->
		$fbnarrow .= $preselectedCtry 
			? "<div id='ctryInfoDiv'>$preselectedCtry</div>" 
			: "<div id='ctryInfoDiv' style='display: none'></div>";	
		// END OF COUNTRY JOURNALS INFO -->
	 $fbnarrow .="</div>";

	$countriesCount = $entryCount < 2 ? "$entryCount country" : "$entryCount countries";
	$hasCities = ($citiesCount > 0) ? true : false;
	$citiesCount = $citiesCount < 2 ? "$citiesCount city" : "$citiesCount cities";
	// COUNTRYLIST -->
	if($entryCount) :
		$locationCountPhrase = ($hasCities) ? "$citiesCount in $countriesCount" : "$countriesCount";
		$fbnarrow .= <<< CountryList
		<div id="wish" class="wish wish_extended">
			<span class="bigger"> 
				$locationCountPhrase
			</span>
		</div>
CountryList;
	endif;
	// END OF COUNTRYLIST -->


// ELSE HAS NO MAP ENTRIES -->
else :
  	$fbnarrow .='</style>';	
  	$fbnarrow .= <<< Links
  	<fb:visible-to-owner>
	  	<div id="top_links">
	    	<a href="http://apps.facebook.com/my-travel-map/">Add Country Lists</a>
	  	</div>
  	</fb:visible-to-owner>
	<div style='font-weight:bold;padding:10px'>No Countries traveled yet!</div>
Links;

endif;
// END IF HAS MAP ENTRIES -->


// SRCIPTS -->
$fbnarrow .= <<< Script
<script>
	var currentValue = 0;
	var totalCount =  $entryCount - 1;

  	function showCountryInfo(id,elem) {
		var divElem = $('wallContainer').getChildNodes();
    	for (var i=0; i < divElem.length; i++) {
			if(divElem[i].getClassName() == 'yellowMarker') {
				divElem[i].setStyle('display', 'none');
				divElem[i].getPreviousSibling().setStyle('display', 'block');
			}			
    	}
		currentValue = id;
    	elem.setStyle('display', 'none');
    	elem.getNextSibling().setStyle('display', 'block');
    	$('pointer').setStyle('left', elem.getNextSibling().getAbsoluteLeft()-170 + 'px');
    	$('ctryInfoDiv').setInnerFBML(entries["ctry"+id]).setStyle('display', 'block');
  	}

	function mapEntriesPrevNextControl(action) {
		var i = currentValue;
		if(action == 'next') {
			i = i == totalCount ? 0 : i+1;
		} else {
			i = i == 0 ? totalCount : i-1;
		}
		currentValue = i;
		showCountryInfo(i,$('pinBlue_'+i));
		accentMarker($('pinYellow_'+i));
	}

  	function accentMarker(obj) {
    	for (var i=0; i < $entryCount; i++) {
      		if (mainPin = $('pinBlue_' + i)) {
        		mainPin.setStyle({zIndex: '4'});
      		}
      		if (altPin = $('pinYellow_' + i)) {
        		altPin.setStyle({zIndex: '4'});
      		}
    	}
    	obj.setStyle({display: 'block', zIndex: '5'});
  	}

  	function restoreMarker(obj) {
    	obj.setStyle('display', 'block');
  	}

	function closeTipBox() {
		$('click_invitation').setStyle('display','none');
	}

	function $(elemId) {
	   return document.getElementById(elemId);
	}

</script>
Script;
?>