<?php
$user = $model['user'];
$isSynced = $model['isSynced'];
$isOwnerUser =$model['isOwnerUser'];
$countryList = $model['countryList'];
$mapEntries = $model['mapEntries'];
$fbFirstName =$model['fbFirstName'];
$displayMapTip =$model['displayMapTip'];
//$uneditableMapEntries = $model['uneditableMapEntries'];
$entriesLocationIDs = array_keys($mapEntries);
$entriesCount = count($entriesLocationIDs);
$editMode = isset($model['edit']) ? true : false;

$traveledName = $isOwnerUser ? "$fbFirstName have" : "$fbFirstName has"; ?>

<div id="tab_canvas" class="">
	<div>
		<div class="app_tab " id="">
			<div>
				<div class="appContainer" id="appContainer">
						
					<?php $selected = 'dashboard'; require_once 'mtm_dashboard.php';?>
									
					<div style="display:none" class="feature">
						<p class=""> Bring your <strong>GoAbroad Network</strong> travel journals
							to <strong>Facebook</strong>! <br/>
							Post your journals, photos and let your friends
							and family know about your experience! </p>
						<p class=""> GoAbroad Network is a social networking website for travelers
							all over the world. It is an online travel community where members can
							share travel journals and photos with families and friends, while connecting
							with a growing network of travelers. </p>
						<p class=""> Not yet travel-blogging? <br/>
							Get a free traveler profile and cool travel
								blog on <a target="_blank" href="http://www.goabroad.net/register.php">www.GoAbroad.net!</a> </p>
					</div>
											
					<div id="inner_wrapper" class="clearfix">
						<div class="wrapper2 clearfix" id="wrapper" style="padding-right:10px">
							<?php if(count($mapEntries) > 0): ?>
								<div style="margin-bottom:10px;">
									<fb:if-section-not-added section="profile">
										<?php 
											//$facebook_api = MyTravelMapController::getFacebookAPI();
											//$facebook_api->api_client->profile_setFBML(NULL, $user, 'boxes', NULL, NULL, 'narrow');
										?>
										<fb:add-section-button section="profile" />
									</fb:if-section-not-added>
								</div>
							<?php endif; ?>
							<div class="map_container_tab map_container_tab_extended" id="iFrameMap">
								<fb:iframe width="100%" height="472" scrolling="no" frameborder="0" style="border:none"
									src="http://www.goabroad.net/facebook/mytravelmap.new/index.php?action=genmap&id=<?=$user?>&fbFirstName=<?=$fbFirstName?>"
								/>
								
							<?php if($mapEntries && $displayMapTip == 'yes' && $isOwnerUser) : ?>
		         	    		<div class="click_invitation" id="click_invitation">
		         					<img src="http://www.goabroad.net/facebook/mytravelmap/images/click_invite_illustration.jpg" />
		         					<p>
		         						<strong>Tip:</strong><br />Click on the pins to browse through the locations.
		         					</p>
		         					<p>
		         						<a onclick="closeTipBox()">close</a>
		         					</p>
		         				</div>
							<?php endif ?>
							</div>

							<?php if(empty($mapEntries) && $isOwnerUser) : ?>
								<div class="click_invitation getting_started_invite">
  									<!-- help text here -->
									<p>
										<strong>Let's Get Started!</strong><br />
										You haven't added any locations on your list yet.
										Start doing that by clicking on the
										<b>Add Country Lists</b> button below.
										<span class="invite_arrow_head"></span>
									</p>
									<!-- help text here -->
								</div>
							<?php endif ?>
								
							<?php if($isOwnerUser) : ?>
								<div class="top_links top_links_extended clear UIMediaButton_Container" id="divEditButton">
									<table cellspacing="0" cellpadding="0" border="0" style="float:right">
										<tr class="UIMediaButton_top_row">
											<td class="UIMediaButton_tl"></td>
											<td class="UIMediaButton_content" rowspan="2">
												<div class="UIMediaButton_bottom clearfix">
													<a onclick="enableEditMode()"><?php echo empty($mapEntries) ? 'Add' : 'Edit' ?> Country Lists</a>
												</div>
											</td>
											<td class="UIMediaButton_tr"></td>
										</tr>
										<tr class="UIMediaButton_lower_row">
											<td class="UIMediaButton_bl"></td>
											<td class="UIMediaButton_br"></td>
										</tr>
									</table>
								</div>
							<?php endif; ?>
							
							<?php $mode = 'sdfdsf';  ?>
							<div class="wish2 wish2_extended">
									
								<!-- CONTRIES VIEW -->
								<div id="editCountriesContainer">

									<ul class="location_list" id="ulContainer">

										<?php 
										$locIDs = array(); $cityLocIDs = array(); $currentCityLocIDs = array(); $arrUnEditableCityLocationID = array();
										foreach ($mapEntries as $mapEntry) : $locationID = $mapEntry->getCountry()->getID(); ?>
										  	<li id="li_<?php echo $locationID ?>">
										    	<strong><?php echo $mapEntry->getCountry()->getName() ?></strong><br/>

												<!-- CITIES -->												
												<?php $i=0; $currentCityLocIDofCountry = array();
												foreach($mapEntry->getCountry()->getCityLocationList() as $entry): ?>
													
													<span id="countryList_<?php echo $entry->getID() ?>" style="color:#444444">
														<?php echo (count($mapEntry->getCountry()->getCityLocationList())-1) > $i ? $entry->getName().', ' : $entry->getName();  ?>
													</span>

											    	<span id="city_<?php echo $entry->getID() ?>" class="editing_mode cityCtr" style="display:none;">
														<?php echo $entry->getName(); ?>
														<a title="Remove City" class="delete_link" id="removeCity_<?php echo $entry->getID() ?>" onclick="confirmDelete(this,'')">
															Remove City
														</a>
													</span>

												<?php $i++; $cityLocIDs[] = $entry->getID(); 
												$currentCityLocIDofCountry[] = $entry->getID(); 
												if (!$entry->getIsEditable()) // if city location can not be removed from map, add to list
													$arrUnEditableCityLocationID[] =  $entry->getID();
												endforeach; 
												$currentCityLocIDs[$locationID] = $currentCityLocIDofCountry;
												?>
												<!-- END CITIES -->

												<!-- Add City and New Location Links -->
												<div id="addLocationLinks_<?php echo $locationID ?>" class="add_location_links" style="display: none;">
													<span style="font-size:10px">Add:</span>
													<a onclick="showCities(<?php echo $locationID ?>, 'citiesContainer_<?php echo $locationID ?>', '<?php echo implode('.', $cityLocIDs) ?>')">
														From List
													</a>
													|
													<a onclick="displayMap('<?php echo $locationID ?>','<?php echo $mapEntry->getCountry()->getCountryID() ?>','<?php echo $mapEntry->getCountry()->getLatitude()."_".$mapEntry->getCountry()->getLongitude() ?>')">
														New Location
													</a>
												</div>

												<a title="Remove Country" class="delete_link" alt="remove" id="removeCountry_<?php echo $locationID ?>" onclick="confirmDelete(this, '<?php echo implode('.', $currentCityLocIDs[$locationID]) ?>')" style="display:none">
													Remove Country
												</a>

												<div style="clear:left"></div>

										  	</li>
											<li id="citiesContainer_<?php echo $locationID ?>" class="editing_mode edit_active" style="display:none"></li>

											<?php $locIDs[] = $locationID; 
										    if (!$mapEntry->getIsEditable()):
										        $uneditableMapEntries[$locationID] = $locationID;
										    endif;
										endforeach; ?>

										<p id="addCountryContainer" style="display:none">
											
											<a class="round_button" onclick="showCountries('displayList');">
												<span><img src="http://goabroad.net/facebook/mytravelmap/images/plus.gif" alt="plus" />Add a Country</span>
											</a>
										</p>

										<li id="countryList" class="editing_mode edit_active clearfix" style="display:none">
											<select name="selectCountry" id="selectCountry" onchange="showCities(this.getValue(),'citiesContainerFromCountry', '')">
												<option selected="selected" value=0> - select country - </option>
												<?php foreach ($countryList as $county) : ?>
													<?php if(in_array($county->getID(), $entriesLocationIDs)) {
														$display = 'style="display:none" disabled="true"';
													} else $display = ''; ?>

													<option id="option_<?php echo $county->getID() ?>" value="<?php echo $county->getID() ?>" <?php echo $display ?>>
														<?php echo $county->getName() ?>
													</option>

												<?php endforeach; ?>
											</select>
											<a class="cancel_link" id="countryListCancelButton" onclick="showCountries('hide')">Cancel</a>
											<div id="citiesContainerFromCountry"></div>
										</li>
										
										<p id="doneButton" style="display:none">
											<br /><a id="doneButton" class="fb_button" onclick="done()">Done</a>
										</p>
										
										<li id="disabler" class="disabler" style="display:none"></li>
									</ul>
								</div>
								
								<h2 class="location_list_heading" id="heading" style="top:15px;">  
									<?php if(count($mapEntries) > 0): ?>		
										<?php echo $traveledName ?> 
										been to <?php if(count($cityLocIDs) > 0):?> <?php echo count($cityLocIDs); echo count($cityLocIDs) < 2 ? ' City' : ' Cities'; ?>
										in <?php endif; ?><?php echo $entriesCount; echo $entriesCount < 2 ? ' Country' : ' Countries' ?> : 
									<?php endif; ?>
								</h2>
								
								
						 		<!-- END OF COUNTRIES VIEW -->							
						</div>
					</div>

					<div class="foot"> My Travel Map
						<span> &copy; <a target="_blank" href="http://www.goabroad.net">GoAbroad
						Network</a>/ <a target="_blank" href="http://www.goabroad.com">GoAbroad.com</a>.
						<span> All Rights Reserved. </span>
						</span>
					</div>						

					<!-- Add City Interface -->
					<div id="customMapContianer" class="mbox">
						<div id="mbox"></div>
						<div id="closeButtonDiv" style="display:none">
							<a href="#" onclick="return closeCustomMap(this)" >Close</a>
						</div>
					</div>
					<div id="divDisabler" class="disabler" style="display:none"></div>					
				</div>
			</div>
		</div>
		<div style="position: absolute; top: -10000px; width: 0px; height: 0px;" id="FB_HiddenContainer"></div>
	</div>
</div>
<fb:js-string var="loading">
	<div class="loading_wrapper">
		<img src='http://www.goabroad.net/facebook/mytravelmap/images/globe_loading.gif'>
		<span>Loading Request . . . </span>
	</div>
</fb:js-string> 
<fb:js-string var="bigLoading">
	<img src='http://www.goabroad.net/facebook/mytravelmap/images/bigGlobe_loading.gif'>
	<span>Fetching Map . . . </span>
</fb:js-string> 
<script>
	var stringLocationIDs = '';
	var postURL = 'http://www.goabroad.net/facebook/mytravelmap.new/index.php';

	function confirmDelete(elem,stringCityLocationIDs) { 
		var flag = elem.getTitle();
		if(flag == 'Remove Country') {
			var dialog_title = 'Delete Country';
			var dialog_message = 'Are you sure you want to delete this country?';
		} else {
			var dialog_title = 'Delete City';
			var dialog_message = 'Are you sure you want to delete this city?';
		}
		
		var dialog = new Dialog().showChoice(dialog_title, dialog_message, 'Yes', 'Cancel'); 
		$('disabler').setStyle('display', 'block');
		dialog.onconfirm = function() { 
			removeLocation(elem,stringCityLocationIDs); 
			$('disabler').setStyle('display', 'none');
		}; 
		dialog.oncancel = function() {
		    $('disabler').setStyle('display', 'none');
		}; 
	}

	function processAjax(queryParams,outputDataContainer) {
		var ajax = new Ajax(); 
		ajax.responseType = Ajax.FBML; 
		ajax.ondone = function(data) { 
			if(outputDataContainer != '') {
				outputDataContainer.setInnerFBML(data);				
			}
			return true;
		} 	
		ajax.requireLogin = true;
		ajax.post(postURL, queryParams);
	}

	function enableEditMode() {
		$('divEditButton').setStyle('display', 'none');
		$('heading').setTextValue('Editing Locations:');
		$('doneButton').setStyle('display', 'block');
		$('addCountryContainer').setStyle('display', 'block');

		var arrLocationID = "<?php echo implode('.', $locIDs) ?>".split('.');
		var arrUneditableLocationID = "<?php echo is_array($uneditableMapEntries) ? implode('.', array_keys($uneditableMapEntries)) : '' ?>".split('.');
	
		for (var i=0; i < arrLocationID.length; i++) {
			$('li_'+arrLocationID[i]).setClassName('editing_mode clearfix countryCtr');			
			$('addLocationLinks_'+arrLocationID[i]).setStyle('display', 'block');
//			$('newLocation_'+arrLocationID[i]).setStyle('display', 'inline');			
			if (arrUneditableLocationID.indexOf(arrLocationID[i]) < 0) // if locationID is not in this array of uneditable entries, allow removal
				$('removeCountry_'+arrLocationID[i]).setStyle('display', 'block');
		}
		
		var arrCityLocationID = "<?php echo implode('.', $cityLocIDs) ?>".split('.');
		var arrUnEditableCityLocID = "<?php echo implode('.', $arrUnEditableCityLocationID) ?>".split('.');
		
		for (var ii=0; ii < arrCityLocationID.length; ii++) {
 			$('city_'+arrCityLocationID[ii]).setStyle('display', 'block');
 			$('countryList_'+arrCityLocationID[ii]).setStyle('display', 'none');
			// if locationID is in this array of uneditable entries, disallow removal
			displayVal = (arrUnEditableCityLocID.indexOf(arrCityLocationID[ii]) >= 0) ? 'none' : 'block';
			$('removeCity_'+arrCityLocationID[ii]).setStyle('display', displayVal);
		}
	}

	function showCities(locationID, divElem, selectedCities) {
		
		if(parseInt(locationID) > 0) {
			$(divElem).setStyle('display', 'block');
			$(divElem).setStyle('z-index', '3');
			//var message = $(divElem).getValue();
			//new Dialog().showMessage('Dialog', message);
			//if($(divElem).getChildNodes().length < 3 || $(divElem).getId() == 'citiesContainerFromCountry') {
			    //$(divElem).setStyle('display', 'block');
			    //$(divElem).setStyle('z-index', '3');
			    $(divElem).setInnerFBML(loading);
			    $('disabler').setStyle('display', 'block');
			    var ajax = new Ajax(); 
			    ajax.responseType = Ajax.FBML; 
			    ajax.ondone = function(data) { 
				    //$('disabler').setStyle('display', 'block');
				    if($('countryList').getStyle('display') != 'none') {
					    $('countryListCancelButton').setStyle('display','none');
				    }
				    $(divElem).setInnerFBML(data);
			    }
			    ajax.requireLogin = true;
			    ajax.onerror = function() { 
    			    show_error_dialog();
    			}
			    var queryParams = { 
				    "action" : "getCities", 
				    "locationID" : locationID, 
				    "selectedCities" : selectedCities
			    };
			    ajax.post(postURL, queryParams);
		    //} else {
		    //    uncheck(divElem);
		    //}
		} else {
			$(divElem).setTextValue('');
		}
	}
	
	function uncheck(divElem) {
        var elem = $(divElem).getFirstChild();
        var checks = elem.getChildNodes();
        for (i = 0; i < checks.length;i++) {
            if (checks[i].getType() == 'checkbox' ) {
                checks[i].setChecked(false);
            }
        }
	}
		
	function showCountries(action) {
		if(action == 'displayList') {
			$('selectCountry').setSelectedIndex(0);
			$('disabler').setStyle('display', 'block');
			$('countryList').setStyle('display', 'block');
			$('addCountryContainer').setStyle('display', 'none');
			if($('countryListCancelButton').getStyle('display') != 'block') {
				$('countryListCancelButton').setStyle('display', 'block');
			}
		}
		else {
			$('disabler').setStyle('display', 'none')
			$('countryList').setStyle('display', 'none');						
			$('addCountryContainer').setStyle('display', 'block');
			$('citiesContainerFromCountry').setStyle('display', 'none');			

			//if($('divCities').getStyle('display') != 'none') {
			//   $('divCities').setTextValue('');
			//}
		}
	}
		
	function addCities(countryLocationID, countryID) {
		var stringLocationIDs = '';
		var cities = $('divCityList_'+countryLocationID).getChildNodes();
		var isFromCountryList = $('countryList').getStyle('display') == 'block';
		var citiesContainer = isFromCountryList ? $('citiesContainerFromCountry') : $('citiesContainer_'+countryLocationID);
		
		for (var i=0; i < cities.length; i++) {
			if(cities[i].getType() == 'checkbox' && cities[i].getChecked() == true) {
				stringLocationIDs += '.'+cities[i].getValue();
			}
		}
		
		if(isFromCountryList) { stringLocationIDs += '.'+countryLocationID; }
		                                                                 
		if(stringLocationIDs != '') {

			citiesContainer.setInnerFBML(loading);

			var ajax = new Ajax();
			ajax.responseType = Ajax.FBML; 
			ajax.ondone = function(data) { 

				$('disabler').setStyle('display', 'none');
				
				if(isFromCountryList) { // IF New Country
					$('option_'+countryLocationID).setDisabled(true);
					$('option_'+countryLocationID).setStyle('display', 'none');

					// Create LI Element For the Entry Container
					var liObj = document.createElement('li');
					liObj.setId('li_'+countryLocationID);
					liObj.setClassName('editing_mode clearfix countryCtr');
					liObj.setStyle('border','2px solid #AAAAAA'); 
					liObj.setInnerFBML(data);
		  			$('ulContainer').insertBefore(liObj, $('addCountryContainer'));


					// Create li Element for List of Cities Container
					var liContainer = document.createElement('li');
					liContainer.setId('citiesContainer_'+countryLocationID);
					liContainer.setClassName('editing_mode edit_active');
					liContainer.setStyle('display', 'none');
		  			$('ulContainer').insertBefore(liContainer, $('addCountryContainer'));

					$('countryList').setStyle('display', 'none');
					$('addCountryContainer').setStyle('display', 'block');

				} else {
					$('li_'+countryLocationID).setInnerFBML(data);
				}

				citiesContainer.setStyle('display', 'none');
			}
			ajax.onerror = function() { 
			    show_error_dialog();
			}  	
			
			ajax.requireLogin = true;
			var queryParams = { 
				"action" : "addCities", 
				"countryID" : countryID, 
				"stringLocationIDs" : stringLocationIDs, 
				"countryLocationID" : countryLocationID
			};
			ajax.post(postURL, queryParams);
		} else {
			citiesContainer.setStyle('display', 'none');
			$('disabler').setStyle('display', 'none');
		}
	}
		
	function removeLocation(elem,stringCityLocationIDs) {
		var params = elem.getId().split('_'); 
		var entry = elem.getParentNode();
		stringLocationIDs = params[1];
		if(stringCityLocationIDs != '') {
			stringLocationIDs += '.'+stringCityLocationIDs;
		}
		entry.setStyle('backgroundColor', '#E5DCDC');
		entry.setTextValue('Deleting...');
		var ajax = new Ajax(); 
		ajax.responseType = Ajax.FBML; 
		ajax.ondone = function(data) { 
			//document.setLocation('http://apps.facebook.com/mytravelmap-dev/');
			removeElement(entry.getId());
			$('option_'+countryLocationID).setDisabled(false);
			$('option_'+params[1]).setStyle('display','');

		}
		ajax.requireLogin = true;
		var queryParams = { "action" : "removeLocation", "stringLocationIDs" : stringLocationIDs};
		ajax.post(postURL, queryParams);
		
	}

	function done() {
		
		$('doneButton').setStyle('color', 'green');
		$('doneButton').setStyle('padding', '8px');
		$('doneButton').setStyle('display', 'block');
		$('doneButton').setStyle('fontSize', '12px');
		$('doneButton').setStyle('marginTop', '20px');
		$('doneButton').setStyle('fontWeight', 'bold');       
		$('doneButton').setStyle('backgroundColor', '#EEF4FF');
		$('doneButton').setStyle('border', '1px dashed #DDDDDD');
		$('doneButton').setTextValue('Updating Map changes . . .');
		
		var countryCtr = getElementsByClass('countryCtr').length;
		var cityCtr = getElementsByClass('cityCtr').length;

		var cityPhrase = "";
		var countryPhrase = "";

		if(cityCtr > 0){ cityPhrase = (cityCtr > 1) ? cityCtr+" cities in" : cityCtr+" city in"; }
		if(countryCtr > 0){ countryPhrase = (countryCtr > 1) ? countryCtr+" countries" : countryCtr+" country"; }
		
		var attachment = {
			'name': "Check out My Travel Map and see where I've travelled to!",
			'href': 'http://apps.facebook.com/my-travel-map/index.php?id=<?php echo $user;?>',
			'description': "I've been to "+cityPhrase+" "+countryPhrase+". Have you been to different places too? Share them with me on My Travel Map.",
			'media': [{ 'type': 'image', 'src': 'http://bit.ly/dwqk9C', 'href': 'http://apps.facebook.com/my-travel-map'}]
		};
		var thecallback = function(post_id, exception, data){
			document.setLocation('http://apps.facebook.com/my-travel-map/index.php?updatedMyTravelMap');
		}
		if(countryCtr > 0 || cityCtr > 0){
			publishNewsFeeds(attachment, thecallback);
		}else{
			document.setLocation('http://apps.facebook.com/my-travel-map/index.php?updatedMyTravelMap');
		}
		// Facebook.streamPublish('', attachment, null, null, "Let your friends know where you've been.", thecallback);
		//if(stringLocationIDs != '') {
		//	var ajax = new Ajax(); 
		//	ajax.responseType = Ajax.FBML; 
		//	ajax.ondone = function(data) { 
		//		document.setLocation('http://apps.facebook.com/mytravelmap-dev/');
		//	}
		//	ajax.requireLogin = true;
		//	var queryParams = { "action" : "removeLocation", "stringLocationIDs" : stringLocationIDs};
		//	ajax.post(postURL, queryParams);
		//} else {
			// document.setLocation('http://apps.facebook.com/my-travel-map/index.php?updatedMyTravelMap');
		//}
	}
	
	// function checkDisplayMap(countryLocationID,countryID,coordinate) {
	// 	var hasCheckedCity = false;
	// 	var cities = $('divCityList_'+countryLocationID).getChildNodes();
	// 	for (var i=0; i < cities.length; i++) {
	// 		if(cities[i].getType() == 'checkbox' && cities[i].getChecked() == true) {
	// 			hasCheckedCity = true;
	// 		}
	// 	}
	// 	if (hasCheckedCity) {
	// 		var dialog = new Dialog().showChoice('Warning', 'Please save first your selected city/cities.', 'Ok', 'Cancel'); 
	// 		$('disabler').setStyle('display', 'block');
	// 		dialog.onconfirm = function() { 
	// 			$('disabler').setStyle('display', 'none');
	// 		}; 
	// 		dialog.oncancel = function() {
	// 		    displayMap(countryLocationID,countryID,coordinate);
	// 		};
	// 	} else {
	// 		displayMap(countryLocationID,countryID,coordinate);
	// 	}
	// }

	function displayMap(countryLocationID,countryID,coordinate) {
		var coor = coordinate.split('_');	
		var isFromCountryList = $('countryList').getStyle('display') == 'block';
		var flag = '';	
		if (isFromCountryList) { flag = 1; } else { flag = 0; }
		$('mbox').setInnerFBML(loading);
		$('divDisabler').setStyle('display', 'block');
		$('customMapContianer').setStyle('position', 'fixed');
		var ajax = new Ajax(); 
		ajax.responseType = Ajax.FBML; 
		ajax.ondone = function(data) { 
			$('customMapContianer').setStyle('background', 'transparent');
			$('mbox').setInnerFBML(data);
			$('closeButtonDiv').setStyle('display', 'block');
		}
		ajax.requireLogin = true;
		var queryParams = { 
			"action" : "displayMap", 
			"countryLocationID" : countryLocationID,
			"countryID" : countryID,
			"latitude" : coor[0], 
			"longitude" : coor[1],
			"isNewCountry" : flag
		};

		ajax.post(postURL, queryParams);
		return false;
	}
	
	function closeCustomMap() {
//		$('customMapContianer').setStyle('background', '');
//		$('customMapContianer').setStyle('border', '');
//		$('customMapContianer').setStyle('margin', '');
//		$('customMapContianer').setTextValue(''); 
        $('mbox').setStyle('background', '');
        $('mbox').setTextValue(''); 
		$('closeButtonDiv').setStyle('display', 'none');
		$('divDisabler').setStyle('display', 'none');
		return false;
	}

	function removeElement(elemID) {
		var elem = document.getElementById(elemID);
		elem.getParentNode().removeChild(elem);
	}
	
	function closeCityList(id) {
		if($('countryList').getStyle('display') == 'block') {
			$('citiesContainerFromCountry').setTextValue('');
			//$('citiesContainerFromCountry').setStyle('display','none');
			$('countryList').setStyle('display','none');
			$('addCountryContainer').setStyle('display','block');
		} else {
			//$('citiesContainer_'+id).setTextValue('');
			$('citiesContainer_'+id).setStyle('display','none');

		}
		$('disabler').setStyle('display', 'none');
	}

	function closeTipBox() {
		$('click_invitation').setStyle('display','none');
		var ajax = new Ajax(); 
		ajax.responseType = Ajax.FBML; 
		ajax.ondone = function(data) { 
			// actions
		}
		ajax.requireLogin = true;
		ajax.onerror = function() { 
		    show_error_dialog();
		}  	
		var queryParams = { 
			"action" : "updateDisplayMapTip", 
			"displayMapTip" : "no"
		};

		ajax.post(postURL, queryParams);
	}
	
	function show_error_dialog() {
	    new Dialog().showMessage('Network Error', 'A network error occurred. Check that you are connected to the internet.');
	}
	
	function $(elemID) {
		return document.getElementById(elemID);
	}
	
	function editMode(isEditMode) {
	    if(isEditMode) {
	        enableEditMode();
	    }
	}
	
	function publishNewsFeeds(attachment, callback){
		Facebook.streamPublish('', attachment, "", null, "Let your friends know where you've been.", callback);
	}
	
	function getElementsByClass(className){
		var regex = new RegExp("(?:^|\\s)"+className+"(?:$|\\s)");
		var elts = $('tab_canvas').getElementsByTagName("*");
		var a = [];
		for(i=0; i<elts.length; i++){
			if(regex.test(elts[i].getClassName()))
				a.push(elts[i]);
		}
		return a;
	}
	
	editMode(<?php echo $editMode;?>);
</script>
