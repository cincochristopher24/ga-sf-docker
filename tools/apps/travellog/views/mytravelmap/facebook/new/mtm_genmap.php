<?php
require_once('travellog/model/Class.EntryTips.php');
$entryHelper = new EntryHelper;
$mapEntries = $model['mapEntries'];
$entriesCount = count($mapEntries);
$countryList = $model['countryList'];
$isOwnerUser =$model['isOwnerUser'];
$displayMapTip =$model['displayMapTip'];
$friendsBeenThere = $model['friendsBeenThere'];
$fbFirstName =isset($_GET['fbFirstName']) ? $_GET['fbFirstName'] : $model['fbFirstName'];
$traveledName = $isOwnerUser ? "your" : "$fbFirstName's"; 
$canvasOwnerID = $model['user']; // owner of canvas viewed ?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>My Travel Map</title>
    <link type="text/css" rel="Stylesheet" media="screen" href="http://www.goabroad.net/facebook/mytravelmap/css/travelMapBubble.css"/>
	<link type="text/css" rel="Stylesheet" media="screen" href="http://www.goabroad.net/facebook/mytravelmap/css/mytravelmap_v2.css"/>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAADN6t8sswaCRGbhKv8-BC0xTfWdpSMA2WXJD7WFd_XyKADP-mYxSQwLFLL4KN4RJUZRLKVgX7vxapkg" type="text/javascript"></script>
</head>
<body onload="load()" onunload="GUnload()">
	<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/en_US" type="text/javascript"></script>
		<div id="map_container" onclick="$('click_invitation').style.display='none'">
    		<div id="map"></div>
		</div>
		<?php if(!empty($mapEntries)) : ?>
			<div class="mtm_right_column">
				<div class="location_info location_info_extended2">
					<?php $photoLink = $link = '#'; $mapEntry = $mapEntries[key($mapEntries)]; ?>
					<ul class="location_navigation">
				    	<li class="prev"><a class="prev_link" href="javascript:void(0)" title="Previous" onclick="mapEntriesPrevNextControl('prev')"><span>Prev</span></a></li>
				    	<li class="next"><a class="next_link" href="javascript:void(0)" title="Next" onclick="mapEntriesPrevNextControl('next')"><span>Next</span></a></li>
				  	</ul>
					<div id="location_info" style="overflow:hidden">				
				  		<img class="location_flag" src="<?php echo $mapEntry->getCountry()->getFlag() ?>" alt="<?php echo $mapEntry->getCountry()->getName() ?> flag" />
				  		<p class="country_name"><strong><?php echo $mapEntry->getCountry()->getName() ?></strong></p>
				  		<p class="location_details">
							<?php //if($travellogID = $mapEntry->getTravelLogID()) :
								//$travel = new Travel($mapEntry->getTravelID());
								if($travellogID = $mapEntry->getCountry()->getTravelLogID()) :
					  			$travelLog = new TravelLog($travellogID);
					  			$link = 'http://www.goabroad.net'.$entryHelper->getLink($travelLog);
								$entryTips = new EntryTips;
								$tips = count($entryTips->getList($travellogID));
								//$journals = $travel->hasAnyEntry();
								$journals = $mapEntry->getCountry()->getJournalEntriesCount();
								$photos = $travelLog->getPhotos();
								$photoCount = count($photos);
						
								if($photoCount) {
				            		$photoLink = 'http://www.goabroad.net/collection.php?type=traveler&ID='.$travelLog->getTravelerID().'&context=journal&genID='.$travelLog->getTravelID();
								} ?>
			
								<?php if($photoCount) : ?>
									<img class="journal_photo" src="<?php echo $photos[rand(0,$photoCount-1)]->getPhotoLink('thumbnail') ?>" />
								<?php endif ?>
						
					    		<?php echo $journals == 1 ? $journals.' Journal Entry' : $journals.' Journal Entries' ?> 
								<br /><a href="<?php echo $link ?>" target="_blank">read latest &raquo;</a><br />

								<?php if($photoCount) : ?>
		 							<?php echo $photos == 1 ? $photoCount.' Journal Photo' : $photoCount.' Journal Photo';?>
					    			<a href="<?php echo $photoLink ?>" target="_blank">browse &raquo;</a><br />
								<?php endif; ?>
						
							<?php endif; ?>

							<?php if($tips) : echo $tips == 1 ? $tips.' Travel Tip' : $tips.' Travel Tips'; endif ?>
				  		</p>
					</div>
				</div>
				<?php 
					$showHeader = true;
					$firstMapEntry = $friendsBeenThere[key($mapEntries)];
					if(!$isOwnerUser){
						$firstMapEntry = is_array($firstMapEntry) ? array_flip($firstMapEntry) : array();
						if(array_key_exists($canvasOwnerID, $firstMapEntry)){
							$showHeader = false;
						}
					}else{
						$showHeader = (count($firstMapEntry) > 0);
					}
				?>
				<div class="friends_been_there" id="friends">
					
					<h3 class="friends_list_header" id="friends_header" style="<?php if(!$showHeader): echo 'display:none;'; endif; ?>">Friends who've been there:</h3>
					<?php $friendsBeenThere = is_array($friendsBeenThere) ? $friendsBeenThere : array(); ?>	
					<?php foreach($friendsBeenThere as $loc => $friends): ?>
						<?//php if($canvasOwnerID == 1244318742): var_dump($loc .'=='. key($mapEntries)); var_dump($loc != key($mapEntries));endif; ?>
						<ul class="friends_list" id="friendlist_<?=$loc?>" style="<?php if($loc != key($mapEntries)): echo 'display:none;'; endif; ?>">
							<?php 
								
								$friends = array_flip($friends);
								if(!$isOwnerUser): // if viewing other map canvass, exclude self from showing
									unset($friends[$canvasOwnerID]);
								endif; 
								
								$friends = array_unique($friends);
								$limit = (count($friends) > 6) ? 6 : count($friends);
								$friends = (count($friends) > 0) ? array_rand($friends, $limit) : array();
								$friends = (is_array($friends)) ? $friends : array($friends); // array_rand returns the key if the limit is set to 1 else array of keys
							?>
							<?php foreach($friends as $friend): ?>
								<li>
									<div style="width:57px;">
										<a href="http://apps.facebook.com/my-travel-map/index.php?id=<?=$friend?>" target="_parent"><fb:profile-pic uid="<?=$friend?>" linked="false" size="square"></fb:profile-pic></a>
										<div style="width:57px;word-wrap:break-word;">
											<a href="http://apps.facebook.com/my-travel-map/index.php?id=<?=$friend?>" target="_parent"><fb:name uid="<?=$friend?>" linked="false"></fb:name></a>
										</div>
									</div>	
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endforeach; ?>
					<?php if($isOwnerUser): ?>
					  	<p class="clear">
							<a href="http://apps.facebook.com/my-travel-map/index.php?action=invite" target="_parent" class="fb_button">Invite Friends</a>
							<a href="javascript:void(0);" onclick="publishToWall(<?php echo $canvasOwnerID; ?>);" class="fb_button">Share My Map</a>
						</p>
					<?php else: ?>
						<p class="clear">
							<a href="http://apps.facebook.com/my-travel-map" target="_parent" class="fb_button">Go to my map</a>
						</p>	
					<?php endif; ?>
				</div>
			</div>
        <?php endif ?>

		<script type="text/javascript">
		var appkey = '147aa5f035170dd13d8670b15cb3f906';
	    //<![CDATA[
	    var blueImage = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_markerTransparent.png';	
	    var yellowImage = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_markerYellowTransparent.png';	
	    var shadowImage = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_marker_shadowTransparent.png';

	    var iconBlue = new GIcon();
	    iconBlue.image = blueImage;
	    iconBlue.shadow = shadowImage;
	    iconBlue.iconSize = new GSize(12, 20);
	    iconBlue.shadowSize = new GSize(22, 20);
	    iconBlue.iconAnchor = new GPoint(6, 20);
	    iconBlue.infoWindowAnchor = new GPoint(5, 1);

	    var tinyBlueImage = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_tinymarkerBlueTransparent.png';
	    var tinyYellowImage = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_tinymarkerYellowTransparent.png';	
	    var iconTinyBlue = new GIcon();
	    iconTinyBlue.image = tinyBlueImage;
		iconTinyBlue.iconSize = new GSize(12, 12);
		iconTinyBlue.shadowSize = new GSize(22, 12);
	    iconTinyBlue.iconAnchor = new GPoint(6, 12);
	    iconTinyBlue.infoWindowAnchor = new GPoint(5, 1);

		var map;
		var markers = new Array();

		var arrlocationIDs = new Array();
		var arrMyHTML = new Array();

		var countryCount = 0;
		var cityCount = 0;
		
	    function load() {
	      	if (GBrowserIsCompatible()) {
	   			map = new GMap2($("map"));
	        	map.addControl(new GSmallMapControl());
	        	map.setCenter(new GLatLng(0, 0), 1);
				
				countryCount = <?php echo count($mapEntries);?>;
	      		<?php $i=1;
	        	foreach ($mapEntries as $entry) :?> 
					cityCount += <?php echo count($entry->getCountry()->getCityLocationList()); ?>;
					<?php $link = ''; $journals = $photoCount = $photos = $tips = 0;
			  		//if ($travellogID = $entry->getTravelLogID()) {
			  		if ($travellogID = $entry->getCountry()->getTravelLogID()) {
						//$travel = new Travel($entry->getTravelID());
			  			$travelLog = new TravelLog($travellogID);
			  			$link = 'http://www.goabroad.net'.$entryHelper->getLink($travelLog);
						$entryTips = new EntryTips;
						$tips = count($entryTips->getList($travellogID));
						$photos = $travelLog->getPhotos();
						//$journals = $travel->hasAnyEntry();
						$photoCount = count($photos);
						if($photoCount) {
							$photo = $photos[rand(0,$photoCount-1)]->getPhotoLink('thumbnail');						
		            		$photoLink = 'http://www.goabroad.net/collection.php?type=traveler&ID='.$travelLog->getTravelerID().'&context=travellog&genID='.$travellogID;
						}
			  		} 

					if($model['pinSetting'] != 1) : 
					    $journals = $entry->getCountry()->getJournalEntriesCount(); ?>
		        		var point = new GLatLng(parseFloat(<?php echo $entry->getCountry()->getLatitude(); ?>), parseFloat(<?php echo $entry->getCountry()->getLongitude(); ?>));
						var marker = createMarker(point,
		                      '<?php echo parseToXML($entry->getCountry()->getName()); ?>',
							  '<?php echo parseToXML($entry->getCountry()->getFlag()); ?>',
		                      '<?php echo $photo ? parseToXML($photo) : ''; ?>',
		                      parseInt(<?php echo $journals ? $journals : 0; ?>),
		                      parseInt(<?php echo $photoCount ? $photoCount : 0; ?>),
		                      parseInt(<?php echo $tips ? $tips : 0; ?>),
		                      '<?php echo parseToXML($link) ?>',
							  '<?php echo parseToXML($photoLink) ?>',
							  'country','<?php echo $entry->getCountry()->getID();?>', <?php echo $i ?>);
						<?php if ($entry->getCountry()->getLatitude() && $entry->getCountry()->getLongitude()) : ?>
			                map.addOverlay(marker);
			            <?php endif; 
						$i++; 
				    endif;

					if($model['pinSetting'] != 0) : 
						$getFriendsInCity = false; // if country has no friends use cities
						foreach($entry->getCountry()->getCityLocationList() as $city) :
						    $cityLocation = $city->getName().', '.$entry->getCountry()->getName(); 
						    $cityLink = $cityPhotoLink = ''; $cityJournals = $cityPhotoCount = $cityPhotos = $cityTips = 0;
						    if($cityTravelLogID = $city->getTravelLogID()) {
						        $cityTravelLog = new TravelLog($cityTravelLogID);
						        $cityLink = 'http://www.goabroad.net'.$entryHelper->getLink($cityTravelLog);
						        $cityJournals = $city->getJournalEntriesCount();
						        $entryTips = new EntryTips;
	    						$cityTips = count($entryTips->getList($cityTravelLogID));
	    						$cityPhotos = $cityTravelLog->getPhotos();
	    						$cityPhotoCount = count($cityPhotos);
	    						if($cityPhotoCount) {
	    							$cityPhoto = $cityPhotos[rand(0,$cityPhotoCount-1)]->getPhotoLink('thumbnail');
	    		            		$cityPhotoLink = 'http://www.goabroad.net/collection.php?type=traveler&ID='.$cityTravelLog->getTravelerID().'&context=travellog&genID='.$cityTravelLogID;
	    						}
					        }?>
			        		var point = new GLatLng(parseFloat(<?php echo $city->getLatitude(); ?>), parseFloat(<?php echo $city->getLongitude(); ?>));	
							var marker = createMarker(point,
									'<?php echo parseToXML($cityLocation) ?>',
									'<?php echo parseToXML($entry->getCountry()->getFlag()) ?>',
			                      	'<?php echo $cityPhoto ? parseToXML($cityPhoto) : '' ?>',
				                    parseInt(<?php echo $cityJournals ? $cityJournals : 0 ?>),
				                    parseInt(<?php echo $cityPhotoCount ? $cityPhotoCount : 0 ?>),
				                    parseInt(<?php echo $cityTips ? $cityTips : 0 ?>),
									'<?php echo parseToXML($cityLink) ?>',
									'<?php echo parseToXML($cityPhotoLink) ?>',
									'city','<?php echo $city->getID();?>', <?php echo $i ?>);

							<?php if ($city->getLatitude() && $city->getLongitude()) : ?> 
								map.addOverlay(marker); 
							<?php endif;
							$i++; 
						endforeach;
					endif;
	      		endforeach; ?>
	      	}
	    }

	    function createMarker(point, location, flag, photoUrl, journals, photos, tips, link, photoLink, type, locID,i) {
			arrlocationIDs[i] = locID;
			var pinIcon = type == 'country' ? iconBlue : iconTinyBlue;
	      	var marker = new GMarker(point, {title: location, icon: pinIcon, clickable: true});

	      	var myHtml;
			myHtml ='<div id="infoContainer_'+i+'"><img class="location_flag" src="'+flag+'" /><p class="country_name"><strong>'+location+'</strong></p>';

			myHtml += '<p class="location_details">';
			if(photos) { myHtml += '<img class="journal_photo" src="'+photoUrl+'" />'; }
			if(journals) { 
				myHtml += journals == 1 ? journals+' Journal Entry' : journals+' Journal Entries';
				myHtml += '<br /><a href="'+link+'" target="_blank">read latest &raquo;</a><br />';
			}
			if(photos) { 
				myHtml += photos == 1 ? photos+' Journal Photo' : photos+' Journal Photos';
				myHtml += ' <a href="'+photoLink+'" target="_blank">browse &raquo;</a><br />';
			}
			if(tips) { 
				myHtml += tips == 1 ? tips+' Travel Tip' : tips+' Travel Tips';
			}
			myHtml += '</p></div>';

			arrMyHTML[i] = myHtml;

			GEvent.addListener(marker, 'click', function() {
			  	$('location_info').innerHTML = myHtml;
				currentValue = i;
				getFriends(locID);
			});
			return marker;
	    }

	    var currentValue = 1;
		var totalCount = <?php echo $i ?> - 1;

		function mapEntriesPrevNextControl(action) {
			var i = currentValue;
			if(action == 'next') {
				i = i == totalCount ? 1 : i+1;
			} else {
				i = i == 1 ? totalCount : i-1;
			}
			currentValue = i;
			$('location_info').innerHTML = arrMyHTML[i];
			getFriends(arrlocationIDs[i]);
		}

		function getFriends(locationID) {
			var friendlist = $('friends').getElementsByTagName('ul');
			for(i=0; i<friendlist.length;i++){
				if(friendlist[i].className == 'friends_list'){
					friendlist[i].style.display = 'none';
				}
			}
			// checked if friends_list is empty
			if($('friendlist_'+locationID) != null){
				if($('friendlist_'+locationID).getElementsByTagName('li').length > 0){
					$('friends_header').style.display = 'block';
					$('friendlist_'+locationID).style.display = 'block';
				}else{
					$('friends_header').style.display = 'none';
				}
			}else{
				$('friends_header').style.display = 'none';
			}		
		}

		function $(elemId) {
			return document.getElementById(elemId);
		}
		
		function publishToWall(profileID){
			FB_RequireFeatures(["Connect"], function() {
                FB.Facebook.init(appkey, "xd_receiver.htm");
				
                FB.ensureInit(function() {
					var cityPhrase = "";
					var countryPhrase = "";

					if(cityCount > 0){ cityPhrase = (cityCount > 1) ? cityCount+" cities in" : cityCount+" city in"; }
					if(countryCount > 0){ countryPhrase = (countryCount > 1) ? countryCount+" countries" : countryCount+" country"; }
					
                    var attachment = {
                        'name': "Check out My Travel Map and see where I've travelled to!",
                        'href': 'http://apps.facebook.com/my-travel-map/index.php?id='+profileID,
                        'description': "I've been to "+cityPhrase+" "+countryPhrase+". Have you been to different places too? Share them with me on My Travel Map.",
                        'media': [{ 'type': 'image', 'src': 'http://bit.ly/dwqk9C', 'href': 'http://apps.facebook.com/my-travel-map'}]
                    };

                    FB.Connect.streamPublish('', attachment, "", null, "Let your friends know where you've been.");
                });
            });
		}
		
		FB_RequireFeatures(["XFBML"], function(){ FB.Facebook.init(appkey, "xd_receiver.htm"); });
	   	//]]>
		</script> 
</body>
</html>
