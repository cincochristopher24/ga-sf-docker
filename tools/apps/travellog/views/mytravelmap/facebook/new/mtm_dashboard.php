<style type="text/css">
 <?php echo htmlentities(file_get_contents('http://www.goabroad.net/facebook/mytravelmap/css/facebook_mtm_shared.css')); ?>
</style>
<div class="dashboard_wrapper">
	<div class="dashboard_header">
	    <h1> My Travel Map </h1>
	    <div class="tabs clearfix">
	    	<div class="center">
	    	<?php if($isOwnerUser) : ?>
	    		<div class="left_tabs" style="float:right">
	    			<ul class="toggle_tabs clearfix" id="toggle_tabs_unused" style="float:right;overflow:hidden;">
	    				<li class="first"> <a <?php if('dashboard'==$selected):?>class="selected"<?php else:?>href="index.php?id=<?php echo $user ?>" class="x"<?php endif;?>  title="My Travel Journals" id="dashboard" >
	    					<span>Dashboard </span>
	    					</a> </li>
	    				<li class=""> <a <?php if('invite'==$selected):?>class="selected"<?php else:?>href="index.php?action=invite&id=<?php echo $user ?>" class="x"<?php endif;?>  title="Share this App with your Friends" id="inviteFriends">
	    					<span>Invite Friends </span>
	    					</a> </li>
	    				<li class="_"> <a <?php if('synchronize'==$selected):?>class="selected"<?php else:?>href="index.php?action=sync&id=<?php echo $user ?>" class="x"<?php endif;?> title="Update your journals with ease." id="synchronize">
	    					<span>Synchronize </span>
	    					</a> </li>
	    				<li class="last"> <a <?php if('setting'==$selected):?>class="selected"<?php else:?>href="index.php?action=changeSetting&id=<?php echo $user ?>" class="x"<?php endif;?> title="Easily Configure your App to match your preference.">
	    					<span>Settings </span>
	    					</a> </li>
	    			</ul>
	    		</div>
	    	<?php else : ?>
				<!--<h5 style="margin: 0 0 20px 45px;"><fb:name uid="<?=$user ?>" possessive="true" /> travel map</h5> -->
			<?php endif; ?>
	    	</div>
	    </div>
	</div>
</div>
