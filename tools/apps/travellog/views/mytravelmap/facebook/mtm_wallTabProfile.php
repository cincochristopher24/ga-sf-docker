<?php
$fbnarrow = <<< Style
  <fb:narrow>
  <style>
  #wallContainer {
   	background: url(http://www.goabroad.net/facebook/mytravelmap/images/wallTabMap.png) #ffffff no-repeat;
    color: #ffffff;
    position: relative;
    height: 108px;
    width: 183px;
	overflow: hidden;
  }
  #wallContainer div {
    position: absolute;
  }
  #wallContainer div img {
    border-style: none;
  }
  
  img {
	filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
  }

  .blueMarker {
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinBlueSmall');
	width:22px;
	height:20px;
  }
  .shadowMarker {
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinShadowSmall');
	width:20px;
	height:16px;
  }

Style;

// set the individual countries' markers position in our CSS
$wallMap = new Map(array('x'=>183, 'y'=>183));
$entryCount = 0;
$countries = '';
$preselectedCtry = '';

if ($mapEntries) {
  foreach ($mapEntries as $entry) {
    $shadowDivID = 's'.$entryCount;
    $markerDivID = 'c'.$entryCount;
    $altMarkerDivID = 'ac'.$entryCount;

    $coordinates = $wallMap->lngLatToXYCoords($entry->getCountry()->getLongitude(), $entry->getCountry()->getLatitude(), 'walltab');

    $y = $coordinates['y'] + 4;
    $x = $coordinates['x'] - 1;

    $fbnarrow .= '#'.$shadowDivID. '{
      top: '.$y.'px;
      left: '.$x.'px;
      z-index: 0
    }';

    $x++;
    $fbnarrow .= '#'.$markerDivID. '{
      top: '.$y.'px;
      left: '.$x.'px;
      z-index: 5
    }';

    $y++;
    $fbnarrow .= '#'.$altMarkerDivID. '{
      top: '.$y.'px;
      left: '.$x.'px;
      z-index: 5
    }';

    $entryCount++;
  }//end for-each $this->mapEntries
  $fbnarrow .='</style>';
  $fbnarrow .= <<< Links
  <div style="text-align:right;padding-bottom:5px;">
  	<fb:visible-to-owner>
	    <a href="http://apps.facebook.com/my-travel-map/index.php?action=refreshProfile">Refresh</a> |
  	 </fb:visible-to-owner>                            	
	    <a href="http://apps.facebook.com/my-travel-map/index.php?id=$userID">View Larger Map</a>
  </div>	
Links;

  $fbnarrow .= '<div id="wallContainer" onclick="dummyFunction()">';

  //watch out for $entryCount
  $isCtrySelected = false;
  $entryHelper = new EntryHelper;
  for ($i = 0; $i<$entryCount; $i++) {
    $e = $mapEntries[$i];
    $ctryName = str_replace("&",'&amp;',$e->getCountry()->getName());
    $js = $e->getJournalEntriesCount();
    $ps = $e->getPhotosCount();
    $ts = $e->getTipsCount();
    
	if($e->getTravelLogID()){
		$travelLog = new TravelLog($e->getTravelLogID());
		$travel = new Travel($travelLog->getTravelID());
		$link = self::GOABROAD_DOTNET_URL.$entryHelper->getLink($travelLog);
		$country = "<a href='$link' target='_blank'>".$e->getCountry()->getName()."</a>";
	}
	else 
		$country = $e->getCountry()->getName();
	
    $fbnarrow .= <<< Marker
      <!-- Marker's shadow -->
      <div id="s$i" class="shadowMarker">
      	<img width="20" height='16' src="$pinShadowSmall"/>
      </div>
      <!-- Main marker -->
      <div id="c$i" class="blueMarker">
		<img title="{$e->getCountry()->getName()}" alt="{$e->getCountry()->getName()}" src="$pinBlueSmall" width="10" />
	  </div>
Marker;
	
    $countries .= $countries ? ', '.$country : $country;

  }//end-for
} else {    
  $fbnarrow .='</style>';	
  $fbnarrow .= <<< Links
  <div style="text-align:right;margin-bottom:5px;">
  	<fb:visible-to-owner>
	    <a href="http://apps.facebook.com/my-travel-map/index.php?action=refreshProfile">Refresh</a> |
	</fb:visible-to-owner>                            	
    <a href="http://apps.facebook.com/my-travel-map/index.php?id=$userID">View Larger Map</a>
  </div>	
Links;
  $fbnarrow .= 'No entry create one!';
}

$fbnarrow .= '</div>';//end container div

$fbnarrow .= <<< Script
<script>
  function dummyFunction() {}
  function showCountryInfo(id) {
    for (var i=0; i < $entryCount; i++) {
      if (pin = document.getElementById('ac' + i)) pin.setStyle('display', 'none');
      document.getElementById('c' + i).setStyle('display', 'block');
    }
    document.getElementById('ac' + id).setStyle('display', 'block');
    document.getElementById('c' + id).setStyle('display', 'none');
    document.getElementById('ctryInfoDiv').setInnerFBML(entries["ctry"+id]).setStyle('display', 'block');
  }
  function accentMarker(obj) {
    for (var i=0; i < $entryCount; i++) {
      if (mainPin = document.getElementById('c' + i)) {
        mainPin.setStyle({zIndex: '4'});
      }
      if (altPin = document.getElementById('ac' + i)) {
        altPin.setStyle({zIndex: '4'});
      }
    }
    obj.setStyle({display: 'block', zIndex: '5'});
  }
  function restoreMarker(obj) {
    obj.setStyle('display', 'block');
  }
</script>
Script;

$fbnarrow .= <<< CountryList
  <!-- COUNTRYLIST -->
  <div id="wish" style="font-size:9px;padding: 5px; margin: 0 0 5px 0; border: 1px solid #e6e6e6; background-color: #fafafa; color: #777777;margin-top:5px;">
    <span style="color: #60718B; font-weight: bold;">
	<fb:fbml version="1.1">
		<fb:name uid='$userID' firstnameonly='true' useyou='false'/>
	</fb:fbml>
	has traveled to ($entryCount): </span> $countries
    <div id="indicator" style="display: none">
      <img style="margin: 5px 0 0 0; float: left;" src="http://www.goabroad.com/facebook/traveltodo/images/loading.gif"><p style="float: left; margin: 10px 0 0 2px;">Loading...</p>
      <div style="clear: both;"></div>
    </div>
  </div>
  </fb:narrow>
<!-- end wish -->
CountryList;
?>