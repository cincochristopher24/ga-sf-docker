<?php $currentCityLocIDs = array(); ?>

<strong><?php echo $mapEntry->getCountry()->getName() ?></strong><br/>

<!-- CITY OF FOR EACH -->		
<?php $i=0; foreach($mapEntry->getCountry()->getCityLocationList() as $entry): ?>
	<? $displayVal = (!$entry->getIsEditable()) ? 'none' : 'block';	?>
   	<span id="city_<?php echo $entry->getID() ?>" class="editing_mode">
		<?php echo $entry->getName(); ?>
		<a title="Remove City" style="display: <?=$displayVal?>" class="delete_link" id="removeCity_<?php echo $entry->getID() ?>" onclick="removeCity(this)">
			Remove City
		</a>
	</span>

<?php $i++; $cityLocIDs[] = $entry->getID(); $currentCityLocIDs[] = $entry->getID(); endforeach; ?>
<!-- END CITY OF FOR EACH -->

<div id="addLocationLinks_<?php echo $locationID ?>" class="add_location_links">
	<span>Add:</span>
	<a href="javascript:void(0)" onclick="showCities(<?php echo $locationID ?>, 'citiesContainer_<?php echo $locationID ?>', '<?php echo implode('.', $currentCityLocIDs) ?>')">
		From List
	</a>
	<strong>|</strong>
	<a href="javascript:void(0)" onclick="displayModalMap('<?php echo $locationID ?>','<?php echo $mapEntry->getCountry()->getCountryID() ?>','<?php echo $mapEntry->getCountry()->getLatitude()."_".$mapEntry->getCountry()->getLongitude() ?>')">
		New Location
	</a>
</div>


<? if ($mapEntry->getIsEditable()) { ?>

<a href="javascript:void(0)" title="Remove Country" class="delete_link" alt="remove" id="removeCountry_<?php echo $locationID ?>" onclick="removeCountry(this,'<?php echo $mapEntry->getCountry()->getCountryID() ?>','<?php echo $mapEntry->getCountry()->getName() ?>')">
	Remove Country
</a>
<div style="clear:left"></div>
<? } ?>