<?php
//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('layoutID', 'main_pages');
Template::includeDependentJs('/min/f=js/prototype.js');
Template::includeDependentJs('js/interactive.form2.js');				// changed from interactive.form.js
Template::includeDependentJs('js/tools/common.js');
Template::includeDependentJs('js/utils/json.js');
Template::includeDependentJs('captcha/captcha.js');

$style = <<<STYLE
<script type="text/javascript">
	var Captcha = new CaptchaLoader({					
		imageTagID	: 'captchaImg',
		onLoad		: function(params){			
			$('encSecCode').value  = params['code'];
		},
		imgAttributes : {
			width : '200',
			height: '50'	
		}
	});
	
	// added by neri: 11-07-08
	
	function cancelReport(referer) {
		window.location.href = referer;
	}
</script>

<style type="text/css">
/*<!-- <[!CDATA[ -->*/
#main div.help {
	margin: 10px 30px 20px 410px;
	font-size: 14px;
	line-height: 1.8em;
	padding: 0 0 0 25px;
	max-width: 35em;
    background: url(/images/dotted.gif) left top repeat-y;	
}
@import url("css/main.css");
/*<!--]]> -->*/
</style>
STYLE;

Template::includeDependent($style);

?>

<!-- edits made by neri 11-07-08: added * to required fields -->

<div id="content_wrapper">
	<div id="section_report" class="section">
        <h2>Report Abuse</h2>
		<div class="content">			
		  <div class="report_cont">
			  <div class="form_box">
			  	<?php if ('ViewForm' == $action): ?>
				  <form name="frmReportAbuse" method="post" action="reportabuse.php" class="interactive_form">
					<ul class="form">
						<? if (isset($tID) || isset($pID) || isset($jID) || isset($jeID) || isset($aID) ) :  ?>
						   <!--li>
						  		<?/*= array_key_exists('UserOffense',$arrErrCode) ? $arrErrCode['UserOffense'] : ''*/ ?>
						   </li-->
							<li>
								<fieldset>
									<? if (isset($tID) || isset($pID) || isset($jID)|| isset($jeID) || isset($aID) ) :  ?>
										<legend>
											What do you find offensive about this user?
											<span class = "required"> * </span>
										</legend>
										<li>
									  		<?= array_key_exists('UserOffense',$arrErrCode) ? $arrErrCode['UserOffense'] : '' ?>
									   </li>
    								<?endif;?>
									<ul>
									   <? for($i=0;$i<count($arrReportOptions);$i++) { ?>
											<li>
												<input class="ga_interactive_form_field" name="chkreport[]" id="rad<?=$i?>" value="<?=$i?>" onclick="<!--no_other()-->" type="checkbox"
												<? if (array_key_exists('chkreport',$_POST) && in_array($i, $_POST['chkreport'])):  ?>checked <?endif;?> > <label for="rad<?=$i?>"><?= $arrReportOptions[$i]?></label>
											</li>
										<? } ?>
											<li>
												<input class="ga_interactive_form_field" name="chkOther" id="rad9" value="chkOther" onclick="<!--no_other()-->" <? if (array_key_exists('chkOther',$_POST)):  ?>checked <?endif;?> type="checkbox"><label for="rad9">Other</label> 
												<input class="ga_interactive_form_field other_box" id="txtOther" name="txtOther" onKeyPress="document.getElementById('rad9').checked = true;" <? if (array_key_exists('txtOther',$_POST)):?>value="<?=$_POST['txtOther']?>" <?endif;?>/>
											</li>
									</ul>
								</fieldset>
						   </li>

						   <li>
								<fieldset>
									<legend>
										Why do you find the content offensive?
										<span class = "required"> * </span>
									</legend>
									<?= array_key_exists('ContentOffense',$arrErrCode) ? $arrErrCode['ContentOffense'] : '' ?>
									<span>
										<textarea id="msg" class="" name="txtContent" cols="50" rows="6"><?= array_key_exists('txtContent',$_POST) ? $_POST['txtContent'] : '' ?></textarea>										
									</span>
								</fieldset>
							</li>

							<input type="hidden" name="tID" value="<?=isset($tID) ? $tID : 0?>">
							<input type="hidden" name="pID" value="<?=isset($pID) ? $pID : 0?>">
							<input type="hidden" name="jID" value="<?=isset($jID) ? $jID : 0?>">
							<input type="hidden" name="jeID" value="<?=isset($jeID) ? $jeID : 0?>">
							<input type="hidden" name="aID" value="<?=isset($aID) ? $aID : 0?>">
							<input type="hidden" name="cat" value="<?=isset($cat) ? $cat : '' ?>">
							<input type="hidden" name="genID" value="<?=isset($genID) ? $genID : 0?>">

						<? //elseif ($report == 'msg') : 
							elseif ($msgID > 0) :  ?>
							<li>
								<fieldset>
									<legend>
										<span>Why do you find this message offensive?</span>
										<span class = "required"> * </span>
									</legend>
									<?= array_key_exists('MsgOffense',$arrErrCode) ? $arrErrCode['MsgOffense'] : '' ?>
									<span>
										<textarea id="msg" class="" name="txtMessage" cols="50" rows="6"><?= array_key_exists('txtMessage',$_POST) ? $_POST['txtMessage'] : '' ?></textarea>										
									</span>
								</fieldset>
							</li>
							<input type="hidden" name="msgID" value="<?= $msgID ?>">
						<? endif; ?>

						<? if ($loggedUserID == 0) : ?>
							<li id="email">
								<fieldset>
								<legend for="email"> Email Address (optional):</legend>
								<?= array_key_exists('ValidEmail',$arrErrCode) ? $arrErrCode['ValidEmail'] : '' ?>					
								<input type="text" id="email" name="email" style="width:250px" <? if (array_key_exists('email',$_POST)):?>value="<?=$_POST['email']?>" <?endif;?> class="text" />
								<p class="supplement">Please include your email address if you wish to receive a response.
								Your personal information will remain strictly confidential and never
								sold, rented, leased nor shared with any person nor company.</p>
								</fieldset>
							</li>
						<? endif ; ?>	
						<li>
							<fieldset>
								<legend>
									Enter the code shown:
									<span class = "required"> * </span> 
								</legend>			
								<?= array_key_exists('MsgCaptcha',$arrErrCode) ? $arrErrCode['MsgCaptcha'].'<br/>' : '' ?> 							
								<input type="text" id="securityCode" name="securityCode" style="width:250px" value="" class="text"/> 														
							</fieldset>				
							<img  id="captchaImg" src="<?=$secSource?>" alt="Captha" border="1" /><br />
							<a href="/reportabuse.php?dt=<?=time()?>" onclick="Captcha.load();return false;">Refresh Image</a>
							<input type="hidden" id="encSecCode" name="encSecCode" value="<?=$secCode?>">										
						</li>							
					</ul>	
					<div class="button_box">	
						<input name="btnSubmit" id="btnSubmit" value="Submit Report" class="submit ga_interactive_form_field report_buts" type="submit">
						<input name="btnCancel" id="btnCancel" value="Cancel" class="submit ga_interactive_form_field report_buts" type="button" onclick = "return cancelReport('<?= $refererLink ?>')">
					</div>					
				  </form>

				  <?php else: ?>
					  <div>
						<? echo $Msg ?>
					  </div>
					  <a href = "<?= $refererLink?>"> Back to <?= $referer ?> </a>
				  <?php endif; ?>				
		  	  </div>
		  </div>
		</div>
	</div>
</div>