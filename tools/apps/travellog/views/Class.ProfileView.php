<?php
require_once("travellog/views/Class.AbstractView.php");
require_once("travellog/helper/Class.HelperFactory.php");
require_once("Class.HtmlHelpers.php");
class ProfileView extends AbstractView{
	function render(){
		$obj_factory = HelperFactory::instance();
		$obj_helper  = $obj_factory->create( HelperType::$PROFILE ); 
		$this->obj_template->set( 'obj_helper', $obj_helper );
		$template = ( $this->contents['obj_profile']->getIsAdvisor() )? 'tpl.EntryGroupInfo.php' : 'tpl.TravelerInfo.php';
		return  $this->obj_template->fetch($template);
	} 
}
?>
