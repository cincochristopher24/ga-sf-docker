<?php require_once 'travellog/helper/messageCenter/messageCenterHelper.php' ?>

<div id="messageNotification"></div>
<div id="main_content">
	<div><?isset($confirmation)?$confirmation->render():""?></div>
	<ul id="nav">
		<li id="theInbox" class="first active" ><a href="javascript:void(0)" onclick="showInbox()"><span>Inbox</span></a></li>
		<li id="theOutbox"><a href="javascript:void(0)" onclick="showOutbox()"><span>Outbox</span></a></li>
		<li class="actions">
			<?php if ($thereAreMoreMessages) : ?>
				<a class="viewall" href="messages.php">View All</a>
			<?php endif; ?>
				<a href="/messages.php?act=messageManyTravelers">Compose Message</a>
		</li>
	</ul>
	<div id="outboxContainer" style="display:none">
		<table id="outbox" class="message_table">
			<tbody>
				<?php if (count($outboxMessages) > 0) : ?>
					<?php foreach ($outboxMessages as $message) : ?>
						<?php
							includeView('singleOutboxMessage', array(
																	'message'       => $message,
																	'snippetLimit'  => $snippetLimit,
																	'canBeSelected' => $canOutboxBeSelected,
																	'canBeDeleted'  => $canOutboxBeDeleted,
																	'showRealName'  => $showRealName,
																));
						?>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td>
							<p class="help_text">No Messages</p>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
	<div id="inboxContainer">
		<table id="inbox" class="message_table">
			<tbody>
				<?php if (count($messages) > 0) : ?>
					<?php foreach ($messages as $message) : ?>
						<?php
							includeView('singleMessage', array(
															'message'        => $message,
															'snippetLimit'   => $snippetLimit,
															'canBeSelected'  => $canBeSelected,
															'canBeDeleted'   => $canBeDeleted,
															'showRealName'   => $showRealName,
															'showReadStatus' => $showReadStatus,
														))
						?>
					<?php endforeach; ?>
				<?php else: ?>
					<p class="help_text">
						<span>You don't have messages yet</span>. <a href="/messages.php?compose">Send a Message</a> to  friend.
					</p>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var isPowerful = <?php echo $isPowerful ? 1 : 0 ?>;
	var groupID = <?php echo isset($_GET['gID']) ? $_GET['gID'] : 0 ?>;
	var caller = 2;
</script>