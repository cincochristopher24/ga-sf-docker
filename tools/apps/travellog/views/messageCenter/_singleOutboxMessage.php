<?php require_once 'travellog/helper/gaTextHelper.php' ?>
<tr class="<?php echo $message->getCssClass() ?>" id="outboxMessage_<?php echo $message->getID() ?>">
	<?php if ($canBeSelected) : ?>
	<td class="checkbox">
		<input id="cbOutbox_<?php echo $message->getID() ?>" type="checkbox" />
	</td>
	<?php endif; ?>
	<td class="<?php echo $message->getCssClass() ?>">
		<span class="icon"></span>
	</td>
	<td class="sender_and_date">
		<span class="sender">
			<?php if ($showRealName && $message->getContentType() == 3) : ?>
				<?php echo stripslashes($message->getRecipientsRealNameLink()) ?>
			<?php else :?>
				<?php echo stripslashes($message->getRecipientsLink()) ?>
			<?php endif; ?>
		</span>
		<span class="date"><?php echo date('F j, Y g:i a', strtotime($message->getDateCreated())) ?></span>
	</td>
	<td class="subject">
		<div class="wrap">
			<div id="lOutbox_<?php echo $message->getID() ?>" onclick="doShowOutbox('<?php echo $message->getID() ?>')">
				<a class="text read" href="javascript:void(0)"><?php echo stripslashes($message->getTitle()) ?></a>
			</div>
			<div class="snippet" id="sOutbox_<?php echo $message->getID() ?>">
				<span><?php echo limitString(stripslashes($message->getContent()), $snippetLimit) ?></span>
			</div>
			<div style="display:none;" id="cOutbox_<?php echo $message->getID() ?>">
				<?php echo nl2br(stripslashes($message->getContent())) ?>
			</div>
		</div>
	</td>
	<?php if ($canBeDeleted) : ?>
	<td class="delete">
		<a href="javascript:void(0)" onclick="showDeletePopup('deleteOutboxMessage(\'<?php echo $message->getID() ?>\')')"></a>
	</td>
	<?php endif; ?>
</tr>