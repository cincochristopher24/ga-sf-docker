<?php require_once 'travellog/helper/gaTextHelper.php' ?>
<tr class="
	<?php if ($showReadStatus) : ?>
		<?php echo $message->isRead() ? 'read' : 'unread' ?>
	<?php else : ?>
		read
	<?php endif; ?>
	"
	id="inboxMessage_<?php echo $message->getID() ?>">
	<?php if ($canBeSelected) : ?>
	<td class="checkbox">
		<input id="cb_<?php echo $message->getID() ?>" type="checkbox" />
	</td>
	<?php endif; ?>
	<td class="<?php echo $message->getCssClass() ?>">
		<span class="icon"></span>
	</td>
	<td class="sender_and_date">
		<span class="sender <?php if ($message->isRead()) : ?>read<?php endif; ?>">
			<?php echo $message->getSenderLink() ?>
			<?php if ($showRealName && $message->getContentType() == 3) : ?>
				<span style="font-color:gray"><em><?php echo stripslashes($message->getSenderRealName()) ?></em></span>
			<?php endif; ?>
		</span>
		<span class="date"><?php echo date('F j, Y g:i a', strtotime($message->getDateCreated())) ?></span>
	</td>
	<td class="subject">
		<div class="wrap">
			<div>
				<?php if( 5 == $message->getContentType() ): ?>
					<?php echo stripslashes($message->getTitle()) ?>
				<?php else: ?>
					<a  id="l_<?php echo $message->getID() ?>" class="text
					<?php if ($message->isRead()) : ?>
						read
					<?php endif; ?>
					" href="javascript:void(0)" onclick="doShow('<?php echo $message->getID() ?>')"><?php echo htmlspecialchars(stripslashes($message->getTitle()),ENT_QUOTES) ?></a>
				<?php endif; ?>
			</div>
			<div style="display:none;" id="c_<?php echo $message->getID() ?>">
				<?php echo nl2br(htmlspecialchars(stripslashes($message->getContent()),ENT_QUOTES)) ?>
			</div>
			<?php if($message->hasContent()) : ?>
				<span id="v_<?php echo $message->getID() ?>" onclick="doShow('<?php echo $message->getID() ?>')">
					<a id="a_<?php echo $message->getID() ?>" href="javascript:void(0)">View</a>
				</span>
			<?php endif; ?>
			<?php if ($message->isReplyAllowed()) : ?>
				<span onclick="showReplyMessageForm('<?php echo $message->getID() ?>')">
					| <a href="javascript:void(0)">Reply</a>
				</span>	
				<div id="reply_<?php echo $message->getID() ?>" class="reply_section" style="display:none;">
					<form method="post" action="messages.php?action=replyToMessage">
					<table>
						<tbody>
							<tr>
								<td>
									<label>To: </label>
								</td>
								<td>
									<span>
										<?php echo $message->getSender() ?>
										<?php if ($showRealName && $message->getContentType() == 3) : ?>
											<span style="font-color:gray"><em><?php echo stripslashes($message->getSenderRealName()) ?></em></span>
										<?php endif; ?>
									</span>
								</td>
							</tr>

							<tr>
								<td>
									<label for="replySubject_<?php echo $message->getID() ?>">Subject:</label>
								</td>
								<td>
									<?
									$re = "";
									$numRe = 0;
									$title = htmlspecialchars(stripslashes($message->getTitle()),ENT_QUOTES);
									if(!preg_match("/^(Re \[\d+\]:)/",$title)):
										$numRe = substr_count($title,'Re: ') + 1;
										$re = ($numRe == 1) ? 'Re: ' : 'Re ['.$numRe.']: ';
										$re .= (preg_replace("/^(Re: )+((.)*)/","\\2",$title));
									else:
										$numRe = intval(preg_replace("/^(Re \[(\d+)\]:)/","\\2",$title))+1;
										$re = "Re [".$numRe."]: ".(preg_replace("/^(Re \[\d+\]:)((.)*)/","\\2",$title));
									endif;
									
									?>
									<input id="replySubject_<?php echo $message->getID() ?>" class="text" type="text" name="subject" value="<?php echo $re ?>" />
								</td>
							</tr>
							<tr>
								<td><label for="replyMessage_<?php echo $message->getID() ?>">Message:</label></td>
								<?php
									$signature = "";
									if(isset($owner)):
										if($owner->getSignature() != ""):
											$signature = "\n\n\n\n________________\n\n".$owner->getSignature();
										endif;	
									endif;
								?>
								<td><textarea id="replyMessage_<?php echo $message->getID() ?>" name="message"><?=$signature?></textarea></td>
							</tr>
							<tr>
								<td colspan="2">
									<input type='checkbox' name='chkSendNotification' id='chkSendNotification_<?php echo $message->getID() ?>' checked />
									<label for='chkSendNotification_<?php echo $message->getID() ?>'>Include your message in the notification?</label>
									<p class="supplement">
										We send an email notification to the recipient to inform them that you've sent a message.<br /> Checking the box will include your message in that notification.
									</p>
								</td>
							</tr>
							<tr>
								<td><input type="button" id="replyButton_<?php echo $message->getID() ?>" value="Send reply" onclick="sendReply('<?php echo $message->getID() ?>');"/></td>
								<td><input type="button" value="Cancel" onclick="hideReplyMessageForm('<?php echo $message->getID() ?>')" /></td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" id="replyRecipient_<?php echo $message->getID() ?>" name="recipientID" value="<?php echo $message->getSenderID() ?>" />
					</form>
				</div>
			<?php endif; ?>
		</div>
	</td>
	<td class="delete">
		<?php if ($canBeDeleted) : ?>
			<span id="deleteMessage_<?php echo $message->getID() ?>">
				<a href="javascript:void(0)" onclick="showDeletePopup('deleteMessage(\'<?php echo $message->getID() ?>\')')"></a>
			</span>
		<?php endif; ?>	
	</td>
</tr>