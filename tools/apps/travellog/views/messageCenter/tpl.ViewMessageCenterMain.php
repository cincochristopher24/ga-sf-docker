<?php require_once 'travellog/helper/gaTextHelper.php' ?>
<?php require_once 'travellog/helper/messageCenter/messageCenterHelper.php' ?>
<!--?php Template::includeDependentJs('/min/f=js/prototype.js'); ?-->

<?php echo $prof_comp->render() ?>
<?php $subNavigation->show() ?>

<div id="content_wrapper" class='layout_2'>
	<div id="message_center" class="section">
		<?php if ($isPowerful) : ?>
		<h2>
			<span>Message Center</span>
		</h2>
		<?php endif; ?>
		<?php if (isset($_SESSION['flashMessage'])) : ?>
			<div class="confirmation">
				<?php echo $_SESSION['flashMessage'] ?>
				<?php unset($_SESSION['flashMessage']) ?>
			</div>
		<?php endif;?>
		<div id="messageNotification">
			<?php if (isset($confirmation)) : ?>
				<?php $confirmation->render() ?>
			<?php endif; ?>
		</div>
		
		<div id="main_content">
			<ul id="nav">		
				<?php if ($showOutbox) : ?>
				<li id="theInbox" class="first
					<?php if(!$highlightOutbox) : ?> 
						active
					<?php endif; ?>
					" ><a href="javascript:void(0)" onclick="showInbox()"><span>Inbox</span></a></li>
					<li id="theOutbox"
						<?php if ($highlightOutbox) : ?>
							class="active"
						<?php endif; ?>
					>
					<a href="javascript:void(0)" onclick="showOutbox()"><span>Outbox</span></a></li>
				<?php endif; ?>
			<?php if ($canComposeMessage) : ?>
				<?php if(isset($_GET['gID'])): ?>
					<?php if( isset($isClub) && $isClub ): ?>
						<li class="actions"><a href="messages.php?act=messageMembers&gID=<?php echo $_GET['gID'] ?>">Compose a Message</a></li>
					<?php else: ?>
						<li class="actions"><a href="messages.php?act=messageStaffOfManyGroups&gID=<?php echo $_GET['gID'] ?>">Compose a Message</a></li>
					<?php endif; ?>
				<?php else: ?>	
					<li class="actions"><a href="messages.php?act=messageManyTravelers">Compose a Message</a></li>
				<?php endif;?>
			<?php endif; ?>
			
			<?php if ($canPostNews) : ?>
				<li class="actions"><a href="news.php?gID=<?php echo $_GET['gID'] ?>">Post News</a></li>
			<?php endif; ?>
			</ul>
			
			<?php if ($canUseTopActions) : ?>
			<div class="menu" id="outboxMenu" style="display:none;">
				<ul id="message_selector" class="message_action">
					<li class="action">
						<label>Actions</label>
	 					<a href="javascript:void(0)" onclick="showBatchDeleteOutboxPopup()">Delete</a>
					</li>
				</ul>
			</div>
			
			<div id="inboxMenu" class="menu">
				<ul id="message_selector" class="message_action">
				<li class="selector">
						<label>Select</label>
						<a id="messageFilter" onclick="doSelectNone()" href="javascript:void(0)">None</a> ,
						<a id="messageFilter" onclick="doSelectRead()" href="javascript:void(0)">Read</a> ,
						<a id="messageFilter" onclick="doSelectUnRead()" href="javascript:void(0)">Unread</a> ,
						<a id="messageFilter" onclick="doSelectAll()" href="javascript:void(0)">All</a>
				</li>
						<!--<select id="messageFilter" class="choices" name="messageFilter" onchange="selectMessages()"
							<option value="none" selected="selected">None</option>												
							<option value="read">Read</option>
							<option value="unread">Unread</option>
							<option value="all">All</option>
						</select -->
				<li class="action">
						<label>Actions</label>
						<a href="javascript:void(0)" onclick="batchMarkAsUnread()">Mark as Unread</a> ,
						<a href="javascript:void(0)" onclick="batchMarkAsRead()">Mark as Read</a> ,
	 					<a href="javascript:void(0)" onclick="showBatchDeletePopup()">Delete</a>
	 			</li>		
				</ul>
			</div>
			<?php endif; ?>
	
			<?php if ($showOutbox) : ?>
			<div id="outboxContainer" style="display:<?php echo $highlightOutbox ? '' : 'none' ?>">
				<table id="outbox" class="message_table">
					<tbody>
						<?php if (count($outboxMessages) > 0) : ?>
							<?php foreach ($outboxMessages as $message) : ?>
								<?php
									includeView('singleOutboxMessage', array(
																			'message'       => $message,
																			'snippetLimit'  => $snippetLimit,
																			'canBeSelected' => $canOutboxBeSelected,
																			'canBeDeleted'  => $canOutboxBeDeleted,
																			'showRealName'  => $showRealName,
																		));
								?>
							<?php endforeach; ?>
						<?php else: ?>
							<p class="help_text"> <span>No Messages yet</span>.</p>
						<?php endif; ?>
					</tbody>
				</table>
				<div id="pagelinks_outbox">
					<?php $outboxPager->showPagination() ?>
				</div>
			</div>
			<?php endif; ?>
			<div id="inboxContainer" style="display:<?php echo $highlightOutbox ? 'none' : '' ?>">
				<table id="inbox" class="message_table">
					<tbody>
						<?php if (count($messages) > 0) : ?>
							<?php foreach ($messages as $message) : ?>
								<?php 
									includeView('singleMessage', array(
																	'message'        => $message,
																	'snippetLimit'   => $snippetLimit,
																	'canBeSelected'  => $canBeSelected,
																	'canBeDeleted'   => $canBeDeleted,
																	'showRealName'   => $showRealName,
																	'showReadStatus' => $showReadStatus,
																))
								?>
							<?php endforeach; ?>
						<?php else: ?>
							<p class="help_text"> <span>No Messages yet</span>.</p>
						<?php endif; ?>
					</tbody>
				</table>
				<div id="pagelinks">
					<?php $pager->showPagination() ?>
				</div>
			</div>
		</div>
	</div>
	<?/*div id="pagelinks">
		<?php $pager->showPagination() ?>
	</div*/?>
</div>
<script type="text/javascript">
	var isPowerful = <?php echo $isPowerful ? 1 : 0 ?>;
	var groupID = <?php echo isset($_GET['gID']) ? $_GET['gID'] : 0 ?>;
	var caller = 1;
	
	<?	if( $showOutbox && isset($_GET["outbox"]) ): ?>
		showOutbox();
	<?	endif; ?>
</script>