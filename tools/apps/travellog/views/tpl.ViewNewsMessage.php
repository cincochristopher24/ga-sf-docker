<div id="content_wrapper">
    <div id="composePanel" class="widePanel">
<?php 
	echo $contents['pFactory']->get_view()->render();
	$contents['subNavigation']->show(); 
?>

<?php switch($contents['displayMessage']): 
		case AbstractNewsController::UNKNOWN_SOURCE:
?>
			<p class="error_notice">Unknown Source</p>
<?php 		break;
		case AbstractNewsController::PERMISSION_DENIED: ?>
			<p class="error_notice">Permission denied in creating news</p>
<?php		break;			
		default:
?>			
        <div class="confirmation">
            <div class="content">
			    <p>Your news article has successfully been saved.</p>
		    </div>
		</div>
		<p><a href="/messages.php?gID=<?php echo $groupID ?>" class="button"><span>Return to the Message Center</span></a></p>
		<!--
		<? if (isset($_SESSION['gID']) && $_SESSION['gID'] > 0): ?>
            <p><a href="/messages.php?gID=<?=$_SESSION['gID']?>" class="button"><span>Return to the Message Center</span></a></p>
        <? endif; ?>
        -->
<?php endswitch;?>

<!-- <p><a href="<?php echo $backUrl;?>">Back to <?php echo $backTag?></a></p> -->
    </div>
</div>
