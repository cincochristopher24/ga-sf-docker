<?php
	// Added custom title & meta description - icasimpan Apr 25,2007
	Template::setMainTemplateVar('title', $grpName . ' Members List - GoAbroad Network: Online Community for Travelers');
	Template::setMainTemplateVar('metaDescription', 'Check out the ' . $grpName . 's members on the GoAbroad Network');

	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');
	Template::setMainTemplateVar('layoutID', 'members');
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs("/js/moo1.ajax.js");
	Template::includeDependentJs("/js/groupMembersManager.js");
	
	Template::includeDependentJs("/js/tools/tools.js");
	
	$code = <<<BOF
<script type="text/javascript">
	var manager = new groupMembersManager();
	tool.AddEvent(window,'load',function(){	manager._useLoadingMessage();});
</script>
BOF;
	Template::includeDependent($code);
	
	Template::includeDependentJs('/js/manageMembers.js');
	Template::includeDependentJs('/js/utils/Class.Request.js');
	Template::includeDependentCss('/css/manageMembers.css');
	
	$iManageMembers =<<<BOF
		<script type = "text/javascript">
			//<!--
			window.onload = function(){
				manageMembers.initialize();
				manageMembers.setGroupId($grpID);
				manageMembers.createBox();
			}
			//-->
		</script>
BOF;
	


	Template::includeDependent($iManageMembers);
	
?>

<? $subNavigation->show(); ?>


<div class="area_wrapper" id="content_wrapper">
	<div class="area" id="top">
		

			<?  if (TRUE == $isAdminLogged ) : ?>
		   			<div class="member_box section" id="add_new_members">
							<h2><span>Add New Member to <?=$grpName?></span></h2>

							<div class="member_box_content content" id="member_box_content2">
								<?=$grpMemberPanelTemplate->render(GroupMembersPanel::$SEARCHMEMBERSFORM)?>						
							</div>																								
							<div class="foot"> </div> 					
					</div>	<!-- end section -->
			<? endif; ?>			
		
		
	<div id="wide_column">
		<div class="member_box section" id="members_list">
			<h1 class="big_header"><span><em><?=$grpName?></em> Members</span></h1>
			<?  if (TRUE == $isAdminLogged ) : ?>
				<ul class="tabs">
					<li <?= (!isset($newRequest)) ? 'class="active"' : '' ?>>
						<a href="javascript:void(0)" onclick="manager._allMembers('grpID=<?=$grpID?>');" id="allMembers">All</a>
					</li>
					<li><a href="javascript:void(0)" onclick="manager._members('grpID=<?=$grpID?>');" id="grpMembers">Members</a></li>
					<? if ($isSubGroup) : ?>
						<li><a href="javascript:void(0)" onclick="manager._staff('grpID=<?=$grpID?>');" id="grpStaff">Staff</a></li>					
					<?endif; ?>
					<li><a href="javascript:void(0)" onclick="manager._pendingInvites('grpID=<?=$grpID?>');" id="pendingInvites">Pending Invites</a></li>
					<li <?= (isset($newRequest)) ? 'class="active"' : '' ?> ><a href="javascript:void(0)" onclick="manager._pendingRequests('grpID=<?=$grpID?>');" id="pendingRequests">Requests to Join</a></li>					
				</ul>
			<? endif; ?>
			<div id="statusPanel1" class="widePanel content" style="display: none;">
				<p class="loading_message">
					<span id="imgLoading">
						<img alt="Loading" src="/images/loading.gif"/>
					</span>
					<em id="statusCaption">Loading data please wait...</em>
				</p>
			</div>
			<div class="member_box_content content"  id="member_box_content" >		
				<ul class="elements" id="box_container">
					<?= (isset($newRequest)) ? $grpMemberPanelTemplate->render(GroupMembersPanel::$PENDINGREQUESTS) : $grpMemberPanelTemplate->render(GroupMembersPanel::$ALLMEMBERS) ?>
				</ul>				
			</div>
			<div class="foot"></div>	
		</div>
	</div>


	<? if (TRUE == $isAdminLogged ) : ?>	
	<div id="narrow_column">
		<div class="member_box section">
				<h2><span>Membership History</span></h2>
				<ul style="" class="tabs">
						<li style="" class="active"><a href="javascript:void(0)" onclick="manager._memUpdates('grpID=<?=$grpID?>');" id="memUpdates">Updates</a></li>
						<li><a href="javascript:void(0)" onclick="manager._deniedInvites('grpID=<?=$grpID?>');" id="deniedInvites">Denied Invitations</a></li>															
				</ul>
				<div id="statusPanel3" class="widePanel content" style="display: none;">
					<p class="loading_message">
						<span id="imgLoading">
							<img alt="Loading" src="/images/loading.gif"/>
						</span>
						<em id="statusCaption">Loading data please wait...</em>
					</p>
				</div>
				
				<div class="member_box_content content">
					<div id="mem_updates">
						<?=$grpMemberPanelTemplate->render(GroupMembersPanel::$MEMUPDATES)?>
					</div>
				</div>
				<div class="foot"></div>
			</div>
		</div>		
	<? endif; ?>
	<div class="clear"></div>
</div>
