<?
	/**
	 * tpl.ViewMessageNavigation.php
	 * @author - marc
	 * Nov 6, 2006
	 */
	 
	 require_once("Class.Constants.php");

?>
<div class="section" id="quick_tasks">
	
	<div class="content" id="messages_navigation">
		
		<ul class="links">
			<? foreach($contents["viewFolders"] as $folder): ?>
				<? switch( $folder) :
						case constants::INBOX : ?>
							<li>
								<? if( $contents["active"] == constants::INBOX ): ?><strong><? endif;?>
									<a href="messages.php">
										Inbox <? if ( $contents["nNewMsg"] ) echo "(" . $contents["nNewMsg"] . ")" ?>
									</a>
								<? if( $contents["active"] == constants::INBOX ): ?></strong><? endif;?>
							</li>
						<? 	break;
						case constants::SENT_ITEMS : ?>
							<li>
								<? if( $contents["active"] == constants::SENT_ITEMS ): ?><strong><? endif;?>
									<a href="messages.php?sent">
										Sent Items
									</a>
								<? if( $contents["active"] == constants::SENT_ITEMS ): ?></strong><? endif;?>
							</li>
						<? 	break;
						case constants::INQUIRY : ?>
							<li>
								<? if( $contents["active"] == constants::INQUIRY ): ?><strong><? endif;?>
									<a href="messages.php?inquiry">
										Inquiry <? if ( $contents["nIncNewMsg"] ) echo "(" . $contents["nIncNewMsg"] . ")" ?>
									</a>
								<? if( $contents["active"] == constants::INQUIRY ): ?></strong><? endif;?>
							</li>
						<? 	break;
						case constants::DRAFTS : ?>
							<li>
								<? if( $contents["active"] == constants::DRAFTS ): ?><strong><? endif;?>
									<a href="messages.php?drafts">
										Drafts
									</a>
								<? if( $contents["active"] == constants::DRAFTS ): ?></strong><? endif;?>
							</li>
						<? 	break;
						case constants::TRASH : ?>
							<li class="last">
								<? if( $contents["active"] == constants::TRASH ): ?><strong><? endif;?>
								<a href="messages.php?trash">
									Trash
								</a> 
								<? if( $contents["nTrashMsg"] ): ?> 
									<a href="messages.php?delete&amp;deleteAll&amp;<?= $contents["active"]; ?>" onclick="javascript: return confirm('This will delete all your messages...');">[ empty ]</a>  
								<? endif; ?>
								<? if( $contents["active"] == constants::TRASH ): ?></strong><? endif;?>
							</li>
						<? break;
				  endswitch;								
			endforeach; ?>
		</ul>
	</div>
</div>
