<div class="stepContainer stepGNav">
	<p id="stepText" class="jLeft" style="padding: 3px 0; <?php if(!$showSteps): ?>display:none;<?php endif;?>"><strong>Thank you for creating a group on <?php echo $siteName ?></strong>. Now the fun begins! Here is how we suggest you proceed with your group.</p><a href="javascript:void(0);" class="stepOption jRight <?php if(!$showSteps): ?>stepOptionShow<?php endif;?>"><img src="/images/loading_small.gif" id="hideShowLoader" style="display:none;padding:2px 0;" class="jRight"/><span id="stepOptionText"><?php if(!$showSteps): ?>Show Steps<?php else:?>Hide Steps<?php endif;?></span></a>
  	
	<div id="thisCont" class="clear">
		<ul id="steps_carousel" class="stepGBox jcarousel-skin-tango" <?php if(!$showSteps): ?>style="display:none;"<?php endif;?>>
			<li id="one">
				<a href="/group.php?gID=<?php echo $groupID;?>&mode=edit" class="stepBox <?php if($steps['step1']): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepOne stepThreeLine"></span>
						Complete Group Profile
					</span>
					<span class="stepDetail">Add your group/ company description, logo, and set global privacy preferences.</span>
				</a>
			</li>
			<li id="two">
				<a href="/group.php?mode=add&pID=<?php echo $groupID;?>" class="stepBox<?php if($steps['step2']): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepTwo"></span>
						Create Subgroups
					</span>
					<span class="stepDetail">Create a sub-group for every unique program or travel group.</span>
				</a>
			</li>
			<li id="three">
				<a href="/members.php?gID=<?php echo $groupID;?>&mode=4" class="stepBox<?php if($steps['step3']): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">	
						<span class="stepNumber stepThree"></span>
						Add <br>Staff
					</span>
					<span class="stepDetail">Add other advisors, faculty or members to help manage your group.</span>
				</a>
			</li>
			<li id="four">
			<?/*	<a href="/subgroups.php?gID=<?php echo $groupID;?>" class="stepBox<?php if($steps['step4']): ?> stepDone<?php endif; ?>">   */?>
				<a href="/group-pages.php?gID=<?php echo $groupID;?>" class="stepBox<?php if($steps['step4']): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">	
						<span class="stepNumber stepFour"></span>
						Sub-group Descriptions
					</span>
					<span class="stepDetail">Give the sub-group a name and description.</span>
				</a>
			</li>
			<li id="five">
				<?php $link = ($redirectToPhoto) ? '/collection.php?type=group&ID='.$groupID : '/video.php?gID='.$groupID; ?>
				<a href="<?php echo $link; ?>" class="stepBox<?php if($steps['step5']): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepFive"></span>
						Add Photos and/or Videos
					</span>
					<span class="stepDetail">Add stock photos of the location/program to start the group going or wait for photos from members.</span>
				</a>
			</li>
			<li id="six">
				<a href="/resourcefiles.php?action=add&cat=admingroup&genID=<?php echo $groupID; ?>" class="stepBox<?php if($steps['step6']): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepSix"></span>
		                Upload Documents
					</span>
					<span class="stepDetail">Documents can be private for the specific sub-group members or viewable by the public. Use this function to distribute files such as forms, itineraries, and pre-departure information.</span>
			</a> 
			</li>
	        <li id="seven">
	            <a href="/event.php?action=add&gID=<?php echo $groupID; ?>" class="stepBox<?php if($steps['step7']): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
		                <span class="stepNumber stepSeven"></span>
		                Create <br>Events
		            </span>
		        	<span class="stepDetail">Add important dates to the calendar to notify the group of pending events.</span>
	        	</a> 
	        </li>
	       <li id="eight">
	            <a href="javascript:void(0);" class="stepBox<?php if($steps['step8']): ?> stepDone<?php endif; ?>" onclick="ImportantLinks.setAction('add'); ImportantLinks.showAddBox()">
	                <span class="stepTitle">
		                <span class="stepNumber stepEight stepThreeLine"></span>
		                Add <br>Important Links
		            </span>
		        	<span class="stepDetail">Give members links to important resources in external web sites.</span>
	        	</a> 
	        </li> 
	       <li id="nine">
	            <a href="/discussion_board/home/<?php echo $groupID; ?>" class="stepBox<?php if($steps['step9']): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
		                <span class="stepNumber stepNine stepFourLine"></span>
		                Pre-<br>populate Knowledge Base/FAQs
		            </span>
		        	<span class="stepDetail">The knowledge base / discussion boards will be one of the most valuable tools in the network where members can search for FAQs with answers from their advisors and peers.</span>
	        	</a> 
	        </li>
	       <li id="ten">
	            <a href="/members.php?gID=<?php echo $groupID; ?>&mode=3" class="stepBox <?php if($steps['step10']): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
		                <span class="stepNumber stepTen stepThreeLine stepDD"></span>
		                Invite/<br>Add Members
		            </span>
		        	<span class="stepDetail">Add groups of email address in bulk and assign them to their appropriate sub-groups.</span>
	        	</a> 
	        </li>
	     </ul>
	</div>
</div>