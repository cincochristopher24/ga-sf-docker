<ul class="feedlist">
	<?php
		foreach($feeds as $feed):
			require_once("travellog/views/groupfeeds/Class.ViewFeed".$feed->getFeedSectionLabel().".php");
			$class_file = "ViewFeed".$feed->getFeedSectionLabel();
			$feedView = new $class_file;
			try{
				$feedView->create($feed,$group);
				$feedView->setLoggedUser($loggedUser);
				$feedView->prepare();
				$feedView->render();
			}catch(exception $e){
				if( $_SERVER["SERVER_NAME"] == "ga-web03.goabroad.net" ||
					$_SERVER["SERVER_NAME"] == "staging.goabroad.net" ||
					$_SERVER["SERVER_NAME"] == "advisor.goabroad.net" ||
					strpos($_SERVER["SERVER_NAME"],".local") ){
					echo "notice in local, staging and ga-web03 servers and in advisor test cobrand only>>> ".$e->getMessage()."<br />";
				}
			}
		endforeach;
	?>
	<?	if( !count($feeds) ): ?>
		<li class="container ajax_journal">     
			<span class="box">
				<p class="highlights feedReply">There aren't any feeds for this section.</p>
			</span>
		</li>
	<?	endif; ?>
</ul>

<? if ( $paging->getTotalPages() > 1 ): ?>
<img id="LOADING_FPAGE" src="/images/loading_small.gif" style="display:none;"/>&nbsp;
<?	$start = $paging->getStartRow() + 1; 
	$end = $paging->getEndRow(); 
	$paging->showPagination();
endif; ?>