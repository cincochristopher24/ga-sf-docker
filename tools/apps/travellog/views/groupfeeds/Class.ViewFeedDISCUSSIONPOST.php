<?php
	require_once("travellog/views/groupfeeds/Class.AbstractViewFeed.php");

	class ViewFeedDISCUSSIONPOST extends AbstractViewFeed{
		public function prepare(){
			require_once("travellog/model/DiscussionBoard/Class.Post.php");
			require_once("travellog/model/Class.Traveler.php");
			try{
				$postID = $this->mFeed->getSectionID();
				$discussionID = $this->mFeed->getSectionRootID();
				
				$post = Post::getInstance($postID);
				$discussion = $post->getParentDiscussion();
				$topic = $discussion->getParentTopic();
				
				$traveler = new Traveler($post->getPosterID());
				
				if( $traveler->isSuspended() ){
					throw new Exception("Suspended travelerID: ".$this->mFeed->getTravelerID()." FeedID: ".$this->mFeed->getFeedID());
				}
				
				$this->mTemplate->set("post",$post);
				$this->mTemplate->set("discussion",$discussion);
				$this->mTemplate->set("topic",$topic);
				$this->mTemplate->set("traveler",$traveler);
				$this->mTemplate->set("feed",$this->mFeed);
			}catch(exception $e){
				throw $e;
			}
		}
		public function render(){
			parent::render();
		}
	}