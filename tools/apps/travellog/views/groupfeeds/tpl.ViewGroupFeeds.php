<?
	require_once('travellog/helper/Class.NavigationHelper.php');
	if(NavigationHelper::isCobrand())
		Template::setMainTemplateVar('page_location', 'Home');
	else
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
?>
<?=$contextInfo->render()?>		
<?=$subNavigation->show()?>
<?=$content->render()?>