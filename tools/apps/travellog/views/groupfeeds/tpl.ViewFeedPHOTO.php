<?php

	$uploadDate = GaDateTime::create($feed->getFeedDate());

	$rowsLimit = new RowsLimit(10,0);
	$photosArray = $photoAlbum->getPhotosUploadedByDay($rowsLimit,$feed->getFeedDate());
	$photos = $photosArray["photos"];
	$photoCount = $photosArray["recordCount"];
	if( !$photoCount ){
		throw new exception("photos uploaded not found; possible undeleted photo upload feed");
	}

	$owner = $traveler;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$owner = new AdminGroup($feed->getGroupID());
		$profileLink = $owner->getFriendlyURL();
		$albumLink = "/collection.php?type=group&ID=".$owner->getGroupID()."&context=photoalbum&genID=".$photoAlbum->getPhotoAlbumID();
		$photoCountLabel = 1 < $photoCount ? "$photoCount Photos were" : "1 Photo was";
	}else{
		$albumLink = "/collection.php?type=traveler&ID=".$photoAlbum->getCreator()."&context=traveleralbum&genID=".$photoAlbum->getPhotoAlbumID();
		$photoCountLabel = 1 < $photoCount ? "$photoCount Photos" : "1 Photo";
	}

	$isAdvisorAlbum = FALSE;
	$creator = $traveler;
	$profileLink = $creator->getFriendlyUrl();
	/*if( $group->getAdministratorID() == $traveler->getTravelerID() ){
		$isAdvisorAlbum = TRUE;	
		$creator = new AdminGroup($traveler->getAdvisorGroup());
		$profileLink = $creator->getFriendlyURL();
	}*/

	$isStaffAlbum = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP && !$isAdvisorAlbum && $owner->isStaff($feed->getTravelerID()) ){
		$isStaffAlbum = TRUE;
		$creator = new AdminGroup($feed->getGroupID());
		$profileLink = $creator->getFriendlyURL();
	}

	$sameGroup = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$sameGroup = $group->getGroupID() == $feed->getGroupID();
	}
?>	

<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>"> 
	<?	if( $isAdvisorAlbum || $isStaffAlbum ): ?> 
		<a href="<?=$profileLink?>" class="">
			<img src="<?=$creator->getGroupPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>   
	<?	else: ?>
		<a href="<?=$profileLink?>">
			<img class="jLeft" src="<?=$creator->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>
	<?	endif; ?>
	<span class="box">
		<p class="highlights feedPhoto">
			<?	if( $feed->getSectionType() == ActivityFeed::TRAVELER ): ?>
				<a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a> <strong><?=stripslashes($owner->getFullName())?></strong> uploaded <?=$photoCountLabel?> to album <a href="<?=$albumLink?>"><?=$photoAlbum->getTitle()?></a>
			<?	else: ?>
				<?=$photoCountLabel?> uploaded to album <a href="<?=$albumLink?>"><?=$photoAlbum->getTitle()?></a>  
				<?	if( !$sameGroup ): ?>
					in <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	endif; ?>
		</p>
		<div class="photo jLeft">
			<?	foreach($photos AS $photo): ?>
				<img width="<?=$photo->getAdjustedThumbnailWidth()?>" height="<?=$photo->getAdjustedThumbnailHeight()?>" border="0" src="<?=$photo->getPhotoLink('featured')?>"/>
			<?	endforeach; ?>
		</div>	
		<p class="jRight eventime"><?=$uploadDate->htmlDateFormat()?></p>
	</span>
</li>