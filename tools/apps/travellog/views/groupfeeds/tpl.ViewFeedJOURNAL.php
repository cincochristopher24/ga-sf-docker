<?php
	try{
		$owner = $journal->getOwner();
		if( !($owner instanceof Group) && !($owner instanceof Traveler) ){
			throw new exception("Owner must be Group or Traveler");
		}
	}catch(exception $e){
		throw $e;
	}
	
	$hasEntry = FALSE;
	if( $journalentry instanceof TravelLog ){
		$entryHelper = new EntryHelper();
		$journalLink = $entryHelper->getLink($journalentry);
		$hasEntry = TRUE;
	}else{
		$journalLink = "";
	}
	
	if( !$hasEntry ){
		throw new exception("journal is empty {feedID: ".$feed->getFeedID()."}");
	}
	
	$onclick = $hasEntry ? "" : "return false;";
	
	$profileLink = "/".$traveler->getName();
	$journalDate = GaDateTime::create($feed->getFeedDate());
	
	$isAdvisorJournal = FALSE;
	$creator = $traveler;
	/*if( $group->getAdministratorID() == $traveler->getTravelerID() ){
		$isAdvisorJournal = TRUE;
		$profileLink = $group->getFriendlyURL();
		$creator = new AdminGroup($traveler->getAdvisorGroup());
	}*/
	/*if( $journal->isGroupJournal() ){
		$isAdvisorJournal = TRUE;
		$profileLink = $group->getFriendlyURL();
		$creator = $journal->getOwner();
	}*/
	
	$isStaffJournal = $feed->getSectionType() == ActivityFeed::GROUP && $owner->getAdministratorID() == $feed->getTravelerID();
	if( !$isStaffJournal && $feed->getSectionType() == ActivityFeed::GROUP && !$isAdvisorJournal && $owner->isStaff($feed->getTravelerID()) ){
		$isStaffJournal = TRUE;
	}
	
	$travelID = $journal->getTravelID();
	$sameGroup = FALSE;
	$privilegedGroupID = $group->getGroupID();
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$sameGroup = $group->getGroupID() == $feed->getGroupID();
		$journalStatus = $owner->getGroupJournalStatus($travelID);
		$privilegedGroupID = $owner->getGroupID();
	}else{
		$journalStatus = $group->getGroupJournalStatus($travelID);
	}
	$approveLinkLabel = $journalStatus["approved"] ? "Unapprove" : "Approve";
	$approveLinkClass = $journalStatus["approved"] ? "Approved" : "Unapproved";
	$featureLinkLabel = $journalStatus["featured"] ? "Remove from Featured List" : "Feature";
	$featureLinkClass = $journalStatus["featured"] ? "Featured" : "Unfeatured";
?>
<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>"> 
	<?	if( $isAdvisorJournal ): ?> 
		<a href="<?=$profileLink?>" class="">
			<img src="<?=$creator->getGroupPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>   
	<?	else: ?>
		<a href="<?=$profileLink?>">
			<img class="jLeft" src="<?=$traveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>
	<?	endif; ?>
	<span class="box">
		<p class="highlights feedJournal">
			<?	if( $isAdvisorJournal ): ?>
				<a href="<?=$profileLink?>" class=""><strong><?=$creator->getName()?></strong></a> created a new JOURNAL <a href="<?=$journalLink?>" onclick="<?=$onclick?>"><?=$journal->getTitle()?></a>
				<?	if( !$sameGroup ): ?>
					for <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	elseif( $isStaffJournal ): ?>
				<a href="<?=$profileLink?>"><strong><?=$creator->getName()?></strong></a> <strong><?=stripslashes($traveler->getFullName())?></strong> created a new JOURNAL <a href="<?=$journalLink?>" onclick="<?=$onclick?>"><?=$journal->getTitle()?></a> 
				<?	if( !$sameGroup ): ?>
					for <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	else: ?>
				<a href="<?=$profileLink?>"><strong><?=$creator->getName()?></strong></a> <strong><?=stripslashes($creator->getFullName())?></strong> created a new JOURNAL <a href="<?=$journalLink?>" onclick="<?=$onclick?>" ><?=$journal->getTitle()?></a> 
			<?	endif; ?>
			
		</p>
		<div class="discuss jLeft">
			<p><?=nl2br($journal->getDescription())?></p>
			
			<?/* FIRST JOURNAL ENTRY */?>
			<?	if( is_null($journalentry) ): ?>
				Journal doesn't have an entry yet.
			<?	else: ?>
				<p>
					<a href="<?=$journalLink?>"><?=$journalentry->getTitle()?></a>
					<div>
						<?=nl2br(HtmlHelpers::TruncateText(strip_tags($journalentry->getDescription()), 50))?>
					</div>
				</p>
			<?	endif; ?>
			<?/* --end of FIRST JOURNAL ENTRY-- */?>
			
			<p>
			<?	if( $hasEntry || $journalStatus["approved"] || $journalStatus["featured"] ): ?>
				<?	if( $isStaffJournal ): ?>
					<a id="lnkFeatureAdminJournal_<?=$travelID?>" href="javascript:void(0)" onclick="groupFeeds.featureAdminJournal(this.id); return false;" class="<?=$featureLinkClass?>"><?=$featureLinkLabel?></a>
				<?	else: ?>
					<a id="lnkApproveMemberJournal_<?=$travelID?>" href="javascript:void(0)" onclick="groupFeeds.approveMemberJournal(this.id); return false;" class="<?=$approveLinkClass?>"><?=$approveLinkLabel?></a>
					<a id="lnkFeatureMemberJournal_<?=$travelID?>" href="javascript:void(0)" onclick="groupFeeds.featureMemberJournal(this.id); return false;" class="<?=$featureLinkClass?>"><?=$featureLinkLabel?></a>
					<?	if( !$journalStatus["related"] ): ?>
						<a id="lnkToUnapprovedMemberJournal_<?=$travelID?>" href="javascript:void(0)" onclick="groupFeeds.toUnapprovedMemberJournal(this.id); return false;">To Unapproved</a>
					<?	endif; ?>
				<?	endif; ?>
				<img src="/images/loading_small.gif" id="ProcessingStatus_<?=$feed->getFeedID()?>" style="display:none;" />
				<input type="hidden" id="JournalFeed_<?=$travelID?>" value="<?=$feed->getFeedID()?>" />
				<input type="hidden" id="privilegedGroupID_<?=$travelID?>" value="<?=$privilegedGroupID?>" />
			<?	endif; ?>
			</p>		
		</div>	
		<p class="jRight eventime"><?=$journalDate->htmlDateFormat()?></p>
	</span>
</li>