<?php
	try{
		$owner = $journal->getOwner();
		if( !($owner instanceof Group) && !($owner instanceof Traveler) ){
			throw new exception("Owner must be Group or Traveler");
		}
	}catch(exception $e){
		throw $e;
	}

	$entryHelper = new EntryHelper();
	$journalLink = $entryHelper->getLink($journalentry);
	$uploadDate = GaDateTime::create($feed->getFeedDate());
	
	$rowsLimit = new RowsLimit(10,0);
	$videosArray = $journalentry->getVideosUploadedByDay($rowsLimit,$feed->getFeedDate());
	$videos = $videosArray["videos"];
	$videoCount = $videosArray["recordCount"];
	if( !$videoCount ){
		throw new exception("videos uploaded not found; possible undeleted entry video feed");
	}
	
	$profileLink = "/".$traveler->getName();
	//$isAdvisorJournal = FALSE;
	/*if( $group->getAdministratorID() == $traveler->getTravelerID() ){
		$isAdvisorJournal = TRUE;
		$profileLink = $group->getFriendlyURL();
	}*/
	/*$isStaffJournal = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP && !$isAdvisorJournal && $owner->isStaff($feed->getTravelerID()) ){
		$isStaffJournal = TRUE;
	}*/
	
	$sameGroup = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$sameGroup = $group->getGroupID() == $feed->getGroupID();
		$videoCountLabel = ( 1 < $videoCount )? "$videoCount Videos were" : "$videoCount Video was";
	}else{
		$videoCountLabel = ( 1 < $videoCount )? "$videoCount Videos" : "$videoCount Video";
	}
?>
<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>">
	<?	if( $journal->isGroupJournal() )://$isAdvisorJournal || $isStaffJournal ): ?> 
		<a href="<?=$profileLink?>" class="">
			<img src="<?=$owner->getGroupPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>   
	<?	else: ?>
		<a href="<?=$profileLink?>">
			<img class="jLeft" src="<?=$owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>
	<?	endif; ?>
	<span class="box">
		<p class="highlights feedVideo">
			<?	if( $feed->getSectionType() == ActivityFeed::TRAVELER ): ?>		
				<a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a> <strong><?=stripslashes($owner->getFullName())?></strong> added <?=$videoCountLabel?> to JOURNAL ENTRY <a href="<?=$journalLink?>"><?=$journalentry->getTitle()?></a>
			<?	else: ?>
				<?=$videoCountLabel?> added to JOURNAL ENTRY <a href="<?=$journalLink?>"><?=$journalentry->getTitle()?></a>  
				<?	if( !$sameGroup && $feed->getSectionType() == ActivityFeed::GROUP ): ?>
					in <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	endif;?>	
		</p>
		
			<?	foreach($videos AS $video):
					if( $feed->getSectionType() == ActivityFeed::TRAVELER ){
						$videoLink = "/video.php?action=getTravelerVideos&travelerID=".$owner->getTravelerID()."&travelID=".$journal->getTravelID()."&travelLogID=".$journalentry->getTravelLogID()."&videoID=".$video->getVideoID()."&jeVideos=1&type=journal";
					}else{
						$videoLink = "/video.php?action=getGroupVideos&gID=".$owner->getGroupID()."&travelID=".$journal->getTravelID()."&travelLogID=".$journalentry->getTravelLogID()."&videoID=".$video->getVideoID()."&jeVideos=1&type=journal";
					}
			?><div class="video jLeft">
				<div class="videomarker">
					<img src="/images/v3/discussions/videosign.gif" border="0" align="left"/>
				</div>
				<img width="92" height="62" border="0" src="<?= $video->getVideoImageUrl() ?>"/>
				<a href="<?=$videoLink?>"><strong><?=$video->getTitle()?></strong></a><br>
				<span><?=$video->getDuration()?></span>
			</div>
			<?	endforeach; ?>
			
		<p class="jRight eventime"><?=$uploadDate->htmlDateFormat()?></p>
	</span>
</li>