<?php
	require_once("travellog/views/groupfeeds/Class.AbstractViewFeed.php");

	class ViewFeedJOINGROUPREQUEST extends AbstractViewFeed{
		public function prepare(){
			require_once("travellog/model/Class.AdminGroup.php");
			require_once("travellog/model/Class.Traveler.php");
			try{
				
				$traveler = new Traveler($this->mFeed->getTravelerID());
				$group = new AdminGroup($this->mFeed->getGroupID());
				
				if( $traveler->isSuspended() ){
					throw new Exception("Suspended travelerID: ".$this->mFeed->getTravelerID()." FeedID: ".$this->mFeed->getFeedID());
				}
				
				$this->mTemplate->set("traveler",$traveler);
				$this->mTemplate->set("group",$group);
				$this->mTemplate->set("feed",$this->mFeed);
			}catch(exception $e){
				throw $e;
			}
		}
		public function render(){
			parent::render();
		}
	}