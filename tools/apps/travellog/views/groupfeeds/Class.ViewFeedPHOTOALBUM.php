<?php
	require_once("travellog/views/groupfeeds/Class.AbstractViewFeed.php");

	class ViewFeedPHOTOALBUM extends AbstractViewFeed{
		public function prepare(){
			require_once("travellog/model/Class.Traveler.php");
			require_once("travellog/model/Class.PhotoAlbum.php");
			try{
				$traveler = new Traveler($this->mFeed->getTravelerID());
				$photoAlbum = new PhotoAlbum($this->mFeed->getSectionID());
				
				if( !($photoAlbum instanceof PhotoAlbum) ){
					throw new exception("Invalid PhotoAlbumID: ".$this->mFeed->getSectionID()." FeedID: ".$this->mFeed->getFeedID());
				}
				if( $traveler->isSuspended() ){
					throw new Exception("Suspended travelerID: ".$this->mFeed->getTravelerID()." FeedID: ".$this->mFeed->getFeedID());
				}
				
				$this->mTemplate->set("traveler",$traveler);
				$this->mTemplate->set("photoAlbum",$photoAlbum);
				$this->mTemplate->set("feed",$this->mFeed);
				$this->mTemplate->set("group",$this->mGroup);
			}catch(exception $e){
				throw $e;
			}
		}
		public function render(){
			parent::render();
		}
	}