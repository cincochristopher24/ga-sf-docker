<?php

	require_once("Class.Template.php");
	require_once("Class.HtmlHelpers.php");
	require_once("Class.GaDateTime.php");
	require_once("travellog/helper/Class.EntryHelper.php");
	require_once("travellog/model/Class.ActivityFeed.php");
	
	require_once('Class.Paging.php');
	require_once('Class.ObjectIterator.php');
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterOp.php');
	require_once('travellog/model/Class.FilterCriteria2.php');
	require_once('travellog/model/Class.RowsLimit.php');
	 
	class ViewGroupFeedsSearch{
	
		private $mParams = array(	"action"	=>	"search",
									"mode"		=>	"ALL",
									"sgID"		=>	NULL,
									"page"		=>	1	);
		private $mGroup = NULL;
		private $mLoggedUser = NULL;
		private $mFeeds = array("activityFeeds"=>array(),"recordCount"=>0);
		
		private $mRpp	=	20;
		private $mPage	=	1;
		
		public function setParams($params=array()){
			$this->mParams = $params;
			$this->mPage = (isset($this->mParams["page"]) && is_numeric($this->mParams["page"]) && 0 < $this->mParams["page"]) ? $this->mParams["page"] : 1;
		}
		public function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		public function setLoggedUser($user=NULL){
			$this->mLoggedUser = $user;
		}
		public function retrieve(){
			$rowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage,$this->mRpp));
			$params = array(	"GROUP_OBJECT"				=>	$this->mGroup,
								"ROWS_LIMIT"				=>	$rowsLimit );
			switch($this->mParams["mode"]){
				case "JOURNALS"		:	$params["JOURNALS_ONLY"] = TRUE; break;
				case "MULTIMEDIA"	:	$params["MULTIMEDIA_ONLY"] = TRUE; break;
				case "DISCUSSION"	:	$params["DISCUSSION_ONLY"] = TRUE; break;
				case "MEMBERSHIP"	:	$params["MEMBERSHIP_ONLY"] = TRUE; break;
				default				:	$params["GET_ALL_FEEDS"] = TRUE; break;
			}
			
			if( !is_null($this->mParams["sgID"]) && is_numeric($this->mParams["sgID"]) && 0 < $this->mParams["sgID"] ){
				$params["GROUP_ID"] = intval($this->mParams["sgID"]);
			}	
			
			$this->mFeeds = ActivityFeed::searchFeeds($params);
		}
		public function render(){
			$paging = new Paging( $this->mFeeds["recordCount"], $this->mPage, '', $this->mRpp );
			$paging->setOnclick("groupFeeds.loadFeedsPage");
			$iterator = new ObjectIterator( $this->mFeeds["activityFeeds"], $paging );
			
			$tpl = new Template();
			$tpl->set("feeds",$this->mFeeds["activityFeeds"]);
			$tpl->set("group",$this->mGroup);
			$tpl->set("loggedUser",$this->mLoggedUser);
			$tpl->set("paging",$paging);
			$tpl->set("iterator",$iterator);
			$tpl->out("travellog/views/groupfeeds/tpl.ViewGroupFeedsSearchResult.php");
		}
		private function calcOffset($page=1,$rpp=20){
			return ($page-1) * $rpp;
		}
	}