<?php
	$postDate = GaDateTime::descriptiveDifference($feed->getFeedDate(), date("Y-m-d H:i:s"));
	
	//$advisorGroup = $creator->getAdvisorGroup();
	
	$ownerGroup = new Admingroup($topic->getGroupID());
	$isMainGroupDiscussion = 0 == $ownerGroup->getParentID();
	$advisorGroup = FALSE;
	/*if( $advisorGroup ){
		$creator = new AdminGroup($advisorGroup);
		$profileLink = $creator->getFriendlyURL();
	}else{
		$profileLink = $creator->getFriendlyUrl();
	}*/
	$profileLink = $creator->getFriendlyUrl();
	
	$discussionID = $discussion->getID();
	
	$isArchived = ($discussion->getStatus()==Discussion::ARCHIVED_STATUS || $topic->getStatus()==Topic::ARCHIVED_STATUS);
	$archiveLinkLabel = $isArchived ? "Activate" : "Archive";
	
	$isPinned = 1 == $discussion->getIsFeatured();
	$pinLinkLabel = $isPinned ? "Unpin Discussion" : "Pin Discussion";
	$pinLinkClass = $isPinned ? "Unpin" : "Pin";
	
	$isAdmin = $group->getAdministratorID()==$loggedUser->getTravelerID();
	if( !$isAdmin ){
		if( 0 < $group->getParentID() ){
			$main = new AdminGroup($group->getParentID());
			$isAdmin = $main->isStaff($loggedUser->getTravelerID());
		}else{
			$isAdmin = $group->isStaff($loggedUser->getTravelerID());
		}
	}
	
	
	if( !$isMainGroupDiscussion && $isAdmin ){
		$isHomepageShown = 1 == $discussion->getHomepageShown();
		$addToBaseLinkLabel = $isHomepageShown ? "Remove from Knowledge Base" : "Add to Knowledge Base";
		$addToBaseLinkClass = $isHomepageShown ? "Remove" : "Add";
	}
?>
<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>">     
	<?	if( $advisorGroup ): ?> 
		<a href="<?=$profileLink?>" class="">
			<img src="<?=$creator->getGroupPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>   
	<?	else: ?>
		<a href="<?=$profileLink?>">
			<img class="jLeft" src="<?=$creator->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>
	<?	endif; ?>
	<span class="box">
		<p class="highlights feedReply">
			<a href="<?=$profileLink?>"><strong><?=$creator->getName()?></strong></a>
			<?	if( !$advisorGroup ): ?>
				<strong><i><?=stripslashes($creator->getFullName())?></i></strong> 
			<?	endif; ?> created a new discussion 
			<a href="<?=$discussion->getUrl()?>"><?=$discussion->getTitle()?></a>
		</p>
		<div class="discuss jLeft">
			<span class="jRight">
				<?/*
				 * commented by nash per nalds order - April 13, 2011
				<a id="lnkDiscussionReply_<?=$discussionID?>" name="DiscussionReply_<?=$discussionID?>" href="<?=$discussion->getUrl()?>&action=reply" <?if($isArchived):?>style="display:none;"<?endif;?> >Reply</a> 
				*/ ?>
				<a id="lnkDiscussionArchive_<?=$discussionID?>" href="javascript:void(0)" onclick="groupFeeds.archiveDiscussion(this.id); return false;" class="<?=$archiveLinkLabel?>"><?=$archiveLinkLabel?></a>
				<?	if( !$isMainGroupDiscussion && $isAdmin && 0 < $feed->getParentGroupID() ): ?>
				<a id="lnkDiscussionAddToBase_<?=$discussionID?>" href="javascript:void(0)" onclick="groupFeeds.addToBaseDiscussion(this.id); return false;" class="<?=$addToBaseLinkClass?>"><?=$addToBaseLinkLabel?></a>
				<?	endif; ?>
				<a id="lnkDiscussionPin_<?=$discussionID?>" href="javascript:void(0)" onclick="groupFeeds.pinDiscussion(this.id); return false;" class="<?=$pinLinkClass?>"><?=$pinLinkLabel?></a>
				<img src="/images/loading_small.gif" id="ProcessingStatus_<?=$feed->getFeedID()?>" style="display:none;" />
				<input type="hidden" id="DiscussionFeed_<?=$discussionID?>" value="<?=$feed->getFeedID()?>" />
			</span>	
		</div>	
		<p class="jRight eventime">Posted <?=$postDate?> ago</p>
	</span>
</li>