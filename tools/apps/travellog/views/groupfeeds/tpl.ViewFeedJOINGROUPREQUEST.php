<?php
	if( $group->isMember($traveler) ){
		throw new exception("Traveler must be a member of group");
	}
	$profileLink = "/".$traveler->getName();
	$feedDate = GaDateTime::create($feed->getFeedDate());
	$isDenied = $group->isInDeniedRequestList($traveler);
?>
<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>">     
	<a href="<?=$profileLink?>">
		<img class="jLeft" src="<?=$traveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
	</a>
	<span class="box">
		<p class="highlights feedMember"><a href="<?=$profileLink?>"><strong><?=$traveler->getName()?></strong></a> <strong><?=stripslashes($traveler->getFullName())?></strong> requests to join <a href=""><?=$group->getName()?></a></p>
		<div class="discuss jLeft">
			<p>
				<div id="requestStatus_<?=$feed->getFeedID()?>" <?if(!$isDenied):?>style="display:none;"<?endif;?> >
					<?	if( $isDenied ): ?>
						This request has been denied.
					<?	endif; ?>
				</div>
				<span id="requestActionLinks_<?=$feed->getFeedID()?>" class="jRight">
					<a id="lnkApproveRequest_<?=$feed->getFeedID()?>" href="javascript:void(0)" onclick="groupFeeds.approveRequest(this.id); return false;">Approve</a> 
					<?	if( !$isDenied ): ?>
						<a id="lnkDenyRequest_<?=$feed->getFeedID()?>" href="javascript:void(0)" onclick="groupFeeds.denyRequest(this.id); return false;">Deny</a>
					<?	endif; ?>
				</span>
				<img src="/images/loading_small.gif" id="ProcessingStatus_<?=$feed->getFeedID()?>" style="display:none;" />
				<input type="hidden" id="requestedGroupID_<?=$feed->getFeedID()?>" value="<?=$group->getGroupID()?>" />
				<input type="hidden" id="requestingTravelerID_<?=$feed->getFeedID()?>" value="<?=$traveler->getTravelerID()?>" />
			</p>	
		</div>	
		<p class="jRight eventime"><?=$feedDate->htmlDateFormat()?></p>
	</span>
</li>