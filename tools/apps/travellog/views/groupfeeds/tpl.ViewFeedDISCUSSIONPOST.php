<?php
	$feedID = $feed->getFeedID();

	$poster = $post->getPoster();
	
	$postDate = GaDateTime::descriptiveDifference($feed->getFeedDate(), date("Y-m-d H:i:s"));
	$message = strip_tags($post->getMessage());
	$tooLong = FALSE;
	if( strlen($message) > 250 ){
		$tooLong = TRUE;
		$messageHalf = substr($message,0,250);
	}
	
	$isArchived = ($discussion->getStatus()==Discussion::ARCHIVED_STATUS || $topic->getStatus()==Topic::ARCHIVED_STATUS);
?>

<li class="container ajax_journal" id="feed_<?=$feedID?>">     
	<a href="<?=$poster->getProfileUrl()?>">
		<img class="jLeft" src="<?=$poster->getProfilePicUrl()?>" border="" align="left" alt=""/>
	</a>
	<span class="box">
		<p class="highlights feedReply"><a href="/<?=$traveler->getName()?>">
			<strong><?=HtmlHelpers::truncateWord($traveler->getName(), 50, '...')?></strong></a>
			<?//	if( !$traveler->getAdvisorGroup() ): ?>
				<strong><i><?=stripslashes($traveler->getFullName())?></i></strong> 
			<?//	endif; ?> replied to 
			<a href="<?=$discussion->getUrl()?>"><?=$discussion->getTitle()?></a>
		</p>
		<div class="discuss jLeft">
			<p>
				<?	if( $tooLong ): ?>
					<span id="messageHalf_<?=$feedID?>"><?=nl2br($messageHalf)?>...</span>
				<?	endif; ?>
				<span id="messageFull_<?=$feedID?>" <?if($tooLong):?>style="display:none;"<?endif;?> >
					<?=nl2br($message)?>
				</span>			
				<br />
				<span class="jRight">
					<?/*
					 * commented by nash per nalds order - April 13, 2011
					<a id="lnkDiscussionPostReply_<?=$post->getID()?>" name="DiscussionReply_<?=$discussion->getID()?>" href="<?=$discussion->getUrl()?>&action=reply" <?if($isArchived):?>style="display:none;"<?endif;?> >Reply</a> 
					*/ ?>
					<?	if( $tooLong ): ?>
						<a id="expandMessage_<?=$feedID?>" href="" onclick="jQuery(this).hide(); jQuery('#messageHalf_<?=$feedID?>').hide('fast'); jQuery('#messageFull_<?=$feedID?>').slideDown('fast'); jQuery('#collapseMessage_<?=$feedID?>').show(); return false;"><img class="expand" src="/images/down-enabled.gif" align="right" border="0"></a>
						<a id="collapseMessage_<?=$feedID?>" href="" style="display:none;" onclick="jQuery(this).hide(); jQuery('#messageFull_<?=$feedID?>').slideUp('fast'); jQuery('#messageHalf_<?=$feedID?>').show('fast'); jQuery('#expandMessage_<?=$feedID?>').show(); return false;"><img class="expand" src="/images/up-enabled.gif" align="right" border="0"></a>
					<?	endif; ?>
				</span>
			</p>	
		</div>	
		<p class="jRight eventime">Posted <?=$postDate?> ago</p>
	</span>
</li>