<?php
	try{
		$owner = $journal->getOwner();
		if( !($owner instanceof Group) && !($owner instanceof Traveler) ){
			throw new exception("Owner must be Group or Traveler");
		}
	}catch(exception $e){
		throw $e;
	}

	$entryHelper = new EntryHelper();
	$journalLink = $entryHelper->getLink($journalentry);
	$uploadDate = GaDateTime::create($feed->getFeedDate());
	
	$rowsLimit = new RowsLimit(10,0);
	$photosArray = $journalentry->getPhotosUploadedByDay($rowsLimit,$feed->getFeedDate());
	$photos = $photosArray["photos"];
	$photoCount = $photosArray["recordCount"];
	if( !$photoCount ){
		throw new exception("photos uploaded not found; possible undeleted entry photo feed {feedID: ".$feed->getFeedID()."}");
	}
	
	$profileLink = "/".$traveler->getName();
	//$isAdvisorJournal = FALSE;
	/*if( $group->getAdministratorID() == $traveler->getTravelerID() ){
		$isAdvisorJournal = TRUE;
		$profileLink = $group->getFriendlyURL();
	}*/
	/*$isStaffJournal = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP && !$isAdvisorJournal && $owner->isStaff($feed->getTravelerID()) ){
		$isStaffJournal = TRUE;
	}*/
	
	$sameGroup = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$sameGroup = $group->getGroupID() == $feed->getGroupID();
		$photoCountLabel = ( 1 < $photoCount )? "$photoCount Photos were" : "$photoCount Photo was";
	}else{
		$photoCountLabel = ( 1 < $photoCount )? "$photoCount Photos" : "$photoCount Photo";
	}
?>
<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>"> 
	<?	if( $journal->isGroupJournal() )://$isAdvisorJournal || $isStaffJournal ): ?> 
		<a href="<?=$profileLink?>" class="">
			<img src="<?=$owner->getGroupPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>   
	<?	else: ?>
		<a href="<?=$profileLink?>">
			<img class="jLeft" src="<?=$owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>
	<?	endif; ?>
	<span class="box">
		<p class="highlights feedPhoto">
			<?	if( $feed->getSectionType() == ActivityFeed::TRAVELER ): ?>
				<a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a> <strong><?=stripslashes($owner->getFullName())?></strong> added <?=$photoCountLabel?> to JOURNAL ENTRY <a href="<?=$journalLink?>"><?=$journalentry->getTitle()?></a>
			<?	else: ?>
				<?=$photoCountLabel?> added to JOURNAL ENTRY <a href="<?=$journalLink?>"><?=$journalentry->getTitle()?></a>  
				<?	if( !$sameGroup ): ?>
					in <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	endif; ?>
		</p>
		<div class="photo jLeft">
			<?	foreach($photos AS $photo): ?>
				<img width="<?=$photo->getAdjustedThumbnailWidth()?>" height="<?=$photo->getAdjustedThumbnailHeight()?>" border="0" src="<?=$photo->getPhotoLink('featured')?>"/>
			<?	endforeach; ?>
		</div>	
		<p class="jRight eventime"><?=$uploadDate->htmlDateFormat()?></p>
	</span>
</li>