<?php
$file_factory  = FileFactory::getInstance();
require_once('Class.HtmlHelpers.php');
//if( $props['is_login'] && isset($props['is_owner']) ) 
	echo $objSubNavigation->show();
	
?>
<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
    	<div id="journals_summary" class="section">
    		<?php if( isset($props['action']) && (strtolower($props['action']) == 'myjournals' || strtolower($props['action']) == 'groupjournals' || strtolower($props['action']) == 'groupmembersjournal')):?>
	    		<h1 class="big_header">
					<span>
				    		<?
				    			if( $props['is_owner'] )
				    				echo $props['journal_page_owner'];
				    			else
				    				echo ( substr(strtolower($props['journal_page_owner']), -1) != 's' )? $props['journal_page_owner'] . "&rsquo;s" : $props['journal_page_owner'] . "'";
				    		?> Journals
				    </span>	
				</h1>
			<?php else:?>	
				<div class="head_left"><div class="head_right"></div></div>	
			<?php endif;?>
  			
			<div class="content">
				
  				<?php echo $obj_journal_lists_view->render();?>
  				
				<?php if( is_object($objPaging) ){ $objPaging->showPagination(array('label'=> 'Journals'));}?>
										
			</div>		
		
			<div class="foot"></div>	
            <div class="clear"></div> 
		
		</div>
	</div>
	<div id="narrow_column">
		<?php if( isset($props['action']) && (strtolower($props['action']) == 'myjournals' || strtolower($props['action']) == 'groupjournals' || strtolower($props['action']) == 'groupmembersjournal')):?>
			<div id="quick_tasks" class="section"> 
				<h2>
				<span>Quick Tasks</span>
				</h2>
				<div class="content">
              		<ul class="actions">
              			<?if( isset($props['is_owner']) && $props['is_owner'] ):?>
	              			<?if( $objPaging != NULL ):?>
	              				<?if( strtolower($props['action']) != 'groupjournals'):?>
									<?if( strtolower($props['action']) == 'myjournals'):?>
		                				<li <?if( strtolower($props['action']) != 'groupmembersjournal' ):?>id="sel"<?endif;?>><a class="button" href="/journal.php?action=MyJournals<?=$props['query_string']?>">Manage Journals</a></li>
		                			<?else:?>
		                				<li <?if( strtolower($props['action']) != 'groupmembersjournal' ):?>id="sel"<?endif;?>><a class="button" href="/journal.php?action=GroupJournals<?=$props['query_string']?>">Manage Journals</a></li>
		                			<?endif;?>
								<?endif;?> 
	                		<?endif;?> 
                			<?if( $props['is_login'] && isset($props['is_parent']) && $props['is_parent'] ):?>
                				<?if( strtolower($props['action']) != 'groupmembersjournal' ):?>
									<li><a class="button" href="/journal.php?action=GroupMembersJournal">Members Journals</a></li>
								<?endif;?>		
                			<?endif;?> 
		                	<li><a class="button" href="/travel.php?action=add&amp;contextGroupID=<?= $props['gID'] ?>">+ Add a Travel Journal</a></li> 
			            <?endif;?>


        		    </ul>
              		<div class="clear"></div>
			    </div>
				<div class="foot"></div>
		  	</div>
		<?php else:?>
			<div id="search" class="section">
				<h2>
				<span>Search</span>
				</h2>
	            <div class="content" >
					<form action="journal.php" method="get" onsubmit="return false;">
						<fieldset class="fieldset1">
	              	        <p> <span>Search by:</span>
			                <select name="countryID" id="countryID" class="countries">
			                	<?php foreach( $collectionCountry as $obj_country ):?>
			                		<option value="<?php echo $obj_country->getLocationID()?>" <?php if( $props['locationID'] == $obj_country->getLocationID() ): ?>selected="selected"<?php endif;?>><?php echo $obj_country->getName()?></option>
								<?endforeach;?>
								<option value="0" selected="selected">Choose a Country </option>
			                </select>
			                </p> 
							<ul>
			                <li><input id="btnFilter" name="btnFilter" onclick="search()" value="Show Journals" class="jour_submit" type="submit" /></li>
			                </ul>
	
		
		                </fieldset>
	                </form>
				              
		        </div>
				<div class="foot"></div>
			</div>
			
			<?php echo $obj_map_view->render()?>
	
		<?php endif;?>	
	</div>
	<div class="clear"></div>
 </div> 