<?php

	require_once("Class.Template.php");

	class ViewUploadedPhotos{
		
		private $mCustomPageHeader = NULL;
		private $mIsPrivileged = FALSE;
		
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
		}
		function setIsPrivileged($privileged=FALSE){
			$this->mIsPrivileged = $privileged;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("privileged",$this->mIsPrivileged);
			$params = array(	"customizing"	=>	($this->mCustomPageHeader->getPhotoOptionTemp() == 0)? $this->mCustomPageHeader->isCustomizing() : FALSE,
								"deleted"		=>	FALSE,
								"initial"		=>	FALSE,
								"countOnly"		=>	FALSE,
								"photoOption"	=>	0	);
			$uploadedPhotos =  $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);
			$tpl->set("photos",$uploadedPhotos);
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewUploadedPhotos.php");
		}
	}