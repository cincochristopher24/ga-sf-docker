<h2>Customize your Homepage</h2>
<ul class="breadcrumbs" id="customize_homebrcumb">
	<li class="root">
		<?	if( 1 == $step ): ?>
			<strong>Step 1: Homepage Texts</strong>
		<?	else: ?>
			<a href="/custom_pageheader.php?action=VIEW&step=1" title="">
				<span>Step 1: Homepage Texts</span>
			</a>
		<?	endif; ?>
	</li>
	<li class="">
		<?	if( 2 == $step ): ?>
			<strong>Step 2: Process Photos</strong>
		<?	else: ?>
			<a href="/custom_pageheader.php?action=VIEW&step=2" title="">
				<span>Step 2: Process Photos</span>
			</a>	
		<?	endif; ?>
	</li>
	<li class="">
		<?	if( 3 == $step ): ?>
			<strong>Step 3: Preview and Save</strong>
		<?	else: ?>
			<a href="/custom_pageheader.php?action=VIEW&step=3" title="">
				<span>Step 3: Preview and Save</span>
			</a>
		<?	endif; ?>
	</li>						
</ul>