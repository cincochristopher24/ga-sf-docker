<h2><span>Step 3:</span><span class="subtle">&nbsp;Preview and Save</span></h2>
<div class="content">

	<div class="form" id="custom_home_form">
	
		<div class="block_header">
			<div class="text">
				<strong>Pool Preview</strong>
				<p class="sub_text" id="pool_preview_sub_text">Click on the thumbnail below to preview the larger version of the image.</p>						
			</div>
		</div>
		
		<div class="block_subhead">
			<p id="subhead_text">
				<?	if( 1 == count($pool) ): ?>
					There is <strong>1 Photo</strong> in your pool.
				<?	else: ?>
					There are <strong><?=count($pool)?> Photos</strong> in your pool
				<?	endif; ?>
			</p>
			<span id="gallery_view_selector" style="display:none;">
			</span>
			<ul class="block_filter">
				<li class="first">Switch to</li>
				<li>
					<a id="lnkToThumbnail" class="selected" href="javascript:void(0)" title="" onclick="customPageHeader.toThumbnailView(); return false;"><span>Thumbnail View</span></a>
				</li>
				<li class="last">
					<a id="lnkToGallery" href="javascript:void(0)" title="" onclick="customPageHeader.toGalleryView(0); return false;"><span>Gallery View</span></a>
				</li>
			</ul>
		</div>
		
		<div id="thumbnail_view">	
			<div class="block_body" id="photo_main_view">
				<ul class="photo_list">
					<?
						$photoIDs = "";
						$photoLinks = "";
						for($i=0; $i<count($pool); $i++){
							$photo = $pool[$i];
							$class = "";
							$ipr = 13;
							if( 0 == ($i % $ipr) ){
								$class = "rowfirst";
							}elseif( ($ipr-1) == ($i % $ipr) ){
								$class = "rowlast";
							}
							$photoIDs .= $photo->getPhotoID();
							$photoLinks .= $photo->getPhotoLink('customheaderstandard');
							if( $i < count($pool)-1 ){
								$photoIDs .= "?";
								$photoLinks .= "?";
							}
					?>
						<li class="<?=$class?>" >							
							<input type="checkbox" id="chkPhoto_<?=$photo->getPhotoID()?>" onclick="customPageHeader.togglePhotoSelection(<?=$photo->getPhotoID()?>);" value="<?=$photo->getPhotoID()?>" />
							<a class="selected" href="javascript:void(0)" title="" style="diplay: block;" onclick="customPageHeader.toGalleryView(<?=$i?>); return false;">
								<span class="thumb">	
									<img src="<?=$photo->getPhotoLink('thumbnail')?>" alt="" title="" />
								</span>								
							</a>
							
							<ul style="display:none;">
								<?php
									$travelername = $photo->getTravelersUsername();
									$shortname = $travelername;
									if( 10 < strlen($travelername) ){
										$shortname = substr($travelername,0,10)."_";
									}
									$caption = htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES);
									$shortcaption = $caption;
									if( 80 < strlen($caption) ){
										$shortcaption = substr($caption,0,80)."...";
									}
								?>
								<li id="travname_<?=$photo->getPhotoID()?>" style="display:none;">
									<?=$travelername?>
								</li>
								<li id="shortname_<?=$photo->getPhotoID()?>" style="display:none;">
									<?=$shortname?>
								</li>
								<li id="photo_caption_<?=$photo->getPhotoID()?>" style="display:none;">
									<?=$caption?>
								</li>
								<li id="shortcaption_<?=$photo->getPhotoID()?>" style="display:none;">
									<?=$shortcaption?>
								</li>
							</ul>
							
						</li>
					<?	} ?>							 							 							 								 							 							 							 						 
				</ul>
			</div>
		</div>
		
		<div id="gallery_view" style="display:none;" class="viewer content">
			<div id="top_wrap" class="cobrand_feat">
				<div id="intro">
					<div class="navhover" id="navhover">
							<a class="prev_photo" id="prev_photo" onclick="customPageHeader.movePrev(); return false;" href="javascript:void(0)"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>
							<a class="next_photo" id="next_photo" onclick="customPageHeader.moveNext(); return false;" href="javascript:void(0)"><span class="txtblock">Next<span class="imgblock"></span></span></a>
					</div>
					<img id="active_photo" src="" alt="Featured Photo" title="Featured Photo" height="324" width="980" />
					<div id="extra_image_overlay" class="featured_photo">
						<h1 class="ganet_slogan">Share your journeys with the world!</h1>	
							<p id="photo_caption" class="photo_caption" title="">
								<img src="/images/v3/cobrand/quot.png" alt="" title="" />
								<span id="photo_caption_text"></span>
								<img src="/images/v3/cobrand/quot-close.png" alt="" title="" />
							</p>			
							<p class="photo_by">
								Photo by: <a id="lnkTravelerName" href="" title="" onclick="return false;"></a>
							</p>							
					</div>			
				</div>
				<div class="cobrand_welcome">
					<div class="wide">
						<h1><?=htmlspecialchars(stripslashes(strip_tags($customPageHeader->getWelcomeTitle())),ENT_QUOTES)?></h1>
						<p> 
							<?=htmlspecialchars(stripslashes(strip_tags($customPageHeader->getWelcomeText())),ENT_QUOTES)?> 
						</p>
					</div>
					<?	if( 1 == $customPageHeader->getDirectSignup() ): ?>
						<div class="narrow" id="signup_ON">
							<a href="" title="Sign-Up Fast and Easy!" onclick="return false;">Sign-Up</a>
							<strong> Absolutely Free!</strong>
							<p>Sign Up fast and easy.</p>				
						</div>	
					<?	else: ?>
						<div class="narrow" id="feature_ganet">
							<a href="" title="GoAbroad Network - The network for travellers!" id="cobrand_signup" onclick="return false;">GoAbroad Network</a>
							<p>This site is proudly powered by the GoAbroad Network.</p>				
						</div>				
					<?	endif; ?>
				</div>		
			</div>
			
			<div id="gallery_view_select">
				<input type="checkbox" id="chkGalleryViewSelector" name="chkGalleryViewSelector" onclick="customPageHeader.galleryViewSelectPhoto();" />	
				<label for="chkGalleryViewSelector">Check to select this photo</label>
			</div>
			
		</div>
		
		<div class="block_foot no-pagination">
		</div>			

		<div class="form_item actions">
			<form name="frmFinalSelection" id="frmFinalSelection" method="post" action="/custom_pageheader.php" onclick="customPageHeader.finalizePhotoSelection(); this.submit();" >
				<input type="hidden" id="selection" name="selection" value="" />
				<input type="hidden" id="step" name="step" value="3" />
				<input type="hidden" id="action" name="action" value="SAVE_FETCHED" />
				<input type="submit" id="btnSubmitSelection" value="Save Homepage Customization" />
				or &nbsp; 
				<a href="javascript:void(0)" title="" onclick="jQuery('#action').val('VIEW'); jQuery('#step').val(2); jQuery('#selection').val(''); jQuery('#frmFinalSelection').attr('action','/custom_pageheader.php?action=VIEW&step=2'); window.location.replace('/custom_pageheader.php?action=VIEW&step=2');" >&larr; Go back to Step 2: Select a different journal</a>							
			</form>
		</div>								
						
	</div>					
	
</div>
<script type="text/javascript">
	customPageHeader.setSelectionData('<?=$photoIDs?>','<?=$photoLinks?>');
</script>