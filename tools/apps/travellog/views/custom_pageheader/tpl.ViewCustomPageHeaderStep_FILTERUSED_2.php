<h2><span>Step 2:</span><span class="subtle">&nbsp;Process Photos</span></h2>
<div class="content">

	<div class="form" id="custom_home_form">
	
		<div class="block_header">
			<div class="text">
				<strong>Current Homepage Photos</strong>
				<p class="sub_text" id="pool_preview_sub_text">Below are the photos currently displayed in your homepage. Select the photos you just want to show this time.</p>						
			</div>
		</div>
		
		<div class="block_subhead">
			<p id="subhead_text">
				Click on the thumbnail below to preview the larger version of the image.
			</p>
			<span id="gallery_view_selector" style="display:none;">
			</span>
			<ul class="block_filter">
				<li class="first">Switch to</li>
				<li>
					<a id="lnkToThumbnail" class="selected" href="javascript:void(0)" title="" onclick="customPageHeader.toThumbnailView(); return false;"><span>Thumbnail View</span></a>
				</li>
				<li class="last">
					<a id="lnkToGallery" href="javascript:void(0)" title="" onclick="customPageHeader.toGalleryView(0); return false;"><span>Gallery View</span></a>
				</li>
			</ul>
		</div>
		
		<div id="thumbnail_view">	
			<div class="block_body" id="photo_main_view">
				<ul class="photo_list">
					<?
						$photoIDs = "";
						$photoLinks = "";
						$activePhotoCount = 0;
						$selectedPhotoFlags = "";
						$fetchedPhotoFlags = "";
						$edit_caption_fields = "";
						$edit_caption_hidden = "";
						for($i=0; $i<count($customPhotos); $i++){
							$photo = $customPhotos[$i]->getPhotoInstance();
							$class = "";
							$ipr = 13;
							if( 0 == ($i % $ipr) ){
								$class = "rowfirst";
							}elseif( ($ipr-1) == ($i % $ipr) ){
								$class = "rowlast";
							}
							$photoIDs .= $photo->getPhotoID();
							$photoLinks .= $photo->getPhotoLink('customheaderstandard');
							$selectedPhotoFlags .= $customPhotos[$i]->isActive() ? "1" : "0";
							$fetchedPhotoFlags .= $customPhotos[$i]->getTravelerID();
							if( $i < count($customPhotos)-1 ){
								$photoIDs .= "?";
								$photoLinks .= "?";
								$selectedPhotoFlags .= "?";
								$fetchedPhotoFlags .= "?";
							}
							if( $customPhotos[$i]->isActive() ){
								$activePhotoCount++;
							}
							
							if( 0 == $customPhotos[$i]->getTravelerID() ){
								$photoID = $photo->getPhotoID();
								$caption = htmlspecialchars($photo->getCaption(),ENT_QUOTES);
								$edit_caption_fields .= '
									<div id="edit_caption_'.$photoID.'" name="edit_caption" style="display:none;">
										<label for="caption_'.$photoID.'">Edit photo caption:</label>
										<input style="width: 400px;" type="text" id="caption_'.$photoID.'" value="'.$caption.'" />											
									</div>';
									
								$edit_caption_hidden .= '<input type="hidden" id="hdnCaption_'.$photoID.'" name="hdnCaption_'.$photoID.'" value="" />';
							}
					?>
						<li class="<?=$class?>" >							
							<input type="checkbox" id="chkPhoto_<?=$photo->getPhotoID()?>" onclick="customPageHeader.togglePhotoSelection(<?=$photo->getPhotoID()?>);" value="<?=$photo->getPhotoID()?>" <?if($customPhotos[$i]->isActive()):?>checked<?endif;?>/>
							<a class="selected" href="javascript:void(0)" title="" style="diplay: block;" onclick="customPageHeader.toGalleryView(<?=$i?>); return false;">
								<span class="thumb">	
									<img src="<?=$photo->getPhotoLink('thumbnail')?>" alt="" title="" />
								</span>								
							</a>
							
							<ul style="display:none;">
								<?php
									if( 0 < $customPhotos[$i]->getTravelerID() ){
										$travelername = "";
										try{
											$traveler = new Traveler($customPhotos[$i]->getTravelerID());
											if( $traveler instanceof Traveler ){
												$travelername = $traveler->getUsername();
											}
										}catch(exception $e){
										}
										$shortname = $travelername;
										if( 10 < strlen($travelername) ){
											$shortname = substr($travelername,0,10)."_";
										}
									}
									$caption = htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES);
									$shortcaption = $caption;
									if( 80 < strlen($caption) ){
										$shortcaption = substr($caption,0,80)."_";
									}
								?>
								<li id="travname_<?=$photo->getPhotoID()?>" style="display:none;">
									<?	if( 0 < $customPhotos[$i]->getTravelerID() ): ?>
										<?=$travelername?>
									<?	endif;?>
								</li>
								<li id="shortname_<?=$photo->getPhotoID()?>" style="display:none;">
									<?	if( 0 < $customPhotos[$i]->getTravelerID() ): ?>
										<?=$shortname?>
									<?	endif;?>
								</li>
								<li id="photo_caption_<?=$photo->getPhotoID()?>" style="display:none;">
									<?=$caption?>
								</li>
								<li id="shortcaption_<?=$photo->getPhotoID()?>" style="display:none;">
									<?=$shortcaption?>
								</li>
							</ul>
							
						</li>
					<?	} ?>							 							 							 								 							 							 							 						 
				</ul>
			</div>
		</div>
		
		<div id="gallery_view" style="display:none;" class="viewer content">
			<div id="top_wrap" class="cobrand_feat">
				<div id="intro">
					<div class="navhover" id="navhover">
							<a class="prev_photo" id="prev_photo" onclick="customPageHeader.movePrev(); return false;" href="javascript:void(0)"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>
							<a class="next_photo" id="next_photo" onclick="customPageHeader.moveNext(); return false;" href="javascript:void(0)"><span class="txtblock">Next<span class="imgblock"></span></span></a>
					</div>
					<img id="active_photo" src="" alt="Featured Photo" title="Featured Photo" height="324" width="980" />
					<div id="extra_image_overlay" class="featured_photo">
						<h1 class="ganet_slogan">Share your journeys with the world!</h1>	
							<p id="photo_caption" class="photo_caption" title="">
								<img src="/images/v3/cobrand/quot.png" alt="" title="" />
								<span id="photo_caption_text"></span>
								<img src="/images/v3/cobrand/quot-close.png" alt="" title="" />
							</p>			
							<p class="photo_by" style="display: none;">
								Photo by: <a id="lnkTravelerName" href="" title="" onclick="return false;"></a>
							</p>							
					</div>			
				</div>
				<div class="cobrand_welcome">
					<div class="wide">
						<h1><?=htmlspecialchars(stripslashes(strip_tags($customPageHeader->getWelcomeTitle())),ENT_QUOTES)?></h1>
						<p> 
							<?=htmlspecialchars(stripslashes(strip_tags($customPageHeader->getWelcomeText())),ENT_QUOTES)?> 
						</p>
					</div>
					<?	if( 1 == $customPageHeader->getDirectSignup() ): ?>
						<div class="narrow" id="signup_ON">
							<a href="" title="Sign-Up Fast and Easy!" onclick="return false;">Sign-Up</a>
							<strong> Absolutely Free!</strong>
							<p>Sign Up fast and easy.</p>				
						</div>	
					<?	else: ?>
						<div class="narrow" id="feature_ganet">
							<a href="" title="GoAbroad Network - The network for travellers!" id="cobrand_signup" onclick="return false;">GoAbroad Network</a>
							<p>This site is proudly powered by the GoAbroad Network.</p>				
						</div>				
					<?	endif; ?>
				</div>						
			</div>
			
			<div class="" style="padding: 20px 20px 0;">
				<?=$edit_caption_fields?>
				<input type="checkbox" id="chkGalleryViewSelector" name="chkGalleryViewSelector" onclick="customPageHeader.galleryViewSelectPhoto();" />	
				<label for="chkGalleryViewSelector" style="display: inline; font-weight: normal;">Check to select this photo</label>				
			</div>				
									
		</div>
		
		<div class="block_foot no-pagination">
		</div>			

		<div class="form_item actions">
			<form name="frmFinalSelection" id="frmFinalSelection" method="post" action="/custom_pageheader.php" onclick="customPageHeader.finalizePhotoSelection(); this.submit();" >
				<input type="hidden" id="selection" name="selection" value="" />
				<input type="hidden" id="step" name="step" value="2" />
				<input type="hidden" id="action" name="action" value="PROCEED_FILTERUSED_STEP3" />
				<input type="hidden" name="filterused" value="" />
				<?=$edit_caption_hidden?>
				<input type="submit" id="btnSubmitSelection" value="Proceed to Step 3: Preview and Save" />
				or &nbsp; 
				<a href="javascript:void(0)" title="" onclick="jQuery('#action').val(''); jQuery('#step').val(''); jQuery('#selection').val(''); jQuery('#frmFinalSelection').attr('action','/custom_pageheader.php?action=VIEW&step=1'); window.location.replace('/custom_pageheader.php?action=VIEW&step=1');" >&larr; Go Back to the Previous Step</a>							
			</form>
		</div>								
						
	</div>					
	
</div>
<script type="text/javascript">
	customPageHeader.selectedCount = <?=$activePhotoCount?>;
	customPageHeader.setSelectionData('<?=$photoIDs?>','<?=$photoLinks?>');
	customPageHeader.setSelectedPhotos('<?=$selectedPhotoFlags?>');
	customPageHeader.setFetchedPhotos('<?=$fetchedPhotoFlags?>');
	customPageHeader.editCurrentPhotos = true;
</script>