<?php
	
	require_once("travellog/model/Class.CustomPageHeaderPhoto.php");
	require_once("Class.Template.php");

	class ViewUploadedCustomPageHeader{
		
		private $mCustomPageHeaderPhoto = NULL;
		
		function __construct($custom=NULL){
			if( $custom instanceof CustomPageHeaderPhoto ){
				$this->mCustomPageHeaderPhoto = $custom;
			}else{
				throw new exception("Argument of ViewUploadedCustomPageHeader must be an instance of CustomPageHeaderPhoto.");
			}
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("customPageHeaderPhoto",$this->mCustomPageHeaderPhoto);
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewUploadedCustomPageHeader.php");
		}
	}