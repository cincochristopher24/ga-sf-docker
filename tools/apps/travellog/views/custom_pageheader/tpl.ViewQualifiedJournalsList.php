<?	if( 0 < count($journalsCount) ): ?>
	<div class="block_subhead">

		<p>Showing <strong><?=$first?>-<?=$last?> of <?=$journalsCount?></strong> journals</p>
	
		<ul class="block_filter">
			<li class="first">Show</li>
			<li>
				<a id="lnkMode_ALL" <?if('ALL'==$mode):?>class="selected"<?endif;?> href="" title="" onclick="customPageHeader.setDisplayMode('ALL'); return false;"><span>All</span></a>
			</li>
			<li>
				<a id="lnkMode_RECENT_UPDATE" <?if('RECENT_UPDATE'==$mode):?>class="selected"<?endif;?> href="" title="" onclick="customPageHeader.setDisplayMode('RECENT_UPDATE'); return false;"><span>Recently Updated</span></a>
			</li>
			<li class="last">
				<a id="lnkMode_SELECTED" <?if('SELECTED'==$mode):?>class="selected"<?endif;?> href="" title="" onclick="customPageHeader.displaySelectedJournals('page=1'); return false;"><span id="selected_journals_count">Selected(0)</span></a>
			</li>														
		</ul>
	</div>

	<div class="block_body">
		<ul class="journals_selection"><!--put class="last" for every third item-->
			<?	for($i=0; $i<count($journals); $i++){ 
					$journal = $journals[$i];
			?>
				<li <?if(0==($i+1)%3):?>class="last"<?endif;?> >						
					<a id="journal_<?=$journal->getTravelID()?>" class="" href="" title="" style="diplay: block;" onclick="customPageHeader.toggleJournalSelection(<?=$journal->getTravelID()?>); return false;">
						<span class="thumb">
							<img src="<?=$journal->getPrimaryPhotoInstance()->getPhotoLink('thumbnail')?>" alt="" title="" />
						</span>
						<span class="details">
							<strong> 
								<?=htmlspecialchars(stripslashes($journal->getTitle()),ENT_QUOTES)?>
							</strong>							
							<span><?=(1==$journal->getEntryCount())? '1 Entry' : $journal->getEntryCount().' Entries'?> | <?=$journal->getQualifiedPhotosCount()?> Qualified <?if(1 < $journal->getQualifiedPhotosCount()):?>Photos<?else:?>Photo<?endif;?></span>
							<span>by <?=$journal->getOwner()->getName()?> </span>
						</span>								
					</a>
				 </li>
			<?	} ?>						 						 							 
		</ul>
	</div>		
	
	<div class="block_foot<?if(2>$paging->getTotalPages()): echo ' no-pagination'; endif;?>">
		<img id="loading_status" src="/images/loading_small.gif" style="display:none;"/>
		<? if ( $paging->getTotalPages() > 1 ):?>
		
			<? $start = $paging->getStartRow() + 1; 
			   $end = $paging->getEndRow(); 
			?>
			<? $paging->showPagination() ?>
		
		<? endif; ?>
	</div>
	<script type="text/javascript">
		customPageHeader.updateSelectedJournalsCount();
		customPageHeader.initJournalSelection();
	</script>
<?	else: ?>
<div class="block_subhead">

	<p>Not any of your approved journals has qualified photos.</p>

	<ul class="block_filter">
		<li class="first">Show</li>
		<li>
			<a class="selected" href="" title="" onclick="return false;"><span>All</span></a>
		</li>
		<li>
			<a href="" title="" onclick="return false;"><span>Recently Updated</span></a>
		</li>
		<li class="last">
			<a href="" title="" onclick="return false;"><span>Selected(0)</span></a>
		</li>														
	</ul>
</div>
<?	endif; ?>