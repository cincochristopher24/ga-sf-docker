<?php
	
	require_once("Class.Template.php");
	require_once("travellog/model/Class.Travel.php");
	
	class ViewCustomPageHeaderStep_FETCH_3{
		
		private $mCustomPageHeader = NULL;
		private $mSelection = array();
		private $mSelectionPool = array();//array of Photo instances
		
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
		}
		function setSelection(){
			// $_SESSION["FETCH_JOURNAL_SELECTION"] was set in AbstractCustomPageHeader 
			// inside the function _viewPhotosToFetch()
			// this variable must be unset after saving the customization for FETCH option
			$this->mSelection = explode(",",$_SESSION["FETCH_JOURNAL_SELECTION"]);
		}
		function prepareSelectionPool(){
			$this->mSelectionPool = array();
			foreach( $this->mSelection as $journalID ){
				$travel = new Travel($journalID);
				if( $travel instanceof Travel ){
					$qualifiedPhotos = $travel->getQualifiedCustomHeaderPhotos();
					$this->mSelectionPool = array_merge($this->mSelectionPool,$qualifiedPhotos);
				}
			}
		}
		
		function render(){
			$this->setSelection();
			$this->prepareSelectionPool();
			
			$tpl = new Template();
			$tpl->set("pool",$this->mSelectionPool);
			$tpl->set("customPageHeader",$this->mCustomPageHeader);
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewCustomPageHeaderStep_FETCH_3.php");
		}
		
	}