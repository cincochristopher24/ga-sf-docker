<?	if( $adminLogged ): ?>
	<div style="margin: 20px 0;">
		<h2 class="cobrand_h2">Manage your Homepage</h2>
	</div>
<?	endif; ?>

<?	if( $photo ): ?>
	<div id="top_wrap" class="cobrand_feat <?if($adminLogged):?>inhomemgr<?endif;?>">
		<div id="intro">
			<!-- start rotating photo -->
			<img src="<?=$photo->getPhotoLink('customheaderstandard')?>" alt="Featured Photo" title="Featured Photo" height="324" width="980" />
			<div id="extra_image_overlay" class="featured_photo">
				<h1 class="ganet_slogan">Share your journeys with the world!</h1>	
				
				<?php
					$caption = htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES);
					$short_caption = $caption;
					if( 80 < strlen($caption) ){
						$short_caption = substr($caption,0,80)."...";
					}
				?>
																								
				<p class="photo_caption" title="<?=$caption?>">
					<?	if( "" != trim($photo->getCaption()) ): ?>
						<img src="/images/v3/cobrand/quot.png" alt="" title="" />	
						<?=$short_caption?> 
						<img src="/images/v3/cobrand/quot-close.png" alt="" title="" />	
					<?	endif; ?>	
				</p>
					
				<?	if( $traveler ): ?>
					<p class="photo_by">
						<?php
							$travelername = $traveler->getName();
							if( 10 < strlen($travelername) ){
								$travelername = substr($travelername,0,10)."_";
							}
						?>
						Photo by: <a href="/<?=$traveler->getName()?>" title="<?=$traveler->getName()?>"><?=$travelername?></a>
					</p>							
				<?	endif; ?>			
					
			</div>			
		</div>
		<div class="cobrand_welcome">
			<div class="wide">
				<h1><?=htmlspecialchars(stripslashes(strip_tags($customPageHeader->getWelcomeTitle())),ENT_QUOTES)?></h1>
				<p> 
					<?=htmlspecialchars(stripslashes(strip_tags($customPageHeader->getWelcomeText())),ENT_QUOTES)?> 
				</p>
			</div>
			<?	if( 1 == $customPageHeader->getDirectSignup() ): ?>
				<div class="narrow" id="signup_ON">
					<a href="/register.php" title="Sign-Up Fast and Easy!">Sign-Up</a>
					<strong> Absolutely Free!</strong>
					<p>Sign Up fast and easy.</p>				
				</div>	
			<?	else: ?>
				<div class="narrow" id="feature_ganet">
					<a href="http://goabroad.net" title="GoAbroad Network - The network for travellers!" id="cobrand_signup">GoAbroad Network</a>
					<p>This site is proudly powered by the GoAbroad Network.</p>				
				</div>				
			<?	endif; ?>
									
		</div>		
	
	<?	if( $adminLogged ): ?>
		<a href="/custom_pageheader.php" class="overlay"> 
			<span class="overlay_text"> 
				Edit welcome texts and photos
				<br />
				<span>
					This is a preview of the existing welcome texts and photos in your homepage.
					<br />
					<strong>Click</strong> to proceed editing.
				</span>
			</span>	
		</a>
	<?	endif; ?>
	
	</div>
<?	elseif( $adminLogged ): ?>
<!-- Rotating Photo -->
		<div id="top_wrap" class="cobrand_feat inhomemgr no-photo-yet">
		<div id="intro" style="display: none;">
			<img src="http://www.goabroad.net/images/rotating/sample_teaser.jpg" alt="Featured Photo" height="324" width="980" />
			<div id="extra_image_overlay" class="featured_photo">
				<h1 class="ganet_slogan">Share your journeys with the world!</h1>																	
				<p class="photo_caption" title="">
				</p>
			</div>			
		</div>
		<div class="cobrand_welcome" style="display: none;">
			<div class="wide">
				<h1>Welcome to our new online community!</h1>
				<p> 
					Now you can keep memoirs of your travel while keeping your friends and family posted, meet and connect with interesting and like-minded travelers, plus get reliable travel advice from savvy travelers and professionals. 
				</p>
			</div>
			<div class="narrow" id="signup_ON">
				<a href="" title="Sign-Up Fast and Easy!" onclick="return false;">Sign-Up</a>
				<strong> Absolutely Free!</strong>
				<p>Sign Up fast and easy.</p>				
			</div>									
		</div>		
		
		<a href="/custom_pageheader.php" class="overlay"> 
			<span class="overlay_text"> 
				Customize welcome texts and photos
				<br />
				<span>
					Now you can customize your welcome texts and photos in your home page. 
					<br />
					<strong>Click</strong> to proceed.
				</span>
			</span>	
		</a>
	</div>
<!-- End rotating photo -->
<?	endif; ?>