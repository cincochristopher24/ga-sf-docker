<h2><span>Step 1:</span><span class="subtle">&nbsp;Homepage Texts</span></h2>
<div class="content">
	<form name="frmHomepageTexts" id="frmHomepageTexts" method="post" action="/custom_pageheader.php" >
		<ul class="form" id="custom_home_form">
			<li class="form_item">
				<label for="home_welcome_title">Homepage Welcome Title:&nbsp;<span class="required">*</span></label>
				<p class="supplement">
					A line of text that would capture your visitors attention when they visit your homepage.
				</p>
				<input class="text wide" id="home_welcome_title" name="home_welcome_title" type="text" value="<?=$welcomeTitle?>"/>
			</li>
		
			<li class="form_item">
				<label for="home_welcome_text">Homepage Welcome Text:&nbsp;<span class="required">*</span></label>
				<p class="supplement">
					Tell more the visitors about the title you've just entered above.
				</p>
				<textarea class="textarea" id="home_welcome_text" name="home_welcome_text" onkeyup="customPageHeader.limitText(this,230,'characters_left'); return false;" onchange="customPageHeader.limitText(this,230,'characters_left'); return false;"><?=$welcomeText?></textarea>
				<div class="char_counter">
					<span id="characters_left"><?=230-strlen($welcomeText)?></span>&nbsp;Characters left							
				</div>
			</li>
		
			<li class="form_item">
				<label for="photo_options">Homepage photo options:</label>
				<p class="supplement">
					How would you like to upload your photos?
				</p>
				<ul class="indented_form">
					<li>
						<input style="margin:0;" class="" type="radio" name="photo_options" value="UPLOAD" <?if(0==$photoOption):?>checked="checked"<?endif;?> onclick="customPageHeader.photoOptionClicked('HANDPICKED');" />
						<span class="indented_frm_label">Handpicked photos</span>
						<p class="supplement">You will upload individual photos from your computers.</p>
						<?/*	if( $hasHandpicked ): ?>
							<p class="supplement"> 
								<span style="padding: 7px; background-color: #f0f0f0;">							
									<strong> <img style="vertical-align: text-bottom;" src="/images/v3/profile/images_stack.png" alt="" title="" />
										<a href="javascript:void(0)" onclick="customPageHeader.viewHandpicked(); return false;" title="">&nbsp;View previously uploaded photos</a>
									</strong> 
								</span>					
							</p>
						<?	endif; */?>
					</li>
					<?	if($hasHandpicked || $hasFetched): ?>
						<li>
							<input style="margin:0;" class="" type="radio" name="photo_options" value="SELECT_UPLOADED" onclick="customPageHeader.photoOptionClicked('SELECT_HANDPICKED');" />
							<span class="indented_frm_label">Select from current photos</span>
							<p class="supplement">Choose this if you just want to filter out some of the photos you are currently using.</p>
						</li>
					<?	endif; ?>
					<li>
						<?	if( $hasQualifiedPhotos ): ?>
							<input style="margin:0;" class="" type="radio" name="photo_options" value="FETCH" <?if(1==$photoOption):?>checked="checked"<?endif;?> onclick="customPageHeader.photoOptionClicked('FETCHED');" />
							<span class="indented_frm_label">Fetch photos from your journals.</span>
							<p class="supplement">Your homepage photos will be randomly selected from your journal(s). This option requires your journals to have high-resolution images.</p>
						<?	else: ?>
							<input style="margin:0;" class="" type="radio" disabled />
							<span class="indented_frm_label">Fetch photos from your journals.</span>
							<p class="supplement">								
								<span class="emphasize">You must have qualified photos(high-resolution images) in your approved journals before you can select this option.</span>
							</p>
						<?	endif;?>
					</li>														
				</ul>
			</li>					
		
			<li class="form_item">
				<label for="signUp_options">Sign-up options:</label>
				<p class="supplement">
					Decide if you would like to enable the sign-up options in your homepage.
				</p>
				<ul class="indented_form">
					<li>
						<input style="margin:0;" class="" type="checkbox" name="signUp_options" value="1" <?if($directSignup):?>checked="checked"<?endif;?> />
						<span class="indented_frm_label"> Allow Direct Sign-up</span>
						<p class="supplement">By checking this option your visitors will be able to sign-up directly from your homepage. </p>
					</li>							
				</ul>
			</li>					
		
			<li class="form_item actions">
				<input type="hidden" name="step" value="<?=$step?>" />
				<input type="hidden" id="action" name="action"	value="SAVETEXT" />
				<input type="button" id="btnSave" value="Proceed To Step 2: Process Photos" onclick="customPageHeader.checkRequiredFields('SAVETEXT'); return false;" />
				<span id="btnDirectSave" style="<?if( !$hasHandpicked && !$hasFetched ):?>display: none;<?endif;?>">
					or &nbsp; <strong><a href="/custom_pageheader.php?action=VIEW&step=3" onclick="customPageHeader.checkRequiredFields('PREVIEWSAVE'); return false;" title="">Preview Changes and Save right away &rarr;</a></strong>
				</span>

				<p id="btnCancel" style="border-top: 1px solid #dfdfdf; padding:10px 0; margin-top: 5px;">
					If you dont want to save any of your changes, click <strong> <a href="javascript:void(0)" title="" onclick="customPageHeader.cancelCustomization(); return false;">Cancel</a> </strong>
				</p>
			</li>					
		
		</ul>					
	</form>
</div>
<!-- end for content -->
<script type="text/javascript">
	jQuery(document).ready(function(){
		<?	if( $hasHandpicked ): ?>
			customPageHeader.hasHandpicked = true;
		<?	endif; ?>
		<?	if( $hasFetched ): ?>
			customPageHeader.hasFetched = true;
		<?	endif; ?>
		<?	if( 1 == $photoOption || !$hasQualifiedPhotos && 1 == $photoOption || !$hasHandpicked && 0 == $photoOption ): ?>
			//jQuery("#btnDirectSave").attr("disabled","true");
			jQuery("#btnDirectSave").hide();
		<?	endif; ?>
	});
</script>