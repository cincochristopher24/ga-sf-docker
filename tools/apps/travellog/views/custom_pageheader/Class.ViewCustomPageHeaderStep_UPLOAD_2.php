<?php
	
	require_once("Class.Template.php");
	
	class ViewCustomPageHeaderStep_UPLOAD_2{
		
		private $mCustomPageHeader = NULL;
		
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
		}
		
		function render(){
			$tpl = new Template();
			$params = array("customizing"	=>	TRUE,
							"deleted"		=>	FALSE	);
			$tpl->set("customPhotos",$this->mCustomPageHeader->getCustomPageHeaderPhotos($params));
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewCustomPageHeaderStep_UPLOAD_2.php");
		}
		
	}