<?php
	require_once("Class.Template.php");
	
	class ViewCustomPageHeaderSection{
		
		private $mParams	=	array( 	"action"	=>	"VIEW",
										"step"		=>	1);
		private $mCustomPageHeader	=	NULL;
		
		private $mTemplate	=	NULL;
		
		function setParams($params=array()){
			$this->mParams = array_merge($this->mParams,$params);
		}
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
		}
		function createSection(){
			if( $this->mCustomPageHeader instanceOf CustomPageHeader ){
				if( 1 == $this->mParams["step"] ){
					require_once("travellog/views/custom_pageheader/Class.ViewCustomPageHeaderStep_1.php");
					$this->mTemplate = new ViewCustomPageHeaderStep_1();
					$this->mTemplate->setCustomPageHeader($this->mCustomPageHeader);
				}else{
					if( $this->mCustomPageHeader->isCustomizing() ){
						$photoOption = $this->mCustomPageHeader->getPhotoOptionTemp();
					}else{
						$photoOption = $this->mCustomPageHeader->getPhotoOption();
					}

					$option = (0 ==$photoOption)? "UPLOAD" : "FETCH";
					
					if( isset($this->mParams["filterused"]) ){
						$option = "FILTERUSED";
					} 
					
					require_once("travellog/views/custom_pageheader/Class.ViewCustomPageHeaderStep_".$option."_".$this->mParams["step"].".php");
					$class_file = "ViewCustomPageHeaderStep_".$option."_".$this->mParams["step"];
					$this->mTemplate = new $class_file;
					$this->mTemplate->setCustomPageHeader($this->mCustomPageHeader);
				}
			}else{
				if( 1 != $this->mParams["step"] ){
					header("Location: /custom_pageheader.php");
					exit;
				}else{
					require_once("travellog/views/custom_pageheader/Class.ViewCustomPageHeaderStep_1.php");
					$this->mTemplate = new ViewCustomPageHeaderStep_1();
					$this->mTemplate->setCustomPageHeader($this->mCustomPageHeader);
				}
			}
		}
		
		
		function render(){
			$this->mTemplate->render();
		}
	}