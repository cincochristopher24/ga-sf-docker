<li id="uploaded_item_<?=$customPageHeaderPhoto->getPhotoID()?>">
	<div class="preview_thumb">
		<img id="uploaded_img_<?=$customPageHeaderPhoto->getPhotoID()?>" src="<?=$customPageHeaderPhoto->getPhotoInstance()->getPhotoLink('customheaderstandard')?>" alt="Preview Thumb Will Be Displayed Here"/>
	</div>
	<div class="photodesc_label">Photo Description:</div>
	<input class="text wide" name="home_photo_description_<?=$customPageHeaderPhoto->getPhotoID()?>" type="text" value="<?=htmlspecialchars(stripslashes($customPageHeaderPhoto->getPhotoInstance()->getCaption()),ENT_QUOTES)?>" />
	<div id="photo_control_<?=$customPageHeaderPhoto->getPhotoID()?>" class="photo_crop">
		<a href="javascript:void(0)" title="" onclick="customPageHeader.loadCropper(<?=$customPageHeaderPhoto->getPhotoID()?>); return false;">Crop</a> or <a href="javascript:void(0)" title="" onclick="customPageHeader.deletePhoto(<?=$customPageHeaderPhoto->getPhotoID()?>); return false;">Delete</a>
		<img id="requesting_delete_<?=$customPageHeaderPhoto->getPhotoID()?>" src="/images/loading_small.gif" style="display:none;"/>
	</div>
</li>