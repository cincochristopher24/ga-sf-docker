<?	require_once('travellog/model/updates/Class.UpdateAction.php'); 
	
 	switch($action){
		case UpdateAction::TRAVELER_ADDED_JOURNAL_ENTRY :
			echo 'created a new journal entry "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal Entry">' . $title . '</a>"';
			break;
		case UpdateAction::TRAVELER_UPDATED_JOURNAL_ENTRY :
			echo 'updated '.$pronoun.' journal entry "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal Entry">' . $title . '</a>"';
			break;	
		case UpdateAction::TRAVELER_DELETED_JOURNAL_ENTRY :
			echo 'deleted '.$pronoun.' journal entry "<span class="feed_highlight">' . $title . '</span>"';
			break;	
		case UpdateAction::TRAVELER_ADDED_JOURNAL :
			if($linktofeed == '')
				echo 'created a new journal "<span class="feed_highlight">' . $title . '</span>"';
			else
				echo 'created a new journal "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal">' . $title . '</a>"';
			break;	
		case UpdateAction::TRAVELER_UPDATED_JOURNAL :
			if($linktofeed == '')
				echo 'updated '.$pronoun.' journal "<span class="feed_highlight">' . $title . '</span>"';
			else
				echo 'updated '.$pronoun.' journal "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal">' . $title . '</a>"';
			break;
		case UpdateAction::TRAVELER_DELETED_JOURNAL :
			if($linktofeed == '')
				echo 'deleted '.$pronoun.' journal "<span class="feed_highlight">' . $title . '</span>"';
			break;			
		case UpdateAction::GROUP_APPROVED_JOURNAL :
			if($linktofeed == '')
				echo '&lsquo;s "<span class="feed_highlight">' . $title . '</span>" journal has been approved by admin';
			else
				echo '&lsquo;s "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal">' . $title . '</a>" journal has been approved by admin';
			break;			
		case UpdateAction::GROUP_UNAPPROVED_JOURNAL :
			if($linktofeed == '')
				echo '&lsquo;s "<span class="feed_highlight">' . $title . '</span>" journal has been disapproved by admin';
			else
				echo '&lsquo;s "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal">' . $title . '</a>" journal has been disapproved by admin';
			break;			
		case UpdateAction::GROUP_FEATURED_JOURNAL :
			if($linktofeed == '')
				echo '&lsquo;s "<span class="feed_highlight">' . $title . '</span>" journal has been featured';
			else
				echo '&lsquo;s "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal">' . $title . '</a>" journal has been featured by admin';
			break;			
		case UpdateAction::GROUP_UNFEATURED_JOURNAL :
			if($linktofeed == '')
				echo '&lsquo;s "<span class="feed_highlight">' . $title . '</span>" journal has been removed from the featured list';
			else
				echo '&lsquo;s "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal">' . $title . '</a>" journal has been removed from the featured list';
			break;			
		default;
	}

?>
