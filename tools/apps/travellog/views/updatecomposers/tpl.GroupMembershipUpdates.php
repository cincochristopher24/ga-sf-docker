<?	require_once('travellog/model/updates/Class.UpdateAction.php'); 
	//echo '<a href="/' . $traveler->getUserName() . '" class="username">' . $traveler->getUserName() . '</a>';
 	switch($action){
		case UpdateAction::TRAVELER_JOINED_GROUP :
			echo 'joined ' . $groupName;
			break;
		case UpdateAction::TRAVELER_LEFT_GROUP :
			echo  'left ' . $groupName;
			break;
		case UpdateAction::TRAVELER_REQUEST_JOIN_GROUP :
			echo 'requested to join ' . $groupName;
			break;
		case UpdateAction::TRAVELER_CANCELLED_REQUEST :
			echo 'cancelled request to join ' . $groupName;
			break;
		case UpdateAction::TRAVELER_ACCEPTED_INVITE :
			echo 'accepted ' . $groupName . ' invitation';
			break;
		case UpdateAction::TRAVELER_DENIED_INVITE :
			echo 'denied ' . $groupName . ' invitation';
			break;
		case UpdateAction::GROUP_INVITED_TRAVELER :
			echo 'was invited to join ' . $groupName;
			break;
		case UpdateAction::GROUP_CANCELLED_INVITE :
			echo 'invitation was cancelled by ' . $groupName;
			break;
		case UpdateAction::GROUP_ACCEPTED_REQUEST :
			echo 'request to join ' . $groupName . ' was accepted';
			break;
		case UpdateAction::GROUP_DENIED_REQUEST :
			echo 'request to join ' . $groupName . ' was denied';
			break;
		case UpdateAction::GROUP_REMOVED_TRAVELER :
			echo 'was removed from ' . $groupName;
			break;
		case UpdateAction::GROUP_ASSIGNED_MEMBER_STAFF :
		case UpdateAction::GROUP_INVITED_NON_GA_STAFF :
			echo 'was assigned as staff of ' . $groupName;
			break;
		case UpdateAction::GROUP_INVITED_STAFF :
			echo 'was invited as staff of ' . $groupName;
			break;
		case UpdateAction::GROUP_CANCELLED_INVITE_STAFF :
			echo 'staff invitation was cancelled by ' . $groupName;
			break;
		case UpdateAction::TRAVELER_DENIED_INVITE_STAFF :
			echo 'denied staff invitation of ' . $groupName;
			break;
		case UpdateAction::POSTED_NEW_POST :
			echo 'replied to discussion <a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Post">"' . $discussionTitle . '"</a> in ' .  $groupName .  '&lsquo;s discussion board';
			break;
		case UpdateAction::POSTED_NEW_DISCUSSION :
			echo 'posted a new discussion <a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Discussion">"' . $discussionTitle . '"</a> in ' .  $groupName .  '&lsquo;s discussion board';
			break;
		case UpdateAction::TRAVELER_MESSAGED_GROUP :
			echo 'sent a group message <a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Message">"' . $messageTitle . '"</a> to ' .  $groupName;
			break;
		case UpdateAction::TRAVELER_ADDED_JOURNAL_ENTRY :
			echo 'created a new journal entry "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Journal Entry">' . $title . '</a>"';
			break;
		case UpdateAction::CREATED_NEW_SURVEY :
			echo 'created a survey "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Survey">' . $surveyTitle . '"</a> for ' . $groupName;
			break;
		case UpdateAction::TRAVELER_ANSWERED_SURVEY :
			echo 'answered survey "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Survey">' . $surveyTitle . '"</a>';
			break;
		case UpdateAction::UPLOADED_RESOURCE_FILE :
			echo 'uploaded resource file "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Resource File">' . $caption . '"</a> for ' . $groupName;
			break;
		case UpdateAction::GROUP_ASSIGNED_SUBGROUP_MEMBER :
			echo 'was assigned as member of ' . $groupName;
			break;
		case UpdateAction::GROUP_POSTED_NEWS :
			echo 'posted a news "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View News">' . $title . '"</a> for ' . $groupName;
			break;
		case UpdateAction::POSTED_NEW_TOPIC :
			echo 'created a topic "<a href="'.$linktofeed.'" class="feed_highlight" target="_blank" title="View Topic">' . $title . '"</a> for ' . $groupName . '&lsquo;s discussion board';
			break;
		default;
	}
?>
