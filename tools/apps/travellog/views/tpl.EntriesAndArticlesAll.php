<?
require_once 'Class.StringFormattingHelper.php';
require_once("travellog/model/Class.SiteContext.php");
?>
<div id="journals_summary" class="section" >
	<a name="anchor"></a>
	<h2><span><?=$grpName?>Travel Journals And Articles</span></h2>	
	
	<div class="content" >								
			<?$ti = microtime(true);?>
			<ul class="journals">
				<?foreach($entries as $journal):?>
				<?php 
					$jID = $journal->getTravelID();
					if ($journal->getOwner() == null){
						echo 'travelid:' . $journal->getTravelID();
						continue;
					}
					
					$city = ''; $country = ''; $locationID = 0;$link = '';
					if ($journal->getType() == "JOURNAL"){
						$trip = $journal->getTrip();
						$location = $trip->getLocation();
						$ctry = (!is_null($location)) ? $location->getCountry() : null;
						if(!is_null($location) && !is_null($ctry)){
							$country = $ctry->getName();
							$city = $location->getName();
							$locationID = $ctry->getCountryID();
						}
						$link = '/journal-entry.php?action=view&amp;travellogID='.$journal->getTravelID();
						$date = $journal->getDate();//$trip->getArrival();
					}else if($journal->getType() == "ARTICLE"){
						$location = LocationFactory::instance()->create($journal->getLocID());
						$ctry = (!is_null($location)) ? $location->getCountry() : null;
						if(!is_null($location) && !is_null($ctry)){
							$country = $ctry->getName();
							$city = $location->getName();
							$locationID = $ctry->getCountryID();
						}
						$link = '/article.php?action=view&amp;articleID='.$journal->getTravelID();
						$date = $journal->getDate();
					}					
				?>
					<li class="container" id="container<?=$jID?>">     
						<h3>
							<a class="journal_title" href="<?=$link?>">
								<?=$journal->getTitle()?>
							</a>
						</h3>
						<?if ($journal->getType() == "JOURNAL"):?>
							<?if (isset($entriesAndPhotos['JOURNAL'][$jID])):?>
								<a href="<?=$link?>"> 
									<img class="jImage float_right" src="<?=$entriesAndPhotos['JOURNAL'][$jID]->getPhotoLink('featured')?>" alt="journal photo" />
								</a>
						 	<?endif;?>
						<?elseif($journal->getType() == "ARTICLE"):?>		
							<?if (isset($entriesAndPhotos['ARTICLE'][$jID])):?>
								<a href="<?=$link?>"> 
									<img class="jImage float_right" src="<?=$entriesAndPhotos['ARTICLE'][$jID]->getPhotoLink('featured')?>" alt="journal photo" />
								</a>
						 	<?endif;?>
						<?endif;?>	
						<div class="authorInfo">
							<?$owner = $journal->getTraveler();?>
							<?//if(!$owner->isAdvisor()):?>
							<?//	if( !$journal->isGroupJournalArticle() || !$owner->isAdvisor() ):?>
							<?	if( !$journal->isGroupJournalArticle() ):?>
								<a class="username" href="/<?=$owner->getUserName()?>">
									<img class="jLeft" width="37" height="37" src="<?=$owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$owner->getUserName()?>" />
								</a>
								<span>		
									<p>by: <a class="author_username" href="/<?=$owner->getUserName()?>"><strong><?=$owner->getUserName()?></strong></a>
											<?if(SiteContext::getInstance()->isInCobrand()):?>
												<?if($powerful):?>
													<strong class="meta"><em> <?=$owner->getFullName()?> </em></strong>
												<?endif;?>
											<?else:?>	
												<?if($powerful):?>
													<?if(Traveler::isMemberOfAnyOfMyGroup($_SESSION['travelerID'],$owner->getTravelerID())):?>
														<strong class="meta"><em> <?=$owner->getFullName()?> </em></strong>
													<?endif;?>
												<?endif;?>		
											<?endif;?>
									</p>	
									<p>created on: <?= (strtotime($date)) ? date('M d, Y',strtotime($date))." | " : "" ?><?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?>
										<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" style="margin: 0 0 0 0;" />	
									</p>
								</span>	
							<?else:?>	
								<? 	//$group = new AdminGroup($owner->getAdvisorGroup());
									$group = $journal->getOwner();
									$site = $group->getServerName();
									$local = "";
									if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
										$local = ".local";
									}
									
									$grouplink = ($site != false) ? 'http://'.$group->getServerName().$local : '/group.php?gID='.$group->getGroupID(); 
								?>
								<a class="username" href="<?=$grouplink?>">
									<img class="jLeft" width="37" height="37" src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$group->getName()?>" />
								</a>
								<span>		
									<p>by: <a class="author_username" href="<?=$grouplink?>">
										<strong><?=$group->getName()?></strong>
											</a>
									</p>	
									<p>created on: <?= (strtotime($journal->getDate())) ? date('M d, Y',strtotime($journal->getDate()))." | " : "" ?><?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?>
										<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" style="margin: 0 0 0 0;" />	
									</p>
								</span>	
							<?endif;?>
						</div>
						<p>
							<?= StringFormattingHelper::wordTrim(strip_tags($journal->getDescription()), 70, '&#8230;')?>
						</p>
						<?if($journal->getType() == "JOURNAL"):?>
							<? if( !$owner->isAdvisor() ): ?>
								<?if (isset($relatedGroupsAr[$jID])):
									if (count($relatedGroupsAr[$jID]) == 1): 
										 $group = $relatedGroupsAr[$jID];
										 $group = array_pop($group);
										 $site = $group->getServerName(); 
											$local = "";
											if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
												$local = ".local";
											}
										$grouplink = ($site != false) ? 'http://'.$group->getServerName().$local : '/group.php?gID='.$group->getGroupID(); 	
									?>	<div class="groupBug">
											<a class="group_advisor" href="<?=$grouplink?>">
												<img class="jLeft" width="37" height="37" src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$group->getName()?>" />
											</a>
											<span>
												<a href="<?=$grouplink?>"><strong><?=$group->getName()?></strong></a>
												<p><strong><em>Advisor Group Journal</em></strong></p>
											</span>
										</div>
									<?endif;?>	
								<?endif;?>
							<?endif;?>	
						<?else:?>
							<? if( !$owner->isAdvisor() ): ?>
								<?if (!($journal->getOwner() instanceOf Traveler)):
									$group = $journal->getOwner();
									$site = $group->getServerName(); 
										$local = "";
										if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
											$local = ".local";
										}
									$grouplink = ($site != false) ? 'http://'.$group->getServerName().$local : '/group.php?gID='.$group->getGroupID(); 	
									?>	<div class="groupBug">
											<a class="group_advisor" href="<?=$grouplink?>">
												<img class="jLeft" width="37" height="37" src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$group->getName()?>" />
											</a>
											<span>
												<a href="<?=$grouplink?>"><strong><?=$group->getName()?></strong></a>
												<p><strong><em>Advisor Group Article</em></strong></p>
											</span>
										</div>
								<?endif;?>
							<?endif;?>	
						<?endif;?>
					</li>			
				<?endforeach;?>
			</ul>
				
			<? if (isset($pagingComp)):?>	
		    	<? $pagingComp->showPagination() ?>
			<? endif; ?>
					
			<?if (count($entries) == 0){
				 echo '<p class="help_text">' . $noJournalMessage  . '</p>';						
			}?>
	</div>
	
	<div class="foot"></div>  
	<div class="yui-skin-sam">
	<div id="dlg">
	</div>
</div>

</div>
<div id="dlg_container">
</div>