<?php
	require_once("travellog/model/Class.Article.php");
	require_once("Class.JournalsCompPaging.php");
	require_once("Class.Template.php");
	
	class ArticleComp{
		private $setup = null;
		private $articles = null;
		private $template = null;
		private $pagination = null;
		private $rpp = 4;
		private $page = 1;
		
		function ArticleComp($details=null){
			if(is_array($details))
				$this->setup = $details;
		}
		
		function retrieveArticles(){
			$obj_article = new Article();
			// context (tag, author, group) data (corresponding value)
			$this->articles = $obj_article->getArticles($this->setup['CONTEXT'],$this->setup['DATA']);
			$this->setTemplate();
		}
		
		function configurePagination(){
			if($this->setup['SUBJECT'] instanceOf Traveler && $this->setup['VIEWMODE'] == "PROFILE")
				return;
				
			$qryStr = 'VIEWMODE=' . $this->setup['VIEWMODE'] .'&SECURITY_CONTEXT=' . $this->setup['SECURITY_CONTEXT'] . 
 				'&ROWS_PER_PAGE=' . $this->rpp . '&TYPE=ARTICLES' ;
			
			$this->rpp = (isset($this->setup['RPP'])) ? $this->setup['RPP'] : $this->rpp;
			
			$jcParam = 'jcp=' . base64_encode($qryStr);	
			$this->page = (isset($this->setup['PAGE'])) ? $this->setup['PAGE'] : 1;
			$this->pagination = new JournalsCompPaging(count($this->articles),$this->page,$jcParam,$this->rpp);
			
			$startrow = $this->rpp * ($this->page -1);
			$this->articles = array_slice($this->articles,$startrow,$this->rpp);
		}
		
		static function convertURLParamToSetupArray(){
			// we expect the parameter to be base64 encoded and held by jcp url param (this url param is created in "configurePaging" method)
			$setupAr = array();
			
			if (isset($_GET['jcp'])){
				$paramStr = base64_decode($_GET['jcp']);
				
				$paramTokens = explode('&',$paramStr);				// divide param string by & delimeter
				foreach($paramTokens as $token){
					$ppAr = explode('=',$token);					// divide token by = delimeter to get variable name -> value pair
					if (isset($ppAr[1]) && $ppAr[1] != ''){
						$setupAr[$ppAr[0]] = $ppAr[1];
					}
				}
			}
			
			// we also need to get the page param and include it in setupAr
			if (isset($_GET['page'])){
				$setupAr['PAGE'] =	$_GET['page']; 
			}
			
			return $setupAr;
		}
		
		function setTemplate(){
			$this->template = 'tpl.ArticlesComp.php';
		}
		
		function setupTemplate(){
			$obj_template = new Template;
			$obj_template->set('enable_controls', $this->setup['OWNER']);
			$obj_template->set('articles', $this->articles);
			
			$message = "No Articles Yet.";
			
			if($this->setup['SUBJECT'] instanceOf Traveler){
				if($this->setup['OWNER']){
					$message = '<span>Do you want to feature unique experiences, people, and places that you have seen in your travels? </span> <br />
					Now you can write these stories as news or travel articles. Write about cultures, activities or anything relevant to the travel community. <a href="/article.php?action=add"><span>Write an article now</span></a>';
				}else{
					$message = $this->setup['SUBJECT']->getUsername().' has no articles yet. Please check back later.';
				}
			}else if($this->setup['SUBJECT'] instanceOf Group){
				if($this->setup['OWNER']){
					$message = '<span>Do you want to share interesting travel stories but you don\'t really need several journal entries to do it? </span> <br /> Now you can write these stories as travel articles. Use Travel Articles to write about places, activities and news that are relevant to the travel community. <a href="/article.php?action=add"><strong>Write an article now<strong></a>';
				}else{
					$message = $this->setup['SUBJECT']->getName().' has no articles yet. Please check back later.';
				}
			}
			
			$showPagination = false;
			if($this->setup['SUBJECT'] instanceOf Group || ($this->setup['SUBJECT'] instanceOf Traveler && $this->setup['VIEWMODE'] != "PROFILE"))
				$showPagination = true;
			
			if($showPagination)
				$obj_template->set('pagination', $this->pagination);			
			
			$obj_template->set('subjectID', $this->setup['DATA']);
			$obj_template->set('context', $this->setup['VIEWMODE']);			
			$obj_template->set('noArticleMessage', $message);
			return $obj_template;
		}
		
		function getArticlesCount(){
			if(!is_null($this->articles))
				return count($this->articles);
			$this->retrieveArticles();	
			return count($this->articles);
		}
		
		function render(){
			require_once("Class.HtmlHelpers.php");
			
			$this->retrieveArticles();
			$this->configurePagination();
			// setting template
			$obj_template = $this->setupTemplate();
			$obj_template->out('travellog/views/'.$this->template);
		}
	}
