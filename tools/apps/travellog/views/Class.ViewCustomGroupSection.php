<?php
	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.Condition.php';
	require_once 'travellog/model/Class.FilterCriteria2.php';
	
	class ViewCustomGroupSection{
		private $mPanel = 'customGroupSectionContainer';
		private $mGroup = null;
		private $mMode = 'view';
		private $mTabAction = 'showActive';
		private $mIsAdminLogged = false;
		private $mSubNavigation = null;
	  private $mConfirmationMessage = null;
		private $mGrpSubGroups = null;
		private $mActiveCount = 0;
		private $mInActiveCount = 0;
		private $mFeatSubGroupsPanel = null;
		private $mSubGroupsPanel = null;
		private $mCustomGroupSectionPanel = null;
		private $mSearchKey = "";
		private $mRowsPerPage = 6;
		
		/**
		 * Sets the number of active groups in custom group section.
		 * 
		 * @param integer $cnt
		 * @return void
		 */
		function setActiveCount($cnt){
			$this->mActiveCount = $cnt;
		}
		
		/**
		 * Sets the search key. This is used in custom group search.
		 */
		function setSearchKey($key){
			$this->mSearchKey = $key;
		}
		
		/**
		 * Sets the number of groups shown per page
		 * 
		 * @param integer $rows
		 * @return void
		 */
		function setNumberOfGroupsPerPage($rows){
			$this->mRowsPerPage = $rows;
		}
		
		/**
		 * Sets the number of inactive groups in custom group section.
		 * 
		 * @param integer $cnt
		 * @return void
		 */
		function setInActiveCount($cnt){
			$this->mInActiveCount = $cnt;
		}
		
		function setPanel($_panel){
			$this->mPanel = $_panel;
		}
		
		function setGroup($_group){
			$this->mGroup = $_group;
		}
		
		function setTabAction($_tabAction){
			$this->mTabAction = $_tabAction;
		}
		
		function setMode($_mode){
			$this->mMode = $_mode;
		}
		
		function setIsAdminLogged($_isAdminLogged){
			$this->mIsAdminLogged = $_isAdminLogged;
		}
		
		function setSubNavigation($_subNav){
			$this->mSubNavigation = $_subNav;
		}
		
		function setFeaturedSubGroupsPanel ($_featSubGroupsPanel){
			$this->mFeatSubGroupsPanel = $_featSubGroupsPanel;
		}
		
		function setSubGroupsPanel ($_subGroupsPanel){
			$this->mSubGroupsPanel = $_subGroupsPanel;
		}
				
		function setCustomGroupSectionPanel ($_customGroupPanel){
			$this->mCustomGroupSectionPanel = $_customGroupPanel;
		}
		
		/**
		 * Sets the value of the confirmation message
		 * 
		 * @param string $message
		 * @return void
		 */
		function setConfirmationMessage($message){
			$this->mConfirmationMessage = $message;
		}
		
		function render(){
			
			switch ($this->mPanel){
			
				case 'customGroupSectionContent':
					$this->renderSectionContent();
					break;
				
				case 'customGroupSectionContainer':
					$this->renderSectionContainer();
					break;
				
				case 'customGroupSectionForm':
					$this->renderSectionForm();
					break;				
							
			}
		
		}
	
		/**
		*	function to show the two columns in custom group section management page
		*	show content in both columns
		*/
		function renderSectionContent(){
			
			require_once 'travellog/model/Class.Condition.php';
			require_once 'travellog/model/Class.FilterCriteria2.php';
			
			$condorder = new Condition;
			$condorder->setAttributeName("name");

			$order = new FilterCriteria2();
			$order->addCondition($condorder);
			
			$is_searching = ("" == trim($this->mSearchKey)) ? false : true;
			
			//get all subgroups of this group
			$allSubGroups = $this->mGroup->getSubGroups(null,$order);
			
			//get all subgroups chosen for this custom group, and set to a list
			$customGroupSec = $this->mGroup->getCustomGroupSection();
			
			$this->tplCustomGroupSection = new Template();
			$this->tplCustomGroupSection->set_path("travellog/views/");
			
			$this->tplCustomGroupSection->set("group",$this->mGroup);	
			$this->tplCustomGroupSection->set("grpID",$this->mGroup->getGroupID());
			$this->tplCustomGroupSection->set("customGroupSection",$customGroupSec );	
			$this->tplCustomGroupSection->set('isAdminLogged',$this->mIsAdminLogged);
		
			$this->tplCustomGroupSection->set('featSubGroupsPanel', $this->mFeatSubGroupsPanel);
			$this->tplCustomGroupSection->set('subGroupsPanel', $this->mSubGroupsPanel);
			$this->tplCustomGroupSection->set('mode', $this->mMode);
			$this->tplCustomGroupSection->set('tabAction', $this->mTabAction);
			$this->tplCustomGroupSection->set('active_cnt', $this->mActiveCount);
			$this->tplCustomGroupSection->set('inactive_cnt', $this->mInActiveCount);
			$this->tplCustomGroupSection->set('is_searching', $is_searching);
			$this->tplCustomGroupSection->set('rows_per_page', $this->mRowsPerPage);
			$this->tplCustomGroupSection->set('allSubGroups',$allSubGroups );
			
			$this->tplCustomGroupSection->set('searchKey',$this->mSearchKey);
			
			$this->tplCustomGroupSection->out("tpl.CustomGroupSection.php");
			
		}
		
		/**
		*	function to render the container of the group section content
		*	shows either the content itself; or the empty page with a link to add a new custom group section
		*/
		
		function renderSectionContainer(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($this->mGroup->getGroupID());
			
			$this->tplContainer = new Template();
			$this->tplContainer->set_path("travellog/views/");
			
			$this->tplContainer->set("profile", $profile_comp->get_view());
			$this->tplContainer->set('subNavigation',$this->mSubNavigation);
			$this->tplContainer->set('grpID',$this->mGroup->getGroupID());
			$this->tplContainer->set('mode', $this->mMode);
			$this->tplContainer->set('isAdminLogged',$this->mIsAdminLogged);
			$this->tplContainer->set('customGroupSectionPanel', $this->mCustomGroupSectionPanel);
			$this->tplContainer->set('success', $this->mConfirmationMessage);
	
			$this->tplContainer->out("tpl.CustomGroupSectionContainer.php");
		}
		
		/**
		*	function to render the form for the group section content
		*	shows either the content itself; or the empty page with a link to add a new custom group section
		*/

		function renderSectionForm(){

			$this->tplContainer = new Template();
			$this->tplContainer->set_path("travellog/views/");

			$this->tplContainer->set('group',$this->mGroup);
	
			$this->tplContainer->out("tpl.CustomGroupSectionForm.php");

		}
	}
?>