<?
$file_factory = FileFactory::getInstance();
$d = $file_factory->getClass('GaDateTime');
?>
<div id="itinerary_header">
	<h2><?=$props['obj_program']->getTitle() ?></h2>
	<div class="iactions">
		<?if( $props['pID'] ):?>
			<strong>
			<? if($props['is_owner']): ?>
				<a href="javascript:void(0)" onclick="jQuery().addProgramForm('action=Edit&pID=<?=$props['pID']?>')">Edit</a>&nbsp;&nbsp;|&nbsp;
				<a href="javascript:void(0)" onclick="jQuery().deleteProgram('action=Delete&pID=<?=$props['pID']?>')">Delete</a>&nbsp;&nbsp;|&nbsp;
				<a href="/photomanagement.php?action=add&cat=program&genID=<?=$props['pID']?>">Add Photo</a>
			<? endif; ?>
			<? if( $props['obj_program']->hasPhoto() ): ?>
				&nbsp;|&nbsp;&nbsp;<a href="/photomanagement.php?action=view&cat=program&genID=<?= $programID ?>">View Photo</a>
			<?endif;?>
			</strong>
		<?endif;?>
	</div><div class="clear"></div>
</div>
<div id="program_info">
	<?if( $props['pID'] ): ?>
		<h3>
		From <?= GaDateTime::friendlyFormat($props['obj_program']->getTheDate()) ?> to 
		<?=GaDateTime::friendlyFormat($props['obj_program']->getFinish()) ?>
		</h3>
		<strong>Groups:</strong> <?= $subgroup_names ?><br /><br />
		<div class="description">
		<?=HtmlHelpers::Textile($props['obj_program']->getDescription())?>
		</div>
	<? else: ?>
		<?if( $props['is_login'] && $props['is_owner'] ):?>
			<strong>You have not created an itenaries yet.</strong>
		<?else:?>
			<strong>No itenaries for this group.</strong>
		<?endif;?>
	<? endif; ?>
</div>
