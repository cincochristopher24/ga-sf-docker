<?php
	$tags = GanetPageTagPeer::retrieveByGroup($group->getGroupID(),$group->getParentID());
	$pagesHelper = GroupPagesHelper::getInstance();

	if( $isAdminLogged ){
		$staff = count($group->getStaff($list=TRUE));
		$members = $group->getGroupMembers($rowslimit = NULL, $filtercriteria = NULL, $order = NULL, $unassigned=FALSE,$includePending=TRUE,$countOnly=TRUE) - $staff;
	}else{
		$members = $group->getMembers($rowslimit = NULL, $filtercriteria = NULL, $order = NULL, $count = TRUE);				
	}
	$memberLabel = 1 > $members ? "" : 1 < $members ? "$members Members" : "$members Member";
	
	require_once("travellog/dao/Class.GroupApprovedJournals.php");
	$relatedEntries = GroupApprovedJournals::countGroupRelatedEntries($group->getGroupID());
	$relatedEntriesLabel = 1 > $relatedEntries ? "" : 1 < $relatedEntries ? "$relatedEntries Journal Entries" : "$relatedEntries Journal Entry";
	
	$counterLabel = "No Members / No Journal Entries";
	if( $members > 0 && $relatedEntries > 0 ){
		$counterLabel = $memberLabel." / ".$relatedEntriesLabel;
	}else if( $members < 1 && $relatedEntries > 0 ){
		$counterLabel = $relatedEntriesLabel;
	}else if( $members > 0 && $relatedEntries < 1 ){
		$counterLabel = $memberLabel;
	}
?>

<tr id="<?php echo $category;?>_item_<?php echo $group->getGroupID()?>" name="<?php echo $category;?>_item" class="">
	<?php if( $isAdminLogged && !$isCobrandSearchPage ): ?>
		<td class="selectCol">
			<input class="checkBox" type="checkbox" name="chkGroup_<?php echo $category;?>" value="<?php echo $group->getGroupID();?>" onchange="if(this.checked){jQuery('#<?php echo $category?>_item_<?php echo $group->getGroupID()?>').addClass('selected');}else{jQuery('#<?php echo $category?>_item_<?php echo $group->getGroupID()?>').removeClass('selected');}" />
		</td>
	<?php endif; ?>
	<td class="detailsCol">
		<a title="View Group Profile" class="thumb" href="<?php echo $group->getFriendlyURL();?>">
			<img class="pic" width="65px" height="65px" alt="Group Emblem" src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>"  />
		</a>
		<div class="details">
			<p class="text">
				<a class="groupName" href="<?php echo $group->getFriendlyURL();?>" title="<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?>">
					<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES); ?>
				</a>
				<input type="hidden" id="groupName_<?php echo $group->getGroupID();?>" value="<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?>" />
				<span class="groupMeta">
					<?php if( $group->isGroupHomeFeatured() && $isAdminLogged ): ?><span class="featBadge">Featured</span><?php endif; ?>
					<?php echo $counterLabel; ?>
				</span>								
			</p>
			<?php if( $isAdminLogged ): ?>								
				<div class="text groupTagList" id="groupTagList_<?=$group->getGroupID()?>">
					<?php if(count($tags)): ?>
					<label class="tagsLabel">Tags:</label>
					<?php foreach($tags AS $x => $tag): ?>
						<?php if( $isAdminLogged && !$isCobrandSearchPage ): ?>
							<a class="tag" href="<?php echo $pagesHelper->getTagUrl($tag);?>">
								<?php echo $tag->getName();?>
								<span class="removeTag" title="Remove Tag" onclick="return false;">
									<img width="9" height="9" src="/images/v3/remove_x.gif" alt="Remove" name="removeTag" id="removeTag_<?=$group->getGroupID()?>_<?=$tag->getID()?>" />
								</span>
							</a><?php if($x<count($tags)-1): ?>, <?php endif;?>
						<?php else: ?>
							<a class="tag" href="<?php echo $pagesHelper->getTagUrl($tag);?>"><?php echo $tag->getName();?></a><?php if($x<count($tags)-1): ?>, <?php endif;?>
						<?php endif; ?>
					<?php endforeach; ?>	
					<?php endif; ?>
				</div>								
			<?php endif; ?>
			<?php
				if( $isAdminLogged && !$isCobrandSearchPage ){
					$tpl = new Template();
					$tpl->set("group",$group);
					$tpl->set("category",$category);
					echo $tpl->fetch("travellog/views/group-pages/tpl.GroupListItemActionLinks.php");
				}
			?>																								
		</div> <!-- end details -->
	</td>
</tr>