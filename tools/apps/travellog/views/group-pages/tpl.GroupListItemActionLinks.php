<p class="actions">
	<?php if( $group->isGroupHomeFeatured() ): ?>
		<a class="alpha" href="javascript: void(0)" onclick="jQuery.fn.unfeature('<?php echo $category;?>',<?php echo $group->getGroupID();?>); return false;" title="Unfeature this group">Unfeature</a>
	<?php else: ?>
		<a class="alpha" href="javascript: void(0)" onclick="jQuery.fn.feature('<?php echo $category;?>',<?php echo $group->getGroupID();?>); return false;" title="Feature this group in your homepage">Feature</a>
	<?php endif; ?>
	<a href="javascript:void(0);" name="<?=$group->getGroupID()?>" id="assign_tags_to_group" title="Assign tags to this group" onclick="return false;">Assign tags</a>
	<a class="omega" href="javascript: void(0)" onclick="jQuery.fn.deleteGroup('<?php echo $category;?>',<?php echo $group->getGroupID();?>); return false;" title="Delete this group">Delete</a>
	<a href="javascript:GroupTemplateManager.showCreateForm(<?=$group->getGroupID()?>)" title="save this group as template">Save as template</a>	
</p>