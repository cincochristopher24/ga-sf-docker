<?php if( is_object($context) && ($context instanceof TravelLog || $context instanceof PhotoAlbum) ):
	$collection_type = $context instanceof TravelLog ? "travellog" : ( 0 < $context->getGroupID() ? "photoalbum" : "traveleralbum" );
	$photocount = $context instanceof TravelLog ? $context->countPhotos() : $context->getCountPhotos();
?>
<div class="photo"> 
	<div id="peralbum_<?php echo get_class($context);?>_<?php echo $context->getID();?>">	
		<a onclick="collectionPopup.loadGallery('<?php echo $collection_type;?>',<?php echo $context->getID();?>,<?php echo $photo->getID();?>)" href="javascript:void(0)">
			<img class="photo_bg" src="<?php echo $photo->getPhotoLink('featured');?>" id="listen_img_album_<?php echo $photo->getID();?>" />
		</a>
																
		<!-- album title container- for ajax callback request-->
		<div id="ptitle_<?php echo $photo->getID();?>">
			<p class="photo_caption">
				<a href="<?php echo $context->getFriendlyURL();?>"><?php echo htmlspecialchars($context->getTitle(),ENT_QUOTES);?></a><br />
				<span style="font-size: 10px; text-align: right;" id="listen_caption_album_<?php echo $photo->getID();?>">
					<?php echo 1 < $photocount ? $photocount." photos" : $photocount." photo";?>
				</span>
			</p>			
		</div>
	</div> 								
</div><!-- end photo -->
<?php endif; ?>