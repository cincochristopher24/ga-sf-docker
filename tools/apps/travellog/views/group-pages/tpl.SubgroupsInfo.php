<?php if( 'narrow' == $layout ): ?>
<div class="section help">		
	<h2 class="title">What are SubGroups?</h2>
	<div class="content">
		<p class="help">
			SubGroups are useful for creating smaller and thus more manageable groups. Each SubGroup functions as a complete independent group with it's own messaging functions, discussion boards, and management tools. 
			<a href="faq.php#subgroups">More on Subgroups &raquo;</a>
		</p>
	</div>
</div>

<?php else: ?>
<div class="section">		
	<h2 class="title">What are SubGroups?</h2>
	<div class="content">
		<div class="help_text">
			<p>
				SubGroups are useful for creating smaller and thus more manageable groups. Each SubGroup functions as a complete independent group with it's own messaging functions, discussion boards, and management tools. 
			</p>
			
			<p>
				We recommend creating a unique SubGroup for every physical group of participants you manage. For instance, "Study in Barcelona - Spring 2010".
			</p>	
			
			<p>
				You can group your members into smaller, more manageable subgroups. Each subgroup can be its own community within your group, with their own private messaging and discussion boards. 	
			</p>
			
			<p>Not sure you need subgroups? Consider the guide below: </p>
			
			<ul class="discList">
				<li>
					Will you be sending out targeted and specific communication to batches of members at a time? 	
				</li>
				<li>
					Will some of your members share a common experience and feel the need to connect exclusively with a group? 
				</li>
			</ul>
			
			<p>
				If you answered Yes to any of the questions above, you may need to create subgroups for more efficient management of your members. We recommend that you build these groups first before adding or inviting members.
			</p>
			
			<p>
				To get started, think about what your groups should be. When you are ready, you can start adding them <a href="/group.php?mode=add&pID=<?php echo $group->getGroupID();?>">here</a>
			</p>		
		
		</div>
	</div>
</div>			
<?php endif; ?>


