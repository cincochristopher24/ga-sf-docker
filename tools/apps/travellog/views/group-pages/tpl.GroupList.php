<?php if( $showCategory ): ?>
	<div id="<?php echo $category;?>_section" class="section">
		<?php if( 'TAGGED' == $category ): ?>
			<h2 id="<?php echo $category?>_label">
				<span class="toLower"><?php echo $label; ?></span>
				<?php if( $isAdminLogged && !$isCobrandSearchPage ): ?>
					<span class="header_actions">
						<span class="lighterText"><a class="strong" href="/group.php?mode=add&pID=241&tags=<?php echo implode("+",$contextTags);?>" target="_blank">Create a New SubGroup</a> under this page</span>
					</span>
				<?php endif; ?>		
			</h2>
		<?php else: ?>	
	
			<h2 id="<?php echo $category?>_label">
				<span><?php echo $label;?></span>
			</h2>
			
		
		<?php endif; ?>
		<div id="<?php echo $category;?>_content" class="content">
			<input type="hidden" id="<?php echo $category?>_item_count" value="<?php echo $totalItemCount?>" />
			<input type="hidden" id="<?php echo $category?>_active_page" value="1" />
			<?php
				if( $isAdminLogged && !$isCobrandSearchPage ):
					$tpl = new Template();
					$tpl->set("category",$category);
					$tpl->set("contextTags",$contextTags);
					$tpl->set("showCategoryStyle",$showCategoryStyle);
					echo $tpl->fetch("travellog/views/group-pages/tpl.BatchActionsHeader.php");
				endif;
			?>
			
			<?php if( 'FEATURED' == $category && $isAdminLogged && !isset($_GET["search"]) ): ?>
				<p id="FEATURED_nonempty_notice" class="section_small_details" <?php if(!count($groups)): ?>style="display:none;"<?php endif;?> >These are the subgroups that appear on your group's home page. Customize your subgroup list to feature new groups, highlight programs or call attention to new content.</p>
				<p id="FEATURED_empty_notice" class="section_small_details" <?php if(count($groups)): ?>style="display:none;"<?php endif;?> >You can select the subgroups that will appear on your group's home page. Customize your subgroup list to feature new groups, highlight programs or call attention to new content.</p>
			<?php elseif( 'RECENT' == $category && $isAdminLogged && !isset($_GET["search"]) ): ?>
				<p id="RECENT_nonempty_notice" class="section_small_details" <?php if(!count($groups)):?>style="display:none;"<?php endif;?> >Use the buttons to manage the subgroups individually, or tick on the checkboxes to feature or assign tags in batches.</p>
			<?php endif; ?>
			
			
			<div id="<?php echo $category;?>_content_body">
				<?php
					$template = new Template();
					$template->set("category",$category);
					$template->set("groups",$groups);
					$template->set("loggedUser",$loggedUser);
					$template->set("isAdminLogged",$isAdminLogged);
					$template->set("isCobrandSearchPage",$isCobrandSearchPage);
					if( isset($paging) ){
						$template->set("paging",$paging);
						$template->set("iterator",$iterator);
					}
					if( $isAdminLogged && !$isCobrandSearchPage ){
						echo $template->fetch("travellog/views/group-pages/tpl.GroupListContent.php");
					}else{
						echo $template->fetch("travellog/views/group-pages/tpl.GroupListContentPublic.php");
					}
				?>
			</div>
		</div>				
	</div>
<?php elseif( $showNoGroupNotice ): ?>
	<div class="section">
		<div class="content">
			<p class="notice">
				<?php if( "SEARCH" == $category ): ?>
					Sorry. We couldn't find a subgroup with name matching <strong><?php echo $_GET["keyword"]; ?></strong>.
				<?php else: ?>
					Sorry. We couldn't find a subgroup under <strong><?php echo implode("&raquo; ",$contextTags);?></strong>.
				<?php endif; ?>
			</p>
		</div>
	</div>
<?php endif; ?>