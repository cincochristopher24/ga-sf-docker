<div class="mainSectionHead">
	<?php if( $isAdminLogged && !$isCobrandSearchPage ): ?>
		<?php if( !$taggedGroupCount ): ?>
			<h2 class="title">You <span id="result_count_label">don't have any subgroup</span> under
				<?php $trace_tags = array(); ?>
				<?php foreach( $tags AS $x => $tag): ?>
					<?php if($x == count($tags)-1 ): ?>
						<span class="pageTag"><?php echo $tag; ?></span>
					<?php else: ?>
						<?php $trace_tags[] = $tag; ?>
						<a href="<?php echo GroupPagesHelper::getInstance()->getTraceTagsUrl($group->getGroupID(),$trace_tags);?>" class="pageTag"><?php echo $tag; ?></a> &raquo;
					<?php endif; ?>
				<?php endforeach; ?>
			</h2>
		<?php else: ?>
			<h2 class="title">You <span id="result_count_label">have <?php echo $taggedGroupCount; ?> <?php if(1 < $taggedGroupCount): ?>subgroups<?php else:?>subgroup<?php endif;?></span> under
				<?php $trace_tags = array(); ?>
				<?php foreach( $tags AS $x => $tag): ?>
					<?php if($x == count($tags)-1 ): ?>
						<span class="pageTag"><?php echo $tag; ?></span>
					<?php else: ?>
						<?php $trace_tags[] = $tag; ?>
						<a href="<?php echo GroupPagesHelper::getInstance()->getTraceTagsUrl($group->getGroupID(),$trace_tags);?>" class="pageTag"><?php echo $tag; ?></a> &raquo;
					<?php endif; ?>
				<?php endforeach; ?>
			</h2>
		<?php endif; ?>
	<?php else: ?>
		<?php if( !$taggedGroupCount ): ?>
			<h2 class="title"><?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?> doesn't have any subgroup under
				<?php $trace_tags = array(); ?>
				<?php foreach( $tags AS $x => $tag): ?>
					<?php if($x == count($tags)-1 ): ?>
						<span class="pageTag"><?php echo $tag; ?></span>
					<?php else: ?>
						<?php $trace_tags[] = $tag; ?>
						<a href="<?php echo GroupPagesHelper::getInstance()->getTraceTagsUrl($group->getGroupID(),$trace_tags);?>" class="pageTag"><?php echo $tag; ?></a> &raquo;
					<?php endif; ?>
				<?php endforeach; ?>
			</h2>
		<?php else: ?>
			<h2 class="title"><?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?> has <?php echo $taggedGroupCount; ?> <?php if(1 < $taggedGroupCount): ?>subgroups<?php else:?>subgroup<?php endif;?> under
				<?php $trace_tags = array(); ?>
				<?php foreach( $tags AS $x => $tag): ?>
					<?php if($x == count($tags)-1 ): ?>
						<span class="pageTag"><?php echo $tag; ?></span>
					<?php else: ?>
						<?php $trace_tags[] = $tag; ?>
						<a href="<?php echo GroupPagesHelper::getInstance()->getTraceTagsUrl($group->getGroupID(),$trace_tags);?>" class="pageTag"><?php echo $tag; ?></a> &raquo;
					<?php endif; ?>
				<?php endforeach; ?>
			</h2>
		<?php endif; ?>
	<?php endif; ?>
	<?/*		
	<p class="details">
		Can't find what you're looking for? Go back to the list of all tags to <a href="<?php echo GroupPagesHelper::getInstance()->getDefaultPageUrl($group->getGroupID()); ?>" class="strong">Start over &raquo;</a>
	</p>
	*/?>
</div>