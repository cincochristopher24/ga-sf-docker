<?php require_once("travellog/views/Class.JournalsComp.php"); ?>
<ul class="journals">			 		
	<?php foreach($items AS $item): ?>
		<?php
			$tpl = new Template();
			$tpl->set("entry",$item);
			$tpl->set("journal",$item->getTravel());
			echo $tpl->fetch("travellog/views/group-pages/tpl.GroupPagesRandomJOURNALItem.php");
		?>
	<?php endforeach; ?>
</ul><!-- end journals -->