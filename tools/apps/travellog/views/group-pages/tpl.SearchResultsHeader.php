<?php if( isset($defaultAdminHeader) && $defaultAdminHeader ): ?>
	<div class="mainSectionHead">		
		<h2 class="title">Manage your subgroups</h2>	
		<p class="details">
			<?php if( 0 < $resultCount ): ?>
				<?php if( "" == $keyword ): ?>
					You have <strong><?php echo $resultCount; ?> <?php echo (1==$resultCount) ? "subgroup" : "subgroups"; ?></strong>. Feature, tag and manage your subgroups with the options below.
				<?php else: ?>
					You have <strong><?php echo $resultCount; ?> <?php echo (1==$resultCount) ? "subgroup" : "subgroups"; ?></strong> with name matching <strong><?php echo $keyword;?></strong>. Feature, tag and manage your subgroups with the options below.
				<?php endif; ?>
			<?php else: ?>
				<?php if( "" == $keyword ): ?>
					Your group <strong><?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?></strong> doesn't have a subgroup yet.
				<?php else: ?>
					Your group <strong><?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?></strong> doesn't have a subgroup with name matching <strong><?php echo $keyword;?></strong>.
				<?php endif; ?>
			<?php endif; ?>
		</p>
	</div>		
<?php else: ?>
	
<div class="mainSectionHead">
	<?php if( "" == $keyword ): ?>
		<?php if( $isAdminLogged && !$isCobrandSearchPage ): ?>
			<?php if(  1 < $resultCount ): ?>
				<h2 class="title">You <span id="result_count_label">have <?php echo $resultCount; ?> subgroups</span></h2>
			<?php elseif( 1 == $resultCount ): ?>
				<h2 class="title">You <span id="result_count_label">have 1 subgroup</span></h2>
			<?php else: ?>
				<h2 class="title">You <span id="result_count_label">don't have a subgroup yet in</span> <?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?></h2>
			<?php endif; ?>
		<?php else: ?>
			<?php if(  1 < $resultCount ): ?>
				<h2 class="title"><?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES); ?> has <?php echo $resultCount ?> subgroups</h2>
			<?php elseif( 1 == $resultCount ): ?>
				<h2 class="title"><?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES); ?> has 1 subgroup</h2>
			<?php else: ?>
				<h2 class="title"><?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES); ?> doesn't have a subgroup yet.</h2>
			<?php endif; ?>
		<?php endif; ?>
	<?php else: ?>
		<h2 class="title">
			<?php if( $isAdminLogged && !$isCobrandSearchPage ): ?>
				<?php if(  1 < $resultCount ): ?>
					You <span id="result_count_label">have <?php echo $resultCount; ?> subgroups</span> with name matching
				<?php elseif( 1 == $resultCount ): ?>
					You <span id="result_count_label">have 1 subgroup</span> with name matching
				<?php else: ?>
					You <span id="result_count_label">don't have a subgroup yet</span> with name matching
				<?php endif; ?>
			<?php else: ?>
				<?php if(  1 < $resultCount ): ?>
					<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES); ?> has <?php echo $resultCount; ?> subgroups with name matching
				<?php elseif( 1 == $resultCount ): ?>
					<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES); ?> has 1 subgroup with name matching
				<?php else: ?>
					<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES); ?> doesn't have a subgroup with name matching
				<?php endif; ?>
			<?php endif; ?>
			<span class="pageTag"><?php echo htmlspecialchars($keyword,ENT_QUOTES); ?></span>
		</h2>
	<?php endif; ?>
	<p class="details">
		<?php if( $resultCount && $isCobrandSearchPage && isset($_GET["tags"]) ): ?>
			Can't find what you're looking for? Go back to the list of all tags to <a href="<?php echo GroupPagesHelper::getInstance()->getDefaultPageUrl($group->getGroupID()); ?>" class="strong">Start over &raquo;</a>
		<?php else: ?>
			<?php if( $isAdminLogged && $isCobrandSearchPage && !isset($_GET["search"]) ): ?>
				You may want to add subgroups now. <a href="/group.php?mode=add&pID=<?php echo $group->getGroupID();?>" class="strong">Create a SubGroup &raquo;</a>
			<?php elseif( $isCobrandSearchPage && !$resultCount ): ?>
				Please check back later.
				<?php if(isset($GLOBALS["CONFIG"]) ): ?>
					Proceed to the <?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?> <a href="/" class="strong">Homepage &raquo;</a>
				<?php else: ?>
					You may want to check other groups within the network. Check other <a href="/group.php" class="strong">Groups &raquo;</a>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
	</p>
</div>

<?php endif; ?>