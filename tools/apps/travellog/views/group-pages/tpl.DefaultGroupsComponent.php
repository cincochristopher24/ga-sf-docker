<?php
	//landing page
	if( isset($featuredGroups) && isset($otherGroups) ){
		if( count($featuredGroups)+count($otherGroups) ){
			if( (count($featuredGroups) || $isAdminLogged) && !$isCobrandSearchPage ){
				$tpl = new Template();
				$label = "Homepage Featured SubGroups";
				$tpl->set("label",$label);
				$tpl->set("category","FEATURED");
				$tpl->set("groups",$featuredGroups);
				$tpl->set("isAdminLogged",$isAdminLogged);
				$tpl->set("loggedUser",$loggedUser);
				$tpl->set("isCobrandSearchPage",$isCobrandSearchPage);
				$tpl->set("contextTags",$contextTags);
				$tpl->set("showNoGroupNotice",FALSE);
				$tpl->set("showCategory", count($featuredGroups) || $isAdminLogged );
				$tpl->set("showCategoryStyle", count($featuredGroups) ? "" : "display:none;" );
				$tpl->set("totalItemCount", count($featuredGroups));
				echo $tpl->fetch("travellog/views/group-pages/tpl.GroupList.php");
			}
			if( count($otherGroups) || $isAdminLogged ){
				$tpl = new Template();
				$label = count($featuredGroups) ? "Other SubGroups" : "";
								
				$tpl->set("label",$label);
				$tpl->set("category","RECENT");
				$tpl->set("groups",$otherGroups);
				$tpl->set("isAdminLogged",$isAdminLogged);
				$tpl->set("loggedUser",$loggedUser);
				$tpl->set("isCobrandSearchPage",$isCobrandSearchPage);
				$tpl->set("contextTags",$contextTags);
				$tpl->set("showNoGroupNotice",FALSE);
				$tpl->set("showCategory", count($otherGroups) || $isAdminLogged );
				$tpl->set("showCategoryStyle", count($otherGroups) ? "" : "display:none;" );
				$tpl->set("totalItemCount", $totalOtherGroupsCount);
				
				$paging = new Paging($totalOtherGroupsCount, $page, 'action=FETCH_RECENT&RECENT', GroupPagesHelper::RPP );
				$paging->setOnclick("jQuery.fn.loadPage");
				$iterator = new ObjectIterator($otherGroups,$paging );
				$tpl->set("paging",$paging);
				$tpl->set("iterator",$iterator);
				
				echo $tpl->fetch("travellog/views/group-pages/tpl.GroupList.php");
			}
		}
	}
	
	//drilled down
	if( isset($taggedGroups) ){
		$tpl = new Template();
		$tpl->set("label","");
		$tpl->set("category","TAGGED");
		$tpl->set("groups",$taggedGroups);
		$tpl->set("isAdminLogged",$isAdminLogged);
		$tpl->set("loggedUser",$loggedUser);
		$tpl->set("isCobrandSearchPage",$isCobrandSearchPage);
		$tpl->set("contextTags",$contextTags);
		$tpl->set("showNoGroupNotice",TRUE);
		$tpl->set("showCategory", count($taggedGroups) || $isAdminLogged );
		$tpl->set("showCategoryStyle", count($taggedGroups) ? "" : "display:none;" );
		$tpl->set("totalItemCount", $totalTaggedGroupsCount);
		
		$paging = new Paging($totalTaggedGroupsCount, $page, 'action=FETCH_TAGGED&TAGGED', GroupPagesHelper::RPP );
		$paging->setOnclick("jQuery.fn.loadPage");
		$iterator = new ObjectIterator($taggedGroups,$paging );
		$tpl->set("paging",$paging);
		$tpl->set("iterator",$iterator);
		
		echo $tpl->fetch("travellog/views/group-pages/tpl.GroupList.php");
	}
	
	//find by name
	if( isset($foundGroups) ){
		$tpl = new Template();
		$tpl->set("label","");
		$tpl->set("category","SEARCH");
		$tpl->set("groups",$foundGroups);
		$tpl->set("isAdminLogged",$isAdminLogged);
		$tpl->set("loggedUser",$loggedUser);
		$tpl->set("isCobrandSearchPage",$isCobrandSearchPage);
		$tpl->set("contextTags",$contextTags);
		$tpl->set("showNoGroupNotice",TRUE);
		$tpl->set("showCategory", count($foundGroups) || $isAdminLogged );
		$tpl->set("showCategoryStyle", count($foundGroups) ? "" : "display:none;" );
		$tpl->set("totalItemCount", $totalFoundGroupsCount );
		
		$paging = new Paging($totalFoundGroupsCount, $page, 'action=FETCH_BY_NAME&SEARCH', GroupPagesHelper::RPP );
		$paging->setOnclick("jQuery.fn.loadPage");
		$iterator = new ObjectIterator($foundGroups,$paging );
		$tpl->set("paging",$paging);
		$tpl->set("iterator",$iterator);
		
		echo $tpl->fetch("travellog/views/group-pages/tpl.GroupList.php");
	}
?>
<p name="confirmation_alert" class="confirmation" style="display:none;">
</p>
<p name="notice_alert" class="notice" style="display:none;">
</p>	
<p name="error_alert" class="error_notice" style="display:none;">
</p>