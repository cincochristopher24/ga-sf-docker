<?php
	$content = (strlen(trim($entry->getDescription())) > 175 )? strip_tags(nl2br(substr($entry->getDescription(),0,175))) . '...' : strip_tags(nl2br($entry->getDescription()));
	
	$owner = $journal->getOwner();
	if (is_null($owner)){
		echo $journal->getTravelID();
		return;
	}
	
	$locationID = 0;
	try{
		$trip = $entry->getTrip();
		if( is_object($trip) ){
			$location = $trip->getLocation();
			if( is_object($location) ){
				$country = $location->getCountry()->getName();
				$city = $trip->getLocation()->getName();
				$locationID = $trip->getLocation()->getCountry()->getCountryID();
			}
		}
	}catch(exception $e){}
	
	$primaryphoto = NULL;
	try{
		$primaryphoto = $entry->getPrimaryPhoto();
	}catch(exception $e){
		$primaryphoto = NULL;
	}
	if(is_null($primaryphoto)){
		$primaryphoto = $journal->getPrimaryPhotoInstance();
	}
?>

<li id="container<?php echo $journal->getTravelID();?>" class="container ajax_journal">     
	<div class="journal_box_wrap">
		<div class="header">							
			<h3 class="group_h">														
					<a href="/journal-entry.php?action=view&amp;travellogID=<?php echo $entry->getTravellogID()?>"><?php echo htmlspecialchars($journal->getTitle(),ENT_QUOTES); ?></a>
			</h3>				 		    									
		</div>				
		<div id="content<?php echo $journal->getTravelID();?>" class="content">					      			   
			 <div class="photo">
				<?php if( $primaryphoto->getID() ): ?>
					<a href="/journal-entry.php?action=view&amp;travellogID=<?php echo $entry->getTravellogID()?>"> 
						<img alt="journal photo" src="<?php echo $primaryphoto->getPhotoLink('featured');?>" class="float_right">
					</a>
				<?php endif; ?>
			 </div>								   															
			<div class="entry_title">
				<a href="/journal-entry.php?action=view&amp;travellogID=<?php echo $entry->getTravellogID()?>">
					<?php echo htmlspecialchars($entry->getTitle(),ENT_QUOTES); ?>
				</a>
			</div>										
			<div class="entry_detail">	
				<span class="location_info">
					<?php if($locationID): ?>
						<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" alt="Travel Journal from <?=$country?>" style="margin: 0 0 0 0;"/>						
						<?php echo (strtolower($city) == strtolower($country))? $country : $city . ', ' . $country?> | <?=(strtotime($trip->getArrival())) ? date('D M d, Y',strtotime($trip->getArrival())) : ''?>
					<?php endif; ?>
				</span>
					
				<p>						
					<?php echo $content; ?>
				</p>
			</div>
					
			<?php if($owner instanceof Traveler): ?>
				<p class="meta">
					traveler: 					
					<a class="author_username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$owner->getUserName()?>"><strong><?=$owner->getUserName()?></strong></a>
				</p>
			<? endif; ?>
		</div>						
	</div>
</li> <!-- end journal entry -->