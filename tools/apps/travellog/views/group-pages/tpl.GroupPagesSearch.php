<?php
	Template::includeDependentJs('/js/jquery.url/jquery.url.packed.js');
	Template::includeDependentJs('/js/tagCloud/scripts/jquery.tagcloudmanager.js');	
?>
<?/*
<div id="intro_container">
	<div id="intro">
		<h1>Get expert travel advice.</h1>
		<div class="content">
		  	Join Groups and get reliable expert tips and advice from savvy travelers and professionals.
		</div>
	</div>
</div>
*/?>
<div class="layout_2" id="content_wrapper">
	<?php if( isset($breadcrumbs) ): ?>
		<?php echo $breadcrumbs; ?>
	<?php endif; ?>
	<?php if( isset($searchResultsHeader) ): ?>
		<?php echo $searchResultsHeader; ?>
	<?php endif; ?>
	
	<div id="wide_column">
		<?php if(isset($wideTagCloud)): ?>
			<?= $wideTagCloud ?>
		<?php endif;?>
		<?php if( isset($defaultGroups) ): ?>
			<?php echo $defaultGroups; ?>
		<?php endif; ?>
		<?php if( isset($randomSections) ): ?>
			<?php foreach($randomSections AS $section): ?>
				<?php echo $section;?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
		
	<div id="narrow_column">
		<?php if(isset($narrowTagCloud)): ?>
			<?= $narrowTagCloud ?>
		<?php endif;?>
		<?php if( isset($searchForm) ): ?>
			<?php echo $searchForm; ?>
		<?php endif; ?>
	</div>
</div>

<?php if( isset($configScript) ): ?>
	<?php echo $configScript; ?>
<?php endif; ?>