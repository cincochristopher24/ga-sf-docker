<?php
	ob_start();
	
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('layoutID', 'page_photos');
	
	//Template::includeDependentJs("/js/prototype.js"); 
	//Template::includeDependentJs("/js/scriptaculous/effects.js"); 
	//Template::includeDependentJs("/js/scriptaculous/scriptaculous.js"); 
	//Template::includeDependentJs("/js/scriptaculous/unittest.js"); 
	
	
	
	Template::includeDependentJs("/js/photoalbum.js", array("bottom"=>true)); 
	
	$code =<<<BOF
	<script type = "text/javascript">
	
	var photo = new PhotoAlbum($groupID,$loginID);
			//window.onload = function(){photo.loadPhotoAlbumLists();}
			//Event.observe(document, "click", photo.showedit, true);
		function page(s,p){photo.page(s,p);}				
	</script>
BOF;

		Template::includeDependent($code, array("bottom"=>true)); 
ob_end_flush();
?>
	
	<? echo $profile_header->render(); ?>
			
	<? $subNavigation->show(); ?>
	
	
	
	<div id="content_wrapper">
		<? if($isOwner):?>
			<a href="/photoalbum.php?action=add&groupID=<? echo $groupID?>">
				Create New Photo album
			</a>
		<? endif; ?>
		
		<div class="section">	
			<h2><span>Photo Album</span></h2>

			<div class="content">
		
				<div class="area<? if (!$subNavigation->getDoNotShow()): ?> mine<?endif;?>" id="intro">
				</div>
				
				<div class="top_head"><span></span></div>
				<div id="album_cont">			
					
						<div id="photoalbum">
							<?=$photoalbumservice->getPhotoAlbum();?>
						</div>
						
						<div class="photo_foot"></div>
						<div class="clear"></div>
				</div>
				
			</div>
			<div class="foot"></div>
			
			
			
			<div id="showedit" style="background-color:#EEEEEE;width:365px;border: solid #455D8B;position:absolute;height:100px;display:none">
				<div style="background-color:#455D8B;color:white;padding:2px;clear:both;"><strong>Edit Title</strong></div>
				<div id="editcontent" style="padding:5px;">
					
				</div>
			</div>
			
		    
		</div>
	</div>	
	
	
	<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){  
				try{
					dd = new YAHOO.util.DD("showedit", 'default', {});
					dd.addInvalidHandleType('input');
				}catch(e){}
			});
    </script>
	
	
	
	
	
	
	
	
	