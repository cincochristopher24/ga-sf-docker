<?php
/*
 * Created on 01 12, 07
 *
 * @author Daphne 
 * Purpose:  template for Group Messages Panel (Bulletin, Alerts, News)
 */
 
 
 
 
?>
<?if (0 < count($messages)) : ?>
 		<ul class="bulletin_list">
 		<?foreach($messages as $message) :?>
			<?if ('BulletinMessage' == get_class($message)) : ?>		 
				<li<?php if(1==$messType): ?> class="bulletin"<?endif;?>>
				 <div class="image_container"></div>
				 <div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
		 			<?php $recipientGroup = $message->getMessageSource()->getRecipientGroup(); ?>
					<p class="meta">
						<?=date('M j, Y',strtotime($message->getCreated()))?>
						<?php if ($recipientGroup instanceof Group) : ?>
							<? if ($recipientGroup->getGroupID() != $grpID): ?>
								<br /><strong><?= $recipientGroup->getName(); ?></strong>
							<?php endif; ?>
						<? endif; ?>
					</p>
					<a class="title" title="View Post" href="/bulletinmanagement.php?hlEntryID=<?=$message->getAttributeID()?>&amp;groupID=<?= $grpID ?>"><?=$message->getTitle()?></a><br /> 
		 			<p class="details">
		 				<a class="username" href="<?=$message->getMessageSource()->getFriendlyUrl() ?>" ><?=$message->getMessageSource()->getName() ?></a>
						<?php 
							if(!is_null($recipientGroup) && !$message->getMessageSource()->postedAsAdministrator()):
						?>			 			
			 			 |
			 			 <a href="<?=$message->getMessageSource()->getRecipientGroup()->getFriendlyUrl()?>">
			 			 	<?=$message->getMessageSource()->getRecipientGroup()->getName() ?>
			 			 </a>
			 			<?php endif; ?>
		 			</p>
				</div>
				</li>
			 
			<?endif; ?>	 	
			 
			 <?if ('PersonalMessage' == get_class($message)) :?>
			 	<!--<li class="message">-->
				<li<?php if(1==$messType): ?> class="messages"<?endif;?>>
				<div class="image_container"></div> 
				<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
			 		<p class="meta"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
			 		<a class="title" href="/messages.php?view&amp;active=inbox&amp;messageID=<?=$message->getAttributeID()?>"><?=$message->getTitle()?>-[Personal Message]</a>
			 		<p class="details">
		 				<a class="username" href="/<?=$message->getSource()->getUsername() ?>" ><?=$message->getSource()->getUsername() ?></a>
		 			</p>
			 	 </div>
			 	</li>
			 <?endif;?>
			 
			 <?if ('InquiryMessage' == get_class($message)) :?>
			 	<!--<li class="inquiry">-->
				<li<?php if(1==$messType): ?> class="inquiry"<?endif;?>>
				<div class="image_container"></div> 
				<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
			 		<p class="meta"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
			 		<a class="title" href="/messages.php?view&amp;active=inquiry&amp;messageID=<?=$message->getAttributeID()?>"><?=$message->getTitle()?>-[Inquiry]</a>
			 		<p class="details">
		 				<a class="username" href="/<?=$message->getSource()->getUsername() ?>" ><?=$message->getSource()->getUsername() ?></a>
		 			</p>
				 </div>
			 	</li>			 	
			 <?endif;?>
			 
			 <? // added by daf 12.22.06 ?>
			 <?if ('News' == get_class($message)) :?>
			 	<li<?php if(1==$messType): ?> class="news"<?endif;?>>
				<div class="image_container"></div> 
				<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
			 		<p class="meta"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
			 		<a class="title" href="/newsmanagement.php?groupID=<?= $grpID ?>&amp;hlEntryID=<?= $message->getNewsID() ?>"><?= $message->getTitle();?></a>			 			
				 </div>
			 	</li>			 	
			 <?endif;?>
			 
			 <? // added by daf 12.22.06 ?>
			 <?if ('AlertMessage' == get_class($message)) :?>
			 	<!--<li class="alert">-->
				 <li<?php if(1==$messType): ?> class="alert"<?endif;?>>
				<div class="image_container"></div> 
				<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
			 		<p class="meta">
						<?=date('M j, Y',strtotime($message->getCreated()))?>
						<?php $msgSource = $message->getMessageSource(); ?>
						<?php $groupRcpt = $msgSource->getRecipientGroup(); ?>
						<?php if($groupRcpt->getGroupID() != $grpID): ?>
							<br /><strong><?= $groupRcpt->getName(); ?></strong>
						<?php endif;?>
					</p>
			 		<p class="alert_text">
						<?= GaString::activateUrlLinks($message->getText()); ?>
					</p>
			 	 </div>
				</li>			 	
			 <?endif;?>
			 
			 <?
			 	// added by: cheryl ivy q. go  09 august 2007
			 	if ('Comment' == get_class($message)){
					$tp = new TravelerProfile($message->getAuthor());
					$Poke = new PokeType($message->getPokeType());

					$pokesview = new PokesView();
	 				$pokesview->setAuthorId($tp->getTravelerID());		// travelerId or groupId of the greeting owner
	 				$pokesview->setOwnerId($_SESSION['travelerID']);			// travelerId or groupId of profile/photo/journal-entry
	 				$pokesview->setPokeTypeId($Poke->getPokeTypeID());
					$pokesview->setMessage($message->getTheText());

					if (!strcasecmp("TravelLog", get_class($message->getContext()))){
						$pokesview-> setContextId($message->getContext()->getTravelLogID());
						$pokesview-> setContext(3);
					}
	 				elseif (!strcasecmp("Photo", get_class($message->getContext()))){
	 					$pokesview-> setContextId($message->getContext()->getPhotoID());
	 					$pokesview-> setContext(2);
	 				}
	 				else{
	 					$pokesview-> setContextId($message->getContext()->getTravelerID());
	 					$pokesview-> setContext(1);
	 				}

	 				if (0 < strlen($message-> getTheText()))
	 					$pokesview-> setHasMessage(true);
	 				else
	 					$pokesview-> setHasMessage(false);
	 				$pokesview-> setAuthor();
	 				$pokesview-> setOwner();
	 			?>
					<li class = "greetings">
						<div class="image_container"></div>
						<? if ( $messType == MessagesPanel::$ALL ) : ?>
							<div class = "details_container viewall">
								<p class="meta"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
								<div class="details">
									<? if (0 < $message->getPokeType()) : ?>							
										<img src = "<?=$Poke->getPath().$Poke->getPokeImage()?>" height = "22" width = "22" />							
									<? endif; ?>
									<div class="message"><?=$pokesview->getPassportMessage()?></div>
								</div>
							</div>
						<? else : ?>
							<div class = "details_container" style = "background-image: none;">
								<p class="meta"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
								<div class="details">
									<? if (0 < $message->getPokeType()) : ?>							
										<img src = "<?=$Poke->getPath().$Poke->getPokeImage()?>" height = "22" width = "22" />							
									<? endif; ?>
									<div class="message"><?=$pokesview->getPassportMessage()?></div>
								</div>
							</div>
						<? endif; ?>	
					</li>
				<?
			 	}
 			endforeach; ?>
 		</ul>		 	 
<? else : ?>
  <p class="help_text"><span>  	
  	 <? if (MessagesPanel::$ALL ==  $messType){ 		  	   
	  	   echo nl2br($ht->getHelpText('group-messagecenter'));
  	 	} 
  	 	
  	 	if (MessagesPanel::$BULLETIN == $messType) {
  	 	   echo nl2br($ht->getHelpText('group-bulletin'));
  	 	} 
  	 	
  	 	if (GroupMessagePanel::$ALERTS == $messType) {
  	 	   echo nl2br($ht->getHelpText('group-alerts'));
  	 	} 
  	 	
  	 	if (GroupMessagePanel::$NEWS == $messType) {
  	 	   echo nl2br($ht->getHelpText('group-news'));
  	 	}
  	 	
  	 	if (MessagesPanel::$GREET == $messType) {
		   echo 'You have no greeting.';
  	 	}
  	 	  	 	
	  ?>
		</span> 
  </p>
<? endif; ?>