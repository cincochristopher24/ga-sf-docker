<?php
/*
 * Created on 10 11, 2006
 * @author Kerwin Gordo
 * Purpose : template to view a list of all comments on the traveler (profile,photo,travellogs (journal entry))	
 */
  
?>
<? $snav->show();?>
<div class="area mine" id="intro">
	<div class="section">
		<h1>Comment Management</h1>
	</div>
</div>
<div class="area" id="main">

		<div id="area_left">
		
		<div class="section">
     	<h2>Profile Comments</h2>
     	<div class="content">
     <?if (isset($commentCount) && $commentCount > 0) :  ?>
     	 <?
     	 	$pl = '';
     	 	if (1 < $commentCount)
     	 		$pl = 's';
     	 ?>	
		
		<a href="profile.php?action=view&travelerID=<?=$travelerID?>"><?=$commentCount?> comment<?=$pl?> on your profile</a></li>
	
		
	 <? else : ?>
	 	<p class="help_text"> <span>No profile comments</span>.</p>	 
	 <? endif;  ?>	 
 		</div>
 	</div>
 

		<div class="section">
     	<h2>Journal Entry Comments</h2>
     	<div class="content">
   
     <?if (0 < count($jEntries)) :?>
		 <table cellspacing="0">
		 	 <tr><td>Journal Entry</td><td>Comments</td></tr>		 
			 <? foreach($jEntries as $jEntryAr) : ?>						 
			 	<tr><td><a href="travellog.php?action=view&travellogID=<?=$jEntryAr['travellog']->getTravelLogID()?>"><?=$jEntryAr['travellog']->getTitle()?></a></td><td><?=$jEntryAr['count']?></td></tr>
			 <? endforeach; ?>
		 </table>
	 <?else : ?>
	 	 <p class="help_text"> <span>No journal comments yet</span>.</p>	 
	 <?endif; ?>
 	</div>	 
 </div>
 <div class="section" >
     <h2>Photo Comments</h2>
     <div class="content">
     <?if (0 < count($photoComments)) :?>
		 <table cellspacing="0">
		 	 <tr><td>Photo</td><td>Comments</td></tr>
		 	 <? foreach($photoComments as $pComment) :?>
		 	 	<?if ( "" == $pComment['photo']->getCaption()) {
		 	 			$photoTitle = $pComment['photo']->getFilename();		 	 		
		 	 		} else { 
		 	 			$photoTitle = $pComment['photo']->getCaption();
		 	 		} 
		 	 	 ?>		 	 	
		 	 	<tr><td><a href="photomanagement.php?cat=profile&action=vfullsize&genID=<?=$pComment['genID']?>&photoID=<?=$pComment['photo']->getPhotoID()?>"><?=$photoTitle?></a></td><td><?=$pComment['count']?></td></tr>
		 	 <? endforeach;?>		 	 	 
		 </table>
	 <?else : ?>	 
	 	<p class="help_text"> <span>No photo comments yet</span>.</p>	 
	 <?endif;?>	 
 </div>
 </div>
 </div>
 </div>

 <div class="commentbut">
 <a href="dashboard.php" class="button">Back to Passport</a>
 </div>	 
 </div>