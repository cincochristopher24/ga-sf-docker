<?php
/**
 * Created on Apr.16.07
 *
 * @author Daphne 
 * Purpose:  template for View Email Invite List Members Panel (Pending Email Invites)
 */
  
?>


	
<?if (0 < count($allMembers)) : ?>
<table id="email_invites" width="100%" cellspacing="1">
	<tr>
		<th>Email Address</th>
		<th>Name</th>
		<th id="col_invite_actions">Actions</th>
	</tr>
	<? foreach($allMembers as $eachemailadd) : 
		$emailInviteAttribute = $group->getEmailInviteAttribute($eachemailadd);
		$firstName = (count($emailInviteAttribute)) ? $emailInviteAttribute['firstname'] : '';
		$lastName = (count($emailInviteAttribute)) ? $emailInviteAttribute['lastname'] : '';
	?>
	<tr>
		<td><?=$eachemailadd?></td>
		<td><?=$firstName . " " . $lastName ?></td>
		<td>
			<ul class="actions">
				<li><a href="javascript:void(0)" class="button" onclick="manager._setPanelNum(1);manager._inviteEmail('mode=inviteEmail&amp;email=<?=$eachemailadd?>&amp;grpID=<?=$group->getGroupID()?>');">Invite Again</a></li>
				<li><a href="javascript:void(0)" onclick="manager._setPanelNum(1);manager._cancelEmail('mode=cancelEmail&amp;email=<?=$eachemailadd?>&amp;grpID=<?=$group->getGroupID()?>');" class="button">Cancel</a>				
					</li>
				<div class="clear">	</div>
			</ul>	
		</td>
			 	
	 	<? endforeach; ?>
	</td>
<? endif; ?>


