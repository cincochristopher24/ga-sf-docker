<?php
	if($groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
		Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
/*
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs('/js/interactive.form.js');
*/
	Template::includeDependentJs('/min/g=InteractiveJs');
	/**Template::includeDependentJs('/js/scriptaculous/scriptaculous.js');
	Template::includeDependentJs('/js/utils/json.js');
	Template::includeDependentJs('http://www.plaxo.com/css/m/js/util.js');
	Template::includeDependentJs('http://www.plaxo.com/css/m/js/basic.js');
	Template::includeDependentJs('http://www.plaxo.com/css/m/js/abc_launcher.js');
	ob_start();	
$incjs = <<<BOF
<script type="text/javascript">
//<![CDATA[
 	function onABCommComplete(data) {
  		 $('entryList').value = JSON.stringify(data);
  		 $('frmAddressBookList').submit();
	}
 	GAInteractiveForm.create('frmAddressBook');
 	
 	function toggleGrpNotificationPref(){
 		if($('radIsNotable_1').checked && $('grpNotificationPref').style.display == 'none'){
 			new Effect.SlideDown('grpNotificationPref', { 
				duration: .20
			});
 		}
 		else if(!$('radIsNotable_1').checked && $('grpNotificationPref').style.display != 'none'){
 			new Effect.SlideUp('grpNotificationPref', { 
				duration: .20
			})
 		}
 	}
 	
//]]>
</script>
BOF;

Template::includeDependent($incjs);	*/

?>			

<style type="text/css">
	#popup_header strong
	{
		color:#FFFFFF;
	}
</style>

<!-- displays the profile component: added by neri 11-04-08 -->
<?= $profile->render()?>
<!-- end -->
	
<?php  $subNavigation->show(); ?>

<script>
	(function($){
		$.fn.deleteAbEntry = function(addressBookID, grpID) {
			var path = "addressbookmanagement.php?action=delete&eID="+addressBookID;
			if(grpID>0) {
				path = path + "&gID=" + grpID;
			}
			CustomPopup.initialize("Delete Address Book Entry?","Are you sure you want to delete this address book entry?",path,"Delete");
			CustomPopup.createPopup();
		}
	})(jQuery);
</script>

<div class="layout_2" id="content_wrapper">	
	<div id="full_column">
		<div class="section" id="address_book">			
			<h2><span>
				<?php if ($method=="add" || $method=="edit"): ?>
					<?= ucfirst($method) ?> Address Book Entry
				<?php else: ?>
					Address Book
				<?php endif; ?>
			</span></h2>
					
			<div class="content">
					
					<?php if(strlen($notice)): ?>
						<p class="help_text">
							<?= $notice ?>
						</p>
					<?php endif; ?>
					
					<?php if ($method=='view'):	?>
						<?php if(!count($allEntries)): ?>
							<p class="help_text">
								<span>Your address book is currently empty</span>.
								<a href="addressbookmanagement.php?action=add<?= $isModerator?'&gID='.$groupID:'' ?>"> Add Your First Address Book Entry Here.</a>
							</p>													
						<?php else: ?>
							<form name="frmAddressBook" id="frmAddressBook" action="addressbookmanagement.php?action=delete" method="post">
								<table cellpadding="4" cellspacing="0" id="address_book_entries">
									<tr>
										<th id="addr_th_name">Name</th>
										<th id="addr_th_email">Email Address</th>
										<th width="20%">Details</th>
										<?/*<th>Notify</th>
										<th>Status</th>
										*/?>
										<th width="10%" id="addr_th_actions">Actions</th>
									</tr>
									<?php 
										$iterator->rewind();
										while( $iterator->valid() ): 												
									?>
										<tr>
											<td><strong><?= $iterator->current()->getName() ?></strong></td>
											<td><?= strlen(trim($iterator->current()->getEmailAddress())) ? $iterator->current()->getEmailAddress() : '&nbsp;' ?></td>
											<td><?= strlen(trim($iterator->current()->getDetails())) ? $iterator->current()->getDetails() : '&nbsp;' ?></td>
											<?/*
											<td align="center"><?= $iterator->current()->getIsNotable()?"Yes":"No" ?></td>
											<td align="center">
												<?php if($iterator->current()->IsNotable()): ?>
													<?php if($iterator->current()->isPending()): ?>
														Pending
													<?php elseif($iterator->current()->isAccepted()):?>
														Accepted
													<?php elseif($iterator->current()->isDenied()):?>
														Denied
													<?php else: ?>
														-
													<?php endif; ?>									
												<?php else: ?>
													-
												<?php endif; ?> 
											</td>
											*/?>
											<td align="center">
												<a href="addressbookmanagement.php?action=edit&amp;eID=<?=$iterator->current()->getAddressBookEntryId() ?><?= $isModerator?'&gID='.$groupID:'' ?>">Edit</a> |						
												<!--a href="addressbookmanagement.php?action=delete&amp;eID=<?=$iterator->current()->getAddressBookEntryId() ?><?= $isModerator?'&gID='.$groupID:'' ?>" onclick="return confirm('Are you sure you want to delete this address book entry?')">Delete</a-->
												<a href="javascript: void(0)" onclick="jQuery.fn.deleteAbEntry(<?=$iterator->current()->getAddressBookEntryId()?>,<?=$isModerator?$groupID:0 ?>);">Delete</a>																						
											</td>
										</tr>
									<?php 
										$iterator->next(); 
										endwhile;
									?>
								</table>
								
								<?php if(count($allEntries)): ?>					
									<?= $paging->showPagination(array('label' => 'Entries')); ?>
								<?php endif; ?>
							</form>
						<?php endif; ?>						
						
					<?php elseif($method=='showListSummary'): ?>
						<table cellpadding="4" cellspacing="1">
							<tr>
								<th>Name</th>
								<th>Email Address</th>
								<th>Status</th>
							</tr>
							<?php foreach($entryList as $iEntry): ?>
								<tr>
									<td><?= $iEntry['name'] ?></td>
									<td><?= $iEntry['emailAdd'] ?></td>
									<td><?= $iEntry['status'] ?></td>
								</tr>
							<?php endforeach; ?>
						</table>
						<div class="clear"></div>
					<?php elseif ($method=="add" || $method=="edit"): ?>
							<form name="frmAddressBook" action="addressbookmanagement.php?action=save<?= $isModerator?'&gID='.$groupID:'' ?>" method="post" class="interactive_form">
								<ul class="form">
									<li>
										<label for="txtEmail">Email Address:<?php if(isset($errCodes['EMAIL_ADDRESS'])): ?>  <i style="color:red"><?= $errCodes['EMAIL_ADDRESS'] ?></i> <?php endif; ?></label>							
										<input type="text" name="txtEmail" id="txtEmail" value="<?= $addressBookEntry->getEmailAddress()?>" class="text" style="width: 344px;" />												
									</li>
									<li>
										<label for="txtName">Name:<?php if(isset($errCodes['NAME'])): ?>  <i style="color:red"><?= $errCodes['NAME'] ?></i> <?php endif; ?></label>
										<input type="text" name="txtName" id="txtName" value="<?= $addressBookEntry->getName()?>" class="text" style="width:344px" />
									</li>
									<li>						
										<label for="txaDetails">Additional Details:</label>
										<p class="supplement">
											Want to include their phone numbers or address to send postcard<br />
											while you're on the road? Use the Additional Details box for any <br />
											extra information you want to include.
										</p>
										<textarea name="txaDetails" id="txaDetails" style="width: 344px; height: 100px"><?= $addressBookEntry->getDetails()?></textarea>
									</li>
									<?/*
									<li>
										<fieldset class="choices">
											<legend><span>Do you want them to be notified whenever you update your<br />journals or add new photos?</span></legend>
											<ul>
												<li>
													<input type="radio" name="radIsNotable" id="radIsNotable_0" value="0" <?= (!$addressBookEntry->getIsNotable())?"checked":""?> onclick="toggleGrpNotificationPref()" /> <label for="radIsNotable_0">No</label>
												</li>						
												<li>
													<input type="radio" name="radIsNotable" id="radIsNotable_1" value="1" <?= ($addressBookEntry->getIsNotable())?"checked":""?> onclick="toggleGrpNotificationPref()" /> <label for="radIsNotable_1">Yes -</label>
													<em>We will send an email to this person asking them whether <br />they are willing to receive updates about your jounals and photos.</em>
												</li>
											</ul>
										</fieldset>
									</li>
									<li>
										<fieldset id="grpNotificationPref" style="display:<?= ($addressBookEntry->getIsNotable())?'':'none'?>">
											<? if(count($groups)): ?>
											<p><strong>Group Notification Preferences</strong></p>
											<table cellpadding="4" cellspacing="0" style="background-color:transparent;margin:10px">
												<thead>
												<tr>
													<th>Group Name</th>
													<th>&nbsp;</th>
													<th>Notify on Add Journal</th>
													<th>&nbsp;</th>
													<th>Notify on Add Photo</th>
												<tr>
												</thead>
												<tbody>
												<?php 
													foreach($groups as $iGroup):
														$isJournalChecked = isset($prefs) && (array_key_exists($iGroup->getGroupID(),$prefs) && $prefs[$iGroup->getGroupID()]->isNotifiableOnAddJournal());
														$isPhotoChecked =  isset($prefs) && (array_key_exists($iGroup->getGroupID(),$prefs) && $prefs[$iGroup->getGroupID()]->isNotifiableOnAddPhoto());
												?>
												<tr>
													<td><?= $iGroup->getName() ?></td>
													<td>&nbsp;</td>
													<td align="center"><input type="checkbox" name="chkAddJournal[]" value="<?= $iGroup->getGroupID() ?>" <?php if($isJournalChecked):?>checked<?php endif; ?>></td>
													<td>&nbsp;</td>
													<td align="center"><input type="checkbox" name="chkAddPhoto[]" value="<?= $iGroup->getGroupID() ?>" <?php if($isPhotoChecked):?>checked<?php endif; ?>></td>
												</tr>
												<?php endforeach;?>
												</tbody>
											</table>
											<? endif; ?>
										</fieldset>
									</li>
									*/?>
									<li class="actions">
										<input type="submit" name="submit" class="submit" value="Save" /><input type="submit" name="submit" class="submit" value="Cancel" />
										<input type="hidden" name="hidAddressBookEntryId" value="<?=$addressBookEntry->getAddressBookEntryId() ?>" />
									</li>	
								</ul>
							</form>
					<?php endif; ?>
                    
					<?php if ($method=='view' || $method=='showListSummary'): ?>
					
					<ul class="actions">
    					<li><a href="addressbookmanagement.php?action=add<?= $isModerator?'&gID='.$groupID:'' ?>" class="button">+ Add Address Book Entry</a></li>
    					<!--
    					<li>
    						<a href="#" onclick="showPlaxoABChooser('entryList', 'plaxo.php'); return false" class="button">Import Contacts</a>
    						<form name="frmAddressBookList" id="frmAddressBookList" action="addressbookmanagement.php?action=savelist" method="post">
    							<input type="hidden" name="entryList" id="entryList" value='' />
    						</form>
    					</li>
    					-->
    				</ul>
					<?/* comment out help text
					<br /><br />
					<p class="help_text">
						<?= $helpText->getHelpText('ADDRESSBOOK'); ?>				
					</p>
					*/?>
					<?php endif; ?>
                    
			</div>
		</div>
	</div>
</div>