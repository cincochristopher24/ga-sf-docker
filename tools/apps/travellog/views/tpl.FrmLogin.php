<? if ( strlen($contents["msg"]) ): ?>
		<p class="error_notice">
			<?= $contents["msg"]; ?>
		</p>
<? endif; ?>

<? $show_login_page = ( isset($contents["login_page"]) )	?	$contents["login_page"]:	FALSE;?>

<?php if($show_login_page): ?>
	<form id="frmLogin" action="<?= $contents['action']; ?>" method="post" class="interactive_form">
		<ul class="form<?php if($contents["isRegister"]): ?> highlight<?php endif;?>" id="main_login_form">
			<li class="element" id="loginHeader">
				<h2 class="head">Already a member?</h2>
			</li>	
			<li class="element">
				<label for="txtUsrName" class="login_control">Email:</label>
				<input type="text" name="txtEmail" id="txtEmailLogin" onfocus="CustomPopup.fetchEmailBubble(<?=$show_login_page?>);" onblur="CustomPopup.removeBubble();" value="" size="15" class="text" />
			</li>
			<li class="element">
				<label for="pwdPasWrd" class="login_control">Password:</label>
				<input name="pwdPasWrd" id="pwdPasWrdLogin" type="password" size="15" class="text"  />
			</li>
			<li class="buttons standard rounded">
					<!--button class="submit" id="loginBtn" name="btnLoginSubmit" type="submit"><span>Login</span></button-->
					<button name="btnLoginSubmit" id="loginBtn2" type="submit">Sign In</button>
					<input name="login" value="Log In" type="hidden" />
					<input type="hidden" name="hdnLoginType" value="<?= $contents['loginType']; ?>" />
					<input type="hidden" name="hdnReferer" value="<?= $contents['referer']; ?>" />
			</li>
			<p id="loginFormErrorMessage" class="errorMessage"></p>
			<li id="loginSocialMediaContainer">
				<p>
					Or you may use your <strong>Twitter</strong>, <strong>Google</strong> or <strong>Facebook</strong> account to login.
				</p>
				<ul class="socialMediaBar">
					<li>
						<a class="socialMediaIcon32" id="twitterIcon32" href="/splauncher.php?action=login&context=twitter" title="Login using your Twitter account">twitter</a>
					</li>
				
					<li>
						<a class="socialMediaIcon32" id="googleIcon32" href="/splauncher.php?action=login&context=google" title="Login using your google account">google</a>
					</li>
					
					<li>
						<a class="socialMediaIcon32" id="facebookIcon32" href="/splauncher.php?action=login&context=facebook" title="Login using your Facebook account">facebook</a>
					</li>
				</ul>
			</li>
	
			<li class="others"> 
				<span><a href="/forgot-password.php" title="Unable to login to your travel blog? Request to change your password.">Forgot your password?</a>	
				<br /></span>
				<? if ( !$contents["isRegister"] && $contents['displayRegisterLink']): ?>
					<span>Not yet a member? &nbsp; <a href="/register.php" class="more">Register now!</a></span>
				<? endif; ?>			
			</li>
		</ul>
	</form>

<?php else: ?>
	<ul class="form<?php if($contents["isRegister"]): ?> highlight<?php endif;?>" id="main_login_form">
		<li class="element">
			<?php if ('isvolunteers.goabroad.net' == $_SERVER['HTTP_HOST']): ?>
				<a id="loginAnchor" style="font-size: 16px !important;" href="#TB_inline?dummy=1&width=400&height=auto&inlineId=signinPopupContent" class="thickbox">Login to your GoAbroad.net account &raquo;</a>	
			<?php else: ?>
				<a id="loginAnchor" href="#TB_inline?dummy=1&width=400&height=auto&inlineId=signinPopupContent" class="thickbox">Login to your account &raquo;</a>	
			<?php endif; ?>
		</li>
	</ul>
<?php endif; ?>


<?php if(!$show_login_page): ?>
	<div id="signinPopupContent" style="display:none;">
		<a class="modalClose" href="" onclick="jQuery('#txtEmailLogin').val(''); jQuery('#pwdPasWrdLogin').val(''); jQuery('#loginFormErrorMessage').attr('style','visibility: hidden;'); self.parent.tb_remove(this); return false;">X</a>
		<div class="modalBody line" id="signInWrapper">
			
			<?php require_once("travellog/model/Class.GroupPeer.php"); ?>
			<?php if( !isset($GLOBALS["CONFIG"]) || (isset($GLOBALS["CONFIG"]) && GroupPeer::directSignupAllowed($GLOBALS["CONFIG"]->getGroupID())) ): ?>
				<div id="top">
					<a href="/register.php"><strong>Register Now</strong> (It's Free!)</a>
				</div>
			<?php endif; ?>
			
			<div id="mid">
				<h2>Sign In</h2>
				<form id="frmLogin" action="<?= $contents['action']; ?>" method="post" class="socialMediaForm">
					<ul>
						<li>
							<label>Email</label>
							<input type="text" name="txtEmail" id="txtEmailLogin" />						
						</li>
					
						<li>
							<label>Password</label>
							<input name="pwdPasWrd" id="pwdPasWrdLogin" type="password" size="15" />				
						</li>
			
						<li class="buttons standard rounded">
							<button name="btnLoginSubmit" id="loginBtn" type="submit">Sign In</button>
							<p id="loginFormErrorMessage" class="errorMessage" style="visibility: hidden;"></p>
							<input name="login" value="Log In" type="hidden" />
							<input type="hidden" name="hdnLoginType" value="<?= $contents['loginType']; ?>" />
							<input type="hidden" name="hdnReferer" value="<?= $contents['referer']; ?>" />
							<input type="hidden" name="btnLoginSubmit" />
						</li>
						<li>
							<a id="modalforgotpwd" href="/forgot-password.php" title="Unable to login to your travel blog? Request to change your password.">Forgot your password?</a>		
						</li>
					</ul>
				</form>

			</div>
		
			<div id="bottom">
				<p><strong>Or you may use your social media account:</strong></p>
				<ul>
					<!--li>facebook</li-->
					<li id="signInTwitter"><a id="twitterIcon32" href="/splauncher.php?action=login&context=twitter" title="Login using your Twitter account"><span class="logoContainer">&nbsp;</span><strong>&nbsp;twitter</strong></a></li>
					<li id="signInGoogle"><a id="googleIcon32" href="/splauncher.php?action=login&context=google" title="Login using your google account"><span class="logoContainer">&nbsp;</span><strong>&nbsp;google</strong></a></li>
					<li id="signInFacebook"><a id="facebookIcon32" href="/splauncher.php?action=login&context=facebook" title="Login using your google account"><span class="logoContainer">&nbsp;</span><strong>&nbsp;facebook</strong></a></li>
					<!--fb:login-button></fb:login-button--> 
				</ul>
			</div>
			
		</div>
	</div>
<?php endif; ?>
<script type="text/javascript">

	jQuery(document).ready(function(){
		jQuery("#txtEmailLogin").focus(function(){
			jQuery("#loginFormErrorMessage").attr("style","visibility: hidden;");
		});
		jQuery("#pwdPasWrdLogin").focus(function(){
			jQuery("#loginFormErrorMessage").attr("style","visibility: hidden;");
		});
		jQuery("#frmLogin").submit(function(){
			if( jQuery("#loginBtn").hasClass("disabled") ){
				return false;
			}
			var pass = jQuery("#pwdPasWrdLogin").val();
			var email = jQuery("#txtEmailLogin").val().trim();
			if( "" == pass && "" == email ){
				jQuery("#loginFormErrorMessage").html("Missing email and password.");
				jQuery("#loginFormErrorMessage").attr("style","visibility: normal;");
				return false;
			}else if( "" == pass ){
				var message = email.match(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/) == null ? "Invalid email format and missing password." : "Missing password.";
				jQuery("#loginFormErrorMessage").html(message);
				jQuery("#loginFormErrorMessage").attr("style","visibility: normal;");
				return false;
			}else if( "" == email ){
				jQuery("#loginFormErrorMessage").html("Missing email.");
				jQuery("#loginFormErrorMessage").attr("style","visibility: normal;");
				return false;
			}
			if( email.match(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/) == null ){
				jQuery("#loginFormErrorMessage").html("Invalid email format.");
				jQuery("#loginFormErrorMessage").attr("style","visibility: normal;");
				return false;
			}
			jQuery("#loginFormErrorMessage").attr("style","visibility: hidden;");
			try{
				jQuery("#loginBtn").addClass("disabled");
			}catch(e){
			}
			try{
				jQuery("#loginBtn2").addClass("disabled");
			}catch(e){
			}
			return true;
		});
	});
</script>
