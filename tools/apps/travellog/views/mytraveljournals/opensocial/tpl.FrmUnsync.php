<div id="frmSync">	
	<h1 class="header">Synchronize your Journals</h1>
	<span class="sub_header"> Update your journals with ease.</span>
	
	<p class="info"> You are currently using <?=$args['ownerName']?>. </p>
	
	<p style="padding-bottom: 10px;">
		This App is currently synchronized with <a href="http://www.goabroad.net/<?=urlencode($args['ownerName'])?>" target="_blank"><?=$args['ownerName']?>'s</a> GoAbroad Network account.<br/>
		
		<strong>GoAbroad Network Account Email used by this App:</strong> <?=$args['dotNetEmail']?>
	</p>	
		
	<p style="padding-bottom: 10px;"> If you wish to synchronize a different GoAbroad Network account, click <strong>Unsynchronize</strong> then sync again. </p>
	<input type="button" value="Unsynchronize" onclick="showLoading();myTravelJournalsController.unsynchronize()" />	 			
	
</div>
