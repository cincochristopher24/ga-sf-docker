<?php
	/*
	 * Created on Jul 11, 2008
	 */
	 $dateTimeTool = new GaDateTime();
?>

<p><?= ToolMan::convertTitleToFriendlyURLFormat($args['entry']->getTitle()); ?></p>
<div class="mainEntry">
	<span>
    	<a href="<?= ToolMan::convertTitleToFriendlyURLFormat($args['entry']->getTitle()); ?>" target="_blank" id="title<?= $args['entry']->getTravelLogID(); ?>" > 
			<?= $args['entry']->getTitle(); ?> 
		</a>
	</span>	
	<span>
		<span class="entry_detail" id="details<?= $entry->getTravelLogID(); ?>">
			<?= $dateTimeTool->friendlyFormat($args['entry']->getTrip()->getArrival()); ?> | 
			<?= implode(',', array_unique(array($args['entry']->getTrip()->getLocation()->getName(),$args['entry']->getTrip()->getLocation()->getCountry()->getName()))); ?>
    	</span>
	</span>	
	<div>
		<p><?= ToolMan::truncateText($args['entry']->getDescription(),230); ?><p>
		<img src="<?= $args['entry']->getPrimaryPhoto(); ?>" />
		<p><a href="<?= ToolMan::convertTitleToFriendlyURLFormat($entry->getTitle()); ?>" target="_blank" >View Full Entry</a></p>
	</div>	
</div>
