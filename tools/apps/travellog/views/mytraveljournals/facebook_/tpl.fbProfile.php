<?php
if( ($isSync == true && $numDisplayed < 1) || $isSync == false){		
	$message = "<font color='red'>No journals to be displayed.</font><br/><br/> <font color='#444444'>$subMessage</font><br/>";
	$fbml = "<fb:wide>$message</fb:wide><fb:narrow>$message</fb:narrow>";
}
else{   

$fbml = <<< userisowner
	<div style="text-align:right;padding-bottom:5px; border-bottom:1px solid #efefef">
		<fb:fbml version="1.1">
			<fb:visible-to-owner>
				<fb:wide> <a href="http://apps.facebook.com/mytraveljournals/refresh.php?action=refresh&box=1">Refresh</a> | </fb:wide>
				<fb:narrow> <a href="http://apps.facebook.com/mytraveljournals/refresh.php?action=refresh">Refresh</a> | </fb:narrow>
            </fb:visible-to-owner>
		</fb:fbml>
		<a href="http://apps.facebook.com/mytraveljournals/index.php?id=$facebookID">View All</a>
	</div>

userisowner;
//<fb:wide> <fb:subtitle> Displaying $numDisplayed journal(s); </fb:subtitle>  </fb:wide>

$fbml .= <<< stylesheet
<style>

	/*   Begin Reset   */

	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, font, img, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	b, u, i, center,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td {
		margin: 0;
		padding: 0;
		border: 0;
		outline: 0;
		font-size: 100%;
		vertical-align: baseline;
		background: transparent;
	}
	
	body {
		line-height: 1;
	}
	ol, ul {
		list-style: none;
	}


	:focus {
		outline: 0;
	}
	
	ins {
		text-decoration: none;
	}
	del {
		text-decoration: line-through;
	}
	
	table {
		border-collapse: collapse;
		border-spacing: 0;
	}
	
	
	/* End Reset  */
	
	
	
	/* Begin Custom Rules */


	a:hover {
	    text-decoration:underline;
	}
	
	ul{
		list-style-type: none;
		margin: 0px auto;
		padding: 0px;
	}
	
	ul li{
		display:block;
	}
	a{
		text-decoration: none;
	}
	.clear{
		clear:both;
	}
	
	.wrapper{
		padding: 0 0;
		color: #474747;
		line-height: 1.43em;
		margin-top: 10px;
	}
	
	.journal_box{
		margin: 0px auto;
		padding:0px;				
	}
	

	ul.journal_list>li{
		border-bottom: 1px solid #D8DFEA;
	
	}
	
	ul.journal_list li.no_bborder{
		border:1px solid #fff;
	}
	
	.jtitle{
		font-size:18px;
		font-weight:bold;
		margin:0;
		outline-style:none;
		outline-width:medium;
	}
	
	.jcontent{
		margin: 0;
	}
	
	
	.header_box h1{
		font-size: 16px;
		padding: 0px;
		text-align: left;
	}
		div.header_box{		
			margin: 0px;
			height: auto;
			width: auto;	
		}
			
		div.entry_scroll_box{
		height: 29px;
		margin: 0px;
		float: right;
	}	

	div.entry_scroll_box a.scroll_left{
		margin: 5px 0px 0px 4px;
		display: block;	
		float: left;
	}
	
	div.entry_scroll_box a.scroll_right{	
		margin: 5px 4px 0px 0px;
		display: block;
		float: right;
	}
	
	div.entry_scroll_box span{
		padding: 0px;
		font-weight: bold;
		float: left;
		margin: 7px 4px 0px;
		font-size:11px;
		color:#3F5764;
	}		
	
	.entry_title{
		display:block;
		padding:0.42em 0 0;
		text-align:left;
	}
	
	.entry_title a{
		color:#3B5998;
		font-size:13px;
	}
		
	
	.entry_detail{
		color:#9F9F9F;
		display:block;
		font-size:11px;
		margin-bottom:15px;
		text-align:left;
	}
	
	.jphoto{
		border:1px solid #CDCCD1; 		
		float:left; 
		/* margin:5px 0 14px 5px; */
		margin-right: 12px;
		padding:3px;  
	}
	.jcontent p{
		font-size:11.5px; /* canvass is 12px */
		line-height: 1.5em;
		color: #474747; 
		margin: 7px 0 5px 0; 
		text-align: left;
	}
	
	
	.jphotothumb_box{
		width: 83px;
		height: 225px;
		background-color:#dedede;
		padding: 0px 0 0 0;
		margin: 0px 0 0 0px;
		
		border-left:1px solid #AFBDC7;
	}
	
	.jphotothumb_box ul li{
		padding: 15px 0 0 7px;
	}
	
	.jphotothumb_box img{
		display:block;
		width: 65px;
		height: 65px;
		background-color: #fff;
		border: none;
		margin: 0;
	}
	
	a.fullEntry_link{
		display: block;
		text-align: left;
	}

	 /* advisor box */
	
	.advisorbox{
		margin:1em 0 0;
		text-align:left;
	}
	
	
	.advisorbox img{
		display:block;
		width:37px;
		height:37px;
		border:1px solid #BFD1E1;
		margin-right:5px;
		float: left;
	}
	
	.advisorbox a{
		display:block;
		font-size:11px;
		margin-left:0;
		padding-left:42px;
		font-weight: bold;
	}
	
	.advisorbox .advDetails{
		color:#666;
		display:block;
		font-size:10px;
		line-height:1.5em; 
		font-weight: normal;		
	}
	
	#journals_list{

	}
	
	#journals_list .journal{
		padding: 0px 0px 15px;
	}               

	li.with_bborder{
		border-bottom:1px solid #D8DFEA;
		margin-bottom:15px;
	}

	.journals_list li.no_bborder{
		border-bottom:1px solid #fff;
	}

	
	 /* for garegister item */
	.garegister span{
		padding:10px 5px;
		display:block;
	}

	#footbox{
		border-bottom:none;
		height:50px;
		margin-left:-8px;
		margin-top:20px;
		height:46px;
		padding-right:8px;
		background: #F7F9FA url(http://www.goabroad.net/facebook/mytraveljournals/images/gradient.png) repeat-x left top;
	}

	.garegister {
		border:1px solid #AFBDC7;
		background: transparent url(http://www.goabroad.net/facebook/mytraveljournals/images/footlogo-ganet.gif) no-repeat scroll 98% 10%;		
		border-bottom:none;
		height:46px;
	}

	.garegister a{
		text-decoration:none;
		font-size:13px;
		font-weight:bold;
		color:#1C71C9;
	}                    
	
   .scroll_entries{
		clear:right;
		margin:1em 0 1.5em;
		text-align:right;
   }
	
   .scroll_entries a span{	
	   	color: #3B5998;   
   }
   
   .scroll_entries a.buttonDisabled span{
		color: #999;
		cursor: default;      
   }
   
   /* MTJ 2009 Jan Refresh */

	.journal_description{
		margin-top: 6px;
	}
	
	.entry_box{
		margin: 6px 0;
		overflow: auto;
	}
		

</style>
stylesheet;

$fbml .= <<< profileBoxTop
<fb:wide>
	<div id='wrapper' class="wrapper">
		<ul id="journals_list">
profileBoxTop;
	$i=0;
	foreach( $journals as $journal ):
		$entries = $journal->getEntries();
		$entryCount = count($entries) > 1 ? count($entries).' entries' : count($entries).' entry';
		$liClass = $i == count($journals)-1 ? 'journal no_bborder' : 'journal with_bborder';
		
		$friendlyJournalURL = fbMytravelJournalsController::getLink($entries[0]);
		$totalEntries = count($journal->getEntries());
		$journalTitle = $journal->getTitle();
		$lastUpdate = $dateTimeTool->friendlyFormat($journal->getLastUpdated());

$fbml .= <<< topOfBox

	<li class="$liClass">
		<div class="journal_box">
	                <!-- begin journal title -->
        			<h1> 
        				<a href="$friendlyJournalURL" id="journal$i" target="_blank" class="jtitle" alt="text" title=""> 
       				 		$journalTitle
        				 </a>
        			</h1>
					<!-- end journal title -->
					
					<!-- begin journal details -->
        			<div class="journal_details">
  							This journal has $entryCount, Last Entry Created on $lastUpdate
        			</div>	
        			<!-- end journal details -->			
        			
        			<!-- begin journal description -->
        			<div class="journal_description">
						{$journal->getDescription()}
        			</div>
        			<!-- end journal description -->
        			
					<div class="clear"> </div>
topOfBox;

					foreach( $entries as $entry ):					

						$primaryPhoto = $entry->getPrimaryPhoto()->getPhotoLink('standard');
						$FriendlyURL = fbMytravelJournalsController::getLink($entry); 
						list($width, $height) = fbMytravelJournalsController::getNewImageSize($primaryPhoto);
						
						$entryTitle = ToolMan::truncateText($entry->getTitle(),30);
						$entryDetails = $dateTimeTool->friendlyFormat($entry->getTrip()->getArrival()) . " | " .implode(', ', array_unique(array($entry->getTrip()->getLocation()->getName(),$entry->getTrip()->getLocation()->getCountry()->getName())));
						$photoCount = $entry->countPhotos() > 1 ? $entry->countPhotos().' Photos' : $entry->countPhotos().' Photo';

// begin jcontent
$fbml .= <<< journalEntries
	<div class="entry_box">
		<div class="jcontent" id="divContent" style="display:block">	

			<div class="photobox">
				<a href="$FriendlyURL" target="_blank">
					<!-- images should be resized to (WxH) 90px X 60px for landscapes, 60px X 90px for portraits -->
					<img id="primaryPhoto$i" class="jphoto" src="$primaryPhoto" width="$width" height ="$height"/>
				</a>									
			</div>	
	
			<div class="entry_title">
				<a href="$FriendlyURL" target="_blank"> 
					$entryTitle
				</a>
			</div>

			<div class="entry_detail">
				$entryDetails
				<div>$photoCount</div> 
			</div> 
		</div>
	</div>
journalEntries;
//end jcontent
endforeach;  // end of inner foreach
//end Entry Box

if( $tmpAdminGroup = $journal->getAdvisorGroup() ): 
$groupLogo = $tmpAdminGroup->getGroupPhoto()->getPhotoLink('thumbnail');
$groupHref = $tmpAdminGroup->getServerName();
$groupName = $tmpAdminGroup->getName();
$fbml .= <<< advisorBox
	<div class="advisorbox" style="display:none;">			

		<img alt="Advisor Logo" src="$groupLogo"/>

		<a href="$groupHref">
			$groupName
			<span class="advDetails">Advisor Group Journal</span>
		</a>

       <div class="clear"> </div>

	</div>
advisorBox;
endif;						

$fbml .= '</li>';
					
$i++;
endforeach;  // end of outer foreach

$fbml .= <<< closingDivUl
</ul>
</div>
</fb:wide>
closingDivUl;
///////////// START of narrow column
$fbml .= <<< narrowStart
<fb:narrow>
	<ul>
narrowStart;
if($showJournalType == 1){
	foreach ($journals as $journal) {
		$journalTitle = strlen($journal->getTitle()) > 70 ? ToolMan::truncateText($journal->getTitle(),70) : $journal->getTitle();
		$entries = $journal->getEntries();
		$journalDetails = $dateTimeTool->friendlyFormat($entries[0]->getTrip()->getArrival()) . " | " .implode(', ', array_unique(array($entries[0]->getTrip()->getLocation()->getName(),$entries[0]->getTrip()->getLocation()->getCountry()->getName())));
		$primaryPhoto = $entries[0]->getPrimaryPhoto()->getPhotoLink('thumbnail');
		$FriendlyURL = fbMytravelJournalsController::getLink($entries[0]);

		$journalImg = $primaryPhoto != 'http://images.goabroad.net/images/default_images/65_photo.gif' ? "<a href='$FriendlyURL'><img src='$primaryPhoto' style='float:left;margin-right:5px;width:65px' target='_blank'/></a>" : '';
		$fbml .= <<< narrowContent
              <li style="padding:5px 0;border-top:1px solid #ddd;overflow:hidden;">
				$journalImg
                <a href ="$FriendlyURL" target="_blank"><strong>$journalTitle</strong></a> <br/>
                <span style="font-size:9px;color:#808080">$journalDetails</span>
              </li>
narrowContent;
	}
}
else{
	if(is_object($journals[0])) :		
		$i = 0; 
		foreach ($journals[0]->getEntries() as $entry) :
		
			$entryTitle = strlen($entry->getTitle()) > 70 ? ToolMan::truncateText($entry->getTitle(),70) : $entry->getTitle();
			$entryDetails = $dateTimeTool->friendlyFormat($entry->getTrip()->getArrival()) . " | " .implode(', ', array_unique(array($entry->getTrip()->getLocation()->getName(),$entry->getTrip()->getLocation()->getCountry()->getName())));
			$primaryPhoto = $entry->getPrimaryPhoto()->getPhotoLink('thumbnail');
			$FriendlyURL = fbMytravelJournalsController::getLink($entry); 

			$journalImg = $primaryPhoto != 'http://images.goabroad.net/images/default_images/65_photo.gif' ? "<a href='$FriendlyURL'><img src='$primaryPhoto' style='float:left;margin-right:5px;width:65px' target='_blank'/></a>" : '';


			$fbml .= <<< narrowContent
	              <li style="padding:5px 0;border-top:1px solid #ddd;overflow:hidden;">
					$journalImg
	                <a href ="$FriendlyURL" target="_blank"><strong>$entryTitle</strong></a> <br/>
	                <span style="font-size:9px;color:#808080">$entryDetails </span>
	              </li>
narrowContent;
			$i++; if($i == 2){break;}
		endforeach;
	endif;
}
	
	
$fbml .= <<< narrowEnd
    </ul>   
</fb:narrow>
narrowEnd;
////// END of narrow column

// SET Profile static views
$facebook->api_client->profile_setFBML(NULL, $facebookID, $fbml, NULL, NULL, $fbml);
} ?>