<style> 
<?php  echo htmlentities(file_get_contents('http://www.goabroad.net/facebook/mytraveljournals/css/profile.css', true)); ?>
</style>
<?php
$dateTimeTool = new GaDateTime();
?>
<div id="inner_wrapper">
<div id='wrapper' class="wrapper">
<?php if(is_array($args['journals']) || is_object($args['journals']))  { ?>
	<ul id="journals_list">
		<? $i=0;

		foreach( $args['journals'] as $journal ):
			$entries = $journal->getEntries();
			$liClass = $i == count($args['journals'])-1 ? 'journal no_bborder' : 'journal with_bborder';
			$totalEntries = count($entries);
		?>
		<li class="<?=$liClass?>">
    		<div class="journal_box">
                    <!-- JOURNAL TITLE -->   
 					<div><?//php echo 'Profile OwnerID:'.$_POST['fb_sig_profile_user']; ?></div> 
        			<h1> 
        				<a href="<?=fbMytravelJournalsController::getLink($entries[0]);?>" id="journal<?=$i?>" target="_blank" class="jtitle" alt="text" title=""> 
       				 		<?=$journal->getTitle(); ?>
        				 </a>
        			</h1>						            
					<!-- END JOURNAL TITLE -->
					<div class="journal_details"> 
						This journal has <?php echo $totalEntries; echo $totalEntries > 1 ? ' entries' : ' entry'; ?>, Last Entry Created <?php echo date('Y-m-d', strtotime($journal->getLastUpdated())) == date('Y-m-d') ? $dateTimeTool->friendlyFormat($journal->getLastUpdated()) : 'on '.$dateTimeTool->friendlyFormat($journal->getLastUpdated()) ?>
					</div>

					<div class="journal_description"> 
   				 		<?=$journal->getDescription(); ?>
					</div>
					<? $ii = 0;
					foreach( $entries as $entry ):
						$primaryPhoto = $entry->getPrimaryPhoto()->getPhotoLink('standard');
						$FriendlyURL = fbMytravelJournalsController::getLink($entry);
						list($width, $height) = fbMytravelJournalsController::getNewImageSize($primaryPhoto);
					?>
					
					<div class="entry_box">
	                	<div class="jcontent" id="divContent<?=$ii?>" style="display:block">
			
							<!-- Begin Entry Thumb -->
							<div class="photobox">
								<a href="<?=$FriendlyURL ?>" target="_blank" id="photoBox<?=$ii?>" >									
									<!-- images should be resized to (WxH) 90px X 60px for landscapes, 60px X 90px for portraits -->
									<img id="primaryPhoto<?=$i?>" class="jphoto" src="<?=$primaryPhoto?>" width="<?=$width?>" height="<?=$height?>"/>
									
								</a>														
							</div>
							<!-- End Entry Thumb -->
							
							<!-- Begin Entry Title -->
	                		<div class="entry_title">
	                    		<a href="<?=$FriendlyURL?>" target="_blank" id="title<?=$i?>" > 
									<?=$entry->getTitle() ?> 
								</a>
	                    	</div>
	                    	<!-- End Entry Title -->
	
							<!-- Begin Entry Details-->
							<div class="entry_detail" id="details<?=$ii?>">
								<?= $dateTimeTool->friendlyFormat($entry->getTrip()->getArrival()); ?> | 
								<?= implode(', ', array_unique(array($entry->getTrip()->getLocation()->getName(),$entry->getTrip()->getLocation()->getCountry()->getName()))); ?>
								<div> <?php echo $entry->countPhotos(); echo $entry->countPhotos() > 1 ? ' Photos' : ' Photo' ?></div>
							</div>					
							<!-- End Entry Details -->																					          							
	                	</div>
	                	<!-- end jcontent--> 
                	</div>					
				    <? $ii++; endforeach; ?>
				
					<? if( $tmpAdminGroup = $journal->getAdvisorGroup() ): ?>

						<div class="advisorbox" style="display: none;">			

							<img alt="Advisor Logo" src="<?= $tmpAdminGroup->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" />

							<a href="<?= $tmpAdminGroup->getServerName(); ?>">
								<?= $tmpAdminGroup->getName(); ?>
								<span class="advDetails">Advisor Group Journal</span>
							</a>

					       <div class="clear"> </div>			

						</div>

					<? endif; ?>			 
					            	
            	<div class="clear"></div>
        	</div>
    	</li>

			<?php
				$i++;
				endforeach; 
			?>
</ul>

<? } else { ?>

	<p>You have not created any journal yet. Start sharing your travels!</p>
	Go to <a href="http://www.goabroad.net" target="_blank">http://www.goabroad.net</a> and create a journal.

<? }?>
</div>

<?php require_once('footer.php');?>