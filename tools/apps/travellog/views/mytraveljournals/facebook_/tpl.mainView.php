<style> 
<?php echo htmlentities(file_get_contents('http://www.goabroad.net/facebook/mytraveljournals/css/mainView.css', true)); ?> 
</style>
<div id="appContainer" class="appContainer">
<div class="dashboard_wrapper">

		<!--begin dashboard header-->	
		<div class="dashboard_header">		
			<h1> My Travel Journals </h1>                     
		
		<!--begin tabs -->		
<?php	$selectedDash = $MainDashboard->getSelected();

		if(isset($isTabView)) :
			if($isTabView) : ?>
				<fb:fbml version="1.1">
				<fb:visible-to-owner> 
					<?php getTabs($selectedDash); ?>
				</fb:visible-to-owner> 
				</fb:fbml>					   				
<?php 		else: 		 	
				if($args['viewerIsOwner']) : getTabs($selectedDash); else : getOwnerName($args['userID']); endif;
			endif;
		else : 
		 	if($args['viewerIsOwner']) : getTabs($selectedDash); else : getOwnerName($args['userID']); endif; 
		endif; ?>			

		<!--end tabs -->		    

		</div> 
		<!--end dashboard header-->			

		<div class="feature" style="display:none;">
			
			<p class="">
				Bring your <strong>GoAbroad Network</strong> travel journals to <strong>Facebook</strong>! <br /> 
				Post your journals, photos and let your friends and family know 
				about your experience!
			</p>
			
			<p class="">
				GoAbroad Network is a social networking website for travelers all over 
				the world. It is an online travel community where members can share 
				travel journals and photos with families and friends, while connecting
				with a growing network of travelers.			
			</p>
			
			<p class="">
				Not yet travel-blogging? <br />				
				Get a free traveler profile and cool travel blog on 
				<a href="http:www.goabroad.net/register.php" target="_blank">www.GoAbroad.net!</a>
			</p>
		
		</div>
		
		
</div> <!-- main enclosing tag-->

<?php function getTabs($selectedDash) { ?>
	<div class="tabs clearfix">
		<div class="center">
			<div class="left_tabs">
				<ul id="toggle_tabs_unused" class="toggle_tabs clearfix">
						<li class="first"> <!--My Travel Journal -->
							<a id="dashboard" title="My Travel Journals"  <?php if(1==$selectedDash) {?> class="selected" <? } else{ ?> class="x" href="http://apps.facebook.com/mytraveljournals/index.php" <? }?>>
								<span>Dashboard </span>
							</a>
						</li>
					
						<li class=""> <!--Invite Friends -->
							<a id="inviteFriends" title="Share this App with your Friends"  <?if(2==$selectedDash) {?> class="selected" <? } else{?> class="x" href="http://apps.facebook.com/mytraveljournals/inviteFriends.php?action=invitefriends" <? }?>>
								<span>Invite Friends </span>									
							</a>
						</li>
						
						<li class="_"> <!--Synchronize -->
							<a id="synchronize" title="Update your journals with ease." <?if(3==$selectedDash) {?> class="selected" <? } else{?> class="x" href="http://apps.facebook.com/mytraveljournals/fbSync.php?action=showSynchronizeForm"<? }?>>
								<span>Synchronize </span>
							</a>	
						</li>
						
						<li class="last"> 
							<a title="Easily Configure your App to match your preference." <?if(4==$selectedDash) {?> class="selected" <? } else{?> class="x" href="http://apps.facebook.com/mytraveljournals/settings.php?action=showSettingsPage" <? }?>>
								<span>Settings </span>
							</a>
						</li>					
					
				</ul>
			</div> <!--end left tabs -->

		</div> <!--end center-->
	</div> 	
<?php } 

function getOwnerName($facebookID){ ?>
	<h5 style="margin-right:20px;text-align:right">
		<fb:name uid="<?php echo $facebookID ?>" possessive="true" /> travel journals
	</h5>
<?php } ?>