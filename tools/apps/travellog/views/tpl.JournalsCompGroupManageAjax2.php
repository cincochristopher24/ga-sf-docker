<?php
/*
 * Purpose: template used instead of tpl.JournalsComp.php when ajax call is used in changing tabs/pages
 *          for JournalsComp. This is also included by tpl.JournalsComp.php for regular loading
 */

$country = '';
$city = '';
$jeCtr = 1; // journal entry counter used for identifying yahoo carousel object
$carJsCode = ''; // yahoo carousel code

// temporary code:
require_once('Class.dbHandler.php');
require_once('travellog/model/Class.GroupFactory.php');
$dbh = new dbHandler();
$grpFactory = GroupFactory::instance();

?>

<input type="hidden" value="<?php echo $viewTab; ?>" id="selectedTab" />
<?php if ($subject instanceOf Group): ?>
    <input type="hidden" value="<?php echo $subject->getName(); ?>" id="hidSubjectName" />
    <?php if (!$subject->isParent() && $subject->getParent() instanceOf Group): ?>
        <input type="hidden" value="<?php echo $subject->getParent()->getName(); ?>" id="hidParentName" />
    <?php endif; ?>
<?php endif; ?>

<ul class="journals">
    <?php foreach ($journals as $journal): ?>
        <?php if ($journal->getType() == "JOURNAL"): ?>
            <?php
                $rAr = array(); // store related groups per journal here
                $jID = $journal->getTravelID();
                $jeCount = count($journalsAndEntries[$jID]);
                $jEntriesAr = $journalsAndEntries[$jID];
                $journalOwner = $journal->getOwner();
                if ($journalOwner == null) {
                    echo 'travelid:'.$journal->getTravelID();
                    continue;
                }
                $jah = new JournalAuthorUIHelper($journalOwner);
                $locationID = 0;
                if (isset($jEntriesAr[$jeCount - 1])) {
                    $trip = $jEntriesAr[$jeCount - 1]->getTrip();
                    $location = $trip->getLocation();
                    if (is_object($location)) {
                        $country = $trip->getLocation()->getCountry()->getName();
                        $city = $trip->getLocation()->getName();
                        $locationID = $trip->getLocation()->getCountry()->getCountryID();
                    }
                }
            ?>
            <li class="container ajax_journal" id="container<?php echo $jID; ?>">
                <div class="journal_box_wrap">
                    <div class="header">
                        <h3 class="group_h">
                            <?php if (isset($jEntriesAr[$jeCount - 1])): ?>
                                <a href="/journal-entry.php?action=view&travellogID=<?php echo $jEntriesAr[$jeCount - 1]->getTravelLogID(); ?>" alt="<?php echo $journal->getTitle(); ?>"><?php echo HtmlHelpers::truncateWord($journal->getTitle(), 50); ?></a>
                            <?php else: ?>
                                <?php echo HtmlHelpers::truncateWord($journal->getTitle(), 50); ?>
                            <?php endif; ?>
                        </h3>
                    </div>
                    <div class="content" id="content<?php echo $journal->getTravelID(); ?>">
                        <?php if (isset($journalPhotos[$jID])): ?>
                            <div class="photo">
                                <a href="/journal-entry.php?action=view&travellogID=<?php echo $jEntriesAr[$jeCount - 1]->getTravelLogID(); ?>">
                                    <img class="float_right" src="<?php echo $journalPhotos[$jID]->getPhotoLink('featured'); ?>" alt="journal photo" />
                                </a>
                            </div>
                        <?php endif; ?>
                        <?php if (isset($jEntriesAr[$jeCount - 1])): ?>
                            <div class="entry_title">
                                <a href="/journal-entry.php?action=view&travellogID=<?php echo $jEntriesAr[$jeCount - 1]->getTravelLogID(); ?>">
                                    <?php echo HtmlHelpers::truncateWord($jEntriesAr[$jeCount - 1]->getTitle(), 50); ?>
                                </a>
                            </div>
                            <div class="entry_detail">
                                <span class="location_info">
                                    <img class="flag" src="http://images.goabroad.com/images/flags/flag<?php echo $locationID; ?>.gif" width="22" height="11" alt="Travel Journals from <?php echo $country; ?>" style="margin: 0 0 0 0;" />
                                    <?php echo (strtolower($city) == strtolower($country)) ? $country : $city.', '.$country; ?> | <?php echo (strtotime($trip->getArrival())) ? date('D M d, Y', strtotime($trip->getArrival())).' | ' : ''; ?>Views: <?php echo $journal->getViews(); ?>
                                </span>
                                <?php if ($jeCount): ?>
                                    <p><?php echo (strlen(trim($jEntriesAr[$jeCount - 1]->getDescription())) > 175) ? strip_tags(nl2br(substr($jEntriesAr[$jeCount - 1]->getDescription(), 0, 175))).'...' : strip_tags(nl2br($jEntriesAr[$jeCount - 1]->getDescription())); ?></p>
                                <?php endif; ?>
                            </div>
                            <?php if($viewTab != 'NEW' && $viewTab != 'UNAPPROVED'): ?>
                                <?php
                                    $iSql = "SELECT tblGroup.name, tblGroup.groupID FROM tblGroupApprovedJournals, tblGroup ".
                                            "WHERE tblGroup.groupID = tblGroupApprovedJournals.groupID ".
                                            "AND tblGroupApprovedJournals.travelID = ".$dbh->makeSqlSafeString($journal->getTravelID())." ".
                                            "AND tblGroupApprovedJournals.approved = 1 ".
                                            "AND (tblGroup.groupID = {$dbh->makeSqlSafeString($subjectID)} OR tblGroup.parentID = {$dbh->makeSqlSafeString($subjectID)});";
                                    $rsGroupRelatedToJournal = new iRecordset($dbh->execute($iSql));
                                    $arGroupLinks = array();
                                    foreach ($rsGroupRelatedToJournal as $row) {
                                        $rAr[] = $row['groupID'];
                                        $relatedGroup = $grpFactory->create(array($row['groupID']));
                                        $arGroupLinks[] = "<a href='{$relatedGroup[0]->getFriendlyURL()}'>{$row['name']}</a>";
                                    }
                                ?>
                                <?php if ('APPROVED' == $viewTab): ?>
                                    <?php echo implode(', ', array_slice($arGroupLinks, 0, 3)); ?>
                                    <?php if (count($arGroupLinks) > 3): ?>
                                        <?php
                                            $content = addslashes(implode('<br /> ', $arGroupLinks));
                                            $jsf = '(function(){'.
                                                    'CustomPopup.initPrompt(\''.$content.'\', \'Related Groups\');'.
                                                    'CustomPopup.setHTML(true);'.
                                                    'CustomPopup.setJS(true);'.
                                                    'CustomPopup.setActionButton(\'Ok\');'.
                                                    'CustomPopup.createPopup();'.
                                                   '})();';
                                            echo " ...<a href='javascript:void(0)' onclick=\"$jsf\">[View All]</a>";
                                        ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <div class="no_entry_detail">
                                There are no entries for this journal.<br />
                                Document those great travel moments and share them with family and friends! A journal book is a set of journal entries about the same trip. You may write about your experiences and add your travel photos to your entries. You may also do a photo journal.
                            </div>
                        <?php endif; ?>
                        
                        <?php if ($showAuthor): ?>
                            <?php $owner = $journal->getTraveler(); ?>
                            <?php if (!$journal->isGroupJournalArticle() && !isset($relatedGroupsAr[$jID])): ?>
                                <p class="entry_group_tag">
                                    <span>
                                        <a class="username" href="http://<?php echo $_SERVER["SERVER_NAME"].'/'.$owner->getUserName(); ?>">
                                            <img class="pic" width="37" height="37" src="<?php echo $owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail'); ?>" title="Read more of <?php echo $owner->getUserName(); ?> travel experiences and adventures!" />
                                        </a>
                                    </span>
                                    <a class="author_username" href="http://<?php echo $_SERVER["SERVER_NAME"].'/'.$owner->getUserName(); ?>">
                                        <strong><?php echo $owner->getUserName(); ?></strong>
                                    </a>
                                </p>
                            <?php elseif (!$journal->isGroupJournalArticle()): ?>
                                <p class="meta">
                                    traveler:
                                    <a class="author_username" href="http://<?php echo $_SERVER["SERVER_NAME"].'/'.$owner->getUserName(); ?>"><strong><?php echo $owner->getUserName(); ?></strong></a>
                                    <?php if ($canAddJournal): ?>
                                        <strong class="meta"><em><?php echo $owner->getFullName(); ?></em></strong>
                                    <?php endif; ?>
                                </p>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <div class="entry_group_tag">
                            <?php if (isset($relatedGroupsAr[$jID][$subjectID])): ?>
                                <?php
                                    $group = $relatedGroupsAr[$jID][$subjectID];
                                    $site = $group->getServerName();
                                ?>
                                <?php if ($site != false): ?>
                                    <span>
                                        <a href="http://<?php echo $group->getServerName(); ?>" alt="<?php echo $group->getName(); ?>">
                                            <img src="<?php echo $group->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="" width="37" height="37" />
                                        </a>
                                    </span>
                                    <a href="http://<?php echo $group->getServerName(); ?>" alt="" class="author_username">
                                        <?php echo HtmlHelpers::truncateWord($group->getName(), 13); ?>
                                    </a>
                                    <br />
                                    <span>Advisor Group Journal</span>
                                <?php else: ?>
                                    <span>
                                        <a href="/group.php?gID=<?php echo $group->getGroupID(); ?>" class="<?php echo $group->getName(); ?>">
                                            <img src="<?php echo $group->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="" width="37" height="37" />
                                        </a>
                                    </span>
                                    <a href="/group.php?gID=<?php echo $group->getGroupID(); ?>" class="author_username">
                                        <?php echo HtmlHelpers::truncateWord($group->getName(), 13); ?>
                                    </a>
                                    <br />
                                    <span>Advisor Group Journal</span>
                                <?php endif; ?>
                            <?php elseif ($journal->isGroupJournalArticle()): ?>
                                <span>
                                    <a href="<?php echo $journalOwner->getFriendlyURL(); ?>" class="<?php echo $journalOwner->getName(); ?>">
                                        <img width="37" height="37" src="<?php echo $journalOwner->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="" />
                                    </a>
                                </span>
                                <a href="<?php echo $journalOwner->getFriendlyURL(); ?>" alt="" class="author_username">
                                    <?php echo HtmlHelpers::truncateWord($journalOwner->getName(), 13); ?>
                                </a>
                                <br />
                                <span>Advisor Group Journal</span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="actions" id="actions<?php echo $journal->getTravelID(); ?>">
                        <?php $group = new AdminGroup($subjectID); ?>
                        <div>
                            <?php if ('STAFF_ADMIN' == $viewTab): ?>
                                <?php if ($journal->getTraveler()->getTravelerID() == $_SESSION['travelerID']): ?>
                                    <a href="/journal-entry.php?action=add&travelID=<?php echo $jID; ?>" class="edit_entry_content" title="Add journal entry"><span id="add<?php echo $journal->getTravelID(); ?>">Add Entry</span></a>
                                    <?php if (isset($jEntriesAr[$jeCount - 1])): ?>
                                        <a href="/journal-entry.php?action=view&travellogID=<?php echo $jEntriesAr[$jeCount - 1]->getTravelLogID(); ?>">Edit Journal</a>
                                    <?php else: ?>
                                        <a href="javascript:void(0)" onclick="CustomPopup.initialize('Delete Journal ?', 'Are you sure you want to delete this journal?', '/travel.php?action=delete&travelID=<?php echo $jID; ?>', 'Delete', '1', 'All entries, photos and other traveler comments linked to this journal will also be deleted. This action cannot be undone.'); CustomPopup.createPopup();">Delete</a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                            
                            <a href="javascript:void(0)" onclick="approveJournal(<?php echo $journal->getTravelID(); ?>, <?php echo $subjectID; ?>)" class="edit_entry_content" title="Approve/Unapprove this journal to be viewed in group"><span id="approve<?php echo $journal->getTravelID(); ?>">Approve</span></a> 
                            <a href="javascript:void(0)" onclick="featureJournal(<?php echo $journal->getTravelID(); ?>, <?php echo $subjectID; ?>)" class="edit_entry_content" title="Move this journal into the featured journals"><span id="feature<?php echo $journal->getTravelID(); ?>">Feature</span></a> 
                            <a href="javascript:void(0)" onclick="forwardToUnapproveJournal(<?php echo $journal->getTravelID(); ?>, <?php echo $subjectID; ?>)" class="edit_entry_content" title="Move this journal into group unapproved journals"><span id="forwardToUnapprove<?php echo $journal->getTravelID(); ?>">To Unapproved</span></a>
                            
                            <?php
                                // quick fix only
                                $arr = JournalsComp::convertURLParamToSetupArray();
                                if (!isset($arr['VIEW_TAB'])) {
                                    $arr['VIEW_TAB'] = 'NEW';
                                }
                            ?>
                            <?php if (($arr['VIEW_TAB'] != 'NEW') && ($arr['VIEW_TAB'] != 'UNAPPROVED')): ?>
                                <?php if ($group->getParentID() == 0): ?>
                                    <a href="javascript:void(0)" onclick="relateJournal(<?php echo $journal->getTravelID(); ?>, <?php echo $subjectID; ?>, '<?php echo implode(",", $rAr); ?>')">Relate</a>
                                <?php endif; ?>
                            <?php endif; ?>
                            <input type="hidden" value="<?php echo $group->getParentID(); ?>" id="hidParentID" />
                        </div>
                    </div>
                </div>
                <!-- check if the journal controls is set for this journal id -->
                <?php if (isset($journalControlsJsCode[$jID])) echo $journalControlsJsCode[$jID]; ?>
            </li>
        <?php else: ?>
            <?php
                $jID = $journal->getTravelID();
                $link = "/article.php?action=view&articleID=".$jID;
                $location = LocationFactory::instance()->create($journal->getLocID());
                if (is_object($location)) {
                    $country = $location->getCountry()->getName();
                    $city = $location->getName();
                    $locationID = $location->getCountry()->getCountryID();
                }
            //$link = '/article.php?action=view&articleID='.$journal->getTravelID();
                $date = $journal->getDate();
            ?>
            <li class="container ajax_journal" id="container_ARTICLE<?php echo $jID; ?>">
                <div class="journal_box_wrap">
                    <div class="header">
                        <h3 class="group_h">
                            <a href="<?php echo $link; ?>" alt="<?php echo $journal->getTitle(); ?>"><?php echo HtmlHelpers::truncateWord($journal->getTitle(), 50); ?></a>
                        </h3>
                    </div>
                    <div class="content" id="content_ARTICLE<?php echo $journal->getTravelID(); ?>">
                        <?php if (isset($journalPhotos[$jID])): ?>
                            <div class="photo">
                                <a href="<?php echo $link; ?>">
                                    <img class="float_right" src="<?php echo $journalPhotos['ARTICLE'][$jID]->getPhotoLink('featured'); ?>" alt="journal photo" />
                                </a>
                            </div>
                        <?php endif; ?>
                        
                        <div class="entry_detail">
                            <span class="location_info">
                                <img class="flag" src="http://images.goabroad.com/images/flags/flag<?php echo $locationID; ?>.gif" width="22" height="11" alt="Travel Journals from <?php echo $country; ?>" style="margin: 0 0 0 0;" />
                                <?php echo (strtolower($city) == strtolower($country)) ? $country : $city.', '.$country; ?> | <?php echo (strtotime($journal->getDate())) ? date('D M d, Y', strtotime($journal->getDate())) : ''; ?>
                            </span>
                            <p>
                                <?php echo (strlen(trim($journal->getDescription())) > 175) ? strip_tags(nl2br(substr($journal->getDescription(), 0, 175))).'...' : strip_tags(nl2br($journal->getDescription())); ?>
                            </p>
                        </div>
                        <?php if ($showAuthor): ?>
                            <?php $owner = $journal->getTraveler(); ?>
                            <?php if (!$owner->isAdvisor() && !isset($relatedGroupsAr[$jID])): ?>
                                <p class="entry_group_tag">
                                    <span>
                                        <a class="username" href="http://<?php echo $_SERVER["SERVER_NAME"].'/'.$owner->getUserName(); ?>">
                                            <img class="pic" width="37" height="37" src="<?php echo $owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail'); ?>" title="Read more of <?php echo $owner->getUserName(); ?> travel experiences and adventure!" />
                                        </a>
                                    </span>
                                    <a class="author_username" href="http://<?php echo $_SERVER["SERVER_NAME"].'/'.$owner->getUserName(); ?>">
                                        <strong><?php echo $owner->getUserName(); ?></strong>
                                    </a>
                                </p>
                            <?php elseif (!$owner->isAdvisor()): ?>
                                <p class="meta">
                                    traveler:
                                    <a class="author_username" href="http://<?php echo $_SERVER["SERVER_NAME"].'/'.$owner->getUserName(); ?>"><strong><?php echo $owner->getUserName(); ?></strong></a>
                                    <?php if ($canAddJournal): ?>
                                        <strong class="meta"><em><?php echo $owner->getFullName(); ?></em></strong>
                                    <?php endif; ?>
                                </p>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <div class="entry_group_tag">
                            <?php $articleOwner = $journal->getOwner(); ?>
                            <?php if (!($articleOwner instanceOf Traveler)): ?>
                                <?php
                                    $group = $articleOwner;
                                    $site = $group->getServerName();
                                ?>
                                <?php if ($site != false): ?>
                                    <span>
                                        <a href="http://<?php echo $group->getServerName(); ?>" alt="<?php echo $group->getName(); ?>">
                                            <img src="<?php echo $group->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="" width="37" height="37" />
                                        </a>
                                    </span>
                                    <a href="http://<?php echo $group->getServerName(); ?>" alt="" class="author_username">
                                        <?php echo HtmlHelpers::truncateWord($group->getName(), 13); ?>
                                    </a>
                                    <br />
                                    <span>Advisor Group Article</span>
                                <?php else: ?>
                                    <span>
                                        <a href="/group.php?gID=<?php echo $group->getGroupID(); ?>" class="<?php echo $group->getName(); ?>">
                                            <img src="<?php echo $group->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="" width="37" height="37" />
                                        </a>
                                    </span>
                                    <a href="/group.php?gID=<?php echo $group->getGroupID(); ?>" class="author_username">
                                        <?php echo HtmlHelpers::truncateWord($group->getName(), 13); ?>
                                    </a>
                                    <br />
                                    <span>Advisor Group Article</span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="actions" id="actions_ARTICLE<?php echo $journal->getTravelID(); ?>">
                        <div>
                            <a href="<?php echo $link; ?>">
                                <span id="article<?php echo $journal->getTravelID(); ?>">
                                    <?php if ((!($articleOwner instanceOf Traveler) && $canAddJournal) || (($articleOwner instanceOf Traveler) && $articleOwner->getTravelerID() == $_SESSION['travelerID'])): ?>
                                        Edit Article
                                    <?php else: ?>
                                        View Article
                                    <?php endif; ?>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>

<?php
    // echo javascript code for journal controls
    foreach ($journalControlsJsCode as $travelID => $jsCode) {
        echo $jsCode;
    }
    
    if (isset($pagingComp)) {
        $pagingComp->showPagination();
    }
    
?>
