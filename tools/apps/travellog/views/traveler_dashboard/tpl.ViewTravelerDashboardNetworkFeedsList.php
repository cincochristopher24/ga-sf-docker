<?php
	if( count($feeds) ):
		foreach($feeds AS $feed):
			$feed->render();
		endforeach;
	elseif( $previousFeeds ):
		echo "<li class='infoBox'>No more updates to display...</li>";
	endif;
?>