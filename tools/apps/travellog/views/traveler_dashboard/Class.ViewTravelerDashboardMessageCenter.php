<?php
	
	require_once("Class.Template.php");

	class ViewTravelerDashboardMessageCenter{
		
		const NUMBER_OF_SHOWN_MESSAGES = 5;
		
		private $mTraveler	=	NULL;
		
		public function setTraveler($traveler=NULL){
			$this->mTraveler = $traveler;
		}
		
		public function render(){
			$messagesManager 	= gaMessagesManagerRepository::load($this->mTraveler->getTravelerID(), SiteContext::getInstance()->isInCobrand() ? 2 : 1);
			$messages     		= $messagesManager->getInboxWihtoutShoutOutMessages();
			$sentMessages 		= $messagesManager->getSentMessages();
			
			require_once("travellog/model/messageCenter/Class.gaTravelerMapper.php");
			$travelerMapper = new gaTravelerMapper();
			$friendIDs = $travelerMapper->getFriendIDs($this->mTraveler->getTravelerID());
			
			$tpl = new Template();
			$tpl->set('travelerID', $this->mTraveler->getTravelerID());
			$tpl->set('groupID', 0);
			$tpl->set('hasMoreMessages', count($messages) > self::NUMBER_OF_SHOWN_MESSAGES);
			$tpl->set('isPowerful', TRUE);
			$tpl->set('snippetLimit', 100);
			$tpl->set('canBeSelected', false);
			$tpl->set('canBeDeleted', true);
			$tpl->set('inboxMessages', array_slice($messages, 0, self::NUMBER_OF_SHOWN_MESSAGES));
			$tpl->set('outboxMessages', array_slice($sentMessages, 0, self::NUMBER_OF_SHOWN_MESSAGES));
			$tpl->set('friendIDs',$friendIDs);
			$tpl->out("travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardMessageCenter.php");
		}
	}