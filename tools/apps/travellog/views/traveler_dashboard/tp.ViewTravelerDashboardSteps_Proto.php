<div class="stepContainer">
  		<p><strong>Thank you for reg sdsdsdsistering on <?php echo $siteName ?></strong>. Now the fun begins! Here is how we suggest you proceed with your new Travel Journal adventures.</p>
         <div class="stepPrev stepGNav">
        	<a href="" class="stepNav">
            	<span class="stepNavArrowL"></span>
	        	<p>Go Back</p>
           	</a> 
        </div>		
		<ul class="stepGBox">
			<li id="one">
				<a href="/edittravelerprofile.php" class="stepBox stepFirst<?php if($step1Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepOne stepThreeLine"></span>
						Complete Company Profile
					</span>
					<span class="stepDetail">Add company description, logo, and set global privacy preferences.</span>
				</a>
			</li>
			<li id="two">
				<a href="/addressbookmanagement.php?action=add" class="stepBox<?php if($step2Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepTwo"></span>
						Create <br>Groups
					</span>
					<span class="stepDetail">Create a group for every unique program or travel group.</span>
				</a>
			</li>
			<li id="three">
				<a href="/travel.php?action=add" class="stepBox<?php if($step3Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">	
						<span class="stepNumber stepThree"></span>
						Add <br>Staff
					</span>
					<span class="stepDetail">Add other advisors or faculty to help manage your groups.</span>
				</a>
			</li>
			<li id="four">
				<a href="/group.php" class="stepBox<?php if($step4Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">	
						<span class="stepNumber stepFour"></span>
						Group Descriptions
					</span>
					<span class="stepDetail">Give the group a title and description.</span>
				</a>
			</li>
			<li id="five">
				<a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox<?php if($step5Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepFive"></span>
						Add Photos and/or Videos
					</span>
					<span class="stepDetail">Add stock photos of the location/program to start the group going or wait for photos from members.</span>
				</a>
			</li>
			<li id="six">
				<a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox<?php if($step6Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepSix"></span>
    	                Unload Documents
					</span>
					<span class="stepDetail">Documents can be private for the specific sub-group members or viewable by the public. Use this function to distribute forms, itineraries, and pre-departure information the students will need.</span>
			</a> 
			</li>
            <li id="seven">
                <a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox<?php if($step7Done): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
    	                <span class="stepNumber stepSeven"></span>
    	                Create <br>Events
    	            </span>
    	        	<span class="stepDetail">Add important dates to the calendar to notify the group of pending events.</span>
            	</a> 
            </li>
           <li id="eight">
                <a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox<?php if($step7Done): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
    	                <span class="stepNumber stepEight stepThreeLine"></span>
    	                Add <br>Important Links
    	            </span>
    	        	<span class="stepDetail">Links important external web sites.</span>
            	</a> 
            </li> 
           <li id="nine">
                <a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox<?php if($step7Done): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
    	                <span class="stepNumber stepNine stepFourLine"></span>
    	                Pre-<br>populate Knowledge Base/FAQs
    	            </span>
    	        	<span class="stepDetail">The knowledge base / discussion boards will be one of the most valuable tools in the network where students can search for FAQs with answers from their advisors and peers.</span>
            	</a> 
            </li>
           <li id="ten">
                <a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox stepLast<?php if($step7Done): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
    	                <span class="stepNumber stepTen stepThreeLine stepDD"></span>
    	                Invite/<br>Add Members
    	            </span>
    	        	<span class="stepDetail">Add groups of student email address in bulk and assign them to their appropriate sub-groups.</span>
            	</a> 
            </li>                                     
         </ul>
         <div class="stepNext stepGNav">
         	<a href="" class="stepNav">
            	<span class="stepNavArrowR"></span>
	            <p>Show More</p>
           	</a> 
         </div>         
</div>