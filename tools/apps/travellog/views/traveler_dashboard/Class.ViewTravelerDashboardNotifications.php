<?php
	
	require_once("Class.Template.php");
	require_once("travellog/model/Class.JournalEntryDraftPeer.php");
	require_once("Class.Criteria2.php");
	require_once('travellog/model/formeditor/Class.SurveyParticipant.php');
	require_once('travellog/model/formeditor/Class.SurveyForm.php');

	class ViewTravelerDashboardNotifications{
		
		private $mTraveler = NULL;
		
		public function setTraveler($traveler=NULL){
			$this->mTraveler = $traveler;
		}
		
		public function render(){
			$notifications = array();			
			
			$siteAccessType 		= SiteContext::getInstance()->isInCobrand() ? 2 : 1;
			$manager 				= gaMessagesManagerRepository::load($this->mTraveler->getTravelerID(), $siteAccessType);
			$cntUnreadMessages 		= $manager->countUnreadMessages();
			$cntFriendRequests		= count($this->mTraveler->getFriendRequests());
			$cntGroupInvitations	= $this->mTraveler->getNumberOfGroupInvites();
			$cntGroupRequests		= $this->mTraveler->getNumberOfGroupRequests();
			$cntDeniedGroupRequests	= $this->mTraveler->getNumberOfDeniedGroupRequests();
			
			$entry_drafts_count = JournalEntryDraftPeer::retrieveJournalEntryDraftsCount($this->mTraveler->getTravelerID(), 1);
			$cntEntryDrafts = (0 < $entry_drafts_count) ? $entry_drafts_count : -1;
			
			$isInCobrand = FALSE;
			$groupsJoined = array();//those groups in which a traveler is not a staff where only members are among the survey respondents
			if( SiteContext::getInstance()->isInCobrand() ){
				$isInCobrand = TRUE;
				$cobrandGroup = new AdminGroup(SiteContext::getInstance()->getGroupID());
				if( $cobrandGroup->getAdministratorID() != $this->mTraveler->getTravelerID() 
					&& !$cobrandGroup->isStaff($this->mTraveler->getTravelerID()) ){
					$groupsJoined = array_merge(array($cobrandGroup),$this->mTraveler->getTravelerCBSubGroups($cobrandGroup));
				}
			}else{
				$groupsJoined = $this->mTraveler->getGroups();
			}
		
			$surveys = array();
			$groups = array();
			foreach($groupsJoined AS $group){
				if( $group->getAdministratorID() != $this->mTraveler->getTravelerID() 
					&& !$group->isStaff($this->mTraveler->getTravelerID()) ){
					$tempSurveys = SurveyForm::getUnparticipatedSurveyFormsOfGroupByTraveler($group->getGroupID(),$this->mTraveler->getTravelerID());
					foreach($tempSurveys AS $temp){
						$surveys[] = $temp;
						$groups[] = $group;
					}
				}
			}
			if( 1 == count($surveys) ){
				$onlyGroupWithSurvey = $groups[0]->getGroupID();
			}
		
			$cntSurveysToAnswer = count($surveys);
						
			if( 0 < $cntUnreadMessages ){
				$notifications[] = '<a href="/messages.php">'.$cntUnreadMessages.' New '.($cntUnreadMessages > 1 ? 'Messages' : 'Message').'</a>';
			}
			
			if( 0 < $cntFriendRequests ){
				$notifications[] = '<a href ="/friend.php">'.$cntFriendRequests.' Friend '.($cntFriendRequests > 1 ? 'Requests' : 'Request').'</a>';
			}
			
			if( 0 < $cntGroupInvitations ){
				$notifications[] = '<a href ="/membership.php">'.$cntGroupInvitations.' Group '.($cntGroupInvitations > 1 ? 'Invitations' : 'Invitation').'</a>';
			}
			
			if( 0 < $cntGroupRequests ){
				$notifications[] = '<a href ="/membership.php">'.$cntGroupRequests.' Group '.($cntGroupRequests > 1 ? 'Requests' : 'Request').'</a>';
			}
			
			if( 0 < $cntDeniedGroupRequests ){
				$notifications[] = '<a href ="/membership.php">'.$cntDeniedGroupRequests.' Denied Group '.($cntDeniedGroupRequests > 1 ? 'Requests' : 'Request').'</a>';
			}
			
			if( 0 < $cntEntryDrafts ){
				$notifications[] = '<a href ="/journal_draft/view_saved_drafts">'.$cntEntryDrafts.' Unfinished Entry '.($cntEntryDrafts > 1 ? 'Drafts' : 'Draft').'</a>';
			}
			
			if( 0 < $cntSurveysToAnswer ){
				if( 1 < $cntSurveysToAnswer ){
					$notifications[] = '<a href ="javascript:void(0)" onclick="travelerDashboard.showSurveyList(); return false;" title="Click to view list.">'.$cntSurveysToAnswer.' Forms to Fill Out</a>';
				}else{
					$survey = $surveys[0];
					$notifications[] = '<a href="/surveyform.php?frmID='.$survey->getSurveyFormID().'&amp;gID='.$onlyGroupWithSurvey.'" title="'.htmlspecialchars(stripslashes(trim($survey->getName())),ENT_QUOTES).'">'.$cntSurveysToAnswer.' Form to Fill Out</a>';
				}
			}
						
			$tpl = new Template();
			$tpl->set('isNotificationShown', 0 < count($notifications));
			$tpl->set('notifications', $notifications);
			$tpl->set('surveys',$surveys);
			$tpl->set('groups',$groups);
			$tpl->out("travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardNotifications.php");
		}
	}