<?php require_once 'travellog/helper/messageCenter/messageCenterHelper.php' ?>
<div id="message_center" class="section">
 	<h2>
 		<span>Message Center</span>
		
 	</h2>
	<div class="content">
		<div id="messageNotification"></div>
		<div id="main_content">
			<ul id="nav">
				<li id="theInbox" class="first active" ><a href="javascript:void(0)" onclick="showInbox()"><span>Inbox</span></a></li>
				<li id="theOutbox"><a href="javascript:void(0)" onclick="showOutbox()"><span>Outbox</span></a></li>
				<li class="actions">
					<?php if( $hasMoreMessages ) : ?>
						<a class="viewall" href="messages.php">View All</a>
					<?php endif; ?>
						<a href="/messages.php?act=messageManyTravelers">Compose Message</a>
				</li>
			</ul>
			<div id="outboxContainer" class="miniMailBox" style="display:none">
				<table id="outbox" class="message_table">
					<tbody>
						<?php if (count($outboxMessages) > 0) : ?>
							<?php foreach ($outboxMessages as $message) : ?>
								<?php
									includeView('singleOutboxMessage', array(
																			'message'       => $message,
																			'snippetLimit'  => $snippetLimit,
																			'canBeSelected' => $canBeSelected,
																			'canBeDeleted'  => $canBeDeleted,
																			'showRealName'  => FALSE
																		));
								?>
							<?php endforeach; ?>
						<?php else: ?>
							<p class="help_text">No Messages</p>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
			
			<div id="inboxContainer" class="miniMailBox">
				<table id="inbox" class="message_table">
					<tbody>
						<?php if( count($inboxMessages) > 0 ): ?>
							<?php foreach( $inboxMessages as $message ): ?>
								<?php
									includeView('singleMessage', array(
																	'message'        => $message,
																	'snippetLimit'   => $snippetLimit,
																	'canBeSelected'  => $canBeSelected,
																	'canBeDeleted'   => $canBeDeleted,
																	'showRealName'   => $isPowerful && $message->getContentType() == gaMessage::PERSONAL && in_array($message->getSenderID(),$friendIDs),
																	'showReadStatus' => $isPowerful
																))
								?>
							<?php endforeach; ?>
						<?php else: ?>
							<p class="help_text">
								<span>You don't have messages yet</span>. <a href="/messages.php?act=messageManyTravelers">Send a Message</a> to  friend.
							</p>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
			
		</div>
		
	</div>
</div>


<script type="text/javascript">
	var isPowerful = <?php echo $isPowerful ? 1 : 0; ?>;
	var groupID = <?php echo $groupID ?>;
	var caller = 2;
	jQuery("#message_center .wrap").attr("style","width: 300px;");
</script>