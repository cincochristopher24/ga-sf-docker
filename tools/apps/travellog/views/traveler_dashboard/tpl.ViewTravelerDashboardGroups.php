<?php
//expected variables: travelerID, administered, staffed, other
	$ctr = 0;
	$hasGroups =  0 < (count($owned)+count($administered)+count($staffed)+count($other));
	
	$managedGroupsCount = count($owned)+count($administered)+count($staffed);
	$hasManagedGroups = 0 < $managedGroupsCount;
	$hasExcessManagedGroups = 3 < $managedGroupsCount;
	
	$otherGroupsCount = count($other);
	$hasOtherGroups = 0 < $otherGroupsCount;
	$hasExcessOtherGroups = 3 < $otherGroupsCount;
	
	$staffed = array_splice($staffed,10);
?>
<div class="section" id="groups">
	<h2>
		<span>Groups <?if($hasGroups && !$hasManagedGroups):?>(<?=$otherGroupsCount?>)<?endif;?></span>
		
		<?php if( !$hasManagedGroups && $hasExcessOtherGroups ): ?>
			<span>
				[<a href="javascript:void(0)" onclick="return false;" id="admin_groups" title="Show All">+</a>]
			</span>
			<script type="text/javascript">
				jQuery("#admin_groups").toggle(
					function(){
						jQuery(".other_excess").show();
						jQuery(this).attr("title","Show Less");
						jQuery(this).html('-');
					},
					function(){
						jQuery(".other_excess").hide();
						jQuery(this).attr("title","Show All");
						jQuery(this).html('+');
					}
				);
			</script>
		<?php endif; ?>
		
		<?php if( $showAddGroupLink ): ?>
			<span class="header_actions">
				<a href="<?=$addGroupLink?>">+ Create a new Group</a>
			</span>
		<?php endif; ?>
	</h2>
	<div class="content">
		<?php if( $hasGroups ): ?>
			<?php if( $hasManagedGroups ): ?>
				<div class="group_class">
					<h3 class="group_class_header group_class_header_managed">
						Managed Groups<?if($hasExcessManagedGroups):?> (<?=$managedGroupsCount?>)<?endif;?>:
						
						<?php if( $hasExcessManagedGroups ): ?>
							<span>
								[<a href="javascript:void(0)" onclick="return false;" id="managed_groups" title="Show All">+</a>]
							</span>
							<script type="text/javascript">
								jQuery("#managed_groups").toggle(
									function(){
										jQuery(".managed_excess").show();
										jQuery(this).attr("title","Show Less");
										jQuery(this).html('-');
									},
									function(){
										jQuery(".managed_excess").hide();
										jQuery(this).attr("title","Show All");
										jQuery(this).html('+');
									}
								);
							</script>
						<?php endif; ?>
					</h3>
					<ul class="groups groups_dashboard">
						<?php $shownManagedGroups = 0; ?>
						<?php for($i=0; $i<count($owned) && $shownManagedGroups<=100; $i++, $shownManagedGroups++){ 
							$owg = $owned[$i];
							$ctr++;
						?>
							<li class="owner <?if(3<$ctr):?>managed_excess<?endif;?>" style="<?if(3<$ctr):?>display:none<?endif;?>" >
								<a class="thumb" href="<?=$owg->getFriendlyURL()?>" title="Visit the travel community <?=htmlspecialchars(stripslashes($owg->getName()),ENT_QUOTES)?>"> <img  class="pic" src="<?=$owg->getGroupPhoto()->getPhotoLink()?>" height="65" width="65" alt="Travel community <?=htmlspecialchars(stripslashes($owg->getName()),ENT_QUOTES)?>" /> </a>
								<div class="details"> 
									<strong><a class="groupname" href="<?=$owg->getFriendlyURL()?>"> <?=htmlspecialchars(stripslashes($owg->getName()),ENT_QUOTES)?> </a></strong>
									(owner)
								</div>
							</li>
						<?php } ?>
						<?php $owned = NULL; ?>
						<?php unset($owned); ?>
						
						<?php for($i=0; $i<count($administered) && $shownManagedGroups<=100; $i++, $shownManagedGroups++){
							$ag = $administered[$i];
							$ctr++;
						?>
							<li class="admin <?if(3<$ctr):?>managed_excess<?endif;?>" style="<?if(3<$ctr):?>display:none<?endif;?>" >
								<a class="thumb" href="<?=$ag->getFriendlyURL()?>" title="Visit the travel community <?=htmlspecialchars(stripslashes($ag->getName()),ENT_QUOTES)?>"> <img  class="pic" src="<?=$ag->getGroupPhoto()->getPhotoLink()?>" height="65" width="65" alt="Travel community <?=htmlspecialchars(stripslashes($ag->getName()),ENT_QUOTES)?>" /> </a>
								<div class="details"> 
									<strong><a class="groupname" href="<?=$ag->getFriendlyURL()?>"> <?=htmlspecialchars(stripslashes($ag->getName()),ENT_QUOTES)?> </a></strong>
									(admin)
								</div>
							</li>
						<?php } ?>
						<?php $administered = NULL; ?>
						<?php unset($administered); ?>
						
						<?php for($i=0; $i<count($staffed) && $shownManagedGroups<=100; $i++, $shownManagedGroups++){
							$sg = $staffed[$i];
							$ctr++;
						?>
							<li class="staff <?if(3<$ctr):?>managed_excess<?endif;?>" style="<?if(3<$ctr):?>display:none<?endif;?>" >
								<a class="thumb" href="<?=$sg->getFriendlyURL()?>" title="Visit the travel community <?=htmlspecialchars(stripslashes($sg->getName()),ENT_QUOTES)?>"> <img  class="pic" src="<?=$sg->getGroupPhoto()->getPhotoLink()?>" height="65" width="65" alt="Travel community <?=htmlspecialchars(stripslashes($sg->getName()),ENT_QUOTES)?>" /> </a>
								<div class="details"> 
									<strong><a class="groupname" href="<?=$sg->getFriendlyURL()?>"> <?=htmlspecialchars(stripslashes($sg->getName()),ENT_QUOTES)?> </a></strong>
									(staff)
								</div>
							</li>
						<?php } ?>
						<?php $staffed = NULL; ?>
						<?php unset($staffed); ?>
						
						<?php if($shownManagedGroups >= 100): ?>
							<li class="managed_excess" style="display:none" >
								Only 100 shown. <a href="/group.php?mode=mygroups">View All &rarr;</a>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			<?php endif; ?>
		
			<?php if( 0 < count($other) ): 
				$ctr = 0;
			?>
				<div class="group_class">
					<?php if( $hasManagedGroups ): ?>
						<h3 class="group_class_header group_class_header_managed">
							Other Groups You Belong To<?if($hasExcessOtherGroups):?> (<?=$otherGroupsCount?>)<?endif;?>:
							
							<?php if( $hasExcessOtherGroups ): ?>
								<span>
									[<a href="javascript:void(0)" onclick="return false;" id="other_groups" title="Show All">+</a>]
								</span>
								<script type="text/javascript">
									jQuery("#other_groups").toggle(
										function(){
											jQuery(".other_excess").show();
											jQuery(this).attr("title","Show Less");
											jQuery(this).html('-');
										},
										function(){
											jQuery(".other_excess").hide();
											jQuery(this).attr("title","Show All");
											jQuery(this).html('+');
										}
									);
								</script>
							<?php endif; ?>
						</h3>
					<?php endif; ?>
					<ul class="groups groups_dashboard">
					
						<?php for($i=0; $i<count($other) && $i<=100; $i++){
							$og = $other[$i];
							$ctr++;
						?>
							<li class="<?if(3<$ctr):?>other_excess<?endif;?>" style="<?if(3<$ctr):?>display:none<?endif;?>" >
								<a class="thumb" href="<?=$og->getFriendlyURL()?>" title="Visit the travel community <?=htmlspecialchars(stripslashes($og->getName()),ENT_QUOTES)?>"> <img  class="pic" src="<?=$og->getGroupPhoto()->getPhotoLink()?>" height="65" width="65" alt="Travel community <?=htmlspecialchars(stripslashes($og->getName()),ENT_QUOTES)?>" /> </a>
								<div class="details"> 
									<strong><a class="groupname" href="<?=$og->getFriendlyURL()?>"> <?=htmlspecialchars(stripslashes($og->getName()),ENT_QUOTES)?> </a></strong>
									(member)
								</div>
							</li>
						<?php } ?>
						
						<?php if(count($other) > 100): ?>
							<li class="other_excess" style="display:none" >
								Only 100 shown. <a href="/group.php?mode=mygroups">View All &rarr;</a>
							</li>
						<?php endif; ?>
						
						<?php $other = NULL; ?>
						<?php unset($other); ?>
						
					</ul>
				</div>
			<?php endif; ?>
		<?php else: ?>
			<p class="side_help_text">
				<?php if( $isInCobrand ): ?>
					<span>You are not a member of any group yet</span>.<br /> Check out the <a href="/subgroups/category_search/"><strong>groups page</strong></a>!
				<?php else: ?>
					<span>You are not a member of any group yet</span>.<br /> Check out the <a href="/group.php"><strong>groups page</strong></a> or <a href="/group.php?mode=add&amp;advisor"><strong>create your own</strong></a>!
				<?php endif; ?>
			</p>
		<?php endif; ?>
	</div>
	<div class="foot"></div>
</div>