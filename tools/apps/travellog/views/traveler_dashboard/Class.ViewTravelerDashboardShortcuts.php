<?php
	
	require_once("Class.Template.php");

	class ViewTravelerDashboardShortcuts{
		
		private $mTraveler	=	NULL;
		
		public function setTraveler($traveler=NULL){
			$this->mTraveler = $traveler;
		}
		
		public function render(){
			$hasJournal = FALSE;
			$hasManyJournals = FALSE;
			$addNewEntryLink = "";
			$journalTitle = "";
			
			$journals = $this->mTraveler->getTravels($privacycriteria=0,$is_owner=TRUE);
			
			if( count($journals) ){
				$hasJournal = TRUE;
				if( 1 == count($journals) ){
					$latestJournal = $journals[0];
					$addNewEntryLink = "/journal-entry.php?action=add&amp;travelID=".$latestJournal->getTravelID();
					$journalTitle = htmlspecialchars(stripslashes($latestJournal->getTitle()),ENT_QUOTES);
				}else{
					$hasManyJournals = TRUE;
				}
			}
			
			$createJournalLink = "/journal.php?action=add";
			$addNewArticleLink = "/article.php?action=add";
			$uploadPhotosLink = "/collection.php?type=traveler&amp;ID=".$this->mTraveler->getTravelerID();
			$shareVideosLink = "/video.php?travelerID=".$this->mTraveler->getTravelerID();
			
			$tpl = new Template();
			$tpl->set("createJournalLink",$createJournalLink);
			$tpl->set("hasJournal", $hasJournal);
			$tpl->set("hasManyJournals", $hasManyJournals);
			$tpl->set("journals", $journals);
			$tpl->set("addNewEntryLink",$addNewEntryLink);
			$tpl->set("journalTitle",$journalTitle);
			$tpl->set("addNewArticleLink",$addNewArticleLink);
			$tpl->set("uploadPhotosLink",$uploadPhotosLink);
			$tpl->set("shareVideosLink",$shareVideosLink);
			$tpl->out("travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardShortcuts.php");
		}
	}