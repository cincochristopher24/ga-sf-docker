<?php
	
	require_once("Class.Template.php");
	require_once("travellog/components/activity_feed/factory/Class.ActivityFeedAdapterFactory.php");
	require_once("travellog/model/Class.ActivityFeed.php");

	class ViewTravelerDashboardNetworkFeeds{
		
		private $mTraveler	=	NULL;
		
		public function setTraveler($traveler=NULL){
			$this->mTraveler = $traveler;
		}
		
		public function render(){		
			$tpl = new Template();
			$tpl->set("travelerID",$this->mTraveler->getTravelerID());
			$tpl->out("travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardNetworkFeeds.php");
		}
	}