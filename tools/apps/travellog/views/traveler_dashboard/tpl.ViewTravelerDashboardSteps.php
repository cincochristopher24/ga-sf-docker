<div class="stepContainer stepTNav">
  	<p id="stepText" class="jLeft" style="padding: 3px 0; <?php if(!$showSteps): ?>display:none;<?php endif;?>"><strong>Thank you for registering on <?php echo $siteName ?></strong>. Now the fun begins! Here is how we suggest you proceed with your new Travel Journal adventures.</p><a href="javascript:void(0);" class="stepOption jRight <?php if(!$showSteps): ?>stepOptionShow<?php endif;?>"><img src="/images/loading_small.gif" id="hideShowLoader" style="display:none;padding:2px 0;" class="jRight"/><span id="stepOptionText"><?php if(!$showSteps): ?>Show Steps<?php else:?>Hide Steps<?php endif;?></span></a>
  		
  		
  	<div id="thisCont" class="clear">
		<ul id="steps_carousel" class="stepTBox jcarousel-skin-tango" <?php if(!$showSteps): ?>style="display:none;"<?php endif;?>>
			<li id="one">
				<a href="/edittravelerprofile.php" class="stepBox <?php if($step1Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepOne"></span>
						Complete <br>Your Profile
					</span>
					<span class="stepDetail">Give your Journal Entries a personality and open up opportunities to meet like minded travelers!</span>
				</a>
			</li>
			<li id="two">
				<a href="/addressbookmanagement.php?action=add" class="stepBox<?php if($step2Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepTwo"></span>
						Create Your Address Book
					</span>
					<span class="stepDetail">Create your address book. Adding the email addresses of your friends and family will keep them updated whenever you post new Journal Entries and photos.</span>
				</a>
			</li>
			<li id="three">
				<a href="/travel.php?action=add" class="stepBox<?php if($step3Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">	
						<span class="stepNumber stepThree"></span>
						Create Your Journals
					</span>
					<span class="stepDetail">Create a Journal Title and a short Description, Add a Journal Entry and upload Travel Photos.	</span>
				</a>
			</li>
			<li id="four">
				<a href="/group.php?mode=mygroups" class="stepBox<?php if($step4Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">	
						<span class="stepNumber stepFour"></span>
						Join and <br>Create Groups
					</span>
					<span class="stepDetail">Join a group or start your own and share your travel experiences while searching for answers to your questions. Get involved in the network!</span>
				</a>
			</li>
			<li id="five">
				<?php $link = ($redirectToPhoto) ? '/collection.php?type=traveler&ID='.$travelerID : '/video.php?travelerID='.$travelerID; ?>
				<a href="<?php echo $link; ?>" class="stepBox<?php if($step5Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepFive stepThreeLine"></span>
						Upload <br>Photos and Videos
					</span>
					<span class="stepDetail">Easy to use photo upload tools. Upload photos for journal entries or upload into photo albums.</span>
				</a>
			</li>
			<li id="six">
				<a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox<?php if($step6Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepSix"></span>
						Write Your Travel Bio
					</span>
					<span class="stepDetail">Share your most memorable travel experiences by answering quick, fun questions about your travels!</span>
				</a>
			</li>
			<li id="seven">
				<a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox<?php if($step7Done): ?> stepDone<?php endif; ?>">
					<span class="stepTitle">
						<span class="stepNumber stepSeven"></span>
    	                Share Your Travel Plans
					</span>
					<span class="stepDetail">Let others know where you've been, where you are, where you're going and what you're up to. List your travel plans and find out who could be traveling your way!</span>
				</a> 
			</li>
            <li id="eight">
                <a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox<?php if($step8Done): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
    	                <span class="stepNumber stepEight stepThreeLine"></span>
    	                Put Your Travels On the Map
    	            </span>
    	        	<span class="stepDetail">Point out the countries you've been to and show your travel map on your profile.</span>
            	</a> 
            </li>
            <li id="nine">
                <a href="/widget.php?travelerID=<?php echo $travelerID; ?>" class="stepBox stepLast<?php if($step9Done): ?> stepDone<?php endif; ?>">
	                <span class="stepTitle">
    	                <span class="stepNumber stepNine stepFourLine"></span>
    	                SYNC with Facebook &amp; other social media sites
    	            </span>
    	        	<span class="stepDetail">Share journals on Facebook! Add our apps on Facebook, MySpace, Orkut &amp; hi5 to bring your travels to your profile.</span>
            	</a> 
            </li>            
         </ul>
	</div>	
</div>
