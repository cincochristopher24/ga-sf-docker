<?php
//expected variables: createJournalLink, hasJournal, addnewEntryLink, journalTitle, addNewArticleLink, uploadPhotosLink, shareVideosLink
?>
<div class="section">
	<h2>
		<span>Shortcuts</span>
	</h2>
	<div class="content">
		<ul class="shortcuts">
			<li>
				<a href="<?=$createJournalLink?>" class="dashOne button_v3" title="Create a new journal."><span> Create a New Journal</span></a>
			</li>
			<?php if( $hasJournal ): ?>
				<?php if( $hasManyJournals ): ?>
					<li>
						<a href="javascript:void(0)" class="dashTwo button_v3" onclick="travelerDashboard.showJournalSelection(); return false;" title="You have <?=count($journals)?> journals to choose from. Click to view list."><span> Add an Entry</span></a>
					</li>
				<?php else: ?>
					<li>
						<a href="<?=$addNewEntryLink?>" class="dashTwo button_v3" title="Add an entry to your latest journal <?=$journalTitle?>."><span> Add an Entry to "<?= 10 < strlen($journalTitle) ? substr($journalTitle,0,10)."..." : $journalTitle ?>"</span></a>
					</li>
				<?php endif; ?>
			<?php endif; ?>
			<li>
				<a href="<?=$addNewArticleLink?>" class="dashThree button_v3" title="Write a new article."><span> Write a New Article </span></a>
			</li>
			<li>
				<a href="<?=$uploadPhotosLink?>" class="dashFour button_v3"><span> Upload Photos </span></a>
			</li>
			<li>
				<a href="<?=$shareVideosLink?>" class="dashFive button_v3"><span> Share Videos </span></a>
			</li>
		</ul>
	</div>
	<div class="foot"></div>
</div>

<?php if($hasManyJournals): ?>
	<div id="journal_selection" style="display:none;">
		<ul class="entries_selection">
			<?php foreach($journals AS $journal): ?>
				<li>
					<a href="<?='/journal-entry.php?action=add&amp;travelID='.$journal->getTravelID()?>" onclick="CustomPopup.removePopup();" title="<?=htmlspecialchars(stripslashes($journal->getDescription()),ENT_QUOTES)?>">
						<?=htmlspecialchars(stripslashes($journal->getTitle()),ENT_QUOTES)?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>