<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJS('/js/interactive.form.js');	
?>

<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');	
?>

<? if ($isAdminLogged) { ?>
	<div class="area" id="intro">
		<div class="section" align="right">
			<h2><a href="onlinesurvey.php?formID=<?=$form->getOnlineFormID()?>">Survey Results</a>
				| <a href="surveylist.php?formID=<?=$form->getOnlineFormID()?>">Participant List</a>
			</h2>		
		</div>	
	</div>

<? } ?>


<div class="area" id="intro">
	<div class="section">
		<h1><?=$form->getTitle()?></h1>
		<div class="content">
			<p><?=$form->getDescription()?></p>
			<form name="frmOnlineForm" action="onlineform.php?formID=<?=$form->getOnlineFormID()?>&mode=submit" method="post" class="interactive_form">
				<ul class="form">
					<? for($i=0; $i<count($arrQuestions);$i++) { ?>
						<? $each = $arrQuestions[$i]; ?>
						<? $currQuesID = $each->getQuestionID() ;?>
						<li>
							<label for="Answer<?=$currQuesID?>"><?=$i+1?>. <?=$each->getQuestion()?></label>	
						<? if (OnlineItemType::$RADIO_BUTTON == $each->getQuestionType()) { ?>
							<? $options = $each->getFormOptions() ?>							
							
								<fieldset class="choices">
									<ul>
										<? $k = 0; foreach ($options as $eachoption) { $k++;?>
											<li>
												<input type="radio" name="Answer<?=$currQuesID?>" id="Answer<?=$currQuesID.'_'.$k?>" value="<?=$eachoption->getOptionID()?>" />
												<label for="Answer<?=$currQuesID.'_'.$k?>"><?=$eachoption->getTitle()?></label>
											</li>
											<? if (0 < $eachoption->getMiscType()) { ?>
												<? if  (OnlineItemType::$TEXTAREA == $eachoption->getMiscType()) { ?>
													<li>
														<textarea name="Misc<?=$currQuesID?>" cols="100" rows="5" id="txaAnswer"></textarea>								
													</li>
												<? } ?>
											<? } ?>
										<? } ?>
									</ul>
								</fieldset>
							
						<? } else if (OnlineItemType::$TEXTBOX == $each->getQuestionType()) { ?>
							
								<input type="text" name="Answer<?=$currQuesID?>" id="Answer<?=$currQuesID?>" value="" class="text" size="50" maxlength="50" />
							
						<? } else if (OnlineItemType::$TEXTAREA == $each->getQuestionType()) { ?>	
							
								<textarea name="Answer<?=$currQuesID?>" cols="100" rows="5" id="Answer<?=$currQuesID?>"></textarea>								
							
						<? } ?>
						</li>
					<? } ?>	
					<? if (!$isAdminLogged) { ?>
						<li>
							<input type="submit" name="submit" value="submit">
						</li>
					<? } ?>
				</ul>
			</form>			
			<div class="clear"></div>
		</div>
	</div>
</div>