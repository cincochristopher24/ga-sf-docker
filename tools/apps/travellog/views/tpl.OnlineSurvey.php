<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');	
?>

<div class="area" id="intro">
	<div class="section" align="right">
		<h2><a href="onlineform.php?formID=<?=$form->getOnlineFormID()?>">Survey Form</a>
			| <a href="surveylist.php?formID=<?=$form->getOnlineFormID()?>">Participant List</a>
		</h2>
	</div>	
</div>

<div class="area" id="main">
	<div class="section">
		<h1><?=$form->getTitle()?></h1>
		<h2><?=$form->getDescription()?></h2>
		<div class="content">
			
			<ul class="form">
				<? for($i=0; $i<count($arrQuestions);$i++) { ?>
					<? $each = $arrQuestions[$i]; ?>
					<? $currQuesID = $each->getQuestionID() ;?>
					<? $arrAnswersByQuestion = $each->getSurveyAnswers(); ?>	
					<li>
						<label><?=$i+1?>. <?=$each->getQuestion()?></label>										 
					</li>	
					<? if (OnlineItemType::$RADIO_BUTTON == $each->getQuestionType()) { ?>
						<? $options = $each->getFormOptions() ?>							
						<li>
							<fieldset class="choices">
								<ul>
									<? foreach ($options as $eachoption) { ?>
										<? $currOptionID = $eachoption->getOptionID(); ?>
										<li>
											<label><?=$eachoption->getTitle()?></label>
											&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
											<? $arrAnswersByOption = $eachoption->getSurveyAnswers(); ?>											   	
											<label><?=count($arrAnswersByOption)?></label>
										</li>
										<? if (0 < $eachoption->getMiscType()) { ?>
											<? if  (OnlineItemType::$TEXTAREA == $eachoption->getMiscType()) { ?>
												<li>
																					
												</li>
											<? } ?>
										<? } ?>
									<? } ?>
								</ul>
							</fieldset>
						</li>
					<? } else { ?>
						<li>
							<? if (1 < count($arrAnswersByQuestion) ) { ?>
								<a href="onlineanswers.php?qID=<?=$currQuesID?>">View <?=count($arrAnswersByQuestion)?> Answers</a>
							<? } else if (1 == count($arrAnswersByQuestion) ){ ?> 
								<a href="onlineanswers.php?qID=<?=$currQuesID?>">View Answer</a>
							<? } ?>
						</li>						
					<? } ?>
				<? } ?>	
				<li>
					
				</li>
			</ul>
	
			<div class="clear"></div>
		</div>
	</div>
</div>