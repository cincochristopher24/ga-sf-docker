<?php
	/*
	 * Class.ViewMessageActions.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/views/Class.AbstractView.php");

	class ViewMessageActions extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewMessageActions.php');
		}
		
	}
?>
