<?php if (isset($profView))
		  echo $profView->render();
?>
<?php if( !$objSubNavigation->getDoNotShow() ) echo $objSubNavigation->show();?>

<div id="content_wrapper" class="layout_2 custom">
	<div id="wide_column">
  		<?php
				if($jCompContextAr['VIEW_TAB'] != 'JOURNALS_FEED')
	  				echo $journalsComp->render();
				else
					echo $journalsComp->renderJournalsFeed();
					
  				$tabs = $journalsComp->generateViewTabURLParams();
  				$linkClass = $jCompContextAr['VIEW_TAB'];
  				//echo 'classfdsdfsd:'.$linkClass;
  		?>
	</div>
	<div id="narrow_column">
		<?if ($is_owner):?>
			<div id="journals_nav" class="section">
				<h2> <span> Navigation</span></h2>
				<ul class="nav_narrowcol_list">
					
					<li <?if($linkClass=='FEATURED'):?>class="active"<?endif;?>>
						<div class="arrow"> &larr; </div>
						<a class="nav_accord" href="/journal.php?action=groupjournals&amp;<?=$tabs['FEATURED']?>"  title="Journals that will be displayed at groups homepage" >Featured</a>
					</li>
					<li <?if($linkClass=='NEW'):?>class="active"<?endif;?> >
						<div class="arrow"> &larr; </div>
						<a class="nav_col_link" href="/journal.php?action=groupjournals&amp;<?=$tabs['NEW']?>"  title="Journals of members not yet approved for inclusion in this group" >New</a>
					</li>
					<li <?if($linkClass=='STAFF_ADMIN'):?>class="active"<?endif;?> >
						<div class="arrow"> &larr; </div>
						<a class="nav_col_link" href="/journal.php?action=groupjournals&amp;<?=$tabs['STAFF_ADMIN']?>" title="Journals written by administrator or staff"	>Staff/Admin</a>
					</li>
					<li <?if($linkClass=='APPROVED'):?>class="active"<?endif;?>>
						<div class="arrow"> &larr; </div>
						<a class="nav_col_link" href="/journal.php?action=groupjournals&amp;<?=$tabs['APPROVED']?>"	 title="Journals of members included in this group"	>Approved</a>
					</li>
					<li <?if($linkClass=='UNAPPROVED'):?>class="active"<?endif;?> >
						<div class="arrow"> &larr; </div>
						<a class="nav_col_link" href="/journal.php?action=groupjournals&amp;<?=$tabs['UNAPPROVED']?>"  title="Journals of members temporarily hidden from viewing in this group"	>Unapproved</a>
					</li>
				</ul>
			</div>
		<?endif;?>
		<?if($linkClass != 'JOURNALS_FEED'):?>
			<div id="journals_search" class="section">
		        <div id="search_wrap">
					<h2><span>Search Journals By Author</span></h2>
					<form action="/journal.php?action=searchjournalsbyauthor&amp;gID=<?=$gID?>" method="post">
						<input class="textbox" type="text" name="keyword" />
						<input type="hidden" name="origParam" value="<?=$origParam?>">
						<input type="hidden" name="viewBy" value="<?=$viewBy?>">
						<input type="hidden" name="jcp" value="<?=$jcp?>">
						<input type="submit" value="Show Journals"/>
					</form>
				
					<hr/>
				
			        <?if ($is_owner):?>
						<a class="button" href="/journal.php?action=add&amp;contextGroupID=<?=$gID?>" >+ Add a new Journal</a>
						<a class="button" href="/article.php?action=add&amp;gID=<?=$gID?>" >+ Add a new Article</a>
						<?php if (0 < $journal_drafts_count): ?>
							<a class="button" href="/journal_draft/view_saved_drafts/group/<?php echo $gID; ?>">View All Drafts</a>
						<?php endif; ?>
					<?endif;?>									
		        </div>
			</div>
		<?endif;?>
	</div>
 </div>

