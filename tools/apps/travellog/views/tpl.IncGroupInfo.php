<?php
  /**
   * Created on Oct 20, 2006
   * Author: Czarisse Daphne P. Dolina
   * Tpl.IncGroupInfo.php - displays group info; group name, description, 
   * institution info if any, inquiries if any, links to edit profile and photos, 
   * link for traveler to join group.. 
   * 
   */


  require_once('Class.HtmlHelpers.php');
  
  $minor_info_shown = false;
?>

<div class="section" id="group_profile">	
  <div class="content">
	  <div id="photo_and_actions">
      <img id="grp_img" class="profile_pic" src="<?=$grpPhoto->getPhotoLink(); ?>" alt="Emblem" title="Emblem" />							
			
			<div class="profile_links">
				<?	if (count($grpCategoryList)):	?>												
							
					<p>
					<span>
					
					  <? if ( Group::FUN == $grpDiscriminator || (Group::ADMIN == $grpDiscriminator && TRUE == $isParent) ) { 
						  $i = 1;
						  $count = count($grpCategoryList);
						  foreach($grpCategoryList as $value):
							  echo $value;
							  if ($i<$count):
								  echo ", "; 
							  endif;
							
							  $i++;
						  endforeach;
					  } ?>
						
					</span>

					<?php $minor_info_shown = true ?>
					
			<?	endif; ?>

			<? if (isset($grpWebURL) && strlen($grpWebURL ) ) : ?>
			  <a href="http://<?= $grpWebURL?>" rel="nofollow" target="_blank">
					<?= 'http://'.$grpWebURL ?>
				</a>
			  
			  </p>
				
				<?php $minor_info_shown = true ?>
				
			<? endif; ?>
			
			<ul>		
				<? if (TRUE == $isAdminLogged): ?>
					
					<li>
					  <a href="javascript:void(0)" onclick="tb_show('','/thickbox/thickbox.php?height=680&width=529&mode=upload&context=group&genID=<?=$grpID?>','');">
					    <?= $grpPhotoLabel ?>
					  </a>
					</li>	
					
				  	<li>
				  		<? if ($group->getGroupPhotoID()): ?>
								<? $popup_link = "/ajaxpages/PhotoUILayout.php?action=deletelogo&amp;context=group&amp;redirect&amp;genid=".$group->getGroupID() ?>
						  	<a href="javascript:void(0)" onclick="CustomPopup.initialize('Remove Group Logo ?','Are you sure you want to remove <?= addslashes($group->getName()) ?> group logo?','<?=$popup_link?>','Remove','0');CustomPopup.createPopup();return false;">
						    	<?=($is_parent_group)?'Remove Group Logo':'Remove Subgroup Logo'?>
						  	</a>
					  	<? endif; ?>
				  	</li>

					<li>
					  <a href="/group.php?gID=<?= $grpID ?>&amp;mode=edit" > 
					    Edit <?=($is_parent_group)?$grpLabelName:'Subgroup' ?> Profile 
					  </a>
					</li>
					
					<li>
					  <a href="/group-privacy.php?groupID=<?=$grpID?>"> 
					    Edit Privacy Preferences 
					  </a>
					</li>
					
					<li>
						<?$popup_link = '/group.php?mode=delete&amp;gID='.$grpID.'&amp;confirm&amp;ref=subgrouphome'; ?>
						<a href="javascript:void(0)" onclick = "CustomPopup.initialize('Remove <?=addslashes($group->getName())?> ?','Are you sure you want to remove <?=addslashes($group->getName())?>?','<?=$popup_link?>','Remove','0');CustomPopup.createPopup();return false;">	
							<?=($is_parent_group)?'Delete Group':'Delete Subgroup'?>
						</a>
					</li>
						
					<?php if ($group->isSubGroup()): ?>
						<li>
							<a title="" href="javascript:GroupTemplateManager.showCreateForm(<?php echo $group->getID(); ?>);">Save as template</a>
						</li>
					<?php else: ?>
						<li>
						  <a href="javascript:void(0)" onclick="window.open('/changepassword.php?action=entry','ChangePassword','width=500,height=355,navigation toolbar=disabled,left=200,top=200')" >
						    Change Password
						  </a>
						</li>
					<?php endif; ?>
				
				<? endif; ?>
				<? if (strlen($group->getGroupMembershipLink('grouphome'))):?>
					<li>
					  <?=$group->getGroupMembershipLink('grouphome')?>
					</li>
				<? endif;?>
						
				<? if (!$isAdminLogged && $isLogged && FALSE == $loggedUser->isAdvisor()) { ?>
					<li>
						<a href="/messages.php?act=messageGroupStaff&gID=<?= $grpID ?>">
						  Message <?= $grpLabelName ?> Staff
						</a>
					</li>
				<? } ?>
						
				<? if (isset($grpSurveyLink) && isset($grpSurveyLabel)) { ?>
				  <li>
				    <a href="/<?=$grpSurveyLink?>">
				      <?=$grpSurveyLabel?>
				    </a>
					</li>
        <? } ?>
      </ul>
		
    </div>
	</div>	
			
	<div class="profile_cont" <? if ($minor_info_shown):?>class="divide"<? endif;?>>
		<h1><?= $grpName ?></h1> 
					
		<div id="institution_info">
		  
		  <strong>About us:</strong>
		  <br />	
			
			<?php echo nl2br(htmlspecialchars($truncated_description_right)); ?>
			
			<? if ($show_collapse): ?>
			  <span id="dots">
			    ...
			  </span>
			
			  <span id="show_more_description" style="display:none;">
			    <?php echo nl2br(htmlspecialchars($truncated_description_left)); ?>
			    <?php echo (isset($grpInstitutionInfo)) ? $grpInstitutionInfo : '' ?>
			  </span>
			<? endif; ?>
		</div> 		
		
		<? if ($show_collapse): ?>
		  <a id='collapse' href='javascript:void(0)'>
			  More&darr;
		  </a>
		<? endif; ?>
		
	</div>

				
  <? if (isset($grpAdmin)): ?>
		<div id="group_administrator" class="users">
			<strong>Administrator:</strong>
			<br />
			
			<? $adminprofile = $grpAdmin->getTravelerProfile()?>
			
			<a href="/<?=$adminprofile->getUserName() ?>" class="user_thumb" >
				<img src="<?=$adminprofile->getPrimaryPhoto()->getPhotoLink('thumbnail') ?>" width="37" height="37" alt="User Profile Pic" title="View Profile" />
			</a>							 
						
			<a href="/<?=$adminprofile->getUserName() ?>" class="username">
				<?=$adminprofile->getUserName() ?>
			</a>
			
			<div style="clear: left;"></div>
		</div>				
  <? endif; ?>		
  </div>
</div>

<? if ($show_collapse): ?>
  <script type="text/javascript">
	  is_shown = false;
    (function($){
      $(document).ready(function(){
        $('#collapse').click(function(){
          if(is_shown){
        	  document.getElementById('dots').style.display = "";
        	  $('#show_more_description').slideUp(400);
        	  $(this).html('More&darr;');
        	  is_shown = false;
          } else{
        	  document.getElementById('dots').style.display = "none";
            $('#show_more_description').slideDown(400);
            is_shown = true;
            _timer_ = setTimeout(function(){
          	  document.getElementById('show_more_description').style.display = "";
          	  clearTimeout(_timer_);
            },405);
            $(this).html('Less&uarr;');
          }
        });
      });
    })(jQuery);
  </script>
<? endif; ?>
<script type="text/javascript" src="/min/f=js/GroupTemplateManager.js"></script>