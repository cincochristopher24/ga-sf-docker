<?
	/**
	* tpl.ViewAllCountries.php
	* @author marc
	* aug 2006
	*/
	require_once("travellog/controller/Class.GANETDestinationController.php");
?>

<div class="area" id="intro">
	<div class="section">
		<h1>Destinations</h1>
		<div class="content">
		<?
			echo nl2br($contents["helpText"]->getHelpText('destinations'));	// added by marc for help text
		?>
		</div>
		<div class="foot"></div>
	</div>
</div>

<div class="area" id="top">
		
		
		<div class="section map">
			<h2><span>World Map</span></h2>
			<div class="content" id="viewallcountries_map">
				<?=
					$contents["map"]->addMapFrame($contents["mFrame"]); 
					$contents["map"]->plotLocations($contents["arrLocationMarker"]); 
				?>
			</div>
			<div class="foot"></div>
		</div>
		
		
		<div class="section" id="country_list">
			<h2><span>Countries</span></h2>
			<div class="content">
				<?php 
					foreach($contents["countryArr"] as $countryCol)
						GANETDestinationController::viewCountryList($countryCol);
				?>
				<div class="clear"></div>
			</div>
			<div class="foot"></div>						
		</div>

</div>



	

