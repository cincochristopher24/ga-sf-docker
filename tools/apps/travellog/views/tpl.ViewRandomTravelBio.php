<?
	/*  commented out by neri: 	01-13-09
	 * 		included these js scripts in the template for the profile component
	 * 			to avoid redundancy 
	
				Template::includeDependentJs('/js/utils/json.js');
				Template::includeDependentJs('/js/LIB.js');	
				Template::includeDependentJs('/js/travelbio.js');
	 *		changed 0 to 1 in the the condition checking the value of numQ
	*/
?>

<? /*if (0 < $numQ || (0 == $numQ && $isOwner)) :*/ ?>
 	<div id = "bubble_philosophy" style = "display: <?=($hideDiv) ? 'none' : 'block'?>"> 
	<?
		$code =<<<BOF
			<script type = "text/javascript">
				//<!--
				window.onload = function(){
					TravelBio.setTravelerId($ownerId);
					TravelBio.setQuestionId($profileQuestionId);
					TravelBio.displayTBAction($numQ);
				}
				//-->
			</script>
BOF;
		Template::includeDependent($code);
	?>	
		<p id="travelbio_statusPanel" style = "display: none">
	        <span id="imgLoading">
 	            <img alt="Loading" src="/images/loading.gif"/><span>Please wait...</span>
			</span>			
	    </p>
	    
		<div id="bubble_arrow">&nbsp;</div>	    
		<strong id = "travelbio_profileQuestion"><?=$profileQuestion?></strong>              
		<p id = "travelbio_profileAnswer"><?=$profileAnswer?></p>

		<div id="travelbio_actions" <?if(1 < $numQ):?>style="height:20px"<?endif;?>>
			<? if ($showAction && 1 < $numQ) : ?>
					<a
						id		= "travelbio_link"
						class   = "more"
						href	= "javascript:void(0)"
						title	= "More from TravelBio"
						onclick	= "TravelBio.changeTravelBio();return false;">
						More from
						<span class="mtb_my">my</span><span class="mtb_travel">travel</span><span class="mtb_bio">bio</span>
					</a>
					
					<!--<a class = "facebook_direct" target = "_blank" title = "Install this App in Facebook" href = "http://www.goabroad.net/faq.php#bring_2_facebook">
						<span class="mtb_my">my</span><span class="mtb_travel">travel</span><span class="mtb_bio">bio</span>  
					</a>--> 
			<? endif; ?>
		</div>	
	</div>
<? /*endif;*/ ?>