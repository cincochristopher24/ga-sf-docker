<?php $teachdisp = "none"; ?>


 
  <?php if (isset($radIsTeacher)): ?>
  
		<?php if ($radIsTeacher == 1): ?>
			<?= $teachdisp = "block"; ?>	
  		<?php endif; ?>
  		

 	<?php endif; ?>
 
  
<div style="clear: left;display:<?=$teachdisp?>" id="accountadd">
  
  <h2>Teacher&#8217;s Profile</h2>
  <ul class="form">
	  <li class="form_set">
	  	<fieldset>
	  		<legend><label for="lstCurrTeachJob">Current Teaching Position<span class="required">*</span></label></legend>
				<span>
					<select name="lstCurrTeachJob" id="lstCurrTeachJob" onchange="teachpos();">
						<option value="0">-SELECT-</option>
						
							<?= $teachposOptions ?>
			        </select>
		        </span>
		        <span>
				  	<?php echo  FormHelpers::CreateTextBox('txtOtherCurrTeachJob', $txtOtherCurrTeachJob,
				    		array(
				    			'size' => 45,
				    			'class' => 'text',
				    			'maxlength' => 50,
								'disabled' => ($lstCurrTeachJob != 34)
				    		) 
				    	); ?>
		    	</span>
	    	</fieldset>
	   </li>
			  
	   <li class="form_set">
	  		
	  		<label for="lstEnglishProf">English Proficiency<span class="required">*</span></label>
			
			<select name="lstEnglishProf" id="lstEnglishProf">
				<option value="0">-SELECT-</option>
						
				<?= $engprofOptions ?>
	        </select>
	        
	   </li>
	  
	  <li class="form_set">
	  
	    <label for="lstYearsTeachExperience">Years of Experience<span class="required">*</span></label>
		
			<select name="lstYearsTeachExperience" id="lstYearsTeachExperience">
				<option value="0">-SELECT-</option>
				
					<?= $teachyrsOptions ?>
	      	</select>
	      	
	   </li>
	  
	  <li class="form_set">
	  	<fieldset>
	  		<legend><label for="lstPrefTeachingLevel">Preferred Teaching Level<span class="required">*</span></label></legend>
			<span>
				<select name="lstPrefTeachingLevel" id="lstPrefTeachingLevel" onchange="teachinglevel()">
					<option value="0">-SELECT-</option>
					
						<?= $prefteachinglevelOptions ?>
		      	</select>
		    </span>
	    	<span>
			     <?php echo  FormHelpers::CreateTextBox('txtOtherPrefTeachingLevel', $txtOtherPrefTeachingLevel,
							   array(
							   'class'     => 'text',
							   'size'      => 45,
					           'maxlength' => 50,
							   'disabled' => ($lstPrefTeachingLevel != 9)
							 )
						 );
					?>
			</span>
		</fieldset>
	  </li>
  </ul>
  
 </div>

  

