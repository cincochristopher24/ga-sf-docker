<?php
	
	require_once("Class.Template.php");
	require_once("travellog/factory/Class.FileFactory.php");

	class ViewGroupSteps{
		
		private $mGroup	=	NULL;
		private $mSteps = array(
			'step1' => FALSE,
			'step2' => FALSE,
			'step3' => FALSE,
			'step4' => FALSE,
			'step5' => FALSE,
			'step6' => FALSE,
			'step7' => FALSE,
			'step8' => FALSE,
			'step9' => FALSE,
			'step10' => FALSE
		);
		private $mRedirectToPhoto = TRUE;
		
		public function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		
		public function setSteps($stepsArray=array()){
			$stepsArray = (is_array($stepsArray)) ? $stepsArray : array();
			$this->mSteps = array_merge($this->mSteps, $stepsArray);
		}
		
		public function setPhotosVideosLink($hasPhotos){
			// if group has photos redirect to videos else redirect to photos
			$this->mRedirectToPhoto = ($hasPhotos) ? FALSE : TRUE;
		}
		
		public function render(){
			$stepsComplete = ($this->mSteps['step1'] && $this->mSteps['step2'] && $this->mSteps['step3'] && $this->mSteps['step4'] && $this->mSteps['step5'] && 
							$this->mSteps['step6'] && $this->mSteps['step7'] && $this->mSteps['step8'] && $this->mSteps['step9'] && $this->mSteps['step10']);
			if(!$stepsComplete || $this->mGroup->showGroupSteps()){	
	 			$jscode = <<<BOF
					<script type="text/javascript">
						var initiallyHidden = true;
						var hideShowClicked = function(){
							jQuery('#stepOptionText').text('');
							jQuery('#hideShowLoader').show();
							jQuery('.stepOption').unbind('click');
							if(jQuery('.stepOption').is('.stepOptionShow')){
								hideSteps({id: {$this->mGroup->getGroupID()}, showSteps: 1, mode: 'temp_show'});
							}else{
								hideSteps({id: {$this->mGroup->getGroupID()}, showSteps: 0, mode: 'temp_hide'});
							}
							return false;
						}
						var hideSteps = function(params){
							jQuery.ajax({
								url: '/ajaxpages/unshowSteps.php',
								data: 'groupid='+params.id+'&showSteps='+params.showSteps,
								type: 'POST',
								success: function(html){
									if(params.mode == 'permanent'){
										jQuery('.stepContainer').hide();
									}else if(params.mode == 'temp_show'){
										jQuery('#stepOptionText').text('Hide Steps');
										jQuery("#thisCont").show();
										jQuery("#stepText").show();
										jQuery('.stepOption').toggleClass('stepOptionShow');
									}else{
										jQuery('#stepOptionText').text('Show Steps');
										jQuery("#thisCont").hide();
										jQuery("#stepText").hide();
										jQuery('.stepOption').toggleClass('stepOptionShow');
									}
								},
								complete: function(){
									jQuery('#hideShowLoader').hide();
									jQuery('.stepOption').bind('click', hideShowClicked);
									if(params.mode == 'temp_show' && initiallyHidden){
										jQuery("#steps_carousel").show();
										jQuery("#steps_carousel").jcarousel({
											visible: 7
										});
										initiallyHidden = false;
									}
								}
							});
						};
						
						jQuery(document).ready(function(){
							if(jQuery("#steps_carousel").is(':visible')){
								initiallyHidden = false;
								jQuery("#steps_carousel").jcarousel({
									visible: 7
								});
							}
							if(jQuery('a.stepDone').length >= 10){
								hideSteps({id: {$this->mGroup->getGroupID()}, showSteps: 0, mode: 'permanent'});
							}

							jQuery('.stepOption').bind('click', hideShowClicked);
						});
					</script>
BOF;
				//Template::includeDependentJs("/js/jquery.jcarousel.js");
				Template::includeDependent($jscode);
				
				$tpl = new Template();
				$tpl->set("groupID", $this->mGroup->getGroupID());
				$tpl->set("showSteps", $this->mGroup->showGroupSteps());
				$tpl->set("redirectToPhoto", $this->mRedirectToPhoto);
				$tpl->set("steps", $this->mSteps);
				if( isset($GLOBALS['CONFIG']) ){
					$tpl->set("siteName", $GLOBALS['CONFIG']->getSiteName());
				}else{
					$tpl->set("siteName", 'GoAbroad.net');
				}	
				$tpl->out("travellog/views/tpl.ViewGroupSteps.php");
			}
		}
	}