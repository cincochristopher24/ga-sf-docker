<?php
	
	$urlString = 'searchType='.$arrVars['searchType'].'&amp;searchKey='.$arrVars['searchKey'].'&amp;searchKey2='.$arrVars['searchKey2'];
	
?>

<?if (isset($searchResultsPanel) && strlen(trim($searchResultsPanel->fetch()))): ?>
	<form action="invitemembers.php" method="POST">
	<div id="statusPanel4" class="widePanel" style="display: none; width: 100%; padding-top: 10px;" >
	<p class="loading_message">
		<span id="imgLoading">
		<img alt="Loading" src="/images/loading.gif"/>
		</span>
		<em id="statusCaption">Loading data please wait...</em>
	</p>
	</div>
	<div id="search_results_panel" class="SearchPanel_box">		
			<?= $searchResultsPanel ?>
	</div>
	
	<? if ( count($arrVars['arrResultID']) || count($arrVars['arrResultEmail']) ) : 
	 
		$showControl = $searchResultsPanel->get('showControl');
		$urlString = $searchResultsPanel->get('urlString');
	
		if ($showControl): 
			if ('txtEmail' == $arrVars['searchType']) : ?>
			<div id="InvMsgContainer">
			   <label>Invitation Message:</label>
			   <textarea id="txaInvitationMessage" name="txaInvitationMessage" cols="40" rows="10"></textarea>
			</div>
			<?endif;?>
			<div class="memSearch_buttons">		
				<input type="hidden" name="urlString" value="<?=$urlString?>">
				<input type="hidden" name="mode" value="inviteAllMultiple">
				<input type="hidden" name="viewType" value="8">
				<input type="hidden" name="grpID" value="<?=$grpID?>">
				<input type="submit" name="submit" value="Send Invitation">
			</div>
			
		<?endif;?>
		<div class="clear"> </div>
	<?endif;?>
	</form>
<?else:

	if (strlen(trim($arrVars['searchKey'])) || strlen(trim($arrVars['searchKey2'])))
		echo '<b>'.$arrVars['searchKey']. ' ' . $arrVars['searchKey2'] . '</b>: User not found!';
	else if (strlen(trim($arrVars['searchKeyUName'])))
		echo '<b>'.$arrVars['searchKeyUName']. '</b>: User not found!';
 endif; ?>