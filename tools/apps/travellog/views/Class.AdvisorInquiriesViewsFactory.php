<?php
	
	/*
	 * Class.AdvisorInquiriesViewsFactory.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
	 
	require_once("Class.Constants.php");
	 
	class AdvisorInquiriesViewsFactory{
	
	 	static $instance = NULL;
		private $views = array();
		
		static function getInstance(){ 
			if( self::$instance == NULL ) self::$instance = new AdvisorInquiriesViewsFactory; 
			return self::$instance;
		}
		
		function createView( $view = constants::VIEW_MESSAGE_LIST){
			
			switch($view){
				case constants::VIEW_LISTINGS_LIST:
					if( !array_key_exists("ViewListingsList", $this->views) ){
						require_once("travellog/views/Class.ViewListingsList.php");
						$this->views["ViewListingsList"] = new ViewListingsList;
					}
					
					return $this->views["ViewListingsList"];
					break;
				
				case constants::VIEW_STUDENTS_LIST:
					if( !array_key_exists("ViewStudentsList", $this->views) ){
						require_once("travellog/views/Class.ViewStudentsList.php");
						$this->views["ViewStudentsList"] = new ViewStudentsList;
					}
					
					return $this->views["ViewStudentsList"];
					break;
				
				case constants::PAGING:
					if( !array_key_exists("Paging", $this->views) ){
						require_once("travellog/views/travelers/Class.PagingListsView.php");
						$this->views["Paging"] = new PagingListsView;
					}
					return $this->views["Paging"];
					break;
					
				case constants::VIEW_MESSAGES_PER_STUDENT_AND_LISTING:
					if( !array_key_exists("Paging", $this->views) ){
						require_once("travellog/views/travelers/Class.PagingListsView.php");
						$this->views["Paging"] = new PagingListsView;
					}
					return $this->views["Paging"];
					break;							
					
				default:
					if( !array_key_exists("ViewMessageList", $this->views) ){
						require_once("travellog/views/Class.ViewAdvisorCommunications.php");
						$this->views["ViewMessageList"] = new ViewAdvisorCommunications;
					}
					return $this->views["ViewMessageList"];			
			}
			
		}
	
	}	 
?>
