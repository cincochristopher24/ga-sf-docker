<?php

//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('layoutID', 'page_photos');
	
	
//Template::includeDependentJs ( "/js/jquery-1.2.6.js" );
//Template::includeDependentJs ( "/js/jquery-1.1.4.pack.js"           );
		
 //Template::includeDependentJs ( "/js/yahooUI/carousel/yahoo-dom-event.js"           );
 //Template::includeDependentJs ( "/js/yahooUI/carousel/utilities.js"           );
//Template::includeDependentJs ( "/js/yahooUI/carousel/dragdrop-min.js"           );
 //Template::includeDependentJs ( "/js/yahooUI/carousel/container_core-min.js"           );
 //Template::includeDependentJs ( "/js/yahooUI/carousel/carousel.js"           );

 //Template::includeDependentCss( "/js/yahooUI/carousel/carousel.css"               );
			
//Template::includeDependentCss("/css/jquery.jcarousel.css");//jQuery plugin added by Jul to replace YahooUI Carousel
//Template::includeDependentJs("/js/jquery.jcarousel.js");
Template::includeDependentJs("/min/f=js/prototype.js");
Template::includeDependentJs("/js/jcarousel-lite.js",array('bottom'=> true));

Template::includeDependentJs (array("/js/PhotoUILayout.js"));

$cache 	= NULL;
$photo		= NULL;

 if(count($ColPhotos)>0){
		 $cache 	= $ColPhotos[0]->getContext()."_".$ColPhotos[0]->getPrimaryPhoto()->getPhotoID()."_".$ColPhotos[0]->getGenID();
		 $param	= 'action=onloadgallery&layout=1&context='.$ColPhotos[0]->getContext().'&loginid=0&genid='.$ColPhotos[0]->getGenID().'&pID='.$ColPhotos[0]->getPrimaryPhoto()->getPhotoID();
		 $photo =<<<BOF
			 	<script type = "text/javascript">
				 	GAJsCache['$cache'] = '$cache';
				 	jQuery(document).ready(function(){
				 		var params = '$param';
				 		new Ajax.Request('/ajaxpages/PhotoUILayout.php', {method: 'get', parameters: params, asynchronous:true, evalScripts:true,
								onComplete:function(transport){
							 		jQuery("#cache_element").data('$cache',  transport.responseText);
							 		GAJsCache[$cache] = "$cache";	
								}
							});
				 		
				 		
			   		});
		 	</script>
BOF;
 }	

$photo =$photo.<<<BOF
	<script type = "text/javascript">
		jQuery.noConflict();
		PhotoService = new mPhotoService('profile', 0, $ga_tID);
		PhotoService.setLayout(1); 
		//jQuery("#photogallery").ready(function(){    
		//	PhotoService.loadgallery(null,0);
		//});
	</script>
BOF;

Template::includeDependent($photo);

?>

<? echo $profile_header->render(); ?>
		
<?= $subNavigation->show();  ?>

<div id="rundown">
	<!-- Begin Content Wrapper -->
	<div id="content_wrapper">
				
			<? if(count($ColPhotos)>0):?>	
				
						<!-- Begin Wide Column -->
						<div id="wide_column" class="layout2">
							<div class="section" id="photo_main_view">
								<h1>
									<span id="albumheader"><? echo stripslashes($ColPhotos[0]->getTitle()) ;?></span>
								</h1>
								<div class="content">
									<div id="gallerycontainer">
											
											<? echo $galleryPhotos;?>
											
									</div>
									
								</div>
								<!-- End Wide Column -->
								<!-- Begin Narrow Column -->
							</div>
						</div>
						<!-- end Wide Column -->
						
						<div id="narrow_column">
							<!-- Begin Photo Collections -->
							<div class="section nbsection">
								<h2> <span>Collections</span> </h2>
								<div class="content">
									<ul class="photo_collections">
										
										<? if(count($ColPhotos)):?>
													<? 
													    $i = 0;
													    foreach($ColPhotos as $photo): 
													        $i++;
													        if (1 == $i) {
													            $class = ' class="active"';
													        } else {
													            $class = '';
													        }

															$photoTitle = addslashes('<span>'.$photo->getTitle().'</span>');

															if('journal'==$photo->getContext()){
																$photoTitle .= addslashes("<span class='header_actions'><a href='journal-entry.php?action=view&travelID={$photo->getGenID()}'> Go to Journal</a></span>");
															}
													        $onclick = "PhotoService.loadCollection('{$photo->getContext()}', {$photo->getPrimaryPhoto()->getPhotoID()}, 'gallerycontainer', {$photo->getGenID()}, '$photoTitle')";
															$photocount = $photo->getNumPhotos();
													?>
														<li<?=$class?>>
															<a class="album_name" href="javascript:void(0)" onclick="<?=$onclick?>">
																<img width="65px;" height="65px;" src="<? echo $photo->getPrimaryPhoto()->getPhotolink('thumbnail') ?>" />
															</a>
															<div class="details">
															<strong><a href="javascript:void(0)" onclick="<?= $onclick;?>"> <? echo stripslashes($photo->getTitle()) ?> </a></strong>
															<p> 
																<?=(1 < $photocount)? $photocount." Photos" : $photocount." Photo"?>
															</p>
															</div>
														</li>
													<? endforeach; ?>
										<? endif; ?>
											
									</ul>
								</div>
							</div>
							<!-- End Photo Collections -->
							
						</div>
						<!-- End Narrow Section -->
						
						<div id="cache_element"></div>
						
			<? else: ?>
				<p class="help_text"> <span>You have not yet uploaded photos</span>. You may add photos to one of your <a href="/journal.php?action=add">Entries</a> or <a href="/journal.php?action=add">Create a new Photo journal</a>.</p>
			
			<? endif;?>	
	</div>
	<!-- End Content Wrapper -->
</div>
<!--  End Rundown -->





	
	