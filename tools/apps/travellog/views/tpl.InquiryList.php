<?
/**
* <b>Template Inquiry List</b>
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>
<?= $maintitle ?>
</title>
<link rel="stylesheet" type="text/css" href="/travellog/css/adhoc.css" media="screen" />
<script src="/travellog/js/messages.js" type="text/javascript"></script>
</head>
<body>
<?php include('travellog/views/tpl.IncHeader.php'); ?>
<div id="wrapper1">
	<div id="wrapper2">
		<div id="maincol">
			<div id="leftcol">
				<div id="quick_tasks">
					<h2>Quick Tasks</h2>
					<a href="#">Return to Passport</a> </div>
			</div>
			<div id="centercol">
							<table cellpadding="4" cellspacing="0" border="0" width="100%">
								<thead>
								<tr>
									<th width="20%">Date</th>
									<th width="40%">Client</th>
									<th width="40%">Listing</th>
								</tr>
								</thead>
								<tbody>
								<? foreach($inquiries as $inquiry): ?>
								<tr>
									<td>
										<?= $inquiry->getDateCreated() ?>
									</td>
									<td> <a href="http://<?= $_SERVER['SERVER_NAME'] ?>/Login/Inquiry.php?action=view&amp;inquiryID=<?= $inquiry->getInquiryID() ?>">
										<?= $inquiry->getClientName() ?>
										</a> </td>
									<td>
										<?= $inquiry->getListingName() ?>
									</td>
								</tr>
								<? endforeach; ?>
								</tbody>
							</table>
				<a href="/passport.php">Return
				to Passport</a> </div>
		</div>
	</div>
</div>
</body>
</html>
