<? 

	/**
	 * tpl.ViewIndividualMessage.php
	 * @author : marc
	 * Oct 20, 2006
	 **/
	 
	require_once('Class.GaDateTime.php');
	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/helper/DisplayHelper.php');
	require_once('Class.Constants.php');
	$d = new GaDateTime();
	
	$sessionManager = new SessionManager();
	
	$mine = ($msg->getSource() instanceof Traveler && $msg->getSource()->getTravelerID() == $sessionManager->get('travelerID')) || ($msg->getSource() instanceof Group && $msg->getSource()->getAdministrator()->getTravelerID() == $sessionManager->get('travelerID')) ? true : false; 
	$highlight = $mine ? ' mine' : '';
	
?>
	<div class="msg">	

		<div class="msgheader<?= $highlight; ?>" id="msgID_<?= $msg->getAttributeID(); ?>" onclick="javascript: SLController.changeInquiryCount('<?= $inquiryView; ?>',<?= $msg->getAttributeID(); ?>,<?= $msg->getListingID(); ?>,<?= $msg->getSource()->getSendableID(); ?>); ">
			<?= substr($msg->getTitle(),0,50) . "..."; ?>&nbsp;|&nbsp;
			From : 
				<?= displayLink($msg->getSource(), $sessionManager->get('travelerID'), constants::INQUIRY); ?>
				&nbsp;|&nbsp;
			To : <? $currIndex = 1;
					foreach( $msg->getDestination() as $dest){
						echo displayLink($dest, $sessionManager->get('travelerID'), constants::INQUIRY);
						if( $currIndex != count($msg->getDestination()) )
							echo ";";
						$currIndex++;	
					} ?>&nbsp;|&nbsp;
			<?= $d->set($msg->getCreated())->friendlyFormat(); ?> 
			<div class="reply" id="new_div_<?= $msg->getAttributeID(); ?>"><? if( !$msg->isRead() && !$mine ) : ?>new<? endif; ?></div> 
		</div>		
		<div class="msgcontent" id="msg_content_<?= $msg->getAttributeID(); ?>" style="display: none">
			<?= NL2BR($msg->getText()); ?> 
			<? if( !$mine ) : ?> 
				<div class="reply"><a href="messages.php?reply&messageID=<?= $msg->getAttributeID(); ?>"> Reply to message </a></div> 
			<? endif; ?>
		</div>	
	</div>
		
