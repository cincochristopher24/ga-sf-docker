<?php
require_once ('Class.HtmlHelpers.php');
?>
<ul>			
	<?php 
		$fields = $surveyForm->getSurveyFormFields();	
		foreach($fields as $iFld):
	?>
		<li>
			<h3 <?php if(SurveyField::SECTION_BREAK == $iFld->getFieldType()){echo 'class="sectionBreak"';} ?>>
				<?= $iFld->getCaption() ?>
				<?php if($iFld->isRequired()):?>
					<span class="required">*</span>
				<?php endif; ?>
			</h3>
			<?php if (array_key_exists($iFld->getSurveyFieldID(), $mappedSurveyFieldAnswers)): ?>
					<?php if($iFld->isAnswerable()): ?>		
							<?php if($iFld->isWithOptions()): ?>
								<div>
									<ul>
										<?php foreach($mappedSurveyFieldAnswers[$iFld->getSurveyFieldID()] as $iVal): ?>
								 			<li><?= $iVal ?></li>
								 		<?php endforeach; ?>
									</ul>
								</div>
							<?php else: ?>
							 	<div>
							 		<?php foreach($mappedSurveyFieldAnswers[$iFld->getSurveyFieldID()] as $iVal): ?>
							 			<?= HtmlHelpers::Textile($iVal) ?>
							 		<?php endforeach; ?>
							 	</div>
							<?php endif; ?>				 	
					<?php endif; ?>
			<?php endif; ?>
		</li>
	<?php endforeach; ?>	
</ul>