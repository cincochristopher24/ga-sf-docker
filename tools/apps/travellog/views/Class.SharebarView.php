<?php

	require_once("travellog/views/Class.BookmarkControlsView.php");

	class SharebarView extends BookmarkControlsView{
		
		function render(){
			$this->prepareURLParameters();
			
			$linksTpl = new Template();
			$linksTpl->set("contents",$this->contents);
			
			$this->obj_template->set('linksTpl',$linksTpl);
			$this->obj_template->set('label',isset($this->contents['label']) ? $this->contents['label'] : "Share this:");
			return $this->obj_template->fetch('tpl.SharebarView.php');
		}
	}