<?php
/**
 * Created on Apr.17.07
 *
 * @author Daphne 
 * Purpose:  template for View Search / Invite Members Panel (Search, Invite)
 */
  
?>
<div id="invite" class="member_box_content_inner_wrap">
	<div id="search_forms">
		<div class="form_division">
			<form name="frmAddMember" action="members.php?mode=invite_multiple&amp;gID=<?=$group->getGroupID()?>" class="interactive_form" method="post">
				<h2>Multiple Invitation By Email</h2>
				<ul class="form">
					<li>
						<fieldset>
						<span>
							<textarea name="txaEmail" id="txaEmail" cols="20" rows="10"><?=$txaEmail?></textarea>
								<label for="txtMemail">Email Address (separated by a comma)</label>
						</span>
						<span class="actions">
							<input type="button" name="btnInviteMultiple" onclick="manager._inviteMultiple('mode=invitemultiple&amp;grpID=<?=$group->getGroupID()?>');" class="submit" value="Invite"  >
						</span>
						</fieldset>
					</li>
				</ul>
			</form>
			<div class="clear"></div>
		</div>	
	</div>
	
	<? if (GroupMembersPanel::$INVITEMEMBERSFORM == $memberTabType) : ?>
		<div id="search_results">
			<h1>Add members by email</h1>
			<p class="help_text"><!--<b>Some dummy help text here for users.</b> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed purus sem, dignissim et, dictum sit amet, commodo faucibus, libero. Nunc erat lectus, laoreet scelerisque, vehicula ut, ultrices ac, ante. Nullam suscipit vulputate dui. Suspendisse feugiat. Donec nonummy. Donec vitae massa. Morbi eget leo. Fusce quis enim vitae velit interdum vulputate. Quisque felis. Donec in sapien. Sed a tellus vel mi varius ullamcorper. In mi dolor, vehicula vitae, commodo vel, viverra sit amet, turpis. Mauris vel est. Fusce dui. Proin dignissim, nisi sit amet euismod imperdiet, metus lectus sollicitudin leo, vel cursus enim nunc id enim. Morbi hendrerit ipsum elementum tellus. Pellentesque semper dui in dui.-->
			</p>	
		</div>
	<? endif; ?>
</div>
<div class="clear"></div>