<?php
require_once("Class.Criteria2.php");
require_once('Class.StringFormattingHelper.php');
// Added a custom title & meta description - icasimpan Apr 25,2007
Template::setMainTemplateVar('title', $countriesry_name .' Travel Journals, Travel Blogs and Travel Experiences of GoAbroad Network Travelers');
Template::setMainTemplateVar('metaDescription', 'View the ' .$country_name. ' travel blogs and journals of GoAbroad Network travelers and be inspired to do the same.');  

Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
//Template::includeDependentCss('/css/journal.css');
Template::setMainTemplateVar('layoutID', 'journals');
if($objSubNavigation->getDoNotShow())
	Template::setMainTemplateVar('page_location', 'Journals');
else
	Template::setMainTemplateVar('page_location', 'My Passport');
 
echo $objSubNavigation->show();	
$criteria  = new Criteria2();
$criteria1 = new Criteria2();
$criteria->setOrderBy('travellogID DESC');
$criteria1->setOrderBy('arrival, tripID');

require_once('travellog/model/Class.HelpText.php');
$obj_help = new HelpText();

?>


<div class="area<? if( !$objSubNavigation->getDoNotShow() ) : ?> mine<? endif; ?>" id="intro">
	asdfasdfasdf
	<div class="section">
		<div class="content">
			<?= HtmlHelpers::Textile($obj_help->getHelpText('journals-page-introduction')) ?>
			<? if( $formvars['mytravels'] ): ?>

				<ul class="actions"><li><a href="/travel.php?action=add" class="button">+ Add a new Travel Journal</a></li></ul>
			<? endif; ?>
		</div>
	</div>
</div>

<div id="container">
      <div id="j_content">

        <div id="j_content_wrap">
          <div id="j_options">
            <? if( !$formvars['mytravels'] ): ?>
	            <ul id="top_tabs">
	              <? if ($formvars['locationID'] === 0):?>
	              	<li class="top_1" id="sel"><span class="newest_jor1"><strong>Newest Journals</strong></span></li>
	              <? else: ?>
	              	<li class="top_1"><span class="newest_jor3"><a href="/journal.php">Newest Journals</a></span></li>
	              <? endif; ?>
	              
		              <li class="top_2" <? if ($formvars['locationID'] !== 0):?>id="sel"<? endif; ?>>
		              	<form action="journal.php" method="get">
			               
			                
			      <? if ($formvars['locationID'] === 0):?>
			                <fieldset class="fieldset1">
	              	        <span class="newest_jor4">
			                <select name="countryID" class="countries">
								<? foreach( $collectionCountry as $objCountry ): ?>
			                		<option value="<?=$objCountry->getLocationID()?>" <?if( $objCountry->getLocationID()==$formvars['locationID'] ) echo 'selected="selected"';?> ><?=$objCountry->getName()?></option>
			                	<? endforeach; ?>
			                	<option value="0" <?if( 0==$formvars['locationID'] ) echo 'selected="selected"';?>>Choose a Country </option>	
			                </select>
			                </span> <span class="actions_jour">
			                <input id="btnFilter" name="btnFilter" value="Show Journals" class="jour_submit" type="submit" />
			                </span>
			                 </fieldset>
			               
	              <? else: ?> 
	                       <fieldset class="fieldset2">
	                       <span class="newest_jor6">
			                <select name="countryID" class="countries">
								<? foreach( $collectionCountry as $objCountry ): ?>
			                		<option value="<?=$objCountry->getLocationID()?>" <?if( $objCountry->getLocationID()==$formvars['locationID'] ) echo 'selected="selected"';?> ><?=$objCountry->getName()?></option>
			                	<? endforeach; ?>
			                	<option value="0" <?if( 0==$formvars['locationID'] ) echo 'selected="selected"';?>>Choose a Country </option>	
			                </select>
			                </span> <span class="actions_jour">
			                <input id="btnFilter" name="btnFilter" value="Show Journals" class="jour_submit" type="submit" />
			                </span>
			                 </fieldset>
	              	   
	              <? endif; ?>
			            
			                
		                </form>
		              </li>
	            </ul>
 			<? endif; ?>
            <div class="clear"></div>

          </div>
          <div id="journals_list">
          <div class="journal_header"><span>&nbsp</span></div>
          
            <? if( $objPaging != NULL ): ?>
	      		<? 
	      			$objObjectIterator->rewind();
	      			while( $objObjectIterator->valid() ): ?>
			      	<div class="journal_box">
						
						<?
					   		$entry_lists       = '';
					   		$ctr               = 0;
					   		$exceedFromTotal   = false;
					   		$arrTrips          = $objObjectIterator->current()->getTrips($criteria1);
					   		$ctrTrips          = count($arrTrips);
					   		$latestLocation    = NULL;
					   		$latestEntry       = NULL;
					   		$latestDescription = NULL;
					   		$latestDate        = NULL;
					   		$latestCountry     = NULL;
					   		$arr_journals      = array();
					   		$collectionLogs    = $objObjectIterator->current()->getJournalLogs();
					   		$jID               = array();      
					   		if( $ctrTrips ){
						   		//foreach( $arrTrips as $objTrips ){
						   		for($i = ($ctrTrips-1); $i >= 0; $i--){	
						   			$arrJournalEntry = $arrTrips[$i]->getTravelLogs($criteria);
						   			foreach( $arrJournalEntry as $objJournalEntry ){
						   				$arr_journals[$objJournalEntry->getTravelLogID()] = $objJournalEntry;
						   				if( $ctr < 2 ){
						   					$count_photos = ( $objJournalEntry->countPhotos() )? '&nbsp;|&nbsp;'.$objJournalEntry->countPhotos():'';
						   					$entry_lists .= '<div class="entry_list"><a href="/journal-entry.php?action=view&travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'&nbsp;&nbsp;</a><p class="entry_date_photo">'.date('d F',strtotime($objJournalEntry->getTrip()->getArrival())).$count_photos;
						   					//$entry_lists .= '<div class="entry_list"><a href="/journal-entry.php?action=view&travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'&nbsp;&nbsp;</a><p class="entry_date_photo">'.date('d F',strtotime($objJournalEntry->getTrip()->getArrival())).' &nbsp;|&nbsp;'.$objJournalEntry->countPhotos();
						   					if( $objJournalEntry->countPhotos() > 1 ){
						   						if( array_key_exists($objJournalEntry->getTravelLogID(), $collectionLogs) ){
						   							$entry_lists .= ' Photos<span class="span_new"><img src="/images/icons_new.gif"/></span>';
						   							$jID[]        = $objJournalEntry->getTravelLogID();
						   						}else
						   							$entry_lists .= ' Photos';
						   					}elseif( $objJournalEntry->countPhotos() ){
						   						if( array_key_exists($objJournalEntry->getTravelLogID(), $collectionLogs) ){
						   							$entry_lists .= ' Photo<span class="span_new"><img src="/images/icons_new.gif"/></span>';
						   							$jID[]        = $objJournalEntry->getTravelLogID();
						   						}else
						   							$entry_lists .= ' Photo';
						   					}
						   					$entry_lists .=	' </p></div>';
						   				}else
						   						$exceedFromTotal = true;
						   				$ctr++;
						   			}
						   			/*if( $ctr < 5 ){
							   			$arrJournalEntry = $arrTrips[$i]->getTravelLogs($criteria);
							   			foreach( $arrJournalEntry as $objJournalEntry ){
							   				if( $ctr < 5 )
							   					$entry_lists .= '<div class="entry_list"><a href="/journal-entry.php?action=view&travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'&nbsp;&nbsp;</a><p class="entry_date_photo">'.$objJournalEntry->getTrip()->getArrival().' &nbsp;|&nbsp; 0 Photos</p></div>';
							   				else break;
							   				$ctr++;
							   			}
						   			}else{
						   				$exceedFromTotal = true;
						   				break;
						   			}*/	
						   		}
						   		$objTrips = $arrTrips[$ctrTrips-1];
						   		if( is_object($objTrips->getLocation()) ){
									if( strlen(trim($objTrips->getLocation()->getName())) ){
										$latestLocation = $objTrips->getLocation()->getName();
										$latestCountry  = $objTrips->getLocation()->getCountry()->getName();
										if( $latestLocation != $latestCountry){
											$latestCountry = ', '. $latestCountry;
										}else{
											$latestCountry = NULL;
										}
									}
								}
						   		$arrJournalEntry = $objTrips->getTravelLogs($criteria);
						   		$objJournalEntry = $arrJournalEntry[0];
						   		//foreach( $arrJournalEntry as $objJournalEntry ){
					   				$latestEntry       = '<a href="/journal-entry.php?action=view&travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'</a>';
					   				$latestDescription = $objJournalEntry->getDescription();
					   				$latestDate        = $d->set($objJournalEntry->getTrip()->getArrival())->friendlyFormat();
					   			//}
					   		}else
					   			$entry_lists .= 'No Entries Available';
						?>
						
						<div class="journal_box_wrap">
							<div class="top_headers">
								<div class="about_user">
								
								
									<?
									//===============================================================================//
									if( $objObjectIterator->current()->getTraveler()->isAdvisor() ){
										$groupID            = AdminGroup::getAdvisorGroupID( $objObjectIterator->current()->getTravelerID() );
										$objAdminGroup      = new AdminGroup($groupID);
										$trav               = NULL;
										$username           = $objAdminGroup->getName();  
									}else{
										$objTravelerProfile = $objObjectIterator->current()->getTraveler()->getTravelerProfile();
										$trav 				= $objObjectIterator->current()->getTraveler()->getTravelerType();
										$username           = $objTravelerProfile->getUserName();
									}	
									
								
								?>
								
							
								<div class="div_username2">	
									<? if( $objObjectIterator->current()->getTraveler()->isAdvisor() ): ?>
								
										<img src="<?=$objAdminGroup->getGroupPhoto()->getPhotoLink()?>" width="37p" height="37px" />
									  </div>
								 
										<span class="para_one"><a href="/group.php?gID=<?=$groupID?>"><?=$username?></a></span>
								 
								  
									<?else:
										$_tmpUsernamePossessive = (strlen($username)-1 == strripos($username,'s'))?$username."'":$username."'s";
									 ?>										
									  
									    <a href="/<?=$username?>" title="Read more of <?=$_tmpUsernamePossessive?> travel experiences and adventure!"><img src="<?=$objObjectIterator->current()->getTraveler()->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink()?>" width="37p" height="37px" alt="Traveler: <?=$username?>" /></a>
		  							  </div>
										<span class="para_one"><a href="/profile.php?action=view&amp;travelerID=<?=$objObjectIterator->current()->getTravelerID()?>" title="Read more of <?=$_tmpUsernamePossessive?> travel experiences and adventure!"><?=$username?></a></span>
									    
									<?endif;?>
									<br />
									<span class="para_two"><?=$trav?></span>
								</div>
								
								<h2><a href="/journal-entry.php?action=view&amp;travelID=<?=$objObjectIterator->current()->getTravelID()?>">
									<?
										echo $objObjectIterator->current()->getTitle();
									?>
								</a></h2>
								
								
						
					
								
								
								<!--<div class="j_actions"><a href="/journalfeed.php?jid=$objObjectIterator->current()->getTravelID()"><img src="images/rss_small.gif" title="Subscribe to this journal" alt="rss feed icon" class="rss_icon" /></a></div>-->
							</div>
						
							
						<div class="top_container">	
							
						<div class="entry_box">
						<div class="inside_entry">
			
							   <?= $entry_lists ?>
					
						   <? if( $exceedFromTotal ): ?>
						   <? endif; ?> 
					       </div>
					       	<div class="clear"></div>	

  				    		  <div class="inside_entry">
  				    	    <?
					    	
					    	$ctr1 = 0;
					  				    	
					    	foreach( $collectionLogs as $objLogs ){
					    
					    		if( array_key_exists( $objLogs->getTravellogID(), $arr_journals) && !in_array($objLogs->getTravellogID(), $jID) ){
					    			$obj_jounral_entry = $arr_journals[$objLogs->getTravellogID()];
					    			echo '<div class="inside_date_photo"><p class="threedots"><img src="images/threedots.gif" /></p></div>';
					    			echo '<div class="entry_list"><a href="/journal-entry.php?action=view&travellogID='.$obj_jounral_entry->getTravelLogID().'">'.$obj_jounral_entry->getTitle().'</a>';
					    			echo '<p class="entry_date_photo">'.date('d F',strtotime($obj_jounral_entry->getTrip()->getArrival())). '&nbsp;|&nbsp;' . $obj_jounral_entry->countPhotos().' Photo';
					    			
				    				if( $obj_jounral_entry->countPhotos() > 1 )
				    					echo 's<span class="span_new"><img src="/images/icons_new.gif"/></span>';
				    				else
				    			 		echo '<span class="span_new"><img src="/images/icons_new.gif"/></span>';
				    			 	echo '</p></div>';
				    			 	$ctr1++;
					    		}
					    		
					    	}
					    ?></div>
					      <div class="inside_viewall">
					      	<?if( $exceedFromTotal ):
				      			$more_entry = NULL;
				      			if( ($ctr+$ctr1) > (2+$ctr1) ) $more_entry = $ctr - ($ctr1+2);
					      	?>
					  			<?if( $more_entry ):?>
					  				<a class="more" href="/journal-entry.php?action=view&travelID=<?=$objObjectIterator->current()->getTravelID()?>" title="Read <?=$_tmpUsernamePossessive?> <?=$more_entry?> other journals."><?=$more_entry?> More...</a>
					  			<?endif;?> 
					  		<?endif;?>     
					      </div>	
					  		 
						</div>
						
					
						<div class="journal_wrapper">
						
						
						<div class="photo">
						
							<?if( $objObjectIterator->current()->getRandomPhoto() ):?>
							
								
									<a href="/journal-entry.php?action=view&amp;travelID=<?=$objObjectIterator->current()->getTravelID()?>">
										<img src="<?=$objObjectIterator->current()->getRandomPhoto()->getPhotoLink()?>" alt="journal photo">
									</a>
									
								
							<? endif; ?>
						
						
							     	
							
								
                                 
                           		</div>
                         				
						
						    
							
							<div class="info meta">
							
								<?
									//===============================================================================//
									if( $objObjectIterator->current()->getTraveler()->isAdvisor() ){
										$groupID            = AdminGroup::getAdvisorGroupID( $objObjectIterator->current()->getTravelerID() );
										$objAdminGroup      = new AdminGroup($groupID);
										$username           = $objAdminGroup->getName();  
									}else{
										$objTravelerProfile = $objObjectIterator->current()->getTraveler()->getTravelerProfile();
										$username           = $objTravelerProfile->getUserName();
									}	
									
									//===============================================================================//
									/*$collectionTrips = $objObjectIterator->current()->getTrips();
									if( count($collectionTrips) ){
										$arrLocationNames = array();
										foreach( $collectionTrips as $objTrips ){
											if( is_object($objTrips->getLocation()) ){
												if( strlen(trim($objTrips->getLocation()->getName())) ){
													$arrLocationNames[] = $objTrips->getLocation()->getName();
												}
											}
										}
										$arrLocationNames = array_unique($arrLocationNames);
										echo '<span id="journal_location_list">'.implode(', ', $arrLocationNames).'</span><br />';
									}*/
									
									if( strlen(trim($latestEntry)) ) echo '<span id="journal_location_list">'.$latestEntry.'</span>';
								?>
								
								<? if( strlen(trim($latestLocation)) ): ?>
									<br />
									<span class="span_place"><?= $latestLocation.$latestCountry  .'</span> | '.$latestDate?>
									<br />
								<? endif; ?>
								
								<?='Views: '.$objObjectIterator->current()->getViews().'<br />';?>
								 
								<!--- if( $objObjectIterator->current()->getTraveler()->isAdvisor() ): ?>
									<a href="/group.php?gID==$groupID" class="username">=$username</a>
								else:
									<a href="/profile.php?action=view&amp;travelerID==$objObjectIterator->current()->getTravelerID()?>" class="username">=$username</a>
								endif;-->
								
								<div>

								<p>
									<?
										if( strlen(trim($latestDescription)) == 0  ) $latestDescription = $objObjectIterator->current()->getDescription();
										echo nl2br(substr($latestDescription, 0, 250));
										if( strlen(trim($latestDescription)) > 250 ) echo '...'; 	
									?>
								</p>
								
                           
							  		
								
								</div>
								
							</div>
							<div class="clear"></div>
							</div>
								
							
							</div>
						
						
						</div>
						
							<div class="clear"></div>
			        </div>
			       
		        <? 
		        	$objObjectIterator->next();
		        	endwhile; ?>
		        	
		        	<div class="journal_header2"><span>&nbsp</span></div>
	        
	            <div class="clear"></div>
	            <? if ($objPaging->getTotalPages() > 1): ?>
	            	<div class="pagination"> <span class="disabled"><?$objPaging->getFirst()?></span>&nbsp; | <span class="disabled"><?$objPaging->getPrevious()?></span>&nbsp; | <?$objPaging->getLinks()?> | <span><?$objPaging->getNext()?></span>&nbsp; | <span><?$objPaging->getLast()?></span> </div>
	            <? endif; ?>
	        <?else:?>
	        	<p class="help_text"><?=$objHelpText->getHelpText('NO-JOURNALS')?></p>
			<? endif; ?>
          </div>
        </div>
      </div>
      <? if( !$formvars['mytravels'] ): ?>
	      <div id="j_highlights">
	      <h2 id="editors_head"><span class="editor_choice">EDITOR'S CHOICE</span></h2>
	        <div id="editors_choice" class="hcontainers">
	          
	          <? if( count($collectionFeaturedSection) ): ?>
	          	<? foreach($collectionFeaturedSection as $objFeaturedSection): ?>
		          	<div class="journal_box">
		          	
		          	<?
	            		//===============================================================================//
	            		if( $objFeaturedSection->getTraveler()->isAdvisor() ){
	            			$groupID            = AdminGroup::getAdvisorGroupID( $objFeaturedSection->getTravelerID() );
	            			$objAdminGroup      = new AdminGroup($groupID);
	            			$username           = $objAdminGroup->getName();  
	            		}else{
	            			$objTravelerProfile = $objFeaturedSection->getTraveler()->getTravelerProfile();
	            			$username           = $objTravelerProfile->getUserName();
	            		}	
	            		
	            		//===============================================================================//
	            		/*$collectionTrips = $objFeaturedSection->getTrips();
	            		if( count($collectionTrips) ){
	            			$arrLocationNames = array();
	            			foreach( $collectionTrips as $objTrips ){
	            				if( is_object($objTrips->getLocation()) ){
		            				if( strlen(trim($objTrips->getLocation()->getName())) ){
			            				$arrLocationNames[] = $objTrips->getLocation()->getName();
		            				}
	            				}
	            			}
	            			$arrLocationNames = array_unique($arrLocationNames);
	            			echo '<span id="journal_location_cs">'.implode(', ', $arrLocationNames).'<br /></span>';
	            		}*/
	            		$collectionTrips   = $objFeaturedSection->getTrips();
	            		$ctrTrips          = count($collectionTrips)-1;
	            		$objTrips          = $collectionTrips[$ctrTrips];
	            		$latestTitle       = NULL;
	            		$latestLocation    = NULL;
				   		$latestEntry       = NULL;
				   		$latestDate        = NULL;
	            		if( is_object($objTrips->getLocation()) ){
            				if( strlen(trim($objTrips->getLocation()->getName())) ){
	            				$latestLocation = $objTrips->getLocation()->getName();
            				}
        				}
        				$arrJournalEntry = $objTrips->getTravelLogs();
				   		foreach( $arrJournalEntry as $objJournalEntry ){
			   				$latestTitle = '<a href="/journal-entry.php?action=view&travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'</a>';
			   				$latestDate  = $d->set($objJournalEntry->getTrip()->getArrival())->friendlyFormat();
			   			}
	            	?>
		          	
		            <h2>
		            	<?if( strlen(trim($latestTitle)) ):?>
		            		<?=$latestTitle?>
		            	<?else:?>
		            		<a href="/journal.php?action=view&amp;travelID=<?=$objFeaturedSection->getTravelID()?>"><?=$objFeaturedSection->getTitle()?></a>
		            	<?endif;?>
		            </h2>
		
		            <div class="photo"><a href="/journal.php?action=view&amp;travelID=<?=$objFeaturedSection->getTravelID()?>"><img src="<?=$objFeaturedSection->getRandomPhoto()->getPhotoLink()?>" alt="journal photo"></a></div>
		            <div class="info meta">
		            	
		            	<? if( $latestLocation ): ?>	
		              		<div id="journal_date_cs" class="date">
								<?=$latestLocation.'&nbsp;|&nbsp;'.$latestDate?>
							</div>
		              	<?endif;?> 
		              	
		              	<?
		              		echo 'Views: '.$objFeaturedSection->getViews().'<br />';
		              	?>	
		            	
		            	<? if( $objFeaturedSection->getTraveler()->isAdvisor() ): ?>
		            		<a href="/group.php?gID=<?=$groupID?>" class="username"><?=$username?></a>
		            	<?else:?>
		              		<a href="/<?=$username?>" class="username"><?=$username?></a>
		              	<?endif;?>
		              
		            </div>
		          
		            <div class="clear"></div>
		          	</div>
		          	
		          	
	          	<?endforeach;?>
	          <?endif;?>
	          
	        </div>
	        
	        <h2 id="popular_head"><span class="popular_choice">MOST POPULAR</span></h2>
	        <div id="most_pop" class="hcontainers">
	        	
	          	<? if( count($collectionPopularTravels) ): ?>
	          		<? foreach( $collectionPopularTravels as $objPopularTravels ): ?>
		          		<div class="journal_box">
		          			<?
			            		//===============================================================================//
			            		if( $objPopularTravels->getTraveler()->isAdvisor() ){
			            			$groupID            = AdminGroup::getAdvisorGroupID( $objPopularTravels->getTravelerID() );
			            			$objAdminGroup      = new AdminGroup($groupID);
			            			$username           = $objAdminGroup->getName();  
			            		}else{
			            			$objTravelerProfile = $objPopularTravels->getTraveler()->getTravelerProfile();
			            			$username           = $objTravelerProfile->getUserName();
			            		}	
			            		
			            		//===============================================================================//
			            		/*$collectionTrips = $objPopularTravels->getTrips();
			            		if( count($collectionTrips) ){
			            			$arrLocationNames = array();
			            			foreach( $collectionTrips as $objTrips ){
			            				if( is_object($objTrips->getLocation()) ){
				            				if( strlen(trim($objTrips->getLocation()->getName())) ){
					            				$arrLocationNames[] = $objTrips->getLocation()->getName();
				            				}
			            				}
			            			}
			            			$arrLocationNames = array_unique($arrLocationNames);
			            			echo '<span id="journal_location_cs">'.implode(', ', $arrLocationNames).'</span><br />';
			            		}*/
			            		$collectionTrips   = $objPopularTravels->getTrips();
			            		$ctrTrips          = count($collectionTrips)-1;
			            		$objTrips          = $collectionTrips[$ctrTrips];
			            		$latestTitle       = NULL;
			            		$latestLocation    = NULL;
						   		$latestEntry       = NULL;
						   		$latestDate        = NULL;
			            		if( is_object($objTrips->getLocation()) ){
		            				if( strlen(trim($objTrips->getLocation()->getName())) ){
			            				$latestLocation = $objTrips->getLocation()->getName();
		            				}
		        				}
		        				$arrJournalEntry = $objTrips->getTravelLogs();
						   		foreach( $arrJournalEntry as $objJournalEntry ){
					   				$latestTitle = '<a href="/journal-entry.php?action=view&travellogID='.$objJournalEntry->getTravelLogID().'" title="'.$objJournalEntry->getTitle().'">'.$objJournalEntry->getTitle().'</a>';
					   				$latestDate  = $d->set($objJournalEntry->getTrip()->getArrival())->friendlyFormat();
					   			}
			            	
			            	?>
			            	<h2>
				            	<?if( strlen(trim($latestTitle)) ):?>
				            		<?=$latestTitle?>
				            	<?else:?>
				            		<a href="/journal.php?action=view&amp;travelID=<?=$objPopularTravels->getTravelID()?>" title="<?= $latestTitle ?>"><?=$objPopularTravels->getTitle()?></a>
				            	<?endif;?>
				            </h2>
		            		<?if( $objPopularTravels->getRandomPhoto() ):?>
					          	<div class="photo">
					          		<a href="/journal.php?action=view&amp;travelID=<?=$objPopularTravels->getTravelID()?>">
					          			<img src="<?=$objPopularTravels->getRandomPhoto()->getPhotoLink()?>" alt="journal photo">
					          		</a>
					          	</div>
				          	<? endif; ?>
				            <div class="info meta">
				            	
		              			
				            	<? if( $latestLocation ): ?>
				            		<div id="journal_date_cs" class="date">
				            			<?=$latestLocation .'&nbsp;|&nbsp;'. $latestDate?>
				            		</div>
				            	<? endif; ?>
                            
				            	<?
				            	echo 'Views: '.$objPopularTravels->getViews().'<br />';
				            	?>

				            	
				            	<? if(  $objPopularTravels->getTraveler()->isAdvisor() ): ?>
				            		<a href="/group.php?gID=<?=$groupID?>" class="username"><?=$username?></a>
				            	<?else:?>
		              				<a href="/<?=$username?>" class="username"><?=$username?></a>
		              			<?endif;?>
		              			<br/>
		            		</div>
		            		
		            		<div class="clear"></div>
		            		
		          		</div>
	          		<? endforeach; ?>
			  	<? endif; ?>	          
	        </div>
	        <div class="journal_header7"><span>&nbsp;</span></div> 	
	      </div>
	<? endif; ?>
	<div class="clear"></div>
    </div>
	<div class="clear"></div>
  </div>
  <div class="clear"></div>



