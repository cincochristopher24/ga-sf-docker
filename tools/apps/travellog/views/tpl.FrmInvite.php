<?php
/**
 * <b>Invite Friends</b> template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
?>

<table border="1" cellpadding="4" cellspacing="4" width="80%">
	<tr>
		<td colspan="2">
			<h2>Enter Username</h2>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			Enter usernames for the friends you want to invite.
		</td>
	</tr>
	<form name="sform" action="/friend.php"method="post">
		<tr>
			<td width="40%">
				<?
					for( $i=1; $i<=10; $i++ ):
						echo '<input type="text" name="usernames[]" id="usernames'.$i.'" size="30" /><br /><br />';
						if ( $i % 5 == 0 && $i != 10) echo '</td><td width="40%">';
					endfor;
				?>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="left">
				<input type="submit" name="request" id="request" value="Invite Friends" />&nbsp;&nbsp;<? $obj_backlink->GetButton() ?>
			</td>
		</tr>
	</form>
</table> 