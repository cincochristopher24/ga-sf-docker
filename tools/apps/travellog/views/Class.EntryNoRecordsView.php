<?php
require_once("travellog/views/Class.AbstractView.php");
class EntryNoRecordsView extends AbstractView{
	function render(){
		return ( $this->contents )? NULL: '<strong>No entries for this journal.<strong>';
	}
}
?>
 