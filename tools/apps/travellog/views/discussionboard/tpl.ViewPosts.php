<?php
	Template::includeDependentJs('/js/DiscussionBoard.js', array('bottom'=>true));

	require_once('Class.GaDateTime.php');
	require_once('Class.HtmlHelpers.php');
	$gaDateTime = new GaDateTime;
?>

<?php echo $profile->render(); ?>
<?php $sub_navigation->show(); ?>

<div class="layout_2" id="content_wrapper">
	<ul class="breadcrumbs">
		<?php if ($is_subgroup): ?>
			<li class="root">
				<a href="/discussion_board/home/<?php echo $group_id; ?>">
					<span>Discussion Board Home</span>
				</a>
			</li>
		<?php else: ?>
			<li class="root">
				<a href="/discussion_board/home/<?php echo $group_id; ?>">
					<span>Knowledge Base Home</span>
				</a>
			</li>
			<li>
				<a href="/topic/discussions/<?php echo $topic->getId(); ?>">
					<span><?php echo $topic->getTitle(); ?></span>
				</a>
			</li>			
		<?php endif; ?>
		<li>
			<strong><span class="discussion_title"><?php echo $discussion->getTitle(); ?></span></strong>
		</li>
	</ul>
		
	<div class="" id="dboard">					
		<h2 id="pin_header" <?php if ($discussion->isFeatured()) { ?>class="pinned" <?php } ?> >
			<img id="pin_image" <?php if (!$discussion->isFeatured()) { ?>style="display:none;"<? } ?> class="pin_img" src="/images/v3/discussions/dboard-pin.gif" alt="Topic" title="" />												
			
			<span class="discussion_title" >
				<?php echo $discussion->getTitle(); ?>
			</span>
			
			<?php if ($is_admin_logged): ?>
				<span>
					<a id="archive_<?php echo $discussion->getId(); ?>" href="javascript:void(0)" <?php if ($discussion->isArchived()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Archive Discussion ?','Are you sure you want to archive this discussion?', function(){ Discussion.archive(<?php echo $discussion->getId(); ?>); },'Archive','1');CustomPopup.setJS();CustomPopup.createPopup();" >
						Archive
					</a> 										
				
					<a id="activate_<?php echo $discussion->getId(); ?>" href="javascript:void(0)" <?php if($discussion->isActive()){ ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Activate Discussion ?','Are you sure you want to activate this discussion?', function(){ Discussion.activate(<?php echo $discussion->getId(); ?>); },'Activate','1');CustomPopup.setJS();CustomPopup.createPopup();" >
						Activate
					</a> 
					|																																		
					<a id="unpin_action" <?php if (!$discussion->isFeatured()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Unpin this discussion?','Are you sure you want to unpin this discussion?', function(){ Discussion.unfeature(<?php echo $discussion->getId(); ?>, 0); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Unfeature discussion">Unpin this discussion</a>
					<a id="pin_action" <?php if ($discussion->isFeatured()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Pin this discussion?','Are you sure you want to pin this discussion?', function(){ Discussion.feature(<?php echo $discussion->getId(); ?>, 0); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Feature discussion">Pin this discussion</a>
					|
					<a href="javascript:void(0);" onclick="Discussion.edit(<?php echo $discussion->getId(); ?>, jQuery('.discussion_title').html());" >Edit discussion</a>
					<?php if ($is_subgroup AND !(TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING == $privacy || TopicPeer::SUBGROUP_PUBLIC_SETTING == $privacy)): ?>
						|
						<a id="add_knowledge_action_<?php echo $discussion->getId(); ?>" <?php if ($discussion->isAddedToKnowledgeBase()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Knowledge Base','Are you sure you want to add this discussion to the knowledge base?', function(){ Discussion.addToKnowledgeBase(<?php echo $discussion->getId(); ?>, 0); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Add to knowledge base">Add to knowledge base</a>
						<a id="remove_knowledge_action_<?php echo $discussion->getId(); ?>" <?php if (!$discussion->isAddedToKnowledgeBase()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Knowledge Base','Are you sure you want to remove this discussion from the knowledge base?', function(){ Discussion.removeFromKnowledgeBase(<?php echo $discussion->getId(); ?>, 0); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Remove from knowledge base">Remove from knowledge base</a>
					<?php endif; ?>				
				</span>
			<?php endif; ?>			
		</h2>
		
		<?php $first_poster = $first_post->getPoster(); ?>
		<div class="content">			
			<div id="dboard_head">													
				<ul class="meta">
					<li class="first"><?php echo ($total_posts_count - 1); ?> <?php echo (1 < ($total_posts_count - 1)) ? 'Replies' : 'Reply'?></li>
					<li>
						Started by
						<?php if (PosterPeer::TRAVELER == $first_poster->getPosterType()): ?>
							<?php echo $first_poster->getUserName(); ?>
							<?php if ($is_admin_logged): ?>
								<em><?php echo $first_poster->getFullName(); ?></em>
							<?php endif; ?>
						<?php else: ?>
							<?php echo $first_poster->getName(); ?>								
						<?php endif; ?>
					</li>
					<li>Last Post Created <?php echo GaDateTime::descriptiveDifference($discussion->getDateUpdated(), GaDateTime::dbDateTimeFormat()); ?> Ago</li>
				</ul>											
			</div>
				
			<table id="dboard_discussion" cellpadding="0" cellspacing="0" border="0">				
				<tbody id="discussion_body" >
					
					<?php foreach ($posts as $post): ?>
						<?php $poster = $post->getPoster(); ?>
						<tr id="<?php echo $post->getId(); ?>" > 
							<td class="dboard_author_info">
								<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
									<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
										<img src="<?php echo $poster->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');; ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
									</a>
								<?php else: ?>
									<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
										<img src="<?php echo $poster->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="<?php echo $poster->getName(); ?>'s Group Logo" class="pic"/>
									</a>								
								<?php endif; ?>	
								
								<div class="details">
									<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
										<a href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getUserName(); ?>'s travel blogs and travel notes">
											<strong><?php echo HtmlHelpers::truncateWord($poster->getUserName(), 18, ''); ?></strong>
										</a>
							    	<?php if ($is_admin_logged): ?>
							    		<em><?php echo HtmlHelpers::truncateWord($poster->getFullName(), 18, ''); ?></em>
							    	<?php endif; ?>
										
										<?php $city = $poster->getTravelerProfile()->getCity(); ?>
										<?php if (null != $city): ?>
											<br />
											<img width="22" height="11" alt="Travel notes from <?php echo $city->getCountry()->getName(); ?>" src="http://images.goabroad.com/images/flags/flag<?php echo $city->getCountry()->getCountryID(); ?>.gif"/>													
										<?php endif; ?>
									<?php else: ?>
										<a href="<?php echo $poster->getFriendlyUrl(); ?>" title="Join <?php echo $poster->getName(); ?>'s advisor group and share travel blogs and travel notes with its members">
											<?php echo HtmlHelpers::truncateWord($poster->getName(), 18, ''); ?>
										</a>								
									<?php endif; ?>
								</div>								
							</td>
								
							<td class="post">
								<p class="meta">Posted on <?php echo $post->getDateCreated("D M d, Y"); ?></p>
								<p> 
									<?php echo preg_replace('/\n/', '<br />', $post->getMessage()); ?>
								</p>
								<?php if ($is_admin_logged OR $logger_id == $poster->getPosterId()):?>
									<div class="actions">
										Actions &nbsp;
										<a href="/post/edit/<?php echo $post->getId(); ?>">
											<span>Edit Post</span>
										</a> 
										|
										<a href="javascript:void(0)" onclick="CustomPopup.initialize('Delete Post ?','Are you sure you want to delete this post?', '/post/delete/<?php echo $post->getId(); ?>','Delete Post','1');CustomPopup.createPopup();" class="delete" >
											<span>Delete Post</span>
										</a>
									</div>
								<?php endif; ?>
							</td>
						</tr>			
						<tr id="spacer_<?php echo $post->getId(); ?>" class="spacer">
							<td colspan="2">
								<span></span>	
							</td>
						</tr>
					<?php endforeach; ?>													
				</tbody>													
			</table>
			
			<?php if ($can_reply && $discussion->isActive()): ?>
				<div id="dboard_footer">
					<a class="button" href="/post/reply/<?php echo $discussion->getId(); ?>">
						<span>Post a Reply</span>
					</a>									
				</div>
			<?php endif; ?>	
		</div>
		<div class="foot"></div>
	</div>
</div>


<?php if (isset($success)): ?>
	<script type="text/javascript">
		window.onload = function(){
			CustomPopup.initPrompt("<?php echo $success; ?>");
			CustomPopup.createPopup();
		}
	</script>
<?php endif; ?>