<?php Template::includeDependentJs('/js/DiscussionBoard.js', array('bottom'=>true)); ?>
<?php $gaDateTime = new GaDateTime(); ?>

<?php echo $profile->render(); ?>
<?php $sub_navigation->show(); ?>

<div class="layout_2" id="content_wrapper">
	<div id="dboard">			
		<div class="content">												
			<div id="dboard_head">
				<h2>Knowledge Base</h2>
				<ul class="meta">
					<li class="first"><?php echo (0 < $topics_count) ? $topics_count : 'No'; ?> Topic<?php if (1 < $topics_count OR 0 == $topics_count) { ?>s<?php } ?></li>
					<li><?php echo (0 < $discussions_count) ? $discussions_count : 'No'; ?> Discussion<?php if (1 < $discussions_count OR 0 == $discussions_count) { ?>s<?php } ?></li>
					<li><?php echo (0 < $posts_count) ? $posts_count : 'No'; ?> Post<?php if (1 < $posts_count OR 0 == $posts_count) { ?>s<?php } ?></li>
					<?php if (isset($latest_post)): ?>
						<li>Last Post Created <?php echo GaDateTime::descriptiveDifference($latest_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> Ago</li>
					<?php endif; ?>
				</ul>					
				<p>
					<?php echo $kbHelpText ?>
				</p>
				
			</div>				
				
			<div id="dboard_body">	
				<?php if ($is_admin_logged): ?>
					<ul class="post_actions">
						<li>
							<a title="" href="/topic/add/<?php echo $group_id; ?>" class="button">
								Start a New Topic
							</a>
						</li>												
					</ul>
				<?php endif; ?>
				
				<?php if (0 < count($added_discussions)): ?>	
					<input type="hidden" id="hidAddedDiscussionsCount" value="<?php echo count($added_discussions); ?>" >
					<table cellspacing="0" id="addedTable" >
						<thead>
							<tr>
								<th class="first" scope="col">
									<h2>
										Added Discussions
									</h2>
								</th>
								<th scope="col">Replies</th>
								<th scope="col">Started by</th>
								<th class="last" scope="col">Last Reply</th> 
							</tr>
								<tr>
									<td colspan="4" class="description"> 
										<p>These are the discussions added to the knowledge base.</p>
									</td>
								</tr>
						</thead>
						<tbody>
							<?php foreach($added_discussions as $discussion): ?>
								<?php $first_post = $discussion->getFirstPost(); ?>
								<?php $first_poster = $first_post->getPoster(); ?>
								<?php $post_count = $discussion->getPostsCount(); ?>
								<?php if (1 < $post_count): ?>
									<?php $latest_post = $discussion->getLatestPost(); ?>
									<?php $latest_poster = $latest_post->getPoster(); ?>
								<?php endif; ?>	
								
								<tr id="discussion_<?php echo $discussion->getId(); ?>" >
									<th scope="row">
										<h3 >
											<a href="/discussion/posts/<?php echo $discussion->getId(); ?>">
												<?php echo $discussion->getTitle(); ?>
											</a>
										</h3>	
										
										<p><?php echo preg_replace('/\n/', '<br />', $first_post->getMessage()); ?></p>										
										<?php if ($is_admin_logged): ?>
											<div class="actions">
												Actions &nbsp;
												<a id="archive_<?php echo $discussion->getId(); ?>" <?php if ($discussion->isArchived()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Archive Discussion ?','Are you sure you want to archive this discussion?', function(){ Discussion.archive(<?php echo $discussion->getId(); ?>); },'Archive','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0)">
													<span>Archive</span>
												</a> 										
											
												<a href="javascript:void(0)" <? if($discussion->isActive()){ ?>style="display:none;"<? } ?> id="activate_<?php echo $discussion->getId(); ?>" onclick="CustomPopup.initialize('Activate Discussion ?','Are you sure you want to activate this discussion?', function(){ Discussion.activate(<?php echo $discussion->getId(); ?>); },'Activate','1');CustomPopup.setJS();CustomPopup.createPopup();">
													<span>Activate</span>
												</a> 
												|						
												<a id="remove_knowledge_action_<?php echo $discussion->getId(); ?>" onclick="CustomPopup.initialize('Knowledge Base','Are you sure you want to remove this discussion from the knowledge base?', function(){ Discussion.removeFromKnowledgeBase(<?php echo $discussion->getID(); ?>, 0, function(){ var addedCount = parseInt(jQuery('#hidAddedDiscussionsCount').val()); jQuery('#hidAddedDiscussionsCount').val(addedCount-1); if (1 == addedCount) { jQuery('#addedTable').css('display', 'none'); } else { jQuery('#discussion_<?php echo $discussion->getId(); ?>').css('display', 'none'); } }); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Remove from knowledge base">Remove from Knowledge Base</a>							
											</div>
										<?php endif; ?>
									</th>
											
									<td><?php echo ($post_count - 1); ?></td>
									<td>
										<?php if (PosterPeer::TRAVELER == $first_poster->getPosterType()): ?>
											<?php echo $first_poster->getUserName(); ?></a>
											<?php if ($is_admin_logged): ?>
												<br />
												<em><?php echo $first_poster->getFullName(); ?></em>
											<?php endif; ?>
										<?php else: ?>
											<?php echo $first_poster->getName(); ?>							
										<?php endif; ?>
									</td>
									<td class="last"> 
										<?php if (1 < $post_count): ?>
											<a href="/discussion/posts/<?php echo $discussion->getId(); ?>#<?php echo $latest_post->getID(); ?>"><?php echo GaDateTime::descriptiveDifference($latest_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> ago</a> 											 
											<br />
											by 
											<?php if (PosterPeer::TRAVELER == $latest_poster->getPosterType()): ?>
												<?php echo $latest_poster->getUserName(); ?></a>
												<?php if ($is_admin_logged): ?>
													<br />
													<em><?php echo $latest_poster->getFullName(); ?></em>
												<?php endif; ?>
											<?php else: ?>
												<?php echo $latest_poster->getName(); ?>							
											<?php endif; ?>
										<?php endif; ?>										
									</td>																		
								</tr>
							<?php endforeach; ?>																																																																
						</tbody>
					</table>	
				<?php endif; ?>

				<?php foreach ($topics as $topic): ?>
					<?php if (isset($discussion_helpers[$topic->getId()]) || $is_member_logged): ?>
						<table cellspacing="0">
							<thead>
								<tr>
									<th class="first" scope="col">
										<h2>
											<a title="" href="/topic/discussions/<?php echo $topic->getId()?>" class="topic_link"><?php echo $topic->getTitle(); ?></a>
										</h2>
									</th>
									<?php if (isset($discussion_helpers[$topic->getId()])): ?>
										<th scope="col">Replies</th>
										<th scope="col">Started by</th>
										<th class="last" scope="col">Last Reply</th> 
									<?php endif; ?>
								</tr>
								<tr>
									<td colspan="4" class="description"> 
										<p> 
											<?php echo $topic->getDescription(); ?>										
										</p>
									</td>
								</tr>
							</thead>
							<tbody id="discussions_<?php echo $topic->getId(); ?>">		
								<?php if (isset($discussion_helpers[$topic->getId()])): ?>
									<?php if ($discussion_helpers[$topic->getId()]->hasFeaturedDiscussions()): ?>	
										<tr> 
											<td class="divider" colspan="4">
												<div>Pinned Discussions</div>
											</td>
										</tr>														
										<?php $discussion_helpers[$topic->getId()]->renderFeaturedDiscussions(); ?>
									<?php endif; ?>
									
									<?php if ($discussion_helpers[$topic->getId()]->hasUnFeaturedDiscussions()): ?>
										<?php if ($discussion_helpers[$topic->getId()]->hasFeaturedDiscussions()): ?>	
											<tr> 
												<td class="divider" colspan="4">
													<div>Other Discussions</div>
												</td>
											</tr>	
										<?php endif; ?>
										<?php $discussion_helpers[$topic->getId()]->renderUnFeaturedDiscussions(); ?>
									<?php endif; ?>																																																										
								<?php else: ?>
									<tr>
										<td>
											<div>There are no discussions yet. Click <a href="/discussion/add/<?php echo $topic->getId();?>">here</a> to add a new discussion.</div>
										</td>
									</tr>												
								<?php endif; ?>	
							</tbody>
							<?/*php if (isset($topic_discussions_count[$topic->getId()]) AND 3 < $topic_discussions_count[$topic->getId()]): ?>
								<tr class="last">
									<td colspan="4">
										<a title="" href="static_dboard-maingroup-topic.html">
											<strong>View the other <?php echo ($topic_discussions_count[$topic->getId()]-3); ?> discussion<?php echo (1 < ($topic_discussions_count[$topic->getId()]-3)) ? "s" : ""; ?> of this topic →</strong>
										</a>
									</td>
								</tr>										
							<?php endif; */?>	
						</table>	
					<?php endif; ?>
				<?php endforeach; ?>
				<?php if (0 < count($recent_discussions)): ?>
					
					<?// we will not show recently discussed section if we only have 1 recent discussion and that discussion only has 1 post ?>
					<?php if(! (1 == count($recent_discussions) && 1 == $recent_discussions[0]->getPostsCount()) ): ?>
					
					<table cellspacing="0">
						<thead>
							<tr>
								<th class="first">
									<h2>Recently Discussed</h2>
								</th>
								<th>  </th>
								<th>  </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="recent-column" colspan="3">
									<ul class="users recent-list">
										<?php $cnt = 0; ?>						
										<?php foreach($recent_discussions as $discussion): ?>
											
											<?php $latest_post = $discussion->getLatestPost(); ?>
											
											<?php if( is_null($latest_post) ): continue; endif; ?>
											
											<?php $poster = $latest_post->getPoster(); ?>									
											<li>
											
												<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
													<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
														<img src="<?php echo $poster->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');; ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
													</a>
												<?php else: ?>
													<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
														<img src="<?php echo $poster->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
													</a>								
												<?php endif; ?>								
												
												<div class="details">											
											    <strong>
											    	<img title="" alt="pinned" src="/images/v3/discussions/reply-to-discussion.gif" class="pin_img"/>
											    	
														<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
															<a class="username" href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getUserName(); ?>'s travel blogs and travel notes"><?php echo $poster->getUserName(); ?></a>
												    	<?php if ($is_admin_logged): ?>
												    		<em><?php echo $poster->getFullName(); ?></em>
												    	<?php endif; ?>														
														<?php else: ?>
															<a class="username" href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getName(); ?>'s travel blogs and travel notes"><?php echo $poster->getName(); ?></a>								
														<?php endif; ?>	
											    </strong>    									    		
													<?php if (1 == $discussion->getPostsCount()): ?>    									    		
														started a new discussion
													<?php else: ?>	
														replied to
													<?php endif; ?>
										    	<a href="/discussion/posts/<?php echo $discussion->getId(); ?>" class="highlight" title=""><?php echo $discussion->getTitle(); ?></a>	
										  		<em><?php echo GaDateTime::descriptiveDifference($latest_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> Ago</em>	
										  		<p> <?php echo preg_replace('/\n/', '<br />', $latest_post->getMessage()); ?> </p> 	
											  	
											  	<?php if ($discussion->isActive() && $can_reply): ?>
														<div class="actions">
															Actions  
															<a href="/post/reply/<?php echo $discussion->getId(); ?>">
																<span>Reply</span>
															</a>																																			
														</div>							
													<?php endif; ?>				  		
												</div>
											</li>
											<?php $cnt++; ?>
											<?php if (3 == $cnt): ?>
												<?php break; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									</ul>								
								</td>
							</tr>
						</tbody>
					</table>
					
					<?php endif; ?>
					
				<?php endif; ?>													
			</div>	
		</div>
		<div class="foot"></div>
	</div>
</div>