<div id="discuss_board" class="section">
	<h2>
		<span>Discussions </span>
		<span class="header_actions">
			<a href="/discussion_board/home/<?php echo $group_id; ?>">Go to Discussions</a>
		</span>
	</h2>
	<div class="content">
		<?php if (0 == count($discussions)): ?>
			<div style="text-align: center;" class="help_text">
				<span>There are no discussions added to this subgroup.
				<a href="/discussion/add/<?php echo $topic_id; ?>">Add Discussion?</a>
				</span>
			</div>
		<?php endif; ?>		
		<ul id="dboard_body" class="discuss_content">
			<div id="featured_unfeatured_container">
				<?php $discussions_template->out(); ?>
			</div>				
			
			<?php if (0 < count($recent_discussions)): ?>
				<table cellspacing="0">
					<thead>
						<tr>
							<th class="first">
								<h2>Recently Discussed</h2>
							</th>
							<th>  </th>
							<th>  </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="recent-column" colspan="3">
								<ul class="users recent-list">
									<?php $cnt = 0; ?>						
									<?php foreach($recent_discussions as $discussion): ?>
										
										<?php $latest_post = $discussion->getLatestPost(); ?>
										<?php if( is_null($latest_post) ): continue; endif; ?>
										
										<?php $poster = $latest_post->getPoster(); ?>									
										<li>
										
											<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
												<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
													<img src="<?php echo $poster->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');; ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
												</a>
											<?php else: ?>
												<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
													<img src="<?php echo $poster->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
												</a>								
											<?php endif; ?>								
											
											<div class="details">											
										    <strong>
										    	<img title="" alt="pinned" src="/images/v3/discussions/reply-to-discussion.gif" class="pin_img"/>
										    	
													<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
														<a class="username" href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getUserName(); ?>'s travel blogs and travel notes"><?php echo $poster->getUserName(); ?></a>
											    	<?php if ($is_admin_logged): ?>
											    		<em><?php echo $poster->getFullName(); ?></em>
											    	<?php endif; ?>														
													<?php else: ?>
														<a class="username" href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getName(); ?>'s travel blogs and travel notes"><?php echo $poster->getName(); ?></a>								
													<?php endif; ?>	
										    </strong>    									    		
												<?php if (1 == $discussion->getPostsCount()): ?>    									    		
													started a new discussion
												<?php else: ?>	
													replied to
												<?php endif; ?>
									    	<a href="/discussion/posts/<?php echo $discussion->getId(); ?>" class="highlight" title=""><?php echo $discussion->getTitle(); ?></a>	
									  		<em><?php echo GaDateTime::descriptiveDifference($latest_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> ago</em>	
									  		<p> <?php echo preg_replace('/\n/', '<br />', $latest_post->getMessage()); ?> </p> 	
										  	
										  	<?php if ($discussion->isActive() && $can_reply): ?>
													<div class="actions">
														Actions  
														<a href="/post/reply/<?php echo $discussion->getId(); ?>">
															<span>Reply</span>
														</a>																																			
													</div>							
												<?php endif; ?>				  		
											</div>
										</li>
										<?php $cnt++; ?>
										<?php if (3 == $cnt): ?>
											<?php break; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								</ul>								
							</td>
						</tr>
					</tbody>
				</table>
			<?php endif; ?>			
		</ul>	
	</div>
</div>

<div class="clear"></div>