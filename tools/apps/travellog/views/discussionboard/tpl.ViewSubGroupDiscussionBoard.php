<?php Template::includeDependentJs('/js/DiscussionBoard.js', array('bottom'=>true)); ?>
<?php $gaDateTime = new GaDateTime(); ?>

<?php echo $profile->render(); ?>
<?php $sub_navigation->show(); ?>

<div class="layout_2" id="content_wrapper">
	<div id="dboard">			
		<div class="content">												
			<div id="dboard_head">													
				<h2>Discussion Board</h2>	
				<ul class="meta">
					<li class="first"><?php echo (0 < $discussions_count) ? $discussions_count : 'No'; ?> Discussion<?php if (1 <> $discussions_count) { ?>s<? } ?></li>
					<li><?php echo (0 < $posts_count) ? $posts_count : 'No'; ?> Post<?php if (1 < $posts_count OR 0 == $posts_count) {?>s<? } ?></li>
					<?php if (isset($last_post)): ?>	
						<li>Last Post Created <?php echo GaDateTime::descriptiveDifference($last_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> Ago</li>
					<?php endif; ?>
				</ul>				
			</div>				
				
			<div id="dboard_body">	
				<ul class="post_actions">
					<?php if ($can_reply): ?>
						<li>
							<a class="button" href="/discussion/add/<?php echo $topic->getId(); ?>" title="">Start a new Discussion</a>
						</li>
					<?php endif; ?>
					<?php if ($is_admin_logged): ?>
						<li>
							<a class="button" href="javascript:Topic.showTopicPrivacyPreference(<?php echo $topic->getId(); ?>, '<?php echo preg_replace('/\'/', '\\\'', $group->getName()); ?>', '<?php echo preg_replace('/\'/', '\\\'', $parent_group->getName()); ?>')" title="Edit Privacy Preference">Edit Privacy Preference</a>
						</li>
						<input type="hidden" id="hidPrivacyPreference" value="<?php echo $topic->getPrivacySetting(); ?>" />
					<?php endif; ?>									
				</ul>
				<table cellspacing="0">
					<thead>
						<tr>
							<th scope="col" class="first">
								<h2>Discussion</h2>
							</th>
							<?php if (0 < $discussions_count): ?>
								<th scope="col">Replies</th>
								<th scope="col">Started by</th>
								<th scope="col" class="last">Last Reply</th>
							<?php endif; ?>
						</tr>
					</thead>
					<tbody id="discussions_<?php echo $topic->getID(); ?>" >
						<?php if (0 < $discussions_count): ?>
							<?php if ($discussion_helper->hasFeaturedDiscussions()): ?>	
								<tr> 
									<td class="divider" colspan="4">
										<div>Pinned Discussions</div>
									</td>
								</tr>														
								<?php $discussion_helper->renderFeaturedDiscussions(); ?>
							<?php endif; ?>
							<?php if ($discussion_helper->hasUnFeaturedDiscussions()): ?>
								<?php if ($discussion_helper->hasFeaturedDiscussions()): ?>	
									<tr> 
										<td class="divider" colspan="4">
											<div>Other Discussions</div>
										</td>
									</tr>	
								<?php endif; ?>
								<?php $discussion_helper->renderUnFeaturedDiscussions(); ?>
							<?php endif; ?>
						<?php else: ?>
							<tr>
								<td >
									<div>This board has no discussions yet. Click <a href="/discussion/add/<?php echo $topic->getID() ?>">here</a> to add a new discussion.</div>
								</td>
							</tr>	
						<?php endif; ?>																																																										
					</tbody>
				</table>

			<?php if (0 < count($recent_discussions)): ?>
				<table cellspacing="0">
					<thead>
						<tr>
							<th class="first">
								<h2>Recently Discussed</h2>
							</th>
							<th>  </th>
							<th>  </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="recent-column" colspan="3">
								<ul class="users recent-list">
									<?php $cnt = 0; ?>						
									<?php foreach($recent_discussions as $discussion): ?>
										<?php $latest_post = $discussion->getLatestPost(); ?>
										
										<?php if( is_null($latest_post) ): continue; endif; ?>
										
										<?php $poster = $latest_post->getPoster(); ?>									
										<li>
										
											<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
												<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
													<img src="<?php echo $poster->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');; ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
												</a>
											<?php else: ?>
												<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
													<img src="<?php echo $poster->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
												</a>								
											<?php endif; ?>								
											
											<div class="details">											
										    <strong>
										    	<img title="" alt="pinned" src="/images/v3/discussions/reply-to-discussion.gif" class="pin_img"/>
										    	
													<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
														<a class="username" href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getUserName(); ?>'s travel blogs and travel notes"><?php echo $poster->getUserName(); ?></a>
											    	<?php if ($is_admin_logged): ?>
											    		<em><?php echo $poster->getFullName(); ?></em>
											    	<?php endif; ?>														
													<?php else: ?>
														<a class="username" href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getName(); ?>'s travel blogs and travel notes"><?php echo $poster->getName(); ?></a>								
													<?php endif; ?>	
										    </strong>    									    		
												<?php if (1 == $discussion->getPostsCount()): ?>    									    		
													started a new discussion
												<?php else: ?>	
													replied to
												<?php endif; ?>
									    	<a href="/discussion/posts/<?php echo $discussion->getId(); ?>" class="highlight" title=""><?php echo $discussion->getTitle(); ?></a>	
									  		<em><?php echo GaDateTime::descriptiveDifference($latest_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> ago</em>	
									  		<p> <?php echo preg_replace('/\n/', '<br />', $latest_post->getMessage()); ?> </p> 	
										  	
										  	<?php if ($discussion->isActive() && $can_reply): ?>
													<div class="actions">
														Actions  
														<a href="/post/reply/<?php echo $discussion->getId(); ?>">
															<span>Reply</span>
														</a>																																			
													</div>							
												<?php endif; ?>				  		
											</div>
										</li>
										<?php $cnt++; ?>
										<?php if (3 == $cnt): ?>
											<?php break; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								</ul>								
							</td>
						</tr>
					</tbody>
				</table>
			<?php endif; ?>	
									
			</div>	
		</div>
		<div class="foot"></div>
	</div>
	<div class="clear"></div>
</div>