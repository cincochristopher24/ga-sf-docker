<?php echo $profile->render() ?>

<?php $sub_navigation->show() ?>

<div class="layout_2" id="content_wrapper">
	<ul class="breadcrumbs">
		<?php if ($is_subgroup): ?>
			<li class="root">
				<a href="/discussion_board/home/<?php echo $group->getGroupID(); ?>">
					<span>Discussion Board Home</span>
				</a>
			</li>
		<?php else: ?>
			<li class="root">
				<a href="/discussion_board/home/<?php echo $group->getGroupID(); ?>">
					<span>Knowledge Base Home</span>
				</a>
			</li>
			<li>
				<a href="/topic/discussions/<?php echo $topic->getId(); ?>">
					<span><?php echo $topic->getTitle(); ?></span>
				</a>
			</li>			
		<?php endif; ?>
		
		<?php if (0 == $post_id): ?>
			<li>
				<a href="/discussion/posts/<?php echo $discussion->getId(); ?>">
					<span><?php echo $discussion->getTitle(); ?></span>
				</a>
			</li>
			<li>
				<strong><span>Reply</span></strong>
			</li>
		<?php else: ?>
			<li>
				<a href="/discussion/posts/<?php echo $discussion->getId(); ?>">
					<span><?php echo $discussion->getTitle(); ?></span>
				</a>
			</li>			
		<?php endif; ?>
	</ul>

	<div id="wide_column">
		<div class="section" id="dboard">
			<h2>
				<?php if (0 < $post_id): ?>
					Edit Post
				<?php else: ?>
					Reply to <span><?php echo $discussion->getTitle(); ?></span>
				<?php endif; ?>
			</h2>
				
			<?php if (0 == $post_id): ?>
				<div class="section_small_details">
					Join the discussion - write your reply:
				</div>
			<?php endif; ?>
									
				<div class="content">					
					<div id="dboard_body">
						<form action="/post/save" method="post" accept-charset="utf-8" class="interactive_form post">
							<ul class="form">
								<li>
									<label for="txtMessage">Message</label>
									<?php if(isset($errors['message'])):?>
										<p class="error">
											<?php echo $errors['message']; ?>
										</p>
									<?php endif;?>
									<textarea name="txaMessage" id="txaMessage"><?php echo $message; ?></textarea>
								</li>
								
								<li class="actions">
									<?php if (0 == $post_id): ?>
										<input type="submit" value="Post Reply" class="submit" />
									<?php else: ?>
										<input type="submit" value="Save" class="submit" />
									<?php endif; ?>
									
									&nbsp; or &nbsp;
									<a class="emphasize_link" href="/discussion/posts/<?php echo $discussion->getId(); ?><?php if (0 < $post_id) { echo "#".$post_id; } ?>"><span>&larr; Return to Discussion</span></a>																	
								</li>
							</ul>
							<input type="hidden" name="hidPostId" value="<?php echo $post_id; ?>">
							<input type="hidden" name="hidDiscussionId" value="<?php echo $discussion_id; ?>">
						</form>
						<div class="clear"></div>
					</div>
				
				</div>
				<div class="foot"></div>
			</div>		
	 </div>
				
	<div class="clear"></div>
</div>