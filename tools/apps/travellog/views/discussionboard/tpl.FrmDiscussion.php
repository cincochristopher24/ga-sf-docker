<?php echo $profile->render() ?>

<?php $sub_navigation->show() ?>

<div class="layout_2" id="content_wrapper">
	<ul class="breadcrumbs">
		<?php if ($is_subgroup): ?>
			<li class="root">
				<a href="/discussion_board/home/<?php echo $group_id; ?>">
					<span>Discussion Board Home</span>
				</a>
			</li>
		<?php else: ?>
			<li class="root">
				<a href="/discussion_board/home/<?php echo $group_id; ?>">
					<span>Knowledge Base Home</span>
				</a>
			</li>
			<li>
				<a href="/topic/discussions/<?php echo $topic_id; ?>">
					<span><?php echo $topic_title; ?></span>
				</a>
			</li>			
		<?php endif; ?>
		
		<li>
			<strong><span>New Discussion</span></strong>
		</li>
	</ul>

	<div id="wide_column">
		<div class="section" id="dboard">
			<h2>
				New Discussion FOR <span><?php echo $topic_title; ?></span>
			</h2>
				
			<div class="section_small_details">
				Start the discussion: write the first post for this discussion: 											
			</div>
									
			<div class="content">					
				<div id="dboard_body">
					<form action="/discussion/save" method="post" accept-charset="utf-8" class="interactive_form post">
						<ul class="form">
							<li>
								<label for="txtTitle">Title</label>
								<?php if(isset($errors['title'])): ?>
									<p class="error">
										<?php echo $errors['title']; ?>
									</p>
								<?php endif;?>
								<input name="txtTitle" type="text" value="<?php echo $title; ?>" class="text big" />
							</li>
															
							<li>
								<label for="txtMessage">Message</label>
								<?php if(isset($errors['message'])):?>
									<p class="error">
										<?php echo $errors['message']; ?>
									</p>
								<?php endif;?>
								<textarea name="txaMessage" id="txaMessage"><?php echo $message; ?></textarea>
							</li>
							
							<li class="actions">
								<input type="submit" value="Post Discussion" class="submit" />
								&nbsp; or &nbsp;
								<?php if ($is_subgroup): ?>
									<a class="emphasize_link" href="/discussion_board/home/<?php echo $group_id; ?>">
										<span>&larr; Return to Discussion Board Home</span>
									</a>								
								<?php else: ?>			
									<a class="emphasize_link" href="/topic/discussions/<?php echo $topic_id; ?>">
										<span>&larr; Return to Topic</span>
									</a>															
								<?php endif; ?>
							</li>
						</ul>
						<input type="hidden" name="hidTopicId" value="<?php echo $topic_id; ?>">
					</form>
					<div class="clear"></div>
				</div>
			
			</div>
			<div class="foot"></div>
		</div>		
	</div>
				
	<div class="clear"></div>
</div>