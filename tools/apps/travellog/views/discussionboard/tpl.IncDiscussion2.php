<tr>
	<th scope="row">
		<?php if ($discussion->isFeatured()): ?>
			<h3 class="pinned">
				<img title="" alt="pinned" src="/images/v3/discussions/dboard-pin.gif" class="pin_img"/>
				<a href="/discussion/posts/<?php echo $discussion_id; ?>"><?php echo $title; ?></a>
			</h3>
		<?php else: ?>
			<h3 >
				<img title="" alt="Topic" src="/images/v3/discussions/topic_or_discussion.gif" class="pin_img"/>
				<a href="/discussion/posts/<?php echo $discussion_id; ?>"><?php echo $title; ?></a>
			</h3>	
		<?php endif; ?>
		
		<p><?php echo preg_replace('/\n/', '<br />', $first_post->getMessage()); ?></p>										
		<?php if ($is_admin_logged): ?>
			<div class="actions">
				Actions &nbsp;
				<a id="archive_<?php echo $discussion_id; ?>" <?php if ($discussion->isArchived()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Archive Discussion ?','Are you sure you want to archive this discussion?', function(){ Discussion.archive(<?php echo $discussion_id; ?>); },'Archive','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0)">
					<span>Archive</span>
				</a> 										
			
				<a href="javascript:void(0)" <? if($discussion->isActive()){ ?>style="display:none;"<? } ?> id="activate_<?php echo $discussion_id; ?>" onclick="CustomPopup.initialize('Activate Discussion ?','Are you sure you want to activate this discussion?', function(){ Discussion.activate(<?php echo $discussion_id; ?>); },'Activate','1');CustomPopup.setJS();CustomPopup.createPopup();">
					<span>Activate</span>
				</a> 
				|						
				<a <?php if ($discussion->isFeatured()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Pin this discussion?','Are you sure you want to pin this discussion?', function(){ var handler = function(response){ var resp = eval(response); if (resp.isSuccessful) { jQuery('#discussions_<?php echo $discussion->getTopicId(); ?>').html(resp.message); } else { DiscussionBoard.showResponseMessage(resp.message); } }; Discussion.feature(<?php echo $discussion_id; ?>, 2, handler); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Feature discussion">Pin discussion</a>		
				<a <?php if (!$discussion->isFeatured()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Unpin this discussion?','Are you sure you want to unpin this discussion?', function(){ var handler = function(response){ var resp = eval(response); if (resp.isSuccessful) { jQuery('#discussions_<?php echo $discussion->getTopicId(); ?>').html(resp.message); } else { DiscussionBoard.showResponseMessage(resp.message); } }; Discussion.unfeature(<?php echo $discussion_id; ?>, 2, handler); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Unfeature discussion">Unpin discussion</a>								
				<?php if ($is_subgroup AND !(TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING == $topic->getPrivacySetting() || TopicPeer::SUBGROUP_PUBLIC_SETTING == $topic->getPrivacySetting())): ?>
					|
					<a id="add_knowledge_action_<?php echo $discussion_id; ?>" <?php if ($discussion->isAddedToKnowledgeBase()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Knowledge Base','Are you sure you want to add this discussion to the knowledge base?', function(){ Discussion.addToKnowledgeBase(<?php echo $discussion->getID(); ?>); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Add to knowledge base">Add to Knowledge Base</a>
					<a id="remove_knowledge_action_<?php echo $discussion_id; ?>" <?php if (!$discussion->isAddedToKnowledgeBase()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Knowledge Base','Are you sure you want to remove this discussion from the knowledge base?', function(){ Discussion.removeFromKnowledgeBase(<?php echo $discussion->getID(); ?>); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Remove from knowledge base">Remove from Knowledge Base</a>						
				<?php endif; ?>			
			</div>
		<?php endif; ?>
	</th>
			
	<td><?php echo ($posts_count - 1); ?></td>
	<td>
		<?php if (PosterPeer::TRAVELER == $first_poster->getPosterType()): ?>
			<?php echo $first_poster->getUserName(); ?></a>
			<?php if ($is_admin_logged): ?>
				<br />
				<em><?php echo $first_poster->getFullName(); ?></em>
			<?php endif; ?>
		<?php else: ?>
			<?php echo $first_poster->getName(); ?>							
		<?php endif; ?>
	</td>
	<td class="last"> 
		<?php if (isset($latest_post)): ?>
			<a href="/discussion/posts/<?php echo $discussion_id; ?>#<?php echo $latest_post->getID(); ?>"><?php echo GaDateTime::descriptiveDifference($latest_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> ago</a> 											 
			<br />
			by 
			<?php if (PosterPeer::TRAVELER == $latest_poster->getPosterType()): ?>
				<?php echo $latest_poster->getUserName(); ?></a>
				<?php if ($is_admin_logged): ?>
					<br />
					<em><?php echo $latest_poster->getFullName(); ?></em>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $latest_poster->getName(); ?>							
			<?php endif; ?>
		<?php endif; ?>										
	</td>																		
</tr>