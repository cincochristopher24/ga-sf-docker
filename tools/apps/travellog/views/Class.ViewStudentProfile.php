<?php
	/*
	 * Class.ViewStudentProfile.php
	 * Created on Nov 29, 2007
	 * created by marc
	 */
 	
 	require_once("travellog/views/Class.AbstractView.php");

	class ViewStudentProfile extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewStudentProfile.php');
		}
		
	}
?>
