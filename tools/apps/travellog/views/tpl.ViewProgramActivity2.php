<?php
require_once('Class.GaDateTime.php');
?>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
	<? if ( $ShowControls ): ?>
	<tr>
			<td colspan="<?= $ShowControls ? 4 : 3; ?>" align="right"><a href="javascript:void(0)" onclick="manager.displayActivityForm('mode=add');"  class="add"><strong>Add Activity</strong></a></td>
	</tr>
	<? endif; ?>
	<tr>
		<th width="150">Date / Time</th>
		<th>Activity</th>
		<th>Description</th>
		<? if ( $ShowControls ): ?>
			<th width="80" align="center">Controls</th>
		<? endif; ?>
	</tr>
	
	<?  if( count($obj_activities) ): 
			foreach( $obj_activities as $obj_activity ): ?>
				<tr>
					<td valign="top"><?= GaDateTime::friendlyFormat($obj_activity->getTheDate()) ?></td>
					<td valign="top"><a href="javascript:void(0)" onclick="manager.getDescription(<?= $obj_activity->getActivityID() ?>)"><strong><?= $obj_activity->getTitle() ?></strong></a></td>
					<td valign="top"><div id="description<?= $obj_activity->getActivityID() ?>" class="collapse" style="display:none"><?= nl2br($obj_activity->getDescription()) ?></td>
					<? if ( $ShowControls ): ?>
						<td valign="top"><a href="javascript:void(0)" onclick="manager.displayActivityForm('mode=edit&aID=<?= $obj_activity->getActivityID() ?>');">Edit</a>&nbsp;|&nbsp;<a href="javascript:void(0)" onclick="manager.deleteActivity('mode=delete&aID=<?= $obj_activity->getActivityID() ?>');">Delete</td>
					<? endif; ?>
				</tr>
	<?      endforeach;
		endif; ?>
</table>
