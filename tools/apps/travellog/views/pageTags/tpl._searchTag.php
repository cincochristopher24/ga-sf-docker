<div class="section">
	<h2><span>Find a tag</span></h2>
	<div class="content">		
		<ul class="form" id="search_form">
			<li>
				<?php if(isset($GLOBALS['CONFIG'])):?>
					<?php $formAction = '/tagmanagement.php?action=searchTag'?>
				<?php else: ?>
					<?php $formAction = '/tagmanagement.php?action=searchTag&gID='.$gID?>
				<?php endif; ?>
				<form action="<?=$formAction?>" method="POST" />							
					<input type="text" class="text" id="searchTagKeyword" name="searchTagKeyword" value="<?=$searchTagKeyword?>" style="width:262px;"/>					
			        <span class="actions">		
				        <input type="submit" value="Search tag" title="Search" name="btnSearchTagName" id="btnSearchTagName" class="submit"/>
			        </span>
		        </form>
			</li>
		</ul>								
	</div> <!-- end content -->						
</div>	<!-- end section -->
<div id="mygroups_action" class="section help">
	<h2><span>What are tags?</span></h2>
	<div class="content">
		<p>
			Assigning Tags to your SubGroups will assist in two ways. First, is to organize the SubGroups into meaningful categories and second to create easy navigation for your users to find the groups on the web site.
			<a href="/faq.php#tags" title="More on tags" target="_blank">More on Tags &raquo;</a>
		</p>
	</div>
</div>