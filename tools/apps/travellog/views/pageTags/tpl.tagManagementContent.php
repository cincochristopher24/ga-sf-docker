<div>
	<table class="tagMgrList">
		<thead>
			<tr>
				<th class="tagCol">
					Tag
				</th>
				<th class="tagWeight">
					No. of groups
				</th>
				<th>
					Actions
				</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 1;?>
			<?php $iterator->rewind(); ?>
			<?php while($iterator->valid()): ?>
				<?php $tag = $iterator->current(); ?>
					<tr class="<?php echo $class = ($tagID == $tag->getID()) ? 'active' : '' ?>">
						<td >
							<a href="#" class="strong" id="tagName_link_<?=$tag->getID()?>"><?=$tag->getName()?></a>
							<div class="right" style="display:none;">
								<input type="text" class="tagTextBox" name="fieldName" id="tag_<?=$tag->getID()?>" value="<?=$tag->getName()?>" />
								
							</div>
						</td>
						<td>
							<span class="strong" id="number_of_groups_<?=$tag->getID()?>"><?=$tag->getWeight()?></span>
						</td>
						<td>
							<p class="actions" id="manageTagActions" name='manageTagActions'>
								<a class="alpha" name='manageTagAction' id="edit_<?=$tag->getID()?>">Rename</a>
								<a class="alpha" name='save_tag' id="save_<?=$tag->getID()?>" style="display:none" onclick="return false;">&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;</a>
								<a class="omega" name='manageTagAction' id="delete_<?=$tag->getID()?>">Delete</a>
								<a class="omega" name='manageTagAction' id="cancel_<?=$tag->getID()?>" style="display:none" onclick="return false;">Cancel</a>
							</p>														
						</td>
					</tr>
			<?php $i++; ?>
			<?php $iterator->next(); ?>
			<?php endwhile; ?>
			<?php if($i==1):?>
				<tr><td colspan='3'>
					<p class="notice">No Records found!</p>
				</td></tr>
			<?php endif;?>
			<?php if ( $paging->getTotalPages() > 1 ):?>
				<tr >
					<td colspan='3' style="text-align:center;" valign="bottom">
					<?php $paging->showPagination()?>
					</td>
				</tr>
			<?php endif; ?>
		</tbody>	
	</table>	
	<input type="hidden" id="currentPage" value="<?=$currentPage?>" />
	<input type="hidden" id="keyword_used" value="<?php echo $searchTagKeyword;?>" />
	<input type="hidden" id="trCount" value="<?=$i?>" />
</div>

<?php if(isset($ajaxRequest) AND $ajaxRequest == true):?>
	<script type="text/javascript">
		jQuery.fn.initializeTagManagement();
	</script>
<?php endif; ?>