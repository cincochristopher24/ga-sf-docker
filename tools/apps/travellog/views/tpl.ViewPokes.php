<?php
	/**
	 * Created on 10 August 2007
	 * tpl.Poke.php
	 * @author Cheryl Ivy Q. Go
	 */

	$displayType = isset($displayType) ? $displayType : '';

 	if (!strcasecmp($displayType, 'messageCenter')){
 		$Sendable = SendableFactory::Instance();
		$Sender = $Sendable-> Create($message->getSenderID());
		$PokeType = new PokeType($message->getPokeType());
 	?>
		<li>
			<div class="meta date"><?=date('M j, Y',strtotime($message->getCreated()))?></div>
			<div style = "padding-bottom: 2px; padding-top: 2px;">
		 		<img src = "images/poke/<?=$PokeType->getPokeImage()?>" height = "22" width = "22" style = "padding-right: 3px;" />
	 			<?=$message->getMessage()?>
		 	</div>
		</li>
	<?
 	}

 	else if (!strcasecmp($displayType, 'hover')){
 		foreach($pokeTypes as $indPokeType) : ?>
			<div id = "poke<?=$indPokeType->getPokeTypeID()?>" onMouseOver = "Poke.highLight(this, true);" onMouseOut = "Poke.highLight(this);" onclick = "Poke.addPoke(<?=$indPokeType->getPokeTypeID()?>)">
				<span style = "padding-left: 6px;"> <img src = "images/poke/<?=$indPokeType->getPokeImage()?>" height = "22" width = "22" /> </span>
				<span style = "padding-left: 4px; font-weight: bold; valign: middle; align: center;"> <?=$indPokeType->getPokeType()?> </span>
			</div>
		<? endforeach;
 	}

 	else if (!strcasecmp($displayType, 'show')){
 		$showLink = (0 < $hasAdded) ? 'false' : 'true';
 		$aMessage = ('profile' == $section) ? 'greet '.$username : 'greet '.$username.' on this '.$section;
 		$t = (1 < $pokesReceived) ? $pokesReceived.' times' : 'once';
 		$dMessage = ('profile' == $section) ? $username.' has been greeted '.$t : $username.' has been greeted '.$t.' on this '.$section; ?>
		<div id = "display_poke_count" class = "<?=$classname?>" style = "color: #1381BA; padding-top: 23px; font-weight: bold;">
			<? if (0 < $pokesReceived) :
					echo $dMessage;
				endif;
			?>
		</div>
		<script type="text/javascript">
			Poke.setUsername('<?=$username?>');
			Poke.setLinkMessage('<?=$aMessage?>');
			Poke.setSection('<?=$context?>');
			Poke.setShowlink('<?=$showLink?>');
			Poke.setGenId(<?=$genID?>);
			Poke.addEvents();
		</script>
		<?
 	}
	
	elseif (!strcasecmp($displayType, 'displayPokes')){
		if (!strcasecmp($section, "photo")){
			$cname = "poke_alerts";
		}
		else{
			$cname = "poke_alert_journal";
		}		

	 	if (count($pokesReceived)) { ?>
			<div class = "<?=$cname?>">
				<div>
					<h1 style = "border-bottom: none; font-size: 22px;"> who poked you? </h1>
				</div>
				<div class = "poke_container">
					<ul>
						<?
							$ctr = 1;
							foreach($pokesReceived as $indPoke){
								$Sendable = SendableFactory::Instance();
								$Sender = $Sendable-> Create($indPoke->getSenderID());
								$PokeType = new PokeType($indPoke->getPokeType());
							?>
								<li>
									<div>
										<img src = "images/poke/<?=$PokeType->getPokeImage()?>" height = "22" width = "22" style = "padding-right: 3px;" />
										<span>
											<?
												$gID = 0;
												if ($gID > 0 ):
													$ag = new AdminGroup($gID); ?>
											 		<? $link = "<a href='/group.php?gID=" . $gID . "'>" . $ag->getName() . "</a>"; ?>
												<?else:?>
											 		<? $link = "<a href=" . $Sender->getTravelerProfile()->getUserName() . ">" . $Sender->getTravelerProfile()->getUserName() . "</a>"; ?>
											 	<?endif;

												if (!strcasecmp($section, "photo")){
											 		if (PokeType::$SMILE == $indPoke->getPokeType())
											 			echo 'Your photo made ' . $link . ' smile';
											 		else if (PokeType::$THUMBSUP == $indPoke->getPokeType())
											 			echo $link . ' has viewed your photo and gave you a thumbs up';
											 		else if (PokeType::$CHEERSMATE == $indPoke->getPokeType())
											 			echo $link . ' has viewed your photo and says, "Cheers, mate!"';
											 		else if (PokeType::$THAIWAI == $indPoke->getPokeType())
											 			echo $link . ' gave your photo a Thai Wai';
											 		else if (PokeType::$WAVE == $indPoke->getPokeType())
											 			echo $link . ' waived at you on your photo';
											 		else if (PokeType::$BOWDOWN == $indPoke->getPokeType())
											 			echo $link . ' bows down to your photographing skills';
											 		else if (PokeType::$CONGRATULATE == $indPoke->getPokeType())
											 			echo $link . ' congratulates you on your awesome photo';
												}
												else if (!strcasecmp($section, "journal")){
													if (PokeType::$SMILE == $indPoke->getPokeType())
											 			echo 'Your journal made ' . $link . ' smile';
											 		else if (PokeType::$THUMBSUP == $indPoke->getPokeType())
											 			echo $link . ' has read your journal and gave you a thumbs up';
											 		else if (PokeType::$CHEERSMATE == $indPoke->getPokeType())
											 			echo $link . ' has read your journal and says, "Cheers, mate!"';
											 		else if (PokeType::$THAIWAI == $indPoke->getPokeType())
											 			echo $link . ' gave your journal a Thai Wai';
											 		else if (PokeType::$WAVE == $indPoke->getPokeType())
											 			echo $link . ' waived at you on your journal';
											 		else if (PokeType::$BOWDOWN == $indPoke->getPokeType())
											 			echo $link . ' bows down to your trip';
											 		else if (PokeType::$CONGRATULATE == $indPoke->getPokeType())
											 			echo $link . ' congratulates you on this experience';
												}
												else{
													if (PokeType::$SMILE == $indPoke->getPokeType())
											 			echo $link . ' smiled at you';
											 		else if (PokeType::$THUMBSUP == $indPoke->getPokeType())
											 			echo $link . ' gave you a thumbs up';
											 		else if (PokeType::$CHEERSMATE == $indPoke->getPokeType())
											 			echo $link . ' says, "Cheers, mate!"';
											 		else if (PokeType::$THAIWAI == $indPoke->getPokeType())
											 			echo $link . ' greeted you with a Thai wai';
											 		else if (PokeType::$WAVE == $indPoke->getPokeType())
											 			echo $link . ' waved at you';
											 		else if (PokeType::$BOWDOWN == $indPoke->getPokeType())
											 			echo $link . ' greeted you with a bow';
											 		else if (PokeType::$CONGRATULATE == $indPoke->getPokeType())
											 			echo $link . ' congratulates you!';
												}
										 		$ctr +=1;
										 	?>
										 </span>
									</div>
								</li>
								<div class = "clear"></div>
							<?
							}
						?>
					</ul>
		 		</div>
		 		<div class = "clear"></div>
	 		</div>
		<?
	 	}
	}
?>