<?
	/**
	 * tpl.FrmMessage.php
	 * @author - marc
	 */
	require_once("Class.Constants.php"); 
	echo $contents["prof_comp"]->render(); // added by ianne - 12/3/2008
	$contents["subNavigation"]->show();
	
?>
	<div id="content_wrapper" class="layout_2">
		
		
		<div id="wide_column">
			<!--inbox/outboox tabs added by ianne - 12/3/2008-->
			<div id="message_center">
				<ul id="nav">
					<li><a href="<?=$contents['inboxlink']?>"><span>Inbox</span></a></li>
					<li><a href="<?=$contents['outboxlink']?>"><span>Outbox</span></a></li>
				</ul>	
			</div>	
			<div class="section" id="section_messages">
			<h2><span>Compose Message</span></h2>
			
			<div class="new_message content">
				<form action="<?= $contents["actionPage"]; ?>" method="post" onsubmit="doSubmitOnce(this)">
					<? if( strlen($contents["errString"]) ): ?>
						<p><?= $contents["errString"]; ?></p>
					<? else: ?>
						<?  if( 0 == $contents["lockDestination"] && in_array($contents["action"],array(constants::COMPOSE,constants::EDIT)) ): ?>
							<? if(trim($contents["recipientHelpText"])):  ?>
								<p class="help_text"><?= $contents["recipientHelpText"]; ?></p>
							<? endif; ?>
						<?endif;?>
						<? if( count($contents["arrError"]) ): ?>
							<?= $sub_views['MESSAGE_ERRORS']->render(); ?>
						<? endif; ?>
						<table class="message" width="100%" cellspacing="0">
							<tbody class="message_info">
								<tr>
									<th scope="row">To:</th>
									<td>
										<?  if( 0 == $contents["lockDestination"] && in_array($contents["action"],array(constants::COMPOSE,constants::EDIT)) ): ?>
											<span class="message_input_container">
												<input type="text" class="text" name="txtDestination" id="txtDestination" value="<?= implode(',',$contents['travTo']); ?>" autocomplete="off" />
											</span>
											<script type="text/javascript">
												var obj = actb(document.getElementById('txtDestination'),[<?= implode(",",$contents["suggestList"]); ?>]);
											</script> 
										<? else:  
											echo implode($contents["travTo"]);
										   endif;		
										?>
									</td>
								</tr>
								<tr>
									<th scope="row">Subject:</th>
									<td>
										<span class="message_input_container">
											<input type="text" class="text" name="txtSubject" id="txtSubject" value="<?= $contents['subject']?>" />
										</span>
									</td>
								</tr>
								<tr>
									<th class="message" scope="row" valign="top">Message:</th>
									<td>
										<textarea name="txaMsgText" id="txaMsgText"  cols="70" rows="10"><? 
											if ( $contents["action"] == constants::REPLY )
												echo "\n\r\n\r\n\r";
											echo $contents["text"];
											?></textarea><br />
									</td>
								</tr>
								<tr class="actions">
									<td>&nbsp;</td>
									<td>
									<? $site=(isset($GLOBALS['CONFIG']))?$GLOBALS['CONFIG']->getSiteName():"GoAbroad Network"; ?>	
									<a class="read_first" href="message-rules.php" onclick="window.open('message-rules.php', 'messageRules', 'resizable=no,width=490,height=520,top=' + ((screen.width/2)- 1000) + ',left=20'); return false;">Please read the <?=$site?> Rules of Conduct before sending your message.</a>
										<br/>
										<input type='checkbox' name='chkSendNotification' id='chkSendNotification' <?if($contents['traveler']->isAdministrator()):?>checked<?endif;?> /><label for='chkSendNotification'>Include your message in the notification?</label>
										<p class="supplement">
											We send an email notification to the recipient to inform them that you've sent a message.<br /> Checking the box will include your message in that notification.
										</p>
										<input type="submit" name="btnSend" id="btnSend" value="Send" class="submit highlight" onclick="document.getElementById('hdnAction').value = 'SEND'" />
										<!--commented out by ianne - 12/3/2008-->
										<? /*if( $contents["user"] == constants::TRAVELER && $contents["hdnRecipientFlag"] != constants::GROUP ): ?>
											<input type="submit" name="btnDraft" value="Save as Draft" class="submit" onclick="document.getElementById('hdnAction').value = 'SAVE_AS_DRAFT'"/>
										<? endif; */?>	
										<input type="hidden" name="hdnReferer" value="<?= htmlspecialchars($contents['referer']); ?>" />
										<input type="hidden" name="hdnTravTo" value="<?= implode($contents['travTo']); ?>" />
										<input type="hidden" name="hdnActive" value="<?= $contents['active']; ?>" />
										<input type="hidden" name="hdnTitle" value="<?= $contents['title']; ?>" />
										<input type="hidden" name="hdnMessageID" value="<?= $contents['messageID']; ?>" />
										<input type="hidden" name="hdnSourceID" value="<?= $contents['sourceID']; ?>" />
										<input type="hidden" name="hdnListingID" value="<?= $contents['listingID']; ?>" />
										<input type="hidden" name="hdnReplyFlag" value="<? if( $contents['action'] == constants::REPLY ) echo '1'; else '0'; ?>" />
										<input type="hidden" name="hdnRecipientFlag" value="<?= $contents['hdnRecipientFlag']; ?>" />
										<input type="hidden" name="hdnLockDestination" value="<?= $contents['lockDestination']; ?>" />
										<input type="hidden" name="hdnRecipientSendableID" value="<?= $contents['recipientSendableID']; ?>" />
										<input type="hidden" name="hdnAction" id="hdnAction" value="" />
									
										<? if( !strlen($contents["errString"]) ): ?>
											<input type="submit" name="btnBack" value="Cancel" class="submit" onclick="document.getElementById('hdnAction').value = 'BACK'" />
										<? endif; ?>
									</td>
								</tr>
								</tbody>
							</table>	
					<? endif;?>	
				</form>
				</div>			
			</div>
		</div>
				
		<div id="narrow_column">
			<?//= $sub_views['MESSAGE_NAVIGATION']->render(); ?> <!--commented out by ianne - 12/3/2008-->
		</div>
	</div>
<?   
	if( strlen($contents["errString"]) ){
		if( $contents["user"] == constants::GROUP )		
			header("Refresh: 3; URL=http://" . $_SERVER['SERVER_NAME'] . "/group.php");
		else		
			header("Refresh: 3; URL=http://" . $_SERVER['SERVER_NAME'] . "/messages.php");
	}
?>	

