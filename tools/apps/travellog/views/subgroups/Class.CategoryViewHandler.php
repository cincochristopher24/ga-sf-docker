<?php
	/**
	 * @(#) Class.CategoryViewHandler.php
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - Feb. 16, 2009
	 */
	
	class CategoryViewHandler {
		/**
		 * @var CustomizedSubGroupCategory The category to be made a view handler.
		 */
		private $mCategory = null;
		
		/**
		 * @var string The subgroups generated output from the template.
		 */
		private $mSubGroupsTpl = "";
		
		/**
		 * @var integer The count of subgroups added.
		 */
		private $mCount = 0;
		
		/**
		 * Constructs the view handler of the category.
		 * 
		 * @param CustomizedSubGroupCategory $category The category to whom the view will be made.
		 */
		function __construct(CustomizedSubGroupCategory $category){
			$this->mCategory = $category;
		}
		
		/**
		 * String representation of this object.
		 * 
		 * @return string
		 */
		function __toString(){
			return $this->fetch();
		}
		
		/**
		 * Adds a subgroup to the category.
		 * 
		 * @param SubGroup $group The subgroup to be added to the category.
		 * 
		 * @return void
		 */
		function addSubGroup(SubGroup $group, AdminGroup $parent){
			if (5 <= $this->mCount) {
				return;
			}
			
			$this->mCount++;
			$tpl = new GAHtmlTemplate();
			$tpl->setTemplate("travellog/views/subgroups/tpl.IncSubGroupLi.php");
			$tpl->set("group", $group);
			$tpl->set("parent", $parent);
			$this->mSubGroupsTpl .= $tpl->fetch();
		}
		
		/**
		 * Returns the generated output of the template for the category view.
		 * 
		 * @return string
		 */
		function fetch(){
			$tpl = new GAHtmlTemplate();
			$tpl->setTemplate("travellog/views/subgroups/tpl.IncCategoryView.php");
			$tpl->set("subgroups", $this->mSubGroupsTpl);
			$tpl->set("category", $this->mCategory);
			
			return $tpl->fetch();
		}
	}
	
