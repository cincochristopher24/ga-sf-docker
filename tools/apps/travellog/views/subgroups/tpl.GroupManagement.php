<?php
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	Template::includeDependentJs('http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js');
	Template::includeDependentJs('http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js');
	Template::includeDependentJs('/js/subGroupsAndCategoriesManager.js');
	Template::includeDependentJs('/js/GroupTemplateManager.js');
		
	$tab_action = (isset($tab_action)) ? $tab_action : "showActive";
?>
  <?= $profile->render()?>
  
  <?=$subNavigation->show();?>

	<div class="layout_2" id="content_wrapper">
		<div id="wide_column">
			<? if ($isAdminLogged && !$is_searching): ?>
				<div id="featured_groups" class="section">
					<h2><span>Featured Subgroups</span></h2>
					<div class="section_small_details">
						Below are the subgroups featured in your group home page.
					</div>
					<div class="content" id="featuredGroupsContent">
						<?=	$featuredSubGroupsTemplate ?>
					</div>
				</div>
			<? endif; ?>
			
			<div id="groups_category_mode" class="section">	
				<h2>
					<span>Subgroups</span>
					<span id="groupCountLabel" class="header_actions"></span>
					<?	if( $isAdminLogged ): ?>
						<span class="header_actions">						
						  <a href="javascript:void(0)" style="<?=("showActive" === $tab_action) ? "color:gray;" : "";?>" onclick="manager._viewActive('gID=<?=$grpID?>&amp;mode=ajax_category&amp;tabAction=showActive&amp;txtGrpName=<?=$searchKey?>&amp;category_name=<?php echo $category_name; ?>');" id="viewActive">
						    Active (<strong id="active_id"><?=$cnt_active?></strong>)
						  </a> 
						  |
						  <a href="javascript:void(0)" style="<?=("showInActive" === $tab_action) ? "color:gray;" : "";?>"  onclick="manager._viewInActive('gID=<?=$grpID?>&amp;mode=ajax_category&amp;tabAction=showInActive&amp;txtGrpName=<?=$searchKey?>&amp;category_name=<?php echo $category_name; ?>');" id="viewInActive">
						    Inactive (<strong id="inactive_id"><?=$cnt_inactive?></strong>) 
						  </a>
						</span>
					<?	endif; ?>
					<input type="hidden" id="tabAction" value="<?=$tab_action?>" />
				</h2>

				<div id="loader" class="widePanel content" style="display: none;">
					<p class="loading_message">
						<span id="imgLoading">
							<img alt="Loading" src="/images/loading.gif"/>
						</span>
						<em id="statusCaption">Loading data please wait...</em>
					</p>
				</div>
				
				<div id="content_to_hide">
					<div class="content" id="subGroupsContent">
						<?=	$subGroupsTemplate ?>			
					</div>
					
					<?= $assigner ?>
				</div>
				
			</div>
		</div>
		
		<div id="narrow_column">
			<? if ((20 < $cnt_active OR 20 < $cnt_inactive OR $is_searching)): ?>
				<div class="section" id="quick_tasks2">
					<h2><span>Search</span></h2>
					<div class="content">
						<ul class="actions">
							<span>
								<input type="text" class="text" id="txtGrpName" name="txtGrpName" value="<?php if ("" == $searchKey) { echo $category_name; } else { echo $searchKey; }?>" />				
							</span>
							<span>		
								Search By:
								<select id="search_by" name="selSearchBy">
									<option value="group" <?php if ("group" == $search_by) {?>selected="selected"<?}?> > Group Name </option>
									<option value="category" <?php if ("category" == $search_by) {?>selected="selected"<?}?> > Category Name </option>
								</select>
							</span>
							<span>	
								<br />	
								<input type="button" value="Search" onclick="manager.search('/subgroups.php?gID=<?=$grpID?>&amp;action=search&amp;search_by='+document.getElementById('search_by').value)" />
							</span>
						</ul>
					</div>				
				</div>
			<? endif; ?>
			
			<? if ($isAdminLogged): ?>
								
				<div class="section" id="quick_tasks">
					
					<label>Subgroup Tasks</label>
					
					<div class="content">
						
						<ul class="actions">
							<li>
								<a href="/group.php?action=view_form&amp;parent_group_id=<?php echo $grpID; ?> " class="button" >
								 + Add Subgroup
								</a>
							</li>
							<li>
								<a href="/subgroupcategories.php?gID=<?=$grpID?>&amp;mode=viewAddCategory" class="button">
								 + Add Category
								</a>
							</li>
							<li>
								<a href="/group_template.php?action=view_template_manager&amp;group_id=<?php echo $grpID; ?>" class="button">
								 View Template Manager
								</a>
							</li>
						</ul>
					
					</div>	
				</div>
			<? endif; ?>
		</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#txtGrpName').keypress(function(e){
			if(13 == e.keyCode) {
				manager.search('/subgroups.php?gID=<?php echo $grpID; ?>&action=search&search_by='+jQuery('#search_by').val());
			}
		});
	});
	<?if(isset($success)):?>
		CustomPopup.initPrompt("<?=$success?>");
		window.onload = function(){
			CustomPopup.createPopup();
		}
	<?endif;?>
</script>