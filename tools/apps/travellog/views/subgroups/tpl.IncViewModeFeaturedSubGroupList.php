<?  if (0 < $iterator->size()) : ?>
	<ul id="featuredGroupID" class="groups">	
		<? while($iterator->hasNext()) : ?>
			<?php
				$group = $iterator->next();
				$subgroup_name = str_replace('-', '_', $group->getName());
				$subgroup_name = str_replace(' ', '-', $subgroup_name);
				$group_name = str_replace('-', '_', $parent->getName());
				$group_name = str_replace(' ', '-', $group_name);
					
				$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;
				
				$group_name = (20 < strlen($group->getName()) && !strpos($group->getName()," ") ) ? substr($group->getName(),0,20) . "..." : $group->getName();
				$categ = $group->getCategory();
				
				$members_count = $group->getMembersCount();
				$staff_count = $group->getStaffCount();
				$journals_count = $group->getJournalsCount();
				$photos_count = ($group->getAlbumPhotosCount() + ((0 == $group->getGroupPhotoID()) ? 0 : 1 ));
				$videos_count = $group->getVideosCount();
			?>			
			
			<li itemID="<?=$group->getGroupID()?>">
				<a href="<?= $friendlyURL?>" class="thumb" title="View Group Profile">
					<img src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>" alt="Group Logo" />
				</a>
				<div class="details"> 
					<a class="groupname" href="<?= $friendlyURL?>"><?=$group_name?></a>
					
					<div class="group_stats"> 
						<span><?php echo (0 == $members_count) ? "No" : $members_count; ?> Member<?php if (1 != $members_count) {?>s<? } ?></span>|
						<span><?php echo (0 == $staff_count) ? "No" : $staff_count;  ?> Staff</span> 
					</div>
					
					<div class="group_stats"> 
						<span><?php echo (0 == $journals_count) ? "No" : $journals_count; ?> Related Journal<?php if (1 != $journals_count) {?>s<? } ?></span>|
						<span><?php echo (0 == $photos_count) ? "No" : $photos_count; ?> Related Photo<?php if (1 != $photos_count) {?>s<? } ?></span>| 
						<span><?php echo (0 == $videos_count) ? "No" : $videos_count; ?> Related Video<?php if (1 != $videos_count) {?>s<? } ?></span> 
					</div>			
					
					<div class="group_category_label"> 
						<strong> Category:&nbsp;
							<span class="group_stats">
								<span><?= (is_null($categ)) ? "None" : $categ->getName();?></span>
							</span> 								
						</strong>								
					</div>																
					
					<div class="group_actions">
					  <a href="javascript:void(0)" <? if (!$group->isFeatured()){?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Remove <?=addslashes($group->getName())?> from Featured Groups ?','Are you sure you want to remove <?= addslashes($group->getName()) ?> from Featured Groups?',function(){manager.unfeature('action=unsetFeatured&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;show=true', <?=$group->getGroupID()?>);},'Remove','1');CustomPopup.setJS();CustomPopup.createPopup();" >
					  	Remove in Features
					  </a>
					  
					  <? if (0 < $categoryCount): ?>
						  <? if (is_null($categ)): ?>
						  	<a href="javascript:void(0)" onclick = "manager.showCategoryAssigner('Assign <?=addslashes($group->getName())?> To A Category?', 0)" >Assign to a category</a>
						  <? else:?>
						  	<a href="javascript:void(0)" onclick = "manager.showCategoryAssigner('Move <?=addslashes($group->getName())?> To Another Category?', <?= $categ->getCategoryID() ?>, '?action=assign_category&amp;gID=<?=$parent->getGroupID()?>&amp;mode=list&amp;sgID=<?=$group->getGroupID()?>')" >Move to another category</a>
						  <? endif; ?>
					  <? endif; ?>
					  <a href="/messages.php?act=messageGroupStaff&gID=<?= $group->getGroupID() ?>">Message Staff</a>
						<?$popup_link = "/group.php?mode=delete&amp;confirm&amp;gID=".$group->getGroupID()?>
						<a href="javascript:void(0)" onclick = "CustomPopup.initialize('Delete <?=$group->getName()?> ?','Are you sure you want to delete <?=$group->getName()?>?','<?=$popup_link?>','Delete','0');CustomPopup.createPopup();">	
							Delete
						</a>
						
						<a href="javascript:GroupTemplateManager.showCreateForm(<?php echo $group->getGroupID(); ?>);" title="">Save as template</a>
					</div>
				</div>									
			</li>
		<? endwhile; ?>									
	</ul>

<? else: ?>
	<p class="help_text"> <span>There are no featured subgroups</span>.</p>	
<? endif; ?>