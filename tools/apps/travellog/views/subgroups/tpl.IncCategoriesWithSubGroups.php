<?
	$tabAction = (SubGroup::ACTIVE == $access) ? "showActive" : "showInActive";
	$nonEmptyCategories = 0;
?>
<? if (0 < $categoryCount): ?>
	<?php if ($isAdminLogged && !$is_searching): ?>
		<div class="section_small_details" style="height:32px">					
			To add a subgroup in Feature Subgroups section, make sure you are featuring a subgroup that is under a featured category.
		</div>
	<?php endif; ?>

	<div class="section_small_details" id="mode">					
			<? if ($isAdminLogged AND !$is_searching AND (1 < $categoryCount OR 1 < $subgroupsCount)) : ?>
				You can arrange <strong>subgroups</strong> and <strong>categories</strong> by dragging them.					
			<? else: ?>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
			<? endif; ?>
		
		
			<div class="section_filter">
				<label>View Mode:</label>
				<select onchange="window.location.href = this.value;">
					<option value="/subgroups.php?gID=<?= $parent->getGroupID() ?>&amp;txtGrpName=<?=$keyword?>&amp;tabAction=<?=$tabAction?>"> Category View </option>
					<option value="/subgroups.php?gID=<?= $parent->getGroupID() ?>&amp;mode=list&amp;txtGrpName=<?=$keyword?>&amp;tabAction=<?=$tabAction?>"> List View </option>
				</select>
			</div>	
	</div>
<? endif; ?>						
	
<? if ($isAdminLogged AND !$is_searching AND (1 < $categoryCount OR 1 < $subgroupsCount)) : ?>
	<input type="button" value="Save" id="save_but" onclick="manager.saveCustomSort('numPerPage=<?=$numPerPage?>&gID=<?=$parent->getGroupID()?>&tabAction=<?=$tabAction?>&action=custom_sort_groups_and_categories')" />
	<p id="orderLoader" style="display:none;"> Saving order... </p>
<? endif; ?>	

<? if (0 < $iterator->size()): ?>
	<?php $iterator->rewind() ?>
	<ul id="category_list" class="groups">
		<? while($iterator->hasNext()): ?>
			<?php 
				$category = $iterator->next();
				$groups_count = $category->getSubGroupsCount("%".$keyword."%", $access); 
				$groups = $category->getSubGroups("%".$keyword."%", $access, " categorysubgrouprank", new RowsLimit($numPerPage, 0)); 
				
				$paging = new Paging($groups_count, 1, "gID=".$parent->getGroupID()."&mode=ajax_category_subgroups&txtGrpName=$keyword&tabAction=$tabAction&numPerPage=$numPerPage&categoryID=".$category->getCategoryID()."',".$category->getCategoryID().",'", $numPerPage);
				$paging->setOnclick("manager.paging");
				
				$groups_count_label = 1 < $groups_count ? "$groups_count groups" : "$groups_count group";
				if( 0 < $groups_count ){
					$nonEmptyCategories++;
				}
			?>
			<?php if ($is_category_searching || 0 < $groups_count || $isAdminLogged): ?>
				<!-- begin category -->
				<li class="category_box" id="li_category_id_<?= $category->getCategoryID() ?>">
					<?	if ($is_category_searching || (1 < $iterator->size() && (0 < $groups_count || $isAdminLogged))): ?>
						<div class="category_header">
							<h3 id="cat_head<?=$category->getCategoryID()?>" <? if ($category->isFeatured() AND $isAdminLogged){ ?> class="featured" <?}?> > 
									<?	if( $category->isUncategorized() && !$isAdminLogged ): ?>
										Other 
									<?	endif; ?>
									<?= $category->getName() ?>
									<? 	if ($category->isUncategorized() && $isAdminLogged ) { ?>
										(uncategorized)
									<? 	} ?>
									<span>(<?= $groups_count_label ?>)</span>
							</h3>
							<? if (0 < $groups_count ): ?>
								<a class="clipper collapse" href="javascript:void(0)" title="Collapse/Clip this category" onclick="manager.slide(<?= $category->getCategoryID() ?>, this)">&raquo;</a>	
								<input type="hidden" id="slider<?= $category->getCategoryID() ?>_slide" value="0" />		
							<? endif; ?>
						
							<div class="category_actions">
								<? if ($isAdminLogged): ?>
									<? $popup_link = "gID=".$parent->getGroupID()."&amp;sgcID=".$category->getCategoryID()."&amp;action=unfeatureCategory"; ?>
									<a href="javascript:void(0);" id="unfeature_category<?= $category->getCategoryID() ?>" <? if (!$category->isFeatured()){ ?>style="display:none;"<? } ?> onclick = "CustomPopup.initialize('Remove <?=addslashes($category->getName())?> From Featured Categories ?','Are you sure you want to remove <?=addslashes($category->getName())?> from featured categories?',function(){ manager.unfeatureCategory('<?=$popup_link?>', <?= $category->getCategoryID() ?>); },'Remove','1'); CustomPopup.setJS(); CustomPopup.createPopup();">Do not feature</a>
								
									<? $popup_link = "gID=".$parent->getGroupID()."&amp;sgcID=".$category->getCategoryID()."&amp;action=featureCategory"; ?>
									<a href="javascript:void(0);" id="feature_category<?= $category->getCategoryID() ?>" <? if ($category->isFeatured()){ ?>style="display:none;"<? } ?> onclick = "CustomPopup.initialize('Feature <?=addslashes($category->getName())?> ?','Are you sure you want to feature <?=addslashes($category->getName())?> category?',function(){ manager.featureCategory('<?=$popup_link?>', <?= $category->getCategoryID() ?>); },'Feature','1'); CustomPopup.setJS(); CustomPopup.createPopup();">Feature</a>
								
									<? if (!$category->isUnCategorized()): ?>
										|
										<a href="/subgroupcategories.php?mode=viewEditCategory&amp;sgcID=<?= $category->getCategoryID() ?>&amp;gID=<?= $parent->getGroupID() ?>">Edit</a>
										<? if (0 == $groups_count) :?>
											|
											<? $popup_link = "/subgroupcategories.php?gID=".$parent->getGroupID()."&amp;sgcID=".$category->getCategoryID()."&amp;action=deleteCategory"; ?>
					      			<a href="javascript:void(0)" onclick = "CustomPopup.initialize('Delete <?=addslashes($category->getName())?> ?','Are you sure you want to delete <?=addslashes($category->getName())?> category? This will remove the category and will not delete the groups in it.','<?=$popup_link?>','OK','0');CustomPopup.createPopup();return false;">	
									      Delete
								      </a>
										<? endif; ?>
									<? endif; ?>
								
									this category&nbsp;
								<? endif; ?>									
							</div>						
						</div>
					<?	endif; ?>
					<div id="loader<?=$category->getCategoryID()?>" class="widePanel content" style="display: none;">
						<p class="loading_message">
							<span id="imgLoading">
								<img alt="Loading" src="/images/loading.gif"/>
							</span>
							<em id="statusCaption">Loading data please wait...</em>
						</p>
					</div>
					
					<div id="slider<?=$category->getCategoryID()?>" >				
						
						<ul class="groups" id="groups_list<?=$category->getCategoryID()?>">
							<input type="hidden" id="subgroups_page<?=$category->getCategoryID()?>" name="subgroups_page<?=$category->getCategoryID()?>" value="1" />
							
							<? while($groups->hasNext()): ?>
							<?php
								$group = $groups->next();
								$subgroup_name = str_replace('-', '_', $group->getName());
								$subgroup_name = str_replace(' ', '-', $subgroup_name);
								$group_name = str_replace('-', '_', $parent->getName());
								$group_name = str_replace(' ', '-', $group_name);
									
								$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;
							
								$members_count = $group->getMembersCount();
								$staff_count = $group->getStaffCount();
								$journals_count = $group->getJournalEntriesCount();
								$photos_count = ($group->getAlbumPhotosCount() + ((0 == $group->getGroupPhotoID()) ? 0 : 1 ));
								$videos_count = $group->getVideosCount();
								$articles_count = $group->getArticlesCount();
							?>
								<!-- start looping group -->
								<li> 
									<a href="<?= $friendlyURL ?>" class="thumb" title="View Group Profile">
										<img src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>" alt="Group Logo" />
									</a>
									<div class="details"> 
										<a class="groupname" href="<?= $friendlyURL ?>"><?= $group->getName() ?></a> 
										
										<? if ($isAdminLogged): ?>
											<span id="featured_label<?=$group->getGroupID()?>" <? if (!$group->isFeatured()){ ?>style="display:none;"<? } ?> class="featured_label">Featured</span>
										<? endif; ?>
										
										<div class="group_stats"> 
											<span><?php echo (0 == $members_count) ? "No" : $members_count; ?> Member<?php if (1 != $members_count) {?>s<? } ?></span>|
											<span><?php echo (0 == $staff_count) ? "No" : $staff_count;  ?> Staff</span> 
										</div>
										
										<div class="group_stats"> 
											<span><?php echo (0 == $journals_count) ? "No" : $journals_count; ?> Journal Entr<?php if (1 != $journals_count) {?>ies<? }else{?>y<?php } ?></span>|
											<span><?php echo (0 == $articles_count) ? "No" : $articles_count; ?> Article<?php if (1 != $articles_count) {?>s<?php }?></span>|
											<span><?php echo (0 == $photos_count) ? "No" : $photos_count; ?> Photo<?php if (1 != $photos_count) {?>s<? } ?></span>| 
											<span><?php echo (0 == $videos_count) ? "No" : $videos_count; ?> Video<?php if (1 != $videos_count) {?>s<? } ?></span> 
										</div>										
										
										<div class="group_actions">
										  <? if ($isAdminLogged) : ?>
											  <a href="javascript:void(0)" id="unfeature<?=$group->getGroupID()?>" <? if (!$group->isFeatured()){?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Remove <?=addslashes($group->getName())?> from Featured Groups ?','Are you sure you want to remove <?= addslashes($group->getName()) ?> from Featured Groups?',function(){manager.unfeature('action=unsetFeatured&amp;mode=list&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;show=true', <?=$group->getGroupID()?>);},'Remove','1');CustomPopup.setJS();CustomPopup.createPopup();" >
											  	Remove in Features
											  </a>
											  <a href="javascript:void(0)" id="feature<?=$group->getGroupID()?>" <? if ($group->isFeatured()){?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Feature <?= addslashes($group->getName()) ?> ?','Are you sure you want to feature <?= addslashes($group->getName()) ?>?',function(){manager.feature('action=setFeatured&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;show=true', <?=$group->getGroupID()?>);},'Feature','1');CustomPopup.setJS();CustomPopup.createPopup();">
											    Feature this Subgroup
											  </a>
											  
											  <? if (0 < $categoryCount): ?>
											  	<a href="javascript:void(0)" onclick = "manager.showCategoryAssigner('Move <?=addslashes($group->getName())?> To Another Category?', <?= $category->getCategoryID() ?>, '?action=assign_category&amp;gID=<?=$parent->getGroupID()?>&amp;mode=view&amp;sgID=<?=$group->getGroupID()?>')" >Move to another category</a>
												<? endif; ?>
												
												<a href="/messages.php?act=messageGroupStaff&gID=<?= $group->getGroupID() ?>">Message Staff</a>
												<? $popup_link = "/group.php?mode=delete&amp;confirm&amp;gID=".$group->getGroupID() ?>
												<a href="javascript:void(0)" onclick = "CustomPopup.initialize('Delete <?= addslashes($group->getName()) ?> ?','Are you sure you want to delete <?= addslashes($group->getName()) ?>?','<?=$popup_link?>','Delete','0');CustomPopup.createPopup();">	
													Delete
												</a>
												
												<? if (1 < ceil($groups_count/$numPerPage)): ?>
													<a href="javascript:void(0)" onclick="manager.showPageAssigner('Move <?= addslashes($group->getName()) ?> to other page?', 1, <?=ceil($groups_count/$numPerPage)?>, 'action=transfer_page_category&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;tabAction=<?=$tabAction?>&amp;page=1&amp;numPerPage=<?=$numPerPage?>', <?=$category->getCategoryID()?>)">
														Transfer Page
													</a>
												<? endif; ?>
												
												<a href="javascript:GroupTemplateManager.showCreateForm(<?php echo $group->getGroupID(); ?>);" title="">Save as template</a>
												
											<? endif; ?>
										</div>
									</div>
									<input type="hidden" name="sort_subgroup_ids<?=$category->getCategoryID()?>[]" value="<?= $group->getGroupID() ?>">								
								</li>
								<!-- end looping group -->
							<? endwhile; ?>
							
							<? if (1 < ceil($groups_count/$numPerPage)): ?>
								<?=$paging->showPagination();?>
							<? endif; ?>	
						</ul>	
						<? if ($isAdminLogged AND "" == trim($keyword) AND 1 < $groups_count) : ?>
							<script type="text/javascript">
								jQuery('#groups_list<?=$category->getCategoryID()?>').sortable({ items: 'li', axis: 'y' });
							</script>
						<? endif; ?>
						
					</div>
					<input type="hidden" name="sort_category_ids[]" value="<?=$category->getCategoryID()?>">
				</li>
			<?php endif; ?>
		<? endwhile; ?>
	</ul>

<? if ($isAdminLogged AND "" == trim($keyword) AND 1 < $categoryCount): ?>
	<script type="text/javascript">
		jQuery('#category_list').sortable({ items: 'li', axis: 'y' });
	</script>
<? endif; ?>	
	
<? else: ?>
	<?php if ($is_category_searching): ?>
		<p class="help_text"> <span>No matching results found for search of category name '<?php echo $category_name; ?>'<span>.</p>
	<?php elseif ($is_searching): ?>
		<p class="help_text"> <span>No matching results found for search of group name '<?php echo $keyword; ?>'<span>.</p>
	<?php else: ?>
		<p class="help_text"><span>There are no <?php if ($isAdminLogged) { echo ($access = SubGroup::ACTIVE) ? "active" : "inactive";} ?> groups</span>.</p>
	<?php endif; ?>
<? endif; ?>

<?php if (0 < $iterator->size() && 0 == $nonEmptyCategories && !$is_category_searching): ?>
	<?php if ($is_searching): ?>
		<p class="help_text"> <span>No matching results found for search of group name '<?php echo $keyword; ?>'<span>.</p>
	<?php else: ?>	
		<p class="help_text"><span>There are no <?php if ($isAdminLogged) { echo ($access = SubGroup::ACTIVE) ? "active" : "inactive";} ?> groups</span>.</p>
	<?php endif; ?>
<?php endif; ?>

<?php if( !$isAdminLogged && 1 == $nonEmptyCategories && !$is_searching): ?>
	<script type="text/javascript">
		jQuery("#groupCountLabel").html("<?=$groups_count_label?>");
		jQuery("#mode").remove();
	</script>
<?php endif; ?>