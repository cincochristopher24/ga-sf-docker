<?php
	/**
	 * @(#) Class.SubGroupsViewHandler.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - Feb. 02, 2009
	 */
	
	class SubGroupsViewHandler {
  	protected $parent = null;  // the ID of the parent group
  	protected $message = ""; // the message that shows after deletion or update
  	protected $group_access_type = 1;
  	protected $subgroups = null;
  	protected $categories = null;
  	protected $keyword = "";
  	protected $isAdminLogged = false;
  	protected $mSubGroupsCount = 0;
  	protected $mNumRows = 0;
  	protected $mPage = 1;
  	protected $index = 0;
  	protected $templates = array("tpl.IncSubGroupList.php", "tpl.IncSubGroupsInLi.php");
  	
  	/**
  	 * Constructs the view handler for subgroups.
  	 * 
  	 * @param SqlResourceIterator $iterator The subgroups to be shown.
  	 * @param integer $subgroups_count The number of subgroups to be used in paging.
  	 * @param integer $page The current page number to show.
  	 * @param integer $numRows The number of rows to show.
  	 */
  	function __construct(SqlResourceIterator $iterator, $subgroups_count, $page, $numRows){
  		$this->subgroups = $iterator;
  		$this->mSubGroupsCount = $subgroups_count;
  		$this->mPage = $page;
  		$this->mNumRows = $numRows;
  	}
  	
  	/**
  	 * Assign variables and creates the template for viewing the list of 
  	 * categories of the given parent group.
  	 * 
  	 * @return string
  	 */
  	function fetch(){
			$assigner = "";
			if ($this->isAdminLogged AND !is_null($this->categories)) {
				$tpl1 = new GAHtmlTemplate();
				$tpl1->setTemplate("travellog/views/subgroups/tpl.IncSubGroupToCategoryAssigner.php");
				$tpl1->set("iterator", $this->categories);
				$assigner = $tpl1->fetch();
			}
			
			$action = (SubGroup::ACTIVE == $this->group_access_type) ? "showActive" : "showInActive";
			$paging = new Paging($this->mSubGroupsCount, $this->mPage, "gID=".$this->parent->getGroupID()."&mode=list&txtGrpName=$this->keyword&tabAction=$action", $this->mNumRows);
			
			$tpl = new Template();
			$tpl->set_path("travellog/views/subgroups/");
			$tpl->set("prompt", $assigner);
			$tpl->set("iterator", $this->subgroups);
			$tpl->set("paging", $paging);
			$tpl->set("numPages", ceil($this->mSubGroupsCount/$this->mNumRows));
			$tpl->set("message", $this->message);
			$tpl->set("parent", $this->parent);
			$tpl->set("keyword", $this->keyword);
			$tpl->set("access", $this->group_access_type);
			$tpl->set("isAdminLogged", $this->isAdminLogged);
			
			return $tpl->fetch($this->templates[$this->index]);
  	}
  	
  	/**
  	 * Renders the list of categories of the given parent group.
  	 * 
  	 * @return void
  	 */
  	function render(){
			echo $this->fetch();
  	}
  	
  	/**
  	 * Sets the template to use. 
  	 * 
  	 * @param integer $template_num
  	 * @return void
  	 */
  	function setTemplateIndex($template_num){
  		$this->index = (1 == $template_num) ? 1 : 0;
  	}
  	
  	/**
  	 * Sets the categories to be shown.
  	 * 
  	 * @param SqlResourceIterator $categories
  	 * @see SqlResourceIterator
  	 * @return void
  	 */
  	function setCategories(SqlResourceIterator $categories){
  		$this->categories = $categories;
  	}
  	
		/**
		 * Sets the flag whether the current logged user is admin or not.
		 * 
		 * @param boolean $isAdminLogged The flag whether the current logged user is admin or not.
		 * @return void
		 */
		function setIsAdminLogged($isAdminLogged){
			$this->isAdminLogged = $isAdminLogged;
		}
		
		/**
		 * Sets the keyword that will be used in viewing subgroups.
		 * 
		 * @param string $keyword The keyword that will be used in viewing subgroups.
		 * @return void
		 */
		function setKeyword($keyword){
			$this->keyword = $keyword;
		}
  	
  	/**
  	 * Sets the access type of categories' subgroups.
  	 * 
  	 * @param integer $access The access type of categories' subgroups.
  	 * @return void
  	 */
  	function setSubGroupAccess($access){
  		$this->group_access_type = $access;
  	}
  	
  	/**
  	 * Sets the confirmation message to be shown.
  	 * 
  	 * @param string $message
  	 * @return void
  	 */
  	function setMessage($message){
  		$this->message = $message;
  	}
  
  	/**
  	 * Sets the parent group to be used in templates.
  	 * 
  	 * @param AdminGroup $parent The parent group to be used in templates.
  	 * @return void
  	 */
  	function setParentGroup(AdminGroup $parent){
  		$this->parent = $parent;
  	}
	}
	
