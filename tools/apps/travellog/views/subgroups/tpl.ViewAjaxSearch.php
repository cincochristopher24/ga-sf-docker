<ul class="groups" >
	<input type="hidden" id="subgroups_page<?=$category->getCategoryID()?>" name="subgroups_page<?=$category->getCategoryID()?>" value="1" />
	
	<? while($groups->hasNext()): ?>
	<?php
		$group = $groups->next();
		$subgroup_name = str_replace('-', '_', $group->getName());
		$subgroup_name = str_replace(' ', '-', $subgroup_name);
		$group_name = str_replace('-', '_', $parent_group->getName());
		$group_name = str_replace(' ', '-', $group_name);
			
		$friendly_url = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;	
	?>
		<!-- start looping group -->
		<li> 
			<a href="<?php echo $friendly_url; ?>" class="thumb" title="View Group Profile">
				<img src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>" alt="Group Logo" />
			</a>
			<div class="details"> 
				<a class="groupname" href="<?php echo $friendly_url; ?>"><?= $group->getName() ?></a> 
	    <p>
	        <?= StringFormattingHelper::wordTrim($group->getDescription(), 40)?>
	
	        <? if (40 < count(explode(' ', strip_tags($group->getDescription()) ) ) ) : ?>
	          <?='&#8230;' ?>
	        <? endif ; ?> 
	
	        <a href="<?php echo $friendly_url; ?>" title="Read more about the <?= $group->getName() ?> travel community">
	          Read More
	        </a>...
	    </p>
	
			<? if (trim($group->getGroupMembershipLink('cobrand_search', $page)) !== ''): ?>
			  <p class="action">
			    <?=$group->getGroupMembershipLink('cobrand_search', $page)?>
			  </p>
			<? endif; ?>
			</div>						
		</li>
		<!-- end looping group -->
	<? endwhile; ?>
	
	<?php echo $paging->showPagination(); ?>
</ul>	