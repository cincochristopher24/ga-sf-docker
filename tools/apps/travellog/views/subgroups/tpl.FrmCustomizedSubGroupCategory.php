<?php
	Template::includeDependentJs("/js/customCategoryFormManager.js");
?>

<?= $profile->render()?>
  
<?=$subNavigation->show();?>
 
<div class="layout_2" id="content_wrapper">
	<div id="wide_column">
		<div id="groups_and_clubs" class="section">	
			<h2>
				<span>Category</span>
			</h2>
			<div class="content" id="subGroupsContent">
				<p class="help_text">
					Fields with  <span class="required">*</span> indicate required fields.
				</p>
				
				<? if (isset($message) AND 0 < strlen($message)): ?>
					<p class="notice">
						<?=$message?>
					</p>
				<? endif; ?>
			
				<form action="subgroupcategories.php" method="POST">
					<strong>Category Name: <span class="required">*</span></strong>
					<br/>
					<input type="text" name="txtcName" id="txtcName" value="<?=$name?>"/>
					<br/>
					<strong id="assigned_title" <? if (0 == $category_groups->size()) { ?>style="display:none;"<? }?>>Groups Assigned to this category:</strong>
					<ul id="assigned_category">
						<? while($category_groups->hasNext()):?>
							<? $group1 = $category_groups->next() ?>
							<li>
								<input type="checkbox" name="subgroups[]" id="chkCategory[]" value="<?=$group1->getGroupID()?>" checked/> <?=$group1->getName()?>
							</li>
						<? endwhile; ?>
					</ul>
					
					<? if (0 < $non_category_groups->size()): ?>
						<div class="uncategorized_groups">
							<strong>Add New Group To This Category</strong> <br />
							<select id="group_selector">
								<option id="option_groupID_0" value="0">Select Group</option>
								<? while($non_category_groups->hasNext()):?>
									<? $group1 = $non_category_groups->next() ?>
									<option id="option_groupID_<?=$group1->getGroupID()?>" value="<?=$group1->getGroupID()?>---<?=$group1->getName()?>"><?=$group1->getName()?></option>
								<? endwhile; ?>
							</select>
							<input type="button" value="Add" onclick="categoryForm.addGroup()" />
						</div>
					<?endif;?>
				
					<input type="hidden" name="sgcID" id="hidcID" value="<?=$id?>"/>
					<input type="hidden" name="gID" id="gID" value="<?=$parentID?>"/>
					<input type="hidden" name="action" value="updateCategory"/>
					<input type="hidden" name="back" value="<?=$backLink->getBackURL()?>" >
					<input type="submit" name="submit" value="Save"/>
					<? $backLink->getButton() ?>
				</form>
			</div>
		</div>
	</div>
	
</div>