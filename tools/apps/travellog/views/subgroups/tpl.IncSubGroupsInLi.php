
<? if (0 < $iterator->size()): ?>	
	<? while($iterator->hasNext()): ?>
		<?php
			$group = $iterator->next();
			$subgroup_name = str_replace('-', '_', $group->getName());
			$subgroup_name = str_replace(' ', '-', $subgroup_name);
			$group_name = str_replace('-', '_', $parent->getName());
			$group_name = str_replace(' ', '-', $group_name);
				
			$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;			
			
			$category = $group->getCategory();
			$category_name = (is_null($category)) ? "None" : $category->getName();
		?>
		<!-- start looping group -->
		<li class="list_box groups"> 
			<a href="<?= $friendlyURL ?>" class="thumb" title="View Group Profile">
				<img src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>" alt="Group Logo" />
			</a>
			<div class="details"> 
				<a class="groupname" href="<?= $friendlyURL ?>"><?= $group->getName() ?></a>
				<? if ($group->isFeatured()): ?>
					<span class="featured_label">Featured</span>
				<? endif; ?>
				
				<div class="group_stats"> 
					<span><?=  $group->getMembersCount() ?> Members</span>|
					<span><?=  $group->getStaffCount() ?> Staff</span> 
				</div>
				
				<div class="group_stats"> 
					<span><?= $group->getJournalsCount() ?> Related Journals</span>|
					<span><?= $group->getPhotosCount() ?> Related Photos</span>| 
					<span><?= $group->getVideosCount() ?> Related Videos</span> 
				</div>
				
				<div class="group_category_label"> 
					<strong> Category:&nbsp;<?= $category_name ?></strong>
				</div>										
				
				<div class="group_actions">
				  <? if ($isAdminLogged): ?>
					  <? if ($group->isFeatured()): ?>
						  <a href="javascript:void(0)" onclick="CustomPopup.initialize('Remove <?=addslashes($group->getName())?> from Featured Groups ?','Are you sure you want to remove <?= addslashes($group->getName()) ?> from Featured Groups?','?action=unsetFeatured&amp;mode=list&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>','Remove','1');CustomPopup.createPopup()" >
						  	Do not feature
						  </a>
						<? else: ?>
						  <a href="javascript:void(0)" onclick="CustomPopup.initialize('Feature <?= addslashes($group->getName()) ?> ?','Are you sure you want to feature <?= addslashes($group->getName()) ?>?','?action=setFeatured&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;mode=list','Feature','1');CustomPopup.createPopup();">
						    Feature this Subgroup
						  </a>
					  <? endif; ?>
					  <? if (is_null($category)): ?>
					  	<a href="javascript:void(0)" onclick = "manager.showCategoryAssigner('Assign <?=addslashes($group->getName())?> To A Category?', 0)" >Assign to a category</a>
					  <? else:?>
					  	<a href="javascript:void(0)" onclick = "manager.showCategoryAssigner('Move <?=addslashes($group->getName())?> To Another Category?', <?= $category->getCategoryID() ?>, '?action=assign_category&amp;gID=<?=$parent->getGroupID()?>&amp;mode=list&amp;sgID=<?=$group->getGroupID()?>')" >Move to another category</a>
					  <? endif; ?>
						<a href="/messages.php?act=messageGroupStaff&gID=<?= $group->getGroupID() ?>">Message Staff</a>
						<? $popup_link = "/group.php?mode=delete&amp;confirm&amp;gID=".$group->getGroupID() ?>
						<a href="javascript:void(0)" onclick = "CustomPopup.initialize('Remove <?= addslashes($group->getName()) ?> ?','Are you sure you want to remove <?= addslashes($group->getName()) ?>?','<?=$popup_link?>','Remove','0');CustomPopup.createPopup();return false;">	
							Delete
						</a>
					<? endif; ?>
				</div>
			</div>	
			<input type="hidden" name="sort_subgroup_ids[]" value="<?= $group->getGroupID() ?>">								
		</li>
		<!-- start looping group -->
	<? endwhile; ?>
<? else: ?>
	<script type='text/javascript'><!--
		document.getElementById('fetch_message').style.display = "none";
	//-->
	</script>"
<? endif; ?>