 <?if(isset($entries)):?>	
	<?$counter = 0;?>
	<ul>
		<?foreach($entries as $entry):?>
			<?if ($entry->getTravelID() == $travelId):?>
				<?$counter++;?>
				<?$patterns = array('/-/','/ /')?>
				<?$replacements = array('_','-') ?>
				<li id="journal_entry_line<?=$entry->getTravelLogID()?>">
					<div>
						<span id="title_container">
							<?if($canEditJournalAr[$entry->getTravelID()]):?>
								<h3><a href="/journal-entry.php?action=view&travellogID=<?=$entry->getTravelLogID()?>"><?=$entry->getTitle()?></a></h3>							
					    		<a href="/journal-entry.php?action=view&travellogID=<?=$entry->getTravelLogID()?>">Edit Entry</a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="del_entry_link" id="<?=$entry->getTravelLogID()?>">Delete Entry</a>
					    	<?else:?>					    		
					    		<h3><a href="/journal-entry.php?action=view&travellogID=<?=$entry->getTravelLogID()?>"><?=$entry->getTitle()?></a></h3>					    								    		
					    	<?endif;?>					    	
						</span>
						<span id="dest_container">
							<p id="location"><?=$entry->getTrip()->getLocation()->getName()?></p>
							<p id="date"><?= date('D M d, Y',strtotime($entry->getLogDate()))?></p>
						</span>
						<div class="clear"></div>
					</div>
				</li>								
			<?endif;?>
		<?endforeach;?> 				 		 
	</ul>
	<?if ($counter == 0): ?>
		<script type="text/javascript" >
			// hide show entries link and arrow link
			//jQuery(".show_entry#" + <?=$travelId?>).remove();
			//jQuery("#arrow" + <?=$travelId?> ).remove();
			jQuery(".show_entry#" + <?=$travelId?>).attr("style","display:none");
			jQuery("#arrow" + <?=$travelId?> ).attr("style","display:none");
		</script>
		<ul>
			<div>
			<li><span id="dest_container"><p id="date">No entries for this journal.</p></span><br/><br/></li>
			<div class="clear"></div>
			</div>
		</ul>	
	<?endif;?>
<?else:?>
		<script type="text/javascript" >
			// remove show entries link and arrow link
			jQuery(".show_entry#" + <?=$travelId?>).remove();
			jQuery("#arrow" + <?=$travelId?> ).remove();
		</script>
<?endif;?>