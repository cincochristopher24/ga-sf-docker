<?php
	
	require_once('travellog/model/Class.Travel.php');
	require_once('travellog/model/Class.TravelLog.php');
	require_once('travellog/model/Class.Article.php');
	require_once('travellog/custom/COBRAND/model/Class.TravelCB.php');
	
	require_once('travellog/vo/Class.JournalsArticlesVO.php');
	
	require_once('Class.JournalsCompPaging.php');
	require_once('Class.Template.php');

	require_once('travellog/model/Class.StaticQuery.php');
	
	class EntriesArticlesComp {
		
		private $viewMode = 'ALL'; 				// default, other values ALL, GROUP
		private $viewTab = '';
		private $securityContext = 'PUBLIC';
		private $site = 'GOABROAD';
		
		private $loggedUserID = 0;
		private $subjectID = 0;
		private $subject = NULL;
		
		private $isAdmin = false;
		private $isStaff = false;
		private $powerful = false;
		
		private $rpp = 10;
		private $page = 1;
		private $pagingComp = NULL;
		private $startrow = 0;
		private $total = 0;
		
		private $entriesAndArticles = array();
		private $entriesAndArticlesPhotos = array();
		private $relatedGroupsAr = array();
		private $searchCriteria2 = array();
		private $template = NULL;		
		
		function EntriesArticlesComp($setup, $subjectID, $loggedUserID=0){
			if(isset($setup['VIEWMODE']))
				$this->viewMode = $setup['VIEWMODE'];
			if(isset($setup['VIEW_TAB']))
				$this->viewTab = $setup['VIEW_TAB'];	
			if(isset($setup['SECURITY_CONTEXT']))
				$this->securityContext = $setup['SECURITY_CONTEXT'];
			if(isset($setup['CURRENT_PAGE']))
				$this->page = (is_numeric($setup['CURRENT_PAGE']) && !empty($setup['CURRENT_PAGE'])) ? abs($setup['CURRENT_PAGE']) : 1;
			if(isset($setup['SITE']))
				$this->site = $setup['SITE'];
				
			$this->loggedUserID = $loggedUserID;
			$this->subjectID = $subjectID;
			
			if( 'GROUP' == $this->viewMode ){
				$this->subject = new AdminGroup($this->subjectID);
				$this->subjectID = $this->subject->getGroupID();
				$this->isAdmin = ($this->loggedUserID == $this->subject->getAdministrator()->getTravelerID()) ? true : false;
				$this->isStaff = $this->subject->isStaff($this->loggedUserID);
				$this->powerful = ($this->isAdmin || $this->isStaff) ? true : false;
			}else if( 'ALL' == $this->viewMode ){
				$this->powerful = ($this->loggedUserID > 0) ? true : false;
			}
			
			$this->selectTemplate();
		}
		
		public function retrieveEntriesAndArticles(){
			$this->startrow = $this->rpp * ($this->page -1);
			if( 'ALL' == $this->viewMode && is_null($this->subject)){
				if(count($this->searchCriteria2)){
					if(isset($this->searchCriteria2['LOCATIONID']))
						$this->entriesAndArticles = Travel::getEntriesAndArticlesByCountryID($this->searchCriteria2['LOCATIONID'],array('START' => $this->startrow,'END' => $this->rpp));
						
					if(isset($this->searchCriteria2['TAG']))
						$this->entriesAndArticles = Travel::getAllTravelsAndArticlesByTag($this->searchCriteria2['TAG'],array('START' => $this->startrow,'END' => $this->rpp));
				
					
				}else
					$this->entriesAndArticles = Travel::getEntriesAndArticles(array('START' => $this->startrow,'END' => $this->rpp));
				$this->total = Travel::getTotalRec();	
			}else{
				if(count($this->searchCriteria2)){
					if(isset($this->searchCriteria2['LOCATIONID']))
						$this->entriesAndArticles = TravelCB::getEntriesAndArticlesByLocationID2(array('START' => $this->startrow,'END' => $this->rpp, 'groupID' => $this->subjectID, 'locationID' => $this->searchCriteria2['LOCATIONID']));
						
					if(isset($this->searchCriteria2['TAG']))
						$this->entriesAndArticles = TravelCB::getAllGroupTravelsAndArticlesByTag(array('START' => $this->startrow,'END' => $this->rpp, 'groupID' => $this->subjectID, 'tag' => $this->searchCriteria2['TAG']));

					$this->total = Travel::getTotalRec();
				}else{
					$this->entriesAndArticles = $this->subject->getEntriesAndArticles(array('START' => $this->startrow,'END' => $this->rpp));     
					$this->total = $this->subject->getEntriesAndArticles(array('START' => $this->startrow,'END' => $this->rpp), true);
				}
			}	
		}
		
		public function retrieveRelatedGroups(){
			require_once('travellog/dao/Class.GroupApprovedJournals.php');
			if ($this->viewMode != 'GROUP' ){
				foreach ($this->entriesAndArticles as $eAndA){
					if($eAndA->getType() == "JOURNAL" ){
						$rGroups = GroupApprovedJournals::getRelatedGroups($eAndA->getID());
						$this->relatedGroupsAr[$eAndA->getTravelID()] = $rGroups ;						
					}
				}
			}				
		}
		
		public function retrieveEntriesAndArticlesPhotos(){
			
			foreach($this->entriesAndArticles as $key => $eAndA){
				if( 'ARTICLE' == $eAndA->getType() ){
					
					if(!StaticQuery::isEmptyArticle($eAndA->getTravelID())) {

						$article =  new Article($eAndA->getTravelID());
						if($article->checkHasprimaryPhoto()){
							$this->entriesAndArticlesPhotos['ARTICLE'][$eAndA->getTravelID()] = $article->getPrimaryPhoto();
						}else{	
							$randPhoto = $article->getRandomPhoto();
							if($randPhoto->getPhotoID() > 0)
								$this->entriesAndArticlesPhotos['ARTICLE'][$eAndA->getTravelID()] = $randPhoto;
						}
					}	
				}else if( 'JOURNAL' == $eAndA->getType() ){
					
					$travellog =  new TravelLog($eAndA->getTravelID());
					if($travellog->checkHasprimaryPhoto()){
						$this->entriesAndArticlesPhotos['JOURNAL'][$eAndA->getTravelID()] = $travellog->getPrimaryPhoto();
					}else{	
						$randPhoto = $travellog->getRandomPhoto();
						if($randPhoto->getPhotoID() > 0)
							$this->entriesAndArticlesPhotos['JOURNAL'][$eAndA->getTravelID()] = $randPhoto;
					}
				
					
				}
				
			}

		}
		
		public function setSearchCriteria2($crit){
			$this->searchCriteria2 = $crit;
		}
		
		public function configurePaging(){
			$qryStr = 'VIEWMODE=' . $this->viewMode . '&VIEW_TAB=' . $this->viewTab .'&SECURITY_CONTEXT=' . $this->securityContext . 
 				'&SITE=' . $this->site . '&ROWS_PER_PAGE=' . $this->rpp ;
 			  								
			
			if (count($this->searchCriteria2) > 0){
				foreach($this->searchCriteria2 as $k => $v){					
					$qryStr .= '&' . strtolower($k) . '=' . $v;
				}
			}
			
			$jcParam = 'jcp=' . base64_encode($qryStr);
			$this->pagingComp = new JournalsCompPaging($this->total,$this->page,$jcParam,$this->rpp,100);
		}
		
		public function generateOrigURLParam(){			
			$origParams = preg_replace('/(&)?page=[0-9]+/','',$_SERVER['QUERY_STRING'],-1);
 			$origParams = preg_replace('/&jcp=(.)+/','',$origParams,-1);
			return $origParams;
		}
		
		function generateViewTabURLParams(){
			$tabs = array('FEATURED','NEW','JOURNALS_FEED','STAFF_ADMIN','APPROVED','UNAPPROVED');

			$qryStr = 'VIEWMODE=' . $this->viewMode . '&SECURITY_CONTEXT=' . $this->securityContext . 
			'&SITE=' . $this->site . '&ROWS_PER_PAGE=' . $this->rpp ;


			if (count($this->searchCriteria2) > 0){
				foreach($this->searchCriteria2 as $k => $v){					
					$qryStr .= '&' . strtolower($k) . '=' . $v;
				}
			}

			$tabsParam = array();
			foreach($tabs as $tab){
				$tabsParam[$tab] = 'gID=' . $this->subjectID .'&jcp=' . base64_encode($qryStr . '&VIEW_TAB=' . $tab);
				//$tabsParam[$tab] = 'jcp=' . $qryStr . '&VIEW_TAB=' . $tab;
			}

			//$jcParam = 'jcp=' . base64_encode($qryStr);				// journal component param to be passed during submit
			return $tabsParam;
		}
		
		public function selectTemplate(){
			switch($this->viewMode){
				case 'ALL':
				case 'GROUP':
					$this->template = 'tpl.EntriesAndArticlesAll.php';
					break;
				// case 'GROUP':
				// 	$this->template = 'tpl.GroupEntriesAndArticles.php';
			}
		}
		
		public function prepareTemplate(){
			$tpl = new Template();
			$tpl->set('entries', $this->entriesAndArticles);
			$tpl->set('entriesAndPhotos', $this->entriesAndArticlesPhotos);
			$tpl->set('relatedGroupsAr', $this->relatedGroupsAr);
			$tpl->set('powerful', $this->powerful);
			$tpl->set('pagingComp', $this->pagingComp);
			$tpl->set('grpName', ($this->subject instanceOf AdminGroup && $this->subject->getParentID() > 0) ? $this->subject->getName().' ' : '');
			$noJournalMessage = 'No travel journals yet.'; 

			if (count($this->entriesAndArticles) == 0){			  
				if ($this->subject != null) {
					if ($this->subject instanceof Traveler) {
						if ($this->subject->getTravelerID() == $this->loggedUserID)
							$noJournalMessage = '<span>Document those great travel moments and share them with family and friends!</span> <br /> A journal book is a set of journal entries about the same trip. You may write about your experiences and add your ' .
							'travel photos to your entries. You may also do a photo journal. <a href="/journal.php?action=add"><strong>Create your first Travel Journal Book</strong></a>';
						else
							$noJournalMessage = $this->subject->getUsername() . ' has no travel journals yet. Please check back later.';
					} else if ($this->subject instanceof Group) {
						$noJournalMessage = $this->subject->getName() . ' has no travel journals yet.';
					}
				}	  
			}
			$tpl->set('noJournalMessage', $noJournalMessage);	
			$tpl->out('travellog/views/'.$this->template);
		}
		
		public function render(){
			require_once('travellog/model/Class.SiteContext.php');
			$siteID = SiteContext::getInstance()->getGroupID();
			
			require_once('Cache/ganetCacheProvider.php');
			$added_key = (Traveler::hasGroupPowers($this->loggedUserID)) ? $this->loggedUserID.'_STAFF' : '0';
			$key	=	'Journal_URL_' . $siteID . $_SERVER['SCRIPT_NAME'] . $_SERVER['QUERY_STRING'] . '_' . $added_key;
			$_SESSION['journal_keys'][] = $key;
			$cache	=	ganetCacheProvider::instance()->getCache();
			
			$this->invalidateCacheEntry($cache);
			
			if (!$this->powerful && $cache != null && $content = $cache->get($key)){
				//echo '>> rendered Journals and Articles from cache <br/>';
				echo $content;
				return;
			}
			$this->retrieveEntriesAndArticles();
			$this->retrieveRelatedGroups();
			$this->configurePaging();
			
			$this->retrieveEntriesAndArticlesPhotos();
			
			ob_start();
			$this->prepareTemplate();
			$content	=	ob_get_contents();
			ob_end_flush();
			
			if ($cache != null) {
				$cache->set($key,$content,array('EXPIRE'=>3600));
			}
			
		}
		
		public function invalidateCacheEntry($cache){
			if(isset($_SESSION['TRAVEL_CACHE_ENTRY_UPDATED']) && $_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] == true){
				$this->cache = ganetCacheProvider::instance()->getCache();
				if ($this->cache != null){
					$keys = isset($_SESSION['journal_keys']) ? $_SESSION['journal_keys'] : array();
					foreach($keys as $key){
						$this->cache->delete($key);
					}
				}
				unset($_SESSION['TRAVEL_CACHE_ENTRY_UPDATED']);
				unset($_SESSION['journal_keys']);
			}
		}
	}		
	