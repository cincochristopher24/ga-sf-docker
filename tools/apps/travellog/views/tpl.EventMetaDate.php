<?php 
	require_once('Class.GaDateTime.php'); 
	$d = new GaDateTime;
	$date = GaDateTime::dbDateTimeFormat($date);
?>
<?php echo $d->set($date)->htmlEventDateFormat(); ?>
<span id="events_time" ><?php echo date('h:i A', strtotime($date)) ?></span>
<?php if(function_exists('date_create') && '' != $timezone ):?>
<span id="events_zone">
	<?php echo 'GMT '. substr(date_create($date, new DateTimeZone($timezone))->format('O'),0,3); ?>
</span>
<?php endif; ?>