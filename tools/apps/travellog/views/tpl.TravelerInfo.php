<?php require_once('Class.HtmlHelpers.php');?>
<div id="profile_info" class="section">
<h2><span>About the Author</span></h2>

<div class="content">

	<a href="<?=$obj_helper->getLink( $contents['obj_profile'] )?>" class="user_thumb" title="View Profile">
	<? if ( strlen($contents['obj_profile']->getPhoto()) ): ?>
		<img src="<?=$contents['obj_profile']->getPhoto()?>" alt="User profile photo thumbnail" />
	<? endif; ?>
	</a>
	<h3><a href="<?=$obj_helper->getLink( $contents['obj_profile'] )?>" title="View Profile"><?= HtmlHelpers::truncateWord($contents['obj_profile']->getUserName(), 10); ?></a></h3>
	
	<div id="rss">
	<a href="/journalfeed.php?jid=<?=$contents['obj_journal']->getTravelID()?>" title="RSS Feed" id="link">Subscribe Rss Feed
	</a>
	</div>
	
	<div class="profile_info meta">
		<?if( $contents['obj_profile']->getAge() ):?>
			<div class="age">
				<strong>Age: </strong>
				<?=$contents['obj_profile']->getAge()?>
			</div>
		<? endif; ?>
		
		<?if( strlen($contents['obj_profile']->getCity()) ):?>
			<div class="location">
				<strong>Currently in:</strong>
				<?=$contents['obj_profile']->getCity() . ', ' . $contents['obj_profile']->getCountry()?> 
			</div>
		<?endif;?>
		
		<?if( false && strlen($contents['obj_profile']->getInterests()) ): // Disable showing of traveling philosophy?>
			<div class="philosophy"><strong>Traveling Philosophy :</strong><br /><?=$contents['obj_profile']->getInterests()?></div>
		<?endif;?>
		
	</div>

</div>
</div>