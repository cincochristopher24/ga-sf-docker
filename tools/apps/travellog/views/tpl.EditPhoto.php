			<?php
			
			$tmp_sid = md5(uniqid(mt_rand(), true));
			
			ob_start();
						
				//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
				Template::setMainTemplateVar('layoutID', 'page_photos');
				$style = "<style>#AjaxLoading{padding:3px;font-size:14px;width:200px;text-align:center;position:absolute;border:none solid #666;background-color:#1f1f1f;}</style>";
				
				Template::includeDependent($style); 
				
				Template::includeDependentJs("/js/prototype.js");
				Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");  
				Template::includeDependentJs("/js/Upload_Progress.js"); 
				
				//Template::includeDependentJs("/js/htmldb_html_elements.js"); 
				//Template::includeDependentJs("/js/submitprogress.js"); 
				
				$code =<<<BOF
	<script type = "text/javascript">
		var UploadMeter = new UploadProgress("$tmp_sid");	
	</script>
BOF;

		Template::includeDependent($code);
				
			ob_end_clean();
			?>
			
			<? $subNavigation->show(); ?>
			
			<div id="content_wrapper">		
					<div id="wide_column">		
						<div id="photo_box" class="section">
							<h2><span>Edit Photo</span></h2>
							<div class="content">		
	
							
									<?php if ( $error ) : ?>
													<div class="errors">	
												
														<ul>
															<? foreach($error as $index => $err ): ?> 
																<li><? echo ($index+1).". ". $err->getMessage()." ".$err->getName()." (".round($err->getsize()/1024,2)." kb)" ?></li>
															<? endforeach; ?>												
														</ul>
												
													</div>
									<?php endif; ?>									
						
	
								
								<div class="photo_help">
									<form  name= "uploader" action="/cgi-bin/uu_upload.pl?tmp_sid=<?=$tmp_sid?>" method="post" enctype="multipart/form-data" >
										
												<img src="<?= $photo; ?>" class="photo_borders" />
				 
												<p>
													<label>Change photo:</label>
													<input type="file" name="upfile_0" >
												</p>
									
												<!-- Start Progress Bar -->
												<div id="progress_bar_notice" style="color:white;display:none;padding-left:3px;">
													<br>
													<p>
														<b> Uploading photos, please wait...</b>
													</p>
				    								<!--
				    								<p>
				    									Depending on the size of the photos and the speed of your connection, 
				    									this could take a few minutes. 
				    									Please do not close this window before the upload is complete. 
				    									This page will update automatically when your upload is finished.
													</p>
													-->					
												</div>
									
											    <div id="progress_bar" style="display:block;padding-left:3px;">
								    
											    </div>
								    
											    <!-- End Progress Bar -->											   	
									
									
														<input type="button" name="upload" value="Upload" onclick="UploadMeter.startUpload()" />	
														<?if($context == 'group' || $context == 'resume'):?>
															<input type="button" name="cancel" value="Cancel" onClick="window.location='<?= "http://".$_SERVER['SERVER_NAME'].$backlink?>'">
														<?else:?>	
															<input type="button" name="cancel" value="Cancel" onClick="window.location='<?= "http://".$_SERVER['SERVER_NAME']."/photomanagement.php?action=vfullsize&cat=".$context."&genID=".$genID."&photoID=".$photoid;  ?>'">
														<?endif;?>
												</p>
									
												<input type="hidden" name="txtcat" 		value="<?=$context;?>">
												<input type="hidden" name="txtaction" 	value="edit">
												<input type="hidden" name="txtgenid" 	value="<?=$genID;?>">
												<input type="hidden" name="txtid" 		value="<?=$photoid;?>">
												<input type="hidden" name="txtscriptname" 		value="/photomanagement.php">
											
											<div class="clear"></div>
								
									</form>
								</div>
							</div>
							<div class="foot"></div>
						</div>
						
					</div>
					<div class="clear"></div>
			</div>
			

