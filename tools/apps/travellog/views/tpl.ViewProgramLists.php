<?
$file_factory = FileFactory::getInstance();
$d = $file_factory->getClass('GaDateTime');
?>
<?if($props['is_owner']):?>
	<ul class="actions">
		<li><a href="javascript:void(0)" onclick="jQuery().addProgramForm('action=Create&amp;gID=<?=$props['gID']?>');" class="button">Create Itinerary</a></li> 
	</ul>
<?endif;?> 
<?if(isset($obj_iterator)):
	$obj_iterator->rewind(); ?>
	<ul class="itineraries">
		<?while( $obj_iterator->valid() ):?> 
			<li class="ptitle" rel="<?=$obj_iterator->current()->getProgramID()?>"> 
				<a href="javascript:void(0)" onclick="" class="ititle"><?=$obj_iterator->current()->getTitle()?></a>
				<div class="idates"><a href="javascript:void(0)" onclick=""><?= $d->set($obj_iterator->current()->getTheDate())->htmlDateFormat();?> to <?= $d->set($obj_iterator->current()->getFinish())->htmlDateFormat();?></a></div>
			</li>
			<?$obj_iterator->next();?>
		<?endwhile;?> 
	</ul>
<?endif;?>
<?if(isset($obj_paging)):?>
	<?$obj_paging->showMiniPagination()?>
<?endif;?>

