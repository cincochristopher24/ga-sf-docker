<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if($groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
	//	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
?>

<div id="content_wrapper" class="yui-skin-sam ltr noTopPadding">
	<div id="wide_column" class="layout2">
		<div class="section_detail">
			<div class="content paddedOnBottom20">
				<h2>Welcome to the CIS On-line Application</h2>
				
				<p>In order to begin the on-line application process you must create an account<span class="setMediumText fcGray strong">*</span>.  You will need to create a username and password specifically for your application.</p>
				<p class="paddedOnBottom20"><span class="setMediumText fcGray strong">*</span> please note this username and password is not the same as your MyCIS username and password (which you may or may not have).</p>
			<?/*	<p class="paddedOnBottom20"><span class="setMediumText fcGray strong">*</span> If you have a saved application please log-in. If you have already submitted your application you must contact CIS to make changes.</p> */ ?>
				<div class="center border_top_dotted paddedOnTop20">
					<h3 class="margin20bottom">Don’t have an account yet?</h3>

					<a class="button_v3 setMediumText" href="register.php">
						<strong>Create an Account Now</strong>
					</a>
			  	</div>
			</div>
		


</div>
	
	</div>
	<div id="narrow_column">
		<div class="section">
			<h2>Login</h2>
		<?/*	<p class="center paddedOnTop fcGray setSmallText">If you already have an account, please login below.</p> */ ?>
			<p class="center paddedOnTop fcGray setSmallText strong">If you already have an account or if you have a saved application please log-in below. If you have already submitted your application you must contact CIS to make changes.</p>
		<form action='?action=validateAccount' method='POST'>
			<ul id="main_login_form" class="form noTopPadding">
					<li class="element">
						<label class="login_control" for="txtUsrName">Email: </label>
						<input type="text" class="text" size="15" value="" onblur="CustomPopup.removeBubble();" onfocus="CustomPopup.fetchEmailBubble(true);" id="txtEmail" name="txtEmail"/>
					</li>
					<li class="element">
						<label class="login_control" for="pwdPasWrd">Password:</label>
						<input type="password" class="text" size="15" id="pwdPasWrd" name="pwdPasWrd"/>
					</li>
					<li class="others">
					<!--	<span><a title="Unable to login to your travel blog? Request to change your password." href="#">Forgot your password?</a>  -->
					</li>
					<li id="login_action">
							<!--<input name="btnSubmit" type="image" src="/images/login_button_home.gif" class="submit" value="Login" align="middle" />-->
							<button type="submit" name="btnLoginSubmit" class="submit"><span>Login</span></button>
							<input type="hidden" value="Log In" name="login"/>
							<input type="hidden" value="0" name="hdnLoginType"/>
							<input type="hidden" value="" name="hdnReferer"/>
					</li>
					<span class="required"><?php echo $errmsg = (isset($errMsg) ? $errMsg : '');?></span>
					<label><a href="?action=retrievePassword" >Forgot Password ?</a></label>
					
					
			</ul>
			
			</form>
			
		</div>
		

		 	 			  
												  

	</div>
</div>