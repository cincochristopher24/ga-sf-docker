<?php
	$sess = SessionManager::getInstance();
	$sess->unsetVar('onlineAppUserID');
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if($groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
	//	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
?>





<div class="area" id="intro">
	<h2 class="border_bottom_solid">Sorry for the inconvenience</h2>
	<div class="section_detail">
		<div class="content paddedOnTop20">
			<p>
				We are unable to accept credit cards at this time. We are sorry for any inconvenience this may cause. Please mail your application fee to: </p>
			<p>	<strong>Center for International Studies <br/>
				17 New South St. Suite 205 <br/>
				Northampton, MA 01060 <br/> </strong>
			</p>
		</div>
	</div>
</div>