<?php $cls = ($selectedStep)?>

<div class="section">
	<h2>Application Progress</h2>
	<ul id="application_progress" class="">
		<li class='<?php if($selectedStep==1): echo 'current'; elseif(1<$selectedStep):echo 'done'; else: echo 'disabled'; endif;?>'>
			<div class="number" id="one"><span>1</span></div>
			<span class="step_name">Personal Information</span>
			<div class="arrow"></div>
		</li>
		<li class='<?php if($selectedStep==2): echo 'current'; elseif(2<$selectedStep):echo 'done'; else: echo 'disabled'; endif;?>'>
			<div class="number" id="two"><span>2</span></div>
			<span class="step_name">Program Information</span>
			<div class="arrow"></div>
		</li>
		<li class='<?php if($selectedStep==3): echo 'current'; elseif(3<$selectedStep):echo 'done'; else: echo 'disabled'; endif;?>'>
			<div class="number" id="three"><span>3</span></div>
			<span class="step_name">Personal Statement</span>
			<div class="arrow"></div>
		</li>
		<li class='<?php if($selectedStep==4): echo 'current'; elseif(4<$selectedStep):echo 'done';else: echo 'disabled'; endif;?>'>
			<div class="number" id="four"><span>4</span></div>
			<span class="step_name">Documents to Mail</span>
			<div class="arrow"></div>
		</li>
		<li class='<?php if($selectedStep==5): echo 'current'; elseif(5<$selectedStep):echo 'done'; else: echo 'disabled'; endif;?>'>
			<div class="number" id="five"><span>5</span></div>
			<span class="step_name">Agreement and Waiver</span>
			<div class="arrow"></div>
		</li>
		<li class=<?php if($selectedStep==6): echo 'current'; else: echo 'disabled'; endif;?>>
			<div class="number" id="six"><span>6</span></div>
			<span class="step_name">Application Fee Payment</span>
			<div class="arrow"></div>
		</li>
	</ul>
	
	<div class="section center">
		<h4 class="margin20bottom"></h4>
		
		<a class="button_v3" href="?logout">
			<strong>Logout</strong>
		</a>
		<input type="hidden" name="saveAndExit" id="saveAndExit" value=''>
  	</div>
</div>


