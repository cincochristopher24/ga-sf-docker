<div id="content_wrapper" class="yui-skin-sam ltr">
	<div id="wide_column" class="layout2">
		<div class="section_detail">
			<div class="content">
				<h2>Study Abroad Program Information</h2>
				
				<p>I need content for this page. Some dummy text for now. Phasellus placerat rhoncus purus. In pulvinar tincidunt mi, accumsan fermentum neque euismod ultrices. Suspendisse ut ante a ipsum faucibus auctor. </p>
				<h3>What you will need:</h3>
				<ul class="disc">
					<li>Credit Card</li>
					<li>SS#</li>
					<li>Lorem ipsum</li>
					<li>Everything here</li>
				</ul>
				<h3>Documents you need to mail to CIS</h3>
				<p>
					Explain here why students need to mail these documents to CIS. Some dummy text for now. Phasellus placerat rhoncus purus. In pulvinar tincidunt mi, accumsan fermentum neque euismod ultrices. Suspendisse ut ante a ipsum faucibus auctor. Phasellus ac eleifend magna! Sed sit amet orci arcu, sit amet cursus augue.
				</p>
				<ul class="disc">
					<li>Course selection worksheet</li>
					<li>Academic Reference Form</li>
					<li>CIS Scholarship Information and Application (optional)</li>
				</ul>
				
				<h3>Study Abroad Checklist</h3>
				<p>
					Here is a step by step process on completing the application form. Sample text here. Phasellus placerat rhoncus purus. In pulvinar tincidunt mi, accumsan fermentum neque euismod ultrices. Suspendisse ut ante a ipsum faucibus auctor. Phasellus ac eleifend magna! Sed sit amet orci arcu, sit amet cursus augue.
				</p>
				<ul class="disc">
					<li>
						Research and select a CIS program using this study abroad handbook and Web site, <a href="http://www.studyabroad-cis.com">www.studyabroad-cis.com</a>. This is also a good time to discuss the idea of studying abroad with your family.
					</li>
					<li>
						Meet with your campus study-abroad advisor to determine the proper procedures for undertaking and getting approval for this experience.
					</li>
					<li>
						During this time you will want to inquire about the possibility of and procedures for using financial aid to study abroad, and to apply for a passport, if you do not already possess one.
					</li>
					<li>
						After institutional approval, complete and submit application materials to CIS. A completed application includes:
						
						<ul class="circle">
							<li>Study-abroad application form</li>
							<li>Official transcript</li>
							<li>Personal statement</li>
							<li>Course-selection worksheet</li>
							<li>Recommendation form</li>
							<li>$50 application fee</li>
						</ul>
					</li>
					<li>
						If applying for an internship, please submit the following in addition to the study-abroad application:
						<ul class="circle">
							<li>Résumé and cover letter</li>
							<li>Reference from an employer</li>
							<li>Additional statement addressing your reason for undertaking an internship</li>
						</ul>
					</li>
					<li>
						Within two to three weeks of submitting a completed application, you will receive notification of the acceptance decision. If you have been accepted, you will receive the CIS acceptance packet.
					</li>
					<li>
						Within two weeks of receiving your acceptance packet, you will need to accept the offer from CIS by submitting all necessary forms (included in the packet) along with a program deposit. (The amount varies from $500 to $900 depending on the program) and a housing-damage deposit. (The amount varies from $250 to $900 depending on the program.)
					</li>
					<li>
						After receiving your completed acceptance forms, CIS will reserve your place in the chosen program and continue to work with you on matters of financial aid, housing, flight arrangements, and visas (if applicable).
					</li>
					<li>
						By the deadlines listed in the policies and procedures section of, final payment or arrangement for final payment must be completed.
					</li>
					<li>
						Prior to departure, CIS will send you detailed predeparture information and ensure that you are well prepared for your study-abroad program. This predeparture information and orientation will cover practical matters such as what to pack, money, travel tips, health and safety, etc. as well as provide cultural-adjustment tips.
					</li>
					<li>
						Depart for the program and have a successful, life-changing experience!
					</li>
				</ul>
				<h3>The importance of saving your application</h3>
				<div class="important_icon">
					<span class="display_none">Important Icon</span>
				</div>
				<div class="important_text">
					<p>
						Tell users what will happen to their application if they won’t save. Some dummy text for now. Phasellus placerat rhoncus purus. In pulvinar tincidunt mi, accumsan fermentum neque euismod ultrices. Suspendisse ut ante a ipsum faucibus auctor. 
					</p>
				</div>
			</div>
		


</div>