<?php
	require_once('Class.HtmlHelpers.php');
	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');

	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.HousingOptionsPeer.php');
	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.DurationOptionsPeer.php');
	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.UserCouponPeer.php');

	$sp = " ";
	$spFeed1 = 40;
	$spFeed2 = 8;
	$linebreak = chr(13).chr(10);

	$yrInSchool = array('1' => 'Freshman', '2' => 'Sophomore', '3' => 'Junior', '4' => 'Senior', '5' => 'College/University Graduate', '6' => 'High School Graduate');
	$housingOption = HousingOptionsPeer::retrieveByPk($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getHousingOptionID());
	$durationOption = DurationOptionsPeer::retrieveByPk($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getDurationOptionID());

	$userCoupon = UserCouponPeer::retrieveByPersonalInfoID($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getID());

	// $approvedCoupons = UserCouponPeer::getApprovedCouponCodes();

	$couponCode = "";
	if(!is_null($userCoupon)){
		if(!is_null($couponCode)){
		// if(in_array($userCoupon->getName(),$approvedCoupons)){
			$couponCode = $userCoupon->getName();
		}
		else {
			$couponCode = $userCoupon->getName() . " - (wrong code)";
		}
	}


	/*additional info */
		$getcampus = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getCampusRep();

		$getcampus2 = "";
		$getcampus3 = "";
		if ($getcampus == ""){
			$getcampus2 = "No";
		}
		else{
		$getcampus2 = "Yes";
		$getcampus3 = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getCampusRep();
		}

		$getfriendgrant =  $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFriendGrant();
		$getfriendgrant2 = "";
		$getfriendgrant3 = "";

		if ($getfriendgrant == ""){
			$getfriendgrant2 = "No";
		}
		else{
		$getfriendgrant2 = "Yes";
		$getfriendgrant3 = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFriendGrant();
		}
		$goAgainGrant = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getGoAgainGrant();


?>
<?=$linebreak?>
<?=$linebreak?>
----------------------------------------------<?=$linebreak?>
Personal Information<?=$linebreak?>
----------------------------------------------<?=$linebreak?>
<?=$linebreak?>
Name<?=str_repeat($sp,$spFeed1-strlen('Name'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getName(); ?> <?=$linebreak?>
Preferred Name<?=str_repeat($sp,$spFeed1-strlen('Preferred Name'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getPreferredName(); ?> <?=$linebreak?>
Gender<?=str_repeat($sp,$spFeed1-strlen('Gender'))?>:<?=str_repeat($sp,$spFeed2)?><?=$gender = (1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getGender()) ? 'Male' : 'Female'; ?> <?=$linebreak?>
Birthdate<?=str_repeat($sp,$spFeed1-strlen('Birthdate'))?>:<?=str_repeat($sp,$spFeed2)?><?=Date("F d, Y", strtotime($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBirthday())); ?> <?=$linebreak?>
Email Address<?=str_repeat($sp,$spFeed1-strlen('Email Address'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail(); ?> <?=$linebreak?>
Alternate Email<?=str_repeat($sp,$spFeed1-strlen('Alternate Email'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getAltEmail(); ?> <?=$linebreak?>
---Address---<?=$linebreak?>
Street Address<?=str_repeat($sp,$spFeed1-strlen('Street Address'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1(); ?> <?=$linebreak?>
City<?=str_repeat($sp,$spFeed1-strlen('City'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1(); ?> <?=$linebreak?>
State<?=str_repeat($sp,$spFeed1-strlen('State'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1(); ?> <?=$linebreak?>
Zip<?=str_repeat($sp,$spFeed1-strlen('Zip'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1(); ?> <?=$linebreak?>
Phone<?=str_repeat($sp,$spFeed1-strlen('Phone'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1(); ?> <?=$linebreak?>
Cellphone<?=str_repeat($sp,$spFeed1-strlen('Cellphone'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCellphone();?><?=$linebreak?>
<?=$linebreak?>
----------------------------------------------<?=$linebreak?>
School Information<?=$linebreak?>
----------------------------------------------<?=$linebreak?>
<?=$linebreak?>
Name of school you currently attend<?=str_repeat($sp,$spFeed1-strlen('Name of school you currently attend'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName() : 'n/a' ?> <?=$linebreak?>
School State<?=str_repeat($sp,$spFeed1-strlen('School State'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getStateAbbr() : 'n/a' ?> <?=$linebreak?>
On-campus study abroad contact<?=str_repeat($sp,$spFeed1-strlen('On-campus study abroad contact'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getContact() ?> <?=$linebreak?>
Email<?=str_repeat($sp,$spFeed1-strlen('Email'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getEmail() ?> <?=$linebreak?>

Major<?=str_repeat($sp,$spFeed1-strlen('Major'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMajor() : 'n/a' ?> <?=$linebreak?>
Year in School<?=str_repeat($sp,$spFeed1-strlen('Year in School'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $yrInSchool[$objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getYearInSchool()] : 'n/a' ?> <?=$linebreak?>
Current Cumulative GPA<?=str_repeat($sp,$spFeed1-strlen('Current Cumulative GPA'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getGenAverage() : 'n/a' ?> <?=$linebreak?>
<?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
Additional Information <?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
<?=$linebreak?>
How did you learn about the CIS Program you're applying for?<?=$linebreak?>
<?=$linebreak?>
<?=isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ?
		($learn = (0 < $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom()
			? CISOnlineAppConst::$addInfoOptKeys[$objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom()]
			: $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers()))
		: 'n/a'
?>
<?=$linebreak?>
<?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
Study Abroad Program Information <?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
<?=$linebreak?>
Term Applying for<?=str_repeat($sp,$spFeed1-strlen('Term Applying for'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTermName() : 'n/a' ?> <?=$linebreak?>
Year Applying for<?=str_repeat($sp,$spFeed1-strlen('Year Applying for'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear() : 'n/a' ?> <?=$linebreak?>
Program Applying For<?=str_repeat($sp,$spFeed1-strlen('Program Applying For'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramName() : 'n/a'; ?> <?=$linebreak?>
Program Option<?=str_repeat($sp,$spFeed1-strlen('Program Option'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionName() : 'n/a' ?> <?=$linebreak?>
Housing Option<?=str_repeat($sp,$spFeed1-strlen('Housing Option'))?>:<?=str_repeat($sp,$spFeed2)?><?=(!is_null($housingOption)) ? $housingOption->getName() : ""?><?=$linebreak?>
Duration Option<?=str_repeat($sp,$spFeed1-strlen('Duration Option'))?>:<?=str_repeat($sp,$spFeed2)?><?=(!is_null($durationOption)) ? $durationOption->getName() : ""?><?=$linebreak?>
---Internship start date---<?=$linebreak?>
Month<?=str_repeat($sp,$spFeed1-strlen('Month'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartMonth()?><?=$linebreak?>
Day<?=str_repeat($sp,$spFeed1-strlen('Day'))?>:<?=str_repeat($sp,$spFeed2)?><?=(0 < $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartDay()) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartDay() : ""?><?=$linebreak?>
<?=$linebreak?>
<?=$linebreak?>
Date of Application<?=str_repeat($sp,$spFeed1-strlen('Date of Application'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::AGREEMENT]) ? Date("Y-M-d", strtotime($objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getApplicationDate())) : 'n/a' ?> <?=$linebreak?>
Last Change Date<?=str_repeat($sp,$spFeed1-strlen('Last Change Date'))?>:<?=str_repeat($sp,$spFeed2)?><?=Date('d M Y')?><?=$linebreak?>
Coupon Code<?=str_repeat($sp,$spFeed1-strlen('Coupon Code'))?>:<?=str_repeat($sp,$spFeed2)?><?=$couponCode?><?=$linebreak?>
<?=$linebreak?>
<?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
Other Info <?=$linebreak?>
---------------------------------------------- <?=$linebreak?>

Billing Email<?=str_repeat($sp,$spFeed1-strlen('Billing Email'))?>:<?=str_repeat($sp,$spFeed2)?><?=!is_null($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBillingEmail()) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBillingEmail():'N/A' ?> <?=$linebreak?>
Financial Aid<?=str_repeat($sp,$spFeed1-strlen('Financial Aid'))?>:<?=str_repeat($sp,$spFeed2)?><?=(0 == $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFinancialAid()) ? 'No' : 'Yes' ?> <?=$linebreak?>
CIS Campus Rep/Alumni Referral<?=str_repeat($sp,$spFeed1-strlen('CIS Campus Rep/Alumni Referral'))?>:<?=str_repeat($sp,$spFeed2)?><?=$getcampus2 ?> <?=$linebreak?>
Name of CIS Campus Rep/Alumni<?=str_repeat($sp,$spFeed1-strlen('Name of CIS Campus Rep/Alumni'))?>:<?=str_repeat($sp,$spFeed2)?><?=$getcampus3 ?> <?=$linebreak?>
Bring a Friend Grant<?=str_repeat($sp,$spFeed1-strlen('Bring a Friend Grant'))?>:<?=str_repeat($sp,$spFeed2)?><?=$getfriendgrant2 ?> <?=$linebreak?>
Name of Friend<?=str_repeat($sp,$spFeed1-strlen('Name of Friend'))?>:<?=str_repeat($sp,$spFeed2)?><?=$getfriendgrant3 ?> <?=$linebreak?>
Go Again Grant<?=str_repeat($sp,$spFeed1-strlen('Go Again Grant'))?>:<?=str_repeat($sp,$spFeed2)?><?=$goAgainGrant ?> <?=$linebreak?>
<?=$linebreak?>
Shirt Size<?=str_repeat($sp,$spFeedi1-strlen('Shirt Size'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getShirtSize() : 'n/a' ?> <?=$linebreak?>
Additional Comments:
<?=$linebreak?>
<?=isset($comment) ? $comment : '' ?><?=$linebreak?>
<?=$linebreak?>
