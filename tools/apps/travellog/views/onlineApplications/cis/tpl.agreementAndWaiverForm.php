<?php
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
	Template::includeDependentJs('/js/prototype.js');
	//Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator-1.1.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
?>			

<?/*
<script type="text/javascript">
	jQuery.noConflict();
</script>

*/?>


<!-- Rotating Photo -->

<div id="content_wrapper" class="yui-skin-sam ltr noTopPadding">
<div id="wide_column" class="layout2">
	<div class="section_detail">
		<div class="content">
			<h2>Agreement and Waiver</h2>
			
			<p class="paddedOnBottom">
			<?/*	Your electronic signature on this on-line application form indicates your understanding and acceptance of the following: */ ?>
			</p>
			
			<ul class="disc">
				<li>
					I certify that all of the above information is correct, and I agree to stand by the financial, academic, and conduct policies and procedures set forth by the Center for International Studies (CIS) and partner institutions.
				</li>
				<li>
					As CIS will be working on my behalf, I hereby authorize the release of my application and other records to its affiliated foreign institutions.
				</li>
				<li>
					I authorize the appropriate officials of my overseas host institution(s) to forward official transcripts of the academic work I complete while abroad to CIS. They (CIS) will then release this information to the appropriate officials at my home institution.
				</li>
				<li>
					As CIS will be working on my behalf in regards to emergency response and assistance, I hereby authorize the release of my personal information regarding my health and safety to my family members, government officials, staff members, and any other relevant parties as CIS determines to be in my best interest.
				</li>
				<li>
					Furthermore, I understand that CIS and its affiliated institutions, in arranging these programs, act only as agents. As such, neither CIS nor any of its employees, or persons, parties, organizations, or agencies collaborating with them, is or shall be responsible or liable for injury, loss, damage, deviation, delay, curtailment, however caused, or the consequences thereof which may occur during any travel or program. CIS and the sponsoring institution reserve the right to cancel or alter any program or course for any reason.
				</li>
			</ul>
			<form action='OnlineApplication.php?step=5&action=save&pID=<?=$pID?>' method='post' id="agreementForm">
				<fieldset class="contentBlock first">
					<legend class="display_none">siganture and date</legend>
					<ul class="form paddingof10 border_bottom_dotted">
							<li class="paddedOnTop line wrap paddedOnBottom">
								<?php $signature = (isset($objArr[CISOnlineAppFieldsMapper::AGREEMENT]) ? $objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getSignature() : ''); ?>
								<label class="setMediumText strong"><input type="checkbox" id="signature" name="Agreement[Signature]" <?php if($signature!=''):?>checked<?php endif; ?> > <!-- I understand and agree to the above statements. --> I have read, understand, and agree to the Application Agreement and Waiver</label>
							<? /*	<?php $signature = (isset($objArr[CISOnlineAppFieldsMapper::AGREEMENT]) ? $objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getSignature() : ''); ?>
								<div class="unit size50">
									<label for="signature" class="setMediumText strong">Signature of applicant  <span class="required">*</span></label>
									<input type="text" id="signature" class="text size95" size="23" name="Agreement[Signature]" value="<?=$signature?>"/>																								
								</div> */ ?>
							</li>
							<li class="paddedOnTop line wrap paddedOnBottom20">
								<?php 
									$dateApplied = Date("l F d, Y");
									if(isset($objArr[CISOnlineAppFieldsMapper::AGREEMENT])):
										$dateApplied = $objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getApplicationDate();
										$dateApplied = strtotime($dateApplied);
										$dateApplied = Date("l F d, Y", $dateApplied);
									endif;
								?>
								<div class="unit size50">
									<label for="date" class="setMediumText strong">Date of application   <span class="required">*</span></label>
									<input type="text" id="date" class="text size95" size="23" name="Agreement[ApplicationDate]" value="<?=$dateApplied?> " disabled="disabled"/>
								</div>
							</li>
												
					</ul>
				</fieldset>
				<div class="wrap paddedOnTop20 right">
				<? /*	<a class="button_v3" href="OnlineApplication.php?step=4&action=view&pID=<?=$pID?>&backTrack"  > */?>
					<a class="button_v3" onclick="return backTrackAction('agreementForm')" >
						<strong>Back</strong>
					</a>
				
					<input type="submit" value="Next/Save" id="submit" name="<?php echo $x = (isset($buttonName) ? $buttonName : 'Submit');?>" class="submit button_v3 ga_interactive_form_field" onclick="return validateAgreementForm()"/>
				
					<a class="button_v3" href="javascript:void(0);" onclick="return saveAndExit('agreementForm')">
						<strong>Save and Quit</strong>

					</a>											
				</div>
			</form>
			
			

		</div>
	


</div>		

</div>
<div id="narrow_column">
	<?php echo $applicationProgressMenu?>
</div>
</div>
