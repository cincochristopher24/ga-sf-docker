<?php
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
	Template::includeDependentJs('/js/prototype.js');
	//Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator-1.1.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
$incjs = <<<BOF
<script type="text/javascript">
	//<![CDATA[
	function cnt(w, e){
		var y = w.value;
		var r = 0;
		var a = y.replace(/\s/g,' ');
		a = a.split(' ');
		
		if (window.event)
		   key = window.event.keyCode;
		else if (e)
		   key = e.which;
		
		if(a.length > 200){
			if(key == 8){
				return true;
			}
			return false;
		}
	}
	
	function isLateOption(){
		var chckBox = $('chckStatement').checked;
		
		if(chckBox == false){
			return validateStatementForm();
		}
		
		return true;
	}
	
	function enableDisableForm(){
		var chckBox = $('chckStatement');
		if(chckBox.checked == true){
		//	$('relate').value = "";
			$('relate').disabled = true;
		//	$('goals').value = "";
			$('goals').disabled = true;
		//	$('expectations').value = "";
			$('expectations').disabled = true;
		}
		else {
			$('relate').disabled = false;
			$('goals').disabled = false;
			$('expectations').disabled = false;
		}
	}
	//]]>
</script>
BOF;

		Template::includeDependent($incjs);
?>			

<!-- displays the profile component: added by neri 11-04-08 -->
<? //$profile->render()?>
<!-- end -->
	
<?  //$subNavigation->show(); ?>

<div id="content_wrapper" class="yui-skin-sam ltr noTopPadding">
	<div id="wide_column" class="layout2">
		<div class="section_detail">
			<div class="content">
				<h2>Personal Statement</h2>
				
				<p class="border_bottom_dotted paddedOnBottom20">
					Part of the mission of CIS is to provide innovative education programs that encourage personal development. While this development can be difficult to measure, we would like to help you get a sense of how you have been changed by your study-abroad experience. Thus, as part of the application process we ask you to provide thoughtful responses to the questions below. These answers are not only part of the admission procedure but also, at the end of your program, you will receive a copy of your responses along with resources to help you process how the study abroad experience affected you.
				</p>
				<form method="post" action="OnlineApplication.php?step=3&action=save&pID=<?=$pID?>" id="personalStatementForm">
				
					<fieldset class="contentBlock first">
						<legend class="display_none">Personal Statement</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line wrap paddedOnBottom">	
								<?php $statement1 = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement1() : ''); ?>
								<div class="paddedOnTop20 border_bottom_dotted">
									<label class="setMediumText"><input type='checkbox' name="PersonalStatement[chckStatement]" id="chckStatement" onclick="enableDisableForm()"/> Check here if you will be submitting your personal statement at a later time.</label>
								</div>
								<div class="paddedOnTop20 unit">
									<label class="setMediumText strong" for="relate">How does the content of the study abroad program you are applying to relate to your present and future academic and career goals? <span class="fcGray">(150-200 words)</span> <span class="required">*</span></label>
									<textarea rows="8" cols="30" name="PersonalStatement[Statement1]" class="textarea size95" id="relate" onkeypress="return cnt(this, event)"><?=$statement1?></textarea>
								</div>
							</li>
								
							<li class="line wrap paddedOnBottom">
								<?php $statement2 = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement2() : ''); ?>
								<div class="paddedOnTop unit">
									<label class="setMediumText strong" for="goals">List and explain four goals relating to academics and/or cultural understanding you hope to achieve during your study abroad experience.<span class="fcGray">(150-200 words)</span> <span class="required">*</span></label>
									<textarea rows="8" cols="30" name="PersonalStatement[Statement2]" class="textarea size95" id="goals" onkeypress="return cnt(this, event)"><?=$statement2?></textarea>
								</div>
							</li>			
							
							<li class="line wrap paddedOnBottom20">	
								<?php $statement3 = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement3() : ''); ?>
								<div class="paddedOnTop unit">
									<label class="setMediumText strong" for="expectations">List and explain four expectations for your own personal growth/change you hope to experience as a result of your participation in this study abroad program. <span class="fcGray">(150-200 words)</span> <span class="required">*</span></label>
									<textarea rows="8" cols="30" name="PersonalStatement[Statement3]" class="textarea size95" id="expectations" onkeypress="return cnt(this, event)"><?=$statement3?></textarea>
								</div>
							</li>		
						</ul>
					</fieldset>
					
					<div class="wrap paddedOnTop20 right">
					<? /*	<a class="button_v3" href="OnlineApplication.php?step=2&action=view&pID=<?=$pID?>&backTrack"  > */ ?>
						<a class="button_v3" onclick="return backTrackAction('personalStatementForm')"  >
							<strong>Back</strong>
						</a>
						
						<input type="submit" value="Next/Save" id="submit" name="<?php echo $x = (isset($buttonName) ? $buttonName : 'Submit');?>" class="submit button_v3 ga_interactive_form_field" <? /*onclick="return validateStatementForm()"*/?> onclick="return isLateOption()" />
						
						<a class="button_v3" href="javascript:void(0);" onclick="return saveAndExit('personalStatementForm')"  >
							<strong>Save and Quit</strong>
						</a>											
					</div>
				</form>
	
			</div>
		
</div>		
	
	</div>
	<div id="narrow_column">
		<?php echo $applicationProgressMenu?>	  

	</div>
</div>


		
