<?php
	//$sess = SessionManager::getInstance();
	//$sess->unsetVar('onlineAppUserID');

	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	//ob_start();
?>


<div class="area" id="intro">
	<h2 class="border_bottom_solid">APPLICATION SUBMITTED! </h2>
	<div class="section_detail">
		<div class="content paddedOnTop20">
				<p> Thank you for submitting your online application and deposit to CISabroad! </p>
                <p>Our staff will be in touch with you within 2 business days to follow up on your next steps, and to answer any questions you might have. 
                </p>
				<p>If you’d like to get started right away on submitting the additional pieces of your application, please download the application instructions at the <a href= "http://www.cisabroad.com/apply-now"> CIS Apply Now</a>  page. </p>
                <p> Also, be sure to read up on CIS Policies and Procedures (download <a href="http://www.cisabroad.com/files/cis_semester_abroad_policies_procedures.pdf">Semester Abroad Policies pdf</a>, <a href="http://www.cisabroad.com/files/cis_short_term_program_policies_procedures.pdf">Short Term Policies pdf</a>) so that you understand our refund policy, deadlines, procedures, expectations, etc.  </p>
		</div>
	</div>
</div>

