<?php

Template::setMainTemplateVar('title', $pageTitle);
Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
Template::includeDependentJs('/js/prototype.js');
Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');


ob_start();

$captcha = Captcha::createImage();

$disableFields = false;
if(isset($errMsg['displayPopUp']) AND $errMsg['displayPopUp'] == true){
	$style = "border: 1px solid black; width: 600px; height:100px; left: 350px; top: 300px; position: absolute; background-color: white;";
	$disableFields = true;
	$recipientEmail = isset($errMsg['mailRecipient']) ? $errMsg['mailRecipient'] : '';
}
else {
	$style = "display:none;";
}


?>
<div id="body">
	<div id="content_wrapper" class="yui-skin-sam ltr">
		<div id="wide_column" class="layout2">
			<div class="section_detail">
				<div class="content">
					<label class="">Please enter your correct email below :</label>
					<form action="?action=retrievePassword" method="POST">
					<ul class="form paddingof10">
						<li class="line paddedOnTop size50">
							<label for="txtEmail" class="setMediumText strong">Email <span class="required">*</span><span id="errMsg1" class="required"><?php echo $emailErrMsg = (isset($errMsg['email']) ? $errMsg['email'] : '');?></span></label>
							<input name="txtEmail" class="text size90" id="txtEmail1" type="text" value="<?php echo $emailVal = (isset($errMsg['emailVal']) ? $errMsg['emailVal'] : '');?>" <?php if($disableFields):?>disabled<?php endif;?> >
							
						</li>
						
					</ul>
					<label class="required"><?php echo $errmsg = (isset($errMsg['captcha']) ? $errMsg['captcha'] : '');?></label>
					<label class="">Please enter correctly the code below: </label>
					<ul class="form paddingof10">
						<li class="line paddedOnTop size40">
							<input type="text" class="text size50 paddedOnBottom" name="inputSecCode" <?php if($disableFields):?>disabled<?php endif;?> /><br/>
							<input type="hidden" id="captchaVal" name="securityCode" value='<?=$captcha['code']?>'>
						</li>
						<li class="line paddedOnTop size40">
							<img src="<?=$captcha['src']?>">
						</li>
					<div class="wrap paddedOnTop20">
						<input type="submit" value="Submit" name="Submit" id='submit' class="submit button_v3 ga_interactive_form_field" <?php if($disableFields):?>disabled<?php endif;?> />
						
					</div>
					</form>
				</div>
			</div>
		</div>
		
	</div>
</div>



<div style="<?=$style?>">
	<p align="center" class="paddedOnTop">
		<label>Your username and password were successfully sent to  <a href="mailto: <?=$recipientEmail?>"><strong><?=$recipientEmail?></strong></a>. <br/>Please check your email in a few minutes and return to login to our system.</label>
		<a class="button_v3" href="" >
			<strong>Ok</strong>

		</a>
	</p>
</div>