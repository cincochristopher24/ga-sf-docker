<?php
	$sess = SessionManager::getInstance();
	$sess->unsetVar('onlineAppUserID');

	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
?>


<div class="area" id="intro">
	<h2 class="border_bottom_solid">Online Application Submitted! </h2>
	<div class="section_detail">
		<div class="content paddedOnTop20">
				<p> Thank you for submitting your CIS Online Application.</p>
                <p>Your school has a direct billing arrangement with CIS and is paying the $200 application deposit on your behalf. Pretty nice! Please talk with your study abroad office for more information about this arrangement.
                </p>
				<p>You will hear from a CIS staff member soon regarding the next steps.</p>
				<p>If you would like to begin working immediately on the next steps, click on the links below to download a copy of the application instructions. </p>
				<p><a href = "http://www.cisabroad.com/files/cis-study-abroad-application-instructions.pdf" target = "_balnk"> Study abroad application instructions </a> </p>
				<p><a href = "http://www.cisabroad.com/files/cis-internship-application-instructions.pdf" target = "_balnk" >Intern abroad application instructions</a> </p>
				<p> These instructions are also located on the <a href= "http://www.cisabroad.com/apply-now" target = "_balnk"> “Apply Now” </a>   page of the CIS website.</p>

		</div>
	</div>
</div>