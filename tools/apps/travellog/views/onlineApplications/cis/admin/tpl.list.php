<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.PersonalInfoMapper.php');
	$sess = SessionManager::getInstance();
	$sess->unsetVar('onlineAppUserID');
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if(isset($groupID) and $groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
	//	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
	Template::includeDependentCss('/min/f=css/onlineApplications/admin.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
$incjs = <<<BOF
<script type="text/javascript">
	//<![CDATA[
	function pagination(page){
		var filterOptTxt = jQuery('#filterOptTxt').val();
		
		if(filterOptTxt == 'all' || filterOptTxt == ''){
			var filterOptQstring = 'action=view';
		}
		else {
			var filterOptQstring = 'action=view&filterBy='+filterOptTxt;
		}
		
		window.location = '?'+page+filterOptQstring;
	}
	
	function filterOption(){
		
	}

	//]]>
</script>
BOF;
	Template::includeDependent($incjs);

?>


<div class="area" id="intro">
	<h2 class="border_bottom_solid">List of applicants</h2>
	<div class="section_detail">
		<div class="content paddedOnTop20">
			<table width='100%'>
				<tr class="border_bottom_solid">
					<td colspan='5'>
						<form action='?action=view' method='POST'>
							Search by : 
						<select name='searchBy'>
							<option value='name' <?php echo ($searchby == 'name') ? 'selected' : '' ?> >Name</option>
							<option value='email' <?php echo ($searchby == 'email') ? 'selected' : '' ?>>Email</option>
						</select>
						<input type='text' name='qryString' class="text size20" value='<?=$searchString?>' />
							Filter by : 
						<select name='filterOpt'>
							<option value='all' <?php echo ('all' == $filterOpt) ? 'selected' : '' ?> >No Filter</option>
							<option value='finished' <?php echo ('finished' == $filterOpt) ? 'selected' : '' ?> >Finished</option>
							<option value='unfinished' <?php echo ('unfinished' == $filterOpt) ? 'selected' : '' ?> >Unfinished</option>
						</select>
						<input type='submit' name='submit' value='Go'>
						</form>
					</td>
					
				</tr>
				<tr id='row1'><th>ID</th><th>Name </th> <th>Email </th> <th>Password </th><th>Submitted </th><th>Date Submitted </th><th>Download<br/>Application </th> </tr>
				<?php	
					$cnt = 1;
					$totalList = $iterator->count();
					$iterator->rewind();
					
					if(!$iterator->valid()): ?>
					<tr id='emptyList'><td colspan='5'><h3>NO SEARCH RESULT FOUND</h3> Try to enter another search key or <a href="?action=view">return to lists</a>.</td></tr>
				<?php
					endif;
				
					while($iterator->valid()):
						$user = $iterator->current();
			//		$totalList = count($usersList);
			//		foreach($usersList as $user): ?>
					
				<tr id="<?php echo ($cnt == $totalList) ? 'lastRow' : ''?>">
					<td><?=$user->getID()?></td>
					<td><?=$user->getName()?></td>
					<td><?=$user->getEmail()?></td>
					<td><?=$user->getPassword()?></td>
					<td><?php echo $isDone = (1 == $user->getIsDone() ? 'Yes': 'No') ?></td>
					<?php $date = ('0000-00-00 00:00:00' == $user->getDateSubmitted()) ? $user->getDateApplied() : $user->getDateSubmitted() ?>
					<td><?php echo $user->getDateApplied(); /*echo ($isDone == 'Yes') ? $date : ''*/ ?></td>
					<td>
					<?php
						$applicantName = $user->getName();

						$applicantName = str_replace(' ', '_', $applicantName); // Replaces all spaces with underscore.
						$applicantName = preg_replace('/[^A-Za-z0-9_]/', '', $applicantName); // Removes special chars.
					  $applicantName = preg_replace('/_+/', '_', $applicantName); //Remove duplicate underscore
						$hrefTxt = '';
						$hrefCsv = '';
						$hrefTitle = 'Unable to download unfinished application.';
						$appID = $user->getID();
						// if(1 == $user->getIsDone()):
							$hrefTxt = "target='_blank' href='?action=download&appID=$appID&filename=$applicantName.txt'";
							$hrefCsv = "target='_blank' href='?action=download&appID=$appID&filename=$applicantName.csv'";
							$hrefTitle = '';
						
						?>
					<a <?=$hrefTxt?> title='<?=$hrefTitle?>'>.txt</a> - <a <?=$hrefCsv?> title='<?=$hrefTitle?>'>.csv</a>
					
					<?//php else : ?>
						<!-- Unfinished -->
					<?//php endif;?>
					
					</td>
				</tr>
				<?php
				 	$cnt++;
					$iterator->next();
					endwhile;
				//	endforeach; ?>
				<tr id='pagination'><td colspan='5'>
					<div class='pagination'>
						<input type="hidden" id="filterOptTxt" name="filterOptTxt" value="<?=$filterOpt?>" readonly='true'>
					<?php if ( $paging->getTotalPages() > 1 ):?>
							<? $paging->getFirst(); 	?>
							<? $paging->getPrevious(); 	?>
							<? $paging->getLinks(); 	?>
							<? $paging->getNext(); 		?>
							<? $paging->getLast(); 		?>
					<?php endif; ?>
					</div>
				</td></tr>
			</table>
		</div>
	</div>
</div>