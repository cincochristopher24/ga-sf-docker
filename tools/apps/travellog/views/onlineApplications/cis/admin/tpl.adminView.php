<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.PersonalInfoMapper.php');
	$sess = SessionManager::getInstance();
	$sess->unsetVar('onlineAppUserID');
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if(isset($groupID) and $groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
	//	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
	Template::includeDependentCss('/min/f=css/onlineApplications/admin.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();

?>
<div class="area" id="intro">
	<h2 class="border_bottom_solid"></h2>
	<div class="section_detail">
		<div class="content paddedOnTop20">
			<div><a href="index.php?action=viewapplicants">View Applicants List</a></div>
			<div><a href="index.php?action=viewpartners">View Partner Schools List</a></div>
		</div>
	</div>
</div>