<?php
	Template::setMainTemplateVar('title', $pageTitle);

	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');

	//Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');

//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');

	/*
	Template::includeDependentCss('http://www.goabroad.net/min/f=css/onlineApplications/online_application.css');

	Template::includeDependentCss('http://www.goabroad.net/min/b=css&amp;f=reset.css,type.css,layout.css,widget.css,section.css,forms.css');
	Template::includeDependentCss('http://www.goabroad.net/custom/CIS/css/main.css');
	Template::includeDependentCss('http://www.goabroad.net/min/f=css/cobrand_customhome.css');

	Template::includeDependent('
<style type="text/css">
	@import url("http://www.goabroad.net/css/online_application.css");
</style>');
	*/
/**
 * From tpl.LayoutMain
 */

/*
<link type="text/css" rel="stylesheet" href="/min/b=css&amp;f=reset.css,type.css,layout.css,widget.css,section.css,forms.css" />

<style type="text/css">
@import url("/css/<?=$layoutID?>.css");
</style>

<link href="<?=$obj_factory->getPath('CSS')?>main.css" rel="stylesheet" type="text/css" />

<?//php Template::includeDependentCss("/min/f=css/cobrand_customhome.css"); ?>

<link type="text/css" rel="stylesheet" href="/min/f=css/cobrand_customhome.css" />

*/

	ob_start();
?>





<div class="area" id="intro">
	<div class="section_detail">
		<div class="content paddedOnTop20">
			<h3>Here’s how the CIS On-line Application works:</h3>
			<ul class="disc">
			</ul>
			<label><strong style="font-size:15px;">Step 1:</strong> Complete the online application</label>
			<ul class="disc">
				<li>Personal Information - all about you</li>
				<li>Program Information - when/where you're going</li>
				<li>Pay the application fee (please have your credit card ready)</li>

			</ul>
			<ul class="disc">
			</ul>
			<label><strong style="font-size:15px;">Step 2:</strong> Submit additional documents to complete your application</label>
			<ul class="disc">
				<li>Course Selection Worksheet</li>
				<li>Academic Reference</li>
				<li>Official Transcript</li>
				<li>Applicant Profile</li>
				<li>CIS Scholarship Application (optional)</li>
			</ul>
			<h3>What you will need to get started:</h3>
			<ul class="disc">
				<li>Credit Card</li>
				<li>Current GPA</li>
				<li>Name, term, and year for the program to which you are applying</li>
				<!--li>Passport (if you have one)</li-->
			</ul>
			<h3>Helpful Hints</h3>
			<p>
				<strong>Be sure to consult with your study abroad office and/or academic advisor before applying.</strong> They may have their own application process or requirements that you need to follow. They can also provide guidance on getting your chosen study abroad program approved for credit transfer and financial aid, as well as other helpful information about working with your home university or college.
			</p>
			<p>
				<strong>Research your program.</strong> Be sure to research the program you’d like to attend <!--at www.studyabroad-cis.com--> to learn about the destination, application deadlines, eligibility requirements, dates, and more. Not sure which program is a good fit for you? Call us toll-free at 877-617-9090 or email us at info@cisabroad.com. We’re happy to help!
			</p>

			<p>
				<strong>Familiarize yourself with the nitty-gritty details.</strong> Read up on CIS Policies and Procedures (download <a href="http://www.cisabroad.com/files/cis_semester_abroad_policies_procedures.pdf">Semester Abroad Policies pdf</a>,
				<a href="http://www.cisabroad.com/files/cis_short_term_program_policies_procedures.pdf">Short Term Policies pdf</a>) so that you understand our refund policy, deadlines, procedures, expectations, etc.
			</p>



			<div class="wrap paddedOnTop20 right border_top_dotted">
				<a class="button_v3 smallorange" href="?step=1&action=view">
					<strong>Continue to your Application</strong>
				</a>
			<?/*	<a class="button_v3" href="?action=viewChecklist">
					<strong>View Checklist</strong>
				</a> */ ?>
			</div>
		</div>
	</div>
</div>