<?php
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
	Template::includeDependentJs('/js/prototype.js');
	//Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator-1.1.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
	
	
$incjs = <<<BOF
<script type="text/javascript">
//<![CDATA[

	function showSecondProg(){
		alert('showSecondProg');
		var chkAddProg = $('chkAddProgram').checked;
		var divStyle = $('program2').style;
		
		if(chkAddProg==true){
			jQuery('#program2').slideDown("slow");
		}
		else {
			var program_2 = $('p2');
			var mOption2 = $('option2');
			program_2.options[0].selected = true;
			mOption2.options[0].selected = true;
			$('options_2').style.display = 'none';
			
			$('program1choice').checked = false;
			$('equally').checked = false;
			
			jQuery('#program2').slideUp("slow");
		}
	}
	
	function getProgramDuration(e){
		var programID = e.options[e.selectedIndex].value;
		var term = $('term');
		var termID = term.options[term.selectedIndex].value;
		ajaxGetDurationsByProgramID(programID,termID);
	//	alert(jQuery);
		
	}
	
	function ajaxGetDurationsByProgramID(progID, termID){
		var durationOptions = $('durationOption');
		var defaultVal = $('durationOption_default').value;
		
		jQuery.ajax({
			url: '/ajaxpages/CIS_onlineApplication/programDuration.php',
			data: { programID : progID, termID : termID },
			success: function(jsonResponse){
				
				if("NO_DURATION" == jsonResponse){
					durationOptions.options.length = 0;
					durationOptions.options[durationOptions.options.length] = new Option('No Option', '');
					$('durationOptionDiv').style.display = 'none';
					
				}
				else {
					$('durationOptionDiv').style.display = '';
					var durations = eval("(" + jsonResponse + ")");

					durationOptions.options.length = 0;

					durationOptions.options[durationOptions.options.length] = new Option('Choose duration', '');
					for(var i in durations){
						if(parseInt(i) >= 0){
							var optLength = durationOptions.options.length;
							durationOptions.options[durationOptions.options.length] = new Option(durations[i]['name'], durations[i]['id']);
							
							if(defaultVal == durations[i]['id']){
								durationOptions.options[optLength].selected = true;
							}
						}
					}
				}
				
			}
			});
	}

	function filterDuration(){
		
		var prog = $('p1');
		var term = $('term');
		var programID = prog.options[prog.selectedIndex].value;
		var termID = term.options[term.selectedIndex].value;
		ajaxGetDurationsByProgramID(programID, termID);
	}
	
	function updateStartDay(e){
		var dYear = $('year');
		var startDay = $('startDay');
		
		var startMonth = e.options[e.selectedIndex].value;
		var yearApplying = dYear.options[dYear.selectedIndex].value;
		
		var month=new Array(12);
		month[0]="January";
		month[1]="February";
		month[2]="March";
		month[3]="April";
		month[4]="May";
		month[5]="June";
		month[6]="July";
		month[7]="August";
		month[8]="September";
		month[9]="October";
		month[10]="November";
		month[11]="December";
		
		var dStartMonth = 0;
		for(var x in month){
			if(parseInt(x) >= 0){
				if(startMonth == month[x]){
					dStartMonth = x;
				}
			}
		}
		
		var days = daysInMonth(dStartMonth, yearApplying);
		
		startDay.options.length = 0;
		startDay.options[startDay.options.length] = new Option(" ","");
		for (var i = 1; i <= days; i++){
			startDay.options[startDay.options.length] = new Option(i,i);
		}
	}
	
	function daysInMonth(month,year) {
		//return 32 - new Date(year, month, 32).getDate();
	    return new Date(year, month, 0).getDate();
	}
	
	function filterAll(){
		var prog1 = $('p1');
		if(0 < prog1.options[prog1.options.selectedIndex].value){
			filterProgramsByTerm();
			filterDuration();
		}
		else {
			$('housingOptionDiv').style.display = "none";
			$('durationOptionDiv').style.display = "none";
			$('startDateDiv').style.display = "none";
		}
	}
	
	function showSpecifyField(){
		var refererOpt = $('referer');
		var selectedRef = refererOpt.options[refererOpt.options.selectedIndex].value;
		if(selectedRef == '' || selectedRef == 0){
			jQuery('#specifyField').slideDown("slow");
		}
		else {
			jQuery('#specifyField').slideUp("slow");
		}
	}
	
	function showCampusRep(){
		var CampusRepYes = $('CampusRepYes').checked;
		var CampusRepNo = $('CampusRepNo').checked;
		if(CampusRepYes == true){
			$('campus_rep_name').style.display = '';
		}
		else if(CampusRepNo == true) {
			$('campus_rep_name').style.display = 'none';
		}
	}
	
	function showFriendGrant(){
		var FriendGrantYes = $('FriendGrantYes').checked;
		var FriendGrantNo = $('FriendGrantNo').checked;
		if(FriendGrantYes == true){
			$('friend_grant_name').style.display = '';
		}
		else if(FriendGrantNo == true) {
			$('friend_grant_name').style.display = 'none';
		}
	}
	
	
Event.observe(window,'load',function(){
//	var prog1 = $('p1');
//	alert(prog1.options[prog1.options.selectedIndex].value);
//	if(prog1.options[prog1.options.selectedIndex].value == ''){
		filterAll();
		showSpecifyField();
		showCampusRep();
		showFriendGrant();
		
		//filterProgramsByTerm();
		//filterOption(1);
		//filterOption(2);
		//filterDuration();
//	}
	
	}
	);

//]]>
</script>
BOF;

		Template::includeDependent($incjs);
?>			





<!-- displays the profile component: added by neri 11-04-08 -->
<? //$profile->render()?>
<!-- end -->
	
<?  //$subNavigation->show(); ?>
		
<? /*
<script type="text/javascript">
		jQuery.noConflict();
</script>
*/ ?>



<!-- Rotating Photo -->
<div id="content_wrapper" class="yui-skin-sam ltr noTopPadding">
	<div id="wide_column" class="layout2">
		<div class="section_detail">
			<div class="content">

				<h2>Program Information</h2>
				<form method="post" action="OnlineApplication.php?step=2&action=save&pID=<?=$pID?>" id="programInformationForm">

					<fieldset class="contentBlock first">
						<legend class="display_none">Program Date</legend>
						<p class="title setSmallText dottedTopBorder">Fields with <span class="required">*</span> indicate required fields. </p>
						<ul class="form paddingof10 border_bottom_dotted">
								<li class="paddedOnTop line wrap">
									<?php $selectedTerm = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTerm() : ''); ?>
									<div class="unit size50">
										<label for="term" class="setMediumText strong">Term Applying for <span class="required">*</span></label>
										<select id="term" name="ProgramInfo[Term]" class="size50" onchange="filterProgramsByTerm()">
											<option value="">Choose a Term</option>
											<?php foreach($terms as $term): ?>
												<option value="<?=$term->getID()?>" <?php if($selectedTerm==$term->getID()):?>selected<?endif;?> ><?=$term->getName()?></option>
											<?php endforeach; ?>
											
										</select>																							
									</div>
								</li>
								
								<li class="line wrap paddedOnBottom">
									<?php $year = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear() : ''); ?>
									<div class="unit size50">
										<label for="year" class="setMediumText strong">Year Applying for <span class="required">*</span></label>
										<select id="year" name="ProgramInfo[Year]" class="size50">
											<option value="">Choose a Year</option>
											<option value="2010" <?php if($year==2010):?>selected<?endif;?> >2010</option>
											<option value="2011" <?php if($year==2011):?>selected<?endif;?> >2011</option>
												<option value="2011" <?php if($year==2012):?>selected<?endif;?> >2012</option>
										</select>																							
									</div>
								</li>						
						</ul>
					</fieldset>
					
					<script type="text/javascript" >
						
						var jsPrograms = <?= json_encode($jsPrograms); ?>;
						var jsOptions = <?= json_encode($jsOptions); ?>;
						var jsHousingOptions = <?= json_encode($jsHousingOptions); ?>
						
						function filterProgramsByTerm(){
							var termOptions = $('term');
							var program_1 = $('p1');
							var programToTerm = [];	
							
							var selectedTermID = termOptions.options[termOptions.options.selectedIndex].value;
							
							clearOptions();
							
							if(0 < selectedTermID){
								program_1.disabled = false;
								for(var i in jsPrograms){
									var terms = jsPrograms[i]['termID'];
									for(var x in terms){
										if(terms[x] == selectedTermID){
											programToTerm[i] = jsPrograms[i];
										}
									}
									
								}
							}
							else {
								program_1.disabled = true;
							}
							
							var selectedProg1 = program_1.options[program_1.options.selectedIndex].value;
							
						//	if(program_1.options[program_1.options.selectedIndex].value == ''){
								program_1.options.length = 0;

								program_1.options[program_1.options.length] = new Option('Choose Program...','');

								for(var i in programToTerm){
									if(parseInt(i) >= 0){
										var optProg1_length = program_1.options.length;
										
										program_1.options[program_1.options.length] = new Option(programToTerm[i]['name'],programToTerm[i]['id']);
										
										if(selectedProg1 == programToTerm[i]['id']){
											program_1.options[optProg1_length].selected = true;
											filterOption(1);
										}
										
									}
								}
								
						//	}
							
							
						}
						
						function filterOption(optNumber){
							var termOptions = $('term');
							var program = $('p'+optNumber);
							
							var selectedProgramID = program.options[program.options.selectedIndex].value;
							var selectedTermID = termOptions.options[termOptions.options.selectedIndex].value;
							
							var programOptions = [];
							var housingOptions = [];
							if(0 < selectedProgramID){
								//$('chkAddProgram').disabled = false;
								
								for(var i in jsOptions){
									var termID = jsOptions[i]['termID'];
									var programID = jsOptions[i]['programID'];
									
									if(selectedTermID == termID && selectedProgramID == programID){
										programOptions[i] = jsOptions[i];
									}
								}
								
								for(var a in jsPrograms){
									if(parseInt(a) >= 0){
										if(selectedProgramID == jsPrograms[a]['id']){
											if(jsPrograms[a]['hasStartDate'] == 1){
												$('startDateDiv').style.display = '';
											}
											else {
												var s_month = $('startMonth');
												var s_day = $('startDay');
												s_month.options[0].selected = true;
												s_day.options[0].selected = true;
												$('startDateDiv').style.display = 'none';
											}
										}
									}
								}
								
								
								for(var x in jsHousingOptions){
									var programID = jsHousingOptions[x]['programID'];
									var houseTermID = jsHousingOptions[x]['termID'];
									if(selectedProgramID == programID && selectedTermID == houseTermID){
										housingOptions[x] = jsHousingOptions[x];
									}
								}
							}
							
							distributeOptions(programOptions, optNumber);
						//	alert(housingOptions.length);
							distributeHousingOptions(housingOptions);
							filterDuration();
							
						}
						
						function distributeOptions(xProgramOptions, optNumber){
							var optionDiv = $('options_'+optNumber);
							var progOption = $('option'+optNumber);
							
							var p1 = $('option1');
							//optionDiv.style.display = "none";
							if(0 < xProgramOptions.length){
								
								optionDiv.style.display = "";
								
								var defaultVal = $('option'+optNumber+'_default').value;
							//	alert(progOption.name+' default value '+defaultVal);
								progOption.options.length = 0;
								
								progOption.options[progOption.options.length] = new Option('Choose Your Options', '');
								for(var i in xProgramOptions){
									if(parseInt(i) >= 0){
										var optLength = progOption.options.length;
										progOption.options[progOption.options.length] = new Option(xProgramOptions[i]['name'], xProgramOptions[i]['id']);
										if(defaultVal == xProgramOptions[i]['id']){
											progOption.options[optLength].selected = true;
										}
									}
								}
								
							}
							else {
								progOption.options.length = 0;
								
								progOption.options[progOption.options.length] = new Option('Choose Your Options', '');
								optionDiv.style.display = "none";
							}
							
						}
						
						function distributeHousingOptions(housingOptions){
							var hOptions = $('housingOption');
							var hasOptions = false;
							if(0 < housingOptions.length){
								hOptions.options.length = 0;
								hOptions.options[hOptions.options.length] = new Option('Choose housing option', '');
								
								var defaultVal = $('housing_default').value;
								
								for(var i in housingOptions){
									
									if(parseInt(i) >= 0){
										hasOptions = true;
										var optLength = hOptions.options.length;
										hOptions.options[hOptions.options.length] = new Option(housingOptions[i]['name'], housingOptions[i]['id']);
										
										if(defaultVal == housingOptions[i]['id']){
											hOptions.options[optLength].selected = true;
										}
									}
								}
							}
							
							if(hasOptions == false){
								hOptions.options.length = 0;
								hOptions.options[hOptions.options.length] = new Option('Choose housing option', '');
								$('housingOptionDiv').style.display = "none";
							}
							else {
								$('housingOptionDiv').style.display = "";
							}
						}
						
						function clearOptions(){
							var option1Div = $('options_1');
							var housingOptionDiv = $('housingOptionDiv');
							var durationOptionDiv = $('durationOptionDiv');
							var startDateDiv = $('startDateDiv');
							
							var progOption1 = $('option1');
							var housingOption = $('housingOption');
							var durationOption = $('durationOption');
						//	var startMonth = $('startMonth');
						//	var startDay = $('startDay');
							
							progOption1.options[0] = new Option('Choose Your Options', '');
							housingOption.options[0] = new Option('Choose housing option', '');
							durationOption.options[0] = new Option('Choose duration', '');
						//	startMonth.options[0].selected = true;
						//	startDay.options[0].selected = true;
							
							option1Div.style.display = "none";
							housingOptionDiv.style.display = "none";
							durationOptionDiv.style.display = "none";
							startDateDiv.style.display = "none";
							
						}
					</script>
					
					<fieldset class="contentBlock first">
						<legend class="display_none">Program Chosen</legend>
						<ul class="form paddingof10 border_bottom_dotted">
								<li class="line wrap paddedOnBottom">
									<?php $prog1 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramID() : ''); ?>
									<?php $prog2 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondProgramID() : ''); ?>
									<div class="paddedOnTop unit size50">
										<label for="p1" class="setMediumText strong">Program Applying For <span class="required">*</span></label>
										<select id="p1" name="ProgramInfo[FirstProgramID]" class="" onchange="filterOption(1); getProgramDuration(this)" <?php if($prog1==""):?> disabled <?php endif; ?> >
											<option value="">Choose Program...</option>
											<?php foreach($programs as $program): ?>
												<option value="<?=$program->getID()?>" <?php if($prog1==$program->getID()):?>selected<?php endif;?> ><?=$program->getName()?></option>
											<?php endforeach; ?>
										</select>
									</div>
									
								</li>
								<li class="line wrap paddedOnBottom">
									<?php $opt1 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionID() : ''); ?>
									
										<ul>
											<li>
												<div id="options_1" class="" style='display:none' >
													<label for="option1" class="setMediumText strong">Program Options<span class="required">*</span></label>
													<input type="hidden" id="option1_default" value="<?=$opt1?>">
													<select id="option1" name="ProgramInfo[FirstOptionID]" class="">
														<option value="<?=$opt1?>" <?php if($opt1!=''):?>selected<?php endif;?> ></option>
													</select>
												</div>
											</li>
											<li>
												<div class="unit size50" id="housingOptionDiv" style='display:none'>
													<?php $housingID = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getHousingOptionID() : ''); ?>
													<label for="option1" class="setMediumText strong">Housing Options<span class="required">*</span></label>
													<input type="hidden" id="housing_default" value="<?=$housingID?>">
													<select id="housingOption" name="ProgramInfo[HousingOptionID]" class="">
															<option value=""></option>
													</select>
												</div>
												<div class="lastUnit size50" id="durationOptionDiv" style='display:none'>
													<?php $durationID = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getDurationOptionID() : ''); ?>
													<label for="option1" class="setMediumText strong">Duration of Internship<span class="required">*</span></label>
													<input type="hidden" id="durationOption_default" value="<?=$durationID?>">
													<select id="durationOption" name="ProgramInfo[DurationOptionID]" class="">
															<option value=""></option>
													</select>
												</div>
												
											</li>
											<li>
												<div id="startDateDiv" style='display:none'>
													<div class="unit size50">
														<?php $startMonth = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartMonth() : ''); ?>
														<label for="option1" class="setMediumText strong">Month you would like to<br/> start your internship? <span class="required">*</span></label>
														<input type="hidden" id="startMonth_default" value="<?=$startMonth?>">
														<select id="startMonth" name="ProgramInfo[StartMonth]" class="" onchange="updateStartDay(this)">
															<option value="" ></option?>
															<?php foreach($startMonths as $month ):?>
																<option value="<?=$month?>" <?php if($startMonth == $month):?>selected<?php endif;?> ><?=$month?></option>
															<?php endforeach;?>
														</select>
													</div>
													<div class="lastUnit size50">
														<?php $startDay = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartDay() : ''); ?>
														<label for="option1" class="setMediumText strong">Day of month you would like to<br/> start your internship? <span class="required">*</span></label>
														<input type="hidden" id="startDay_default" value="<?=$startDay?>">
														<select id="startDay" name="ProgramInfo[StartDay]" class="">
															<option value="" ></option?>
															<?php for($day = 1; $day <= 31 ; $day++):?>
															<option value="<?php echo $day ?>" <?php if($startDay == $day):?>selected<?php endif;?> ><?php echo $day?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
											</li>
										</ul>
									
								</li>
							</ul>
							
					</fieldset>
					
					<fieldset class="contentBlock first">
						<legend class="display_none">Interested in</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">	
								<div class="unit size50">
									<?php $interest = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getInterest() : ''); ?>
									<label for="interested" class="setMediumText strong">Are you interested in studying abroad for a full academic year?</label>
									<input type="text" id="interested" class="text size50" size="23" name="ProgramInfo[Interest]" value="<?=$interest?>"/>
								</div>
								<!-- <div class="unit lastUnit formhelptext size40"></div> -->
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">	
								<div class="unit size50">
									<?php $learnFrom = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom() : '-1'); ?>
									<label for="interested" class="setMediumText strong">How did you learn about this CIS program? <span class="required">*</span></label>
									<select id="referer" name="AdditionalInfo[LearnProgramFrom]" class="size50" onchange="showSpecifyField()">
										<option value="" <?php if($learnFrom == '-1'):?>selected<?php endif;?> ></option>
										<?php foreach(CISOnlineAppConst::getAddInfoOptions() as $key => $option):?>
											<option value="<?=$key?>" <?php if((int)$learnFrom === $key):?>selected<?php endif;?> ><?=$option?></option>
										<?php endforeach;?>
									</select>
								</div>
								<!-- <div class="unit lastUnit formhelptext size40"></div> -->
								<div id="specifyField" class="unit lastUnit size40">
									<?php $others = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers() : ''); ?>
									<label for="website" class="setMediumText strong"<span class="fcGray">Please Specify: </span></label>
									<input type="text" id="website" class="text size95" size="23" name="AdditionalInfo[Others]" value="<?=$others?>"/>
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size50">
									<?php $financialAid = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFinancialAid() : 0); ?>
									<label for="interested" class="setMediumText strong">Are you planning on using financial aid to pay for this program? <span class="required">*</span></label>
									<input type="radio" value="1" name="AdditionalInfo[FinancialAid]" <?php if($financialAid == 1): ?>checked<?php endif;?> >Yes <input type="radio" value="0" name="AdditionalInfo[FinancialAid]" <?php if($financialAid == 0): ?>checked<?php endif;?>>No
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size50">
									<label for="interested" class="setMediumText strong">Enter Coupon code if you have any: <br/><span class="setSmallText required">Warning: Case sensitive!</span></label>
									<?php if($couponCode):?><p class="setSmallText required">For security reasons, you have to re-enter the code.</p><?php endif;?>
									<input type="text" id="coupon" class="text size50" size="23" name="CouponCode" value=""/>
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size90">
									<?php $campusRep = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getCampusRep() : ""); ?>
									<label for="interested" class="setMediumText strong">Were you referred by a CIS Campus Representative/Alumni? If yes, please name the CIS Campus Representative.</label>
									<input type="radio" value="1" id="CampusRepYes" name="CampusRep" <?php if($campusRep != ""): ?>checked<?php endif;?> onclick="showCampusRep()" >Yes <input type="radio" value="0" id="CampusRepNo" name="CampusRep" <?php if($campusRep == ""): ?>checked<?php endif;?> onclick="showCampusRep()" >No
									<div id="campus_rep_name">
										<input type="text" class="text size20" size="40" name="AdditionalInfo[CampusRep]" value="<?php echo $campusRep?>"/>
									</div>
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size90">
									<?php $friendGrant = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFriendGrant() : ""); ?>
									<label for="interested" class="setMediumText strong">Are you applying for a Bring a Friend Grant? If yes, please name your friend</label>
									<input type="radio" value="1" id="FriendGrantYes" name="FriendGrant" <?php if($friendGrant != ""): ?>checked<?php endif;?> onclick="showFriendGrant()" >Yes <input type="radio" value="0" id="FriendGrantNo" name="FriendGrant" <?php if($friendGrant == ""): ?>checked<?php endif;?> onclick="showFriendGrant()" >No
									<div id='friend_grant_name'>
										<input type="text" class="text size20" size="40" name="AdditionalInfo[FriendGrant]" value="<?php echo $friendGrant ?>"/>
									</div>
								</div>
							</li>
						</ul>
						
						<legend class="display_none">Comments</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<?php $com = ($comment == null) ? '' : $comment; ?>
								<label for="comments" class="setMediumText strong">Additional comments about your application:</label>
								<textarea rows="5" cols="53" name="txtAcomment"><?=$com?></textarea>
							</li>
						</ul>
					</fieldset>
					
					<div class="wrap paddedOnTop20 right">
					<? /*<a class="button_v3" href="OnlineApplication.php?step=1&action=view&pID=<?=$pID?>&backTrack"  > */ ?>
						<a class="button_v3" href="OnlineApplication.php?step=1&action=view&pID=<?=$pID?>&backTrack" onclick="return backTrackAction('programInformationForm')" >
							<strong>Back</strong>
						</a>
						
						<input type="submit" value="Next/Save" id="submit" name="<?php echo $x = (isset($buttonName) ? $buttonName : 'Submit');?>" class="submit button_v3 ga_interactive_form_field" onClick="return validateProgramInfoForm()"/>
						
						<a class="button_v3" href="javascript:void(0);" onclick="return saveAndExit('programInformationForm')">
							<strong>Save and Quit</strong>
						</a>											
					</div>
				</form>

			</div>

		</div>		

	</div>
	<div id="narrow_column">
			<?php echo $applicationProgressMenu?>
	</div>
</div>
