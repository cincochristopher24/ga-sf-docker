<?php
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');	
	Template::includeDependentCss('/min/f=css/jquery.autocomplete.css');	
	Template::includeDependentJs('/js/prototype.js');
	//Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator-1.1.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');

	Template::includeDependentJs('/js/jquery.autocomplete.js');

	ob_start();
	$remove_fields = true;
	
$incjs = <<<BOF
<script type="text/javascript">
	//<![CDATA[
	function showPAddressForm(){
		
		var divStyle = $('permanentAddress').style;
		
		if(divStyle.display == 'none' && $('n').checked == true){
			jQuery('#permanentAddress').slideDown("slow");
		
		}
		else if(divStyle.display == 'block' && $('y').checked == true) {
			var parent = $('permanentAddress');
			var inputChildren = parent.getElementsByTagName("input");
			var txtareaChild = parent.getElementsByTagName("textarea");
			var valIsEmpty = true;
			var a = 0;
			for(var i = 0; i < inputChildren.length; i++){
				if(inputChildren[i].getAttribute('value') != ''){
					a++;
					if(a==1){
						var ans = confirm('This will erase all your entries in the permanent address section. Click Ok to continue.');
						if(!ans){
							$('n').checked = true;
							valIsEmpty = false;
							break;
						}
					}
				
					inputChildren[i].setAttribute('value', '');
					txtareaChild[0].innerHTML = '';
				}
								
			}
		
		//	jQuery('#permanentAddress input').each(function(){
		//		var child = $(this);
		//		var val = $(this).val();
		//		alert(val);
		//	});
			if(valIsEmpty == true){
				jQuery('#permanentAddress').slideUp("slow");
			}
			
		}
		
	}
	
	function showSpecifyField(){
		var refererOpt = $('referer');
		var selectedRef = refererOpt.options[refererOpt.options.selectedIndex].value;
		if(selectedRef == '' || selectedRef == 0){
			jQuery('#specifyField').slideDown("slow");
		}
		else {
			jQuery('#specifyField').slideUp("slow");
		}
	}
	/***
	function activateOtherSchool(){
		var schoolOpt = $('school');
		var selectedSchoolVal = schoolOpt.options[schoolOpt.options.selectedIndex].value;
		if(selectedSchoolVal == '0'){
			$('otherSchools').style.display = '';
		}
		else {
			$('school1').value = '';
			$('otherSchools').style.display = 'none';
		}		
	}
	***/
	
	function disableHighSchoolCheckbox(){
		var chckHighschool = $('chckHighSchool');
		
		if(chckHighschool.checked == true){
			chckHighschool.checked = false;
		}
	}
	
	function enableOtherSchoolCheckbox(){
		
		var chckOtherSchool = $('chckOtherSchool');
		if(chckOtherSchool.checked == false){
			chckOtherSchool.checked = true;
		}
	}
	
	function disableOtherSchool(){
	
	
		var chckHighschool = $('chckHighSchool');
		var chckOtherSchool = $('chckOtherSchool');
		
		var txtOtherSchool = $('school1');
		//var optSchool = $('school');
		var otherSchools = $('otherSchools');
		
		var yearOpt = $('year');
		
		var defaulYearInSchool = parseInt(jQuery('#yearInSchool').val()) - 1;
		
		
		if(chckHighschool.checked == true || chckOtherSchool.checked == true){
			if(otherSchools.style.display == 'none'){
				jQuery('#otherSchools').slideDown("slow");
			}
			$('schoolX').disabled = true;
			
			yearOpt.options.length = 0;
			
			if(chckHighschool.checked == false){
				//alert('other school');
				jQuery('#majorAndMinor').slideDown('slow');

				yearOpt.options[yearOpt.options.length] = new Option('Freshman','1');
				yearOpt.options[yearOpt.options.length] = new Option('Sophomore','2');
				yearOpt.options[yearOpt.options.length] = new Option('Junior','3');
				yearOpt.options[yearOpt.options.length] = new Option('Senior','4');
				yearOpt.options[yearOpt.options.length] = new Option('College/University Graduate','5');
				yearOpt.options[yearOpt.options.length] = new Option('High School Graduate','6');

				yearOpt.options[defaulYearInSchool].selected = true;
			}
			else {
				
			//	chckOtherSchool.checked = true;
				
				
				
				jQuery('#majorAndMinor').slideUp('slow');
				yearOpt.options[yearOpt.options.length] = new Option('Freshman','1');
				yearOpt.options[yearOpt.options.length] = new Option('Sophomore','2');
				yearOpt.options[yearOpt.options.length] = new Option('Junior','3');
				yearOpt.options[yearOpt.options.length] = new Option('Senior','4');


				if(defaulYearInSchool < 5){
					yearOpt.options[defaulYearInSchool].selected = true;
				}
			}
			
		//	checkCheckedSchoolOption();
			
			
			txtOtherSchool.disabled = false;
		}
	/*	else if(chckOtherSchool.checked == true){
			if(otherSchools.style.display == 'none'){
				jQuery('#otherSchools').slideDown("slow");
			}
			$('schoolX').disabled = true;
			txtOtherSchool.disabled = false;
			yearOpt.options.length = 0;
			jQuery('#majorAndMinor').slideDown('slow');
			
			yearOpt.options[yearOpt.options.length] = new Option('Freshman','1');
			yearOpt.options[yearOpt.options.length] = new Option('Sophomore','2');
			yearOpt.options[yearOpt.options.length] = new Option('Junior','3');
			yearOpt.options[yearOpt.options.length] = new Option('Senior','4');
			yearOpt.options[yearOpt.options.length] = new Option('College/University Graduate','5');
			yearOpt.options[yearOpt.options.length] = new Option('High School Graduate','6');
			
			yearOpt.options[defaulYearInSchool].selected = true;
			
		}*/
		
		else {
			jQuery('#otherSchools').slideUp("slow");
			$('schoolX').disabled = false;
			jQuery('#majorAndMinor').slideDown('slow');
			
			yearOpt.options.length = 0;
			yearOpt.options[yearOpt.options.length] = new Option('Freshman','1');
			yearOpt.options[yearOpt.options.length] = new Option('Sophomore','2');
			yearOpt.options[yearOpt.options.length] = new Option('Junior','3');
			yearOpt.options[yearOpt.options.length] = new Option('Senior','4');
			yearOpt.options[yearOpt.options.length] = new Option('College/University Graduate','5');
			yearOpt.options[yearOpt.options.length] = new Option('High School Graduate','6');
			
			yearOpt.options[defaulYearInSchool].selected = true;
			//optSchool.disabled = false;
			txtOtherSchool.value = "";
		}
		
		
		
		
		
	}
	
	function showSpecialNeeds(){
		var spDiv = $('specialNeedsDiv');
		
		var y = $('spYes');
		var n = $('spNo');
		
		if(spDiv.style.display == 'none'){
			y.checked = true;
			n.checked = false;
			jQuery('#specialNeedsDiv').slideDown("slow");
		}
		else {
			y.checked = false;
			n.checked = true;
			jQuery('#specialNeedsDiv').slideUp("slow");
		}
	}

	Event.observe(window,'load',function(){
	//	alert('load');
		//activateOtherSchool();
		disableOtherSchool();
	//	showSpecifyField();
		(function($){
			var schools = {};
			$("#schoolX").autocomplete('/ajaxpages/searchCISUniversity.php');
			$('#schoolX').result(
				function(event, data, formatted) {
					$('#schoolXVal').val(data ? data[2]+'_'+data[1] : 0);
				}
			).blur(function(){
				$(this).search();
			});
		})(jQuery);
	});
	//]]>
</script>
BOF;
	Template::includeDependent($incjs);
?>			

<div id="content_wrapper" class="yui-skin-sam ltr noBottomPadding">
<div id="wide_column" class="layout2">
	<div class="section_detail">
		<div class="content">
				<h2>Personal Information</h2>
				<form method="post" action="OnlineApplication.php?step=1&action=save" id="formPersonalInfo" name="formPersonalInfo">
				
					<fieldset class="contentBlock first">
						<legend class="display_none">Profile Information</legend>
						<p class="title setSmallText dottedTopBorder">Fields with <span class="required">*</span> indicate required fields. </p>
						<ul class="form paddingof10 border_bottom_dotted">
								<li class="line paddedOnTop size50">
									<div class="line wrap">
										<label for="fn" class="setMediumText strong">Name <span class="required">*</span></label>
										<div class="unit size50">
											<?php $firstName = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName() : '');?>
											<input name="PersonalInfo[FirstName]" class="text size90" id="fn" type="text" value="<?=$firstName?>">
											<label class="fcGray text setSmallText" for="fn">First Name <span class="required">*</span></label>										
										</div>						
										<div class="unit size50 lastUnit">
											<?php $lastName = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName() : '');?>
											<input name="PersonalInfo[LastName]" class="text size90" id="ln" type="text" value="<?=$lastName?>">
											<label class="fcGray text setSmallText" for="ln">Last Name <span class="required">*</span></label>	
										</div>
									</div>
								</li>
								<?/*
								<li class="line wrap">
									<div class="unit size50">
										<?php $preferredName = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getPreferredName() : '');?>
										<input name="PersonalInfo[PreferredName]" class="text size95" id="pn" type="text" value="<?=$preferredName?>">
										<label class="fcGray text setSmallText" for="pn">Preferred Name </label>
									</div>
								</li>
								*/ ?>
								<li class="line wrap size50">	
									<div class="unit size50">
										<label for="PersonalInfo[Gender]" class="setMediumText strong">Gender <span class="required">*</span></label>
										<ul class="checkbox_radio_list">
											<?php $gender = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getGender() : 1);?>
											<li><input type="radio" class="checkbox_radio_list paddedSOnTop" id="m" value="1" name="PersonalInfo[Gender]" <?php if($gender==1): ?>checked<?endif;?>/> <label for="m" class="font-normal">Male</label></li>
											<li><input type="radio" class="checkbox_radio_list paddedSOnTop" id="f" value="2" name="PersonalInfo[Gender]" <?php if($gender==2): ?>checked<?endif;?>/> <label for="f" class="font-normal">Female</label></li>
										</ul>
									</div>
								</li>

								<li class="line size70">
									<div class="line size50 wrap">
										<label for="mm" class="setMediumText strong">Birthdate <span class="required">*</span></label>
										<?php
											$month = '';
											$day   = '';
											$year  = '';
											if(isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO])):
												$birthdate = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBirthday();
												$birthdate = strtotime($birthdate);
												$month = date('n',$birthdate);
												$day   = date('j',$birthdate);
												$year  = date('Y',$birthdate);
											endif;
											
										?>
										<div class="unit size50">
											<div class="unit size50">
												<select name="PersonalInfo[mm]" id="mm" onChange="return newDateLimiter(this, event);">
													<option value='' <?php if($month==''): ?>selected<?php endif; ?> >0</option>
													<? for($i = 1; $i <=12; $i++): ?>
														<option value='<?=$i?>' <?php if($month==$i): ?>selected<?php endif; ?> ><?=$i?></option>
													<? endfor; ?>
												</select>
											<?/*	<input name="PersonalInfo[mm]" id="mm" type="text" value="<?=$month?>" onKeypress="return numbersonly(this, event);" onChange="return dateLimiter(this, event);"> */ ?>
												<label class="fcGray text setSmallText" for="mm">MM</label>										
											</div>						
											<div class="unit size50 lastUnit">	
												<select name="PersonalInfo[dd]" class="" id="dd">
													<option value='' <?php if($day==''): ?>selected<?php endif; ?> >0</option>
													<? for($i = 1; $i <=31; $i++): ?>
														<option value='<?=$i?>' <?php if($day==$i): ?>selected<?php endif; ?> ><?=$i?></option>
													<? endfor; ?>
												</select>
											<?/*	<input name="PersonalInfo[dd]" class="text size70" id="dd" type="text" value="<?=$day?>" onKeypress="return numbersonly(this, event);" onChange="return dateLimiter(this, event);"> */ ?>
												<label class="fcGray text setSmallText" for="dd">DD</label>	
											</div>	
										</div>		
										<div class="unit lastUnit size50">					
											<div class="unit lastUnit">	
												<select name="PersonalInfo[yyyy]" class="" id="yyyy" >
													<? for($i = 1970; $i <=2000; $i++): ?>
														<option value='<?=$i?>' <?php if($year==$i): ?>selected<?php endif; ?> ><?=$i?></option>
													<? endfor; ?>
												</select>
											<?/*	<input name="PersonalInfo[yyyy]" class="text size70" id="yyyy" type="text" value="<?=$year?>" onKeypress="return numbersonly(this, event);" onChange="return dateLimiter(this, event);"> */?>
												<label class="fcGray text setSmallText" for="yyyy">YYYY</label>	
											</div>	
										</div>									
									</div>
								</li>
								
								<li class="line wrap paddedOnBottom">	
									<div class="unit size50">
										<?php $email = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail() : '');?>
										<label for="email" class="setMediumText strong">Email <span class="required">*</span></label>
										<input type="text" id="email" class="text size95" size="23" name="PersonalInfo[Email]" value="<?=$email?>"/>

									</div>
									<div class="unit lastUnit formhelptext size40">
										 Please check to make sure your email address is correct. We will send all relevant information regarding your application to this address.
									</div>
								</li>

								<li class="line wrap paddedOnBottom">	
									<div class="unit size50 paddedOnBottom">
										<?php $email2 = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getAltEmail() : '');?>
										<label for="email2" class="setMediumText strong">Alternate Email</label>
										<input type="text" id="email2" class="text size95" size="23" name="PersonalInfo[AltEmail]" value="<?=$email2?>"/>

									</div>
									<div class="unit lastUnit formhelptext size40">
										 In case your first email address is unavailable, we may be able to send you emails in this account.
									</div>
								</li>
								<li class="line wrap paddedOnBottom">	
									<div class="unit size50 paddedOnBottom">
										<?php $email3 = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBillingEmail() : '');?>
										<label for="email3" class="setMediumText strong">Billing Email</label>
										<input type="text" id="email3" class="text size95" size="23" name="PersonalInfo[BillingEmail]" value="<?=$email3?>"/>

									</div>
									<div class="unit lastUnit formhelptext size40">
										 Please provide the email address where invoices should be sent.
									</div>
								</li>										
						</ul>
					</fieldset>
					
					<fieldset class="contentBlock first">
						<legend class="paddedOnBottom20 setMediumText paddedOnTop20">Address</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line wrap paddedOnBottom">	
								<div class="unit size50">
									<?php $street1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1() : ''); ?>
									<label for="address" class="setMediumText strong">Street Address <span class="required">*</span></label>
									<input type="text" id="address" class="text size95" size="23" name="AddressInfo[Street1]" value="<?=$street1?>"/>
								<?/*	<textarea id="address" class="textarea size95" name="AddressInfo[Street1]" cols="30" rows="4"><?=$street1?></textarea> */ ?>
								</div>
							</li>
							<li class="line wrap paddedOnBottom">	
								<div class="unit size50">
									<?php $city1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1() : ''); ?>
									<label for="city" class="setMediumText strong">City <span class="required">*</span></label>
									<input type="text" id="city" class="text size95" size="23" name="AddressInfo[City1]" value="<?=$city1?>"/>
								</div>
							</li>
							
							<li class="line paddedOnTop size50 paddedOnBottom">
								<div class="line wrap">
									<div class="unit size50">
										<?php $state1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1() : ''); ?>
										<label for="state" class="setSMediumText strong">State </label>
										<select id="state" name="AddressInfo[State1]">
											<option value="">Choose State</option>
											<?php foreach($states as $state):?>
												<option value="<?=$state['abbr']?>" <?php if($state1==$state['abbr']):?>selected<?php endif; ?> ><?=$state['abbr']?></option>
											<?php endforeach; ?>
										</select>
									<?/*	<input type="text" id="state" class="text size90" size="25" name="AddressInfo[State1]" value="<?=$state1?>"/> */?>
									</div>						
									<div class="unit size50 lastUnit">
										<?php $zip1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1() : ''); ?>
										<label for="zip" class="setSMediumText strong">Zip <span class="required">*</span></label>
										<input type="text" id="zip" class="text size90" size="25" name="AddressInfo[Zip1]" value="<?=$zip1?>" onKeyPress="return numbersonly(this, event)"/>
									</div>												
								</div>
							</li>
							<li class="line wrap paddedOnBottom">	
								<div class="unit size50">
									<?php $phone1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1() : ''); ?>
									<label for="phone" class="setMediumText strong">Phone <span class="required">*</span></label>
									<input type="text" id="phone" class="text size95" size="23" name="AddressInfo[PhoneNumber1]" value="<?=$phone1?>" onKeyPress="return numbersonly(this, event)"/>
								</div>
							</li>
							<li class="line wrap paddedOnBottom">	
								<div class="unit size50">
									<?php $cellphone = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCellphone() : ''); ?>
									<label for="cphone" class="setMediumText strong">Cell Phone</label>
									<input type="text" id="cphone" class="text size95" size="23" name="AddressInfo[Cellphone]" value="<?=$cellphone?>" onKeyPress="return numbersonly(this, event)"/>
								</div>
							</li>
							
						</ul>
					</fieldset>		

					
					<fieldset class="contentBlock first">
						<legend class="paddedOnBottom20 setMediumText paddedOnTop20">School Information</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line wrap paddedOnBottom">	
								<div class="paddedOnBottom unit size50">
									<?php $schoolID = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolID() : '0'); ?>
									<?php $type = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getType() : ''); ?>
									<?php									
										if($type == 0){ $schoolID = 'usu_'.$schoolID; }
										else if($type == 1){ $schoolID = 'partner_'.$schoolID; }
										
									//	echo $type;
										
									?>
									<label for="schoolX" class="setMediumText strong">Name of School you currently attend <span class="required">*</span></label>
									<?php
										$schoolXname = (
											isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) && $type != 2
												? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName() 
												: ''
										);
									?>
									<input type="text" id="schoolX" name="SchoolInfo[OtherSchoolName1]" class="text size95" value="<?= $schoolXname ?>" />
									<input type="hidden" id="schoolXVal" name="SchoolInfo[SchoolID]" value="<?=$schoolID?>" />
									
									<div>
										<label style="position: relative; width: 40%; float: left;"><input type="checkbox" id="chckOtherSchool" name="SchoolInfo[Other]" onclick="disableHighSchoolCheckbox(); disableOtherSchool()" <?php if($type==3):?>checked<?php endif; ?> /> Other School</label>
										<label style="position: relative; width: 40%; float: left;"><input type="checkbox" id="chckHighSchool" name="SchoolInfo[Hs]" onclick="enableOtherSchoolCheckbox(); disableOtherSchool()" <?php if($type==2):?>checked<?php endif; ?> /> High School</label>										
									</div>
								</div>
							</li>
							<li class="line wrap paddedOnBottom">
								<div id='otherSchools' class="lastUnit size50" >
									<?php $sName = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getOtherSchoolName() : ''); ?>
									
									<label for="school1" class="setMediumText strong">Specify Name of School <span class="required">*</span></label>
									<input type="text" id="school1" class="text size95" size="23" name="SchoolInfo[OtherSchoolName]" value="<?=$sName?>" />
								</div>
							</li>
							<li class="line wrap paddedOnBottom">
								<div class="paddedOnBottom unit size50" id="majorAndMinor">
									<ul>
										<li class="line wrap paddedOnBottom">	
												<?php $major = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMajor() : ''); ?>
												<label for="major" class="setMediumText strong">Major <span class="required">*</span></label>
												<input type="text" id="major" class="text size95" size="23" name="SchoolInfo[Major]" value="<?=$major?>"/>
										</li>

									</ul>
								</div>
							</li>
							
							<li class="line wrap">	
								<div class="paddedOnBottom unit size50">
									<?php $yearInSchool = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getYearInSchool() : 1); ?>
									<input type="hidden" id="yearInSchool" value="<?php echo $yearInSchool ?>">
									<label for="year" class="setMediumText strong">Year in School <span class="required">*</span></label>
									<select id="year" name="SchoolInfo[YearInSchool]" class="size50">
										<option value="1" <?php if($yearInSchool==1):?>selected<?php endif; ?> >Freshman</option>
										<option value="2" <?php if($yearInSchool==2):?>selected<?php endif; ?> >Sophomore</option>
										<option value="3" <?php if($yearInSchool==3):?>selected<?php endif; ?> >Junior</option>
										<option value="4" <?php if($yearInSchool==4):?>selected<?php endif; ?> >Senior</option>
										<option value="5" <?php if($yearInSchool==5):?>selected<?php endif; ?> >College/University Graduate</option>
										<option value="6" <?php if($yearInSchool==6):?>selected<?php endif; ?> >High School Graduate</option>
									</select>

								</div>
							</li>
							
							<li class="line size50 paddedOnBottom">
								<div class="line wrap">
									<div class="unit">
										<?php $genAverage = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getGenAverage() : ''); ?>
										<label for="gpa" class="setSMediumText strong">Current cumulative GPA <span class="required">*</span></label>
										<input type="text" id="gpa" class="text size50" size="25" name="SchoolInfo[GenAverage]" value="<?=$genAverage?>" onKeyPress="return numbersonly(this, event)"/>														
									</div>																
								</div>
							</li>
							
						</ul>
					</fieldset>
					<?/*
					<fieldset class="contentBlock first">
						<legend class="paddedOnBottom20 setMediumText paddedOnTop20">Additional Information</legend>
						<ul class="form paddingof10">
								
							<li class="line wrap">	
								<?php $learnFrom = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom() : '-1'); ?>
								<div class="paddedOnBottom unit size50">
									<label for="referer" class="setMediumText strong">How did you learn about the CIS Program you are applying for? <span class="required">*</span> <span class="fcGray setSmallText">(choose one)</span></label>
									<select id="referer" name="AdditionalInfo[LearnProgramFrom]" class="size50" onchange="showSpecifyField()">
										<option value="" <?php if($learnFrom == '-1'):?>selected<?php endif;?> ></option>
										<?php foreach(CISOnlineAppConst::getAddInfoOptions() as $key => $option):?>
											<option value="<?=$key?>" <?php if((int)$learnFrom === $key):?>selected<?php endif;?> ><?=$option?></option>
										<?php endforeach;?>
									</select>																							

								</div>
							</li>
						</ul>
						<div id="specifyField">
						<ul class="form">
							<li class="line wrap paddedOnBottom">	
								<div class="paddedOnBottom unit size50">
									<?php $others = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers() : ''); ?>
									<label for="website" class="setMediumText strong"<span class="fcGray">Please Specify: </span></label>
									<input type="text" id="website" class="text size95" size="23" name="AdditionalInfo[Others]" value="<?=$others?>"/>																								
								</div>
							</li>
							</div>
						</ul>
					</fieldset>
					*/?>
					<input type='hidden' name='pID' value='<?php echo $pID = (isset($_GET['pID']) ? $_GET['pID'] : '')?>'>
					<div class="border_top_dotted wrap paddedOnTop20 right">
						<input type="submit" value="Next/Save" name="<?php echo $x = (isset($buttonName) ? $buttonName : 'Submit');?>" id='submit' class="submit button_v3 ga_interactive_form_field" onclick="return validatePersonalInfoForm()" />
					<!--	<input type="submit" value="Save and Quit" name="submit1" class="button_v3" /> -->
						<a class="button_v3" href="javascript:void(0);" onclick="return saveAndExit('formPersonalInfo')">
							<strong>Save and Quit</strong>
						</a>											
					</div>
				</form>

			</div>

	</div>		

	</div>
	<div id="narrow_column">
			<?php echo $applicationProgressMenu?>
	</div>
	</div>
