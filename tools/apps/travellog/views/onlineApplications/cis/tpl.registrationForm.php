<?php
Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
Template::includeDependentJs('/js/prototype.js');
//Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator-1.1.js');
//Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
ob_start();
$token = isset($_GET['token']) ? '&token='.urlencode($_GET['token']) : '';
?>
<div id="body">
	<div id="content_wrapper" class="yui-skin-sam ltr">
		<div id="wide_column" class="layout2">
			<div class="section_detail">
				<div class="content">
					To register, please fill out the following form:
					<form action="?action=processRegistration<?php echo $token?>" method="POST">
					<ul class="form paddingof10">
						<li class="line paddedOnTop size50">
							<label for="txtEmail" class="setMediumText strong">Email <span class="required">*</span><span id="errMsg1" class="required"><?php echo $errMsg;?></span></label>
							<input name="txtEmail" class="text size90" id="txtEmail1" type="text" value="<?php echo $txtEmail; ?>" >
							
						</li>
						<li class="line paddedOnTop size50">
							<label for="psword" class="setMediumText strong">Password <span class="required">*</span><span id="errMsg2" class="required"></span></label>
							<input name="psword" class="text size90" id="psword1" type="password" value="<?php echo $psword; ?>" >
						</li>
						<li class="line paddedOnTop size50">
							<label for="repsword" class="setMediumText strong">Re-type Password <span class="required">*</span></label>
							<input name="repsword" class="text size90" id="repsword" type="password" >
						</li>
					</ul>
					<div class="wrap paddedOnTop20">
						<input type="submit" value="Register" name="Submit" id='submit' class="submit button_v3 ga_interactive_form_field" onclick="return validateRegistration()" />
						
					</div>
					</form>
				</div>
			</div>
		</div>
		
	</div>
</div>

