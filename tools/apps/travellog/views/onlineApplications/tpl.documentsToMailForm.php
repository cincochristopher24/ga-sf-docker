<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if($groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
	//	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
	Template::includeDependentJs('/js/prototype.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
	
$incjs = <<< BOF
<script type="text/javascript">
//<![CDATA[

	function saveAndExit(frmId){
		var action = $(frmId).action;
		var actionArr = action.split('?');
		$(frmId).action = actionArr[0]+'?logout';
		
		
		$('submit').click();

		return false;
	}

//]]>
</script>
BOF;

	Template::includeDependent($incjs);
?>
<!-- displays the profile component: added by neri 11-04-08 -->
<? //$profile->render()?>
<!-- end -->
	
<?  
/*		
<script type="text/javascript">
	jQuery.noConflict();
</script>
*/
?>


<!-- Rotating Photo -->

<div id="content_wrapper" class="yui-skin-sam ltr noTopPadding">
<div id="wide_column" class="layout2">
	<div class="section_detail">
		<div class="content">
			<h2>Additional Documents to Download and Mail to CIS</h2>
			
			<p class="paddedOnBottom">
				Almost done! You will need to mail additional documents to CIS in order to complete the online application you have just submitted. Don’t worry if the documents are sent separately. We are set up to track them, and we’ll contact you as each application piece arrives.
			</p>
			
			<h3>We will have pdfs of the following documents here for download:</h3>
			
			<ul class="border_bottom_dotted">
				<li class="paddedOnTop line wrap">
					<div class="unit hugeNumber">1.</div>
					<div class="display_block lastUnit size90">
						<a href="documents/course_selection_worksheet.pdf" target="_blank" class="setLargeText strong display_block">Course selection worksheet</a>
						This tells us what courses you hope to take and how they will transfer back to your home institution. Work with your study abroad advisor and/or academic advisor to complete this worksheet. If you’re not sure you can submit this along with the rest of your application, just let us know. We can often process your application without it as long as we know it’s coming in later. This worksheet is not required for summer internship programs.
						<a href="documents/course_selection_worksheet.pdf" target="_blank" class="fcGray display_block strong setMediumText">
							<span class="pdf_icon"></span>
							Download Document
						</a>
					</div>
				</li>
				
				<li class="paddedOnTop line wrap">
					<div class="unit hugeNumber">2.</div>
					<div class="display_block lastUnit size90">
						<a href="documents/academic_reference_form.pdf" target="_blank" class="setLargeText strong display_block">Academic Reference Form</a>
						We require an academic reference as part of your application. A professor or advisor can fill this form out for you, or send us a letter of reference.
						<a href="documents/academic_reference_form.pdf" target="_blank" class="fcGray display_block strong setMediumText">
							<span class="pdf_icon"></span>
							Download Document
						</a>
					</div>
				</li>
				
				<li class="paddedOnTop line wrap">
					<div class="unit hugeNumber">3.</div>
					<div class="display_block lastUnit size90">
						<span class="setLargeText strong display_block fcFBlue">Transcript</span>
						A current official transcript from your home institution is required. If you attended one or more previous institutions and those grades don’t show on your current transcript, please also submit transcripts for the previous institutions.
					</div>
				</li>
				
				<li class="paddedOnTop line wrap paddedOnBottom">
					<div class="unit hugeNumber">4.</div>
					<div class="display_block lastUnit size90">
						<a href="documents/CIS_scholarship_application.pdf" target="_blank" class="setLargeText strong display_block">CIS Scholarship Information and Application <span class="fcGray">(optional)</span></a>
						This is not required, but we certainly encourage you to apply! Scholarships are for students applying semester/year programs only.
						<a href="documents/CIS_scholarship_application.pdf" target="_blank" class="fcGray display_block strong setMediumText">
							<span class="pdf_icon"></span>
							Download Document
						</a>
					</div>
				</li>
				
			</ul>

			<form action='OnlineApplication.php?step=5&action=view&pID=<?=$pID?>' method='post' id="documentsForm">
				<div class="wrap paddedOnTop20 right">
					<a class="button_v3" href="?step=3&action=view&pID=<?=$pID?>&backTrack">
						<strong>Back</strong>
					</a>
				
					<input type="submit" value="Next" id="submit" name="Submit" class="submit button_v3 ga_interactive_form_field"/>
				
					<a class="button_v3" href="javascript:void(0);" onclick="return saveAndExit('documentsForm')">
						<strong>Save and Quit</strong>

					</a>											
				</div>
			</form>

		</div>

</div>		

</div>
<div id="narrow_column">
		<?php echo $applicationProgressMenu?>
</div>
</div>
