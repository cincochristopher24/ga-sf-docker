<?php
	/*
	 * tpl.ViewListing.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
	
	require_once('travellog/helper/DisplayHelper.php');
	require_once('travellog/model/Class.GaStats.php');
	 
?>
<div class="listing_details">
<div class="xtrainfo">
	<? 
		//countries
		foreach( $contents['listing']->getCountry() as $Country){
			echo '<img height="12" width="24" src="http://images.goabroad.com/images/flags/flag'. $Country->getCountryID() . '.gif" /> ' . $Country->getName() . "<br />";
			if( count($Country->getMainRegions($contents['listing']->getListingID())) ){
				foreach( $Country->getMainRegions($contents['listing']->getListingID()) as $MainRegion ){
					echo $MainRegion->getName() . " : ";
					$countIdx = 1;
					foreach( $MainRegion->getCities($contents['listing']->getListingID()) as $City ){
						echo $City->getName();
						if( $countIdx != count($MainRegion->getCities()) )	
							echo ", ";
						$countIdx++;	
					}
					echo "<br />";
				}
			}
			
		}
		
		echo "<br />";
		
		// terms
		if( count($contents['listing']->getTerm()) ) {
			$countIdx = 0;
			echo "<strong>Term : </strong>";
			foreach( $contents['listing']->getTerm() as $Term ){
				echo $Term;
				if( $countIdx != count($contents['listing']->getTerm()) )	
					echo ", ";
				$countIdx++;	
			}
			echo "<br />";
		}
	   	
	   	// length
	   	if( strlen(trim($contents['listing']->getLength())) )	
	   		echo "<strong>Length of Position : </strong>" . trim($contents['listing']->getLength()) . "<br />";
	   	
	   	// duration
	   	if( count($contents['listing']->getDuration()) ) {
			$countIdx = 0;
			echo "<strong>Duration";
			if( count($contents['listing']->getDuration()) > 0 )
				echo "s of Program : </strong>";
			else
				echo " of Program : </strong>";
			foreach( $contents['listing']->getDuration() as $Duration ){
				echo $Duration;
				if( count($contents['listing']->getDuration()) > 1 ){
					$countIdx++;
					if( $countIdx == count($contents['listing']->getDuration()) - 1 )
						echo " and ";
					elseif( $countIdx != count($contents['listing']->getDuration()) )	
						echo ", ";
				}
			}
			echo "<br />";
		}	
		
		// typical duration 
		if( strlen(trim($contents['listing']->getTypicalDuration())) )
			echo "<strong>Typical duration of program : </strong>" . trim($contents['listing']->getTypicalDuration()) . "<br />"; 
		
		// dates
		if( strlen(trim($contents['listing']->getDates())) )
			echo "<strong>Dates : </strong>" . $contents['listing']->getDates();
	   			
	?>
</div>
<?
	// description
	if( strlen(trim($contents['listing']->getDescription())) )
		echo "<p><strong>Description : </strong> " . trim($contents['listing']->getDescription()) .  "</p>"; 
	
	// highlights
	if( strlen(trim($contents['listing']->getHighlights())) )
		echo "<p><strong>Highlights : </strong> " . trim($contents['listing']->getHighlights()) .  "</p>"; 
		
	// degrees available
	if( count($contents['listing']->getAcadDegree()) )	{
		echo "<p>";
		typeorganizer($contents['listing']->getAcadDegree(),'Degree','Available');
		echo "</p>";
	}
	
	// qualifications
	if( strlen(trim($contents['listing']->getQualSkills())) )
		echo "<p><strong>Qualifications : </strong> " . trim($contents['listing']->getQualSkills()) .  "</p>"; 
		
	// minimum educational attainment	
	if( strlen(trim($contents['listing']->getMinRequirement())) )	
		echo "<p><strong>Minimum Education : </strong> " . trim($contents['listing']->getMinRequirement()) .  "</p>"; 
		
	// jobs/intern type
	if( count($contents['listing']->getInternType()) )	{
		if( $contents['listing']->getIsJob() && $contents['listing']->getIsInternship() )
			$strType = "Job/Intern Type";
		elseif( $contents['listing']->getIsJob() )		
			$strType = "Job Type";
		else
			$strType = "Intern Type";
		echo "<p>";
		typeorganizer($contents['listing']->getInternType(),$strType);
		echo "</p>";
	}
	
	// languages
	if( count($contents['listing']->getLanguage()) ){
		echo "<p>";
		typeorganizer($contents['listing']->getLanguage(),'Language')	;
		echo "</p>";	
	}
	
	// languages used
	if( count($contents['listing']->getLanguageUsed()) ){
		echo "<p>";
		typeorganizer($contents['listing']->getLanguageUsed(),'Language','Used as a Medium of Teaching');
		echo "</p>";	
	}
	
	// subject areas
	if( count($contents['listing']->getSubjectArea()) ){
		echo "<p>";
		typeorganizer($contents['listing']->getSubjectArea(),'Subject Area');
		echo "</p>";
	}
	
	// cost
	if( strlen(trim($contents['listing']->getCost())) )
		echo "<p><strong>Cost in $US :</strong> " . trim($contents['listing']->getCost());
		
	// fee includes
	if( count($contents['listing']->getFeeIncludes()) ){
		echo "<p>";
		typeorganizer($contents['listing']->getFeeIncludes(),'Cost Includes','',1);
		if( strlen(trim($contents['listing']->getCostInclude())) )
			echo trim($contents['listing']->getCostInclude());
		echo "</p>";	
	}
	elseif( strlen(trim($contents['listing']->getCostInclude())) ){
		echo "<p><strong>Cost Includes :</strong> " . trim($contents['listing']->getCostInclude()) . "</p>";
	}
	
	// credit avail
	if( $contents['listing']->getCreditAvail() > 0)
		echo "<p><strong>Credit Available :</strong> yes </p>";
	
	// salary / stipend
	if( strlen(trim($contents['listing']->getStipend())) )
		echo "<p><strong>Salary / Pay : </strong>" . trim($contents['listing']->getStipend()) .  "</p>";
		
	// experience 
	echo "<p><strong>Experience Required : </strong>";
	if( $contents['listing']->getExperience() == 0 )		
		echo "no</p>";
	else
		echo "yes</p>";
	
	if( strlen(trim($contents['listing']->getExperienceDesc())) )
		echo "<p>" . trim($contents['listing']->getExperienceDesc()) . "</p>";	
	
	// typical volunteer
	if( strlen(trim($contents['listing']->getTypicalVolunteer())) )	
		echo "<p><strong>Typical Volunteer : </strong> " . $contents['listing']->getTypicalVolunteer() . " </p>";	
		
	// age range
	if( strlen(trim($contents['listing']->getAgeRange())) )	
		echo "<p><strong>Age Range : </strong> " . $contents['listing']->getAgeRange() .  "</p>";	
		
	// program open to	
	if( count($contents['listing']->getParticipant()) ){
		$countIdx = 0;
		echo "<p><strong>This Program is open to</strong> ";
		foreach( $contents['listing']->getParticipant() as $Participant ){
			echo $Participant;	
			if( count($contents['listing']->getParticipant()) ){
				$countIdx++;	
				if( $countIdx == count($contents['listing']->getParticipant()) - 1 )
					echo " and ";
				elseif( $countIdx != count($contents['listing']->getParticipant()) )	
					echo ", ";
			}	
		}
		echo " Participants. ";
		if( $contents['listing']->getFamilies() == 0 && $contents['listing']->getCouples() == 0 )
			echo "</p>";
	}
	
	// families / couples
	if( $contents['listing']->getFamilies() || $contents['listing']->getCouples() ){
		echo "This program is also open to ";
		if( $contents['listing']->getFamilies() )
			echo "Families";
		if( $contents['listing']->getFamilies() && $contents['listing']->getCouples() )
			echo ",&nbsp;";
		if( $contents['listing']->getCouples() )
			echo "Couples";
		echo " and Individuals.</p>";
	}
	
	// living arrangement
	if( count($contents['listing']->getLiving()) ){
		echo "<p>";
		typeorganizer($contents['listing']->getLiving(),'Typical Living Arrangement')	;
		echo "</p>";
	}
	
	// ind || group travel
	if( $contents['listing']->getIndTravel() || $contents['listing']->getGroupTravel() ){
		echo "<p><strong>Participants Travel ";
		echo "countries ngadi </strong>";
		if( $contents['listing']->getIndTravel() )
			echo "Independently";
		if( $contents['listing']->getIndTravel() && $contents['listing']->getGroupTravel() )	
			echo " Or ";
		if( $contents['listing']->getGroupTravel() )	
			echo "in Groups";
		echo ".</p>";
	}

	// ind/group work
	if( $contents['listing']->getIndependent() || strlen(trim($contents['listing']->getGroupWork())) ){
		echo "<p><strong>Typically Participants Work</strong> ";
		if( $contents['listing']->getIndependent() )
			echo "Independently";
		if( $contents['listing']->getIndependent() && strlen($contents['listing']->getGroupWork()) )	
			echo " Or ";
		if( strlen(trim($contents['listing']->getGroupWork())) )	
			echo "in Groups of " . trim($contents['listing']->getGroupWork());
		echo ".</p>";
	}
	
	// scholarship
	if( strlen(trim($contents['listing']->getScholarship())) ){
		echo "<p><strong>Scholarships Are Available.</strong> - " . trim($contents['listing']->getScholarship()) . "</p>";
	}
	
	// application
	if( count($contents['listing']->getApplication()) ){
		echo "<p>";
		typeorganizer($contents['listing']->getApplication(),'Application Process Involves','',1);	
		echo "</p>";
	}
	
	// application time
	if( strlen(trim($contents['listing']->getApplicationTime())) ){
		echo "<p><strong>Typically The Application Time is</strong> " . trim($contents['listing']->getApplicationTime()) . "</p>";
	}
	
	// post service include
	if( count($contents['listing']->getPostService()) ){
		echo "<p>";
		typeorganizer($contents['listing']->getPostService(),'Post Service Includes','',1)	;
		echo "</p>";
	}
	
	// mission
	if( strlen(trim($contents['listing']->getMission())) ){
		echo "<p><strong>Mission : </strong>" . trim($contents['listing']->getMission()) . "</p>";
	}
	
	// year began
	if( strlen(trim($contents['listing']->getYearBegan())) ){
		echo "<p><strong>Year Began : </strong>" . trim($contents['listing']->getYearBegan()) . "</p>";
	}
	
	// religious type
	if( strlen(trim($contents['listing']->getReligious())) ){
		echo "<p>" . trim($contents['listing']->getListingName()) . " is a " . trim($contents['listing']->getReligious()) . " based organization. We do ";
		if( $contents['listing']->getFaith() == 0)
			echo "not ";
		echo "require our participants to be of the same faith. Our volunteers will ";
		if( $contents['listing']->getProselytizing() )	
			echo "not ";
		echo "be involved in religious proselytizing.</p>";
	}
	
	
		
?>
</div>