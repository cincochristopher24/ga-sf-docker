<?php
	ob_start();
	
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'page_photos');

	
	
	Template::includeDependentJs("/js/prototype.js");
	Template::includeDependentJs("/js/scriptaculous/effects.js"); 	
	Template::includeDependentJs("/js/scriptaculous/dragdrop.js");
	//Template::includeDependentJs("/js/cpaint/lib/cpaint2.inc.js"); 
	Template::includeDependentJs("/js/photo.js"); 
	
	$code =<<<BOF
	<script type = "text/javascript">
	var photo = new Photo($genID,"$context",$loginID);
			
	</script>
BOF;

		Template::includeDependent($code);
	
	
	ob_end_clean();
	
	
	
	
	?>
	
	<? $subNavigation->show(); ?>
	
	<div id="content_wrapper" class="area_wrapper">				
		<div class="area section" id="top">
			<h2>
				<span>Rotate Photo</span>
				<div class="clear"/>
			</h2>
			
			<div class="content">
				<div class="box_cont2">		
				<div class="top_controls">	
					<? if($owner): ?>
						<p class="control_text">
							<a href="<?= $viewallphotolink ?>">View all Photo&nbsp;</a>
							<span class="top_text"></span>
						</p>
								
					<? endif; ?>
				</div>
				
				<div class="rotate_cont">
					<div id="piccontainer" class="rotate_box" style="margin: 10px 0 10px 0">								
						<img class="photo_bg" id="imgbox" src="<?= urldecode($photolink); ?>" />	
					</div>
					
					<div class="rotphot_cont" style="padding: 5px 0 0 200px; background-color:#353E43; border:1px solid #8A9CA8;">
						
						<span style="border:0px solid;height:33px;" id="loading"></span>
		  				<input class="rot_left_but" type="image" src="/images/rotleft_but.gif" name="cmdrotateleft" id="cmdrotateleft" value="Rotate Left" onclick="photo.rotatephoto(<?=$photoid?>,'left')">
		  				<input class="rot_right_but" type="image" src="/images/rotright_but.gif" name="cmdrotateright" id="cmdrotateright" value="Rotate Right" onclick="photo.rotatephoto(<?=$photoid?>,'right')">
		  				
					</div>
				</div>
				<div class="clear"></div>
				
			</div>	
			</div> 
			<div class="foot"/>
		</div>
		</div> 
	</div>
	
	
	