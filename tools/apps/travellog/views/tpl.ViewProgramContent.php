<?
require_once('Class.GaDateTime.php');
$d = new GaDateTime();
?>

<? if($showControls): ?>
	<ul class="actions">
		<li><a href="javascript:void(0)" onclick="manager.displayProgramForm('mode=add&amp;groupID=<?= $groupID ?>');" class="button">Create Itinerary</a></li>
	</ul>
<? endif; ?>
<? if(isset($obj_program_iterator)):
	$obj_program_iterator->rewind(); ?>
	<ul class="itineraries">
		<?	while( $obj_program_iterator->valid() ): 
		?>
			<li class="ptitle" id="ptitle<?=$obj_program_iterator->current()->getProgramID()?>" <?if ($obj_program_iterator->current()->getProgramID()==$programID) echo 'style="background:#E0EAF4"'?>>
				<a href="javascript:void(0)" onclick="manager.getDetail(<?=$obj_program_iterator->current()->getProgramID()?>,<?= $groupID ?>)" class="ititle"><?=$obj_program_iterator->current()->getTitle()?></a>
				<div class="idates"><a href="javascript:void(0)" onclick="manager.getDetail(<?=$obj_program_iterator->current()->getProgramID()?>,<?= $groupID ?>)"><?= $d->set($obj_program_iterator->current()->getTheDate())->htmlDateFormat();?> to <?= $d->set($obj_program_iterator->current()->getFinish())->htmlDateFormat();?></a></div>
				<? $obj_program_iterator->next(); ?>
			</li>
		<?	endwhile; ?>
	</ul>
<? endif; ?>
<?if(isset($obj_program_paging)):?>
	<br />
	<span class="previous" style="float:left;font-weight:bold"><?$obj_program_paging->getPrevious();?></span>
	<span class="next"  style="float:right;font-weight:bold"><?$obj_program_paging->getNext();?></span>
	<br />
<? endif; ?>

<span id="DUMMY-PROGRAMID" style="display:none"><?=$programID?></span>
<span id="DUMMY-GROUPID" style="display:none"><?=$groupID?></span>
