<?/*php
	$countLabel = 1 < $photoCount ? "$photoCount Photos" : "$photoCount Photo";
	$primary = $album->getPrimaryPhoto();
?>
<div class="list photo"> 
	<div>	
		<a onclick="collectionPopup.loadGallery('<?=$context?>',<?=$album->getPhotoAlbumID()?>,<?=$primary->getPhotoID()?>);" href="javascript:void(0)">
			<img src="<?=$primary->getPhotoLink('featured')?>" id="listen_img_album_<?=$album->getPhotoAlbumID()?>"/>
		</a>
		
			<p class="photoalbum">
				<a href="javascript:void(0)" onclick="collectionPopup.loadGallery('<?=$context?>',<?=$album->getPhotoAlbumID()?>,<?=$primary->getPhotoID()?>);">
					<strong><?=htmlspecialchars(stripslashes($album->getTitle()),ENT_QUOTES)?></strong>
				</a>
				<br/>
				<span>
					<?=$countLabel?>
				</span>
			</p>			
		
	</div> 
</div>
*/?>

<?php
	$contextObj = $photo->getContext();
	$photoLink = $photo->getPhotoLink("featured");
	$width = "";
	$height = "";
	$ownerID = 0;
	switch($photo->getPhototypeID()){
		case 1	:	$context = "profile"; 
					$ownerID = $contextObj->getTravelerID();
					$genID = $contextObj->getTravelerID();
					$title = "Profile Photos";
					$photoLink = $photo->getPhotoLink("fullsize");
					$link = "/collection.php?type=$type&amp;ID=$ID&amp;context=$context&amp;genID=$genID";
					$class = "profilealbum";
					$adjustedDimension = $photo->getAdjustedThumbnailDimension("fullsize",164);
					$width = "width=".$adjustedDimension["width"];
					$height = "height=".$adjustedDimension["height"];
					break;
		case 2	:	$context = "travellog"; 
					$ownerID = $contextObj->getTravelerID();
					$genID = $contextObj->getTravelLogID();
					$title = $contextObj->getTitle();
					$link = "/collection.php?type=$type&amp;ID=$ID&amp;context=$context&amp;genID=$genID";
					$class = "entry";
					break;
		case 7	:	$context = "traveleralbum";
					$genID = $contextObj->getPhotoAlbumID();
					$title = $contextObj->getTitle();
					$link = "/collection.php?type=$type&amp;ID=$ID&amp;context=$context&amp;genID=$genID";
					$class = "photoalbum";
					break;
		case 11	:	$context = "article";
					$genID = $contextObj->getArticleID();
					$title = $contextObj->getTitle();
					$link = "/collection.php?type=$type&amp;ID=$ID&amp;context=$context&amp;genID=$genID";
					$class = "entry";
					break;
		default :	$context = "profile";
					$ownerID = $contextObj->getTravelerID();
					$genID = $contextObj->getTravelerID();
					$title = "Profile Photos";
					$photoLink = $photo->getPhotoLink("fullsize");
					$link = "/collection.php?type=$type&amp;ID=$ID&amp;context=$context&amp;genID=$genID";
					$class = "profilealbum";
					$adjustedDimension = $photo->getAdjustedThumbnailDimension("fullsize",164);
					$width = "width=".$adjustedDimension["width"];
					$height = "height=".$adjustedDimension["height"];
					break;
	}
?>
<div class="list photo"> 
	<div>	
		<a onclick="collectionPopup.setParams('traveler','<?= $ownerID; ?>');collectionPopup.loadGallery('<?=$context?>',<?=$genID?>,<?=$photo->getPhotoID()?>);" href="javascript:void(0)">
			<img src="<?=$photoLink?>" <?=$width?> <?=$height?>/>
		</a>
		<p class="<?=$class?>">
				<a href="<?=$link?>">
					<strong><?=htmlspecialchars(stripslashes($title),ENT_QUOTES)?></strong>
				</a>
				<br/>
				<?php if(!empty($date)): ?>
					<span>
						<?=$date?> ago
					</span>
				<?php endif; ?>
		</p>			
	</div> 
</div>