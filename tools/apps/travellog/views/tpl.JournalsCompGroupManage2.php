<?php
    Template::includeDependentJs('/min/g=ManageJournalJs');
?>
<div id="journals_summary" class="section" >	
	<a name="anchor"></a>
	<h2><span><?php echo $compTitle; ?></span></h2>	
    <div class="content action_container">
        <?php if ($canAddJournal): ?>

			<!-- commented by Clyde -->
        	<!--<div class="header_actions">        	
        		<a href="<?=$addJournalBookLink?>">Add a Journal Book</a>        	
        	</div>-->

        <?php endif; ?>	
    </div>
	<div class="content action_container">	
		<div id="content">
			<?php include('tpl.JournalsCompGroupManageAjax2.php'); ?>		
			
			<?php if (count($journals) == 0) {
				 echo '<p class="help_text"><span>' . $noJournalMessage  . '</span></p>';						
			} ?>

		</div>
	</div>
</div>
<div id="dlg_container">
</div><!--
<script language='javascript' type='text/javascript' src='/min/g=ManageJournalJs'></script>
--><!--script src="/js/manageJournals.js"></script-->
<!--
	* ianne - 11/29/2008
	*	added custompopups confirmation and success messages to journal-related functionalities
	*	(publish/unpublish, delete, feature/unfeature, approve/unapprove, to unapproved)
	* neri - 01/20/2009
	* 	changed "Unfeature" text to "Remove from Featured List" in featureJournal()
	* ianne - 10/26/09
	*   moved js to manageJournals.js
-->
<!-- javascript code for publish/unpublish journal -->
<div id="dlg"></div>






