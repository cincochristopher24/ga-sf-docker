<?php
/**
 * <b>View event</b> contents template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */

Template::setMainTemplateVar('page_location', 'My Passport');

require_once('Class.HtmlHelpers.php');
require_once('Class.GaDateTime.php');
$d = new GaDateTime();
?>
<div class="area" id="area_left">
	<div class="section" id="quick_tasks">
		<h1><?= $event_type ?></h1>
		<div class="content">
			<ul class="actions">
				<? if($isOwner): ?>
					<li><a href="javascript:void(0)" onclick="<?= $action_new ?>" class="button">Create Event</a></li>
				<? endif; ?>
			</ul>
			<? if(isset($obj_paging)): ?>
				<? if ( $obj_paging->getTotalPages() > 1 ): ?>
					<div class="paging">
						<?$obj_paging->getPrevious()?>| <?$obj_paging->getNext()?>
					</div>
				<? endif; ?>
			<? endif; ?>
			<ul class="events" id="events_calendar">
				<?
					if(isset($obj_iterator)):
						$obj_iterator->rewind();
						while( $obj_iterator->valid() ):?>
							<li id="title<?=$obj_iterator->current()->getEventID() ?>" class="viewevents">
								<div class="meta date">
									<?= $d->set($obj_iterator->current()->getTheDate())->htmlDateFormat();?>
								</div>
								<h3><a href="javascript:void(0)" onclick="manager.toggleEvent(<?= $obj_iterator->current()->getEventID()?> ,<?= $obj_iterator->current()->getContext()?> );"><?= HtmlHelpers::WrapText($obj_iterator->current()->getTitle(), 16, "<br />");?></a></h3>
								<div class="clear"></div>
							</li>
				<? 			$obj_iterator->next();
						endwhile; 
					endif;
				?>
			</ul>
			<? if(isset($obj_paging)): ?>
				<? if ( $obj_paging->getTotalPages() > 1 ): ?>
					<div class="paging">
						<?$obj_paging->getPrevious()?>| <?$obj_paging->getNext()?>
					</div>
				<? endif; ?>
				<div id="eventID" style="display: none;"><? if(isset($primaryID)) echo $primaryID; ?></div>
				<div id="context" style="display: none;"><? if(isset($context)) echo $context; ?></div>
			<? endif; ?>
		</div>
	</div>	
</div>

<div class="area" id="area_right">
		<div class="section" id="event_details">
			<h2>Event Details</h2>
			<div class="content">
				<div id="view"><? if(isset($message)) echo HtmlHelpers::Textile($message); ?></div>
				<div class="clear"></div>
			</div>
		</div>
</div>

<div class="clear"></div>
