<?php
require_once('Class.HelperGlobal.php');
$today = getdate();
?>
<?php if(!isset($GLOBALS['CONFIG'])): ?>
<div id="footer_ad">
	<?php if(false): ?>
	<div id="content_wrapper">
		<script type="text/javascript"><!--
			google_ad_client = "ca-pub-0455507834956932";
			/* .NET Footer Ad */
			google_ad_slot = "6500363680";
			google_ad_width = 728;
			google_ad_height = 90;
			//-->
		</script>
		<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
	</div>
	<?php endif; ?>
</div>
<?php endif; ?>

<div id="footer_new">
	<div id="footer_innerwrap">
	
		<div id="topmost">
			<ul>
				<li class="first"><a href="/about-us.php">About Us</a></li>
				<li><a href="/feature.php">What's New</a></li>
				<?= NavigationHelper::displayGlobalNavigation() ?>
			</ul>
			
			<p id="slogan">
				Share your journeys<br/>
				with the world!
			</p>
	
			<ul>
			
				<li class="first"><a href="/passport.php">My Passport</a></li>
				<li><a href="/travelers.php">Travelers</a></li>
				<li><a href="/journal.php">Journals</a></li>
	
				<li><a href="/group.php">Groups</a></li>
				<li>
					<?= HelperGlobal::displayLoginLink(); ?>
				</li>
				<?= HelperGlobal::displayRegisterLink('<li class="last">', '</li>'); ?>
			</ul>
		</div>
		
		<div id="midmost">
			<ul class="footer_wide">
				<li><span>syncs with</span></li>
				<li><a href="http://www.goabroad.net/faq.php#facebook" class="facebookclass" id="facebook">facebook</a></li>
				<li><a href="http://www.youtube.com" target="_blank" class="youtubeclass" id="youtube">youtube</a></li>
				<li><a href="http://www.myspace.com" target="_blank" class="myspaceclass" id="myspace">myspace</a></li>
				<li><a href="http://www.hi5.com" target="_blank" class="hi5class" id="hi5">hi5</a></li>
				<li><a href="http://www.orkut.com" target="_blank" class="orkutclass" id="orkut">orkut</a></li>
			</ul>
			
			<ul class="footer_narrow">
				<li><span>affiliates</span></li>
				<li><a href="https://www.bbb.org/online/consumer/cks.aspx?id=1090108133417" target="_blank" id="bbb" title="Click to verify BBB accreditation and to see a BBB report." class="bbbclass">bbb</a></li>
				<li><a href="http://www.nafsa.org" target="_blank" id="nafsa" class="nafsaclass">nafsa</a></li>
				<li id="lastlogo"><a href="http://www.wysetc.org/" target="_blank" id="wystc" class="wystcclass">wystc</a></li>
			</ul>
			
			 <ul class="footer_wide footer_buttons">
				<li><span>follow us</span></li>
				<li><a href="http://facebook.com/GoAbroadCom" target="_blank" title="Follow us on Facebook" class="footer_facebook_button_class" id="footer_facebook_button">GoAbroad.net on Facebook</a></li>
				<li><a href="http://twitter.com/GoAbroad" target="_blank" title="Follow us on Twitter" class="footer_twitter_button_class" id="footer_twitter_button">Goabroad on Twitter</a></li>
				<li><a href="http://youtube.com/user/GoAbroadCom" target="_blank" title="Watch our videos on YouTube" class="footer_youtube_button_class" id="footer_youtube_button">GoAbroad.net on YouTube</a></li>
			</ul>
			
		</div>
		
		<div id="lowermost">
		
			<!--iframe src="http://www.facebook.com/plugins/likebox.php?id=192597394475&amp;height=64&amp;stream=false&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:292px; height:64px; float:right;" allowTransparency="true"></iframe-->
			<?php if(false): ?>
			<div style="float:right;">
				<fb:like-box profile_id="192597394475" width="292" connections="0" stream="false" header="false"></fb:like-box>
			</div>
			<?php endif; ?>
			
			<ul>
				<li><a href="http://www.goabroad.net"><img src="/images/footerdotnetlogo.gif" width="119" height="63" alt="GoAbroad.net"/></a></li>
				
				<li><strong>GoAbroad.net</strong> <span>|</span> Share your journeys with the world!</li>
				
				<li><p id="footlet">
		 			Copyright &copy; <?= $today['year'] ?> GoAbroad.net&reg;
				</p></li>
			</ul>
		
		</div>
		
	</div>
</div>