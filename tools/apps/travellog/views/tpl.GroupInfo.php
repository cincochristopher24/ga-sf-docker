<?php
/*
 * Created on Oct 20, 2006
 * Author: Czarisse Daphne P. Dolina
 * Tpl.IncGroupInfo.php - displays group info; group name, description, 
 * institution info if any, inquiries if any, links to edit profile and photos, 
 * link for traveler to join group.. 
 * 
 */

require_once('Class.HtmlHelpers.php');

$code =<<<BOF
	<script type = "text/javascript">
		
		function ecollapse(){ 
			$('institution_info').style.marginLeft = '0px';
						
			if($("view_link").style.display == 'block'){
				$('institution_info').style.height = 'auto';
				$('ellipsis').style.display = 'none';
				
				$('ex_p_link').style.backgroundPosition = 'right 3px ';
				$('view_link').style.display = 'none';
				$('hide_link').style.display = 'block';
				
			}
			else{
				$('institution_info').style.height = getAdjustedHeight()+'em';
				$('institution_info').style.overflow = 'hidden';
				$('ellipsis').style.display = 'inline';	
			
				$('ex_p_link').style.backgroundPosition = 'right -41px ';
				$('view_link').style.display = 'block';
				$('hide_link').style.display = 'none';							
			}						
		}
		
	</script>
	
BOF;

Template::includeDependent($code);
		
?>


<div class="section" id="group_intro">	
<div class="journal_header"><span>&nbsp;</span></div> 
<div class="content">
    <div class="photo_group">
		<div>	
			<!--<div class="journal_header3"><span>&nbsp;</span></div>-->
			<div class="admin_area" id="admin_area">
				<img id="grp_img" src="<?=$grpPhoto->getPhotoLink(); ?>" alt="Emblem" title="Emblem" />			
				
				 <ul class="actions">
					<? if (TRUE == $isAdminLogged) { ?>
						<li><a href="<?= htmlspecialchars($grpPhotoLink) ?>" class="thumb"><?= $grpPhotoLabel ?></a></li>	
						<? if ( isset($grpDelPhotoLabel) && isset($grpDelPhotoLink)	) { ?>
						<li><a href="<?= htmlspecialchars($grpDelPhotoLink) ?>" onclick="javascript:return confirm('Do you want to REMOVE this logo?');"><?= $grpDelPhotoLabel ?></a></li>	
						<? } ?>		
					<? } ?>
				
					<? if ( isset($grpViewPhotoLink) && isset($grpViewPhotoLabel)) { ?>
						<li><a href="<?= htmlspecialchars($grpViewPhotoLink) ?>" ><?= $grpViewPhotoLabel ?></a></li>
					<? } ?>
					
					<? if (TRUE == $isAdminLogged) { ?>
						<li><a href="<?= htmlspecialchars($grpAddPhotoLink) ?>" ><?= $grpAddPhotoLabel ?></a></li>
					<? } ?>
				</ul>
				<? if ($isLogged) : ?>
					<h2><?= $grpLabelName ?> Actions</h2>
				<? endif;?>
				<ul class="actions">			
					<? if (TRUE == $isAdminLogged) { ?>
	
					<li>
						<a href="group.php?gID=<?= $grpID ?>&amp;mode=edit" >Edit <?= $grpLabelName ?> Profile</a>
					</li>
					<li>
						<a href="group-privacy.php?groupID=<?=$grpID?>">Edit Privacy Preference</a>
					</li>
					<br />
					<li>
						<a href="javascript:void(0)" onclick="window.open('changepassword.php?action=entry','ChangePassword','width=500,height=355,navigation toolbar=disabled,left=200,top=200')" >Change Password</a>					
					</li>
					<? } ?>		
					<? if (NULL != $loggedUser && FALSE == $loggedUser->isAdvisor()) { 
						$validGrpAccess = array(GroupAccess::$OPEN, GroupAccess::$OPEN_APPROVAL);
						if ( $isLogged && !$isMemberLogged && !$isAdminLogged  && in_array($grpAccess, $validGrpAccess)) { ?>					
							<li><a href="join.php?gID=<?= $grpID ?>" >Join this <?= $grpLabelName ?></a></li>
						<? } elseif ( TRUE == $isMemberLogged && FALSE == $isAdminLogged ) { ?>
							<li><a href="confirm.php?gID=<?= $grpID ?>&amp;mode=leave" >Leave <?= $grpLabelName ?></a></li>
						<? } ?>
					<? } ?>	
					
					<? if (!$isAdminLogged && $isLogged && FALSE == $loggedUser->isAdvisor()) { ?>
						<li>
						 	<a href="messages.php?act=messageGroupStaff&gID==<?= $grpID ?>">Message <?= $grpLabelName ?> Staff</a>
						</li>
					<? } ?>
					<? if (isset($grpSurveyLink) && isset($grpSurveyLabel)) { ?>
						<li>
						 	<a href="<?=$grpSurveyLink?>"><?=$grpSurveyLabel?></a>
						</li>
					<? } ?>			
				</ul>
		
				
				<? if ($isAdminLogged && $loggedUser->isAdvisor()) { ?>
					<?$cntnewmsg = count($loggedUser->getInbox()->getNewMessages()) ?>
					<? if (0 < $cntnewmsg || isset($grpInquiryStr) ) : ?>
						<div class="newalertshead"></div>
							<ul class="newalerts">
								<? if (0 < $cntnewmsg) : ?>
									<li><a href="messages.php"><strong><?=$cntnewmsg?> new message<?= ($cntnewmsg > 1) ? 's' : '' ?></strong></a></li>
								<? endif; ?>	
								<? if (isset($grpInquiryStr)) : ?>
									<li><a href="student-communications.php"><b><?= $grpInquiryStr ?></b></a></li>
								<? endif; ?>						
							</ul>
					<? endif; ?>
					<div id="advisor_how_to">
						<p><a href="/downloads/AdvisorHowTo.pdf">GoAbroad Network Advisor Tutorial</a></p>
					</div>
				<? } ?>	
			</div>			
		</div>
		<div class="journal_header4"><span>&nbsp;</span></div> 	
				<? if (isset($grpAdmin)) { ?>
					<div id="group_administrator" class="users">
						<strong>Administrator:</strong><br />
						<? $adminprofile = $grpAdmin->getTravelerProfile()?>
						<a href="/<?=$adminprofile->getUserName() ?>" class="user_thumb" >
							<img src="<?=$adminprofile->getPrimaryPhoto()->getThumbnailPhotoLink() ?>" width="37" height="37" alt="User Profile Pic" title="View Profile" />
						</a>							 
						<a href="/<?=$adminprofile->getUserName() ?>" class="username">
							 <?=$adminprofile->getUserName() ?>
						</a>
						<div style="clear: left;"></div>
					</div>
				
				<? } ?>
			</div>	
		
			<div id="group_basics">
				<h1><?= $grpName ?></h1>
			
				<?	if (NULL != $grpCategory) {	?>												
					<p class="type_group">
						<? if ( Group::FUN == $grpDiscriminator || (Group::ADMIN == $grpDiscriminator && TRUE == $isParent) ) { ?>
								<?= $grpCategory->getName(); ?> 
						<? } ?>
					</p>
				<?	} ?>
			
				<p>
				<? if (isset($grpWebURL) && strlen($grpWebURL ) ) : ?>
						<a href="http://<?= $grpWebURL?>">
							<?= 'http://'.$grpWebURL ?>
						</a>
				<? endif; ?>		
				</p>	
			</div>
		
			<? if ($isAdminLogged || $isMemberLogged) : ?>
				<input type="hidden" id ="collapse" value="true">
			<? else : ?>
				<input type="hidden" id ="collapse" value="false">		
			<? endif ; ?>

				 
		<div id="group_details">
			
			<div id="institution_info"><?= nl2br($grpDesc) . $grpInstitutionInfo ?></div>
			<span id="ellipsis" style="display: none">...</span>			
			
			<div class="clear"></div>
		</div>
		
	</div>

	<div id="ex_col_link" style="display: none; ">
		<p id="ex_p_link"><a id='col_layer' href='javascript:void(0)' onclick="ecollapse()">
			<span id="view_link" style="display: none">View More</span>
			<span id="hide_link" style="display: none">Hide</span>
		</a></p>				
	</div>
	
	<script>
		
		if  (document.getElementById('institution_info').getHeight() > document.getElementById('admin_area').getHeight()){
			
			$('institution_info').style.display = 'block';
			$('ex_col_link').style.display = 'block';
			
			if (document.getElementById('collapse').value == 'true') {
			
				$('institution_info').style.height = getAdjustedHeight()+'em';
				$('institution_info').style.overflow = 'hidden';
				$('institution_info').style.marginLeft = '0px';
				
				$('ellipsis').style.display = 'inline';
				$('ex_p_link').style.backgroundPosition = 'right -41px ';
				$('view_link').style.display = 'block';
			}
			else { 
				$('ex_p_link').style.backgroundPosition = 'right 3px ';
				$('hide_link').style.display = 'block';				
			}																		
		}	
		
		function getAdjustedHeight() {
	
			var multiple = 1.6;		// one line is 1.6em
			var grp_img_height = document.getElementById('grp_img').getHeight();
			
			var admin_height = document.getElementById('admin_area').getHeight();
			
			if (10 > document.getElementById('grp_img').getHeight() )	// adjust img height if img hasnt loaded yet
				admin_height = admin_height +  110;
			
			var newhght = admin_height - document.getElementById('group_basics').getHeight(); 
			
			var emval = newhght / 11 ;	// get the em value given the px height
			var newhght = (Math.floor(emval/multiple) * multiple) ;			// get the em value in given multiple range plus adjustment
			
			return newhght;		
		}

	</script>
	
</div>