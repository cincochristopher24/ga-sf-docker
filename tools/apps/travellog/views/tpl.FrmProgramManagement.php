<?php
/**
 * <b>Program</b> management form template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
?>

<div class="section">
<div class="content">
<?if( count($props['errors']) ):?>
	<div class="errors">
		<ul>
		<?foreach( $props['errors'] as $error ) : ?>
			<li><?=$error?></li>
		<?endforeach;?>
		</ul>
	</div>
<?endif;?> 
<form name="sform" onSubmit="return false;">
		<ul class="form">
			<li><label for="title">Title</label>
				<div id="errTitle" class="errors"></div>
				<input type="text" name="title"  size="40" id="title"  class="text big" value="<?=$props['title']?>" />
			</li>
			<?if( count($props['subgroups_lists']) ):?>
				<li>
					<fieldset class="choices">
						<legend><span>SubGroup<? if( count($props['subgroups_lists']) > 1 ) echo 's'; ?></span></legend>
						<div id="errSubGroup" class="errors"></div>
						<ul <? if( count($props['subgroups_lists']) > 1 ) echo 'class="many"' ?>>
						<?
						  $ctr = 1;	
						  foreach($props['subgroups_lists'] as $obj_subgroup):
								$checked = '';
								if( count( $props['subgroups'] ) ){
									if( in_array($obj_subgroup->getGroupID(), $props['subgroups'] ) ){
										$checked = 'checked=true';
									}
								}
						?> 
							<li>
								<input type="checkbox" name="subgroups[]" id="subgroups<?=$ctr?>" value="<?=$obj_subgroup->getGroupID()?>" <?=$checked?> /><label for="subgroups<?=$ctr?>"><?=$obj_subgroup->getName()?></label>
							</li>
						<?
						  $ctr++;	
						  endforeach;?>
						</ul>
					</fieldset>
				</li>
			<?endif;?>
			<li>
				<fieldset>
					<legend><label for="txtStartDate">Start</label></legend>
					<div id="errStartDate" class="errors"></div>
					<input style="width:109px;" type="text" readonly="true" class="date-pick text" name="start_date" id="start_date" value="<?=$props['start_date']?>"/>
					<div class="clear"></div>
				</fieldset>
			</li>
			<li>
				<fieldset>
					<legend><label for="txtFinishDate">End</label></legend>
					<div id="errFinishDate" class="errors"></div>
					<input style="width:109px;" type="text" readonly="true" class="date-pick text" name="finish_date" id="finish_date" value="<?=$props['finish_date']?>"/>
					<div class="clear"></div>
				</fieldset>
			</li>
			<li>
				<label for="description">Description</label>
				<div id="errDescription" class="errors"></div>
				<textarea name="description" id="description" rows="10" cols="80"><?=$props['description']?></textarea>
			</li> 
			<li>
				<fieldset class="choices">
					<legend><span>Show this Itinerary to</span></legend>
					<ul>
						<li><input type="radio" name="display" id="display1" value="1" <?if($props['display'] == 1 ) echo 'checked=true'; ?> /><label for="radPublic1">Public</label></li>
						<li><input type="radio" name="display" id="display2" value="2" <?if($props['display'] == 2 ) echo 'checked=true'; ?> /><label for="radPublic2">Groups</label></li>
					</ul>  
				</fieldset>
			</li>
			<li class="actions">  
				<input type="button" class="submit" name="action" id="action" value="Save" onclick="jQuery('input').saveProgram()" />&nbsp;&nbsp;<input type="button" class="submit" name="btnCancel" id="btnCancel" value="Cancel" onclick="jQuery().cancelProgram()" />   
				<input type="hidden" name="mode" id="mode" value="<?=$props['mode']?>" /> 
				<input type="hidden" name="gID"  id="gID"  value="<?=$props['gID'] ?>" />
				<input type="hidden" name="pID"  id="pID"  value="<?=$props['pID'] ?>" /> 
			</li> 
	</ul>
	<div class="clear"></div> 
</form>
</div>
</div>