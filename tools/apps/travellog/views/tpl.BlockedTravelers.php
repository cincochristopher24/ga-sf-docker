<? if( isset($blocked_users) && count($blocked_users) ): ?>
	<div class="section">
		<h2>
			<span>Blocked Users</span>
			<span id="bu_loader" style="display:none;">
				<img alt="Loading" src="/images/loading_small.gif" width="15" height="15" />
			</span>
		</h2>
		<div class="content">
			<ul class="users">
				<? $i = 0;
				   foreach ( $blocked_users as $blocked_user ): ?>
					<?php if(!$showAll && ($i == $showN)): ?>
						</ul><ul class="users blockedUsers" style="display:none;">
					<?php elseif($showAll && ($i == $showN)): ?>		
						</ul><ul class="users blockedUsers">
					<?php endif; ?>		
				
					<li id="bu_<?=$blocked_user->getTravelerID()?>" <? if($i === 0):?>class="first"<?endif;?>>																
						<a title="View Profile" class="thumb" href="/<?php echo $blocked_user->getUserName()?>">
							<img alt="User Profile Photo" src="<?=$blocked_user->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" width="37" height="37" />
						</a>
						<div class="details">								
						<strong><a title="View Profile" class="username"  href="/<?php echo $blocked_user->getUserName()?>">
							<?=$blocked_user->getUserName()?>
						</a></strong>
						<p>
							<a href = "javascript:void(0)" onclick="unblockFriend({username:'<?=$blocked_user->getUserName()?>',friendID:'<?=$blocked_user->getTravelerID()?>', n:'<?=$showN?>'});"><span>Unblock</span></a>
						</p>
						</div>	
					</li>
				<? $i++;
				   endforeach; ?>
			</ul>
			<div style="margin-top:2px;font-size:10px;padding-bottom:5px;overflow:auto;width:100%;">	
				<?if(!$showAll && count($blocked_users) > $showN):?>
					<a href="javascript:void(0)" class="blockedUsers more" style="float:right;text-decoration:none;">+ view more</a>
				<?elseif($showAll && count($blocked_users) > $showN):?>	
					<a href="javascript:void(0)" class="blockedUsers" style="float:right;text-decoration:none;">- view less</a>
				<?endif;?>
			</div>
		</div>
	
		<div class="foot"></div>
	
	</div>
<? endif; ?>
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$('a.blockedUsers').click(function(){
				if($(this).is('.more')){
					$('ul.blockedUsers').slideDown('slow');
					$(this).html('- view less');
				}else{
					$('ul.blockedUsers').slideUp('slow');
					$(this).html('+ view more');
				}
				$(this).toggleClass('more');
				$('#blocked_travelers').myScroll();
			});
		});
	})(jQuery);
</script>