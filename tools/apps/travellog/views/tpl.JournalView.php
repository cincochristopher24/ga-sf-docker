<div class="journal_title">
	<h1 id="JOURNALTITLE" class="clear">
		<?=$contents['obj_journal']->getTitle()?>
	</h1>
		<?if( $contents['show_admin_controls'] ):?>
			<a href="javascript:void(0)" id="JournalTitleLink" class="entry_links bottom_shift" rel="<?=$contents['obj_journal']->getTravelID()?>">
				<span>Edit Journal Title</span>
			</a>
		<?endif;?>

	  	<div id="JOURNALDESCRIPTION" class="journaldescription"><?=nl2br($contents['obj_journal']->getDescription())?></div>
		<?if( $contents['show_admin_controls'] ):?>
			<a href="javascript:void(0)" id="JournalDescriptionLink" rel="<?=$contents['obj_journal']->getTravelID()?>" class="entry_links bottom_shift">
				<span>Edit Journal Description</span>
			</a>
		<?endif;?>
	<div style="clear:both;"></div>
</div>