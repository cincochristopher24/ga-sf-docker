<div class="noticeBox">
	<div id="noticeHeader">
		<?php if( isset($vars["siteContext"]) ): ?>
			<span id="noticeHeaderTitle" class="jLeft">New Features Launched on <?=$vars["siteContext"]->getSiteName()?> Network!</span>
		<?php else: ?>
			<span id="noticeHeaderTitle" class="jLeft">New Features Launched on GoAbroad Network!</span>
		<?php endif; ?>
		<a href="javascript: document.getElementById('frmNotice').submit()" class="jRight link">Proceed to your account &#187;</a>
	</div>
	
<h2></h2>

<ul>
	<li><img src="/images/traveler_notice.jpg" border="0" align="middle"></li>


	<li><span class="noticeHeadText">Get all your updates when you log in!</span></li>
	
	<?php if( isset($vars["siteContext"]) ): ?>
		<li>You will see an all-new Passport page when you log in to <?=$vars["siteContext"]->getSiteName()?> Network. Now, it's easier to see what's new with your friends and groups!</li>
	<?php else: ?>
		<li>You will see an all-new Passport page when you log in to GoAbroad Network. Now, it's easier to see what's new with your friends and groups!</li>		
	<?php endif; ?>	
	
	<li>
		<span class="noticeHeadText2">Feeds</span>
		Check out new journals and photos from your friends and groups. Leave them greetings and comments to let them know you dropped by!
	</li>
	
	<li>
		<span class="noticeHeadText2">Notifications</span>
		These are quick notes to let you know if you have new messages, greetings, comments, invitations and friend requests.
	</li>
	
	<li>
		<span class="noticeHeadText2">Groups</span>
		Here's quick access to your groups.
	</li>
	
	<li>
		<span class="noticeHeadText2">Messages</span>
		Your messages at a glance.
	</li>
	
	<li>To know more about the change, <a href="/faq.php">please visit our illustrated guide &#187;</a></li>
	
	<li>
		<form id="frmNotice" name="frmNotice" method="post" action="/travelerdashboard.php">
			<fieldset>		
				<input type="checkbox" id="dontShow" name="DASHBOARD_FEATURE_NOTICE" class="ga_interactive_form_field"/>
				<label class="subscribes inline" for="dontShow">	
					Don&rsquo;t show this notice again in the future
				</label>
			</fieldset>
		</form>
	</li>
</ul>

<a href="javascript: document.getElementById('frmNotice').submit()" class="jRight button">Proceed to your account &#187;</a>

</div>