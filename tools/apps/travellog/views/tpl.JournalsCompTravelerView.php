<script type="text/javascript" src="/js/journals-comp.js"></script>

<? function displayJournalEntryTitle($jeLine){
		$jePhotoCount = $jeLine->countPhotos();				        			 			        			 
		 $photoStr = '';
		 if ($jePhotoCount > 0)
		 	$photoStr .= ' | ' . $jePhotoCount . ' Photo';
		 if ($jePhotoCount > 1)
		 	$photoStr .=  's'; ?>		
		    	
		<li  > 
			<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jeLine->getTravelLogID()?>"> 
				<strong>
					 <?=$jeLine->getTitle()?>
				</strong>
			</a><br />
			<span class="meta">
				<?=date('d F',strtotime($jeLine->getTrip()->getArrival()))  . $photoStr ?>
			</span>
		</li>
<?	} ?>
  
<div id="journals_summary" class="section traveler_context" >	
	<a name="anchor"></a>
	<?//php var_dump($isAlreadySync)?>
	<h2><span><?=$compTitle?></span>	    
		<span id="linkicon">
			<a href="http://apps.facebook.com/mytraveljournals"><span>Facebook</span></a>
		</span>
	</h2>
	<div class="content" >
		<?
			$country = '';
			$city = '';
			$jeCtr = 1;					// journal entry counter used for identifying yahoo carousel object
			$carJsCode = '';			// yahoo carousel code
		?>
			
		<ul class="journals">
			<?foreach($journals as $journal):
				$jID = $journal->getTravelID();
				$jeCount = count($journalsAndEntries[$jID]);
				$jEntriesAr = $journalsAndEntries[$jID];
				
				if ($journal->getOwner() == null){
					echo 'travelid:' . $journal->getTravelID();
					continue;
				}
								
				$jah = new JournalAuthorUIHelper($journal->getOwner());
				
				if (isset($jEntriesAr[$jeCount-1])){
					$trip = $jEntriesAr[$jeCount-1]->getTrip();
					$country = $trip->getLocation()->getCountry()->getName();
					$city = $trip->getLocation()->getName();
				} 
				?>
				 <li id="container<?=$jID?>">     
						<h3>
												
							<?if (isset($jEntriesAr[$jeCount-1])): ?>									
								<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" alt="<?=$journal->getTitle()?>"><?=$journal->getTitle()?></a>
							<?else:?>
								<?=$journal->getTitle()?>
							<?endif;?>	
														
							<span  class="action_links" id="controls<?=$journal->getTravelID()?>">
								<a class="button" href="/journal-entry.php?action=add&amp;travelID=<?=$journal->getTravelID()?>">Add an Entry</a> <a class="button" href="/journal.php?action=edit&amp;travelID=<?=$journal->getTravelID()?>">Edit Journal</a>
							</span>
							
						</h3>				
						<div class="content" id="content<?=$journal->getTravelID()?>">
							<div class="preview">
								<?if (isset($journalPhotos[$jID])):?>
									 <div class="photo">
										<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>"> 
											<img src="<?=$journalPhotos[$jID]->getPhotoLink('featured')?>" alt="journal photo" />
										</a>							
									 </div>	
								   <?endif;?>
								<?if (isset($jEntriesAr[$jeCount-1])):?>								
									<h4><a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>">
									<?=$jEntriesAr[$jeCount-1]->getTitle()?>
									</a></h4>	
										<p class="meta">						
										<?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?> | <?=date('D M d, Y',strtotime($trip->getArrival()))?> | Views: <?=$journal->getViews()?>
										</p>
										<?if ($jeCount):?>							
											<?=(strlen(trim($jEntriesAr[$jeCount-1]->getDescription())) > 250 )? strip_tags(nl2br(substr($jEntriesAr[$jeCount-1]->getDescription(),0,250))) . '...' : strip_tags(nl2br($jEntriesAr[$jeCount-1]->getDescription()))?>
										<?endif;?>	
								<?else:?>
									<p class="help_text"> 
										<span>There are no entries for this journal</span>.
										<a href="/journal-entry.php?action=add&amp;travelID=<?=$jID?>">Add an Entry</a> | 
										<a href="javascript:void(0)" onclick="CustomPopup.initialize('Delete Journal ?','Are you sure you want to delete this journal?','/travel.php?action=delete&amp;travelID=<?=$jID ?>','Delete','1','All entries, photos and other traveler comments linked to this journal will also be deleted. This action cannot be undone.');CustomPopup.createPopup();" >Delete</a>
									</p>
								<?endif;?>
							
							
								<?if (isset($relatedGroupsAr[$jID])):
									if (count($relatedGroupsAr[$jID]) == 1): 
										 $group = array_pop($relatedGroupsAr[$jID]);
										 //$group = array_pop($group);
										 $site = $group->getServerName(); 	
									?>
										<p class="entry_group_tag">
											<?if($site != false):?>						
												<span>
													<a href="http://<?=$group->getServerName()?>" title="<?= $group->getName()?>">	
														<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
													</a>
												</span>	
												<a href="http://<?=$group->getServerName()?>" class="author_username">
													<strong><?=$group->getName()?></strong>
												</a>
												<span class="meta">Advisor Group Journal</span>			
		
											<?else:?>
												<span>
													<a href="/group.php?gID=<?=$group->getGroupID()?>" class="<?= $group->getName()?>">
														<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
													</a>
												</span>					
												<a href="/group.php?gID=<?=$group->getGroupID()?>" class="author_username">
													<strong><?=$group->getName()?></strong>
												</a>
												<span class="meta">Advisor Group Journal</span>	
		
											<?endif;?>	
										</p>														
									<?endif;?>
								<?endif;?>	
								</div>
								<div class="entries">
									<?
										$arlen = count($jEntriesAr);
										$jEntriesAr_up_to_5 = array_slice($jEntriesAr, -5);
										$show_all = $arlen > 5; // Show complete array list when there are more than 5 entries
									?>
									<h4>
										<span><?=$arlen?> <?if ($arlen > 1):?>Entries<?else:?>Entry<?endif;?></span>
										<?if ($show_all):?>
											<a href="javascript:void(0)" onclick="document.getElementById('content<?=$journal->getTravelID()?>').style.display = 'none'; document.getElementById('entrylist<?=$journal->getTravelID()?>').style.display = 'block';" >Show All &raquo;</a>
										<?endif;?>
									</h4>
									<ul>
								        	<?foreach($jEntriesAr_up_to_5 as $jeLine):
								        			displayJournalEntryTitle($jeLine) ?>
											<?endforeach;?>
									</ul>
								</div>
								
						</div>
					<?if ($show_all):?>
						<div class="entries" id="entrylist<?=$journal->getTravelID()?>">					
							<?
								// The following code splits the entry list into 3 columns of approximate equal lengths
								$entries_per_column = ceil($arlen/3);
								$columns = array_chunk($jEntriesAr,$entries_per_column);
								$i = 1;
							?>
							<h4>
								<span><?=$arlen?> Entries</span>
								<a href="javascript:void(0)" onclick="document.getElementById('entrylist<?=$journal->getTravelID()?>').style.display = 'none'; document.getElementById('content<?=$journal->getTravelID()?>').style.display = 'block';">Return to Preview &raquo;</a>
							</h4>
						    <?foreach ($columns as $entries): ?>
								<ul <?if (3 == $i):?>class="last"<?endif;?>>
									<?foreach($entries as $jeLine):
						        			displayJournalEntryTitle($jeLine)?>
									<?endforeach;?>
								</ul>
								<? $i++ ?>
							<?endforeach;?>
						</div>
					<?endif;?>
				</li>
						
			<?endforeach;?>
		</ul>
			<!-- echo javascript code for journal controls -->
			<?foreach($journalControlsJsCode as $travelID => $jsCode){
				echo $jsCode; 
			  }?>
			
			
			<? if (isset($pagingComp)):?>	
			    	<? $pagingComp->showPagination() ?>
			<? endif; ?>
			
						
			<?if (count($journals) == 0){
				 echo '<p class="help_text">' . $noJournalMessage  . '</p>';						
			}?>

		</div>

	  <?if ($canAddJournal):?>
			<ul class="actions">        	
				<li><a class="button" href="<?=$addJournalBookLink?>">+ Add a new Journal</a></li>
				<li><a class="button" href="/journal_draft/view_saved_drafts">View All Drafts</a></li>
			</ul>
    <?endif;?>

</div>

<script type="text/javascript">
	function confirmDelete(url,title) {
  						if (confirm('Are you sure you want to delete the journal, ' + title + '? All of your entries and photos linked to this journal and comments from other travelers will also be deleted. This cannot be undone. ')){	
  										document.location = url;
  						}
    }		
</script>	
