<?php
  Template::includeDependentJs('/js/jquery-1.1.4.pack.js');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Groups / Clubs');

	if ('arrange' == $mode && $isAdminLogged ) {
		$style = "
		  <style>									
			  ul.boxy {
			    display:block;
			  }
			  ul.boxy li {
			    cursor:move;
			    position: relative;		
			  }
		  </style>";

		Template::includeDependent($style);
		
	  	Template::includeDependentJs('/min/f=js/prototype.js');
		Template::includeDependentJs('/js/scriptaculous/effects.js');
		Template::includeDependentJs('/js/scriptaculous/dragdrop.js');
	}
	
	Template::includeDependentJs('/js/groups_util.js');
	Template::includeDependentJs('/js/customGroupsManager.js');
?>

<?= $profile->render()?>
   
<?=$subNavigation->show();?>

<div id="statusPanel" class="widePanel content" style="display: none;">
	<p class="loading_message">
		<span id="imgLoading">
			<img alt="Loading" src="/images/loading.gif"/>
		</span>
		<em id="statusCaption">Loading data please wait...</em>
	</p>
</div>

<div id="customGroupSection_content">
  <?=$customGroupSectionPanel->render()?>
</div>
<!--added by ianne - 11/20/2008 success message after confirm-->
<?if(isset($success)):?>
	<script type="text/javascript">
		CustomPopup.initPrompt("<?=$success?>");
		window.onload = function(){
			CustomPopup.createPopup();
		}
	</script>
<?endif;?>