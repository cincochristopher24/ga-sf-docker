<?php
/*
 * Created on 08 30, 07
 *
 * Template to view the steps to do for those who are new to Goabroad.net
 */
 
 Template::includeDependentJs("/js/jquery-1.1.3.1.pack.js");
 Template::includeDependentJs("/js/jquery.jcarousel.js");
 Template::includeDependentJs("/js/jquery.ready.js");
 Template::includeDependentCss("/css/jquery.jcarousel.css");
 Template::includeDependentCss("/css/skins/steps/skin.css");
 $jscode = <<<BOF
 <script type="text/javascript">
	//<![CDATA[
		function removeSteps(travelerID){
			
			new Ajax.Request('/ajaxpages/unshowSteps.php',{method : 'get',parameters: {travelerid : travelerID}  });
			
			document.getElementById('stepmaincontainer').style.display = 'none';
			
		}
	//]]>	
 </script>
 
 <script type="text/javascript">
	jQuery.noConflict();
	jQuery.elementReady('steps_carousel', function() {
    	jQuery('#steps_carousel').jcarousel();
		});

</script>
 
BOF;
Template::includeDependent($jscode); 
 
?>
<div id="stepmaincontainer" class="section">
	<div class="head_left"><div class="head_right"></div></div>
	<div class="content">
	<div class="box2">
		<img src="/images/close_button.gif" id="remove_steps_button" onclick="removeSteps(<?=$travelerID?>)" title="Remove this section" alt="close button" height="30" width="30" />
		<p class="title">
			Thank you for registering on <span style="color:#0596c7;">GoAbroad.net</span>
		</p>
		<p class="info">Now the fun begins! Here is how we suggest you proceed with your new Travel Journal adventures.</p>
		
	</div>	
	
	<div id="stepsubcontainer">

		<ol id="steps_carousel" class="jcarousel-skin-tango">
	
			<li class="box" id="step1">
				<p><a href="/edittravelerprofile.php">Complete Your Profile</a></p>
				<p class="step_detail">Give your Journal Entries a personality and open up opportunities to meet like minded travelers!</p>
			</li>
			<li class="box" id="step2">
				<p><a href="/addressbookmanagement.php?action=add">Create Your Address Book</a></p>
				<p class="step_detail">Create your address book. Adding contact info of your friends and family will keep them updated whenever you post new Journal Entries and photos.</p>
			</li>
			<li class="box" id="step3">
				<p><a href="/travel.php?action=add">Create Your Journals</a></p>
				<p class="step_detail">Create a Journal Title and a short Description, Add a Journal Entry and upload Travel Photos.</p>
			</li>
			<li class="box" id="step4">
				<p><a href="/group.php">Join and Create Groups</a></p>
				<p class="step_detail">Join a group, start your own club or share your travel experiences while searching for answers to your questions. Get involved in the network!</p>
			</li>
			<li class="box" id="step5">
				<p><a href="/editgatravelbio.php">Write Your Travel Bio</a></p>
				<p class="step_detail">Share your most memorable travel experiences by answering quick, fun questions about your travels!</p>
			</li>
			<li class="box" id="step6">
				<p><a href="/travelscheduler.php?action=add&amp;src=3">Share Your Travel Plans</a></p>
				<p class="step_detail">Let others know where you've been, where you are, where you're going and what you're up to. List your travel plans and find out who could be traveling your way! </p>
			</li>
			
		</ol>
	</div>

	<div class="clear"></div>
	</div>
	<div class="foot"></div>
</div>	
	
