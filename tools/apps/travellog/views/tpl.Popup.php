<?php if($isThickboxModal): ?>
	<div class="close">
		<a href="" onclick="self.parent.tb_remove(this); return false;"><span>close</span></a>
	</div>
	<div class="container">
		<table class="clear">
			<tr>
				<td>
					<div id="modal_window">
						<h2><?php echo $title;?></h2>
						<div class="line">
							<?php echo $contents;?>
						</div>
					</div>
				</td>
			</tr>
		</table>		
	</div>
<?php else: ?>
	<?php
/*
	 * Tpl.ChangePassword.php
	 * Created on Aug 18, 2006
	 *	Author: K. Gordo
	 */

	Template::setMainTemplate("travellog/views/tpl.LayoutPopup.php");
	Template::includeDependentJs("/js/prototype.js");	
	Template::includeDependentJs('/js/interactive.form.js');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	?>
	<div id="popup_content">
		<div class="section" >
			<h1 id="pop_header"><span><?=$title?></span></h1>
			<div class="content">
				<?=$contents?>
			</div>
			<div class="foot"></div>
		</div>
	</div>
<?php endif; ?>