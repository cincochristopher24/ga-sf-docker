<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('title', $title);
	/*Template::includeDependentCss("/css/tabs.css");*/
	Template::includeDependentJs("/js/bulletinManager.js");
	Template::setMainTemplateVar('page_location', $pageLocation);
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs('/js/interactive.form.js');
	ob_start();
	
	
$incjs = <<<BOF
<script type="text/javascript">
//<![CDATA[
 GAInteractiveForm.create('composePanel');
//]]>
</script>
BOF;

Template::includeDependent($incjs);	
	ob_start();	
?>
<?php if ('DISABLED' != $mode): ?>
<script type="text/javascript">
//<![CDATA[
			BulletinManager.init({
				method: "<?=$method?>",
				groupID : "<?=$groupID?>",
				travelerID : "<?=$travelerID?>",
				hlEntryID : "<?=$hlEntryID?>",
				sendableID : "<?=$sendableID?>",
				concept : "<?=$concept?>",
				isAdmin : "<?=$isAdmin?>",
				isGroupMember : "<?=$isGroupMember?>",
				isSubgroup : "<?=$isSubgroup?>",
				groupType : "<?=$groupType?>",
				mode : "<?=$mode?>",
				itemsPerPage: <?=$itemsPerPage ?>,				
				pageNavPerPage : <?=$pageNavPerPage	?>
			});
//]]>
</script>				
<?php endif; ?>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);
	$subNavigation->show();
?>		

<div class="layout_2" id="content_wrapper">
	
	
	<div id="wide_column">
		<div class="section" id="bulletin_view">
			<h2><span id="transMethod">Bulletin Details</span></h2>
			<!--<div class="head_left"><div class="head_right"></div></div>-->
			<div class="content">
				<?php if('DISABLED'==$mode): ?>
					<!-- <h3>Sorry, but since you do not have any friends yet and you are not a member of any group, you are not allowed to post any bulletins. </h3> -->
					<!-- edited by daf feb.12.07 ; change rule from AND to OR-->
					<p class="help_text"><span>Sorry, but since you do not have any friends and you are not a member of any group, you are not allowed to post any bulletins</span>. </p>
				<?php else: ?>
					<!--
					<ul>
						<li class="" id="detailTab"><strong><a href="javascript:void(0)" onclick="BulletinManager.displayDetailPanel()">Details</a></trong></li>
						<?php if($mode == "READ_WRITE"): ?>
						<li align="center" class="" id="composeTab"><strong><a href="javascript:void(0)" onclick="BulletinManager.displayComposePanel()">Compose</a></strong></li>
						<?php endif; ?>
					</ul>
					-->
					<!-- detail panel -->
					<div id="detailPanel" class="widePanel" style="display:none">													
						<?php if($mode == "READ_WRITE"): ?>
						<div id="methodPanel">
							<!--<a href="javascript: void(0)" id="editEntryLink" onclick="NewsManager.editEntry(0)">[Edit]</a>&nbsp;-->
							<a href="javascript: void(0)" id="deleteEntryLink" onclick="BulletinManager.deleteEntry(0)">[ Delete ]</a>
						</div>
						<?php endif; ?>
						
						<h1 id="detailTitle"></h1>
						<p class="meta info">
						<strong>Posted by:</strong> <span id="detailPostedBy"></span><br />
						<strong>Recipients:</strong> <span id="detailRecipients"></span><br />
						<strong>Posted on:</strong> <span id="detailDate">date</span><br />
						</p>						 
						
						<div id="detailContent"></div>
					</div>
				
					<!-- compose panel -->
					<?php if($mode == "READ_WRITE"): ?>
					<div id="composePanel" class="widePanel" style="display: none;">
						
						<ul class="form">
							<li>
								<label for="composeTitle">Title:</label>
								<p class="error message" id="composeTitleErr"></p>
								<input type="text" class="text" id="composeTitle" style="width:400px" />
							</li>
							<li>
								<label for="composeDetails">Details: </label>
								<p class="error message" id="composeDetailsErr"></p>
								<textarea  id="composeDetails" style="width:400px;height:200px" cols="52" rows="14"></textarea>
							</li>
							<?php if($concept == "GROUP"): ?>
							<li>
								<label for="notifyRecipients"><input type="checkbox" name="notifyRecipients" id="notifyRecipients" />Notify <?= GaString::makePossessive($objContext->getName()) ?> members about this new bulletin.</label>
							</li>
							<?php endif; ?>
							<li>
								<label for="includeMsg"><input type="checkbox" name="includeMsg" id="includeMsg" />Include bulletin message in the notification.</label>
							</li>
							<li>
								<label>Recipient: <?php if($concept == "TRAVELER"): ?>Network of friends<?php elseif($concept == "GROUP"): ?>Group members<?php endif; ?></label>
								<!--								
								<span class="error message" id="composeRecipientErr"></span>
								<fieldset class="choices">								
									<legend><span>Send To: </span></legend>																									
									<div id="togglePanel"><a href="javascript: void(0)" onclick="BulletinManager.toggleCheckBoxes(true)">Select all</a>&nbsp;|&nbsp;<a href="javascript: void(0)" onclick="BulletinManager.toggleCheckBoxes(false)">Deselect all</a></div>
									<?php if($concept == "TRAVELER"): ?>
										<ul>
											<li><input type="checkbox" value="withFriends" id="chkWithFriends" /><label for="chkWithFriends">Network of Friends</label></li>
										</ul>
									<?php elseif($concept == "GROUP"): ?>
										Group Members									
									<?php endif; ?>
									<fieldset class="choices">
										<legend><span id="groupCaption">Subgroups: </span></legend>
										<ul id="groupPanel1" class="column"></ul>
										<ul id="groupPanel2" class="column"></ul>
										<div class="clear"></div>
									</fieldset>
								</fieldset>
								-->
							</li>
							
							<li class="actions">
									<input type="button" class="button submit" value="Post" onclick="BulletinManager.saveEntry()" />
									<!--<input type="button" class="button" value="Cancel" onclick="BulletinManager.cancelEntry()" />-->
									<input type="hidden" value="0" id="composeID" />
							</li> 
						</ul>
						<div class="clear"></div>
					</div>
					<?php endif; ?>
					<div id="statusPanel" class="widePanel" style="display:none">
						<p class="loading_message">
								<span id="imgLoading">
									<img src="/images/loading.gif" alt="Loading" />
								</span>
								<em id="statusCaption">Loading data please wait...</em>
						</p>														
					</div>
					<!--<div id="logPanel" class="widePanel"></div>-->
				<?php endif; ?>				
			</div>
		
			<div class="foot"></div>
		
		</div>
	</div>
	
	<div id="narrow_column">	
			<div class="section" id="quick_tasks">
			
				<h2><span>Bulletin Board</span></h2>
				<div class="content" id="bulletin">
					<?php if((('GROUP'==$concept && $isAdmin == 'true')||'TRAVELER'==$concept) && 'DISABLED'!=$mode):?>
					<ul class="actions">
						<li><a href="javascript:void(0)" onclick="BulletinManager.displayComposePanel()" class="button">Create New Post</a></li>
					</ul>
					<?php endif;?>
					
					<div id="navPanel">
						
					</div>
					
					<div id="pageNavPanel" class="pagination mini"></div>
					<div class="clear"></div>
			
				</div>
			
			<div class="foot"></div>
			
			</div>
	</div>
	
	<div class="clear"></div>
</div>