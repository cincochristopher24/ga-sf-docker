<?php
/*
 * Created on 09 20, 2006
 * @author Kerwin Gordo 
 * Purpose: template for Calendar of activities
 */

require_once('Class.GaDateTime.php');
$d = new GaDateTime();
require_once('Class.DateTimeHolder.php');
$ID = 0;
?>

<div id="events" class="section">
	<h2>
	<span>Events</span>
	<span class="header_actions">
		<?php if(isset($owner) && $owner):?>
			<a id="create" href="/calendar-event.php?action=Add">Add Event</a> |
		<?php elseif( !isset($owner) ):?> 
			<a id="create" href="/calendar-event.php?action=Add">Add Event</a> |
		<?php endif;?> 
	
		<?if ((isset($eventCount) && 5 < $eventCount) || (isset($owner) && $owner)) : ?>
			<?php
				if ($owner) $linkLabel = 'Manage Events';
				elseif (5 < $eventCount)
					$linkLabel = 'View All Events';
			?>
			<a href="/calendar-event.php<?if(isset($link_view)) echo '?'.$link_view; ?>" class="more"> <?=$linkLabel?></a> 
		<?endif;?>
	</span>
	</h2>
	
	<div class="content">
	
	<? if (isset($calEvents) && 0 <  count($calEvents)) { ?>
			<ul class="events_container events" id="events_list">
			<?$cnt = 0;?>
			<?foreach($calEvents as $k => $calEvent)  :?>	 	
			 	<? if($cnt >= 5) break; ?>
		 		<li>
		 			<?
		 		        $editLink = '';
		 				$delLink = '';
		 				
		 				// activity event for AdminGroup
		 				if (0 == $calEvent->getContext()) {
		 					//$editLink = 'activity.php?action=edit&amp;aID=' . $calEvent->getEventID().'&context=0';
		 					//$delLink = 'activity.php?action=delete&amp;aID=' . $calEvent->getEventID().'&context=0';
		 					$editLink = 'event?action=edit&eID=' . $calEvent->getEventID().'&context=0';
		 					$delLink = 'event?action=remove&eID=' . $calEvent->getEventID().'&context=0';
		 				}
		 				
		 				/**
			             * Edited By: Aldwin Sabornido
			             * Date     : Dec. 8, 2006
			             * Purpose  : I've changed the link actions...'
			             */
		 				// group event for all groups
		 				if (1 == $calEvent->getContext()) {
							$ID = 0;
		 					//$editLink = 'event.php?action=editGroupEvent&amp;eID=' . $calEvent->getEventID();
		 					//$delLink = 'event.php?action=deleteGroupEvent&amp;eID=' . $calEvent->getEventID();
		 					$editLink = '/event.php?action=edit&eID=' . $calEvent->getEventID();
		 					$delLink = '/event.php?action=remove&eID=' . $calEvent->getEventID();
		 				}
		 				
		 				// traveler event 
		 				if (2 == $calEvent->getContext()) {
							$ID = isset($_GET["travelerID"]) ? $_GET["travelerID"] : 0;
		 					//$editLink = 'event.php?action=editTravelerEvent&amp;eID=' . $calEvent->getEventID();
		 					//$delLink = 'event.php?action=deleteTravelerEvent&amp;eID=' . $calEvent->getEventID();
		 					//$editLink = '/calendar-event.php?action=edit&eID=' . $calEvent->getEventID();
		 					//$delLink = '/calendar-event.php?action=delete&eID=' . $calEvent->getEventID();
		 					$editLink = '/calendar-event.php?action=edit&eID=' . $calEvent->getEventID();
		 					$delLink = '/calendar-event.php?action=remove&eID=' . $calEvent->getEventID();
		 				}
		 				
		 				$raw_date = strtotime($calEvent->getTheDate());
		 			?>
					
					<?php
						// we will now load meta date through ajax since we will still convert it to local time
						$containerClass = $calEvent->getEventID().'_'.strtotime($calEvent->getTheDate()).'_'.$calEvent->getTimeZoneID().'_'.$k;
						$link_to_event = urlencode((isset($link_view)) ? $calEvent->getLink().'&'.$link_view : $calEvent->getLink());
						DateTimeHolder::getInstance()->addProfileEventDetails($calEvent->getEventID(),$calEvent->getTitle(),$calEvent->getContext(),$link_to_event,$owner);
						DateTimeHolder::getInstance()->addDateTime($calEvent->getTheDate(), $containerClass, $calEvent->getTimeZoneID());
					?>
					
					<span class="meta date" id="<?php echo $containerClass; ?>"></span>
					
					
			 		<?/*<span class="meta date" id="<?php echo $containerClass; ?>">
			 			<?= $d->set($raw_date)->htmlEventDateFormat(); ?>
						<span id="events_time" ><?php echo date('h:i A', $raw_date) ?></span>
						<?php if(function_exists('date_create') && $calEvent->getTimeZoneID() ):?>
						<span id="events_zone" onmouseover="jQuery().showEquivalentLocalTime('<?=$calEvent->getEventID()?>','groupevent', this)" onmouseout="CustomPopup.removeBubble();">
							<?php echo 'GMT '. substr(date_create($calEvent->getTheDate(), new DateTimeZone($calEvent->getTimeZone()))->format('O'),0,3); ?>
						</span>
						<?php endif; ?>
			 		</span>	*/?>
					
					
					
			 		<h4><a href="<?= $calEvent->getLink()?><?if(isset($link_view)) echo '&'.$link_view;?>"><?=$calEvent->getTitle()?></a></h4>
			 		<?/* if (isset($myCreatedGroups) && (CalendarEvent::$ACTIVITY == $calEvent->getContext() || CalendarEvent::$GROUP_EVENT == $calEvent->getContext())) : ?>
			 			<?if (array_key_exists('' . $calEvent->getContextID(),$myCreatedGroups )):?>
			 				<div class="controls"><a href="<?=$editLink?>">Edit</a> | <a href="<?=$delLink?>" onclick="return confirm('Are you sure you want to delete this item?')">Delete</a></div>
			 			<?endif;?>
			 		<? elseif (isset($canEditPersonalEvent)) :?>	
			 			<div class="controls"><a href="<?=$editLink?>">Edit</a> | <a href="<?=$delLink?>" onclick="return confirm('Are you sure you want to delete this item?')">Delete</a></div>
			 		<? endif; */?>
					<?if($owner && CalendarEvent::$TRAVELER_EVENT == $calEvent->getContext()):?>
						<div class="details"><a href="<?=$editLink?>">Edit</a> | <a href="javascript:void(0);" onclick="CustomPopup.initialize('Delete Event ?','Are you sure you want to delete this event?','<?=$delLink?>','Delete Event','1');CustomPopup.createPopup();">Delete</a></div>
					<?endif;?>
			 		<div class="clear"></div>
		 		</li>
				<? $cnt++; ?>
		 	<?  endforeach ; ?>	
		 	</ul>
	<? } else {?>
	 	<p class="help_text">
	 		<? echo 'There are no current calendar events.'; ?>	
	 	</p>
	<? } ?>
		
	</div>
	<script type="text/javascript" src="/min/g=EventProfileJs"></script>
<!--
	<script type="text/javascript" src="/js/jquery.travelerEvent.js"></script>
	<script type="text/javascript" src="/js/localtime.js"></script>
	<script type="text/javascript" src="/js/jquery.json-1.3.min.js"></script>
-->
	<?php if( 0 < count($calEvents) ): ?>
	<script type="text/javascript" charset="utf-8">
		b = '<?php echo DateTimeHolder::getInstance()->toJsonData()?>';
		LocalTimeConverter.idBased = true;
		LocalTimeConverter.convertAndSortEventDates(b,1,<?=$ID?>);
	</script>
	<?php endif; ?>
	<div class="foot"></div>
</div>
