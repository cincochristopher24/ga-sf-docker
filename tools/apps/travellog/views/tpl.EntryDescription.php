<? require_once("Class.FriendlyUrl.php"); ?>

<?$obj_next_prev = $contents['next_prev'];?> 

<div class="entryholder">
<?if( $contents['show_admin_controls'] ):?>
	<div class="entrydesc_control" rel="<?=$contents['obj_entry']->getTravelLogID()?>">	 
		<!--p class="help_text">To avoid loss of information, we encourage you to copy your journal entry from a text editor.</p-->
		<a href="javascript:void(0)" class="entry_links top_shift" id="EntryDescriptionLink"><span>Edit Entry Content</span></a>   
	</div>
<?endif;?>
<div id="ENTRYDESCRIPTION" class="entrydescription">
	<?if(!preg_match('/<p[^>]*>/',$contents['obj_entry']->getDescription())):?>
		<?=nl2br(preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/","<a href=\"\\0\">\\0</a>", $contents['obj_entry']->getDescription()))?>
	<?else:?>
		<?=preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/","<a href=\"\\0\">\\0</a>", $contents['obj_entry']->getDescription())?>
	<?endif;?>
</div>
</div>

<div class="group_bug">
<?if(!isset($GLOBALS['CONFIG'])):?>	
<ul>
<?php
	$rGroups = GroupApprovedJournals::getRelatedGroups($contents['obj_entry']->getTravelID());
	
	foreach($rGroups as $owner):
		//$owner = $contents['obj_journal']->getGroup();
		if( $owner instanceof Group ):
			/*$approved2 = false;
			$approved1 = $contents['obj_journal']->getIsGroupApproved(); 
			if( !$owner->isParent() ){ 
				$owner     = $owner->getParent();
				$approved2 = $contents['obj_journal']->getIsGroupApproved();	
			}
			if( $approved1 || $approved2 ):*/
			$site = $owner->getServerName();
	
?>
	<li style="clear: both;">
		<?php if( $site != false ):?>
						
			<a href="http://<?php echo $owner->getServerName()?>" class="group_logo"><img src="<?php echo $owner->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" /></a>
						
		<?php else:?>
		
			<a href="/group.php?gID=<?php echo $owner->getGroupID()?>" class="group_logo"><img src="<?php echo $owner->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" /></a>
			
		<?php endif;?>


			<div><?php if( $site != false ):?>
				<a href="http://<?php echo $owner->getServerName()?>"><h2><?php echo $owner->getName()?></h2></a>
				
				<?php if( GroupApprovedJournals::hasMoreThanOneJournal($owner->getGroupID()) ):?>
				<?php //if( $contents['obj_journal']->getHasMoreThanOneJournal() ):?>
					<a href="http://<?php echo $owner->getServerName()?>/journal.php">view other journals</a>
				<?php endif;?>
							
			<?php else:?>
				<a href="/group.php?gID=<?php echo $owner->getGroupID()?>"><h2><?php echo $owner->getName()?></h2></a>
				
				<?php if( GroupApprovedJournals::hasMoreThanOneJournal($owner->getGroupID()) ):?>
				<?//php if( $contents['obj_journal']->getHasMoreThanOneJournal() ):?>
					<a href="/journal.php?action=groupJournals&gID=<?php echo $owner->getGroupID()?>">view other journals</a>       
				<?php endif;?>
				
			<?php endif;?></div>
		</li>
<?php
			//endif;
		endif; 
	endforeach;
?> </ul>
<?endif;?>
</div>

<ul id="journal_entry_page">
    <?if( $obj_next_prev['prev'] != NULL ):?>
    	<li class="previous"> 
    		<a href="<?=preg_replace('/\/[^\/]*$/', '', $obj_helper->getLink($obj_next_prev['prev']))?>/<?=urlencode(FriendlyUrl::encode($obj_next_prev['prev']->getTitle()))?>" title="Go to the previous journal entry">
    			<?= HtmlHelpers::truncateWord($obj_next_prev['prev']->getTitle(), 30); ?>
    		</a>
   			<span class="entry_title_info"><?=HtmlHelpers::truncateWord($obj_next_prev['prev']->getCity(), 20)?><br/><?=$obj_next_prev['prev']->getArrival()?><?if( $obj_next_prev['prev']->getCountPhotos() ):?><br/><?=$obj_next_prev['prev']->getCountPhotos();?> Photo<?if( $obj_next_prev['prev']->getCountPhotos() > 1 ) echo 's'?><?endif;?></span>
    	</li>
    <?endif;?>	
   
<?if( $obj_next_prev['next'] != NULL ):?>
    	<li class="next"> 
    		<a href="<?=preg_replace('/\/[^\/]*$/', '', $obj_helper->getLink($obj_next_prev['next']))?>/<?=urlencode(FriendlyUrl::encode($obj_next_prev['next']->getTitle()))?>" title="Go to the next journal entry">
    			<?= HtmlHelpers::truncateWord($obj_next_prev['next']->getTitle(), 30); ?>
    		</a>
			<span class="entry_title_info"><?=HtmlHelpers::truncateWord($obj_next_prev['next']->getCity(), 20)?><br/><?=$obj_next_prev['next']->getArrival()?><?if( $obj_next_prev['next']->getCountPhotos() ):?><br/><?=$obj_next_prev['next']->getCountPhotos();?> Photo<?if( $obj_next_prev['next']->getCountPhotos() > 1 ) echo 's'?><?endif;?></span>
    	</li>

    	
    <?endif;?>
</ul>

<? if (!$contents['show_admin_controls']): 	// added by neri: 11-12-08 ?>
	<a id="report" href="/reportabuse.php?jeID=<?=$contents['obj_entry']->getTravelLogID()?>">Report inappropriate journal entry</a>
<? endif; ?>
