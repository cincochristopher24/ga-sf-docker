<?php
	require_once 'travellog/model/travelbio/Class.FBini.php';
?>	
	<? if ($owner): ?>
		<div class="header_actions<? if (count($answers)) : ?> edit_action<?endif;?>">
			<? if (count($answers)) : ?>
				<a class="editadd_travelbio" href="editgatravelbio.php" > Edit Travel Bio </a>
			<? else : ?>
				<a class="editadd_travelbio" href="editgatravelbio.php" > Add Travel Bio </a>
			<? endif; ?>
		</div>
	<? endif; ?>
	
	<? if ($owner || count($answers)): ?>
		<div class="content" id="travelbio_content">
	<? endif;																

	if (count($answers)){								
		$qCtr = 0;
		$t = 1;
		$defaultQ = 16;
		$isViewAll = false;
		
		foreach($answers as $iAns){
			if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
				$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
			}
		}		
	}
	
	

	if (count($answers)){	
		echo '<ul id="travelbio_qa">';
			foreach($profileQuestions as $iQuestion){
				if(array_key_exists($iQuestion->getQuestionID(),$structAns)){	
					if($qCtr >= $defaultQ){
						echo '<li class="travelbio_questions_list" id=ans'.$t.' style="display: none;"> <h3 class="travelbio_questions">' . $iQuestion->getIntroduction() . '</h3>';
						$t +=1;
						$isViewAll = true;
					}
					else{
						echo '<li class="travelbio_questions_list"> <h3 class="travelbio_questions">' . $iQuestion->getIntroduction() . '</h3>';
					}
					
					if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT || $iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA){
						$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
							? $structAns[$iQuestion->getQuestionID()][0]->getValue()
							: '';
						echo '<div class="travelbio_answers_container"><p class="travelbio_answers"> '. $val .'</p></div><div style="clear: both;"></div>';
						$qCtr+=1;
					}
					elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX){
						$textVal = '';
						$ictr = 0;
						echo '<div class="travelbio_answers_container">' .
								'<p class="travelbio_answers">';
								foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){
									$tempAnswer = (0 == $ictr) ? $iChoice->getValue() : ', ' . $iChoice->getValue();
									if($iChoice->isTypeCheckbox() && 'Others' != $iChoice->getValue()){
										echo $tempAnswer;
									}
									elseif($iChoice->isTypeText()){
										$textVal = $iChoice->getValue();
									}
									$ictr += 1;
								}
								if(strlen($textVal)){
									$tempAnswer = (0 == $ictr) ? $textVal : ', ' . $textVal;
									echo $tempAnswer;
								}
								$qCtr+=1;
							echo '</p>';
						echo '</div><div style="clear: both;"></div>';
					}
					echo '</li>';
				}		
			}
		echo '</ul>';			
	}
	else{
		if($owner) : ?>
			<p class="help_text" style="margin: 0 0 0 0; padding: 0 0 0 5px">
				</span>You have no Travel Bio yet</span>.
			</p>
		<?  else : ?>
			<p class="help_text">
				<strong><i><?=$username ?></i></strong> did not add a Travel Bio yet.
			</p>
		<? endif;
	}

	if ($owner || count($answers)): ?>
		</div>	
	<? endif;?>