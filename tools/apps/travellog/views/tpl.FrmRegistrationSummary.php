<?php
	/*
	 * tpl.RegistrationSummary.php
	 * Created on Jun 26, 2007
	 * created by marc
	 */
	 require_once("Class.Constants.php");
	$redirect = (0 < trim(strlen($contents['redirect']))) ? '?redirect='.$contents['redirect'] : '';
?>

<div id="content_wrapper">
	<div class="section" id="section_register">
		<div class="head_left"><div class="head_right"></div></div>
		<div class="content" id="register_summary">
			<h1>Please review your email address before you continue: </h1>
			<?/*<p>We will be sending an email to activate your account, 
			  and it will be sent to the email address you have provided.</p>
			<p>If you 
				wish to edit any of the information below, or if you mistyped anything, just click on the "<span class="register_blue">Back</span>" 
				button; otherwise, please click on "<span class="register_blue">Continue registration</span>".</p>
		  */?>
			<dl>
			
				<? if (false): // Remove the following for now ?>
  				<? if( in_array($contents["user"],array(constants::TRAVELER,constants::TRAVELERCB)) ): ?>
  					<dt>Name :</dt>
  					<dd> <strong><?= $contents["firstname"]; ?> <?= $contents["lastname"]; ?></strong></dd>
  				<? else: ?> 
  					<dt>Name of Advisor Group :</dt>
  					<dd><strong><?= $contents["instname"]; ?></strong></dd>
  				<? endif; ?>
				<? endif; ?>
				
				<dt><br />Email Address :</dt>
				<dd>
					<strong><?= $contents["emailAdd"]; ?></strong>
					<p>
					  <br />
					  We will send your account information to you after this registration,
					  please make sure the address above is correct. If you mistyped it, 
					  simply click on the <strong>Back</strong> button; otherwise, please click on 
					  <strong>Continue Registration</strong>.
					</p>
				</dd>
				
				<? if (false): // Remove the following for now ?>
				  <dt>Username :</dt>
  				<dd> <strong><?= $contents["username"]; ?></strong><br />
  					<span id="span_username">The username you have indicated is the name that will appear on your profile.</span> 
  				 </dd>
			
  			    <dt>Password :</dt>
  				<dd><strong>******</strong><br />
  			    	<span class="spans_form2a">Your password is not shown here for security purposes, it will be emailed to you once you're done with this registration.</span>
  				</dd>
				
  				<? if( in_array($contents["user"],array(constants::TRAVELER,constants::TRAVELERCB)) && $contents['chkStudent']): ?>
  					<dt>University :</dt>
  					<dd><span class="spans_form1"><?= $contents["university"]; ?></span></dd>
  				<? endif; ?>
			  
			  <? endif; ?>
				<? if( $contents['subscribeNL'] ): ?>
					<dt>&nbsp;</dt>
					<dd>
					<p><span class="spans_form1">You have chosen to subscribe to the GoAbroad.com Newsletter. We will send you(<?= $contents['emailAdd']; ?>) an email that you will use to verify email address ownership.
GoAbroad.com is doing this to maintain our reputation of sending valuable information only to those who asked for it.</span></p>
					<p><span class="spans_form1">Also, please don't forget to add newsletter@goabroad.com to your address book or your safe sender list.</span></p>
					</dd>
				<? endif; ?>
			</dl>	
				<form action="register.php<?=$redirect?>" method="post" id="registration_confirmation">
					<ul class="form">
						<li class="actions">
							<input class="buttons submit" type="submit" name="btnBackRegister" value="Back" />
							<input class="buttons submit" type="submit" name="btnContinueRegister" value="Continue Registration" />
							<input type="hidden" name="hdnRegKey" value="<?= $contents["regKey"]; ?>" />
							<input type="hidden" name="hdnRegID" value="<?= $contents["regID"]; ?>" /> 
							<input type="hidden" name="hdnMode" value="<?= $contents["user"]; ?>" />
							<?php if(isset($contents["service_provider"]) && !is_null($contents["service_provider"])): ?>
								<input type="hidden" name="hdnAuthorizedSP" value="<?php echo $contents["service_provider"];?>" />
							<?php endif; ?>
							<?//added this hidden input field to store advisor's firstname; tblTmpRegister doesn't have the firstname field?>
							<input type="hidden" name="txtFirstName" value="<?=$contents["firstname"];?>" />
						</li>
				    </ul>
				</form>  
				<div class="clear"></div>
		</div>
		<div class="foot"></div>	
	</div>
</div>
