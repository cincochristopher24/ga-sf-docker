<?php
	/**
	 * Created on 09 25, 2006
	 * Purpose: template to list comments
	 * Last edited by: Cheryl Ivy Q. Go  27 September 2007
	 * For Greetings, Salutations and Comments
	 */
	 
	$d = new GaDateTime();

	$t = 1;
	$ictr = 0;
	$default = 4;
	$isViewAll = false;
	
	$travelerID = (0 == $travelerID) ? -1 : $travelerID;

	if ($photoCat !== '' && CommentType::$PHOTO == $context)
		$divID = 'commentscontainer';
	else
		$divID = 'comments';
	
	$link = 'http://' . $_SERVER['HTTP_HOST'];
?>
<? if(!$showConfirmationMessageOnly): ?>
	<h2>
	  <span>Shout-out</span>
	
		<span class="header_actions ">
		  <? if($canAddComment):?>
		    <a class = "add" href = "javascript:void(0)" onclick = "Comment.showCommentBox();">
		      Post a Shout-out
		    </a>
		  <? endif; ?>
		</span>
	</h2>
<? endif; ?>
<div class="content">
  	<? if(!$showConfirmationMessageOnly): ?>
		<p id = "comment_status_zone" class = "hide_status_zone">
		    <span id = "imgLoading">
		      <img alt = "Loading" src = "/images/loading.gif"/>
		    </span>
			<em id = "statusCaption">Loading please wait...</em>
		</p>
  	<? endif; ?>
  <ul id = "display_comments_content">	
  	<? if (0 < strlen($message)):?>
	    <p class="notice">
	      <?=$message?>
	    </p>
	<? endif; ?>	
    <? if(!$showConfirmationMessageOnly): ?>
		<? if (0 < count($comments)) : ?>	  	  
				  <?php 
					  foreach($comments as $comment ) : 
					  	$author = $comment->getAuthorObject();
					    $link = "javascript:void(0)";
			      
				  	      switch($author->getAuthorType()){
				  	        case CommentAuthor::TRAVELER:
				  	          $link = "/".$author->getName();
				  	          break;
				  	        case CommentAuthor::ADMIN_GROUP:
				  	          //$link = "/groups/".$author->getName();
							  $link = $author->getAuthorAsTraveler()->getTravelerProfile()->getUserName();
				  	          break; 	    
				  	      }
			      			
					    		$tmpClassName = (trim($comment->getTheText()) !== "") ? "comment_text" : "";
		  		    
								$li_attributes = '';
					
						  		if($ictr >= $default){
								  $li_attributes .= ' id="greeting'.$t.'" style="display: none;"';
									$t +=1;
									$isViewAll = true;
								}
						
								$Poke = new PokeType($comment->getPokeType());

							 	$pokesview = new PokesView();
							 	$pokesview->setVisitorId($travelerID);		// travelerId of logged-in user
							 	$pokesview->setOwnerId($ownerID);			// travelerId or groupId of profile/photo/journal-entry
							 	$pokesview->setPokeTypeId($Poke->getPokeTypeID());
							 	$pokesview->setContextId($contextID);
							 	$pokesview->setContext($context);
							 	$pokesview->setAuthorObject($author);
							 	
						?>
							<li <?= $li_attributes ?> >
					 		  <a href="<?=$link?>"  class="thumb">
					 			  <img src="<?=$author->getPhotoSource()?>" height="37" width="37" alt="User Profile Photo" />
					 			</a>

							 	<div class="comments_content">
						 				<div class="comment_info"> 
						 				  <strong>
						 				    <?=$pokesview->getMessage()?>
						 				  </strong> 
				 				  
						 				  <span class="timeCreated jRight">
						 				    <?= $d->set($comment->getCreated())->friendlyFormat() ?>
						 				  </span>					 							
										</div>
									<? if (0 < $comment->getPokeType()) : ?>
										<? $class_name = preg_replace('/.png$/','',$Poke->getPokeImage())?>
										<div class="shout do_<?=$class_name?>">do <?= $class_name ?></div>
									<? endif; ?>
									
									<div id="thecommenttext<?=$comment->getCommentID()?>" class="<?=$tmpClassName?>">	
										<?= nl2br(stripslashes($comment->getTheText())) ?>
									</div>

									<? if ($travelerID == $author->getAuthorID() || isset($okToDelete)) : ?>
										<div id="actions<?=$comment->getCommentID()?>" class="actions">
									 			<? if ($travelerID == $author->getAuthorID()) : ?>
									 					<a href="javascript:void(0)" onclick="Comment.setDivId('<?=$divID?>');Comment.showedit(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>,<?=$travelerID?>)">Edit</a> |
														<a href="javascript:void(0)" onclick="Comment.setDivId('<?=$divID?>');Comment.deletecomments(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>,<?=$travelerID?>)">Delete</a>
									 			<? elseif (isset($okToDelete)): ?>
									 					<a href="javascript:void(0)" onclick="Comment.setDivId('<?=$divID?>');Comment.deletecomments(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>,<?=$travelerID?>)">Delete</a>
									 			<? endif; ?>
									 	</div>
									<? endif; ?>
								 	<div id="commenteditloading<?=$comment->getCommentID()?>" style="display:none;color:red;padding-top:10px;">
								 		<img src="/images/loadingAnimation.gif"/>
								 	</div>
					 	

								 </div>
							</li>
							<? $ictr += 1; ?>
				  		<? endforeach; ?>
					</ul>  		
		<? else:?>
			</ul>		
		 	<? if(!$showConfirmationMessageOnly): ?>
				<div class="help_text">
					<? $owner = new TravelerProfile($ownerID); ?>
					<? if ($travelerID == $owner->getTravelerID()): ?>
							<span>No one has left you a shout-out yet</span>. <a href="/travelers.php"><strong>Post a shout-out</strong></a> to other travelers!
					<? else: ?>
			  			<? if(CommentType::$TRAVELLOG == $context): ?>
			  				<?
			  					$entry = new Travellog($contextID);
			  					if ($entry->getTravel()->isGroupJournal()):
			  						$ag = $entry->getOwner(); ?>
			  						Be the first to post on <span><?=$ag->getName()?>'<?php if ('s' != substr($ag->getName(), -1)) { ?>s<? } ?></span> travel page!			  					
			  				<?  else: ?>
			  						Be the first to post on <span><?=$owner->getUserName()?>'<?php if ('s' != substr($owner->getUserName(), -1)) { ?>s<? } ?></span> travel page!
			  				<? 	endif; ?>
						<?php elseif( CommentType::$VIDEO == $context ): ?>
							Be the first to post a comment on this video!
			  			<? else: ?>
			  				Be the first to post on <span><?=$owner->getUserName()?>'<?php if ('s' != substr($owner->getUserName(), -1)) { ?>s<? } ?></span> travel page!
			  			<? endif; ?>
			  			<?/* $gID = 0; ?>
			  			<? if ($gID > 0 ): ?>
			  				<? $ag = new AdminGroup($gID); ?>
			  				Be the first to post on <span><?=$ag->getName()?>'<?php if ('s' != substr($ag->getName(), -1)) { ?>s<? } ?></span> travel page!
						<? else: ?>
								Be the first to post on <span><?=$owner->getUserName()?>'<?php if ('s' != substr($owner->getUserName(), -1)) { ?>s<? } ?></span> travel page!
				  		<? endif; */?>
			  			<? if (0 >= $travelerID): ?>
				  			If you are a member, <a href="/login.php">log in</a> to leave a shoutout.
				  		<? endif; ?>
				  		<? if (0 >= $travelerID): ?>
				  			<br />
							<?php if( $directSignupAllowed ): ?>
				  				Not yet a member? <a href="/register.php">Register</a> now&#8212;it&#8217;s fast, easy and totally free.
							<?php endif; ?>
						<? endif; ?>
					<? endif; ?>
	  			</div>
	  		<? endif; ?>
		<? endif; ?>
			  
			<? $all_link_style = ((isset($isViewAll) && $isViewAll)) ? "display:block;" : "display:none;"; ?>
			<? if(!$showConfirmationMessageOnly): ?>
				<div class="section_foot" style="<?=$all_link_style?>"> 
			  		<a id="greet_layer" href="javascript:void(0)" onclick = "Comment.ecollapse(<?=$t?>, 'greet_layer')" > 
				    	View All 
				  	</a>
				</div>
			<? endif; ?>
	<? 	else: ?>
		</ul>
	<?	endif; ?>
</div>

<? if ( isset($_SESSION['popup_shoutout']) && ($_SESSION['popup_shoutout']) ) : ?>
	<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				Comment.showCommentBox();
			});
		})(jQuery);
	</script>
	<? unset($_SESSION['popup_shoutout']) ?>
<? endif; ?>