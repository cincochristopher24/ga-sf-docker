<?php if(isset($cobrand)): ?>
	<link href="/custom/<?php echo $cobrand ?>/css/main.css" rel="stylesheet" type="text/css" />
<?php endif; ?>
	
<style type="text/css">
<!--
  @import url(/css/video.css);
  @import url(/css/widget.css);
  @import url(/css/type.css);
  @import url(/css/forms.css);
-->
</style>

<script type="text/javascript">
	window.focus();
		
	function validateLink() {
		var mode = "";
		var string = "";
		var videoUrl = document.getElementById("videoUrl").value;
		
		if (0 < videoUrl.length) {
			if ((-1 == videoUrl.indexOf("embed")) && (-1 == videoUrl.indexOf("object")) && (-1 != videoUrl.indexOf(".com"))) {
				if ((-1 != videoUrl.indexOf("youtube.com")) || (-1 != videoUrl.indexOf("video.google.com"))) {  
					document.getElementById("formcont").style.display = "none";
					document.getElementById("loading_image").style.display = "block";
					return true;
				}
				
				else {
					mode = "block";
					string = "Video URL is not supported.";
				}
			}
			
			else {
				mode = "block";
				string = "Invalid video URL.";
			}
		}
		
		else if (0 == videoUrl.length){
			mode = "block";
			string = "Video URL is required.";	
		}
		
		document.getElementById("linkError").style.display = mode;
		document.getElementById("linkError").innerHTML = string;
		return false;
	}
	
	function closeWindow(reload) {
		window.close();
		
		if ("" != reload)
			window.opener.location.href = "/" + reload;
	}
</script>

<div id="popup_content" class="prompt" style="display:block">
	<h2 id="pop_header" class="header">
		<strong>
			<? if (isset($videoID)): ?>
				<? if (!$videoID): ?> 
					Add a Video
				<? else: ?> 
					Edit a Video
				<? endif; ?>	
			<? endif; ?> 
		</strong>
	</h2>
	<div class="confirm">
		<? if (isset($message)): ?>
			<p class="confirmation"><?= $message; ?></p>
			<p><input type="button" name="button" value="Close" onclick="closeWindow('<?= $reload ?>')" /></p>
		
		<? else: ?>
		
			<div id = "loading_image" style = "display:none">
				<span> <center> <img src = "/images/loading.gif"/> </center> </span> 
			</div>
			
			<form action="video.php?action=saveFromPopUp" method="post" onsubmit="return validateLink();">  
				
				<div class="form" id = "formcont">
					<span id = "linkError" style = "display: none" class = "required"> </span>
					<div>
						<div><strong>URL:</strong></div>
						<div><input type="text" id="videoUrl" name="videoUrl" value="<?= $videoUrl ?>" size="30" class="inputs"/></div>
					</div>
					<div>
						<div class="left"><strong>Title:</strong></div>
						<div><input type="text" id="title" name="title" value="<?= htmlspecialchars($videoTitle) ?>" size="30" class="inputs"/></div>
					</div>						
					<div>
						<div class="left"><strong>Caption:</strong></div>
						<div><textarea name="caption" id="caption" class="inputs" style="width:378px;"><?= htmlspecialchars($videoCaption) ?></textarea></div>
					</div>
				</div>
	</div>
	<div class="buttons_box">
		<input type="submit" id = "btnSubmit" name="btnSubmit" value="<?= $buttonLabel ?>" onclick = "return validateLink()" class="prompt_button"/>
		<input type="button" id = "btnCancel" name="btnCancel" value="Cancel" onclick = "window.close()" class="prompt_button"/>
	</div>			
				<input type = "hidden" id = "videoID" name = "videoID" value = "<?= $videoID ?>">
				<input type = "hidden" id = "type" name = "type" value = "<?= $type ?>"> 
				<input type = "hidden" id = "jeVideos" name = "jeVideos" value = "<?= $jeVideos ?>">
				
				<? if ('album' == $type): ?>
					<input type = "hidden" id = "albumID" name = "albumID" value = "<?= $albumID ?>">
				<? else: ?>
					<input type = "hidden" id = "travelID" name = "travelID" value = "<?= $travelID ?>">
					<input type = "hidden" id = "travelLogID" name = "travelLogID" value = "<?= $travelLogID ?>">
				<? endif; ?>
				
				<? if (isset($travelerID)): ?>
					<input type = "hidden" id = "travelerID" name = "travelerID" value = "<?= $travelerID ?>">
				<? else: ?>
					<input type = "hidden" id = "gID" name = "gID" value = "<?= $groupID ?>">
				<? endif; ?>
			</form>
		<? endif; ?>
</div>