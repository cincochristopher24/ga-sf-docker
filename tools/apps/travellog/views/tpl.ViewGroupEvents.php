<?php
/**
 * <b>View Event</b> template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */

//SubNavigation::displayLinks();
 
?>

<div class="area" id="top">
	<div class="section" 
		<h1><?= $group_title ?></h1>
	</div>
</div>

<div class="area" id="main">
	<?= $contents ?>
	<div class="clear"></div>
</div>
		
