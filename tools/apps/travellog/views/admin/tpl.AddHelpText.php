<?php
/*
 * Created on 11 28, 06
 *
 * @author Kerwin Gordo
 * Purpose: template to add GA.net HelpText 
 */
 
 Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
 
?>

<h2>Add Help Text</h2>	

  <br />
  <a href="helptext.php">View/Edit/Delete Helptext</a> 
  <br />
    
  <?if (isset($message)):?>
  	<br />
  	<ul><li><?=$message?></li></ul>	
  	<br />
  <?endif;?> 	
  
  <form name="addform" method="post" action="helptext.php?action=save" >
  <h3>Help Key</h3>
	  <input type="text" name="hkey">  
	  <div>
	  	  <h3>Help Text Description</h3>
	  	  <textarea id="helpdesc" name="helpdesc" rows="3" cols="80" wrap="on"></textarea>			 
	  </div>
	  
	  <br /><br />
	  
  	  <div>  	
  	    <h3>Help Text</h3>		
	    <textarea name="helptext" rows="10" cols="80" wrap="on"></textarea>  
  	  </div>
	  
	  
  	  <input type="submit" name="name" value="Save" title="Add Help Text and Description" />    
  </form>

