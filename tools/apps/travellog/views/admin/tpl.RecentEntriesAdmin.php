<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
	require_once 'travellog/controller/admin/Class.ViewFeaturedDatesController.php';
?>

<div id = "intro_container">
	<div id = "intro" align = "center">
		<h1>JOURNAL ENTRIES</h1>
	</div>
</div>

<div id = "content_wrapper" class = "layout_2">
	<div id = "wide_column">
		<h2>
			<span><?=$label?></span>
		</h2>
		<div class = "section">
			<div class = "content">
				<div style = "padding-top: 10px;">
					<?=$search_template?>
					<span style = "float: right">
						<?=$entries_link?>
					</span>
				</div>
				<div style = "padding-top: 20px;">
					<? if ($paging->getTotalPages() > 1) : ?>
						<div><h2><?=$showLabel?></h2></div>

						<div class = "pagination">
							<div class = "page_markers">
								<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec?>
							</div>	
							<?$paging->getFirst()?>
							&nbsp;|&nbsp;
							<?$paging->getPrevious()?>
							&nbsp;&nbsp;
							<?$paging->getLinks()?>
							&nbsp;&nbsp;
							<?$paging->getNext()?>
							&nbsp;|&nbsp;
							<?$paging->getLast()?>
						</div>
					<? endif; ?>

					<? if ( 0 < count($recentEntries) ) : ?>

						<ul class = "users">
							<? foreach ( $recentEntries as $eachentry ) : ?>
								<li>
									<a class = "thumb" href = "../journal-entry.php?action=view&amp;travelerID=<?=$eachentry->getTravelerID()?>&amp;travellogID=<?=$eachentry->getTravelLogID()?>">
										<img alt = "Journal Entry Photo" src="<?=$eachentry->getRandomPhoto()->getPhotoLink()?>" style = "height: 100px; width: 150px" />
									</a>
									<div class = "details" style = "padding-left: 85px;">
										<a href = "../journal-entry.php?action=view&amp;travelerID=<?=$eachentry->getTravelerID()?>&amp;travellogID=<?= $eachentry->getTravelLogID()?>" >
											<span style = "font-size: 14px; font-weight: bold">
												<?
													$mTitle = (40 < strlen($eachentry->getTitle()))
																? substr($eachentry->getTitle(), 0, 40) . '..'
																: $eachentry->getTitle();
													echo $mTitle;
												?>
											</span>
										</a>
										<hr width = "25%" align = "left" />
										<? if ($section->isItemFeatured($eachentry->getTravelLogID())) : ?>
											<p class = "gray"><strong>(Featured)</strong></p>
											<p>
												&raquo; 
												<a href="entries.php?feature=<?=$eachentry->getTravelLogID()?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">Remove Feature</a>
											</p>
										<?  elseif (!$eachentry->getOwner()->isSuspended()) : ?>
											<p>
												&raquo; 
												<a href="entries.php?feature=<?=$eachentry->getTravelLogID()?>">Feature This!</a>
											</p>
										<? endif; ?>
										<p>
											&raquo; 
											<a href="/journal-entry.php?action=Edit&travellogID=<?=$eachentry->getTravelLogID()?>">
												Edit Journal Entry
											</a>
										</p>
										<!--p>
											&raquo; 
											<a href="javascript:void(0)" onclick="window.open('/admin/pulldown.php?travelID=<?=$eachentry->getTravelID()?>&amp;travelLogID=<?=$eachentry->getTravelLogID()?>','Pulldown Entry','menubar=no,toolbar=no,location=NO,directories=NO,status,scrollbars=NO,resizable,width=300,height=200');return false;">
												Pulldown This Entry
											</a>
										</p-->
									</div>
									<div class = "travbox_info">
										<p class = "gray">
											<?=$d->set($eachentry->getTrip()->getArrival())->friendlyFormat()?>
										</p>
										<p class = "gray">
									
										<?php if(!is_null($eachentry->getTrip()->getLocation())): // added by jonathan.antivo ?>

											<? $cID = $eachentry->getTrip()->getLocation()->getCountry()->getCountryID();  
											   $city = $eachentry->getTrip()->getLocation()->getName();
											   $country = $eachentry->getTrip()->getLocation()->getCountry()->getName();  
											?>
											<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" />
											<?=( strtolower($city)==strtolower($country) ) ? $country : $city .', '. $country ?>
										
										<?php endif; ?>		

										</p>
										<p>
											<? $owner = $eachentry->getOwner(); ?>
											<? if ( !is_null($owner) ) : ?>
												
												<? if ($owner->isSuspended()) : ?>
													<img src="/images/alerts_bg.gif" title="Suspended">
												<? endif;?>	
												
												<?if($owner instanceof Traveler):?>
													<a class="username" href="/<?=$owner->getUserName() ?>" >
														<?=$owner->getUserName() ?>
													</a>
												<?else:?>	
													<a class="username" href="../group.php?gID=<?=$owner->getGroupID() ?>" >
														<?=$owner->getName() ?>
													</a>
												<?endif;?>
											<? endif ?>
										</p>
										<a class="journal_title" href="/journal-entry.php?action=view&amp;travelID=<?=$eachentry->getTravelID()?>&amp;travelerID=<?=$eachentry->getTravelerID()?>">
											<?=$eachentry->getTravel()->getTitle()?>
										</a>
										<?php $lastFeaturedDate = $eachentry->getLastFeaturedDate() ?>
										<br /><?=strlen($lastFeaturedDate) ? 'Date Featured: ' . $d->set($lastFeaturedDate)->friendlyFormat() : '' ?>
										<? if (ViewFeaturedDatesController::displayViewLink($eachentry->getTravelLogID(), 4)) : ?>
											<div style = "padding-top: 5px;">
												<a
													href	= "javascript: void(0)"
													class	= "edit"
													onClick = "window.open('viewFeaturedDates.php?sectionID=4&type=1&genID=<?=$eachentry->getTravelLogID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
													[view dates]
												</a>
											</div>
										<? endif; ?>
									</div>
								</li>
							<? endforeach; ?>
						</ul>
					<? else : ?>
						<h4 style = "text-align: center; text-transform: uppercase; font-weight: bold">Your search has returned 0 rows.</h4>
					<? endif; ?>

					<? if ($paging->getTotalPages() > 1) : ?>
						<div class = "pagination">
							<div class = "page_markers">
								<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec ?>
							</div>
							<?$paging->getFirst()?>
							&nbsp;|&nbsp;
							<?$paging->getPrevious()?>
							&nbsp;&nbsp;
							<?$paging->getLinks()?>
							&nbsp;&nbsp;
							<?$paging->getNext()?>
							&nbsp;|&nbsp;
							<?$paging->getLast();?>
						</div>		
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>

	<div id = "narrow_column">
		<div class = "section">			
			<h2 style = "text-align: center">
				<? if (count($currFeat)) : ?>
					<? if (1 < count($currFeat)) : ?>
					 	CURRENTLY FEATURED JOURNAL ENTRIES
					<? else : ?>
						CURRENTLY FEATURED JOURNAL ENTRY
					<? endif; ?>
				<? endif; ?>
				<div style = "font-weight: normal; font-size: 12px; padding-top: 5px; text-transform: none">
					<strong>Caption: </strong>
					<? if (strlen($section->getCaption())) : ?>	
						<?=$section->getCaption()?>
					<? else : ?>
						-default -
					<? endif ; ?>
					<br />
					<strong>Quantity: </strong>
					<?=$section->getQuantity()?>
					<br />
					<a
						href	= "javascript: void(0)"
						class	= "edit"
				 		onClick	= "window.open('caption.php?sectionID=4','PopUp','left=300,height=200,width=400,status=yes')">
						change
					</a>
				</div>
			</h2>
			<div class = "content">
				<ul class = "users">
					<? foreach ($currFeat as $eachFeat) : ?>
						<li>
							<div style="height:105px">
								<a class = "thumb" href = "/journal-entry.php?action=view&amp;travelerID=<?=$eachFeat->getTravelerID()?>&amp;travellogID=<?=$eachFeat->getTravelLogID()?>" >
									<img alt="Journal Entry Photo" src="<?=$eachFeat->getRandomPhoto()->getPhotoLink() ?>"  style = "width: 130px; height: 100px;"  />
								</a>
								<div class = "details" style = "width: 125px; margin-left: 65px">
									<h4>
										<a href = "/journal-entry.php?action=view&amp;travelerID=<?=$eachFeat->getTravelerID()?>&amp;travellogID=<?= $eachFeat->getTravelLogID()?>" >
											<?
												$mTitle = (15 < strlen($eachFeat->getTitle()))
															? substr($eachFeat->getTitle(), 0, 15) . '..'
															: $eachFeat->getTitle();
												echo $mTitle;
											?>
										</a>
									</h4>
									<hr width = "95%" align = "left">
									<p>
										&raquo; 
										<a href = "entries.php?feature=<?=$eachFeat->getTravelLogID()?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">Remove Feature</a>
									</p>
									<p>
										&raquo; 
										<a href = "/journal-entry.php?action=Edit&travellogID=<?=$eachFeat->getTravelLogID()?>">Edit Journal Entry</a>
									</p>
									<!--p>
										&raquo; 
										<a href = "javascript:void(0)" onclick="window.open('/admin/pulldown.php?travelID=<?=$eachFeat->getTravelID()?>&amp;travelLogID=<?=$eachFeat->getTravelLogID()?>','Pulldown Entry','menubar=no,toolbar=no,location=NO,directories=NO,status,scrollbars=NO,resizable,width=300,height=200');return false;">Pulldown This Entry</a>
									</p-->
								</div>
							</div>
							<div style = "font-size: 11px">
								<p class = "gray"><?= $d->set($eachFeat->getTrip()->getArrival())->friendlyFormat()?></p>
								<p class = "gray">
									<? $cID = $eachFeat->getTrip()->getLocation()->getCountry()->getCountryID()  ?>
									<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11">
									<? $city = $eachFeat->getTrip()->getLocation()->getName();
									   $country = $eachFeat->getTrip()->getLocation()->getCountry()->getName();?>
									<?=( strtolower($city)==strtolower($country) ) ? $country : $city .', '. $country ?>									
								</p>
								<?php $lastFeaturedDate = $eachFeat->getLastFeaturedDate() ?>
								<p>
									<? $owner = $eachFeat->getOwner(); ?>
									<? if ( !is_null($owner) ) : ?>
										
										<? if ($owner->isSuspended()) : ?>
											<img src="/images/alerts_bg.gif" title="Suspended">
										<? endif;?>	
										
										<?if($owner instanceof Traveler):?>
											<a class="username" href="/<?=$owner->getUserName() ?>" >
												<?=$owner->getUserName() ?>
											</a>
										<?else:?>	
											<a class="username" href="../group.php?gID=<?=$owner->getGroupID() ?>" >
												<?=$owner->getName() ?>
											</a>
										<?endif;?>
									<? endif ?>
								</p>
								<p class = "gray">
									<strong>Date Featured: </strong>
									<?= strlen($lastFeaturedDate) ? $d->set($lastFeaturedDate)->friendlyFormat() : '' ?>
								</p>								
								<? if (ViewFeaturedDatesController::displayViewLink($eachFeat->getTravelLogID(), 4)) : ?>
									&nbsp; &nbsp;
									<a
										href	= "javascript: void(0)"
										class	= "edit"
										onClick = "window.open('viewFeaturedDates.php?sectionID=4&type=1&genID=<?=$eachFeat->getTravelLogID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
										[view dates]
									</a>
								<? endif; ?>
							</div>
						</li>
					<? endforeach; ?>		
				</ul>
			</div>
			<div class = "foot"></div>
		</div>
	</div>
</div>