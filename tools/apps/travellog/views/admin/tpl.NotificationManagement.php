<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::includeDependentCss("/css/tabs.css");
	Template::includeDependentJs("/js/tools/tools.js");
	Template::includeDependentJs("/js/window/Class.WindowFrame.js");
	Template::includeDependentJs("/js/utils/Class.Request.js");
	Template::includeDependentJs("/js/notificationManager.js");
	ob_start();
?>		
	<script type="text/javascript">
		NotificationManager.init({
			itemsPerPage: <?= $itemsPerPage?>,			
			pageNavPerPage : <?= $pageNavPerPage?>
		});
	</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);
?>	
	<div id="wrapper1">
		<table cellpadding="10px">
			<tr>
				<td valign="top">
					<!--navigation panel-->										
					<p align="center">
						<a href="javascript:void(0)" onclick="NotificationManager.createEntry()">Compose</a><br />						
					</p>										
					<table style="width:200px">
						<tr>
							<td>
								<div id="pageNavPanel">Page navigation</div>
							</td>
						</tr>
					</table>	
					<div id="navPanel"></div>						
				</td>
				<td valign="top">
					<div id="tabPanel">
						<table cellpadding="6px">
							<tr>
								<td align="center" class="" id="detailTab"><strong><a href="javascript:void(0)" onclick="NotificationManager.displayDetailPanel()">Details</a></trong></td>								
								<td align="center" class="" id="composeTab"><strong><a href="javascript:void(0)" onclick="NotificationManager.displayComposePanel()">Compose</a></strong></td>								
							</tr>
						</table>
					</div>
					<!-- detail panel -->
					<div id="detailPanel" class="widePanel" style="display:none">													
						<font id="detailTitle" style="font-size:14px"></font>			
						<br />
						<a href="javascript: void(0)" id="editEntryLink" onclick="NotificationManager.editEntry(0)">[Edit]</a>&nbsp;
						<a href="javascript: void(0)" id="deleteEntryLink" onclick="NotificationManager.deleteEntry(0)">[Delete]</a>&nbsp;
						<a href="javascript: void(0)" id="showCodeLink" onclick="NotificationManager.showCode(0)">[Show Code]</a>&nbsp;
						<br /><br />
						<strong>Notification Tag: </strong>
						<span id="detailNotificationTag"></span><br /><br />
						<strong>Subject: </strong>
						<span id="detailSubject"></span><br /><br />
						<strong>Is HTML: </strong>
						<span id="detailIsHtml"></span><br /><br />
						<strong>Message: </strong><br />
						<div id="detailMessage"></div>
					</div>
					<!-- compose panel -->
				
					<div id="composePanel" class="widePanel" style="display:none">
						<h3 id="transMethod">Compose Notification</h3>
						<table cellpadding="3px">
							<tr>
								<td valign="top"><strong>Preserved Variables</strong></td>
								<td>
									<span>																				
										Subject Traveler Info:<br />
										<table>
											<?php foreach($arTravelerInfoVars as $key => $val): ?>
												<tr>
													<td><?=$key?></td>
													<td><?=$val?></td>
												</tr>
											<?endforeach;?>											
										</table>
										<br />
										Addressbook Entry Info: (These are dynamic variables. It is parallel to the current AddressBook Entry.)
										<table>
											<?php foreach($arEntryInfoVars as $key => $val): ?>
												<tr>
													<td><?=$key?></td>
													<td><?=$val?></td>
												</tr>
											<?endforeach;?>											
										</table>
										<br />
										Note: Preserved Variables are not available in the Notification Tag field.<br />
									</span>
								</td>								
							</tr>
							<tr>
								<td valign="top"><strong>Custom Variables</strong></td>
								<td>
									<span>																				
										Custom Variables must be in this format [$VARNAME$] (case-sensitive).<br />
										Note: Custom Variables are not available in the Notification Tag field.<br />										
									</span>
								</td>								
							</tr>							
							<tr>
								<td><strong>Notification Tag: </strong></td>
								<td><span class="errCaption" id="composeNotificationTagErr"></span><input type="text" id="composeNotificationTag" style="width:400px" /></td>
							</tr>
							<tr>
								<td><strong>Subject: </strong></td>
								<td><span class="errCaption" id="composeSubjectErr"></span><input type="text" id="composeSubject" style="width:400px" /></td>
							</tr>
							<tr>
								<td><strong>Is HTML: </strong></td>
								<td>
									<span class="errCaption" id="composeIsHtmlErr"></span>									
									<input type="radio" name="radIsHtml" value="0" id="radIsHtmlNo" checked="true">No &nbsp;&nbsp;
									<input type="radio" name="radIsHtml" value="1" id="radIsHtmlYes">Yes 
								</td>
							</tr>
							<tr>
								<td valign="top"><strong>Content: </strong></td>
								<td><span class="errCaption" id="composeMessageErr"></span><textarea  id="composeMessage" style="width:450px;height:300px"></textarea></td>
							</tr>							
							<tr>
								<td colspan="2" align="right">
									<input type="button" value="Cancel" onclick="NotificationManager.cancelEntry()" />&nbsp;<input type="button" value="Save" onclick="NotificationManager.saveEntry()" />
									<input type="hidden" value="0" id="composeID" />
								</td>
							</tr>
						</table>
					</div>
				
					<div id="statusPanel" class="widePanel" style="display:none">
						<table cellpadding="3px" width="100%" height="100%">
							<tr>
								<td width="100%" height="100%" align="center">
									<span id="imgLoading">
										<img src="http://dev.goabroad.net/processing3.gif" />
									</span>
									<br />
									<strong id="statusCaption">Loading data please wait...</strong>										
								</td>									
							</tr>
						</table>																	
					</div>
					<!--<div id="logPanel" class="widePanel"></div>-->						
				</td>
			</tr>
		</table>
	</div>	