<?php
/*
 * Created on 11 28, 06
 *
 * @author Kerwin Gordo
 * Purpose: template to add GA.net HelpText 
 */
 
 Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
 
?>
<script type="text/javascript" src="/js/jquery-1.1.4.pack.js"></script>	
  <script type="text/javascript">
  	   jQuery.noConflict();
  	   jQuery(document).ready(function(){
  	   		jQuery("#other_site").focus(function(){
  	   			jQuery("#ganet_site").attr("checked",0);
  	   			jQuery("#cobrand_site").attr("checked",0);        
  	   		})
  	   		
  	   		jQuery("input:checkbox").click(function(){
  	   			if (jQuery(this).attr("checked")){
  	   				jQuery(this).siblings().attr("checked",0);				
  	   			}
  	   				
  	   		});
  	   		
  	   		
  	   		jQuery("#a_spec").click(function(){
  	   			jQuery("#a_spec").hide();
  	   			jQuery("#cb_site_input  input").attr("checked",0);
  	   			jQuery("#cb_site_input").hide();
  	   			jQuery("#text_site_input").show();
  	   				
  	   		})
  	   		
  	   		
  	   			
  	   });
  </script>


<h2>Add Help Text</h2>	

  <br />
  <a href="helptext2.php">View/Edit/Delete Helptext</a> 
  <br />
    
  <?if (isset($message)):?>
  	<br />
  	<ul><li><?=$message?></li></ul>	
  	<br />
  <?endif;?> 	
  
  <form name="addform" method="post" action="helptext2.php?action=save" >
  <h3>Help Key</h3>
	  <input type="text" name="hkey">  <br /> <br />
	  <h3>Site</h3>
	  
	  <div id="cb_site_input" >
	  <input type="checkbox" name="sitename" value="GANET" id="ganet_site" checked> GANET <br/>
	  <input type="checkbox" name="sitename" value="COBRAND" id="cobrand_site"> COBRAND <br/>
	  </div>
	  <a href="#" id="a_spec">Specific site</a>
	  
	  <div id="text_site_input" style="display:none">
	  <input type="text" name="other_site" id="other_site"> Specific site name
	  </div>
	  <div>
	  	  <h3>Help Text Description</h3>
	  	  <textarea id="helpdesc" name="helpdesc" rows="3" cols="80" wrap="on"></textarea>			 
	  </div>
	  
	  <br /><br />
	  
  	  <div>  	
  	    <h3>Help Text</h3>		
	    <textarea name="helptext" rows="10" cols="80" wrap="on"></textarea>  
  	  </div>
	  
	  
  	  <input type="submit" name="name" value="Save" title="Add Help Text and Description" />    
  </form>

