<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>

<div class="area" id="intro">
	<div class="section" align="center">
		<h1>ERROR LOGS</h1>		
	</div>
</div>

<div id = "top" class="area" style = "float:none;">
	<div style = "margin-left:auto;margin-right:auto;width: 980px;">
		<div class="wrapper">
			<a href='errorlogs.php'>View All Error Logs</a>
			<hr/>
			<div class="section">
				<div class="content">
					<?=$search?>
				</div>
			</div>
			<hr/>
			<div class="pagination">
				<? if(isset($paging)):?>
					<? if ($paging->getTotalPages() > 1 ): ?>		
					<div class="page_markers">
						Showing <?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $count?> Error Logs.
					</div>	
					<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
					<? else:?>
					<div class="page_markers">
						Showing <?=$count?> Error Log<?if($count > 1):?>s<?endif;?>.
					</div>
					<? endif; ?>
				<? endif; ?>		
				</div>
				<div class="section">
					<div class="content">
						<?$li_even_odd=NULL;?>
						<ul>
						<?php if(0 < count($logs)):?>
						<?php foreach($logs as $log):?>
							<?$li_even_odd = ( '#eeeeee' != $li_even_odd ) ? '#eeeeee' : '#fff'; ?>
							<li style="background-color:<?=$li_even_odd?>;overflow: auto;">
								<b>ErrorID:</b> <?=$log->getID()?><br />
								<b>TYPE:</b> <?=$log->getErrorType($log->getType())?><br />
								<b>MESSAGE:</b> <?=$log->getMessage()?><br />
								Error occurred at line <?=$log->getLine()?> 
								in <b><?=$log->getFile()?></b><br />
								<b>DATE:</b> <?=GaDateTime::formatDate("M d, Y g:s A",$log->getDateAdded())?><br />
								<b>VARIABLES:</b> <br />
								<pre><?=$log->getVars()?></pre><br />
								<b>STACKTRACE:</b> <br />
								<pre><?=$log->getStackTrace()?></pre><br /
							</li>
						<?php endforeach;?>
						<?php endif;?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>						