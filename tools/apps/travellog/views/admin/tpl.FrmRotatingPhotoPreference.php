<form action="rotatingPhoto.php?action=editPreference" method="POST">
	<strong>Max. Photos Per Page:</strong> <input type="text" name="txtMaxPhotoPerPage" value="<?=$photos_per_page?>" size="5" onkeypress="return RotatingPhotoJS.numbersOnly(event);"/>
	<br/>
	<strong>View:</strong> <? if($filter["filter_by"] === "status"):?>
	Status
	<input type="hidden" name="hidFilterBy" value="<?=$filter["filter_by"]?>">
	<select name="cmbFilter">
		<option <?if($filter["filter"]=="all"):?>selected<?endif;?> value="all">all</option>
		<option <?if($filter["filter"]=="0"):?>selected<?endif;?> value="0">active</option>
		<option <?if($filter["filter"]=="1"):?>selected<?endif;?> value="1">inactive</option>
	</select>
	<? endif;?>
	<br/>
	<strong>Sort By:</strong> <?=$order["order_by"]?>
	<input type="hidden" name="hidOrderBy" value="<?=$order["order_by"]?>">
	<select name="cmbOrder">
		<option <?if($order["order"]==="ASC"):?>selected<?endif;?> value="ASC">ascending</option>
		<option <?if($order["order"]==="DESC"):?>selected<?endif;?> value="DESC">descending</option>
	</select>
	<br/> 
	<input type="submit" value="View" name="submit"/>
</form>