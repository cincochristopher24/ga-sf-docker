<?php
	include_once('travellog/views/admin/tpl.AdminHeader.php');
	$d = new GaDateTime();
?>

<div id="content_wrapper" class='layout_2'>
	<div id="message_center" class="section">
		<h2>
			<span>
				Shout Outs From Unregistered Users
			</span>
		</h2>
		
		<div id="comments" class="content">
		
			<form action="/admin/viewShoutOut.php" method="get">
				<div>
					<div style="width: 70px; float: left; padding-top: 7px;"><strong>By Group:</strong></div>
					<input type="text" name="group" value="<?php echo $group; ?>" />
				</div>

				<div>
					<div style="width: 70px; float: left; padding-top: 7px;"><strong>By Name:</strong></div>
					<input type="text" name="name" value="<?php echo $name; ?>" />
				</div>
				
				<div>
					<div style="width: 70px; float: left; padding-top: 7px;"><strong>Sort By:</strong></div>
					<select name="order_by">
						<option value="1" <?php if ($orderBy == 1): ?>selected="selected"<?php endif; ?>>Most recent</option>
						<option value="2" <?php if ($orderBy == 2): ?>selected="selected"<?php endif; ?>>Name</option>
					</select>
				<div>
				
				<input type="submit" value="Submit"/>
			</form>
			
			<?php if ($shoutOutList): ?>
			<ul id = "display_comments_content">
				<?php
					$display = 0;
					foreach($shoutOutList as $comment):
						try{
							$author = $comment->getAuthorObject();
							$context = $comment->getContext();
							$Poke = new PokeType($comment->getPokeType());
							$pokesview = new PokesView();
							$pokesview->setVisitorId(-1);		// travelerId of logged-in user
							$pokesview->setOwnerId($comment->getContextOwnerID());
							$pokesview->setPokeTypeId($Poke->getPokeTypeID());
							$pokesview->setContextId($comment->getContextId());
							if($context instanceof TravelerProfile)
								$pokesview->setContext(CommentType::$PROFILE);
							if($context instanceof Photo)
								$pokesview->setContext(CommentType::$PHOTO);
							if($context instanceof TravelLog)
								$pokesview->setContext(CommentType::$TRAVELLOG);
							if($context instanceof Article)
								$pokesview->setContext(CommentType::$ARTICLE);
							$pokesview->setAuthorObject($author);
							$display++;
						}
						catch(exception $e){
							continue;
						}
				?>
				
				<li>
					<?php $comment->getContextOwnerID()?>
					<?php if($context instanceof TravelerProfile ):?>
						<a href="/<?php echo $comment->getContext()->getUserName()?>"><?php echo $comment->getContext()->getUserName()?></a> 
					<?php elseif($context instanceof TravelLog):?>
						<a href="/journal-entry.php?action=view&amp;travellogID=<?php echo $comment->getContext()->getTravelLogID()?>"><?php echo $comment->getContext()->getTitle()?></a>
					<?php elseif($context instanceof Article):?>
						<a href="/article.php?action=view&articleID=<?php echo $comment->getContext()->getArticleID()?>"><?php echo $comment->getContext()->getTitle()?></a>	
					<?php elseif($context instanceof Photo):?>
						Photo
					<?php endif;?>
					<div>
						<a href="#"  class="thumb">
							<img src="<?php echo $author->getPhotoSource()?>" height="37" width="37" alt="User Profile Photo" />
						</a>
						
						<div class="comments_content">
							<div class="comment_info"> 
								<strong>
									<?php echo $pokesview->getMessage()?>
								</strong> 
								<span>
									<?php echo $d->set($comment->getCreated())->friendlyFormat() ?>
									<?php if ($comment->isApproved()): ?>
										<em>(approved)</em>
									<?php else: ?>
										<em style="color: #f00;">(unapproved)</em>
									<?php endif; ?>
								</span>
							</div>
							
							<?php if (0 == $comment->getPokeType()) : ?>
							<div>
								<?php echo HtmlHelpers::Textile($comment->getTheText()) ?>
							</div>
							<?php else : ?>
								<?php $class_name = preg_replace('/.png$/','',$Poke->getPokeImage())?>
							<div class="shout do_<?php echo $class_name?>">do <?php echo $class_name ?></div>
							<div>
								<?php echo HtmlHelpers::Textile($comment->getTheText()) ?>
							</div>
							<?php endif; ?>
						</div>
						<div>
							Email Address: <strong><?php echo $comment->getEmail(); ?></strong>
						</div>
					</div>
				</li>
				<?php endforeach;?>
			</ul>
			<?php else: ?>
				No record found!
			<?php endif; ?>
		</div>
		
		<div class="pagination">
		<?php if(isset($paging)):?>
			<?php if ($paging->getTotalPages() > 1 ): ?>
			<div class="page_markers">
				Showing <?php echo $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $count?> Shout Outs.
			</div>
			<?php $paging->getFirst(); ?>&nbsp;|&nbsp;<?php $paging->getPrevious(); ?>&nbsp;&nbsp;<?php $paging->getLinks(); ?>&nbsp;&nbsp;<?php $paging->getNext(); ?>&nbsp;|&nbsp;<?php $paging->getLast(); ?>
			<?php endif; ?>
		<?php endif; ?>
		</div>
			
		<div class="content">
			<p class="help_text"></p>
		</div>
	</div>
</div>