<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
?>

<div id = "intro_container">
	<div id = "intro" align = "center">
		<h1>GROUP ADVISORS</h1>
		<div style="overflow:hidden;margin-top:20px;width:980px;">
			<div style="width: 780px;text-align:left;float:left;">
				<form name="frmSearch" action="/admin/groupAdvisors.php" method="post">
					<strong>Search by group name: </strong>
					<input type="textbox" name="txtGroupName" id="txtGroupName" value="<?=$groupName?>" size="40" />
					&nbsp;
					<input type="submit" name="btnSearch" id="btnSearch" value="Search" />
				</form>
			</div>
			<div style="float:left">
				<? if ( $showViewAll ) : ?>
					<strong><a href="/admin/groupAdvisors.php">View All Advisors</a></strong>
				<? endif; ?>
			</div>
		</div>
	</div>
</div>

<? if ( 0 < count($advisors) ) : ?>
	<div id = "content_wrapper" class = "layout_2">
		<div class = "section">
			<div class = "content">

				<table width="100%" cellspacing="5" cellpadding="5" align="center" style="border:1px solid #CCCCCC;background-color:#BBBBBB;color:#000000;">
					<tr style="font-weight:bold;text-align:center;">
						<th width="9%" style="font-weight:bold;text-align:center;background-color:#EEEEEE;border-color:#FFFFFF #EEEEEE #EEEEEE #FFFFFF;border-right:1px solid #CCCCCC">
							Traveler ID
						</th>
						<th width="15%" style="font-weight:bold;text-align:center;background-color:#EEEEEE;border-color:#FFFFFF #EEEEEE #EEEEEE #FFFFFF;border-right:1px solid #CCCCCC">
							Username
						</th>
						<th width="18%" style="font-weight:bold;text-align:center;background-color:#EEEEEE;border-color:#FFFFFF #EEEEEE #EEEEEE #FFFFFF;border-right:1px solid #CCCCCC">
							Full Name
						</th>
						<th width="28%" style="font-weight:bold;text-align:center;background-color:#EEEEEE;border-color:#FFFFFF #EEEEEE #EEEEEE #FFFFFF;border-right:1px solid #CCCCCC">
							Email Address
						</th>
						<th width="30%" style="font-weight:bold;text-align:center;background-color:#EEEEEE;border-color:#FFFFFF #EEEEEE #EEEEEE #FFFFFF;">
							Group Name
						</th>
					</tr>
					<? foreach ( $advisors as $each ) : ?>
						<tr style="padding-top:5px;">
							<td style="text-align:center;border-right:1px solid #CCCCCC;border-top:1px solid #CCCCCC;background-color:#FFFFFF;vertical-align:middle;">
								<?=$each['travelerID']?>
							</td>
							<td style="text-align:center;border-right:1px solid #CCCCCC;border-top:1px solid #CCCCCC;background-color:#FFFFFF;vertical-align:middle;">
								<a href="/<?=$each['username']?>"><?=$each['username']?></a>
								<br />
								<p>
									<a class="edit" onclick="window.open('/changeUserName.php?username=<?=$each['username']?>&amp;ID=<?=$each['travelerID']?>','changeUserName','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars=no,resizable,width=490,height=190, top=' + ((screen.width/2)- 330) + ', left=380'); return false" href="javascript: void(0)" style="color:red">
										[change]
									</a>
								</p>
							</td>
							<td style="text-align:center;border-right:1px solid #CCCCCC;border-top:1px solid #CCCCCC;background-color:#FFFFFF;vertical-align:middle;">
								<?=$each['fullname']?>
							</td>
							<td style="text-align:center;border-right:1px solid #CCCCCC;border-top:1px solid #CCCCCC;background-color:#FFFFFF;vertical-align:middle;">
								<?=$each['email']?>
							</td>
							<td style="text-align:center;border-top:1px solid #CCCCCC;background-color:#FFFFFF;vertical-align:middle;">
								<a href="/group.php?gID=<?=$each['groupID']?>"><?=$each['groupname']?></a>
							</td>
						</tr>
					<? endforeach; ?>
				</table>

	   			<div class="pagination">
					<? if ($paging->getTotalPages() > 1 ): ?>		
						<div class="page_markers">
							<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $recordCount ?>
						</div>
						<? $paging->getFirst(); ?>
						&nbsp;|&nbsp;
						<? $paging->getPrevious(); ?>
						&nbsp;&nbsp;
						<? $paging->getLinks(); ?>
						&nbsp;&nbsp;
						<? $paging->getNext(); ?>
						&nbsp;|&nbsp;
						<? $paging->getLast(); ?>
					<? endif; ?>	
				</div>

			</div>
		</div>
	</div>
<? else : ?>
	<div style="font-weight:bold;color:red;text-align:center;margin-top:30px">No record found</div>
<? endif; ?>