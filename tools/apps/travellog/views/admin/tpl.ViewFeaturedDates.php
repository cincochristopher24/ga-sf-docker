<?php
	/********************************************************/
	/*	@author:		Cheryl Ivy Q. Go					*/
	/*	Date Created:	30 September 2008					*/
	/*	Purpose:		template for viewFeaturedDates.php	*/
	/********************************************************/

	require_once 'travellog/model/admin/Class.FeaturedSection.php';
	$d = new GaDateTime();

	if ($sectionID == FeaturedSection::$JOURNAL_ENTRY)
		$mSection = 'Journal Entry';
	else if ($sectionID == FeaturedSection::$PHOTO)
		$mSection = 'Photo';
	else if ($sectionID == FeaturedSection::$TRAVELER)
		$mSection = 'Traveler';
	else
		$mSection = 'Group';	
?>

<html>
	<head>
		<title>Featured Dates</title>
		<style type = "text/css">
			@import url("/css/base.css");
			@import url("/css/main_pages.css");

			#display_dates .content .elements {
				color: #999999;
				overflow: auto;
				padding: 0;
			}
		</style>
	</head>

	<body id = "main_pages">
		<div id = "body">
			<div id = "intro_container">
				<div align = "center">
					<h2>
						<? if (0 < count($featuredDates)) : ?>
							<? $pl = (1 < count($featuredDates)) ? 's' : '' ?>
							<?='Date' . $pl . ' When ' . $mSection . ' Was Featured'?>
						<? else: ?>
							<div style = "height: 40px;"></div>
							<?=$mSection . ' was not featured yet!'?>
							<div style = "height: 30px;"></div>
						<? endif; ?>
					</h2>
				</div>
			</div>
			<div id = "content_wrapper" style = "width: 400px; min-height: 250px;">
				<div class = "section" align = "center" style = "border-top:1px solid #DFDFDF">
					<div class = "content" style = "padding-top: 20px; max-height: 210px;">
						<div id = "display_dates">
							<? if (0 < count($featuredDates)) : ?>
								<ul class = "elements" style = "max-height: 205px;">								
									<? foreach ($featuredDates as $eachDate) : ?>
										<li>
											<?=$d->set($eachDate)->friendlyFormat()?>
											<div class="clear"></div>
										</li>
									<? endforeach; ?>
								</ul>
							<? endif; ?>
						</div>
					</div>
					<div class = "foot"/>
					<div class = "clear"/>
				</div>
			</div>
		</div>
	</body>
</html>