<?php
/**
 * Created on May 10, 2007
 *
 * @author daf 
 */
 
 	Template::setMainTemplate("travellog/views/tpl.LayoutMain.php");
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Admin');	
 
?>

	<? if (0 < count($errorcodes)) { ?>
		<span class="errors"><?=$errormsg?></span>											
	<? } ?>
	
	<? $urlparamstr = '' ?>
	
	<? if (isset($adID)) 
		$urlparamstr = "&amp;adID=" . $adID ;		
	?>
	
	<form name="addAds" action="ads.php?mode=save<?=$urlparamstr?>" method="post">
		<br />
		BANNER ID
		<br /> 
		<? if (in_array('txtBannerID',$errorcodes)) { ?>
		 	<span class="error">ERROR</span>
		<? } ?>
		<input type="text" name="txtBannerID" value="<?=$txtBannerID?>">
		<br>
		AD TEXT: Note: Please enclose hyperlink text in curly braces {} .
		<br />
		<? if (in_array('txaAdText',$errorcodes)) { ?>
		 	<span class="error">ERROR</span>
		<? } ?>
		<textarea name="txaAdText" rows="6" cols="20"><?=$txaAdText?></textarea>
		<br>
		<input type="submit" name="next" value="Save">
		
	</form>
