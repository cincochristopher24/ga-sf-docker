<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>

<div class="area" id="intro">
	<div class="section" align="center">
		<h1>ROTATING PHOTOS</h1>		
	</div>
</div>

<div id = "top" class="area" style = "float:none;">
	<div style = "margin-left:auto;margin-right:auto;width: 980px;">
		<div class="wrapper">
			<a href="rotatingPhoto.php?action=displayUploadForm">Upload Rotating Photos</a>
			<div class="section">
				<div class="content">
					<strong>Active Rotating Photos:</strong> <?=$active?><br/>
					<strong>Inactive Rotating Photos:</strong> <?=$inactive?>
				</div>
			</div>
			<hr/>
			<div class="section">
				<div class="content">
					<?=$editPreferenceForm?>
				</div>
			</div>
			<hr/>
			<div class="pagination">
			<? if(isset($paging)):?>
				<? if ($paging->getTotalPages() > 1 ): ?>		
				<div class="page_markers">
					Showing <?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $count?> Rotating Photos.
				</div>	
				<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
				<? else:?>
				<div class="page_markers">
					Showing <?=$count?> Rotating Photos.
				</div>
				<? endif; ?>
			<? endif; ?>		
			</div>
			<div class="section">
				<div class="content">
					<ul>
					<? for($i=0;$i<count($photos);$i++):?>
					<? $photo = $photos[$i];?>
						<hr/>
						<li>
							<img src="http://<?=$_SERVER['SERVER_NAME']?>/admin/showRotatingPhoto.php?image=../images/rotating/<?=$photo->getPhotoName()?>"/>
							<p>
								<strong>Photo ID:</strong> <?=$photo->getPhotoID()?>
								&nbsp;&nbsp;&nbsp;
								<strong>Filename:</strong> <?=$photo->getFilename()?>
								&nbsp;&nbsp;&nbsp;
								<strong>New Filename:</strong> <?=$photo->getPhotoName()?>
							</p>
							<input type="hidden" id="hidPhotoID-<?=$i?>" name="hidPhotoID-<?=$i?>" value="<?=$photo->getPhotoID()?>"/>
							<p>
								<strong>Caption:</strong> <?=str_replace("\"","&quot;",stripslashes($photo->getCaption()))?>
							</p>
							<p>
								<strong>Status:</strong> <?if($photo_active_status==$photo->getStatus()):?>Active<?else:?>Inactive<?endif;?>
							</p>
							<p>
								<strong>Date Uploaded:</strong> <?=$photo->getDateUploaded()?>
							</p>
							<p>
							<strong><?if($photo->getTypeID()==$traveler_type_id):?>Traveler<?else:?>Group<?endif;?>ID:</strong> <?=$photo->getUserID()?>
							<?if(!$photo->isExistingUser()):?> (User Does Not Exist!)<?elseif($photo->isSuspendedUser()):?> (User Is Suspended!)<?endif;?>
							<?if($photo->getUserID()):?><strong>Name:</strong> <?=str_replace("\"","&quot;",stripslashes($photo->getFrom()))?><?endif;?>
							</p>
							<input type="button" value="edit" onclick="location.href='rotatingPhoto.php?action=editPhoto&id=<?=$photo->getPhotoID()?>'"/>
							<input type="button" value="replace image" onclick="document.getElementById('replace_image-<?=$i?>').style.display='block';"/>
							<div id="replace_image-<?=$i?>" style="display:none;">
							<form action="rotatingPhoto.php?action=replacePhoto" method="POST" enctype="multipart/form-data">
								<input type="hidden" id="hidPhotoName" name="hidPhotoName" value="<?=$photo->getPhotoName()?>"/>
								<input type="hidden" id="hidPhotoID" name="hidPhotoID" value="<?=$photo->getPhotoID()?>"/>
								<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="2000000" />
								<input id="hidPhotoID-<?=$i?>" name="photo" type="file" />
								<input type="submit" value="REPLACE"/>
							</form>
							</div>
						</li>
					<?endfor;?>
						<hr/>
					</ul>
				</div>
			</div>
			<a href="rotatingPhoto.php?action=displayUploadForm">Upload Rotating Photos</a>
			<div class="pagination">
			<? if(isset($paging)):?>
				<? if ($paging->getTotalPages() > 1 ): ?>		
				<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
				<? endif; ?>
			<? endif; ?>		
			</div>
		</div>
	</div>
</div>