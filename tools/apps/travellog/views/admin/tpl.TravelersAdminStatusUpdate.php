<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
	require_once 'travellog/controller/admin/Class.ViewFeaturedDatesController.php';
?>

<div id = "intro_container">
	<div id = "intro" align = "center">
		<h1>UPDATE TRAVELERS STATUS</h1>
	</div>
</div>

<div>
	<div>
		<h2>
			<span><?=$title?></span>
		</h2>
		<div>
			<div>
				<div style = "padding-top: 10px;">
					<?= $search_template ?>
					<span style = "float: right">
						<?= $travelers_link ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?= $newest_traveler_link ?>
					</span>
				</div>
				<div style = "padding-top: 20px;">
					<? if ($paging->getTotalPages() > 1) : ?>
						<div class = "pagination">
							<div class = "page_markers">
								<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . count($obj_travelers) ?>
							</div>	
							<?$paging->getFirst()?>
							&nbsp;|&nbsp;
							<?$paging->getPrevious()?>
							&nbsp;&nbsp;
							<?$paging->getLinks()?>
							&nbsp;&nbsp;
							<?$paging->getNext()?>
							&nbsp;|&nbsp;
							<?$paging->getLast()?>
						</div>
					<? endif; ?>

					<?
						if (count($obj_travelers)) :
					?>						
						<ul class="users">
							<?
								foreach($obj_travelers as $eachtraveler) :
									$photolink = $eachtraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink();

									if (0 != strncmp("http://", $photolink , "7"))
										$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
							?>
									<li style="width:400px;height:100px;float:left;">
										<a class = "thumb" href="/<?=$eachtraveler->getUsername()?>">
											<img src="<?=$photolink?>" alt="<?=$eachtraveler->getUsername()?>" style = "height: 95px; width: 85px" />
										</a>
										<div class = "details" style = "padding-left: 20px;">
											<strong>
												<a href="/<?=$eachtraveler->getUsername()?>"><?=$eachtraveler->getUsername()?></a>
											</strong>
											<p class = "gray">
												<?=$eachtraveler->getCountTravelJournals()?>
												<?=(1 < $eachtraveler->getCountTravelJournals()) ? ' Journals' : ' Journal'?> | 
												<?=$eachtraveler->getCountTravelLogs()?>
												<?=(1 < $eachtraveler->getCountTravelLogs()) ? ' Entries' : ' Entry'?>
											</p>
											<? if ("0000-00-00 00:00:00" != $eachtraveler->getTravelerProfile()->getDateJoined()): ?>
												<p class = "gray">
													Member Since: <?= $d->set($eachtraveler->getTravelerProfile()->getDateJoined())->friendlyFormat()?>
												</p>
											<? endif; ?>
											<p class = "gray">
												Last Login: <?= $d->set($eachtraveler->getTravelerProfile()->getLastLogin())->friendlyFormat()?>
											</p>
											<?/*php $lastFeaturedDate = $eachtraveler->getLastFeaturedDate() ?>
											<? if (strlen($lastFeaturedDate)) : ?>
												<p class = "gray">
													Date Last Featured: <?=$d->set($lastFeaturedDate)->friendlyFormat()?>
												</p>
											<? endif; ?>
											<? if (ViewFeaturedDatesController::displayViewLink($eachtraveler->getTravelerID(), 2)) : ?>
												<p class = "gray">
													<a
														href	= "javascript: void(0)"
														class	= "edit"
														onClick = "window.open('viewFeaturedDates.php?sectionID=2&type=1&genID=<?=$eachtraveler->getTravelerID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
														[view dates]
													</a>
												</p>
											<? endif; */?>
											<? if (!$eachtraveler->hasGoodStatus()) : ?>									
												<p class = "gray>">
													&raquo;<a id="update_<?=$eachtraveler->getTravelerID()?>_1" href="javascript:void(0);" onclick="updateStatus(<?=$eachtraveler->getTravelerID()?>);">Add Good Status</a>
												</p>
											<? else : ?>								
												<p class = "gray">
													&raquo;<a id="update_<?=$eachtraveler->getTravelerID()?>_0" href="javascript:void(0);" onclick="updateStatus(<?=$eachtraveler->getTravelerID()?>);">Clear Good Status</a>
												</p>
											<? endif; ?>
										</div>
										
									</li>
							<? endforeach; ?>		
						</ul>
					<? else : ?>
						<h4 style = "text-align: center; text-transform: uppercase; font-weight: bold">Your search has returned 0 rows.</h4>
					<? endif; ?>

					<? if ($paging->getTotalPages() > 1) : ?>
						<div class = "pagination">
							<div class = "page_markers">
								<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . count($obj_travelers) ?>
							</div>
							<?$paging->getFirst()?>
							&nbsp;|&nbsp;
							<?$paging->getPrevious()?>
							&nbsp;&nbsp;
							<?$paging->getLinks()?>
							&nbsp;&nbsp;
							<?$paging->getNext()?>
							&nbsp;|&nbsp;
							<?$paging->getLast();?>
						</div>		
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>
	
</div><div style="clear:both;"></div>
<script type="text/javascript">
	function updateStatus(id){
		if(confirm("Do you want to change the status of this traveler?")){
			var elt = jQuery("a[id^='update_"+id+"_']");
			var status = elt.attr('id').split('_')[2];
			elt.html('<img width="15" height="15" src="/images/load_gray.gif" alt="Loading..." />');
			jQuery.ajax({
				type: "GET",
				url: "travelers.php",
				data: "travID="+id+"&travelerStatus="+status,
				success: function(rsp){
					if(rsp == ""){
						if(status == 1){
							elt.html("Clear Good Status");
							elt.attr('id','update_'+id+'_0');
						}else{
							elt.html("Add Good Status");
							elt.attr('id','update_'+id+'_1');
						}	
					}
				}
			});
		}
	}
</script>