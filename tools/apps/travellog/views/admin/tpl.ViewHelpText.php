<?php
/*
 * Created on 11 27, 06
 *
 */
 
 Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
 			  
 	
?>

  <script type="text/javascript">
		function updateHelp(){
			hform = document.getElementById("helpform");
			hform.action = 'helptext.php?action=update&hkey=' + hkey + '&index=' + index;			
			hform.submit();
		} 	
		
		function deleteHelp() {
			if (!confirm('Are you sure you want to delete this Help Text?'))
				return;
			
			hform = document.getElementById("helpform");
			hform.action = 'helptext.php?action=delete&hkey=' + hkey + '&index=' + index;			
			hform.submit();
		} 	  	
  </script> 	

  <h2>Help Text Management</h2>		

  <br />
  <a href="helptext.php?action=add">Add New Helptext</a> 
  <br />  
  
  <?if (isset($message)):?>
  	<br />
  	<ul><li><?=$message?></li></ul>	
  	<br />
  <?endif;?> 

  <h3>Help Keys</h3>
  <form name="keysform"  method="post" action="helptext.php" >	
	  <select name="index" onchange="this.form.submit()">
	  	<?$ctr=0;?>
	  	<?foreach($rsar as $row) :?>
	     
	    	<option value="<?=$ctr?>" <?if ($ctr==$index) echo ' selected ' ?>    ><?=$row['hkey']?></option>
	    	<? if ($ctr == $index) : ?>
		    	<script type="text/javascript">		    
		    		hkey = '<?=$row['hkey']?>';
		    		index = <?=$index?>
		    	</script> 
	    	<? endif; ?>
	    	<?$ctr++?>
	    <?endforeach;?>
	  </select>
  </form>
  
  <br /><br />
  <form name="helpform" id="helpform" method="post" >
	  <div>
	  	  <h3>Help Text Description</h3>
	  	  <textarea id="helpdesc" name="helpdesc" rows="3" cols="80" wrap="on"><?=$description?></textarea>			 
	  </div>
	  
	  <br /><br />
	  <?if (isset($helptext)) :?>
	  	<div>  	
	  	  <h3>Help Text</h3>		
		  <textarea name="helptext" rows="10" cols="80" wrap="on"><?=$helptext?></textarea>  
	  	</div>
	  <?endif; ?>  	    	      
  </form>
  <input type="submit" name="name" value="Update" title="Update edits on Help Text and Description" onclick="updateHelp()"/>
  <input type="submit" name="name" value="Delete" title="Delete this Help Text" onclick="deleteHelp()"/>
  
  
