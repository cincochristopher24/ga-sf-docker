<fieldset class="fieldset1">
	<form name="f1" action="groups.php" method="get">	
		<span style = "font-weight: bold">Group Type: </span>
		<select name="cmbGrpType" class="fieldboxes" id="cmbGrpType" onChange="document.f1.submit()" style = "width: 60%">
			<?= $grpCategories ?>
			<option value = "viewlist" <? if ($grpType == 'viewlist') { ?> selected <? } ?>  >All Categories</option>
		</select>
	</form>
	<form name="f2" action="groups.php" method="get">
		<span style = "font-weight: bold">Group Filter: </span>
		<select name="cmbGrpDiscrim" id="cmbGrpDiscrim" onChange="document.f2.submit()">
			<option value="fun" <? if ($grpDiscrim == 'fun') {?> selected <? } ?> >Clubs</option>
			<option value="admin" <? if ($grpDiscrim == 'admin') { ?> selected <? } ?> >Groups</option>
			<option value="suspend" <? if ($grpDiscrim == 'suspend') {?> selected <? } ?> >Suspended Clubs/Groups</option>
			<option value="viewlist" <? if ($grpDiscrim == 'viewlist') { ?> selected <? } ?> >--All--</option>
		</select>
	</form>
</fieldset>