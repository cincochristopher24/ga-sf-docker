<?
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	
?>
	
		<strong>Features</strong>
		<br />&nbsp;&nbsp;&nbsp;&raquo;
		<a href="groups.php">Group</a>
		<br />&nbsp;&nbsp;&nbsp;&raquo;
		<a href="travelers.php">Traveler</a>
		<br />&nbsp;&nbsp;&nbsp;&raquo;
		Photos -- <a href="photos.php?viewtype=entry">Entry Photos</a> | <a href="photos.php?viewtype=profile">Profile Photos</a> | <a href="photos.php?viewtype=group">Group Photos</a>
		<br />&nbsp;&nbsp;&nbsp;&raquo;
		<a href="entries.php">Journal Entry</a>
		<br />&nbsp;&nbsp;&nbsp;&raquo;
		<a href="rankfeatures.php">Arrange Features</a>
		<br />&nbsp;&nbsp;&nbsp;&raquo;
		<a href="viewFeaturedDates.php?type=2">View Featured on Date</a>
		<br />&nbsp;&nbsp;&nbsp;&raquo;
		<a href="articles.php">Pulldown Articles/Entries</a>	
		<br><br>
	
		<strong>Cobrand</strong>
		<br />
		<a href="cobrandControlPanel.php">Cobrand Control Panel</a>
		<br><br />
		
		<strong>Locations</strong>
		<br />
		<a href="geotool/geotool.php">GeoTool</a>
		<br />
		<a href = "approveCity.php"> Approve City </a>
		<br><br />

		<strong>Messages</strong>
		<br />
		<a href="../messages.php">View Messages</a>
		<br />
		<a href="spam-messages.php">View Spam Messages</a>
		<br />
		<a href="approveShoutOut.php">View Unapproved Shout Outs</a>
		<br />
		<a href="viewShoutOut.php">View Unregistered User Shout Outs</a>
		<br /><br />

		<strong>Categories / Text</strong>
		<br />
		<a href="groupcategory.php">Group Categories</a>
		<br>
		<a href="travelertype.php">Traveler Types</a>
		<br>
		<a href="helptext2.php">Help Text</a>
		<br /><br />

		<strong>User Accounts</strong>
		<br />
		<a href="travelers.php?type=status_updates">Update Traveler Status</a>
		<br />
		<a href="cobrandgroups.php">Add Travelers from File</a>
		<br />
		<a href = "inactiveTravelers.php"> Inactive Travelers </a>
		<br />
		<a href = "users.php"> GoAbroad.net Users </a>
		<br />
		<a href="demographics/index.php">Demographics</a>
		<br>
		<a href="addgroups.php">Add Groups from File</a>
		<br />
		<a href = "groupAdvisors.php"> Group Advisors </a>
		<br><br />

		<strong>Photos</strong>
		<br />
		<a href="rotatingPhoto.php">Upload Rotating Photos</a>
		<br />
		<a href="ads.php">Manage Ads</a>
		<br /><br />

		<strong>System</strong>
		<br />
		<a href="useraccount.php">Admin User Accounts</a>
		<br />
		<a href="errorlogs.php">View Error Logs</a>
		<br><br />
	
	<a href="logout.php">LOG OUT</a>