<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
	require_once 'travellog/controller/admin/Class.ViewFeaturedDatesController.php';
?>

<div id = "intro_container">
	<div id = "intro" align = "center">
		<h1>TRAVELERS</h1>
	</div>
</div>

<div id = "content_wrapper" class = "layout_2">
	<div id = "wide_column">
		<h2>
			<span><?=$title?></span>
		</h2>
		<div id = "groups_and_clubs" class = "section">
			<div class = "content">
				<div style = "padding-top: 10px;">
					<?= $search_template ?>
					<span style = "float: right">
						<?= $travelers_link ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?= $newest_traveler_link ?>
					</span>
				</div>
				<div id = "groups_list_main" style = "padding-top: 20px;">
					<? if (0 < count($obj_travelers)) : ?>
						<? if ($totalrec > $rowsperpage) : ?>
							<div class = "pagination">
								<div class = "page_markers">
									<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec?>
								</div>	
								<?$paging->getFirst()?>
								&nbsp;|&nbsp;
								<?$paging->getPrevious()?>
								&nbsp;&nbsp;
								<?$paging->getLinks()?>
								&nbsp;&nbsp;
								<?$paging->getNext()?>
								&nbsp;|&nbsp;
								<?$paging->getLast()?>
							</div>
						<? endif; ?>

						<ul class = "users">
							<?
								foreach($obj_travelers as $eachtraveler) :
									$photolink = $eachtraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink();

									if (0 != strncmp("http://", $photolink , "7"))
										$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
							?>
									<li>
										<a class = "thumb" href="/<?=$eachtraveler->getUsername()?>">
											<img src="<?=$photolink?>" alt="<?=$eachtraveler->getUsername()?>" style = "height: 95px; width: 85px" />
										</a>
										<div class = "details" style = "padding-left: 20px;">
											<strong>
												<a href="/<?=$eachtraveler->getUsername()?>"><?=$eachtraveler->getUsername()?></a>
											</strong>
											<hr width = "25%" align = "left" />
											<?if ($eachtraveler->isDeactivated()):?>
												<p class = "gray"><strong>[Deactivated Account]</strong></p>
											<? endif; ?>
											<?php if ( AdminUserAccount::ACCESS_MODERATOR != $_SESSION['userAccess']) :?>
												<? if ($section->isItemFeatured($eachtraveler->getTravelerID())) : ?>
													<p class = "gray"><strong>[Featured]</strong></p>
													<p class = "gray">
														&raquo;<a href="travelers.php?feature=<?=$eachtraveler->getTravelerID()?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">Remove Feature</a>
													</p>
												<? elseif (!$eachtraveler->isDeactivated()):?>
													<p class = "gray">
														&raquo;<a href="travelers.php?feature=<?=$eachtraveler->getTravelerID()?>">Feature This!</a>
													</p>
												<? endif; ?>
												<? if (!$eachtraveler->hasGoodStatus()) : ?>									
													<p class = "gray>">
														&raquo;<a id="update_<?=$eachtraveler->getTravelerID()?>_1" href="javascript:void(0);" onclick="updateStatus(<?=$eachtraveler->getTravelerID()?>);">Add Good Status</a>
													</p>
												<? else : ?>								
													<p class = "gray">
														&raquo;<a id="update_<?=$eachtraveler->getTravelerID()?>_0" href="javascript:void(0);" onclick="updateStatus(<?=$eachtraveler->getTravelerID()?>);">Clear Good Status</a>
													</p>
												<? endif; ?>											
												<p class = "gray">
													&raquo;<a href="messages.php?compose&travTo=<?= $eachtraveler->getTravelerID()?>">Send Message</a>
												</p>
											<? endif; ?>	
											<?php if ( AdminUserAccount::ACCESS_SALES != $_SESSION['userAccess']) :?>
												<? if (TRUE == $eachtraveler->isSuspended()) : ?>									
													<p class = "gray>">
														&raquo;<a href="travelers.php?travID=<?= $eachtraveler->getTravelerID()?>&amp;activate" onclick="javascript:return confirm('Do you want to ACTIVATE this Traveler?');">Activate Traveler</a>
													</p>
												<? else : ?>								
													<p class = "gray">
														&raquo;<a href="travelers.php?travID=<?= $eachtraveler->getTravelerID()?>&amp;suspend" onclick="javascript:return confirm('Do you want to SUSPEND this Traveler?');">Suspend Traveler</a>
													</p>
												<? endif; ?>
											<? endif; ?>
										</div>
										
										<div class = "travbox_info">
											<p class = "gray">
												<?=$eachtraveler->getCountTravelJournals()?>
												<?=(1 < $eachtraveler->getCountTravelJournals()) ? ' Journals' : ' Journal'?> | 
												<?=$eachtraveler->getCountTravelLogs()?>
												<?=(1 < $eachtraveler->getCountTravelLogs()) ? ' Entries' : ' Entry'?>
											</p>
											
											<? if ("0000-00-00 00:00:00" != $eachtraveler->getTravelerProfile()->getDateJoined()): ?>
												<p class = "gray">
													Member Since: <?= $d->set($eachtraveler->getTravelerProfile()->getDateJoined())->friendlyFormat()?>
												</p>
											<? endif; ?>

											<? if ("0000-00-00 00:00:00" != $eachtraveler->getTravelerProfile()->getLastLogin()): ?>
											<p class = "gray">
												Last Login: <?= $d->set($eachtraveler->getTravelerProfile()->getLastLogin())->friendlyFormat()?>
											</p>
											<? endif; ?>
											<?php $lastFeaturedDate = $eachtraveler->getLastFeaturedDate() ?>
											<? if (strlen($lastFeaturedDate)) : ?>
												<p class = "gray">
													Date Last Featured: <?=$d->set($lastFeaturedDate)->friendlyFormat()?>
												</p>
											<? endif; ?>
											<? if (ViewFeaturedDatesController::displayViewLink($eachtraveler->getTravelerID(), 2)) : ?>
												<p class = "gray">
													<a
														href	= "javascript: void(0)"
														class	= "edit"
														onClick = "window.open('viewFeaturedDates.php?sectionID=2&type=1&genID=<?=$eachtraveler->getTravelerID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
														[view dates]
													</a>
												</p>
											<? endif; ?>
										</div>
									</li>
							<? endforeach; ?>
						</ul>

						<? if ($paging->getTotalPages() > $rowsperpage) : ?>
							<div class = "pagination">
								<div class = "page_markers">
									<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec ?>
								</div>
								<?$paging->getFirst()?>
								&nbsp;|&nbsp;
								<?$paging->getPrevious()?>
								&nbsp;&nbsp;
								<?$paging->getLinks()?>
								&nbsp;&nbsp;
								<?$paging->getNext()?>
								&nbsp;|&nbsp;
								<?$paging->getLast();?>
							</div>
						<? endif; ?>
					<? else : ?>
						<h4 style = "text-align: center; text-transform: uppercase; font-weight: bold">Your search has returned 0 rows.</h4>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>

	<div id = "narrow_column">
		<div class = "section">
			<h2 style = "text-align: center">
				<? if (count($currFeat)) : ?>
					<? if (1 < count($currFeat)) : ?>
					 	CURRENTLY FEATURED TRAVELERS
					<? else : ?>
						CURRENTLY FEATURED TRAVELER
					<? endif; ?>
				<? endif; ?>
				<div style = "font-weight: normal; font-size: 12px; padding-top: 5px; text-transform: none">
					<strong>Caption: </strong>
					<?=$section->getCaption()?>
					<br />
					<strong>Quantity: </strong>
					<?=$section->getQuantity()?>
					<?php if ( AdminUserAccount::ACCESS_MODERATOR != $_SESSION['userAccess']) :?>
					<br />
					<a
						href	= "javascript: void(0)"
						class	= "edit"
				 		onClick	= "window.open('caption.php?sectionID=2','PopUp','left=300,height=200,width=400,status=yes')">
						change
					</a>
					<?php endif;?>
				</div>
			</h2>
			<div class = "content">
				<ul class = "users">
					<?
						foreach ($currFeat as $eachtraveler) :
							 $photolink = $eachtraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink();
								if (0 != strncmp("http://", $photolink , "7"))
									$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
					?>
						<li>							
							<a class = "thumb" href="http://<?=$_SERVER['SERVER_NAME']?>/<?=$eachtraveler->getUsername()?>">
								<img src="<?= $photolink ?>" alt="<?=$eachtraveler->getUsername()?>" style = "width: 80px; height: 90px;" />
							</a>
							<div class = "details" style = "width: 150px; margin-left: 15px">
								<h4>
									<a href="http://<?=$_SERVER['SERVER_NAME']?>/<?=$eachtraveler->getUsername()?>">
										<?=$eachtraveler->getUsername()?>
									</a>
								</h4>
								<hr width = "98%" align = "left">
								<?php if ( AdminUserAccount::ACCESS_MODERATOR != $_SESSION['userAccess']) :?>
									<p class = "gray">
										&raquo;<a href="travelers.php?feature=<?=$eachtraveler->getTravelerID()?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">Remove Feature</a>
									</p>
									<p class = "gray">
										&raquo;<a href="messages.php?compose&travTo=<?= $eachtraveler->getTravelerID()?>">Send Message</a>
									</p>
								<? endif; ?>	
								<?php if ( AdminUserAccount::ACCESS_SALES != $_SESSION['userAccess']) :?>
									<? if (TRUE == $eachtraveler->isSuspended()) : ?>
										<p class = "gray">
											&raquo;<a href="travelers.php?travID=<?= $eachtraveler->getTravelerID()?>&amp;activate" onclick="javascript:return confirm('Do you want to ACTIVATE this Traveler?');">Activate Traveler</a>
										</p>
									<? else : ?>
										<p class = "gray">
											&raquo;<a href="travelers.php?travID=<?= $eachtraveler->getTravelerID()?>&amp;suspend" onclick="javascript:return confirm('Do you want to SUSPEND this Traveler?');">Suspend Traveler</a>
										</p>
									<? endif; ?>
								<? endif; ?>
							</div>
							
							<div style = "font-size: 10px; float: left">
								<p class = "gray">
									<?=$eachtraveler->getCountTravelJournals()?>
									<?=(1 < $eachtraveler->getCountTravelJournals()) ? ' Journals' : ' Journal'?> | 
									<?=$eachtraveler->getCountTravelLogs()?>
									<?=(1 < $eachtraveler->getCountTravelLogs()) ? ' Entries' : ' Entry'?>
								</p>
								<p class = "gray">
									<? if ("0000-00-00 00:00:00" != $eachtraveler->getTravelerProfile()->getDateJoined()): ?>
										Member Since: <?= $d->set($eachtraveler->getTravelerProfile()->getDateJoined())->friendlyFormat()?>
									<? endif; ?>
								</p>
									<? if ("0000-00-00 00:00:00" != $eachtraveler->getTravelerProfile()->getLastLogin()): ?>
								<p class = "gray">
									Last Login: <?= $d->set($eachtraveler->getTravelerProfile()->getLastLogin())->friendlyFormat()?>
								</p>
									<? endif; ?>
								<?php $lastFeaturedDate = $eachtraveler->getLastFeaturedDate() ?>
								<p class = "gray">
									Date Featured: <?= strlen($lastFeaturedDate) ? $d->set($lastFeaturedDate)->friendlyFormat() : '' ?>
								</p>
							</div>
							</li>
						<? endforeach; ?>		
					</ul>
				</div>
			</div>
			<div class = "foot"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function updateStatus(id){
		if(confirm("Do you want to change the status of this traveler?")){
			var elt = jQuery("a[id^='update_"+id+"_']");
			var status = elt.attr('id').split('_')[2];
			elt.html('<img width="15" height="15" src="/images/load_gray.gif" alt="Loading..." />');
			jQuery.ajax({
				type: "GET",
				url: "travelers.php",
				data: "travID="+id+"&travelerStatus="+status,
				success: function(rsp){
					if(rsp == ""){
						if(status == 1){
							elt.html("Clear Good Status");
							elt.attr('id','update_'+id+'_0');
						}else{
							elt.html("Add Good Status");
							elt.attr('id','update_'+id+'_1');
						}	
					}
				}
			});
		}
	}
</script>