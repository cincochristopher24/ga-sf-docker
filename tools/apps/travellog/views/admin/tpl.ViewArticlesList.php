<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
?>

<div id = "intro_container">
	<div id = "intro" align = "center">
		<h1>TRAVEL ARTICLES and JOURNAL ENTRIES</h1>
	</div>
</div>

<div id = "content_wrapper" class = "layout_2">
	<div id = "wide_column">
		<h2>
			<span>Showing All Articles and Journal Entries</span>
		</h2>
		<div class = "section">
			<div class = "content">
				<div style = "padding-top: 10px;">
					<?=$search_template?>
				</div>
				<div>
					<b>* Auto-Pulldown</b> - moves the article/entry down to <?php echo date('Y-m-d', strtotime("-1 year"))?>
				</div>
				<div style = "padding-top: 20px;">
						<? if (0 < count($articles)) : ?>
							<? if ($paging->getTotalPages() > 1) : ?>
								<div class = "pagination">
									<div class = "page_markers">
										<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $paging->getRecordCount()?>
									</div>	
									<?$paging->getFirst()?>
									&nbsp;|&nbsp;
									<?$paging->getPrevious()?>
									&nbsp;&nbsp;
									<?$paging->getLinks()?>
									&nbsp;&nbsp;
									<?$paging->getNext()?>
									&nbsp;|&nbsp;
									<?$paging->getLast()?>
								</div>
							<? endif; ?>
							<ul class = "users">
								<? foreach($articles as $article): ?>
									<?php 
									      $owner = $article->getOwner();
									      if($owner instanceof Traveler):
									      	$ownerID = $owner->getTravelerID();
									      else:
									      	$ownerID = $owner->getGroupID();
									      endif;
									      if($article instanceof JournalsArticlesVO):
									      	$id = $article->getTravelID();
									      	if('JOURNAL'==$article->getType()):
									      		try{
									      			$obj = new TravelLog($id);	
									      		}
									      		catch(exception $e){}
									      		$link = '/journal-entry.php?action=view&amp;travellogID='.$id;
										      	$type = "entry";
										      	$trip = $article->getTrip();
										      	$location = $trip->getLocation();
									      	else:
									      		$obj = new Article($id);
									      		$link = '/article.php?action=view&amp;articleID='.$id;
										      	$type = "article";
										      	$location = LocationFactory::instance()->create($article->getLocID());
									      	endif;
										    $pulldown = "/admin/articles.php?mode=PULL_DOWN&amp;articleID=".$id."&amp;type=".$type;
										    $country = $location->getCountry()->getName();
											$city = $location->getName();
											$locationID = $location->getCountry()->getCountryID();
										    $photo_link = is_object($obj) ? $obj->getPrimaryPhoto()->getPhotoLink(): '';
									      	$date = $article->getDate();
									      else:
										      if($article instanceof Article):
										      	$id = $article->getArticleID();
										      	$link = '/article.php?action=view&amp;articleID='.$id;
										      	$type = "article";
										      else:
										      	$id = $article->getTravelLogID();
										      	$link = '/journal-entry.php?action=view&amp;travellogID='.$id;
										      	$type = "entry";
										      endif;
										      $pulldown = "/admin/articles.php?mode=PULL_DOWN&amp;articleID=".$id."&amp;type=".$type;
										      $location = LocationFactory::instance()->create($article instanceof Article ? $article->getLocID() : $article->getTrip()->getLocationID());
											  $country = $location->getCountry()->getName();
											  $city = $location->getName();
											  $locationID = $location->getCountry()->getCountryID();
											  $photo_link = $article->getPrimaryPhoto()->getPhotoLink();
											  $date = $article->getLogDate();
										  endif;
									?>
									<li>
										<a class = "thumb" href = "<?php echo $link; ?>">
											<img alt = "Article Photo" src="<?=$photo_link ?>" style = "height: 100px; width: 150px" />
										</a>
										<div class = "details" style = "padding-left: 85px;">
											<a href = "<?php echo $link; ?>" >
												<span style = "font-size: 14px; font-weight: bold">
													<?
														$mTitle = (40 < strlen($article->getTitle()))
																	? substr($article->getTitle(), 0, 40) . '..'
																	: $article->getTitle();
														echo $mTitle;
													?>
												</span>
											</a>
											<hr width = "25%" align = "left" />
											<?/*<p>
												&raquo; 
												<a href="/article.php?action=edit&articleID=<?=$article->getArticleID()?>">
													Edit Article
												</a>
											</p>*/?>
											<p>
												&raquo; 
												<a href="<?php echo $pulldown?>">
													Auto-Pulldown this <?php echo $type ?>.
												</a>
											</p>
											or choose a date and pulldown manually.
											<p>
											<form action="/admin/articles.php" method="GET">
											<input style="width:109px;" type="text" readonly="true" class="date-pick" name="lastupdate" id="lastupdate" value=""/><br />
											<input type="hidden" name="mode" id="mode" value="PULL_DOWN"/>
											<input type="hidden" name="type" id="type" value="<?php echo $type ?>"/>
											<input type="hidden" name="articleID" id="articleID" value="<?php echo $id ?>"/>					<input type="submit" value="Pulldown this <?php echo $type ?>" /></form></p>
										</div>
										<div class = "travbox_info">
											<p class = "gray">
												<?=$d->set($date)->friendlyFormat();?>
											</p>
											<p class = "gray">
												
												<img src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" />
												<?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?>
											</p>
											<p>
												<?if($owner instanceof Traveler):?>
													<a class="username" href="/<?=$owner->getUserName() ?>" >
														<?=$owner->getUserName() ?>
													</a>
												<?else:?>	
													<a class="username" href="../group.php?gID=<?=$owner->getGroupID() ?>" >
														<?=$owner->getName() ?>
													</a>
												<?endif;?>
											</p>
										</div>
									</li>
								<? endforeach; ?>
						</ul>
					<? else : ?>
						<h4 style = "text-align: center; text-transform: uppercase; font-weight: bold">Your search has returned 0 rows.</h4>
					<? endif; ?>

					<? if (0 < count($articles)) : ?>
						<? if ($paging->getTotalPages() > 1) : ?>
							<div class = "pagination">
								<div class = "page_markers">
									<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $paging->getRecordCount()?>
								</div>	
								<?$paging->getFirst()?>
								&nbsp;|&nbsp;
								<?$paging->getPrevious()?>
								&nbsp;&nbsp;
								<?$paging->getLinks()?>
								&nbsp;&nbsp;
								<?$paging->getNext()?>
								&nbsp;|&nbsp;
								<?$paging->getLast()?>
							</div>
						<? endif; ?>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="/min/b=css&amp;f=styles.css,basic.css" media="screen" rel="stylesheet" type="text/css" />
<script src="/minify_f.php?files=/js/jquery-1.1.4.pack.js" type="text/javascript"></script>
<script src="/minify_f.php?files=/js/date.js"              type="text/javascript"></script>
<script src="/minify_f.php?files=/js/jquery.datePicker.js" type="text/javascript"></script>
<script type="text/javascript">   
//<![CDATA[
	(function($){
		$(function()
	    {
			if( $('input').hasClass('date-pick') ){
				var edate = $.trim($('.date-pick').val());
				if( edate.length == 0 ){
					var d = new Date(); 
					edate = d.addDays(0).asString();
				}
				$('.date-pick').datePicker();     
				$('.date-pick').dpSetStartDate('2001-01-01');      
				$('#lastupdate').datePicker().val(edate).trigger('change'); 
			} 
	    });
	})(jQuery);
//]]>  
</script>