<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
?>
	<?php if($showForm): ?>
			<form action="useraccount.php?action=save&userAccountID=<?=$userID?>" method="POST" onsubmit="return validateForm(this);">
				<table border="0" width="75%" align="center">
					<tr>
						<td>User Name:</td>
						<td><input type="text" name="txtUserName" value="<?php echo $userAccount->getUsername() ?>">
						<?php if(isset($errorStack['userName']))
							echo $errorStack['userName']
						?>
						</td>
					</tr>
					<tr>
						<td>Password:
						<?php if (0 < $userID): ?>
						<br/>Leave blank if you don't wish to change password.
						<?php endif;?>
						</td>
						<td><input type="password" name="txtPassWord" value="">
						<?php if(isset($errorStack['passWord']))
							echo $errorStack['passWord']
						?>
						</td>
					</tr>
					<tr>
						<td>Retype Password:</td>
						<td><input type="password" name="txtPassWord2">
						<?php if(isset($errorStack['passWord2']))
							echo $errorStack['passWord2']
						?>
						</td>
					</tr>
					<tr>
						<td>Access Level:</td>
						<td><select name="cmbAccessLevel">
								<?php foreach($userAccessTypes as $key => $value): ?>
									<option value="<?php echo $key?>" <?php echo $userAccount->getAccesslevel() == $key ? 'selected' : ''?>><?php echo $value?>
								<?php endforeach;?>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="Save">
						</td>
						
					</tr>
				</table>
			</form>
	<?php else: ?>
			<p><a href="useraccount.php?action=add">Add New</a></p>
			<p><b>Admin User Accounts</b></p>
			<br />
			<table border="0" width="75%" align="center">
				<tr>
					<td>USERNAME</td>
					<td>PASSWORD</td>
					<td>ACCESS TYPE</td>
					<td>ACTIONS</td>
				</tr>
				<? foreach ($userAccounts as $eachAccount) { ?>
					<tr>
						<td>
							<?= $eachAccount->getUsername() ?>
						</td>
						<td> *****
						</td>
						<td>
							<?= $eachAccount->getUserAccess() ?>
						</td>
						<td>
							<a href="useraccount.php?action=edit&amp;userAccountID=<?= $eachAccount->getUserAccountID() ?>">
							Edit							</a>	
							| <a onclick="javascript:return confirm('Do you want to DELETE this User Account?')" href="useraccount.php?action=delete&amp;userAccountID=<?= $eachAccount->getUserAccountID() ?>">
							Delete</a>
						</td> 
					</tr>
				<? } ?>		
			</table>
	<?php endif; ?>