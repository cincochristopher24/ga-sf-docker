<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();	
?>

<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>
<div class="area" id="intro">
	<div class="section" align="center">
		<? if (3 == $section->getSectionID()) : ?>
			<h1>JOURNALS</h1>
		<? elseif(9 == $section->getSectionID()) : ?>
			<h1>EDITOR'S CHOICE</h1>
		<? endif ; ?>
		<h2>
		CAPTION : 
		<?=$section->getCaption()?>
		<br />
		QUANTITY : 
		<?=$section->getQuantity()?>
		<br />
		<a href="javascript: void(0)"  onClick="window.open('caption.php?sectionID=<?=$section->getSectionID()?>','PopUp','left=300,height=200,width=400,status=yes')">change</a>
		</h2>
	</div>
</div>		

<div class="area" id="intro">
	<div class="section">
		<? if (count($currFeat)) { 
			if (1 < count($currFeat)) echo '<h2>CURRENTLY FEATURED JOURNALS</h2>';
			else echo '<h2>CURRENTLY FEATURED JOURNAL</h2>';
		} ?>
	</div>
</div>		

<div class="area" id="top">
	<div id="journals_list" class="list">	
		<?	foreach ($currFeat as $eachtravel) {	
			$obj_owner	=  $eachtravel->getOwner();
			if ('Traveler' == get_class($obj_owner)){
				$owner_name = $obj_owner->getUserName();
				$owner_type = 1;
			}
			else{
				$owner_name = $obj_owner->getName();
				$owner_type = 2;
			}
			    
			 $photolink = $eachtravel->getRandomPhoto()->getPhotoLink();
				if (0 != strncmp("http://", $photolink , "7"))
					$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;			 	
		?>
			<div class="journal section">
				<div class="content">
					<div class="wrapper">
						<h2>
							<a href="/journal-entry.php?action=view&amp;travelID=<?=$eachtravel->getTravelID()?>">
								<?= $eachtravel->getTitle()?>
							</a>
						</h2>
						<div class="description">
							<hr width="25%" align="left">
							<? if (9 == $section->getSectionID()) : ?>
								<? $urlstr = "journals.php?specialfeature=".$eachtravel->getTravelID()."&specialsectionID=" . $section->getSectionID() ?>
							<? else : ?>
								<? $urlstr = "journals.php?feature=".$eachtravel->getTravelID() ?>
							<? endif; ?>
							<a href="<?=$urlstr?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">&raquo;&nbsp;Remove Feature</a>
							&nbsp;|&nbsp;<a href="/journal.php?action=Edit&travelID=<?=$eachtravel->getTravelID()?>">&raquo;&nbsp;Edit Journal</a>
							<br />							
						</div> 
					</div>
					<div class="photo">
						<a href="/journal-entry.php?action=view&amp;travelID=<?=$eachtravel->getTravelID()?>">
							<img src="<?=$photolink?>" alt="<?=$eachtravel->getTitle()?>" width="123" height="90" />
						</a>								
					</div>
					<div class="info meta">
						<? if (count($eachtravel->getTrips())): ?>
								<? $travelLoc = $eachtravel->getTrips();
									$limit = 2;
									if (2 > count($travelLoc))
										$limit = count($travelLoc);
									for($idxLoc = 0; $idxLoc < $limit; $idxLoc++){
										if (null != $travelLoc[$idxLoc]->getLocation()):
											if (0 < $idxLoc)
												echo ", ";
											echo $travelLoc[$idxLoc]->getLocation()->getName();
										endif;										
									}
								?>	
							<? endif;
							?>
							<br />	
							Viewed <?=$eachtravel->getViews()?> times
							<br />
							Last Updated: <?= $d->set($eachtravel->getLastUpdated())->friendlyFormat()?>
							<br />
							<? if (1 == $owner_type) { ?>
								<a class="username" href="/<?=$owner_name?>"><?=$owner_name?></a>									
							<? } else  { ?>
								<a class="username" href="/group.php?gID=<?=$groupID?>"><?=$owner_name?></a>
							<? } ?>
					</div>
					<div class="clear"></div>
				</div>
			</div>		
		<? } ?>		
	</div>
</div>

<div class="area" id="intro">
	<div class="section">
		<h2><?= $title ?></h2>
	</div>
</div>	

<div class="area" id="top">
	<div id="top_controls">						
		<?= $search_template ?>						
	</div>
	<div id="journals_list">
			<div class="pagination">
				<? if ($paging->getTotalPages() > 1 ): ?>		
					<div class="page_markers">
						<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . count($obj_travels) ?>
					</div>	
					<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
				<? endif; ?>	
			</div>
		
			<?  if (count($iterator) ) {
				$iterator->rewind();
				while( $iterator->valid() ) {
					$eachtravel = $iterator->current();
					$obj_owner	=  $eachtravel->getOwner();
					if ('Traveler' == get_class($obj_owner)){
						$owner_name = $obj_owner->getUserName();
						$owner_type = 1;
					}
					else{
						$owner_name = $obj_owner->getName();
						$owner_type = 2;
					}
					
					$photolink = $eachtravel->getRandomPhoto()->getPhotoLink();
					if (0 != strncmp("http://", $photolink , "7"))
						$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
					
			?>
					
				<div class="journal section">
					<div class="content">
						<div class="wrapper">
							<h2>
								<a href="/journal-entry.php?action=view&amp;travelID=<?=$eachtravel->getTravelID()?>">
									<?= $eachtravel->getTitle()?>
								</a>
							</h2>
							<div class="description">
								<hr width="25%" align="left">
								<? if ($section->isItemFeatured($eachtravel->getTravelID())) { ?>
										<strong>[Featured]</strong><br />
										<? if (9 == $section->getSectionID()) : ?>
											<? $urlstr = "journals.php?specialfeature=".$eachtravel->getTravelID()."&specialsectionID=" . $section->getSectionID() ?>
										<? else : ?>
											<? $urlstr = "journals.php?feature=".$eachtravel->getTravelID() ?>
										<? endif; ?>
										<a href="<?=$urlstr?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">&raquo;&nbsp;Remove Feature</a>
								<?	} else { ?>
										<? if (9 == $section->getSectionID()) : ?>
											<? $urlstr = "journals.php?specialfeature=".$eachtravel->getTravelID()."&specialsectionID=" . $section->getSectionID() ?>
										<? else : ?>
											<? $urlstr = "journals.php?feature=".$eachtravel->getTravelID() ?>
										<? endif; ?>
										<a href="<?=$urlstr?>">&raquo;&nbsp;Feature This!</a>
								<? } ?>			
								&nbsp;|&nbsp;<a href="/journal.php?action=Edit&travelID=<?=$eachtravel->getTravelID()?>">&raquo;&nbsp;Edit Journal</a><br />		
							</div>
						</div>
						<div class="photo">
							<a href="/journal-entry.php?action=view&amp;travelID=<?=$eachtravel->getTravelID()?>">
								<img src="<?=$photolink?>" alt="<?=$eachtravel->getTitle()?>" width="123" height="90" />
							</a>								
						</div>
						<div class="info meta">
							<? if (count($eachtravel->getTrips())): ?>																		
									<? $travelLoc = $eachtravel->getTrips();
									
									$limit = 2;
									if (2 > count($travelLoc))
										$limit = count($travelLoc);
									for($idxLoc = 0; $idxLoc < $limit; $idxLoc++){
										if (null != $travelLoc[$idxLoc]->getLocation()):
											if (0 < $idxLoc)
												echo ", ";
											echo $travelLoc[$idxLoc]->getLocation()->getName();
										endif;										
									}	?>				 									
								<? endif;
								?>
								<br />	
								Viewed <?=$eachtravel->getViews()?> times
								<br />
								Last Updated: <?= $d->set($eachtravel->getLastUpdated())->friendlyFormat()?>
								<br />
								<? if (1 == $owner_type) { ?>
									<a class="username" href="/<?=$owner_name?>"><?=$owner_name?></a>									
								<? } else  { ?>
									<a class="username" href="/group.php?gID=<?=$groupID?>"><?=$owner_name?></a>
								<? } ?>
						</div>
						<div class="clear"></div>
					</div>
				</div>		
			<?	
				$iterator->next(); 
				} 
			  } 
			?>
			<div class="pagination">
				<? if ($paging->getTotalPages() > 1 ): ?>		
					<div class="page_markers">
						<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . count($obj_travels) ?>
					</div>	
					<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
				<? endif; ?>	
			</div>
	</div>
</div>
		
