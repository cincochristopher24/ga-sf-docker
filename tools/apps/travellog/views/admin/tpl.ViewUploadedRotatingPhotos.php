<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>
<div class="area" id="intro">
	<div class="section" align="center">
		<h1>ROTATING PHOTOS</h1>		
	</div>
</div>

<div id = "top" class="area" style = "float:none;">
	<div style = "margin-left:auto;margin-right:auto;width: 980px;">
		<div class="wrapper">
			<a href="rotatingPhoto.php?action=view">View All Rotating Photos</a>
			<div class="section">
				<div class="content">
					<form action="rotatingPhoto.php" method="post">
					<ul>
					<? for($i=0;$i<count($photos);$i++):?>
					<? $photo = $photos[$i];?>
						<li>
							<img src="http://<?=$_SERVER['SERVER_NAME']?>/images/rotating/<?=$photo->getPhotoName()?>"/>
							<p>
								<strong>Filename:</strong> <?=$photo->getFilename()?>
								&nbsp;&nbsp;&nbsp;
								<strong>New Filename:</strong> <?=$photo->getPhotoName()?>
							</p>
							<input type="hidden" id="hidPhotoID-<?=$i?>" name="hidPhotoID-<?=$i?>" value="<?=$photo->getPhotoID()?>"/>
							<p>
								<strong>Caption:</strong>
								<input type="text" name="txtCaption-<?=$i?>" size="100" value="<?=str_replace("\"","&quot;",stripslashes($photo->getCaption()))?>"/>
							</p>
							<p>
								<strong>Status:</strong>
								<input type="radio" name="radStatus-<?=$i?>" value="0"<?if($photo_active_status==$photo->getStatus()):?>checked<?endif;?>/>Active
								<input type="radio" name="radStatus-<?=$i?>" value="1"<?if($photo_inactive_status==$photo->getStatus()):?>checked<?endif;?>/>Inactive
							</p>
							<p>
								<strong>Link From:</strong>
								<input type="radio" id="radTypeTraveler-<?=$i?>" name="radType-<?=$i?>" value="0"<?if($photo->getTypeID()==$traveler_type_id):?>checked<?endif;?> onclick="RotatingPhotoJS.selectType(<?=$i?>,0);"/>Traveler
								<input type="radio" id="radTypeGroup-<?=$i?>" name="radType-<?=$i?>" value="1"<?if($photo->getTypeID()==$group_type_id):?>checked<?endif;?> onclick="RotatingPhotoJS.selectType(<?=$i?>,1);"/>Group
								<br/>
								<strong>ID:</strong><?=$photo->getUserID()?>
								<?if(!$photo->isExistingUser()):?> (User Does Not Exist!)
								<?elseif($photo->isSuspendedUser()):?> (User Is Suspended!)
								<?endif;?>
								<br/>
								<strong>Name:</strong> 
								<input type="text" id="txtFrom-<?=$i?>" name="txtFrom-<?=$i?>" value="<?=str_replace("\"","&quot;",stripslashes($photo->getFrom()))?>"/>
								<span id="type_filter-<?=$i?>"> 
									<? if($photo->getTypeID()==0):?>
									<select id="cmbTravelerFilter-<?=$i?>" name="cmbTravelerFilter-<?=$i?>">
										<option value="username">Username</option>
										<option value="email">Email</option>
									</select>
									<? endif;?>
								</span>
								<input type="button" value="SEARCH" onclick="RotatingPhotoJS.searchUser(<?=$i?>);"/>
							</p>
							<input type="hidden" name="hidUserID-<?=$i?>" id="hidUserID-<?=$i?>" value="<?=$photo->getUserID()?>"/>
							<div id="searchResult-<?=$i?>"></div>
							<br/>
						</li>
						<hr/>
					<?endfor;?>
					</ul>
					<input type="hidden" name="count" value="<?=count($photos)?>"/>
					<input type="hidden" name="action" value="edit"/>
					<input type="submit" value="SAVE" name="submit"/>
					</form>
				</div>
			</div>
			<a href="rotatingPhoto.php?action=view">View All Rotating Photos</a>
		</div>
	</div>
</div>