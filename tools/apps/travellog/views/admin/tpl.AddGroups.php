<?php
	if(!empty($fields)){
		$groupFields = '';
		foreach($fields as $field)
			$groupFields .= "<option value='" . $field . "'>" . $field . "</field>";
	}
?>
<div id="content_wrapper">
	<h1><em>Load Group Details from Excel Files</em></h1>
	<hr />
	<form action = "addgroups.php" method="post" enctype="multipart/form-data">
		<input type="file" name="file" id="file" />
		<div style="float:right">
			<input type="submit" value="Load File">
		</div>
		<input type="hidden" name="action" value="loadFile">
	</form>
	<br />
	<hr />
	
	<?php if(empty($errors)):?>
		
		<?php if(empty($details)):?>
			
			<?php if(empty($createdGroups)):?>
				<table border="1" id="details" width="100%" >
					<tr><td align="center"><strong><em>No details loaded!</em></strong></td></tr>
				</table>
			<?php else: ?>
				<h2> <?=$status?> <?=$parentGroup ? 'Sub-Groups for ' . '<a href="' . $parentGroup->getFriendlyURL() . '"> ' .  $parentGroup->getName() . '</a>' : 'Groups'?>:</h2>
				<div id="top"></div>
				<form action="addgroups.php" method="post">
					<strong><em>With selected</em></strong>
					<select name="manage">
						<option value="0"> ----- </option>
						<option value="delete"> Delete Groups </option>
					</select>
					<input type="submit" value="GO" onclick="return confirm('Action is undoable, are you sure you want to do this?');">
					<table border="1" id="details" width="50%" >
						<tr>
							<th><input type="checkbox" onclick="toggleCheckbox(this.checked, 'chkGroup')" ></th>
							<th>Group ID</th>
							<th>Group Name</th>
						</tr>
						<?php foreach($createdGroups as $group):?>
							<tr>
								<td align="center"><input type="checkbox" name="chkGroup[]" class="chkGroup" value="<?=$group->getGroupID()?>"></td>
								<td align="center"><?=$group->getGroupID()?></td>
								<td align="center"><a href="<?=$group->getFriendlyURL()?>"><?=$group->getName()?></a></td>
							</tr>
						<?php endforeach;?>
					</table>
					<input type="hidden" name="action" value="manage">
				</form>
				<a href="#top">Back to top </a>
			<?php endif;?>
			
			
		<?php else:?>
			
			<div id="top">
				<form method="post" action = "addgroups.php">
					Make subgroups for:
					<select name="parentGroup" id="parentGroup">
						<?php foreach($cobrandGroups as $grp):?>
							<option value="<?=$grp->getGroupID()?>" ><?=$grp->getName()?></option>
						<?php endforeach;?>
					</select>
					<input type="submit" value="Create Groups" onclick="return prepareData('details', 'createGroups')">
					<input type="submit" value="Search Groups" onclick="return prepareData('details', 'searchGroups')">
					<input type="hidden" id="hidData" name="hidData">
					<input type="hidden" name="action" id="action">
				</form>
			</div>
			<table border="1" id="details" width="100%" >
				<tr>
					<th id = 1><input type="checkbox" onclick="toggleCheckbox(this.checked, 'chkDetail')" ></th>
					<?php for($x = 1; $x<$numCols; ++$x):?>
						<th id="<?=$x+1?>">
							<div>
							<select id="grpFields<?=$x+1?>">
								<?=$groupFields?>
							</select>
							</div>
							<a href="javascript:void(0)" onclick="deleteColumn('details', this.parentNode.id)">Delete this column</a></th>
					<?php endfor;?>
				</tr>
			
				<?php foreach($details as $k => $v) :?>
					<tr>
						<td align="center"><input type="checkbox" class="chkDetail" value="<?=$k?>"></td>
						<?php foreach($v as $colKey => $col):?>
							<td class="row<?=$k?>"><?=$col?></td>
						<?php endforeach;?>
					</tr>
				<?php endforeach;?>
			</table>
			<a href="#top">Back to Top</a>
			
		<?php endif;?>
		
	<?php else:?>
	<div class="errors">
		<ul>
			<li><strong><?php echo join('</li><li>', $errors)?></strong></li>
		</ul>
	</div>
	<?php endif;?>
	<br />
</div>