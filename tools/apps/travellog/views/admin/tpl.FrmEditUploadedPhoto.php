<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>
<div class="area" id="intro">
	<div class="section" align="center">
		<h1>ROTATING PHOTOS</h1>		
	</div>
</div>

<a href="uploadPhoto.php">Upload Photos</a>
<form action="uploadPhoto.php" method="post">
<? for($i=0;$i<count($photos);$i++):?>
<? $photo = $photos[$i];?>
	<div>
		Filename: <?=$photo->getFilename()?><br/>
		New Filename: <?=$photo->getPhotoName()?><br/>
		<img src="http://<?=$_SERVER['SERVER_NAME']?>/images/rotating/<?=$photo->getPhotoName()?>"/>
		<input type="hidden" id="hidPhotoID-<?=$i?>" name="hidPhotoID-<?=$i?>" value="<?=$photo->getPhotoID()?>"/>
	</div>
	<div>
		Caption:
		<input type="text" name="txtCaption-<?=$i?>" size="100" value="<?=str_replace("\"","&quot;",stripslashes($photo->getCaption()))?>"/>
	</div>
	<div>
		Status:
		<input type="radio" name="radStatus-<?=$i?>" value="0"<?if($photo_active_status==$photo->getStatus()):?>checked<?endif;?>/>Active
		<input type="radio" name="radStatus-<?=$i?>" value="1"<?if($photo_inactive_status==$photo->getStatus()):?>checked<?endif;?>/>Inactive
	</div>
	<div>
		Link From:
		<input type="radio" id="radTypeTraveler-<?=$i?>" name="radType-<?=$i?>" value="0"<?if($photo->getTypeID()==$traveler_type_id):?>checked<?endif;?> onclick="RotatingPhotoJS.selectType(<?=$i?>,0);"/>Traveler
		<input type="radio" id="radTypeGroup-<?=$i?>" name="radType-<?=$i?>" value="1"<?if($photo->getTypeID()==$group_type_id):?>checked<?endif;?> onclick="RotatingPhotoJS.selectType(<?=$i?>,1);"/>Group
		<br/>
		ID:<?=$photo->getUserID()?>
		<?if(!$photo->isExistingUser()):?> (User Does Not Exist!)
		<?elseif($photo->isSuspendedUser()):?> (User Is Suspended!)
		<?endif;?>
		<br/>
		Name: 
		<input type="text" id="txtFrom-<?=$i?>" name="txtFrom-<?=$i?>" value="<?=str_replace("\"","&quot;",stripslashes($photo->getFrom()))?>"/>
		<span id="type_filter-<?=$i?>"> 
			<? if($photo->getTypeID()==0):?>
			<select id="cmbTravelerFilter-<?=$i?>" name="cmbTravelerFilter-<?=$i?>">
				<option value="username">Username</option>
				<option value="email">Email</option>
			</select>
			<? endif;?>
		</span>
		<input type="button" value="SEARCH" onclick="RotatingPhotoJS.searchUser(<?=$i?>);"/>
	</div>
	<input type="hidden" name="hidUserID-<?=$i?>" id="hidUserID-<?=$i?>" value="<?=$photo->getUserID()?>"/>
	<div id="searchResult-<?=$i?>"></div>
	<br/><br/>
<?endfor;?>
<input type="hidden" name="count" value="<?=count($photos)?>"/>
<input type="hidden" name="action" value="edit"/>
<input type="submit" value="SAVE" name="submit"/>
</form>

<div class="pagination">
<? if(isset($paging)):?>
<? if ($paging->getTotalPages() > 1 ): ?>		
	<div class="page_markers">
		<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . count(RotatingPhoto::getAllPhotos()) ?>
	</div>	
	<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
<? endif; ?>
<? endif; ?>		
</div>