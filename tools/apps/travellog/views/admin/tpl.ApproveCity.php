<?php
	$load_city = "manager.getUnapprovedCity();";
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::includeDependent($map->getJsToInclude());
	Template::includeDependentJs("/min/f=js/prototype.js", array('top' => true));
	Template::includeDependentJs("/js/scripts.js");

	if ('' == $param) :{
	$code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	var manager = new TravellogManager(); 
	window.onload = function(){
		manager.useLoadingMessage();
		$load_city
	}
//]]>
</script>
BOF;
	}
	else :{
$code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	var manager = new TravellogManager(); 
	window.onload = function(){
		manager.useLoadingMessage();
		manager.getSelectedUnapprovedCity('$param');
	}
//]]>
</script>
BOF;
	}
	endif;
	Template::includeDependentJs("js/moo1.ajax.js");
	Template::includeDependentJs("js/travellogManager.js");
	Template::includeDependent($code);

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
?>

<div id = "body" class = "layout_2">
	<div class = "section">
		<div class = "content">
			<? if (count($arrCountry)) : ?>
				<form id = "frmSearch" action = "approveCity.php" method = "post">
					<div style = "width: 630px; margin-left: auto; margin-right: auto;">
						<fieldset>
							<div style = "width: 530px; float: right;">
								<p>
									<span>
										<strong>Country: </strong>
										<select name = "countryID" id = "countryID" onchange = "manager.getUnapprovedCity();">
											<? foreach ($arrCountry as $indCountry) : ?>
											<option value = "<?=$indCountry->getLocationID()?>" <? if ($indCountry->getLocationID() == $countryLocID): ?> selected <? endif; ?>> <?=$indCountry->getName()?> </option>
											<? endforeach; ?>
										</select>
									</span>
									<span style = "margin-left: 20px">
									 	<strong>City: </strong>
										<select name = "cityID" id = "cityID">
											<? foreach ($arrCity as $indCity) : ?>
											<option value = "<?=$indCity->getLocationID()?>" <? if ($indCity->getLocationID() == $cityLocID): ?> selected <? endif; ?>> <?=$indCity->getName()?> </option>
											<? endforeach; ?>
										</select>
									</span>
								</p>
								<p style = "float: right">
									<input type = "submit" name = "btnSubmit" id = "btnSubmit" value = " Submit " />
								</p>
							</div>
						</fieldset>						
					</div>
				</form>
				<form id = "frmApprove" action = "act_approveCity.php" method = "post">
					<div style = "width: 1050px; margin-left: auto; margin-right: auto;">
						<div class="content">
							<ul class = "form">
								<li style = "padding-bottom: 10px;">
									<label for = "txtCity"> What's the name of this place? </label>
									<input type = "text" name = "txtCity" id = "txtCity" value = "<?=$txtCity?>" />
								</li>
								<li>
									<label> Where is it? </label>
									<div style = "padding-left: 5px;">
										<fieldset>
											<span style = "float: left">
												<div id = "viewallcountries_map" style = "border: solid 1px; width: 520px">
													<?php
														$map-> addMapFrame($mFrame);
														$map-> plotLocations($arrLocationMarker);
													?>
												</div>
											</span>
											<span style = "float: right">
												<div style = "padding-left: 60px;">
													<div style = "padding: 25px; border: solid 1px; margin-left: auto; margin-right: auto;">
														<div> <h4> <?=strtoupper($txtCity)?> is at: </h4> </div>
														<div style = "padding-top: 10px;">
															Latitude: &nbsp; &nbsp;
															<input type = "text" name = "geoLat" id = "geoLat" value = "" />
														</div>
														<div style = "padding-top: 10px; padding-bottom: 10px;">
															Longitude:
															<input type = "text" name = "geoLong" id = "geoLong" value = "" />
														</div>
														<div style = "padding-top: 30px; padding-left: 25px;">
															<input type = "button" name = "btnGetCoordinates" id = "btnGetCoordinates" value = "Get Coordinates" onclick = "showGeoCode()" />
															<input type = "button" name = "btnPlot" id = "btnPlot" value = "Plot to Map" onclick = "plot()" />
															<input type = "button" name = "btnReplace" id = "btnReplace" value = "Replace Coordinates" onclick = "replaceCoords()" />
														</div>
													</div>
												</div>
											</span>
										</fieldset>
									</div>
									<div class="clear"></div>
								</li>
								<li style = "padding-bottom: 20px;">
									<fieldset>
										<strong>LONGITUDE</strong>
										<input type = "text" name = "txtLong" id = "txtLong" value = "<?=$txtLong?>" />
										<strong>LATITUDE</strong>
										<input type = "text" name = "txtLat" id = "txtLat" value = "<?=$txtLat?>" />
									</fieldset>
								</li>
								<div class="clear"></div>
								<? 	if (stristr($navigator_user_agent, "safari")) : ?>
									<div style = "padding-bottom: 20px;"></div>
								<? endif; ?>
								<li style = "padding-bottom: 10px;">
									<input type = "submit" name = "btnApprove" id = "btnApprove" value = "Approve" />
									<? if (0 < $cntJournal) : ?> <input type = "button" name = "btnDelete" id = "btnDelete" value = "Delete" onclick = "alert('City cannot be deleted! One or more journal-entries are connected to it.');" />
									<? else : ?> <input type = "submit" name = "btnDelete" id = "btnDelete" value = "Delete" /> <? endif; ?>
								</li>
							</ul>
						</div>
						<div class="clear"></div>
					</div>
					<?php
						$cityLocID = isset($_POST['cityID']) ? $_POST['cityID'] : $cityLocID;
						$countryLocID = isset($_POST['countryID']) ? $_POST['countryID'] : $countryLocID;
					?>
					<input type = "hidden" name = "hdnCityID" id = "hdnCityID" value = "<?=$cityLocID?>" />
					<input type = "hidden" name = "hdnCountryID" id = "hdnCountryID" value = "<?=$countryLocID?>" />
				</form>
			<? else : ?>
				<h3 align = "center"> No Unapproved City! </h3>
			<? endif; ?>
		</div>
	</div>
</div>