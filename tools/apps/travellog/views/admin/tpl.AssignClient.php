<div id="page">
	<div class="content">
		<div id="rightColumn" class="column">
			<div class="mainBox">
				<h2>Search .com Client Accounts</h2>
			</div>
			
			<div class="searchProgram divider">
	 			<form autocomplete="off" id="searchForm" method="post">
					<p>
						<input type="text" name="clientName" id="clientName" size="50"/>
						<input type="submit" name="submit" id="submit" value="Search Client " class="btnLess"/>
					</p>
				</form>
			</div>
			<?php if(count($clients)) { ?>
				<div>
					<p>Showing <?=count($clients)?> results</p>
				</div>
			<?php } else { ?>
				<div>
					<p>No results found for the search.</p>
				</div>	
			<?php } ?>
			<div class="mainBox">
				<?php if(count($clients))  : ?>
					<table width="50%" align="center">
						<tr>
							<td width="20%" align="center">ID</td>
							<td  align="center">Name</td>
							<td  align="center">Assign</td>
						</tr>
					<?php foreach($clients as $key => $value) { ?>
						<tr>
							<td><?= $key ?></td>
							<td><a href="http://www.goabroad.com/admin/new_clients.cfm?clientID=<?=$key?>"><?= $value ?></a></td>
							<td><a href="/admin/assignClient.php?gID=<?=$gID?>&amp;cID=<?=$key?>">Select</a></td>
						</tr>
					<?php }?>
					</table>
					</table>
				<?php endif; ?>					
			</div>			
		</div>
	</div>		
</div>
