<?php
/**
 * Created on May 10, 2007
 *
 * @author daf 
 */
 
 	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
?>
	<div class="area" id="intro">
		<div class="section" align="right">
			<h2><a href="index.php">GoAbroad Network Admin</a></h2>
		</div>
		<div class="section" align="center">
			<h1>Cobrand Newsletter Generator</h1>		
		</div>
	</div>		
	
	<div class="area" id="intro">
		<div class="section">
		</div>
	</div>	
	<div class="area" id="top">
	  
    <form action="generated.php" method="POST" enctype="multipart/form-data">
	    <ul class="form" id="nl_generator_form">
	      <li id="nl_template_first_field">  
	        <label for="nl_template">Template</label>
	        <select name="nl_template" id="nl_template">
	          <option value="intro">Select a Template</option>
	          <?php foreach ($nl_templates as $template_name => $vars): ?>
	            <option value="<?=$template_name?>"><?=$template_name?></option>
	          <?php endforeach;?>
	        </select>
	      </li>
    	  <li>
	        <label for="data_file">Upload Json Data File</label>
      		<input type="file" name="data_file" id="data_file" />
	      </li>
	    </ul>
	    <div id="nl_template_fields"></div>
      <ul class="form">
	      <li class="actions">
	        <input type="submit" class="submit" name="btnGenerate" value="Generate" id="btnGenerate" disabled="disabled" />
	      </li>
	    </ul>
	    
    	<script type="text/javascript">
    	//<!--
    	$ = jQuery;
    	var templates = {
    	  <?php 
    	    /**
    	     * The following code creates a javascript object codeblock 
    	     * with a format similar to this:
    	     * '200912': {
       	   *   'cobrand_name': ['Cobrand Name', 'default_value'],
       	   *   'cobrand_url': ['Cobrand Url', 'http://default_value']
       	   *  }
    	     */
    	    $last_template = end($nl_templates);
    	    foreach ($nl_templates as $template_name => $vars): 
    	  ?>
    	  '<?=$template_name?>': {
    	    <?php 
    	      $last_var = end($vars);
    	      foreach($vars as $var_name => $var_value): 
    	        $label_text = ucwords(str_replace('_', ' ', $var_name));
    	    ?>
  	      '<?=$var_name?>' : ['<?=$label_text?>', '<?=$var_value?>']<?php if ( $last_var !== $var_value ): ?>,<?php endif; ?>
    	    <?php endforeach; ?>
    	  }<?php if ( $last_template !== $vars ): ?>,<?php endif; ?>
  	    <?php endforeach; ?>
    	  
    	};
    	var show_forms = function() {
    	 var the_template = $('#nl_template option:selected').val();
    	 if (the_template == 'intro') {
    	   $('#nl_template_fields').html('');
    	   $('#btnGenerate').attr('disabled', 'disabled');
    	   $('#data_file').attr('disabled', 'disabled');
    	   return;
    	 }
    	 var list = '<ul class="form">';
    	 for (var i in templates[the_template]) {
    	   var v = templates[the_template][i];
    	   list += '\n<li>\n<label for="' + i + '">' + v[0] + '</label>\n';
    	   list += '<input type="text" name="' + i + '" id="' + i + '" size="' + (v[1].length + 5) + '" ?>\n';
    	   list += '<p class="supplement">e.g: ' + v[1] + '</p>\n</li>';
    	 }
    	 list += '\n</ul>';
    	 $('#nl_template_fields').html(list);
    	 $('#btnGenerate').removeAttr('disabled');
    	 $('#data_file').removeAttr('disabled');
    	};
    	show_forms();
    	$('#nl_template').change(show_forms);
    	//-->
    	</script>
	  </form>
	  
	</div>