<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />				
		<title>Bulletin Management</title>
		<link rel="stylesheet" type="text/css" href="/travellog/css/adhoc.css" media="screen" />
		<script type="text/javascript">
			function sendOptionManager(){																				
				var toggleChkBoxes = function(state){
					var inputs = document.getElementsByTagName('input');
					for( var i = 0, input; input = inputs[i]; i++ ) {
						if(input.type == "checkbox"){
							input.checked = state;
						}
					}
				};
				this.selectAll = function(){
					toggleChkBoxes(true);
				};
				this.deselectAll = function(){
					toggleChkBoxes(false);
				};
			}
		</script>
	</head>
	<body onload="_sendOptionManager = new sendOptionManager()">
		<div id="wrapper1">
			<?php   
				if($method=="view"):									
			?>
				<div>
					<table cellpadding="7px">
						<tr>
							<td valign="top">
								<!-- panel -->
								<div>									
									<table cellpadding="3px">
										<tr>
											<td align="center">
												<a href="/bulletinManagement.php?method=compose&groupID=<?= $groupID ?>&hlEntryID=<?= $hlEntryID?>">Post New Bulletin</a>
											</td>
										</tr>
										<tr>
											<td>												
												<table border="1" cellpadding="4px" style="width:200px">													
													<?php 														
														foreach($bulletins as $bulletin){ 												
													?>
														<tr <?php if($hlEntryID == $bulletin->getMessageID()){ echo "bgcolor='yellow'"; $hlBulletin = $bulletin;} ?>>
															<td>																
																<?= date('M d, Y',strtotime($bulletin->getCreated())) ?><br />
																<a href="/bulletinManagement.php?groupID=<?= $groupID ?>&hlEntryID=<?=$bulletin->getMessageID()?>"><?= $bulletin->getTitle() ?></a><br />
																<?= $bulletin->getSource()->getUserName() ?> | 
																<?php 
																	$dest = $bulletin->getDestination();
																	$destLen = count($dest);
																	for($i=0;$i<$destLen;$i++){
																		$isOwner = false;																																		
																		if($dest[$i] instanceof Group)
																			echo $dest[$i]->getName();
																		else{																																					
																			if($dest[$i]->getUserName() != $bulletin->getSource()->getUserName()){
																				echo $dest[$i]->getUserName();
																			}
																			else{
																				$isOwner = true;
																			}
																		}
																		if($i+1<$destLen && !$isOwner) echo ", ";
																	}
																?>
															</td>
														</tr>
													<?php 															
														} 
													?>
												</table>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<td valign="top">
								<!-- info -->
								<table border="0" cellpadding="1px" style="width:400px">
									<?php if (isset($hlBulletin)):  ?>
										<tr>
											<td align="right">
												<?= $hlBulletin->getCreated() ?>
											</td>
										</tr>
										<tr>
											<td>
												<h3><?= $hlBulletin->getTitle() ?></h3>
												<a href="/bulletinManagement.php?attributeID=<?= $hlBulletin->getAttributeID() ?>&groupID=<?= $groupID ?>&method=edit&hlEntryID=<?= $hlEntryID?>">Edit</a> | 
												<a href="/bulletinManagement.php?attributeID=<?= $hlBulletin->getAttributeID() ?>&groupID=<?= $groupID ?>&method=delete&hlEntryID=<?= $hlEntryID?>">Delete</a><br /><br />
												<strong>Details: </strong><br />
												<?= $hlBulletin->getText() ?>
											</td>
										</tr>
									<?php else: ?>
										<tr>
											<td>No bulletins.</td>
										</tr>
									<?php endif; ?>
								</table>
							</td>
						</tr>
					</table>
				</div>
			<?php 				
				elseif($method=="edit" || $method=="compose"):
			?>	
				<form name="frmBulletin" method="post" action="bulletinManagement.php">
					<table>
						<tr>
							<td valign="top">Title:</td>
							<td>
								<?php if(isset($errCodes) && in_array(1,$errCodes)): ?>
									<font color="red">Please enter a title for the bulletin.</font><br />
								<?php endif; ?>
								<input name="txtTitle" value="<?=$title ?>" type="text" style="width:350px">
							</td>
						</tr>
						<tr>
							<td valign="top">Message:</td>
							<td>
								<?php if(isset($errCodes) && in_array(2,$errCodes)): ?>
									<font color="red">Please enter a message for the bulletin.</font><br />
								<?php endif; ?>
								<textarea name="txaMessage" cols="50" rows="10"><?=$message ?></textarea>
							</td>
						</tr>
						<tr>
							<td valign="top">Send To:</td>
							<td>
								<table>
									<tr>
										<td colspan="2">
											<?php if(isset($errCodes) && in_array(3,$errCodes)): ?>
												<font color="red">Please select one from the recipients.</font><br />
											<?php endif; ?>
											<a href="javascript: void(0)" onclick="_sendOptionManager.selectAll()">Select All</a>&nbsp;
											<a href="javascript: void(0)" onclick="_sendOptionManager.deselectAll()">Deselect All</a><br />								
											<input type="checkbox" value="friends" name="chkSendTo[]" 
												<?= (isset($sendTo) && in_array("friends",$sendTo))?"checked":"" ?>
											>Friends
										</td>
									</tr>
									<tr>
										<td colspan="2">Groups:</td>
									</tr>
									<?php 
										//loop for the groups						
										$groupCount = count($travelerGroups);				
										$groupLen = ceil($groupCount/2) ;
										echo "Group Count: ".count($travelerGroups);										 
										echo "Group Len: ".$groupLen;
										$ctr = 0;
										for($i=0;$i<$groupLen;$i++){
									?>
										<tr>
											<td>
												<input type="checkbox" value="<?= $travelerGroups[$ctr]->getSendableID() ?>" name="chkSendTo[]" <?= (isset($sendTo) && in_array($travelerGroups[$ctr]->getSendableID(),$sendTo))?"checked":"" ?> >
												<?= $travelerGroups[$ctr]->getName() ?>
											</td>
											<td>
											<?php if($ctr < $groupCount): ?>
												<?php $ctr++;  ?>
												<input type="checkbox" value="<?= $travelerGroups[$ctr]->getSendableID() ?>" name="chkSendTo[]" <?= (isset($sendTo) && in_array($travelerGroups[$ctr]->getSendableID(),$sendTo))?"checked":"" ?>>
												<?= $travelerGroups[$ctr]->getName()?>
											<?php else: ?>
												&nbsp;
											<?php endif; ?>
											</td>
										</tr>
									<?php
										$ctr++;										
										}
									?>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="right">
								<input type="submit" name="btnSubmit" value="Cancel">&nbsp;&nbsp;
								<input type="submit" name="btnSubmit" value="Submit">								
							</td>
						</tr>
					</table>
					<input type="hidden" name="hidMethod" value="save">
					<input type="hidden" name="hidAttributeID" value="<?=$attributeID?>">
					<input type="hidden" name="hidGroupID" value="<?=$groupID?>">
					<input type="hidden" name="hlEntryID" value="<?=$hlEntryID?>">
				</form>
			<?php  
				endif;
			?>
		</div>					
	</body>
</html>