<?php
	
	/*
	 * Class.ViewMessageList.php
	 * Created on Nov 12, 2007
	 * created by marc
	 */
	 
	require_once("travellog/views/Class.AbstractView.php");

	class ViewMessageList extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewMessageList.php');
		}
		
	}
	
?>
