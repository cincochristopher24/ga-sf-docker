<?
	/** 
	* tpl.ViewMessageList.php
	* @author marc
	* aug 2006
	*/
	require_once('Class.GaDateTime.php');
	require_once('travellog/helper/DisplayHelper.php');
	require_once("Class.Constants.php");
	$dateobj = new GaDateTime(); 
	
	$contents["subNavigation"]->show();
?>
	<div id="content_wrapper" class='layout_2'>
			
			<div id="wide_column">
				
				<div class="section" id="section_messages">
					<h2><span>
						<?= ucwords(( $contents["active"] == constants::SENT_ITEMS ) ? "sent items" : $contents["active"]); ?>
					</span></h2>
				<? if ( count($contents["messageList"]) ) : ?>
					<div class="content">
					<?//= $sub_views['MESSAGE_PAGING']->render(); ?>
					<form action="messages.php" method="post" id="message_form">
						<div class="message">
						<table id="message_list" width="100%" cellspacing="0">
							<? if( $contents["active"] == constants::INQUIRY && count($contents["arrListings"]) ): ?>
								<tr>
									<th colspan="4">Listing Name : &nbsp;
										<select name="lstListingID" onchange="document.forms[0].submit()">
											<? foreach( $contents["arrListings"] as $listing): ?>
												<option value="<?= $listing->getListingID(); ?>" <? if( $listing->getListingID() == $contents["listingID"] ): ?> selected <? endif; ?> ><?= $listing->getListingName(); ?></option>
											<? endforeach; ?>
										</select>
										<? if( 0 < $contents['msgList']->getNumberOfNewMessagesPerListing($contents["listingID"]) ) : ?>
										 	<?= $contents['msgList']->getNumberOfNewMessagesPerListing($contents["listingID"]); ?> new inquir<? if( 1 < $contents['msgList']->getNumberOfNewMessagesPerListing($contents["listingID"]) ): ?>ies<? else: ?>y<? endif; ?>
										<? endif; ?> 		
									</th>
								</tr>
							<? endif; ?>		
							<tr class="message_head">
								<th scope="col" width="10">
									<input type="checkbox" name="chkMsgCheckAll"  id="chkMsgCheckAll1" onclick="toggleCheckAll('chkMessageID[]', 'chkMsgCheckAll1', 'chkMsgCheckAll'); javascript: chkBoxTicked()"  title="Check/Uncheck All" />
								</th>
								<th scope="col"<? if( $contents["active"] == constants::TRASH) : ?>width="35%"<? else: ?>width="50%"<? endif;?>>Subject</th>
								<? if( $contents["active"] == constants::TRASH ) : ?>
									<th scope="col">From</th>
								<? endif; ?>	
								<th scope="col">
									<?= ( in_array($contents["active"],array(constants::INBOX,constants::INQUIRY)) ) ? "From" : "To"  ?>
								</th>
								<th scope="col" width="140">Date</th>	
							</tr> 
							<? foreach ($contents["messageList"] as $message): ?>
								<?  
									$class = '';
									if (!$message->isRead()){
										$class = 'class="unread"';
									}
								?>
								<tr <?= $class ?>>
									<td>
										<input type="checkbox" name="chkMessageID[]" value=<?= $message->getAttributeID(); ?> onclick='javascript: chkBoxTicked()' />
									</td>
									<td>
										<? if (strcmp($contents["active"],constants::INBOX) == 0 && !$message->isRead()) : ?><strong><? endif; ?>
										<a href="<?= "messages.php?view&amp;active=" . $contents['active'] . "&amp;messageID=" . $message->getAttributeID() ; ?>"><? if( strlen(trim($message->getTitle()))) echo stripslashes($message->getTitle()); else echo "[none]"; ?></a>
										<? if (strcmp($contents["active"],constants::INBOX) == 0 && !$message->isRead()) : ?></strong><? endif; ?>
									</td>
									<? if( $contents["active"] == constants::TRASH ) : ?>
										<td>
											<?= displayLink($message->getSource(),$contents["ownerID"]); ?>
										</td>
									<? endif; ?>	
									<td>
										<?	$dest = $message->getDestination();
											if ( in_array($contents["active"],array(constants::INBOX,constants::INQUIRY)) ) : ?>  
												<?= displayLink($message->getSource(),$contents["ownerID"]); ?>
											<? else:
												$arrDestination = array(); 
												foreach( $dest as $destTrav )
													$arrDestination[] = displayLink($destTrav,$contents["ownerID"]); ?>
												<?= implode(", ",$arrDestination); ?>
											<? endif; ?>		
									</td>
									<td>
									<?= $dateobj->set($message->getCreated())->friendlyFormat(); ?>
									</td>		
								</tr>
							<? endforeach; ?>										
						</table>
						</div>
						
						<div class="actions">
							<input type="hidden" name="hdnReferer" value="<?= $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>" />
							<input type="hidden" name="hdnActive" value="<?= $contents['active']; ?>" />
							<input type="hidden" name="hdnPage" value="<?= $contents['page'] ?>" />
							<? if( $contents["active"] == constants::TRASH ): ?>
								<input type="submit" name="btnDelete" value="Delete" id='btnDelete' onclick="javascript: return confirm('Are you sure you want to delete the selected messages?');" class="submit" disabled='true' />
							<? else: ?>	
								<input type="submit" name="btnTrash" id='btnTrash' value="Move Selected Items to Trash" class="submit" disabled='true' />
							<? endif; ?>	
						</div>

					</form>							
					<?= $sub_views['MESSAGE_PAGING']->render(); ?>
					</div>
				<? else :?>
					
					<div class="content">
						<p class="help_text">Your <?= ( $contents["active"] == constants::SENT_ITEMS ) ? "Sent Items" : ucfirst($contents["active"]);  ?> folder is empty.</p>
					</div>
				<? endif; ?>				
				</div>				
			</div>		
			<div id="narrow_column">
				<ul class="actions">
					<li><a href="messages.php?compose" class="button"> Compose New Message </a></li>
				</ul>
				<?= $sub_views['MESSAGE_NAVIGATION']->render(); ?>
			</div>
</div>
