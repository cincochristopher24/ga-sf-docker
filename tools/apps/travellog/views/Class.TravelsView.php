<?php
/*
 * Created on 03 19, 07
 * @author Kerwin Gordo
 * Purpose: Display travels
 */
 
 require_once("Class.Template.php");
 require_once("Class.Connection.php");
 require_once("Class.Recordset.php");
 require_once('travellog/model/Class.JournalPrivacyPreference.php');
 require_once('travellog/model/Class.HelpText.php');

 class TravelsView {
 
 	//private $mTravels = array();
 	protected $mOwner = null;
 	protected $mTemplatePath = 'travellog/views/'; 
 	protected $mTravelerID = 0;
 	protected $canAdd = false;
 	protected $grpOfficers = array();			// an array of travelerID for admin/staff/moderators of a group
 	
 	/**
 	 * constructor
 	 * @param Traveler|AdminGroup owner
 	 */
 	function TravelsView($owner) {
 		$this->mOwner = $owner;
 		$this->initialize();
 	}

	/**
	 * sets the path of the html template to display the comment
	 * @param $tPath a string indicating an absolute or relative path
	 */ 	
 	function setTemplatePath($tPath){
 		$this->mTemplatePath = $tPath;
 	}
 	
 	/**
 	 * set the travelerID of the logged user
 	 */
 	function setTravelerID($travelerID) {
 		$this->mTravelerID = $travelerID;
 	}
 	
 	function render(){ 		 		
 		if ($this->mOwner != null){
 			
			$jpp = new JournalPrivacyPreference();
			if ($this->mOwner instanceof Traveler)
				$ownerTravID = $this->mOwner->getTravelerID();
			else
				$ownerTravID = $this->mOwner->getAdministrator()->getTravelerID();
			
			$privacycriteria = $jpp->getPrivacyCriteria2s($this->mTravelerID , $ownerTravID );

			if ($this->mOwner instanceof Traveler && $this->mTravelerID == $this->mOwner->getTravelerID())
				$travels = $this->mOwner->getTravels($privacycriteria, 1);
			elseif ($this->mOwner instanceof AdminGroup && $this->mTravelerID == $ownerTravID)
				$travels = $this->mOwner->getTravels($privacycriteria, null, true); // true = owner is viewing	
			else
				$travels = $this->mOwner->getTravels($privacycriteria);
			
			$tlogs = TravelLog::getTravellogs($this->mOwner);	
			if ($this->mOwner instanceof AdminGroup)		
				$tlogs = array_merge($tlogs,$this->loadTravellogsOfMembers($travels));
				
 			$tt = new Template();
 				
 			//$tcount = count($travels);
 			$hasMoreJournals = false;
 				 			
 			
	 			
	 			$tt->set('canAdd',$this->canAdd);
	 			if ($travels == null || count($travels) == 0 ){
	 				$ht = new HelpText();
	 				$tt->set('helptext',$ht->getHelpText('dashboard-traveljournals'));
	 			}
	 			
	 			if ($this->mOwner instanceof Traveler && $this->mTravelerID == $this->mOwner->getTravelerID()){	
					$tt->set('addTravelLink','/journal.php?action=add');
					$this->canAdd = true;
					$tt->set('canAdd',$this->canAdd);							
	 			}
	 			
	 			/**
	 			 * setting of context where this component is viewed
	 			 */
	 			
	 			/*if ($this->mOwner instanceof Traveler && $this->mTravelerID > 0)
	 				$tt->set('viewContext','Passport');	 						
	 			
	 			if ($this->mOwner instanceof AdminGroup )
	 				$tt->set('viewContext','Group'); */
	 			
	 			/**
	 			 * end setting of context
	 			 */

	 			if ($this->mOwner instanceof AdminGroup ){	
										
					if (array_search($this->mTravelerID,$this->grpOfficers)) {					 					
						$tt->set('addTravelLink','/journal.php?action=add&amp;contextGroupID='.$this->mOwner->getGroupID());
						$this->canAdd = true;
						$tt->set('canAdd',$this->canAdd);		
					}		
										
	 			}	 						
	 			
	 			$travels = $this->filterTravels($travels);
	 			$tlogs = $this->filterTravelEntries($tlogs);
	 			//$hasTravelEntriesAr = $this->createJournalHasContentsArray($travels,$tlogs);
	 			$canEditJournalAr = $this->createCanEditJournalArray($travels);
	 			//echo '<pre>';
	 			//foreach($canEditJournalAr as $j){
	 			//	echo $j . '><br/>';
	 			//}
	 			//exit;
	 			
	 			$tt->set('canEditJournalAr',$canEditJournalAr);
	 			//$tt->set('hasTravelEntriesAr',$hasTravelEntriesAr);
	 			$tt->set('tlogs',$tlogs);
	 			$tt->set('travels_array',$travels);	 					
	 			$tt->out($this->mTemplatePath . 'tpl.IncTravelSummary.php');	 				 					 			 	
 		}
 	}
 	
 	/**
 	 * create an array of travelids . This tells if the journal associated with the travel id
 	 * 	has journal entries 
 	 */
 	protected function createJournalHasContentsArray($travels,$tlogs) {
 		$hasTLogs = array();
 		foreach($travels as $travel) {
 			foreach ($tlogs as $tlog){
 				if ($travel->getTravelID() == $tlog->getTravelID()) {
 					$hasTLogs[''.$travel->getTravelID()] = $travel->getTravelID(); 					
 					break;	 
 				}	 				 				
 			}
 		} 		
 		return $hasTLogs;
 	}
 	
 	
 	protected function createCanEditJournalArray($travels){
 		$canEditJournalAr = array();				// an assoc array travelID -> boolean to determine if user can edit travel(journal)
 		
 		// we are in the context of passport/profile page therefore there will be no group journals here
 		if ($this->mOwner instanceof Traveler) {
 			if ($this->mTravelerID == $this->mOwner->getTravelerID()){
 				foreach($travels as $travel){
 					$canEditJournalAr[$travel->getTravelID()] = 1; 
 				}
 			} else {
 				foreach($travels as $travel){
 					$canEditJournalAr[$travel->getTravelID()] = 0; 
 				}
 			}
 		}
 		
 		// we are in the context of viewing this component in Group so included journals are journals created by group and journals created by 
 		//    members of the group
 		if ($this->mOwner instanceof AdminGroup) {
 			foreach($travels as $travel){
 				$jAuth = $travel->getOwner();
 				if ($jAuth instanceof Traveler){
 					if ($jAuth->getTravelerID() == $this->mTravelerID){
 						$canEditJournalAr[$travel->getTravelID()] = 1;
 					} else {
 						$canEditJournalAr[$travel->getTravelID()] = 0;
 					}
 				}
 				
 				if ($jAuth instanceof AdminGroup && $jAuth->getGroupID() == $this->mOwner->getGroupID() ){ 					 					
 					if (array_search($this->mTravelerID,$this->grpOfficers)){
 						$canEditJournalAr[$travel->getTravelID()] = 1;
 					} else {
 						$canEditJournalAr[$travel->getTravelID()] = 0;
 					}
 				} 				
 			} 			
 		}
 		 		 	 				
 		return $canEditJournalAr;
 	}
 	
 	/**
 	 * Note: this is a rule function
 	 * remove some travels on the array which should not be displayed.
 	 * 
 	 */
 	protected function filterTravels($travels){
 		$i = 0;
 		foreach ($travels as $travel){
	 		if ($this->mTravelerID == 0){
	 			// public view
	 			if (!$travel->getPublish()){
	 				unset($travels[$i]);		 				
	 			}	 	
	 		} else {
	 			// user is logged in but is not admin
	 			if (!$this->canAdd && !$travel->getPublish()){	 			
	 				unset($travels[$i]);	 				
	 			}
	 		}
	 		$i++;
 		}
 		 		 			
 		return $travels;
 	}
 	
 	
 	/**
 	 * Note: this is a rule function
 	 * remove some travel entries on the array which should not be displayed.
 	 * 
 	 */
 	protected function filterTravelEntries($tlogs){
 		$i = 0;
 		foreach ($tlogs as $tlog){
	 		if ($this->mTravelerID == 0){
	 			if (!$tlog->getPublish()){
	 				unset($tlogs[$i]);		 				
	 			}	 	
	 		} else {
	 			if (!$this->canAdd && !$tlog->getPublish()){	 			
	 				unset($tlogs[$i]);	 				
	 			}
	 		}
	 		$i++;
 		}
 		 		 			
 		return $tlogs; 		
 	}
 	
 	protected function initialize(){
 		// set group officers
 		if ($this->mOwner instanceof AdminGroup ){	
			$this->grpOfficers[] = 'padding';      // we insert a value at 0 index so that 0 offset will not be used and be mistaken as false when calling array_search
			$this->grpOfficers[] = $this->mOwner->getAdministrator()->getTravelerID();
			$moderators = $this->mOwner->getModerators();
			foreach($moderators as $moderator){
				$this->grpOfficers[] = $moderator->getTravelerID();
			}
			$staffers = $this->mOwner->getStaff();
			foreach($staffers as $staff){
				$this->grpOfficers[] = $staff->getTravelerID();
			}
				
 		}
 		 		
 	}
 	
 	
 	protected function loadTravellogsOfMembers($travels){
 		$members = array();
 		foreach ($travels as $travel ){
 			if ($travel->getOwner() instanceof Traveler){
 				$members[$travel->getOwner()->getUserName()] = $travel->getOwner();
 			}
 		} 
 		
 		$memTLogs = array();
 		foreach($members as $member){
 			$memTLogs = array_merge($memTLogs,TravelLog::getTravellogs($member));
 		}
 		
 		return $memTLogs;	
 	}
 	
 	
 	/**
 	 * render Journal Entries of a given Travel id of this traveler($this->mOwner)
 	 */
 	function renderAjTravelEntries($travelId){
 		require_once('travellog/model/Class.TravelLog.php');
 		$template = new Template();
 		
 		$entries = TravelLog::getTravellogs($this->mOwner);
 		 		
 		$template->set('entries',$entries); 		
 		$template->set('travelId',$travelId);
 		$template->set('traveler',$this->mOwner);
 		
 		return $template->fetch('travellog/views/tpl.AjTravelEntries.php'); 		
 	} 
 	
 	
 	
 }
  
?>
