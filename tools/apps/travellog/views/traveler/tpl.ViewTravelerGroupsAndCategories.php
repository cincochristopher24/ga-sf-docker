<?php
  Template::setMainTemplateVar('title', $title);
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', $page_location);
	Template::includeDependentJs('/js/subgroup.js', array('bottom' => true));
?>

<?php echo $profile_component->render(); ?>

<?php $sub_navigation_component->show(); ?>

<div id="content_wrapper" class="layout_2">
		<div id="wide_column">
			
			<div id="groups_and_clubs" class="section">	
				<h2><span><?php echo $main_title; ?></span></h2>
				<?php echo $data_component->render(); ?>
			</div>
		</div>
		
		<div id="narrow_column">
      <?php echo $search_form_component->render(); ?>
		</div>
</div>

<!--added by ianne - 11/20/2008 success message after confirm-->
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#keyword').keypress(function(e){
      if(13 == e.keyCode) {
        SubGroupManager.search(jQuery('#search_by').val(), jQuery('#keyword').val());
      }
    });
  });
  
  <?php if(isset($success)): ?>
    jQuery(document).ready(function(){
      CustomPopup.initPrompt("<?php echo $success; ?>");
			CustomPopup.createPopup();
		});
  <?php endif; ?>
</script>