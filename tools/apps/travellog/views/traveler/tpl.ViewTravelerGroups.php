<?php
  Template::setMainTemplateVar('title', $title);
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', $page_location);
?>
<div id="traveler_top_wrapper">
	<div id="top_profile">
  		<div id="intro">
			<?php echo $profile_component->render(); ?>
		</div>
	</div>
</div>

<?php $sub_navigation_component->show(); ?>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
		<div id="groups_and_clubs" class="section">			
			<h2> <span><?php echo $main_title ?></span> </h2>				
			<div class="content">
				<div id="groups_list_main">
          <?php if (0 == $groups_count): ?>	
            <?php if ($is_searching): ?>
              <div class="containerbox">Your search has yielded no results.</div>
            <?php elseif ($is_owner AND $is_cobrand): ?>
              <p class="help_text">
                <span>You are not a member of any group yet</span>.<br /> Check out the <a href="/subgroups/category_search/"><strong>groups page</strong></a>!
              </p>
            <?php elseif ($is_owner): ?>
              <p class="help_text">
                <span>You are not a member of any group yet</span>.<br /> Check out the <a href="/group.php"><strong>groups page</strong></a> or <a href="/group.php?mode=add"><strong>create your own</strong></a>!
              </p>
            <?php else: ?>
              <p class="help_text">
                The traveler has no groups yet. 
              </p>           
            <?php endif; ?>	
          <?php endif; ?>
          
          <ul>
            <?php foreach($group_list as $group): ?>
              <?php 
                $categories = $group->getGroupCategory();
                $type = "";
                foreach ($categories as $category) 
                {
                  $type .= $category->getName().", ";
                }
					  
                $type = preg_replace('/, $/','',$type);		
              ?>
			
              <li>	
                <a class="group_thumb" href="<?php echo $group->getFriendlyURL(); ?>">
                  <img src="<?php echo $group->getGroupPhoto()->getPhotoLink('standard'); ?>" alt="<?php echo $group->getName(); ?>" >
                </a>					
				
                <div class="groups_info">
                  <strong>
                    <a href="<?php echo $group->getFriendlyURL(); ?>">
                      <?php echo $group->getName(); ?>
                    </a>
                  </strong>
                  
                  <p>
                    <?php echo StringFormattingHelper::wordTrim($group->getDescription(), 40); ?>
                    <?php if (40 < count(explode(' ', strip_tags($group->getDescription())))) : ?>
                      <?php echo '&#8230;'; ?>
                    <?php endif ; ?>
                     
                    <a href="<?php echo $group->getFriendlyURL(); ?>" title="Read more about the <?php echo $group->getName() ?> travel community">
                      Read More
                    </a>
                  </p>
					
                  <?php if (0 < count($categories)): ?>
                    <p>
                      <span class="gray">
                        <strong>Type :</strong>
                        <?php echo $type; ?>
                      </span>
                    </p>
                  <?php endif; ?>

                  <?php if (Group::ADMIN ==  $group->getDiscriminator()) : ?>
                    <?php $gpp = $group->getPrivacyPreference(); ?>
                    <?php $subgroups = $group->getSubGroups(new RowsLimit(3), null, false, $keyword); ?>
                    <? if (0 < count($subgroups) AND ((NULL != $loggedUser && $group->isMember($loggedUser)) || $gpp->getViewGroups()) ): ?>
                      <? $cntr = 0; ?>
                      <? $start = TRUE; ?>		
                      <? $showsub = TRUE; ?>
                  <p>
                    <? foreach ($subgroups as $eachsub ) : ?>
                      <? if ((NULL != $loggedUser && TRUE == $eachsub->isMember($loggedUser) )) : ?> 
                        <? if ($start) : ?>
							<?php if($is_owner): ?>
								<strong>You are a member of the following Sub Group:</strong><br />
							<?php else: ?>
								<strong><?php echo $loggedUser->getUsername(); ?> is a member of the following Sub Group:</strong><br />
							<?php endif; ?>
                        <? else : ?>,
                        <? endif; ?>
					              
                        <a href="<?= $eachsub->getFriendlyURL()?>">
                          <?= $eachsub->getName(); ?>
                        </a>
		                    
                        <? $cntr++ ;?>

                         <? if (3 == $cntr): ?>
                           <?= ' ... ' ; ?>
                           <? break; ?>
                         <? endif; ?>
                         								    
                         <? $start = FALSE; ?>
                       <? endif; ?>
                     <? endforeach; ?>
                   </p>
                 <? endif; ?> 
               <? endif; ?>
				  		 															
               <?php if (trim($group->getGroupMembershipLink('mygroups')) !== ''): ?>
                 <p class="action">
                   <?php echo $group->getGroupMembershipLink('mygroups',$current_page) ?>
                 </p>
               <?php endif; ?>
             </div>
           </li>	     
         <?php endforeach; ?>
         </ul>
    
         <?php $paging->showPagination(array('label'=>' Groups')); ?>
        </div>
      </div>
    </div>		
  </div>
  
  <?php if ($is_owner) : ?>
	  <div id="narrow_column">
		  <div class="section">
			  <div class="content">
          <ul class="form" id="search_form">
	          <li>
              <form action="group.php?travelerID=<?php echo $travelerID; ?>" method="POST" />
                <label for="keyword">Search</label>
                <input type="text" class="text" id="keyword" name="keyword" value="<?php echo $keyword ?>" style="width:262px;">

				        <span class="actions">		
					        <input type="submit" value="Search group" title="Search" name="btnSearchName" id="btnSearchName" class="submit"/>
				        </span>
				      </form>
            </li>
          </ul>	

          <div id="mygroups_action">
            <ul class="actions">
              <li>
                <a class="button" href="group.php">Join a Group</a>
              </li>
              <?php if ($is_cobrand): ?>
              <?php else: ?>
                <li>
                  <a class="button" href="group.php?mode=add&amp;advisor">Create a new Group</a>
                </li>
  				<?/*
                <li>
                  <a class="button" href="group.php?mode=add&amp;advisor">Create a new Advisor Group</a>
                </li>
				*/?>
              <?php endif; ?>
            </ul>			
          </div>
			  </div>
		  </div>
	  </div>
  <?php endif; ?>	
</div>

<!--added by ianne - 11/20/2008 success message after confirm-->
<?php if(isset($success)): ?>
	<script type="text/javascript">
		window.onload = function(){
			CustomPopup.initPrompt("<?php echo $success; ?>");
			CustomPopup.createPopup();
		}
	</script>
<?php endif; ?>