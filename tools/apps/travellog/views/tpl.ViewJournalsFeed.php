<!-- Start HTML Mark-up for Feeds -->  		
		<!-- Begin #journals_feed Section -->	
		<div id="journals_feed" class="section" >	
			<a name="anchor"></a>	
			<h2>
				<span>Journals Feed</span>
			</h2>	
			
			<div class="section_small_details">
				Showing the latest journal updates by members
				<input type="button" id="refreshBtn" value="Refresh Feed" onclick="window.location.reload();"/>
			</div>
		
			<div id="content">
				<ul class="users activity_feed">
					<!-- Start Feed -->
					<?foreach($feeds as $feed):?>
						<?php if($feed->itemExists()): ?>
							<li class="feed">
								<a class="thumb" href="<?=$feed->getDoerLink()?>" title="View <?=$feed->getDoerUserName()?>'s profile"> 
									<img src="<?=$feed->getDoerPhotoThumbnail()?>" alt="Member thumbnail" width="65" height="65" />
								</a>						
								<div class="details">
									<?=$feed->getItemTitle()?>
									<?=$feed->getItemContent()?>
									<div class="feed_timestamp"><?=$feed->getItemDate()?></div>
								</div>														
							</li>
						<?php endif; ?>
					<?endforeach;?>
					<!-- End Feed -->	
				</ul>							
			</div>
			
			<?if(!count($feeds)):?>
				<div class="help_text" style="margin:5px 0;">There are no updates on member journals. Please check back later.</div>
			<?endif;?>
			<!-- End Content -->		
		</div>
		<? if (isset($pagingComp)):?>	
		    	<? $pagingComp->showPagination() ?>
		<? endif; ?>		
		<!-- End #journals_feed Section -->


<!-- End HTML Mark-up for Feeds -->