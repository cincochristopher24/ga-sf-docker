<?php
	/*
	 * tpl.FrmAdvisorRegister.php
	 * Created on Mar 22, 2007
	 * created by marc
	 */

?>

	<div id="content_wrapper" class="registration">
		<div id="wide_column">
			<div id="page_form" class="formbox">
				<div class="title">
						<h1>Advisor&#8217;s Registration</h1>
						<p><strong>Thank you for your interest in joining GoAbroad Network</strong>. You are one step away from sharing your travels and connecting with the GoAbroad Network community. We just need your basic info to get you started. Thank you!</p>
				</div>
				<div class="formInfoBox jLeft"><span class="required">*</span> <strong>Required fields</strong></div>
				<? if( strlen($contents["strErrorMessages"]) ): ?>
					<p class="help_text"><?= $contents["strErrorMessages"]; ?></p>
				<? else: ?>	
					<form name="register" action="register.php" method="post" onsubmit="document.getElementById('btnAdvisorRegister').disabled	 = true" class="interactive_form clear" id="advisor_registration_form" >

						<ul class="form">
							<li>
								<label class="inpt_box" for="txtAdvisorName">Name of Advisor Group : <span class="required">*</span></label>
								<? if( array_key_exists(constants::EMPTY_GROUPNAME,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::EMPTY_GROUPNAME]?>
									</p>
								<? elseif( array_key_exists(constants::GROUPNAME_ALREADY_USED,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::GROUPNAME_ALREADY_USED] ?>
									</p>
								<? elseif( array_key_exists(constants::INVALID_GROUPNAME_FORMAT,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::INVALID_GROUPNAME_FORMAT] ?>
									</p>
								<? endif; ?>
								<p class="supplement">This can be the name of your organization or institution, or your name, if you are a professional administering this group.</p>
								<input type="text" class="text" name="txtAdvisorName" id="txtAdvisorName" value="<?= $contents["instname"] ?>" />
							</li>
							<li>
								<label class="inpt_box">First Name: <span class="required">*</span></label>
								<input type="text" class="text" name="txtFirstName" id="txtFirstName" value="<?= $contents["firstname"]; ?>" />
								<? if( array_key_exists(constants::EMPTY_FIRSTNAME,$contents["arrErrorMessages"]) ): ?>
								<p class="supplement error">
									 <?= $contents["arrErrorMessages"][constants::EMPTY_FIRSTNAME]; ?>
								</p>
								<? endif; ?>
							</li>	
							<li>
								<label class="inpt_box">Last Name: <span class="required">*</span></label>
								<input type="text" class="text" name="txtLastName" id="txtLastName" value="<?= $contents["lastname"]; ?>" />
								<? if( array_key_exists(constants::EMPTY_LASTNAME,$contents["arrErrorMessages"]) ): ?>
								<p class="supplement error">
									 <?= $contents["arrErrorMessages"][constants::EMPTY_LASTNAME] ?>
								</p>	 
								<? endif;?>
							</li>
							<li>
								<label class="inpt_box">UserName : <span class="required">*</span></label>
								<input type="text" class="text" name="txtAdvisorUsrName" id="txtAdvisorUsrName" value="<?= $contents["advisorUsrname"] ?>" maxlength='25' />
								<? if( array_key_exists(constants::EMPTY_USERNAME,$contents["arrErrorMessages"]) ):?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::EMPTY_USERNAME] ;?> 
									</p>
								<? elseif( array_key_exists(constants::INVALID_USERNAME_FORMAT,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::INVALID_USERNAME_FORMAT] ?>
									</p>
								<? elseif( array_key_exists(constants::USERNAME_ALREADY_USED,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::USERNAME_ALREADY_USED] ?>
									</p>
								<? endif; ?>
							</li>
							<li class="column">
								<label class="inpt_box">Password : <span class="required">*</span></label>
								<? if( array_key_exists(constants::INVALID_PASSWORD_FORMAT,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">Password must be at least 6 characters long</p>
								<? else: ?>	
									<p class="supplement">Password must be at least 6 characters long</p>
								<? endif; ?>
								<input type="password"  class="text" name="pwdAdvisorPasWrd" id="pwdAdvisorPasWrd" value="" />
							</li>
							<li class="column">
								<label class="inpt_box">Confirm password : <span class="required">*</span></label>
								<? if( array_key_exists(constants::PASSWORD_CONFIRMATION_FAILED,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::PASSWORD_CONFIRMATION_FAILED] ?>
									</p>
								<? endif;?>
								<input type="password" class="text" name="pwdConfirmAdvisorPasWrd" id="pwdConfirmAdvisorPasWrd" value="" />
							</li>
							<li>
								<label class="inpt_box">E-mail address : <span class="required">*</span></label>
								<? if( array_key_exists(constants::EMPTY_EMAIL_ADDRESS,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::EMPTY_EMAIL_ADDRESS] ?>
									</p>
								<? elseif( array_key_exists(constants::INVALID_EMAIL_FORMAT,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::INVALID_EMAIL_FORMAT] ?>
									</p>
								<? elseif( array_key_exists(constants::EMAIL_ALREADY_USED,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement error">
										<?= $contents["arrErrorMessages"][constants::EMAIL_ALREADY_USED] ?>
									</p>
								<? endif; ?>
								<p class="supplement">This email address must be valid and working. We will send a confirmation email to this address for you to complete your registration.</p>
								<input type="text"  class="text" name="txtAdvisorEmail" id="txtAdvisorEmail" value="<?= $contents["advisorEmailAdd"] ?>" />
							</li>
							<li class="confirm">
								<input type="checkbox" name="chkAdvisorSubscribe" id="chkAdvisorSubscribe" <? if($contents["subscribeNL"]):?>checked="checked"<? endif; ?> />
								<label for="chkAdvisorSubscribe" class="subscribes inline">Subscribe me to the GoAbroad.com Newsletter.</label>
								<p class="supplement">We never sell, lease nor rent our list to ANY outside companies. Please view our <a href="/privacy-policy.php" rel="popup">privacy policy.</a></p>
							</li>
							<li class="confirm">
								<input type="checkbox" name="chkAdvisorAgree" id="chkAdvisorAgree" <? if($contents["agreeTerm"]): ?>checked<? endif; ?> />
								<label for="chkAdvisorAgree" class="subscribes inline">I accept the <a href="terms.php?advisor" rel="popup" >terms
								of use</a>. <span class="required">*<span></label></li>

								<? if( array_key_exists(constants::DISAGREE_TERMS,$contents["arrErrorMessages"]) ):?>
									 <p class="supplement">
									 	<?= $contents["arrErrorMessages"][constants::DISAGREE_TERMS] ?>
									 </p>	
								<? endif; ?>	 
							</li>	
							<li class="captcha">
								<label for="securityCode">Security Code : <span class="required">*</span></label>
								<input type="text" id="securityCode" name="securityCode" value="" class="text" />
								<p class="supplement">Please type the code that you see in the image below:</p>
								<img class="captcha_image" id="captchaImg" src="<?= $contents["security"]["src"]; ?>" alt="Captcha" />
								<p>
									<a href="javascript:void(0)" onclick="Captcha.load()" >[Refresh Image]</a>
									<a href="captcha-info.php?location=register" title="Why do you need to enter this code?" onclick="window.open('captcha-info.php?location=register', 'captchainfo', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable,width=500,height=235, top=' + ((screen.width/2)- 1000) + ', left=20'); return false;">
									<img src="http://images.goabroad.com/images/icons/help.gif">What's this?</a>
									<input type="hidden" id="encSecCode" name="encSecCode" value="<?= $contents["security"]["code"]; ?>">							
							    </p>

								<? if( array_key_exists(constants::SECURITY_CODE_MISMATCH,$contents["arrErrorMessages"]) ): ?>
									<p class="supplement">
							    		<?= $contents["arrErrorMessages"][constants::SECURITY_CODE_MISMATCH] ?>
							    	</p>
								<? endif; ?>

							</li>	
							<li class="actions">
								<input type="submit" name="btnAdvisorRegister" id="btnAdvisorRegister" value="Register" class="submit" />
								<input type="hidden" name="btnAdvisorRegister" value="Register" />
								<input type="hidden" name="hdnGcID" value="<?= $contents["gcID"]; ?>" />
								<input type="hidden" name="hdnRef" value="<?= $contents["ref"]; ?>" />
								<input type="hidden" name="hdnRegKey" value="<?= $contents["regKey"]; ?>" />
								<input type="hidden" name="hdnRegID" value="<?= $contents["regID"]; ?>" />
							</li>
						</ul>
					</form>					
				<? endif; ?>

			</div>
		</div>
		<div id="narrow_column">
			<div class="helpextra"></div>
			<div class="helpbox">
					<h2><?= $contents["advisorQuestion"]; ?></h2>
                    <?= HtmlHelpers::Textile($contents["advisorAnswer"]); ?>
            </div>
		</div>
	</div>


<?
	if( strlen($contents["strErrorMessages"]) )
		header("Refresh: 3; URL=http://" . $_SERVER['SERVER_NAME'] . "/"  . $contents["redirectPage"] );
?>