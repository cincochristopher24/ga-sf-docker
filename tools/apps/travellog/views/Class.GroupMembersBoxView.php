<?php

require_once("Class.Template.php");

class GroupMembersBoxView{
	
 	public static $MEMBERS 				= 1;
	public static $STAFF 				= 2;
	
	private $group = null;
	private $memberTabType = false;
	private $pageContext = 0;	// 1 == subgroup context ; 2= parent group context
	private $isAdminLogged = false;
	
	public function setGroup($_group){
		
		$this->group = $_group;
	}
	
	public function setPageContext($_pageContext){
		
		$this->pageContext = $_pageContext;	
	}
	
	
	public function setIsAdminLogged($_isAdminLogged){
		
		$this->isAdminLogged = $_isAdminLogged;	
	}
	
	
 	public function render($_memberTabType = 1){ 		
 		
		$this->memberTabType = $_memberTabType;
		
 		switch ($this->memberTabType){
 			case self::$MEMBERS :
 				$this->renderMembers();
 				break;
 			case self::$STAFF:
 				$this->renderStaff();
 				break;
 			default: 
 				$this->renderMembers();
 				break;				 			
 		}	 
 	}
	

 	/**
 	 * display all (official) members of the group
 	 */
 	private function renderMembers(){
 		require_once('Cache/ganetCacheProvider.php');
 		$cache	=	ganetCacheProvider::instance()->getCache();
 		
 		$loggedID	=	isset($this->loggedUser)?  $this->loggedUser->getTravelerID() : 0 ;
 		$key	=	'GroupMembersBoxView_Members_' . $this->group->getGroupID() . '_' . $loggedID . '_' . $this->memberTabType .
 		 			'_' . $this->isAdminLogged;
 		
 		if ($cache != null && $content = $cache->get($key)) {
 			echo $content;
 			return;
 		}
 		
 		$rowslimitnum = 5;

		$rowslimit  = new RowsLimit($rowslimitnum,0);
		
		$grpMembers = $this->group->getMembers($rowslimit);
		$origCountMembers = $this->group->getMembers(null, null, null,true );
		ob_start();
	
		if (0 < $origCountMembers) {
			
			usort($grpMembers, array('Group','orderByName'));	// sort the members by name	 
			
			$memberTemplate = new Template();		
			$memberTemplate->set("loggeduserID",(isset($this->loggedUser))? $this->loggedUser->getTravelerID() : 0 );						
			$memberTemplate->set("grpMembers", $grpMembers );
			$memberTemplate->set("memberTabType", $this->memberTabType );
			$memberTemplate->set("isAdminLogged", $this->isAdminLogged );	
			$memberTemplate->set("gID", $this->group->getGroupID() );	
			$memberTemplate->out("travellog/views/tpl.IncGroupMembersBox.php");	
		}	
		
		else
			echo '<p class="side_help_text"> There are no members of this group. </p>';		

		$content	=	ob_get_contents();
		ob_end_flush();
		
		if ($cache != null){
			$cache->set($key,$content,array('EXPIRE'=>7200));			
		}
			
 	}
 	
 
	/**
 	 * display all staff of the group
 	 */
 	private function renderStaff(){
 		require_once('Cache/ganetCacheProvider.php');
 		$cache	=	ganetCacheProvider::instance()->getCache();
 		
 		$loggedID	=	isset($this->loggedUser)?  $this->loggedUser->getTravelerID() : 0 ;
 		$key	=	'GroupMembersBoxView_Staff_' . $this->group->getGroupID() . '_' . $loggedID . '_' . $this->pageContext . '_' . 
 		 			$this->memberTabType . '_' . $this->isAdminLogged;
 		
 		if ($cache != null && $content = $cache->get($key)) {
 			echo $content;
 			return;
 		}
 		
 		ob_start();
 		
 		if (1 == $this->pageContext)
			$grpStaff = $this->group->getStaff();
		elseif (2 == $this->pageContext)
			$grpStaff = $this->group->getStaffofSubGroups();
	
		if (count($grpStaff)) {
			
			usort($grpStaff,array('Group','orderByName'));	// sort the members by name	 
			$grpStaff = array_slice($grpStaff, 0, 5);
			
			$memberTemplate = new Template();		
			$memberTemplate->set("loggeduserID",(isset($this->loggedUser))? $this->loggedUser->getTravelerID() : 0 );						
			$memberTemplate->set("grpMembers", $grpStaff);
			$memberTemplate->set("memberTabType", $this->memberTabType );
			$memberTemplate->set("pageContext", $this->pageContext );	
			$memberTemplate->set("isAdminLogged", $this->isAdminLogged );
			$memberTemplate->set("gID", $this->group->getGroupID() );				
			$memberTemplate->out("travellog/views/tpl.IncGroupMembersBox.php");	
		}		
		
		else 	
			echo '<p class="side_help_text"> There are no staff for this group. </p>';

		$content =	ob_get_contents();
		ob_end_flush();

		if ($cache != null) {
			$cache->set($key,$content,array('EXPIRE'=>7200));
		}
		
 	}
}