<?
	/**
	 * tpl.ViewAdminCommPageListView.php
	 * @author : marc
	 * Oct 19, 2006
	 */
	 
	require_once('travellog/model/Class.Listing.php');
	require_once('travellog/helper/DisplayHelper.php');
	require_once('travellog/model/Class.Traveler.php');
	
?>

<div id="admin_messages">
	<? if ( $dispPaging == 1): ?>
		<div class="paging">
			<? $paging->getFirst(); ?> |
			<? $paging->getPrevious() ; ?> |
			<? $paging->getLinks() . "&nbsp;"; ?> |
			<? $paging->getNext() . "&nbsp;"; ?> |
			<? $paging->getlast() . "&nbsp;"; ?>
		</div>
	<? endif; ?> 
	<? 
		foreach( $listListingID as $listingID ) :
	    	$nListing = new Listing($listingID);  
	?>
			<div class="fl_left header_mess" id="msgPack_<?= $listingID; ?>">
				<? if( $inquiryBox->getNumberOfNewMessagesPerListing($listingID) > 0 ) : ?> 
					&nbsp;<?= $inquiryBox->getNumberOfNewMessagesPerListing($listingID); ?>&nbsp;
				<? endif; ?>
			</div>	
			<div class="stretcher header_mess title"><?= $nListing->getListingName(); ?></div>
			<div class="msgPack">
			  	<? 
					foreach($inquiryBox->getStudents($listingID) as $student) : 
						$objStud = new Traveler($student);
			  	?>	
			  			<div class="fl_left header_mess" id="grpMsg_<?= $listingID; ?>_<?= $objStud->getSendableID(); ?>">
				  			<? if( $inquiryBox->getNumberOfNewAdminMessagesPerStudentAndListing($objStud->getSendableID(),$listingID) > 0 ) : ?> 
				  				&nbsp;<?= $inquiryBox->getNumberOfNewAdminMessagesPerStudentAndListing($objStud->getSendableID(),$listingID); ?> &nbsp;
				  			<? endif; ?>
						</div>
			  			<div class="header_mess" onclick="javascript: display_element('all_msg_<?= $objStud->getTravelerID(); ?>_<?= $listingID; ?>'); disp_messages('all_msg_<?= $objStud->getTravelerID(); ?>_<?= $listingID; ?>',<?= $listingID; ?>,<?= $student; ?>,'listView');"><?= $objStud->getTravelerProfile()->getLastName(); ?>, <?= $objStud->getTravelerProfile()->getFirstName(); ?></div>
			  			<div class="display" id="all_msg_<?= $objStud->getTravelerID(); ?>_<?= $listingID; ?>" style="display: none">
						</div>
				<? endforeach; ?>
			</div> 
		<? endforeach; ?>					
	<? if ( $dispPaging == 1): ?>
		<div class="paging">
			<? $paging->getFirst(); ?> |
			<? $paging->getPrevious() ; ?> |
			<? $paging->getLinks() . "&nbsp;"; ?> |
			<? $paging->getNext() . "&nbsp;"; ?> |
			<? $paging->getlast() . "&nbsp;"; ?>
		</div>
	<? endif; ?>

</div>