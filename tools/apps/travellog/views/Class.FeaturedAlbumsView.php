<?php
	require_once("Class.Template.php");
	
	class FeaturedAlbumsView{
		private $mOwner = NULL;
		private $mIsPrivileged = FALSE;
		
		public function __construct($owner=NULL,$privileged=FALSE){
			$this->mOwner = $owner;
			$this->mIsPrivileged = $privileged;
		}
		
		public function render(){
			$tpl = new Template();
			$tpl->set("privileged",$this->mIsPrivileged);
			if( isset($GLOBALS["CONFIG"]) && !$this->mIsPrivileged ){
				$tpl->set("recentUploads",$this->mOwner->getMostRecentlyUploadedPhotos($firstN=3,$GLOBALS["CONFIG"]->getGroupID()));
			}else{
				$tpl->set("recentUploads",$this->mOwner->getMostRecentlyUploadedPhotos());
			}
			$tpl->set("type","traveler");
			$tpl->set("ID",$this->mOwner->getTravelerID());
			$tpl->out("travellog/views/tpl.FeaturedAlbumsView.php");
		}
	}