<?php

	class ViewFeatureChangeNoticeTemplate{
		
		private $mVars = NULL;
		
		function setVars($vars){
			$this->mVars = $vars;
		}
		
		function render(){
			$template = new Template();
			$template->set("vars",$this->mVars);
			$template->out($this->mVars["template"]);
		}
	}