<?php
/*
 * Created on Aug 10, 2006
 * template to display if traveler has latest travel log
 */
?>

<? if (isset($last_log))  : ?>
	
	<h3>
		<a href="<?=$view_log_link?>">
			<?=$last_log_title?>
		</a>
	</h3>
	<div class="meta">
		<span class="date"><?=$last_log_date?></span><br />
		<a href="<?=$last_log_journal_link?>" class="journal_title"><?=$journal_title?></a>
	</div>
	<p class="journal_entry_summary">
		<?=$last_log?>
	</p>
	<div class="actions">			
			<a class="button" id="link_edit_entry" href="<?= htmlspecialchars($edit_log_link) ?>">Edit</a>
			<a class="button" id="link_delete_entry" href=<?= htmlspecialchars($delete_log_link) ?> onclick="return confirm('Are you sure you want to delete this item?')">Delete</a>
	</div>
<? elseif (isset($add_log_link)) : ?>	
	<h2>Latest Journal Entry</h2>
	<p>
		No journal entry found.
	</p>
	<!--<div class="actions">
		<a href="<?= htmlspecialchars($add_log_link) ?>">Add journal entry</a>
	</div>-->
<? endif; ?>
