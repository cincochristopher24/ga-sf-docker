<?php
require_once("travellog/views/Class.AbstractView.php");
class TravelersLocationMessageView extends AbstractView{
	function render(){
		switch( $this->contents['view_type'] ){
			case "6":
				$this->obj_template->set('sub_views', $this->sub_views);
				return $this->obj_template->fetch('tpl.InThatLocationMessageView.php');
			break;
			
			case "7":
				return $this->obj_template->fetch('tpl.PlanningToGoMessageView.php');
			break;
			
			case "8":
				return $this->obj_template->fetch('tpl.HaveBeenMessageView.php');
			break;
			
			default:
				if (0 == $this->contents['travelers_count']) return $this->obj_template->fetch('tpl.NoRecords.php'); 
				//$this->contents['obj_iterator']->rewind();
				//if( !$this->contents['obj_iterator']->valid() ) return $this->obj_template->fetch('tpl.NoRecords.php');
		}
	}
}
?>
