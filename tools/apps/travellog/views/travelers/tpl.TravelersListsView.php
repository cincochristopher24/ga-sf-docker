<?
	if (0 < $contents['locationID']) {
		echo $sub_views['LOCATION_MESSAGE_VIEW']->render();
	}
	$contents['travelers'] = (($contents['travelers'] instanceOf ResourceIterator) && $contents['travelers']->size() <= 0) ? array() : $contents['travelers'];
?>
	<? if (0 == count($contents['travelers'])) : ?>
		<div class="containerbox">No records found!</div>
	<? endif; ?>

	<ul class="users">
<? 
	foreach ($contents['travelers'] as $traveler) :
		if($traveler->isDeactivated()){
			continue;
		}
		$profile = $traveler->getTravelerProfile();
		$countryID = 0;
		$countryID = ($profile->getCity() != NULL )? $profile->getCity()->getCountry()->getCountryID(): 0;
		
		if($profile->getCurrLocationID()){
			if(get_class($profile->getCurrLocation()) == 'Country' )
				$currently_in = $profile->getCurrLocation()->getName();
			else{
				$currLoc = $profile->getCurrLocation();
				$currently_in = $currLoc ? $currLoc->getCountry()->getName() : '';
			}
		}
		else {
			$currently_in = '';
		} 

		$current_travelerID     = $traveler->getTravelerID();
		//$is_friend_already      = ($contents['is_login'] && !$contents['is_advisor'] && $contents['obj_traveler']->isFriend($current_travelerID)) ? true : false;
		//$has_requested          = ($contents['is_login'] && !$contents['is_advisor'] && $contents['obj_traveler']->hasRequested($current_travelerID)) ? true : false;
		$is_friend_already      = ($contents['is_login'] && $contents['obj_traveler']->isFriend($current_travelerID)) ? true : false;
		$has_requested          = ($contents['is_login'] && $contents['obj_traveler']->hasRequested($current_travelerID)) ? true : false;
		$total_entries_videos	  = isset($contents['videos_count'][$current_travelerID]) ? $contents['videos_count'][$current_travelerID ] : 0; //traveler->getCountTravelerVideos();
		$total_entries_writen   = /* isset($contents['journals_count'][$current_travelerID]) ? $contents['journals_count'][$current_travelerID] : 0;*/ $traveler->getCountTravelLogs();
		$total_entries_photos   = isset($contents['photos_count'][$current_travelerID]) ? $contents['photos_count'][$current_travelerID] : 0;//$profile->getCountEntriesPhotos();
		$photo = (isset($contents['primary_photos'][$current_travelerID])) ? new Photo($profile, 0, $contents['primary_photos'][$current_travelerID]) : new Photo($profile, 0);
		$traveler_photo = $photo->getPhotoLink('thumbnail');
		//$traveler_photo         = $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
		$username               = $traveler->getUsername();
		$real_name				= $profile->getFirstName().' '.$profile->getLastName(); 				 
		
		$planning_to_go         = '';
		$dates                  = '';
		if( $traveler->getTravelDestination($current_travelerID) != NULL ){
			$planning_to_go     = $traveler->getTravelDestination($current_travelerID)->getCountryName();
			$dates              = $traveler->getTravelDestination($current_travelerID)->getDateRange();
		}
		
		$hasAuthority = false; 
		
		if ($contents['is_login']) 
			$hasAuthority = Traveler::isMemberOfAnyOfMyGroup($contents['travelerID'],$current_travelerID);	
?>			 
	
			<li>
				<a class="thumb" href="/<?=$username?>"><img src="<?=$traveler_photo?>" alt="<?=$username?>" width="65" height="65"/></a>
				<div class="details">
					<strong><a class="username" href="/<?=$username?>"><?=$username?></a></strong> 
					
					<? if($hasAuthority): ?><strong><?= $real_name ?></strong><? endif; ?>  
					
					<? if($countryID): ?>
						<img src="http://images.goabroad.com/images/flags/flag<?=$countryID?>.gif" width="22px" height="11px" />
					<? endif; ?>		
					<?// if( $contents['is_login'] && !$contents['is_advisor'] && $current_travelerID != $contents['travelerID'] ):?>  
				  	<? if( $contents['is_login'] && $current_travelerID != $contents['travelerID'] ):?>  
						<p>
							<a href="/messages.php?act=messageTraveler&id=<?=$current_travelerID?>">Send Message</a>
			  			<? if(!$is_friend_already && !$has_requested ):?>
						|
								<a href="javascript:void(0)" onclick="CustomPopup.initialize('Add <?=$username?> as friend ?','Are you sure you want to add <?=$username?> as a friend?','/friend.php?action=request&amp;fID=<?=$current_travelerID?>&amp;travelers&amp;page=<?=$contents['page']?>&amp;locationID=<?=$contents['locationID']?>&amp;keywords=<?=$contents['keywords']?>&amp;searchby=<?=$contents['search_by']?>','Add as Friend','1');CustomPopup.createPopup();">Add as a friend</a> 
				  		<? elseif($has_requested): ?>
						|
								<a href="javascript:void(0)" onclick="CustomPopup.initialize('Cancel Request ?','Are you sure you want to cancel your request to add <?=$username?> as a friend?','/friend.php?action=cancel&amp;fID=<?=$current_travelerID?>&amp;travelers&amp;page=<?=$contents['page']?>&amp;locationID=<?=$contents['locationID']?>&amp;keywords=<?=$contents['keywords']?>&amp;searchby=<?=$contents['search_by']?>','Yes','1');CustomPopup.createPopup();">Cancel Request</a> 
				  		<?endif;?>
						</p>
				  	<?endif;?>
			  	</div>
				  					  		   
				<div class="travbox_info">					
					<?if( strlen(trim($currently_in)) ):?>
						<p class="gray"><strong class="dark_gray">Currently in : </strong><?=$currently_in?></p>
					<?endif;?> 
					<?if( strlen(trim($planning_to_go)) && $planning_to_go != $currently_in ):?>  
						<p class="gray">
							<strong class="dark_gray">Planning to go to :</strong>
							<?=$planning_to_go?>
							<?=$dates?>
						</p>
	 				<?endif;?>
					<p class="gray">
						<?=( $total_entries_writen )? $total_entries_writen: 'No';?> Entr<?=($total_entries_writen == 1)? 'y': 'ies'; ?> Written<span> | </span>
						<?=( $total_entries_photos )? $total_entries_photos: 'No'?> Photo<?if($total_entries_photos != 1):?>s<?endif; ?><span> | </span>
						<?=( $total_entries_videos )? $total_entries_videos: 'No'?> Video<?if($total_entries_videos != 1):?>s<?endif; ?>
					</p>
				</div>
			</li>
<? endforeach; ?>	
</ul>
<?=$sub_views['PAGING_LIST_VIEW']->render()?>
<!--added by ianne - 11/20/2008 success message after confirm-->
<?if(isset($success)):?>
	<script type="text/javascript">
		window.onload = function(){
			CustomPopup.initPrompt("<?=$success?>");
			CustomPopup.createPopup();
		}
	</script>
<?endif;?>