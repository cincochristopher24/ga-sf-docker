<?php
class TravelersViewFactory{
	private static $instance = NULL;
	
	private $views = array();
	
	static function getInstance(){ 
		if( self::$instance == NULL ) self::$instance = new TravelersViewFactory; 
		return self::$instance;
	}
	
	function createView($name){
		switch($name){
			case 'TravelersView':
				if( !array_key_exists('TravelersView', $this->views) ){
					require_once('travellog/views/travelers/Class.TravelersView.php');
					$this->views['TravelersView'] = new TravelersView;
				}
				return $this->views['TravelersView'];
			break;
			
			case 'MapView':
				if( !array_key_exists('MapView', $this->views) ){
					require_once('travellog/views/travelers/Class.TravelersMapView.php');
					$this->views['MapView'] = new TravelersMapView;
				}
				return $this->views['MapView'];
			break;
			
			case 'OtherInfoView':
				if( !array_key_exists('OtherInfoView', $this->views) ){
					require_once('travellog/views/travelers/Class.TravelersOtherInfoView.php');
					$this->views['OtherInfoView'] = new TravelersOtherInfoView;
				}
				return $this->views['OtherInfoView'];
			break;
			
			case 'SearchFormView':
				if( !array_key_exists('SearchFormView', $this->views) ){
					require_once('travellog/views/travelers/Class.TravelersSearchFormView.php');
					$this->views['SearchFormView'] = new TravelersSearchFormView;
				}
				return $this->views['SearchFormView'];
			break;
			
			case 'ListsView':
				if( !array_key_exists('ListsView', $this->views) ){
					require_once('travellog/views/travelers/Class.TravelersListsView.php');
					$this->views['ListsView'] = new TravelersListsView;
				}
				return $this->views['ListsView'];
			break;
			
			case 'LocationMessageView':
				if( !array_key_exists('LocationMessageView', $this->views) ){
					require_once('travellog/views/travelers/Class.TravelersLocationMessageView.php');
					$this->views['LocationMessageView'] = new TravelersLocationMessageView;
				}
				return $this->views['LocationMessageView'];
			break;
			
			case 'PagingListsView':
				if( !array_key_exists('PagingListsView', $this->views) ){
					require_once('travellog/views/travelers/Class.PagingListsView.php');
					$this->views['PagingListsView'] = new PagingListsView;
				}
				return $this->views['PagingListsView'];
			break;
		}
	}
}
?>
