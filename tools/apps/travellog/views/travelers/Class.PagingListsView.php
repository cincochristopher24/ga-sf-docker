<?php
require_once("travellog/views/Class.AbstractView.php");
class PagingListsView extends AbstractView{
	function render(){ 
		return ($this->contents['obj_paging']->getTotalPages() > 1)? $this->obj_template->fetch('tpl.PagingListsView.php'): NULL;
	}
}
?>