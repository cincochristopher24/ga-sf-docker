<?$this->doNotUseMainTemplate();?>
<div class="interactive_form"> 
<ul class="form">
	<li>
		<label for="txtTitle">Event Title</label>
		<div id="errTitle" style="color:red"></div>
		<input type="text" name="title" id="title" value="<?=$props['title']?>" size="40" class="text big" />
	</li>
	<li>
		<label for="txtDate">Date</label>
		<div id="errDate" style="color:red"></div>
		<input style="width:109px;" type="text" readonly="true" class="date-pick" name="event_date" id="event_date" value="<?=$props['event_date']?>"/>
		<label for="event_time">Time</label>
		<input style="width:109px;" type="text" class="text" name="event_time" id="event_time" value="<?php echo $props['event_time']?>"/>
	</li>
	<li>
		<label for="timezone">Time Zone</label>
		<select name="timezone" id="timezone" >
			<option value="0" selected >-- Select Time Zone --</option>
			
			<?php foreach( $props['groupedTimezones'] as $id => $each ):?>
				<option value="<?php echo $id?>" <?php if($props['timezone'] == $id) echo 'selected'?> ><?php echo $each?></option>
			<?php endforeach; ?>
			
			<?/*php foreach( $props['groupedTimezones'] as $continent => $v ):?>
				<optgroup label="<?php echo $continent?>" >
					<?php foreach($v as $id => $timezone ):?>
						<option value="<?php echo $id?>" <?php if($props['timezone'] == $id) echo 'selected'?> ><?php echo $timezone?></option>
					<?php endforeach; ?>
				</optgroup>
			<?php endforeach; */?>
		</select>
	</li>
	<li>
		<label for="txaDescription">Event Details</label>
		<div id="errDescription" style="color:red"></div>
		<textarea name="description" id="description" rows="10" cols="80"><?=$props['description']?></textarea>
	</li>
	<?if($props['mode'] == 'add'):?>
	<li>
		<input type="checkbox" name="notification" id="notification" value="1" checked />
		<label for="notification" style="display:inline;">Notify Group Members</label>
	</li>
	<?endif;?>
	<li>
		<fieldset class="choices">
			<legend><span>Show this Event to</span></legend>
			<ul>
				<!--? if($context == 2): ?-->
					<!--li>
						<input type="radio" name="radPublic[]" id="radPublic1" value="1" <? if( $post['radPublic'] == 1 ) echo 'checked=true'; ?> />
						<label for="radPublic1">Public</label>
					</li>
					
					<li>
						<input type="radio" name="radPublic[]" id="radPublic2" value="2" <? if( $post['radPublic'] == 2 ) echo 'checked=true'; ?> />
						<label for="radPublic2">Friends</label>
					</li-->
				<!--? endif; ?-->
				<li>
					<input type="radio" name="display[]" id="display1" value="1" <?if( $props['display'] == 1 ):?>checked="true"<?endif;?> />
					<label for="display1">Public</label>
				</li>
				<?if( $props['group'] instanceOf FunGroup ):?>
				<li>
					<input type="radio" name="display[]" id="display2" value="2" <?if( $props['display'] == 2 ):?>checked="true"<?endif;?> />
					<label for="display2">Group Members and Friends</label> 
				</li>
				<?else:?>
				<li>
					<input type="radio" name="display[]" id="display2" value="2" <?if( $props['display'] == 2 ):?>checked="true"<?endif;?> />
					<label for="display2">Group Members</label> 
				</li>
				<?endif;?>
			</ul>
		</fieldset>
	</li>
	<li class="actions">
		<input type="hidden" name="mode"      id="mode"      value="<?=$props['mode']?>" />
		<input type="hidden" name="gID"       id="gID"       value="<?=$props['gID']?>"  />
		<input type="hidden" name="eID"       id="eID"       value="<?=$props['eID']?>"  />
		<input type="hidden" name="context"   id="context"   value="1"                 />
		<input type="button" name="action"    id="action"    value="Save"   class="submit" onclick="jQuery('input').save()" />&nbsp;&nbsp;
		<input type="button" name="btnCancel" id="btnCancel" value="Cancel" class="submit" onclick="jQuery().cancel()" /> 
	</li>
</ul>
<div class="clear"></div>
</div>

  