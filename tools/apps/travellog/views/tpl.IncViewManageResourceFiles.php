
<?
	

	Template::setMainTemplate($layoutMain);
	
	Template::includeDependentJs("/js/prototype.js"); 
	
	//Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");

	//Template::includeDependentJs("/js/cpaint/lib/cpaint2.inc.js"); 
	//Template::includeDependentJs("/js/resourcefile.js"); 
	Template::includeDependentJs("/js/scriptaculous/effects.js");
	Template::includeDependentJs("/js/scriptaculous/dragdrop.js");
	Template::includeDependentJs("/js/scriptaculous/controls.js");
	
	$style = "<style>	
					.drophover{background-color:#3087B4;color:#ffffff;}
			  </style>";
	
	Template::includeDependent($style); 
	
		
	if($group){
		Template::setMainTemplateVar('page_location', $group);
		Template::setMainTemplateVar('group', $group);
	}
	
	$code =<<<BOF
	<script type = "text/javascript">
		function useLoadingMessage(loadingcontainer,loading){
			var messageZone = document.createElement('img');
	    	messageZone.setAttribute('id', 'messageZone');
	    	messageZone.setAttribute('src', 'images/'+loading);
	    	messageZone.setAttribute('style', 'z-index:-1;');
	    	$(loadingcontainer).appendChild(messageZone);		
		}
	</script>
BOF;

		Template::includeDependent($code);
		
	$subNavigation->show();
	
?>
<div class="resource_manager" id="content_wrapper">
	<div class="area" id="top">			
			<div class="section" id="section">
				<h1><span><?= $headercaption; ?></span></h1>
				
				<div class="content"  id="contn">
						
					<p>To select a file/s to be displayed on the main group or sub-groups, 
						drag and drop the file found on the left window to a 
						group/sub-group on the center window. The selected file/s will 
						automatically be displayed on the right window.
					</p>	
							
					  <table border="0" cellspacing="0" cellpadding="0" id="resource_management_framer">
					    <tr>
					      <td valign="top" id="parent_cell">
					      		
								<div id="parent">
									<h2>All Files</h2>
									<div id="allfilescontainer">
										<?if(count($allresourcefiles)):?>	
											<?foreach($allresourcefiles as $allresourcefiles):?>
												<div id="file_<?=$allresourcefiles->getResourcefileID();?>"  class="allfiles" style="z-index: 0; opacity: 0.99999; top: 0px; left: 0px;">			
														<img  src="images/pdf.gif" alt="" />
															<?  echo $allresourcefiles->getCaption()?$allresourcefiles->getCaption():$allresourcefiles->getFilename();?>																					
												</div>
												
												<script type="text/javascript" charset="utf-8">
													new Draggable('file_<?=$allresourcefiles->getResourcefileID();?>',{ghosting:true, revert:true});
												</script>	
													
											
											<?endforeach;?>
										<?else:?>																
												No Files Found!		
										<?endif;?>	
									</div>				
								</div>
					      
					      </td>
					      
					      <td width="33%" id="subgroups_cell">
					      		
					    		<div id="subgroups">
									<h2>Sub Groups</h2>
									<?if(count($subgroups)):?>	
											<div id="drop_<?if($isparent){echo $admgroup->getGroupID();}else{ echo $parent->getGroupID();}?>" class="subroup_container">
												<a href="javascript:void(0)" onclick="new Ajax.Updater('subcontainer', '/resourcefiles.php', {onLoading:function(request){$('txttmpsubgroup').value=<?=$isparent?$admgroup->getGroupID():$parent->getGroupID();?>;useLoadingMessage('indicator','indicator.gif')}, onComplete:function(request){Element.remove('messageZone')}, parameters:'action=dropview&amp;subgID=<?=$isparent?$admgroup->getGroupID():$parent->getGroupID();?>',evalScripts:true, asynchronous:true})"><?=$isparent?$admgroup->getName():$parent->getName();?> (main group)</a>				
	  										</div>
											
											<div style="clear: both;"></div>
											<script type="text/javascript">Droppables.add('drop_<?=$isparent?$admgroup->getGroupID():$parent->getGroupID();?>', {accept:'allfiles', hoverclass: 'drophover', onDrop:function(element){$('txttmpsubgroup').value=<?=$isparent?$admgroup->getGroupID():$parent->getGroupID();?>;new Ajax.Updater('subcontainer', '/resourcefiles.php', {onLoading:function(request){useLoadingMessage('indicator','indicator.gif')}, onComplete:function(request){Element.remove('messageZone');Element.show('deletecontainer');}, parameters:'action=dropadd&subgroupid=<?=$isparent?$admgroup->getGroupID():$parent->getGroupID();?>&rid=' + encodeURIComponent(element.id),evalScripts:true, asynchronous:true})}});</script>
												
											<?foreach($subgroups as $subgroups):?>
												<div id="drop_<?=$subgroups->getGroupID();?>" class="subroup_container">
													<a href="javascript:void(0)" onclick="new Ajax.Updater('subcontainer', '/resourcefiles.php', {onLoading:function(request){$('txttmpsubgroup').value=<?=$subgroups->getGroupID();?>;useLoadingMessage('indicator','indicator.gif')}, onComplete:function(request){Element.remove('messageZone')}, parameters:'action=dropview&amp;subgID=<?=$subgroups->getGroupID()?>',evalScripts:true, asynchronous:true})"><?=$subgroups->GetName() ?></a>				
		  										</div>
												<div style="clear: both;"></div>
												<script type="text/javascript">Droppables.add('drop_<?=$subgroups->getGroupID();?>', {accept:'allfiles', hoverclass: 'drophover', onDrop:function(element){$('txttmpsubgroup').value=<?=$subgroups->getGroupID();?>;new Ajax.Updater('subcontainer', '/resourcefiles.php', {onLoading:function(request){useLoadingMessage('indicator','indicator.gif')}, onComplete:function(request){Element.remove('messageZone');Element.show('deletecontainer');}, parameters:'action=dropadd&subgroupid=<?=$subgroups->getGroupID();?>&rid=' + encodeURIComponent(element.id),evalScripts:true, asynchronous:true})}});</script>
											
											<?endforeach;?>
												
									<?endif;?>
								</div>  
								
					      </td>
					      
					      <td width="33%;" valign="top" id="subcountainer_cell">
					      		<div id="subcontainer">
					      		
					      		</div>
					      		<input type="hidden" name="txttmpsubgroup" id="txttmpsubgroup">
					      			<script type="text/javascript">Droppables.add('subcontainer', {accept:'allfiles', hoverclass: 'drophover', onDrop:function(element){new Ajax.Updater('subcontainer', '/resourcefiles.php', {onLoading:function(request){useLoadingMessage('indicator','indicator.gif')}, onComplete:function(request){Element.remove('messageZone')}, parameters:'action=dropadd&subgroupid=' + $('txttmpsubgroup').value + '&rid=' + encodeURIComponent(element.id),evalScripts:true, asynchronous:true})}});</script>
									<table border="0" width="100%">
								    	<tr>
								      		<td width="50%"><div id="indicator"></div></td>
								      		<td align="right">
												<div id="deletecontainer" style="display:none">
													<input type="button" name="btndelete" id="btndelete" value="Remove Selected Files" onclick="if(confirm('Do you want to remove this file from this group?'))new Ajax.Updater('subcontainer', '/resourcefiles.php',{onLoading:function(request){useLoadingMessage('indicator','indicator.gif')}, onComplete:function(request){Element.remove('messageZone')}, method:'post', parameters:'action=dropremove&amp;subgID=' + $('txttmpsubgroup').value + '&amp;chkrid='+ encodeURIComponent(Form.serialize('frmsubfiles')),evalScripts:true, asynchronous:true});" />
												</div>
											</td>
								    	</tr>
								  	</table>
	  			      				
					      </td>
					      
					      	
					    </tr>
					  </table>
	  
	  				  <div class="clear"></div>
						
				</div>
				<div class="foot"></div>
			</div>
	</div>
</div>
	
					