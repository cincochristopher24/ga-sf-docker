<?php
/**
 * Created on Apr.17.07
 *
 * @author Daphne 
 * Purpose:  template for View Search / Invite Members Panel (Search, Invite)
 */
  
?>
<div id="member_search" class="member_box_content_inner_wrap">
	<div id="search_forms">	
		<div class="section_smll_widget" id="search_form_graybox"> 
			<div class="head_gray_right">
				<div class="head_gray_left"> </div>
			</div>
			
			<div class="gray_content">
				<? if ($group->isSubGroup() && isset($nonSGMembersofParent) && count($nonSGMembersofParent)) : ?>					
					<!-- begin form add parent group members to subgroup -->
					<form name="frmAddMember" action="invitemembers.php" method="POST">
						<label>Add existing <?=$group->getParent()->getName()?> members to <?= (strlen($group->getName())> 25) ? substr($group->getName(),0, 25).'...': $group->getName() ?>  </label>
						<ul class="form">
							<li>
								<fieldset>
								<span>
									<div style="min-height: 25px; max-height:200px; overflow-y:auto;">
										<?foreach($nonSGMembersofParent as $eachMember):?>
											<input style="margin-top:0; padding:5px 10px 5px 0;" type="checkbox" name="parentMembersID[]" value="<?=$eachMember->getTravelerID()?>" id="parentMembersID"><?=$eachMember->getUserName()?><br />										
										<?endforeach;?>
									</div>																			
								</span>
								<!--><span class="actions">
									<input tabindex="1" type="button" name="btnInviteParentMembers" onclick="manager._setPanelNum(2);manager._inviteAllMultiple('&amp;mode=inviteAllMultiple&amp;grpID=<?=$group->getGroupID()?>');" class="submit" value="Send Invitation" >
								</span>-->
								<input type="hidden" name="mode" value="inviteAllMultiple">
								<input type="hidden" name="grpID" value="<?=$group->getGroupID()?>">
								<input type="hidden" name="viewType" value="8">
								
								<input type="submit" name="submit" value="Send Invitation">
								</fieldset>
							</li>
						</ul>
					</form>
				<? endif; ?>
			
				<!-- begin form search by email -->
					<form name="frmAddMember" action="#" method="POST">
						<label>Invite by Email</label>
						<ul class="form">
							<li>
								<fieldset>
									<span>
																		
										<p id="txtMemail">
											For batch invites enter email addresses below separated by commas.
										</p>
									
										<textarea name="txaEmail" id="txaEmail" tabindex="1"><?=isset($_POST['txaEmail']) ? $_POST['txaEmail']:''?></textarea>
									
									</span>
									
								<? if (!$group->isSubGroup()) : ?>
									<span>									
											<p id="txtSubGroup"> 
												Assign the email addresses I entered above to:
											</p>
										
											<select name="cmbSubGroup" id="cmbSelectSubGroupID" tabindex="3">
												<option value="0"> None </option>
												<?foreach($arrSubGroups as $eachSG):?>
													<option 
														value="<?=$eachSG->getGroupID()?>" 
														<?= (isset($subGroup) && $subGroup->getGroupID() == $eachSG->getGroupID() ) ? 'selected' : '' ?>>
														<?=(27 < strlen($eachSG->getName())) ? substr($eachSG->getName(),0,27) . "..." : $eachSG->getName()?>
													</option>
												<?endforeach;?>
											</select>										
									</span>
								<? endif; ?>
								
								<input type="hidden" name="mode" value="invite">
								<input type="hidden" name="grpID" value="<?=$group->getGroupID()?>">
								<input type="hidden" name="searchType" value="txtEmail">
								<input type="hidden" name="viewType" value="8">
									<span>
										<input type="submit" name="submit" value="Invite" tabindex="4" />
									</span>	
								
								</fieldset>
							</li>
						</ul>
					</form>					
				
				<!-- begin form search by name -->
					<form name="frmAddMember" action="#" method="post" class="interactive_form" id="existingusers_frm">

					<label>Search existing users</label>

					<ul class="form">
						<li class="form_set">
							<fieldset>
								<span>
									<p> Search existing users using either their username, first name, or last name.</p>
								</span>
																
								<span>
									<input tabindex="5" id="txtUserName" name="txtUserName" value="<?=isset($_POST['txtUserName']) ? $_POST['txtUserName'] : ''?>" class="text ga_interactive_form_field" maxlength="50" type="text" />			
									<label for="txtUserName">Username</label>
								</span>
							<span>
								<input tabindex="6" id="txtFirstName" name="txtFirstName" value="<?=isset($_POST['txtFirstName']) ? $_POST['txtFirstName'] : ''?>" class="text ga_interactive_form_field" maxlength="50" type="text" />
								<label for="txtFirstName">First Name</label>
							</span>
							<span>
								<input tabindex="7" id="txtLastName" name="txtLastName" value="<?=isset($_POST['txtLastName']) ? $_POST['txtLastName'] : ''?>" class="text ga_interactive_form_field" maxlength="50" type="text" />
								<label for="txtLastName">Last Name</label>
							</span>

							<input type="hidden" name="mode" value="invite">
							<input type="hidden" name="grpID" value="<?=$group->getGroupID()?>">
							<input type="hidden" name="searchType" value="txtName">
							<input type="hidden" name="viewType" value="8">
							
							<span>
								<input type="submit" name="submit" value="Search">
							</span>
								
							</fieldset>
						</li>
					</ul>
				</form>	
				<div class="clear"> </div>
			</div> <!-- end gray content -->
			
			<div class="foot_gray_right">
				<div class="foot_gray_left"> </div>
			</div>	
		
		</div> <!-- end section small widget-->
	</div> <!-- end search forms -->
	
	<? if (GroupMembersPanel::$SEARCHMEMBERSFORM == $memberTabType) : ?>
		<div id="search_results">						
			
			<? if ( (isset($arrNewlyAdded) and count($arrNewlyAdded) )) { ?>
				<div class="confirmation" id="confirm_invitation">
					<p>
					<? if (isset($arrNewlyAdded) and count($arrNewlyAdded)) { 
						echo 'This group has ';	
						echo (count($arrNewlyAdded)==1) ? ' a new member : ' : ' new members :';
						echo '<br />';
						echo '<i>';
						echo (count($arrNewlyAdded)<=2) ? implode(' and ', $arrNewlyAdded) : implode(', ',array_slice($arrNewlyAdded,0,count($arrNewlyAdded)-1)) . ' and ' . $arrNewlyAdded[count($arrNewlyAdded)-1] ;	
						echo '</i>';
					}?>
					</p>				
				</div>
			<? } ?>
			
			<? if( count($sentInvites['emails']) || count($sentInvites['users']) ): ?>
				<div class="confirmation" id="confirm_invitation">
					<? if( count($sentInvites['emails']) ): ?>
						<p>An invitation was sent to the following email addresses:</p>
						<ul>
							<? foreach( $sentInvites['emails'] as $emailInvite ): ?>
								<li><?= $emailInvite; ?></li>
							<? endforeach;?>
							<div class="clear"> </div>
						</ul>	

						<div class="clear"> </div>	
					<? endif; ?>

					
					<? if( count($sentInvites['users']) ): ?>
						<p>An invitation was sent to the following:</p>
						<ul>
							<? foreach( $sentInvites['users'] as $user ): ?>
								<li><?= $user; ?></li>
							<? endforeach;?>
							<div class="clear"> </div>							
						</ul>	
						<div class="clear"> </div>						
					<? endif; ?>
				</div>
			<? endif; ?>
			
			<?if ($_SERVER['REQUEST_METHOD'] == 'POST') {?>
				<?=$obj_message_panel->render(8)?>
			<? }else { ?>
			<div class="help_text" style="display:none;"> 
					<span style="display:block; font-size:11px; margin-bottom:10px; color: #D7751E;"> Adding new members to your group is easy.</span>  				
					<span style="font-size:11px; line-height:1.5em; color: #D7751E;">Simply enter email addresses in the "Invite by Email" form and select a group if appropriate and click "Invite". Our system will generate a report of any email addresses that are linked to existing GoAbroad.net accounts and those which are not will be invited to create an account via email. On the report page you will have the option to set the group per individual invitation.</span>
			</div>
			<img src="/images/invite_by_mail.jpg">
			<? }?>			
		</div>
		
	<? endif; ?>
	<div id="statusPanel2" class="widePanel" style="display: none;">
	<p class="loading_message">
		<span id="imgLoading">
		<img alt="Loading" src="/images/loading.gif"/>
		</span>
		<em id="statusCaption">Loading data please wait...</em>
	</p>
	</div>
	
	
</div>
<div class="clear"></div>
