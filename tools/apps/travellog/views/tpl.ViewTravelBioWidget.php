<div id="bio_bubble" style="display:<?php echo $editModeMTB ? 'none' : 'block'; ?>;"> 
	<div class="content">
		<p id="mtbLoadingIndicator" style="display: none">
	        Please wait...
	    </p>
		<strong id="profileQuestion"><?php echo $profileQuestion; ?></strong>  
	    <p id="profileAnswer"><?php echo $profileAnswer; ?></p>  		
	</div>

	<div class="footer">
        <input id="qID" type="hidden" value="<?php echo $profileQuestionID; ?>" />
        <a id="moreLink" href="#moreLink" title="More from TravelBio" style="display: <?php echo (count($structAns) > 1) ? 'block' : 'none'?>;"> More </a>
	</div>
    
</div>

<div class="widget_help" id="mtbHelp" style="display:<?php echo $editModeMTB ? 'none' : 'block'; ?>">             
    <strong> Your TravelBio is <span id="percentDone"><?php echo $percentDone?></span>% complete </strong>                     

    <p class="help_text"> 
        You have answered <span id="countAnsweredQuestions"><?php echo $numQ; ?></span> out of <?php echo $totalNumQ; ?> questions<font id="whycomplete"><?if($percentDone < 100):?>, why not complete it?<?endif;?></font>
        Whenever you want to make some changes you can just click <strong>Edit TravelBio</strong>.
    </p>

    <ul class="actions">
        <li class="first">
            <a id="loadMtbLink" href="#loadMtbLink" class="button"> Edit TravelBio  </a>
        </li>
    </ul>
    
</div>    
              
<form id="editMtbForm" name="editMtbForm" action="widget.php?app=mtb&action=edit" method="post" style="display:<?php echo $editModeMTB ? 'block' : 'none'; ?>;" >
	<?php 
	$ctr = 0;
	foreach($profileQuestions as $iQuestion):?>
        <p>
            <div><?= ++$ctr . '. ' . $iQuestion->getQuestion() ?> </div>
            <div>
			<?php 
				if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT): 
					$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
						? htmlentities($structAns[$iQuestion->getQuestionID()][0]->getValue())
						:'';
			?>
				<input type="text" class="text" name="txt_<?= $iQuestion->getQuestionID() ?>" value="<?= $val ?>" onkeydown='return noEnterKey(event);' />
				<?php elseif($iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA): 
						$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
							? htmlentities($structAns[$iQuestion->getQuestionID()][0]->getValue())
							:'';
				?>
				<textarea cols="30" rows="5" name="txa_<?= $iQuestion->getQuestionID() ?>"><?= $val ?></textarea>
				<?php elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX): ?>				
				<ul class="checkbox_list">					
						<?php 						
							$choices = $iQuestion->getChoices();
							$selections = array();
							$textVal = '';						
							if(array_key_exists($iQuestion->getQuestionID(),$structAns)){
								foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){							
									if($iChoice->isTypeCheckbox()){
										$selections[] = $iChoice->getValue(); 
									}
									elseif($iChoice->isTypeText()){
										$textVal = $iChoice->getValue(); 
									}
								}
							}
							foreach($choices as $iChoice):
						?>
							<li class="checkbox_list_item">
								<input type="checkbox" name="chk_<?= $iQuestion->getQuestionID() ?>[]" style="width:auto" value="<?= $iChoice->getChoice() ?>" <?= in_array($iChoice->getChoice(),$selections)?'checked="true"':'' ?>  />&nbsp;<?= $iChoice->getChoice() ?>
							</li>
						<?php endforeach;?>					
					<li class="checkbox_list_item checkbox_list_item_text">
						<input type="text" class="text" name="txtchk_<?= $iQuestion->getQuestionID() ?>" value="<?= $textVal ?>" />	
					</li>
				</ul>
				<?php endif; ?>
    		</div>
        </p>
	<?php endforeach; ?>

        <ul class="actions">
            <li class="first">            
                <a id="submitMtb" href="#bio_bubble" class="button">Save Changes</a>
            </li>
            <li class="last">            
                <a id="cancelSubmit" href="#bio_bubble" class="button">Cancel</a>
            </li>            
        </ul>    
</form>				

<div id="savedMtb" class="a_notice" style="display: none">Your changes have been saved.</div>

<script type='text/javascript'>
function noEnterKey(key){
//	alert('bbbbb');
	var kcode;
	if(window.event) { kcode=window.event.keyCode; }
		
	else if (key) {	kcode = ((key.which) ? key.which : key.keycode); }
	
//	alert(kcode);
	if(kcode == 13){
	  // this function can be seen at profileWidgets.js
	  ajaxSubmitMTB();	
	  return false;
	}
}

</script>						