<?php
	
	/**
	* tpl.IncLogin.php
	* @auth marc
	* aug 2006
	*/
	
	require_once("Application.php");
	require_once('Class.Template.php');
	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
	require_once('travellog/model/Class.AdminGroup.php');
	require_once('travellog/model/Class.MailGenerator.php');
	require_once('class.phpmailer.php');	
	require_once('travellog/model/Class.TravelerProfile.php');
	
	define("TRAVELER",0);
	define("ADVISOR",1);
	
	define("DISALLOW",0);
	define("ALLOW",1);
	define("ACTIVATE",2);
	
	$tmpLogin = new Template();
 	$msgLogin = '';
 	
 	$log_in_user = DISALLOW;
 	
 	if( isset($_GET["advisor"]) || ( isset($_POST["hdnLoginType"]) && ADVISOR == $_POST["hdnLoginType"]) )
 		$MODE = ADVISOR;
 	else
 		$MODE = TRAVELER;	
 		
 	$travID = 0;	
 	
 	/**
 	* if user has logged in
 	*/
 	if( isset( $_GET["failedLogin"]) ){
 		$ht = new HelpText();
 		$msgLogin = nl2br($ht->getHelpText('register-failedlogin'));	// added by kerwin for help text
 	}
 	
 	// user confirmation
	elseif( isset($_GET["ref"]) ){
		require_once("Class.Crypt.php");
		$crypt = new Crypt();
		$myString = str_replace(" ","+",$_GET["ref"]);
		$travID = $crypt->decrypt($myString);
		
		if( !is_numeric($travID) ){
 			header("location: register.php");
 			exit;
 		}	
 		
 		$traveler = new Traveler($travID);
 		$grpID = AdminGroup::getAdvisorGroupID($travID);
 		$loginLink = "login.php"; 
 		if( 0 < $grpID ){
 			$myGroup = new AdminGroup($grpID);
 			$loginLink .= "?advisor";
 		}
 		
 		// email user
		$mailGenerator = new MailGenerator($travID,3);
		$mailGenerator->send();
			
		$log_in_user = ALLOW; 
 		
	}	 
 	
 	elseif ( strlen($usrName) || strlen($pasWord) || isset($_POST["btnSubmit"]) ){
 		/**
 		* create database utility object
 		* check for user using the same username and password
 		*/
 		$nConnection = new Connection();
 		$nRecordset = new Recordset($nConnection);
 		$sqlquery = "SELECT travelerID,active FROM tblTraveler WHERE username='" . addslashes($usrName) . 
 			"' AND password='" . addslashes($pasWord) . "'";
 		$myquery = $nRecordset->Execute($sqlquery);
 		
 		if( $tmptrav = mysql_fetch_array($myquery) ){
 			$travID = $tmptrav["travelerID"];
 			if( $tmptrav["active"] )
 				$log_in_user = ALLOW;
 			else{
 				$log_in_user = DISALLOW;
 				// encrypt travelerID for confirmation
				require_once("Class.Crypt.php");
				$crypt=  new Crypt();
				$cryptTravID = $crypt->encrypt($travID);
				$cryptTravID = substr($cryptTravID,0,strlen($cryptTravID)-1);
				
				ob_end_clean();
				header("location: activation-support.php?tID=$cryptTravID");
				exit;
			}	
 		}
 		else
 			$msgLogin = 'Invalid username and/or password.'; 
 	}
 				
	/**
	* login user
	*/
	if ( 0 < $travID && $log_in_user == ALLOW ){
					
		$nConnection = new Connection();
 		$nRecordset = new Recordset($nConnection);
		
		/** 
		* update database info for user
		* set session variables
		*/
		$appendQuery = "";
		if( isset($_GET["ref"]) )
			$appendQuery = ", active = 1 ";
		
		$sqlquery = "UPDATE tblTraveler set latestlogin='" . date('c') . "' " . $appendQuery . " WHERE travelerID = " . $travID;
		$nRecordset->Execute($sqlquery);
		setcookie("login",crypt($travID,CRYPT_BLOWFISH));
		$_SESSION['isLogin'] = '1';
		$_SESSION['travelerID'] = $travID;
		$_SESSION['login'] = crypt($travID,CRYPT_BLOWFISH);
		setcookie('travelerID',$travID);
		
		/**
		* create session for user in forums
		* note: the following code was originally taken from phpbb
		*/

		/**
		* define needed info for forum login
		*/
		define('IN_PHPBB',true);
		require_once('config.php');
		$db = mysql_connect($dbhost, $dbuser, $dbpasswd, $dbname, false);
		require_once('includes/functions.php');
		
		$sqlquery = "SELECT * FROM phpbb_config";
		$tbConfig = $nRecordset->Execute($sqlquery);
		$board_config = array();
		while ( $row = mysql_fetch_array($tbConfig) )
			$board_config[$row['config_name']] = $row['config_value'];
		$val = $board_config['rand_seed'] . microtime();
		$val = md5($val);
		$board_config['rand_seed'] = md5($board_config['rand_seed'] . $val . 'a');
		$sqlquery = "UPDATE phpbb_config SET
			config_value = '" . $board_config['rand_seed'] . "'
			WHERE config_name = 'rand_seed'";
		if( !$nRecordset->Execute($sqlquery) ){
			$msgLogin = 'There was an error in creating the session. The web administrator has been notified already. Please try logging in again';
			mail('marc@goabroad.com', 'error in ga_net login', 'Unable to reseed PRNG.<p>' . $sqlquery ,'From: The GoAbroad Network <admin@goabroad.net>');
		}
		
		$client_ip = ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : getenv('REMOTE_ADDR') );
		$ip_sep = explode('.', $client_ip);
		$user_ip = sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
		$current_time = time();
		$session_id = md5(substr($val, 4, 16));
		$_SESSION['sid'] = $session_id; 
		$_SESSION['user_ip'] = $user_ip;
		
		// delete previous sessions
		$nRecordset->Execute("DELETE FROM phpbb_sessions WHERE session_user_id = " . $_SESSION['travelerID']);
		// insert new session
		require_once("forums/includes/sessions.php");
		reviveSession();
		
	 	if ( !strlen(trim($msgLogin)) ){
		 	$_SESSION["gID"] = AdminGroup::getAdvisorGroupID($travID); 
		 	ob_end_clean();
		 	if( strlen(trim($_POST["hdnReferer"])) ){
		 		header("location: " . trim($_POST["hdnReferer"]) );
		 	}
		 	elseif( ADVISOR == $MODE || 0 < $_SESSION["gID"] ){
		 		$_SESSION["clientID"] = AdminGroup::getAdvisorClientID($travID);
		 		header("location: group.php?gID=" . $_SESSION["gID"]);
		 	}
		 	else	
		 		header("location: passport.php");
			exit;
		}	 			
	}
 	
 	/**
	 * Modified By: Aldwin S. Sabornido
	 * Date       : July 26, 2007
	 * Comments   : converts the ^ symbol to & symbol
	 */
 	if( isset($_GET["redirect"]) && strlen(trim($_GET["redirect"])) ){
 		$_SERVER["QUERY_STRING"] = str_replace("^", "#", $_SERVER["QUERY_STRING"]);
 		$hdnReferer              = substr($_SERVER["QUERY_STRING"],strpos($_SERVER["QUERY_STRING"],"redirect=")+9);
 	}	
 	elseif( isset($_POST["hdnReferer"]) && strlen(trim($_POST["hdnReferer"])) )
 		$hdnReferer = $_POST["hdnReferer"];	
 	else $hdnReferer = "";	
 	
 	$tmpLogin->set_vars( array(
  		'msg' => $msgLogin,
  		'action' => $action,
  		'isRegister' => isset($isRegister) ? $isRegister : 0,
  		'hdnLoginType' => $MODE,
  		'hdnReferer' => $hdnReferer 
  	));
	
	/**
	* display login form
	*/
	echo $tmpLogin->fetch('travellog/views/tpl.FrmLogin.php');
	
?>

