<?
	/**
	* tpl.FrmStudentProfile.php
	* @author marc
	* aug 2006
	*/
	
	$contents["subNavigation"]->show();
?>

<div class="area" id="content_wrapper">
	
	<div class="area" id="area_right">
		<div class="section">
			<h1><span>Edit Your Student Profile</span></h1>
			<div class="content">
				<form method="post" action="<?= $contents['action'] ?>" class="interactive_form" style="width: 100%;">
					<ul class="form">
						<li class="column">
								<label for="cmbFirstOption">First Option: </label>
								<select name="cmbFirstOption" id="cmbFirstOption">
									<option value="0"> - Select - </option>
									<?= $contents['countryOption0']; ?>
								</select>
						</li>
						
						<li class="column">
								<label for="cmbSecondOption">Second Option: </label>
								<select name="cmbSecondOption" id="cmbSecondOption">
									<option value="0"> - Select - </option>
									<?= $contents['countryOption1']; ?>
								</select>
						</li>
						<li class="column">
								<label for="cmbThirdOption">Third Option: </label>
								<select name="cmbThirdOption" id="cmbThirdOption">
									<option value="0"> - Select - </option>
									<?= $contents['countryOption2']; ?>
								</select>
						</li>
					</ul>
					<div class="clear"></div>
						<?= $sub_views['STUDENT_PREFERENCE']->render(); ?>
					<div class="clear"></div>
						
					<ul class="form">
						<li class="actions">
							<input type="submit" value="Save" class="submit" /> 
						</li>
					</ul>
					<div class="clear"></div>
				</form>
			</div>
		
			<div class="foot"></div>
		
		</div>
	</div>
	
	<div class="clear"></div>
</div>


