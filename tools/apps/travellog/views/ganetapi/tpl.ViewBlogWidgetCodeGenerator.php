<?php
	Template::setMainTemplateVar('title', $title);
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', $page_location);
?>

<?php echo $profile_component->render(); ?>

<?php $sub_navigation_component->show(); ?>

<div id="content_wrapper" class="layout_2" style="position:relative">

		<div id="wide_column">
			<!--Blog Widget-->
			<div id="blog_widget" class="section" style="position:relative;">
			    <h2 style="margin-bottom: 20px;">
			    	<a href="<?php echo $travelerWidgetLink; ?>" class="">Traveler widget</a> | Blogs widget
			    </h2>
			    
			    <div id="widget_options" style="float: left; margin-right: 100px; border: medium none; padding-left: 0;">
			    	<fieldset>
			    		<form style="display: block; width: 200px;" method="post">
			    			<p><strong>Attributes</strong></p>
			    			<div>
			    				<div>Title</div>
			    		    	<div>
			    					<input type="text" name="wHeading" id="wHeading" value="<?php echo htmlspecialchars($header); ?>" class="text" >
			    				</div>
			    			</div>
			    			
			    			<div>
			    				<div>Description <em>(Max length: 50)</em></div>
			    		    	<div>
			    					<input type="text" name="wDescription" id="wDescription" value="<?php echo htmlspecialchars($description); ?>" class="text" maxlength="50">
			    				</div>
			    			</div>
			    			
			    			<div>
			    				<div>Number of Blogs Shown</div>
			    		    	<div>
			    					<input type="text" name="wCount" id="wCount" value="<?php echo htmlspecialchars($count); ?>" class="text">
			    				</div>
			    			</div>
			    			
			    			<div>
			    				<div>Widget Width <em>(Max value: 650)</em></div>
			    		    	<div>
			    					<input type="text" name="wWidth" id="wWidth" value="<?php echo htmlspecialchars($width); ?>" class="text">
			    				</div>
			    			</div>
			    			
			    			<!--div class="checkbox_list_item">
			    				<p>
			    					Show Scrollbar <input type="checkbox" id="wScrollbar">
			    				</p>
			    			</div-->
			    			
			    			<div>
			    				<div>Font to Use</div>
			    				<select name="fontGroup" id="wFont">
			    					<option value="">Select a font</option>
			    					<option value="Lucida Grande">Lucida Grande</option>
			    					<option value="Verdana">Verdana</option>
			    					<option value="Arial">Arial</option>
			    				</select>
			    			</div>
			    			
			    			<br>
			    			<p><strong>Show Travel Journals from</strong></p>
			    			
			    			<div>
			    				<div>Subgroup</div>
			    				<select name="wSubGroup" id="wSubGroup" style="width:200px;">
									<option value="0">All</option>
									<?php foreach ($subgroups as $id => $name): ?>
									<option value="<?php echo $id; ?>"><?php echo $name; ?></option>
									<?php endforeach; ?>
								</select>
			    			</div>
			    			
			    			<div>
			    				<div>Country</div>
			    				<select name="wCountry" id="wCountry" style="width:200px;">
									<option value="0">All</option>
									<?php foreach ($countries as $id => $name): ?>
									<option value="<?php echo $id; ?>"><?php echo $name; ?></option>
									<?php endforeach; ?>
								</select>
			    			</div>
			    			
			    			<br>
			    			<p><strong>Sort Blogs By</strong></p>
			    			
			    			<div>
			    				<select name="wSort" id="wSort">
									<option value="<?php echo GanetApiConstant::SORT_RECENT; ?>" selected="selected">Most recently updated</option>
									<option value="<?php echo GanetApiConstant::SORT_RANDOM; ?>">Random</option>
								</select>
			    			</div>
			    			
			    			<br>
			    	        <ul class="actions">
			    	            <li>            
			    	                <a class="button" href="javascript: void(0);" id="wSubmit">Get Code</a>
			    	            </li>         
			    	        </ul>    
			    		</form>
			    	</fieldset>
			    </div>
			    
			</div>

			<!--End of Blog Widget-->
			
		
		<div id="widget_preview" style="position:absolute; left:300px; top:100px;">
	    	<?php echo $default_code; ?>
	    </div>
		
</div>
<script type="text/javascript">
	var default_frame_code = '<?php echo $default_code; ?>';
</script>
<script type="text/javascript" src="/min/f=js/jquery.ganetapi.js"></script>