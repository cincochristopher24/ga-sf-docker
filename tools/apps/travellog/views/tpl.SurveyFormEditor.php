<?php
	Template::displayLog(false); 
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
	Template::includeDependentCss('/css/formeditor/gen.css');
	//Template::includeDependentCss('/css/formeditor/survey.css');
	Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
	Template::includeDependentJs('/js/interactive.form.js');
	require_once('travellog/helper/Class.NavigationHelper.php');
	if (NavigationHelper::isCobrand()) {
		Template::setMainTemplateVar('page_location', 'Home');
	}else{
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	Template::includeDependentJs('/js/scriptaculous/scriptaculous.js');
	Template::includeDependentJs('/js/utils/json.js');
	Template::includeDependentJs('/js/formeditor/control.js');
	Template::includeDependentJs('/js/formeditor/propertyviewer.js');
	Template::includeDependentJs('/js/formeditor/formeditor.js');
	Template::setMainTemplateVar('layoutID', 'survey');
	Template::displayLog(false);
	
	$is_new_survey = $surveyFormID == 0; ?>
	
<!-- displays the profile component: added by neri 11-06-08 -->
<?= $profile->render()?>
<!-- end -->
	
<?	$subNavigation->show();			
	ob_start();	
?>
<script type="text/javascript">
//<![CDATA[	
		//Fix image cache problem with IE 6
		try {
			document.execCommand('BackgroundImageCache', false, true);
		} catch(e) {}
		
		FormEditor.init({
			groupID 		: <?= $groupID?>,
			surveyFormID 	: <?= $surveyFormID ?>,			
			groups			: <?=trim($jsonGrpStr)?>,
			formData		: <?=trim($jsonForm)?>,
			mainContainer 	: '_main',
			panelStage		: 'sidebar',
			panel			: {
				panelStage		: 'sidebar',
				mainPanel		: 'mainPanel',
				formPanel 		: 'propertyPanel',
				fieldPanel		: 'propertyPanel',
				controlPanel	: 'controlPanel',
				messagePanel	: 'messagePanel',
				tabStage		: 'tabStage',
				formTab			: 'liform',
				fieldTab		: 'lifield',
				controlTab		: 'liadd'
			},
			handler 		: '/ajaxpages/formEditorHandler.php'
		});
//]]>
</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);	
?>
<div class="area mine" id="intro">
  <div class="section">
    <h1>Survey Center</h1>
  </div>
</div>
<div class="area_wrapper"> 
	<div id="container">
	<div id="container_inner_wrapper">
	<div id="survey_options">
    	<ul id="top_tabs">
        	<li class="first_tab"  <? if (!$is_new_survey) echo 'id="sel"'; ?> ><a href="/surveycenter.php?gID=<?=$groupID?>">Manage Surveys</a></li>
          	<li <? if ($is_new_survey) echo 'id="sel"'; ?> ><a href="/surveyformeditor.php?frmID=0&gID=<?=$groupID?>">Create a New Survey</a></li>
        </ul>
        <div class="clear"></div>
   	</div>
   	<div id="c_content">
        <div id="c_content_wrapper">
	    	<div id="mainContainer" style="height:300px">
				<div id="_main" class="main" style="padding: 35px 0 0 0;"></div>
  			</div>
  		</div>	
  		<div id="sidebar">  			
         	<div id="s_actions">
            	<ul>
              		<li id="liform"><a id="liform_tab" href="#" onclick="return false;"><span>Form Properties</span></a></li>
              		<li id="lifield"><a id="lifield_tab" href="#" onclick="return false;"><span>Edit Questions</span></a></li>
              		<li id="liadd"><a id="liadd_tab" href="#" onclick="return false;"><span>Add New Questions</span></a></li>
            	</ul>
          	</div>
          	<div id="controlPanel" class="panel floatingPanel">
				<h2>Add a New Field</h2>
				
				<ul class="addOptions">
					<li class="txt" id="txt">
						<a href="#" id="link_txt" onclick="return false;"><strong>Single Line Text</strong>
							<span>Choose this if the answer to your question requires a few words or a short sentence.</span>
						</a>
					</li>					
					<li class="txtarea" id="txta" >
						<a href="#" id="link_txta" onclick="return false;" ><strong>Paragraph</strong>
							<span>Choose this if the answer to your question requires a few sentences or a paragraph.</span>
						</a>
					</li>
					<li class="chk" id="chk">
						<a href="#" id="link_chk" onclick="return false;"><strong>Checkboxes</strong>
							<span>Choose this if your question requires your participants to check one or more options that apply.</span>
						</a>
					</li>
					<li class="opt" id="opt">
						<a href="#" id="link_opt" onclick="return false;" ><strong>Multiple Choice</strong>
							<span>Choose this if your question requires your participants to choose only one option from a few given choices.</span>
						</a>
					</li>
					<li class="dd" id="dd">
						<a href="#" id="link_dd" onclick="return false;"><strong>Drop Down</strong>
							<span>Choose this if your question requires your participants to choose only one option from a list of given choices.</span>
						</a>
					</li>
					<li class="sb" id="sb">
						<a href="#" id="link_sb" onclick="return false;" ><strong>Section Break</strong>
							<span>Add this if you want to begin a new section on your survey.</span>
						</a>
					</li>
				</ul>
			</div>
  			<div id="propertyPanel" class="panel prop "style="position:absolute"></div>
  			<div id="messagePanel" class="panel prop floatingPanel" style="clear : both;"></div>		
			<div class="clear"></div>
		</div>
		<div id="statusBar">Loading Data...</div>
		<!--<div id="debug">Debugging...</div>-->
		<div class="clear"></div>
	</div>	
	<div class="clear"></div>
	</div>
</div>
</div>
<div class="clear"></div>
<div id="helpBox" style="display:none;">
	<div class="clear"></div>
	<div id="helpHead">
		<strong>Help</strong>
		<span>
			<img src="/images/formeditor/delete.png" onclick="HelpManager.hideHelpBox()" />
		</span>
	</div> 
	<div id="helpBody">
		This is a help text
	</div>
	<div class="clear"></div>
</div>