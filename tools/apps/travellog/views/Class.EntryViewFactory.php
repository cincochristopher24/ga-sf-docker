<?php
require_once("travellog/views/Class.EntryViewType.php");
class EntryViewFactory{
	static $instance = NULL;
	
	static function instance(){
		if( self::$instance == NULL ) self::$instance = new EntryViewFactory; 
		return self::$instance;
	}	
	
	function create( $view_type ){
		switch( $view_type ){
			case EntryViewType::$EntryTitleCalloutPhoto:
				require_once("travellog/views/Class.EntryTitleCalloutPhoto.php");
				return new EntryTitleCalloutPhoto;
			break;
			
			case EntryViewType::$EntryDescription:
				require_once("travellog/views/Class.EntryDescription.php");
				return new EntryDescription;
			break;
		}
	}
}
?>
