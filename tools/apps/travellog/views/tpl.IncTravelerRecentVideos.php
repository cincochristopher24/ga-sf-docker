<div class="section newMedia">
	<h2>
		<span>Videos</span>
		<?php if(isset($label)): ?>
			<span class="header_actions">
				<div style="clear: both;">
					<a href="/video.php?travelerID=<?php echo $travelerID ?>"><?php echo $label ?></a>
				</div>
			</span>
		<?php endif; ?>
	</h2> 
	<div class="content">		
		<?php if(0 < count($videos)): ?>
			<?php $i = 0; ?>
			
			<?php while($i < count($videos) && $i < 3): ?>
				<?php if ('TravelLogToVideo' == get_class($videos[$i])): ?>	
					<div class="list"> 
						<div>	
							<p class="duration"><span><?php echo $videos[$i]->getDuration() ?></span></p>
							<a href="#" onclick="tb_open_new('','/video.php?action=playEntryVideo&videoID=<?= $videos[$i]->getVideoID() ?>&height=360&width=510')">
								<img src="<?php echo $videos[$i]->getVideoImageUrl() ?>" border="0" align="left" width="120" height="90">
							</a>
								<p class="entry clear">
									<a href="#" onclick="tb_open_new('','/video.php?action=playEntryVideo&videoID=<?= $videos[$i]->getVideoID() ?>&height=360&width=510')">
										<strong><?php echo $videos[$i]->getTitle() ?></strong>
									</a><br/>
									<span>
										<?php echo GaDateTime::descriptiveDifference($videos[$i]->getDateAdded(), GaDateTime::dbDateTimeFormat()); ?> ago
									</span>
								</p>			
						</div> 
					</div>
				<?php else: ?>	
					<div class="list"> 
						<div>	
							<p class="duration"><span><?php echo $videos[$i]->getDuration() ?></span></p>
							<a href="#" onclick="tb_open_new('','/video.php?action=playAlbumVideo&videoID=<?= $videos[$i]->getVideoID() ?>&albumID=<?= $videos[$i]->getParentAlbum()->getAlbumID() ?>&height=360&width=510')">
								<img src="<?php echo $videos[$i]->getVideoImageUrl() ?>" border="0" align="left" width="120" height="90">
							</a>
							<p class="videoalbum clear">
									<a href="#" onclick="tb_open_new('','/video.php?action=playAlbumVideo&videoID=<?= $videos[$i]->getVideoID() ?>&albumID=<?= $videos[$i]->getParentAlbum()->getAlbumID() ?>&height=360&width=510')">
										<strong><?php echo $videos[$i]->getTitle() ?></strong>
									</a><br/>
									<span>
										<?php echo GaDateTime::descriptiveDifference($videos[$i]->getDateAdded(), GaDateTime::dbDateTimeFormat()); ?> ago
									</span>
							</p>
						</div> 
					</div>
				<?php endif; ?>	
				
				<?php $i++; ?>
			<?php endwhile; ?>	
		<?php endif; ?>									
	</div>

</div>