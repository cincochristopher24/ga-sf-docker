<?
require_once 'Class.StringFormattingHelper.php';
require_once("travellog/model/Class.SiteContext.php");
?>
<div id="journals_summary" class="section" >
	<a name="anchor"></a>
	<h2><span><?=$compTitle?></span></h2>	
	
	<div class="content" >								
			<?
				$country = '';
				$city = '';
				$jeCtr = 1;					// journal entry counter used for identifying yahoo carousel object
				$carJsCode = '';			// yahoo carousel code
				
			?>

				<?$ti = microtime(true);?>

			<ul class="journals">
				<?foreach($journals as $journal):
					$jID = $journal->getTravelID();
					$jeCount = count($journalsAndEntries[$jID]);
					$jEntriesAr = $journalsAndEntries[$jID];
					if ($journal->getOwner() == null)
						echo 'travelid:' . $journal->getTravelID();
									
					$jah = new JournalAuthorUIHelper($journal->getOwner());
					$locationID = 0;
					if (isset($jEntriesAr[$jeCount-1])){
						$trip = $jEntriesAr[$jeCount-1]->getTrip();
						$country = $trip->getLocation()->getCountry()->getName();
						$city = $trip->getLocation()->getName();
						$locationID = $trip->getLocation()->getCountry()->getCountryID();
					}
					?>
					 	<li class="container" id="container<?=$jID?>">     
						
							<div class="content" id="content<?=$journal->getTravelID()?>">	

								<?if (isset($journalPhotos[$jID])):?>
										
										<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>"> 
											<img class="float_right" src="<?=$journalPhotos[$jID]->getPhotoLink('featured')?>" alt="journal photo" />
										</a>
										
								 <?endif;?>

								<?if (isset($jEntriesAr[$jeCount-1])):?>								
																	
										<h3>
										<a class="journal_title" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>">
										<?=$jEntriesAr[$jeCount-1]->getTitle()?>
										</a>
										</h3>							

									<p class="meta">
										<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" alt="Travel Journals from <?=$country?>" style="margin: 0 0 0 0;" />						
										<?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?> | <?=(strtotime($trip->getArrival())) ? date('D M d, Y',strtotime($trip->getArrival())).' | ' : ''?>Views: <?=$journal->getViews()?>
									</p>
									<p>
										<?if ($jeCount):?>
											<?= StringFormattingHelper::wordTrim($jEntriesAr[$jeCount-1]->getDescription(), 70, '&#8230;')?>
											<?if (false):?>
											<?=(strlen(trim($jEntriesAr[$jeCount-1]->getDescription())) > 175 )? strip_tags(nl2br(substr($jEntriesAr[$jeCount-1]->getDescription(),0,175))) . '...' : strip_tags(nl2br($jEntriesAr[$jeCount-1]->getDescription()))?>
											<?endif;?>
										<?endif;?>	
									</p>
								<?else:?>
									<p class="help_text"> <span>There are no entries for this journal</span>. <br />
										Document those great travel moments and share them with family and friends! A journal book is a set of journal entries about the same trip. You may write about your experiences and add your travel photos to your entries. You may also do a photo journal.
									</p>
								<?endif;?>
								
								<?if ($showAuthor):?>			 							    
								    	<!--<a href="<?=$jah->getString('OWNER_URL')?>" title="Read more of <?=$jah->getString('OWNER_URL')?> travel experiences and adventure!"><img class="img_border" src="<?=$jah->getString('PHOTO_THUMBNAIL_URL')?>" width="37px" height="37px"  /></a>-->  
										<? /* <p class="meta">traveler: <a href="<?=$jah->getString('OWNER_URL')?>" title="Read more of <?=$jah->getString('OWNER_URL')?> travel experiences and adventure!"><strong><?=$jah->getString('OWNER')?></strong></a></p>
										*/?>
										<? $owner = $journal->getTraveler(); ?>
										<? if( !$owner->isAdvisor() ): ?>
											<p class="entry_group_tag">
												<span>
													<a class="username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$owner->getUserName()?>">
														<img class="pic" width="37" height="37" src="<?=$owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$owner->getUserName()?>" title="Read more of <?=$owner->getUserName()?> travel experiences and adventure!" />
													</a>
													
													</span>					
													<a class="author_username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$owner->getUserName()?>"><strong><?=$owner->getUserName()?></strong></a>	
													<?if(SiteContext::getInstance()->isInCobrand() && $canAddJournal):?>
														<strong class="meta"><em> <?=$owner->getFullName()?> </em></strong>
													<?endif;?>
													
													
													<!-- Nick Aguilos | Jan 27 2009 -->
													<br />
														<!-- <strong id="journals_label">Journal:</strong>-->
														<img id="journal" src="/images/journalicon2.gif"></img>
																			
														<?if (isset($jEntriesAr[$jeCount-1])): ?>									
															<a class="journal_title" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" title="<?=$journal->getTitle()?>">
																<?=$journal->getTitle()?>
															</a>
														<?else:?>
															<?=$journal->getTitle()?>
														<?endif;?>
														
											</p>
										<?else:?>	
											<? 	$group = new AdminGroup($owner->getAdvisorGroup()); 
												$site = $group->getServerName();
												$local = "";
												if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
													$local = ".local";
												}
											?>
											<p class="entry_group_tag">
												<?if($site != false):?>	
													<a href="http://<?=$group->getServerName().$local?>" title="<?= $group->getName()?>">	
														<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
													</a>
													<a href="http://<?=$group->getServerName().$local?>" alt="" class="author_username">
														<strong><?=$group->getName()?></strong>
													</a>
													<!-- NICK AGUILOS -->
													<br />
													<!--<strong class="journal_label">Journal:</strong>-->
													<img id="journal" src="/images/journalicon2.gif"></img>
													<?if (isset($jEntriesAr[$jeCount-1])):?>
														<a class="journal_title" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" title="<?=$journal->getTitle()?>"><?=$journal->getTitle()?></a>
													<?else:?>
														<strong><?=$journal->getTitle()?></strong>
													<?endif;?>
													<!-- end of edits -->										
												</a>
											
											
												<?else:?>
													<a href="<?=$local?>/group.php?gID=<?=$group->getGroupID()?>" class="<?= $group->getName()?>">
														<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
													</a>
													<a href="<?=$local?>/group.php?gID=<?=$group->getGroupID()?>" class="author_username">
														<strong><?=$group->getName()?></strong>
													</a>	
													<!-- Nick Aguilos | Jan 27 2009 -->
													<br />
													<!-- <strong id="journals_label">Journal:</strong>-->
													<img id="journal" src="/images/journalicon2.gif"></img>
																		
													<?if (isset($jEntriesAr[$jeCount-1])): ?>									
														<a class="journal_title" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" title="<?=$journal->getTitle()?>">
															<?=$journal->getTitle()?>
														</a>
													<?else:?>
														<?=$journal->getTitle()?>
													<?endif;?>
												
												<?endif;?>
											</p>
										<? endif; ?>
								<?endif;?>
								<? $owner = $journal->getTraveler(); ?>
								<? if( !$owner->isAdvisor() ): ?>
									<?if (isset($relatedGroupsAr[$jID])):
										if (count($relatedGroupsAr[$jID]) == 1): 
											 $group = $relatedGroupsAr[$jID];
											 $group = array_pop($group);
											 $site = $group->getServerName(); 
												$local = "";
												if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
													$local = ".local";
												}
										?>
											<p class="entry_group_tag not_author <?if (!$journal->getTraveler()->isAdvisor()):?>not_author<?endif;?>">
												<?if($site != false):?>
													<span>
													<a href="http://<?=$group->getServerName().$local?>">	
														<img class="float_left" src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="<?= $group->getName()?>" width="37" height="37"/></a>
													</span>	
													<a href="http://<?=$group->getServerName().$local?>" class="author_username">
													<strong><?=$group->getName()?></strong>
													</a>
												
													<span class="meta"><strong>Advisor Group Journal</strong></span>
												
												<?else:?>
													<span>
													<a href="<?=$local?>/group.php?gID=<?=$group->getGroupID()?>" class="<?= $group->getName()?>">
														<img class="float_left" src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/></a>
													</span>					
													<a href="<?=$local?>/group.php?gID=<?=$group->getGroupID()?>" class="author_username">
														<strong><?=$group->getName()?></strong>	
													</a>
												
													<span class="meta"><strong>Advisor Group Journal</strong></span>
												
											
													
														
														
												<?endif;?>
												
											</p>
										
											<!-- Nick Aguilos | Jan 27 2009 -->
													<?/* if( $owner->isAdvisor() ): ?>
															<!--<div class="entry_group_tag" id="journals_label"><strong>Journal:</strong>-->
															<img id="journal" src="/images/journalicon2.gif"></img>
															<?if (isset($jEntriesAr[$jeCount-1])): ?>									
															<a id="journal_title" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" alt="<?=$journal->getTitle()?>"><?=$journal->getTitle()?></a>
															<?else:?>
															<?=$journal->getTitle()?>
															<?endif;?>	
															</a>
															<!--/div-->
														
													<? endif; */?>	
										<?endif;?>
									<?endif;?>	
								<?endif;?>	
								
								<?if (false): // Do not show for now. The extra markup here is messing up the html code?>
								<!-- 
								<div class="entry_box">					
										<?$ctr = 1?>
									   	<?$arlen = count($jEntriesAr)?>
									   	<a id="up-arrow<? if( $arlen > $jEntryInViewCount) echo $jeCtr;?>" class="<?if( $arlen > $jEntryInViewCount):?>journal_button_top<?else:?>jbuttontop_disable<?endif;?>"></a>
										<div id="mycarousel<? if( $arlen > $jEntryInViewCount) echo $jeCtr;?>" class="inside_entry">
											<div class="carousel-clip-region">
										        <ul class="carousel-list">						        	
										        	<?foreach($jEntriesAr as $jeLine):
										        			 $jePhotoCount = $jeLine->countPhotos();				        			 			        			 
										        			 $photoStr = '';
										        			 if ($jePhotoCount > 0)
										        			 	$photoStr .= ' | ' . $jePhotoCount . ' Photo';
										        			 if ($jePhotoCount > 1)
										        			 	$photoStr .=  's'; ?>		

															<li id="mycarousel<?=$jeCtr;?>-item-<?=$ctr;?>" <?if($ctr == $arlen):?>class="active"<?endif;?>   > 
																<a href="/journal-entry.php?action=view&travellogID=<?=$jeLine->getTravelLogID()?>"> 
																	<strong>
																		 <?=substr($jeLine->getTitle(),0,20)?>												 
																 		 <?	if(strlen(trim($jeLine->getTitle())) > 20) echo '...'; ?> 
																	</strong>
																	<span class="entry_date_photo">
																		<?=date('d F',strtotime($jeLine->getTrip()->getArrival()))  . $photoStr ?>
																		<?if ($ctr == $arlen):?>
																		<span></span>
																		<?endif;?>
																	</span>
																</a>
															</li>
															<?$ctr++?>
													<?endforeach;?>		

												</ul>	 
											</div>
										</div>			
										<a id="down-arrow<?if( $arlen > $jEntryInViewCount) echo $jeCtr;?>" class="<?if( $arlen > $jEntryInViewCount):?>journal_button_bottom<?else:?>jbuttonbot_disable<?endif;?>"></a>	
									</div>
									<div class="clear"></div>
								</div>
								-->
								<?endif;?>						
							</div>
						</li>
				
				<?$tf = microtime(true);
				//echo 'template time:' . ($tf-$ti) . '<br/>'; ?>	
				<?endforeach;?>
				</ul>
				
				<? if (isset($pagingComp)):?>	
				    	<? $pagingComp->showPagination() ?>
				<? endif; ?>
					
			<?if (count($journals) == 0){
				 echo '<p class="help_text">' . $noJournalMessage  . '</p>';						
			}?>

	</div>
	
  <div class="foot"></div>  
  <div class="yui-skin-sam">
  	<div id="dlg">
  	</div>
  </div>

</div>
<div id="dlg_container">
</div>
<?if (false):?>
<!-- javascript code for carousel for each journal -->
<script type="text/javascript">		
	//<![CDATA[												
		var Carousel = function(carouselElementID, carouselCfg) {			
			this.init(carouselElementID, carouselCfg);
		}; 
		
		Carousel.prototype = {
		   init: function(id, cfg) {
		      var config = {
		            numVisible:        5,
		            animationSpeed:    0.15,
		            scrollInc:         4,
		            navMargin:         2,
		            orientation:       "vertical",
			        prevButtonStateHandler: this.handlePrevButtonState,
			        nextButtonStateHandler: this.handleNextButtonState
		      };
		
		      for (var key in cfg) {
		        
		         if (!cfg.hasOwnProperty(key)) { continue; }
		         config[key] = cfg[key];  
		      }
		
		      this.carousel = new YAHOO.extension.Carousel(id, config);
		   },
		   		
		   handlePrevButtonState: function(type, args) {
		        var enabling = args[0];
		        var leftImage = args[1];
		        if( enabling ){
		        	document.getElementById(leftImage.id).className = 'journal_button_top';
		        }
		        else{
		        	document.getElementById(leftImage.id).className = 'jbuttontop_disable';
		        }
		   },
		
		   handleNextButtonState: function(type, args) {
		        var enabling = args[0]; 
		        var rightImage = args[1];
		        if( enabling ){
		        	document.getElementById(rightImage.id).className = 'journal_button_bottom';
		        }
		        else{
		        	document.getElementById(rightImage.id).className = 'jbuttonbot_disable';
		        }      
		    }
		};	
				
		jQuery(document).ready(function() {  pageLoad(); } );
		//YAHOO.util.Event.addListener(window, 'load', pageLoad);
		 
	//]]> 
</script>		
<?endif;?>
<div id="dlg"></div>
