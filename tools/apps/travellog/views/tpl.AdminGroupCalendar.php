<?		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
?>
<style type="text/css">
	#popup_header strong
	{
		color:#FFFFFF;
	}
</style>

<!-- displays the profile component: added by neri 11-06-08 -->
<?= $profile->render()?>
<!-- end -->

<?=$subNavigation->show();?>
<div class="layout_2" id="content_wrapper">

	<div id="wide_column">
		<div class="section" id="event_details">
				<h2 id="event_details_header"><span>Event Details</span></h2>
				<div class="content"> 
                	<div id="event_view"></div>    
					<p class="loadingblock"  id="loading">
					    <span id="imgLoading">
					        <img alt="Loading" src="/images/loading.gif"/>
					    </span>
					    <em id="statusCaption">Loading data Please wait...</em>
					</p>
				</div>
		</div>
	</div>

	<div id="narrow_column">
		<div class="section" id="quick_tasks">
			<h2><span><?=$props['event_type']?></span></h2>
			<div class="content"><?=$group_event_lists?></div>
		</div>	
	</div>
</div>
<?php require_once('Class.DateTimeHolder.php'); ?>

<script type="text/javascript" >
  //<![CDATA[
	  jQuery.noConflict();  
	  jQuery(document).ready(function(){
		  jQuery('#events_calendar li').observeEvents();
		jQuery("#loading").ajaxStart(function(){
		    
		jQuery(this).css('display', 'inline');
		jQuery('#event_details #event_view').empty();
		  });
		  jQuery("#loading").ajaxStop(function(){
		    jQuery(this).css('display', 'none'); 
		  });
	  });
  //]]> 
</script>
<script type="text/javascript" src="/min/g=GroupCalendarJs"></script>
<!--<script type="text/javascript" src="/js/date.js"></script>
<script type="text/javascript" src="/js/jquery.datePicker.js"></script>
<script type="text/javascript" src="/js/jquery.groupEvent.js"></script>
<script type="text/javascript" src="/js/jquery.timeentry.js"></script>
<script type="text/javascript" src="/js/localtime.js"></script>
<script type="text/javascript" src="/js/jquery.json-1.3.min.js"></script> -->

<script type="text/javascript" charset="utf-8">
	b = '<?php echo DateTimeHolder::getInstance()->toJsonData()?>';
	// set action for observeEvents called after sorting
	action = <?php echo strtolower($props['action']);?>;
	// get = '';
	// <?if(isset($_GET['dbhDebug'])):?>
	// 	get = '&dbhDebug=1';
	// <?endif;?>	
	LocalTimeConverter.convertAndSortEventDates(b,0);
</script>