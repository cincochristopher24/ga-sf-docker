<?php 
	$collections = array_merge($albumCollections, $travelCollections);
	$collectionsPerStrip = 5;
	$stripCount = ceil(count($collections)/$collectionsPerStrip);
	$stripsVisible = 1; 
	$strips = array();
	$curStripCount = 0;
	$ownerID = (isset($travelerID)) ? $travelerID : $groupID; 
	$ownerType = (isset($travelerID)) ? 'Traveler' : 'Group';
	$defaultIndex = 0;
?>

<div class="mediaviewer">
	<?	if( $stripsVisible < $stripCount ): ?>
		<div id="btnPrevStrip" class="topbuttons">
			<a class="upmore" href="" onclick="paginatedCollectionList.scrollToPrevStrip(); return false;">
				<span>up</span>
			</a>
		</div>
	<?	endif; ?>
	
	<div id="slider" class="box" style="height: <?if(count($collections)<$collectionsPerStrip): echo (ceil(count($collections))*92)+(ceil(count($collections))*2)-1; else: echo (ceil($collectionsPerStrip)*92)+(ceil($collectionsPerStrip)*2)-1;endif;?>px;">
		<div id="slider_content" class="slider_content" style="top: 0px; opacity: 1;">
		<?php foreach($collections as $x => $collection): ?>
			<?php  
				$strips[] = $collection;
				if ($collection instanceof VideoAlbum && isset($albumID) && $albumID == $collection->getAlbumID()) 
					$defaultIndex = $x;	
				else if ($collection instanceof Travel && isset($travelID) && $travelID == $collection->getTravelID())
					$defaultIndex = $x;  
			?>
			<?php if( 0 == (($x+1) % $collectionsPerStrip) ): ?>
				<ul id="slider_section-<?=++$curStripCount?>" <? if($stripCount < $stripsVisible): ?>upper<?endif;?>" style="height: <?=ceil(count($strips))*92+(ceil(count($strips))*2)+2?>px;">
					<? foreach($strips as $y => $item): ?>
						<?php 
							$videos = 0;
							$link = "/video.php?action=get".$ownerType."Videos&amp;";
							$link .= (isset($travelerID)) ? "travelerID" : "gID";
							$link .= "=".$ownerID."&amp;";
							
							if($item instanceof VideoAlbum) {
								$itemID = $item->getAlbumID();
								$videos = $item->getVideos();
								$link .= "albumID=".$itemID."&amp;type=album"; 
							}
							
							else if ($item instanceof Travel) {
								$itemID = $item->getTravelID();
								$videos = $item->getTravelVideos();
								$link .= "travelID=".$itemID."&amp;type=journal";
							}
							
							$title = (20 < strlen($item->getTitle())) ? substr($item->getTitle(), 0, 20).'...' : $item->getTitle();
							$thumbnail = (0 < count($videos)) ? $videos[0]->getVideoImageUrl() : '/images/default_images/120_video.gif';	
						?>
						
							<li <?php if ($collectionID == $itemID): ?>class="active"<?php endif; ?>>					
								<div style="height: 77px;">
									<span class = "thumbnail"> 
										<a href = "<?php echo $link ?>"> 
											<img src = "<?php echo $thumbnail ?>" width="90" height="65" /> 
										</a>
									</span>
									<h3> 
										<strong id = "album<?php echo $itemID ?>"> <a href = "<?php echo $link ?>"> <?php echo $title  ?> </a> </strong>
										<?php if($item instanceof VideoAlbum): ?>
											<span class="control" id = "control<?php echo $itemID ?>" style = "display:none">
												<?php if('Traveler' == $ownerType): ?>
													<form action = "/videoAlbum.php?action=saveAlbum&amp;albumID=<?php echo $itemID ?>&amp;travelerID=<?php echo $travelerID ?>" method = "post" onSubmit = "return VideoAlbum.validateAlbumTitle(<?php echo $itemID ?>)">
												<? else: ?>
													<form action = "/videoAlbum.php?action=saveAlbum&amp;albumID=<?php echo $itemID ?>&amp;gID=<?php echo $groupID ?>" method = "post" onSubmit = "return VideoAlbum.validateAlbumTitle(<?php echo $itemID ?>)">
												<? endif; ?>
														<input type = "text" id = "txtTitle_<?php echo $itemID ?>" name = "txtTitle" value = "<?php echo htmlspecialchars($item->getTitle()) ?>">
														<input type = "submit" name = "btnSubmit" value = "Save">
														<input type = "button" name = "btnCancel" value = "Cancel" onclick = "VideoAlbum.cancelEditAlbum(<?php echo $itemID ?>, '<?php echo addslashes(htmlspecialchars($item->getTitle())) ?>')">
													</form>
											</span>
										<?php endif; ?>
									</h3>
									<span id="album_control<?php echo $itemID ?>"> 
										<?php if ($isOwner && $item instanceof VideoAlbum): ?>
											<a href="javascript:void(0)" onclick = "VideoAlbum.editAlbum(<?php echo $itemID ?>, '<?php echo addslashes(htmlspecialchars($item->getTitle())) ?>')">Edit</a>  
											<?php if (0 == count($videos)): ?> 
												<a href = "javascript:void(0)" id = "deleteAlbum" onclick = "VideoAlbum.showDeleteCustomPopUp(<?php echo $itemID ?>, <?php echo $ownerID ?>, '<?php echo $ownerType ?>')">Delete</a> 
											<?php endif; ?>
										<?php elseif ($item instanceof Travel): ?>
											<a href="/journal-entry.php?action=view&amp;travelID=<?php echo $itemID ?>">Go to Journal</a>
										<?php endif; ?> 
									</span>
									<p style="width:100px;overflow:hidden;"><?php echo count($videos) ?> Video<?php if(1 < count($videos)): ?>s<?php endif; ?></p>
								</div> 
							</li>
					<? endforeach; ?>
				</ul>
			
				<?php $strips = array(); ?> 
			<?php endif; ?>
		<?php endforeach; ?>
		
		<?php if( 0 < count($strips) ): ?>
			<ul id="slider_section-<?=++$curStripCount?>" style="height: <?=ceil(count($strips))*92+(ceil(count($strips))*2)+2?>px;">
				<?php foreach($strips as $y => $item): ?>
					<?php 
						$videos = 0;
						$link = "/video.php?action=get".$ownerType."Videos&amp;";
						$link .= (isset($travelerID)) ? "travelerID" : "gID";
						$link .= "=".$ownerID."&amp;";
						
						if($item instanceof VideoAlbum) {
							$itemID = $item->getAlbumID();
							$videos = $item->getVideos(); 
							$link .= "albumID=".$itemID."&amp;type=album"; 
						}
						
						else if ($item instanceof Travel) {
							$itemID = $item->getTravelID();
							$videos = $item->getTravelVideos();
							$link .= "travelID=".$itemID."&amp;type=journal";
						}
						
						$title = (20 < strlen($item->getTitle())) ? substr($item->getTitle(), 0, 20).'...' : $item->getTitle();
						$thumbnail = (0 < count($videos)) ? $videos[0]->getVideoImageUrl() : '/images/default_images/120_video.gif';			
					?>
						<li <?php if ($collectionID == $itemID): ?>class="active"<?php endif; ?>>					
							<div style="height: 77px;">
								<span class = "thumbnail"> 
									<a href = "<?php echo $link ?>"> 
										<img src = "<?php echo $thumbnail ?>" width="90" height="65" />
									</a>
								</span>
								<h3> 
									<strong id = "album<?php echo $itemID ?>"> <a href = "<?php echo $link ?>"> <?php echo $title  ?> </a> </strong>
									<?php if($item instanceof VideoAlbum): ?>
										<span class="control" id = "control<?php echo $itemID ?>" style = "display:none">
											<?php if('Traveler' == $ownerType): ?>
												<form action = "/videoAlbum.php?action=saveAlbum&amp;albumID=<?php echo $itemID ?>&amp;travelerID=<?php echo $travelerID ?>" method = "post" onSubmit = "return VideoAlbum.validateAlbumTitle(<?php echo $itemID ?>)">
											<? else: ?>
												<form action = "/videoAlbum.php?action=saveAlbum&amp;albumID=<?php echo $itemID ?>&amp;gID=<?php echo $groupID ?>" method = "post" onSubmit = "return VideoAlbum.validateAlbumTitle(<?php echo $itemID ?>)">
											<? endif; ?>
													<input type = "text" id = "txtTitle_<?php echo $itemID ?>" name = "txtTitle" value = "<?php echo htmlspecialchars($item->getTitle()) ?>">
													<input type = "submit" name = "btnSubmit" value = "Save">
													<input type = "button" name = "btnCancel" value = "Cancel" onclick = "VideoAlbum.cancelEditAlbum(<?php echo $itemID ?>, '<?php echo addslashes(htmlspecialchars($item->getTitle())) ?>')">
												</form>
										</span>
									<?php endif; ?>
								</h3>
								<span id="album_control<?php echo $itemID ?>"> 
									<?php if ($isOwner && $item instanceof VideoAlbum): ?>
										<a href="javascript:void(0)" onclick = "VideoAlbum.editAlbum(<?php echo $itemID ?>, '<?php echo addslashes(htmlspecialchars($item->getTitle())) ?>')">Edit</a>  
										<?php if (0 == count($videos)): ?> 
											<a href = "javascript:void(0)" id = "deleteAlbum" onclick = "VideoAlbum.showDeleteCustomPopUp(<?php echo $itemID ?>, <?php echo $ownerID ?>, '<?php echo $ownerType ?>')">Delete</a>
										<?php endif; ?> 
									<?php elseif ($item instanceof Travel): ?>
										<a href="/journal-entry.php?action=view&amp;travelID=<?php echo $itemID ?>">Go to Journal</a>
									<?php endif; ?> 
								</span>
								<p style="width:100px;overflow:hidden;"><?php echo count($videos) ?> Video<?php if(1 < count($videos)): ?>s<?php endif; ?></p>
							</div> 
						</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		</div>
	</div>
		
	<?php if( $stripsVisible < $stripCount ): ?>	
		<div id="btnNextStrip" class="bottombuttons">
			<a href="" class="downmore" onclick="paginatedCollectionList.scrollToNextStrip(); return false;">
				<span>down</span>
			</a>
		</div>					
	<?php endif; ?>					
</div>	

<?php $startPos = floor($defaultIndex / $collectionsPerStrip) + 1; ?>

<script type="text/javascript">
	jQuery(document).ready(function(){
		paginatedCollectionList.setSlider(contentSlider,"slider_section",<?php echo $startPos ?>,<?php echo $collectionsPerStrip ?>,<?php echo $stripCount ?>);
	});
</script>