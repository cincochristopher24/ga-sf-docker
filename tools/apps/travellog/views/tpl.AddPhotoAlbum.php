<?php
	ob_start();
	
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'page_photos');
	ob_end_clean();
	
?>
		<?= $profile->render()?>
		<? $subNavigation->show(); ?>
		
		<div id="content_wrapper">				
			<div id="wide_column">
				<div id="photo_box" class="section">	
					<h2><span>Add Photo Album</span></h2>	
					<div class="content">																								
						<?php if ( $error ) : ?>
							<div class="errors">								
								<ul>									
									<li><?= $error ?></li>									
								</ul>
							</div>
						<?php endif; ?>
						
						<div class="photo_help">								
							<form action="" method="post">
  					  	  		<p><span>Create a title for your album</span></p> 
				  				<p><label>Title:</label><input type="text" name="txttitle" size="35" MAXLENGTH="24" /></p>				  			
								<p class="button_cont"><input type="submit" name="btnnext" value="Next" /></p>
						 	</form>
						</div>								
					</div>
					<div class="foot"></div>						
				</div>
			</div>
			<div class="clear"></div>
		</div>

				
		
