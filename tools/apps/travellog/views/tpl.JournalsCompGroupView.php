<?
require_once 'Class.StringFormattingHelper.php';
$currentGroupID = 0;
if( isset($_GET["gID"]) ){
	$currentGroupID = intval($_GET["gID"]);
}else if( isset($GLOBALS["CONFIG"]) ){
	$currentGroupID = $GLOBALS["CONFIG"]->getGroupID();
}
?>
  <div id="journals_summary" class="section" >	
	<a name="anchor"></a>

	<h2>
		<span><?=$compTitle?></span>
		<?if ($canManage):?>
			<span class="header_actions"><a href="/journal.php?action=groupjournals&amp;gID=<?=$subjectID?>">Manage Journals</a></span>
		<? else: ?>	
			<?php if(count($journals)): ?>
				<?php $journalEntriesCnt = ($subject instanceOf AdminGroup) ? ($subject->getJournalEntriesCount() + $subject->getArticlesCount()) : null ; ?>
				<span class="header_actions"><a href="/journal.php?action=groupjournals&amp;gID=<?=$subjectID?>">
					<?php if(is_null($journalEntriesCnt)): ?>
						View All Journal Entries
					<?php else: ?>	
						View <?php if($journalEntriesCnt > 1): ?>All <?php echo $journalEntriesCnt; ?><?php endif;?> Journal Entr<?php if($journalEntriesCnt == 1): ?>y<?php else: ?>ies<?php endif; ?>
					<?php endif; ?>		
				</a></span>
			<?php endif; ?>	
		<?endif;?>
	</h2>
	
	
	<div class="content action_container" >		
		<div id="content">
			<?
				$country = '';
				$city = '';
				$jeCtr = 1;					// journal entry counter used for identifying yahoo carousel object				
			?>
			<?php if($subFeatured && count($journals) && $canManage): ?>
				<div style="text-align: center;" class="help_text">
					<span>
						There are no featured travel journals. All group approved journals are shown.
					</span>
				</div>
			<?php endif; ?>	
			<ul class="journals">
				<?foreach($journals as $journal):
					$jID = $journal->getTravelID();
					$jeCount = count($journalsAndEntries[$jID]);
					$jEntriesAr = $journalsAndEntries[$jID];
					
					if ($journal->getOwner() == null){
						echo 'travelid:' . $journal->getTravelID();
						continue;
					}
									
					$jah = new JournalAuthorUIHelper($journal->getOwner());
					$locationID = 0;
					if (isset($jEntriesAr[$jeCount-1])){
						$trip = $jEntriesAr[$jeCount-1]->getTrip();
						$location = $trip->getLocation();
						if(!is_null($location)){
							$country = $location->getCountry()->getName();
							$city = $location->getName();
							$locationID = $location->getCountry()->getCountryID();
						}
					} 
				?>
				
								
					 <li id="container<?=$jID?>">     
						<h3 id="entry_header">					
							<?if (isset($jEntriesAr[$jeCount-1])): ?>									
									<a class="journal_title" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>">
									<?=$jEntriesAr[$jeCount-1]->getTitle()?>
									</a>
							<?endif;?>
							
							<?if( isset($_SESSION['travelerID']) && $journal->getTraveler()->getTravelerID() == $_SESSION['travelerID'] ):?>
								&nbsp;
								<span  class="action_links" id="controls<?=$jID?>">
									<a class="button" href="/journal-entry.php?action=add&amp;travelID=<?=$jID?>">Add an Entry</a> 
									<?if (isset($jEntriesAr[$jeCount-1])): ?>									
										<a class="button" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>">Edit Journal</a>
									<?endif;?>
								</span>	
							<?endif;?>	
						</h3>
					
						
											
						<div class="content" id="content<?=$journal->getTravelID()?>">					 
			    			<?if (isset($journalPhotos[$jID])):?>
							 <div class="photo">
								<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>"> 
									<img class="float_right" src="<?=$journalPhotos[$jID]->getPhotoLink('featured')?>" alt="journal photo" />
								</a>							
							 </div>	
						   <?endif;?>
							
							<?if (isset($jEntriesAr[$jeCount-1])):?>								
								
								<p class="meta">
									<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" alt="Travel Journal from <?=$country?>" style="margin: 0 0 0 0;"/>						
									<?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?> | <?=(strtotime($trip->getArrival())) ? date('D M d, Y',strtotime($trip->getArrival())).'' : ''?>
								</p>
								<p class="entry_preview">
									<?if ($jeCount):?>
										<?= StringFormattingHelper::wordTrim($jEntriesAr[$jeCount-1]->getDescription(), 70, '&#8230;')?>
										<?if (false):?>
											<?=(strlen(trim($jEntriesAr[$jeCount-1]->getDescription())) > 175 )? strip_tags(nl2br(substr($jEntriesAr[$jeCount-1]->getDescription(),0,175))) . '...' : strip_tags(nl2br($jEntriesAr[$jeCount-1]->getDescription()))?>
										<?endif;?>
									<?endif;?>	
								</p>
							<?else:?>
								<div class="help_text"> <span>There are no entries for this journal</span>.</div>
								<?if( isset($_SESSION['travelerID']) && $journal->getTraveler()->getTravelerID() == $_SESSION['travelerID'] ):?>
									<p  class="action_links" id="controls<?=$jID?>">
										<a href="/journal-entry.php?action=add&amp;travelID=<?=$jID?>">Add an Entry</a> | 
										<a href="javascript:void(0)" onclick="CustomPopup.initialize('Delete Journal ?','Are you sure you want to delete this journal?','/travel.php?action=delete&amp;travelID=<?=$jID ?>','Delete','1','All entries, photos and other traveler comments linked to this journal will also be deleted. This action cannot be undone.');CustomPopup.createPopup();" >Delete</a>
										<!--a href="/travel.php?action=delete&amp;travelID=<?=$jID ?>" onclick="return confirm('Are you sure you want to delete the journal, <?=$journal->getTitle()?>? All of your entries and photos linked to this journal and comments from other travelers will also be deleted. This cannot be undone. ');">Delete</a-->
									</p>
								<?endif;?>
							<?endif;?>
							
								<?if (isset($relatedGroupsAr[$jID][$subjectID])):
									 $group = $relatedGroupsAr[$jID][$subjectID];
									 $site = $group->getServerName();
									$local = "";
									if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
										$local = ".local";
									}	
								?>
									<p class="entry_group_tag <?if (!$journal->getTraveler()->isAdvisor()):?>not_author<?endif;?>">
										<?if($site != false):?>	
											<a href="http://<?=$group->getServerName().$local?>" title="<?= $group->getName()?>">	
												<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
											</a>
											<a href="http://<?=$group->getServerName().$local?>" alt="" class="author_username">
											<?=$group->getName()?>
											</a>
											
											<br/>
											
											<span>Advisor Group Journal</span>	
											
												
			
										<?else:?>
											<a href="/group.php?gID=<?=$group->getGroupID()?>" class="<?= $group->getName()?>">
												<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
											</a>
											<a href="/group.php?gID=<?=$group->getGroupID()?>" class="author_username">
											<?=$group->getName()?>	
											</a>
											<br/>
											<span>Advisor Group Journal</span>	
			
										<?endif;?>
									</p>
								<?endif;?>	
							
							<?if ($showAuthor):?>				    
									<?/*!-- <a href="<?=$jah->getString('OWNER_URL')?>" title="Read more of <?=$jah->getString('OWNER_URL')?> travel experiences and adventure!"><img src="<?=$jah->getString('PHOTO_THUMBNAIL_URL')?>" width="37px" height="37px"  /></a> -->  
									<? /*<p class="author_username">author: <a href="<?=$jah->getString('OWNER_URL')?>" title="Read more of <?=$jah->getString('OWNER_URL')?> travel experiences and adventure!"> <strong><?=HtmlHelpers::truncateWord($jah->getString('OWNER'), 13)?></strong></a></p>
									*/?>
									<? $owner = $journal->getTraveler(); ?>
									<? if( !$journal->isGroupJournal() )://!$owner->isAdvisor($currentGroupID) ): ?>
										<p class="entry_group_tag">
											<span>
												<a class="username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$owner->getUserName()?>">
													<img class="pic" width="37" height="37" src="<?=$owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" title="Read more of <?=$owner->getUserName()?> travel experiences and adventure!" />
												</a>
											</span>					
											<a class="author_username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$owner->getUserName()?>"><strong><?=$owner->getUserName()?></strong></a>
											<?if($canAddJournal):?>	
												<strong class="meta"><em> <?=$owner->getFullName()?> </em></strong>
											<?endif;?>
											<br />		
											<!--nick aguilos | 2-04-2009-->									
											<!--<strong class="journal_label">Journal:</strong>-->
											<img id="journal" src="/images/journal_icon2.gif"></img>		
											<?if (isset($jEntriesAr[$jeCount-1])):?>
											
												<a class="journal_title" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" title="<?=$journal->getTitle()?>"><?=$journal->getTitle()?></a>
											<?else:?>
												<strong><?=$journal->getTitle()?></strong>
											<?endif;?>
											<!-- end of edits -->
																			
										</p>
									<? elseif( !isset($relatedGroupsAr[$jID]) ) : ?>
										<? 	//$group = new AdminGroup($owner->getAdvisorGroup());
											$group = new AdminGroup($currentGroupID);
											if( 0 < $group->getParentID() ){
												$group = $group->getParent();
											}
											$site = $group->getServerName();
											$local = "";
											if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
												$local = ".local";
											}
										?>
										<p class="entry_group_tag">
											<?if($site != false):?>	
												<a href="http://<?=$group->getServerName().$local?>" title="<?= $group->getName()?>">	
													<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
												</a>
												<a href="http://<?=$group->getServerName().$local?>" alt="" class="author_username">
													<strong><?=$group->getName()?></strong>
												</a>
												<!-- NICK AGUILOS -->
												<br />
												<!--<strong class="journal_label">Journal:</strong>-->
												<img id="journal" src="/images/journal_icon2.gif"></img>
												<?if (isset($jEntriesAr[$jeCount-1])):?>
													<a class="journal_title" href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" title="<?=$journal->getTitle()?>"><?=$journal->getTitle()?></a>
												<?else:?>
													<strong><?=$journal->getTitle()?></strong>
												<?endif;?>
												<!-- end of edits -->										
											</a>
											
											
											<?else:?>
												<a href="<?=$local?>/group.php?gID=<?=$group->getGroupID()?>" class="<?= $group->getName()?>">
													<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
												</a>
												<a href="<?=$local?>/group.php?gID=<?=$group->getGroupID()?>" class="author_username">
													<strong><?=$group->getName()?></strong>
												</a>	
												
												
											<?endif;?>
										</p>
									<? endif; ?>
							<?endif;?>						
						</div>
								
						<div class="entry_box">					
								
						</div>
					</li>
				<?endforeach;?>
				</ul>
			
				<? if (isset($pagingComp)):?>	
				    	<? $pagingComp->showPagination() ?>
				<? endif; ?>
			
			<?if (count($journals) == 0){
				// echo '<p class="help_text"><span>' . $noJournalMessage  . '</span></p>';
			?>
				<div class="gline">
					<div class="gunit image">
						<img src="/images/g_journals.jpg" alt="Travel Journals" width="165" height="145" />
					</div>
					<div class="glastUnit description">
						<h3>Share program experiences through blogs!</h3>
						<p>
							You can write blogs for your group, or you can feature selected members' blogs.					
						</p>
						<a class="button_v3 goback_button jLeft" href="/journal.php?action=add&amp;contextGroupID=<?=$currentGroupID?>"><strong>Add Journal</strong></a>
					</div>
				</div>
			<? }?>

		</div>
		
		<div class="yui-skin-sam">
			<div id="dlg">
			</div>
		</div>
	</div>
</div>
<div id="dlg_container">
</div>
<div id="dlg"></div>