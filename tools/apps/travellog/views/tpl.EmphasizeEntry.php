<?
$callout = trim($contents['obj_entry']->getCallout());
?>
<?=$sub_view['profile_view']->render()?>
<?=$sub_view['entry_sub_navigation_view']->render()?>
<div id="content_wrapper" class="control_divider">
	<?=$sub_view['journal_view']->render()?>
	<?=$contents['comments_view']->fetchShoutOutPopUp()?>
	<div id="wide_column">

            	    <?=$sub_view['title_callout_photo_view']->render()?>
                    <?=$sub_view['no_records_view']->render()?>


    		        <?=$sub_view['entry_primary_photo_view']->render()?>


		<div class="entry_content">	

               	<div class="entrysideline">
					<?=$sub_view['highlights_view']->render()?>
					<?= $sub_view['entry_view_videos']->render() ?>
				</div>
				
				<div class="entrymiddleline">
				
				<?php if ($callout != '' || $contents['show_admin_controls']): ?>

				<p class="entrycallout">
					<span class="leftquote">&nbsp;</span><span id="ENTRYCALLOUT"><?=trim($callout)?></span><span class="rightquote">&nbsp;</span>
				</p>
				<?if( $contents['show_admin_controls'] ):?>
    				<a class="entry_links bottom_shift" href="javascript:void(0)" id="EntryCalloutLink" rel="<?=$contents['obj_entry']->getTravelLogID()?>"><span><?if( strlen($callout) ):?>Edit<?else:?>Add<?endif;?> Entry Callout</span></a>
			    <?endif;?>

<?php endif;?>
                    	<?=$sub_view['desc_view']->render()?>
                 	    <?=$sub_view['tips_view']->render()?>
						<?/*?><p class="articleviewer bookmark_controls"><strong>Bookmark this Entry:</strong>
							<?=$sub_view['bookmark_controls']->render()?>
						</p>*/?>
                        <div id="comments" class="section">
							<?=$contents['comments_view']->renderAjax()?>
						</div>
                </div>
        </div>
	</div>

	<div id="narrow_column">
	
	<?php if( $contents['show_admin_controls'] ):?>
	<a class="entry_links bottom_shift deletentry" href="javascript:void(0)" onclick="CustomPopup.initialize('Delete a Journal Entry?','Are you sure you want to delete this entry?','/journal-entry.php?action=delete&travellogID=<?php echo $contents['obj_entry']->getTravelLogID()?>','Yes');CustomPopup.createPopup();"><span>Delete</span></a>
<?php endif;?>	
    	<div id="travel_map" style="width:203px; height:203px"></div>
    			
		<div id="gallerycontainer"><?=$sub_view['thumbnail_view']->render()?></div>	

		<?=$sub_view['toc_view']->render()?>
		<!-- HTML Version of the New Journal Entry List -->		
        <div id="gaothers">
        	<?=$sub_view['ads_view']->render()?>
        </div>

 	 </div>
</div>

<!-- 
	informs the user if loading of his video's attributes was successful
	added by neri: 11-26-08 
-->

<? if ($contents['fail']): ?>
	<script type = "text/javascript">
		alert("Sorry, a problem has been encountered while retrieving the attributes of this video. You may try adding this video again or you can try adding a different video.");
	</script>
<? endif; ?>

<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$.ajax({
				url: "/retrieve-map.php",
				data: "context=1&travellogID=<?php echo $contents['obj_entry']->getTravelLogID()?>&travelID=<?php echo $contents['obj_entry']->getTravelID()?>",
				type: "GET",
				success: function(res){
					$('#travel_map').attr('class','map');
					eval(res);
				}
			});	
		});
	})(jQuery);
	
	<?php if( $contents['show_admin_controls'] ):?>
		window.onbeforeunload = function(e){
			if( null != document.getElementById("content_tbl") ){
				return "You might lose some unsaved text if you do so. You may want to save your entry properly before navigating away from this page.";
			}
		}
	<?php endif; ?>
</script>