<?php
require_once("travellog/views/Class.AbstractView.php");
class GPlusOneControlView extends AbstractView{
	
	function render(){
		$this->obj_template->set('contents', '');
		return $this->obj_template->fetch('tpl.GPlusOneControlView.php');
	}
}