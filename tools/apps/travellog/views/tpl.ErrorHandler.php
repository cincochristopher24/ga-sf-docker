<?php
header("HTTP/1.0 404 Not Found");
?>
<html>
	<head>
		<title> Error </title>
		<style type="text/css">
			@import url("http://images.goabroad.com/css/goabroad2.css");
			body {
			background-color: white;
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 11px;
			line-height: 1.5em;
			margin: 0;
			padding: 0;
			}
			h1 {
			margin: 25px;
			}
			a {
			font-weight: normal;
			}
			a img {
			border-width: 0;
			}
		</style>
	</head>
	<body bgcolor = "#ffffff">
		<table width = "90%" border = "0" cellpadding = "5" cellspacing = "5" align="center">
		<tr>
			<td align = "center">
				<a href="http://www.goabroad.net/"><img src="http://www.goabroad.net/images/header_logo_transparent.gif" alt="GoAbroad.net logo" title="GoAbroad.net"></a>
				<h1>Error</h1> 
				<p>There has been an error on this page.<br />
				
				<p> Please report bad links to the <a href="mailto: error@goabroad.net"> webmaster</a>.
				<br> 
				Be sure to let us know what page you were referred from and what page you are trying to access. Thank You.
				</td>
				</tr>
				<tr>
				<td align="center">
				&#169; Copyright  
				&nbsp; 2006-2007 | 
				GoAbroad.net 
				<br> 
				<a href="http://www.goabroad.net/feedback.php"> Contact GoAbroad.net </a> </font>
			</td>
		</tr>
		</table>
	
	</body>
</html>
