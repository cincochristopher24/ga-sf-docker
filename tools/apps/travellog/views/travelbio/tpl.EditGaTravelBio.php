<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'page_editprof');
	Template::setMainTemplateVar('page_editprof', 'My Passport');	
	Template::includeDependentJs('/js/utils/Class.Request.js');
	$code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	function confirm_entry(){
		input_box = confirm("Are you sure you want to unsynchronize your GoAbroad.Net and Facebook Travel Bio?");
		if (input_box){ 
			var req = new Request();
			req.registerEvent('onSuccess',function(){
				alert("Synchronization successful!");
				window.location.reload();
			});
			req.registerEvent('onFail',function(){alert('Fail...')});
			req.registerEvent('onProcess',function(){});
			req.setMethod('POST');
			//req.addFormVariable('action', 'unsynchronize');
			req.sendRequest("/travelbio/popup.php?action=unsynchronize");
		}
	}
//]]>
</script>
BOF;
	Template::includeDependent($code);	
	
	Template::includeDependentJs ( "/js/jquery-1.1.4.pack.js"           );

	Template::includeDependentCss( "/min/f=css/Thickbox.css"                  );
	Template::includeDependentCss( "/lytebox/lytebox.css"               );

	Template::includeDependentJs (
					array(
						"/lytebox/lytebox.js",
						"/js/interface/interface.js",
						"/js/interface/source/idrag.js",
						"/js/interface/source/idrop.js",
						"/js/interface/source/isortables.js",
						"/js/ajaxfileupload.js",
						"/js/jeditable.js"
					));
	Template::includeDependentJs ( "/js/yahooUI/carousel/yahoo-dom-event.js"           );
	Template::includeDependentJs ( "/js/yahooUI/carousel/utilities.js"           );
	Template::includeDependentJs ( "/js/yahooUI/carousel/dragdrop-min.js"           );
	Template::includeDependentJs ( "/js/yahooUI/carousel/container_core-min.js"           );
	Template::includeDependentJs ( "/js/yahooUI/carousel/carousel.js"           );

	Template::includeDependentCss( "/js/yahooUI/carousel/carousel.css"               );
	Template::includeDependentJs ( "/min/f=js/prototype.js");

	Template::includeDependentJs (array("/js/thickbox.js","/js/PhotoUILayout.js"));



	$photo_script =<<<BOF
	<script type = "text/javascript">
		jQuery.noConflict();
		PhotoService = new mPhotoService('profile', $travelerID, $travelerID);
		PhotoService.setLayout(1); 
		//PhotoService.setisOwner(1); 

	</script>
BOF;

	Template::includeDependent($photo_script);
	
	$subNav-> show();
?>
<div id="content_wrapper" class="layout_2">	
	<div id="wide_column">
		<div id="travelbio_questions" class="section">						
			<?php if('SHOW_FORM' == $tplAction): ?>			
					<h2><span> <?= $labelAction ?> </span></h2>
					<div class="content">						
						<form name = "frmEdit" action = "editgatravelbio.php" method = "post" id="mytravelbio_form">				
							<?php 
								$ctr = 0;
								foreach($profileQuestions as $iQuestion): 
							?>
								<p>
									<div><?= ++$ctr . '. ' . $iQuestion->getQuestion() ?> </div>
									<div>
									<?php 
										if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT): 
											$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
												? $structAns[$iQuestion->getQuestionID()][0]->getValue()
												:'';
									?>
									<input type="text" class="text" name="txt_<?= $iQuestion->getQuestionID() ?>" value="<?= htmlentities($val) ?>" />
									<?php elseif($iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA): 
											$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
												? $structAns[$iQuestion->getQuestionID()][0]->getValue()
												:'';
									?>
										<textarea cols="30" rows="5" name="txa_<?= $iQuestion->getQuestionID() ?>"><?= htmlentities($val) ?></textarea>
									<?php elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX): ?>				
										<ul class="checkbox_list">					
											<?php 						
												$choices = $iQuestion->getChoices();
												$selections = array();
												$textVal = '';						
												if(array_key_exists($iQuestion->getQuestionID(),$structAns)){
													foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){							
														if($iChoice->isTypeCheckbox()){
															$selections[] = htmlentities($iChoice->getValue()); 
														}
														elseif($iChoice->isTypeText()){
															$textVal = htmlentities($iChoice->getValue());
														}
													}
												}
												foreach($choices as $iChoice):
											?>
												<li class="checkbox_list_item">
													<input type="checkbox" name="chk_<?= $iQuestion->getQuestionID() ?>[]" style="width:auto" value="<?= htmlentities($iChoice->getChoice()) ?>" <?= in_array($iChoice->getChoice(),$selections)?'checked="true"':'' ?>  />&nbsp;<?= $iChoice->getChoice() ?>
												</li>
											<?php endforeach;?>					
											<li class="checkbox_list_item checkbox_list_item_text">
												<input type="text" class="text" name="txtchk_<?= $iQuestion->getQuestionID() ?>" value="<?= htmlentities($textVal) ?>" />	
											</li>
										</ul>
									<?php endif; ?>
									</div>
								</p>
							<?php endforeach; ?>
							<ul>
								<li class="actions">								
									<input name = "submit" class = "submit" value = "Save" type = "submit" />
								</li>
							</ul>
						</form>												
					</div>
									
					
			<?php elseif('SHOW_SUMMARY' == $tplAction): ?>
					<h1><span>Travel Bio Saved.</span></h1>
					<div class="content">						
						Your Travel Bio has been saved. You can <a href="/profile.php?action=view&amp;travelerID=<?= $travelerID ?>">go to your profile</a> or you may <a href="/editgatravelbio.php">continue to edit your Travel Bio</a>.
					</div>
			<?php endif; ?>
		</div>
	</div>
	
	<div id="narrow_column">		
		<div id="quick_tasks" class="section" >
				<h2><span>Profile Tasks</span></h2>
				<div class="content">
					<ul class="actions">
						<li>
							<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(1,1,'gallery')" class="button">Manage Profile Photos</a> 					
						</li>
						<li>
							<a href="javascript:void(0)" class="button" onclick="window.open('changepassword.php?action=entry','ChangePassword','width=500,height=230,navigation toolbar=disabled,left=200,top=200')" >Change Password</a>
						</li>
						<li>
							<a href="privacy.php" class="button">Privacy Preferences</a>
						</li>
						
						<?if (false):?>
						<li>
							<a href="student-profile.php" class="button">Virtual Advisor</a>					
						</li>
						<li>
							<a href="resume.php?action=view" class="button">Edit/View Resume</a>					
						</li>
						<?endif;?>
					</ul>
					
				</div>
			</div>				
		<div id="travelbio_info_txt" class="section">												
				<p>
					<strong> Share your travel insights and experiences by answering your Travel Bio! </strong>
				</p>
				<?php if (!$isSync) : ?>
				<p>
					<strong> <u> Tip: </u> </strong>
				</p>
				<p>
					Show your friends how well-traveled you are by adding the <strong> Travel Bio </strong> app to your
					Facebook profile. You can share it with your networks and compare it with your friends Bio's! <br />
				</p>
				<p>
					To export your answers to Facebook, just synchronize your accounts after adding the Travel 
					Bio app to your Facebook profile. <br /> <a href = "javascript:void(0);" onclick = "window.open('/travelbio/popup.php?action=display', 'window2', 'resizable=yes,scrollbars=no,width=420,height=250, top=' + ((screen.width/2)- 400) + ', left=450'); return false"> What is Synchronize? </a>
				</p>
				<?php else : ?>				
				<p>	
					Your Travel Bio is synchronized with your Facebook profile app. Editing your answers here will 
					also change them on your Facebook profile.
				</p>
				<p>
					To edit your answers separately, or to associate this GoAbroad Network account with a different Facebook account, you may 
					<a href = "javascript:void(0);" onclick = "confirm_entry();"> remove synchronization </a> here.
				</p>				
				<?php endif; ?>														
			</div>	
	</div>
</div>