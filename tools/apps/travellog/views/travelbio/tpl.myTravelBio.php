<fb:dashboard></fb:dashboard>
<?php 
	if ($isOwnerUser) : 
		echo $dashboard;
	else : 
		if(isset($_REQUEST['fb_sig_in_profile_tab'])) : 
			echo '<fb:visible-to-owner>'.$dashboard.'</fb:visible-to-owner>';
		 else :
			echo '<h5 style="margin-left:45px"><fb:name uid="'.$user_id.'" possessive="true" /> travel bio</h5>';
		endif;
	endif;
?>

<style type="text/css"> <?=$style; ?> </style>
<div style="margin:25px 70px;">
<ul>
	<?php
		foreach($profileQuestions as $iQuestion) :
			if(array_key_exists($iQuestion->getQuestionID(),$structAns)) : ?>
				<li class="entry"><h4 class="question" style"font-size:12px"><?=$iQuestion->getIntroduction()?></h4>

	<?php 		if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT || $iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA) :

					$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
						? $structAns[$iQuestion->getQuestionID()][0]->getValue()
						: ''; ?>
					<img class="answer_icon" src="<?=FBini::getAnswerIconPath() ?>"/>
					<p class="answer"><?=$val?></p>
					<div class="clear"></div>

	<?php 		elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX) :

					$textVal = '';
					foreach($structAns[$iQuestion->getQuestionID()] as $iChoice) :
						if($iChoice->isTypeCheckbox() && 'Others' != $iChoice->getValue()) : ?>
							<img class="answer_icon" src="<?=FBini::getAnswerIconPath() ?>"/>
							<p class="answer"><?=$iChoice->getValue()?></p>
							<div class="clear"></div>
	<?php               elseif($iChoice->isTypeText()) :
							$textVal = $iChoice->getValue(); 
						endif;
					endforeach;

					if(strlen($textVal)) : ?>
						<img class="answer_icon" src="<?=FBini::getAnswerIconPath()?>"/>
						<p class="answer"><?=$textVal?></p>									
	<?php			endif;			
				endif; ?>
			
				</li>
	<?php	endif;
	
		endforeach;

	?>
</ul>
</div>
<!--
<?php if(isset($_REQUEST['fb_sig_in_profile_tab'])) : ?>
	<script>
		function hideDashboardContainer() {
			var nodes = document.getElementById('dashboardContainer').childNodes;
			for (var i=0; i < nodes.length; i++) {
				if (nodes[i].nodeName == 'SPAN') {
					var child_childNodes = nodes[i].childNodes;
				   	for (var ii=0; ii < child_childNodes.length; ii++) {
				   		if (child_childNodes[ii].nodeName == 'SPAN') {
							child_childNodes[ii].style.display = 'none';
						}
				   	}
					break;
				}
			}
		}
	</script>
<?php endif; ?>
-->