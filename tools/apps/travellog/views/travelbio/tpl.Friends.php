<fb:dashboard></fb:dashboard>
<?= $dashboard ?>
<fb:request-form action="index.php" method="POST" invite="true" type="TravelBio" content="You have been invited to add the My Travel Bio application! <?= htmlentities('<fb:name uid="'.$fbUserID.'" firstnameonly="true" />') ?>  wants you to add My Travel Bio so that you can share your travel experiences!">
	<fb:multi-friend-selector actiontext="Below are your friends who do not have My Travel Bio. Invite them to share their travel experiences with you!" rows="5" exclude_ids="<?= $excludedUIDList ?>"/>
</fb:request-form>

<?php exit; ?>

<form action="friends.php" method="post">
		<?php if($invitesSent):	?>
			<div style='border: 1px solid rgb(192, 192, 192); padding: 10px; overflow: auto; margin: 15px; font-family: "lucida grande",tahoma,verdana,arial,sans-serif;'>
				Invites were successfully sent.
			</div>
		<?php endif; ?>
		<div style='border: 1px solid rgb(192, 192, 192); padding: 10px; overflow: auto; margin: 15px; height: 275px; width: 593px; font-family: "lucida grande",tahoma,verdana,arial,sans-serif;'>
		<?php
			$createTag = true;
			$ctr = 0;		
			foreach($uids as $iUid): 
				if($createTag){
					$createTag =false;
				}
		?>		
			<div style="float: left; width: 180px; margin-bottom: 8px;">
				<div style="float: left; margin-right: 5px;">
					<input type="checkbox" name="chk_user[]" style="width:auto" value="<?= $iUid['uid'] ?>" <?= $iUid['has_added_app']==1?'disabled':''  ?> />&nbsp;
				</div>
				<div style="float: left; margin-right: 5px;">
					<fb:profile-pic uid="<?= $iUid['uid'] ?>" linked="no" size="square" width="30" /> &nbsp; 
				</div>
				<div style="overflow: hidden; float: left; margin-bottom: 2px; height: 50px; width: 100px;">
					<?php if($iUid['has_added_app']==1): ?> 
						<a href="http://apps.facebook.com/travelbio/view.php?uid=<?=$iUid['uid'] ?>"><fb:name uid="<?= $iUid['uid'] ?>" />(with TravelBio)</a> 
					<?php else: ?>
						<fb:name uid="<?= $iUid['uid'] ?>"  />
					<?php endif; ?>
				</div>
			</div>
		<?php
			if(++$ctr / 3 == 0 ){
				$createTag = true;
			}	
			endforeach;
			if(!$createTag){
			}
		?>	
		</div>
	<div style="margin: 15px;">
		<input type="submit" name="submit" value="Send Invitation" />
	</div>
</form>