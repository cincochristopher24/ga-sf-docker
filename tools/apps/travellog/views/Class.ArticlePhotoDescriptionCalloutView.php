<?php
require_once("travellog/views/Class.AbstractView.php");
class ArticlePhotoDescriptionCalloutView extends AbstractView{
	function render(){
		$this->obj_template->set( 'sub_view', $this->sub_views );
		$this->obj_template->set( 'contents', $this->contents );
		return $this->obj_template->fetch('tpl.ArticlePhotoDescriptionCalloutView.php');
	}
	
	function renderForAjax(){
		$this->obj_template->set( 'sub_view', $this->sub_views );
		$this->obj_template->set( 'contents', $this->contents );
		$this->obj_template->out('tpl.ArticlePhotoDescriptionCalloutView.php');
	}
}
?>
