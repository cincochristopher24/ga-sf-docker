<?php if( count($featuredGroups) || $isAdminLogged ): ?>
	<div class="section newMedia" id="sub_groups">
		<h2>
			<span>Subgroups</span>
			<span class="header_actions">
		    <?php if($isAdminLogged): ?>
		    	<a href="/group.php?mode=add&amp;pID=<?=$grpID?>">Add Subgroup</a> |
		    <?php endif; ?>
			<?php if(isset($GLOBALS["CONFIG"]) ): ?>
				<?php if($isAdminLogged): ?>
					<a href="/group-pages.php" ><?= $link_label ?></a>
				<?php else: ?>
					<a href="/group-pages.php?search" ><?= $link_label ?></a>
				<?php endif; ?>
			<?php else: ?>
				<a href="/group-pages.php?gID=<?=$grpID?>" ><?= $link_label ?></a>
			<?php endif; ?>
			</span>
		</h2>
		<div class="content">
			<?/*php if ($isAdminLogged && !$origFeaturedCount): ?>
				<p class="side_help_text">There are no featured subgroups. Six most recent subgroups are shown.</p>
			<?php endif; */?>
			<?php if(count($featuredGroups)): ?>
					<?php $i = 1;?>
					<?php foreach($featuredGroups AS $group):
						$subgroup_name = str_replace('-', '_', $group->getName());
						$subgroup_name = str_replace(' ', '-', $subgroup_name);
						$group_name = str_replace('-', '_', $parent->getName());
						$group_name = str_replace(' ', '-', $group_name);
				
						$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;
			
						// photo tag
						$cntPhotos = (0 < $group->getGroupPhotoID()) ? 1 : 0;
						$cntPhotos = $cntPhotos + $group->getAlbumPhotosCount();
			 
						$photo_tag_cnt = (1 < $cntPhotos)  ? $cntPhotos.' Photos' : $cntPhotos.' Photo';
						$photo_tag_cnt = (0 == $cntPhotos) ? "" : $photo_tag_cnt;
						// end of photo tag
			
						// journal tag
						// $cntJournals = $group->getJournalsCount();
						$cntJournals = $group->getJournalEntriesCount();
			
						// article tag
						$cntArticles = $group->getArticlesCount();
		  
						if($cntJournals > 0 && $cntArticles > 0){
							$journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journal Entries' : $cntJournals.' Journal Entry';
							$journal_tag_cnt .= (1 < $cntArticles) ? ' | '.$cntArticles.' Articles' : ' | '.$cntArticles.' Article';
						}else if($cntJournals <= 0 && $cntArticles > 0)
							$journal_tag_cnt = (1 < $cntArticles) ? $cntArticles.' Articles' : $cntArticles.' Article';
						else if($cntJournals > 0 && $cntArticles <= 0)
							$journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journal Entries' : $cntJournals.' Journal Entry';
						else
							$journal_tag_cnt = "";
						// end of journal tag

				    	// members tag
				    	$cntMembers = $group->getMembersCount();
						$members_tag_cnt = (1 < $cntMembers) ? $cntMembers.' Members' : $cntMembers.' Member';
						$members_tag_cnt = (0 == $cntMembers) ? '' : $members_tag_cnt;
						// end of members tag
			
						$group_name = (20 < strlen($group->getName()) && !strpos($group->getName()," ")) ? substr($group->getName(),0,20) . "..." : $group->getName();
						?>			
						
						<div class="list group"> 
					    	<div>								
					    		<a title="View Group Profile" class="thumb" href="<?=$friendlyURL?>">
					    			<img src="<?=$group->getGroupPhoto()->getPhotoLink('standard') ?>">
					    		</a>
					    		<p>
					    			<a href="<?=$friendlyURL?>" title="<?=$group->getName()?>">
					    				<strong><?=$group_name?></strong>
					    			</a>
					    			<br>
					    			<span>
					    				<?= $photo_tag_cnt ?>   <? if ($cntPhotos) { ?> <br /> <? } ?>
										<?= $members_tag_cnt ?> <? if ($cntMembers) { ?> <br /> <? } ?>
										<?= $journal_tag_cnt ?> <? if ($cntJournals || $cntArticles) { ?> <br /> <? } ?>
										
										<? if (0 == $cntMembers): ?>
											<? if (NULL != $loggedUser && FALSE == $loggedUser->isAdvisor()):
													$validGrpAccess = array(GroupAccess::$OPEN, GroupAccess::$OPEN_APPROVAL);
													if (!$isAdminLogged  && in_array($group->getGroupAccess(), $validGrpAccess) && in_array ($parent->getGroupAccess(), $validGrpAccess ) ):
														if(!$group->isInRequestList($loggedUser)):
															$message = 'You are about to join the '.strtolower($group->getLabel()).' '.$group->getName().'. Your request is subject to the administrator&lsquo;s approval and will be sent after clicking Join. You will be able to interact with this group once your request has been approved.';
															$refLink = '&amp;ref=groupsubgroup';

															if($group->getGroupAccess() == GroupAccess::$OPEN):
																$message = 'You are about to join ' . $group->getName().'. You will automatically be a member of the group after clicking Join.';
															endif;

															$strlinkjoin = ' Do you want to <a href="javascript:void(0)" onclick="CustomPopup.initialize(\'Join '.$group->getName().' ?\',\'Are you sure you want to join the '.strtolower($group->getLabel()).' '.$group->getName().'?\',\'/membership.php?mode=join&amp;gID='. $group->getGroupID().$refLink.'&confirm\',\'Join\',\'0\',\''.$message.'\');CustomPopup.createPopup();">join</a> this group?';
														endif;				
													endif;
												endif;

												echo  'Subgroup has no members yet.' ; echo (isset($strlinkjoin)) ? $strlinkjoin : '';				
											endif; ?>
					    			</span>
					    		</p>			
					    	</div> 
					    </div>
						<?php $cnt = $i % 3;?>
						<?php if($cnt == 0):?>
							<div class="clear"></div>
						<?php endif;?>
						<?php $i++; ?>
					<?php endforeach; ?>
			<?php else :?>
				<?php if($isAdminLogged): ?>
				<div class="gline">
					<div class="gunit image">
						<img src="/images/g_subgroups.jpg" alt="subgroups" width="227" height="144" />
					</div>
					<div class="glastUnit description">
						<h3>Does your organization have multiple projects?</h3>
						<p>
							Subgroup functions as a complete, independent group with its own messaging functions, discussion boards, and management tools. We recommend creating a unique subgroup for every physical group of participants you manage. For instance, "Study in Barcelona - Spring 2010".
						</p>
						<a class="button_v3 goback_button jLeft" href="/group.php?mode=add&amp;pID=<?=$grpID?>"><strong>Create a Subgroup</strong></a>
					</div>
				</div>
				<?php else: ?>
					<div class="gline">
						<div class="glastUnit description">
							<h3></h3>
							<p>
								This group has no subgroup yet.
							</p>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>	
	</div>
<?php endif; ?>