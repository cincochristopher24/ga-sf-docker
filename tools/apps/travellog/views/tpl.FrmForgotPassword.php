<?php
	/*
	 * tpl.FrmForgotPassword.php
	 * Created on Feb 13, 2007
	 * created by marc
	 */
	 
	require_once("Class.Constants.php"); 
	
	/***************************************************************
	 * edit of neri:	01-06-09
	 * 		added network to the url variables of captcha-info.php
	 ***************************************************************/
	 
?>
<!--<div class="area" id="intro">
	<div class="section">
		
	</div>
</div>-->


<!--<div class="area" id="main">-->

<div id="content_wrapper">

		<div id="page_form" class="section">
				<h2>Password Recovery Assistant</h2>
				<div class="help_text">
					
					<p>
						<h3>Trouble logging in?</h3>
						Please note that we now require the email address that you used to register. Please make sure you typed your <strong>email address</strong> correctly. 
					</p>
					<p>
						Use the form to recover your password.
					</p>
				</div>
				<form action="/forgot-password.php" method="post" class="interactive_form">
					<? if( in_array(constants::TRAVELER_DOES_NOT_EXIST,$contents["arrError"]) ): ?>
						<div class="error_notice">
							<p>We did not find a user that uses that email address. Make sure you spelled your email address correctly. <br /> Not yet registered? <a href="/register.php">Register Now!</a></span>
							</p>
						</div>
					<? elseif( in_array(constants::EMPTY_EMAIL_ADDRESS,$contents["arrError"]) ): ?>
						<div class="error_notice">
							<p>Please provide us with the email address you used in creating your GoAbroad Network account. <br />
								<span>Not yet registered? <a href="/register.php">Register Now!</a></span>
							</p>
						</div>	
					<? endif; ?>
					<ul class="form">
						<li>
							<label for="txtFindString">Email Address:</label>
							<input type="text" name="txtFindString" id="txtFindString" value="<?= $contents["email"]; ?>" class="text big" />
							<p class="supplement" for="txtFindString">The email address you used to register.</p>
						</li>
						<li>
							<label for="txtFindString">Security Code:</label>
							<p class="supplement">Please type the code that you see in the image below:</p>
							<input type="text" id="securityCode" name="securityCode" value="" class="text" />
							
							<? if( in_array(constants::SECURITY_CODE_MISMATCH,$contents["arrError"]) ): ?>
								<p class="error"><strong>Security code mismatch</strong></p>
							<? endif; ?>
							
							<img  id="captchaImg" src="<?= $contents["src"]; ?>" alt="Captcha" /><br />
							<a id="refresh" href="javascript:void(0)" onclick="Captcha.load()" style="color:red; font-weight:normal">[Refresh Image]</a>
							<a href="http://www.goabroad.com/captchainfo.cfm" title="Why do you need to enter this code?" onclick="window.open('captcha-info.php?location=forgot-password&amp;network=<?= $contents['network'] ?>', 'captchainfo', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable,width=500,height=235, top=' + ((screen.width/2)- 1000) + ', left=20'); return false;">
							<img src="http://images.goabroad.com/images/icons/help.gif" border="none"> What is this?</a>
							<input type="hidden" id="encSecCode" name="encSecCode" value="<?= $contents["code"]; ?>">							
							
						</li>	
						<li class="actions">
							<input type="submit" name="btnSubmit" value="Send My Password" class="submit" />
						</li>
					</ul>					
				</form>							
			</div>
</div>







