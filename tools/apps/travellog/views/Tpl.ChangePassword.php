<?php
/*
 * Tpl.ChangePassword.php
 * Created on Aug 18, 2006
 *	Author: K. Gordo
 */
 
 /***************************************************************************
  * edits of neri:
  * 	added jquery-1.2.6.js as dependent js:					11-05-08
  * 	changed interactive.form.js to interactive.form2.js:	11-05-08
  * 	modified inline js script:								11-28-08
  * 	commented out tpl.LayoutPopup.php as main template:		12-23-08
  * 	checked if pass3 is not null in js:						01-05-09
  * 	changed static text "GoAbroad Network" according to 
  * 		value of the variable network:						01-06-09
  ***************************************************************************/

Template::includeDependentJs('/min/f=js/interactive.form2.js', array('bottom' => true));
Template::includeDependentJs('/min/f=js/custompopup-1.2.js', array('bottom' => true));
Template::includeDependentJs('/min/f=js/changepassword.js', array('bottom' => true));
Template::setMainTemplateVar('layoutID', 'main_pages');
?>
		<div id="popup_content" class="section">
				<h2 id="pop_header"><span>Change <?if( 0 == strlen(trim($ref))):?>Account Settings<?else:?>Password <?endif;?></span></h2>
				<div class="content">
					<? if (isset($success_message)): ?>
						<p class="confirmation"><?= $success_message; ?></p>
						<p>&nbsp;</p>
						<? if( strlen($ref) ) : ?>
						 	<p class="help_text">You may now access your <?= $network ?> account by going to the <a href="login.php">login</a> page.</p>
						 <? else : ?>	
							<? $link = (isset($_GET['group'])) ? "group.php?gID=".$_GET['group'] : $uname?>
							<input type="hidden" id="hidLink" name="hidLink" value="<?= $link; ?>">
						 	<p><input type="button" name="closeButton" id="closeButton" value="Close" /></p>
						 <? endif; ?>
					<? else: ?>
						<!-- edited by marc, Feb 13, 2007 -->
						<? $actionpage = (isset($_GET['group']) ? "changepassword.php?action=update&amp;group=".$_GET['group']."$ref" : "changepassword.php?action=update$ref")?>
						<? $hasref = ( 0 == strlen(trim($ref))) ? "" : true?>
						<input type="hidden" id="hidHasref" name="hidHasref" value="<?= $hasref; ?>">
						<form name="passform" id="passform" action="<?=$actionpage?>" method="post">  
							<? if (isset($error)) { ?>
								<div class='error'>
									<ul>
									<?foreach($error as $err):?>
										<li><?=$err?></li>
									<?endforeach;?>
									</ul>
								</div>
							<?}?>
							<ul class="form">
							<div <? if( 0 != strlen(trim($ref)) ): ?> style = "display:none" <?endif;?> >
								<?if(!isset($_GET['group'])):?>
									<li>
										<label for="inpt_box"> <span class="feature_helper">Username:</span> <span class="required feature_helper">*</span><a class="featureMrk_sp feature_usertxt" href="javascript:void(0)" onmouseout="CustomPopup.removeBubble();" onmouseover="CustomPopup.fetchNewBubble('Customize your profile name!',this);"><span>New</span></a></label>
										<input type="text" id="username" name="username" value="<?= $username  ?>" class="text feature_clr" maxlength="25"/>
										<input type="hidden" id="hdnusername" name="hdnusername" value="<?= $username  ?>"/>	
										<br /><br />
										<span class="supplement">Please use only letters, numbers or underscore. Username must be one word only.</span>	
									</li>									
									<!-- <li>&nbsp;</li> -->
							  		<hr style="clear: both; border: 1px solid #dfdfdf;"/>
								<?endif;?>
								<li>
									<label for="inpt_box">Email:</label>  <span id = "currentemail"><?=$email?></span>
								</li>
								<li>
									<label for="inpt_box">New Email:</label>
									<input type="text" id="newemail" name="newemail" value="" class="text" />
								</li>
								<li>
									<label for="inpt_box">Retype New Email:</label>
									<input type="text" id="newemail2" name="newemail2" value="" class="text" />
								</li>							
							</div>
							<hr style="clear: both; border: 1px solid #dfdfdf;"/>
							<? if( 0 == strlen(trim($ref)) ): ?> 
								<li>
								<label class="inpt_box">Current password:</label>
								<input type="password" name="currentPassword" id = "currentPassword" class="text" />			
								</li>
							<? endif; ?>
							<li>
								<label class="inpt_box">New password:</label>
								<input type="password" name="newPassword" id="newPassword" class="text" />
								<br />
								<span class="supplement">Passwords must be 6 characters or more.</span>								
							</li>
						
							<li>
								<label class="inpt_box">Retype new password:</label>
								<input type="password" name="newPassword2" id="newPassword2" class="text" />
							</li>	
						
							<li class="actions" id="pop_button">
								<input type="submit" id = "btnSubmit" name="submit" value="Update" class="submit" />
							</li>
							 </ul>
						</form>
					<? endif; ?>
					</div>
			</div>
			
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$('#newemail2').attr("value", "");
			$('#currentPassword').attr("value", "");
		});
	})(jQuery);
</script>
