Hi <?php echo $recipient?>,


<?php echo $poster; if( $isFirstPost ): ?> created a discussion <?php else: ?> posted a reply to the discussion <?php endif;?>'<?php echo $discussion?>' in <?php echo GaString::makePossessive($group->getName())?> Discussion Board.


To view this post, please follow this link:
http://<?php echo $siteUrl . $postLink ?>


To view <?php echo GaString::makePossessive($group->getName())?> Discussion Board, please follow this link:
http://<?php echo $siteUrl.$dboardLink?>




Many thanks,

<?php echo $emailName?>

http://<?php echo $siteUrl?>




<?php echo $notificationFooter ?>