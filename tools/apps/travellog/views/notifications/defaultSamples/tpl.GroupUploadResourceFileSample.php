Hi [#recipient_name#],


Your administrator for the group [#group_name#] has uploaded a new resource file.

To view this file, please follow the link below:

[#link_to_resource_file#]




Many thanks,


[#site_name#]
[#site_url#]