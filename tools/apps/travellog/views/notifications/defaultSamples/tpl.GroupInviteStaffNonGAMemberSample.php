Hello [#recipient_name#]!


Greetings from [#site_name#]!

The administrator of [#name_of_parent_group#] has invited you to join and be a staff of their group. 
A new [#site_name#] account was also created for you with the following information: 

Email: [#email#]
Password: [#password#]



Many thanks,


[#site_name#]
[#site_url#]