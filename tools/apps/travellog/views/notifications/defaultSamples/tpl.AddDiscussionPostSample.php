Hi [#recipient_name#],

[#doer_name#] posted a reply to discussion '[#discussion_name#]' in [#group_name#]'s Discussion Board.


To view this post, follow this link:
[#link_to_post#]

To view [#group_name#]'s Discussion Board, follow this link:
[#link_to_discussion_board#]



Many thanks,

[#site_name#]
[#site_url#]
