Hi [#recipient_name#],

[#doer_name#] requested to join your group [#group_Name#].


======================================================
To accept request from [#doer_name#], follow the link below:
[#link_to_accept_request#]

To deny request from [#doer_name#], follow the link below:
[#link_to_deny_request#]
======================================================


To manage your members, please follow the link below:
[#link_to_manage_members#]


Many thanks,


[#site_name#]
[#site_url#]