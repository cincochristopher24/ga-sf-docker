Hi [#recipient_name#],

Your administrator for the group [#group_name#] created new event "[#event_title#]". 

To view this event, please follow the link below:
[#link_to_event#]



Many thanks,

[#site_name#]
[#site_url#]