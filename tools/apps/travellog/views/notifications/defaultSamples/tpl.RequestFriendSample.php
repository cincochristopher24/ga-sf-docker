Hi [#recipient_name#],

[#doer_name#] has requested you as a friend on [#site_name#]. To
confirm or deny this request, please click on the link below:

[#link_to_request#]

Happy travels!

[#site_name#]
[#site_url#]