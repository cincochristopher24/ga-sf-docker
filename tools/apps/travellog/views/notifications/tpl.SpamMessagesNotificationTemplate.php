Hi <?=ucfirst($recipient)?>,

<?=count($spamMessages)?> message<?=count($spamMessages) > 1 ? 's' :''?> marked as spam from <?=$source->getName()?>:


<?php foreach($spamMessages as $iMessages):?>
==========================================================
Title: <?=$iMessages['message']->getTitle()?>

Recipient: <?=$iMessages['destination']->getUsername()?>

MessageID: <?=$iMessages['message']->getMessageID()?>

Message:
<?=$iMessages['message']->getText()?>

==========================================================
Message Link:
http://<?=$siteUrl . '/messages.php?reply&active=inbox&messageID=' . $iMessages['message']->getAttributeID()?>


<?php endforeach;?>


Many thanks,


<?=$siteName?>

http://<?=$siteUrl?>
