<?php 
	require_once('Class.HtmlHelpers.php');
	if( $messageSource->postedAsStaff() ): 
		$s = 'Your staff ' . $messageSource->getName();
	elseif( $messageSource->postedAsAdministrator() ):
		$s = 'Your administrator for the group ' . $messageSource->getName();
	else:
		$s = $messageSource->getName();
	endif;
	$s .= ' sent you a message on ' . $siteName . '.';
?>
Hi <?=$recipient?>,

<?=$s . chr(10)?>

<?php if($isMessageIncluded) :?>
======================================================
Subject: <?=$message->getTitle() . chr(10)?>

Message: 

<?	$msg_text = HtmlHelpers::TruncateText($message->getText(), 20);
	if( $messageSource->postedAsAdministrator() && $message->getText() != $msg_text ){
		$msg_text .= "\n Click here to view more: http://".$siteUrl."/messages.php\n";
		if( isset($recipientGroup) && $recipientGroup instanceof Group ): $msg_text .= '?gID='.$recipientGroup->getGroupID(); endif;
	}else{
		$msg_text = $message->getText() ."\n";
	}
?>
<?=$msg_text?>
=====================================================

<?php endif;?>
To view and/or reply to this message, please follow this link:
http://<?=$siteUrl?>/messages.php<?php if( isset($recipientGroup) && $recipientGroup instanceof Group ): echo '?gID='.$recipientGroup->getGroupID(); endif;?>



Many thanks,


<?php echo $emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>