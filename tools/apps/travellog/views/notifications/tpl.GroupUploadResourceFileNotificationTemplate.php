Hi <?php echo $recipient?>,


Your group administrator for <?php echo $group->getName()?> has uploaded <?=(count($resources) > 1 ? "new resource files":"a new resource file")?>.

To view <?=(count($resources) > 1 ? "these files, please follow these links" : "this file, please follow this link")?>:


<?foreach($resources as $resourcefile):?>

http://<?=$siteUrl?>/<?=$resourcefile->getResourcefileLink()?>

<?endforeach;?>



Many thanks,


<?=$siteName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>