Dear <?=$addressBookEntry->getName()?>,


Thank you for confirming your consent. You have agreed to receive updates about <?=GaString::makePossessive($username)?> activities on <?=$siteName?>.
<?=(is_null($gender) ? 'He/She' : ucfirst($gender) )?> will be able to send you messages. You will also find out if <?=$username?>:

	* wrote new travel journals
	* edited <?=$gender2?> travel journals
	* and uploaded new photos.

To view <?=GaString::makePossessive($username)?> profile, please click the following link:

http://<?=$siteUrl . $profileLink?>


If you're not yet registered on the <?=$siteName?>, you may sign up by clicking on the following link:

http://<?=$siteUrl?>/register.php

Have a great day!


Many thanks,

The <?=$siteName?> Team
http://<?=$siteUrl?>