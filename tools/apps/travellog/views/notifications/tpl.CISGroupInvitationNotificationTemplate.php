Hi<?=('' != trim($recipient) ? " $recipient" : "")?>,

This is an invitation to join the group <?=$group->getName()?>. GoAbroad is a great resource for CIS students!

By joining, you will be able to:

- View the exciting travel journals of participants on CIS study abroad programs

- View photos of traveling students and post photos of your own adventures abroad

- Blog about your experiences and share your adventures with all of us back at home

- Connect with students who have been on or are going on your same program

- Link your travel blog and photos to your Facebook account!

CIS will be sending e-messages to you about your study abroad program through GoAbroad.com, including info about visas, flights, and excursions.

To accept this invitation, please follow this link:
http://<?=$siteUrl?>/membership.php?gID=<?=$group->getGroupID()?>&mode=accept

We hope that GoAbroad will help you to get more out of your CIS experience, by providing you with resources, tools and connections that will link you with your *new* community abroad, while helping you stay connected to your community at home.


Many thanks,
The GoAbroad Network Team and the CIS Program Team
http://<?=$siteUrl?>




<?php echo $notificationFooter ?>