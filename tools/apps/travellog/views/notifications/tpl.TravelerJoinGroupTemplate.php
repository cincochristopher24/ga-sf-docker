<?foreach($travelers as $traveler):
	$arr[] = $traveler->getUsername();
  endforeach;?>

Hi <?=$recipient?>,


<?=((count($arr)<=2) ? implode(' and ', $arr) : implode(', ',array_slice($arr,0,count($arr)-1)) . ' and ' . $arr[count($arr)-1]) . (count($travelers) > 1 ? ' have':' has') ?> joined your group <?= $group->getName()?>.


<?foreach($travelers as $traveler):?>
To view <?=$traveler->getUsername()?>'s profile, please follow the link below:
http://<?=$siteUrl?>/<?=$traveler->getUsername()?>



<?endforeach;?>

To manage your members, follow the link below:

http://<?=$siteUrl?>/members.php?gID=<?=$group->getGroupID()?>



Many thanks,

<?=$emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>