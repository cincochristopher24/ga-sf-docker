<?
	if($source->postedAsStaff())
		$extra = 'Your staff, ' . $source->getName() . ' posted';
	elseif($source->postedAsAdministrator())
		$extra = 'Your administrator has posted';
	else
		$extra = ucfirst($source->getName()) . ' posted';
	$extra .= (count($alerts) > 1 ? ' new alerts' : ' a new alert') . ' for the ' . (($group instanceof AdminGroup) ? 'group ': 'club ') . $group->getName();
?>
<? /* HTML Mode *?>
Hi <?= ucfirst($recipient) ?>, <BR><BR>
<?= $extra ?>. The latest message was posted on <?= $time ?>.<BR>
To read the post, please follow the link below: <BR><BR>
<a href = "http://<?= $siteUrl . '/group.php?gID=' . $group->getGroupID()?>"> <?= $group->getName() ?></a>
<BR><BR>
Many thanks, <BR><BR>
<strong>The <?=$siteName ?> Team </strong>
<br /> <a href = 'http://<?=$siteUrl?>'> http://<?=$siteUrl?> </a>
<? /**/ ?>
<? /*#TEXT MODE#*/?>
Hi <?=ucfirst($recipient)?>,


<?=$extra?>.
<?$links = 0?>

<?foreach($alerts as $iAlerts):?>
<?if($iAlerts['isMessageIncluded']):?>

======================================================
Expiration Date: <?=date('F d, Y', strtotime($iAlerts['alert']->getExpiry()))?>

Message:
<?=$iAlerts['alert']->getText()?>

======================================================


<?else:?>
<?$links++?>
<?endif;?>
<?endforeach;?>
<?if(count($links) > 0):?>
<?= (count($links) == count($alerts) ? (count($links) > 1 ? 'To view these' : 'To view this' ) : chr(10) . 'To view other')?> <?=count($links) > 1 ? 'alerts':'alert'?>, please follow the link below:
<?else:?>
To view <?=(count($alerts) > 1 ? 'these alerts':'this alert')?>, please follow the link below: 
<?endif;?>
<?/*foreach($alerts as $iAlerts):?>

======================================================
Expiration Date: <?=date('F d, Y', strtotime($iAlerts['alert']->getExpiry()))?>

Message:
<?=$iAlerts['alert']->getText()?>

======================================================

<?endforeach;*/?>

http://<?=$siteUrl . '/group.php?gID=' . $group->getGroupID()?>



Many thanks,

<?=$siteName?>

http://<?=$siteUrl?>
<?/*END OF TEXT MODE*/?>