Dear <?=$addressBookEntry->getName()?>,


You have chosen not to receive updates about <?=GaString::makePossessive($username)?> activities on <?=$siteName?>.
You can still view <?=$gender2 ?> travel journals and photos by viewing <?=$gender2?> profile at:

http://<?=$siteUrl . $profileLink?>


If you're not yet registered on the <?=$siteName?>, you may sign up by clicking on the following link:

http://<?=$siteUrl?>/register.php

Have a great day!


Many thanks,

The <?=$siteName?> Team
http://<?=$siteUrl?>