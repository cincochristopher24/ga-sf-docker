<?	
	require_once("travellog/factory/Class.FileFactory.php");
	$factory = Filefactory::getInstance();
	$factory->registerClass("Crypt", array("path" => ""));
	$crypt = $factory->getClass("Crypt");
?>
Hello <?=$traveler->getTravelerProfile()->getEmail()?>!


The administrator of <?=$parent->getName()?> has invited you to join their group and be a staff of on it.


For this, a new <?php echo $siteName?> account has been created for you with the following login details:

Email: <?=$traveler->getTravelerProfile()->getEmail()?>


Password: <?=$traveler->getPassword()?>




Many thanks,


<?=$emailName?>

http://<?=$siteUrl?>




- - - - -

You received this email because you have an invitation from <?php echo $parent->getName()?>. We haven't added your email address to any lists, nor will we share it with anyone at any time.

This e-mail was sent from an unattended mailbox. Please do not reply. Send any questions to http://<?php echo $siteUrl?>/feedback.php