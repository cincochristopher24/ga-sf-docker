Hi <?=ucwords($profile->getFirstName())?>,
<?php if(isset($extra)): echo "\n$extra\n"; endif; ?>

Congratulations on your acceptance to join <?=$senderName?>.  We are excited to have you join us onsite and become a part of our growing network.

<?=$siteUrl?>  is one of your primary tools before, during and after your program.  Your next steps are:

1. Logon to your personalized ProWorld webpage

		* Logon = <?=$traveler->getTravelerProfile()->getEmail()?>

		* Password = <?=$traveler->getPassword()?>



2. Learn more and have fun

		* Create your profile for other <?=$senderName?> volunteers to see

		* Connect with other <?=$senderName?> volunteers

		* Post your stories and photos

		* Learn about your project and housing placement



3. Complete these required forms at your .NET page

		* Health Form - Complete at least 6 weeks before your start date

		* Travel Form - Complete at least 6 weeks before your start date


Once again, congratulations and welcome to <?=$senderName?>.

Sincerely,
The <?=$senderName?> Team
______________________
<?=$siteName?>

877.42WORLD
<?=$siteHomeUrl?>


real projects...
...real experience