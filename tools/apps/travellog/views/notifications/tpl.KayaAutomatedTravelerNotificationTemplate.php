Dear <?=ucwords($profile->getFirstName())?>,

Welcome to Kaya!  We look forward to having you on our programs and becoming part of our growing Kaya Volunteer network. We would like to invite you to join our fantastic online Kaya Community, which will become one of your primary tools before, during and after your program.

We created this network to provide you the pre-departure information that you will need to prepare for your trip, and to allow you to share your volunteering experience with other volunteers and the rest of the world!  You can use your profile to keep your family and friends updated by writing journal entries, which can even be linked to your Facebook or MySpace account!

You can also use this network to share your pictures and videos and connect with past, present and future volunteers. And as part of the Kaya community, you can read about other volunteers experiences from their time at your location.

To get started log on to your personalized Kaya Volunteer webpage by going to:
 
http://kaya.goabroad.net/login.php
 
                * Login = <?=$traveler->getTravelerProfile()->getEmail()?>

                * Password = <?=$traveler->getPassword()?>

 
Once you've logged on you will find all the information you need to prepare for your trip including your Welcome Pack and Online Forms.

Your welcome pack and online forms are located in the group section of your profile.  Once you've logged on, your personal homepage has a series of steps for creating your profile.  To find your welcome pack and online forms simply click on step 4 "Join and Create Groups", on the next page you will see that you have already been assigned to the group <?= $notifGrpName?>, just click on this group and on the right-hand side of the page you will find your welcome pack and the links to our online Travel, Skills, and Health forms, which will provide us with the information we need to prepare for your arrival.

We suggest that you read your welcome pack thoroughly as it includes all the important information you will need to prepare for your trip.  Please pay particular attention to the information on visas, vaccination, insurance and arrival information, as these elements will be your responsibility to plan.  Make sure you print out this pack and keep it with you on your journey as it will serve as your reference for anything you need to know about your destination, including any emergency contact details. We suggest that you also download a copy of the pack to leave with your family at home whilst you are away, to reassure them of your plans and enable them to contact you in an emergency.

Also, your health and skills information are very important for us to make the necessary arrangements for your volunteer project, therefore, we would ask you that you please fill out your Health and Skills forms within the next 2 weeks.  If you won't be organising your health/travel insurance until a later date it is fine for you to send your Health form to us without this information and forward your insurance details on to us at a later date.  We understand that you may not be booking your flight right away so you can send us your flight information once that is booked, but at least 2 weeks prior to your arrival. Please note that last-minute flights are usually a lot more expensive than flights bought in advance and we advise that you secure your flights in good time before the costs escalate.

Once all the form filling in is completed you can start with the exciting task of creating your profile.  You can upload pictures and videos, create discussion groups, complete your travel biography, contact past present and future volunteers, show others what countries you've travelled to on your personal travel map, and share your travel plans to find out who else could be travelling your way.

You may want to write a journal to keep your family and friends updated about your experience.  You don't need to limit it to your project work, if you find a great restaurant or a discover a secret attraction that you think others should know about post it on your profile so that future volunteers can benefit!

We hope you make full use of your Kaya Community account, as by sharing your experiences, others may also be encouraged to volunteer and make a difference. While we can provide you all the information about projects and locations, your experiences on the ground can really bring a placement to life and provide inspiration for thousands of others to travel responsibly.

Thank you for choosing to volunteer with Kaya, we hope you have a fantastic trip!
 
The Kaya Volunteer Team
Kaya Responsible Travel
info@kayavolunteer.com
www.kayavolunteer.com