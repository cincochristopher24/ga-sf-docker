Hi <?php echo $friend->getTravelerProfile()->getFirstName()?>,

<?php echo $doer->getUsername()?> wants to add you as a friend on <?php echo $siteName?>. To confirm or deny this request, please click on the link below:

http://<?php echo $siteUrl?>/login.php?redirect=friend.php

*We protect your privacy and <?php echo $doer->getUsername()?> will not be notified if you choose to deny this request.



Many thanks!

<?php echo $emailName ?>

http://<?php echo $siteUrl?>




<?php echo $notificationFooter ?>