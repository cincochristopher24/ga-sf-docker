Hi <?=ucwords($profile->getFirstName())?>,

We are excited about your interest in CISabroad programs!  We look forward to working with you, and we hope that you will become part of our growing organization.

Please take a minute to join our online network, MyCISabroad. We use this site as the primary communication tool before, during, and after your program.  We will assign you to a group site which will give you access to loads of information about your particular program.

Through MyCISabroad you will be able to:

	- Create a profile for other CISabroad program participants to see
	- View and post pictures and videos
	- Access information about your program
	- Learn about travel opportunities and flight recommendations
	- Post journal entries
	- Access required program forms
	- Find out about cell phone options
	- Check out current visa information
	- Meet other program participants and more!


Your Next Steps are:

Follow these quick steps to get to your personalized page:
 
1. Log on to your personalized CISabroad webpage by going to http://MyCISabroad.net and clicking on the My Passport tab at the top of the page.
 
                * Login = <?=$traveler->getTravelerProfile()->getEmail()?>

                * Password = <?=$traveler->getPassword()?>




To change your password, click on Change Account Settings in the upper right of the Manage Your Profile box.			
					
2. Learn more and have fun. 
 
Explore the site and see what it has to offer!  You can connect your MyCISabroad account to your Facebook and Twitter accounts by clicking on the Apps tab in your Dashboard. We know most of your friends are on these social networks, and we want you to be able to share your travel experiences with them. By adding our applications, you can keep blogging on MyCISabroad and be confident that your stories and photos are instantly shared with your friends on these sites.

 3.  Complete the following required forms on MyCISabroad.net.

	- Airport Pick-Up and Flight Information 
	- Overseas Study Program Health Information
	- Study Abroad Advisor Emergency Contact Information
	- Transcript Mailing Information for After Your Program 


Once again, congratulations and welcome to CISabroad!



The CISabroad Team
-------------------------------------
CIS Abroad  
http://www.cisabroad.com
17 New South Street, #205
Northampton, MA 01060
Phone: 1.877.617.9090 or 1.413.582.0407
Fax: 1.413.582.0327