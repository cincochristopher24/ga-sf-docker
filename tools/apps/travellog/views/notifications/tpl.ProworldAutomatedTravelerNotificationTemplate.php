Hi <?=ucwords($profile->getFirstName())?>,

Congratulations on your acceptance to join ProWorld.  We are excited to have you join us onsite and become a part of our growing network.

I invite you to sign in and explore our online networking site called www.MyProWorld.NET.  This site will be one of your primary tools before, during, and after your program.  Through MyProWorld.NET, ProWorld will assign you to different groups which will pertain to your specific site where you will be able to access more information about the: 

*	Country you will be volunteering in
*	Project you will be working on
*	Living arrangements

Projects and homestay families are assigned after a pre-program phone call which will help us to determine the best placement for you, typically 4-6 weeks before your program begins, so please be patient if you have not been assigned one just yet. Also, as a reminder, homestay families do not always have easy access to the internet, and so we don't require them to log on to .NET.
Therefore, if you are interested in communicating directly with your homestay family before your arrival, please contact a ProWorld staff member at your site who can deliver any messages or help you to contact them.

Once you are onsite, or even before, you can use www.MyProWorld.NET to:

*	Network directly with other volunteers who will be joining your same program, as well as staff based at your site 
*	Post journal entries
*	Load pictures or videos
*	Communicate with friends and family back home to join in on the adventure with you!

At the bottom of this email there is a link to download a quick tutorial guide which will help guide you through .NET and allow for an easy and fun navigating experience. As always, please feel free to contact ProWorld staff with any questions.  

Your next steps are:

1. Logon to your personalized ProWorld webpage

       * Logon = <?=$traveler->getTravelerProfile()->getEmail()?>

       * Password = <?=$traveler->getPassword()?>


2. Learn more and have fun

       * Create your profile for other ProWorld volunteers to see
       * Connect with other ProWorld volunteers
       * Post your stories, journal entries, photos, and videos
       * Learn about your project and housing placement

3. Complete these required forms on your .NET page
       * Accepted Participant Form - Complete within 10 days of acceptance so we can start preparing your program.
       * Health Form - Complete at least 6 weeks before your start date
       * Travel Form - Complete at least 6 weeks before your start date


<?php /*For a quick tutorial on using www.MyProWorld.NET, see our How-To Guide (http://myproworld.goabroad.net/MyProWorld.NET-How-To-Guide.php).*/?>
For a quick tutorial on using www.MyProWorld.NET, see our How-To Guide (http://myproworld.goabroad.net/users/resourcefiles/2007/May/1601/admingroup/14aef8dc61057e1257213382.pdf)

Once again, congratulations and welcome to ProWorld.

Sincerely,
The ProWorld Team
______________________
ProWorld Service Corps
877.42WORLD
www.proworldvolunteers.org

real projects...
...real experience
