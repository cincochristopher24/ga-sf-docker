Hi <?=ucwords($profile->getFirstName())?>,

Congratulations on your acceptance to participate on a <?= $notifGrpName?> program!  We look forward to working with you in Barcelona and having you become part of our growing Barcelona SAE group of students and alumni.

Please take a minute to join our online network, which we use as the primary communication tool before, during and after your program.  Follow these quick steps to get your personalized page:
 
1. Logon to your personalized Barcelona SAE webpage by going to http://barcelonasae.goabroad.net
 
                * Logon = <?=$traveler->getTravelerProfile()->getEmail()?>

                * Password = <?=$traveler->getPassword()?>

 
2. Learn more and have fun. With your BarcelonaSAE.goabroad.net account you'll be able to:
 
                * Create your profile for other <?= $notifGrpName?> participants to see
                * Connect with other <?= $notifGrpName?> participants even before you arrive
                * Learn about your upcoming program
                * Once the program begins, post your stories and photos, get program updates, follow the other participants' adventures and much more.

 
Once again, congratulations and welcome to Barcelona SAE!
 
Margita and The Barcelona SAE Team
Margita Hupkova, International Education Representative
Margarita@barcelonasae.com
www.barcelonasae.com