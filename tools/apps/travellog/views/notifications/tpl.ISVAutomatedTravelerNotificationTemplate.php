Hi <?=ucwords($profile->getFirstName())?>,

ISV invites you to join our brand new online community so you can connect with other ISV travellers and share your experience with the world.

We have provided you with your own account and added you as a member to <?= $notifGrpName?> so you can meet other students who have travelled with ISV and future ISV travellers. Our goal is to build a huge online community of our 20,000+ alumni so we can all stay in touch.

The ISV community page is amazing, by joining this page you will be able to start your own online blog/journal so your friends and family can keep track of what you are doing.  These journals have a stack of features and will allow you to:

- Upload photos and videos

- Share your experience in a much more in-depth way that short Facebook postings

- Find out what's new through news and [your_group_name] calendar events that will show up on your page. 

- Share your experiences with family and friends who may not be your friends on Facebook (because you may not always want grandma to see what you did on the weekend!)

- Stay connected with all ISV alumni

- Receive email broadcasts from ISV

- Write about new travel experiences you are going to embark on

- Receive updates from your project organisations 

- Share with your community all your thoughts and feelings before, during and after your trip

- See other friends’ photos and videos

- Join discussions with members through their Discussion Boards

- Help promote volunteering to the world at large

- Participate in surveys and evaluations conducted by ISV

This is a brand new feature ISV has created to support you better as a participant and past ISV traveller. Please login now and create your own profile and start posting. To join <?= $notifGrpName?>, please follow the link below:

http://isvolunteers.goabroad.net/

And please use the following login details:

<?=$traveler->getTravelerProfile()->getEmail()?>

<?=$traveler->getPassword()?>

***Please change this temporary password to something you will remember better. Follow this link to change your password: http://isvolunteers.goabroad.net/account-settings.php

Many thanks,
The ISV Team

http://isvolunteers.goabroad.net

This e-mail was sent from an unattended mailbox. Please do not reply. Send any questions to http://isvolunteers.goabroad.net/feedback.php