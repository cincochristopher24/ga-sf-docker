<?php // TEMPLATE FOR DELETING JOURNAL ENTRY/ARTICLE INSTANT NOTIFICATION TEMPLATE ?>
Hi <?=$recipientName?>,


We apologize but we have had to remove your post, "<?=$title?>". We remove test posts that have been idle for 48 hours.


***If the deleted post is a journal you created for your company or product, please be informed that a common reason for removal of content is the violation of the Commercial Use provision on our Terms of Use. Please review the Terms by following this link: http://<?=$termsLink?>. You may re-post your content provided they conform to the Terms of Use. Please note that continued violation of the Commercial Use provision, or any of the other terms, may result in suspension or termination of your account. 
 
 
Regards,

The GoAbroad Network Team



- - - - -

This e-mail was sent from an unattended mailbox. Please do not reply. Send any questions to http://<?php echo $siteUrl?>/feedback.php