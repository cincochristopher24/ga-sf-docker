<?
	/**
	* tpl.FrmAllPreferences.php
	* @author marc
	* aug 2006
	*/
?>

<ul class="form" id="preferences_list">
<?php 

	foreach( $contents["preferences"] as $program ){
		$nPrefStud = new Template();
		$isChecked = '';
		/**
		* get the student preference
		*/
		$arrStudProgType = array();
		
		if ( count($contents["studentPreference"]->getPreference()) )
			foreach ( $contents["studentPreference"]->getPreference() as $studProgram ) {
				if ( $program->getProgramID() == $studProgram->getprogramID() ) {
					$isChecked = 'checked="checked"';
					if ( $program->getProgramID() != 2 ){
						$nStudProgType = $studProgram->getProgramType();
						if ( count($nStudProgType) )
							foreach ( $nStudProgType as $progType )
								$arrStudProgType[] = $progType->getProgramTypeID();
					}
					break;		
				}
				
			}
		
		/**
		* get all the Program Types and store in array
		*/
		$arrProgType = $program->getProgramType();
		$arrProgTypeChoices = array();
		
		if ( count($arrProgType) ){
			foreach ( $arrProgType as $nProgType ){
				$indProgType = array();
				$indProgType["key"] = $nProgType->getProgramTypeID();
				$indProgType["value"] = $nProgType->getProgramType();
				$indProgType["ID"] = 'type_' . $program->getprogramID() . '_' . $nProgType->getProgramTypeID();
				$arrProgTypeChoices[] = $indProgType;
			}
		}
		$arrProgCol = array();
		$arrProgAllCol = array();
		/**
		* divide all countries into 4 rows
		* each row must be contained in an array
		* all row arrays must be pushed in an array
		*/
		$maxRowElem = ceil(count($arrProgType) / 4);
		$rowCtr = 0;
		foreach ( $arrProgTypeChoices as $indProgtype){
			if ($rowCtr < $maxRowElem)
				$rowCtr++;
			else{
				$rowCtr = 1;
				$arrProgAllCol[] =  $arrProgCol;
				$arrProgCol = array();
			}
			
			$arrProgCol[] = $indProgtype;
		}
		$arrProgAllCol[] = $arrProgCol;
		
		$nPrefStud->set_vars( array(
			'programName'=>$program->getProgram(),
			'programID'=>$program->getProgramID(),
			'isSelected'=>$isChecked,
			'programTypeOptions'=>$arrProgAllCol,
			'arrStudProgType'=>$arrStudProgType
		));
		/**
		* display student preference form 
		*/
		$nPrefStud->out('travellog/views/tpl.FrmProgram.php');
	}
?>
</ul>