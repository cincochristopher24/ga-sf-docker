<?
	/**
	* tpl.FrmProgram.php
	* @author marc
	* aug 2006
	*/
?>

<li>
	<fieldset class="choices">
		<legend>
			<input type="checkbox" name="chkProgram_<?=$programID ?>" id="chkProgram_<?=$programID?>" value="<?=$programID?>" <?=$isSelected?> />
			<label for="chkProgram_<?=$programID?>"> <?=$programName ?></label>
		</legend>
		
		<?php if ($programTypeOptions): ?>
			<?php foreach ( $programTypeOptions as $programTypeOptionCol ): ?>		
				<ul class="column">
					<?php $arrProgColDisp = FormHelpers::CreateChkbox_ASSOC($programTypeOptionCol,'chkType_' . $programID . "[]",$arrStudProgType); 
						foreach ( $arrProgColDisp as $programTypeOption ):
							echo '<li>'.$programTypeOption .'</li>';
						endforeach;
					?>	 
				</ul>
			<?php endforeach; ?>
		<?php endif; ?>
		<div class="clear"></div>	
	</fieldset>
</li>
