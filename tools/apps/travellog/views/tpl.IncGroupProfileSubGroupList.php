<?php if( count($featuredGroups) || $isAdminLogged ): ?>
	<div class="section" id="sub_groups">
		<h2>
			<span>Subgroups</span>
			<span class="header_actions">
		    <?php if($isAdminLogged): ?>
		    	<a href="/group.php?mode=add&amp;pID=<?=$grpID?>">Add Subroup</a> |
		    <?php endif; ?>
			<?php if(isset($GLOBALS["CONFIG"]) ): ?>
				<?php if($isAdminLogged): ?>
					<a href="/group-pages.php" ><?= $link_label ?></a>
				<?php else: ?>
					<a href="/group-pages.php?search" ><?= $link_label ?></a>
				<?php endif; ?>
			<?php else: ?>
				<a href="/group-pages.php?gID=<?=$grpID?>" ><?= $link_label ?></a>
			<?php endif; ?>
			</span>
		</h2>
		<div class="content">
			<?php if(count($featuredGroups)): ?>
				<ul class="groups">
					<?php foreach($featuredGroups AS $group):
						$subgroup_name = str_replace('-', '_', $group->getName());
						$subgroup_name = str_replace(' ', '-', $subgroup_name);
						$group_name = str_replace('-', '_', $parent->getName());
						$group_name = str_replace(' ', '-', $group_name);
				
						$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;
			
						// photo tag
						$cntPhotos = (0 < $group->getGroupPhotoID()) ? 1 : 0;
						$cntPhotos = $cntPhotos + $group->getAlbumPhotosCount();
			 
						$photo_tag_cnt = (1 < $cntPhotos)  ? $cntPhotos.' Photos' : $cntPhotos.' Photo';
						$photo_tag_cnt = (0 == $cntPhotos) ? "" : $photo_tag_cnt;
						// end of photo tag
			
						// journal tag
						// $cntJournals = $group->getJournalsCount();
						$cntJournals = $group->getJournalEntriesCount();
			
						// article tag
						$cntArticles = $group->getArticlesCount();
		  
						if($cntJournals > 0 && $cntArticles > 0){
							$journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journal Entries' : $cntJournals.' Journal Entry';
							$journal_tag_cnt .= (1 < $cntArticles) ? ' | '.$cntArticles.' Articles' : ' | '.$cntArticles.' Article';
						}else if($cntJournals <= 0 && $cntArticles > 0)
							$journal_tag_cnt = (1 < $cntArticles) ? $cntArticles.' Articles' : $cntArticles.' Article';
						else if($cntJournals > 0 && $cntArticles <= 0)
							$journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journal Entries' : $cntJournals.' Journal Entry';
						else
							$journal_tag_cnt = "";
						// end of journal tag

				    	// members tag
				    	$cntMembers = $group->getMembersCount();
						$members_tag_cnt = (1 < $cntMembers) ? $cntMembers.' Members' : $cntMembers.' Member';
						$members_tag_cnt = (0 == $cntMembers) ? '' : $members_tag_cnt;
						// end of members tag
			
						$group_name = (20 < strlen($group->getName()) && !strpos($group->getName()," ")) ? substr($group->getName(),0,20) . "..." : $group->getName();
						?>			

						<li>
							<a title="View Group Profile" class="thumb" href="<?=$friendlyURL?>"><img class="pic" width="65px" height="65px" alt="Group Emblem" src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>"  /></a>
					
							<div class="details">
								<a class="groupname" href="<?=$friendlyURL?>" title="<?=$group->getName()?>"><?=$group_name?></a>

								<p>
									<?= $photo_tag_cnt ?>   <? if ($cntPhotos) { ?> <br /> <? } ?>
									<?= $members_tag_cnt ?> <? if ($cntMembers) { ?> <br /> <? } ?>
									<?= $journal_tag_cnt ?> <? if ($cntJournals || $cntArticles) { ?> <br /> <? } ?>
						
									<? if (0 == $cntMembers): ?>
										<? if (NULL != $loggedUser && FALSE == $loggedUser->isAdvisor()):
												$validGrpAccess = array(GroupAccess::$OPEN, GroupAccess::$OPEN_APPROVAL);
												if (!$isAdminLogged  && in_array($group->getGroupAccess(), $validGrpAccess) && in_array ($parent->getGroupAccess(), $validGrpAccess ) ):
													if(!$group->isInRequestList($loggedUser)):
														$message = 'You are about to join the '.strtolower($group->getLabel()).' '.$group->getName().'. Your request is subject to the administrator&lsquo;s approval and will be sent after clicking Join. You will be able to interact with this group once your request has been approved.';
														$refLink = '&amp;ref=groupsubgroup';
											
														if($group->getGroupAccess() == GroupAccess::$OPEN):
															$message = 'You are about to join ' . $group->getName().'. You will automatically be a member of the group after clicking Join.';
														endif;
											
														$strlinkjoin = ' Do you want to <a href="javascript:void(0)" onclick="CustomPopup.initialize(\'Join '.$group->getName().' ?\',\'Are you sure you want to join the '.strtolower($group->getLabel()).' '.$group->getName().'?\',\'/membership.php?mode=join&amp;gID='. $group->getGroupID().$refLink.'&confirm\',\'Join\',\'0\',\''.$message.'\');CustomPopup.createPopup();">join</a> this group?';
													endif;				
												endif;
											endif;
					
											echo  'Group has no members yet.' ; echo (isset($strlinkjoin)) ? $strlinkjoin : '';				
										endif; ?>						
								</p>				
							</div>
						</li>		
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>	
	</div>
<?php endif; ?>
<?/*
<? if (0 < $featuredCount && $isAdminLogged): ?>
	<div class="section" id="sub_groups">
		<h2>
		  <span>Subgroups</span>
		  <span class="header_actions">
		    <? if($isAdminLogged): ?>
		      <a href="/group.php?mode=add&amp;pID=<?=$grpID?>">Add Subroup</a> |
		    <? endif; ?>
		    <a href="/subgroups.php?gID=<?=$grpID?>" ><?= $link_label ?></a>
		  </span>
		</h2>
	</div>
<? endif; ?>

<? if (0 == $featuredCount): ?>
	<? $it = $parent->getMySubGroups(null, null, " datecreated DESC", new RowsLimit(5, 0)) ?>
		
	<div class="section" id="sub_groups">
		<h2>
		  <span>Subgroups</span>
		  <span class="header_actions">
		    <? if($isAdminLogged): ?>
		      <a href="/group.php?mode=add&amp;pID=<?=$grpID?>">Add Subgroup</a> |
		    <? endif; ?>
		    <a href="/subgroups.php?gID=<?=$grpID?>" ><?= $link_label ?></a>
		  </span>
		</h2>
			
		<? if (0 < $it->size()): ?>
			<div class="content">
				<? if ($isAdminLogged): ?>
				<p class="side_help_text">There are no featured subgroups. Five most recent subgroups are shown.</p>
				<!-- <div style="text-align: center;" class="help_text">
					<span>
						There are no featured groups. Five most recent groups are shown.
					</span>
				</div> -->
				<? endif; ?>
				
				<ul class="groups">
					<? while($it->hasNext()):
						$group = $it->next();
						
						$subgroup_name = str_replace('-', '_', $group->getName());
						$subgroup_name = str_replace(' ', '-', $subgroup_name);
						$group_name = str_replace('-', '_', $parent->getName());
						$group_name = str_replace(' ', '-', $group_name);
							
						$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;
						
						// photo tag
						$cntPhotos = (0 < $group->getGroupPhotoID()) ? 1 : 0;
						$cntPhotos = $cntPhotos + $group->getAlbumPhotosCount();
						 
						$photo_tag_cnt = (1 < $cntPhotos)  ? $cntPhotos.' Photos' : $cntPhotos.' Photo';
						$photo_tag_cnt = (0 == $cntPhotos) ? "" : $photo_tag_cnt;
						// end of photo tag
						
						// journal tag
						// $cntJournals = $group->getJournalsCount();
						$cntJournals = $group->getJournalEntriesCount();
						
						// article tag
						$cntArticles = $group->getArticlesCount();
					  
						if($cntJournals > 0 && $cntArticles > 0){
							$journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journal Entries' : $cntJournals.' Journal Entry';
							$journal_tag_cnt .= (1 < $cntArticles) ? ' | '.$cntArticles.' Articles' : ' | '.$cntArticles.' Article';
						}else if($cntJournals <= 0 && $cntArticles > 0)
							$journal_tag_cnt = (1 < $cntArticles) ? $cntArticles.' Articles' : $cntArticles.' Article';
						else if($cntJournals > 0 && $cntArticles <= 0)
							$journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journal Entries' : $cntJournals.' Journal Entry';
						else
							$journal_tag_cnt = "";
						// end of journal tag

				    // members tag
				    $cntMembers = $group->getMembersCount();
						$members_tag_cnt = (1 < $cntMembers) ? $cntMembers.' Members' : $cntMembers.' Member';
						$members_tag_cnt = (0 == $cntMembers) ? '' : $members_tag_cnt;
						// end of members tag
						
						$group_name = (20 < strlen($group->getName()) && !strpos($group->getName()," ")) ? substr($group->getName(),0,20) . "..." : $group->getName();
					?>			
	
						<li>
							<a title="View Group Profile" class="thumb" href="<?=$friendlyURL?>"><img class="pic" width="65px" height="65px" alt="Group Emblem" src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>"  /></a>
								
							<div class="details">
								<a class="groupname" href="<?=$friendlyURL?>" title="<?=$group->getName()?>"><?=$group_name?></a>
		
								<p>
									<?= $photo_tag_cnt ?>   <? if ($cntPhotos) { ?> <br /> <? } ?>
									<?= $members_tag_cnt ?> <? if ($cntMembers) { ?> <br /> <? } ?>
									<?= $journal_tag_cnt ?> <? if ($cntJournals || $cntArticles) { ?> <br /> <? } ?>
									
									<? if (0 == $cntMembers): ?>
										<? if (NULL != $loggedUser && FALSE == $loggedUser->isAdvisor()):
												$validGrpAccess = array(GroupAccess::$OPEN, GroupAccess::$OPEN_APPROVAL);
												if (!$isAdminLogged  && in_array($group->getGroupAccess(), $validGrpAccess) && in_array ($parent->getGroupAccess(), $validGrpAccess ) ):
													if(!$group->isInRequestList($loggedUser)):
														$message = 'You are about to join the '.strtolower($group->getLabel()).' '.$group->getName().'. Your request is subject to the administrator&lsquo;s approval and will be sent after clicking Join. You will be able to interact with this group once your request has been approved.';
														$refLink = '&amp;ref=groupsubgroup';
														
														if($group->getGroupAccess() == GroupAccess::$OPEN):
															$message = 'You are about to join ' . $group->getName().'. You will automatically be a member of the group after clicking Join.';
														endif;
														
														$strlinkjoin = ' Do you want to <a href="javascript:void(0)" onclick="CustomPopup.initialize(\'Join '.$group->getName().' ?\',\'Are you sure you want to join the '.strtolower($group->getLabel()).' '.$group->getName().'?\',\'/membership.php?mode=join&amp;gID='. $group->getGroupID().$refLink.'&confirm\',\'Join\',\'0\',\''.$message.'\');CustomPopup.createPopup();">join</a> this group?';
													endif;				
												endif;
											endif;
								
											echo  'Subgroup has no members yet.' ; echo (isset($strlinkjoin)) ? $strlinkjoin : '';				
										endif; ?>						
								</p>				
							</div>
						</li>		
					<? endwhile; ?>
				</ul>
			</div>
		<? else: ?>
			<div class="content">
				<p class="side_help_text">	No subgroups yet.</p>
			</div>	
		<? endif; ?>
	</div>
<? endif; ?>

<? if($isAdminLogged OR 0 < $featuredCount): ?>
	<?= $categories ?>
<? endif; */?>