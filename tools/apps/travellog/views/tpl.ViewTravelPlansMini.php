<?php  $numItems = count($travelItems); ?>
<div id="ganet_app_container" class="_">

<? if ($numItems > 0) : ?>
	<ul id="normal_travels" class="travel_list">
		<?php $travels = $travelItems; require 'tpl.IncTravelPlanMini.php'; ?>
	</ul>
	
	<?php if ($owner) : ?>
	    <p>
	      <?php if ($totalNumItems > $numItems):?> | <a href="widget.php?travelerID=<?php echo $travelerID; ?>&action=editMTP&do=viewAll">View all</a><?php endif; ?>
	    </p>
	 <?php endif; ?>	
			
	<?php if ($owner || count($countriesInterested['countryIDs'])) : ?>
	<div id="travel_wish">
		<p id="travelwish_title"> 
			My Travel Wishlist (<?php echo count($countriesInterested['countryIDs']) ?>)
		</p>
		<p id="travelwish_countries">								
			<?php echo join(', ', $countriesInterested['countryNames']) ?>
		</p>
		
		<?php if ($owner) : ?>

		<div id="show_destinations" class="widget_actions">
			<a href="javascript:void(0)" onclick="LIB.toggleVisibility('destination_list', 'show_destinations')">Edit Wish List</a>
		</div>			

		<div id="destination_list" style="display:none;">
			<form id="wishListForm" method="post" action="widget.php?app=mtp&action=editCountryList" onsubmit="submitWishList(this); return false;">
				<p id="wish_question">What countries are you interested in traveling to?</p>
    			<ul id="countrylist_wish">
					<?php foreach($countryList as $country) : ?>
					<li>
						<input name="sel_countries[]" type="checkbox" value="<?php echo $country->getCountryID() ?>" <?php if (in_array($country->getCountryID(), $countriesInterested['countryIDs'])) : ?>checked="checked"<?php endif; ?> />
						<?php echo $country->getName() ?>												
					</li>	
					<?php endforeach; ?>
				</ul>
                <div>
				    <input type="submit" value="Save changes" /> &nbsp;
					<input type="button" value="Cancel" onclick="LIB.toggleVisibility('destination_list', 'show_destinations');">
				</div>
            </form>
		</div>
		<?php endif; ?>
	</div>
	<?php endif; ?>		
					
<?php elseif ($owner) : ?>
    <p class="side_help_text"><strong>You haven't listed your travels yet.</strong><br />
		Share your travel plans and find out who could be traveling your way! <br />
		<a href="widget.php?travelerID=<?php echo $travelerID; ?>&action=editMTP&do=add"><strong>Create a New Itinerary</strong></a>
	</p>
<?php endif; ?>
</div>

<script>
function submitWishList(myform) {

  new LIB.Ajax('POST', 'widget.php?app=mtp&action=editCountryList', { 
    form: myform, 
    onSuccess: function(o) {
      var obj = eval('(' + o.responseText + ')');
      LIB.getEBI('travelwish_countries').innerHTML = obj.countries;  
      LIB.getEBI('travelwish_title').innerHTML = 'My Travel Wishlist (' + obj.numCountries + ')';
      LIB.toggleVisibility('destination_list', 'show_destinations');
    }
  });
}
</script>