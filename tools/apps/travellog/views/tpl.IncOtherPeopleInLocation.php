<?php
/*
 * Created on Aug 10, 2006
 */
?>

<div class="section" id="travelers">
<h2>
    <span>Travelers<!-- in 
	    <?= (isset($myLocation)) ? $myLocation->getName() : 'My Location' ?>-->
    </span>
    <span class="header_actions">
		<? if (isset($myLocation)) : ?>
			<a href="javascript:void()" onclick="window.open('/setlocation.php?locID=<?=$myLocation->getLocationID()?>','null','height=200,width=500')">Update Location</a>
		<? else: ?>
			<a href="<?=$setLocationLink?>">Set Location</a>
		<? endif; ?>
	</span>
</h2>
<div class="content travelers">
	
<?php if (isset($travelers_array) && isset($myLocation)) : ?>	
			
			<ul id="travelers_container" class="users elements" >				
				<?php foreach ($travelers_array as $traveler) : ?>
					
					<li <?php if ($traveler['traveler']->isOnline()):?>class="online"<?php endif;?>>
						<a title="View Profile" class="thumb" href="/<?=$traveler['username']?>" >
							<img class="pic" alt="User Profile Photo" src="<?=$traveler['traveler']->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" height="37" width="37" />
						</a>	
						<div class="details">	
							<strong><a class="username" href="/<?=$traveler['username']?>"><?= $traveler['traveler']->getUserName()?></a></strong>
							<a href="/messages.php?act=messageTraveler&id=<?=$traveler['traveler']->getTravelerID()?>">Send a Message</a> 
							<?if ($traveler['add_friend_link'] != '' ):?>
								|
								<a href="<?=$traveler['add_friend_link']?>" class="add_friend">
									<a href="<?=$traveler['add_friend_link']?>">Add as Friend</a> 
								</a>
							<?endif;?>
							<br />
							<a class="negative_action" href="/friend.php?action=block&amp;fID=<?=$traveler['traveler']->getTravelerID()?>" onclick="return confirm('Are you sure you want to block this traveler')">Block User</a>						
						</div>	
						<div class="clear"></div>				
					</li>
				<?php endforeach; ?>
			</ul>			

		
<?php elseif (isset($myLocation)) :  ?>
		<p class="help_text">
			<?=$helpText?>				
		</p>		


<?php else : ?>
	
		<p class="help_text"> <span>You haven't set your current location yet</span>. Setting your current location will show you any travelers on the GoAbroad Network who are in the same location you have indicated, their profile will show up here.
		</p>
		<!---<br/><a href="javascript:void()" onclick="window.open('/setlocation.php','null','height=200,width=500')">Update your current location</a>-->
	
<?php endif; ?>
</div>

<div class="foot"></div>	
</div>