<?php if ($contents['show_admin_controls']): ?>
<div class="create">	
	<ul class="actions">
		<li>
			<a href="/journal-entry.php?action=add&amp;travelID=<?=$contents['obj_entry']->getTravelID()?>" class="button">+ Add a Journal Entry</a>
		</li>
		<?php if (0 < $contents['journal']->getDraftsCount()): ?>
			<li>
				<a href="/journal_draft/view_saved_drafts/<?php echo $contents['obj_entry']->getTravelID(); ?>" class="button">View Drafts</a>
			</li>
		<?php endif; ?>
	</ul>	
</div>
<?php endif; ?>

<div class="journalviewer">
	<ul id="journal_entry_list">
	<?php
		//echo $contents['travellogID'];
		
		/*****************************************************************************************************
		 * edits by neri:
		 * 		added the variable entry_videos for the count of video(s) of a journal entry		11-11-08 
		 * 		displayed the number of videos for a journal entry  
		 *****************************************************************************************************/
		
		$curr_travellog_id = $contents['obj_entry']->getTravelLogID(); 
		foreach($contents['col_entry'] as $obj_entry):
			$id                = $obj_entry->getTravelID();
			$entry_title       = $obj_entry->getTitle();
			$entry_date        = (strtotime($obj_entry->getArrival())) ? date('F d, Y',strtotime($obj_entry->getArrival())) : '';
			$entry_photos      = $obj_entry->getCountPhotos();
			$entry_videos      = $obj_entry->getCountVideos();					
			$entry_title_strip = str_replace('-','_',$entry_title);
			$entry_title_strip = str_replace(' ','-',$entry_title_strip); 
			$username          = $obj_entry->getUsername();
	?>
			<li <?php if( $curr_travellog_id == $obj_entry->getTravelLogID()):?>class="active"<?php endif;?>>
				<div>
				<?php if( $curr_travellog_id == $obj_entry->getTravelLogID()):?>
					<strong><?php echo $entry_title; ?></strong>
				<?php else: ?>
					<a href="/<?php echo $obj_entry->getFriendlyUrl()?>" >
						<?php echo $entry_title; ?>
					</a>
				<?php endif; ?>
				<p id = "entryDetails">
					<?php echo $entry_date?><br/>
					<?php 
						if( $entry_photos )
							echo $entry_photos;
						else
							echo 'No';
					?> Photo<?php if( $entry_photos > 1):?>s<?php endif;?> |
					
					<span id = "videosCnt">
						<?php 
							if( $entry_videos )
								echo $entry_videos;
							else
								echo 'No';
						?> 
					</span>	
					Video<?php if( $entry_videos > 1):?>s<?php endif;?>
				</p>
				</div>
			</li>
	
	<?php endforeach;?>
	</ul>
</div>
