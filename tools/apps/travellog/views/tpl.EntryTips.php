<? if( count($contents['col_tips']) || $contents['show_admin_controls'] ): ?>
<div id="main-tips" class="section">
	
	<h2>
	  <span>Tips</span>
	
	  <?if( $contents['show_admin_controls'] ):?>
		  <span class="header_actions"><a href="javascript:void(0)" class="add" id="EntryTipsLink" rel="gatips">Add a Travel Tip</a></span>
		  <span id="tp_loading" style="display:none"><img src="/images/loading.gif" /></span>
	  <?endif;?> 
	
	</h2>
	
	<?if( $contents['show_admin_controls'] ):?>
		<p class="help_text" id="TipsHelpText" <?php if( count($contents['col_tips']) ):?>style="display:none"<?php endif;?>>Got a tip or two for your friends about what to avoid, what to seek out and indulge in, and where to go? Write them here.</p>
	<?endif;?>

		<ul id="ENTRYTIPSLIST" class="content">
			<li id="contenttipscontainer0" style="display:none" class="holdercontent"></li> 
		  
		  <?if( count($contents['col_tips']) ):?>
			  <? $slen = count( $contents['col_tips'] )-1; ?>
			  <? $ctr  = 0; ?>
			  <?	foreach( $contents['col_tips'] as $obj_tips ): ?>
				  <li id="contenttipscontainer<?=$obj_tips->getTravelTipID()?>" class="<?if( $ctr%2 == 0 ):?> stripeone <?else:?> stripetwo <?endif;?>" >
					  <?=nl2br($obj_tips->getDescription())?>
		        <? if( $contents['show_admin_controls'] ):?>
				      <div>
					      <a href="javascript:void(0)" onclick="jQuery.edit_tip(<?php echo $obj_tips->getTravelTipID()?>)"><img border="0" alt="" src="/images/highlight_edit.gif"/> Edit</a> | 
					      <a href="javascript:void(0)" onclick="jQuery.remove_tip(<?php echo $obj_tips->getTravelTipID()?>)"><img border="0" alt="" src="/images/highlight_delete.gif"/> Delete</a>
				      </div>
		        <?endif;?>
				  </li>
			    <? $ctr++; ?>
			  <? endforeach; ?>
		  <?endif;?>
		
		</ul>
</div>

<? endif; ?>
