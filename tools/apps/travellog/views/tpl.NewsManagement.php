<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	//Template::includeDependentCss("/css/tabs.css");
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentJs("/js/newsManager.js");
	ob_start();	
?>		
	<script type="text/javascript">
		NewsManager.init({
			groupID : <?= $groupID?>,
			hlEntryID : <?= $hlEntryID?>,
			method : "<?= $method?>",
			itemsPerPage: <?= $itemsPerPage?>,
			isGroupMember : <?= $isGroupMember?>,
			pageNavPerPage : <?= $pageNavPerPage?>,
			mode : '<?= $mode ?>'
		});
	</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);
	$subNavigation->show();
?>	
	
<div id="content_wrapper" class="layout_2">
	
	<div id="wide_column">
		<div class="section" id="gnews_view">
			<h1 class="big_header"><span>News Details</span></h1>
			<div class="content">					
				<div id="detailPanel" class="widePanel" style="display:none">													
					<h3 id="detailTitle"></h3><div id="detailDate" class="meta date">date</div>
					<?php if($mode == "READ_WRITE"): ?>
					
					<!--<a href="javascript: void(0)" id="editEntryLink" onclick="NewsManager.editEntry(0)">[Edit]</a>&nbsp;-->
					<a href="javascript: void(0)" id="deleteEntryLink" onclick="NewsManager.deleteEntry(0)">[Delete]</a>
					<?php endif; ?>
					
					<div id="detailContent"></div>
				</div>
				<!-- compose panel -->
				<?php if($mode == "READ_WRITE"): ?>
				<div id="composePanel" class="widePanel" style="display:none">
					<h2 id="transMethod">Compose News</h2>
					<ul class="form">
						<li><label for="composeTitle">Title:</label><span class="error message" id="composeTitleErr"></span><input type="text" id="composeTitle" style="width:400px" /><li>
						<li><label for="composeDetails">Details: </label><span class="error message" id="composeDetailsErr"></span><textarea  id="composeDetails" style="width:400px;height:200px"></textarea></li>
						<li><input type="checkbox" id="composeDisplayPublic" />Show Public</li>
						<li class="actions">
							<!--<input type="button" value="Cancel" onclick="NewsManager.cancelEntry()" />&nbsp;-->
							<input type="button" value="Save" onclick="NewsManager.saveEntry()" />
							<input type="hidden" value="0" id="composeID" />
						</li>
					</ul><div class="clear"></div>
				</div>
		
				<?php endif; ?>
				<div id="statusPanel" class="widePanel" style="display:none">
				<p class="loading_message">
					<span id="imgLoading">
						<img src="/images/loading.gif" alt="Loading" />
					</span>									
					<em id="statusCaption">Loading data please wait...</em>
				</p>																											
				</div>						
			</div>
			<div class="foot"></div>
		</div>
	</div>
	<div id="narrow_column">
		<div class="section" id="quick_tasks">
			<h2><span>News</span></h2>
			<div class="content" id="gnews">
				<?php if($mode == "READ_WRITE"): ?>
				<ul class="actions">

					<li><a href="javascript:void(0)" onclick="NewsManager.createEntry()" class="button">Compose</a></li>
					</ul>

				<?php endif; ?>
				
				<div id="pageNavPanel" class="paging"></div>
				<div id="navPanel"></div>						
			</div>
			<div class="foot"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>

