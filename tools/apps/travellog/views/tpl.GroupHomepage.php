<?php
	
	// Added custom title & meta description - icasimpan Apr 25, 2007
	if ($group->getDiscriminator() == 1)
		$titleAndDesciptionPhrase = $group->getName() . ' Club';
	else if ($group->getDiscriminator() == 2)
		$titleAndDesciptionPhrase = $group->getName() . ' Group';
	
	Template::setMainTemplateVar('title', $titleAndDesciptionPhrase . ' - GoAbroad Network: Online Community for Travelers');
	Template::setMainTemplateVar('metaDescription', 'Connect with the travelers on GoAbroad Network by joining the ' . $titleAndDesciptionPhrase); 
	
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
	Template::setMainTemplateVar('layoutID', 'admin_group');
	if (isset($group)){
		//Template::setMainTemplateVar('page_location', $group->getName());
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
		Template::setMainTemplateVar('group', $group);
	}
	else
		Template::setMainTemplateVar('page_location', 'Groups');
	
	if (isset($advisor)){
		Template::setMainTemplateVar('advisor', $advisor);
	}
		
	//$code = <<<BOF
//<script type="text/javascript">
//	jQuery(document).ready(function(){
//		var manager = new groupMessageManager();
		//var membersManager = new groupMembersBoxManager();
//	});
//</script>		 
//BOF;

//Template::includeDependentJs("/js/jquery-1.1.4.pack.js");			
//Template::includeDependentJs("/js/moo1.ajax.js");
//Template::includeDependentJs("/js/groupMessageManager.js",array("bottom"=>true));
//Template::includeDependentJs("/js/groupMembersBoxManager.js",array("bottom"=>true));
	
//	Template::includeDependent($code);
//	Template::includeDependentJs('/js/importantlinks.js',array("bottom"=>true));
//	Template::includeDependentJs('/js/utils/Class.Request.js',array("bottom"=>true));
//	Template::includeDependentJs('/js/DiscussionBoard.js', array('bottom' => true));
	Template::includeDependentJs('/min/g=GroupMainJs');
	if($viewImportantLinks){
	$iLink =<<<BOF
		<script type = "text/javascript">
			//<!--
			window.onload = function(){
				ImportantLinks.initialize();
				ImportantLinks.setGroupId($grpID);
				ImportantLinks.createBox();
			}
			//-->
		</script>
BOF;
	


	Template::includeDependent($iLink);
	}

?>

<script type="text/javascript">
		jQuery.noConflict();
</script>

<?= $mainGroupInfo_view->render()?>		


<? $subNavigation->show() ?>

<div id="rundown">
  <div id="content_wrapper" class="yui-skin-sam">
		<?php if ($this->hasVar('copying_notice_component')): ?>
			<?php $this->getVar('copying_notice_component')->render(); ?>
		<?php endif; ?>
	
	<?php if($isParent && $isAdminLogged): echo $viewGroupSteps->render(); endif; ?>
	  <div id="wide_column" class="layout2">
		  
		  <? if(!$isParent) :?>
			  <div id="subgroup">
				  <?= $grpInfoTemplate ?>
				</div>
			<? endif; ?>
						
			<? if (GROUP::ADMIN == $grpDiscriminator) : ?>
				<? if (isset($grpPhotoAlbumsTemplate)): ?>
					<div id="photoalbum">
						<?= $grpPhotoAlbumsTemplate; ?>
					</div>
				<? endif; ?>
				
				<? if (isset($grpRecentVideosTemplate)): ?>
					<div id="recent_videos">
						<?= $grpRecentVideosTemplate; ?>
					</div>
				<? endif; ?>
			<? endif; ?>
				
			<? //if (isset($grpJournalsTemplate) && $viewGroupJournals): ?>
			<? if (isset($grpJournalsTemplate)) : ?>
				<?= $grpJournalsTemplate->render();	?>			
			<? endif; ?>
			
			<? if($viewGroupArticles): ?>
				<?= $grpArticlesTemplate->render();	?>						
			<? endif; ?>
			
			<!-- BEGIN MESSAGE CENTER -->
			<?php if ($showMessageCenter) : ?>
			  <div id="message_center" class="section">
				  <h2>
				    <span>Message Center</span>
						
						<span class="header_actions">
						  <?php if ($isPowerful) : ?>
								<ul id="message_actions">
									<?php if ($thereAreMoreMessages) : ?>
									<a href="/messages.php?gID=<?php echo $_GET['gID'] ?>">View All</a>
									|
									<?php endif; ?>
									<a href="/news.php?gID=<?php echo $_GET['gID'] ?>">Post News</a>
									|
									<?if(GROUP::ADMIN == $grpDiscriminator):?>
										<?	if( $isParent ): ?>
											<a href="/messages.php?act=messageStaffOfManyGroups&amp;gID=<?=$_GET['gID']?>">Compose Message</a>
										<?	else: ?>
											<a href="/messages.php?act=messageGroupStaff&amp;gID=<?=$_GET['gID']?>">Compose Message</a>
										<?	endif; ?>
									<?else:?>
										<a href="/messages.php?act=messageMembers&amp;gID=<?=$_GET['gID']?>">Compose Message</a>
									<?endif;?>
								</ul>
							<?php endif; ?>
						</span>
				  </h2>
							
					<div class="content">
					  <?php echo $messageCenterView ?>
					</div>
				</div>
		  <?php endif; ?>
      <!-- END MESSAGE CENTER -->
			
			<? /* GROUP DISCUSSIONS */?>
			<?php echo $grpDiscussionsTemplate; ?>
			<? /* END OF GROUP DISCUSSIONS */?>

		</div>

		<div id="narrow_column">
			<? if (isset($groupTagCloud) && !is_null($groupTagCloud)): ?>
				<?= $groupTagCloud; ?>
			<? endif; ?>
		 	<? if (Group::ADMIN == $grpDiscriminator && !$group->isSubgroup()): ?>
				<? // for the subgroups section ?>
				<? if (($viewSubGroups && isset($grpSubGroupsTemplate))): ?>
				  <?= $grpSubGroupsTemplate;	?>
				<? endif; ?>
			  <? // end of subgroups ?>
			  
			  <? if (isset($memberTemplate)): ?>
			    <?php echo $memberTemplate; ?>
			  <? endif; ?>
			  
			  <? if (isset($grpSurveyTemplate)): ?>
				  <?php echo $grpSurveyTemplate; ?>
				<? endif; ?>
					
			<? else : ?>
				<? if (isset($memberTemplate)): ?>
				  <?= $memberTemplate; ?>
				<? endif; ?>
				
				<? if (isset($grpSurveyTemplate)): ?>
				  <?= $grpSurveyTemplate; ?>
				<? endif; ?>
					
			<? endif; ?> 					
									
			<? if ( TRUE == $viewCalendar && isset($grpCalendarTemplate)) : ?>
				<?= $grpCalendarTemplate; ?>
			<? endif; ?> 
					

			<? if ( TRUE == $viewImportantLinks) : ?>
			  <div class="section" id="implinks">
				  <h2>
				    <span> Important Links </span> 
								
					  <? if ($isAdminLogged) : ?>
						  <span class="header_actions">
							  <a href = "javascript:void(0)" onclick = "ImportantLinks.setAction('add'); ImportantLinks.showAddBox()">Add Link</a>
						  </span>
					  <? endif; ?>
					</h2>
							
					<div class="content">
						<div id = "statusPanel2" class = "widePanel" style = "display: none;">
							<p class = "loading_message">
								<span id = "imgLoading">
									<img alt = "Loading" src = "/images/loading.gif"/>
								</span>
								
								<em id = "statusCaption">Loading links please wait...</em>
							</p>
						</div>
						
						<?=$grpImportantLinksTemplate->render()?>
					</div>	
				</div>
			<? endif; ?>
					
			<? if (Group::ADMIN == $grpDiscriminator && $viewResourceFiles && isset($grpFilesTemplate)) : ?>
				  <?= $grpFilesTemplate; ?>
		  <? endif;	?>
	  </div> 
  </div>
</div>	
<!--added by ianne - 11/20/2008 success message after confirm-->
<?if(isset($success)):?>
	<script type="text/javascript">
		window.onload = function(){
			CustomPopup.initPrompt("<?=$success?>");
			CustomPopup.createPopup();
		}
	</script>
<?endif;?>