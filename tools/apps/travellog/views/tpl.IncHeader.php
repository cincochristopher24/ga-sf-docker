<?php
require_once 'Class.HelperGlobal.php';
require_once 'travellog/helper/Class.NavigationHelper.php';
require_once 'travellog/helper/Class.LoginHelper.php';
if (!isset($page_location)) {
	$page_location = '';
}
?>

<div id="header">
		
		<h1>
			<? if ('/' == $_SERVER['REQUEST_URI']): ?>
				<span>GoAbroad.net</span>
			<? else: ?>
				<a href="/"><span>GoAbroad.net</span></a>
			<? endif; ?>
		</h1>
		<p class="slogan"><span>The Network for Travelers</span></p>
		<ul id="skip">
			<? if ('/login.php' == $_SERVER['REQUEST_URI']): ?>
			    <li><a href="#a_login">Skip to Login</a></li>
			<? endif; ?>
			<li><a href="#body">Skip to Content Area</a></li>
		</ul>
		
		<ul id="global_navigation" class="socialMedia">
			<li class="links">
				<strong>Follow</strong>
				<a class="facebook" href="http://facebook.com/GoAbroadCom" title="follow us on facebook"><span>Facebook</span></a>
				<a class="twitter" href="http://twitter.com/GoAbroad" title="follow us on twitter"><span>Twitter</span></a>
				<a class="youtube" href="http://youtube.com/user/GoAbroadCom" title="follow us on youtube"><span>YouTube</span></a>
			</li>

			<li id="access_links">

				<? if (!isset($_REQUEST['ref'])): // login fields won't show in change password if user has forgot his password: neri 12-23-08 ?>
					<? if (HelperGlobal::isLogin()):?>
						<span>You are logged in as <strong><?= HelperGlobal::getEmailAddress() ?></strong></span>
						| <?= HelperGlobal::displayLoginLink(); ?>
					
					 <? elseif ( '/login.php' != $_SERVER['SCRIPT_NAME'] && strpos($_SERVER['REQUEST_URI'], '/admin/' ) === FALSE): ?>
	        			<?		
	        					require_once 'travellog/factory/Class.ControllerFactory.php';
	        					$obj_controllerfactory = ControllerFactory::getInstance();
	        					$viewLogin = $obj_controllerfactory->createController('Login')->performAction();
	        					echo $viewLogin->render();
	        			        LoginHelper::useLabelToggleJs();
	        			?>
	        			
					<? endif;?>
				<? endif; ?>
			</li>
			
		</ul>
		
		
		<?//if (HelperGlobal::isLogin() && !HelperGlobal::isAdvisor()):?>
		<?if (HelperGlobal::isLogin()):?>
		    <p id="header_link_invite_others">
			    <a href="/invite.php"><span>Invite Others</span></a>
			</p>
		<?endif;?>
		
		<ul id="main_nav">
			<?						
				echo NavigationHelper::displayMainNavigation($page_location);
			?>
		</ul>
		<div class="clear"></div>
</div>
