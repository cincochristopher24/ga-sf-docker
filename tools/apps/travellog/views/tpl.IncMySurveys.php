<?php
require_once('Class.GaDateTime.php');
$d = new GaDateTime();
?>
<? if (0 < count($surveyForms)) : ?>
	<div class="section" id="surveys" >
		<a name="my_surveys"><h2><span>Surveys</span></h2></a>
		<div class="content">
				<ul>
				<?php foreach($surveyForms as $iSurveyForm) : ?>
					<li>
						<p class="date"><?= $d->set($iSurveyForm['surveyForm']->getDateCreated())->friendlyFormat(); ?></p>
						<p class="destination">
							<a href="/group.php?gID=<?=$iSurveyForm['group']->getGroupID()?>"><?= $iSurveyForm['group']->getName() ?></a>
						</p>
						<p>
							<a href="/surveyform.php?frmID=<?= $iSurveyForm['surveyForm']->getSurveyFormID() ?>&gID=<?= $iSurveyForm['group']->getGroupID() ?>">
								<strong><?= $iSurveyForm['surveyForm']->getName() ?></strong>
							</a>
							<br />
							<span class="survey_form_caption"><?= $iSurveyForm['surveyForm']->getCaption() ?></span>
						</p>
					</li>
				<?php endforeach;?>
				</ul>
		</div>
		
		<div class="foot"></div>
	</div>
<? endif;?>
