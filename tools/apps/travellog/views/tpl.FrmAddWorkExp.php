
<?php 

Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::includeDependentJs ( '/min/f=js/prototype.js', array('top' => true));
Template::includeDependentJs('/js/interactive.form.js');

$incjs = <<<BOF

<script type="text/javascript">

function createWorkExp(){
	//var _txt = tool.createNewElement('input',{type:'checkbox',value:'x',id:'ID',checked:true});
	//get the number of created forms, count the txtElPosition#
	var workFrame = document.getElementById('workExp'); 	
	var workExpEl = workFrame.getElementsByTagName('input');
	var workExpEltxt = workFrame.getElementsByTagName('textarea');
	var elCtrPos = 1;
	var elCtrEmp = 1;
	var elCtrJob = 1;
	var elCtrLen = 1;
	for(var i=0;i<workExpEl.length;i++){		
		if(workExpEl[i].name.substr(0,14)=='txtWE1Position'){
			elCtrPos++;
		}
		else if(workExpEl[i].name.substr(0,14)=='txtWE1Employer'){
			elCtrEmp++;	
		}
		else if(workExpEl[i].name.substr(0,13)=='txtWE1LenServ'){
			elCtrLen++;
		}
		
	}	
	for(var i=0;i<workExpEltxt.length;i++){
		if(workExpEltxt[i].name.substr(0,13)=='txtWE1JobDesc'){
			elCtrJob++;
		}
	}
	var newHtml = '';
	newHtml += '<h2>Work Experience '+elCtrPos+'</h2>';
	newHtml += '<li class="form_set">';
	newHtml += '<label for="txtWE1Position'+elCtrPos+'">Position</label>';
	newHtml += '<input name="txtWE1Position'+elCtrPos+'" value="" class="text ga_interactive_form_field" size="50" maxlength="50" id="txtWE1Position'+elCtrPos+'" type="text">';
	newHtml += '</li>';
	newHtml += '<li class="form_set">';
	newHtml += '<label for="txtWE1Employer'+elCtrEmp+'">Employer</label>';
	newHtml += '<input name="txtWE1Employer'+elCtrEmp+'" value="" class="text ga_interactive_form_field" size="50" maxlength="50" id="txtWE1Employer'+elCtrEmp+'" type="text">';
	newHtml += '</li>';
	newHtml += '<li class="form_set">';
	newHtml += '<label for="txtWE1JobDesc'+elCtrJob+'">Job Description</label>';
	newHtml += '<textarea class="ga_interactive_form_field" name="txtWE1JobDesc'+elCtrJob+'" cols="48" rows="5" id="txtWE1JobDesc'+elCtrJob+'"></textarea>';
	newHtml += '</li>';
	newHtml += '<li class="form_set">';
	newHtml += '<label for="txtWE1LenServ'+elCtrLen+'">Length of Service</label>';
	newHtml += '<input name="txtWE1LenServ'+elCtrLen+'" value="" class="text ga_interactive_form_field" size="50" maxlength="50" id="txtWE1LenServ'+elCtrLen+'" type="text">';
	newHtml += '</li>';
	workFrame.innerHTML += newHtml;	 
	document.getElementById("hidWID").value++;	
 }


</script>
BOF;

Template::includeDependent($incjs);

	/**
	 * Added by naldz (Nov 20,2006)
	 * This is for the subnavigation links
	 */	
	$subNavigation->show();
?>
		<div class="area" id="content_wrapper">
			<div class="section">
				<h1><span><?= $labelwork ?></span></h1>
				<div class="content">
			
			
					<form name="workexp" method="post" action="resume.php?action=<?= $act ?>" class="interactive_form">
				
				
				
						<div align="left"><a href="javascript:void(0)" onclick="createWorkExp()">Add Another Work Experience</a></div>
				
						<h2>Work Experience 1</h2>
					    <ul class="form" id="workExp">	  
							      <li>
							        <label for="txtWE1Position1">Position</label>
							        <input name= "txtWE1Position1" id="txtWE1Position1" value="" type="text" class="text" size = "50" width = "50" />        
								  </li>
						  	
							      <li>
							        <label for="txtWE1Employer1">Employer</label>
							        <input name= "txtWE1Employer1" id="txtWE1Employer1" value="" type="text" class="text" size = "50" width = "50" />
							      </li>
						  
							      <li> 
							        <label for="txtWE1JobDesc1">Job Description</label>
							        <textarea name="txtWE1JobDesc1" cols="47" rows="5" id="txtWE1JobDesc1"></textarea>
							      </li>
						
							      <li> 
							        <label for="txtWE1LenServ1">Length of Service</label>
							        <input name= "txtWE1LenServ1" id="txtWE1LenServ1" value="" type="text" class="text" size= "50" width= "50" />
							      </li>
						
						</ul>	  
						<input type="hidden" name="hidWID" value="1" id="hidWID">
				      
						<ul class="form">
								<li class="actions">
									<input name  = "btnCancel" type  = "button" value = "Cancel" class = "submit" onclick="javascript:window.location.href='http://<?= $_SERVER['SERVER_NAME'] ?>/resume.php?action=view'" />
									<input name  = "btnSubmit" type  = "submit" value = "Submit" class = "submit" />
								</li>
						</ul>
						<div class="clear"></div>
					</form>
				</div>
				<div class="foot"></div>
			</div>
		</div>