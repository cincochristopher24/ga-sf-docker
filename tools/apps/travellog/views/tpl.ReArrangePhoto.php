
<?php
	ob_start();
	
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'page_photos');
		
	$style = "
			<style>									
				#boxes {font-family: Arial, sans-serif;list-style-type: none;	margin: 0px;padding: 0px;width:800px; display:block;}
				#boxes li {cursor: move;position: relative;float: left;margin: 15px 15px 5px 5px;width: 85px;text-align: center;padding:5px;}
			</style>					
				";
				
				
	Template::includeDependent($style); 
	
	
	Template::includeDependentJs("/js/prototype.js");
	Template::includeDependentJs("/js/scriptaculous/effects.js"); 	
	Template::includeDependentJs("/js/scriptaculous/dragdrop.js");
	Template::includeDependentJs("/js/rearrangephotos.js"); 
	ob_end_clean();
	?>
	
	<? $subNavigation->show(); ?>
	
	<div class="area_wrapper">	
	
		<div class="area" id="intro">
	      	<div class="section">		
				<h1>Rearrange Photos </h1>
			</div>
		</div>			
				
		<div class="area" id="top">
			<div class="content">
				<div class="box_cont2">		
				<div class="top_controls">	
					<? if($owner): ?>
						<p class="control_text">
							<a href="<?= $uploadphoto ?>" class="up_photo">Upload Photo&nbsp;</a>
							<span class="top_text">Click and drag on the photos to rearrange them.</span>
						</p>
								
					<? endif; ?>
					
				</div>
				
					<form name="frmposition" action="<?= $rearrangephoto_action ?>" method="POST" onsubmit="return setPosition()" class="form_align">
							
										<div class="box_arrange">																			
											<ul id="boxes">		
												<? for($x=0; $x<$reccount; $x++): ?>											
														<li itemID="<?= $photo[$x]->getPhotoID(); ?>" class=" rearrange_box">																																							
																<div class="rearrange_whitebg">			
																	<div class="bg_gray">
																		<img class="rearrange_thumb" src="<? echo $photo[$x]->getPhotoLink('thumbnail'); ?>" />																			
																	</div>
																</div>	
														</li>			
												<? endfor; ?>
											</ul>	
											<div class="clear"></div>
											<input type="hidden" name="txtposition" id="txtposition" value="">	
										</div>	
										<br />
										
										<div style="clear:both;"></div>
										<p><input  type="submit" value="Save Changes" name="rearrange"/></p>
										<br /><br />
							
							</form>
				</div>	
	
			
			 <script type="text/javascript" charset="utf-8">
				   	Sortable.create('boxes', {overlap:'horizontal',constraint: false});
			</script>
			</div>
		</div> 
	</div> 
	