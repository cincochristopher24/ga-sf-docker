<?
	/**
	* tpl.ViewMessageConfirmation.php
	* @author marc
	* aug 2006
	*/
	require_once("travellog/helper/DisplayHelper.php");
	require_once("Class.Constants.php");
?>

	<div id="content_wrapper" class="layout_2">
			
				<div class="section" id="section_messages">
				<div class="content">
					<? if ($contents["isSent"]): ?>
						<div class="confirmation">
						    <div class="content">
    							Your message was sent to:<br />
    							<ul><li>
    								<? $size = count($contents["filteredDestination"]["validDest"]); ?>
    								<? foreach( $contents["filteredDestination"]["validDest"] as $destTrav ): ?>
    									<?= displayLink($destTrav,$contents["ownerID"]); ?>
    									<? $size--; 
    									   	if($size == 1):
    											echo "and";
    										elseif($size == 0):
    											echo "";
    										else:
    											echo ",";
    										endif;		
    									?>
    								<? endforeach; ?>
    							</li></ul>
							</div>
						</div>
				
							<?if(count($contents["filteredDestination"]["blockedByDest"])):?>
								<div id="blocked_users">
									<?if( constants::TRAVELER == $contents["user"] ):?> 
											You are blocked by the following user<?if( 1 < count($contents["filteredDestination"]["blockedByDest"]) ):?>s<?endif;?>:
									<?else:?>
											You do not administer the following group<?if( 1 < count($contents["filteredDestination"]["blockedByDest"]) ):?>s<?endif;?>:
									<?endif;?>
									<ul><li>
										<? $size = count($contents["filteredDestination"]["blockedByDest"])?>
										<? foreach( $contents["filteredDestination"]["blockedByDest"] as $destTrav ): ?>
											<?= $destTrav; ?>
											<? $size--; 
											   	if($size == 1):
													echo "and";
												elseif($size == 0):
													echo "";
												else:
													echo ",";
												endif;		
											?>
										<? endforeach; ?>
									</li></ul>	
								</div>
							<?endif;?>

				
							<?if(count($contents["filteredDestination"]["suspendedDest"])):?>
								<div id="suspended_users">
									<?if( constants::TRAVELER == $contents["user"] ): ?>
											The following user<?if( 1 < count($contents["filteredDestination"]["suspendedDest"]) ):?>s are<?else:?> is<?endif;?> unable to receive messages at the moment:
									<?else:?>
											The following group<?if( 1 < count($contents["filteredDestination"]["suspendedDest"]) ):?>s are<?endif;?>/club <?if( 1 < count($contents["filteredDestination"]["suspendedDest"]) ):?>s are<?else:?>is<?endif;?> unable to receive messages at the moment:
									<?endif;?>
									<ul><li>
										<? $size = count($contents["filteredDestination"]["suspendedDest"])?>
										<? foreach( $contents["filteredDestination"]["suspendedDest"] as $destTrav ): ?>
											<?= $destTrav; ?>
											<? $size--; 
											   	if($size == 1):
													echo "and";
												elseif($size == 0):
													echo "";
												else:
													echo ",";
												endif;		
											?>
										<? endforeach; ?>
									</li></ul>	
								</div>
							<?endif;?>	
				
						
							<? if( count($contents["filteredDestination"]["failedDest"]) ): ?>
							<div id="failed_messages">
									The following message recipient<? if( 1 < count($contents["filteredDestination"]["failedDest"]) ): ?>s<? endif; ?> do<? if( 1 == count($contents["filteredDestination"]["failedDest"]) ): ?>es<? endif; ?> not exist : <br>
										<ul><li>
											<? $size = count($contents["filteredDestination"]["failedDest"])?>
											<? foreach( $contents["filteredDestination"]["failedDest"] as $fail ):?> 
												<? echo $fail; ?>
												<? $size--; 
												   	if($size == 1):
														echo "and";
													elseif($size == 0):
														echo "";
													else:
														echo ",";
													endif;		
												?>
											<?endforeach;?>	
										</li></ul>
							</div>
							<? endif; ?>
						
					<? else: ?>	
						<p class="error_notice">An error occurred while sending your message. Please
						contact the administrator.</p>
					<? endif; ?><br />		
			</div>			
		</div>
	</div>
