<div id="main_comment_dialog">
	<div class="mbox_comment" id="mbox_comment" style="display: none;">
    
		<table id="table_comment" class="table_popbox">
			<tbody>
				<tr>
					<td class="popbox_topLeft"></td>
					<td class="popbox_border"></td>
					<td class="popbox_topRight"></td>
				</tr>

				<tr>
					<td class="popbox_border"></td>
					<td class="comment_content" id="comment_content">
						<table cellpadding="0" cellspacing="0" class="commentBox">
							<tr>
								<td>
									<ul class="poke_corner ul_comment_container">	              
										<li id="preview_smile" class="poke_peview">
											<a id="poke1" href="javascript:void(0);" title="Title text here" > Smile </a>
 										</li>
    									<li id="preview_thumbsup" class="poke_peview">
											<a id="poke2" href="javascript:void(0);" title="Title text here" onclick=""> Thumbs Up </a>
        								</li>
										<li id="preview_cheersmate" class="poke_peview">
          									<a id="poke3" href="javascript:void(0);" title="Title text here" onclick=""> Cheers mate </a>
          								</li>
          								<li id="preview_thaiwai" class="poke_peview">
            								<a id="poke4" href="javascript:void(0);" title="Title text here" onclick=""> Thai Wai </a>
          								</li>
          								<li id="preview_wave" class="poke_peview">
          									<a id="poke5" href="javascript:void(0);" title="Title text here" onclick=""> Wave </a>
          								</li>
          								<li id="preview_bowdown" class="poke_peview">
											<a id="poke6" href="javascript:void(0);" title="Title text here" onclick=""> Bow Down </a>
										</li>   
										<li id="preview_handshake" class="poke_peview">
          									<a id="poke7" href="javascript:void(0)" title="Title text here" onclick=""> Congratulate </a>
										</li>              	           	              	              	              	              	
			   						</ul>
								</td>
								<td class="mainField">
           							<? if (!$isLogged): ?>
										<div class="comment_corner loginBlock">
											<p><strong>Not a member but still want to leave a comment?</strong><br>Give your name and email address below. <em>Your comment is subject to moderator's approval.</em></p>
											<div class="other_creds">
												<span>
	                      							<label for="so_name">Name <span class="required">*</span></label>
													<input type="text" name="so_name" id="so_name" />
												</span>
												<span>
													<label for="so_email">Email <span class="required">*</span></label>
													<input type="text" name="so_email" id="so_email" />
												</span>
											</div>
										</div>
									<? endif; ?>

									<div class="comment_corner">
	              			    		<label for="limitedtextarea">Add a comment</label>
										<div style="padding-top: 5px;">
											<textarea id="limitedtextarea" rows="8" cols="37"></textarea>	                
											<div style="margin: 0px; padding: 0px;">
												<input type="text" readonly="yes" id="countdown" size="4" value="1000"/>
												<span style="font-weight: normal; font-size: 10px;">
                    								characters remaining
                								</span>
				    						</div>
										</div>

										<? if (!$isLogged): ?>
											<div class="captcha">
												<img alt="Captcha" src="/captcha.php?code=tUw%2FNB7JNu8%3D&amp;width=150&amp;height=50&amp;dt=1229444913" id="captchaImg" class="captcha_image"/>
												<label for="securityCode">Type the security code above:<span class="required">*</span></label>
	    										<input type="text" class="text ga_interactive_form_field" value="" name="securityCode" id="securityCode" size="17"/>	    
	    										<a class="refresh button_v3" onclick="Captcha.load()" href="javascript:void(0)">Refresh Image</a>
	     										<input type="hidden" value="tUw/NB7JNu8=" name="encSecCode" id="encSecCode" class="ga_interactive_form_field"/>
											</div>
										<? endif; ?>
		              					<div id="add_comment_alert"></div>
					  					<div id="imgLoading1" style="display:none">
											<img src="/images/loading.gif" alt="Loading" style="display:block;margin:0 auto;"/>
										</div>
		             	 				<div>
											<button onclick="Comment.addComment();" class="button_v3">Post</button>
											<button onclick="Comment.hideCommentBox();" class="button_v3">Cancel</button>
										</div>
									</div>
								</td>
 									<? if (!$isLogged): ?>
 										<!-- for the login side of the shout-out -->
 										<td>
										<div class="login_corner">
											<h2><strong>Already a member?</strong></h2>
											<p><span style="color:#4482BE;font-weight:bold">Sign in below</span> so your shout-outs are posted immediately!</p>
											<div class="other_creds">
												<form name="frmLogin" action="/login.php?redirect=<?=$_SERVER['REQUEST_URI']?>" method="post">
													<label>Email Address:</label>
													<input type="text" name="txtEmail" id="login_email" value="" size="26" />
													<label>Password:</label>
													<input type="password" name="pwdPasWrd" id="login_password" value="" size="26" />

													<input type="submit" name="btnLoginSubmit" id="btnLogin" value="Login" class="button_v3" />
													<input type="hidden" value="Log In" name="login" />
													<input type="hidden" value="0" name="hdnLoginType" />
													<input type="hidden" value="<?=$_SERVER['REQUEST_URI']?>" name="hdnReferer" />
												</form>
											</div>
											<?php 
												require_once("travellog/model/Class.SiteContext.php"); 
											 	$siteContext = SiteContext::getInstance();
												if($siteContext->isInCobrand()){
													require_once("travellog/model/Class.AdminGroup.php");
													$cobrand = new AdminGroup($siteContext->getGroupID());
												}
											?>
											<?php if( !$siteContext->isInCobrand() || (isset($cobrand) && $cobrand->directSignupAllowed())): ?>
												<div class="other_creds TopDiv">
													<h2><strong>Not yet a member?</strong></h2>
													<p>Sign up now, it's quick and easy.</p>
													<input type="button" name="btnRegister" id="btnRegister" value="" onclick="window.location.href='/register.php?traveler&redirect=<?=$_SERVER['REQUEST_URI']?>'" class="signButton" />
												</div>
											<?php endif; ?>	
										</div>
									</td>
								<? endif; ?>
	  						</tr>            	
      					</table>
         				</td>
         				<td class="popbox_border"></td>
       			</tr>

       			<tr>
         				<td class="popbox_bottomLeft"></td>
         				<td class="popbox_border"></td>
         				<td class="popbox_bottomRight"></td>
       			</tr>
     			</tbody>
		</table>

	</div>
</div>

<div></div>