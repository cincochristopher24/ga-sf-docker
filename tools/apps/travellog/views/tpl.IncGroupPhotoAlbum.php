<div class="section" id="grpphotoalbum">
	<h2><span>Photo Album</span></h2> 
	<div class="content">												
		<? if ($isAdminLogged) : ?>
		 <div class="header_actions">
			<a href="javascript:void(0)" onclick="showadd_title()" id="newalbum">Add new Album</a>
		</div>
		<? endif;?>
		
		<? print_r($loggedUserID); ?>
		
		<?if(count($grpPhotoAlbums)): ?>
				<!-- Carousel Structure :-->
				<div class="layout_3">
						<div id="PALbum_mycarousel"  class="carousel-component">
							
							<div class="carousel-prev arrow_but">
								<a href="" id="PALbum_prev-arrow" class="left_arrow"></a>
								<!-- <img id="PALbum_prev-arrow" class="left-button-image" src="/js/yahooUI/carousel/black_button_a_active.gif" alt="Previous Button"> -->
							</div>
							
								
							<div class="carousel-next">
								<img id="PALbum_next-arrow" class="right-button-image" src="/js/yahooUI/carousel/black_button_b_active.gif" alt="Next Button">
							</div>
							
							
							
							<div class="carousel-clip-region">
									<ul id="carlist" class="carousel-list carousel-horizontal">
										<? foreach($grpPhotoAlbums as $k => $palbum ): ?>
											<li id="PALbum_mycarousel-item-<?= $k+1; ?>" class="photo_box">
												
												<!-- per album container- for ajax callback request-->
												<div id="peralbum_<?= $palbum->getPhotoalbumID()?>">	
													<? if($isAdminLogged || count($palbum->getPhotos())>0): ?>
														<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(1,1,'gallery',<?= $palbum->getPhotoalbumID()?>)">
															<img src="<?=$palbum->getPrimaryPhoto()->getPhotoLink('default')?>"/>
														</a>
													<?else: ?>
															<img src="<?=$palbum->getPrimaryPhoto()->getPhotoLink('default')?>"/>
													<? endif; ?>
													<!-- album title container- for ajax callback request-->
														<div id="ptitle_<?= $palbum->getPhotoalbumID()?>">
															<p class="photo_caption">
																<?= substr($palbum->getTitle(),0,20)?>
																<br>
																<span style="text-align:right">
																	<?= count($palbum->getPhotos())?> <?= count($palbum->getPhotos())>1?"photos":"photo" ?>	
																</span>
															</p>			
														</div>
													
													<!-- end album title container- for ajax callback request-->
													<div class="actions">
														
														<? if ($isAdminLogged) : ?>
															<? $ptitle = str_replace("'", " ", $palbum->getTitle());?>
															<a href="javascript:void(0)" onclick='showedit_title("<?= $ptitle;?>",<?= $palbum->getPhotoalbumID()?>)'> 	
																Edit Photo Album
															</a>
															<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(1,1,'gallery',<?= $palbum->getPhotoalbumID()?>)">
																Manage/Upload Photos	
															</a>
					
															<a href="javascript:void(0)" onclick="deleteAlbum(<?= $palbum->getPhotoalbumID()?>)">Delete</a>
														<? endif; ?>
													</div>
												</div>
												<!-- end per album container- for ajax callback request-->
												
											</li>
										<? endforeach; ?>	
									</ul>
							</div>
							
							
							<div class="carousel-prev arrow_but">
								<a href="" id="PALbum_next-arrow" class="right_arrow"></a>
								<!-- <img id="PALbum_prev-arrow" class="left-button-image" src="/js/yahooUI/carousel/black_button_a_active.gif" alt="Previous Button"> -->
							</div>
							
						</div>
				</div>
				<!--  End Carousel Structure -->
		
		<?else:?>
			<div style="color:#fff;text-align:center">
				There are no Photo albums added to this group.
			</div>
		<? endif;?>
		
	</div>
	<div class="foot"></div>
</div>



<?if(count($grpPhotoAlbums)):?>
	
	<script type="text/javascript">
		
		var PALbum_handlePrevButtonState = function(type, args) {
			var PALbum_enabling = args[0];
			var PALbum_YUIleftImage = args[1];
			if(PALbum_enabling) {
				PALbum_YUIleftImage.src = "/js/yahooUI/carousel/black_button_a_active.gif";	
			} else {
				PALbum_YUIleftImage.src = "/js/yahooUI/carousel/black_button_a_notactive.gif";	
			}
			
		};
		var PALbum_handleNextButtonState = function(type, args) {
			var PALbum_enabling = args[0];
			var PALbum_YUIrightImage = args[1];
			
			if(PALbum_enabling) {
				PALbum_YUIrightImage.src = "/js/yahooUI/carousel/black_button_b_active.gif";
			} else {
				PALbum_YUIrightImage.src = "/js/yahooUI/carousel/black_button_b_notactive.gif";
			}
			
		};
		
		
		
		new YAHOO.extension.Carousel("PALbum_mycarousel", 
				{
					numVisible:        3,
					animationSpeed:    0.15,
					revealAmount:	   0,	
					scrollInc:         3,
					navMargin:         22,
					prevElement:     "PALbum_prev-arrow",
					nextElement:     "PALbum_next-arrow",
					size:              <?= count($grpPhotoAlbums) ;?>,
					prevButtonStateHandler:   PALbum_handlePrevButtonState,
					nextButtonStateHandler:   PALbum_handleNextButtonState
				}
		);
		
	</script>	
		
<? endif;?>		
		
		
	<script>	
		
		
		YAHOO.namespace("photoalbum.container");
		
		YAHOO.photoalbum.container.palbumid = 0;
		
		function showAddtitle() {
	
			// Define various event handlers for Dialog
			var handleSubmit = function() {
				this.submit();
			};
			var handleCancel = function() {
				this.cancel();
			};
			var handleSuccess = function(o) {
				var response = o.responseText;
				if($F('action') == 'create'){	
					jQuery("#photoalbum").slideDown("slow");
					jQuery("#photoalbum").html(response);
				}else{
					jQuery("#peralbum_"+YAHOO.photoalbum.container.palbumid).html(response);
				}
				
				
			};
			var handleFailure = function(o) {
				alert("Submission failed: " + o.status);
			};
		
			// Instantiate the Dialog
			YAHOO.photoalbum.container.dialog1 = new YAHOO.widget.Dialog("newalbumform", 
									{ width : "25em",
									  fixedcenter : true,
									  visible : false, 
									  constraintoviewport : false,
									  buttons : [ { text:"Save", handler:handleSubmit, isDefault:true },
										      { text:"Cancel", handler:handleCancel } ]
									});
		
			// Validate the entries in the form to require that both first and last name are entered
			YAHOO.photoalbum.container.dialog1.validate = function() {
				var data = this.getData();
				if (jQuery.trim(data.albumtitle) == "" ) {
					alert("Please enter title of album.");
					return false;
				} else {
					return true;
				}
			};
		
			// Wire up the success and failure handlers
			YAHOO.photoalbum.container.dialog1.callback = { success: handleSuccess,
								     failure: handleFailure };
			
			// Render the Dialog
			YAHOO.photoalbum.container.dialog1.render();
			//YAHOO.util.Event.addListener("newalbum", "click", YAHOO.photoalbum.container.dialog1.show, YAHOO.photoalbum.container.dialog1, true);
			
		}
		
		YAHOO.util.Event.onDOMReady(showAddtitle);
		
		function showadd_title(){
			jQuery("#newalbumform").css({display:"block"});	
			$('albumtitle').value = "";
			$('action').value = "create";
			$('albumid').value = 0;
			YAHOO.photoalbum.container.dialog1.show();
		}
		
		function showedit_title(title,albumid){
			jQuery("#newalbumform").css({display:"block"});	
			YAHOO.photoalbum.container.palbumid  = albumid;		
			$('albumtitle').value = title;
			$('action').value = "updatetitle";
			$('albumid').value = albumid;
			YAHOO.photoalbum.container.dialog1.show();
		}
		
		function deleteAlbum(photoalbumid){
			
			var handleSuccess = function(o) {
				var response = o.responseText;
					jQuery("#photoalbum").slideDown("slow");
					jQuery("#photoalbum").html(response);
			};
			var handleFailure = function(o) {alert("Submission failed: " + o.status);};
			var callback ={success:handleSuccess,failure:handleFailure,};
			if(confirm('Do you want to Delete this Album?'))
				YAHOO.util.Connect.asyncRequest('GET', '/photoalbum.php?action=del&photoalbumid='+photoalbumid+'&groupID='+<?= $grpID ?>, callback);
		}
		
	</script>
	


<div class="yui-skin-sam">
	<div id="newalbumform" style="display:none">
		<div class="hd">Please enter title of album</div>
		<div class="bd">
			<form action="/photoalbum.php">
				<input id="albumtitle" type="textbox" size="47" maxsize="25"  name="albumtitle" />
				<input id="albumid" type="HIDDEN" name="albumid" value="0"/>
				<input id="action" type="HIDDEN" name="action" value="create"/>
				<input type="HIDDEN" name="groupID" value="<?= $grpID ?>"/>
			</form>
		</div>
	</div>
</div>
