<?php
	require_once 'Class.Template.php';
	require_once "Class.Paging.php";
	
	class ViewSubGroupsPanel{

		private $mPanel = 'subGroupSection';
		private $mPanelName = "";
		private $mTemplate = "tpl.IncViewModeSubGroupList.php";
		private $mGroup = null;
		private $mAction = '';
		private $mCategory = "all";
		private $mAlertMsg = '';
		private $mMode = 'view';
		private $mIsAdminLogged = false;
		private $mSearchKey = '';
		private $mNumRowsRetrieve = NULL;
		private $mCategories = array();
		
		/**
		 * The list of subgroups to be shown. The length of this list
		 * may not be the same as the groups count.
		 * @var array
		 */
		private $mGrpSubGroups = null;
		
		/**
		 * the total number of subgroups of the given admin group
		 * @var integer
		 */
		private $mGrpSubGroupsCount = 0;
		
		/**
		 * the current page number (for paging)
		 * @var integer
		 */
		private $mCurPage = 0;
		
		/**
		 * Method to set the current page
		 * @param integer $page
		 * @return void
		 */
		function setCurrentPage($page){
			$this->mCurPage = $page;
		}
		
		/**
		 * Sets the category of the subgroups to be shown.
		 * 
		 * @param string $category The category of the subgroups.
		 * @return void
		 */
		function setCategory($category){
			$this->mCategory = $category;
		}
		
		/**
		 * Method to set the number of rows to retrieved from calling the 
		 * getSubGroupsBySection and getCustomGroupSection methods
		 * @param integer $rows
		 * @return void
		 */
		function setNumberOfRowsToRetrieve($rows){
			$this->mNumRowsRetrieve = $rows;
		}
		
		/**
		 * Method to set the subgrouplist to show
		 * 
		 * @param array $groupList
		 * @return void
		 */
		function setSubGroupList($groupList){
			$this->mGrpSubGroups = $groupList;
		}
		
		/**
		 * Method to set the total number of subgroups
		 * 
		 * @param array $groupList
		 * @return void
		 */
		function setSubGroupCnt($cnt){
			$this->mGrpSubGroupsCount = $cnt;
		}
		
		/**
		 * Sets the panel name. This is actually used for custom group section.
		 * The name that is set here corresponds to the current name/label of the custom group section.
		 * 
		 * @param string $name
		 * @return void
		 */
		function setPanelName($name){
			$this->mPanelName = $name;
		}
		
		function setPanel($_panel){
			$this->mPanel = $_panel;
		}
		
		function setGroup($_group){
			$this->mGroup = $_group;
		}
		
		function setAction($_action){
			$this->mAction = $_action;
		}
		
		function setAlertMsg($_alertMsg){
			$this->mAlertMsg = $_alertMsg;
		}
		
		function setMode($_mode){
			$this->mMode = $_mode;
			if ("arrange" == $_mode)
			  $this->mTemplate = "tpl.IncArrangeModeSubGroupList.php";
			else
			  $this->mTemplate = "tpl.IncViewModeSubGroupList.php";
		}
		
		function setIsAdminLogged($_isAdminLogged){
			$this->mIsAdminLogged = $_isAdminLogged;
		}
		
		function setSearchKey($_searchKey){
			$this->mSearchKey = $_searchKey;
		}
		
		function setCategories($categories){
			$this->mCategories = $categories;
		}	

		function render(){
			$numPages = ceil($this->mGrpSubGroupsCount/$this->mNumRowsRetrieve);
			$grpID = $this->mGroup->getGroupID();
			$paging = new Paging($this->mGrpSubGroupsCount, $this->mCurPage, "gID=$grpID&mode=$this->mMode&isAdminLogged=$this->mIsAdminLogged&txtGrpName=$this->mSearchKey&tabAction=$this->mAction&category=$this->mCategory", $this->mNumRowsRetrieve);
			$action_label = ("showActive" == $this->mAction) ? "active" : "inactive";
			$referrer = ('subGroupSection' == $this->mPanel) ? "groupsubgroup" : "groupcustomgroup";
			
			$tpl = new Template();
			$tpl->set_path("travellog/views/subgroups/");
			$tpl->set("grpSubGroups", $this->mGrpSubGroups);
			$tpl->set("numPages", $numPages);
			$tpl->set("grpID", $grpID);
			$tpl->set("alertMsg", $this->mAlertMsg );
			$tpl->set("action", $this->mAction);
			$tpl->set("action_label", $action_label);
			$tpl->set("mode", $this->mMode );
			$tpl->set("pages", $numPages);
			$tpl->set("currentPage", $this->mCurPage);
			$tpl->set("isAdminLogged", $this->mIsAdminLogged );
			$tpl->set("searchKey", $this->mSearchKey );
			$tpl->set("panel", $this->mPanel);
			$tpl->set("referrer", $referrer);
			$tpl->set("panel_name", $this->mPanelName);
			$tpl->set("cur_category", $this->mCategory);
			$tpl->set("paging", $paging);
			$tpl->set("categories", $this->mCategories);
			
			$tpl->out($this->mTemplate);		
		}
	}