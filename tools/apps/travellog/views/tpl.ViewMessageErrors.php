<?php
	/*
	 * tpl.ViewMessageErrors.php
	 * Created on Nov 16, 2007
	 * created by marc
	 */
	 
	require_once("Class.Constants.php");
	require_once("travellog/helper/DisplayHelper.php"); 
?>

<tr>
	<td colspan="2" class="errors">
		<ul class="error_notice">
			<? if( in_array(constants::ERR_SEND_TO_SELF,$contents["arrError"])): ?>
				<li>Sorry, but you can't send a message to yourself, try saving it as a draft instead.</li>
			<? endif; ?>
			<? if( in_array(constants::ERR_BLOCKED,$contents["arrError"])): ?>
				<li>Sorry, we are unable to send this message because you are blocked by: <?= implode(",",$contents["filteredDestination"]["blockedByDest"]); ?></li>
			<? endif; ?>
			<? if( in_array(constants::ERR_SUSPENDED,$contents["arrError"])): 
				if(1 == count($contents["filteredDestination"]["suspendedDest"])): ?>
					<li>Sorry, <?= implode(",",$contents["filteredDestination"]["suspendedDest"]); ?> is unable to recieve messages at the moment.</li>
				<? else: ?>	
					<li>Sorry, the following are unable to receive messages at the moment: <?= implode(",",$contents["filteredDestination"]["suspendedDest"]); ?></li>
				<? endif;	
			   endif; ?>	
			<? if( in_array(constants::ERR_FAILED,$contents["arrError"])): ?>
				<li>Please check to make sure you have spelled the recipient's name correctly.</li>
			<? endif; ?>	
			<? if( in_array(constants::ERR_UNDEFINED_DEST,$contents["arrError"])): ?>
				<li>Unknown message destination.</li>
			<? endif; ?>	
			<? if( in_array(constants::ERR_SUBJECT_MESSAGE,$contents["arrError"])): ?>
				<li>Please write your message and its subject.</li>
			<? endif; ?>
			<? if( in_array(constants::ERR_SUBJECT,$contents["arrError"])): ?>
				<li>Please write a subject for your message.</li>
			<? endif; ?>
			<? if( in_array(constants::ERR_MESSAGE,$contents["arrError"])): ?>
				<li>You have not yet written a message.</li>
			<? endif; ?>
		</ul>
	</td>
</tr>
