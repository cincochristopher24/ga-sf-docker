<?php
	Template::displayLog(false); 
	Template::includeDependentJs('/gaforms/gaformeditor.js');
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('layoutID', 'survey');
	$subNavigation->show();
	ob_start();	
?>
<script type="text/javascript">
//<![CDATA[	
		//Fix image cache problem with IE 6
		try {
			document.execCommand('BackgroundImageCache', false, true);
		} catch(e) {}
		GaFormEditor.initForm({
			'formData'	: <?= $formData ?>,
			'formStage'	: 'container_inner_wrapper',
			'theme'		: 'SurveyCenterGanet',
			'participants' : <?= $jsonParticipants ?>,
			'actionURL'	: '/onlineforms/formeditorhandler.php?groupID=<?= $gID ?>&status=1',
			'afterSave' : function(transport){
				//$('debug').innerHTML = transport.responseText;
				if(true === <?= $refresh ?>){
					window.location = '/onlineforms/formeditor.php?'+'frmID='+transport.responseText;
				}
			}
		});
//]]>
</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);	
?>
<div class="area mine" id="intro">
  <div class="section">
    <h1>Online Forms</h1>
  </div>
</div>
<div class="area_wrapper"> 
	<div id="container">
		<div id="container_inner_wrapper">
			<div id="survey_options">
		    	<ul id="top_tabs">
		        	<li <? if ($formData != 'null') echo 'id="sel"'; ?> ><a href="manage.php">Manage Online Forms</a></li>
		          	<li <? if ($formData == 'null') echo 'id="sel"'; ?> ><a href="formeditor.php">Create a New Online Form</a></li>
		        </ul>
		        <div class="clear"></div>
		   	</div>
		</div>
		<div id="debug"></div>
	</div>
</div>
