<?php	
	require_once ('Class.HtmlHelpers.php');
	Template::setMainTemplateVar('layoutID', 'survey');	
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs('/js/interactive.form.js');
	Template::includeDependentJs('/js/scriptaculous/scriptaculous.js');
	$subNavigation->show();	
	ob_start();	
?>
<script type="text/javascript">
//<![CDATA[	
	function showDetail(divCont){
		if($('link_'+divCont).innerHTML == 'Show'){
			new Effect.SlideDown('cont_'+divCont, { 
				duration: .10
			});
			$('link_'+divCont).innerHTML = 'Hide';
		}
		else{
			new Effect.SlideUp('cont_'+divCont, { 
				duration: .10
			});
			$('link_'+divCont).innerHTML = 'Show';
		}	
	}
//]]>
</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);
?>
	<div id="content_wrapper">	
		<div id="container_inner_wrapper">
			<div id="survey_options">
		    	<ul id="top_tabs">
		    		<li id="sel" class="first_tab"><a href="manage.php" class="first_tab">Manage Online Forms</a></li>
		      		<li><a href="formeditor.php?frmID=0">Create a New Online Form</a></li>      		
		    	</ul>
		    	<div class="clear"></div>
		  	</div>
			<div id="content">
				<div id="stats_content" class="section">
					<h2><?= $onlineForm->getName() ?> Stats (<?= $statsData['totalFormValues'] ?> participant<? if ($statsData['totalFormValues'] > 1) echo 's'; ?>)</h2>
					<p id="survey_caption"><?= $onlineForm->getCaption() ?></p>
					<ul class="tabs">
						<li class="active"><strong><a href="stats.php?frmID=<?= $onlineForm->getGaOnlineFormID() ?>">Overview</a></strong></li>
						<li><?php if( 0 < $statsData['totalFormValues']): ?><a href="stats.php?frmID=<?= $onlineForm->getGaOnlineFormID() ?>&view=entries"><?php endif; ?>Entries<?php if(0<$statsData['totalFormValues']):?></a><?php endif; ?></li>
					</ul>
					<div id="stats_basic">
						<ul>
						<?php
							$fields = $formData['fields'];
							foreach($fields as $iFld):
						?>
							<li>
								<h3>
									<?= $iFld['caption'] ?><?php if($iFld['requiredStatus']):?> <span class="required">*</span><?php endif; ?>
								</h3>
								<?php 
									$fieldValue = $statsData['fields'][$iFld['gaFormFieldID']];
								?>
									<div class="field_results">
										<?php if ( $fieldValue['type'] == 'array' ): ?>
											<table border="0" cellpadding="0" width="100%" cellspacing="0" class="choice_stats">
											<?php 
												foreach($fieldValue['data'] as $fieldCaption => $totalAns):
													$percent = $percent = ($totalAns > 0 ? round(($totalAns/$statsData['totalFormValues'])*100, 1) : 0);		
											?>
													<tr>
														<td class="choice_label"><?= $fieldCaption ?></td>
														<td class="choice_results stat_numbers"><?= $totalAns ?></td>
														<td class="choice_percentage"><?= $percent ?>%</td>
														<td class="choice_percentage_figure"><span><span style="width:<?= $percent ?>%"></span></span></td>
													</tr>
											
											<?php endforeach; ?>
											</table>	
										<?php else: ?>
											<p class="field_info">
												<span class="stat_numbers">
													<?php $responses = count($fieldValue['data']) ?>
													<?= $responses ?>
												</span>
												<?php if(0 < $responses): ?>
													 Responses | <a href="javascript:void(0)" onclick="showDetail(<?=$iFld['gaFormFieldID'] ?>)" id="link_<?=$iFld['gaFormFieldID']?>">Show</a>
												<?php else: ?>
													 Response
												<?php endif; ?> 
											</p>
											<div id="cont_<?=$iFld['gaFormFieldID']?>" style="display:none">
												<ul class="stat_texts">
												<?php foreach($fieldValue['data'] as $iAns):  ?>
													<li>
														<?= HtmlHelpers::Textile($iAns) ?>
													</li>
												<?php endforeach;?>
												</ul>
											</div>
										<?php endif; ?>
									</div>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>	
		<div class="clear"></div>	
	</div>