<?php if(count($groups) > 0): ?>
	<div style="padding: 20px;">
		<?/*php if($approved && count($groups) <= 0): ?>
			<p>This journal is not related to any group.</p>
		*/?>	
		<?php if($approved): ?>
			<?php $groupPhrase = (count($groups) == 1) ? "group" : "following groups";?>
			<p>This journal is featured on the <?php echo $groupPhrase; ?>: </p>
		<?php else:?>
			<?php $groupPhrase = (count($groups) == 1) ? "This group" : "These groups";?>
			<?php $checkPhrase = (count($groups) == 1) ? "Check the group to grant permission and leave it unchecked to deny permission." : "Checking the groups grants them permission while leaving them unchecked denies permission.";?>
			<p>
				<?php echo $groupPhrase; ?> would like to ask permission to approve and feature your journal in their group page.
				<br/><br/>
				<?php echo $checkPhrase; ?>
			</p>
		<?php endif; ?>	
		<ul>
			<?php foreach($groups as $group): ?>
				<li>
					<?php if(!$approved): ?>
						<input class="groupIDs" type="checkbox" value="<?php echo $group->getGroupID();?>" />
					<?php endif; ?>
					<?php echo $group->getName();?>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>