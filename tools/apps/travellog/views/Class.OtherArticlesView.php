<?php

	require_once("travellog/views/Class.AbstractView.php");

	class OtherArticlesView extends AbstractView{	
		function render(){
			$this->obj_template->set("contents",$this->contents);
			return $this->obj_template->fetch("tpl.OtherArticlesView.php");
		}
	}