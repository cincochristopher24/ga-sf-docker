<ul class="form">
<form name="sform" onsubmit="return false;">
	<li>
		<label for="title">Title</label>
		<div id="errTitle" class="errors"></div>
		<input type="text" class="text" name="title" id="title" value="<?=$props['title']?>" size="40" />
	</li>
	<li>
		<label for="txtDate">Date</label>
		<div id="errDate" class="errors"></div> 
		<input style="width:109px;" type="text" readonly="true" class="date-pick text" name="event_date" id="event_date" value="<?=$props['event_date']?>"/>
	</li>
	<li>
		<label for="description">Description</label>
		<div id="errDescription" class="errors"></div>
		<textarea name="description" id="description" rows="10" cols="80"><?=$props['description']?></textarea>
	</li>
	<li>
		<label for="txtVenue">Venue</label>
		<input type="text" class="text" name="venue" id="venue" value="<?=$props['venue']?>" size="40" />
	</li>
	<li>
		<fieldset class="choices">
			<legend><span>Show this Activity To</span></label>
			<ul>
				<li><input type="radio" name="display" id="display1" value="1" <? if( $props['display'] == 1 ) echo 'checked=true'; ?> /><label for="display1">Public</label></li>
				<li><input type="radio" name="display" id="display2" value="2" <? if( $props['display'] == 2 ) echo 'checked=true'; ?> /><label for="display2">Groups</label></li>
			</ul>
		</fieldset>
	</li>
	<li class="actions"> 
		<input type="submit" class="submit" name="action" id="action" value="Save" onclick="jQuery('input').save()" />&nbsp;&nbsp;<input type="submit" class="submit" name="btnAdd" id="btnAdd" value="Cancel" onclick="jQuery().cancel()" />
		<input type="hidden" name="context"       id="context"       value="0"                          />
		<input type="hidden" name="mode"          id="mode"          value="<?=$props['mode']?>"        />
		<input type="hidden" name="start_date"    id="start_date"    value="<?=$props['start_date']?>"  />
		<input type="hidden" name="finish_date"   id="finish_date"   value="<?=$props['finish_date']?>" />
		<input type="hidden" name="pID"           id="pID"           value="<?=$props['pID']?>"         />
		<input type="hidden" name="eID"           id="eID"           value="<?=$props['eID']?>"         />
	</li>
</form>
</ul>
<div class="clear"></div>
 	

