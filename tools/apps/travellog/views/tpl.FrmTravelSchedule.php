<?php
/*
	Filename: 	 tplFrmTravelSchedule.php
	Author:		 Jonas Tandinco
	Purpose:  	 create or modify travel items
	Date created: Jul/11/2007

	EDIT HISTORY:
 
	Jul/18/2006 by Jonas Tandinco
		1.	enabled sub navigation

	Jul/19/2007 by Jonas Tandinco
		1.	changed the order of countries to Alphabetic Ascending

	Sep/05/2007 by Jonas Tandinco
		1.	added link to add destinations
		2.	removed travel status
		3.	remmoved travel destination as it is now optional

	Sep/13/2007 by Jonas Tandinco
		1.	changed the Cancel link into a button but has the same functionality

	Sep/18/2007 by Jonas Tandinco
		1.	made sure start date is displayed only when the start date is specified for a travel

	Sep/20/2007 by Jonas Tandinco
		1.	removed dates from top-level of a travel
		2.	changed UI for destination dates from calendar control to 3 dropdown combo boxes
		
	Sep/24/2007 by Jonas Tandinco
		1.	changed help text
		
	Oct/02/2007 by Jonas Tandinco
		1.	fixed issues on addslashes by adding stripslashes to values if magic gpc is on
		
	Oct/10/2007 by Jonas Tandinco
		1.	updated help text
		
	Oct/11/2007 by Jonas Tandinco
		1.	fixed issue on redirect to main page or profile or passport
		
	Oct/15/2007 by Jonas Tandinco
		1.	updated help text for itinerary description
		
	Oct/31/2007 by Jonas Tandinco
		1.	modified method calls to traveldestination object for its country field
*/
?>

<?php //Template::setMainTemplate('travellog/views/tpl.LayoutMain.php') ?>
<?php Template::setMainTemplateVar('page_location', 'My Passport') ?>

<?php Template::includeDependentCss('/css/travel_todo_style.css') ?>
<?php Template::includeDependentJs('/js/travelschedule.js') ?>

<?php $subNavigation->show() ?>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
		<div class="section">
			<h1 class="big_header">
				<span>
				<?php if (!$travel->getTravelWishID()) : ?>
					Add a New Travel Itinerary
				<?php else: ?>
					Modify Travel Itinerary
				<?php endif; ?>
				</span>
			</h1>
			<div class="content">	
					<?php if (count($errors)) : ?>
					    <div class="error_notice">
						    <ul>
    						<?php foreach ($errors as $error) : ?>
    							<li><?php echo $error ?></li>
    		 				<?php endforeach; ?>
    						</ul>
    					</div>
					<?php endif; ?>

					<form id="facebook" name="facebook" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>?action=save&amp;src=<?php echo isset($_GET['src']) ? $_GET['src'] : 1 ?>">
						<div id="ganet_goabroad_wrapper">

							<h4>Give your itinerary a name:</h4>
							<input name="title" class="input_box" type="text" value="<?php
								if (get_magic_quotes_gpc()) {
									echo stripslashes(htmlentities($travel->getTitle()));
								} else {
									echo htmlentities($travel->getTitle());
								}
							?>" />
							<p>Ex. My World Tour in 2007; Exploring Greece; My Year Out</p>
							<h4>Itinerary Description:</h4>
							<textarea id="travel_details" class="input_box" name="details"><?php
								if (get_magic_quotes_gpc()) {
									echo stripslashes(htmlentities(trim($travel->getDetails())));
								} else {
									echo htmlentities(trim($travel->getDetails()));
								}
							?></textarea>
							<p>Please write a few details about your trip, ex. [My World Tour in 2007] My quest to set foot on every continent</p>
							<div id="theCountries">						
								<?php foreach($travel->getDestinations() as $i => $destination) : ?>
									<div class="destinations" id="<?php echo 'theCountry' . $i ?>">
										<h4 id="lblDestination">Select Destination Country:</h4>
										<select id="<?php echo 'selCountry' . $i ?>" name="selCountries[]" class="destination_select_countries" style="margin: 2px 0 0 0">
											<option value="0">-- Select Country --</option>
											<?php foreach ($countryList as $country) : ?>
												<option id="<?php echo $country->getCountryID() ?>" value="<?php echo $country->getCountryID() ?>" 
													<?php if ($destination->getCountry()->getCountryID() == $country->getCountryID()) : ?>
														selected="selected"
													<?php endif; ?>
												>
													<?php echo $country->getName() ?>
												</option>
											<?php endforeach; ?>
										</select>
										<a id="<?php echo 'btnRemoveCountry' . $i ?>" class="delete_entry" style="font-weight: bold; font-size: 12px;" href="#" onclick="removeCountry(this.parentNode)">delete this destination</a>
										<h4>Destination Details:</h4>
										<textarea id="desc_<?php echo $i ?>" name="dest_descriptions[]" class="input_box"><?php
											if (get_magic_quotes_gpc()) {
												echo stripslashes($destination->getDescription());
											} else {
												echo trim($destination->getDescription());
											}
										?></textarea>
										<p>Please write your specific destination, i.e. the cities, states, provinces, etc.</p>
										<?php
											$arrMonths = array(
													'-Month-',
													'January',
													'February',
													'March',
													'April',
													'May',
													'June',
													'July',
													'August',
													'September',
													'October',
													'November',
													'December'
												)
										?>

										<h4>Travel Start Date:</h4>
										<select style="margin: 0" id="start_month_<?php echo $i ?>" name="start_months[]" onchange="dateReflect(<?php echo $i ?>)">
											<?php for ($j = 0; $j < count($arrMonths); $j++) : ?>
												<option value="<?php echo $j ?>"
													<?php if ($destination->getStartMonth() == $j) : ?>
														selected="selected"
													<?php endif; ?>
												>
													<?php echo $arrMonths[$j] ?>
												</option>
											<?php endfor; ?>
										</select>
										<select style="margin: 0" id="start_day_<?php echo $i ?>" name="start_days[]" onchange="dateReflect(<?php echo $i ?>)">
											<option value="0">-Day-</option>
											<?php for ($j = 1; $j <= 31; $j++) : ?>
												<option value="<?php echo $j ?>"
													<?php if ($destination->getStartDay() == $j) : ?>
														selected="selected"
													<?php endif; ?>
												>
													<?php echo $j ?>
												</option>
											<?php endfor; ?>
										</select>
										<select style="margin: 0" id="start_year_<?php echo $i ?>" name="start_years[]" onchange="dateReflect(<?php echo $i ?>)">
											<option value="0">-Year-</option>
											<?php for ($j = 2000; $j <= 2030; $j++) : ?>
												<option value="<?php echo $j ?>"
													<?php if ($destination->getStartYear() == $j) : ?>
														selected="selected"
													<?php endif; ?>
												>
													<?php echo $j ?>
												</option>
											<?php endfor; ?>
										</select>
										<p>Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</p>

										<h4>Travel End Date:</h4>
										<select style="margin: 0" id="end_month_<?php echo $i ?>" name="end_months[]" onchange="setEndDateChanged(<?php echo $i ?>)">
											<?php for ($j = 0; $j < count($arrMonths); $j++) : ?>
												<option value="<?php echo $j ?>"
													<?php if ($destination->getEndMonth() == $j) : ?>
														selected="selected"
													<?php endif; ?>
												>
													<?php echo $arrMonths[$j] ?>
												</option>
											<?php endfor; ?>
										</select>
										<select style="margin: 0" id="end_day_<?php echo $i ?>" name="end_days[]" onchange="setEndDateChanged(<?php echo $i ?>)">
											<option value="0">-Day-</option>
											<?php for ($j = 1; $j <= 31; $j++) : ?>
												<option value="<?php echo $j ?>"
													<?php if ($destination->getEndDay() == $j) : ?>
														selected="selected"
													<?php endif; ?>
												>
													<?php echo $j ?>
												</option>
											<?php endfor; ?>
										</select>
										<select style="margin: 0" id="end_year_<?php echo $i ?>" name="end_years[]" onchange="setEndDateChanged(<?php echo $i ?>)">
											<option value="0">-Year-</option>
											<?php for ($j = 2000; $j <= 2030; $j++) : ?>
												<option value="<?php echo $j ?>"
													<?php if ($destination->getEndYear() == $j) : ?>
														selected="selected"
													<?php endif; ?>
												>
													<?php echo $j ?>
												</option>
											<?php endfor; ?>
										</select>
										<p>Optional</p>
									</div>
								<?php endforeach; ?>
							</div>
							<div>

							</div>
							<?php if (isset($_GET['src'])) : ?>
								<input type="hidden" name="src" value="<?php echo $_GET['src'] ?>" />
							<?php elseif (isset($_POST['src'])) :?>
								<input type="hidden" name="src" value="<?php echo $_POST['src'] ?>" />
							<?php else : ?>
								<input type="hidden" name="src" value="1" />
							<?php endif; ?>
							<div>
								<input type="hidden" name="hdnTravelWishID" value="<?php echo $travel->getTravelWishID() ?>" />
								<input id="save_btn" type="submit" value="Save" />
								<input style="margin: 20px 10px 0 0;" id="add_destination_btn" type="button" value="Add second destination" onclick="addCountry()"/>
								<?php if (isset($_GET['src'])) : ?>
									<?php if ($_GET['src'] == 2) : ?>
										<a href="profile.php?travelerID=<?php echo $_SESSION['travelerID'] ?>">Cancel</a>
									<?php elseif ($_GET['src'] == 3) : ?>
										<a href="passport.php">Cancel</a>
									<?php else : ?>
										<a href="travelplans.php">Cancel</a>
									<?php endif; ?>
								<?php else : ?>
									<a href="travelplans.php">Cancel</a>
								<?php endif; ?>
							</div>
						</div>
					</form>
				</div>
			<div class="foot"></div>
		</div>
	</div>
	<div id="narrow_column">
		<div class="section" id="quick_tasks">
			<h2><span>MYTRAVELPLANS</span></h2>
			<div class="content">
				<ul class="actions">								
					<li>
						<a href="/travelplans.php" class="button">View Travels</a>				
					</li>	
				</ul>
			</div>
			<div class="foot"></div>
		</div>
		<div class="section">
			<div class="head_left">
				<p class="head_right"></p>
			</div>
			<div class="content" style="padding-top: 5px">
				<p>Let your friends know when and where you want to travel next or show them the places you visited! This is the perfect tool to see who else are going your way and meet other travelers. To export your travel itinerary on Facebook, simply synchronize your accounts after adding the My Travel Plans application.</p>
				<strong id="synch_info">What is synchronize?</strong>
				<p>If you have a Facebook account, you can synchronize your GoAbroad Network Travel Plans with your Facebook profile application. Synchronizing means that both your Facebook and GoAbroad Network profiles will show exactly the same travel itinerary no matter where you edit them. To synchronize, log-in to your Facebook account. Once logged in, just add the My Travel Plans application and choose Synchronize from the Settings menu.</p>
			</div>
			<div class="foot"></div>
		</div>
	</div>
	
	<div class="clear"></div>
</div>

<!--
	<div class="area" id="area_left">
		<div class="section" id="quick_tasks">
			<div class="content">
				<ul class="actions">			
					
					<li>
						<a href="/travelplans.php" class="button">View Travels</a>				
					</li>
	
				</ul>
			</div>
		</div>
	</div>

	<div class="area" id="area_right">
		<div class="section">
			<div class="content">	
				<?php if (count($errors)) : ?>
					<ul class="errors" style="font-family: "Lucida Sans", sans-serif; font-size: 9px;">
					<?php foreach ($errors as $error) : ?>
						<li style="padding: 0 0 0 20px"><span style="color: red;">* </span><?php echo $error ?></li>
	 				<?php endforeach; ?>
					</ul>
				<?php endif; ?>
				
				<form id="facebook" name="facebook" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>?action=save&src=<?php echo isset($_GET['src']) ? $_GET['src'] : 1 ?>">
					<div id="ganet_goabroad_wrapper">
						<h2>
							<?php if (!$travel->getTravelWishID()) : ?>
								Add a New Travel Itinerary
							<?php else: ?>
								Modify Travel Itinerary
							<?php endif; ?>
						</h2>
						<h4>Give your itinerary a name:</h4>
						<input name="title" class="input_box" type="text" value="<?php
							if (get_magic_quotes_gpc()) {
								echo stripslashes(htmlentities($travel->getTitle()));
							} else {
								echo htmlentities($travel->getTitle());
							}
						?>" />
						<p>Ex. My World Tour in 2007; Exploring Greece; My Year Out</p>
						<h4>Itinerary Description:</h4>
						<textarea id="travel_details" class="input_box" name="details"><?php
							if (get_magic_quotes_gpc()) {
								echo stripslashes(htmlentities(trim($travel->getDetails())));
							} else {
								echo htmlentities(trim($travel->getDetails()));
							}
						?></textarea>
						<p>Please write a few details about your trip, ex. [My World Tour in 2007] My quest to set foot on every continent</p>
						<div id="theCountries">						
							<?php foreach($travel->getDestinations() as $i => $destination) : ?>
								<div class="destinations" id="<?php echo 'theCountry' . $i ?>">
									<h4 id="lblDestination">Select Destination Country:</h4>
									<select id="<?php echo 'selCountry' . $i ?>" name="selCountries[]" class="destination_select_countries" style="margin: 2px 0 0 0">
										<option value="0">-- Select Country --</option>
										<?php foreach ($countryList as $country) : ?>
											<option id="<?php echo $country->getCountryID() ?>" value="<?php echo $country->getCountryID() ?>" 
												<?php if ($destination->getCountry()->getCountryID() == $country->getCountryID()) : ?>
													selected="selected"
												<?php endif; ?>
											>
												<?php echo $country->getName() ?>
											</option>
										<?php endforeach; ?>
									</select>
									<a id="<?php echo 'btnRemoveCountry' . $i ?>" class="delete_entry" style="font-weight: bold; font-size: 12px;" href="#" onclick="removeCountry(this.parentNode)">delete this destination</a>
									<h4>Destination Details:</h4>
									<textarea id="desc_<?php echo $i ?>" name="dest_descriptions[]" class="input_box"><?php
										if (get_magic_quotes_gpc()) {
											echo stripslashes($destination->getDescription());
										} else {
											echo trim($destination->getDescription());
										}
									?></textarea>
									<p>Please write your specific destination, i.e. the cities, states, provinces, etc.</p>
									<?php
										$arrMonths = array(
												'-Month-',
												'January',
												'February',
												'March',
												'April',
												'May',
												'June',
												'July',
												'August',
												'September',
												'October',
												'November',
												'December'
											)
									?>
									
									<h4>Travel Start Date:</h4>
									<select style="margin: 0" id="start_month_<?php echo $i ?>" name="start_months[]" onchange="dateReflect(<?php echo $i ?>)">
										<?php for ($j = 0; $j < count($arrMonths); $j++) : ?>
											<option value="<?php echo $j ?>"
												<?php if ($destination->getStartMonth() == $j) : ?>
													selected="selected"
												<?php endif; ?>
											>
												<?php echo $arrMonths[$j] ?>
											</option>
										<?php endfor; ?>
									</select>
									<select style="margin: 0" id="start_day_<?php echo $i ?>" name="start_days[]" onchange="dateReflect(<?php echo $i ?>)">
										<option value="0">-Day-</option>
										<?php for ($j = 1; $j <= 31; $j++) : ?>
											<option value="<?php echo $j ?>"
												<?php if ($destination->getStartDay() == $j) : ?>
													selected="selected"
												<?php endif; ?>
											>
												<?php echo $j ?>
											</option>
										<?php endfor; ?>
									</select>
									<select style="margin: 0" id="start_year_<?php echo $i ?>" name="start_years[]" onchange="dateReflect(<?php echo $i ?>)">
										<option value="0">-Year-</option>
										<?php for ($j = 2000; $j <= 2030; $j++) : ?>
											<option value="<?php echo $j ?>"
												<?php if ($destination->getStartYear() == $j) : ?>
													selected="selected"
												<?php endif; ?>
											>
												<?php echo $j ?>
											</option>
										<?php endfor; ?>
									</select>
									<p>Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</p>
		
									<h4>Travel End Date:</h4>
									<select style="margin: 0" id="end_month_<?php echo $i ?>" name="end_months[]" onchange="setEndDateChanged(<?php echo $i ?>)">
										<?php for ($j = 0; $j < count($arrMonths); $j++) : ?>
											<option value="<?php echo $j ?>"
												<?php if ($destination->getEndMonth() == $j) : ?>
													selected="selected"
												<?php endif; ?>
											>
												<?php echo $arrMonths[$j] ?>
											</option>
										<?php endfor; ?>
									</select>
									<select style="margin: 0" id="end_day_<?php echo $i ?>" name="end_days[]" onchange="setEndDateChanged(<?php echo $i ?>)">
										<option value="0">-Day-</option>
										<?php for ($j = 1; $j <= 31; $j++) : ?>
											<option value="<?php echo $j ?>"
												<?php if ($destination->getEndDay() == $j) : ?>
													selected="selected"
												<?php endif; ?>
											>
												<?php echo $j ?>
											</option>
										<?php endfor; ?>
									</select>
									<select style="margin: 0" id="end_year_<?php echo $i ?>" name="end_years[]" onchange="setEndDateChanged(<?php echo $i ?>)">
										<option value="0">-Year-</option>
										<?php for ($j = 2000; $j <= 2030; $j++) : ?>
											<option value="<?php echo $j ?>"
												<?php if ($destination->getEndYear() == $j) : ?>
													selected="selected"
												<?php endif; ?>
											>
												<?php echo $j ?>
											</option>
										<?php endfor; ?>
									</select>
									<p>Optional</p>
								</div>
							<?php endforeach; ?>
						</div>
						<div>
							
						</div>
						<?php if (isset($_GET['src'])) : ?>
							<input type="hidden" name="src" value="<?php echo $_GET['src'] ?>" />
						<?php elseif (isset($_POST['src'])) :?>
							<input type="hidden" name="src" value="<?php echo $_POST['src'] ?>" />
						<?php else : ?>
							<input type="hidden" name="src" value="1" />
						<?php endif; ?>
						<div style="margin: 20px 0 0 0">
							<input type="hidden" name="hdnTravelWishID" value="<?php echo $travel->getTravelWishID() ?>" />
							<input id="save_btn" type="submit" value="Save" />
							<input style="margin: 20px 10px 0 0;" id="add_destination_btn" type="button" value="Add second destination" onclick="addCountry()"/>
							<?php if (isset($_GET['src'])) : ?>
								<?php if ($_GET['src'] == 2) : ?>
									<a href="profile.php?travelerID=<?php echo $_SESSION['travelerID'] ?>">Cancel</a>
								<?php elseif ($_GET['src'] == 3) : ?>
									<a href="passport.php">Cancel</a>
								<?php else : ?>
									<a href="travelplans.php">Cancel</a>
								<?php endif; ?>
							<?php else : ?>
								<a href="travelplans.php">Cancel</a>
							<?php endif; ?>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
-->

<script type="text/javascript">
	refill(<?php echo count($travel->getDestinations()) ?>);
	$('theCountries').show();
</script>