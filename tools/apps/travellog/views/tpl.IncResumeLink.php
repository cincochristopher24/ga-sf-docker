<?php
/*
 * Created on Aug 10, 2006
 */

?>
<div id="my_resume" class="section">
	<h2>My Resume</h2>
	
	<? if (isset($edit_resume_link)) : ?>
		<p>
			Views: <?=$resume_views ?> <br />
			<a href="<?=$edit_resume_link?>">Edit Resume</a> |
			<a href="<?=$view_resume_link?>">View Resume</a>
		</p>
	<? else:  ?>
		<p class="help_text">
			<span>You haven't created a resume yet</span>. <a href="resume.php?action=add">Create your resume &raquo;</a>	</p>
		
	<? endif; ?>
	
</div> 
