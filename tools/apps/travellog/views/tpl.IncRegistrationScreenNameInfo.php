<div class="close">
	<a href="" onclick="self.parent.tb_remove(this); return false;"><span>close</span></a>
</div>
<div class="container">
	<table class="clear">
		<tr>
			<td>
				<div id="modal_window">
					<h2>What is a Screen Name?</h2>
					<div class="line">
						<div class="unit number">
							1.
						</div>
						<div class="lastUnit">
							<p>
								Your Screen Name serves as your unique identifier on our website. The name you will provide will be used as your own URL as shown below. You can give your personal URL to your friends and they can directly access your profile. 
							</p>
							<img src="/images/registration-page/register-addressbar.jpg" alt="" class="paddingOf10Top" />
						</div>
					</div>

					<div class="line paddingOf30Top">
						<div class="unit number">
							2.
						</div>
						<div class="lastUnit">
							<p>
								Your Screen Name will be the one displayed on your profile page, while keeping your real name private.
							</p>
							<img src="/images/registration-page/register-sampleprofile.jpg" alt="" class="paddingOf10Top" />
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>		
</div>