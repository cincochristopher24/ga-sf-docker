<div class="section newMedia">
	<h2>
		<span>Recent Videos</span>
		<?php if(isset($label)): ?>
			<span class="header_actions"><div><a href="/video.php?gID=<?php echo $grpID ?>"><?php echo $label ?></a></div></span>
		<?php endif; ?>
	</h2>
	<div class="content">
		<?php if(0 < count($videos)): ?>
			
				<?php $i = 0; ?>
				<?php while($i < count($videos) && $i < 4): ?>
				<div class="list">
					<div>
						<p class="duration"><span><?php echo $videos[$i]->getDuration() ?></span></p>
						<?php if ('TravelLogToVideo' == get_class($videos[$i])): ?>
						<a href="javascript:void(0)" onclick="tb_open_new('','/video.php?action=playEntryVideo&videoID=<?= $videos[$i]->getVideoID() ?>&height=360&width=510')">
						<?php else: ?>
						<a href="javascript:void(0)" onclick="tb_open_new('','/video.php?action=playAlbumVideo&videoID=<?= $videos[$i]->getVideoID() ?>&albumID=<?= $videos[$i]->getParentAlbum()->getAlbumID() ?>&height=360&width=510')">
						<?php endif; ?>
							<img src="<?php echo $videos[$i]->getVideoImageUrl() ?>" border="0" align="left" width="120" height="90">
						</a>
						<p class="videoalbum clear"><?php echo $videos[$i]->getTitle() ?></p>
					</div>
				</div>	
					<?php $i++; ?>
				<?php endwhile; ?>
			
			
		<? else: ?>
			<?php if($isAdminLogged): ?>
			<?/*	<div class="help_text" style="text-align:center">
					<span>
						<? if ( $is_parent_group ) : ?>
							There are no Videos added to this group.
						<? else : ?>
							There are no Videos added to this subgroup.
						<? endif ?>
					 	<a href="/video.php?gID=<? echo $grpID ?>">Add a Video?</a>
					</span>
				</div> */?>
				<div class="gline">
					<div class="gunit image">
						<img src="/images/g_videos.jpg" alt="Recent Videos" width="157" height="141" />
					</div>
					<div class="glastUnit description">
						<h3>Got videos of your group in action?</h3>
						<p>
							Show them here! When your members have added their own videos, you can feature your selected videos here.						
						</p>
						<a class="button_v3 goback_button jLeft" href="/video.php?gID=<?=$grpID?>"><strong>Upload a Video</strong></a>
					</div>
				</div>
			<?php endif; ?>
		<? endif; ?>
	</div>
</div>	
