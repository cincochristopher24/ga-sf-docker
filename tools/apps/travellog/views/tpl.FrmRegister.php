<?
	/**
	* tpl.FrmRegister.php
	* @author marc
	* aug 2006
	*/
	
	Template::setMainTemplate("travellog/views/tpl.LayoutMain.php");
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::includeDependentJs('/js/interactive.form.js');
	Template::includeDependentJs('js/tools/common.js');
	Template::includeDependentJs('js/utils/json.js');
	Template::includeDependentJs('captcha/captcha.js');
	
$style = <<<STYLE
<script type="text/javascript">
	var Captcha = new CaptchaLoader({					
		imageTagID	: 'captchaImg',
		onLoad		: function(params){			
			$('encSecCode').value  = params['code'];
		},
		imgAttributes : {
			width : '200',
			height: '50'	
		}
	});
</script>
STYLE;

Template::includeDependent($style);
?>

<div class="area" id="main">
		<div class="section" id="section_register">
			<div class="content">
			<? if( $errCode == 1 ) : ?>
					<p class="help_text"> <span>An account has already been created for <b><em><?= $instName; ?></em></b></span>. You may access your account by <a href="login.php?advisor">logging in</a> to the network.</p>
			<? elseif( $errCode == 2 ) : ?>
					<p class="help_text"> <span>Permission denied in your request</span>. You will now be redirected to the homepage.</p> 
			<? else: ?>
			
				<div class="clear"></div>
				
				<div>
				<?= $frmRegister; ?>
				<?= $chooseTpl; ?>
				
				<? if( $showAccountActivation ): ?>
				<div class="help_reg">
				<h2><a href="activation-support.php">Having Trouble Activating<br /> Your Account?</a></h2>
				</div> 
				
				<div style="clear: both"></div>
                </div>
				
			<? endif; ?>
			<? endif; ?>	
				<div class="clear"></div>
				</div>
			</div>
		<div class="clear"></div>
	</div>
<?   
	if( $errCode == 1 )
		header("Refresh: 3; URL=http://" . $_SERVER['SERVER_NAME'] . "/login.php?advisor");
	elseif( $errCode == 2 )
		header("Refresh: 3; URL=http://" . $_SERVER['SERVER_NAME'] . "/index.php");
?>	

