
<?php

Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
 
	/**
	 * Added by naldz (Nov 20,2006)
	 * This is for the subnavigation links
	 */	
	//SubNavigation::displayLinks();
?>

<div id="wrapper1">
	<div id="wrapper2">
		<div id="maincol">
			<div id="centercol">
				<a href="resume.php?action=view">View Resume</a>
				<h1>Work Experience</h1>
				    
				<?php if($workexpcount): ?>
				<table id="work_experience_table" width="100%">
					<tr>
						<th>Position</th>
						<th>Employer</th>
						<th>Job Description</th>
						<th width="130">Length of Service</th>
					</tr>
				<?php foreach($qworkexp as $value): ?>
				
				   <tr>
				      <td><?= $value->getJobPosition() ?></td>
				      <td><?= $value->getEmployer() ?></td>
				      <td><?= $value->getJobDescription() ?></td>
				      <td><?= $value->getLenServ() ?></td>
				      <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="resume.php?weID=<?= $value->getworkexperienceID() ?>&amp;action=editworkform">Edit</a>
				      	&nbsp;&nbsp;&nbsp;&nbsp;<a href="resume.php?weID=<?= $value->getworkexperienceID() ?>&amp;action=delete" onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
				   </tr>
				   
				<?php endforeach; ?>
				</table>
					
				
				<div id="add_work_experience_form">
					<form name="workloop" method="post" action="resume.php?action=<?= $act ?>">
								<p><strong>Add work experience?</strong> <br />
								<label for="txtWorkCount">Type number of work experience(s):</label>
								<input type="text" name="txtWorkCount" id="txtWorkCount" value="1" size="2" class="text" />&nbsp;
							      			<input type="submit" name="btnSubmit" value="Submit" />
								</p>
					</form>
				</div>
					
			<?php endif; ?>
			
			<?php if($workexpcount == 0): ?>
				<div id="add_work_experience_form" class="action_group">
							<form name="workloop" method="post" action="resume.php?action=<?= $act ?>">
								<p>Add work experience? Type number of work experience(s)&nbsp;<input type="text" name="txtWorkCount" value="1" size="2" class="text" />&nbsp;
							      			<input type="submit" name="btnSubmit" value="Submit" />
								</p>
							</form>
				</div>
			<?php endif; ?>
			
			<?php if($resourcefilecount): ?>
				<div class="action_group">
					<p><strong>Do you want to <a href="resourcefiles.php?action=view&amp;cat=resume&amp;travelerID=<?= $travelerID ?>">remove</a> or <a href="resourcefiles.php?action=view&amp;cat=resume&amp;travelerID=<?= $travelerID ?>">replace</a> your uploaded PDF resume? </strong></p>
				</div>
			<?php else: ?>
				<div class="action_group">
					<p><strong>Do you have a personalized resume? Click <a href="resourcefiles.php?action=add&amp;cat=resume&amp;travelerID=<?= $travelerID ?>">here</a> to upload &raquo;</strong></p>
				</div>
			<?php endif; ?>
			
			
			</div>
</div>
</div>
</div>
