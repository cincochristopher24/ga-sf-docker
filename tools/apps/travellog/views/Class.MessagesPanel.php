<?php
	/*
	 * Created on 12 19, 06
	 * @author Kerwin Gordo
	 * Purpose: A "view" class to summarize all some Message types (Personal,Bulletin,Inquiry) in one "panel"
	 *
	 * Last Edited By: Cheryl Ivy Q. Go			28 August 2008
	 * Purpose: Removed 'View All' link in messages panel if there is no message
	 */

	require_once('Class.Template.php');
 	require_once('travellog/views/Class.PokesView.php');
 	require_once('travellog/model/Class.Traveler.php');
 	require_once('travellog/model/Class.MessageSpace.php');
 	require_once('travellog/model/Class.BulletinMessage.php');
 
 	class MessagesPanel {
	 	public static $ALL 				= 1;
	 	public static $PERSONAL_MESSAGE = 2;
	 	public static $BULLETIN 		= 3;
	 	public static $INQUIRY 			= 4;
	 	public static $GREET			= 7;
	 	public static $ALERT 			= 8;
 		public static $SURVEY			= 9;

	 	//edited by daf; changed private to protected

		protected $msgs						= array();
	 	protected $context					= 0;	// traveler or group object
	 	protected $maxMessages				= 50;	// max no. of messages to display
 	 	protected $totalNumberOfNewMessages	= 0;

	 	public function setContext($context){
	 		$this->context = $context;
	 	}

	 	public function fetch($messType = 1) {
	 		ob_start();
	 		$this->render($messType);
	 		$code = ob_get_contents();
	 		ob_end_clean();
	 		return $code;
	 	}
 	
	 	/**
	 	 * function to display the Messages
	 	 * @param integer $messType type of message to render 
	 	 */
	 	public function render($messType = 1){ 		

	 		if (is_a($this->context,'Traveler')) { 		
		 		if (0 != $this->context->getTravelerID()){		 		
			 		switch ($messType){
			 			case MessagesPanel::$ALL :
			 				$this->renderAll();
			 				break;
			 			case MessagesPanel::$PERSONAL_MESSAGE :
			 				$this->renderPersonalMessages();
			 				break;
			 			case MessagesPanel::$BULLETIN:
			 				$this->renderBulletin();
			 				break;
			 			case MessagesPanel::$ALERT:
			 				$this->renderAlerts();
			 				break;
			 			case MessagesPanel::$INQUIRY:
			 				$this->renderInquiries();
			 				break;
			 			case MessagesPanel::$GREET:
			 				$this->renderGreetings();
			 				break;
			 			case MessagesPanel::$SURVEY:
			 				$this->renderSurveys();
			 				break;
			 			default: 
			 				$this->renderAll();
			 				break;
			 		}
		 		}	
	 		} elseif (is_a($this->context,'Group')) {
	 			$this->renderBulletin();
	 		}	
	 	}
 	
	 	/**
	 	 * display all messages side by side sorted by date
	 	 * 	 Bulletin: show from current date, Personal: show unread, Inquiries: showall 
	 	 */
	 	private function renderAll(){
			require_once('travellog/model/Class.HelpText.php');
			$ht = new HelpText();
			
			/********* START: LAST EDITED BY: Cheryl Ivy Q. Go *************/
			$messages = array();
	 		$ms = new MessageSpace();
			
	 		// get bulletins (Filter:NEW)
	 		$tempMessages = $ms->getBulletins($this->context->getSendableID());

			foreach($tempMessages as $indMessage){
				if (!$indMessage->isRead())
					$messages[] = $indMessage;
			}				
			/********* END: LAST EDITED BY: Cheryl Ivy Q. Go *************/

	 		// get personal messages (unread)
	 		$messages = array_merge($messages,$this->context->getInbox()->getNewMessages());

	 		// get inquiries
	 		$messages = array_merge($messages,$this->context->getInquiryBox()->getMessages(null,'unread'));

			/******************* START: GET GREETINGS, SALUTATIONS AND COMMENTS **********************/
			// profile
			if (0 < count($this->context->getTravelerProfile()->getComments(null, true))){
				$comments = $this->context->getTravelerProfile()->getComments(null, true);
		 	 	$comments = $this->filterComments($comments);
				if (0 < count($messages))
					$messages = array_merge($messages, $comments);
				else
					$messages = $comments;
			}

			// journal-entry
	 	 	$tlogs = Travellog::getTravellogs($this->context);
			if (null != $tlogs) {
		 	 	foreach ($tlogs as $tlog){
		 	 		if ($tlog->getComments(null, true) != null){
		 	 			if (0 < count($tlog-> getComments(null, true))){
		 	 				$comments = $tlog-> getComments(null, true);
		 	 				$comments = $this->filterComments($comments);
		 	 				if (0 < count($messages))
		 	 					$messages = array_merge($messages,$comments);
		 	 				else
		 	 					$messages = $comments;
		 	 			}	 	 			
		 	 		}
		 	 	}
	 	 	}

			// get photo comments
			$photos = $this-> context-> getTravelerProfile()-> getPhotos();
	 	 	if (null != $photos) {
		 	 	foreach ($photos as $photo){
		 	 		if (0 < count($photo->getComments(null, true))){
		 	 			$comments = $photo->getComments(null, true);
		 	 			$comments = $this->filterComments($comments);
		 	 			if (0 < count($messages))
		 	 				$messages = array_merge($messages, $comments);
		 	 			else
		 	 				$messages = $comments;
		 	 		}
		 	 	}
	 	 	}
			/****************** END: GET GREETINGS, SALUTATIONS AND COMMENTS **********************/
			
			
			/******** START: Get Alerts ********/	 	
			$alerts = $this->getAlerts();
			if(!is_null($alerts)){
				$messages = array_merge($messages, $alerts);
			}
			/******** END: Get Alerts ********/
			
			/******** START: Get Surveys ********/
			$surveys = $this->getSurveys();
			$messages = array_merge($messages, $surveys);
			/******** END: Get Surveys ********/
			if (0 < count($messages)){
		 		// sort the messages by date
		 		usort($messages,array('Message','orderByDate'));
		 		// reverse it for descending order
		 		$messages = array_reverse($messages);
			}
 		
	 		if (count($messages) > $this->maxMessages)
	 			$messages = array_slice($messages,0,$this->maxMessages);
	 		
			$this->totalNumberOfNewMessages = count($messages);
	 		
			$template = new Template();
	 		$template->set('messages',$messages);
	 		$template->set('messType',MessagesPanel::$ALL); 
	 		$template->set('dashboard_messagecenter_mess',$ht->getHelpText('dashboard-messagecenter'));		
	 		$template->out('travellog/views/tpl.MessagesPanel.php');
	 	}
 		
		public function getTotalNumberOfNewMessages(){
			return $this->totalNumberOfNewMessages;
		}
		public function getTotalNumberOfMessages(){
			return count($this->msgs);
		}
		public function getBulletinsForMessagesPanel(){
			require_once('travellog/model/Class.MessageBoard.php');
			require_once('travellog/model/Class.SessionManager.php');

			$messageBoard = new MessageBoard();
			$obj_session = SessionManager::getInstance();

	 		$messages = $messageBoard->getBulletins(array(
	 			'owner' => $this->context,
	 			'viewer'=> new Traveler($obj_session->get('travelerID'))
	 		));

			if (count($messages) > $this->maxMessages)
		 			$messages = array_slice($messages, 0, $this->maxMessages);

			if (count($messages)){
		 		// sort the messages by date
		 		usort($messages, array('Message', 'orderByDate'));
		 		// reverse it for descending order
		 		$messages = array_reverse($messages);
	 		}

		 	$this->msgs = $messages;
		}

	 	/**
	 	 * display all Bulletin message type
	 	 */
	 	private function renderBulletin(){
	 		/*
			$ms = new MessageSpace();

	 		// get bulletins
	 		$messages = $ms->getBulletins($this->context->getSendableID(),new RowsLimit($this->maxMessages));
 		
	 		//if (count($messages) > $this->maxMessages)
	 		//	$messages = array_slice($messages,0,$this->maxMessages);
 			 			 		 		 		
	 		$template = new Template();
	 		$template->set('messages',$messages);
	 		$template->set('messType',MessagesPanel::$BULLETIN); 		
	 		$template->out('travellog/views/tpl.MessagesPanel.php');
			
			require_once('travellog/model/Class.MessageBoard.php');
			require_once('travellog/model/Class.SessionManager.php');

			$messageBoard = new MessageBoard();
			$obj_session = SessionManager::getInstance();
	 		$messages = $messageBoard->getBulletins(array(
	 			'owner' => $this->context,
	 			'viewer'=> new Traveler($obj_session->get('travelerID'))
	 		));

			if (count($messages) > $this->maxMessages)
		 			$messages = array_slice($messages, 0, $this->maxMessages);

			if (count($messages)){
		 		// sort the messages by date
		 		usort($messages, array('Message', 'orderByDate'));
		 		// reverse it for descending order
		 		$messages = array_reverse($messages);
	 		}
			*/

	 		$template = new Template();
	 		$template->set('messages', $this->msgs);
	 		$template->set('messType', MessagesPanel::$BULLETIN);
	 		$template->out('travellog/views/tpl.MessagesPanel.php');
	 	}
	 	
	 	/***
	 	 * Added by: Naldz (Display Alerts)
	 	 * Feb 27, 2008
	 	 */
	 	private function getAlerts(){
	 		require_once('travellog/model/Class.SessionManager.php');
			$obj_session = SessionManager::getInstance();
			$messageSpace = new MessageSpace();
			return $messageSpace->getAlerts($this->context->getSendableID());
	 	}
 		private function renderAlerts(){
			$alerts = $this->getAlerts();
			$template = new Template();
	 		$template->set('messages', $alerts);
	 		$template->set('messType', MessagesPanel::$ALERT);
 			$template->out('travellog/views/tpl.MessagesPanel.php');	 				
 		}
 		
 		
 		/**
 		 * Added by: Naldz (Display Surveys)
 		 * Feb 27, 2008
 		 */
 		protected function getSurveys(){
			//get the survey forms of this traveler that needs his participation.
			require_once('travellog/model/formeditor/Class.SurveyForm.php');
			$tempSurveys = SurveyForm::getUnparticipatedSurveyFormsByTraveler($this->context->getTravelerID());
			$surveys = array(); 			
			foreach($tempSurveys as $iSurvey){
				$surveys[] = $iSurvey['surveyForm'];
			}
			return $surveys;
 		}
 		protected function renderSurveys(){
 			$surveys = $this->getSurveys();
 			$template = new Template();
	 		$template->set('messages', $surveys);
	 		$template->set('messType', MessagesPanel::$SURVEY);
 			$template->out('travellog/views/tpl.MessagesPanel.php');
 		}

		public function getPersonalMessagesForMessagesPanel(){
			$this->msgs = array();

			$ms = new MessageSpace();
			$rl = new RowsLimit($this->maxMessages);

	 		$this->msgs = $this->context->getInbox()->getMessageList($rl);
		}

	 	/**
	 	 * display Personal messages
	 	 */
	 	private function renderPersonalMessages() {
			/*
	 		$ms = new MessageSpace();

			$rl = new RowsLimit($this->maxMessages);
 		 		
	 		// get personal messages (all)
	 		//$messages = $this->context->getInbox()->getNewMessages();
	 		$messages = $this->context->getInbox()->getMessageList($rl);
 		
	 		//if (count($messages) > $this->maxMessages)
	 		//	$messages = array_slice($messages,0,$this->maxMessages); 		
 		 	*/

	 		$template = new Template();
	 		$template->set('messages', $this->msgs);
	 		$template->set('messType', MessagesPanel::$PERSONAL_MESSAGE);
	 		$template->out('travellog/views/tpl.MessagesPanel.php'); 		
	 	}
 	
	 	/**
	 	 * display all Inquiries
	 	 */
	 	 protected function renderInquiries(){ 	 		
	 	 	$template = new Template();
	 	 	if (is_a($this->context,'Traveler')) {
		 	 	$messages = $this->context->getInquiryBox()->getMessages(new RowsLimit($this->maxMessages));	 	 	
		 		$template->set('messages', $messages);	 		
		 		$template->set('messType', MessagesPanel::$INQUIRY);
		 		$template->out('travellog/views/tpl.MessagesPanel.php');
	 	 	}
	 	 }

		/**
		 * 
		 */
		private function renderGreetings(){
			$template = new Template();
			$rl = new RowsLimit($this->maxMessages);

			// profile
			$messages = $this->context->getTravelerProfile()->getComments(null, true);

			// journal-entry
	 	 	$tlogs = Travellog::getTravellogs($this->context);
			if (null != $tlogs) {
		 	 	foreach ($tlogs as $tlog){
		 	 		if ($tlog->getComments($rl, true) != null){
		 	 			if (0 < count($tlog-> getComments($rl, true))){
		 	 				if (0 < count($messages))	 	 				
		 	 					$messages = array_merge($messages, $tlog-> getComments($rl, true));
		 	 				else
		 	 					$messages = $tlog-> getComments($rl, true);
		 	 			}
		 	 		}
		 	 	}
	 	 	}

			// get photo comments
			$photos = $this-> context-> getTravelerProfile()-> getPhotos();
	 	 	if (null != $photos) {
		 	 	foreach ($photos as $photo){
		 	 		if (0 < count($photo->getComments($rl, true))){
		 	 			if (0 < count($messages))
		 	 				$messages = array_merge($messages, $photo->getComments($rl, true));
		 	 			else
				 		 	$messages = $photo->getComments($rl, true);
		 	 		}
		 	 	}
	 	 	}

			if (0 < count($messages)){
				// sort the messages by date
		 		usort($messages, array('Comment','orderByDate'));
		 		// reverse it for descending order
		 		$messages = array_reverse($messages);
			}

	 	 	$template->set('messages', $messages);	 		
	 		$template->set('messType', MessagesPanel::$GREET);
	 		$template->out('travellog/views/tpl.MessagesPanel.php');
		}


	 	 /**
	 	  * function called if Context is traveler object wherein old bulletins (older than datejoined) of the group he joined
	 	  * 	will not be displayed 
	 	  *  @param array $messages		array of Bulletin Messages
	 	  *  note: messages must be ordered by date descending so that the array will not be looped to the end
	 	  */ 
	 	 /***
	 	  * This function was turned public by naldz (Feb 19, 2007)
	 	  */
/*	 	 public function removeBulletinOlderThanMe($messages) {
	 	 	if ('Traveler' == get_class($this->context)) {
 	 		
	 	 	  /*  $dateJoined = $this->context->getTravelerProfile()->getDateJoined();
 	 		
	 	 		$offset = 0;
	 	 		$size = count($messages);
	 	 		foreach($messages as $message){
	 	 			if ($dateJoined > $message->getCreated()) {
	 	 				if ($offset > 0){ 	 				
	 	 					$messages = array_slice($messages,0,$offset); 	 					
	 	 				}else { 
	 	 					$messages = array_slice($messages,0,0);
	 	 				} 	 					
	 	 				break;
	 	 			}
	 	 			$offset++;
	 	 		} */	
 	 		
	 	 	/*	$fmessages = array();			// new array of filtered messages
	 	 		$groupsMembership = array();		// an associative array (groupID => Group membership date) of groups belonging to a traveler, this is for fast lookup of date of membership (no db)
	 	 		$friendship = array();				// an associative array  (travelerID => Friendship date)  of friends belonging to a traveler, this is for fast lookup of friendship date (no db) 
 	 		
 	 		
	 	 		/*foreach ($messages as $message){ 	 			
 	 			
	 	 			if (is_a($message->getSource(),'Group')){
 	 				
	 	 				$group = $message->getSource();
	 	 				// search the groupID from groupsMembership array if existing
	 	 				if (array_key_exists('' . $group->getGroupID()  , $groupsMembership )) {
	 	 					if ($message->getCreated() > $groupsMembership['' . $group->getGroupID()] ){
	 	 						// message is after the groupmembership so we include this message
	 	 						$fmessages[] = $message;
	 	 					} 
	 	 				} else {
	 	 					// group id is not in groupsMembership array so we append this for use of the next searches
	 						$mbshipDate = $group->getMembershipDate($this->context);
	 						$groupsMembership['' . $group->getGroupID()] = $mbshipDate;
	 						// compare the message date 						
	 						if ($message->getCreated() > $mbshipDate ) {
	 							// message is after the groupmembership so we include this message
	 	 						$fmessages[] = $message;
	 						} 						
	 	 				}
 	 				
	 	 			} 
	 	 			if (is_a($message->getSource(),'Traveler')){
	 	 				$traveler = $message->getSource();
	 	 				// search travelerID from  friendship array if existing
	 	 				if (array_key_exists('' . $traveler->getTravelerID(), $friendship )) {
	 	 					if ($message->getCreated() > $friendship['' . $traveler->getTravelerID()]) {
	 	 						// message is after friendship so we include this message
	 	 						$fmessages[] = $message;	
	 	 					} 	 					
	 	 				} else {
	 	 					// travelerID is not in friendship array so we append this for use of the next searches
 	 		
	 	 						$fshipDate = $traveler->getFriendshipDate($this->context->getTravelerID());
	 	 						$frienship['' . $traveler->getTravelerID()] = $fshipDate;
	 	 						// compare the message date
	 	 						if ($message->getCreated() > $fshipDate) {
									// message is after friendship so we include this message
									$fmessages[] = $message; 	 							
	 	 						}
 	 						 	 		
	 	 				}		
	 	 			} 
 	 			
 	 			
	 	 		} */
 	 		
	 	 	/*	foreach ($messages as $message) {
	 	 			if ($this->shouldIncludeMessage($this->context,$message)) {
	 	 				$fmessages[] = $message;
	 	 			}
	 	 		}  
 	 		
 	 		
	 	 	} 	 
	 	 	return $fmessages; 	 	
	 	 	//return $messages;
	 	 }*/
 	 
	 	 /**
	 	  * function to determine if the message should be included in the display. This will check the destinations
	 	  * 	of the message and check if the date of membership for groups and friendship for friends is older 
	 	  * 	than the creation of the message. If so this message is included 
	 	  * @param $traveler	traveler object
	 	  * @param $message		Message object
	 	  * @return boolean
	 	  */
	 	 private function shouldIncludeMessage($traveler,$message) {
	 	 	$mygroups = $traveler->getGroups();
	 	 	$dateref = date('Y-m-d');		// stores the oldest membership date /or friendship date , this is to be compared against creation date of message
	 	 	$oldestgroup = null;
 	 	
	 		$destinations = $message->getDestination();
	 		foreach ($destinations as $dest){
	 			if (is_a($dest,'Group')) {
	 				// check if $dest is in mygroups
	 				foreach ($mygroups as $mygroup){
	 					if ($dest->getGroupID() == $mygroup->getGroupID())
	 					{
	 						$mshipDate = $dest->getMembershipDate($traveler);
	 						// replace dateref if mshipDate is older
			 				if ($mshipDate < $dateref)
			 					$dateref = $mshipDate;
	 					}
	 				} 				
	 			}  			 			
	 		}
 		
	 		// check if the source is a friend
	 		$source = $message->getSource();
	 		if ($source->isFriend($traveler->getTravelerID())) {
	 			$fshipDate = $source->getFriendShipDate($traveler->getTravelerID());
	 			if ($fshipDate < $dateref)
	 				$dateref = $fshipDate;
	 		}
 			
	 	 	if ($dateref < $message->getCreated())
	 	 		return true;
	 	 	else 
	 	 		return false;			 	 		 	 	
 	 	
	 	 }
	 	 
	 	 /**
	 	  * function to remove (filter out) comments from list 
	 	  * 	@param string $filter 'the status of the comments that will be filtered out'
	 	  */
	 	 protected function filterComments($comments,$filter = 'read'){
	 	 	$i = 0;
	 	 	foreach ($comments as $comment){
	 	 		if ($filter == 'read'){
	 	 			if ($comment->getIsRead())
	 	 				unset($comments[$i]);	
	 	 		}
	 	 		if ($filter == 'unread'){
	 	 			if (!$comment->getIsRead())
	 	 				unset($comments[$i]);
	 	 		}
	 	 		$i++;
	 	 	}
	 	 	
	 	 	return $comments;
	 	 }
	 	 
 	 
	 	 /**
	 	  * tells if the message received is sent to me directly and or sent to the group where I belong
	 	  * @return numeric 1 = sent directly as a friend bulletin, 2 = sent to me as a member of a group 
	 	  */
	 	/* private function isSentAsFriendBulletin($message){
	 	 	if (is_a($this->context,'Traveler')){
	 	 		$destinations = $message->getDestination();
	 	 		foreach ($destinations as $dest){
	 	 			if (is_a($dest,'Traveler')) {
	 	 				if ($this->context->getTravelerID() == $dest->getTravelerID())
	 	 					return true;
	 	 			}	
	 	 		} 	 		
	 	 	}
	 	 	return false;
	 	 } */
 	 
	 }
 
?>