<?php
	/*
	 * tpl.ViewMessageActions.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
	 require_once("Class.Constants.php");
?>
<?php if( in_array(constants::REPLY,$contents["messageActions"]) ): ?>
	<input type="submit" name="btnReply" id="btnReply" value="Reply" class="submit" onclick="document.getElementById('hdnAction').value = 'REPLY'" />&nbsp;
<?php endif; ?>
<?php if( in_array(constants::EDIT,$contents["messageActions"]) ): ?>
	<input type="submit" name="btnEdit" id="btnEdit" value="Edit" class="submit" onclick="document.getElementById('hdnAction').value = 'EDIT'" />&nbsp;
<? endif; ?>
<?php if( in_array(constants::MOVE_TO_TRASH,$contents["messageActions"]) ): ?>
	<input type="submit" name="btnTrash" id="btnTrash" value="Send to Trash" class="submit" onclick="document.getElementById('hdnAction').value = 'MOVE_TO_TRASH'" />
<? else: ?>
	<input type="submit" name="btnDelete" id="btnDelete" value="Delete" onclick="javascript: if( confirm('Are you sure you want to delete the selected messages?')){ document.getElementById('hdnAction').value = 'DELETE'; return true; } else{ return false;}"  class="submit" onclick="" />	  
<? endif; ?>
<input type="submit" name="btnBack" value="Back" class="submit" />
