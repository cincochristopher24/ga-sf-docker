<?php
	/*
	 * Class.RegisterViewsFactory.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	 require_once("Class.Constants.php");
	 
	 class RegisterViewsFactory{
	 	static $instance = NULL;
	
		private $views = array();
		
		static function getInstance(){ 
			if( self::$instance == NULL ) self::$instance = new RegisterViewsFactory; 
			return self::$instance;
		}
		
		function createView( $view = constants::VIEW_CHOOSE_REGISTER){
			
			switch($view){
				case constants::VIEW_TRAVELER_REGISTRATION_FORM:
					if( !array_key_exists("ViewTravelerRegistrationForm", $this->views) ){
						require_once("travellog/views/Class.ViewTravelerRegistrationForm.php");
						$this->views["ViewTravelerRegistrationForm"] = new ViewTravelerRegistrationForm;
					}
					return $this->views["ViewTravelerRegistrationForm"];
				
				case constants::VIEW_GROUP_REGISTRATION_FORM:
					if( !array_key_exists("ViewGroupRegistrationForm", $this->views) ){
						require_once("travellog/views/Class.ViewGroupRegistrationForm.php");
						$this->views["ViewGroupRegistrationForm"] = new ViewGroupRegistrationForm;
					}
					return $this->views["ViewGroupRegistrationForm"];	
				
				case constants::VIEW_REGISTRATION_DETAILS:
					if( !array_key_exists("ViewRegistrationDetails", $this->views) ){
						require_once("travellog/views/Class.ViewRegistrationDetails.php");
						$this->views["ViewRegistrationDetails"] = new ViewRegistrationDetails;
					}
					return $this->views["ViewRegistrationDetails"];		
				
				case constants::VIEW_WELCOME_PAGE:
					if( !array_key_exists("ViewWelcomePage", $this->views) ){
						require_once("travellog/views/Class.ViewWelcomePage.php");
						$this->views["ViewWelcomePage"] = new ViewWelcomePage;
					}
					return $this->views["ViewWelcomePage"];			
					
				default:
					if( !array_key_exists("ViewChooseRegister", $this->views) ){
						require_once("travellog/views/Class.ViewChooseRegister.php");
						$this->views["ViewChooseRegister"] = new ViewChooseRegister;
					}
					return $this->views["ViewChooseRegister"];		
			}
		}
		
	 }
?>
