<?php
/**
 * <b>Main Template</b> template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
 
//SubNavigation::displayLinks();
//if (isset($isOwner) && $isOwner)
echo $obj_view->render();
echo $subNavigation->show();
echo $contents;
?>
