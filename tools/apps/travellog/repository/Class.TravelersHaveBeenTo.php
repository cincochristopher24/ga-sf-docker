<?php
class TravelersHaveBeenTo{
	
	private $total_records = 0;
	
	/***************************************************************************************
	 * edits of neri to GetFilterList():
	 * 		filtered data so as not to display certain traveler(s) to block users: 11-14-08
	 * 		modified way of setting traveler's attributes and checked if traveler 
	 * 			is suspended:													   12-17-08 
	 ***************************************************************************************/
	
	function GetFilterList($props){
		require_once('travellog/model/Class.Traveler.php');
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn = new Connection;
		$rs1  = new Recordset( $conn );
		$rs2  = clone($rs1);
		$arr  = array();
	
		$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS travelerID, username FROM ' .
						'( ' .
							'SELECT tblTraveler.travelerID, username, ranking ' .
							'FROM tblTraveler, tblTravelerRanking, SocialApplication.tbUser, SocialApplication.tbTravelWish, SocialApplication.tbTravelWishToCountry, GoAbroad_Main.tbcountry ' .
							'WHERE ' .
							'tblTraveler.travelerID                           			= tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID                			= SocialApplication.tbUser.travelerID ' .
							'AND SocialApplication.tbUser.userID                    	= SocialApplication.tbTravelWish.userID ' .
							'AND SocialApplication.tbTravelWish.travelWishID        	= SocialApplication.tbTravelWishToCountry.travelWishID ' .
							'AND SocialApplication.tbTravelWishToCountry.startdate 		< NOW() ' .
							'AND SocialApplication.tbTravelWishToCountry.startdate 		!= "0000-00-00" ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  	= GoAbroad_Main.tbcountry.countryID ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  	= %d ' .
							'AND tblTraveler.active                           			> 0 ' .	
							'AND tblTraveler.isSuspended  								= 0 ' .					
							'UNION ' .
							'SELECT travelerID, username, ranking FROM' . 
							'( ' .
								'SELECT tblTraveler.travelerID, username, ranking ' .
								'FROM ' .
								'tblTraveler, tblTravelerRanking, tblTravel, tblTrips, tblTravelLog,  GoAbroad_Main.tbcountry ' .
								'WHERE ' .
								'tblTraveler.travelerID            		= tblTravelerRanking.travelerID ' .
								'AND tblTravelerRanking.travelerID 		= tblTravel.travelerID ' .
								'AND tblTravel.travelID            		= tblTrips.travelID ' .
								'AND tblTrips.tripID            		= tblTravelLog.tripID ' .
								'AND arrival                       		< NOW() ' .
								'AND tblTrips.locID                		= %d ' .
								'AND tblTraveler.active            		> 0 ' .
								'AND tblTraveler.isSuspended   	   		= 0 ' .	
								'AND tblTravel.deleted   	   			= 0 ' .	
								'AND tblTravelLog.deleted   	   		= 0 ' .	
								'UNION ' .
								'SELECT tblTraveler.travelerID, username, ranking ' .
								'FROM ' .
								'tblTraveler, tblTravelerRanking, tblTravel, tblTrips, tblTravelLog, GoAbroad_Main.tbcity ' .
								'WHERE ' .
								'tblTraveler.travelerID             = tblTravelerRanking.travelerID ' .
								'AND tblTravelerRanking.travelerID  = tblTravel.travelerID ' .
								'AND tblTravel.travelID             = tblTrips.travelID ' .
								'AND tblTrips.tripID            	= tblTravelLog.tripID ' .
								'AND arrival                        < NOW() ' .
								'AND tblTrips.locID                 = GoAbroad_Main.tbcity.locID ' .
								'AND GoAbroad_Main.tbcity.countryID =  %d ' .
								'AND tblTraveler.active             > 0 ' .
								'AND tblTraveler.isSuspended   	    = 0 ' .	
								'AND tblTravel.deleted   	   			= 0 ' .	
								'AND tblTravelLog.deleted   	   		= 0 ' .
							') ' .
							'qryLocations ' .
						') ' .
						'qryHaveBeenTo ' .
						'WHERE 1 = 1 ' . 
						'%s ' 
						,$props['countryID']
						,$props['locationID']
						,$props['countryID']
						,$props['obj_criteria']->getCriteria2('AND')
					);

		$rs1->Execute($sql1);
		
		if( $rs1->Recordcount() ){
			
			$sql2 = 'SELECT FOUND_ROWS() AS totalrecords';
			$rs2->Execute($sql2);
			$this->total_records = $rs2->Result(0, "totalrecords");
			
			while($row = mysql_fetch_assoc($rs1->Resultset())){
				/*$obj_traveler = new Traveler;
				$obj_traveler->setTravelerID( $row['travelerID']        );
				$obj_traveler->setUsername  ( $row['username']          );	
				$obj_traveler->setOnline    ( $row['session_logged_in'] );*/
				
				$obj_traveler = new Traveler($row['travelerID']);
				
				if (!$obj_traveler->isBlocked($props['travelerID']) && !$obj_traveler->isSuspended())				
					$arr[] = $obj_traveler; 
			}	
		} 
		return $arr;
	}
	
	function GetTravelersCountryWithPastTravels(){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn         = new Connection;
		$rs1          = new Recordset( $conn );
		$arr_contents = array();
		$sql1 = sprintf
					(
						'SELECT locID, country FROM ' .
						'(' .
							'SELECT GoAbroad_Main.tbcountry.locID, country ' .
							'FROM tblTraveler, SocialApplication.tbUser, SocialApplication.tbTravelWish, SocialApplication.tbTravelWishToCountry, GoAbroad_Main.tbcountry ' .
							'WHERE ' .
							'tblTraveler.travelerID                           = SocialApplication.tbUser.travelerID ' .
							'AND SocialApplication.tbUser.userID                    = SocialApplication.tbTravelWish.userID ' .
							'AND SocialApplication.tbTravelWish.travelWishID        = SocialApplication.tbTravelWishToCountry.travelWishID ' .
							'AND SocialApplication.tbTravelWishToCountry.startdate < NOW() ' .
							'AND SocialApplication.tbTravelWishToCountry.startdate 	!= "0000-00-00" ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  = GoAbroad_Main.tbcountry.countryID ' .
							'AND tblTraveler.active                           > 0 ' .
							'AND tblTraveler.isSuspended  				  	  = 0 ' .	
							'UNION ' .
							'SELECT locID, country FROM ' .  
							'( ' .
								'SELECT GoAbroad_Main.tbcountry.locID, country ' .
								'FROM ' .
								'tblTraveler, tblTravel, tblTrips, tblTravelLog, GoAbroad_Main.tbcountry ' .
								'WHERE ' .
								'tblTraveler.travelerID            = tblTravel.travelerID ' .
								'AND tblTravel.travelID            = tblTrips.travelID ' .
								'AND tblTrips.tripID            	= tblTravelLog.tripID ' .
								'AND arrival                       < NOW() ' .
								'AND tblTrips.locID                = GoAbroad_Main.tbcountry.locID ' .
								'AND tblTraveler.active            > 0 ' .
								'AND tblTraveler.isSuspended   	   = 0 ' .	
								'AND tblTravel.deleted   	   	   = 0 ' .	
								'AND tblTravelLog.deleted          = 0 ' .
								'UNION ' .
								'SELECT GoAbroad_Main.tbcountry.locID, country ' .
								'FROM ' .
								'tblTraveler, tblTravel, tblTrips, tblTravelLog, GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry ' .
								'WHERE ' .
								'tblTraveler.travelerID             = tblTravel.travelerID ' .
								'AND tblTravel.travelID             = tblTrips.travelID ' .
								'AND tblTrips.tripID            	= tblTravelLog.tripID ' .
								'AND arrival                        < NOW() ' .
								'AND tblTrips.locID                 = GoAbroad_Main.tbcity.locID ' .
								'AND GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID ' . 
								'AND tblTraveler.active             > 0 ' .
								'AND tblTraveler.isSuspended   	   = 0 ' .	
								'AND tblTravel.deleted   	   	   = 0 ' .	
								'AND tblTravelLog.deleted   	   = 0 ' .
							') ' .
							'qryLocations ' .
						') ' .
						'qryTravelersPastTravels ' .
						'GROUP BY locID ' .
						'ORDER BY country ' 
					); 
		
		
		$rs1->Execute($sql1);
		
		if( $rs1->Recordcount() ){
			$arr_contents[] = array('locationID' => 0, 'name' => '- select a country -');
			while( $row = mysql_fetch_assoc($rs1->Resultset()) ){
				$arr_contents[] = array('locationID' => $row['locID'], 'name' => $row['country']); 
			}
		}
		return $arr_contents;
	} 
	
	function GetTravelersCountryWithPastTravelsJSON(){
		require_once('JSON.php');
		$json     = new Services_JSON(SERVICES_JSON_IN_ARR);
		$contents = $json->encode( $this->GetTravelersCountryWithPastTravels() );
		return $contents;  
	}
	
	function getTotalRecords(){
		return $this->total_records;
	}
}
?>
