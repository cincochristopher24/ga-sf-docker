<?php
class TravelerLocationCount{
	
	/* neri modified query so as not to display users who have blocked 
				the logged traveler or those who are suspended: 11-28-08 */
	
	function GetFilterList(&$props){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		require_once('travellog/model/Class.Traveler.php');
		
		$conn = new Connection;
		$rs1  = new Recordset( $conn );
		$rs2  = clone($rs1);
		$arr  = array();
			
		$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS travelerID ' .
						'FROM tblTraveler ' .
						'WHERE ' .
						'active > 0 ' .
						'AND tblTraveler.isSuspended = 0 ' .
						'AND travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
						'AND travelerID NOT IN (SELECT travelerID FROM tblBlockedUsers WHERE tblBlockedUsers.userID = '.$props['travelerID'].') ' .
						'AND currlocationID IN (%s) ' .
						'LIMIT 1'
						,$props['locationIDList']
					);
		
		$rs1->Execute($sql1);

		$sql2 = 'SELECT FOUND_ROWS() as totalrecords';
		$rs2->Execute($sql2);
		
		$props['current_location_count'] = $rs2->Result(0, 'totalrecords');
	}
}
?>
