<?php
class CountryPhotosCount{
	function GetFilterList(&$props){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn = new Connection;
		$rs1  = new Recordset( $conn );
		$rs2  = clone($rs1);
		
		$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS photoID FROM tblTraveler, tblTravel, tblTrips, tblTravelLog, tblTravelLogtoPhoto ' .
						'WHERE ' .
						'tblTraveler.travelerID 					= tblTravel.travelerID ' .
						'AND tblTravel.travelID				= tblTrips.travelID ' .
						'AND tblTraveler.isSuspended		= 0 '.
						'AND tblTraveler.active					> 0 '.
						'AND tblTrips.locID 						IN (%s) ' .
						'AND tblTrips.tripID 						= tblTravelLog.tripID ' .
						'AND tblTravelLog.travellogID 	= tblTravelLogtoPhoto.travellogID ' .
						'GROUP BY photoID ' .
						'LIMIT 1'
						,$props['locationIDList']
					);
					
		$rs1->Execute($sql1);
		
		$sql2 = 'SELECT FOUND_ROWS() as totalrecords';
		$rs2->Execute($sql2);
		
		$props['country_photo_count'] = $rs2->Result(0, 'totalrecords'); 
	}
}
?>
