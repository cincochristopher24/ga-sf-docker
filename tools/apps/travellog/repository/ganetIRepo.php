<?php
/*
 * Created on 03 20, 09
 * 
 * Author:
 * Purpose: 
 * 
 */
 
 
 interface ganetIRepo {
 	
 	function retrieve();
 	function retrieveCount();	// counterpart of retrieve function used to get only the count
 	
 	function getList();
 	function getListCount();    // counterpart of getList function used to get only the count
 	
 }
 
 
 
?>
