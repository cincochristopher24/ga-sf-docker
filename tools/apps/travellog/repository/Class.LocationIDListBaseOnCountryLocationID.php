<?php
class LocationIDListBaseOnCountryLocationID{
	function GetFilterList(&$props){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn = new Connection;
		$rs1  = new Recordset( $conn );
		
		/** for travelers who are currently in any states of US **/
		if (7758 == $props['locationID']) {
			$sql1  = sprintf
					(
						'SELECT GoAbroad_Main.tbstate.locID, GoAbroad_Main.tbcountry.countryID FROM GoAbroad_Main.tbstate, GoAbroad_Main.tbcountry ' .
						'WHERE '.
						'GoAbroad_Main.tbstate.locID > 0 ' .
						'AND GoAbroad_Main.tbcountry.countryID = 91 ' .
						'AND GoAbroad_Main.tbcountry.locID = %d ' .
						'GROUP BY locID'
						, $props['locationID']
					);	
		}
		
		else {
			$sql1  = sprintf
					(
						'SELECT GoAbroad_Main.tbcity.locID, GoAbroad_Main.tbcountry.countryID FROM GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry ' .
						'WHERE ' .
						'GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID AND GoAbroad_Main.tbcountry.locID= %d ' .
						'AND GoAbroad_Main.tbcity.locID > 0 ' .
						'UNION '.
						'SELECT tblNewCity.locID, GoAbroad_Main.tbcountry.countryID FROM tblNewCity, GoAbroad_Main.tbcountry ' .
						'WHERE ' .
						'tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID AND GoAbroad_Main.tbcountry.locID= %d '.
						'AND tblNewCity.locID > 0 ' .
						'GROUP BY locID'
						,$props['locationID']
						,$props['locationID']
					);	
		}
					
		$rs1->Execute($sql1);
		
		if( $rs1->Recordcount() ){
			$arr_locationIDs = array();
			$arr_locationIDs[] = $props['locationID'];
			while( $row = mysql_fetch_assoc($rs1->Resultset()) ){
				$arr_locationIDs[]  = $row['locID'];
				$props['countryID'] = $row['countryID'];	
			}
			
			$props['locationIDList'] = implode(',', $arr_locationIDs);
		}
		else{
			$props['locationIDList'] = $props['locationID'];
			$props['countryID']      = 0;
		} 
	}
}
?>
