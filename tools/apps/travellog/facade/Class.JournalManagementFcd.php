<?php
class JournalManagementFcd{
	
	private 
	
	$show_admin_controls = true,
	
	$pos                 = 0,
	
	$vo                  = array();
	
	function retrieve( $travelID ){
		if( !array_key_exists('obj_journal', $this->vo) ){
			require_once("travellog/model/Class.JournalModel.php");
			$obj_journal = new JournalModel;
			$this->vo['obj_journal'] = $obj_journal->Get( $travelID );
		}
		
		return $this->vo['obj_journal'];
	} 
	
	function retrieveView( $travelID, $travellogID=0 ){
		require_once("travellog/views/Class.JournalView.php");
		$obj_views                           = new JournalView;
		$arr_contents['obj_journal']         = $this->retrieve( $travelID );
		$arr_contents['show_admin_controls'] = $this->show_admin_controls;
		if($travellogID > 0){
			$arr_contents['obj_entry']   = $this->retrieveEntry( $travellogID );
			$arr_contents['bookmark_controls'] = $this->retrieveBookmarkControls( $arr_contents['obj_entry'] );
			$arr_contents['fb_like_control'] = $this->retrieveFBLikeControl($arr_contents['obj_entry']);
		}	
		$obj_views->setContents( $arr_contents );
		
		return $obj_views;
	}
	
	function retrieveEntry( $travellogID ){
		if( !array_key_exists('obj_entry', $this->vo) ){
			require_once("travellog/model/Class.EntryModel.php");
			$obj_model             = new EntryModel;
			$this->vo['obj_entry'] = $obj_model->Get( $travellogID );
		}
		return $this->vo['obj_entry']; 
	}
	
	function retrieveEntryByContextView( $arr_filter, $obj_journal ){
		require_once("travellog/views/Class.EntryViewFactory.php");
		$arr_contents['obj_entry']   = $this->retrieveEntry( $arr_filter['travellogID'] );
		$arr_contents['obj_journal'] = $obj_journal;
		$arr_contents['bookmark_controls'] = $this->retrieveBookmarkControls( $arr_contents['obj_entry'] );
		$arr_contents['fb_like_control'] = $this->retrieveFBLikeControl($arr_contents['obj_entry']);
		$arr_contents['g_plusone_control'] = $this->retrieveGPlusOneControl($arr_contents['obj_entry']);
		if( $arr_filter['view_type'] == 2 ) $arr_contents['next_prev'] = $this->getNextPrev( $arr_filter );
		$arr_contents['show_admin_controls'] = $this->show_admin_controls;
		$factory                             = EntryViewFactory::instance();
		$obj_views                           = $factory->create( $arr_filter['view_type'] );
		
		$obj_views->setContents( $arr_contents );  
		
		return $obj_views;  
	}
	
	function retrieveEntryByFilterView( $arr_filter ){
		require_once("travellog/views/Class.EntryView.php");
		require_once("travellog/views/Class.EntryViewType.php");
		
		$obj_views    = new EntryView; 
		$travellogID  = $arr_filter['travellogID'];
		//$group_id     = $arr_filter['group_id'];
		 
		$arr_contents['obj_journal']   = $this->retrieve( $arr_filter['travelID'] );
		$arr_contents['obj_entry']     = $this->retrieveEntry( $travellogID );
		
		$arr_contents['comments_view'] = $this->retrieveCommentsByFilterView( $arr_filter );
		
		$arr_filter['view_type']       = EntryViewType::$EntryDescription;
		 	 
		$obj_views->addSubView( $this->retrieveEntryByContextView( $arr_filter, $arr_contents['obj_journal'] )                           , 'desc_view'                );
		
		$arr_filter['view_type']       = EntryViewType::$EntryTitleCalloutPhoto;
		$obj_views->addSubView( $this->retrieveEntryByContextView( $arr_filter, $arr_contents['obj_journal'] )                           , 'title_callout_photo_view' );
		 
		//$obj_views->addSubView( $this->retrieveProfileView( $arr_contents['obj_journal']->getTravelerID(), $arr_contents['obj_journal'] ), 'profile_view'             );
		if( isset( $arr_filter['group_id'] ) ){
			$obj_views->addSubView( $this->retrieveGroupProfileView( $arr_filter['group_id'], $arr_contents['obj_journal'] ), 'profile_view'             );
		}
		else
			$obj_views->addSubView( $this->retrieveTravelerProfileView( $arr_contents['obj_journal']->getTravelerID(), $arr_contents['obj_journal'] ), 'profile_view'             );
		
		// $obj_views->addSubView( $this->retrieveMapByFilterView( $arr_filter )                              , 'map_view'                 );
		$obj_views->addSubView( $this->retrieveHighlightsByFilterView( $travellogID )                      , 'highlights_view'          );
		$obj_views->addSubView( $this->retrieveTipsByFilterView( $travellogID )                            , 'tips_view'                );
		$obj_views->addSubView( $this->retrieveEntryTableOfContentsView($travellogID, $arr_contents)                    , 'toc_view'                 );
		$obj_views->addSubView( $this->retrieveEntryPrimaryPhotoView( $travellogID, $arr_filter )                       , 'entry_primary_photo_view' );
		//$obj_views->addSubView( $this->retrieveEntrySubNavigationView( $arr_contents['obj_journal']->getTravelerID() ), 'entry_sub_navigation_view' ); 			// modified by neri: 12-11-08
		$obj_views->addSubView( $this->retrieveEntryPhotoThumbnails($arr_filter)							, 'thumbnail_view');
		$obj_views->addSubView( $this->retrieveEntrySubNavigationView( $arr_contents['obj_journal']->getOwner() ), 'entry_sub_navigation_view' );
		
		$obj_views->addSubView( $this->retrieveView( $arr_filter['travelID'], $arr_filter['travellogID'] )                             , 'journal_view'             );
		$obj_views->addSubView( $this->retrieveEntryAds()                                                  , 'ads_view'                 );
		
		$obj_views->addSubView( $this->retrieveEntryNoRecordsView( $travellogID )                          , 'no_records_view'          );
		// $obj_views->addSubView( $this->retrieveBookmarkControls( $arr_contents['obj_entry'] )              , 'bookmark_controls'        );
		// added by neri for GANET video functionality: 10-14-08
		$obj_views->addSubView( $this->retrieveEntryViewVideos ( $travellogID )                       	   , 'entry_view_videos' 		);
		
		$obj_session = SessionManager::getInstance();
		
		$photo_request = array();
		$photo_request['action'] = "onloadgallery";
		$photo_request['layout'] = $arr_contents['obj_entry']->getTemplateType()?1:4;
		$photo_request['context'] = "travellog";
		$photo_request['loginid'] = $obj_session->get('travelerID');
		$photo_request['genid'] = $travellogID;
		$photo_request['startrow'] = 1;
		$photo_request['pagenum'] = 1;
		
		$arr_contents['show_admin_controls'] = $this->show_admin_controls;
		if( isset($arr_filter['group_id']) ) $arr_contents['group_id'] = $arr_filter['group_id'];
		$arr_contents['traveler_id'] = $arr_filter['travelerID'];  
		$arr_contents['fail'] = (isset($arr_filter['fail'])) ? $arr_filter['fail'] : null;				//added by neri: 11-26-08
		
		$obj_views->addSubView( $this->retrieveGalleryView($photo_request)                          , 'gallery'          );
		$obj_views->setContents( $arr_contents );
		
		return $obj_views;
	}
	
	/**
	 * method for retrieving view for GANET video
	 * added by neri
	 * 10-14-08
	 */
	 
	function retrieveEntryViewVideos ( $travellogID ) {
		require_once('travellog/views/Class.EntryViewVideos.php');
		
		$obj_view 								= new EntryPrimaryVideoView;
		$arr_contents							= array();
		$arr_contents['show_admin_controls']	= $this->show_admin_controls;
		$arr_contents['obj_entry']				= $this->retrieveEntry( $travellogID );
		$obj_view->setContents( $arr_contents );
		return $obj_view;
	}

	function retrieveEntrySubNavigationView( $owner ){
		require_once("travellog/views/Class.EntrySubNavigationView.php");
		require_once("travellog/model/Class.SubNavigation.php");
		
		$obj_sub_navigation = new SubNavigation;
		$obj_view           = new EntrySubNavigationView;
		$arr_contents       = array();
		
		if ($owner instanceof Traveler) {
			$obj_sub_navigation->setContext('TRAVELER');
			$obj_sub_navigation->setContextID($owner->getTravelerID());
		}
		
		else {
			$obj_sub_navigation->setContext('GROUP');
			$obj_sub_navigation->setContextID($owner->getGroupID());
		}
		
		$obj_sub_navigation->setLinkToHighlight('MY_JOURNALS');
		$arr_contents['obj_sub_navigation'] = $obj_sub_navigation;
		$obj_view->setContents($arr_contents);
		return $obj_view; 
	}
	
	function retrieveEntryTableOfContentsView($travellogID, $params){
		require_once("travellog/model/Class.EntryTableOfContents.php");
		require_once("travellog/views/Class.EntryTableOfContentsView.php");
		$obj_entry = $this->retrieveEntry( $travellogID );
		$this->retrieve( $obj_entry->getTravelID() );
		$arr_filter['travellogID']           = $travellogID;
		$arr_filter['travelID']              = $obj_entry->getTravelID();
		$obj_model                           = new EntryTableOfContents;
		$obj_views                           = new EntryTableOfContentsView;
		$arr_contents['col_entry']           = $this->getEntries( $arr_filter );
		$arr_contents['current_pos']         = $this->pos;
		$obj_model->setEntryCollections( $arr_contents['col_entry']   );
		$obj_model->setCurrentPosition ( $arr_contents['current_pos'] );
		$arr_contents                        = $obj_model->getTableOfContents();
		$arr_contents['obj_entry']           = $obj_entry;
		$arr_contents['travelID']            = $arr_filter['travelID'];
		$arr_contents['show_admin_controls'] = $this->show_admin_controls;
		$arr_contents['journal']             = $params['obj_journal'];
		$obj_views->setContents( $arr_contents );
		
		return $obj_views;
	}
	
	function retrieveEntryPrimaryPhotoView( $travellogID, $arr_filter ){
		require_once("travellog/views/Class.EntryPrimaryPhotoView.php");
		$obj_view                            = new EntryPrimaryPhotoView;
		$arr_contents                        = array();   
		$arr_contents['show_admin_controls'] = $this->show_admin_controls; 
		$arr_contents['obj_entry']           = $this->retrieveEntry( $travellogID );
		if( isset($arr_filter['group_id']) )
			$arr_contents['group_id']        = $arr_filter['group_id'];
		$arr_contents['traveler_id']         = $arr_filter['travelerID'];
		$obj_view->setContents( $arr_contents );
		
		return $obj_view;  
	}
	
	function retrieveGalleryView($request){
		require_once("travellog/views/Class.JEntryPhotoGalleryView.php");
		$obj_view                            = new JEntryPhotoGalleryView;
		$obj_view->setContents($request);
		return $obj_view;  
	} 
	
	function retrieveProfile( $travelerID ){
		require_once("travellog/model/Class.ProfileModel.php");
		$obj_model    = new ProfileModel;
		
		return $obj_model->Get( $travelerID );
	}
	
	function retrieveTravelerProfileView( $travelerID, $obj_journal ){
		$file_factory = FileFactory::getInstance();
		$factory      = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
		$profile_comp = $factory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
		$profile_comp->init($travelerID);

		$obj_views    = $profile_comp->get_view();
		
		return $obj_views;
	}
	
	function retrieveGroupProfileView( $groupID, $obj_journal ){
		$file_factory = FileFactory::getInstance();
		$factory      = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
		$profile_comp = $factory->create( ProfileCompFactory::$GROUP_CONTEXT );
		$profile_comp->init($groupID);

		$obj_views    = $profile_comp->get_view();
		
		return $obj_views;
	}
	
	function createHighlights( $arr_vars ){
		require_once("travellog/model/Class.EntryHighlights.php");
		$obj_model = new EntryHighlights;
		
		$obj_model->Save( $arr_vars );
	}
	
	function retrieveHighlight( $highlightID ){
		require_once("travellog/model/Class.EntryHighlights.php");
		$obj_model = new EntryHighlights;

		return $obj_model->Get( $highlightID );
	} 
	
	function retrieveJSONHighlight( $highlightID ){
		require_once("travellog/model/Class.EntryHighlights.php");
		$obj_model = new EntryHighlights;

		return $obj_model->GetJSON( $highlightID );
	} 
	
	function retrieveHighlightsByFilter( $travellogID ){
		require_once("travellog/model/Class.EntryHighlights.php");
		$obj_model = new EntryHighlights;
		
		return $obj_model->GetList( $travellogID );
	}
	
	function retrieveJSONHighlightsByFilter( $travellogID ){
		require_once("travellog/model/Class.EntryHighlights.php");
		$obj_model = new EntryHighlights;
		
		return $obj_model->GetListJSON( $travellogID );
	}
	
	function retrieveHighlightsByFilterView( $travellogID ){
		require_once("travellog/views/Class.EntryHighlightsView.php");
		$obj_views                           = new EntryHighlightsView;
		$arr_contents['col_highlights']      = $this->retrieveHighlightsByFilter( $travellogID );
		$arr_contents['show_admin_controls'] = $this->show_admin_controls;
		$arr_contents['travellogID']         = $travellogID;
		$obj_views->setContents( $arr_contents );
		
		return $obj_views;
	}
	
	function updateHighlights( $arr_vars ){
		require_once("travellog/model/Class.EntryHighlights.php");
		$obj_model = new EntryHighlights;
		
		$obj_model->Update( $arr_vars );
	}
	
	function deleteHighlights( $arr_vars ){
		require_once("travellog/model/Class.EntryHighlights.php");
		$obj_model = new EntryHighlights;
		
		$obj_model->Delete( $arr_vars );
	}
	
	
	function createTips( $arr_vars ){
		require_once("travellog/model/Class.EntryTips.php");
		$obj_model = new EntryTips;
		
		$obj_model->Save( $arr_vars );
	}
		
	function retrieveTips( $travellogID ){
		require_once("travellog/model/Class.EntryTips.php");
		$obj_model = new EntryTips;
		
		return $obj_model->GetList( $travellogID );
	}
	
	function retrieveJSONTips( $tipID ){
		require_once("travellog/model/Class.EntryTips.php");
		$obj_model = new EntryTips;

		return $obj_model->GetJSON( $tipID );
	} 
	
	function retrieveTipsByFilterView( $travellogID ){
		require_once("travellog/views/Class.EntryTipsView.php");
		$obj_views                           = new EntryTipsView;
		$arr_contents['col_tips']            = $this->retrieveTips( $travellogID );
		$arr_contents['show_admin_controls'] = $this->show_admin_controls;
		$arr_contents['travellogID']         = $travellogID;
		$obj_views->setContents( $arr_contents );
		
		return $obj_views;
	}
	
	function retrieveJSONTipsByFilter( $travellogID ){
		require_once("travellog/model/Class.EntryTips.php");
		$obj_model = new EntryTips;
		
		return $obj_model->GetListJSON( $travellogID );
	}
	
	function updateTips( $arr_vars ){
		require_once("travellog/model/Class.EntryTips.php");
		$obj_model = new EntryTips;
		
		$obj_model->Update( $arr_vars );
	}
	
	function deleteTips( $arr_vars ){
		require_once("travellog/model/Class.EntryTips.php");
		$obj_model = new EntryTips;
		
		$obj_model->Delete( $arr_vars );
	}
	
	function retrieveMapByFilter( $arr_filter ){
		require_once("travellog/model/Class.MapModel.php");
		$obj_model = new MapModel;
		
		return $obj_model->Get( $arr_filter );
	}
	
	function retrieveMapByFilterView( $arr_filter ){
		require_once("travellog/views/Class.MapView.php");
		$obj_views                   = new MapView;
		$obj_journal                 = $this->retrieve( $arr_filter['travelID'] );
		$arr_filter['arr_trips']     = $obj_journal->getTrips();
		$arr_contents['arr_marker']  = $this->retrieveMapByFilter( $arr_filter );
		$arr_contents['travellogID'] = $arr_filter['travellogID'];
		$obj_views->setContents( $arr_contents );
		
		return $obj_views;
	}
	
	function retrieveCommentsByFilterView( $arr_filter ){
		$file_factory = FileFactory::getInstance();
		$obj_comments = $file_factory->getClass('CommentsView'); 
		$obj_comments->setEnvURL( $_SERVER['REQUEST_URI']."^comment_form" );
		$obj_comments->setTravelerID( $arr_filter['travelerID'] );
		$obj_comments->setOwnerID( $arr_filter['ownerID'] );
		$obj_comments->setContext(3, $arr_filter['travellogID']);
		$obj_comments->readComments();
				
		return $obj_comments;   
	}
	
	function retrieveEntryAds(){
		require_once("travellog/facade/Class.AdsFcd.php");
		$ads_fcd   = new AdsFcd;
		$obj_views = $ads_fcd->retrieveEntryAdsView();
		
		return $obj_views;
	}
	
	
	function permalink( $arr_filter ){
		require_once("travellog/model/Class.EntryModel.php");
		$obj_model = new EntryModel;

		return $obj_model->permalink( $arr_filter );
	}
		
	function showAdminControls( $arr_vars ){
		
		require_once("travellog/model/admin/Class.AdminUserAccount.php");
		
		$obj_session               = SessionManager::getInstance();
		$obj_journal               = $this->retrieve( $arr_vars['travelID'] );
		$this->show_admin_controls = ( $arr_vars['travelerID'] == $obj_journal->getTravelerID() || ($obj_session->isSiteAdmin() && AdminUserAccount::ACCESS_ADMIN == $obj_session->get('userAccess') ) )? true: false; 	
		return $this->show_admin_controls;
	} 
	
	function getEntries( $arr_filter ){
		if( !array_key_exists('arr_entries', $this->vo) ){
			require_once("Class.Criteria2.php"); 
			require_once('Class.HtmlHelpers.php');
			require_once("travellog/vo/Class.EntryVO.php");
			$obj_session = SessionManager::getInstance();
			$arr_entry   = array();
			$ctr         = 0;
			$obj_journal = $this->retrieve( $arr_filter['travelID'] ); 
			$arr_trips   = $obj_journal->getTrips();
			
			if( count($arr_trips) ){
				$obj_criteria = new Criteria2;
				$obj_criteria->setOrderBy("travellogID"); 
				//if( $arr_filter['ownerID'] != $arr_filter['travelerID'] ){
				if( $obj_session->get('travelerID') != $arr_filter['travelerID'] ){
					//$obj_criteria->mustBeEqual('tblTravelLog.publish', 1); 
				}
				foreach( $arr_trips as $obj_trip ){
					$arr_temp_entry = $obj_trip->getTravelLogs($obj_criteria);
					foreach( $arr_temp_entry as $obj_temp_entry ){
						if($obj_temp_entry instanceOf TravelLog){
							$obj_new_member = new EntryVO;
							$obj_new_member->setTravelLogID ( $obj_temp_entry->getTravelLogID()                                       );						
							$obj_new_member->setTitle       ( $obj_temp_entry->getTitle()                                             );
							$obj_new_member->setDescription ( HtmlHelpers::truncateWord($obj_temp_entry->getDescription(), 25)        );
							$obj_new_member->setCallout     ( $obj_temp_entry->getCallOut()                                           );
							$obj_new_member->setTravelID    ( $obj_temp_entry->getTravelID()                                          );
							$obj_new_member->setUrlAlias    ( $obj_temp_entry->getUrlAlias()                                          );
							try{
								$location = $obj_temp_entry->getTrip()->getLocation();
								if(is_object($location)){
									$obj_new_member->setCountry     ( $location->getCountry()->getName()      );
									$obj_new_member->setCountryID   ( $location->getCountry()->getCountryID() );
									$obj_new_member->setCity        ( $location->getName()                    );
								}
								$obj_new_member->setArrival     ( date('M-d-y', strtotime($obj_temp_entry->getTrip()->getArrival()))      ); 
							}catch(exception $e){
							}
							$obj_new_member->setUsername    ( $obj_temp_entry->getTraveler()->getUsername()                           );
							$obj_new_member->setTemplateType( $obj_temp_entry->getTemplateType()                                      );
							$obj_new_member->setIsAdvisor   ( $obj_temp_entry->getTraveler()->isAdvisor()                             );
							$obj_new_member->setCountPhotos ( $obj_temp_entry->countPhotos()                                          );
			
							/********************************************
							 * additional attributes for obj_new_member
							 * added by neri
							 * 10-14-08
							 ********************************************/
						
							$obj_new_member->setPrimaryVideo( $obj_temp_entry->getPrimaryVideo()									  );
							$obj_new_member->setVideos 		( $obj_temp_entry->getTravelLogVideos()									  );		 
							$obj_new_member->setCountVideos ( count($obj_temp_entry->getTravelLogVideos())							  );
						
							/********************************************/
			
							if( $obj_temp_entry->getTraveler()->isAdvisor() ){
								$obj_group = $obj_temp_entry->getOwner();
								if($obj_group instanceof Group){
									if( $obj_group->getParentID() ){
										$obj_new_member->setSubGroupName( $obj_group->getName()              );		
										$obj_new_member->setGroupName   ( $obj_group->getParent()->getName() );
									}else
										$obj_new_member->setGroupName   ( $obj_group->getName()              );				
								}
							}
						
							$arr_entry[] = $obj_new_member;
						
							/**
							 * @todo Must be change to entry title instead of entry id.
							 */
							if( $arr_filter['travellogID'] == $obj_temp_entry->getTravelLogID() ) $this->pos = $ctr;
						  
							$ctr++;
						}	
					}		
				}			
			}
			$this->vo['arr_entries'] = $arr_entry;
		}
		
		return $this->vo['arr_entries'];
	}
	
	private function getNextPrev( $arr_filter ){
		$arr_entries  = $this->getEntries( $arr_filter );
		$arr_contents = array();
		$slen         = count($arr_entries)-1;
		if( $this->pos ) $arr_contents['prev'] = $arr_entries[$this->pos-1];
		else $arr_contents['prev'] = NULL;
		
		if( $this->pos < $slen ) $arr_contents['next'] = $arr_entries[$this->pos+1];
		else $arr_contents['next'] = NULL;
		
		return $arr_contents;  
	}

	function getCurrentPosition(){
		return $this->pos;
	}
	
	private function retrieveEntryNoRecordsView( $travellogID ){
		require_once("travellog/views/Class.EntryNoRecordsView.php");
		$obj_views = new EntryNoRecordsView;
		$obj_views->setContents( $travellogID );

		return $obj_views; 
	}
	
	private function retrieveEntryPhotoThumbnails($arr_filter){
		require_once("travellog/UIComponent/NewCollection/views/Class.ViewThumbGrid.php");
		$request = array(	"loginid"	=>	$arr_filter["travelerID"],
							"genid"		=>	$arr_filter["travellogID"],
							"ownerid"	=>	$arr_filter["ownerID"],
							"travelerid"=>	$arr_filter["travelerID"]	);
		if( isset($arr_filter["group_id"]) ){
			$request["group_id"] = $arr_filter["group_id"];
		}
		return new ViewThumbGrid($request);
		
	}
	
	// bookmarks
	function retrieveBookmarkControls($entry){
		require_once("travellog/views/Class.BookmarkControlsView.php");
		$obj_view = new BookmarkControlsView();
		
		$permalink 	= "http://".$_SERVER['SERVER_NAME']."/".$entry->getFriendlyURL();
		$title 		= $entry->getTitle();
		
		$arr_contents = array(
			'permalink' => $permalink,
			'title'		=> $title
		);
		
		$obj_view->setContents($arr_contents);
		$obj_view->prepareURLParameters();
		return $obj_view;
	}
	
	//FB like button
	function retrieveFBLikeControl($entry){
		
		require_once('Class.Template.php');
		$siteContext = SiteContext::getInstance();
		$site_name = isset($GLOBALS["CONFIG"]) ? $siteContext->getSiteName()." Network" : "GoAbroad Network";
		$primaryPhoto = $entry->getPrimaryPhotoObj();
		
		$dotnetlogo = 'http://www.goabroad.net/images/footerdotnetlogo.gif';
		$primaryPhotoLink = (($entry->getPrimaryPhotoID() > 0) && $primaryPhoto instanceof Photo) ? $primaryPhoto->getPhotoLink("featured") : $dotnetlogo;
		$primaryPhotoLink = ($primaryPhotoLink == $dotnetlogo && $siteContext->isInCobrand()) ? '' : $primaryPhotoLink;

		$fbMetaTags = array(	'<meta name="title" content="'.$entry->getTitle().'"/>',			
								'<meta property="og:title" content="'.$entry->getTitle().'"/>',
								'<meta property="og:site_name" content="'.$site_name.'"/>',
								'<meta property="og:image" content="'.$primaryPhotoLink.'"/>',
								'<link rel="image_src" href="'.$primaryPhotoLink.'" />');

		Template::clearFBRequiredMetaTags();
		Template::hideProfileFBLike();
		Template::setFBRequiredMetaTags($fbMetaTags);
		
		require_once("travellog/views/Class.FBLikeControlView.php");
		$obj_view = new FBLikeControlView();
		
		$permalink 	= "http://".$_SERVER['SERVER_NAME']."/".$entry->getFriendlyURL();
		$title 		= $entry->getTitle();
		
		$arr_contents = array(
			'permalink' => $permalink,
			'title'		=> $title,
			'name'		=> 'entry'
		);
		
		$obj_view->setContents($arr_contents);
		return $obj_view;
	}
	
	//Google Plus One button
	function retrieveGPlusOneControl($entry){
		require_once("travellog/views/Class.GPlusOneControlView.php");
		$obj_view = new GPlusOneControlView();
		
		$obj_view->setContents(null);
		return $obj_view;
	}
	
}
?>