<?php
	/*
	 * Class.MessagesFcd.php
	 * Created on Nov 12, 2007
	 * created by marc
	 */
	 
	 require_once("Class.Constants.php");
	 
	 class MessagesFcd{
	 	
	 	private
	
		$props          = array(),
		        
		$view_factory   = array(),
		
		$models_factory = array();
		
		function __construct( $props ){
			require_once('travellog/views/Class.MessagesViewsFactory.php');
			$this->view_factory  = MessagesViewsFactory::getInstance();
			$this->props         = $props;
		}
		
		function retrieveMessageList(){
			$obj_view = $this->view_factory->createView();
			$obj_view->setPath('travellog/views/');
			$obj_view->addSubView( $this->retrieveMessageNavigation(),	'MESSAGE_NAVIGATION');
			$obj_view->addSubView( $this->retrieveMessagePaging(),	'MESSAGE_PAGING');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
		function retrieveMessage(){
			$obj_view = $this->view_factory->createView(constants::VIEW_MESSAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->addSubView( $this->retrieveMessageNavigation(),	'MESSAGE_NAVIGATION');
			$obj_view->addSubView( $this->retrieveMessageActions(),	'MESSAGE_ACTIONS');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveMessageNavigation(){
			$obj_view = $this->view_factory->createView(constants::MESSAGE_NAVIGATION);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveMessagePaging(){
			$obj_view = $this->view_factory->createView(constants::MESSAGE_PAGING);
			$obj_view->setPath('travellog/views/travelers/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveMessageForm(){
			$obj_view = $this->view_factory->createView(constants::MESSAGE_FORM);
			$obj_view->setPath('travellog/views/');
			$obj_view->addSubView( $this->retrieveMessageNavigation(),	'MESSAGE_NAVIGATION');
			$obj_view->addSubView( $this->retrieveMessageErrors(),	'MESSAGE_ERRORS');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveMessageErrors(){
			$obj_view = $this->view_factory->createView(constants::MESSAGE_ERRORS);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveMessageConfirmation(){
			$obj_view = $this->view_factory->createView(constants::MESSAGE_CONFIRMATION);
			$obj_view->setPath('travellog/views/');
			$obj_view->addSubView( $this->retrieveMessageNavigation(),	'MESSAGE_NAVIGATION');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveMessageActions(){
			$obj_view = $this->view_factory->createView(constants::MESSAGE_ACTIONS);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveSpamList($includePaging=true){
			$obj_view = $this->view_factory->createView(constants::SPAM_LIST);
			$obj_view->setPath('travellog/views/admin/');
			if($includePaging)
				$obj_view->addSubView( $this->retrieveMessagePaging(),	'MESSAGE_PAGING');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
	 }
?>