<?php
require_once("travellog/route/model/Class.gaIAction.php");
class SaveTravelSchedule implements gaIAction{
	function performAction( &$props ){
		require_once('travellog/model/Class.TravelItem.php');
		require_once('travellog/model/Class.TravelDestination.php');
		require_once('travellog/model/Class.TravelSchedule.php');
		require_once('travellog/model/Class.Travel.php');
		
		$obj_destination = new TravelDestination;
		$obj_item        = new TravelItem;
		$obj_travel      = new Travel( $props['travelID'] );
		$props['userID'] = TravelSchedule::getUserIDWithTravelerID( $props['travelerID'] );
		
		$obj_destination->setCountryID ( $props['countryID']           );
		$obj_destination->setStartDate ( $props['arrivaldate']         );
		
		$obj_item->setUserID           ( $props['userID']              );
		$obj_item->setTitle            ( $obj_travel->getTitle()       );
		$obj_item->setDetails          ( $obj_travel->getDescription() );
		$obj_item->setDestinations     ( array( $obj_destination     ) );
		
		$obj_item->save();
			
		$props['travelWishID'] = $obj_item->getTravelWishID(); 
		
		TravelSchedule::saveTraveltoTravelWish( $props );
		
	}
}
?>
