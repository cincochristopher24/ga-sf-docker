<?php
	/**
	 * @(#) Class.JournalRules.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 27, 2009
	 */
	
	require_once('travellog/model/Class.SessionManager.php');
	
	/**
	 * Rules of the journals
	 */
	class JournalRules {
		
		const GROUP = 1;
		const TRAVELER = 2;
		
		/**
		 * Rules for saving a new journal.
		 * 
		 * @param array $data The data where the validity of the journal will be based.
		 * 
		 * @return boolean true when the data were valid; false otherwise.
		 */
		static function validateSavingNewJournal(array &$data){
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['title'] = $_POST['title'];
			$data['description'] = $_POST['description'];
			$errors = array();		
			
			if ("" == trim($_POST['title'])) {
				$errors[] = "Title is a required field!";
			}
			
			if ("" == trim($_POST['description'])) {
				$errors[] = "Description is a required field!";
			}
			
			$data['errors'] = $errors;
			
			if (0 < $data['travelerID'] AND isset($_GET['gID']) AND 0 < $_GET['gID']) {
				require_once("travellog/model/Class.GroupPeer.php");
				$params['group'] = GroupPeer::retrieveByPk($_GET['gID']);
				if (!is_null($params['group']) AND ($params['group']->getAdministratorID() == $params['travelerID'] OR $params['group']->isStaff($params['travelerID']))) {
					$data['refType'] = 2;
					$data['refID'] = $_GET['gID'];
					$data['gID'] = $_GET['gID'];
					$data['travel_travelerID'] = $params['group']->getAdministratorID();
					return true;
				}
			}
			else if (0 < $data['travelerID']) {
				$data['refType'] = 1;
				$data['refID'] = $data['travelerID'];
				$data['travel_travelerID'] = $data['travelerID'];
				return true;
			}
			
			return false;
		}
		
		/**
		 * Rules for viewing journal add form.
		 * 
		 * @param array $data The data where the validity of the journal will be based.
		 * 
		 * @return boolean true when the data were valid; false otherwise.
		 */
		static function validateViewAddForm(array &$data){
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['title'] = isset($_POST['title']) ? $_POST['title'] : "";
			$data['description'] = isset($_POST['description']) ? $_POST['description'] : "";
			
			if (0 < $data['travelerID'] AND isset($_GET['gID']) AND 0 < $_GET['gID']) {
				require_once("travellog/model/Class.GroupPeer.php");
				$data['group'] = GroupPeer::retrieveByPk($_GET['gID']);
				if (!is_null($data['group']) AND ($data['group']->getAdministratorID() == $data['travelerID'] OR $data['group']->isStaff($data['travelerID']))) {
					$data['context'] = self::GROUP;
					$data['gID'] = $_GET['gID'];					
					return true;
				}
			}
			else if (0 < $data['travelerID']) {
				$data['context'] = self::TRAVELER;
				return true;
			}
			
			return false;
		}
	}
	
