<?php
	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/model/Class.SiteContext.php');
	/***
	require_once('travellog/model/Class.Calendar.php');
	require_once('travellog/model/Class.AdminGroup.php');
	***/
	require_once('travellog/model/Class.SubNavigationLink.php');
	require_once('Class.ToolMan.php');
	
	abstract class AbstractTravelerSubNavigation
	{
		protected $mClasses = array(
			'TRAVELER' => array(
				'className' => 'Traveler',
				'file' 		=> 'travellog/model/Class.Traveler.php'
			),
			'CALENDAR' => array(
				'className' => 'Calendar',
				'file' 		=> 'travellog/model/Class.Calendar.php'
			),
			'ADMIN_GROUP' => array(
				'className' => 'AdminGroup',
				'file'		=> 'travellog/model/Class.AdminGroup.php'
			),
			'JOURNAL_PRIVACY_PREF' => array(
				'className' => 'JournalPrivacyPreference',
				'file'		=> 'travellog/model/Class.JournalPrivacyPreference.php'
			)
		);
		
		protected $mContext = null;
		protected $mLinkToHighlight = null;
		protected $mContextID = null;
		protected $mHiddenLinks = array();
		protected $mCustomLinks = array();
		protected $mAlteredLinks = array();
		protected $mAlteredUrls	= array();
		
		protected $mLoggedTraveler = null;
		protected $mViewedTraveler = null;
		protected $mIsOwn = false;
		protected $mLinkLabels = array();
		
		public function __construct($vars=array()){
			$this->mContext 		= $vars['context'];
			$this->mLinkToHighlight = $vars['linkToHighlight'];
			$this->mContextID 		= $vars['contextID'];		
			$this->mHiddenLinks		= $vars['hiddenLinks'];
			$this->mCustomLinks 	= $vars['customLinks'];
			$this->mAlteredLinks	= $vars['alteredLinks'];
			$this->mAlteredUrls		= $vars['alteredUrls'];
			
			$session  = SessionManager::getInstance();
			$travelerID = $session->get('travelerID');
			
			require_once($this->mClasses['TRAVELER']['file']);
			
			if($travelerID != 0){
				$loggedTravelerID = $_SESSION['travelerID'];
		
				/***
				if(array_key_exists('CONFIG',$GLOBALS)){
					require_once('travellog/custom/COBRAND/model/Class.TravelerCB.php');
					$loggedTraveler = new TravelerCB($loggedTravelerID);	
				}
				else{
					$loggedTraveler = new Traveler($loggedTravelerID);	
				}
				***/
				$loggedTraveler = new $this->mClasses['TRAVELER']['className']($loggedTravelerID);
			}
			else{
				$loggedTravelerID = -1;
				$loggedTraveler = new $this->mClasses['TRAVELER']['className'](0);
			}
			
			$this->mLoggedTraveler = $loggedTraveler;
			
			/*****************************************
			 * tab labels modified by neri: 11-05-08
			 *****************************************/
			
			//check if the logged traveler is viewing his/her own	
			if($loggedTravelerID == $this->mContextID){//if the 
				$this->mIsOwn = true;
				$this->mViewedTraveler = $loggedTraveler;
				$this->mLinkLabels = array(
					/*'MY_PASSPORT' 			=> 'My Passport',
					'MY_PROFILE'			=> 'My Profile',
					'MY_JOURNALS'			=> 'My Journals',
					'MY_FRIENDS'			=> 'My Friends',
					'MY_GROUPS'				=> 'My Groups',
					'MESSAGES'				=> 'Messages',
					'COMMENTS'				=> 'Comments',
					'ADDRESS_BOOK'			=> 'Addressbook',
					'BULLETIN'				=> 'Bulletin',
					'CALENDAR'				=> 'Calendar',
					'ONLINE_ADVISING'		=> 'Online Advising',
					'RESUME'				=> 'Resume',
					'PARTICIPANT_INQUIRIES'	=> 'Participant Inqueries'*/
					
					'MY_DASHBOARD'			=> 'Dashboard',
					'MY_PROFILE'			=> 'Profile',
					'MY_FRIENDS'			=> 'Friends',
					'MY_GROUPS'				=> 'Groups',
					'MY_JOURNALS'			=> 'Journals',
					'MY_PHOTOS'				=> 'Photos',
					'MY_VIDEOS'				=> 'Videos',
					'MESSAGES'				=> 'Message Center',
					'BULLETIN'				=> 'Bulletin',
					'ADDRESS_BOOK'			=> 'Addressbook',
					'CALENDAR'				=> 'Calendar',
					'WIDGETS'				=> 'Apps'
					
				);
			}
			else{
				require_once('Class.GaString.php');
				$this->mIsOwn = false;
				$this->mViewedTraveler = new $this->mClasses['TRAVELER']['className']($this->mContextID);
				//$this->mViewedTraveler = new Traveler($this->mContextID);
				$this->mLinkLabels = array(
					/*'MY_PROFILE'			=> GaString::makePossessive(ucfirst($this->mViewedTraveler->getUserName())). ' Profile',
					'MY_JOURNALS'			=> 'Journals',
					'MY_FRIENDS'			=> 'Friends',
					'MY_GROUPS'				=> 'Groups',
					'CALENDAR'				=> 'Calendar'*/
					
					'MY_PROFILE'			=> 'Profile',
					'MY_FRIENDS'			=> 'Friends',
					'MY_GROUPS'				=> 'Groups',
					'MY_JOURNALS'			=> 'Journals',
					'MY_PHOTOS'				=> 'Photos',
					'MY_VIDEOS'				=> 'Videos',
					'CALENDAR'				=> 'Calendar'
				);
			}
		}
		
		/*protected function createPassportLink(){
			if($this->mIsOwn){
				$linkConst = 'MY_PASSPORT';
				$tempUrl = "/passport.php";
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$this->mLinkLabels[$linkConst];
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}*/
		
		protected function createDashboardLink(){
			$linkConst = 'MY_DASHBOARD';
			$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
				?$this->mAlteredUrls[$linkConst]
				:'/travelerdashboard.php';
			$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
			$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
			$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks)?$this->mAlteredLinks[$linkConst]:$this->mLinkLabels[$linkConst]);
			return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
		}
		
		protected function createProfileLink(){
			$linkConst = 'MY_PROFILE';
			$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
				?$this->mAlteredUrls[$linkConst]
				:'/'.$this->mViewedTraveler->getUserName();
			$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
			$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
			$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks)?$this->mAlteredLinks[$linkConst]:$this->mLinkLabels[$linkConst]);
			return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
		}
		
		protected function createJournalsLink(){
			$linkConst = 'MY_JOURNALS';
			$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
				?$this->mAlteredUrls[$linkConst]
				:"/travel.php?action=myjournals&travelerID=".$this->mContextID;
				//:($this->mIsOwn ? "/travel.php?action=myjournals" : "/travel.php?action=myjournals&travelerID=".$this->mContextID);
			$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
			$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
			$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$this->mLinkLabels[$linkConst];

			require_once($this->mClasses['JOURNAL_PRIVACY_PREF']['file']);
			$privacyPref  = new $this->mClasses['JOURNAL_PRIVACY_PREF']['className']();
			$journals     = $this->mViewedTraveler->getTravels( $privacyPref->getPrivacyCriteria2s( $this->mLoggedTraveler->getTravelerID(), $this->mViewedTraveler->getTravelerID()));

			require_once("travellog/model/Class.Article.php");
			$arCnt = Article::getArticleCount($this->mViewedTraveler->getTravelerID(),'TRAVELER');
			
			if(!$this->mIsOwn && !count($journals) && $arCnt <= 0){
				return null;
			}
			return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			//return null;
			/***
			$linkConst = 'MY_JOURNALS';
			$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls) ? $this->mAlteredUrls[$linkConst] : '/journal.php?';
			$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )? true : false;
			$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks)) ? true : false;
			$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks)) ? $this->mAlteredLinks[$linkConst] : $this->mLinkLabels[$linkConst];
			return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			***/
		}
		
		protected function createFriendsLink(){
			if($this->mViewedTraveler->getPrivacyPreference()->canViewFriends($this->mLoggedTraveler->getTravelerID()) || $this->mIsOwn){
				$linkConst = 'MY_FRIENDS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:($this->mIsOwn ? "/friend.php" : "/friend.php?tID=".$this->mContextID);
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$this->mLinkLabels[$linkConst];
								
				if(!$this->mIsOwn && 0 == count($this->mViewedTraveler->getFriends())){
					return null;
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createGroupsLink(){
			if($this->mViewedTraveler->getPrivacyPreference()->canViewGroups($this->mLoggedTraveler->getTravelerID()) || $this->mIsOwn){
				$linkConst = 'MY_GROUPS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:($this->mIsOwn ? "/group.php?mode=mygroups" : "/group.php?travelerID=".$this->mContextID);
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$this->mLinkLabels[$linkConst];
				if(!$this->mIsOwn && 5 >= count($this->mViewedTraveler->getGroups()) ){
					return null;
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createMessagesLink(){
			if($this->mIsOwn){
				/*** get the number of unread messages from inbox***/
				//$inbox = $this->mLoggedTraveler->getInbox();					
				//$newMsgsCount = $inbox->getNumberOfNewMessages();
				
				// modified by neri to coincide the number of messages in the subnavigation with the notifications in the profile component 
				
//				require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
//		
//				$traveler = gaTravelerMapper::getTraveler($this->mLoggedTraveler->getTravelerID());
//				$messageCenter = gaMessageCenterMapper::getTravelerMessageCenter($traveler);
//				
//				$newMsgsCount = count($messageCenter->getUnreadMessages());

				require_once('travellog/model/Class.SiteContext.php');
				
				$siteContext = SiteContext::getInstance();
				$siteAccessType = $siteContext->isInCobrand() ? 2 : 1;
				
				require_once 'travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php';
				$manager = gaMessagesManagerRepository::load($this->mLoggedTraveler->getTravelerID(), $siteAccessType);
				
				$newMsgsCount = $manager->countUnreadMessages();
				
				$linkConst = 'MESSAGES';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/messages.php";
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;					
				
				$html  = ' <span id="message_count_status2" style="' . ($newMsgsCount < 1 ? 'display:none' : '') . '">';
				$html .= 	'(<span id="new_messages_count2">';
				$html .=		$newMsgsCount;
				$html .=    '</span>)';
				$html .= '</span>';
				
				$defCaption = $html;
				
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Messages'.$defCaption;
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;	
		}
		
		protected function createAddressbookLink(){
			if($this->mIsOwn){
				$linkConst = 'ADDRESS_BOOK';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/addressbookmanagement.php";
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Address Book';
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createBulletinLink(){
			if($this->mIsOwn){
				$linkConst = 'BULLETIN';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/bulletinmanagement.php";
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Bulletin';
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createCalendarLink(){
			if($this->mViewedTraveler->getPrivacyPreference()->canViewCalendar($this->mLoggedTraveler->getTravelerID()) || $this->mIsOwn){
			
				$linkConst = 'CALENDAR';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:($this->mIsOwn ? "/calendar-event.php" : "/calendar-event.php?tID=".$this->mContextID);
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$this->mLinkLabels['CALENDAR'];
				
				//$cal = new Calendar($this->mViewedTraveler);
				require_once($this->mClasses['CALENDAR']['file']);
				$cal = new $this->mClasses['CALENDAR']['className']($this->mViewedTraveler);
				// $cal->setPrivacyContext($this->mViewedTraveler); // set the current user logged in the system
				$cal->setPrivacyContext($this->mLoggedTraveler); // set the current user logged in the system
				$events = $cal->getEvents(null, date('Y-m-d'));
				
				$eventCount = count($events);
				if($eventCount > 0 || $this->mIsOwn){
					return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
				}
			}
			return null;
		}
		
		/*protected function createOnlineAdvisingLink(){
			if($this->mIsOwn){
				$linkConst = 'STUDENT_SECTION';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/student-profile.php";
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Online Advising';
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createResumeLink(){
			if($this->mIsOwn){
				$linkConst = 'RESUME';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/resume.php?action=view";
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Resume';
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createParticipantInquiriesLink(){
			if($this->mIsOwn){	
				$navTraveler = new Traveler($this->mContextID);					 
				if(0 != AdminGroup::getAdvisorClientID($navTraveler->getTravelerID())){
					$linkConst = 'PARTICIPANT_INQUIRIES';
					$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
						?$this->mAlteredUrls[$linkConst]
						:"/student-communications.php";
					$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
					$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
					$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Participant Inquiries';				
					return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);											
				}
			}
			return null;
		}*/
		
		/**************************************************************
		 * added by neri for GANET video, widget, photos functionalities
		 * 11-05-08
		 **************************************************************/
		 
		protected function createVideosLink(){
			$linkConst = 'MY_VIDEOS';
			$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls) ? $this->mAlteredUrls[$linkConst] : '/video.php?travelerID='.$this->mContextID;
			$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )? true : false;
			$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks)) ? true : false;
			$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks)) ? $this->mAlteredLinks[$linkConst] : $this->mLinkLabels[$linkConst];
			
			$cobrandGroupID = (isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getGroupID() : 0;
			
			if(!$this->mIsOwn) 
				if (0 == count($this->mViewedTraveler->getTravelerVideos($cobrandGroupID, $this->mLoggedTraveler->getTravelerID())))
					return null;
			
			return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden); 
		}
		
		protected function createWidgetsLink(){
			if ($this->mIsOwn) {
				$linkConst = 'WIDGETS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls) ? $this->mAlteredUrls[$linkConst] : '/widget.php?travelerID='.$this->mContextID;
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )? true : false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks)) ? true : false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks)) ? $this->mAlteredLinks[$linkConst] : $this->mLinkLabels[$linkConst];
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		/*****************************************************************************
		 * edits of neri to createPhotosLink():
		 * 		required the GA_TravelerCollectionPhotos class:			12-18-08
		 * 		checked if there are photos for the viewed traveler:	12-18-08
		 *****************************************************************************/
		
		protected function createPhotosLink(){			
			require_once('travellog/UIComponent/NewCollection/helper/Class.PhotoCollectionHelper.php');
			require_once('travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionLoaderFactory.php');
			$collectionHelper = PhotoCollectionHelper::getInstance();
			$collectionHelper->setType(PhotoCollectionConstants::TRAVELER_TYPE);
			$collectionHelper->setTraveler($this->mViewedTraveler);
			$collectionHelper->setUserLevel($this->mIsOwn ? PhotoCollectionConstants::OWNER : PhotoCollectionConstants::VIEWER);
			$collectionLoader = PhotoCollectionLoaderFactory::getInstance()->create($collectionHelper);
			
			if (!$this->mIsOwn && !$collectionLoader->hasPhotos() )
				return null;
				
			else {
				$linkConst = 'MY_PHOTOS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls) ? $this->mAlteredUrls[$linkConst] : '/collection.php?type=traveler&ID='.$this->mContextID;
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )? true : false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks)) ? true : false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks)) ? $this->mAlteredLinks[$linkConst] : $this->mLinkLabels[$linkConst];
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
		}
		
		protected function createNewLink($text,$url,$isHighlighted,$isHidden){
			$tempLink = new SubNavigationLink();
			$tempLink->setLinkText($text);
			$tempLink->setUrl($url);
			$tempLink->setIsHighlighted($isHighlighted);
			$tempLink->setIsHidden($isHidden);
			return $tempLink;
		}
		
		public function isViewerOwner(){
			return $this->mIsOwn;
		}
		
		final public function show(){
			//apply caching here
			$memCache = ganetCacheProvider::instance()->getCache();
			$key = 'SubNavigation_'.$this->mContext.'_'.$this->mContextID.'_'.$this->mLinkToHighlight.'_'.SessionManager::getInstance()->get('travelerID').'_DOMAINID_'.SiteContext::getInstance()->getGroupID();
			$content = is_null($memCache) ? false : $memCache->get($key);
			if($this->mIsOwn || !$content){
					/************************************************************
					 * edits by neri for GANET video, widget, photos functionalities
					 * 11-05-08
					 ************************************************************/
					$subNavLinks = array();
					$subNavTpl 	= new Template();
					$subNavTpl->set('isOwn',$this->mIsOwn);
			
					/*$linkObj = $this->createPassportLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					} */
			
					//Dashboard tab only visible to owner
					if($this->mIsOwn){
						$linkObj = $this->createDashboardLink();
						if(!is_null($linkObj)){
							$subNavLinks[] = $linkObj;
						}
					}
			
					$linkObj = $this->createProfileLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					} 
			
					$linkObj = $this->createFriendsLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
			
					$linkObj = $this->createGroupsLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
			
					$linkObj = $this->createJournalsLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
			
					$linkObj = $this->createPhotosLink();
					if(!is_null($linkObj)){
						//$linkObj->setHasNewFeatures(true);
						$linkObj->setPublicView($this->mIsOwn);
						$subNavLinks[] = $linkObj;
					}	
			
					$linkObj = $this->createVideosLink();
					if(!is_null($linkObj)){
						//$linkObj->setHasNewFeatures(true);
						$linkObj->setPublicView($this->mIsOwn);
						$subNavLinks[] = $linkObj;
					}	
			
					$linkObj = $this->createMessagesLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
			
					/***
					$linkObj = $this->createBulletinLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
					***/
			
					$linkObj = $this->createAddressbookLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}

					$linkObj = $this->createCalendarLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
			
					/***
					$linkObj = $this->createOnlineAdvisingLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
			
					$linkObj = $this->createResumeLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
			
					$linkObj = $this->createParticipantInquiriesLink();
					if(!is_null($linkObj)){
						$subNavLinks[] = $linkObj;
					}
					***/
			
					$linkObj = $this->createWidgetsLink();
					if(!is_null($linkObj)){
						//$linkObj->setHasNewFeatures(true);
						$linkObj->setPublicView($this->mIsOwn);
						$subNavLinks[] = $linkObj;
					}	
				
			
					//add the custom links
			
					foreach($this->mCustomLinks as $customLink){					
						$tempLink = $this->createNewLink($customLink['TEXT'],$customLink['URL'],$customLink['IS_HIGHLIGHTED'],false);					
						//check the position of the customLink
						if(isset($customLink['POSITION']) && is_numeric($customLink['POSITION']) && $customLink['POSITION'] < count($this->mCustomLinks)){
							$ar1 = array_slice($this->mCustomLinks,0,$customLink['POSITION']);
							$ar2 = array_slice($this->mCustomLinks,$customLink['POSITION']);
							array_push($ar1,$tempLink);
							$subNavLinks = array_merge($ar1,$ar2);
						}
						else{
							$subNavLinks[] = $tempLink;
						}
					}
			
					$content = '';
					if(count($subNavLinks) > 1){			
						$subNavTpl->set('mainLinks',$subNavLinks);
						$subNavTpl->set('subLinks',array());
						$content = $subNavTpl->fetch("travellog/views/tpl.IncPassportAreaSubNavigation.php");
					}	

					//save to cache if not owner
					if(!$this->mIsOwn && !is_null($memCache)){
						$memCache->set($key, $content, array('EXPIRE'=>1800));
					}
			}
			echo $content;
		}
	}
?>