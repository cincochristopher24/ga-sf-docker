<?php
	require_once('travellog/model/Class.SiteContext.php');
	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterCriteria2.php');
	require_once('travellog/model/Class.FilterOp.php');
	require_once('travellog/model/Class.SubNavigationLink.php');
	require_once ('gaexception/Class.InstantiationException.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	
	require_once('Class.ToolMan.php');
	
	abstract class AbstractGroupSubNavigation
	{
		const ADMINISTRATOR = 1;
		const MEMBER		= 2;
		const NOT_MEMBER 	= 3;
		
		const ADMIN_GROUP	= 1;
		const FUN_GROUP		= 2;
		
		protected $mClasses = array(
			'TRAVELER' => array(
				'className' => 'Traveler',
				'file' 		=> 'travellog/model/Class.Traveler.php'
			),
			'CALENDAR' => array(
				'className' => 'Calendar',
				'file' 		=> 'travellog/model/Class.Calendar.php'
			),
			'ADMIN_GROUP' => array(
				'className' => 'AdminGroup',
				'file'		=> 'travellog/model/Class.AdminGroup.php'
			)			
		);
		
		protected $mContext = null;
		protected $mLinkToHighlight = null;
		protected $mContextID = null;
		protected $mGroupType = null;
		protected $mHiddenLinks = array();
		protected $mCustomLinks = array();
		protected $mAlteredLinks = array();
		protected $mAlteredUrls	= array();
		
		protected $mLoggedTraveler = null;
		protected $mViewedTraveler = null;
		protected $mTravelerStatus = null;
		protected $mTravelerStatusInSubGroup = null;
		protected $mSubNavGroup = null;	
		protected $mIsOwn = false;
		protected $mLinkLabels = array();
		
		
		protected $mParentGroup = null;
		protected $mPrivacyPref = null;
		protected $mCustomGroupSection = null;
		
		public function __construct($vars=array()){
			$this->mContext 		= $vars['context'];
			$this->mLinkToHighlight = $vars['linkToHighlight'];
			$this->mContextID 		= $vars['contextID'];
			$this->mGroupType 		= $vars['groupType'];
			$this->mHiddenLinks		= $vars['hiddenLinks'];
			$this->mCustomLinks 	= $vars['customLinks'];
			$this->mAlteredLinks	= $vars['alteredLinks'];
			$this->mAlteredUrls		= $vars['alteredUrls'];
			
			$session  = SessionManager::getInstance();
			$travelerID = $session->get('travelerID');
			
			$this->mLoggedTraveler = TravelerPeer::retrieveByPk($travelerID);
		}
		
		protected function isInCobrand(){
			return false;
		}
		
		/***
		 * visible only if subject group is an admin subgroup.
		 */
		/***
		protected function createParentGroupNameLink(){
			if($this->mSubNavGroup->getDiscriminator()==Group::ADMIN && $this->mSubNavGroup->isSubGroup()){	
				$parentGroup = $this->mSubNavGroup->getParent();
				$linkConst = 'PARENT_GROUP_NAME';					
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/group.php?gID=".$parentGroup->getGroupID();
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$parentGroup->getName();
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		***/
		/***
		 * Always visible. 
		 */
		protected function createGroupNameLink($group,$privacyPref,$options=array()){
			$linkConst = 'GROUP_NAME';
			$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
				?$this->mAlteredUrls[$linkConst]
				:"/group.php?gID={$group->getGroupID()}";
			//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight)?true:false;
			$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
			$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
			//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$group->getName();
			//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$group->getName();
			$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : $group->getName());
			return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
		}
		
		/***
		 * visible only to the administrator, moderator 
		 * or (not member but the group has a member).  
		 */
		protected function createMembersLink($group,$privacyPref,$options=array()){			
			$linkConst = 'MEMBERS';
			$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
				?$this->mAlteredUrls[$linkConst]
				:"/members.php?gID={$group->getGroupID()}";
			//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
			$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
			$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
			//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Members';
			//$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Members/Staff');
			
			if(array_key_exists('CAPTION',$options)){
				$tempLinkCaption = $options['CAPTION'];
			}
			else{
				$tempLinkCaption = (self::FUN_GROUP == $this->mGroupType ? 'Members' : 'Members/Staff' );
			}
			
			$memberCount = $group->getMembers(NULL,NULL,NULL,TRUE);
			if(self::ADMINISTRATOR != $this->mTravelerStatus && !$memberCount && !$group->hasStaff() ){
				if( 0 < $group->getParentID() || 0 == $group->getParentID() && !$group->getCountStaffofSubGroups() ){
					return null;
				}
			}
			return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
		}
		
		/***
		 * Visible only to the administrator in the parent group context
		 ***/
		
		protected function createStaffLink($group,$privacyPref,$options=array()){
			// /assignstaff.php
			if(self::ADMIN_GROUP==$this->mGroupType && (!$group->isSubGroup() && self::ADMINISTRATOR == $this->mTravelerStatus)){	
				$linkConst = 'STAFF';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/assignstaff.php?gID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;		
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Staff';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Staff');
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		/***
		 * visible only to the administrator, moderator 
		 * or (member or not member provided that the group has a subgroup). 
		 */
		protected function createSubGroupsLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP==$this->mGroupType && 
				!$group->isSubGroup() && 
				(self::NOT_MEMBER != $this->mTravelerStatus || (self::NOT_MEMBER==$this->mTravelerStatus && $privacyPref->getViewGroups() ))){
				$linkConst = 'SUBGROUPS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/group-pages.php?gID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight)?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Groups';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Subgroups');
				if(self::ADMINISTRATOR != $this->mTravelerStatus && !count($group->getSubGroups())){
					 return null;
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		
		/***
		 * added by daf on Aug07,2008
		 * show Custom Group Section (section name, if any) in subnavigation	
		 * visible only to the administrator, moderator 
		 * or (member or not member provided that the group has a subgroup). 
		 */
		protected function createCustomGroupsLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP==$this->mGroupType && 
				!$group->isSubGroup() && 
				(self::NOT_MEMBER != $this->mTravelerStatus || (self::NOT_MEMBER==$this->mTravelerStatus && $privacyPref->getViewGroups() )) ){
				$linkConst = 'CUSTOM_GROUP';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/customgroupsection.php?gID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight)?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Subgroups';
				
				if (array_key_exists('CAPTION',$options)){
					$tempLinkCaption = $options['CAPTION'];
				}
				elseif(strlen($this->mCustomGroupSection->getSectionLabel())){
					$label = $this->mCustomGroupSection->getSectionLabel();
					$tempLinkCaption = (strlen($label) > 20 ? substr($label,0,20).'...' : $label);
				}
				else{
					$tempLinkCaption = 'Projects and Programs';
				}
				if(self::ADMINISTRATOR != $this->mTravelerStatus 
					&& !count($group->getCustomGroupSection()->getAllSubGroups())){
					 return null;
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		
		/**
		 * Visible only to the administrator, moderator 
		 * or (member or not member provided that the group has a journal).
		 */
		
		protected function createJournalsLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP==$this->mGroupType){	
				$linkConst = 'MY_JOURNALS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/travel.php?action=groupJournals&gID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Journals';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Journals');
				
				require_once("travellog/model/Class.Article.php");
				/*$article = new Article();
				$articles = $article->getArticles('group',$group->getGroupID());*/
				$arCnt = Article::getArticleCount($group->getGroupID());
				
				if(self::ADMINISTRATOR != $this->mTravelerStatus && !count($group->getTravels()) && $arCnt <= 0){
					 return null;
				}			
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		/******************************************************************
		 * edits of neri to createPhotoLink():
		 * 		required the GA_GroupCollectionPhotos class		12-18-08
		 * 		checked if viewed group had photos:				12-18-08
		 ******************************************************************/
		
		protected function createPhotoLink($group,$privacyPref,$options=array()){
						
			if(self::ADMIN_GROUP==$this->mGroupType){
				
				require_once('travellog/UIComponent/NewCollection/helper/Class.PhotoCollectionHelper.php');
				require_once('travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionLoaderFactory.php');
				$collectionHelper = PhotoCollectionHelper::getInstance();
				$collectionHelper->setType(PhotoCollectionConstants::GROUP_TYPE);
				$collectionHelper->setGroup($group);
				$collectionHelper->setUserLevel(self::ADMINISTRATOR == $this->mTravelerStatus || self::ADMINISTRATOR == $this->mTravelerStatusInSubGroup ? PhotoCollectionConstants::ADMIN : PhotoCollectionConstants::VIEWER);
				$collectionLoader = PhotoCollectionLoaderFactory::getInstance()->create($collectionHelper);
				
				$linkConst = 'PHOTOS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:'/collection.php?type=group&ID='.$group->getGroupID();
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Photos');
				
				if (self::ADMINISTRATOR != $this->mTravelerStatus) {
					if (!count($group->getTravels($privacycriteria=0, $limit=null, $owner=false, $countOnly=TRUE)))
						return null;
						
					if( !$collectionLoader->hasPhotos() ){
						return null;
					}
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		/***
		 * Visible only if the subject group is an admin group and the viewer is the administrator 
		 * or the moderator or member or (not a member but the group has an itinerary).
		 */
		/***
		protected function createItinerariesLink($group,$privacyPref,$options=array()){
			if(	self::ADMIN_GROUP==$this->mGroupType &&	(self::NOT_MEMBER != $this->mTravelerStatus || (self::NOT_MEMBER==$this->mTravelerStatus && $privacyPref->getViewCalendar()))){
				$linkConst = 'ITINERARIES';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/program.php?action=view&gID={$group->getGroupID()}";
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Itineraries';
				if(self::ADMINISTRATOR != $this->mTravelerStatus){					
					$filter = NULL;
					$condpublic = new Condition();
					$condpublic->setAttributeName("displaypublic");
					$condpublic->setOperation(FilterOp::$EQUAL);
					$condpublic->setValue(1);
					
					$filter = new FilterCriteria2();
					$filter->addBooleanOp('AND');
					$filter->addCondition($condpublic);
						
					$rowslimitnum = 5;
					$rowslimit = new RowsLimit($rowslimitnum + 1, 0);
					$grpPrograms = $group->getPrograms($rowslimit, $filter);
					
					if(!count($grpPrograms)){
						return null;	
					} 
				}	
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			
			return null;
		}
		***/
		/***
		 * Visible only to the administrator, moderator, member or 
		 * (not member but the group pref allows it and the subject group has an event.)
		 */
		
		protected function createEventsLink($group,$privacyPref,$options=array()){
			if(self::NOT_MEMBER != $this->mTravelerStatus || (self::NOT_MEMBER==$this->mTravelerStatus && $privacyPref->getViewCalendar())){
				$linkConst = 'EVENTS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/event.php?action=viewGroupEvent&gID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Events';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Events');
				if(self::ADMINISTRATOR != $this->mTravelerStatus){
					// 			 		require_once($this->mClasses['CALENDAR']['file']);
					// $cal = new $this->mClasses['CALENDAR']['className']($group);
					// 
					// $cal = new Calendar($group);
					// if (!is_null($this->mLoggedTraveler)){
					// 	$cal->setPrivacyContext($this->mLoggedTraveler);							// set the current user logged in the system
					// }
					if($group instanceOf FunGroup)
						$events = $group->getGroupEvents(null,date("Y-m-d"));
					else	
						$events = $group->getCalendarActivities(NULL,date("Y-m-d"));
					
					$session = SessionManager::getInstance();
					$travelerID = $session->get('travelerID');
					$temp_events = array();
					if( !$session->getLogin() && count($events) ){
						foreach ( $events as $event){
							if ( $event->getDisplayPublic() == 1 ) $temp_events[] = $event; 
						}
					}
					elseif( count($events) ){
						$temp_events = $events;
					}
						
					if(!count($temp_events)){
						return null;
					}
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		/***
		 * Visible only the administrator, moderator, members or 
		 * (not members provided that the privacy pref allows it and the group has a bulletin that suits the viewer).
		 */
		/***
		protected function createBulletinLink($group,$privacyPref,$options=array()){
			// * Always show the Bulletin link since there would be no way for a member to post a bulletin if there is no link.
			
			if(self::NOT_MEMBER != $this->mTravelerStatus || (self::NOT_MEMBER==$this->mTravelerStatus && $privacyPref->getViewBulletin())){
				$linkConst = 'BULLETIN';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/bulletinmanagement.php?groupID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'News';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'News');
				if( self::NOT_MEMBER == $this->mTravelerStatus ){
					$msgSpace = new MessageSpace();
					$entries = $msgSpace->getBulletins($group->getSendableID(),null,null);
					if(!count($entries)){
						return null;
					}
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		***/
		/***
		protected function createAlertsLink($group,$privacyPref,$options=array()){
			if(self::ADMINISTRATOR==$this->mTravelerStatus){	
				$linkConst = 'ALERTS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/alertmanagement.php?groupID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;	
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Alerts';							
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Alerts');
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
				
			}
			return null;
		}
		***/
		/***
		protected function createMessagesLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP==$this->mGroupType && (!$group->isSubGroup() && self::ADMINISTRATOR == $this->mTravelerStatus)){
				$inbox = $this->mLoggedTraveler->getInbox();
				$newMsgsCount = count($inbox->getNewMessages());
				$linkConst = 'MESSAGES';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/messages.php";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;					
				$defCaption = ($newMsgsCount)?'('.$newMsgsCount.')':'';					
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Messages'.$defCaption;
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Messages'.$defCaption);
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
													
		}
		***/
		
		protected function createMessageCenterLink($group,$privacyPref,$options=array()) {
			require_once('travellog/model/Class.SiteContext.php');
			require_once 'travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php';
			
			$gaGroup = gaGroupMapper::getGroup($group->getGroupID());
			
			$siteContext = SiteContext::getInstance();
			$siteAccessType = $siteContext->isInCobrand() ? 2 : 1;
			
			//$messagesManager = gaMessagesManagerRepository::load($gaGroup->getAdministratorID(), $siteAccessType);
			$messagesManager = gaMessagesManagerRepository::loadGroup($gaGroup->getAdministratorID(), $siteAccessType, $gaGroup);
			
			if (isset($_SESSION['travelerID']) && in_array($_SESSION['travelerID'], $gaGroup->getOverseerIDs()))
				$isPowerful = true;
			else
				$isPowerful = false;
			
			if ($isPowerful) {
				$messages = $messagesManager->getMessages(array('NEWS', 'SHOUTOUT'));
				
				$newMsgsCount = 0;
				foreach ($messages as $message) {
					if (!$message->isRead()) {
						$newMsgsCount++;
					}
				}
					
				$html  = ' <span id="message_count_status2" style="' . ($newMsgsCount < 1 ? 'display:none' : '') . '">';
				$html .= 	'(<span id="new_messages_count2">';
				$html .=		$newMsgsCount;
				$html .=    '</span>)';
				$html .= '</span>';
				
				$defCaption = $html;
			} elseif ( isset($_SESSION['travelerID']) && in_array($_SESSION['travelerID'], $gaGroup->getMemberIDs())) {
				if ($messagesManager->countUnreadNews() > 0) {
					$defCaption = '';
				} else {
					return null;
				}
			} else {
				if($privacyPref->getViewBulletin() && count($messagesManager->getNewsOnly()) > 0){
					$defCaption = '';
				} else {
					return null;
				}
			}
			
				$linkConst = 'MESSAGE_CENTER';
				
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					//:"#";
					:"/messages.php?gID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;	
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Message Center' . $defCaption);
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			//}
			//return null;
		}
		
		protected function createAddressbookLink($group,$privacyPref,$options=array()){
			//if(self::ADMIN_GROUP==$this->mGroupType && (!$group->isSubGroup() && self::ADMINISTRATOR == $this->mTravelerStatus)){
			if(self::ADMIN_GROUP==$this->mGroupType && (self::ADMINISTRATOR == $this->mTravelerStatus)){
				$linkConst = 'ADDRESS_BOOK';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/addressbookmanagement.php?gID=".$group->getGroupID();
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;		
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Addressbook';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Addressbook');
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createResourceFilesLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP==$this->mGroupType && (self::NOT_MEMBER!=$this->mTravelerStatus || (self::NOT_MEMBER==$this->mTravelerStatus && $privacyPref->getDownloadFiles()))){ 
				$linkConst = 'RESOURCE_FILES';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/resourcefiles.php?cat=admingroup&action=view&genID={$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Resource Files';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Resource Files');
				$grpResourceFiles = $group->getResourceFiles();
				if(!count($grpResourceFiles) && self::ADMINISTRATOR != $this->mTravelerStatus){
					return null;
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createFormsLink($group,$privacyPref,$options=array()){
			$linkConst = 'FORMS';
			$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
				?$this->mAlteredUrls[$linkConst]
				:"#";
			//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
			$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
			$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;	
			$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Forms');
			return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
		}
		
		protected function createSurveyCenterLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP==$this->mGroupType && (!$group->isSubGroup() && self::ADMINISTRATOR == $this->mTravelerStatus)){	
				$linkConst = 'SURVEY_CENTER';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/surveycenter.php?gID=".$group->getGroupID();
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;		
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Survey Center';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Surveys');
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
			
		/**
		 * Rules for showing discussion/Knowledge Base link.
		 * 	Note: members also includes the administrator and staff
		 * 	General Rules:
		 * 		1. Club have no discussion link.
		 * 		2. Admin(administrator, staff) can always view the link.
		 * 		3. Archived discussions are viewable to admin only.
		 * 
		 * 	SubGroup:
		 * 		1. If there are no topics yet, only the members of the parent group can view the link.
		 * 		2. If there is a topic and the privacy setting is:
		 * 			2.1 Topic::SUBGROUP_PUBLIC_SETTING
		 * 				2.1.1 If there are discussions, the public can view the link.
		 * 				2.1.2 If there are no discussions, only the members of the subgroup can view the link.
		 * 			2.2 Topic::PUBLIC_SETTING
		 * 				2.2.1 If there are discussions, the public can view the link.
		 * 				2.2.2 If there are no discussions, only the members of the parent group can view the link.
		 * 			2.3 Topic::MEMBERS_ONLY_SETTING
		 * 				2.3.1 Only the members of the group can view the link.
		 * 			2.4 Topic::SUBGROUP_MEMBERS_ONLY_SETTING
		 * 				2.4.1 Only the members of the subgroup can view the link.
		 *	Root Group:
		 *		1. Archived topics are viewable to admin only.
		 *		2. If there are no topics yet and there are no added discussions, only the admin can view the link.
		 *		3. If there are active topics but have no discussions, only the members of the 
		 *			group can view the link.
		 *		4. If there are no topics but there are added discussions, the public can view the link.
		 *		4. If there are active topics and with active discussions, the public can view the link. 
		 */
		protected function createDiscussionBoardLink($group,$privacyPref,$options=array()){
			if(Group::FUN <> $group->getDiscriminator()) {
				if (self::ADMINISTRATOR != $this->mTravelerStatus) {
					require_once('travellog/model/Class.DiscussionPeer.php');
					require_once('travellog/model/Class.TopicPeer.php');	
					
					$session  = SessionManager::getInstance();
					$traveler_id = $session->get('travelerID'); 
					if (0 == DiscussionPeer::getGroupDiscussionsCount($group, $traveler_id)) {
						if (self::MEMBER != $this->mTravelerStatus) {
							return;
						}
						else if (0 == TopicPeer::getTopicsCount($group, $traveler_id)) {
							return;
						}
					}
				}				
				
				$linkConst = 'DISCUSSION_BOARD';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/discussion_board/home/{$group->getGroupID()}";
				//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;		
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);				
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;		
				//$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:'Discussion Board';
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Discussions');
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			
			return null;
		}
		
		// added by neri: 12-12-08
		
		protected function createVideoLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP == $this->mGroupType ){	
				$linkConst = 'VIDEOS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:'/video.php?gID='.$group->getGroupID();
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Videos');
				
				if(self::ADMINISTRATOR != $this->mTravelerStatus) 
					if (0 == count($group->getGroupVideos()))
						return null;
				
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			
			return null;
		}
		
		protected function createGroupFeedsLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP == $this->mGroupType ){
				$linkConst = 'GROUP_FEEDS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:"/groupfeeds.php?gID=".$group->getGroupID();
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : FALSE);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?TRUE:FALSE;		
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Feeds');
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return NULL;
		}
		
		protected function createGroupAppsLink($group,$privacyPref,$options=array()){
			if(self::ADMIN_GROUP==$this->mGroupType && (!$group->isSubGroup() && self::ADMINISTRATOR == $this->mTravelerStatus)){	
				$siteContext = SiteContext::getInstance();
				$siteAccessType = $siteContext->isInCobrand() ? 2 : 1;
				$linkConst = 'WIDGET';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:(($siteAccessType == 1) ? "/group.php?mode=traveler_api&gID=".$group->getGroupID() : "/group.php?mode=traveler_api");
				$tempIsHighlighted = (array_key_exists('IS_HIGHLIGHTED',$options) ? $options['IS_HIGHLIGHTED'] : false);
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;		
				$tempLinkCaption = (array_key_exists('CAPTION',$options) ? $options['CAPTION'] : 'Apps');
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}
		
		protected function createNewLink($text,$url,$isHighlighted,$isHidden){
			$tempLink = new SubNavigationLink();
			$tempLink->setLinkText($text);
			$tempLink->setUrl($url);
			$tempLink->setIsHighlighted($isHighlighted);
			$tempLink->setIsHidden($isHidden);
			$tempLink->setContext($this->mContext);
			return $tempLink;
		}
		
		protected function alterLinkUrl($linkConst,$url){
			$this->mAlteredUrls[$linkConst] = $url;
		}
		
		public function isViewerOwner(){
			return self::ADMINISTRATOR == $this->mTravelerStatus;
		}
		
		final public function show(){			
			require_once('travellog/model/Class.GroupFactory.php');
			require_once('travellog/model/Class.GroupPrivacyPreference.php');
			require_once('travellog/model/Class.AdminGroup.php');

			$factory =  GroupFactory::instance();
			try{
				$subNavGroup =  $factory->create(array($this->mContextID));
			}
			catch(exception $e){
				ToolMan::redirect('/index.php');
			}
			$this->mSubNavGroup = $subNavGroup[0];
			$this->mGroupType = ($subNavGroup[0]->getDiscriminator() == GROUP::ADMIN)?self::ADMIN_GROUP:self::FUN_GROUP;

			//check out the logged traveler
			if(is_null($this->mLoggedTraveler)){
				$travelerStatus = self::NOT_MEMBER;
			}
			else{
				if($subNavGroup[0]->getAdministrator()->getTravelerID() == $this->mLoggedTraveler->getTravelerID() || $subNavGroup[0]->isStaff($this->mLoggedTraveler->getTravelerID())){
					$travelerStatus = self::ADMINISTRATOR;
				}
		
				elseif($subNavGroup[0]->isMember($this->mLoggedTraveler)){
					$travelerStatus = self::MEMBER;
				}
				else{
					$travelerStatus = self::NOT_MEMBER;
				}
			}			
			$this->mTravelerStatus = $travelerStatus;
			$this->mTravelerStatusInSubGroup = $this->mTravelerStatus;

			//if(self::ADMINISTRATOR == $this->mTravelerStatus || !$content){
			//if In cobrand, show only to members
			//if in main dot net, show even to non members
			
			$showSubNav = self::ADMINISTRATOR == $this->mTravelerStatus || (!$this->isInCobrand() || ($this->isInCobrand() && self::MEMBER == $this->mTravelerStatus));
			if($showSubNav){
				$memCache = ganetCacheProvider::instance()->getCache();
				$key = 'SubNavigation_'.$this->mContext.'_'.$this->mContextID.'_'.$this->mLinkToHighlight.'_'.SessionManager::getInstance()->get('travelerID').'_DOMAINID_'.SiteContext::getInstance()->getGroupID().'_status:'.$this->mTravelerStatus;
				$content = is_null($memCache) ? false : $memCache->get($key);
				
				$getFresh = self::ADMINISTRATOR == $this->mTravelerStatus || self::MEMBER == $this->mTravelerStatus;
				
				if(FALSE === $content || $getFresh){
					$subNavLinks = array();
					$subNavTpl 	= new Template();
					$subNavTpl->set('groupName',$this->mSubNavGroup->getName());
					
					$isown = ( self::NOT_MEMBER == $this->mTravelerStatus )	?	false	:	true;
			
					$mainLinks = array();
					$subLinks = array();
					$parentGroup = $this->mSubNavGroup;
			
					if($this->mSubNavGroup->isSubGroup()){
						try {
						  $privacyPref = new GroupPrivacyPreference($this->mSubNavGroup->getGroupID());
						  $linkObj = $this->createGroupNameLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'GROUP_NAME'==$this->mLinkToHighlight));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }
				
							if( !is_null($this->mLoggedTraveler) && (self::ADMINISTRATOR == $travelerStatus || $this->mSubNavGroup->isStaff($this->mLoggedTraveler->getTravelerID())) ){
								$linkObj = $this->createGroupFeedsLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'GROUP_FEEDS'==$this->mLinkToHighlight));
								if(!is_null($linkObj)){
									$subLinks[] = $linkObj;
								}
							}
				  
						  $linkObj = $this->createMembersLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'MEMBERS'==$this->mLinkToHighlight));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }
				
						  $linkObj = $this->createJournalsLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'MY_JOURNALS'==$this->mLinkToHighlight));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }
				  
							$linkObj = $this->createPhotoLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'PHOTOS'==$this->mLinkToHighlight));
							if(!is_null($linkObj)){
								$subLinks[] = $linkObj;
							}
				  
						  $linkObj = $this->createVideoLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('MY_VIDEOS'==$this->mLinkToHighlight)));
							if(!is_null($linkObj)){
								//$linkObj->setHasNewFeatures(true);
								$subLinks[] = $linkObj;
							}
				
						  $linkObj = $this->createEventsLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'EVENTS'==$this->mLinkToHighlight));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }
				  
						  /***
						  $linkObj = $this->createMessagesLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'MESSAGES'==$this->mLinkToHighlight));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }
						  ***/
				  
						  $linkObj = $this->createMessageCenterLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('MESSAGE_CENTER'==$this->mLinkToHighlight)));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }
				  
						  $linkObj = $this->createAddressbookLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'ADDRESS_BOOK'==$this->mLinkToHighlight));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }
				
						  $linkObj = $this->createResourceFilesLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'RESOURCE_FILES'==$this->mLinkToHighlight,'CAPTION' => 'Files'));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }
				
						  $linkObj = $this->createDiscussionBoardLink($this->mSubNavGroup,$privacyPref,array('IS_HIGHLIGHTED'=>'DISCUSSION_BOARD'==$this->mLinkToHighlight));
						  if(!is_null($linkObj)){
							  $subLinks[] = $linkObj;
						  }			
						}
						catch(InstantiationException $ex) {}
						//$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
						$parentGroup = $this->mSubNavGroup->getParent();
					}
			
					/*********/
					$privacyPref = new GroupPrivacyPreference($parentGroup->getGroupID());
					$this->mPrivacyPref = $privacyPref;
					$this->mParentGroup = $parentGroup;
			
					//check out the logged traveler again this time for main group
					if(is_null($this->mLoggedTraveler)){
						$travelerStatus = self::NOT_MEMBER;
					}
					else{
						if($parentGroup->getAdministrator()->getTravelerID() == $this->mLoggedTraveler->getTravelerID() || $parentGroup->isStaff($this->mLoggedTraveler->getTravelerID())){
							$travelerStatus = self::ADMINISTRATOR;
						}
				
						elseif($parentGroup->isMember($this->mLoggedTraveler)){
							$travelerStatus = self::MEMBER;
						}
						else{
							$travelerStatus = self::NOT_MEMBER;
						}
					}			
					$this->mTravelerStatus = $travelerStatus;
					$isown = ( self::NOT_MEMBER == $this->mTravelerStatus )	?	false	:	true;
			
			
					$linkObj = $this->createGroupNameLink($parentGroup,$privacyPref,array(
						'IS_HIGHLIGHTED'=>('GROUP_NAME'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup()),
						'CAPTION'		=> 'HOME'
					));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
			
					if( self::ADMINISTRATOR == $travelerStatus || !is_null($this->mLoggedTraveler) && $this->mParentGroup->isStaff($this->mLoggedTraveler->getTravelerID()) ){
						$linkObj = $this->createGroupFeedsLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('GROUP_FEEDS'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
						if(!is_null($linkObj)){
							$linkObj->setHasNewFeatures(true);
							$mainLinks[] = $linkObj;
						}
					}
			
					$linkObj = $this->createMembersLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('MEMBERS'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
			
					$isGroupLinkHighlighed = $this->mSubNavGroup->isSubGroup() || 'SUBGROUPS'==$this->mLinkToHighlight;			
					$linkObj = $this->createSubGroupsLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>$isGroupLinkHighlighed));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;			
					}
			
					/***
					$isCustomGroupLinkHighlighed = (!is_null($customGroupSection) && $this->mSubNavGroup->isSubGroup() && $customGroupSection->isSubGroupInSection($this->mSubNavGroup->getGroupID())) || 'CUSTOM_GROUP'==$this->mLinkToHighlight;			
					$linkObj = $this->createCustomGroupsLink($parentGroup,$privacyPref,array(
						'IS_HIGHLIGHTED'=>($isCustomGroupLinkHighlighed)
					));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
					***/
			
					$linkObj = $this->createJournalsLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('MY_JOURNALS'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
			
					$linkObj = $this->createPhotoLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('PHOTOS'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
					if(!is_null($linkObj)){
						//$linkObj->setHasNewFeatures(true);
						$linkObj->setPublicView($isown);
						$mainLinks[] = $linkObj;
					}
			
					$linkObj = $this->createVideoLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('MY_VIDEOS'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
					if(!is_null($linkObj)){
						//$linkObj->setHasNewFeatures(true);
						$linkObj->setPublicView($isown);
						$mainLinks[] = $linkObj;
					}
			
					$linkObj = $this->createEventsLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('EVENTS'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
			
					$linkObj = $this->createMessageCenterLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('MESSAGE_CENTER'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
		    
					$linkObj = $this->createAddressbookLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('ADDRESS_BOOK'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
		
					$linkObj = $this->createResourceFilesLink($parentGroup,$privacyPref,array(
						'IS_HIGHLIGHTED'=>('RESOURCE_FILES'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup()),
						'CAPTION'		=> 'Files'
					));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
			
					/***
					$linkObj = $this->createFormsLink($parentGroup,$privacyPref);
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
					***/
			
					$linkObj = $this->createSurveyCenterLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('SURVEY_CENTER'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
			
					$linkObj = $this->createDiscussionBoardLink($parentGroup,$privacyPref,array('IS_HIGHLIGHTED'=>('DISCUSSION_BOARD'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup()), 'CAPTION'=>'Knowledge Base'));
					if(!is_null($linkObj)){
						//$linkObj->setHasNewFeatures(true);
						$linkObj->setPublicView($isown);
						$mainLinks[] = $linkObj;
					}
					
					$linkObj = $this->createGroupAppsLink($parentGroup,$privacyPref,array(
						'IS_HIGHLIGHTED'=>('WIDGET'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup()), 
						'CAPTION'=>'Apps'
					));
					if(!is_null($linkObj)){
						$mainLinks[] = $linkObj;
					}
					
					/***
					if($this->isInCobrand() && AbstractGroupSubNavigation::ADMINISTRATOR != $this->mTravelerStatus){
						$mainLinks = $subLinks;
						$subLinks = array();
					}
					***/
					/*********/
					if(count($mainLinks) > 1){
						$subNavTpl->set('mainLinks',$mainLinks);
						$subNavTpl->set('subLinks',$subLinks);
						$content = $subNavTpl->fetch("travellog/views/tpl.IncPassportAreaSubNavigation.php");
					}
					
					//save to cache if not admin, staff or member
					//save to cache if not owner
					if(!$getFresh AND !is_null($memCache)){
						$memCache->set($key, $content, array('EXPIRE'=>1800));
					}
				}
				
				echo $content;
			}
			
			
		}
		
	}
?>