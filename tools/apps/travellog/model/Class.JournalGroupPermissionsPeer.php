<?php

	require_once('Class.ConnectionProvider.php');
	require_once('travellog/model/Class.JournalGroupPermissions.php');
	
	class JournalGroupPermissionsPeer{
		
		static public function countUsingTravelAndGroup($travelID=0, $groupID=0){
			$sql = "SELECT COUNT(*) as count FROM tblJournalGroupPermissions WHERE travelID = $travelID AND groupID = $groupID";
			
			$rs	= ConnectionProvider::instance()->getRecordset();
			$rs->execute($sql);

			$row = mysql_fetch_array($rs->Resultset());
			return $row['count'];
		}
		
		static public function retrieveByTravelAndGroup($travelID=0, $groupID=0){
			$sql = "SELECT * FROM tblJournalGroupPermissions WHERE travelID = $travelID AND groupID = $groupID LIMIT 0,1";
			
			$rs	= ConnectionProvider::instance()->getRecordset();
			$rs->execute($sql);
			
			if($rs->Recordcount() > 0){
				$row = mysql_fetch_array($rs->Resultset());

				$jgp = new JournalGroupPermissions;
				$jgp->setData($row);
				return $jgp;
			}
			return null;
		}
		
		static public function entryAlreadyExists($travelID=0, $groupID=0){
			$count = self::countUsingTravelAndGroup($travelID, $groupID);
			return $count > 0 ? true : false;
		}
		
	}