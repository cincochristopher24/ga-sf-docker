<?php
	/**
	 * @(#) Class.SubGroupCategoryRepository.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - Feb. 9, 2009
	 */
	
	class SubGroupCategoryRepository {
		private static $data = array();
		
		/**
		 * Creates the CustomizedSubGroupCategory given the IDs. This stores the 
		 * CustomizedSubGroupCategory and when you create the CustomizedSubGroupCategory
		 * using this method this returns the previously created CustomizedSubGroupCategory.
		 * 
		 * @return CustomizedSubGroupCategory
		 */
		static function createSubGroupCategory($id){
			require_once("travellog/model/Class.CustomizedSubGroupCategory.php");
			
			if (!isset(self::$data[$id])) {
				self::$data[$id] = new CustomizedSubGroupCategory($id);
			}
			
			return self::$data[$id];
		}
	}
	
