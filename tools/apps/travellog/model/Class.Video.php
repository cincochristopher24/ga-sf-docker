<?php
/*
 * Created on 10 6, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 	/**
 	 * an abstract class for the entity video
 	 */
 
	 abstract class Video {
	 	/**
	 	 * member variables
	 	 */
	 	
	 	protected $mVideoID;
	 	protected $mVideoUrl;
	 	protected $mVideoImageUrl;
	 	protected $mCaption = null;
	 	protected $mDateAdded;
	 	protected $mIsPrimaryVideo;
	 	protected $mDuration;
	 	protected $mTitle;
	 	protected $mParentAlbum;
		protected $mCommentCount;
	 	
	 	/**
	 	 * generic methods
	 	 */
	 	
	 	public function setVideoID ($videoID) {
	 		$this->mVideoID = $videoID;
	 	}
	 	
	 	public function setVideoUrl ($url) {
	 		$this->mVideoUrl = $url;
	 	}
	 	
	 	public function setVideoImageUrl ($imageUrl) {
	 		$this->mVideoImageUrl = $imageUrl;
	 	}
	 	
	 	public function setCaption ($caption) {
	 		$this->mCaption = $caption; 
	 	}
	 	
	 	public function setDateAdded ($date) {
	 		$this->mDateAdded = $date;
	 	}
	 	
	 	public function setAsPrimaryVideo ($flag) {
	 		$this->mIsPrimaryVideo = $flag;
	 	}
	 	
	 	public function setDuration ($duration) {
	 		$this->mDuration = $duration;
	 	}
	 	
	 	public function setTitle ($title) {
	 		$this->mTitle = $title;
	 	}
	 	
	 	public function setParentAlbum ($album) {
	 		$this->mParentAlbum = $album;
	 	} 
	 	
	 	public function getVideoID () {
	 		return $this->mVideoID;
	 	}
	 	
	 	public function getVideoUrl () {
	 		return $this->mVideoUrl;
	 	}
	 	
	 	public function getVideoImageUrl () {
	 		return $this->mVideoImageUrl;
	 	}
	 	
	 	public function getCaption () {
	 		return $this->mCaption;
	 	}
	 	
	 	public function getDateAdded () {
	 		return $this->mDateAdded;
	 	}
	 	
	 	public function isPrimaryVideo () {
	 		return $this->mIsPrimaryVideo;
	 	}
	 	
	 	public function getDuration () {
	 		return $this->mDuration;
	 	}
	 	
	 	public function getTitle () {
	 		return $this->mTitle;
	 	}
	 	
	 	public function getParentAlbum () {
	 		return $this->mParentAlbum;
	 	}
	
		public function getComments($rowslimit = NULL, $count = FALSE){
			$handler = new dbHandler();
			$arrComment = array();

			$sql = "SELECT tc.* " .
				" FROM tblCommentToVideo as talc, tblComment as tc " .
				" WHERE talc.commentID = tc.commentID ".
				" AND talc.videoID = ".$handler->makeSqlSafeString($this->getVideoID()).
				" AND tc.isApproved = 1 ";
			$sql .= " ORDER BY tc.commentID DESC ";
			$sql .= (is_null($rowslimit)) ? "" : SqlUtil::createLimitClause($rowslimit);
			
			try {
				$rs = new iRecordset($handler->execute($sql));
				
				if($count)	return $rs->retrieveRecordCount();				
				while (!$rs->EOF()) {
					$arrComment[] = new Comment($rs->current());
					try {
						$rs->next();
					}catch (exception $ex) {
						throw new exception($ex->getMessage());
					}
				}
			}catch(exception $ex){
				throw new exception($ex->getMessage());
			}
			
			$this->mCommentCount = count($arrComment);	
			return $arrComment;              
	    }
	
		public function getCommentCount(){
			if( is_null($this->mCommentCount) ){
				return $this->getComments(NULL, false, TRUE);
			}
				
			return $this->mCommentCount;
		}
		
		public function addComment( Comment $comment )
		{
			$comment->Create();
			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
        	$commentID = $comment->getCommentID();
            
            $sql = "INSERT into tblCommentToVideo (commentID, videoID) VALUES ($commentID, $this->mVideoID)";
            $this->rs->Execute($sql);
		}
		
		/**
		   * Removes a comment from this video
		   * 
		   * @param Comment $comment
		   * @see Comment
		   * @throws exception
		   * @return void
		   */
		public function removeComment(Comment $comment){          
		    $handler = new dbHandler();
		    $commentID = $comment->getCommentID();

		    $sql = "DELETE FROM tblCommentToVideo " .
		      " WHERE `videoID` = ".$handler->makeSqlSafeString($this->mVideoID).
					" AND `commentID` = ".$handler->makeSqlSafeString($comment->getCommentID());    

		    try {
		    	$handler->execute($sql);
		    } 
		    catch(exception $ex){
		    	throw new exception($ex->getMessage());
		    }                                       
		}
	 	
	 	/** 
	 	 * abstract methods
	 	 */
	 	
	 	abstract function create ();
	 	abstract function update ();
	 	abstract function delete ();
		abstract function getPlayActionLink ();
		abstract function getFriendlyUrl ();
	 } 
