<?php
require_once('Class.Connection.php');
require_once('Class.Recordset.php');
class TravelerRanking{
	
	private 
	
	$travelerID = 0,
	
	$ranking    = 0;
	
	function Save(){
		$conn    = new Connection;
		$rs      = new Recordset($conn);
		$sql     = sprintf('INSERT INTO tblTravelerRanking (travelerID,ranking) VALUES (%d,%d)',$this->travelerID,$this->ranking);
		$results = $rs->Execute($sql);
	}
	
	function Update(){
		$conn    = new Connection;
		$rs      = new Recordset($conn);
		$sql     = sprintf('UPDATE tblTravelerRanking SET ranking=%d WHERE travelerID=%d',$this->ranking,$this->travelerID);
		$results = $rs->Execute($sql);
	}
	
	function getAllTravelersWithNoRanking(){
		$conn    = new Connection;
		$rs      = new Recordset($conn);
		$sql     = "SELECT `travelerID` FROM tblTraveler where 1 = 1 and active > 0 AND travelerID not in " .
				"(SELECT DISTINCT travelerID FROM tblTravelerRanking) " .
				"AND travelerID not in (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor)";
				
		$results = $rs->Execute($sql);
		$arr     = array();
		if($rs->Recordcount()){
			while($row = mysql_fetch_assoc($results)){
				$objNewMember = new TravelerRanking;
				$objNewMember->setTravelerID( $row['travelerID'] );
				$objNewMember->setRanking   ( 0                  );
				$arr[] = $objNewMember; 		
			}
		}
		return $arr;
	}
	
	function setTravelerID($travelerID){ $this->travelerID = $travelerID; }
	function getTravelerID()           { return $this->travelerID;        }
	
	function setRanking($ranking){ $this->ranking = $ranking; }
	function getRanking()        { return $this->ranking;     }
}
?>
