<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: 
 */
 
    require_once("Class.Connection.php");
    require_once("Class.Recordset.php");
    require_once("Class.UploadManager.php");
    require_once('travellog/model/Class.GroupFactory.php');
    require_once('Class.ConnectionProvider.php');
    require_once('Cache/ganetCacheProvider.php');
         
    class PhotoAlbum{
        
        /**
         * Define variables for Class Member Attributes
         */                           
            private $mConn      = NULL;
            private $mRs        = NULL;
           
            private $mGroupID      = NULL;
            
            private $mPhotoAlbumID = NULL;
            private $mTitle        = NULL;
            private $mLastUpdate   = NULL;
            private $mCreator      = NULL;

            private $mPhotos = array();
            public  $phototype	= 7;
            private $randphoto    = NULL;
            
			private $mPrimaryPhoto = NULL;
			private $mIsFeatured = 0;
			
			private static $instantiatedAlbums = array();
                           
        /**
         * Constructor Function for this class
         */
            function PhotoAlbum($photoalbumID = 0){
                
                $this->mPhotoAlbumID = $photoalbumID;
                
                try {
                    $this->mRs   = new Recordset(ConnectionProvider::instance()->getConnection());
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mPhotoAlbumID){
                    
					if( isset(self::$instantiatedAlbums[$this->mPhotoAlbumID]) ){
						$data = self::$instantiatedAlbums[$this->mPhotoAlbumID];
						      
	                    $this->mTitle       = stripslashes($data["title"]);           
	                    $this->mCreator     = stripslashes($data["creator"]);
	                    $this->mLastUpdate  = $data["lastupdate"];           
                        $this->mGroupID   = $data["groupID"];
						$this->mPrimaryPhoto = $data["primaryPhoto"];
						$this->mIsFeatured = $data["isFeatured"];
					}else{

                    	$sql = "SELECT * FROM tblPhotoAlbum WHERE photoalbumID = '$this->mPhotoAlbumID' ";
	                    $this->mRs->Execute($sql);
                
	                    if (0 == $this->mRs->Recordcount()){
	                        throw new Exception("Invalid PhotoAlbumID");        // ID not valid so throw exception
	                    }
                    
	                    // Sets values to its class member attributes.      
	                    $this->mTitle       = stripslashes($this->mRs->Result(0,"title"));           
	                    $this->mCreator     = stripslashes($this->mRs->Result(0,"creator"));
	                    $this->mLastUpdate  = $this->mRs->Result(0,"lastupdate");           
                    
	                    // Sets a value to its parent key.
	                    $this->mGroupID   = $this->mRs->Result(0,"groupID");

						$this->mPrimaryPhoto = $this->mRs->Result(0,"primaryPhoto");
						$this->mIsFeatured = $this->mRs->Result(0,"isFeatured");
						
						$data["title"] = $this->mRs->Result(0,"title");
						$data["creator"] = $this->mRs->Result(0,"creator");
						$data["lastupdate"] = $this->mRs->Result(0,"lastupdate");
						$data["groupID"] = $this->mRs->Result(0,"groupID");
						$data["primaryPhoto"] = $this->mRs->Result(0,"primaryPhoto");
						$data["isFeatured"] = $this->mRs->Result(0,"isFeatured");
						self::$instantiatedAlbums[$this->mPhotoAlbumID] = $data;
					}
                }
            }
           
           
        /**
         * Setter Functions
         */
            function setPhotoAlbumID($photoalbumID){
               $this->mPhotoAlbumID = $photoalbumID;
            }
            
            function setTitle($title){
               $this->mTitle = $title;
            }
           
            function setLastUpdate($lastupdate){
               $this->mLastUpdate = $lastupdate;
            }
            
            function setCreator($creator){
               $this->mCreator = $creator;
            }
            
            function setPrimaryPhotoID($val) {
            	$this->mPrimaryPhoto = (int) $val;
            }
            
            function getPrimaryPhotoID() {
            	return $this->mPrimaryPhoto;
            }
             
        /**
         * Getter Functions
         */       
			function getID(){
				return $this->getPhotoAlbumID();
			}
			
            function getPhotoAlbumID(){
               return $this->mPhotoAlbumID;
            }
            
            function getTitle(){
                return $this->mTitle;
            }
            
            function getLastUpdate(){
                return $this->mLastUpdate;
            }
            
            function getCreator(){
               return $this->mCreator;
            }
           
      		function getPrimary(){
				return $this->mPrimaryPhoto;
			}

        /**
         * Setter Function for the Parent Key
         */
            function setGroupID($groupID){
                $this->mGroupID = $groupID;
            }     
            
     
        /**
         * Getter Function for the Parent Key
         */        
            function getGroupID(){
                return $this->mGroupID;
            }
            
            function getGroup(){
                
                $factory =  GroupFactory::instance();
				$group   =  $factory->create(array($this->mGroupID));
			
                return $group[0];
            }

			//function to get the owner of this album
			//returns either an instance of Group or Traveler
			function getOwner(){
				$owner = NULL;
				if( !is_null($this->mGroupID) && 0 < $this->mGroupID ){
					$factory =  GroupFactory::instance();
					$group   =  $factory->create(array($this->mGroupID));
	                $owner = $group[0];
				}else if( !is_null($this->mGroupID) && 0 == $this->mGroupID && !is_null($this->mCreator) ){
					$owner = new Traveler($this->mCreator);
				}
				return $owner;
			}
            
       
        /**
         * CRUD Methods (Create, Update , Delete)
         */
            function Create(){
               
                $Atitle      = addslashes($this->mTitle);
                $Acreator    = addslashes($this->mCreator);
                $Alastupdate = date("Y-m-d H:i:s");
                
                $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
                
                $sql = "INSERT into tblPhotoAlbum (groupID, title, lastupdate, creator) " .
                        "VALUES ('$this->mGroupID', '$Atitle', '$Alastupdate', '$Acreator')";
                $this->mRs->Execute($sql);       
                $this->mPhotoAlbumID = $this->mRs->GetCurrentID();

	            // delete cache entry for photo albums of group set in PhotoAlbumAdapter
	            $this->invalidateCacheEntry();
				
            }
            
            function Update(){
                
                $Atitle      = addslashes($this->mTitle);
                $Alastupdate = date("Y-m-d H:i:s");
                
                $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
                $sql = "UPDATE tblPhotoAlbum SET title = '$Atitle', lastupdate = '$Alastupdate' WHERE photoalbumID = '$this->mPhotoAlbumID' ";
                $this->mRs->Execute($sql);
                
            	// delete cache entry for photo albums of group set in PhotoAlbumAdapter
	            $this->invalidateCacheEntry();
            }
            
            function Delete(){
 
                $myphotos = $this->getPhotos();
                              
                for($idx_ph=0;$idx_ph<count($myphotos);$idx_ph++){
					if( $myphotos[$idx_ph]->getPhotoTypeID() == $this->phototype ){
						if( !$this->isGrabbedPhoto($myphotos[$idx_ph]->getPhotoID() ) ){
							$myphotos[$idx_ph]->Delete();	
						}
					}// loop thru the records and delete each one
				}
                $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
                $sql = "DELETE FROM tblPhotoAlbumtoPhoto WHERE photoalbumID = '$this->mPhotoAlbumID'";
				$this->mRs->Execute($sql);	
                $sql = "DELETE FROM tblPhotoAlbum WHERE photoalbumID = '$this->mPhotoAlbumID' ";       
                $this->mRs->Execute($sql);

				//added by Jul to delete feeds of this album
				require_once("travellog/model/Class.ActivityFeed.php");
				$PHOTOALBUM = ActivityFeed::PHOTOALBUM;
				$PHOTO = ActivityFeed::PHOTO;
				$albumID = $this->mPhotoAlbumID;
				$sql = "DELETE FROM tblActivityFeed	
						WHERE (feedSection = $PHOTOALBUM AND sectionID = $albumID)
							OR (feedSection = $PHOTO AND sectionRootID = $albumID)";
				$this->mRs->Execute($sql);
				
				// delete cache entry for photo albums of group set in PhotoAlbumAdapter
	            $this->invalidateCacheEntry();				
            }       
            
                 
        /**
         * Purpose: Adds photo to this photo album.
         */
            function addPhoto($photo,$grabbed=FALSE){
				//$grabbed added by Jul to distinguish original upload from grabbed adding of the photo

            	$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
                //editted by: joel
            	//################################
            	$sqlq = "SELECT max(position) as mposition from tblPhotoAlbumtoPhoto WHERE photoalbumID = '$this->mPhotoAlbumID'";
	 			$this->mRs->Execute($sqlq);
            	
            	
            	if($this->mRs->Recordcount() > 0){
            			$rec = mysql_fetch_array($this->mRs->Resultset());
            			$pos = $rec['mposition']+1;           			
            	}else{
            			$pos =0;	
            	}
            	//################################
                                
                $photoID = $photo->getPhotoID();
                $sql = "INSERT into tblPhotoAlbumtoPhoto (photoalbumID, photoID, position) VALUES ('$this->mPhotoAlbumID', '$photoID', '$pos')";
                $this->mRs->Execute($sql);
				
				if( !$grabbed ){
					$dateUpdated = date("Y-m-d H:i:s");
					$sql = "UPDATE tblPhotoAlbum
							SET lastupdate = '$dateUpdated'
							WHERE photoalbumID = '$this->mPhotoAlbumID'";
					$this->mRs->Execute($sql);
				
					//added by Jul to create feeds for this uploaded photo
				//	require_once("travellog/model/Class.ActivityFeed.php");
				//	try{
				//		ActivityFeed::create($photo,$this);
				//	}catch(exception $e){}
				}
				// delete cache entry for photo albums of group set in PhotoAlbumAdapter
	            $this->invalidateCacheEntry();
            }
        
        /**
         * Purpose: Removes a photo from this photo album.
         * Deletes the photo as well as its link to this program.
         */
            function removePhoto($photoID,$dateUploaded){ //Jul added parameter dateuploaded for feeds
                
               //$photo = new Photo($this, $photoID);                                            // delete photo
               //$photo->Delete();

            	$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            	
                $sql = "DELETE FROM tblPhotoAlbumtoPhoto where photoalbumID = '$this->mPhotoAlbumID' AND photoID = '$photoID' ";    
                $this->mRs->Execute($sql);

				//added by Jul to delete feeds of the photo
     			$uploadedByDayCount = $this->getPhotosUploadedByDay(NULL,$dateUploaded,TRUE);
				if( 0 == $uploadedByDayCount ){
					require_once("travellog/model/Class.ActivityFeed.php");	
					$params = array("FEED_SECTION"		=>	ActivityFeed::PHOTO,
									"SECTION_ROOT_ID"	=>	$this->mPhotoAlbumID,
									"FEED_DATE"			=>	$dateUploaded );
					$photoFeeds = ActivityFeed::getInstances($params);
					if( 0 < $photoFeeds["recordCount"] ){
						$photoFeeds["activityFeeds"][0]->Delete();
					}
				}
				// delete cache entry for photo albums of group set in PhotoAlbumAdapter
	            $this->invalidateCacheEntry();
            }
            
    		function invalidateCacheEntry($photoID=0){
    			// delete cache entry for photo albums of group set in PhotoAlbumAdapter
				$cache	=	ganetCacheProvider::instance()->getCache();
				if ($cache != null) {
					if( 0 < $this->mGroupID ){
						$cache->delete('Group_PhotoAlbums_' . $this->mGroupID);
						$cache->delete('Group_FeaturedPhotoAlbums_' . $this->mGroupID);
						$cache->delete('Photo_Collection_Group_Admin_' . $this->mGroupID);
						$cache->delete('Photo_Collection_Group_Public_' . $this->mGroupID);
					}else{//invalidate cache entry for traveler albums set in Traveler->getPhotoAlbums()
						$cache->delete('Traveler_PhotoAlbums_' . $this->mCreator);
						$cache->delete('Photo_Collection_Traveler_Owner_' . $this->mCreator);
						$cache->delete('Photo_Collection_Traveler_Public_' . $this->mCreator);
						$cache->delete('Photo_Collection_CBTraveler_Public_' . $this->mCreator);
					}
					$cache->delete('PhotoAlbum_PrimaryPhoto_' . $this->mPhotoAlbumID);
					$cache->delete( 'PhotoAlbum_Photos_'  . $this->mPhotoAlbumID);
					$cache->delete('Photo_' . get_class($this) . '_' .$photoID);
				}	
    		}
         
         //added by: joel
            
            function reArrangePhoto($marrphotoID){
            	$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            	
            	foreach($marrphotoID as $index => $photoID){
            			 $sql = "UPDATE tblPhotoAlbumtoPhoto SET position = '$index' WHERE photoID =  '$photoID' ";
		 				$this->mRs->Execute($sql);
            	}
            } 
            
        
        /**
         * Purpose: Returns an array of photos for this photo album.
         * Rows Limit is enabled for this function.
         */
            function getPhotos($rowslimit = null,$countOnly=FALSE){
                $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            	
                if (0 == count($this->mPhotos)){
                    
                    if (null != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    $sql = "SELECT tblPhoto.* from tblPhotoAlbumtoPhoto, tblPhoto " .
                    		"WHERE photoalbumID = '$this->mPhotoAlbumID' " .
                    		"AND tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID " .
                    		"ORDER BY position ASC " . $limitstr;
                    $this->mRs->Execute($sql);
                    
					if( $countOnly ){
						return $this->mRs->Recordcount();
					}

                    if (0 == $this->mRs->Recordcount()){
                        return array();            
                    }
                    else {
                        /*while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                            try {
                                $photo = new Photo($this,0,$recordset);
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                            $this->mPhotos[] = $photo;
                        }*/
						require_once("travellog/model/Class.PhotoType.php");
                        while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                            try {
								switch($recordset["phototypeID"]){
									case PhotoType::$PROFILE	:	require_once("travellog/model/Class.TravelerProfile.php");
																	$context = TravelerProfile::getTravelerContext($recordset["photoID"]);
																	if( $context ){
																		$photo = new Photo($context,0,$recordset);
																		if( $photo->getPhotoID() == $this->mPrimaryPhoto ){
																			$photo->setPrimaryPhoto(1);
																		}
																		$this->mPhotos[] = $photo;
																	}
																	break;
									case PhotoType::$TRAVELLOG	:	require_once("travellog/model/Class.TravelLog.php");
																	$context = TravelLog::getTravelLogContext($recordset["photoID"]);
																	if( $context ){
																		$photo = new Photo($context,0,$recordset);
																		if( $photo->getPhotoID() == $this->mPrimaryPhoto ){
																			$photo->setPrimaryPhoto(1);
																		}
																		$this->mPhotos[] = $photo;
																	}
																	break;
									default						:	/*$grabbed = self::getPhotoalbumContext($recordset["photoID"]);
																	if( $grabbed ){
																		$photo = new Photo($grabbed,0,$recordset);
																	}else{
																		$photo = new Photo($this,0,$recordset);
																	}*/
																	//$photo = new Photo($this,0,$recordset);
																	try{
																		$context = self::getPhotoalbumContext($this->mPhotoAlbumID,$recordset["photoID"]);
																		if( !$context ){
																			$context = $this;
																		}
																		$photo = new Photo($context,0,$recordset);
																		if( $photo->getPhotoID() == $this->mPrimaryPhoto ){
																			$photo->setPrimaryPhoto(1);
																		}
																		$this->mPhotos[] = $photo;
																	}catch(exception $ex){
																		continue;
																	}
																	break;
								}
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                        }
                    }               
                }
                
                return $this->mPhotos;
            }
    
    		function getCountPhotos(){
    			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
    			$sql = "SELECT photoID from tblPhotoAlbumtoPhoto WHERE photoalbumID = '$this->mPhotoAlbumID'";
                $this->mRs->Execute($sql);
    			return $this->mRs->Recordcount();
    		}
    		
    		function getPrimaryPhoto(){				
    			require_once 	'travellog/vo/Class.ganetValueObject.php';
				require_once	'travellog/model/Class.PhotoType.php';	
				require_once	'travellog/model2/photo/Class.ganetPhotoRepo.php'; 					

				$albumID = $this->mPhotoAlbumID;
				$command	=	'PRIMARY_PHOTO';
				$context	=	new ganetValueObject();
				$context->set('PHOTO_ALBUM_ID',$albumID);
				$context->set('PHOTO_TYPE',PhotoType::$PHOTOALBUM);
				$context->set('PHOTO_ALBUM',$this);
				$vo	=	new ganetValueObject();
				$vo->set('CONTEXT',$context);
				$vo->set('COMMAND',$command);		

				$photoRepo	=	ganetPhotoRepo::instance();
				return $photoRepo->retrieve($vo);
	 					
 						/*$sql = "SELECT tblPhotoAlbum.primaryPhoto as photoID, tblPhoto.phototypeID as phototypeID
								FROM tblPhotoAlbum, tblPhoto
								WHERE tblPhotoAlbum.photoalbumID = $albumID AND
									tblPhotoAlbum.primaryPhoto = tblPhoto.photoID
								GROUP BY tblPhotoAlbum.primaryPhoto";
						$this->mRs->Execute($sql);
						
						if( $this->mRs->Recordcount() == 0 ){
							$sql		= "SELECT tblPhoto.photoID as photoID, tblPhoto.phototypeID as phototypeID from tblPhotoAlbumtoPhoto,tblPhoto 
									 		WHERE tblPhotoAlbumtoPhoto.photoID = tblPhoto.photoID AND tblPhoto.primaryphoto = 1
									 		AND tblPhotoAlbumtoPhoto.photoalbumID = '".$this->mPhotoAlbumID."' ";
							$this->mRs->Execute($sql);
							if ($this->mRs->Recordcount() == 0){	// if no photo records yet, set to default value zero
			 					$rand = $this->getRandomPhoto();
								if( $rand ){
									$_photoID = $rand->getPhotoID();
				 					$_phototypeID = $rand->getPhotoTypeID();
								}else{
									$_photoID = 0;
				 					$_phototypeID = 7;
								}
			 				}else{ 
								$_photoID = $this->mRs->Result(0,"photoID");
								$_phototypeID = $this->mRs->Result(0,"phototypeID");
								$sql = "UPDATE tblPhotoAlbum
										SET primaryPhoto = $_photoID
										WHERE photoalbumID = ".$this->mPhotoAlbumID;
								$this->mRs->Execute($sql);
							}
						}else{
							$_photoID = $this->mRs->Result(0,"photoID");
							$_phototypeID = $this->mRs->Result(0,"phototypeID");
						}*/
						
	 			
					/*	require_once("travellog/model/Class.PhotoType.php");
                        try {
							switch($_phototypeID){
								case PhotoType::$PROFILE	:	require_once("travellog/model/Class.TravelerProfile.php");
																$context = TravelerProfile::getTravelerContext($_photoID);
																$this->primphoto = new Photo($context,$_photoID);
																break;
								case PhotoType::$TRAVELLOG	:	require_once("travellog/model/Class.TravelLog.php");
																$context = TravelLog::getTravelLogContext($_photoID);
																$this->primphoto = new Photo($context,$_photoID);
																break;
								default						:	$this->primphoto = new Photo($this,$_photoID);
																break;
							}
						}catch (Exception $e) {
                        	throw $e;
                        }
	 			
	 			return $this->primphoto;*/
	 			
	 		}
    		
    		
    		 // get random photo from the traveler profile
	 		function getRandomPhoto(){
	 			
				$rs = new Recordset(ConnectionProvider::instance()->getConnection());
	 					
	 			if ($this->randphoto == NULL){
	 				
	 				// get the number of photos to choose from
	 				$sql = "SELECT count(photoID) as totalrec from tblPhotoAlbumtoPhoto WHERE photoalbumID = '$this->mPhotoAlbumID' ";
	 				$rs->Execute($sql);
	 				
	 				if ($rs->Result(0,"totalrec") == 0){
	 					try {
	 						$this->randphoto = new Photo($this);
	 					}
						catch (Exception $e) {						  
						   throw $e;
						}
	 				}	 				
	 				else {
	 					
	 					$max = $rs->Result(0,"totalrec") - 1;
						
						$randidx = mt_rand(0 , $max );	 		// the index starting from top row
						
	 					$sql = "SELECT tblPhotoAlbumtoPhoto.photoID as photoID, tblPhoto.phototypeID as phototypeID
								FROM tblPhotoAlbumtoPhoto, tblPhoto
								WHERE tblPhotoAlbumtoPhoto.photoalbumID = '$this->mPhotoAlbumID' AND
									tblPhotoAlbumtoPhoto.photoID = tblPhoto.photoID
							 	ORDER BY tblPhotoAlbumtoPhoto.photoID LIMIT $randidx , 1"  ;
	 					$rs->Execute($sql);
	 					
						while ($recordset = mysql_fetch_array($rs->Resultset())) {
							require_once("travellog/model/Class.PhotoType.php");
	                        try {
								switch($recordset['phototypeID']){
									case PhotoType::$PROFILE	:	require_once("travellog/model/Class.TravelerProfile.php");
																	$context = TravelerProfile::getTravelerContext($recordset['photoID']);
																	$this->randphoto = new Photo($context,$recordset['photoID']);
																	break;
									case PhotoType::$TRAVELLOG	:	require_once("travellog/model/Class.TravelLog.php");
																	$context = TravelLog::getTravelLogContext($recordset['photoID']);
																	$this->randphoto = new Photo($context,$recordset['photoID']);
																	break;
									default						:	//$this->randphoto = new Photo($this,$recordset['photoID']);
																	try{
																		$context = self::getPhotoalbumContext($this->mPhotoAlbumID,$recordset["photoID"]);
																		if( !$context ){
																			$context = $this;
																		}
																		$this->randphoto = new Photo($context,$recordset['photoID']);
																	}catch(exception $ex){
																		$this->randphoto = new Photo($this);
																	}
																	break;
								}
							}catch (Exception $e) {
	                        	throw $e;
	                        }
						}
	
	 					/*while ($recordset = mysql_fetch_array($rs->Resultset())) {
			 				try {
			 					$this->randphoto = new Photo($this, $recordset['photoID'] );
			 				}
							catch (Exception $e) {						  
							   throw $e;
							}
	 					}*/
	 				}
	 			}
	 			
	 			return $this->randphoto;
	 		}
    		
    		/**
	 		 * Added by Joel 
	 		 * March 1 2007
	 		 * To check If Resume has primary photo
	 		 * @return Boolean 
	 		 */
             function checkHasprimaryphoto(){
	 			
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
	
				$sql = "SELECT primaryPhoto as photoID 
						FROM tblPhotoAlbum
						WHERE photoalbumID = ".$this->mPhotoAlbumID;
				$rs->Execute($sql);	
						
				if( $rs->recordcount() == 0 ){
	 				/*$sql = "SELECT primaryphoto from tblPhoto, tblPhotoAlbumtoPhoto " .
		 					"WHERE tblPhotoAlbumtoPhoto.photoID = tblPhoto.photoID " .
		 					"AND primaryphoto =1 " .
		 					"AND tblPhotoAlbumtoPhoto.photoalbumID ='".$this->mPhotoAlbumID."'" ;
					$rs->Execute($sql);*/
					
					$sql		= "SELECT tblPhoto.photoID
									FROM tblPhotoAlbumtoPhoto,tblPhoto 
							 		WHERE tblPhotoAlbumtoPhoto.photoID = tblPhoto.photoID AND tblPhoto.primaryphoto = 1
							 		AND tblPhotoAlbumtoPhoto.photoalbumID = '".$this->mPhotoAlbumID."' ";
					$this->mRs->Execute($sql);
					
					if ($this->mRs->Recordcount() == 0)	// if no photo records yet, set to default value zero
	 					return FALSE;	 					
	 				else{ 
						$_photoID = $this->mRs->Result(0,"photoID");
						$sql = "UPDATE tblPhotoAlbum
								SET primary = $_photoID
								WHERE photoalbumID = ".$this->mPhotoAlbumID;
						$this->mRs->Execute($sql);
					}
				}
				return TRUE;
				
				
				/*if($rs->recordcount() == 0){
					return false;	
				}else{
					return true;
				}*/
            }


			//added by Jul
			function getPhotoIDs(){
				$mRs	= new Recordset(ConnectionProvider::instance()->getConnection());
				
				$sql = "SELECT photoID
						FROM tblPhotoAlbumtoPhoto
						WHERE tblPhotoAlbumtoPhoto.photoalbumID = $this->mPhotoAlbumID
						GROUP BY photoID";
				$mRs->Execute($sql);
				
				$ids = array();
				
				if( $mRs->Recordcount() ){
					while( $row = mysql_fetch_assoc($mRs->Resultset()) ){
						$ids[] = $row["photoID"];
					}
				}
				return $ids;
			}
			
			function getOriginalUploadedIDs(){
				$mRs	= new Recordset(ConnectionProvider::instance()->getConnection());
				
				$sql = "SELECT tblPhotoAlbumtoPhoto.photoID AS photoID
						FROM tblPhotoAlbumtoPhoto, tblPhotoAlbum, tblPhoto
						WHERE tblPhotoAlbum.photoalbumID = $this->mPhotoAlbumID AND
							tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID AND
							tblPhotoAlbum.groupID = $this->mGroupID AND
							tblPhotoAlbumtoPhoto.photoID = tblPhoto.photoID AND
							tblPhoto.phototypeID = 7
						GROUP BY photoID";
				$mRs->Execute($sql);
				//echo $sql;
				$ids = array();
				
				if( $mRs->Recordcount() ){
					while( $row = mysql_fetch_assoc($mRs->Resultset()) ){
						$ids[] = $row["photoID"];
					}
				}
				return $ids;
			}
			
			function removeGrabbedPhoto($photoID=0){
				$mRs	= new Recordset(ConnectionProvider::instance()->getConnection());
				
				$sql = "DELETE FROM tblPhotoAlbumtoPhoto WHERE photoalbumID = '$this->mPhotoAlbumID' AND photoID = $photoID ";    
				$mRs->Execute($sql);
				
				if( $this->mPrimaryPhoto == $photoID && 0 < $photoID ){
					$random = $this->getRandomPhoto();
					if( $random instanceof Photo ){
						$this->setAsPhotoAlbumPrimary($random->getPhotoID());
					}else{
						$this->setAsPhotoAlbumPrimary(0);
					}
				}
				
				$this->invalidateCacheEntry($photoID);
			}
    		
			private $dateCreated = NULL;
			function getDateCreated(){
				if( is_null($this->dateCreated) ){
					require_once("travellog/model/Class.ActivityFeed.php");
					$params = array("FEED_SECTION"		=>	ActivityFeed::PHOTOALBUM,
									"SECTION_ID"		=>	$this->getPhotoAlbumID());
					$feeds = ActivityFeed::getInstances($params);
					if( 0 < $feeds["recordCount"] ){
						$this->dateCreated = $feeds["activityFeeds"][0]->getFeedDate();
					}
				}
				return $this->dateCreated;
			}
			
			function getPhotosUploadedByDay($rowslimit=NULL,$date=NULL,$countOnly=FALSE){
				if ( !is_null($rowslimit) ){
                    $offset   = $rowslimit->getOffset();
                    $rows     = $rowslimit->getRows();
                    $limitstr = " LIMIT " . $offset . " , " . $rows;
                }else{
                    $limitstr  = ''; 
                }
				if( is_null($date) ){
					$date = date("Y-m-d H:i:s");
				}
				$albumID = $this->mPhotoAlbumID;
                $sql = "SELECT SQL_CALC_FOUND_ROWS tblPhoto.* from tblPhotoAlbumtoPhoto, tblPhoto 
                		WHERE CAST(dateuploaded AS DATE) = CAST('$date' AS DATE) 
							AND photoalbumID = $albumID
                			AND tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID
                		ORDER BY dateuploaded DESC " . $limitstr;
                $this->mRs->Execute($sql);
				
				$rs2 = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql2 = "SELECT FOUND_ROWS() AS recordCount";
				$rs2->Execute($sql2);
				$data = mysql_fetch_array($rs2->Resultset());
				
				$count = $data["recordCount"];
				if( $countOnly ){
					return $count;
				}
				$photos = array();
                if (0 < $count){
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                        try {
							$objContext = $this;
							if( PhotoType::$TRAVELLOG == $recordset["phototypeID"] ){
								require_once("travellog/model/Class.TravelLog.php");
								$objContext = TravelLog::getTravelLogContext($recordset["photoID"]);
							}else if( PhotoType::$PHOTOALBUM == $recordset["phototypeID"] ){
								try{
									$objContext = self::getPhotoAlbumContext($this->mPhotoAlbumID,$recordset["photoID"]);
									if( !$objContext ){
										$objContext = $this;
									}
								}catch(exception $e){
									continue;
								}
							}
							
							$photo = new Photo($objContext,0,$recordset);
							if( $photo instanceof Photo ){
								$photos[] = $photo;
							}
                        }catch (Exception $e){
                        }
                    }
                }
				return array("photos"=>$photos,"recordCount"=>$count);
			}
			
			// added by Jul, for featured group photoalbums
			public function setIsFeatured($featured=0){
				$this->mIsFeatured = $featured;
			}
			public function getIsFeatured(){
				return $this->mIsFeatured;
			}
			public function isFeatured(){
				return 1 == $this->mIsFeatured;
			}
			public function Feature($featured=TRUE,$owner=NULL){
				$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
				if( ($this->isFeatured() && $featured) || (!$this->isFeatured() && !$featured) ){
					return;
				}
				$isFeatured = $featured ? 1 : 0;
				
				if( $isFeatured && $owner->getFeaturedAlbumCount() == 3 ){
					return;
				}
					
				$photoalbumID = $this->mPhotoAlbumID;
				$sql = "UPDATE tblPhotoAlbum
						SET isFeatured = $isFeatured
						WHERE photoalbumID = $photoalbumID";
                $this->mRs->Execute($sql);
				$this->invalidateCacheEntry();
			}
			
			//checks if a photo is grabbed from a traveler album
			public function isGrabbedPhoto($photoID=0){
				$albumID = $this->mPhotoAlbumID;
				$rs = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT tblPhotoAlbum.photoAlbumID AS albumID
						FROM tblPhotoAlbum, tblPhotoAlbumtoPhoto
						WHERE tblPhotoAlbum.photoalbumID != $albumID
							AND tblPhotoAlbum.groupID = 0
							AND tblPhotoAlbum.photoAlbumID = tblPhotoAlbumtoPhoto.photoalbumID
							AND tblPhotoAlbumtoPhoto.photoID = $photoID
						LIMIT 0,1";
				$rs->Execute($sql);
				return 0 < $rs->Recordcount() ? TRUE : FALSE;
			}
			
			//added by Jul, make safe setting a grabbed photo as a primary 
			public function setAsPhotoAlbumPrimary($photoID=0){
				$albumID = $this->mPhotoAlbumID;
				$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "UPDATE tblPhotoAlbum
						SET primaryPhoto = $photoID
						WHERE photoalbumID = $albumID";
				$rs->Execute($sql);
				$this->mPrimaryPhoto = $photoID;
				$this->invalidateCacheEntry($photoID);
			}
			
			public static function getPhotoAlbumContext($albumID=0,$photoID=0){
				$rs = new Recordset(ConnectionProvider::instance()->getConnection());
				//get the traveler albumID from which photoID originally belongs
				//if there's a traveler photo album that has this photoID, it's where the photo was grabbed from
				// NOTE: only group albums can grab from a traveler photo album
				$sql = "SELECT tblPhotoAlbum.photoAlbumID AS albumID
						FROM tblPhotoAlbum, tblPhotoAlbumtoPhoto
						WHERE tblPhotoAlbum.photoalbumID != $albumID
							AND tblPhotoAlbum.groupID = 0
							AND tblPhotoAlbum.photoAlbumID = tblPhotoAlbumtoPhoto.photoalbumID
							AND tblPhotoAlbumtoPhoto.photoID = $photoID
						LIMIT 0,1";
				$rs->Execute($sql);
				if( 0 < $rs->Recordcount() ){
					$recordset = mysql_fetch_array($rs->Resultset());
					try{
						$album = new PhotoAlbum($recordset["albumID"]);
						if( $album instanceof PhotoAlbum ){
							return $album;
						}else{
							throw new exception("invalid photo album id: {".$recordset["albumID"]."}");
						}
					}catch(exception $e){
						throw $e;
					}
				}
				return FALSE;
			}
			
			//returns an array of photoalbumIDs associated with its groupID and creatorID
			//which contains this photo
			//particularly used to retrieve group albums which grabbed the photo
			public static function getPhotoAlbumsWhichGrabbedPhoto($photoID=0){
				$albums = array();
				if( 0 == $photoID ){
					return $albums;
				}
				$rs = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT tblPhotoAlbum.photoalbumID AS albumID,
							tblPhotoAlbum.groupID AS groupID,
							tblPhotoAlbum.creator AS travelerID
						FROM tblPhotoAlbum, tblPhotoAlbumtoPhoto
						WHERE tblPhotoAlbum.photoalbumID = tblPhotoAlbumtoPhoto.photoalbumID
							AND tblPhotoAlbumtoPhoto.photoID = $photoID
						GROUP BY albumID";
				$rs->Execute($sql);
				if( 0 < $rs->Recordcount() ){
					while( $data = mysql_fetch_array($rs->Resultset()) ){
						$albums[] = array("albumID"=>$data["albumID"],
											"groupID"=>$data["groupID"],
											"travelerID"=>$data["travelerID"]);
					}
				}
				return $albums;
			}
			
			public static function UpdatePhotoAlbumsWhichGrabbedPhoto($photoID=0){
				if( 0 == $photoID ){
					return;
				}
				$rs = new Recordset(ConnectionProvider::instance()->getConnection());
				$albums = self::getPhotoAlbumsWhichGrabbedPhoto($photoID);
				foreach($albums AS $albumData){
					try{
						$albumID = $albumData["albumID"];
						$album = new PhotoAlbum($albumID);
						if( $album instanceof PhotoAlbum ){
							$sql = "DELETE FROM tblPhotoAlbumtoPhoto WHERE photoalbumID = '$albumID' AND photoID = '$photoID'";
							$rs->Execute($sql);
							if( $album->getPrimary() == $photoID ){//update primary photo if this photoID is the primary photo of the album
								try{
									$random = $album->getRandomPhoto();
									if( $random instanceof Photo ){
										$album->setAsPhotoAlbumPrimary($random->getPhotoID());
									}else{
										$album->setAsPhotoAlbumPrimary(0);
									}
								}catch(exception $ex){
									$album->setAsPhotoAlbumPrimary(0);
								}
							}
							$album->invalidateCacheEntry($photoID);
						}
					}catch(exception $e){
					}
				}
			}
			
			// added by Jul
			public function getURL(){
				if( !is_null($this->getGroupID()) && 0 < $this->getGroupID() ){
					return "/collection.php?type=group&amp;ID=".$this->getGroupID()."&amp;context=photoalbum&amp;genID=".$this->getPhotoAlbumID();
				}else{
					return "/collection.php?type=traveler&amp;ID=".$this->getCreator()."&amp;context=traveleralbum&amp;genID=".$this->getPhotoAlbumID();
				}
			}
			
			public function getFriendlyURL(){
				return $this->getURL();
			}
    }