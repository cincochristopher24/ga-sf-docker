<?php
/*
 * Created on Apr 11, 2008
 * Class.PhotoAlbumAdapter.php
 * 
 * 
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once("Class.Connection.php");
require_once("Class.Recordset.php");
require_once('Cache/ganetCacheProvider.php');

 
class PhotoAlbumAdapter{
	
	private $loguserid = 0;	
	
	
	function setLoguserID($loguserid){
		$this->loguserid = $loguserid;
	}
	
	
	
	function getAlbums(Group $group, $countOnly=FALSE){
		
		$cache 	=	ganetCacheProvider::instance()->getCache();
		$foundInCache 	=	0;
		
		$groupid = $group->getGroupID();

		$key	=	'Group_PhotoAlbums_' . $groupid;
		
		if ($cache != null && $albums =	$cache->get($key )){
			//echo 'Found (PhotoAlbumAdapter->getAlbums) group photo albums in cache <br/> ';
			if ($albums[0] == 'EMPTY'){
				$albums	=	array();				// if we found the contents 'EMPTY'we reassign the array to a blank array
			}	
			return $albums;
		}	
		
		$albums 	= array();
		$GRPAlbum	= $group->getPhotoAlbums();
		
		if($GRPAlbum){
			foreach($GRPAlbum as $i => $palbum ){
				$albums[$i]['creator'] 			= $palbum->getCreator();
				$albums[$i]['context'] 			= "photoalbum";
				$albums[$i]['genID']			= $palbum->getPhotoalbumID();
				$albums[$i]['title']			= $palbum->getTitle();
				//$albums[$i]['numphotos']		= count($palbum->getPhotos());
				$albums[$i]['numphotos']		= count($palbum->getPhotos(NULL,$countOnly));		// note (kgordo): this might not be correct, if so pls use the line above 
				$albums[$i]['publish']			= false;
				$albums[$i]['primaryphoto']		= $palbum->getPrimaryPhoto();
				$albums[$i]['isFeatured']		= $palbum->isFeatured() ? 1 : 0;
			}
		}
		
		if ($cache != null){
			if (!count($albums)) {
				// a blank array put to cache causes null when retreived back
				$cache->set($key, array('EMPTY'), array('EXPIRE'=>3600) );	
			}else {
				$cache->set($key, $albums, array('EXPIRE'=>3600) );
			}
			
		}
		
		
		return $albums;
		
	}
	
	function getFeaturedAlbums(Group $group){
		$cache 	=	ganetCacheProvider::instance()->getCache();
		$foundInCache 	=	0;
		
		$groupid = $group->getGroupID();

		$key	=	'Group_FeaturedPhotoAlbums_' . $groupid;
		
		if ($cache != null && $albums =	$cache->get($key )){
			if ($albums[0] == 'EMPTY'){
				$albums	=	array();
			}	
			return $albums;
		}	
		
		$albums 	= array();
		$GRPAlbum	= $group->getFeaturedPhotoAlbums();
		
		if($GRPAlbum){
			foreach($GRPAlbum as $i => $palbum ){
				$albums[$i]['creator'] 			= $palbum->getCreator();
				$albums[$i]['context'] 			= "photoalbum";
				$albums[$i]['genID']			= $palbum->getPhotoalbumID();
				$albums[$i]['title']			= $palbum->getTitle();
				$albums[$i]['numphotos']		= count($palbum->getPhotos(NULL,$countOnly=FALSE));
				$albums[$i]['publish']			= false;
				$albums[$i]['primaryphoto']		= $palbum->getPrimaryPhoto();
				$albums[$i]['isFeatured']		= $palbum->isFeatured() ? 1 : 0;
			}
		}
		
		if ($cache != null){
			if (!count($albums)) {
				$cache->set($key, array('EMPTY'), array('EXPIRE'=>3600) );	
			}else {
				$cache->set($key, $albums, array('EXPIRE'=>3600) );
			}
			
		}
		
		return $albums;
	}
	
	
}

?>
