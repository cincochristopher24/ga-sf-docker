<?php
	/**
	 *	Created on Jul 27, 2006
	 *	LocationFactory.php
	 *	@package travellog.model
	 *	@author Kerwin Gordo
	 *	Purpose: create the appropriate location subclass (Continent,Country, City, Region, NonPopLocation etc)
	 *	Last Edited By: Cheryl Ivy Q. Go		6 November 2008		Fixed Bugs
	 *					K. Gordo				23 April   2009		Added caching
	 */

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.LocationType.php';
	require_once 'gaexception/Class.InstantiationException.php';
	require_once 'Cache/ganetCacheProvider.php';
	require_once 'travellog/model/Class.State.php';
	require_once 'travellog/model/Class.City.php';
	require_once 'travellog/model/Class.Country.php';
	require_once 'travellog/model/Class.Region.php';
	require_once 'travellog/model/Class.Continent.php';
	require_once 'travellog/model/Class.NewCity.php';

	class LocationFactory {
		private $mConn	= null;
		private $mRs	= null;
		private $mRs2	= null;
		private $cache	= null;
		private $mRepository = array();
 		private static $mInstance;

	 	private function LocationFactory() {
	 		try {
                $this->mConn = new Connection();
                $this->mRs	= new Recordset($this->mConn);
				$this->mRs2	= new Recordset($this->mConn);
				
				$this->cache	=	ganetCacheProvider::instance()->getCache();
            }
            catch (Exception $e) {
               throw $e;
            }
	 	}

	 	/**
	 	 * creates a single instance of this class
	 	 */
	 	public static function instance(){
	 		if (self::$mInstance == null ) {
	 			self::$mInstance = new LocationFactory();
	 		}

	 		return self::$mInstance;	
	 	}
	 	
	 	protected function setLocationToCache($location){
	 		if ($this->cache != null) {
	 			$this->cache->set('Location_' . $location->getLocationID(),$location,86400);	// set to cache for 1 day
	 		}
	 	}
	 	
	 	/**
	 	 * create an instance of location based on location id
	 	 */
	 	public function create($_locId = 0) {
			if (0 < $_locId){
				$foundInCache = false;
				if ($this->cache != null) {
					
					$location = $this->cache->get('Location_' . $_locId );
					if ($location != null){
						//echo 'Location ' . $location->getName() . ' found in cache';
						return $location;	
					}					
				} 
				
				if (!$foundInCache){
									
					$sql = "SELECT locID, locationtypeID FROM tblLocation WHERE locID = " . $_locId;
					$this->mRs->Execute($sql);
	
					if (0 == $this->mRs->Recordcount()){
						throw new InstantiationException("location id " . $_locId . " is not a valid ID");
					}
	
			 		$mLocationType	= $this->mRs->Result(0, 'locationtypeID');
	
		 			switch ($mLocationType){ 			
			 			case LocationType::$CONTINENT :				
						 	require_once 'Class.Continent.php';
			 				break;
			 			case LocationType::$COUNTRY :
							require_once 'Class.Country.php';
	
							$sql2 = "SELECT * FROM GoAbroad_Main.tbcountry WHERE locID = " . $_locId;
			 				$this->mRs2->Execute($sql2);
	
			 				if (0 == $this->mRs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new InstantiationException("invalid location id");	
				 			}
	
			 				$mLocation = new Country;
			 				$mLocation->setLocationID($this->mRs2->Result(0, "locID"));
			 				$mLocation->setCountryID($this->mRs2->Result(0, "countryID"));
			 				$mLocation->setContinentID($this->mRs2->Result(0, "continentID"));
			 				$mLocation->setName( stripslashes($this->mRs2->Result(0, "country")) );
			 				
			 				$this->setLocationToCache($mLocation);
			 				break;
			 			case LocationType::$REGION :
							require_once 'Class.Region.php';
	
							$sql2 = "SELECT * FROM tblRegion WHERE locID = " . $_locId;
							$this->mRs2->Execute($sql2);
	
			 				if (0 == $this->mRs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new InstantiationException("invalid location id");	
				 			}
	
					 		$mLocation = new Region();
					 		$mLocation->setLocationID($this->mRs2->Result(0, "locID"));
			 				$mLocation->setCountryID($this->mRs2->Result(0, "countryID"));
			 				$mLocation->setContinentID($this->mRs2->Result(0, "continentID"));
			 				$mLocation->setName( stripslashes($this->mRs2->Result(0, "name")) );
			 				
			 				$this->setLocationToCache($mLocation);
			 				break;
			 			case LocationType::$CITY :
							require_once 'Class.City.php';
	
			 				$sql2 = "SELECT cityID, locID, regionID, countryID, continentID, city " .
									"FROM GoAbroad_Main.tbcity " .
									"WHERE locID = " . $_locId;
							$this->mRs2->Execute($sql2);
	
			 				if (0 == $this->mRs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new InstantiationException("invalid location id");	
				 			}
	
			 				$mLocation = new City();
			 				$mLocation->setCityID($this->mRs2->Result(0, "cityID"));
			 				$mLocation->setLocationID($this->mRs2->Result(0, "locID"));
			 				$mLocation->setRegionID($this->mRs2->Result(0, "regionID"));
			 				$mLocation->setCountryID($this->mRs2->Result(0, "countryID"));
			 				$mLocation->setContinentID($this->mRs2->Result(0, "continentID"));
			 				$mLocation->setName( stripslashes($this->mRs2->Result(0, "city")) );
			 				
			 				$this->setLocationToCache($mLocation);
			 				
			 				break;
			 			case LocationType::$NON_POP:
							require_once 'Class.NonPopLocation.php';
	
			 				$sql2 = "SELECT * FROM tblNonPopLocation WHERE locID = " . $_locId;
							$this->mRs2->Execute($sql2);
	
			 				if (0 == $this->mRs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new InstantiationException("invalid location id");	
				 			}
	
			 				$mLocation = new NonPopLocation;
			 				$mLocation->setLocationID($this->mRs2->Result(0, "locID"));
			 				$mLocation->setRegionID($this->mRs2->Result(0, "regionID"));
			 				$mLocation->setCountryID($this->mRs2->Result(0, "countryID"));
			 				$mLocation->setContinentID($this->mRs2->Result(0, "continentID")); 	
			 				$mLocation->setName( stripslashes($this->mRs2->Result(0, "name")) );		

			 				$this->setLocationToCache($mLocation);
			 				break;	 				
			 			case LocationType::$NEWCITY:
							require_once 'Class.NewCity.php';
	
							$sql2 = "SELECT cityID, locID, countryID, city FROM tblNewCity where locID = " . $_locId;
							$this->mRs2->Execute($sql2);
	
			 				if (0 == $this->mRs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new InstantiationException("invalid location id");	
				 			}
	
			 				$mLocation = new NewCity;
							$mLocation->setExists(true);
							$mLocation->setApprove(0);
							$mLocation->setContinentID(0);
							$mLocation->setCityID($this->mRs2->Result(0, "cityID"));
							$mLocation->setLocationID($this->mRs2->Result(0, "locID"));
							$mLocation->setCountryID($this->mRs2->Result(0, "countryID"));
							$mLocation->setName( stripslashes($this->mRs2->Result(0, "city")) );
			 				
							$this->setLocationToCache($mLocation);
							
							break;
			 			case LocationType::$STATE:
			 				require_once 'Class.State.php';
	
							$sql2 = "SELECT locID, stateID, state FROM GoAbroad_Main.tbstate WHERE locID = " . $_locId;
							$this->mRs2->Execute($sql2);
	
			 				if (0 == $this->mRs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new InstantiationException("invalid location id");	
				 			}
	
			 				$mLocation = new State;
				  			$mLocation->setCountryID(91);
				  			$mLocation->setContinentID(0);
							$mLocation->setLocationID($this->mRs2->Result(0, "locID"));
				  			$mLocation->setStateID($this->mRs2->Result(0, "stateID"));
				  			$mLocation->setName( stripslashes($this->mRs2->Result(0, "state")) );
				  			
				  			$this->setLocationToCache($mLocation);
				  			
			 				break;
			 		}
					return $mLocation;

				}
			}
		}

		public function createCountry($_countryId){
			$found = false;
			if ($this->cache != null) {
				$country	=	$this->cache->get('Country_' . $_countryId);
				
				if ($country) {
					return $country;
				}
			}
			
			if (!$found){
				$country	=	new Country($_countryId);
				if ($this->cache != null){
					$this->cache->set('Country_' . $_countryId, $country, 86400 );
				}
				return $country;
			}
			
		}
		
		public function createCity($_cityId){
			$found = false;
			if ($this->cache != null) {
				$city	=	$this->cache->get('City_' . $_cityId);
				
				if ($city) {
					//echo 'Got city from cache ' . $city->getName() . '<br/>';
					return $city;
				}
			}
			
			if (!$found){
				$city	=	new Country($_cityId);
				if ($this->cache != null){
					$this->cache->set('City_' . $_cityId, $city, 86400 );
				}
				return $city;
			}
		}
	
	}
?>