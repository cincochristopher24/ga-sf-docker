<?

	/**
	* Class.GaStats.php
	* simulate GoAbroad.com stats
	* @author : marc
	* Sep 22, 2006
	*/
	
	require_once('travellog/model/Class.Listing.php');
	
	
	function generateFLVStat($listing,$studentPreference){
			
		$arrProgramID = array();
		foreach( $studentPreference as $program )
			$arrProgramID[] = $program->getProgramID();
		foreach( $listing->getProgram() as $program){
			if( in_array($program->getProgramID(),$arrProgramID) )
				mysql_query("INSERT INTO goabroad_stats.tblogfulllistingview" . date('Y') . "
						 (logdate,listingID,programID,countryID,clientID,site) 
						 VALUES ('" . date('Y-m-d') . "',
						 	'" . $listing->getListingID() . "',
						 	'" . $program->getProgramID() . "',
						 	'0',
						 	'" . $listing->getClient()->getClientID() . "',
						 	'ga-net')");
		}
	}
	
	function generatePVStat($listingID,$studentPreference){
		
		$listing = new listing($listingID);
		
		$arrProgramID = array();
		foreach( $studentPreference as $program )
			$arrProgramID[] = $program->getProgramID();

		foreach( $listing->getProgram() as $program){
			if( in_array($program->getProgramID(),$arrProgramID) )	{
				$myquery = mysql_query("UPDATE goabroad_stats.tblogpageview" . date('Y') . "
									SET views = views + 1
									WHERE logdate = '" . date('Y-m-d') .  "'
									AND clientID = '" . $listing->getClient()->getClientID() . "'
									AND countryID = 0
									AND programID = '" . $program->getProgramID() . "'
									AND listingID = '" . $listing->getListingID() . "'
									AND site LIKE 'ga-net'");
				if( 0 == mysql_affected_rows()){
					mysql_query("INSERT INTO goabroad_stats.tblogpageview" . date('Y') . "(logdate,clientID,countryID,programID,listingID,views,site)
								 VALUES('" . date('Y-m-d') . "', 
								 		'" . $listing->getClient()->getClientID() . "',
								 		0,
								 		'" . $program->getProgramID() . "',
								 		'" . $listing->getListingID() . "',
								 		1,
								 		'ga-net')");
				}
			}	
		}
	}
	
?>