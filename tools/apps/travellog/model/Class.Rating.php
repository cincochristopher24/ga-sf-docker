<?php
	/*
	 * Class.Rating.php
	 * Created on Jul 12, 2007
	 * created by marc
	 */

	 require_once("Class.Connection.php");
	 require_once("Class.Recordset.php");

	 class Rating{

	 	private $ratingID;
	 	private $rating;
	 	private $conn;
	 	private $rs;

	 	function __construct( $ratingID = 0 ){

	 		$this->conn = new Connection();
	 		$this->rs = new Recordset($this->conn);

	 		if( $ratingID ){
	 			$sqlquery = "SELECT * FROM SocialApplication.tbRating WHERE ratingID = $ratingID";
	 			$this->rs->execute($sqlquery);

	 			if ($this->rs->Recordcount() == 0){
					throw new exception ("Rating does not exist!");
				}

				$this-> ratingID = $this->rs->Result(0, "ratingID");
				$this-> rating = $this->rs->Result(0, "rating");

	 		}
	 	}

	 	function setRating($rating){
	 		$this->rating = trim($rating);
	 	}

	 	function setRatingID($ratingID){
	 		$this->ratingID = $ratingID;
	 	}

	 	function getRating(){
	 		return $this->rating;
	 	}

	 	function getRatingID(){
	 		return $this->ratingID;
	 	}

	 	function create(){

	 		$sqlQuery = "INSERT INTO SocialApplication.tbRating " .
	 				"SET rating = '" . $this->rating . "'";
	 		$this->rs->execute($sqlQuery);
	 		$this->ratingID = mysql_insert_id();

	 	}

	 	function update(){
	 		$sqlQuery = "UPDATE SocialApplication.tbRating " .
	 				"SET rating = '" . $this->rating . "' " .
	 				"WHERE ratingID = " . $this->ratingID;
	 		$this->rs->execute($sqlQuery);
	 	}

	 	function delete(){
	 		$sqlQuery = "DELETE FROM SocialApplication.tbRating " .
	 				"WHERE ratingID = " . $this->ratingID;
	 		$this->rs->execute($sqlQuery);
	 	}

	 	static function getAllRatings(){

	 		$conn = new Connection();
	 		$rs = new Recordset($conn);
	 		$arrRatings = array();

	 		$sqlQuery = "SELECT ratingID, rating FROM SocialApplication.tbRating ORDER BY ratingID DESC";
	 		$rs->execute($sqlQuery);
	 		while( $tmpRow = mysql_fetch_array($rs->Resultset()))
	 			$arrRatings[] = new Rating($tmpRow["ratingID"]);

	 		return $arrRatings;

	 	}

	 }

?>
