<?php

	class SiteAdmin{
		
		// vars for class attributes
 		private $siteAdminID = 0;
		private $username    = NULL;
		private $password    = NULL;
		private $userAccess   = 0;
		
		function SiteAdmin($_siteAdminID=0){
			
			$this->SiteAdminID = $_siteAdminID;
			
			try {
 				$this->conn = new Connection();
				$this->rs   = new Recordset($this->conn);
			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			if ($this->SiteAdminID > 0){
				$sql = "SELECT * from tblSiteAdmin where siteAdminID = '$this->siteAdminID' ";
		 		$this->rs->Execute($sql);
		 		
		 		if ($this->rs->Recordcount() == 0){
 					throw new Exception("Invalid SiteAdmin ID");	// ID not valid so throw exception
 				}
 				
 				// set values to its attributes
				$this->username = stripslashes($this->rs->Result(0,"username")); 
				$this->password = stripslashes($this->rs->Result(0,"password")); 
					
			}	
		}
		
		
		//setters
 		
	 		function setSiteAdminID($_siteAdminID){
	 			$this->siteAdminID = $_siteAdminID;	
	 		}

			function setUserName($_username){
	 			$this->username = $_username;
	 		}
				 		
	 		function setPassword($_password){
	 			$this->password = $_password;
	 		}
	 		
	 	//getters
 		 		
	 		function getSiteAdminID(){
	 			return $this->siteAdminID;
	 		}
	 		
			function getUserName(){
				return $this->username;
			}
			
	 		function getPassword(){
	 			return $this->password;
	 		}
	
			function getUserAccess(){
	 			return $this->userAccess;
	 		}
	    
	    
	 	   function isValidLogin(){
		  	
		    $sql = "SELECT siteAdminID, accesslevel  FROM tblSiteAdmin WHERE username = '$this->username' AND password = '$this->password' ";
		  	$this->rs->Execute($sql);
		  	
		  	if (0 < $this->rs->Recordcount()){	
		  		/**
					$adminID = $this->rs->Result(0,"siteAdminID");
			  		$_SESSION['isSiteAdminLogin'] = '1';
		 			$_SESSION['siteAdminID'] = $adminID;

			  		$crypt = crypt($adminID,CRYPT_BLOWFISH);
			  		setcookie("siteAdminLogin",$crypt);
				*/
				$this->siteAdminID = $this->rs->Result(0,"siteAdminID");
				$this->userAccess = $this->rs->Result(0,"accesslevel");
		  		return true;
		  	}
		  	else
				return false;	
			
		}
				
	}
?>