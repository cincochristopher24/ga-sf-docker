<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: 
 */
 
/**
*	EDITS:
*		chris - Jan 21, 2009 -- added timezone field
* 		chris - Jan 23, 2009 -- cast date field as datetime
**/
    
    require_once("Class.Connection.php");
    require_once("Class.Recordset.php");
	require_once("Class.GaDateTime.php");
    require_once("travellog/model/Class.GroupFactory.php"); 
    
    class GroupEvent{
        
                        
        /**
         * Define variables for Class Member Attributes
         */ 
            private $mConn      = NULL;
            private $mRs        = NULL;
           
            //vars for parent classes
 			private $mGroup    	   = NULL;
 		
            private $mGroupID      = NULL;
            
            private $mGroupEventID = NULL;
            private $mTitle        = NULL;
            private $mDate         = NULL;
            private $mDescription  = NULL;
            private $mLink 		   = NULL; 					// added by K. Gordo for link to the event details
            private $mDisplayPublic  = NULL;
            private $mContext	   = NULL;
			private $mTimeZoneID	= 0;
       
       
        /**
         * Constructor Function for this class
         */
            function GroupEvent($groupEventID = 0){
                
                $this->mGroupEventID = $groupEventID;
                
                try {
                    $this->conn = new Connection();
                    $this->mRs   = new Recordset($this->conn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mGroupEventID){
                    
                    $sql = "SELECT * FROM tblGroupEvent WHERE groupeventID = '$this->mGroupEventID' ";
                    $this->mRs->Execute($sql);
                
                    if (0 == $this->mRs->Recordcount()){
                        throw new Exception("Invalid GroupEventID");        // ID not valid so throw exception
                    }
                    
                    // Sets values to its class member attributes.
                    $this->mTitle          = stripslashes($this->mRs->Result(0,"title"));           
                    $this->mDescription    = stripslashes($this->mRs->Result(0,"description"));       
                    $this->mDate           = $this->mRs->Result(0,"adate");         
                    $this->mDisplayPublic  = $this->mRs->Result(0,"displaypublic");
                    $this->mContext  	   = $this->mRs->Result(0,"context");
					$this->mTimeZoneID	   = $this->mRs->Result(0,'timezoneID');
                    
                    // Sets a value to its parent key.
                    $this->mGroupID   = $this->mRs->Result(0,"groupID");    
                }
            }
           
        /**
         * Setter Functions
         */   
            function setGroupEventID($groupEventID){
               $this->mGroupEventID = $groupEventID;
            }
            
            function setTitle($title){
               $this->mTitle = $title;
            }
           
            function setTheDate($date){
               $this->mDate = $date;
            }
           
            function setDescription($description){
               $this->mDescription = $description;
            }
            
            function setDisplayPublic($displaypublic){
               $this->mDisplayPublic = $displaypublic;
            }

			function setTimeZoneID($tzID=0){
				$this->mTimeZoneID = $tzID;
			}
           
           
        /**
         * Getter Functions
         */  
            function getGroupEventID(){
               return $this->mGroupEventID;
            }
            
            function getTitle(){
                return $this->mTitle;
            }
           
            function getTheDate(){
                return $this->mDate;
            }
           
            function getDescription(){
                return $this->mDescription;
            }

			function getTimeZoneID(){
				return $this->mTimeZoneID;
			}
			
			function getTimeZone(){
				require_once('Class.TimeZone.php');
				$obj = new TimeZone($this->mTimeZoneID);
				return $obj->getTimeZone();
			}
			
            /**
             * Edited By: Aldwin Sabornido
             * Date     : Dec. 8, 2006
             * Purpose  : Just added a context to the link... hehehe!!!
             */
            // added by K. Gordo
            function getLink() {
            	//return $this->mLink;
            	// temp
            	return 'calendar-event.php?gID=' . $this->mGroupID . '&eID=' . $this->mGroupEventID .'&context=1';
            }
            
            function getDisplayPublic(){
                return $this->mDisplayPublic;
            }
        
        	public function getContext() {
				return $this->mContext;
			}
                
        /**
         * Setter Function for the Parent Key
         */
            function setGroupID($groupID){
                $this->mGroupID = $groupID;
            }     
            
     
        /**
         * Getter Function for the Parent Key
         */      
            function getGroupID(){
                return $this->mGroupID;
            }
           
		    
		/**
         * Getter Function for the Parent Object
         */      
            function getGroup(){
                if ($this->mGroup == NULL){
		 			try {
		 				$mGroup = GroupFactory::instance()->create(array($this->mGroupID));
						$this->mGroup = $mGroup[0];
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->mGroup;
            }
                           
        /**
         * CRUD Methods (Create, Update , Delete)
         */
            function Create(){
                $this->mDate  = GaDateTime::dbDateTimeFormat(strtotime($this->mDate));
                $Atitle       = addslashes($this->mTitle);
                $Adescription = addslashes($this->mDescription);
                
                $sql = "INSERT into tblGroupEvent (title, description, adate, groupID, displaypublic, timezoneID) " .
                        "VALUES ('$Atitle', '$Adescription', '$this->mDate', '$this->mGroupID', '$this->mDisplayPublic', '$this->mTimeZoneID')";
                $this->mRs->Execute($sql);       
                $this->mGroupEventID = $this->mRs->GetCurrentID();
            }
            
            function Update(){
                $this->mDate  = GaDateTime::dbDateTimeFormat(strtotime($this->mDate));
                $Atitle       = addslashes($this->mTitle);
                $Adescription = addslashes($this->mDescription);
                
                $sql = "UPDATE tblGroupEvent SET title = '$Atitle', description = '$Adescription', adate = '$this->mDate', displaypublic = '$this->mDisplayPublic', timezoneID = '$this->mTimeZoneID' " .
                        "WHERE groupeventID = '$this->mGroupEventID' ";
                $this->mRs->Execute($sql);
            }
            
            function Delete(){
                
                $sql = "DELETE FROM tblGroupEvent WHERE groupeventID = '$this->mGroupEventID' ";    
                $this->mRs->Execute($sql);   
            }       
                       
    }
    
?>
