<?php
	/**
	 * Created on September 7, 2006
	 * Class.MessageSpace.php 
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.AlertMessage.php';
	require_once 'travellog/model/Class.BulletinMessage.php';
	require_once 'travellog/model/Class.MessageSendableFactory.php';

	class MessageSpace{		
		private $mRs 	= NULL;
		private $mRs2 	= NULL;
		private $mRs3 	= NULL;
		private $mRs4 	= NULL;
		private $mConn	= NULL;

		private $mRecordId			= 0;
		private $mAlertType			= '';
		private $mSendableId		= 0;
		private $mTotalRecords		= 0;
		private $mBulletinMessages	= array();
		private $mAlertMessages		= array();

		/**
		 * @staticvar
		 */

		public static $instance = NULL;
		
		/**
		 * Constructor of class MessageSpace
		 * @return MessageSpace|exception phpDocumentor MessageSpace object or exception
		 */
		 
		function MessageSpace(){				
 			try{ 				
 				$this->mConn = new Connection();
				$this->mRs   = new Recordset($this->mConn);
				$this->mRs2  = new Recordset($this->mConn);
				$this->mRs3  = new Recordset($this->mConn);
				$this->mRs4  = new Recordset($this->mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			return $this;
		}

		/**
		 * Function name: Store
		 * @param Message $_message
		 */
		function Store(Message $_message = NULL){
			// Set values to be inserted to table			
			$text = addslashes($_message-> getText());
			$subject = addslashes($_message-> getTitle());		
			$discriminator = $_message-> getDiscriminator();
			$destination = $_message-> getDestination();
			$source = $_message->getSource()->getSendableID();
			$dateTime = date("Y-m-d H:i:s");

			// Insert values to tblMessages (source is either admin or traveler)			
			$sql = "INSERT INTO tblMessages (discriminator, senderID, title, message, dateCreated) " .
					"VALUES (" . $discriminator . ", " . $source . ", '" . $subject . "', '" . $text . "', '" . $dateTime . "')";
			$this->mRs->Execute($sql);

			// get messageID
			$messId = $this->mRs->GetCurrentId();
			$_message->setMessageID($this->mRs->GetCurrentId());
			
			if (DiscriminatorType::$ALERT == $discriminator){
				$expiry = $_message->getExpiry();

				// insert values to tblMessageToAlert			
				$sql2 = "INSERT INTO tblMessageToAlert (messageID, expiry) " .
						"VALUES (" . $messId . ", '" . $expiry . "')";
				$this->mRs2->Execute($sql2);
			}

			foreach ($destination as $dest){
				// if destination is traveler, get friends of traveler posting the bulletin
				if (0 == strcasecmp(get_class($dest), "traveler")){
					$Friends = $dest->getFriends();
					// add sendableID of friends of sender
					foreach ($Friends as $indFriend){
						$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID) " .
								"VALUES (" . $messId . ", " . $indFriend->getSendableID() . ")";
						$this->mRs3->Execute($sql3);
					}
				}
				// if destination is group
				else{
					// if source is Administrator, get members of group, do not include sendableID of administrator
					if ($_message->getSource() instanceof Group){						
						// get TravelerID of sender
						$travelerID = $_message-> getSource()-> getAdministrator()-> getTravelerID();

						// get members of GROUP including staff as members, do not include sendableID of ADMINISTRATOR
						$sql3 = "SELECT DISTINCT tblTraveler.sendableID " .
									"FROM tblGrouptoTraveler, tblGroup, tblTraveler " .
									"WHERE tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
									"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
									"AND tblTraveler.travelerID <> " . $travelerID . " " .
									"AND tblGroup.sendableID = " . $dest->getSendableID();
						$this->mRs3->Execute($sql3);

						while ($Sendable = mysql_fetch_array($this->mRs3->Resultset())){
							$sql4 = "INSERT INTO tblMessageToAttribute (messageID, recipientID) " .
									"VALUES (" . $messId . ", " . $Sendable['sendableID'] . ")";
							$this->mRs4->Execute($sql4);
						}
					}
					/*
					// if source is Traveler
					// according to new requirements, members can no longer post bulletin -- but still, i'll keep this condition
					else if (0 == strcasecmp(get_class($_message->getSource()), "traveler")){
						// get members of subgroup, include sender, do not include admin, do not include staff
						$sql3 = "SELECT DISTINCT tblTraveler.sendableID " .
									"FROM tblGrouptoTraveler, tblTraveler " .
									"WHERE tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
									"AND tblGrouptoTraveler.groupID = " . $dest->getGroupID() . " " .
									"AND tblTraveler.travelerID <> " . $dest->getAdministrator()->getTravelerID() . " " .
									"AND tblTraveler.travelerID NOT IN " .
									"( " .
										"SELECT sendableID " .
										"FROM tblGrouptoFacilitator	" .
										"WHERE groupID = " . $dest->getGroupID() . " " .
										"AND type = 1 " .
										"AND status = 1 " .
									")";
						// get staff of group
						$sql3 .= "UNION SELECT DISTINCT sendableID " .
									"FROM tblGrouptoFacilitator " .
									"WHERE groupID = " . $dest->getGroupID() . " " .
									"AND type = 1 " .
									"AND status = 1";
						$this->mRs3->Execute($sql3);

						while ($Sendable = mysql_fetch_array($this->mRs3->Resultset())){
							$sql4 = "INSERT INTO tblMessageToAttribute (messageID, recipientID) " .
									"VALUES (" . $messId . ", " . $Sendable['sendableID'] . ")";
							$this->mRs4->Execute($sql4);
						}
					}
					*/
					// if source is staff
					else{
						// get members of group, do not include admin
						$sql3 = "SELECT DISTINCT tblTraveler.sendableID " .
									"FROM tblGrouptoTraveler, tblTraveler " .
									"WHERE tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
									"AND tblGrouptoTraveler.groupID = " . $dest->getGroupID() . " " .
									"AND tblTraveler.travelerID <> " . $dest->getAdministrator()->getTravelerID();
						$this->mRs3->Execute($sql3);

						while ($Sendable = mysql_fetch_array($this->mRs3->Resultset())){
							$sql4 = "INSERT INTO tblMessageToAttribute (messageID, recipientID) " .
									"VALUES (" . $messId . ", " . $Sendable['sendableID'] . ")";
							$this->mRs4->Execute($sql4);
						}
					}
				}
			}

			//$messageTo = SendableFactory::Instance();
			//$messSendable = $messageTo->Create($dest->getSendableID());
			//$_message->setDestination($messSendable);

			$_message->setCreated(date("d M Y, h: i A"));
			$_message->setDestination($dest);

			// sender of message should also have a copy of posted message
			// if DEST: traveler sendableID of sender traveler; if group sendableID of group (admin's copy)
			$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID) " .
					"VALUES (" . $messId . ", " . $dest->getSendableID() . ")";
			$this->mRs3->Execute($sql3);

			$_message->setAttributeID($this->mRs3->GetCurrentID());
		}

		/**
		 * Function name: Delete
		 * @param integer $_messageID
		 * Deletes message from tables
		 */

		function Delete($_attrId = 0, $_discriminator = 0, $_messageId = 0){
			/*
			// Get discriminator of message			
			$sql = "SELECT DISTINCT tblMessages.discriminator, tblMessages.messageID " .
					"FROM tblMessages , tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessageToAttribute.attributeID = " . $_attrID;
			$this->mRs->Execute($sql);

			$discriminator = $this->mRs->Result(0, "discriminator");
			$messageID = $this->mRs->Result(0, "messageID");
			*/

			if (DiscriminatorType::$ALERT == $_discriminator)
				self::DeleteAlert($_attrId, $_messageId);
			else
				self::DeleteBulletin($_messageId);
		}

		/***************************** START: SETTERS *************************************/
		function setTotalRecords($_totalRecords = 0){			
			$this->mTotalRecords = $_totalRecords;
		}
		/******************************* END: SETTERS *************************************/

		/***************************** START: GETTERS *************************************/
		function getTotalRecords(){			
			return $this->mTotalRecords;
		}
		function getAbsolutePosition($method, $sendableID, $recordID){	 		 	
 			switch($method){
 				case "getBulletins":	 		 			                
					return $this->getBulletins($sendableID, NULL, $recordID);
 					break;
 				default:
 					return 0;
 			}	 		 	
 		}
		function getBulletins($_sendableId = 0, $_rowsLimit = NULL, $_recordId = 0){

			$arrMessages = array();

			// check if sendableID is TRAVELER | GROUP | STAFF
			$sendableType = MessageSendableFactory::getSendableType($_sendableId);

			if (0 < strlen($sendableType)){
				$this->mRecordId = $_recordId;
				$this->mSendableId = $_sendableId;

				if (!strcasecmp($sendableType, 'TRAVELER') || !strcasecmp($sendableType, 'STAFF'))
					$arrMessages = $this->getTravelerBulletin($_rowsLimit);
				else if (!strcasecmp($sendableType, 'GROUP'))
					$arrMessages = $this->getGroupBulletin($_rowsLimit);
			}

			return $arrMessages;
		}
		private function getTravelerBulletin($_rowsLimit = NULL){
			try{
				$travelerId = MessageSendableFactory::getTravelerId($this->mSendableId);
			}
			catch (exception $e){
				throw $e;
			}

			if (0 < $this->mRecordId){
				$sql0 = "SET @x := 0;";
				$this->mRs->Execute($sql0);

				$sql = "SELECT _tbTemp1.rowNum AS offset " . 
					"FROM(" .							
						"SELECT @x:=@x+1 as rowNum,  _tbTemp._entryID " .
						"FROM (" .
							"SELECT tblMessageToAttribute.attributeID AS _entryID, tblMessages.dateCreated, tblMessages.messageID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;
					// get messages to recipient as staff
					/*
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID AS _entryID, tblMessages.dateCreated, tblMessages.messageID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGrouptoFacilitator " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND type = 1 " .
								"AND status = 1 " .
								"AND travelerID = " . $travelerId .
							")";
					*/
					// get messages to recipient as administrator
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID AS _entryID, tblMessages.dateCreated, tblMessages.messageID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND discriminator = 1 " .
								"AND isSuspended = 0 " .
								"AND administrator = " . $travelerId .
							")";
					$sql .= ") AS _tbTemp GROUP BY messageID ORDER BY dateCreated DESC " .
					") AS _tbTemp1 " .
				"WHERE _tbTemp1._entryID = " . $this->mRecordId;

				$this->mRs->Execute($sql);

				return $this->mRs->Result(0, "offset");	
			}			
			else{
				// set query limit
				if ($_rowsLimit == NULL)
					$queryLimit = " ";
				else{
					$row = $_rowsLimit->getRows();
					$offset = $_rowsLimit->getOffset();
					$queryLimit = " LIMIT ". $offset . ", " . $row;
				}

				$sql = "SELECT SQL_CALC_FOUND_ROWS * " .
						"FROM (" .
							"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, " .
							"tblMessages.title, tblMessages.message, tblMessages.senderID, tblMessageToAttribute.isRead " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;
					// get messages to recipient as staff
					/*
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, " .
							"tblMessages.title, tblMessages.message, tblMessages.senderID, tblMessageToAttribute.isRead " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGrouptoFacilitator " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND type = 1 " .
								"AND status = 1 " .
								"AND travelerID = " . $travelerId .
							")";
					*/
					// get messages to recipient as administrator
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, " .
							"tblMessages.title, tblMessages.message, tblMessages.senderID, tblMessageToAttribute.isRead " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND discriminator = 1 " .
								"AND isSuspended = 0 " .
								"AND administrator = " . $travelerId .
							")";
					$sql .= ") AS tblTemp " .
					"GROUP BY messageID ORDER BY dateCreated DESC " . $queryLimit;
				$this->mRs->Execute($sql);
			}

			$sql2 = "SELECT FOUND_ROWS() as totalRecords";
			$this->mRs2->Execute($sql2);

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_assoc($this->mRs->Resultset())){
				$message = new BulletinMessage();
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));
				$message->setIsRead($rMessage['isRead']);

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setBulletinRecipient();

				$arrMessage[] = $message;
			}
		
			return $arrMessage;
		}
		private function getGroupBulletin($_rowsLimit = NULL){
			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
				$groupId = $Sendable->getGroupID();
			}
			catch (exception $e){
				throw $e;
			}

			if (0 < $this->mRecordId){
				$sql0 = "SET @x := 0;";
				$this->mRs->Execute($sql0);

				$sql = "SELECT _tbTemp1.rowNum AS offset " . 
					"FROM(" .							
						"SELECT @x:=@x+1 as rowNum,  _tbTemp._entryID " .
						"FROM (" .
							"SELECT tblMessageToAttribute.attributeID AS _entryID, tblMessages.messageID, tblMessages.dateCreated " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 2 " .								
							"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;

						if ($Sendable->getDiscriminator() == Group::ADMIN){
							$sql .= " UNION " .
								"SELECT tblMessageToAttribute.attributeID AS _entryID, tblMessages.messageID, tblMessages.dateCreated " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
								"AND tblMessageToAttribute.trashed = 0 " .
								"AND tblMessages.discriminator = 2 " .
								"AND EXISTS " .
								"(" .
									"SELECT sendableID " .
									"FROM tblGroup " .
									"WHERE sendableID = tblMessageToAttribute.recipientID " .
									"AND isSuspended = 0 " .
									"AND parentID = " . $groupId .
								")";
						}
						$sql .= ") AS _tbTemp GROUP BY messageID ORDER BY dateCreated DESC " .
					") AS _tbTemp1 " .
					"WHERE _tbTemp1._entryID = " . $this->mRecordId;
				$this->mRs->Execute($sql);

				return $this->mRs->Result(0, "offset");
			}
			else{
				// set query limit
				if ($_rowsLimit == NULL)
					$queryLimit = " ";
				else{
					$row = $_rowsLimit->getRows();
					$offset = $_rowsLimit->getOffset();
					$queryLimit = " LIMIT ". $offset . ", " . $row;
				}

				$sql = "SELECT SQL_CALC_FOUND_ROWS * " .
						"FROM (" .
							"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, " .
							"tblMessages.title, tblMessages.message, tblMessages.senderID, tblMessageToAttribute.isRead " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;

						if ($Sendable->getDiscriminator() == Group::ADMIN){
							// if is parent group
							$sql .= " UNION " .
								"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, " .
								"tblMessages.title, tblMessages.message, tblMessages.senderID, tblMessageToAttribute.isRead " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
								"AND tblMessageToAttribute.trashed = 0 " .
								"AND tblMessages.discriminator = 2 " .
								"AND EXISTS " .
								"(" .
									"SELECT sendableID " .
									"FROM tblGroup " .
									"WHERE sendableID = tblMessageToAttribute.recipientID " .
									"AND isSuspended = 0 " .
									"AND parentID = " . $groupId .
								")";
						}
						$sql .= ") AS tblTemp " .
					"GROUP BY messageID " .
					"ORDER BY dateCreated DESC " . $queryLimit;				
				$this->mRs->Execute($sql);

				$sql2 = "SELECT FOUND_ROWS() as totalRecords";
				$this->mRs2->Execute($sql2);

				$arrMessage = array();
				$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

				while($rMessage = mysql_fetch_assoc($this->mRs->Resultset())){
					$message = new BulletinMessage();
					$message->setSendableId($this->mSendableId);
					$message->setCreated($rMessage['dateCreated']);
					$message->setMessageId($rMessage['messageID']);
					$message->setAttributeId($rMessage['attributeID']);
					$message->setText(stripslashes($rMessage['message']));
					$message->setTitle(stripslashes($rMessage['title']));
					$message->setIsRead($rMessage['isRead']);

					$factory = MessageSendableFactory::Instance();
					$_source = $factory->Create($rMessage['senderID']);

					$message->setSource($_source);
					$message->setBulletinRecipient();

					$arrMessage[] = $message;
				}

				return $arrMessage;
			}
		}
		function getAlerts($_sendableId = 0, $_alertType = NULL, $_rowsLimit = NULL){

			$arrMessages = array();

			// check if sendableID is TRAVELER | GROUP | STAFF
			$sendableType = MessageSendableFactory::getSendableType($_sendableId);
			
			if (0 < strlen($sendableType)){
				$this->mAlertType = $_alertType;
				$this->mSendableId = $_sendableId;

				if (!strcasecmp($sendableType, 'TRAVELER') || !strcasecmp($sendableType, 'STAFF'))
					$arrMessages = $this->getTravelerAlert($_rowsLimit);
				else if (!strcasecmp($sendableType, 'GROUP'))
					$arrMessages = $this->getGroupAlert($_rowsLimit);
			}

			return $arrMessages;
		}
		private function getTravelerAlert($_rowsLimit = NULL){
			// get query limit
			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);			
				$queryLimit = " LIMIT " . $offset . ", ". $row;
			}

			// expired alerts			
			if (!strcmp($this->mAlertType, 'expired'))
				$whereClause = " WHERE expiry < CURRENT_DATE() ";

			// if not expired alerts			
			else if (!strcmp($this->mAlertType, 'active'))
				$whereClause = " WHERE expiry >= CURRENT_DATE() ";						
			else
				$whereClause = " ";

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
				$travelerId = $Sendable->getTravelerID();
			}
			catch (exception $e){
				throw $e;
			}

			$sql = "SELECT SQL_CALC_FOUND_ROWS * " .
					"FROM (" .
						"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, tblMessages.title, " .
						"tblMessages.message, tblMessages.senderID, tblMessageToAlert.expiry, tblMessageToAttribute.isRead " .
						"FROM tblMessages, tblMessageToAttribute, tblMessageToAlert " .
						"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
						"AND tblMessageToAlert.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 1 " .
						"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;
					// get messages to recipient as staff
					/*
					if ( $Sendable->isStaff() )
						$sql .= " UNION " .											
								"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, tblMessages.title, " .
								"tblMessages.message, tblMessages.senderID, tblMessageToAlert.expiry, tblMessageToAttribute.isRead " .
								"FROM tblMessages, tblMessageToAttribute, tblMessageToAlert, tblGrouptoFacilitator " .
								"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
								"AND tblMessageToAlert.messageID = tblMessageToAttribute.messageID " .
								"AND tblGrouptoFacilitator.sendableID = tblMessageToAttribute.recipientID " .
								"AND tblMessages.discriminator = 1 " .
								"AND tblGrouptoFacilitator.type = 1 " .
								"AND tblGrouptoFacilitator.status = 1 " .
								"AND tblGrouptoFacilitator.travelerID = " . $travelerId;
					*/
					// get messages to recipient as admin
					if ( $Sendable->isAdministrator() )
						$sql .= " UNION " .
								"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, tblMessages.title, " .
								"tblMessages.message, tblMessages.senderID, tblMessageToAlert.expiry, tblMessageToAttribute.isRead " .
								"FROM tblMessages, tblMessageToAttribute, tblMessageToAlert, tblGroup " .
								"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
								"AND tblMessageToAlert.messageID = tblMessageToAttribute.messageID " .
								"AND tblGroup.sendableID = tblMessageToAttribute.recipientID " .
								"AND tblMessages.discriminator = 1 " .
								"AND tblGroup.isSuspended = 0 " .
								"AND tblGroup.administrator = " . $travelerId;
					$sql .= ") as tblTemp " . $whereClause .
				"GROUP BY messageID " .
				"ORDER BY expiry ASC " . $queryLimit;
			$this->mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this->mRs2->Execute($sql2);

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_assoc($this->mRs->Resultset())){
				$message = new AlertMessage();
				$message->setIsRead($rMessage['isRead']);
				$message->setExpiry($rMessage['expiry']);
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setAlertRecipient();

				$arrMessage[] = $message;
			}

			return $arrMessage;
		}
		private function getGroupAlert($_rowsLimit = NULL){
			// get query limit
			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);			
				$queryLimit = " LIMIT " . $offset . ", ". $row;
			}

			// expired alerts			
			if (!strcmp($this->mAlertType, 'expired'))
				$whereClause = " WHERE expiry < CURRENT_DATE() ";

			// if not expired alerts			
			else if (!strcmp($this->mAlertType, 'active'))
				$whereClause = " WHERE expiry >= CURRENT_DATE() ";						
			else
				$whereClause = " ";

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
				$groupId = $Sendable->getGroupID();
			}
			catch (exception $e){
				throw $e;
			}

			$sql = "SELECT SQL_CALC_FOUND_ROWS * " .
					"FROM (" .
						"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, tblMessages.title, " .
						"tblMessages.message, tblMessages.senderID, tblMessageToAlert.expiry, tblMessageToAttribute.isRead " .
						"FROM tblMessages, tblMessageToAttribute, tblMessageToAlert " .
						"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
						"AND tblMessageToAlert.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 1 " .
						"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;

					if ($Sendable->getDiscriminator() == Group::ADMIN){
						// if parent group, get messages for subgroups
						$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID, tblMessages.dateCreated, tblMessages.messageID, tblMessages.title, " .
							"tblMessages.message, tblMessages.senderID, tblMessageToAlert.expiry, tblMessageToAttribute.isRead " .
							"FROM tblMessages, tblMessageToAttribute, tblMessageToAlert " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessageToAlert.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 1 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND isSuspended = 0 " .
								"AND parentID = " . $groupId .
							")";
					}
					$sql .= ") as tblTemp " . $whereClause .
				"GROUP BY messageID " .
				"ORDER BY expiry ASC " . $queryLimit;
			$this->mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this->mRs2->Execute($sql2);

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_assoc($this->mRs->Resultset())){
				$message = new AlertMessage();
				$message->setIsRead($rMessage['isRead']);
				$message->setExpiry($rMessage['expiry']);
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setAlertRecipient();

				$arrMessage[] = $message;
			}

			return $arrMessage;
		}
		/******************************** END: GETTERS *************************************/
		
		/**************************** START: STATIC FUNCTIONS ******************************/
		private static function DeleteAlert($_attrID = 0, $_messId = 0){
			try{ 				
 				$mConn = new Connection();
				$mRs = new Recordset($mConn);
				$mRs2 = new Recordset($mConn);
			}	
			catch (exception $e){				   
			   throw $e;
			}

			// Delete rows in tblMessageToAttribute			
			$sql = "DELETE FROM tblMessageToAttribute WHERE attributeID = " . $_attrID;
			$mRs->Execute($sql);

			// check if there are still attributeIDs linked to messageID
			$sql2 = "SELECT attributeID FROM tblMessageToAttribute WHERE messageID = " . $_messId;
			$mRs2->Execute($sql2);

			if (0 == $mRs2->Recordcount()){
				// Delete row in tblMessages
				$sql = "DELETE FROM tblMessages WHERE messageID = " . $_messId;
				$mRs->Execute($sql);

				// Delete row in tblMessageToAlert
				$sql = "DELETE FROM tblMessageToAlert WHERE messageID = " . $_messId;
				$mRs->Execute($sql);
			}
		}
		private static function DeleteBulletin($_messId = 0){
			try{ 				
 				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
				$mRs2 = new Recordset($mConn);
			}	
			catch (exception $e){				   
			   throw $e;
			}

			// Delete row in tblMessages
			$sql = "DELETE FROM tblMessages WHERE messageID = " . $_messId;
			$mRs->Execute($sql);

			// Delete rows in tblMessageToAttribute			
			$sql2 = "DELETE FROM tblMessageToAttribute WHERE messageID = " . $_messId;
			$mRs2->Execute($sql2);
		}
		public static function isOwner($_attributeId = 0, $_sendableId = 0, $_discriminator = ''){
			// if ALERT
			if ($_discriminator == DiscriminatorType::$ALERT || !strcasecmp($_discriminator, 'alert'))
				return self::isAlertOwner($_attributeId, $_sendableId);
			// if BULLETIN
			else
				return self::isBulletinOwner($_attributeId, $_sendableId);
		}
		private static function isAlertOwner($_attributeId = 0, $_sendableId = 0){
			try{ 				
				$mConn = new Connection();
				$mRs   = new Recordset($mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($_sendableId);
			}
			catch (exception $e){
				throw $e;
			}

			if ($Sendable){
				$sql = "SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
						"AND tblMessages.discriminator = 1 " .
						"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
						"AND tblMessageToAttribute.recipientID = " . $_sendableId;

				// if TRAVELER
				if (!strcasecmp(get_class($Sendable), 'Traveler')){
					$travelerId = $Sendable->getTravelerID();

					/*
					// get messages to recipient as staff
					if ( $Sendable->isStaff() )
						$sql .= " UNION " .
								"SELECT tblMessageToAttribute.attributeID " .
								"FROM tblMessages, tblMessageToAttribute, tblGrouptoFacilitator " .
								"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
								"AND tblGrouptoFacilitator.sendableID = tblMessageToAttribute.recipientID " .
								"AND tblMessages.discriminator = 1 " .
								"AND tblGrouptoFacilitator.type = 1 " .
								"AND tblGrouptoFacilitator.status = 1 " .
								"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
								"AND tblGrouptoFacilitator.travelerID = " . $travelerId;
					*/
					// get messages to recipient as admin
					if ( $Sendable->isAdministrator() )
						$sql .= " UNION " .
								"SELECT tblMessageToAttribute.attributeID " .
								"FROM tblMessages, tblMessageToAttribute, tblGroup " .
								"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
								"AND tblGroup.sendableID = tblMessageToAttribute.recipientID " .
								"AND tblGroup.isSuspended = 0 " .
								"AND tblMessages.discriminator = 1 " .
								"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
								"AND tblGroup.administrator = " . $travelerId;
				}
				else{
					if ($Sendable->getDiscriminator() == Group::ADMIN){
						$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 1 " .
							"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND isSuspended = 0 " .
								"AND parentID = " . $Sendable->getGroupID() .
							")";
					}
				}
				$mRs-> Execute($sql);

				if (0 < $mRs->Recordcount())
					return true;
				else
					return false;
			}
			else
				return false;
		}
		private static function isBulletinOwner($_attributeId = 0, $_sendableId = 0){
			try{ 				
				$mConn = new Connection();
				$mRs   = new Recordset($mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($_sendableId);
			}
			catch (exception $e){
				throw $e;
			}

			if ($Sendable){
				$sql = "SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
						"AND tblMessages.discriminator = 2 " .
						"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
						"AND tblMessageToAttribute.recipientID = " . $_sendableId;

				// if TRAVELER
				if (!strcasecmp(get_class($Sendable), 'Traveler')){
					$travelerId = $Sendable->getTravelerID();

					/*
					// get messages to recipient as staff
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGrouptoFacilitator " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND type = 1 " .
								"AND status = 1 " .
								"AND travelerID = " . $travelerId.
							")";
					*/

					// get messages to recipient as administrator
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND isSuspended = 0 " .
								"AND administrator = " . $travelerId.
							")";
				}
				else{
					if ($Sendable->getDiscriminator() == Group::ADMIN){
						$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND isSuspended = 0 " .
								"AND parentID = " . $Sendable->getGroupID() .
							")";
					}
				}
				$mRs->Execute($sql);

				if (0 < $mRs->Recordcount())
					return true;
				else
					return false;
			}
			else
				return false;
		}
		static function addBulletinsPostedByAdvisor($_group = null, $_traveler = null){
			try{ 				
 				$mConn = new Connection();
				$mRs   = new Recordset($mConn);
				$mRs2  = new Recordset($mConn);
				$mRs3  = new Recordset($mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$sql = "SELECT tblMessages.messageID " .
					"FROM tblMessages, tblMessageToAttribute, tblInviteList, tblGroup " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblGroup.groupID = tblInviteList.groupID " .
					"AND tblMessageToAttribute.recipientID = tblGroup.sendableID " .
					"AND tblMessages.dateCreated >= tblInviteList.inviteDate " .
					"AND tblMessages.discriminator = 2 " .
					"AND tblInviteList.groupID = " . $_group->getGroupID();

			// if subgroup, include messages for parent
			if ( $_group->isSubGroup() && !strcasecmp(get_class($_group), 'AdminGroup') ){
				$sql .= " UNION " .
						"SELECT tblMessages.messageID " .
						"FROM tblMessages, tblMessageToAttribute, tblInviteList, tblGroup " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblGroup.groupID = tblInviteList.groupID " .
						"AND tblMessageToAttribute.recipientID = tblGroup.sendableID " .
						"AND tblMessages.dateCreated >= tblInviteList.inviteDate " .
						"AND tblMessages.discriminator = 2 " .
						"AND tblInviteList.groupID = " . $_group->getParent()->getGroupID();
			}

			$mRs->Execute($sql);
			$arrBulletin = array();

			while($resMessage = mysql_fetch_assoc($mRs->Resultset())){
				// check if same message is already posted for new member traveler
				$sql2 = "SELECT attributeID " .
						"FROM tblMessageToAttribute " .
						"WHERE messageID = " . $resMessage['messageID'] . " " .
						"AND recipientID = " . $_traveler->getSendableID();
				$mRs2->Execute($sql2);

				if (0 == $mRs2->Recordcount()){
					// insert to tblMessageToAttribute
					$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID) " .
							"VALUES (" . $resMessage['messageID'] . ", " . $_traveler->getSendableID() . ")";
					$mRs3->Execute($sql3);
				}
			}
		}
		/****************************** END: STATIC FUNCTIONS ******************************/
	}	
?>