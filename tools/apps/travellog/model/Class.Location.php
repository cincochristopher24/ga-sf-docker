<?php
/**
 * Created on Jul 26, 2006
 * Location - an abstract class to represent a geographical location
 * @abstract
 * @author Kerwin Gordo
 * 	
 */
 
  require_once('Class.Coordinates.php');
 
 class Location {
 	protected $locationID;
 	protected $name;
 	protected $coords;					// the mapping coordinates of this location
 	
 	/*
 	 * crud functions
 	 */
 	 
 	 
 	  	 
 	 /*
 	  * miscellaneous (getter/setter,etc)
 	  */
 	  
 	  function setLocationID($locationID) {$this->locationID = $locationID;}
 	  
 	  function getLocationID() {return $this->locationID;}
 	  
 	  function setName($name) {
 	  	$this->name = $name;
 	  }
 	  
 	  function getName(){
 	  	return $this->name;
 	  }
 	  
 	  function getCoordinates() {
 	  	if ($this->coords == null) {
 	  		$this->coords = new Coordinates($this->locationID);
 	  	} 
 	  	return $this->coords; 	  	
 	  }
 	  
 	   	 
 	 /*
 	  * abstract methods
 	  */
 	  	// methods for getting bounded locations
 	  	function getCountries() {}	
 		function getCities() {}
 		function getRegions() {}
 		function getStates() {} // Added By: Aldwin S. Sabornido
 		function getNonPopLocations() {}
 		
 		// methods for getting bounding location
 		function getRegion() {}
 		function getState() {} // Added By: Aldwin S. Sabornido
 		function getCountry() {}
 		function getContinent() {}
 		 			 	
 }
 
 
?>
