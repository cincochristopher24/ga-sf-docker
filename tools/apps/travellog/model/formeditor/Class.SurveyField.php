<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	require_once('Class.InputControl.php');
	
	class SurveyField
	{
		const TEXTBOX 		= 0;		
		const CHECKBOX 		= 1;
		const RADIO			= 2;
		const COMBO			= 3;
		const SECTION_BREAK	= 4;
		
		const VERTICAL_ORIENTATION = 0;
		const HORIZONTAL_ORIENTATION = 1;
		
		const NOT_REQUIRED 	= 0;
		const REQUIRED		= 1; 
		
		const FORM			= 0;
		const INPUT_FIELD 	= 1;
		
		private $mSurveyFieldID;
		private $mParentType;
		private $mParentID;
		private $mFieldType;
		private $mCaption;
		private $mRequiredStatus;
		private $mOrientation;		
		private $mPosition;
		private $mInputs = null;
		
		private $mDb = null;

		function __construct($params=0){			
			$this->clearValues();
			$this->mDb = new dbHandler();		
			$this->mSurveyFieldID = (is_array($params) && array_key_exists('surveyFieldID',$params)?$params['surveyFieldID']:(is_numeric($params)?$params:0));
			if($this->mSurveyFieldID != 0){
				$qry = 	'SELECT surveyFieldID, parentType, parentID, fieldType, caption, requiredStatus, orientation, position ' .
						'FROM tblSurveyField ' .
						'WHERE surveyFieldID = '.$this->mSurveyFieldID;
				$rs = new iRecordset($this->mDb->execQuery($qry));				
				if(!$rs->retrieveRecordCount()){
					throw new exception('Invalid Survey Field ID ['.$this->mSurveyFields.'] in object of type Survey Field.');
				}							
				$this->mSurveyFieldID 	= $rs->getSurveyFieldID(0);
				$this->mParentType		= $rs->getParentType(0);
				$this->mParentID		= $rs->getParentID(0);
				$this->mFieldType		= $rs->getFieldType(0);
				$this->mCaption			= $rs->getCaption(0);
				$this->mRequiredStatus	= $rs->getRequiredStatus(0);
				$this->mOrientation		= $rs->getOrientation(0);
				$this->mPosition		= $rs->getPosition(0);																		
			}
		}				
		public function init($params){
			$this->mSurveyFieldID 	= array_key_exists('surveyFieldID',$params)?$params['surveyFieldID']:$this->mSurveyFieldID;
			$this->mParentType 		= array_key_exists('parentType',$params)?$params['parentType']:$this->mParentType;
			$this->mParentID 		= array_key_exists('parentID',$params)?$params['parentID']:$this->mParentID;
			$this->mFieldType 		= array_key_exists('fieldType',$params)?$params['fieldType']:$this->mFieldType;
			$this->mCaption 		= array_key_exists('caption',$params)?$params['caption']:$this->mCaption;
			$this->mRequiredStatus 	= array_key_exists('requiredStatus',$params)?$params['requiredStatus']:$this->mRequiredStatus;
			$this->mOrientation 	= array_key_exists('orientation',$params)?$params['orientation']:$this->mOrientation;
			$this->mPosition 		= array_key_exists('position',$params)?$params['position']:$this->mPosition;							
			if(array_key_exists('inputs',$params) && is_array($params['inputs'])){
				foreach($params['inputs'] as $iInput){
					if(!is_null($iInput)){
						$input = InputControl::createControl($iInput['surveyFieldInputID'],$iInput['inputType']);
						$input->init($iInput);
						$this->mInputs[] = $input;
					}
				}	
			}			
		}		
		
		/*** SETTERS ***/				
		public function setParentID($arg){
			$this->mParentID = $arg;
		}
		
		public function setFieldType($arg){
			$this->mFieldType = $arg;
		}
				
		public function setCaption($arg){
			$this->mCaption = $arg;
		}
		
		public function setAsRequired(){
			$this->mRequiredStatus = self::REQUIRED;
		}
		
		public function setAsNotRequired(){
			$this->mRequiredStatus = self::NOT_REQUIRED;
		}
		
		public function setAsFormField(){
			$this->mParentType = self::FORM;
		}
		
		public function setAsInputField(){
			$this->mParentType = self::INPUT_FIELD;
		}
		
		public function setOrientationAsVertical(){
			$this->mOrientation = self::VERTICAL_ORIENTATION;
		}
		
		public function setOrientationAsHorizontal(){
			$this->mOrientation = self::HORIZONTAL_ORIENTATION;
		}
		/*** GETTERS ***/
		
		public function getSurveyFieldID(){
			return $this->mSurveyFieldID;
		}
		
		public function getParentID(){
			return $this->mParentID;
		}
		
		public function getFieldType(){
			return $this->mFieldType;
		}
		
		public function getCaption(){
			return $this->mCaption;
		}
		
		public function isRequired(){
			return ($this->mRequiredStatus == self::REQUIRED);
		}
		
		public function getPosition(){
			return $this->mPosition;
		}
		
		public function isOrientationVertical(){
			return $this->mOrientation == self::VERTICAL_ORIENTATION;
		}
		
		public function isOrientationHorizontal(){
			return $this->mOrientation == self::HORIZONTAL_ORIENTATION;	
		}
		
		public function isFormField(){
			return ($this->mParentType == self::FORM);
		}
		
		public function isInputField(){
			return ($this->mParentType == self::INPUT_FIELD);	
		}
		
		public function isAnswerable(){
			return (self::SECTION_BREAK != $this->getFieldType() );
		}
		
		public function isWithOptions(){
			return (self::RADIO == $this->getFieldType() || self::CHECKBOX == $this->getFieldType() || self::COMBO == $this->getFieldType() );
		}
		
		public function getOptions(){
			$options = array();
			if($this->isWithOptions()){
				if(self::RADIO == $this->getFieldType() || self::CHECKBOX == $this->getFieldType()){
					$fldInputs = $this->getFieldInputs();
					foreach($fldInputs as $iInput){
						$options[] =  $iInput->getAttribute('text');
					}
				}
				else{
					$fldInput = $this->getFieldInputs();
					$options = $fldInput[0]->getAttribute('options');
				}
			}
			return $options;
		}
		
		/*** CRUDE ***/
		public function save(){			
			if(0 != $this->mSurveyFieldID){//edit
				$qry =	'UPDATE tblSurveyField SET ' .									
							'`parentType` 					='.ToolMan::makeSqlSafeString( $this->mParentType ).', '.
							'`parentID` 					='.ToolMan::makeSqlSafeString( $this->mParentID ).', '.
							'`fieldType` 					='.ToolMan::makeSqlSafeString( $this->mFieldType ).', '.
							'`caption`						='.ToolMan::makeSqlSafeString( $this->mCaption ).', '.		
							'`requiredStatus`				='.ToolMan::makeSqlSafeString( $this->mRequiredStatus ).', '.
							'`orientation`					='.ToolMan::makeSqlSafeString( $this->mOrientation ).', '.
							'`position`						='.ToolMan::makeSqlSafeString( $this->mPosition ).' '.
						'WHERE surveyFieldID ='.ToolMan::makeSqlSafeString( $this->mSurveyFieldID );
				$this->mDb->execQuery($qry);				
			}
			else{//add
				$this->mDateCreated = date("Y-m-d G:i:s");
				$qry = 	'INSERT INTO tblSurveyField(' .							
							'`parentType`,' .
							'`parentID`,' .
							'`fieldType`,' .
							'`caption`,' .							
							'`requiredStatus`,' .
							'`orientation`,' .
							'`position`' .							
						') ' .
						'VALUES (' .							
							ToolMan::makeSqlSafeString( $this->mParentType ).', '.
							ToolMan::makeSqlSafeString( $this->mParentID ).', '.
							ToolMan::makeSqlSafeString( $this->mFieldType ).', '.
							ToolMan::makeSqlSafeString( $this->mCaption ).', '.
							ToolMan::makeSqlSafeString( $this->mRequiredStatus ).', '.
							ToolMan::makeSqlSafeString( $this->mOrientation ).', '.
							ToolMan::makeSqlSafeString( $this->mPosition ).
						')';
				$this->mDb->execQuery($qry);
				$this->mSurveyFieldID = $this->mDb->getLastInsertedID();
			}
		}
		
		public function getData(){
			$ar = array();
			$ar['surveyFieldID'] = $this->mSurveyFieldID;
			$ar['parentType'] = $this->mParentType;
			$ar['parentID'] = $this->mParentID;
			$ar['fieldType'] = $this->mFieldType;
			$ar['caption'] = $this->mCaption;
			$ar['requiredStatus'] = $this->mRequiredStatus;
			$ar['orientation'] = $this->mOrientation;
			$ar['position'] = $this->mPosition;
			$ar['inputs'] = array();
			$inputs = $this->getFieldInputs();			
			foreach($inputs as $ipt){
				$ar['inputs'][] = $ipt->getData();
			}
			return $ar;	
		}
					
		public function delete(){
			$qry = 	'DELETE FROM tblSurveyField ' .
					'WHERE `surveyFieldID` ='.ToolMan::makeSqlSafeString($this->mSurveyFieldID);
			$this->mDb->execQuery($qry);			
			//delete the field inputs
			$fieldInputs = $this->getFieldInputs();
			foreach($fieldInputs as $fldInput){
				$fldInput->delete();
			}
			$this->clearValues();
		}
		
		public function clearValues(){
			$this->mSurveyFieldID	= 0;
			$this->mParentType		= 0; 
			$this->mParentID		= 0;
			$this->mFieldType		= 0;
			$this->mCaption			= null;
			$this->mRequiredStatus	= 0;
			$this->mOrientation 	= 0;
			$this->mPosition		= 0;
		}

		public function getFieldInputs(){		
			if(is_array($this->mInputs)){
				return $this->mInputs;
			}
			else{
				require_once('Class.InputControl.php');
				return InputControl::getFieldInputs($this->mSurveyFieldID);
			}
		}
		
		public static function getFormFields($formID){
			$db = new dbHandler();
			$qry = 	'SELECT surveyFieldID ' .
					'FROM tblSurveyField ' .
					'WHERE parentType = '.self::FORM.' '.
						'AND parentID = '.ToolMan::makeSqlSafeString($formID) .' '.
					'ORDER By position';			
			$rs = new iRecordset($db->execQuery($qry));
					
			$ar = array();
			if($rs->retrieveRecordCount() > 0){				
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){					
					$ar[] = new SurveyField($rs->getSurveyFieldID());
				}							
			}
			return $ar;
		}
		
		public static function getInputFields($inputID){
			$db = new dbHandler();
			$qry = 	'SELECT surveyFieldID ' .
					'FROM tblSurveyField ' .
					'WHERE parentType = '.self::INPUT_FIELD.' '.
						'AND parentID = '.ToolMan::makeSqlSafeString($inputID) . ' ' .
					'ORDER BY position';			
			$rs = new iRecordset($db->execQuery($qry));			
			$ar = array();
			if($rs->retrieveRecordCount() > 0){				
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){					
					$ar[] = new SurveyField($rs->getSurveyFieldID());
				}
			}
			return $ar;
		} 
	}
?>