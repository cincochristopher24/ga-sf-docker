<?php
	require_once('Class.Decorator.php');
	require_once('Class.SurveyParticipant.php');
	
	class SurveyFormAnswerDecorator extends Decorator
	{
		private $mSurveyParticipant = null;
		
		public function __construct($param){
			$this->mSurveyParticipant = new SurveyParticipant($param->getSurveyParticipantID());
			parent::__construct($param);
		}
		
		public function getGroupID(){
			return $this->mSurveyParticipant->getGroupID();
		}
	}
?>