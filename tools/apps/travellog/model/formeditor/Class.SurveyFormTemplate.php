<?php
/*
 * Created on May 15, 2007
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once('Class.SurveyForm.php');
class SurveyFormTemplate extends SurveyForm
{
	public function __construct($params=0){		
		parent::__construct($params);
		$this->setAsTemplate();
		if(0==$this->mSurveyFormID){
			$this->setName('New Template');
		}
	}
	
	public function mold($frm){
		$tplData = $this->getData();
		$tplData['surveyFormID'] = 0;
		$tplData['dateCreated']  = date("Y-m-d G:i:s");
		$tplData['isTemplate']	 = 0; 
		foreach($tplData['fields'] as $key => $val){
			$tplData['fields'][$key]['surveyFieldID'] = 0;
			$tplData['fields'][$key]['parentID'] = 0;
			foreach($tplData['fields'][$key]['inputs'] as $key1 => $val1){
				$tplData['fields'][$key]['inputs'][$key1]['surveyFieldID']=0;
				$tplData['fields'][$key]['inputs'][$key1]['surveyFieldInputID']=0;				
			}
		}		
		$frm->init($tplData);		
	}
	
	public static function getGroupSurveyFormTemplates($gID=0){
		$db = new dbHandler();
		$qry = 	'SELECT surveyFormID ' .
				'FROM tblSurveyForm ' .
				'WHERE groupID = '.ToolMan::makeSqlSafeString( $gID ).' ' .
					'AND isTemplate = 1 ' .
				'ORDER BY dateCreated';
		$rs = new iRecordset($db->execQuery($qry));
		$ar = array();
		if(0<$rs->retrieveRecordCount()){
			for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
				$ar[] = new SurveyForm($rs->getSurveyFormID());
			}
		}
		return $ar;
	}
	
	public static function isExists($id){
		$db = new dbHandler();
		$qry = 	'SELECT surveyFormID ' .
				'FROM tblSurveyForm ' .
				'WHERE surveyFormID = '.ToolMan::makeSqlSafeString( $id ) . ' ' .
					'AND isTemplate = 1 ';
		$rs = new iRecordset($db->execQuery($qry));
		return $rs->retrieveRecordCount() > 0;
	}
}
?>