<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.GaString.php');
	
	class SurveyFieldAnswerValue
	{
		private $mSurveyFieldAnswerID;
		private $mSurveyFormAnswerID;
		private $mValue;
		
		private $mDb;
		
		public function __construct($param=null,$data=null){
			$this->clear();
			$this->mDb = new dbHandler();
			if(!is_null($param)){
				$sql =	'SELECT `surveyFieldAnswerValueID`, `surveyFieldAnswerID`, `value` ' .
						'FROM `tblSurveyFieldAnswerValue` ' .
						'WHERE `surveyFieldAnswerValueID` = '.GaString::makeSqlSafe($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				
				if($rs->retrieveRecordCount() > 0){
					$this->initialize($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid SurveyFieldAnswerValueID '.$param.' passed to object of type SurveyFieldAnswerValue.');
				}
			}
			if(is_array($data)){
				$this->initialize($data);
			}			
		}
		
		private function clear(){
			$this->setSurveyFieldAnswerValueID(0);
			$this->setSurveyFieldAnswerID(0);
			$this->setValue(null);			
		}
		
		private function initialize($data){
			$this->setSurveyFieldAnswerValueID($data['surveyFieldAnswerValueID']);
			$this->setSurveyFieldAnswerID($data['surveyFieldAnswerID']);
			$this->setValue($data['value']);
		}
		
		/*** SETTERS ***/
		private function setSurveyFieldAnswerValueID($param=null){
			$this->mSurveyFieldAnswerValueID = $param;
		}
		
		public function setSurveyFieldAnswerID($param=null){
			$this->mSurveyFieldAnswerID = $param;
		}
		
		public function setValue($param=null){
			$this->mValue = $param;
		}
		
		/*** GETTERS ***/
		public function getSurveyFieldAnswerValueID(){
			return $this->mSurveyFieldAnswerValueID;
		}
		
		public function getSurveyFieldAnswerID(){
			return $this->mSurveyFieldAnswerID;
		}
		
		public function getValue(){
			return $this->mValue;
		}
		
		/*** CRUDE ***/
		
		public function save(){
			if(0==$this->getSurveyFieldAnswerValueID()){//add
				$sql =	'INSERT INTO `tblSurveyFieldAnswerValue`(' .
							'`surveyFieldAnswerID`,' .
							'`value`' .
						')VALUES(' .
							GaString::makeSqlSafe($this->getSurveyFieldAnswerID()).',' .
							GaString::makeSqlSafe($this->getValue()).
						')';
				$this->mDb->execQuery($sql);
				$this->setSurveyFieldAnswerValueID($this->mDb->getLastInsertedID());
			}
			else{//update
				$sql = 	'UPDATE `tblSurveyFieldAnswerValue` SET ' .
							'`surveyFieldAnswerID` 	= '.GaString::makeSqlSafe($this->getSurveyFieldAnswerID()).',' .
							'`value` 				= '.GaString::makeSqlSafe($this->getValue()).' ' .
						'WHERE `surveyFieldAnswerValueID`= '.GaString::makeSqlSafe($this->getSurveyFieldAnswerValueID());
				$this->mDb->execQuery($sql);
			}
		}
		
		public function delete(){
			$sql = 	'DELETE FROM tblSurveyFieldAnswerValue ' .
					'WHERE `surveyFieldAnswerValueID` = '.GaString::makeSqlSafe($this->getSurveyFieldAnswerValueID());
			$this->mDb->execQuery($sql);
			$this->clear();
		}
		
		public static function getAnswerValuesBySurveyFieldAnswerID($param){
			$db = new dbHandler();
			$sql =	'SELECT `surveyFieldAnswerValueID`, `surveyFieldAnswerID`, `value` ' .
					'FROM `tblSurveyFieldAnswerValue` ' .
					'WHERE `surveyFieldAnswerID` = '.GaString::makeSqlSafe($param);
			$rs = new iRecordset($db->execQuery($sql));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyFieldAnswerValue(null,$rs->retrieveRow());
				}
			}
			return $ar;
		}
		
		public static function deleteAnswerValuesBySurveyFieldAnswerID($param){
			$db = new dbHandler();
			$sql =	'DELETE FROM `tblSurveyFieldAnswerValue` ' .
					'WHERE `surveyFieldAnswerID` = '.GaString::makeSqlSafe($param);
			$db->execQuery($sql);
			return true;
		}
		
		
	}
?>