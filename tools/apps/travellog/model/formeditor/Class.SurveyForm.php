<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	require_once('Class.SurveyField.php');	
	require_once('Class.SurveyParticipant.php');
	require_once('Class.GaString.php');
	
	class SurveyForm
	{
		const UNLOCKED	= 0;
		const LOCKED	= 1;		
		const HIDDEN_PARTICIPANTS 	= 0;
		const VISIBLE_PARTICIPANTS 	= 1;		
		
		protected $mSurveyFormID;
		protected $mName;		
		protected $mCaption;
		protected $mGroupID;
		protected $mDateCreated;
		protected $mParticipantVisibilityStatus;
		protected $mLockStatus;		
		protected $mParticipants = null;
		protected $mFields = null;
		protected $mDb = null;
						
		function __construct($params=0){										
			$this->clearValues();
			$this->mSurveyFormID = is_numeric($params)?$params:$params['surveyFormID'];
			$this->mDb = new dbHandler();			
			if($this->mSurveyFormID != 0){
				$qry = 	'SELECT surveyFormID, name, caption, groupID, dateCreated, participantVisibilityStatus, lockStatus ' .
						'FROM tblSurveyForm ' .
						'WHERE surveyFormID = '.$this->mSurveyFormID;
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(!$rs->retrieveRecordCount()){
					$trace = debug_backtrace();
					throw new exception('Invalid Survey Form ID ['.$this->mSurveyFormID.'] in object of type Survey Form.<<< CALLER FILE: '. $trace[0]['file'] .' LINE: '.$trace[0]['line'].'>>>');
				}				
				$this->mSurveyFormID 				= $rs->getSurveyFormID(0);
				$this->mName						= $rs->getName(0);				
				$this->mCaption						= $rs->getCaption(0);
				$this->mGroupID						= $rs->getGroupID(0);
				$this->mDateCreated 				= $rs->getDateCreated(0);
				$this->mParticipantVisibilityStatus	= $rs->getParticipantVisibilityStatus(0);
				$this->mLockStatus					= $rs->getLockStatus(0);						
			}
		}
		
		public function init($params){					
			$this->mName 	= (array_key_exists('name',$params))?$params['name']:$this->mName;
			$this->mCaption = (array_key_exists('caption',$params))?$params['caption']:$this->mCaption;
			$this->mGroupID = (array_key_exists('groupID',$params))?$params['groupID']:$this->mGroupID;
			$this->mDateCreated = (array_key_exists('dateCreated',$params))?$params['dateCreated']:$this->mDateCreated;
			$this->mParticipantVisibilityStatus = (array_key_exists('participantVisibilityStatus',$params))?$params['participantVisibilityStatus']:$this->mParticipantVisibilityStatus;
			$this->mLockStatus = (array_key_exists('lockStatus',$params))?$params['lockStatus']:$this->mLockStatus;			
			//if there are fields, put them in the array of fields
			if(array_key_exists('fields',$params) && is_array($params['fields']) ){
				$this->mFields = array();
				foreach($params['fields'] as $iFld){
					if(!is_null($iFld)){
						$fld = new SurveyField($iFld['surveyFieldID']);
						$fld->init($iFld);
						$this->mFields[] = $fld;
					}
				}
			}			
			//if there are participans, include them into the array
			if(array_key_exists('participants',$params) && is_array($params['participants']) ){				
				$this->mParticipants = array();
				foreach($params['participants'] as $iParticipant){
					$participant = new SurveyParticipant($iParticipant['surveyParticipantID']);
					$participant->init($iParticipant);
					$this->mParticipants[] = $participant;	
				}
			}
		}
		
		/*** GETTERS ***/
		public function getSurveyFormID(){			
			return $this->mSurveyFormID;
		}		
		public function getName(){			
			return $this->mName;
		}					
		public function getCaption(){
			return $this->mCaption;			
		}
		public function getGroupID(){
			return $this->mGroupID;			
		}
		public function getOwnerGroup(){
			require_once('travellog/model/Class.GroupFactory.php');
			$factory = GroupFactory::instance();
			$mGroup = $factory->create( array($this->getGroupID()) );

			return $mGroup[0];
		}
		public function getDateCreated(){
			return $this->mDateCreated;
		}
		public function getCreated(){
			return $this->getDateCreated();
		}
		public function isParticipantVisible(){
			return ($this->mParticipantVisibilityStatus == self::VISIBLE_PARTICIPANTS);
		}		
		public function isLocked(){
			return ($this->mLockStatus == self::LOCKED);
		}
		
		public function getGroupParticipant($gID=0){
			return SurveyParticipant::getSurveyParticipantBySurveyFormIDAndGroupID($this->getSurveyFormID(),$gID);
		}		
		/*** SETTERS ***/
		
		public function setName($arg){
			$this->mName = $arg;
		}					
		public function setCaption($arg){
			$this->mCaption = $arg;			
		}		
		public function setParticipantsAsVisible(){
			$this->mParticipantVisibilityStatus = self::VISIBLE_PARTICIPANTS;
		}		
		public function setParticipantsAsHidden(){
			$this->mParticipantVisibilityStatus = self::HIDDEN_PARTICIPANTS;
		}		
		public function setAsLocked(){
			$this->mLockStatus = self::LOCKED;					
		}
		public function setAsUnlocked(){
			$this->mLockStatus = self::UNLOCKED;					
		}		
		/*** CRUDE ***/		
		public function save(){
			//validations 
			if(0==strlen($this->mName)){
				throw new exception("Survey form Name cannot be an empty string.");
			}								
			if(0 != $this->mSurveyFormID){//edit
				$qry =	'UPDATE tblSurveyForm SET ' .
							'`name`							='.ToolMan::makeSqlSafeString( $this->mName ).', '.									
							'`caption`						='.ToolMan::makeSqlSafeString( $this->mCaption ).', '.		
							'`groupID`						='.ToolMan::makeSqlSafeString( $this->mGroupID ).', '.													
							'`participantVisibilityStatus`	='.ToolMan::makeSqlSafeString( $this->mParticipantVisibilityStatus ).', '.
							'`lockStatus`					='.ToolMan::makeSqlSafeString( $this->mLockStatus ).' '.							
						'WHERE surveyFormID ='.ToolMan::makeSqlSafeString( $this->mSurveyFormID );
				$this->mDb->execQuery($qry);
			}
			else{//add
				$this->mDateCreated = date("Y-m-d G:i:s");
				$qry = 	'INSERT INTO tblSurveyForm(' .
							'`name`,' .
							'`caption`,' .
							'`groupID`,' .							
							'`dateCreated`,' .
							'`participantVisibilityStatus`,' .
							'`lockStatus` ' .							
						') ' .
						'VALUES (' .
							ToolMan::makeSqlSafeString( $this->mName ).', '.
							ToolMan::makeSqlSafeString( $this->mCaption ).', '.
							ToolMan::makeSqlSafeString( $this->mGroupID ).', '.
							ToolMan::makeSqlSafeString( $this->mDateCreated ).', '.
							ToolMan::makeSqlSafeString( $this->mParticipantVisibilityStatus ).', '.
							ToolMan::makeSqlSafeString( $this->mLockStatus ).
						')';
				$this->mDb->execQuery($qry);
				$this->mSurveyFormID = $this->mDb->getLastInsertedID();
				
			}
		}
		
		public function getData(){
			//contruct an structure for the fields and properties			
			$form = array();
			$form['surveyFormID'] = $this->mSurveyFormID;
			$form['name'] = (strlen($this->mName) > 0)?$this->mName:'';
			$form['caption'] = (strlen($this->mCaption) > 0)?$this->mCaption:'';
			$form['groupID'] = $this->mGroupID;
			$form['dateCreated'] = $this->mDateCreated;
			$form['participantVisibilityStatus'] = $this->mParticipantVisibilityStatus;
			$form['lockStatus'] = $this->mLockStatus;			
			$form['fields']  = array();
			$fields = $this->getSurveyFormFields();
			foreach($fields as $fld){
				$form['fields'][] = $fld->getData();
			}
			$participants = $this->getParticipants();
			$form['participants'] = array();
			foreach($participants as $iPar){
				$form['participants'][] = $iPar->getData();
			}
			return $form;
		}
		
		public function getTotalNumberOfTravelerParticipants(){						
			$total = 0;
			$participants = $this->getParticipants();
			foreach($participants as $iPar){
				$total += $iPar->getNumberOfTravelerParticipants();				
			}
			return $total;
		}
		
		public function getTotalNumberOfUniqueTravelerParticipants(){
			$grpParticipants = $this->getParticipants();
			require_once("travellog/model/Class.GroupFactory.php");			
			$factory =  GroupFactory::instance();			
			$arTravelers = array();
			foreach($grpParticipants as $iGrpPar){
				try{
					$mGrp = $factory->create(array($iGrpPar->getGroupID()));
					$grp = $mGrp[0];
				}
				catch(exception $e){
					$grp = null;
				}
				if(!is_null($grp)){
					$pars = $grp->getMembers();
					if(!is_null($pars)){
						foreach($pars as $iPar){
							if(!in_array($iPar->getTravelerID(),$arTravelers)){
								$arTravelers[] = $iPar->getTravelerID();
							}
						}
					}
				}	
			}
			return count($arTravelers);
		}
		
		public function getNumberOfTravelerParticipantsWhoParticipated(){
			require_once('travellog/model/formeditor/Class.SurveyFormAnswer.php');
			$travelerParticipants = SurveyFormAnswer::getSurveyFormAnswersBySurveyFormID($this->getSurveyFormID());
			return count($travelerParticipants);
		}
		
		public function delete(){
			//clear the survey form answers
			$frmAnswers = $this->getSurveyFormAnswers();
			foreach($frmAnswers as $iFrmAns){
				$iFrmAns->delete();
			}
			//delete the participants
			$participants = $this->getParticipants();
			foreach($participants as $iPar){
				$iPar->delete();
			}
			//delete the fields
			$fields = $this->getSurveyFormFields();
			foreach($fields as $fld){
				$fld->delete();
			}
			$qry = 	'DELETE FROM tblSurveyForm ' .
					'WHERE `surveyFormID` ='.ToolMan::makeSqlSafeString($this->mSurveyFormID);
			$this->mDb->execQuery($qry);
			$this->clearValues();
		}
		
		public function getSurveyFormAnswers(){
			require_once('travellog/model/formeditor/Class.SurveyFormAnswer.php');
			return SurveyFormAnswer::getSurveyFormAnswersBySurveyFormID($this->getSurveyFormID());
		}
		
		public function addField($fld){
			if(!is_array($this->mFields)){
				$this->mFields = array();
			}
			$this->mFields[] = $fld;
		}
		
		public function clearFields(){
			$this->mFields = null;
		}
		
		public function clearValues(){
			$this->mSurveyFormID				= 0;
			$this->mName						= 'Untitled Form';			
			$this->mCaption			 			= 'You can add your survey questions using the options on the left. To name this survey and add your instructions, click on the Form Properties tab on the left.';
			$this->mGroupID		 				= 0;
			$this->mDateCreated		 			= date("Y-m-d G:i:s");
			$this->mParticipantVisibilityStatus	= self::HIDDEN_PARTICIPANTS;
			$this->mLockStatus			 		= self::LOCKED;
			$this->mFields 						= null;
		}
		
		public function getSurveyFormFields(){
			if(!is_array($this->mFields)){				
				return SurveyField::getFormFields($this->mSurveyFormID);
			}
			else{
				return $this->mFields;
			}
		}
		
		public function getParticipants(){
			return SurveyParticipant::getSurveyParticipants($this->mSurveyFormID);				 
		}
		
		public function isGroupParticipant($groupID=0){
			return !is_null($this->getGroupParticipant($groupID));
		}
		
		
		public function mold($frm){
			$tplData = $this->getData();
			$tplData['surveyFormID'] = 0;
			$tplData['dateCreated']  = date("Y-m-d G:i:s");
			$tplData['name']  = $tplData['name'].'(Duplicate)';
			foreach($tplData['fields'] as $key => $val){
				$tplData['fields'][$key]['surveyFieldID'] = 0;
				$tplData['fields'][$key]['parentID'] = 0;
				foreach($tplData['fields'][$key]['inputs'] as $key1 => $val1){
					$tplData['fields'][$key]['inputs'][$key1]['surveyFieldID']=0;
					$tplData['fields'][$key]['inputs'][$key1]['surveyFieldInputID']=0;
				}
			}
			$frm->init($tplData);
		}
		
		public static function getGroupSurveyForms($gID=0){
			$db = new dbHandler();
			$qry = 	'SELECT surveyFormID ' .
					'FROM tblSurveyForm ' .
					'WHERE groupID = '.ToolMan::makeSqlSafeString( $gID ).' ' .						
					'ORDER BY dateCreated DESC';
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if(0<$rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyForm($rs->getSurveyFormID());
				}
			}
			return $ar;
		}
			
		public static function isExists($id){
			$db = new dbHandler();
			$qry = 	'SELECT surveyFormID ' .
					'FROM tblSurveyForm ' .
					'WHERE surveyFormID = '.ToolMan::makeSqlSafeString( $id );						
			$rs = new iRecordset($db->execQuery($qry));
			return $rs->retrieveRecordCount() > 0;
		}
		
		public static function getSurveyFormByGroupIDAndName($groupID=0,$param=null){
			$db = new dbHandler();
			$qry = 	'SELECT surveyFormID ' .
					'FROM tblSurveyForm ' .
					'WHERE `name` = '.GaString::makeSqlSafe(trim($param)).' ' .
					'AND `groupID` = '.GaString::makeSqlSafe($groupID);
			$rs = new iRecordset($db->execQuery($qry));
			return $rs->retrieveRecordCount() > 0 ? new SurveyForm($rs->getSurveyFormID()) : null; 
		}
		
				
		/**
		 * Return the Survey forms of the traveler that 
		 */
		public static function getUnparticipatedSurveyFormsByTraveler($travelerID=0){
			//get the groups of this traveler
			require_once('Class.SurveyFormAnswer.php');
			require_once('travellog/model/Class.Traveler.php');
			require_once('Class.SurveyParticipant.php');
			$traveler = new Traveler($travelerID);
			$groups = $traveler->getGroups();
			$surveyForms = array();
			if(is_array($groups)){
				//get the surveyForms where the current group is a participant
				foreach($groups as $iGroup){
					$groupSurveyForms = SurveyParticipant::getParticipatedSurveysOfGroupByGroupID($iGroup->getGroupID());
					//check if the user has already participated in this survey
					foreach($groupSurveyForms as $iSurveyForm){
						if(!$iSurveyForm->isLocked() && is_null(SurveyFormAnswer::getSurveyFormAnswerBySurveyFormIDAndTravelerID($iSurveyForm->getSurveyFormID(),$traveler->getTravelerID())) && !array_key_exists($iSurveyForm->getSurveyFormID(), $surveyForms)){
							$surveyForms[$iSurveyForm->getSurveyFormID()] = array('group'=>$iGroup,'surveyForm'=>$iSurveyForm);
						}
					}
				}
			}
			krsort($surveyForms);
			return $surveyForms;
		}
		
		public static function getUnparticipatedSurveyFormsOfGroupByTraveler($groupID=0,$travelerID=0){
			require_once('Class.SurveyFormAnswer.php');
			require_once('travellog/model/Class.Traveler.php');
			require_once('Class.SurveyParticipant.php');
			require_once('travellog/model/Class.GroupFactory.php');
			
			//get the surveyforms of the group
			$surveyForms = array();
			
			$traveler = new Traveler($travelerID);
			try{
				$factory = GroupFactory::instance();
				$group = $factory->create(array($groupID));
			}
			catch(exception $e){
				return $surveyForms;
			}
			
			if(0!=$travelerID){
				if(!$group[0]->isMember($traveler)){
					return $surveyForms;
				}
				$groupSurveyForms = SurveyParticipant::getParticipatedSurveysOfGroupByGroupID($groupID);
				foreach($groupSurveyForms as $iSurveyForm){
					if(!$iSurveyForm->isLocked() && is_null(SurveyFormAnswer::getSurveyFormAnswerBySurveyFormIDAndTravelerID($iSurveyForm->getSurveyFormID(),$travelerID))){
						$surveyForms[$iSurveyForm->getSurveyFormID()] = $iSurveyForm;
					}
				}
			}
			krsort($surveyForms);
			return $surveyForms;
		}
	}
?>