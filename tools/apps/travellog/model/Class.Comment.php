<?php
	/**
	 * @(#) Class.Comment.php
	 * 
	 * @author Czarisse Daphne P. Dolina
	 * @version 1.0 - Sep. 14, 2006
	 * @version 2.0 - Dec. 18, 2006 (Modified by Antonio Pepito Cruda Jr.)
	 */

  require_once("Class.dbHandler.php");
  require_once("Class.iRecordset.php");
	require_once("travellog/model/Class.TravelerProfile.php");
	require_once("travellog/model/Class.CommentType.php");
	require_once("travellog/model/Class.TravelLog.php");
	require_once("travellog/model/Class.Article.php");
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.CommentAuthor.php");
	require_once("Cache/ganetCacheProvider.php");
	require_once("Class.ConnectionProvider.php");
	
  /**
   * Comment class. If ID of this class is less than or equal to zero 
   * it means that this class doesn't exist.
   */
  class Comment{
  	const APPROVED = 1;
  	const UNAPPROVED = 0;
  	const READ = 1;
  	const NOT_READ = 0;

		/**
     * Define variables for Class Member Attributes
     */
    private $mConn     		= NULL;
    private $mRs       		= NULL;
    private $mGroupID     = NULL;
    private $mCommentID   = 0;
    private $mCommentType = NULL;
    private $mText    		= NULL;
    private $mDate    		= NULL;
    private $mAuthor  		= NULL;
    private $mIsRead  		= Comment::NOT_READ;
    private $mPokeType		= 0;
    private $mEmail       = "";
    private $mName        = "";
    private $mIsApproved  = Comment::UNAPPROVED;
    private $mFromSite		= "";
    private $cache	=	null;
                         
    /**
     * Constructor Function for this class
     */
    function Comment($params = 0){            
    	$this->cache	=	ganetCacheProvider::instance()->getCache();
    	
    	$handler = new dbHandler();
      
      if (is_numeric($params)) {
	      	$found	=	false;
	      	if ($this->cache != null){
	      		$comment	=	$this->cache->get('Comment_' . $params);
	      		if ($comment){
	      			//echo 'Found Comment_' . $params . ' from cache <br/>';
	      			$found	=	true;
	      			
	      			$this->mCommentID = $comment->getCommentID();
			    	$this->mPokeType = 	$comment->getPokeType();
			    	$this->mText 	= 	$comment->getTheText();
			    	$this->mDate = 		$comment->getCreated();
			    	$this->mAuthor = 	$comment->getAuthor();
			    	$this->mIsRead = 	$comment->getIsRead();
			    	$this->mEmail = 	$comment->getEmail();
			    	$this->mName = 		$comment->getName();
			    	$this->mIsApproved = $comment->isApproved();
			    	$this->mFromSite = $comment->getFromSite();
	      			
	      		}
	      	}
	      	if (!$found) {  	
		      	$sql = "SELECT * " .
		          " FROM tblComment " .
		          " WHERE commentID = ".$handler->makeSqlSafeString($params);
		        
		        $rs = new iRecordset($handler->execute($sql));
				if (0 < $rs->retrieveRecordCount()) { //if record found
				  	$record = $rs->current();
			    	$this->initialize($record);
				}
				
				if ($this->cache != null){
					$this->cache->set('Comment_' . $this->mCommentID,$this,86400);
				}
	      	}	
      }
      else if (is_array($params)) {
      	$this->initialize($params);
      }
    }
    
    /**
     * Sets the attribute values of this Comment based on the 
     * values passed.
     * 
     * @param array $attributes associative array with keys the same in fields of tblComment
     * @return void
     */
    function initialize($attributes){
    	$this->mCommentID = (isset($attributes['commentID'])) ? $attributes['commentID'] : 0;
    	$this->mPokeType = (isset($attributes['pokeType'])) ? $attributes['pokeType']  : 0;
    	$this->mText = (isset($attributes['text'])) ? $attributes['text'] : "";
    	$this->mDate = (isset($attributes['adate'])) ? $attributes['adate'] : "";
    	$this->mAuthor = (isset($attributes['author'])) ? $attributes['author'] : 0;
    	$this->mIsRead = (isset($attributes['isread'])) ? $attributes['isread'] : Comment::NOT_READ;
    	$this->mEmail = (isset($attributes['email'])) ? $attributes['email'] : "";
    	$this->mName = (isset($attributes['name'])) ? $attributes['name'] : "";
    	$this->mIsApproved = (isset($attributes['isApproved'])) ? $attributes['isApproved'] : Comment::UNAPPROVED;
    	$this->mFromSite = (isset($attributes['fromSite'])) ? $attributes['fromSite'] : "";
    }
           
                       
    /**
     * Setter Functions
     */ 
    function setCommentID($commentID){
      $this->mCommentID = $commentID;
    }
            
    function setCommentType($commenttype){
      $this->mCommentType = $commenttype;
    }
            
    function setTheText($text){
      $this->mText = $text;
    }
           
    function setCreated($date){
      $this->mDate = $date;
    }
           
    function setAuthor($author){
      $this->mAuthor = $author;
    }
            
    function setTheDate($mDate){
      $this->mDate = $mDate;
    } 
            
    function setIsRead($isread){
      $this->mIsRead = $isread;
    }
    
    /**
     * Sets the poke type of this comment.
     * 
     * @param integer|PokeType $pokeType
     * @return void
     */      
    function setPokeType($pokeType){
      $this->mPokeType = $pokeType;
    }
    
    /**
     * Sets the email of the non-GANET account that made this comment.
     * 
     * @param string $email
     * @return void
     */
    function setEmail($email){
    	$this->mEmail = $email;
    }
    
    /**
     * Sets the fromSite attribute value.
     * 
     * @param string $fromSite
     * @return void
     */
    function setFromSite($fromSite){
      $this->mFromSite = $fromSite;
    }
    
    /**
     * Sets the name of the non-GANET account that made this comment.
     * 
     * @param string $name
     * @return void
     */
    function setName($name){
    	$this->mName = $name;
    }
    
    /**
     * Sets the isApproved attribute value. Values are Comment::APPROVED and
     * Comment::UNAPPROVED only.
     * 
     * @param integer $isApproved
     * @return void
     */
    function setIsApproved($isApproved){
      $this->mIsApproved = $isApproved;
    }

    /**
     * Getter Functions
     */  
    function getCommentID(){
      return $this->mCommentID;
    }
            
    function getCommentType(){
      return $this->mCommentType;
    }
            
    function getTheText(){
      return $this->mText;
    }
           
    function getCreated(){
      return $this->mDate;
    }
    
    /**
     * Gets the ID of the author of this comment.
     * 
     * @return integer
     */
    function getAuthor(){
      return $this->mAuthor;
    }
    
    /**
     * Gets the author of this comment.
     * 
     * @return CommentAuthor
     */
    function getAuthorObject(){
    	return new CommentAuthor(array(
    	  "authorID" => $this->mAuthor,
    	  "name"     => $this->mName,
    	  "email"    => $this->mEmail
    	));
    }
            
    function getIsRead(){
      return $this->mIsRead;
    }

		function getPokeType(){
		  return $this->mPokeType;
		}
		
		/**
		 * Retrieves the poke type ID of this comment.
		 * 
		 * @return integer
		 */
		function getPokeTypeID(){
			if(is_numeric($this->mPokeType)){
				return $this->mPokeType;
			}
			else {
				return $this->mPokeType->getPokeTypeID();
			}
		}
		
		/**
		 * Tells whether this comment is approved or not
		 * 
		 * @return boolean
		 */
		function isApproved(){
			return (Comment::APPROVED == $this->mIsApproved) ? true : false;
		}
		
		/**
		 * Gets the email address of the one that made this comment.
		 * 
		 * @return string
		 */
		function getEmail(){
			return $this->mEmail;
		}
		
		/**
		 * Gets the site where this comment was posted.
		 * 
		 * @return string
		 */
		function getFromSite(){
			return $this->mFromSite;
		}
		
		/**
		 * Gets the name of the one that made this comment.
		 * 
		 * @return string
		 */
		function getName(){
			return $this->mName;
		}
        
    /**
     * CRUD Methods (Create, Update , Delete)
     */
    function Create(){
		  $handler = new dbHandler();
		  
			$sql = "INSERT INTO tblComment (`pokeType`, `text`, `author`, `adate`, `email`, `name`, `isApproved`, `fromSite`) " .
	      " VALUES (" .
	      "".$handler->makeSqlSafeString($this->mPokeType).",".
        "".$handler->makeSqlSafeString($this->mText)."," .
        "".$handler->makeSqlSafeString($this->mAuthor).",".
        "'".date("Y-m-d H:i:s") ."'," .
        "".$handler->makeSqlSafeString($this->mEmail).",".
        "".$handler->makeSqlSafeString($this->mName).",".
        "".$handler->makeSqlSafeString($this->mIsApproved).",".
        "".$handler->makeSqlSafeString($this->mFromSite)."".
        ")";
	             
      $handler->execute($sql);
      $this->setCommentID($handler->getLastInsertedId());
      $this->invalidateCacheEntry();                
    }
            
    function Update(){
      $handler = new dbHandler();
      
      if (0 < $this->mCommentID){
	      $sql = "UPDATE tblComment SET " .
				  " text = ".$handler->makeSqlSafeString($this->mText).",".
					" isread = ".$handler->makeSqlSafeString($this->mIsRead).",".
					" name = ".$handler->makeSqlSafeString($this->mName).",".
					" email = ".$handler->makeSqlSafeString($this->mEmail).",".
					" isApproved = ".$handler->makeSqlSafeString($this->mIsApproved).",".
					" fromSite = ".$handler->makeSqlSafeString($this->mFromSite).",".
					" adate = '".date("Y-m-d H:i:s")."' " .
					" WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
	        
	      $handler->execute($sql);
			$this->invalidateCacheEntry();	      
		}
    }

    function UpdateIsRead(){
		  if(0 < $this->mCommentID){
	      $handler = new dbHandler();
	      
	      $sql = "UPDATE tblComment " .
				  " SET isread = ".$handler->makeSqlSafeString($this->mIsRead). 
          " WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
	      
	      $handler->execute($sql);
	      $this->invalidateCacheEntry();
		  }
    }
            
    function Delete(){
      if (0 < $this->mCommentID){
			  $handler = new dbHandler();
			  
			  $sql = "DELETE FROM tblComment " .
			    " WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);       
	      
	      $handler->execute($sql);
	      $this->invalidateCacheEntry();
	  }                
    }     
    
		/**
     * Retrieves the context object of this Comment. For example, if this 
     * comment is given to a photo its context object is Photo, if it is
     * given to a traveler profile its context object is TravelerProfile
     * and if it is given to a journal entry it's context object is TravelLog.
     * This actually follows the factory pattern but this uses a database data.
     * 
     * @return Photo|TravelerProfile|TravelLog
     */
    function getContext(){
      $handler = new dbHandler();
      
	  $sql = "SELECT videoID FROM tblCommentToVideo WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
	  $rs = new iRecordset($handler->execute($sql));
	 
	  if( 0 == $rs->retrieveRecordCount() ){
		  $sql = "SELECT photoID FROM tblCommenttoPhoto " .
	        "WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);

	      $rs = new iRecordset($handler->execute($sql));    

	      if (0 == $rs->retrieveRecordCount()) {
	        $sql = "SELECT travellogID FROM tblTravelLogtoComment " .
	        	"WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);

	        $rs = new iRecordset($handler->execute($sql)); 

		      if (0 == $rs->retrieveRecordCount()) {
	          $sql = "SELECT travelerID FROM tblTravelertoComment " .
	            "WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);

			      $rs = new iRecordset($handler->execute($sql));    

			      if (0 < $rs->retrieveRecordCount()) {
			      	$mContext = new TravelerProfile($rs->gettravelerID());
			      }   
			      else {
			       	$sql = "SELECT articleID FROM tblArticletoComment ".
						   "WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
					$rs = new iRecordset($handler->execute($sql));	

					if($rs->retrieveRecordcount()){
						$mContext = new Article($rs->getarticleID());	
					}else{
						return NULL;
					}	

			      }          
		      } 
		      else {
		        $mContext = new TravelLog($rs->gettravellogID());
		      }             
	      }          
	      else {
	        $mContext = new Photo(NULL, $rs->getphotoID());
	      }	
	  }
	  else{
		$mContext = new VideoAlbumToVideo($rs->getvideoID());
		
		if( 0 == (int) $mContext->getParentAlbum()->getAlbumID() ){
			$mContext = new TravelLogToVideo($rs->getvideoID()); 
		}
	  }
                
      return $mContext;              
    }
    
		/**
     * This is only temporary. I call for a recode.
     * 
     * @return int the context ID.
     */
    function getContextId(){
      $handler = new dbHandler();
      
      $sql = "SELECT photoID FROM tblCommenttoPhoto " .
        "WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
      
      $rs = new iRecordset($handler->execute($sql));    
                
      if (0 == $rs->retrieveRecordCount()) {
        $sql = "SELECT travellogID FROM tblTravelLogtoComment " .
        	"WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
	      
        $rs = new iRecordset($handler->execute($sql)); 
	                
	      if (0 == $rs->retrieveRecordCount()) {
          $sql = "SELECT travelerID FROM tblTravelertoComment " .
            "WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
		      
		      $rs = new iRecordset($handler->execute($sql));    
		                
		      if (0 < $rs->retrieveRecordCount()) {
		      	return $rs->gettravelerID();
		      }   
		      else {
		       	$sql = "SELECT articleID FROM tblArticletoComment ".
					   "WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
				$rs = new iRecordset($handler->execute($sql));	
				
				if($rs->retrieveRecordcount()){
					return $rs->getarticleID();	
				}else{
					return 0;
				}	
				
		      }          
	      } 
	      else {
	        return $rs->gettravellogID();
	      }             
      }          
      else {
        return $rs->getphotoID();
      }            
    }
      
    /**
     * Retrieves the traveler ID of the owner of the travellog, photo or 
     * traveler profile in which this comment was made for.
     * 
     * @return integer  
     */
    function getContextOwnerID(){
      $handler = new dbHandler();
      
      $sql = "SELECT photoID FROM tblCommenttoPhoto " .
        "WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
        
      $rs = new iRecordset($handler->execute($sql));    
                
      if (0 == $rs->retrieveRecordCount()){
        $sql = "SELECT b.travelerID " .
          " FROM " .
          " tblTravelLog as a, " .
          " tblTravel as b, " .
          " tblTravelLogtoComment as c " .
          " WHERE a.travelID = b.travelID " .
          " AND a.travellogID = c.travellogID " .
          " AND c.commentID = ".$handler->makeSqlSafeString($this->mCommentID);
          
        $rs = new iRecordset($handler->execute($sql));
	                
        if (0 == $rs->retrieveRecordCount()){
          $sql = "SELECT travelerID FROM tblTravelertoComment " .
          	"WHERE commentID =  ".$handler->makeSqlSafeString($this->mCommentID);
          
          $rs = new iRecordset($handler->execute($sql));   
		                
          if (0 < $rs->retrieveRecordCount()){
		        return $rs->gettravelerID();
		      }
	      }
        else {
	        return $rs->gettravelerID();
	      }
      }
      else {
        $sql = "SELECT travelerID FROM tblTravelertoPhoto " .
          "WHERE photoID = ".$handler->makeSqlSafeString($rs->getphotoID());
        
        $rs = new iRecordset($handler->execute($sql));  

        if (0 < $rs->retrieveRecordCount()){
		      return $rs->gettravelerID();
		    }
      }
        
      return 0;   
    }

		function orderByDate($l,$r){
			if ($l->getCreated() == $r->getCreated())
			  return 0;
			elseif ($l->getCreated() < $r->getCreated())
				return -1;
			else 
				return 1;
		}
		
		public function isRead() {
			return $this->getIsRead();
		}
		
		/**
		 * Sets the comment to approve.
		 * 
		 * @throws exception
		 * @return void
		 */
		public function Approve(){
      $handler = new dbHandler();
	      
	    if (0 < $this->mCommentID){
		    $sql = "UPDATE tblComment SET " .
          " isApproved = 1" .
				  " WHERE commentID = ".$handler->makeSqlSafeString($this->mCommentID);
		    
		    try { 
			    $handler->execute($sql);
			    $this->invalidateCacheEntry();
		    } 
		    catch(exception $ex){
		    	throw new exception ($ex->getMessage());
		    }
		  }
	  }
		
		/**
		 * Retrieves all the unapproved comment (shout-out).
		 * How to use:
		 *   $limit = new RowsLimit(10,0);
		 *   $comments = Comment::getAllUnapprovedShoutOut($limit);
		 *   
		 * @param RowsLimit $limit
		 * @see RowsLimit
		 * @throws exception
		 * @return array Array of Comment object
		 */
		public static function getAllUnapprovedShoutOut($limit=null){
			$handler = new dbHandler();
			$sql = "SELECT * FROM tblComment WHERE isApproved = 0 ORDER BY adate DESC ";
			$sql .= (is_null($limit)) ? "" : $limit->createSqlLimit();
			
			$shoutOuts = array();
			
			try {
				$rs = new iRecordset($handler->execute($sql));
				
				while(!$rs->EOF()) {
					$shoutOuts[] = new Comment($rs->current());
					
					try {
						$rs->next();	
					}
					catch (exception $ex) {
						throw new exception ($ex->getMessage());
					}
				}
	 		}
	 		catch(exception $ex){
	 			throw new exception ($ex->getMessage());
	 		}
	 		
			return $shoutOuts;
		}
		
		/**
		 * Retrieves number of unapproved comment(shout-outs).
		 * How to use:
		 *   $cnt = Comment::getAllUnapprovedShoutOutCount();
		 * 
		 * @throws exception
		 * @return array Array of Comment object
		 */
		public static function getAllUnapprovedShoutOutCount(){
			$handler = new dbHandler();
			$sql = "SELECT count(commentID) as count FROM tblComment WHERE isApproved = 0";
			
			try {
			  $rs = new iRecordset($handler->execute($sql));
			  $record = $rs->current();
			  return $record['count'];
			}
			catch(exception $ex){
			  throw new exception ($ex->getMessage());
			}
		}
		
		public function getDateCreated(){
			$dateArray = explode("-", $this->mDate);
			$yr = $dateArray[0];
			$mn = $dateArray[1];
			$dy = $dateArray[2];
			$day = explode(" ", $dy);
	 		return date("F d, Y", mktime(0,0,0,$mn,$day[0],$yr));
		}
		
		
		// invalidate entry in cache
		private function invalidateCacheEntry(){
			if ($this->cache != null) {
				$this->cache->delete('Comment_' . $this->mCommentID);
				
			}
		}
		
		protected $_mTravelerRecipientID;
		
		public function getTravelerRecipientID() {
			return $this->_mTravelerRecipientID;
		}
		
		public function setTravelerRecipientID($v) {
			if (!is_int($v)) {
				throw new Exception('Integer value expected');
			}
			
			$this->_mTravelerRecipientID = (int) $v;
		}
		
		/**
		 * Retrieves shoutout from unregistered users
		 *
		 * @param RowsLimit $limit
		 * @param array $options
		 * @return array Array of Comment objects
		 */
		public static function getUnRegisteredUserShoutOut(RowsLimit $limit=null, $options=array()){
			$handler = new dbHandler();
			$shoutOuts = array();
			//different query if options contain group
			if (array_key_exists('group', $options)){
				$sql = self::retrieveUnRegisteredUserGroupShoutOutQuery($options);
			}
			else{
				$sql = "SELECT tblComment.* from tblComment ".self::retrieveUnRegisteredUserShoutOutQuery($options);
			}
			
			$orderBy = array_key_exists('order_by', $options) ? $options['order_by'] : 1;
			if ($orderBy == 2){
				$sql .= "ORDER BY name ASC ";
			}
			else{
				$sql .= "ORDER BY adate DESC ";
			}
			$sql .= (is_null($limit)) ? "" : $limit->createSqlLimit();
			
			try {
				$rs = new iRecordset($handler->execute($sql));
				while(!$rs->EOF()) {
					$comment = new Comment($rs->current());
					$shoutOuts[] = $comment;
					try {
						$rs->next();	
					}
					catch (exception $ex) {
						throw new Exception($ex->getMessage());
					}
				}
	 		}
	 		catch(exception $ex){
	 			throw new Exception($ex->getMessage());
	 		}
	 		
			return $shoutOuts;
		}
		
		/**
		 * Retrieves shoutout count from unregistered users
		 *
		 * @param array $options
		 * @return int count of Comments
		 */
		public static function getUnRegisteredUserShoutOutCount($options=array()){
			$handler = new dbHandler();
			//different query if options contain group
			if (array_key_exists('group', $options)){
				$sql = str_replace('SELECT tblComment.*', 'SELECT count(tblComment.commentID) as count', self::retrieveUnRegisteredUserGroupShoutOutQuery($options));
			}
			else{
				$sql = "SELECT count(tblComment.commentID) as count FROM tblComment ".self::retrieveUnRegisteredUserShoutOutQuery($options);
			}
			
			try {
				$count = 0;
				$rs = new iRecordset($handler->execute($sql));
				while(!$rs->EOF()) {
					$record = $rs->current();
					$count += $record['count'];
					try {
						$rs->next();	
					}
					catch (exception $ex) {
						return $count;
					}
				}
				return $count;
			}
			catch(exception $ex){
				throw new Exception($ex->getMessage());
			}
		}
		
		/**
		 * Retrieves group shoutout sql query string
		 *
		 * @param array $options
		 * @return string
		 */
		protected static function retrieveUnRegisteredUserGroupShoutOutQuery($options=array()){
			if ($groupIDs = self::retrieveGroupIDs($options)){
				$sql = "(SELECT tblComment.* FROM tblComment ".
				"INNER JOIN tblTravelertoComment ".
					"ON tblComment.commentID = tblTravelertoComment.commentID ".
				"INNER JOIN tblTraveler ".
					"ON tblTravelertoComment.travelerID = tblTraveler.travelerID ".
				"INNER JOIN tblGrouptoTraveler ".
					"ON (tblTraveler.travelerID = tblGrouptoTraveler.travelerID AND ".
					"tblGrouptoTraveler.groupID IN (".implode(',', $groupIDs)."))".
				self::retrieveUnRegisteredUserShoutOutQuery($options).
				") ".
				"UNION ".
				"(SELECT tblComment.* FROM tblComment ".
				"INNER JOIN tblTravelLogtoComment ".
					"ON tblComment.commentID = tblTravelLogtoComment.commentID ".
				"INNER JOIN tblTravelLog ".
					"ON (tblTravelLogtoComment.travellogID = tblTravelLog.travellogID AND ".
					"tblTravelLog.publish = 1 AND ".
					"tblTravelLog.deleted = 0) ".
				"INNER JOIN tblTravel ".
					"ON tblTravelLog.travelID = tblTravel.travelID ".
				"INNER JOIN tblTraveler ".
					"ON (tblTravel.travelerID = tblTraveler.travelerID AND ".
					"tblTraveler.isSuspended = 0 AND ".
					"tblTraveler.active > 0 AND ".
					"tblTraveler.deactivated = 0) ".
				"INNER JOIN tblGrouptoTraveler ".
					"ON (tblTraveler.travelerID = tblGrouptoTraveler.travelerID AND ".
					"tblGrouptoTraveler.groupID IN (".implode(',', $groupIDs)."))".
				self::retrieveUnRegisteredUserShoutOutQuery($options).
				") ".
				"UNION ".
				"(SELECT tblComment.* FROM tblComment ".
				"INNER JOIN tblArticletoComment ".
					"ON tblComment.commentID = tblArticletoComment.commentID ".
				"INNER JOIN tblArticle ".
					"ON (tblArticletoComment.articleID = tblArticle.articleID AND ".
					"tblArticle.groupID IN (".implode(',', $groupIDs)."))".
				self::retrieveUnRegisteredUserShoutOutQuery($options).
				") ";
			}
			else {
				$sql = "SELECT tblComment.* FROM tblComment WHERE 0 ";
			}
			
			return $sql;
		}
		
		/**
		 * Retrieves shoutout sql query string
		 *
		 * @param array $options
		 * @return string
		 */
		protected static function retrieveUnRegisteredUserShoutOutQuery($options=array()){
			$sql = " ";
			$sql .= "WHERE tblComment.author = 0 ";
			if (array_key_exists('name', $options)){
				$sql .= "AND tblComment.name LIKE '%" . mysql_real_escape_string($options['name']) . "%' ";
			}
			
			return $sql;
		}
		
		/**
		 * Retrieves groupIDs
		 *
		 * @param array $options
		 * @return array
		 */
		protected static function retrieveGroupIDs($options=array()){
			//todo: transfer this function to its appropriate class
			$handler = new dbHandler();
			$groupIDs = array();
			$groupSql = "SELECT groupID FROM tblGroup WHERE ".
				"name LIKE '%".mysql_real_escape_string($options['group'])."%' AND ".
				"isSuspended = 0";
			$rs = new iRecordset($handler->execute($groupSql));
			while (!$rs->EOF()) {
				$row = $rs->current();
				$groupIDs[] = $row['groupID'];
				try {
					$rs->next();
				}
				catch (exception $ex) {
					throw new Exception($ex->getMessage());
				}
			}
			
			return $groupIDs;
		}
		
  }
        
?>