<?
	class CalendarEvent
	{
		public static $ACTIVITY = 0;				    // mContext possible values (AdminGroup activity)
		public static $GROUP_EVENT = 1;					//							(FunGroup activity)
		public static $TRAVELER_EVENT = 2;				//							(Personal activity)
		
		
		private $mEventID;			// id of the calendar event
		private $mTitle;			// title of the calendar event
		private $mDate;				// date/time of this event
		private $mDescription;		// summarized description of the event
		private $mLink;				// hyperlink to the details of this calendar event
		private $mContext;   		// context; 0 = Activity; 1 = GroupEvent; 2 = Traveler Event
		private $mContextID;        // if context = Activity -> contextID is groupID of the owner of the activity
									// if context = GroupEvent -> contextID is groupID of the owner of the GroupEvent
									// if context = Traveler Event -> contextID is the travelerID of the the Traveler Event
		private $mDisplayPublic;	// 
		
		private $mTimeZoneID = 0;
		
		public function CalendarEvent($itemID, $title, $date, $description, $link, $context, $contextID = 0, $displayPublic = false) {
			
			$this->mEventID		= $itemID;
			$this->mTitle 		= $title;
			$this->mDate 		= $date;
			$this->mDescription = $description;
			$this->mLink 		= $link;
			$this->mContext 	= $context;
			$this->mContextID   = $contextID;
			$this->mDisplayPublic = $displayPublic;
		}
		
		public function setTimeZoneID($param){
			$this->mTimeZoneID = $param;
		}
		
		public function getTimeZoneID(){
			return $this->mTimeZoneID;
		}
		
		function getTimeZone(){
			require_once('Class.TimeZone.php');
			$tz = new TimeZone($this->mTimeZoneID);
			return $tz->getTimeZone();
		}
		
		public function getEventID() {
			return $this->mEventID;	
		}
		
		public function getTitle() {
			return $this->mTitle;	
		}
		
		public function setTitle($title) {
			$this->mTitle = $title;	
		}
		
		public function getTheDate() {
			return $this->mDate;
		}
		
		public function getDescription() {
			return $this->mDescription;
		}
		
		public function getLink() {
			return $this->mLink;
		}
		
		public function getContext() {
			return $this->mContext;
		}
		
		public function getContextID() {
			return $this->mContextID;
		}

		public function getDisplayPublic() {
			return $this->mDisplayPublic;
		}				
		
		// user defined sort function to be called when sorting by date
		function orderByDate($l,$r){
			if ($l->getTheDate() == $r->getTheDate())
			   return 0;
			elseif ($l->getTheDate() < $r->getTheDate())
				return -1;
			else 
				return 1;	  
		} 
	
	}
?>