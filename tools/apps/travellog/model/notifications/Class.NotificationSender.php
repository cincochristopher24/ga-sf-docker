<?php
	require_once('Class.BufferedNotification.php');
	class NotificationSender
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)) self::$instance = new NotificationSender;
			return self::$instance;
		}
		
		private function __construct(){}
		public function send($notifications=array(),$instant=false){
			foreach($notifications as $iNotif){
				$res = $iNotif->send($instant);
				if(0==$res['status']){
					BufferedNotification::addToBuffer($iNotif,$res['error']);
				}
			}
		}
	}
?>