<?php
	require_once('Class.Template.php');
	class NotificationComposer
	{
		public static function compose($vars,$tplName=null){
			if(is_null($tplName)){
				$trace = debug_backtrace();
				$callerClass	= $trace[1]['class'];
				$templateName = 'tpl.'.substr($callerClass,0,strlen($callerClass)-5).'Template.php';
			}
			else{
				$templateName = $tplName;
			}
			$tpl = new Template();
			$tpl->setVars($vars);
			$tpl->doNotUseMainTemplate();
			$tpl->setPath('travellog/views/notifications/');
			return $tpl->fetch($templateName);
		}
	}
?>