<?php
	require_once('Class.dbHandler.php');
	require_once('Class.Decorator.php');
	require_once('Class.GaDateTime.php');
	require_once('Class.Notification.php');
	
	class BufferedNotification extends Decorator
	{
		static public function getAllInstances(){
			$db = new dbHandler();
			$rs = new iRecordset($db->execute('SELECT * FROM tblBufferedNotification '));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new BufferedNotification(null,$row);
			}
			return $ar;
		}
		
		static public function addToBuffer($notification,$error){
			if(!$notification instanceof BufferedNotification){
				$bufNotification = new bufferedNotification;
				$bufNotification->initialize($notification);
				$bufNotification->setError($error);
				$bufNotification->save();
				return true;
			}
			return false;
		}
	
		private $mDb						= null;
		private $mBufferedNotificationID 	= null;
		private $mError						= null;
		private $mDateCreated				= null;
		
		private function BufferedNotification($keyID=null,$data=array()){
			$this->mDb = new dbHandler();
			$this->mCoreObject = new Notification();
			if(!is_null($keyID)){
				$rs = $ths->mDb->execute('SELECT * FROM tblBufferedNotification WHERE bufferedNotificationID = '.$this->mDb->makeSqlSafeString($keyID));
				if(!$rs->retrieveRecordCount()){
					throw new NotificationException(NotificationException::INVALID_BUFFERED_NOTIFICATION_ID);
				}
				$this->initData($rs[0]);
			}
			if(count($data)){
				$this->initData($data);
			}
			//try to load the buffered recipients
			$rs = new iRecordset($this->mDb->execute('SELECT * FROM tblBufferedNotificationRecipient WHERE bufferedNotificationID = '.$this->mDb->makeSqlSafeString($this->getBufferedNotificationID())));
			foreach($rs as $row){
				if(NotificationRecipient::TO_RECIPIENT==$row['type']){
					$this->addRecipient($row['emailAddress'],$row['name']);
				}
				elseif(NotificationRecipient::CC_RECIPIENT==$row['type']){
					$this->addCcRecipient($row['emailAddress'],$row['name']);
				}
				elseif(NotificationRecipient::BCC_RECIPIENT==$row['type']){
					$this->addBccRecipient($row['emailAddress'],$row['name']);
				}
				elseif(NotificationRecipient::REPLY_TO_RECIPIENT==$row['type']){
					$this->addReplyTo($row['emailAddress'],$row['name']);
				}
			}
		}
		
		private function initData($data){
			$this->setBufferedNotificationID($data['bufferedNotificationID']);
			$this->setError($data['error']);
			$this->setDateCreated($data['dateCreated']);
			
			$this->setSender($data['senderEmailAddress']);
			$this->setSenderName($data['senderName']);
			$this->setSubject($data['subject']);
			$this->setMessage($data['message']);
			$this->setAltMessage($data['altMessage']);
		}
		
		private function initialize($notification){
			$this->mCoreObject = $notification;
		}
		
		private function setBufferedNotificationID($bufferedNotificationID=null){
			$this->mBufferedNotificationID = $bufferedNotificationID;
		}
		
		public function setError($code){
			$this->mStatusCode = $code;
		}
		
		public function setDateCreated($dateCreated){
			$this->mDateCreated = $dateCreated;
		}
		
		public function getBufferedNotificationID(){
			return $this->mBufferedNotificationID;
		}
		
		public function getStatusCode(){
			return $this->mStatusCode;
		}
		
		public function getDateCreated(){
			return $this->mDateCreated;
		}
		
		
		public function save(){
			if(!$this->getBufferedNotificationID()){//perform insert
				$sql = 	'INSERT INTO tblBufferedNotification SET '.
							'senderEmailAddress = '.$this->mDb->makeSqlSafeString($this->getSender()).', '.
							'senderName = '.$this->mDb->makeSqlSafeString($this->getSenderName()).', '.
							'subject = '.$this->mDb->makeSqlSafeString($this->getSubject()).', '.
							'message = '.$this->mDb->makeSqlSafeString($this->getMessage()).', '.
							'altMessage = '.$this->mDb->makeSqlSafeString($this->getAltMessage()).', '.
							'dateCreated = '.$this->mDb->makeSqlSafeString(GaDateTime::dbDateTimeFormat()).', '.
							'error = '.$this->mDb->makeSqlSafeString($this->getStatusCode());
				$this->mDb->execute($sql);
				$this->setBufferedNotificationID($this->mDb->getLastInsertedID());
			}
			else{//perform update
				$sql = 	'UPDATE tblBufferedNotification SET '.
							'senderEmailAddress = '.$this->mDb->makeSqlSafeString($this->getSender()).', '.
							'senderName = '.$this->mDb->makeSqlSafeString($this->getSenderName()).', '.
							'subject = '.$this->mDb->makeSqlSafeString($this->getSubject()).', '.
							'message = '.$this->mDb->makeSqlSafeString($this->getMessage()).', '.
							'altMessage = '.$this->mDb->makeSqlSafeString($this->getAltMessage()).', '.
							'error = '.$this->mDb->makeSqlSafeString($this->getStatusCode()).' '.
						'WHERE bufferedNotificationID = '.$this->mDb->makeSqlSafeString($this->getBufferedNotificationID());
				$this->mDb->execute($sql);
			}
			//clear the recipients
			$this->clearRecipientRecords();
			$sql = 'INSERT INTO tblBufferedNotificationRecipient (`bufferedNotificationID`,`emailAddress`,`name`,`type`) VALUES ';
			$values = array();
			foreach($this->getRecipients() as $iRec){
				$values[] = '('.$this->getBufferedNotificationID().', '.$this->mDb->makeSqlSafeString($iRec->getEmailAddress()).', '.$this->mDb->makeSqlSafeString($iRec->getName()).','. NotificationRecipient::TO_RECIPIENT .')';
			}
			foreach($this->getCcRecipients() as $iRec){
				$values[] = '('.$this->getBufferedNotificationID().', '.$this->mDb->makeSqlSafeString($iRec->getEmailAddress()).', '.$this->mDb->makeSqlSafeString($iRec->getName()).','. NotificationRecipient::CC_RECIPIENT .')';
			}
			foreach($this->getBccRecipients() as $iRec){
				$values[] = '('.$this->getBufferedNotificationID().', '.$this->mDb->makeSqlSafeString($iRec->getEmailAddress()).', '.$this->mDb->makeSqlSafeString($iRec->getName()).','. NotificationRecipient::BCC_RECIPIENT .')';
			}
			foreach($this->getReplyTo() as $iRec){
				$values[] = '('.$this->getBufferedNotificationID().', '.$this->mDb->makeSqlSafeString($iRec->getEmailAddress()).', '.$this->mDb->makeSqlSafeString($iRec->getName()).','. NotificationRecipient::REPLY_TO_RECIPIENT .')';
			}
			if(count($values)){
				$sql .= implode(', ',$values);
				$this->mDb->execute($sql);
			}
		}
		
		private function clearRecipientRecords(){
			$sql = 'DELETE FROM tblBufferedNotificationRecipient WHERE bufferedNotificationID = '.$this->mDb->makeSqlSafeString($this->getBufferedNotificationID());
			$this->mDb->execute($sql);
		}
		
		public function delete(){
			//delete the recipients
			$this->clearRecipientRecords();
			$sql = 'DELETE FROM tblBufferedNotification WHERE bufferedNotificationID = '.$this->mDb->makeSqlSafeString($this->getBufferedNotificationID());
			$this->mDb->execute($sql);
		}
		
		public function send(){
			$res = $this->mCoreObject->send();
			if(0==$res['status']){
				//delete this buffered notification if it has been buffered for 7 days.
				if(GaDateTime::dateDiff($this->getDateCreated(),GaDateTime::dbDateTimeFormat(),'d') >= 7){
					$this->delete();
				}
				else{
					$this->setError($res['error']);
					$this->save();
				}
			}
			else{
				$this->delete();
			}
			return $res;
		}
	}
?>