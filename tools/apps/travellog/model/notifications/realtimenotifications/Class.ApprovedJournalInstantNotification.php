<?php
	
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/Class.Travel.php');
 	require_once('travellog/model/Class.AdminGroup.php');

	class ApprovedJournalInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'TRAVEL_ID'	=> null,
				'GROUP_ID'	=> null
			);
			$options = array_merge($default,$options);
			
			if( !$options['TRAVEL_ID'] || !$options['GROUP_ID'] )
				return;
			
			try{
				$journal 	= new Travel($options['TRAVEL_ID']);
			 	$group 		= new AdminGroup($options['GROUP_ID']);
				$parent		= is_null($parent = $group->getParent()) ? $group : $parent;
				//$config		= new Con_fig($_SERVER['SERVER_NAME']);
				$config 	= SiteContext::getInstance();
				$emailName	= $config->getEmailName();
				$siteName	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				$siteUrl	= $config->getServerName();
				$owner		= $journal->getOwner();
				
				if( $owner instanceof Group || $owner->isAdvisor() )
					return;
				if( $owner instanceof Traveler ){
					// check if recipient's notification preference is set to can receive notification
					require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
					$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($owner->getTravelerID());
					if( FALSE == $canReceiveNotification ){
						return;
					}
					// end check
				}
					
				$footerVars		= array(
					'siteName'	=> $siteName,
					'emailName'	=> $emailName,
					'siteUrl'	=> $siteUrl,
					'group'		=> $group,
					'isAdvisor'	=> false
				);
				
				$tplVars		= array_merge($footerVars, array(
					'notificationFooter' => NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php'),
					'journal'	=> $journal,
					'recipient'	=> $owner->getUsername(),
					'travelID'	=> $options['TRAVEL_ID'],
					'authKey'	=> crypt($owner->getUserName() . $owner->getPassword())
				));
				
				$notification = new Notification;
				$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.ApprovedJournalNotificationTemplate.php'));
				$notification->setSubject("Your journal now appears on ".$group->getName());
				$notification->setSender($config->getGanetAdminEmail());
				$notification->setSenderName($emailName);
				$notification->addReplyTo($config->getAdminEmail(), $emailName);
				$notification->addRecipient($owner->getTravelerProfile()->getEmail());
				$this->mNotifications[] = $notification;
			}
			catch(exception $e){}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
	}

?>