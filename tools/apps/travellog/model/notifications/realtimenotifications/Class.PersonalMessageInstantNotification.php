<?php
	
	/**
	* send notification after a message is sent
	**/
	
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/Class.MessageSource.php');
	require_once('travellog/model/Class.MessageType.php');
	
	require_once('travellog/model/messageCenter/personal_message/Class.gaPersonalMessage.php');
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');

	class PersonalMessageInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		private $mDb			= null;
		
		public function __construct(){
			$this->mDb = new dbHandler;
		}
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'MESSAGE'			=> null,
				'INCLUDE_MESSAGE'	=> false
			);
			$options 		= array_merge($default, $options);
			
			// if there is no message 
			if( is_null($options['MESSAGE']) ){	
				return;
			}
			
			// message is new model, create instance of old model
			if( $options['MESSAGE'] instanceof gaPersonalMessage ){
				try{
					$tempMessage = MessageFactory::getInstance()->create($options['MESSAGE']->getAttributeID());
					$tempMessage->setDestination(array(SendableFactory::Instance()->Create($options['MESSAGE']->getRecipientID())));
					$options['MESSAGE'] = $tempMessage;
				}
				catch(exception $e){return;} // failed to create instance of old model
			}
			
			
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 	= SiteContext::getInstance();
			$emailName		= $domainConfig->getEmailName();
			$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl 		= $domainConfig->getServerName();
			$messageSource 	= new MessageSource($options['MESSAGE']);
			$destinations	= $options['MESSAGE']->getDestination();
			$recipients 	= array();
			$replyTo		= null;
			$srcGroupID		= $messageSource->getGroupID();
			$srcGroup		= null;
			if( $srcGroupID ){
				try{
					$srcGroup = GroupFactory::instance()->create(array($srcGroupID));
					$srcGroup = $srcGroup[0];
				}
				catch(exception $e){
					$srcGroup = null;
				}
			}
			
			//added by Jul to include travelers with pending invitation to join the group;
			$invitedTravelerIDs = array();
			$invitingGroup = array();
			
			foreach( $destinations as $destination ){
				
				// destination is group
				if( $destination instanceof Group ){
					// if source is a group/administrator, recipients will be the members
					if( $options['MESSAGE']->getSource() instanceof Group || $messageSource->postedAsStaff() ){
						$recipients = array_merge($recipients, $destination->getMembers() );
						//added by Jul to include travelers with pending invitation to join the group;
						$invitedTravelers = $destination->getInvitedTravelers();
						foreach($invitedTravelers as $invited){
							$invitedTravelerIDs[] = $invited->getTravelerID();
							$invitingGroup[] = $destination;
							$recipients[] = $invited;
						}
					}
					// recipient will be the group administrator
					else{
						$recipients[] = $destination;
						$recipientGroup = $destination;

						/** 
						 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
						 *				cobrand site and recipient is admin of group, use cobrand
						 */
						// if group has a cobrand site use respective cobrand, else, use dotNet
						//$cb = ( $cb	= Config::findByGroupID($destination->getGroupID())) ? $cb : Config::findByGroupID();
						
						// edited by chris: check first if there's a parentID and we will only use the parentID to find the cobrand group
						$parentID = 0 < $destination->getParentID() ? $destination->getParentID() : $destination->getGroupID(); 
						$cb = ( $cb	= Config::findByGroupID($parentID) ) ? $cb : null;
						
						if ( !is_null($cb) ){
							$siteUrl	= $cb->getServerName();
							$sender 	= $cb->getAdminEmail();
							$emailName	= $cb->getEmailName();
							$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
						}
					}
				}
				// destination is a regular traveler
				else{
					$recipients[] = $destination;
				}
			}

			$recipientSendableIDs = array();			
			foreach( $recipients as $recipient ) {
				$recipientSendableIDs[] = $recipient->getSendableID();
			}
			
			$sql = 	'SELECT `messageType`, `recipientID` ' .
					'FROM `tblMessageToAttribute` ' .
					'WHERE `messageID` = ' . GaString::makeSqlSafe($options['MESSAGE']->getMessageID()) . ' ' .
					'AND `recipientID` IN (' . implode(',',$recipientSendableIDs) . ') ' .
					'AND (messageType = ' . MessageType::$SPAMBOX . ' OR messageType = ' . MessageType::$INBOX . ')';
			$rs = new iRecordset($this->mDb->execute($sql));
			unset($recipientSendableIDs);
			foreach($rs as $row){
				$recipientSendableIDs[$row['recipientID']] = $row['messageType'];
			}
			
			if( !isset($recipientSendableIDs) || empty($recipientSendableIDs) ) return;
			
			$travelerIDsDone = array();
			// create notification for each recipient
			foreach( $recipients as $recipient ){
				$recipientStaffs	= array();
				$recipientGroup		= null;
				$gRecipientID 		= 0;
				$subjectGroup		= $srcGroup;
				
				if( $recipient instanceof Traveler ){
					if($recipient->isSuspended() || $recipient->isDeactivated()){
						continue;
					}
					// check if recipient's notification preference is set to can receive notification
					$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($recipient->getTravelerID());
					if( FALSE == $canReceiveNotification ){
						continue;
					}
					// end check
				}
				
				if( $recipient instanceof Group ){
					$subjectGroup 	= $recipient;
					$gRecipientID 	= $recipient->getSendableID();
					$recipient 		= $recipient->getAdministrator();
					$recipientGroup	= $subjectGroup;
					
					// recipients will be the staff of the destination group
					$recipientStaffs=  $destination->getStaff();
				}
				
				if( array_key_exists($recipient->getSendableID(), $recipientSendableIDs) || array_key_exists($gRecipientID, $recipientSendableIDs) ) {
					
					//if( !$recipient instanceof Traveler )
					//	$recipient = $recipient->getAdministrator();
					
					// if this is marked as spam, send this to admin@goabroad.net 
					if( (isset($recipientSendableIDs[$recipient->getSendableID()]) && MessageType::$SPAMBOX == $recipientSendableIDs[$recipient->getSendableID()] ) || 
						(isset($recipientSendableIDs[$gRecipientID]) && MessageType::$SPAMBOX == $recipientSendableIDs[$gRecipientID])
					){
					//if( MessageType::$SPAMBOX == $recipientSendableIDs[$recipient->getSendableID()] ){
						$vars = array(	
							'recipient' 	=> $recipient->getUsername(),
							'message' 		=> $options['MESSAGE'],
							'recipientGroup'=> $recipientGroup,
							'source' 		=> $messageSource,
							'siteName' 		=> $siteName,
							'siteUrl' 		=> $siteUrl
						);
						$notification = new Notification;
						$notification->setMessage(NotificationComposer::compose($vars,'tpl.SpamMessageInstantNotificationTemplate.php'));
						$notification->setSender($domainConfig->getGanetAdminEmail());
						$notification->setSenderName($domainConfig->getEmailName());
						$notification->addReplyTo($domainConfig->getAdminEmail(), $domainConfig->getEmailName());
						$notification->setSubject('Message marked as spam from ' . $messageSource->getName() );
						$notification->addRecipient('admin@goabroad.net');
					}
					else {
						
						$footerVars = array(
							'siteName'	=> $siteName,
							'siteUrl'	=> $siteUrl
						);
						
						if( in_array($recipient->getTravelerID(),$invitedTravelerIDs) ){
							
							if( !in_array($recipient->getTravelerID(), $travelerIDsDone) ){
								$footerVars = array(
									'siteName'	=> "The GoAbroad Network Online Community",
									'siteUrl'	=> $siteUrl
								);
								$footerVars['group']	= $subjectGroup;
							
								$vars = array_merge($footerVars, array(
									'message'		=> $options['MESSAGE'],
									'messageSource'	=> $messageSource,
									'recipient'		=> $recipient->getUsername(),
									'emailName'		=> $emailName,
									'isMessageIncluded' => TRUE,
									'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooterForInvitedTravelers.php')
								));
								$vars["siteName"] = $siteName;
								$vars["siteUrl"] = $siteUrl;
								$notification = new Notification;
								$notification->setMessage(NotificationComposer::compose($vars, 'tpl.PersonalMessageInstantNotificationTemplateForInvitedTravelers.php'));
								$notification->setSubject('You have a new message on '.$siteName);
								$notification->setSender($domainConfig->getGanetAdminEmail());
								$notification->setSenderName($emailName);
								$notification->addRecipient($recipient->getTravelerProfile()->getEmail());

								if( $messageSource->postedAsStaff() ){
									$notification->addReplyTo($options['MESSAGE']->getSource()->getTravelerProfile()->getEmail());
								} else {
									$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
								}
								
								$travelerIDsDone[] = $recipient->getTravelerID();
							}
							
						}else{
						
							if( $subjectGroup instanceof Group ){
								$footerVars['isAdvisor']= $recipient->getTravelerID() == $subjectGroup->getAdministratorID();
								$footerVars['group']	= $subjectGroup;
							}
						
							$vars = array_merge($footerVars, array(
								'message'		=> $options['MESSAGE'],
								'messageSource'	=> $messageSource,
								'recipient'		=> $recipient->getUsername(),
								'recipientGroup'=> $recipientGroup,
								'emailName'		=> $emailName,
								'isMessageIncluded' => $options['INCLUDE_MESSAGE'],
								'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
							));
							
							// if recipient is group and it has staffs, send notif to staff
							if( !empty($recipientStaffs) ){
								$this->addStaffRecipients($recipientStaffs, array(
									'sender'	=> $domainConfig->getGanetAdminEmail(),
									'replyTo'	=> $domainConfig->getAdminEmail(),
									'siteName'	=> $siteName,
									'siteUrl'	=> $siteUrl,
									'emailName'	=> $emailName,
									'commonVars'=> $vars
								));
							}
							
							// if destination is a subgroup, we will not send a copy to the admin
							if( $recipientGroup instanceof Group && $recipientGroup->getParentID() > 0 ){
								continue;
							}
							
							$notification = new Notification;
							$notification->setMessage(NotificationComposer::compose($vars, 'tpl.PersonalMessageInstantNotificationTemplate.php'));
							$notification->setSubject('You have a new message on '.$siteName);
							$notification->setSender($domainConfig->getGanetAdminEmail());
							$notification->setSenderName($emailName);
							$notification->addRecipient($recipient->getTravelerProfile()->getEmail());

							if( $messageSource->postedAsStaff() ){
								$notification->addReplyTo($options['MESSAGE']->getSource()->getTravelerProfile()->getEmail());
							} else {
								$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
							}
						}

					}
					$this->mNotifications[] = $notification;
				}
			}
		}
		
		private function addStaffRecipients( $recipientStaffs, $params = array()){
			
			$default = array(
				'sender'	=> '',
				'replyTo'	=> '',
				'siteName'	=> '',
				'emailName'	=> '',
				'siteUrl'	=> '',
				'commonVars'=> array()
			);
			$params = array_merge($default, $params);
			$footerVars = array(
				'siteName' 	=> $params['siteName'],
				'siteUrl'	=> $params['siteUrl']
			);
			$commonVars = $params['commonVars'];
			
			foreach( $recipientStaffs as $recipient ){
				if($recipient->isSuspended() || $recipient->isDeactivated()){
					continue;
				}
				// check if recipient's notification preference is set to can receive notification
				$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($recipient->getTravelerID());
				if( FALSE == $canReceiveNotification ){
					continue;
				}
				// end check
				
				$vars = array_merge($footerVars, array(
					'message'		=> $commonVars['message'],
					'messageSource'	=> $commonVars['messageSource'],
					'recipient'		=> $recipient->getUsername(),
					'recipientGroup'=> $commonVars['recipientGroup'],
					'emailName'		=> $commonVars['emailName'],
					'isMessageIncluded' => $commonVars['isMessageIncluded'],
					'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
				));

				$notification = new Notification;
				$notification->setMessage(NotificationComposer::compose($vars, 'tpl.PersonalMessageInstantNotificationTemplate.php'));
				$notification->setSubject('You have a new message on '.$params['siteName']);
				$notification->setSender($params['sender']);
				$notification->setSenderName($params['emailName']);
				$notification->addReplyTo($params['replyTo'], $params['emailName']);
				$notification->addRecipient($recipient->getTravelerProfile()->getEmail());
				$this->mNotifications[] = $notification;
			}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
			/***
			foreach($this->mNotifications as $iNotification)
				$iNotification->send(true);
			***/
		}
	}

?>