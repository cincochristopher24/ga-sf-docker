<?php
	
	/** This notification will not use gaLogs since log data is only available upon script shutdown. **/
	
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/Class.Traveler.php');

	require_once('travellog/model/notifications/Class.NotificationSender.php');

	class GroupInviteTravelerInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		private static $used 	= array();
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'TRAVELER'	=> null,
				'GROUP'		=> null,
				'CUSTOM_MESSAGE' => null
			);
			$options 	= array_merge($default, $options);
			if( is_null($options['TRAVELER']) || is_null($options['GROUP']) )
				return;
			
			if( !isset(self::$used[$options['TRAVELER']->getTravelerID()]) ){
				self::$used[$options['TRAVELER']->getTravelerID()][] = $options['GROUP']->getGroupID();
			}
			else{
				if( in_array($options['GROUP']->getGroupID(), self::$used[$options['TRAVELER']->getTravelerID()]) )
					return;
				else
					self::$used[$options['TRAVELER']->getTravelerID()][] = $options['GROUP']->getGroupID();
			}
			
			// check if recipient's notification preference is set to can receive notification
			require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
			$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($options['TRAVELER']->getTravelerID());
			if( FALSE == $canReceiveNotification ){
				return;
			}
			// end check
				
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 	= SiteContext::getInstance();
			$emailName		= $domainConfig->getEmailName();
			$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl 		= $domainConfig->getServerName();	
			$notification 	= new Notification;
			
			$footerVars		= array(
				'siteName'	=> $siteName,
				'emailName'	=> $emailName,
				'siteUrl'	=> $siteUrl
			);
			
			if( !is_null($options['CUSTOM_MESSAGE']) && '' != trim($options['CUSTOM_MESSAGE']) ){
				$notification->setMessage($options['CUSTOM_MESSAGE']);
			}
			else{
				$vars = array_merge($footerVars, array(	
					'group'		=> $options['GROUP'],
					'recipient' => $options['TRAVELER']->getUserName(),
					'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
				));
				if ( (241 == $options['GROUP']->getID()) || (241 == $options['GROUP']->getParentID()) )
					$notification->setMessage(NotificationComposer::compose($vars,'tpl.CISGroupInvitationNotificationTemplate.php'));
				else
					$notification->setMessage(NotificationComposer::compose($vars,'tpl.GroupInvitationNotificationTemplate.php'));
			}
			
			$notification->setSubject("Invitation from group " . $options['GROUP']->getName());
			$notification->setSender($domainConfig->getGanetAdminEmail());
			$notification->setSenderName($emailName);
			$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
			$notification->addRecipient($options['TRAVELER']->getTravelerProfile()->getEmail());
			$this->mNotifications[] = $notification;
			
			/*
			$db = new dbHandler;
			$sql = 	'SELECT * FROM `Travel_Logs`.`tblInviteList` ' .
					'WHERE `tblInviteList`.`status` = 0 ' .
					'AND `tblInviteList`.`travelerID` = ' . GaString::makeSqlSafe($opts['travelerID']) . ' ' .
					'AND `tblInviteList`.`groupID` = ' . GaString::makeSqlSafe($opts['groupID']) . ' ' .
					'LIMIT 0,1';
					
			$rs = new iRecordset($db->execute($sql));
			foreach($rs as $row){
				$domainConfig = new Con_fig($_SERVER['SERVER_NAME']);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();
				
				try{
					$group = GroupFactory::instance()->create(array($row['groupID']));
					$group = $group[0];
					$traveler = new Traveler($row['travelerID']);
					$vars = array(	'recipient' => $traveler->getUserName(),
									'group'		=> $group,
									'siteName'	=> $siteName,
									'siteUrl'	=> $siteUrl,
									'customMessage'	=>	$group->getCustomMessage() );
					
					$notification = new Notification;
					$notification->setMessage(NotificationComposer::compose($vars,'tpl.GroupInvitationNotificationTemplate.php'));
					$notification->setSubject("Invitation from group " . $group->getName());
					$notification->setSender($domainConfig->getAdminEmail());
					$notification->setSenderName($domainConfig->getEmailName());
					$notification->addRecipient($traveler->getTravelerProfile()->getEmail());
					$this->mNotifications[] = $notification;
				}
				catch(exception $e){} // catch invalid travelerID or group ID
				
			}*/
			
		}
	}

?>