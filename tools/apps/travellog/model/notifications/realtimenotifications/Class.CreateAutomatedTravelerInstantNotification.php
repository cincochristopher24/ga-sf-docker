<?php
	
	/**
	* create a notification every time a traveler is created using the addTravelers tool for proworld only
	**/
	
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.Crypt.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');

	require_once('travellog/model/notifications/Class.NotificationSender.php');

	class CreateAutomatedTravelerInstantNotification implements InstantNotification {
		
		const CB_PROWORLD	= 245;
		const CB_IEP		= 33;
		const CB_KAYA		= 2961;
		const TEST_GROUP	= 3;
		const CB_BARCELONA	= 3301;
		const CB_GEO		= 633;
		const CB_CIS		= 241;
		const CB_ISV		= 1760;
		const CB_ADELANTE	= 2359;
		
		private $mNotifications = array();
		private $acceptedGroups	= array(self::CB_PROWORLD, self::CB_IEP, self::CB_KAYA, self::TEST_GROUP, self::CB_BARCELONA, self::CB_GEO, self::CB_CIS, self::CB_ISV, self::CB_ADELANTE);
		
		private $cbDetails		= array(
			'template'	=> array(
				self::CB_PROWORLD	=> 'tpl.ProworldAutomatedTravelerNotificationTemplate.php',
				self::CB_IEP		=> 'tpl.IEPAutomatedTravelerNotificationTemplate.php',
				self::CB_KAYA		=> 'tpl.KayaAutomatedTravelerNotificationTemplate.php',
				self::TEST_GROUP	=> 'tpl.TestGroupAutomatedTravelerNotificationTemplate.php',
				self::CB_BARCELONA	=> 'tpl.BarcelonaAutomatedTravelerNotificationTemplate.php',
				self::CB_GEO		=> 'tpl.GeoVisionsAutomatedTravelerNotificationTemplate.php',
				self::CB_CIS		=> 'tpl.CISAutomatedTravelerNotificationTemplate.php',
				self::CB_ISV		=> 'tpl.ISVAutomatedTravelerNotificationTemplate.php',
				self::CB_ADELANTE	=> 'tpl.AdelanteAutomatedTravelerNotificationTemplate.php',
			),
			'bcc'		=> array(
				self::CB_PROWORLD	=> 'admin@myproworld.org',
				self::CB_IEP		=> 'info@iepabroad.com',
				self::CB_KAYA		=> 'info@kayavolunteer.com',
				self::TEST_GROUP	=> 'zarisdafni@gmail.com',
				self::CB_BARCELONA	=> 'info@barcelonasae.com',
				self::CB_GEO		=> 'alexandra@geovisions.org',
				self::CB_CIS		=> 'thea.moraleta@goabroad.com',
				self::CB_ISV		=> 'emailus@isvonline.org',
				self::CB_ADELANTE	=> 'zarisdafni@gmail.com',
			)
		);
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'TRAVELERS'		=> array(),
				'GROUP_ID'		=> 0,
				'FORCE_RESEND'	=> false,
				'NOTIF_GROUPNAME' => '',
			);
			$options = array_merge($default, $options);
			
			// we will send notifs to accepted groups
			if( in_array($options['GROUP_ID'], $this->acceptedGroups ) ){
				$domainConfig 	= Config::findByGroupID($options['GROUP_ID']);
				
				// get dbHandler
				$dbHandler = new dbHandler();
				
				if( self::CB_PROWORLD == $options['GROUP_ID'] ){
					$siteName 		= 'ProWorld Service Corps';
					$siteUrl 		= 'www.MyProWorld.NET';
					$siteHomeUrl	= 'www.myproworld.org';
					$senderName		= 'ProWorld';
					$subject		= 'Your ProWorld.NET Invitation';
				}
				else{
					$senderName		= $domainConfig->getEmailName();
					$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$senderName);
					$subject		= 'Your '.$siteName.' Invitation';
					$siteUrl		= $domainConfig->getServerName();
					$siteHomeUrl	= $domainConfig->getUrl();
				}
				$sender		= $domainConfig->getGanetAdminEmail();
				$replyTo	= $domainConfig->getAdminEmail();
				$bcc 		= $this->cbDetails['bcc'][$options['GROUP_ID']];
				$template	= $this->cbDetails['template'][$options['GROUP_ID']];				
				$group_id	= $options['GROUP_ID'];
				$notifGrpName = $options['NOTIF_GROUPNAME'];
				
				foreach( $options['TRAVELERS'] as $iTraveler ){
					$profile = $iTraveler->getTravelerProfile();
					$email = $profile->getEmail();
					
					// check if traveler was already sent a notification from this group_id
					$traveler_id = $iTraveler->getTravelerID();
						
					$sql = "SELECT * FROM tblAutomatedTravelerSentNotificationList WHERE travelerID = $traveler_id AND groupID = $group_id";
					$rs = new iRecordset($dbHandler->execute($sql));
					
					// even if the traveler is already in the list, there maybe an instance where we need to resend a notification to this traveler, 
					// one instance is when we use the manage traveler tool in admin where the email is updated and resent.
					if( $rs->retrieveRecordcount() && !$options['FORCE_RESEND'] ){
						// we will not send again
						continue;
					}
					else{
						if( !$rs->retrieveRecordcount() ){
							// insert record
							$insert = "INSERT INTO `Travel_Logs`.`tblAutomatedTravelerSentNotificationList`(`travelerID` ,`groupID` ,`dateSent`)
									   VALUES ($traveler_id, $group_id, NOW( )) ; ";
							$dbHandler->execute($insert);
						}
						
					}
					
					if( GaString::isValidEmailAddress($email) ){
						
						$vars = array(
							'siteUrl' 		=> $siteUrl,
							'siteHomeUrl'	=> $siteHomeUrl,
							'siteName'		=> $siteName,
							'traveler'		=> $iTraveler,
							'profile'		=> $profile,
							'senderName'	=> $senderName,
							'notifGrpName' => $notifGrpName
						);
						
						$notification = new Notification;
						$notification->setSender($sender);
						$notification->setSenderName($senderName);
						$notification->addReplyTo($replyTo, $senderName);
						$notification->setMessage(NotificationComposer::compose($vars, $template));
						$notification->setSubject($subject);
						$notification->addRecipient($email);
						$notification->addBccRecipient($bcc);
						
						// bcc russell in proworld, IEP and CIS
						if( self::CB_PROWORLD == $group_id || self::CB_IEP == $group_id){
							$notification->addBccRecipient('thea.moraleta@goabroad.com');
						}
						
						// bcc thea in barcelona
						if( self::CB_BARCELONA == $group_id){
							$notification->addBccRecipient('thea@goabroad.com');
						}
						
						$this->mNotifications[] = $notification;
					}
					
				}
			}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
			/***
			foreach($this->mNotifications as $iNotification)
				$iNotification->send(true);
			***/
		}
	}

?>