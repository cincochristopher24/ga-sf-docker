<?php

	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	class RequestFriendInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'FRIEND' 	=> null, 
				'DOER'		=> null
			);
			$options 		= array_merge($default, $options);
			$friend			= $options['FRIEND'];
			$doer			= $options['DOER'];
			//$domainConfig	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 	= SiteContext::getInstance();
			$emailName		= $domainConfig->getEmailName();
			$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl 		= $domainConfig->getServerName();
			
			if( !$friend instanceof Traveler || !$doer instanceof Traveler )
				return;
			
			// check if recipient's notification preference is set to can receive notification
			require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
			$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($friend->getTravelerID());
			if( FALSE == $canReceiveNotification ){
				return;
			}
			// end check
			
			$footerVars 	= array(
				'siteName'	=> $siteName,
				'siteUrl'	=> $siteUrl
			);
			
			$tplVars 		= array_merge($footerVars, array(
				'friend' 	=> $friend,
				'doer'		=> $doer,
				'emailName'	=> $emailName,
				'notificationFooter' => NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
			));
			$notification = new Notification;
			$notification->setSender($domainConfig->getGanetAdminEmail());
			$notification->setSenderName($emailName);
			$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
			$notification->setSubject("You have a new friend request on $siteName");
			$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.RequestFriendNotificationTemplate.php'));
			$notification->addRecipient($friend->getTravelerProfile()->getEmail());
			$this->mNotifications[] = $notification;
		}
	}

?>