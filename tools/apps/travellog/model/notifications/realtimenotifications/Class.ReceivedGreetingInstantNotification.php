<?php
	
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/Class.Comment.php');
	require_once('travellog/model/Class.TravelerProfile.php');
	require_once('Class.GaDateTime.php');

	class ReceivedGreetingInstantNotification implements InstantNotification {
		private $mNotifications = array();
		
		public function createNotifications($options = array()){
			$default = array(
				'COMMENT'	=> null,
				'FOR_ADMIN'	=> true,
				'FOR_RECEIVER'	=> true,
				'POSTED_BY_OWNER' => FALSE
			);
			$options = array_merge($default,$options);
			$comment = $options['COMMENT'];
			if( !($comment instanceof Comment ))
				return;
			$fromSite = $comment->getFromSite();
			try{
				$config 	= SiteContext::getInstanceByDomainName($fromSite);
				$emailName	= $config->getEmailName();
				$sender 	= $config->getGanetAdminEmail();
				$replyTo	= $config->getAdminEmail();
				$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			
				$author		= new TravelerProfile($comment->getAuthor());
				if(0 < $comment->getAuthor())
					$authorName	= $author->getUsername();
				else
					$authorName = $comment->getName();
				
				$canReceiveNotification = TRUE;
					
				$contextObj = $comment->getContext();
				switch( $comment->getCommentType() ){
					case CommentType::$VIDEO : // video of a traveler received a greeting
						$owner = ( $contextObj instanceof VideoAlbumToVideo ) ? 
									$contextObj->getParentAlbum()->getOwner() : 
									$contextObj->getParentTravelLog()->getOwner();
						$link = $contextObj->getFriendlyUrl();
						
						if( $owner instanceof Traveler ){ 
							
							// check if recipient's notification preference is set to can receive notification
							require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
							$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($owner->getTravelerID());
							
							$recipientName		= $owner->getUserName();
							$recipient			= $owner->getTravelerProfile()->getEmail();
							$message		= 'Your video received a new greeting from '.$authorName;
							$adminMessage	= GaString::makePossessive($recipientName).' video received a new greeting from '.$authorName ;
							$adminMessage1	= 'Shout-out posted by '.$authorName.' needs approval';							
						}
						else if( $owner instanceof Group ){
							$recipientName		= $owner->getAdministrator()->getUserName();
							$recipient			= $owner->getAdministrator()->getTravelerProfile()->getEmail();
							$message		= 'Your group '.GaString::makePossessive($owner->getName()).' video received a new greeting from '.$authorName;
							$adminMessage	= 'Group '.GaString::makePossessive($owner->getName()).' video received a new greeting from ' . $authorName;
							$adminMessage1	= 'Shout-out posted by '. $authorName .' needs approval';
						
							$parentID = 0 < $owner->getParentID() ? $owner->getParentID() : $owner->getGroupID(); 
							$cb = ( $cb	= Config::findByGroupID($parentID) ) ? $cb : null;
							
							if ( !is_null($cb) ){
								$fromSite	= $cb->getServerName();
								$sender 	= $cb->getGanetAdminEmail();
								$emailName	= $cb->getEmailName();
								$replyTo	= $cb->getAdminEmail();
								$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
							}
						}
						
						break;
						
					case CommentType::$TRAVELLOG : // journal-entry of a traveler received a greeting
						$owner					= $contextObj->getOwner();
						//$link					= 'journal-entry.php?action=view&travellogID=' . $contextObj->getTravelLogID();
						$link					= $contextObj->getFriendlyURL();
						if( $owner instanceof Traveler ){ 
							
							// check if recipient's notification preference is set to can receive notification
							require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
							$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($owner->getTravelerID());
							/*if( FALSE == $canReceiveNotification ){
								return;
							}*/
							// end check
							
							$recipientName		= $owner->getUserName();
							$recipient			= $owner->getTravelerProfile()->getEmail();
							$message		= 'Your journal-entry received a new greeting from ' . $authorName;
							$adminMessage	= GaString::makePossessive($recipientName) . ' journal-entry received a new greeting from ' . $authorName ;
							$adminMessage1	= 'Shout-out posted by '. $authorName .' needs approval';							
						}
						else{ // owner is group
							$recipientName		= $owner->getAdministrator()->getUserName();
							$recipient			= $owner->getAdministrator()->getTravelerProfile()->getEmail();
							$message		= 'Your group ' . GaString::makePossessive($owner->getName()) . ' journal-entry received a new greeting from ' . $authorName;
							$adminMessage	= 'Group ' . GaString::makePossessive($owner->getName()) . ' journal-entry received a new greeting from ' . $authorName;
							$adminMessage1	= 'Shout-out posted by '. $authorName .' needs approval';
							/** 
							 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
							 *				cobrand site and recipient is admin of group, use cobrand
							 */
							// if group has a cobrand site use respective cobrand, else, use dotNet
							//$cb = ( $cb	= Config::findByGroupID($owner->getGroupID())) ? $cb : Config::findByGroupID();
							
							// edited by chris: check first if there's a parentID and we will only use the parentID to find the cobrand group
							$parentID = 0 < $owner->getParentID() ? $owner->getParentID() : $owner->getGroupID(); 
							$cb = ( $cb	= Config::findByGroupID($parentID) ) ? $cb : null;
							
							if ( !is_null($cb) ){
								$fromSite	= $cb->getServerName();
								$sender 	= $cb->getGanetAdminEmail();
								$emailName	= $cb->getEmailName();
								$replyTo	= $cb->getAdminEmail();
								$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
							}
						}
						break;
					case CommentType::$PHOTO :
						$photoContext			= $contextObj->getContext();
						$adminMessage1			= 'Shout-out posted by ' . ($author instanceof AdminGroup ? 'the group ' . $author->getName() : $author->getUserName()) .' needs approval';
						
						switch($contextObj->getPhotoTypeID()){
							case PhotoType::$PROFILE : // profile photo
							
								// check if recipient's notification preference is set to can receive notification
								require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
								$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($photoContext->getTravelerID());
								if( FALSE == $canReceiveNotification ){
									return;
								}
								// end check
							
								$link			= 'photomanagement.php?action=vfullsize&cat=profile&genID=' . $photoContext->getTravelerID() . '&photoID=' . $contextObj->getPhotoID() ;
								$message		= 'Your photo received a new greeting from ' . $authorName;
								$recipientName	= $photoContext->getUserName();
								$recipient		= $photoContext->getEmail();
								$adminMessage	= GaString::makePossessive($recipientName) . ' photo received a new greeting from ' . $authorName;
								break;
							case PhotoType::$TRAVELLOG : // journal-entry photo
								$owner			= $photoContext->getOwner();
								$link			= 'journal-entry.php?action=view&travellogID=' . $photoContext->getTravelLogID();
								if($owner instanceof Traveler){
									
									// check if recipient's notification preference is set to can receive notification
									require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
									$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($owner->getTravelerID());
									if( FALSE == $canReceiveNotification ){
										return;
									}
									// end check
									
									$recipientName = $owner->getUserName();
									$recipient	= $owner->getTravelerProfile()->getEmail();
									$message	= 'Your photo received a new greeting from ' . $authorName;
									$adminMessage = GaString::makePossessive($recipientName) . ' photo received a new greeting from ' . $authorName;
								}
								else{
									$recipientName = $owner->getName();
									$recipient	= $owner->getAdministrator()->getTravelerProfile()->getEmail();
									$message	= 'Your group ' . GaString::makePossessive($owner->getName()) . ' photo received a new greeting from ' . $authorName;
									$adminMessage = 'Group ' . GaString::makePossessive($owner->getName()) . ' photo received a new greeting from ' . $authorName;

									/** 
									 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
									 *				cobrand site and recipient is admin of group, use cobrand
									 */
									// if group has a cobrand site use respective cobrand, else, use dotNet
									$cb = ( $cb	= Config::findByGroupID($owner->getGroupID())) ? $cb : Config::findByGroupID();
									if ( !is_null($cb) ){
										$fromSite	= $cb->getServerName();
										$sender 	= $cb->getGanetAdminEmail();
										$emailName	= $cb->getEmailName();
										$replyTo	= $cb->getAdminEmail();
										$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
									}
								}
								break;
							case PhotoType::$PHOTOALBUM : // photo album
								$owner			= $photoContext->getGroup();
								$link			= 'photomanagement.php?action=vfullsize&cat=photoalbum&groupID=' . $owner->getGroupID() . '&genID=' . $photoContext->getPhotoAlbumID() . '&photoID=' . $contextObj->getPhotoID();
								$recipient		= $owner->getAdministrator()->getTravelerProfile()->getEmail();
								$recipientName	= $owner->getAdministrator()->getUserName();
								$message		= 'Your group ' . GaString::makePossessive($owner->getName()) . ' photo received a new greeting from ' . $authorName;
								$adminMessage	= 'Group ' . GaString::makePossessive($owner->getName()) . ' photo received a new greeting from ' . $authorName;

								/** 
								 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
								 *				cobrand site and recipient is admin of group, use cobrand
								 */
								// if group has a cobrand site use respective cobrand, else, use dotNet
								$cb = ( $cb	= Config::findByGroupID($owner->getGroupID())) ? $cb : Config::findByGroupID();
								if ( !is_null($cb) ){
									$fromSite	= $cb->getServerName();
									$sender 	= $cb->getGanetAdminEmail();
									$emailName	= $cb->getEmailName();
									$replyTo	= $cb->getAdminEmail();
									$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
								}
								break;
						}
						break;
					case CommentType::$ARTICLE :
						$owner					= $contextObj->getTraveler();
						//$link					= 'journal-entry.php?action=view&travellogID=' . $contextObj->getTravelLogID();
						$link					= '/article.php?action=view&articleID=' . $contextObj->getArticleID();
						if( $owner instanceof Traveler ){ 
							
							// check if recipient's notification preference is set to can receive notification
							require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
							$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($owner->getTravelerID());
							/*if( FALSE == $canReceiveNotification ){
								return;
							}*/
							// end check
							
							$recipientName		= $owner->getUserName();
							$recipient			= $owner->getTravelerProfile()->getEmail();
							$message		= 'Your article received a new greeting from ' . $authorName;
							$adminMessage	= GaString::makePossessive($recipientName) . ' article received a new greeting from ' . $authorName;
							$adminMessage1	= 'Shout-out posted by '. $authorName .' needs approval';
						}
						else{ // owner is group
							$recipientName		= $owner->getAdministrator()->getUserName();
							$recipient			= $owner->getAdministrator()->getTravelerProfile()->getEmail();
							$message		= 'Your group ' . GaString::makePossessive($owner->getName()) . ' article received a new greeting from ' .$authorName;
							$adminMessage	= 'Group ' . GaString::makePossessive($owner->getName()) . ' article received a new greeting from ' . $authorName;
							$adminMessage1	= 'Shout-out posted by '. $authorName .' needs approval';
						
							/** 
							 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
							 *				cobrand site and recipient is admin of group, use cobrand
							 */
							// if group has a cobrand site use respective cobrand, else, use dotNet
							$cb = ( $cb	= Config::findByGroupID($owner->getGroupID())) ? $cb : Config::findByGroupID();
							if ( !is_null($cb) ){
								$fromSite	= $cb->getServerName();
								$sender 	= $cb->getGanetAdminEmail();
								$emailName	= $cb->getEmailName();
								$replyTo	= $cb->getAdminEmail();
								$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
							}
						}
						break;
					default : // Traveler Profile
					
						// check if recipient's notification preference is set to can receive notification
						require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
						$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($contextObj->getTravelerID());
						/*if( FALSE == $canReceiveNotification ){
							return;
						}*/
						// end check
						$owner = $contextObj;
						
						$recipientName			= $contextObj->getUserName();
						$recipient				= $contextObj->getEmail();
						$message			= 'You received a new greeting from ' . $authorName;
						$adminMessage		= $recipientName . ' received a new greeting from ' . $authorName;
						$adminMessage1		= 'Shout-out posted by ' . $authorName .' needs approval';
						$link					= $contextObj->getUserName();
						
						break;
				}
				
				$footerVars		= array(
					'siteName'	=> $siteName,
					'emailName'	=> $emailName,
					'siteUrl'	=> $fromSite
				);
				
				// create notification
				$vars = array_merge($footerVars, array(	
					'author' 			=> $author,
					'comment'			=> $comment,
					'link'				=> $link,
					'gaDateTime'		=> new GaDateTime,
					'recipient'			=> $recipientName,
					'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
				));
				
				// notification for traveler
				if($options['FOR_RECEIVER']) {
					$adminNotified = FALSE;
					if( $canReceiveNotification && !$options['POSTED_BY_OWNER'] ){
						$notification = new Notification;
						$notification->setSubject('New ' . $siteName . ' greeting from ' . $authorName);
						$notification->setSender($sender);
						$notification->setSenderName($emailName);
						$notification->addReplyTo($replyTo, $emailName);
						$notification->setMessage(NotificationComposer::compose(array_merge($vars, array('message' => $message, 'recipientName' => $recipientName)), 'tpl.ReceivedCommentNotificationTemplate.php'));
						$notification->addRecipient($recipient);
						$this->mNotifications[] = $notification;
						// notification for GoAbroad.net Administrator when friend
						if($options['FOR_ADMIN']) {
							$notification = new Notification;
							$notification->setSubject('New ' . $siteName . ' greeting from ' . $authorName);
							$notification->setSender($config->getGanetAdminEmail());
							$notification->setSenderName($config->getEmailName());
							$notification->addReplyTo($config->getAdminEmail(), $config->getEmailName());
							$notification->setMessage(NotificationComposer::compose(array_merge($vars, array('message' => $adminMessage, 'recipientName' => 'GoAbroad Network Administrator')), 'tpl.ReceivedCommentNotificationTemplate.php'));
							$notification->addRecipient('admin@goabroad.net');
							$this->mNotifications[] = $notification;
							$adminNotified = TRUE;
						}
					}
					// notification for others who posted a comment in the journal entry or article
					if( "" != trim($comment->getTheText()) && 
						($comment->getCommentType() == CommentType::$TRAVELLOG ||
						 $comment->getCommentType() == CommentType::$ARTICLE ||
						 $comment->getCommentType() == CommentType::$PROFILE) ){//include profile
						try{
							$relatedComments = $contextObj->getComments();
							$posters = array();
							require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
							foreach($relatedComments AS $relatedComment){
								$temp = $relatedComment->getAuthorObject();
								//include if poster is not the owner and not the poster
								$canReceive = TRUE;
								$posterEmail = "";
								if( $temp->getAuthorType() != CommentAuthor::NON_GANET){
									$tempTraveler = $temp->getAuthorAsTraveler();
									if( $tempTraveler->isSuspended() || $tempTraveler->isDeactivated()){
										$canReceive = FALSE;
									}else{
										$posterEmail = $tempTraveler->getTravelerProfile()->getEmail();
										$canReceive = $recipient != $posterEmail; //recipient not poster
										$canReceive = $canReceive && $posterEmail != $comment->getAuthorObject()->getAuthorAsTraveler()->getTravelerProfile()->getEmail(); //poster not the author of the new comment
										$canReceive = $canReceive && TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($tempTraveler->getTravelerID());//poster can receive notif
										$canReceive = $canReceive && !array_key_exists($posterEmail,$posters);
									}
								}else{
									$posterEmail = $temp->getEmail();
									$canReceive = $posterEmail != $recipient && $posterEmail != $comment->getEmail();
									$canReceive = $canReceive && !array_key_exists($posterEmail,$posters);
								}
								if( $canReceive ){ //still not included 
									$posters[$posterEmail] = $temp;
								}
							}
							if( count($posters) ){
								$owner_possessive = GaString::makePossessive($owner->getName());
								$owner_possessive2 = $owner_possessive;
								if( $options['POSTED_BY_OWNER'] ){
									$profile = NULL;
									if( $owner instanceof Traveler ){
										$profile = $owner->getTravelerProfile();
									}else{
										$profile = $owner->getAdministrator()->getTravelerProfile();
									}
									$owner_possessive2 = "M" == strtoupper($profile->getGender()) ? "his" : "her";
								}
								$_subject = $authorName." posted a shout-out on ".$owner_possessive2;
								$_subject .= $comment->getCommentType() == CommentType::$TRAVELLOG ? " journal entry" : ($comment->getCommentType() == CommentType::$ARTICLE ? " article" : " profile");
								$_message = $authorName." also posted a shout-out on ".$owner_possessive2;
								$_message .= $comment->getCommentType() == CommentType::$TRAVELLOG ? " journal entry.\n" : ($comment->getCommentType() == CommentType::$ARTICLE ? " article.\n" : " profile.\n");
								
								foreach($posters AS $posterEmail => $poster){
									//echo $_subject;
									$endMessage = "\nYou received this notification because you provided this email address when you left a shout-out on ".$owner_possessive;
									if( $poster->getAuthorType() != CommentAuthor::NON_GANET ){
										$endMessage = "\nYou received this notification because you left a shout-out on ".$owner_possessive;
									}
									$endMessage .= $comment->getCommentType() == CommentType::$TRAVELLOG ? " journal entry on " : ($comment->getCommentType() == CommentType::$ARTICLE ? " article on " : " profile on ");
									$endMessage .= $siteName;
									
									$_vars = array_merge($footerVars,
												array(	'comment'			=> $comment,
														'link'				=> $link,
														'gaDateTime'		=> new GaDateTime,
														'showComment'		=> $poster->getAuthorType() != CommentAuthor::NON_GANET,
														'endMessage'		=> $endMessage,
														'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooterForCommentReply.php') ));
									$notification = new Notification;
									$notification->setSubject($_subject);
									$notification->setSender($config->getGanetAdminEmail());
									$notification->setSenderName($config->getEmailName());
									$notification->addReplyTo($config->getAdminEmail(), $config->getEmailName());
									//echo '<textarea rows="35" cols="150">'.NotificationComposer::compose(array_merge($_vars, array('message' => $_message, 'recipientName' => $poster->getName())), 'tpl.CommentReplyNotificationTemplate.php').'</textarea>';
									$notification->setMessage(NotificationComposer::compose(array_merge($_vars, array('message' => $_message, 'recipientName' => $poster->getName())), 'tpl.CommentReplyNotificationTemplate.php'));
									$notification->addRecipient($posterEmail);
									$this->mNotifications[] = $notification;
								}
								//exit;
								if( $options['FOR_ADMIN'] && !$adminNotified ){
									$notification = new Notification;
									$notification->setSubject('New ' . $siteName . ' greeting from ' . $authorName);
									$notification->setSender($config->getGanetAdminEmail());
									$notification->setSenderName($config->getEmailName());
									$notification->addReplyTo($config->getAdminEmail(), $config->getEmailName());
									$notification->setMessage(NotificationComposer::compose(array_merge($vars, array('message' => $adminMessage, 'recipientName' => 'GoAbroad Network Administrator')), 'tpl.ReceivedCommentNotificationTemplate.php'));
									$notification->addRecipient('admin@goabroad.net');
									$this->mNotifications[] = $notification;
									$adminNotified = TRUE;
								}
							}
						}catch(exception $exception){
						}
					}
				}
				// notification for GoAbroad.net Administrator
				if($options['FOR_ADMIN'] && !($options['FOR_RECEIVER'])) {
					$approveUrl  = "";
					$approveUrl .= !(strpos($fromSite,"dev.") === false)	? "dev.": "";
					$approveUrl .= !(strpos($fromSite,"www.") === false)	? "www.": "";
					$approveUrl .= "goabroad.net";
					$approveUrl .= (strpos($fromSite,".local"))	? ".local"	: "";
					$notification = new Notification;
					$notification->setSubject('New ' . $siteName . ' greeting from ' . $authorName);
					$notification->setSender($config->getGanetAdminEmail());
					$notification->setSenderName($config->getEmailName());
					$notification->addReplyTo($config->getAdminEmail(), $config->getEmailName());
					$notification->setMessage(NotificationComposer::compose(array_merge($vars, array('message' => $adminMessage1, 'recipientName' => 'GoAbroad Network Administrator', 'approveUrl' => $approveUrl)), 'tpl.AdminReceivedCommentNotificationTemplate.php'));
					$notification->addRecipient('admin@goabroad.net');
					$this->mNotifications[] = $notification;
				}
			}
			catch(exception $e){}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
			unset($this->mNotifications);
		}
	}
?>