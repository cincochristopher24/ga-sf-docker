<?php
	
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');

	class PostEventInstantNotification implements InstantNotification {
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function createNotifications($options = array()){
			$default = array(
				'EVENT'				=> null,
				'INCLUDE_MESSAGE'	=> false
			);
			$options 	= array_merge($default, $options);
			$event		= $options['EVENT'];
			if( is_null($event) )
				return;
			
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 	= SiteContext::getInstance();
			$emailName 		= $domainConfig->getEmailName();
			$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl 		= $domainConfig->getServerName();
			try{
				$group		= $event->getGroup();
				$members	= $group->getMembers();
				
				foreach( $members as $iMember ){
					if($iMember->isSuspended() || $iMember->isDeactivated()){
						continue;
					}
					// check if recipient's notification preference is set to can receive notification
					$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($iMember->getTravelerID());
					if( FALSE == $canReceiveNotification ){
						continue;
					}
					// end check
					
					$footerVars	= array(
						'group'		=> $group,
						'siteName'	=> $siteName,
						'siteUrl'	=> $siteUrl,
						'isAdvisor'	=> false
					);
					
					$tplVars 	= array_merge($footerVars, array(
						'recipient'			=> $iMember->getUsername(),
						'event'				=> $event,
						'emailName'			=> $emailName,
						'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
					));
					
					$notification 	= new Notification;
					$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.PostEventInstantNotificationTemplate.php'));
					$notification->setSubject('New event posted on '.$group->getName());
					$notification->setSender($domainConfig->getGanetAdminEmail());
					$notification->setSenderName($emailName);
					$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
					$notification->addRecipient($iMember->getTravelerProfile()->getEmail());
					$this->mNotifications[] = $notification;
					break;
				}
			}
			catch(exception $e){}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
	}

?>