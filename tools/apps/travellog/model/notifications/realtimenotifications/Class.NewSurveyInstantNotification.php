<?php

require_once 'travellog/model/notifications/interfaces/Interface.InstantNotification.php';
require_once('travellog/model/notifications/Class.Notification.php');
require_once('travellog/model/notifications/Class.NotificationSender.php');
require_once('travellog/model/notifications/Class.NotificationComposer.php');
require_once('travellog/model/Class.AdminGroup.php');
require_once('travellog/model/Class.SiteContext.php');

class NewSurveyInstantNotification implements InstantNotification {
	
	private $_mCollNotifications = array();
	
	public function createNotifications($options = array()) {
		$domainConfig = SiteContext::getInstance();
		
		$survey = $options['survey'];
		
		$params = array();
		
		$params['formID'] = $survey->getSurveyFormID();
		$params['surveyName'] = $survey->getName();
		
		$params['adminEmail']     = $domainConfig->getAdminEmail();
		$params['adminEmailName'] = $domainConfig->getEmailName();

		$params['ganetAdminEmail'] = $domainConfig->getGanetAdminEmail();
		
		$params['subject'] = 'You have a new survey to fill up on GoAbroad.net';
		
		foreach ($survey->getParticipants() as $participant) {
			$group = new AdminGroup($participant->getGroupID());
			
			if (!($group instanceof AdminGroup))
				continue;

			$params['group']    = $group;
			$params['groupID']  = $group->getGroupID();
			
			$params['siteName'] = $group->getName();
			$params['siteUrl']  = $_SERVER['SERVER_NAME'];
			
			$params['footer']   = NotificationComposer::compose($params, 'tpl.IncNotificationFooter.php');
			
			$members = $group->getMembers();
			
			foreach ($members as $traveler) {
				
				// check if recipient's notification preference is set to can receive notification
				$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($traveler->getTravelerID());
				if( FALSE == $canReceiveNotification || $traveler->isSuspended() || $traveler->isDeactivated()){
					continue;
				}
				// end check
				
				$params['isAdvisor']      = $traveler->getTravelerID() == $group->getAdministratorID();
				$params['recipientEmail'] = $traveler->getTravelerProfile()->getEmail();
				$params['recipient']      = $traveler->getUserName();
				
				$this->_mCollNotifications[] = $this->makeNotification($params);
			}
		}
	}
	
	protected function makeNotification($params) {
		$message = NotificationComposer::compose($params, 'tpl.NewSurveyInstantNotification.php');

		$notification = new Notification;
		
		$notification->setMessage    ( $message );
		$notification->setSubject    ( $params['subject']        );
		$notification->setSender     ( $params['ganetAdminEmail']);
		$notification->setSenderName ( $params['adminEmailName'] );

		$notification->addReplyTo    ( $params['adminEmail'], $params['adminEmailName'] );
		
		$notification->addRecipient  ( $params['recipientEmail'] );

		return $notification;
	}
	
	public function send($isInstant = true) {
		NotificationSender::getInstance()->send($this->_mCollNotifications, $isInstant);
	}
	
}