<?php
	/**
	# this notification will not fetch data from logs but from physical table
	**/

	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.dbHandler.php');
	require_once('Class.GaString.php');
	
	class GroupInviteStaffInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function createNotifications($options = array()){
			$this->mNotifications= array();
			$default = array(
				'TRAVELER'	=> null,
				'GROUP'		=> null
			);
			$options 		= array_merge($default, $options);
			$traveler 		= $options['TRAVELER'];
			$group 			= $options['GROUP'];
			
			if( is_null($group) || is_null($traveler) )
				return;
			
			// check if recipient's notification preference is set to can receive notification
			require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
			$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($traveler->getTravelerID());
			if( FALSE == $canReceiveNotification ){
				return;
			}
			// end check
			
			$parentGroup 	= !is_null($parentGroup = $group->getParent()) ? $parentGroup : $group;
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 	= SiteContext::getInstance();
			$emailName		= $domainConfig->getEmailName();
			$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl 		= $domainConfig->getServerName();
			
			$footerVars		= array(
				'siteName'	=> $siteName,
				'emailName'	=> $emailName,
				'siteUrl'	=> $siteUrl
			);
			
			$vars = array_merge($footerVars, array(	
				'recipient'	=> $traveler->getUserName(),
				'parent'	=> $parentGroup,
				'subgroup' 	=> $group,
				'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
			));
							
			$notification = new Notification;
			$notification->setSender($domainConfig->getGanetAdminEmail());
			$notification->setSenderName($emailName);
			$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
			$notification->setSubject($parentGroup->getName() . ' invites you to be a staff on their group!');
			$notification->setMessage(NotificationComposer::compose($vars, 'tpl.GroupInviteStaffNotificationTemplate.php'));
			$notification->addRecipient($traveler->getTravelerProfile()->getEmail());
			$this->mNotifications[] = $notification;
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
	}

?>