<?php
	
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/Class.Travel.php');
 	require_once('travellog/model/Class.AdminGroup.php');

	class AdminDeleteArticleEntryInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'ARTICLE_ENTRY' => null,
			);
			$options = array_merge($default,$options);
			
			if( !$options['ARTICLE_ENTRY'] )
				return;
			
			try{				
				$config 	= SiteContext::getInstance();
				$emailName	= $config->getEmailName();
				$siteName	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				$siteUrl	= $config->getServerName();
				
				if (FALSE === strpos($siteUrl, "dev.") && (strpos($_SERVER["SERVER_NAME"], "dev.") || 0 === strpos($_SERVER["SERVER_NAME"], "dev."))) {
					$siteUrlTokens = explode(".", $siteUrl);
					$temp = array ();
					foreach ($siteUrlTokens as $token) {
						if ("goabroad" == strtolower($token)) {
							$temp[] = "dev";
						}
						$temp[] = strtolower($token);
					}
					$siteUrl = join(".", $temp);
				}
					
				$article_entry = $options['ARTICLE_ENTRY'];
				$recipientEmail = $article_entry->getTraveler()->getTravelerProfile()->getEmail();
				
				// check if recipient's notification preference is set to can receive notification
				require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
				$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($article_entry->getTraveler()->getTravelerID());
				if( FALSE == $canReceiveNotification ){
					return;
				}
				// end check
					
				$footerVars		= array(
					'siteName'	=> $siteName,
					'emailName'	=> $emailName,
					'siteUrl'	=> $siteUrl
				);
				
				$tplVars		= array_merge($footerVars, array(
					//'notificationFooter' => NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php'),
					'termsLink'	=> $siteUrl."/terms.php",
					'recipientName'	=> $article_entry->getTraveler()->getUsername(),
					'title'     => $article_entry->getTitle()
				));
								
				$notification = new Notification;
				$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.AdminDeleteArticleEntryNotificationTemplate.php'));
				$notification->setSubject("Message from GoAbroad Network Admin");
				$notification->setSender($config->getGanetAdminEmail());
				$notification->setSenderName($emailName);
				$notification->addReplyTo($config->getAdminEmail(), $emailName);
				$notification->addRecipient($recipientEmail);
				$this->mNotifications[] = $notification;
			}
			catch(exception $e){}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
	}

?>