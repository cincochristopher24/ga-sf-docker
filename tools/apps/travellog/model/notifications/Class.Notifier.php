<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('Class.GaDateTime.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('Class.NotificationSender.php');
	class Notifier
	{
		private static $queues = array();
		
		public static function sendNotifications($paramStartDate=null,$paramEndDate=null){
			$trace = debug_backtrace();
			$curTrace = $trace[0];
			if(is_null($paramStartDate) || is_null($paramEndDate)){
				$endDate   	= GaDateTime::dbDateTimeFormat(); 
				$startDate 	= GaDateTime::dbDateTimeFormat(GaDateTime::dateAdd($endDate,'h','-12')); 
			}
			else{
				$startDate 	= $paramStartDate;
				$endDate   	= $paramEndDate;
			}
			if(!GaDateTime::isValidDate($startDate)){
				throw new NotificationException(NotificationException::INVALID_DATE,$curTrace,'StartDate: '.$startDate);
			}
			if(!GaDateTime::isValidDate($endDate)){
				throw new NotificationException(NotificationException::INVALID_DATE,$curTrace,'EndDate: '.$endDate);
			}
			if(1 == GaDateTime::dateCompare($startDate,$endDate)){
				throw new NotificationException(NotificationException::INVALID_DATE_RANGE);
			}
			//get all the pending notifications 
			echo '<pre>';
			var_dump(self::$queues);
			echo '</pre>';
			
			foreach(self::$queues as $iQueue => $iQueueObj){
				echo '<br />===== Start '.$iQueue.'=====<br />';
				$notifications = $iQueueObj->getNotifications($startDate,$endDate);				
				NotificationSender::send($notifications);
				/***
				foreach($notifications as $iNotification){
					//$result = $iNotification->send();
					
				}
				if($iQueueObj instanceof ClearableNotificationQueue){
					$iQueueObj->clear();
				}
				***/
				echo '<br />===== End '.$iQueue.'=====<br />';
			}
		}
		
		public static function registerNotificationQueue(NotificationQueue $queue, $key){
			self::$queues[$key] = $queue;
		}
		
		public static function getAllNotificationQueues(){
			return self::$queues;
		}
		
		public static function initQueues(){
			$osDelimiter =  '/';
			$arDir = explode($osDelimiter,__FILE__);
			$strategyDir = implode($osDelimiter, array_slice($arDir,0,count($arDir)-1)).$osDelimiter.'queues';
			$strategyFiles = scandir($strategyDir);
			foreach($strategyFiles as $iFile){
				//check if it is a php file
				if('NotificationQueue.php' == substr($iFile,strlen($iFile)-21, 21)){
					$comKey = strtoupper(str_replace(array('Class.','NotificationQueue.php'),'',$iFile));
					$clsName = str_replace(array('Class.','.php'),'',$iFile);
					require_once('travellog/model/notifications/queues/'.$iFile);
					self::registerNotificationQueue(new $clsName(),$comKey);
				}
			}
		}
		
		/*** This function only sends buffered notifications ***/
		public static function sendBufferedNotifications(){
			require_once('travellog/model/notifications/queues/Class.BufferedNotificationQueue.php');
			$bufferedQueue = new BufferedNotificationQueue();
			$notifications = $bufferedQueue->getNotifications();
			foreach($notifications as $iNotification){
				
			}
		}
	}
	Notifier::initQueues();
?>
