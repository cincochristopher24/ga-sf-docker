<?php
	require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
	require_once('travellog/model/notifications/Class.Notifier.php');
	require_once('Class.Template.php');
	class NotificationTemplate {
		
		private static $instance 			= null;
		private static $defaultTemplateFiles= array();
		private static $defaultPath			= null;
		
		private function __construct(){}
		
		private function getDefaultTemplatesByNotifications($notifications=array()){
			$arr = array();
			foreach( $notifications as $key ){
				if( array_key_exists($key, self::$defaultTemplateFiles) ){
					$tpl = new Template;
					$tpl->set_path(self::$defaultPath.DIRECTORY_SEPARATOR);
					$arr[$key] = array(
						'name'		=> self::$defaultTemplateFiles[$key],
						'instance'	=> $tpl
					);
				}
			}
			return $arr;
		}
		
		public function getDefaultInstantNotificationTemplates(){
			$notifications = array_keys(RealTimeNotifier::getAllInstantNotifications());
			return $this->getDefaultTemplatesByNotifications($notifications);
		}
		
		public function getDefaultNotificationQueueTemplates(){
			$notifications = array_keys(Notifier::getAllNotificationQueues());
			return $this->getDefaultTemplatesByNotifications($notifications);
		}
		
		public static function getInstance(){
			if( is_null(self::$instance) ) self::$instance = new self;
			return self::$instance;
		}
		
		public static function registerAllDefaultTemplates(){
			$viewsPath		= 'views'.DIRECTORY_SEPARATOR.'notifications'.DIRECTORY_SEPARATOR.'defaultSamples';
			$pathToFiles 	= DIRNAME(DIRNAME(DIRNAME(__FILE__))).DIRECTORY_SEPARATOR.$viewsPath;
			$templateFiles	= scandir($pathToFiles);
			foreach( $templateFiles as $iFile ){
				if('Sample.php' == substr($iFile,strlen($iFile)-10, 10)){
					$key	= strtoupper(str_replace(array('tpl.','Sample.php'),'',$iFile));
					self::$defaultTemplateFiles[$key] = $iFile;
				}
			}
			self::$defaultPath = 'travellog'.DIRECTORY_SEPARATOR.$viewsPath;
		}
	}
	NotificationTemplate::registerAllDefaultTemplates();
?>