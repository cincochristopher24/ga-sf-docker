<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/Class.Traveler.php');

	require_once('travellog/model/Class.PostPeer.php');
	require_once('travellog/model/Class.PosterPeer.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	
	class TravelerSavesPostNotificationQueue implements NotificationQueue, ClearableNotificationQueue{
		
		private $mReader 		= NULL;
		private $mNotifications = array();
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = "(logView._EXECUTION_DATE_ >= " . GaString::makeSqlSafe($startDate) . " AND " .
							  "logView._EXECUTION_DATE_ <= " . GaString::makeSqlSafe($endDate) . ")";
			
			$inclusiveGroups = implode(',', DiscussionPeer::$_DIGEST_ENABLED_GROUPS);
			$sql = "
				SELECT logView.* <$, `tblTopics`.`groupID` $>  FROM `DiscussionBoard`.`tblPosts` as logView 
				<$, `DiscussionBoard`.`tblDiscussions` $> 
				<$, `DiscussionBoard`.`tblTopics` $>
				WHERE logView.`_EVENT_` = 1 AND $rangeCriteria
				AND logView.`_NEW_discussionID` = <$ `tblDiscussions`.`ID` $> 
				AND <$ `tblDiscussions`.`topicID` $> = <$ `tblTopics`.`ID` $> 
				AND <$ `tblTopics`.`groupID` $> IN (<$
					SELECT `tblGroup`.`groupID` FROM `Travel_Logs`.`tblGroup`
					WHERE (`tblGroup`.`groupID` IN ($inclusiveGroups) OR `tblGroup`.`parentID` IN ($inclusiveGroups))
				$>)
			";
			$rs = $this->mReader->getLogsBySql($sql);
			$rs->dump();
			$this->createNotification($this->groupData($rs->retrieveData()));
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				$temp = isset($items[$row['_DOMAIN_NAME_']][$row['groupID']][$row['_NEW_discussionID']]['travelers']) 
					? $items[$row['_DOMAIN_NAME_']][$row['groupID']][$row['_NEW_discussionID']]['travelers'] : array();

				if(!in_array($row['_NEW_posterID'], $temp)){
					$items[$row['_DOMAIN_NAME_']][$row['groupID']][$row['_NEW_discussionID']]['travelers'][] = $row['_NEW_posterID'];
				}
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
				$emailName 		= $domainConfig->getEmailName();
				$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				$siteUrl 		= $domainConfig->getServerName();
				
				
				foreach($iGroupedData as $gID => $details){
					$travelers = array();
					try{
						$arrGrp = GroupFactory::instance()->create(array($gID));
						$group = $arrGrp[0];
						
						$parentID = 0 < $group->getParentID() ? $group->getParentID() : $group->getGroupID(); 
						$cb = ( $cb	= Config::findByGroupID($parentID)) ? $cb : null;
						if ( !is_null($cb) ){
							$siteUrl	= $cb->getServerName();
							$sender 	= $cb->getAdminEmail();
							$emailName	= $cb->getEmailName();
							$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
						}
						else{
							$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
							$emailName 		= $domainConfig->getEmailName();
							$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
							$siteUrl 		= $domainConfig->getServerName();
						}

						$footerVars = array(
							'isAdvisor'	=> true,
							'group'		=> $group,
							'siteName'	=> $siteName,
							'siteUrl'	=> $siteUrl
						);
						
						$vars = array_merge($footerVars, array(	
							'recipient' => $group->getAdministrator()->getUserName(),
							'data' 		=> $details,
							'group'		=> $group,
							'emailName'	=> $emailName,
							'trace'		=> $trace,
							'notificationError' => NotificationException::OBJECT_INSTANTIATION_ERROR,
							'notificationFooter' => NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
						));

						try{
							$message = NotificationComposer::compose($vars,'tpl.TravelerSavesPostNotificationsTemplate.php');
							$notification = new Notification();
							$notification->setSubject("New Posts for Discussion ");
							$notification->setSender($domainConfig->getGanetAdminEmail());
							$notification->setSenderName($emailName);
							$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
							$notification->setMessage($message);
							if(241 == $group->getGroupID() || 241 == $group->getParentID()){
								$notification->addRecipient("jdebiec@cisabroad.com");
							}else{
								$notification->addRecipient($group->getAdministrator()->getTravelerProfile()->getEmail());
							}
							$this->mNotifications[] = $notification;
						}catch(exception $e){}
					}catch(exception $e){}
				}
			}
		}
	}
?>
