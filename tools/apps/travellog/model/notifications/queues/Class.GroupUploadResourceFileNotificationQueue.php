<?php
	
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/Class.ResourceFiles.php');
	
	require_once('travellog/model/Class.NotificationSetting.php');
	
	require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
	
	class GroupUploadResourceFileNotificationQueue implements NotificationQueue, ClearableNotificationQueue{
		
		private $mReader 		= NULL;
		private $mNotifications = array();
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = '(logView.`_EXECUTION_DATE_` >= ' . GaString::makeSqlSafe($startDate) . ' AND ' .
							  'logView.`_EXECUTION_DATE_` <= ' . GaString::makeSqlSafe($endDate) . ')';
			$sql = 	'SELECT *, `tblNotificationSetting`.`_NEW_messageIncluded`,`tblNotificationSetting`.`_NEW_notifyGroupMembers` ' .
					'FROM `Travel_Logs`.`tblGrouptoResourceFile` logView LEFT JOIN ' .
					'`Travel_Logs`.`tblNotificationSetting` ON ' .
						'logView.`_NEW_resourcefileID` = `tblNotificationSetting`.`_NEW_contextID` ' .
						'AND `tblNotificationSetting`.`_NEW_context` = '.NotificationSetting::GROUP_RESOURCE_CONTEXT.' '.
					'WHERE logView.`_EVENT_` = 1 '.
						'AND ' . $rangeCriteria . ' ' .
						'AND EXISTS (<$ ' .
							'SELECT `groupID` '.
							'FROM `Travel_Logs`.`tblGrouptoResourceFile` ' .
							'WHERE ' .
								'logView.`_NEW_groupID` = `groupID` ' .
								'AND logView.`_NEW_resourcefileID` = `resourcefileID` '.
						'$>) '.
						'AND logView.`_EVENT_` = 1 ' .
						'AND `tblNotificationSetting`.`_NEW_notifyGroupMembers` = 1 '.
					'ORDER BY logView.`_EXECUTION_DATE_` DESC';
			$rs = $this->mReader->getLogsBySql($sql);
			$this->createNotification($this->groupData($rs->retrieveData()));
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['time']))
					$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['time'] = $row['_EXECUTION_DATE_'];
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['resources']) OR !in_array($row['_NEW_resourcefileID'],$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['resources']))
					$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['resources'][] = $row['_NEW_resourcefileID'];
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig 	= new Con_fig($domainName);
				$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
				$emailName		= $domainConfig->getEmailName();
				$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				$siteUrl 		= $domainConfig->getServerName();
				foreach($iGroupedData as $groupID => $details){
					// try block for non-existent groups
					try{
						$arrGrp = GroupFactory::instance()->create(array($groupID));
						$group = $arrGrp[0];
						$time = date('F d, Y g:i:s A',strtotime($details['time']));
						$resources = array();

						// create a resource file object for every resource id
						foreach($details['resources'] as $resource){
							try{
								$resource = new ResourceFiles($resource);	
							}
							catch(exception $e){
								throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR, $trace[0], '[CLASS]ResourceFiles: ' . $e->getMessage());
							}
							$resource->setContext($group);
							$resources[] = $resource;
						}

						// add notification for each member of this group
						foreach($group->getMembers() as $traveler){
							if($traveler->getTravelerID() != $group->getAdministratorID() && !$traveler->isSuspended() && !$traveler->isDeactivated()){
								
								// check if recipient's notification preference is set to can receive notification
								$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($traveler->getTravelerID());
								if( FALSE == $canReceiveNotification ){
									continue;
								}
								// end check
								
								if(!$traveler->isAutomaticallyAdded()){
									
									$footerVars = array(
										'siteName'	=> $siteName,
										'siteUrl'	=> $siteUrl,
										'isAdvisor'	=> false,
										'group'		=> $group
									);
									
									$vars = array_merge($footerVars, array(	
										'recipient' 		=> $traveler->getUsername(),
										'resources' 		=> $resources,
										'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php'),
										'emailName'			=> $emailName,
										'time'				=> $time
									));
									$notification = new Notification();
									$notification->setMessage(NotificationComposer::compose($vars, 'tpl.GroupUploadResourceFileNotificationTemplate.php'));
									$notification->setSubject("New " . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName) . " Resource Files " . (count($resources) > 1 ? "Updates":"Update") . " for " . (($group instanceof AdminGroup) ? "group " : "club ") . $group->getName());
									$notification->setSender($domainConfig->getGanetAdminEmail());
									$notification->setSenderName($emailName);
									$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
									$notification->addRecipient($traveler->getTravelerProfile()->getEmail());
									$this->mNotifications[] = $notification;
								}
							}
						}
					}
					catch(exception $e){
						
					} 
				}
			}
		}
		
	}
?>
