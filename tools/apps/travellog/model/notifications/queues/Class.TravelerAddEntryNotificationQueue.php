<?php
	
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('travellog/model/Class.TravelLog.php');
	
	require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');

	class TravelerAddEntryNotificationQueue implements NotificationQueue {
		
		private $mNotifications = array();
		
		public function getNotifications($startDate, $endDate){
			$this->mNotifications = array();
			$reader = new Reader;
			$rs = $reader->getLogsBySql(
				'SELECT * FROM `Travel_Logs`.`tblTravelLog` as logView <$, `Travel_Logs`.`tblTravelLog`$> ' .
				'WHERE ' .
					'logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe($startDate) . ' AND ' . GaString::makeSqlSafe($endDate) . ' ' . 
					'AND logView.`_EVENT_` = 1 ' .
					'AND logView.`_NEW_travellogID` = <$ `tblTravelLog`.`travellogID`$> ' .
				'ORDER BY `_EXECUTION_DATE_` DESC '
			);
			$rs->dump();
			$this->createNotification($this->groupData($rs));
			return $this->mNotifications;
		}
		
		/*public function send(){
			foreach($this->mNotifications as $notification)
				$notification->send();
		}*/
		
		private function groupData($rs){
			$items = array();
			foreach($rs as $row){
				$items[$row['_DOMAIN_NAME_']][$row['_NEW_travelID']][] 	= $row['_NEW_travellogID'];
				$items[$row['_DOMAIN_NAME_']][$row['_NEW_travelID']] 	= array_unique($items[$row['_DOMAIN_NAME_']][$row['_NEW_travelID']]);
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			
			foreach($groupedData as $domainName => $journals){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
				$emailName		= $domainConfig->getEmailName();
				$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				$siteUrl 		= $domainConfig->getServerName();
				$sender 		= $domainConfig->getGanetAdminEmail();
				$replyTo        = $domainConfig->getAdminEmail();
				
				foreach($journals as $id => $entries){
					try{
						$journal = new Travel($id);
						$owner = $journal->getOwner();
						
						if('Group' == get_parent_class($owner))
							continue;
						
						$newEntries = array();
						foreach($entries as $id){
							try{
								$newEntries[] = new Travellog($id);
							}
							catch(exception $e){continue;}
						}
						
						if(empty($newEntries))
							continue;
						
						$groups = $owner->getGroups();
						
						// send notification to each advisor
						foreach($groups as $iGroup){

							if(!$iGroup instanceof AdminGroup)
								continue;

							/** 
							 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
							 *				cobrand site and recipient is admin of group, use cobrand
							 */
							// if group has a cobrand site use respective cobrand, else, use dotNet
							//$cb = ( $cb	= Config::findByGroupID($iGroup->getGroupID())) ? $cb : Config::findByGroupID();
							
							$parentID = 0 < $iGroup->getParentID() ? $iGroup->getParentID() : $iGroup->getGroupID(); 
							$cb = ( $cb	= Config::findByGroupID($parentID)) ? $cb : null;
							
							if ( !is_null($cb) ){
								$siteUrl	= $cb->getServerName();
								$sender 	= $cb->getGanetAdminEmail();
								$emailName	= $cb->getEmailName();
								$replyTo	= $cb->getAdminEmail();
								$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
							}
							else{
								$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
								$emailName		= $domainConfig->getEmailName();
								$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
								$siteUrl 		= $domainConfig->getServerName();
								$sender 		= $domainConfig->getGanetAdminEmail();
								$replyTo		= $domainConfig->getAdminEmail();
							}
							/*
							// domain is a cobrand site
							if( 0 != $domainConfig->getGroupID() ){ 
								$emailName	= $domainConfig->getEmailName();
								$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
							}
							*/

							$footerVars = array(
								'isAdvisor'	=> true,
								'group'		=> $iGroup,
								'siteName'	=> $siteName,
								'siteUrl'	=> $siteUrl
							);
							
							$vars = array_merge($footerVars, array(
								'recipient'	=> $iGroup->getName(),
								'travel'	=> $journal,
								'entries'	=> $newEntries,
								'owner'		=> $owner,
								'emailName'	=> $emailName,
								'notificationFooter' => NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
							));
							$administrator = $iGroup->getAdministrator();
							
							// check if recipient's notification preference is set to can receive notification
							$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($administrator->getTravelerID());
							if( FALSE == $canReceiveNotification ){
								continue;
							}
							// end check
							
							$notification = new Notification;
							$notification->addRecipient($administrator->getTravelerProfile()->getEmail());
							$notification->setMessage(NotificationComposer::compose($vars, 'tpl.TravelerAddEntryNotificationTemplate.php'));
							$notification->setSubject($owner->getUsername() . ' has added new journal ' . (count($newEntries) > 1 ? 'entries':'entry'));
							$notification->setSender($sender);
							$notification->setSenderName($emailName);
							$notification->addReplyTo($replyTo, $emailName);
							
							// proworld or kaya bcc russell and thea
							if( 245 == $iGroup->getGroupID() || 245 == $iGroup->getParentID() || 2961 == $iGroup->getGroupID() || 2961 == $iGroup->getParentID() ){
								$notification->addBccRecipient('thea.moraleta@goabroad.com');
							}
							
							// bcc rochelle for living routes notifications
							if( 1141 == $iGroup->getGroupID() || 1141 == $iGroup->getParentID() ){
								//$notification->addBccRecipient('rochelle@goabroad.com');
							}
							$this->mNotifications[] = $notification;
						}
						
					}
					catch(exception $e){}
				}
			}
		}
	}

?>