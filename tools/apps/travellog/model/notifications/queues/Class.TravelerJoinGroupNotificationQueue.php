<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	
	class TravelerJoinGroupNotificationQueue implements NotificationQueue, ClearableNotificationQueue{
		
		private $mReader 		= NULL;
		private $mNotifications = array();
		
		public function __construct(){
			$this->mReader = new Reader();
			//echo 'starting Traveler Join Group...<br />';
		}
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = 	"(logView._EXECUTION_DATE_ >= " . GaString::makeSqlSafe($startDate) . " AND " .
								 "logView._EXECUTION_DATE_ <= " . GaString::makeSqlSafe($endDate) . ") ";
			$sql = 	'SELECT * ' .
					'FROM ' .
						'`Travel_Logs`.`tblGrouptoTraveler` logView ' .
					'WHERE ' .
						'logView.`_EVENT_` = 1 AND ' . $rangeCriteria . ' ' .
						'AND EXISTS (<$ ' .
						'SELECT `groupID` FROM `Travel_Logs`.`tblGrouptoTraveler` ' .
						'WHERE ' .
							'logView.`_NEW_travelerID` = `travelerID` ' .
							'AND logView.`_NEW_groupID` = `groupID` ' .
							'AND logView.`_NEW_adate` = `adate` $>) ' .
					'ORDER BY logView._EXECUTION_DATE_ DESC ';
			$rs = $this->mReader->getLogsBySql($sql);
			$rs->dump();
			$this->createNotification($this->groupData($rs->retrieveData()));
			echo 'returning traveler joun group...<br />';
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
				
		private function groupData($resource){
			echo 'start of grouping data...<br />';
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				try{
					$arrGrp = GroupFactory::instance()->create(array($row['_NEW_groupID']));
					$group = $arrGrp[0];
					if($row['_NEW_travelerID'] != $group->getAdministratorID()){
						if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['time']))
						$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['time'] = $row['_EXECUTION_DATE_'];
					if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['travelers'][$row['_NEW_travelerID']]))
						$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['travelers'][$row['_NEW_travelerID']] =  $row['_NEW_travelerID'];	
					}
				}
				catch(exception $e){}
			}
			echo 'end of grouping data...<br />';
			return $items;
		}
		
		private function createNotification($groupedData){
			echo 'start of creating notifications...<br />';
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
				$emailName 		= $domainConfig->getEmailName();
				$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				$siteUrl 		= $domainConfig->getServerName();
				foreach($iGroupedData as $gID => $details){
					$time = date('F d, Y g:i:s A',strtotime($details['time']));
					$travelers = array();
					//try block for non existent groups
					try{
						$arrGrp = GroupFactory::instance()->create(array($gID));
						$group = $arrGrp[0];
						foreach($details['travelers'] as $travelerID){
							if($travelerID != $group->getAdministratorID()){
								try{
									$travelers[] = new Traveler($travelerID);	
								}
								catch(exception $e){
									throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR, $trace[0], '[CLASS]Traveler: ' . $e->getMessage());
								}
							}
						}

						/** 
						 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
						 *				cobrand site and recipient is admin of group, use cobrand
						 */
						// if group has a cobrand site use respective cobrand, else, use dotNet
						$parentID = 0 < $group->getParentID() ? $group->getParentID() : $group->getGroupID(); 
						$cb = ( $cb	= Config::findByGroupID($parentID)) ? $cb : null;
						echo "<br /> DUMP CB <br />";
						var_dump($cb);
						if ( !is_null($cb) ){
							$siteUrl	= $cb->getServerName();
							$sender 	= $cb->getAdminEmail();
							$emailName	= $cb->getEmailName();
							$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
						}
						else{
							$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
							$emailName 		= $domainConfig->getEmailName();
							$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
							$siteUrl 		= $domainConfig->getServerName();
						}

						$footerVars = array(
							'siteName'	=> $siteName,
							'siteUrl'	=> $siteUrl,
							'isAdvisor'	=> true,
							'group'		=> $group
						);
						
						$vars = array_merge($footerVars, array(	
							'recipient' => $group->getAdministrator()->getUserName(),
							'travelers' => $travelers,
							'time' 		=> $time,
							'emailName'	=> $emailName,
							'notificationFooter' => NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
						));
						$notification = new Notification();
						$notification->setSubject("New Member " . (count($travelers)>1 ? "Updates":"Update") . " for  " . (($group instanceof AdminGroup) ? "group ":"club ") . $group->getName());
						$notification->setSender($domainConfig->getGanetAdminEmail());
						$notification->setSenderName($emailName);
						$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
						$notification->setMessage(NotificationComposer::compose($vars,'tpl.TravelerJoinGroupTemplate.php'));
						$notification->addRecipient($group->getAdministrator()->getTravelerProfile()->getEmail());
						$this->mNotifications[] = $notification;
					}
					catch(exception $e){
						
					}
				}
			}
			echo 'end of creating notifications...<br />';			
		}
		
	}
?>
