<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/Class.NotificationSetting.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.SendableFactory.php');
	require_once('travellog/model/Class.MessageSource.php');
	require_once('travellog/model/Class.PersonalMessage.php');
	require_once('travellog/model/Class.MessageType.php');
	
	// TODO : MessageType 5 send to admin@goabroad.net
	class TravelerPersonalMessageNotificationQueue implements NotificationQueue, ClearableNotificationQueue{
		
		private $mReader 		= NULL;
		private $mNotifications = array();
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = "(logView.`_EXECUTION_DATE_` >= " . GaString::makeSqlSafe($startDate) . " AND " .
							  "logView.`_EXECUTION_DATE_` <= " . GaString::makeSqlSafe($endDate) . ")";
			$sql =  "SELECT logView.*, `tblNotificationSetting`.`_NEW_messageIncluded` " .
					"FROM " .
						"`Travel_Logs`.`tblMessageToAttribute` logView LEFT JOIN " .
						"`Travel_Logs`.`tblNotificationSetting` " .
						"ON " .
							"logView.`_NEW_messageID` = `tblNotificationSetting`.`_NEW_contextID` " . 
							"AND `tblNotificationSetting`.`_NEW_context` = " . GaString::makeSqlSafe(NotificationSetting::MESSAGE_CONTEXT) . ' ' .
					"WHERE " .
						"logView.`_EVENT_` = 1 " .
						"AND EXISTS ( <$ " .
						"SELECT `messageID` FROM `Travel_Logs`.`tblMessages` " .
						"WHERE " .
							"logView.`_NEW_messageID` =`messageID` " .
							"AND `discriminator` = 3 $> ) " .
						"AND $rangeCriteria " .
						"AND (`_NEW_messageType` = " . GaString::makeSqlSafe(MessageType::$INBOX) . " OR `_NEW_messageType` = " . GaString::makeSqlSafe(MessageType::$SPAMBOX) . ") " . // inbox or spambox
						'AND EXISTS (<$ SELECT `attributeID` FROM `Travel_Logs`.`tblMessageToAttribute` WHERE logView.`_NEW_attributeID` = `attributeID` $>) ' . 
					"ORDER BY logView.`_EXECUTION_DATE_` DESC";
			$rs = $this->mReader->getLogsBySql($sql);
			//$rs->dump(); //exit;
			$this->createNotification($this->groupData($rs->retrieveData()));
			return $this->mNotifications;	
		}
		
		public function clear(){
			NotificationSetting::deleteMessageNotificationSettings();
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['time']))
					$items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['time'] = $row['_EXECUTION_DATE_'];
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['recipients'][$row['_NEW_recipientID']][$row['_NEW_attributeID']]))
					$items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['recipients'][$row['_NEW_recipientID']][$row['_NEW_attributeID']] = array('attributeID' => $row['_NEW_attributeID'], 'messageIncluded' => $row['_NEW_messageIncluded']);
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();			
				
				foreach($iGroupedData as $doer => $details){
					$spamMessages = array(); // messages from user marked as spam
					$spamSource = null;
					foreach($details['recipients'] as $recipient => $attributes){
						// try block for non-existent group or traveler
						try{
							$obj = SendableFactory::Instance()->Create($recipient);

							// if message is addressed to a group, the recipient is the administrator
							if('Group' == get_parent_class($obj)){ 
								$obj = $obj->getAdministrator();

								/** 
								 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
								 *				cobrand site and recipient is admin of group, use cobrand
								 */
								// if group has a cobrand site use respective cobrand, else, use dotNet
								$cb = ( $cb	= Config::findByGroupID($obj->getGroupID())) ? $cb : Config::findByGroupID();
								if ( !is_null($cb) ){
									$siteUrl	= $cb->getServerName();
									$sender 	= $cb->getAdminEmail();
									$emailName	= $cb->getEmailName();
									$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
								}
							}

							// regular travelers
							if((($obj instanceof Traveler) OR 'Traveler' == get_parent_class($obj)) AND $obj->getTravelerID() != $doer){

								if(!$obj->isAutomaticallyAdded() && !$obj->isSuspended() && !$obj->isDeactivated()){
									$messages = array();
									$source = NULL;							

									foreach($attributes as $iAttributes){
										try{
											$temp = new PersonalMessage($iAttributes['attributeID']);	

										}
										catch(exception $e){
											throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR,$trace[0],'[CLASS]PersonalMessage: ' . $e->getMessage());
										}
										$source = new MessageSource($temp);
										if($temp->getMessageTypeID() != MessageType::$SPAMBOX)
											$messages[] = array('message' => $temp, 'isMessageIncluded' => ($iAttributes['messageIncluded'] == 1) ? TRUE:FALSE); 
										else{
											$spamMessages[] = array('message' => $temp, 'isMessageIncluded' => ($iAttributes['messageIncluded'] == 1) ? TRUE:FALSE, 'destination' => $obj); 
											$spamSource = $source;
										}
									}
									if(0 == count($messages)) continue ;
									//TODO : format time to necessary format
									$time = date('F d, Y g:i:s A',strtotime($details['time']));
									$vars = array(	'recipient' => $obj->getUsername(),
													'messages' => $messages,
													'source' => $source,
													'siteName' => $siteName,
													'siteUrl'	=> $siteUrl,
													'time' => $time);
									$notification = new Notification();
									$notification->setMessage(NotificationComposer::compose($vars,'tpl.TravelerPersonalMessageNotificationTemplate.php'));
									$notification->setSender($domainConfig->getAdminEmail());
									$notification->setSenderName($domainConfig->getEmailName());
									$notification->setSubject(ucfirst($source->getName()) . " sent you " . (count($messages) > 1 ? "messages": "a message") . " on " . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName));
									$notification->addRecipient($obj->getTravelerProfile()->getEmail());
									$this->mNotifications[] = $notification;
									//echo '<pre>'; var_dump($messages); echo '</pre><br /><br />';
								}	

							}
						}
						catch(exception $e){
							
						}
						
					}
					
					// add notification for spam messages
					if(0 < count($spamMessages)){
						$time = date('F d, Y g:i:s A',strtotime($details['time']));
						$vars = array(	'recipient' => 'GoAbroad Network Administrator',
										'spamMessages' => $spamMessages,
										'source' => $spamSource,
										'siteName' => $siteName,
										'siteUrl' => $siteUrl,
										'time' => $time);
						$notification = new Notification;
						//$notification->setMessage(NotificationComposer::compose($vars, 'tpl.TravelerPersonalMessageNotificationTemplate.php'));
						$notification->setMessage(NotificationComposer::compose($vars,'tpl.SpamMessagesNotificationTemplate.php'));
						$notification->setSender($domainConfig->getAdminEmail());
						$notification->setSenderName($domainConfig->getEmailName());
						$notification->setSubject('Messages marked as spam!');
						$notification->addRecipient('admin@goabroad.net');
						$this->mNotifications[] = $notification;
					}
				}			
			}
			
		}
	}
?>
