<?php
	class NotificationException extends Exception
	{		
		/*Composer exceptions*/
		const TEMPLATE_NOT_FOUND			= 101;
		const INVALID_DATE_RANGE			= 102;
		const INVALID_DATE					= 103;
		
		/*Queue Exceptions*/
		const OBJECT_INSTANTIATION_ERROR	= 201;
		
		/*BufferedNotification */
		const INVALID_BUFFERED_NOTIFICATION_ID = 301;
		
		private static $descriptions = array(
			self::TEMPLATE_NOT_FOUND			=> 'Unable to locate template in /travellog/views/notifications/',
			self::INVALID_DATE_RANGE			=> 'StartDate must be lesser than end date.',
			self::INVALID_DATE					=> 'Invalid date.',
			
			self::OBJECT_INSTANTIATION_ERROR	=> 'Failed to instantiate object.',
			self	
		);
		
		private $mErrCode = 0;
		public function __construct($code,$trace=null,$desc=null){
			$msg = (is_array($trace)
					? self::$descriptions[$code].' <<< Error occured in FILE: '.$trace['file'].' on LINE: '.$trace['line'].' >>> '
					: self::$descriptions[$code]);
			if(!is_null($desc)){
				$msg.= $desc;
			}
			parent::__construct($msg,$code);
			$this->mErrCode = $code;
		}
		
	 	public function __toString() {
        	return __CLASS__ . ' Error '.$this->mErrCode.': '.$this->getMessage();
    	}		
	}
?>