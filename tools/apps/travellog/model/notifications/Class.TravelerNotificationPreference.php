<?php

	require_once('Class.dbHandler.php');
	
	class TravelerNotificationPreference {
		
		private $db = null;
		private $travelerPreferences = array();
		
		private static $instance = null;
		
		private function __construct(){
			$this->db = new dbHandler;
			$this->travelerPreferences = array();
		}
		
		static public function getInstance(){
			if( is_null(self::$instance) ) self::$instance = new self;
			return self::$instance;
		}
		
		public function travelerCanReceiveNotification($travelerID=0){
			if( !array_key_exists($travelerID, $this->travelerPreferences) ){
				$sql = "SELECT tblTravelerNotificationPreferences.canReceiveNotification
						FROM tblTravelerNotificationPreferences
						WHERE 1
						AND tblTravelerNotificationPreferences.travelerID = $travelerID
						LIMIT 1";
				$rs = new iRecordset($this->db->execute($sql));
				if( $rs->retrieveRecordcount() ){
					$row = $rs->retrieveRow(0);
					$this->travelerPreferences[$travelerID] = $row['canReceiveNotification'] == 1;
				}
				else{
					$this->travelerPreferences[$travelerID] = true; // default
				}
			}
			return $this->travelerPreferences[$travelerID];
		}
		
	} // end class:: Remi, correct me if I'm wrong with my understanding in our current work flow... I work on a PP feature ENTIRELY BASED on how it works in AW, 
