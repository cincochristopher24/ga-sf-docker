<?php

	interface InstantNotification {
		
		public function createNotifications($opts = array());
		public function send();
		
	}

?>