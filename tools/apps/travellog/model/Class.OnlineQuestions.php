<?php
/**
 * Created on Mar 22, 2007
 * @author daf
 * 
 * 
 */
 	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
 	require_once('travellog/model/Class.OnlineFormOptions.php');
 	require_once('travellog/model/Class.OnlineSurveyAnswers.php');
 	
 	class OnlineQuestions {
 	
		/**
		 * Define variables for Class Member Attributes
		 */
		     
			 private $mQuestionID     	= 0;
			 private $mQuestion       	= '';
			 private $mQuestionType		= 0;
			 
			 private $mFormID			= 0;
			 private $mPosition			= 0;
			 	
			 private $mForm				= null;
			 	
			 private $mFormOptions   		= array();
		     private $mSurveyAnswers  		= array();
		     
		     
		     
		/**
		 * Constructor Function for this class
		 */
		           
		    function OnlineQuestions($questionID = 0){ 
		    	
		    	$this->mQuestionID = $questionID;
		    	
		    	
		    	try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mQuestionID){
                    
                    $sql = "SELECT * FROM tblOnlineQuestions WHERE onlineQuestionID = $this->mQuestionID ";
                    $this->mRs->Execute($sql);
                	
                    if ($this->mRs->Recordcount()){
                        
                        // Sets values to its class member attributes.
	                    $this->mQuestion    	= stripslashes($this->mRs->Result(0,"question"));       
	                    $this->mQuestionType 	= $this->mRs->Result(0,"type");           
	                        
                    }
                    
                    $sql = "SELECT onlineformID, position FROM tblOnlineFormToQuestions WHERE questionID = $this->mQuestionID ";
                    $this->mRs->Execute($sql);
                	
                    if ($this->mRs->Recordcount()){
                    	$this->mFormID    	= $this->mRs->Result(0,"onlineformID");
                        $this->mPosition    	= $this->mRs->Result(0,"position");
                    }
                }
		    	
		    }
		    
		/**
         * Setter Functions
         */    
            function setQuestion($_question){
                $this->mQuestion = $_question;
            }
            
            function setQuestionType($_type){
                $this->mQuestionType = $_type;
            }
            
		
		/**
         * Getter Functions
         */    
            function getQuestionID(){
                return $this->mQuestionID ;
            }
            
            function getQuestion(){
                return $this->mQuestion ;
            }
            
            function getQuestionType(){
                return $this->mQuestionType ;
            }
            
            
            function getForm(){
               
                if ($this->mForm == NULL){
		 			try {
		 				$this->mForm = new OnlineForm($this->mFormID) ;
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->mForm;
                
            }
            
            function getPosition(){
                return $this->mPosition ;
            }
          
    		
    		function Save(){
    			    			
    			$_question      = addslashes($this->mQuestion);
                
    			if (0 == $this->mQuestionID) {
    				
    				$sql = "INSERT into tblOnlineQuestions (question, type) " .
                        "VALUES ('$_question', $this->mQuestionType)";
	                $this->mRs->Execute($sql);       
	                $this->mQuestionID = $this->mRs->GetCurrentID();
    				
    			}
    			
    			else {
    				
    				$sql = "UPDATE tblOnlineQuestions SET " .
    						"question = '$_question', type = $this->mQuestionType " .
    						"WHERE onlineQuestionID = $this->mQuestionID" ;
	                $this->mRs->Execute($sql);       
    				
    			}
    		}
    		
    		
    		function Delete(){
               
                $sql = "DELETE FROM tblOnlineQuestions WHERE onlineQuestionID = $this->mQuestionID ";     
                $this->mRs->Execute($sql);
            } 
            
            
          /**
           * Purpose: Returns an array of options for this question.
           */
            function getFormOptions(){
                
                if (0 == count($this->mFormOptions)){
                    
                    $sql = "SELECT optionID from tblOnlineQuestionsToOptions WHERE questionID = $this->mQuestionID ORDER BY position ASC " ;
                    $this->mRs->Execute($sql);
                    
                   
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                        try {
                            $option = new OnlineFormOptions($recordset['optionID']);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                        $this->mFormOptions[] = $option;
                    }               
                }
                
                return $this->mFormOptions;
            }
    	
    	
            
    	 /**
           * Purpose: Returns an array of answers for this question.
           */
            function getSurveyAnswers(){
                
                
				 if (0 == count($this->mSurveyAnswers)){
				
					$sql = "SELECT onlinesurveyanswerID " .
							"FROM tblOnlineSurveyAnswers " .
							"WHERE questionID = $this->mQuestionID " ;
					$this->mRs->Execute($sql);
			 		
			 		while($recordset = mysql_fetch_array($this->mRs->Resultset())){
	                    try {
	 						$this->mSurveyAnswers[] = new OnlineSurveyAnswers($recordset["onlinesurveyanswerID"]); 									
	 					} 					
						catch (Exception $e) {
						   throw $e;					   
						}
	                }
	 				
				}
				 
 				return $this->mSurveyAnswers; 		
           }
                       
    }
?>
