<?php
/*
 * Created on Feb 14, 2007
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */


class UploadValidatorFactory{
	
	private 	$files 		= NULL;
	private  	$validator 	= array();
	private		$document_titles = array();
	
	function setFiles($files){		
		$this->files = $files;
	}
	
	function setDocumentTitles($titles=array()){
		$this->document_titles = $titles;
	}
	
	function getFiles(){		
		return $this->files;
	}
	
	function create($type){
		
		switch($type){
			case "image":
					include_once('Class.ImageValidator.php'); 
					$this->validator = new ImageValidator();
					$this->validator->setFiles($this->files);
					$this->validator->validate();
			break;
			
			case "PDF":
					include_once('Class.PdfValidator.php'); 
					$this->validator = new PdfValidator();
					$this->validator->setFiles($this->files);
					$this->validator->setDocumentTitles($this->document_titles);
					$this->validator->validate();
			break;
		}
		
		return $this->validator;
	}
	
	function getValidator(){
		return $this->validator;
	}
		
}

?>
