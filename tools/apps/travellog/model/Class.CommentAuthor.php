<?php
  /**
   * @(#) Class.CommentAuthor.php
   * 
   * Author class of the Comment.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 12 17, 08
   */
  
  class CommentAuthor {
  	/**
  	 * author type
  	 */
  	const ADMIN_GROUP = 1; // admin group account author
  	const TRAVELER    = 2; // traveler account
  	const NON_GANET   = 3; // non-GANET author
  	
  	/**
  	 * attributes
  	 */
  	private $mAuthorObject = null;
  	/**
  	 * Properties of the author;
  	 *   1. authorID
  	 *   2. name
  	 *   3. email 
  	 */
  	private $mAuthorProp   = array();
  	private $mAuthorType   = null;
  	
	private $mAuthorAsTraveler = null;
	private $mAuthorID = 0;

  	/**
  	 * constructor
  	 * 
  	 * @param array|integer $param For array its must be an associative array (keys must be authorID, email and name). For integer it is the ID of the author. 
  	 */
  	public function __construct($param){
  	  if (is_numeric($param)) {
  	  	$param = array("authorID"=>$param, "email"=>"", "name"=>"");
  	  }
  	  
  	  if (0 >= $param['authorID']) { // for non-GANET author
  	  	$this->mAuthorType = CommentAuthor::NON_GANET;
  	  }
  	  else {
  	  	$gID = 0;
  	  	$this->mAuthorID = $param['authorID'];
  	  	if (0 < $gID) { // for AdminGroup account
  	  		$this->mAuthorObject = new AdminGroup($gID);
  	  		$this->mAuthorType = CommentAuthor::ADMIN_GROUP;
  	  		$param['name'] = $this->mAuthorObject->getName();
			$this->mAuthorAsTraveler = $this->mAuthorObject->getAdministrator();
  	  	}
  	  	else { // for traveler account
  	  	  try {
			  	  $traveler	=	new Traveler($param['authorID']);
  	  	  		  $this->mAuthorObject = $traveler->getTravelerProfile();
			  	  $this->mAuthorType = CommentAuthor::TRAVELER;
			  	  $param['name'] = $this->mAuthorObject->getUserName();
				  $this->mAuthorAsTraveler = $traveler;
			  	}	
			  	catch (exception $ex) { // if it is not an admin group and not a traveler we default to non-GANET author  
			  		$this->mAuthorType = CommentAuthor::NON_GANET;
			  	}
  	  	}
  	  }
  	  
  	  $this->mAuthorProp = $param;
  	}
  	
  	/**
  	 * Gets the source of the author's photo.
  	 * 
  	 * @return string
  	 */
  	function getPhotoSource(){
  	  $default_source = "default_images/user_thumb.gif";
  	  
			switch($_SERVER['SERVER_NAME']){
			  case "dev.goabroad.net":
				  $default_source = "http://dev.goabroad.net/images/".$default_source;
					break;
				case "www.goabroad.net":
				  $default_source = "http://images.goabroad.net/images/".$default_source;
					break;
				case "goabroad.net.local":
					$default_source = "http://goabroad.net.local/images/".$default_source;
					break;
				default:
					$default_source = "http://".$_SERVER['SERVER_NAME']."/images/".$default_source;
			}
  	  
  	  switch($this->mAuthorType){
  	    case CommentAuthor::NON_GANET:
  	      return $default_source;
  	      break;
  	    case CommentAuthor::TRAVELER:
  	      return $this->mAuthorObject->getPrimaryPhoto()->getPhotoLink('thumbnail');
  	      break;
  	    case CommentAuthor::ADMIN_GROUP:
  	      //return $this->mAuthorObject->getGroupPhoto()->getPhotoLink('thumbnail');
		  return $this->mAuthorAsTraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');
  	      break;
  	    default:
  	      return $default_source;  	    
  	  }
  	}
  	
  	/**
  	 * Gets the name of the author. For traveler the name is its username, for admin group
  	 * its the name of the admin group and for the non-GANET user its the name passed in this
  	 * object.
  	 * 
  	 * @return string
  	 */
  	function getName(){
  		//return $this->mAuthorProp['name'];
		if( $this->mAuthorType == CommentAuthor::ADMIN_GROUP ){
			if( is_null($this->mAuthorAsTraveler) ){
				$this->mAuthorAsTraveler = new Traveler($this->mAuthorID);
			}
			return $this->mAuthorAsTraveler->getTravelerProfile()->getUserName();
		}
		return $this->mAuthorProp['name'];
  	}
  	
  	/**
  	 * Gets the email of the author. For traveler and group the email is empty string while
  	 * for the non-GANET user its the email passed in this object.
  	 * 
  	 * @return string
  	 */
  	function getEmail(){
  		return $this->mAuthorProp['email'];
  	}
  	
  	/**
  	 * Gets the type of this author.
  	 * 
  	 * @return integer Returns one of the following values (CommentAuthor::NON_GANET, CommentAuthor::TRAVELER or CommentAuthor::ADMIN_GROUP)
  	 */
  	function getAuthorType(){
  		return $this->mAuthorType;
  	}
  	
  	/**
  	 * Gets the ID of the author.
  	 * 
  	 * @return integer
  	 */
  	function getAuthorID(){
  		return $this->mAuthorProp['authorID'];
  	}
  	
  	/**
  	 * Gets the author object (this is also the context object of the comment). 
  	 * 
  	 * @throws exception
  	 * @return TravelerProfile|AdminGroup
  	 */
  	function getAuthorObject(){
  		if (is_null($this->mAuthorObject)) {
  			throw new exception("This function is not applicable for non-GANET author.");
  		}
  		
  		return $this->mAuthorObject;
  	}

	function getAuthorAsTraveler(){
		if( is_null($this->mAuthorAsTraveler) ){
			$this->mAuthorAsTraveler = new Traveler($this->mAuthorID);
		}
		return $this->mAuthorAsTraveler;
	}
  }
