<?php
	
	/*
	 * Class.Config.php
	 * Created on Jun 14, 2007
	 * created by marc
	 */

	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once('travellog/model/Class.DomainResolver.php');
	 
	 class Config{
	 	
	 	private $mConfigID		= 	0;
	 	private $mServerName	=	'';
	 	private $mGroupID		=	0;
	 	private $mSiteName		= 	'';
	 	private $mSiteDescr		=	'';
	 	private $mFolderName	=	'';
	 	private $mCopyrightYear	=	'';
	 	private $mAdminEmail	=	'';
	 	private $mGanetAdminEmail =	'';
	 	private $mEmailName	    =   '';
	 	private $mUrl			= 	'';
	 	private $mAdminGroup    = null;	
	    private $mAllowCustomPageHeader    = false;	
	 	private $mConn;
	 	private $mRs;
	 	
	 	private $serverNameList = array();
	 		 		 	
	 	function __construct($domainName=null){
	 		//$this->mServerName = $_SERVER['HTTP_HOST'];
			//$this->mServerName = (!is_null($domainName)?$domainName:$_SERVER['HTTP_HOST']);
			$this->mServerName = DomainResolver::getInstance()->resolve((!is_null($domainName)?$domainName:$_SERVER['HTTP_HOST']));
	 		$this->mConn = new Connection;
            $this->mRs   = new Recordset($this->mConn);
	 		
	 		if ( strlen(trim($this->mServerName)) ){
                    
                $sql = "SELECT * FROM tblConfig WHERE serverName = '$this->mServerName' ";

                $this->mRs->Execute($sql);
      		
                if (0 == $this->mRs->Recordcount()){
                    throw new Exception("Invalid server name.");        // ID not valid so throw exception
                }

                // Sets values to its class member attributes.
                $this->mConfigID		= $this->mRs->Result(0,"configID");   
                $this->mServerName      = stripslashes($this->mRs->Result(0,"serverName"));           
                $this->mGroupID  		= $this->mRs->Result(0,"groupID");       
                $this->mSiteName        = stripslashes($this->mRs->Result(0,"siteName"));         
                $this->mSiteDescr		= stripslashes($this->mRs->Result(0,"siteDescr"));
                $this->mFolderName  	= stripslashes($this->mRs->Result(0,"folderName"));
                $this->mCopyrightYear 	= $this->mRs->Result(0,"copyrightYear");
                $this->mAdminEmail		= $this->mRs->Result(0,"adminEmail");
                $this->mGanetAdminEmail = $this->mRs->Result(0,"ganetAdminEmail");
                $this->mEmailName		= $this->mRs->Result(0,"emailName");
                $this->mUrl				= $this->mRs->Result(0,"url");			
				$this->mAllowCustomPageHeader	= $this->mRs->Result(0,"allowCustomPageHeader");			
            }
	 		
	 	}
	 	
	 	/**
         * Getter Functions
         */       
         
        function getConfigID(){
          return $this->mConfigID;
        }
        
        function getGroupID(){
          return $this->mGroupID;	
        }
        
        function getServerName(){
          return $this->mServerName;
        }
        
        function getSiteName(){
          return $this->mSiteName;
        }
        
        function getSiteDescription(){
          return $this->mSiteDescr;
        }
 		
 		function getFolderName(){
          return $this->mFolderName;
        }
        
        function getCopyrightYear(){
          return $this->mCopyrightYear;
        }
 
 		function getConnection(){
	 		return $this->mConn;
	 	}
	 	
	 	function getResultset(){
	 		return $this->mRs;
	 	}
	 	
	 	function getAdminEmail(){
	 		return $this->mAdminEmail;
	 	} 
	 	
	 	function getGanetAdminEmail(){
	 		return $this->mGanetAdminEmail;
	 	}
	 
	 	function getEmailName(){
	 		return $this->mEmailName;
	 	}
	 	
	 	function getUrl(){
	 		return $this->mUrl;
	 	}
	
		function getAllowCustomPageHeader(){
	 		return $this->mAllowCustomPageHeader;
	 	}
	 	
	 	function getServerNameList(){
	 		
	 		$sql = "SELECT serverName FROM tblConfig ";
            $this->mRs->Execute($sql);
            
            while ($recordset = mysql_fetch_array($this->mRs->ResultSet())){
            	$serverNameList[] = $recordset['serverName'];
            }
	 		
	 		return $serverNameList;	 			
	 	}
	 	
	 	function getOnlineAdvising() {
	 		return 1;
	 	}
	 	
	 	function getAdminGroup() {
	 		if (0 < $this->mGroupID){
				require_once("travellog/model/Class.AdminGroup.php");
				$this->mAdminGroup  	= new AdminGroup($this->mGroupID);	                          
			}
			return $this->mAdminGroup;	
	 	}
	 	
	 	function getClientID(){
	 		
	 		$sql = "SELECT clientID FROM tblGrouptoAdvisor WHERE groupID =  " . $this->mGroupID;
	 		$this->mRs->Execute($sql);
	 		
	 		if ($this->mRs->Recordcount())
	 			return $this->mRs->Result(0, "clientID");
	 		else
	 			return 0;	
	 	}
	 	
		public static function findByGroupID($groupID = 0){
			if( 0 == $groupID )
				return null;
				
			require_once('Class.dbHandler.php');
			$db = new dbHandler;
			$rs = new iRecordset($db->execute(
				'SELECT serverName FROM `tblConfig` WHERE `groupID` = ' . GaString::makeSqlSafe($groupID) . ' LIMIT 0,1'
			));
			return $rs->retrieveRecordcount() ? new Config($rs->getserverName(0)) : null;
		}
	
		static public function findByServerName($serverName=null){
			try {
				return new Config($serverName);
			}
			catch (exception $ex) {
				return null;
			}
			/*
			require_once('Class.dbHandler.php');
			$db = new dbHandler;
			$rs = new iRecordset($db->execute(
				'SELECT serverName '.
				'FROM `tblConfig` '.
				'WHERE `serverName` = '.$db->makeSqlSafeString($serverName).' '. 
				'LIMIT 0,1'
			));
			
			return $rs->retrieveRecordcount() ? new Config($rs->getServerName(0)) : null;
			*/
		}
		
		public function isAdministrator($travelerID){
			return $this->getAdminGroup()->getAdministratorID() == $travelerID;
		}
	 }
?>