<?php
	class GaFormException extends Exception
	{
		const API_KEY_READ_ONLY 		= 101;
		const API_SECRET_REQUIRED		= 102;
		const API_CLIENT_NAME_REQUIRED 	= 103;
		const UNKNOWN_API_CLIENT		= 104;
		const UNKNOWN_SERVICE_METHOD	= 105;	
		const UNKNOWN_GAFORM_ID			= 106;
		
		const UNKNOWN_ATTRIBUTE_NAME	= 201;
		const INVALID_ACTION_URL		= 202;
		const UNKNOWN_THEME				= 203;
		const INVALID_FORM_DATA_FORMAT	= 204;
		const NO_FORM_PERMISSION		= 205;
		const INSUFFICIENT_DATA			= 206;
		const INVALID_FIELD_VALUE_TYPE	= 207;
		
		static private $descriptions = array(
			self::API_KEY_READ_ONLY			=> 'The Api Key is readonly.',
			self::API_SECRET_REQUIRED		=> 'The Api Secret is required.',
			self::API_CLIENT_NAME_REQUIRED	=> 'The Api Client Name is required.',
			self::UNKNOWN_API_CLIENT		=> 'The API key and API secret does not belong to any GaForm client.',
			self::UNKNOWN_SERVICE_METHOD	=> 'Unknown GaFormServiceProvider Method.',
			self::UNKNOWN_GAFORM_ID			=> 'Unknown GaFormID or the Client is not the owner of the form.',
			
			self::UNKNOWN_ATTRIBUTE_NAME	=> 'Unknown attribute name.',
			self::INVALID_ACTION_URL		=> 'The ActionUrl is required.',
			self::UNKNOWN_THEME				=> 'A valid gaForm theme is required.',
			self::INVALID_FORM_DATA_FORMAT	=> 'The Form Data format is invalid.',
			self::NO_FORM_PERMISSION		=> 'You are trying to save a form that does not belong to you.',
			self::INSUFFICIENT_DATA			=> 'The data supplied is insufficient to complete the requested operation.',
			self::INVALID_FIELD_VALUE_TYPE	=> 'The field type is invalid.'
		);
		
		public function __construct($code,$desc=null){
			$msg = self::$descriptions[$code];
			if(!is_null($desc)){ $msg.= ' '.$desc; }
			parent::__construct($msg,$code);
		}
	}
?>