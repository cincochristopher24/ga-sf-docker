<?php
	require_once('Class.DataModel.php');
	require_once('travellog/model/gaForms/Class.GaFormException.php');
	require_once('travellog/model/gaForms/formEditor/Class.GaForm.php');
	class GaFormClient extends DataModel
	{
		private function __construct($keyID=null,$data=null){
			parent::initialize('GaForms','tblGaFormClient');
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid GaFormClientID: '.$keyID);
			}
			if(is_array($data)){
				$this->setFields($data);
			}
		}
		
		public function createNewForm(){
			$gaForm = new GaForm();
			$gaForm->setGaFormClientID($this->getGaFormClientID());
			return $gaForm;
		}
		
		public function save(){
			if(!strlen($this->getApiSecret())){
				throw new GaFormException(GaFormException::API_SECRET_REQUIRED);
			}
			if(!strlen($this->getClientName())){
				throw new GaFormException(GaFormException::API_CLIENT_NAME_REQUIRED);
			}
			if(!$this->getGaFormClientID()){
				parent::executeMethod('setApiKey',array(str_replace('.','',uniqid(rand(), true))));
			}
			parent::save();
		}
		
		public function setApiKey(){
			throw new GaFormException(GaFormException::API_KEY_READ_ONLY);
		}		
		
		public function getGaForm($formID=0){
			$gaForm = GaForm::getGaFormByFormIDAndGaFormClientID($formID,$this->getGaFormClientID());
			if(!is_null($gaForm)){
				return $gaForm;
			}
			throw new GaFormException(GaFormException::UNKNOWN_GAFORM_ID);
		}
		
		public function getGaForms($formIDs=array()){
			return GaForm::getMultipleGaFormsByFormIDsAndGaFormClientID($formIDs,$this->getGaFormClientID());
		}
		
		public function getAllGaForms(){
			return GaForm::getMultipleGaFormsByGaFormClientID($this->getGaFormClientID());
		}
		
		static public function createNewGaFormClient(){
			return new GaFormClient();
		}
		
		static public function getGaFormClientApiKeyAndApiSecret($apiKey,$apiSecret){
			$db = new dbHandler('db=GaForms');
			$sql = 	'SELECT * FROM tblGaFormClient '.
					'WHERE apiKey = '.$db->makeSqlSafeString($apiKey).' '.
					'AND apiSecret = '.$db->makeSqlSafeString($apiSecret);
			$rs = new iRecordset($db->execute($sql));
			if($rs->retrieveRecordCount()){
				return new GaFormClient(null,$rs->retrieveRow(0));
			}
			return null;
		}
		
		static public function getClientByName($name=null){
			$db = new dbHandler('db=GaForms');
			$sql = 	'SELECT * FROM tblGaFormClient '.
					'WHERE name = '.$db->makeSqlSafeString($name);
			$rs = new iRecordset($db->execute($sql));
			if($rs->retrieveRecordCount()){
				return new GaFormClient(null,$rs->retrieveRow(0));
			}
			return null;
		}
	}
?>