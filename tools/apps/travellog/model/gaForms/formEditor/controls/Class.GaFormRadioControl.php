<?php
	require_once('Class.GaFormControl.php');
	class GaFormRadioControl extends GaFormControl
	{				
		public function getDefAttributes(){
			return array('isChecked' => 0,'text' => '');
		}
		public function getControlType(){
			return GaFormControl::RADIO;
		}
	}
?>