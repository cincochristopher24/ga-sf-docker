<?php
	require_once('Class.GaFormControl.php');
	class GaFormComboBoxControl extends GaFormControl
	{				
		public function getDefAttributes(){
			return array('size' => 0,'selectedIndex' => 0,'options' => array());
		}
		public function getControlType(){
			return GaFormControl::COMBOBOX;
		}
	}
?>