<?php
	require_once('Class.GaFormControl.php');
	class GaFormTextBoxControl extends GaFormControl
	{				
		public function getDefAttributes(){
			return array('size' => 0,'isMultiline' => 0,'value'	=> '');
		}
		public function getControlType(){
			return GaFormControl::TEXTBOX;
		}
	}
?>