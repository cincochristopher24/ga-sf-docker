<?php
	require_once('Class.GaFormControl.php');
	class GaFormSectionBreakControl extends GaFormControl
	{				
		public function getDefAttributes(){
			return array();
		}
		public function getControlType(){
			return GaFormControl::SECTION_BREAK;
		}
	}
?>