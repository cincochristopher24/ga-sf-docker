<?php
	require_once('Class.DataModel.php');
	require_once('travellog/model/gaForms/formEditor/Class.GaFormField.php');
	
	class GaFormFieldValue extends DataModel
	{
		private $mDataValues = null; 
		public function __construct($keyID=null,$data=null){
			parent::initialize('GaForms', 'tblGaFormFieldValue');
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid GaFormFieldValueID');
			}
			if(is_array($data)){
				$this->setFields($data);
			}
			$this->initDataValues();
		}
		
		public function delete(){
			//delete the data
			$gaFormValueID = $this->mDb->makeSqlSafeString((is_null($this->getGaFormFieldValueID()) ? 0 : $this->getGaFormFieldValueID()));
			$this->mDb->execute('DELETE FROM GaForms.tblGaFormFieldValueData WHERE gaFormFieldValueID = '.$gaFormValueID);
			parent::delete();
		}
		
		private function initDataValues(){
			if(!is_null($this->getGaFormFieldValueID())){
				$gaFormField = GaFormField::getByID($this->getGaFormFieldID());
				$sql = 'SELECT * FROM GaForms.tblGaFormFieldValueData WHERE gaFormFieldValueID = '.$this->mDb->makeSqlSafeString($this->getGaFormFieldValueID());
				$rs = new iRecordset($this->mDb->execute($sql));				
				if($gaFormField->getValueType() == 'array')	$this->mDataValues = $rs->retrieveColumn('value');
				else{
					if($rs->retrieveRecordCount()){
						$this->mDataValues = $rs->getValue(0);
					}
				}
			}
		}
		
		public function save($fieldValue){
			parent::save();
			//insert the date values
			$values = (!is_array($fieldValue) ? array($fieldValue) : $fieldValue);
			$sql = 'INSERT INTO GaForms.tblGaFormFieldValueData (`gaFormFieldValueID`,`value`) VALUES ';
			$sqlAr = array();
			$gaFormFieldValueID = $this->mDb->makeSqlSafeString($this->getGaFormFieldValueID());
			foreach($values as $iValue){
				$sanitizedVal = $this->mDb->makeSqlSafeString($iValue);
				$sqlAr[] = "($gaFormFieldValueID, $sanitizedVal)";
			}
			if(count($sqlAr)) $this->mDb->execute($sql.implode(',',$sqlAr));
		}
		
		public function getSerialiazableFieldValueData(){
			return $this->mDataValues;
		}
		
		static public function getFieldValuesByFormValueID($formValueID){
			$db = new dbHandler('db=GaForms');
			$rs = new iRecordset($db->execute('SELECT * FROM tblGaFormFieldValue WHERE gaFormValueID = '.$db->makeSqlSafeString($formValueID)));
			$ar = array();
			foreach($rs as $row){
				$ar[$row['gaFormFieldValueID']] = new self(null,$row);
			}
			return $ar;
		}
		
		static public function getFieldValuesByGaFormFieldID($fieldID=0){
			$db = new dbHandler('db=GaForms');
			$rs = new iRecordset($db->execute('SELECT * FROM tblGaFormFieldValue WHERE gaFormFieldID = '.$db->makeSqlSafeString($fieldID)));
			$ar = array();
			foreach($rs as $row){
				$ar[$row['gaFormFieldValueID']] = new self(null,$row);
			}
			return $ar;
		}
	}
?>