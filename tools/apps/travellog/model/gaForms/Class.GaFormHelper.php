<?php
	class GaFormHelper
	{
		const TEXTBOX 		= 0;
		const CHECKBOX 		= 1;
		const RADIO			= 2;
		const COMBOBOX		= 3;
		const SECTION_BREAK	= 4;
		
		static private $instance = null;
		static public function create(){
			if(is_null(self::$instance)) self::$instance = new self;
			return self::$instance;
		}
		
		static private $fieldClassNames = array(
			'4'	=> 'sectionBreak'
		);
		
		private $mFormPrefix = '';
		public function renderControl($controlData=array(),$options=array()){
			$this->mFormPrefix = (array_key_exists('formPrefix',$options) && is_string($options['formPrefix']) ? $options['formPrefix'] : $this->mFormPrefix);
			$controlName = $this->mFormPrefix.$controlData['gaFormFieldID'];
			if($controlData['controlType'] == self::TEXTBOX){
				$className = (($controlData['size']==0)?'small_text':(($controlData['size']==1)?'medium_text':'large_text'));
				$value = (array_key_exists('value',$options) ? $options['value'] : $controlData['value']);
				if($controlData['isMultiline']) return "<textarea name='$controlName' class='$className'>$value</textarea>";
				else "<input name='$controlName' type='text' class='$className' value='$value' />";
			}
			elseif($controlData['controlType'] == self::CHECKBOX || $controlData['fieldType'] == self::RADIO){
				$controlName = $controlName.'[]';
				$className = '';
				$isChecked = (array_key_exists('value',$options) && is_array($options['value']) && in_array($controlData['text'],$options['value']) ? true : $controlData['isChecked']);
				$inputType = $controlData['controlType'] == self::CHECKBOX ? 'checkbox' : 'radio';
				return "<input type='$inputType' name='$controlName' ".( $isChecked ? 'checked' : '')." value='{$controlData['text']}' />".$controlData['text'];
			}
			elseif($controlData['controlType'] == self::COMBOBOX){
				$className = (($data['size']==0)?'small_select':(($data['size']==1)?'medium_select':'large_select'));
				$tag = "<select name='$controlName' class='$className'>";
				foreach($controlData['options'] as $idx => $iOpt){
					$isSelected = (array_key_exists('value',$options) && $options['value'] == $iOpt ? true : $controlData['selectedIndex'] == $idx);
					$tag .= "<option value='{$iOpt}' ".($isSelected ? 'selected=\'true\'' : '')." >$iOpt</option>";
				}
				$tag .= '</select>';
				return $tag;
			}
		}
		
		public function getFieldName($fieldData){
			return 'gffield_'.$fieldData['gaFormFieldID'];
		}
		
		public function getFieldLabelClassName($field){
			return (array_key_exists($field['fieldType'], self::$fieldClassNames) ? self::$fieldClassNames[$field['fieldType']] : '');
		}
		
		public function sanitizeFormFields($formValues){
			if(strlen($this->mFormPrefix)){
				$values = array();
				foreach($formValues as $key => $val){
					$values[substr($key,count($this->mFormPrefix)-1,strlen($key))] = $val;
				}
				return $values;
			}
			return $formValues;
		}
	}
?>