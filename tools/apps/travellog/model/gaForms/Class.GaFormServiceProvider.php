<?php
	require_once('travellog/model/gaForms/Class.GaFormException.php');
	require_once('travellog/model/gaForms/Class.GaFormClient.php');
	//require_once('travellog/model/gaForms/Class.GaFormTheme.php');
	require_once('travellog/model/gaForms/Class.GaFormValue.php');
	require_once('Class.dbHandler.php');
	require_once('Class.Template.php');
	require_once('JSON.php');
	
	
	class GaFormServiceProvider
	{
		static public function create($apiKey,$apiClient){
			$gaFormClient = GaFormClient::getGaFormClientApiKeyAndApiSecret($apiKey,$apiClient);
			if(is_null($gaFormClient)){
				throw new GaFormException(GaFormException::UNKNOWN_API_CLIENT);
			}
			return new GaFormServiceProvider($gaFormClient);
		}
		private $mPublishedMethods = array(
			'SAVEFORM' 				=> 	'saveForm',
			'CREATENEWFORM'			=>	'createNewForm',
			'GETFORM'				=>	'getForms',
			'GETFORMDATA'			=> 	'getFormData',
			'DELETEFORMS'			=>	'deleteForms',
			'RENDERFORM'			=>	'renderForm',
			'EDITFORM'				=> 	'editForm',
			'GETDUPLICATEFORMDATA' 	=> 	'getDuplicateFormData',
			'SAVEFORMVALUE'			=> 	'saveFormValue',
			'GETFORMVALUE'			=> 	'getFormValue',
			'GETMULTIPLEFORMVALUES'	=> 	'getMultipleFormValues',
			'GETSTATISTICS'			=> 	'getStatistics'
		);
		private $mGaFormClient 	= null;
		private $mDb 			= null;
		private $mJson			= null;
		
		private function __construct($gaFormClient){
			$this->mGaFormClient = $gaFormClient;
			$this->mDb = new dbHandler();
			$this->mJson = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
		}
		
		private function createNewForm($clientVars=array()){
			$gaForm = $this->mGaFormClient->createNewForm();
			return $gaForm->getJsonSerializableData();
		}
		
		private function getFormData($clientVars=array()){
			if(is_array($clientVars['formID'])){
				$gaForms = $this->mGaFormClient->getGaForms($clientVars['formID']);
				$formData = array();
				foreach($gaForms as $iGaForm){
					$formData[$iGaForm->getGaFormID()] = $iGaForm->getJsonSerializableData();
				}
				return $formData;
			}
			else{
				$gaForm = $this->mGaFormClient->getGaForm($clientVars['formID']);
				if(!is_null($gaForm)){
					return $gaForm->getJsonSerializableData();
				}
			}
			throw new GaFormException(GaFormException::NO_FORM_PERMISSION);
		}
		
		private function saveForm($clientVars=array()){
			//if the gaFormID is not 0, check if the client is the real owner of the gaForm being modified.
			//var_dump($clientVars['formData']);
			$formData = $this->mJson->decode($clientVars['formData']);
			if($formData){
				$gaFormID = (array_key_exists('gaFormID',$formData) && is_numeric($formData['gaFormID']) ? $formData['gaFormID'] : null);
				if(!is_null($gaFormID)){
					if(0!=$gaFormID){
						$gaForm = $this->mGaFormClient->getGaForm($gaFormID);
						if(is_null($gaForm)) throw new GaFormException(GaFormException::NO_FORM_PERMISSION);
					}
					else $gaForm = $this->mGaFormClient->createNewForm();
					$gaForm->saveFormData($formData);
					//return the id of the form
					return $gaForm->getGaFormID();
				}
			}
			throw new GaFormException(GaFormException::INVALID_FORM_DATA_FORMAT);
		}
		
		private function deleteForms($clientVars=array()){
			$gaForms = $this->mGaFormClient->getGaForms($clientVars['formIDs']);
			foreach($gaForms as $iGaForm){
				$iGaForm->delete();
			}
		}
		
		private function getDuplicateFormData($clientVars=array()){
			$gaForm = $this->mGaFormClient->getGaForm($clientVars['formID']);
			return $gaForm->getJsonSerializableDuplicateData();
		}
		
		private function getFormValue($clientVars=array()){
			//check out the 
			$gaFormValue = GaFormValue::getByID($clientVars['formValueID']);
			if(!is_null($gaFormValue) && $gaFormValue->getGaForm()->getGaFormClientID() == $this->mGaFormClient->getGaFormClientID()){
				return $gaFormValue->getSerializableFormValueData();
			}
			throw new GaFormException(GaFormException::NO_FORM_DATA_PERMISSION);
		}
		
		private function getMultipleFormValues($clientVars=array()){
			//check each form value if it belongs to a form that belongs to the current client
			$gaFormValues = GaFormValue::getInstancesByIDs($clientVars['formValueIDs']);
			$gaForms = $this->mGaFormClient->getAllGaForms();
			$data = array();
			foreach($gaFormValues as $iGaFormValue){
				if(array_key_exists($iGaFormValue->getGaFormID(),$gaForms)){
					$data[$iGaFormValue->getGaFormValueID()] = $iGaFormValue->getSerializableFormValueData();
				}
			}
			return $data;
		}
		
		private function saveFormValue($clientVars){			
			//check if the gaFormID was passed in
			if(!array_key_exists('gaFormID',$clientVars)) throw new GaFormException(GaFormException::INSUFFICIENT_DATA,'GaFormID is required!');
			//check if the gaFormValueID was passed in
			if(!array_key_exists('gaFormValueID',$clientVars)) throw new GaFormException(GaFormException::INSUFFICIENT_DATA,'GaFormValueID is required. If this is a new form value, please enter a null value.');
			
			//check if the gaFormID belongs to the client.
			$gaForm = $this->mGaFormClient->getGaForm($clientVars['gaFormID']);
			if(is_null($gaForm)) throw new GaFormException(GaFormException::NO_FORM_PERMISSION);
			
			//if the gaformValueID is not null, check if it belongs to the gaForm
			if(!is_null($clientVars['gaFormValueID'])){
				$gaFormValue = $gaForm->getFormValue($clientVars['gaFormValueID']);
				if(is_null($gaFormValue)) throw new GaFormException(GaFormException::INVALID_GAFORM_VALUE_ID);
			}
			else{
				$gaFormValue = $gaForm->createNewFormValue();
			}
			
			//get the fields of this form
			$errorFields = array();
			$validFields = array();
			$gaFormFields = $gaForm->getFormFields();
			
			foreach($gaFormFields as $fieldID => $iField){
				if(array_key_exists($fieldID, $clientVars)){
					if($gaFormFields[$fieldID]->getValueType() == gettype($clientVars[$fieldID])){
						$isEmpty = ($iField->getValueType() == 'array' ? 0==count($clientVars[$fieldID]) : 0==strlen($clientVars[$fieldID]));
						if($iField->isRequired() && $isEmpty) $errorFields[$fieldID] = 'This field is required.';
						else $validFields[$fieldID] = $clientVars[$fieldID];
					}
					else{
						throw new GaFormException(GaFormException::INVALID_FIELD_VALUE_TYPE,'Expected a '.$gaFormFields[$fieldID]->getValueType().' value for field '.$fieldID.' but a value of type '.gettype($val).' was given.');
					}
				}
				else{
					if($iField->isRequired()) $errorFields[$fieldID] = 'This field is required.';
				}
			}
			
			if(!count($errorFields)){
				if(array_key_exists('validateOnly',$clientVars) && true === $clientVars['validateOnly']){
					return 0;
				}
				else{
					$gaFormValue->save();
					$gaFormValue->clearFieldValues();
					foreach($validFields as $fieldID => $val){
						$gaFormValue->saveFieldValue($fieldID,$val);
					}
					return $gaFormValue->getGaFormValueID();
				}
			}
			else{
				return $errorFields;
			}
		}
		
		private function getStatistics($clientVars=array()){
			$gaForm = $gaForm = $this->mGaFormClient->getGaForm($clientVars['formID']);
			return $gaForm->getStatistics();
		}
		
		public function execute($method,$clientVars){
			$uCaseMethod = strtoupper($method);
			if(array_key_exists($uCaseMethod,$this->mPublishedMethods)){
				$actualMethod = $this->mPublishedMethods[$uCaseMethod];
				return $this->$actualMethod($clientVars);
			}
			throw new GaFormException(GaFormException::UNKNOWN_SERVICE_METHOD,':'.$method);
		}
	}
?>