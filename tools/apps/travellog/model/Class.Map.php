<?php
	//require_once('Class.Ini_Config.php');
	class Map{		
		public static $mapCtr = 0;
		private $mapFrameRegistry = array();
		public function includeJs(){			
			/*
			<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RR_mv8qxSfI61UPf1AUwo3FOOuLUhRKkqFi4o2p6bfPfZfqmGtkz3Jlaw" type="text/javascript"></script>--><!-- ip ni aldwin -->						
			<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAxssX44ErD-uqyd-GW74yDBS_IWFy692u9pZ7fVeFegkeYwDfHRQ0DmyB5o0hSF4zV4IdIwGBmzCXEw"	type="text/javascript"></script>--><!-- goabroad.net.local -->						
			<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RT2yXp_ZAY8_ufC3CFXhHIE1NvwkxQK172wzBL98irx9meqK9uiOlMTOw" type="text/javascript"></script>--><!--localHost-->
			<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RSxAnUob8CCeJxJdtYJBdK6zFkHgxQ5xa2hmJL9BHBK116ssslR4r05yA" type="text/javascript"></script>--><!-- dev.goabroad.net -->
			<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RQwbP-JBhpjGqlbK3A4xF5aLrtY6RSWOoaOMJXoU0oq2GqMOlflVMN-hg" type="text/javascript"></script>--><!-- www.goabroad.net -->
			<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RTaRo377kmnnPR-m6WZJ1eKrLA3uhRWOb5bAJou0jRcQpZZJ4tu_Ndvog" type="text/javascript"></script>--><!-- www.goabroad.com-->
			<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RScORTG_sOsrv1IBKpZjqjBOwk9LhSSRQf3fsJxmnB-oWoJEJWls7bRhQ" type="text/javascript"></script>--><!-- test.goabroad.com-->
			*/
			//$ini = IniConfig::instance();
			echo $this->getJsToInclude();			
		}
		
		public function getJsToInclude(){
			//$ini = Ini_Config::instance();
			//$scripts = "<script src='http://maps.google.com/maps?file=api&amp;v=2.s&amp;key=".$ini->getGMapKey()."' type='text/javascript'></script>";
			require_once('travellog/model/Class.SiteContext.php');
			//$scripts = "<script src='http://maps.google.com/maps?file=api&amp;v=2.s&amp;key=".SiteContext::getInstance()->getGMapApiKey()."' type='text/javascript'></script>";
			$scripts = "<script src='https://maps.googleapis.com/maps/api/js?key=".SiteContext::getInstance()->getGMapApiKey()."&v=3&sensor=true' type='text/javascript'></script>";
			
			if (!class_exists('GAHtmlTemplate')) {
				$scripts.= "<script src='/js/tools/tools.js' type='text/javascript'></script>";
				$scripts.= "<script src='/js/map.js' type='text/javascript'></script>";
				$scripts.= "<script src='/js/mapAgent.js' type='text/javascript'></script>";
				$scripts.= "<script src='/js/mapHelper.js' type='text/javascript'></script>";
			} else {
				$scripts .= GAHtmlTemplate::includeDependentJs(
					array(
						//'/js/tools/tools.js', '/js/map.js', '/js/mapAgent.js', '/js/mapHelper.js'
						'/min/g=JournalMapJs'
					),
					array('return_string' => true)
				);
			}
			return $scripts;
		}
		
		public function addMapFrame($mFrame){
			$mId = ($mFrame->getMapId()=='')?count($this->mapFrameRegistry):$mFrame->getMapId();
			$regMapId = '_map'.$mId;
			//set the new mapID of the new mapframe
			$mFrame->setMapId($regMapId);
			//loop through the mapFrames and check if there are duplicate ids.
			$withDuplicate = false;
			for($i=0;$i<count($this->mapFrameRegistry);$i++){
				if($this->mapFrameRegistry[$i]->getMapId() == $mFrame->getMapId()){
					$withDuplicate = true;
				}
			}
			if($withDuplicate) throw new exception('Error! Duplicate Map ID Detected.');
			else{
				echo "<div id=\"".$regMapId."\" class=\"map\" style=\"width:".$mFrame->getWidth()."px; height:".$mFrame->getHeight()."px\"></div>";
				$this->mapFrameRegistry[count($this->mapFrameRegistry)] = $mFrame;
			}
		}
		
		public function plotLocations($markers=array(), $mId='', $isJQuery = false){
			//set the default mapID, if mId is not defined, get the id of the first map in the map registry									
			 $iMapFrame = $this->getMapFrame($mId);
			 $imapId = $iMapFrame->getMapId();
			 echo '<script  type="text/javascript">';			 
			 echo "//<![CDATA[\n";
			 echo ' jQuery(document).ready(function(){';
			 echo 'var mDummy = new mapDummy();';
			 echo 'mDummy.setMapId("'.$imapId.'");';
			 echo 'mDummy.setConnectMarkers('.$iMapFrame->getConnectMarkers().');';			 
			 echo 'mDummy.setFixZoomLevel(' . $iMapFrame->getZoomLevel() . ');';

			 for($i=0;$i<count($markers);$i++){
			 	echo 'var _dataDummy = new dataDummy();';			 				 	
			 	echo ' _dataDummy.setLat('.$markers[$i]->getLocation()->getCoordinates()->getY().');';
			 	echo ' _dataDummy.setLng('.$markers[$i]->getLocation()->getCoordinates()->getX().');';
			 	if($markers[$i]->isFocused()){ echo ' _dataDummy.setFocus();'; };
			 	echo ' _dataDummy.setLink("'.$markers[$i]->getLink().'");';
			 	echo ' _dataDummy.setTxt("'.$markers[$i]->getTxt().'");';
			 	echo ' _dataDummy.setVisibility("'.$markers[$i]->getVisibility().'");';
			 	echo 'mDummy.addDataDummy(_dataDummy);';
			 }
			 echo 'mapAgent.addMapDummy(mDummy);';
			 echo 'mapAgent.load();';
			 echo '';
			 echo '});';
			 echo "//]]>";
			 echo '</script>';
		}
		
		/**
		 * This is for support of ajax loading of the map.
		 */
		public function plotLocationsThroughAjax($markers=array(), $iMapFrame, $imapId){
			 echo 'var mDummy = new mapDummy();';
			 echo 'mDummy.setMapId("'.$imapId.'");';
			 echo 'mDummy.setConnectMarkers('.$iMapFrame->getConnectMarkers().');';			 
			 echo 'mDummy.setFixZoomLevel(' . $iMapFrame->getZoomLevel() . ');';

			 for($i=0;$i<count($markers);$i++){
			 	echo 'var _dataDummy = new dataDummy();';			 				 	
			 	echo ' _dataDummy.setLat('.$markers[$i]->getLocation()->getCoordinates()->getY().');';
			 	echo ' _dataDummy.setLng('.$markers[$i]->getLocation()->getCoordinates()->getX().');';
			 	if($markers[$i]->isFocused()){ echo ' _dataDummy.setFocus();'; };
			 	echo ' _dataDummy.setLink("'.$markers[$i]->getLink().'");';
			 	echo ' _dataDummy.setTxt("'.$markers[$i]->getTxt().'");';
			 	echo ' _dataDummy.setVisibility("'.$markers[$i]->getVisibility().'");';
			 	echo 'mDummy.addDataDummy(_dataDummy);';
			 }
			 echo 'mapAgent.addMapDummy(mDummy);';
			 echo 'mapAgent.load();';
		}
		
		private function getMapFrame($mId){			
			if($mId == ''){
			 	if (count($this->mapFrameRegistry) == 0) throw new exception('Error! No map was defined.');
			 	else return $this->mapFrameRegistry[0];//if map id is not given, get the first mapframe in the mapFrameRegistry
			 }
			 else{			 				 	
			 	for($i=0;$i<count($this->mapFrameRegistry);$i++){
			 		if('_map'.$mId == $this->mapFrameRegistry[$i]->getMapId()){			 			
			 			return $this->mapFrameRegistry[$i];
			 		}
			 	}
			 	throw new exception('Error! Map with map Id '. $mId .'is not defined.');
			 }			 
		} 		
	}
	
	class MapFrame{		
		private $width;
		private $height;
		private $mapType;
		private $mapId;
		private $connectMarkers;		
		private $validMapTypes;
		private $zoomLevel =1;
				
		function __construct(){
			$this->width = '500';
			$this->height = '300';
			$this->mapType = 'custom';
			$this->mapId = Map::$mapCtr++;
			$this->connectMarkers = true;
			$this->validMapTypes = array('custom','world');			
		}
		public function setWidth($wd){
			$this->width = $wd;
		}
		public function setHeight($ht){
			$this->height = $ht;
		}
		public function setMapType($type){
			if(!in_array($type,$this->validMapTypes)) throw new exception('Error! Invalid Map Type. Valid map types are "custom" and "world".');
			else $this->mapType = $type;
		}
		public function setZoomLevel($zoom){			
			$this->zoomLevel = ($zoom <=0 || !is_numeric($zoom)) ? 1 : $zoom;
		}
		public function setMapId($mId){
			$this->mapId = $mId;			
		}
		public function setConnectMarkers($con){
			if(!is_bool($con)) throw new exception('Error! MapFrame method setConnectMarkers takes in a boolean parameter.');
			else $this->connectMarkers = $con;
		}
		public function getWidth(){
			return $this->width;
		}
		public function getHeight(){
			return $this->height;
		}
		public function getMapType(){
			return $this->mapType;
		}
		public function getMapId(){
			return $this->mapId;
		}
		public function getZoomLevel(){
			return $this->zoomLevel;
		}
		public function getConnectMarkers(){
			return $this->connectMarkers;
		}
	}
?>