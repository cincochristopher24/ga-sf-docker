<?php

	/**
	 * Created on September 7, 2006
	 * Class.SendableFactory.php
	 * @author Cheryl Ivy Q. Go
	 * Purpose: create the appropriate location subclass (Continent,Country, City, Region, NonPopLocation etc)
	 */

	require_once 'travellog/model/Class.SendableType.php';

	class SendableFactory{
	
		private $rs = NULL;
		private $rs2 = NULL;
		private $conn = NULL;
	
		private static $instance;
		static $SendableType;
		
		
		/**
		 * Constructor of class SendableFactory
		 * @return SendableFactory
		 */
		 
		private function SendableFactory(){
							
			try{ 				
				$this-> conn = new Connection();
				$this-> rs   = new Recordset($this->conn);
				$this-> rs2   = new Recordset($this->conn);
			}
			
			catch (exception $e){				   
			   throw $e;
			}
				
		}
		
		
		/**
		 * Function name: getInstance
		 * @static
		 * @return SendableFactory
		 * Creates a single instance of this class
		 */
		 
		static function Instance(){
		
			if (self::$instance == NULL)
				self::$instance = new SendableFactory();
				
			return self::$instance;
		}
		
		/**
		 * Function name: Create
		 * @param integer $_sendableID
		 * @return Traveler|Group|NULL|exception
	 	 * Creates an instance of sendable based on sendable id
	 	 */
	 	 
		function Create($_sendableID = 0){
			$sendable = null;

			if (0 < $_sendableID){
				$sqlQuery = "SELECT `sendableType` " .
							"FROM tblSendable " .
							"WHERE `sendableID` = " . $_sendableID . " " .
							"GROUP BY `sendableID`";
				$this-> rs-> Execute($sqlQuery);

				if (0 < $this->rs->Recordcount()){
					$sendabletype = $this->rs->Result(0, "sendabletype"); 		

		 			switch ($sendabletype){
			 			case SendableType::$TRAVELER :
			 				$sqlQuery2 = "SELECT `travelerID` " .
										"FROM tblTraveler " .
										"WHERE `sendableID` = " . $_sendableID;
			 				$this-> rs2-> Execute($sqlQuery2);

			 				if (0 == $this->rs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new exception("Invalid sendable id");	
				 			}		

					 		self::$SendableType = SendableType::$TRAVELER;
			 				$sendable = new Traveler($this->rs2->Result(0, "travelerID"));

			 				break;
			 			case SendableType::$GROUP :
			 				$sqlQuery2 = "SELECT `groupID` " .
										"FROM tblGroup " .
										"WHERE `sendableID` = " . $_sendableID;
			 				$this-> rs2-> Execute($sqlQuery2);

							if (0 == $this->rs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new exception("Invalid sendable id");	
				 			}

				 			self::$SendableType = SendableType::$GROUP;

			 				try{
								$mGroup = GroupFactory::instance()->create( array($this->rs2->Result(0, "groupID")) );
								$sendable = $mGroup[0];
							}

							catch (exception $e){
								throw $e;
							}

			 				break; 			
			 			case SendableType::$STAFF :
			 				$sqlQuery2 = "SELECT * " .
										"FROM tblGrouptoFacilitator " .
										"WHERE `sendableID` = " . $_sendableID . " " .
										"AND type = 1";
			 				$this-> rs2-> Execute($sqlQuery2);

							if (0 == $this->rs2->Recordcount()) {			 			
					 			// id not valid so throw exception
					 			throw new exception("Invalid sendable id");	
				 			}

				 			self::$SendableType = SendableType::$STAFF;
				 			require_once 'travellog/model/Class.Staff.php';

			 				try{
								$sendable = new Staff($this->rs2->Result(0,'travelerID'), $this->rs2->Result(0,'groupID'));
							}

							catch (exception $e){
								throw $e;
							}

			 				break;
					}
				}
			}

 			return $sendable;
		}
	}
?>
