<?php
	/**
	 * Created on 14 August 2007
	 * Class.PokeType.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once('Cache/ganetCacheProvider.php');
	require_once('Class.ConnectionProvider.php');
	
	class PokeType{

		private $rs					= NULL;
		private $conn				= NULL;
		private $path				= '/images/poke/';
		private $image				= '';
		private $author				= '';
		private $context			= '';
		private $pokeType			= '';
		private $pokeTypeID			= 0;

		public static $SMILE		= 1;
		public static $THUMBSUP		= 2;
		public static $CHEERSMATE	= 3;
		public static $THAIWAI		= 4;
		public static $WAVE			= 5;
		public static $BOWDOWN		= 6;
		public static $CONGRATULATE	= 7;

		function PokeType($_pokeTypeID = 0){
			try{
				$this-> rs	 = new Recordset(ConnectionProvider::instance()->getConnection());
			}
			catch(exception $e){
				throw $e;
			}

			if (0 < $_pokeTypeID){
				$found = false;				
				$cache = ganetCacheProvider::instance()->getCache();
				if ($cache != null) {
					$pokeType = $cache->get('PokeType_' . $_pokeTypeID );
					if ($pokeType){
						//echo 'Found PokeType_' . $_pokeTypeID .' in cache <br/>';
						$this-> pokeTypeID = $_pokeTypeID;
						$this-> pokeType = $pokeType->getPokeType();
						$this-> image =    $pokeType->getPokeImage();
						$found = true;
					}
				}
				
				if (!$found){
					$sql = "select * from tblPokeType where pokeTypeID = " . $_pokeTypeID;
					$this-> rs-> Execute($sql);
	
					if (0 == $this->rs->Recordcount())
						throw new exception ("Poke type ID is invalid!");
	
					$this-> pokeTypeID = $this-> rs-> Result(0, "pokeTypeID");
					$this-> pokeType = $this-> rs-> Result(0, "pokeType");
					$this-> image = $this-> rs-> Result(0, "image");
					
					if ($cache != null){
						$cache->set('PokeType_' . $this->pokeTypeID,$this,array('EXPIRE'=>86400));
					}
				}	
			}
			return;
		}

		/**
		 * Setters
		 */
		function setPokeType($_pokeType = ''){
			$this-> pokeType = $_pokeType;
		}

		function setPokeImage($_image = ''){
			$this-> image = $_image;
		}

		function setPath($_path){
			$this-> path = $_path;
		}

		/**
		 * Getters
		 */
		function getPokeTypeID(){
			return $this-> pokeTypeID;
		}

		function getPokeType(){
			return $this-> pokeType;
		}

		function getPokeImage(){
			return $this-> image;
		}

		function getPath(){
			return "http://" . $_SERVER['HTTP_HOST'] . $this->path;
		}

		/**
		 * Static
		 */
		static function getPokeTypes($_section = ''){
			try{
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
			}
			catch(exception $e){
				throw $e;
			}

			if (!strcasecmp($_section, "profile"))
				$condition = "pokeTypeID <> 2";
			else
				$condition = " 1 = 1 ";

			$sql = "select pokeTypeID from tblPokeType where " . $condition;
			$rs-> Execute($sql);

			$arrPokeType = array();
			while ($pokeType = mysql_fetch_array($rs->Resultset())){
				$arrPokeType[] = new PokeType($pokeType['pokeTypeID']);
			}

			return $arrPokeType;
		}
	}
?>