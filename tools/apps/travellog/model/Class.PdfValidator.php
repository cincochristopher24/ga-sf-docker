<?php
/*
 * Created on Feb 14, 2007
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
include_once('Class.ValidatorOuput.php'); 
include_once('Class.UploadValidator.php'); 
include_once('Class.PathManager.php');

class PdfValidator extends UploadValidator{ 
		
		private $arr_files				= array();
		private $arr_validatoroutput 	= array(); 
		private $arr_validfiles		= array();
		private $arr_invalidfiles		= array();
		
		private $arr_document_titles = array();

		function setFiles($files){
			$this->arr_files = $files;
		}
		
		function setDocumentTitles($titles=array()){
			$this->arr_document_titles = $titles;
		}
		
		function  validate(){
			
				$files = $this->arr_files;
				$cnt = 1;
				
				$temppath = PathManager::$temporarypath;
				
				if($files){
				
					foreach($files as $index => $ufile){
							
							$objval_ouput = new ValidatorOutput();						
							$objval_ouput->setSize($ufile->getFilesize());
							$objval_ouput->setFType($ufile->getFiletype());
							$objval_ouput->setCaption(str_replace('.pdf','',$ufile->getFilename()));
							if( isset($this->arr_document_titles[$index]) ){
								$objval_ouput->setDocumentTitle($this->arr_document_titles[$index]);
							}
							
							$pdf = NULL;
														
							if(file_exists($temppath.$ufile->getFilename())){
									
									
									$path_parts = pathinfo($temppath.$ufile->getFilename());
									$ufilename = $cnt.uniqid().time().".".$path_parts['extension'];						
									
									rename($temppath.$ufile->getFilename(),$temppath.$ufilename);
									
									$content	=	file_get_contents($temppath.$ufilename,FALSE,NULL,0,500);																	
									$pdf = strstr($content, 'PDF');											
																					
									if($pdf){
											$objval_ouput->setName($ufilename);
											
											$objval_ouput->setMessage("Successfully uploaded!");
											$objval_ouput->setStatus(true);	
									}else{
											//invalid Type of PDF
											$objval_ouput->setName($ufile->getFilename());
											$objval_ouput->setMessage("Invalid type of PDF");
																				
											unlink($temppath.$ufilename);
																																		
									}
							}else{
											
											//invalid Type of Image
											$objval_ouput->setName($ufile->getFilename());
											$objval_ouput->setMessage("Invalid type of PDF");
											//unlink($temppath.$ufilename);	
											
							}
							$this->arr_validatoroutput[] = $objval_ouput;
					
					$cnt = $cnt + 1;
					
					}
				
				}else{
					$objval_ouput = new ValidatorOutput();						
					$objval_ouput->setSize(0);
					$objval_ouput->setFType(NULL);
					//Null Values
					$objval_ouput->setName(NULL);
					$objval_ouput->setMessage("Please select PDF file!");
					
					$this->arr_validatoroutput[] = $objval_ouput;
					
				}
				
		}
		
		
		
		public function getValidFiles(){
			foreach($this->arr_validatoroutput as $outfiles)
				if($outfiles->getStatus())$this->arr_validfiles[] =	$outfiles;
			return $this->arr_validfiles;
		}
		
		public function getInValidFiles(){
			foreach($this->arr_validatoroutput as $outfiles)
				if(!$outfiles->getStatus())$this->arr_invalidfiles[] =$outfiles;
			return $this->arr_invalidfiles;
		}

		public function getResults(){
			return $this->arr_validatoroutput;	
		}
		
		

}

?>
