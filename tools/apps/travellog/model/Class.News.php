<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: 
 */
    
    require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	 
    class News{

        /**
         * Define variables for Class Member Attributes
         */
            private $mConn     		 = NULL;
            private $mRs       		 = NULL;
           
            private $mGroupID  		 = NULL;
            
            private $mNewsID   		 = NULL;
            private $mTitle    		 = NULL;
            private $mDate     		 = NULL;
            private $mContent  		 = NULL;
            private $mDisplayPublic  = NULL;
           
                   
        /**
         * Constructor Function for this class
         */
            function News($newsID = 0){
                
                $this->mNewsID = $newsID;
                
                try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mNewsID){
                    
                    $sql = "SELECT * FROM tblNews WHERE newsID = '$this->mNewsID' ";
                    $this->mRs->Execute($sql);
                
                    if (0 == $this->mRs->Recordcount()){
                        throw new Exception("Invalid NewsID");        // ID not valid so throw exception
                    }
                    
                    // Sets values to its class member attributes.
                    $this->mTitle     		= stripslashes($this->mRs->Result(0,"title"));           
                    $this->mContent   		= stripslashes($this->mRs->Result(0,"content"));       
                    $this->mDate      		= $this->mRs->Result(0,"adate");       
                    $this->mDisplayPublic  	= $this->mRs->Result(0,"displaypublic");  
                    
                    
                    // Sets a value to its parent key.
                    $this->mGroupID   = $this->mRs->Result(0,"groupID");
                    
                }
            }
           
                       
        /**
         * Setter Functions
         */ 
            function setNewsID($newsID){
               $this->mNewsID = $newsID;
            }
            
            function setTitle($title){
               $this->mTitle = $title;
            }
           
            function setTheDate($date){
               $this->mDate = $date;
            }
           
            function setContent($content){
               $this->mContent = $content;
            }
            
            function setDisplayPublic($displaypublic){
               $this->mDisplayPublic = $displaypublic;
            }
           
           
        /**
         * Getter Functions
         */  
            function getNewsID(){
               return $this->mNewsID;
            }
            
            function getTitle(){
                return $this->mTitle;
            }
           
            function getTheDate(){
                return $this->mDate;
            }
           
           	function getCreated(){
                return $this->mDate;
            }
            
            function getContent(){
                return $this->mContent;
            }
      
      		function getDisplayPublic(){
                return $this->mDisplayPublic;
            }
                      
        /**
         * Setter Function for the Parent Key
         */
            function setGroupID($groupID){
                $this->mGroupID = $groupID;
            }     
            
     
        /**
         * Getter Function for the Parent Key
         */        
            function getGroupID(){
                return $this->mGroupID;
            }
            
     
        /**
         * CRUD Methods (Create, Update , Delete)
         */
            function Create(){
                
                $Atitle   = addslashes($this->mTitle);
                $Acontent = addslashes($this->mContent);
                
                $sql = "INSERT into tblNews (groupID, title, content, adate, displaypublic) " .
                        "VALUES ('$this->mGroupID', '$Atitle', '$Acontent', '$this->mDate', '$this->mDisplayPublic')";
                $this->mRs->Execute($sql);       
                $this->mNewsID = $this->mRs->GetCurrentID();
            }
            
            function Update(){
                
                $Atitle   = addslashes($this->mTitle);
                $Acontent = addslashes($this->mContent);
                
                $sql = "UPDATE tblNews SET title = '$Atitle', content = '$Acontent', adate = '$this->mDate', displaypublic = '$this->mDisplayPublic' " .
                		"WHERE newsID = '$this->mNewsID'  " ;
                $this->mRs->Execute($sql);
            }
            
            function Delete(){
                
                $sql = "DELETE FROM tblNews WHERE newsID = '$this->mNewsID' ";         
                $this->mRs->Execute($sql);
            }          
    }
        
?>
