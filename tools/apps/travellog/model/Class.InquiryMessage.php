<?php
	/**
	 * Created on September 27, 2006
	 * Class.InquiryMessage.php extends Class.Message.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'travellog/model/Class.Message.php';

	class InquiryMessage extends Message{
		
		//private $rs = NULL;
		//private $conn = NULL;
		
		/**
		 * Constructor of class InquiryMessage
		 * @param integer $_attributeID
		 * @return Message
		 */
		
		function InquiryMessage($_attributeID = 0, $_sendableId = 0){			
			try{ 				
 				$this->conn  = new Connection();
				$this->rs    = new Recordset($this->conn);
			}
			
			catch (exception $e){				   
			   throw $e;
			}
			
			$this-> setDiscriminator(DiscriminatorType::$INQUIRY);
			$this-> setSendableID($_sendableId);

			return $this-> Message($_attributeID);
		}
				
		/**
	 	 * Function name: Send
	 	 */
	 	
		function Send(){
	 		$_source = $this-> getSource()-> getSendableID();

			try{
				$factory = SendableFactory::Instance();
				$Source = $factory-> Create($_source);
			}
			
			catch (exception $e){
				throw $e;
			}

	 		$Source-> sendMessage($this);
	 	}
	 		 	
	 	/**
	 	 * Function name: Save
	 	 */

	 	function Save(){
	 		
	 		$_source = $this-> getSource()-> getSendableID();
	 		
	 		$factory = SendableFactory::Instance();
			$Source = $factory-> Create($_source);
	 		
	 		$Source-> saveAsDraft($this);
	 	}
	 	
	 	
	 	/**
	 	 * Function name: setParentID
	 	 * @param int $_parentID
	 	 */
	 	
	 	function setParentID($_parentID = 0){
	 		$this-> parentID = $_parentID;
	 	}
	 	
	 	/**
		 * Function name: setListingID
		 * @param int $_listingID
		 */
		 		 
		function setListingID($_listingID = 0){
			$this-> listingID = $_listingID;
		}
		
		/**
		 * Function name: setSource
		 * @param Traveler $_src
		 */
		 		 
		function setSource($_src = NULL){
			$this-> source = $_src;
		}

		/**
		 * Function name: setSendableID
		 * @param int $_sendableID
		 */

		function setSendableID($_sendableID = NULL){
			$this-> sendableID = $_sendableID;
		}

		
		/**
		 * Function name: setDestination
		 * @param array $_dest
		 */
		 
		function setDestination($_dest = array()){
			$this-> destination = $_dest;
		}
		
		/**
		 * Function name: isParent
		 * @param integer $_attributeID
		 * @return Bool
		 */
		 
		static function isParent($_attributeID = 0){
			
			try{ 				
 				$conn  = new Connection();
				$rs    = new Recordset($conn);
			}
			
			catch (exception $e){				   
			   throw $e;
			}
			
			$sqlQuery = "SELECT b.`messageID` " .
						"FROM tblMessageToAttribute AS a, tblMessageToInquiry AS b " .
						"WHERE a.`attributeID` = " . $_attributeID . " " .
						"AND b.`parentID` = 0 " .
						"AND b.`messageID` = a.`messageID`";
			$rs->Execute($sqlQuery);
			
			if (0 == $rs->Recordcount())
				return false;
			else
				return true;
		}
		
		/**
		 * Function name: getRepliesToMessage
		 * @param RowsLimit $_rowsLimit
		 * @return array|NULL|exception phpDocumentor array of Messages or Null or exception
		 */
		 
		function getRepliesToMessage(){

			$sqlQuery = "SELECT c.`attributeID` " .
						"FROM tblMessages as a, tblMessageToInquiry as b, tblMessageToAttribute as c " .
						"WHERE a.`discriminator` = 4 " .
						"AND " .
							"(" .
								"(c.`messageType` = 4 AND a.senderID = " . $this->source->getSendableID() . ")" .
								" OR " .
								"(c.`messageType` = 2 AND c.recipientID = " . $this->source->getSendableID() . ")" .
							") " .
						"AND c.`trashed` = 0 " .
						"AND b.`parentID` = " . $this->getMessageID() . " " .
						"AND a.`messageID` = b.`messageID` " .
						"AND c.`messageID` = b.`messageID` " .
						"ORDER BY a.`dateCreated` DESC ";

			$this-> rs-> Execute($sqlQuery);
			$messages = array();
			
			while ($resMessage = mysql_fetch_array ($this->rs->Resultset())){
				
				$Message = new InquiryMessage($resMessage['attributeID'], $this->sendableID);
				$messages[] = $Message;
			}
			
			return $messages;
		}
	}
?>