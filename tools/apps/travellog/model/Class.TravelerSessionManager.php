<?php
	require_once('Class.iRecordset.php');
	require_once('Class.dbHandler.php');
	
	class TravelerSessionManager
	{
		
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new self;
			}
			return self::$instance;
		}
		
		private $travelerSessions = array();
		private $db = null;
		private function __construct(){
			$this->db = new dbHandler();
		}
		
		public function hasSession($userID,$domain=null){
			$key = $userID.'_'.$domain;
			if(!array_key_exists($key,$this->travelerSessions)){
				$sql = "SELECT * FROM tblSessions WHERE session_user_id = {$userID}";
				if(!is_null($domain)){
					 $sql .= " AND domain = '{$domain}'";
				}
				$rs = new iRecordset($this->db->execute($sql));
				$this->travelerSessions[$key] = $rs->retrieveRecordCount() > 0;
			}
			return $this->travelerSessions[$key];
		}
		
		public function killSessions($userID,$domain=null){
			$sql = 	"DELETE FROM tblSessions " .
 					"WHERE session_user_id = {$userID}";
			if(!is_null($domain)){	
 				$sql .= " AND domain = '{$domain}'";
			}
			$this->db->execute($sql);			
		}
	}