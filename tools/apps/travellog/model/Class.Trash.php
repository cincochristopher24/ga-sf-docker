<?php
	/**
	 * Created on September 7, 2006
	 * Class.Trash.php 
	 * @author Cheryl Ivy Q. Go
	 * Last Edited on 22 Jan 2008 -- refactored
	 * Last Edited on 13 May 2008 -- optimized query
	 */

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.MessageSendableFactory.php';

	class Trash{

		private $mRs	= NULL;
		private $mRs2	= NULL;
		private $mRs3	= NULL;
		private $mRs4	= NULL;
		private $mRs5	= NULL;
		private $mConn	= NULL;

		private $mMessageId		= 0;
		private $mSendableId	= 0;
		private $mTotalRecords	= 0;
		
		/**
		 * Constructor of class Trash
		 * @param integer $_sendableID
		 * @return Trash
		 */
		
		function Trash($_sendableID = 0){				
 			try{ 				
 				$this->mConn = new Connection();
				$this->mRs	= new Recordset($this->mConn);
				$this->mRs2	= new Recordset($this->mConn);
				$this->mRs3	= new Recordset($this->mConn);
				$this->mRs4	= new Recordset($this->mConn);
				$this->mRs5	= new Recordset($this->mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$this->mSendableId = $_sendableID;			
			return $this;	
		}

		/**
		 * Function name: Delete
		 * @param integer $_attributeID
		 * Deletes message with messageID = $_messageID from tblMessages
		 */

		function Delete($_attributeID = 0){
			// get messageID and messageType of message that is to be deleted			
			$sql2 = "SELECT messageID, messageType " .
					"FROM tblMessageToAttribute " .
					"WHERE attributeID = " . $_attributeID;
			$this->mRs2->Execute($sql2);

			$messageID = $this->mRs2->Result(0, "messageID");
			$messageType = $this->mRs2->Result(0, "messageType");

			if (0 < $this->mRs2->Recordcount()){
				// check if there are travelers that use the same message
				$sql3 = "SELECT attributeID FROM tblMessageToAttribute WHERE messageID = " . $messageID;
				$this->mRs3->Execute($sql3);

				// if there are still travelers who use the same message			
				if (1 < $this->mRs3->Recordcount()){
					// if INBOX or INQUIRY, set trashed field to deleted				
					if ( (MessageType::$INBOX == $messageType) || (MessageType::$INQUIRYBOX == $messageType) || (MessageType::$SENT == $messageType) ){

						$sql4 = "DELETE FROM tblMessageToAttribute WHERE attributeID = " . $_attributeID;
						$this->mRs4->Execute($sql4);
					}
					// DRAFT
					else{
						// Delete row in tblMessages
						$sql4 = "DELETE FROM tblMessages WHERE messageID = " . $messageID;
						$this->mRs4->Execute($sql4);

						// Delete rows in tblMessageToAttribute
						$sql5 = "DELETE FROM tblMessageToAttribute WHERE messageID = " . $messageID . " AND messageType = " . $messageType;
						$this->mRs5->Execute($sql5);
					}
				}
				else{
					// Delete rows in tblMessageToAttribute				
					$sql4 = "DELETE FROM tblMessageToAttribute WHERE messageID = " . $messageID;
					$this->mRs4->Execute($sql4);

					// Delete message from tblMessages				
					$sql5 = "DELETE FROM tblMessages WHERE messageID = " . $messageID;
					$this->mRs5->Execute($sql5);
				}
			}			
		}

		/**
		 * Function name: DeleteAll
		 * Deletes all trashed messages from tblMessages
		 */

		function DeleteAll(){
			$this->DeleteTrashedInbox();
			$this->DeleteTrashedSent();
			$this->DeleteTrashedDraft();
			// kulang pa hin delete trashed inquiry
		}
		function DeleteTrashedInbox(){
			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
			}
			catch (exception $e){
				throw $e;
			}

			// Get trashed INBOX messages
			$sql = "SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.trashed = 1 " .
					"AND tblMessageToAttribute.messageType = 1 " .
					"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;

			if ($Sendable instanceof Traveler){
				$travelerId = $Sendable->getTravelerID();

				// if user is administrator of any group, include messages
				$sql .= " UNION " .
					"SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.messageType = 1 " .
					"AND tblMessageToAttribute.trashed = 1 " .
					"AND EXISTS " .
					"(" .
						"SELECT sendableID " .
						"FROM tblGroup " .
						"WHERE sendableID = tblMessageToAttribute.recipientID " .
						"AND isSuspended = 0 " .
						"AND administrator = " . $travelerId .
					")";
				// if user is staff of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 1 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE sendableID = tblMessageToAttribute.recipientID " .
							"AND type = 1 " .
							"AND status = 1 " .
							"AND travelerID = " . $travelerId .
						")";
			}
			else{
				if ($Sendable->getDiscriminator() == Group::ADMIN){
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.trashed = 1 " .
							"AND tblMessages.discriminator = 3 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND isSuspended = 0 " .
								"AND parentID = " . $Sendable->getGroupID() .
							")";
				}
			}
			$this->mRs-> Execute($sql);

			if (0 < $this->mRs->Recordcount()){
				while($Message = mysql_fetch_array($this->mRs->Resultset()))
					$this->Delete($Message['attributeID']);
			}
		}
		function DeleteTrashedSent(){
			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
			}
			catch (exception $e){
				throw $e;
			}

			// Get trashed SENT messages
			$sql = "SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.trashed = 1 " .
					"AND tblMessageToAttribute.messageType = 2 " .
					"AND tblMessages.senderID = " . $this->mSendableId;

			// According to requirements, only TRAVELER has a SENT FOLDER
			if ($Sendable instanceof Traveler){
				$travelerId = $Sendable->getTravelerID();

				// if user is administrator of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 2 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGroup " .
							"WHERE sendableID = tblMessages.senderID " .
							"AND isSuspended = 0 " .
							"AND administrator = " . $travelerId .
						")";
				// if user is staff of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 2 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE sendableID = tblMessages.senderID " .
							"AND type = 1 " .
							"AND status = 1 " .
							"AND travelerID = " . $travelerId .
						")";
			}
			$this->mRs->Execute($sql);

			if (0 < $this->mRs->Recordcount()){
				while($Message = mysql_fetch_array($this->mRs->Resultset()))
					$this->Delete($Message['attributeID']);
			}
		}
		function DeleteTrashedDraft(){
			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
			}
			catch (exception $e){
				throw $e;
			}

			// Get trashed DRAFT messages
			$sql = "SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.trashed = 1 " .
					"AND tblMessageToAttribute.messageType = 3 " .
					"AND tblMessages.senderID = " . $this->mSendableId;

			// According to requirements, only TRAVELER has a DRAFTS FOLDER
			if ($Sendable instanceof Traveler){
				$travelerId = $Sendable->getTravelerID();

				// if user is administrator of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 3 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGroup " .
							"WHERE sendableID = tblMessages.senderID " .
							"AND isSuspended = 0 " .
							"AND administrator = " . $travelerId .
						")";
				// if user is staff of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 3 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE sendableID = tblMessages.senderID " .
							"AND type = 1 " .
							"AND status = 1 " .
							"AND travelerID = " . $travelerId .
						")";
			}
			$this->mRs->Execute($sql);

			if (0 < $this->mRs->Recordcount()){
				while($Message = mysql_fetch_array($this->mRs->Resultset()))
					$this->Delete($Message['attributeID']);
			}
		}

		/***************************** START: SETTERS *************************************/
		function setInstance($_instance = 0){
			$this->mInstance = $_instance;
		}
		function setTotalRecords($_totalRecords = 0){
			$this->mTotalRecords = $_totalRecords;
		}
		function setMessageId($_messageID = 0){
			$this->mMessageId = $_messageID;
		}
		function setSendableId($_sendableID = 0){
			$this->mSendableId = $_sendableID;
		}
		/******************************* END: SETTERS *************************************/

		/***************************** START: GETTERS *************************************/
		function getTotalRecords(){
			return $this->mTotalRecords;
		}
		function getTrashList($_rowsLimit = NULL){

			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
			}
			catch (exception $e){
				throw $e;
			}

			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " .
					"(";
					$sql .= "SELECT * FROM " .
						"(";
						// Get trashed INBOX messages
						$sql .= "SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
								"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
								"AND tblMessages.discriminator = 3 " .
								"AND tblMessageToAttribute.trashed = 1 " .
								"AND tblMessageToAttribute.messageType = 1 " .
								"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;

						if ($Sendable instanceof Traveler){
							$travelerId = $Sendable->getTravelerID();

							// if user is administrator of any group, include messages
							$sql .= " UNION " .
								"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
								"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID,  tblMessageToAttribute.messageType " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
								"AND tblMessages.discriminator = 3 " .
								"AND tblMessageToAttribute.messageType = 1 " .
								"AND tblMessageToAttribute.trashed = 1 " .
								"AND EXISTS " .
								"(" .
									"SELECT sendableID " .
									"FROM tblGroup " .
									"WHERE sendableID = tblMessageToAttribute.recipientID " .
									"AND isSuspended = 0 " .
									"AND administrator = " . $travelerId .
								")";
							// if user is staff of any group, include messages
							$sql .= " UNION " .
								"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
								"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
								"AND tblMessages.discriminator = 3 " .
								"AND tblMessageToAttribute.messageType = 1 " .
								"AND tblMessageToAttribute.trashed = 1 " .
								"AND EXISTS " .
								"(" .
									"SELECT sendableID " .
									"FROM tblGrouptoFacilitator " .
									"WHERE sendableID = tblMessageToAttribute.recipientID " .
									"AND type = 1 " .
									"AND status = 1 " .
									"AND travelerID = " . $travelerId .
								")";
						}
						else{
							if ($Sendable->getDiscriminator() == Group::ADMIN){
								$sql .= " UNION " .
									"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
									"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
									"FROM tblMessages, tblMessageToAttribute " .
									"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
									"AND tblMessageToAttribute.messageType = 1 " .
									"AND tblMessageToAttribute.trashed = 1 " .
									"AND tblMessages.discriminator = 3 " .
									"AND EXISTS " .
									"(" .
										"SELECT sendableID " .
										"FROM tblGroup " .
										"WHERE sendableID = tblMessageToAttribute.recipientID " .
										"AND isSuspended = 0 " .
										"AND parentID = " . $Sendable->getGroupID() .
									")";
							}
						}
						$sql .= ") as allInbox GROUP BY attributeID";

				$sql .= " UNION ";

				$sql .= "SELECT * FROM " .
					"(";
					// Query all trashed DRAFT AND SENT MESSAGES of traveler
					$sql .= "SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.trashed = 1 " .
							"AND tblMessages.senderID = " . $this->mSendableId;

					// According to requirements, only TRAVELER has a DRAFTS FOLDER
					if ($Sendable instanceof Traveler){
						$travelerId = $Sendable->getTravelerID();

						// if user is administrator of any group, include messages
						$sql .= " UNION " .
								"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
								"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
								"AND tblMessages.discriminator = 3 " .
								"AND tblMessageToAttribute.trashed = 1 " .
								"AND EXISTS " .
								"(" .
									"SELECT sendableID " .
									"FROM tblGroup " .
									"WHERE sendableID = tblMessages.senderID " .
									"AND isSuspended = 0 " .
									"AND administrator = " . $travelerId .
								")";
						// if user is staff of any group, include messages
						$sql .= " UNION " .
								"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
								"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
								"AND tblMessages.discriminator = 3 " .
								"AND tblMessageToAttribute.trashed = 1 " .
								"AND EXISTS " .
								"(" .
									"SELECT sendableID " .
									"FROM tblGrouptoFacilitator " .
									"WHERE sendableID = tblMessages.senderID " .
									"AND type = 1 " .
									"AND status = 1 " .
									"AND travelerID = " . $travelerId .
								")";
					}
				$sql .= ") as allInbox GROUP BY messageID";
			$sql .= ") as allInbox GROUP BY attributeID ORDER BY datecreated DESC  ". $queryLimit;
			$this->mRs->Execute($sql);

			$messages = array();
			$sql2 = "SELECT FOUND_ROWS() as totalRecords";
			$this->mRs2-> Execute($sql2);

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_array($this->mRs->Resultset())){
				$message = new PersonalMessage();
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setMessageTypeId($rMessage['messageType']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));
				
				$message->setIsRead($rMessage['isRead']);

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setMessageRecipient();

				$arrMessage[] = $message;
			}

			return $arrMessage;
		}		
		function getTotalNumberOfTrash(){
			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
			}
			catch (exception $e){
				throw $e;
			}

			// Get trashed INBOX messages
			$sql = "SELECT attributeID, messageID FROM " .
					"(";
						$sql .= "SELECT tblMessageToAttribute.attributeID, tblMessageToAttribute.messageID " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
								"AND tblMessages.discriminator = 3 " .
								"AND tblMessageToAttribute.trashed = 1 " .
								"AND tblMessageToAttribute.messageType = 1 " .
								"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;

						if ($Sendable instanceof Traveler){
							$travelerId = $Sendable->getTravelerID();

							// if user is administrator of any group, include messages
							$sql .= " UNION " .
									"SELECT tblMessageToAttribute.attributeID, tblMessageToAttribute.messageID " .
									"FROM tblMessages, tblMessageToAttribute " .
									"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
									"AND tblMessages.discriminator = 3 " .
									"AND tblMessageToAttribute.messageType = 1 " .
									"AND tblMessageToAttribute.trashed = 1 " .
									"AND EXISTS " .
									"(" .
										"SELECT sendableID " .
										"FROM tblGroup " .
										"WHERE sendableID = tblMessageToAttribute.recipientID " .
										"AND isSuspended = 0 " .
										"AND administrator = " . $travelerId .
									")";
							// if user is staff of any group, include messages
							$sql .= " UNION " .
									"SELECT tblMessageToAttribute.attributeID, tblMessageToAttribute.messageID " .
									"FROM tblMessages, tblMessageToAttribute " .
									"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
									"AND tblMessages.discriminator = 3 " .
									"AND tblMessageToAttribute.messageType = 1 " .
									"AND tblMessageToAttribute.trashed = 1 " .
									"AND EXISTS " .
									"(" .
										"SELECT sendableID " .
										"FROM tblGrouptoFacilitator " .
										"WHERE sendableID = tblMessageToAttribute.recipientID " .
										"AND type = 1 " .
										"AND status = 1 " .
										"AND travelerID = " . $travelerId .
									")";
						}
						else{
							if ($Sendable->getDiscriminator() == Group::ADMIN){
								$sql .= " UNION " .
										"SELECT tblMessageToAttribute.attributeID, tblMessageToAttribute.messageID " .
										"FROM tblMessages, tblMessageToAttribute " .
										"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
										"AND tblMessageToAttribute.messageType = 1 " .
										"AND tblMessageToAttribute.trashed = 1 " .
										"AND tblMessages.discriminator = 3 " .
										"AND EXISTS " .
										"(" .
											"SELECT sendableID " .
											"FROM tblGroup " .
											"WHERE sendableID = tblMessageToAttribute.recipientID " .
											"AND isSuspended = 0 " .
											"AND parentID = " . $Sendable->getGroupID() .
										")";
							}
						}
				$sql .= ") as allInbox GROUP BY attributeID ";

			$sql .= " UNION " .
					"SELECT attributeID, messageID FROM " .
					"(";
						// Query all trashed DRAFT AND SENT MESSAGES of traveler
						$sql .= "SELECT tblMessageToAttribute.attributeID, tblMessageToAttribute.messageID " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
								"AND tblMessages.discriminator = 3 " .
								"AND tblMessageToAttribute.trashed = 1 " .
								"AND tblMessages.senderID = " . $this->mSendableId;

						// According to requirements, only TRAVELER has a DRAFT AND SENT FOLDER
						if ($Sendable instanceof Traveler){
							$travelerId = $Sendable->getTravelerID();

							// if user is administrator of any group, include messages
							$sql .= " UNION " .
									"SELECT tblMessageToAttribute.attributeID, tblMessageToAttribute.messageID " .
									"FROM tblMessages, tblMessageToAttribute " .
									"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
									"AND tblMessages.discriminator = 3 " .
									"AND tblMessageToAttribute.trashed = 1 " .
									"AND EXISTS " .
									"(" .
										"SELECT sendableID " .
										"FROM tblGroup " .
										"WHERE sendableID = tblMessages.senderID " .
										"AND isSuspended = 0 " .
										"AND administrator = " . $travelerId .
									")";
							// if user is staff of any group, include messages
							$sql .= " UNION " .
									"SELECT tblMessageToAttribute.attributeID, tblMessageToAttribute.messageID " .
									"FROM tblMessages, tblMessageToAttribute " .
									"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
									"AND tblMessages.discriminator = 3 " .
									"AND tblMessageToAttribute.trashed = 1 " .
									"AND EXISTS " .
									"(" .
										"SELECT sendableID " .
										"FROM tblGrouptoFacilitator " .
										"WHERE sendableID = tblMessages.senderID " .
										"AND type = 1 " .
										"AND status = 1 " .
										"AND travelerID = " . $travelerId .
									")";
						}
					$sql .= ") as allInbox GROUP BY messageID";
			$this->mRs->Execute($sql);

			return $this->mRs->Recordcount();
		}
		/******************************* END: GETTERS *************************************/

		static function isOwner($_attributeID = 0, $_sendableID = 0){
			try{
 				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($_sendableID);
			}
			catch (exception $e){
				throw $e;
			}

			if ($Sendable instanceof Traveler){
				$travelerId = $Sendable->getTravelerID();

				// Query all trashed INBOX MESSAGES of traveler
				$sql = "SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND tblMessageToAttribute.messageType = 1 " .
						"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
						"AND tblMessageToAttribute.recipientID = " . $_sendableID;
				// if user is administrator of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 1 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGroup " .
							"WHERE sendableID = tblMessageToAttribute.recipientID " .
							"AND isSuspended = 0 " .
							"AND administrator = " . $travelerId .
						")";
				// if user is staff of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 1 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE sendableID = tblMessageToAttribute.recipientID " .
							"AND type = 1 " .
							"AND status = 1 " .
							"AND travelerID = " . $travelerId .
						")";

				// Query all trashed DRAFT AND SENT MESSAGES of traveler
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
						"AND tblMessages.senderID = " . $_sendableID;
				// if user is administrator of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGroup " .
							"WHERE sendableID = tblMessages.senderID " .
							"AND isSuspended = 0 " .
							"AND administrator = " . $travelerId .
						")";
				// if user is staff of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.trashed = 1 " .
						"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE sendableID = tblMessages.senderID " .
							"AND type = 1 " .
							"AND status = 1 " .
							"AND travelerID = " . $travelerId .
						")";
				$mRs-> Execute($sql);
				
				if (0 < $mRs->Recordcount())
					return true;
				else
					return false;
			}
		}
	}
?>