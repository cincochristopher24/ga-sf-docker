<?php
	/**
	 * @(#) Class.SqlCriteria2.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - May 22, 2009
	 */
	
	/**
	 * This is used for the creation of sql statements. Usage is actually applied
	 * to peer classes. 
	 */
	class SqlCriteria2 {
	
		/** The mysql EQUAL operator **/
		const EQUAL = '=';
		
		/** The mysql NOT EQUAL operator **/
		const NOT_EQUAL = '<>';
		
		/** The mysql GREATER THAN operator **/
		const GREATER_THAN = '>';
		
		/** The mysql LESS THAN operator **/
		const LESS_THAN = '<';
		
		/** The mysql GREATER THAN EQUAL operator **/
		const GREATER_EQUAL = '>=';
		
		/** The mysql LESS THAN EQUAL operator **/
		const LESS_EQUAL = '<=';
		
		/** The mysql IS NULL operator **/
		const IS_NULL = 'IS NULL';
		
		/** The mysql IS NOT NULL operator **/
		const IS_NOT_NULL = 'IS NOT NULL';
		
		/** The mysql LIKE operator **/
		const LIKE = 'LIKE';
		
		/** The mysql NOT LIKE operator **/
		const NOT_LIKE = 'NOT LIKE';
		
		/** The mysql IN operator **/
		const IN = 'IN';
		
		/** The mysql NOT IN operator **/
		const NOT_IN = 'NOT IN';
		
		/** The mysql LEFT JOIN operator **/
		const LEFT_JOIN = 'LEFT JOIN';
		
		/** The mysql RIGHT JOIN operator **/
		const RIGHT_JOIN = 'RIGHT JOIN';
		
		/** The mysql INNER JOIN operator **/
		const INNER_JOIN = 'INNER JOIN';
		
		/** The mysql CROSS JOIN operator **/
		const CROSS_JOIN = 'CROSS JOIN';
		
		/** The mysql JOIN operator **/
		const JOIN = 'JOIN';
		
		/** The mysql NATURAL JOIN operator **/
		const NATURAL_JOIN = 'NATURAL_JOIN';
		
		/** The mysql STRAIGHT JOIN operator **/
		const STRAIGHT_JOIN = 'STRAIGHT_JOIN';
		
		/** The mysql NATURAL LEFT JOIN operator **/
		const NATURAL_LEFT_JOIN = 'NATURAL LEFT JOIN';
		
		/** The mysql NATURAL RIGHT JOIN operator **/
		const NATURAL_RIGHT_JOIN = 'NATURAL RIGHT JOIN';
		
		/** The mysql LEFT OUTER JOIN operator **/
		const LEFT_OUTER_JOIN = 'LEFT OUTER JOIN';
		
		/** The mysql RIGHT OUTER JOIN operator **/
		const RIGHT_OUTER_JOIN = 'RIGHT OUTER JOIN';
		
		/** The mysql NATURAL LEFT OUTER JOIN operator **/
		const NATURAL_LEFT_OUTER_JOIN = 'NATURAL LEFT OUTER JOIN';
		
		/** The mysql NATURAL RIGHT OUTER JOIN operator **/
		const NATURAL_RIGHT_OUTER_JOIN = 'NATURAL RIGHT OUTER JOIN';
		
		/** The mysql REGEXP operator **/
		const REGEXP = 'REGEXP';
		
		/** The mysql NOT REGEXP operator **/
		const NOT_REGEXP = 'NOT REGEXP';
		
		/** The mysql ascending direction for GROUP BY or ORDER BY **/
		const ASC = 'ASC';
		
		/** The mysql descending direction for GROUP BY or ORDER BY **/
		const DESC = 'DESC';
		
		/** The mysql ALL word **/
		const ALL = 'ALL';
		
		/** The mysql DISTINCT word **/
		const DISTINCT = 'DISTINCT';
		
		/** Void or empty. Used as a default value. **/
		const VOID = '';
		
		private 
			
		$_CRITERIA = NULL,
		
		$_FIELDS   = NULL,
		
		$_TABLES   = NULL,
		
		$_ORDERBY  = NULL,
		
		$_GROUPBY  = NULL,
		
		$_LIMIT    = NULL;
		
		/**
		 * The columns in the GROUP BY clause.
		 * 
		 * @var array
		 */
		private $mGroupBy = array();
		
		/**
		 * The having conditions in the sql statement.
		 * 
		 * @var array
		 */
		private $mHavingConditions = array();
		
		/**
		 * The sql limit representation.
		 * 
		 * @var string
		 */
		private $mLimit = '';
		
		/**
		 * The columns in the ORDER BY clause.
		 * 
		 * @var array
		 */
		private $mOrderBy = array();
		
		/**
		 * The columns in the sql statement.
		 * 
		 * @var array
		 */
		private $mSelectItems = array();
		
		/**
		 * The tables in the sql statement.
		 * 
		 * @var array
		 */
		private $mSelectTables = array();
		
		/**
		 * The where conditions in the sql statement.
		 * 
		 * @var array
		 */
		private $mWhereConditions = array();
		
		/**
		 * The Criteria2 objects union to this criteria.
		 * 
		 * @var array
		 */
		private $mUnions = array();
		
		/**
		 * Adds a condition to the WHERE clause.
		 * 
		 * @param string $left_expression  The left expression to a sql where operation.
		 * @param string $right_expression The right expression to a sql where operation. 
		 * 																 For REGEXP or NOT_REGEXP this is the pattern to be matched.
		 * 																 For IS_NULLED or IS_NOT_NULL this field is not used but this must be set by any value.
		 * @param string $operator				 The operator to a sql where operation.
		 * 
		 * @return void
		 */
		function add($left_expression, $right_expression, $operator = self::EQUAL){
			if ($condition = $this->createCondition($left_expression, $right_expression, $operator)) {
				$condition = (0 < count($this->mWhereConditions)) ? ' AND '.$condition : $condition;
				$this->mWhereConditions[] = $condition;
			}
		}
		
		/**
		 * Adds a condition to the WHERE clause using the AND operator.
		 * 
		 * @param string $left_expression  The left expression to a sql where operation.
		 * @param string $right_expression The right expression to a sql where operation. 
		 * 																 For REGEXP or NOT_REGEXP this is the pattern to be matched.
		 * 																 For IS_NULLED or IS_NOT_NULL this field is not used but this must be set by any value.
		 * @param string $operator				 The operator to a sql where operation.
		 * 
		 * @return void
		 */
		function addAnd($left_expression, $right_expression, $operator = self::EQUAL){
			if ($condition = $this->createCondition($left_expression, $right_expression, $operator)) {
				$condition = (0 < count($this->mWhereConditions)) ? ' AND '.$condition : $condition;
				$this->mWhereConditions[] = $condition;
			}
		}
		
		/**
		 * Adds a condition to the WHERE clause using the OR operator.
		 * 
		 * @param string $left_expression  The left expression to a sql where operation.
		 * @param string $right_expression The right expression to a sql where operation. 
		 * 																 For REGEXP or NOT_REGEXP this is the pattern to be matched.
		 * 																 For IS_NULLED or IS_NOT_NULL this field is not used but this must be set by any value.
		 * @param string $operator				 The operator to a sql where operation.
		 * 
		 * @return void
		 */
		function addOr($left_expression, $right_expression, $operator = self::EQUAL){
			if ($condition = $this->createCondition($left_expression, $right_expression, $operator)) {
				$condition = (0 < count($this->mWhereConditions)) ? ' OR '.$condition : $condition;
				$this->mWhereConditions[] = $condition;
			}
		}	
		
		/**
		 * Adds a column in the GROUP BY clause.
		 * 
		 * @param string  $column        The name of the column to be added in the GROUP BY clause.
		 * @param string  $sorting_order The sorting order for the GROUP BY.
		 * @param boolean $with_roll_up  Flag that states whether to add WITH ROLLUP to GROUP BY clause or not.
		 * 
		 * @return void
		 */
		function addGroupByColumn($column, $sorting_order = self::VOID, $with_roll_up = false){
			$column .= (self::VOID == $sorting_order) ? '' : ' '.$sorting_order;
			$column .= ($with_roll_up) ? ' WITH ROLLUP' : '';
			$this->mGroupBy[] = $column;
		}
		
		/**
		 * Adds a condition to the HAVING clause.
		 * 
		 * @param string $left_expression  The left expression to a sql where operation.
		 * @param string $right_expression The right expression to a sql where operation. 
		 * 																 For REGEXP or NOT_REGEXP this is the pattern to be matched.
		 * 																 For IS_NULLED or IS_NOT_NULL this field is not used but this must be set by any value.
		 * @param string $operator				 The operator to a sql where operation.
		 * 
		 * @return void
		 */
		function addHavingCondition($left_expression, $right_expression, $operator = self::EQUAL){
			if ($condition = $this->createCondition($left_expression, $right_expression, $operator)) {
				$condition = (0 < count($this->mHavingConditions)) ? ' AND '.$condition : $condition;
				$this->mHavingConditions[] = $condition;
			}
		}
		
		/**
		 * Adds a condition to the HAVING clause using the AND operator
		 * 
		 * @param string $left_expression  The left expression to a sql where operation.
		 * @param string $right_expression The right expression to a sql where operation. 
		 * 																 For REGEXP or NOT_REGEXP this is the pattern to be matched.
		 * 																 For IS_NULLED or IS_NOT_NULL this field is not used but this must be set by any value.
		 * @param string $operator				 The operator to a sql where operation.
		 * 
		 * @return void
		 */
		function addHavingConditionAnd($left_expression, $right_expression, $operator = self::EQUAL){
			if ($condition = $this->createCondition($left_expression, $right_expression, $operator)) {
				$condition = (0 < count($this->mHavingConditions)) ? ' AND '.$condition : $condition;
				$this->mHavingConditions[] = $condition;
			}
		}
		
		/**
		 * Adds a condition to the HAVING clause using the OR operator
		 * 
		 * @param string $left_expression  The left expression to a sql where operation.
		 * @param string $right_expression The right expression to a sql where operation. 
		 * 																 For REGEXP or NOT_REGEXP this is the pattern to be matched.
		 * 																 For IS_NULLED or IS_NOT_NULL this field is not used but this must be set by any value.
		 * @param string $operator				 The operator to a sql where operation.
		 * 
		 * @return void
		 */
		function addHavingConditionOr($left_expression, $right_expression, $operator = self::EQUAL){
			if ($condition = $this->createCondition($left_expression, $right_expression, $operator)) {
				$condition = (0 < count($this->mHavingConditions)) ? ' OR '.$condition : $condition;
				$this->mHavingConditions[] = $condition;
			}
		}
		
		/**
		 * Adds a column in the ORDER BY clause.
		 * 
		 * @param string $column        The name of the column to be added an ORDER BY clause.
		 * @param string $sorting_order The sorting order for the ORDER BY.
		 * 
		 * @return void
		 */
		function addOrderByColumn($column, $sorting_order = self::VOID){
			$column .= (self::VOID == $sorting_order) ? '' : ' '.$sorting_order;
			$this->mOrderBy[] = $column;
		}
		
		/**
		 * Adds a LIMIT to the sql statement.
		 * 
		 * @param integer $rows The number of rows in the LIMIT clause.
		 * @param integer $offset The offset of the data to be retrieved.
		 * 
		 * @return void
		 */
		function addLimit($rows, $offset = 0){
			$this->mLimit = ' LIMIT '.$offset.','.$rows;
		}
		
		/**
		 * Adds a column to the select statement.
		 * 
		 * @param string $column The column to be added to the select statement.
		 * @param string $alias  The alias of the column added.
		 * 
		 * @return void
		 */
		function addSelectColumn($column, $alias = null){
			if (!((!is_null($alias) AND isset($this->mSelectItems[$alias])) OR (is_null($alias) AND isset($this->mSelectItems[$column])))) {			
				$name = (is_null($alias)) ? $column : $alias;
				$column .= (is_null($alias)) ? '' : ' AS '.$alias;
				$this->mSelectItems[$name] = $column;
			}
		}
		
		/**
		 * Adds a table to the select statement.
		 * 
		 * @param string 		$table 						The table to be added to the select statement.
		 * @param string 		$alias 						The alias of the table added.
		 * @param IndexHint $index_hint_list  The index hint list for the table.
		 * 
		 * @return void 
		 */
		function addSelectTable($table, $alias = null, array $index_hint_list = null){
			if (!((!is_null($alias) AND isset($this->mSelectTables[$alias])) OR (is_null($alias) AND isset($this->mSelectTables[$table])))) {
				$name = (is_null($alias)) ? $table : $alias;
				$table .= (is_null($alias)) ? '' : ' AS '.$alias;
				$table .= (is_array($index_hint_list)) ? ' '.implode($index_hint_list, ', ') : ''; 
				$this->mSelectTables[$name] = $table;
			}
		}
		
		/**
		 * Creates the correct condition from the given parameters.
		 * 
		 * @param string $left_expression  The left expression to a sql where operation.
		 * @param string $right_expression The right expression to a sql where operation. 
		 * 																 For REGEXP or NOT_REGEXP this is the pattern to be matched.
		 * 																 For IS_NULLED or IS_NOT_NULL this field is not used but this must be set by any value.
		 * @param string $operator				 The operator to a sql where operation.
		 * 
		 * @return string|boolean The correct condition from the given parameters; If there are problems with the given parameters, this returns false;
		 */
		private function createCondition($left_expression, $right_expression, $operator){
			switch ($operator) {
				case self::EQUAL:
				case self::NOT_EQUAL:
				case self::GREATER_THAN:
				case self::LESS_THAN:
				case self::GREATER_EQUAL:
				case self::LESS_EQUAL:
				case self::LIKE:
				case self::NOT_LIKE:
				case self::REGEXP:
				case self::NOT_REGEXP:
					$condition  = $left_expression.' '.$operator.' '.$right_expression.'';
				break;
				case self::IS_NULL:
				case self::IS_NOT_NULL:
					$condition = ''.$left_expression.' '.$operator;
				break;
				case self::IN:
				case self::NOT_IN:
					$condition = $left_expression.' '.$operator.' ('.$right_expression.')'; 
				break;
				default:
					return false;
			}
			
			return $condition;			
		}
		
		/**
		 * Returns the GROUP BY clause created from the given columns added to GROUP BY.
		 * 
		 * @return string the GROUP BY clause created from the given columns added to GROUP BY.
		 */
		function createGroupByClause(){
			return (0 < count($this->mGroupBy)) ? ' GROUP BY '.implode($this->mGroupBy, ', ') : '';
		}
		
		/**
		 * Returns the HAVING clause created from the given having-conditions.
		 * 
		 * @return string the HAVING clause created from the given having-conditions.
		 */
		function createHavingClause(){
			return (0 < count($this->mHavingConditions)) ? ' HAVING '.implode($this->mHavingConditions, ' ') : '';
		}
		
		/**
		 * Returns the ORDER BY clause created from the given columns added to ORDER BY.
		 * 
		 * @return string the ORDER BY clause created from the given columns added to ORDER BY.
		 */
		function createOrderByClause(){
			return (0 < count($this->mOrderBy)) ? ' ORDER BY '.implode($this->mOrderBy, ', ') : '';
		}
		
		/**
		 * Returns the created LIMIT clause from the given limit.
		 * 
		 * @return string the created LIMIT clause from the given limit.
		 */
		function createLimitClause(){
			return $this->mLimit;
		}
		
		/**
		 * Returns the full select statement.
		 * 
		 * @return string the full select statement.
		 */
		function createSelectStatement(){
			$sql = 'SELECT ';
			$sql .= (0 < count($this->mSelectItems)) ? implode($this->mSelectItems, ', ') : '*';
			$sql .= ' FROM '.implode($this->mSelectTables, ', ');
			$sql .= $this->createWhereClause();
			$sql .= $this->createGroupByClause();
			$sql .= $this->createHavingClause();
			$sql .= $this->createOrderByClause();
			$sql .= $this->createLimitClause();
			
			foreach ($this->mUnions as $union) {
				$sql .= ' '.$union['operator'].' '.$union['criteria'];
			}
			
			return $sql;
		}
		
		/**
		 * Returns the WHERE clause created from the given where-conditions.
		 * 
		 * @return string The WHERE clause created from the given where-conditions.
		 * 								If there are no where-conditions set, this defaults to empty string.
		 */
		function createWhereClause(){
			return (0 < count($this->mWhereConditions)) ? ' WHERE '.implode($this->mWhereConditions, ' ') : '';
		}
		
		/**
		 * Union the given criteria to this criteria.
		 * 
		 * @param Criteria2 $criteria       The criteria to be union to this criteria.
		 * @param string   $union_operator The operator for the union. Poosible values Criteria2::DISTINCT, Criteria2::ALL, leave it blank.
		 * 
		 * @return void
		 */
		function union($criteria, $union_operator = self::VOID){
			$union = array();
			$union['criteria'] = $criteria;
			$union['operator'] = $union_operator;
			
			$this->mUnions[] = $union;
		}
		
		/**
		 * Enables __toString magic function.
		 */
		function __toString(){
			return $this->createSelectStatement();
		}
	
	}
	
