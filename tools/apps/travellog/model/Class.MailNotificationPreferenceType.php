<?php
/*
 * Created on 11 23, 06
 * @author Kerwin Gordo
 * Purpose: stores types of mail notifications to entries in the travelers address book 
 */
 
 class MailNotificationPreferenceType {
 	public static $NOTIFY_ON_NEW_JOURNALENTRY = 1;
	public static $NOTIFY_ON_EDIT_JOURNALENTRY = 2;
	public static $NOTIFY_ON_NEW_PHOTO = 3;
 	
 }
 
 
?>
