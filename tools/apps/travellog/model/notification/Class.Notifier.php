<?php
	/**
	 * Author: Reynaldo Castellano III
	 * DateCreated: November 23, 2006
	 * Description: A class that would automatically send notifications to a subject travelers' friends
	 */				
	require_once("travellog/model/Class.Traveler.php");
	require_once("Class.Decorator.php");	
	//A decorator that will act as a facade
	require_once('travellog/model/notification/Class.TravelerNotifier.php');	
	require_once('travellog/model/notification/Class.GroupNotifier.php');
	class Notifier extends Decorator
	{
		const TRAVEL_JOURNAL_ENTRY_ADD		= 'TravelJournalEntryAdd';
		const TRAVEL_JOURNAL_ENTRY_UPDATE 	= 'TravelJournalEntryUpdate';
		const PHOTO_UPLOAD					= 'PhotoUpload';
		const CONFIRMATION					= 'Confirmation';
		
		const TRAVELER_CONTEXT				= 1;
		const GROUP_CONTEXT					= 2;

		private $mIncludeGroupMembers 				= false;
		private $mIncludeGroupMembersAddressBook 	= false;
		private $mIncludeGroupAddressBook 			= false;
		private $mNotificationTag					= null;
		private $mIsAdmin							= false;
		private $mFormat							= null;
		
		function __construct($subjectTravelerID=null){
			$subjectTraveler = new Traveler($subjectTravelerID);
			$core =($subjectTraveler->isAdvisor())?new GroupNotifier():new TravelerNotifier();
			$this->mIsAdmin = $subjectTraveler->isAdvisor();
			$core->setSubjectTravelerID($subjectTravelerID);
			parent::__construct($core);
		}
		
		function setNotificationTag($notificationTag=null){
			$this->mNotificationTag = $notificationTag;
			switch($notificationTag){
				case self::TRAVEL_JOURNAL_ENTRY_ADD:					
					break;
				case self::TRAVEL_JOURNAL_ENTRY_UPDATE:
					break;					
				case self::PHOTO_UPLOAD:
					break;
				case self::CONFIRMATION:
					break;
				default:
					throw new exception("Invalid Notification Tag [$notificationTag] passed to method setNotificationTag of object of type Notifier.");
			}
		}
		
		function includeGroupMembers($include=true){
			$this->mIncludeGroupMembers = $include;
		}
		
		function includeGroupMembersAddressbook($include=true){
			$this->mIncludeGroupMembersAddressBook = $include;
		}
		
		function includeGroupAddressBook($include=true){
			$this->mIncludeGroupAddressBook = $include;
		}
		
		function sendNotification(){
			$this->mCoreObject->setNotificationTag($this->mNotificationTag);
			if($this->mIsAdmin){
				$this->mCoreObject->includeGroupMembers($this->mIncludeGroupMembers);	
				$this->mCoreObject->includeGroupMembersAddressbook($this->mIncludeGroupMembersAddressBook);
				$this->mCoreObject->includeGroupAddressbook($this->mIncludeGroupAddressBook);
			}			
			$this->mCoreObject->sendNotification();
		}
	}
?>