<?php
	
	/**
	 * Author: Reynaldo Castellano III
	 * DateCreated: November 23, 2006
	 * Description: A class that would automatically send notifications to a subject travelers' friends
	 */	
	require_once("Class.dbHandler.php"); 
	require_once("Class.iRecordset.php");
	require_once("Class.Notification.php");	
	require_once("travellog/model/Class.AddressBook.php");
	require_once("travellog/model/Class.TravelerProfile.php");
	require_once("class.phpmailer.php");
	require_once("Class.ToolMan.php");
	require_once("travellog/model/Class.ConfirmationData.php");
	
	require_once("travellog/model/notification/format/Class.TravelJournalEntryAdd.php");
	require_once("travellog/model/notification/format/Class.TravelJournalEntryUpdate.php");
	require_once("travellog/model/notification/format/Class.PhotoUpload.php");
	require_once("travellog/model/notification/format/Class.Confirmation.php");
	
	abstract class BaseNotifier
	{
		protected $mNotification 		= null;
		protected $mSubjectTravelerID 	= 0;		
		protected $mVars 				= array();
		
		public function setNotificationTag($notificationTag){											
			$this->mNotification = new $notificationTag();
		}
		
		public function setSubjectTravelerID($subjectTravelerID){
			$this->mSubjectTravelerID = $subjectTravelerID;
		}
		
		public function getNotification(){
			return $this->mNotification;
		}
		
		public function getSubjectTravelerID(){
			return $this->mSubjectTravelerID;
		}
		
		public function setVar($varName,$varValue){
			$this->mVars[$varName] = $varValue;
		}
		
		abstract public function sendNotification();
	}
?>