<?php
	require_once("Class.dbHandler.php"); 
	require_once("Class.iRecordset.php");
	require_once("Class.ToolMan.php");
	class Notification
	{
		private static $totalRecords = 0;
		
		private $mNotificationID = 0;
		private $mNotificationTag = null;
		private $mSubject = null;
		private $mMessage = null;
		private $mIsHtml = 0;
		
		private $db = null;
		
		function __construct($notificationTag=null,$notificationID=0){
			$this->db = new dbHandler();
			if($notificationTag!=null || $notificationID!=0){
				$qNotification = $this->db->execQuery("SELECT notificationID, notificationTag, subject, message, isHtml FROM Travel_Logs.tblNotification WHERE notificationTag = '$notificationTag' OR notificationID = $notificationID");
				$rsNotification = new iRecordset($qNotification);
				if(!$rsNotification->retrieveRecordCount()){
					throw new exception("Invalid Notification Tag [$notificationTag] or Notification ID [$notificationID] passed to object of class Notification.");						
				}
		 		else{
		 			$this->mNotificationID = $rsNotification->getNotificationID(0);
		 			$this->mNotificationTag = $rsNotification->getNotificationTag(0);
		 			$this->mSubject = $rsNotification->getSubject(0);
		 			$this->mMessage = $rsNotification->getMessage(0);
		 			$this->mIsHtml = $rsNotification->getIsHtml(0);
		 		}
			}
		}
				
		public function setNotificationTag($notificationTag){
			$this->mNotificationTag = $notificationTag;
		}
		
		public function setSubject($subject){
			$this->mSubject = $subject;
		}
		
		public function setMessage($message){
			$this->mMessage = $message;
		}
		
		public function setIsHtml($isHtml=0){
			$this->mIsHtml = (1==$isHtml)?1:0;
		}
		
		public function getNotificationID(){
			return $this->mNotificationID;
		}
		
		public function getNotificationTag(){
			return $this->mNotificationTag;
		}
		
		public function getSubject(){
			return $this->mSubject;
		}
		
		public function getFormattedSubject($subjectTravelerID){
			require_once('travellog/model/Class.TravelerProfile.php');					
			$travelerProfile = new TravelerProfile($subjectTravelerID);
			$arTravelerInfo = array();
			$arTravelerInfo['[$_travelerID$]'] = $travelerProfile->getTravelerID();
			$arTravelerInfo['[$_encTravelerID$]'] = ConfirmationData::getEncryptedTravelerID($travelerProfile->getTravelerID());
			$arTravelerInfo['[$_birthDate$]'] = $travelerProfile->getBirthDay();
			$arTravelerInfo['[$_userName$]'] = $travelerProfile->getUserName();
			$arTravelerInfo['[$_firstName$]'] = $travelerProfile->getFirstName();
			$arTravelerInfo['[$_lastName$]'] = $travelerProfile->getLastName();
			$arTravelerInfo['[$_age$]'] = $travelerProfile->getFirstName();
			$arTravelerInfo['[$_gender$]'] = $travelerProfile->getGender();
			$msg = $this->mSubject;
			foreach($arTravelerInfo as $key => $val){
				$msg = str_replace($key,$val, $msg);
			}
			return $msg;
		}
		
		public function getMessage(){
			return $this->mMessage;
		}
		
		public function getFormattedMessage($subjectTravelerID){
			require_once('travellog/model/Class.TravelerProfile.php');					
			$travelerProfile = new TravelerProfile($subjectTravelerID);
			$arTravelerInfo = array();
			$arTravelerInfo['[$_travelerID$]'] = $travelerProfile->getTravelerID();
			$arTravelerInfo['[$_encTravelerID$]'] = ConfirmationData::getEncryptedTravelerID($travelerProfile->getTravelerID());
			$arTravelerInfo['[$_birthDate$]'] = $travelerProfile->getBirthDay();
			$arTravelerInfo['[$_userName$]'] = $travelerProfile->getUserName();
			$arTravelerInfo['[$_firstName$]'] = $travelerProfile->getFirstName();
			$arTravelerInfo['[$_lastName$]'] = $travelerProfile->getLastName();
			$arTravelerInfo['[$_age$]'] = $travelerProfile->getFirstName();
			$arTravelerInfo['[$_gender$]'] = $travelerProfile->getGender();
			$msg = $this->mMessage;
			foreach($arTravelerInfo as $key => $val){
				$msg = str_replace($key,$val, $msg);
			}
			return $msg;
		}
		
		public function getIsHtml(){
			return (1==$this->mIsHtml)?true:false;
		}
		
		public function save(){			
			//check if the nodtification subject already exist in the database.
			$qry = 	"SELECT notificationID " .
					"FROM Travel_Logs.tblNotification " .
					"WHERE notificationTag = ". ToolMan::makeSqlSafeString($this->mNotificationTag)." " .
					"AND notificationID != ".ToolMan::makeSqlSafeString($this->mNotificationID);
			$qNotification = $this->db->execQuery($qry);
			$rsNotification = new iRecordset($qNotification);
			if(0 == $rsNotification->retrieveRecordCount()){
				if($this->mNotificationID != 0){//if NotificationID != 0, this indicates that the entry should be updated.
					try{
						$qry = 	"UPDATE Travel_Logs.tblNotification " .
								"SET notificationTag = ".ToolMan::makeSqlSafeString($this->mNotificationTag).", " .
								"message = ".ToolMan::makeSqlSafeString($this->mMessage).", " .
								"Subject = ".ToolMan::makeSqlSafeString($this->mSubject).", " .
								"isHtml =".ToolMan::makeSqlSafeString($this->mIsHtml)." ".
							   	"WHERE notificationID = ".ToolMan::makeSqlSafeString($this->mNotificationID);
						$qNotification = $this->db->execQuery($qry);
						return true;
					}	
					catch(exception $e){
						throw new exception($e->getMessage());
					}				
				}
				else{//Add the new entry to database
					try{						
						$qry = 	"INSERT INTO Travel_Logs.tblNotification (notificationTag, message, subject, isHtml) " .
								"VALUES (".
									ToolMan::makeSqlSafeString($this->mNotificationTag).",".
									ToolMan::makeSqlSafeString($this->mMessage).",".									
									ToolMan::makeSqlSafeString($this->mSubject).",".
									ToolMan::makeSqlSafeString($this->mIsHtml).
								")";
						$qNotification = $this->db->execQuery($qry);
						return true;							   	
					}
					catch(exception $e){
						throw new exception($e->getMessage());
					}
				}
			}
			else{
				throw new exception("Error! Notification Tag already exists.");
			}
		}
		
		public function delete(){
			//delete the entry from the database
			try{
				$qNotification = $this->db->execQuery("DELETE FROM Travel_Logs.tblNotification WHERE notificationID = $this->mNotificationID");
				return true;
			}
			catch(exception $e){
				throw new exception($e->getMessage());
			}			
		}
		
		public static function getAllNotifications($offset=0,$end=10){
			$arNotification = array();
			$db = new dbHandler();
			$qNotification = $db->execQuery("SELECT SQL_CALC_FOUND_ROWS notificationTag FROM tblNotification ORDER BY notificationTag LIMIT $offset,$end");
			$rsNotification = new iRecordset($qNotification);
			$rsTotalRec = new iRecordset($db->execQuery("SELECT FOUND_ROWS() as totalRecords"));
			Notification::$totalRecords = $rsTotalRec->getTotalRecords();
			while(!$rsNotification->EOF()){
				$arNotification[] = new Notification($rsNotification->getNotificationTag());
				$rsNotification->moveNext();
			}
			return $arNotification;
		}
		
		public static function getAbsolutePosition($recordID){			
			$db = new dbHandler();
			$db->execQuery("SET @x := 0;");						
			$sql = "SELECT _table.rowNum as offset FROM (SELECT @x := @x+1 AS rowNum, notificationID AS _ID FROM tblNotification ORDER BY notificationTag ) AS _table WHERE _table._ID = '$recordID'";
            $rsAbs = new iRecordset($db->execQuery($sql));			
			return $rsAbs->getOffset();
		}
				
		public static function getTotalRecords(){
			return Notification::$totalRecords;
		}
	}
?>