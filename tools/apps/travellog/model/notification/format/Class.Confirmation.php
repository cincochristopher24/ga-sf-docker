<?php
	require_once("Class.BaseFormat.php");
	require_once("travellog/model/Class.ConfirmationData.php");
	class Confirmation
	{ 
		public function getSubject(){
			return 'Notification Confirmation';
		}
		
		public function getMessageBody($owner=null,$entry=null){			
			
			$profile = new TravelerProfile($owner->getTravelerID());
			$firstName = $entry->getFirstName();
			$lastName = $entry->getLastName();			
			$travelerID = $owner->getTravelerID();
			$ownerName = $profile->getFirstName();
			
			if (null == $profile->getGender()){
				$gender1 = 'his/her';
				$gender2 = 'him/her';
			}
			else{
				$gender1 = ('M' == $profile->getGender())?'his':'her';
				$gender2 = ('M' == $profile->getGender())?'him':'her';
			}
		
			require_once('travellog/model/Class.ConfirmationData.php');		
			$oID = ConfirmationData::getEncryptedTravelerID($travelerID);
			$eID = ConfirmationData::getEncryptedAddressBookEntryID($entry->getAddressBookEntryID());
			
			$travelerUserName = ucwords($ownerName);					
			$ar = str_split($travelerUserName);
			$userNameOwn = ('s' == strtolower($ar[count($ar)-1]))?$travelerUserName."'":$travelerUserName."'s";					
			$profileLink = "http://".$_SERVER['HTTP_HOST']."/profile.php?action=view&amp;travelerID=$travelerID";
			$addressBookconfr = "http://".$_SERVER['HTTP_HOST']."/confirmnotification.php?oID=$oID&amp;eID=$eID&amp;cnf=1";
			$addressBookdeny = "http://".$_SERVER['HTTP_HOST']."/confirmnotification.php?oID=$oID&amp;eID=$eID&amp;cnf=0";
						
			$msg = 	"Dear $firstName $lastName,\n" .
					"Greetings from the GoAbroad Network!\n" .
					"\n" .
					"You received this email because $travelerUserName added you to\n" .
					"$gender1 GoAbroad Network Address Book, and would like you\n" .
					"to receive updates about $gender1 activities on this site\n" .
					"\n" .
					"Goabroad Network is a social networking site dedicated to\n" .
					"travelers worldwide. $travelerUserName shares $gender1 travels on our\n" .
					"website by writing travel journals, uploading photos, and sending\n" .
					"messages to friends like you. To view $userNameOwn GoAbroad\n" .
					"Network profile, please click on the link below\n" .
					"\n" .
					"$profileLink\n" .
					"\n" .
					"We would then like to ask for your permission about receiving\n" .
					"updates regarding $userNameOwn activities on the GoAbroad\n" .
					"Network. If you click on the link below, you AGREE to be notified if\n" .
					"$travelerUserName wrote new travel journals, uploaded new photos or to\n" .
					"receive messages from $gender2.\n" .
					"\n" .
					"$addressBookconfr\n" .
					"\n" .
					"If you prefer NOT to be notified of such updates, please click on the link below\n" .
					"$addressBookdeny\n" .
					"\n" .
					"============================================================\n" .
					"\n" .
					"To create your own account on the GoAbroad Network, please visit\n" .
					"\n" .
					"http://www.goabroad.net\n" .
					"\n" .
					"============================================================\n" .
					"\n" .
					"Thank you and have a great day!\n" .
					"\n" .
					"\n" .
					"The GoAbroad Network Team&amp;quot;";	
			return $msg;
		}
	}
?>