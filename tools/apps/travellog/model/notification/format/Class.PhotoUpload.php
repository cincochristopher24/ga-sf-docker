<?php
	require_once("Class.BaseFormat.php");
	require_once("travellog/model/Class.ConfirmationData.php");
	class PhotoUpload
	{
		public function getSubject($subject=null){
			if($subject instanceof Traveler){
				$profile = new TravelerProfile($subject->getTravelerID());
				$travelerName = $profile->getFirstName();
				return "Your friend $travelerName has uploaded a new travel photo.";
			}
			elseif($subject instanceof AdminGroup){
				return $subject->getName()." has uploaded a new travel photo.";
			}
			else{
				throw new exception('Invalid subject in method getSubject in PhotoUpload.');
			}
		}
		//adi na ak dinhi ha get message body
		public function getMessageBody($subject=null,$link=null){
			if(null == $link){
				throw new exception('Invalid link passed to static function getMessageBody of class PhotoUpload');
			}
			if($subject instanceof Traveler){			
				$profile = new TravelerProfile($subject->getTravelerID());
				$travelerName = $profile->getFirstName();
				$msg = 	"Your friend $travelerName has uploaded new travel photos on\n" .
						"GoAbroad Network. To view these photos, click on\n" .
						"the link below:\n" .
						"\n" .
						"$link\n" .
						"\n" .
						"============================================================\n";
			}
			elseif($subject instanceof AdminGroup){
				$msg = 	$subject->getName()." has uploaded new travel photos on\n" .
						"GoAbroad Network. To view these photos, click on\n" .
						"the link below:\n" .
						"\n" .
						"$link\n" .
						"\n" .
						"============================================================\n";
			}
			else{
				throw new exception('Invalid subject in method getMessageBody in TravelJournalEntryUpdate.');
			}
			return $msg;
		}
		
		public function getOptOutMessage($addressbookEntry=null){			
			if(!$addressbookEntry instanceof AddressBookEntry){
				throw new exception('Invalid $addressbookEntry passed to static function getOptOutMessage of class TravelJournalEntryUpdate');
			}
			$name = BaseFormat::getSubjectName(new Traveler($addressbookEntry->getOwnerTravelerID()));
			$msg = 	"\n " .
					"You received this email because you chose to be updated\n" .
					"about $name's activities on GoAbroad Network.\n" .
					"If you no longer want to receive such updates, simply click\n" .
					"on the link below:\n" .
					"\n" .
					"http://".$_SERVER['HTTP_HOST']."/confirmnotification.php?oID=".ConfirmationData::getEncryptedTravelerID($addressbookEntry->getOwnerTravelerID())."&amp;eID=".ConfirmationData::getEncryptedAddressBookEntryID($addressbookEntry->getAddressBookEntryID())."&amp;cnf=0\n" .
					"\n" .
					"\n" .
					"============================================================\n";
			return $msg;
		}
	}
?>