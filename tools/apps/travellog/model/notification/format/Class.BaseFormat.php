<?php
	class baseFormat
	{
		public static function getSubjectName($subject){
			if ($subject->isAdvisor()){
				//get the name of the group administered group				
				require_once('travellog/model/Class.AdminGroup.php');				
				require_once('travellog/model/Class.GroupFactory.php');
				$factory =  GroupFactory::instance();
				$group   =  $factory->create( array(AdminGroup::getAdvisorGroupID($subject->getTravelerID())) );
				return $group[0]->getName();	
			}
			else{
				//get the name of the traveler
				require_once('travellog/model/Class.TravelerProfile.php');
				$profile = new TravelerProfile($subject->getTravelerID());
				if(strlen($profile->getFirstname())){
					return $profile->getFirstname();
				}
				else{
					return $subject->getUserName();
				}
			}
		}	
	}
?>