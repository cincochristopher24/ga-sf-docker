<?php
	/**
	 * Author: Reynaldo Castellano III
	 * DateCreated: November 23, 2006
	 * Description: A class that would automatically send notifications to a subject travelers' friends
	 */		
	require_once("travellog/model/notification/Class.BaseNotifier.php");
	
	class GroupNotifier extends BaseNotifier
	{
		private $mIncludeGroupMembers 				= false;
		private $mIncludeGroupMembersAddressbook 	= false;
		private $mIncludeGroupAddressBook	 		= false;
		
		private $mSubjectGroupID	= 0;
		
		public function includeGroupMembers($include=false){
			$this->mIncludeGroupMembers = $include;
		}
		
		public function includeGroupMembersAddressbook($include=false){
			$this->mIncludeGroupMembersAddressbook=$include;
		}
		
		public function includeGroupAddressbook($include=false){
			$this->mIncludeGroupAddressBook =$include;
		}
		
		public function sendNotification(){			
			if($this->mNotification == null){
				throw new exception("Notification Tag has not been set in object of class Notifier.");
			}
			elseif(!$this->mSubjectTravelerID){
				throw new exception("Subject Traveler ID has not been set in object of class Notifier.");
			}
			else{
				try{					
					//group members should not be offered the opt out link					
					//construct the addresses
					require_once("travellog/model/Class.AdminGroup.php");
					$subjectGroup 	= new AdminGroup($this->mSubjectGroupID);
					$recipients 	= array();
					if($this->mIncludeGroupAddressBook){						
						require_once('travellog/model/Class.TravelerProfile.php');
						$subjectTraveler 	= new Traveler($this->mSubjectTravelerID);
						$profile 			= new TravelerProfile($this->mSubjectTravelerID);
						$addressBook 		= new AddressBook($this->mSubjectTravelerID);
						$addressBookEntries = $addressBook->getNotableAddressBookEntries();
						foreach($addressBookEntries as $entries){
							if(array_key_exists( trim($entries->getEmailAddress()),$recipients)){
								$recipients[$entries->getEmailAddress()]['owner'] = $recipients[$entries->getEmailAddress()]['owner'].', '.$profile->getFirstName(); 	
							}
							else{
								$rec = array();
								$rec['owner']	= $profile->getFirstName();
								$rec['from'] 	= 'ADDRESSBOOK';
								$rec['address'] = trim($entries->getEmailAddress());
								$rec['addrEntry'] = $entries;							
								$recipients[trim($entries->getEmailAddress())] 	= $rec;
							}							
						}
					}
					
					/*
					if($this->mIncludeGroupMembersAddressbook){
						//get the addressbook entries of each of the groupmember and
						$groupMembers = $subjectGroup->getGroupMembers();
						foreach($groupMembers as $member){
							$addressBook 		= new AddressBook($member->getTravelerID());
							$profile 			= new TravelerProfile($member->getTravelerID());
							$addressBookEntries = $addressBook->getNotableAddressBookEntries();
							foreach($addressBookEntries as $entries){								
								if(array_key_exists( trim($entries->getEmailAddress()),$recipients)){
									$recipients[$entries->getEmailAddress()]['owner'] = $recipients[$entries->getEmailAddress()]['owner'].', '.$profile->getFirstName(); 	
								}
								else{
									$rec = array();
									$rec['owner']	= $profile->getFirstName();
									$rec['from'] 	= 'ADDRESSBOOK';
									$rec['address'] = trim($entries->getEmailAddress());
									$recipients[trim($entries->getEmailAddress())] 	= $rec;
								}
							}
						}
					}
					*/
					
					if($this->mIncludeGroupMembers){
						$groupMembers = $subjectGroup->getMembers();
						if(is_array($groupMembers)){
							foreach($groupMembers as $member){
								$profile = new TravelerProfile($member->getTravelerID());
								if(strlen($memberTraveler->getEmail())){							
									$rec = array();
									$rec['owner']	= $profile->getFirstName();
									$rec['from'] 	= 'GROUP';
									$rec['address'] = trim($memberTraveler->getEmail());
									$recipients[] 	= $rec;
								}
							}
						}
					}
					
					require_once('travellog/model/Class.AddressBookEntry.php');
					require_once('travellog/model/Class.TravelerProfile.php');

					$subjectTraveler 	= new Traveler($this->mSubjectTravelerID);
					$profile 			= new TravelerProfile($this->mSubjectTravelerID);
					$addressBook 		= new AddressBook($this->mSubjectTravelerID);
					$addressBookEntries = $addressBook->getNotableAddressBookEntries();
					//begin sending email notifications....
					
					$mail = new PHPMailer();
					$mail->IsSMTP();
					$mail->IsHTML(false);
					$mail->From = $profile->getEmail();
					$fullname = $profile->getFirstName().' '.$profile->getLastName();
					$mail->FromName = $fullname;
					
					foreach($recipients as $rec){
						//set the addressbook custom vars						
						$msgSubject =  	$this->mNotification->getSubject($subjectTraveler);
						$mail->Subject = $msgSubject;						
						
						switch(get_class($this->mNotification)){							
							case Notifier::TRAVEL_JOURNAL_ENTRY_ADD:
								$msgBody = $this->mNotification->getMessageBody($subjectTraveler,$this->mVars['travellog']);																
								if('ADDRESSBOOK'==$rec['from']){									
									$msgBody .= $this->mNotification->getOptOutMessage($rec['addrEntry']);
								}
								break;
							case Notifier::TRAVEL_JOURNAL_ENTRY_UPDATE:
								$msgBody = $this->mNotification->getMessageBody($subjectTraveler,$this->mVars['travellog']);																
								if('ADDRESSBOOK'==$rec['from']){
									$msgBody .= $this->mNotification->getOptOutMessage($rec['addrEntry']);
								}
								break;
							case Notifier::PHOTO_UPLOAD:
								$msgBody = '';
								break;
						}
						$mail->Body = $msgBody;
						$mail->AddAddress($rec['address']);
						if(!$mail->Send()){
							throw new exception('Unable to send email message.');
						}
						$mail->ClearAddresses();						
					}
				}
				catch(exception $e){
					throw new exception($e->getMessage());
				}
			}
		}
	}
?>