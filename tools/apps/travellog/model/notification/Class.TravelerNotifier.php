<?php
	/**
	 * Author: Reynaldo Castellano III
	 * DateCreated: November 23, 2006
	 * Description: A class that would automatically send notifications to a subject travelers' friends
	 */		
	require_once("travellog/model/notification/Class.BaseNotifier.php");	
	class TravelerNotifier extends BaseNotifier	
	{
		public function sendNotification(){			
			if($this->mNotification == null){
				throw new exception("Notification Tag has not been set in object of class Notifier.");
			}
			elseif(!$this->mSubjectTravelerID){
				throw new exception("Subject Traveler ID has not been set in object of class Notifier.");
			}
			else{
				try{
					require_once('travellog/model/Class.AddressBookEntry.php');
					require_once('travellog/model/Class.TravelerProfile.php');
					$subjectTraveler 	= new Traveler($this->mSubjectTravelerID);
					$profile 			= new TravelerProfile($this->mSubjectTravelerID);
					$addressBook 		= new AddressBook($this->mSubjectTravelerID);
					$addressBookEntries = $addressBook->getNotableAddressBookEntries();
					//begin sending email notifications....
					$mail = new PHPMailer();
					$mail->IsSMTP();
					$mail->IsHTML(false);
					/*
					$mail->From = $profile->getEmail();
					$fullname = $profile->getFirstName().' '.$profile->getLastName();
					$mail->FromName = $fullname;
					*/
					$mail->From = 'admin@goabroad.net';
					$mail->FromName = 'GoAbroad.net';
					
					foreach($addressBookEntries as $addrEntry){
						//set the addressbook custom vars						
						$msgSubject =  	$this->mNotification->getSubject($subjectTraveler);																						
						$mail->Subject = $msgSubject;						
						switch(get_class($this->mNotification)){
							case Notifier::CONFIRMATION:
								$msgBody = $this->mNotification->getMessageBody($subjectTraveler,$this->mVars['entry']);
								break;
							case Notifier::TRAVEL_JOURNAL_ENTRY_ADD:
								$msgBody = $this->mNotification->getMessageBody($subjectTraveler,$this->mVars['travellog']);																
								$msgBody .= $this->mNotification->getOptOutMessage($addrEntry);
								break;
							case Notifier::TRAVEL_JOURNAL_ENTRY_UPDATE:
								$msgBody = $this->mNotification->getMessageBody($subjectTraveler,$this->mVars['travellog']);																
								$msgBody .= $this->mNotification->getOptOutMessage($addrEntry);
								break;
							case Notifier::PHOTO_UPLOAD:
								$msgBody = $this->mNotification->getMessageBody($subjectTraveler,$this->mVars['link']);																
								$msgBody .= $this->mNotification->getOptOutMessage($addrEntry);
								break;
						}
						$mail->Body = $msgBody;						
						$curAddr 	= trim($addrEntry->getEmailAddress());
						$mail->AddAddress($curAddr);
						if(!$mail->Send()){
							throw new exception('Unable to send email message.');
						}
						$mail->ClearAddresses();						
					}
				}
				catch(exception $e){
					throw new exception($e->getMessage());
				}
			}
		}
	}
?>