<?php
	require_once('Class.InputControl.php');
	
	class CheckBoxControl extends InputControl
	{				
		function __construct($params){
			$this->mInputType = 1;
			$this->mAttributes['isChecked'] = 0;			
			$this->mAttributes['text'] = '';
			parent::__construct($params);
		}
	}
?>
