<?php
	/***
	@author: Reynaldo Castellano III
	@date: 2007-11-15 19:18:53
	***/

	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.GaString.php');

	class GaSurveyClient
	{
		private $mGaSurveyClientID;
		private $mApiKey;
		private $mApiSecret;
		private $mDb;

		public function __construct($param=null,$data=null){
			$this->mDb = new dbHandler('db=');
			$this->clearProperties();
			if(!is_null($param)){
				$sql =	'SELECT `gaSurveyClientID`,`apiKey`,`apiSecret` '.
						'FROM `GaSurvey`.`tblGaSurveyClient` '.
						'WHERE `gaSurveyClientID` = '.GaString::makeSqlSafe($param);
				$rs = new iRecordset($this->mDb->execute($sql));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid gaSurveyClientID '.$param.' passed to object of type GaSurveyClientDm.');
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setGaSurveyClientID(0);
			$this->setApiKey(null);
			$this->setApiSecret(null);
		}

		private function initProperties($data){
			$this->setGaSurveyClientID($data['gaSurveyClientID']);
			$this->setApiKey($data['apiKey']);
			$this->setApiSecret($data['apiSecret']);
		}

		//SETTERS
		private function setGaSurveyClientID($param=0){
			$this->mGaSurveyClientID=$param;
		}

		public function setApiKey($param=null){
			$this->mApiKey=$param;
		}

		public function setApiSecret($param=null){
			$this->mApiSecret=$param;
		}

		//GETTERS
		public function getGaSurveyClientID(){
			return $this->mGaSurveyClientID;
		}

		public function getApiKey(){
			return $this->mApiKey;
		}

		public function getApiSecret(){
			return $this->mApiSecret;
		}

		//CRUDE
		public function save(){
			if(!$this->getGaSurveyClientID()){
				$sql =	'INSERT INTO `GaSurvey`.`tblGaSurveyClient`('.
							'`apiKey`,'.
							'`apiSecret`'.
						')VALUES('.
							GaString::makeSqlSafe($this->getApiKey()).','.
							GaString::makeSqlSafe($this->getApiSecret()).
						')';
				$this->mDb->execute($sql);
				$this->setGaSurveyClientID($this->mDb->getLastInsertedID());
			}
			else{
				$sql =	'UPDATE `GaSurvey`.`tblGaSurveyClient` SET '.
							'`apiKey`= '.GaString::makeSqlSafe($this->getApiKey()).','.
							'`apiSecret`= '.GaString::makeSqlSafe($this->getApiSecret()).' '.
						'WHERE `gaSurveyClientID` = '.GaString::makeSqlSafe($this->getGaSurveyClientID());
				$this->mDb->execute($sql);
			}
		}

		public function delete(){
			$sql =	'DELETE FROM `GaSurvey`.`tblGaSurveyClient` '.
					'WHERE `gaSurveyClientID` = '.GaString::makeSqlSafe($this->getGaSurveyClientID());
			$this->mDb->execute($sql);
			$this->clearProperties();
		}
		
		public static function createByApiKeyAndApiSecret($apiKey=null,$apiSecret=null){
			$db = new dbHandler('db=');
			$sql =	'SELECT `gaSurveyClientID`,`apiKey`,`apiSecret` '.
					'FROM `GaSurvey`.`tblGaSurveyClient` '.
					'WHERE `apiKey` = '.GaString::makeSqlSafe($apiKey).' ' .
					'AND `apiSecret` = '.GaString::makeSqlSafe($apiSecret);
			$rs = new iRecordset($db->execute($sql));
			return $rs->retrieveRecordCount() > 0 ? new GaSurveyClient(null,$rs->retrieveRow(0)) : null;  
		}
	}
?>