<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.GaString.php');
	require_once('Class.SurveyFieldAnswer.php');
	
	class SurveyFormAnswer
	{
		private $mSurveyFormAnswerID;
		private $mSurveyFormID;
		private $mDateCreated;
		
		private $mDb;
		
		public function __construct($param=null,$data=null){
			$this->mDb = new dbHandler();
			$this->clear();
			if(!is_null($param)){
				$sql = 	'SELECT surveyFormAnswerID, surveyFormID, dateCreated ' .
						'FROM GaSurvey.tblSurveyFormAnswer ' .
						'WHERE surveyFormAnswerID = '.GaString::makeSqlSafe($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount() > 0){
					$this->initialize($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid SurveyFormAnswerID '.$param.' passed to object of type SurveyFormAnswer.');
				}
			}
			if(is_array($data)){
				$this->initialize($data);
			}
		}
		
		private function clear(){
			$this->setSurveyFormAnswerID(0);
			$this->setSurveyFormID(0);
			$this->setDateCreated(null);
		}

		private function initialize($data){
			$this->setSurveyFormAnswerID($data['surveyFormAnswerID']);
			$this->setSurveyFormID($data['surveyFormID']);
			$this->setDateCreated($data['dateCreated']);
		}
		
		/*** SETTERS ***/
		
		private function setSurveyFormAnswerID($param=null){
			$this->mSurveyFormAnswerID = $param;
		}
		
		public function setSurveyFormID($param=null){
			$this->mSurveyFormID = $param;
		}
		
		public function setDateCreated($param=null){
			$this->mDateCreated = $param;
		}
		
		/*** GETTERS ***/
		
		public function getSurveyFormAnswerID(){
			return $this->mSurveyFormAnswerID;
		}
		
		public function getSurveyFormID(){
			return $this->mSurveyFormID;
		}
		
		public function getDateCreated(){
			return $this->mDateCreated;
		}
		
		public function getSurveyFieldAnswers(){
			return SurveyFieldAnswer::getSurveyFieldAnswersBySurveyFormAnswerID($this->getSurveyFormAnswerID());
		}
		
		public function getSurveyFieldAnswerBySurveyFieldID($surveyFieldID=0){
			return SurveyFieldAnswer::getSurveyFieldAnswerBySurveyFormAnswerIDAndSurveyFieldID($this->getSurveyFormAnswerID(),$surveyFieldID);
		}
		
		/***
		 * CRUDE
		 */
		public function save(){
			if(!$this->getSurveyFormAnswerID()){//add
				$sql =	'INSERT INTO GaSurvey.tblSurveyFormAnswer(' .
							'`surveyFormID`,' .
							'`dateCreated`' .
						')VALUES(' .
							GaString::makeSqlSafe($this->getSurveyFormID()).',' .
							GaString::makeSqlSafe(date('Y-m-d G:i:s')).
						')';
				$this->mDb->execQuery($sql);
				$this->setSurveyFormAnswerID($this->mDb->getLastInsertedID());
			}
			else{//update
				$sql = 	'UPDATE GaSurvey.tblSurveyFormAnswer SET ' .
							'`surveyFormID`			= '.GaString::makeSqlSafe($this->getSurveyFormID()).',' .
						'WHERE `surveyFormAnswerID` = '.GaString::makeSqlSafe($this->getSurveyFormAnswerID());
				$this->mDb->execQuery($sql);
			}
		}
		
		public function delete(){
			$fldAnswers = $this->getSurveyFieldAnswers();
			foreach($fldAnswers as $iFldAnswer){
				$iFldAnswer->delete();
			}
			$sql = 	'DELETE FROM GaSurvey.tblSurveyFormAnswer ' .
					'WHERE `surveyFormAnswerID` = '.GaString::makeSqlSafe($this->getSurveyFormAnswerID());
			$this->mDb->execQuery($sql);
			$this->clear();
		}
		
		public static function getSurveyFormAnswersBySurveyFormID($surveyFormID=0){
			$db = new dbHandler('db=');
			$sql = 	'SELECT `surveyFormAnswerID`, `surveyFormID`, `dateCreated` ' .
					'FROM GaSurvey.tblSurveyFormAnswer ' .
					'WHERE surveyFormID = '.GaString::makeSqlSafe($surveyFormID);
			$rs = new iRecordset($db->execQuery($sql));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyFormAnswer(null,$rs->retrieveRow());
				} 
			}
			return $ar;
		}
		
		public static function getSurveyFormAnswerBySurveyFormAnswerID($surveyFormAnswerID=0){
			$db = new dbHandler('db=');
			$sql = 	'SELECT `surveyFormAnswerID`, `surveyFormID`, `dateCreated` ' .
					'FROM GaSurvey.tblSurveyFormAnswer ' .
					'WHERE surveyFormAnswerID = '.GaString::makeSqlSafe($surveyFormAnswerID);
			$rs = new iRecordset($db->execQuery($sql));
			return ($rs->retrieveRecordCount() > 0 ? new SurveyFormAnswer(null,$rs->retrieveRow(0)) : null);
		}
			
	}
?>