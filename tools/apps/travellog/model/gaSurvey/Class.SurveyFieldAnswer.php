<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.GaString.php');
	require_once('Class.SurveyFieldAnswerValue.php');
	
	class SurveyFieldAnswer
	{
		private $mSurveyFieldAnswerID;
		private $mSurveyFormAnswerID;
		private $mSurveyFieldID;
		
		private $mDb;
		
		public function __construct($param=null,$data=null){
			$this->clear();
			$this->mDb = new dbHandler('db=');
			if(!is_null($param)){
				$sql =	'SELECT `surveyFieldAnswerID`, `surveyFormAnswerID`, `surveyFieldID` ' .
						'FROM `GaSurvey`.`tblSuryveyFieldAnswer` ' .
						'WHERE `surveyFieldAnswerID` = '.GaString::makeSqlSafe($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				
				if($rs->retrieveRecordCount() > 0){
					$this->initialize($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid SurveyFieldAnswerID '.$param.' passed to object of type SurveyFieldAnswer.');
				}
			}
			if(is_array($data)){
				$this->initialize($data);
			}			
		}
		
		private function clear(){
			$this->setSurveyFieldAnswerID(0);
			$this->setSurveyFormAnswerID(0);
			$this->setSurveyFieldID(0);			
		}
		
		private function initialize($data){
			$this->setSurveyFieldAnswerID($data['surveyFieldAnswerID']);
			$this->setSurveyFormAnswerID($data['surveyFormAnswerID']);
			$this->setSurveyFieldID($data['surveyFieldID']);
		}
		
		/*** SETTERS ***/
		private function setSurveyFieldAnswerID($param=null){
			$this->mSurveyFieldAnswerID = $param;
		}
		
		public function setSurveyFormAnswerID($param=null){
			$this->mSurveyFormAnswerID = $param;
		}
		
		public function setSurveyFieldID($param=null){
			$this->mSurveyFieldID = $param;
		}
		
		/*** GETTERS ***/
		public function getSurveyFieldAnswerID(){
			return $this->mSurveyFieldAnswerID;
		}
		
		public function getSurveyFormAnswerID(){
			return $this->mSurveyFormAnswerID;
		}
		
		public function getSurveyFieldID(){
			return $this->mSurveyFieldID;
		}
		
		public function getAnswerValues(){
			return SurveyFieldAnswerValue::getAnswerValuesBySurveyFieldAnswerID($this->getSurveyFieldAnswerID());
		}
		
		/*** CRUDE ***/
		
		public function save(){
			if(!$this->getSurveyFieldAnswerID()){//add
				$sql =	'INSERT INTO `GaSurvey`.`tblSurveyFieldAnswer`(' .
							'`surveyFormAnswerID`,' .
							'`surveyFieldID`' .
						')VALUES(' .
							GaString::makeSqlSafe($this->getSurveyFormAnswerID()).',' .
							GaString::makeSqlSafe($this->getSurveyFieldID()).
						')';
				$this->mDb->execQuery($sql);
				$this->setSurveyFieldAnswerID($this->mDb->getLastInsertedID());
			}
			else{//update
				$sql = 	'UPDATE GaSurvey.tblSurveyFieldAnswer SET ' .
							'`surveyFormAnswerID` 		= '.GaString::makeSqlSafe($this->getSurveyFormAnswerID()).',' .
							'`surveyFieldID` 			= '.GaString::makeSqlSafe($this->getSurveyFieldID()).' ' .
						'WHERE `surveyFieldAnswerID`	= '.GaString::makeSqlSafe($this->getSurveyFieldAnswerID());
				$this->mDb->execQuery($sql);
			}
		}
		
		public function delete(){
			$this->deleteValues();
			$sql = 	'DELETE FROM GaSurvey.tblSurveyFieldAnswer ' .
					'WHERE `surveyFieldAnswerID` = '.GaString::makeSqlSafe($this->getSurveyFieldAnswerID());
			$this->mDb->execQuery($sql);
			//clear the values
			$this->clear();
		}
		
		public function deleteValues(){
			SurveyFieldAnswerValue::deleteAnswerValuesBySurveyFieldAnswerID($this->getSurveyFieldAnswerID());
		}
		
		public static function getSurveyFieldAnswersBySurveyFormAnswerID($param){
			$db = new dbHandler('db=');
			$sql =	'SELECT `surveyFieldAnswerID`, `surveyFormAnswerID`, `surveyFieldID` ' .
					'FROM `GaSurvey`.`tblSurveyFieldAnswer` ' .
					'WHERE `surveyFormAnswerID` = '.GaString::makeSqlSafe($param);
			$rs = new iRecordset($db->execQuery($sql));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyFieldAnswer(null,$rs->retrieveRow());
				}
			}
			return $ar;
		}
		
		public static function getSurveyFieldAnswerBySurveyFieldID($param){
			$db = new dbHandler('db=');
			$sql =	'SELECT `surveyFieldAnswerID`, `surveyFormAnswerID`, `surveyFieldID` ' .
					'FROM `GaSurvey`.`tblSurveyFieldAnswer` ' .
					'WHERE `surveyFieldID` = '.GaString::makeSqlSafe($param);
			$rs = new iRecordset($db->execQuery($sql));
			return $rs->retrieveRecordCount() > 0 ? new SurveyFieldAnswer(null,$rs->retrieveRow(0)) : null;
		}
		
		public static function getSurveyFieldAnswerBySurveyFormAnswerIDAndSurveyFieldID($surveyFormAnswerID=0,$surveyFieldID=0){
			$db = new dbHandler('db=');
			$sql =	'SELECT `surveyFieldAnswerID`, `surveyFormAnswerID`, `surveyFieldID` ' .
					'FROM `GaSurvey`.`tblSurveyFieldAnswer` ' .
					'WHERE `surveyFormAnswerID` = ' .GaString::makeSqlSafe($surveyFormAnswerID).' '.
					'AND `surveyFieldID` = '.GaString::makeSqlSafe($surveyFieldID);
			$rs = new iRecordset($db->execQuery($sql));
			return $rs->retrieveRecordCount() > 0 ? new SurveyFieldAnswer(null,$rs->retrieveRow(0)) : null;
		}
		
	}
?>