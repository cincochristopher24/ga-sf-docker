<?php
	/**
	 * Created on 03 July 2007  6:35 PM
	 * Class.ProfileQuestion.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once ('travellog/model/travelbio/Class.FBini.php');
	require_once ('travellog/model/travelbio/Class.Choices.php');

	class ProfileQuestion{
		private $db			= null;
		private $qID 		= 0;
		private $qType 		= 0;
		private $exists		= false;
		private $question 	= '';
		private $introduction		= '';
	
		public static $TEXT 			= 1;
		public static $RADIO 			= 2;
		public static $CHECKBOX 		= 3;
		public static $SELECTBOX 		= 4;
		public static $TEXTAREA 		= 5;
		public static $CHECK_TEXTBOX	= 6;

		function ProfileQuestion($_qID = 0){
			try{
				$this-> db = FBIni::createDBHandler();
			}
			catch(exception $e){
				throw $e;
			}
			
			$this-> qID = $_qID;
			if (0 < $this->qID){
				$rs = new iRecordset($this->db->execQuery("select * from tbProfileQuestions where profileQuestionID = " . ToolMan::makeSqlSafeString($this->qID)));				

				if (0 < $rs->retrieveRecordCount()){
					$this-> exists = true;
					$this-> setQuestion ($rs->getQuestion(0));
					$this-> setQuestionType ($rs->getQuestionType(0));
					$this-> setIntroduction ($rs->getIntroduction(0));
				}
				else
					throw new exception('Invalid questionID '.$_qID.' passed to object of type ProfileQuestion.');
			}
		}

		function addChoices(){
			return $Choice = new Choices();
		}

		// Setters
		function setQuestion($_question = ''){
			$this-> question = $_question;
		}

		function setQuestionType($_qType = 0){
			$this-> qType = $_qType;
		}
		
		function setIntroduction($introduction = ''){
			$this->introduction = $introduction;
		}

		// Getters
		function getQuestionID(){
			return $this-> qID;
		}

		function getQuestion(){
			return $this-> question;
		}

		function getQuestionType(){
			return $this-> qType;
		}

		function getChoices(){
			return Choices::getChoices($this->qID);
		}

		function getIntroduction(){
			return $this->introduction;
		}

		static function getQuestions(){
			try{
				$db = FBIni::createDBHandler();
			}
			catch(exception $e){
				throw $e;
			}
			
			$rs = new iRecordset($db->execQuery("select distinct profileQuestionID from tbProfileQuestions order by profilequestionID asc"));

			$arrQ = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$arrQ[] = new ProfileQuestion($rs->getProfileQuestionID());
				}
			}
			return $arrQ;
		}

		// Boolean
		function isExist(){
			return $this-> exists;
		}

		// CRUD
		function Save(){
			if (!$this->isExist()){
				$sql = "insert into tbProfileQuestions (questionType, question, introduction) " .
						"values (" . ToolMan::makeSqlSafeString($this->qType) . ", " . ToolMan::makeSqlSafeString($this-> question) . ", ". ToolMan::makeSqlSafeString($this->introduction) .")";
				$this->db->execQuery($sql);
			}
			else
				$this-> Update();
		}

		function Update(){
			if ($this->isExist()){
				$sql = "update tbProfileQuestions set " .
							"questionType = " . ToolMan::makeSqlSafeString($this->qType) . ", " .
							"question = " . ToolMan::makeSqlSafeString($this->question) . ", ".
							"introduction = ". ToolMan::makeSqlSafeString($this->introduction) .
					" where profileQuestionID = " . ToolMan::makeSqlSafeString($this-> qID);
				$this->db->execQuery($sql);
			}
		}

		function Delete(){
			if ($this->isExist()){
				// delete question
				$this->db->execQuery("delete from tbProfileQuestions where profileQuestionID = " . ToolMan::makeSqlSafeString($this-> qID));
				// delete choices
				$this->db->execQuery("delete from tbChoice where profileQuestionID = " . ToolMan::makeSqlSafeString($this->qID));
			}
		}		

		// Added BY: Cheryl Ivy Q. Go  20 February 2008
		public static function getRandomQuestionByUser($_userId = 0, $_qId = 0){
			$db = FBIni::createDBHandler();

			$sql = "SELECT a.profileQuestionID " .
					"FROM tblTravelBioUserAnswer AS a, tblTravelBioUser AS b " .
					"WHERE b.travelerID = " . $_userId . " " .
					"AND a.profileQuestionID <> " . $_qId . " " .
					"AND a.travelBioUserID = b.travelBioUserID " .
					"ORDER BY RAND() LIMIT 1";
			$rs = new iRecordset($db->execQuery($sql));

			if (0 < $rs->retrieveRecordCount())
				return new ProfileQuestion($rs->getProfileQuestionID());
			else
				return NULL;
		}
		
		// Added BY: Cheryl Ivy Q. Go  25 February 2008
		public static function getNumberofQuestionsWithAnswer($_userId = 0){
			$db = FBIni::createDBHandler();

			$sql = "SELECT a.profileQuestionID " .
					"FROM tblTravelBioUserAnswer AS a, tblTravelBioUser AS b " .
					"WHERE b.travelerID = " . $_userId . " " .
					"AND a.travelBioUserID = b.travelBioUserID";
			$rs = new iRecordset($db->execQuery($sql));

			return $rs->retrieveRecordCount();
		}
	}
?>