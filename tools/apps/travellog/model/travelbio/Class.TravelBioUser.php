<?php
	require_once('travellog/model/travelbio/Class.TravelBioUser.php');
	require_once('travellog/model/travelbio/Class.Answer.php');
	require_once('travellog/model/travelbio/Class.FBini.php');

	class TravelBioUser{
		private $db						= null;
		private $fbApi					= null;
		private $mTravelBioUserID		= 0;
		private $mFbUserID				= 0;
		private $mTravelerID			= 0;
		private $mNumQuestionsDisplayed	= 5;
		private $mDateAdded				= null;
		
		const FB_USER	= 0;
		const GA_USER 	= 1;
		const BOTH 		= 2;

		function __construct($param = null){
			$this->db = FBIni::createDBHandler();
			if (is_numeric($param)){				
				$qry = 	'SELECT travelBioUserID, fbUserID, travelerID, dateAdded, numQuestionsDisplayed ' .
						'FROM tblTravelBioUser ' .
						'WHERE travelBioUserID ='.ToolMan::makeSqlSafeString($param);
				$rs = new iRecordset($this->db->execQuery($qry));
				if (0 < $rs->retrieveRecordCount()){
					$this->setTravelBioUserID( $rs->getTravelBioUserID(0) );
					$this->setFbUserID( $rs->getFbUserID(0) );
					$this->setTravelerID( $rs->getTravelerID(0) );
					$this->setDateAdded( $rs->getDateAdded(0) );
					$this->setNumOfQuestionsDisplayed( $rs->getNumQuestionsDisplayed(0) );
				}
				else{
					throw new exception('Invalid travelerBioUserID '.$param.' passed to object of type TravelBioUser.');
				}
			}
		}
		
		// Setters
		private function setTravelBioUserID($param=0){
			$this->mTravelBioUserID = $param;		
		}
		
		public function setFbUserID($facebookID = 0){
			$this->mFbUserID = $facebookID;
		}
		
		public function setTravelerID($_travID = 0){
			$this->mTravelerID = $_travID;
		}

		public function setDateAdded($_date = ''){
			$this->mDateAdded = $_date;
		}
		
		public function setNumOfQuestionsDisplayed($_numQ = 0){
			$this->mNumQuestionsDisplayed = $_numQ;
		}
		
		// Getters		
		public function getTravelBioUserID(){
			return $this->mTravelBioUserID;
		}

		public function getFbUserID(){
			return $this->mFbUserID;
		}
		
		public function getTravelerID(){
			return $this->mTravelerID;
		}

		public function getDateAdded(){
			return $this->mDateAdded;
		}
		
		public function getNumOfQuestionsDisplayed(){
			return $this->mNumQuestionsDisplayed;
		}
		
		public function getUserType(){
			if( 0 != $this->getFbUserID() && 0 != $this->getTravelerID() ){
				return self::BOTH;
			}
			elseif(0 != $this->getFbUserID() && 0 == $this->getTravelerID()){
				return self::FB_USER;
			}
			else{
				return self::GA_USER;
			}
		}
		
		public function getAnswers(){
			return Answer::getAnswersByUser($this->mTravelBioUserID);
		}
		
		public function getAnswerByQuestion($_questionID = 0){
			return Answer::getAnswerByQuestion($this->mTravelBioUserID, $_questionID);
		}
		
		public function clearAnswers(){
			Answer::clearUserAnswers($this->mTravelBioUserID);
		}
		
		// CRUD
		public function save(){
			if( 0== $this->getFbUserID() && 0 == $this->getTravelerID() ) {
				throw new exception('Error! FBuserID and TravelerID are both zero. At least one must not be zero.');
			}
			//check for duplicate ids
			//check if there is a record that has an fbUserID equal to this one but with different travelBioUserID
			if($this->isFbUser()){
				$fbUser = self::getTravelBioUserByFacebookID($this->getFbUserID());
				if(!is_null($fbUser)){
					if($fbUser->getTravelBioUserID() != $this->getTravelBioUserID()){
						throw new exception('Error! A TravelBioUser with an fbUserID of '. $this->getFbUserID() .' already exists.');
					}
				}
			}
			if($this->isGaUser()){
				$gaUser = self::getTravelBioUserByTravelerID($this->getTravelerID());
				if(!is_null($gaUser)){
					if($gaUser->getTravelBioUserID() != $this->getTravelBioUserID()){
						throw new exception('Error! A TravelBioUser with a travelerID of '.$this->getTravelerID(). ' already exists.');
					}
				}
			}
			if (0 == $this->getTravelBioUserID()){								
				$dateAdded = (!is_null($this->getDateAdded()) ? $this->getDateAdded() :  date("Y-m-d G:i:s") );
				$sql = 	'INSERT INTO tblTravelBioUser (' .
							'fbUserID, ' .
							'travelerID, ' .
							'dateAdded, ' .
							'numQuestionsDisplayed ' .
						') VALUES ('.
							ToolMan::makeSqlSafeString( $this->getFbUserID() ) .',' .
							ToolMan::makeSqlSafeString( $this->getTravelerID() ) .', ' .							
							ToolMan::makeSqlSafeString( $dateAdded ) .', ' .
							ToolMan::makeSqlSafeString( $this->getNumOfQuestionsDisplayed() ) .	
						')';
				$this->db->execQuery($sql);
				$this->setTravelBioUserID($this->db->getLastInsertedID());
			}
			else{				
				$qry = 	'UPDATE tblTravelBioUser ' .
						'SET ' .
							'fbUserID 					= ' . ToolMan::makeSqlSafeString($this->getFbUserID()).', ' .
							'travelerID 				= ' . ToolMan::makeSqlSafeString($this->getTravelerID()).', ' .
							'numQuestionsDisplayed 		= ' . ToolMan::makeSqlSafeString($this->getNumOfQuestionsDisplayed()).' '.
						'WHERE travelBioUserID 			= ' . ToolMan::makeSqlSafeString($this->getTravelBioUserID());
				$qry = $this->db->execQuery($qry);
			}
		}

		public function delete(){
			$this->db->execQuery('DELETE FROM tblTravelBioUser WHERE travelBioUserID = ' . ToolMan::makeSqlSafeString($this->getTravelBioUserID()));
			//clear the answers of this TravelBioUser
			Answer::clearUserAnswers($this->getTravelBioUserID());
		}
		
		public function isFbUser(){
			return $this->getFbUserID() != 0; 
		}
		
		public function isGaUser(){
			return $this->getTravelerID() != 0;
		}
		
		public function isSynchronized(){
			return $this->isFbUser() && $this->isGaUser(); 
		}
		
		public function isFbUserOnly(){
			return $this->isFbUser() && !$this->isSynchronized();
		}
		
		public function isGaUserOnly(){
			return $this->isGaUser() && !$this->isSynchronized();
		}
		
		/****
		 * @param indentity: Flag that would determine what type travelbiouser will this instance be after the unsychronization process.
		 */
		public function unsynchronize($identity=self::FB_USER){
			if($this->isSynchronized()){				
				if(self::FB_USER==$identity){
					$travelerID = $this->getTravelerID();
					$this->setTravelerID(0);
					$this->save();
					$dupUser = $this->createDuplicate(0,$travelerID);					
				}
				else{
					$fbUserID = $this->getFbUserID();
					$this->setFbUserID(0);
					$this->save();
					$dupUser = $this->createDuplicate($this->getFbUserID(),0);					
				}
				return $dupUser;
			}
			return null;			
		}
		
		private function createDuplicate($fbUserID,$travelerID){
			$newTravelBioUser = new TravelBioUser();
			$newTravelBioUser->setFbUserID($fbUserID);
			$newTravelBioUser->setTravelerID($travelerID);
			$newTravelBioUser->getNumOfQuestionsDisplayed($this->getNumOfQuestionsDisplayed());
			$newTravelBioUser->setDateAdded($this->getDateAdded());
			$newTravelBioUser->save();
			$origAnswers = $this->getAnswers();
			foreach($origAnswers as $iAns){
				$newAns = $iAns->createDuplicate();
				$newAns->setTravelBioUserID($newTravelBioUser->getTravelBioUserID());
				$newAns->save();
			}
			return $newTravelBioUser;
		}
		
		public function setAsGaUserOnly(){
			//this operation will only be successfull if and only if the TravelBioUser is currently synchronized.
			if($this->isSynchornized()){
				$this->setFbUserID(0);
				$this->save();
			}
		}
		
		public function setAsFbUserOnly(){
			//this operation will only be successfull if and only if the travelBioUser is currently synchronized.
			if($this->isSynchornized()){
				$this->setTravelerID(0);
				$this->save();
			}
		}
		
		public function getFbFriends(){
			//the user must be a facebook user
			if($this->isFbUser()){
				$fbApi = $this->createFbApi();				
				$fql = 	"SELECT uid, has_added_app ".
	        			"FROM user ".
	        			"WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = ". $this->getFbUserID() .")";
				return $fbApi->api_client->fql_query($fql);
			}
		}
		
		private function createFbApi(){
			if(is_null($this->fbApi)){
				require_once('travellog/model/FacebookPlatform/facebook.php');
				$this->fbApi = new Facebook(FBini::getApiKey(), FBini::getApiSecret());
				//$this->fbApi->set_user(FBini::getAdminFbUserID(), FBini::getInfiniteSessionKey());
			}
			return $this->fbApi;
		}
		
		/**
		 * @return : Boolean
		 * @param genID (TravelerID / userType )
		 * @param userType ;
		 */		

		public static function isExist($genID, $userType){
			$db = FBIni::createDBHandler();
			if(self::FB_USER == $userType){
				return (self::getTravelBioUserByFacebookID($genID) != null );
			}
			elseif(self::GA_USER == $userType){
				return (self::getTravelBioUserByTravelerID($genID) != null );
			}
			else{
				throw new exception('Invalid Usertype '. $userType .' passed to static function isExist of class User.');
			}	
		}
		
		public static function getTravelBioUserByFacebookID($fbID){
			if(0==$fbID){
				return null;
			}
			$db = FBIni::createDBHandler();
			$qry =	'SELECT travelBioUserID ' .
					'FROM tblTravelBioUser ' .
					'WHERE fbUserID = '.ToolMan::makeSqlSafeString($fbID);
			$rs = new iRecordset($db->execQuery($qry));
			if(0 != $rs->retrieveRecordCount()){
				return new TravelBioUser($rs->getTravelBioUserID(0));
			}
			else{
				return null;
			}
		}

		public static function getTravelBioUserByTravelerID($travelerID){
			if(0==$travelerID){
				return null;
			}
			$db = FBIni::createDBHandler();
			$qry =	'SELECT travelBioUserID ' .
					'FROM tblTravelBioUser ' .
					'WHERE travelerID = '.ToolMan::makeSqlSafeString($travelerID);
			$rs = new iRecordset($db->execQuery($qry));
			if(0 != $rs->retrieveRecordCount()){
				return new TravelBioUser($rs->getTravelBioUserID(0));
			}
			else{
				return null;
			}
		}

		public static function getTravelBioUsers($travelerID = 0, $limit = 0){
			$db = FBIni::createDBHandler();
			$arrTravelBioUser = array();
			
			$qry = "SELECT travelBioUserID, fbUserID, travelerID, dateAdded, numQuestionsDisplayed 
					FROM tblTravelBioUser 
					WHERE fbUserID != 0 AND travelerID > $travelerID 
					ORDER BY travelerID ASC LIMIT $limit";
			$rs = new iRecordset($db->execQuery($qry));

			for($i=0; $i<$rs->retrieveRecordCount(); $i++) {
				$travelBioUser = new TravelBioUser;
				$travelBioUser->setTravelBioUserID( $rs->getTravelBioUserID($i) );
				$travelBioUser->setFbUserID( $rs->getFbUserID($i) );
				$travelBioUser->setTravelerID( $rs->getTravelerID($i) );
				$travelBioUser->setDateAdded( $rs->getDateAdded($i) );
				$travelBioUser->setNumOfQuestionsDisplayed( $rs->getNumQuestionsDisplayed($i) );
				
				$arrTravelBioUser[] = $travelBioUser;
			}
			return $arrTravelBioUser;
		}

		public static function isValidMTBUser($travelerID = 0 ){
			if(!$travelerID) return 0;
			$db = FBIni::createDBHandler();
			$id = ToolMan::makeSqlSafeString($travelerID);
			$qry = "SELECT travelerID FROM tblTravelBioUser WHERE fbUserID != 0 AND travelerID = $id";
			$rs = new iRecordset($db->execQuery($qry));
			return $rs->retrieveRecordCount() ? $rs->getTravelerID(0) : 0;
		}
		
		public static function getFbUsers(){
			$db = FBIni::createDBHandler();
			$qry =	'SELECT travelBioUserID ' .
					'FROM tblTravelBioUser ' .
					'WHERE fbUserID != 0';
			/***
			$qry =	'SELECT travelBioUserID, fbUserID, travelerID ' .
					'FROM tblTravelBioUser ' .
					'WHERE fbUserID = 507323382 OR fbUserID = 505580161';
			***/
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if(0 != $rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new TravelBioUser($rs->getTravelBioUserID());
				}
			}
			return $ar;
		}
		
		public static function getGaUsers(){
			$db = FBIni::createDBHandler();
			$qry =	'SELECT travelBioUserID ' .
					'FROM tblTravelBioUser ' .
					'WHERE travelerID != 0';
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if(0 != $rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new TravelBioUser($rs->getTravelBioUserID());
				}
			}
			return $ar;
		}

	}
?>