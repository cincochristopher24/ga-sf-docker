<?php
	require_once('Class.Decorator.php');
	class GaTravelBioUser extends Decorator
	{
		private $mTraveler = null;
		private $mProfile =  null;
		/***
		 * @param TravelBioUser coreObj
		 * @param Traveler traveler
		 */
		function __construct($coreObj,$traveler){
			parent::__construct($coreObj);
			$this->mTraveler = $traveler;
			$this->mProfile = $traveler->getTravelerProfile();
		}
		
		public function getFirstName(){
			return $this->mProfile->getFirstName();
		}
		
		public function getLastName(){
			return $this->mProfile->getLastName();
		}
		
		public function getName(){
			return $this->mProfile->getFirstName().' '.$this->mProfile->getLastName();
		}
		
		public function getHomeTown(){
			$city = $this->mTraveler->getCity();
		}
		
		public function getSex(){
			return $this->mProfile->getGender();
		}
		
		public function getCurrentLocation(){
			return $this->mTraveler->getCurrLocation();
		}
		
		public function getUserName(){
			return $this->mTraveler->getUserName();
		}
	}
?>