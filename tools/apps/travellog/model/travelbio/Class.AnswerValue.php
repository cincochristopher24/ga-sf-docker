<?php
	require_once('travellog/model/travelbio/Class.FBini.php');
	class AnswerValue
	{
		//answer values types
		const TEXT 		= 1;
		const RADIO 	= 2;
		const CHECKBOX 	= 3;
		const SELECTBOX = 4;
		const TEXTAREA 	= 5;
		
		private $mAnswerValueID 		= 0;
		private $mTravelBioUserAnswerID	= 0;
		private $mType					= 0;
		private $mValue					= null;	
		private $mDb					= null;

		function __construct($answerValueID=null){
			$this->mDb = FBini::createDbHandler();
			if(!is_null($answerValueID)){
				$qry = 	'SELECT `answerValueID`, `travelBioUserAnswerID`, `type`, `value` ' .
						'FROM tblAnswerValue ' .
						'WHERE answerValueID = '.ToolMan::makeSqlSafeString($answerValueID);
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(0==$rs->retrieveRecordCount()){
					throw new exception('Invalid answerValueID '.$answerValueID.' passed to object of type AnswerValue');
				}
				else{
					$this->setAnswerValueID( $rs->getAnswerValueID(0) );
					$this->setAnswerID( $rs->getTravelBioUserAnswerID(0) );
					$this->setType( $rs->getType(0) );
					$this->setValue( $rs->getValue(0) );
				}
			}
		}
		
		private function setAnswerValueID($param=0){
			$this->mAnswerValueID = $param;
		}
		
		public function setAnswerID($param=0){
			$this->mTravelBioUserAnswerID = $param;
		}				
		public function setAsText(){
			$this->mType = self::TEXT;
		}				
		public function setAsRadio(){
			$this->mType = self::RADIO;
		}				
		public function setAsCheckbox(){
			$this->mType = self::CHECKBOX;
		}		
		public function setAsSelectbox(){
			$this->mType = self::SELECTBOX;			
		}		
		public function setAsTextarea(){
			$this->mType = self::TEXTAREA;			
		}		
		public function setValue($param=null){
			$this->mValue = $param;
		}		
		public function setType($param=null){
			$this->mType = $param;
		}
					
		//GETTERS
		public function getAnswerValueID(){
			return $this->mAnswerValueID;
		}
		public function getAnswerID(){
			return $this->mTravelBioUserAnswerID;
		}
		public function isTypeText(){
			return self::TEXT == $this->mType;
		}
		public function isTypeRadio(){
			return self::RADIO == $this->mType;
		}
		public function isTypeCheckbox(){
			return self::CHECKBOX == $this->mType;
		}
		public function isTypeSelectbox(){
			return self::SELECTBOX == $this->mType;
		}
		public function isTypeTextarea(){
			return self::TEXTAREA == $this->mType;
		}
		public function getType(){
			return $this->mType;
		}
		public function getValue(){
			return $this->mValue;
		}
		
		/*** CRUDE ***/
		public function save(){
			//perform class level validations
			if(0==$this->getAnswerValueID()){//add
				$qry =	'INSERT INTO tblAnswerValue( ' .
							'`travelBioUserAnswerID` ,' .
							'`type` ,' .
							'`value` ' .
						')VALUES(' .
							ToolMan::makeSqlSafeString( $this->getAnswerID() ).',' .
							ToolMan::makeSqlSafeString( $this->getType() ).',' .
							ToolMan::makeSqlSafeString( $this->getValue() ).
						')';
				$this->mDb->execQuery($qry);
				$this->setAnswerValueID($this->mDb->getLastInsertedID());
			}
			else{//update
				$qry =	'UPDATE tblAnswerValue SET ' .
							'`travelBioUserAnswerID`= ' .ToolMan::makeSqlSafeString( $this->getAnswerID() ) .','.
							'`Type` 				='	.ToolMan::makeSqlSafeString( $this->getType() ).', '.
							'`value` 				='	.ToolMan::makeSqlSafeString( $this->getValue() ).' '.
						'WHERE `answerValueID` 		=' 	.ToolMan::makeSqlSafeString( $this->getAnswerValueID() );
				$this->mDb->execQuery($qry);			
			}
		}
		
		public function delete(){
			$qry = 	'DELETE FROM tblAnswerValue ' .
					'WHERE `answerValueID` = '.ToolMan::makeSqlSafeString( $this->getAnswerValueID() );
			$this->mDb->execQuery($qry);
		}
		
		/*** Static Functions ***/
		public static function getAnswerValuesByAnswerID($answerID=0){
			$db = FBini::createDbHandler();
			$qry = 	'SELECT `answerValueID` ' .
					'FROM tblAnswerValue ' .
					'WHERE travelBioUserAnswerID = '. ToolMan::makeSqlSafeString($answerID) .' '.
					'ORDER BY answerValueID asc';
			$rs = new iRecordset($db->execQuery($qry));			
			$ar = array();
			if(0 != $rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new AnswerValue($rs->getAnswerValueID());
				}
			}
			return $ar;
		}
	}
?>