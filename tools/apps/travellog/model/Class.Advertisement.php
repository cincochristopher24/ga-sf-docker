<?php
/**
 * Created on May 7, 2007
 * @author Ronald Wayne C. Duran
 * Purpose: Show rotating ads on website
 */
 
require_once("Class.Connection.php");
require_once("Class.Recordset.php");
	 
class Advertisement {
	
	private $mConn      = NULL;
    private $mRs        = NULL;  
            
	private static $MADS = array();
	private static $ImagePath = '/images/others/';
	
	
	/**
	 * Attributes
	 */
	private $mAdvertisementID	= 	0;
	private $mBannerID			= 	0;
	private $mClientID			= 	0;
	private $mImagePath  		=  	'';
	private $mAltText			=	'';
	private $mSite				=	'';
	private $mPlainAdText		=	'';
	
	private $mLink				=	'';
	private $mAdText			=	'';
	
	function Advertisement ($_adID = 0){
		
		try {
            $this->mConn = new Connection();
            $this->mRs   = new Recordset($this->mConn);
        }
        catch (Exception $e) {                 
           throw $e;
        }
          
        $this->mAdvertisementID		=	$_adID;
                
		if (0 < $this->mAdvertisementID){
			
			
			
			$sql = "SELECT tblAdvertisement.*, GoAbroad_Main.ads.filename, GoAbroad_Main.ads.alt, GoAbroad_Main.ads.href, GoAbroad_Main.ads.clientID " .
					"FROM Travel_Logs.tblAdvertisement, GoAbroad_Main.ads " .
					"WHERE Travel_Logs.tblAdvertisement.bannerID = GoAbroad_Main.ads.ad_ID " .
					"AND advertisementID = '$this->mAdvertisementID' ";
            $this->mRs->Execute($sql);
        
            if (0 == $this->mRs->Recordcount()){
                //throw new Exception("Invalid AdvertisementID");        // ID not valid so throw exception
            }
            
            // Sets values to its class member attributes.   
            $this->mBannerID        = $this->mRs->Result(0,"bannerID");           
            $this->mClientID  	= $this->mRs->Result(0,"clientID");       
            $this->mImagePath   = stripslashes($this->mRs->Result(0,"filename"));         
            $this->mAltText     = stripslashes($this->mRs->Result(0,"alt"));
            $this->mSite  		= stripslashes($this->mRs->Result(0,"href"));
            $this->mPlainAdText = stripslashes($this->mRs->Result(0,"adtext"));
                    
		}
	}
	
	
	/**
	 * Setters
	 */
	function setAdvertisementID($_advertisementID) {
		$this->mAdvertisementID = $_advertisementID;
	}
	
	function setBannerID($_bannerID) {
		$this->mBannerID = $_bannerID;
	}

	function setClientID($_clientID) {
		$this->mClientID = $_clientID;
	}
	
	function setImagePath($_imagePath) {
		$this->mImagePath = $_imagePath;
	}
	
	function setAltText($_altText) {
		$this->mAltText = $_altText;
	}
	
	function setSite($_site) {
		$this->mSite = $_site;
	}
	
	function setPlainAdText($_plainAdText) {
		$this->mPlainAdText = $_plainAdText;
	}
	
	function setAdText($_adtext) {
		$this->mAdText = $_adtext;
	}
	
	
	
	/**
	 * Getters
	 */
	
	function getAdvertisementID(){
		return $this->mAdvertisementID;	
	}
	function getImagePath() {
		return '/images/other/' . $this->mImagePath;
	}
	
	function getAltText(){
		return $this->mAltText ;
	}
	
	function getBannerID() {
		return $this->mBannerID;
	}
	
	function getClientID() {
		return $this->mClientID;
	}
	
	function getSiteLink(){
		
		return $this->mSite;
	}
	
	function getUrl() {
		$this->mLink = "http://www.goabroad.com/tracker.cfm?i=" . $this->mBannerID ."&amp;url=" . urlencode($this->mSite) . "&amp;clientID=" .  $this->mClientID . "&amp;programID=10&amp;hostx=goabroadnet";
		
		
		return $this->mLink;
	}
	
	function getPlainAdText(){
		
		return $this->mPlainAdText;
	}
	
	function getAdText() {
		$a_pre_tag = '<a href="'. $this->getUrl() . '" target="_blank">';
		$a_end_tag = '</a>';
		
		$string = $this->mPlainAdText;
		$pattern[0] = '/\{/';
		$pattern[1] = '/\}/';
		
		$replacement[0] = $a_pre_tag;
		$replacement[1] = $a_end_tag;
		
		$this->mAdText =  preg_replace($pattern, $replacement, $string);
		
		return $this->mAdText;
	}
	
	
	function Save(){
		
		if (0 == $this->mAdvertisementID)
			$sql = "INSERT INTO tblAdvertisement (bannerID, adtext) VALUES ($this->mBannerID , '$this->mAdText' )";
		else
			$sql = "UPDATE tblAdvertisement SET bannerID = $this->mBannerID , adtext = '$this->mAdText' WHERE advertisementID = $this->mAdvertisementID";
		
		$this->mRs->Execute($sql);
		$this->mAdvertisementID = $this->mRs->GetCurrentID(); 
			
	}
	
	function Delete (){
		
		$sql = "DELETE FROM tblAdvertisement WHERE advertisementID = $this->mAdvertisementID";
		$this->mRs->Execute($sql);
		
	}
	
	
	public static function getRandom($offset){
		
		try {
            $conn = new Connection();
            $rs   = new Recordset($conn);
        }
        catch (Exception $e) {                 
           throw $e;
        }
        
		$sql = "SELECT tblAdvertisement.*, GoAbroad_Main.ads.filename, GoAbroad_Main.ads.alt, GoAbroad_Main.ads.href, GoAbroad_Main.ads.clientID " .
					"FROM Travel_Logs.tblAdvertisement, GoAbroad_Main.ads " .
					"WHERE Travel_Logs.tblAdvertisement.bannerID = GoAbroad_Main.ads.ad_ID " .
					"LIMIT $offset , 1 ";
					
		$rs->Execute($sql);
		
		if ($rs->RecordCount()) {
			$randomAd = new Advertisement();
			$randomAd->setAdvertisementID	($rs->Result(0,"advertisementID"));
			$randomAd->setBannerID	($rs->Result(0,"bannerID"));
			$randomAd->setClientID	($rs->Result(0,"clientID")); 
			$randomAd->setImagePath	(stripslashes($rs->Result(0,"filename")));
			$randomAd->setAltText	(stripslashes($rs->Result(0,"alt")));
			$randomAd->setSite		(stripslashes($rs->Result(0,"href")));
			$randomAd->setPlainAdText(stripslashes($rs->Result(0,"adtext")));
	}
		else
			$randomAd = new Advertisement(0);
			
		return $randomAd;
	}
	
	public static function getAllAds(){
		
		try {
            $conn = new Connection();
            $rs   = new Recordset($conn);
        }
        catch (Exception $e) {                 
           throw $e;
        }
        
		$sql = "SELECT * FROM tblAdvertisement ORDER BY bannerID";
		$rs->Execute($sql);
	
		$arrAds = array();
		
		while ($recordset = mysql_fetch_array($rs->ResultSet())):
			try {
				$arrAds[] = new Advertisement($recordset['advertisementID']);
			}
			catch (Exception $e) {
               throw $e;
            }			
		endwhile;
		
		
		return $arrAds;
		
	}
	
	
	public static function getCountAllAds(){
		
		try {
            $conn = new Connection();
            $rs   = new Recordset($conn);
        }
        catch (Exception $e) {                 
           throw $e;
        }
        
		$sql = "SELECT * FROM tblAdvertisement ORDER BY bannerID";
		$rs->Execute($sql);
	
		$countAds = $rs->RecordCount();
		
		return $countAds;
		
	}
	
	/**
	 * Temp
	 */
	/**
	private static function initAds() {
		self::setAd( 
						1981, 
						'World Endeavors',
						'worldendeavors_2007.png',
						'Change Your World',
						'http://www.goabroad.com/tracker.cfm?i=2001&amp;url=http://www.worldendeavors.com&amp;clientID=1981&amp;programID=10&amp;hostx=goabroadnet',
						'<a href="http://www.goabroad.com/tracker.cfm?i=2001&amp;url=http://www.worldendeavors.com&amp;clientID=1981&amp;programID=10&amp;hostx=goabroadnet">World Endeavors</a> <br />
						Volunteer, Internship and Study Abroad.<br />
						Change Your World!'
					);

		self::setAd( 
						2399, 
						'CIS-Center for International Studies',
						'cis_2007b.gif',
						'Study Live Explore',
						'http://www.goabroad.com/tracker.cfm?i=2009&amp;url=http://www.studyabroad-cis.com/&amp;clientID=2399&amp;programID=10&amp;hostx=goabroadnet',
						'<a href="http://www.goabroad.com/tracker.cfm?i=2009&amp;url=http://www.studyabroad-cis.com/&amp;clientID=2399&amp;programID=10&amp;hostx=goabroadnet">Study abroad with CIS</a><br />
						Internship, Semester, &amp; Summer
						Opportunities Available'
					);

		self::setAd(
						745, 
						'AustraLearn',
						'australearn_2007.gif',
						'Programs in Australia | New Zealand',
						'http://www.goabroad.com/tracker.cfm?i=2011&amp;url=http://www.australearn.org&amp;clientID=745&amp;programID=10&amp;hostx=goabroadnet',
						'<a href="http://www.goabroad.com/tracker.cfm?i=2011&amp;url=http://www.australearn.org&amp;clientID=745&amp;programID=10&amp;hostx=goabroadnet">AustraLearn</a><br />
						Study in Australia, New Zealand, and the South Pacific'
					);

		self::setAd(
						8955, 
						'Global Learning Semesters',
						'gls_2007.gif',
						'The World in One Semester',
						'http://www.goabroad.com/tracker.cfm?i=2013&amp;url=http://www.globalsemesters.com&amp;clientID=8955&amp;programID=10&amp;hostx=goabroadnet',
						'Transformative Study Abroad Programs with <a href="http://www.goabroad.com/tracker.cfm?i=2013&amp;url=http://www.globalsemesters.com&amp;clientID=8955&amp;programID=10&amp;hostx=goabroadnet">Global Learning Semesters</a>'
					);

		self::setAd(
						12884, 
						'The Scholar Ship',
						'the_scholar_ship_2007b.gif',
						'Experience an Extraordinary Voyage',
						'http://www.goabroad.com/tracker.cfm?i=2015&amp;url=http://www.TheScholarShip.com&amp;clientID=12884&amp;programID=10&amp;hostx=goabroadnet',
						'<a href="http://www.goabroad.com/tracker.cfm?i=2015&amp;url=http://www.TheScholarShip.com&amp;clientID=12884&amp;programID=10&amp;hostx=goabroadnet">The Scholar Ship</a>'
					);

		self::setAd(
						1592, 
						'CIEE',
						'ciee_2007d.gif',
						'What will your story be?',
						'http://www.goabroad.com/tracker.cfm?i=2017&amp;url=http://www.ciee.org/isp&amp;clientID=1592&amp;programID=10&amp;hostx=goabroadnet',
						'<a href="http://www.goabroad.com/tracker.cfm?i=2017&amp;url=http://www.ciee.org/isp&amp;clientID=1592&amp;programID=10&amp;hostx=goabroadnet">CIEE</a> has over 100 study abroad programs in 35 countries & 40 subject areas'
					);
	}
	*/
	/**
	 * Temp
	 */
	/**
	function setAd($clientID, $clientName, $image, $altText, $url, $text) {
		self::$MADS[] = array(
							'clientID'      => $clientID, 
							'clientName'    => $clientName,
							'image'         => '/images/others/'.$image,
							'altText'       => $altText,
							'url'           => $url,
							'text'          => $text
						);
		
	}
	*/
	/**
	 * Temp
	 */
	/**
	static function getAdx($adID = 0) {
		if (!count(self::$MADS)) {
			self::initAds();
		}
		$adarr = self::$MADS[rand(0, count(self::$MADS) -1)];
		$ad = new Advertisement();
		$ad->setClientID($adarr['clientID']);
		$ad->setClientName($adarr['clientName']);
		$ad->setAltText($adarr['altText']);
		$ad->setImagePath($adarr['image']);
		$ad->setUrl($adarr['url']);
		$ad->setAdText($adarr['text']);
		return $ad;
	}
	*/
	static function getAd($adID = 0) {
		
		$rand_idx = rand(0, self::getCountAllAds()-1);
		
		$ad = self::getRandom($rand_idx);
		
		return $ad;
	}
	
	
}

?>
