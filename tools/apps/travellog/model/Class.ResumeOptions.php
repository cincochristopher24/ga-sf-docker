<?php
/*
 * Created on Aug 3, 2006
 */
 
require_once("Class.Connection.php");
require_once("Class.Recordset.php");
 
 
 class ResumeOptions
 {
 		// get list of intern types
 		
 		static function getInternTypeList()
 		{
 			
 			$conn = new Connection();
			$rs = new Recordset($conn);
					
 			$degproglist = "select * from GoAbroad_Main.tbdegprog order by degprog";
 			$results = $rs->Execute($degproglist);
 			
 			$major = array();
 			
 			while($row = mysql_fetch_assoc($results))
 			{
 				$major[$row['degprogID']] = $row['degprog'];
 			}
 			
 			return $major;
 		}
 		
 		// get list of academic degrees
 		
 		static function getAcadDegreeList()
 		{
 			
 			$conn = new Connection();
			$rs = new Recordset($conn);
			
 			$degreelist = "select * from GoAbroad_Main.tbacaddegree order by acaddegreeID";
 			$results = $rs->Execute($degreelist);
 			
 			$acaddegree = array();
 			
 			while ($row = mysql_fetch_assoc($results))
 			{
 				$acaddegree[$row['acaddegreeID']] = $row['acaddegree'];
 			}
 			
 			return $acaddegree;
 		}
 		
 		// get list of teaching positions
 		
 		static function getTeachingPositionList()
 		{
 			
 			$conn = new Connection();
			$rs = new Recordset($conn);
			
 			$teachposlist = "select * from GoAbroad_Main.tbteachingposition order by teachingpositionID";
 			$results = $rs->Execute($teachposlist);
 			
 			$teachpos = array();
 			
 			while($row = mysql_fetch_assoc($results))
 			{
 				$teachpos[$row['teachingpositionID']] = $row['teachingposition'];
 			}
 			
 			return $teachpos;
 		}
 		
 		// get list of english proficiency
 		
 		static function getEnglishProfList()
 		{
 			
 			$conn = new Connection();
			$rs = new Recordset($conn);
			
 			$engproflist = "select * from tblEnglishProficiency order by englishproficiencyID";
 			$results = $rs->Execute($engproflist);
 			
 			$englishprof = array();
 			
 			while($row = mysql_fetch_assoc($results))
 			{
 				$englishprof[$row['englishproficiencyID']] = $row['englishproficiency'];
 			}
 			
 			return $englishprof;
 		}
 		
 		// get list of teaching years
 		
 		static function getTeachYearsList()
 		{
 			
 			$conn = new Connection();
			$rs = new Recordset($conn);
			
 			$teachyrslist = "select * from tblTeachingYears order by teachingyearsID";
 			$results = $rs->Execute($teachyrslist);
 			
 			$teachyears = array();
 			
 			while($row = mysql_fetch_assoc($results))
 			{
 				$teachyears[$row['teachingyearsID']] = $row['teachingyears']; 
 			}
 			
 			return $teachyears;
 		} 
 		
 		// get list of teaching certifications
 		
 		static function getTeachCertList()
 		{
 			
 			$conn = new Connection();
			$rs = new Recordset($conn);
			
 			$teachcertlist = "select * from tblTeachingCertificate order by teachingcertificateID";
 			$results = $rs->Execute($teachcertlist);
 			
 			$teachcert = array();
 			
 			while($row = mysql_fetch_assoc($results))
 			{
 				$teachcert[$row['teachingcertificateID']] = $row['teachingcertificate']; 
 			}
 			
 			return $teachcert;
 		}
 		
 		// get list of teaching levels
 		
 		static function getTeachingLevelList()
 		{
 			
 			$conn = new Connection();
			$rs = new Recordset($conn);
			
 			$teachlevellist = "select * from tblPreferredTeachingLevel order by preferredteachinglevelID";
 			$results = $rs->Execute($teachlevellist);
 			
 			$prefteachlevel = array();
 			
 			while($row = mysql_fetch_assoc($results))
 			{
 				$prefteachlevel[$row['preferredteachinglevelID']] = $row['preferredteachinglevel'];
 			}
 			
 			return $prefteachlevel;
 		}
 		
 		
 }
?>
