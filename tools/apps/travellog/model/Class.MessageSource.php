<?php
	/**
	 * @author: Reynaldo Castellano III
	 * @date: January 29, 2008
	 */
	class MessageSource
	{
		const TRAVELER_CONTEXT = 1;
		const GROUP_CONTEXT 	= 2;
		
		private $mMessage 		= null;
		private $mSource 		= null;
		private $mRecipients 	= null;
		private $mContext		= null;
		private $mRecipientGroup= null;
		
		/**
		 * @param source : can be an instance of traveler or group.
		 */
		public function __construct($message=null){
			$this->mMessage = $message;
			$this->mSource = $this->mMessage->getSource();
		}
		
		/***
		 * Checks if the source posted as an administrator
		 */
		public function postedAsAdministrator(){
			return $this->mSource instanceof Group;
		}
		
		/**
		 * Checks if the source posted as a staff
		 */
		public function postedAsStaff(){
			return $this->mSource instanceof Staff;
		} 
		
		/**
		 * Checks if the source posted as a moderator
		 */
		public function postedAsModerator(){
			return $this->mSource instanceof Moderator;
		}
		
		/**
		 * Checks if the source posted as a member of a group
		 */
		public function postedAsMember(){
			return $this->getMessageContext() == self::GROUP_CONTEXT 
				&& !$this->postedAsAdministrator() 
				&& !$this->postedAsStaff() 
				&& !$this->postedAsModerator();
		}
		
		/**
		 * Checks if the source posted as a mere traveler.
		 */
		public function postedAsTraveler(){
			return $this->getMessageContext() == self::TRAVELER_CONTEXT;
		}
		
		
		/***
		 * Returns the name of the source group or traveler 
		 */
		public function getName(){
			return $this->mSource->getName();
		}
		
		/**
		 * Returns the designation of the surce at the time of posting.
		 * Possible values:
		 * 	- Administrator : If the source posted as an administrator.
		 *  - Staff			: If the source posted as a staff.
		 * 	- Moderator		: If the source posted as a moderator.
		 *  - [Username]	: If the source posted as plain traveler or a member of a group.
		 */
		public function getDesignation(){
			//check if the source posted as 
			if($this->postedAsAdministrator()){
				return 'Administrator';
			}
			elseif($this->postedAsStaff()){
				return $this->getName().'(Staff)';
			}
			elseif($this->postedAsModerator()){
				return $this->getName().'(Moderator)';
			}
			else{
				return $this->getName();
			}
		}
		
		public function getFriendlyURL(){
			return $this->mSource->getFriendlyURL();
		}
		
		public function getGroupID(){
			if($this->mSource instanceof Group){
				return $this->mSource->getGroupID();
			}
			return 0;
		}
		
		public function getTravelerID(){
			if($this->mSource instanceof Traveler){
				return $this->mSource->getTravelerID() ;
			}
			return 0;
		}
		
		/**
		 * Determines the context of the message
		 * Possible values: Traveler or Group 
		 */		
		private function getMessageContext(){
			if(is_null($this->mContext)){
				$this->mContext = !is_null($this->getRecipientGroup()) 
					? self::GROUP_CONTEXT 
					: self::TRAVELER_CONTEXT;
			}
			return $this->mContext;
		}
		
		public function getRecipientGroup(){
			if(is_null($this->mRecipientGroup)){
				$recipients = $this->getMessageRecipients();
				foreach($recipients as $iRecipient){
					if($iRecipient instanceof GROUP){
						$this->mContext  = self::GROUP_CONTEXT;
						$this->mRecipientGroup = $iRecipient;
						break;
					}
				}
			}
			return $this->mRecipientGroup;
		}
		
		
		private function getMessageRecipients(){
			if(is_null($this->mRecipients)){
				$this->mRecipients = $this->mMessage->getDestination(); 
			}
			return $this->mRecipients;
		}
		
	}
?>