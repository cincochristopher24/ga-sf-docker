<?php
/**
* <b>SearchFirstandLastnameService</b> class
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/
require_once("travellog/model/Class.LocationFactory.php");
require_once("Class.Connection.php");
require_once("Class.Recordset.php");
class SearchTravelerInLocationService{
	function performSearch(){
		$conn = new Connection();
		$rs   = new Recordset($conn);
		$arr  = array();
		/*$sql  = "SELECT GoAbroad_Main.tbcountry.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler,GoAbroad_Main.tbcountry " .
				"WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcountry.locID " .
				"AND  tblTraveler.isSuspended = 0 " .
				"AND  tblTraveler.active > 0 ".
				"AND travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) " .
				"AND GoAbroad_Main.tbcountry.locID > 0 " .
				"GROUP BY GoAbroad_Main.tbcountry.locID " .
				"UNION ".
				"SELECT GoAbroad_Main.tbcity.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler,GoAbroad_Main.tbcountry,GoAbroad_Main.tbcity " .
				"WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcity.locID " .
				"AND  tblTraveler.isSuspended = 0 " .
				"AND  tblTraveler.active > 0 ".
				"AND GoAbroad_Main.tbcountry.countryID = GoAbroad_Main.tbcity.countryID ".
				"AND GoAbroad_Main.tbcity.locID > 0 " .
				"AND travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) " .
				"GROUP BY GoAbroad_Main.tbcountry.locID";*/
		$sql  = "SELECT GoAbroad_Main.tbcountry.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler,GoAbroad_Main.tbcountry " .
				"WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcountry.locID " .
				"AND  tblTraveler.isSuspended = 0 " .
				"AND  tblTraveler.active > 0 ".
				"AND GoAbroad_Main.tbcountry.locID > 0 " .
				"GROUP BY GoAbroad_Main.tbcountry.locID " .
				"UNION ".
				"SELECT GoAbroad_Main.tbcity.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler,GoAbroad_Main.tbcountry,GoAbroad_Main.tbcity " .
				"WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcity.locID " .
				"AND  tblTraveler.isSuspended = 0 " .
				"AND  tblTraveler.active > 0 ".
				"AND GoAbroad_Main.tbcountry.countryID = GoAbroad_Main.tbcity.countryID ".
				"AND GoAbroad_Main.tbcity.locID > 0 " .
				"GROUP BY GoAbroad_Main.tbcountry.locID";
		$results = $rs->Execute($sql);
		if ($rs->Recordcount()){
			while($row = mysql_fetch_assoc($results)){
				if( $row['locID'] > 0){
					$factory      = LocationFactory::instance();
					$objNewMember = $factory->create($row["locID"]);
					if (strcasecmp(get_class($objNewMember),"country") == 0)
						$arr[$objNewMember->getName()] = $objNewMember;	
					else
						$arr[$objNewMember->getCountry()->getName()] = $objNewMember->getCountry();
				}
			}
		}	
		ksort($arr);
		return $arr;	 
	}
}
?>
