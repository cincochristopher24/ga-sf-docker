<?php
	require_once 'Class.ToolMan.php';
	require_once 'Class.dbHandler.php';
	require_once 'Class.iRecordset.php';

	class FBIni{
		public static function createDBHandler(){
			require_once('Class.dbHandler.php');
			/***
			if('DEV'==$server || 'PROD' == $server){
				$host = ($server == 'DEV')?'192.168.3.9':'eth0.ga_database01';
				$db = new dbHandler('NONE');
				$db->setHost($host);
				$db->setUser('goabroad');
				$db->setPassword('g4pt4lip4p4');
				$db->connect();	
			}
			***/
			$db = new dbHandler();
			$db->setDB('SocialApplication');
			return $db;
		}
		
		public static function getApiKey(){
			return 'a2a4d53f9caee9c6e2d57b850b7ab8dc';
		}

		public static function getApiSecret(){
			return '3cdf2b9b5590f130331684dde485a6ae';
		}
		
		public static function getImagePath(){
			//return 'http://www.studyabroadlibrary.com/apps/travtype/footprints/images/';
			return 'http://images.goabroad.net/images/facebook/';
		}
		
		public static function getLogoPath(){
			return self::getImagePath().'mytravelbioicon.gif';
		}
		
		public static function getAnswerIconPath(){
			return self::getImagePath().'answer_icon.gif';	
		}
			
		public static function getCallbackDirectory(){
			return 'http://www.goabroad.net/travelbio/';
		}
		
		public static function getFBRootDirectory(){
			return 'http://apps.facebook.com/travelbio/';
		}
		
		public static function getAdminFbUserID(){
			return '507323382';
			//return '505580161';
		}
		
		public static function getInfiniteSessionKey(){
			return '8ce7abb1f377f1c324fcc1ca-507323382';
			//return 'ca3d7484104403ba45b9dec2-505580161';
		}
	}
?>