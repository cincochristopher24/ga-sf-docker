<?

/*

 	Created on Jul 31, 2006
  	Created by: Joel C. Llano
	
 */

Class resizeimage {

	
	private $area = "freesize";
	
	
	function setArea($area){
		$this->area = $area;	
	}
	
	function resize($size,$width,$height,$filename,$filetype) {
			
			ini_set("memory_limit","100M");
			
			
			switch($this->area){
				
					case "canvas":
							/*
							 * if width greater than height (landscape image)
							 */
							if($width >= $height){					
									if($height >= $size){															
											$actualheight	=$size;			
											$x = (($height  - $size) * 100)/$height;						
											$x = ($x/100) * $width;	
											$actualwidth  =$width - $x;																		
									}else{
											
											$actualheight	= $height;
											$actualwidth	=$width;											
																									
									}							
							}else{
							/*
							 * if height greater than width (portrait image)
							*/		
									if($width >= $size){															
											$actualwidth	= $size;			
											$x = (($width  - $size) * 100)/$width;			
											$x = ($x/100) * $height;			
											$actualheight	= $height - $x;																		
									}else{
											$actualwidth	=$width;			
											$actualheight	= $height;																
									}							
												
							}		
					break;
										
					default:
							/*
							 * if width greater than height (landscape image)
							 */
							if($width >= $height){					
									if($width >= $size){															
											$actualwidth	= $size;			
											$x = (($width  - $size) * 100)/$width;			
											$x = ($x/100) * $height;			
											$actualheight	= $height - $x;																		
									}else{
											$actualheight	=$size;			
											$x = (($height  - $size) * 100)/$height;						
											$x = ($x/100) * $width;	
											$actualwidth  =$width - $x;																	
									}														
							}else{
							/*
							 * if height greater than width (portrait image)
							*/						
									if($height >= $size){															
											$actualheight	=$size;			
											$x = (($height  - $size) * 100)/$height;						
											$x = ($x/100) * $width;	
											$actualwidth  =$width - $x;																		
									}else{										
											$actualwidth	= $size;			
											$x = (($width  - $size) * 100)/$width;			
											$x = ($x/100) * $height;			
											$actualheight	= $height - $x;															
									}			
							}		
					break;
			}	
			
			
		
		$image_p = imagecreatetruecolor($actualwidth, $actualheight);                       
				
		//$imagetype = substr($filetype,6,4);
		
		$imagetype = getimagesize($filename);		
			
			switch (substr($imagetype['mime'],6,4)) {
					case "jpeg":
						   	$image = imagecreatefromjpeg($filename); 						
							imagecopyresampled($image_p, $image, 0, 0, 0, 0, $actualwidth, $actualheight, $width, $height);      	
							imagejpeg($image_p, $filename, 75);
						 	break;
					case "gif":					
						   	$image = imagecreatefromgif($filename); 						
							imagecopyresampled($image_p, $image, 0, 0, 0, 0, $actualwidth, $actualheight, $width, $height);      				
							//imagecopyresized($image_p, $image, 0, 0, 0, 0, $actualwidth, $actualheight,  $width,$height);
							imagegif($image_p, $filename, 75);
						   	break;
					case "png":
						  	$image = imagecreatefrompng($filename); 						
							imagecopyresampled($image_p, $image, 0, 0, 0, 0, $actualwidth, $actualheight, $width, $height);      	
							imagepng($image_p, $filename, 75);
						  	break;
					default:
							
					break;		
			}
			
			 imagedestroy($image_p);
			 imagedestroy($image);	
	}
	
}
	
?>