<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');

	class AddressBookEntryPreferenceAction
	{
		const ACTION_ADD_JOURNAL 	= 0;
		const ACTION_ADD_PHOTO		= 1;
		
		private $mDb 									= null;
		private $mAddressBookEntryPreferenceActionID 	= null;
		private $mAddressBookEntryPreferenceID			= null;
		private $mAction								= null;
		
		function __construct($param=null,$data=null){
			$this->mDb = new dbHandler();
			if(!is_null($param)){
				$qry = 	'SELECT addressBookEntryPreferenceActionID, addressBookEntryPreferenceID, action ' .
						'FROM tblAddressBookEntryPreferenceAction ' .
						'WHERE addressBookEntryPreferenceActionID = '.ToolMan::makeSqlSafeString($param);		
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(0==$rs->retrieveRecordCount()){
					throw new exception('Invalid addressBookEntryPreferenceActionID '.$param.' passed to object of type AddressBookEntryPreferenceAction.');
				}
				$this->init($rs->retrieveRow(0));
			}
			if(is_array($data)){
				$this->init($data);
			}
		}
		
		private function init($data){
			$this->setAddressBookEntryPreferenceActionID($data['addressBookEntryPreferenceActionID']);
			$this->setAddressBookEntryPreferenceID($data['addressBookEntryPreferenceID']);
			$this->setAction($data['action']);
		}
		
		/*** SETTERS ***/
		private function setAddressBookEntryPreferenceActionID($param){
			$this->mAddressBookEntryPreferenceActionID = $param;
		}
		
		public function setAddressBookEntryPreferenceID($param){
			$this->mAddressBookEntryPreferenceID = $param;
		}
		
		public function setAction($param){
			$this->mAction = $param;
		}
		
		/*** GETTERS ***/
		public function getAddressBookEntryPreferenceActionID(){
			return $this->mAddressBookEntryPreferenceActionID;
		}
		
		public function getAddressBookEntryPreferenceID(){
			return $this->mAddressBookEntryPreferenceID;
		}
		
		public function getAction(){
			return $this->mAction;
		}
		
		/*** CRUDE ****/
		public function save(){
			if(!is_numeric($this->getAddressBookEntryPreferenceID()) || 0==$this->getAddressBookEntryPreferenceID() ){
				throw new exception('Cannot perform save operation. Invalid addressEntryBookPreferenceID '.$this->getAddressBookEntryPreferenceID().' passed to object of type AddressBookEntryPreferenceAction.');
			}
			if(!self::isValidAction($this->getAction())){
				throw new exception('Cannot perform save operation. Invalid action passed to object of type AddressBookEntryPreferenceAction.');
			}
			if(is_null($this->getAddressBookEntryPreferenceActionID())){
				$qry =	'INSERT INTO tblAddressBookEntryPreferenceAction( ' .
							'`addressBookEntryPreferenceID`, ' .
							'`action`' .
						')VALUES(' .
							ToolMan::makeSqlSafeString( $this->getAddressBookEntryPreferenceID() ) .', ' .
							ToolMan::makeSqlSafeString( $this->getAction() ).		
						')';
				$this->mDb->execQuery($qry);		
				$this->setAddressBookEntryPreferenceActionID($this->mDb->getLastInsertedID());
			}
			else{
				$qry =	'UPDATE tblAddressBookEntryPreferenceAction SET' .
							'`addressBookEntryPreferenceID` = ' .ToolMan::makeSqlSafeString( $this->getAddressBookEntryPreferenceID() ).', '.
							'`action` = '.ToolMal::makeSqlSafeString($this->getAction()) .' ' .
						'WHERE addressBookEntryPreferenceActionID = '. ToolMan::makeSqlSafeString( $this->getAddressBookEntryPreferenceActionID() );
				$this->execQuery($qry);
			}
		}
		
		public function delete(){
			$qry = 	'DELETE FROM tblAddressBookEntryPreferenceAction ' .
					'WHERE addressBookEntryPreferenceActionID = '.ToolMan::makeSqlSafeString( $this->getAddressBookEntryPreferenceActionID() );
			$this->mDb->execQuery($qry);
		}
		
		/*** STATIC FUNCTIONS  ***/
		public static function isValidAction($param){
			return self::ACTION_ADD_JOURNAL === $param || self::ACTION_ADD_PHOTO === $param;
		}
		
		public static function getActionsByAddressBookEntryPreferenceID($param){
			$db = new dbHandler();
			$qry = 	'SELECT addressBookEntryPreferenceActionID, addressBookEntryPreferenceID, action ' .
					'FROM tblAddressBookEntryPreferenceAction ' .
					'WHERE addressBookEntryPreferenceID = '.ToolMan::makeSqlSafeString($param);
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new AddressBookEntryPreferenceAction(null,$rs->retrieveRow());
				}
			}
			return $ar;
		}
		
		public static function deleteActionsByAddressBookEntryPreferenceID($param){
			$db = new dbHandler();
			$qry =	'DELETE FROM tblAddressBookEntryPreferenceAction ' .
					'WHERE addressBookEntryPreferenceID = '.ToolMan::makeSqlSafeString($param);
			$db->execQuery($qry);
		}
		
		public static function getActionByAddressBookEntryPreferenceIDAndAction($prefID,$action){
			$db = new dbHandler();
			if(!self::isValidAction($action)){
				throw new exception('Invalid action passed to static function getActionByAddressBookEntryPreferenceIDAndAction of class AddressBookEntryPreferenceAction.');
			}
			$qry =	'SELECT addressBookEntryPreferenceActionID, addressBookEntryPreferenceID, action ' .
					'FROM tblAddressBookEntryPreferenceAction ' .
					'WHERE addressBookEntryPreferenceID = '.ToolMan::makeSqlSafeString($prefID).' ' .
					'AND action = '.ToolMan::makeSqlSafeString($action);
			$rs = new iRecordset($db->execQuery($qry));
			if($rs->retrieveRecordCount() > 0){
				return new AddressBookEntryPreferenceAction(null,$rs->retrieveRow(0));
			}
			return null;
		}
	}
?>
