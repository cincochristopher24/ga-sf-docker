<?php
	/**
	 * @(#) Class.CountryPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 4, 2009
	 */
	 
	require_once("travellog/model/base/Class.BaseCountryPeer.php");
	
	/**
	 * Peer class of country
	 */
	class CountryPeer extends BaseCountryPeer {
		
		const COUNTRY_ID = "GoAbroad_Main.tbcountry.countryID";
		const LOCATION_ID = "GoAbroad_Main.tbcountry.locationID";
		const COUNTRY = "GoAbroad_Main.tbcountry.country";
		const APPROVED = "GoAbroad_Main.tbcountry.approved";
		const REGION_ID = "GoAbroad_Main.tbcountry.regionID";
		const CONTINENT_ID = "GoAbroad_Main.tbcountry.continentID";
		const COUNTRY_CODE = "GoAbroad_Main.tbcountry.countrycode";
	}
	
