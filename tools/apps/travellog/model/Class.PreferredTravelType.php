<?
	
/**
 * Created on Nov 23, 2006
 * Created by: Joel C. LLano
 */
	
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	

	class PreferredTravelType{
		
		private $mConn         = NULL;
        private $mRs           = NULL;
        
		private $preferredtypeid		= 0;
		private $preferredtype		= NULL;
		
		function PreferredTravelType($preferredtypeid = 0){
 				
 				$this->preferredtypeid= $preferredtypeid;
 				
 				try {
	 				$this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
				}
				
				catch (Exception $e) {				   
				   throw $e;
				}

			 			if ($preferredtypeid > 0){
			 				
					 				$sql = "SELECT * from tblPreferredTravelType where preferredtypeid = '$preferredtypeid' ";
						 			$this->mRs->Execute($sql);
					 			
					 				if ($this->mRs->Recordcount() == 0){
					 					throw new Exception("Invalid PreferredTravelType ID");	// ID not valid so throw exception
					 				}
					 				
					 				// set values to its attributes
									$this->preferredtype 		= stripslashes($this->mRs->Result(0,"preferredtype")); 
				 		}
						
						
 			}
		
		public static function getAllPreferredTravelType(){
				try {
	 				$conn = new Connection();
					$rs   = new Recordset($conn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT * from tblPreferredTravelType  order by preferredtype ASC ";
				$rs->Execute($sql);
				
				$travelerpreferttype_s = array();
							
				while ($rsar = mysql_fetch_array($rs->Resultset())) {
					$travelerpreferttype_s[] = new PreferredTravelType($rsar['preferredtypeid']);
				}
				return $travelerpreferttype_s;
		}
		
		
		/**
		 * Use 	__call function to dynamically call getter and setters
		 */
		
		function __call($method, $arguments) {
				//prefix ->get and set <- to lowercase
		        $prefix = strtolower(substr($method, 0, 3));
		        $property = strtolower(substr($method, 3));
		
		        if (empty($prefix) || empty($property)) {
		            return;
		        }
		
		        if ($prefix == "get" && isset($this->$property)) {
		            return $this->$property;
		        }
		
		        if ($prefix == "set") {
		            $this->$property = $arguments[0];
		        }
    	}
		
		/**
         * CRUD Methods (Create, Update , Delete)
         */ 
        function Create(){
            
            $Aname  = addslashes($this->preferredtype);
            
            $sql = "INSERT into tblPreferredTravelType (preferredtype) VALUES ('$Aname')";
            $this->mRs->Execute($sql);       
            $this->preferredtypeid = $this->mRs->GetCurrentID();
        }
        
        function Update(){
            
            $Aname = addslashes($this->preferredtype);
            
            $sql = "UPDATE tblPreferredTravelType SET preferredtype = '$Aname' WHERE preferredtypeid = '$this->preferredtypeid' ";
            $this->mRs->Execute($sql);
        }
        
        function Delete(){
            
            $sql = "DELETE FROM tblPreferredTravelType WHERE preferredtypeid = '$this->preferredtypeid' ";        
            $this->mRs->Execute($sql);       
        }       
		
		
	}



?>