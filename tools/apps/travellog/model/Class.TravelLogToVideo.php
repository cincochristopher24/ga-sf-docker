<?php
/*
 * Created on 10 7, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

	/**
	 * class mapping the entities TravelLog and Video
	 */

	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
	require_once('Class.TravelLog.php');
	require_once('Class.Video.php');
	require_once('Class.ConnectionProvider.php');
    require_once('Cache/ganetCacheProvider.php');
	
	class TravelLogToVideo extends Video {
		
		/**
		 * member variable
		 */
		
		private $mParentTravelLog = NULL;
		private $mConn     		  = NULL;
        private $mRs       		  = NULL;
		
		/**
		 * constructor
		 */
		
		public function __construct($videoID = 0) {
	 		$this->mVideoID = $videoID;
			
			try {
                $this->mRs   = new Recordset(ConnectionProvider::instance()->getConnection());
            }
            catch (Exception $e) {                 
               throw $e;
            }
            
            if (0 < $this->mVideoID){ 
               	$query = "SELECT " .
	 						"tblTravelLogtoVideo.videoID, " .
	 						"tblTravelLogtoVideo.travellogID, " .
	 						"tblVideo.videoURL, " .
	 						"tblVideo.videoImageURL, " .
	 						"tblVideo.caption, " .
	 						"tblVideo.dateAdded, " .
	 						"tblVideo.isPrimaryVideo, " .
	 						"tblVideo.duration, " .
	 						"tblVideo.title " .
	 					 "FROM tblTravelLogtoVideo INNER JOIN tblVideo " .
	 					 "WHERE tblTravelLogtoVideo.videoID = tblVideo.videoID " .
	 					 "AND tblTravelLogtoVideo.videoID = $videoID";
                $this->mRs->Execute($query);
            
                if (0 == $this->mRs->Recordcount()){
                    throw new Exception('Invalid VideoID');        // ID not valid so throw exception
                }
                
                // Sets values to its class member attributes.      
                $this->mVideoUrl       	= $this->mRs->Result(0, 'videoURL');           
                $this->mVideoImageUrl   = $this->mRs->Result(0, 'videoImageURL');
                $this->mCaption 		= stripslashes($this->mRs->Result(0, 'caption'));
                $this->mDateAdded 		= $this->mRs->Result(0, 'dateAdded');  
                $this->mIsPrimaryVideo  = $this->mRs->Result(0, 'isPrimaryVideo');    
                $this->mDuration		= $this->mRs->Result(0, 'duration');
                $this->mTitle			= $this->mRs->Result(0, 'title');
                $this->mParentTravelLog = new TravelLog($this->mRs->Result(0, 'travelLogID'));       
           }
	 	}
	 	
	 	/**
	 	 * setter method
	 	 */
	 	
	 	public function setParentTravelLog($travelLog) {
	 		$this->mParentTravelLog = $travelLog;
	 	}
	 	
	 	/**
	 	 * getter method
	 	 */
	 	
	 	public function getParentTravelLog() {
	 		return $this->mParentTravelLog;
	 	}
	 	
	 	/**
	 	 * method that creates an instance of this class
	 	 */
	 	
	 	public function create() {
	 		$travelLogID = $this->mParentTravelLog->getTravelLogID();
			$this->mIsPrimaryVideo = (0 == count($this->mParentTravelLog->getTravelLogVideos())) ? 1 : 0;
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		
	 		$query = "INSERT INTO tblVideo (" .
	 					"videoURL, " .
	 					"videoImageURL, " .
	 					"caption, " .
	 					"dateAdded, " .
	 					"isPrimaryVideo, " .
	 					"duration, " .
	 					"title) " .
	 				  "VALUES (" .
	 					"'$this->mVideoUrl',".
	 					"'$this->mVideoImageUrl',".
	 					"'".addslashes($this->mCaption)."',".
	 					"'$this->mDateAdded',".
	 					"$this->mIsPrimaryVideo,".
	 					"'$this->mDuration',".
	 					"'".addslashes($this->mTitle)."')";
					 	
            $this->mRs->Execute($query);       
            $this->mVideoID = $this->mRs->GetCurrentID();
            
            $query = "INSERT INTO tblTravelLogtoVideo (" .
	 						"videoID, " .
	 						"travellogID) " .
	 					  "VALUES (" .
	 					  	"$this->mVideoID, " .
	 					  	"$travelLogID)";
            $this->mRs->Execute($query);
            
            //added by Jul to create feeds for this video
			require_once("travellog/model/Class.ActivityFeed.php");
			try{
				ActivityFeed::create($this,$this->mParentTravelLog);
			}catch(exception $e){}
            
			$this->invalidateCacheEntry();
	 	}
	 	
	 	/**
	 	 * method that updates an instance of this class
	 	 */
	 	 
	 	public function update() {
	 		$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            
           	$query = "UPDATE tblVideo SET ".
	 					"videoURL 		= '".$this->mVideoUrl."', ".
	 					"videoImageURL 	= '".$this->mVideoImageUrl."', ".
	 					"caption 		= '".addslashes($this->mCaption)."', ".
	 					"dateAdded 		= '".$this->mDateAdded."', ".
	 					"isPrimaryVideo = ".$this->mIsPrimaryVideo.", ".
	 					"duration 		= '".$this->mDuration."', ".
	 					"title 			= '".addslashes($this->mTitle)."' ".
	 				  "WHERE videoID 	= ".$this->mVideoID;
            $this->mRs->Execute($query);
           	$this->invalidateCacheEntry();
	 	}
	 	
	 	/**
	 	 * method that deletes an instance of this class
	 	 */
	 	 
	 	public function delete() {
	 		$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            $query = "DELETE tblTravelLogtoVideo, tblVideo FROM tblVideo " .
	 				 "INNER JOIN tblTravelLogtoVideo ON " .
	 				 "tblTravelLogtoVideo.videoID = tblVideo.videoID " .
	 				 "WHERE tblTravelLogtoVideo.videoID = $this->mVideoID";
			$this->mRs->Execute($query);	
            
			//added by Jul to delete feeds of this video
 			$addedByDayCount = $this->mParentTravelLog->getVideosUploadedByDay(NULL,$this->mDateAdded,TRUE);
			if( 0 == $addedByDayCount ){
				require_once("travellog/model/Class.ActivityFeed.php");	
				$params = array("FEED_SECTION"		=>	ActivityFeed::VIDEO,
								"SECTION_ROOT_ID"	=>	$this->mParentTravelLog->getTravelLogID(),
								"FEED_DATE"			=>	$this->mDateAdded );
				$videoFeeds = ActivityFeed::getInstances($params);
				if( 0 < $videoFeeds["recordCount"] ){
					$videoFeeds["activityFeeds"][0]->Delete();
				}
			}
			$this->invalidateCacheEntry();
	 	}
	
		public function getPlayActionLink(){
			return "/video.php?action=playEntryVideo&videoID=".$this->getVideoID();
		}
		
		public function addComment( Comment $comment ){
			parent::addComment($comment);

			$owner = $this->getParentTravelLog()->getOwner();
			$travelerID = ( $owner instanceof Traveler ) ? $owner->getTravelerID() : $owner->getAdministratorID();
			
			if( $travelerID != $comment->getAuthor() ){
				$comment->setCommentType(CommentType::$VIDEO);
				require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
				RealTimeNotifier::getInstantNotification(RealTimeNotifier::RECEIVED_GREETING, array('COMMENT' => $comment))->send();
			}
		}
		
		public function hasActionPrivilege($traveler_id){
			$owner = $this->getParentTravelLog()->getOwner();

			if ($owner instanceof Traveler) {
				return ($owner->getTravelerID() == $traveler_id);
			}
			else {
				return ($owner->getAdministratorID() == $traveler_id OR $owner->isStaff($traveler_id));
			}			
		}
		
		public function getFriendlyUrl(){
			$travelLog = $this->getParentTravelLog();
			$owner = $travelLog->getOwner();
			
			$link = 'video.php?action=';				
			if( $owner instanceof Traveler ){
				$link .= 'getTravelerVideos&travelerID='.$owner->getTravelerID();
			}
			else if( $owner instanceof Group ){
				$link .= 'getGroupVideos&gID='.$owner->getID();
			}
			
			$link .= '&travelID='.$travelLog->getTravelID().'&travelLogID='.$travelLog->getID().'&videoID='.$this->getVideoID().'&type=journal';
			
			return $link;
		}
	 	
	 	public function invalidateCacheEntry(){
			$refID = 0;
			$refType = '';
			
			$cache = ganetCacheProvider::instance()->getCache();
			
			if (!is_null($cache)) {
				$cache->delete('JournalVideos_'.$this->mParentTravelLog->getTravelID());
		 		$cache->delete('JournalEntryVideos_'.$this->mParentTravelLog->getTravelLogID());
				
				$sql = 'SELECT tblTravelLink.refID, tblTravelLink.refType ' .
					   'FROM tblTravel, tblTravelLog, tblTravelLink ' .
					   'WHERE tblTravel.travelID = tblTravelLog.travelID ' .
					   'AND tblTravel.travelLinkID = tblTravelLink.travelLinkID ' .
					   'AND tblTravelLog.travelLogID = '.$this->mParentTravelLog->getTravelLogID();
				$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
				$this->mRs->Execute($sql);	
			
				while($row = mysql_fetch_assoc($this->mRs->Resultset())) {
					$refID = $row['refID'];
					$refType = $row['refType'];
				}	
				
				if (1 == $refType) {
					$cache->delete('Traveler_Videos_'.$refID);
					$cache->delete('CobrandTraveler_Videos_'.$refID);
				}
				else
					$cache->delete('Group_Videos_'.$refID);	
			}
		}
	}
	
