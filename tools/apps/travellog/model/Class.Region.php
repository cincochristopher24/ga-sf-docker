<?php
/*
 * Created on Jul 28, 2006
 * Author: Kerwin Gordo
 * Purpose: A subclass of Location and represents a Region which is a geographical division inside a country
 */
 
 require_once("gaexception/Class.InstantiationException.php");
 require_once 'travellog/model/Class.City.php';

 class Region extends Location {
 	private $regionID;
 	private $countryID;
 	private $continentID;
 	
 	private $cities;
 	private $regions;
 	private $country;
 	private $continent;
 	
 	private $regionCountries = array();
 	
 	
 	function setRegionID($regionID) {$this->regionID = $regionID;}
 	function getRegionID() {return $this->regionID;}
 	function setCountryID($countryID) {$this->countryID = $countryID;} 	
 	function setContinentID($continentID) {$this->continentID = $continentID;}
 	
 	
 	// overrides
 	
 	  	function getCountries() {return null;}
 	  		
 		function getCities() {
 			if ($this->cities == null) {
 				$rs = mysql_query("select * from GoAbroad_Main.tbcity where regionID='" . $this->regionID . "'" );
 				
 				if (!$rs)
 					return null;
 					
 				
 				$this->cities = array();
 				while($rsar = mysql_fetch_array($rs)) {
 					$city = new City();
 					$city->setName($rsar['name']);
	 				$city->setCityID($rsar['cityID']);
	 				$city->setRegionID($rsar['regionID']);
	 				$city->setCountryID($rsar['countryID']);
	 				$city->setContinentID($rsar['continentID']);	 				
	 				$this->cities[] = city;
 				}	 
 								 			
 			
 			 }
 			 return $this->cities; 
 		} 	 			 			
 		
 		function getRegions() { 
 			
 			if ($this->regions == null) {
 				$rs = mysql_query("select * from GoAbroad_Main.tbregion");
 				
 				while($rsar = mysql_fetch_array($rs)) {
 					$region = new Region();
 					$region->setName($rsar['region']);
	 				$region->setRegionID($rsar['regionID']);
	 				$this->regions[] = $region;
 				}	 
 							
 			} 	
 			
 			return $this->regions;	
 		}
 		
 		function getRegionCountries($list = false,$listType = 'countryID'){
 			$this->regionCountries = array();
 			
			$rs = mysql_query("select * from GoAbroad_Main.tbcountry where regionID = {$this->regionID} AND approved = 1");
			
			while($rsar = mysql_fetch_array($rs)) {
				if( $list )
					$this->regionCountries[] = isset($rsar[$listType]) ? $rsar[$listType] : $rsar['countryID']; 
				else{
					$country = new Country();
	 				$country->setLocationID($rsar['locID']);
	 				$country->setContinentID($rsar['continentID']);
	 				$country->setCountryID($rsar['countryID']);
	 				$country->setName($rsar['country']); 		
	 				$country->regionCountries[] = $country;	
				}
			}
 			
 			return $this->regionCountries;
 		} 	
 			
 		function getNonPopLocations() {
 			
 			 }
 		 	
 		function getRegion() {
 			return $this;
 		}
 		
 		function getCountry() {
 			if ($this->country == null) {
 				$rs = mysql_query("select * from GoAbroad_Main.tbcountry where countryID = '" . $this->countryID . "'");
 				
 				$rsar = mysql_fetch_array($rs);
 				
 				$this->country = new Country();
 				$this->setLocationID($rsar2['locID']);
 				$this->setContinentID($rsar2['continentID']);
 				$this->setCountryID($rsar2['countryID']);
 				$this->setName($rsar2['country']); 					
 			} 	
 			return $this->country;		 			
 			
 		}
 		function getContinent() {
 			if ($this->continent == null) {
	 			$rs = mysql_query("select * from tblContinent where continentID='" . $this->continentID . "'");
	 			$rsar = mysql_fetch_array($rs);
	 			
	 			//print_r($rsar);
	 			//exit();
	 			
	 			$continent = new Continent();
	 			$continent->setLocationID($rsar['locID']);
	 			$continent->setName($rsar['name']);
	 			$continent->setContinentID($rsar['continentID']);
 			}	
 			return $continent;
 			
 		}	
 	
 }
 
 
?>
