<?php

	/**
	 * @author Kerwin Gordo 
	 * Purpose: abstracts the schedule of events for a traveler or a group
	 *
	 */

	require_once("gaexception/Class.InstantiationException.php");
	require_once 'travellog/model/Class.CalendarEvent.php';

	class Calendar
	{ 
		// static fields
		public static $DATE = 1;
		public static $DATE_DESC = 2;
	
		
		private $mStart;						// start date
		private $mEnd;							// end date
		private $mEvents = array();				// an array of CalendarEvents
		private $mContext;						// the owner of the the Calendar (might be a Traveler, FunGroup or AdminGroup)
		private $mGroups = NULL;				//  the groups where mContext belongs if mContext is a traveler -or- the subgroups where mContext belongs if mContext is AdminGroup 
		private $mPrivacyContext = NULL;		// traveler object who is "viewing" this calendar; this is used to limit select the appropriate
											    // calendar events  that are viewable by the traveler
		private $mPCGroups;						// groups where the "viewing" traveler belongs(mPrivacyContext)
		
		private $mRecordOrder;					// ordering of calendar records
		private $mTotalEvents = 0;				// total no. of events retrieved without applying filter
		private $mTries = 1;					// no. of tries on getEvents function
	
		private $subjectGroups = NULL;	
		/**
		 *  constructor
		 *  @param $context the context which this calendar will display. It might be of type Traveler or a subclass of Group
		 */	
		public function Calendar($context) {
			
		
			if (null == $context )
				throw new InstantiationException("cannot instantiate Calendar with null context");
			
			if ( $context instanceof Traveler || $context instanceof Group ) {
				$this->mContext = $context;
								
				if ($context instanceof Traveler ) {										
					$context->setIsSubGroup(true); // added by daf may.23.07 ; the groups of travelers shud be both parent and subgroups
					$groups =$context->getGroups();
					if(array_key_exists('CONFIG',$GLOBALS)){
						if(is_null($groups)) $groups = array();
						$groups = array_merge($groups,GroupFactory::instance()->create(array($GLOBALS['CONFIG']->getGroupID())));
					}
					
					if (NULL != $groups) {
						foreach ($groups as $group) {
							$this->mGroups['' . $group->getGroupID()] = $group;
						}
					}
					$this->getSubjectGroups();	
				} 
			}		
			else 
				throw new InstantiationException("invalid context object");	
								
			$this->mRecordOrder = Calendar::$DATE;					
												
		}
		
		/**
		 * sets who is currently viewing this calendar. This is important since some items in the Calendar
		 *   are not to be viewed by everyone
		 * @param $traveler the Traveler object who is currently logged in 
		 */
		public function setPrivacyContext($traveler) {
			$this->mPrivacyContext = $traveler;
			$traveler->setIsSubGroup(true); // added by daf may.23.07 ; the groups of travelers shud be both parent and subgroups
			$this->mPCGroups = $traveler->getGroups();
		}
		
		/**
		 * filter the date range to include in the Calendar
		 * @param $start a date object signifying the beginning date
		 * @param $end   a date object signifying the enddate
		 */
		public function setDateRange($start,$end) {
			$this->mStart = $start;
			$this->mEnd = $end;		
		}
		
		/**
		 * sets the ordering of the calendar events
		 * @param $order a integer signifying how to order the events (Calendar::$DATE and Calendar::$DATE_DESC)
		 */
		public function setOrder($order) {
			if (Calendar::$DATE == $order || Calendar::$DATE_DESC == $order)
				$this->mRecordOrder = $order;
		}
		
		/**
		 * returns the groups which the current context belongs
		 * @return array array of Groups of Traveler or Subgroups of an AdminGroup
		 */
		public function getContextGroups() {
			return $this->mGroups;
		}
		
		/**
		 * function to get the groupevents and program activities of a traveler or a group
		 * @param startdate , the  starting date of calendar events that will be retrieved from the calendar
		 * @return array CalendarEvent objects
		 */
		public function getEvents($limit = NULL, $startDate = null) {
			
			$this->mEvents = array();								
				
			if ($this->mContext instanceof FunGroup) {
				$this->getGroupEvents($this->mContext,$limit,$startDate);
				$this->mTotalEvents = count($this->mEvents);				
				// remove records which should not be seen by the current user
				$i = 0;
				foreach ($this->mEvents as $event){
					if (!$this->showEvent($event))
						unset($this->mEvents[$i]);
					$i++;	
				}													
			}
			
			if ($this->mContext instanceof AdminGroup) {
				$this->getAdminGroupEvents($this->mContext,$limit,$startDate);
				$this->mTotalEvents = count($this->mEvents);
				// remove records which should not be seen by the current user
				$i = 0;
				foreach ($this->mEvents as $event){
					if (!$this->showEvent($event))
						unset($this->mEvents[$i]);
					$i++;	
				}				
			}
							
			
			if ($this->mContext instanceof Traveler) {
				
				// get events for this traveler
				$this->getTravelerEvents($this->mContext,null,$startDate);
				$this->mTotalEvents = count($this->mEvents);							
				
				// get events for the groups of this traveler
				//$this->mContext->setIsSubGroup(true); // added by daf may.23.07 ; the groups of travelers shud be both parent and subgroups
				$groups = $this->mContext->getGroups();				
				
				if(array_key_exists('CONFIG',$GLOBALS)){
					if(is_null($groups)) $groups = array();
					$groups = array_merge($groups,GroupFactory::instance()->create(array($GLOBALS['CONFIG']->getGroupID())));
				}
				if (NULL != $groups){				
					foreach($groups as $group){
						// note: for getting activities of a traveler we don't supply a rows limit parameter since a traveler
						// 		 might belong to multiple groups instead after calling getGroupEvents/getCalendarActivities 
						// 		 on the groups the traveler belongs and aggregating it, it is the time to use the Rows Limit		
										
						if ($group instanceof FunGroup) {
							$this->getGroupEvents($group,null,$startDate);
							// remove records which should not be seen by the current user
							$i = 0;
							foreach ($this->mEvents as $event){
								if (!$this->showEvent($event))
									unset($this->mEvents[$i]);
								$i++;	
							}			
							
						}
						
						if ($group instanceof AdminGroup) {
							$group->setTravelerOwned(true);
							$this->getAdminGroupEvents($group,null,$startDate);
							// remove records which should not be seen by the current user
							$i = 0;
							foreach ($this->mEvents as $event){
								if (!$this->showEvent($event))
									unset($this->mEvents[$i]);
								$i++;	
							}		
							
						}					
					}
				}
				
				if (0 == count($this->mEvents))
				  return array();
								
								
				// then we sort the results 								
				if (Calendar::$DATE  == $this->mRecordOrder) {				
					usort($this->mEvents,array("CalendarEvent","orderByDate"));									
				}
				
				if (Calendar::$DATE_DESC  == $this->mRecordOrder) {
					usort($this->mEvents,array("CalendarEvent","orderByDate"));
					$this->mEvents = array_reverse($this->mEvents);
				}
											
				
						
				
				// recreate the array w/o a key (this will be used to manipulate the array if there is a row limit)
				$events = array();
				foreach($this->mEvents as $event){
					$events[] = $event;
				}
				$this->mEvents = $events;
				
				// now we apply the rows limit if there is any				
				if (NULL != $limit) {
				
					if ($limit->getOffset() < count($this->mEvents)) {
						$events = array();
						for($i=$limit->getOffset();$i < count($this->mEvents) && $i < $limit->getRows() ;$i++) {
							$events[] = $this->mEvents[$i];
						}
						$this->mEvents = $events;						
					} else {
						return array();				// we return an empty array since the offset is over the array range
					}					
				}
				
				
				// now we put the group name on the title
				foreach ($this->mEvents as $event){
					if (CalendarEvent::$ACTIVITY == $event->getContext() || CalendarEvent::$GROUP_EVENT == $event->getContext()) {
						$group = $this->mGroups['' . $event->getContextID()];
						$event->setTitle($event->getTitle() . '  [' . $group->getName() . ']' );						
					}
				}
																				
			}										
			
			if (0 < count($this->mEvents))														
				return $this->mEvents;										
			elseif (0 == count($this->mEvents) && 2 == $this->mTries)
				return $this->mEvents;							// there are no events
				
				
			// we have zero results (caused by filtering) so we just get what events are available (without a date filter)
			$this->mTries++;
			$tempTotalEvents = $this->mTotalEvents;    // we put mTotalEvents value in a temporary variable because we have to set it to zero and call getEvents again
			$tempRecordOrder = $this->mRecordOrder; 	// put also record order in a temporary variable
			$this->mTotalEvents = 0;					// set to zero to avoid possibility of infinite loop
			
				//-we call this function again w/o date param and with descending date order//
				$this->mRecordOrder = Calendar::$DATE_DESC;  
				$events = $this->getEvents();
				//- reset back the temp variables
				$this->mTotalEvents = $tempTotalEvents;
				$this->mRecordOrder = $tempRecordOrder;
								
			return $events;	 	
			
		}
		
		/**
		 * @return boolean tells whether getEvents function has been called recursively (2 times) due to filtering issues (refer to getEvents function)
		 * 		Note: must be called only after calling getEvents
		 */
		public function hasRetriedGetEvents(){
			if (2 == $this->mTries){
				// reset no. of tries to 1
				$this->mTries = 1;
				return true;
			}	
			return false;			
		} 
		
		/**
		 * get the total no. of calendar events
		 * @return integer no. of calendar events
		 */
		function getTotalCalendarEvents()
		{
			return $this->mTotalEvents;
		}
		
		/**
		 * helper function to get the events of a FunGroup
		 * @return array array of CalendarEvent
		 */
		private function getGroupEvents($context,$limit = NULL,$startDate) {
			$gevents = $context->getGroupEvents($limit,$startDate);
				
			// loop the $gevents and create CalendarEvent object and group into $this->mEvents array
			if (NULL != $gevents) {					
				foreach($gevents as $gevent) {
					$ce = new CalendarEvent($gevent->getGroupEventID(), $gevent->getTitle(),$gevent->getTheDate(),$gevent->getDescription(),$gevent->getLink(),$gevent->getContext(),$gevent->getGroupID(),$gevent->getDisplayPublic());
					$this->mEvents[] = $ce;					
				}					
			}			
		}
		
		/**
		 * helper function to get the events of an AdminGroup. This includes the Group events in general
		 * and the activities for programs 
		 */
		private function getAdminGroupEvents($context,$limit = NULL,$startDate) {
			$calendarEvents = $context->getCalendarActivities($limit,$startDate);
			
			if (count($calendarEvents) > 0)
				$this->mEvents = array_merge($this->mEvents,$calendarEvents);
				
		}
		
		private function getTravelerEvents($traveler,$limit = NULL,$startDate) {
			$tevents = $traveler->getTravelerEvents($limit,$startDate);
			
			if (NULL != $tevents){
				foreach ($tevents as $tevent) {
					$ce = new CalendarEvent($tevent->getTravelerEventID(),$tevent->getTitle(),$tevent->getTheDate(),$tevent->getDescription(),$tevent->getLink(),CalendarEvent::$TRAVELER_EVENT,$tevent->getTravelerID(),$tevent->getDisplayPublic());
					$ce->setTimeZoneID($tevent->getTimeZoneID());
					$this->mEvents[] = $ce;
				}
			}		
			
		}
		
		private function getSubjectGroups(){
			if($this->mContext instanceOf Traveler)
				$this->subjectGroups = $this->mContext->getGroups();

			if(SiteContext::getInstance()->isInCobrand()){
				$the_cobrand = GroupFactory::instance()->create(array(SiteContext::getInstance()->getGroupID()));
				$this->subjectGroups = array_merge($this->subjectGroups,array($the_cobrand[0]));
			}	
		}
		
		/**
		 * helper function to show a calendar event or not based on the privacy preference of the owner of the CalendarEvent
		 * @param CalendarEvent $calEvent is the CalendarEvent object to show or not
		 * @return boolean true if to be displayed, false otherwise
		 */
		private function showEvent($calEvent){
			if (1 == $calEvent->getDisplayPublic()){				// display to public 	
				if( $calEvent->getContext() == CalendarEvent::$GROUP_EVENT ){
					if (NULL != $this->subjectGroups){
						foreach($this->subjectGroups as $sgroup) {
							if ($calEvent->getContextID() == $sgroup->getGroupID()){
								if($sgroup->getPrivacyPreference()->getViewCalendar()){
									return true;	
								}else{
									if(NULL != $this->mPrivacyContext){
										if($sgroup->isMember($this->mPrivacyContext))
											return true;
									}	
								}
							}		
						}
					}else return true;	
				}else return true;
			}elseif (2 == $calEvent->getDisplayPublic() || 3 == $calEvent->getDisplayPublic() ) {		// display to friends or groupmates
				
				// traveler context
				if ($this->mContext instanceof Traveler && NULL != $this->mPrivacyContext ){
					if ($this->mPrivacyContext instanceof Traveler){
						// is a friend
						if ($this->mContext->isFriend($this->mPrivacyContext->getTravelerID()) && ($calEvent->getContext() == CalendarEvent::$TRAVELER_EVENT)){
							return true;
						}
						
						if( $calEvent->getContext() == CalendarEvent::$GROUP_EVENT ){
							if (NULL != $this->mPCGroups){
								foreach($this->mPCGroups as $group) { 
									if ($calEvent->getContextID() == $group->getGroupID())
										return true;
								}
							}
						}
						
						// logged user is the subject
						if ($this->mPrivacyContext->getTravelerID() == $this->mContext->getTravelerID())
							return true; 							
						
					}
				}
				// group context
				if ($this->mContext instanceof FunGroup  || $this->mContext instanceof AdminGroup ) {
						if (NULL != $this->mPCGroups){
							foreach($this->mPCGroups as $group) { 
								if ($calEvent->getContextID() == $group->getGroupID())
									return true;
								if ($this->mContext->getAdministratorID() == $this->mPrivacyContext->getTravelerID())
									return true;
							}
						}													
				}
				
			} 
			else {
				if (NULL != $this->mPrivacyContext ) {
					if ($this->mContext instanceof Traveler) {					
						if ($this->mPrivacyContext->getTravelerID() == $this->mContext->getTravelerID())
							return true; 							
					}	
					/*if ('FunGroup' == get_class($this->mContext)  || 'AdminGroup' == get_class($this->mContext) ) {
						if (NULL != $this->mPCGroups){
							foreach($this->mPCGroups as $group) { 
								if ($calEvent->getContextID() == $group->getGroupID())
									return true;
							}
						}													
					}*/
				}				
			}
								
			return false;	
		}
		
		
		
			
	}

?>
