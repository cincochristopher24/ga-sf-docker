<?php
	/**
	 * @(#) Class.BaseCountryPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 4, 2009
	 */
	
	require_once("Class.ResourceIterator.php");
	require_once("Class.ObjectRepository.php");
	require_once("travellog/model/Class.GanetDbHandler.php");
	
	/**
	 * Base class for country peer
	 */
	class BaseCountryPeer {
	
		/**
		 * Returns the number of countries that satisfies the given criteria.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by, group by and where clauses.
		 * 
		 * @return integer the number of countries that satisfies the given criteria.
		 */
		static function doCount(Criteria2 $criteria){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "SELECT COUNT(*) as count FROM GoAbroad_Main.tbcountry ";
				$sql .= $criteria->createWhereClause();
				$resource = new ResourceIterator($handler->execute($sql));
				
				return $resource->getCount();
			}
			catch (exception $ex) {}
		}
		
		/**
		 * Returns the countries that satisfies the given criteria.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by, group by and where clauses.
		 * 
		 * @return array the countries that satisfies the given criteria.
		 */
		static function doSelect(Criteria2 $criteria){
			try {
				$handler = GanetDbHandler::getDbHandler();
				$sql = "SELECT * from GoAbroad_Main.tbcountry ";
				$sql .= $criteria->createSqlClause();
				
				return new ResourceIterator($handler->execute($sql), "Country");
			}
			catch (exception $ex) {
				echo $ex->getMessage(); exit;
			}
		}
		
		/**
		 * Returns the country with ID equal to the given country ID.
		 * 
		 * @param integer $id The id of the country to be retrieved.
		 * 
		 * @return Country|null the country with ID equal to the given country ID; null if the ID does not exists.
		 */
		static function retrieveByPk($id){
			$repo = ObjectRepository::instance();			
			
			if (!$repo->objectExists("Country", $id)) {
				require_once("Class.Criteria2.php");
				
				$criteria = new Criteria2();
				$criteria->add(CountryPeer::COUNTRY_ID, $id);
				$criteria->add(CountryPeer::APPROVED, 1);
				$countries = self::doSelect($criteria);
				
				if ($countries->valid()) {
					$repo->storeObject($countries->current(), $id);
				}
			}
			
			return $repo->retrieveObject("Country", $id);
		}	
	
	}
	
