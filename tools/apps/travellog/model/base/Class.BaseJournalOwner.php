<?php
	/**
	 * @(#) Class.BaseJournalOwner.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 28, 2009
	 */
	
	require_once("travellog/model/Class.GanetDbHandler.php");
	
	/**
	 * Base class of journal owner.
	 */
	abstract class BaseJournalOwner {
	 
		/**
		 * The travellink ID of the journal owner.
		 * 
		 * @var integer 
		 */
		protected $mTravellinkID = 0;
	 
		/**
		 * The ref ID of the journal owner.
		 * 
		 * @var integer 
		 */
		protected $mRefID = 0;
	 
		/**
		 * The reftype of the journal owner.
		 * 
		 * @var integer 
		 */
		protected $mReftype = 0;
			  
		/**
		 * Deletes the journal owner in the database.
		 *
		 * @return void
		 */
		function delete(){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "DELETE FROM tblTravelLink WHERE `travellinkID` = ".$handler->makeSqlSafeString($this->mTravellinkID);
				
				$handler->execute($sql);
			}
			catch(exception $ex){}
		}
	 
		/**
		 * Returns the travellink ID of the journal owner.
		 * 
		 * @return integer the travellink ID of the journal owner.
		 */
		function getTravellinkID(){
			return $this->mTravellinkID;
		}
		
		/**
		 * Returns the reference object of this travel link.
		 * 
		 * @return Traveler|AdminGroup the reference object of this travel link.
		 */
		function getReferenceObject(){
			return (1 == $this->mReftype) ? new Traveler($this->mRefID) : new AdminGroup($this->mRefID);
		}
	 
		/**
		 * Returns the ref ID of the journal owner.
		 * 
		 * @return integer the ref ID of the journal owner.
		 */
		function getRefID(){
			return $this->mRefID;
		}
	 
		/**
		 * Returns the reftype of the journal owner.
		 * 
		 * @return integer the reftype of the journal owner.
		 */
		function getReftype(){
			return $this->mReftype;
		}	
	  	
		/**
		 * Sets the properties of the journal owner.
		 * 
		 * @param array $props The new values for the properties of JournalOwner.
		 * 
		 * @return void
		 */
		function initialize(array $props){
			if (0 < count($props)) {
				$this->mTravellinkID = isset($props['travellinkID']) ? $props['travellinkID'] : $this->mTravellinkID;
				$this->mRefID = isset($props['refID']) ? $props['refID'] : $this->mRefID;
				$this->mReftype = isset($props['reftype']) ? $props['reftype'] : $this->mReftype;
			}
		}
			
		/**
		 * Inserts/Updates a journal owner in the database. When the travellinkID is
		 * equal to 0 this inserts a new entry in the database, otherwise this updates
		 * an existent entry.
		 *
		 * @return void
		 */
		function save(){
			try {
				$handler = GanetDbHandler::getDbHandler();
  			
				if (0 == $this->mTravellinkID) {
					$sql = "INSERT INTO tblTravelLink (`refID`, `reftype`)".
						" VALUES (". 
						"".$handler->makeSqlSafeString($this->mRefID).",".
						"".$handler->makeSqlSafeString($this->mReftype). 											
						")";
				}
				else {
					$sql = "UPDATE tblTravelLink SET ".
						" `refID` = ".$handler->makeSqlSafeString($this->mRefID).",".
						" `reftype` = ".$handler->makeSqlSafeString($this->mReftype).
						" WHERE `travellinkID` = ".$handler->makeSqlSafeString($this->mTravellinkID);			
				}
				
				$handler->execute($sql);
				$this->mTravellinkID = (0 == $this->mTravellinkID) ? $handler->getLastInsertedID():$this->mTravellinkID;
			}
			catch(exception $ex){}
		}
	 
		/**
		 * Sets the travellink ID of the journal owner.
		 * 
		 * @param integer $travellinkID the travellink ID of the journal owner.
		 */
		function setTravellinkID($travellinkID){
			$this->mTravellinkID = $travellinkID;
		}	
	 
		/**
		 * Sets the ref ID of the journal owner.
		 * 
		 * @param integer $refID the ref ID of the journal owner.
		 */
		function setRefID($refID){
			$this->mRefID = $refID;
		}	
	 
		/**
		 * Sets the reftype of the journal owner.
		 * 
		 * @param integer $reftype the reftype of the journal owner.
		 */
		function setReftype($reftype){
			$this->mReftype = $reftype;
		}	
		
	}