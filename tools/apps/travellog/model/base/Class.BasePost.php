<?php  
	/**
	 * @(#) Class.BasePost.php
	 * 
	 * Autogenerated by GoAbroad Code Generator 1.0
	 *
	 * @version 1.0 Jun 19, 2009 
	 */
	
	require_once('travellog/model/Class.DiscussionBoardDbHandler.php');
	require_once('travellog/model/Class.PostPeer.php');
	
	/**
	 * Base class of post.
	 */
	abstract class BasePost {
	 
		/**
		 * The id of the post.
		 * 
		 * @var int 
		 */
		protected $mId = null;
	 
		/**
		 * The discussion id of the post.
		 * 
		 * @var int 
		 */
		protected $mDiscussionId = 0;
	 
		/**
		 * The poster id of the post.
		 * 
		 * @var int 
		 */
		protected $mPosterId = 0;
	 
		/**
		 * The message of the post.
		 * 
		 * @var string 
		 */
		protected $mMessage = '';
	 
		/**
		 * The date created of the post.
		 * 
		 * @var string 
		 */
		protected $mDateCreated = '0000-00-00 00:00:00';
		 
		/**
		 * The related discussion of the post.
		 * 
		 * @var Discussion 
		 */
		protected $mDiscussion = null;
	
		/**
		 * Deletes the post in the database.
		 *
		 * @return boolean Returns true on success and false on failure.
		 */
		function delete(){
			return PostPeer::doDelete(array($this));
		}
		
		/**
		 * Returns true if the object exists in the database.
		 * 
		 * @return boolean true if the object exists in the database. Otherwise, this returns false.
		 */
		function exists(){
			return (is_null($this->getPrimaryKey())) ? false : true;
		}
	 
		/**
		 * Returns the id of the post.
		 * 
		 * @return int The id of the post.
		 */
		function getId(){
			return $this->mId;
		}	
	 
		/**
		 * Returns the discussion id of the post.
		 * 
		 * @return int The discussion id of the post.
		 */
		function getDiscussionId(){
			return $this->mDiscussionId;
		}	
	 
		/**
		 * Returns the poster id of the post.
		 * 
		 * @return int The poster id of the post.
		 */
		function getPosterId(){
			return $this->mPosterId;
		}	
	 
		/**
		 * Returns the message of the post.
		 * 
		 * @return string The message of the post.
		 */
		function getMessage(){
			return $this->mMessage;
		}	
	 
		/**
		 * Returns the date created of the post.
		 * 
		 * @return string The date created of the post.
		 */
		function getDateCreated($format = 'Y-m-d H:i:s'){
			return date($format, strtotime($this->mDateCreated));
		}	
		 
		/**
		 * Returns the related discussion of the post.
		 * 
		 * @return Discussion The related discussion of the post.
		 */
		function getDiscussion(){
			if (is_null($this->mDiscussion) OR $this->mDiscussion->getPrimaryKey() != $this->mDiscussionId) {
				require_once('travellog/model/Class.DiscussionPeer.php');
				
				$this->mDiscussion = DiscussionPeer::retrieveByPk($this->mDiscussionId);
			}
			
			return $this->mDiscussion;
		}	
		
		/**
		 * Sets the related discussion of the post.
		 * 
		 * @param Discussion $val The related discussion of the post.
		 *
		 * @return void
		 */
		function setDiscussion(Discussion $val){	
			$this->mDiscussion = $val;
		}
		
		/**
		 * Returns the primary key of post.
		 *
		 * @return integer the primary key of post.
		 */
		function getPrimaryKey(){
			return $this->getId();
		}
		
		/**
		 * Populates the properties of the post.
		 * 
		 * @param array  $props The new values for the properties of post.
		 * @param string $type  The type of field keys to be retrieve with possible values equal to GanetBasePeer::TYPE_TABLEFIELD, GanetBasePeer::TYPE_PEERFIELD, GanetBasePeer::TYPE_PHPFIELD and GanetBasePeer::TYPE_NUM.  
		 *
		 * @return void
		 */
		function fromArray(array $props, $type = GanetBasePeer::TYPE_TABLEFIELD){
			$keys = PostPeer::getFieldNames($type);
			$props[$keys[0]] = $this->setId($props[$keys[0]]);
			$props[$keys[1]] = $this->setDiscussionId($props[$keys[1]]);
			$props[$keys[2]] = $this->setPosterId($props[$keys[2]]);
			$props[$keys[3]] = $this->setMessage($props[$keys[3]]);
			$props[$keys[4]] = $this->setDateCreated($props[$keys[4]]);
		}
		
		/**
		 * Inserts/Updates a post in the database. When the id is
		 * equal to 0 this inserts a new entry in the database, otherwise this updates
		 * an existent entry.
		 *
		 * @return boolean Returns true on success and false on failure. If this cannot insert and update, this returns false.
		 */
		function save(){			
			if (!$this->exists()) {
				//return PostPeer::doInsert(array($this));
				return PostPeer::doInsertOne($this);
			}
			else {
				return PostPeer::doUpdate(array($this));	
			}
		}
 
		/**
		 * Sets the id of the post.
		 * 
		 * @param int $val the id of the post.
		 *
		 * @return void
		 */
		function setId($val){
			if ($val !== null) {
				$this->mId = $val;
			}		
		}
 
		/**
		 * Sets the discussion id of the post.
		 * 
		 * @param int $val the discussion id of the post.
		 *
		 * @return void
		 */
		function setDiscussionId($val){
			if ($val !== null) {
				$this->mDiscussionId = $val;
			}		
		}
 
		/**
		 * Sets the poster id of the post.
		 * 
		 * @param int $val the poster id of the post.
		 *
		 * @return void
		 */
		function setPosterId($val){
			if ($val !== null) {
				$this->mPosterId = $val;
			}		
		}
 
		/**
		 * Sets the message of the post.
		 * 
		 * @param string $val the message of the post.
		 *
		 * @return void
		 */
		function setMessage($val){
			if ($val !== null) {
				$this->mMessage = $val;
			}		
		}
 
		/**
		 * Sets the date created of the post.
		 * 
		 * @param string $val the date created of the post.
		 *
		 * @return void
		 */
		function setDateCreated($val){
			if ($val !== null) {
				$this->mDateCreated = $val;
			}		
		}
	
		/**
		 * Sets the primary key of post.
		 *
		 * @param integer the primary key of post.
		 * 
		 * @return void
		 */
		function setPrimaryKey($val){
			return $this->setId($val);
		}

		/**
		 * Returns the properties of the post.
		 *
		 * @param string $type The type of field keys to be retrieve with possible values equal to GanetBasePeer::TYPE_TABLEFIELD, GanetBasePeer::TYPE_PEERFIELD, GanetBasePeer::TYPE_PHPFIELD and GanetBasePeer::TYPE_NUM.
		 *
		 * @return mixed[] the properties of the post.
		 */
		function toArray($type = GanetBasePeer::TYPE_TABLEFIELD){
			$keys = PostPeer::getFieldNames($type);
			$props = array();
			$props[$keys[0]] = $this->getId();
			$props[$keys[1]] = $this->getDiscussionId();
			$props[$keys[2]] = $this->getPosterId();
			$props[$keys[3]] = $this->getMessage();
			$props[$keys[4]] = $this->getDateCreated();

			return $props;
		}

	}