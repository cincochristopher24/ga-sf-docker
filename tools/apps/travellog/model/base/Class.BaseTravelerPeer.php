<?php
	/**
	 * @(#) Class.BaseTravelerPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 1, 2009
	 */
	 
	require_once("Class.ResourceIterator.php");
	require_once("travellog/model/Class.GanetDbHandler.php");
	require_once("Class.Criteria2.php");
	
	/**
	 * Base class of TravelerPeer
	 */
	abstract class BaseTravelerPeer {
		
		/** The database name of this class **/
		const DATABASE_NAME = 'Travel_Logs';
		
		/** The table name of this class **/
		const TABLE_NAME = 'tblTraveler';
		
		/** The primary field of this class **/
		const PRIMARY_KEY = 'travelerID';
		
		/** The id of the traveler. **/
		const TRAVELER_ID = 'tblTraveler.travelerID';
		
		/** The username of the traveler **/
		const USERNAME = 'tblTraveler.username';
		
		/** The password of the traveler **/
		const PASSWORD = 'tblTraveler.password';
		
		const SENDABLE_ID = "tblTraveler.sendableID";
		const DATE_REGISTERED = "tblTraveler.dateregistered";
		const IS_SUSPENDED = "tblTraveler.isSuspended";
		const ACTIVE = "tblTraveler.active";
		const VIEWS = "tblTraveler.views";
		const CURRENT_LOCATION_ID = "tblTraveler.currlocationID";
		const LATEST_LOGIN = "tblTraveler.latestlogin";
		
		/**
		 * The instances of the Traveler
		 * 
		 * @var Traveler[]
		 */
		public static $sInstances = array();
		
		/**
		 * Returns the number of travelers of ganet that satisfies the given criteria.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return integer the number of travelers of ganet that satisfies the given criteria.
		 */
		static function doCount(Criteria2 $criteria){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "SELECT COUNT(*) as count FROM tblTraveler ";
				$sql .= $criteria->createWhereClause();
				$resource = new ResourceIterator($handler->execute($sql));
				
				return $resource->getCount();
			}
			catch (exception $ex) {}
		}
		
		/**
		 * Returns the travelers that satisfies the given criteria.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return array the travelers that satisfies the given criteria.
		 */
		static function doSelect(Criteria2 $criteria){
			require_once('travellog/model/Class.Traveler.php');
			
			$data = array();
			try {
				$handler = GanetDbHandler::getDbHandler();
				$sql = "SELECT * FROM tblTraveler ";
				$sql .= $criteria->createSqlClause();
				
				$resource = $handler->execute($sql);
				while($row = mysql_fetch_assoc($resource)) {
					if (!isset(self::$sInstances[$row[self::PRIMARY_KEY]])) {
						$obj = new Traveler();
						$obj->initialize($row);
						self::$sInstances[$obj->getTravelerID()] = $obj; 
					}			
					$data[] = self::$sInstances[$row[self::PRIMARY_KEY]];
				}
			}
			catch (exception $ex) {}
			
			return $data;
		}
		
		/**
		 * Returns the traveler with ID equal to the given traveler ID.
		 * 
		 * @param integer $id The id of the traveler to be retrieved.
		 * 
		 * @return Traveler|null the traveler with ID equal to the given traveler ID; null if the ID does not exists.
		 */
		static function retrieveByPk($id){
			if (isset(self::$sInstances[$id])) {
				return self::$sInstances[$id];
			}
						
			$data = self::retrieveByPks(array($id));
			
			return (0 < count($data)) ? $data[0] : null;
		}
		
		/**
		 * Returns the Traveler instances of the given ids.
		 * 
		 * @param integer[] $ids The ids to be fetched its objects.
		 * 
		 * @return Traveler[] The Traveler instances of the given ids.
		 */
		static function retrieveByPks(array $ids){
			$objects = array();
			$to_be_queried = array();
			
			foreach ($ids as $id) {
				if (isset(self::$sInstances[$id])) {
					$objects[] = self::$sInstances[$id];
				}
				else { 
					$to_be_queried[] = $id;
				}
			}
			
			if (0 < count($to_be_queried)) {
				$criteria = new Criteria2();
				$criteria->add(self::TABLE_NAME.'.'.self::PRIMARY_KEY, implode($to_be_queried, ','), Criteria2::IN);
				$data = self::doSelect($criteria);
				$objects = array_merge($objects, $data);
			}
			
			return $objects;
		}
		
	}