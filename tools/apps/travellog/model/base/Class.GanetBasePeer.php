<?php   
	/**
	 * @(#) Class.GanetBasePeer.php
	 *
	 * @version 1.0 December 23, 2009
	 */

	/**
	 * Base peer class.
	 * renamed BasePeer to GanetBasePeer to resolve issues with propel's BasePeer
	 */
	abstract class GanetBasePeer {
	
		/**
		 * column (peer) name type
		 * e.g. Book::AUTHOR_ID
		 */
		const TYPE_PEERFIELD = 'peerField';
	
		/**
		 * table field name
		 * e.g. 'author_id'
		 */
		const TYPE_TABLEFIELD = 'tablefield';
	
		/**
		 * num type
		 * simply the numerical array index, e.g. 4
		 */
		const TYPE_NUM = 'num';
	
	}

