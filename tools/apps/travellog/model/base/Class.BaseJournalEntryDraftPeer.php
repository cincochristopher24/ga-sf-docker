<?php
	/**
	 * @(#) Class.BaseJournalEntryDraftPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 24, 2009
	 */
	
	require_once("travellog/model/Class.GanetDbHandler.php");
	
	/**
	 * Base class of JournalEntryDraft peer
	 */
	abstract class BaseJournalEntryDraftPeer {
	 
		/** The callout of the journal entry draft. */
		const CALLOUT = "tblJournalEntryDraft.callout";
	 
		/** The city ID of the journal entry draft. */
		const CITY_ID = "tblJournalEntryDraft.cityID";
	 
		/** The country ID of the journal entry draft. */
		const COUNTRY_ID = "tblJournalEntryDraft.countryID";
	 
		/** The description of the journal entry draft. */
		const DESCRIPTION = "tblJournalEntryDraft.description";
	 
		/** The is auto saved of the journal entry draft. */
		const IS_AUTO_SAVED = "tblJournalEntryDraft.isAutoSaved";
	 
		/** The journal entry draft ID of the journal entry draft. */
		const JOURNAL_ENTRY_DRAFT_ID = "tblJournalEntryDraft.journalEntryDraftID";
	 
		/** The log date of the journal entry draft. */
		const LOG_DATE = "tblJournalEntryDraft.logDate";
	 
		/** The title of the journal entry draft. */
		const TITLE = "tblJournalEntryDraft.title";
	 
		/** The travel ID of the journal entry draft. */
		const TRAVEL_ID = "tblJournalEntryDraft.travelID";
		
		/**
		 * Returns the number of journal entry draft.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return integer the number of journal entry draft.
		 */
		static function doCount(Criteria2 $criteria){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "SELECT COUNT(*) as count FROM tblJournalEntryDraft ";
				$sql .= $criteria->createWhereClause();

				$resource = $handler->execute($sql);
				$record = mysql_fetch_assoc($resource);
				
				return $record['count'];
			}
			catch (exception $ex) {}
			
			return 0;
		}
		
		/**
		 * Returns all the journal entry draft that satifies the criteria.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return array the journal entry draft that satifies the criteria.
		 */
		static function doSelect(Criteria2 $criteria){
			require_once("travellog/model/Class.JournalEntryDraft.php");
			
			$data = array();
			try {
				$handler = GanetDbHandler::getDbHandler();

				$sql = "SELECT * FROM tblJournalEntryDraft ";
				$sql .= $criteria->createSqlClause();

				$resource = $handler->execute($sql);
				
				while ($row = mysql_fetch_assoc($resource)) {
					$obj = new JournalEntryDraft();
					$obj->initialize($row);
					$data[] = $obj;
				}			
			}
			catch (exception $ex) {}
			
			return $data;
		}
		
		/**
		 * Returns the journal entry draft with ID equal to the given journal entry draft ID.
		 * 
		 * @param integer $id The id of the journal entry draft to be retrieved.
		 * 
		 * @return JournalEntryDraft|null the journal entry draft with ID equal to the given journal entry draft ID; null if the ID does not exists.
		 */
		static function retrieveByPk($id){
			require_once("Class.Criteria2.php");
			
			$handler = GanetDbHandler::getDbHandler();
			
			$criteria = new Criteria2();
			$criteria->add(self::JOURNAL_ENTRY_DRAFT_ID, $handler->makeSqlSafeString($id));
			$data = self::doSelect($criteria);
			
			return (0 < count($data)) ? $data[0] : null;
		}
	
	}