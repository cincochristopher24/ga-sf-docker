<?php
/*
 * Created on Aug 8, 2006
 *
 * Created by: Czarisse Daphne P. Dolina
 */

	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.Travel.php");
	require_once("travellog/model/Class.Traveler.php");
	
	 
	class Featured{
		
		private $featphoto 	    = array();
		private $feattravel 	= array();
		private $feattraveler	= array();
		private $featgroup	    = array();
		
		private static $arrtravelog	= array();
		private static $approvedParents = array();
		
		/**
		 ********* PHOTO FUNCTIONS *************
		 */
		 
		public static function getRandomPhoto($numrows = 1){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$sqlcount = "SELECT COUNT( distinct tblTravel.travelerID) AS totalrec " .
						"FROM tblTravel, tblTravelLog, tblTravelLogtoPhoto " .
						"WHERE tblTravel.travelID = tblTravelLog.travelID " .
						"AND tblTravel.deleted = 0 ".
						"AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID ";
 			$rs->Execute($sqlcount);
 			
 			if ($rs->Result(0,"totalrec") == 0){
				try {
					return NULL;
		 		}
				catch (Exception $e) {				   
				   throw $e;
				}
			}
			
			else {
				
				$listtraveler = "";
				$filterstr = "";
				
				$arrtrav = array();
				
				if ($rs->Result(0,"totalrec") < $numrows)
					$numrows = $rs->Result(0,"totalrec");
					
				$max = $rs->Result(0,"totalrec") - 1;
				$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
				
				while (count($arrtrav) < $numrows){
					
					// get the current recordcount with filter
						if (count($arrtrav)){
							$listtraveler = implode(',',$arrtrav);
							$filterstr = "AND tblTravel.travelerID NOT IN ($listtraveler) ";
						}
						
						$sqlcount = $sqlcount . $filterstr  ;
	 					$rs->Execute($sqlcount);
	 			
						$max = $rs->Result(0,"totalrec") - 1;
						$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
					
					// get the random record
					$sql = "SELECT tblTravel.travelerID, tblTravelLogtoPhoto.photoID, tblTravelLogtoPhoto.travellogID " .
							"FROM tblTravel, tblTravelLog, tblTravelLogtoPhoto " .
							"WHERE tblTravel.travelID = tblTravelLog.travelID " .
							"AND tblTravel.deleted = 0 ".
							"AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID " . $filterstr .
							"ORDER BY tblTravelLogtoPhoto.photoID LIMIT " . $randidx . " , 1" ;
					$rs->Execute($sql);
 					
					try {
		 				$travellog = new TravelLog($rs->Result(0,"travellogID"));
		 				$featphoto[] = new Photo($travellog, $rs->Result(0,"photoID"));	
		 				
		 				Featured::$arrtravelog[] = $travellog;
		 			}
					catch (Exception $e) {				   
					   throw $e;
					} 	
				
 					$arrtrav[] = $rs->Result(0,"travelerID"); 		// puts selected index in array; value should be unique;
	 				
				}
				
			}
 			
 			return $featphoto;
			
		}
		
		
		public static function setPhotoFeatured($_photoID, $_status = true){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			if (false == $_status)
				$status = 0;
			else
				$status = 1;
				 
			// update photo status if featured or not
			$sql = "UPDATE tblTravelLogtoPhoto SET isfeatured = ". $status . " WHERE photoID = " . $_photoID ;
			$rs->Execute($sql);
			
		}
		
		
		public static function isFeaturedPhoto($_photoID){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			// check if given photo is featured
			$sql = "SELECT photoID FROM tblTravelLogtoPhoto WHERE isfeatured = 1 AND photoID = " . $_photoID ;
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0)
				return TRUE;
			else
				return FALSE;
		}
		
		
		public static function getFeaturedPhoto($numrows = 0){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$featphoto  = array();
			$limitstr = '';
			
			if (0 < $numrows)
				$limitstr = "LIMIT 0, " . $numrows ;
				
			// get the featured photo
			$sql = "SELECT tblTravelLogtoPhoto.* " .
					"FROM tblTravelLogtoPhoto " .
					"WHERE isfeatured = 1 " .	$limitstr ;			
					 
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0){
				while ($recordset = mysql_fetch_array($rs->Resultset())) {
					try {
		 				$travellog = new TravelLog($recordset['travellogID']);
		 				$featphoto[] = new Photo($travellog, $recordset['photoID']);	
		 				
		 				Featured::$arrtravelog[] = $travellog;
		 			}
					catch (Exception $e) {				   
					   throw $e;
					} 	
				}
			}
 			
 			return $featphoto;
			
		}
		
		
		
		
		
		public static function getFeatPhotoLog(){
	 		return Featured::$arrtravelog;
	 	}
		
		
		/**
		 ********* TRAVEL FUNCTIONS *************
		 */
		 
		public static function getRandomTravel($numrows = 1){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$sqlcount = "SELECT COUNT(travelID) AS totalrec FROM tblTravel WHERE deleted = 0";
	 		$rs->Execute($sqlcount);
 			
 			if ($rs->Result(0,"totalrec") == 0){
				return NULL;	 		
			}
			
			else {
				
				$listtravel = "";
				$filterstr = "";
				
				$arrtravel = array();
				
				if ($rs->Result(0,"totalrec") < $numrows)
					$numrows = $rs->Result(0,"totalrec");
					
	 			$max = $rs->Result(0,"totalrec") - 1;
				$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
				
	 			while (count($arrtravel) < $numrows){
					
					// get the current recordcount with filter
						if (count($arrtravel)){
							$listtravel = implode(',',$arrtravel);
							$filterstr = "WHERE tblTravel.travelID NOT IN ($listtravel) ";
						}
						
						$sqlcount = $sqlcount . $filterstr ;
		 				$rs->Execute($sqlcount);
		 				
		 				$max = $rs->Result(0,"totalrec") - 1;
						$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
					
					// get the random record
					$sql = "SELECT travelID, travelerID FROM tblTravel " . $filterstr .
							"AND deleted = 0 ORDER BY travelID LIMIT " . $randidx . " , 1" ;
	 				$rs->Execute($sql);
					
					
					try {
		 				$feattravel[] = new Travel($rs->Result(0,"travelID") );
		 			}
					catch (Exception $e) {				   
					   throw $e;
					} 	
				
 					$arrtravel[] = $rs->Result(0,"travelID"); 		// puts selected index in array; value should be unique;
	 					
				}
				
			}
			
			return $feattravel;
			
		}
		
		
		public static function setTravelFeatured($_travelID, $_status = true){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			if (false == $_status)
				$status = 0;
			else
				$status = 1;
				 
			// update travel journal status if featured or not
			$sql = "UPDATE tblTravel SET isfeatured = ". $status . " WHERE travelID = " . $_travelID ;
			$rs->Execute($sql);
			
		}
		
		
		
		public static function isFeaturedTravel($_travelID){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			// check if given travel is featured
			$sql = "SELECT travelID FROM tblTravel WHERE isfeatured = 1 AND travelID = " . $_travelID ." AND deleted = 0";
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0)
				return TRUE;
			else
				return FALSE;
		}
		
		
		
		public static function getFeaturedTravel($numrows = 0){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
				
			$feattravel 	= array();
			$limitstr = '';
			
			if (0 < $numrows)
				$limitstr = "LIMIT 0, " . $numrows ;
				
			$sql = "SELECT travelID, travelerID FROM tblTravel " .
					"WHERE isfeatured = 1 " .
					"AND deleted = 0 ".
					"ORDER BY travelID DESC " . $limitstr ;
					
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0){
				while ($recordset = mysql_fetch_array($rs->Resultset())) {
					try {
		 				$feattravel[] = new Travel($recordset['travelID']);
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
				}
			}			
				
			
			return $feattravel;
			
		}
		
	
		public static function setFeaturedTravelInGroup($groupID,$travelID) {
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
				
				$sql = "INSERT INTO tblGroupFeaturedJournals (groupID,travelID) VALUES(" . $groupID . "," . $travelID . ")";
				$rs->Execute($sql);
				
				// added by K. Gordo: if journal is featured in a group, it is automatically approved
				require_once('travellog/dao/Class.GroupApprovedJournals.php');
				$gaj = new GroupApprovedJournals($travelID);
				$gaj->setGroupID($groupID);				
				$gaj->setApproved(1);
				
				if (GroupApprovedJournals::isExistsJournal($travelID,$groupID))					
					$gaj->Update();
				else 					
					$gaj->Save();

				// approve in parent group as well	
				require_once('travellog/model/Class.GroupFactory.php');
				$group = GroupFactory::instance()->create(array($groupID));
				$parentID = $group[0]->getParentID();
				if($parentID > 0 && !in_array($parentID, self::$approvedParents)){
					$gajParent = new GroupApprovedJournals($travelID);
					$gajParent->setGroupID($parentID);				
					$gajParent->setApproved(1);
			
					if (GroupApprovedJournals::isExistsJournal($travelID,$parentID))					
						$gajParent->Update();
					else 					
						$gajParent->Save();
					self::$approvedParents[] = $parentID;	
				}		
 			}
			catch (Exception $e) {				   
			   throw $e;
			}			
			
		}
		
		public static function unsetFeaturedTravelInGroup($groupID,$travelID) {
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
				
				$sql = "DELETE FROM tblGroupFeaturedJournals WHERE travelID=" . $travelID . " AND groupID=" . $groupID;
				  
				$rs->Execute($sql);
								
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
		}
		
		public static function isFeaturedTravelInGroup($groupID,$travelID){
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
				
				$sql = "SELECT gfj.* FROM tblGroupFeaturedJournals as gfj, tblTravel as t WHERE gfj.travelID=" . $travelID . " AND gfj.travelID = t.travelID AND t.deleted = 0 AND gfj.groupID=" . $groupID;
				  
				$rs->Execute($sql);
				if ($rs->Recordcount() > 0)
					return true;					
				else
					return false;
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
		}
		
		/**
		 * get travel ids of featured travels in a group
		 */
		public static function getFeaturedTravelIDsInGroup($groupID){
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
				
				$sql = "SELECT gfj.* FROM tblGroupFeaturedJournals as gfj, tblTravel as t WHERE gfj.groupID=" . $groupID ." AND gfj.travelID = t.travelID AND t.deleted = 0"; 
				  
				$rs->Execute($sql);
				$travelIDs = array();
				if ($rs->Recordcount() > 0){
					while ($recordset = mysql_fetch_array($rs->Resultset())) {
						$travelIDs[$recordset['travelID']] = $recordset['travelID'] ;
					}
				}				
				return $travelIDs;
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
		}
		
		
		
		
		/**
		 ********* TRAVELER FUNCTIONS *************
		 */
		 
		
		public static function getRandomTraveler($numrows = 1){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			if (!isset($_GET['travelerID'] ))
				$travID = 0;
			else
				$travID = $_GET['travelerID'];
			
			$wherestr = "WHERE travelerID <> ". $travID . " AND travelerID not in (select distinct travelerID from tblGrouptoAdvisor) "  ;  // sql condition that excludes traveler to be randomly chosen, when its profile is currently viewed;
			
			
			$sqlcount = "SELECT COUNT(travelerID) AS totalrec FROM tblTraveler " . $wherestr;
	 		$rs->Execute($sqlcount);
 			
 			if ($rs->Result(0,"totalrec") == 0){
				return NULL;	 		
			}
			
			else {
				
				$listtraveler = "";
				$filterstr = "";
				
				$arrtrav = array();
				
				if ($rs->Result(0,"totalrec") < $numrows)
					$numrows = $rs->Result(0,"totalrec");
					
	 			$max = $rs->Result(0,"totalrec") - 1;
				$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
				
				while (count($arrtrav) < $numrows){
					
					// get the current recordcount with filter
						if (count($arrtrav)){
							$listtraveler = implode(',',$arrtrav);
							$filterstr = " AND travelerID NOT IN ($listtraveler) AND travelerID not in (select distinct travelerID from tblGrouptoAdvisor) ";
						}
						
						$sqlcount = $sqlcount . $filterstr ;
		 				$rs->Execute($sqlcount);
		 				
		 				$max = $rs->Result(0,"totalrec") - 1;
						$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
					
					
					// get the random record
					$sql = "SELECT travelerID FROM tblTraveler " . $wherestr . $filterstr . 
							" ORDER BY travelerID LIMIT " . $randidx . " , 1"  ;
					$rs->Execute($sql);
					
					try {
		 				$feattraveler[] = new Traveler($rs->Result(0,"travelerID"));
		 			}
					catch (Exception $e) {				   
					   throw $e;
					} 	
				
 					$arrtrav[] = $rs->Result(0,"travelerID"); 				// puts selected travelerID in array; value should be unique;
	 				
				}
					
			}
			
			return $feattraveler;
		}
		
		
		public static function setTravelerFeatured($_travelerID, $_status = true){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			if (false == $_status)
				$status = 0;
			else
				$status = 1;
				 
			// update traveler status if featured or not
			$sql = "UPDATE tblTraveler SET isfeatured = ". $status . " WHERE travelerID = " . $_travelerID ;
			$rs->Execute($sql);
			
		}
		
		
		public static function isFeaturedTraveler($_travelerID){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			// check if given traveler is featured
			$sql = "SELECT travelerID FROM tblTraveler WHERE isfeatured = 1 AND travelerID = " . $_travelerID ;
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0)
				return TRUE;
			else
				return FALSE;
		}
		
		
		public static function getFeaturedTraveler($numrows = 0){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$feattraveler	= array();
			$limitstr = '';
			
			if (0 < $numrows)
				$limitstr = "LIMIT 0, " . $numrows ;
				
			// get the featured traveler
			$sql = "SELECT travelerID FROM tblTraveler " .
					"WHERE isfeatured = 1 " .
					"AND travelerID not in (select distinct travelerID from tblGrouptoAdvisor) " . $limitstr   ;
					 
			$rs->Execute($sql);

			if ($rs->Recordcount() > 0){
				while ($recordset = mysql_fetch_array($rs->Resultset())) {
					try {
		 				$feattraveler[] = new Traveler($recordset['travelerID']);
		 			}
					catch (Exception $e) {				   
					   throw $e;
					} 	
				}
			}
						
			return $feattraveler;
		}
		
		
		
		
		
		/**
		 ********* GROUP FUNCTIONS *************
		 */
		
		
		public static function setGroupFeatured($_groupID, $_status = true){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			if (false == $_status)
				$status = 0;
			else
				$status = 1;
				 
			// update group status if featured or not
			$sql = "UPDATE tblGroup SET isfeatured = ". $status . " WHERE groupID = " . $_groupID ;
			$rs->Execute($sql);
			
		}
		
		
		public static function isFeaturedGroup($_groupID){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			// check if given group is featured
			$sql = "SELECT groupID FROM tblGroup WHERE isfeatured = 1 AND groupID = " . $_groupID ;
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0)
				return TRUE;
			else
				return FALSE;
		}
		
		 
		public static function getFeaturedGroup($numrows = 0){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$featgroup  = array();
			$arrGroupId = array();
			$limitstr = '';
			
			if (0 < $numrows)
				$limitstr = "LIMIT 0, " . $numrows ;
				
			// get the featured group
			$sql = "SELECT groupID FROM tblGroup " .
					"WHERE isfeatured = 1 " .
					"AND isSuspended = 0 " .
					"ORDER BY datecreated DESC " . $limitstr   ;
					
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0){
				while ($recordset = mysql_fetch_array($rs->Resultset()))
					$arrGroupId[] = $recordset['groupID'];
				$featgroup = GroupFactory::instance()->create($arrGroupId);
			}

			return $featgroup;
		}
		
		
		/**
		 ********* MOST RECENT TRAVELLOG FUNCTIONS *************
		 */
		
		
		public static function setRecentEntryFeatured($_travellogID, $_status = true){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			if (false == $_status)
				$status = 0;
			else
				$status = 1;
				 
			// update travellog status if featured or not
			$sql = "UPDATE tblTravelLog SET isfeatured = ". $status . " WHERE travellogID = " . $_travellogID ;
			$rs->Execute($sql);
			
		}
		
		
		public static function isFeaturedRecentEntry($_travellogID){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			// check if given travellog is featured
			$sql = "SELECT travellogID FROM tblTravelLog WHERE isfeatured = 1 AND travellogID = " . $_travellogID ;
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0)
				return TRUE;
			else
				return FALSE;
		}
		
		 
		public static function getFeaturedRecentEntry($numrows = 0){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$feattravellog  = array();
			$limitstr = '';
			
			if (0 < $numrows)
				$limitstr = "LIMIT 0, " . $numrows ;
				
			// get the featured travellog
			$sql = "SELECT travellogID FROM tblTravelLog " .
					"WHERE isfeatured = 1 " .
					"ORDER BY travellogID DESC " . $limitstr   ;
					
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0){
				while ($recordset = mysql_fetch_array($rs->Resultset())) {
					try {
		 				$feattravellog[]   =  new Travellog($recordset['travellogID']);		 				
		 			}
					catch (Exception $e) {				   
					   throw $e;
					} 	
				}
			}
						
			return $feattravellog;
		}
		
		
		 /**
		 ********* SPECIAL FEATURED TRAVELERS FUNCTIONS *************
		 */
		public static function setTravelerSpecialFeature($_sectionID = 0, $_travelerID = 0, $_status = true){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			if (false == $_status) // remove feature
				$sql = "DELETE FROM tblFeaturedSectiontoTravelers WHERE sectionID = $_sectionID AND travelerID = $_travelerID "  ;
			else // update traveler status if featured or not
				$sql = "INSERT INTO tblFeaturedSectiontoTravelers (sectionID, travelerID) VALUES ($_sectionID , $_travelerID) " ;
				
			$rs->Execute($sql);
			
		}
		
		
		public static function isSpecialFeaturedTraveler($_sectionID = 0, $_travelerID = 0){
			try {
				$conn = new Connection;
				$rs = new Recordset($conn);				
			}
			catch (Exception $e){
				throw $e;
			}
			
			// check if given traveler is featured
			$sql = "SELECT travelerID FROM tblFeaturedSectiontoTravelers WHERE sectionID = $_sectionID AND travelerID = $_travelerID " ;
			$rs->Execute($sql);
			
			if ($rs->Recordcount() > 0)
				return TRUE;
			else
				return FALSE;
		}
		
		
		public static function getSpecialFeaturedTraveler($_sectionID = 0, $numrows = 0){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$feattraveler	= array();
			$limitstr = '';
			
			if (0 < $numrows)
				$limitstr = "LIMIT 0, " . $numrows ;
				
			// get the specials features traveler
			$sql = "SELECT travelerID " .
					"FROM tblFeaturedSectiontoTravelers " .
					"WHERE sectionID = " . $_sectionID . $limitstr   ;
					 
					 
			$rs->Execute($sql);

			if ($rs->Recordcount() > 0){
				while ($recordset = mysql_fetch_array($rs->Resultset())) {
					try {
		 				$feattraveler[] = new Traveler($recordset['travelerID']);
		 			}
					catch (Exception $e) {				   
					   throw $e;
					} 	
				}
			}
						
			return $feattraveler;
		}
		
		public static function getGroupsWhereTravelIsFeatured($travelID){
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);

				$sql = "SELECT gfj.* FROM tblGroupFeaturedJournals as gfj WHERE gfj.travelID=" . $travelID; 

				$rs->Execute($sql);
				$groupIDs = array();
				if ($rs->Recordcount() > 0){
					while ($recordset = mysql_fetch_array($rs->Resultset())) {
						$groupIDs[$recordset['groupID']] = $recordset['groupID'] ;
					}
				}				
				return $groupIDs;
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
		}
	}
	
?>