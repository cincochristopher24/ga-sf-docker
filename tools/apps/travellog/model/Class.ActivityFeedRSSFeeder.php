<?php

	require_once("travellog/model/Class.ActivityFeed.php");
	require_once("travellog/model/DiscussionBoard/Class.Post.php");
	require_once("travellog/model/Class.RowsLimit.php");
	require_once("travellog/helper/Class.EntryHelper.php");
	require_once("Class.Paging.php");
	require_once("Class.HtmlHelpers.php");
	
	class ActivityFeedRSSFeeder{
		
		public function publish($group=NULL){
			if( !($group instanceof Group) ){
				throw new exception("argument to ActivityFeedRSSFeeder->publish(group) must be an instance of Group");
			}

			ob_start(); 		
	 		header('Content-type: text/xml');
	 		echo '<?xml version="1.0" encoding="utf-8"?>';
	 		echo '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">';
	 		echo '<channel>';
			echo '<atom:link href="http://' . $_SERVER['SERVER_NAME'] .  '/groupfeedsrss.php?gID='. $group->getGroupID() .'" rel="self" type="application/rss+xml" />';
			echo '<title>Recent Activity Feeds in ' . htmlspecialchars($group->getName(),ENT_QUOTES) .'</title>';
	 		echo '<link>http://' . $_SERVER['SERVER_NAME'] .  '/groupfeeds.php?gID='.$group->getGroupID().'</link>';
	 		echo '<description>' . '</description>';
			echo '<language>en-us</language>';
			
			$rpp = 20;
			$page = isset($_GET["page"]) ? $_GET["page"] : 1;
			$rowsLimit = $this->getRowsLimit($rpp,$page);
			
			$params = array(	"GROUP_OBJECT"				=>	$group,
								"GET_ALL_FEEDS"				=>	TRUE,
								"ROWS_LIMIT"				=>	$rowsLimit );
	
			$feedsArray = ActivityFeed::searchFeeds($params);
			$feeds = $feedsArray["activityFeeds"];
			$feedsCount = $feedsArray["recordCount"];
	
			$paging = new Paging( $feedsCount, $page, 'gID=' . $group->getGroupID(), $rpp );
			
			if( $paging->getTotalPages() > 1 ){
				echo '<item>';
				echo '<title>Page '.$page.' of '.$paging->getTotalPages().'</title>';
				echo '<description><![CDATA[';
				echo $paging->showPagination();
				echo ']]></description>';
				echo '</item>';
			}
			
			if( 0 < $feedsCount ){
				foreach($feeds as $feed){
					
					$item = array(	"title"			=>	"",
									"link"			=>	$_SERVER['SERVER_NAME'],
									"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
									"description"	=>	"");
					
					switch( $feed->getFeedSection() ){
						case ActivityFeed::DISCUSSION :
							$item = $this->createDISCUSSIONItem($feed,$group);
							break;
						case ActivityFeed::DISCUSSIONPOST :
							$item = $this->createDISCUSSIONPOSTItem($feed,$group);
							break;
						case ActivityFeed::PHOTOALBUM :
							$item = $this->createPHOTOALBUMItem($feed,$group);
							break;
						case ActivityFeed::VIDEOALBUM :
							$item = $this->createVIDEOALBUMItem($feed,$group);
							break;
						case ActivityFeed::JOURNAL :
							$item = $this->createJOURNALItem($feed,$group);
							break;
						case ActivityFeed::JOURNALENTRY :
							$item = $this->createJOURNALENTRYItem($feed,$group);
							break;
						case ActivityFeed::JOINGROUPREQUEST :
							$item = $this->createJOINGROUPREQUESTItem($feed,$group);
							break;
						case ActivityFeed::NEWMEMBER :
							$item = $this->createNEWMEMBERItem($feed,$group);
							break;
						case ActivityFeed::PHOTO :
							$item = $this->createPHOTOItem($feed,$group);
							break;
						case ActivityFeed::VIDEO :
							$item = $this->createVIDEOItem($feed,$group);
							break;
						case ActivityFeed::ENTRYPHOTO :
							$item = $this->createENTRYPHOTOItem($feed,$group);
							break;
						case ActivityFeed::ENTRYVIDEO :
							$item = $this->createENTRYVIDEOItem($feed,$group);
							break;
					}					
					
					if( !$item ){
						continue;
					}
					
					echo '<item>'; 
		        	echo '<title>'.htmlspecialchars($item["title"],ENT_QUOTES).'</title>';
		        	echo '<link>http://'.$this->toWellFormedXMLSyntax($item["link"]).'</link>';
					echo '<guid>http://'.$_SERVER['SERVER_NAME'].'/groupfeeds.php?gID='.$group->getGroupID().'&amp;feedID='.$feed->getFeedID().'</guid>';
		        	echo '<pubDate>'.$item["feedDate"].' PST'.'</pubDate>';
					echo '<description><![CDATA['. $item["description"] .']]> </description>';        	
					echo '</item>';
				}
		
			}else{
				echo '<item>';
				echo '<description><![CDATA[<p><strong>'.$group->getName() .'</strong> doesn\'t have recent activity update.<br />Subscribe for RSS feeds and keep updated.</p>]]></description>';
				echo '</item>';
			}
	
			if( $paging->getTotalPages() > 1 ){
				echo '<item>';
				echo '<title>Page '.$page.' of '.$paging->getTotalPages().'</title>';
				echo '<description><![CDATA[';
				echo $paging->showPagination();
				echo ']]></description>';
				echo '</item>';
			}
	
			echo '</channel>';
	 		echo '</rss>';

	 		$output = ob_get_contents();
	 		ob_end_clean();
	
	 		echo $output;
		}
		
		private function createDISCUSSIONItem($feed,$group){
			try{
				if( $group->getAdministratorID() == $feed->getTravelerID() ){
					//$traveler = new Traveler($group->getAdministratorID());
					//$creator = new AdminGroup($traveler->getAdvisorGroup());
					$creator = new Traveler($group->getAdministratorID());
					$profileLink = $creator->getFriendlyURL();
				}else{
					$creator = new Traveler($feed->getTravelerID());
					$profileLink = $creator->getFriendlyUrl();
				}
				if( $creator->isSuspended() ){
					return FALSE;
				}
				
				try{
					$discussion = Discussion::createInstance($feed->getSectionID());
					if( !($discussion instanceof Discussion) ){
						return FALSE;
					}
					$topic = $discussion->getParentTopic();
					if( !($topic instanceof Topic) ){
						return FALSE;
					}
					$ownerGroup = new AdminGroup($topic->getGroupID());
					if( !($ownerGroup instanceof AdminGroup) ){
						return FALSE;
					}
				}catch(exception $e){
					return FALSE;
				}
				
				$title = "New Discussion: ".$discussion->getTitle();
				$description = "<p>A new Discussion created by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$creator->getName()."</a>";
				if( $group->isParent() ){
					$description .= " for <a href='http://".$_SERVER['SERVER_NAME'].$ownerGroup->getFriendlyURL()."'>".$ownerGroup->getName()."</a>";
				}
				$description .= "</p>";
				return array(	"title"			=>	$title,
								"link"			=>	$_SERVER['SERVER_NAME'].$discussion->getUrl(),
								"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
								"description"	=>	$description);
			}catch(exception $e){
				return FALSE;
			}
		}
		
		private function createDISCUSSIONPOSTItem($feed,$group){
			try{	
				if( $group->getAdministratorID() == $feed->getTravelerID() ){
					//$traveler = new Traveler($group->getAdministratorID());
					//$creator = new AdminGroup($traveler->getAdvisorGroup());
					$creator = new Traveler($group->getAdministratorID());
					$profileLink = $creator->getFriendlyURL();
				}else{
					$creator = new Traveler($feed->getTravelerID());
					$profileLink = $creator->getFriendlyUrl();
				}
				if( $creator->isSuspended() ){
					return FALSE;
				}
				
				try{
					$post = Post::getInstance($feed->getSectionID());
					if( !($post instanceof Post) ){
						return FALSE;
					}
					$discussion = $post->getParentDiscussion();
					if( !($discussion instanceof Discussion) ){
						return FALSE;
					}
					$topic = $discussion->getParentTopic();
					if( !($topic instanceof Topic) ){
						return FALSE;
					}
					$ownerGroup = new AdminGroup($topic->getGroupID());
					if( !($ownerGroup instanceof AdminGroup) ){
						return FALSE;
					}
				}catch(exception $e){
					return FALSE;
				}
				
				$title = "A Reply to ".$discussion->getTitle();
			
				$description = "<p>".$post->getMessage()."</p>".
								"<p>Posted by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$creator->getName()."</a>";
				if( $group->isParent() ){
					$description .= " in <a href='http://".$_SERVER['SERVER_NAME'].$ownerGroup->getFriendlyURL()."'>".$ownerGroup->getName()."</a>";
				}
				$description .= "</p>";
				return array(	"title"			=>	$title,
								"link"			=>	$_SERVER['SERVER_NAME'].$discussion->getUrl(),
								"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
								"description"	=>	$description);
			}catch(exception $e){
				return FALSE;
			}
		}
		
		private function createPHOTOALBUMItem($feed,$group){
			try{
				$album = new PhotoAlbum($feed->getSectionID());
				if( !($album instanceof PhotoAlbum ) ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			$rowsLimit = new RowsLimit(10,0);
			$photos = $album->getPhotos($rowsLimit);
			$photoCount = count($photos);
			
			if( !$photoCount ){
				return FALSE;
			}
			
			$title = "New Photo Album: ".$album->getTitle();
			
			$creator = new Traveler($feed->getTravelerID());
			if( $creator->isSuspended() ){
				return FALSE;
			}
			
			if( ActivityFeed::TRAVELER == $feed->getSectionType() ){
				
				$profileLink = $creator->getFriendlyUrl();
				$albumLink = "/collection.php?type=traveler&ID=".$album->getCreator()."&context=traveleralbum&genID=".$album->getPhotoAlbumID();
				$description = "<p>";
				if( 0 < $photoCount ){
					foreach($photos AS $photo){
						$description .= "<img src='".$photo->getPhotoLink('thumbnail')."' />";
					}
				}
				$description .= "</p>".
								"<p>".
									"created by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$creator->getName()."</a>".
								"</p>";				
			}else{
				$ownerGroup = new AdminGroup($feed->getGroupID());
				$albumLink = "/collection.php?type=group&ID=".$ownerGroup->getGroupID()."&context=photoalbum&genID=".$album->getPhotoAlbumID();
				/*if( $feed->getTravelerID() == $group->getAdministratorID() ){
					$creator = new AdminGroup($creator->getAdvisorGroup());
					$profileLink = $creator->getFriendlyURL();
				}else{
					$profileLink = $creator->getFriendlyUrl();
				}*/
				$profileLink = $creator->getFriendlyUrl();
				
				$description = "<p>";
				if( 0 < $photoCount ){
					foreach($photos AS $photo){
						$description .= "<img src='".$photo->getPhotoLink('thumbnail')."' />";
					}
				}
				$description .= "</p>".
								"<p>created by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$creator->getName()."</a>";
				if( $feed->getGroupID() != $group->getGroupID() ){
					$description .= " for <a href='http://".$_SERVER['SERVER_NAME'].$ownerGroup->getFriendlyURL()."'>".$ownerGroup->getName()."</a>";
				}
				$description .= "</p>";
			}
			
			
			
			return 	array(	"title"			=>	$title,
							"link"			=>	$_SERVER['SERVER_NAME'].$albumLink,
							"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
							"description"	=>	$description);
		}
		
		private function createVIDEOALBUMItem($feed,$group){
			try{
				$videoAlbum = new VideoAlbum($feed->getSectionID());
				if( !($videoAlbum instanceof VideoAlbum) ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			$rowsLimit = new RowsLimit(10,0);
			$videos = $videoAlbum->getVideos2($rowsLimit);
			if( !count($videos) ){
				return FALSE;
			}

			$title = "New Video Album: ".$videoAlbum->getTitle();

			$creator = new Traveler($feed->getTravelerID());
			if( $creator->isSuspended() ){
				return FALSE;
			}
			
			$description = "";
			if( $feed->getSectionType() == ActivityFeed::GROUP ){
				$owner = new AdminGroup($feed->getGroupID());
				$albumLink = "/video.php?action=getGroupVideos&gID=".$videoAlbum->getCreatorID()."&albumID=".$videoAlbum->getAlbumID()."&type=album";
				
				/*if( $feed->getTravelerID() == $group->getAdministratorID() ){
					$creator = new AdminGroup($creator->getAdvisorGroup());
					$profileLink = $creator->getFriendlyURL();
				}else{
					$profileLink = $creator->getFriendlyUrl();
				}*/
				$profileLink = $creator->getFriendlyUrl();
				
				if( 0 < count($videos) ){
					$description = "<p>";
					foreach($videos AS $video){
						$videoLink = "/video.php?action=getGroupVideos&gID=".$owner->getGroupID()."&albumID=".$videoAlbum->getAlbumID()."&videoID=".$video->getVideoID()."&type=album";					
						$description .= "<a href='$videoLink'><img src='".$video->getVideoImageUrl()."' title='".$video->getTitle()."' /></a>";
					}
					$description .= "</p>";
				}
				$description .= "<p>created by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$creator->getName()."</a>";
				if( $feed->getGroupID() != $group->getGroupID() ){
					$description .= " for <a href='http://".$_SERVER['SERVER_NAME'].$owner->getFriendlyURL()."'>".$owner->getName()."</a>";
				}
				$description .= "</p>";
			}else{
				$profileLink = $creator->getFriendlyURL();
				if( 0 < count($videos) ){
					$albumLink = "/video.php?action=getTravelerVideos&travelerID=".$videoAlbum->getCreatorID()."&albumID=".$videoAlbum->getAlbumID()."&type=album";
					$description = "<p>";
					if( 0 < count($videos) ){
						foreach($videos AS $video){
							$videoLink = "/video.php?action=getTravelerVideos&travelerID=".$creator->getTravelerID()."&albumID=".$videoAlbum->getAlbumID()."&videoID=".$video->getVideoID()."&type=album";						
							$description .= "<a href='$videoLink'><img src='".$video->getVideoImageUrl()."' title='".$video->getTitle()."' /></a>";
						}
					}
					$description .= "</p>";
				}else{
					$albumLink = "/video.php?travelerID=".$videoAlbum->getCreatorID();
				}
				$description .=	"<p>created by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$creator->getName()."</a></p>";
			}
			
			return 	array(	"title"			=>	$title,
							"link"			=>	$_SERVER['SERVER_NAME'].$albumLink,
							"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
							"description"	=>	$description);
		}
		
		private function createJOURNALItem($feed,$group){			
			try{
				$travel = new Travel($feed->getSectionID());
				if( !($travel instanceof Travel) ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			$travellog = $travel->getFirstJournalEntry();
			if( !($travellog instanceof TravelLog) ){
				return FALSE;
			}
			
			$owner = $travel->getOwner();
			
			$title = "New Journal: ".$travel->getTitle();
			if( $travellog instanceof TravelLog ){
				$entryHelper = new EntryHelper();
				$journalLink = $entryHelper->getLink($travellog);

	        	$location = $travellog->getTrip()->getLocation();
	        	$locStr =  "<a href='http://".$_SERVER['SERVER_NAME'].$journalLink."'>".$location->getName().", ".$location->getCountry()->getName()."</a>";        	
	        	$dateStr = date('D, j M Y',strtotime($travellog->getLogDate()));
				$entry = HtmlHelpers::TruncateText(strip_tags($travellog->getDescription()), 100);
	        	$description = "<p>".$locStr.", ".$dateStr."<br />".$entry."</p>";
				
			}else{
				$journalLink = "";
				$description = "<i>Journal doesn't have an entry yet.</i>";
			}
			
			if( ActivityFeed::TRAVELER == $feed->getSectionType() ){
				if( $owner->isSuspended() ){
					return FALSE;
				}
				$description .= "<p>published by <a href='http://".$_SERVER['SERVER_NAME'].$owner->getFriendlyUrl()."'>".$owner->getName()."</a></p>";
			}else{
				$creator = new Traveler($feed->getTravelerID());
				/*if( $group->getAdministratorID() == $feed->getTravelerID() ){
					$creator = new AdminGroup($creator->getAdvisorGroup());
					$profileLink = $creator->getFriendlyURL();
				}else{
					$profileLink = $creator->getFriendlyUrl();
				}*/
				$profileLink = $creator->getFriendlyUrl();
				$description .= "<p>published by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$creator->getName()."</a>";
				if( $feed->getGroupID() != $group->getGroupID() ){
					$description .= " for <a href='http://".$_SERVER['SERVER_NAME'].$owner->getFriendlyURL()."'>".$owner->getName()."</a>";
				}
				$description .= "</p>";
			}
			
			return 	array(	"title"			=>	$title,
							"link"			=>	$_SERVER['SERVER_NAME'].$journalLink,
							"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
							"description"	=>	$description);
		}
		
		private function createJOURNALENTRYItem($feed,$group){
			try{
				$travellog = new TravelLog($feed->getSectionID());
				$travel = new Travel($travellog->getTravelID());
				if( !($travellog instanceof TravelLog) && !($travel instanceof Travel) ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			$owner = $travel->getOwner();
						
			$entryHelper = new EntryHelper();
			$entryLink = $entryHelper->getLink($travellog);
			
			$title = "New Journal Entry: ".$travellog->getTitle();
			
			$location = $travellog->getTrip()->getLocation();
        	$locStr =  "<a href='http://".$_SERVER['SERVER_NAME'].$entryLink."'>".$location->getName().", ".$location->getCountry()->getName()."</a>";        	
        	$dateStr = date('D, j M Y',strtotime($travellog->getLogDate()));
			$entry = HtmlHelpers::TruncateText(strip_tags($travellog->getDescription()), 100);
        	$description = "<p>".$locStr.", ".$dateStr."<br />".$entry."</p>";
        	
			if( ActivityFeed::TRAVELER == $feed->getSectionType() ){
				if( $owner->isSuspended() ){
					return FALSE;
				}
				$description .= "<p>published by <a href='http://".$_SERVER['SERVER_NAME'].$owner->getFriendlyUrl()."'>".$owner->getName()."</a></p>";
			}else{
				$creator = new Traveler($feed->getTravelerID());
				/*if( $group->getAdministratorID() == $feed->getTravelerID() ){
					$creator = new AdminGroup($creator->getAdvisorGroup());
					$profileLink = $creator->getFriendlyURL();
				}else{
					$profileLink = $creator->getFriendlyUrl();
				}*/
				$profileLink = $creator->getFriendlyUrl();
				
				$description .= "<p>published by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$creator->getName()."</a>";
				if( $feed->getGroupID() != $group->getGroupID() ){
					$description .= " for <a href='http://".$_SERVER['SERVER_NAME'].$owner->getFriendlyURL()."'>".$owner->getName()."</a>";
				}
				$description .= "</p>";
			}
			
			return 	array(	"title"			=>	$title,
							"link"			=>	$_SERVER['SERVER_NAME'].$entryLink,
							"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
							"description"	=>	$description);
		}
		
		private function createJOINGROUPREQUESTItem($feed,$group){
			try{		
				$traveler = new Traveler($feed->getTravelerID());
				if( $traveler->isSuspended() ){
					return FALSE;
				}
				
				$requestedGroup = new AdminGroup($feed->getGroupID());
			
				$title = $traveler->getName()." requested to join ".$requestedGroup->getName();
			
				return 	array(	"title"			=>	$title,
								"link"			=>	$_SERVER['SERVER_NAME']."/members.php?gID=".$requestedGroup->getGroupID(),
								"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
								"description"	=>	"");
			}catch(exception $e){
				return FALSE;
			}
		}
		
		private function createNEWMEMBERItem($feed,$group){
			try{
				$traveler = new Traveler($feed->getTravelerID());
				if( $traveler->isSuspended() ){
					return FALSE;
				}
				
				$joinedGroup = new AdminGroup($feed->getGroupID());
				$title = $traveler->getName()." became a member of ".$joinedGroup->getName();

				$description = "";
				if( "0000-00-00 00:00:00" != $feed->getAuxDate() ){
					$auxDate = date('D, j M Y H:i:s',strtotime($feed->getAuxDate()));
					if( ActivityFeed::MEMBER_BY_INVITE == $feed->getSectionRootID() ){	
						$description = "<p><a href='http://".$_SERVER['SERVER_NAME'].$traveler->getFriendlyUrl()."'>".$traveler->getName()."<a> was invited to join <a href='http://".$_SERVER['SERVER_NAME'].$joinedGroup->getFriendlyURL()."'>".$joinedGroup->getName()."</a> on $auxDate</p>";
					}elseif( ActivityFeed::MEMBER_BY_REQUEST == $feed->getSectionRootID() ){
						$description = "<p><a href='http://".$_SERVER['SERVER_NAME'].$traveler->getFriendlyUrl()."'>".$traveler->getName()."<a> requested to join <a href='http://".$_SERVER['SERVER_NAME'].$joinedGroup->getFriendlyURL()."'>".$joinedGroup->getName()."</a> on $auxDate</p>";
					}
				}
			
				return 	array(	"title"			=>	$title,
								"link"			=>	$_SERVER['SERVER_NAME']."/members.php?gID=".$joinedGroup->getGroupID()."&mode=1",
								"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
								"description"	=>	$description);
			}catch(exception $e){
				return FALSE;
			}
		}
		
		private function createPHOTOItem($feed,$group){
			try{			
				$album = new PhotoAlbum($feed->getSectionRootID());
				if( !($album instanceof PhotoAlbum) ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			$rowsLimit = new RowsLimit(10,0);
			$photosArray = $album->getPhotosUploadedByDay($rowsLimit,$feed->getFeedDate());
			$photos = $photosArray["photos"];
			$photoCount = $photosArray["recordCount"];
			if( !$photoCount ){
				return FALSE;
			}
			
			$photoCountLabel = 1 < $photoCount ? "Photos were" : "Photo was";
			
			$title = "$photoCount $photoCountLabel uploaded to Photo Album ".$album->getTitle();
			
			$description = "<p>";
			foreach($photos As $photo){
				$description .= "<img src='".$photo->getPhotoLink('thumbnail')."' />";
			}
			$description .= "</p>"; 
			
			if( ActivityFeed::TRAVELER == $feed->getSectionType() ){
				$traveler = new Traveler($feed->getTravelerID());
				if( $traveler->isSuspended() ){
					return FALSE;
				}
				
				$albumLink = "/collection.php?type=traveler&ID=".$album->getCreator()."&context=traveleralbum&genID=".$album->getPhotoAlbumID();
				$description .= "<p>album owned by <a href='http://".$_SERVER['SERVER_NAME'].$traveler->getFriendlyUrl()."'>".$traveler->getName()."</a></p>";
			}else{
				$ownerGroup = new AdminGroup($feed->getGroupID());
				$albumLink = "/collection.php?type=group&ID=".$ownerGroup->getGroupID()."&context=photoalbum&genID=".$album->getPhotoAlbumID();
				$description .= "<p>album owned by <a href='http://".$_SERVER['SERVER_NAME'].$ownerGroup->getFriendlyURL()."'>".$ownerGroup->getName()."</a></p>";
			}
			
			return 	array(	"title"			=>	$title,
							"link"			=>	$_SERVER['SERVER_NAME'].$albumLink,
							"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
							"description"	=>	$description);
		}
		
		private function createVIDEOItem($feed,$group){	
			try{		
				$videoAlbum = new VideoAlbum($feed->getSectionRootID());
				if( !($videoAlbum instanceof VideoAlbum) ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			$rowsLimit = new RowsLimit(10,0);
			$videosArray = $videoAlbum->getVideosUploadedByDay($rowsLimit,$feed->getFeedDate());
			$videos = $videosArray["videos"];
			if( !$videosArray["recordCount"] ){
				return FALSE;
			}
			$videoCountLabel = 1 < $videosArray["recordCount"] ? $videosArray["recordCount"]." Videos were" : $videosArray["recordCount"]." Video was";
			
			$title = "$videoCountLabel added to Video Album ".$videoAlbum->getTitle();
			$description = "";
			if( $feed->getSectionType() == ActivityFeed::GROUP ){
				$owner = new AdminGroup($feed->getGroupID());
				$albumLink = "/video.php?action=getGroupVideos&gID=".$videoAlbum->getCreatorID()."&albumID=".$videoAlbum->getAlbumID()."&type=album";
				if( 0 < count($videos) ){
					$description = "<p>";
					foreach($videos AS $video){
						$videoLink = "/video.php?action=getGroupVideos&gID=".$owner->getGroupID()."&albumID=".$videoAlbum->getAlbumID()."&videoID=".$video->getVideoID()."&type=album";					
						$description .= "<a href='http://".$_SERVER['SERVER_NAME'].$videoLink."'><img src='".$video->getVideoImageUrl()."' title='".$video->getTitle()."' /></a>";
					}
					$description .= "</p>";
				}
				
				$description .= "<p>video album owned by <a href='http://".$_SERVER['SERVER_NAME'].$owner->getFriendlyURL()."'>".$owner->getName()."</a></p>";
			}else{
				$creator = new Traveler($feed->getTravelerID());
				if( $creator->isSuspended() ){
					return FALSE;
				}
				if( 0 < count($videos) ){
					$albumLink = "action=getTravelerVideos&travelerID=".$videoAlbum->getCreatorID()."&albumID=".$videoAlbum->getAlbumID()."&type=album";
					$description = "<p>";
					if( 0 < count($videos) ){
						foreach($videos AS $video){
							$videoLink = "/video.php?action=getTravelerVideos&travelerID=".$creator->getTravelerID()."&albumID=".$videoAlbum->getAlbumID()."&videoID=".$video->getVideoID()."&type=album";						
							$description .= "<a href='http://".$_SERVER['SERVER_NAME'].$videoLink."'><img src='".$video->getVideoImageUrl()."' title='".$video->getTitle()."' /></a>";
						}
					}
					$description .= "</p>";
				}else{
					$albumLink = "/video.php?travelerID=".$videoAlbum->getCreatorID();
				}
				$description .=	"<p>video album owned by <a href='http://".$_SERVER['SERVER_NAME'].$creator->getFriendlyUrl()."'>".$creator->getName()."</a></p>";
			}
			
			return 	array(	"title"			=>	$title,
							"link"			=>	$_SERVER['SERVER_NAME'].$albumLink,
							"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
							"description"	=>	$description);
		}
		
		private function createENTRYPHOTOItem($feed,$group){
			try{
				$travellog = new TravelLog($feed->getSectionRootID());
				if( !($travellog instanceof TravelLog) ){
					return FALSE;
				}		
			}catch(exception $e){
				return FALSE;
			}
			$entryHelper = new EntryHelper();
			$entryLink = $entryHelper->getLink($travellog);
			
			$rowsLimit = new RowsLimit(10,0);
			$photosArray = $travellog->getPhotosUploadedByDay($rowsLimit,$feed->getFeedDate());
			$photos = $photosArray["photos"];
			$photoCount = $photosArray["recordCount"];
			if( !$photoCount ){
				return FALSE;
			}
			
			$photoCountLabel = 1 < $photoCount ? "Photos were" : "Photo was";
			$title = "$photoCountLabel uploaded for Journal Entry ".$travellog->getTitle();
			
			$description = "<p>";
			foreach($photos As $photo){
				$description .= "<img src='".$photo->getPhotoLink('thumbnail')."' />";
			}
			$description .= "</p>";
			
			if( ActivityFeed::GROUP == $feed->getSectionType() ){
				$owner = new AdminGroup($feed->getGroupID());
				$profileLink = $owner->getFriendlyURL();
			}else{
				$owner = new Traveler($feed->getTravelerID());
				if( $owner->isSuspended() ){
					return FALSE;
				}
				$profileLink = $owner->getFriendlyUrl();
			}
			$description .= "<p>journal owned by <a href='http://".$_SERVER['SERVER_NAME'].$profileLink."'>".$owner->getName()."</a></p>";
			
			return 	array(	"title"			=>	$title,
							"link"			=>	$_SERVER['SERVER_NAME'].$entryLink,
							"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
							"description"	=>	$description);
		}
		
		private function createENTRYVIDEOItem($feed,$group){
			try{
				$travellog = new TravelLog($feed->getSectionRootID());
				$travel = new Travel($travellog->getTravelID());
				if( !($travellog instanceof TravelLog) && !($travel instanceof Travel) ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			$entryHelper = new EntryHelper();
			$entryLink = $entryHelper->getLink($travellog);
			
			$rowsLimit = new RowsLimit(10,0);
			$videosArray = $travellog->getVideosUploadedByDay($rowsLimit,$feed->getFeedDate());
			$videos = $videosArray["videos"];
			if( !$videosArray["recordCount"] ){
				return FALSE;
			}
			
			$videoCountLabel = 1 < $videosArray["recordCount"] ? $videosArray["recordCount"]." Videos were" : $videosArray["recordCount"]." Video was";
			
			$title = "$videoCountLabel added to for Journal Entry ".$travellog->getTitle();
			
			if( $feed->getSectionType() == ActivityFeed::GROUP ){
				$owner = new AdminGroup($feed->getGroupID());
				$description = "<p>";
				foreach($videos AS $video){
					$videoLink = "/video.php?action=getGroupVideos&gID=".$owner->getGroupID()."&travelID=".$travel->getTravelID()."&travelLogID=".$travellog->getTravelLogID()."&videoID=".$video->getVideoID()."&jeVideos=1&type=journal";
					$description .= "<a href='http://".$_SERVER['SERVER_NAME'].$videoLink."'><img src='".$video->getVideoImageUrl()."' title='".$video->getTitle()."' /></a>";
				}
				$description .= "</p>";			
				$description .= "<p>journal owned by <a href='http://".$_SERVER['SERVER_NAME'].$owner->getFriendlyURL()."'>".$owner->getName()."</a></p>";
			}else{				
				$owner = new Traveler($feed->getTravelerID());
				if( $owner->isSuspended() ){
					return FALSE;
				}
				$description = "<p>";
				foreach($videos AS $video){
					$videoLink = "/video.php?action=getTravelerVideos&travelerID=".$owner->getTravelerID()."&travelID=".$travel->getTravelID()."&travelLogID=".$travellog->getTravelLogID()."&videoID=".$video->getVideoID()."&jeVideos=1&type=journal";
					$description .= "<a href='http://".$_SERVER['SERVER_NAME'].$videoLink."'><img src='".$video->getVideoImageUrl()."' title='".$video->getTitle()."' /></a>";
				}
				$description .= "</p>";
				$description .=	"<p>journal owned by <a href='http://".$_SERVER['SERVER_NAME'].$owner->getFriendlyUrl()."'>".$owner->getName()."</a></p>";
			}
			
			
			return 	array(	"title"			=>	$title,
							"link"			=>	$_SERVER['SERVER_NAME'].$entryLink,
							"feedDate"		=>	date('D, j M Y H:i:s',strtotime($feed->getFeedDate())),
							"description"	=>	$description);
		}
		
		function toWellFormedXMLSyntax($str=""){
			if( "" == $str ){
				return $str;
			}
			$temp = str_replace("&","&amp;",$str);
			$temp = str_replace(">","&gt;",$temp);
			$temp = str_replace("<","&lt;",$temp);
			$temp = str_replace("'","&apos;",$temp);
			$temp = str_replace("\"","&quot;",$temp);
			
			return $temp;
		}
		
		function getRowsLimit($rpp=15,$page=1){
			$offset = ($page-1) * $rpp;
			$rowsLimit = new RowsLimit($rpp, $offset);
			return $rowsLimit;
		}
	}