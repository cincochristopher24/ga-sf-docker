<?php
	/**
	 * Created on Jul 27, 2006
	 *	@author Kerwin Gordo
	 *	Purpose: A subclass of Location and represents a City
	 * 
	 * Last Edited By	: Cheryl Ivy Q. Go
	 * Purpose			: Class City has been modified to adjust with the new requirements of GA.net.
	 * 						Users can add new Cities to the database which will be verified by the day crew.
	 */
	
	require_once 'Class.Location.php';
	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'Class.LocationType.php';
	require_once 'Class.ConnectionProvider.php';
	
	class State extends Location {
	 	private $mRs;
	 	private $mRs2;
	 	private $mConn;

	 	private $stateID;
	 	private $countryID;
	 	private $continentID;
	 	
	  	private $region;
	  	private $country;
	  	private $continent;  	
	  	private $approved = 1;

	  	// Constructor
	  	function State($stateID = 0){			
	  		try{
				$this-> mConn = ConnectionProvider::instance()->getConnection();
				$this-> mRs   = new Recordset(ConnectionProvider::instance()->getConnection());
				$this-> mRs2  = new Recordset(ConnectionProvider::instance()->getConnection());
			}
			catch(exception $e){
				throw $e;
			}

	  		if ($stateID){

	  			$sql = "select * from GoAbroad_Main.tbstate where stateID = " . $stateID;
	  			$this-> mRs-> Execute($sql);

	 			if (0 == $this->mRs->Recordcount()) throw new InstantiationException("state id " . $stateID . " is not a valid ID"); 
	  			
	  			$this->setLocationID( $this->mRs->Result(0, "locID")               );
	  			$this->setName      ( stripslashes($this->mRs->Result(0, "state")) );
	  			$this->continentID = 0;
	  			$this->countryID   = 91;
	  			$this-> stateID    = $stateID;
	  		}
		}

		// Setters
		function setStateID($stateID = 0) {
	 		$this->stateID = $stateID;
	 	}
	 	
	 	function setCountryID($countryID = 0) {
	 		$this->countryID = $countryID;
	 	}
	 	
	 	function setContinentID($continentID = 0) {
			$this->continentID = $continentID;
		}
	 	
	 	// Getters
	 	function getStateID() {
	 		return $this->stateID;
	 	}	 	
	 	// overrides
	 	
 	  	function getCountries()       { return null; }	
 		function getCities()          { return null; } 			 			
 		function getNonPopLocations() { return null; }
		function getStates()          { return null; }
		 		
 		function getCountry() {
 			if ($this->country == null) {
 				$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
 				$sql = "select * from GoAbroad_Main.tbcountry where countryID = " . $this->countryID . " and approved = 1";
 				$this->mRs->Execute($sql);
 				
 				$this->country = new Country;
 				$this->country-> setLocationID($this->mRs->Result(0, "locID"));
 				$this->country-> setContinentID($this->mRs->Result(0, "continentID"));
 				$this->country-> setCountryID($this->mRs->Result(0, "countryID"));
 				$this->country-> setName($this->mRs->Result(0, "country")); 						
 			}

 			return $this->country;		
 		}

 		function getContinent() {
 			if ($this->continent == null) {
	 			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
 				$sql = "select * from tblContinent where continentID = " . $this->continentID;
				$this-> mRs-> Execute($sql);
	 			
	 			$continent = new Continent();
	 			$continent-> setLocationID($this->mRs->Result(0, "locID"));
	 			$continent-> setName($this->mRs->Result(0, "name"));
	 			$continent-> setContinentID($this->mRs->Result(0, "continentID"));
 			}

 			return $continent; 			
 		}	
 		
 		function save(){
 			
 			if (count($this->getName()) == 0) {
 				throw new Exception("State must be set!");
 			}
 			
 			if ($this->countryID == 0) {
 				throw new Exception("CountryID must be set!");
 			} 			
 			
 			$sql = "INSERT INTO tblLocation (locationtypeID) VALUES ('" . LocationType::$STATE . "',1)";
 			$this->mRs-> Execute($sql);
 			
 			$this->setLocationID(mysql_insert_id());
 			 			
 			$sql2 = "INSERT INTO GoAbroad_Main.tbstate (state, locID) VALUES ('" . addslashes($this->getName()) . "', " . $this->getLocationID() .")";
 			$this->mRs2-> Execute($sql2);
 			
 			$this->stateID = mysql_insert_id();
 		}
 		
 		function update(){
			if ($this->stateID == 0) throw new Exception("StateID must be set!");
 			$sql = "UPDATE GoAbroad_Main.tbstate SET state = '" . addslashes($this->getName()) . "' WHERE stateID = " . $this->stateID;
			$this->mRs->Execute($sql);
 		} 
 		
 		function delete(){ 			
 			$sql = "DELETE FROM tblLocation WHERE locID = " . $this->getLocationID();
 			$this-> mRs-> Execute($sql);
 			 			
 			$sql = "DELETE FROM GoAbroad_Main.tbstate WHERE locID = " . $this->getLocationID();
 			$this->mRs->Execute($sql);

 			$this-> setLocationID(0);
 			$this-> setName("");
 			$this-> setCountryID(0);
 			$this-> setContinentID(0);
 			$this-> stateID = 0;
 		}
		
		
		// static function added by: nash
		// added on sept 1, 2009
		static function getStateList(){
			$states = array();
			$rs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = 'SELECT * FROM GoAbroad_Main.tbstate';
			$rs->execute($sql);
			
			for($i = 0; $i < $rs->RecordCount(); $i++){
				$obj = $rs->FetchAsObject();
				foreach($obj as $key => $val){
					$state[$key] = $val;
				}
				
				$states[] = array(	'id'   => $state['stateID'],
									'name' => $state['state'],
									'abbr' => $state['stateabbr']
								);		
			}
			
			return $states;
		}

	 }  
?>