<?php
	require_once('Class.FBini.php');
	class AnswerValue
	{
		const TEXT 		= 1;
		const RADIO 		= 2;
		const CHECKBOX 	= 3;
		const SELECTBOX 	= 4;
		const TEXTAREA 	= 5;
		
		private $mAnswerID 		= 0;
		private $mAnswerToUserID 	= 0;
		private $mAnswerType		= 0;
		private $mValue			= null;
		
		private $mDb				= null;
		
		function __construct($answerID=null){
			$this->mDb = FBini::createDbHandler();
			if(!is_null($answerID)){
				$qry = 	'SELECT answerID, answerToUserID, answerType, answer ' .
						'FROM tbAnswer ' .
						'WHERE answerID = '.ToolMan::makeSqlSafeString($answerID);
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(0==$rs->retrieveRecordCount()){
					throw new exception('Invalid answerID '.$answerID.' passed to object of type AnswerValue');
				}
				else{
					$this->mAnswerID 		= $rs->getAnswerID(0);
					$this->mAnswerToUserID 	= $rs->getAnswerToUserID(0);
					$this->mAnswerType		= $rs->getAnswerType(0);
					$this->mValue			= $rs->getAnswer(0);
				}
			}			
		}
		
		public function setAnswerToUserID($param=0){
			$this->mAnswerToUserID = $param;
		}		
		
		public function setAsText(){
			$this->mAnswerType = self::TEXT;
		}		
		
		public function setAsRadio(){
			$this->mAnswerType = self::RADIO;
		}		
		
		public function setAsCheckbox(){
			$this->mAnswerType = self::CHECKBOX;
		}
		
		public function setAsSelectbox(){
			$this->mAnswerType = self::SELECTBOX;			
		}
		
		public function setAsTextarea(){
			$this->mAnswerType = self::TEXTAREA;			
		}
		
		public function setValue($param=null){
			$this->mValue = $param;
		}
		
		public function setAnswerType($param=null){
			$this->mAnswerType = $param;
		}
		
		//GETTERS
		public function getAnswerID(){
			return $this->mAnswerID;
		}
		public function getAnswerToUserID(){
			return $this->mAnswerToUserID;
		}
		public function isTypeText(){
			return self::TEXT == $this->mAnswerType;
		}
		public function isTypeRadio(){
			return self::RADIO == $this->mAnswerType;
		}
		public function isTypeCheckbox(){
			return self::CHECKBOX == $this->mAnswerType;
		}
		public function isTypeSelectbox(){
			return self::SELECTBOX == $this->mAnswerType;
		}
		public function isTypeTextarea(){
			return self::TEXTAREA == $this->mAnswerType;
		}
		public function getAnswerType(){
			return $this->mAnswerType;
		}
		public function getValue(){
			return $this->mValue;
		}
		
		
		/*** CRUDE ***/
		public function save(){
			if(0==$this->mAnswerID){//add
				$qry =	'INSERT INTO tbAnswer( ' .
							'`answerToUserID` ,' .
							'`answerType` ,' .
							'`answer` ' .
						')VALUES(' .
							ToolMan::makeSqlSafeString($this->mAnswerToUserID).',' .
							ToolMan::makeSqlSafeString($this->mAnswerType).',' .
							ToolMan::makeSqlSafeString($this->mValue).
						')';
				$this->mDb->execQuery($qry);
				$this->mAnswerID = $this->mDb->getLastInsertedID();
			}
			else{//update
				$qry =	'UPDATE tbAnswer SET ' .
							'`answerToUserID` 	= ' .ToolMan::makeSqlSafeString($this->mAnswerToUserID) .','.
							'`answerType` 		='	.ToolMan::makeSqlSafeString($this->mAnswerType).', '.
							'`answer` 			='	.ToolMan::makeSqlSafeString($this->mValue).' '.
						'WHERE `answerID` =' 	.ToolMan::makeSqlSafeString($this->mAnswerID);
				$this->mDb->execQuery($qry);			
			}
		}
		
		public function delete(){
			$qry = 	'DELETE FROM tbAnswer ' .
					'WHERE `answerID` = '.ToolMan::makeSqlSafeString($this->mAnswerID);
			$this->mDb->execQuery($qry);
		}
		
		/*** Static Functions ***/
		public static function getAnswerValuesByAnswerID($answerToUserID=0){
			$db = FBini::createDbHandler();
			$qry = 	'SELECT `answerID` ' .
					'FROM tbAnswer ' .
					'WHERE answerToUserID = '. ToolMan::makeSqlSafeString($answerToUserID) .
					' ORDER BY answerID asc';
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array(); 
			if(0 != $rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new AnswerValue($rs->getAnswerID());
				}
			}
			return $ar;
		}
	}
?>