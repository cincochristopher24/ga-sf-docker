<?php
	/**
	 * Created on September 27, 2006
	 * Class.InquiryBox.php 
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.InquiryMessage.php';
	require_once 'travellog/model/Class.InquiryMessageNotification.php';
	
	class InquiryBox{		
		
		private $rs		= NULL;
		private $rs2	= NULL;
		private $rs3	= NULL;
		private $rs4	= NULL;
		private $rs5	= NULL;		
		private $conn	= NULL;

		private $mInstance		= 0;
		private $mmSendableId	= 0;
		private $mInboxOwner	= 0;
		private $mTotalRecords	= 0;
		
		/**
		 * Constructor of class Inbox
		 * @param integer $_sendableID
		 * @return InquiryBox
		 */		
		function InquiryBox($_sendableID = 0){			

 			try{ 				
 				$this-> conn = new Connection();
				$this-> rs   = new Recordset($this->conn);
				$this-> rs2   = new Recordset($this->conn);
				$this-> rs3   = new Recordset($this->conn);
				$this-> rs4   = new Recordset($this->conn);
				$this-> rs5   = new Recordset($this->conn);
			}
			
			catch (exception $e){				   
			   throw $e;
			}

			$this-> mSendableId = $_sendableID;
			return $this;
		}
				
		
		/**
		 * Function name: Send
		 * @param Message $_message
		 * Inserts message to table tblMessages
		 */				
		function Send(Message $_message = NULL){

			// Set values to be inserted to table
			
			$parentID = $_message-> getParentID();
			$listingID = $_message-> getListingID();
			$text = addslashes($_message-> getText());
			$subject = addslashes($_message-> getTitle());
			$discriminator = $_message-> getDiscriminator();
			$destination = $_message-> getDestination();
			$source = $_message-> getSource()-> getSendableID();
			
			// Insert values to database table
			
			$sqlQuery = "INSERT INTO tblMessages (`discriminator`, `senderID`, `title`, `message`) " .
						"VALUES ($discriminator, $source, '$subject', '$text')";
			$this-> rs-> Execute($sqlQuery);
			
			$sqlQuery2 = "SELECT LAST_INSERT_ID() AS id";
			$this-> rs2-> Execute($sqlQuery2);
			
			$messID = $this->rs2->Result(0, "id");

			// Loop through destinations and insert into tblMessageToAttribute
			if (0 < sizeof($destination)){			
				foreach ($destination as $dest){				
					$sendableID = $dest->getSendableID();					
					$sqlQuery3 = "INSERT INTO tblMessageToAttribute (`messageID`,`recipientID`,`messageType`) " .
								"VALUES ($messID, $sendableID, 4)";
					$this-> rs3-> Execute($sqlQuery3);
				}
			}			
			else{
				$sqlQuery3 = "INSERT INTO tblMessageToAttribute (`messageID`,`messageType`) VALUES ($messID, 4)";
				$this-> rs3-> Execute($sqlQuery3);
			}
			
			// Insert parentID and listingID in tblMessageToInquiry			
			$sqlQuery4 = "INSERT INTO tblMessageToInquiry (`messageID`,`listingID`,`parentID`) " .
						"VALUES ($messID, $listingID, $parentID)";
			$this-> rs4-> Execute($sqlQuery4);
			
			//if (0 >= sizeof($destination))
			//	InquiryMessage::sendEmail($_message);
			
			$EmailNotification = new InquiryMessageNotification();
			$EmailNotification-> Create($_message);
			$EmailNotification-> Send();

			return $messID;
		}

		/**
		 * Function name: Delete
		 * @param integer $_attributeID
		 * Sets trashed of message with attributeID = $_attributeID to 1
		 */		 
		function Delete($_attributeID = 0){
			
			// Update row in table tblMessages			
			$sqlQuery = "UPDATE tblMessageToAttribute " .
						"SET `trashed` = 1, " .
						"`isRead` = 0 " .
						"WHERE `attributeID` = " . $_attributeID;
			$this-> rs-> Execute($sqlQuery);
		}		
		
		/***************************** START: SETTERS *************************************/
		function setInstance($_instance = 0){
			$this->mInstance = $_instance;
		}
		function setTotalRecords($_totalRecords = 0){
			$this->mTotalRecords = $_totalRecords;
		}
		function setSendableId($_sendableID = 0){
			$this->mSendableId = $_sendableID;
		}
		function setInboxOwner($_owner = 0){
			$this->mInboxOwner = $_owner;
		}
		/******************************* END: SETTERS *************************************/
		
		/**
		 * Function name: getTotalRecords
		 * @return integer
		 */			
		function getTotalRecords(){
			return $this-> totalRecords;
		}
		
		
		/* =================================== FOR STUDENT / ADMIN VIEW ( LIKE COMMUNICATIONS SYSTEM ) ================================== */
		
		/**
		 * Function name: getListings1
		 * @param integer $_studentID
		 * @param mixed $_rowsLimit
		 * @return array listingIDs
		 */		 
		function getListings1($_studentID = 0, $_rowsLimit = NULL){
			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			
			if (0 == $_studentID)
				$whereClause = " ";
			else if (0 < $_studentID)
				$whereClause = " and a.`senderID` = ". $_studentID;
				
			$arrListings = array();

			// check if sendableID is Group or Traveler
			$Sendable = SendableFactory::Instance();
			$rSendable = $Sendable->Create($this->mSendableId);
			if ( $rSendable instanceof Traveler )
				$sendableID = $this->mSendableId;
			else
				$sendableID = $rSendable->getSendableID();

			// get listings			
			$sqlQuery = "SELECT DISTINCT SQL_CALC_FOUND_ROWS c.`listingID` " .
						"FROM tblMessages AS a, tblMessageToAttribute AS b, tblMessageToInquiry AS c " .
						"WHERE a.`discriminator` = 4 " .
						"AND b.`messageType` = 4 " .
						"AND b.`recipientID` = " . $sendableID . " " .
						"AND b.`trashed` = 0 ".	$whereClause . " " .
						"AND a.`messageID` = b.`messageID` " .
						"AND c.`messageID` = a.`messageID` " .
						"ORDER BY b.`isRead` ASC, a.`dateCreated` DESC ". $queryLimit;
			$this-> rs-> Execute($sqlQuery);
			
			$sqlQuery2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this-> rs2-> Execute($sqlQuery2);
			
			$this-> setTotalRecords ($this->rs2->Result(0, "totalRecords"));
			
			while($Listing = mysql_fetch_array($this->rs->Resultset())){
				$arrListings[] = $Listing['listingID'];
			}
				
			return $arrListings;
		}
		
		/**
		 * Function name: getListings2
		 * @param mixed $_rowsLimit
		 * @return array listingIDs
		 */
		 
		function getListings2($_rowsLimit = NULL){
			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			
			$arrListings = array();

			// check if sendableID is Group or Traveler
			$Sendable = SendableFactory::Instance();
			$rSendable = $Sendable->Create($this->mSendableId);
			if ( $rSendable instanceof Traveler )
				$sendableID = $this->mSendableId;
			else
				$sendableID = $rSendable->getSendableID();
			
			// get listings			
			$sqlQuery = "SELECT DISTINCT SQL_CALC_FOUND_ROWS c.`listingID` " .
						"FROM tblMessages AS a, tblMessageToAttribute AS b, tblMessageToInquiry AS c " .
						"WHERE a.`discriminator` = 4 " .
						"AND b.`messageType` = 4 " .
						"AND " .
							"(".
								"a.`senderID` = $sendableID OR b.`recipientID` = " . $sendableID .
							") " .
						"AND b.`trashed` = 0 " .
						"AND a.`messageID` = b.`messageID` " .
						"AND c.`messageID` = a.`messageID` " .
						"ORDERBY b.`isRead` ASC, a.`dateCreated` DESC ". $queryLimit;
			$this-> rs-> Execute($sqlQuery);
			
			$sqlQuery2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this-> rs2-> Execute($sqlQuery2);
			
			$this-> setTotalRecords ($this->rs2->Result(0, "totalRecords"));
						
			while($Listing = mysql_fetch_array($this->rs->Resultset())){
				$arrListings[] = $Listing['listingID'];
			}
				
			return $arrListings;
		}
				
		/**
		 * Function name: getNumberOfNewMessagesPerListing
		 * @param integer $_listingID
		 * @return integer number of new messages with listingID = $_listingID
		 */
		 
		function getNumberOfNewMessagesPerListing($_listingID = 0){
			
			// Get number of new messages where listingID is $_listingID			
			$sqlQuery = "SELECT b.`attributeID` " .
						"FROM tblMessages AS a, tblMessageToAttribute AS b, tblMessageToInquiry AS c " .
						"WHERE a.`discriminator` = 4 " .
						"AND b.`recipientID` = " . $this->mSendableId . " " .
						"AND b.`trashed` = 0 " .
						"AND b.isRead = 0 " .
						"AND b.`messageType` = 4 " .
						"AND c.`listingID` = " . $_listingID . " " .
						"AND a.`messageID` = c.`messageID` " .
						"AND b.`messageID` = a.`messageID` " .
						"GROUP BY b.`messageID`";
			$this-> rs-> Execute($sqlQuery);

			return $this->rs->Recordcount();
		}
		
		/**
		 * Function name: getStudents
		 * @param integer $_listingID
		 * @param mixed $_rowsLimit
		 * @return array students who sent inquiries to Admin
		 */
		 
		function getStudents($_listingID = 0, $_rowsLimit = NULL){

			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			
			else{
				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			
			if (0 == $_listingID)
				$whereClause = " ";
			else if (0 < $_listingID)
				$whereClause = " and c.`listingID` = ". $_listingID;
			
			$arrStudents = array();
						
			// get travelerID of students who sent inquiries to administrator
			
			$sqlQuery = "select distinct SQL_CALC_FOUND_ROWS d.`travelerID` from tblMessages as a, tblMessageToAttribute as b, tblMessageToInquiry as c, " .
					"tblTraveler as d where a.`discriminator` = 4 and b.`messageType` = 4 and b.`recipientID` = $this->mSendableId and b.`trashed` = 0 " .
					$whereClause. " and d.`sendableID` = a.`senderID` and a.`messageID` = b.`messageID` and c.`messageID` = a.`messageID` order by " .
					"b.`isRead` asc, a.`dateCreated` desc ". $queryLimit;
			$this-> rs-> Execute($sqlQuery);

			$sqlQuery2 = "select FOUND_ROWS() as totalRecords";
			$this-> rs2-> Execute($sqlQuery2);
			
			$this-> setTotalRecords ($this->rs2->Result(0, "totalRecords"));
			
			while($Student = mysql_fetch_array($this->rs->Resultset())){
				$arrStudents[] = $Student['travelerID'];
			}
				
			return $arrStudents;
		}
		
		/**
		 * Function name: getNumberOfNewMessagesPerStudent
		 * @param integer $_studentID
		 * @return integer number of new messages with studentID = $_studentID
		 */
		 
		function getNumberOfNewMessagesPerStudent($_studentID = 0){
			
			// Get number of new messages where studentID is $_studentID
			
			$sqlQuery = "select distinct b.`attributeID` from tblMessages as a, tblMessageToAttribute as b where a.`discriminator` = 4 and (a.`senderID` = $_studentID " .
					"OR b.`recipientID` = $this->mSendableId ) and b.`trashed` = 0 and b.isRead = 0 and b.`messageType` = 4 and b.`messageID` = a.`messageID`";
			$this-> rs-> Execute($sqlQuery);

			return $this->rs->Recordcount();
		}
				
		/**
		 * Function name: getAdminMessagesPerStudentAndListing
		 * @param integer $_listingID
		 * @param integer $_studentID
		 * @param RowsLimit $_rowsLimit
		 * @return array|NULL|exception phpDocumentor array of Messages or Null or exception
		 */
		 
		function getAdminMessagesPerStudentAndListing($_listingID = 0, $_studentID = 0, $_rowsLimit = NULL){

			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			
			else{
				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			
			$messages = array();
						
			// Get messages per student and per listing
			
			$sqlQuery = "select SQL_CALC_FOUND_ROWS b.`attributeID` from tblMessages as a, tblMessageToAttribute as b, tblMessageToInquiry as c where " .
					"a.`discriminator` = 4 and (a.`senderID` = $_studentID OR b.`recipientID` = $this->mSendableId) and b.`messageType` = 4 and b.`trashed` = 0 " .
					"and c.`listingID` = $_listingID and c.`parentID` = 0 and a.`messageID` = b.`messageID` = c.`messageID` AND a.`messageID` = c.`messageID`" .
					"order by a.`dateCreated` desc ". $queryLimit;
			$this-> rs-> Execute($sqlQuery);
			
			$sqlQuery2 = "select FOUND_ROWS() as totalRecords";
			$this-> rs2-> Execute($sqlQuery2);
			
			$this-> setTotalRecords ($this->rs2->Result(0, "totalRecords"));
						
			while ($resMessage = mysql_fetch_array ($this->rs->Resultset())){
				
				$Message = new InquiryMessage($resMessage['attributeID'], $this->mSendableId);
				$messages[] = $Message;
			}
			
			return $messages;
		}
		
		/**
		 * Function name: getNumberOfNewAdminMessagesPerStudentAndListing
		 * @param integer $_studentID
		 * @param integer $_listingID
		 * @return integer number of new messages with listingID = $_listingID and studentID = $_studentID
		 */
		 
		function getNumberOfNewAdminMessagesPerStudentAndListing($_studentID = 0, $_listingID = 0){

			// Get new messages
			
			$sqlQuery = "select b.`attributeID` from tblMessages as a, tblMessageToAttribute as b, tblMessageToInquiry as c where a.`discriminator` = 4 and " .
					"(a.`senderID` = $_studentID OR b.`recipientID` = $this->mSendableId) and b.`messageType` = 4 and b.`isRead` = 0 and b.`trashed` = 0 and " .
					"c.`listingID` = $_listingID and b.`messageID` = a.`messageID` and c.`messageID` = b.`messageID`";
			$this-> rs-> Execute($sqlQuery);

			return $this->rs->Recordcount();
		}
		
		/**
		 * Function name: getStudentMessagesPerStudentAndListing
		 * @param integer $_listingID
		 * @param RowsLimit $_rowsLimit
		 * @return array|NULL|exception phpDocumentor array of Messages or Null or exception
		 */
		 
		function getStudentMessagesPerStudentAndListing($_listingID = 0, $_rowsLimit = NULL){

			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			
			else{
				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			
			$messages = array();
						
			// Get messages per student and per listing
			
			$sqlQuery = "select SQL_CALC_FOUND_ROWS b.`attributeID` from tblMessages as a, tblMessageToAttribute as b, tblMessageToInquiry as c where " .
					"a.`discriminator` = 4 and a.`senderID` = $this->mSendableId and b.`messageType` = 4 and b.`trashed` = 0 and c.`listingID` = $_listingID " .
					"and c.`parentID` = 0 and a.`messageID` = c.`messageID` and b.`messageID` = a.`messageID` order by a.`dateCreated` desc ". $queryLimit;
			$this-> rs-> Execute($sqlQuery);

			$sqlQuery2 = "select FOUND_ROWS() as totalRecords";
			$this-> rs2-> Execute($sqlQuery2);
			
			$this-> setTotalRecords ($this->rs2->Result(0, "totalRecords"));
						
			while ($resMessage = mysql_fetch_array ($this->rs->Resultset())){
				
				$Message = new InquiryMessage($resMessage['attributeID'], $this->mSendableId);
				$messages[] = $Message;
			}
			
			return $messages;
		}
		
		/**
		 * Function name: getNumberOfNewStudentMessagesPerStudentAndListing
		 * @param integer $_listingID
		 * @return integer number of new messages with listingID = $_listingID and studentID = $_studentID
		 */
		 
		function getNumberOfNewStudentMessagesPerStudentAndListing($_listingID = 0){

			// Get new messages
			
			$sqlQuery = "select b.`attributeID` from tblMessages as a, tblMessageToAttribute as b, tblMessageToInquiry as c where a.`discriminator` = 4 and " .
					"b.`messageType` = 4 and b.`isRead` = 0 and b.`trashed` = 0 and b.`recipientID` = $this->mSendableId and c.`listingID` = $_listingID and " .
					"b.`messageID` = a.`messageID` and c.`messageID` = b.`messageID`";
			$this-> rs-> Execute($sqlQuery);

			return $this->rs->Recordcount();
		}
		
		
		/* ==================================== FOR MESSAGING SYSTEM ( LIKE PERSONAL MESSAGES ) ====================================== */
		

		/**
		 * Function name: getNumberOfNewMessages
		 * @return integer number of new messages
		 */	
		 
		function getNumberOfNewMessages(){
			
			// Get record count of new messages
			
			$sqlQuery = "select b.`attributeID` from tblMessages as a, tblMessageToAttribute as b where a.`discriminator` = 4 and b.`isRead` = 0 and " .
					"b.`trashed` = 0 and b.`recipientID` = $this->mSendableId and b.`messageType` = 4 and b.`messageID` = a.`messageID` group by b.`attributeID`";					
			$this-> rs-> Execute($sqlQuery);

			return $this->rs->Recordcount();
		}
		
		/**
		 * Function name: getMessagesPerListing
		 * @param integer $_listingID
		 * @param RowsLimit $_rowsLimit
		 * @return array|NULL|exception phpDocumentor array of Messages or Null or exception
		 */
		 
		function getMessagesPerListing($_listingID = 0, $_rowsLimit = NULL){

			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			
			else{
				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			
			$messages = array();
						
			// Get messages per listing
			
			$sqlQuery = "select distinct SQL_CALC_FOUND_ROWS b.`attributeID` from tblMessages as a, tblMessageToAttribute as b, tblMessageToInquiry as c where " .
					"a.`discriminator` = 4 and b.`messageType` = 4 and b.`trashed` = 0 and b.`recipientID` = $this->mSendableId and c.`listingID` = $_listingID " .
					"and a.`messageID` = c.`messageID` and b.`messageID` = a.`messageID` order by a.`dateCreated` desc ". $queryLimit;
			$this-> rs-> Execute($sqlQuery);

			$sqlQuery2 = "select FOUND_ROWS() as totalRecords";
			$this-> rs2-> Execute($sqlQuery2);
			
			$this-> setTotalRecords ($this->rs2->Result(0, "totalRecords"));
						
			while ($resMessage = mysql_fetch_array ($this->rs->Resultset())){
				
				$Message = new InquiryMessage($resMessage['attributeID'], $this->mSendableId);
				$messages[] = $Message;
			}
			
			return $messages;
		}
		
		/**
		 * Function name: getMessages
		 * @param RowsLimit $_rowsLimit
		 * @param string filter (filter to include messages. values 'all', 'read', 'unread')
		 * @return array|NULL|exception phpDocumentor array of Messages or Null or exception
		 */
		 
		function getMessages($_rowsLimit = NULL, $filter = 'all' ){
			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			
			$messages = array();
						
			// Get inquiry messages Note: Modified by K. Gordo to include filter in retrieving inquiry messages 
			switch ($filter) {			
				case 'read':
					$sqlQuery = "SELECT DISTINCT SQL_CALC_FOUND_ROWS b.`attributeID` " .
								"FROM tblMessages AS a, tblMessageToAttribute AS b " .
								"WHERE a.`discriminator` = 4 " .
								"AND b.`messageType` = 4 " .
								"AND b.`trashed` = 0 " .
								"AND b.`recipientID` = " . $this->mSendableId . " " .
								"AND b.`messageID` = a.`messageID` " .
								"AND b.isRead = 1 " .
								"ORDER BY a.`dateCreated` DESC ". $queryLimit;
					break;
				case 'unread':
					$sqlQuery = "SELECT DISTINCT SQL_CALC_FOUND_ROWS b.`attributeID` " .
								"FROM tblMessages AS a, tblMessageToAttribute AS b " .
								"WHERE a.`discriminator` = 4 " .
								"AND b.`messageType` = 4 " .
								"AND b.`trashed` = 0 " .
								"AND b.`recipientID` = " . $this->mSendableId . " " .
								"AND b.`messageID` = a.`messageID` " .
								"AND b.isRead = 0 " .
								"ORDER BY a.`dateCreated` DESC ". $queryLimit;
					break;	
				default:				
					$sqlQuery = "SELECT DISTINCT SQL_CALC_FOUND_ROWS b.`attributeID` " .
								"FROM tblMessages AS a, tblMessageToAttribute AS b " .
								"WHERE a.`discriminator` = 4 " .
								"AND b.`messageType` = 4 " .
								"AND b.`trashed` = 0 " .
								"AND b.`recipientID` = " . $this->mSendableId . " " .
								"AND b.`messageID` = a.`messageID` " .
								"ORDER BY a.`dateCreated` DESC ". $queryLimit;
					
			}
			$this-> rs-> Execute($sqlQuery);

			$sqlQuery2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this-> rs2-> Execute($sqlQuery2);
			
			$this-> setTotalRecords ($this->rs2->Result(0, "totalRecords"));
						
			while ($resMessage = mysql_fetch_array ($this->rs->Resultset())){				
				$Message = new InquiryMessage($resMessage['attributeID'], $this->mSendableId);
				$messages[] = $Message;
			}
			
			return $messages;
		}
	}
?>