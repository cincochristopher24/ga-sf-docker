<?php
/*
 * Created on Jul 26, 2006
 * Created by: Czarisse Daphne P. Dolina
 */
 	require_once('travellog/model/Class.Country.php');
 	require_once('travellog/model/Class.CommentContext.php');
 	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");
 	require_once('travellog/model/Class.Group.php');
	require_once 'travellog/model/Class.CommentNotification.php';
 	require_once 'Class.ConnectionProvider.php';
 	require_once 'Cache/ganetCacheProvider.php';
 	require_once('travellog/model/Class.TravelLogToVideo.php');		// added by neri: 10-08-08
	
	class TravelLog implements CommentContext {
 		
 		// vars for db connection
 		private $conn		= NULL;
 		private $rs			= NULL;
		private $rs2		= NULL;
 		
 		// vars for parent ID
 		private $travelerID  = NULL;
 		private $travelID	= NULL;
 		private $tripID      = NULL;
 		
 		//vars for parent classes
 		private $traveler    = NULL;
 		private $travel	     = NULL;
 		private $trip	     = NULL;
 		
 		private $owner    = NULL;
 		
 		// vars for class attributes
 		private $travellogID   = NULL;
 		private $title         = NULL;
 		private $publish       = 0;
 		private $description   = NULL;
 		private $callout       = NULL;
 		private $template_type = 0;
 		private $travellinkID  = NULL;
 		private $deleted  = 0;
 		private $logdate		= "0000-00-00 00:00:00";
		private $urlAlias	   = NULL;
		
 		// vars for other methods
 		private $randphoto    = NULL;
 		private $primphoto   = NULL;
 		private $arrphotos    = array();
 		private $arrsecondaryphotos    = array();
 		private $arrcomment    = array();
 		private $cache	=	null;
 		 		
 		// vars for static function
 		
 		private $arrtravellogs   = array();
 		private $arrfilteredtravellogs   = array();
 		private $countTravelLogs   = NULL;
 		
 		private static $statvar;
 		
 		// vars for static function (returns array of countries)
 		private $arrcountrieswithlogs = array();
 		
 		
 		public  $phototype	= 2;

		public static $totalEntries	= 0;

 		//constructor
 		
	 		function TravelLog($_travellogID = 0,$data=null){
	 			$this->travellogID = $_travellogID;
	 			$this->cache	=	ganetCacheProvider::instance()->getCache();
	 			
	 			try {
		 			$this->conn = ConnectionProvider::instance()->getConnection();
					$this->rs   = new Recordset($this->conn);
					$this->rs2	= new Recordset($this->conn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}
				
		 		if ($this->travellogID > 0){
		 			$found	=	false;
		 			if ($this->cache != null){
		 				$jEntry	=	$this->cache->get(get_class($this) . '_' . $this->travellogID);
		 				if ($jEntry) {
		 					$found = true;
		 					// set values to its attributes
			 				$this->title  		= $jEntry->getTitle(); 
				 			$this->description 	= $jEntry->getDescription(); 
				 			$this->callout    	= $jEntry->getCallOut();
				 			$this->publish    	= $jEntry->getPublish(); 
				 			$this->logdate 		= $jEntry->getLogDate(); 
				 			$this->template_type= $jEntry->getTemplateType(); 
				 			// set values to the parent ID
			 				$this->travelerID 	= $jEntry->getTravelerID(); 
			 				$this->travelID 		= $jEntry->getTravelID();
			 				$this->tripID  		= $jEntry->getTripID();
			 				
			 				$this->travellinkID 	= $jEntry->getTravellinkID();
							$this->urlAlias 	= $jEntry->getUrlAlias();
							
							if(!$this->urlAlias) $found = false;
		 				}
		 			}
		 			
		 			if (!$found) {
			 			$sql = "SELECT tblTravelLog.* , tblTravel.travelerID, tblTravel.travellinkID  FROM tblTravelLog, tblTravel WHERE tblTravelLog.deleted = 0 AND tblTravelLog.travelID = tblTravel.travelID AND tblTravel.deleted = 0 AND tblTravelLog.travellogID = '$this->travellogID'  "; // edited by ianne
			 			$this->rs->Execute($sql);
			 			
		 				if ($this->rs->Recordcount() == 0){
		 					throw new Exception("Invalid TravelLog ID: " . $_travellogID);		// id not valid so throw exception
		 				}	 	
		 				
		 				// set values to its attributes
		 				$this->title  		= stripslashes($this->rs->Result(0,"title")); 
			 			$this->description 	= stripslashes($this->rs->Result(0,"description")); 
			 			$this->callout    	= stripslashes($this->rs->Result(0,"callout"));
			 			$this->publish    	= $this->rs->Result(0,"publish"); 
			 			$this->logdate 		= $this->rs->Result(0,"logdate"); 
			 			$this->template_type= $this->rs->Result(0,"templateType"); 
			 			// set values to the parent ID
		 				$this->travelerID 	= $this->rs->Result(0,"travelerID"); 
		 				$this->travelID 		= $this->rs->Result(0,"travelID");
		 				$this->tripID  		= $this->rs->Result(0,"tripID");
						$this->urlAlias  	= $this->rs->Result(0,"urlAlias");
		 				
		 				$this->travellinkID 	= $this->rs->Result(0,"travellinkID");
				 		if ($this->cache != null) {
					 		$this->cache->set(get_class($this) . '_' . $this->travellogID,$this,86400);			
					 	}
		 			}
	 			}
				elseif(is_array($data)){
					$this->travellogID	= $data['travellogID'];
					$this->title 		= stripslashes($data['title']);
					$this->description 	= stripslashes($data['description']);
					$this->callout		= stripslashes($data['callout']);
					$this->publish		= $data['publish'];
					$this->logdate		= $data['logdate'];
					$this->template_type= $data['templateType'];
					$this->travelerID	= $data['travelerID'];
					$this->travelID		= $data['travelID'];
					$this->tripID		= $data['tripID'];
					$this->travellinkID = $data['travellinkID'];
					$this->urlAlias 	= $data['urlAlias'];
					if ($this->cache != null) {
				 		$this->cache->set(get_class($this) . '_' . $this->travellogID,$this,86400);			
				 	}
				}
				
		 		
	 		}
 		
 		//setters
 		
	 		function setTravelLogID($_travellogID){
	 			$this->travellogID = $_travellogID;
	 		}
	 		
	 		function setTitle($_title){
	 			$this->title = $_title;
	 		}
	 		
	 		function setPublish($_publish){
	 			$this->publish = $_publish; 			
	 		}
	 		
	 		function setDescription($_description){
	 			$this->description = $_description;
	 		}
	 		
	 		function setCallOut($_callout){
	 			$this->callout = $_callout;
	 		}
	 		
	 		function setLogDate($_logdate){
	 			$this->logdate = $_logdate;
	 		}
	 		
	 		function setTemplateType($_template_type){
	 			$this->template_type = $_template_type;
	 		}
	 		function setTravellinkID($_travellinkID){
	 			$this->travellinkID =  $_travellinkID;
	 		}	
	 		
	 		function setDeleted($_deleted){
	 			$this->deleted =  $_deleted;
	 		}
	
			function setUrlAlias($_alias){
				$this->urlAlias = $_alias;
			}
 		//getters
 		
			function getID(){
				return $this->getTravelLogID();
			}

 			function getTravelLogID(){
				return $this->travellogID;
			}
			
			function getTitle(){
				return $this->title;
			}
			
			function getPublish(){
	 			return $this->publish;
	 		}
			
			function getDescription(){
				return $this->description;
			}
			
			function getCallOut(){
				return $this->callout;
			}
			
			function getLogDate(){
				return $this->logdate;
			}
		
			function getTemplateType(){
	 			return $this->template_type;
	 		}
	 		
	 		function getTravellinkID(){
				//condition added by Jul, to make sure function won't return null
				if( is_null($this->travellinkID) ){
					try{
						$travel = $this->getTravel();
						if( $travel instanceof Travel ){
							$this->travellinkID = $travel->getTravelLinkID();
						}else{
							$this->travellinkID = 0;
						}
					}catch(exception $e){
						$this->travellinkID = 0;
					}
				}
	 			return $this->travellinkID;
	 		}	
	 		function getDeleted(){
	 			return $this->deleted;
	 		}
	
			function getUrlAlias(){
				return $this->urlAlias;
			}
		
		// setter of parent ID's
	 	
	 		function setTravelerID ($_travelerID){
	 			$this->travelerID = $_travelerID;	
	 		}
	 		
	 		function setTravelID($_travelID){
	 			$this->travelID = $_travelID;
	 		}
	 		
	 		function setTripID($_tripID){
	 			$this->tripID = $_tripID;
	 		}
	 		
	 	 		
	 		
	 	// getter of parent ID's
	 	
	 		function getTravelerID(){
				return $this->travelerID;
			}
	 		
	 		function getTravelID(){
	 			return $this->travelID ;
	 		}
	 		
	 		function getTripID(){
	 			return $this->tripID ;
	 		}
	 		
	 		
	 	// getter of parent classes
	 	
	 		function getTraveler(){
				
				if ($this->traveler == NULL){
		 			try {
		 				$this->traveler = new Traveler($this->travelerID);
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->traveler;
	 			
			}
	 		
	 		
	 		// get the owner of this travellog
	 		function getOwner(){
		 		
		 		if ($this->owner == NULL){
					if( is_null($this->travellinkID) ){
						$this->getTravellinkID();
					}
		 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 			$sql = "SELECT refID, reftype FROM tblTravelLink WHERE travellinkID = $this->travellinkID  " ;
	 				$this->rs->Execute($sql);
	 			
		 			try {
		 				if (1 == $this->rs->Result(0,"reftype"))
		 					$this->owner = new Traveler($this->rs->Result(0,"refID"));
		 				else if (2 == $this->rs->Result(0,"reftype")){
							$mGroup = GroupFactory::instance()->create( array($this->rs->Result(0,"refID")) );
							if( 0 == count($mGroup) ){
								throw new exception("unable to instantiate groupID: ".$this->rs->Result(0,"refID"));
							}
							$this->owner = $mGroup[0];
		 				}
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->owner;
	 			
	 		}	
	 		
	 		function getTravel(){
	 			
	 			if ($this->travel == NULL){
		 			try {
		 				$this->travel = new Travel ($this->travelID) ;
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->travel;	 	
	 			
	 		}
	 		
	 		function getTrip(){
	 			
	 			if ($this->trip == NULL){
		 			try {
		 				$this->trip = new Trip ($this->tripID);
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->trip;	 	
	 			
	 		}
	 		
		// CRUD methods
		
			function Create(){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = sprintf
						(
							"INSERT INTO tblTravelLog (publish, travelID, tripID, title, description, callout, templateType, urlAlias) " .
							"VALUES (%d, %d, %d, '%s', '%s', '%s', %d, '%s')"
							, $this->publish
							, $this->travelID
							, $this->tripID
							, mysql_real_escape_string($this->title)
							, mysql_real_escape_string($this->description)
							, mysql_real_escape_string($this->callout) 
							, $this->template_type
							, mysql_real_escape_string($this->formatUrlAlias()) 
						);
	 			$this->rs->Execute($sql);
	 			$this->travellogID = $this->rs->GetCurrentID();

				//added by Jul to create feed for this journal entry
				require_once("travellog/model/Class.ActivityFeed.php");
				try{
					ActivityFeed::create($this);
				}catch(exception $e){}

	 			//update travel's lastupdated field and journal entries count
	 			try {
	 				$travel = new Travel($this->travelID );
	 				$travel->setEntryCount($travel->getEntryCount() + 1);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				$travel->Update();
				$this->invalidateCacheEntry();
			}
			function Update(){
				
				$titl          = mysql_real_escape_string($this->title);
 				$desc          = mysql_real_escape_string($this->description);	
 				$callout       = mysql_real_escape_string($this->callout); 	
 				$template_type = $this->template_type;
 				$now           = date("Y-m-d H:i:s");
				// $alias         = mysql_real_escape_string($this->urlAlias);
				$alias 		   = mysql_real_escape_string($this->formatUrlAlias());

 				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$sql = "UPDATE tblTravelLog SET tripID = '$this->tripID', title = '$titl', description = '$desc', lastedited = '$now', callout = '$callout', templateType = '$template_type', urlAlias = '$alias'  WHERE travellogID = '$this->travellogID'" ;
	 			$this->rs->Execute($sql);
	 			
	 			try {
	 				$travel = new Travel($this->travelID );		//update travel's lastupdated field
	 			}	
				catch (Exception $e) {				   
				   throw $e;
				}
				//	$travel->Update();
		 		$this->invalidateCacheEntry();	
				$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;
	 		}
	 		
	 		/**
			* edits by ianne
			*	commented out delete queries.
			*	flagged as deleted instead of actually deleting travel logs.
			**/
	 		function Delete(){
	 			
	 			$myphotos = $this->getPhotos();		// get photos of travel log / journal entry
 				
 				for($idx_ph=0;$idx_ph<count($myphotos);$idx_ph++){
					$myphotos[$idx_ph]->Delete();						// loop thru the records and delete each one
				}
				
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				//$sql = "DELETE FROM tblTravelLog where travellogID = '$this->travellogID' ";			// delete the travel log entry
				$sql = "UPDATE  tblTravelLog SET deleted = 1 where travellogID = '$this->travellogID' ";			// delete the travel log entry
	 			$this->rs->Execute($sql);													
	 			
	 			try {
	 				$trip = new Trip ($this->tripID);
	 					
	 				if (count($trip->getTravelLogs()) == 0)					// check if trip has no more travel logs,
	 				{  		
	 					$trip->Delete();										// then delete the trip as well	 					
	 				}
	 				
	 				// reduce entryCount in Travel
	 				$travel = new Travel($this->travelID );
	 				$travel->setEntryCount($travel->getEntryCount() - 1);
	 				$travel->Update();
	 					
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				//added by Jul to delete feed of this journal entry
				require_once("travellog/model/Class.ActivityFeed.php");
				$JOURNALENTRY = ActivityFeed::JOURNALENTRY;
				$sql = "DELETE FROM tblActivityFeed
						WHERE feedSection = $JOURNALENTRY AND sectionID = '$this->travellogID'";
				$this->rs->Execute($sql);
				
				$this->invalidateCacheEntry();
	 		}
	 		 
	 		function Archive(){
	 			try {
	 				
	 				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
					$sql = "UPDATE  tblTravelLog SET deleted = 1 where travellogID = '$this->travellogID' ";			// delete the travel log entry
	 				$this->rs->Execute($sql);
	 			
	 				$travel = new Travel($this->travelID );
	 				$travel->setEntryCount($travel->getEntryCount() - 1);
					if($this->TravelLogContainsPrimaryPhoto($travel->getPrimaryphoto())){
						$travel->setPrimaryphoto(0);
						$randphoto = $travel->getRandomPhoto();
						if($randphoto instanceOf Photo)
							$travel->setPrimaryphoto($randphoto->getPhotoID());
					}
	 				$travel->Update($travel->getLastUpdated());
	
					// if Journal is empty remove from featured and approved lists
					if($travel->getEntryCount() == 0){
						$sql = "DELETE FROM tblGroupApprovedJournals where travelID = '$this->travelID' ";
			 			$this->rs->Execute($sql);	

			 			$sql = "DELETE FROM tblGroupFeaturedJournals where travelID = '$this->travelID' ";
			 			$this->rs->Execute($sql);
					}
					
					$params = array('TRAVELLOG_ID' => $this->travellogID);
					$travel->deleteRelatedPhotos($params);	
										
					
					//added by Jul to remove feeds of this journal entry
					require_once("travellog/model/Class.ActivityFeed.php");
					$JOURNALENTRY = ActivityFeed::JOURNALENTRY;
					$sql = "DELETE FROM tblActivityFeed 
							WHERE feedSection = $JOURNALENTRY AND sectionID = '$this->travellogID'";
					$this->rs->Execute($sql);	

					$this->invalidateCacheEntry();
					$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}					
	 		} 	
	 		
	 	// other methods
				 		
	 		// get random photo from the specified journal entry
	 		function getRandomPhoto(){
	 				
	 			if ($this->randphoto == NULL){
	 				
	 				// get the number of photos to choose from
	 				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 				$sql = "SELECT count(photoID) as totalrec from tblTravelLogtoPhoto WHERE travellogID = '$this->travellogID' ";
	 				$this->rs->Execute($sql);
	 				
	 				if ($this->rs->Result(0,"totalrec") == 0){
	 					try {
	 						$this->randphoto = new Photo($this);
	 					}
						catch (Exception $e) {						  
						   throw $e;
						}
	 				}	 				
	 				else {
	 					
	 					$max = $this->rs->Result(0,"totalrec") - 1;
						
						$randidx = mt_rand(0 , $max );	 		// the index starting from top row
						
	 					$sql = "SELECT photoID FROM tblTravelLogtoPhoto WHERE travellogID = '$this->travellogID' ORDER BY photoID LIMIT $randidx , 1"  ;
	 					$this->rs->Execute($sql);
	 					
	 					while ($recordset = mysql_fetch_array($this->rs->Resultset())) {
			 				try {	
			 					$this->randphoto = new Photo($this, $recordset['photoID'] );
			 				}
							catch (Exception $e) {						  
							   throw $e;
							}
	 					}
	 				}
	 			}
	 			
	 			return $this->randphoto;
	 		}
	 		
	 		
	 		// get primary photo of travellog
	 		function getPrimaryPhoto(){
	 			/*$this->cache = ganetCacheProvider::instance()->getCache();
	 			if ($this->primphoto == NULL){
	 				$found = false;
	 				if ($this->cache != null){
	 					$this->primphoto	=	$this->cache->get('Travellog_PrimaryPhoto_' . $this->travellogID);
	 					if ($this->primphoto){
	 						$found = true;
	 					}
	 				}
	 				
	 				if (!$found){
		 				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 				$sql = "SELECT tblPhoto.photoID from tblTravelLogtoPhoto, tblPhoto WHERE tblTravelLogtoPhoto.photoID = tblPhoto.photoID AND tblPhoto.primaryphoto = 1 AND tblTravelLogtoPhoto.travellogID = '$this->travellogID' ";
		 				$this->rs->Execute($sql);
		 				
		 				$_photoID = 0;	 
		 				if ($this->rs->Recordcount())	// if no photo records yet, set to default value zero
							$_photoID = $this->rs->Result(0,"photoID"); 			
							
						try {	
							$this->primphoto = new Photo($this, $_photoID);

							if ($this->cache != null){
								$this->cache->set('Travellog_PrimaryPhoto_' . $this->travellogID,$this->primphoto,86400 );
							}
						}
						catch (Exception $e) {
						   throw $e;
						}	
	 				}	
	 			}*/
	
				if(is_null($this->primphoto)){
					$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 				$sql = "SELECT tblPhoto.photoID from tblTravelLogtoPhoto, tblPhoto WHERE tblTravelLogtoPhoto.photoID = tblPhoto.photoID AND tblPhoto.primaryphoto = 1 AND tblTravelLogtoPhoto.travellogID = '$this->travellogID' ";
	 				$this->rs->Execute($sql);
 				
	 				$_photoID = 0;	 
	 				if ($this->rs->Recordcount())	// if no photo records yet, set to default value zero
						$_photoID = $this->rs->Result(0,"photoID"); 			
					
					try {	
						$this->primphoto = new Photo($this, $_photoID);
					}
					catch (Exception $e) {
					   throw $e;
					}
				}
	 			
	 			return $this->primphoto;
	 		}
	 		
	 		
	 		// get all secondary photos of the specified journal entry
	 		function getSecondaryPhotos($rowslimit = null){
				if (count($this->arrsecondaryphotos) == 0){
					
					$limitstr  = '';
					
					if ($rowslimit){
			 			$offset = $rowslimit->getOffset();
			 			$rows =  $rowslimit->getRows();
			 			$limitstr = " LIMIT " . $offset . " , " . $rows;
			 		}	
		 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 			$sql = "SELECT tblPhoto.photoID from tblTravelLogtoPhoto, tblPhoto WHERE tblTravelLogtoPhoto.photoID = tblPhoto.photoID AND travellogID = '$this->travellogID' AND tblPhoto.primaryphoto <> 1 " . $limitstr;
	 				$this->rs->Execute($sql);
	 				
	 				if ($this->rs->Recordcount() == 0){
	 					return NULL;	 			
	 				}
		 			
		 			else {
			 			while ($recordset = mysql_fetch_array($this->rs->Resultset())) {
		 					try {
		 						$photo = new Photo($this, $recordset['photoID']);
		 					}
							catch (Exception $e) {						  
							   throw $e;
							}
		 					$this->arrsecondaryphotos[] = $photo;
		 				}
		 			}
				}
				
				return $this->arrsecondaryphotos;
			} 
			
			
	 		// get all photos of the specified journal entry
	 		function getPhotos($rowslimit = null){
				
				//if (count($this->arrphotos) == 0){
					
					$this->arrphotos = array();
					$limitstr  = '';
					
					if ($rowslimit){
			 			$offset = $rowslimit->getOffset();
			 			$rows =  $rowslimit->getRows();
			 			$limitstr = " LIMIT " . $offset . " , " . $rows;
			 		}	
		 			
		
					/***
		 			if($this->travellogID){
			 			$sql = "SELECT photoID from tblTravelLogtoPhoto WHERE travellogID = '$this->travellogID' ORDER BY position DESC "  . $limitstr;
		 				$this->rs->Execute($sql);
		 				
		 				if ($this->rs->Recordcount() == 0){
		 					return NULL;	 			
		 				}
			 			
			 			else {
				 			while ($recordset = mysql_fetch_array($this->rs->Resultset())) {
			 					try {
			 						//$photo = new Photo($this, $recordset['photoID']);
									
			 					}
								catch (Exception $e) {						  
								   throw $e;
								}
			 					$this->arrphotos[] = $photo;
			 				}
			 			}
		 			}
					***/
					if($this->travellogID){
						$this->arrphotos = Photo::getTravelLogPhotos($this,$limitstr);
					}
				//}
				
				return $this->arrphotos;
			} 
			
			function getNotPrimaryPhotos(){
	 			
	 			if (count($this->arrphotos) == 0){
	 				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 				$sql = 	'SELECT tblPhoto.* '.
					'FROM tblPhoto, tblTravelLogtoPhoto '.
					'WHERE tblTravelLogtoPhoto.photoID = tblPhoto.photoID ' .
					'AND tblTravelLogtoPhoto.primary = 0'.
					"AND tblTravelLogtoPhoto.travellogID = ".$this->travellogID.' '.
					'ORDER BY tblTravelLogtoPhoto.position DESC';
	 				$this->rs->Execute($sql);
	 				
	 				
	 				if ($this->rs->Recordcount() == 0){
	 					return NULL;	 	
	 				}
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {	
	 						$photo = new Photo($this, 0,$recordset);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$this->arrphotos[] = $photo;
	 				}
	 			
	 			}
	 			
	 			return $this->arrphotos;
	 		}
			
			
			/*** Added by naldz (June 06, 2008) ***/
			public function getLastFeaturedDate(){
				require_once('gaLogs/Reader/Class.Reader.php');
				$reader = new Reader();
				$sql = 	'SELECT _EXECUTION_DATE_ as lastFeaturedDate '.
						'FROM Travel_Logs.tblFeaturedItems '.
						'WHERE _NEW_sectionID = 4 '.
						'AND _NEW_genID = '.GaString::makeSqlSafe($this->getTravelLogID()).' '.
						'AND _COMMAND_ = "INSERT" '.
						'ORDER BY lastFeaturedDate DESC ';
				$rs = $reader->getLogsBySql($sql);
				if($rs->retrieveRecordCount() > 0){
					return $rs->getLastFeaturedDate(0);
				}
				return null;
			}

			/*** ##### Added By: Cheryl Ivy Q. Go	1 October 2008 ##### ***/
			function getDatesEntryIsFeatured(){
				require_once('gaLogs/Reader/Class.Reader.php');
				$mReader = new Reader();
				$mDates = array();

				$sql = 	'SELECT _EXECUTION_DATE_ as lastFeaturedDate ' .
						'FROM Travel_Logs.tblFeaturedItems ' .
						'WHERE _NEW_sectionID = 4 ' .
						'AND _NEW_genID = ' . GaString::makeSqlSafe($this->getTravelLogID()) . ' ' .
						'AND _COMMAND_ = "INSERT" ' .
						'ORDER BY lastFeaturedDate DESC ';
				$rs = $mReader->getLogsBySql($sql);

				if(0 < $rs->retrieveRecordCount())
					$mDates = $rs->retrieveColumn('lastFeaturedDate');

				return $mDates;
			}
			
		// static functions
		
			// get the travellogs in specified location
	 		public static function getTravelLogsInLocation($_locationID){
	 			
	 			try {
					$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT tblTravel.travelerID, tblTravelLog.travellogID FROM tblTravel , tblTrips , tblTravelLog WHERE tblTravel.travelID = tblTrips.travelID AND  tblTrips.tripID = tblTravelLog.tripID AND  tblTravelLog.deleted = 0 AND tblTravel.deleted = 0 AND tblTrips.locID = '$_locationID' " ;
 				$rs->Execute($sql);
	 			
	 			if ($rs->Recordcount() == 0){
 					return NULL;	 	
 				}
 				
 				else {
		 			while ($recordset = mysql_fetch_array($rs->Resultset())) {
		 				try {
	 						$travellogsinlocation = new TravelLog ($recordset['travellogID']); 												
	 					} 					
						catch (Exception $e) {
						   throw $e;
						   
						}
						
	 					$arrtravellogs[] = $travellogsinlocation;
	 				}
 				}
 				return $arrtravellogs;
		 				
	 		}
	 		
			
			// get travellogs based on filter conditions
			public static function getFilteredTravelLogs($filtercriteria = null, $rowslimit = null, $criteria = null){
	 			
	 			try {
					$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
					$rs2   = new Recordset(ConnectionProvider::instance()->getConnection());
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$condition = '';
				$limitstr  = '';
				$arrfilteredtravellogs = array();
				
				$strCriteria2 = (null != $criteria) ? $criteria->getCriteria2() : '' ;
				
				if ($filtercriteria) {
					$myconditions = $filtercriteria->getConditions();
	 			
	 				if ($myconditions) {
			 			$attname = $myconditions[0]->getAttributeName();
		 				$operation = $myconditions[0]->getOperation();
		 				
			 			switch ($operation){
			 				case FilterOp::$ORDER_BY :
			 					$condition = " ORDER BY " . $attname ;
			 					break;
			 				case FilterOp::$ORDER_BY_DESC :
			 					$condition = " ORDER BY " . $attname . " DESC " ;
			 					break;
			 			}
	 				}
				}
					
		 		if ($rowslimit){
		 			$offset = $rowslimit->getOffset();
		 			$rows =  $rowslimit->getRows();
		 			$limitstr = " 	LIMIT " . $offset . " , " . $rows;
		 		}	

				/***
	 			SELECT tblTravelLog.* , tblTravel.travelerID, tblTravel.travellinkID  
				FROM tblTravelLog, tblTravel 
				WHERE tblTravelLog.travelID = tblTravel.travelID 
				AND tblTravelLog.travellogID = '$this->travellogID' 
		 		***/
		
				/***
		 		$sql = "SELECT SQL_CALC_FOUND_ROWS tblTravelLog.travellogID, tblTravel.travelerID 
						FROM tblTravel, tblTravelLog 
						WHERE tblTravel.travelID = tblTravelLog.travelID " . $strCriteria2 . $condition . $limitstr;
				***/
				
				
		 		
		 		$sql = "SELECT SQL_CALC_FOUND_ROWS tblTravelLog.* , tblTravel.travelerID, tblTravel.travellinkID  
						FROM tblTravel, tblTravelLog 
						WHERE tblTravel.travelID = tblTravelLog.travelID AND tblTravelLog.deleted = 0 AND tblTravel.deleted = 0  
						$strCriteria2 $condition $limitstr";
				
		 		$rs->Execute($sql);
		 		
		 		$sql2 = "SELECT FOUND_ROWS() as totalrec";
		 		$rs2->Execute($sql2);
		 		
		 		TravelLog::$statvar = $rs2->Result(0, "totalrec");
		 		
 				while ($recordset = mysql_fetch_array($rs->Resultset())) {
	 				try {
 						//$filteredtravellogs = new TravelLog ($recordset['travellogID']);
						$filteredtravellogs = new TravelLog (0,$recordset);
 					} 					
					catch (Exception $e) {
					   throw $e;
					   
					}
					
 					$arrfilteredtravellogs[] = $filteredtravellogs;
 				}
 				
 				return $arrfilteredtravellogs;
 				
	 		}
	
	 		public static function getTotalRec(){
	 			return TravelLog::$statvar;
	 		}	
	 		
	 		public static function getCountTravelLogs($_travelerID){
	 			try {
	 				require_once("travellog/model/Class.GanetDbHandler.php");
	 				require_once("Class.ResourceIterator.php");
					
	 				
	 				$handler = GanetDbHandler::getDbHandler();
	 				if(isset($GLOBALS['CONFIG']))
						$sql = "SELECT COUNT(DISTINCT(travellogID)) as count " .
		 					" FROM tblTravel, tblTrips, tblTravelLog, tblGroupApprovedJournals, tblTravelLink " .
		 					" WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
							" AND tblTravelLink.reftype = 1 " .
							" AND tblTravelLink.refID = " .$handler->makeSqlSafeString($_travelerID).
		 					" AND tblTravelLog.publish = 1 " .
		 					" AND tblTravelLog.deleted = 0 " .
							" AND tblTravelLog.travelID = tblGroupApprovedJournals.travelID " . 
							" AND tblGroupApprovedJournals.groupID = " .$GLOBALS['CONFIG']->getGroupID() .
							" AND tblGroupApprovedJournals.approved = 1 " .
		 					" AND tblTravel.publish = 1 " .
		 					" AND tblTravel.deleted = 0 " .
		 					" AND tblTravel.travelID = tblTrips.travelID " .
		 					" AND tblTrips.tripID = tblTravelLog.tripID ";
					else
		 				$sql = "SELECT COUNT(DISTINCT(travellogID)) as count " .
		 					" FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink " .
		 					" WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
							" AND tblTravelLink.reftype = 1 " .
							" AND tblTravelLink.refID = " .$handler->makeSqlSafeString($_travelerID).
		 					" AND tblTravelLog.publish = 1 " .
		 					" AND tblTravelLog.deleted = 0 " .
		 					" AND tblTravel.publish = 1 " .
		 					" AND tblTravel.deleted = 0 " .
		 					" AND tblTravel.travelID = tblTrips.travelID " .
		 					" AND tblTrips.tripID = tblTravelLog.tripID ";
	 				
	 				$rs = new ResourceIterator($handler->execute($sql));
	 				return $rs->getCount();
	 			}
	 			catch (exception $ex) {}
	 			
	 			/*
	 			try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
					$rs1  = clone($rs);				
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$countTravelLogs = 0;
					
		 		//$sql = "SELECT COUNT(tblTravelLog.travellogID) as totalrec FROM tblTravel, tblTravelLog WHERE tblTravel.travelID = tblTravelLog.travelID AND tblTravel.travelerId = '$_travelerID' " ;
		 		
		 		$sql = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS travellogID FROM tblTravel, tblTrips, tblTravelLog ' .
						'WHERE ' .
						'    tblTravel.travelID     = tblTrips.travelID ' .
						'AND tblTravel.travelerID   = %d ' .
						'AND tblTrips.tripID        = tblTravelLog.tripID ' .
						'AND tblTravelLog.publish   = 1 ' .
						'AND tblTravelLog.deleted   = 0 ' .
					    'AND tblTravel.publish      = 1 ' .
					    'AND tblTravel.deleted   	= 0 ' .	  
						'GROUP BY travellogID ' .
						'LIMIT 1'
						,$_travelerID
					);
		 			   
		 		
		 		$rs->Execute($sql);
		 		
		 		$sql1 = 'SELECT FOUND_ROWS() as totalrecords'; 
				$rs1->Execute($sql1);
		 		
		 		$countTravelLogs = $rs1->Result(0, 'totalrecords'); 
		 		
		 		//if ($rs->Result(0,"totalrec"))
		 		//	$countTravelLogs = $rs->Result(0, "totalrec");
		 		
		 		return $countTravelLogs;*/
 				
	 		}
	 		
	 		
	 	//static function that returns array of countries
	 		
	 		// get the countries with travel logs
			public static function getCountriesWithLogs($criteria = null){
				require_once('travellog/model/Class.LocationFactory.php');
				
				$cache	= ganetCacheProvider::instance()->getCache();
				$host	= $_SERVER['SERVER_NAME'];
				$found	= false;
				$arrcountrieswithlogs = array();
				if ($cache != null && $criteria == null) {
					$countryIDKeys	=	$cache->get($host . '_CountryIDsWithLogs');
					if ($countryIDKeys){
						$found = true;
						//$arrcountrieswithlogs	=	$cache->get($countryIDKeys);
						//echo 'Found country keys in cache <br/>';
						
						foreach ($countryIDKeys as $key){
							$arrcountrieswithlogs[]	=	$cache->get($key);
						}
					}
				}
				
				if (!$found){
					try {
						$conn = ConnectionProvider::instance()->getConnection();
						$rs   = new Recordset($conn);								
					}
					catch (Exception $e) {				   
					   throw $e;
					}
					
					$strCriteria2 = (null != $criteria) ? $criteria->getCriteria2() : '' ;
					
					/*$sql = "SELECT GoAbroad_Main.tbcountry.countryID " . 
					 	"FROM Travel_Logs.tblTrips,GoAbroad_Main.tbcountry " . 
					 	"WHERE Travel_Logs.tblTrips.locID = GoAbroad_Main.tbcountry.locID " . 
					 	"UNION " . 
					 	"SELECT countryID " .
					 	"FROM Travel_Logs.tblTrips,GoAbroad_Main.tbcity " . 
					 	"WHERE tblTrips.locID = tbcity.locID " . 
					 	"UNION " . 
					 	"SELECT countryID " . 
					 	"FROM tblTrips,tblRegion " . 
					 	"WHERE tblTrips.locID = tblRegion.locID " . 
					 	"GROUP BY countryID";*/
					/*$sql = "SELECT GoAbroad_Main.tbcountry.countryID " . 
					 	"FROM Travel_Logs.tblTrips, Travel_Logs.tblTravelLog, GoAbroad_Main.tbcountry " . 
					 	"WHERE Travel_Logs.tblTrips.locID = GoAbroad_Main.tbcountry.locID " . 
					 	"AND Travel_Logs.tblTrips.tripID = Travel_Logs.tblTravelLog.tripID " .
					 	"AND Travel_Logs.tblTravelLog.deleted = 0 " .
					 	"UNION " . 
					 	"SELECT countryID " .
					 	"FROM Travel_Logs.tblTrips, Travel_Logs.tblTravelLog, GoAbroad_Main.tbcity " . 
					 	"WHERE tblTrips.locID = tbcity.locID " . 
					 	"AND Travel_Logs.tblTrips.tripID = Travel_Logs.tblTravelLog.tripID " .
					 	"AND Travel_Logs.tblTravelLog.deleted = 0 " .
					 	"UNION " . 
					 	"SELECT countryID " . 
					 	"FROM tblTrips, Travel_Logs.tblTravelLog, tblRegion " . 
					 	"WHERE tblTrips.locID = tblRegion.locID " . 
					 	"AND Travel_Logs.tblTrips.tripID = Travel_Logs.tblTravelLog.tripID " .
					 	"AND Travel_Logs.tblTravelLog.deleted = 0 " .
					 	$strCriteria2 . 
					 	"GROUP BY countryID";*/
					 $sql = "SELECT GoAbroad_Main.tbcountry.countryID " . 
					 	"FROM Travel_Logs.tblTraveler, Travel_Logs.tblTravel, Travel_Logs.tblTrips, Travel_Logs.tblTravelLog, GoAbroad_Main.tbcountry " . 
					 	"WHERE Travel_Logs.tblTrips.locID = GoAbroad_Main.tbcountry.locID " .
					 	"AND Travel_Logs.tblTravel.travelID = Travel_Logs.tblTrips.travelID " .
					 	"AND Travel_Logs.tblTravel.travelID = Travel_Logs.tblTravelLog.travelID " . 
					 	"AND Travel_Logs.tblTrips.tripID = Travel_Logs.tblTravelLog.tripID " .
					 	"AND Travel_Logs.tblTravel.travelerID = Travel_Logs.tblTraveler.travelerID " .
					 	"AND Travel_Logs.tblTravelLog.deleted = 0 " .
					 	"AND Travel_Logs.tblTravel.deleted = 0 " .
					 	"AND Travel_Logs.tblTraveler.isSuspended = 0 " .
					 	"UNION " . 
					 	"SELECT countryID " .
					 	"FROM Travel_Logs.tblTraveler, Travel_Logs.tblTravel, Travel_Logs.tblTrips, Travel_Logs.tblTravelLog, GoAbroad_Main.tbcity " . 
					 	"WHERE tblTrips.locID = tbcity.locID " . 
					 	"AND Travel_Logs.tblTravel.travelID = Travel_Logs.tblTrips.travelID " .
					 	"AND Travel_Logs.tblTravel.travelID = Travel_Logs.tblTravelLog.travelID " . 
					 	"AND Travel_Logs.tblTrips.tripID = Travel_Logs.tblTravelLog.tripID " .
					 	"AND Travel_Logs.tblTravel.travelerID = Travel_Logs.tblTraveler.travelerID " .
					 	"AND Travel_Logs.tblTravelLog.deleted = 0 " .
					 	"AND Travel_Logs.tblTravel.deleted = 0 " .
					 	"AND Travel_Logs.tblTraveler.isSuspended = 0 " .
					 	"UNION " . 
					 	"SELECT countryID " . 
					 	"FROM Travel_Logs.tblTraveler, Travel_Logs.tblTravel, tblTrips, Travel_Logs.tblTravelLog, tblRegion " . 
					 	"WHERE tblTrips.locID = tblRegion.locID " . 
					 	"AND Travel_Logs.tblTravel.travelID = Travel_Logs.tblTrips.travelID " .
					 	"AND Travel_Logs.tblTravel.travelID = Travel_Logs.tblTravelLog.travelID " . 
					 	"AND Travel_Logs.tblTrips.tripID = Travel_Logs.tblTravelLog.tripID " .
					 	"AND Travel_Logs.tblTravel.travelerID = Travel_Logs.tblTraveler.travelerID " .
					 	"AND Travel_Logs.tblTravelLog.deleted = 0 " .
					 	"AND Travel_Logs.tblTravel.deleted = 0 " .
					 	"AND Travel_Logs.tblTraveler.isSuspended = 0 " .
					 	$strCriteria2 . 
					 	"GROUP BY countryID";	
							
					$rs->Execute($sql);
					
					if ($rs->Recordcount() == 0){
						return NULL;	 	
					}
					
					else {
						
						$locFactory	=	LocationFactory::instance();
						$countryIDKeys = array();
						while ($recordset = mysql_fetch_array($rs->Resultset())) {
							
							try {
								//$nCountry = new Country($recordset['countryID']);
								$nCountry	=	$locFactory->createCountry($recordset['countryID']);
								if($nCountry instanceOf Country){
									$arrcountrieswithlogs[] = $nCountry;
								}	
								//$countryIDKeys[] =	'Country_' . $recordset['countryID'];
							} catch (Exception $e) { }
										
						}
						
						// set the countryID keys to cache
						/*if ($cache != null){
							$cache->set($host . '_CountryIDsWithLogs',$countryIDKeys,3600);	
						}*/
						
					}
					
					usort($arrcountrieswithlogs,array("TravelLog","orderByCountryName"));
					
					$countryIDKeys = array();	
					foreach($arrcountrieswithlogs as $c){
						$countryIDKeys[] 	=	'Country_' . $c->getCountryID();
					}
					
					if ($cache != null){
						$cache->set($host . '_CountryIDsWithLogs',$countryIDKeys,3600);	
					}
					
				}
				
				return $arrcountrieswithlogs;
						
			}
			
		/**
		 * static function to get all travellogs given a traveler (added by K. Gordo)
		 * changed (2/15/08) {
		 		+ since groups can now have travellogs(journal entries) we will refer to travellinkID
		 
		 * }	
		 * changed (5/8/08) {		 	
		 *		+ added UNION in query to include rows in tblTraveltoGroup since this is where the relationship
		 *			of subgroup to journals resides	
		 *  }	
		 */	
	 	public static function getTravellogs($owner, $rl = NULL, $orderByArrival=false) {
	 		/****************************************************/
			/*	Last Edited By	: Cheryl Ivy Q. Go				*/
			/*	Date Edited		: 13 June 2008					*/
			/*	Purpose			: added reftype in query		*/
			/****************************************************/

	 		try {
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
			}
			catch (Exception $e){
				   throw $e;
			}
	 		
	 		$sql = "SELECT travellogID, tblTravelLog.title, tblTravelLog.description, tblTravelLog.callout, tblTravelLog.publish, tblTravelLog.logdate, tblTravelLog.templateType, tblTravelLog.tripID, tblTravel.travelerID, tblTravel.travelID, tblTravel.travellinkID " .
					"FROM tblTravelLog, tblTravel, tblTravelLink " . ($orderByArrival ? ',tblTrips ':' ').
					"WHERE tblTravelLog.travelID = tblTravel.travelID " .
					($orderByArrival ? 'AND tblTravelLog.tripID = tblTrips.tripID ': ' ').
					"AND tblTravelLog.deleted = 0 " .
					"AND tblTravel.deleted = 0 " .
					"AND tblTravel.travellinkID = tblTravelLink.travellinkID ";

	 		if ($owner instanceof Traveler) {
				$sql .= " AND tblTravelLink.reftype = 1 AND tblTravelLink.refID = " . $owner->getTravelerID();
			}
	 		if ($owner instanceof AdminGroup) {
				if( 0 == $owner->getParentID() ){
	 				$sGroups = $owner->getSubGroups();

		 			if (is_array($sGroups) && count($sGroups) > 0 ){
		 				$sGroupIds = '';
		 				foreach($sGroups as $sGroup)
		 					$sGroupIds .= $sGroup->getGroupID() . ',';
	 					 				
		 				$sql .= " AND tblTravelLink.reftype = 2 AND tblTravelLink.refID IN (" . $sGroupIds . $owner->getGroupID() . ")";	
		 			}
					else {
		 				$sql .= " AND tblTravelLink.reftype = 2 AND tblTravelLink.refID = " . $owner->getGroupID();
		 			}
				}else{
					$sql .= " AND tblTravelLink.reftype = 2 AND tblTravelLink.refID IN (" . $owner->getGroupID() . ",".$owner->getParentID().")";
				}

	 			if ($owner->getParentID() > 0) {
	 				// query if this is a subgroup
	 				$sql .= " UNION " .
							"SELECT travellogID, tblTravelLog.title, tblTravelLog.description, tblTravelLog.callout, tblTravelLog.publish, tblTravelLog.logdate, tblTravelLog.templateType, tblTravelLog.tripID, tblTravel.travelerID, tblTravel.travelID, tblTravel.travellinkID " .
							"FROM tblTravelLog, tblTravel, tblTraveltoGroup " . ($orderByArrival ? ',tblTrips ':' ').
							"WHERE tblTravelLog.travelID = tblTravel.travelID " .
							($orderByArrival ? 'AND tblTravelLog.tripID = tblTrips.tripID ': ' ').
							"AND tblTravelLog.deleted = 0 " .
							"AND tblTravel.deleted = 0 " .
							"AND tblTraveltoGroup.travelID = tblTravel.travelID " .
							"AND tblTraveltoGroup.groupID = " . $owner->getGroupID();
	 			}
	 		}	
			if( $orderByArrival )
				$sql = $sql . " ORDER BY travelID, tblTrips.arrival DESC, travellogID";
	 		else
				$sql = $sql . " ORDER BY travelID, travellogID";

	 		if (NULL != $rl)
	 			$sql = $sql . " LIMIT $rl->getOffset(), $rl->getRows()";

	 		$tlogs = array();	
	 		$rs->Execute($sql);	 		
	 		if ($rs->Recordcount() > 0){	
	 			while ($rsar = mysql_fetch_array($rs->Resultset())) {
	 				try {
	 					//$tlogs[] = new Travellog($rsar['travellogID']);
	 					$tlog = new Travellog();
	 					$tlog->setTravelLogID($rsar['travellogID']);
	 					$tlog->setTitle($rsar['title']);
	 					$tlog->setDescription($rsar['description']);
	 					$tlog->setCallOut($rsar['callout']);
	 					$tlog->setPublish($rsar['publish']);
	 					$tlog->setLogDate($rsar['logdate']);
	 					$tlog->setTemplateType($rsar['templateType']);
	 					$tlog->setTravelerID($rsar['travelerID']);
	 					$tlog->setTravelID($rsar['travelID']);
	 					$tlog->setTripID($rsar['tripID']);
	 					$tlog->setTravellinkID($rsar['travellinkID']);
	 					$tlogs[] = $tlog;
	 					
	 				} catch (Exception $e) {}
	 			}	 			
	 		}
	 		return $tlogs;				 	 		
	 	}
	 	
	 	
	 	public static function getRecentJournalEntries($date = null){
	 		
	 		try {
					$rs   = new Recordset(ConnectionProvider::instance()->getConnection());								
			} catch (Exception $e) {								   
				   throw $e;
			}	
	 		if(isset($_GET['memdebug'])) echo 'journal entries1: ',memory_get_usage(),'<br />';
	 		if (null == $date)
	 			$sql = "select travellogID from tblTravelLog ORDER BY logdate DESC";
	 		else			
	 			$sql = "select travellogID from tblTravelLog WHERE tblTravelLog.deleted = 0 AND (logdate >= '$date' OR lastedited >= '$date') ORDER BY logdate DESC";  
	 		$rs->Execute($sql);
	 		if(isset($_GET['memdebug'])) echo 'journal entries2: ',memory_get_usage(),'<br />';
	 		$arrRecent = array();
	 		
	 		if ($rs->Recordcount()){
	 			while ($rsar = mysql_fetch_array($rs->Resultset())) {
	 				try {
	 					$arrRecent[] = new TravelLog($rsar['travellogID']);
	 				} catch (Exception $e) {}
	 			}
	 		}
	 		if(isset($_GET['memdebug'])) echo memory_get_usage(),'<br />';
	 		return $arrRecent;
	 	}
		public static function getEmptyDatesJournalEntries(){
	 		
	 		try {
					$rs   = new Recordset(ConnectionProvider::instance()->getConnection());								
			} catch (Exception $e) {								   
				   throw $e;
			}	
	 		
	 		
 			$sql = "select travellogID from tblTravelLog, tblTrips WHERE tblTravelLog.deleted = 0 AND tblTravelLog.tripID = tblTrips.tripID AND tblTrips.arrival = '0000-00-00' ORDER BY logdate DESC";
	 		$rs->Execute($sql);
	 		
	 		$arrRecent = array();
	 		
	 		if ($rs->Recordcount()){
	 			while ($rsar = mysql_fetch_array($rs->Resultset())) {
	 				try {
	 					$arrRecent[] = new TravelLog($rsar['travellogID']);
	 				} catch (Exception $e) {}
	 			}
	 		}
	 		
	 		return $arrRecent;
	 		
	 	}

		/**
		 *	This function was created for the optimization of the entries.php page in the admin side of dotNet
		 *	@author		miss cig
		 *	@param		RowsLimit $_rowsLimit, $_date
		 *	@return		Array TravelLog
		 */
		public static function getPublishedRecentJournalEntries( RowsLimit $_rowslimit, $_date = null )
		{
			$mConn	= ConnectionProvider::instance()->getConnection();
			$mRs	= new Recordset( $mConn );
			$mRs2	= new Recordset( $mConn );

			$mQueryStr = "";
			if ( !is_null($_rowslimit) )
			{
				$mQueryStr	= " LIMIT " . $_rowslimit->getOffset() . ", " . $_rowslimit->getRows();
			}

			if ( null == $_date )
			{
	 			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTravelLog.*, tblTravel.travelerID, tblTravel.travellinkID " .
	 					"FROM tblTravelLog, tblTravel " .
						"WHERE tblTravelLog.travelID = tblTravel.travelID " .
						"AND tblTravelLog.deleted = 0 " .
						"AND tblTravelLog.publish = 1 " .
						"AND tblTravel.deleted = 0 " .
						"ORDER BY logdate DESC " . $mQueryStr;
			}
	 		else
			{
	 			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTravelLog.*, tblTravel.travelerID, tblTravel.travellinkID " .
	 					"FROM tblTravelLog, tblTravel " .
						"WHERE tblTravelLog.travelID = tblTravel.travelID " .
						"AND (tblTravelLog.logdate >= '$_date' OR tblTravelLog.lastedited >= '$_date') " .
						"AND tblTravelLog.deleted = 0 " .
						"AND tblTravelLog.publish = 1 " .
						"AND tblTravel.deleted = 0 " .
						"ORDER BY logdate DESC " . $mQueryStr;
			}
	 		$mRs->Execute( $sql );

			$sql2 = "SELECT FOUND_ROWS() as totalRecords";
			$mRs2->Execute( $sql2 );

			self::$totalEntries = $mRs2->Result(0, "totalRecords");

			$arrRecent = array();
	 		if ( 0 < $mRs->Recordcount() )
			{
	 			while ( $rsar = mysql_fetch_assoc($mRs->Resultset()) )
				{
	 				try
					{
	 					$arrRecent[] = new TravelLog( 0, $rsar );
	 				}
					catch ( Exception $e ) {}
	 			}
	 		}

	 		return $arrRecent;
		}

		/**
		 *	This function was created for the optimization of the entries.php page in the admin side of dotNet
		 *	@author		miss cig
		 *	@return		Array TravelLog
		 */
		public static function getPublishedEmptyDatesJournalEntries( RowsLimit $_rowslimit )
		{
	 		$mConn	= ConnectionProvider::instance()->getConnection();
			$mRs	= new Recordset( $mConn );
			$mRs2	= new Recordset( $mConn );	 		

			$mQueryStr = "";
			if ( !is_null($_rowslimit) )
			{
				$mQueryStr	= " LIMIT " . $_rowslimit->getOffset() . ", " . $_rowslimit->getRows();
			}

			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTravelLog.*, tblTravel.travelerID, tblTravel.travellinkID " .
 					"FROM tblTravelLog, tblTravel, tblTrips " .
					"WHERE tblTravelLog.travelID = tblTravel.travelID " .
					"AND tblTrips.tripID = tblTravelLog.tripID " .
					"AND tblTravelLog.deleted = 0 " .
					"AND tblTravelLog.publish = 1 " .
					"AND tblTravel.deleted = 0 " .
					"AND tblTrips.arrival = '0000-00-00' " .
					"ORDER BY logdate DESC " . $mQueryStr;
	 		$mRs->Execute( $sql );

			$sql2 = "SELECT FOUND_ROWS() as totalRecords";
			$mRs2->Execute( $sql2 );

			self::$totalEntries = $mRs2->Result(0, "totalRecords");

	 		$arrRecent = array();
	 		if ( 0 < $mRs->Recordcount() )
			{
	 			while ( $rsar = mysql_fetch_assoc($mRs->Resultset()) )
				{
	 				try
					{
	 					$arrRecent[] = new TravelLog( 0, $rsar );
	 				}
					catch (Exception $e) {}
	 			}
	 		}

	 		return $arrRecent;
	 	}
	 	
	 	// new functions
	 		
	 		
	        //Purpose: Adds photo for this travel log.
	         
            function addPhoto($photo){
                $this->rs = new Recordset(ConnectionProvider::instance()->getConnection());
                //editted by: joel
            	//################################
            	$sqlq = "SELECT max(position) as mposition from tblTravelLogtoPhoto WHERE travelLogID = '$this->travellogID'";
	 			$this->rs->Execute($sqlq);
            	
            	
            	if($this->rs->Recordcount() > 0){
            			$rec = mysql_fetch_array($this->rs->Resultset());
            			$pos = $rec['mposition']+1;           			
            	}else{
            			$pos =0;	
            	}
            	//################################
                
                
                $photoID = $photo->getPhotoID();   
                $sql = "INSERT into tblTravelLogtoPhoto (travelLogID, photoID, position) VALUES ('$this->travellogID', '$photoID', '$pos' )";
		 		$this->rs->Execute($sql);
		
				//added by Jul to create feeds for this uploaded photo
				require_once("travellog/model/Class.ActivityFeed.php");
				try{
					ActivityFeed::create($photo,$this);
				}catch(exception $e){}
				
				$this->invalidateCacheEntry();//invalidate cache entry for this journal entry	
            }
            
            // Purpose: Removes a photo from this photo album.
	        //  Deletes the photo as well as its link to this program.
	        function removePhoto($photoID,$dateUploaded){
                $this->rs 	=	new Recordset(ConnectionProvider::instance()->getConnection());
                $sql = "DELETE FROM tblTravelLogtoPhoto where photoID = '$photoID' ";
		 		$this->rs->Execute($sql);  
		 		
				$sql = "DELETE FROM tblPhotoAlbumtoPhoto where photoID = '$photoID' ";
		 		$this->rs->Execute($sql);
				
				//////////////////////////////////////////////
				//check if is set as primary in Travel
				/////////////////////////////////////////////
				
				require_once('travellog/model/Class.Travel.php');  
				$travel 	= new Travel($this->getTravelID());
				
				if($travel->getPrimaryphoto() == $photoID){
					$travel->setPrimaryphoto(false);
					$new_primary = $travel->getRandomPhoto();
					$travel->setPrimaryphoto($new_primary->getPhotoID());
					$travel->update();
				}
				
				/////////////////end//////////////////////////
				
				//Delete also Logs
				
				require_once('travellog/model/Class.JournalLog.php');
				require_once('travellog/model/Class.JournalLogType.php');
				$obj_log = new JournalLog;
				$obj_log->setLogType(JournalLogType::$ADDED_NEW_PHOTOS);
				$obj_log->setLinkID($photoID); //last photoID of Travellog
				$obj_log->Delete();
				
				
				//added by Jul to delete feeds of the photo
     			$uploadedByDayCount = $this->getPhotosUploadedByDay(NULL,$dateUploaded,TRUE);
				if( 0 == $uploadedByDayCount ){
					require_once("travellog/model/Class.ActivityFeed.php");	
					$params = array("FEED_SECTION"		=>	ActivityFeed::ENTRYPHOTO,
									"SECTION_ROOT_ID"	=>	$this->travellogID,
									"FEED_DATE"			=>	$dateUploaded );
					$photoFeeds = ActivityFeed::getInstances($params);
					if( 0 < $photoFeeds["recordCount"] ){
						$photoFeeds["activityFeeds"][0]->Delete();
					}
				}
				
				$this->invalidateCacheEntry();//invalidate cache entry for this journal entry
			}
			
			
			//added by: joel            
            function reArrangePhoto($marrphotoID){
            	$this->rs = new Recordset(ConnectionProvider::instance()->getConnection());
            	foreach($marrphotoID as $index => $photoID){
            			 $sql = "UPDATE tblTravelLogtoPhoto SET position = '$index' WHERE photoID =  '$photoID' ";
		 				$this->rs->Execute($sql);
            	}
				
				$cache	=	ganetCacheProvider::instance()->getCache();
				if ($cache != null){
						$cache->delete('Travellog_Photos_' . $this->getTravelLogID());
				}

            }
		
		/**
		 * Adds a comment to this travellog.
		 * 
		 * @param Comment $comment The comment to be added.
		 * @see Comment
		 * @throws exception
		 * @return void
		 */
    function addComment($comment){                
      $comment->Create();
      $handler = new dbHandler();
             
      $sql = "INSERT INTO " .
        " tblTravelLogtoComment(travellogID, commentID) " .
        " VALUES (" .
        "".$handler->makeSqlSafeString($this->travellogID).",".
        "".$handler->makeSqlSafeString($comment->getCommentID()).
        ")";
        
      try {
        $handler->execute($sql);
        $comment->setCommentType(3);
      }
      catch(exception $ex){
      	throw new exception($ex->getMessage());
      }
    }
            
    /**
     * Removes the given comment from this travel log (journal entry).
     * 
     * @param Comment $comment The comment to be removed
     * @see Comment
     * @return void
     */
    function removeComment($comment){
    	$handler = new dbHandler();         
      $commentID = $comment->getCommentID();
                
      $sql = "DELETE FROM tblTravelLogtoComment " .
        " WHERE travellogID = ".$handler->makeSqlSafeString($this->travellogID).
        " AND commentID = ".$handler->makeSqlSafeString($comment->getCommentID());    
      
      try {
        $handler->execute($sql);
      } 
      catch(exception $ex){
        throw new exception($ex->getMessage());
      }        
    }
            
    /**
     * Retrieves all the comment of this travel log (journal entry).
     * 
     * @param RowsLimit $rowslimit This is for the limit clause
     * @see RowsLimit
     * @param boolean $isPassport If this is true the comments that are retrieved are the comments whose author is not the owner of this travel log (journal entry). If false, the comments that will be retrieved are all the comments of this travel log (journal entry).
     * @throws exception
     * @return array Array of Comment object
     */     
    function getComments($rowslimit = NULL, $isPassport = false){
      $handler = new dbHandler();
      $arrComment = array();

			$sql = "SELECT DISTINCT tc.* " .
				" FROM tblTravelLogtoComment as ttlc, tblComment as tc " .
				" WHERE ttlc.commentID = tc.commentID ".
				" AND ttlc.travellogID = ".$handler->makeSqlSafeString($this->travellogID).
				" AND tc.isApproved = 1 ";
			$sql .= " ORDER BY tc.commentID DESC ";
			$sql .= (is_null($rowslimit)) ? "" : SqlUtil::createLimitClause($rowslimit);

			$rs = new iRecordset($handler->execute($sql));
				
      try {
        $rs = new iRecordset($handler->execute($sql));
        while (!$rs->EOF()) {
          $arrComment[] = new Comment($rs->current());
          try {
            $rs->next();
          }
          catch (exception $ex) {
          	throw new exception($ex->getMessage());
          }
        }
  		}
  		catch(exception $ex){
  			throw new exception($ex->getMessage());
  		}

		  return $arrComment;              
    }
    
    /**
     * Retrieves the number of comments of this travel log (journal entry).
     * 
     * @param boolean $isPassport If this is true the comments that are retrieved are the comments whose author is not the owner of this travel log (journal entry). If false, the comments that will be retrieved are all the comments of this travel log (journal entry).
     * @throws exception
     * @return integer
     */        
    function getNumberOfComments($isPassport = false) {
      $handler = new dbHandler();
      
      $sql = "SELECT COUNT(*) as cnt FROM (";
			$sql = "SELECT DISTINCT tc.* " .
				" FROM tblTravelLogtoComment as ttlc, tblComment as tc, tblTraveler as tt " .
				" WHERE ttlc.commentID = tc.commentID ".
				" AND ttlc.travellogID = ".$handler->makeSqlSafeString($this->travellogID).
				" AND (tc.author = 0 OR (tt.travelerID = tc.author AND tt.isSuspended = 0)) " .
				" AND tc.isApproved = 1 ";
		  $sql .= ($isPassport) ? " AND tc.author <> ".$handler->makeSqlSafeString($this->travelerID).")" : ")"; 
			
			try {				
			  $rs = new iRecordset($handler->execute($sql));
			  return $rs->getcnt();
			}
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}   	
    }
            
            
            /**
	 		 * Added by Joel 
	 		 * March 1 2007
	 		 * To check If travellog has primary photo
	 		 * @return Boolean 
	 		 */
            function checkHasprimaryphoto(){
	 			
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
	 			
	 			$sql = "SELECT primaryphoto from tblPhoto, tblTravelLogtoPhoto " .
	 					"WHERE tblTravelLogtoPhoto.photoID = tblPhoto.photoID " .
	 					"AND primaryphoto =1 " .
	 					"AND tblTravelLogtoPhoto.travellogID ='".$this->travellogID."'" ;
				$rs->Execute($sql);
				
				
				if($rs->recordcount() == 0){
					return false;	
				}else{
					return true;
				}
				        											
	 			
	 		}
          
          // user defined sort function to be called when sorting by countryname
		  function orderByCountryName($l,$r){
		  	
			if ($l->getName() == $r->getName())
			   return 0;
			elseif ($l->getName() < $r->getName())
				return -1;
			else 
				return 1;	  
		 } 
		 
		 /*
		  * Added By: Aldwin S. Sabornido
		  * Date    : August 8, 2007
		  */
		 function countPhotos(){
			$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql  = 'SELECT COUNT(photoID) AS totalphotos FROM tblTravelLogtoPhoto WHERE travellogID = '.$this->travellogID;
			$rs->Execute($sql);
			return $rs->Result(0,"totalphotos");
		 }         
		 
		 function permalink( Criteria2 $c ){
			$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql  = "SELECT travellogID FROM tblTravelLog WHERE tblTravelLog.deleted = 0 AND ". $c->getCriteria2();

			$rs->Execute($sql);
			$travellogID = $rs->Result(0,"travellogID");
			return ( $travellogID )? $travellogID: 0; 
		 }
		 
		 function PublishUnpublishEntry(){
		 	$value = ( $this->publish )? 0 : 1;
			$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql  = 'UPDATE tblTravelLog SET publish = ' .$value. ' WHERE travellogID = '.$this->travellogID; 
			$rs->Execute($sql);
			
			// if all journal entries are unpublished then the parent Travel(journal) should also be unpublished (K. Gordo)
			if ($value == 0){
				$travel = new Travel($this->travelID);
				$tlogs = array();
				$trips = $travel->getTrips();
				foreach($trips as $trip){
					$tlogs = array_merge($tlogs,$trip->getTravelLogs());
				}
				
				$pub = false;
				foreach($tlogs as $tlog){
					if ($tlog->getPublish()){
						$pub = true;
						break;
					}
				}
				
				if (!$pub){
					$travel->setPublish(0);
					$travel->Update();
				}
				
				// we also need to unfeature this journal entry if it is unpublished and it is featured in the homepage
				require_once('travellog/model/admin/Class.FeaturedSection.php');
				$fs = new FeaturedSection(FeaturedSection::$JOURNAL_ENTRY);
				$fs->unsetItemFeatured($this->travellogID);
				
			} else {
				// if parent Travel is unpublished then we set it to publish
				$travel = new Travel($this->travelID);
				$travel->setPublish(1);
				$travel->Update();
			}
			
		 }

		/********************** ADDED BY: Cheryl Ivy Q. Go		23 September 2008 ************************/
		/********************** PURPOSE: Get travellog photos for admin(features) ************************/
		public static function getAdminFeaturedTravelLogPhotos($_rowsLimit = null){
 			try {
				$mRs	= new Recordset(ConnectionProvider::instance()->getConnection());
				$mRs2	= new Recordset(ConnectionProvider::instance()->getConnection());
 			}
			catch (Exception $e) {		   
			   throw $e;
			}

			if ($_rowsLimit){
				$offset = $_rowsLimit-> getOffset();
	 			$rows =  $_rowsLimit-> getRows();
	 			$limitstr = " LIMIT " . $offset . " , " . $rows;
			}
			else
				$limitstr = '';

			$sql = "SELECT SQL_CALC_FOUND_ROWS tblPhoto.*, tblTravel.travelerID, tblTravelLog.travellogID, tblTravelLog.title " .
					"FROM tblTravel, tblTravelLog, tblTravelLogtoPhoto, tblPhoto " .
					"WHERE tblTravel.travelID = tblTravelLog.travelID " .
					"AND tblTravelLog.deleted = 0 " .
					"AND tblTravel.deleted = 0 " . 
					"AND tblTravelLogtoPhoto.photoID = tblPhoto.photoID " .
					"AND tblTravelLogtoPhoto.travellogID = tblTravelLog.travellogID " .
					"ORDER BY tblPhoto.dateuploaded DESC " . $limitstr;
	 		$mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() AS totalrecords";
			$mRs2->Execute($sql2);

			TravelLog::$statvar = $mRs2->Result(0, "totalrecords");
	 		$arrTravelLogPhotos = array();

			while ($recordset = mysql_fetch_assoc($mRs->Resultset())) {
				$TravelLog = new TravelLog();
				$TravelLog->setTravelLogID($recordset['travellogID']);
				$TravelLog->setTravelerID($recordset['travelerID']);
				$TravelLog->setTitle(stripslashes($recordset['title']));

				$Photo = new Photo($TravelLog);
				$Photo->setPhotoID($recordset['photoID']);
				$Photo->setCaption($recordset['caption']);
				$Photo->setPhotoTypeID($recordset['phototypeID']);
				$Photo->setPrimaryPhoto($recordset['primaryphoto']);
				$Photo->setDateuploaded($recordset['dateuploaded']);
				$Photo->setFileName(stripslashes($recordset['filename']));
				$Photo->setThumbFileName(stripslashes($recordset['thumbfilename']));

				$arrTravelLogPhotos[] = $Photo;
			}

			return $arrTravelLogPhotos;			
 		}
 		
 		/**
 		 * method that gets the videos of a certain travel log
 		 * added by neri
 		 * 10-08-08
 		 */
 		 
 		function getTravelLogVideos() {
 			$cache 	=	ganetCacheProvider::instance()->getCache();
			$key	=	'JournalEntryVideos_'.$this->travellogID;
			
			if ($cache != null && $videos =	$cache->get($key)){
				if ($videos[0] == 'EMPTY'){
					$videos	= array();				// if we found the contents 'EMPTY'we reassign the array to a blank array
				}	
				return $videos;
			}
			
			$videos = array();
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
 			
 			$query = "SELECT videoID FROM tblTravelLogtoVideo WHERE travelLogID = $this->travellogID ORDER BY videoID ASC";
 			$this->mRs->Execute($query);
 			
 			if (0 < $this->mRs->Recordcount()) {
 				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
 			 		try {
 			 			$video = new TravelLogToVideo($recordset['videoID']);
 			 		}
 			 		
 			 		catch (exception $e) {						  
						throw new exception ($e->getMessage());
					}
					
					$videos[] = $video;	 		
 			 	}	
 			}
 			
 			return $videos;
 		}
 		
 		/**
 		 * method that gets the primary video
 		 * added by neri
 		 * 10-08-08
 		 */
 		 
 		function getPrimaryVideo() {
 			$this->mPrimaryVideo = null;
 			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
 			
 			$query = "SELECT tblVideo.videoID FROM tblVideo INNER JOIN tblTravelLogtoVideo ON tblVideo.videoID = tblTravelLogtoVideo.videoID WHERE " .
 					 "tblVideo.isPrimaryVideo = 1 AND tblTravelLogtoVideo.travelLogID = $this->travellogID";
 			$this->mRs->Execute($query);	
 			
 			if (0 < $this->mRs->Recordcount()) {
 				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
 			 		try {
 			 			$this->mPrimaryVideo = new TravelLogToVideo($recordset['videoID']);
 			 		}
 			 		
 			 		catch (exception $e) {						  
						throw new exception ($e->getMessage());
					}	 		
 			 	}	
 			}
 			
 			return $this->mPrimaryVideo;
 		}
 		
 		function getFriendlyURL(){
			require_once("Class.FriendlyUrl.php");
			$this->getOwner();
			
			$friendlyURL = "";

			$friendlyURL = "";

			if($this->owner instanceof Traveler) {
				$friendlyURL = $this->owner->getUserName().'/journals/'.$this->travelID;
			}
			else if($this->owner instanceof AdminGroup){
				$friendlyURL = (0 < $this->owner->getParentID()) ? 'groups/'.$this->owner->getParent()->getName().'/subgroup/'.$this->owner->getName() : 'groups/'.$this->owner->getName();
				$friendlyURL = FriendlyUrl::encode($friendlyURL.'/journals/'.$this->travelID);
			}
			
			// return $friendlyURL.'/'.urlencode(FriendlyUrl::encode($this->title));
			return $friendlyURL.'/'.$this->urlAlias;
		}
		
		//added by Jul
		public static function getTravelLogContext($photoID){
			$mRs	= new Recordset(ConnectionProvider::instance()->getConnection());
			
			$sql = "SELECT tblTravelLog.travellogID AS travellogID
					FROM tblTravelLogtoPhoto, tblTravelLog
					WHERE tblTravelLogtoPhoto.photoID = $photoID AND
						tblTravelLogtoPhoto.travellogID = tblTravelLog.travellogID 
						AND tblTravelLog.deleted = 0 
					GROUP BY travellogID";
			$mRs->Execute($sql);
			
			$travellog = FALSE;
			
			if( $mRs->Recordcount() ){
				while( $row = mysql_fetch_assoc($mRs->Resultset()) ){
					$travellog = new TravelLog($row["travellogID"]);
				}
			}
			return $travellog;
		}
		
		function getPhotoIDs(){
			$mConn	= ConnectionProvider::instance()->getConnection();
			$mRs	= new Recordset($mConn);
			
			$sql = "SELECT photoID
					FROM tblTravelLogtoPhoto
					WHERE tblTravelLogtoPhoto.travellogID = $this->travellogID
					GROUP BY photoID";
			$mRs->Execute($sql);
			
			$ids = array();
			
			if( $mRs->Recordcount() ){
				while( $row = mysql_fetch_assoc($mRs->Resultset()) ){
					$ids[] = $row["photoID"];
				}
			}
			return $ids;
		}
		
		function getOriginalUploadedIDs(){
			return array();
		}
		
		function setQualifiedCustomHeaderPhoto($photoID=0,$qualified=FALSE){
			
			$mConn	= ConnectionProvider::instance()->getConnection();
			$mRs	= new Recordset($mConn);
			
			$qualified = $qualified ? 1 : 0;
			$travellogID = $this->getTravelLogID();
			$sql = "UPDATE tblTravelLogtoPhoto
					SET qualifiedCustomHeader = $qualified
					WHERE travellogID = $travellogID
						AND photoID = '$photoID'
						AND qualifiedCustomHeader != $qualified";
			$mRs->Execute($sql);
		
		}
		
		// invalidate entry in cache
		private function invalidateCacheEntry(){
			$this->cache	=	ganetCacheProvider::instance()->getCache();
			if ($this->cache != null) {
				$this->cache->delete(get_class($this) . '_' . $this->travellogID);
				
				// invalidate relation keys to parent which are cached
				if ($relKeys	=	$this->cache->get('Trip_' . get_class($this)  . '_hashes_' . $this->tripID)){
					foreach($relKeys as $relKey){
						$this->cache->delete($relKey);
					}	
				}
				
				// invalidate cached relations with parent Travel class
				if ($relKeys	=	$this->cache->get('Travel_Trip_hashes_' . $this->travelID)){
					foreach($relKeys as $relKey){
						$this->cache->delete($relKey);
					}	
				}
				// and TravelCB class, if any
				if ($relKeys	=	$this->cache->get('TravelCB_Trip_hashes_' . $this->travelID)){
					foreach($relKeys as $relKey){
						$this->cache->delete($relKey);
					}	
				}
				
				
				$this->cache->delete('Travellog_PrimaryPhoto_' . $this->travellogID);
	 			$this->cache->delete('Travellog_Photos_'  . $this->travellogID);
				try{
					$this->cache->delete('Photo_' .get_class($this). '_'. $this->getPrimaryPhoto()->getPhotoID());
					
					//added by Jul to delete cached photo collection for this entry
					$owner = $this->getOwner();
					if( $owner instanceof Traveler ){
						$this->cache->delete('Photo_Collection_Traveler_Owner_'.$owner->getTravelerID());
						$this->cache->delete('Photo_Collection_Traveler_Public_'.$owner->getTravelerID());
						$this->cache->delete('Photo_Collection_CBTraveler_Public_'.$owner->getTravelerID());
					}else if( $owner instanceof Group ){
						$this->cache->delete('Photo_Collection_Group_Admin_'.$owner->getGroupID());
						$this->cache->delete('Photo_Collection_Group_Public_'.$owner->getGroupID());
					}
				}catch(exception $e){
				}
				$this->cache->delete('JournalEntryVideos'.$this->travellogID);		// added by neri: 06-10-09
				$this->getTravel()->invalidateJournalsPageCaching();
			}
		}
		
		function getPhotosUploadedByDay($rowslimit=NULL,$date=NULL,$countOnly=FALSE){
			if ( !is_null($rowslimit) ){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }else{
                $limitstr  = ''; 
            }
			if( is_null($date) ){
				$date = date("Y-m-d H:i:s");
			}
			$travellogID = $this->travellogID;
            $sql = "SELECT SQL_CALC_FOUND_ROWS tblPhoto.* from tblTravelLogtoPhoto, tblPhoto 
            		WHERE phototypeID = 2
						AND CAST(dateuploaded AS DATE) = CAST('$date' AS DATE) 
						AND travellogID = $travellogID
            			AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID
            		ORDER BY dateuploaded DESC " . $limitstr;
			$this->rs = new Recordset(ConnectionProvider::instance()->getConnection());
            $this->rs->Execute($sql);
			
			$rs2 = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql2 = "SELECT FOUND_ROWS() AS recordCount";
			$rs2->Execute($sql2);
			$data = mysql_fetch_array($rs2->Resultset());
			
			$count = $data["recordCount"];
			if( $countOnly ){
				return $count;
			}
			$photos = array();
            if (0 < $count){
                while ($recordset = mysql_fetch_array($this->rs->Resultset())) {
                    try {
						$photo = new Photo($this,0,$recordset);
						if( $photo instanceof Photo ){
							$photos[] = $photo;
						}
                    }catch (Exception $e){
                    }
                }
            }
			return array("photos"=>$photos,"recordCount"=>$count);
		}
		
		function getVideosUploadedByDay($rowslimit=NULL,$date=NULL,$countOnly=FALSE){
			if ( !is_null($rowslimit) ){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }else{
                $limitstr  = ''; 
            }
			if( is_null($date) ){
				$date = date("Y-m-d H:i:s");
			}
			$travellogID = $this->travellogID;
            $sql = "SELECT SQL_CALC_FOUND_ROWS tblVideo.* from tblVideo, tblTravelLogtoVideo 
            		WHERE CAST(dateAdded AS DATE) = CAST('$date' AS DATE)
						AND tblVideo.videoID = tblTravelLogtoVideo.videoID
						AND tblTravelLogtoVideo.travellogID = $travellogID
            		ORDER BY dateAdded DESC " . $limitstr;
			$this->rs = new Recordset(ConnectionProvider::instance()->getConnection());
            $this->rs->Execute($sql);
			
			$rs2 = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql2 = "SELECT FOUND_ROWS() AS recordCount";
			$rs2->Execute($sql2);
			$data = mysql_fetch_array($rs2->Resultset());
			
			$count = $data["recordCount"];
			if( $countOnly ){
				return $count;
			}
			$videos = array();
            if (0 < $count){
				try{
	 				if (0 < $this->rs->Recordcount()){
		 				while ($recordset = mysql_fetch_array($this->rs->Resultset())){
			 				try {
		 						$video = new TravelLogToVideo($recordset['videoID']);
								if( $video instanceof TravelLogToVideo ){
									$videos[] = $video;
								}
							}catch(exception $e){						  
							   continue;
							}
		 				}
	 				}
	 			}catch(exception $ex){}
			}
			return array("videos"=>$videos, "recordCount"=>$count);
		}
		
		function updateDateLastEdited($date=NULL){
			
			if( $date != NULL ){
 			 	$sql = "UPDATE tblTravelLog SET lastedited = '$date' WHERE travellogID = '$this->travellogID'" ;
 				$this->rs->Execute($sql);	
			}
			// flush cache
			require_once('Cache/ganetMemcache.php');
			$c = new ganetMemcache();
			$c->flush();
			
		}
		
		function TravelLogContainsPrimaryPhoto($photoID){
			$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT tblTravelLogtoPhoto.photoID 
					FROM tblTravelLogtoPhoto 
						WHERE tblTravelLogtoPhoto.photoID = $photoID
						AND tblTravelLogtoPhoto.travellogID = ".$this->travellogID;
			$rs->Execute($sql);
			return ($rs->Recordcount() > 0) ? true : false;
		}
		
		function hasActionPrivilege($traveler_id){
			$owner = $this->getOwner();

			if ($owner instanceof Traveler) {
				return ($owner->getTravelerID() == $traveler_id);
			}
			else {
				return ($owner->getAdministratorID() == $traveler_id OR $this->owner->isStaff($traveler_id));
			}			
		}
		
		static function titleExistsInJournal($title,$travelID,$travellogID){
			//$title = ereg_replace("[&\/\'\"]", "", strip_tags($title));
			$title = preg_replace("/[&\/\'\"]/", "", strip_tags($title));
			$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT travellogID
					FROM tblTravelLog
						WHERE travellogID <> " .$travellogID. "
						AND travelID = " .$travelID. "
						AND title = '".$title."'
						AND deleted = 0";
			$rs->Execute($sql);
			return ($rs->Recordcount() > 0) ? true : false;
		}
		
		function formatUrlAlias(){
			$sql = "SELECT urlAlias, travellogID FROM tblTravelLog WHERE 1 AND deleted = 0 AND travelID = " .$this->getTravelID();
			$rs	= ConnectionProvider::instance()->getRecordset();
			$rs->Execute($sql);
			
			$aliases = array();
			while($row = mysql_fetch_assoc($rs->Resultset())){
				$aliases[$row['travellogID']] = $row['urlAlias'];
			}
			
			require_once("travellog/helper/Class.UrlAliasHelper.php");
			$alias = UrlAliasHelper::getInstance()->format($this->getTitle());
			$originalAlias = $alias;
			
			$cnt = 1;
			$currentAlias = isset($aliases[$this->getTravellogID()]) ? $aliases[$this->getTravellogID()] : "";
			while(in_array($alias, $aliases) && ($currentAlias != $alias)){
				$alias = $originalAlias.'-'.$cnt;
				$cnt++;
			}

			if($currentAlias != $alias){
				$aliases[$this->getTravellogID()] = $alias;
			}

			return ($currentAlias != $alias) ? $alias : $currentAlias;
		}
 	}
?>
