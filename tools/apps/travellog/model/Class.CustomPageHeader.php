<?php
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");
	require_once("travellog/model/Class.CustomPageHeaderPhoto.php");
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.AdminGroup.php");
	require_once("amazon/Class.GANETAmazonS3WebService.php");
	
	class CustomPageHeader{
		
		private $mGroupID = 0;
		private $mWelcomeTitle = "";
		private $mWelcomeText = "";
		private $mPhotoOption = 0;
		private $mDirectSignup = 0;
		private $mLastStep = 0;
		private $mIsActive = 0;
		
		private $mWelcomeTitleTemp = "";
		private $mWelcomeTextTemp = "";
		private $mPhotoOptionTemp = 0;
		private $mDirectSignupTemp = 0;
		private $mIsCustomizing = 0;
		
		private $mConn = NULL;
		private $mRs = NULL;
		private $mDb = NULL;
		
		private $mExists = FALSE;
		
		private $mGroup = NULL;
		
		public function __construct($groupID=NULL,$data=NULL){
			try {
	 			$this->mConn = new Connection();
				$this->mRs = new Recordset($this->mConn);
				$this->mDb = new dbHandler();
				
				if( 0 < $groupID ){
					$sql = "SELECT *
							FROM tblCustomPageHeader
							WHERE groupID = ".$this->mDb->makeSqlSafeString($groupID);
					
					$this->mRs->Execute($sql);
					if ( 0 < $this->mRs->Recordcount() ){
	 					$this->mGroupID = $this->mRs->Result(0,"groupID");
						$this->mWelcomeTitle = stripslashes($this->mRs->Result(0,"welcomeTitle"));
						$this->mWelcomeText = stripslashes($this->mRs->Result(0,"welcomeText"));
						$this->mPhotoOption = $this->mRs->Result(0,"photoOption");
						$this->mDirectSignup = $this->mRs->Result(0,"directSignup");
						$this->mLastStep = $this->mRs->Result(0,"lastStep");
						$this->mIsActive = $this->mRs->Result(0,"isActive");
						$this->mWelcomeTitleTemp = stripslashes($this->mRs->Result(0,"welcomeTitleTemp"));
						$this->mWelcomeTextTemp = stripslashes($this->mRs->Result(0,"welcomeTextTemp"));
						$this->mPhotoOptionTemp = $this->mRs->Result(0,"photoOptionTemp");
						$this->mDirectSignupTemp = $this->mRs->Result(0,"directSignupTemp");
						$this->mIsCustomizing = $this->mRs->Result(0,"isCustomizing");
						$this->mExists = TRUE;
	 				}
				}else if( is_array($data) ){
					$this->mGroupID = $data["groupID"];
					$this->mWelcomeTitle = $data["welcomeTitle"];
					$this->mWelcomeText = $data["welcomeText"];
					$this->mPhotoOption = $data["photoOption"];
					$this->mDirectSignup = $data["directSignup"];
					$this->mLastStep = $data["lastStep"];
					$this->mIsActive = $data["isActive"];
					$this->mWelcomeTitleTemp = $data["welcomeTitleTemp"];
					$this->mWelcomeTextTemp = $data["welcomeTextTemp"];
					$this->mPhotoOptionTemp = $data["photoOptionTemp"];
					$this->mDirectSignupTemp = $data["directSignupTemp"];
					$this->mIsCustomizing = $data["isCustomizing"];
					$this->mExists = TRUE;
				}else if( !is_null($groupID) && !is_null($data) ){
					throw new exception("Proper usage of CustomPageHeader constructor: new CustomPageHeader(groupID,data); where groupID>0 or data is array with keys{'groupID','welcomeTitle','welcomeText','photoOption','directSignup','lastStep','isActive'}");
				}
			}
			catch (Exception $e) {				   
			   throw $e;
			}
		}
		
		//setters
		public function setGroupID($groupID=0){
			$this->mGroupID = $groupID;
		}
		public function setWelcomeTitle($title=""){
			$this->mWelcomeTitle = $title;
		}
		public function setWelcomeText($text=""){
			$this->mWelcomeText = $text;
		}
		public function setPhotoOption($photoOption=0){
			$this->mPhotoOption = $photoOption;
		}
		public function setDirectSignup($signup=0){
			$this->mDirectSignup = $signup;
		}
		public function setLastStep($step=0){
			$this->mLastStep = $step;
		}
		public function setIsActive($isActive=0){
			$this->mIsActive = $isActive;
		}
		public function setWelcomeTitleTemp($title=""){
			$this->mWelcomeTitleTemp = $title;
		}
		public function setWelcomeTextTemp($text=""){
			$this->mWelcomeTextTemp = $text;
		}
		public function setPhotoOptionTemp($photoOption=0){
			$this->mPhotoOptionTemp = $photoOption;
		}
		public function setDirectSignupTemp($signup=0){
			$this->mDirectSignupTemp = $signup;
		}
		public function setIsCustomizing($customizing=0){
			$this->mIsCustomizing = $customizing;
		}
		public function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		public function exists(){
			return $this->mExists;
		}
		public function isActive(){
			return ( 1 == $this->mIsActive )? TRUE : FALSE;
		}
		public function isCustomizing(){
			return ( 1 == $this->mIsCustomizing )? TRUE : FALSE;
		}
		
		//getters
		public function getGroupID(){
			return $this->mGroupID;
		}
		public function getWelcomeTitle(){
			return $this->mWelcomeTitle;
		}
		public function getWelcomeText(){
			return $this->mWelcomeText;
		}
		public function getPhotoOption(){
			return $this->mPhotoOption;
		}
		public function getDirectSignup(){
			return $this->mDirectSignup;
		}
		public function getLastStep(){
			return $this->mLastStep;
		}
		public function getWelcomeTitleTemp(){
			return $this->mWelcomeTitleTemp;
		}
		public function getWelcomeTextTemp(){
			return $this->mWelcomeTextTemp;
		}
		public function getPhotoOptionTemp(){
			return $this->mPhotoOptionTemp;
		}
		public function getDirectSignupTemp(){
			return $this->mDirectSignupTemp;
		}
		public function getIsCustomizing(){
			return $this->mIsCustomizing;
		}
		public function getIsActive(){
			return $this->mIsActive;
		}
		public function getGroup(){
			if( NULL == $this->mGroup ){
				$this->mGroup = new AdminGroup($this->mGroupID);
			}
			return $this->mGroup;
		}
		
		public function getCustomPageHeaderPhotos($params=array()){
			$temp = array(	"customizing"	=>	NULL,
							"active"		=>	NULL,
							"deleted"		=>	NULL,
							"initial"		=>	NULL,
							"countOnly"		=>	FALSE,
							"photoOption"	=>	NULL,
							"isFiltered"	=>	NULL,
							"ignorePhotoOption"	=>	FALSE,
							"isSaved"		=>	NULL	);
			$params = array_merge($temp,$params);
			
			$groupID = $this->mGroupID;
			
			$isCustomizing = 0;
			if( $params["initial"] ){
				$photoOption = $this->mPhotoOptionTemp;
			}else{
				$photoOption = $this->mPhotoOption;
				if( $params["customizing"] ){
					$isCustomizing = 1;
					$photoOption = $this->mPhotoOptionTemp;
				}
			}
			$photoOption_str = "";
			if( !is_null($params["initial"]) ){
				if( $params["initial"]){
					$photoOption = $this->mPhotoOptionTemp;
				}
			}
			
			if( !is_null($params["photoOption"]) ){
				$photoOption = $params["photoOption"];
			}
			$photoOption_str = " AND photoOption = $photoOption ";
			
			if( $params["ignorePhotoOption"] ){
				$photoOption_str = "";
			}
			
			$customizing_str = "";
			if( !is_null($params["customizing"]) ){
				$isCustomizing = 0;
				if( $params["customizing"] ){
					$isCustomizing = 1;
				}
				$customizing_str = " AND isCustomizing = $isCustomizing ";
			}
			
			$deleted_str = "";
			if( !is_null($params["deleted"]) ){
				$isDeleted = 0;
				if( $params["deleted"] ){
					$isDeleted = 1;
				}
				$deleted_str = " AND isDeleted = $isDeleted ";
			}
			
			$active_str = "";
			if( !is_null($params["active"]) ){
				$isActive = 0;
				if( $params["active"] ){
					$isActive = 1;
				}
				$active_str = " AND isActive = $isActive ";
			}
			
			$filtered_str = "";
			if( !is_null($params["isFiltered"]) ){
				$isFiltered = 0;
				if( $params["isFiltered"] ){
					$isFiltered = 1;
				}
				$filtered_str = " AND isFiltered = $isFiltered ";
			}
			
			$saved_str = "";
			if( !is_null($params["isSaved"]) ){
				$isSaved = 0;
				if( $params["isSaved"] ){
					$isSaved = 1;
				}
				$saved_str = " AND isSaved = $isSaved ";
			}
			
			$sql = "SELECT *
					FROM tblCustomPageHeaderPhoto
					WHERE groupID = $groupID 
						$photoOption_str
						$active_str
						$customizing_str
						$deleted_str
						$filtered_str
						$saved_str
					ORDER BY photoID ASC";
			
			$this->mRs->Execute($sql);
			if( $params["countOnly"] ){
				return $this->mRs->Recordcount();
			}

			$customPageHeaderPhotos = array();
			while ($data = mysql_fetch_assoc($this->mRs->Resultset())) {
				try{
					$customPageHeaderPhoto = new CustomPageHeaderPhoto(NULL,$data);
					$customPageHeaderPhoto->setGroup($this->getGroup());
				}catch(Exception $e){}
				$customPageHeaderPhotos[] = $customPageHeaderPhoto;
			}
			return $customPageHeaderPhotos;
		}
		
		public function getRandomPhoto($customizing=FALSE,$filtered=NULL){
			$groupID = $this->mGroupID;
			if( is_null($filtered) ){
				if( $customizing ){
					$photoOption = $this->mPhotoOptionTemp;
					$sql = "SELECT *
							FROM tblCustomPageHeaderPhoto
							WHERE groupID = $groupID
								AND isDeleted = 0
								AND photoOption = $photoOption
								AND (isCustomizing = 1 OR isSaved = 1)";
				}else{
					$sql = "SELECT *
							FROM tblCustomPageHeaderPhoto
							WHERE groupID = $groupID
								AND isActive = 1";
				}
			}elseif( $filtered ){
				$sql = "SELECT *
						FROM tblCustomPageHeaderPhoto
						WHERE groupID = $groupID
							AND isFiltered = 1";
			}
			$this->mRs->Execute($sql);
			
			$recordcount = $this->mRs->Recordcount();
			if( $recordcount ){
				$newIndex = NULL;
				$maxIndex = $recordcount-1;
				if( !isset($_SESSION["random_custom_photo_list"]) ){
					$list = range(0,$maxIndex);
					shuffle($list);
					$_SESSION["random_custom_photo_list"] = $list;
					$_SESSION["last_index"] = $newIndex = 0;
				}else{
					$list = $_SESSION["random_custom_photo_list"];
					$lastIndex = $_SESSION["last_index"];
					if( $lastIndex >= $maxIndex ){
						$list = $_SESSION["random_custom_photo_list"];
						shuffle($list);
						$_SESSION["random_custom_photo_list"] = $list;
						$_SESSION["last_index"] = $newIndex = 0;
					}else{
						$_SESSION["last_index"] = $newIndex = $lastIndex+1;
					}
				}
				$idx = 0;
				while( $data = mysql_fetch_assoc($this->mRs->Resultset()) ){
					if( $idx == $_SESSION["random_custom_photo_list"][$newIndex] ){
						$customPageHeaderPhoto = new CustomPageHeaderPhoto(NULL,$data);
						if( is_null($this->mGroup) ){
							$this->mGroup = new AdminGroup($this->mGroupID);
						}
						$customPageHeaderPhoto->setGroup($this->mGroup);
						return $customPageHeaderPhoto;
					}
					$idx++;
				}
			}else{
				if( isset($_SESSION["random_custom_photo_list"]) ){
					unset($_SESSION["random_custom_photo_list"]);
					unset($_SESSION["last_index"]);
				}
				return FALSE;
			}
		}
		
		public function Save($finalStep=FALSE){
			$groupID = $this->mDb->makeSqlSafeString($this->mGroupID);
			$welcomeTitle = $this->mDb->makeSqlSafeString(strip_tags($this->mWelcomeTitle,"<a>"));
			$welcomeText = $this->mDb->makeSqlSafeString(strip_tags($this->mWelcomeText,"<a>"));
			$photoOption = $this->mDb->makeSqlSafeString($this->mPhotoOption);
			$directSignup = $this->mDb->makeSqlSafeString($this->mDirectSignup);
			$lastStep = $this->mDb->makeSqlSafeString($this->mLastStep);
			$isActive = $this->mDb->makeSqlSafeString($this->mIsActive);
			$welcomeTitleTemp = $this->mDb->makeSqlSafeString(strip_tags($this->mWelcomeTitleTemp,"<a>"));
			$welcomeTextTemp = $this->mDb->makeSqlSafeString(strip_tags($this->mWelcomeTextTemp,"<a>"));
			$photoOptionTemp = $this->mDb->makeSqlSafeString($this->mPhotoOptionTemp);
			$directSignupTemp = $this->mDb->makeSqlSafeString($this->mDirectSignupTemp);
			
			if( $this->mExists ){
				if( $finalStep ){
					$sql = "UPDATE tblCustomPageHeader
							SET welcomeTitle = $welcomeTitleTemp,
								welcomeText = $welcomeTextTemp,
								photoOption = $photoOptionTemp,
								directSignup = $directSignupTemp,
								welcomeTitleTemp = '',
								welcomeTextTemp = '',
								photoOptionTemp = 0,
								directSignupTemp = 0,
								lastStep = 0,
								isActive = 1,
								isCustomizing = 0
							WHERE groupID = $groupID";
					$this->DeleteUnusedPhotos();
				}else{
					$sql = "UPDATE tblCustomPageHeader
							SET welcomeTitleTemp = $welcomeTitleTemp,
								welcomeTextTemp = $welcomeTextTemp,
								photoOptionTemp = $photoOptionTemp,
								directSignupTemp = $directSignupTemp,
								isCustomizing = 1,
								lastStep = $lastStep
							WHERE groupID = $groupID";
				}
				if( isset($_SESSION["random_custom_photo_list"]) ){
					unset($_SESSION["random_custom_photo_list"]);
					unset($_SESSION["last_index"]);
				}
			}else{
				$sql = "INSERT INTO tblCustomPageHeader(groupID,welcomeTitleTemp,welcomeTextTemp,photoOptionTemp,directSignupTemp,isCustomizing,lastStep,isActive)
						VALUES($groupID,$welcomeTitleTemp,$welcomeTextTemp,$photoOptionTemp,$directSignupTemp,1,1,0)";
			}//var_dump($sql);exit;
			$this->mRs->Execute($sql);
		}
		
		public function Delete(){
			$groupID = $this->mDb->makeSqlSafeString($this->mGroupID);
			$sql = "DELETE FROM tblCustomPageHeader WHERE groupID = $groupID";
			$this->mRs->Execute($sql);
		}
		
		public function Activate(){
			$groupID = $this->mDb->makeSqlSafeString($this->mGroupID);
			$lastStep = $this->mDb->makeSqlSafeString($this->mLastStep);
			$sql = "UPDATE tblCustomPageHeader SET isActive = '1', lastStep = $lastStep WHERE groupID = $groupID";
			$this->mRs->Execute($sql);
		}
		
		public function UpdateLastStep(){
			$groupID = $this->mDb->makeSqlSafeString($this->mGroupID);
			$lastStep = $this->mDb->makeSqlSafeString($this->mLastStep);
			$sql = "UPDATE tblCustomPageHeader SET lastStep = $lastStep WHERE groupID = $groupID";
			$this->mRs->Execute($sql);
		}
		
		public function DeleteUnusedPhotos(){
			$params = array(	"customizing"	=>	TRUE,
								"deleted"		=>	TRUE	);
			$customPhotos = $this->getCustomPageHeaderPhotos($params);
			foreach($customPhotos as $customPhoto){
				$customPhoto->Delete();
			}
		}
		
		public function DeleteOldFetchedPhotos(){
			$params = array("customizing"	=>	TRUE,
							"deleted"		=>	FALSE	);
			$customPhotos = $this->getCustomPageHeaderPhotos($params);
			foreach($customPhotos as $customPhoto){
				$customPhoto->Delete();
			}
		}
		
		public function UnfilterPhotos(){
			$groupID = $this->mDb->makeSqlSafeString($this->mGroupID);
			$sql = "UPDATE tblCustomPageHeaderPhoto SET isFiltered = 0 WHERE groupID = $groupID";
			$this->mRs->Execute($sql);
		}
		
		public function InactivatePhotos(){
			$groupID = $this->mDb->makeSqlSafeString($this->mGroupID);
			$sql = "UPDATE tblCustomPageHeaderPhoto SET isActive = 0 WHERE groupID = $groupID";
			$this->mRs->Execute($sql);
		}
		
		public function UndoChanges(){
			$params = array("ignorePhotoOption"	=>	TRUE,
			 				"isSaved"			=>	FALSE);
			$inactivePhotos = $this->getCustomPageHeaderPhotos($params);
			foreach($inactivePhotos as $photo){//delete unsaved photos
				$photo->Delete();
			}
			
			/*$params = array(	"customizing"	=>	TRUE,
								"photoOption"	=>	$this->getPhotoOptionTemp(),
								"active"		=>	TRUE );
			$tempDeletedPhotos = $this->getCustomPageHeaderPhotos($params);
			foreach($tempDeletedPhotos as $photo){
				$photo->UndoChanges();
			}*/
			
			$lastStep = 0;
			$welcomeTitleTemp = $this->mWelcomeTitleTemp = "''";
			$welcomeTextTemp = $this->mWelcomeTextTemp = "''";
			$photoOptionTemp = $this->mPhotoOptionTemp = 0;
			$directSignupTemp = $this->mDirectSignupTemp = 0;
			$this->mIsCustomizing = 0;
			$sql = "UPDATE tblCustomPageHeader
					SET welcomeTitleTemp = $welcomeTitleTemp,
						welcomeTextTemp = $welcomeTextTemp,
						photoOptionTemp = $photoOptionTemp,
						directSignupTemp = $directSignupTemp,
						isCustomizing = 0,
						lastStep = $lastStep
					WHERE groupID = ".$this->mGroupID;
			$this->mRs->Execute($sql);
			
			$sql = "UPDATE tblCustomPageHeaderPhoto
					SET isFiltered = 0,
						isCustomizing = 0,
						captionTemp = '',
						isDeleted = 0
					WHERE groupID = ".$this->mGroupID;
			$this->mRs->Execute($sql);
		}
		
		public function SaveFetchedPhoto($photo=NULL){
			
			$filename = $photo->getFilename();
			$path = $photo->getPathOfPhoto();
			
			$orig_file = $path."orig-".$filename;
			$custom_fullsize = $path."customheaderfullsize".$filename;
			$custom_standard = $path."customheaderstandard".$filename;
			$thumbnail = $path."thumbnail".$filename;
			
			$imageInfo = @getimagesize($orig_file);
			
			if( $imageInfo ){
				$newFilename = "";
				switch(substr($imageInfo['mime'],6,4)){
					case "jpeg"	:	$newFilename = uniqid().time().".jpg";
									break;
					case "gif"	:	$newFilename = uniqid().time().".gif";
									break;
					case "png"	:	$newFilename = uniqid().time().".png";
									break;
					case "pjpeg":	$newFilename = uniqid().time().".jpg";
									break;
					default		:	$newFilename = uniqid().time().".jpg";
									break;
				}
				
				$newPhoto = new Photo($this->mGroup);
				$newPhoto->setFilename($newFilename);
				$newPhoto->setCaption($photo->getCaption());
				$newPhoto->setPhotoTypeID(12);
				$newPhoto->Create();
				
				$newPath = $newPhoto->getPathOfPhoto();
				
				$new_orig_file = $newPath."orig-".$newFilename;
				$new_custom_fullsize = $newPath."customheaderfullsize".$newFilename;
				$new_custom_standard = $newPath."customheaderstandard".$newFilename;
				$new_thumbnail = $newPath."thumbnail".$newFilename;
				
				copy($orig_file,$new_orig_file);
				copy($custom_fullsize,$new_custom_fullsize);
				copy($custom_standard,$new_custom_standard);
				copy($thumbnail,$new_thumbnail);
				
				$rawFilesToAdd = array();
				$rawFilesToAdd[] = "orig-".$newFilename;
				$rawFilesToAdd[] = "customheaderfullsize".$newFilename;
				$rawFilesToAdd[] = "customheaderstandard".$newFilename;
				$rawFilesToAdd[] = "thumbnail".$newFilename;
				
				$ganetAWS = GANETAmazonS3WebService::getInstance();
				$ganetAWS->pushRawFilestoS3($rawFilesToAdd,$newPath);
				
				$customPhoto = new CustomPageHeaderPhoto();
				$customPhoto->setPhotoInstance($newPhoto);
				$customPhoto->setGroupID($this->getGroupID());
				$customPhoto->setPhotoOption($this->getPhotoOptionTemp());
				$customPhoto->setPhotoID($newPhoto->getPhotoID());
				$customPhoto->setTravelerID($photo->getContext()->getTravelerID());
				$customPhoto->setIsActive(1);
				$customPhoto->setIsCustomizing(0);
				$customPhoto->setIsSaved(1);
				$customPhoto->Save();
			}
			
		}
	}