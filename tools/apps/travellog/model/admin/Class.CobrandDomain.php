<?php
	require_once('Class.DataModel.php');
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('travellog/model/Class.Client.php');
	require_once("travellog/model/Class.AdminGroup.php");
	require_once('gaLogs/Class.ClientDomain.php');
	
	class CobrandDomain extends DataModel
	{
		private $mGroup		= null;
		private $mLoggerDomain = null;
		
		private function __construct($keyID=null,$data=null){
			parent::initialize('Travel_Logs','tblConfig');
			if(!is_null($keyID)){
				if(!$this->load($keyID)){
					throw new exception("Invalid ConfigID: $keyID passed to object of type CobrandDomain.");
				}
			}
			if(is_array($data)){
				parent::setFields($data);
			}
		}
		
		public function getGroup(){
			if(is_null($this->mGroup) || $this->mGroup->getGroupID() != $this->getGroupID()){
				try{
					$this->mGroup = new AdminGroup($this->getGroupID());
				}
				catch(exception $e){
					$this->mGroup = null;
				}
			}
			return $this->mGroup;
		}
		
		public function getCobrandDomainID(){
			return $this->getConfigID();
		}
		
		public function getLoggerDomain(){
			if(is_null($this->mLoggerDomain)){
				$this->mLoggerDomain = ClientDomain::getClientDomainByDomainName($this->getServerName());
			}
			return $this->mLoggerDomain;
		}
		
		public function save(){
			parent::save();
			$this->mGroup 		 = null;
			$this->mLoggerDomain = null;
		}
		
		static public function getInstanceByDomainName($domainName=''){
			$db = new dbhandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblConfig WHERE serverName = {$db->makeSqlSafeString($domainName)}"));
			return ($rs->retrieveRecordCount() > 0 ? new CobrandDomain(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getInstanceByDomainID($domainID=0){
			$db = new dbhandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblConfig WHERE configID = {$db->makeSqlSafeString($domainID)}"));
			return ($rs->retrieveRecordCount() > 0 ? new CobrandDomain(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getInstances(){
			$db = new dbhandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblConfig ORDER BY serverName"));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new CobrandDomain(null,$row);
			}
			return $ar;
		}
		
		static public function createBlankInstance(){
			return new CobrandDomain;
		}
		
	}
?>