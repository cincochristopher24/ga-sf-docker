<?php

require_once('travellog/model/messageCenter/Class.gaGroupMapper.php');
require_once('travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php');
require_once('travellog/model/messageCenter/personal_message/Class.gaPersonalMessage.php');
require_once('Cache/ganetCacheProvider.php');

class GanetSpamMessage extends gaPersonalMessage {
	
	protected $_mCollGroupIDs = array();
	protected $_mCollGroups = array();
	
	// TODO: to be removed
	public function getDisplayedRecipients() {
		return $this->getRecipients();
	}
	
	public function send() {
	 
	}
	
	public function initialize(array $result) {
		if (count($result)){	
			$this->_mID = isset($result['id']) ? $result['id'] : null;
			$this->_mDateCreated = isset($result['dateCreated']) ? $result['dateCreated'] : null; 
			$this->_mSenderID = isset($result['senderID']) ? $result['senderID'] : null;
			$this->_mSubject = isset($result['subject']) ? $result['subject'] : null;
			$this->_mBody = isset($result['body']) ? $result['body'] : null;
			$this->_mIsMessageIncludedInNotif = isset($result['isMessageIncludedInNotif']) ? $result['isMessageIncludedInNotif'] : null;
			$this->_mRecipientIDs = isset($result['recipientID']) ? array($result['recipientID']) : array();
		}
	}
	
	function getSender(){
		if( $this->_mSenderID && !$this->_mSender ) {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			$this->_mSender = gaTravelerMapper::getTraveler($this->_mSenderID);
		}
		return $this->_mSender;
	}
	
	function getRecipients() {
		if ( !empty($this->_mRecipientIDs) && empty($this->_mCollRecipients) ) {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			foreach($this->_mRecipientIDs as $id) {
				$this->_mCollRecipients[] = gaTravelerMapper::getTraveler($id);
			}
		}
		return $this->_mCollRecipients;
	}
	
	function addRecipientID($v) {
		if (!in_array($v, $this->_mRecipientIDs))
			$this->_mRecipientIDs[] = $v;
		return $this->_mRecipientIDs;
	}
	
	function isForGroupStaff() {
		if (empty($this->_mCollGroupIDs))
			$this->_mCollGroupIDs = gaPersonalMessageDM::getMessageGroupRecipients($this->getID(), false);
		return !empty($this->_mCollGroupIDs) ? true : false;
	}
	
	function getGroupIDs() {
		if ($this->isForGroupStaff())
			return $this->_mCollGroupIDs;
		return array();
	}
	
	function getGroups() {
		if (empty($this->_mCollGroups))
			$this->_mCollGroups = gaPersonalMessageDM::getMessageGroupRecipients($this->getID(), true);
		else if (!empty($this->_mCollGroupIDs)) {
			foreach ($this->_mCollGroupIDs as $id) {
				$this->_mCollGroups[] = gaGroupMapper::getGroup($id);
			}
		}
		return $this->_mCollGroups;
	}
	
	function approve() {
		GanetSpamMessagePeer::doUpdate($this->getID(), $this->getRecipientIDs());
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache))
			$cache->delete('ganet_pmcopy_' . $this->getSenderID());
		return $this;
	}
	
	function delete() {
		foreach($this->_mRecipientIDs as $id)
			gaPersonalMessageDM::deleteCopy($id, $this->getID());
		return $this;
	}
	
	function deleteOne($recipientID = 0) {
		gaPersonalMessageDM::deleteCopy($recipientID, $this->getID());
		return $this;
	}
	
}