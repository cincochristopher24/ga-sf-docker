<?php
/***
 * Created on March 5, 2009
 *
 */
 
 require_once('Class.DataModel.php');
 require_once('errorLogger/Class.GAErrorLog.php');
 require_once('travellog/model/Class.RowsLimit.php');
 
 class ErrorLogs extends DataModel {
 	
 	private $mErrorLog = NULL;
 	private $mErrorTypes = array(
		1		=> 'E_ERROR',	
		2 		=> 'E_WARNING',
		4		=> 'E_PARSE',
		8		=> 'E_NOTICE',
		16		=> 'E_CORE_ERROR',
		32  	=> 'E_CORE_WARNING',
		64		=> 'E_COMPILE_ERROR',
		128		=> 'E_COMPILE_WARNING',
		256		=> 'E_USER_ERROR',
		512		=> 'E_USER_WARNING',
		1024	=> 'E_USER_NOTICE',
		6143	=> 'E_ALL',
		2048	=> 'E_STRICT',
		4096	=> 'E_RECOVERABLE_ERROR',
		8192	=> 'E_DEPRECATED',
		16384	=> 'E_USER_DEPRECATED'
	);
 	
 	public function __construct($errorID=0){
		parent::initialize('Logs','tblErrors');
		parent::load($errorID);
	}
	
	function getErrorType($int){
		if(1 > $int)
			return $int;
		
		if( array_key_exists($int,$this->mErrorTypes) )
			return $int."- ".str_replace("_"," ",substr($this->mErrorTypes[$int],2));
		else {
			//todo:binValue
			$arr = str_split(strrev(decbin($int)));
			$str = "";
			foreach($arr as $key=>$val) {
				if("1" == $val)
					$str .= pow(2,$key)."- ".substr($this->mErrorTypes[pow(2,$key)],2).", ";
			}
			return str_replace("_"," ",$str);
		}
	}
		
 	public function getAllLogsCount($searchBy = null, $keyword = null) {
		$db = new dbHandler('db=Logs');
		$sql = "SELECT count(ID) FROM tblErrors";
		$sql .= (!empty($searchBy) && !empty($keyword))
				? (" WHERE $searchBy ".(("ID"==$searchBy) ? "= ".$db->makeSqlSafeString($keyword) : "LIKE ".$db->makeSqlSafeString("%".$keyword."%")."") ): "";
		$rs = new iRecordset($db->execute($sql));
		$record = $rs->current();
		return $record['count(ID)'];
 	}
 	
 	public function getLogs($rowsLimit, $searchBy = null, $keyword = null) {
 		$db = new dbHandler('db=Logs');
		$sql = "SELECT ID FROM tblErrors";
		$sql .= (!empty($searchBy) && !empty($keyword))
				? (" WHERE $searchBy ".(("ID"==$searchBy) ? "= ".$db->makeSqlSafeString($keyword) : "LIKE ".$db->makeSqlSafeString("%".$keyword."%")."") ): "";
		$sql .= " ORDER BY dateAdded desc";
		$sql .= ($rowsLimit instanceof RowsLimit) ? " ".$rowsLimit->createSqlLimit() : "";
		$rs = new iRecordset($db->execute($sql));
		$logs = array();
		while(!$rs->EOF()){
			$record = $rs->current();
			$logs[] = new ErrorLogs($record['ID']); 
			$rs->next();
		}
		return $logs;
 	}
 	
 }