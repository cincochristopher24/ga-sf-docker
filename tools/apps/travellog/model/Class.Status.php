<?php
	
	/*
	 * Class.Status.php
	 * Created on Jul 12, 2007
	 * created by marc
	 */
	 
	require_once("Class.Connection.php");
	 require_once("Class.Recordset.php");
	 
	 class Status{
	 	
	 	private $statusID;
	 	private $status;
	 	private $conn;
	 	private $rs;
	 	
	 	function __construct( $statusID = 0 ){
	 		
	 		$this->conn = new Connection();
	 		$this->rs = new Recordset($this->conn);
	 		
	 		if( $statusID ){
	 			$sqlquery = "SELECT * FROM SocialApplication.tbStatus WHERE statusID = $statusID";
	 			$this->rs->execute($sqlquery);
	 			
	 			if ($this->rs->Recordcount() == 0){				
					throw new exception ("Status does not exist!");
				}
				
				$this-> statusID = $this->rs->Result(0, "statusID");
				$this-> status = $this->rs->Result(0, "status");
	 			
	 		}
	 	}
	 	
	 	function setStatus($status){
	 		$this->status = trim($status);
	 	}
	 	
	 	function setStatusID($statusID){
	 		$this->statusID = $statusID;
	 	}
	 	
	 	function getStatus(){
	 		return $this->status;
	 	}
	 	
	 	function getStatusID(){
	 		return $this->statusID;
	 	}
	 	
	 	function create(){
	 	
	 		$sqlQuery = "INSERT INTO SocialApplication.tbStatus " .
	 				"SET status = '" . $this->status . "'";
	 		$this->rs->execute($sqlQuery);
	 		$this->statusID = mysql_insert_id();
	 					
	 	}
	 	
	 	function update(){
	 		$sqlQuery = "UPDATE SocialApplication.tbStatus " .
	 				"SET status = '" . $this->status . "' " .
	 				"WHERE statusID = " . $this->statusID;
	 		$this->rs->execute($sqlQuery);
	 	}
	 	
	 	function delete(){
	 		$sqlQuery = "DELETE FROM SocialApplication.tbStatus " .
	 				"WHERE statusID = " . $this->statusID;
	 		$this->rs->execute($sqlQuery);
	 	}
	 	
	 	static function getAllStatus(){
	 		
	 		$conn = new Connection();
	 		$rs = new Recordset($conn);
	 		$arrStatus = array();
	 		
	 		$sqlQuery = "SELECT statusID FROM SocialApplication.tbStatus";
	 		$rs->execute($sqlQuery);
	 		while( $tmpRow = mysql_fetch_array($rs->Resultset()))
	 			$arrStatus[] = new Status($tmpRow["statusID"]);
	 		
	 		return $arrStatus;
	 		
	 	}
	 	
	 } 
?>
