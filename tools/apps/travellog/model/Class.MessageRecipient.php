<?php
	class MessageRecipient
	{
		private $mMessage = null;
		private $mRecipient = null;
		
		public function __construct($msg=null,$recipient=null){
			$this->mMessage = $msg;
			$this->mRecipient = $recipient;
		}
		
		public function isOwner(){
			$source = $this->mMessage->getSource();
			return $source->getSendableID() == $this->mRecipient->getSendableID();
		}
		
		public function getName(){
			return $this->mRecipient->getName();
		}
		
		public function isGroup(){
			return $this->mRecipient instanceof Group;	
		}
		
		public function isTraveler(){
			return $this->mRecipient instanceof Traveler;
		}
		
		public function getSendableID(){
			return $this->mRecipient->getSendableID();
		}
				
	}
?>