<?php
/**
 * Created on Mar 22, 2007
 * @author daf
 * 
 * 
 */
 	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
	
 	class OnlineFormOptions {
 	
		/**
		 * Define variables for Class Member Attributes
		 */
		     
			 private $mOptionID     	= 0;
			 private $mTitle       		= '';
			 private $mValue			= '';
			 private $mMiscType			= 0;
			 
			 private $mSurveyAnswers  		= array();
		     
		/**
		 * Constructor Function for this class
		 */
		           
		    function OnlineFormOptions($optionID = 0){ 
		    	
		    	$this->mOptionID = $optionID;
		    	
		    	
		    	try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mOptionID){
                    
                    $sql = "SELECT * FROM tblOnlineFormOptions WHERE onlineFormOptionID = $this->mOptionID ";
                    $this->mRs->Execute($sql);
                	
                    if ($this->mRs->Recordcount()){
                        
                        // Sets values to its class member attributes.
	                    $this->mTitle  		= stripslashes($this->mRs->Result(0,"title"));       
	                    $this->mValue 		= stripslashes($this->mRs->Result(0,"value"));   
	                    $this->mMiscType 	= $this->mRs->Result(0,"misctype");
	                        
                    }
                }
		    	
		    }
		    
		/**
         * Setter Functions
         */    
            function seTitle($_title){
                $this->mTitle = $_title;
            }
            
            function setValue($_value){
                $this->mValue = $_value;
            }
            
            function setMiscType($_misctype){
                $this->mMiscType = $_misctype;
            }
            
		
		/**
         * Getter Functions
         */    
            function getOptionID(){
                return $this->mOptionID ;
            }
            
            function getTitle(){
                return $this->mTitle ;
            }
            
            function getValue(){
                return $this->mValue ;
            }
            
            function getMiscType(){
                return $this->mMiscType ;
            }
          
    		
    		function Save(){
    			    			
    			$_title      = addslashes($this->mTitle);
                
    			if (0 == $this->mOnlineFormID) {
    				
    				$sql = "INSERT into tblOnlineFormOptions (title, value, misctype) " .
                        "VALUES ('$_title', $this->mValue, $this->mMiscType)";
	                $this->mRs->Execute($sql);       
	                $this->mOptionID = $this->mRs->GetCurrentID();
    				
    			}
    			
    			else {
    				
    				$sql = "UPDATE tblOnlineFormOptions SET " .
    						"title = '$_title', value = $this->mValue, misctype = $this->mMiscType " .
    						"WHERE onlineFormOptionID = $this->mOptionID" ;
	                $this->mRs->Execute($sql);       
    				
    			}
    		}
    		
    		
    		function Delete(){
               
                $sql = "DELETE FROM tblOnlineFormOptions WHERE onlineFormOptionID = $this->mOptionID ";     
                $this->mRs->Execute($sql);
            } 
        
          /**
           * Purpose: Returns an array of answers for this question.
           */
            function getSurveyAnswers(){
                
                
				 if (0 == count($this->mSurveyAnswers)){
				
					$sql = "SELECT onlinesurveyanswerID " .
							"FROM tblOnlineSurveyAnswers " .
							"WHERE optionID = $this->mOptionID " ;
					$this->mRs->Execute($sql);
			 		
			 		while($recordset = mysql_fetch_array($this->mRs->Resultset())){
	                    try {
	 						$this->mSurveyAnswers[] = new OnlineSurveyAnswers($recordset["onlinesurveyanswerID"]); 									
	 					} 					
						catch (Exception $e) {
						   throw $e;					   
						}
	                }
	 				
				}
				 
 				return $this->mSurveyAnswers; 		
           }
           
    }
?>
