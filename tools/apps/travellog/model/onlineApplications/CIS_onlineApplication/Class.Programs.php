<?php


require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');


class Programs {
	
	protected	$id,
				$programName,
				$term = array(),
				$hasStartDate;
				
	protected	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppPrograms SET 
					Name  = '.ToolMan::makeSqlSafeString( $this->getProgramName() );
					
		$this->conn->execQuery($sql);
		$this->setID($this->conn->getLastInsertedID());
	}
	
	
	// setters
	public function setID($v){
		$this->id = $v;
	}
		
	public function setName($v){
		$this->programName = $v;
	}
	
	public function setTerm($v){
	//	$this->term = $v;
		array_push($this->term, $v);
	}
	
	public function setHasStartDate($v){
		$this->hasStartDate = $v;
	}
	
	// getters
	public function getID(){
		return $this->id;
	}
		
	public function getName(){
		return $this->programName;
	}
	
	public function getTerm(){
		return $this->term;
	}
	
	public function hasStartDate(){
		return $this->hasStartDate;
	}
	
	public function getHasStartDate(){
		return $this->hasStartDate();
	}
}
?>