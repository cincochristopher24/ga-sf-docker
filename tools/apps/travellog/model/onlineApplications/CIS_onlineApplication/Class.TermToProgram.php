<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class TermToProgram {
	
	protected	$id,
				$termID,
				$programID;
	
	protected	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppTermToProgram SET
					 TermID    ='.ToolMan::makeSqlSafeString( $this->getTermID()   ).', '.
					'ProgramID ='.ToolMan::makeSqlSafeString( $this->getProgramID());
					
		$this->conn->execQuery($sql);
	}
	
	// setters
	public function setID($v){
		$this->id = $v;
	}
	
	public function setTermID($v){
		$this->termID = $v;
	}
	
	public function setProgramID($v){
		$this->programID = $v;
	}
	
	// getters
	public function getID(){
		return $this->id;
	}
	
	public function getTermID(){
		return $this->termID;
	}
	
	public function getProgramID(){
		return $this->programID;
	}
}
?>