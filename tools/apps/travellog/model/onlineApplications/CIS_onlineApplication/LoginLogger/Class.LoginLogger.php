<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
class LoginLogger {
	
	protected	$id,
				$email,
				$password,
				$browser,
				$status,
				$date;
				
	protected	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	/* 
	 * SETTERS
	 */ 
	
	public function setID($v)			{ $this->id = $v;		}	
	public function setEmail($v)		{ $this->email = $v;	}
	public function setPassword($v)		{ $this->password = $v;	}
/*	public function setBrowser()		{
		//$this->browser = $v;
		$this->browser = get_browser(null, true);
	}*/
	public function setStatus($v)		{ $this->status = $v; 	}
	
	/* 
	 * GETTERS
	 */ 
	
	public function getID()			{ return $this->id;		 }
	public function getEmail()		{ return $this->email;	 }
	public function getPassword()	{ return $this->password;}
	public function getStatus()		{ return $this->status; }
	
	public function getBrowser()	{ 
		return $_SERVER['HTTP_USER_AGENT'];
	//	$this->browser = get_browser(null, true);
	//	return $this->transformToString($this->browser);
	}
	
	private function transformToString($var){
		ob_start();
		print_r($var);
		$str = ob_get_contents();
		ob_end_clean();
		return $str;
	}
	/* 
	 * SAVE
	 */ 
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppLoginLogs SET
					`email`    ='.ToolMan::makeSqlSafeString( $this->getEmail()  ).', '.
					'`password`    ='.ToolMan::makeSqlSafeString( $this->getPassword()  ).', '.
					'`browser`    ='.ToolMan::makeSqlSafeString( $this->getBrowser()  ).', '.
					'`status`    ='.ToolMan::makeSqlSafeString( $this->getStatus()  );
	//	echo $sql;
		$this->conn->execQuery($sql);
		
	}
}