<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class UserCoupon {
	
	protected 	$id,
				$personalInfoID,
				$coupon;
	
	protected	$conn;

	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppUserCoupon SET 
					personalInfoID = '.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ) .', '.
					'coupon  = '.ToolMan::makeSqlSafeString( $this->getCoupon() ).' '.
					'ON DUPLICATE KEY UPDATE coupon  = '.ToolMan::makeSqlSafeString( $this->getCoupon() );
		$this->conn->execQuery($sql);
		$this->setID($this->conn->getLastInsertedID());
	}
	
	
	// setters
	public function setID($v){
		$this->id = $v;
	}
		
	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}
	
	public function setCoupon($v){
		$this->coupon = $v;
	}
	
	// getters
	public function getID(){
		return $this->id;
	}
		
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	
	public function getCoupon(){
		return $this->coupon;
	}
	
	public function setName($v){
		$this->setCoupon($v);
	}
	public function getName(){
		return $this->getCoupon();
	}
	
	public function hasCode(){
		$sql = 'SELECT * from tblCISOnlineAppUserCoupon WHERE personalInfoID = '.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() );
		$rs = new iRecordset($this->conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			return true;
		}
		return false;
	}
	
	public function delete(){
		$sql = 'DELETE FROM tblCISOnlineAppUserCoupon WHERE personalInfoID = '.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() );
		$this->conn->execQuery($sql);
		
	}
	
}