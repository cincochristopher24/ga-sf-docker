<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
require_once('Class.iRecordset.php');

class Steps{
	
	protected 	$id,
				$personalInfoID,
				$step;
				
	protected	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppStepsDone SET
					PersonalInfoID = '.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()    ).', '.
					'Step = '.ToolMan::makeSqlSafeString( $this->getDoneStep()    ).' '.
				'ON duplicate KEY UPDATE '.
					'Step = '.ToolMan::makeSqlSafeString( $this->getDoneStep()    );
		$this->conn->execQuery($sql);
	}
	
	// setters
	public function setID($v){
		$this->id = $id;
	}
	
	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}
	
	public function setDoneStep($v){
		$this->step = $v;
	}
	
	// getters
	public function getID(){
		return $this->id;
	}
	
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	
	public function getDoneStep(){
		return $this->step;
	}
	
	public function getStepByPersonalID($id){
		$sql = 'SELECT * FROM tblCISOnlineAppStepsDone WHERE PersonalInfoID = '.$id.' limit 1';
		$rs = new iRecordset($this->conn->execQuery($sql));
		
		if($rs->retrieveRecordCount() > 0){
			return $rs->getStep();
		}
		
		return false;
	}
	
	public function delete(){
		$sql = 'DELETE FROM tblCISOnlineAppStepsDone WHERE PersonalInfoID = '.$this->getPersonalInfoID();
		$this->conn->execQuery($sql);
	}
	
	
}
?>