<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class OnlineAppToStudentDB {
	
	private $id = null;
	private $online_app_user_id = null;
	private $studentDB_id  = null;
	
	public function setID($v){
		$this->id = $v;
	}
	
	public function setOnlineAppUserID($v){
		$this->online_app_user_id = $v;
	}
	
	public function setStudentID($v){
		$this->studentDB_id = $v;
	}
	
	public function getID(){
		return $this->id;
	}
	
	public function getOnlineAppUserID(){
		return $this->online_app_user_id;
	}
	
	public function getStudentID(){
		return $this->studentDB_id;
	}
	
	public function save(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = sprintf("INSERT INTO tblCISOnlineAppToStudentDB SET userID = %d, studentID = %d, dateAdded = now()", $this->online_app_user_id, $this->studentDB_id);
		
		$conn->execQuery($sql);
	}
}