<?php

class DurationOptions {
	
	private $id;
	private $name;
	private $programID;
	
	public function setID($v){
		$this->id = $v;
	}
	
	public function setName($v){
		$this->name = $v;
	}
	
	public function setProgramID($v){
		$this->programID = $v;
	}
	
	public function getID(){
		return $this->id;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function getProgramID(){
		return $this->programID;
	}
}