<?php

class Options {
	
	protected	$id,
				$programID,
				$name,
				$termID;
				
	public function __construct(){}
	
	
	// setters
	public function setID($v){
		$this->id = $v;
	}
	
	public function setProgramID($v){
		$this->programID = $v;
	}
	
	public function setName($v){
		$this->name = $v;
	}
	
	public function setTermID($v){
		$this->termID = $v;
	}
	
	// getters
	public function getID(){
		return $this->id;
	}
	
	public function getProgramID(){
		return $this->programID;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function getTermID(){
		return $this->termID;
	}
	
	// save
	public function save(){}
}
?>