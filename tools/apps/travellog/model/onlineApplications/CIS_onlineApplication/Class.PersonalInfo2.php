<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
require_once('Class.iRecordset.php');


class PersonalInfo2 {


// personal info details
	protected 	$id,
				$onlineAppUserID,
				$firstName,
				$lastName,
				$preferredName,
				$gender,
				$birthday,
				$citizenship,
				$countryBirthPlace,
				$secNumber,
				$email,
				$altEmail,
				$billEmail,
				$shirtSize;

// address information
	protected	$personalInfoID,
				$type = 1,  // set the default value
				$street1,
				$city1,
				$state1,
				$zip1,
				$phoneNumber1,
				$cellphone,
				$street2,
				$city2,
				$state2,
				$zip2,
				$phoneNumber2,
				$permanentAddress,
				$validUntil;

	// for school information
	protected 	$schoolID,
				$schoolType,
				$otherSchool,
				$major,
				$minor,
				$yearInSchool,
				$genAverage,
				$currentCourses,
				$stateAbbr = null,
				$school_contact,
				$school_email;
				



protected 	$conn;

public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}


	// #### SAVE TO DATABASE ####
	# insert into tblCISOnlineAppPersonalInfo (FirstName) values ('ant')
	public function save(){
			$sql = 'INSERT INTO tblCISOnlineAppPersonalInfo SET
					FirstName ='.ToolMan::makeSqlSafeString     ( $this->getFirstName()           ).', '.
					'LastName  ='.ToolMan::makeSqlSafeString     ( $this->getLastName()            ).', '.
					'Gender    ='.ToolMan::makeSqlSafeString     ( $this->getGender()              ).', '.
					'BirthDay  ='.ToolMan::makeSqlSafeString     ( $this->getBirthday()            ).', '.
					'Email    ='.ToolMan::makeSqlSafeString      ( $this->getEmail()               ).', '.
					'AltEmail ='.ToolMan::makeSqlSafeString      ( $this->getAltEmail()            ).', '.
					'BillingEmail = '.ToolMan::makeSqlSafeString ( $this->getBillingEmail()        ).', '.
					'ShirtSize = '.ToolMan::makeSqlSafeString    ( $this->getShirtSize()           )  ;


		$this->conn->execQuery($sql);
		//$this->setID($this->conn->getLastInsertedID());

		//self::$lastInsertedID = $this->conn->getLastInsertedID();
	//	var_dump($sql);

				$sql = 'INSERT INTO tblCISOnlineAppAddressInfo SET
					PersonalInfoID    ='.$this->conn->getLastInsertedID().', '.
					'AddressType      ='.ToolMan::makeSqlSafeString( $this->getAddressType()   ).', '.
					'Street1  ='.ToolMan::makeSqlSafeString( $this->getStreet1()          ).', '.
					'City1    ='.ToolMan::makeSqlSafeString( $this->getCity1()            ).', '.
					'State1   ='.ToolMan::makeSqlSafeString( $this->getState1()           ).', '.
					'Zip1     ='.ToolMan::makeSqlSafeString( $this->getZip1()             ).', '.
					'PhoneNumber1 ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber1() ).', '.
					'Cellphone    ='.ToolMan::makeSqlSafeString( $this->getCellphone());
				$this->id = $this->conn->getLastInsertedID();

		$this->conn->execQuery($sql);


		$sql = 'INSERT INTO tblCISOnlineAppSchoolInfo SET
					PersonalInfoID ='.$this->id.', '.
					'SchoolID      ='.ToolMan::makeSqlSafeString( $this->getSchoolID()     ).', '.
					'Type          ='.ToolMan::makeSqlSafeString( $this->getSchoolType()     ).', '.
					'OtherSchoolName ='.ToolMan::makeSqlSafeString( $this->getOtherSchoolName()     ).', '.
					'Major         ='.ToolMan::makeSqlSafeString( $this->getMajor()          ).', '.
					'Minor         ='.ToolMan::makeSqlSafeString( $this->getMinor()          ).', '.
					'YearInSchool  ='.ToolMan::makeSqlSafeString( $this->getYearInSchool()   ).', '.
					'GenAverage    ='.ToolMan::makeSqlSafeString( $this->getGenAverage()     ).', '.
					'CurrentCourses='.ToolMan::makeSqlSafeString( $this->getCurrentCourses() ).', '.
					'contact	   ='.ToolMan::makeSqlSafeString( $this->getSchoolContact()     ).', '.
					'email    	   ='.ToolMan::makeSqlSafeString( $this->getSchoolEmail()     );

		$this->conn->execQuery($sql);

		//$this->lastInsertedPersonalInfoID = array('PersonalInfoID' => $personalInfoObj->getID());
	}





	public function setOnlineAppUserID($v){
		$this->onlineAppUserID = $v;
	}

	public function setFirstName($v){
		$this->firstName = $v;
	}

	public function setLastName($v){
		$this->lastName = $v;
	}

	public function setPreferredName($v){
		$this->preferredName = $v;
	}

	public function setGender($v){
		$this->gender = $v;
	}

	public function setBirthday($v){
		$this->birthday = $v;
	}

	public function setCitizenship($v){
		$this->citizenship = $v;
	}

	public function setCountryBirthPlace($v){
		$this->countryBirthPlace = $v;
	}

	public function setSecNumber($v){
		$this->secNumber = $v;
	}

	public function setEmail($v){
		$this->email = $v;
	}

	public function setAltEmail($v){
		$this->altEmail = $v;
	}

	public function setBillingEmail($v){
		$this->billEmail = $v;
	}
	
	public function setShirtSize($v){
		$this->shirtSize = $v;
	}



// ##### GETTERS #####


	public function getOnlineAppUserID(){
		return $this->onlineAppUserID;
	}

	public function getFirstName(){
		return $this->firstName;
	}

	public function getLastName(){
		return $this->lastName;
	}

	public function getPreferredName(){
		return $this->preferredName;
	}

	public function getGender(){
		return $this->gender;
	}

	public function getBirthday(){
		return $this->birthday;
	}

	public function getCitizenship(){
		return $this->citizenship;
	}

	public function getCountryBirthPlace(){
		return $this->countryBirthPlace;
	}

	public function getSecNumber(){
		return $this->secNumber;
	}

	public function getEmail(){
		return $this->email;
	}

	public function getAltEmail(){
		return $this->altEmail;
	}


	public function getName(){
		return $this->getFirstName().' '.$this->getLastName();
	}

	public function getBillingEmail(){
		return $this->billEmail;
	}
	
	public function getShirtSize(){
		return $this->shirtSize;
	}



	// setter and getters of addresses

// #### SETTERS ####
	public function setId($v){
		$this->id = $v;
	}

	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}

	public function setAddressType($v){
		$this->type = $v;
	}

	public function setStreet1($v){
		$this->street1 = $v;
	//	var_dump('street1 set to ='.$v);
	}

	public function setCity1($v){
		$this->city1 = $v;
	}

	public function setState1($v){
		$this->state1 = $v;
	}

	public function setZip1($v){
		$this->zip1 = $v;
	}

	public function setPhoneNumber1($v){
		$this->phoneNumber1 = $v;
	}

	public function setCellphone($v){
		$this->cellphone = $v;
	}

	public function setStreet2($v){
		$this->street2 = $v;
	}

	public function setCity2($v){
		$this->city2 = $v;
	}

	public function setState2($v){
		$this->state2 = $v;
	}

	public function setZip2($v){
		$this->zip2 = $v;
	}

	public function setPhoneNumber2($v){
		$this->phoneNumber2 = $v;
	}

	public function setPermanentAddress($v){
		$this->permanentAddress = $v;
	}

	public function setValidUntil($v){
		$this->validUntil = $v;
	}

	// #### GETTERS ####
	public function getId(){
		return $this->id;
	}

	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}

	public function getAddressType(){
		return $this->type;
	}

	public function getStreet1(){
	//	var_dump('this is what u get = '.$this->street1);
		return $this->street1;
	}

	public function getCity1(){
		return $this->city1;
	}

	public function getState1(){
		return $this->state1;
	}

	public function getZip1(){
		return $this->zip1;
	}

	public function getPhoneNumber1(){
		return $this->phoneNumber1;
	}

	public function getCellphone(){
		return $this->cellphone;
	}

	public function getStreet2(){
		return $this->street2;
	}

	public function getCity2(){
		return $this->city2;
	}

	public function getState2(){
		return $this->state2;
	}

	public function getZip2(){
		return $this->zip2;
	}

	public function getPhoneNumber2(){
		return $this->phoneNumber2;
	}

	public function getPermanentAddress(){
		return $this->permanentAddress;
	}

	public function getValidUntil(){
		return $this->validUntil;
	}

	// school information

// #### SETTERS #####



	public function setSchoolID($v){
		$this->schoolID = $v;
	}

	public function setSchoolType($v){
		$this->schoolType = $v;
	}

	public function setOtherSchoolName($v){
		$this->otherSchool = $v;
	}

	public function setMajor($v){
		$this->major = $v;
	}

	public function setMinor($v){
		$this->minor = $v;
	}

	public function setYearInSchool($v){
		$this->yearInSchool = $v;
	}

	public function setGenAverage($v){
		$this->genAverage = $v;
	}

	public function setCurrentCourses($v){
		$this->currentCourses = $v;
	}
	
	public function setSchoolContact($v){
		$this->school_contact = $v;
	}
	
	public function setSchoolEmail($v){
		$this->school_email = $v;
	}

	// #### GETTERS ####



	public function getSchoolID(){
		return $this->schoolID;
	}

	public function getSchoolType(){
		return $this->schoolType;
	}

	public function getOtherSchoolName(){
		return $this->otherSchool;
	}

	public function getMajor(){
		return $this->major;
	}

	public function getMinor(){
		return $this->minor;
	}

	public function getYearInSchool(){
		return $this->yearInSchool;
	}

	public function getGenAverage(){
		return $this->genAverage;
	}

	public function getCurrentCourses(){
		return $this->currentCourses;
	}
	
	
	public function getSchoolContact(){
		return $this->school_contact;
	}
	
	public function getSchoolEmail(){
		return $this->school_email;
	}

	public function getSchoolName(){
		if(0 < $this->schoolID){

			if($this->type == '0'){
				require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.USUniversitiesPeer.php');
				$school = USUniversitiesPeer::retrieveByPk($this->schoolID);
				// try fix bug
				if(!$school){
					$school = USUniversitiesPeer::retrieveFromOldDB($this->schoolID);
				}
				$this->stateAbbr = USUniversitiesPeer::getSchoolStateAbbrByStateID($school->getStateID());
				return $school->getName();

			}
			else if($this->type == '1') {
				require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.CISPartnerSchoolsPeer.php');
				$school = CISPartnerSchoolsPeer::retrieveByPk($this->schoolID);
				$this->stateAbbr = CISPartnerSchoolsPeer::getSchoolStateAbbrByStateID($school->getStateID());
				return $school->getName();
			}

		}
		else {

			return $this->otherSchool;
		}
	}




}





?>