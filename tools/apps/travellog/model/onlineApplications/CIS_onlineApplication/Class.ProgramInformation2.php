<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
require_once('Class.iRecordset.php');

class ProgramInformation2 {

protected	$id,
				$personalInfoID,
				$term,
				$year,
				$firstProgram,
				$option1,
				$secondProgram,
				$option2,
				$priority,
				$interest,
				$volunteering,
				$housing_options_id,
				$duration_id,
				$startMonth,
				$startDay;

protected 		$specialNeeds,
				$learnProgramFrom,
				$others,
				$campusRep,
				$friendGrant,
				$goAgainGrant,
				$financialAid;


protected 		$signature,
				$applicationDate,
				$lastEdited;

protected	$message;

protected $conn;

public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}

public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppProgramInfo SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'Term          ='.ToolMan::makeSqlSafeString( $this->getTerm()           ).', '.
					'Year          ='.ToolMan::makeSqlSafeString( $this->getYear()           ).', '.
					'FirstProgramID  ='.ToolMan::makeSqlSafeString( $this->getFirstProgramID() ).', '.
					'FirstOptionID   ='.ToolMan::makeSqlSafeString( $this->getFirstOptionID()  ).', '.
					'SecondProgramID ='.ToolMan::makeSqlSafeString( $this->getSecondProgramID()).', '.
					'SecondOptionID  ='.ToolMan::makeSqlSafeString( $this->getSecondOptionID() ).', '.
					'PriorityOption  ='.ToolMan::makeSqlSafeString( $this->getPriorityOption() ).', '.
					'Interest      ='.ToolMan::makeSqlSafeString( $this->getInterest()         ).', '.
					'Volunteering  ='.ToolMan::makeSqlSafeString( $this->getVolunteering()     ).', '.
					'housing_options_id ='.ToolMan::makeSqlSafeString( $this->getHousingOptionID() ).', '.
					'duration_id ='.ToolMan::makeSqlSafeString( $this->getDurationOptionID()     	).', '.
					'startMonth ='.ToolMan::makeSqlSafeString( $this->getStartMonth()     		).', '.
					'startDay ='.ToolMan::makeSqlSafeString( $this->getStartDay()    			);
		$this->conn->execQuery($sql);


		$this->setID($this->conn->getLastInsertedID());


			$sql = 'INSERT INTO tblCISOnlineAppAdditionalInfo SET
					PersonalInfoID    ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()   ).', '.
					'SpecialNeeds     ='.ToolMan::makeSqlSafeString( $this->getSpecialNeeds()     ).', '.
					'LearnProgramFrom ='.ToolMan::makeSqlSafeString( $this->getLearnProgramFrom() ).', '.
					'Others           ='.ToolMan::makeSqlSafeString( $this->getOthers()           ).', '.
					'CampusRep        ='.ToolMan::makeSqlSafeString( $this->getCampusRep()        ).', '.
					'FriendGrant      ='.ToolMan::makeSqlSafeString( $this->getFriendGrant()      ).', '.
					'goAgainGrant     ='.ToolMan::makeSqlSafeString( $this->getGoAgainGrant()     ).', '.
					'FinancialAid     ='.ToolMan::makeSqlSafeString( $this->getFinancialAid()     );
		$this->conn->execQuery($sql);


		$sql = 'INSERT INTO tblCISOnlineAppAgreement SET
					PersonalInfoID    ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()  ).', '.
					'Signature        ='.ToolMan::makeSqlSafeString( $this->getSignature()       ).', '.
					'ApplicationDate  ='.ToolMan::makeSqlSafeString( $this->getApplicationDate() ).', '.
					'LastEdited       ='.ToolMan::makeSqlSafeString( $this->getLastEdited()      );
		$this->conn->execQuery($sql);
	//	var_dump($sql);

		$sql = 'INSERT INTO tblCISOnlineAppComments SET
					userID = '.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()        ).', '.
					'comment  = '.ToolMan::makeSqlSafeString( $this->getMessage()   ).' '.
				'ON DUPLICATE KEY UPDATE '.
					'comment  = '.ToolMan::makeSqlSafeString( $this->getMessage()   );

		$this->conn->execQuery($sql);

}

public function getPersonalId() {
		//$cpaSchools = array();
		$personalId = null;
		$sql = "SELECT * FROM `tblCISOnlineAppPersonalInfo` order by id DESC limit 1";
		$rs = new iRecordset($this->conn->execQuery($sql));

	/*	while(!$rs->EOF()){
			$cpaSchools[] = $rs->retrieveRow();
			$rs->moveNext();
		}
	*/
		$personalId = $rs->retrieveRow();
		return $personalId;

		//return $cpaSchools;
	}

public function getComment() {
		//$cpaSchools = array();
		$comment = null;
		$sql = "SELECT * FROM `tblCISOnlineAppComments` order by id DESC limit 1";
		$rs = new iRecordset($this->conn->execQuery($sql));
		$getComment = $rs->retrieveRow();
		return $getComment;
	}

	public function getlearnFromOthers() {
		//$cpaSchools = array();
		$learnFrom = null;
		$sql = "SELECT * FROM `tblCISOnlineAppAdditionalInfo` order by id DESC limit 1";
		$rs = new iRecordset($this->conn->execQuery($sql));
		$learnFrom = $rs->retrieveRow();
		return $learnFrom;
	}



public function setMessage($v){ $this->message = $v; }
public function getMessage(){ return $this->message; }



// #### SETTERS ####
	public function setID($v){
		$this->id = $v;
	}

	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}

	public function setTerm($v){
		$this->term = $v;
	}

	public function setYear($v){
		$this->year = $v;
	}

	public function setFirstProgramID($v){
		$this->firstProgram = $v;
	}

	public function setFirstOptionID($v){
		$this->option1 = $v;
	}

	public function setSecondProgramID($v){
		$this->secondProgram = $v;
	}

	public function setSecondOptionID($v){
		$this->option2 = $v;
	}

	public function setPriorityOption($v){
		$this->priority = $v;
	}

	public function setInterest($v){
		$this->interest = $v;
	}

	public function setVolunteering($v){
		$this->volunteering = $v;
	}

	public function setHousingOptionID($v){
		$this->housing_options_id = $v;
	}

	public function setDurationOptionID($v){
		$this->duration_id = $v;
	}

	public function setStartMonth($v){
		$this->startMonth = $v;
	}

	public function setStartDay($v){
		$this->startDay = $v;
	}

	// #### GETTERS ####
	public function getID(){
		return $this->id;
	}

	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}

	public function getTerm(){
		return $this->term;
	}

	public function getYear(){
		return $this->year;
	}

	public function getFirstProgramID(){
		return $this->firstProgram;
	}

	public function getFirstOptionID(){
		return $this->option1;
	}

	public function getSecondProgramID(){
		return $this->secondProgram;
	}

	public function getSecondOptionID(){
		return $this->option2;
	}

	public function getPriorityOption(){
		return $this->priority;
	}

	public function getInterest(){
		return $this->interest;
	}

	public function getVolunteering(){
		return $this->volunteering;
	}

	public function getHousingOptionID(){
		return $this->housing_options_id;
	}

	public function getDurationOptionID(){
		return $this->duration_id;
	}

	public function getStartMonth(){
		return $this->startMonth;
	}

	public function getStartDay(){
		return $this->startDay;
	}


	//aditional info
public function setSpecialNeeds($v){
		$this->specialNeeds = $v;
	}

	public function setLearnProgramFrom($v){
		$this->learnProgramFrom = $v;
	}

	public function setOthers($v){
		$this->others = $v;
	}

	public function setCampusRep($v){
		$this->campusRep = $v;
	}

	public function setFriendGrant($v){
		$this->friendGrant = $v;
	}
	
	public function setGoAgainGrant($v){
		$this->goAgainGrant = $v;
	}

	public function setFinancialAid($v){
		$this->financialAid = $v;
	}

public function getSpecialNeeds(){
		return $this->specialNeeds;
	}

	public function getLearnProgramFrom(){
		return $this->learnProgramFrom;
	}

	public function getOthers(){
		return $this->others;
	}

	public function getCampusRep(){
		return $this->campusRep;
	}

	public function getFriendGrant(){
		return $this->friendGrant;
	}
	
	public function getGoAgainGrant(){
		return $this->goAgainGrant;
	}

	public function getFinancialAid(){
		return $this->financialAid;
	}


# agreement

	public function setSignature($v){
		$this->signature = $v;
	}
	public function setApplicationDate($v){
		$this->applicationDate = $v;
	}

	public function setLastEdited($v){
		$this->lastEdited = $v;
	}

public function getSignature(){
		return $this->signature;
	}

	public function getApplicationDate(){
		return $this->applicationDate;
	}

	public function getLastEdited(){
		return $this->lastEdited;
	}

}


?>