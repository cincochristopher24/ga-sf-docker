<?php

class CISOnlineAppFieldsMapper {
	
	// this are the fieldnames array keys
	const 	OA_USER_ID = 'OnlineAppUserID',
			FIRSTNAME  	= 'FirstName',
			LASTNAME	= 'LastName',
			PREFERRED_NAME = 'PreferredName',
			GENDER		= 'Gender',
			BIRTHDAY	= 'Birthday',
			CITIZENSHIP	= 'Citizenship',
			BIRTHPLACE	= 'CountryBirthPlace',
			SECURITY_NUMBER	= 'SecNumber',
			EMAIL		    = 'Email',
			ALTERNATE_EMAIL	= 'AltEmail',
			BILLING_EMAIL   = 'BillingEmail',
			SHIRTSIZE		= 'ShirtSize',
			PHONE_NUMBER    = 'PhoneNumber',
			PASSPORT_NUMBER	= 'PassportNumber',
			EXPIRY_DATE	    = 'ExpiryDate',
			
			PERSONAL_INFO_ID = 'PersonalInfoID',
			
			STATEMENT1	= 'Statement1',
			STATEMENT2	= 'Statement2',
			STATEMENT3 	= 'Statement3',
			
			PAYING_PERSONS = 'PayingPersons',
			MAIN_BILLING_CONTACT = 'MainBillingContact',
			RELATIONSHIP = 'Relationship',
			ADDRESS		 = 'Address',
			
			ZIP			 = 'Zip',
			SPECIALNEEDS = 'SpecialNeeds',
			
			TERM		= 'Term',
			YEAR		= 'Year',
			FIRSTPROG 	= 'FirstProgramID',
			FIRST_OPT   = 'FirstOptionID',
			SECONDPROG	= 'SecondProgramID',
			SECOND_OPT  = 'SecondOptionID',
			PRIORITY    = 'PriorityOption',
			INTEREST	= 'Interest',
			VOLUNTEERING = 'Volunteering',
			
			SCHOOL_ID    = 'SchoolID',
			SCHOOL_TYPE  = 'Type',
			OTHER_SCHOOL = 'OtherSchoolName',
			MAJOR		 = 'Major',
			MINOR		 = 'Minor',
			YEAR_IN_SCHOOL = 'YearInSchool',
			GEN_AVERAGE	 = 'GenAverage',
			CURRENT_COURSES = 'CurrentCourses',
			SCHOOL_CONTACT  = 'contact',
			SCHOOL_EMAIL  = 'email',
			
			ADDRESS_TYPE = 'AddressType',
			STREET1		= 'Street1',
			STREET2		= 'Street2',
			CITY1		= 'City1',
			CITY2		= 'City2',
			STATE1		= 'State1',
			STATE2		= 'State2',
			ZIP1		= 'Zip1',
			ZIP2		= 'Zip2',
			PHONE_NUMBER1 = 'PhoneNumber1',
			PHONE_NUMBER2 = 'PhoneNumber2',
			CELLPHONE     = 'Cellphone',
			PERMANENT_ADDRESS = 'PermanentAddress',
			VALID_UNTIL = 'ValidUntil',
			
			LEARN_PROG_FROM = 'LearnProgramFrom',
			OTHERS			= 'Others',
			CAMPUS_REP      = 'CampusRep',
			FRIEND_GRANT    = 'FriendGrant',
			GOAGAIN_GRANT	= 'goAgainGrant',
			FINANCIAL_AID   = 'FinancialAid',
			
			
			SIGNATURE	= 'Signature',
			APPLICATION_DATE = 'ApplicationDate',
			
			PROGRAM_INFO_ID  = 'ProgramInfoID',
			
			HOUSING_OPTION_ID = 'HousingOptionID',
			DURATION_ID = 'DurationOptionID',
			START_MONTH = 'StartMonth',
			START_DAY = 'StartDay'
			;
	
	// this are the object array keys....
	const	PERSONAL_INFO = 'PersonalInfo',
			ADDRESS_INFO  = 'AddressInfo',
			PASSPORT_INFO = 'PassportDetails',
			EMERGENCY_CONTACT = 'EmergencyContact',
			SCHOOL_INFO       = 'SchoolInfo',
			ADDITIONAL_INFO   = 'AdditionalInfo',
			PROGRAM_INFO      = 'ProgramInformation',
			PERSONAL_STATEMENT = 'PersonalStatement',
			AGREEMENT          = 'Agreement',
			BILLING_INFO       = 'BillingInfo';
	
	
	public static $personalInfoFieldMap = array(self::OA_USER_ID,
												self::FIRSTNAME,
												self::LASTNAME,
												self::PREFERRED_NAME,
												self::GENDER,
												self::BIRTHDAY,
												self::CITIZENSHIP,
												self::BIRTHPLACE,
												self::SECURITY_NUMBER,
												self::EMAIL,
												self::ALTERNATE_EMAIL,
												self::BILLING_EMAIL,
												self::SHIRTSIZE);
	
	public static $addressInfoFieldMap  = array(self::PERSONAL_INFO_ID,
												self::ADDRESS_TYPE,
												self::STREET1,
												self::CITY1,
												self::STATE1,
												self::ZIP1,
												self::PHONE_NUMBER1,
												self::CELLPHONE,
												self::PHONE_NUMBER2,
												self::STREET2,
												self::CITY2,
												self::STATE2,
												self::ZIP2,
												self::PERMANENT_ADDRESS,
												self::VALID_UNTIL);
	
	public static $passportDetails = array(self::PERSONAL_INFO_ID,
											self::PASSPORT_NUMBER,
											self::EXPIRY_DATE);
	
	public static $emergencyContactFieldMap = array(self::PERSONAL_INFO_ID,
													self::FIRSTNAME,
													self::LASTNAME,
													self::RELATIONSHIP,
													self::ADDRESS,
													self::ZIP,
													self::PHONE_NUMBER,
													self::SPECIALNEEDS);
	
	public static $schoolInfoFieldMap   = array(self::PERSONAL_INFO_ID,
												self::SCHOOL_ID,
												self::SCHOOL_TYPE,
												self::OTHER_SCHOOL,
												self::MAJOR,
												self::MINOR,
												self::YEAR_IN_SCHOOL,
												self::GEN_AVERAGE,
												self::CURRENT_COURSES,
												self::SCHOOL_CONTACT,
												self::SCHOOL_EMAIL);
												
	
	public static $programInfoFieldMap  = array(self::PERSONAL_INFO_ID,
												self::TERM,
												self::YEAR,
												self::FIRSTPROG,
												self::FIRST_OPT,
												self::SECONDPROG,
												self::SECOND_OPT,
												self::PRIORITY,
												self::INTEREST,
												self::VOLUNTEERING,
												self::HOUSING_OPTION_ID,
												self::DURATION_ID,
												self::START_MONTH,
												self::START_DAY);
	
	public static $personalInfoToProgram = array(self::PERSONAL_INFO_ID,
												 self::PROGRAM_INFO_ID);
	
	public static $personalStatementFieldMap    = array(self::PERSONAL_INFO_ID,
														self::STATEMENT1,
														self::STATEMENT2,
														self::STATEMENT3);
	
	public static $billingInfoFieldMap  = array(self::PERSONAL_INFO_ID,
												self::PAYING_PERSONS,
												self::MAIN_BILLING_CONTACT,
												self::FIRSTNAME,
												self::LASTNAME,
												self::RELATIONSHIP,
												self::ADDRESS,
												self::PHONE_NUMBER,
												self::EMAIL);
	
	public static $additionalInfoFielMap = array(self::PERSONAL_INFO_ID,
												self::SPECIALNEEDS,
												self::LEARN_PROG_FROM,
												self::OTHERS,
												self::CAMPUS_REP,
												self::FRIEND_GRANT,
												self::GOAGAIN_GRANT,
												self::FINANCIAL_AID);
	
	public static $agreementFieldMap    = array(self::PERSONAL_INFO_ID,
												self::SIGNATURE,
												self::APPLICATION_DATE);
	
	
	
}



class CISOnlineAppConst{
	
	


	const	OTHER 					= 0,
			STUDY_ABROAD_ADV_OFFICE = 1,
			GOABROAD				= 2,
			FACULTY_PROFESSOR		= 3,
			STUDY_ABROAD			= 4,
			CIS_REPRESENTATIVE		= 5,
			GOOGLE					= 6,
			STUDY_ABROAD_FAIR		= 7,
			ALUMNI_REFERRAL			= 8,
			YAHOO					= 9,
			VISIT_IMPORT			= 10,
			OVERSEAS_UNIV_WEBSITE	= 11,
			MY_UNIV_WEBSITE			= 12,
			STUDENT_REFERRAL		= 13,
			IIE_PASSPORT			= 14,
			GOLDEN_KEY				= 15,
			DIVERSITY_ABROAD		= 16,
			CIS_WEBSITE				= 17,
			ADVISOR					= 18,
			POSTER					= 19;
			
	public static $addInfoOptKeys = array(
								self::ADVISOR				  => 'Advisor',
								self::ALUMNI_REFERRAL         => 'CIS Alumni Referral',
								self::CIS_REPRESENTATIVE      => 'CIS Representative',
								self::CIS_WEBSITE			  => 'CIS Website',
								self::DIVERSITY_ABROAD		  => 'Diversity Abroad',
								self::FACULTY_PROFESSOR       => 'Faculty',
								self::GOABROAD                => 'GoAbroad.com',
								self::GOLDEN_KEY			  => 'Golden Key',
								self::GOOGLE                  => 'Google',
								self::IIE_PASSPORT			  => 'IIE Passport',
								self::MY_UNIV_WEBSITE		  => 'My University\'s Website',
								self::OVERSEAS_UNIV_WEBSITE   => 'Overseas University Website',
								self::POSTER				  => 'Poster',
								self::STUDENT_REFERRAL		  => 'Student Referral',
								self::STUDY_ABROAD_FAIR       => 'Study Abroad Fair',
								self::STUDY_ABROAD_ADV_OFFICE => 'Study Abroad Office',
								self::STUDY_ABROAD            => 'StudyAbroad.com',
								self::VISIT_IMPORT			  => 'Visit Import',
								self::YAHOO					  => 'Yahoo',
								self::OTHER                   => 'Other Website'
								);
	
	public static function getAddInfoOptions(){
		return self::$addInfoOptKeys;
	}
}
?>