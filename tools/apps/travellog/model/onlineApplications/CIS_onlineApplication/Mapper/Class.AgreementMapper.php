<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.Agreement.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class AgreementMapper {
	
	
	public static function populateObjects(iRecordset $rs){
		$agreements = array();
		while(!$rs->EOF()){
			$agreement = new Agreement();
			$agreement->setID               ( $rs->getId()               );
			$agreement->setPersonalInfoID   ( $rs->getPersonalInfoID()   );
			$agreement->setSignature	    ( $rs->getSignature()        );
			$agreement->setApplicationDate  ( $rs->getApplicationDate()  );
			
			
			$agreements[] = $agreement;
			$rs->moveNext();
		}
		return $agreements;
	}
	
	public static function populateObject(iRecordset $rs){
		$agreement = self::populateObjects($rs);
		return $agreement[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAgreement WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$agreementObj = self::populateObject($rs);
			return $agreementObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAgreement WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$agreementObjs = self::populateObjects($rs);
			return $agreementObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAgreement WHERE PersonalInfoID = '.$personalInfoID;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$agreementObjs = self::populateObject($rs);
			return $agreementObjs;
		}
		
		return null;
	}
	
	public static function retrieveApplicationDateByGenID($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAgreement WHERE PersonalInfoID = '.$id.' limit 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			return $rs->getApplicationDate();
		}
		
		return null;
	}
}
?>