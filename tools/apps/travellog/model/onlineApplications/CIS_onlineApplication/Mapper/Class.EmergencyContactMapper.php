<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.EmergencyContact.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class EmergencyContactMapper {
	
	public static function populateObjects(iRecordset $rs){
		$emergencyContacts = array();
		while(!$rs->EOF()){
			$emergencyContact = new EmergencyContact();
			$emergencyContact->setID             ( $rs->getId()             );
			$emergencyContact->setPersonalInfoID ( $rs->getPersonalInfoID() );
			$emergencyContact->setFirstName      ( $rs->getFirstName()      );
			$emergencyContact->setLastName       ( $rs->getLastName()       );
			$emergencyContact->setRelationship   ( $rs->getRelationship()   );
		//	$emergencyContact->setAddress        ( $rs->getPersonalInfoID() );  // address will be fetched from addressMapper
		//	$emergencyContact->setZip            ( $rs->getZip()            );
		//	$emergencyContact->setPhoneNumber    ( $rs->getPhoneNumber()    );
		//	$emergencyContact->setSpecialNeeds   ( $rs->getSpecialNeeds()   );
			
			$emergencyContacts[] = $emergencyContact;
			$rs->moveNext();
		}
		return $emergencyContacts;
	}
	
	public static function populateObject(iRecordset $rs){
		$emergencyContact = self::populateObjects($rs);
		return $emergencyContact[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppEmergencyContact WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$emergencyContactObj = self::populateObject($rs);
			return $emergencyContactObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppEmergencyContact WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$emergencyContactObjs = self::populateObjects($rs);
			return $emergencyContactObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppEmergencyContact WHERE PersonalInfoID = '.$personalInfoID;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$emergencyContactObj = self::populateObject($rs);
			return $emergencyContactObj;
		}
		
		return null;
	}
}
?>