<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.AdditionalInfo.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class AdditionalInfoMapper {
	
	
	public static function populateObjects(iRecordset $rs){
		$additionalInfos = array();
		while(!$rs->EOF()){
			$additionalInfo = new AdditionalInfo();
			$additionalInfo->setID               ( $rs->getId()               );
			$additionalInfo->setPersonalInfoID   ( $rs->getPersonalInfoID()   );
			$additionalInfo->setSpecialNeeds	 ( $rs->getSpecialNeeds()     );
			$additionalInfo->setLearnProgramFrom ( $rs->getLearnProgramFrom() );
			$additionalInfo->setOthers           ( $rs->getOthers()           );
			$additionalInfo->setCampusRep        ( $rs->getCampusRep()        );
			$additionalInfo->setFriendGrant      ( $rs->getFriendGrant()      );
			$additionalInfo->setGoAgainGrant      ( $rs->getGoAgainGrant()      );
			$additionalInfo->setFinancialAid     ( $rs->getFinancialAid()     );
			$additionalInfos[] = $additionalInfo;
			$rs->moveNext();
		}
		return $additionalInfos;
	}
	
	public static function populateObject(iRecordset $rs){
		$additionalInfo = self::populateObjects($rs);
		return $additionalInfo[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAdditionalInfo WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$additionalInfoObj = self::populateObject($rs);
			return $additionalInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAdditionalInfo WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$additionalInfoObjs = self::populateObjects($rs);
			return $additionalInfoObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAdditionalInfo WHERE PersonalInfoID = '.$personalInfoID.' limit 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$additionalInfoObj = self::populateObject($rs);
			return $additionalInfoObj;
		}
		
		return null;
	}
}
?>