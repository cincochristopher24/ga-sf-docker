<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.DurationOptions.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class DurationOptionsPeer {
	
	public static function retrieveByProgramID($progID = 0){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "SELECT a.* FROM tblCISOnlineAppDurationOptions as a
				LEFT JOIN tblCISOnlineAppProgramToDuration as b ON a.id = b.duration_id
				WHERE b.program_id = {$progID} and b.defered = 0";
		$rs = new iRecordset($conn->execQuery($sql));
		$durations = array();
		while(!$rs->EOF()){
			$durations[] = array('id' => $rs->getID(), 'name' => $rs->getName());
			$rs->moveNext();
		}
		return $durations;
	}
	
	public static function retrieveByProgramIDAndTermID($progID = 0, $termID = 0){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "SELECT a.* FROM tblCISOnlineAppDurationOptions as a
				LEFT JOIN tblCISOnlineAppProgramToDuration as b ON a.id = b.duration_id
				WHERE b.program_id = {$progID} AND b.term_id = {$termID} and b.defered = 0";
		$rs = new iRecordset($conn->execQuery($sql));
		$durations = array();
		while(!$rs->EOF()){
			$durations[] = array('id' => $rs->getID(), 'name' => $rs->getName());
			$rs->moveNext();
		}
		return $durations;
	}
	
	public static function retrieveDurationOptionsPerProgram(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "SELECT a.*, b.program_id FROM tblCISOnlineAppDurationOptions AS a
				INNER JOIN tblCISOnlineAppProgramToDuration AS b ON a.ID = b.duration_id
                WHERE b.defered = 0";
		$rs = new iRecordset($conn->execQuery($sql));

		$Options = array();
		while(!$rs->EOF()){

		//	if(!array_key_exists($rs->getId(), $Programs)){
				$Option = new DurationOptions();
				$Option->setID        ( $rs->getId()        );
				$Option->setProgramID ( $rs->getProgram_id() );
				$Option->setName      ( $rs->getName()      );

				$Options[] = $Option;
		//	}
		//	else {
		//		$Programs[$rs->getId()]->setTerms($rs->getTermID());
		//	}



			$rs->moveNext();
		}
		return $Options;
	}
	
	public static function retrieveByPk($pk){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppDurationOptions WHERE id = '.$pk.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$Option = new DurationOptions();
			$Option->setID        ( $rs->getId()        );
			$Option->setName      ( $rs->getName()      );
			return $Option;
		}

		return null;
	}
	
	public static function retrieveByName($option_name){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppDurationOptions WHERE `name` LIKE "'.$option_name.'" LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$Option = new DurationOptions();
			$Option->setID        ( $rs->getId()        );
			$Option->setName      ( $rs->getName()      );
			return $Option;
		}
		
		return null;
	}
}