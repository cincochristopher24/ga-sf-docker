<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.Options.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class OptionsPeer {

	public static function populateObjects(iRecordset $rs){
		$Options = array();
		while(!$rs->EOF()){
			$Option = new Options();
			$Option->setID        ( $rs->getId()        );
		//	$Option->setProgramID ( $rs->getProgramID() );
			$Option->setName      ( $rs->getName()      );


			$Options[] = $Option;
			$rs->moveNext();
		}
		return $Options;
	}

	public static function populateObject(iRecordset $rs){
		$Option = self::populateObjects($rs);
		return $Option[0];
	}

	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');

		//$sql = 'SELECT * FROM tblCISOnlineAppOptions WHERE id = '.$id.' LIMIT 1';
		$sql = 'SELECT * FROM tblCISOnlineAppOptions_new WHERE id = '.$id.' AND deferred = 0 LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$Option = self::populateObject($rs);
			return $Option;
		}

		return null;
	}

	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}

		$conn = new dbHandler('db=Travel_Logs');
	//	$sql = 'SELECT * FROM tblCISOnlineAppOptions WHERE id IN ('.implode(",",$ids).')';
		$sql = 'SELECT * FROM tblCISOnlineAppOptions_new WHERE id IN ('.implode(",",$ids).') AND deferred = 0';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$Options = self::populateObjects($rs);
			return $Options;
		}

		return null;
	}

	public static function retrieveAll(){
		$conn = new dbHandler('db=Travel_Logs');
	//	$sql = 'SELECT * FROM tblCISOnlineAppOptions';
		$sql = 'SELECT * FROM tblCISOnlineAppOptions_new WHERE deferred = 0';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$Options = self::populateObjects($rs);
			return $Options;
		}

		return null;
	}

	public static function retrievelOptionsOnProgramsAndTerms(){
		$conn = new dbHandler('db=Travel_Logs');
	//	$sql = 'SELECT a . * , b.TermID FROM tblCISOnlineAppOptions a LEFT JOIN tblCISOnlineAppOptionToTerm b ON a.id = b.OptionID ORDER BY a.name ASC';
		/****
		$sql = 'SELECT a . * , b.TermID, c.program_id
				FROM tblCISOnlineAppOptions AS a
				LEFT JOIN tblCISOnlineAppOptionToTerm AS b ON a.id = b.OptionID
				LEFT JOIN tblCISOnlineAppOptionsToProgram AS c ON a.id = c.option_id
				GROUP BY a.Name, b.TermID, c.program_id
				ORDER BY a.id ASC';

		****/
		/***
		$sql = 'SELECT a . * , b.TermID, c.program_id
				FROM tblCISOnlineAppOptions_new AS a
				LEFT JOIN tblCISOnlineAppOptionToTerm AS b ON a.id = b.OptionID
				LEFT JOIN tblCISOnlineAppOptionsToProgram AS c ON a.id = c.option_id
				WHERE a.deferred = 0
				GROUP BY a.Name, b.TermID, c.program_id
				ORDER BY a.id ASC';
		**/
		$sql = "SELECT a . * , b.program_id, b.term_id
				FROM tblCISOnlineAppOptions_new AS a
				LEFT JOIN tblCISOnlineAppOptionsToProgAndTerm AS b on a.id = b.option_id
				WHERE a.deferred = 0 and b.defered = 0
				GROUP BY a.Name, b.term_id, b.program_id
				ORDER BY a.id ASC";

		$rs = new iRecordset($conn->execQuery($sql));

		$Options = array();
		while(!$rs->EOF()){

		//	if(!array_key_exists($rs->getId(), $Programs)){
				$Option = new Options();
				$Option->setID        ( $rs->getId()        );
				$Option->setProgramID ( $rs->getProgram_id() );
				$Option->setName      ( $rs->getName()      );
				$Option->setTermID    ( $rs->getTerm_id()    );

				$Options[] = $Option;
		//	}
		//	else {
		//		$Programs[$rs->getId()]->setTerms($rs->getTermID());
		//	}



			$rs->moveNext();
		}
		return $Options;
	}

	public static function retrieveByName($option_name){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppOptions_new WHERE `name` LIKE "'.$option_name.'" AND `deferred` = 0 LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$terms = self::populateObject($rs);
			return $terms;
		}

		return null;
	}
}
?>