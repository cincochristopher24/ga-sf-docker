<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.UserCoupon.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class UserCouponPeer {
	/***
	private static $approvedCodes = array('Julie', 'Kelly', 'Nick', 'Luke',
									'Keith', 'Laura B.', 'Kate', 'Karen',
									'Laura S.',	'Christine', 'Kristin');

	private static $approvedCodes = array('BU','MV','KV', 'CA', 'LS', 'TN',
									'LO', 'KB', 'NV', 'KO',
									'KE', 'KG', 'LB', 'JL');
	
	private static $approvedCodes = array('Early Fall', 'Early Summer', 'Discover Summer',
									'Leaps and Bounds', 'Rome', 'Barcelona', 'Kangaroo', 'Vamos',
									'Super Student');
	***/

	public static function retrieveByPersonalInfoID($pID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = sprintf("SELECT * FROM tblCISOnlineAppUserCoupon WHERE personalInfoID = '%s' LIMIT 1",$pID);
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$coupon = new UserCoupon();
			$coupon->setName($rs->getCoupon());
			return $coupon;
		}

		return null;
	}

	/***
	public static function getApprovedCouponCodes(){
		return self::$approvedCodes;
	} ***/
}