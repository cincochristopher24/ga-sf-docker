<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.SchoolInfo.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class SchoolInfoMapper {
	
	public static function populateObjects(iRecordset $rs){
		$schoolInfos = array();
		while(!$rs->EOF()){
			$schoolInfo = new SchoolInfo();
			$schoolInfo->setID             ( $rs->getId()              );
			$schoolInfo->setPersonalInfoID ( $rs->getPersonalInfoID()  );
			$schoolInfo->setSchoolID       ( $rs->getSchoolID()        );
			$schoolInfo->setType           ( $rs->getType()            );
			$schoolInfo->setOtherSchoolName( $rs->getOtherSchoolName() );
			$schoolInfo->setMajor          ( $rs->getMajor()           );
			$schoolInfo->setMinor          ( $rs->getMinor()           );
			$schoolInfo->setYearInSchool   ( $rs->getYearInSchool()    );
			$schoolInfo->setGenAverage     ( $rs->getGenAverage()      );
			$schoolInfo->setCurrentCourses ( $rs->getCurrentCourses()  );
			$schoolInfo->setContact   	   ( $rs->getContact()      );
			$schoolInfo->setEmail		   ( $rs->getEmail()  );
			
			
			$schoolInfos[] = $schoolInfo;
			$rs->moveNext();
		}
		return $schoolInfos;
	}
	
	public static function populateObject(iRecordset $rs){
		$schoolInfo = self::populateObjects($rs);
		return $schoolInfo[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppSchoolInfo WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$schoolInfoObj = self::populateObject($rs);
			return $schoolInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppSchoolInfo WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$schoolInfoObjs = self::populateObjects($rs);
			return $schoolInfoObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppSchoolInfo WHERE PersonalInfoID = '.$personalInfoID.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$schoolInfoObj = self::populateObject($rs);
			return $schoolInfoObj;
		}
		
		return null;
	}
	
}
?>