<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISPartnerSchools.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class CISPartnerSchoolsPeer {
	
	public static function populateObjects(iRecordset $rs){
		$CISPartnerSchools = array();
		while(!$rs->EOF()){
			$CISPartnerSchool = new CISPartnerSchools();
		//	var_dump($term);
			$CISPartnerSchool->setID   ( $rs->getId()   );
			$CISPartnerSchool->setName ( $rs->getName() );
			$CISPartnerSchool->setStateID ($rs->getStateID() );
			$CISPartnerSchools[] = $CISPartnerSchool;
			$rs->moveNext();
		}
		return $CISPartnerSchools;
	}
	
	public static function populateObject(iRecordset $rs){
		$CISPartnerSchools = self::populateObjects($rs);
		return $CISPartnerSchools[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPartnerSchools WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$CISPartnerSchool = self::populateObject($rs);
			return $CISPartnerSchool;
		}
		else {
			// try to retrieve from new table
			return self::retrieveByUniversityID($id);
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPartnerSchools WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$CISPartnerSchools = self::populateObjects($rs);
			return $CISPartnerSchools;
		}
		else {
			// try to retrieve from new table
			return self::retrieveByUniversityIDs($ids);
		}
		
		return null;
	}
	
	public static function retrieveAll(){
		$conn = new dbHandler('db=Travel_Logs');
	//	$sql = 'SELECT * FROM tblCISOnlineAppPartnerSchools';
		$sql = "SELECT univ.*
				FROM tblGaUniversities_new AS univ INNER JOIN 
				tblCISOnlineAppPartnerSchools_new AS partner ON partner.university_id = univ.id
				ORDER BY univ.name";
		$rs = new iRecordset($conn->execQuery($sql));
	//	var_dump($rs);
		if($rs->retrieveRecordCount() > 0){
			$CISPartnerSchools = self::populateObjects($rs);
			return $CISPartnerSchools;
		}
		
		return null;
	}
	
	public static function retrieveAllInArray(){
		$conn = new dbHandler('db=Travel_Logs');
	//	$sql = 'SELECT * FROM tblCISOnlineAppPartnerSchools';
		$sql = "SELECT univ.*
				FROM tblGaUniversities_new AS univ INNER JOIN 
				tblCISOnlineAppPartnerSchools_new AS partner ON partner.university_id = univ.id";
				
		$rs = new iRecordset($conn->execQuery($sql));
	//	var_dump($rs);
		if($rs->retrieveRecordCount() > 0){
			$CISPartnerSchools = array();
			while(!$rs->EOF()){
				$CISPartnerSchool = new CISPartnerSchools();
			//	var_dump($term);
				$CISPartnerSchool->setID   ( $rs->getId()   );
				$CISPartnerSchool->setName ( $rs->getName() );
				$CISPartnerSchool->setStateID ($rs->getStateID() );
				$CISPartnerSchools[$rs->getId()] = $CISPartnerSchool;
				$rs->moveNext();
			}
			
			return $CISPartnerSchools;
		}
		
		return null;
	}
	
	static public function getSchoolStateAbbrByStateID($stateID = 0){
		$conn = new dbHandler('db=GoAbroad_Main');
		$sql = "SELECT stateabbr FROM tbstate where stateID = ".$stateID." LIMIT 1";
		$rs = new iRecordset($conn->execute($sql));
		$stateAbbr = null;
		while(!$rs->EOF()){
			$stateAbbr = $rs->getStateabbr();
			$rs->moveNext();
		}
		
		return $stateAbbr;
	}
	
	public static function retrieveByUniversityID($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = sprintf("SELECT * FROM tblGaUniversities_new WHERE id = '%s' LIMIT 1", $id);
		$rs = new iRecordset($conn->execute($sql));
		$university = null;
		while(!$rs->EOF()){
			$university = new CISPartnerSchools();
		//	var_dump($term);
			$university->setID   ( $rs->getId()   );
			$university->setName ( $rs->getName() );
			$university->setStateID ($rs->getStateID() );
			$rs->moveNext();
		}
		
		return $university;
	}
	
	public static function retrieveByUniversityIDs($ids){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblGaUniversities_new WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execute($sql));
		$university = null;
		while(!$rs->EOF()){
			$university = new CISPartnerSchools();
		//	var_dump($term);
			$university->setID   ( $rs->getId()   );
			$university->setName ( $rs->getName() );
			$university->setStateID ($rs->getStateID() );
			$rs->moveNext();
		}
		
		return $university;
	}
	
}