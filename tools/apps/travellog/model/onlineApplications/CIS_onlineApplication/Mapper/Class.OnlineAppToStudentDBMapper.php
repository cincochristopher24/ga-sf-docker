<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.OnlineAppToStudentDB.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class OnlineAppToStudentDBMapper {
	
	public static function populateObjects(iRecordset $rs){
		$objects = array();
		while(!$rs->EOF()){
			$obj = new OnlineAppToStudentDB();
			$obj->setID          ( $rs->getId()          );
			$obj->setOnlineAppUserID ( $rs->getUserID()  );
			$obj->setStudentID   ( $rs->getStudentID()   );
		
			$objects[] = $obj;
			$rs->moveNext();
		}
		return $objects;
	}
	
	public static function populateObject(iRecordset $rs){
		$objects = self::populateObjects($rs);
		return $objects[0];
	}
	
	public static function retrieveByStudentID($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = sprintf("SELECT * FROM tblCISOnlineAppToStudentDB WHERE studentID = %d LIMIT 1", $id);
	//	echo $sql;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$obj = self::populateObject($rs);
			return $obj;
		}
		
		return null;
	}
	
	public static function retrieveByUserID($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = sprintf("SELECT * FROM tblCISOnlineAppToStudentDB WHERE userID = %d LIMIT 1", $id);
	//	echo $sql;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$obj = self::populateObject($rs);
			return $obj;
		}
		
		return null;
	}
	
	public static function retrieveByAppUserAndStudentID($user_id, $student_id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = sprintf("SELECT * FROM tblCISOnlineAppToStudentDB WHERE userID = %d AND studentID = %d LIMIT 1", $user_id, $student_id);
	//	echo $sql;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$obj = self::populateObject($rs);
			return $obj;
		}
		
		return null;
	}
}