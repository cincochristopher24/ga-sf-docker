<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.HousingOptions.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class HousingOptionsPeer {
	
	public static function populateObjects(iRecordset $rs){
		$terms = array();
		while(!$rs->EOF()){
			$term = new HousingOptions();
		//	var_dump($term);
			$term->setID   ( $rs->getId()   );
			$term->setName ( $rs->getName() );
			
			$terms[] = $term;
			$rs->moveNext();
		}
		return $terms;
	}
	
	public static function populateObject(iRecordset $rs){
		$term = self::populateObjects($rs);
		return $term[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppHousingOptions WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$term = self::populateObject($rs);
			return $term;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppHousingOptions WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$terms = self::populateObjects($rs);
			return $terms;
		}
		
		return null;
	}
	
	public static function retrieveAll(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppHousingOptions';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$terms = self::populateObjects($rs);
			return $terms;
		}
		
		return null;
	}
	
	public static function retrieveHousingOptionsPerProgram(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "SELECT a.*, b.program_id, b.term_id FROM tblCISOnlineAppHousingOptions AS a
				INNER JOIN tblCISOnlineAppProgramToHousingOptions AS b ON a.ID = b.housing_options_id
				ORDER BY a.id ASC";
		$rs = new iRecordset($conn->execQuery($sql));

		$Options = array();
		while(!$rs->EOF()){

		//	if(!array_key_exists($rs->getId(), $Programs)){
				$Option = new HousingOptions();
				$Option->setID        ( $rs->getId()        );
				$Option->setProgramID ( $rs->getProgram_id() );
				$Option->setName      ( $rs->getName()      );
				$Option->setTermID    ( $rs->getTerm_id()  );
				$Options[] = $Option;
		//	}
		//	else {
		//		$Programs[$rs->getId()]->setTerms($rs->getTermID());
		//	}



			$rs->moveNext();
		}
		return $Options;
	}
	
	public static function retrieveByName($option_name){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppHousingOptions WHERE `name` LIKE "'.$option_name.'" LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$Option = new DurationOptions();
			$Option->setID        ( $rs->getId()        );
			$Option->setName      ( $rs->getName()      );
			return $Option;
		}
		
		return null;
	}
}