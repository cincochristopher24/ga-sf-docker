<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.BillingInfo.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class BillingInfoMapper {
	
	public static function populateObjects(iRecordset $rs){
		$billingInfos = array();
		while(!$rs->EOF()){
			$billingInfo = new BillingInfo();
			$billingInfo->setID                 ( $rs->getId()                  );
			$billingInfo->setPersonalInfoID     ( $rs->getPersonalInfoID()      );
			$billingInfo->setPayingPersons      ( $rs->getPayingPersons()       );
			$billingInfo->setMainBillingContact ( $rs->getMainBillingContact()  );
			$billingInfo->setFirstName          ( $rs->getFirstName()           );
			$billingInfo->setLastName           ( $rs->getLastName()            );
			$billingInfo->setRelationship       ( $rs->getRelationship()        );
			$billingInfo->setAddress            ( $rs->getAddress()             );
			$billingInfo->setPhoneNumber        ( $rs->getPhoneNumber()         );
			$billingInfo->setEmail              ( $rs->getEmail()               );
			
			$billingInfos[] = $billingInfo;
			$rs->moveNext();
		}
		return $billingInfos;
	}
	
	public static function populateObject(iRecordset $rs){
		$billingInfo = self::populateObjects($rs);
		return $billingInfo[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppBillingInfo WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$billingInfoObj = self::populateObject($rs);
			return $billingInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppBillingInfo WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$billingInfoObjs = self::populateObjects($rs);
			return $billingInfoObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppBillingInfo WHERE PersonalInfoID = '.$personalInfoID;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$billingInfoObj = self::populateObject($rs);
			return $billingInfoObj;
		}
		
		return null;
	}
	
}
?>