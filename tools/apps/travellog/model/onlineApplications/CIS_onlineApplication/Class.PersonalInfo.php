<?php

/*
 * model created by NASH - 07/27/2009
 * **this is a temporary model as we are dealing with a temporary db since form creation for online app
 *   is still in the making by master Nalds
 *
 */ 

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
require_once('Class.iRecordset.php');


class PersonalInfo {
	
	
	protected 	$id,
				$onlineAppUserID,
				$firstName,
				$lastName,
				$preferredName,
				$gender,
				$birthday,
				$citizenship,
				$countryBirthPlace,
				$secNumber,
				$email,
				$altEmail,
				$billEmail,
				$shirtSize;
				
	private static $lastInsertedID;
	
	protected 	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
		
	}
	
	/* $values must be an associative array
	 * example = $value('txtFirstName' => any value)
	 * 
	 * prefixes: text     - text
	 * 			 textArea - txtA
	 *			 checkbox - chck
	 *			 radio    - rBtn
	 *			 combo	  - cmbo
	 */ 
	
	public function setValues($values = array()){
	//	var_dump(CISOnlineAppFieldsMapper::$personalInfoFieldMap);
		
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$personalInfoFieldMap)){
				$functionName = 'set'.$arrKey;
			//	echo "<br/>".$functionName;
				$this->$functionName($value);
			}
		}
	//	exit;
		
	}
	
	// #### SAVE TO DATABASE ####
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppPersonalInfo SET
					OnlineAppUserID ='.ToolMan::makeSqlSafeString( $this->getOnlineAppUserID()     ).', '.
					'FirstName ='.ToolMan::makeSqlSafeString     ( $this->getFirstName()           ).', '.
					'LastName  ='.ToolMan::makeSqlSafeString     ( $this->getLastName()            ).', '.
					'PreferredName ='.ToolMan::makeSqlSafeString ( $this->getPreferredName()       ).', '.
					'Gender    ='.ToolMan::makeSqlSafeString     ( $this->getGender()              ).', '.
					'BirthDay  ='.ToolMan::makeSqlSafeString     ( $this->getBirthday()            ).', '.
					'Citizenship ='.ToolMan::makeSqlSafeString   ( $this->getCitizenship()         ).', '.
					'CountryBirthPlace='.ToolMan::makeSqlSafeString( $this->getCountryBirthPlace() ).', '.
					'SecNumber ='.ToolMan::makeSqlSafeString     ( $this->getSecNumber()           ).', '.
					'Email    ='.ToolMan::makeSqlSafeString      ( $this->getEmail()               ).', '.
					'AltEmail ='.ToolMan::makeSqlSafeString      ( $this->getAltEmail()            ).', '.
					'BillingEmail = '.ToolMan::makeSqlSafeString ( $this->getBillingEmail()        ) .', '.
					'ShirtSize = '.ToolMan::makeSqlSafeString    ( $this->getShirtSize()           ) ;
		
		$this->conn->execQuery($sql);
		$this->setID($this->conn->getLastInsertedID());
	
	//	self::$lastInsertedID = $this->conn->getLastInsertedID();
	//	var_dump($sql);
	}
	
	public function update(){
		$sql = 'UPDATE tblCISOnlineAppPersonalInfo SET
					OnlineAppUserID ='.ToolMan::makeSqlSafeString( $this->getOnlineAppUserID()    ).', '.
					'FirstName ='.ToolMan::makeSqlSafeString( $this->getFirstName()     ).', '.
					'LastName  ='.ToolMan::makeSqlSafeString( $this->getLastName()    ).', '.
					'PreferredName ='.ToolMan::makeSqlSafeString( $this->getPreferredName()    ).', '.
					'Gender    ='.ToolMan::makeSqlSafeString( $this->getGender()      ).', '.
					'BirthDay  ='.ToolMan::makeSqlSafeString( $this->getBirthday()    ).', '.
					'Citizenship ='.ToolMan::makeSqlSafeString( $this->getCitizenship() ).', '.
					'CountryBirthPlace='.ToolMan::makeSqlSafeString( $this->getCountryBirthPlace() ).', '.
					'SecNumber ='.ToolMan::makeSqlSafeString( $this->getSecNumber()  ).', '.
					'Email    ='.ToolMan::makeSqlSafeString( $this->getEmail()       ).', '.
					'AltEmail ='.ToolMan::makeSqlSafeString( $this->getAltEmail()    ).', '.
					'BillingEmail ='.ToolMan::makeSqlSafeString( $this->getBillingEmail()    ).', '.
					'ShirtSize = '.ToolMan::makeSqlSafeString( $this->getShirtSize() ). ' '.
				'WHERE id = '.ToolMan::makeSqlSafeString( $this->getID() );
					
		$this->conn->execQuery($sql);
		
	}
	
	public function saveProcessAsDone(){
		$sql = 'UPDATE tblCISOnlineAppPersonalInfo SET DoneProcess = 1, dateSubmitted = now() WHERE id = '.ToolMan::makeSqlSafeString( $this->getID() );

		$this->conn->execQuery($sql);
	}
	
	static public function getLastInsertedInfoID(){
		return self::$lastInsertedID;
	}
	
	// ##### SETTERS #####
	public function setID($v){
		$this->id = $v;
	}
	
	public function setOnlineAppUserID($v){
		$this->onlineAppUserID = $v;
	}
	
	public function setFirstName($v){
		$this->firstName = $v;
	}
		
	public function setLastName($v){
		$this->lastName = $v;
	}
	
	public function setPreferredName($v){
		$this->preferredName = $v;
	}
	
	public function setGender($v){
		$this->gender = $v;
	}
	
	public function setBirthday($v){
		$this->birthday = $v;
	}
		
	public function setCitizenship($v){
		$this->citizenship = $v;
	}
	
	public function setCountryBirthPlace($v){
		$this->countryBirthPlace = $v;
	}
	
	public function setSecNumber($v){
		$this->secNumber = $v;
	}
	
	public function setEmail($v){
		$this->email = $v;
	}
	
	public function setAltEmail($v){
		$this->altEmail = $v;
	}
	
	public function setBillingEmail($v){
		$this->billEmail = $v;
	}
	
	public function setShirtSize($v){
		$this->shirtSize = $v;
	}
	/*
	public function setPassNumber($v){
		$this->passNumber = (!is_null($v) ? $v : '');
	}
	
	public function setExpiryDate($v){
		$this->expiryDate = (!is_null($v) ? $v : '');
	}
	*/
	// ##### GETTERS #####
	public function getID(){
		return $this->id;
	}
	
	public function getOnlineAppUserID(){
		return $this->onlineAppUserID;
	}
	
	public function getFirstName(){
		return $this->firstName;
	}
	
	public function getLastName(){
		return $this->lastName;
	}
	
	public function getPreferredName(){
		return $this->preferredName;
	}
	
	public function getGender(){
		return $this->gender;
	}
	
	public function getBirthday(){
		return $this->birthday;
	}
	
	public function getCitizenship(){
		return $this->citizenship;
	}
	
	public function getCountryBirthPlace(){
		return $this->countryBirthPlace;
	}
	
	public function getSecNumber(){
		return $this->secNumber;
	}
	
	public function getEmail(){
		return $this->email;
	}
	
	public function getAltEmail(){
		return $this->altEmail;
	}
	
	
	public function getName(){
		return $this->getFirstName().' '.$this->getLastName();
	}
	
	public function getBillingEmail(){
		return $this->billEmail;
	}
	
	public function getShirtSize(){
		return $this->shirtSize;
	}
	/*
	public function getPassNumber(){
		return $this->passNumber;
	}
	
	public function getExpiryDate(){
		return $this->expiryDate;
	}*/
}

?>