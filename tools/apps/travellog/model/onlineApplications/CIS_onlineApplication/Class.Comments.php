<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
require_once('Class.iRecordset.php');
class Comments {
	
	protected	$message,
				$id;
				
	protected	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	// setters
	public function setMessage($v){ $this->message = $v; }
	public function setGenID($v)  { $this->id = $v;      }
	
	//getters
	public function getMessage(){ return $this->message; }
	public function getGenID()  { return $this->id;      }
	
	// save
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppComments SET
					userID = '.ToolMan::makeSqlSafeString( $this->getGenID()        ).', '.
					'comment  = '.ToolMan::makeSqlSafeString( $this->getMessage()   ).' '.
				'ON DUPLICATE KEY UPDATE '.
					'comment  = '.ToolMan::makeSqlSafeString( $this->getMessage()   );
		
		$this->conn->execQuery($sql);
		
	}
	
	public function delete(){
		$sql = 'DELETE FROM tblCISOnlineAppComments WHERE userID = '.ToolMan::makeSqlSafeString( $this->getGenID() );
		$this->conn->execQuery($sql);
		
	}
	
	public function hasComment(){
		$sql = 'SELECT * from tblCISOnlineAppComments WHERE userID = '.ToolMan::makeSqlSafeString( $this->getGenID() );
		$rs = new iRecordset($this->conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			return true;
		}
		return false;
	}
	
	public function retrieveCommentByUserId($userID){
		$sql = 'SELECT comment FROM tblCISOnlineAppComments WHERE userID = '.$userID.' limit 1';
		$rs = new iRecordset($this->conn->execQuery($sql));
		$message = null;
		if($rs->retrieveRecordCount() > 0){
			while(!$rs->EOF()){
				$message = $rs->getComment();
				$rs->moveNext();
			}
		}
		return $message;
	}
	
}
?>