<?php
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
require_once('Class.Template.php');

echo 'start';
$conn = new dbHandler('db=Travel_Logs');

$sql1 = "SELECT PersonalInfoID as procID FROM tblCISOnlineAppStepsDone";
$rs = new iRecordset($conn->execute($sql1));

$unfinishedProcessIds = array();
foreach($rs as $row){
	$unfinishedProcessIds[] = $row['procID'];
}
/*
echo 'processes<br/>';
$sql2 = 'SELECT id as ID FROM tblCISOnlineAppPersonalInfo WHERE id not in ('.implode(",",$unfinishedProcessIds).')';
echo $sql2.'<br/>';
$rs1 = new iRecordset($conn->execute($sql2));

foreach($rs1 as $row){
	echo $row['ID'].'<br/>';
}


*/
$sql3 = 'UPDATE tblCISOnlineAppPersonalInfo SET DoneProcess = 1 WHERE id not in ('.implode(",",$unfinishedProcessIds).')';
$conn->execute($sql3);
echo "records successfully updated!";

?>