<?php
/**
 * Created on Sep 13 07
 * @author Czarisse Daphne P. Dolina
 * Purpose: Send Email Alerts to Advisors and Club Administrators
 */
 
 	class AdvisorAlert{
 		
 		private $mAlertType 	= '';
 		private $mAlertSender 	= null;
 		private $mFromEmail 	= 'admin@goabroad.net';
 		private $mFromName 		= 'The GoAbroad Network';
 		
 		private $mSubject		= '';
 		private $mBody			= '';	
 		
 		private $mSubjectLine   = '';
 		private $mInstruction   = '';
 		private $mLinkURL   	= '';
 		
 		private $mEntryID 		= 0;
 		private $objGroup 		= null;
 		 		
 		public static $MemberAlert 	= 1;
 		public static $BulletinAlert 	= 2;
 		
 		function __construct(){
 			
 		}
 		
 		
 		//setters
 		function setAlertType($_alertType){
 			$this->mAlertType = $_alertType; 		
 		}
 		
 		function setAlertSender($_sender){
 			$this->mAlertSender = $_sender;
 		}
 		
 		function setFromEmail($_fromEmail){
 			$this->mFromEmail = $_fromEmail;
 		}
 		
 		function setFromName($_fromName){
 			$this->mFromName = $_fromName;
 		}
 		
 		function setEntryID($_entryID){
 			$this->mEntryID = $_entryID;
 		}
 		
 		function setGroup($_objGroup){
 			$this->objGroup = $_objGroup;
 		}
 		
 	
 		// getters
 		function getAlertSender(){
 			return $this->mAlertSender;
 		}
 		
 		function getFromEmail(){
 			return $this->mFromEmail;
 		}
 		
 		function getFromName(){
 			return $this->mFromName;
 		}
 		
 		function getSubject(){
 			return $this->mSubject;
 		}
 		
 		function getBody(){
 			return $this->mBody; 
 		}
 		
 		function initDetails(){
 			
 			switch($this->mAlertType):
 			
 				case AdvisorAlert::$MemberAlert :
 					$this->mSubject = $this->mAlertSender->getUserName() . ' has joined your ' . $this->getGroupType() . ' ' . $this->objGroup->getName();
 					if ($this->objGroup->getGroupAccess() == GroupAccess::$OPEN):
 						$this->mInstruction = 'To view ' . $this->getGroupType() . ' members';
 					elseif ($this->objGroup->getGroupAccess() == GroupAccess::$OPEN_APPROVAL):
 						$this->mInstruction = 'To view pending requests';
 					endif;
 					$this->mLinkURL = 'http://'. $_SERVER['SERVER_NAME'].'/login.php?redirect=members.php?gID='.$this->objGroup->getGroupID();
 					break;
 				
 				case AdvisorAlert::$BulletinAlert :
 					$this->mSubject = $this->mAlertSender->getUserName() . ' has posted a bulletin in your ' . $this->getGroupType() . ' ' . $this->objGroup->getName();
 					$this->mInstruction = 'To see the bulletin post';	
 					$this->mLinkURL = 'http://'. $_SERVER['SERVER_NAME'].'/login.php?redirect=bulletinmanagement.php?hlEntryID='.$this->mEntryID.'&groupID='.$this->objGroup->getGroupID();
 					break;
 				
 				default;
 					break;
 		
 			endswitch;
 			
 			$msg = "Hi %groupname%,

%subjectline%.

%instruction%, please follow the link below:

%link%
		

Many Thanks,

The GoAbroad Network Team" ;

			$this->mBody = str_ireplace(array("%groupname%","%subjectline%","%instruction%","%link%"), array($this->objGroup->getAdministrator()->getUserName(),$this->mSubject,$this->mInstruction,$this->mLinkURL), $msg);
			
				
 		}
 		
 		function getGroupType(){
 			
 			$grpType = '';
 			
 			if ($this->objGroup->getDiscriminator() == Group::FUN):
 				$grpType = 'club';
 			elseif ($this->objGroup->getDiscriminator() == Group::ADMIN):
 				$grpType = 'group';
 			endif;
 			
 			return $grpType;
 			
 		}
 	}
?>