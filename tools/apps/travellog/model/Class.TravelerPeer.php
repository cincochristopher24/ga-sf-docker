<?php
	/**
	 * @(#) Class.TravelerPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 1, 2009
	 */
	 
	require_once("travellog/model/base/Class.BaseTravelerPeer.php");
	
	/**
	 * Peer class of traveler.
	 */
	class TravelerPeer extends BaseTravelerPeer {
		
		/**
		 * Returns the traveler with username equal to the given username.
		 * 
		 * @param string $username The username of the traveler queried.
		 * 
		 * @return Traveler|null the traveler with username equal to the given username; false if username does not exist.
		 */
		static function getTravelerByUserName($username){
			if (is_string($username)) {
				require_once("travellog/model/Class.TravelerPeer.php");
				require_once("Class.Criteria2.php");
				
				$handler = GanetDbHandler::getDbHandler();
				$criteria = new Criteria2();
				$criteria->add(TravelerPeer::USERNAME, $handler->makeSqlSafeString($username));
				$travelers = TravelerPeer::doSelect($criteria);
				
				return (0 < count($travelers)) ? $travelers[0] : null;			
			}
			
			return null;
		}
		
		/**
		 * Returns the traveler with email and password equal to the given email and password, respectively.
		 * 
		 * @param string $email The email of the traveler queried.
		 * @param string $password The password of the traveler queried.
		 * 
		 * @return Traveler|FALSE the traveler with email equal to the given email and password equal to the given password; FALSE if username and password pair does not exist.
		 */
		static function getTravelerIDByEmailAndPassword($email="",$password=""){
			require_once("Class.ConnectionProvider.php");
			require_once("Class.Recordset.php");
			
			$password = "'".addslashes($password)."'";
			$email = "'".addslashes($email)."'";
			$sql = "SELECT travelerID
					FROM tblTraveler
					WHERE email = {$email}
						AND password = {$password}
						AND isSuspended = 0
					LIMIT 1";
			$rs = new Recordset(ConnectionProvider::instance()->getConnection());
			$rs->Execute($sql);

			if( 0 < $rs->Result(0, "travelerID") ){
				return $rs->Result(0, "travelerID");
			}
			return FALSE;
		}
		
		static function getCountTravelJournals(gaTraveler $traveler){
			if( is_null($traveler->getCountTravelJournals()) ){
				$travelerID = $traveler->getID();
				$sqlQuery = "SELECT COUNT(travelID) as travelID 
							FROM tblTravel, tblTravelLink
							WHERE tblTravel.travellinkID = tblTravelLink.travellinkID 
								AND tblTravelLink.reftype = 1 
								AND tblTravelLink.refID = {$travelerID} 
								AND  entryCount > 0 
								AND deleted = 0";
				$rs = new Recordset(ConnectionProvider::instance()->getConnection());
				$rs->Execute($sqlQuery);
				
				$traveler->setCountTravelJournals($rs->Result(0, "travelID"));
			}
			return $traveler->getCountTravelJournals();
		}
		static function isProfileCompleted(gaTraveler $traveler, $includePersonals=TRUE){
			if( is_null($traveler->getIsProfileCompleted()) ){
				$profile = new TravelerProfile($traveler->getID());
				$isCompleted = false;				
				if($includePersonals){	
					if($profile->getFirstname() && $profile->getLastname() && $profile->getEmail()
							&& strtotime($profile->getBirthDay()) != 943891200	&& $profile->getHTLocationID() /*&&	$profile->getTravelerPupose()*/ 						
							&& $profile->getTravelerTypeID() && $profile->getGender()
							&& $profile->getInterests() /*&& $profile->getShortBio()*/ && $profile->getCulture()
							&& $profile->getTravelerProgramInterested() && $profile->getTravelerCountriesInterested() 
						
							//&& ($this->getCountrysTravelled() || $profile->getTravelerCountriesTravelled())
						
							&& ($profile->getOtherprefertravel() || $profile->getTravelerPrefferedTravelType())     
							&& $profile->getIncome() ){
							
					    	$isCompleted = true;		    
				    
					}
				}else{
					if($profile->getFirstName() && $profile->getLastName() && $profile->getEmail()
							&& strtotime($profile->getBirthDay()) != 943891200	&& $profile->getHTLocationID() && $profile->getGender()){
							
					    	$isCompleted = true;		    
					}
				}	
				$traveler->setIsProfileCompleted($isCompleted);
			}
			return $traveler->getIsProfileCompleted();
		}
		
		static function usernameExists($username=""){
			require_once("Class.ConnectionProvider.php");
			require_once("Class.Recordset.php");
			$sql = "SELECT COUNT(*) AS similar 
					FROM tblTraveler
					WHERE tblTraveler.username REGEXP '^($username)$'";
			$rs = new Recordset(ConnectionProvider::instance()->getConnection());
			$rs->Execute($sql);
			
			return 0 < $rs->Result(0, "similar");
		}
		
		static function composeUniqueUsername($username=""){
			require_once("Class.ConnectionProvider.php");
			require_once("Class.Recordset.php");
			$sql = "SELECT COUNT(*) AS similar 
					FROM tblTraveler
					WHERE tblTraveler.username REGEXP '^($username)$'";
			$rs = new Recordset(ConnectionProvider::instance()->getConnection());
			$rs->Execute($sql);
			
			if( 0 < $rs->Result(0, "similar") ){
				$i = 0;
				do{
					$i++;
					$tempName = str_replace(" ","_",$username.($rs->Result(0, "similar")+$i));
				}while( self::usernameExists($tempName) );
				return $tempName;
			}else{
				return str_replace(" ","_",$username);
			}	
		}
	}
	
