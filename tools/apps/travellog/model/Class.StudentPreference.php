<?php
	
	/**
	 * Class.StudentPreference.php
	 * @author marc
	 * Created on Aug 1, 2006
	 * @var array arrPreference - array of programs/preference("program type of GA listings"), only ta,va,ja,ia,sa,ls
	 * @var object nConnection - see Class.Connection.php
	 * @var object nRecordset - see Class.Recordset.php
	 */
 	
 	require_once('Class.Connection.php');
 	require_once('Class.Recordset.php');
 	require_once('Class.ListingProgram.php');
 	
 	class studentPreference{
		
		private $arrPreference = array();
		private $nConnection;
		private $nRecordset;
		
		/**
		 * constructor
		 * @param int getAll - flag :: 1 = retrieves all of GA listing's program type
		 * @param int traveleriID - strudent's travelerID 
		 * @return object self
		 */ 
		function studentPreference($getAll = 0,$travelerID = 0){
			
			try {
 				$this->nConnection = new Connection();
				$this->nRecordset   = new Recordset($this->nConnection);
			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			if ( $getAll > 0 || $travelerID > 0){
				$sqlquery = "SELECT tblIncProgram.incprogramID FROM tblIncProgram";
				if ($travelerID > 0)					
					$sqlquery = $sqlquery . ",tblTravelertoIncProgram " .
					"WHERE tblIncProgram.incprogramID = tblTravelertoIncProgram.incprogramID " .
					"AND tblTravelertoIncProgram.travelerID = " . $travelerID;
					 
				$sqlquery = $sqlquery  . " GROUP BY incprogramID";
				$myquery = $this->nRecordset->Execute($sqlquery);
				while ($row = mysql_fetch_array($myquery)){
					$nProgram = new listingProgram($row["incprogramID"],$travelerID);
					$this->addPreference($nProgram);
				}
			}
			return $this; 
				
		}
		
		/**
		 * @param object program - see Class.ListingProgram.php
		 */
		function addPreference( $program ){
			$this->arrPreference[] = $program;
		}
		
		/**
		 * @param int programID - program ID of the program to be removed
		 */
		function deletePreferenceItem($programID){
			for($i = 0; $i <= count($this->preference); $i++,$myObj=$this->preference[$i]){
				if($programID == $myobj->categoryID)
					{ 	unset($this->preference[$i]);
						break;
					}	
			}
		}
	
		/**
		 * @return array student's program/preferences
		 */
		function getPreference(){
			return $this->arrPreference;
		}
	
	}
 
?>
