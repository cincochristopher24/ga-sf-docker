<?php
	/**
	 * @(#) Class.AdvisorPoster.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 17, 2009
	 */
	
	require_once('travellog/model/Class.PosterPeer.php');
	require_once('travellog/model/Class.AdminGroup.php');
	require_once('travellog/model/Class.Poster.php');
	
	/**
	 * Model class for advisor poster. 
	 */
	class AdvisorPoster extends AdminGroup implements Poster {
	 
		/**
		 * Returns the poster id of the poster.
		 * 
		 * @return int The poster id of the poster.
		 */
		function getPosterId(){
			return $this->getAdministratorID();
		}	
	 
		/**
		 * Returns the poster type of the poster.
		 * 
		 * @return int The poster type of the poster. Returns PosterPeer::ADVISOR.
		 */
		function getPosterType(){
			return PosterPeer::ADVISOR;
		}		
		
	}