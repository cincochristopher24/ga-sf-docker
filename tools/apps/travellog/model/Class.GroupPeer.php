<?php
require_once("travellog/model/base/Class.BaseGroupPeer.php");

/**
 * Peer class of groups. Handles static methods that are related to groups.
 */
class GroupPeer extends BaseGroupPeer 
{
  /**
   * Returns true if the given traveler is admin of the given group.
   * 
   * @param  Traveler  $traveler  The given traveler
   * @param  Group     $group     The given group
   * 
   * @return  boolean  true if the given traveler is admin of the given group. 
   */
  public function isTravelerAdminOfGroup(Traveler $traveler, Group $group)
  {
  	// Return false if traveler or group is new or non existent in the database.
  	if (0 == $traveler->getTravelerID() OR 0 == $group->getGroupID())
  	{
  	  return false;
  	}
  	else
  	{
  	  static $repo =  array();
  	
      if (!isset($repo[$traveler->getTravelerID().'_'.$group->getGroupID()]))
      {
        $repo[$traveler->getTravelerID().'_'.$group->getGroupID()] = ($group->getAdministratorID() == $traveler->getTravelerID() OR $group->isStaff($traveler->getTravelerID()));
      }
    
      return $repo[$traveler->getTravelerID().'_'.$group->getGroupID()];
  	}
  }

  /**
  * Returns an array of groupIDs of main groups where a traveler is a member
  * @param	Traveler	$traveler	The traveler in focus
  * @return Array		an array of groupIDs
  **/
  public static function getIDsOfMainGroupsJoined(Traveler $traveler){
		$travelerID = $traveler->getTravelerID();
		$sql = "SELECT tblGroup.groupID AS groupID
				FROM tblGroup, tblGrouptoTraveler
				WHERE tblGroup.parentID = 0
					AND tblGroup.isSuspended = 0
					AND tblGroup.groupID = tblGrouptoTraveler.groupID
					AND tblGrouptoTraveler.travelerID = $travelerID
				GROUP BY tblGroup.groupID ASC";
		
		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);

		$arrGroupId	= array();

		while($mGroup = mysql_fetch_array($rs->Resultset())){
			$arrGroupId[] = $mGroup['groupID'];
		}

		return $arrGroupId;
  }

  /**
  * gets subgroups of a cobrand group
  * @param		Traveler		$traveler	The traveler
  * @param		AdminGroup		$group		The cobrand group
  * @return 	AdminGroup[] 	array of AdminGroup instances
  **/
  public static function getTravelerCBSubGroups(Traveler $traveler, AdminGroup $group){
  		$groupID = $group->getGroupID();
		$travelerID = $traveler->getTravelerID();
		
		$sql = "SELECT tblGroup.*
				FROM tblGroup, tblGrouptoTraveler
				WHERE tblGroup.parentID = $groupID
					AND tblGroup.isSuspended = 0
					AND tblGroup.groupID = tblGrouptoTraveler.groupID
					AND tblGrouptoTraveler.travelerID = $travelerID
				ORDER BY tblGroup.name ASC";
				
		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);
		
		$groups = array();
		
		while( $data = mysql_fetch_array($rs->Resultset()) ){
			$groups[] = new AdminGroup($data);
		}
		
		return $groups;
  }

  /**
  * gets the advisor groups(only main groups) created by a traveler
  * @param		FilterCriteria2	conditions set in an object instance of FilterCriteria2
  * @param		FilterCriteria2	order by criteria for search results
  * @param		RowsLimit		to limit search results
  * @param		boolean			Determines if to be returned as an array of AdminGroup or an array of groupIDs
  * @return 	array 			AdminGroup[] or int[]
  **/
  public static function getGroups(FilterCriteria2 $filterCriteria2=NULL, FilterCriteria2 $orderCriteria2=NULL, RowsLimit $rowsLimit=NULL, $asObject=TRUE){
	$filterstr = "";
	if( !is_null($filterCriteria2) ){
		$condition = $filterCriteria2->getConditions();	
		$booleanop = $filterCriteria2->getBooleanOps();
		for($i=0;$i<count($condition);$i++){
			$attname   = $condition[$i]->getAttributeName();                    			
			$value 	   = $condition[$i]->getValue();
			
			switch($condition[$i]->getOperation()){	
				case FilterOp::$EQUAL:
					$operator = " = ";
					break;
				case FilterOp::$NOT_EQUAL:
					$operator = " <> ";
					break;
				case FilterOp::$GREATER_THAN_OR_EQUAL:
					$operator = " >= ";
					break;
				default:
					$operator = " = ";
					break;
			}		        					 
			$filterstr .= " " . $booleanop[$i] . " ". $attname . $operator . $value;
		}
	}
	
	$orderstr = "";
	if( !is_null($orderCriteria2) ){
		$orderstr = " ORDER BY ";
    	$condition = $orderCriteria2->getConditions();
		for($i=0; $i<count($condition); $i++){
			$attname   = $condition[$i]->getAttributeName();
			switch($condition[$i]->getOperation()){	
				case FilterOp::$ORDER_BY:
					$sortorder = " ASC ";
					break;
				case FilterOp::$ORDER_BY_DESC:
					$sortorder = " DESC ";
					break;
				default:
					$sortorder = " ASC ";
					break;
			}
			$orderstr .= $attname . $sortorder;
			$orderstr .= count($condition)-1 > $i ?  ", " : "";
		} 		
    }
	
	$limitstr = "";
	if ( !is_null($rowsLimit) ){
        $offset   = $rowsLimit->getOffset();
        $rows     = $rowsLimit->getRows();
        $limitstr = " LIMIT " . $offset . " , " . $rows;
    }
	
  	$sql = "SELECT *
			FROM tblGroup
			WHERE 1 = 1 
			$filterstr
			$orderstr
			$limitstr";
	
	$rs = ConnectionProvider::instance()->getRecordset();
	$rs->execute($sql);
	
	$groups = array();
	while( $data = mysql_fetch_array($rs->Resultset()) ){
		$groups[] = $asObject ? new AdminGroup($data) : $data["groupID"];
	}
	
	return $groups;
  }

  public static function getRecentSubgroups(AdminGroup $group, $includeFeatured=FALSE, $limitStr=NULL){
		$groupID = $group->getGroupID();
		
		$includeFeatured_exp = "";
		if( !$includeFeatured ){
			$includeFeatured_exp = " AND tblGroup.groupHomeFeatured = 0 ";
		}
		$limitStr = is_null($limitStr) ? " LIMIT 0,10" : $limitStr;
		
		$sql = "SELECT tblGroup.*
				FROM tblGroup
				WHERE tblGroup.parentID = $groupID
					AND tblGroup.isSuspended = 0
					$includeFeatured_exp
				ORDER BY tblGroup.groupID DESC
				$limitStr";
		
		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);

		$groups = array();
		
		while( $data = mysql_fetch_array($rs->Resultset()) ){
			$groups[] = new AdminGroup($data);
		}
		
		return $groups;
  }

  public static function countRecentSubgroups(AdminGroup $group, $includeFeatured=FALSE){
		$groupID = $group->getGroupID();
		
		$includeFeatured_exp = "";
		if( !$includeFeatured ){
			$includeFeatured_exp = " AND tblGroup.groupHomeFeatured = 0 ";
		}
		
		$sql = "SELECT COUNT(*) AS resultCount
				FROM tblGroup
				WHERE tblGroup.parentID = $groupID
					AND tblGroup.isSuspended = 0
					$includeFeatured_exp";
		
		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);
		
		$data = mysql_fetch_array($rs->Resultset());
		return $data["resultCount"];
  }

	public static function getRecentSubgroupsAsSubGroup(AdminGroup $group, $includeFeatured=FALSE, $recentN=10){
		$groupID = $group->getGroupID();

		$includeFeatured_exp = "";
		if( !$includeFeatured ){
			$includeFeatured_exp = " AND tblGroup.groupHomeFeatured = 0 ";
		}

		$sql = "SELECT tblGroup.*
				FROM tblGroup
				WHERE tblGroup.parentID = $groupID
					AND tblGroup.isSuspended = 0
					$includeFeatured_exp
				ORDER BY tblGroup.groupID DESC
				LIMIT 0,{$recentN}";

		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);

		$groups = array();

		while( $data = mysql_fetch_array($rs->Resultset()) ){
			$groups[] = new SubGroup($data);
		}

		return $groups;
  }

/**
  * gets featured subgroups of a main group
  * @param		AdminGroup		$group		The main group object
  * @return 	AdminGroup[] 	array of AdminGroup instances
  **/
  public static function getFeaturedGroups(AdminGroup $group){
  		$groupID = $group->getGroupID();

		$sql = "SELECT tblGroup.*
				FROM tblGroup
				WHERE tblGroup.parentID = $groupID
					AND tblGroup.isSuspended = 0
					AND tblGroup.groupHomeFeatured > 0
				ORDER BY tblGroup.groupHomeFeaturedRank ASC, tblGroup.name ASC";

		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);

		$groups = array();

		while( $data = mysql_fetch_array($rs->Resultset()) ){
			$groups[] = new AdminGroup($data);
		}

		return $groups;
  }

  public static function countSubgroups(AdminGroup $group){
		$groupID = $group->getGroupID();

		$sql = "SELECT COUNT(*) AS resultCount
				FROM tblGroup
				WHERE tblGroup.parentID = $groupID
					AND tblGroup.isSuspended = 0";

		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);

		$data = mysql_fetch_array($rs->Resultset());
		return $data["resultCount"];
  }

/**
  * gets featured subgroups of a main group
  * @param		AdminGroup		$group		The main group object
  * @return 	AdminGroup[] 	array of AdminGroup instances
  **/
  public static function getFeaturedGroupsAsSubGroup(AdminGroup $group){
  		$groupID = $group->getGroupID();

		$sql = "SELECT tblGroup.*
				FROM tblGroup
				WHERE tblGroup.parentID = $groupID
					AND tblGroup.isSuspended = 0
					AND tblGroup.groupHomeFeatured > 0
				ORDER BY tblGroup.groupHomeFeaturedRank ASC, tblGroup.name ASC";

		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);

		$groups = array();

		while( $data = mysql_fetch_array($rs->Resultset()) ){
			$groups[] = new SubGroup($data);
		}

		return $groups;
  }

  public static function getTaggedGroups(AdminGroup $group, $tags, $limitStr = ""){
		$groupID = $group->getGroupID();

		$_tags = array();
		foreach($tags As $tag){
			$_tags[] = "'".$tag."'";
		}
		$tags_exp = implode(",",$_tags);
		
		$tag_count = count($tags);
		
		$sql = "SELECT tblGroupTags.* 
				FROM tblGroup, tblGroupTags, tblPageTags
				WHERE tblGroupTags.groupID = tblGroup.groupID
				AND tblGroupTags.pageTagID = tblPageTags.ID
				AND tblPageTags.groupID = {$groupID}
				AND tblPageTags.name IN ($tags_exp)
				GROUP BY tblGroupTags.groupID
				HAVING COUNT( tblGroupTags.groupID ) = {$tag_count}
				$limitStr";
		
		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);
		
		$groups = array();
		while( $data = mysql_fetch_array($rs->Resultset()) ){
			$groups[] = new AdminGroup($data["groupID"]);
		}
		return $groups;
  }

  public static function countTaggedGroups(AdminGroup $group, $tags){
		$groupID = $group->getGroupID();

		$_tags = array();
		foreach($tags As $tag){
			$_tags[] = "'".$tag."'";
		}
		$tags_exp = implode(",",$_tags);
		
		$tag_count = count($tags);
		
		$sql = "SELECT tblGroupTags.*
				FROM tblGroup, tblGroupTags, tblPageTags
				WHERE tblGroupTags.groupID = tblGroup.groupID
				AND tblGroupTags.pageTagID = tblPageTags.ID
				AND tblPageTags.groupID = {$groupID}
				AND tblPageTags.name IN ($tags_exp)
				GROUP BY tblGroupTags.groupID
				HAVING COUNT( tblGroupTags.groupID ) = {$tag_count}";
		
		$rs	=	ConnectionProvider::instance()->getRecordset();
		$rs->execute($sql);
		
		return $rs->Recordcount();
  }

  public static function directSignupAllowed($groupID=0){
  	try{
		$group = new AdminGroup($groupID);
		return $group->directSignupAllowed();
	}catch(exception $e){
		return FALSE;
	}
  }

}
	
