<?php
  /**
   * @(#) Class.SqlFields.php
   * 
   * Handler of sql fields.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 12 11, 08
   */
  
  require_once "travellog/model/Class.SqlField.php";
  
  class SqlFields {
  	private $fields = array();
  	
  	/**
  	 * Adds a column.
  	 * 
  	 * @param string $fld field name
  	 * @param string $table table name
  	 * @param string $db database name
  	 * @param string $alias the alias in sql
  	 * @return void
  	 */
  	function addField($fld = "", $table = "", $db = "", $alias = ""){
  		$this->fields[] = new SqlField($fld, $table, $db, $alias);
  	}
  	
  	/**
  	 * Gets all the fields.
  	 * 
  	 * @return array Array of SqlField
  	 */
  	function getAllSqlFields(){
  		return $this->fields;
  	}
  }
