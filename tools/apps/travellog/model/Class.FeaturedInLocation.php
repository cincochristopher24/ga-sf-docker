<?php
/*
 * Created on Aug 8, 2006
 *
 * Created by: Czarisse Daphne P. Dolina
 */

	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.Travel.php");
	require_once("travellog/model/Class.Traveler.php");
	
	 
	class FeaturedInLocation{
		
		private $featphoto 		= array();
		private $feattravel 		= array();
		private $feattraveler	= array();
		
		private static $arrtravellog	= array();
		
		private static $photocount        = 0;
		private static $travelcount       = 0;
		private static $currtravelercount = 0;
		private static $httravelercount   = 0;
		
		// get travellog photos from travellogs OR travels set in a specific location
		public static function getFeaturedPhoto($arrlocID = '', $numrows = 1){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
				$rs2  = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$arrlocstr = implode(',',$arrlocID);
			
			$sqlcount = "SELECT count( distinct tblTravel.travelerID) as totalrec " .
					"FROM tblTravel, tblTrips, tblTravelLog, tblTravelLogtoPhoto " .
					"WHERE tblTravel.travelID = tblTrips.travelID " .
					"AND tblTrips.tripID = tblTravelLog.tripID " .
					"AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID " .
					//"AND (tblTravel.locID IN ($arrlocstr) OR tblTrips.locID IN ($arrlocstr) )";
					"AND tblTrips.locID IN ($arrlocstr)";
 			$rs->Execute($sqlcount);
 			
 			if ($rs->Result(0,"totalrec") == 0){
				try {
					return array();
		 		}
				catch (Exception $e) {				   
				   throw $e;
				}
			}
			
			else {
				
				$listtraveler = "";
				$filterstr = "";
				
				$arrtrav = array();
				
				self::$photocount = $rs->Result(0,"totalrec");
				
				if ($rs->Result(0,"totalrec") < $numrows)
					$numrows = $rs->Result(0,"totalrec");
					
				$max = $rs->Result(0,"totalrec") - 1;
				$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
				
				while (count($arrtrav) < $numrows){
		 			
		 			// get the current recordcount with filter
						if (count($arrtrav)){
							$listtraveler = implode(',',$arrtrav);
							$filterstr = "AND tblTravel.travelerID NOT IN ($listtraveler) ";
						}
						
						$sqlcount = $sqlcount . $filterstr  ;
	 					$rs->Execute($sqlcount);
	 			
						$max = $rs->Result(0,"totalrec") - 1;
						$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
					
					
					// get the random record	
		 			$sql = "SELECT tblTravel.travelerID, tblTravelLogtoPhoto.photoID, tblTravelLogtoPhoto.travellogID " .
		 					"FROM tblTravel, tblTrips, tblTravelLog, tblTravelLogtoPhoto " .
		 					"WHERE tblTravel.travelID = tblTrips.travelID " .
		 					"AND tblTrips.tripID = tblTravelLog.tripID " .
		 					"AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID " .
		 					//"AND (tblTravel.locID IN ($arrlocstr) OR tblTrips.locID IN ($arrlocstr)) " . $filterstr .
		 					"AND tblTrips.locID IN ($arrlocstr) " . $filterstr .
		 					"ORDER BY tblTravelLogtoPhoto.photoID LIMIT " . $randidx . " , 1" ;
	 				$rs->Execute($sql);
	 				
	 				try {
		 				$travellog = new TravelLog($rs->Result(0,"travellogID"));
		 				$featphoto[] = new Photo($travellog, $rs->Result(0,"photoID"));	
		 				
		 				FeaturedInLocation::$arrtravellog[] = $travellog;
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}	 							
		 				
		 			$arrtrav[] = $rs->Result(0,"travelerID");		// puts selected index in array; value should be unique;
		 			
				}
	 				
			}
 			
			return $featphoto;
			
		}
		
		public static function getFeaturedPhotoCount(){
			return FeaturedInLocation::$photocount;
		}
		
		public static function getFeatPhotoLog(){
	 		return FeaturedInLocation::$arrtravellog;
	 	}
	 	
		// get travels set in a specific location
		public static function getFeaturedTravel_Orig($arrlocID = '', $numrows = 1){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$arrlocstr = implode(',',$arrlocID);
			
			$sqlcount = "SELECT count(distinct travelID) as totalrec " .
					//"FROM tblTravel " .
					"FROM tblTrips " .
					"WHERE locID IN ($arrlocstr) ";
	 		$rs->Execute($sqlcount);
 			 
 			if ($rs->Result(0,"totalrec") == 0){
				return array();	 		
			}
			
			else {
				
				$listtravel = "";
				$filterstr = "";
				
				$arrtravel = array();
				
				self::$travelcount = $rs->Result(0,"totalrec");
				
				if ($rs->Result(0,"totalrec") < $numrows)
					$numrows = $rs->Result(0,"totalrec");
					
	 			$max = $rs->Result(0,"totalrec") - 1;
				$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
				
				while (count($arrtravel) < $numrows){
		 			// get the current recordcount with filter
						if (count($arrtravel)){
							$listtravel = implode(',',$arrtravel);
							//$filterstr = " AND tblTravel.travelID NOT IN ($listtravel) ";
							$filterstr = " AND tblTrips.travelID NOT IN ($listtravel) ";
						}
						
						$sqlcount = $sqlcount . $filterstr ;
		 				$rs->Execute($sqlcount);
		 				
		 				$max = $rs->Result(0,"totalrec") - 1;
						$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
					
					// get the random record	
		 			$sql = "SELECT tblTravelLog.travelID , tblTravelLog.travellogID " . 
		 					//"FROM tblTravel " .
		 					"FROM tblTrips, tblTravelLog " .
		 					"WHERE locID IN ($arrlocstr) " . $filterstr .
		 					" AND tblTrips.tripID = tblTravelLog.tripID " .
		 					"ORDER BY travelID LIMIT " . $randidx . " , 1" ;		 			
		 			$rs->Execute($sql);
		 			
		 			try {
		 				$feattravel[$rs->Result(0,"travellogID")] = new Travel($rs->Result(0,"travelID") );		 				
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}	 	
				
					$arrtravel[] = $rs->Result(0,"travelID"); 		// puts selected index in array; value should be unique;
								
	 			}
			}
			
			return $feattravel;
			
		}
		
		
		// get travels set in a specific location
		public static function getFeaturedTravel($arrlocID = '', $numrows = 1){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$arrlocstr = implode(',',$arrlocID);
			$feattravel = array();
			$limitstr = "LIMIT 0 , " . $numrows ;
			
			// new code implements privacy preferences
			$sql2 = "SELECT tblTravel.travelID as travelID FROM tblTravel,tblTrips, tblTravelLog WHERE  tblTravel.travelID = tblTrips.travelID AND tblTravel.privacypref = 0 AND tblTravelLog.travelID = tblTravel.travelID AND tblTrips.locID IN ( ". $arrlocstr . " )"; 
			
			//if logged in
			if (isset($_SESSION['travelerID'])){
				
				//get journals of current traveler's friend's journals
				$unionStr1 = "SELECT tblTravel.travelID as travelID 
				FROM tblTravel, tblTravelLog, tblTrips, tblFriend , tblTravelLink 
				WHERE  tblTravel.travellinkID = tblTravelLink.travellinkID 
				AND tblTravelLink.reftype = 1 
				AND tblTravelLink.refID = tblFriend.friendID 
				AND tblTravel.travelID = tblTrips.travelID
				AND tblTrips.travelID = tblTravelLog.travelID 
				AND tblTrips.locID IN ( ". $arrlocstr . 
				" ) AND tblFriend.travelerID = " . $_SESSION['travelerID'] . " AND tblTravel.privacypref IN (" . 				JournalPrivacyPreferenceType::$FRIENDS . "," .  JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS . ")";
				/**
				//get journals of current traveler's groupmates
				$unionStr2 = "SELECT tblTravel.travelID as travelID 
				FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink 
				WHERE tblTravel.travellinkID = tblTravelLink.travellinkID 
				AND tblTravelLink.reftype = 1 
				AND tblTravel.travelID = tblTrips.travelID
				AND tblTrips.travelID = tblTravelLog.travelID
				AND tblTravelLink.refID IN (SELECT b.travelerID 
					FROM tblGrouptoTraveler AS a, tblGrouptoTraveler AS b
					WHERE a.groupID = b.groupID
					AND a.travelerID = " . $_SESSION['travelerID'] . " AND a.travelerid <> b.travelerid
					GROUP BY b.travelerid) 
			    AND tblTravel.privacypref IN (" . JournalPrivacyPreferenceType::$GROUP_MEMBERS . "," .  JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS . 
				") AND tblTrips.locID IN ( ". $arrlocstr . " )";
			
				// get your own journals
				$unionStr3 = "SELECT tblTravel.travelID as travelID 
				FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink 
				WHERE tblTravel.travellinkID = tblTravelLink.travellinkID 
				AND tblTravelLink.reftype = 1 
				AND tblTravelLink.refID = "  . $_SESSION['travelerID'] .
				" AND tblTravel.travelID = tblTrips.travelID
				AND tblTrips.travelID = tblTravelLog.travelID
				AND tblTrips.locID IN ( ". $arrlocstr . " )";
				*/
				
				//get journals of current traveler's groups
				$unionStr2 = "SELECT tblTravel.travelID as travelID
				FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink 
				WHERE tblTravel.travellinkID = tblTravelLink.travellinkID 
				AND tblTravelLink.reftype = 2 
				AND tblTravel.travelID = tblTrips.travelID
				AND tblTrips.travelID = tblTravelLog.travelID
				AND tblTravelLink.refID IN (SELECT groupID 
					FROM tblGrouptoTraveler
					WHERE travelerID = " . $_SESSION['travelerID'] . " ) " . 
			    " AND tblTravel.privacypref IN (" . JournalPrivacyPreferenceType::$GROUP_MEMBERS . "," .  JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS . 
				") AND tblTrips.locID IN ( ". $arrlocstr . " )";
			
				require_once('travellog/model/Class.AdminGroup.php');
				$advGroupID = AdminGroup::getAdvisorGroupID($_SESSION['travelerID']);
				
				if (0 < $advGroupID) {
					
					// get your own journals as an advisor
					$unionStr3 = "SELECT tblTravel.travelID as travelID
					FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink 
					WHERE tblTravel.travellinkID = tblTravelLink.travellinkID 
					AND tblTravel.travelID = tblTrips.travelID
					AND tblTrips.travelID = tblTravelLog.travelID
					AND tblTrips.locID IN ( ". $arrlocstr . " )" .
					" AND tblTravelLink.reftype = 2 
					AND (tblTravelLink.refID = " . $advGroupID . 
					" OR tblTravelLink.refID IN (SELECT groupID 
						FROM tblGroup
						WHERE parentID = " . $advGroupID  . " )) ";
				
					
				}else {
				
					// get your own journals
					$unionStr3 = "SELECT tblTravel.travelID as travelID
					FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink 
					WHERE tblTravel.travellinkID = tblTravelLink.travellinkID 
					AND tblTravelLink.reftype = 1 
					AND tblTravelLink.refID = "  . $_SESSION['travelerID'] .
					" AND tblTravel.travelID = tblTrips.travelID
					AND tblTrips.travelID = tblTravelLog.travelID
					AND tblTrips.locID IN ( ". $arrlocstr . " )";
					
				
				}
									
				$sql2 =  $sql2 . " UNION " . $unionStr1 . " UNION " . $unionStr2 . " UNION " . $unionStr3 ;
				 
			}
			
			$sql =  "SELECT * FROM ( " .
						"SELECT * FROM ( " .
							"SELECT tblTravelLog.travelID as travelID, tblTravelLog.logdate as _date, tblTravelLog.travellogID " . 
		 					"FROM tblTrips, tblTravelLog " .
		 					"WHERE locID IN ($arrlocstr) " . 
		 					"AND tblTrips.tripID = tblTravelLog.tripID " .
		 					"AND  tblTravelLog. lastedited = '0000-00-00 00:00:00' " .
		 					"UNION " .
		 					"SELECT tblTravelLog.travelID as travelID, tblTravelLog.lastedited as _date, tblTravelLog.travellogID " . 
		 					"FROM tblTrips, tblTravelLog " .
		 					"WHERE locID IN ($arrlocstr) " . 
		 					"AND tblTrips.tripID = tblTravelLog.tripID " .
		 					"AND  tblTravelLog. lastedited != '0000-00-00 00:00:00' " .
						" ) as _tb2  WHERE travelID IN (" . $sql2 . " )   ORDER BY _tb2. _date DESC " .
		 			") as _tb  GROUP BY _tb.travelID ORDER BY _tb._date DESC " 	;
		 	
 			$rs->Execute($sql);
 				
 			self::$travelcount = $rs->Recordcount();
 			
 			if ($rs->Recordcount()){
				
				$sql = $sql . $limitstr ;
				$rs->Execute($sql);
				
				while ($recordset = mysql_fetch_array($rs->Resultset())) {
	 				try {
 						$feattravel[$recordset["travellogID"]] = new Travel($recordset["travelID"]);		 									
 					} 					
					catch (Exception $e) {
					   throw $e;
					   
					}					
 				}
			}
		
			return $feattravel ;
		}
		
		
		public static function getFeaturedTravelCount(){
			return FeaturedInLocation::$travelcount;
		}
		
		// get travelers who are currently in a specific location
		// OR		
		// get travelers who live in this location
		public static function getFeaturedTraveler($arrlocID = '', $numrows = 1, $current = 0){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
				$rs2   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$arrlocstr = implode(',',$arrlocID);
			
		 	if (1 == $current){
			 	$sqlcount = "SELECT tblTraveler.travelerID  " .
							"FROM tblTraveler " .
							"WHERE currlocationID IN ($arrlocstr) " .
							"AND travelerID not in (select distinct travelerID from tblGrouptoAdvisor) " ;
		 	}
		 	else{
		 		$sqlcount = "SELECT tblTraveler.travelerID  " .
							"FROM tblTraveler " .
							"WHERE htlocationID IN ($arrlocstr)" .
							"AND currlocationID NOT IN ($arrlocstr) " .
							"AND travelerID not in (select distinct travelerID from tblGrouptoAdvisor) " ;
		 	}		
								
		 	$rs->Execute($sqlcount);
		 	
		 	if ($rs->Recordcount() == 0 ){
				return array();	 		
			}
			
			else {
				
				$listtraveler = "";
				$filterstr = "";
				
				$arrtrav = array();
				
				if (1 == $current)
					self::$currtravelercount = $rs->Recordcount();
				else
					self::$httravelercount   = $rs->Recordcount();
				
				
				if ($rs->Recordcount() < $numrows)
					$numrows = $rs->Recordcount();
					
	 			$max = $rs->Recordcount() - 1;
				$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
				
				while (count($arrtrav) < $numrows){
			 		
			 		// get the current recordcount with filter
						if (count($arrtrav)){
							$listtraveler = implode(',',$arrtrav);
							$filterstr = " AND travelerID NOT IN ($listtraveler) ";
						}
						
						$sqlcount2 = $sqlcount . $filterstr ;
					 		
						$rs->Execute($sqlcount2);
		 				
		 				$max = $rs->Recordcount() - 1;
						$randidx = mt_rand(0,$max); 		 		// get a random index from the recordcount
					
						// get the random record
						$sql = $sqlcount . $filterstr . "ORDER BY travelerID LIMIT " . $randidx . " , 1"  ;
				 			
		 				$rs->Execute($sql);
		 			
		 			
		 			try {
	 					$feattraveler[] = new Traveler($rs->Result(0,"travelerID"));
	 				}
					catch (Exception $e) {				   
					   throw $e;
					}
						
					$arrtrav[] = $rs->Result(0,"travelerID");			// puts selected travelerID in array; value should be unique;
					
	 			}	
			}
			
			return $feattraveler;
		}
		
		public static function getFeaturedTravelerCount($current = 0){
			if (1 == $current)
				return FeaturedInLocation::$currtravelercount;
			else
				return FeaturedInLocation::$httravelercount;
		}
		
	}
	
?>