<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: 
 */
        
    require_once("travellog/model/Class.Group.php");
    require_once("travellog/model/Class.Photo.php");
    require_once('Class.dbHandler.php');
    
    require_once("travellog/model2/group/Class.ganetGroupService.php");				// could be removed in future after refactoring the services
	require_once("travellog/vo/Class.ganetValueObject.php");
	require_once("travellog/model2/dao/Class.ganetFunGroupDao.php");
    
    class FunGroup extends Group{  
         
         private $mPhotos    = array();
         public  $phototype	= 5;
         private $randphoto    = NULL; 
                
        /**
         * Define variables for Class Member Attributes
         */
          //  private $mGroupEvents  = array();
           
           
            
        /**
         * Constructor Function for this class
         */
            function FunGroup ($groupID = 0){
                $this->mGroupID = $groupID;
                
                try {
                    $this->mConn = ConnectionProvider::instance()->getConnection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                $this->dao	=	new ganetFunGroupDao();
                
                if (0 < $this->mGroupID){
                   /* $sql = "SELECT * FROM tblGroup WHERE groupID = '$this->mGroupID' ";
                    $this->mRs->Execute($sql);
                
                    if (0 == $this->mRs->Recordcount()){
                        throw new Exception("Invalid GroupID");        // ID not valid so throw exception
                    }
                    
                    // Sets values to its class member attributes.
                    $this->mSendableID       = $this->mRs->Result(0,"sendableID");         
                    $this->mName             = stripslashes($this->mRs->Result(0,"name"));           
                    $this->mDescription      = stripslashes($this->mRs->Result(0,"description"));       
                    $this->mDiscriminator    = $this->mRs->Result(0,"discriminator");         
                    $this->mGroupAccess      = $this->mRs->Result(0,"groupaccess");  
                    $this->mAdministratorID  = $this->mRs->Result(0,"administrator");         
                    $this->mModeratorID      = $this->mRs->Result(0,"moderator");
                    $this->mGroupPhotoID     = $this->mRs->Result(0,"photoID");
                    $this->mGroupRank        = $this->mRs->Result(0,"inverserank");
                    $this->mDateCreated        = $this->mRs->Result(0,"datecreated");
 					*/
 					
 					$gServ = new ganetGroupService();
					$vo = new ganetValueObject();
					$vo->set('id',$groupID);
					$group = $gServ->retrieve($vo);
					
					$this->setFunGroupData($group->props);			// temporary 4/6/09 by kgordo                   
                }
                
            }

			// set Fun Group data in array and assign values; minimize db query
			function setFunGroupData($_data = array() ) {
				$this->props = $_data;
				
				if (count($_data)){
			
					$this->mGroupID			= isset($_data["groupID"]) ? $_data["groupID"] : 0; 
					$this->mSendableID 		= isset($_data["sendableID"]) ? $_data["sendableID"] : 0;
					$this->mName 			= isset($_data["name"]) ? $_data["name"] : '';
		 			$this->mDescription 	= isset($_data["description"]) ? $_data["description"] : '';
		 			$this->mDiscriminator 	= isset($_data["discriminator"]) ? $_data["discriminator"] : 1;
		 			$this->mGroupAccess 	= isset($_data["groupaccess"]) ? $_data["groupaccess"] : 1;
		 			$this->mAdministratorID = isset($_data["administrator"]) ? $_data["administrator"] : null;
		 			$this->mModeratorID     = isset($_data["moderator"]) ? $_data["moderator"] : 0;
		 			$this->mGroupPhotoID 	= isset($_data["photoID"]) ? $_data["photoID"] : 0; 
		 			$this->mGroupRank 		= isset($_data["inverserank"]) ? $_data["inverserank"] : 0;
		 			$this->mDateCreated		= isset($_data["datecreated"]) ? $_data["datecreated"] : '';			
				
				}
			}
			
                    
       
       /**
         * CRUD Methods (Create, Update , Delete)
         * In Create, add a record first in tblSendable before creating an Admin Group.
         * In Delete(), all group events associated with this fun group must be deleted also.
         */  
        
             function Create(){
                $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
             	$handler = new dbHandler();
                $sql = "INSERT into tblSendable (sendabletype) " .
                        "VALUES ('2')";
                $this->mRs->Execute($sql);       
                $this->mSendableID = $this->mRs->GetCurrentID();
                $aname         = $handler->makeSqlSafeString($this->mName);
                $adescription  = $handler->makeSqlSafeString($this->mDescription);
                
                $sql = "INSERT into tblGroup (sendableID, name, description, groupaccess, discriminator, administrator, moderator, photoID) " .
                        "VALUES ('$this->mSendableID', $aname, $adescription, '$this->mGroupAccess', '$this->mDiscriminator', '$this->mAdministratorID', 0, '$this->mGroupPhotoID'  )";
                $this->mRs->Execute($sql);       
                $this->mGroupID = $this->mRs->GetCurrentID();
                
                $sql2 = "UPDATE tblGroup SET inverserank = " . $this->mGroupID . " WHERE groupID = " . $this->mGroupID ;
                $this->mRs->Execute($sql2);
                
            }
            
		/**
		 * Inserts a new entry or updates an entry in the database. If groupID is greater
		 * than 0, this updates an entry in the database. If groupID is less than or equal
		 * to 0, this inserts a new entry in the database.
		 * 
		 * @throws exception Throws exception when the connection to host failed.
		 * 
		 * @return void 
		 */
		function save(){
			
		}
            
            function Update(){
                $this->dao->update($this);
                
                /*$handler = new dbHandler();
                $aname         = $handler->makeSqlSafeString($this->mName);
                $adescription  = $handler->makeSqlSafeString($this->mDescription);
                
                $sql = "UPDATE tblGroup SET name = $aname, description = $adescription, groupaccess = '$this->mGroupAccess' WHERE groupID = '$this->mGroupID'  " ;
                $this->mRs->Execute($sql);*/
            }
            
            
            function Delete(){
				/**
				 *	Last Edited By: Cheryl Ivy Q. Go	20 October 2008
				 *	Purpose: Clubs can now be deleted so I edited this function so that a fun group can be deleted regardless if it still has contents
				**/

	               $myphotos = $this->getPhotos();
	            for($idx = 0; $idx < count($myphotos); $idx++){
	               	$eachphoto = $myphotos[$idx]; 
	                   $this->removePhoto($eachphoto->getPhotoID());		// delete photos of Fun Group
	               }

				// delete all messages, invites, requests, members and group events
	               $this->removeAllGroupLinks();

	              $this->dao->delete($this);
	              
	              /* $sql = "DELETE FROM tblGroup WHERE groupID = " . $this->mGroupID;
	               $this->mRs->Execute($sql);

				$sql = "DELETE FROM tblSendable WHERE sendableType = 2 AND sendableID = " . $this->mSendableID;
				$this->mRs->Execute($sql);*/
				$this->invalidateCacheKeys($this->mGroupID);
           }
           
           
        /**
         * Purpose: Adds photo to this fun group.
         */
            function addPhoto($photo){
                $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
                //editted by: joel
            	//################################
            	$sqlq = "SELECT max(position) as mposition from tblGrouptoPhoto WHERE groupID = '$this->mGroupID'";
	 			$this->mRs->Execute($sqlq);
            	
            	
            	if($this->mRs->Recordcount() > 0){
            			$rec = mysql_fetch_array($this->mRs->Resultset());
            			$pos = $rec['mposition']+1;           			
            	}else{
            			$pos =0;	
            	}
            	//################################
                
                $photoID = $photo->getPhotoID();
                $sql = "INSERT into tblGrouptoPhoto (groupID, photoID, position) VALUES ('$this->mGroupID', '$photoID', '$pos')";
                $this->mRs->Execute($sql);                
            }
          
            
        /**
         * Purpose: Removes a photo from this fun group.
         * Deletes the photo as well as its link to this fun group.
         */
            function removePhoto($photoID){
                $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
                //$photo = new Photo($this, $photoID);                                        // delete photo
                //$photo->Delete();
                
                $sql = "DELETE FROM tblGrouptoPhoto " .
                        "WHERE photoID = '$photoID' ";      // delete photo link to program
                $this->mRs->Execute($sql);       
            }
           
          //added by: joel
            
            function reArrangePhoto($marrphotoID){
            	$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
            	foreach($marrphotoID as $index => $photoID){
            			 $sql = "UPDATE tblGrouptoPhoto SET position = '$index' WHERE photoID =  '$photoID' ";
		 				$this->mRs->Execute($sql);
            	}
            }  
         
         
        /**
         * Purpose: Returns an array of photos for this fun group.
         * Rows Limit is enabled for this function.
         */
            function getPhotos($rowslimit = NULL){
                $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
                if (0 == count($this->mPhotos)){
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    //$sql = "SELECT photoID from tblGrouptoPhoto WHERE groupID = '$this->mGroupID' ORDER BY position DESC " . $limitstr;
                    
                     $sql = "SELECT tblPhoto.* from tblGrouptoPhoto, tblPhoto " .
                    		"WHERE groupID = '$this->mGroupID' " .
                    		"AND tblPhoto.photoID = tblGrouptoPhoto.photoID " .
                    		"ORDER BY position DESC " . $limitstr;
                    
                    
                    $this->mRs->Execute($sql);
                    
                    if (0 == $this->mRs->Recordcount()){
                        return NULL;            
                    }
                    else {
                        while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                            try {
                                $photo = new Photo($this, 0, $recordset);
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                            $this->mPhotos[] = $photo;
                        }
                    }               
                }
                
                return $this->mPhotos;
            }
            
            
             // get random photo from the traveler profile
	 		function getRandomPhoto(){
	 			
	 			  $conn = ConnectionProvider::instance()->getConnection();
				  $rs = new Recordset($conn);
	 					
	 			if ($this->randphoto == NULL){
	 				
	 				// get the number of photos to choose from
	 				$sql = "SELECT count(photoID) as totalrec from tblGrouptoPhoto WHERE groupID = '$this->mGroupID' ";
	 				$rs->Execute($sql);
	 				
	 				if ($rs->Result(0,"totalrec") == 0){
	 					try {
	 						$this->randphoto = new Photo($this);
	 					}
						catch (Exception $e) {						  
						   throw $e;
						}
	 				}	 				
	 				else {
	 					
	 					$max = $rs->Result(0,"totalrec") - 1;
						
						$randidx = mt_rand(0 , $max );	 		// the index starting from top row
						
	 					$sql = "SELECT photoID FROM tblGrouptoPhoto WHERE groupID = '$this->mGroupID' ORDER BY photoID LIMIT $randidx , 1"  ;
	 					$rs->Execute($sql);
	 					
	 					while ($recordset = mysql_fetch_array($rs->Resultset())) {
			 				try {	
			 					$this->randphoto = new Photo($this, $recordset['photoID'] );
			 				}
							catch (Exception $e) {						  
							   throw $e;
							}
	 					}
	 				}
	 			}
	 			
	 			return $this->randphoto;
	 		}
               
            /**
	 		 * Added by Joel 
	 		 * March 1 2007
	 		 * To check If Resume has primary photo
	 		 * @return Boolean 
	 		 */
             function checkHasprimaryphoto(){
	 			
	 			$conn = ConnectionProvider::instance()->getConnection();
				$rs   = new Recordset($conn);
	 			
	 			$sql = "SELECT primaryphoto from tblPhoto, tblGrouptoPhoto " .
	 					"WHERE tblGrouptoPhoto.photoID = tblPhoto.photoID " .
	 					"AND primaryphoto =1 " .
	 					"AND tblGrouptoPhoto.groupID ='".$this->mGroupID."'" ;
				$rs->Execute($sql);
				
				
				if($rs->recordcount() == 0){
					return false;	
				}else{
					return true;
				}
            }

		
             
    }
    
?>