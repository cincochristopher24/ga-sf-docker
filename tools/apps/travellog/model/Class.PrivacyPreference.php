<?php

/*
 * Created on 09 27, 2006
 * @author Kerwin Gordo
 * Purpose: holds the privacy preference of a traveler 
 */

require_once ('Class.Connection.php');
require_once ('Class.Recordset.php');
require_once ('gaexception/Class.InstantiationException.php');
require_once ('travellog/model/Class.PrivacyPreferenceType.php');
require_once ('travellog/model/Class.MailNotificationPreferenceType.php');
require_once ('Cache/ganetCacheProvider.php');


class PrivacyPreference {
	
	/**********************************************************************************************
	 * edits of neri: 
	 * 		added static variables RECEIVE and DONT_RECEIVE								01-08-09
	 * 		added member variable mUpdates and set it to value of RECEIVE as default	01-08-09
	 * 		added setModeOfReceivingUpdates and getModeOfReceivingUpdates functions		01-08-09
	 * 		added static variables NOTIFIED_BY_CO_PARTICIPANTS, 
	 * 			NOT_NOTIFIED_BY_CO_PARTICIPANTS, NOTIFIED_BY_ALL_ACTIVITIES,
	 * 			NOT_NOTIFIED_BY_ALL_ACTIVITIES											01-26-09
	 * 		added member variables mNotificationFromCoParticipants and 
	 * 			mNotificationFromActivities and setting them both to notified			01-26-09
	 * 		added setter and getter functions for receiving notifications
	 * 			from group discussions													01-26-09
	 * 		modified query in update()													01-26-09  
	 **********************************************************************************************/
	
	public static $SHOW_EVERYONE = 1;
	public static $FRIENDS = 2;
	public static $GROUPMATES = 3;
	public static $FRIENDS_AND_GROUPMATES = 4;
	public static $NO_ONE = 0;
	public static $RECEIVE = 5;
	public static $DONT_RECEIVE = 6;
	public static $NOTIFIED_BY_CO_PARTICIPANTS = 7;
	public static $NOT_NOTIFIED_BY_CO_PARTICIPANTS = 8;
	public static $NOTIFIED_BY_ALL_ACTIVITIES = 9;
	public static $NOT_NOTIFIED_BY_ALL_ACTIVITIES = 10;
	
	// notifications to entries in address book 
	//public static $NOTIFY_ON_NEW_JOURNALENTRY = 5;
	//public static $NOTIFY_ON_EDIT_JOURNALENTRY = 6;
	//public static $NOTIFY_ON_NEW_PHOTO = 7;

	private $mCon = NULL;
	private $mRs = NULL;
	private $mTravelerID = 0;
	private $mShowNames; // show names preference
	private $mShowEmail; // show email preference
	private $mShowGroups; // show groups  preference
	private $mShowFriends; // show friends preference
	private $mShowOnline; // show online status preference
	private $mShowCalendar; // show calendar preference
	private $mShowResume;	// show resume preference
	private $mNotifyOnNewJE; // notify entries in address book if there is a new journal entry (boolean)
	private $mNotifyOnEditJE; //  notify entries in address book if there is an edit on journal entry (boolean)
	private $mNotifyOnNewPhoto; //notify entries in address book if there is an new photo  (boolean)
	private $mUpdates; // updates from goAbroad network
	private $mNotificationFromCoParticipants; // notification from co-participants in same discussion thread
	private $mNotificationFromActivities; // notification from subgroup's discussion activities

	private $mTraveler;
	private $cache;

	function PrivacyPreference($travelerID = 0) {
		$this->mShowNames = PrivacyPreference::$NO_ONE;
		$this->mShowEmail = PrivacyPreference::$NO_ONE;
		$this->mShowGroups = PrivacyPreference::$SHOW_EVERYONE;		
		$this->mShowFriends = PrivacyPreference::$SHOW_EVERYONE;
		$this->mShowOnline = PrivacyPreference::$SHOW_EVERYONE;
		$this->mShowCalendar = PrivacyPreference::$SHOW_EVERYONE;
		$this->mShowResume = PrivacyPreference::$NO_ONE;
		$this->mNotifyOnNewJE = true;
		$this->mNotifyOnEditJE = true;
		$this->mNotifyOnNewPhoto = true;
		$this->mUpdates = PrivacyPreference::$RECEIVE;
		$this->mNotificationFromCoParticipants = PrivacyPreference::$NOTIFIED_BY_CO_PARTICIPANTS;
		$this->mNotificationFromActivities = PrivacyPreference::$NOTIFIED_BY_ALL_ACTIVITIES;
		
		$this->mCon = new Connection();
		$this->mRs = new Recordset($this->mCon);

		if (0 < $travelerID) {
			$this->mTravelerID = $travelerID;
			
			// cache check code
			$this->cache	=	ganetCacheProvider::instance()->getCache();
			$found = false;
			if ($this->cache != null){
				
			}
			
			

			$qry = "select * from tblPrivacyPreference where travelerID='$travelerID'";

			$this->mRs->Execute($qry);

			if ($this->mRs->Recordcount() > 0) {
				while ($rsar = mysql_fetch_array($this->mRs->Resultset())) {
					switch ($rsar['preferencetype']) {
						case PrivacyPreferenceType::$SHOW_NAMES :
							$this->mShowNames = $rsar['preference'];
							break;
						case PrivacyPreferenceType::$SHOW_EMAIL :
							$this->mShowEmail = $rsar['preference'];
							break;	
						case PrivacyPreferenceType::$SHOW_GROUPS :
							$this->mShowGroups = $rsar['preference'];
							break;
						case PrivacyPreferenceType::$SHOW_FRIENDS :
							$this->mShowFriends = $rsar['preference'];
							break;							
						case PrivacyPreferenceType :: $SHOW_ONLINE_STATUS :
							$this->mShowOnline = $rsar['preference'];
							break;
						case PrivacyPreferenceType:: $SHOW_CALENDAR :
							$this->mShowCalendar = $rsar['preference'];
							break;
						case PrivacyPreferenceType:: $SHOW_RESUME :
							$this->mShowResume = $rsar['preference'];
							break;	
						case PrivacyPreferenceType::$RECEIVE_UPDATES :
							$this->mUpdates = $rsar['preference'];
							break;
						case PrivacyPreferenceType::$NOTIFICATION_FROM_COPARTICIPANTS_OF_DISCUSSION :
							$this->mNotificationFromCoParticipants = $rsar['preference'];
							break;
						case PrivacyPreferenceType::$NOTIFICATION_FROM_SUBGROUP_DISCUSSION_ACTIVITIES :
							$this->mNotificationFromActivities = $rsar['preference'];
							break;
					}
	
				}
			}	
			
			$qry = "select * from tblMailNotificationPreference where travelerID='$travelerID'";
			
			$this->mRs->Execute($qry);
			
			if ($this->mRs->Recordcount() > 0) {
				while ($rsar = mysql_fetch_array($this->mRs->Resultset())) {
					switch ($rsar['mailnotificationtype']) {
						case MailNotificationPreferenceType::$NOTIFY_ON_NEW_JOURNALENTRY :
							$this->mNotifyOnNewJE = $rsar['preference'];
							break;
						case MailNotificationPreferenceType::$NOTIFY_ON_EDIT_JOURNALENTRY :
							$this->mNotifyOnEditJE = $rsar['preference'];
							break;	
						case MailNotificationPreferenceType::$NOTIFY_ON_NEW_PHOTO :
							$this->mNotifyOnNewPhoto = $rsar['preference'];
							break;						
					}
	
				}
			}
			
		}
	}

	/**
	 * returns true if the travelerID param is permitted to view the names
	 * of this traveler (this includes the email address)
	 */
	function canViewNames($travelerID) {
		return $this->privacyRule($this->mShowNames, $travelerID);
	}
	
	function canViewEmail($travelerID) {
		return $this->privacyRule($this->mShowEmail,$travelerID);
	}
	
	function canViewGroups($travelerID) {
		return $this->privacyRule($this->mShowGroups, $travelerID);
	}
	
	function canViewFriends($travelerID) {
		return $this->privacyRule($this->mShowFriends, $travelerID);
	}

	function canViewOnlineStatus($travelerID) {
		return $this->privacyRule($this->mShowOnline, $travelerID);
	}
	
	function canViewCalendar($travelerID) {
		return $this->privacyRule($this->mShowCalendar,$travelerID);
	}
	
	function canViewResume($travelerID) {
		return $this->privacyRule($this->mShowResume,$travelerID);
	}

	/*************
	 * getter/setter
	 */

	function setTravelerID($travelerID) {
		$this->mTravelerID = $travelerID;
	}

	function getTravelerID() {
		return $this->mTravelerID;
	}

	/**
	 * returns preference on showing names and email
	 */
	function showNamesTo() {
		return $this->mShowNames;
	}

	function setShowNamesTo($preference) {
		$this->mShowNames = $preference;
	}
	
	function showEmailTo() {
		return $this->mShowEmail;
	}

	function setShowEmailTo($preference) {
		$this->mShowEmail = $preference;
	}
	

	function showGroupsTo() {
		return $this->mShowGroups;
	}

	function setShowGroupsTo($preference) {
		$this->mShowGroups = $preference;
	}
	
	function showFriendsTo() {
		return $this->mShowFriends;
	}

	function setShowFriendsTo($preference) {
		$this->mShowFriends = $preference;
	}
			

	function showOnlineTo() {
		return $this->mShowOnline;
	}

	function setShowOnlineTo($preference) {
		$this->mShowOnline = $preference;
	}
	
	function showCalendarTo() {
		return $this->mShowCalendar;
	}
	
	function setShowCalendarTo($preference){
		$this->mShowCalendar = $preference;
	}
	
	function showResumeTo() {
		return $this->mShowResume;
	}
	
	function setShowResumeTo($preference){
		$this->mShowResume = $preference;
	}
	
	/**
	 * get/set MailNotification preferences
	 */
	 
	 function setNotifyOnNewJE($preference){
	 	$this->mNotifyOnNewJE = $preference;
	 }
	 
	 function getNotifyOnNewJE() {
	 	return $this->mNotifyOnNewJE;
	 }
	 
	 function setNotifyOnEditJE($preference){
	 	$this->mNotifyOnEditJE = $preference;
	 }
	 
	 function getNotifyOnEditJE() {
	 	return $this->mNotifyOnEditJE;
	 }
	 
	 function setNotifyOnNewPhoto($preference){
	 	$this->mNotifyOnNewPhoto = $preference;
	 }
	 
	 function getNotifyOnNewPhoto() {
	 	return $this->mNotifyOnNewPhoto;
	 }
	 
	 function setModeOfReceivingUpdates($preference){
	 	$this->mUpdates = $preference;
	 }
	 
	 function getModeOfReceivingUpdates(){
	 	return $this->mUpdates;
	 }
	 
	 function setModeOfReceivingNotificationFromCoParticipants($preference){
	 	$this->mNotificationFromCoParticipants = $preference;
	 }
	 
	 function getModeOfReceivingNotificationFromCoParticipants(){
	 	return $this->mNotificationFromCoParticipants;
	 }
	
	 function setModeOfReceivingNotificationFromSubgroupDiscussionActivities($preference){
	 	$this->mNotificationFromActivities = $preference;
	 }
	 
	 function getModeOfReceivingNotificationFromSubgroupDiscussionActivities(){
	 	return $this->mNotificationFromActivities;
	 }

	/***
	 * update
	 */

	function update() {
		if (0 < $this->mTravelerID) {
			// delete first all the privacy preference entry
			$qry = "DELETE FROM tblPrivacyPreference WHERE travelerID='$this->mTravelerID'";

			$this->mRs->Execute($qry);

			$shownames = PrivacyPreferenceType::$SHOW_NAMES;
			$showemail = PrivacyPreferenceType::$SHOW_EMAIL;
			$showgroups = PrivacyPreferenceType::$SHOW_GROUPS;
			$showfriends = PrivacyPreferenceType::$SHOW_FRIENDS;			
			$showonlinestatus = PrivacyPreferenceType::$SHOW_ONLINE_STATUS;
			$showcalendar = PrivacyPreferenceType::$SHOW_CALENDAR;
			$showresume = PrivacyPreferenceType::$SHOW_RESUME;
			$receiveupdates = PrivacyPreferenceType::$RECEIVE_UPDATES;
			$notificationfromcoparticipants = PrivacyPreferenceType::$NOTIFICATION_FROM_COPARTICIPANTS_OF_DISCUSSION;
			$notificationfromsubgroupactivities = PrivacyPreferenceType::$NOTIFICATION_FROM_SUBGROUP_DISCUSSION_ACTIVITIES;
						
			// then insert the new preferences
			$qry = "INSERT INTO tblPrivacyPreference (travelerID,preferenceType,preference) VALUES ('$this->mTravelerID',$showonlinestatus,'$this->mShowOnline')," .
			"('$this->mTravelerID',$showgroups,'$this->mShowGroups'),('$this->mTravelerID',$shownames,'$this->mShowNames'),('$this->mTravelerID',$showemail,'$this->mShowEmail')," .
			"('$this->mTravelerID',$showfriends,'$this->mShowFriends'), ('$this->mTravelerID',$showcalendar,'$this->mShowCalendar' )," .
			"('$this->mTravelerID',$showresume,'$this->mShowResume'), ('$this->mTravelerID',$receiveupdates,'$this->mUpdates')," .
			"('$this->mTravelerID',$notificationfromcoparticipants,'$this->mNotificationFromCoParticipants'), ('$this->mTravelerID',$notificationfromsubgroupactivities,'$this->mNotificationFromActivities')" ;
			$this->mRs->Execute($qry);
			
			
			// delete all mailnotification preferences
			$qry = "DELETE FROM tblMailNotificationPreference WHERE travelerID='$this->mTravelerID'";

			$this->mRs->Execute($qry);
			
			$notify_on_new_je = MailNotificationPreferenceType::$NOTIFY_ON_NEW_JOURNALENTRY;
			$notify_on_edit_je = MailNotificationPreferenceType::$NOTIFY_ON_EDIT_JOURNALENTRY;
			$notify_on_new_photo = MailNotificationPreferenceType::$NOTIFY_ON_NEW_PHOTO;
			
			// insert new mail notification preferences
			$qry = "INSERT INTO tblMailNotificationPreference (travelerID,mailnotificationtype,preference) VALUES ('$this->mTravelerID',$notify_on_new_je,'$this->mNotifyOnNewJE')," .
				"('$this->mTravelerID',$notify_on_edit_je,'$this->mNotifyOnEditJE'),('$this->mTravelerID',$notify_on_new_photo,'$this->mNotifyOnNewPhoto')";
				
			$this->mRs->Execute($qry);	 			
			

		}
	}

	/*********************************************
	 * private functions
	 * 
	 */

	private function isFriend($travelerID) {
		if (NULL == $this->mTraveler)
			$this->mTraveler = new Traveler($this->mTravelerID);
		$friends = $this->mTraveler->getFriends();
		if (NULL != $friends) {
			if (array_key_exists($travelerID, $friends))
				return true;
			else
				return false;
		}
		return false;
	}

	private function isGroupmate($viewingUser) {
		if (NULL == $this->mTraveler)
			$this->mTraveler = new Traveler($this->mTravelerID);
		$userGroups = $this->mTraveler->getGroups();
		$viewingGroups = $viewingUser->getGroups();
		if (NULL != $viewingGroups && NULL != $userGroups) {
			foreach ($userGroups as $userGroup) {
				foreach ($viewingGroups as $viewingGroup) {
					if ($viewingGroup->getGroupID() == $userGroup->getGroupID())
						return true;
	
				}
			}
		}	
		return false;
	}

	/**
	 * common function to check for viewing access passed are values of viewing preference
	 *  for diff preference types (mShowNames,mShowOnline,mShowGroupAndFriends)
	 */
	private function privacyRule($preference, $travelerID) {
		switch ($preference) {
			case PrivacyPreference :: $SHOW_EVERYONE :
				return true;
			case PrivacyPreference :: $NO_ONE :
				return false;
			case PrivacyPreference :: $FRIENDS :
				if (0 == $travelerID)
					return false;
				return $this->isFriend($travelerID);
			case PrivacyPreference :: $GROUPMATES :
				if (0 == $travelerID)
					return false;
				$viewingUser = new Traveler($travelerID);
				return $this->isGroupmate($viewingUser);
			case PrivacyPreference:: $FRIENDS_AND_GROUPMATES:
				if (0 == $travelerID)
					return false;
				$viewingUser = new Traveler($travelerID);
				return ($this->isGroupmate($viewingUser) || $this->isFriend($travelerID) );	
		}
	}

}
?>
