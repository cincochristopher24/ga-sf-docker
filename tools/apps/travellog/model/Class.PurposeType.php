<?
	
/**
 * Created on Nov 23, 2006
 * Created by: Joel C. LLano
 */
	
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	

	class PurposeType{
		
		private $mConn         = NULL;
        private $mRs           = NULL;
        
		private $tpurposetypeid 		= 0;
		private $tptype					= NULL;
		
		function PurposeType($tpurposetypeid = 0){
 				
 				$this->tpurposetypeid = $tpurposetypeid;
 				
 				try {
	 				$this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}

			 			if ($tpurposetypeid > 0){
			 				
					 				$sql = "SELECT * from tblPurposeType where tpurposetypeid = '$tpurposetypeid' ";
						 			$this->mRs->Execute($sql);
					 			
					 				if ($this->mRs->Recordcount() == 0){
					 					throw new Exception("Invalid PurposeType ID");	// ID not valid so throw exception
					 				}
					 				
					 				// set values to its attributes
									$this->tptype 		= stripslashes($this->mRs->Result(0,"tptype")); 
				 		}
						
						
 			}
		
		public static function getAllPurposeType(){
				try {
	 				$conn = new Connection();
					$rs   = new Recordset($conn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT * from tblPurposeType  order by tpurposetypeid ASC ";
				$rs->Execute($sql);
				
				$travelerpurposetype_s = array();
							
				while ($rsar = mysql_fetch_array($rs->Resultset())) {
					$travelerpurposetype_s[] = new 	PurposeType($rsar['tpurposetypeID']);
				}
				return $travelerpurposetype_s;
		}
		
		
		/**
		 * Use 	__call function to dynamically call getter and setters
		 */
		
		function __call($method, $arguments) {
				//prefix ->get and set <- to lowercase
		        $prefix = strtolower(substr($method, 0, 3));
		        $property = strtolower(substr($method, 3));
		
		        if (empty($prefix) || empty($property)) {
		            return;
		        }
		
		        if ($prefix == "get" && isset($this->$property)) {
		            return $this->$property;
		        }
		
		        if ($prefix == "set") {
		            $this->$property = $arguments[0];
		        }
    	}
		
		/**
         * CRUD Methods (Create, Update , Delete)
         */ 
        function Create(){
            
            $Aname  = addslashes($this->tptype);
            
            $sql = "INSERT into tblPurposeType (tptype) VALUES ('$Aname')";
            $this->mRs->Execute($sql);       
            $this->tpurposetypeid = $this->mRs->GetCurrentID();
        }
        
        function Update(){
            
            $Aname = addslashes($this->tptype);
            
            $sql = "UPDATE tblPurposeType SET tptype = '$Aname' WHERE tpurposetypeID = '$this->tpurposetypeid' ";
            $this->mRs->Execute($sql);
        }
        
        function Delete(){
            
            $sql = "DELETE FROM tblPurposeType WHERE tpurposetypeid = '$this->tpurposetypeid' ";        
            $this->mRs->Execute($sql);       
        }       
		
		
	}



?>