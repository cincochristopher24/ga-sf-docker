<?php
	require_once 'travellog/model/Class.FunGroup.php';
	require_once 'travellog/model/Class.AdminGroup.php';

	class MessageSendableFactory{
		private $mRs	= NULL;
		private $mRs2	= NULL;
		private $mConn	= NULL;

		private static $mInstance;

		const TRAVELER	= 1;
		const GROUP		= 2;
		const STAFF		= 3;

		private function MessageSendableFactory(){							
			try{ 				
				$this->mConn = new Connection();
				$this->mRs   = new Recordset($this->mConn);
				$this->mRs2	 = new Recordset($this->mConn);
			}
			catch (exception $e){				   
			   throw $e;
			}
		}

		/**
		 * Function name: getInstance
		 * @static
		 * @return SendableFactory
		 * Creates a single instance of this class
		 */

		static function Instance(){		
			if (self::$mInstance == NULL)
				self::$mInstance = new MessageSendableFactory();

			return self::$mInstance;
		}

		/**
		 * Function name: Create
		 * @param integer $_sendableID
		 * @return Traveler|Group|NULL|exception
	 	 * Creates an instance of sendable based on sendable id
	 	 */
	 	 
		function Create($_sendableId = 0){

			if (0 < $_sendableId){
				$sql = "SELECT DISTINCT sendableType " .
						"FROM tblSendable " .
						"WHERE sendableID = " . $_sendableId;
				$this->mRs->Execute($sql);

				if (0 < $this->mRs->Recordcount()){
					$sendableType = $this->mRs->Result(0, "sendabletype"); 		

		 			switch ($sendableType){
			 			case self::TRAVELER:
							$sql2 = "SELECT travelerID, username " .
									"FROM tblTraveler " .
									"WHERE sendableID = " . $_sendableId;
			 				$this->mRs2-> Execute($sql2);

			 				if (0 == $this->mRs2->Recordcount()) {
					 			// id not valid so throw exception
					 			throw new exception("Sendable ID: " . $_sendableId . " is not valid!");	
				 			}		

					 		$travelerData = array();
							$travelerData["sendableID"] = $_sendableId;
							$travelerData["username"] = $this->mRs2->Result(0,"username");
							$travelerData["travelerID"] = $this->mRs2->Result(0,"travelerID");

							$mSendable = new Traveler();
							$mSendable->setTravelerData($travelerData);

			 				break;

			 			case self::GROUP :
			 				$sql2 = "SELECT discriminator, groupID, name, administrator " .
			 						"FROM tblGroup " .
									"WHERE sendableID = " . $_sendableId;
							$this->mRs2->Execute($sql2);

							$mDiscriminator = $this->mRs2->Result(0, "discriminator");

							switch($mDiscriminator){
								case GROUP::FUN :
									$groupData = array();
									$groupData["sendableID"] = $_sendableId;
									$groupData["name"] = $this->mRs2->Result(0, "name");
									$groupData["groupID"] = $this->mRs2->Result(0, "groupID");
									$groupData["discriminator"] = $this->mRs2->Result(0, "discriminator");
									$groupData["administrator"] = $this->mRs2->Result(0, "administrator");

									$mSendable = new FunGroup();
									$mSendable->setFunGroupData($groupData);

					 				break;

					 			case GROUP::ADMIN :
									$groupData = array();
									$groupData["sendableID"] = $_sendableId;
									$groupData["name"] = $this->mRs2->Result(0, "name");
									$groupData["groupID"] = $this->mRs2->Result(0, "groupID");
									$groupData["discriminator"] = $this->mRs2->Result(0, "discriminator");
									$groupData["administrator"] = $this->mRs2->Result(0, "administrator");

									$mSendable = new AdminGroup();
									$mSendable->setAdminGroupData($groupData);

					 				break;
							}
			 				break;

			 			case self::STAFF :
							require_once 'travellog/model/Class.Staff.php';

			 				$sql2 = "SELECT travelerID, groupID " .
									"FROM tblGrouptoFacilitator " .
									"WHERE type = 1 " .
									"AND sendableID = " . $_sendableId;
			 				$this->mRs2-> Execute($sql2);

							if (0 == $this->mRs2->Recordcount()) {			 			
					 			// id not valid so throw exception
					 			throw new exception("Invalid sendable id");	
				 			}
			 				try{
								$mSendable = new Staff($this->mRs2->Result(0,'travelerID'), $this->mRs2->Result(0,'groupID'));
							}
							catch (exception $e){
								throw $e;
							}
			 				break;							
					}
				}
				return $mSendable;
			} 			
		}

		static function getTravelerId($_sendableId = 0){
			try{ 				
				$mConn = new Connection();
				$mRs   = new Recordset($mConn);
			}
			catch (exception $e){
			   throw $e;
			}

			$sql = "SELECT travelerID " .
					"FROM tblTraveler " .
					"WHERE sendableID = " . $_sendableId;
			$mRs->Execute($sql);

			if (0 == $mRs->Recordcount())
				throw new exception ("Sendable ID: " . $_sendableId . " does not exist!");

			return $mRs->Result(0, "travelerID");
		}
		static function getGroupId($_sendableId = 0){
			try{ 				
				$mConn = new Connection();
				$mRs   = new Recordset($mConn);
			}
			catch (exception $e){
			   throw $e;
			}

			$sql = "SELECT groupID " .
					"FROM tblGroup " .
					"WHERE sendableID = " . $_sendableId;
			$mRs->Execute($sql);

			if (0 == $mRs->Recordcount())
				throw new exception ("Sendable ID: " . $_sendableId . " does not exist!");

			return $mRs->Result(0, "groupID");
		}
		static function getSendableType($_sendableId = 0){
			try{ 				
				$mConn = new Connection();
				$mRs   = new Recordset($mConn);
			}
			catch (exception $e){
			   throw $e;
			}

			$mSendableType = '';

			if (0 < $_sendableId){
				$sql = "SELECT DISTINCT sendableType " .
						"FROM tblSendable " .
						"WHERE sendableID = " . $_sendableId;
				$mRs->Execute($sql);

				if (0 < $mRs->Recordcount()){
					$sendableType = $mRs->Result(0, "sendabletype"); 		

		 			switch ($sendableType){
			 			case self::TRAVELER:
							$mSendableType = 'TRAVELER';
			 				break;

			 			case self::GROUP :
			 				$mSendableType = 'GROUP';
			 				break;

			 			case self::STAFF :
			 				$mSendableType = 'GROUP';
			 				break;
					}
				}
			}

 			return $mSendableType;
		}
	}
?>