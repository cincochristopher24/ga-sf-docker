<?php
class EntryModel{
	function Get( $travellogID ){
		require_once("travellog/model/Class.TravelLog.php");
		require_once("travellog/vo/Class.EntryVO.php");
		$obj_entry_vo = new EntryVO;
		 try{
			$obj_entry    = new TravelLog( $travellogID ); 
			
			$obj_entry_vo->setTravelLogID   ( $obj_entry->getTravelLogID()                                       );
			$obj_entry_vo->setDescription   ( $obj_entry->getDescription()                                       );
			$obj_entry_vo->setTitle         ( $obj_entry->getTitle()                                             );
			$obj_entry_vo->setPublish       ( $obj_entry->getPublish()                                           );
			$obj_entry_vo->setCallout       ( $obj_entry->getCallOut()                                           );
			$obj_entry_vo->setTravelID      ( $obj_entry->getTravelID()                                          );
			$obj_entry_vo->setUrlAlias      ( $obj_entry->getUrlAlias()                                          );
			
			try{
				$location = $obj_entry->getTrip()->getLocation();
				if(is_object($location)){
					$obj_entry_vo->setCountry       ( $location->getCountry()->getName()      );
					$obj_entry_vo->setCountryID     ( $location->getCountry()->getCountryID() );
					$obj_entry_vo->setCity          ( $location->getName()                    );
				}
			}catch(Exception $_e){}
			$obj_entry_vo->setArrival       ( $obj_entry->getTrip()->getArrival()                                );
			$obj_entry_vo->setUsername      ( $obj_entry->getTraveler()->getUsername()                           );
			$obj_entry_vo->setTemplateType  ( $obj_entry->getTemplateType()                                      );
			$obj_entry_vo->setIsAdvisor     ( $obj_entry->getTraveler()->isAdvisor()                             );
			$obj_entry_vo->setPrimaryPhotoID( $obj_entry->getPrimaryPhoto()->getPhotoID()                        );
			$obj_entry_vo->setPrimaryPhoto  ( $obj_entry->getPrimaryPhoto()->getPhotoLink('jentryheader')        );
			$obj_entry_vo->setCountPhotos   ( $obj_entry->countPhotos()                                          );
			
			// added by ianne sets the primary photo obj
			$obj_entry_vo->setPrimaryPhotoObj      ( $obj_entry->getPrimaryPhoto()       						 );
			
			/**********************************************
			 * added by neri for GANET video functionality
			 * 10-14-08
			 **********************************************/
			 
			$obj_entry_vo->setPrimaryVideo	( $obj_entry->getPrimaryVideo()   									 );			
			$obj_entry_vo->setVideos		( $obj_entry->getTravelLogVideos()									 ); 
			$obj_entry_vo->setCountVideos	( count($obj_entry->getTravelLogVideos())							 );
			$obj_entry_vo->setOwner			( $obj_entry->getOwner()											 );
			
			/**********************************************/
			
			/*if( $obj_entry->getTraveler()->isAdvisor() ){ 
				$obj_group = $obj_entry->getOwner();
				if( $obj_group->getParentID() ){
					$obj_entry_vo->setSubGroupName( $obj_group->getName()              );		
					$obj_entry_vo->setGroupName   ( $obj_group->getParent()->getName() );
				}else
					$obj_entry_vo->setGroupName   ( $obj_group->getName()              );				
			}*/
			
		}catch( Exception $e){} 
		return $obj_entry_vo;
		 
	}
	   
	function permalink( $arr_filter ){
		require_once("travellog/model/Class.TravelLog.php");
		require_once("Class.Criteria2.php");
		// $c         = new Criteria2;
		// $obj_entry = new TravelLog;
		// $c->mustBeEqualInString( 'title'    , $arr_filter['entry_title']);
		// $c->mustBeEqual        ( 'travelID' , $arr_filter['travelID']);
		//         
		// return $obj_entry->permalink( $c );
		
		$obj_entry = new TravelLog;
		
		$aliasCriteria = new Criteria2;
		$aliasCriteria->mustBeEqualInString('urlAlias', $arr_filter['urlAlias']);
		$aliasCriteria->mustBeEqual('travelID', $arr_filter['travelID']);

		$titleCriteria = new Criteria2;
		$titleCriteria->mustBeEqualInString('title', $arr_filter['entry_title']);
		$titleCriteria->mustBeEqual('travelID', $arr_filter['travelID']);
		
		return ($travellogID = $obj_entry->permalink($aliasCriteria)) 
			? $travellogID : $obj_entry->permalink($titleCriteria);
	}
}
?>
