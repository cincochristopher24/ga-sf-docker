<?php

	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");

	class ResourceFileBatchUpload{
		
		private $mID = 0;
		private $mCustomMessage = "";
		private $mNotifyMembers = 1;
		
		private $conn = NULL;
		private $rs = NULL;
		
		public function __construct($param=NULL){
			try{
				$this->conn = new Connection();
                $this->rs   = new Recordset($this->conn);
			}catch(exception $e){
				throw $e;
			}
			$data = $param;
			if( is_numeric($data) && 0 < intval($data) ){
				$id = intval($data);
				$sql = "SELECT * FROM tblResourceFileBatchUpload WHERE ID = {$id}";
				$this->rs->Execute($sql);
				
				if( !$this->rs->Recordcount() ){
					throw new exception("Record not found for tblResourceFileBatchUpload.ID = $id!");
				}
				$data = mysql_fetch_array($this->rs->Resultset());
			}
			if( is_array($data) && isset($data["ID"]) && isset($data["customMessage"]) && isset($data["notifyMembers"]) ){
				if( !is_numeric($data["ID"]) || 0 >= intval($data["ID"]) ){
					throw new exception("ID not valid!");
				}
				if( !in_array($data["notifyMembers"],array("1","0",1,0)) ){
					throw new exception("NotifyMembers should only be 1 or 0!");
				}
				$this->mID = $data["ID"];
				$this->mCustomMessage = stripslashes($data["customMessage"]);
				$this->mNotifyMembers = intval($data["notifyMembers"]);
			}
		}
		
		public function Save(){
			if( 0 < $this->mID ){
				$this->Update();
			}else{
				$this->Create();
			}
		}
		
		private function Create(){
			$message = "'".addslashes($this->mCustomMessage)."'";
			$notify = $this->mNotifyMembers;
			$sql = "INSERT INTO tblResourceFileBatchUpload(customMessage,notifyMembers) VALUES($message,$notify)";
			$this->rs->Execute($sql);
			
			$this->mID = $this->rs->GetCurrentID();
		}
		
		private function Update(){
			$id = $this->mID;
			$message = "'".addslashes($this->mCustomMessage)."'";
			$sql = "UPDATE tblResourecFileBatchUpload
					SET customMessage = $message
					WHERE ID = $this->mID";
			$this->rs->Execute($sql);
		}
		
		public function Delete(){
			$id = $this->mID;
			$sql = "DELETE FROM tblResourceFileBatchUpload WHERE ID = {$id}";
			$this->rs-Execute($sql);
		}
		
		public function setCustomMessage($message=""){
			$this->mCustomMessage = $message;
		}
		
		public function setNotifyMembers($notify=1){
			if( !in_array($notify,array("1","0",0,1)) ){
				$notify = 1;
			}else{
				$notify = intval($notify);
			}
			$this->mNotifyMembers = $notify;
		}
		
		public function getID(){
			return $this->mID;
		}
		
		public function getCustomMessage(){
			return $this->mCustomMessage;
		}
		
		public function getNotifyMembers(){
			return $this->mNotifyMembers;
		}
	}