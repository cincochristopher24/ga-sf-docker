<?php
/**
 * Created on Mar 22, 2007
 * @author daf
 * 
 * 
 */
 
 	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
 	require_once('travellog/model/Class.Traveler.php');
 	
 	class OnlineSurvey {
 	
		/**
		 * Define variables for Class Member Attributes
		 */
		     
			 private $mOnlineSurveyID  	    = 0;
			 private $mTravelerID      		= 0;
			 private $mOnlineFormID      	= 0;
			 
		     private $mParticipants			= array();
		     
		     public static $arrParticipantID = array();
		     
		     
		/**
		 * Constructor Function for this class
		 */
		           
		    function OnlineSurvey($onlineSurveyID = 0){ 
		    	
		    	$this->mOnlineSurveyID = $onlineSurveyID;
		    	
		    	
		    	try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mOnlineSurveyID){
                    
                    $sql = "SELECT * FROM tblOnlineSurvey WHERE onlineSurveyID = $this->mOnlineSurveyID ";
                    $this->mRs->Execute($sql);
                	
                    if ($this->mRs->Recordcount()){
                        
                        // Sets values to its class member attributes.
	                    $this->mTravelerID         = $this->mRs->Result(0,"travelerID");   
	                    $this->mOnlineFormID       = $this->mRs->Result(0,"onlineFormID");
	                        
                    }
                }
		    	
		    }
		    
		/**
         * Setter Functions
         */    
            function setTravelerID($_travelerID){
                $this->mTravelerID = $_travelerID;
            }
            
            function setOnlineFormID($_onlineFormID){
                $this->mOnlineFormID = $_onlineFormID;
            }
            
           
		/**
         * Getter Functions
         */    
            function getOnlineSurveyID(){
                return $this->mOnlineSurveyID ;
            }
            
            function getTravelerID(){
                return $this->mTravelerID ;
            }
            
            function getOnlineFormID(){
                return $this->mOnlineFormID ;
            }
            
            
    		
    		function Save(){
    			    			
    			if (0 == $this->mOnlineSurveyID) {
    				
    				$sql = "INSERT into tblOnlineSurvey (travelerID, onlineformID) " .
                        "VALUES ($this->mTravelerID, $this->mOnlineFormID)";                        
	                $this->mRs->Execute($sql);       
	                $this->mOnlineSurveyID = $this->mRs->GetCurrentID();
    				
    			}
    			
    			else {
    				
    				$sql = "UPDATE tblOnlineSurvey SET " .
    						"travelerID = $this->mTravelerID , onlineformID = $this->mOnlineFormID " .
    						"WHERE onlineSurveyID = $this->mOnlineSurveyID" ;
	                $this->mRs->Execute($sql);       
    				
    			}
    		}
    		
    		
    		function Delete(){
               
                $sql = "DELETE FROM tblOnlineSurvey WHERE onlineSurveyID = '$this->mOnlineSurveyID' ";     
                $this->mRs->Execute($sql);
            } 
     	
     	
     	  /**
           * Purpose: Returns an array of travelers who are members of a group who participated in this survey.
           */
            public static function getParticipants($_onlineformID= 0){
                
                try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$arrParticipants = array();
				
                if (0 == count($arrParticipants)){
                    
                    $sql = "SELECT tblOnlineSurvey.travelerID " .
							"FROM tblOnlineSurvey, tblOnlineSurveyAnswers " .
							"WHERE tblOnlineSurvey.onlinesurveyID = tblOnlineSurveyAnswers.onlinesurveyID " .
							"AND tblOnlineSurvey.onlineformID = $_onlineformID " .
							"AND tblOnlineSurvey.travelerID >  0 " .
							"GROUP BY tblOnlineSurvey.travelerID" ;
                    $rs->Execute($sql);
                    
                    while ($recordset = mysql_fetch_array($rs->Resultset())) {
                        try {
                            $arrParticipants[] = new Traveler($recordset['travelerID']);
                            self::$arrParticipantID[] = $recordset['travelerID'];
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                       
                    }               
                }
                
                return $arrParticipants;
            }
            
            public static function getarrParticipantID(){
            	
            	return self::$arrParticipantID;
            }
    }
?>
