<?php
	/**
	 * Filename: Class.NewCity.php
	 * Date&Time Created : 1 June 2007  10:00PM
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'Class.Location.php';
	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'Class.LocationType.php';
	require_once 'Class.ConnectionProvider.php';
	
	class NewCity extends Location{

		private $rs = null;
		private $rs2 = null;
		private $rs3 = null;
		private $conn = null;

		private $cityID = 0;
		private $approved = 0;
		private $countryID = 0;
		private $travelerID = 0;
		private $continentID = 0;
		private $exists = false;
		private $country = null;
		private $travelerCities = null;

		function NewCity($_cityID = 0){
			try{
				$this-> conn = ConnectionProvider::instance()->getConnection();
				$this-> rs   = new Recordset($this->conn);
				$this-> rs2  = new Recordset($this->conn);
				$this-> rs3  = new Recordset($this->conn);
			}
			catch(exception $e){
				throw $e;
			}

			if (0 < $_cityID){
				$sql = "select * from tblNewCity where cityID = " . $_cityID;
				$this-> rs-> Execute($sql);

				if ($this->rs->Recordcount()){
					$this-> exists = true;
					$this-> countryID = $this-> rs-> Result(0, "countryID");
					$this-> setLocationID($this-> rs-> Result(0, "locID"));
					$this-> setName(stripslashes($this-> rs-> Result(0, "city")));
					$this-> setApprove(0);
					$this-> setContinentID(0);
				}
			}

			$this-> cityID = $_cityID;
			$this-> travelerID = isset($_SESSION['travelerID']) ? $_SESSION['travelerID'] : 0;

			return;
		}

		// Setters
		function setExists($_exist = false){
			$this->exists = $_exist;
		}

		function setCityID($_cityID = 0){
			$this->cityID = $_cityID;
		}

		function setTravelerID($_travID = 0){
			$this-> travelerID = $_travID;
		}
		
		function setCountryID($_countryID = 0){
			$this-> countryID = $_countryID;
		}
		
		function setApprove($_approved = 0){
			$this-> approved = $_approved;
		}
		
		function setContinentID($_continentID = 0){
			$this-> continentID = $_continentID;
		}

		// Getters
		function getCityID(){
			return $this-> cityID;
		}
		
		function getCountryID(){
			return $this-> countryID;
		}
		
		function getTravelerID(){
			return $this-> travelerID;
		}
		
		function getApprove(){
			return $this-> approved;
		}
		
		function getContinentID(){
			return $this-> continentID;
		}
		
		function getCountry(){
			if (null == $this->country){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				$sql = "select distinct locID from GoAbroad_Main.tbcountry where countryID = " . $this-> countryID;
				$this-> rs-> Execute($sql);
				
				if ($this->rs->Recordcount()){
					$location = LocationFactory::instance();
					$this->country = $location-> create($this->rs->Result(0, "locID"));
				}
			}
			return $this-> country;
		}

		function getTravelerCities(){
			if (null == $this->travelerCities){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				$sql = "select a.cityID from tblNewCity as a, tblTravelerToCity as b where b.locID = a.locID and b.travelerID = " . $this->travelerID;
				$this->rs-> Execute($sql);
				
				$arrCity = array();
				if ($this-> rs->Recordcount()){
					while($city = mysql_fetch_array($this->rs->Resultset())){
						$arrCity[] = new NewCity($city['cityID']);
					}
				}
			}
			return $arrCity;
		}
		
		// Boolean
		function isExist(){
			return $this-> exists;
		}

		// Static
		static function getAllCities(){
			try{
				$rs = new Recordset(ConnectionProvider::instance()->getConnection());
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "select distinct cityID from tblNewCity order by city asc";
			$rs-> Execute($sql);

			$arrCity = array();
			if ($rs->Recordcount()){
				while($city = mysql_fetch_array($rs->Resultset())){
					$arrCity[] = new NewCity($city['cityID']);
				}
			}
			return $arrCity;
		}

		// CRUD
		function save(){
			if (!$this->exists){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$this->rs3	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				$sql = "SELECT * FROM tblNewCity " .
						"WHERE city LIKE '" . addslashes($this->getName()) . "' " .
						"AND countryID = " . $this->countryID . " " .
						"GROUP BY cityID";
				$this->rs->Execute($sql);

				if (0 == $this->rs->Recordcount()){
					// insert a row first in tblLocation
		 			$sql2 = "INSERT INTO tblLocation (locationtypeID) " .
							"VALUES ('" . LocationType::$NEWCITY . "')";
		 			$this->rs2->Execute($sql2);

					$sql = "SELECT LAST_INSERT_ID() as lastId";
					$this->rs->Execute($sql);

					$this->setLocationID($this->rs->Result(0, "lastId"));

		 			// insert a row in tbcity
		 			$sql3 = "INSERT INTO tblNewCity (countryID, city, locID) " .
							"VALUES (" . $this->countryID . ", '" . addslashes($this->getName()) . "', " . $this->getLocationID() . ")";
		 			$this->rs3-> Execute($sql3);
		 			$this->cityID = mysql_insert_id();
				}
				else{
					$this->cityID = $this->rs->Result(0, "cityID");
					$this->setLocationID($this->rs->Result(0, "locID"));
				}

				// insert a row in tblTravelerToCity
				$sql2 = "INSERT INTO tblTravelerToCity (travelerID, locID) " .
						"VALUES (" . $this->travelerID . ", " . $this->locationID . ")";
				$this->rs2->Execute($sql2);
			}
			else
				$this->update();
		}
		
		function update(){
			if ($this->exists){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$this->rs3	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				// edit row in tblLocation
	 			$sql = "update tblLocation set locationtypeID = '" . LocationType::$CITY . "' where locID = " . $this->getLocationID();
				$this->rs-> Execute($sql);

				// delete row from tblNewCity
				$sql2 = "delete from tblNewCity where cityID = " . $this->cityID;
				$this-> rs2-> Execute($sql2);

				// delete row from tblTravelerToCity
				$sql3 = "delete from tblTravelerToCity where locID = " . $this->locationID;
				$this-> rs3-> Execute($sql3);

				// insert row new row in tbcity
				$sql = "insert into GoAbroad_Main.tbcity (countryID, city, approved, locID) values (". $this->countryID . ", '" . addslashes($this->getName()) . "', " . 
						$this->approved . ", " . $this->getLocationID() . ")";
				$this-> rs-> Execute($sql);
			}
			else
				$this-> save();
		}
		
		function delete(){
			if ($this->exists){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$this->rs3	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				// delete row from tblNewCity
				$sql = "delete from tblNewCity where cityID = " . $this->cityID;
				$this-> rs-> Execute($sql);

				// delete row from tblLocation
				$sql2 = "delete from tblLocation where locID = " . $this->locationID;
				$this-> rs2-> Execute($sql2);

				// delete row from tblTravelerToCity
				$sql3 = "delete from tblTravelerToCity where locID = " . $this->locationID;
				$this-> rs3-> Execute($sql3);
			}
		}
	}
?>