<?php
	/**
	 * Created : 22 June 2007  7:20PM
	 * Class.EmailNotification.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'class.phpmailer.php';
	require_once 'Class.GaDateTime.php';
	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.Listing.php';
	require_once 'travellog/model/Class.PhotoType.php';
	require_once 'travellog/model/Class.AdminGroup.php';	
	require_once 'travellog/model/Class.CommentType.php';
	require_once 'travellog/model/Class.TravelerProfile.php';

	class EmailNotification{
		protected $rs				= null;
		protected $conn				= null;
		protected $to 				= null;
		protected $cc 				= null;
		protected $bcc 				= null;
		protected $from				= '';
		protected $toName			= null;
		protected $subject 			= '';
		protected $message 			= '';
		protected $fromName			= '';
		protected $noticeID			= 0;
		protected $destination		= '';
		protected $isForAdmin		= false;

		protected $dateCreated	= '';

		public static $INBOX 		= 1;
		public static $INQUIRY 		= 2;
		public static $ADDCITY		= 3;
		public static $COMMENT		= 4;
		public static $BULLETIN		= 5;

		function EmailNotification($_noticeID = 0){
			try{
				$this->conn = new Connection();
				$this->rs   = new Recordset($this->conn);
			}
			catch(exception $e){
				throw $e;
			}

			$this->noticeID = $_noticeID;
			return;
		}

		function setTo($_toEmail = ''){
			$this-> to = $_toEmail;
		}
		
		function setToName($_toName = ''){
			$this-> toName = $_toName;
		}
		
		function setCC($_ccEmail = array()){
			$this-> cc = $_ccEmail;
		}
		
		function setBCC($_bccEmail = array()){
			$this-> bcc = $_bccEmail;
		}
		
		function setFrom($_fromMail = ''){
			$this-> from = $_fromMail;
		}
		
		function setFromName($_fromName = ''){
			$this-> fromName = $_fromName;
		}
		
		function setSubject($_subject = ''){
			$this-> subject = $_subject;
		}

		function setMessage($_text = ''){
			$this-> message = $_text;
		}

		function setIsForAdmin($_forAdmin = false){
			$this-> isForAdmin = $_forAdmin;
		}

		function setDateCreated($_date = ''){
			$this-> dateCreated = $_date;
		}
		
		function setDestination($_dest = ''){
			$this-> destination = $_dest;
		}

		function getTo(){
			return $this-> to;
		}
		
		function getToName(){
			return $this-> toName;
		}
		
		function getCC(){
			return $this-> cc;
		}
		
		function getBCC(){
			return $this-> bcc;
		}
		
		function getFrom(){
			return $this-> from;
		}
		
		function getFromName(){
			return $this-> fromName;
		}

		function getSubject(){
			return $this-> subject;
		}

		function getMessage(){
			return $this-> message;
		}

		function getIsForAdmin(){
			return $this-> isForAdmin;
		}

		function getDateCreated(){
			return $this-> dateCreated;
		}
		
		function getDestination(){
			return $this-> destination;
		}

		function Send(){
			$mail = new PHPMailer();					
			$mail->IsSMTP();			
			$mail->IsHTML(true);
			$mail->From = $this->from;
			$mail->FromName = $this->fromName;					
			$mail->Subject  = $this->subject;			
			$mail->Body = $this->message;				
			$mail->AddAddress($this->to);
			for ($ctr = 0; $ctr < sizeof($this->cc); $ctr++){
				$mail->AddCC($this->cc[$ctr]);
			}
			for ($ctr = 0; $ctr < sizeof($this->bcc); $ctr++){
				$mail->AddBCC($this->bcc[$ctr]);
			}
			$mail->Send();
		}
	}
?>
