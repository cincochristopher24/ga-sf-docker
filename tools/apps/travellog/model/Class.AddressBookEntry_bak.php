<?php
	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");
	class AddressBookEntry{		
		private $addressBookEntryId = 0;
		private $addressBookId = 0;
		private $travelerId = 0;
		private $firstName = "";
		private $lastName = "";		
		private $details = "";		
		private $userName = "";
		private $email = "";
		private $isNotable = 0;
		private $db = null;

		function __construct($mAddressBookEntryId=0){
			$this->db = new dbHandler();
			if($mAddressBookEntryId != 0){
				$this->retrieve($mAddressBookEntryId);
			}
		}
		/***
		 * Getters
		 */
		public function getAddressBookEntryId(){
			return $this->addressBookEntryId;	
		}
		public function getAddressBookId(){
			return $this->addressBookId;	
		}
		public function getTravelerId(){
			return $this->travelerId;	
		}
		public function getFirstName(){
			return $this->firstName;	
		}
		public function getLastName(){
			return $this->lastName;			
		}
		public function getDetails(){
			return $this->details;			
		}
		public function getUserName(){
			return $this->userName;
		}
		public function getEmail(){
			return $this->email;
		}						
		public function getFullName(){			
			return (strlen($this->firstName)||strlen($this->firstName))?$this->firstName." ".$this->lastName:"";		
		}
		public function getIsNotable(){
			return $this->isNotable;	
		}
		/***
		 * Setters
		 */
		public function setFirstName($mFirstName){
			$this->firstName = $mFirstName;		
		}
		public function setLastName($mLastName){
			$this->lastName = $mLastName;			
		}
		public function setDetails($mDetails){
			$this->details = $mDetails;			
		}
		public function setUserName($mUserName){
			$this->userName = $mUserName;
		}
		public function setEmail($mEmail){
			$this->email = $mEmail;
		}
		public function setAddressBookId($mAddressBookId){
			$this->addressBookId = $mAddressBookId;
		}
		public function setIsNotable($mIsNotable){
			$this->isNotable = $mIsNotable;
		}
		public function isGaNetMember(){
			return ($this->travelerId != 0)?true:false;
		}		
		//Implementing crude functions
		public function save(){
			if(0==$this->addressBookEntryId){//create a new record			
				//commented by naldz due to new set of rules.
				if(0){
					//see to it that one of the required fields(username/email) was passed.
					if(!strlen($this->userName) && !strlen($this->email)){
						throw new exception("Unset username and email address, must at least have one of the two.");
						return false;
					}
						
					
					//malipong na logic, because of malipong na rules...(TRICKY) daw kuno.
					$result = true;
					$okToSave = false;
					
					$rsTravelerByUserName = $this->getTravelerByProperty('username',$this->userName);
					$rsTravelerByEmail = $this->getTravelerByProperty('email',$this->email);				
					
					$rsTraveler = false;
					$errorMessage = array();
					//if both username and email was given, check the database if the username has the given email add
					if(strlen($this->userName) > 0 && strlen($this->email) > 0){					 
						 //if username and email exists in the data base
						 if($rsTravelerByUserName->retrieveRecordcount() && $rsTravelerByEmail->retrieveRecordCount()){
						 	//if same traveler record
						 	if($rsTravelerByUserName->getemail() == $rsTravelerByEmail->getemail() && $rsTravelerByUserName->getusername() == $rsTravelerByEmail->getusername()){
						 		$okToSave = true;					 	
						 		$rsTraveler = $rsTravelerByUserName;
						 	}
						 	//if different records
						 	else{
						 		$errorMessage[] = "We have found a GoAbroad.net member with the given username ". $rsTravelerByUserName->getusername()." but his/her email address is ". $rsTravelerByUserName->getemail(). ", different from the given ".$this->email;
						 		$errorMessage[] = "We have found a GoAbroad.net member with the given email address ". $rsTravelerByEmail->getemail()." but his/her username is ".$rsTravelerByEmail->getusername().", different from the given ".$this->userName;
						 	} 
						 }
						 elseif($rsTravelerByUserName->retrieveRecordcount()){					 	
						 	$errorMessage[] = "We have found a GoAbroad.net member with the given username ". $rsTravelerByUserName->getusername() ." but his/her email address is ". $rsTravelerByUserName->getemail() .", different from the given ". $this->email;
						 }
						 elseif($rsTravelerByEmail->retrieveRecordcount()){
						 	$errorMessage[] = "We have found a GoAbroad.net member with the given email ". $rsTravelerByEmail->getemail() ." but his/her username is ". $rsTravelerByEmail->getusername() .", different from the given ".$this->userName;
						 }
					}
					//if given only was username, assume that the new entry is a traveler
					elseif(strlen($this->userName) > 0){
						//check if the userName belongs to a traveler
						if($rsTravelerByUserName->retrieveRecordCount() > 0){
							$okToSave = true;						
							$rsTraveler = $rsTravelerByUserName;						
						}
						//the given username was not found, issue an error
						else{
							//issue an error that no traveler was 
							$errorMessage[] = "Sorry, we did not find a GoAbroad.net member with a username of $this->userName."; 	
						}
					}
					//If given only was email, the new entry might be a traveler or not.
					elseif(strlen($this->email) > 0){
						$okToSave = true;
						//check if the email add belongs to a traveler, if it is get the extra info of that traveler
						if($rsTravelerByEmail->retrieveRecordCount() > 0){						
							$rsTraveler = $rsTravelerByEmail;
						}					
					}
				}
				
				if($okToSave){
					//initialize other required variables for saving to the database
					$vTravelerId = (is_bool($rsTraveler))?0:$rsTraveler->gettravelerID();					
					if(strlen($this->lastName) > 0){ $vLastName = $this->lastName; }
					else { $vLastName = (is_bool($rsTraveler))?"":$rsTraveler->getlastname();	}															
					if(strlen($this->firstName) > 0) $vFirstName = $this->firstName;
					else $vFirstName = (is_bool($rsTraveler))?"":$rsTraveler->getfirstname();					
					$vEmail = (is_bool($rsTraveler))?$this->email:$rsTraveler->getemail();					
					$qry = "INSERT INTO Travel_Logs.tblAddressBookEntry (addressBookID, travelerID, email, firstName, lastName, details, isNotable) " .
					   	   "VALUES(". mysql_escape_string($this->addressBookId). ",".  mysql_escape_string($vTravelerId). ",'" .mysql_escape_string($vEmail). "','" .mysql_escape_string($vFirstName). "','" .mysql_escape_string($vLastName). "','" .mysql_escape_string($this->details). "',".mysql_escape_string($this->isNotable).")"; 
					$this->db->execQuery($qry);
					return true;
				}
				else{
					return $errorMessage;
				}							
			} 
			else{//update the current record
				//if the current addressbook entry points to member of GAnet, email should not be edited.
				//if the current addressbook enter points to a GAnet non-member, email should be updateable.			
				$qry = "UPDATE Travel_Logs.tblAddressBookEntry SET firstName = '".mysql_escape_string($this->firstName)."', lastName = '".mysql_escape_string($this->lastName)."', " .
					   "details = '".mysql_escape_string($this->details)."', isNotable= ".mysql_escape_string($this->isNotable);
				if($this->travelerId == 0){
					$qry .= ",email = '".mysql_escape_string($this->email)."' ";
				} 
				$qry .= " WHERE addressBookEntryId = $this->addressBookEntryId";
				$this->db->execQuery($qry);
				return true;
			}
		}
		public function retrieve($mAddressBookEntryId=0){
			
			if(!$mAddressBookEntryId){
				throw new exception('$mAddressBookEntryId was expected in Class AddressBookEntry but was not passed in.');
				return false;
			}									
			//get the record from tblAddressBookEntry
			$qAddressBookEntry = $this->db->execQuery("select * from Travel_Logs.tblAddressBookEntry where addressBookEntryId = $mAddressBookEntryId");
			$rsAddressBookEntry = new iRecordset($qAddressBookEntry);
			if(!$rsAddressBookEntry->retrieveRecordCount()){
				throw new exception('AddressbookentryID that was passed in Class AddressBookEntry did not exist in the database.');
				return false;
			}			
			//assign the private properties of the AddressBookEntry Class.
			$this->addressBookEntryId = $rsAddressBookEntry->getaddressBookEntryID();
			$this->addressBookId = $rsAddressBookEntry->getaddressBookID();
			$this->travelerId = $rsAddressBookEntry->gettravelerID();			
			$this->firstName = $rsAddressBookEntry->getfirstName();
			$this->lastName = $rsAddressBookEntry->getlastName();
			$this->details = $rsAddressBookEntry->getdetails();
			$this->email = 	$rsAddressBookEntry->getemail();	
			$this->isNotable = $rsAddressBookEntry->getIsNotable();
				
			if($this->travelerId > 0){	
				//get extra info(username and email) of the traveler from tblTraveler			
				$qTraveler = $this->db->execQuery("select username, email from Travel_Logs.tblTraveler where travelerID = $this->travelerId");			
				$rsTraveler = new iRecordset($qTraveler);			
				if(!$rsTraveler->retrieveRecordCount()){
					throw new exception('TravelerID that was passed in AddressBookEntry class does not exist in the database.');
					return false;
				}
				$this->userName = $rsTraveler->getusername();
				$this->email = $rsTraveler->getemail();
			}
		}		
		public function delete($mAddressBookEntryId=0){
			//check if there is a record to delete
			$tempAddressBookEntryId = (0 != $mAddressBookEntryId && is_numeric($mAddressBookEntryId))?$mAddressBookEntryId:$this->addressBookEntryId;
			$this->db->execQuery("delete from Travel_Logs.tblAddressBookEntry where addressBookEntryId = $tempAddressBookEntryId");				
		}		
		private function getTravelerByProperty($fieldToCheck,$fieldValue){
			$qTraveler = $this->db->execQuery("select firstname, lastname, travelerID, username, email from Travel_Logs.tblTraveler where $fieldToCheck = '".mysql_escape_string($fieldValue)."'");
			$rsTraveler = new iRecordset($qTraveler);
			return $rsTraveler;
		}		
	}
?>