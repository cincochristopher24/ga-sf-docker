<?php
 	require_once 'Class.Connection.php';
 	require_once("travellog/model/Class.GanetDbHandler.php");
 	require_once("Class.ResourceIterator.php");
 	require_once 'Class.Recordset.php';
 	require_once 'travellog/model/Class.Listing.php';
 	require_once 'travellog/model/Class.Favorite.php';
	require_once 'travellog/model/Class.Sendable.php';
 	require_once 'travellog/model/Class.Inquiry.php';
 	require_once 'travellog/model/Class.Travel.php';
 	require_once 'travellog/model/Class.Inquiry.php';
 	require_once 'travellog/model/Class.SentItems.php';
 	require_once 'travellog/model/Class.Drafts.php';
 	require_once 'travellog/model/Class.Trash.php';
 	require_once 'travellog/model/Class.InquiryBox.php';
 	require_once 'travellog/model/Class.TravelerProfile.php';
 	require_once 'travellog/model/Class.PrivacyPreference.php';
 	require_once 'travellog/model/Class.TravelerEvent.php';
 	require_once 'travellog/model/Class.TravelLog.php';  
 	require_once 'travellog/model/Class.TravelDestination.php';
 	require_once 'travellog/model/Class.Inbox.php';
 	require_once 'travellog/model/Class.MessageType.php';
	require_once 'Class.dbHandler.php';//added by Jul, 2008-11-17
	require_once 'travellog/model/Class.VideoAlbum.php'; // added by Neri, 2009-02-02
	
	require_once 'travellog/model2/traveler/Class.ganetTravelerService.php';
	require_once 'travellog/vo/Class.ganetValueObject.php';
	require_once 'Class.ConnectionProvider.php';
	require_once('Cache/ganetCacheProvider.php');
	require_once('travellog/model/Class.GroupPeer.php');
	require_once('travellog/model/Class.JournalPeer.php');
	
	class Traveler implements Sendable {				
		protected $rs		= NULL;
		protected $rs2		= NULL;
		protected $rs3		= NULL;
		protected $rs4		= NULL;
		protected $conn		= NULL;

		protected $travID		= 0;
		protected $online		= 0;
		protected $active		= 0;
		protected $suspended	= 0;
		protected $sendableID	= 0;
		protected $username		= '';
		protected $password		= '';
		protected $profile		= NULL;
		protected $locationID	= 0;
		protected $showSteps	= 1;
		protected $presentLocation	= 0;
		protected $travelerStatus    	= 0;
		protected $deactivated	= 0;

		protected $subgroup		= NULL;
		protected $inquiry		= NULL;
		protected $favorites	= NULL;

		protected $sent			= NULL;
		protected $inbox		= NULL;
		protected $trash		= NULL;
		protected $drafts		= NULL;
		protected $inquiryBox	= NULL;
		protected $travelerEvents = NULL;
		protected $friendshipDate = NULL;

		protected $numberOfJournals		= 0;
		protected $numberOfTravelers	= 0;
		protected $numberOfTravelLogs	= 0;
		protected $numberOfTravelVideos = null;

		protected $numberOfFriends		= 0;
		protected $numberOfBlockedUsers	= 0;
		protected $numberOfGroupInvites	= 0;
		protected $numberOfGroupRequests	= 0;
		protected $numberOfFriendRequests	= 0;
		protected $numberOfPendingFriends	= 0;
		protected $obj_travel_destination	= NULL;

		private $mIsAdvisor	= 0;
		
		/**
		 * @staticvar integer $statvar
		 */
		 
 		public static $statvar;
 		public static $cntNewMembers = 0;
 		public static $cntAllTravelers = 0;
 		public static $cntGAUsers = 0;
 		
		/**
		 * Constructor of class Traveler
		 * @param integer $_id
		 * @return Traveler|exception phpDocumentor Traveler object or exception
		 */

		function Traveler($_id = 0, $data=null){
			$this->tServ = new ganetTravelerService();
			
			try{ 				
 				$this->conn = ConnectionProvider::instance()->getConnection();
				$this->rs   = new Recordset($this->conn);
				$this->rs2  = new Recordset($this->conn);
				$this->rs3  = new Recordset($this->conn);
				$this->rs4  = new Recordset($this->conn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			if( 0 < $_id ){
				$this-> setTravelerID($_id);
				$vo = new ganetValueObject();
				$vo->set('id',$_id);
				$vo->set('CLASS_NAME',get_class($this));
				
				$t = $this->tServ->retrieve($vo);					// used service to retrieve traveler data
				
				$this->active 			= $t->isActive();
				$this->suspended 		= $t->isSuspended();
				$this->locationID 		= $t->getLocationID();
				$this->showSteps 		= $t->getShowSteps();
				$this->sendableID		= $t->getSendableID();
				$this->username 		= $t->getUserName();
				$this->password			= $t->getPassword();
				$this->profile			= $t->getTravelerProfile();
				$this->mIsAdvisor		= $t->getIsAdvisor();
				$this->deactivated		= $t->isDeactivated();

			}
			elseif(is_array($data)){
				$this->active 			= $data['active'];
				$this->suspended 		= $data['isSuspended'];
				$this->locationID 		= $data['currlocationID'];
				$this->showSteps 		= $data['showsteps'];
				$this->setTravelerID($data['travelerID']);
				$this->setSendableID($data['sendableID']);
				$this->setUserName($data['username']);
				$this->setPassword($data['password']);
				if(isset($data['travelerStatus']))
					$this->travelerStatus = $data['travelerStatus'];
				if(isset($data['isadvisor'])){
					$this->mIsAdvisor = $data['isadvisor'];
				}
				$this->deactivated		= $data['deactivated'];
			}
		}
		
		/**
		 * Initializes the properties of the class.
		 * 
		 * @param array $props The values of the properties of the object.
		 * 
		 * @return void
		 */
		function initialize(array $props){
			if (count($props)){	
				$this->travID		= isset($props["travelerID"]) ? $props["travelerID"] : 0;
				$this->active		= isset($props["active"]) ? $props["active"] : 0; 
				$this->suspended	= isset($props["isSuspended"]) ? $props["isSuspended"] : 0;
	 			$this->locationID	= isset($props["currlocationID"]) ? $props["currlocationID"] : 0; 
	 			$this->sendableID	= isset($props["sendableID"]) ? $props["sendableID"] : 0;
	 			$this->username		= isset($props["username"]) ? $props["username"] : '';
				$this->password		= isset($props["password"]) ? $props["password"] : ''; 
	 			$this->showSteps	= isset($props["showsteps"]) ? $props["showsteps"] : 1;	
				$this->mIsAdvisor	= isset($props["isadvisor"]) ? $props["isadvisor"] : $this->mIsAdvisor;
	 			$this->deactivated	= isset($props["deactivated"]) ? $props["deactivated"] : 0;
	
				$this->profile = new TravelerProfile();
				$this->profile->setProfileData($props);
			}
		}

		// set data in array and assign values; minimize db query
		function setTravelerData($_data = array() ) {
			if (count($_data)){				
				$this->travID		= isset($_data["travelerID"]) ? $_data["travelerID"] : 0;
				$this->active		= isset($_data["active"]) ? $_data["active"] : 0; 
				$this->suspended	= isset($_data["isSuspended"]) ? $_data["isSuspended"] : 0;
	 			$this->locationID	= isset($_data["currlocationID"]) ? $_data["currlocationID"] : 0; 
	 			$this->sendableID	= isset($_data["sendableID"]) ? $_data["sendableID"] : 0;
	 			$this->username		= isset($_data["username"]) ? $_data["username"] : '';
				$this->password		= isset($_data["password"]) ? $_data["password"] : ''; 
	 			$this->showSteps	= isset($_data["showsteps"]) ? $_data["showsteps"] : 1;			
				$this->travelerStatus		= isset($_data["travelerStatus"]) ? $_data["travelerStatus"] : 0;
				$this->mIsAdvisor	= isset($_data["isadvisor"]) ? $_data["isadvisor"] : $this->mIsAdvisor;			
				$this->deactivated	= isset($_data["deactivated"]) ? $_data["deactivated"] : 0;
			}
		}

		/**
		 * Function name: Suspend
		 * Sets isSuspended in tblTraveler to 1 when a traveler is suspended or banned from the site
		 */		 
		function Suspend(){			
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$sqlQuery = "update tblTraveler set `isSuspended` = 1 where `travelerID` = $this->travID";
			$this->rs->Execute($sqlQuery);			
			$this->suspended = 1;
			
			$this->invalidateCacheEntry();
		}

		/**
		 * Function name: Activate
		 * Sets isSuspended in tblTraveler to 0 when traveler has been activated after suspension
		 */		
		function Activate(){			
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$sqlQuery = "update tblTraveler set `isSuspended` = 0 where `travelerID` = $this->travID";
			$this->rs->Execute($sqlQuery);
			$this->suspended = 0;
			
			$this->invalidateCacheEntry();
		}
		
		/**
		* Function name: DeactivateTraveler
		* Sets deactivated in tblTraveler to 1 when traveler requests account deactivation
		**/
		function DeactivateAccount(){
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$sqlQuery = "update tblTraveler set `deactivated` = 1 where `travelerID` = $this->travID";
			$this->rs->Execute($sqlQuery);
			
			$this->deactivated = 1;
			$this->invalidateCacheEntry();
		}
		
		function isDeactivated(){
			return $this->deactivated;
		}


		/**
		 * Function name: getClientID
		 * @return integer clientID
		 */
		 
		public static function getClientID($_sendableID = 0){			
			try{
 				//$mConn	=	ConnectionProvider::instance()->getConnection();
				$mRs	=  ConnectionProvider::instance()->getRecordset();
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$sql = "SELECT DISTINCT tblGrouptoAdvisor.clientID " .
					"FROM tblGrouptoAdvisor, tblTraveler " .
					"WHERE tblGrouptoAdvisor.travelerID = tblTraveler.travelerID " .
					"AND tblTraveler.sendableID = " . $_sendableID;
			$mRs->Execute($sql);

			return $mRs->Result(0, "clientID");
		}

		/**
		 * Function name: getTravelersInLocation
		 * @param integer $_locID
		 * @return array Travelers
		 */

		public static function getTravelersInLocation($_locID = 0){
			try{
				//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= 	ConnectionProvider::instance()->getRecordset();
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "SELECT travelerID, username, password, currlocationID, sendableID, isSuspended, active, showsteps " .
					"FROM tblTraveler " .
					"WHERE currlocationID = " . $_locID;
			$mRs->Execute($sql);

			$arrTraveler = array();
			if (0 < $mRs->Recordcount()){
				while ($Traveler = mysql_fetch_array($mRs->Resultset())){
					$mTraveler = new Traveler();
					$mTraveler->setTravelerData($Traveler);
					$arrTraveler[] = $mTraveler;
				}
			}

			return $arrTraveler;
		}

		/**
		 * Function name: getMostPopular
		 * @static
		 * @param mixed $_rowslimit
		 * @return array|NULL|exception phpDocumentor array of Traveler object or NULL or exception
		 */
		
		/*********************************************************************************
		 * edits of neri to getMostPopular():
		 *		added loggedTravelerID so as not to display certain travelers' 
		 *			profiles to blocked users: 									11-14-08 
		 *		checked if traveler is suspended:								12-17-08		
		 ********************************************************************************/
		
		public static function getMostPopular($_rowslimit = NULL, $loggedTravelerID = 0){			
			try{
 				//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
				$mRs2	= ConnectionProvider::instance()->getRecordset2();
			}
			catch (exception $e){				   
			   throw $e;
			}

			if ($_rowslimit){
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$limitstr = " LIMIT " . $offset . " , " . $rows;
	 		}	 		
	 		else
	 			$limitstr = " LIMIT 0, 6";

			$sql = "SELECT SQL_CALC_FOUND_ROWS travelerID, username, password, currlocationID, sendableID, isSuspended, active, showsteps " .
					"FROM tblTraveler " .
					"WHERE active > 0 " .
					"AND travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor " .
						")" .
					"GROUP BY travelerID " .
					"ORDER BY views DESC" . $limitstr;
			$mRs->Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$result2 = mysql_fetch_array($mRs2->Execute($sql2));

			$arrTravelers = array();
			Traveler::$statvar = $result2['totalRecords'];

			if (0 < $mRs->Recordcount()){
				while ($Traveler = mysql_fetch_array($mRs->Resultset())){
					$mTraveler = new Traveler();
					$mTraveler->setTravelerData($Traveler);
					
					if (!$mTraveler->isSuspended()) {
						if (0 < $loggedTravelerID && !$mTraveler->isBlocked($loggedTravelerID))				// editted by neri: 11-17-08
							$arrTravelers[] = $mTraveler;
						else if (0 == $loggedTravelerID)
							$arrTravelers[] = $mTraveler;
					}
				}
			}

			return $arrTravelers;
		}		

		public static function getMostTraveled($_numrows = 0){			
			try{ 				
 				//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$arrTraveler = array();
			$limitstr = ($_numrows) ? " LIMIT 0, " . $_numrows : "";
			
			$sql = "SELECT tblTraveler.travelerID, tblTraveler.username, tblTraveler.password, tblTraveler.currlocationID, " .
					"tblTraveler.sendableID, tblTraveler.isSuspended, tblTraveler.active, tblTraveler.showsteps, " .
					"count(tblTravelertoCountriesTravelled.countryID) AS numcountries " .
					"FROM tblTravelertoCountriesTravelled, tblTraveler " .
					"WHERE tblTraveler.travelerID = tblTravelertoCountriesTravelled.travelerID " .
					"AND NOT EXISTS " .
						"(SELECT DISTINCT travelerID from tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblTraveler.travelerID) " .
					"GROUP BY tblTraveler.travelerID " .
					"ORDER BY numcountries DESC" . $limitstr;
			$mRs->Execute($sql);

			while ($Traveler = mysql_fetch_array($mRs->Resultset())){
				try{
					$mTraveler = new Traveler();
					$mTraveler->setTravelerData($Traveler);
					$arrTraveler[] = $mTraveler;
				}
				catch (exception $e) {		
					throw $e;
				}
			}

			return $arrTraveler;			
		}		
		
		public static function getMostChronicles($numrows = 0){			
			try{ 				
 				//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$travelers = array();
			$limitstr = ($numrows) ? " LIMIT 0, " . $numrows : "";
			
			$sql = "SELECT tblTraveler.travelerID, tblTraveler.username, tblTraveler.password, tblTraveler.currlocationID, " .
					"tblTraveler.sendableID, tblTraveler.isSuspended, tblTraveler.active, tblTraveler.showsteps, " .
					"count(tblTravelLog.travellogID) AS numentries " .
					"FROM tblTravelLog, tblTravel, tblTraveler " .
					"WHERE tblTravelLog.travelID = tblTravel.travelID " .
					"ANF tblTraveler.travelerID = tblTravel.travelerID " .
					"AND NOT EXISTS " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblTraveler.travelerID) " .
					"GROUP BY tblTraveler.travelerID " .
					"ORDER BY numentries DESC " . $limitstr;
			$mRs->Execute($sql);

			while ($Traveler = mysql_fetch_array($mRs->Resultset())){
				try{
					$travelers[] = new Traveler ($Traveler['travelerID']);				// changed by neri from $traveler to $Traveler: 11-17-08 
				}
				catch (exception $e) {		
					throw $e;
				}
			}			
			return $travelers;
			
		}
		
		
		public static function getMostPhotographs($numrows = 0){			
			try{
 				//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$arrTraveler = array();
			$limitstr = ($numrows) ? " LIMIT 0, " . $numrows : "";

			$sql = "SELECT tblTraveler.travelerID, tblTraveler.username, tblTraveler.password, tblTraveler.currlocationID, " .
					"tblTraveler.sendableID, tblTraveler.isSuspended, tblTraveler.active, tblTraveler.showsteps, " .
					"count(DISTINCT photoID.photoID) as numphotos " .
					"FROM tblTravelLogtoPhoto, tblTravelLog, tblTravel, tblTraveler " .
					"WHERE tblTravelLogtoPhoto.travellogID = tblTravelLog.travellogID " .
					"AND tblTravelLog.travelID = tblTravel.travelID " .
					"AND tblTraveler.travelerID = tblTravel.travelerID " .
					"AND tblTravel.travelerID NOT IN " .
						"(select distinct travelerID from tblGrouptoAdvisor) " .
					"group by tblTravel.travelerID " .
					"ORDER BY numphotos DESC " . $limitstr;
			$mRs->Execute($sql);
			
			while ($Traveler = mysql_fetch_array($mRs->Resultset())){
					$mTraveler = new Traveler;
					$mTraveler->setTravelerData($Traveler);
					$arrTraveler[] = $mTraveler;
			}

			return $arrTraveler;
		}
		
		
		public static function getMostPhotographsByTravelerID($travelerID = 0, $numrows = 0){			
			try{
 				//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$numphotos = 0;
			
			$sql = "SELECT travelerID, count(distinct photoID) as numphotos " .
					"FROM tblTravelLogtoPhoto, tblTravelLog, tblTravel " .
					"WHERE tblTravelLogtoPhoto.travellogID = tblTravelLog.travellogID " .
					"AND tblTravelLog.travelID = tblTravel.travelID " .
					"AND travelerID = " . $travelerID .
					" GROUP BY travelerID";
			$mRs->Execute($sql);

			if ($mRs->RecordCount()) {
				try{
					$numphotos = $mRs->Result(0,'numphotos');
				}
				catch (exception $e) {		
					throw $e;
				}
			}			

			return $numphotos;			
		}
		
		/**
		 * Function name: getNumberOfNewMembers
		 * @static
		 * @return integer - number of newest members in the network
		 */
		 
		public static function getCountNewestMembers(){
						
			try{ 				
 				//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
			}
			
			catch (exception $e){				   
			   throw $e;
			}
			
			$sqlQuery = "select dateregistered, travelerID from tblTraveler where active > 0 AND travelerID not in (select distinct travelerID from tblGrouptoAdvisor) " .
					"group by travelerID order by dateregistered desc limit 0, 6";
			$rs-> Execute($sqlQuery);
			
			Traveler::$cntNewMembers = $rs-> Recordcount();
			return $rs-> Recordcount();
		}
		
		/**
		 * Function name: getNewestMembers
		 * @static
		 * @param mixed $_rowslimit
		 * @return array|NULL|exception phpDocumentor array of Traveler object or NULL or exception
		 */
		 
		/***********************************************************************************
		 * edits of neri to getNewestMembers():
		 *		added loggedTravelerID so as not to display certain travelers' 
		 *			profiles to blocked users: 									11-14-08 
		 *		checked if traveler is suspended:								12-17-08
		 ***********************************************************************************/ 
		 
		public static function getNewestMembers($_rowslimit = NULL, $loggedTravelerID = 0){
						
			try{ 				
 				//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
			}
			
			catch (exception $e){				   
			   throw $e;
			}
			
			if ($_rowslimit){
				$offset = $_rowslimit-> getOffset();
				$rows = $_rowslimit-> getRows();
				$limitstr = " LIMIT " . $offset . " , " . $rows;				
			}
			
			else
				$limitstr = " LIMIT 0, 6";
				
			$travelers = array();
			
			$sqlQuery = "select dateregistered, travelerID from tblTraveler where active > 0 AND travelerID not in (select distinct travelerID from tblGrouptoAdvisor) " .
					"group by travelerID order by dateregistered desc" . $limitstr;
			$rs-> Execute($sqlQuery);
			
			if (0 != $rs->Recordcount()){
				while ($traveler = mysql_fetch_array($rs->Resultset())){
					try{
						$travelerObj = new Traveler ($traveler['travelerID']);
						
						if (!$travelerObj->isSuspended()) {
							if (0 < $loggedTravelerID && !$travelerObj->isBlocked($loggedTravelerID))
								$travelers[] = $travelerObj;
							else if (0 == $loggedTravelerID)
								$travelers[] = $travelerObj;
						}
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				
				return $travelers;
			}
			
			else
				return NULL;
		}

		/**
		 * Function name: getNumberOfAllTravelers
		 * @static
		 * @return integer - number of travelers
		 */
		    
		public static function getCountAllTravelers(){
			
			try{ 				
 				//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
				$rs2  = ConnectionProvider::instance()->getRecordset2();
			}
			
			catch (exception $e){				   
			   throw $e;
			}

			$sqlQuery = "select distinct `travelerID` from tblTraveler where active > 0 AND travelerID not in " .
					"(select distinct travelerID from tblGrouptoAdvisor)";
			$rs-> Execute($sqlQuery);
			
			Traveler::$statvar = $rs-> Recordcount();
			return $rs-> Recordcount();
		}
		
		/**
		 * Function name: getAllTravelers
		 * @static
		 * @param mixed $_rowslimit
		 * @param bool $_filtercriteria
		 * @return array|NULL|exception array of Traveler object or NULL or exception
		 */
		
		/**********************************************************************************
		 * edits of neri to getAllTravelers():
		 *		added loggedTravelerID so as not to display certain travelers'
		 * 			profiles to blocked users: 									11-14-08 
		 * 		filtered data: 													11-14-08
		 * 		checked if traveler is suspended:								12-17-08
		 *
		 *	edits by	miss cig
		 *	recoded fetching of filtercriteria, turned it into a foreach loop	11-19-09
		 **********************************************************************************/
		 
		public static function getAllTravelers($_filtercriteria = NULL, $_rowslimit = NULL, $loggedTravelerID = 0, $includeAdvisor = false, $includeSuspended = false){
			try{ 				
 				//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
				$mRs2	= ConnectionProvider::instance()->getRecordset2();
			}
			catch (exception $e){				   
			   throw $e;
			}

			$arrTraveler = array();
			
			if ($_filtercriteria){	
				$ctr = 0;
				$qryCondition = "";
				$mCondition	= $_filtercriteria->getConditions();

				foreach ( $mCondition as $mCondition )
				{
					$mAttrName	= $mCondition->getAttributeName();
					$mOperation	= $mCondition->getOperation();

					switch ($mOperation){
		 				case FilterOp::$ORDER_BY :
		 					$qryCondition .= " ORDER BY " . $mAttrName;
		 					break;
		 				case FilterOp::$ORDER_BY_DESC :
		 					$qryCondition .= " ORDER BY " . $mAttrName . " DESC " ;
		 					break;
		 				case FilterOp::$LIKE :
		 					$mBoolean = $_filtercriteria->getBooleanOps();
		 					$mValue = $mCondition->getValue();
		 					$qryCondition .= $mBoolean[$ctr]. ' ' . $mAttrName . " LIKE '%".trim($mValue)."%' ";
		 					break;
		 				case FilterOp::$IN:
		 					$mBoolean = $_filtercriteria->getBooleanOps();
		 					$mValue = $mCondition->getValue();
		 					$qryCondition .= $mBoolean[$ctr]. ' ' . $mAttrName .' IN ('.$mValue.') ';
		 					break;
		 			}
					$ctr += 1;
				}
			}
			else
				$qryCondition = " ORDER BY `latestlogin` DESC";

		 	if ($_rowslimit){	 			
	 			$mOffset = $_rowslimit-> getOffset();
	 			$mRows =  $_rowslimit-> getRows();
	 			$qryLimit = " LIMIT " . $mOffset . " , " . $mRows;
	 		}	
	 		else
	 			$qryLimit = "";

			$sql = "SELECT SQL_CALC_FOUND_ROWS travelerID, username, password, currlocationID, sendableID, isSuspended, active, showsteps, travelerStatus, deactivated " .
					"FROM tblTraveler " .
					"WHERE active = 1 ";
			
			/*if(!$includeAdvisor){
				$sql .=		"AND tblTraveler.travelerID NOT IN " .
							"(" .
								"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor " .
							")";
			}*/
			
			$sql .= $qryCondition . $qryLimit;

			$mRs->Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$mRs2->Execute($sql2);

			Traveler::$statvar = $mRs2->Result(0, "totalRecords");
			
			if (0 < $mRs2->Result(0, "totalRecords")){
				while ($Traveler = mysql_fetch_assoc($mRs->Resultset())){
					$mTraveler = new Traveler();
					$mTraveler->setTravelerData($Traveler);
					
					if (!$mTraveler->isSuspended() || $includeSuspended) {
						if (0 < $loggedTravelerID && !$mTraveler->isBlocked($loggedTravelerID))				
							$arrTraveler[] = $mTraveler;
						else if (0 == $loggedTravelerID)
							$arrTraveler[] = $mTraveler;
					}
				}
			}

			return $arrTraveler;
		}

		/**
		 * Function name: getAdminFeaturedTravelerPhotos
		 * @static
		 * @param mixed $_rowslimit
		 * @param bool $_filtercriteria
		 * @return array|NULL|exception array of Traveler object or NULL or exception
		 */
		public static function getAdminFeaturedTravelerPhotos($_filtercriteria = null, $_rowsLimit = null){
			try{ 				
 				//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
				$mRs2	= ConnectionProvider::instance()->getRecordset2();
			}
			catch (exception $e){				   
			   throw $e;
			}

			if ($_filtercriteria){	
				$mCondition	= $_filtercriteria->getConditions();
	 			$mAttrName	= $mCondition[0]->getAttributeName();
				$mOperation	= $mCondition[0]->getOperation();

	 			switch ($mOperation){
	 				case FilterOp::$ORDER_BY :
	 					$qryCondition = " ORDER BY " . $mAttrName;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$qryCondition = " ORDER BY " . $mAttrName . " DESC " ;
	 					break;
	 				case FilterOp::$LIKE :
	 					$mBoolean = $_filtercriteria->getBooleanOps();
	 					$mValue = $mCondition[0]->getValue();
	 					$qryCondition = $mBoolean[0]. ' ' . $mAttrName . " LIKE '%$mValue%'";
	 					break;
	 				case FilterOp::$IN:
	 					$mBoolean = $_filtercriteria->getBooleanOps();
	 					$mValue = $mCondition[0]->getValue();
	 					$qryCondition = $mBoolean[0]. ' ' . $mAttrName .' IN ('.$mValue.')';
	 					break;
	 			}
			}
			else
				$qryCondition = " ORDER BY `latestlogin` DESC";

			if ($_rowsLimit){
				$offset = $_rowsLimit-> getOffset();
	 			$rows =  $_rowsLimit-> getRows();
	 			$limitstr = " LIMIT " . $offset . " , " . $rows;
			}
			else
				$limitstr = '';

			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.travelerID, tblTraveler.sendableID, tblPhoto.* " .
					"FROM tblTraveler, tblTravelertoPhoto, tblPhoto " .
					"WHERE tblTraveler.active = 1 " .
					"AND tblTraveler.travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor " .
						")" .
					"AND tblTravelertoPhoto.travelerID = tblTraveler.travelerID " .
					"AND tblPhoto.photoID = tblTravelertoPhoto.photoID " . $qryCondition . $limitstr;
			$mRs->Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$mRs2->Execute($sql2);

			Traveler::$statvar = $mRs2->Result(0, "totalRecords");
			$arrTravelerPhotos = array();

			if (0 < $mRs2->Result(0, "totalRecords")){
				while ($recordset = mysql_fetch_assoc($mRs->Resultset())){
					$profileData = array();								
					$profileData['travelerID'] = $recordset['travelerID'];

					$mProfile = new TravelerProfile();
					$mProfile->setProfileData($profileData);

					$Photo = new Photo($mProfile);
					$Photo->setPhotoID($recordset['photoID']);
					$Photo->setCaption($recordset['caption']);
					$Photo->setPhotoTypeID($recordset['phototypeID']);
					$Photo->setPrimaryPhoto($recordset['primaryphoto']);
					$Photo->setDateuploaded($recordset['dateuploaded']);
					$Photo->setFileName(stripslashes($recordset['filename']));
					$Photo->setThumbFileName(stripslashes($recordset['thumbfilename']));

					$arrTravelerPhotos[] = $Photo;
				}
			}

			return $arrTravelerPhotos;
		}
		
		/**
		 * Function name: getTravelerIDByUsername
		 * @param string $_username
		 * @return integer
		 */
		 
		public static function getTravelerIDByUsername($_username = ''){
		  	
		  	try{ 				
 				$conn = ConnectionProvider::instance()->getConnection();
				$rs   = new Recordset($conn);
			}
			
			catch (exception $e){				   
			   throw $e;
			}
			
			$sqlQuery = "select `travelerID` from tblTraveler where `username` = '$_username' limit 1";
		  	$rs-> Execute($sqlQuery);
		  	
		  	if (0 < $rs->Recordcount())		  		
		  		return $rs->Result(0, "travelerID");
		  	
		  	else
				return 0;	
			
		}
		
		/**
		 * Function name: getTravelerIDByEmail
		 * @param string $_emailAdd
		 * @return integer
		 */
		 
		public static function getTravelerIDByEmail($_emailAdd = ''){
			
			try{
				$conn = ConnectionProvider::instance()->getConnection();
				$rs = new Recordset($conn);
			}
			
			catch (exception $e){
				throw $e;
			}
			
			$arrTrav = array();
			
			$sqlQuery = "select `travelerID` from tblTraveler where `email` = '$_emailAdd' and travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$rs->Execute($sqlQuery);
			
			while ($Traveler = mysql_fetch_array($rs->Resultset())){
				$arrTrav[] = $Traveler['travelerID'];
			}
			
			return $arrTrav;
		}
		
		public static function getFullNameByEmail($_emailAdd = ''){
			
			try{
				$conn = ConnectionProvider::instance()->getConnection();
				$rs = new Recordset($conn);
			}
			
			catch (exception $e){
				throw $e;
			}
			
			$sqlQuery = "select `firstname`, `lastname` from tblTraveler where `email` = '$_emailAdd' and travelerID not in (select distinct travelerID from tblGrouptoAdvisor) limit 1";
			$rs->Execute($sqlQuery);
			
			if($Traveler = mysql_fetch_array($rs->Resultset()))
				return $Traveler['firstname']. " ".$Traveler['lastname'];
			else return false;
			
		}		
		
		/**
		 * Function name: getTravelerIDByName
		 * @param string $_firstName
		 * @param string $_lastName
		 * @return integer
		 */
		 
		public static function getTravelerIDByName($_firstName = '', $_lastName = ''){
			
			try{ 				
 				$conn = ConnectionProvider::instance()->getConnection();
				$rs   = new Recordset($conn);
			}
			
			catch (exception $e){				   
			   throw $e;
			}
			
			$arrTrav = array();
			$_lastName = strtolower($_lastName);
			$_firstName = strtolower($_firstName);
			
			if (0 < strlen($_firstName) && 0 < strlen($_lastName)){
				$sqlQuery = "select `travelerID` from tblTraveler where `firstname` = '$_firstName' and `lastname` = '$_lastName' " .
						" and travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
				$rs-> Execute($sqlQuery);
			}
			
			else if (0 < strlen($_firstName) && 0 == strlen($_lastName)){
				$sqlQuery = "select `travelerID` from tblTraveler where `firstname` = '$_firstName' and travelerID not in " .
						"(select distinct travelerID from tblGrouptoAdvisor)";
				$rs-> Execute($sqlQuery);
			}
			
			else if (0 == strlen($_firstName) && 0 < strlen($_lastName)){
				$sqlQuery = "select `travelerID` from tblTraveler where `lastname` = '$_lastName' and travelerID not in " .
						"(select distinct travelerID from tblGrouptoAdvisor)";
				$rs-> Execute($sqlQuery);
			}
			
			if (0 != strlen($_firstName) || 0 != strlen($_lastName)){
				
				while($traveler = mysql_fetch_array($rs->Resultset())){
					$arrTrav[] = $traveler['travelerID'];
				}
			}
			
			return $arrTrav;
		}
		 
		/**
		 * Function name: getInactiveTravelers
		 * @static
		 * @return array of Inactive Travelers
		 */

		static function getInactiveTravelers($_rowslimit = ''){
			try{
				$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= new Recordset($mConn);
				$mRs2	= new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$limitstr = "";
			$arrTraveler = array();

			if ($_rowslimit){
	 			$rows =  $_rowslimit-> getRows();
				$offset = $_rowslimit-> getOffset();
	 			$limitstr = " LIMIT " . $offset . " , " . $rows;
			}

			$sql = "SELECT SQL_CALC_FOUND_ROWS * " .
					"FROM tblTraveler " .
					"WHERE active = 0 " .
					"GROUP BY travelerID " .
					"ORDER BY username ASC" . $limitstr;
			$mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() AS totalRecords";
			$mRs2->Execute($sql2);

			Traveler::$statvar = $mRs2->Result(0, "totalRecords");

			while($Traveler = mysql_fetch_array($mRs->Resultset())){
				$mTraveler = new Traveler;
				$mTraveler->setTravelerData($Traveler);
				$arrTraveler[] = $mTraveler;
			}

			return $arrTraveler;
		}

		/**
		 * Function name: sendMessage
		 * @param Message $_message
		 * @static
		 * Call functions and/instantiate classes based on value of Discriminator
		 */
		
		function sendMessage(Message $_message = NULL){
			
			$messageID = $_message-> getMessageID();
			$destination = $_message-> getDestination();
			
			if (MessageType::$INBOX == $_message->getMessageTypeID()){
				
				$Inbox = $this-> getInbox();
				$messageID = $Inbox-> Send($_message);
				
				$SentItems = $this-> getSentitems();
				$SentItems-> Store($messageID, $destination);
			}
			
			elseif (MessageType::$DRAFTS == $_message->getMessageTypeID()){
				
				$Drafts = $this-> getDrafts();
				$Drafts-> Send($_message);
			}
			
			elseif (MessageType::$INQUIRYBOX == $_message->getMessageTypeID()){

				$InquiryBox = $this-> getInquiryBox();
				$messageID = $InquiryBox-> Send($_message);

				$SentItems = $this-> getSentitems();
				$SentItems-> Store($messageID, $destination);
			}
		}
				
		/**
		 * Function name: saveAsDraft
		 * @param Message $_message
		 * @static
		 * Call functions and/instantiate classes based on value of Discriminator
		 */
		
		function saveAsDraft(Message $_message = NULL){			
				$Drafts = $this-> getDrafts();
				$Drafts-> Save($_message);
		}
		
		
		/**
		 * Function name: setTravelerID
		 * @param integer $_travID
		 */
		
		function setTravelerID ($_travID = 0){
			$this-> travID = $_travID;
		}
		
		/**
		 * Function name: setSendableID
		 * @param integer $_sendableID
		 */
		
		function setSendableID ($_sendableID = 0){
			$this-> sendableID = $_sendableID;
		}
		
		/**
		 * Function name: setUserName
		 * @param string $_username
		 */
		 
		function setUserName ($_username = ''){
			$this-> username = $_username;
		}
		
		/**
		 * Function name: setPassword
		 * @param string $_password
		 */
		 
		function setPassword ($_password = ''){
			$this-> password = $_password;
		}
		
		/**
		 * Function name: setNumberOfTravelers
		 * @param integer $_num
		 */
		 
		function setNumberOfTravelers ($_num = 0){
			$this-> numberOfTravelers = $_num;
		}
		
		/**
		 * Function name: setNumberOfFriends
		 * @param integer $_num
		 */
		 
		function setNumberOfFriends ($_num = 0){
			$this-> numberOfFriends = $_num;
		}
		
		/**
		 * Function name: setNumberOfGroupInvites
		 * @param integer $_num
		 */
		 
		function setNumberOfGroupInvites ($_num = 0){
			$this-> numberOfGroupInvites = $_num;
		}
				
		/** Function name: setNumberOfPendingFriends
		 * @param integer $_num
		 */
		
		function setNumberOfPendingFriends ($_num = 0){
			$this-> numberOfPendingFriends = $_num;
		}
						
		/** Function name: setNumberOfFriendRequests
		 * @param integer $_num
		 */
		 
		function setNumberOfFriendRequests ($_num = 0){
			$this-> numberOfFriendRequests = $_num;
		}
		
		/** Function name: setNumberOfGroupRequests
		 * @param integer $_num
		 */
		 
		function setNumberOfGroupRequests ($_num = 0){
			$this-> numberOfGroupRequests = $_num;
		}
		
		/** Function name: setNumberOfBlockedUsers
		 * @param integer $_num
		 */
		 
		function setNumberOfBlockedUsers ($_num = 0){
			$this-> numberOfBlockedUsers = $_num;
		}
		
		function setTravelerStatus($travelerStatus){
			$this->travelerStatus = $travelerStatus;
		}
		
		function getTravelerStatus(){
			return $this->travelerStatus;
		}
		
		/**
		 * Function name: setPresentLocation
		 * @param integer $_locationID
		 * Sets or updates the current location of the traveler
		 */
		 
		function setPresentLocation ($_locationID = 0){
			$this->rs4	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this-> presentLocation = $_locationID;
			
			$sqlQuery4 = "update tblTraveler set `currlocationID` = '$this->presentLocation' where `travelerID` = '$this->travID'";
			$this-> rs4-> Execute($sqlQuery4);
		}
		
		/**
		 * Function name: setAsAdvisor
		 * @param $_groupID
		 * @param $_clientID
		 */
		 
		function setAsAdvisor($_groupID = 0, $_clientID = 0){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sqlQuery = "insert into tblGrouptoAdvisor (`groupID`, `travelerID`, `clientID`) values ($_groupID, $this->travID, $_clientID)";
			$this-> rs-> Execute($sqlQuery);	
		}		
		
		/**
		 * Function name: updatePassword
		 * Updates traveler's password
		 */
		 
		function updatePassword(){
			$this->rs4	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sqlQuery4 = "update tblTraveler set `password` = '$this->password' where `travelerID` = '$this->travID'";
			$this-> rs4-> Execute($sqlQuery4);
			$this->invalidateCacheEntry();
		}
		
		/**
		 * Function name: requestFriendship
		 * @param Traveler $_traveler
		 * Adds row to tblFriendRequest
		 */
		 
		function requestFriendship($_friendID = 0){
			
			/*if (!$this->isAdvisor()){				
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				// check if friend is advisor
				$sqlQuery = "select distinct travelerID from tblGrouptoAdvisor where travelerID = " . $_friendID;
				$this-> rs-> Execute($sqlQuery);
				
				if (0 == $this->rs->Recordcount()){ // if friend is not an advisor
								
					//Adds row to tblFriendRequest				
					$sqlQuery = "insert into tblFriendRequest (`travelerID`, `friendID`) values ('$this->travID', '$_friendID')";
					$this-> rs-> Execute($sqlQuery);				
				}				
			}*/
			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			// check if not yet invited
			$sqlQuery = "SELECT *
						 FROM tblFriendRequest
						 WHERE (travelerID = '$this->travID' AND friendID = '$_friendID')
							OR (travelerID = '$_friendID' AND friendID = '$this->travID')
						 LIMIT 0,1";
			$this->rs->Execute($sqlQuery);
			
			if (0 == $this->rs->Recordcount()){ // if no yet invited	
				//Adds row to tblFriendRequest				
				$sqlQuery = "insert into tblFriendRequest (`travelerID`, `friendID`) values ('$this->travID', '$_friendID')";
				$this->rs->Execute($sqlQuery);				
			}
		}
		
		/**
		 * Function name: addInquiry
		 * @param Inquiry $_inquiry
		 * Adds traveler's inquiry to table
		 */
		 
		function addInquiry(Inquiry $_inquiry = NULL){						
			// Instantiate class Inquiry
			$Inquiry = $_inquiry;

			// Insert row to table tblInquiry
			$Inquiry-> SaveNew ();
		}
		
		/**
		 * Function name: addFavorite
		 * @param Favorite $_favorite
		 * Adds traveler's favorite to table
		 */
		 
		function addFavorite(Favorite $_favorite = NULL){
			
			// Instantiate class Favorite
			$Favorite = $_favorite;
			
			// Insert row to table tblFavorite
			$Favorite-> SaveNew ();
		}
				
		/**
		 * Function name: addFriend
		 * @param Traveler $_traveler
		 * Adds friend to tblFriend and deletes friendRequest from tblFriendRequest
		 */
		 
		function addFriend($_friendID = 0){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->rs3	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->rs4	=	new Recordset(ConnectionProvider::instance()->getConnection());
			
			//if (!$this->isAdvisor()){				
				// check if friend is advisor
				//$sqlQuery = "select distinct travelerID from tblGrouptoAdvisor where travelerID = " . $_friendID;
				//$this-> rs-> Execute($sqlQuery);
				
				//if (0 == $this->rs->Recordcount()){ // if friend is not an advisor
					$aDate = date("Y-m-d");
					
					// Adds friend to tblFriend and deletes friendRequest from tblFriendRequest
					
					$sqlQuery = "delete from tblFriendRequest where `travelerID` = '$_friendID' and `friendID` = '$this->travID'";
					$this-> rs-> Execute($sqlQuery);
					
					$sqlQuery2 = "insert into tblFriend (`travelerID`, `friendID`, `adate`) values ('$this->travID', '$_friendID', '$aDate')";
					$this-> rs2-> Execute($sqlQuery2);
					
					$sqlQuery3 = "insert into tblFriend (`travelerID`, `friendID`, `adate`) values ('$_friendID', '$this->travID', '$aDate')";
					$this-> rs3-> Execute($sqlQuery3);
					
					// Delete friend request of friend added
					
					$sqlQuery4 = "delete from tblFriendRequest where `travelerID` = '$this->travID' and `friendID` = $_friendID";
					$this-> rs4-> Execute($sqlQuery4);
				//}
			//}
		}
				
		/**
		 * Function name: addGroup
		 * @param Group $_group
		 * Adds group to tblGroup and tblGrouptoTraveler 
		 */
		 
		function addGroup(Group $_group = NULL){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			// Insert $_group to tblGroup
			$_group-> create();
			$groupID = $_group-> getGroupID();
			$dateTime = date("Y-m-d H:i:s");
			
			$sqlQuery = "insert into tblGrouptoTraveler (`groupID`, `travelerID`, `adate`) values ('$groupID', '$this->travID', '$dateTime')";
			$this-> rs-> Execute($sqlQuery);
		}
		
		/**
		 * Function name: deleteFavorite
		 * @param integer $_favoriteID
		 * Deletes traveler's favorite from table based on favorite ID
		 */
		 
		function deleteFavorite($_favoriteID = 0){
		
			// Deletes row in table tblFavorite according to favoriteID
			
			$favorite = new Favorite ();
			$favorite-> setFavoriteID ($_favoriteID);
			$favorite-> setTravelerID ($this-> travID);
			$favorite-> Delete ();
		}
		
		/**
		 * Function name: deleteInquiry
		 * @param integer $_inquiryID
		 * Deletes traveler's inquiry from table based on inquiry ID
		 */
		 
		function deleteInquiry($_inquiryID = 0){
		
			// Deletes row in table tblInquiry according to inquiryID
			
			$inquiry = new Inquiry ();
			$inquiry-> setInquiryID ($_inquiryID);
			$inquiry-> setTravelerID ($this-> travID);
			$inquiry-> Delete ();
		}
		
		/**
		 * Function name: deleteTravel
		 * @param integer $_travelID
		 * Deletes traveler's travel from table based on travel ID
		 */
		 
		function deleteTravel($_travelID = 0){
			
			// Deletes row in table tblTravel according to travelID
			
			$travel = new Travel ($this->travID, $_travelID);
			$travel-> Delete();
		}
		
		/**
		 * Function name: removeFriend
		 * @param integer $_friendID
		 * Deletes row from tblfriend where friendID = $_friendID
		 */
		 
		 function removeFriend($_friendID = 0){
		 	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 	// Deletes row from tblfriend where friendID = $_friendID and vice- versa
		 	
		 	$sqlQuery = "delete from tblFriend where `travelerID` = " . $this->travID . " and `friendID` = " . $_friendID;
		 	$this-> rs-> Execute($sqlQuery);
		 	
		 	$sqlQuery2 = "delete from tblFriend where `travelerID` = " . $_friendID . " and `friendID` = " . $this->travID;
		 	$this-> rs2-> Execute($sqlQuery2);
		 }
		
		/**
		 * Function name: removeGroup
		 * @param integer $_groupID
		 * Deletes row from tblGroup and tblGrouptoTraveler where groupID = $_groupID
		 */
		 
		 function removeGroup($_groupID = 0){
		 	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 	// Deletes row from tblGroup where groupID = $_groupID
			$mGroup = GroupFactory::instance()->create(array($_groupID));
			$mGroup[0]->Delete();

		 	// Deletes row from tblGrouptoTraveler where groupID = $_groupID
		 	$sqlQuery = "delete from tblGrouptoTraveler where `travelerID` = '$this->travID' and `groupID` = '$_groupID'";
		 	$this-> rs-> Execute($sqlQuery);
		 }
		
		/**
		 * Function name: cancelFriendRequest
		 * @param integer $_friendID
		 * Deletes row from tblFriendRequest where friendID = $_friendID
		 */
		
		function cancelFriendRequest($_friendID = 0){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			// Deletes row from tblFriendRequest where friendID = $_friendID
			
			$sqlQuery = "delete from tblFriendRequest where travelerID = $this->travID and friendID = $_friendID";
			$this-> rs-> Execute($sqlQuery);
		}
		
		/**
		 * Function name: rejectFriendRequest
		 * @param integer $_friendID
		 * Deletes row from tblFriendRequest where friendID = $_friendID
		 */
		 
		function rejectFriendRequest($_friendID = 0){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			// Deletes row from tblFriendRequest where friendID = $_friendID
			
			$sqlQuery = "delete from tblFriendRequest where travelerID = $_friendID and friendID =  $this->travID";
			$this-> rs-> Execute($sqlQuery);
		}
		
		/**
		 * Function name: blockUser
		 * @param integer $_friendID
		 * Sets blocked field in tblFriend to 1
		 */
		 
		 function blockUser($_friendID = 0){
		 	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 	// Removes blocked user as friend of the traveler
		 	
		 	if ($this->isFriend($_friendID))					// added by neri: 11-12-08
		 		$this->removeFriend($_friendID);
		 	
		 	// Inserts row in tblBlockedUsers
		 	
		 	$sqlQuery2 = "insert into tblBlockedUsers (`travelerID`, `userID`) values ($this->travID, $_friendID)";
		 	$this-> rs2-> Execute($sqlQuery2);
		 }
		 
		 /**
		 * Function name: unblockUser
		 * @param integer $_friendID
		 * Sets blocked field in tblFriend to 0
		 */

		 function unblockUser($_friendID = 0){
		 	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 	// Deletes row in tblBlockedUsers
		 	
		 	//$this->addFriend($_friendID);							// commented out by neri: 01-30-09						
		 	
		 	$sqlQuery = "delete from tblBlockedUsers where `travelerID` = $this->travID and `userID` = $_friendID";
		 	$this-> rs-> Execute($sqlQuery);
		 }
		
		/**
		 * Function name: isSuspended
		 * @return bool True|False
		 */
		
		function isSuspended(){
			return $this-> suspended;
		}
		 
		/**
		  * Function name: isOnline()
		  * @return bool True| False
		  */
		  
		function isOnline(){
			return $this-> online;
		}
		
		function setOnLine($online){
			$this-> online = $online;
		}
		
		function isActive(){
			return $this-> active;
		}
		
		/**
		 * Function name: isAdvisor
		 * @return bool
		 *
		 *	Last Edited By:	Miss CIG  24 November 2009
		 *	Purpose:	Fixed old, buggy query. If $groupID > 0, set groupID in the query equal to the variable.
		 *					So that, if no variable $groupID is passed it will just check if a traveler is an 
		 *					administrator regardless on what group.
		 */
		 
		function isAdvisor( $groupID = 0 )
		{
			/*$this->mIsAdvisor = (0 == $this->travID) ? false : $this->mIsAdvisor;
			
			if (null == $this->mIsAdvisor){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				// This data should be stored on the Traveler table instead 
				// of creating a separate query (expensive) just for this tiny info
				$sqlQuery = "select travelerID from tblGrouptoAdvisor where `travelerID` = " . $this->travID;
				$this->rs->Execute($sqlQuery);

				$this->mIsAdvisor = (0 < $this->rs->Recordcount()) ? true : false;
			}
			return $this->mIsAdvisor;*/
			$queryStr = "";
			$travelerID = $this->travID;

			// groupID argument is added to check if traveler is the administrator of a group,
			if ( 0 < $groupID )
			{
				$queryStr .= " AND groupID = " . $groupID . " ";
			}
			
			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT groupID " .
					"FROM tblGroup " .
					"WHERE discriminator > 1 " .
					"AND administrator = " . $travelerID . $queryStr .
					" LIMIT 1 ";
			$this->rs->Execute($sql);

			return (0 < $this->rs->RecordCount());
		}
		
		function getAdvisorGroup(){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			//$sqlQuery = "select groupID from tblGrouptoAdvisor where `travelerID` = " . $this->travID;
			$sqlQuery = "select groupID from tblGroup where parentID = 0 and discriminator > 1 and `administrator` = " . $this->travID;
			$this->rs->Execute($sqlQuery);
			
			if( 0 < $this->rs->Recordcount() ){
				$data =  mysql_fetch_assoc($this->rs->Resultset());
				return $data['groupID'];
			}
			return 0;
		}
		
		//added by Jul, to update isadvisor field of a traveler
		//isadvisor is updated once a parent advisor group is created or deleted
		function updateIsAdvisor(){
			$travelerID = $this->travID;
			$rs =	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT groupID
					FROM tblGroup
					WHERE administrator = '$travelerID'
						AND discriminator > 1
					LIMIT 0,1";
			$rs->Execute($sql);
			if( 0 < $rs->RecordCount() && 0 == $this->mIsAdvisor ){//still has at least one advisor group
				$sql2 = "UPDATE tblTraveler
						 SET isadvisor = 1
						 WHERE travelerID = '$travelerID'
							AND isadvisor = 0";
				$this->mIsAdvisor = 1;
				$this->getTravelerProfile()->setIsAdvisor($this->mIsAdvisor);
			
				$rs2 = new Recordset(ConnectionProvider::instance()->getConnection());
				$rs->Execute($sql2);
				$this->invalidateCacheEntry();
			}else if( 0 == $rs->RecordCount() && 1 == $this->mIsAdvisor ){//no more advisor group
				$sql2 = "UPDATE tblTraveler
						 SET isadvisor = 0
						 WHERE travelerID = '$travelerID'
							AND isadvisor = 1";
				$this->mIsAdvisor = 0;
				$this->getTravelerProfile()->setIsAdvisor($this->mIsAdvisor);
			
				$rs2 = new Recordset(ConnectionProvider::instance()->getConnection());
				$rs->Execute($sql2);
				$this->invalidateCacheEntry();
			}
		}
		
		/**
		 * Function name: isAdministrator
		 * added by: marc, Oct 31 2006
		 * @return bool
		 */
		 
		function isAdministrator(){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sqlQuery = "SELECT administrator FROM tblGroup WHERE `administrator` = $this->travID LIMIT 1";
			$this-> rs-> Execute($sqlQuery);
			
			if (0 == $this->rs->Recordcount())
				return false;
			else
				return true;
		}
		
		/**
		 * Function name: isBlocked
		 * @param integer $_userID
		 * @return bool
		 */
		 
		function isBlocked($_userID = 0){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sqlQuery = "select * from tblBlockedUsers where `travelerID` = $this->travID " .
					"and `userID` = $_userID";
			$this-> rs-> Execute($sqlQuery);
			
			if (0 == $this->rs->Recordcount())
				return false;
			else
				return true;
		}
		
		/**
		 * Returns true if the given traveler(in the form of its traveler ID) is a friend of the traveler.
		 * 
		 * @param integer $travelerID The ID of a traveler to be tested if it is a friend of this traveler. 
		 * 
		 * @return boolean true if the given traveler(in the form of its traveler ID) is a friend of the traveler.
		 */
		function isFriend($travelerID){
			// i don't think that we need to check if the traveler ID is an advisor.
			// we can do it before calling this method.
			// this is for refactoring purposes
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "SELECT COUNT(*) as count FROM tblFriend " .
				 " WHERE `travelerID` = ".$handler->makeSqlSafeString($this->travID)." " .
				 " AND `friendID` = ".$handler->makeSqlSafeString($travelerID)." ";
				
				$rs = new ResourceIterator($handler->execute($sql));
				
				return (0 == $rs->getCount()) ? false : true;
			}
			catch (exception $ex) {}  
			
			/*if (0 == $_travelerID)
				throw new exception ("Traveler ID does not exist!");

			if (!$this->isAdvisor()){
				// check if friend is advisor
				$sqlQuery = "select distinct travelerID from tblGrouptoAdvisor where travelerID = " . $_travelerID;
				$this->rs->Execute($sqlQuery);

				// if traveler is not an advisor, check friend
				if (0 == $this->rs->Recordcount()){
					$sqlQuery = "select travelerID from tblFriend where `travelerID` = $this->travID and `friendID` = $_travelerID";
					$this->rs->Execute($sqlQuery);
					
					if (0 < $this->rs->Recordcount())
						return true;
				}
			}
			return false;*/
		}
		
		
		function setIsSubGroup($_subgroup = false){
			$this-> subgroup = $_subgroup;
		}
		
		function isSubgroup(){
			return $this-> subgroup;
		}
		
		/**
		 * Function name: getName
		 * @return string
		 */
		
		function getName(){
			return $this->getUserName();
		}
		
		/**
		 * Function name: getTravelerID
		 * @return integer traveler's ID
		 */
		 
		function getTravelerID(){
			return $this-> travID;
		}
		
		/**
		 * Function name: getIsAdvisor
		 * @return integer determines if the traveler is an advisor of at least one advisor group
		 */
		 
		function getIsAdvisor(){
			return $this->mIsAdvisor;
		}
		
		/**
		 * Function name: getSendableID
		 * @return string username of traveler
		 */
		
		function getSendableID(){
			return $this-> sendableID;
		}
		
		/**
		 * Function name: getUserName
		 * @return string traveler's username
		 */
		 		
		function getUserName(){
			return $this-> username;
		}
		
		/**
		 * Function name: getPassword
		 * @return string traveler's password
		 */
		 
		function getPassword(){
			return $this-> password;
		}
		
		/**
		 * Function name: getNumberOfTravelers
		 * @return integer number of travelers
		 */
		 
		function getNumberOfTravelers(){
			return $this-> numberOfTravelers;
		}
				
		/**
		 * Function name: getNumberOfFriends
		 * @return integer number of friends
		 */
		 
		function getNumberOfFriends(){
			return $this-> numberOfFriends;
		}
		
		/** Function name: getNumberOfPendingFriends
		 * @return integer number of friend requests a traveler made
		 */
		 
		function getNumberOfPendingFriends(){
			return $this-> numberOfPendingFriends;
		}
		
		/**
		 * Added by: K. Gordo 8/30/07
		 * function to tell if we show the steps in using ga.net in passport section
		 * @return boolean 
		 * 
		 */
		 function getShowSteps(){
		 	return $this->showSteps;
		 }
		
		 /**
		 * Added by: K. Gordo 8/30/07
		 * function to show/not to show the steps in using ga.net 
		 * @param numeric 1 or 0
		 * 
		 */			
		 function setShowSteps($bol = 0){
		 	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 	$this->showSteps = $bol;
		 	
		 	$sql = "update tblTraveler set showsteps='$bol' where travelerID='$this->travID'";
		 	$this->rs->Execute($sql); 
		 }
		
		/**
		 * Function name: getNumberOfGroupInvites
		 * @return integer
		 */
		 
		function getNumberOfGroupInvites(){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			if (0 == $this->numberOfGroupInvites) {
				$sql = "select count(*) as cnt from tblInviteList where travelerID='$this->travID' and (status = 0 OR status = 2)";
				$this->rs->Execute($sql);
				
				$rsar = mysql_fetch_array($this->rs->ResultSet());
				$this->numberOfGroupInvites = $rsar['cnt'];
			}	
			
			return $this-> numberOfGroupInvites;
		}
		
		/** Function name: getNumberOfFriendRequests
		 * @return integer number of pending friend requests a traveler has
		 */
		 
		function getNumberOfFriendRequests(){
			return $this-> numberOfFriendRequests;
		}
		
		/** Function name: getNumberOfGroupRequests
		 * @return integer
		 */
		 
		function getNumberOfGroupRequests (){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			if (0 == $this->numberOfGroupRequests){
				$sql = "select count(*) as cnt from tblRequestList where travelerID='$this->travID' and status = 0";
				$this->rs->Execute($sql);
				
				$rsar = mysql_fetch_array($this->rs->ResultSet());
				$this->numberOfGroupRequests = $rsar['cnt'];
			}
			
			return $this-> numberOfGroupRequests;
		}
		
		/** Function name: getNumberOfDeniedGroupRequests (added by K. Gordo)
		 * @return integer
		 */
		 
		function getNumberOfDeniedGroupRequests (){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "select count(*) as cnt from tblRequestList where travelerID='$this->travID' and status = 1";
			$this->rs->Execute($sql);
			
			$rsar = mysql_fetch_array($this->rs->ResultSet());
			return $rsar['cnt'];			
		}
		
		
		/** Function name: getNumberOfBlockedUsers
		 * @return integer number of blocked users
		 */
		 
		function getNumberOfBlockedUsers (){
			return $this-> numberOfBlockedUsers;
		}
		
		/**
		 * Function name: getNumberOfJournals
		 * @return integer number of journals
		 */
		 
		function getNumberOfJournals(){
			return $this-> numberOfJournals;
		}
		
		/**
		 * Function name: getNumberOfTravelLogs
		 * @return integer number of travel logs
		 */
		 
		function getNumberOfTravelLogs(){
			return $this-> numberOfTravelLogs;
		}
		
		/**
		 * Function name: getBlockedUsers
		 * @return array|NULL|exception phpDocumentor array of Travelers or null or exception
		 */
		
		function getBlockedUsers($idonly=false){			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT tblTraveler.* " .
					"FROM tblTraveler, tblBlockedUsers " .
					"WHERE tblTraveler.travelerID = tblBlockedUsers.userID " .
					"AND tblBlockedUsers.travelerID = " . $this->travID.
					" GROUP BY tblBlockedUsers.userID";
			$this->rs->Execute($sql);

			$blockedUsers = array();
			$this-> setNumberOfBlockedUsers($this->rs->Recordcount());
			
			if (0 < $this->rs->Recordcount()){				
				while ($blocked = mysql_fetch_array($this->rs->Resultset())){
					if($idonly){
						$blockedUsers[] = $blocked['travelerID'];
					}else{
						$mTraveler = new Traveler;
						$mTraveler->setTravelerData($blocked);
						$blockedUsers[] = $mTraveler;
					}
				}
			}
			
			return $blockedUsers;
		}

		/**
		 * Function name: getInbox  
		 * @return Inbox
		 * Instantiates class Inbox
		 */		 
		function getInbox(){
			// Instantiate class Inbox			
			$my_message = new Inbox();
			$my_message->setSendableId($this->sendableID);
			$my_message->setInstance(1);

			$this->inbox = $my_message;
			return $this->inbox;
		}

		/**
		 * Function name: getSentitems  
		 * @return SentItems
		 * Instantiates class SentItems
		 */
		function getSentitems(){			
			// Instantiate class SentItems			
			$my_message = new SentItems();
			$my_message->setSendableId($this->sendableID);
			$my_message->setInstance(1);

			$this->sent = $my_message;
			return $this->sent;
		}

		/**
		 * Function name: getDrafts  
		 * @return Drafts
		 * Instantiates class Drafts
		 */		 
		function getDrafts(){	
			// Instantiate class Drafts
			$my_message= new Drafts();
			$my_message->setSendableId($this->sendableID);
			$my_message->setInstance(1);

			$this->drafts = $my_message;
			return $this->drafts;
		}
		
		/**
		 * Function name: getTrash  
		 * @return Trash
		 * Instantiates class Trash
		 */
		 
		function getTrash(){		
			// Instantiate class Trash			
			$my_message = new Trash();
			$my_message->setSendableId($this->sendableID);
			$my_message->setInstance(1);

			$this->trash = $my_message;
			return $this->trash;
		}
		
		/**
		 * Function name: getInquiry
		 * @return InquiryBox
		 * Instantiates class InquiryBox
		 */
		 
		function getInquiryBox(){			
			// Instantiates class Inquiry
			$my_message = new InquiryBox();
			$my_message->setSendableId($this->sendableID);
			$my_message->setInstance(1);

			$this->inquiryBox = $my_message;			
			return $this->inquiryBox; 
		}		
		
		/**
		 * Function name: setTravelerProfile  
		 * @param TravelerProfile object
		 */

		function setTravelerProfile($_profile = null){

			$this-> profile = $_profile;
		}
		
		/**
		 * Function name: getTravelerProfile  
		 * @return TravelerProfile
		 * Instantiates class TravelerProfile
		 */
		 
		function getTravelerProfile(){
			
			// Instantiate class TravellerProfile

			if ($this->profile == NULL){
				$my_profile = new TravelerProfile ($this->travID);
				$this->profile = $my_profile;
			}
			
			return $this->profile;
		}
		
		/**
		 * Function name: getTravels  
		 * @return array|NULL|exception phpDocumentor array of Travels or Null or exception
		 */
		 
		function getTravels($privacycriteria = 0, $_isOwner = 0){
			require_once('travellog/model2/travel/Class.ganetTravelRepo.php');
			require_once('travellog/vo/Class.ganetValueObject.php');
			
			$context	=	new ganetValueObject();
			$context->set('TRAVELER_ID',$this->travID);
			$context->set('IS_OWNER',$_isOwner);
			$filter		=	new ganetValueObject();
			$filter->set('PRIVACY_CRITERIA',$privacycriteria);
			$command	=	'TRAVELER';
			$vo			=	new ganetValueObject();
			$vo->set('COMMAND',$command);
			$vo->set('FILTER',$filter);
			$vo->set('CONTEXT',$context);
			
			$travelRepo	=	new ganetTravelRepo();
			$travels	=	$travelRepo->getList($vo);
			
			// viewer is not owner of travels, get only pushed journals with published entries
			/*if (0 == $_isOwner){
				$sqlQuery = "SELECT DISTINCT tblTravel.* " .
							"FROM tblTravel, tblTravelLog, tblTravelLink " .
							"WHERE tblTravelLog.travelID = tblTravel.travelID " .
							"AND tblTravelLink.travellinkID = tblTravel.travellinkID " .
							"AND tblTravel.publish = 1 " .
							"AND tblTravel.deleted = 0 " .
							"AND tblTravelLog.publish = 1 " .
							"AND tblTravelLink.reftype = 1 " .
							"AND tblTravelLink.`refID` = " . $this->travID;
				if (strlen($privacycriteria))
					$sqlQuery = $sqlQuery . " AND tblTravel.privacypref IN ( " . $privacycriteria . " )";
				$sqlQuery = $sqlQuery . " ORDER BY tblTravel.lastupdated DESC ";
			}
			else{
				$sqlQuery = "SELECT DISTINCT tblTravel.* " .
							"FROM tblTravel, tblTravelLink " .
							"WHERE tblTravelLink.travellinkID  = tblTravel.travellinkID " .
							"AND tblTravel.deleted = 0 " .
							"AND tblTravelLink.reftype = 1 " .
							"AND tblTravelLink.refID = " . $this->travID;

				if (strlen($privacycriteria))
					$sqlQuery = $sqlQuery . " AND tblTravel.privacypref IN ( " . $privacycriteria . " )";
				//$sqlQuery = $sqlQuery . " ORDER BY tblTravel.title DESC ";
				// modified by daf - 15Dec2008 ; change order from alphabetical to recently updated
				$sqlQuery = $sqlQuery . " ORDER BY tblTravel.lastupdated DESC ";
			}*/
			
			/*$this-> rs-> Execute($sqlQuery);

			$travels = array();

			// Instantiate class Travel for each travelID
			if (0 != $this->rs->Recordcount()){
				while ($travel = mysql_fetch_array($this->rs->Resultset())){
					try{
				      	// edited by K. Gordo (removed first param of the constructor)
						$tempTravel = new Travel ();
						$tempTravel->setTravelID($travel['travelID']);
						$tempTravel->setLocationID($travel['locID']);
						$tempTravel->setTitle($travel['title']); 
						$tempTravel->setDescription($travel['description']); 
						$tempTravel->setRating($travel['rating']); 
						$tempTravel->setVotes($travel['votes']); 
						$tempTravel->setViews($travel['viewed']); 
						$tempTravel->setLastUpdated($travel['lastupdated']);
						$tempTravel->setPrimaryphoto($travel['primaryphoto']);
						$tempTravel->setPrivacyPref($travel['privacypref']); 
						$tempTravel->setPublish($travel['publish']);
						$tempTravel->setIsFeatured($travel['isfeatured']);
						$tempTravel->setEntryCount($travel['entryCount']); 
						$tempTravel->setTravelerID($travel['travelerID']);
						$tempTravel->setTravelLinkID($travel['travellinkID']);
						$travels[] = $tempTravel;
					}
					catch (exception $e) {		
						throw $e;
					}
				}
			}*/

			return $travels;
		}
		
		function getCBTravels($privacycriteria = 0, $_isOwner = 0, $subjectID){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sqlGroup = "SELECT groupID 
						 FROM tblGroup
							 WHERE parentID = $subjectID 
								 OR groupID = $subjectID";
			// viewer is not owner of travels, get only pushed journals with published entries
			if (0 == $_isOwner){
				$sqlQuery = "SELECT DISTINCT tblTravel.* " .
							"FROM tblTravel, tblTravelLog, tblTravelLink, tblGroupApprovedJournals " .
							"WHERE tblTravelLog.travelID = tblTravel.travelID " .
							"AND tblTravel.travelID = tblGroupApprovedJournals.travelID " .
							"AND tblTravelLink.travellinkID = tblTravel.travellinkID " .
							"AND tblTravel.deleted = 0 " .
							"AND tblTravelLink.reftype = 1 " .
							"AND tblGroupApprovedJournals.approved = 1 " . 
							"AND tblGroupApprovedJournals.groupID IN (" . $sqlGroup . ") " .
							"AND tblTravelLink.`refID` = " . $this->travID;
				if (strlen($privacycriteria))
					$sqlQuery = $sqlQuery . " AND tblTravel.privacypref IN ( " . $privacycriteria . " )";
				$sqlQuery = $sqlQuery . " ORDER BY tblTravel.lastupdated DESC ";
			}
			else{
				$sqlQuery = "SELECT DISTINCT tblTravel.* " .
							"FROM tblTravel, tblTravelLink, tblGroupApprovedJournals " .
							"WHERE tblTravelLink.travellinkID  = tblTravel.travellinkID " .
							"AND tblTravel.travelID = tblGroupApprovedJournals.travelID " .
							"AND tblTravel.deleted = 0 " .
							"AND tblTravelLink.reftype = 1 " .
							"AND tblGroupApprovedJournals.approved = 1 " . 
							"AND tblGroupApprovedJournals.groupID IN (" . $sqlGroup . ") " .
							"AND tblTravelLink.refID = " . $this->travID;

				if (strlen($privacycriteria))
					$sqlQuery = $sqlQuery . " AND tblTravel.privacypref IN ( " . $privacycriteria . " )";
				//$sqlQuery = $sqlQuery . " ORDER BY tblTravel.title DESC ";
				// modified by daf - 15Dec2008 ; change order from alphabetical to recently updated
				$sqlQuery = $sqlQuery . " ORDER BY tblTravel.lastupdated DESC ";
			}
			
			$this-> rs-> Execute($sqlQuery);

			$travels = array();

			// Instantiate class Travel for each travelID
			if (0 != $this->rs->Recordcount()){
				while ($travel = mysql_fetch_array($this->rs->Resultset())){
					try{
				      	// edited by K. Gordo (removed first param of the constructor)
						$tempTravel = new Travel ();
						$tempTravel->setTravelID($travel['travelID']);
						$tempTravel->setLocationID($travel['locID']);
						$tempTravel->setTitle($travel['title']); 
						$tempTravel->setDescription($travel['description']); 
						$tempTravel->setRating($travel['rating']); 
						$tempTravel->setVotes($travel['votes']); 
						$tempTravel->setViews($travel['viewed']); 
						$tempTravel->setLastUpdated($travel['lastupdated']);
						$tempTravel->setPrimaryphoto($travel['primaryphoto']);
						$tempTravel->setPrivacyPref($travel['privacypref']); 
						$tempTravel->setPublish($travel['publish']);
						$tempTravel->setIsFeatured($travel['isfeatured']);
						$tempTravel->setEntryCount($travel['entryCount']); 
						$tempTravel->setTravelerID($travel['travelerID']);
						$tempTravel->setTravelLinkID($travel['travellinkID']);
						$travels[] = $tempTravel;
					}
					catch (exception $e) {		
						throw $e;
					}
				}
			}

			return $travels;
		}
		
		/**** ## Added By Naldz (June 06, 2008) ## ****/
		function getLastFeaturedDate(){
			require_once('gaLogs/Reader/Class.Reader.php');
			$reader = new Reader();
			$sql = 	'SELECT _EXECUTION_DATE_ as lastFeaturedDate '.
					'FROM Travel_Logs.tblFeaturedItems '.
					'WHERE _NEW_sectionID = 2 '.
					'AND _NEW_genID = '.GaString::makeSqlSafe($this->getTravelerID()).' '.
					'AND _COMMAND_ = "INSERT" '.
					'ORDER BY lastFeaturedDate DESC ';
			$rs = $reader->getLogsBySql($sql);
			if($rs->retrieveRecordCount() > 0){
				return $rs->getLastFeaturedDate(0);
			}
			return null;
		}

		/*** ##### Added By: Cheryl Ivy Q. Go	1 October 2008 ##### ***/
		function getDatesTravelerIsFeatured(){
			require_once('gaLogs/Reader/Class.Reader.php');
			$mReader = new Reader();
			$mDates = array();

			$sql = 	'SELECT _EXECUTION_DATE_ as lastFeaturedDate '.
					'FROM Travel_Logs.tblFeaturedItems '.
					'WHERE _NEW_sectionID = 2 '.
					'AND _NEW_genID = ' . GaString::makeSqlSafe($this->getTravelerID()) . ' ' .
					'AND _COMMAND_ = "INSERT" '.
					'ORDER BY lastFeaturedDate DESC ';
			$rs = $mReader->getLogsBySql($sql);

			if(0 < $rs->retrieveRecordCount())
				$mDates = $rs->retrieveColumn('lastFeaturedDate');

			return $mDates;
		}
		
		/**
		 * Function name: getFavorites  
		 * @return Favorite
		 * Instantiates class Favorite
		 */
		 
		function getFavorites(){
			
			// Instantiate class Favorite
			
			if ($this-> favorites == NULL){
				
				$Favorite = new Favorite ();
				$Favorite-> setTravelerID ($this->travID);
				$this-> favorites = $Favorite;
			}
			
			return $this-> favorites;
		}
		
		/**
		 * Function name: getInquiry  
		 * @return Inquiry
		 * Instantiates class Inquiry
		 */
		 
		function getInquiry(){
			
			// Instantiate class Inquiry
			
			if ($this-> inquiry == NULL){				
				
				$Inquiry = new Inquiry ();
				$Inquiry-> setTravelerID ($this->travID);
				$this-> inquiry = $Inquiry;
			}
			
			return $this-> inquiry;
		}
		
		/**
		 * Retrieves the number of groups of this traveler
		 * 
		 * @param string $groupName limits the group to be retrieve so that only group that has name LIKE %$groupName% will be retrieve
		 * @return integer
		 */
		public function getGroupsCount($groupName = ""){
			$handler = new dbHandler();
			$mCondition = "";
			
			if (!$this->isSubGroup())
				$mCondition = " and tblGroup.parentID = 0";

			$sql = "SELECT count(DISTINCT tblGroup.groupID) AS count " .
			 			"FROM tblGrouptoTraveler, tblGroup " .
						"WHERE tblGroup.groupID = tblGrouptoTraveler.groupID " .
						"AND tblGroup.isSuspended = 0 " .
						"AND tblGroup.name LIKE " .
						"".$handler->makeSqlSafeString("%".$groupName."%")."".
						"AND tblGrouptoTraveler.travelerID = " . $this->travID . $mCondition;
			
			$result = $handler->execute($sql);
			$rs = new iRecordset($result);
			$rec = $rs->current();
			
			return $rec['count'];			
		}
		
		/**
		 * Function name: getGroups
		 * @return Group
		 * gets list of Groups the traveler has
		 */
		 
		function getGroups($_filterCriteria2 = NULL, $_rowslimit = NULL, $groupName = ""){
			if (!$this->isSubGroup())
				$mCondition = " and tblGroup.parentID = 0";
			else
				$mCondition = "";

			if ($_filterCriteria2){				
				$myConditions = $_filterCriteria2->getConditions();
	 			$mAttname = $myConditions[0]->getAttributeName();
				$mOperation = $myConditions[0]->getOperation();

	 			switch ($mOperation){
	 				case FilterOp::$ORDER_BY :
	 					$mCondition = $mCondition . " ORDER BY " . $mAttname;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$mCondition = $mCondition . " ORDER BY " . $mAttname . " DESC " ;
	 					break;
	 			}
			}
			else
				$mCondition = $mCondition . " ORDER BY `name` ASC ";
			
			if ($_rowslimit){	 			
	 			$mOffset = $_rowslimit->getOffset();
	 			$mRows =  $_rowslimit->getRows();
	 			$mLimitStr = " 	LIMIT " . $mOffset . " , " . $mRows;
	 		}	
	 		else
	 			$mLimitStr = "";
		
		 $handler = new dbHandler();
			
			// Get groups a traveler belongs to			
			$sql = "SELECT DISTINCT tblGroup.groupID AS groupID " .
			 			"FROM tblGrouptoTraveler, tblGroup " .
						"WHERE tblGroup.groupID = tblGrouptoTraveler.groupID " .
						"AND tblGroup.isSuspended = 0 " .
						"AND tblGroup.name LIKE " .
						"".$handler->makeSqlSafeString("%".$groupName."%")."".
						"AND tblGrouptoTraveler.travelerID = " . $this->travID . $mCondition . $mLimitStr;
			
			//echo $sql."<br />";
			//$this->rs->Execute($sql);
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->execute($sql);
			
			$arrGroup	= array();
			$arrGroupId	= array();

			if (0 < $this->rs->Recordcount()){
				while($mGroup = mysql_fetch_array($this->rs->Resultset()))
					$arrGroupId[] = $mGroup['groupID'];
				$arrGroup = GroupFactory::instance()->create($arrGroupId);
			}

			return $arrGroup;
		}
		
		/**
		 * Function name: getPresentLocation  
		 * @return Country|Region|City|NonPopLocation|NULL|exception
		 */
		 
		function getPresentLocation(){			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			if ($this->presentLocation == NULL){
				
				$sqlQuery = "select `currlocationID` from tblTraveler where `travelerID` = '$this->travID'";
				$this-> rs-> Execute($sqlQuery);
				
				$this-> locationID = $this->rs->Result(0, "currlocationID");			
				$factory = LocationFactory::instance();
				
				try {		
					$this-> presentLocation = $factory-> create ($this-> locationID);
				} 
				catch (exception $e){
					//throw $e; // modified by : Aldwin S. Sabornido
					$this-> presentLocation = NULL;
				}
			}
			
			return $this-> presentLocation;
		}
		
		function getLocationID(){
			return $this->locationID;
		}
		
		/**
		 * Function name: getOtherTravelersInPresentLocation  
		 * @return array|NULL|exception phpDocumentor array of Traveler objects or Null or exception
		 */
		 
		function getOtherTravelersInPresentLocation($_filtercriteria = null, $_rowslimit = null, $_random = false){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
			
			if ($_filtercriteria){
			
				$myconditions = $_filtercriteria-> getConditions();
	 			$attname = $myconditions[0]-> getAttributeName();
				$operation = $myconditions[0]-> getOperation();
				
	 			switch ($operation){
	 				case FilterOp::$ORDER_BY :
	 					$condition = " ORDER BY " . $attname ;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$condition = " ORDER BY " . $attname . " DESC " ;
	 					break;
	 			}
			}
			else{
				if ($_random)
					$condition = " ORDER BY RAND() ";
				else
					$condition = " ORDER BY latestlogin DESC";
			}

		 	if ($_rowslimit){
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$limitstr = " 	LIMIT " . $offset . " , " . $rows;
	 		}	
	 		else
	 			$limitstr = "";

			$sql = "SELECT SQL_CALC_FOUND_ROWS travelerID, username, password, currlocationID, sendableID, isSuspended, active, showsteps " .
			 		"FROM tblTraveler " .
					"WHERE currlocationID = " . $this->locationID . " " .
					"AND travelerID <> " . $this->travID . " " .
					"AND travelerID NOT IN " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) " .
					$condition . $limitstr;
			$this->rs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this->rs2->Execute($sql2);

			$travelersPresentLoc = array();
			$this->setNumberOfTravelers($this->rs2->Result(0, "totalRecords"));

			if (0 < $this->rs2->Result(0, "totalRecords")){
				while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					$mTraveler = new Traveler;
					$mTraveler->setTravelerData($traveler);
					$travelersPresentLoc[] = $mTraveler;
				}
			}

			return $travelersPresentLoc;
		}
		
		/**
		 * Function name: getOtherTravelersInMyHomeTown  
		 * @return array|NULL|exception phpDocumentor array of Traveler objects or Null or exception
		 */
		 
		/**********************************************************************************
		 * edits of neri to getOtherTravelersInMyHomeTown():
		 *		added loggedTravelerID so as not to display certain travelers' 
		 *			profiles to blocked users: 									11-14-08 	
		 *		checked if traveler is suspended:								12-17-08	
		 **********************************************************************************/ 
		 
		function getOtherTravelersInMyHomeTown($_filtercriteria = NULL, $_rowslimit = NULL, $loggedTravelerID = 0){			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$travelersHometown = array();
			$htLocation = $this->getTravelerProfile()->getHTLocationID();
			
			if ($_filtercriteria){
				$myconditions = $_filtercriteria-> getConditions();
	 			$attname = $myconditions[0]-> getAttributeName();
				$operation = $myconditions[0]-> getOperation();
				
	 			switch ($operation){
	 				case FilterOp::$ORDER_BY :
	 					$condition = " ORDER BY " . $attname ;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$condition = " ORDER BY " . $attname . " DESC " ;
	 					break;
	 			}
			}
			else
				$condition = " ORDER BY latestlogin DESC";
		 	
			if ($_rowslimit){
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$limitstr = " 	LIMIT " . $offset . " , " . $rows;
	 		}	
	 		else
	 			$limitstr = "";
			
			$sql = "SELECT SQL_CALC_FOUND_ROWS * " .
					"FROM tblTraveler " .
					"WHERE htlocationID = " . $htLocation . " " .
					"AND travelerID <> " . $this->travID . " " .
					"AND active > 0 " .
					"AND travelerID NOT IN " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) " .
					"GROUP BY travelerID " .
					$condition . $limitstr;
			$this->rs->Execute($sql);
			
			$sql2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this->rs2->Execute($sql2);

			$this->setNumberOfTravelers($this->rs2->Result(0, "totalRecords"));

			if (0 < $this->rs2->Result(0, "totalRecords")){
				while ($Traveler = mysql_fetch_array($this->rs->Resultset())){
					$mTraveler = new Traveler;
					$mTraveler->setTravelerData($Traveler);
					
					if (!$mTraveler->isSuspended()) {
						if (0 < $loggedTravelerID && !$mTraveler->isBlocked($loggedTravelerID))	
							$travelersHometown[] = $mTraveler;
						else if (0 == $loggedTravelerID)
							$travelersHometown[] = $mTraveler;
					}
				}
			}

			return $travelersHometown;		
		}
		
		/**
		 * Function name: getCountTravelJournals  
		 * @return integer travel journals
		 * @param $includeAll - if include all journals from traveler regardless if in cobrand view or not
		 */
		 
		function getCountTravelJournals($includeAll=false){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			
			if(isset($GLOBALS['CONFIG']) && !$includeAll)
				$sqlQuery = 'select COUNT(tblTravel.travelID) as travelID from tblTravel, tblTravelLink, tblGroupApprovedJournals where tblTravel.travellinkID = tblTravelLink.travellinkID AND tblTravelLink.reftype = 1 AND tblTravelLink.refID = '. $this->travID .' and entryCount > 0 and deleted = 0 and tblTravel.travelID=tblGroupApprovedJournals.travelID and tblGroupApprovedJournals.groupID = '.$GLOBALS['CONFIG']->getGroupID().' and tblGroupApprovedJournals.approved = 1';
			else
				$sqlQuery = 'select COUNT(travelID) as travelID from tblTravel, tblTravelLink where tblTravel.travellinkID = tblTravelLink.travellinkID AND tblTravelLink.reftype = 1 AND tblTravelLink.refID = '. $this->travID .' and  entryCount > 0 and deleted = 0';
			
			$this-> rs-> Execute($sqlQuery);
			
			$this-> numberOfJournals = $this->rs->Result(0, "travelID");
			
			return $this-> numberOfJournals;
		}
		
		/**
		 * Function name: getCountTravelLogs  
		 * @return integer travel logs
		 */
		 
		function getCountTravelLogs(){
			return ($this-> numberOfTravelLogs = TravelLog::getCountTravelLogs($this->travID));
		}
		
		/**
		 * Function name: getPresentTravel  
		 * @ignore
		 */
		 
		function getPresentTravel(){
			
			// For future use 
		}
		
		/**
		 * Function name: getFriends
		 * @return array|NULL|exception phpDocumentor array of Traveler objects or Null or exception
		 */
		
		function getFriends($_filtercriteria = NULL, $_rowslimit = NULL, $prioritizeFriendsWithPhoto=FALSE){			
			
			require_once('travellog/model2/traveler/Class.ganetTravelerRepo.php');
			
			if ($_rowslimit){
				$_filtercriteria->setStartRow($_rowslimit->getOffset());
				$_filtercriteria->setRowsPerPage($_rowslimit->getRows());	
			}
			
			$command	=	$prioritizeFriendsWithPhoto ? 'TRAVELER_FRIENDS_PRIORITIZE_WITH_PHOTO': 'TRAVELER_FRIENDS';
			$filter		=	new ganetValueObject();
			$filter->set('FILTER_CRITERIA',$_filtercriteria);
			$context	=	new ganetValueObject();
			$context->set('TRAVELER_ID',$this->travID);
			$vo	=	new ganetValueObject();
			$vo->set('CONTEXT',$context);
			$vo->set('COMMAND',$command);
			$vo->set('FILTER',$filter);
			
			$travelerRepo	=	new ganetTravelerRepo();
			return $travelerRepo->getList($vo);
			
		/*	if ($_filtercriteria){				
				$myconditions = $_filtercriteria-> getConditions();
	 			$attname = $myconditions[0]-> getAttributeName();
				$operation = $myconditions[0]-> getOperation();

	 			switch ($operation){
	 				case FilterOp::$ORDER_BY :
	 					$condition = " ORDER BY " . $attname ;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$condition = " ORDER BY " . $attname . " DESC " ;
	 					break;
	 			}
			}			
			else
				$condition = " ORDER BY username";
		 	
		 	if ($_rowslimit){
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$limitstr = " 	LIMIT " . $offset . " , " . $rows;
	 		}	 		
	 		else
	 			$limitstr = "";
	 		
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblFriend.friendID, tblTraveler.* " .
					"FROM tblFriend, tblTraveler " .
					"WHERE tblTraveler.travelerID = tblFriend.friendID " .
					"AND tblFriend.travelerID = $this->travID " .
					"AND tblFriend.friendID NOT IN " .
						"(select distinct travelerID from tblGrouptoAdvisor) " .
					$condition . $limitstr;
			$this->rs->Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$this->rs2->Execute($sql2);

			$friends = array();
			$this-> setNumberOfFriends($this->rs2->Result(0, "totalRecords"));

			if (0 < $this->rs2->Result(0, "totalRecords")){				
				while ($friend = mysql_fetch_array($this->rs->Resultset())){
						$obj_traveler = new Traveler;
						$obj_traveler->setTravelerData($friend);
						$friends[$friend['friendID']] = $obj_traveler;
				}
			}
				
			return $friends;*/
		}
		
		/**
		 * Function name: getFriendRequests
		 * @return array|NULL|exception phpDocumentor array of Traveler objects or Null or exception
		 * Gets list of friends that invited the traveler
		 */		 
		function getFriendRequests(){			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT DISTINCT tblTraveler.* " .
					"FROM tblTraveler, tblFriendRequest " .
					"WHERE tblTraveler.travelerID = tblFriendRequest.travelerID " .
					"AND tblFriendRequest.friendID = '$this->travID' ".
					"AND tblTraveler.active = 1 ".
					"AND tblTraveler.isSuspended = 0 ".
					"AND tblTraveler.deactivated = 0 ";
			$this->rs->Execute($sql);

			$friendRequests = array();
			$this->setNumberOfFriendRequests($this->rs->Recordcount());

			if (0 < $this->rs->Recordcount()){
				while ($friend = mysql_fetch_array ($this->rs->Resultset())){
					$mTraveler = new Traveler;
					$mTraveler->setTravelerData($friend);
					$friendRequests[] = $mTraveler;										
				}
			}

			return $friendRequests;
		}
	
		function getSuspendedFriendRequests(){			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT DISTINCT tblTraveler.* " .
					"FROM tblTraveler, tblFriendRequest " .
					"WHERE tblTraveler.travelerID = tblFriendRequest.travelerID " .
					"AND tblFriendRequest.friendID = '$this->travID' ".
					"AND tblTraveler.active = 1 ".
					"AND tblTraveler.isSuspended = 1 ".
					"AND tblTraveler.deactivated = 0 ".
					"AND isAvailableRequest = 1 ";
			$this->rs->Execute($sql);

			$suspendedFriendRequests = array();
	
			if (0 < $this->rs->Recordcount()){
				while ($friend = mysql_fetch_array ($this->rs->Resultset())){
					$mTraveler = new Traveler;
					$mTraveler->setTravelerData($friend);
					$suspendedFriendRequests[$friend['travelerID']] = $mTraveler;										
				}
			}

			return $suspendedFriendRequests;
		}
		
		function removeUnavailableFriendRequests(){			
		
			$arrSuspendedFriendRequestsTravID = array_keys($this->getSuspendedFriendRequests());
			if (count($arrSuspendedFriendRequestsTravID))
				$travIDList = implode(",",$arrSuspendedFriendRequestsTravID );
			else
				$travIDList = 0;
				
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "UPDATE  tblFriendRequest " .
					"SET isAvailableRequest = 0 " .
					"WHERE tblFriendRequest.friendID = '$this->travID' ".
					"AND tblFriendRequest.travelerID IN (" . $travIDList. ") ";
			$this->rs->Execute($sql);
		
		}
		
		/**
		 * Function name: getGroupRequests
		 * @return array|NULL|exception phpDocumentor array of Group objects or Null or exception
		 * Gets the list of groups that the traveler invited
		 */
		 
		function getGroupRequests($_filterCriteria2 = NULL){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$condition = '';
			
			if (NULL != $_filterCriteria2){
			
				$filterCondition = $_filterCriteria2-> getConditions();
				$attribute = $filterCondition[0]-> getAttributeName();
				$value     = $filterCondition[0]-> getValue();
				
				switch($filterCondition[0]->getOperation()){
    				
    				case FilterOp::$EQUAL:
    					$operator	= " = ";
    					break;
    				case FilterOp::$NOT_EQUAL:
    					$operator   = " <> ";
    					break;
					case FilterOp::$IN:			//added bu Jul, 2008-11-19
						$operator	= " IN ";
						break;
    				default:
    					$operator   = " = ";
    					break;
    			}
    			
				$condition = " AND " .$attribute . $operator . $value;
			}
			
						
			$sqlQuery = "select distinct `groupID` from tblRequestList where `travelerID` = $this->travID " . $condition;
			$this-> rs-> Execute($sqlQuery);

			$arrGroupId = array();
			$groupRequests = array();
			$this->setNumberOfGroupRequests ($this->rs->Recordcount());
			
			if (0 < $this->rs->Recordcount()){
				while ($groupReqs = mysql_fetch_array ($this->rs->Resultset()))
					$arrGroupId[] = $groupReqs['groupID'];

				$groupRequests = GroupFactory::instance()->create($arrGroupId);
			}

			return $groupRequests;
		}
		
		/**
		 * Function name: getGroupInvites
		 * @return array|NULL|exception phpDocumentor array of Group objects or Null or exception
		 * Gets the list of groups that invited the traveler
		 */
		 
		function getGroupInvites(){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());			
			$sqlQuery = "select distinct `groupID` from tblInviteList where `travelerID` = $this->travID and (status = 0 OR status = 2)";
			$this-> rs-> Execute($sqlQuery);

			$arrGroupId = array();
			$groupInvites = array();
			$this-> setNumberOfGroupInvites($this->rs->Recordcount());
			
			if (0 < $this->rs->Recordcount()){
				while ($groupInvite = mysql_fetch_array ($this->rs->Resultset()))
					$arrGroupId[] = $groupInvite['groupID'];

				$groupInvites = GroupFactory::instance()->create($arrGroupId);
			}	

			return $groupInvites;
		}
		
		/**
		 * added by: Jul
		 * date added: 2008-11-19
		 * purpose: to fetch group membership invites (excluding staff/super staff inivitation) by a main group and it's subgroups for this traveler
		 * @param FilterCriteria2 $_filterCriteria2 an instance of FilterCriteria2
		 * @return array of AdminGroup objects
		 */
		function getGroupMembershipInvites($_filterCriteria2){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$condition = '';
			
			if (NULL != $_filterCriteria2){
			
				$filterCondition = $_filterCriteria2-> getConditions();
				$attribute = $filterCondition[0]-> getAttributeName();
				$value     = $filterCondition[0]-> getValue();
				
				switch($filterCondition[0]->getOperation()){
    				
    				case FilterOp::$EQUAL:
    					$operator	= " = ";
    					break;
    				case FilterOp::$NOT_EQUAL:
    					$operator   = " <> ";
    					break;
					case FilterOp::$IN:			//added bu Jul, 2008-11-19, in this case, the list value must have already been placed inside parenthesis
						$operator	= " IN ";
						break;
    				default:
    					$operator   = " = ";
    					break;
    			}
    			
				$condition = " AND " .$attribute . $operator . $value;
			}
			
			$sqlQuery = "SELECT DISTINCT `groupID`
						FROM tblInviteList
						WHERE 1=1 AND status = 0 $condition";
			$this->rs-> Execute($sqlQuery);

			$arrGroupId = array();
			$groupInvites = array();
			$this-> setNumberOfGroupInvites($this->rs->Recordcount());
			
			if (0 < $this->rs->Recordcount()){
				while ($groupInvite = mysql_fetch_array ($this->rs->Resultset()))
					$arrGroupId[] = $groupInvite['groupID'];

				$groupInvites = GroupFactory::instance()->create($arrGroupId);
			}	

			return $groupInvites;
		}
		
		/**
		 * Function name: getFriendRequests
		 * @return array|NULL|exception phpDocumentor array of Traveler objects or Null or exception
		 * Gets the list of friends that the traveler invited
		 */
		 
		function getPendingRequests(){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			/*$sql = "SELECT tblTraveler.* " .
					"FROM tblTraveler, tblFriendRequest " .
					"WHERE tblTraveler.travelerID = tblFriendRequest.friendID " .
					"AND tblFriendRequest.travelerID = $this->travID " .
					"AND tblFriendRequest.friendID NOT IN " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor)";*/
			$sql = "SELECT tblTraveler.* " .
				"FROM tblTraveler, tblFriendRequest " .
				"WHERE tblTraveler.travelerID = tblFriendRequest.friendID " .
				"AND tblFriendRequest.travelerID = '$this->travID' ".
				"AND tblTraveler.active = 1 ".
				"AND tblTraveler.isSuspended = 0 ";
			$this->rs->Execute($sql);

			$pendingRequests = array();
			$this-> setNumberOfPendingFriends($this->rs->Recordcount());
			
			if (0 < $this->rs->Recordcount()){
				while($friend = mysql_fetch_array($this->rs->Resultset())){				
					try{
						$mTraveler = new Traveler;
						$mTraveler->setTravelerData($friend);
						$pendingRequests[] = $mTraveler;
					}					
					catch (exception $e){
						throw $e;
					}
				}

				return $pendingRequests;
			}
		}
		
		/*****************************
		 * getPrivacyPreference 
		 *    added by K. Gordo
		 */
		 
		function getPrivacyPreference(){
			if($this->travID == 0){
				$pp = new PrivacyPreference();
			 	$pp-> setTravelerID($this->travID);
			 	return $pp;
			}else{
				return new PrivacyPreference($this->travID);
			}
			// commented out redundant code. - jul	
			// 		 	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			// $sqlQuery = "SELECT * FROM tblPrivacyPreference where travelerID = $this->travID";
			// 		 	$this-> rs-> Execute($sqlQuery);
			// 	
			// 		 	$pp = new PrivacyPreference();
			// 		 	$pp-> setTravelerID($this->travID);
			// 		 	
			// 		 	if (0 == $this->rs->Recordcount()) 	 		
			// 		 		return $pp;
			// 		 			 			 	
			// 		 	// there is a record so we instantiate PrivacyPreference with travelerID param
			// 		 	return new PrivacyPreference($this->travID);		 	
		 	
		}

		/**
		 * Function name: getFriendshipDate
		 * @param integer $_friendID
		 * @return date
		 */

		function getFriendshipDate($_friendID = 0){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			if (NULL == $this->friendshipDate){
				
				$sqlQuery = "select distinct `adate` from tblFriend where travelerID = ". $this->travID .
						" and friendID = " . $_friendID;
				$this-> rs-> Execute($sqlQuery);
				
				if (0 == $this->rs->Recordcount())
					throw new exception("Travelers not yet friends!");
				else
					$this->friendshipDate =  $this->rs->Result(0, "adate");
			}
			
			return $this->friendshipDate;
		}	
		
		/*****************************
		 * addTravelerEvent
		 * 		added by K. Gordo
		 */
		 
		 function addTravelerEvent($travelerEvent) {
		 		$travelerEvent->setTravelerID($this->travID);          
                $travelerEvent->Create();                                 
		 }
		 
		 
		 function removeTravelerEvent($travelerEventID){
                
                try {
                	$travelerEvent = new TravelerEvent($travelerEventID);                  
                	$travelerEvent->Delete();
                } 
                
                catch (Exception $e) {}	                                       
            }
		 
		 /*****************************
		 * getTravelerEvents
		 * 		added by K. Gordo
		 */
		  
		 function getTravelerEvents($rowslimit = null, $startdate = null) {
		 	    $this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());                
                if (NULL != $rowslimit){
                    $offset   = $rowslimit->getOffset();
                    $rows     = $rowslimit->getRows();
                    $limitstr = " LIMIT " . $offset . " , " . $rows;
                }  
                else
                    $limitstr  = '';
                 
                $condition = "";

                if (NULL != $startdate){
                	//$startdate = date('Y-m-d');				// if startdate not given we default to the current date
                	$condition = " and adate >= '" . $startdate . "'";
                }
                    
                $sql = "SELECT travelereventID from tblTravelerEvent WHERE travelerID = '$this->travID'" . $condition . " ORDER BY adate " . $limitstr;
                $this-> rs-> Execute($sql);
                
                if (0 == $this->rs->Recordcount()){
                    return NULL;            
                }                    
                else {
                    while ($recordset = mysql_fetch_array($this->rs->Resultset())) {
                        try {
                            $travelerevent = new TravelerEvent($recordset['travelereventID']);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                        $this->travelerEvents[] = $travelerevent;
                    }
                }               
        
            
            return $this->travelerEvents;
		 }
		 
		/**
		 * Function name: transferAdvisorMessages
		 */
		
		function transferAdvisorMessages(){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$arrMessages = array();
			$arrListings = array();
						
			$clientID = AdminGroup::getAdvisorClientID($this->travID);
			$arrListings = listing::getClientListings($clientID);
			
			// edited by marc, Nov 3
			if( count($arrListings) ){
				$lstListings = implode(",", $arrListings);
			
				// get messageIDs of messages of Advisor
				
				$sqlQuery = "select distinct b.`messageID` from tblMessages as a, tblMessageToAttribute as b, tblMessageToInquiry as c where a.`discriminator` = 4 and ".
						"b.`messageType` = 4 and b.`recipientID` = 0 and c.`listingID` in ($lstListings) and a.`messageID` = c.`messageID` and b.`messageID` = a.`messageID`";
				$this->rs->Execute($sqlQuery);
				
				while($Message = mysql_fetch_array($this->rs->Resultset()))
					$arrMessages[] = $Message['messageID'];
				$lstMessages = implode(",", $arrMessages);
						
				
				$sqlQuery2 = "update tblMessageToAttribute set `recipientID` = $this->sendableID where `messageID` in ($lstMessages)";
				$this->rs2->Execute($sqlQuery2);	
			}
			
		}
		
		/*
		 * function name: exists
		 * by: marc
		 * feb 13, 2007
		 * checks if traveler exists based on email
		 * arguments: 
		 * 		strFind => string to find
		 * returns travelerID if match has been found or 0 otherwise
		 */
		
		public static function exist($strFind){
			
			$conn = ConnectionProvider::instance()->getConnection();
			$rs = new Recordset($conn);
			
			// edited by marc
			$rs->Execute("SELECT travelerID " .
					"FROM tblTraveler " .
					"WHERE `email` = '" . trim($strFind) . "'");
			
			if( 0 == $rs->Recordcount())
				return 0;
			else{
				$tmpTrav = mysql_fetch_array($rs->ResultSet());
				return $tmpTrav["travelerID"];
			}	
			
		}
		
		/**############################################
	     * Get Countrys travelled by Traveler based on journal entry created
	     * @param  NULL
	     * @return  Country Object
	     * @access public
	     * by Joel C. Llano
	     ###########################################*/
	     
	     public function getCountrysTravelled(){
	     	
	     	$countrystravelled	= array();
	     	$countryids			= array();
	     	
	     	/*
	     	$tmptr = $this->getTravels();
	     		
	     	echo "<pre>";
	     		print_r($tmptr[0]);  
	     	echo "</pre>";
	     	
	     	exit;
	     	*/

			// last edited by cheryl go		14 March 2008
			// purpose: added parameter in getTravels function
	     	if(count($this->getTravels())){
	     		foreach($this->getTravels() as $travel){
	     			$trip = $travel->getTrips();
						if(count($trip)){     				
		     				foreach($trip as $trip){
		     					$countryId = $trip->getLocation()->getCountry()->getCountryID();
		     					if(!in_array($countryId,$countryids)){
		     						$countryids[]			= $trip->getLocation()->getCountry()->getCountryID();
		     						$countrystravelled[] 	= $trip->getLocation()->getCountry();	     					
		     					}
		     				}
						}
	     			
	     		}		
	     	}
	     	
	     	return $countrystravelled;
	     	
	     }
		
		/**###########################
	     * check if traveler completed his/her profile 
	     * @param  NULL
	     * @return  boolean
	     * @access public
	     * by Joel C. Llano
	     ###########################*/		
		public function isProfileCompleted($includePersonals=true){			
			
			$profile = new TravelerProfile($this->travID);
			$isCompleted = false;				
			if($includePersonals){	
				if($profile->getFirstname() && $profile->getLastname() && $profile->getEmail()
						&& strtotime($profile->getBirthDay()) != 943891200	&& $profile->getHTLocationID() /*&&	$profile->getTravelerPupose()*/ 						
						&& $profile->getTravelerTypeID() && $profile->getGender()
						&& $profile->getInterests() /*&& $profile->getShortBio()*/ && $profile->getCulture()
						&& $profile->getTravelerProgramInterested() && $profile->getTravelerCountriesInterested() 
						
						//&& ($this->getCountrysTravelled() || $profile->getTravelerCountriesTravelled())
						
						&& ($profile->getOtherprefertravel() || $profile->getTravelerPrefferedTravelType())     
						&& $profile->getIncome() ){
							
				    	$isCompleted = true;		    
				    
				}
			}else{
				if($profile->getFirstName() && $profile->getLastName() && $profile->getEmail()
						&& strtotime($profile->getBirthDay()) != 943891200	&& $profile->getHTLocationID() && $profile->getGender()){
							
				    	$isCompleted = true;		    
				}
			}	
			return $isCompleted;
		}	
	
		public function getTravelerType(){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql  = 'SELECT travelertype FROM tblTraveler,tblTravelerType ' .
					'WHERE tblTraveler.travelertypeID = tblTravelerType.travelertypeID ' .
					'AND tblTraveler.travelerID = '.$this->travID;
			$this->rs->Execute($sql);

			if( $this->rs->Recordcount() )
				return $this->rs->Result(0,"travelertype");
		}
		
		/**###########################
	     * get The Latest Journal Entry of Traveler 
	     * @param  
	     * @return TravelLog Object
	     * @access private
	     * by Joel C. Llano
	     ###########################*/	
		
		function getTravelerLatestJournalEntry($privacycriteria = ''){			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$latestTravellog = array();

			$sql  = "SELECT max(travellogID) as tlID " .
					"FROM tblTravelLog, tblTravel " .
					"WHERE tblTravel.travelerID = $this->travID " .
					"AND tblTravel.travelID = tblTravelLog.travelID";

			if (strlen($privacycriteria))
				$sql = $sql . " AND privacypref IN ( " . $privacycriteria . " )";
			$this->rs->Execute($sql);

			if($this->rs->Result(0,"tlID")){
				$TravelLog = new TravelLog($this->rs->Result(0,"tlID"));
				$latestTravellog = $TravelLog;
			}
			
			return $latestTravellog;
		}
		
		function TravelerByUsername($_username){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = sprintf(
								"SELECT travelerID " .
								"FROM tblTraveler " .
								"WHERE username = '%s' ",
								addslashes($_username)
							);
			$this->rs->Execute($sql);

			if( $this->rs->Recordcount() ){
				$ObjResult = $this->rs->FetchAsObject();
				$this->travID = $ObjResult->travelerID;

				return ( $this->isAdvisor() ) ? 0 : $ObjResult->travelerID;
			}
			else
				return 0;
		}

		/**
		 * getAdministered groups of Administrator
		 * @return collection of Class AdminGroup or FunGroup
		 * @author Cheryl Ivy Q. Go
		 */
		function getGroupsAdministered($byID = false){			
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$arrGroup = array();
			$arrGroupId = array();

			$sql = "SELECT groupID FROM tblGroup WHERE administrator = " . $this-> travID;
			$this->rs->Execute($sql);

			while($group = mysql_fetch_array($this->rs->Resultset()))
				$arrGroupId[] = $group['groupID'];
			if($byID)
				return $arrGroupId;
			else
				$arrGroup = GroupFactory::instance()->create($arrGroupId);

			return $arrGroup;
		}

		/**
		 * getGAUsers
		 * @return collection of Class Traveler
		 * @author Cheryl Ivy Q. Go
		 */
		public static function getGAUsers($_filterCriteria2 = NULL, $_rowsLimit = NULL){
			 try{
			 	$conn = ConnectionProvider::instance()->getConnection();
			 	$rs   = new Recordset($conn);
			 	$rs2  = new Recordset($conn);
			 	$rs3  = new Recordset($conn);
			 }
			 catch(exception $e){
			 	throw $e;
			 }

			if ($_filterCriteria2){
				$condition = '';
				$myconditions = $_filterCriteria2-> getConditions();
				foreach($myconditions as $indCondition){
					$value = (strcasecmp('sql_query', $indCondition->getValue())) ? $indCondition-> getValue() : '(select distinct travelerID from tblGrouptoAdvisor)';
					$operation = $indCondition-> getOperation();
					$attname = $indCondition-> getAttributeName();					

					switch($operation){
						case FilterOp::$EQUAL:
							$condition = $condition . " AND " . $attname . " = " . $value;
							break;
						case FilterOp::$GREATER_THAN_OR_EQUAL:
							$condition = $condition . " AND " . $attname . " >= " . $value;
							break;
						case FilterOp::$LESS_THAN_OR_EQUAL:
							$condition = $condition . " AND " . $attname . " <= " . $value;
							break;
						case FilterOp::$IN:
							$condition = $condition . " AND " . $attname . " IN " . $value;
							break;
						case FilterOp::$NOT_IN:
							$condition = $condition . " AND " . $attname . " NOT IN " . $value;
							break;
						default:
							$condition = $condition . " AND " . $attname . " BETWEEN " . $value;
							break;
					}
				}
			}
			else
				$condition = " ORDER BY `dateregistered` DESC ";

			if ($_rowsLimit){
				$offset = $_rowsLimit-> getOffset();
	 			$rows =  $_rowsLimit-> getRows();
	 			$limitstr = " LIMIT " . $offset . " , " . $rows;
			}
			else
				$limitstr = '';

			$sql = "select SQL_CALC_FOUND_ROWS travelerID, username, password, currlocationID, sendableID, isSuspended, active, showsteps from tblTraveler where 1 = 1 " . $condition . $limitstr;
			$rs-> Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$result = mysql_fetch_array($rs2->Execute($sql2));

			$arrUsers = array();
			Traveler::$cntGAUsers = $result['totalRecords'];

			while ($traveler = mysql_fetch_array($rs->Resultset())){
				$sql3 = "select distinct groupID from tblGrouptoAdvisor where travelerID = " . $traveler['travelerID'];
				$rs3-> Execute($sql3);

				if (0 < $rs3->Recordcount()){
					$group = GroupFactory::instance()->create( array($rs3->Result(0, "groupID")) );
					$arrUsers[] = $group[0];
				}
				else{
					$mTraveler = new Traveler;
					$mTraveler->setTravelerData($traveler);
					$arrUsers[] = $mTraveler;
				}
			}

			return $arrUsers;
		}
		
		/**
		 * Added By: Aldwin Sabornido
		 * Date    : Oct 10, 2007
		 */
		static function getMostPopularInRandomOrder(){
			$conn      = ConnectionProvider::instance()->getConnection();
			$rs        = new Recordset($conn);
			$rs1       = clone ($rs);
			$str_locID = 0; 
			$arr       = array();
			$sql       = sprintf(
									'SELECT GoAbroad_Main.tbcountry.locID FROM GoAbroad_Main.tbcountry as tbcountry ' .
									'WHERE tbcountry.countryID = %d ' .
									'UNION ' .
									'SELECT GoAbroad_Main.tbcity.locID FROM GoAbroad_Main.tbcountry as tbcountry, GoAbroad_Main.tbcity as tbcity ' .
									'WHERE tbcountry.countryID      = %d ' .
									'AND GoAbroad_Main.tbcity.locID > 0 ' .
									'AND tbcountry.countryID        = tbcity.countryID ' .
									'GROUP BY locID'
									,69
									,69
						   	    );
			
			$rs->Execute($sql);
			
			if( $rs->Recordcount() ){
				while($row = mysql_fetch_assoc($rs->Resultset())){
					$str_locID .= ',' . $row['locID'];
				}

				$sql = sprintf(
									'SELECT travelerID, username, password, currlocationID, sendableID, isSuspended, active, showsteps ' .
									'FROM tblTraveler ' .
									'WHERE active > 0 ' .
									'AND travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
									'AND htlocationID NOT IN (%s) ' .
									'GROUP BY travelerID ' .
									'ORDER BY views DESC ' .
									'LIMIT 20'
									,$str_locID
							  );
				$rs1->Execute($sql); 

				if( $rs1->Recordcount() ){
					while($row = mysql_fetch_assoc($rs1->Resultset())){
						$mTraveler = new Traveler;
						$mTraveler->setTravelerData($row);
						$arr[] = $mTraveler;
					}
				}
			}
			shuffle($arr);

			return array_slice($arr, 0, 5);  
		}
		
		function setTravelDestination(TravelDestination $obj){
			$this->obj_travel_destination = $obj;
		}
		
		function hasRequested($friendID){
			$conn = ConnectionProvider::instance()->getConnection();
			$rs   = new Recordset($conn);
			
			$sql  = sprintf('SELECT travelerID FROM tblFriendRequest WHERE travelerID = %d AND friendID = %d LIMIT 1', $this-> travID, $friendID );
			$rs->Execute($sql);
			return $rs->Recordcount();
		}
		
		function getTravelDestination(){
			if( $this->obj_travel_destination != NULL ){
				return $this->obj_travel_destination;
			}else{
				//TODO: Must create a travel plan object
			}	
		}
		
		/***
		 * Added By Naldz: Reynaldo Castellano III
		 * Date: January 31, 2008
		 */
		function getFriendlyUrl(){
			if($this->getTravelerID() > 0){
				return '/'.$this->getUserName();
			}
			return '';
		}
		
		/**
		* Added by chris
		* check if traveler is automatically added
		*/
		public function isAutomaticallyAdded(){
			require_once('Class.dbHandler.php');
			require_once('Class.GaString.php');
			$db = new dbHandler('db=Travel_Logs');
			$sql = 'SELECT `isAutomatedTraveler` FROM `Travel_Logs`.`tblTraveler` WHERE `travelerID` = ' . GaString::makeSqlSafe($this->travID) . ' LIMIT 0,1';
			$rs = new iRecordset($db->execute($sql));
			if(0 < $rs->retrieveRecordcount() AND 1 == $rs->getisAutomatedTraveler(0))
				return true;
			return false;
		}

		static function isAllJournalsUnpublish($travelerID){
			$conn = ConnectionProvider::instance()->getConnection();
			$rs   = new Recordset($conn);

			$sql  = sprintf('SELECT tblTravel.travelID FROM tblTravel WHERE tblTravel.publish = 1  AND tblTravel.deleted = 0 AND tblTravel.travelerID = %d LIMIT 1', $travelerID);
			$rs->Execute($sql);
			return $rs->Recordcount() ? false: true; 			
		}
		
		static function getUnpublishJournals($travelerID){
			$conn = ConnectionProvider::instance()->getConnection();
			$rs   = new Recordset($conn);
			$arr  = array();
			$sql  = sprintf('SELECT tblTravel.travelID, tblTravel.title FROM tblTravel, tblTravelLog WHERE tblTravel.travelID = tblTravelLog.travelID AND tblTravelLog.publish = 0  AND tblTravel.deleted = 0 AND tblTravel.travelerID = %d GROUP BY tblTravel.travelID', $travelerID);
			$rs->Execute($sql);
			if( $rs->Recordcount() ){
				while( $row = mysql_fetch_assoc($rs->Resultset()) ){
					$obj_travel = new Travel;
					$obj_travel->setTravelID( $row['travelID'] );
					$obj_travel->setTitle   ( $row['title']    );
					$arr[] = $obj_travel; 
				}
			}
			return $arr;
		}

		
		
		//return array : arr[travelID][title]['primaryphoto'][count_of_photos_per_travel]
		static function getTravelPhotos($travelerID){			
			$mConn	= ConnectionProvider::instance()->getConnection();
			$mRs	= new Recordset($mConn);
			
			$sql = "SELECT  tblTravel.primaryphoto, tblTravel.title as travel_title, tblTravelLog.*, tblPhoto.*, tblTravelLogtoPhoto.position " .
					"FROM tblTravel,  tblTravelLog, tblPhoto, tblTravelLogtoPhoto " .
	 				"WHERE tblTravel.travelerID = $travelerID " .
 				  	"AND tblTravel.travelID = tblTravelLog.travelID " .
					"AND tblTravel.deleted = 0 " .	
 				 	"AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID " .
 				 	"AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID " .
 				 	"GROUP BY tblPhoto.photoID " .
 				   	"ORDER BY tblTravelLogtoPhoto.position DESC";
	 		$mRs->Execute($sql);

			$tmp_holder		= array();
			$temp			= array();
			$trav_photos	= array();
			$holder			= array();

			if( $mRs->Recordcount() ){
				require_once 'travellog/model/Class.TravelLog.php';
				require_once 'travellog/model/Class.Photo.php';

				//filter
				while( $row = mysql_fetch_assoc($mRs->Resultset()) ){
						$tmp_holder[$row['travelID']][$row['photoID']] = $row;
				}

				$mRs->Execute($sql); 
				while( $row = mysql_fetch_assoc($mRs->Resultset()) ){					
					if(!in_array($row['travelID'],$holder)){	
							$TravelLog = new TravelLog(0,$row);

							$temp['travelID']	=  $row['travelID'];
							$temp['title'] 		=  $row['travel_title'];
							$temp['num']		= count($tmp_holder[$row['travelID']]);

							$current_photos	= $tmp_holder[$row['travelID']];

							if($row['primaryphoto']>0){
								$photo = new Photo($TravelLog,0,$row);
								$temp['photo'] = $photo;
							}
							else{
								$random_photos = array_rand($current_photos);

								$photo = new Photo($TravelLog,0,$current_photos[$random_photos]);
								$temp['photo'] = $photo;
							}
							$trav_photos[] = $temp;
					}

					$holder[] = $row['travelID'];
				}				
			}

			return $trav_photos;
		}
		
		function hasPendingRequest($traveler_id){
			$conn = ConnectionProvider::instance()->getConnection();
			$rs   = new Recordset($conn);

			/*$sql  = "SELECT DISTINCT friendID " .
					"FROM tblFriendRequest " .
					"WHERE travelerID = $this->travID " .
					"AND friendID = $traveler_id " .
					"AND friendID NOT IN " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) " .
					"LIMIT 1";*/
			$sql  = "SELECT DISTINCT friendID " .
					"FROM tblFriendRequest " .
					"WHERE travelerID = $this->travID " .
					"AND friendID = $traveler_id " .
					"LIMIT 1";
			$rs->Execute($sql);

			return $rs->Recordcount();
		}
		
		function hasFriendRequest($traveler_id){
			$conn = ConnectionProvider::instance()->getConnection();
			$rs   = new Recordset($conn);

			/*$sql  = "SELECT DISTINCT travelerID " .
					"FROM tblFriendRequest " .
					"WHERE friendID = $this->travID " .
					"AND travelerID =  $traveler_id " .
					"AND travelerID NOT IN " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) " .
					"LIMIT 1";*/
			$sql  = "SELECT DISTINCT travelerID " .
					"FROM tblFriendRequest " .
					"WHERE friendID = $this->travID " .
					"AND travelerID =  $traveler_id " .
					"LIMIT 1";
			$rs->Execute($sql);

			return $rs->Recordcount();
		}
		
		/* added by Jul, 2008-11-17 */
		static function getTravelerByEmail($email=""){
			try{
				$conn = ConnectionProvider::instance()->getConnection();
				$rs   = new Recordset($conn);
				$db = new dbHandler();
				$email = $db->makeSqlSafeString($email);
				$sql = "SELECT tblTraveler.*
						FROM tblTraveler
						WHERE tblTraveler.email = $email
						AND tblTraveler.active  = 1 
						AND tblTraveler.deactivated = 0";
				$rs->Execute($sql);
				if( 0 == $rs->Recordcount() ){
					return FALSE;
				}
				$recordset = mysql_fetch_array($rs->Resultset());
	                  
				$travelerData = array();
				$travelerData["travelerID"] = $recordset['travelerID'];
				$travelerData["active"] = $recordset['active'];
				$travelerData["isSuspended"] = $recordset['isSuspended'];
	 			$travelerData["currlocationID"] = $recordset['currlocationID'];
	 			$travelerData["sendableID"] = $recordset['sendableID'];
	 			$travelerData["username"] = $recordset['username'];
	 			$travelerData["password"] = $recordset['password'];
	 			$travelerData["showsteps"] = $recordset['showsteps'];
				$travelerData["deactivated"] = $recordset['deactivated'];
									
				$traveler = new Traveler;
				$traveler-> setTravelerData($travelerData);

				// set profile data in array and set to profile; minimize DB query
				$profileData = array();								
				$profileData["travelerID"] = $recordset['travelerID'];
				$profileData["username"] = $recordset['username'];
				$profileData["firstname"] = $recordset['firstname'];
	 			$profileData["lastname"] = $recordset['lastname'];
	 			$profileData["email"] = $recordset['email'];
	 			$profileData["gender"] = $recordset['gender'];
	 			$profileData["htlocationID"] = $recordset['htlocationID'];
	 			$profileData["currlocationID"] = $recordset['currlocationID'];
	 			$profileData["address1"] = $recordset['address1'];
	 			$profileData["address2"] = $recordset['address2'];
	 			$profileData["phone"] = $recordset['phone'];
	 			$profileData["travelertypeID"] = $recordset['travelertypeID'];
	 			$profileData["birthdate"] = $recordset['birthdate'];
	 			$profileData["interests"] = $recordset['interests'];
	 			$profileData["shortbio"] = $recordset['shortbio'];
	 			$profileData["iculture"] = $recordset['iculture'];
	 			$profileData["dateregistered"] = $recordset['dateregistered'];
	 			$profileData["latestlogin"] = $recordset['latestlogin'];
	 			$profileData["views"] =  $recordset['views'];
	 			$profileData["intrnltravel"] = $recordset['intrnltravel'];
	 			$profileData["istripnexttwoyears"] = $recordset['istripnexttwoyears'];
	 			$profileData["income"] =	 $recordset['income'];
	 			$profileData["otherprefertravel"] =	 $recordset['otherprefertravel'];
	 			$profileData["ispromosend"] = $recordset['ispromosend'];
	 			$profileData["signature"] = $recordset['signature'];

				$profile = new TravelerProfile;
				$profile->setProfileData($profileData);
				
				$traveler->setTravelerProfile($profile);
				
				return $traveler;							
			}catch(exception $e){
				throw $e;
			}
		}
		
		function getFullName(){
			try{
				$profile = $this->getTravelerProfile();
				return $profile->getFirstName()." ".$profile->getLastName();
			}catch(exception $e){
				return "";
			}
		}
		
		function getFirstName(){
			try{
				$profile = $this->getTravelerProfile();
				return $profile->getFirstName();
			}catch(exception $e){
				return "";
			}
		}

		
		function getEmail(){
			try{
				$profile = $this->getTravelerProfile();
				return $profile->getEmail();
			}catch(exception $e){
				return "";
			}
		}

		// Added By : Adelbert Silla
		// Date : March 25, 2010
		function getImageEmail(){
			require_once('travellog/helper/Class.HelperTraveler.php');
			return HelperTraveler::convertTextToImage($this->getEmail());
		}


		/**************************** START: STAFF FUNCTIONS HERE *******************************/

			// checks if a traveler is a staff
			function isStaff(){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT travelerID " .
						"FROM tblGrouptoFacilitator " .
						"WHERE type = 1 " .
						"AND travelerID = " . $this->travID;
				$this->rs->Execute($sql);

				if (0 == $this->rs->Recordcount())
					return false;
				else
					return true;
			}

			// checks if a traveler is a staff of a specific group
			function isGroupStaff($groupID = 0){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT travelerID ".
						"FROM tblGrouptoFacilitator " .
						"WHERE type = 1 " .
						"AND status = 1 " .
						"AND travelerID = " . $this->travID . " " .
						"AND groupID = " . $groupID;
				$this->rs->Execute($sql);

				if (0 == $this->rs->Recordcount())
					return false;
				else
					return true;
			}

			// gets the grousp where a traveler has pending staff invitations
			function getPendingStaffInvitationsToGroup($_parentId = 0){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT DISTINCT tblInviteList.groupID " .
						"FROM tblInviteList, tblGroup " .
						"WHERE tblInviteList.groupID = tblGroup.groupID " .
						"AND tblGroup.isSuspended = 0 " .
						"AND tblInviteList.status = 2 " .
						"AND tblInviteList.travelerID = " . $this->travID;
				if (0 < $_parentId)
					$sql .= " AND tblGroup.parentID = " . $_parentId;
				$this->rs->Execute($sql);

				$arrGroup = array();
				if (0 < $this->rs->Recordcount()){
					while($Group = mysql_fetch_assoc($this->rs->Resultset())){
						$mGroup = new AdminGroup;
						$mGroup->setGroupID($Group['groupID']);
						$arrGroup[] = $mGroup;
					}
				}

				return $arrGroup;
			}

			// gets the staffed groups of a traveler
			static function getStaffedGroups($_travelerId = 0, $_arrGroupId = array()){
				try{
					$mConn = ConnectionProvider::instance()->getConnection();
					$mRs = new Recordset($mConn);
				}
				catch(exception $e){
					throw $e;
				}

				$arrGroups = array();
				$arrGroupId = array();

				$sql = "SELECT tblGroup.groupID " .
					"FROM tblGrouptoFacilitator, tblGroup " .
					"WHERE tblGroup.groupID = tblGrouptoFacilitator.groupID " .
					"AND tblGrouptoFacilitator.status = 1 " .
					"AND tblGrouptoFacilitator.type = 1 " .
					"AND tblGrouptoFacilitator.travelerID = " . $_travelerId . " ";

				if (0 < sizeof($_arrGroupId)){
					$lstGroupId = implode(", ", $_arrGroupId);
					$sql .= "AND tblGroup.parentID IN ($lstGroupId) ";
				}
				$sql .= "ORDER BY tblGroup.name";
				$mRs->Execute($sql);

				while($Group = mysql_fetch_assoc($mRs->Resultset()))
					$arrGroupId[] = $Group['groupID'];
				$arrGroups = GroupFactory::instance()->create($arrGroupId);

				return $arrGroups;
			}

			// gets all staffed subgroups
			static function getAllStaffedSubGroups($_travelerId = 0, $_parentId = 0){
				try{
					$mConn = ConnectionProvider::instance()->getConnection();
					$mRs = new Recordset($mConn);
				}
				catch(exception $e){
					throw $e;
				}

				$arrGroups = array();

				$sql = "SELECT tblGroup.groupID, tblGroup.name " .
						"FROM tblGrouptoFacilitator, tblGroup " .
						"WHERE tblGrouptoFacilitator.groupID = tblGroup.groupID " .
						"AND tblGroup.isSuspended = 0 " .
						"AND tblGroup.parentID = " . $_parentId . " " .
						"AND tblGrouptoFacilitator.type = 1 " .
						"AND tblGrouptoFacilitator.status = 1 " .
						"AND tblGrouptoFacilitator.travelerID = " . $_travelerId . " " .
						"GROUP BY tblGroup.groupID " .
						"ORDER BY tblGroup.name ASC";
				$mRs->Execute($sql);

				while($Group = mysql_fetch_assoc($mRs->Resultset())){
					$mGroup = new AdminGroup;
					$mGroup->setAdminGroupData($Group);
					$arrGroups[] = $mGroup;
				}

				return $arrGroups;
			}
		/***************************** END: STAFF FUNCTIONS HERE ********************************/
		
		// added by ianne - 01-02-2009
		// checks if a traveler is member of any group where a certain traveler is administrator
		function isAdminTravelerMember($travID){
			try{
				$mConn = ConnectionProvider::instance()->getConnection();
				$mRs = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$traveler_array = array();	
			$sql = "SELECT travelerID FROM tblGrouptoTraveler as gt, tblGroup as g WHERE g.administrator = $this->travID AND g.groupID=gt.groupID";
			$result = $mRs->Execute($sql);			
			while($row = mysql_fetch_assoc($result)){
				$traveler_array[] = $row['travelerID'];
			}
			
			if(in_array($travID,$traveler_array))
				return true;
			else
				return false;
		}
		
		// added by neri: 01-23-09
		// checks if traveler is member of an admin group and a subgroup 
		
		function isMemberOfAnAdminGroup() {
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT DISTINCT tblGrouptoTraveler.groupID FROM tblGrouptoTraveler, tblGroup WHERE tblGrouptoTraveler.travelerID = $this->travID " .
					"AND tblGroup.discriminator = 2 AND tblGroup.parentID = 0 AND tblGrouptoTraveler.groupID = tblGroup.groupID";
			$this->rs->Execute($sql);
			return $this->rs->Recordcount();
		}
		
		function isMemberOfASubgroup() {
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT DISTINCT tblGrouptoTraveler.groupID FROM tblGrouptoTraveler, tblGroup WHERE tblGrouptoTraveler.travelerID = $this->travID " .
					"AND tblGroup.discriminator = 2 AND tblGroup.parentID > 0 AND tblGrouptoTraveler.groupID = tblGroup.groupID";
			$this->rs->Execute($sql);
			return $this->rs->Recordcount();
		}
		
		// added by neri: 02-02-09
		// gets the number of video albums a traveler has
		
		function getVideoAlbums() {
			$cache 	= ganetCacheProvider::instance()->getCache();
			$key 	= 'Traveler_VideoAlbums_'.$this->getTravelerID();
			
			if ($cache != null && $videoAlbums = $cache->get($key)) {
				if ($videoAlbums[0] == 'EMPTY')
					$videoAlbums = array();
				return $videoAlbums;
			}
			
			$videoAlbums = array();
			
			$sql = "SELECT * FROM tblVideoAlbum WHERE creatorID = $this->travID AND creatorType = 'traveler' ORDER BY albumID ASC";
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			$mRs->Execute($sql);
			
			if (0 < $mRs->Recordcount()) {
				while ($recordset = mysql_fetch_array($mRs->Resultset())) {
	                try {
	                    $videoAlbum = new VideoAlbum();
	                    $videoAlbum->setAlbumID($recordset['albumID']);
	                    $videoAlbum->setTitle($recordset['title']);
	 					$videoAlbum->setCreatorID($recordset['creatorID']);
	 					$videoAlbum->setCreatorType($recordset['creatorType']);
	 					$videoAlbum->setDateCreated($recordset['dateCreated']);
	 					$videoAlbum->setLastDateUpdated($recordset['lastDateUpdated']);
	                }
	                catch (Exception $e) {
	                   throw $e;
	                }
	                $videoAlbums[] = $videoAlbum;
	            }
			}
			
			if ($cache != null){
				if (!count($videoAlbums)) {
					// a blank array put to cache causes null when retreived back
					$cache->set($key, array('EMPTY'), array('EXPIRE'=>3600) );	
				}else {
					$cache->set($key, $videoAlbums, array('EXPIRE'=>3600) );
				}

			}
			
			return $videoAlbums;
		}
		
		/**
		 * gets the videos of the traveler
		 * @param integer $cobrandGroupID (0 if in main .net)
		 * @param integer $loggedTravelerID
		 * @author neri : 06-10-09 
		 */ 
		
		function getTravelerVideos($cobrandGroupID = 0, $loggedTravelerID) {
			$cache 	= ganetCacheProvider::instance()->getCache();
			$key 	= ($cobrandGroupID > 0 && $loggedTravelerID != $this->getTravelerID()) ? 'CobrandTraveler_Videos_'.$this->getTravelerID() : 'Traveler_Videos_'.$this->getTravelerID();
			
			if ($cache != null && $videos = $cache->get($key)) {
				if ($videos[0] == 'EMPTY')
					$videos = array();
				return $videos;
			}
			
			$videos = array();
			
			if ($cobrandGroupID > 0 && $loggedTravelerID != $this->getTravelerID()) 
				$journalVideosSql = 'SELECT tblVideo.videoID, tblVideo.dateAdded, tblVideo.albumID ' .
									'FROM tblGroupApprovedJournals, tblTravel, tblTravelLog, tblTravelLogtoVideo, tblVideo, tblTravelLink ' .
									'WHERE tblGroupApprovedJournals.groupID = '. $cobrandGroupID .' ' .
									'AND tblGroupApprovedJournals.travelID = tblTravel.travelID ' .
									'AND tblGroupApprovedJournals.approved = 1 ' .
									'AND tblTravel.deleted = 0 ' .
									'AND tblTravelLog.deleted = 0 ' .
									'AND tblTravel.travelerID = '. $this->getTravelerID() .' '.
									'AND tblTravel.travelID = tblTravelLog.travelID ' .
									'AND tblTravel.travelLinkID = tblTravelLink.travelLinkID ' .
									'AND tblTravelLink.refType = 1 ' .
									'AND tblTravelLink.refID = '. $this->getTravelerID() .' ' .
									'AND tblTravelLog.travellogID = tblTravelLogtoVideo.travellogID ' .
									'AND tblTravelLogtoVideo.videoID = tblVideo.videoID ';
			else
				$journalVideosSql = 'SELECT tblVideo.videoID, tblVideo.dateAdded, tblVideo.albumID ' .
									'FROM tblTravel, tblTravelLog, tblTravelLogtoVideo, tblVideo, tblTravelLink ' .
									'WHERE tblTravel.deleted = 0 ' .
									'AND tblTravelLog.deleted = 0 ' .
									'AND tblTravel.travelerID = '. $this->getTravelerID() .' '.
									'AND tblTravel.travelID = tblTravelLog.travelID ' .
									'AND tblTravel.travelLinkID = tblTravelLink.travelLinkID ' .
									'AND tblTravelLink.refType = 1 ' .
									'AND tblTravelLink.refID = '. $this->getTravelerID() .' ' .
									'AND tblTravelLog.travellogID = tblTravelLogtoVideo.travellogID ' .
									'AND tblTravelLogtoVideo.videoID = tblVideo.videoID ';
			
			$sql = 'SELECT videoID, dateAdded, albumID ' .
				   'FROM (' .
				   		$journalVideosSql.
				   		'UNION ' .
				   		'SELECT tblVideo.videoID, tblVideo.dateAdded, tblVideoAlbum.albumID ' .
				   		'FROM tblVideoAlbum, tblVideo ' .
				   		'WHERE tblVideoAlbum.albumID > 0 ' .
				   		'AND tblVideoAlbum.albumID = tblVideo.albumID ' .
				   		'AND tblVideoAlbum.creatorID = ' .$this->getTravelerID().' ' .
				   		'AND tblVideoAlbum.creatorType = \'traveler\' ' .
				   ') AS videoTable ' .
				   'ORDER BY dateAdded DESC ';
			
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			$mRs->Execute($sql);
			
			if (0 < $mRs->Recordcount()) {
				while($row = mysql_fetch_assoc($mRs->Resultset())){
					try {
						if (0 < $row['albumID']) 
							$video = new VideoAlbumToVideo($row['videoID']);
						else 
							$video = new TravelLogToVideo($row['videoID']);
					}
					catch(Exception $e) {}
					
					$videos[] = $video;
				}
			}
			
			if ($cache != null){
				if (!count($videos)) {
					// a blank array put to cache causes null when retreived back
					$cache->set($key, array('EMPTY'), array('EXPIRE'=>3600) );	
				}else {
					$cache->set($key, $videos, array('EXPIRE'=>3600) );
				}
			}	
			
			return $videos;
		}
		
		//added by Jul
		function getPhotoAlbums($rowslimit = NULL){
			
           	$cache 	=	ganetCacheProvider::instance()->getCache();
			$key	=	'Traveler_PhotoAlbums_' . $this->getTravelerID();

			if ($cache != null && $albums =	$cache->get($key )){
				//echo 'Found (PhotoAlbumAdapter->getAlbums) group photo albums in cache <br/> ';
				if ($albums[0] == 'EMPTY'){
					$albums	=	array();				// if we found the contents 'EMPTY'we reassign the array to a blank array
				}	
				return $albums;
			}
			
		   $albums = array();

           if ($rowslimit instanceof RowsLimit){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            } 
            else
                $limitstr  = '';  
            
            //$sql = "SELECT photoalbumID FROM tblPhotoAlbum WHERE groupID = '$this->mGroupID'  ORDER BY photoalbumID DESC" . $limitstr;
            $sql = "SELECT * FROM tblPhotoAlbum WHERE groupID = 0 AND creator = '$this->travID' ORDER BY photoalbumID DESC" . $limitstr;
			
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			$mRs->Execute($sql);
            
            while ($recordset = mysql_fetch_array($mRs->Resultset())) {
                try {
                    //$photoalbum = new PhotoAlbum($recordset['photoalbumID']);
                    $photoalbum = new PhotoAlbum();
                    $photoalbum->setPhotoAlbumID($recordset['photoalbumID']);
                    $photoalbum->setTitle($recordset['title']);
                    $photoalbum->setCreator($recordset['creator']);
                    $photoalbum->setLastUpdate($recordset['lastupdate']);
                    $photoalbum->setGroupID(0);
                    
                }
                catch (Exception $e) {
                   throw $e;
                }
                $albums[] = $photoalbum;
            }
            
			if ($cache != null){
				if (!count($albums)) {
					// a blank array put to cache causes null when retreived back
					$cache->set($key, array('EMPTY'), array('EXPIRE'=>3600) );	
				}else {
					$cache->set($key, $albums, array('EXPIRE'=>3600) );
				}

			}

            return $albums;
        }

		function countPhotosToGrabByGroup($groupID=0){
			
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			
			$photocount = 0;
			
			/*$sql = "SELECT COUNT(tblPhoto.photoID) AS photocount
					FROM tblPhoto
						RIGHT JOIN tblTravelertoPhoto
							ON tblPhoto.photoID = tblTravelertoPhoto.photoID AND
								tblTravelertoPhoto.travelerID = $this->travID
					WHERE phototypeID = 1";
			
			$mRs->Execute($sql);
			$recordset = mysql_fetch_array($mRs->Resultset());
			$photocount += $recordset["photocount"];*/
			
			$albums = $this->getPhotoAlbums();
			
			foreach($albums as $album){
				$photocount += $album->getCountPhotos();
			}
			
			//get group-related and approved journals
			$sql = "SELECT tblTravel.travelID AS travelID
					FROM tblGroupApprovedJournals, tblTravel
					WHERE tblGroupApprovedJournals.groupID = $groupID AND
						tblGroupApprovedJournals.approved = 1 AND
						tblGroupApprovedJournals.travelID = tblTravel.travelID AND
						tblTravel.travelerID = $this->travID AND
						tblTravel.deleted = 0 AND
						tblTravel.publish = 1";
			$mRs->Execute($sql);
			while ($recordset = mysql_fetch_array($mRs->Resultset())) {
                try {
                    $travel = new Travel($recordset["travelID"]);
					$travellogs = $travel->getTravelerTravelLogs();
					foreach($travellogs as $travellog){
						$photocount += count($travellog->getPhotos());
					}
                }
                catch (Exception $e) {
                }
            }
			
			return $photocount;
		}
		
		function getPhotosToGrabByGroup($groupID=0){
			
			require_once("travellog/model/Class.Photo.php");
			
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			
			$photos = array();
			
			/*$sql = "SELECT tblPhoto.*
					FROM tblPhoto
						RIGHT JOIN tblTravelertoPhoto
							ON tblPhoto.photoID = tblTravelertoPhoto.photoID AND
								tblTravelertoPhoto.travelerID = $this->travID
					WHERE phototypeID = 1";
			
			$mRs->Execute($sql);
			while ($recordset = mysql_fetch_array($mRs->Resultset())) {
				$photos[] = new Photo($this->getTravelerProfile(),0,$recordset);
			}*/
			
			
			$albums = $this->getPhotoAlbums();
			
			foreach($albums as $album){
				$temp = $album->getPhotos();
				if( is_array($temp) ){
					$photos = array_merge($photos,$temp);
				}
			}
			
			//get group-related and approved journals
			$sql = "SELECT tblTravel.travelID AS travelID
					FROM tblGroupApprovedJournals, tblTravel
					WHERE tblGroupApprovedJournals.groupID = $groupID AND
						tblGroupApprovedJournals.approved = 1 AND
						tblGroupApprovedJournals.travelID = tblTravel.travelID AND
						tblTravel.travelerID = $this->travID AND
						tblTravel.deleted = 0 AND
						tblTravel.publish = 1";
			$mRs->Execute($sql);
			while ($recordset = mysql_fetch_array($mRs->Resultset())) {
                try {
                    $travel = new Travel($recordset["travelID"]);
					$travellogs = $travel->getTravelerTravelLogs();
					foreach($travellogs as $travellog){
						$temp = $travellog->getPhotos();
						if( is_array($temp) ){
							$photos = array_merge($photos,$temp);
						}
					}
                }
                catch (Exception $e) {
                }
            }
			return $photos;
		}
		
		function hasGoodStatus(){
			if($this->travelerStatus == 0)
				return false;
			return true;	
		}
		
		function updateTravelerStatus($bool){
			$conn = new Connection();
			$mRs = new Recordset($conn);
			
			$sql = "UPDATE tblTraveler SET travelerStatus = $bool WHERE travelerID = ".$this->travID;
			$mRs->Execute($sql);
			$this->setTravelerStatus($bool);
		}
		
		public function invalidateCacheEntry(){
			$this->cache	=	ganetCacheProvider::instance()->getCache();
			if ($this->cache != null) {
				$this->cache->delete('Traveler_' . $this->travID);
				$this->cache->delete('TravelerCB_' . $this->travID);
			}
		}
		
		public static function isMemberOfAnyOfMyGroup($facilitator,$traveler){
			$rs = ConnectionProvider::instance()->getRecordset();
			$sql = "SELECT g.groupID 
					FROM tblGrouptoTraveler as tgt, tblGroup as g
					WHERE tgt.groupID = g.groupID 
						AND tgt.travelerID = '$traveler'
						AND g.isSuspended = 0 
						AND g.discriminator > 1 
						AND g.administrator = '$facilitator'
						UNION 
						SELECT g.groupID 
						FROM tblGrouptoTraveler as tgt, tblGrouptoFacilitator as tgf, tblGroup as g
						WHERE tgt.groupID = tgf.groupID 
							AND tgt.travelerID = '$traveler'
							AND tgf.groupID = g.groupID
							AND tgf.travelerID = '$facilitator' 
							AND tgf.status = 1 
							AND tgf.type = 1";
					
			$rs->Execute($sql);
			return ($rs->Recordcount() > 0) ? true : false;
		}
		
		/**
		* checks if traveler is a staff or an advisor of any group
		**/
		public static function hasGroupPowers($travelerID){
			if(!$travelerID) return false;
			$rs = ConnectionProvider::instance()->getRecordset();
			$sql = "SELECT groupID FROM tblGrouptoFacilitator as tgf
					WHERE tgf.travelerID = '$travelerID' 
						AND tgf.status = 1 
						AND tgf.type = 1
					UNION 
					SELECT groupID 
					FROM tblGroup 
					WHERE administrator = '$travelerID' 
						AND discriminator > 1";
			$rs->Execute($sql);
			return ($rs->Recordcount() > 0) ? true : false;
		}
		
		function getFeaturedPhotoAlbums($featured=TRUE){
			$travelerID = $this->travID;
			if( $featured ){
				$sql = "SELECT *
						FROM tblPhotoAlbum
						WHERE isFeatured = 1
							AND groupID = 0
							AND creator = $travelerID
						ORDER BY lastupdate DESC";
			}else{
				$sql = "SELECT *
						FROM tblPhotoAlbum
						WHERE isFeatured = 0
							AND groupID = 0
							AND creator = $travelerID
						ORDER BY lastupdate DESC";		
			}
			$rs = ConnectionProvider::instance()->getRecordset();
			$rs->Execute($sql);
			
			$featuredAlbums = array();
			while ($recordset = mysql_fetch_array($rs->Resultset())) {
                try {
                    $photoalbum = new PhotoAlbum();
                    $photoalbum->setPhotoAlbumID($recordset['photoalbumID']);
                    $photoalbum->setTitle($recordset['title']);
                    $photoalbum->setCreator($recordset['creator']);
                    $photoalbum->setLastUpdate($recordset['lastupdate']);
                    $photoalbum->setGroupID($recordset['groupID']);
                    $photoalbum->setIsFeatured($recordset["isFeatured"]);
					$featuredAlbums[] = $photoalbum;
                }
                catch (Exception $e) {
                }
            }
            
            return $featuredAlbums;
		}
		
		function getFeaturedAlbumCount($featured=TRUE){
			$travelerID = $this->travID;
			if( $featured ){
				$sql = "SELECT COUNT(*) AS albumCount
						FROM tblPhotoAlbum
						WHERE isFeatured = 1
							AND groupID = 0
							AND creator = $travelerID";
			}else{
				$sql = "SELECT COUNT(*) AS albumCount
						FROM tblPhotoAlbum
						WHERE isFeatured = 0
							AND groupID = 0
							AND creator = $travelerID";		
			}
			$rs = ConnectionProvider::instance()->getRecordset();
			$rs->Execute($sql);
			
			$recordset = mysql_fetch_array($rs->Resultset());
			
			return $recordset["albumCount"];
		}
		
		function getMostRecentlyUploadedPhotos($firstN=3,$groupID=NULL){
			$travelerID = $this->travID;
			$limitClause = " LIMIT 0, $firstN";
			
			if( is_null($groupID) ){
				$sql = "(SELECT tblPhoto.*
						FROM tblPhoto, tblTravelertoPhoto
						WHERE tblPhoto.phototypeID = 1
							AND tblPhoto.photoID = tblTravelertoPhoto.photoID
							AND tblTravelertoPhoto.travelerID = $travelerID)
					
						UNION
					
						(SELECT tblPhoto.*
						FROM tblPhoto, tblTravelLogtoPhoto, tblTravelLog, tblTravel, tblTravelLink
						WHERE tblPhoto.phototypeID = 2
							AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID
							AND tblTravelLogtoPhoto.travellogID = tblTravelLog.travellogID
							AND tblTravelLog.deleted = 0
							AND tblTravelLog.travelID = tblTravel.travelID
							AND tblTravel.deleted = 0
							AND tblTravel.travelerID = $travelerID
							AND tblTravel.travellinkID = tblTravelLink.travellinkID
							AND tblTravelLink.reftype = 1)
					
						UNION
					
						(SELECT tblPhoto.*
						FROM tblPhoto, tblArticletoPhoto, tblArticle
						WHERE tblPhoto.phototypeID = 11
							AND tblPhoto.photoID = tblArticletoPhoto.photoID
							AND tblArticletoPhoto.articleID = tblArticle.articleID
							AND tblArticle.deleted = 0
							AND tblArticle.groupID = 0
							AND tblArticle.authorID = $travelerID)	
					
						UNION
					
						(SELECT tblPhoto.*
						FROM tblPhoto, tblPhotoAlbumtoPhoto, tblPhotoAlbum
						WHERE tblPhoto.phototypeID = 7
							AND tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID
							AND tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID
							AND tblPhotoAlbum.groupID = 0
							AND tblPhotoAlbum.creator = $travelerID)
					
						ORDER BY dateuploaded DESC
						$limitClause";
			}else{
				$sql = "(SELECT tblPhoto.*
						FROM tblPhoto, tblTravelertoPhoto
						WHERE tblPhoto.phototypeID = 1
							AND tblPhoto.photoID = tblTravelertoPhoto.photoID
							AND tblTravelertoPhoto.travelerID = $travelerID)
					
						UNION
					
						(SELECT tblPhoto.*
						FROM tblTravel, tblGroupApprovedJournals, tblTravelLink, 
							tblTravelLog, tblTravelLogtoPhoto, tblPhoto
						WHERE tblPhoto.phototypeID = 2
							AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID
							AND tblTravelLogtoPhoto.travellogID = tblTravelLog.travellogID
							AND tblTravelLog.deleted = 0
							AND tblTravelLog.travelID = tblTravel.travelID
							AND tblTravel.deleted = 0
							AND tblTravel.travelerID = $travelerID
							AND tblTravel.travellinkID = tblTravelLink.travellinkID
							AND tblTravelLink.reftype = 1
							AND tblTravel.travelID = tblGroupApprovedJournals.travelID
							AND tblGroupApprovedJournals.groupID = $groupID
							AND tblGroupApprovedJournals.approved = 1)
					
						UNION
					
						(SELECT tblPhoto.*
						FROM tblPhoto, tblArticletoPhoto, tblArticle
						WHERE tblPhoto.phototypeID = 11
							AND tblPhoto.photoID = tblArticletoPhoto.photoID
							AND tblArticletoPhoto.articleID = tblArticle.articleID
							AND tblArticle.deleted = 0
							AND tblArticle.groupID = 0
							AND tblArticle.authorID = $travelerID)	
					
						UNION
					
						(SELECT tblPhoto.*
						FROM tblPhoto, tblPhotoAlbumtoPhoto, tblPhotoAlbum
						WHERE tblPhoto.phototypeID = 7
							AND tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID
							AND tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID
							AND tblPhotoAlbum.groupID = 0
							AND tblPhotoAlbum.creator = $travelerID)
					
						ORDER BY dateuploaded DESC
						$limitClause";
			}
			$rs = ConnectionProvider::instance()->getRecordset();
			$rs->Execute($sql);
			
			$recentUploads = array();
			while($data = mysql_fetch_array($rs->Resultset())){
				try{
					$photo = new Photo(NULL,0,$data);
					if( $photo instanceof Photo ){
						$recentUploads[] = $photo;
					}
				}catch(exception $e){
				}
			}
			return $recentUploads;
		}
		
		/**
		* get the groupIDs of the main groups where this traveler has membership
		* @return int[] array of integers; empty array if this traveler doesn't belong to any group
		**/
		function getIDsOfMainGroupsJoined(){
			return GroupPeer::getIDsOfMainGroupsJoined($this);
		}
		
		/**
		* gets subgroups of a cobrand group
		* @param	AdminGroup		$group	The cobrand group
		* @return 	AdminGroup[] 	array of AdminGroup instances
		**/
		function getTravelerCBSubGroups(AdminGroup $group){
			return GroupPeer::getTravelerCBSubGroups($this,$group);
		}
		
		/**
		* gets the latest journal this traveler has added
		* @return Travel an instance of journal; NULL if traveler has not added one
		*/
		function getLatestJournalAdded(){
			return JournalPeer::getLatestJournalAdded($this);
		}
		
		function hasPhotos(){
			require_once('travellog/UIComponent/NewCollection/helper/Class.PhotoCollectionHelper.php');
			require_once('travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionLoaderFactory.php');
			$collectionHelper = PhotoCollectionHelper::getInstance();
			$collectionHelper->setType(PhotoCollectionConstants::TRAVELER_TYPE);
			$collectionHelper->setTraveler(new Traveler($this->travID));
			$collectionHelper->setUserLevel(PhotoCollectionConstants::OWNER);
			$collectionLoader = PhotoCollectionLoaderFactory::getInstance()->create($collectionHelper);
			return $collectionLoader->hasPhotos();
		}
		
		function hasVideos(){
			$cobrandGroupID = (isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getGroupID() : 0;
			return count($this->getTravelerVideos($cobrandGroupID, $this->getTravelerID()));
		}
		
		function hasSyncedApplications(){
			$sql = "SELECT * FROM `SocialApplication`.`tbUser` WHERE travelerID = ".$this->getTravelerID();
			$rs = ConnectionProvider::instance()->getRecordset();
			$rs->Execute($sql);
			$hasSynced = false;
			while($data = mysql_fetch_array($rs->Resultset())){
				$hasSynced = (!empty($data['facebookID']) || !empty($data['orkutID']) || !empty($data['myspaceID']) || !empty($data['hi5ID'])) ? true : false;
			}
			return $hasSynced;
		}
		
		function orderByLastLogin($l,$r){
			if ($l->getTravelerProfile()->getLastLogin() == $r->getTravelerProfile()->getLastLogin())
			   return 0;
			elseif ($l->getTravelerProfile()->getLastLogin() < $r->getTravelerProfile()->getLastLogin())
				return -1;
			else 
				return 1;	  
		}
		
		static function getMapPinSettings($id = null) {
			if(!$id) return false;
			
			$sqlQuery = "SELECT mapPinSettings FROM tblTraveler WHERE `travelerID` = $id LIMIT 1";
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			$mRs->Execute($sqlQuery);
            $row = mysql_fetch_array($mRs->Resultset());

			return $row ? $row['mapPinSettings'] : 0;
		}

		static function updateMapPinSettings($id = null, $mapPinSettings) {
			if(!$id) return false;
			$sqlQuery = "UPDATE tblTraveler set mapPinSettings = $mapPinSettings WHERE travelerID=$id";
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			$mRs->Execute($sqlQuery);
		}
	}