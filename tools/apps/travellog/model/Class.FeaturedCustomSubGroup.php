<?php
/**
 * Created on Jul.11.2008
 * @author Czarisse Daphne P. Dolina
 * Purpose: domain model for a featured sub group
 */

	require_once("travellog/model/Class.AdminGroup.php");
	
	class FeaturedCustomSubGroup extends AdminGroup {
		
		
		function isSubGroupFeatured(){
			
			if (0 < $this->mFeaturedCustomSubGroupRank)
				return true;
			else
				return false;
				
		}
				
		function setSubGroupFeatured() {
			
			if (!$this->isSubGroupFeatured()):
				$this->resetRankOfFeaturedSubGroups();
				
				$latestrank = $this->getRankOfNewFeaturedSubGroup();
				$sql = "UPDATE tblCustomizedGroupSectiontoSubGroups SET featuredsubgrouprank = " . $latestrank . " WHERE subgroupID = " . $this->mGroupID ;
				$this->mRs->Execute($sql);
			endif;
		}
		
		function unsetSubGroupFeatured(){
			
			$sql = "UPDATE tblCustomizedGroupSectiontoSubGroups SET featuredsubgrouprank = 0 WHERE subgroupID =  " . $this->mGroupID ;
			$this->mRs->Execute($sql);
			
			$this->resetRankOfFeaturedSubGroups();
		}
		
		function getRankOfNewFeaturedSubGroup() {
			
			$sql = "SELECT featuredsubgrouprank 
						FROM tblCustomizedGroupSection as a, tblCustomizedGroupSectiontoSubGroups as b
			 			WHERE a.customizedGroupSectionID = b.customizedGroupSectionID
						 AND a.advisorGroupID = " . $this->getParentID() . " ORDER BY b.featuredsubgrouprank DESC LIMIT 0 , 1" ;
			$this->mRs->Execute($sql);
			
			$newrank = 1;
			if ($this->mRs->RecordCount())
				$newrank = $this->mRs->Result(0,'featuredsubgrouprank') + 1;
				
			return $newrank;			
			
		}
		
		function resetRankOfFeaturedSubGroups() {
			
			$sql = "SELECT subgroupID FROM  tblCustomizedGroupSection as a, tblCustomizedGroupSectiontoSubGroups as b
			 WHERE a.customizedGroupSectionID = b.customizedGroupSectionID
			 AND a.advisorGroupID = " . $this->mParentID . 
					" AND b.featuredsubgrouprank > 0 ORDER BY b.featuredsubgrouprank" ;
		
			$this->mRs->Execute($sql);
			$resultset = $this->mRs->Resultset();
			$cnt = 0;
		
			while ($recordset = mysql_fetch_array($resultset)) {
				$cnt ++ ;
				$sql = "UPDATE tblCustomizedGroupSectiontoSubGroups SET featuredsubgrouprank = " . $cnt . " WHERE subgroupID = " . $recordset['subgroupID'] ;
				$this->mRs->Execute($sql);
			}	
			
		}
		
		public static function arrangeRankOfFeaturedSubGroups($_arrFeatSubGroup = array()) {
			
			$rs = new Recordset(new Connection());
			
			$cnt = 0;
			foreach ($_arrFeatSubGroup as $each) :
				$cnt ++ ;
				$sql = "UPDATE tblCustomizedGroupSectiontoSubGroups SET featuredsubgrouprank = " . $cnt . " WHERE subgroupID = " . $each ;
				$rs->Execute($sql);
			endforeach;	
			
		}
		

	}
		
?>
