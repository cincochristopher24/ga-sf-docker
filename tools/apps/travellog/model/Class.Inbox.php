<?php
	/**
	 * Created on Jul 26, 2006
	 * Class.Inbox.php 
	 * @author Cheryl Ivy Q. Go
	 * Last Edited on 22 Jan 2008 -- refactored
	 * Last Edited on 13 May 2008 -- optimized queries
	 */

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.PersonalMessage.php';

	class Inbox{		

		private $mRs	= NULL;
		private $mR2	= NULL;
		private $mR3	= NULL;
		private $mR4	= NULL;	
		private $mConn	= NULL;

		private $mInstance		= 0;
		private $mMessageId		= 0;
		private $mSendableId	= 0;
		private $mTotalRecords	= 0;

		const TRAVELER	= 1;
		const GROUP		= 2;
		
		/**
		 * Constructor of class Inbox
		 * @param integer $_sendableId
		 * @return Inbox
		 */		
		function Inbox(){
 			try{ 				
 				$this->mConn = new Connection();
				$this->mRs	= new Recordset($this->mConn);
				$this->mRs2	= new Recordset($this->mConn);
				$this->mRs3	= new Recordset($this->mConn);
				$this->mRs4	= new Recordset($this->mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			return $this;
		}

		/**
		 * Function name: Send
		 * @param Message $_message
		 * Inserts message to table tblMessages
		 */
				
		function Send(Message $_message = NULL){
			/************************************/
			/* Last Edited On: 29 April 2008	*/
			/* Purpose: batch insert			*/
			/************************************/

			// Set values to be inserted to table
			$text = addslashes($_message->getText());
			$subject = addslashes($_message->getTitle());		
			$discriminator = $_message->getDiscriminator();
			$destination = $_message->getDestination();
			$source = $_message->getSource()->getSendableID();			// Source: sendableID of Traveler/Group/Moderator
			$dateTime = date("Y-m-d H:i:s");

			// insert message to tblMessages		
			$sql = "INSERT INTO tblMessages (discriminator, senderID, title, message, dateCreated) " .
					"VALUES (" . $discriminator . ", " . $source . ", '$subject', '$text', '" . $dateTime . "')";
			$this->mRs->Execute($sql);

			$messId = $this->mRs->GetCurrentID();
			$_message->setMessageId($messId);
			$arrGroup = array();

			foreach ($destination as $dest){
				// if DESTINATION is TRAVELER
				if ($dest instanceof Traveler){
					$recipient = $dest->getSendableID();

					// FROM TRAVELER TO TRAVELER
					if ($_message->getSource() instanceof Traveler){
						// check if they are FRIENDS
						$isFriend = $dest->isFriend($_message->getSource()->getTravelerID());

						// FRIENDS
						if ($isFriend){
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messId . ", " . $recipient . ", 1)";
						}
						// NOT FRIENDS
						else{
							// if from GoAbroadNetwork Team
							if ( (4933 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "www.goabroad.net")) ||
								(153 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "dev.goabroad.net")) ||
								(412 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "goabroad.net.local")) ){
								$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messId . ", " . $recipient . ", 1)";
							}
							// if either the source or the destination is an admin of any group
							else if($_message->getSource()->isAdministrator() || $dest->isAdministrator()){ 
								// if source is admin and destination is member
								if($_message->getSource()->isAdminTravelerMember($dest->getTravelerID())){ 
									$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
											"VALUES (" . $messId . ", " . $recipient . ", 1)";
								}else{
									if($dest->isAdministrator()){ 
										// if destination is admin and source is member
										if($dest->isAdminTravelerMember($_message->getSource()->getTravelerID())){
											$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
													"VALUES (" . $messId . ", " . $recipient . ", 1)";
										}else{
											$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
													"VALUES (" . $messId . ", " . $recipient . ", 5)";
										}			
									}else{
										$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
												"VALUES (" . $messId . ", " . $recipient . ", 5)";		
									}	
								}
							}else{
								$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messId . ", " . $recipient . ", 5)";
							}
						}
					}

					// FROM GROUP TO TRAVELER
					else{
						// checks if RECIPIENT is GROUP MEMBER
						$isMember = $_message->getSource()->isMember($dest);

						// MEMBER
						if ($isMember){
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messId . ", " . $recipient . ", 1)";
						}
						// NOT MEMBER
						else{
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messId . ", " . $recipient . ", 5)";
						}
					}

					$this->mRs2->Execute($sql2);
					$_message-> setAttributeId($this->mRs2->GetCurrentID());
				}
				// if DESTINATION is GROUP/ CLUB / SUBGROUP
				else{
					// FROM GROUP TO GROUP
					if ($_message->getSource() instanceof Group){
						$travDestination = $dest->getAdministrator()->getTravelerID();
						$travSource = $_message->getSource()->getAdministrator()->getTravelerID();

						// check if ADMINISTRATOR
						$isAdmin = ( $travDestination == $travSource ) ? true : false;

						// ADMINISTRATOR
						if ($isAdmin){
							// get members of group: exclude Admin, include staff as member of group
							//last edited by Jul to include members with pending invitation
							$sendableID = $dest->getSendableID();
							$sql2 = "SELECT tblTraveler.sendableID AS sendableID 
									FROM tblGrouptoTraveler, tblGroup, tblTraveler 
									WHERE tblGrouptoTraveler.groupID = tblGroup.groupID 
										AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID 
										AND tblGroup.sendableID = $sendableID 
										AND tblTraveler.travelerID <> $travSource 
										GROUP BY sendableID 
									UNION 
									SELECT tblTraveler.sendableID AS sendableID 
									FROM tblTraveler, tblInviteList, tblGroup 
									WHERE tblInviteList.groupID = tblGroup.groupID 
										AND tblGroup.sendableID = $sendableID  
										AND tblInviteList.status != 1 
										AND tblTraveler.travelerID = tblInviteList.travelerID 
										AND tblTraveler.active = 1 
										AND tblTraveler.isSuspended = 0 
									GROUP BY sendableID";
							$this->mRs2->Execute($sql2);

							while ($SendableId = mysql_fetch_array($this->mRs2->Resultset())){
								$recipient = $SendableId['sendableID'];

								$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messId . ", " . $recipient . ", 1)";
								$this->mRs3->Execute($sql3);
								$_message->setAttributeId($this->mRs3->GetCurrentID());
							}
							
						}
						// NOT ADMINISTRATOR
						else{
							$recipient = $dest->getSendableID();

							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messId . ", " . $recipient . ", 5)";
							$this->mRs2->Execute($sql2);
						}
					}
					// FROM TRAVELER TO GROUP
					else if ($_message->getSource() instanceof Traveler){	// edited by ianne to include cobrand messages 12/09/2008
						// check if SENDER is MEMBER of GROUP
						$isMember = $dest->isMember($_message->getSource());

						// MEMBER
						if ($isMember){
							$recipient = $dest->getSendableID();

							// message for ADMINISTRATOR
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messId . ", " . $recipient . ", 1)";
							$this->mRs2->Execute($sql2);

							// get STAFF of GROUP
							$Staff = $dest->getStaff();

							foreach($Staff as $indStaff){
								$recipient = $indStaff->getSendableID();

								// message for STAFF
								$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messId . ", " . $recipient . ", 1)";
								$this->mRs3->Execute($sql3);
							}
						}
						// NOT MEMBER
						else{
							$recipient = $dest->getSendableID();

							// message for ADMINISTRATOR
							if ( (4933 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "www.goabroad.net")) ||
								(153 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "dev.goabroad.net")) ||
								(412 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "goabroad.net.local")) ){	
								// if from GoAbroadNetwork Team
								$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messId . ", " . $recipient . ", 1)";
							}
							else{
								$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messId . ", " . $recipient . ", 5)";
							}
							$this->mRs2->Execute($sql2);

							// get STAFF of GROUP
							$Staff = $dest->getStaff();

							foreach($Staff as $indStaff){
								$recipient = $indStaff->getSendableID();

								// message for STAFF
								if ( (4933 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "www.goabroad.net")) ||
									(153 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "dev.goabroad.net")) ||
									(412 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "goabroad.net.local")) ){
									// if from GoAbroadNetwork Team
									$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
											"VALUES (" . $messId . ", " . $recipient . ", 1)";
								}
								else{
									$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
											"VALUES (" . $messId . ", " . $recipient . ", 5)";
								}
								$this->mRs3->Execute($sql3);
							}
						}
					}

					// FROM STAFF TO GROUP
					else{
						// check if STAFF of recipient GROUP
						$isStaff = $dest->isStaff($_message->getSource()->getTravelerID());

						// STAFF
						if ($isStaff){
							$recipient = $dest->getSendableID();
							$travDestination = $dest->getAdministrator()->getTravelerID();
							$travSource = $_message->getSource()->getTravelerID();

							// message for ADMINISTRATOR
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messId . ", " . $recipient . ", 1)";
							$this->mRs2->Execute($sql2);
							
							// get members: exclude source and administrator
							// last edited by Jul to include members with pending invitations
							$groupID = $dest->getGroupID();
							$sql3 = "SELECT tblTraveler.sendableID AS sendableID
									FROM tblGrouptoTraveler, tblTraveler 
									WHERE tblTraveler.travelerID = tblGrouptoTraveler.travelerID 
										AND tblGrouptoTraveler.groupID = $groupID
										AND tblTraveler.travelerID <> $travSource 
										AND tblTraveler.travelerID <> $travDestination
								 	GROUP BY sendableID
									UNION
									SELECT tblTraveler.sendableID AS sendableID
									FROM tblTraveler, tblInviteList
									WHERE tblInviteList.groupID = $groupID
										AND tblInviteList.status != 1
										AND tblTraveler.travelerID = tblInviteList.travelerID
										AND tblTraveler.active = 1
										AND tblTraveler.isSuspended = 0
									GROUP BY sendableID";
							$this->mRs3-> Execute($sql3);

							while ($SendableId = mysql_fetch_array($this->mRs3->Resultset())){
								$recipient = $SendableId['sendableID'];

								$sql4 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messId . ", " . $recipient . ", 1)";
								$this->mRs4->Execute($sql4);
								$_message->setAttributeId($this->mRs4->GetCurrentID());
							}
							
						}

						// NOT STAFF
						else{
							$source = $_message->getSource()->getSendableID();
							$recipient = $dest->getSendableID();

							// message for ADMINISTRATOR
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messId . ", " . $recipient . ", 5)";
							$this->mRs2->Execute($sql2);

							// get STAFF of GROUP
							$Staff = $dest->getStaff();

							foreach($Staff as $indStaff){
								$recipient = $indStaff->getSendableID();

								// message for STAFF
								if ($recipient != $source){
									$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
											"VALUES (" . $messId . ", " . $recipient . ", 5)";
									$this->mRs3->Execute($sql3);
								}
							}
						}
					}
				}
			}

			return $messId;
		}

		/**
		 * Function name: Delete
		 * @param integer $_attributeId
		 * Sets trashed of message with attributeId = $_attributeId to 1
		 */
		 
		function Delete($_attributeId = 0){			
			// Update row in table tblMessages			
			$sql = "UPDATE tblMessageToAttribute SET trashed = 1, isRead = 0 WHERE attributeID = " . $_attributeId;
			$this->mRs-> Execute($sql);
		}

		/***************************** START: SETTERS *************************************/
		function setInstance($_instance = 0){
			$this->mInstance = $_instance;
		}
		function setTotalRecords($_totalRecords = 0){
			$this->mTotalRecords = $_totalRecords;
		}
		function setMessageId($_messageId = 0){
			$this->mMessageId = $_messageId;
		}
		function setSendableId($_sendableId = 0){
			$this->mSendableId = $_sendableId;
		}
		function setInboxRecipient(){
			
		}
		/******************************* END: SETTERS *************************************/

		/***************************** START: GETTERS *************************************/
		function getTotalRecords(){
			return $this->mTotalRecords;
		}
		function getMessageList($_rowsLimit = NULL){

			$arrMessages = array();

			if (self::TRAVELER == $this->mInstance)
				$arrMessages = $this->getTravelerInbox($_rowsLimit);
			else
				$arrMessages = $this->getGroupInbox($_rowsLimit);

			return $arrMessages;
		}
		function getTravelerInbox($_rowsLimit = NULL){
			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);			
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
				$travelerId = $Sendable->getTravelerID();
			}
			catch (exception $e){
				throw $e;
			}

			// Get messages in inbox
			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ";
				$sql .= "(" .
							"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;
				// if user is administrator of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
						"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.trashed = 0 " .
						"AND tblMessageToAttribute.messageType = 1 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGroup " .
							"WHERE sendableID = tblMessageToAttribute.recipientID " .
							"AND isSuspended = 0 " .
							"AND administrator = " . $travelerId .
						")";
				// if user is staff of any group, include messages
				if ( $Sendable->isStaff() )
					$sql .= " UNION " .
							"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGrouptoFacilitator " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND type = 1 " .
								"AND status = 1 " .
								"AND travelerID = " . $travelerId .
							")";
			$sql .= ") as allInbox GROUP BY attributeID ORDER BY datecreated DESC  ". $queryLimit;
			$this->mRs-> Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() as totalRecords";
			$this->mRs2-> Execute($sql2);

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_array($this->mRs->Resultset())){
				$message = new PersonalMessage();
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setMessageTypeId($rMessage['messageType']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));
				
				$message->setIsRead($rMessage['isRead']);

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setMessageRecipient();

				$arrMessage[] = $message;
			}

			return $arrMessage;
		}
		function getGroupInbox($_rowsLimit = NULL){ echo 'group inbox';exit;
			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);			
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
				$groupId = $Sendable->getGroupID();
			}
			catch (exception $e){
				throw $e;
			}

			// Get messages in inbox
			$sql = "SELECT SQL_CALC_FOUND_ROWS attributeID, dateCreated FROM ";
				$sql .= "(" .
							"SELECT tblMessageToAttribute.attributeID, tblMessages.datecreated " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;

				if ($Sendable->getDiscriminator() == Group::ADMIN){
					$sql .= " UNION " .
						"SELECT tblMessageToAttribute.attributeID, tblMessages.datecreated " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessageToAttribute.messageType = 1 " .
						"AND tblMessageToAttribute.trashed = 0 " .
						"AND tblMessages.discriminator = 3 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGroup " .
							"WHERE sendableID = tblMessageToAttribute.recipientID " .
							"AND isSuspended = 0 " .
							"AND parentID = " . $groupId .
						")";
				}
				$sql .= ") as allInbox GROUP BY attributeID ORDER BY datecreated DESC  ". $queryLimit;
				$this->mRs-> Execute($sql);

				$sql2 = "SELECT FOUND_ROWS() as totalRecords";
				$this->mRs2-> Execute($sql2);

				$arrMessage = array();
				$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));


				while($message = mysql_fetch_array($this->mRs->Resultset())){
					$message = new PersonalMessage();
					$message->setSendableId($this->mSendableId);
					$message->setCreated($rMessage['dateCreated']);
					$message->setMessageId($rMessage['messageID']);
					$message->setAttributeId($rMessage['attributeID']);
					$message->setMessageTypeId($rMessage['messageType']);
					$message->setText(stripslashes($rMessage['message']));
					$message->setTitle(stripslashes($rMessage['title']));

					$message->setIsRead($rMessage['isRead']);

					$factory = MessageSendableFactory::Instance();
					$_source = $factory->Create($rMessage['senderID']);

					$message->setSource($_source);
					$message->setMessageRecipient();

					$arrMessage[] = $message;
				}

				return $arrMessage;
		}
		function getNumberOfNewMessages(){
			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
			}
			catch (exception $e){
				throw $e;
			}
			
			// Get recordcount of new messages
			$sql = "SELECT attributeID FROM ";
				$sql .= "(" .
							"SELECT tblMessageToAttribute.attributeID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.isRead = 0 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;

				if ($Sendable instanceof Traveler){
					// if user is administrator of any group, include messages
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.isRead = 0 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND isSuspended = 0 " .
								"AND administrator = " . $Sendable->getTravelerID() .
							")";
					// if user is staff of any group, include messages
					$sql .= " UNION " .
							"SELECT tblMessageToAttribute.attributeID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.isRead = 0 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGrouptoFacilitator " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND type = 1 " .
								"AND status = 1 " .
								"AND travelerID = " . $Sendable->getTravelerID() .
							")";
				}
				else{
					if ($Sendable->getDiscriminator() == Group::ADMIN){
						$sql .= " UNION " .
								"SELECT tblMessageToAttribute.attributeID " .
								"FROM tblMessages, tblMessageToAttribute " .
								"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
								"AND tblMessageToAttribute.messageType = 1 " .
								"AND tblMessageToAttribute.trashed = 0 " .
								"AND tblMessageToAttribute.isRead = 0 " .
								"AND tblMessages.discriminator = 3 " .
								"AND EXISTS " .
								"(" .
									"SELECT sendableID " .
									"FROM tblGroup " .
									"WHERE sendableID = tblMessageToAttribute.recipientID " .
									"AND isSuspended = 0 " .
									"AND parentID = " . $Sendable->getGroupID() .
								")";
					}
				}
			$sql .= ") as allInbox GROUP BY attributeID";
			$this->mRs-> Execute($sql);
			
			return $this->mRs->Recordcount();
		}
		function getNewMessages($_rowsLimit = NULL){
			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{
				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
			}
			catch (exception $e){
				throw $e;
			}

			// Get recordcount of new messages
			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ";
				$sql .= "(" .
							"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.isRead = 0 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.recipientID = " . $this->mSendableId;
				if ($Sendable instanceof Traveler){
					// if user is administrator of any group, include messages
					$sql .= " UNION " .
							"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.isRead = 0 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGroup " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND isSuspended = 0 " .
								"AND administrator = " . $Sendable->getTravelerID() .
							")";
					// if user is staff of any group, include messages
					$sql .= " UNION " .
							"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.messageType = 1 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.isRead = 0 " .
							"AND EXISTS " .
							"(" .
								"SELECT sendableID " .
								"FROM tblGrouptoFacilitator " .
								"WHERE sendableID = tblMessageToAttribute.recipientID " .
								"AND type = 1 " .
								"AND status = 1 " .
								"AND travelerID = " . $Sendable->getTravelerID() .
							")";
			}
			else{
				if ($Sendable->getDiscriminator() == Group::ADMIN){
					$sql .= " UNION " .
						"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
						"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessageToAttribute.messageType = 1 " .
						"AND tblMessageToAttribute.trashed = 0 " .
						"AND tblMessageToAttribute.isRead = 0 " .
						"AND tblMessages.discriminator = 3 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGroup " .
							"WHERE sendableID = tblMessageToAttribute.recipientID " .
							"AND isSuspended = 0 " .
							"AND parentID = " . $Sendable->getGroupID() .
						")";
				}
			}
			$sql .= ") as allInbox GROUP BY attributeID ORDER BY datecreated DESC";

			$this->mRs-> Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$this->mRs2-> Execute($sql2);
			
			$arrMessage = array();
			$this-> setTotalRecords ($this->mRs2->Result(0, "totalRecords"));
						
			while ($rMessage = mysql_fetch_array ($this->mRs->Resultset())){
				$message = new PersonalMessage();
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setMessageTypeId($rMessage['messageType']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));
				
				$message->setIsRead($rMessage['isRead']);

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setMessageRecipient();

				$arrMessage[] = $message;
			}
			
			return $arrMessage;
		}
		/******************************** END: GETTERS *************************************/

		static function isOwner($_attributeId = 0, $_sendableId = 0){
			try{
 				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($_sendableId);
			}
			catch (exception $e){
				throw $e;
			}

			$sql = "SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.messageType = 1 " .
					"AND tblMessageToAttribute.recipientID = " . $_sendableId . " " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeId;
			// if user is administrator of any group, include messages
			$sql .= " UNION " .
					"SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.messageType = 1 " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
					"AND EXISTS " .
					"(" .
						"SELECT sendableID " .
						"FROM tblGroup " .
						"WHERE sendableID = tblMessageToAttribute.recipientID " .
						"AND isSuspended = 0 " .
						"AND administrator = " . $Sendable->getTravelerID() .
					")";
			// if user is staff of any group, include messages
			$sql .= " UNION " .
					"SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.messageType = 1 " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeId . " " .
					"AND EXISTS " .
					"(" .
						"SELECT sendableID " .
						"FROM tblGrouptoFacilitator " .
						"WHERE sendableID = tblMessageToAttribute.recipientID " .
						"AND type = 1 " .
						"AND status = 1 " .
						"AND travelerID = " . $Sendable->getTravelerID() .
					")";
			$mRs-> Execute($sql);

			if (0 < $mRs->Recordcount())
				return true;
			else
				return false;
		}
	}
?>