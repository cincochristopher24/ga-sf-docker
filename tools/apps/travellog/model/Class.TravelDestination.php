<?php
/*
	Filename:		Class.TravelDestination.php
	Module:			My Travel Plans (Travel Scheduler, Travel ToDo, Go Abroad)
	Author:			Jonas Tandinco
	Date created:	Sep/05/2007
	Purpose:		Class definition for a travel destination

	EDIT HISTORY:
	
	Sep/21/2007 by Jonas Tandinco
	
		1.	divided start and end dates into 3 properties for month, day, and year
		
	Sep/28/2007 by Jonas Tandinco
	
		1.	enhanced display of travel destination dates to make it more intelligent
		
	Oct/01/2007 by Jonas Tandinco
	
		1.	tweaked algorithm for displaying dates to make it more effient in displaying date ranges
		2.	added startDate and endDate properties which is the only physically stored field in the database.
		
	Oct/08/2007 by Jonas Tandinco
	
		1.	refactored and optimized code
		
	Oct/15/2007 by Jonas Tandinco
	
		1.	added code to getDateRange() to fix a date range display issue
		2.	corrected inconsistencies in getDateRange()
		
	Oct/31/2007 by Jonas Tandinco
		1.	changed countryID, countryName filed to a country object
	
	Feb/25/2008 by Aldwin
		1.	added a setter and getter function for CountryName 
*/

class TravelDestination {
	
	private $travelWishToCountryID;
	private $travelWishID;

	private $country;
	private $country_name = '';
	private $description;

	private $startDate;
	private $endDate;
	
	// owner
	private $travelPlan = NULL;

	// constructor
	public function TravelDestination() {
		$this->country = new Country();
	}
	
	// getters
	public function getTravelPlan() {
		return $this->travelPlan;
	}
	
	public function getCountryName() {
		return $this->country_name ;
	}
	
	public function getCountry() {
		return $this->country;
	}

	public function getStartDate() {
		return $this->startDate;
	}
	
	public function getEndDate() {
		return $this->endDate;
	}

	public function getDescription() {
		return $this->description;
	}
	
	public function getEndDay() {
		if (!$this->getEndDate()) {
			return 0;
		}

		$endDay = explode('-', $this->getEndDate());
		$endDay = (int) $endDay[2];

		return $endDay;
	}
	
	public function getEndMonth() {
		if (!$this->getEndDate()) {
			return 0;
		}

		$endMonth = explode('-', $this->getEndDate());
		$endMonth = (int) $endMonth[1];

		return $endMonth;
	}
	
	public function getEndYear() {
		if (!$this->getEndDate()) {
			return 0;
		}

		$endYear = explode('-', $this->getEndDate());
		$endYear = (int) $endYear[0];

		return $endYear;
	}
	
	public function getStartDay() {
		if (!$this->getStartDate()) {
			return 0;
		}

		$startDay = explode('-', $this->getStartDate());
		$startDay = (int) $startDay[2];
		
		return $startDay;
	}
	
	public function getStartMonth() {
		if (!$this->getStartDate()) {
			return 0;
		}

		$startMonth = explode('-', $this->getStartDate());
		$startMonth = (int) $startMonth[1];
		
		return $startMonth;
	}
	
	public function getStartYear() {
		if (!$this->getStartDate()) {
			return 0;
		}

		$startYear = explode('-', $this->getStartDate());
		$startYear = (int) $startYear[0];
		
		return $startYear;
	}

	public function getTravelWishID() {
		return $this->travelWishID;
	}
	
	public function getTravelWishToCountryID() {
		return $this->travelWishToCountryID;
	}
	
	// settters
	public function setTravelPlan($travelPlan) {
		$this->travelPlan = $travelPlan;
	}
	
	public function setCountryName($country_name) {
		$this->country_name = $country_name;
	}
	
	public function setCountry($country) {
		$this->country = $country;
	}

	public function setStartDate($startDate) {
		$this->startDate = $startDate;
	}
	
	public function setEndDate($endDate) {
		$this->endDate = $endDate;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setTravelWishID($travelWishID) {
		$this->travelWishID = $travelWishID;
	}
	
	public function setTravelWishToCountryID($travelWishToCountryID) {
		$this->travelWishToCountryID = $travelWishToCountryID;
	}
	
	public function save() {
		if (!$this->getTravelWishToCountryID()) {
			TravelDestinationPeer::add($this);
		} else {
			TravelDestinationPeer::edit($this);
		}
	}

	// display a formatted date
	public function getDateRange() {
		// do not display anything if there is no value for start date
		if (!$this->getStartYear()) {
			return '';
		}
	  
		// immediately display start date if there's no end date
		if (!$this->getEndYear()) {
			return $this->formatDate($this->getStartMonth(), $this->getStartDay(), $this->getStartYear());
		}

		if ($this->getStartYear() == $this->getEndYear()) {
			if ($this->getStartMonth() && $this->getEndMonth()) {
				if ($this->getStartMonth() == $this->getEndMonth()) {
					if ($this->getStartDay() && $this->getEndDay()) {
						if ($this->getStartDay() == $this->getEndDay()) {
							// Jun 29 2007 - Jun 29 2007 becomes Jun 29 2007
							return date('M j Y', mktime(0, 0, 0, $this->getStartMonth(), $this->getStartDay(), $this->getStartYear()));
						} else {
							// Jun 13 2007 - Jun 28 2007 becomes Jun 13 - 28 2007
							return date('M j', mktime(0, 0, 0, $this->getStartMonth(), $this->getStartDay(), $this->getStartYear())) . ' - ' .
								   date('j Y', mktime(0, 0, 0, $this->getEndMonth(),   $this->getEndDay(),   $this->getEndYear()  ));
						}
					} elseif ($this->getEndDay()) {
						// Jun 2007 - Jun 28 2007 becomes Jun - Jun 28 2007
						return date('M',     mktime(0, 0, 0, $this->getStartMonth(), 1,                  $this->getStartYear())) . ' - ' .
							   date('M j Y', mktime(0, 0, 0, $this->getEndMonth(),   $this->getEndDay(), $this->getEndYear()  ));
					} elseif ($this->getStartDay()) {
						// Jan 1 2007 - Jan 2007 becomes Jan 1 2007
						return date('M j Y', mktime(0, 0, 0, $this->getStartMonth(), $this->getStartDay(), $this->getStartYear()));
					} else {
						// Jun 2007 - Jun 2007 becomes Jun 2007
						return date('M Y', mktime(0, 0, 0, $this->getStartMonth(), 1, $this->getStartYear()));
					}
				} else {
					if ($this->getStartDay() && $this->getEndDay()) {
						// Feb 1 2007 - Mar 1 2007 becomes Feb 1 - Mar 1 2007
						return date('M j',   mktime(0, 0, 0, $this->getStartMonth(), $this->getStartDay(), $this->getStartYear())) . ' - ' .
							   date('M j Y', mktime(0, 0, 0, $this->getEndMonth(),   $this->getEndDay(),   $this->getEndYear()  ));
					} elseif ($this->getStartDay()) {
						// Feb 1 2007 - Mar 2007 becomes Feb 1 - Mar 2007
						return date('M j', mktime(0, 0, 0, $this->getStartMonth(), $this->getStartDay(), $this->getStartYear())) .  ' - ' .
							   date('M Y', mktime(0, 0, 0, $this->getEndMonth(),   1,                    $this->getEndYear()  ));
					} elseif ($this->getEndDay()) {
						// Feb 2007 - Mar 1 2007 becomes Feb - Mar 1 2007
						return date('M',     mktime(0, 0, 0, $this->getStartMonth(), 1,                  $this->getStartYear())) . ' - ' .
							   date('M j Y', mktime(0, 0, 0, $this->getEndMonth(),   $this->getEndDay(), $this->getEndYear()  ));
					} else {
						// Feb 2007 - Mar 2007 becomes Feb - Mar 2007
						return date('M',   mktime(0, 0, 0, $this->getStartMonth(), 1, $this->getStartYear())) . ' - ' .
							   date('M Y', mktime(0, 0, 0, $this->getEndMonth(),   1, $this->getEndYear()  ));
					}
				}
			} elseif ($this->getEndMonth()) {
				// 2007 - Mar 2007 becomes 2007 - Mar 2007
				return $this->getStartYear() . ' - ' .
					   date('M Y', mktime(0, 0, 0, $this->getEndMonth(), 1, $this->getEndYear()));
			} elseif ($this->getStartMonth()) {
				// Jan 2007 - 2007 becomes Jan 2007
				return date('M Y', mktime(0, 0, 0, $this->getStartMonth(), 1, $this->getStartYear()));
			} else {
				// 2007 - 2007 becomes 2007
				return $this->getStartYear();
			}
		} else {
			return $this->formatDate($this->getStartMonth(), $this->getStartDay(), $this->getStartYear()) . ' - ' .
				   $this->formatDate($this->getEndMonth(),   $this->getEndDay(),   $this->getEndYear()  );
		}
	}
	
	private function formatDate($month, $day, $year) {
		if (!$month && !$day && !$year) {
			return '';
		} elseif ($year && !$month && !$day) {
			return $year;
		} elseif ($month && $year && !$day) {
			return date('M Y', mktime(0, 0, 0, $month, 1, $year));
		} elseif ($month && $day && $year) {
			return date('M j Y', mktime(0, 0, 0, $month, $day, $year));
		}
	}
}