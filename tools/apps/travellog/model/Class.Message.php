<?php
	/**
	 * Created on Jul 26, 2006
	 * Class.Message.php 
	 * @abstract
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.Listing.php';
	require_once 'travellog/model/Class.MessageType.php';
	require_once 'travellog/model/Class.DiscriminatorType.php';
	require_once 'travellog/model/Class.MessageSendableFactory.php';
	
	abstract class Message{	
				
		protected $mRs	= NULL;
		protected $mRs2	= NULL;
		protected $mRs3	= NULL;
		protected $mRs4	= NULL;
		protected $mConn	= NULL;
		
		private $mRead			= 0;
		private $mDiscriminator	= 0;
		private $mAttributeId	= 0;
		private $mMessageTypeId	= 0;
		private $mText			= NULL;
		private $mTitle			= NULL;
		private $mCreated		= NULL;
		
		/**
		 * @staticvar
		 */
		
		protected $mParentId	= 0;
		protected $mListingId	= 0;
		protected $mMessageId	= 0;
		protected $mExpiry		= NULL;
		protected $mSource		= NULL;
		protected $mSendableId	= 0;
		protected $mDestination	= array();

		/**
		 * Constructor of class Message
		 * @param integer $_attributeId
		 * @return Message|exception phpDocumentor Message object or exception
		 */
		
		function Message($_attributeId = 0){
			try{ 				
 				$this->mConn  = new Connection();
				$this->mRs	= new Recordset($this->mConn);
				$this->mRs2	= new Recordset($this->mConn);
				$this->mRs3	= new Recordset($this->mConn);
				$this->mRs4	= new Recordset($this->mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$this->setAttributeId($_attributeId);

			if ($this->mAttributeId != 0){

				$sql = "SELECT tblMessages.messageID, tblMessages.discriminator, tblMessages.title, " .
							"tblMessages.message, tblMessages.dateCreated, tblMessages.senderID, " .
							"tblMessageToAttribute.messageType, tblMessageToAttribute.isRead " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessageToAttribute.attributeID = " . $this->mAttributeId;
				$this->mRs->Execute($sql);

				// Message Id does not exist				
				if (0 == $this->mRs->Recordcount()){										
					throw new exception ("Attribute ID: does not exist!");
				}

				$this->mRead	= $this->mRs->Result(0, "isRead");
				$this->mCreated	= $this->mRs->Result(0, "dateCreated");
				$this->mTitle	= stripslashes($this->mRs->Result(0, "title"));
				$this->mText	= stripslashes($this->mRs->Result(0, "message"));
				$this->mMessageId		= $this->mRs->Result(0, "messageID");
				$this->mMessageTypeId	= $this->mRs->Result(0, "messageType");
				$this->mDiscriminator	= $this->mRs->Result(0, "discriminator");

				// if ALERT					
				if (DiscriminatorType::$ALERT == $this->mDiscriminator){					
					// get expiry date
					$sql2 = "SELECT expiry FROM tblMessageToAlert WHERE messageID = " . $this->mMessageId;
					$this->mRs2->Execute($sql2);
					$this->mExpiry = $this->mRs2->Result(0, "expiry");					
				}
				// if INQUIRY			
				else if (DiscriminatorType::$INQUIRY == $this->mDiscriminator){					
					// get listingId and parentId					
					$sql2 = "SELECT listingID, parentID FROM tblMessageToInquiry WHERE messageID = " . $this->mMessageId;
					$this->mRs2->Execute($sql2);
					
					$this->mParentId	= $this->mRs2->Result(0, "parentID");
					$this->mListingId	= $this->mRs2->Result(0, "listingID");					
				}

				// get source || TRAVELER/STAFF/GROUP			
				try{
					$factory = MessageSendableFactory::Instance();
					$_source = $factory->Create($this->mRs->Result(0, "senderID"));
				}
				catch (exception $e){
					throw $e;
				}
				$this->mSource = $_source;

				// PERSONAL
				if ((DiscriminatorType::$PERSONAL == $this->mDiscriminator))
					$this->setMessageRecipient();
				// BULLETIN OR ALERT
				else{
					if ((DiscriminatorType::$BULLETIN == $this->mDiscriminator))
						$this->setBulletinRecipient();
					else
						$this->setAlertRecipient();
				}
			}

			return $this;
		}
		
		/***************************** START: SETTERS *************************************/		
		function setTitle($_title = ''){
			$this->mTitle = $_title;
		}		 
		function setText($_text = ''){
			$this->mText = $_text;
		}
		function setIsRead($_read = 0){
			$this->mRead = $_read;
		}
		function setRead($_read = 0){

			$this->mRead = $_read;

			// Update row in table tblMessages		
			$sql = "UPDATE tblMessageToAttribute SET isread = " . $_read  . " WHERE attributeID = " . $this->mAttributeId;
			$this->mRs->Execute($sql);
		}		 
		function setCreated($_created = ''){
			$this->mCreated = $_created;
		}
		function setMessageId($_messageId = 0){
			$this->mMessageId = $_messageId;
		}
		function setAttributeId($_attributeId = 0){
			$this->mAttributeId = $_attributeId;
		}
		function setMessageTypeId($_messageTypeId = 0){
			$this->mMessageTypeId = $_messageTypeId;
		}
		function setDiscriminator($_discriminator = 0){
			$this->mDiscriminator = $_discriminator;
		}
		/******************************** END: SETTERS *************************************/
		
		/***************************** START: GETTERS *************************************/		
		function getTitle(){
			return $this->mTitle;
		}		 
		function getText(){
			return $this->mText;
		}
		function getCreated(){
			return $this->mCreated;
		}
		function getExpiry(){
			return $this->mExpiry;	
		}
		function getMessageId(){
			return $this->mMessageId;
		}
		function getAttributeId(){
			return $this->mAttributeId;
		}
		function getMessageTypeId(){
			return $this->mMessageTypeId;
		}
		function getDiscriminator(){
			return $this->mDiscriminator;
		}
		function getListingId(){
			return $this->mListingId;
		}
		function getParentId(){
			return $this->mParentId;
		}
		function getSource(){
			return $this->mSource;
		}
		function getSendableId(){
			return $this->mSendableId;
		}
		function getDestination(){
			return $this->mDestination;
		}

		/**
		 * Added by Naldz
		 * January 29, 2008
		 */
		function getMessageSource(){
			require_once('travellog/model/Class.MessageSource.php');
			return $this->getMessageId() ? new MessageSource($this) : null;
		}
		function getRecipients(){
			require_once('travellog/model/Class.MessageRecipient.php');
			$dests = $this->getDestination();
			$recipients = array();
			foreach($dests as $iDest){
				if($iDest instanceof Group){
					if($iDest->isSubGroup()){
						return array(new MessageRecipient($this,$iDest));	
					}
					$recipients[] = new MessageRecipient($this,$iDest);
				}
			}
			return $recipients;
		}
		/******************************* END: GETTERS *************************************/

		/**
		 * Function name: isRead
		 * @return bool
		 */		
		function isRead(){			
			// Boolean: Message is read or unread		
			if ($this->mRead == 0)
				return false;
			else
				return true;
		}
		
		/**
		 * Function name: isOwner
		 * @param int $_attributeId
		 * @param int $_sendableId
		 * @param string $_messageType
		 * @return bool
		 */		
		public static function isOwner($_attributeId = 0, $_sendableId = 0, $_messageType = ''){

			/****************** FOR NOW $_sendableId IS ALWAYS TRAVELER **************************/
			// if INBOX
			if (!strcmp($_messageType, 'inbox'))
				$isOwner = Inbox::isOwner($_attributeId, $_sendableId);
			// if SENT
			else if (!strcmp($_messageType, 'sent'))
				$isOwner = SentItems::isOwner($_attributeId, $_sendableId);
			// if DRAFTS
			else if (!strcmp($_messageType, 'drafts'))
				$isOwner = Drafts::isOwner($_attributeId, $_sendableId);
			// if TRASH
			else if (!strcmp($_messageType, 'trash'))
				$isOwner = Trash::isOwner($_attributeId, $_sendableId);

			return $isOwner;
		}

		/**
		 * user defined sort function to be called when sorting by date
		 * edited by K. Gordo 
		 */
		function orderByDate($l,$r){
			if ($l->getCreated() == $r->getCreated())
			   return 0;
			elseif ($l->getCreated() < $r->getCreated())
				return -1;
			else 
				return 1;	  
		}

		/**
		 * Function name: Send
		 * @abstract
		 */
		abstract protected function Send();
	}
?>