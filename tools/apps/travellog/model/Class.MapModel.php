<?php
class MapModel{
	function Get( $arr_filter ){
		$arr_trips  = $arr_filter['arr_trips'];
		$arr_marker = NULL;
		if( count($arr_trips) ){
			require_once("travellog/helper/Class.HelperFactory.php");
			require_once("travellog/model/Class.LocationMarker.php");
			
			$obj_factory      = HelperFactory::instance();
			$arr_marker       = array();
			$arr_marker_map   = array();
			$obj_entry_helper = $obj_factory->create( HelperType::$JOURNAL_ENTRY ); 
			
			foreach( $arr_trips as $obj_trip ){
				try{
					$arr_entry = $obj_trip->getTravelLogs();
					if( count($arr_entry) ){
						$obj_marker = new LocationMarker;
						$obj_marker->setLocation( $obj_trip->getLocation() );
						$obj_marker->setLink( $obj_entry_helper->getLink( $arr_entry[0] ) );
					
						$arr_marker[] = $obj_marker;
						$arr_marker_map[$arr_entry[0]->getTravelLogID()] = count($arr_marker)-1;
					}
				}catch(exception $e){
				}
			}
			
			if( array_key_exists( $arr_filter['travellogID'], $arr_marker_map ) )
				$arr_marker[$arr_marker_map[$arr_filter['travellogID']]]->setFocus();
			
		}	
		return $arr_marker;
	}
	
	
	function GetTravelerMap(&$props){
		$file_factory = FileFactory::getInstance();
		$arr_marker     = array();
		$col_locations  = array();
		if( $props['view_type'] == 7 ){
			$obj           = $file_factory->getClass('PlanningToGoTo');
			$col_locations = $obj->GetTravelersCountryWithFutureTravels(); 
			$action        = 'PlanningToGo';
		}
		elseif( $props['view_type'] == 8 ){
			$obj           = $file_factory->getClass('HaveBeenTo');
			$col_locations = $obj->GetTravelersCountryWithPastTravels();
			$action        = 'HaveBeenTo';
		}
		else{
			$obj           = $file_factory->getClass('CurrentTravelers');  
			$col_locations = $obj->GetTravelersCountryWithCurrentLocation(); 
			$action        = 'CurrentlyIn';
		}
		if( count($col_locations) ){
			require_once("travellog/model/Class.LocationMarker.php");
			require_once("travellog/model/Class.LocationFactory.php");
			$arr_marker_map = array();
			$obj_factory    = LocationFactory::instance();  
			foreach( $col_locations as $arr ){
				$obj_marker   = new LocationMarker;
				//$obj_location = $obj_factory->create($arr['locationID']);	
				$country = new Country();
				$country->setLocationID($arr['locationID']);
				$obj_marker->setLocation($country);
				//TODO: make the tooltip a little small to fit the map size
				//$obj_marker->setTxt($arr['name']);
				$obj_marker->setLink( '/travelers.php?action='.$action.'&locationID='.$arr['locationID'] );
				$arr_marker[] = $obj_marker;
			}
		}
		$props['arr_marker'] = $arr_marker; 
	}
	
	function GetArticleMap($arr_filter){
		$arr_marker = NULL;
		if( count($arr_filter) ){
			require_once("travellog/model/Class.LocationMarker.php");
			require_once("travellog/model/Class.LocationFactory.php");
			
			$arr_marker       = array();
			//$arr_marker_map   = array();
			$factory = LocationFactory::instance();
			foreach( $arr_filter as $obj_article ){
				$obj_marker = new LocationMarker;	
				$obj_marker->setLocation( $factory->create($obj_article->getLocID()) );
				$obj_marker->setLink( '/article.php?action=view&articleID='.$obj_article->getArticleID() );
					
				$arr_marker[] = $obj_marker;
				//$arr_marker_map[$arr_entry[0]->getTravelLogID()] = count($arr_marker)-1;
			}
		}
			
		/*if( array_key_exists( $arr_filter['travellogID'], $arr_marker_map ) )
			$arr_marker[$arr_marker_map[$arr_filter['travellogID']]]->setFocus();
			
		}*/	
		return $arr_marker;
	}
}
?>
