<?php
/**
 * Created on September 11, 2006
 * Class.MessageType.php
 * @author Cheryl Ivy Q. Go
 */
 
	class MessageType{
	
		public static $INBOX		= 1;
		public static $SENT			= 2;
		public static $DRAFTS		= 3;
		public static $INQUIRYBOX	= 4;
		public static $SPAMBOX		= 5;
	}
?>
