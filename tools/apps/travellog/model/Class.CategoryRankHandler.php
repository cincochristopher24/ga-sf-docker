<?php
	/**
	 * @(#) Class.CategoryRankHandler.php
	 * 
	 * Handler of updating the ranking of the categories.
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - Feb. 5, 2009
	 */
	
	class CategoryRankHandler {
		/**
		 * Changes the rank of the categories. The values in the array will be interpreted as the category ID
		 * and the key in the array will be the rank of the category.
		 * 
		 * @throws exception
		 * @param array $subgroupIDs The IDs of the subgroups to be set the ranks.
		 * @return void
		 */
		static function arrangeRankOfCategories(array $categoryIDs) {
		  try {
		  	$handler = new dbHandler();

				foreach($categoryIDs as $key=>$val) {
					$sql = "UPDATE tblSubGroupCategory SET `categoryRank` = ".$handler->makeSqlSafeString($key)." WHERE categoryID = ".$handler->makeSqlSafeString($val);
					$handler->execute($sql);
				}
		  }
		  catch (exception $ex){
		  	throw $ex;
		  }
		}
		
		/**
		 * Changes the rank of the subgroups. The values in the array will be interpreted as the subgroup ID
		 * and the key in the array will be the rank of the subgroup.
		 * 
		 * @throws exception
		 * @param array $subgroupIDs The IDs of the subgroups to be set the ranks.
		 * @return void
		 */
		static function arrangeRankOfSubGroups(array $subgroupIDs) {
		  try {
		  	$handler = new dbHandler();

				foreach($subgroupIDs as $key=>$val) {
					$sql = "UPDATE tblGroupToSubGroupCategory SET `categorysubgrouprank` = ".$handler->makeSqlSafeString($key)." WHERE `groupID` = ".$handler->makeSqlSafeString($val);
					$handler->execute($sql);
				}
		  }
		  catch (exception $ex){
		  	throw $ex;
		  }
		}
	}