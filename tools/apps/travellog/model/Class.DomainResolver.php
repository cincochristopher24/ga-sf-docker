<?php
	/***
		Author: Naldz Castellano III
		Date: Nov. 28, 2008
	***/
	class DomainResolver
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)) self::$instance = new self();
			return self::$instance;
		}
		private function __construct(){}
		public function resolve($domain=null){
			$tempDomain = str_ireplace(array('.local','dev.'),'',$domain);
			return (2==count(explode('.',$tempDomain)) ? 'www.' : '').$tempDomain;
		}
	}
?>