<?php
/*
 * Created on Aug 7, 2006
 */
 
 
require_once("Class.Connection.php");
require_once("Class.Recordset.php");
require_once("Class.WorkExperience.php");
require_once("Class.ResumeOptions.php");
require_once("Class.Country.php");
require_once("Class.Photo.php");
require_once("Class.PathManager.php");
require_once("Class.TravelerProfile.php");




class Resume
{
	
	private $travelerID;
	private $resumeID;
	private $workexperienceID;
	private $acaddegreeID;
	private $acaddegree;
	private $majorID;
	private $major;
	private $univName;
	private $univCountryID;
	private $univcountry;
	private $acaddegreeID2;
	private $acaddegree2;
	private $majorID2;
	private $major2;
	private $univName2;
	private $univCountryID2;
	private $univcountry2;
	private $certName;
	private $yearObtained;
	private $certName2;
	private $yearObtained2;
	private $certName3;
	private $yearObtained3;
	private $isCurrEmployed;
	private $isTeacher;
	private $currJob;
	private $prefEmployment;
	private $moreDetails;
	private $jobTypeID;
	private $jobtype;
	private $prefCountryID;
	private $prefcountry;
	private $prefCity;
	private $workWhen;
	private $englishproficiencyID;
	private $engprof;
	private $preferredteachinglevelID;
	private $prefteachlevel;
	private $teachingpositionID;
	private $teachpos;
	private $teachingyearsID;
	private $teachyrs;
	private $datecreated;
	private $isNewsletterSubscribed;
	private $dateedited;
	private $otherteachingposition;
	private $otherteachinglevel;
	private $showresume;
	private $phone;
	private $address1;
	private $address2;
	
	private $arrphotos    = array();
	private $primphoto   = NULL;
	private $mResourceFiles = array();
	
	private $randphoto    = NULL;
	public  $phototype	= 3;
	
	public  $resourcefiletype	= 2;
	
	// setters
		
		function settravelerID($_travelerID)
		{
			$this->travelerID = $_travelerID;
		}
		
		function setresumeID($_resumeID)
		{
			$this->resumeID = $_resumeID;
		}
		
		function setphone($_phone)
		{
			$this->phone = $_phone;
		}
		
		function setaddress1($_address1)
		{
			$this->address1 = $_address1;
		}
		
		function setaddress2($_address2)
		{
			$this->address2 = $_address2;	
		}
		
		function setacaddegreeID($_acaddegreeID)
		{
			$this->acaddegreeID = $_acaddegreeID;
		}
		
		function setmajorID($_majorID)
		{
			$this->majorID = $_majorID;
		}
		
		function setunivName($_univName)
		{
			$this->univName = $_univName;
		}
		
		function setunivCountryID($_univCountryID)
		{
			$this->univCountryID = $_univCountryID;
		}
		
		function setacaddegreeID2($_acaddegreeID2)
		{
			$this->acaddegreeID2 = $_acaddegreeID2;
		}
		
		function setmajorID2($_majorID2)
		{
			$this->majorID2 = $_majorID2;
		}
		
		function setunivName2($_univName2)
		{
			$this->univName2 = $_univName2;
		}
		
		function setunivCountryID2($_univCountryID2)
		{
			$this->univCountryID2 = $_univCountryID2;
		}
		
		function setcertName($_certName)
		{
			$this->certName = $_certName;
		}
		
		function setyearObtained($_yearObtained)
		{
			$this->yearObtained = $_yearObtained;
		}
		
		function setcertName2($_certName2)
		{
			$this->certName2 = $_certName2;
		}
		
		function setyearObtained2($_yearObtained2)
		{
			$this->yearObtained2 = $_yearObtained2;
		}
		
		function setcertName3($_certName3)
		{
			$this->certName3 = $_certName3;
		}
		
		function setyearObtained3($_yearObtained3)
		{
			$this->yearObtained3 = $_yearObtained3;
		}
		
		function setisCurrEmployed($_isCurrEmployed)
		{
			$this->isCurrEmployed = $_isCurrEmployed;
		}
		
		function setisTeacher($_isTeacher)
		{
			$this->isTeacher = $_isTeacher;
		}
		
		function setcurrJob($_currJob)
		{
			$this->currJob = $_currJob;
		}
		
		function setprefEmployment($_prefEmployment)
		{
			$this->prefEmployment = $_prefEmployment;
		}
		
		function setmoreDetails($_moreDetails)
		{
			$this->moreDetails = $_moreDetails;
		}
		
		function setjobTypeID($_jobTypeID)
		{
			$this->jobTypeID = $_jobTypeID;
		}
		
		function setprefCountryID($_prefCountryID)
		{
			$this->prefCountryID = $_prefCountryID;
		}
		
		function setprefCity($_prefCity)
		{
			$this->prefCity = $_prefCity;
		}
		
		function setworkWhen($_workWhen)
		{
			$this->workWhen = $_workWhen;
		}
		
		function setenglishproficiencyID($_englishproficiencyID)
		{
			$this->englishproficiencyID = $_englishproficiencyID;
		}
		
		function setpreferredteachinglevelID($_preferredteachinglevelID)
		{
			$this->preferredteachinglevelID = $_preferredteachinglevelID;
		}
		
		function setteachingpositionID($_teachingpositionID)
		{
			$this->teachingpositionID = $_teachingpositionID;
		}
		
		function setteachingyearsID($_teachingyearsID)
		{
			$this->teachingyearsID = $_teachingyearsID;
		}
		
		function setdatecreated($_datecreated)
		{
			$this->datecreated = $_datecreated;
		}
		
		function setisNewsletterSubscribed($_isNewsletterSubscribed)
		{
			$this->isNewsletterSubscribed = $_isNewsletterSubscribed;
		}
		
		function setdateedited($_dateedited)
		{
			$this->dateedited = $_dateedited;
		}
		
		function setotherteachingposition($_otherteachingposition)
		{
			$this->otherteachingposition = $_otherteachingposition;
		}
		
		function setotherteachinglevel($_otherteachinglevel)
		{
			$this->otherteachinglevel = $_otherteachinglevel;
		}
		
		function setshowresume($_showresume)
		{
			$this->showresume = $_showresume;
		}
		
		
		
	//getters
		
		function gettravelerID()
		{
			return $this->travelerID; 
		}
		
		function getresumeID()
		{
			return $this->resumeID;
		}
		
		function getphone()
		{
			return $this->phone;	
		}
		
		function getaddress1()
		{
			return $this->address1;
		}
		
		function getaddress2()
		{
			return $this->address2;
		}
		
		function getacaddegreeID()
		{
			return $this->acaddegreeID;
		}
		
		function getacaddegree()
		{
			return $this->acaddegree;
		}
		
		function getmajor()
		{
			return $this->major;
		}
		
		function getmajorID()
		{
			return $this->majorID;
		}
		
		function getunivName()
		{
			return $this->univName;
		}
		
		function getunivCountryID()
		{
			return $this->univCountryID;
		}
		
		function getunivcountry()
		{
			return $this->univcountry;
		}
		
		function getacaddegreeID2()
		{
			return $this->acaddegreeID2;
		}
		
		function getacaddegree2()
		{
			return $this->acaddegree2;
		}
		
		function getmajorID2()
		{
			return $this->majorID2;
		}
		
		function getmajor2()
		{
			return $this->major2;
		}
		
		function getunivName2()
		{
			return $this->univName2;
		}
		
		function getunivCountryID2()
		{
			return $this->univCountryID2;
		}
		
		function getunivcountry2()
		{
			return $this->univcountry2;
		}
		
		function getcertName()
		{
			return $this->certName;
		}
		
		function getyearObtained()
		{
			return $this->yearObtained;
		}
		
		function getcertName2()
		{
			return $this->certName2;
		}
		
		function getyearObtained2()
		{
			return $this->yearObtained2;
		}
		
		function getcertName3()
		{
			return $this->certName3;
		}
		
		function getyearObtained3()
		{
			return $this->yearObtained3;
		}
		
		function getisCurrEmployed()
		{
			return $this->isCurrEmployed;
		}
		
		function getisTeacher()
		{
			return $this->isTeacher;
		}
		
		function getcurrJob()
		{
			return $this->currJob;
		}
		
		function getprefEmployment()
		{
			return $this->prefEmployment;
		}
		
		function getmoreDetails()
		{
			return $this->moreDetails;
		}
		
		function getjobTypeID()
		{
			return $this->jobTypeID;
		}
		
		function getjobtype()
		{
			return $this->jobtype;
		}
		
		function getprefCountryID()
		{
			return $this->prefCountryID;
		}
		
		function getprefcountry()
		{
			return $this->prefcountry;
		}
		
		function getprefCity()
		{
			return $this->prefCity;
		}
		
		function getworkWhen()
		{
			return $this->workWhen;
		}
		
		function getenglishproficiencyID()
		{
			return $this->englishproficiencyID;
		}
		
		function getengprof()
		{
			return $this->engprof;
		}
		
		function getpreferredteachinglevelID()
		{
			return $this->preferredteachinglevelID;
		}
		
		function getprefteachlevel()
		{
			return $this->prefteachlevel;
		}
		
		function getteachingpositionID()
		{
			return $this->teachingpositionID;
		}
		
		function getteachpos()
		{
			return $this->teachpos;
		}
		
		function getteachingyearsID()
		{
			return $this->teachingyearsID;
		}
		
		function getteachyrs()
		{
			return $this->teachyrs;
		}
		
		function getdatecreated()
		{
			return $this->datecreated;
		}
		
		function getisNewsletterSubscribed()
		{
			return $this->isNewsletterSubscribed;
		}
		
		function getdateedited()
		{
			return $this->dateedited;
		}
		
		function getotherteachingposition()
		{
			return $this->otherteachingposition;
		}
		
		function getotherteachinglevel()
		{
			return $this->otherteachinglevel;
		}
		
		function getshowresume()
		{
			return $this->showresume;
		}
		
		
		
		// constructor
		
		function Resume($travelerID=0) {
			$conn = new Connection();
			$rs = new Recordset($conn);
			
			$this->travelerID =$travelerID;
			
			$qry = "select * from tblUserResume where travelerID='$travelerID'";
			$rs->Execute($qry);
			
			
			$country = new Country($rs->Result(0,"prefCountryID"));
			$prefcountryname = $country->getName();
			$country = new Country($rs->Result(0,"univCountryID"));
			$univcountryname = $country->getName();
			$country = new Country($rs->Result(0,"univCountryID2"));
			$univcountryname2 = $country->getName();
			
			$acaddegrees = ResumeOptions::getAcadDegreeList();
			$interntypes = ResumeOptions::getInternTypeList();
			$englishprof = ResumeOptions::getEnglishProfList();
			$prefteachlevel = ResumeOptions::getTeachingLevelList();
			$teachpos = ResumeOptions::getTeachingPositionList();
			$teachyrs = ResumeOptions::getTeachYearsList();
				
				
					
			// if traveler has a resume
			if ($rs->RecordCount() > 0){				
				
				$this->resumeID = $rs->Result(0,"resumeID");
				//$this->phone = $rs->Result(0,"phone");
				//$this->address1 = $rs->Result(0,"address1");
				//$this->address2 = $rs->Result(0,"address2");
				$this->acaddegreeID = $rs->Result(0,"acaddegreeID");
				
				if($rs->Result(0, "acaddegreeID") != 0)
					$this->acaddegree = $acaddegrees[$rs->Result(0, "acaddegreeID")];
					
				$this->majorID = $rs->Result(0,"majorID");
				$this->major = $interntypes[$rs->Result(0,"majorID")]; 
				$this->univName = $rs->Result(0,"univName");
				$this->univCountryID = $rs->Result(0,"univCountryID");
				$this->univcountry = $univcountryname;
				$this->acaddegreeID2 = $rs->Result(0,"acaddegreeID2");
				
				if($rs->Result(0, "acaddegreeID2") != 0)
					$this->acaddegree2 = $acaddegrees[$rs->Result(0, "acaddegreeID2")];
				
				$this->majorID2 = $rs->Result(0,"majorID2");
				if($rs->Result(0,"majorID2") != 0)
					$this->major2 = $interntypes[$rs->Result(0,"majorID2")];
					 
				$this->univName2 = $rs->Result(0,"univName2");
				$this->univCountryID2 = $rs->Result(0,"univCountryID2");
				$this->univcountry2 = $univcountryname2;
				$this->certName = $rs->Result(0,"certName");
				$this->yearObtained = $rs->Result(0,"yearObtained");
				$this->certName2 = $rs->Result(0,"certName2");
				$this->yearObtained2 = $rs->Result(0,"yearObtained2");
				$this->certName3 = $rs->Result(0,"certName3");
				$this->yearObtained3 = $rs->Result(0,"yearObtained3");
					
				$this->isCurrEmployed = $rs->Result(0,"isCurrEmployed");
				$this->isTeacher = $rs->Result(0,"isTeacher");
				$this->currJob = $rs->Result(0,"currJob");
				$this->prefEmployment = $rs->Result(0,"prefEmployment");
				$this->moreDetails = $rs->Result(0,"moreDetails");
				$this->jobTypeID = $rs->Result(0,"jobTypeID");
				$this->jobtype = $interntypes[$rs->Result(0,"jobTypeID")]; 
				$this->prefCountryID = $rs->Result(0,"prefCountryID");
				$this->prefcountry = $prefcountryname;
				$this->prefCity = $rs->Result(0,"prefCity");
				$this->workWhen = $rs->Result(0,"workWhen");
				
				$this->englishproficiencyID = $rs->Result(0,"englishproficiencyID");
				if($rs->Result(0,"englishproficiencyID"))
					$this->engprof = $englishprof[$rs->Result(0,"englishproficiencyID")];
				
				$this->preferredteachinglevelID = $rs->Result(0,"preferredteachinglevelID");
				if($rs->Result(0,"preferredteachinglevelID") && $rs->Result(0,"preferredteachinglevelID") != 9)
					$this->prefteachlevel = $prefteachlevel[$rs->Result(0,"preferredteachinglevelID")];
				else
					$this->otherteachinglevel = $rs->Result(0,"otherteachinglevel");				// -
						
				$this->teachingpositionID = $rs->Result(0,"teachingpositionID");
				if($rs->Result(0,"teachingpositionID") && $rs->Result(0,"teachingpositionID") != 34)
					$this->teachpos = $teachpos[$rs->Result(0,"teachingpositionID")];
				else
					$this->otherteachingposition = $rs->Result(0,"otherteachingposition");			// -
						 
				$this->teachingyearsID = $rs->Result(0,"teachingyearsID");
				if($rs->Result(0,"teachingyearsID"))
					$this->teachyrs = $teachyrs[$rs->Result(0,"teachingyearsID")];
					
				$this->datecreated = $rs->Result(0,"datecreated");
				$this->isNewsletterSubscribed = $rs->Result(0,"isNewsletterSubscribed");
				$this->dateedited = $rs->Result(0,"dateedited");
				$this->showresume = $rs->Result(0,"isHidden");				
				//$this->travelerID = $rs->Result(0,"travelerID");
						
				return $this;
				
					
			}
			elseif ($rs->RecordCount() == 0 && $travelerID > 0) {				
				throw new Exception("Invalid Traveler ID");
			}
			 
					
		}
		
		
	// CRU methods
		
		
			function Create() // insert resume
			{
						
						$conn = new Connection();
						$rs = new Recordset($conn);
					
						$qry = "select * from tblUserResume where travelerID='$this->travelerID'";
						$rs->Execute($qry);
						
						if ($rs->RecordCount() == 0)
						{
							
							
							//$phonenum = addslashes($this->phone);
							//$faddress = addslashes($this->address1);
							//$saddress = addslashes($this->address2);
							$funivName = addslashes($this->univName);
							$sunivName = addslashes($this->univName2);
							$fcertName = addslashes($this->certName);
							$scertName = addslashes($this->certName2);
							$tcertName = addslashes($this->certName3);
							$fyear = addslashes($this->yearObtained);
							$syear = addslashes($this->yearObtained2);
							$tyear = addslashes($this->yearObtained3);
							$slashcurrJob = addslashes($this->currJob); 
							$slashcity = addslashes($this->prefCity);
							$slashworkWhen = addslashes($this->workWhen); 
							$slashmoreDetails = addslashes($this->moreDetails);
							$slashteachpos = addslashes($this->otherteachingposition);
							$slashteachlevel = addslashes($this->otherteachinglevel);
							 
						 	$sql = "insert into `tblUserResume` (`travelerID`, `isHidden`, `acaddegreeID`, `acaddegreeID2`, `majorID`, `majorID2`, `univName`, `univName2`, `univCountryID`, `univCountryID2`,
						 				`certName`, `yearObtained`, `certName2`, `yearObtained2`, `certName3`, `yearObtained3`, `isCurrEmployed`, `isTeacher`, `currJob`, `prefEmployment`, `moreDetails`, 
						 				`jobTypeID`, `prefCountryID`, `prefCity`, `workWhen`, `englishproficiencyID`, `preferredteachinglevelID`, `otherteachinglevel`, `teachingpositionID`, `otherteachingposition`,
						 				`teachingyearsID`, `datecreated`, `isNewsletterSubscribed`)
							 	VALUES
							 			('$this->travelerID', '$this->showresume', '$this->acaddegreeID', '$this->acaddegreeID2', '$this->majorID', '$this->majorID2', '$funivName', '$sunivName', 
							 			'$this->univCountryID', '$this->univCountryID2', '$fcertName', '$fyear', '$scertName', '$syear', '$tcertName', '$tyear', '$this->isCurrEmployed', '$this->isTeacher', '$slashcurrJob',
							 			'$this->prefEmployment', '$slashmoreDetails', '$this->jobTypeID', '$this->prefCountryID', '$slashcity', '$slashworkWhen', '$this->englishproficiencyID', '$this->preferredteachinglevelID',
							 			'$slashteachlevel', '$this->teachingpositionID', '$slashteachpos', '$this->teachingyearsID', '$this->datecreated', '$this->isNewsletterSubscribed')";
							
							$rs->Execute($sql);	
											
							$this->resumeID = $rs->GetCurrentId();
							
							
							return $this->resumeID;
						}
						
						
			}
		
		
		function Update() // update resume
		{
			
					$conn = new Connection();
					$rs = new Recordset($conn);
					
					//$phonenum = addslashes($this->phone);
					//$faddress = addslashes($this->address1);
					//$saddress = addslashes($this->address2);
					$funivName = addslashes($this->univName);
					$sunivName = addslashes($this->univName2);
					$fcertName = addslashes($this->certName);
					$scertName = addslashes($this->certName2);
					$tcertName = addslashes($this->certName3);
					$fyear = addslashes($this->yearObtained);
					$syear = addslashes($this->yearObtained2);
					$tyear = addslashes($this->yearObtained3);
					$slashcurrJob = addslashes($this->currJob); 
					$slashcity = addslashes($this->prefCity);
					$slashworkWhen = addslashes($this->workWhen); 
					$slashmoreDetails = addslashes($this->moreDetails);
					$slashteachpos = addslashes($this->otherteachingposition);
					$slashteachlevel = addslashes($this->otherteachinglevel);
					
					$sql = "update `tblUserResume`set `isHidden` = '$this->showresume', `acaddegreeID` = '$this->acaddegreeID', `acaddegreeID2` = '$this->acaddegreeID2', `majorID` = '$this->majorID',
							`majorID2` = '$this->majorID2', `univName` = '$funivName', `univName2` = '$sunivName', `univCountryID` = '$this->univCountryID', `univCountryID2` = '$this->univCountryID2',
							`certName` = '$fcertName', `yearObtained` = '$fyear', `certName2` = '$scertName', `yearObtained2` = '$syear', `certName3` = '$tcertName', `yearObtained3` = '$tyear',
							`isCurrEmployed` = '$this->isCurrEmployed', `isTeacher` = '$this->isTeacher', `currJob` = '$slashcurrJob', `prefEmployment` = '$this->prefEmployment', 
							`moreDetails` = '$slashmoreDetails', `jobTypeID` = '$this->jobTypeID', `prefCountryID` = '$this->prefCountryID', `prefCity` = '$slashcity', `workWhen` = '$slashworkWhen',
							`englishproficiencyID` = '$this->englishproficiencyID', `preferredteachinglevelID` = '$this->preferredteachinglevelID', `otherteachinglevel` = '$slashteachlevel',
							`teachingpositionID` = '$this->teachingpositionID', `otherteachingposition` = '$slashteachpos', `teachingyearsID` = '$this->teachingyearsID',
							`isNewsletterSubscribed` = '$this->isNewsletterSubscribed', `dateedited` = '$this->dateedited' where `travelerID` = '$this->travelerID'";
					$rs->Execute($sql);
			
		}
		
	
		
		// other methods
		
		
		// get photo of the specified resume
	 		function getPhotos(){
				/*
				$conn = new Connection();
				$rs = new Recordset($conn);
				
				if (count($this->arrphotos) == 0){
					
					$sql = "SELECT photoID from tblResumetoPhoto WHERE travelerID = '$this->travelerID'  ORDER BY position ASC";
	 				$rs->Execute($sql);
	 				
	 				if ($rs->Recordcount() == 0){
	 					return NULL;	 			
	 				}
		 			
		 			else {
			 			while ($recordset = mysql_fetch_array($rs->Resultset())) {
		 					try {
		 						$photo = new Photo($this, $recordset['photoID']);
		 					}
							catch (Exception $e) {						  
							   throw $e;
							}
		 					$this->arrphotos[] = $photo;
		 				}
		 			}
				}
				
				return $this->arrphotos;
				*/
				
				try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
 			
 				$sql = "SELECT tblPhoto.photoID from tblUserResume,tblPhoto WHERE tblUserResume.photoID = tblPhoto.photoID AND  tblUserResume.travelerID = '$this->travelerID'";
 				$rs->Execute($sql);
 			
 				$recordset = mysql_fetch_array($rs->Resultset());
 					 				
 				if($rs->Recordcount()){
 					try {	
 						$photo = new Photo($this, $recordset['photoID']);
 					}
					catch (Exception $e) {
					   throw $e;
					}
 					$this->arrphotos[] = $photo;
 				}
 				
				return $this->arrphotos;
				
			} 
			
			
			// get primary photo of Resume 
	 		function getPrimaryPhoto(){
	 			
				$conn = new Connection();
				$rs = new Recordset($conn);
				
	 			if ($this->primphoto == NULL){
	 				
	 				$sql = "SELECT tblPhoto.photoID from tblUserResume,tblPhoto WHERE tblUserResume.photoID = tblPhoto.photoID AND  tblUserResume.travelerID = '$this->travelerID'";
 					$rs->Execute($sql);
	 				
	 				
	 				if ($rs->Recordcount() == 0)	// if no photo records yet, set to default value zero
	 					$_photoID = 0;	 					
	 				else 
						$_photoID = $rs->Result(0,"photoID"); 			
					
					
					
						
					try {	
						$this->primphoto = new Photo($this, $_photoID);							
					}
					catch (Exception $e) {
					   throw $e;
					}	
	 			}
	 			
	 			
	 			return $this->primphoto;
	 			
	 		}
	 		
	 	// returns recordcount of work experience per traveler 	
			
		function getWorkExpCount()
		{
				
					$conn = new Connection();
					$rs = new Recordset($conn);
					
					$sql = "select `travelerID` from `tblWorkExperience` where `travelerID` = '$this->travelerID'";
					$rs->Execute($sql);
					
			
					return $rs->Recordcount();
		}
		
		
		// returns the list of work experience(s) per traveler
		
		function getWorkExpList()
		{
				
					$conn = new Connection();
					$rs = new Recordset($conn);
					
					
					$qwork = "select * from `tblWorkExperience` where `travelerID` = '$this->travelerID' order by `workexperienceID`";
					$results = $rs->Execute($qwork);
					$rs->Close();
					
					$workexp = array();
					
					if($rs->Recordcount())
					{
							
							while ($row = mysql_fetch_assoc($results))
							{
								
									$workexperience = new WorkExperience();
									$workexperience->setworkexperienceID($row['workexperienceID']);
									$workexperience->setJobPosition($row['JobPosition']);
									$workexperience->setEmployer($row['Employer']);
									$workexperience->setJobDescription($row['JobDescription']);
									$workexperience->setLenServ($row['LenServ']);
									$workexp[] = $workexperience;
									 			
							}
					}
					
					
					return $workexp;
								
		}
		
		
		// returns the professional certification of each traveler
		
		function getCertification()
		{
			
				$conn = new Connection();
				$rs = new Recordset($conn);
					
				$qcert = "select `certName`, `yearObtained`, `certName2`, `yearObtained2`, `certName3`, `yearObtained3` from `tblUserResume` where `travelerID` = '$this->travelerID'";
				$results = $rs->Execute($qcert);	
				$rs->Close();
				
				$certifications = array();
				
				if($rs->Recordcount())
				{
					
					
					while ($row = mysql_fetch_assoc($results))
					{
							if ( strlen($row['certName']) ||  strlen($row['yearObtained']) || strlen($row['certName2']) || strlen($row['yearObtained2']) || strlen($row['certName3']) || strlen($row['yearObtained3']))
							{		
									$resume = new Resume();
									$resume->setcertName($row['certName']);
									$resume->setyearObtained($row['yearObtained']);
									$resume->setcertName2($row['certName2']);
									$resume->setyearObtained2($row['yearObtained2']);
									$resume->setcertName3($row['certName3']);
									$resume->setyearObtained3($row['yearObtained3']);
									$certifications[] = $resume;
							}
					}
					
					
				}
				
				return $certifications;
				
		}
		
		
		// returns the recordcount of resourcefile
		function getResumeResourceFile()
		{
			
			$conn = new Connection();
			$rs = new Recordset($conn);
			
			$sql = "select * from tblResumetoResourceFile where travelerID = '$this->travelerID'";
			$rs->Execute($sql);
			
			
			return $rs->Recordcount();
			
			
		}
		
		
		// new functions added by daphne 09/12/2006
		
	 		//Purpose: Adds photo for this resume.
	         
            function addPhoto($photo){
                /*
                $conn = new Connection();
				$rs = new Recordset($conn);
				
				//editted by: joel
            	//################################
            	$sqlq = "SELECT max(position) as mposition from tblResumetoPhoto WHERE travelerID = '$this->travelerID'";
	 			$rs->Execute($sqlq);
            	
            	
            	if($rs->Recordcount() > 0){
            			$rec = mysql_fetch_array($rs->Resultset());
            			$pos = $rec['mposition']+1;           			
            	}else{
            			$pos =0;	
            	}
            	//################################
				
					
                $photoID = $photo->getPhotoID();   
                $sql = "INSERT into tblResumetoPhoto (travelerID, photoID, position) VALUES ('$this->travelerID', '$photoID', '$pos' )";
		 		$rs->Execute($sql);
		 		*/
		 		
		 		try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
                
                
                if($this->getPhotos()){
               		$objphoto = $this->getPhotos();
                	
                	require_once('travellog/model/Class.UploadManager.php');
               		$uploadmanager = new UploadManager();
					$uploadmanager->setContext($this);	
					$uploadmanager->setTypeUpload('image');
					$uploadmanager->deletefile($objphoto[0]->getFileName());
                }
                
                $photoID = $photo->getPhotoID();
                $sql = "UPDATE tblUserResume SET photoID = '$photoID' WHERE `travelerID` = '$this->travelerID'";
                $rs->Execute($sql);      
		 		
		 				 				 		
            }
		
			// Purpose: Removes a photo from this photo album.
	        //  Deletes the photo as well as its link to this program.
	        function removePhoto($photoID){
               
                $conn = new Connection();
				$rs = new Recordset($conn);
				
				/*	
                $sql = "DELETE FROM tblResumetoPhoto where photoID = '$photoID' ";
		 		$rs->Execute($sql);
		 		*/
		 		
		 		$sql = "UPDATE tblUserResume SET photoID =0 WHERE `travelerID` = '$this->travelerID'";
	 			$rs->Execute($sql);   
		 		 
            }
            
            //added by: joel
            function reArrangePhoto($marrphotoID){
            	
            	$conn = new Connection();
				$rs = new Recordset($conn);
            	
            	foreach($marrphotoID as $index => $photoID){
            			 $sql = "UPDATE tblResumetoPhoto SET position = '$index' WHERE photoID =  '$photoID' ";
		 				$rs->Execute($sql);
            	}
            }
           
            
        /**
         * Purpose: Adds a resource file for this resume.
         */
            function addResourceFile($resourceFile){
                
                $conn = new Connection();
				$rs = new Recordset($conn);
				
                $resourceFileID = $resourceFile->getResourceFileID();
                $sql = "INSERT into tblResumetoResourceFile (travelerID, resourcefileID)  
                		VALUES ('$this->travelerID', '$resourceFileID')"; 				// insert link of resource file to this resume
                $rs->Execute($sql);
            }
            
        
        /**
         * Purpose: Removes a resource file from this resume.
         */  
            function removeResourceFile($resourceFileID){
                
                $conn = new Connection();
				$rs = new Recordset($conn);
				
                $sql = "DELETE FROM tblResumetoResourceFile 
                        WHERE resourcefileID = '$resourceFileID' ";      // delete link of resource file to this resume
                $rs->Execute($sql);
            }

		/**
         * Purpose: Returns an array of resource files of this resume.
         * Rows Limit is enabled for this function.
         */
            function getResourceFiles($rowslimit = NULL){
                
                if (0 == count($this->mResourceFiles)){
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    $conn = new Connection();
					$rs = new Recordset($conn);
				
                    $sql = "SELECT resourcefileID from tblResumetoResourceFile WHERE travelerID = '$this->travelerID' ORDER BY resourcefileID " . $limitstr;
                    $rs->Execute($sql);
                    
                    if (0 == $rs->Recordcount()){
                        return NULL;            
                    }
                    else {
                        while ($recordset = mysql_fetch_array($rs->Resultset())) {
                            try {
                                $resourcefile = new ResourceFiles($recordset['resourcefileID']);'' .
                                $resourcefile->setContext($this);
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                            $this->mResourceFiles[] = $resourcefile;
                        }
                    }               
                }
                
                return $this->mResourceFiles;
            }                
          
          
          // get random photo from the traveler profile
	 		function getRandomPhoto(){
	 			
	 			  $conn = new Connection();
				  $rs = new Recordset($conn);
	 					
	 			if ($this->randphoto == NULL){
	 				
	 				// get the number of photos to choose from
	 				$sql = "SELECT count(photoID) as totalrec from tblResumetoPhoto WHERE travelerID = '$this->travelerID' ";
	 				$rs->Execute($sql);
	 				
	 				if ($rs->Result(0,"totalrec") == 0){
	 					try {
	 						$this->randphoto = new Photo($this);
	 					}
						catch (Exception $e) {						  
						   throw $e;
						}
	 				}	 				
	 				else {
	 					
	 					$max = $rs->Result(0,"totalrec") - 1;
						
						$randidx = mt_rand(0 , $max );	 		// the index starting from top row
						
	 					$sql = "SELECT photoID FROM tblResumetoPhoto WHERE travelerID = '$this->travelerID' ORDER BY photoID LIMIT $randidx , 1"  ;
	 					$rs->Execute($sql);
	 					
	 					while ($recordset = mysql_fetch_array($rs->Resultset())) {
			 				try {	
			 					$this->randphoto = new Photo($this, $recordset['photoID'] );
			 				}
							catch (Exception $e) {						  
							   throw $e;
							}
	 					}
	 				}
	 			}
	 			
	 			return $this->randphoto;
	 		}
          	
          	
          	
          	/**
	 		 * Added by Joel 
	 		 * March 1 2007
	 		 * To check If Resume has primary photo
	 		 * @return Boolean 
	 		 */
            function checkHasprimaryphoto(){
	 			
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
	 			
	 			$sql = "SELECT primaryphoto from tblPhoto, tblResumetoPhoto " .
	 					"WHERE tblResumetoPhoto.photoID = tblPhoto.photoID " .
	 					"AND primaryphoto =1 " .
	 					"AND tblResumetoPhoto.travelerID ='".$this->travelerID."'" ;
				$rs->Execute($sql);
				
				
				if($rs->recordcount() == 0){
					return false;	
				}else{
					return true;
				}
            }
}
?>
