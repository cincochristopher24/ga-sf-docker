<?php
  /**
   * @(#) Class.SubGroupRankHandler.php
   * 
   * Class that handles the arranging of ranks of subgroups.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 01 5, 09
   */
   
  class SubGroupRankHandler {
  	/**
  	 * Arranges the ranks of the subgroups of the $category if it is an instance of 
  	 * CustomizedSubGroupCategory, the uncategorized subgroups of the given parent group
  	 * if $category is equal to uncategorized, else all the subgroups of the given parent group.
  	 * The arrangement depends on the given sort type. If sort type is equal to SubGroupSortType::ALPHABETICAL
  	 * the subgroups are arrange alphabetically, and if sort type equal to SubGroupSortType::MOST_RECENT_CREATED
  	 * the subgroups are arranged by date created decreasingly.
  	 * 
  	 * @throws exception
  	 * @param string|CustomizedSubGroupCategory $category Possible values (uncategorized - for subgroups that has no category, all - for all subgroups, instance of CustomizedSubGroupCategory for subgroups that has a category).
  	 * @param AdminGroup $parent The parent group of the subgroups that is to be arranged alphabetically.
		 * @param integer $sortType Values can be SubGroupSortType::ALPHABETICAL and SubGroupSortType::MOST_RECENT_CREATED only.
		 * @see SubGroupSortType
		 * @see AdminGroup
		 * @return void
  	 */
  	static function autoSortGroups($category, AdminGroup $parent, $sortType){
  		try {
  			if ($category instanceof CustomizedSubGroupCategory) {
  				$category->autoSortGroups($sortType);
  			}
  			else {
  				$parent->autoSortGroups($sortType);
  			}
  		}
  		catch (exception $ex){
  			throw new exception($ex->getMessage());
  		}
  	}
  	
		/**
		 * Change the ranking in the database of the given subgroup using the given rank. The given subgroup can 
		 * be a subgroup of a category or it can be an uncategorized subgroup. When the given category is an instance
		 * of CustomizedSubGroupCategory, the given subgroups rank in the subgroups of the said category will be affected,
		 * otherwise the given subgroups rank will be affected in the uncategorized subgroups of the given parent group.
		 * 
		 * @throws exception
		 * @param AdminGroup $subgroup the subgroup to be updated
		 * @param integer $rank the new rank of the subgroup
		 * @param string $action one of the following (showActive or showInActive)
		 * @param string|CustomizedSubGroupCategory $category The category of the subgroups to be arranged.
		 * @return void
		 */
		static function updateNewRankOfSubGroup($subgroup, $given_rank, $action, $category, $parent) {
      try {
      	if ($category instanceof CustomizedSubGroupCategory) {
        	$category->updateNewRankOfSubGroup($subgroup, $given_rank, $action);
      	}
        else {
        	$parent->updateNewRankOfSubGroup($subgroup, $given_rank, $action);
        }
      }
			catch (exception $ex){
				throw new exception($ex->getMessage);
			}
		}
		
		/**
		 * Changes the rank of the given subgroup IDs.
		 * 
		 * @throws exception
		 * @param array $_arrSubGroupID the array of sub group IDs to be updated
		 * @param integer $cnt the starting rank of the subgroup
		 */
		static function arrangeRankOfSubGroups($_arrSubGroupID = array()) {
		  try {
		  	$handler = new dbHandler();

				foreach($_arrSubGroupID as $key=>$val) {
					$sql = "UPDATE tblGroup SET subgrouprank = ".($key)." WHERE groupID = ".$handler->makeSqlSafeString($val);
					$handler->execute($sql);
				}
		  }
		  catch (exception $ex){
		  	throw $ex;
		  }
		}
  }
