<?php
	/***
	 * TO DO: Should group addressbook entries by thier status(GAnet and non-GAnet user) --- DONE!!!
	 */
	require_once("Class.dbHandler.php"); 
	require_once("Class.iRecordset.php");
	require_once("Class.AddressBookEntry.php");
	
	class AddressBook{
		const ALL = 0;
		const GA_NET_MEMBERS = 1;
		const GA_NET_NON_MEMBERS = 2;		
		private $ownerTravelerId;
		private $addressBookId;
		private $addressBookEntries = array();
		private $db = null;
		private $addressBookEntriesAreLoaded = false;		
				
		function __construct($mOwnerTravelerId=0){
			if(!$mOwnerTravelerId) {
				throw new exception('Owner Traveler ID was expected in class AddressBook but was not passed in.');
				return false;
			}
			//get the addressbook of the owner travell
			$this->db = new dbHandler();
			$this->ownerTravelerId = $mOwnerTravelerId;
			$qAddressBook = $this->getAddressBook($this->ownerTravelerId);
			$rsAddressBook = new iRecordset($qAddressBook);			
			if(!$rsAddressBook->retrieveRecordCount()){
				//check if the id really belongs to GAnet member
				$qOwner = $this->db->execQuery("select travelerID from Travel_Logs.tblTraveler where travelerID = $this->ownerTravelerId");
				$rsOwner = new iRecordset($qOwner);
				//If the id does not belong to a ganet member, issue an error message				
				if(!$rsOwner->retrieveRecordCount()){
					throw new exception('Owner Traveler ID does not exist in database.');
					return false;
				}
				//if the id belongs to a GAnet member, create a new address book for the member.
				else{
					$this->createAddressBook($this->ownerTravelerId);
					$qAddressBook = $this->getAddressBook($this->ownerTravelerId);
					$rsAddressBook = new iRecordset($qAddressBook);	
				}
			}			
			$this->addressBookId = $rsAddressBook->getaddressBookID();
			$this->updateAddressBookEntries();
		}
		
		public function getAddressBookEntries($addressBookEntriesStatus=AddressBook::ALL){
			if(!$this->addressBookEntriesAreLoaded) $this->loadAddressBookEntries();
			$gaNetMember = false;		
			switch($addressBookEntriesStatus){
				case AddressBook::GA_NET_MEMBERS:
					$gaNetMember = true;
					break;
				case AddressBook::GA_NET_NON_MEMBERS:
					$gaNetMember = false;
					break;
				default:
					return $this->addressBookEntries;	
			}
			$entries = array();
			foreach($this->addressBookEntries as $addrBookEntry){
				if($addrBookEntry->isGaNetMember() == $gaNetMember){
					$entries[] = $addrBookEntry; 
				}
			}
			return $entries;
		}
		public function loadAddressBookEntries(){
			//initialize database connection			
			
			//get the addressbook entries of this address book
			$qAddressBookEntries = $this->db->execQuery("select addressBookEntryID from Travel_Logs.tblAddressBookEntry where addressBookID = $this->addressBookId order by travelerId");
			$rsAddressBookEntries = new iRecordset($qAddressBookEntries);
			while(!$rsAddressBookEntries->EOF()){
				$newAddressBookEntry = new AddressBookEntry();
				$newAddressBookEntry->retrieve($rsAddressBookEntries->getaddressBookEntryID());
				$this->addressBookEntries[] = $newAddressBookEntry;
				$rsAddressBookEntries->moveNext();
			}
			$this->addressBookEntriesAreLoaded = true;
		}
		/***
		 * Update the addressbook entries in the address book.
		 * Base on thier email address, check if they are already member of Goabroad.net
		 */
		private function updateAddressBookEntries(){
			//get all the email addresses and addressBookEntryId of the addressbook entries of this addressbook that are not members of GAnet
			$qNonMembers = $this->db->execQuery("select addressBookEntryID, email from Travel_Logs.tblAddressBookEntry where addressBookID = $this->addressBookId and travelerId = 0");
			$rsNonMembers = new iRecordset($qNonMembers);			
			$emails = "'".implode("','",$rsNonMembers->retrieveColumn('email'))."'";
			if(strlen($emails) && $emails != ''){
				//check each email addresses if it is already in the tblTraveler and get the travelerID
				$qry = "SELECT travelerID, email from tblTraveler where email in ($emails) order by travelerID";				
				$qNewTravelers =  $this->db->execQuery($qry);
				$rsNewTravelers = new iRecordset($qNewTravelers);
				//update the travelerId of all the addressbook entries that are new members of the GAnet.
				while(!$rsNewTravelers->EOF()){
					$currentTravelerId = $rsNewTravelers->gettravelerID();
					$currentEmail = mysql_escape_string($rsNewTravelers->getemail());
					$this->db->execQuery("UPDATE tblAddressBookEntry set travelerId = $currentTravelerId where addressBookId = $this->addressBookId and email = '$currentEmail'");
					$rsNewTravelers->moveNext();	
				}
			}
		}
		public function addAddressBookEntry($mAddressBookEntry){
			$mAddressBookEntry->setAddressBookId($this->addressBookId);
			return $mAddressBookEntry->save();
		}
		private function createAddressBook($mTravelerId){
			$qAddressBookEntries = $this->db->execQuery("INSERT INTO tblAddressBook(`ownerTravelerID`) values($mTravelerId)");
		}
		private function getAddressBook($mTravelerId){
			return $this->db->execQuery("select addressBookID from Travel_Logs.tblAddressBook where ownerTravelerId = $mTravelerId");
		}
		private function getAddressBookEntry($mAddrBookEntryId){
			foreach($this->addressBookEntries as $addrBookEntry){
				if($addrsBookEntry->getAddressBookEntryId() == $mAddrBookEntryId){
					return $addrBookEntry;
				}
			}
			return false;
		}
		public function isTravelerListed($aTravelerID = null){
			if($aTravelerID == null){
				throw new exception('Traveler ID is required in function [isTravelerListed].');
				return false;
			}
			if(!$this->addressBookEntriesAreLoaded) $this->loadAddressBookEntries();
			foreach($this->addressBookEntries as $addrBookEntry){
				if($addrBookEntry->getTravelerId() == $aTravelerID){
					return true;
				}
			}
			return false;
		}
	}
?>
