<?php
	class SubNavigationLink{
		private $mLinkText 	= '';
		private $mUrl 	= '';
		private $mIsHighlighted = false;
		private $mIsHidden = false;
		private $mContext  = '';
		private $mNew = false;
		private $mIsOwn = false;
		
		public function setLinkText($linkText=''){
			$this->mLinkText = $linkText;
		}
		
		public function setUrl($url=''){
			$this->mUrl = $url;
		}
		
		public function setIsHighlighted($isHighlighted=false){
			$this->mIsHighlighted = $isHighlighted;
		}
		
		public function setIsHidden($isHidden=false){
			$this->mIsHidden = $isHidden;
		}
		
		public function setContext($context){
			$this->mContext = $context;
		}
		
		public function getLinkText(){
			return $this->mLinkText;
		}
		
		public function getUrl(){
			return $this->mUrl;
		}
		
		public function getIsHighlighted(){
			return $this->mIsHighlighted;
		}
		
		public function getIsHidden(){
			return $this->mIsHidden;
		}
		
		public function getContext(){
			return $this->mContext;
		}
		
		/**function s for new bubble**/
		public function getNewLink($tab){
			$text = "";
			
			switch($tab){
				case 'Profile':
					$text = "Customize your profile name!";
					break;
				case 'Videos':
					$text = "View my videos!";
					if($this->mIsOwn)
						$text = "Add videos to your journals!";
						
					break;
				case 'Journals':
					$text = "Send your journals as e-postcards!";
					if('GROUP' == $this->mContext)
						$text = "Send your journals as e-postcards!";
					break;
				case 'Apps':
					$text = "Bring your traveler profile to Facebook!";
					break;
				case 'Photos':
					if('GROUP' == $this->mContext){
						$text = "View all our photos here!";
						if($this->mIsOwn)
							$text = "View all of your group&lsquo;s photos!";
							
					}else{
						$text = "View all my photos here!";
						if($this->mIsOwn)
							$text = "View all your photos!";
					}	
					break;
				case 'Groups':
					$text = "Join your group&lsquo;s discussions!";
					if('GROUP' == $this->mContext)
						$text = "Create group discussions!";
					break;
				case 'Knowledge Base':
				case 'Discussions':
					$text = "Join our discussions!";
					if($this->mIsOwn)
						$text = "Create group discussions!";
					break;
				case 'Feeds':
					$text = "Get updated on your members\' activities!";
					break;
			}
			return $text;	
		}
		
		public function setHasNewFeatures($new=false){
			$this->mNew = $new;
		}
		
		public function getHasNewFeatures(){
			return $this->mNew;
		}
		
		public function setPublicView($isown=false){
			$this->mIsOwn = $isown;
		}
		
		public function getPublicView(){
			return $this->mIsOwn;
		}
	}
?>
