<?php	
	//require_once('travellog/model/notification/Class.Notification.php');
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	
	class ConfirmationData
	{
		const ENCRYPTION_KEY = '10203zxcv';	
		public static function getEncryptedTravelerID($travelerID=null){					
			$db = new dbHandler();					
			$qry = 	"SELECT HEX(AES_ENCRYPT(travelerID,'".self::ENCRYPTION_KEY."')) as ecrTravelerID " .
					"FROM  tblTraveler " .
					"WHERE travelerID = ".ToolMan::makeSqlSafeString($travelerID);
			$rs = new iRecordset($db->execQuery($qry));					
			return ($rs->retrieveRecordCount() > 0)?$rs->getEcrTravelerID(0):null;
		}
		
		public static function getEncryptedAddressBookEntryID($entryID=null){						
			$db = new dbHandler();
			$qry = 	"SELECT HEX(AES_ENCRYPT(addressBookEntryID,'".self::ENCRYPTION_KEY."')) as ecrEntryID " .
					"FROM  tblAddressBookEntry " .
					"WHERE addressBookEntryID = ".ToolMan::makeSqlSafeString($entryID);			
			$rs = new iRecordset($db->execQuery($qry));					
			return ($rs->retrieveRecordCount() > 0)?$rs->getEcrEntryID(0):null;			
		}
		
		public static function getDecryptedTravelerID($cryptedStr=null){					
			//check if the oID is a valid TravelerID				
			$db = new dbHandler();
			$qry = 	"SELECT travelerID " .
					"FROM tblTraveler " .
					"WHERE HEX(AES_ENCRYPT(travelerID,'".self::ENCRYPTION_KEY."')) = '$cryptedStr'";	
			$rs = new iRecordset($db->execQuery($qry));			
			return($rs->retrieveRecordCount() > 0)?$rs->getTravelerID(0):null;		
		}
		
		public static function getDecryptedAddressBookEntryID($cryptedStr=null){
			$db = new dbHandler();
			//check if the eID is a valid entryID			
			$qry = 	"SELECT addressBookEntryID " .
					"FROM tblAddressBookEntry " .
					"WHERE HEX(AES_ENCRYPT(addressBookEntryID,'".self::ENCRYPTION_KEY."')) = '$cryptedStr'";	
			$rs = new iRecordset($db->execQuery($qry));
			return($rs->retrieveRecordCount() > 0)?$rs->getAddressBookEntryID(0):null;
		}
		
		public static function getTravelerInfoVariables(){
			$array = array();
			$ar['[$_travelerID$]'] 		= 'The ID of the subject traveler';
			$ar['[$_encTravelerID$]'] 	= 'The encrypted subject traveler ID';
			$ar['[$_birthDate$]'] 		= 'Birthdate of the subject traveler';
			$ar['[$_userName$]'] 		= 'Username of the subject traveler';
			$ar['[$_firstName$]'] 		= 'First Name of the subject traveler';
			$ar['[$_lastName$]'] 		= 'Last Name of the subject traveler';
			$ar['[$_age$]'] 			= 'Age of the subject traveler';
			$ar['[$_gender$]'] 			= 'Gender of the subject traveler';
			return $ar;				
		}
		
		public static function getEntryInfoVariables(){
			$ar = array();
			$ar['[$_addressBookEntryID$]']			= 'The ID of the Addressbook Entry';
			$ar['[$_encAddressBookEntryID$]'] 		= 'The encrypted addressBook ID';
			$ar['[$_addressBookEntryFirstName$]']	= 'First name of the Addressbook Entry';
			$ar['[$_addressBookEntryLastName$]'] 	= 'Last name of the Addressbook Entry';
			return $ar;
		}
	}
?>