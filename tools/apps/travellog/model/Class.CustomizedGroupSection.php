<?php
	/**
	*	Created Jul 28, 2008
	*  Daphne 
	*/
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	
	class CustomizedGroupSection {
			
		private $mConn      = NULL;
    private $mRs        = NULL;

		private $customizedGroupSectionID = 0;
		private $mSectionLabel  = '';
		private $mOwnerGroupID = 0;
		private $mDateCreated = null;
		private $mRankSettings = 0;
			
		function __construct($_cGroupSectionID = 0){
				
			$this->customizedGroupSectionID = $_cGroupSectionID;
             
      try {
        $this->mConn = new Connection();
        $this->mRs   = new Recordset($this->mConn);
      }
      catch (Exception $e) {                 
        throw $e;
      }

		  if (0 < $this->customizedGroupSectionID){
				$sql = "SELECT * FROM tblCustomizedGroupSection WHERE customizedGroupSectionID = " . $this->customizedGroupSectionID ;
					
				$this->mRs->Execute($sql);
            
        if (0 == $this->mRs->Recordcount()){
          throw new Exception("Invalid customizedGroupSectionID");   
     			// ID not valid so throw exception
        }

				$this->mSectionLabel = $this->mRs->Result(0, "sectionlabel");	
				$this->mOwnerGroupID = $this->mRs->Result(0, "advisorGroupID");
				$this->mRankSettings = $this->mRs->Result(0, "ranksettings");
						
			}
				
		}
			
		function setOwnerGroupID($_ownerGroupID){		
			$this->mOwnerGroupID = $_ownerGroupID;
		}
			
		function setSectionLabel($_sectionLabel){		
			$this->mSectionLabel = $_sectionLabel;	
		}
			
		function setRankSettings($_rankSettings){		
			$this->mRankSettings = $_rankSettings;	
	  }
			
			
		function getCustomGroupSectionID(){		
			return $this->customizedGroupSectionID;	
		}
			
		function getSectionLabel(){		
			return $this->mSectionLabel;	
		}
			
		function getRankSettings(){		
			return $this->mRankSettings;	
		}
			
		// save settings of customized group section's display rank for subgroups
		function saveRankSettings(){
			$sql = "UPDATE tblCustomizedGroupSection SET ranksettings = " . $this->mRankSettings . " WHERE advisorGroupID = " . $this->mOwnerGroupID;
			$this->mRs->Execute($sql);
		}
				
		function getFeaturedSubGroups(){
			$sql = "SELECT a.* FROM tblGroup as a, " .
				" tblCustomizedGroupSection as b ,  " .
				" tblCustomizedGroupSectiontoSubGroups as c " .
				" WHERE a.groupID = c.subgroupID " .
				" AND b.customizedGroupSectionID = c.customizedGroupSectionID " .
				" AND b.advisorGroupID = " . $this->mOwnerGroupID . 
				" AND c.featuredsubgrouprank <= 5 " .
				" AND c.featuredsubgrouprank > 0 " .
				" ORDER BY c.featuredsubgrouprank ";
				
			$this->mRs->Execute($sql);

			$arrFeaturedSubGroups = array();
			
			while($recordset = mysql_fetch_array($this->mRs->Resultset())){
				$subgroupData = array();
				$subgroupData["groupID"] 	= $recordset["groupID"];
				$subgroupData["name"] 		= $recordset["name"];
				$subgroupData["featuredsubgrouprank"] = $recordset["featuredsubgrouprank"];
				$subgroupData["subgrouprank"] = $recordset["subgrouprank"];
				$subgroupData["administrator"] = $recordset["administrator"];
				$subgroupData["groupaccess"] = $recordset["groupaccess"];
				$subgroupData["discriminator"] = $recordset["discriminator"];
				$subgroupData["photoID"] = $recordset["photoID"];

				$group = new AdminGroup();
				$group->setAdminGroupData($subgroupData);

				$arrFeaturedSubGroups[] = $group;
			}

			return $arrFeaturedSubGroups;
		}
		
		/**
		 * Gets the subgroups that are under this custom group. Subgroups can
		 * be filtered whether they are active, inactive or (active & inactive) groups.
		 * 
		 * define: groupAccess
		 *   1 => open               - active
		 *   2 => open with approval - active
		 *   3 => invite only        - active
		 *   4 => closed             - inactive
		 * 
		 * @param string $_action values are 'showActive' or 'showInactive' or null
		 * @param string $searchKey the sub group name to search
		 * @param RowsLimit $limit the LIMIT clause in sql
		 * @return array of AdminGroup
		 */
		function getSubGroupsBySection($action = 'showActive' , $searchKey = '', $limit = null){
      $handler = new dbHandler();
      
      // fields
      $fields = new SqlFields();
      $fields->addField("groupID","a");
      $fields->addField("name","a");
      $fields->addField("featuredsubgrouprank","c");
      $fields->addField("subgrouprank","c");
      $fields->addField("administrator","a");
      $fields->addField("groupaccess","a");
      $fields->addField("discriminator","a");
      $fields->addField("photoID","a");
      // end of fields
      
      // tables
      $tables = new SqlFields();
      $tables->addField("","tblGroup","","a");
      $tables->addField("","tblCustomizedGroupSection","","b");
      $tables->addField("","tblCustomizedGroupSectiontoSubGroups","","c");
      // end of tables
      
      // filters
      $cnd = new Condition();
      $cnd->setAttributeName("a.`name`");
      $cnd->setOperation(FilterOp::$LIKE);
      $cnd->setValue("%$searchKey%");
      $cnd1 = new Condition();
      $cnd1->setAttributeName("a.`groupID`");
      $cnd1->setOperation(FilterOp::$EQUAL);
      $cnd1->setValue("c.`subgroupID`");
      $cnd2 = new Condition();
      $cnd2->setAttributeName("b.`customizedGroupSectionID`");
      $cnd2->setOperation(FilterOp::$EQUAL);
      $cnd2->setValue("c.`customizedGroupSectionID`");      
      
      $filter = new FilterCriteria2();
      $filter->addCondition($cnd);
      $filter->addBooleanOp("AND");
      $filter->addCondition($cnd1);
      $filter->addBooleanOp("AND");
      $filter->addCondition($cnd2);
      $filter->addBooleanOp("AND");
      // end of filters
			
			// filter the group access
			$grpAccessList = ('showActive' == $action) ? "1,2,3" : "";
			$grpAccessList = ('showInActive' == $action) ? "4" : $grpAccessList;
			
			if ("" != trim($grpAccessList)){
				$cnd2 = new Condition();
        $cnd2->setAttributeName("a.`groupaccess`");
        $cnd2->setOperation(FilterOp::$IN);
        $cnd2->setValue($grpAccessList);
        
        $filter->addCondition($cnd2);
        $filter->addBooleanOp("AND");			
			}
			// end of filtering group access
			
			// order
			$cnd1 = new Condition();
      $cnd1->setAttributeName("c.`subgrouprank`");
      $cnd1->setOperation(FilterOp::$ORDER_BY);
			$cnd2 = new Condition();
      $cnd2->setAttributeName("b.`datecreated`");
      $cnd2->setOperation(FilterOp::$ORDER_BY_DESC);
      
      $order = new FilterCriteria2();
      $order->addCondition($cnd1);
      $order->addCondition($cnd2);
      // end of order
			
			$arrSubGroupsBySection = SubGroupFactory::instance($this->mOwnerGroupID)->getSubGroups($fields, $limit, $order, $filter, $tables);
				
			return $arrSubGroupsBySection;
		}
			
		/**
		 * Retrieves the total number of subgroups of this custom group
		 * 
		 * @param string $_action one of the following(showActive or showInactive or null), null means all subgroups in custom group section
		 * @param string $searchKey the group name to search
		 * @return integer the number of subgroups
		 * 
		 */
		function getSubGroupsBySectionCount($action = 'showActive' , $searchKey = ''){			
      $handler = new dbHandler();
      
      // tables
      $tables = new SqlFields();
      $tables->addField("","tblGroup","","a");
      $tables->addField("","tblCustomizedGroupSection","","b");
      $tables->addField("","tblCustomizedGroupSectiontoSubGroups","","c");
      // end of tables
      
      // filters
      $cnd = new Condition();
      $cnd->setAttributeName("a.`name`");
      $cnd->setOperation(FilterOp::$LIKE);
      $cnd->setValue("%$searchKey%");
      $cnd1 = new Condition();
      $cnd1->setAttributeName("a.`groupID`");
      $cnd1->setOperation(FilterOp::$EQUAL);
      $cnd1->setValue("c.`subgroupID`");
      $cnd2 = new Condition();
      $cnd2->setAttributeName("b.`customizedGroupSectionID`");
      $cnd2->setOperation(FilterOp::$EQUAL);
      $cnd2->setValue("c.`customizedGroupSectionID`");      
      
      $filter = new FilterCriteria2();
      $filter->addCondition($cnd);
      $filter->addBooleanOp("AND");
      $filter->addCondition($cnd1);
      $filter->addBooleanOp("AND");
      $filter->addCondition($cnd2);
      $filter->addBooleanOp("AND");
      // end of filters
			
			// filter the group access
			$grpAccessList = ('showActive' == $action) ? "1,2,3" : "";
			$grpAccessList = ('showInActive' == $action) ? "4" : $grpAccessList;
			
			if ("" != trim($grpAccessList)){
				$cnd2 = new Condition();
        $cnd2->setAttributeName("a.`groupaccess`");
        $cnd2->setOperation(FilterOp::$IN);
        $cnd2->setValue($grpAccessList);
        
        $filter->addCondition($cnd2);
        $filter->addBooleanOp("AND");			
			}
			// end of filtering group access
			
			$cnt = SubGroupFactory::instance($this->mOwnerGroupID)->getSubGroupsCount($filter, $tables);
				
			return $cnt;
		}
		
		/**
		 * Method to update the customized subgroups rank of the given subgroups IDs 
		 * starting from rank count specified
		 * @param array $_arrSubGroupID the array of sub group IDs to be updated
		 * @param integer $count the starting rank count
		 */		
			public static function arrangeRankOfSubGroups($_arrSubGroupID = array(), $cnt = 0) {
				$rs = new Recordset(new Connection());

				foreach ($_arrSubGroupID as $each) :
					$cnt ++ ;
					$sql = "UPDATE tblCustomizedGroupSectiontoSubGroups SET subgrouprank = " . $cnt . " WHERE subgroupID = " . $each ;
					$rs->Execute($sql);
				endforeach;
			}
			
		/**
		 * Method to update the rank of given subgroup
		 * update the subgrouprank attribute
		 * @param AdminGroup $subgroup the subgroup to be updated
		 * @param integer $rank the new rank of the subgroup
		 * @param string $action one of the following (showActive or showInActive)
		 * @return void
		 */
		public function updateNewRankOfSubGroup($subgroup, $rank, $action) {
			$grpAccessList = ('showActive' == $action) ? "1,2,3" : "";
			$grpAccessList = ('showInActive' == $action) ? "4" : $grpAccessList;
        
			$sql = "SELECT a.groupID " .
				" FROM  tblGroup as a , tblCustomizedGroupSection as b , tblCustomizedGroupSectiontoSubGroups as c " .
				" WHERE a.groupID = c.subgroupID " .
				" AND b.customizedGroupSectionID = c.customizedGroupSectionID " .
				" AND b.advisorGroupID = " . $this->mOwnerGroupID . 
				" AND a.groupaccess IN (" .  $grpAccessList . ") " .
				" ORDER BY c.subgrouprank , b	.datecreated DESC " ;
			
			$this->mRs->Execute($sql);
			$foundRank = false;
			
			$rs = new Recordset(new Connection());
			$sID = $subgroup->getGroupID();
			$cnt = 1;		
			$ids = array();	
			
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
				$ids[$cnt] = $recordset['groupID'];
				
				if ($sID == $recordset['groupID'])
				  $sRank = $cnt;
				$cnt++;
			}
			
			foreach ($ids as $key=>$id) {
				if ($rank < $sRank) {
					if ($key >= $rank) {
            $sql = "UPDATE tblCustomizedGroupSectiontoSubGroups " .
              " SET subgrouprank = " .($key + 1). " " .
              " WHERE subgroupID = " . $id ;
            $rs->Execute($sql);						
					}
					
					if ($sID == $id)
					  break;
				}
				else {
					if ($key >= $sRank) {
            $sql = "UPDATE tblCustomizedGroupSectiontoSubGroups " .
              " SET subgrouprank = " .($key - 1). " " .
              " WHERE subgroupID = " . $id ;
            $rs->Execute($sql);						
					}
					
					if ($rank == $key)
					  break;
				}			
			}
			
      $sql = "UPDATE tblCustomizedGroupSectiontoSubGroups " .
        " SET subgrouprank = " . $rank . " " .
        " WHERE subgroupID = ". $sID ;
      $rs->Execute($sql);
		}
		
		/**
		 * retrieves the number of subgroups of this custom group
		 */
		function getAllSubGroupsCount(){		
				$sql = "SELECT count(a.groupID)
						  FROM  tblGroup as a , tblCustomizedGroupSection as b ,  tblCustomizedGroupSectiontoSubGroups as c
				 		  WHERE a.groupID = c.subgroupID
						  AND b.customizedGroupSectionID = c.customizedGroupSectionID
						  AND b.advisorGroupID = " . $this->mOwnerGroupID . 
						" ORDER BY c.subgrouprank , b	.datecreated DESC " ;
				$this->mRs->Execute($sql);
			
				$record = mysql_fetch_array($this->mRs->Resultset());
				
				return $record[0];
			}
		
		/**
		 * retrieves all subgroups of this custom group
		 */
		function getAllSubGroups(){		
				$sql = "SELECT a.groupID , a.name, c.featuredsubgrouprank , c.subgrouprank FROM  tblGroup as a , tblCustomizedGroupSection as b ,  tblCustomizedGroupSectiontoSubGroups as c
				 		  WHERE a.groupID = c.subgroupID
						  AND b.customizedGroupSectionID = c.customizedGroupSectionID
						  AND b.advisorGroupID = " . $this->mOwnerGroupID . 
						" ORDER BY c.subgrouprank , b	.datecreated DESC " ;
				$this->mRs->Execute($sql);
			
				$arrAllSubGroups = array();

				while($recordset = mysql_fetch_array($this->mRs->Resultset())){

					$subgroupData = array();
					$subgroupData["groupID"] 	= $recordset["groupID"];
					$subgroupData["name"] 		= $recordset["name"];
					$subgroupData["featuredsubgrouprank"] = $recordset["featuredsubgrouprank"];
					$subgroupData["subgrouprank"] = $recordset["subgrouprank"];

					$group = new AdminGroup();
					$group->setAdminGroupData($subgroupData);

					$arrAllSubGroups[] = $group;
				}

				return $arrAllSubGroups;
			}
			
			function save(){
				
				if (0 == $this->customizedGroupSectionID){
					$now  = date("Y-m-d H:i:s");
					$sql = "INSERT INTO tblCustomizedGroupSection SET sectionlabel = '" . $this->mSectionLabel . "' , advisorGroupID = " . $this->mOwnerGroupID . ", datecreated = '" . $now . "'" ;
				} else {
					$sql = "UPDATE tblCustomizedGroupSection SET sectionlabel = '" . $this->mSectionLabel .  "' WHERE customizedGroupSectionID = " . $this->customizedGroupSectionID;
				}
				
				$this->mRs->Execute($sql);
				
			}
			
			
			function remove(){
				
				//delete child records
				$sql = "DELETE FROM tblCustomizedGroupSectiontoSubGroups WHERE customizedGroupSectionID = " . $this->customizedGroupSectionID;
				$this->mRs->Execute($sql);
				
				//delete custom group section record
				$sql = "DELETE FROM tblCustomizedGroupSection WHERE advisorGroupID = " . $this->mOwnerGroupID;
				$this->mRs->Execute($sql);
				
				
			}
			
			
			function addSubGroupToSectionWithRank($_subGroupID = 0, $_grpAccess = 1){
				
				if (4 == $_grpAccess)
					$grpAccessList = "4";
				else
					$grpAccessList = "1,2,3";
					
				$newRank = $this->getRankOfNewCustomSubGroup($grpAccessList);
				if (0 < $_subGroupID && !$this->isSubGroupInSection($_subGroupID)){
					$now  = date("Y-m-d H:i:s");
					
					$sql = "INSERT INTO tblCustomizedGroupSectiontoSubGroups 
					SET customizedGroupSectionID = " . $this->customizedGroupSectionID . " ,
					 subgroupID = " . $_subGroupID . " , 
					 dateadded = '" . $now . "' , 
					 subgrouprank = " . $newRank ;
		 
					$this->mRs->Execute($sql);
					
					
				}
			}
			
			
			function addSubGroupToSectionWithNoRank($_subGroupID = 0){
				
				if (0 < $_subGroupID && !$this->isSubGroupInSection($_subGroupID)){
					$now  = date("Y-m-d H:i:s");
					
					$sql = "INSERT INTO tblCustomizedGroupSectiontoSubGroups 
					SET customizedGroupSectionID = " . $this->customizedGroupSectionID . " ,
					 subgroupID = " . $_subGroupID . " , 
					 dateadded = '" . $now . "'" ;
		 
					$this->mRs->Execute($sql);
					
					
				}
			}
			
			function removeSubGroupFromSection($_subGroupID = 0){
				
				$sql = "DELETE FROM tblCustomizedGroupSectiontoSubGroups WHERE subgroupID = " . $_subGroupID . 
						" AND customizedGroupSectionID = " . $this->customizedGroupSectionID ;
					
				$this->mRs->Execute($sql);	
				
			}
			
			function isSubGroupInSection($_subGroupID = 0){
				$sql = "SELECT *  FROM tblCustomizedGroupSectiontoSubGroups WHERE subgroupID = " . $_subGroupID . 
						" AND customizedGroupSectionID = " . $this->customizedGroupSectionID ;					
				$this->mRs->Execute($sql);
				
				if ($this->mRs->RecordCount())
					return true;
				return false;
			}
			
			//function to get the latest available rank
			function getRankOfNewCustomSubGroup($_grpAccessList = 1) {

				$sql = "SELECT c.subgrouprank FROM tblGroup as a,  tblCustomizedGroupSection as b, tblCustomizedGroupSectiontoSubGroups as c 
				WHERE a.groupID = b.advisorGroupID 
				AND b.customizedGroupSectionID = c.customizedGroupSectionID
				AND  a.groupID = " . $this->mOwnerGroupID . 
				" AND groupaccess IN (" .  $_grpAccessList . ") " .
				" ORDER BY c.subgrouprank DESC LIMIT 0 , 1" ;
			
				$this->mRs->Execute($sql);

				$newrank = 1;
				if ($this->mRs->RecordCount())
					$newrank = $this->mRs->Result(0,'subgrouprank') + 1;

				return $newrank;			

			}
			
			public static function autoSortGroups($_groupID = 0 , $_cmbSortType = 0) {
			
				$arrSubGroupIDs = array();
				
				if (2 == $_cmbSortType){
					$sortKey = 'Name';
				}
				else if (3 == $_cmbSortType){
					$sortKey = 'DateCreated';
				}
			
				$rs = new Recordset(new Connection());
				
				//  --------- sort Active Groups first ---------
				$arrSubGroupIDs = CustomizedGroupSection::getSubGroupIDsBySectionAndSortOrder($_groupID, 'showActive', $sortKey );
				
				$cnt = 0;
				foreach ($arrSubGroupIDs as $each) :
					$cnt ++ ;
					$sql = "UPDATE tblCustomizedGroupSectiontoSubGroups SET subgrouprank = " . $cnt . " WHERE subgroupID = " . $each ;
					$rs->Execute($sql);
				endforeach;
				
				
				//  --------- sort InActive Groups  ---------
				$arrSubGroupIDs = CustomizedGroupSection::getSubGroupIDsBySectionAndSortOrder($_groupID, 'showInActive', $sortKey );
				$cnt = 0;
				foreach ($arrSubGroupIDs as $each) :
					$cnt ++ ;
					$sql = "UPDATE tblCustomizedGroupSectiontoSubGroups SET subgrouprank = " . $cnt . " WHERE subgroupID = " . $each ;
					$rs->Execute($sql);
				endforeach;
				
				
			}
			
			
			public static function getSubGroupIDsBySectionAndSortOrder($_groupID = 0 , $_action = 'showActive' , $_sortKey = 'Name'){

				//define: groupAccess:
				// 1 = open; 
				// 2 = open with approval; 
				// 3 = invite only ;
				// 4 = closed ;

				if ('showActive' == $_action)
					$grpAccessList = "1,2,3";
				elseif ('showInActive' == $_action)
					$grpAccessList = "4";
				
				if ('Name' == $_sortKey ){
					$sortKey = 'a.name';
					$sortOrder = 'ASC';
				}
				else if ('DateCreated' == $_sortKey ) {
					$sortKey = 'a.datecreated';
					$sortOrder = 'DESC';
				}
				else{
					$sortKey = 'a.name';
					$sortOrder = 'ASC';
				}
				
				$rs = new Recordset(new Connection());
					
				$sql = "SELECT a.groupID , a.name, c.featuredsubgrouprank , c.subgrouprank FROM  tblGroup as a , tblCustomizedGroupSection as b ,  tblCustomizedGroupSectiontoSubGroups as c
				 		  WHERE a.groupID = c.subgroupID
						  AND b.customizedGroupSectionID = c.customizedGroupSectionID
						  AND b.advisorGroupID = " . $_groupID . 
						" AND a.groupaccess IN (" .  $grpAccessList . ") " .
						" ORDER BY " . $sortKey . " " . $sortOrder;
				$rs->Execute($sql);

				$arrSubGroupIDsBySectionAndSortOrder = array();

				while($recordset = mysql_fetch_array($rs->Resultset())){

					$arrSubGroupIDsBySectionAndSortOrder[] = $recordset["groupID"];
					
				}
				return $arrSubGroupIDsBySectionAndSortOrder;

			}
	}

?>