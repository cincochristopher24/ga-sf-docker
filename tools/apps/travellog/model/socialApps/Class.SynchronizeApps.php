<?php

// Author: adelbert
// Date: June 03, 2009
// Purpose: used to sync apps

require_once 'utility/Class.dbHandler.php';
require_once 'utility/Class.iRecordset.php';      
require_once 'travellog/helper/ganet_web_service/Class.wsDbHelper.php';    
//require_once 'travellog/helper/ganet_web_service/Class.wsHelper.php';      
//require_once 'Class.WebServiceIni.php';

class SynchronizeApps {
    
	private	$userID,
			$oldUserID,
			$travelerID,
			$containerID,
			$container,

			$email,
			$password,

			$isActive,
			$isAdvisor,
			$isSuspended,
			$isResynchronize,
			$isUsingOwnAccount,

			$conn;


	function __construct($args = array()){

		$this->conn = new dbHandler();

		$this->setContainerID($args['containerID']);
		$this->setContainer($args['container']);
		
        // should be followed before setContainerID() and setContainer() methods
		$this->setUserIdByContainerID();
						
		$this->setEmail($args['email']);
		$this->setPassword($args['password']);

	}
	
	function __toString(){
		return 'Your Accounts have been successfully synchronized.';
	}

	// setters
	function setUserID($userID){
		$this->userID = $userID;
	}

	function setOldUserID($oldUserID){
		$this->oldUserID = $oldUserID;
	}	

	function setTravelerID($travelerID){
		$this->travelerID = $travelerID;
	}

	function setContainerID($containerID){
		$this->containerID = $containerID;
	}

	function setContainer($container){
		$this->container = $container;
	}                                 
	
	function setEmail($email){
		$this->email = $email;
	}

	function setPassword($password){
		$this->password = $password;
	}


	// getters
	function getUserID(){
		return $this->userID;
	}

	function getOldUserID(){
		return $this->oldUserID;
	}
	
	function getContainerID(){
		return $this->containerID;
	}

	function getTravelerID(){
		return $this->travelerID;
	}

	function getContainer(){
		return $this->container;
	}

	function getEmail(){
		return $this->email;
	}

	function getPassword(){
		return $this->password;
	}

	function getContainerField(){
		return wsDbHelper::getUserField($this->getContainer());
	}

	function isActive(){
		return $this->isActive;
	}   
	
	function isAdvisor(){
		return $this->isAdvisor;		
	}	


	function isSuspended(){
		return $this->isSuspended;		
	}

	function isResynchronize(){
		return $this->isResynchronize;
	}

	function isUsingOwnAccount(){
		return $this->isUsingOwnAccount;
	}


	function synchronize(){				
		$this->conn->setDB('SocialApplication');
		$sql = "UPDATE tbUser SET ".$this->getContainerField()."=".$this->getContainerID()." WHERE travelerID=".$this->getTravelerID();
		return $this->conn->execQuery($sql);
	}   
			

	function isAlreadySynced(){		
		$this->conn->setDB('SocialApplication');
		$sql = "SELECT userID FROM tbUser WHERE ".$this->getContainerField()."!=0 AND travelerID=".$this->getTravelerID();
		$rs = new iRecordSet($this->conn->execQuery($sql));
		if( $rs->retrieveRecordCount() ){		
			$this->isUsingOwnAccount = $rs->getUserID() == $this->getUserID();
			return true;
		} 
		else return false;
	}

		
	function setUserIdByContainerID(){
		$this->conn->setDB('SocialApplication');
		$sql = "SELECT userID, travelerID FROM tbUser WHERE ".$this->getContainerField()."=".$this->getContainerID();
		$rs = new iRecordSet($this->conn->execQuery($sql));
		if( $rs->retrieveRecordCount() ){
			$this->isResynchronize = $rs->getTravelerID() != 0;
			$this->setUserID($rs->getUserID());
			$this->setOldUserID($rs->getUserID());
		}                                      
	}
	
	function deleteDummyUser(){
		$this->conn->setDB('SocialApplication');		
		$sql = "DELETE FROM tbUser WHERE userID=".$this->getUserID();
		return $this->conn->execQuery($sql);
	}
	
	function unsetContainerID(){
		$this->conn->setDB('SocialApplication');
		$sql = "UPDATE tbUser SET ".$this->getContainerField()."=0 WHERE userID=".$this->getOldUserID();
		return $this->conn->execQuery($sql);
	}

	function updateTravelWish(){
		$this->conn->setDB('SocialApplication');
	    $sql = 'UPDATE tbTravelWish SET userID='.$this->getUserID().' WHERE userID='.$this->getOldUserID();
		$this->conn->execQuery($sql);
	}


	function addUserInTblMyTravelJournals(){
		if(!$this->isUserExistsInTblTravelJournalsUsers()){
			$this->conn->setDB('SocialApplication');
			$sql = "INSERT INTO tblTravelJournalUsers SET dateAdded=now(), userID=".$this->getUserID();
			$this->conn->execQuery($sql);							
		}
	}

	private function isUserExistsInTblTravelJournalsUsers(){
		$this->conn->setDB('SocialApplication');
		$sql = 'SELECT count(*) AS isExists FROM tblTravelJournalUsers WHERE userID='.$this->getUserID();
		$rs = new iRecordset($this->conn->execQuery($sql));
		return $rs->getIsExists();		
	}

	
	function getUserIdByTravelerID(){
		$this->conn->setDB('SocialApplication');
		$sql = "SELECT userID FROM tbUser WHERE travelerID=".$this->getTravelerID();
		$rs = new iRecordset($this->conn->execQuery($sql));
		return $rs->retrieveRecordCount() ? $rs->getUserID() : false;
	}		
			

	function isGanetUser(){

		$this->conn->setDB('Travel_Logs');
		$sql = "SELECT `travelerID`, `active`, `isSuspended`, `isadvisor` FROM `tblTraveler` WHERE `email`='".mysql_real_escape_string($this->getEmail())."' AND `password` = '" . mysql_real_escape_string($this->getPassword()). "' LIMIT 1";
		$rs = new iRecordset($this->conn->execQuery($sql));

		if( $rs->retrieveRecordCount() ){

			$this->setTravelerID($rs->getTravelerID()); 
						
			// set user active value
			$this->isActive = (bool)$rs->getActive();

			// set user isSuspended value
			$this->isSuspended = (bool)$rs->getIsSuspended();

			// set user isAdvisor value
			// TEMPORARILY Commented (DO NOT DELETE)
			// $this->isAdvisor = (bool)$rs->getIsadvisor();

			// Temporary code to set if user us Advisor.
			$sql_ = "SELECT count(*) AS isadvisor FROM `tblGrouptoAdvisor` WHERE `travelerID` =".$rs->getTravelerID();
			$rs_ = new iRecordset($this->conn->execQuery($sql_));
			$this->isAdvisor = (bool)$rs_->getIsadvisor();

			// return travelerID if user exists
			return $this->getTravelerID();
		}

		return false;			                                                                         
	}
}
