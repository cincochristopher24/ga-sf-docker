<?php

require_once 'utility/Class.dbHandler.php';
require_once 'utility/Class.iRecordset.php';
require_once 'travellog/model/socialApps/helper/Class.AppsUserHelper.php';

class AppsUserPeer {

	private static function getConnection(){
		$conn = new dbHandler;
		$conn->setDB('SocialApplication');
		return $conn;
	}
	
	static function addUserByContainerAndContainerID($container=null, $containerID=null) {
		if(!$container OR !$containerID) return false;

		if($containerFieldName = AppsUserHelper::getContainerFieldNameByContainer($container)){
			$conn = self::getConnection();
			$sql = "INSERT INTO `tbUser` SET {$containerFieldName} = {$containerID}";
			$conn->execQuery($sql);
			if( $userID = $conn->getLastInsertedID() )
				return $userID;

		} else return false;		
	}

	static function delete($userID=null){
		if($userID){
			$conn = self::getConnection();
			$sql = "DELETE FROM tbUser WHERE userID = {$userID}";
			$conn->execQuery($sql);
		}
	}

	static function retrieveAppsUserByPk($userID=null){
		if(!$userID) return false;
		$conn = self::getConnection();
		$sql = "SELECT Travel_Logs.tblTraveler.email,`username`, `userID`,`tbUser`.`travelerID` 
				FROM tbUser LEFT JOIN Travel_Logs.tblTraveler 
				ON `tbUser`.`travelerID` = `tblTraveler`.`travelerID` 
				WHERE userID = {$userID}";
		$rs = new iRecordset($conn->execQuery($sql));
		if( $rs->retrieveRecordCount() ){
			$AppsUser = new AppsUser;
			$AppsUser->setUserID($rs->getUserID());
			$AppsUser->setTravelerID($rs->getTravelerID());
			$AppsUser->setDotNetEmail($rs->getEmail());
			$AppsUser->setDotNetUsername($rs->getUsername());
			return $AppsUser;
	   }
	}

	
	static function getAppsUserByContainerAndContainerID($container, $containerID){
		if($containerFieldName = AppsUserHelper::getContainerFieldNameByContainer($container)){
			$conn = self::getConnection();
			$sql = "SELECT Travel_Logs.tblTraveler.email,`username`, `userID`,`tbUser`.`travelerID` 
					FROM tbUser LEFT JOIN Travel_Logs.tblTraveler 
					ON `tbUser`.`travelerID` = `tblTraveler`.`travelerID` 
					WHERE {$containerFieldName} = {$containerID} LIMIT 1";
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() ){
				$AppsUser = new AppsUser;
				$AppsUser->setUserID($rs->getUserID());
				$AppsUser->setContainer($container);
				$AppsUser->setContainerID($containerID);
				$AppsUser->setTravelerID($rs->getTravelerID());
				$AppsUser->setDotNetEmail($rs->getEmail());
				$AppsUser->setDotNetUsername($rs->getUsername());
				return $AppsUser;
		   }
		}
	}
	
	static function isSynchronized($userID=null){
		if(!$userID) return false;
		$conn = self::getConnection();
		$sql = "SELECT count(userID) as isSync FROM tbUser WHERE travelerID != 0 AND userID = {$userID} LIMIT 1";
		$rs = new iRecordSet($conn->execQuery($sql));
		return (bool)$rs->getIsSync();
	}


	// this function use to add new traveler to table tbUser when new .net user has added
	static function addTravelerToAppsUser($travelerID) {
		$conn = self::getConnection();
		$sql = "INSERT INTO tbUser SET travelerID={$travelerID}";
		$conn->execQuery($sql);
	}

	function addApp($appName){

		switch($appName){
			case WebServiceIni::MY_TRAVEL_MAP:
				break;
			case WebServiceIni::MY_TRAVEL_PLANS:
				break;
			case WebServiceIni::MY_TRAVEL_BIO:
				MyTravelBioApp::addUser($this->id);
				break;
//			case WebServiceIni::MY_TRAVEL_JOURNALS:
//				AbstractMyTravelJournalApp::addUser($this->id);
//				break;
		}	
	}

	function hasApp($appName){

		$hasApp = false;

		switch($appName){
			case WebServiceIni::MY_TRAVEL_MAP:
				break;
			case WebServiceIni::MY_TRAVEL_PLANS:
				break;
			case WebServiceIni::MY_TRAVEL_BIO:
				break;
			case WebServiceIni::MY_TRAVEL_JOURNALS:
//				$hasApp = AbstractMyTravelJournalApp::hasMyTravelJournalApp($this->id);
				break;
		}
		return $hasApp;
	}     

	
}

?>