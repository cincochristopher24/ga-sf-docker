<?php
/*
	Filename:		Class.fbNeedToUpdateAccount.php
	Module:			Social Apps
	Author:			Adelbert Silla
	Date created:	Aug. 10, 2009
	Purpose:		Class definition for fbNeedToUpdateAccount

*/

class fbNeedToUpdateAccount {
	
	protected $id,
			  $travelerID = 0,
			  $application,
			  $status,
			  $dateUpdated;

	// setters
	function setID($id){
		$this->id = $id;
	}

	function setTravelerID($travelerID){
		$this->travelerID = $travelerID;
	}

	function setApplication($application){
		$this->application = $application;
	}

	function setStatus($status){
		$this->status = $status;
	}

	function setDateUpdated($date){
		$this->dateUpdated = $date;
	}

	
	// getters
	function getID(){
		return $this->id;
	}

	function getTravelerID(){
		return $this->travelerID;
	}

	function getApplication(){
		return $this->application;
	}

	function getStatus(){
		return $this->status;
	}

	function getDateUpdated(){
		return $this->dateUpdated;
	}

	// CRUD
	function create(){
		fbNeedToUpdateAccountPeer::create($this);
	}
	
	function update(){
		fbNeedToUpdateAccountPeer::update($this);
	}

	function delete(){
		fbNeedToUpdateAccountPeer::delete($this->getID());		
	}
	// END OF CRUD
}

?>