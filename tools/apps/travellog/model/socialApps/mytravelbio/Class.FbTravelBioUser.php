<?php
	require_once('Class.Decorator.php');
	
	//uid, first_name, last_name, hometown_location, sex, current_location
	class FbTravelBioUser extends Decorator
	{
		private $mUID				= null;
		private $mFirstName 		= null;
		private $mLastName 			= null;
		private $mHometown 			= null;
		private $mSex				= null;
		private $mCurrentLocation 	= null;
		
		function __construct($coreObj,$initialData=null){
			parent::__construct($coreObj);
			if(is_array($initialData)){
				$this->mUID 			= (array_key_exists('uid',$initialData)?$initialData['uid']:null);
				$this->mFirstName 		= (array_key_exists('first_name',$initialData)?$initialData['first_name']:null);
				$this->mLastName 		= (array_key_exists('last_name',$initialData)?$initialData['last_name']:null);
				$this->mHomeTown 		= (array_key_exists('hometown_location',$initialData)?$initialData['hometown_location']:null);
				$this->mSex 			= (array_key_exists('sex',$initialData)?$initialData['sex']:null);
				$this->mCurrentLocation = (array_key_exists('current_location',$initialData)?$initialData['current_location']:null);
			}
		}

		public function getFirstName(){
			return $this->mFirstName;
		}
		
		public function getLastName(){
			return $this->mLastName;
		}
		
		public function getName(){
			return $this->mFirstName.' '.$this->mLastName;
		}
		
		public function getHomeTown(){
			return $this->mHomeTown;
		}
		
		public function getSex(){
			return $this->mSex;
		}
		
		public function getCurrentLocation(){
			return $this->mCurrentLocation;
		}		
	}
?>