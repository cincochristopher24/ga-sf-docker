<?php	
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	require_once('travellog/model/travelbio/Class.TravelBioUser.php');
	require_once('travellog/model/travelbio/Class.ProfileBox.php');
	require_once('travellog/model/travelbio/Class.FBini.php');
	
	class TravelBioManager
	{
		const ERROR_ALREADY_SYNCED = 1;
		const ERROR_DUAL_ACCOUNT = 2;
		const ERROR_TRAVELER_ALREADY_SYNCED = 3;
		const SUCCESS = 4;
		
		const FB_ACCOUNT 	= 1;
		const GA_ACCOUNT 	= 2;
		const SYNC_ACCOUNT	= 3;
		const ALL_ACCOUNT 	= 4;
				
		const DEF_NUMREC = 25;
		
		const DEFAULT_PROFILE_FBML = 1;
		const CUSTOM_PROFILE_FBML = 2;
		
		public static $totalRec = 0;
				
		public static function refreshFbUserProfileBox($fbUser,$type=self::CUSTOM_PROFILE_FBML){
			if($fbUser->isFbUser()){
				require_once('travellog/model/FacebookPlatform/facebook.php');
				$facebook = new Facebook(FBini::getApiKey(), FBini::getApiSecret());
				$facebook->set_user(FBini::getAdminFbUserID(), FBini::getInfiniteSessionKey());
				$fbml = ($type==self::CUSTOM_PROFILE_FBML?ProfileBox::generateCustomProfileFBML($fbUser):ProfileBox::generateDefaultProfileFBML($fbUser));
				//$facebook->api_client->profile_setFBML($fbml,$fbUser->getFbUserID());
				try{
//					$facebook->api_client->profile_setFBML($fbml,$fbUser->getFbUserID(),$fbml);
					$facebook->api_client->profile_setFBML(NULL,$fbUser->getFbUserID(), $fbml, NULL, NULL, $fbml);
				}
				catch(exception $e){}
			}
		}
		
		public static function syncFbGaUsers($fbUser,$travelerID,$useAccount=null){
			//initialize GA user

			$gaUser = TravelBioUser::getTravelBioUserByTravelerID($travelerID);
			//if the gauser is not a travelbio user			
			if(!is_null($gaUser)){
				//the ga user has a travelbio but it is not synced to an fb account, its ok to proceed but we have to check if it has answers.
				//if it has answers, we must let the user choose which account is he/she going to keep. 
				$travelerBioAnswers = $gaUser->getAnswers();
				$fbUserBioAnswers = $fbUser->getAnswers();					
				if(count($travelerBioAnswers) && count($fbUserBioAnswers)){
					if(self::GA_ACCOUNT==$useAccount){
						$domAccount = $gaUser;
						$nonDomAccount = $fbUser;
					}
					elseif(self::FB_ACCOUNT==$useAccount){
						$domAccount = $fbUser;
						$nonDomAccount = $gaUser;
					}
					else{
						return self::ERROR_DUAL_ACCOUNT;
					}
				}
				else{
					if(count($travelerBioAnswers)){
						$domAccount = $gaUser;
						$nonDomAccount = $fbUser;
					}
					else{
						$domAccount = $fbUser;
						$nonDomAccount = $gaUser;
					}
				}
				//if the fbUser is currently synced to another ga user, we must unsynced it and create a new record for it.
				if($fbUser->isSynchronized()){
					$fbUser->unsynchronize();
				}
				$domAccount->setTravelerID($travelerID);
				$domAccount->setFbUserID($fbUser->getFbUserID());					
				$nonDomAccount->delete();
				$domAccount->save();
				self::refreshFbUserProfileBox($domAccount);
				return self::SUCCESS;
			}
			else{//if travelerID entered has no travelBio, we can insert directly
				//if the fbUser is currently synced with another gaUser, unsync it an resync the fbUser to the new gaUser 
				if($fbUser->isSynchronized()){
					$fbUser->unsynchronize();
				}
				$fbUser->setTravelerID($travelerID);
				$fbUser->save();
				self::refreshFbUserProfileBox($fbUser);
				return self::SUCCESS;
			}
		}
		
		public static function clearFbCookies(){			
			require_once('travellog/model/travelbio/Class.FBini.php');		
			require_once('travellog/model/FacebookPlatform/facebook.php');
			
			$apiKey 	= FBini::getApiKey();
			$apiSecret 	= FBini::getApiSecret();
			
			/***
			 * Note: Be carefull not to clear non fb cookies
			***/
			//generate the sig key. Sig key is compose of the userID, sessionKey and the apiSecret and encrypted using MD5
			//we need to get the whatever is stored in the cookies			
			$curUser 		= ( array_key_exists($apiKey.'_user',$_COOKIE)? $_COOKIE[$apiKey.'_user'] : null );
			$curSessionKey 	= ( array_key_exists($apiKey.'_session_key',$_COOKIE)? $_COOKIE[$apiKey.'_session_key']:null ) ;
			$curCookies 	= array('user'=>$curUser,'session_key'=>$curSessionKey);
			$curSig 		=  Facebook::generate_sig($curCookies,$apiSecret);
			
			$cookies = array();
		    $cookies[$apiKey.'_user'] 			= null;
		    $cookies[$apiKey.'_session_key'] 	= null;
		    $cookies[$curSig] 					= null;
		    
			foreach($cookies as $key=>$val){
				if( array_key_exists($key, $_COOKIE) ){
					setcookie($key, '', time()-1000);
			        setcookie($key, '', time()-1000, '/');
			        $_COOKIE[$key] = '';
				}
			}
		}
		
		private static function getFBRecords($arUIDs=array()){
			require_once('travellog/model/FacebookPlatform/facebook.php');

			$facebook = new Facebook(FBini::getApiKey(), FBini::getApiSecret());
			$facebook->set_user(FBini::getAdminFbUserID(), FBini::getInfiniteSessionKey());
			$fql = 	"SELECT uid, first_name, last_name, name, hometown_location, sex, current_location ".
	            	"FROM user ".
	            	"WHERE uid IN (".implode(',',$arUIDs).")";
			$fbUsers = $facebook->api_client->fql_query($fql);
			//create a map using the facebook uids as keys
			$fbUsersMap = array();
			if(is_array($fbUsers)){
				foreach($fbUsers as $iFbUser){
					$fbUsersMap[$iFbUser['uid']] = $iFbUser;
				}
			}
			return $fbUsersMap;
		}
				
		public static function getTravelBioUsers($type=self::ALL_ACCOUNT,$fr=0){
			require_once('travellog/model/travelbio/Class.FBini.php');
			require_once('travellog/model/travelbio/Class.TravelBioUser.php');
			require_once('travellog/model/travelbio/Class.GaTravelBioUser.php');
			require_once('travellog/model/travelbio/Class.FbTravelBioUser.php');
			require_once('travellog/model/Class.Traveler.php');
			
			$db = FBini::createDBHandler();
			
			$qry = 	'SELECT SQL_CALC_FOUND_ROWS travelBioUserID, travelerID, fbUserID ' .
					'FROM tblTravelBioUser ';			
			switch($type){
				case self::FB_ACCOUNT:
					$qry .= 'WHERE travelerID = 0 ';				
				break;
				case self::GA_ACCOUNT:
					$qry .= 'WHERE fbUserID = 0 ';
				break;
				case self::SYNC_ACCOUNT:
					$qry .= 'WHERE fbUserID != 0 AND travelerID != 0 ';
				break;
			}
			
			$qry .= 'LIMIT '.$fr.','.self::DEF_NUMREC;
			$rs = new iRecordset($db->execQuery($qry));
			
			$db2 = FBini::createDBHandler();
			$qry2 = "SELECT FOUND_ROWS() as totalRecords";
            $rs2 = new iRecordset($db2->execQuery($qry2));   
            self::$totalRec = $rs2->gettotalrecords();
             
			$fbUserMap = array();
			//at this point get the fbuser data
			if(self::GA_ACCOUNT != $type  && $rs->retrieveRecordCount() > 0){
				$fbUserIDs = array(); 
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					if(0!=$rs->getFbUserID()){
						$fbUserIDs[] = $rs->getFbUserID();
					}
				}
				$fbUserMap = self::getFBRecords($fbUserIDs);
			}
		
			$travelBioUsers = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					//Check the type of travelbiouser, if gaonly, use GaTravelBioUser. Otherwise, use FbTravelBioUser
					//if GA only					
					if( 0!=$rs->getTravelerID() && 0==$rs->getFbUserID() ){	
						$travelBioUser = new TravelBioUser($rs->getTravelBioUserID());
						$traveler = null;
						try{
							$traveler = new Traveler($rs->getTravelerID());
						}
						catch(exception $e){
							$traveler = null;					
						}
						if(!is_null($traveler)){
							$travelBioUsers[$rs->getTravelBioUserID()] = new GaTravelBioUser( $travelBioUser, $traveler );
						}
						else{
							$travelBioUsers[$rs->getTravelBioUserID()] = null;
						}
					}
					else{
						if(array_key_exists($rs->getFbuserID(), $fbUserMap )){
							$travelBioUsers[$rs->getTravelBioUserID()] = new FbTravelBioUser( new TravelBioUser($rs->getTravelBioUserID()), $fbUserMap[$rs->getFbuserID()] );
						}
					}
				}
			}
			
			return $travelBioUsers;
		}
		
		public static function getTotalRecords(){
			return self::$totalRec;	
		}
		
		public static function getTravelBioItems($travelBioUser){			
			require_once('travellog/model/travelbio/Class.Answer.php');
			require_once('travellog/model/travelbio/Class.ProfileQuestion.php');
			$profileQuestions = ProfileQuestion::getQuestions();			
			$answers = $travelBioUser->getAnswers();
			//get the answers for each question, display only the questions with answers
			$structAns = array();
			foreach($answers as $iAns){
				if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
					$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
				}
			}
			return $structAns;
		}
		
	  	static function addNewsFeed() {
			require_once('travellog/model/FacebookPlatform/facebook.php');
			$facebook = new Facebook(FBini::getApiKey(), FBini::getApiSecret());
			$templateBundleID = 77797845864;
			$tokens = array('id' => $facebook->get_loggedin_user());
			$friends = $facebook->api_client->friends_get();
			$friends = !empty($friends) ? implode(',', $friends) : '';
			$body_general = '';                                                                                
			
			$result = $facebook->api_client->feed_publishUserAction($templateBundleID, $tokens ,$friends, $body_general);
			self::errorCatcher($result);
		}
		
		static private function errorCatcher($result) {
			//self::errorCatcher($result);
			if (!$result) {
				throw new Exception('My Travel Bio failed on setting feed_publishUserAction');
			}
		}
		
	}
?>