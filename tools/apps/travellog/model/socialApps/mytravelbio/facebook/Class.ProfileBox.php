<?php
	require_once('travellog/model/travelbio/Class.ProfileQuestion.php');
	require_once('travellog/model/travelbio/Class.FBini.php');
	require_once('travellog/model/Class.Traveler.php');
	class ProfileBox
	{
		//construct and FBML for the given user
		public static function generateCustomProfileFBML($fbUser,$showAll=false){
			require_once('travellog/model/travelbio/Class.Answer.php');
			require_once('travellog/model/travelbio/Class.TravelBioManager.php');
			//get the number of user that the user wants to display in his profile box
			$profileQuestions = ProfileQuestion::getQuestions();
			shuffle($profileQuestions);			
			if($showAll){
				$numQ = count($profileQuestions);
			}
			else{
				$numQ = (Answer::ALL == $fbUser->getNumOfQuestionsDisplayed() ? count($profileQuestions) : $fbUser->getNumOfQuestionsDisplayed() );
			}
			$answers = $fbUser->getAnswers();
			if(0==count($answers)){
				return self::generateDefaultProfileFBML($fbUser);
			}       
			$str = '<fb:wide>';
			$str .= '<style type="text/css">'.file_get_contents('http://'.$_SERVER['HTTP_HOST'].'/css/travelbio/profilebox.css').'</style>';
			//$str .= '<fb:if-is-own-profile>' .self::generateSubtitle().'</fb:if-is-own-profile>';
			//$str .= '<fb:fbml version="1.1"><fb:visible-to-user uid="'.$fbUser->getFbUserID().'">' .self::generateSubtitle().'</fb:visible-to-user></fb:fbml>';
			$str .= self::generateSubtitle($fbUser);
			//get the answers for each question, display only the questions with answers
			$structAns = TravelBioManager::getTravelBioItems($fbUser);
			
			$ctr = 0;
			$str .= '<div id="main"><ul>';
			foreach($profileQuestions as $iQuestion){
				if(array_key_exists($iQuestion->getQuestionID(),$structAns)){	
					$str .= '<li class="entry"><h4 class="question">'.$iQuestion->getIntroduction().'</h4>';
					if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT || $iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA){
						$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
							? $structAns[$iQuestion->getQuestionID()][0]->getValue()
							: '';
						$str .='<img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/><p class="answer">'. $val.'</p><div class="clear"></div>';
					}
					elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX){
						$textVal = '';
						foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){
							if($iChoice->isTypeCheckbox() && 'Others' != $iChoice->getValue()){
								$str .='<img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/><p class="answer">'. $iChoice->getValue().'</p><div class="clear"></div>';									
							}
							elseif($iChoice->isTypeText()){
								$textVal = $iChoice->getValue(); 
							}
						}
						if(strlen($textVal)){
							$str .='<img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/><p class="answer">'. $textVal.'</p>';									
						}
					}
					$str .= '</li>';
					if($numQ == ++$ctr) break;
				}
			}			
			$str .= '</ul>';
			//show this portion if not all travelbio answers were shown			
			if( $ctr < count($structAns) ){
				$str .= '<a href="'.FBini::getFBRootDirectory().'myTravelBio.php?id='. $fbUser->getFbUserID() .'">View more</a>';
			}
			
			if($fbUser->isSynchronized()){
				try{
					$traveler = new Traveler($fbUser->getTravelerID());
				}
				catch(exception $e){
					$traveler = null;	
				}
				if(!is_null($traveler)){
					if( $ctr < count($structAns) ){
						$str .= '&nbsp;|&nbsp;';
					}
					//$str .= '<a class="viewmore" href="'.FBini::getCallbackHost().'/profile.php?action=view&travelerID='.$fbUser->getTravelerID().'">' .
					$str .= '<a class="viewmore" href="'.FBini::getCallbackHost().$traveler->getUserName().'">' .													
								'Visit <fb:name uid="'.$fbUser->getFbUserID().'" firstnameonly="true" possessive="true" /> GoAbroad.net profile'.
							'</a>';
				}
			}
			$str .= '</div>';                       
			$str .= '</fb:wide>';
			$str .= self::generateFbNarrow($profileQuestions, $structAns,$fbUser);
			return $str;
		}
		
		public static function generateDefaultProfileFBML($fbUser){
			$str = 	'<div id="title"><img class="title" src="'.FBini::getLogoPath().'"/></div>'.
					//'<fb:visible-to-user uid="'.$fbUser->getFbUserID().'">'.
			 		self::generateSubtitle($fbUser).
					//'</fb:visible-to-user>'.
					'<fb:name uid="'.$fbUser->getFbUserID().'" firstnameonly="true" possessive="true" /> Travel Bio has no answers.';
			return $str;
		}
		
		public static function generateSubtitle($fbUser){
			return 
			//'<fb:fbml version="1.1">'.
				//'<fb:visible-to-user uid="'.$fbUser->getFbUserID().'">'.
				'<fb:visible-to-owner>'.
					//'<fb:subtitle>' .
		 				//'<fb:action href="'.FBini::getFBRootDirectory().'edit.php" title="Edit your Travel Bio Answers-">Edit Travel Bio Answers</fb:action>' .
		 				//'<fb:action href="'.FBini::getFBRootDirectory().'friends.php" title="Invite your friends to have a Travel Bio-">Invite Friends</fb:action>' .
		 				//'<fb:action href="'.FBini::getFBRootDirectory().'settings.php" title="Edit your Travel Bio Settings-">Settings</fb:action>' .
						'<a href="'.FBini::getFBRootDirectory().'edit.php">Edit My Travel Bio Answers</a> | '.
						'<a href="'.FBini::getFBRootDirectory().'friends.php">Invite Friends</a> | '.
						'<a href="'.FBini::getFBRootDirectory().'settings.php">Settings</a>'.
	 				//'</fb:subtitle>'.
				'</fb:visible-to-owner>';
				//'</fb:visible-to-user>'.
			//'</fb:fbml>';	
		}

		public static function generateFbNarrow($profileQuestions, $structAns,$fbUser){
			$ctr = 0;
			$str = '<fb:narrow><div style="text-align:right;border-bottom: 1px solid #efefef;padding-bottom:3px;"> <a href="http://apps.new.facebook.com/travelbio/myTravelBio.php?id='.$fbUser->getFbUserID().'">View All</a> </div>
			 <style type="text/css">'.file_get_contents('http://'.$_SERVER['HTTP_HOST'].'/css/travelbio/profilebox.css').'</style><ul style="margin-top:3px;">';
			foreach($profileQuestions as $iQuestion){
				if(array_key_exists($iQuestion->getQuestionID(),$structAns)){	
					$str .= '<li class="entry"><h4 class="question">'.$iQuestion->getIntroduction().'</h4>';
					if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT || $iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA){
						$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
							? $structAns[$iQuestion->getQuestionID()][0]->getValue()
							: '';
						$str .='<img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/><p class="answer">'. $val.'</p><div class="clear"></div>';
					}
					elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX){
						$textVal = '';
						foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){
							if($iChoice->isTypeCheckbox() && 'Others' != $iChoice->getValue()){
								$str .='<img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/><p class="answer">'. $iChoice->getValue().'</p><div class="clear"></div>';									
							}
							elseif($iChoice->isTypeText()){
								$textVal = $iChoice->getValue(); 
							}
						}
						if(strlen($textVal)){
							$str .='<img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/><p class="answer">'. $textVal.'</p>';									
						}
					}
					$str .= '</li>';
					$ctr++;
					if($ctr == 3) break;
				}

			}			
			$str .= '</ul> </fb:narrow>';

        	return $str;
		}
	}
?>