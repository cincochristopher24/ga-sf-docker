<?php
interface IController {
  function setModelAndView(IModelAndView $modelAndView);
  static function sendResponse(IController $controller);
  function applyInputToModel();
}

abstract class AbstractController implements IController {
  protected $modelAndView;

  function setModelAndView(IModelAndView $modelAndView) {
    $this->modelAndView = $modelAndView;
  }

  static function sendResponse(IController $controller) {
    $controller->setModelAndView($controller->applyInputToModel());
    $controller->modelAndView->applyModelToView();
  }
}

interface IModelAndView {
  function setView($view);
  function setModel($model);
  function setModelValue($key, $value);
  function applyModelToView();
}

abstract class AbstractModelAndView implements IModelAndView {
  protected $view;
  protected $model;

  function setView($view) {
    $this->view = $view;
  }
  function setModel($model) {
    $this->model = $model;
  }
  function setModelValue($key, $value) {
    $this->model[$key] = $value;
  }
  protected function preserveSession() {
    $session = $GLOBALS['_SESSION'];
    unset($GLOBALS);
    $GLOBALS['_SESSION'] = $session;
  }
  function applyModelToView() {
    //$this->preserveSession();
    $model = $this->model;

    foreach ($model as $key => $value) {
      $$key = $value;
    }

    include($this->view);
  }
}

class ModelAndView extends AbstractModelAndView {
  function __construct($view) {
    $this->setView($view);
  }
}
?>