<?php
/**
	Filename:		gaRegion.class.php
	@author			Jonas Tandinco
	Created:		Feb/28/2008
	Purpose:		temporary class for getting region coordinates
*/

class gaRegion {
	
	public static function getRegionCoordinates($regionID) {
		switch ($regionID) {
			case 1:		// AFRICA
				return array('lat' => 0.213671, 'long' => 16.98485);
			
			case 2:		// ASIA
				return array('lat' => 34.364439, 'long' => 108.3307);
			
			case 3:		// AUSTRALIA
				return array('lat' => -25.274398, 'long' => 133.775136);
			
			case 4:		// EUROPE
			case 10:		
				return array('lat' => 58.299985, 'long' => 23.0493);
			
			case 5:		// AMERICA
				return array('lat' => 37.09024, 'long' => -95.712891);
			
			case 6:		// CARRIBEAN
				return array('lat' => 14.604847155053898, 'long' => -86.484375);
				
			case 8:		// MIDDLE EAST
				return array('lat' => 21.94304553343818, 'long' => 52.03125); 
				
			case 11:	// ARAUCANIA
				return array('lat' => 0, 'long' => 0);
			
			case 7:		// NORTH AMERICA
				return array('lat' => 45.583289756006316, 'long' => -93.515625);
			
			case 9:		// SOURTH AMERICA
				return array('lat' => -21.735104, 'long' => -63.28125);
			
			default:
				throw new Exception('invalid regionID');
		}

//		$regionCoordinates = array();
//		
//		$regionCoordinates[] = array ('id' => 1,  'name' => 'Africa',                   'lat' =>  0.213671, 'long' => 16.98485);
//		//$regionCoordinates[] = array ('id' => 11, 'name' => 'Araucania',               'lat' => 0, 'long' => 0);
//		$regionCoordinates[] = array ('id' => 2,  'name' => 'Asia',                     'lat' =>  34.364439, 'long' => 108.3307);
//		$regionCoordinates[] = array ('id' => 3,  'name' => 'Australia & Oceania',      'lat' => -25.274398, 'long' => 133.775136);
//		$regionCoordinates[] = array ('id' => 6,  'name' => 'Caribbean',               'lat' => 0, 'long' => 0);
//		$regionCoordinates[] = array ('id' => 10, 'name' => 'Eastern Europe & Russia', 'lat' => 0, 'long' => 0);
//		$regionCoordinates[] = array ('id' => 5,  'name' => 'Central America',         'lat' => 0, 'long' => 0);
//		$regionCoordinates[] = array ('id' => 8,  'name' => 'Middle East',             'lat' => 0, 'long' => 0);
//		$regionCoordinates[] = array ('id' => 7,  'name' => 'North America',           'lat' => 0, 'long' => 0);
//		$regionCoordinates[] = array ('id' => 9,  'name' => 'South America',           'lat' => 0, 'long' => 0);
//		$regionCoordinates[] = array ('id' => 4,  'name' => 'Western Europe',          'lat' => 0, 'long' => 0);
//		
//		return $regionCoordinates;
	}
	
}