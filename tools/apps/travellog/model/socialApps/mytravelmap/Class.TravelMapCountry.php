<?php
/*
	Filename:		Class.TravelMapCountry.php
	Author:			Jonas Tandinco
	Created:		Dec/20/2007
	Purpose:		represent info for a country in my Travel Maps
	
	EDIT HISTORY:
	
	Jan/07/2007 by Jonas Tandinco
		1.	implemented the getRandomPhoto functionality
		
	Jan/08/2007 by Jonas Tandinco
		1.	passed a valid journal entry ID to a new TravelLog object passed to a new Photo object
			in selectRandomPhoto()
*/

require_once 'travellog/model/Class.Photo.php';

class TravelMapCountry {
	private $numPhotos         = 0;
	private $numJournalEntries = 0;
	private $numTravelTips     = 0;
	
	private $latitude          = 0;
	private $longitude         = 0;
	
	private $countryID         = 0;
	private $countryName       = '';
	private $regionID          = NULL;
	
	private $locationID        = NULL;
	private $travelMapCities   = array();	
	
	private $photoIDs          = array();
	private $randomPhoto       = null;			// random photo from journal entries of a certain country
	
	protected $isEditable      = NULL;
	
	// CONSTRUCTOR
	public function __contruct($countryID = 0) {
		$this->countryID         = $countryID;
		$this->numJournalEntries = 0;
	}
	
	// GETTERS
	public function isEditable() {
		return $this->isEditable;
	}
	
	public function getRegionID() {
		return $this->regionID;
	}
	
	public function getPhotoIDs() {
		return $this->photoIDs;
	} 
	
	// returns a hyperlink to a random photo of this country of this traveler
	public function getRandomPhoto() {
		if (is_null($this->randomPhoto)) {
			$this->randomPhoto = $this->selectRandomPhoto();
		}
		
		return $this->randomPhoto;
	}
	
	public function getCountryID() {
		return $this->countryID;
	}
	
	public function getCountryName() {
		return $this->countryName;
	}
	
	public function getNumPhotos() {
		return $this->numPhotos;
	}
	
	public function getNumJournalEntries() {
		return $this->numJournalEntries;
	}
	
	public function getNumTravelTips() {
		return $this->numTravelTips;
	}
	
	public function getLatitude() {
		return $this->latitude;
	}
	
	public function getLongitude() {
		return $this->longitude;
	}

	public function getLocationID() {
		return $this->locationID;
	}

	function getCities() {
		return $this->travelMapCities;
	}
	
	// SETTERS
	public function setIsEditable($v) {
		$this->isEditable = $v;
	}
	
	public function setRegionID($regionID) {
		$this->regionID = $regionID;
	}
	
	public function setPhotoIDs($photoIDs) {
		$this->photoIDs = $photoIDs;
	}
	
	public function setRandomPhoto($randomPhoto) {
		$this->randomPhoto = $randomPhoto;
	}
	
	public function setCountryID($countryID) {
		$this->countryID = $countryID;
	}
	
	public function setCountryName($countryName) {
		$this->countryName = $countryName;
	}
	
	public function setNumPhotos($numPhotos) {
		$this->numPhotos = $numPhotos;
	}
	
	public function setNumJournalEntries($numJournalEntries) {
		$this->numJournalEntries = $numJournalEntries;
	}
	
	public function setNumTravelTips($numTravelTips) {
		$this->numTravelTips = $numTravelTips;
	}
	
	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}
	
	public function setLongitude($longitude) {
		$this->longitude = $longitude;
	}
 
	public function setLocationID($locationID) {
		$this->locationID = $locationID;
	}

	function setCities($travelMapCities = array()) {
		$this->travelMapCities = $travelMapCities;
	}
   
	// OTHER
	public function hasRandomPhoto() {
		return !is_null($this->randomPhoto);
	}
	
	private function selectRandomPhoto() {
		$photoIDs = $this->photoIDs;
		
		$randomIndex = mt_rand(0, count($photoIDs) - 1);
		$selectedPhotoID = $photoIDs[$randomIndex];
		
		// turn the selected photo ID into a Photo Object
		
		// get journalEntryID of the selected photo
		$journalEntryID = TravelMapPeer::getJournalEntryID($selectedPhotoID);
		
		$selectedPhoto = new Photo(new TravelLog($journalEntryID), $selectedPhotoID);
		
		return $selectedPhoto->getPhotoLink('thumbnail');
	}
	
}