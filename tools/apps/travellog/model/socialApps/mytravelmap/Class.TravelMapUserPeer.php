<?php

require_once 'Class.dbHandler.php';
require_once 'Class.iRecordset.php';
require_once('travellog/model/socialApps/mytravelmap/Class.TravelMapUser.php');

class TravelMapUserPeer {
	static private function populateObject($row) {
		if(!empty($row)) {
			$user = new TravelMapUser;
			$user->initialize($row);
			return $user;
		}
		return null;
	}
	
	static public function retrieveTravelMapUserByUserID($id) {
		if(!$id) return null;
		
		$conn = self::getConnection('SocialApplication');
		$sql = "SELECT * FROM tbUser 
	            WHERE userID = $id";

		$resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			return self::populateObject($record);	
		}
		return null;
	}
	
	static public function retrieveTravelMapUserByFacebookID($id) {
		if(!$id) return null;
		
		$conn = self::getConnection('SocialApplication');
		$sql = "SELECT * FROM tbUser 
	            WHERE facebookID = '$id'";

		$resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			return self::populateObject($record);	
		}
		return null;
	}
	
	static public function retrieveTravelMapUserByTravelerID($id) {
		if(!$id) return null;
		
		$conn = self::getConnection('SocialApplication');
		$sql = "SELECT * FROM tbUser 
	            WHERE travelerID = $id";

		$resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			return self::populateObject($record);	
		}
		return null;
	}
	
	static public function retrieveUserCredentialsFromFbID($fbID = null) {
		$userCreds = array();
		$user = self::retrieveTravelMapUserByFacebookID($fbID);
		if($user) {
			$userCreds = array(
				'userID' 	 => $user->getUserID(), 
				'facebookID' => $user->getFacebookID(), 
				'travelerID' => $user->getTravelerID(), 
				'pinSetting' => $user->getPinSetting(), 
				'displayMapTip' => $user->getDisplayMapTip(), 
				'message'	 => 'ok');
		}
		return $userCreds;
	}
	
	static public function updatePinSetting($pinSetting = 0, $userID=null) {
		if(!$userID) return false;
		$conn = self::getConnection('SocialApplication');
		$sql = "UPDATE tbUser 
	            SET displayPinSetting = $pinSetting 
	            WHERE userID = $userID";

		return $conn->execQuery($sql);
	}

	static public function updateDisplayMapTip($userID,$value = 'no') {
		$conn = self::getConnection('SocialApplication');
		$sql = "UPDATE tbUser 
	            SET displayMapTip = '$value' 
	            WHERE userID = $userID";

		return $conn->execQuery($sql);
	}
	
	
	private static function getConnection($dbName = 'SocialApplication'){
  		$conn = new dbHandler();  	
		$conn->setDB($dbName);
		return $conn;
	}
	
}