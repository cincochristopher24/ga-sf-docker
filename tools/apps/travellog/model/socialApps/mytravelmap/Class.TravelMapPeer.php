<?php
/*
	Filename:		Class.TravelMapPeer.php
	Author:			Jonas Tandinco
	Created:		Dec/19/2007
	Purpose:		handles database access for a travel map
	
	EDIT HISTORY:
	
	Jan/07/2008 by Jonas Tandinco
		1.	added getPhotoIDs() used to retrieve a random photo
		
	Jan/08/2008 by Jonas Tandinco
		1.	added getJournalEntryID for temporary use of TravelMapCountry object to create Photo objects that
			requires an object context (TravelLogs) that also requires a valid journal entry ID
			
	Jan/16/2008 by Jonas Tandinco
		1.	removed getCountriesTraveled() and replace it with getCountryNames() used for displaying the country
			names traveled under myTravelMaps
			
	Jan/17/2008 by Jonas Tandinco
		1.	removed getCountryNames()
*/

require_once 'Class.Connection.php';
require_once 'Class.Recordset.php';
require_once 'Class.TravelMapCountry.php';
require_once 'Cache/ganetCacheProvider.php';

class TravelMapPeer {

	/**
	 * retrieves the traveler's travel map
	 * 
	 * @param travelerID
	 * @return travelMap object
	 */
	public static function getTravelMap($travelerID) {
		require_once 'Class.TravelMap.php';
		
		$travelMap = new TravelMap();
		
		$travelMap->setCountriesTraveled ( self::getCountriesTraveled($travelerID) );
		$travelMap->setGMapKey           ( self::getGMapKey()                      );
		
		return $travelMap;
	}
	
	public static function getGMapKey() {
		/***
		require_once 'Class.Ini_Config.php';
		$ini = Ini_Config::instance();
		
		return $ini->getGMapKey();
		***/
		/***
			Modified By Naldz(Dec. 16, 2008)
		***/
		require_once('travellog/model/Class.SiteContext.php');
		return SiteContext::getInstance()->getGMapApiKey();
	}
	
	public static function getCountriesTraveled($travelerID) {
		// get travelmap countries from cache
		$cache = ganetCacheProvider::instance()->getCache();
		if ($cache != null) {
			$allCountriesTraveled = $cache->get('Traveler_Countries_Traveled_' . $travelerID  );
			if ($allCountriesTraveled) {
				//echo 'Found (TravelMapPeer) Traveler_Countries_Traveled_' . $travelerID . ' <br/> '; 
				return $allCountriesTraveled;	
			}			
		}

		require_once 'gaOpenSocialTravelerMapper.php';
		$userID = gaOpenSocialTravelerMapper::getUserIDWithTravelerID($travelerID);
		
		require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.TravelMapEntry.php';
        require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.MyTravelMapDAO.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCityLocation.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCountryLocation.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocation.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocationPeer.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBNewCity.php';
		$allCountriesTraveled = MyTravelMapDAO::__retrieveSyncMapEntries($userID, $travelerID);
		uasort($allCountriesTraveled, array('TravelMapPeer', 'cmp'));
        return $allCountriesTraveled;
		// $allCountriesTraveledIDs = array();
		// $allCountriesTraveled    = array();
		// 
		// // 1. GET COUNTRIES FROM WRITTEN JOURNAL ENTRIES
		// $countriesFromJournalEntries = self::getCountriesFromJournalEntries($travelerID);
		// 
		// foreach ($countriesFromJournalEntries as $country) {
		// 	$allCountriesTraveledIDs[] = $country->getCountryID();
		// 	$allCountriesTraveled[]    = $country;
		// }
		// 
		// // 2. GET COUNTRIES FROM MY TRAVEL PLANS
		// require_once 'gaOpenSocialTravelerMapper.php';
		// 
		// $userID = gaOpenSocialTravelerMapper::getUserIDWithTravelerID($travelerID);
		// $countriesFromMyTravelPlans = self::getCountriesFromMyTravelPlans($userID);
		// 
		// foreach ($countriesFromMyTravelPlans as $country) {
		// 	if (!in_array($country->getCountryID(), $allCountriesTraveledIDs)) {
		// 		$allCountriesTraveledIDs[] = $country->getCountryID();
		// 		$allCountriesTraveled[]    = $country;
		// 	}
		// }
		// 
		// // 3. GET COUNTRIES FROM COUNTRIES I'VE TRAVELED TO
		// $countriesFromCountriesTraveledTo = self::getCountriesFromCountriesTraveledTo($travelerID);
		// 
		// foreach ($countriesFromCountriesTraveledTo as $country) {
		// 	if (!in_array($country->getCountryID(), $allCountriesTraveledIDs)) {
		// 		$allCountriesTraveledIDs[] = $country->getCountryID();
		// 		$allCountriesTraveled[]    = $country;
		// 	}
		// }
		// 
		// // set countries traveled to cache
		// if ($cache != null && count($allCountriesTraveled) > 0){
		// 	$cache->set('Traveler_Countries_Traveled_' . $travelerID, $allCountriesTraveled,array('EXPIRE'=>3600));
		// }
		// 
		// return $allCountriesTraveled;
	}
	
	static function cmp($mapEntry1, $mapEntry2) {
		return strcmp($mapEntry1->getCountry()->getName(), $mapEntry2->getCountry()->getName());
	}
	
	
	public static function getCountriesFromMyTravelPlans($userID) {
		$conn = new Connection('SocialApplication');
		$rs   = new Recordset($conn);

		$sql = "SELECT DISTINCT countryID " .
			   "FROM tbTravelWishToCountry, tbTravelWish " .
			   "WHERE startDate <= '" . date('Y-m-d') . "' " .
			   "AND startdate <> '0000-00-00' " .
			   "AND tbTravelWishToCountry.travelWishID = tbTravelWish.travelWishID " .
			   "AND tbTravelWish.userID = " . $userID;

		$rs->Execute($sql);

		$travelMapCountries = array();
		if ($rs->Recordcount() > 0) {
			$arrCountries = array();
			while ($row = mysql_fetch_array($rs->Resultset())){
				$countryID = $row['countryID'];
				
				if (!in_array($countryID, $arrCountries)) {
					$arrCountries[] = $row['countryID'];
				}
			}
			
			// convert to travelMapCountry objects
			foreach ($arrCountries as $countryID) {
				//$country = new Country($countryID);
				$country = LocationFactory::instance()->createCountry($countryID);
				
				$travelMapCountry = new TravelMapCountry($countryID);
				
				$travelMapCountry->setCountryName       ( $country->getName()                      );
				$travelMapCountry->setCountryID         ( $countryID                               );
				$travelMapCountry->setLongitude         ( $country->getCoordinates()->getX()       );
				$travelMapCountry->setLatitude          ( $country->getCoordinates()->getY()       );
				$travelMapCountry->setRegionID          ( $country->getRegionID()                  );
				$travelMapCountry->setIsEditable        ( FALSE                                    );
				
				$travelMapCountries[] = $travelMapCountry;
			}
		}

		return $travelMapCountries;
	}
	
	// retrieve countries from countries I've traveled to
	public static function getCountriesFromCountriesTraveledTo($travelerID) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		$sql = "SELECT countryID, locationID " .
			   "FROM tblTravelertoCountriesTravelled " .
			   "WHERE travelerID = " . $travelerID;
		$rs->Execute($sql);

		$travelMapCountries = array();
		if ($rs->Recordcount() > 0) {
			$arrCountries       = array();
			while ($row = mysql_fetch_array($rs->Resultset())){
				$cID = $row['countryID'];
				$lID = $row['locationID'];
				
				if (0 != $cID && (!in_array($cID, $arrCountries))) {
					$arrCountries[] = $row['countryID'];
				}
				else if(0 == $cID && 0 < $lID) {
					require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocation.php');
					$location = new FBLocation();
					$location->setID($lID);
					$obj = $location->getLocation();
					if($obj instanceof State) {
						$country_ID = 91;
					}
					else {
						$country_ID = $obj->getCountryID();
					}
					if(!in_array($country_ID, $arrCountries)) {
						$arrCountries[] = $country_ID;
					}
				}
			}
	
			// convert to travelMapCountry objects
			foreach ($arrCountries as $countryID) {
				//$country = new Country($countryID);
				$country = LocationFactory::instance()->createCountry($countryID);
				
				$travelMapCountry = new TravelMapCountry($countryID);
				
				$travelMapCountry->setCountryName       ( $country->getName()                      );
				$travelMapCountry->setCountryID         ( $countryID                               );
				$travelMapCountry->setLongitude         ( $country->getCoordinates()->getX()       );
				$travelMapCountry->setLatitude          ( $country->getCoordinates()->getY()       );
				$travelMapCountry->setRegionID          ( $country->getRegionID()                  );
				$travelMapCountry->setIsEditable        ( TRUE                                     );
				
				$travelMapCountries[] = $travelMapCountry;
			}			
		}

		return $travelMapCountries;
	}
	
	// returns an array of TravelMapCountry objects
	// TODO: do not use traveler object but do a direct SQL query to retrieve data
	public static function getCountriesFromJournalEntries($travelerID) {
		$traveler = new Traveler($travelerID);
		
		$arrCountries                = array();
     	$travelMapCountries          = array();
     	$arrJournalEntriesPerCountry = array();
     	$arrTravelTipsPerCountry     = array();
     	$arrTravelPhotosPerCountry   = array();
     	$arrPhotoIDs                 = array();     	
     	
     	foreach ($traveler->getTravels(null) as $journal) {

   			$trips = $journal->getTrips();

			if (count($trips)) {
   				foreach($trips as $trip){

				foreach ($trip->getTravelLogs() as $journalEntry) {
					$countryID = NULL;
					try{
						$location = $journalEntry->getTrip()->getLocation();
						if(is_object($location)){
							$countryID = $location->getCountry()->getCountryID();
						}else{
							continue;
						}
					}catch(Exception $e){
						continue;
					}
					
					// compute number of journal entries per journal
					if (array_key_exists($countryID, $arrJournalEntriesPerCountry)) {
						$arrJournalEntriesPerCountry[$countryID]++;
					} else {
						$arrJournalEntriesPerCountry[$countryID] = 1;
					}

					// compute number of travel tips per journal entry
					$numTravelTips = self::doCountTravelTips($journalEntry->getTravelLogID());
					
					if (array_key_exists($countryID, $arrTravelTipsPerCountry)) {
						$arrTravelTipsPerCountry[$countryID] += $numTravelTips;
					} else {
						$arrTravelTipsPerCountry[$countryID]  = $numTravelTips;
					}

					// compute number of travel photos per journal entry
					// alternative: $numTravelPhotos = count($journalEntry->getPhotos());
					$numTravelPhotos = self::doCountTravelPhotos($journalEntry->getTravelLogID());
					
					if (array_key_exists($countryID, $arrTravelPhotosPerCountry)) {
						$arrTravelPhotosPerCountry[$countryID] += $numTravelPhotos;
					} else {
						$arrTravelPhotosPerCountry[$countryID]  = $numTravelPhotos;
					}
					
					if ($numTravelPhotos > 0) {
						// get the photo IDs of photos for a journal entry
						if (array_key_exists($countryID, $arrPhotoIDs)) {
							$arrPhotoIDs[$countryID] = array_merge($arrPhotoIDs[$countryID], self::getPhotoIDs($journalEntry->getTravelLogID()));
						} else {
							$arrPhotoIDs[$countryID] = self::getPhotoIDs($journalEntry->getTravelLogID());
						}
					}
					
    					if(!in_array($countryID, $arrCountries)) {
						$arrCountries[] = $countryID;
    					}
				}
				
   				}
			}
   		}

		// create TravelMapCountry objects
		foreach ($arrCountries as $countryID) {
			//$country = new Country($countryID);
			$country = LocationFactory::instance()->createCountry($countryID);
			
			$travelMapCountry = new TravelMapCountry($countryID);
			
			$travelMapCountry->setCountryName       ( $country->getName()                      );
			$travelMapCountry->setCountryID         ( $countryID                               );
			$travelMapCountry->setLongitude         ( $country->getCoordinates()->getX()       );
			$travelMapCountry->setLatitude          ( $country->getCoordinates()->getY()       );
			$travelMapCountry->setRegionID          ( $country->getRegionID()                  );
			$travelMapCountry->setNumJournalEntries ( $arrJournalEntriesPerCountry[$countryID] );
			$travelMapCountry->setNumTravelTips     ( $arrTravelTipsPerCountry[$countryID]     );
			$travelMapCountry->setNumPhotos         ( $arrTravelPhotosPerCountry[$countryID]   );
			$travelMapCountry->setIsEditable        ( FALSE                                    );
			
			if ($arrTravelPhotosPerCountry[$countryID] > 0) {
				$travelMapCountry->setPhotoIDs($arrPhotoIDs[$countryID]);
			}
			
			$travelMapCountries[] = $travelMapCountry;
		}
    	
     	return $travelMapCountries;
	}
	
	// returns the number of journal entries per journal
	// NOTE: this function is currently unused
	public static function doCountJournalEntries($journalID) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		$sql = "SELECT count(travellogID) " .
			   "FROM tblTravel, tblTrips, tblTravelLog " .
			   "WHERE travelID = $journalID" .
			   "AND tblTravel.travelID = tblTrips.travelID " .
			   "AND tblTravelLog.tripID = tblTrips.tripID";

		$rs->Execute($sql);
		
		$row = mysql_fetch_row($rs->Resultset());

		return $row[0];
	}

	// returns the number of travel trips per journal entry
	public static function doCountTravelTips($journalEntryID) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		$sql = "SELECT count(travelTipID) " .
			   "FROM tblTravelTips " .
			   "WHERE travellogID = " . $journalEntryID;

		$rs->Execute($sql);
		
		$row = mysql_fetch_row($rs->Resultset());

		return $row[0];
	}

	// returns the number of travel photos per journal entry
	public static function doCountTravelPhotos($journalEntryID) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		// retrieve number of photos associated to a travel journal entry
		$sql = "SELECT count(photoID) " .
			   "FROM tblTravelLogtoPhoto " .
			   "WHERE travellogID = " . $journalEntryID;

		$rs->Execute($sql);
		
		$row = mysql_fetch_row($rs->Resultset());

		return $row[0];
	}
	
	// overwrite list of countries traveled
	// Note: $arrCountries = array of countryID and locationID (format = '69_68'). 
	public static function setCountriesTraveled($travelerID, $arrEntries) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		// delete existing values
		$sql = "DELETE FROM tblTravelertoCountriesTravelled " .
			   "WHERE travelerID = $travelerID";
			   
		$rs->Execute($sql);

		// add the new values
		$sql = "INSERT INTO tblTravelertoCountriesTravelled (travelerID, countryID, locationID) VALUES ";
		foreach ($arrEntries as $entry) {
			list($countryID,$locationID) = explode('_',$entry);
		   	$sql .= "($travelerID, $countryID, $locationID),";
		}
		$rs->Execute(substr($sql,0,-1));
	}
	
	// improved count of countries from journal entries
	public static function getCountriesFromJournalEntries2($travelerID) {
		
	}
	
	// retrieve the photoIDs of a certain journal entry
	public static function getPhotoIDs($journalEntryID) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		$sql = "SELECT photoID " .
			   "FROM tblTravelLogtoPhoto " .
			   "WHERE travellogID = $journalEntryID";

		$rs->Execute($sql);
		
		$arrPhotoIDs = array();
		
		while ($row = mysql_fetch_row($rs->Resultset())) {
			$arrPhotoIDs[] = $row[0];
		}

		return $arrPhotoIDs;
	}
	
	/**
	* EDITS: exclude deleted journal entries
	* last edited: 2009-08-29
	* last edited by: Augustianne Laurenne Barreta
	**/
	// helper function that returns the journalEntryID given a photoID
	public static function getJournalEntryID($photoID) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		$sql = "SELECT tblTravelLogtoPhoto.travellogID " .
			   "FROM tblTravelLogtoPhoto, tblTravelLog " .
			   "WHERE photoID = $photoID " .
					"AND tblTravelLogtoPhoto.travellogID = tblTravelLog.travellogID " . 
					"AND tblTravelLog.deleted = 0";

		$rs->Execute($sql);
		
		$row = mysql_fetch_row($rs->Resultset());

		return $row[0];
	}
	
	/**
	 * updates the countries traveled of an unsynchronized account
	 * 
	 * @param userID
	 * @param arrCountryIDs - new countries selected
	 */
	public static function saveUnsyncTravelMap($userID, $arrCountryIDs) {
		$rs = new Recordset(new Connection('SocialApplication'));

		// delete existing values
		$sql = "DELETE FROM tbTravelMap " .
			   "WHERE userID = $userID";
			   
		$rs->Execute($sql);

		// add the new values
		foreach ($arrCountryIDs as $countryID) {
			$sql = "INSERT INTO tbTravelMap (userID, countryID) " .
				   "VALUES ($userID, $countryID)";
			$rs->Execute($sql);			
		}
	}
	
	/**
	 * synchronizes an opensocial account to a goabroad.net account
	 * 
	 * @param container - opensocial site (e.g. orkut.com)
	 * @param containerID - id provided by the opensocial site for account identification
	 * @param theTravelerID - the goabroad.net account
	 * 
	 * @return TRUE if successful
	 * @return FALSE upon failure
	 */
	public static function sync($container, $containerID, $theTravelerID) {
		require_once 'gaOpenSocialTravelerMapper.php';
		
		$rs = new Recordset(new Connection('SocialApplication'));
		
		$containerField = gaOpenSocialTravelerMapper::getContainerField($container);
		
		$query = "SELECT userID, travelerID " .
				 "FROM tbUser " .
				 "WHERE $containerField = $containerID " .
				 "AND travelerID = 0 " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			$row = mysql_fetch_row($rs->Resultset());
			
			$userID     = $row[0];
			$travelerID = $row[1];
			
			// delete temporary countries
			$query = "DELETE FROM tbTravelMap " .
					 "WHERE userID = $userID";
			$rs->Execute($query);
			
			// delete the temporary account
			$query = "DELETE FROM tbUser " .
					 "WHERE userID = $userID";
			$rs->Execute($query);
					 
			// sync account
			$query = "UPDATE tbUser " .
					 "SET $containerField = $containerID " .
					 "WHERE travelerID = $theTravelerID";
			$rs->Execute($query);
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * change the owner of an opensocial app account
	 * 
	 * @param container     (e.g. orkut.com, myspace.com)
	 * @param containerID   (opensocial container assigned ID)
	 * @param newTravelerID (new .net account to be linked)
	 * 
	 * @return TRUE if successful
	 * @return FALSE otherwise
	 */
	public static function changeAccount($container, $containerID, $newTravelerID) {
		require_once 'gaOpenSocialTravelerMapper.php';
		
		$rs = new Recordset(new Connection('SocialApplication'));
		
		$containerField = gaOpenSocialTravelerMapper::getContainerField($container);
		
		// TODO: should be in transcation

		// unlink previous account
		$query = "UPDATE tbUser " .
				 "SET $containerField = 0 " .
				 "WHERE $containerField = $containerID";
		$rs->Execute($query);

		// link new account
		$query = "UPDATE tbUser " .
				 "SET $containerField = $containerID " .
				 "WHERE travelerID = $newTravelerID";
		$rs->Execute($query);
		
		return TRUE;
	}
	
	/**
	 * retrieves countryIDs for unsync accounts
	 * 
	 * @param userID
	 * 
	 * @return array of countryIDs
	 */
	public static function getUnsyncCountriesTraveledIDs($userID) {
		$rs = new Recordset(new Connection('SocialApplication'));
		
		$query = "SELECT countryID " .
				 "FROM tbTravelMap " .
				 "WHERE userID = $userID";
		
		$rs->Execute($query);
		
		$result = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_row($rs->Resultset())) {
				$result[] = $row[0];
			}
		}
		
		return $result;
	}
	
	/**
	 * retrieves unsynchronized travel map data
	 * 
	 * @param userID
	 * 
	 * @return array of country data
	 */
	public static function getUnsyncTravelMapData($userID) {
		$rs = new Recordset(new Connection('SocialApplication'));
		
		$query = "SELECT countryID " .
				 "FROM tbTravelMap " .
				 "WHERE userID = $userID";
		
		$rs->Execute($query);
		
		$result = array();
		if ($rs->Recordcount() > 0) {
			// turn countryIDs into travelMapCountry objects
			require_once 'travellog/model/Class.Country.php';
			require_once 'Class.TravelMapCountry.php';
			
			$arrTravelMapCountries = array();
			while ($row = mysql_fetch_row($rs->Resultset())) {
				$countryID = $row[0];
				//$country = new Country($countryID);
				$country = LocationFactory::instance()->createCountry($countryID);
				
				$travelMapCountry = new TravelMapCountry($countryID);
				
				$travelMapCountry->setCountryName( $country->getName()                );
				$travelMapCountry->setCountryID  ( $country->getCountryID()           );
				$travelMapCountry->setLongitude  ( $country->getCoordinates()->getX() );
				$travelMapCountry->setLatitude   ( $country->getCoordinates()->getY() );
				
				$arrTravelMapCountries[] = $travelMapCountry;
			}
			
			foreach ($arrTravelMapCountries as $country) {
				$countryData = array();
				
				$countryData['countryName'] = $country->getCountryName();
				
				$countryData['lat']         = $country->getLatitude();
				$countryData['long']        = $country->getLongitude();
				
				$result[$country->getCountryID()] = $countryData;
			}
		}
		
		return $result;
	}

	// added by: Adelbert
	// Date: May 14, 2009
	// Purpose: Check if the GoAbroad.net user account is already sync with other opensocial user
	public static function isGanetAccountAlreadySync($container,$travelerID) {
		require_once 'gaOpenSocialTravelerMapper.php';
		$rs = new Recordset(new Connection('SocialApplication')); 
		$containerField = gaOpenSocialTravelerMapper::getContainerField($container);
		$query = "SELECT count(*) FROM tbUser WHERE travelerID=$travelerID AND $containerField!=0 LIMIT 1";	
		$rs->Execute($query);
		$row = mysql_fetch_row($rs->Resultset());
		return $row[0];
  	}   	
  	
  	// Date: Dec 1, 2009
  	// Purpose: overwrite list of locations traveled, instead of using countryID, locationID is now used
	public static function _setLocationsTraveled($travelerID, $arrLocations) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		// delete existing values
		$sql = "DELETE FROM tblTravelertoCountriesTravelled " .
			   "WHERE travelerID = $travelerID";
			   
		$rs->Execute($sql);

		// add the new values
		foreach ($arrLocations as $locationID) {
			$sql = "INSERT INTO tblTravelertoCountriesTravelled (travelerID, locationID) " .
				   "VALUES ($travelerID, $locationID)";
			
			$rs->Execute($sql);
		}
	}

}
