<?php
require_once('Cache/ganetCacheProvider.php');

class MyTravelMapRepository {
  private $userCredentials = array('userID' => 0, 'facebookID' => 0, 'travelerID' => 0);
  private $uneditableEntries = array();
  private $cache = null;
  
  private function getCache() {
    if($this->cache == null)
        $this->cache = ganetCacheProvider::instance()->getCache();
	return $this->cache;
  }
  
  private function clearCache() {
	//$this->getCache()->delete('TravelMapEntriesofUser_' . $this->userCredentials['userID']);
	$this->getCache()->delete(MyTravelMapDAO::getMtmUserCacheKey($this->userCredentials['userID']));
	//invalidate cache from dotnet if sync-ed account
	if($this->userCredentials['travelerID'])
		$this->getCache()->delete('Traveler_Countries_Traveled_' . $this->userCredentials['travelerID']);
  }

  public function __construct($userCredentials) {
    $this->userCredentials = $userCredentials;
  }

  public function setUserCredentials($userCredentials) {
    $this->userCredentials = $userCredentials;
  }

  public function getCountryList() {
    return MyTravelMapDAO::getCountryList();
  }  

  public function updateEntries($countryIDs) {

    if ($this->userCredentials['facebookID']==0) return;

	//TODO finalized code
	// edited by adelbert
    if ($travelerID = $this->userCredentials['travelerID']) {
		require_once('gaLogs/Class.GaLogger.php');
      	require_once 'travellog/model/socialApps/mytravelmap/Class.TravelMapPeer.php';
		GaLogger::create()->start($travelerID,'TRAVELER');
      	TravelMapPeer::setCountriesTraveled($travelerID, $countryIDs);
//      MyTravelMapDAO::updateEntriesSynced($countryIDs, $this->userCredentials['travelerID']);
    } else {
      MyTravelMapDAO::updateEntries($countryIDs, $this->userCredentials['userID']);
    }
  }

  // NOTE: this is designed (to avoid duplicate db queries) to be called after getTravelMapEntries 
  // is invoked, otherwise this will return an emtpy array.
  public function getUneditableMapEntries() {
    return $this->uneditableEntries;
  }

  public function getTravelMapEntries() {
    $entries = array();
    $this->uneditableEntries = array();

    if ($this->userCredentials['facebookID']==0) return $entries;

    if ($this->userCredentials['travelerID']) {
      //entries from GaNET journals
      $entries = MyTravelMapDAO::getTravelMapEntriesSynced($this->userCredentials['travelerID']);
      $entriesFromMyTravelPlans = MyTravelMapDAO::getTravelMapEntriesFromMyTravelPlans($this->userCredentials['userID']);
      $entriesFromCountriesTraveledTo = MyTravelMapDAO::getTravelMapEntriesFromCountriesTraveledTo($this->userCredentials['travelerID']);

      if ($entries) {
        foreach ($entriesFromMyTravelPlans as $e) {
          $candidateCountryID = $e->getCountry()->getCountryID();
          $addEntry = false;
          $entryCnt = count($entries);
          for ($i=0; $i<$entryCnt; $i++) {
            if ($candidateCountryID == $entries[$i]->getCountry()->getCountryID()) break;
            if ($i+1 == $entryCnt) $addEntry = true;
          }
          if ($addEntry) $entries[] = $e;
        }

        $this->uneditableEntries = $entries;

        foreach ($entriesFromCountriesTraveledTo as $e) {
          $candidateCountryID = $e->getCountry()->getCountryID();
          $addEntry = false;
          $entryCnt = count($entries);
          for ($i=0; $i<$entryCnt; $i++) {
            if ($candidateCountryID == $entries[$i]->getCountry()->getCountryID()) break;
            if ($i+1 == $entryCnt) $addEntry = true;
          }
          if ($addEntry) $entries[] = $e;
        }
      } else if ($entriesFromMyTravelPlans) {
        $entries = $entriesFromMyTravelPlans;
        $this->uneditableEntries = $entries;

        if ($entriesFromCountriesTraveledTo) {
          foreach ($entriesFromCountriesTraveledTo as $e) {
            $candidateCountryID = $e->getCountry()->getCountryID();
            $addEntry = false;
            $entryCnt = count($entries);
            for ($i=0; $i<$entryCnt; $i++) {
              if ($candidateCountryID == $entries[$i]->getCountry()->getCountryID()) break;
              if ($i+1 == $entryCnt) $addEntry = true;
            }
            if ($addEntry) $entries[] = $e;
          }
        }
      } else if ($entriesFromCountriesTraveledTo) {
        $entries = $entriesFromCountriesTraveledTo;
      }
    } else {
      $entries = MyTravelMapDAO::getTravelMapEntries($this->userCredentials['userID']);
    }

    usort($entries, array('MyTravelMapRepository', 'cmp'));
    return $entries;
  }

 // new added by bert
 static public function getTravelMapEntriesByuserCredentials($userCredentials = array()) {
   $entries = array();
   $uneditableEntries = array();

   if ($userCredentials['facebookID']==0) return $entries;

   if ($userCredentials['travelerID']) {
     //entries from GaNET journals
     $entries = MyTravelMapDAO::getTravelMapEntriesSynced($userCredentials['travelerID']);
     $entriesFromMyTravelPlans = MyTravelMapDAO::getTravelMapEntriesFromMyTravelPlans($userCredentials['userID']);
     $entriesFromCountriesTraveledTo = MyTravelMapDAO::getTravelMapEntriesFromCountriesTraveledTo($userCredentials['travelerID']);

     if ($entries) {
       foreach ($entriesFromMyTravelPlans as $e) {
         $candidateCountryID = $e->getCountry()->getCountryID();
         $addEntry = false;
         $entryCnt = count($entries);
         for ($i=0; $i<$entryCnt; $i++) {
           if ($candidateCountryID == $entries[$i]->getCountry()->getCountryID()) break;
           if ($i+1 == $entryCnt) $addEntry = true;
         }
         if ($addEntry) $entries[] = $e;
       }

       $uneditableEntries = $entries;

       foreach ($entriesFromCountriesTraveledTo as $e) {
         $candidateCountryID = $e->getCountry()->getCountryID();
         $addEntry = false;
         $entryCnt = count($entries);
         for ($i=0; $i<$entryCnt; $i++) {
           if ($candidateCountryID == $entries[$i]->getCountry()->getCountryID()) break;
           if ($i+1 == $entryCnt) $addEntry = true;
         }
         if ($addEntry) $entries[] = $e;
       }
     } else if ($entriesFromMyTravelPlans) {
       $entries = $entriesFromMyTravelPlans;
       $uneditableEntries = $entries;

       if ($entriesFromCountriesTraveledTo) {
         foreach ($entriesFromCountriesTraveledTo as $e) {
           $candidateCountryID = $e->getCountry()->getCountryID();
           $addEntry = false;
           $entryCnt = count($entries);
           for ($i=0; $i<$entryCnt; $i++) {
             if ($candidateCountryID == $entries[$i]->getCountry()->getCountryID()) break;
             if ($i+1 == $entryCnt) $addEntry = true;
           }
           if ($addEntry) $entries[] = $e;
         }
       }
     } else if ($entriesFromCountriesTraveledTo) {
       $entries = $entriesFromCountriesTraveledTo;
     }
   } else {
     $entries = MyTravelMapDAO::getTravelMapEntries($userCredentials['userID']);
   }

   uasort($entries, array('MyTravelMapRepository', 'cmp'));

   return $entries;
 }


  static function cmp($mapEntry1, $mapEntry2) {
  	return strcmp($mapEntry1->getCountry()->getName(), $mapEntry2->getCountry()->getName());
  }


  public function updateTravelWish($oldUserID,$newUserID){
	FacebookDAO::updateTravelWish($oldUserID,$newUserID);
  }
    
  
  public static function getGanetUser($facebookID) {
  	return MyTravelMapDAO::getGanetUser($facebookID);
  }                                                  
  public static function getCountryNameByID($countryID) {
  	return MyTravelMapDAO::getCountryNameByID($countryID);
  }                                                  

  static function getFacebookIDByTravelerID($travelerID){
  	return MyTravelMapDAO::getFacebookIDByTravelerID($travelerID);	
  }
  
  /**********************************************************************************************/
  /************************************     NEW METHODS      ************************************/
  /**********************************************************************************************/
  
  /**
   *  new method to add new/custom city
   *  @param $x longitude coordinate
   *  @param $y latitude coordinate
   *  @param $locID the locationID of country
   *  @param $name the name of the custom city
   *
   *  @return $locationID of the new city | FALSE
   */
  public function _addNewCity($x = null, $y = null, $locID = 0, $name = '', $countryID=0) {
  	if(is_null($x) || is_null($y) || !$locID || !$name)
  		return false;
  	
    require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocationPeer.php');
  	require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBNewCity.php');
  	require_once('travellog/model/Class.Coordinates.php');
  	
  	$new_city = new FBNewCity();
  	$new_city->setCountryID((!$countryID) ? FBLocationPeer::convertLocationIDtoCountryID($locID) : $countryID);
  	$new_city->setName($name);
  	$new_city->save();
  	
  	$coordinate = new Coordinates($new_city->getLocationID());
  	$coordinate->setX($x);
  	$coordinate->setY($y);
  	$coordinate->save();
  	 
  	$this->clearCache();
  	return $new_city->getLocationID();
  }
  
  /**
   *  new method to add a travel map entry of a sync account
   *  @param $travelerID the ID of dotNet  traveler
   *  @param $locationIDs array of locationIDs
   */
  public function _addSyncedTravelMapEntry($travelerID = 0, $locationIDs = array(), $countryID = 0) {
  	$this->clearCache();
  	return MyTravelMapDAO::_addSyncedTravelMapEntry($travelerID, $locationIDs, $countryID);
  }
  
  /**
   *  new method to remove a travel map entry of a sync account
   *  @param $travelerID the ID of dotNet  traveler
   *  @param $locationIDs array of locationIDs
   */
  public function _removeSyncedTravelMapEntry($travelerID = 0, $locationIDs = array()) {
  	$this->clearCache();
  	return MyTravelMapDAO::_removeSyncedTravelMapEntry($travelerID, $locationIDs);
  }
  
  /**
   *  new method to add a travel map entry of an unsync account
   *  @param $facebookID
   *  @param $locationIDs array of locationIDs
   */
  public function _addUnSyncedTravelMapEntry($facebookID = 0, $locationIDs = array(), $countryID = 0) {
  	$this->clearCache();
  	return MyTravelMapDAO::_addUnSyncedTravelMapEntry($facebookID, $locationIDs, $countryID);
  }
  
  /**
   *  new method to remove a travel map entry of an unsync account
   *  @param $facebookID
   *  @param $locationIDs array of locationIDs
   */
  public function _removeUnSyncedTravelMapEntry($facebookID = 0, $locationIDs = array()) {
  	$this->clearCache();
  	return MyTravelMapDAO::_removeUnSyncedTravelMapEntry($facebookID, $locationIDs);
  }
  
  /**
   *  new method to get country list
   *  @return array FBCountryLocation Objects instead of FBCountry Objects
   */
  public function _getCountryList() {
  	return MyTravelMapDAO::_getCountryList(true);
  }
  
  /**
   *  new method to update locationID field instead of countryID
   *  @param should be locationIDs instead of countryIDs
   */
  public function _updateEntries($locationIDs) {

    if ($this->userCredentials['facebookID']==0) return;

	//TODO finalized code
	// edited by adelbert
    if ($travelerID = $this->userCredentials['travelerID']) {
		require_once('gaLogs/Class.GaLogger.php');
      	require_once 'travellog/model/socialApps/mytravelmap/Class.TravelMapPeer.php';
      	require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocationPeer.php');
		GaLogger::create()->start($travelerID,'TRAVELER');
		
		// TODO: while tblTravelertoCountriesTravelled.countryID isn't converted yet to locationID, 
		//       filter locationIDs first by countryIDs, else no need, just pass the $locationIDs to 
		//       TravelMapPeer::_setCountriesTraveled() instead - DONE
		//$countryIDs = FBLocationPeer::convertLocationIDsToCountryIDs($locationIDs);
		
      	//TravelMapPeer::setCountriesTraveled($travelerID, $countryIDs);
      	
      	// CAUTION! This deletes the old data including countryID and adds only to locationID field
		TravelMapPeer::_setLocationsTraveled($travelerID, $locationIDs);
        //MyTravelMapDAO::updateEntriesSynced($countryIDs, $this->userCredentials['travelerID']);
    } else {
      MyTravelMapDAO::_updateEntries($locationIDs, $this->userCredentials['userID']);
    }

    $this->clearCache();
  }
  
  /**
   *  new method that returns array of MyTravelMapEntry Objects
   *  uses FBCountryLocation instead of FBCountry for countries returned
   *  TODO: too long method, needs to be refactored!
   */
  public function _getTravelMapEntries() {
    $found = false;
	if ($this->getCache() != null) {
	    $entries = $this->cache->get('TravelMapEntriesofUser_' . $this->userCredentials['userID']);
		if ($entries) {
			return $entries;
		}
	}
	if (!$found) {

	  if($this->userCredentials['travelerID'])
   		  $entries = MyTravelMapDAO::__retrieveSyncMapEntries($this->userCredentials['userID'], $this->userCredentials['travelerID']);
	  else $entries = MyTravelMapDAO::__retrieveUnsyncMapEntries($this->userCredentials['userID']); 

      if(!empty($entries)) {
      	  uasort($entries, array('MyTravelMapRepository', 'cmp'));
          $this->cache->set('TravelMapEntriesofUser_' . $this->userCredentials['userID'], $entries, 86400 );
      }
      return $entries;
    }

  }
  
  public function _getJournalEntryInLocation($location) {
  	$this->jEntryArr = MyTravelMapDAO::_getJournalEntryInLocation($location);
  }
  
  /**
   *  new method to retrieve count of journal entries in this location
   *  @param $locationID
   *
   *  @return int
   */
  
  public function _getCountJournalEntriesInLocation($locationID) {
  	if(!$this->jEntryArr)
  		$this->_getJournalEntryInLocation($location);
  	return $this->jEntryArr['count'];
  }
  
  /**
   *  new method to retrieve latest journal entry in this location
   *  @param $locationID
   *
   *  @return TravelLog Object
   */
  public function _getLatestJournalEntryInLocation($locationID) {
  	if(!$this->jEntryArr)
  		$this->_getJournalEntryInLocation($location);
  	return $this->jEntryArr['object'];
  }	
  
  /**
   *  new method to retrieve count of journal entries tips in the given location
   *  @param $locationID
   *
   *  @return int
   */
  public function _getCountJournalEntriesTipsInLocation($location) {
  	return MyTravelMapDAO::_getCountJournalEntriesTipsInLocation($location);
  }
  
  /**
   *  new method to retrieve count of journal entries photos in the given location
   *  @param $locationID
   *
   *  @return int
   */
  public function _getCountJournalEntriesPhotosInLocation($location) {
  	return MyTravelMapDAO::_getCountJournalEntriesPhotosInLocation($location);
  }
  
  /**
   *  new method to retrieve a map entry
   *  @param $id userID|travelerID
   *  @param $locationID
   *  @param $isSync
   *
   *  @return int
   */
  public function _retrieveTravelMapEntry($countryID) {
	$travelerID = $this->userCredentials['travelerID'];
	$userID = $this->userCredentials['userID'];
	
  	return MyTravelMapDAO::__retrieveTravelMapEntry($countryID, $userID, $travelerID);
  }
  
}
?>