<?php
abstract class AbstractFBLocation {

	protected $mLocationID;
	protected $mLocationName;
	protected $mLocationType;
	protected $mLatitude;
	protected $mLongitude;
	protected $mLocation;
	protected $mJournalEntriesCount;
	protected $mTipsCount;
	protected $mPhotosCount;
	protected $mPhotos;
	protected $mTravellogID;
	protected $mIsEditable = true;
	
	public function setID($v) {
		if ($this->mLocationID != $v) {
			$this->mLocationID = $v;
		}
		return $this;
	}
	
	public function getID() {
		return $this->mLocationID;
	}
	
	public function setLocationID($v) {
		if ($this->mLocationID != $v) {
			$this->mLocationID = $v;
		}
		return $this;
	}
	
	public function getLocationID() {
		return $this->mLocationID;
	}
	
	public function setName($v) {
		if ($this->mLocationName != $v) {
			$this->mLocationName = $v;
		}
		return $this;
	}
	
	public function getName() {
		return $this->mLocationName;
	}
	
	public function setLocationType($v) {
		if ($this->mLocationType != $v) {
			$this->mLocationType = $v;
		}
		return $this;
	}
	
	public function getLocationType() {
		return $this->mLocationType;
	}
	
	public function setLatitude($v) {
		if ($this->mLatitude != $v) {
			$this->mLatitude = $v;
		}
		return $this;
	}
	
	public function getLatitude() {
		return $this->mLatitude;
	}
	
	public function setLongitude($v) {
		if ($this->mLongitude != $v) {
			$this->mLongitude = $v;
		}
		return $this;
	}
	
	public function getLongitude() {
		return $this->mLongitude;
	}
	
	public function setLocation($v) {
		if ($this->mLocation != $v) {
			$this->mLocation = $v;
		}
		return $this;
	}
	
	public function getLocation() {
		return $this->mLocation;
	}
	
	public function setJournalEntriesCount($v) {
	    if ($this->mJournalEntriesCount != $v) {
			$this->mJournalEntriesCount = $v;
		}
		return $this;
	}
	
	public function getJournalEntriesCount() {
	    return $this->mJournalEntriesCount;
	}
	
	public function setTipsCount($v) {
	    if ($this->mTipsCount != $v) {
			$this->mTipsCount = $v;
		}
		return $this;
	} 
	
	public function getTipsCount() {
	    return $this->mTipsCount;
	}
	
	public function setPhotosCount() {
	    if ($this->mPhotosCount != $v) {
			$this->mPhotosCount = $v;
		}
		return $this;
	}
	
	public function getPhotosCount() {
	    return $this->mPhotosCount;
	}
	
	public function setPhotos($v) {
	    if ($this->mPhotos != $v) {
			$this->mPhotos = $v;
		}
		return $this;
	}
	
	public function getPhotos() {
	    return $this->mPhotos;
	}
	
	public function setTravelLogID($v) {
	    if ($this->mTravellogID != $v) {
			$this->mTravellogID = $v;
		}
		return $this;
	}
	
	public function getTravelLogID() {
	    return $this->mTravellogID;
	}
	
	public function getIsEditable() {
	    return $this->mIsEditable;
	 }
  
	public function setIsEditable($mIsEditable = true) {
    	$this->mIsEditable = $mIsEditable;
  	}
	
	public function initialize($v) {
		if(is_array($v)) {
			$this->mLocationID = array_key_exists('locationID',$v) ? $v['locationID'] : $this->mLocationID;
			$this->mLatitude   = array_key_exists('lat',$v) ? $v['lat'] : $this->mLatitude;
			$this->mLongitude  = array_key_exists('long',$v) ? $v['long'] : $this->mLongitude;
		}
		return $this;
	}
	
}