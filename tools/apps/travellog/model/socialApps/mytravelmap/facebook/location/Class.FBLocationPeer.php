<?php

require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
require_once('Cache/ganetCacheProvider.php');
require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocation.php');
require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCountryLocation.php');
require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCityLocation.php');

class FBLocationPeer {
	
	public static $cache;
	
	static public function getCache() {
	    if(self::$cache == null)
	        self::$cache	=	ganetCacheProvider::instance()->getCache();
	    return self::$cache;
	}
		
	private static function getConnection($dbName = null){
		$conn = new dbHandler();
		if(!empty($dbName))		
			$conn->setDB($dbName);

		return $conn;
	}
	
	static public function convertCountryIDtoLocationID($countryID) {
		$conn = self::getConnection();
		$sql = "SELECT locID
	            FROM GoAbroad_Main.tbcountry
	            WHERE countryID =" . $countryID;

		$resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			return $record['locID'];
		}
		return 0;
	}
	
	static public function convertCountryIDsToLocationIDs($countryIDs) {
		$locations = array();
		$conn = self::getConnection();
		$sql = "SELECT locID
	            FROM GoAbroad_Main.tbcountry
	            WHERE countryID in (" . implode(",", $countryIDs) . ")";

		$resource = $conn->execQuery($sql);
		while($record = mysql_fetch_assoc($resource)) {
			$locations[] = $record['locID'];
		}
		return $locations;  
	}
	
	static public function convertLocationIDtoCountryID($locationID) {
		$conn = self::getConnection();
		$sql = "SELECT countryID
	            FROM GoAbroad_Main.tbcountry
	            WHERE locID =" . $locationID;

		$resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			return $record['countryID'];
		}
		return 0;
	}
	
	static public function convertLocationIDsToCountryIDs($locationIDs) {
		$countries = array();
		$conn = self::getConnection();
		$sql = "SELECT countryID
	            FROM GoAbroad_Main.tbcountry
	            WHERE locID in (" . implode(",", $locationIDs) . ")";

		$resource = $conn->execQuery($sql);
		while($record = mysql_fetch_assoc($resource)) {
			$countries[] = $record['countryID'];
		}
		return $countries;  
	}
	
	static public function getCountryNameFromLocationID($locationID) {
		$conn = self::getConnection();
		$sql = "SELECT country
	            FROM GoAbroad_Main.tbcountry
	            WHERE locID =" . $locationID;

		$resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			return $record['country'];
		}
		return null;
	}
	
	static public function getCountryLocationList($enabledKey = false) {
		require_once('travellog/model/Class.LocationType.php');
    	$countries = array();
		$conn = self::getConnection();
    	$sql = 'SELECT a.countryID, a.country, a.locID, b.lat, b.long 
    	        FROM GoAbroad_Main.tbcountry as a 
    	        LEFT JOIN Travel_Logs.tblCoordinate AS b ON a.locID=b.locID
				WHERE a.approved=1 AND a.countryID NOT IN (14,219,240,104) 
				ORDER BY a.country';

		$recordsets = new iRecordset($conn->execQuery($sql));
		
		foreach ($recordsets as $rs) {
    		$country = new FBCountryLocation;
    		$country->setID($rs['locID']);
    		$country->setCountryID($rs['countryID']);
    		$country->setName($rs['country']);
    		$country->setLocationType(LocationType::$COUNTRY);
    		$country->setLatitude($rs['lat']);
    		$country->setLongitude($rs['long']);
    		if($enabledKey)
    			$countries[$rs['locID']] = $country;
    		else
    			$countries[] = $country;
		}
    	return $countries;
  	}
  	
  	/**
  	 *  retrieves the city location list of a country
  	 *  @param $options array
  	 *      $options:
  	 *          type => 'locationID' : 'countryID'; default = locationID
  	 *          id   => id : 0; default = 0
  	 *  How to use:
  	 *      $options = array(
  	 *                     'type' => 'countryID',
  	 *                     'id'   => 91
  	 *                 );
  	 *      $us_cities = FBLocationPeer::getCityLocationList($options); //returns an array of FBCityLocation Objects of U.S.A.
  	 *      foreach($us_cities as $us_city) {
  	 *          $loc_id    = $us_city->getID();
  	 *          $city_id   = $us_city->getCityID();
  	 *          $city_name = $us_city->getName();
  	 *          ...
  	 *      }
  	 *
  	 *  @return array FBCityLocation Objects
  	 */
  	static public function getCityLocationList($options = array()) {
		require_once('travellog/model/Class.LocationType.php');
		
		$location_type = array_key_exists('type', $options) ? $options['type'] : 'locationID';
		$id = array_key_exists('id', $options) ? $options['id'] : 0;
		
    	$cities = array();
		$conn = self::getConnection();
		
		if('locationID' == $location_type) {
			$sql = "SELECT a.cityID, a.countryID, a.city, a.countryregionID, a.locID, c.lat, c.long 
			        FROM GoAbroad_Main.tbcity as a 
			        LEFT JOIN GoAbroad_Main.tbcountry as b ON a.countryID = b.countryID 
			        LEFT JOIN Travel_Logs.tblCoordinate AS c ON a.locID=c.locID
			        WHERE a.approved = 1 
			        AND b.locID =" . $id;
		}
		else {
			$sql = "SELECT a.cityID, a.countryID, a.city, a.countryregionID, a.locID , b.lat, b.long
			        FROM GoAbroad_Main.tbcity as a
			        LEFT JOIN Travel_Logs.tblCoordinate AS b ON a.locID=b.locID
			        WHERE a.countryID =" . $id;
		}
		
		$recordsets = new iRecordset($conn->execQuery($sql));
		
		foreach ($recordsets as $rs) {
    		$city = new FBCityLocation;
    		$city->setID($rs['locID']);
    		$city->setCityID($rs['cityID']);
    		$city->setName($rs['city']);
    		$city->setCountryRegionID($rs['countryregionID']);
    		$city->setCountryID($rs['countryID']);
    		$city->setLocationType(LocationType::$CITY);
    		$city->setLatitude($rs['lat']);
    		$city->setLongitude($rs['long']);
    		$cities[] = $city;
		}
    	return $cities;
  	}
  	
  	static public function getCountryLocationByLocationID($locationID) {
  	    
  	    $found = false;
		self::getCache();
		if (self::$cache != null) {
			$country = self::$cache->get('FBCountryLocation_' . $locationID);
			if ($country) {
				$found = true;
				return $country;
			}
		}
		
		if (!$found){
  	        $conn = self::getConnection("GoAbroad_Main");
  	        
  	        $sql = "SELECT a.countryID, a.locID, b.lat, b.long    
		        FROM GoAbroad_Main.tbcountry as a 
		        LEFT JOIN Travel_Logs.tblCoordinate AS b ON a.locID=b.locID
		        WHERE a.locID = ".$locationID;
		
		    $resource = $conn->execQuery($sql);
    	    if($record = mysql_fetch_assoc($resource)) {
    		    $country = new FBCountryLocation();
    		    $country->setID($record['locID']);
    		    $country->setCountryID($record['countryID']);
    		    $country->setLatitude($record['lat']);
    		    $country->setLongitude($record['long']);
    		    if (self::$cache != null) {
    			    self::$cache->set('FBCountryLocation_' . $locationID, $country, 86400 );
    			}
    		    return $country;
    	    }
	    }
        return null;
  	}
  	
  	static public function getCities($countryID) {
  	    $cities = array();
		$found = false;
		self::getCache();
		if (self::$cache != null) {
			$list = self::$cache->get('CitiesOfCountryID_' . $countryID);
			if ($list) {
				$found = true;
				return $list;
			}
		}
		
		if (!$found){
    		if(91 == $countryID) {
    		    $cities = self::getCitiesOfUsa();
    		}
    		else{
    		    $conn = self::getConnection();

        		$sql = "SELECT cityID, locID, regionID, countryID, continentID, city   
        		        FROM GoAbroad_Main.tbcity
        		        WHERE countryID = ".$countryID." 
        		        AND approved = 1 AND locID <> 0 ORDER BY city";

        		$recordsets = new iRecordset($conn->execQuery($sql));

            	foreach ($recordsets as $rs) {			
            	        $city = new FBCityLocation();
            	        $city->setLocationID($rs['locID']);
            	        $city->setCityID($rs['cityID']);
            	        $city->setRegionID($rs['regionID']);
            	        $city->setCountryID($rs['countryID']);
            	        $city->setContinentID($rs['continentID']);
            	        $city->setName(stripslashes($rs['city']));
            	        $cities[$rs['locID']] = $city;
            	}
    		}    		
        
        	if (self::$cache != null) {
			    self::$cache->add('CitiesOfCountryID_' . $countryID, $cities, 86400 );
			}
		}
  	    
    	return $cities;
  	}
  	
  	static public function getCitiesOfUsa() {
        $cities = array();
  	    $conn = self::getConnection('GoAbroad_Main');
		$sql = "SELECT s.state, c.cityID, c.locID, c.regionID, c.countryID, c.continentID, c.city   
	            FROM tbstate as s 
	            INNER JOIN tbcountryregion as cg ON s.state = cg.countryregion 
	            INNER JOIN tbcity as c ON c.countryregionID = cg.countryregionID
	            WHERE c.countryID = 91 
	            AND c.approved = 1 AND c.locID <> 0 ORDER BY c.city";
	            
		$recordsets = new iRecordset($conn->execQuery($sql));

        foreach ($recordsets as $rs) {			
            $city = new FBCityLocation();
            $city->setLocationID($rs['locID']);
            $city->setCityID($rs['cityID']);
            $city->setRegionID($rs['regionID']);
            $city->setCountryID($rs['countryID']);
            $city->setContinentID($rs['continentID']);
            $city->setName(stripslashes($rs['city']));
            $city->setState(stripslashes($rs['state']));
            
            $cities[$rs['locID']] = $city;
        }    
        return $cities;
  	}
  	
  	static public function getStateLocationFromCityLocationID($locationID) {
  		$conn = self::getConnection('GoAbroad_Main');
		$sql = "SELECT s.locID, s.state
	            FROM tbstate as s 
	            INNER JOIN tbcountryregion as cg ON s.state = cg.countryregion 
	            INNER JOIN tbcity as c ON c.countryregionID = cg.countryregionID
	            WHERE c.locID =" . $locationID;
	            
		$resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			$city = new FBCityLocation;
			$city->setID($record['locID']);
			$city->setName($record['state']);
			return $city;
		}
		return null;
  	}
	
}

?>
