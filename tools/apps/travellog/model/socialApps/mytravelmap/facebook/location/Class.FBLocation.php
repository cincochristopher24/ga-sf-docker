<?php

require_once('travellog/model/socialApps/mytravelmap/facebook/location/abstract/Class.AbstractFBLocation.php');

class FBLocation extends AbstractFBLocation {

	function getLocationType() {
    	if(empty($this->mLocationType)) {
    		require_once('travellog/model/Class.LocationType.php');
    		
    		switch(get_class($this->getLocation())){
    			case 'Continent':
    				$this->mLocationType = LocationType::$CONTINENT;
    				break;
    			case 'Country':
    				$this->mLocationType = LocationType::$COUNTRY;
    				break;
    			case 'Region':
    				$this->mLocationType = LocationType::$REGION;
    				break;
    			case 'City':
    				$this->mLocationType = LocationType::$CITY;
    				break;
    			case 'NewCity':
    				$this->mLocationType = LocationType::$NEWCITY;
    				break;
    			case 'NonPopulation':
    				$this->mLocationType = LocationType::$NON_POP;
    				break;
    			case 'State':
    				$this->mLocationType = LocationType::$STATE;
    				break;
    		}
    	}
    	return $this->mLocationType;
  	}
  
  	function getLocation(){
    	if(empty($this->mLocation)) {
    		require_once('travellog/model/Class.LocationFactory.php');
    		
    		$this->mLocation = LocationFactory::instance()->create($this->mLocationID);
    	}
    	return $this->mLocation;
  	}
}

?>
