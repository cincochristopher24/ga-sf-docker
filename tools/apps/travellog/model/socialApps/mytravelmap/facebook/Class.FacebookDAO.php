<?php

//require_once 'Class.SingletonConnection.php';
require_once 'Class.dbHandler.php';
require_once 'Class.iRecordset.php';
/**
 * Common database operations for Facebook applications. Can be included or
 * extended by other DAOs.
 */
class FacebookDAO {

	// Modified by Adelbert
	// Date: Oct. 13, 2009
	// Changed SingletonConnection db Connection to dbHandler
	// Purpose: To avoid query error

  /**
   * Gets the userID, facebookID, and travelerID of a specific user
   * If facebookID does not exist in table creates a new entry
   * @param unknown_type $fbID
   * @return unknown
   */

	public static function getUserCredentials($fbID=null) {
  		$userCreds = array();
		$conn = self::getConnection();
  		$sql = "SELECT userID, facebookID, travelerID FROM tbUser WHERE facebookID=$fbID LIMIT 1";
		$rs = new iRecordset($conn->execQuery($sql));
		
		if($rs->retrieveRecordCount()) {
      		$userCreds = array(
				'userID' 	 => $rs->getUserID(0), 
				'facebookID' => $rs->getFacebookID(0), 
				'travelerID' => $rs->getTravelerID(0), 
				'message'	 => 'ok');
		}		
		return $userCreds;
	}
	   
	// TEMPORARY FIX	
	public static function addAppUser($facebookID = null) {
		if(!$facebookID) return false;
		$conn = self::getConnection();
      	$sql = "INSERT INTO tbUser (facebookID, dateAdded) VALUES ($facebookID, now())";
		return $conn->execQuery($sql);
	}

	// TEMPORARY FIX
	public static function isAlreadyAddedInTableUser($facebookID=null) {
		if(!$facebookID) return true;
		$conn = self::getConnection();
  		$sql = "SELECT count(*) AS isUser FROM tbUser WHERE facebookID = $facebookID LIMIT 1";
		$rs = new iRecordset($conn->execQuery($sql));
		return (bool)$rs->getIsUser(0);
	}

	public static function getMTMUsersCredenials($userID, $limit=20) {
		$userCreds = array();
		$conn = self::getConnection();
  		$sql = "SELECT userID, facebookID, travelerID FROM tbUser WHERE facebookID != 0 AND userID > $userID 
				ORDER BY userID ASC LIMIT $limit";
				
		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
      		$userCreds[] = array(
				'userID' 	 => $rs['userID'], 
				'facebookID' => $rs['facebookID'],  
				'travelerID' => $rs['travelerID'], 
				'message'	 => 'ok');
		}
		return $userCreds;
	}

	public static function isMTMValidUserID($userID=null) {
		if(!$userID) return false;
		$conn = self::getConnection();
  		$sql = "SELECT count(userID) AS isValid FROM tbUser WHERE userID = $userID";
		$rs = new iRecordset($conn->execQuery($sql));
		return (bool)$rs->getIsValid(0);
	}

  	static function updateTravelWish($oldUserID,$newUserID){
		$conn = self::getConnection();
    	$sql = "UPDATE tbTravelWish SET userID=$newUserID WHERE userID=$oldUserID";
		return $conn->execQuery($sql);  
  	}

	private static function getConnection($dbName = 'SocialApplication'){
  		$conn = new dbHandler();  	
		$conn->setDB($dbName);
		return $conn;
	}

}

?>