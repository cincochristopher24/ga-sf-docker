<?php
interface fbIController {
  function setModelAndView(IModelAndView $modelAndView);
  static function sendResponse(fbIController $controller);
  function applyInputToModel();
}

abstract class AbstractController implements fbIController {
  protected $modelAndView;

  function setModelAndView(IModelAndView $modelAndView) {
    $this->modelAndView = $modelAndView;
  }

  static function sendResponse(fbIController $controller) {
    $controller->setModelAndView($controller->applyInputToModel());
    $controller->modelAndView->applyModelToView();
  }
}

interface IModelAndView {
  function setView($view);
  function setModel($model);
  function setModelValue($key, $value);
  function applyModelToView();
}

abstract class AbstractModelAndView implements IModelAndView {
  protected $view;
  protected $model;

  function setView($view) {
    $this->view = $view;
  }
  function setModel($model) {
    $this->model = $model;
  }
  function setModelValue($key, $value) {
    $this->model[$key] = $value;
  }
  protected function preserveSession() {
    $session = $GLOBALS['_SESSION'];
    unset($GLOBALS);
    $GLOBALS['_SESSION'] = $session;
  }
  function applyModelToView() {
    //$this->preserveSession();
    $model = $this->model;
	if($this->view) {
    	include($this->view);		
	}
  }
}

class ModelAndView extends AbstractModelAndView {
  function __construct($view=null) {
    $this->setView($view);
  }
}
?>