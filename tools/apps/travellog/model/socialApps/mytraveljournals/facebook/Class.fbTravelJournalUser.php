<?php
	require_once('travellog/model/FacebookPlatform/facebook.php');	
	require_once('travellog/model/socialApps/mytraveljournals/facebook/Class.fbInitialization.php');             
	require_once('travellog/model/socialApps/mytravelmap/gaOpenSocialTravelerMapper.php');          

	class fbTravelJournalUser extends gaOpenSocialTravelerMapper 
	{       
	protected $container = "facebook.com";
	protected $mfbUserID = null;
	public $facebook;
	public $isUpdateScript = null;
	public $userToUpdate = null;
	public $fbApi = null;

	public function __construct(){
		$this->facebook = new Facebook(fbInitialization::getApiKey(),fbInitialization::getApiSecret());
			$this->mfbUserID = $this->facebook->get_loggedin_user();
	}
	function getfbUserID(){
		return $this->mfbUserID;
	}

	//added by nash. . .
	public function isFbUser(){
		return $this->getFbUserID() != 0; 
	}

	public function getFbFriends(){
		//the user must be a facebook user
		if($this->isFbUser()){
			$this->fbApi = $this->createFbApi();				
			$fql = 	"SELECT uid, is_app_user ".
	       			"FROM user ".
	       			"WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = ". $this->getfbUserID() .")";
			return $this->fbApi->api_client->fql_query($fql);
		}
	}

	private function createFbApi(){
		if(is_null($this->fbApi)){
			$this->fbApi = new Facebook(fbInitialization::getApiKey(), fbInitialization::getApiSecret());
		}
		return $this->fbApi;
	}

	// DEPRECATED
	public function runUpdateScript($runScript)
	{
		$this->isUpdateScript = $runScript;
	}

	public function userIDtoUpdate($uid)
	{
		$this->userToUpdate = $uid;
	}

	public function getUserIDtoUpdate()
	{
		return $this->userToUpdate;
	}

	//added codes end here. . .
	} 
?>