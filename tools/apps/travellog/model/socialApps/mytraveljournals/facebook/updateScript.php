<?php

require_once ('Class.fbInitialization.php');
require_once ("travellog/model/Class.Travel.php");
require_once ("travellog/model/Class.Traveler.php");
require_once ("travellog/model/Class.TravelLog.php");
require_once ("travellog/model/Class.AdminGroup.php");  
require_once ('model/Class.fbTravelJournalUser.php');   

class UpdateScript
{
	protected $db = null;
 	protected $dbGANetwork;
 	public $fbUser = null;
 	public $travellerID;
 	public $numberOfJournalDisplayed;
 	
 	public $Traveller;
 	public $TravelID;
 	public $TravelLoglID;
 	
	public function UpdateScript($uid)
	{
		$this->db = fbInitialization::dbConnect();
		$this->dbGANetwork = fbInitialization::dbConnectToGANetwork();

		$this->fbUser = $uid;
		$qry = "SELECT * from tblTravelJournalUser where fbuserID=".$this->fbUser;
		$rs = new iRecordset($this->db->execQuery($qry));
		foreach($rs as $row)
		{
			$this->travellerID = $row['travelerID'];
			$this->numberOfJournalDisplayed = $row['numjournaldisplayed'];
		}
	//	return $this->travellerID;
	}

	public function isSync()
	{
	//	return $this->travellerID;
		if(0!=$this->travellerID)  return true;
		else return 0;
	}
	
	public function getTravellerID()
	{
		return $this->travellerID;
	}
	
	public function getNumberJournalsDisplayed()
	{
		return $this->numberOfJournalDisplayed;
	}
	


	
}
?>