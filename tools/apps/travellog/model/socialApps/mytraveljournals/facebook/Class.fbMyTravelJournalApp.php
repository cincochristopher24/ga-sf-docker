<?php	
	require_once('travellog/model/socialApps/mytraveljournals/facebook/Class.fbInitialization.php');
	require_once('travellog/model/socialApps/mytraveljournals/Class.AbstractMyTravelJournalApp.php');
	require_once('travellog/model/FacebookPlatform/facebook.php');	

	class fbmyTravelJournalApp extends AbstractMyTravelJournalApp {
		
		private $facebook;
		
		function __construct() {
			$this->facebook = new Facebook(fbInitialization::getApiKey(),fbInitialization::getApiSecret());
		}
		
		function getFacebook(){
			return $this->facebook;
		}		

		static function redirect($url){
			header( 'Location: ' . $url );
		}

//		static function 
	}
?>