<?php
	
	require_once 'utility/Class.dbHandler.php';
	require_once 'utility/Class.iRecordset.php';      
	
	class AbstractMyTravelJournalApp{
		
		private $user,
				$numTravelJournalsShown,
				$conn,
				$entryPhotoLimit = 2,
				$entryLimit = 3,
				$showJournalType = '0';
		
		function __construct(){
			$this->conn = new dbHandler();
			$this->conn->setDB('SocialApplication');
		}
		
		function setUser(WebServiceUser $user){
			$this->user = $user;
		}

		function setShowJournalType($showJournalType){
			$this->showJournalType = $showJournalType;
		}
		

		function setNumTravelJournalsShown($numTravelJournalsShown){
			$this->numTravelJournalsShown = $numTravelJournalsShown;
		}
		

		function getEntryPhotoLimit(){
			return $this->entryPhotoLimit;
		}
		
		function getEntryLimit(){
			$this->entryLimit;
		}
		
		function update(){
			// brute check for valid user
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			if($this->user->getID()){
				$sql = 	"UPDATE tblTravelJournalUsers " . 
						"SET noOfJournalsShown = {$this->numTravelJournalsShown} , showJournalType = '{$this->showJournalType}'" .
						"WHERE userID = {$this->user->getID()}";
				$conn->execQuery($sql);	
			}
			else
				return false;
		}
		
		/*
		 *  this function is used to add a new user
		 */
		// DEPRECATED
		// note by adelbert
		static function addUser($userID){
			if(!self::isExistsInTblTravelJournalsUsers($userID)){
				$conn = new dbHandler();		
				$conn->setDB('SocialApplication');
				$sql = 	"INSERT into tblTravelJournalUsers SET userID = {$userID}, dateadded = now(), noOfJournalsShown = 0";        
	 			$conn->execQuery($sql);				
			}
// 			return $conn->getLastInsertedID();
		}

		static function isExistsInTblTravelJournalsUsers($userID){
			$conn = new dbHandler();		
			$conn->setDB('SocialApplication');
			$sql = 	"SELECT count(*) AS isExists FROM tblTravelJournalUsers WHERE userID=".$userID;
 			$rs = new iRecordset($conn->execQuery($sql));
 			return $rs->getIsExists();
		}
		
		// verify if user has already added MTJ app
		static function hasMyTravelJournalApp($wsID = 0,$domain){
			if($wsID == 0) {return false;}
			else{
				$conn = new dbHandler();
				$conn->setDB('SocialApplication');
				$field = wsDbHelper::getUserField($domain);
				$sql = 	"SELECT COUNT(*) as `hasApp` " .
						"FROM `tbUser` " .
						"WHERE {$field} = {$wsID}";
	 			$rs = new iRecordset($conn->execQuery($sql));

	 			return $rs->getHasApp();				
			}
		}
		// note by adelbert
		// deprecated
		static function isGanetAccountAlreadySync($travelerID){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			//	$sql = "SELECT `userID` FROM `tblTravelJournalUsers` WHERE `userID`={$userID} LIMIT 1";
			$sql = "SELECT count(*) AS isSync FROM tbUser WHERE travelerID={$travelerID} AND (SELECT count(*) FROM tblTravelJournalUsers WHERE tbUser.userID = userID) > 0 LIMIT 1";
			$rs = new iRecordSet($conn->execQuery($sql));
			return $rs->getIsSync();
		}

		static function isSynchronized($userID){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			//	$sql = "SELECT `userID` FROM `tblTravelJournalUsers` WHERE `userID`={$userID} LIMIT 1";
			$sql = "SELECT userID FROM tbUser WHERE userID={$userID} AND (SELECT count(*) FROM tblTravelJournalUsers WHERE userID={$userID}) > 0";
			$rs = new iRecordSet($conn->execQuery($sql));
			return $rs->retrieveRecordCount() ? $rs->getUserID() : false;
		}

		static function updateGanetUser($oldID,$newID,$appID){
			self::updateUserID($oldID,$newID);
			self::removeShowJournalType($oldID);
			self::deleteSelectedJournals($oldID,$appID);
			self::updateShowJournalType(array( 'userID' => $newID, 'appID' => $appID, 'showJournalType' => '0'));
 		}

/*		static function updateShowJournalType($userID,$showJournalType){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "UPDATE tblTravelJournalUsers SET showJournalType = '{$showJournalType}' WHERE userID =".$userID;
 			$conn->execQuery($sql);                                                                                      
 		}
*/
		static function updateShowJournalType($params = array()){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "INSERT INTO tblShowJournalType SET userID= {$params['userID']}, appID={$params['appID']}, showJournalType='{$params['showJournalType']}' ON DUPLICATE KEY UPDATE showJournalType='{$params['showJournalType']}'";
 			$conn->execQuery($sql);                                                                                      
 		}

		static function removeShowJournalType($userID){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "DELETE FROM tblShowJournalType WHERE userID = {$userID}";
 			$conn->execQuery($sql);                                                                                      
 		}
		// note by adelbert
		// deprecated
		static function isAlreadyExist($userID){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = 	"SELECT userID FROM tblTravelJournalUsers WHERE userID =".$userID;			
 			$rs = new iRecordset($conn->execQuery($sql));
			return $rs->retrieveRecordCount() ? true : false;
 		}
		
		// function called to remove user data when user will remove app from his/her O.S. profile
		static function removeJournalAppData($userID){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = 	"DELETE FROM `tblTravelJournalUsers` WHERE `userID` = {$userID}";
 			$conn->execQuery($sql);
 		}
		
		// transfered from old model
		
		function setSelectedJournals($arrTravelID) { // previous state STATIC removed by nash
			self::deleteSelectedJournals($this->user->getID(),$this->user->getWsID());
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			for($i=0; $i<count($arrTravelID); $i++){
				$qry = 	"INSERT into tblDisplayedJournals " .
						"SET userID = ". $this->user->getID() .", appID = ".$this->user->getWsID().", travelID= ".$arrTravelID[$i];
				$conn->execQuery($qry);
			}
		}
		
		static function deleteSelectedJournals($userID, $appID) {
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$qry = "DELETE FROM tblDisplayedJournals WHERE userID=" .$userID. " AND appID=".$appID;
			$conn->execQuery($qry);
		}
				
		// end - transfered from old model
	} 
?>
