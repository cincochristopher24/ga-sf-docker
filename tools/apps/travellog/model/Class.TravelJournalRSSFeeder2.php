<?php
/*
 * Created on 10 10, 07
 * 
 * @author Kerwin Gordo
 * Purpose: creates rss feed for a traveler (This is a modification of TravelJournalRSSFeeder)
 * 
 */
  
 require_once('Class.Connection.php');
 require_once('Class.Criteria2.php');
 require_once('travellog/model/Class.TravelLog.php');

 class TravelJournalRSSFeeder2 {	 	
 	/**
 	 * function to output rss content for a Journal. This will be called if a new travellog is added or
 	 * 		updated. This function will just recreate all the journal entries for the journal specified and
 	 * 		overwrites the old file.
 	 *  @param Travel journal
 	 */
 	
 	
 	
 	function publish($traveler) {
		$travels = $traveler->getTravels();
		$travellogs = array();
		foreach($travels AS $travel){
			$jEntries =$travel->getTravelerTravelLogs();
			foreach($jEntries AS $jEntry){
				$travellogs[] = $jEntry;
			}
			unset($jEntry);
		}
		unset($travel);
		
 		/*$c = new Criteria2;
		$c->mustBeEqual('logtype', 1);
		$c->setOrderBy('travellogID  DESC');
 		$journalLogs = array();
 		foreach($travels as $travel){			 				
 			$journalLogs = array_merge($journalLogs,$travel->getJournalLogs($c)); 			
 		}
 		
 		
 		
 		$c = new Criteria2;
		$c->mustBeEqual('logtype', 2);
		$c->setOrderBy('travellogID  DESC');
 		
 		foreach($travels as $travel){			
 			$journalLogs = array_merge($journalLogs,$travel->getJournalLogs($c));
 		}
 		
 		// recreate the array to combine entries w/ same travellogid (to make unique)
 		$nJournalLogs = array();
 		foreach ($journalLogs as $journalLog) {
 			if (!array_key_exists('k',$nJournalLogs))
 				$nJournalLogs['k' . $journalLog->getTravellogID()] = $journalLog;
 		}
 		
 		
 		//echo '<pre>';
 		//print_r($nJournalLogs);
 		//echo '</pre>';
 		//exit;
 		
 		// instantiate the travellogs
 		$travellogs = array();						// the array to hold all the logs for this journal
 		
 		foreach($journalLogs as $jLog){
 			$travellogs[] = new Travellog($jLog->getTravellogID());
 		}
 		
 		//echo '<pre>';
 		//print_r($travellogs);
 		//echo '</pre>'; 		
  		//if (0 == count($travellogs)) //edit by Jul, since this page will display a blank page if a traveler has not published any journal yet
  		//	return ;
  		*/

 		ob_start(); 		
 		header('Content-type: text/xml');
 		echo '<?xml version="1.0" encoding="utf-8"?>';
 		echo '<rss version="2.0">';
 		echo '<channel>';
		echo '<title>' . $traveler->getUserName() .'\'s' . 'Travel Journals' . '</title>';
 		echo '<link>http://' . $_SERVER['SERVER_NAME'] .  '/journal.php</link>';
 		echo '<description>' . '</description>';
 		echo '<lastBuildDate>' . date('D, j M Y H:i:s',time())  . ' PST</lastBuildDate>';
        echo '<language>en-us</language>';

		if( 0 < count($travellogs) ){

        	foreach($travellogs as $tlog){
	        	// count photos
	        	$photos = $tlog->getPhotos();
	        	$readMoreStr = 'Read More';
	        	if (0 < count($photos))
	        		$readMoreStr .= '/View ' . count($photos) . ' Photo';
        	
	        	if (1 < count($photos))	
	        	    $readMoreStr .= 's';
        	        	
	        	echo '<item>'; 
	        	echo '<title>' . $tlog->getTitle() . ' - [' .$tlog->getTravel()->getTitle() . ']</title>';
	        	echo '<link>http://' . $_SERVER['SERVER_NAME'] . '/travellog.php?action=view&amp;travellogID=' . $tlog->getTravelLogID()  . '</link>';
	        	echo '<pubDate>' .   date('D, j M Y H:i:s',strtotime($tlog->getLogDate()))  . ' PST' . '</pubDate>';
	        	$location = $tlog->getTrip()->getLocation();
	        	$locStr =  '<a href="http://' . $_SERVER['SERVER_NAME'] . '/travellog.php?action=view&travellogID=' . $tlog->getTravelLogID()  . '">' . $location->getName() . ', ' . $location->getCountry()->getName() . '</a>';        	
	        	$dateStr = date('D, j M Y',strtotime($tlog->getLogDate()));
	        	$moreStr = '<a href="http://' . $_SERVER['SERVER_NAME'] . '/travellog.php?action=view&travellogID=' . $tlog->getTravelLogID() . '">' . $readMoreStr . '</a>'; 
	        	echo '<description><![CDATA[' . '<p>' . $locStr . ', ' . $dateStr . '<br />' . htmlspecialchars(strip_tags(str_replace("&nbsp;","",$this->truncateParagraph($tlog->getDescription())."..."))) .  '</p><p>' . $moreStr . '</p>]]> </description>';
	        	echo '</item>';
	        }  
		
		}else{
			echo '<item>';
			echo '<description><![CDATA[<p><strong>'.$traveler->getUserName() .'</strong> has not published any Travel Journal yet.<br />Subscribe for RSS feeds and keep updated.</p>]]></description>';
			echo '</item>';
		}
 		 			
 		echo '</channel>';
 		echo '</rss>';
 		
 		 		
 		$output = ob_get_contents();
 		ob_end_clean();
 		 		
 		echo $output;
 		 		 		
 	}
 	
 	
 	function truncateParagraph($prgp){
 		$wordLimit = 60;
 		
 		$prgpAr = explode(" ",$prgp);
 		
 		if (60 >= count($prgpAr))
 			$newPrgp = $prgp;
 		else {
 			$prgpAr = array_slice($prgpAr,0,$wordLimit);
 			$newPrgp = implode(" ",$prgpAr);
 		}	
 		
 		return $newPrgp;
 	}
 	
 	function hasTravelupdate(Traveler $traveler){
 		
 		$travels = $traveler->getTravels();
 		 		 		
 		$c = new Criteria2;
		$c->mustBeEqual('logtype', 1);
		$c->setOrderBy('travellogID  DESC');
 		$journalLogs = array();
 		foreach($travels as $travel){			 				
 			$journalLogs = array_merge($journalLogs,$travel->getJournalLogs($c)); 			
 		}
 		
 		
 		
 		$c = new Criteria2;
		$c->mustBeEqual('logtype', 2);
		$c->setOrderBy('travellogID  DESC');
 		
 		foreach($travels as $travel){			
 			$journalLogs = array_merge($journalLogs,$travel->getJournalLogs($c));
 		}
 		
 		// recreate the array to combine entries w/ same travellogid (to make unique)
 		$nJournalLogs = array();
 		foreach ($journalLogs as $journalLog) {
 			if (!array_key_exists('k',$nJournalLogs))
 				$nJournalLogs['k' . $journalLog->getTravellogID()] = $journalLog;
 		}
 		
 		if(count($nJournalLogs))
 			return true;
 		else
 			return false;
 		
 		
 	}
 	
 
 	
 }   
?>