<?php
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');
	
	require_once('travellog/model/Class.TopicPeer.php');

	class PostedNewTopic implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			if( empty($groupIDs) ) 
				return array();
			$sql = 	'SELECT logView.*, CAST(logView._EXECUTION_DATE_ AS DATE ) as eDate ' .
					'FROM `DiscussionBoard`.`tblTopics` as logView ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'INSERT\' ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
						'AND logView.`_NEW_groupID` IN (\'' . join('\', \'', $groupIDs) . '\') ' .
						'AND logView.`_DOER_ID_` <> ' . GaString::makeSqlSafe($this->parentGroup->getAdministratorID()) . ' ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forGROUPRECENTACTIVITY($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$topic 	= TopicPeer::retrieveByPK($row['_NEW_ID']);
					$creator= new Traveler($row['_DOER_ID_']);
					$group	= $topic->getGroup();
					if( $creator->getTravelerID() != $this->parentGroup->getAdministratorID() ){
						
						$update = new Update;
						$update->setExecutionDate($row['_EXECUTION_DATE_']);
						$update->setDoer(new UpdateDoer($creator));
						$update->setRetriever($this);
						$update->setContextLink("/topic/discussions/".$topic->getID());
						$update->setContextName('Topic');
						$update->setSubjectGroup($this->subjectGroup);
						$update->setParentGroup($this->parentGroup);
						$update->setAction(UpdatesComposer::compose(
							array(
								'action' 			=> UpdateAction::POSTED_NEW_TOPIC,
								'title'				=> $topic->getTitle(),
								'groupName'			=> $group->getName(),
								'linktofeed'		=> $update->getContextLink()
							), 'tpl.GroupMembershipUpdates.php'));
						$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;	
					}
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>