<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');

	class PhotoAlbumWithDeletedPhotos implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			//if( empty($groupIDs) ) 
			//	return array();
			$groupID = $this->subjectGroup->getGroupID();
			
			$db  = new dbHandler('Travel_Logs');
			$sql = '( SELECT photoID ' . 
				   'FROM `Travel_Logs`.`tblTravelLogtoPhoto` as ttp, `Travel_Logs`.`tblTravelLog` as tl, `Travel_Logs`.`tblTravel` as t , `Travel_Logs`.`tblGroupApprovedJournals` as gaj ' . 
				   'WHERE ttp.travellogID = tl.travellogID ' . 
						'AND tl.deleted = 0 ' . 
						'AND tl.travelID = t.travelID '.
						'AND t.deleted = 0 ' . 
						'AND t.travelID = gaj.travelID '.
						'AND gaj.groupID = ' .$groupID . ' ' .  
					'GROUP BY photoID ) UNION ( SELECT photoID FROM `Travel_Logs`.`tblTravelertoPhoto` GROUP BY photoID ) ';
			$rs1 = new iRecordset($db->execute($sql));
			
			$sql = 	'SELECT DISTINCT(logView.`_OLD_photoalbumID`), logView.`_DOER_ID_`, MAX(logView.`_EXECUTION_DATE_`) AS _EXECUTION_DATE_ ' .
					'FROM `Travel_Logs`.`tblPhotoAlbumtoPhoto` as logView <$ ,Travel_Logs.tblPhotoAlbum $> ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'DELETE\' ' .
						'AND logView.`_OLD_photoalbumID` = <$ tblPhotoAlbum.photoalbumID $> ' .
						'AND logView.`_OLD_photoID` NOT IN (\'' . join('\', \'', $rs1->retrieveColumn('photoID')) . '\') ' .
						'<$ ' .
							'AND `tblPhotoAlbum`.`groupID` = \''.$groupID.'\' ' . 
						'$> ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
					'GROUP BY `logView`.`_OLD_photoalbumID` ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forMEMBERDELETEDPHOTOS($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					//$photos	= $this->retrievePhotoArray($row['_OLD_photoalbumID'],$row['_EXECUTION_DATE_']);
					$traveler = new Traveler($row['_DOER_ID_']);
					$update = new Update;
					$update->setExecutionDate($row['_EXECUTION_DATE_']);
					$update->setDoer(new UpdateDoer($traveler));
					$update->setRetriever($this);
					$update->setContextLink('/collection.php?type=group&ID='.$this->subjectGroup->getGroupID().'&context=photoalbum&genID='.$row['_OLD_photoalbumID']);
					$update->setContextName('Photo Album');
					$update->setSubjectGroup($this->subjectGroup);
					$update->setParentGroup($this->parentGroup);
					$update->setPhotoAlbum(new PhotoAlbum($row['_OLD_photoalbumID']));
					//$update->setPhotos($photos);
					$update->setAction(UpdatesComposer::compose(
						array(
							'action' 	=> UpdateAction::PHOTO_ALBUM_WITH_DELETED_PHOTOS,
							'date'		=> GaDateTime::create($row['_EXECUTION_DATE_'])->commonDateFormat()
						), 'tpl.PhotoUpdates.php'));
						
					$time_key = strtotime($row['_EXECUTION_DATE_']);
					
					while(array_key_exists($time_key,$arr)){
						$time_key = strtotime("+1 second",$time_key);
					}
					$arr[$time_key] = $update;
					
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>