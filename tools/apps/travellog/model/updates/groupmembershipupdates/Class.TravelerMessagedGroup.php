<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');
	
	require_once('travellog/model/Class.SendableFactory.php');

	class TravelerMessagedGroup implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			if( empty($groupIDs) ) 
				return array();
			$db  = new dbHandler;
			$sql = 	'SELECT `tblGroup`.`sendableID`, groupID ' .
					'FROM tblGroup ' . 
					'WHERE 1 ' .
						'AND `tblGroup`.`groupID` IN (\'' . join('\', \'', $groupIDs) . '\') '	;
			$rs1 = new iRecordset($db->execute($sql));
			if( !$rs1->retrieveRecordcount() ){
				return array();
			}
			$sql = 	'SELECT logView.*, CAST(logView._EXECUTION_DATE_ AS DATE ) as eDate ' .
						', `tblMessages`.`_NEW_title` ' .
					'FROM `Travel_Logs`.`tblMessageToAttribute` as logView, `Travel_Logs`.`tblMessages` ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'INSERT\' ' .
						'AND logView.`_NEW_messageType` = 1 ' . 
						'AND logView.`_LOG_ID_` = `tblMessages`.`_LOG_ID_` ' .
						'AND logView.`_NEW_messageID` = `tblMessages`.`_NEW_messageID` ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
						'AND logView.`_NEW_recipientID` IN (\'' . join('\', \'', $rs1->retrieveColumn('sendableID')) . '\') ' .
					'GROUP BY logView.`_NEW_messageID` ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			
			$startQRY = microtime(true);
			$rs = $this->reader->getLogsBySql($sql);
			$endQRY	= microtime(true);
			if( array_key_exists('dbhDebug', $_GET)){
				//echo "<br />$sql<br /><br />";
				//echo "QRY START : {$startQRY} END: {$endQRY} DIFF: " . ($endQRY-$startQRY) . '<br/>';
			}
			
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forGROUPRECENTACTIVITY($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$traveler 	= new Traveler($row['_DOER_ID_']);
					$group		= SendableFactory::instance()->create($row['_NEW_recipientID']);
					if( $traveler->getTravelerID() != $this->parentGroup->getAdministratorID() ){
						$update = new Update;
						$update->setExecutionDate($row['_EXECUTION_DATE_']);
						$update->setDoer(new UpdateDoer($traveler));
						$update->setRetriever($this);
						$update->setContextLink('/messages.php?gID=' . $this->subjectGroup->getGroupID());
						$update->setContextName('Message Center');
						$update->setSubjectGroup($this->subjectGroup);
						$update->setParentGroup($this->parentGroup);
						$update->setAction(UpdatesComposer::compose(
							array(
								'action' 			=> UpdateAction::TRAVELER_MESSAGED_GROUP,
								'messageTitle'		=> $row['_NEW_title'],
								'groupName'			=> $group->getGroupID() == $this->parentGroup->getGroupID() ? 'group' : $group->getName(),
								'linktofeed'		=> $update->getContextLink()
							), 'tpl.GroupMembershipUpdates.php'));
						$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;	
					}
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>