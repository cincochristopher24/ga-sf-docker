<?php
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('Class.GaString.php');
	require_once('Class.GaDateTime.php');
	
	require_once('travellog/model/Class.Traveler.php');
	
	class GroupInviteTraveler implements GroupMembershipUpdatesRetriever{
			
		private $mReader 		= NULL;
		private $subjectGroup 	= NULL;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			
			if( empty($groupIDs) )
				return array();
			
			$sql = 	'SELECT *, CAST(`_EXECUTION_DATE_` AS DATE ) as eDate ' .
						',GROUP_CONCAT(`_NEW_groupID` SEPARATOR "," ) as gIDs ' .
						',GROUP_CONCAT(`_EXECUTION_DATE_` SEPARATOR "," ) as eDates ' .
					'FROM `Travel_Logs`.`tblInviteList` ' .
					'WHERE 1 ' .
						'AND `_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe($startDate) . ' AND ' . GaString::makeSqlSafe($endDate) . ' '.
						'AND `_NEW_groupID` IN (\'' . join('\', \'', $groupIDs) . '\') ' .
						'AND `_NEW_status` = 0 ' .
						'AND (`_COMMAND_` = \'INSERT\' OR `_COMMAND_` = \'UPDATE\') ' .
						'AND `_DOER_ID_` <> _NEW_travelerID ' .
					'GROUP BY eDate, `_NEW_travelerID` ' .
					'ORDER BY `_EXECUTION_DATE_` DESC ';
			
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$s = "for" . strtoupper($callerClass);
			$rs = $this->mReader->getLogsBySql($sql);
			/*if( array_key_exists('dbhDebug', $_GET) ){
				echo "$sql<br/>";
				$rs->dump();
			}*/
			return method_exists($this, $s) ? $this->$s($rs->retrieveData()) : array();
		}
		
		private function forGROUPMEMBERSHIPUPDATES($datasource){
			
			$arr = array();
			$previous = array();
			while($row = mysql_fetch_assoc($datasource)){
				try{
					/*$arrGrp = GroupFactory::instance()->create(array($row['_NEW_groupID']));
					$group	= $arrGrp[0];
					$traveler = new Traveler($row['_NEW_travelerID']);*/
					
					$eDates		= explode(',', $row['eDates']);
					array_walk($eDates, create_function('&$v,$k', '$v = strtotime($v);'));
					rsort($eDates);
					array_walk($eDates, create_function('&$v,$k', '$v = GaDateTime::dbDateTimeFormat($v);'));
					$row['_EXECUTION_DATE_'] = $eDates[0];
					$traveler 	= new Traveler($row['_NEW_travelerID']);
					$groupName 	= 'group';
					if( $this->subjectGroup->getGroupID() == $this->parentGroup->getGroupID() ){
						$arrGrp		= array_unique(explode(',', $row['gIDs']));
						if( count($arrGrp) > 1){
							$groupName = count($arrGrp) . ' groups';
						}
						else{
							if( $arrGrp[0] != $this->subjectGroup->getGroupID() ){
								$group 		= GroupFactory::instance()->create(array($arrGrp[0]));
								$groupName 	= $group[0]->getName();
							}
						}
						/*$subGroups 	= array_values(array_diff($arrGrp, array($this->subjectGroup->getGroupID())));
						$temp = '';
						if( 1 < count($subGroups) )
							$temp = count($subGroups) . ' subgroups';
						elseif( 1 == count($subGroups) ){
							$group 	= GroupFactory::instance()->create(array($subGroups[0]));
							$temp 	= $group[0]->getName();
						}
						
						if( in_array($this->subjectGroup->getGroupID(), $arrGrp) ){
							$groupName = 'group ' . (count($subGroups) > 0 ? 'and ' . $temp : $temp);
						}
						else{
							$groupName = $temp;
						}*/	
					}
					
					if( $traveler->getTravelerID() != $this->parentGroup->getAdministratorID() ){
						$update = new Update;
						$update->setExecutionDate($row['_EXECUTION_DATE_']);
						$update->setDoer($traveler);
						$update->setAction(UpdatesComposer::compose(
							array(
								'traveler' 	=> $traveler, 
								'action' 	=> UpdateAction::GROUP_INVITED_TRAVELER,
								'groupName'	=> $groupName
							), 'tpl.GroupMembershipUpdates.php'));
						$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;
					}
				}
				catch(exception $e){}
			}
			/*if( array_key_exists('dbhDebug', $_GET) ){
				var_dump($arr);
			}*/
			return $arr;
		}
	}
?>
