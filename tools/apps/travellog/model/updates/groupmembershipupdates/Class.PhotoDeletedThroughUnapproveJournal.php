<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');

	class PhotoDeletedThroughUnapproveJournal implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		private $albumID = null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			//if( empty($groupIDs) ) 
			//	return array();
			
			$groupID = $this->subjectGroup->getGroupID();
			$sql = 	'SELECT logView.* ' . 
					'FROM `Travel_Logs`.`tblGroupApprovedJournals` as logView <$ ,`Travel_Logs`.`tblTravel` $> ' . 
					'WHERE 1=1 ' . 
						'AND logView._COMMAND_ = \'UPDATE\' ' .
					 	'AND logView.`_NEW_groupID` = \''.$groupID.'\' ' . 
					 	'AND logView.`_NEW_approved` = 0 AND logView.`_OLD_approved` = 1 ' . 
						'AND logView.`_NEW_travelID` = <$ `tblTravel`.`travelID` $> ' .
						'AND <$ tblTravel.`deleted` $> = 0 ';
			$rs1 = $this->reader->getLogsBySql($sql);		
			
			$this->albumID = $params['ALBUM_ID'];
			$sql = 	'SELECT DISTINCT(logView.`_DOER_ID_`), COUNT(_OLD_photoID) as cnt, MAX(logView.`_EXECUTION_DATE_`) as _EXECUTION_DATE_ ' .
					'FROM `Travel_Logs`.`tblPhotoAlbumtoPhoto` as logView <$ ,`Travel_Logs`.`tblTravelLogtoPhoto` ,`Travel_Logs`.`tblTravelLog` $> ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'DELETE\' ' .
						'AND logView.`_OLD_photoID` = <$ `tblTravelLogtoPhoto`.`photoID` $> ' .
						'AND logView.`_OLD_photoalbumID` = \'' .$this->albumID. '\' ' .
						//'AND logView.`_EXECUTION_DATE_` IN (\'' . join('\', \'', $rs1->retrieveColumn('_EXECUTION_DATE_')) . '\') ' . 
						'<$ ' . 
							'AND `tblTravelLogtoPhoto`.`travellogID` = `tblTravelLog`.`travellogID` ' .  
							'AND `tblTravelLog`.`travelID` IN (\'' . join('\', \'', $rs1->retrieveColumn('_NEW_travelID')) . '\') ' .
						'$> ' .						
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
					'GROUP BY `logView`.`_EXECUTION_DATE_` ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forMEMBERDELETEDPHOTOS($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$traveler = new Traveler($row['_DOER_ID_']);
					$update = new Update;
					$update->setExecutionDate($row['_EXECUTION_DATE_']);
					$update->setDoer(new UpdateDoer($traveler));
					$update->setRetriever($this);
					$update->setSubjectGroup($this->subjectGroup);
					$update->setContextLink('/collection.php?type=group&ID='.$this->subjectGroup->getGroupID().'&context=photoalbum&genID='.$this->albumID);
					$update->setParentGroup($this->parentGroup);
					$update->setPhotoAlbum(new PhotoAlbum($this->albumID));
					$update->setUAction(UpdateAction::PHOTO_DELETED_THROUGH_UNAPPROVE_JOURNAL);
					$update->setAction(UpdatesComposer::compose(
						array(
							'action' 	=> UpdateAction::PHOTO_DELETED_THROUGH_UNAPPROVE_JOURNAL,
							'photocnt'	=> $row['cnt']
						), 'tpl.PhotoUpdates.php'));
					$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>