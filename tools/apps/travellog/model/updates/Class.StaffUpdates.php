<?php
	
	require_once('travellog/model/updates/Class.AbstractGroupUpdates.php');
	
	class StaffUpdates extends GroupUpdates {
		
		public function __construct($group){
			parent::__construct($group);
		}
		
		public function prepareUpdates($options = array()){
			$st = microtime(true);
			$this->updates 	= array();
			$this->setOptions($options);
			
			if( !is_null($this->options['LOGGED_TRAVELER']) )
				$this->loggedTraveler = $this->options['LOGGED_TRAVELER'];
			
			if( is_null($this->loggedTraveler) || 'Group' != get_parent_class($this->subjectGroup) ){
				return;
			}
			
			$allowedKeys= $this->options['DENIED_INVITES_ONLY'] ? array(UpdateAction::TRAVELER_DENIED_INVITE_STAFF) : UpdateAction::getStaffUpdateActions();
			$groupIDs	= $this->getStaffedGroupIDs();
			foreach( $allowedKeys as $key ){
				if( !array_key_exists($key, $this->retrievers) )
					continue;
				$retriever = $this->retrievers[$key];
				$retrieveVars = array(
					'startDate' => $this->options['START_DATE'], 
					'endDate' 	=> $this->options['END_DATE'], 
					'group' 	=> $this->subjectGroup,
					'parentGroup' => $this->parentGroup,
					'groupIDs'	=> $groupIDs
				);
				$retrievedUpdates = $retriever->retrieve($retrieveVars);
				if(!empty($retrievedUpdates)){
					$this->updates += $retrievedUpdates;
				}
			}
			$this->sortUpdates();
			$et = microtime(true);
			//echo "START : {$st} END: {$et} DIFF: " . ($et-$st) . '<br/>'; exit;
			return $this->updates;
			
		} // end prepareUpdates
		
		public function render(){
			$vars = array(
				'groupMembershipUpdates' => $this->updates
			);
			$tpl = new Template();
			$tpl->setVars($vars);
			$tpl->set_path('travellog/views/');
			return $tpl->fetch('tpl.ViewGroupMembershipUpdatesPanel.php');
		}
	}

?>