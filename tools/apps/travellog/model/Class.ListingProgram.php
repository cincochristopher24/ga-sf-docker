<?
	/**
	 * Class.ListingProgram.php
	 * @author marc
	 * oct 6, 2006
	 * represents GA Listing program/categories
	 */
	 
	require_once('Class.Connection.php');
 	require_once('Class.Recordset.php');
 	require_once('Class.ListingProgramType.php');

	/**
	 * @var int incprogramID
	 * @var string program
	 * @var array programType
	 * @var object nConnection - see Class.Connection.php
	 * @var object nRecordset - see Class.Recordset.php
	 */
	 
	class listingProgram{
	
		private $incprogramID;
		private $program;
		private $programType;
		private $nConnection;
		private $nRecordset;
				
		/**
		 * constructor
		 * @param int incprogramID
		 * @param int travelerID
		 * @return object self
		 */ 
		
		function listingProgram($incprogramID = 0,$travelerID = 0){
			
			try {
 				$this->nConnection = new Connection();
				$this->nRecordset   = new Recordset($this->nConnection);
			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			if ( $incprogramID > 0 ){
				$sqlquery = "SELECT  Travel_Logs.tblIncProgram.incprogramID,program FROM Travel_Logs.tblIncProgram,GoAbroad_Main.tbprogram";
				if ($travelerID > 0)
					$sqlquery = $sqlquery . ",Travel_Logs.tblTravelertoIncProgram";	
				$sqlquery = $sqlquery . " WHERE Travel_Logs.tblIncProgram.programID=GoAbroad_Main.tbprogram.programID ";
				if ($travelerID > 0)
					$sqlquery = $sqlquery . " AND Travel_Logs.tblIncProgram.incprogramID =Travel_Logs.tblTravelertoIncProgram.incprogramID " .
					"AND Travel_Logs.tblTravelertoIncProgram.travelerID=" . $travelerID;			
				$sqlquery = $sqlquery . " AND Travel_Logs.tblIncProgram.incprogramID=" . $incprogramID . " GROUP BY program";
				$myquery = $this->nRecordset->Execute($sqlquery);
				if (mysql_num_rows($myquery) == 0)
					die ('Invalid Program');
				$result = mysql_fetch_array($myquery);		
				$this->setProgramID($result["incprogramID"]);
				$this->setProgram($result["program"]);
				$this->qryProgramTypes($travelerID);
			}
			
			return $this;
		}
		
		/**
		 * @param int programID
		 */	
		function setProgramID($programID){
			$this->incprogramID = $programID;
		}
	
		/**
		 * @return int self::programID
		 */
		function getProgramID(){
			return $this->incprogramID;
		}
	
		/**
		 * @param string program 
		 */
		function setProgram($program){
			$this->program = $program;
		}
	
		/**
		 * @return string self::program
		 */
		function getProgram(){
			return $this->program;
		}
	
		/**
		 * @param array programType
		 */
		function setProgramType($programType){
			$this->programType = $programType;
		}
	
		/**
		 * @return array self::programType
		 */
		function getProgramType(){
			return $this->programType;
		}
		
		/**
		 * gets all program types
		 * if travelerID > 0, gets all program type chosen by student
		 * @param int travelerID
		 */
		function qryProgramTypes($travelerID = 0){
			
			switch($this->incprogramID){
			
				//Volunteer Abroad		:: tbvolunteertype
				case "1": 
					$sqlquery = "SELECT volunteertypeID as incprogramtypeID FROM GoAbroad_Main.tbvolunteertype";
					 if($travelerID > 0)
					 	$sqlquery = $sqlquery . ",Travel_Logs.tblTravelertoIncProgram " .
					 	"WHERE volunteertypeID = incprogramtypeID " .
					 	"AND incprogramID = 1 " .
					 	"AND travelerID=" . $travelerID;
					break;
			
				// Intrenships Abroad & Jobs Abroad		:: tbinterntype 		
				case "3":
					$sqlquery = "SELECT interntypeID as incprogramtypeID FROM GoAbroad_Main.tbinterntype";
					if($travelerID > 0)
					 	$sqlquery = $sqlquery . ",Travel_Logs.tblTravelertoIncProgram " .
					 	"WHERE interntypeID = incprogramtypeID " .
					 	"AND incprogramID = 3 " .
					 	"AND travelerID=" . $travelerID;
					break;				
				case "4": 
					$sqlquery = "SELECT interntypeID as incprogramtypeID FROM GoAbroad_Main.tbinterntype";
				  	if($travelerID > 0)
					 	$sqlquery = $sqlquery . ",Travel_Logs.tblTravelertoIncProgram " .
					 	"WHERE interntypeID = incprogramtypeID " .
					 	"AND incprogramID = 4 " .
					 	"AND travelerID=" . $travelerID;
					break;
					
				//Study Abroad    :: tbdegprog
				case "5": 
					$sqlquery = "SELECT degprogID as incprogramtypeID FROM GoAbroad_Main.tbdegprog";
					if($travelerID > 0)
					 	$sqlquery = $sqlquery . ",Travel_Logs.tblTravelertoIncProgram " .
					 	"WHERE degprogID = incprogramtypeID " .
					 	"AND incprogramID = 5 " .
					 	"AND travelerID=" . $travelerID;
					break;
											
				// Languages Abroad		:: tblanguage		
				case "6":
					$sqlquery = "SELECT languageID as incprogramtypeID FROM GoAbroad_Main.tblanguage";
					if($travelerID > 0)
					 	$sqlquery = $sqlquery . ",Travel_Logs.tblTravelertoIncProgram " .
					 	"WHERE languageID = incprogramtypeID " .
					 	"AND incprogramID = 6 " .
					 	"AND travelerID=" . $travelerID;
					break;
			}
			$arrProgramType = array();
			if ($this->incprogramID != 2){
				$myquery = $this->nRecordset->Execute($sqlquery);
				while ( $row = mysql_fetch_array($myquery) ){
					$nProgramType = new listingProgramType($row["incprogramtypeID"],$this->incprogramID);
					$arrProgramType[] = $nProgramType;
				}
			}	
			$this->setProgramType($arrProgramType);
		}
		
		/**
		 * gets all programs
		 * @return array - array of programs/categories
		 */		
		function getAllPrograms(){
 			$sqlquery = "SELECT incprogramID FROM tblIncProgram";
 			$myquery = $this->nRecordset->Execute($sqlquery);
 			$arrPrograms = array();
 			while ( $row = mysql_fetch_array($myquery)){
 				$nProgram = new listingProgram($row["incprogramID"]);
 				$arrPrograms[] = $nProgram;
 			}
 			//mysql_free($myquery);
 			return $arrPrograms;
 		}
	
	}
?>
