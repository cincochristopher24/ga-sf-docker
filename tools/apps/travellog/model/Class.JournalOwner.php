<?php
	/**
	 * @(#) Class.JournalOwner.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 28, 2009
	 */
	
	require_once('travellog/model/base/Class.BaseJournalOwner.php');
	
	/**
	 * Model class for journal owner.
	 */
	class JournalOwner extends BaseJournalOwner {
		
	}