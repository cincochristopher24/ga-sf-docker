<?php
/*
 * Created on 02 2, 09
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

	require_once('Class.VideoAlbumToVideo.php');
	require_once('Class.Connection.php');
    require_once('Class.Recordset.php');
    require_once('Class.ConnectionProvider.php');
    require_once('Cache/ganetCacheProvider.php');
	
	class VideoAlbum {
		
		/**
		 * member variables
		 */
		
		protected $mAlbumID;
		protected $mTitle;
		protected $mCreatorID;
		protected $mCreatorType;
		protected $mDateCreated;
		protected $mLastDateUpdated; 
		
		private $mConn      = NULL;
        private $mRs        = NULL;
		
		/**
		 * constructor
		 */
		
		public function __construct ($albumID = 0) {
			$this->mAlbumID = $albumID;
			
			try {
                $this->mRs   = new Recordset(ConnectionProvider::instance()->getConnection());
            }
            catch (Exception $e) {                 
               throw $e;
            }
            
            if (0 < $this->mAlbumID){ 
               	$query = 'SELECT * FROM tblVideoAlbum WHERE albumID = '.$albumID;
                $this->mRs->Execute($query);
            
                if (0 == $this->mRs->Recordcount()){
                    throw new Exception('Invalid VideoAlbumID');        // ID not valid so throw exception
                }
                
                // Sets values to its class member attributes.      
                $this->mTitle       	= stripslashes($this->mRs->Result(0, 'title'));           
                $this->mCreatorID   	= $this->mRs->Result(0, 'creatorID');
                $this->mCreatorType 	= $this->mRs->Result(0, 'creatorType');
                $this->mDateCreated 	= $this->mRs->Result(0, 'dateCreated');  
                $this->mLastDateUpdated = $this->mRs->Result(0, 'lastDateUpdated');           
           }
		}
		
		/**
		 * setters
		 */
		
		public function setAlbumID ($albumID) {
			$this->mAlbumID = $albumID;
		}
		
		public function setTitle ($title) {
			$this->mTitle = $title;
		}
		
		public function setCreatorID ($creatorID) {
			$this->mCreatorID = $creatorID;
		}
		
		public function setCreatorType ($type) {
			$this->mCreatorType = $type;
		}
		
		public function setDateCreated ($dateCreated) {
			$this->mDateCreated = $dateCreated;
		}
		
		public function setLastDateUpdated ($lastDateUpdated) {
			$this->mLastDateUpdated = $lastDateUpdated;
		}
		
		/**
		 * getters
		 */
		
		public function getAlbumID () {
			return $this->mAlbumID;
		}
		
		public function getTitle () {
			return $this->mTitle;
		}
		
		public function getCreatorID () {
			return $this->mCreatorID;
		}
		
		public function getCreatorType () {
			return $this->mCreatorType;
		}
		
		public function getDateCreated () {
			return $this->mDateCreated;
		}
		
		public function getLastDateUpdated () {
			return $this->mLastDateUpdated;
		}
		
		public function getOwner () {
			switch( strtolower($this->getCreatorType()) ){
				case 'traveler':		
					return new Traveler($this->getCreatorID());
				case 'group': 			
					$groups = GroupFactory::instance()->create(array($this->getCreatorID()));
					return $groups[0];
				case 'group_template': 	
					return GanetGroupTemplatePeer::retrieveByPk($album->getCreatorID());
			}
		}
		
		/**
		 * crude methods
		 */
		 
		public function create () {
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 		$query = "INSERT INTO tblVideoAlbum(" .
						"title, " .
						"creatorID, " .
						"creatorType, " .
						"dateCreated," .
						"lastDateUpdated) ".
					 "VALUES (" .
					 	"'".addslashes($this->mTitle)."', " .
					 	"$this->mCreatorID, " .
					 	"'$this->mCreatorType', " .
					 	"'$this->mDateCreated', " .
					 	"'$this->mLastDateUpdated')";
					 	
            $this->mRs->Execute($query);       
            $this->mAlbumID = $this->mRs->GetCurrentID();
            
            //added by Jul to create feeds for this newly added video album
			require_once("travellog/model/Class.ActivityFeed.php");
			try{
				ActivityFeed::create($this);
			}catch(exception $e){}
            
			$this->invalidateCacheEntry();
		}
		
		public function update () {
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            
           	$query = "UPDATE tblVideoAlbum SET ".
	 					"title 				= '".addslashes($this->mTitle)."', ".
	 					"creatorID 			= ".$this->mCreatorID.", ".
	 					"creatorType 		= '".$this->mCreatorType."', ".
	 					"dateCreated 		= '".$this->mDateCreated."', ".
	 					"lastDateUpdated 	= '".$this->mLastDateUpdated."' ".
	 				  "WHERE albumID 		= ".$this->mAlbumID;
            $this->mRs->Execute($query);
           	$this->invalidateCacheEntry();
		}
		
		public function delete () {
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            $query = 'DELETE FROM tblVideo WHERE albumID = '.$this->mAlbumID;
			$this->mRs->Execute($query);	
            $query = 'DELETE FROM tblVideoAlbum WHERE albumID = '.$this->mAlbumID;       
            $this->mRs->Execute($query);

			//added by Jul to delete feeds of this video album
			require_once("travellog/model/Class.ActivityFeed.php");
			$VIDEOALBUM = ActivityFeed::VIDEOALBUM;
			$VIDEO = ActivityFeed::VIDEO;
			$albumID = $this->mAlbumID;
			$sql = "DELETE FROM tblActivityFeed
					WHERE (feedSection = $VIDEOALBUM AND sectionID = $albumID)
						OR (feedSection = $VIDEO AND sectionRootID = $albumID)";
			$this->mRs->Execute($sql);
			
			$this->invalidateCacheEntry();	
		} 
		
		public function getVideos () {
			$cache 	=	ganetCacheProvider::instance()->getCache();
			$key	=	'AlbumVideos_'.$this->getAlbumID();
			
			if ($cache != null && $videos =	$cache->get($key)){
				if ($videos[0] == 'EMPTY'){
					$videos	= array();				// if we found the contents 'EMPTY'we reassign the array to a blank array
				}	
				return $videos;
			}
			
			$videos = array();
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
 			
 			$query = 'SELECT videoID FROM tblVideo WHERE albumID = '.$this->mAlbumID.' ORDER BY videoID ASC';
 			$this->mRs->Execute($query);
 			
 			if (0 < $this->mRs->Recordcount()) {
 				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
 			 		try {
 			 			$video = new VideoAlbumToVideo($recordset['videoID']);
 			 		}
 			 		
 			 		catch (exception $e) {						  
						throw new exception ($e->getMessage());
					}
					
					$videos[] = $video;	 		
 			 	}	
 			}
 			
 			return $videos;
		}
		
		/**
 		 * method that gets the primary video
 		 */
 		 
 		public function getPrimaryVideo() {
 			$this->mPrimaryVideo = null;
 			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
 			
 			$query = 'SELECT tblVideo.videoID from tblVideo WHERE tblVideo.isPrimaryVideo = 1 AND albumID = '.$this->mAlbumID;
 			$this->mRs->Execute($query);	
 			
 			if (0 < $this->mRs->Recordcount()) {
 				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
 			 		try {
 			 			$this->mPrimaryVideo = new VideoAlbumToVideo($recordset['videoID']);
 			 		}
 			 		
 			 		catch (exception $e) {						  
						throw new exception ($e->getMessage());
					}	 		
 			 	}	
 			}
 			
 			return $this->mPrimaryVideo;
 		}
		
		function getVideos2($rowslimit=NULL){
			$limitstr = "";
			if( !is_null($rowslimit) ){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }
            
            $this->mVideos = array();
            $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
            
			$query = "SELECT videoID
						FROM tblVideo
						WHERE albumID = $this->mAlbumID 
						ORDER BY videoID DESC
						$limitstr";
			$this->mRs->Execute($query);
						
 			if (0 < $this->mRs->Recordcount()) {
 				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
	 				try {
 						$video = new VideoAlbumToVideo($recordset['videoID']);
						if( $video instanceof VideoAlbumToVideo ){
							$this->mVideos[] = $video;
						}
					}catch(exception $e){						  
					   continue;
					}
 				}
			}
 			
 			return $this->mVideos;
		}
		
		function getVideosUploadedByDay($rowslimit=NULL,$date=NULL,$countOnly=FALSE){
			if ( !is_null($rowslimit) ){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }else{
                $limitstr  = ''; 
            }
			if( is_null($date) ){
				$date = date("Y-m-d H:i:s");
			}
			$albumID = $this->mAlbumID;
            
            $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
            
            $sql = "SELECT SQL_CALC_FOUND_ROWS tblVideo.* from tblVideo 
            		WHERE CAST(dateAdded AS DATE) = CAST('$date' AS DATE) 
						AND albumID = $albumID
            		ORDER BY dateAdded DESC " . $limitstr;
            //$this->rs->Execute($sql);
            $this->mRs->Execute($sql);	
			
			//$rs2 = new Recordset($this->conn);
			$rs2 = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql2 = "SELECT FOUND_ROWS() AS recordCount";
			$rs2->Execute($sql2);
			$data = mysql_fetch_array($rs2->Resultset());
			
			$count = $data["recordCount"];
			if( $countOnly ){
				return $count;
			}
			$videos = array();
            if (0 < $count){
				try{
	 				if (0 < $this->mRs->Recordcount()){
		 				while ($recordset = mysql_fetch_array($this->mRs->Resultset())){
			 				try {
		 						$video = new VideoAlbumToVideo($recordset['videoID']);
								if( $video instanceof VideoAlbumToVideo ){
									$videos[] = $video;
								}
							}catch(exception $e){						  
							   continue;
							}
		 				}
	 				}
	 			}catch(exception $ex){}
			}
			return array("videos"=>$videos, "recordCount"=>$count);
		}
		
		public function invalidateCacheEntry(){
			$cache	=	ganetCacheProvider::instance()->getCache();
			if (!is_null($cache)) {
				$cache->delete(ucwords($this->mCreatorType).'_VideoAlbums_'.$this->mCreatorID);
				$cache->delete('AlbumVideos_'.$this->mAlbumID);
			}
		}
		
		public function getURL(){
			if( 'traveler' == $this->getCreatorType() ){
				return "/video.php?action=getTravelerVideos&amp;travelerID=".$this->getCreatorID()."&amp;albumID=".$this->getAlbumID()."&amp;type=album";
			}else{
				return "/video.php?action=getGroupVideos&amp;gID=".$this->getCreatorID()."&amp;albumID=".$this->getAlbumID()."&amp;type=album";
			}
		}
		
		public function getFriendlyURL(){
			return $this->getURL();
		}
	}