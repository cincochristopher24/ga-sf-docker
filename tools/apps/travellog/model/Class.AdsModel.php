<?php
class AdsModel{
	function GetList(){
		require_once("travellog/model/Class.Advertisement.php");
		require_once("travellog/vo/Class.AdsVO.php");
		$obj_ads_vo = new AdsVO;
		$obj_ads    = Advertisement::getAd();
		
		$obj_ads_vo->setUrl      ( $obj_ads->getUrl()       );
		$obj_ads_vo->setImagePath( $obj_ads->getImagePath() );
		$obj_ads_vo->setAltText  ( $obj_ads->getAltText()   );
		$obj_ads_vo->setAdText   ( $obj_ads->getAdText()    );
		    
		return $obj_ads_vo;
	}
}
?>
