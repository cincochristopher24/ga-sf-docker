<?php
/*
 * Created on 02 2, 09
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 	/**
	 * class mapping the entities VideoAlbum and Video
	 */

	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
	require_once('Class.VideoAlbum.php');
	require_once('Class.Video.php');
	require_once('Class.ConnectionProvider.php');
    require_once('Cache/ganetCacheProvider.php');
	
	class VideoAlbumToVideo extends Video {
		
		private $mConn      = NULL;
        private $mRs        = NULL;
		
		/**
		 * constructor
		 */
		
		public function __construct($videoID = 0) {
			$this->mVideoID = $videoID;
			
			try {
                $this->mRs   = new Recordset(ConnectionProvider::instance()->getConnection());
            }
            catch (Exception $e) {                 
               throw $e;
            }
            
            if (0 < $this->mVideoID){ 
               	$query = 'SELECT * FROM tblVideo WHERE videoID = '.$videoID;
                $this->mRs->Execute($query);
            
                if (0 == $this->mRs->Recordcount()){
                    throw new Exception('Invalid VideoID');        // ID not valid so throw exception
                }
                
                // Sets values to its class member attributes.      
                $this->mVideoUrl       	= $this->mRs->Result(0, 'videoURL');           
                $this->mVideoImageUrl   = $this->mRs->Result(0, 'videoImageURL');
                $this->mCaption 		= stripslashes($this->mRs->Result(0, 'caption'));
                $this->mDateAdded 		= $this->mRs->Result(0, 'dateAdded');  
                $this->mIsPrimaryVideo  = $this->mRs->Result(0, 'isPrimaryVideo');    
                $this->mDuration		= $this->mRs->Result(0, 'duration');
                $this->mTitle			= $this->mRs->Result(0, 'title');
                $this->mParentAlbum 	= new VideoAlbum($this->mRs->Result(0, 'albumID'));       
           }
		}
		
		/**
		 * crude methods
		 */
		
		public function create() {
			$albumID = $this->mParentAlbum->getAlbumID();
			$this->mIsPrimaryVideo = (0 == count($this->mParentAlbum->getVideos())) ? 1 : 0;
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		
	 		$query = "INSERT INTO tblVideo(" .
						"videoURL, " .
						"videoImageURL, " .
						"caption, " .
						"dateAdded, " .
						"isPrimaryVideo, " .
						"duration, " .
						"title, " .
						"albumID) " .
					  "VALUES (" .
					  	"'$this->mVideoUrl',".
	 					"'$this->mVideoImageUrl',".
	 					"'".addslashes($this->mCaption)."',".
	 					"'$this->mDateAdded',".
	 					"'$this->mIsPrimaryVideo',".
	 					"'$this->mDuration',".
	 					"'".addslashes($this->mTitle)."'," .
	 					"$albumID)";
					 	
            $this->mRs->Execute($query);       
            $this->mVideoID = $this->mRs->GetCurrentID();
            
            //added by Jul to create feeds for this video
			require_once("travellog/model/Class.ActivityFeed.php");
			try{
				ActivityFeed::create($this,$this->mParentAlbum);
			}catch(exception $e){}
            
			$this->invalidateCacheEntry();
		}
		
		public function update() {
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            
           	$query = "UPDATE tblVideo SET ".
	 					"videoURL 		= '".$this->mVideoUrl."', ".
	 					"videoImageURL 	= '".$this->mVideoImageUrl."', ".
	 					"caption 		= '".addslashes($this->mCaption)."', ".
	 					"dateAdded 		= '".$this->mDateAdded."', ".
	 					"isPrimaryVideo = ".$this->mIsPrimaryVideo.", ".
	 					"duration 		= '".$this->mDuration."', ".
	 					"title 			= '".addslashes($this->mTitle)."', ".
	 					"albumID		= ".$this->mParentAlbum->getAlbumID()." ".
	 				  "WHERE videoID 	= ".$this->mVideoID;
            $this->mRs->Execute($query);
           	$this->invalidateCacheEntry();
		}
		
		public function delete() {
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            $query = 'DELETE FROM tblVideo WHERE videoID = '.$this->mVideoID;
			$this->mRs->Execute($query);	
            
			//added by Jul to delete feeds of this video
 			$addedByDayCount = $this->mParentAlbum->getVideosUploadedByDay(NULL,$this->mDateAdded,TRUE);
			if( 0 == $addedByDayCount ){
				require_once("travellog/model/Class.ActivityFeed.php");	
				$params = array("FEED_SECTION"		=>	ActivityFeed::VIDEO,
								"SECTION_ROOT_ID"	=>	$this->mParentAlbum->getAlbumID(),
								"FEED_DATE"			=>	$this->mDateAdded );
				$videoFeeds = ActivityFeed::getInstances($params);
				if( 0 < $videoFeeds["recordCount"] ){
					$videoFeeds["activityFeeds"][0]->Delete();
				}
			}
			
			$this->invalidateCacheEntry();	
		}
		
		public function addComment( Comment $comment ){
			parent::addComment($comment);

			$owner = $this->getParentAlbum()->getOwner();
			$travelerID = ( $owner instanceof Traveler ) ? $owner->getTravelerID() : $owner->getAdministratorID();
			
			if ($travelerID != $comment->getAuthor()){
				$comment->setCommentType(CommentType::$VIDEO);
				require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
				RealTimeNotifier::getInstantNotification(RealTimeNotifier::RECEIVED_GREETING, array('COMMENT' => $comment))->send();
			}
		}
		
		public function hasActionPrivilege($traveler_id){
			$owner = $this->getParentAlbum()->getOwner();

			if( $owner instanceof Traveler ){
				return ($owner->getTravelerID() == $traveler_id);
			}
			else if( $owner instanceof Group ){
				return ($owner->getAdministratorID() == $traveler_id OR $owner->isStaff($traveler_id));
			}			
			
			return false;
		}
		
		public function getFriendlyUrl(){
			$album = $this->getParentAlbum();
			$owner = $album->getOwner();
			
			if( $owner instanceof Traveler || $owner instanceof Group ){
				$link = 'video.php?action=';				
				
				if( $owner instanceof Traveler ){
					$link .= 'getTravelerVideos&travelerID='.$owner->getTravelerID();
				}
				else if( $owner instanceof Group ){
					$link .= 'getGroupVideos&gID='.$owner->getID();
				}
				
				$link .= '&albumID='.$album->getAlbumID().'&videoID='.$this->getVideoID().'&type=album';
			}
			else{
				$link = 'group_template.php?action=view_videos&vID='.$this->getVideoID();
			}
			
			return $link;
		}
		
		public function getPlayActionLink(){
			return "/video.php?action=playAlbumVideo&videoID=".$this->getVideoID()."&albumID=".$this->mParentAlbum->getAlbumID();
		}
		
		function invalidateCacheEntry(){
			$cache = ganetCacheProvider::instance()->getCache();
			
			if (!is_null($cache)) {
				$cache->delete('AlbumVideos_'.$this->mParentAlbum->getAlbumID());
				$albumCreatorType = ucwords($this->mParentAlbum->getCreatorType()); 
				
				if ('Traveler' == $albumCreatorType) 
					$cache->delete('CobrandTraveler_Videos_'.$this->mParentAlbum->getCreatorID());	
				$cache->delete($albumCreatorType.'_Videos_'.$this->mParentAlbum->getCreatorID());	
			}
		}
	}