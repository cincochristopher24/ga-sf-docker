<?php
	/**
	 * Created on September 7, 2006
	 * Class.Drafts.php
	 * @author Cheryl Ivy Q. Go
	 * Last Edited on 22 Jan 2008 -- refactored
	 * Last Edited on 13 May 2008 -- optimized queries
	 */

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';

	class Drafts{
		
		private $mRs = NULL;
		private $mRs2 = NULL;
		private $mRs3 = NULL;
		private $mRs4 = NULL;
		private $mConn = NULL;

		private $mMessageId		= 0;
		private $mSendableId	= 0;
		private $mTotalRecords	= 0;

		const TRAVELER	= 1;
		const GROUP		= 2;

		/**
		 * Constructor of class Drafts
		 * @param integer $_travID
		 * @return Drafts
		 */
		
		function Drafts($_sendableID = 0){				
 			try{
 				$this->mConn = new Connection();
				$this->mRs	= new Recordset($this->mConn);
				$this->mRs2	= new Recordset($this->mConn);
				$this->mRs3	= new Recordset($this->mConn);
				$this->mRs4	= new Recordset($this->mConn);
			}
			catch (exception $e){				   
			   throw $e;
			}
	
			$this->mSendableId = $_sendableID;
			return $this;
		}

		/**
		 * Function name: Save
		 * @param Message $_message
		 * Inserts message to table tblMessages and tblMessageToAttribute
		 */
		
		function Save(Message $_message = NULL){

			// Set values to be inserted to table
			$messID = $_message->getMessageID();
			$text = addslashes($_message->getText());
			$subject = addslashes($_message->getTitle());
			$messageType = $_message->getMessageTypeID();
			$discriminator = $_message->getDiscriminator();
			$destination = $_message->getDestination();
			$source = $_message->getSource()->getSendableID();
			$dateTime = date("Y-m-d H:i:s");

			// Insert values to database table
			if (0 == $messID){			
				$sql = "INSERT INTO tblMessages (discriminator, senderID, title, message, dateCreated) " .
						"VALUES (" . $discriminator . ", ". $source . ", '" . $subject . "', '" . $text . "', '" . $dateTime . "')";
				$this->mRs->Execute($sql);
				
				$sql2 = "SELECT LAST_INSERT_ID() AS id";
				$this->mRs2->Execute($sql2);
				
				$messID = $this->mRs2->Result(0, "id");
				$_message->setMessageID($messID);
			}
			else{
				$sql = "UPDATE tblMessages " .
							"SET " .
								"title 		= '" . $subject . "', " .
								"message 	= '" . $text . "', " .
								"dateCreated = '" . $dateTime . "' " .
							"WHERE messageID = " . $messID;
				$this->mRs->Execute($sql);

				$sql2 = "DELETE FROM tblMessageToAttribute WHERE messageID = " . $messID;
				$this->mRs2-> Execute($sql2);
			}

			if (0 < sizeof($destination)){			
				foreach ($destination as $dest){				
					$sendableID = $dest->getSendableID();

					$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
								"VALUES (" . $messID . ", " . $sendableID . ", 3)";
					$this->mRs3->Execute($sql3);
				}
			}
			else{
				$sql3 = "INSERT INTO tblMessageToAttribute (messageID, messageType) " .
							"VALUES (" . $messID . ", 3)";
				$this->mRs3->Execute($sql3);
			}

			if (DiscriminatorType::$INQUIRY == $discriminator){
				$parentID = $_message->getParentID();
				$listingID = $_message->getListingID();

				// Insert parentID and listingID in tblMessageToInquiry				
				$sql4 = "INSERT INTO tblMessageToInquiry (messageID, listingID, parentID) " .
						"VALUES (" . $messID . ", " . $listingID . ", " . $parentID . ")";
				$this->mRs4->Execute($sql4);
			}
		}

		/**
		 * Function name: Send
		 * @param Message $_message
		 * Deletes and Inserts message from and to table tblMessages and tblMessageToAttribute
		 */
		
		function Send(Message $_message = NULL){

			// Set values to be inserted to table			
			$text = addslashes($_message-> getText());
			$subject = addslashes($_message-> getTitle());
			$discriminator = $_message-> getDiscriminator();
			$messageID = $_message-> getMessageID();
			$destination = $_message-> getDestination();
			$source = $_message->getSource()->getSendableID();			// Source: sendableID of Traveler/Group/Moderator
			$dateTime = date("Y-m-d H:i:s");

			// Update table tblMessages
			$sql = "UPDATE tblMessages " .
						"SET " .
							"title 	= '" . $subject . "', " .
							"message = '" . $text . "', " .
							"dateCreated = '" . $dateTime . "' " .
						"WHERE messageID = " . $messageID;
			$this->mRs->Execute($sql);

			// Delete rows in tblMessageToAttribute			
			$sql2 = "DELETE FROM tblMessageToAttribute " .
					"WHERE messageType = 3 " .
					"AND messageID = " . $messageID;
			$this->mRs2->Execute($sql2);

			// Loop through destinations and insert rows in tblMessageToAttribute			
	 		foreach ($destination as $dest){

				// if DESTINATION is TRAVELER
				if ($dest instanceof Traveler){
					$recipient = $dest->getSendableID();

					// FROM TRAVELER TO TRAVELER
					if ($_message->getSource() instanceof Traveler){
						// check if they are FRIENDS
						$isFriend = $dest->isFriend($_message->getSource()->getTravelerID());

						// FRIENDS
						if ($isFriend){
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messageID . ", " . $recipient . ", 1)";
						}
						// NOT FRIENDS
						else{
							// if from GoAbroadNetwork Team
							if ( (4933 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "www.goabroad.net")) ||
								(153 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "dev.goabroad.net")) ||
								(412 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "goabroad.net.local")) ){
								$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messageID . ", " . $recipient . ", 1)";
							}
							else{
								$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messageID . ", " . $recipient . ", 5)";
							}
						}
					}

					// FROM GROUP TO TRAVELER
					else{
						// checks if RECIPIENT is GROUP MEMBER
						$isMember = $_message->getSource()->isMember($dest);

						// MEMBER
						if ($isMember){
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messageID . ", " . $recipient . ", 1)";
						}
						// NOT MEMBER
						else{
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messageID . ", " . $recipient . ", 5)";
						}
					}

					$this->mRs2->Execute($sql2);
					$_message->setAttributeID($this->mRs2->GetCurrentID());
				}
				// if destination is GROUP/ CLUB/ SUBGROUP
				else{
					// FROM GROUP TO GROUP
					if ($_message->getSource() instanceof Group){
						$travDestination = $dest->getAdministrator()->getTravelerID();
						$travSource = $_message->getSource()->getAdministrator()->getTravelerID();

						// check if ADMINISTRATOR
						$isAdmin = ( $travDestination == $travSource ) ? true : false;

						// ADMINISTRATOR
						if ($isAdmin){
							// get members of group: exclude Admin, include staff as member of group
							$sql2 = "SELECT DISTINCT tblTraveler.sendableID " .
									"FROM tblGrouptoTraveler, tblGroup, tblTraveler " .
									"WHERE tblGrouptoTraveler.groupID = tblGroup.groupID " .
									"AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
									"AND tblGroup.isSuspended = 0 " .
									"AND tblGroup.sendableID = " . $dest->getSendableID() . " " .
									"AND tblTraveler.travelerID <> " . $travSource;
							$this->mRs2->Execute($sql2);

							while ($SendableID = mysql_fetch_array($this->mRs2->Resultset())){
								$recipient = $SendableID['sendableID'];

								$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messageID . ", " . $recipient . ", 1)";
								$this->mRs3->Execute($sql3);
								$_message->setAttributeID($this->mRs3->GetCurrentID());
							}
						}
						// NOT ADMINISTRATOR
						else{
							$recipientID = $dest->getSendableID();

							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messageID . ", " . $recipient . ", 5)";
							$this->mRs2->Execute($sql2);
						}
					}
					// FROM TRAVELER TO GROUP
					else if (!strcasecmp(get_class($_message->getSource()), "Traveler")){
						// check if SENDER is MEMBER of GROUP
						$isMember = $dest->isMember($_message->getSource());

						// MEMBER
						if ($isMember){
							$recipient = $dest->getSendableID();

							// message for ADMINISTRATOR
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messageID . ", " . $recipient . ", 1)";
							$this->mRs2->Execute($sql2);

							// get STAFF of GROUP
							$Staff = $dest->getStaff();

							foreach($Staff as $indStaff){
								$recipient = $indStaff->getSendableID();

								// message for STAFF
								$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messageID . ", " . $recipient . ", 1)";
								$this->mRs3->Execute($sql3);
							}
						}
						// NOT MEMBER
						else{
							$recipient = $dest->getSendableID();

							// message for ADMINISTRATOR
							if ( (4933 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "www.goabroad.net")) ||
								(153 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "dev.goabroad.net")) ||
								(412 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "goabroad.net.local")) ){	
								// if from GoAbroadNetwork Team
								$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messageID . ", " . $recipient . ", 1)";
							}
							else{
								$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messageID . ", " . $recipient . ", 5)";
							}
							$this->mRs2->Execute($sql2);

							// get STAFF of GROUP
							$Staff = $dest->getStaff();

							foreach($Staff as $indStaff){
								$recipient = $indStaff->getSendableID();

								// message for STAFF
								if ( (4933 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "www.goabroad.net")) ||
									(153 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "dev.goabroad.net")) ||
									(412 == $source && !strcasecmp($_SERVER['SERVER_NAME'], "goabroad.net.local")) ){
									// if from GoAbroadNetwork Team
									$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
											"VALUES (" . $messageID . ", " . $recipient . ", 1)";
								}
								else{
									$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
											"VALUES (" . $messageID . ", " . $recipient . ", 5)";
								}
								$this->mRs3->Execute($sql3);
							}
						}
					}

					// FROM STAFF TO GROUP
					else{
						// check if STAFF of recipient GROUP
						$isStaff = $dest->isStaff($_message->getSource());

						// STAFF
						if ($isStaff){
							$recipient = $dest->getSendableID();
							$travDestination = $dest->getAdministrator()->getTravelerID();
							$travSource = $_message->getSource()->getTravelerID();

							// message for ADMINISTRATOR
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messageID . ", " . $recipient . ", 5)";
							$this->mRs2->Execute($sql2);

							// get members: exclude ADMINISTRATOR
							$sql3 = "SELECT DISTINCT tblTraveler.sendableID " .
									"FROM tblGrouptoTraveler, tblTraveler " .
									"WHERE tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
									"AND tblGrouptoTraveler.groupID = " . $dest->getGroupID() . " " .
									"AND tblTraveler.travelerID <> " . $travSource . " " .
									"AND tblTraveler.travelerID <> " . $travDestination ;
							$this->mRs3-> Execute($sql3);

							while ($SendableID = mysql_fetch_array($this->mRs3->Resultset())){
								$recipient = $SendableID['sendableID'];

								$sql4 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messageID . ", " . $recipient . ", 1)";
								$this->mRs4->Execute($sql4);
								$_message->setAttributeID($this->mRs4->GetCurrentID());
							}
						}

						// NOT STAFF
						else{
							$recipient = $dest->getSendableID();

							// message for ADMINISTRATOR
							$sql2 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
									"VALUES (" . $messageID . ", " . $recipient . ", 5)";
							$this->mRs2->Execute($sql2);

							// get STAFF of GROUP
							$Staff = $dest->getStaff();

							foreach($Staff as $indStaff){
								$recipient = $indStaff->getSendableID();

								// message for STAFF
								$sql3 = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
										"VALUES (" . $messageID . ", " . $recipient . ", 5)";
								$this->mRs3->Execute($sql3);
							}
						}
					}
				}
			}

			$SentItems = new SentItems($this->mSendableId);
			$SentItems-> Store($messageID, $destination);
		}
		
		/**
		 * Function name: Delete
		 * @param integer $_attributeID
		 * Sets messageType of message with attributeID = $_attributeID to 2(Trash)
		 */
		 
		function Delete($_messageID = 0){

			// Update row in table tblMessages			
			$sql = "UPDATE tblMessageToAttribute SET trashed = 1, isRead = 0 WHERE messageID = " . $_messageID;
			$this->mRs->Execute($sql);
		}

		/***************************** START: SETTERS *************************************/
		function setInstance($_instance = 0){
			$this->mInstance = $_instance;
		}
		function setTotalRecords($_totalRecords = 0){
			$this->mTotalRecords = $_totalRecords;
		}
		function setMessageId($_messageID = 0){
			$this->mMessageId = $_messageID;
		}
		function setSendableId($_sendableID = 0){
			$this->mSendableId = $_sendableID;
		}
		/******************************* END: SETTERS *************************************/

		/***************************** START: GETTERS *************************************/
		function getTotalRecords(){
			return $this->mTotalRecords;
		}		
		function getDraftsList($_rowsLimit = NULL){

			$arrMessages = array();

			if (self::TRAVELER == $this->mInstance)
				$arrMessages = $this->getTravelerDraftBox($_rowsLimit);
			else
				$arrMessages = $this->getGroupDraftBox($_rowsLimit);

			return $arrMessages;
		}
		function getTravelerDraftBox($_rowsLimit = NULL){			
			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
				$travelerId = $Sendable->getTravelerID();
			}
			catch (exception $e){
				throw $e;
			}

			// Get DRAFT messages
			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ";
				$sql .= "(" .
						"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
						"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.trashed = 0 " .
						"AND tblMessageToAttribute.messageType = 3 " .
						"AND tblMessages.senderID = " . $this->mSendableId;

				// According to requirements, only TRAVELER has a DRAFTS FOLDER		-- NAGCHANGE NALIWAT!!!

				// if user is administrator of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
						"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 3 " .
						"AND tblMessageToAttribute.trashed = 0 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGroup " .
							"WHERE sendableID = tblMessages.senderID " .
							"AND isSuspended = 0 " .
							"AND administrator = " . $travelerId .
						")";
				// if user is staff of any group, include messages
				$sql .= " UNION " .
						"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
						"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
						"FROM tblMessages, tblMessageToAttribute " .
						"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
						"AND tblMessages.discriminator = 3 " .
						"AND tblMessageToAttribute.messageType = 3 " .
						"AND tblMessageToAttribute.trashed = 0 " .
						"AND EXISTS " .
						"(" .
							"SELECT sendableID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE sendableID = tblMessages.senderID " .
							"AND type = 1 " .
							"AND status = 1 " .
							"AND travelerID = " . $travelerId .
						")";
			$sql .= ") as allInbox GROUP BY messageID ORDER BY datecreated DESC  ". $queryLimit;
			$this->mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this->mRs2->Execute($sql2);

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_array($this->mRs->Resultset())){
				$message = new PersonalMessage();
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setMessageTypeId($rMessage['messageType']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));
				
				$message->setIsRead($rMessage['isRead']);

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setMessageRecipient();

				$arrMessage[] = $message;
			}

			return $arrMessage;
		}
		function getGroupDraftBox($_rowsLimit = NULL){			
			if ($_rowsLimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			// Get DRAFT messages
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, " .
				"tblMessages.senderID, tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
				"FROM tblMessages, tblMessageToAttribute " .
				"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
				"AND tblMessages.discriminator = 3 " .
				"AND tblMessageToAttribute.trashed = 0 " .
				"AND tblMessageToAttribute.messageType = 3 " .
				"AND tblMessages.senderID = " . $this->mSendableId . " " .
				"GROUP BY messageID " .
				"ORDER BY datecreated DESC  " . $queryLimit;
			$this->mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() AS totalRecords";
			$this->mRs2->Execute($sql2);

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_array($this->mRs->Resultset())){
				$message = new PersonalMessage();
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setMessageTypeId($rMessage['messageType']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));
		
				$message->setIsRead($rMessage['isRead']);

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setMessageRecipient();

				$arrMessage[] = $message;
			}

			return $arrMessage;
		}
		/******************************* END: GETTERS *************************************/

		static function isOwner($_attributeID = 0, $_sendableID = 0){
			try{
 				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($_sendableID);
				$travelerId = $Sendable->getTravelerID();
			}
			catch (exception $e){
				throw $e;
			}

			$sql = "SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessages.senderID = " . $_sendableID . " " .
					"AND tblMessageToAttribute.messageType = 3 " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeID;
			// if user is administrator of any group, include messages					
			$sql .= " UNION " .
					"SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.messageType = 1 " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
					"AND EXISTS " .
					"(" .
						"SELECT sendableID " .
						"FROM tblGroup " .
						"WHERE sendableID = tblMessages.senderID " .
						"AND isSuspended = 0 " .
						"AND administrator = " . $Sendable->getTravelerID() .
					")";
			// if user is staff of any group, include messages
			$sql .= " UNION " .
					"SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.messageType = 1 " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
					"AND EXISTS " .
					"(" .
						"SELECT sendableID " .
						"FROM tblGrouptoFacilitator " .
						"WHERE sendableID = tblMessages.senderID " .
						"AND type = 1 " .
						"AND status = 1 " .
						"AND travelerID = " . $travelerId .
					")";
			$mRs-> Execute($sql);

			if (0 < $mRs->Recordcount())
				return true;
			else
				return false;
		}
	}
?>