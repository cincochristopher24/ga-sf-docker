<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: 
 */
     
    require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("travellog/model/Class.PathManager.php");
	require_once("Class.UploadManager.php");
	
    class ResourceFiles{
        
        /**
         * Define variables for Class Member Attributes
         */        
            private $mConn         = NULL;
            private $mRs           = NULL;
           
           
 			// var for context parameter (parameter is an object)
 			private $mcontext		 = NULL;
           
            private $mResFileID    = NULL;
            private $mType         = NULL;
            private $mFilename     = NULL;
            private $mCaption		= NULL;
            private $mResFileType  = NULL;
            private $mprivacypreference = 0;
            private $mUploadedBy   = NULL;
			private $mBatchUploadID = 0;
           
            public static $GROUP   	    = 1;
            public static $RESUME 	    = 2;
              
        /**
         * Constructor Function for this class
         */
            function ResourceFiles($resfileID = 0){
                                
                $this->mResFileID = $resfileID;
                
                try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mResFileID){
                    
                    $sql = "SELECT * FROM tblResourceFile WHERE resourcefileID = '$this->mResFileID' ";
                    $this->mRs->Execute($sql);
                
                    if (0 == $this->mRs->Recordcount()){
                        throw new Exception("Invalid ResourceFileID");        // ID not valid so throw exception
                    }
                    
                    // Sets values to its class member attributes.
                    $this->mType      			= $this->mRs->Result(0,"type");           
                    $this->mFileName  			= stripslashes($this->mRs->Result(0,"filename"));
                    $this->mCaption  			= stripslashes($this->mRs->Result(0,"caption"));
                    $this->mResFileType  		= stripslashes($this->mRs->Result(0,"resourcefiletype"));
                    $this->mprivacypreference	= stripslashes($this->mRs->Result(0,"privacypreference"));
					$this->mUploadedBy          = $this->mRs->Result(0,"uploadedBy");
					$this->mBatchUploadID		= $this->mRs->Result(0,"batchUploadID");
                }
            }
                  
                           
        /**
         * Setter Functions
         */ 
            function setResourceFileID($resFileID){
               $this->mResFileID = $resFileID;
            }
            
            function setTheType($type){
               $this->mType = $type;
            }
           
            function setFileName($fileName){
               $this->mFileName = $fileName;
            }
            
            function setCaption($caption){
           		$this->mCaption = $caption;
            }
            
            function setResourceFileType($resfileType){
               $this->mResFileType = $resfileType;
            }
            
            function setPrivacypreference($privacypreference){
            	$this->mprivacypreference = $privacypreference;
            }
            
            function setUploadedBy($uploadedBy){
            	$this->mUploadedBy = $uploadedBy;
            }

			function setBatchUploadID($id){
				$this->mBatchUploadID = $id;
			}
           
           function setContext($context){
           		$this->mcontext = $context;
           }
           
           function getContext(){
           		return $this->mcontext;
           }
           
        /**
         * Getter Functions
         */    
            function getResourceFileID(){
                return $this->mResFileID;
            }
            
            function getTheType(){
                return $this->mType;
            }
           
            function getFileName(){
                return $this->mFileName;
            }
            
            function getCaption(){
            	return $this->mCaption;	
            }
            
            function getResourceFileType(){
                return $this->mResFileType;
            }
      		
      		function getPrivacypreference(){
            	return $this->mprivacypreference;
            }
            
            function getUploadedBy(){
            	return $this->mUploadedBy;
            }

			function getBatchUploadID(){
				return $this->mBatchUploadID;
			}
      		
        
        /**
         * CRUD Methods (Create, Update , Delete)
         */
            function Create(){
                
                $Atype     = $this->mType;
                $Afilename = addslashes($this->mFileName);
                $Acaption = addslashes($this->mCaption);
                
                $sql = "INSERT into tblResourceFile (type, filename, caption,  resourcefiletype, privacypreference, uploadedBy, batchUploadID) " .
                        "VALUES ('$Atype', '$Afilename', '$Acaption', '$this->mResFileType', '$this->mprivacypreference', '$this->mUploadedBy', '$this->mBatchUploadID')";
                $this->mRs->Execute($sql);       
                $this->mResFileID = $this->mRs->GetCurrentID();
                                
                //$this->mcontext->addResourcefile($this);
                   
            }
            
            function Update(){
                
                $Atype     = $this->mType;
                $Afilename = addslashes($this->mFileName);
                $Acaption = addslashes($this->mCaption);
                 
                $sql = "UPDATE tblResourceFile SET  filename = '$Afilename', caption = '$Acaption', privacypreference='$this->mprivacypreference'  WHERE resourcefileID = '$this->mResFileID' ";
                $this->mRs->Execute($sql);
            }
            
            function Delete(){
                	
                	$delete = false;
                	
                	if(get_class($this->mcontext) == "AdminGroup"){
	                	//to do: if admingroup select first from tbgrouptoresourcefile if more than 1 if 1 delete all including files
	                	$sql = "SELECT * FROM tblGrouptoResourceFile WHERE resourcefileID = '$this->mResFileID' ";       
		                $this->mRs->Execute($sql);
                		
	                	if($this->mRs->RecordCount() == 1){
	                		$delete = true;
	                	}	
                	}else{
						$delete = true;		                		
                	}   		
            		
            		$this->mcontext->removeResourceFile($this->mResFileID);
            		
            		if($delete){
        				
						$path = new PathManager($this->mcontext);
						$uploadmanager = new UploadManager();	 			
						$uploadmanager->setDestination($path);
						
						$uploadmanager->setTypeUpload('PDF');											
						$uploadmanager->deletefile($this->getFileName());
		                
		                $sql = "DELETE FROM tblResourceFile WHERE resourcefileID = '$this->mResFileID' ";       
		                $this->mRs->Execute($sql);
            
            		}
            		
            }
            
            /**
             * get resource file link/path 
             * by: Joel - sept 22 06
             */
            
            function getResourcefileLink(){
            		
		 		if (0 < $this->mResFileID){
		 			
		 			try {
		 				 $path = new PathManager($this->mcontext,"resourcefiles");
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$fullpath =  $path->GetPathRelative() . $this->mFileName;
		 							 			
		 			if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$fullpath)){	// if file to be displayed exists in URL. then proceed to display 
		 				return $fullpath;
		 			}else{		 				
		 				return "no file exist";
		 			}			 			
		 		}
            }
            
            function getFullPathInfo(){
            	
            	try{
		 			$path = new PathManager($this->mcontext,"resourcefiles");
		 		}
					catch (Exception $e) {				   
					throw $e;
				}            	
            	return $path->GetPathRelative();;
            }
            
            function getAssignedGroups(){
            	
            	$grp = array();
            	
            	$sql = "SELECT * FROM tblGrouptoResourceFile WHERE resourcefileID = '$this->mResFileID' ";       
		        $this->mRs->Execute($sql);
            	
            	if($this->mRs->Recordcount()){
            	    require_once("travellog/model/Class.AdminGroup.php");
            	    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
	                   try {
	                       $group = new AdminGroup($recordset['groupID']);
	                   }
	                   catch (Exception $e) {
	                      throw $e;
	                   }
	                   $grp[] = $group;
	                }
            	}
            	
            	return $grp;
            	
            }
            
            function getAssignedTemplates(){
            	require_once('vendor/autoload/GanetAutoloader.php');

				GanetAutoloader::getInstance()->register();
				GanetPropelRuntime::getInstance()->initPropel();
            	
            	$resourceFile = GanetResourceFilePeer::retrieveByPk($this->mResFileID);
            	
            	$tpls = array();
            	foreach( $resourceFile->getGanetTemplateResourceFiles() as $templateResourceFile ){
            		$tpls[] = $templateResourceFile->getGanetGroupTemplate();
            	}
            	
            	return $tpls;
            }
            
            function getFileSize(){
            	$path = $this->getResourcefileLink();
            	if('no file exist' == $path)
            		return '0 B';
            	$units = array('B', 'KB', 'MB');
            	$bytes = max(filesize($path), 0);
    			$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    			$pow = min($pow, count($units) - 1);
    			$bytes /= pow(1024, $pow);
				$point = (1 < $pow)? 2 : 0;
				
				return round($bytes, $point) . ' ' . $units[$pow];
            }
            
            function getUploadedByTraveler(){
            	require_once("travellog/model/Class.TravelerProfile.php");
            	
            	return new TravelerProfile($this->mUploadedBy);
            }
            
            function getUploaderName(){
            	return $this->getUploadedByTraveler()->getUsername();
            }
            
            function getUploaderLink(){
            	return 'http://'.$_SERVER['SERVER_NAME'].'/'.$this->getUploaderName();
            }

			public static function getResourcefileByFilename($filename=""){
				$filename = "'".addslashes($filename)."'";
				$sql = "SELECT * FROM tblResourceFile WHERE filename = $filename LIMIT 0,1";
				try{
                    $conn = new Connection();
                    $rs   = new Recordset($conn);
					$rs->Execute($sql);
					if( 0 < $rs->Recordcount() ){
						return new ResourceFiles($rs->Result(0,"resourcefileID"));
					}else{
						return FALSE;
					}
                }catch(Exception $e){                 
                   return FALSE;
                }
			}
                   
    }
    
?>
