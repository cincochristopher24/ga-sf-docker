<?php
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");
	
	class Tags{
		
		// constants for contexts; add as need arises
		const ARTICLE = 0;
		
		private $mConn = null,
				$mDB   = null,
				$mRs   = null,
				
				//vars for tbl fields
				$mContextID	= null,
				$mContext  	= null,
				$mTagString	= null,
				$mTagArray	= array();
		
		function Tags(){
			try {
	 			$this->mConn = new Connection();
				$this->mRs   = new Recordset($this->mConn);
			}
			catch (Exception $e) {				   
			   throw $e;
			}
		}
		
		function setContextID($contextID){
			$this->mContextID = $contextID;
		}
		
		function setContext($context){
			$this->mContext = $context;
		}
		
		function setTagString($tag){
			$this->mTagString = $tag;
		}
		
		function setTagArray($tagarray){
			$this->mTagArray = $tagarray;
		}
		
		function getContextID(){
			return $this->mContextID;
		}
		
		function getContext(){
			return $this->mContext;
		}
		
		function getTagString(){
			return $this->mTagString;
		}
		
		function getTagArray(){
			return $this->mTagArray;
		}
		
		function Create($tag){
			$tag = mysql_real_escape_string($tag);
			
 			$sql = "INSERT into tblTags (contextID, context, tag) " .
 					"VALUES ('$this->mContextID', '$this->mContext', '$tag')";
 			$this->mRs->Execute($sql);		
 			$this->mArticleID = $this->mRs->GetCurrentID();
 		}

		// deletes using contextID
		function DeleteByBatch(){
			$sql = "DELETE FROM tblTags WHERE contextID = $this->mContextID AND context = $this->mContext";
			$this->mRs->Execute($sql);
			$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;
		}
		
		function Save(){
			if(strlen(trim($this->mTagString)) > 0){
				$this->mTagArray = explode(',', $this->mTagString);
				
				$this->DeleteByBatch();
				foreach($this->mTagArray as $tag){				
					$this->Create(trim($tag));
				}
			}else
				$this->DeleteByBatch();
			$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;	
		}
		
		function getTagsByContextID($contextID){
			$sql = "SELECT tag FROM tblTags WHERE contextID = $contextID AND context = $this->mContext";
			$this->mRs->Execute($sql);
			
			while ($row = mysql_fetch_assoc($this->mRs->Resultset())) {
				$this->mTagArray[] = $row['tag'];
			}
			
			if(count($this->mTagArray))
				$this->mTagString = implode(',',$this->mTagArray);
		}
		
		function getTagsByContext($context){
			$sql = "SELECT tag FROM tblTags WHERE context = $context";
			$this->mRs->Execute($sql);
			
			while ($row = mysql_fetch_assoc($this->mRs->Resultset())) {
				$this->mTagArray[] = $row['tag'];
			}
			
			if(count($this->mTagArray))
				$this->mTagString = implode(',',$this->mTagArray);
		}
		
		function tagsToString($tagarray){
			if(!count($tagarray)) return "";			
			return (implode(',', $tagarray));
		}
	}