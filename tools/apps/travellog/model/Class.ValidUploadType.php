<?php

/**
 * Created on Jul 31, 2006
 * @author Joel C. LLano
 * Purpose: abstracts different types of Valid files for upload
 */
	
Class ValidUploadType {

	public static $JPEG 	= "image/jpeg";
	public static $GIF		= "image/gif";
	public static $PNG		= "image/png";
	public static $PDF		= "application/pdf";

}



?>