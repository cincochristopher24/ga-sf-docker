<?php
	require_once('Class.Decorator.php');
	require_once('Cache/ganetCacheProvider.php');
	
	class SubNavigation{
		protected $mContext = null;
		protected $mLinkToHighlight = null;
		protected $mContextID = null;
		protected $mGroupType = null;
		protected $mDoNotShow = false;
		protected $mHiddenLinks = array();
		protected $mCustomLinks = array();
		protected $mAlteredLinks = array();
		protected $mAlteredUrls	= array();
		protected $mCache	=	null;
		
		public function SubNavigation(){
			$this->mCache	=	ganetCacheProvider::instance()->getCache();
			   
		} 
		
		public function setContext($context=null){
			$this->mContext = $context;
		}
		
		public function setLinkToHighlight($linkToHighlight=null){
			$this->mLinkToHighlight = $linkToHighlight;
		}
		
		public function setContextID($contextID=null){
			$this->mContextID = $contextID;
		}
		
		public function setGroupType($groupType){
			$this->mGroupType = $groupType;
		}
		
		public function setDoNotShow($doNotShow=true){
			$this->mDoNotShow = $doNotShow;
		}
		
		public function getDoNotShow(){
			return $this->mDoNotShow;
		}
		
		public function addHiddenLink($linkConst){
			$this->mHiddenLinks[] = $linkConst;
		}

		public function clearHiddenLinks(){
			$this->mHiddenLinks = array();
		}
		
		public function setLinkCaption($linkConst,$caption){
			$this->mAlteredLinks[$linkConst] = $caption;
		}

		public function setLinkUrl($linkConst,$url){
			$this->mAlteredUrls[$linkConst] = $url;
		}
		
		public function clearAlteredLinkCaptions(){
			$this->mAlteredLinks = array();
		}
		
		public function addCustomLink($customLink = null){
			if(!isset($customLink['TEXT'])){
				throw new exception('TEXT is required in as a key in an associative array passed to method addCustomLink of object of type SubNavigation.');
			}
			elseif(!isset($customLink['URL'])){
				throw new exception('URL is required in as a key in an associative array passed to method addCustomLink of object of type SubNavigation.');
			}
			$customLink['IS_HIGHLIGHTED'] = (isset($customLink['IS_HIGHLIGHTED']) && $customLink['IS_HIGHLIGHTED'] == true)?true:false;			
			$this->mCustomLinks[] = $customLink;			
		}
		
		public function clearCustomLinks(){
			$this->mCustomLinks = array();
		}
		
		public function show(){
			// Remove temporarily, caching in subnavigation
			
			//depending on the context, initialize the appropiate class
			if(!$this->mDoNotShow && !is_null($this->mContext)){
				
				// find if there is a cached content for subnavigation
				/*if ($this->mCache != null){
					$cacheKey	=	$this->getCacheKey();	
					
					if ($content = $this->mCache->get($cacheKey)){
						//echo 'got ' . $cacheKey . ' from cache <br/>';
						echo $content;
						return;
					}
				}*/
				
				$vars = array(
					'context' 			=> $this->mContext,
					'linkToHighlight' 	=> $this->mLinkToHighlight,
					'contextID'			=> $this->mContextID,
					'groupType'			=> $this->mDoNotShow,
					'hiddenLinks'		=> $this->mHiddenLinks,
					'customLinks'		=> $this->mCustomLinks,
					'alteredLinks'		=> $this->mAlteredLinks,
					'alteredUrls'		=> $this->mAlteredUrls
				);
				
				if('GROUP' == $this->mContext){
					require_once('travellog/model/Class.GroupSubNavigation.php');
					$subNavigation = new GroupSubNavigation($vars);
				}
				else{
					require_once('travellog/model/Class.TravelerSubNavigation.php');
					$subNavigation = new TravelerSubNavigation($vars);
				}
				
				//if ($this->mCache == null) {
					$subNavigation->show();					// no cache available so proceed w/normal processing
				//} else {
					// well buffer html content of subnavigation and store it in cache for 30 min
				/*	ob_start();
					
					$subNavigation->show();
					
					$cachedContent	=	ob_get_contents();
					
					$this->mCache->set($this->getCacheKey(),$cachedContent,array('EXPIRE'=>1800) );
					
					ob_end_flush();
				}*/
				
			}
		}
		
		
		protected function getCacheKey(){
			require_once('travellog/model/Class.SessionManager.php');
			require_once('travellog/model/Class.SiteContext.php');
			//$key = 'SubNavigation_' . $this->mContext . '_' . $this->mContextID . '_' . $this->mLinkToHighlight . '_' . SessionManager::getInstance()->get('travelerID');	// just a hack to get a traveler id to create a unique key for caching
			//Added domain ID to make it unique in cobrand context
			$key = 	'SubNavigation_' . $this->mContext . '_' . $this->mContextID . 
					'_' . $this->mLinkToHighlight . '_' .SessionManager::getInstance()->get('travelerID').
					'_DOMAINID_'.SiteContext::getInstance()->getGroupID();
			return $key;
		}
		
		
		
	}
?>