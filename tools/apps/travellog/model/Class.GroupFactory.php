<?php
	/**
	 * @(#) Class.Group.php
	 * 
	 * @package goabroad.net
	 * @subpackage model
	 * 
	 * @author Czarisse Daphne P. Dolina
	 * @version 1.0 September 14, 2006
	 * @version 2.0 October 22, 2008 Cheryl Ivy Q. Go
	 * @version 3.0 February 18, 2009 Antonio Pepito Cruda Jr.
	 */

	require_once("Class.dbHandler.php");
	require_once("Class.SqlResourceIterator.php");
	require_once("travellog/model/Class.Group.php");
	require_once("travellog/model/Class.FunGroup.php");
	require_once("travellog/model/Class.AdminGroup.php");
	require_once("travellog/model/Class.GanetDbHandler.php");
	
	require_once('Cache/ganetCacheProvider.php');
	
	/**
	 * Factory for models that extends the Group class. This also serves as a storage
	 * of instantiated groups so that when you instantiate a group using an ID, this will
	 * class will check first if that group has already been instantiated and it will return
	 * the instance of the class if it has already instantiated. If not this instantiates the
	 * group and returns the instance of the group.
	 */
  class GroupFactory {
		
		private $gIDs			=	array();	// an array of group ids held, used to retrieve group objects in cache
		
		/**
		 * The repository of instantiated groups.
		 * 
		 * @var array
		 */		
		private $mRepository = array();
		
		/**
		 * The instance of this class.
		 * 
		 * @staticvar GroupFactory 
		 */
		private static $sInstance = null;
		
		/**
		 * Constructs the GroupFactory object.
		 */
		private function __construct(){
		}

 		/**
	 	 * Returns the instance of this class.
	 	 * 
	 	 * @return GroupFactory Returns the instance of this class.
	 	 */
	 	public static function instance(){
	 		if (is_null(self::$sInstance)){
	 			self::$sInstance = new GroupFactory();
	 		}

	 		return self::$sInstance;
	 	}

 		/**
 		 * Returns the correct objects of the given group IDs. For example, when you pass
 		 * an ID that is a Club, this returns an instance of the FunGroup and when it is an
 		 * admin group, this returns an instance of the AdminGroup. 
	 	 * 
	 	 * @throws exception Throws exception when there is an sql error.
	 	 * 
	 	 * @return array Returns an array of objects(AdminGroup, FunGroup) created from the group IDs.
	 	 */
	 	function create(array $_arrGroupId, $_filter = null){
	 		$groups = array();
			if ($_filter != null)
				$filterHash =	$_filter->getHash();
			else
				$filterHash = 	'';
		
			if (0 < count($_arrGroupId)) {
				if (!is_null($_filter)) {
					require_once "travellog/model/Class.Condition.php";

					$mCondition = $_filter->getConditions();	
					$mBooleanOp = $_filter->getBooleanOps();
					$mAttName = $mCondition[0]->getAttributeName();                    			
					$mValue	= $mCondition[0]->getValue();

					switch($mCondition[0]->getOperation()){        				
						case FilterOp::$ORDER_BY_DESC:
							$mFilterStr = " ORDER BY " . $mAttName . " DESC ";
							break;
						default:
							$mFilterStr = " ORDER BY " . $mAttName . " ASC ";
					}
				}
				else {
					$mFilterStr = " ORDER BY name ASC ";
				}
				
				$ids = "";
				foreach($_arrGroupId as $id){
					// clean-up id
					if (filter_var($id, FILTER_VALIDATE_INT)) {
						if (isset($this->mRepository[$id])) {
							$groups[] = $this->mRepository[$id];
						}
						else {
							$ids .= $id.",";
						}	
					}
				}				
				$ids = preg_replace("/,$/", "", $ids);
				
				try {
					if (!empty($ids)) {
						$handler = new dbHandler();
						
						$sql = "SELECT '0' as clientID, a.* FROM tblGroup as a " .
							" WHERE groupID NOT IN " .
							"	(" .
							"		SELECT DISTINCT groupID FROM tblGrouptoAdvisor as b " .
							"		WHERE b.groupID IN ($ids)" .
							"	) " .
							"	AND a.groupID IN ($ids) " .
							" UNION " .
							" SELECT DISTINCT b.clientID, a.* FROM tblGroup as a, tblGrouptoAdvisor as b" .
							"	WHERE a.groupID = b.groupID " .
							" AND a.groupID IN ($ids) ";
						$sql = "SELECT * FROM ($sql) as temp ".$mFilterStr;
						
						$resource = $handler->execute($sql);

						while ($row = mysql_fetch_assoc($resource)) {
							$groups[] = $this->createGroupObject($row);
						}
					}
				}
				catch(exception $ex){
					throw $ex;
				}
			}

			return $groups;
		}
		
 		/**
 		 * Returns the groups that has name equal to the given group name.
	 	 * 
	 	 * @param array $name The group name to be searched.
	 	 * @param boolean $subGroupIncluded If this is true the group name will also be tested to subgroups.
	 	 * 
	 	 * @return array the groups that has name equal to the given group name.
	 	 */
	 	function createGroupsUsingName($name, $subGroupIncluded = false){
	 		$groups = array();
			
			if (is_string($name)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					
					$sql = "SELECT '0' as clientID, a.* FROM tblGroup as a " .
						" WHERE name = ".$handler->makeSqlSafeString($name);
					$sql .= ($subGroupIncluded) ? "" : " AND `parentID` = 0 ";
					$sql .= " UNION " .
						" SELECT DISTINCT b.clientID, a.* FROM tblGroup as a, tblGrouptoAdvisor as b" .
						"	WHERE a.groupID = b.groupID " .
						" AND `name` = ".$handler->makeSqlSafeString($name);
					$sql .= ($subGroupIncluded) ? "" : " AND `parentID` = 0 ";
					$sql = "SELECT * FROM ($sql) as temp ";
					
					$resource = $handler->execute($sql);
					
					while ($row = mysql_fetch_assoc($resource)) {
						$groups[] = $this->createGroupObject($row);
					}
				}
				catch(exception $ex){}
			}

			return $groups;
		}
		
		/**
		 * Instantiates and initializes the groups attributes. This returns the correct object
		 * of the given array argument.
		 * 
		 * @throws exception when key 'discriminator' is not found in the array.
		 * 
		 * @param array $props The properties of the group object created.
		 * 
		 * @return FunGroup|AdvisorGroup|NodeGroup|null the correct instance of the group given the groups properties. Returns null if the group discriminator is not valid.
		 */
		function createGroupObject($props){
			if (!isset($this->mRepository[$props['groupID']])) {
				$cache	=	ganetCacheProvider::instance()->getCache();
				
				switch($props['discriminator']){
					case Group::FUN:
						$this->mRepository[$props['groupID']] = new FunGroup();
						$this->mRepository[$props['groupID']]->setFunGroupData($props);
					break;
					case Group::ADMIN:
						$this->mRepository[$props['groupID']] = new AdminGroup($props);							
					break;
					case Group::NODE_GROUP:
						$this->mRepository[$props['groupID']] = new AdminGroup($props);										
					break;
				}
			}
			
			return $this->mRepository[$props['groupID']];					
		}
		
		/**
		 * Empties the repository of groups.
		 * 
		 * @return void
		 */
		function emptyRepository(){
			$keys = array_keys($this->mRepository);
			
			foreach($keys as $key) {
				unset($this->mRepository[$key]);
			}
			unset($this->mRepository);
			$this->mRepository = array();
		}
		
		
		/**
		 * prepares the cache if available and load the groups cached to prevent sql queries if possible
		 *   kgordo 4/6/09 
		 */ 
		/*function prepareCachedGroups($filterHash){
				$this->cache	=	ganetCacheProvider::instance()->getCache();
				if (isset($this->cache)){
				
					// retrieve array of group ids stored in cache
					$this->gIDs 	=	 $this->cache->get('GroupFactoryIDs_' . $filterHash);
					
					if ($this->gIDs){
						// retrieve the group objects in bulk (multi-gets)
						$groupKeys = array();
						foreach ($this->gIDs as $id){
							$groupKeys[] = 'Group_' . $id;
						}
						
						$groups	=	$this->cache->get($groupKeys);
						if ($groups){
							foreach($groups as $group){
								$this->mRepository[$group->getGroupID()]	=	$group;
								//echo "got group " . $group->getName() . " from cache <br/>";		
							}					
						}					
					} 
				} else {
					$this->cacheConnected = false;
				}
		
				
				
		}*/
		
	}