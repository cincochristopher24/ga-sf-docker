<?php
	require_once('Class.DataModel.php');
	require_once('travellog/model/DiscussionBoard/Class.Poster.php');
	require_once('travellog/model/DiscussionBoard/Class.Discussion.php');
	require_once('travellog/model/DiscussionBoard/Class.FactoryResource.php');
	require_once('Class.GaDateTime.php');
	
	class Post extends DataModel {
		function __construct($postID=null,$data=null){
			parent::initialize('DiscussionBoard', 'tblPosts');
			if($postID and !$this->load($postID)){
				throw new exception('Invalid Post ID');
			}
			if(is_array($data)){
				$this->setFields($data);
			}
			if(!$this->getID()){
				$this->setDateCreated(GaDateTime::dbDateTimeFormat());
			}
		}
		
		/**
		 * Returns the creation date of the post.
		 * 
		 * @param string $format The format for the date returned. Follows the php date formatting.
		 * 
		 * @return string the creation date of the post.
		 */
		function getDateCreated($format = "Y-m-d H:i:s"){
			return date($format, strtotime($this->mFields['dateCreated']));
		}

		public function getPoster(){
			return Poster::createInstance($this->getPosterID(),$this);
		}
		
		public function getParentDiscussion(){
			return Discussion::createInstance($this->getDiscussionID());
		}
		
		public function getUrl(){
			return $this->getParentDiscussion()->getUrl().'&latest'.'#'.$this->getID();
		}
		
		public function getMessage(){
			if($this->getID() and $this->getPoster()->isSuspended()){
				return 'The author of this post has been banned from the site. The post will no longer be seen in this discussion. If you have questions, <a href="/feedback.php">contact us</a>.';
			}
			return parent::executeMethod('getMessage');
		}
		
		/** Factory Methods **/
		static public function getInstance($postID=null,$data=null){
			return new Post($postID,$data);
		}
		
		static public function getInstances($options){
			$db = new dbHandler('db=DiscussionBoard');
			$def = array(
				'GROUP_ID'			=> null,
				'TOPIC_ID' 			=> null,
				'TOPIC_PREF'		=> null,
				'TOPIC_STATUS'		=> null,
				'DISCUSSION_ID'		=> null,
				'DISCUSSION_STATUS'	=> null,
				'POSTER_ID'			=> null,
				'ORDER_BY'			=> 'ID ASC',
				'LIMIT'				=> null
			);
			$options = array_merge($def,$options);
			$sql = 	'SELECT SQL_CALC_FOUND_ROWS tblPosts.* '.
					'FROM tblPosts, tblDiscussions, tblTopics '.
					'WHERE tblPosts.discussionID = tblDiscussions.ID '.
					'AND tblDiscussions.topicID = tblTopics.ID ';
			if(!is_null($options['DISCUSSION_ID'])){
				$sql .= ' AND tblDiscussions.ID = '.$db->makeSqlSafeString($options['DISCUSSION_ID']);
			}
			if(!is_null($options['GROUP_ID'])){
				$sql .= ' AND tblTopics.groupID = '.$db->makeSqlSafeString($options['GROUP_ID']);
			}
			if(!is_null($options['TOPIC_ID'])){
				$sql .= ' AND tblTopics.ID = '.$db->makeSqlSafeString($options['TOPIC_ID']);
			}
			if(!is_null($options['TOPIC_PREF'])){
				$sql .= ' AND tblTopics.privacySetting <= '.$db->makeSqlSafeString($options['TOPIC_PREF']);
			}
			if(!is_null($options['TOPIC_STATUS'])){
				$sql .= ' AND tblTopics.status = '.$db->makeSqlSafeString($options['TOPIC_STATUS']);
			}
			if(!is_null($options['DISCUSSION_ID'])){
				$sql .= ' AND tblDiscussions.ID = '.$db->makeSqlSafeString($options['DISCUSSION_ID']);
			}
			if(!is_null($options['DISCUSSION_STATUS'])){
				$sql .= ' AND tblDiscussions.status = '.$db->makeSqlSafeString($options['DISCUSSION_STATUS']);
			}
			if(!is_null($options['POSTER_ID'])){
				$sql .= ' AND posterID = '.$db->makeSqlSafeString($options['POSTER_ID']);
			}
			$sql .= ' ORDER BY '.$options['ORDER_BY'];
			if(!is_null($options['LIMIT'])){
				$sql .= ' LIMIT '.$options['LIMIT'];
			}
			//var_dump($sql);
			$rs = new iRecordset($db->execute($sql));
			//$rs->dump();
			$totalCount = $rs->retrieveRecordCount();
			if(!is_null($options['LIMIT'])){
				$rsTotal = new iRecordset($db->execute('SELECT FOUND_ROWS() as total'));
				//$rsTotal->dump();
				$totalCount = $rsTotal->getTotal();
			}
			
			$ar = array();
			foreach($rs as $row){
				$ar[] = new self(null,$row);
			}
			return FactoryResource::create(array(
				'instances' => $ar,
				'realTotal'	=> $totalCount
			));
		}
		
		//added by Jul overriding Save to accomodate creation of feed for this post 
		public function save(){
			$isNew = !$this->getID();
			parent::save();
			if( $isNew ){
				require_once("travellog/model/Class.ActivityFeed.php");
				try{
					ActivityFeed::create($this);
				}catch(exception $e){}
			}
		}
		
		public function delete(){
			$postID = $this->getID();
			parent::delete();
			try{
				$conn = new Connection();
				$rs = new Recordset($conn);
				$db = new dbHandler();
				require_once("travellog/model/Class.ActivityFeed.php");
				$DISCUSSIONPOST = ActivityFeed::DISCUSSIONPOST;
				$sql = "DELETE FROM tblActivityFeed
						WHERE feedSection = $DISCUSSIONPOST
							AND sectionID = $postID";
				$db->execute($sql);		
			}catch(exception $e){}
		}
	}
?>