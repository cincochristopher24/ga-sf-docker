<?php
	require_once('Class.DataModel.php');
	require_once('travellog/model/DiscussionBoard/Class.DiscussionBoard.php');
	require_once('travellog/model/DiscussionBoard/Class.Discussion.php');
	require_once('travellog/model/DiscussionBoard/Class.FactoryResource.php');
	require_once('travellog/model/Class.GroupFactory.php');
	
	class Topic extends DataModel {
		/*** Constants For Topic Status ***/
		const ACTIVE_STATUS 	= 1;
		const ARCHIVED_STATUS 	= 2;
		
		static private $TOPIC_STATUS = array(
			self::ACTIVE_STATUS => 'Active',
			self::ARCHIVED_STATUS => 'Archived'
		);
		
		/** The constant for public view and the members of the parent group can add. **/
		const PUBLIC_SETTING = 1;
		
		/** The constant for only members of the parent group can view and add. **/
		const MEMBERS_ONLY_SETTING = 2;
		
		/** The constant for public view and the members of the subgroup can add. **/
		const SUBGROUP_PUBLIC_SETTING = 3;
		
		/** The constant for only members of the subgroup can view and add. **/
		const SUBGROUP_MEMBERS_ONLY_SETTING = 4;

		static private $PRIVACY_SETTINGS = array(
			self::MEMBERS_ONLY_SETTING,
			self::PUBLIC_SETTING
		);
		
		/**
		 * The number of dicussions of the topic.
		 * 
		 * @var integer
		 */
		private $mDiscussionsCount = null;
		
		/**
		 * The featured discussions of the topic.
		 * 
		 * @var array
		 */
		private $mFeaturedDiscussions = null;
		
		/**
		 * The unfeatured discussions of the topic.
		 * 
		 * @var array
		 */
		private $mUnfeaturedDiscussions = null;
		
		function __construct($keyID = null, $data = null){
			//parent::initialize('DiscussionBoard', 'tblTopics');
			parent::initDataModel(array(
				self::DATABASE 	=> 'DiscussionBoard',
				self::TABLE		=> 'tblTopics',
				self::PROTECTED_ATTRIBUTES => array(
					'dateCreated'
				)
			));
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid Topic ID');
			}
			if(is_array($data)){
				$this->setFields($data);
			}
			/** Set default values if new Record **/
			if(!$this->getID()){
				$this->setStatus(self::ACTIVE_STATUS);
				$this->setPrivacySetting(self::PUBLIC_SETTING);
				$this->setDateCreated(GaDateTime::dbDateTimeFormat());
			}
		}
		
		/**
		 * Adds a featured discussion of the topic.
		 * 
		 * @param Discussion The discussion to be added to the list of featured discussions of the topic.
		 * 
		 * @return void
		 */
		function addFeaturedDiscussion(Discussion $discussion){
			$this->mFeaturedDiscussions[$discussion->getID()] = $discussion;
		}
		
		/**
		 * Adds an unfeatured discussion of the topic.
		 * 
		 * @param Discussion The discussion to be added to the unfeatured discussions of the topic.
		 * 
		 * @return void
		 */
		function addUnfeaturedDiscussion(Discussion $discussion){
			$this->mUnfeaturedDiscussions[$discussion->getID()] = $discussion;
		}
		
		public function getDateCreated(){
			return $this->executeMethod('getDateCreated',array());
		}
		
		public function getStatusLabel(){
			return self::$TOPIC_STATUS[$this->getStatus()];
		}
		
		public function getDiscussions($limit=null){
			$options = array(
				'TOPIC_ID' => $this->getID()
			);
			if(!is_null($limit)){
				$options['limit'] = '1';
			}
			return Discussion::getInstances($options);
		}
		
		/**
		 * Returns the number of topic's discussions.
		 * 
		 * @return integer the number of topic's discussions.
		 */
		function getDiscussionsCount(){
			if (is_null($this->mDiscussionsCount)) {
				require_once("Class.Criteria2.php");
				require_once("travellog/model/Class.DiscussionPeer.php");
				
				$criteria = new Criteria2();
				$criteria->add(DiscussionPeer::TOPIC_ID, $this->getID());
				
				$this->mDiscussionsCount = DiscussionPeer::doCount($criteria);
			}
			
			return $this->mDiscussionsCount;
		}
		
		public function getActiveDiscussions(){
			return Discussion::getInstances(array(
				'TOPIC_ID' 	=> $this->getID(),
				'STATUS'	=> Discussion::ACTIVE_STATUS
			));
		}
		
		public function getLatestPosts($limit=null){
			return Post::getInstances(array(
				'TOPIC_ID' 	=> $this->getID(),
				'ORDER_BY'	=> 'tblPosts.ID DESC',
				'LIMIT'		=> $limit
			));
		}
		
		public function getParentGroup(){
			$mGroup = GroupFactory::instance()->create( array($this->getGroupID()) );
			return $mGroup[0];
		}
		
		public function getDiscussionBoard(){
			return DiscussionBoard::getInstanceByGroup($this->getParentGroup());
		}
		
		/**
		 * Returns the featured discussions of the topic.
		 * 
		 * @return array the featured discussions of the topic.
		 */
		function getFeaturedDiscussions(){
			if (is_null($this->mFeaturedDiscussions)) {
				require_once("Class.Criteria2.php");
				require_once("travellog/model/Class.DiscussionPeer.php");
				
				$criteria = new Criteria2();
				$criteria->add(DiscussionPeer::TOPIC_ID, $this->getID());
				$criteria->add(DiscussionPeer::IS_FEATURED, '1');
				
				$this->mFeaturedDiscussions = DiscussionPeer::doSelect($criteria);
			}
			
			return $this->mFeaturedDiscussions;
		}
		
		/**
		 * Returns the unfeatured discussions of the topic.
		 * 
		 * @return array the unfeatured discussions of the topic.
		 */
		function getUnfeaturedDiscussions(){
			if (is_null($this->mUnfeaturedDiscussions)) {
				require_once("Class.Criteria2.php");
				require_once("travellog/model/Class.DiscussionPeer.php");
				
				$criteria = new Criteria2();
				$criteria->add(DiscussionPeer::TOPIC_ID, $this->getID());
				$criteria->add(DiscussionPeer::IS_FEATURED, '0');
				
				$this->mUnfeaturedDiscussions = DiscussionPeer::doSelect($criteria);
			}
			
			return $this->mUnfeaturedDiscussions;
		}
		
		public function getUrl(){
			return '/topic/discussions/'.$this->getID();
		}
		
		public function getAddDiscussionUrl(){
			return '/discussionboard/discussion.php?topicID='.$this->getID().'&action=newDiscussion';
		}
		
		public function delete(){
			if(count($this->getDiscussions(1)->getInstances())){
				throw new exception('Unable to archive topic. The topic already contains some conversation.');
			}
			parent::delete();
		}
		
		public function archive(){
			if(!$this->getID()){
				throw new exception('Unable to archive topic. The topic does not exists yet.');
			}
			$this->setStatus(self::ARCHIVED_STATUS);
			$this->save();
		}
		
		public function activate(){
			$this->setStatus(self::ACTIVE_STATUS);
			$this->save();
		}
		
		public function isDeletable(){
			return !count($this->getDiscussions(1)->getInstances());
		}
		
		public function isArchivable(){
			return count($this->getDiscussions(1)->getInstances()) && $this->getStatus() == self::ACTIVE_STATUS;
		}
		
		public function isActivateable(){
			return $this->isArchived();
		}
		
		public function isArchived(){
			return $this->getStatus() == self::ARCHIVED_STATUS;
		}
		
		/*** Factory Methods ***/
		static public function createInstance($keyID=null){
			return new self($keyID);
		}
		
		/**
		 * Sets the number of topic's discussions.
		 * 
		 * @param integer $discussions_count The number of topic's discussions.
		 * 
		 * @return void
		 */
		function setDiscussionsCount($discussions_count){
			$this->mDiscussionsCount = $discussions_count;
		}
		
		/**
		 * Sets the featured discussions of the topic.
		 * 
		 * @param array The featured discussions of the topic.
		 * 
		 * @return void
		 */
		function setFeaturedDiscussions(array $discussions){
			$this->mFeaturedDiscussions = $discussions;
		}
		
		/**
		 * Sets the unfeatured discussions of the topic.
		 * 
		 * @param array The unfeatured discussions of the topic.
		 * 
		 * @return void
		 */
		function setUnfeaturedDiscussions(array $discussions){
			$this->mUnfeaturedDiscussions = $discussions;
		}
		
		static public function getInstances($options=array()){
			$def = array(
				'TOPIC_ID'		=> null,
				'GROUP_ID' 		=> null,
				'PREF'			=> null,
				'STATUS'		=> null,
				'ORDER_FIELD'	=> 'dateCreated',
				'ORDER_TYPE'	=> 'desc',
				'LIMIT'			=> null
			);
			$options = array_merge($def,$options);
			if(!is_null($options['PREF']) && !in_array($options['PREF'],self::$PRIVACY_SETTINGS)){
				throw new exception('Undefined Privacy Preference: '.$options['PREF']);
			}
			$db = new dbHandler('db=DiscussionBoard');
			$sql = 	'SELECT SQL_CALC_FOUND_ROWS * '.
					'FROM tblTopics '.
					'WHERE 1=1 ';
			if(!is_null($options['TOPIC_ID'])){
				$sql .=	' AND ID = '.$db->makeSqlSafeString($options['TOPIC_ID']);
			}
			if(!is_null($options['GROUP_ID'])){
				$sql .=	' AND groupID = '.$db->makeSqlSafeString($options['GROUP_ID']);
			}
			if(!is_null($options['PREF'])){
				$sql .= ' AND privacySetting <= '. $db->makeSqlSafeString($options['PREF']);
			}
			if(!is_null($options['STATUS'])){
				$sql .= ' AND status = '. $db->makeSqlSafeString($options['STATUS']);
			}
			$sql .= ' ORDER BY '.$def['ORDER_FIELD'].' '.$def['ORDER_TYPE'];
			if(!is_null($options['LIMIT'])){
				$sql .= ' LIMIT '.$options['LIMIT'];
			}
			$rs = new iRecordset($db->execute($sql));
			$totalCount = $rs->retrieveRecordCount();
			if(!is_null($options['LIMIT'])){
				$rsTotal = new iRecordset($db->execute('SELECT FOUND_ROWS() as total'));
				$totalCount = $rsTotal->getTotal();
			}
			$ar = array();
			/***
			var_dump($sql);
			$rs->dump();
			***/
			foreach($rs as $row){
				$ar[$row['ID']] = new self(null,$row);
			}
			return FactoryResource::create(array(
				'instances' => $ar,
				'realTotal'	=> $totalCount
			));
		}
	}
?>