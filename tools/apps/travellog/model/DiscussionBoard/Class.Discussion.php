<?php
	require_once('Class.DataModel.php');
	require_once('Class.GaDateTime.php');
	require_once('travellog/model/DiscussionBoard/Class.FactoryResource.php');
	require_once('travellog/model/DiscussionBoard/Class.Post.php');
	require_once('travellog/model/DiscussionBoard/Class.Topic.php');
	
	class Discussion extends DataModel {
		
		/**
		 * The number of posts.
		 * 
		 * @var integer
		 */
		private $mPostsCount = null;
		
		const ARCHIVED_STATUS	= 0;
		const ACTIVE_STATUS	= 1;
		
		function __construct($keyID = null, $data = null){
			parent::initialize('DiscussionBoard', 'tblDiscussions');
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid Discussion ID');
			}
			if(is_array($data)){
				$this->setFields($data);
			}
			if(!$this->getID()){
				$this->setStatus(self::ACTIVE_STATUS);
				$this->setDateCreated(GaDateTime::dbDateTimeFormat());
			}
		}
		
		public function getUrl(){
			return '/discussion/posts/' . $this->getID();
		}
		
		public function getFirstPost(){
			return Post::getInstances(array(
				'DISCUSSION_ID' => $this->getID(),
				'ORDER_BY'		=> 'tblPosts.ID ASC',
				'LIMIT'			=> 1
			));
		}
		
		public function getLatestPosts($limit=null){
			return Post::getInstances(array(
				'DISCUSSION_ID' => $this->getID(),
				'ORDER_BY'		=> 'tblPosts.ID DESC',
				'LIMIT'			=> $limit
			));
		}
		
		/**
		 * Returns true if the discussion is archived.
		 * 
		 * @return boolean true if the discussion is archived.
		 */
		function isArchived(){
			return ($this->getStatus() == self::ARCHIVED_STATUS);
		}
		
		/**
		 * Sets the number of posts of the discussion.
		 * 
		 * @param integer $count The number of posts of the discussion.
		 * 
		 * @return void
		 */
		function setPostsCount($count){
			$this->mPostsCount = $count;
		}
		
		/**
		 * Returns the number of posts of the discussion.
		 * 
		 * @return integer the number of posts of the discussion.
		 */
		function getPostsCount(){
			if (is_null($this->mPostsCount)) {
				$this->mPostsCount = 0;
				
				require_once('travellog/model/Class.PostPeer.php');
				$criteria = new Criteria2();
				$criteria->add(PostPeer::DISCUSSION_ID, $this->getID());
				
				$this->mPostsCount = PostPeer::doCount($criteria);
			}
			
			return $this->mPostsCount;
		}
		
		public function getPosts($limit=null){
			return Post::getInstances(array(
				'DISCUSSION_ID' => $this->getID()
			));
			$option['limit'] = $limit;
		}
		
		public function getParentTopic(){
			return Topic::createInstance($this->getTopicID());
		}
		
		public function isArchiveable(){
			return $this->getPostsCount() > 0 && $this->getStatus() != self::ARCHIVED_STATUS;
		}
		
		public function isActivatable(){
			return ($this->getStatus() == self::ARCHIVED_STATUS);
		}
		
		public function isDeletable(){
			return !count($this->getPosts()->getInstances());
		}
		
		public function archive(){
			if($this->getID() && $this->isArchiveable()){
				$this->setStatus(self::ARCHIVED_STATUS);
				$this->save();
			}
		}
		
		public function activate(){
			if($this->getID()){
				$this->setStatus(self::ACTIVE_STATUS);
				$this->save();
			}
		}
		
		// returns an array of unique posters involved in this discussion
		public function getPosters(){
			$sql = "SELECT post.* 
					FROM DiscussionBoard.tblPosts as post 
					WHERE post.discussionID = '{$this->getID()}'
					GROUP BY post.posterID ";
			$rs  = new iRecordset($this->mDb->execute($sql));
			$arr = array();
			foreach( $rs as $row ){
				$post = Post::getInstance(null, $row);
				$arr[] = $post->getPoster();
			}
			return $arr;
		}
		
		
		/** Factory Methods **/
		static public function createInstance($keyID=null){
			try{ 
				return new self($keyID); 
			}
			catch(exception $e){
				return null;
			}
		}
		
		/***
			Options:
				- TOPIC_ID 		: [NUMERIC] The ID of the topics whose discussions is to be retrieved.
				- GROUP			: [OBJECT] An instance of class group whose discussions is to be retrieved.
				- PREF			: [NUMERIC] The privacy pref of the topics whose discussions is the retrieved. Applicable only if the GROUP option is provided.
				- STATUS		: [NUMERIC] The status of the discussions to be retrieved.
				- ORDER_FIELD	: [STRING] The field on which the sorting will be based upon.
				- ORDER_TYPE	: [STRING] The order type.
				- LIMIT			: [STRING] The limit.
		***/
		
		static public function getInstances($options=array()){
			$def = array(
				'TOPIC_ID'		=> null,
				'GROUP_ID' 		=> null,
				'PREF'			=> null,
				'STATUS'		=> null,
				'ORDER_BY'		=> 'ID DESC',
				'LIMIT'			=> null
			);
			$options = array_merge($def,$options);
			$db = new dbHandler('db=DiscussionBoard');
			$sql= 	'SELECT SQL_CALC_FOUND_ROWS tblDiscussions.* FROM tblDiscussions ';
			if(!is_null($options['GROUP_ID'])){
				$sql .= ', tblTopics ';
			}		
			$sql .= ' WHERE 1=1 ';
			if(!is_null($options['GROUP_ID'])){
				$sql .= 'AND tblDiscussions.topicID = tblTopics.topicID '.
						'AND tblDiscussions.groupID = '.$db->makeSqlSafeString($options['GROUP_ID']);
				if(!is_null($options['PREF'])){
					$sql .= 'AND tblTopic.privacyPref = '.$db->makeSqlSafeString($options['PREF']); 
				}
			}			
			if(!is_null($options['TOPIC_ID'])){
				$sql.= ' AND topicID = '.$db->makeSqlSafeString($options['TOPIC_ID']);
			}
			if(!is_null($options['STATUS'])){
				$sql .= ' AND status = '.$db->makeSqlSafeString($options['STATUS']);
			}
			$sql .= ' ORDER BY '.$options['ORDER_BY'];
			if(!is_null($options['LIMIT'])){
				$sql .= ' LIMIT '.$options['LIMIT'];
			}
			$rs = new iRecordset($db->execute($sql));
			$totalCount = $rs->retrieveRecordCount();
			if(!is_null($options['LIMIT'])){
				$rsTotal = new iRecordset($db->execute('SELECT FOUND_ROWS() as total'));
				$totalCount = $rsTotal->getTotal();
			}
			$ar = array();
			foreach($rs as $row){
				$ar[] = new self(null,$row);
			}
			return FactoryResource::create(array(
				'instances' => $ar,
				'realTotal'	=> $totalCount
			));
		}
		
		static public function getLatestDiscussions($options=array()){
			$db = new dbHandler('db=DiscussionBoard');
			$def = array(
				'GROUP_ID' 		=> null,
				'TOPIC_ID'		=> null,
				'TOPIC_PREF'	=> null,
				'TOPIC_STATUS'	=> null,
				'LIMIT'			=> null,
				'STATUS'		=> null,
			);
			$options = array_merge($def,$options);
			$sql = 	'SELECT SQL_CALC_FOUND_ROWS tblDiscussions.*, MAX(tblPosts.ID) as postID '.
					'FROM tblDiscussions, tblPosts, tblTopics '.
					'WHERE tblDiscussions.ID = tblPosts.discussionID '.
					'AND tblDiscussions.topicID = tblTopics.ID ';
			if(!is_null($options['GROUP_ID'])){
				$sql .= ' AND tblTopics.groupID = '.$db->makeSqlSafeString($options['GROUP_ID']);
			}
			if(!is_null($options['TOPIC_ID'])){
				$sql .= ' AND tblTopics.ID = '.$db->makeSqlSafeString($options['TOPIC_ID']);
			}
			if(!is_null($options['STATUS'])){
				$sql .= ' AND tblDiscussions.status = '.$db->makeSqlSafeString($options['STATUS']);
			}
			if(!is_null($options['TOPIC_PREF'])){
				$sql .= ' AND tblTopics.privacySetting <= '.$db->makeSqlSafeString($options['TOPIC_PREF']);
			}
			if(!is_null($options['TOPIC_STATUS'])){
				$sql .= ' AND tblTopics.status = '.$db->makeSqlSafeString($options['TOPIC_STATUS']);
			}
			$sql .= ' GROUP BY tblDiscussions.ID ';
			$sql .= ' ORDER BY postID DESC ';
			if(!is_null($options['LIMIT'])){
				$sql .= ' LIMIT '.$options['LIMIT'];
			}

			$rs = new iRecordset($db->execute($sql));
			//$rs->dump();
			$totalCount = $rs->retrieveRecordCount();
			if(!is_null($options['LIMIT'])){
				$rsTotal = new iRecordset($db->execute('SELECT FOUND_ROWS() as total'));
				$totalCount = $rsTotal->getTotal();
			}
			
			$ar = array();
			foreach($rs as $row){
				$ar[] = new self(null,$row);
			}
			return FactoryResource::create(array(
				'instances' => $ar,
				'realTotal'	=> $totalCount
			));
		}
		
		public function getSubgroupDiscussions($options=array()){
			$db = new dbHandler('db=DiscussionBoard');
			$default = array(
				'PARENT_ID' 	=> 0,
				'FEATURED'		=> null,
				'GROUP_ID'		=> null,
				'TOPIC_ID'		=> null,
				'TOPIC_PREF'	=> null,
				'TOPIC_STATUS'	=> null,
				'LIMIT'			=> null,
				'STATUS'		=> null,
			);
			$options = array_merge($default, $options);
			$sql = 	"SELECT SQL_CALC_FOUND_ROWS tblDiscussions.* ".
					"FROM DiscussionBoard.tblDiscussions, DiscussionBoard.tblTopics, ".
						"(SELECT a.groupID FROM Travel_Logs.tblGroup as a WHERE parentID = ".$db->makeSqlSafeString($options['PARENT_ID'])." ) as groups ".
					"WHERE 1 ".
						"AND tblTopics.groupID = groups.groupID ".
						"AND tblDiscussions.topicID = tblTopics.ID ";
			if(!is_null($options['GROUP_ID'])){
				$sql .= ' AND tblTopics.groupID = '.$db->makeSqlSafeString($options['GROUP_ID']);
			}
			if(!is_null($options['TOPIC_ID'])){
				$sql .= ' AND tblTopics.ID = '.$db->makeSqlSafeString($options['TOPIC_ID']);
			}
			if(!is_null($options['STATUS'])){
				$sql .= ' AND tblDiscussions.status = '.$db->makeSqlSafeString($options['STATUS']);
			}
			if(!is_null($options['FEATURED'])){
				$sql .= ' AND tblDiscussions.homepageShown = '.$db->makeSqlSafeString($options['FEATURED']);
			}
			if(!is_null($options['TOPIC_PREF'])){
				$sql .= ' AND tblTopics.privacySetting <= '.$db->makeSqlSafeString($options['TOPIC_PREF']);
			}
			if(!is_null($options['TOPIC_STATUS'])){
				$sql .= ' AND tblTopics.status = '.$db->makeSqlSafeString($options['TOPIC_STATUS']);
			}
			$sql .= ' GROUP BY tblDiscussions.ID ';
			if(!is_null($options['LIMIT'])){
				$sql .= ' LIMIT '.$options['LIMIT'];
			}
			
			$rs = new iRecordset($db->execute($sql));
			$totalCount = $rs->retrieveRecordCount();
			if(!is_null($options['LIMIT'])){
				$rsTotal = new iRecordset($db->execute('SELECT FOUND_ROWS() as total'));
				$totalCount = $rsTotal->getTotal();
			}
			
			$ar = array();
			foreach($rs as $row){
				$ar[] = new self(null,$row);
			}
			return FactoryResource::create(array(
				'instances' => $ar,
				'realTotal'	=> $totalCount
			));		
		}
		
		//added by Jul overriding Save to accomodate creation of feed for this discussion 
		public function save(){
			$isNew = !$this->getID();
			parent::save();
			if( $isNew ){
				require_once("travellog/model/Class.ActivityFeed.php");
				try{
					ActivityFeed::create($this);
				}catch(exception $e){}
			}
		}
		
		public function delete(){
			$discussionID = $this->getID();
			parent::delete();
			try{
				$conn = new Connection();
				$rs = new Recordset($conn);
				$db = new dbHandler();
				require_once("travellog/model/Class.ActivityFeed.php");
				$DISCUSSION = ActivityFeed::DISCUSSION;
				$sql = "DELETE FROM tblActivityFeed
						WHERE feedSection = $DISCUSSION
							AND sectionID = $discussionID";
				$db->execute($sql);		
			}catch(exception $e){}
		}
		
	}
?>