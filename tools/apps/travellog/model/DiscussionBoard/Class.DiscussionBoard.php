<?php
	require_once('travellog/model/DiscussionBoard/Class.Topic.php');
	require_once('travellog/model/DiscussionBoard/Class.Discussion.php');
	class DiscussionBoard
	{
		private $mGroup = null;
		private function __construct($group){
			$this->mGroup = $group;
		}
		
		/***
		
		$def = array(
			'TOPIC_ID'		=> null,
			'GROUP_ID' 		=> null,
			'PREF'			=> null,
			'STATUS'		=> null,
			'ORDER_FIELD'	=> 'dateCreated',
			'ORDER_TYPE'	=> 'desc',
			'LIMIT'			=> null
		);
		
		***/
		public function getTopics($viewer=null){
			/***
			$options = array('GROUP_ID' => $this->mGroup->getGroupID());
			if(is_null($viewer) || !$this->mGroup->isMember($viewer)){
				$options['PREF'] = Topic::PUBLIC_SETTING;
			}
			return Topic::getInstances($options);
			***/
			$options = array(
				'GROUP_ID' 		=> $this->mGroup->getGroupID(),
				'PREF'			=> TOPIC::PUBLIC_SETTING,
				'STATUS'		=> TOPIC::ACTIVE_STATUS
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['PREF'] = Topic::MEMBERS_ONLY_SETTING;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['STATUS'] = null;
					}
				}
			}			
			return Topic::getInstances($options);
			
		}
		
		public function getGroup(){
			return $this->mGroup;
		}
		/***
		public function getLatestTopics($viewer=null){
			$options = array(
				'GROUP_ID' 		=> $this->mGroup->getGroupID(),
				'ORDER_FIELD'	=> 'dateTimeCreated',
				'ORDER_TYPE'	=> 'desc',
				'LIMIT'			=> '10',
				'STATUS'		=> TOPIC::ACTIVE_STATUS,
				'PREF'			=> TOPIC::PUBLIC_SETTING
			);
			if(!is_null($viewer) ){
				if($this->mGroup->isMember($viewer)){
					$options['PREF'] = Topic::MEMBER_SETTING;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['STATUS'] = null;
					}
				}
			}			
			return Topic::getInstances($options);
		}
		***/
		
		public function getLatestDiscussions($viewer=null){
			$options = array(
				'GROUP_ID'		=> $this->mGroup->getGroupID(),
				'LIMIT'			=> '5',
				'STATUS'		=> Discussion::ACTIVE_STATUS,
				'TOPIC_PREF'	=> TOPIC::PUBLIC_SETTING,
				'TOPIC_STATUS'	=> TOPIC::ACTIVE_STATUS
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['TOPIC_PREF'] 	= null;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['STATUS']	= null;
						$options['TOPIC_STATUS'] = null;
					}
				}
			}
			return Discussion::getLatestDiscussions($options);
		}
		
		public function getFeaturedSubgroupDiscussions($viewer=null){
			$options = array(
				'PARENT_ID'		=> $this->mGroup->getGroupID(),
				'STATUS'		=> Discussion::ACTIVE_STATUS,
				'TOPIC_PREF'	=> TOPIC::PUBLIC_SETTING,
				'TOPIC_STATUS'	=> TOPIC::ACTIVE_STATUS,
				'FEATURED'		=> 1
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['TOPIC_PREF'] 	= null;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['STATUS']	= null;
						$options['TOPIC_STATUS'] = null;
					}
				}
			}
			return Discussion::getSubgroupDiscussions($options);
		}
		
		
		public function isTopicIDExists($topicID=0){
			$topics = Topic::getInstances(array(
				'TOPIC_ID'	=> $topicID,
				'GROUP_ID'	=> $this->mGroup->getGroupID(),
				'LIMIT'		=> 1
			));
			return count($topics->getInstances()) > 0;
		}
		
		public function createTopic($topicID=null){
			$topic = Topic::createInstance($topicID);
			$topic->setGroupID($this->mGroup->getGroupID());
			return $topic;
		}
		
		/*** Functions For Discussion ***/
		public function getDiscussionsInTopic($topic,$viewer){
			$options = array(
				'TOPIC_ID'		=> $topic->getID(),
				'STATUS'		=> Discussion::ACTIVE_STATUS,
				'TOPIC_PREF'	=> Topic::PUBLIC_SETTING,
				'TOPIC_STATUS'	=> Topic::ACTIVE_STATUS
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['TOPIC_PREF'] 	= null;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['STATUS']	= null;
						$options['TOPIC_STATUS'] = null;
					}
				}
			}
			return Discussion::getInstances($options);
		}
		/****
		$def = array(
			'TOPIC_ID' 		=> null,
			'TOPIC_PREF'	=> null,
			'TOPIC_STATUS'	=> null,
			'DISCUSSION_ID'	=> null,
			'POSTER_ID'		=> null,
			'ORDER_BY'		=> 'ID ASC',
			'LIMIT'			=> null
		);
		****/
		/*** Functions For  Posts ***/
		public function getLatestPostsInDiscussion($discussion,$viewer,$limit=1){
			$options = array(
				'DISCUSSION_ID'	=> $discussion->getID(),
				'TOPIC_PREF'	=> Topic::PUBLIC_SETTING,
				'TOPIC_STATUS'	=> Topic::ACTIVE_STATUS,
				'ORDER_BY'		=> 'ID DESC',
				'LIMIT'			=> $limit
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['TOPIC_PREF'] 	= null;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['TOPIC_STATUS'] = null;
					}
				}
			}
			return Post::getInstances($options);
		}
		
		public function getLatestPostsInGroup($viewer,$limit=1){
			$options = array(
				'GROUP_ID'			=> $this->mGroup->getGroupID(),
				'TOPIC_PREF'		=> Topic::PUBLIC_SETTING,
				'TOPIC_STATUS'		=> Topic::ACTIVE_STATUS,
				'DISCUSSION_STATUS'	=> Discussion::ACTIVE_STATUS,
				'ORDER_BY'			=> 'ID DESC',
				'LIMIT'				=> $limit
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['TOPIC_PREF'] 	= null;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['TOPIC_STATUS'] 		= null;
						$options['DISCUSSION_STATUS'] 	= null;
					}
				}
			}

			return Post::getInstances($options);
		}
		
		public function getLatestPostsInTopic($topic,$viewer,$limit=1){
			$options = array(
				'TOPIC_ID'			=> $topic->getID(),
				'TOPIC_PREF'		=> Topic::PUBLIC_SETTING,
				'TOPIC_STATUS'		=> Topic::ACTIVE_STATUS,
				'DISCUSSION_STATUS'	=> Discussion::ACTIVE_STATUS,
				'ORDER_BY'			=> 'ID DESC',
				'LIMIT'				=> $limit
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['TOPIC_PREF'] 	= null;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['TOPIC_STATUS'] 		= null;
						$options['DISCUSSION_STATUS'] 	= null;
					}
				}
			}
			return Post::getInstances($options);
		}
		
		public function getPostsInTopic($topic,$viewer,$limit=null){
			$options = array(
				'TOPIC_ID'			=> $topic->getID(),
				'TOPIC_PREF'		=> Topic::PUBLIC_SETTING,
				'TOPIC_STATUS'		=> Topic::ACTIVE_STATUS,
				'DISCUSSION_STATUS'	=> Discussion::ACTIVE_STATUS,
				'LIMIT'				=> null
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['TOPIC_PREF'] 	= null;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['TOPIC_STATUS'] 		= null;
						$options['DISCUSSION_STATUS'] 	= null;
					}
				}
			}
			return Post::getInstances($options);
		}
		
		
		public function getDiscussionPosts($discussion,$viewer){
			$options = array(
				'DISCUSSION_ID'		=> $discussion->getID(),
				'STATUS'			=> Discussion::ACTIVE_STATUS,
				'TOPIC_PREF'		=> Topic::PUBLIC_SETTING,
				'DISCUSSION_STATUS'	=> Topic::ACTIVE_STATUS,
				'TOPIC_STATUS'	=> TOPIC::ACTIVE_STATUS
			);
			if(!is_null($viewer)){
				if($this->mGroup->isMember($viewer)){
					$options['TOPIC_PREF'] 	= null;
					if($this->mGroup->getAdministratorID() == $viewer->getTravelerID()){
						$options['TOPIC_STATUS'] = null;
						$options['DISCUSSION_STATUS'] 	= null;
					}
				}
			}
			return Post::getInstances($options);
		}
		
		public function getHomePageUrl(){
			return '/discussion_board/home/'.$this->mGroup->getGroupID();
		}
		
		static public function getInstanceByGroup($group=null){
			return new self($group);
		}
	}
?>