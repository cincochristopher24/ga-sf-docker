<?php
  /**
   * @(#) Class.SqlField.php
   * 
   * Handles an sql field. Sql field is defined as the field name in a table or the 
   * table name itself.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 12 11, 08
   */
  
  class SqlField{
  	private $dbName = "";
  	private $tbName = "";
  	private $fldName = "";
  	private $alias = "";
  	
  	/**
  	 * constructor
  	 * 
  	 * @param string $fld field name
  	 * @param string $table table name
  	 * @param string $db database name
  	 * @param string $alias alias for sql
  	 */
  	function __construct($fld, $table = "", $db = "", $alias = ""){
  		$this->fldName = $fld;
  		$this->tbName = $table;
  		$this->dbName = $db;
  		$this->alias = $alias;
  	}
  	
  	/**
  	 * Sets the table name.
  	 * 
  	 * @param string $table
  	 * @return void
  	 */
  	function setTableName($table){
  		$this->tbName = $table;
  	}
  	
  	/**
  	 * Sets the field name.
  	 * 
  	 * @param string $field
  	 * @return void
  	 */
  	function setFieldName($field){
  		$this->fldName = $field;
  	}
  	
  	/**
  	 * Sets the database name.
  	 * 
  	 * @param string $db
  	 * @return void
  	 */
  	function setDbName($db){
  		$this->dbName = $db;
  	}
  	
  	/**
  	 * Sets the alias.
  	 * 
  	 * @param string $alias
  	 * @return string
  	 */
  	function setAlias($alias){
  		$this->alias = $alias;
  	}
  	
  	/**
  	 * Gets the table name.
  	 * 
  	 * @return string
  	 */
  	function getTableName(){
  		return $this->tbName;
  	}
  	
  	/**
  	 * Gets the database name.
  	 * 
  	 * @return string
  	 */
  	function getDbName(){
  		return $this->dbName;
  	}
  	
  	/**
  	 * Gets the field name.
  	 * 
  	 * @return string
  	 */
  	function getFieldName(){
  		return $this->fldName;
  	}
  	
  	/**
  	 * Gets the alias of this sql field.
  	 * 
  	 * @return string
  	 */
  	function getAlias(){
  		return $this->alias;
  	}
  }
