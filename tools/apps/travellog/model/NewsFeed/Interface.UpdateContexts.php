<?php
	interface UpdateContexts{
		
		// sets specific detail (could be objects) for each feed item
		public function setDetailOfItems();
		
		// read updates from logreader
		public function readUpdates();
		
		// group, sort, apply contexts and sections to updates read from log reader
		public function processUpdates($params = array());
		
	}
?>
