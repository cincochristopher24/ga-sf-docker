<?php
	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	
	class CommentUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates 	= array();
		private static $latestTime	= null;
		
		public function __construct(){
			$this->mSection = Sections::COMMENTS;
		}
		
		public function setDetailOfItems(){
			foreach($this->mItems as $item){
				$this->mItemDetail[] = new Comment($item['commentID']);
			}
		}
		
		public function readUpdates($params = array()){
			if(!isset($params['SUBJECTS']))
				throw new exception("Array passed to " . __CLASS__ . "->readUpdates must contain key 'SUBJECTS'");
			(isset($params['CONDITION']))? $s = $params['CONDITION'] : $s = '';
			
			// profile comments
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblTravelertoComment` " .
					"WHERE _NEW_travelerID IN (" . implode(", ", $params['SUBJECTS']) . ") " .
					"AND _EVENT_ = '" . EventType::INSERT . "' AND " . $s . " ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'PROFILE_COMMENT', 'SECTION' => Sections::COMMENTS));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'PROFILE_COMMENT');
			// end of profile comments
			
			// journal entry comments
					// query inside <$ $> will be ignored by Reader
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblTravelLogToComment` WHERE " .
					"_EVENT_ = '" . EventType::INSERT . "' AND $s AND `_NEW_travelLogID` IN " .
					"( <$ SELECT `travellogID` FROM `Travel_Logs`.`tblTravel`, `Travel_Logs`.`tblTravelLog` " .
					"WHERE `tblTravel`.`travelID` = `tblTravelLog`.`travelID` " .
					"AND `tblTravel`.`travelerID` IN ('" . implode("', '", $params["SUBJECTS"]) . "') $>) " .
					"ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'JOURNAL_ENTRY_COMMENT', 'SECTION' => Sections::COMMENTS));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'JOURNAL_ENTRY_COMMENT'); 
			// end of journal entry comments
			
			// photo comments 
					// query inside <$ $> will be ignored by Reader
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblCommenttoPhoto` WHERE _EVENT_ = '" . EventType::INSERT . "' AND $s AND _NEW_photoID IN (" .
					"<$ SELECT `tblPhoto`.`photoID` FROM `Travel_Logs`.`tblPhoto`, `Travel_Logs`.`tblTravelertoPhoto` " .
					"WHERE `tblPhoto`.`photoID` = `tblTravelertoPhoto`.`photoID` AND `tblPhoto`.`photoTypeID` IN (1,2) " .
					"AND `tblTravelertoPhoto`.`travelerID` IN ('" . implode("', '", $params["SUBJECTS"]) . "') $> ) " .
					"ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'PHOTO_COMMENT', 'SECTION' => Sections::COMMENTS));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'PHOTO_COMMENT');
			// end of photo comments
		}
		
		public function processUpdates($params = array()){
			$feed = &self::$updates[$params['DATE']][$params['EVENT']][$params['TIME']];
			$feed['ITEMS'][$params['TIME']] = $params['DATA']['_NEW_'];
			$feed['ITEMS'][$params['TIME']]['EXEC_TIME'] = $params['TIME'];
			$feed['DOER_ID'] = $params['DOER_ID'];
			switch ($params['CONTEXT']){
				case 'PROFILE_COMMENT' :
					$feed['RECIPIENT_ID'] = $params['DATA']['_NEW_']['travelerID'];
					break;
				case 'JOURNAL_ENTRY_COMMENT' :
					require_once('travellog/model/Class.TravelLog.php');
					$travellog = new TravelLog($params['DATA']['_NEW_']['travellogID']);
					$feed['RECIPIENT_ID'] = $travellog->getTraveler()->getTravelerID();
					break;
				case 'PHOTO_COMMENT' :
					$dbHandler = new dbHandler();
					$sql = 	"SELECT `travelerID` FROM `Travel_Logs`.`tblTravelertoPhoto` " .
							"WHERE `photoID` = " . ToolMan::makeSqlSafeString($params['DATA']['_NEW_']['photoID']);
					$rs = new iRecordset($dbHandler->execQuery($sql));
					if($rs->retrieveRecordCount()){
						$row = $rs->retrieveRow(0);
						$feed['RECIPIENT_ID'] = $row['travelerID'];
					}
					break;
				default :
					throw new exception('Unknown newsfeed comment context');
			}
			$feed['CONTEXT'] = $params['CONTEXT'];
			$feed['EVENT']	= $params['EVENT'];
			return self::$updates[$params['DATE']][$params['EVENT']]; 		
		}
		
	}
?>
