<?php
	
	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	
	class FriendUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates 	= array();
		private static $latestTime 	= null;
		
		public function __construct(){
			$this->mSection = Sections::FRIEND;
		}
		
		public function setDetailOfItems(){
			// no objects to set, just give items
			foreach($this->mItems as $item)
				$this->mItemDetail[] = $item;
		}
		
		public function readUpdates($params = array()){
			$condition = $params['CONDITION'];
			$excludeSelfCondition = (is_null($params['SELF'])) ? "" 
					: 	"AND `_NEW_friendID` != " . ToolMan::makeSqlSafeString($params['SELF']) . 
						" AND `_NEW_travelerID` != " . ToolMan::makeSqlSafeString($params['SELF']);
			 
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblFriend` WHERE (`_NEW_travelerID` IN (" . implode(", ", $params['SUBJECTS']) .  ") " .
					"OR `_NEW_friendID` IN (" . implode(", ", $params['SUBJECTS']) .  ") ) " .
					"AND _EVENT_ = '" . EventType::INSERT . "' AND $condition AND `_NEW_travelerID` = _DOER_ID_ " . $excludeSelfCondition .
					"ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'FRIEND', 'SECTION' => Sections::FRIEND));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'FRIEND');
		}
		
		public function processUpdates($params = array()){
			$feed = &self::$updates[$params['DATE']][$params['EVENT']][$params['TIME']];
			$feed['ITEMS'][$params['TIME']] = $params['DATA']['_NEW_'];
			$feed['RECIPIENT_ID']	= $params['DATA']['_NEW_']['friendID'];
			$feed['DOER_ID'] 	= $params['DATA']['_NEW_']['travelerID'];
			$feed['EVENT']		= $params['EVENT'];
			$feed['CONTEXT']	= $params['CONTEXT'];
			return self::$updates[$params['DATE']][$params['EVENT']];
		}
	}
?>
