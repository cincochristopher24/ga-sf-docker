<?php

	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	
	class CalendarEventUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates 	= array();
		private static $latestTime 	= null;
		
		public function __construct(){
			$this->mSection = Sections::CALENDAR_EVENT;
		}
		
		public function setDetailOfItems(){
			require_once('travellog/model/Class.TravelerEvent.php');
			require_once('travellog/model/Class.GroupEvent.php');
			
			foreach($this->mItems as $key => $item){
				if('TRAVELER_EVENT' == $item['CONTEXT']){
					$this->mItemDetail[] = new TravelerEvent($item['travelereventID']);		
				}
				elseif('GROUP_EVENT' == $this->mContext){
					$obj_session = SessionManager::getInstance();
					$loggedTraveler 	= new Traveler($obj_session->get('travelerID'));
					$groupEvent 		= new GroupEvent($item['groupeventID']);
					$group 				= $groupEvent->getGroup();
					
					if($group->isMember($loggedTraveler)){
						$this->mItemDetail[] = $groupEvent;
					}
					else{
						//$groupPreference 	= new GroupPrivacyPreference($group->getGroupID());
						//if(TRUE == $groupPreference->getViewCalendar()){
							//$this->mItemDetail[] = $groupEvent;
						//}
						//else{ // public is not allowed to view this event so don't show this item
							unset($this->mItems[$key]);
						//}
					}
				}
			}
		}
		// end functs for feed item display
		
		public function readUpdates($params = array()){
			$condition = $params['CONDITION'];
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblTravelerEvent` t1 WHERE " .
					"$condition AND _EVENT_ = '" . EventType::INSERT . "' AND EXISTS " .
					"( <$ SELECT travelereventID FROM `Travel_Logs`.`tblTravelerEvent` t2 " .
					"WHERE t1._NEW_travelereventID = t2.travelereventID $>)" .
					"ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'TRAVELER_EVENT', 'SECTION' => Sections::CALENDAR_EVENT));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'TRAVELER_EVENT');
			
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblGroupEvent` t1 WHERE " .
					"$condition AND `_EVENT_` = '" . EventType::INSERT . "' AND EXISTS " .
					"( <$ SELECT groupeventID FROM `Travel_Logs`.`tblGroupEvent` t2 " .
					"WHERE t1._NEW_groupeventID = t2.groupeventID $>)" .
					"ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'GROUP_EVENT', 'SECTION' => Sections::CALENDAR_EVENT));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'GROUP_EVENT');		 	
		}
		
		public function processUpdates($params = array()){
			if(!isset(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]))
				self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']] = $params['TIME'];
			else{
				(strtotime(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]) < strtotime($params['TIME']))
					? self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']] = $params['TIME']
					: self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]; 
			}
			$feed = &self::$updates[$params['DATE']][$params['DOER_ID']][$params['EVENT']]['FEED'];
			$feed['ITEMS'][$params['TIME']] = $params['DATA']['_NEW_'];
			$feed['ITEMS'][$params['TIME']]['CONTEXT'] = $params['CONTEXT']; 
			//$feed['ITEMS'][$params['DATA']['_NEW_']['travelereventID']]['EXEC_TIME'] = $params['TIME'];
			$feed['DOER_ID'] 	= $params['DOER_ID'];
			$feed['CONTEXT']	= $params['CONTEXT'];
			$feed['EVENT']		= $params['EVENT'];
			$arr = array();
			foreach(self::$updates[$params['DATE']] as $doerID => $command){
				foreach($command as $cmd => $doerUpdates)
					$arr[self::$latestTime[$params['DATE']][$doerID][$cmd]] = $doerUpdates['FEED'];
			}
			return $arr;	
		}
	}
?>
