<?php
	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	require_once('travellog/model/Class.MessageFactory.php');
	require_once('travellog/model/Class.BulletinMessage.php');
	
	class BulletinUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates 	= array();
		private static $latestTime 	= null;
		
		public function __construct(){
			$this->mSection = Sections::BULLETIN;	
		}
		
		public function setDetailOfItems(){
			$loggedTraveler = NewsFeed::getLoggedTraveler();
			foreach($this->mItems as $key => $item){
				$message = new BulletinMessage($item['attributeID']);
				$source = $message->getSource();
				// check is doer is the sender
				if(('Moderator' == get_class($source) OR 'Traveler' == get_class($source)) AND $this->mDoerID != $source->getTravelerID())
					unset($this->mItems[$key]);
				
				// check if the administrator of the group who sent the bulletin is the doer
				elseif('Group' == get_parent_class($source) AND $this->mDoerID != $source->getAdministratorID())
					unset($this->mItems[$key]);
					
				// additional check for each destination, check if logged user can view the bulletin
				else{
					foreach($message->getDestination() as $dest){
						// check if logged user is a member of the destination group 
						if('Group' == get_parent_class($dest)){
							// check group preference
							/**$groupPreference = new GroupPrivacyPreference($dest->getGroupID());
							if($dest->isMember($loggedTraveler)){
								$this->mItemDetail[$item['attributeID']] = $message;
							}
							else{
								if(TRUE == $groupPreference->getViewBulletin()){
									$this->mItemDetail[$item['attributeID']] = $message;
								}
								else{
									unset($this->mItems[$key]); // remove feed item
								}
							}**/
							if($dest->isMember($loggedTraveler)){
								$this->mItemDetail[$item['attributeID']] = $message;
							}
							else
								unset($this->mItems[$key]); // remove feed item
						}
						// destination is a traveler, and logged user is doer's friend so bulletin is viewable
						else{ // Traveler
							$this->mItemDetail[$item['attributeID']] = $message;		
						}
					}
				}
			}
		}
		
		public function readUpdates($params = array()){
			$condition = $params['CONDITION'];
			/*$sql = "SELECT * FROM `Travel_Logs.tblMessageToAttribute WHERE `_NEW_attributeID` IN " .
					"( <$ SELECT t1.attributeID FROM `Travel_Logs`.`tblMessageToAttribute` t1, `Travel_Logs`.`tblMessages` t2 " .
					"WHERE t1.messageID = t2.messageID AND t2.discriminator = '2'$> ) AND $condition " .
					"ORDER BY _EXECUTION_DATE_ DESC";*/
			$sql = "SELECT * FROM `Travel_Logs.tblMessageToAttribute logView WHERE `_NEW_attributeID` IN " .
					"( <$ SELECT t1.attributeID FROM `Travel_Logs`.`tblMessageToAttribute` t1, `Travel_Logs`.`tblMessages` t2 " .
					"WHERE logView._NEW_attributeID = t1.attributeID AND logView._NEW_messageID = t1.messageID AND " .
					"t1.messageID = t2.messageID AND t2.discriminator = '2'$> ) AND $condition " .
					"ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'BULLETIN', 'SECTION' => Sections::BULLETIN));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'BULLETIN');
		}
		
		public function processUpdates($params = array()){
			if(!isset(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]))
				self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']] = $params['TIME'];
			else{
				(strtotime(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]) < strtotime($params['TIME']))
					? self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']] = $params['TIME']
					: self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]; 
			}
			$feed = &self::$updates[$params['DATE']][$params['DOER_ID']][$params['EVENT']]['FEED'];
			$feed['ITEMS'][$params['DATA']['_NEW_']['messageID']] = $params['DATA']['_NEW_'];
			
			$feed['DOER_ID'] 	= $params['DOER_ID'];
			$feed['EVENT']		= $params['EVENT'];
			$feed['CONTEXT'] = $params['CONTEXT'];
			$feed['SECTION']	= Sections::BULLETIN;
			$arr = array();
			
			foreach(self::$updates[$params['DATE']] as $doerID => $command){
				foreach($command as $cmd => $doerUpdates)
					$arr[self::$latestTime[$params['DATE']][$doerID][$cmd]] = $doerUpdates['FEED'];
			}
			return $arr;
		}
	}
?>