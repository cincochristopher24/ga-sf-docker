
<?php
	
	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	require_once('travellog/model/NewsFeed/Class.FeedItems.php');
	
	class JournalEntryUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates 	= array();
		private static $latestTime 	= null;
		
		public function __construct(){
			$this->mSection = Sections::JOURNAL_ENTRY;
		}
		
		public function setDetailOfItems(){
			require_once('travellog/model/Class.TravelLog.php');
			foreach($this->mItems as $item){
				$this->mItemDetail[] = new TravelLog($item['travellogID']);
			}
		}
		
		public function readUpdates($params = array()){
			$condition 		= $params['CONDITION'];
			$sql =	"SELECT * FROM `Travel_Logs`.`tblTravelLog` t1 WHERE (_EVENT_ = '" . EventType::INSERT . "' OR _EVENT_ = '" . EventType::UPDATE . "') " .
					"AND $condition AND EXISTS ( <$ SELECT travellogID FROM `Travel_Logs`.`tblTravelLog` t2 " .
					"WHERE t1._NEW_travellogID = t2.travellogID $> ) ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'JOURNAL_ENTRY', 'SECTION' => Sections::JOURNAL_ENTRY));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'JOURNAL_ENTRY'); 
		}
		
		public function processUpdates($params = array()){
			if(!isset(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]))
				self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']] = $params['TIME'];
			else{
				(strtotime(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]) < strtotime($params['TIME']))
					? self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']] = $params['TIME']
					: self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]; 
			}
			$feed = &self::$updates[$params['DATE']][$params['DOER_ID']][$params['EVENT']]['FEED'];
			$feed['ITEMS'][$params['DATA']['_NEW_']['travellogID']] = $params['DATA']['_NEW_'];
			$feed['ITEMS'][$params['DATA']['_NEW_']['travellogID']]['EXEC_TIME'] = $params['TIME'];
			$feed['DOER_ID'] 	= $params['DOER_ID'];
			$feed['CONTEXT']	= $params['CONTEXT'];
			$feed['EVENT']		= $params['EVENT'];
			$arr = array();
			foreach(self::$updates[$params['DATE']] as $doerID => $command){
				foreach($command as $cmd => $doerUpdates)
					$arr[self::$latestTime[$params['DATE']][$doerID][$cmd]] = $doerUpdates['FEED'];
			}
			return $arr;	
		}
	}
?>
