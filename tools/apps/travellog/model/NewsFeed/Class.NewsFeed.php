<?php
/**
Show updates of friends if a friend has:

- added a journal entry
- edited a journal entry
- uploaded a profile photo
- uploaded a journal entry photo
- added a travel plan
- edited a travel plan
- edited mytravelbio
- received a comment/poke for his/her profile, journal entry, or photo
- joined a group/club
- created a club
- become friends with another traveler
- added a calendar event
- posted a bulletin

 */
 	require_once('Class.ToolMan.php');
 	require_once('Class.GaDateTime.php');
 	require_once('travellog/model/Class.GroupPrivacyPreference.php');
 	require_once('travellog/model/NewsFeed/Class.DataHandler.php');
 	require_once('travellog/model/NewsFeed/Class.Sections.php');
 	require_once('travellog/model/NewsFeed/Class.EventType.php');
	require_once('travellog/model/NewsFeed/Class.FeedItems.php');
	require_once('travellog/model/NewsFeed/Class.NewsFeedException.php');
 	require_once('travellog/views/Class.NewsFeedView.php');
 	
 	class NewsFeed{
		
		const DEFAULT_DATE_RANGE 	= 5;
		//const DEFAULT_MAX_UPDATES 	= 20;
		 
		private $mTraveler 	= null;
		private $mFriends 	= array();
		private $mSettings 	= array();
		private $mUpdates	= array();
		private $mConditions = array();
		
		private static $mLoggedTraveler = null;
		
		/**
		 * @param Traveler traveler
		 */
		public function __construct($traveler){
			$trace = debug_backtrace();
			$debugTrace = (isset($trace[1]) ? $trace[1] : $trace[0]);
			if(FALSE == ($traveler instanceof Traveler))
				throw new NewsFeedException(NewsFeedException::INVALID_CONSTRUCTOR_PARAM, $debugTrace);
			$this->mTraveler = $traveler;
			
			// set default of logged traveler to owner of newsfeed
			self::$mLoggedTraveler = $this->mTraveler;
			$this->mFriends = $this->mTraveler->getFriends();
			
			// set defaults
			$this->mSettings['DATE_RANGE'] = self::DEFAULT_DATE_RANGE; // read updates within default date range
		}
		
		/**
		 * @param int numberOfDays
		 */
		public function setDateRange($numberOfDays = self::DEFAULT_DATE_RANGE){
			if($numberOfDays <= 0 OR !is_numeric($numberOfDays))
				$numberOfDays = self::DEFAULT_DATE_RANGE;
			$this->mSettings['DATE_RANGE'] = $numberOfDays;
		}
		
		/*public function setMaxUpdates($numberOfMaxUpdates){
			$this->mSettings['MAX_UPDATES'] = $numberOfMaxUpdates;
		}*/
		
		/**
		 * get personal updates of logged in traveler
		 */
		public function getPersonalUpdates(){
			if(!is_null($this->mTraveler))
				$this->readUpdates(array($this->mTraveler->getTravelerID()), true);
			return $this->mUpdates;
		}
		
		/**
		 * get updates of friends of logged in traveler
		 */
		public function getFriendUpdates(){
			if(0 < count($this->mFriends))
				$this->readUpdates(array_keys($this->mFriends));
			return $this->mUpdates;
		}
		
		/**
		 * @return array 'processed updates'
		 */
		public function showUpdates($updates = null){
			if(is_null($updates))
				$updates = $this->mUpdates;
			$newsFeedView = new NewsFeedView();
			return $newsFeedView->render($updates);
		}
		
		private function applyConditions(){
			$this->mConditions['doers'] 	= '_DOER_ID_ IN (' . implode(', ', $this->mSettings['DOERS']) .')';
			$this->mConditions['dateRange'] = '(_EXECUTION_DATE_ >= ' . ToolMan::makeSqlSafeString(GaDateTime::dbDateFormat(GaDateTime::dateAdd(GaDateTime::now(), 'd', $this->mSettings['DATE_RANGE'] * (-1))) . ' 00:00:00') . ' AND _EXECUTION_DATE_ <= ' . ToolMan::makeSqlSafeString(GaDateTime::dbDateFormat(GaDateTime::now()) . ' 23:59:59') . ')';
			return implode(' AND ',$this->mConditions);
		}
		
		private function readUpdates($doers, $isPersonal = false){
			$this->mSettings['DOERS'] = $doers;
			$conditionSql = $this->applyConditions();
			$this->registerSectionUpdateHandlers();
			Sections::getUpdateHandler(Sections::TRAVEL_BIO)	->readUpdates(array('CONDITION' => $conditionSql)); // set updates for travelbio
			Sections::getUpdateHandler(Sections::COMMENTS)		->readUpdates(array('SUBJECTS' => $doers, 'CONDITION' => $this->mConditions['dateRange'])); // set updates for comments
			Sections::getUpdateHandler(Sections::GROUPS)		->readUpdates(array('SUBJECTS' => $doers, 'CONDITION' => $this->mConditions['dateRange'])); // set updates for groups
			Sections::getUpdateHandler(Sections::FRIEND)		->readUpdates(array('SUBJECTS' => $doers, 'CONDITION' => $this->mConditions['dateRange'], 'SELF' => ((!$isPersonal) ? $this->mTraveler->getTravelerID() : null))); // set updates for friend updates
			Sections::getUpdateHandler(Sections::JOURNAL_ENTRY)	->readUpdates(array('CONDITION' => $conditionSql)); // set updates for journal entries
			Sections::getUpdateHandler(Sections::PHOTO)			->readUpdates(array('CONDITION' => $conditionSql));	// set update for photo
			Sections::getUpdateHandler(Sections::BULLETIN)		->readUpdates(array('CONDITION' => $conditionSql)); // set updates for bulletins
			Sections::getUpdateHandler(Sections::CALENDAR_EVENT)->readUpdates(array('CONDITION' => $conditionSql)); // set updates for calendar events
			Sections::getUpdateHandler(Sections::TRAVEL_PLANS) 	->readUpdates(array('CONDITION' => $conditionSql)); // set updates for travel plans
			$this->mUpdates = DataHandler::getProcessedData(); // returns array of feeds (obj)
		}
		
		private function registerSectionUpdateHandlers(){
			$path = "travellog/model/NewsFeed/Contexts/";
			Sections::registerUpdateHandler(Sections::TRAVEL_PLANS, $path . "Class.TravelPlanUpdates.php", "TravelPlanUpdates");
			Sections::registerUpdateHandler(Sections::TRAVEL_BIO, $path . "Class.TravelBioUpdates.php", "TravelBioUpdates");
			Sections::registerUpdateHandler(Sections::COMMENTS, $path . "Class.CommentUpdates.php", "CommentUpdates");
			Sections::registerUpdateHandler(Sections::GROUPS, $path . "Class.GroupUpdates.php", "GroupUpdates");
			Sections::registerUpdateHandler(Sections::FRIEND, $path . "Class.FriendUpdates.php", "FriendUpdates");
			Sections::registerUpdateHandler(Sections::JOURNAL_ENTRY, $path . "Class.JournalEntryUpdates.php",  "JournalEntryUpdates");
			Sections::registerUpdateHandler(Sections::PHOTO, $path . "Class.PhotoUpdates.php", "PhotoUpdates");
			Sections::registerUpdateHandler(Sections::BULLETIN, $path . "Class.BulletinUpdates.php", "BulletinUpdates");
			Sections::registerUpdateHandler(Sections::CALENDAR_EVENT, $path . "Class.CalendarEventUpdates.php", "CalendarEventUpdates");
		}
		
		/**
		 * @return Traveler 'logged traveler to goabroad.net'
		 */
		public static function getLoggedTraveler(){
			return self::$mLoggedTraveler;
		}
		
		/**
		 * @param Traveler 'logged traveler to ga.net'
		 */
		public static function setLoggedTraveler($traveler){
			self::$mLoggedTraveler = $traveler;
		}
		
		/**
		 * @param int travelerID
		 * @return bool 'check if traveler is also the logged user'
		 */
		public static function isLoggedUser($travelerID){
			return self::$mLoggedTraveler->getTravelerID() == $travelerID;
		}
	}
?>
