<?php
/*
 * Created on Aug 10, 2006 
 */
 
 
require_once("Class.Connection.php");
require_once("Class.Recordset.php");



class WorkExperience
{
		
		
		private $travelerID;
		private $workexperienceID;
		private $workcount;
		private $JobPosition;
		private $Employer;
		private $JobDescription;
		private $LenServ; 
		
		
		
		// setters
		
		
		function setworkcount($_workcount)
		{
			$this->workcount = $_workcount;
		}
		
		function setworkexperienceID($_workexperienceID)
		{
			$this->workexperienceID = $_workexperienceID;
		}
		
		function setJobPosition($_JobPosition)
		{
			$this->JobPosition = $_JobPosition;
		}
		
		function setEmployer($_Employer)
		{
			$this->Employer = $_Employer;
		}
		
		function setJobDescription($_JobDescription)
		{
			$this->JobDescription = $_JobDescription;
		}
		
		function setLenServ($_LenServ)
		{
			$this->LenServ = $_LenServ;
		}
		
		
		
		// getters
		
		
		function getworkexperienceID()
		{
			return $this->workexperienceID;
		}
		
		function getworkcount()
		{
			return $this->workcount;
		}
		
		function getJobPosition()
		{
			 return $this->JobPosition;
		}
		
		function getEmployer()
		{
			return $this->Employer;
		}
		
		function getJobDescription()
		{
			return $this->JobDescription;
		}
		
		function getLenServ()
		{
			return $this->LenServ;
		}
		
		
		
		// constructor
		
		
		function WorkExperience($travelerID = 0, $workexperienceID = 0)
		{
			
				$conn = new Connection();
				$rs = new Recordset($conn);
				
				$this->travelerID = $travelerID; 
				
				if($workexperienceID > 0){
					$qwork = "select * from tblWorkExperience where travelerID = '$travelerID' and workexperienceID = '$workexperienceID'";
				}
				else{
					$qwork = "select * from tblWorkExperience where travelerID = '$travelerID'";
				}
				
				$rs->Execute($qwork);	
					
					
				if ($rs->RecordCount() > 0) {
					
				
				//$this->resumeID = $rs->Result(0,"resumeID");		// -
				$this->workexperienceID = $rs->Result(0,"workexperienceID");		// -
				$this->JobPosition = $rs->Result(0,"JobPosition");						// -
				$this->Employer = $rs->Result(0,"Employer");						// -
				$this->JobDescription = $rs->Result(0,"JobDescription");					// -
				$this->LenServ = $rs->Result(0,"LenServ"); 						// -
				
				
				return $this;
				}	
				
		}
		
		
		
		// CRUD methods
		
		
		function CreateWork()
		{
					
							$conn = new Connection();
							$rs = new Recordset($conn);
						
							$slashJobPos = addslashes($this->JobPosition);
							$slashEmployer = addslashes($this->Employer);
							$slashJobDesc = addslashes($this->JobDescription);
							$slashLenServ = addslashes($this->LenServ);
					
							$sql = "insert into `tblWorkExperience` (`travelerID`, `JobPosition`, `Employer`, `JobDescription`, `LenServ`)
										VALUES ('$this->travelerID', '$slashJobPos', '$slashEmployer', '$slashJobDesc', '$slashLenServ')";
							$rs->Execute($sql);		
										
		}
		
		
		function UpdateWork()
		{
			
					$conn = new Connection();
					$rs = new Recordset($conn);
					
					$slashJobPos = addslashes($this->JobPosition);
					$slashEmployer = addslashes($this->Employer);
					$slashJobDesc = addslashes($this->JobDescription);
					$slashLenServ = addslashes($this->LenServ);
							
					$sql = "update `tblWorkExperience` set `JobPosition` = '$slashJobPos', `Employer` = '$slashEmployer', `JobDescription` = '$slashJobDesc',
							`LenServ` = '$slashLenServ' where `workexperienceID` = '$this->workexperienceID'";
					$rs->Execute($sql);
			
			
		}
		
		
		function DeleteWork()
		{
			
					$conn = new Connection();
					$rs = new Recordset($conn);
					
					$sql = "delete from `tblWorkExperience` where `workexperienceID` = '$this->workexperienceID'";
					$rs->Execute($sql);
					
			
		}
		
		
		
		
}
?>
