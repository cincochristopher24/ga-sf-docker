<?php
	/***
	 * @author Naldz Castellano III
	 */
	
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	require_once('travellog/model/Class.PendingNotificationRecipient.php');
	require_once('Class.HelperGlobal.php');
	
	class PendingNotification
	{
		const TRAVELER_CONTEXT 	= 1;
		const GROUP_CONTEXT 	= 2;
		
		private $mDb = null;
		private $mPendingNotificationID;
		private $mContext;
		private $mContextID;
		private $mSection;
		private $mAction;
		private $mLink;
		private $mGenID;
		private $mDateAdded;
		private $mRecipients;
		
		/***
		 * Constructor function
		 * @param int $param : The pending notification ID this object will be representing.
		 * @param array $data : Initialized data to feed this object. This is to avoid another query if you already have the data of the object.
		 */
		function __construct($param=null,$data=null){
			$this->mDb = new dbHandler();
			$this->clearData();
			if(!is_null($param)){
				$qry =	'SELECT pendingNotificationID, context, contextID, section, action, link, genID, dateAdded ' .
						'FROM tblPendingNotification ' .
						'WHERE pendingNotificationID = '.ToolMan::makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if($rs->retrieveRecordCount() > 0){
					$this->init($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid pendingNotificationID '.$param.' passed to object of type PendingNotification.');
				}
			}
			if(is_array($data)){
				$this->init($data);
			}
		}
		
		private function init($data){
			$this->setPendingNotificationID($data['pendingNotificationID']);
			$this->setContext($data['context']);
			$this->setContextID($data['contextID']);
			$this->setSection($data['section']);
			$this->setAction($data['action']);
			$this->setLink($data['link']);
			$this->setGenID($data['genID']);
			$this->setDateAdded($data['dateAdded']);	
		}
		
		/**SETTERS**/
		private function setPendingNotificationID($param=0){
			$this->mPendingNotificationID = $param;	
		}
		
		public function setContext($param=null){
			$this->mContext = $param;
		}
		
		public function setContextID($param=0){
			$this->mContextID = $param;
		}
		
		public function setSection($param=null){
			$this->mSection = $param;
		}
		
		public function setAction($param=null){
			$this->mAction = $param;
		}
		
		public function setLink($param=null){
			$this->mLink = $param;
		}
		
		public function setGenID($param=0){
			$this->mGenID = $param;
		}
		
		private function setDateAdded($param=null){
			$this->mDateAdded = $param;
		} 
		
		public function addRecipient($recipient){
			if(!PendingNotificationRecipient::isValidRecipient($recipient)){
				throw new exception('Invalid recipient '.$recipient.' passed to object of type PendingNotification.');
			}
			if($this->_isNewEntry()){
				if(!in_array($recipient,$this->mRecipients)){
					$this->mRecipients[] = $recipient;
				}
			}
			else{
				//add directly to db
				if(!PendingNotificationRecipient::isRecipientExists($recipient,$this->getPendingNotificationID())){
					$rcpt = new PendingNotificationRecipient();
					$rcpt->setPendingNotificationID($this->getPendingNotificationID());
					$rcpt->setRecipient($recipient);
					$rcpt->save();
				} 
			}
		}
		
		/*** getters ***/
		public function getPendingNotificationID(){
			return $this->mPendingNotificationID;
		}
		
		public function getContext(){
			return $this->mContext;
		}
		
		public function getContextID(){
			return $this->mContextID;
		}
		
		public function getSection(){
			return $this->mSection;
		}
		
		public function getAction(){
			return $this->mAction;
		}
		
		public function getGenID(){
			return $this->mGenID;
		}
		
		public function getLink(){
			return $this->mLink;
		}
		
		public function getDateAdded(){
			return $this->mDateAdded;
		}
		
		public function isGroupContext(){
			return $this->getContext() == self::GROUP_CONTEXT;
		}
		
		public function isTravelerContext(){
			return $this->getContext() == self::TRAVELER_CONTEXT;
		}
		
		private function _isNewEntry(){
			return $this->getPendingNotificationID() === 0;
		}
		
		public function getRecipients(){
			return (
			 	$this->_isNewEntry()
				 	? $this->mRecipients 
				 	: PendingNotificationRecipient::getRecipientsByPendingNotificationID($this->getPendingNotificationID()) 
			 );			
		}
		
		private function clearData(){
			$this->mPendingNotificationID 	= 0;
			$this->mContext 				= 0;
			$this->mContextID 				= 0;
			$this->mSection 				= null;
			$this->mAction 					= null;
			$this->mLink					= null;
			$this->mGenID 					= 0;
			$this->mDateAdded 				= null;
			$this->mRecipients				= array();
		}
		
		/*** CRUDE ***/
		public function save(){
			if(!self::isValidContext($this->getContext())){
				throw new exception('Cannot perform save operation. Invalid context passed to object of type PendingNotification.');
			}
			if(!is_numeric($this->getContextID()) || 0==$this->getContextID()){
				throw new exception('Cannot perform save operation. Invalid contextID '.$this->getContextID().' passed to object of type PendingNotification.');
			}
			if(!self::isValidSection($this->getSection())){
				throw new exception('Cannot perform save operation. Invalid section '.$this->getSection().' passed to object of type PendingNotification.');
			}
			if( !self::isValidAction($this->getSection(),$this->getAction() )){
				throw new exception('Cannot perform save operation. Invalid action '.$this->getAction().' passed to object of type PendingNotification.');
			}
			if($this->_isNewEntry()){//add
				$qry =	'INSERT INTO tblPendingNotification(' .
							'`context`, ' .
							'`contextID`, ' .
							'`section`, ' .
							'`action`, ' .
							'`link`, ' .
							'`genID`, ' .
							'`dateAdded`' .
						')VALUES(' .
							ToolMan::makeSqlSafeString($this->getContext()) .', ' .
							ToolMan::makeSqlSafeString($this->getContextID()) .', ' .
							ToolMan::makeSqlSafeString($this->getSection()) .', ' .
							ToolMan::makeSqlSafeString($this->getAction()) .', ' .
							ToolMan::makeSqlSafeString($this->getLink()) .', ' .
							ToolMan::makeSqlSafeString($this->getGenID()) .', ' .
							ToolMan::makeSqlSafeString(date("Y-m-d G:i:s")) .
						')';
				$this->mDb->execQuery($qry);
				$this->setPendingNotificationID($this->mDb->getLastInsertedID());
				//save the stored recipients...
				foreach($this->mRecipients as $iRcpt){
					$this->addRecipient($iRcpt);
				}
			}
			else{//update
				$qry =	'UPDATE tblPendingNotification SET ' .
							'`context` 		=' 	.ToolMan::makeSqlSafeString($this->getContext() ).','.
							'`contextID` 	='	.ToolMan::makeSqlSafeString($this->getContextID() ).',' .
							'`section` 		='	.ToolMan::makeSqlSafeString($this->getSection() ).',' .
							'`action` 		='	.ToolMan::makeSqlSafeString($this->getAction() ).',' .
							'`link` 		='	.ToolMan::makeSqlSafeString($this->getLink() ).',' .
							'`genID` 		='	.ToolMan::makeSqlSafeString($this->getGenID() ).' ' .
						'WHERE `pendingNotificationID` =' 	.ToolMan::makeSqlSafeString($this->getPendingNotificationID());
				$this->mDb->execQuery($qry);
			}
		}
		/**
		 * Deletes a particular pending notification
		 */
		public function delete(){
			$qry =	'DELETE FROM tblPendingNotification ' .
					'WHERE pendingNotificationID = '.ToolMan::makeSqlSafeString($this->getPendingNotificationID());
			$this->mDb->execQuery($qry);
			PendingNotificationRecipient::deleteRecipientsByPendingNotificationID($this->getPendingNotificationID());
			$this->clearData();
			//clear the recipients			
		}
		
		/**
		 * Returns the pending notifications of a certain traveler.
		 * @param int $param : The travelerID of the traveler whose pending notifications is to be retrieved.
		 */
		public static function getPendingNotificationsByTravelerID($param=0){
			$db = new dbHandler();
			$qry =	'SELECT pendingNotificationID, context, contextID, section, action, link, genID, dateAdded ' .
					'FROM tblPendingNotification ' .
					'WHERE context = '.ToolMan::makeSqlSafeString(self::TRAVELER_CONTEXT).' ' .
					'AND contextID = '.ToolMan::makeSqlSafeString($param);
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new PendingNotification(null,$rs->retrieveRow()); 
				}
			}
			return $ar;
		}
		
		/**
		 * Returns the pending notificactions of a certain group.
		 * @param int $param : The groupID of the group whose pending notifications os to be retrieved.
		 */
		public static function getPendingNotificationsByGroupID($param=0){
			$db = new dbHandler();
			$qry =	'SELECT pendingNotificationID, context, contextID, section, action, link, genID, dateAdded ' .
					'FROM tblPendingNotification ' .
					'WHERE context = '.ToolMan::makeSqlSafeString(self::GROUP_CONTEXT).' ' .
					'AND contextID = '.ToolMan::makeSqlSafeString($param);
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new PendingNotification(null,$rs->retrieveRow()); 
				}
			}
			return $ar;
		}
		
		/**
		 * Returns the travelerID that are pending notificaltions.
		 */
		public static function getTravelerSubjects(){
			$db = new dbHandler();
			$qry = 	'SELECT DISTINCT contextID ' .
					'FROM tblPendingNotification ' .
					'WHERE context = '.	ToolMan::makeSqlSafeString(self::TRAVELER_CONTEXT);
			$rs = new iRecordset($db->execQuery($qry));
			return ( $rs->retrieveRecordCount() > 0 ? $rs->retrieveColumn('contextID') : array()); 
		}
		
		/**
		 * Returns the contextIDs (travelerID/groupID) that are present in the tblPendingNotification
		 */
		public static function getGroupSubjects(){
			$db = new dbHandler();
			$qry = 	'SELECT DISTINCT contextID ' .
					'FROM tblPendingNotification ' .
					'WHERE context = '.	ToolMan::makeSqlSafeString(self::GROUP_CONTEXT);
			$rs = new iRecordset($db->execQuery($qry));
			return ( $rs->retrieveRecordCount() > 0 ? $rs->retrieveColumn('contextID') : array());
		}
		
		public static function getSubjects(){
			return array_merge(self::getTravelerSubjects(),self::getGroupSubjects());
		}
		
		private static function isValidContext($param=null){
			return self::TRAVELER_CONTEXT == $param || self::GROUP_CONTEXT == $param;
		}
		
		/*** TRICKY ***/
		private static function isValidSection($param=null){
			return array_key_exists($param, self::$NotificationSections);
		}
		
		private static function isValidAction($section,$action){
			if(self::isValidSection($section)){
				return array_key_exists($action,self::$NotificationSections[$section]['actions']);	
			}
			return false;
		}
		
		private static $NotificationSections = array(
			'PHOTO' => array(
				'actions' => array(
					'UPLOAD' => 'has uploaded new photos'	
				)
			),
			'JOURNAL_ENTRY' => array(
				'actions' => array(
					'ADD' => 'has added new journal entries'	
				)
			), 
		);
		/*** TRICKY ***/
	}
?>