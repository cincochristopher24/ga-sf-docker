<?php
	/**
	 * @(#) Class.AdminGroup.php
	 * 
	 * @package goabroad.net
	 * @subpackage model
	 * 
	 * @author Czarisse Daphne P. Dolina
	 * @version 1.0 September 6, 2006
	 * @version 2.0 October 27, 2008 Cheryl Ivy Q. Go
	 * @version 3.0 February 18, 2009 Antonio Pepito Cruda Jr.
	 */

	require_once("travellog/model/Class.Group.php");
	require_once("travellog/model/Class.PhotoAlbum.php");
	require_once("travellog/model/Class.Program.php");
	require_once("travellog/model/Class.News.php");
	require_once("travellog/model/Class.ResourceFiles.php");
	require_once("travellog/model/Class.GroupEvent.php");
	require_once("travellog/model/Class.OnlineForm.php");
	require_once("travellog/model/Class.RowsLimit.php");
	require_once("travellog/model/Class.MessageSpace.php");
	require_once("travellog/model/Class.AdvisorGroupInfo.php");
	require_once("travellog/model/formeditor/Class.SurveyForm.php");
	require_once("travellog/model/formeditor/Class.SurveyParticipant.php");
	require_once("travellog/model/Class.ObjectsRepo.php");
	require_once("Class.dbHandler.php");
	require_once("travellog/helper/Class.SqlUtil.php");
	require_once("travellog/model/Class.SubGroupFactory.php");
	require_once("travellog/model/Class.SubGroupSortType.php");
	require_once("travellog/model/Class.SubGroupRankHandler.php");
	require_once("travellog/model/Class.CustomizedSubGroupCategory.php");
	require_once("Class.SqlResourceIterator.php");
	require_once("travellog/model/Class.VideoAlbum.php");
	
	require_once("travellog/model2/group/Class.ganetGroupService.php");	// could be removed in future after refactoring the services
	require_once("travellog/vo/Class.ganetValueObject.php");
	require_once("travellog/model2/dao/Class.ganetAdminGroupDao.php");
	require_once('Class.ConnectionProvider.php');
	require_once("travellog/model/Class.GroupPeer.php");
	
	/**
	 * Handler class for Advisor groups.
	 */
	class AdminGroup extends Group {  
		/**
		 * constants
		 */
		const STAFF = 1;
		const MODERATOR = 2;
		
		protected $mParentID = 0;
		private $mIsParent = NULL;
		private $mParent = NULL;
		private $mIsSubGroup = NULL;
		private $mResourceFiles = array();
		private $mNews = array();
		private $mPrograms = array();
		private $mCalendarEvents = array();
		private $mPhotoAlbums = array();
		private $mTravels = array();
		private $mSubGroups = null;
		private $mSubGroupCount = null;
		private $mSubGroupCategoriesRankSettings = 0;
		private $mProjectCount = 0;
		private $mProjects = array();
		private $mSubGroupsAndProjectsCount = 0;
		private $mSubGroupsAndProjects = array();
  		private $mClientID = 0;
		private $mGroupEmail = '';
		private $mModeratorID = 0;
   		private $mAdvisorGroupInfo = NULL;
    	private $mOnlineSurveyForm = NULL;
    	private $mIsForDelete = false;
		protected $mSubGroupRank = 0;
    	protected $mFeaturedSubGroupRank = 0;
		private $mRankSettings = 0;
	    public  $resourcefiletype	= 1;
    
    	public 	$props = array();				// temp by kgordo 4/6/09, properties set when instanting group
		protected $dao = null;    
        
		private $mTravelerOwned = false; // var for group events
		
		private $mGroupHomeFeatured = 0;
		private $mGroupHomeFeaturedRank = 0;
		
		/**
		 * Constructs the admin group in two ways. First, if the argument passed is an 
		 * integer type this uses the integer as the ID of the group to query in the 
		 * database. If the argument is an array type, this array will be used to initialize
		 * the values of this group's properties.
		 * 
		 * @param array|integer $param The values for the properties of the group or the ID of the group.
		 */       
		function __construct($param = 0){                
			try {
				$this->mConn = ConnectionProvider::instance()->getConnection();
				$this->mRs   = new Recordset($this->mConn);
			}
			catch (Exception $e) {                 
				throw $e;
			}
			
			$this->dao	=	new ganetAdminGroupDao();
                
			if (is_numeric($param) AND 0 < $param ){
				/*$handler = new dbHandler();
				
				$sql = "SELECT * FROM tblGroup WHERE " .
					" `groupID` = ".$handler->makeSqlSafeString($param);
					
				$it = new SqlResourceIterator($handler->execute($sql));
				
				if ($it->hasNext()) {
					$this->initialize($it->next());
					
					$sql = "SELECT clientID FROM tblGrouptoAdvisor WHERE " .
						" `groupID` = ".$handler->makeSqlSafeString($param);
					
					$it = new SqlResourceIterator($handler->execute($sql));
					
					if ($it->hasNext()) {
						$this->mClientID = $it->clientID();
					}
				}*/
				
				
				
				$gServ = new ganetGroupService();
				$vo = new ganetValueObject();
				$vo->set('id',$param);
				$group = $gServ->retrieve($vo);
				
				$this->setAdminGroupData($group->props);			// temporary 4/6/09 by kgordo
				
				//echo '<pre>';
				//print_r($group->getInstanceProps());exit;
				
				
			}
			else if (is_array($param)) {
				$this->initialize($param);
			}                
		}
		
		/**
		 * Returns the properties of the group.
		 * 
		 * @return mixed[] the properties of the group
		 */
		function getAdminGroupData(){
			$props = array();
			$props['administrator'] = $this->mAdministratorID;
			$props['datecreated'] = $this->mDateCreated;
			$props['description'] = $this->mDescription;
			$props['discriminator'] = $this->mDiscriminator;
			$props['groupaccess'] = $this->mGroupAccess;
			$props['groupID'] = $this->mGroupID;
			$props['inverserank'] = $this->mGroupRank;
			$props['name'] = $this->mName;
			$props['photoID'] = $this->mGroupPhotoID;
			$props['sendableID'] = $this->mSendableID;
			$props['parentID'] = $this->mParentID;
			$props['moderator'] = $this->mModeratorID;
			$props['ranksettings'] = $this->mRankSettings;
			$props['clientID'] = $this->mClientID;
			$props['administratorClientID'] = $this->mAdministratorClientID;
			$props['subgroupcategoryranksettings'] = $this->mSubGroupCategoriesRankSettings;					
			$props['groupHomeFeatured'] = $this->mGroupHomeFeatured;
			$props['groupHomeFeaturedRank'] = $this->mGroupHomeFeaturedRank;
			return $props;
		}
      
		/**
		 * Initializes the admin group's attributes using the given array.
		 * 
		 * @param array $props The values for the group's attributes.
		 * 
		 * @return void
		 */
		protected function initialize(array $props){
			$this->props = array_merge($this->props,$props);					// 4/6/09 kgordo NOTE: This is temporary, should be removed after refactor 
			
			if (0 < count($props)) {
				parent::initialize($props);
				$this->mDiscriminator = 2;
				$this->mModeratorID = isset($props['moderator']) ? $props['moderator'] : $this->mModeratorID;
	 			$this->mParentID = isset($props['parentID']) ? $props['parentID'] : $this->mParentID; 
				$this->mRankSettings = isset($props['ranksettings']) ? $props['ranksettings']  : $this->mRankSettings;
				$this->mClientID = isset($props['clientID']) ? $props['clientID'] : $this->mClientID;
				$this->mAdministratorClientID = isset($props['administratorClientID']) ? $props['administratorClientID'] : $this->mAdministratorClientID;
				$this->mSubGroupCategoriesRankSettings = isset($props['subgroupcategoryranksettings']) ? $props['subgroupcategoryranksettings'] : $this->mSubGroupCategoriesRankSettings;
				$this->mIsParent = 0 < $this->mParentID ? false :  true;
				$this->mGroupHomeFeatured = isset($props['groupHomeFeatured']) ? $props['groupHomeFeatured'] : 0 ;
				$this->mGroupHomeFeaturedRank = isset($props['groupHomeFeaturedRank']) ? $props['groupHomeFeaturedRank'] : 0 ;
			}			
		}
		
           	
		/**
		 * Initializes the admin group's attributes using the given array.
		 * 
		 * @param array $props The values for the group's attributes.
		 * 
		 * @deprecated 3.0 - February 18, 2009
		 * @see #initialize
		 * 
		 * @return void
		 */
		function setAdminGroupData(array $props) {
			if (0 < count($props)){
				$this->initialize($props);
			}
		}
		
		/**
		 * Sets the ID of the parent group.
		 * 
		 * @param integer $id The ID of the parent of the group.
		 * 
		 * @return void
		 */
		function setParentID($id){
			$this->mParentID = $id;
		}
		
		/**
		 * Sets the ID of the related client.
		 * 
		 * @param integer $clientID The ID of the related client.
		 * 
		 * @return void
		 */      
		function setClientID($clientID){
			$this->mClientID = $clientID;
		}
            
            
            function setInstitutionInfo($_advisorGroupInfo){
            	$_advisorGroupInfo->Save();
            }
            
			function setSubGroupRank($_subGroupRank){
                $this->mSubGroupRank = $_subGroupRank;
            }

			function setFeaturedSubGroupRank($_featSubGroupRank){
                $this->mFeaturedSubGroupRank = $_featSubGroupRank;
            }

			function setRankSettings($_rankSettings){
                $this->mRankSettings = $_rankSettings;
            }

			function setGroupEmail($_groupEmail){
				$this->mGroupEmail = $_groupEmail;
			}
			
			function setSubGroupCategoriesRankSettings($rank){
				$this->mSubGroupCategoriesRankSettings = $rank;
			}

		/**
         * Getter Functions
         */

            function getParentID(){
                return $this->mParentID;
            }

			function getSubGroupRank(){
                return $this->mSubGroupRank;
            }

			function getFeaturedSubGroupRank(){
                return $this->mFeaturedSubGroupRank;
            }
				
			function getRankSettings(){
                return $this->mRankSettings;
            }
            
			function getSubGroupCategoriesRankSettings(){
				return $this->mSubGroupCategoriesRankSettings;
			}
			
			function getGroupEmail(){
				return $this->mGroupEmail;
			}
        
        /**
         * CRUD Methods (Create, Update , Delete)
         * In Create, add a record first in tblSendable before creating an Admin Group.
         * In Delete(), all photoalbums, programs, news and resource files associated with this admin group must be deleted also.
         */    
        
		function Create(){
      try {
	      $handler = new dbHandler();      
				
				$sql = "INSERT into tblSendable (sendabletype) " .
	        "VALUES ('2')";
	      $handler->execute($sql);
                
       	$sql = "INSERT INTO tblGroup " .
       		" (sendableID, name, description, groupaccess, discriminator, administrator, moderator, parentID, photoID, administratorClientID, copyingStatus) " .
	        "	VALUES (" .
	        "'".$handler->getLastInsertedID()."'," .
	        "".$handler->makeSqlSafeString($this->mName).", " .
	        "".$handler->makeSqlSafeString($this->mDescription).", " .
	        "".$handler->makeSqlSafeString($this->mGroupAccess).", " .
	        "".$handler->makeSqlSafeString($this->mDiscriminator)."," .
	        "".$handler->makeSqlSafeString($this->mAdministratorID).", " .
	        "	0, " .
	        "".$handler->makeSqlSafeString($this->mParentID).", " .
	        "".$handler->makeSqlSafeString($this->mGroupPhotoID).", " .
					"".$handler->makeSqlSafeString($this->mAdministratorClientID)."," .
					"".$handler->makeSqlSafeString($this->mCopyingStatus)."".
	        "	)";
	      
	      $handler->execute($sql);
	      $this->mGroupID = $handler->getLastInsertedID();
	
	      if (0 == $this->mParentID){
          /*$sql = " UPDATE tblGrouptoAdvisor " .
            "	SET groupID = ".$handler->makeSqlSafeString($this->mGroupID).
            "	WHERE clientID = ".$handler->makeSqlSafeString($this->mClientID).
            "	AND travelerID = ".$handler->makeSqlSafeString($this->mAdministratorID);
	        $handler->execute($sql); */
	  		$sql = "INSERT INTO tblGrouptoAdvisor(groupID,travelerID,clientID)
	 				VALUES('$this->mGroupID','$this->mAdministratorID','$this->mAdministratorClientID')";
			$handler->execute($sql);
			
			$this->UpdateAdvisorAsTraveler();
          }
	                
	      $sql2 = "UPDATE tblGroup SET inverserank = ".$handler->makeSqlSafeString($this->mGroupID). 
					" WHERE groupID = ".$handler->makeSqlSafeString($this->mGroupID);
        $handler->execute($sql2);
      }
      catch(exception $ex){
      	throw $ex;
      }
    }
            
    function Update(){            
      /*$handler = new dbHandler();
      $Aname = $handler->makeSqlSafeString($this->mName);
      $Adescription = $handler->makeSqlSafeString($this->mDescription);
                
      $sql = "UPDATE tblGroup SET name = $Aname, description = $Adescription, groupaccess = '$this->mGroupAccess' WHERE groupID = '$this->mGroupID'  " ;
      $this->mRs->Execute($sql);*/          
        
      $this->dao->update($this);
        
                
		  // if email add entered is different from the current, and considering that it has been validated as a unique entry, then update email record.	
				
      	if ( '' != trim($this->mGroupEmail) && 0 != strcasecmp(trim($this->mGroupEmail) , $this->getAdministrator()->getTravelerProfile()->getEmail())) {
        	$AGroupEmail = $handler->makeSqlSafeString($this->mGroupEmail);	
 			
 			$tp	=	$this->getAdministrator()->getTravelerProfile();
 			$tp->setEmail($AGroupEmail);
 			$tp->Update();
        
        	/*$sql = "UPDATE tblTraveler SET email = $AGroupEmail WHERE travelerID = '$this->mAdministratorID'  " ;
		    	$this->mRs->Execute($sql);*/
	 	}   
    }

	//invoked when an advisor group is Deleted or Created
	//to update isadvisor field in tblTraveler
	function UpdateAdvisorAsTraveler(){
		try{
			$this->getAdministrator()->updateIsAdvisor();
		}catch(exception $e){}
	}
            
            
    function Delete(){
      $myresourcefiles = $this->getResourceFiles();      
      $myprograms = $this->getPrograms();
      $myphotoalbums = $this->getPhotoAlbums();
      $mysubgroups = $this->getSubGroups();


                for($idx=0;$idx<count($myresourcefiles);$idx++){
                	$eachresourcefile = $myresourcefiles[$idx];  
                    $eachresourcefile->Delete();                                            // delete resource files of this admin group
                }

                for($idx=0;$idx<count($myprograms);$idx++){
                	$eachprogram = $myprograms[$idx]; 
                    $eachprogram->Delete();                                                 // delete programs of this admin group
                }


                for($idx=0;$idx<count($myphotoalbums);$idx++){
                	$eachphotoalbum = $myphotoalbums[$idx]; 
                    $eachphotoalbum->Delete();                                              // delete photo albums of this admin group
                }


                for($idx=0;$idx<count($mysubgroups);$idx++){
                	$eachsubgroup = $mysubgroups[$idx]; 
                    $eachsubgroup->Delete();                                              // delete subgroups of this admin group
                }

				/*** Added By Naldz (June 02, 2008) - START BLOCK ***/

				//delete all surveys of this groups
				$groupSurveys = SurveyForm::getGroupSurveyForms($this->mGroupID);
				foreach($groupSurveys as $iGroupSurvey){
					$iGroupSurvey->delete();
				}

				//delete all represented survey groupParticipant of this group
				$particpatedGroupSurveys = SurveyParticipant::getParticipatedSurveysOfGroupByGroupID($this->mGroupID);
				foreach($particpatedGroupSurveys as $iParticipatedGroupSurvey){
					$groupParticipant = $iParticipatedGroupSurvey->getGroupParticipant($this->mGroupID);
					if(!is_null($groupParticipant)){
						$groupParticipant->delete();
					}
				}
				/*** Added By Naldz (June 02, 2008) - END BLOCK ***/

				/******************* START:: Added By: Cheryl Ivy Q. Go		17 October 2008 ********************/
					require_once 'travellog/model/Class.Traveler.php';
					require_once 'travellog/model/Class.Staff.php';

					// Delete all staff of group
					Staff::DeleteAllGroupStaff($this->mSendableID);
				/******************** END:: Added By: Cheryl Ivy Q. Go		17 October 2008 ********************/

            	$this->removeAllGroupLinks();

            	$travels = $this->getTravels();
				// temporarily disabled function
            	// foreach($travels as $travel){
            	// 	$travel->Delete();
            	// }

				if( 0 == $this->getParentID() ){//update isadvisor when deleting a parent group only
					$this->UpdateAdvisorAsTraveler();
				}
				
				$this->dao->delete($this);
  
            	// added by K. Gordo. Delete entries in tblGroupApprovedJournals with this groupID
	 			/*$sql = "DELETE FROM tblGroupApprovedJournals where groupID = '$this->mGroupID' ";
	 			$this->mRs->Execute($sql);	

	 			$sql = "DELETE FROM tblTraveltoGroup where groupID = '$this->mGroupID' ";
	 			$this->mRs->Execute($sql);	

				$sql = "DELETE FROM tblSendable WHERE sendableID = " . $this->mSendableID;
				$this->mRs->Execute($sql);

            	$sql = "DELETE FROM tblGroup where groupID = '$this->mGroupID' ";           // delete selected admin group
				$this->mRs->Execute($sql);*/

				//added by JUL to remove feeds related to a group
				$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				require_once("travellog/model/Class.ActivityFeed.php");
				$GROUP = ActivityFeed::GROUP;
				$sql ="DELETE FROM tblActivityFeed WHERE sectionType = $GROUP AND groupID = ".$this->mGroupID;
				$this->mRs->Execute($sql);

				$this->invalidateCacheKeys($this->mGroupID);
            }
           
         
        /**
         * Purpose: This is called when traveler is added as a member of a group.
         */
            function addMember($traveler){                
				if (!$this->isMember($traveler)) {
                	
					if (GroupAccess::$OPEN == $this->mGroupAccess)
	                	$isopenjoin = 1;
	                else
	                	$isopenjoin = 0;
                	
	                $travID = $traveler->getTravelerID();  
	                $now  = date("Y-m-d H:i:s");

	                $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	                $sql = "INSERT into tblGrouptoTraveler (groupID, travelerID, adate) VALUES ('$this->mGroupID', '$travID', '$now')";
	                $this->mRs->Execute($sql);

	                // if this is a subgroup, and traveler is not a member of the parent group,
	                if ($this->isSubGroup() && !$this->getParent()->isMember($traveler)){
						// get status of invitation (0 or 2=pending as staff of group)
						$iStatus = Group::getInvitationToGroupStatus($this->getParent()->getGroupID(), $travID);

						if (2 == $iStatus){
							MessageSpace::addBulletinsPostedByAdvisor($this->getParent(), $traveler);
							// set as staff of parent group
							$this->getParent()->addStaff($travID);
						}

						// then add traveler as member of parent group, and remove from parent groups' invite and request list             	
						$this->getParent()->addMember($traveler);
	                }
	
					require_once("travellog/model/Class.ActivityFeed.php");
					$memberBy = ActivityFeed::MEMBER_BY_DEFAULT;
					$date = $this->isInInviteList($traveler);
					if( $date  ){
						$memberBy = ActivityFeed::MEMBER_BY_INVITE;
					}else if( $this->isInRequestList($traveler) ){
						$params = array("FEED_SECTION"		=>	ActivityFeed::JOINGROUPREQUEST,
										"SECTION_TYPE"		=>	ActivityFeed::GROUP,
										"TRAVELER_ID"		=>	$travID,
										"GROUP_ID"			=>	$this->getGroupID() );
						$feeds = ActivityFeed::getInstances($params);
						if( 0 < $feeds["recordCount"] ){
							$date = $feeds["activityFeeds"][0]->getFeedDate();
						}
						$memberBy = ActivityFeed::MEMBER_BY_REQUEST;
					}else{
						$date = NULL;
					}
					
					try{
						ActivityFeed::create($traveler,$this,$memberBy,$date);
					}catch(exception $e){}
	
	
					$this->removeFromInviteList($traveler->getTravelerID());
					$this->removeFromRequestList($traveler->getTravelerID());
					$this->removeFromEmailInviteList($traveler->getTravelerProfile()->getEmail());

					// remove from tblEmailInviteList and insert to tblInviteList
					$this->convertEmailInviteList($traveler->getTravelerID(), $traveler->getTravelerProfile()->getEmail());
				
					if( 0 < $this->getParentID() ){
						$this->invalidateCacheKeys($this->getParentID());
					}
					$this->invalidateCacheKeys($this->getGroupID());
				}				               
            }

            
        /**
         * Purpose: This is called when traveler is removed as a member of a group.
         */
            function removeMember($travelerID){      
                $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            	$sql = "DELETE FROM tblGrouptoTraveler WHERE groupID = '$this->mGroupID' AND travelerID = '$travelerID' ";
                $this->mRs->Execute($sql);

				// edited by Jul to remove member from projects/programs
				if( $this->isParent() ){
					$traveler = new Traveler($travelerID);
					$subGroupsAndProjects = $this->getSubGroupsAndProjects(NULL,NULL,0,NULL);
					if( 0 < $subGroupsAndProjects[1] ){
						foreach( $subGroupsAndProjects[0] AS $subGroup ){
							if( $subGroup->isMember($traveler) ){
								$subGroup->removeMember($travelerID);
							}
						}
					}
					$this->invalidateCacheKeys($this->getParentID());
				}else{
					$this->getParent()->removeStaff($travelerID);
				}

                $this->removeFromInviteList($travelerID);	 	// remove from invite list
			    $this->removeFromRequestList($travelerID); 		// remove from request list
				$this->removeStaff($travelerID);			// remove if it is a staff of this group
				
				$this->removeGroupRelatedJournalsOfTraveler($travelerID);
				
				//added by Jul to remove feeds of a traveler related to this group
				require_once("travellog/model/Class.ActivityFeed.php");
				$GROUP = ActivityFeed::GROUP;
				$groupID = $this->getGroupID();
				$sql = "DELETE FROM tblActivityFeed
						WHERE sectionType = $GROUP AND groupID = $groupID AND travelerID = '$travelerID'";
				$this->mRs->Execute($sql);
				
				$this->invalidateCacheKeys($this->getGroupID());
            }
    
		    /**
		     * Returns true if this group is a parent group meaning its parent group is zero.
		     * 
		     * @return boolean true if this group is a parent group meaning its parent group is zero.
		     */
				function isParent(){
					return (0 < $this->mParentID) ? false : true;
				}
        
        /**
         * Sets the parent group if this is a subgroup.
         * 
         * @param AdminGroup $parent The parent of this subgroup.
         * 
         * @return void
         */
        function setParent(AdminGroup $parent){
        	$this->mParent = $parent;
        }   
            
        /**
         * Retrieves the parent admin group of this group if this is a subgroup.
         * 
         * @throws exception
         * @return NULL|AdminGroup Returns null when this group is not a subgroup meaning the parent ID is 0 in the database.
         */  
		function getParent(){
			
			if (NULL == $this->mParent && 0 < $this->mParentID ){
				$handler = new dbHandler();
				
				/*$sql = "SELECT parentID from tblGroup " .
						" WHERE groupID = ".$handler->makeSqlSafeString($this->mGroupID).
						" AND parentID > 0 ";
            	$sql = "SELECT * FROM tblGroup WHERE groupID IN ($sql)";*/
				$sql = "SELECT *
						FROM tblGroup
						WHERE groupID = $this->mParentID";
           
            	$rs = new iRecordset($handler->execute($sql));
           
            	if ($rs->retrieveRecordCount()) {
            		$this->mParent = new AdminGroup();
            		$this->mParent->setAdminGroupData($rs->current());
            	}            
          	}
               
        	return $this->mParent;
        }
            
        
        /**
         * Purpose: Checks if this admin group is a subgroup
         */              
            function isSubGroup(){
                
                if (NULL == $this->mIsSubGroup){
                    
                	/*$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
                	
                    $sql = "SELECT parentID from tblGroup WHERE groupID = '$this->mGroupID' AND parentID > 0 ";
                    $this->mRs->Execute($sql);
                    
                    if (0 == $this->mRs->Recordcount())
                        $this->mIsSubGroup = false;  
                    else 
                        $this->mIsSubGroup = true;*/
                    $this->mIsSubGroup =  0 < $this->mParentID ? TRUE : FALSE;
                }
                
                return $this->mIsSubGroup;
            }
            
            
        /**
         * Purpose: Adds a subgroup to this admin group.
         * To check for validity, the group (to which the subgroup is to be added) must not be a subgroup.
         * If valid, then add group as subgroup to this parent group.
         */  
            function addSubGroup($subgroup){
                
                if (FALSE == $this->isSubGroup()) {                        
                    $subgroup->setParent($this->mGroupID);             // set this groupID as parentID of subgroup
                    $subgroup->Create();                               // create subgroup
                
					$this->invalidateCacheKeys($this->mGroupID);
				}
            }
           
        /**
         * Purpose: Removes a subgroup from this admin group.
         */  
            function removeSubGroup($groupID){
                
                 if (TRUE == $this->isParent()) {                      // if this admin group is a parent (has a subgroup already)
                    $subgroup = new AdminGroup($groupID);              // instantiate subgroup
                    $subgroup->Delete();                               // delete this subgroup                    
                
					$this->invalidateCacheKeys($this->mGroupID);
				}
            }
            
        /**
         * Purpose: Returns an array of subgroups of this admin group with category
         * 			if category = null, then all subgroups of admin group that has no category
         * @see CustomizedSubGroupCategory
         */
         	function getSubGroupsWithCategory($category = NULL) {
         		$handler = new dbHandler();
         		$sql = "SELECT DISTINCT * FROM `tblGroup`";
         		if((!is_null($category) && $category instanceof CustomizedSubGroupCategory) ) {
     				$sql .= ",`tblGroupToSubGroupCategory` WHERE" .
     						" `tblGroup`.`groupID` = `tblGroupToSubGroupCategory`.`groupID`" .
     						" AND `tblGroupToSubGroupCategory`.`categoryID` = '".$category->getCategoryID()."'";
     			}
         		else {
         			//$query = "SELECT count(categoryID) FROM `tblGroupToSubGroupCategory`";
         			//$this->mRs->Execute($query);
         			//$result = mysql_fetch_assoc($this->mRs->Resultset());	
         			//if( (0 < $result['count(categoryID)']) )
         			//$sql .= ",`tblSubGroupCategory`,`tblGroupToSubGroupCategory` WHERE" .
         		    $sql .=	" WHERE `tblGroup`.`groupID` NOT IN" .
         					" ( SELECT DISTINCT `tblGroupToSubGroupCategory`.`groupID` FROM `tblGroupToSubGroupCategory`,`tblSubGroupCategory`" .
         					" WHERE `tblSubGroupCategory`.`categoryID` = `tblGroupToSubGroupCategory`.`categoryID`" .
         					" AND `tblSubGroupCategory`.`parentID` = '$this->mGroupID' )";
         			//else
         			//	$sql .= " WHERE 1";
         		}
         		$sql .= " AND `tblGroup`.`parentID` = '$this->mGroupID' ORDER BY name";
         		//$this->mRs->Execute($sql);;
         		$rs = new iRecordset($handler->execute($sql));
         		$arrSubGroups = array();
				if (0 < $rs->retrieveRecordCount()){
					while(!$rs->EOF()){
						try {
							$subgroup = new AdminGroup();
							$subgroup->setAdminGroupData($rs->current());						
	                    }
	                    catch (Exception $e) {
	                       throw $e;
	                    }
	                    $arrSubGroups[] = $subgroup;
	                    $rs->next();
					}
				}
				return $arrSubGroups;
         	}
            
        /**
         * Purpose: Adds photo album for this group.
         */
            function addPhotoAlbum($photoAlbum){
                
                $photoAlbum->setGroupID($this->mGroupID);                          // set this groupID for photo album
                $photoAlbum->Create();                                             // create photo album
            }
            
            
        /**
         * Purpose: Removes a photo album from this group.
         */
            function removePhotoAlbum($photoAlbumID){
                
                $photoAlbum = new PhotoAlbum($photoAlbumID);                         // instantiate photo album
                $photoAlbum->Delete();                                              // delete photo album
            }
            
            
        /**
         * Purpose: Returns an array of photo albums of this group.
         * Rows Limit is enabled for this function.
         */
            function getPhotoAlbums($rowslimit = NULL){
             	$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());  
            	$this->mPhotoAlbums = array();
	
               if ($rowslimit instanceof RowsLimit){
                    $offset   = $rowslimit->getOffset();
                    $rows     = $rowslimit->getRows();
                    $limitstr = " LIMIT " . $offset . " , " . $rows;
                } 
                else
                    $limitstr  = '';  
                
                //$sql = "SELECT photoalbumID FROM tblPhotoAlbum WHERE groupID = '$this->mGroupID'  ORDER BY photoalbumID DESC" . $limitstr;
                $sql = "SELECT * FROM tblPhotoAlbum WHERE groupID = '$this->mGroupID'  ORDER BY photoalbumID DESC" . $limitstr;
                $this->mRs->Execute($sql);
                
                while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                    try {
                        //$photoalbum = new PhotoAlbum($recordset['photoalbumID']);
                        $photoalbum = new PhotoAlbum();
                        $photoalbum->setPhotoAlbumID($recordset['photoalbumID']);
                        $photoalbum->setTitle($recordset['title']);
                        $photoalbum->setCreator($recordset['creator']);
                        $photoalbum->setLastUpdate($recordset['lastupdate']);
                        $photoalbum->setGroupID($recordset['groupID']);
                        $photoalbum->setPrimaryPhotoID($recordset['primaryPhoto']);
						if( isset($recordset["isFeatured"]) ){
                        	$photoalbum->setIsFeatured($recordset["isFeatured"]);
						}
                    }
                    catch (Exception $e) {
                       throw $e;
                    }
                    $this->mPhotoAlbums[] = $photoalbum;
                }
                
                return $this->mPhotoAlbums;
            }
            
        	function getPhotoAlbumCount(){
				$sql = "SELECT photoalbumID
						FROM tblPhotoAlbum
						WHERE groupID = ".$this->mGroupID;
				$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
				$this->mRs->Execute($sql);
				return $this->mRs->Recordcount();
			}
			
			function getFeaturedPhotoAlbums($featured=TRUE){
				$groupID = $this->mGroupID;
				if( $featured ){
					$sql = "SELECT *
							FROM tblPhotoAlbum
							WHERE isFeatured = 1
								AND groupID = $groupID
							ORDER BY lastupdate DESC";
				}else{
					$sql = "SELECT *
							FROM tblPhotoAlbum
							WHERE isFeatured = 0
								AND groupID = $groupID
							ORDER BY lastupdate DESC";		
				}
				$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
				$this->mRs->Execute($sql);
				
				$featuredAlbums = array();
				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                    try {
                        $photoalbum = new PhotoAlbum();
                        $photoalbum->setPhotoAlbumID($recordset['photoalbumID']);
                        $photoalbum->setTitle($recordset['title']);
                        $photoalbum->setCreator($recordset['creator']);
                        $photoalbum->setLastUpdate($recordset['lastupdate']);
                        $photoalbum->setGroupID($recordset['groupID']);
                        $photoalbum->setIsFeatured($recordset["isFeatured"]);
						$featuredAlbums[] = $photoalbum;
                    }
                    catch (Exception $e) {
                    }
                }
                
                return $featuredAlbums;
			}
			
			function getFeaturedAlbumCount($featured=TRUE){
				$groupID = $this->mGroupID;
				if( $featured ){
					$sql = "SELECT COUNT(*) AS albumCount
							FROM tblPhotoAlbum
							WHERE isFeatured = 1
								AND groupID = $groupID";
				}else{
					$sql = "SELECT COUNT(*) AS albumCount
							FROM tblPhotoAlbum
							WHERE isFeatured = 0
								AND groupID = $groupID";		
				}
				$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
				$this->mRs->Execute($sql);
				
				$recordset = mysql_fetch_array($this->mRs->Resultset());
				
				return $recordset["albumCount"];
			}
        
        /**
         * Purpose: Adds a program for this admin group.
         * If this is a parent admin group, add the program.
         * But if this group is a subgroup, check if the program also exists in the parent admin group.
         * If it does, then add the program to the subgroup as well.
         */
            function addProgram($program){
              	$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
               /* if (TRUE == $this->isParent()){                         
                    $program->setGroupID($this->mGroupID);                    // set this groupID for the program
                    $program->Create();                                       // create program
                }
                else{   
                    $programID = $program->getProgramID();
                    $sql = "SELECT programID from tblProgram WHERE groupID = '$this->mParentID' AND programID = '$programID' " ;
                    $this->mRs->Execute($sql);
                
                    if (0 < $this->mRs->Recordcount()){
                        $program->setGroupID($this->mGroupID);                    // set this groupID for the program
                        $program->Create();                                       // create program
                    }
                } */
                
                // changed by K. Gordo (we should use tblGrouptoProgram for subgroups, I hope I'm right)
                if ($this->isParent()){
                	$program->setGroupID($this->mGroupID);                    // set this groupID for the program
                    $program->Create();                                       // create program
     
                } else {
     
           			
                	// this is a subgroup so we have to write the program to db if it does not exist yet in parent 
                	// and we have to insert an entry into tblGrouptoProgram to associate the program the this subgroup

					// get parent groupID
					$pGroupID = $this->getParent()->getGroupID();
					
	
					
					// check if programID exists in tblProgram w/ the groupID as the parent group ID
					$sql = "SELECT programID from tblProgram WHERE groupID = '$pGroupID' AND programID = '" . $program->getProgramID() . "'" ;
                    $this->mRs->Execute($sql);
           
           			
           
           			// if it does not exist then call parent::addProgram , if it is then we add an entry to the linking table     
                    if (0 == $this->mRs->Recordcount()){     
                       	$program->setGroupID($this->getParent()->getGroupID());    // set this groupID for the program
                        $program->Create();                                       // create program
                    } 
                    
                    $sql = "INSERT INTO tblGrouptoProgram (groupID,programID) values ('$this->mGroupID','"  . $program->getProgramID() . "')";
                    $this->mRs->Execute($sql);						                	                                	
                }
                 
    
            }
           
           
        /**
         * Purpose: Removes a program from this admin group.
         */  
            function removeProgram($programID){
               $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection()); 
               /* $program = new Program($programID);                       // instantiate program
                $program->Delete();                                       // delete program
                */
                
               // edited by K. Gordo 
               // 	 flow: if this is a subgroup, we only have to delete the entry for the program in tblGrouptoProgram
               //		   if this is a parent group then we have to delete the program using $program->Delete() and all
               //			 entries of this program in tblGrouptoProgram table
               
               if ($this->isParent()){
				  // delete program in parent
				  $program = new Program($programID);                       // instantiate program
                  $program->Delete();
                  
                  // delete entries in tblGrouptoProgram
                  $sql = "DELETE FROM tblGrouptoProgram WHERE programID='$programID'";                	
               	  $this->mRs->Execute($sql);	
               	
               } else {
               	  $sql = "DELETE FROM tblGrouptoProgram WHERE programID='$programID' AND groupID='$this->mGroupID'";
               	  $this->mRs->Execute($sql);	
               }              
            }
            
        /**
         * Purpose: check if a program already exists for this group
         * 	by: K. Gordo
         */
         	function programExists($programID){
         		$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
         		if ($this->isParent()) 
     		       $sql = "SELECT programID from tblProgram WHERE groupID = '$this->mGroupID' AND programID='$programID'";	                    
             	else 
             	   $sql = "SELECT programID from tblGrouptoProgram WHERE groupID = '$this->mGroupID' AND programID='$programID'";
             	   
             	$this->mRs->Execute($sql);
             	if (0 < $this->mRs->Recordcount())
             		return true;
             	else
             		return false;	             	        		
         	}
         
            
            
        /**
         * Purpose: Returns an array of programs of this admin group.
         * Rows Limit is enabled for this function.
         */             
            function getPrograms($rowslimit = NULL, $filter = NULL, $order = NULL){
               $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection()); 
               /* if (0 == count($this->mPrograms)){
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    $sql = "SELECT programID from tblProgram WHERE groupID = '$this->mGroupID' ORDER BY programID " . $limitstr;
                    $this->mRs->Execute($sql);
                    
                    if (0 == $this->mRs->Recordcount()){
                        return NULL;            
                    }                    
                    else {
                        while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                            try {
                                $program = new Program($recordset['programID']);
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                            $this->mPrograms[] = $program;
                        }
                    }               
                } */
                
                
                // edited by K. Gordo
                // flow: if this is a parent group then we return all programs tblProgram table
                //	     if this is a subgroup we return all the program entries found in tblGrouptoProgram
                
                $this->mPrograms = array();
                
                $limitstr   = ''; 
                $filterstr  = ''; 
                $orderstr   = ' ORDER BY start DESC '; 
                
                if (NULL != $rowslimit){
                    $offset   = $rowslimit->getOffset();
                    $rows     = $rowslimit->getRows();
                    $limitstr = " LIMIT " . $offset . " , " . $rows;
                }  
             
             
             	if (NULL != $filter){

                	$condition = $filter->getConditions();	
        			$booleanop = $filter->getBooleanOps();
        			
        			for($i=0;$i<count($condition);$i++){
	        			$attname   = $condition[$i]->getAttributeName();                    			
	        			$value 	   = $condition[$i]->getValue();
	        			
	        			switch($condition[$i]->getOperation()){
	        				
	        				case FilterOp::$EQUAL:
	        					$operator = " = ";
	        					break;
	        				case FilterOp::$NOT_EQUAL:
	        					$operator = " <> ";
	        					break;
	        				case FilterOp::$GREATER_THAN_OR_EQUAL:
	        					$operator = " >= ";
	        					break;
	        				default:
	        					$operator = " = ";
	        					break;
	        			}
	        			
	        			$filterstr .= " " . $booleanop[$i] . " ". $attname . $operator . " '$value' ";
        			}	                		
                }
                
                if (NULL != $order){

                	$condition = $order->getConditions();	
        			
        			$attname   = $condition[0]->getAttributeName();                    			
        				        			
        			switch($condition[0]->getOperation()){
        				
        				case FilterOp::$ORDER_BY:
        					$sortorder = " ASC ";
        					break;
        				case FilterOp::$ORDER_BY_DESC:
        					$sortorder = " DESC ";
        					break;
        				default:
        					$sortorder = " ASC ";
        					break;
        			}
        			
        			$orderstr = " ORDER BY " . $attname . $sortorder;
        			                		
                }
             
             	if ($this->isParent())    
                    $sql = "SELECT programID from tblProgram WHERE groupID = '$this->mGroupID' " . $filterstr . $orderstr . $limitstr;	                    
             	else 
             		$sql = "SELECT tblProgram.programID from tblProgram, tblGrouptoProgram WHERE tblProgram.programID = tblGrouptoProgram.programID " .
             				"AND tblGrouptoProgram.groupID = '$this->mGroupID' " . $filterstr . $orderstr  . $limitstr;
                
                $this->mRs->Execute($sql);
                    
                if (0 == $this->mRs->Recordcount()){
                    return array();   					// empty array         
                }                    
                else {
                   
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                        try {
                            $program = new Program($recordset['programID']);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                        $this->mPrograms[] = $program;
                    }
                }            
                
                
                return $this->mPrograms;
            }   
            
            
        	
        	
        
        /**
         * Purpose: Returns an array of activities of this admin group, regardless of its program.
         * And combine it with group events set for this group.
         * Rows Limit is enabled for this function.
         */             
            
            function getCalendarActivities($rowslimit = NULL, $startdate = NULL, $filter = null, $variance = false){
                require_once("travellog/model/Class.CalendarEvent.php"); 
               
					$this->mCalendarEvents = array();
                    
                    $sqlstrdate1 = '';
                    $sqlstrdate2 = '';
                    $limitstr    = '';
                    $filterstr    = '';
                    $sortorder = ' DESC ';
                    
                    if (NULL != $rowslimit){
	                    $offset   = $rowslimit->getOffset();
	                    $rows     = $rowslimit->getRows();
	                    $limitstr = " LIMIT " . $offset . " , " . $rows;
	                }  
                
                	if ($variance){
                    	$sqlstrdate1 = "AND CalendarEvent.adate < '$startdate' " ; 
	                	$sqlstrdate2 = "AND CalendarEvent.adate < '$startdate' " ;
                    	$sortorder = " DESC ";
                    }
                    
                    else if (NULL != $startdate){
	                	//$startdate = date('Y-m-d');				// if startdate not given we default to the current date	
	                	$sqlstrdate1 = "AND CalendarEvent.adate >= '$startdate' " ; 
	                	$sqlstrdate2 = "AND CalendarEvent.adate >= '$startdate' " ;
	                		                	
	                } 
	                
	                if (NULL != $filter){
	
	                	$condition = $filter->getConditions();	
	        			$attname   = $condition[0]->getAttributeName();                    			
	        			$value 	   = $condition[0]->getValue();
	        			
	        			switch($condition[0]->getOperation()){
	        				
	        				case FilterOp::$EQUAL:
	        					$operator = " = ";
	        					break;
	        				case FilterOp::$NOT_EQUAL:
	        					$operator = " <> ";
	        					break;
	        				default:
	        					$operator = " = ";
	        					break;
	        			}
	        			
	        			$filterstr = " AND " . $attname . $operator . $value;
	                		
	                }
					// edited by chris: Jan-23-2009 
					//also show activities of parents if this group is a subgroup
	                $groupCriteria2 = (!$this->mTravelerOwned && $this->getParentID()) ? "(CalendarEvent.groupID = '$this->mGroupID' OR CalendarEvent.groupID = '{$this->getParentID()}')" : "CalendarEvent.groupID = '$this->mGroupID'";
                    $sql = "SELECT CalendarEvent.activityID as itemID, CalendarEvent.title, CalendarEvent.adate as mydate, " .
                    		"CalendarEvent.description, CalendarEvent.displaypublic, CalendarEvent.context, CalendarEvent.timeZoneID " .
                    		"FROM tblProgram, tblActivity as CalendarEvent " .
                    		"WHERE tblProgram.programID = CalendarEvent.programID " .
                    		"AND tblProgram.groupID = '$this->mGroupID' " .
                    		 $sqlstrdate1 .
                    		 $filterstr . 
                    		" UNION " .
                    		"SELECT CalendarEvent.groupeventID as itemID, CalendarEvent.title, CalendarEvent.adate as mydate, " .
                    		"CalendarEvent.description, CalendarEvent.displaypublic, CalendarEvent.context, CalendarEvent.timeZoneID " .
                    		"FROM tblGroupEvent as CalendarEvent " .
                    		//"WHERE CalendarEvent.groupID = '$this->mGroupID' " .
							"WHERE $groupCriteria2 " .
                    		 $sqlstrdate2 .       
                    		 $filterstr .              		
							" ORDER BY mydate " . $sortorder . " , itemID " . $sortorder .
							$limitstr ;
					//$this->mRs->Execute($sql);
                    $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
                    $this->mRs->Execute($sql);
							
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                       
                       $calendarevent = new CalendarEvent($recordset['itemID'], $recordset['title'], $recordset['mydate'], $recordset['description'], 'calendar-event.php?gID=' . $this->mGroupID  . '&amp;eID=' . $recordset['itemID'], $recordset['context'],$this->mGroupID,$recordset['displaypublic']);
					   $calendarevent->setTimeZoneID($recordset['timeZoneID']);
                       $this->mCalendarEvents[] = $calendarevent;                           
                    }
                    
                              
                return $this->mCalendarEvents;
            }  
            
            public function isTravelerOwned(){
				return $this->mTravelerOwned;
			}
			
			public function setTravelerOwned($bool){
				$this->mTravelerOwned = $bool;
			}
        
        /**
         * Purpose: Adds news for this group.
         */
            function addNews($news){
                
                $news->setGroupID($this->mGroupID);                    // set this groupID for the news
                $news->Create();                                       // create news
            }
            
        /**
         * Purpose: Removes news from this admin group.
         */  
            function removeNews($newsID){
                
                $news = new News($newsID);                              // instantiate news
                $news->Delete();                                        // delete news
            }
            
        /**
         * Purpose: Returns an array of news of this group.
         * If this is a subgroup, then news for the parent group are also returned.
         * Rows Limit is enabled for this function.
         */            
            function getNews($filtercriteria = NULL, $rowslimit = NULL){
                $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
                $mRs2   = new Recordset(ConnectionProvider::instance()->getConnection());
                
                $this->mNews = array();
                
                $limitstr = '';
           		$filterstr = '';
            
                if (NULL != $rowslimit){
                    $offset   = $rowslimit->getOffset();
                    $rows     = $rowslimit->getRows();
                    $limitstr = " LIMIT " . $offset . " , " . $rows;
                }  
              
                
                if (NULL != $filtercriteria){
					/*
                	$condition = $filtercriteria->getConditions();	
        			$attname   = $condition[0]->getAttributeName();                    			
        			$value 	   = $condition[0]->getValue();
        			
        			switch($condition[0]->getOperation()){
        			*/
        			$condition = $filtercriteria->getConditions();	
        			$booleanop = $filtercriteria->getBooleanOps();
        			
        			for($i=0;$i<count($condition);$i++){
	        			$attname   = $condition[$i]->getAttributeName();                    			
	        			$value 	   = $condition[$i]->getValue();
	        			
	        			switch($condition[$i]->getOperation()){	
	        				case FilterOp::$EQUAL:
	        					$operator = " = ";
	        					break;
	        				case FilterOp::$NOT_EQUAL:
	        					$operator = " <> ";
	        					break;
	        				case FilterOp::$GREATER_THAN_OR_EQUAL:
	        					$operator = " >= ";
	        					break;
	        				default:
	        					$operator = " = ";
	        					break;
	        			}
        			
        				//$filterstr = " AND " . $attname . $operator . $value;
        				$filterstr .= " " . $booleanop[$i] . " ". $attname . $operator . $value;
        			}	
                }
            
                $sql = "SELECT SQL_CALC_FOUND_ROWS newsID,adate from tblNews WHERE (groupID = '$this->mGroupID' OR groupID = '$this->mParentID') " . $filterstr . " ORDER BY adate DESC, newsID DESC " . $limitstr;                
                $this->mRs->Execute($sql);
               
	            $sql = "SELECT FOUND_ROWS() as totalRecords";
                $mRs2->Execute($sql);              
                
                $this->mTotalRecords = $mRs2->Result(0, "totalRecords") ;
                
                while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                    try {
                        $news = new News($recordset['newsID']);
                    }
                    catch (Exception $e) {
                       throw $e;
                    }
                    $this->mNews[] = $news;
                }
            
                return $this->mNews;
            }
             
        
        /**
		 * Purpose: Gets the travel journals for this group  
		 * 
		 */
		 
		function getTravels($privacycriteria = 0, $limit = null, $owner = false, $countOnly = FALSE ){
			/*
				edits: ianne 01/20/2008
				travels are now flagged as deleted instead of deleting from db. 
				edited function to include only travels with deleted = 0
			*/
			require_once("Class.Criteria2.php");
			// Instantiate class Travel for each travelID 
			$obj_criteria = new Criteria2;
			$obj_criteria->setOrderBy ( 'lastupdated DESC' );
			$obj_criteria->setGroupBy ( 'travelID'         );

			if( strlen(trim($limit)) ) 
				$obj_criteria->setLimit( $limit ); 

			/***
				First select clause gets the journals of members that were approved by advisor
				Second select clause gets the journals of the advisor not found in the
			***/
			
			$publishTable = (!$owner) ? ", tblTravelLog " : "";
			$publishLink = (!$owner) ? "AND tblTravel.travelID = tblTravelLog.travelID " : "";
			$publishClause = (!$owner) ? "AND tblTravel.publish = 1 AND tblTravelLog.publish = 1 " : "";
			
			
			if( strlen(trim($limit)) ) $obj_criteria->setLimit( $limit ); 
			

			$sql1    = sprintf
				(
					'SELECT SQL_CALC_FOUND_ROWS * FROM ' .
					'( '.
						'SELECT qryGroupTravel.* , tblTravel.publish, tblTravel.entryCount ' .
						'FROM qryGroupTravel,tblGroupApprovedJournals, tblTravel ' . $publishTable .
						'WHERE qryGroupTravel.travelID = tblGroupApprovedJournals.travelID ' .
						'AND tblGroupApprovedJournals.travelID = tblTravel.travelID ' .
						'AND tblTravel.deleted = 0 '.
						$publishLink . 
						'AND qryGroupTravel.privacypref IN ('.$privacycriteria.') ' .
						'AND tblGroupApprovedJournals.approved = 1 ' .
						'AND tblGroupApprovedJournals.groupID  = %d ' .
						$publishClause .
						'UNION ALL ' .
						'SELECT qryGroupTravel.* , tblTravel.publish, tblTravel.entryCount ' .
						'FROM qryGroupTravel, tblTravel, tblTravelLink ' . $publishTable .
						'WHERE qryGroupTravel.travelID = tblTravel.travelID ' .
						'AND tblTravel.deleted = 0 '.
						$publishLink . 
						'AND tblTravelLink.travelLinkID = tblTravel.travellinkID '.
						'AND tblTravelLink.refType = 2 '.
						'AND tblTravelLink.refID = %d '.
						//'AND qryGroupTravel.travelerID       = %d ' .
						'AND qryGroupTravel.privacypref IN ('.$privacycriteria.') ' .
						'AND qryGroupTravel.travelID NOT IN ( SELECT travelID FROM tblGroupApprovedJournals WHERE groupID = %d AND approved = 1 ) ' .
						$publishClause . 
					') qryGroupJournals '.
					'%s ' 
					, $this->mGroupID
					, $this->mGroupID
					, $this->mGroupID
					, $obj_criteria->getCriteria2()
				);
				
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$results = $this->mRs->Execute($sql1); 
			$sql2    = 'SELECT FOUND_ROWS() AS totalrec';
			$results2 = $this->mRs->Execute($sql2);
			
			if( $countOnly ){
				$row = mysql_fetch_assoc($results2);
				return $row["totalrec"];
			}
			
			$this->mTravels = array();
			
			while( $row = mysql_fetch_assoc( $results ) ){
				$objNewMember = new Travel;
				$objNewMember->setTravelID    ( $row['travelID']     );
				$objNewMember->setTitle       ( $row['title']        );
				$objNewMember->setDescription ( $row['description']  );
				$objNewMember->setRating      ( $row['rating']       );
				$objNewMember->setVotes       ( $row['votes']        );
				$objNewMember->setViews       ( $row['viewed']       );
				$objNewMember->setLastUpdated ( $row['lastupdated']  );
				$objNewMember->setPrimaryphoto( $row['primaryphoto'] );
				$objNewMember->setTravelerID  ( $row['travelerID']   );
				$objNewMember->setPublish($row['publish']);				//added by K. Gordo		
				$objNewMember->setEntryCount($row['entryCount']);
				$objNewMember->setTravelLinkID( $row['travellinkID'] );
				$this->mTravels[] = $objNewMember;
			}      
			
			return $this->mTravels;
		}
		
		/**
		 * Get Journals of Group Members
		 * by K. Gordo to get all Travels in just one query
		 */
		function getTravelsOfMembers($privacycriteria = 0, $limit = null, $owner = false){
			/*
				edits: ianne 01/20/2008
				travels are now flagged as deleted instead of deleting from db. 
				edited function to include only travels with deleted = 0
			*/
			
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = sprintf('select * from tblTravel as t, tblGrouptoTraveler as gtt, tblTraveler as trvlr, tblTravelLink as tl
					        where gtt.groupID = %d and gtt.travelerID = trvlr.travelerID and trvlr.travelerID = t.travelerID and t.travellinkID = tl.travellinkID and tl.reftype=1 and t.deleted = 0 order by lastupdated desc',$this->getGroupID());
			
			$this->mRs->Execute($sql);
			
			$travels = array();
			while ($travel = mysql_fetch_array($this->mRs->Resultset())){							      	
				$tempTravel = new Travel ();
				$tempTravel->setTravelID($travel['travelID']);
				$tempTravel->setLocationID($travel['locID']);
				$tempTravel->setTitle($travel['title']); 
				$tempTravel->setDescription($travel['description']); 
				$tempTravel->setRating($travel['rating']); 
				$tempTravel->setVotes($travel['votes']); 
				$tempTravel->setViews($travel['viewed']); 
				$tempTravel->setLastUpdated($travel['lastupdated']);
				$tempTravel->setPrimaryphoto($travel['primaryphoto']);
				$tempTravel->setPrivacyPref($travel['privacypref']); 
				$tempTravel->setPublish($travel['publish']);
				$tempTravel->setIsFeatured($travel['isfeatured']);
				$tempTravel->setEntryCount($travel['entryCount']); 
				$tempTravel->setTravelerID($travel['travelerID']);
				$tempTravel->setTravelLinkID($travel['travellinkID']);
				$travels[] = $tempTravel;				
			}			
			
			return $travels;
		}
		
		function getTravelsAndArticles($privacycriteria = 0, $limit = null, $owner = false, $countOnly = FALSE ){
			/*
				edits: ianne 01/20/2008
				travels are now flagged as deleted instead of deleting from db. 
				edited function to include only travels with deleted = 0
			*/
			require_once("Class.Criteria2.php");
			// Instantiate class Travel for each travelID 
			$obj_criteria = new Criteria2;
			$obj_criteria->setOrderBy ( 'lastupdated DESC' );
			//$obj_criteria->setGroupBy ( 'travelID'         );

			if( strlen(trim($limit)) ) 
				$obj_criteria->setLimit( $limit ); 

			/***
				First select clause gets the journals of members that were approved by advisor
				Second select clause gets the journals of the advisor not found in the
			***/
			
			$publishTable = (!$owner) ? ", tblTravelLog " : "";
			$publishLink = (!$owner) ? "AND tblTravel.travelID = tblTravelLog.travelID " : "";
			$publishClause = (!$owner) ? "AND tblTravel.publish = 1 AND tblTravelLog.publish = 1 " : "";
			
			
			if( strlen(trim($limit)) ) $obj_criteria->setLimit( $limit ); 
			
			$asql = "SELECT tblArticle.authorID, " . 
						"tblArticle.articleID, " . 
						"tblArticle.locID, " . 
						"tblArticle.title, " . 
						"tblArticle.description, " . 
						"0 as primaryphoto, " . 
						"0 as preference, " . 
						"tblArticle.lastedited as lastupdated, " .
						"tblArticle.logdate as _date, " . 							
						"'ARTICLE' as type, " .
						"tblArticle.deleted, " .
						"1 as publish, " .
						"1 as entryCount, " .
						"0 as views, " . 
						"tblArticle.groupID as travellinkID " .
					" FROM tblArticle " .
					" WHERE tblArticle.deleted = 0 " .
					" AND tblArticle.groupID = " .$this->mGroupID. 
					" GROUP BY tblArticle.articleID";
					
			$sql1 = "SELECT tblTravel.travelerID, " . 
						"tblTravel.travelID as ID, " . 
						"tblTravel.locID, " . 
						"tblTravel.title, " . 
						"tblTravel.description, " . 
						"tblTravel.primaryphoto, " . 
						"tblTravel.privacypref, " . 
						"tblTravel.lastupdated as lastupdated, " .
						"tblTravelLog.logdate as _date, " .
						"'JOURNAL' as type, " . 
						"tblTravel.deleted, " .
						"tblTravel.publish, " .
						"tblTravel.entryCount, " .
						"tblTravel.viewed as views, " .
						"tblTravel.travellinkID " .
					"FROM qryGroupTravel,tblGroupApprovedJournals, tblTravel " . $publishTable .
					"WHERE qryGroupTravel.travelID = tblGroupApprovedJournals.travelID " .
					"AND tblGroupApprovedJournals.travelID = tblTravel.travelID " .
					"AND tblTravel.deleted = 0 ".
					$publishLink . 
					"AND qryGroupTravel.privacypref IN (".$privacycriteria.") " .
					"AND tblGroupApprovedJournals.approved = 1 " .
					"AND tblGroupApprovedJournals.groupID  = ". $this->mGroupID . " " . 
					$publishClause .
					"UNION ALL " .
					"SELECT tblTravel.travelerID, " . 
						"tblTravel.travelID as ID, " . 
						"tblTravel.locID, " . 
						"tblTravel.title, " . 
						"tblTravel.description, " . 
						"tblTravel.primaryphoto, " . 
						"tblTravel.privacypref, " . 
						"tblTravel.lastupdated as lastupdated, " .
						"tblTravelLog.logdate as _date, " .
						"'JOURNAL' as type, " . 
						"tblTravel.deleted, " .
						"tblTravel.publish, " .
						"tblTravel.entryCount, " .
						"tblTravel.viewed as views, " .
						"tblTravel.travellinkID " .
					"FROM qryGroupTravel, tblTravel, tblTravelLink " . $publishTable .
					"WHERE qryGroupTravel.travelID = tblTravel.travelID " .
					"AND tblTravel.deleted = 0 ".
					$publishLink . 
					"AND tblTravelLink.travelLinkID = tblTravel.travellinkID ".
					"AND tblTravelLink.refType = 2 ".
					"AND tblTravelLink.refID = ". $this->mGroupID . " " . 
					"AND qryGroupTravel.privacypref IN (".$privacycriteria.") " .
					"AND qryGroupTravel.travelID NOT IN ( SELECT travelID FROM tblGroupApprovedJournals WHERE groupID = ".$this->mGroupID." AND approved = 1 ) " .
					$publishClause ." GROUP BY ID UNION ".$asql." ".$obj_criteria->getCriteria2();
			
			$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
			$results = $this->mRs->Execute($sql1); 
			$sql2    = "SELECT FOUND_ROWS() AS totalrec";
			$results2 = $this->mRs->Execute($sql2);
			
			if( $countOnly ){
				$row = mysql_fetch_assoc($results2);
				return $row["totalrec"];
			}
			
			$this->mTravels = array();
			
			require_once("travellog/vo/Class.JournalsArticlesVO.php");
			require_once("travellog/model/Class.Article.php");

			while ($recordset = mysql_fetch_assoc($results)){					
				try {
					$filteredTravel	= new JournalsArticlesVO();
					$filteredTravel->setTravelerID($recordset['travelerID']);
					$filteredTravel->setTravelID($recordset['ID']);
					$filteredTravel->setLocID($recordset['locID']);
					$filteredTravel->setTitle($recordset['title']);
					$filteredTravel->setDescription($recordset['description']);
					$filteredTravel->setPrimaryphoto($recordset['primaryphoto']);
					$filteredTravel->setPrivacyPref($recordset['privacypref']);
					$filteredTravel->setDate($recordset['_date']);
					$filteredTravel->setType($recordset['type']); 
					$filteredTravel->setPublish($recordset['publish']);
					$filteredTravel->setEntryCount($recordset['entryCount']);
					$filteredTravel->setViews($recordset['views']);
					if($filteredTravel->getType() == "JOURNAL"){
						$travel = new Travel(0, 
							array( 
								"locID" => NULL,
								"title" => NULL,
								"description" => NULL,
								"rating" => NULL,
								"votes" => NULL,
								"viewed" => NULL,
								'lastupdated' => NULL,
								"primaryphoto" => NULL,
								"privacypref" => NULL,
								"publish" => NULL,
								"isfeatured" => NULL,
								"entryCount"  => NULL,
								"travelerID" => NULL,
								"travellinkID" => $recordset['travellinkID'], 
								"travelID" => $recordset['ID']
							)
						);
						$filteredTravel->setOwner($travel->getOwner());
					}else{
						$article = new Article(0, 
							array(
								"articleID" => NULL,
								"title" => NULL,
								"description" => NULL,
								"callout" => NULL,
								"locID" => NULL,
								"editorsNote" => NULL,
								"articledate" => NULL,
								"logdate" => NULL,
								"lastedited" => NULL,
								"isfeatured" => NULL,
								"publish" => NULL,
								"deleted" => NULL,
								"groupID" => $recordset['travellinkID'], 
								"authorID" => $recordset['travelerID'])
							);
						$filteredTravel->setOwner($article->getOwner());
					}
				}
				catch (Exception $e) {
				   throw $e;
				}
				$this->mTravels[] = $filteredTravel;
			}      
			
			return $this->mTravels;
		}
        
		function getTravelsAndArticlesOfMembers($privacycriteria = 0, $limit = null, $owner = false){
			/*
				edits: ianne 01/20/2008
				travels are now flagged as deleted instead of deleting from db. 
				edited function to include only travels with deleted = 0
			*/
			$asql = "SELECT tblArticle.authorID, " . 
						"tblArticle.articleID, " . 
						"tblArticle.locID, " . 
						"tblArticle.title, " . 
						"tblArticle.description, " . 
						"0 as primaryphoto, " . 
						"0 as preference, " . 
						"tblArticle.lastedited as lastupdated, " .
						"tblArticle.logdate as _date, " . 							
						"'ARTICLE' as type, " .
						"tblArticle.deleted, " .
						"1 as publish, " .
						"1 as entryCount, " .
						"0 as views, " . 
						"tblArticle.groupID as travellinkID " .
					" FROM tblArticle, tblGrouptoTraveler as gtt, tblTraveler as trvlr " .
					" WHERE gtt.groupID = ".$this->mGroupID .
					" AND gtt.travelerID = trvlr.travelerID " .
					" AND trvlr.travelerID = tblArticle.authorID ".
					" AND tblArticle.deleted = 0 " .
					" AND (tblArticle.groupID = ".$this->mGroupID." OR tblArticle.groupID = 0) ".
					" GROUP BY tblArticle.articleID";
			
			$sql = "SELECT tblTravel.travelerID, " . 
						"tblTravel.travelID as ID, " . 
						"tblTravel.locID, " . 
						"tblTravel.title, " . 
						"tblTravel.description, " . 
						"tblTravel.primaryphoto, " . 
						"tblTravel.privacypref, " . 
						"tblTravel.lastupdated as lastupdated, " .
						"now() as _date, " .
						"'JOURNAL' as type, " . 
						"tblTravel.deleted, " .
						"tblTravel.publish, " .
						"tblTravel.entryCount, " .
						"tblTravel.viewed as views, " .
						"tblTravel.travellinkID " .
					"FROM tblTravel , tblGrouptoTraveler as gtt, tblTraveler as trvlr, tblTravelLink as tl " .
					"WHERE gtt.groupID = ".$this->mGroupID .
						" AND gtt.travelerID = trvlr.travelerID " .
						" AND trvlr.travelerID = tblTravel.travelerID ".
						" AND tblTravel.travellinkID = tl.travellinkID ".
						" AND tl.reftype=1 and tblTravel.deleted = 0 ".
						" GROUP BY ID " . 
						" UNION " .$asql.
						" ORDER BY lastupdated DESC";
			
			$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
			$results = $this->mRs->Execute($sql);
			
			$travels = array();
			require_once("travellog/vo/Class.JournalsArticlesVO.php");
			require_once("travellog/model/Class.Article.php");

			while ($recordset = mysql_fetch_assoc($results)){					
				try {
					$filteredTravel	= new JournalsArticlesVO();
					$filteredTravel->setTravelerID($recordset['travelerID']);
					$filteredTravel->setTravelID($recordset['ID']);
					$filteredTravel->setLocID($recordset['locID']);
					$filteredTravel->setTitle($recordset['title']);
					$filteredTravel->setDescription($recordset['description']);
					$filteredTravel->setPrimaryphoto($recordset['primaryphoto']);
					$filteredTravel->setPrivacyPref($recordset['privacypref']);
					$filteredTravel->setDate($recordset['_date']);
					$filteredTravel->setType($recordset['type']); 
					$filteredTravel->setPublish($recordset['publish']);
					$filteredTravel->setEntryCount($recordset['entryCount']);
					$filteredTravel->setViews($recordset['views']);
					if($filteredTravel->getType() == "JOURNAL"){
						$travel = new Travel(0, 
							array( 
								"locID" => NULL,
								"title" => NULL,
								"description" => NULL,
								"rating" => NULL,
								"votes" => NULL,
								"viewed" => NULL,
								'lastupdated' => NULL,
								"primaryphoto" => NULL,
								"privacypref" => NULL,
								"publish" => NULL,
								"isfeatured" => NULL,
								"entryCount"  => NULL,
								"travelerID" => NULL,
								"travellinkID" => $recordset['travellinkID'], 
								"travelID" => $recordset['ID']
							)
						);
						$filteredTravel->setOwner($travel->getOwner());
					}else{
						$article = new Article(0, 
							array(
								"articleID" => NULL,
								"title" => NULL,
								"description" => NULL,
								"callout" => NULL,
								"locID" => NULL,
								"editorsNote" => NULL,
								"articledate" => NULL,
								"logdate" => NULL,
								"lastedited" => NULL,
								"isfeatured" => NULL,
								"publish" => NULL,
								"deleted" => NULL,
								"groupID" => $recordset['travellinkID'], 
								"authorID" => $recordset['travelerID'])
							);
						$filteredTravel->setOwner($article->getOwner());
					}
				}
				catch (Exception $e) {
				   throw $e;
				}
				$travels[] = $filteredTravel;				
			}			
			
			return $travels;
		}
        	
		function getEntriesAndArticles($cri=null, $countOnly=false){
			try {
				$mConn = new Connection();
				$mRs = new Recordset($mConn);
				$mRs2 = new Recordset($mConn);
			}
			catch (Exception $e) {				   
			   throw $e;
			}

			if(!is_null($cri)){
				$start = $cri['START'];
				$end = $cri['END'];
			}
					
			$asql = "SELECT tblArticle.authorID, " . 
						"tblArticle.articleID, " . 
						"tblArticle.locID, " . 
						"tblArticle.title, " . 
						"tblArticle.description, " . 
						"tblArticle.lastedited as lastupdated, " .
						"tblArticle.logdate as _date, " . 							
						"0 as trip, " . 
						"'ARTICLE' as type, " .
						"0 as views, " . 
						"tblArticle.groupID as travellinkID, " .
						"0 as t_ID " .
					" FROM tblArticle, tblGrouptoTraveler as gtt, tblTraveler as t " .
					" WHERE tblArticle.deleted = 0 " . 
					" AND tblArticle.authorID = t.travelerID " .
					" AND t.isSuspended = 0 " . 
					" AND t.deactivated = 0 " . 
					" AND t.travelerID = gtt.travelerID " .
					" AND (tblArticle.groupID = ".$this->mGroupID .
						" OR (gtt.groupID = ".$this->mGroupID." )) ".
					" GROUP BY tblArticle.articleID";					
			
			$esql = "SELECT tblTravel.travelerID, " . 
						"tblTravelLog.travelLogID as ID, " . 
						"tblTravel.locID, " . 
						"tblTravelLog.title, " . 
						"tblTravelLog.description, " . 
						"tblTravelLog.lastedited as lastupdated, " .
						"tblTravelLog.logdate as _date, " .
						"tblTravelLog.tripID as trip, " . 
						"'JOURNAL' as type, " . 
						"tblTravel.viewed as views, " . 
						"tblTravel.travellinkID, " .
						"tblTravel.travelID as t_ID " .
					"FROM qryGroupTravel,tblGroupApprovedJournals, tblTravel, tblTravelLog, tblTraveler " . 
					"WHERE qryGroupTravel.travelID = tblGroupApprovedJournals.travelID " .
						"AND tblGroupApprovedJournals.travelID = tblTravel.travelID " .
						"AND tblTravel.deleted = 0 ".
						"AND tblTravel.travelerID = tblTraveler.travelerID " . 
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 " .
						"AND tblTravel.travelID = tblTravelLog.travelID " . 
						"AND tblGroupApprovedJournals.approved = 1 " .
						"AND tblGroupApprovedJournals.groupID  = ". $this->mGroupID . " " .
						"AND tblTravelLog.deleted = 0 " .
					"GROUP BY ID " . 	
					"UNION ALL " . 
					"SELECT tblTravel.travelerID, " . 
						"tblTravelLog.travelLogID as ID, " . 
						"tblTravel.locID, " . 
						"tblTravelLog.title, " . 
						"tblTravelLog.description, " . 
						"tblTravelLog.lastedited as lastupdated, " .
						"tblTravelLog.logdate as _date, " .
						"tblTravelLog.tripID as trip, " . 
						"'JOURNAL' as type, " . 
						"tblTravel.viewed as views, " .
						"tblTravel.travellinkID, " .
						"tblTravel.travelID as t_ID " .
					"FROM qryGroupTravel, tblTravel, tblTravelLink, tblTravelLog " .
					"WHERE qryGroupTravel.travelID = tblTravel.travelID " .
					"AND tblTravel.deleted = 0 ".
					"AND tblTravel.travelID = tblTravelLog.travelID " . 
					"AND tblTravelLink.travelLinkID = tblTravel.travellinkID ".
					"AND tblTravelLink.refType = 2 ".
					"AND tblTravelLink.refID = ". $this->mGroupID . " " . 
					"AND qryGroupTravel.travelID NOT IN ( SELECT travelID FROM tblGroupApprovedJournals WHERE groupID = ".$this->mGroupID." AND approved = 1 ) " .
					"AND tblTravelLog.deleted = 0 " .
					"GROUP BY ID ";//" UNION ".$esql2;
					
			$sql = 	"SELECT SQL_CALC_FOUND_ROWS tbl.travelerID, " . 
							"tbl.ID, " . 
							"tbl.locID, " . 
							"tbl.title, " . 
							"tbl.description, " . 
							"tbl.lastupdated, " .
							"tbl._date, " .
							"tbl.trip, " . 
							"tbl.type, " . 
							"tbl.views, " .
							"tbl.travellinkID, " .
							"tbl.t_ID " .
						"FROM ($esql UNION $asql ORDER BY CASE WHEN lastupdated = '0000-00-00 00:00:00' " . 
							"THEN _date " . 
							"ELSE lastupdated " . 
						"END DESC) as tbl ";
						
			
			if(isset($start) && isset($end))		
				$sql .= "LIMIT $start,$end";							
			
			$mRs->Execute($sql);
			
			$mRs2->Execute('SELECT FOUND_ROWS() as totalrec');
			if( $countOnly ){
				$row = mysql_fetch_assoc($mRs2->Resultset());
				return $row["totalrec"];
			}
			
			require_once("travellog/vo/Class.JournalsArticlesVO.php");
			require_once("travellog/model/Class.Article.php");
			
			$arrTravel = array();
			while ($recordset = mysql_fetch_assoc($mRs->Resultset())){
 				try {
						$filteredTravel	= new JournalsArticlesVO();
					$filteredTravel->setTravelerID($recordset['travelerID']);
					$filteredTravel->setTravelID($recordset['ID']);
					$filteredTravel->setID($recordset['t_ID']);
					$filteredTravel->setLocID($recordset['locID']);
					$filteredTravel->setTitle($recordset['title']);
					$filteredTravel->setDescription($recordset['description']);
					$filteredTravel->setDate($recordset['_date']);
					$filteredTravel->setType($recordset['type']); 
					$filteredTravel->setTrip(new Trip($recordset['trip']));
					$filteredTravel->setViews($recordset['views']); 
					if($filteredTravel->getType() == "JOURNAL"){
						$travel = new Travel(0, 
							array( 
								"locID" => NULL,
								"title" => NULL,
								"description" => NULL,
								"rating" => NULL,
								"votes" => NULL,
								"viewed" => NULL,
								'lastupdated' => NULL,
								"primaryphoto" => NULL,
								"privacypref" => NULL,
								"publish" => NULL,
								"isfeatured" => NULL,
								"entryCount"  => NULL,
								"travelerID" => NULL,
								"travellinkID" => $recordset['travellinkID'], 
								"travelID" => $recordset['ID']
							)
						);
						$filteredTravel->setOwner($travel->getOwner());
					}else{
						$article = new Article(0, 
							array(
								"articleID" => NULL,
								"title" => NULL,
								"description" => NULL,
								"callout" => NULL,
								"locID" => NULL,
								"editorsNote" => NULL,
								"articledate" => NULL,
								"logdate" => NULL,
								"lastedited" => NULL,
								"isfeatured" => NULL,
								"publish" => NULL,
								"deleted" => NULL,
								"groupID" => $recordset['travellinkID'], 
								"authorID" => $recordset['travelerID'])
							);
						$filteredTravel->setOwner($article->getOwner());
					}
				}
				catch (Exception $e) {
				   throw $e;
				}
					$arrTravel[] = $filteredTravel;
			}
			return $arrTravel;
		}
		
		/**
         * Purpose: Adds a resource file for this group.
         */
            function addResourceFile($resourceFile){
                $this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
                $resourceFileID = $resourceFile->getResourceFileID();
                $sql = "INSERT into tblGrouptoResourceFile (groupID, resourcefileID) " .  // insert link of resource file to group
                		"VALUES ('$this->mGroupID', '$resourceFileID')";
                $this->mRs->Execute($sql);
            }
            
        
        /**
         * Purpose: Removes a resource file from this admin group.
         */  
            function removeResourceFile($resourceFileID){
               $this->mRs->setConnection(ConnectionProvider::instance()->getConnection()); 
               $sql = "DELETE FROM tblGrouptoResourceFile " .
                        "WHERE groupID = '$this->mGroupID' AND resourcefileID = '$resourceFileID' ";      // delete link of resource file to group*/
               // $sql = "DELETE FROM tblGrouptoResourceFile " .
               //        "WHERE resourcefileID = '$resourceFileID' ";      // delete link of resource file to group
               $this->mRs->Execute($sql);
            }
            
            
        /**
         * Purpose: Returns an array of resource files of this group.
         * If this is a subgroup, then resource files for the parent group are also returned.
         * Rows @ is enabled for this function.
         */
            function getResourceFiles($rowslimit = NULL, $privacypreference = NULL, $view = NULL){
                
                if (0 == count($this->mResourceFiles)){
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    //$sql = "SELECT resourcefileID from tblGrouptoResourceFile WHERE groupID = '$this->mGroupID' OR " .
                    //		"(groupID = '$this->mParentID' AND groupID <> 0) ORDER BY resourcefileID " . $limitstr;
                    
                    
                    
                    /**######################
                    # if $privacypreference == 1(member) show files where privacypreference GT 0   
                    # if not member or $privacypreference ==2 show files where privacypreference GT 1 
                    # if owner show all files
                    ########################*/
                    
                    if($privacypreference == 1){
                    	if(NULL === $view){
                    		$sql = "SELECT tblResourceFile.resourcefileID from tblGrouptoResourceFile,tblResourceFile " .
    	                			"WHERE tblResourceFile.resourcefileID = tblGrouptoResourceFile.resourcefileID ".
	                    			"AND groupID = '$this->mGroupID' " .
        	            			"AND privacypreference > 0 ORDER BY tblResourceFile.resourcefileID DESC" . $limitstr;
                    	}else{
                    		$sql = "SELECT tblResourceFile.resourcefileID from tblGrouptoResourceFile,tblResourceFile " .
	                    			"WHERE tblResourceFile.resourcefileID = tblGrouptoResourceFile.resourcefileID ".
    	                			"AND groupID = '$this->mGroupID' " .
        	            			"AND privacypreference = ". $view ." ORDER BY tblResourceFile.resourcefileID DESC" . $limitstr;
                    	}
                    	
                    }elseif($privacypreference == 2){
                    	$sql = "SELECT tblResourceFile.resourcefileID from tblGrouptoResourceFile,tblResourceFile " .
                    			"WHERE tblResourceFile.resourcefileID = tblGrouptoResourceFile.resourcefileID ".
                    			"AND groupID = '$this->mGroupID' " .
                    			"AND privacypreference = 2 ORDER BY tblResourceFile.resourcefileID DESC" . $limitstr;
                    }else{
                    	if(NULL === $view){
                    		$sql = "SELECT resourcefileID from tblGrouptoResourceFile WHERE groupID = '$this->mGroupID' ORDER BY resourcefileID DESC" . $limitstr;
                    	}
                    	else{
                    		$sql = "SELECT tblResourceFile.resourcefileID from tblGrouptoResourceFile,tblResourceFile " .
                    			"WHERE tblResourceFile.resourcefileID = tblGrouptoResourceFile.resourcefileID ".
                    			"AND groupID = '$this->mGroupID' " .
                    			"AND privacypreference = ". $view ." ORDER BY resourcefileID DESC" . $limitstr;
                    	}
                    }
                    
                    $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
                    $this->mRs->Execute($sql);
                    
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                       try {
                           $resourcefile = new ResourceFiles($recordset['resourcefileID']);
							if( $resourcefile instanceof ResourceFiles ){
								$resourcefile->setContext($this);
								$this->mResourceFiles[] = $resourcefile;
							}else if(3 == $this->mGroupID || 3==$this->mParentID){//for debugging in prod
								echo "Invalid resourcefileD: ".$recordset['resourcefileID'];
							}
                       }
                       catch (Exception $e) {
							if(3 == $this->mGroupID || 3==$this->mParentID){//for debugging in prod
								echo "Invalid resourcefileID: ".$recordset['resourcefileID'];
							}
                       }
                    }
                                   
                }
                
                return $this->mResourceFiles;
            }    
            
            
            function getResourceFilesWithPrivacyPreference($rowslimit = NULL,$privacypreference = NULL){
                
                if (0 == count($this->mResourceFiles)){
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    //$sql = "SELECT resourcefileID from tblGrouptoResourceFile WHERE groupID = '$this->mGroupID' OR " .
                    //		"(groupID = '$this->mParentID' AND groupID <> 0) ORDER BY resourcefileID " . $limitstr;
                    
                    $sql = "SELECT resourcefileID from tblGrouptoResourceFile WHERE groupID = '$this->mGroupID' ORDER BY resourcefileID " . $limitstr;
                   $this->mRs->setConnection(ConnectionProvider::instance()->getConnection()); 
                    $this->mRs->Execute($sql);
                    
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                        try {
                            $resourcefile = new ResourceFiles($recordset['resourcefileID']);
                            $resourcefile->setContext($this);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                        $this->mResourceFiles[] = $resourcefile;
                    }
                               
                }
                
                return $this->mResourceFiles;
            }    
            
            
            
		/**
         * Purpose: Returns the institution information of the administrator of this admin group.
         */
            function getInstitutionInfo(){
               $this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
                if (NULL == $this->mAdvisorGroupInfo  ){
                   
                    if (0 < $this->mClientID) {
	                    $sql = "SELECT instname, tbclient.countryID, weburl, phone, fax, address1, address2, address3, city, state, zip, mission from GoAbroad_Main.tbclient, GoAbroad_Main.tbcountry " .
	                    		"WHERE tbclient.countryID = tbcountry.countryID AND clientID = '$this->mClientID' " ;
	                    $this->mRs->Execute($sql);
	                    
	                    $advisorGroupInfo = new AdvisorGroupInfo();
	                    
	                    if ($this->mRs->Recordcount()){
	                        $advisorGroupInfo->setClientID($this->mClientID) ;
	                        $advisorGroupInfo->setWebURL(stripslashes($this->mRs->Result(0,"weburl"))) ;           
	                        $advisorGroupInfo->setPhone(stripslashes($this->mRs->Result(0,"phone"))) ;   
	                        $advisorGroupInfo->setFax(stripslashes($this->mRs->Result(0,"fax"))) ;                     
	                        $advisorGroupInfo->setAddress1(stripslashes($this->mRs->Result(0,"address1"))) 	 ;    
	                        $advisorGroupInfo->setAddress2(stripslashes($this->mRs->Result(0,"address2"))) 	 ;    
	                        $advisorGroupInfo->setAddress3(stripslashes($this->mRs->Result(0,"address3"))) 	 ;    
	                        $advisorGroupInfo->setCity(stripslashes($this->mRs->Result(0,"city"))) 	 ;
	                        $advisorGroupInfo->setState(stripslashes($this->mRs->Result(0,"state"))) ;
	                        $advisorGroupInfo->setZip(stripslashes($this->mRs->Result(0,"zip")))     ;
	                        $advisorGroupInfo->setCountryID($this->mRs->Result(0,"countryID"))    ;
	                        $advisorGroupInfo->setMission(stripslashes($this->mRs->Result(0,"mission")))  	 ;
	                    }    
	                   
	                    $this->mAdvisorGroupInfo = $advisorGroupInfo ; 
                    } 
                    else {
                    	$this->mAdvisorGroupInfo = new AdvisorGroupInfo($this->mGroupID);
                    	
                    }       
                }
                
                return $this->mAdvisorGroupInfo;
            }    
            
            
        /**
         * Purpose: Returns the institution information of the administrator of this admin group.
         */
            function getOnlineForm(){
               $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
                if (NULL == $this->mOnlineSurveyForm  ){
                   
                    if (0 < $this->mGroupID) {
	                    $sql = "SELECT onlineformID FROM tblOnlineForm " .
	                    		"WHERE groupID = $this->mGroupID " ;
	                    $this->mRs->Execute($sql);
	                    
	                    $advisorGroupInfo = new AdvisorGroupInfo();
	                    
	                    if ($this->mRs->Recordcount()){
	                        
	                        $this->mOnlineSurveyForm = new OnlineForm($this->mRs->Result(0,"onlineformID"));
	                    }    
	                   
                    }       
                }
                
                return $this->mOnlineSurveyForm;
            }  
            
        /**
         * Purpose: Returns the institution information of the administrator of this admin group.
         */
            function checkForDelete(){
               
                if (false == $this->mIsForDelete  ){
                   
                    $rowslimitnum = 1;
					$rowslimit = new RowsLimit($rowslimitnum, 0);
		
                    $myresourcefiles = $this->getResourceFiles($rowslimit);
	                $mynews = $this->getNews(null, $rowslimit);
	                $myprograms = $this->getPrograms($rowslimit);
	                $myphotoalbums = $this->getPhotoAlbums($rowslimit);
	                $mysubgroups = $this->getSubGroups($rowslimit);
	                $mygroupevents = $this->getGroupEvents($rowslimit);
	                $mygroupmembers = $this->getMembers($rowslimit);
	                $myrequestlist = $this->getRequestList();
		            $myinvitelist = $this->getInviteList();
		            
		            $msgspace = new MessageSpace(); 
					$myalerts = $msgspace->getAlerts($this->mSendableID);		
					$mybulletins = $msgspace->getBulletins($this->mSendableID,$rowslimit);
		
	                if (count($myresourcefiles))	
	                	$this->mIsForDelete = false;
	                else if (count($mynews))	
	                	$this->mIsForDelete = false;
	                else if (count($myprograms))	
	                	$this->mIsForDelete = false;
	                else if (count($myphotoalbums))	
	                	$this->mIsForDelete = false;
	                else if (count($mysubgroups))	
	                	$this->mIsForDelete = false;
	                else if (count($mygroupevents))	
	                	$this->mIsForDelete = false;
	                else if (count($mygroupmembers))	
	                	$this->mIsForDelete = false;
	                else if (count($myrequestlist))	
	                	$this->mIsForDelete = false;
	                else if (count($myinvitelist))	
	                	$this->mIsForDelete = false;	
	                else if (count($myalerts))	
	                	$this->mIsForDelete = false;
	                else if (count($mybulletins))	
	                	$this->mIsForDelete = false;	
	                else
	                	$this->mIsForDelete = true;
	            
	            	// add bulletins and alerts
                   
                }
                
                return $this->mIsForDelete;
            }    
                       
    	/**
		 * Static Function
         * Purpose: Returns the encrypted value of clientID related to admin group
         */ 
	 		public static function getEncrypted($clientID,$key){
	 			
	 			$encrypted = NULL;
	 			
	 			try {
		 			$conn = ConnectionProvider::instance()->getConnection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT HEX(AES_ENCRYPT(clientID," .
						" '$key' ))" .
						" as gcID " .
						"FROM GoAbroad_Main.tbclient " .
						"WHERE clientID = " . $clientID ;
				$rs->Execute($sql);
		 	
		 		if (0 == $rs->Recordcount()){
                    return 0;            
                }                    
                else {
		 			try {		
 						$encrypted = $rs->Result(0,"gcID"); 				// decrypted clientID value
 					} 					
					catch (Exception $e) {
					   throw $e;
					}
				
 				}
 				
 				return $encrypted; 				
	 		}	
	 		
	 		
		/**
		 * Static Function
         * Purpose: Returns the decrypted value of encrypted string 
         */ 
	 		public static function getDecrypted($gcID,$key){
	 			
	 			$decrypted = NULL;
	 			
	 			try {
		 			$conn = ConnectionProvider::instance()->getConnection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT clientID " .
						"FROM GoAbroad_Main.tbclient " .
						"WHERE HEX(AES_ENCRYPT(clientID," .
						" '$key' " .
						")) = '$gcID' " ;
				$rs->Execute($sql);
		 		
		 		if (0 == $rs->Recordcount()){
                    return 0;            
                }                    
                else {
		 			try {
 						$decrypted = $rs->Result(0,"clientID"); 				// decrypted clientID value												
 					} 					
					catch (Exception $e) {
					   throw $e;
					   
					}				
 				}
             
 				return $decrypted; 				
	 		}	
		
		
		/**
		 * Static Function
         * Purpose: Returns decrypted value if valid url, or false if not
         */ 
	 		public static function isValidAdmin($gcID, $ref){
	 			
	 			$key1 = 'V68DrejO7Gfnb7eRf107Bq';
				$key2 = 'n73oGtW49VB0gQxgh3Ifk8';
				
				$decrypted = AdminGroup::getDecrypted($gcID,$key1);
				$hash = AdminGroup::getEncrypted($decrypted,$key2);
				
			
				if ($ref == $hash ){
					return $decrypted;
				}
				else{
					return false;
				}	
						
	 		}	
	 	
	 		
	 	/**
		 * Static Function
         * Purpose: Returns the advisorGroupID of a traveler; the admin group of the given traveler
         */ 
	 		public static function getAdvisorGroupID($travelerID){
	 			
	 			$advisorGroupID = -1;
	 			
	 			try {
		 			$conn = ConnectionProvider::instance()->getConnection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				/**$sql = "SELECT groupID " .
						"FROM tblGrouptoAdvisor " .
						"WHERE travelerID = '$travelerID' " ;*/
				$sql = "SELECT groupID " .
						"FROM tblGroup " .
						"WHERE administrator = '$travelerID' " .
						"AND discriminator > 1";		
				$rs->Execute($sql);
		 		
		 		if (0 == $rs->Recordcount()){
                    return -1;            
                }                    
                else {
		 			try {
 						$advisorGroupID = $rs->Result(0,"groupID"); 									
 					} 					
					catch (Exception $e) {
					   throw $e;
					   
					}
				
 				}
                
 				return $advisorGroupID; 				
	 						
	 		}
	 	
	 	/**
		 * Static Function
         * Purpose: Returns the advisorClientID of a traveler; the clientID of the admin group of the given traveler
         */ 
	 		public static function getAdvisorClientID($travelerID){
	 			
	 			try {
		 			$conn = ConnectionProvider::instance()->getConnection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT clientID " .
						"FROM tblGrouptoAdvisor " .
						"WHERE travelerID = '$travelerID' " ;
				$rs->Execute($sql);
		 		
		 		if (0 == $rs->Recordcount()){
                    return 0;            
                }                    
                else {
		 			try {
 						$advisorClientID = $rs->Result(0,"clientID"); 									
 					} 					
					catch (Exception $e) {
					   throw $e;					   
					}
 				}
                
 				return $advisorClientID; 				
	 						
	 		}
	 		
	 		
	 		
	 			
	 		/***
	 		 * Added by naldz:(Oct. 19, 2006)
	 		 * Purpose: Add a function that would get the absolute position of a record
	 		 */	
	 		 public function getAbsolutePosition($method,$recordID,$filtercriteria = NULL,$rowslimit=null){
	 		 	$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
	 		 	require_once("Class.iRecordset.php");
	 		 	switch($method){
	 		 		case "getNews":	 		 			               
		                $limitstr = '';
		           		$filterstr = '';
		                
		                if (NULL != $rowslimit){
		                    $offset   = $rowslimit->getOffset();
		                    $rows     = $rowslimit->getRows();
		                    $limitstr = " LIMIT " . $offset . " , " . $rows;
		                }  
		               
		                if (NULL != $filtercriteria){
		                	/*
		                	$condition = $filtercriteria->getConditions();	
		        			$attname   = $condition[0]->getAttributeName();
		        			$value 	   = $condition[0]->getValue();		        			
		        			switch($condition[0]->getOperation()){		        				
		        				case FilterOp::$EQUAL:
		        					$operator = " = ";
		        					break;
		        				case FilterOp::$NOT_EQUAL:
		        					$operator = " <> ";
		        					break;
		        				default:
		        					$operator = " = ";
		        					break;
		        			}		        			
		        			$filterstr = " AND " . $attname . $operator . $value;
		        			*/
		        			$condition = $filtercriteria->getConditions();	
		        			$booleanop = $filtercriteria->getBooleanOps();
		        			
		        			for($i=0;$i<count($condition);$i++){
			        			$attname   = $condition[$i]->getAttributeName();                    			
			        			$value 	   = $condition[$i]->getValue();
			        			
			        			switch($condition[$i]->getOperation()){	
			        				case FilterOp::$EQUAL:
			        					$operator = " = ";
			        					break;
			        				case FilterOp::$NOT_EQUAL:
			        					$operator = " <> ";
			        					break;
			        				case FilterOp::$GREATER_THAN_OR_EQUAL:
			        					$operator = " >= ";
			        					break;
			        				default:
			        					$operator = " = ";
			        					break;
			        			}		        					 
		        				$filterstr .= " " . $booleanop[$i] . " ". $attname . $operator . $value;
		        			}	
		        					                	
		                }		           
		                //$sql = "SELECT count(newsID) as offset from tblNews WHERE newsID >= $recordID and (groupID = '$this->mGroupID' OR groupID = '$this->mParentID') " . $filterstr . " ORDER BY adate DESC, newsID DESC ";		                
		                //$sql = "SELECT COUNT( * ) AS offset FROM tblNews WHERE tblNews.newsID <= (SELECT tblNews.newsID FROM tblNews WHERE ( groupID = '1' OR groupID = '0' ) AND newsID =19 ORDER BY adate DESC , newsID DESC )";
		                
		                $sql0 = "SET @x := 0;"; 
						$this->mRs->Execute($sql0);
						$sql = 	"SELECT _tbNews.rowNum as offset " .
								"FROM (" .
									"SELECT @x := @x+1 AS rowNum, newsID AS _newsID FROM tblNews " .
									"WHERE (groupID = '$this->mGroupID' OR groupID = '$this->mParentID') " . $filterstr . " " .
									"ORDER BY adate DESC , newsID DESC $limitstr" .
								") AS _tbNews " .
								"WHERE _tbNews._newsID = '$recordID'";
		                $this->mRs->Execute($sql);		                		                
		                $rsOffset = new iRecordset($this->mRs->Resultset());
		        		return ($rsOffset->retrieveRecordCount() > 0)?$rsOffset->getoffset():0;		        													
	 		 			break;
	 		 		default:
	 		 			return 0;
	 		 	}	 		 	
	 		 }
	 		 
	 		 static function getGroupIDByName( $group_name ){
	 		 	$conn = ConnectionProvider::instance()->getConnection();
				$rs   = new Recordset($conn);
				$sql  = sprintf("SELECT groupID FROM tblGroup WHERE `name` = '%s'", addslashes($group_name));
				$rs->Execute($sql);
				return ( $rs->Recordcount() && $rs->Result(0, 'groupID') )? $rs->Result(0, 'groupID'): 0;
	 		 }
	 		 
	 		 function getGroupMembersJournals($gID){
				/*
					edits: ianne 01/20/2008
					travels are now flagged as deleted instead of deleting from db. 
					edited function to include only travels with deleted = 0
				*/
		
				$conn = ConnectionProvider::instance()->getConnection();
				$rs   = new Recordset($conn);
				$arr  = array();
				$sql  = sprintf
						(
							'SELECT qryGroupTravel.* ' .
							'FROM qryGroupTravel ' .
							'WHERE ' .
							'    groupID = %d ' .
							'AND (privacypref = 0 OR privacypref = 3) ' .
							'AND travelerID NOT IN (SELECT travelerID FROM tblGrouptoAdvisor) ' .
							'AND travelID NOT IN (SELECT travelID FROM tblTravel WHERE deleted = 1) ' .
							'ORDER BY lastupdated DESC ' 
							,$gID
						);
				$rs->Execute($sql); 
				   
				if( $rs->Recordcount() ){
					while( $row = mysql_fetch_assoc($rs->Resultset())){
						$obj_journal = new Travel;
						$obj_journal->setTravelID    ( $row['travelID']     ); 
						$obj_journal->setLocationID  ( $row['locID']        );
						$obj_journal->setTitle       ( $row['title']        );
						$obj_journal->setDescription ( $row['description']  );
						$obj_journal->setRating      ( $row['rating']       );
						$obj_journal->setVotes       ( $row['votes']        );
						$obj_journal->setViews       ( $row['viewed']       );
						$obj_journal->setLastUpdated ( $row['lastupdated']  );
						$obj_journal->setPrimaryphoto( $row['primaryphoto'] );
						$obj_journal->setTravelerID  ( $row['travelerID']   );
						$obj_journal->setPrivacyPref ( $row['privacypref']  );
						$arr[] = $obj_journal; 
					}			
				}	
				return $arr;
	 		 }

		/**
		 * Function name: getInquiry
		 * @return InquiryBox
		 * Instantiates class InquiryBox
		 */
		 
		function getInquiryBox(){
			
			// Instantiates class Inquiry
			
			$my_inquiry = new InquiryBox ($this->mSendableID);
			$this-> inquiryBox = $my_inquiry;
			
			return $this-> inquiryBox; 
		}

		/**
		 *	@author Cheryl Ivy Q. Go		09 July 2008
		 *	Purpose: Gets all subgroups of group
		 */
		function getAllSubGroups(){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT groupID, sendableID, name, description, discriminator, groupaccess, administrator, parentID, " .
			 			"photoID, inverserank, featuredsubgrouprank " .
					"FROM tblGroup " .
					"WHERE isSuspended = 0 " .
					"AND parentID = " . $this->mGroupID . " " .
					"GROUP BY groupID " .
					"ORDER BY name ASC";
			$this->mRs->Execute($sql);

			$arrSubGroups = array();
			if (0 < $this->mRs->Recordcount()){
				while($Group = mysql_fetch_assoc($this->mRs->Resultset())){
					try {
						$subgroupData = array();
						$subgroupData["name"]		= $Group['name'];
			 			$subgroupData["photoID"]	= $Group['photoID'];
						$subgroupData["groupID"]	= $Group['groupID'];
			 			$subgroupData["parentID"]	= $Group['parentID'];
						$subgroupData["clientID"]	= $this->mClientID;
			 			$subgroupData["inverserank"]	= $Group['inverserank'];
						$subgroupData["sendableID"]		= $Group['sendableID'];
						$subgroupData["groupaccess"]	= $Group['groupaccess'];
						$subgroupData["description"]	= $Group['description'];
			 			$subgroupData["discriminator"]	= $Group['discriminator'];
			 			$subgroupData["administrator"]	= $Group['administrator'];
						$subgroupData["featuredsubgrouprank"]	= $Group["featuredsubgrouprank"];
					
						$subgroup = new AdminGroup();
						$subgroup->setAdminGroupData($subgroupData);
					
                    }
                    catch (Exception $e) {
                       throw $e;
                    }
                    $arrSubGroups[] = $subgroup;
				}
			}

			return $arrSubGroups;
		}		
		
        /**
          Purpose: Returns the group's moderator.
         */
            function getModerators($_list = false){
				$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
				$this->mArrModerators = array();
				
				$sql = "SELECT * FROM tblGrouptoFacilitator WHERE groupID = '$this->mGroupID' AND status = 1 AND type = " . self::MODERATOR ;
				$this->mRs->Execute($sql);
				
				while ($recordset = mysql_fetch_array($this->mRs->Resultset())){
					
					if ($_list)
						$this->mArrModerators[$recordset['travelerID']] = $recordset['travelerID'];
					else{
						require_once('travellog/model/Class.Moderator.php');
						$this->mArrModerators[] = new Moderator($recordset['travelerID'], $this->mGroupID);
					}
						
				}
				return $this->mArrModerators;
			}

		/**
			Purpose: add a moderator to a group
		 */	
			function addModerator($_travelerID){
				$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
				// if this traveler has a previous record of being a group moderator; activate
				if ($this->isInActiveModerator($_travelerID)){
					$sql = "UPDATE tblGrouptoFacilitator SET status = 1 WHERE groupID = '$this->mGroupID' AND travelerID = '$_travelerID' AND type = " . self::MODERATOR ;
					$this->mRs->Execute($sql);
				}
				elseif (!$this->isModerator($_travelerID)){
					
					$sql = "INSERT into tblSendable (sendabletype) " .
	                        "VALUES ('4')";
	                $this->mRs->Execute($sql);       
	                $_sendableID = $this->mRs->GetCurrentID();
	
					$sql = "INSERT INTO tblGrouptoFacilitator (groupID, travelerID, sendableID, status, type) VALUES ('$this->mGroupID','$_travelerID', '$_sendableID' , 1, " . self::MODERATOR . ")";
					$this->mRs->Execute($sql);
				}
				 
			}
		
		/**
			Purpose: remove this traveler as group moderator
		 */	
			function removeModerator($_travelerID){
				$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
				if ($this->isModerator($_travelerID)){
					$sql = "UPDATE tblGrouptoFacilitator SET status = 0 WHERE groupID = '$this->mGroupID' AND travelerID = '$_travelerID' AND type = " . self::MODERATOR;
					
					$this->mRs->Execute($sql);
				}
				
			}
			
		/**
			Purpose: check if traveler is a moderator of this group
		 */	
			function isModerator($_travelerID){
				if($_travelerID == 0){
					return false;
				}
				$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT * FROM tblGrouptoFacilitator WHERE groupID = '$this->mGroupID' AND travelerID = '$_travelerID' AND status = 1 AND type = " . self::MODERATOR;
				$this->mRs->Execute($sql);
				
				if ($this->mRs->Recordcount())
					return true;
				else
					return false;
			}
					
		/**
			Purpose: check if traveler is an inactive moderator of this group
		 */	
			function isInActiveModerator($_travelerID){
				$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT * FROM tblGrouptoFacilitator WHERE groupID = '$this->mGroupID' AND travelerID = '$_travelerID' AND status = 0 AND type = " . self::MODERATOR;
				$this->mRs->Execute($sql);

				if ($this->mRs->Recordcount())
					return true;
				else
					return false;
			}
		
		
		 /**
	         * Purpose: This is called when a traveler is invited by advisor to join a subgroup.
	         * Function parameter is travelerID of existing .net user and a member of the parent group of this subgroup
	         */
	            function assignTravelertoSubGroup($_traveler = null){
					$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
	               	if (!$this->isMember($_traveler) ){                 
                       
                        $source = $this->getSendableID();
                        $activestatus = 0;							// 0 = non-member; 1=active member
						$actiontype = 7;							// 7 = invite
						$this->addMembershipUpdates($_traveler, $source, $activestatus, $actiontype);
							
                        $travID = $_traveler->getTravelerID();
                        
                        if ($this->isInDeniedInviteList($_traveler)){
                        	$sql = "UPDATE tblInviteList SET status = 0 WHERE groupID = '$this->mGroupID' AND travelerID = '$travID' ";
                        	$this->mRs->Execute($sql);                        	
                        }
                        elseif (!$this->isInInviteList($_traveler)){                        	
                        	$sql = "INSERT into tblInviteList (groupID, travelerID) VALUES ('$this->mGroupID', '$travID')";
                        	$this->mRs->Execute($sql);
                        }
                    }	
	                        
	            }
			
			function getRelatedPhotoAlbums(){
				
				$albums = array();
				
				if($this->getTravels()){
					require_once("travellog/model/Class.TravelLog.php");	
					foreach($this->getTravels() as $travel){
						if($travel->isApproved($this->mGroupID)){
							$albums[] = TravelLog::getTravellogs($this);
						}
					}
					
				}
				
				return $albums;
			}
			
		function getServerName(){
			if (!isset($this->serverName)) {
				$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = sprintf('SELECT serverName FROM tblConfig WHERE groupID = %d', $this->mGroupID);
				$this->mRs->Execute($sql);
				$this->serverName	=	$this->mRs->Result(0,'serverName');
				return $this->mRs->Recordcount() ? $this->mRs->Result(0,'serverName') : false;
			} else {
				return $this->serverName;
			} 
		}
		
		function getSubGroupsByTravelerID($_travelerID = 0, $list = false){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "select a.* from tblGroup as a, tblGrouptoTraveler  as b where b.`travelerID` = $_travelerID and  a.isSuspended = 0 and a.`groupID` = b.`groupID` and a.parentID =  $this->mGroupID GROUP by a.groupID ";
		
			$this->mRs->Execute($sql);
			
			$arrSubGroups = array();
			$resultSet = $this->mRs->Resultset();
			
			while ($recordset = mysql_fetch_array($resultSet)){
	
				$subgroupData = array();
				$subgroupData["groupID"] = $recordset["groupID"];
				$subgroupData["sendableID"] = $recordset["sendableID"];
				$subgroupData["name"] = $recordset["name"];
				$subgroupData["description"] = $recordset["description"];
	 			$subgroupData["discriminator"] = $recordset["discriminator"];
				$subgroupData["groupaccess"] = $recordset["groupaccess"];
	 			$subgroupData["administrator"] = $recordset["administrator"];
	 			$subgroupData["parentID"] = $recordset["parentID"];
	 			$subgroupData["photoID"] = $recordset["photoID"];
	 			$subgroupData["inverserank"] = $recordset["inverserank"];
												
				$sql = "SELECT clientID FROM tblGrouptoAdvisor WHERE groupID = $this->mGroupID";
                $this->mRs->Execute($sql);
                 
                if (0 < $this->mRs->Recordcount()){
                     $subgroupData["clientID"]  = $this->mRs->Result(0,"clientID");
                }
			
				$group = new AdminGroup();
				$group->setAdminGroupData($subgroupData);
			
				$arrSubGroups[] = ($list) ? $group->getName() : $group ; 
			}
			
			return $arrSubGroups;
			
		}
		
		//added by Jul
		//returns array of subgroup names where a traveler is affiliated
		function getSubgroupAffiliations($travelerID=0){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$parentID = $this->mGroupID;
			$sql = "SELECT tblGroup.name AS name
					FROM tblGroup, tblGrouptoTraveler
					WHERE tblGroup.parentID = $parentID
						AND tblGroup.groupID = tblGrouptoTraveler.groupID
						AND tblGrouptoTraveler.travelerID = $travelerID
					GROUP BY tblGroup.groupID";
			$this->mRs->Execute($sql);
			$subgroupnames = array();
			while($recordset = mysql_fetch_array($this->mRs->Resultset())){
				$subgroupnames[] = $recordset["name"];
			}
			return $subgroupnames;
		}
		
		function getSubGroupInvitationsByTravelerID($_travelerID = 0, $list = false){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "select a.* from tblGroup as a, tblInviteList  as b where b.`travelerID` = $_travelerID and a.isSuspended = 0 and a.`groupID` = b.`groupID` and b.status <> 1 and a.parentID =  $this->mGroupID GROUP by a.groupID ";
		
			$this->mRs->Execute($sql);
			
			$arrSubGroups = array();
			$resultSet = $this->mRs->Resultset();
			
			while ($recordset = mysql_fetch_array($resultSet)){
	
				$subgroupData = array();
				$subgroupData["groupID"] = $recordset["groupID"];
				$subgroupData["sendableID"] = $recordset["sendableID"];
				$subgroupData["name"] = $recordset["name"];
				$subgroupData["description"] = $recordset["description"];
	 			$subgroupData["discriminator"] = $recordset["discriminator"];
				$subgroupData["groupaccess"] = $recordset["groupaccess"];
	 			$subgroupData["administrator"] = $recordset["administrator"];
	 			$subgroupData["parentID"] = $recordset["parentID"];
	 			$subgroupData["photoID"] = $recordset["photoID"];
	 			$subgroupData["inverserank"] = $recordset["inverserank"];
												
				$sql = "SELECT clientID FROM tblGrouptoAdvisor WHERE groupID = $this->mGroupID";
                $this->mRs->Execute($sql);
                 
                if (0 < $this->mRs->Recordcount()){
                     $subgroupData["clientID"]  = $this->mRs->Result(0,"clientID");
                }
			
				$group = new AdminGroup();
				$group->setAdminGroupData($subgroupData);
			
				$arrSubGroups[] = ($list) ? $group->getName() : $group ; 
			}
			
			return $arrSubGroups;
			
		}
		
		
		// save settings of group's display rank for subgroups
		
		function saveRankSettings(){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "UPDATE tblGroup SET ranksettings = " . $this->mRankSettings . " WHERE groupID = " . $this->mGroupID;
			$this->mRs->Execute($sql);
			
		}
		
		/**
		 * Returns the featured subgroups of this parent group.
		 * 
		 * @throws exception
		 * @see SqlResourceIterator
		 * @return SqlResourceIterator Returns the featured subgroups of this parent group. The SqlResourceIterator object returned is of class SubGroup.
		 */
		function getFeaturedSubGroups($limit = NULL){
			try {
				$handler = new dbHandler();
				$sql = "SELECT a.*, c.categoryID, c.categorysubgrouprank " .
					" FROM tblGroup as a, tblSubGroupCategory as b, tblGroupToSubGroupCategory as c " .
					" WHERE a.parentID = ".$handler->makeSqlSafeString($this->mGroupID).
					"	AND b.categoryID = c.categoryID " .
					"	AND b.isFeatured = 1 " .
					" AND a.groupID = c.groupID " .
					" AND a.parentID = b.parentID " .
					" AND a.discriminator = 2 " .
					" AND a.featuredsubgrouprank > 0 " .
					" ORDER BY featuredsubgrouprank ";
				$sql .= ($limit instanceof RowsLimit) ? " ".$limit->createSqlLimit() : "";
				
				return new SqlResourceIterator($handler->execute($sql), "SubGroup");
			} 
			catch(exception $ex){
				throw $ex;
			}
		}
		
    /**
     * Returns the featured categories.
     * 
     * @throws exception
     * @return SqlResourceIterator Returns the featured categories. Return type is SqlResourceIterator of class CustomizedSubGroupCategory.
     */
    function getFeaturedSubGroupCategories(){
		  try {
			  $handler = new dbHandler();
			  
			  $sql = "SELECT * FROM tblSubGroupCategory " .
			    " WHERE parentID = ".$handler->makeSqlSafeString($this->mGroupID).
					" AND isFeatured = 1 ORDER BY categoryRank";
			  
			  return new SqlResourceIterator($handler->execute($sql), "CustomizedSubGroupCategory");
		  }
		  catch(exception $ex){
			  throw $ex;
		  }  	
    }
    
    /**
     * Returns the subgroups of this parent group.
     * 
     * @param string $searchKey The keyword to be match with the names of the subgroups.
     * @param integer $access Flag whether to retrieve subgroups that are active, inactive or both.
     * @param string $order The order of the subgroups to be retrieved.
     * @param RowsLimit $rowslimit The limit of the subgroups to be retrieved.
     * @see RowsLimit
		 * @throws exception
		 * @return SqlResourceIterator Returns the subgroups of this parent group. The class used by the returned SqlResourceIterator is SubGroup.
     */
    function getMySubGroups($searchKey = null, $access = null, $order = NULL, $rowslimit = NULL){
			try {
				$grp_access = (SubGroup::ACTIVE == $access) ? "1,2,3" : null;
				$grp_access = (SubGroup::INACTIVE == $access) ? "4" : $grp_access;
				
				$handler = new dbHandler();
				$sql = "SELECT a.*, c.categoryID, c.categorysubgrouprank " .
					" FROM tblGroup as a, tblSubGroupCategory as b, tblGroupToSubGroupCategory as c " .
					" WHERE a.parentID = ".$handler->makeSqlSafeString($this->mGroupID).
					"	AND b.categoryID = c.categoryID " .
					" AND a.groupID = c.groupID " .
					" AND a.parentID = b.parentID ";
	      $sql .= (is_null($grp_access)) ? "" : " AND a.groupaccess IN ($grp_access) ";
	      $sql .= (is_null($searchKey) OR "" == trim($searchKey)) ? "" : " AND a.name LIKE ".$handler->makeSqlSafeString($searchKey);
	      $sql .= " UNION ";
	      $sql .= " SELECT a.*, null as categoryID, 0 as categorysubgrouprank FROM tblGroup as a" .
	      	" WHERE a.parentID = ".$handler->makeSqlSafeString($this->mGroupID).
	      	" AND a.groupID NOT IN " .
	      	" 	(" .
	      	"			SELECT groupID FROM tblGroupToSubGroupCategory as b, tblSubGroupCategory as c" .
	      	"			WHERE b.categoryID = c.categoryID AND c.parentID = ".$handler->makeSqlSafeString($this->mGroupID).
	      	"		)";
	      
	      $sql = "SELECT * FROM ($sql) as temp ";
				$sql .= (is_null($order)) ? " ORDER BY name ASC " : " ORDER BY ".$order;				
				$sql .= ($rowslimit instanceof RowsLimit) ? " ".$rowslimit->createSqlLimit() : "";
	      
	      return new SqlResourceIterator($handler->execute($sql), "SubGroup");
	    }
	    catch(exception $ex){
	    	throw $ex;
	    }
    }
    
    /**
     * Returns the number of subgroups of this parent group.
     * 
     * @param string $searchKey The keyword to be match with the names of the subgroups.
     * @param integer $access Flag whether to count subgroups that are active, inactive or both.
		 * @throws exception
		 * @return SqlResourceIterator Returns the subgroups of this parent group. The class used by the returned SqlResourceIterator is SubGroup.
     */
    function getMySubGroupsCount($searchKey = null, $access = null){
			try {
				$grp_access = (SubGroup::ACTIVE == $access) ? "1,2,3" : null;
				$grp_access = (SubGroup::INACTIVE == $access) ? "4" : $grp_access;
				
				$handler = new dbHandler();		
				
				$sql = "SELECT count(groupID) as count from tblGroup " .
	        " WHERE parentID = ".$handler->makeSqlSafeString($this->mGroupID);
	      $sql .= (is_null($grp_access)) ? "" : " AND groupaccess IN ($grp_access) ";
	      $sql .= (is_null($searchKey) OR "" == trim($searchKey)) ? "" : " AND name LIKE ".$handler->makeSqlSafeString($searchKey);
	      
	      $it = new SqlResourceIterator($handler->execute($sql));
	      
	      return $it->count();
	    }
	    catch(exception $ex){
	    	throw $ex;
	    }
    }
	
    /**
     * Retrieves all the subgroups of this parent group (admin group).
     * 
     * @param RowsLimit $limit The LIMIT of the subgroups to be retrieve.
     * @see RowsLimit
		 * @param FilterCriteria2 $order The ORDER of subgroups to be retrieved.
		 * @see FilterCriteria2
		 * @param boolean $isCount The flag whether to get the count only or the array of subgroups. If true returns the subgroups count, else returns the subgroups object.
		 * @param string $searchKey
		 * @throws exception
		 * @return integer|array If the $isCount flag is false returns an Array of SubGroup object, else returns count of subgroups.
     */
    function getSubGroups($rowslimit = NULL, $order = NULL, $isCount = false, $searchKey=""){
    	if (false == $isCount) {
    	  //if (is_null($this->mSubGroups)) {
    	    $this->mSubGroups = array();
    	    $handler = new dbHandler();
    	
          $sql = "SELECT * from tblGroup " .
            " WHERE parentID = ".$handler->makeSqlSafeString($this->mGroupID)."" .
            " AND name LIKE ".$handler->makeSqlSafeString("%".$searchKey."%");
			    $sql .= (is_null($order)) ? " ORDER BY name ASC " : SqlUtil::createOrderByClause($order)." ";				
			    $sql .= (is_null($rowslimit)) ? "" : SqlUtil::createLimitClause($rowslimit)." ";
								
  		    try {
  		      $rs = new iRecordset($handler->execute($sql));
			
			      while (!$rs->EOF()) {
              $grp = new AdminGroup();
              $grp->setAdminGroupData($rs->current());
              $this->mSubGroups[] = $grp;
          
              try {
                $rs->next();
              }
              catch (exception $ex) {
                throw new exception($ex->getMessage());
				      }
            }
  		    }
  		    catch(exception $ex){
  		    	throw new exception($ex->getMessage);
  		    }
			//	}
				
				return $this->mSubGroups;
    	}
    	else {
    		return $this->getSubGroupsCount($searchKey);
    	}	                
    }
    
    /**
     * Returns the number of subgroups of this parent group (admin group).
     * 
     * @param string $keyword The keyword to be match to the name of the groups. All groups that is like this keyword will be retrieved.
		 * @throws exception
		 * @return integer Returns the number of subgroups of this parent group (admin group).
     */
    function getSubGroupsCount($keyword = ""){
      //if (is_null($this->mSubGroupCount)) {
    	  $this->mSubGroupCount = 0;
    	  $handler = new dbHandler();
    	
        $sql = "SELECT count(*) as count from tblGroup " .
          " WHERE parentID = ".$handler->makeSqlSafeString($this->mGroupID)."";
		if( "" != $keyword ){
          $sql .= " AND name LIKE ".$handler->makeSqlSafeString("%".$keyword."%");
		}			
  		  try {
  		    $rs = new iRecordset($handler->execute($sql));
			    
			    if (0 < $rs->retrieveRecordCount()) {
			      $record = $rs->current();
			      $this->mSubGroupCount = $record['count'];
			    }			
  		  }
  		  catch(exception $ex){
  		  	throw new exception($ex->getMessage);
  		  }
    	//}
    	
    	return $this->mSubGroupCount;              
    }
    
    /**
     * Returns the categories of subgroups of this parent group.
     * 
     * @param string $keyword The category name to be searched.
     * @param boolean $isUncategorizedIncluded The flag whether to include the uncategorized category.
     * @param RowsLimit $limit The LIMIT of the categories to be retrieved.
     * @see RowsLimit
     * @throws exception
     * @see SqlResourceIterator
     * @return SqlResourceIterator Returns the categories of subgroups of this parent group. Returns SqlResourceIterator of class CustomizedSubGroupCategory.
     */
    function getSubGroupCategories($keyword = null, $isUncategorizedIncluded = false, $limit = null){
		  try {
			  $handler = new dbHandler();
			  
			  $sql = "SELECT * FROM tblSubGroupCategory " .
			    " WHERE parentID = ".$handler->makeSqlSafeString($this->mGroupID);
			  $sql .= (is_null($keyword) OR "" == trim($keyword)) ? "" : " AND name LIKE " .$handler->makeSqlSafeString($keyword);
			  $sql .= ($isUncategorizedIncluded) ? "" : " AND isUncategorized = 0 ";
			  $sql .= " ORDER BY categoryRank";
			  $sql .= (is_null($limit)) ? "" : SqlUtil::createLimitClause($limit)." ";
			  
			  return new SqlResourceIterator($handler->execute($sql), "CustomizedSubGroupCategory");
		  }
		  catch(exception $ex){
			  throw $ex;
		  }
    }
    
    /**
     * Returns the number of subgroup category of this admin group.
     * 
     * @param string $keyword The category name to be searched.
     * @param boolean $isUncategorizedIncluded The flag whether to include the uncategorized category in counting.
     * @throws exception
     * @return integer Returns the number of subgroup category of this admin group.
     */
    function getSubGroupCategoriesCount($keyword = null, $isUncategorizedIncluded = false){	  
		  try {
				$handler = new dbHandler();

				$sql = "SELECT count(*) as count FROM tblSubGroupCategory " .
					" WHERE parentID = ".$handler->makeSqlSafeString($this->mGroupID);
			  $sql .= (is_null($keyword) OR "" == trim($keyword)) ? "" : " AND name LIKE " .$handler->makeSqlSafeString($keyword);
			  $sql .= ($isUncategorizedIncluded) ? "" : " AND isUncategorized = 0 ";
			  
			  $rs = new SqlResourceIterator($handler->execute($sql));
			  
			  return $rs->count();		  
		  }
		  catch(exception $ex){
			  throw $ex;
		  }
    }
    
    /**
     * Returns the category for uncategorized subgroups.
     * 
     * @throws exception
     * @return CustomizedSubGroupCategory Returns the category for uncategorized subgroups. If there are no category for uncategorized subgroup yet, this returns null.
     */
    function getCategoryForUncategorizedSubGroups(){	  
		  try {
				$handler = new dbHandler();

				$sql = "SELECT * FROM tblSubGroupCategory " .
					" WHERE parentID = ".$handler->makeSqlSafeString($this->mGroupID).
					" AND isUncategorized = 1 ";
			  
			  $rs = new SqlResourceIterator($handler->execute($sql));
			 	if ($rs->hasNext()) {
			 		return new CustomizedSubGroupCategory($rs->next());
			 	}	
			 	else {
			 		return null;
			 	}	  
		  }
		  catch(exception $ex){
			  throw $ex;
		  }
    }
		
		/**
		 * Change the ranking of the given subgroup in the database using the given rank.
		 * 
		 * @param AdminGroup $subgroup the subgroup to be updated
		 * @param integer $rank the new rank of the subgroup
		 * @param string $action one of the following (showActive or showInActive)
		 * @return void
		 */
		function updateNewRankOfSubGroup($subgroup, $given_rank, $action) {
			$handler = new dbHandler();
			$grpAccessList = "1,2,3";
			$grpAccessList = ('showInActive' == $action) ? "4" : $grpAccessList;
      
			$sID = (is_numeric($subgroup)) ? $subgroup : $subgroup->getGroupID();
			$subgroups = array();
      
      $sql = "SELECT DISTINCT(a.groupID) FROM tblGroup as a " .
      	" WHERE a.groupaccess IN (".$grpAccessList.") " .
      	" AND a.parentID = ".$handler->makeSqlSafeString($this->mGroupID).
				" ORDER BY subgrouprank , datecreated DESC " ;
			
			try {
				$rs = new iRecordset($handler->execute($sql));
				$cnt = 1;
				
				while(!$rs->EOF()){
					$record = $rs->current();
					$cnt = ($cnt == $given_rank AND $sID <> $record['groupID']) ? $cnt + 1 : $cnt;
					
					if ($sID <> $record['groupID']) {
					  $subgroups[$cnt] = $record['groupID'];
					  $cnt++;
					}
					
					$rs->next();
				}
				
				$subgroups[$given_rank] = $sID;
				
				SubGroupRankHandler::arrangeRankOfSubGroups($subgroups);
				
				$this->setRankSettings(SubGroupSortType::CUSTOM);
		  	$this->saveRankSettings();
			}
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}
		}
		
		/**
		 * Arranges by most recent created or alphabetically the uncategorized subgroups of this parent group. If the value of $sortType is equal
		 * to SubGroupSortType::MOST_RECENT_CREATED the subgroups are arrange in most recent created and when the value of $sortType is equal to 
		 * SubGroupSortType::ALPHABETICAL it is arrange alphabetically.
		 * 
		 * @throws exception
		 * @param integer $sortType Values can be SubGroupSortType::ALPHABETICAL and SubGroupSortType::MOST_RECENT_CREATED only.
		 * @see SubGroupSortType
		 * @return void
		 */
		function autoSortGroups($sortType) {
			try {
				$handler = new dbHandler();
      	$sql = "SELECT DISTINCT(a.groupID) FROM tblGroup as a " .
      		" WHERE a.parentID = ".$handler->makeSqlSafeString($this->mGroupID)." AND a.groupID " .
					" NOT IN (" .
					"   SELECT a.groupID " .
					"   FROM tblGroupToSubGroupCategory as a, tblSubGroupCategory as b " .
		  		"   WHERE a.categoryID = b.categoryID AND b.parentID = ".$handler->makeSqlSafeString($this->mGroupID).
					" ) ";
				$rs = new iRecordset($handler->execute($sql));
				
				$sql = "SET @count = 0";
				$handler->execute($sql);
				
				$sql = "UPDATE tblGroup SET subgrouprank = (SELECT @count:= @count + 1) " .
					" WHERE groupID IN (";
				
				while(!$rs->EOF()){
					$record = $rs->current();
					$sql .= $record['groupID'].",";
					
					try {
						$rs->next();
					}
					catch(exception $ex){
						throw new exception($ex->getMessage());
					}
				}
				$sql = preg_replace("/,$/",") ",$sql);
					
				switch ($sortType) {
					case SubGroupSortType::ALPHABETICAL:
						$sql .= " ORDER BY name";
					break;
					case SubGroupSortType::MOST_RECENT_CREATED:
					  $sql .= " ORDER BY datecreated DESC ";
					break;
					default:
						throw new exception("Sort type not recognized!");
				}
				
				$handler->execute($sql);
				
				$this->setRankSettings($sortType);
		  	$this->saveRankSettings();
			}
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}
		}
		
		/**
		 * Arranges the rank of the subgroup categories alphabetically, by most recent created or using the categoryRank. 
		 * If $sortType is equal to SubGroupSortType::ALPHABETICAL the subgroup categories are arranged alphabetically 
		 * and when the value of $sortType is equal to SubGroupSortType::MOST_RECENT_CREATED, the subgroup categories
		 * are arranged by most recent created.
		 * 
		 * @throws exception
		 * @param integer $sortType Values can be SubGroupSortType::ALPHABETICAL and SubGroupSortType::MOST_RECENT_CREATED only.
		 * @param integer $cnt Incremented by 1, this is the starting rank in the arrangement of subgroup category rank.
		 * @return void
		 */
		function autoSortSubGroupCategoryRanks($sortType, $cnt = 0){
			try {
				$handler = new dbHandler();
				
				$sql = "SET @count = ".$cnt;
				$handler->execute($sql);
				
				$sql = "UPDATE tblSubGroupCategory " .
					" SET categoryRank = (SELECT @count:=@count+1) " .
					" WHERE parentID = ".$handler->makeSqlSafeString($this->mGroupID)."";
				
				switch ($sortType) {
					case SubGroupSortType::ALPHABETICAL:
						$sql .= " ORDER BY name";
					break;
					case SubGroupSortType::MOST_RECENT_CREATED:
					  $sql .= " ORDER BY datecreated DESC ";
					break;
					case SubGroupSortType::CATEGORY_RANK:
					  $sql .= " ORDER BY categoryRank ";
					break;
					default:
						throw new exception("Sort type not recognized!");
				}
				
				$handler->execute($sql);
			}
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}
		}
		
		/**
		 * Arranges the category ranks in ascending order of the given category IDs using the given $count 
		 * as the starting rank.
		 * 
		 * @throws exception
		 * @param array of integer $categoryIDs The array of category IDs. The ranking of the IDs in the array will be its ranking after saving in the database.
		 * @param integer $count The starting rank of the categories.
		 * @return void
		 */
		function sortCategoryRanks($categoryIDs, $cnt = 0){
		  try {
		  	$handler = new dbHandler();
		  	$subgroups_cnt = count($categoryIDs);
				
				for($cnt = $cnt + 1; $cnt <= $subgroups_cnt; $cnt++) {
					$sql = "UPDATE tblSubGroupCategory SET categoryRank = $cnt WHERE categoryID = ".$handler->makeSqlSafeString($categoryIDs[$cnt - 1]);
					$handler->execute($sql);
				}
		  }
		  catch (exception $ex){
		  	throw new exception($ex->getMessage());
		  }
		}
		
		/**
		 * Sets the rank settings for the subgroup categories of this parent group.
		 * 
		 * @throws exception
		 * @param $integer $newRankSettings
		 * @return void
		 */
		function saveSubGroupCategoriesRankSettings($newRankSettings){
			try {
				$handler = new dbHandler();
				
				$sql = "UPDATE tblGroup " .
					" SET subgroupcategoryranksettings = ".$handler->makeSqlSafeString($newRankSettings)." " .
					" WHERE groupID = ".$handler->makeSqlSafeString($this->mGroupID);
				$handler->execute($sql);
				
				$this->mSubGroupCategoriesRankSettings = $newRankSettings;
			}
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}
		}
	
		//function to get the latest available rank
		function getRankOfNewSubGroup($grpAccessList = 4) {
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT subgrouprank FROM tblGroup WHERE parentID = " . $this->getParentID() . 
			" AND groupaccess IN (" .  $grpAccessList . ") " .
			" ORDER BY subgrouprank DESC LIMIT 0 , 1" ;
			$this->mRs->Execute($sql);
			
			$newrank = 1;
			if ($this->mRs->RecordCount())
				$newrank = $this->mRs->Result(0,'subgrouprank') + 1;
				
			return $newrank;			
			
		}
		
		// function to add the subgrouprank of the newly created subgroup
		function updateRankOfNewSubGroup() {
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			if (4 == $this->mGroupAccess)
				$grpAccessList = "4";
			else
				$grpAccessList = "1,2,3";
			
			$newrank = $this->getRankOfNewSubGroup($grpAccessList);
			
			$sql = "UPDATE tblGroup SET subgrouprank = " . $newrank . " WHERE groupID = " . $this->mGroupID;
			$this->mRs->Execute($sql);
			
		}
		
		public static function searchSubGroupByName($parentGID = 0 , $_name = ''){
			
			$rs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = 	"SELECT groupID FROM tblGroup " .
					"WHERE parentID = $parentGID AND name LIKE '%$_name%'";
			$rs->execute($sql);

			$arrGroup = array();
			$arrGroupId = array();

			while( $row = mysql_fetch_assoc($rs->ResultSet()) )
				$arrGroupId[] = $row['groupID'];
			$arrGroup = GroupFactory::instance()->create($arrGroupId);

			return $arrGroup;
		}
		
		function toSimpleArray($_arrSubGroups = array(), $_key = 'groupID'){
			
			$simpleArrSubGroup = array();
			
			foreach($_arrSubGroups as $each){
				$simpleArrSubGroup[] = $each->getGroupID();
			}
			
			return $simpleArrSubGroup;
		}
		
		
		public static function getSubGroupIDsBySectionAndSortOrder($_groupID = 0 , $_action = 'showActive' , $_sortKey = 'Name'){

			//define: groupAccess:
			// 1 = open; 
			// 2 = open with approval; 
			// 3 = invite only ;
			// 4 = closed ;

			if ('showActive' == $_action)
				$grpAccessList = "1,2,3";
			elseif ('showInActive' == $_action)
				$grpAccessList = "4";
			
			if ('Name' == $_sortKey ){
				$sortKey = 'a.name';
				$sortOrder = 'ASC';
			}
			else if ('DateCreated' == $_sortKey ) {
				$sortKey = 'a.datecreated';
				$sortOrder = 'DESC';
			}
			else{
				$sortKey = 'a.name';
				$sortOrder = 'ASC';
			}
			
			$rs = new Recordset(ConnectionProvider::instance()->getConnection());
				
			$sql = "SELECT a.groupID , a.name FROM  tblGroup as a
			 		  WHERE a.parentID  = " . $_groupID . 
					" AND a.groupaccess IN (" .  $grpAccessList . ") " .
					" ORDER BY " . $sortKey . " " . $sortOrder;
			$rs->Execute($sql);

			$arrSubGroupIDsBySectionAndSortOrder = array();

			while($recordset = mysql_fetch_array($rs->Resultset())){

				$arrSubGroupIDsBySectionAndSortOrder[] = $recordset["groupID"];
				
			}
			return $arrSubGroupIDsBySectionAndSortOrder;

		}
		
		/**
		 * Method to get the programs/projects of this group
		 * @param RowsLimit $rowslimit an instance of RowsLimit holding the offset and page
		 * @param FilterCriteria2 $order an instance of FilterCriteria2 which holds an array of objects Condition specify the ordering of the results
		 * @param int $mode values: 0 = all groups&projects, 1=subgroups only, 2=projects only
		 * @return array of two elements: 1. an array of objects AdminGroup; 2. the total records fetched
		 * 
		 * Added by		:	Jul
		 * Date Added	:	2008-11-12
		 */
		function getSubGroupsAndProjects($rowslimit = NULL, $order = NULL, $mode=0, $filter=NULL){
            $this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
            $limitstr  = ''; 
			$filterstr = '';
            $orderstr  = '';
			$arrOrderBy  = array();
            
            if (NULL != $rowslimit){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }  
            
			if (NULL != $filter){

            	$condition = $filter->getConditions();	
    			$booleanop = $filter->getBooleanOps();
    			
    			for($i=0;$i<count($condition);$i++){
        			$attname   = $condition[$i]->getAttributeName();                    			
        			$value 	   = $condition[$i]->getValue();
        			
        			switch($condition[$i]->getOperation()){
        				
        				case FilterOp::$EQUAL:
        					$operator = " = ";
        					break;
        				case FilterOp::$NOT_EQUAL:
        					$operator = " <> ";
        					break;
        				case FilterOp::$GREATER_THAN_OR_EQUAL:
        					$operator = " >= ";
        					break;
						case FilterOp::$IN:
							$operator = " IN ";
							break;
        				default:
        					$operator = " = ";
        					break;
        			}
        			
        			$filterstr .= " " . $booleanop[$i] . " ". $attname . $operator . " $value ";
    			}	                		
            }

            if (NULL != $order){
            	$condition = $order->getConditions();	        			
    			
				foreach ($condition as $eachcondition) {
					$attname   = $eachcondition->getAttributeName();                   			
    				        			
        			switch($eachcondition->getOperation()){	        				
        				case FilterOp::$ORDER_BY:
        					$sortorder = " ASC ";
        					break;
        				case FilterOp::$ORDER_BY_DESC:
        					$sortorder = " DESC ";
        					break;
        				default:
        					$sortorder = " ASC ";
        					break;
        			}

					$arrOrderBy[] = $attname . $sortorder;
    			}
    			$orderstr = " ORDER BY " . implode(" , ", $arrOrderBy);
    			                		
            }
			$sql = "SELECT SQL_CALC_FOUND_ROWS *
			 		FROM tblGroup
					WHERE parentID = ".$this->mGroupID.$filterstr.$orderstr.$limitstr;
			if( 1 == $mode ){//group only
				$sql = "SELECT SQL_CALC_FOUND_ROWS * from tblGroup WHERE parentID = '$this->mGroupID' " .$filterstr. 
								" AND groupID NOT IN (
									SELECT subgroupID
									FROM tblCustomizedGroupSection AS a, tblCustomizedGroupSectiontoSubGroups AS b
									WHERE a.customizedGroupSectionID = b.customizedGroupSectionID
									AND a.advisorGroupID = " . $this->mGroupID . 
								")".
								$orderstr . $limitstr;
			}else if( 2 == $mode ){//projects only
				$sql = "SELECT SQL_CALC_FOUND_ROWS *
				 		FROM tblGroup
						WHERE parentID = ".$this->mGroupID.$filterstr."
							AND groupID IN (
								SELECT subgroupID
								FROM tblCustomizedGroupSection AS a, tblCustomizedGroupSectiontoSubGroups AS b
								WHERE a.customizedGroupSectionID = b.customizedGroupSectionID
								AND a.advisorGroupID = ".$this->mGroupID.")".$orderstr . $limitstr;
			}
			
            $this->mRs->Execute($sql);

			if ($this->mRs->Recordcount() != $this->mSubGroupsAndProjectsCount){
				$this->mSubGroupsAndProjects = array();
				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
					try{
						$groupData = array();
						$groupData["groupID"] = $recordset['groupID'];
						$groupData["sendableID"] = $recordset['sendableID'];
						$groupData["name"] = $recordset['name'];
						$groupData["description"] = $recordset['description'];
						$groupData["discriminator"] = $recordset['discriminator'];
						$groupData["groupaccess"] = $recordset['groupaccess'];
						$groupData["administrator"] = $recordset['administrator'];
						$groupData["parentID"] = $recordset['parentID'];
						$groupData["photoID"] = $recordset['photoID'];
						$groupData["inverserank"] = $recordset['inverserank'];
						$groupData["clientID"] = $this->mClientID;
						$groupData["featuredsubgrouprank"] = $recordset["featuredsubgrouprank"];

						$group = new AdminGroup();
						$group->setAdminGroupData($groupData);
						
						$this->mSubGroupsAndProjects[] = $group;
	                }catch(Exception $e){
                       throw $e;
                    }
	         	}
            }
			
			/*$sql2 = "SELECT COUNT(*) AS `total_records`
			 		FROM tblGroup
					WHERE parentID = ".$this->mGroupID;
			if( 1 == $mode ){//group only
				$sql2 = "SELECT COUNT(*) AS `total_records` from tblGroup WHERE parentID = '$this->mGroupID' " . 
								" AND groupID NOT IN (
									SELECT subgroupID
									FROM tblCustomizedGroupSection AS a, tblCustomizedGroupSectiontoSubGroups AS b
									WHERE a.customizedGroupSectionID = b.customizedGroupSectionID
									AND a.advisorGroupID = " . $this->mGroupID . ")";
			}else if( 2 == $mode ){//projects only
				$sql2 = "SELECT COUNT(*) AS `total_records`
				 		FROM tblGroup
						WHERE parentID = ".$this->mGroupID."
							AND groupID IN (
								SELECT subgroupID
								FROM tblCustomizedGroupSection AS a, tblCustomizedGroupSectiontoSubGroups AS b
								WHERE a.customizedGroupSectionID = b.customizedGroupSectionID
								AND a.advisorGroupID = ".$this->mGroupID.")";
			}*/
			$sql2 = "SELECT FOUND_ROWS() AS `total_records`";
			$this->mRs->Execute($sql2);
			$recordset = mysql_fetch_array($this->mRs->Resultset());
			return array($this->mSubGroupsAndProjects,$recordset["total_records"]);               
		}
		
		/**
		 * Method to get the join requests of a group
		 * Added by		:	Jul
		 * Date Added	:	2008-11-19
		 */
		function getJoinRequests($rowslimit=NULL,$filter=NULL){
			$limitstr = "";
			if (NULL != $rowslimit){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }
			$this->mRs 	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$group_criteria = " = ".$this->mGroupID;

			if( !is_null($filter) ){
				$condition = $filter->getConditions();	
    			$booleanop = $filter->getBooleanOps();
    			
    			for($i=0;$i<count($condition);$i++){
        			$attname   = $condition[$i]->getAttributeName();                    			
        			$value 	   = $condition[$i]->getValue();
        			
        			switch($condition[$i]->getOperation()){
        				
        				case FilterOp::$EQUAL:
        					$operator = " = ";
        					break;
        				case FilterOp::$NOT_EQUAL:
        					$operator = " <> ";
        					break;
        				case FilterOp::$GREATER_THAN_OR_EQUAL:
        					$operator = " >= ";
        					break;
						case FilterOp::$IN:
							$operator = " IN ";
							break;
        				default:
        					$operator = " = ";
        					break;
        			}
        			
        			$group_criteria = " " . $booleanop[$i] . " ". $attname . $operator . " $value ";
    			}
			}else if ( $this->isParent() ){
				$sb_objects = $this->getSubGroups();
				$subgroups = array($this->mGroupID);
				foreach( $sb_objects AS $sb ){
					$subgroups[] = $sb->getGroupID();
				}
				unset($sb);
				$group_criteria = " IN (". implode(',',$subgroups) .") ";
			}                    
            $sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* 
					FROM tblRequestList, tblTraveler 
					WHERE tblTraveler.travelerID = tblRequestList.travelerID 
					AND tblTraveler.active = 1 
					AND tblTraveler.isSuspended = 0
					AND tblTraveler.deactivated = 0 
					AND tblRequestList.status = 0 
					AND tblRequestList.groupID $group_criteria 
					AND tblTraveler.travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor)
					GROUP BY tblTraveler.travelerID
					$limitstr";
					
			$this->mRs->Execute($sql);
			$request_list = array();
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
				$mTraveler = new Traveler;
				$mTraveler->setTravelerData($recordset);
                $request_list[] = $mTraveler;
			}               
            
			$sql2 = "SELECT FOUND_ROWS() AS `request_count`";
			$this->mRs->Execute($sql2);
			$recordset = mysql_fetch_array($this->mRs->Resultset());
			
            return array($request_list,$recordset["request_count"]);
		}

		/**************************** START: STAFF FUNCTIONS HERE *******************************/
		
		// checks if a traveler is a staff of a group (parent or sub, doesn't matter)
		function isStaff($traveler_id){
			if($traveler_id == 0){
				return false;
			}	
			$sql = "SELECT travelerID " .
					"FROM tblGrouptoFacilitator " .
					"WHERE type = 1 " .
					"AND status = 1 " .
					"AND groupID = " . $this->mGroupID . " " .
					"AND travelerID = " . $traveler_id." ".
					"LIMIT 0,1";
			
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->mRs->Execute($sql);

			if ($this->mRs->Recordcount())
				return true;
			else
				return false;
		}
		
		// checks if traveler is a staff in at least a group in this group network
		function isStaffInNetwork($_travelerId = 0){
			if($_travelerId == 0){
				return false;
			}
			$sql = "SELECT tblGrouptoFacilitator.travelerID " .
					"FROM tblGrouptoFacilitator, tblGroup " .
					"WHERE tblGroup.groupID = tblGrouptoFacilitator.groupID " .
					"AND (tblGroup.groupID = '".$this->mGroupID."' OR tblGroup.parentID = '".$this->mGroupID."')" .
					"AND tblGrouptoFacilitator.status = 1 " .
					"AND tblGrouptoFacilitator.type = " . self::STAFF . " " .
					"AND tblGrouptoFacilitator.travelerID = '" . $_travelerId ."'".
					"LIMIT 0,1";
			
			$this->mRs	= 	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->mRs->Execute($sql);

			if (0 < $this->mRs->Recordcount())
				return true;
			else
				return false;
		}

		// checks if traveler is a staff of parent group
		function isSuperStaff($_travelerId = 0){
			if($_travelerId == 0){
				return false;
			}
			$sql = "SELECT DISTINCT tblGrouptoFacilitator.travelerID " .
					"FROM tblGrouptoFacilitator, tblGroup " .
					"WHERE tblGroup.groupID = tblGrouptoFacilitator.groupID " .
					"AND tblGroup.parentID = 0 " .
					"AND tblGrouptoFacilitator.status = 1 " .
					"AND tblGrouptoFacilitator.type = " . self::STAFF . " " .
					"AND tblGrouptoFacilitator.travelerID = " . $_travelerId . " " .
					"AND tblGrouptoFacilitator.groupID = " . $this->mGroupID." ".
					"LIMIT 0,1";
			
			$this->mRs	= 	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->mRs->Execute($sql);

			if (0 < $this->mRs->Recordcount())
				return true;
			else
				return false;
		}

		// checks if traveler is a staff but not a superstaff
		function isRegularStaff($_travelerId = 0){
			if($_travelerId == 0){
				return false;
			}
			$sql = "SELECT DISTINCT tblGrouptoFacilitator.travelerID " .
					"FROM tblGrouptoFacilitator, tblGroup " .
					"WHERE tblGrouptoFacilitator.groupID = tblGroup.groupID " .
					"AND tblGroup.parentID = " . $this->mGroupID . " " .
					"AND tblGroup.isSuspended = 0 " .
					"AND tblGrouptoFacilitator.status = 1 " .
					"AND tblGrouptoFacilitator.type = " . self::STAFF . " " .
					"AND tblGrouptoFacilitator.travelerID = " . $_travelerId . " " .
					"AND tblGrouptoFacilitator.travelerID NOT IN " .
					"(" .
						"SELECT DISTINCT travelerID " .
						"FROM tblGrouptoFacilitator " .
						"WHERE groupID = " . $this->mGroupID . " " .
						"AND status = 1 " .
						"AND type = " . self::STAFF .
					")".
					" LIMIT 0,1";
			$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
			$this->mRs->Execute($sql);

			if (0 < $this->mRs->Recordcount())
				return true;
			else
				return false;
		}		

		// add as group staff
		function addStaff($_travelerID){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			// if this traveler has a previous record of being a group staff; activate
			if ($this->isInActiveStaff($_travelerID)){
				$sql = "UPDATE tblGrouptoFacilitator " .
						"SET status = 1 " .
						"WHERE groupID = " . $this->mGroupID . " " .
						"AND travelerID = " . $_travelerID . " " .
						"AND type = " . self::STAFF;
				$this->mRs->Execute($sql);
			}
			elseif (!$this->isStaff($_travelerID)){
				$sql = "INSERT into tblSendable (sendabletype) " .
                        "VALUES ('3')";
                $this->mRs->Execute($sql);

                $_sendableID = $this->mRs->GetCurrentID();

				$sql = "INSERT INTO tblGrouptoFacilitator (groupID, travelerID, sendableID, status, type) " .
						"VALUES ('$this->mGroupID','$_travelerID', '$_sendableID' , 1, " . self::STAFF . ")";
				$this->mRs->Execute($sql);
			}
			
			if( 0 < $this->getParentID() ){
				$this->invalidateCacheKeys($this->getParentID());
			}
			$this->invalidateCacheKeys($this->getGroupID());
		}

		// removes a traveler as a staff of a group
		function removeStaff($_travelerID){			
			if ($this->isStaff($_travelerID)){
				$sql = "UPDATE tblGrouptoFacilitator " .
						"SET status = 0 " .
						"WHERE groupID = " . $this->mGroupID . " " .
						"AND travelerID = " . $_travelerID . " " .
						"AND type = " . self::STAFF;				
				$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
				$this->mRs->Execute($sql);
				
				if( 0 < $this->getParentID() ){
					$this->invalidateCacheKeys($this->getParentID());
				}
				$this->invalidateCacheKeys($this->getGroupID());
			}
		}

		// checks if traveler has a pending staff invitation to group
		function isPendingStaffToGroup($_travelerID = 0){
			$sql = "SELECT travelerID " .
					"FROM tblInviteList " .
					"WHERE status = 2 " .
					"AND groupID = " . $this->mGroupID . " " .
					"AND travelerID = " . $_travelerID." ".
					"LIMIT 0,1";
			$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
			$this->mRs->Execute($sql);

			if (0 < $this->mRs->Recordcount())
				return true;
			else
				return false;
		}

		// invites traveler to subgroup and setts its status as pending staff
		function addAsPendingStaff($_traveler = null){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			if ($_traveler){
				$_travelerId = $_traveler->getTravelerID();

		       	if (!$this->isInInviteList($_traveler)){
					$sql = "INSERT INTO tblInviteList (groupID, travelerID, status) " .
							"VALUES (" . $this->mGroupID . ", " . $_travelerId . ", 2)";
		        	$this->mRs->Execute($sql);
				}
				else{
					$now  = date("Y-m-d H:i:s");
		        	$sql = "UPDATE tblInviteList SET status = 2, inviteDate = '" . $now . "' " .
							"WHERE groupID = " . $this->mGroupID . " AND travelerID = " . $_travelerId;
		        	$this->mRs->Execute($sql);
		        }
			}
		}

		// cancels invitation to traveler as staff and member of subgroup
		function cancelAsPendingStaff($_travelerId = 0){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "DELETE FROM tblInviteList WHERE groupID = " . $this->mGroupID . " " .
					"AND travelerID = " . $_travelerId . " AND status = 2";
			$this->mRs->Execute($sql);
		}

		// gets all subgroups with assigned staff
		function getStaffedSubGroups($_regularStaffOnly = true){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT DISTINCT tblGrouptoFacilitator.groupID, tblGroup.name, tblGroup.parentID " .
					"FROM tblGrouptoFacilitator, tblGroup, tblTraveler " .
					"WHERE tblGrouptoFacilitator.groupID = tblGroup.groupID " .
					"AND tblTraveler.travelerID = tblGrouptoFacilitator.travelerID " .
					"AND tblGrouptoFacilitator.type = 1 " .
					"AND tblGrouptoFacilitator.status = 1 " .
					"AND tblTraveler.active = 1 " .
					"AND tblTraveler.isSuspended = 0 " .
					"AND tblTraveler.deactivated = 0 " .
					"AND tblGroup.isSuspended = 0 " .
					"AND tblGroup.parentID = " . $this->mGroupID;
			if ( $_regularStaffOnly )
			{
				$sql .= " AND NOT EXISTS " .
						"(" .
							"SELECT groupID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE type = 1 " .
							"AND status = 1 " .
							"AND travelerID = tblTraveler.travelerID " .
							"AND groupID = " . $this->mGroupID .
						") ";
			}
					
			$sql .= " ORDER BY tblGroup.name ASC";
			$this->mRs->Execute($sql);

			$arrGroupId = array();
			$arrSubGroups = array();

			if (0 < $this->mRs->Recordcount()){
				while($Group = mysql_fetch_assoc($this->mRs->Resultset())){
					$mData = array();
					$mData['groupID']	= $Group['groupID'];
					$mData['name']		= $Group['name'];
					$mData['parentID']	= $Group['parentID'];
					$mData['parentID']	= $Group['parentID'];

					$mGroup = new AdminGroup;
					$mGroup->setAdminGroupData($mData);
					$arrSubGroups[] = $mGroup;
				}
			}	
			return $arrSubGroups;
		}

		/**
		 * Sets the staff of the parent group as staff and members of this subgroup.
		 * 
		 * @throws exception
		 * @return void
		 */
		function addSuperStaffAsSubGroupStaffAndMembers(){
			if (!$this->isSubGroup())
			  return;
			
			try {
				$handler = new dbHandler();
				
				$sql = "SELECT tblGrouptoFacilitator.travelerID " .
					"FROM tblGrouptoFacilitator, tblTraveler " .
					"WHERE tblGrouptoFacilitator.travelerID = tblTraveler.travelerID " .
					"AND tblTraveler.active = 1 " .
					"AND tblTraveler.isSuspended = 0 " .
					"AND tblTraveler.deactivated = 0 " .
					"AND tblGrouptoFacilitator.status = 1 " .
					"AND tblGrouptoFacilitator.groupID = ".$handler->makeSqlSafeString($this->mParentID).
					"AND type = " . self::STAFF;

				$result = $handler->execute($sql);
				$rs = new iRecordset($result);

				while (!$rs->EOF()) {
		    	$data = $rs->current();
					$this->addStaff($data['travelerID']);
					$this->addMember(new Traveler($data['travelerID']));
					$rs->next();	
				}
			}
			catch(exception $ex){
				throw $ex;
			}
		}

		// gets all the staff of a group
		function getStaff($_list = false, $_subgrouponly = false, $_prioritizeStaffWithPhotos=false)
		{
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$this->mArrStaff = array();

			if( $_prioritizeStaffWithPhotos )
			{
				$sql = "SELECT tblGrouptoFacilitator.travelerID, tblTravelertoPhoto.photoID IS NULL AS photoID " .
						"FROM tblGrouptoFacilitator, tblTraveler " .
						"LEFT JOIN tblTravelertoPhoto ".
						"ON tblTravelertoPhoto.travelerID = tblTraveler.travelerID ".
						"WHERE tblGrouptoFacilitator.travelerID = tblTraveler.travelerID " .
						"AND tblTraveler.active = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 " .
						"AND tblGrouptoFacilitator.status = 1 " .
						"AND tblGrouptoFacilitator.groupID = " . $this->mGroupID . " " .
						"AND type = " . self::STAFF;
			}
			else
			{
				$sql = "SELECT tblGrouptoFacilitator.travelerID " .
						"FROM tblGrouptoFacilitator, tblTraveler " .
						"WHERE tblGrouptoFacilitator.travelerID = tblTraveler.travelerID " .
						"AND tblTraveler.active = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 " .
						"AND tblGrouptoFacilitator.status = 1 " .
						"AND tblGrouptoFacilitator.groupID = " . $this->mGroupID . " " .
						"AND type = " . self::STAFF;
			}
			if (true == $_subgrouponly)
			{
				$sql .= " AND NOT EXISTS " .
						"(" .
							"SELECT groupID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE type = 1 " .
							"AND status = 1 " .
							"AND travelerID = tblTraveler.travelerID " .
							"AND groupID = " . $this->getParent()->getGroupID() .
						")";
			}
			if( $_prioritizeStaffWithPhotos )
			{
				$sql .= " GROUP BY tblTraveler.travelerID ORDER BY photoID ASC, tblTraveler.latestlogin DESC";
			}
			$this->mRs->Execute($sql);

			while ($recordset = mysql_fetch_array($this->mRs->Resultset()))
			{		
				if ($_list)
					$this->mArrStaff[$recordset['travelerID']] = $recordset['travelerID'];
				else
				{
					require_once("travellog/model/Class.Staff.php");
					$this->mArrStaff[] = new Staff($recordset['travelerID'], $this->mGroupID);
				}					
			}
			return $this->mArrStaff;
		}

		// checks if traveler is an inactive staff of this group
		function isInActiveStaff($_travelerID = 0){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT travelerID " .
					"FROM tblGrouptoFacilitator " .
					"WHERE type = 1 " .
					"AND status = 0 " .
					"AND groupID = " . $this->mGroupID . " " .
					"AND travelerID = " . $_travelerID." ".
					"LIMIT 0,1";
			$this->mRs->Execute($sql);

			if ($this->mRs->Recordcount())
				return true;
			else
				return false;
		}

		// gets the staff of subgroups of an admin group
		function getStaffofSubGroups($_list = false, $_vars = array(), $prioritizeStaffWithPhotos=FALSE)
		{
			if( $prioritizeStaffWithPhotos )
			{
				$sql = "SELECT tblTraveler.*, tblTravelertoPhoto.photoID IS NULL AS photoID
						FROM tblGroup, tblGrouptoFacilitator, tblTraveler
						LEFT JOIN tblTravelertoPhoto
						ON tblTravelertoPhoto.travelerID = tblTraveler.travelerID
						WHERE tblGroup.groupID = tblGrouptoFacilitator.groupID
						AND tblGrouptoFacilitator.travelerID = tblTraveler.travelerID  
						AND tblTraveler.active = 1 
						AND tblTraveler.isSuspended = 0 
						AND tblTraveler.deactivated = 0
						AND 
							(tblGroup.parentID = '$this->mGroupID' || tblGroup.groupID = '$this->mGroupID')
						AND tblGrouptoFacilitator.status = 1 
						AND tblGrouptoFacilitator.type = " . self::STAFF . " GROUP BY travelerID ORDER BY photoID ASC, latestlogin DESC";
			}
			else
			{
				$sql = "SELECT tblTraveler.*
						FROM tblGroup, tblGrouptoFacilitator, tblTraveler
						WHERE tblGroup.groupID = tblGrouptoFacilitator.groupID
						AND tblGrouptoFacilitator.travelerID = tblTraveler.travelerID  
						AND tblTraveler.active = 1 
						AND tblTraveler.isSuspended = 0 
						AND tblTraveler.deactivated = 0
						AND 
							(tblGroup.parentID = '$this->mGroupID' || tblGroup.groupID = '$this->mGroupID')
						AND tblGrouptoFacilitator.status = 1 
						AND tblGrouptoFacilitator.type = " . self::STAFF . " GROUP BY travelerID ";

				if(array_key_exists('orderByName', $_vars))
					$sql .= " ORDER BY " . $_vars['orderByName'];
			
				if(array_key_exists('limit', $_vars) && is_numeric($_vars['limit']))
					$sql .= " LIMIT 0, " . $_vars['limit'];
			}
			
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->mRs->Execute($sql);

			$grpStaff = array();
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())){		
				if ($_list)
					$grpStaff[$recordset['travelerID']] = $recordset['travelerID'];
				else{
					$travelerData = array();
					$travelerData["travelerID"] = $recordset['travelerID'];
					$travelerData["active"] = $recordset['active'];
					$travelerData["isSuspended"] = $recordset['isSuspended'];
		 			$travelerData["currlocationID"] = $recordset['currlocationID'];
		 			$travelerData["sendableID"] = $recordset['sendableID'];
		 			$travelerData["username"] = $recordset['username'];
		 			$travelerData["password"] = $recordset['password'];
		 			$travelerData["showsteps"] = $recordset['showsteps'];
					$travelerData["deactivated"] = $recordset['deactivated'];
													
					$traveler = new Traveler;
					$traveler-> setTravelerData($travelerData);
					
					$grpStaff[] = $traveler;
				}					
			}

			return $grpStaff;
		}
		
		// gets the count of the staff of subgroups of an admin group
		function getCountStaffofSubGroups(){
			$sql = "SELECT count(DISTINCT tblTraveler.travelerID) 
					FROM tblGroup, tblGrouptoFacilitator, tblTraveler
					WHERE tblGroup.groupID = tblGrouptoFacilitator.groupID
					AND tblGrouptoFacilitator.travelerID = tblTraveler.travelerID  
					AND tblTraveler.active = 1 
					AND tblTraveler.isSuspended = 0 
					AND tblTraveler.deactivated = 0 
					AND 
						(tblGroup.parentID = '$this->mGroupID' || tblGroup.groupID = '$this->mGroupID')
					AND tblGrouptoFacilitator.status = 1 
					AND tblGrouptoFacilitator.type = " . self::STAFF ;			
			
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->mRs->Execute($sql);
			
			$recordset = mysql_fetch_array($this->mRs->Resultset());
			return $recordset['count(DISTINCT tblTraveler.travelerID)'];
		}
		
		function hasStaff(){
			$groupID = $this->mGroupID;
			$type = self::STAFF;
			
			if( 0 < $this->getParentID() ){	
				$sql = "SELECT tblGrouptoFacilitator.travelerID 
							FROM tblGrouptoFacilitator, tblTraveler 
							WHERE tblGrouptoFacilitator.travelerID = tblTraveler.travelerID 
							AND tblTraveler.active = 1 
							AND tblTraveler.isSuspended = 0 
							AND tblTraveler.deactivated = 0
							AND tblGrouptoFacilitator.status = 1 
							AND tblGrouptoFacilitator.groupID = $groupID
							AND tblGrouptoFacilitator.type = $type
						UNION DISTINCT
						SELECT tblGrouptoFacilitator.travelerID
							FROM tblGroup, tblGrouptoFacilitator, tblTraveler
							WHERE tblGroup.groupID = tblGrouptoFacilitator.groupID
							AND tblGrouptoFacilitator.travelerID = tblTraveler.travelerID  
							AND tblTraveler.active = 1 
							AND tblTraveler.isSuspended = 0 
							AND tblTraveler.deactivated = 0 
							AND (tblGroup.parentID = $groupID || tblGroup.groupID = $groupID)
							AND tblGrouptoFacilitator.status = 1 
							AND tblGrouptoFacilitator.type = $type";
			}else{
				$sql = "SELECT tblGrouptoFacilitator.travelerID 
						FROM tblGrouptoFacilitator, tblTraveler 
						WHERE tblGrouptoFacilitator.travelerID = tblTraveler.travelerID 
						AND tblTraveler.active = 1 
						AND tblTraveler.isSuspended = 0 
						AND tblTraveler.deactivated = 0
						AND tblGrouptoFacilitator.status = 1 
						AND tblGrouptoFacilitator.groupID = $groupID
						AND type = $type
						LIMIT 0,1";
			}
			
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$this->mRs->Execute($sql);
			
			return 0 < $this->mRs->Recordcount();
		}

		// gets all the staff of a group regardless if superstaff or not, unless stated
		function getAllGroupStaff($_rowsLimit = null, $_subgrouponly = false){
			$mRs2   = new Recordset(ConnectionProvider::instance()->getConnection());
			$mLimitStr = "";

			if ($_rowsLimit){
                $mRows		= $_rowsLimit->getRows();
                $mOffset	= ($_rowsLimit->getOffset() * $mRows);
                $mLimitStr	= " LIMIT " . $mOffset . " , " . $mRows;
            }

			// get all staff
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.travelerID, tblTraveler.username, tblTraveler.firstname, tblTraveler.lastname " .
					"FROM tblGrouptoFacilitator, tblTraveler " .
					"WHERE tblTraveler.travelerID = tblGrouptoFacilitator.travelerID " .
					"AND tblTraveler.active = 1 " .
					"AND tblTraveler.isSuspended = 0 " .
					"AND tblTraveler.deactivated = 0 " .
					"AND tblGrouptoFacilitator.type = 1 " .
					"AND tblGrouptoFacilitator.status = 1 " .
					"AND tblGrouptoFacilitator.groupID = " . $this->mGroupID;
			if ($_subgrouponly){
				$sql .= " AND NOT EXISTS " .
						"(" .
							"SELECT groupID " .
							"FROM tblGrouptoFacilitator " .
							"WHERE type = 1 " .
							"AND status = 1 " .
							"AND travelerID = tblTraveler.travelerID " .
							"AND groupID = " . $this->getParent()->getGroupID() .
						") ";
			}
			$sql .= " GROUP BY travelerID " .
					" ORDER BY username ASC " . $mLimitStr;
			$this->mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() as totalRecords";
			$mRs2-> Execute($sql2);

			$arrStaff = array();
			$this->mTotalRecords = $mRs2->Result(0, "totalRecords");

			while ($Staff= mysql_fetch_assoc($this->mRs->Resultset())){
				$mTraveler = new Traveler;
				$mTraveler->setTravelerData($Staff);
				$arrStaff[] = $mTraveler;
			}

			return $arrStaff;
		}

		// gets all superstaff and regular staff
		function getAllStaff($_rowsLimit = null){
			$this->mRs = new Recordset(ConnectionProvider::instance()->getConnection());
			$mRs2   = new Recordset(ConnectionProvider::instance()->getConnection());
			$mLimitstr = "";

			if ($_rowsLimit){
                $mRows		= $_rowsLimit->getRows();
                $mOffset	= ($_rowsLimit->getOffset() * $mRows);
                $mLimitstr	= " LIMIT " . $mOffset . " , " . $mRows;
            }

			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " .
					"(" .
						// get all regular staff
						"SELECT tblTraveler.travelerID, tblTraveler.username, tblTraveler.firstname, tblTraveler.lastname " .
						"FROM tblGrouptoFacilitator, tblGroup, tblTraveler " .
						"WHERE tblGrouptoFacilitator.groupID = tblGroup.groupID " .
						"AND tblTraveler.travelerID = tblGrouptoFacilitator.travelerID " .
						"AND tblGrouptoFacilitator.type = 1 " .
						"AND tblGrouptoFacilitator.status = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.active = 1 " .
						"AND tblTraveler.deactivated = 0 " .
						"AND tblGroup.isSuspended = 0 " .
						"AND tblGroup.parentID = " . $this->mGroupID .
						" UNION " .
						// get all super staff
						"SELECT tblTraveler.travelerID, tblTraveler.username, tblTraveler.firstname, tblTraveler.lastname " .
						"FROM tblGrouptoFacilitator, tblTraveler " .
						"WHERE tblTraveler.travelerID = tblGrouptoFacilitator.travelerID " .
						"AND tblTraveler.active = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 " .
						"AND tblGrouptoFacilitator.type = 1 " .
						"AND tblGrouptoFacilitator.status = 1 " .
						"AND tblGrouptoFacilitator.groupID = " . $this->mGroupID .
					") AS tblTemp " .
					"GROUP BY travelerID " .
					"ORDER BY username ASC " . $mLimitstr;
			$this->mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() as totalRecords";
			$mRs2-> Execute($sql2);

			$arrStaff = array();
			$this->mTotalRecords = $mRs2->Result(0, "totalRecords");

			while($Staff = mysql_fetch_assoc($this->mRs->Resultset())){
				$mTraveler = new Traveler;
				$mTraveler->setTravelerData($Staff);
				$arrStaff[] = $mTraveler;
			}

			return $arrStaff;
		}
		/***************************** END: STAFF FUNCTIONS HERE ********************************/
		
		/**
		 * gets video albums of this group
		 * @author Nherrisa Mae Celeste
		 * @return VideoAlbum[]
		 */
		
		function getVideoAlbums() {
			$cache 	= ganetCacheProvider::instance()->getCache();
			$key 	= 'Group_VideoAlbums_'.$this->mGroupID;
			
			if ($cache != null && $videoAlbums = $cache->get($key)) {
				if ($videoAlbums[0] == 'EMPTY')
					$videoAlbums = array();
			}
			
			$videoAlbums = array();
			
			$sql = "SELECT * FROM tblVideoAlbum WHERE creatorID = $this->mGroupID AND creatorType = 'group' ORDER BY albumID ASC";
	
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			$mRs->Execute($sql);
			
			if (0 < $mRs->Recordcount()) {
				while ($recordset = mysql_fetch_array($mRs->Resultset())) {
	                try {
	                    $videoAlbum = new VideoAlbum();
	                    $videoAlbum->setAlbumID($recordset['albumID']);
	                    $videoAlbum->setTitle($recordset['title']);
	 					$videoAlbum->setCreatorID($recordset['creatorID']);
	 					$videoAlbum->setCreatorType($recordset['creatorType']);
	 					$videoAlbum->setDateCreated($recordset['dateCreated']);
	 					$videoAlbum->setLastDateUpdated($recordset['lastDateUpdated']);
	                }
	                catch (Exception $e) {
	                   throw $e;
	                }
	                $videoAlbums[] = $videoAlbum;
	            }
			}
			
            if ($cache != null){
				if (!count($videoAlbums)) {
					// a blank array put to cache causes null when retreived back
					$cache->set($key, array('EMPTY'), array('EXPIRE'=>3600) );	
				}else {
					$cache->set($key, $videoAlbums, array('EXPIRE'=>3600) );
				}
			}
			
			return $videoAlbums;
		}
		
		// added by neri: 06-10-09
		// gets the video(s) of a group
		
		function getGroupVideos() {
			$cache 	= ganetCacheProvider::instance()->getCache();
			$key 	= 'Group_Videos_'.$this->getGroupID();
			
			if ($cache != null && $videos = $cache->get($key)) {
				if ($videos[0] == 'EMPTY')
					$videos = array();
				return $videos;
			}
			
			$grpFacilitators = '';
			$videos = array();
			
			foreach ($this->getAllStaff() as $staff)
				$grpFacilitators .= $staff->getTravelerID().',';
				
			$grpFacilitators .= $this->getAdministratorID();
			
			$sql = 'SELECT videoID, dateAdded, albumID ' .
				   'FROM (' .
				   		'SELECT tblVideo.videoID, tblVideo.dateAdded, tblVideo.albumID ' .
				   		'FROM tblTravel, tblTravelLog, tblTravelLogtoVideo, tblVideo, tblGroupApprovedJournals, tblTravelLink, qryGroupTravel ' .
				   		'WHERE qryGroupTravel.travelID = tblGroupApprovedJournals.travelID ' .
				   		'AND tblGroupApprovedJournals.travelID = tblTravel.travelID ' .
				   		'AND tblTravel.deleted = 0 ' .
				   		'AND tblTravel.publish = 1 ' .
				   		'AND tblTravel.travelID = tblTravelLog.travelID ' .
				   		'AND qryGroupTravel.privacypref IN (0,1,2,3,4) ' .
				   		'AND tblGroupApprovedJournals.approved = 1 ' .
				   		'AND tblGroupApprovedJournals.groupID = ' .$this->getGroupID().' '.
				   		'AND tblTravelLog.deleted = 0 ' .
				   		'AND tblTravelLog.publish = 1 ' .
				   		'AND tblTravel.travelLinkID = tblTravelLink.travelLinkID ' .
				   		'AND tblTravelLink.refID = ' .$this->getGroupID().' '.
				   		'AND tblTravelLink.refType = 2 ' .
				   		'AND tblTravelLog.travellogID = tblTravelLogtoVideo.travellogID ' .
				   		'AND tblTravelLogtoVideo.videoID = tblVideo.videoID ' .
				   		'AND tblTravel.travelerID IN ('.$grpFacilitators.') ' .
				   		'UNION ' .
				   		'SELECT tblVideo.videoID, tblVideo.dateAdded, tblVideoAlbum.albumID ' .
				   		'FROM tblVideoAlbum, tblVideo ' .
				   		'WHERE tblVideoAlbum.albumID > 0 ' .
				   		'AND tblVideoAlbum.albumID = tblVideo.albumID ' .
				   		'AND tblVideoAlbum.creatorID = ' .$this->getGroupID().' ' .
				   		'AND tblVideoAlbum.creatorType = \'group\' ' .
				   ') AS videoTable ' .
				   'ORDER BY dateAdded DESC ';
			
			$conn = ConnectionProvider::instance()->getConnection();
			$mRs = new Recordset($conn);
			$mRs->Execute($sql);
			
			if (0 < $mRs->Recordcount()) {
				while($row = mysql_fetch_assoc($mRs->Resultset())){
					try {
						if (0 < $row['albumID']) 
							$video = new VideoAlbumToVideo($row['videoID']);
						else 
							$video = new TravelLogToVideo($row['videoID']);
					}
					
					catch(Exception $e) {}
					
					$videos[] = $video;
				}
			}
			
			if ($cache != null){
				if (!count($videos)) {
					// a blank array put to cache causes null when retreived back
					$cache->set($key, array('EMPTY'), array('EXPIRE'=>3600) );	
				}else {
					$cache->set($key, $videos, array('EXPIRE'=>3600) );
				}
			}	
			
			return $videos;
		}
		
		//added by Jul for custom page header
		function getJournalsWithQualifiedPhotos($order=NULL,$filter=NULL,$travel_array=array(),$countOnly=FALSE){
			$this->mRs->setConnection(ConnectionProvider::instance()->getConnection());
			$orderstr = "ORDER BY tblTravel.title ASC";
			if (NULL != $order){
            	$condition = $order->getConditions();	        			
    			
				foreach ($condition as $eachcondition) {
					$attname   = $eachcondition->getAttributeName();                   			
    				        			
        			switch($eachcondition->getOperation()){	        				
        				case FilterOp::$ORDER_BY:
        					$sortorder = " ASC ";
        					break;
        				case FilterOp::$ORDER_BY_DESC:
        					$sortorder = " DESC ";
        					break;
        				default:
        					$sortorder = " ASC ";
        					break;
        			}

					$arrOrderBy[] = $attname . $sortorder;
    			}
    			$orderstr = " ORDER BY " . implode(" , ", $arrOrderBy);
    			                		
            }

			$filterstr = "";
			if (NULL != $filter){
            	$condition = $filter->getConditions();	
    			$attname   = $condition[0]->getAttributeName();                    			
    			$value 	   = $condition[0]->getValue();
    			
    			switch($condition[0]->getOperation()){
    				
    				case FilterOp::$EQUAL:
    					$operator	= " = ";
    					break;
    				case FilterOp::$NOT_EQUAL:
    					$operator = " <> ";
    					break;
					case FilterOp::$LIKE:
						$operator = " LIKE ";
						break;
					case FilterOp::$GREATER_THAN_OR_EQUAL:
						$operator = " >= ";
						break;
					case FilterOp::$LESS_THAN_OR_EQUAL:
						$operator = " <= ";
						break;
    				default:
    					$operator = " = ";
    					break;
    			}
    			
    			$filterstr = " AND " . $attname . $operator . $value;	
            }

			if( 0 < count($travel_array) ){
				$filterstr .= "AND tblTravel.TravelID IN (".implode(",",$travel_array).")";
			}
			
			$groupID = "'".$this->getGroupID()."'";
			$sql = "SELECT tblTravel.*, 
						COUNT(tblTravelLogtoPhoto.photoID) AS photoCount
					FROM tblGroupApprovedJournals,
						tblTravel,
						tblTravelLog,
						tblTravelLogtoPhoto
					WHERE tblGroupApprovedJournals.groupID = $groupID
						AND tblGroupApprovedJournals.travelID = tblTravel.travelID
						$filterstr
						AND tblTravel.travelID = tblTravelLog.travelID
						AND tblTravelLog.deleted = 0
						AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID
						AND tblTravelLogtoPhoto.qualifiedCustomHeader = 1
					GROUP BY travelID
					$orderstr";
			$this->mRs->Execute($sql);
			
			$travels = array();
			if( $countOnly ){
				return $this->mRs->Recordcount();
			}
			if ( 0 < $this->mRs->Recordcount()) {
				 while ($data = mysql_fetch_array($this->mRs->Resultset())) {
                    try{
                        $travel = new Travel(0,$data);
						if( 0 < $data["photoCount"] ){
							$travel->setQualifiedPhotosCount($data["photoCount"]);
							$travels[] = $travel;
						}
                    }catch (exception $e){}
                }
			}
			return $travels;
		}
		
		/**
		 * Returns the number of journal entries related to this subgroup.
		 * 
		 * @return integer
		 * @author Augustianne Laurenne L. Barreta
		 */
		function getJournalEntriesCount(){
			try {
				$handler = new dbHandler();
				
				$sql = "SELECT count(distinct(tblTravelLog.travellogID)) as count
						FROM qryGroupTravel, tblTravelLog, tblTravel, tblGroupApprovedJournals, tblTraveler
						WHERE tblTravelLog.deleted = 0 
							AND tblTravelLog.travelID = tblTravel.travelID
							AND tblTravel.deleted = 0 
							AND tblTravel.travelerID = tblTraveler.travelerID
							AND tblTraveler.isSuspended = 0							
							AND tblTraveler.deactivated = 0
							AND qryGroupTravel.travelID = tblGroupApprovedJournals.travelID
							AND tblGroupApprovedJournals.travelID = tblTravel.travelID
							AND tblGroupApprovedJournals.groupID = {$handler->makeSqlSafeString($this->mGroupID)}
							AND tblGroupApprovedJournals.approved = 1";
						
				$resource = $handler->execute($sql);
				
				$record = mysql_fetch_assoc($resource);
				
				return $record['count'];
			}
			catch(exception $ex){
				throw $ex;
			}
		}
		
		function getArticlesCount(){
			require_once("travellog/model/Class.Article.php");
			return Article::getArticleCount($this->mGroupID);
		}
		
		//added by Jul, function to check if a traveler is an advisor.
		//if groupID is supplied, checks if the traveler is the advisor/administrator of the particular group
		public static function isGroupAdvisor($travelerID=0,$groupID=NULL){
			$conn = ConnectionProvider::instance()->getConnection();
			$rs = new Recordset($conn);
			
			if( is_null($groupID) ){
				$sql = "SELECT groupID
						FROM tblGroup
						WHERE administrator = '$travelerID'
							AND discriminator > 1
						LIMIT 0,1";
			}else{
				$sql = "SELECT groupID
						FROM tblGroup
						WHERE groupID = '$groupID'
							AND administrator = '$travelerID'
							AND discriminator > 1
						LIMIT 0,1";
			}
			
			$rs->Execute($sql);
			return 0 < $rs->RecordCount();
		}
		
		public function directSignupAllowed(){
			$groupID = $this->getGroupID();
			
			$conn = ConnectionProvider::instance()->getConnection();
			$rs = new Recordset($conn);
			
			$sql = "SELECT directSignup
					FROM tblCustomPageHeader
					WHERE groupID = $groupID
					LIMIT 0,1";
			
			$rs->Execute($sql);
			
			if( !$rs->Recordcount() ){
				return FALSE;
			}
			
			$data = mysql_fetch_array($rs->Resultset());
			
			if( 1 == $data["directSignup"] ){
				return TRUE;
			}
			return FALSE;
		}
		
		public function removeGroupRelatedJournalsOfTraveler($travelerID){
			require_once("travellog/dao/Class.GroupApprovedJournals.php");
			GroupApprovedJournals::removeGroupRelatedJournalsOfTraveler($travelerID,$this->getGroupID());
			require_once('Cache/ganetCacheProvider.php');
			$cache = ganetCacheProvider::instance()->getCache();
			if($cache != null){
				try{				
					$siteID = (0 < $this->getParentID()) ? $this->getParentID() : $this->getGroupID();
					// traveler profile - COBRAND
					$cache->delete('Journal_Profile_Context_' . $siteID . '_' . $travelerID);

					// MAIN
					$cache->delete('Journal_Group_Context_PUBLIC_' . $this->getGroupID() . '_0/group.phpgID=' .$this->getGroupID()); 
					// COBRANDS
					$cache->delete('Journal_Group_Context_PUBLIC_' . $this->getGroupID() . '_' . $siteID . '/group.phpgID=' .$this->getGroupID()); 
					$cache->delete('Journal_Group_Context_PUBLIC_' . $this->getGroupID() . '_' . $siteID . '/group.php'); 
					$cache->delete('Journal_Group_Context_PUBLIC_' . $this->getGroupID() . '_' . $siteID . '/index.php'); 
										
					// main journals tab - COBRAND approved, public view
					$cache->delete('Journal_URL_' . $siteID . '/journal.php_0');
					// journals tab - COBRAND approved including subgroups, public view
					$cache->delete('Journal_URL_' . $siteID . '/journal.phpaction=groupJournals&gID=' . $this->getGroupID() . '_0');
					$cache->delete('Journal_URL_' . $siteID . '/journal.phpaction=groupjournals&gID=' . $this->getGroupID() . '_0');
					// journals tab - MAIN including subgroups, public view
					$cache->delete('Journal_URL_0/journal.phpaction=groupJournals&gID=' . $this->getGroupID() . '_0');
				}catch(exception $e){}
			}
		}
		
		/*
		* functions for subgroup featured in main group's home page
		*/
		function setGroupHomeFeatured($featured){
			$this->mGroupHomeFeatured = $featured;
		}
		function setGroupHomeFeaturedRank($rank){
			$this->mGroupHomeFeaturedRank = $rank;
		}
		
		function getGroupHomeFeatured(){
			return $this->mGroupHomeFeatured;
		}
		function getGroupHomeFeaturedRank(){
			return $this->mGroupHomeFeaturedRank;
		}
		
		function isGroupHomeFeatured(){
			return 1 == $this->mGroupHomeFeatured;
		}
		
		function UpdateGroupHomeFeatured(){
			try {
				$handler = new dbHandler();
				$groupID = $this->getGroupID();
				$featured = $this->getGroupHomeFeatured();
				$sql = "UPDATE tblGroup SET groupHomeFeatured = {$featured} WHERE groupID = {$groupID}";
				$handler->execute($sql);
				$this->invalidateCacheKeys($groupID);
			}catch(exception $ex){
			}
		}
    }    
?>