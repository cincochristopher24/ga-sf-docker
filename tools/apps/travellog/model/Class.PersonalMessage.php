<?php
	/**
	 * Created on September 7, 2006
	 * Class.PersonalMessage.php extends Class.Message.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'travellog/model/Class.Message.php';
	require_once 'travellog/model/Class.Traveler.php';

	class PersonalMessage extends Message{
		
		/**
		 * Constructor of class PersonalMessage
		 * @return Message|exception phpDocumentor Message object or exception
		 */
		
	 	function PersonalMessage($_attributeId = 0, $_sendableId = 0){

			$this->setSendableId($_sendableId);
			$this->setDiscriminator(DiscriminatorType::$PERSONAL);

	 		return $this->Message($_attributeId);
	 	}
	 	
	 	/**
	 	 * Function name: Send
	 	 */

	 	function Send(){
			$Source = $this->getSource();
			$Source->sendMessage($this);
	 	}

	 	/**
	 	 * Function name: Save
	 	 */

	 	function Save(){
			$Source = $this->getSource();
			$Source->saveAsDraft($this);
	 	}	 	

	 	/***************************** START: SETTERS *************************************/
	 	function setParentId($_parentId = 0){
	 		$this->mParentId = $_parentId;
	 	}
		function setListingId($_listingId = 0){
			$this->mListingId = $_listingId;
		}	 
		function setSource($_src = NULL){
			$this->mSource = $_src;
		}		 		 
		function setSendableId($_sendableId = NULL){
			$this->mSendableId = $_sendableId;
		}		 
		function setDestination($_dest = array()){
			$this->mDestination = $_dest;
		}
		function setMessageRecipient(){
			$arrDestination = array();

			if (0 < $this->mSendableId){

				// get recipients of message
				if ($this->getMessageTypeId() == MessageType::$SENT || $this->getMessageTypeId() == MessageType::$DRAFTS){
					$sql3 = "SELECT tblMessageToAttribute.recipientID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessageToAttribute.messageType = " . $this->getMessageTypeId() . " " .
							"AND tblMessages.discriminator = " . $this->getDiscriminator() . " " .
							"AND tblMessageToAttribute.messageID = " . $this->mMessageId;
				}
				else{
					$sql3 = "SELECT tblMessageToAttribute.recipientID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessageToAttribute.messageType = " . $this->getMessageTypeId() . " " .
							"AND tblMessages.discriminator = " . $this->getDiscriminator() . " " .
							"AND tblMessageToAttribute.attributeID = " . $this->getAttributeId();
				}
				$this->mRs3->Execute($sql3);

				if (0 < $this->mRs3->Recordcount()){
					$mFactory = MessageSendableFactory::Instance();
					while($dest = mysql_fetch_array($this->mRs3->Resultset())){
						try{
							$mDestination = $mFactory->Create($dest['recipientID']);
							if ($mDestination)
								$arrDestination[] = $mDestination;
						}
						catch (exception $e){
							throw $e;
						}
					}
				}
			}

			$this->mDestination = $arrDestination;
		}
		/******************************* END: SETTERS *************************************/

		public static function DeleteAllGroupPM($_sendableId = 0){
			// check if sendableID is of group
			$mFactory = MessageSendableFactory::Instance();
			$mSendable = $mFactory->Create($_sendableId);

			if ($mSendable instanceof Group){
				// If it's an Admin Group, check if it's a SG. For now, only subgroups can be deleted, therefore only PMs of SG will be removed
				// Clubs however can also be deleted, including all of its PMs

				if (($mSendable->getDiscriminator() == Group::ADMIN && $mSendable->isSubGroup()) || $mSendable->getDiscriminator() == Group::FUN){
					try{
						$mConn = new Connection();
						$mRs	= new Recordset($mConn);
						$mRs2	= new Recordset($mConn);
					}
					catch(exception $e){
						throw $e;
					}
					$arrMessageId = array();

					// get messages where sender is the group to be deleted
					$sql = "SELECT DISTINCT messageID FROM tblMessages WHERE discriminator = 3 AND senderID = " . $_sendableId;
					$mRs->Execute($sql);

					if (0 < $mRs->Recordcount()){
						while($Message = mysql_fetch_assoc($mRs->Resultset()))
							$arrMessageId[] = $Message['messageID'];
					}

					// get messages where recipient is the group to be deleted
					$sql2 = "SELECT DISTINCT tblMessages.messageID AS messageID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.recipientID = " . $_sendableId;
					$mRs2->Execute($sql2);

					if (0 < $mRs2->Recordcount()){
						while($Message = mysql_fetch_assoc($mRs2->Resultset()))
							$arrMessageId[] = $Message['messageID'];
					}

					if (0 < count($arrMessageId)){
						$lstMessageId = implode(",", $arrMessageId);

						// delete messages
						$sql = "DELETE FROM tblMessages WHERE messageID IN (" . $lstMessageId . ")";
						$mRs->Execute($sql);

						$sql2 = "DELETE FROM tblMessageToAttribute WHERE messageID IN (" . $lstMessageId . ")";
						$mRs2->Execute($sql2);
					}
				}
			}
		}
	 }
?>