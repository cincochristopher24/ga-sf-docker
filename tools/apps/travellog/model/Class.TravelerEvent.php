<?php
/**
 * Created on Oct 12, 2006
 * @author Kerwin Gordo adopted from GroupEvent class by Daphne Dolina
 * Purpose: Abstracts the scheduled events for a traveler
 */
    
/**
*	EDITS:
*		chris - Jan 21, 2009 -- added timezone field
* 		chris - Jan 23, 2009 -- cast date field as datetime
**/
 
    require_once("Class.Connection.php");
    require_once("Class.Recordset.php");
	require_once("Class.GaDateTime.php");
    require_once("travellog/model/Class.GroupFactory.php"); 
    
    class TravelerEvent{
        
                        
        /**
         * Define variables for Class Member Attributes
         */ 
            private $mConn      = NULL;
            private $mRs        = NULL;
           
            //vars for parent classes
 			private $mTraveler    	   = NULL;
 		
            private $mTravelerID      = NULL;
            
            private $mTravelerEventID = NULL;
            private $mTitle        = NULL;
            private $mDate         = NULL;
            private $mDescription  = NULL;
            private $mLink 		   = NULL; 					// added by K. Gordo for link to the event details
            private $mDisplayPublic  = NULL;
			private $mTimeZoneID	= 0;
       
       
        /**
         * Constructor Function for this class
         */
            function TravelerEvent($travelerEventID = 0){
                
                $this->mTravelerEventID = $travelerEventID;
                
                try {
                    $this->conn = new Connection();
                    $this->mRs   = new Recordset($this->conn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mTravelerEventID){
                    
                    $sql = "SELECT * FROM tblTravelerEvent WHERE travelereventID = '$this->mTravelerEventID' ";
                    $this->mRs->Execute($sql);
                
                    if (0 == $this->mRs->Recordcount()){
                        throw new Exception("Invalid travelerEventID");        // ID not valid so throw exception
                    }
                    
                    // Sets values to its class member attributes.
                    $this->mTravelerEventID = $this->mRs->Result(0,"travelerEventID");
                    $this->mTitle          = stripslashes($this->mRs->Result(0,"title"));           
                    $this->mDescription    = stripslashes($this->mRs->Result(0,"description"));       
                    $this->mDate           = $this->mRs->Result(0,"adate");         
                    $this->mDisplayPublic  = $this->mRs->Result(0,"displaypublic");
                    $this->mTimeZoneID	   = $this->mRs->Result(0,'timezoneID');

                    // Sets a value to its parent key.
                    $this->mTravelerID   = $this->mRs->Result(0,"travelerID");    
                }
            }
           
        /**
         * Setter Functions
         */   
            function setTravelerEventID($travelerEventID){
               $this->mTravelerEventID = $travelerEventID;
            }
            
            function setTitle($title){
               $this->mTitle = $title;
            }
           
            function setTheDate($date){
               $this->mDate = $date;
            }
           
            function setDescription($description){
               $this->mDescription = $description;
            }
            
            function setDisplayPublic($displaypublic){
               $this->mDisplayPublic = $displaypublic;
            }

			function setTimeZoneID($tzID=0){
				$this->mTimeZoneID = $tzID;
			}
           
           
        /**
         * Getter Functions
         */  
            function getTravelerEventID(){
               return $this->mTravelerEventID;
            }
            
            function getTitle(){
                return $this->mTitle;
            }
           
            function getTheDate(){
                return $this->mDate;
            }
           
            function getDescription(){
                return $this->mDescription;
            }

			function getTimeZoneID(){
				return $this->mTimeZoneID;
			}
			
			function getTimeZone(){
				require_once('Class.TimeZone.php');
				$obj = new TimeZone($this->mTimeZoneID);
				return $obj->getTimeZone();
			}
            
            /**
             * Edited By: Aldwin Sabornido
             * Date     : Dec. 8, 2006
             * Purpose  : Just added a context to the link... hehehe!!!
             */
            function getLink() {            
            	return 'calendar-event.php?tID=' . $this->mTravelerID . '&eID=' . $this->mTravelerEventID .'&context=2';
            }
            
            function getDisplayPublic(){
                return $this->mDisplayPublic;
            }
        
                
        /**
         * Setter Function for the Parent Key
         */
            function setTravelerID($travelerID){
                $this->mTravelerID = $travelerID;
            }     
            
     
        /**
         * Getter Function for the Parent Key
         */      
            function getTravelerID(){
                return $this->mTravelerID;
            }
            
		/**
         * Getter Function for the Parent Object
         */      
            function getTraveler(){
                if ($this->$mTraveler == NULL){
		 			try {		 				
						$this->$mTraveler   =  new Traveler($this->mTravelerID);
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->$mTraveler;
            }
                           
        /**
         * CRUD Methods (Create, Update , Delete)
         */
            function Create(){
                $this->mDate  = GaDateTime::dbDateTimeFormat(strtotime($this->mDate));
                $Atitle       = addslashes($this->mTitle);
                $Adescription = addslashes($this->mDescription);
                
                $sql = "INSERT into tblTravelerEvent (title, description, adate, travelerID, displaypublic, timezoneID) " .
                        "VALUES ('$Atitle', '$Adescription', '$this->mDate', '$this->mTravelerID', '$this->mDisplayPublic', '$this->mTimeZoneID')";
                $this->mRs->Execute($sql);       
                $this->mTravelerEventID = $this->mRs->GetCurrentID();
            }
            
            function Update(){
                $this->mDate  = GaDateTime::dbDateTimeFormat(strtotime($this->mDate));
                $Atitle       = addslashes($this->mTitle);
                $Adescription = addslashes($this->mDescription);
                
                $sql = "UPDATE tblTravelerEvent SET title = '$Atitle', description = '$Adescription', adate = '$this->mDate', displaypublic = '$this->mDisplayPublic', timezoneID = '$this->mTimeZoneID' " .
                        "WHERE TravelereventID = '$this->mTravelerEventID' ";
                $this->mRs->Execute($sql);
            }
            
            function Delete(){
                
                $sql = "DELETE FROM tblTravelerEvent WHERE TravelereventID = '$this->mTravelerEventID' ";    
                $this->mRs->Execute($sql);   
            }       
                       
    }
    
?>
