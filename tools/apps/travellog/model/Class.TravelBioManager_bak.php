<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	//require_once('travellog/model/Class.User.php');
	require_once('travellog/model/Class.TravelBioUser.php');
	require_once('travellog/model/Class.ProfileBox.php');
	
	class TravelBioManager
	{
		const ERROR_ALREADY_SYNCED = 0;
		const ERROR_DUAL_ACCOUNT = 1;
		const ERROR_TRAVELER_ALREADY_SYNCED = 2;
		const SUCCESS = 3;
		
		const FB_ACCOUNT = 0;
		const GA_ACCOUNT = 1;
		
		const DEF_NUMREC = 20;
		
		private static function getFBRecords($arUIDs=array()){
			require_once('travellog/model/facebook.php');
			$facebook = new Facebook(FBini::getApiKey(), FBini::getApiSecret());
			$facebook->set_user(FBini::getAdminFbUserID(), FBini::getInfiniteSessionKey());
			$fql = 	"SELECT uid, first_name, last_name, name, hometown_location, sex, current_location ".
	            	"FROM user ".
	            	"WHERE uid IN (".implode(',',$arUIDs).")";
			$fbUsers = $facebook->api_client->fql_query($fql);
			//create a map using the facebook uids as keys
			$fbUsersMap = array();
			foreach($fbUsers as $iFbUser){
				$fbUsersMap[$iFbUser['uid']] = $iFbUser;
			}
			return $fbUsersMap;
		}
		/***
		 * Returns all the travelBio users both in ga.net and facebook.
		 */
		public static function getAppUsers($useDecorator=false,$options=array()){
			//set the options
			$sortField = ( isset($options['sortField'])?$options['sortField']:'dateAdded' );
			$sortType = ( isset($options['sortType'])?$options['sortType']:'asc' );
			$limitFrom = ( isset($options['limitFrom'])?$options['limitFrom']:0 );
			$numRecords =  ( isset($options['numRecords'])?$options['numRecords']:self::DEF_NUMREC );
			$db = FBini::createDBHandler();
			$qry = 	'SELECT slambookUserID, genID, userType ' .
					'FROM tbTravelSlambookUser ' .
					'WHERE (userType = 0 AND status=1) ' .
						'OR userType >= 1 ' .
					'ORDER BY '.$sortField.' '.$sortType.' '.
					'LIMIT '.$limitFrom.', '.$numRecords;
			return self::createAppUsers($qry,$useDecorator);
		}
		
		public static function getSyncedAppUsers($useDecorator=false,$option=array()){
			$sortField = ( isset($options['sortField'])?$options['sortField']:'dateAdded' );
			$sortType = ( isset($options['sortType'])?$options['sortType']:'asc' );
			$limitFrom = ( isset($options['limitFrom'])?$options['limitFrom']:0 );
			$numRecords =  ( isset($options['numRecords'])?$options['numRecords']:self::DEF_NUMREC );
			$db = FBini::createDBHandler();
			$qry = 	'SELECT slambookUserID, genID, userType ' .
					'FROM tbTravelSlambookUser ' .
					'WHERE userType >= 2 ' .
					'ORDER BY '.$sortField.' '.$sortType.' '.
					'LIMIT '.$limitFrom.', '.$numRecords;
			return self::createAppUsers($qry,$useDecorator);
		}
		
		public static function getUsersWithFbAccount($useDecorator=false,$options=array()){
			$sortField = ( isset($options['sortField'])?$options['sortField']:'dateAdded' );
			$sortType = ( isset($options['sortType'])?$options['sortType']:'asc' );
			$limitFrom = ( isset($options['limitFrom'])?$options['limitFrom']:0 );
			$numRecords =  ( isset($options['numRecords'])?$options['numRecords']:self::DEF_NUMREC );
			//select all the records which are facebook user only (0) or Both (2)
			$qry =	'SELECT slambookUserID, genID, userType ' .
					'FROM SocialApplication.tbTravelSlambookUser ' .
					'WHERE (userType = 0 OR userType = 2) ' .
					'AND status = 1 ' .
					'ORDER BY '.$sortField.' '.$sortType.' '.
					'LIMIT '.$limitFrom.', '.$numRecords;
			return self::createAppUsers($qry,$useDecorator);
		}
		
		public static function getFbAppUsersOnly($useDecorator=false,$options=array()){
			$sortField = ( isset($options['sortField'])?$options['sortField']:'dateAdded' );
			$sortType = ( isset($options['sortType'])?$options['sortType']:'asc' );
			$limitFrom = ( isset($options['limitFrom'])?$options['limitFrom']:0 );
			$numRecords =  ( isset($options['numRecords'])?$options['numRecords']:self::DEF_NUMREC );
			//select all the records which are facebook user only (0) or Both (2)
			$qry =	'SELECT slambookUserID, genID, userType ' .
					'FROM SocialApplication.tbTravelSlambookUser ' .
					'WHERE userType = 0 ' .
					'ORDER BY '.$sortField.' '.$sortType.' '.
					'LIMIT '.$limitFrom.', '.$numRecords;
			return self::createAppUsers($qry,$useDecorator);
		}
		
		public static function getGaAppUsersOnly($useDecorator=false,$options=array()){
			$sortField = ( isset($options['sortField'])?$options['sortField']:'dateAdded' );
			$sortType = ( isset($options['sortType'])?$options['sortType']:'asc' );
			$limitFrom = ( isset($options['limitFrom'])?$options['limitFrom']:0 );
			$numRecords =  ( isset($options['numRecords'])?$options['numRecords']:self::DEF_NUMREC );
			//select all the records which are facebook user only (0) or Both (2)
			$qry =	'SELECT slambookUserID, genID, userType ' .
					'FROM SocialApplication.tbTravelSlambookUser ' .
					'WHERE userType = 1 ' .
					'ORDER BY '.$sortField.' '.$sortType.' '.
					'LIMIT '.$limitFrom.', '.$numRecords;
			return self::createAppUsers($qry,$useDecorator);
		}
		
		private static function createAppUsers($qry='',$useDecorator=false){
			require_once('travellog/model/Class.FBini.php');
			$db = FBini::createDBHandler();
			//select all the records which are facebook user only (0) or Both (2)
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				require_once('travellog/model/socialApps/Class.FBUser.php');
				$arFbUserIDs = array();
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					if(User::$FB == $rs->getUserType() || User::$BOTH == $rs->getUserType()){
						$arFbUserIDs[] = $rs->getGenID();
					}
				}
				$fbUsersMap = self::getFBRecords($arFbUserIDs);
				if($useDecorator){
					//wrap the slambookuser with the FBUser decorator class
					require_once('travellog/model/socialApps/Class.FBUser.php');
					require_once('travellog/model/Class.FBTravelerUser.php');
					require_once('travellog/model/Class.Traveler.php');
					for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
						if(User::$FB == $rs->getUserType() || User::$BOTH == $rs->getUserType()){
							if(array_key_exists($rs->getGenID(),$fbUsersMap)){
								$ar[$rs->getSlambookUserID()] = new FBUser(new User($rs->getSlambookUserID()), $fbUsersMap[$rs->getGenID()] );
							}
						}
						else{
							try{
								$ok = true;
								$travelerObj = new Traveler($rs->getGenID());
							}
							catch(exception $e){
								$ok = false;
							}
							if($ok){
								$ar[$rs->getSlambookUserID()] = new FBTravelerUser(new User($rs->getSlambookUserID()),$ok);
							}
						}						
					}
				}
				else{
					for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
						$ar[$rs->getSlambookUserID()] = new User($rs->getSlambookUserID());
					}
				}
			}
			return $ar;	
		}
		
		public static function refreshFbUserProfileBox($fbUser){
			require_once('travellog/model/facebook.php');
			require_once('travellog/model/Class.FBini.php');
			$facebook = new Facebook(FBini::getApiKey(), FBini::getApiSecret());
			$facebook->set_user(FBini::getAdminFbUserID(), FBini::getInfiniteSessionKey());
			$facebook->api_client->profile_setFBML(ProfileBox::generateCustomProfileFBML($fbUser),$fbUser->getGenID());
		}

		public static function syncFbGaUsers($fbUser,$travelerID,$useAccount=null){
			//initialize GA user
			$gaUser = TravelBioUser::getTravelBioUserByTravelerID($travelerID);
			//$slambookID_travelerID = User::getSlambookUserIDbyGenID($travelerID,1);
			if(!is_null($gaUser)){
				if( $gaUser->isSynchronize() ){
					if($gaUser->getTravelBioUserID() != $fbUser->getTravelBioUserID()){
						return self::ERROR_TRAVELER_ALREADY_SYNCED;
					}
					else{
						//check if the fbUser is already synced with the given gaUser
						return self::ERROR_ALREADY_SYNCED;
					}
				}
				else{//the ga user has a travelbio but it is not synced to an fb account, its ok to proceed but we have to check 
					//what travelbio to follow.
					$travelerBioAnswers = $gaUser->getAnswers();
					$fbUserBioAnswers = $fbUser->getAnswers();
					if(count($travelerBioAnswers) && count($fbUserBioAnswers)){
						if(self::GA_ACCOUNT==$useAccount){
							$domAccount = $gaUser;
							$nonDomAccount = $fbUser;	
						}
						elseif(self::FB_ACCOUNT==$useAccount){
							$domAccount = $fbUser;
							$nonDomAccount = $gaUser;
						}
						else{
							return self::ERROR_DUAL_ACCOUNT;
						}
					}
					else{
						if(count($travelerBioAnswers)){
							$domAccount = $gaUser;
							$nonDomAccount = $fbUser;
						}
						else{
							$domAccount = $fbUser;
							$nonDomAccount = $gaUser;
						}
					}
					//if the fbUser is currently synced to another ga user, we must unsynced it and create a new record for it.
					if($fbUser->isSynchronized()){
						$objs = $fbUser->unsynchronize();
						$fbUser = $objs['fbUser'];
					}
					self::syncFbUser($domAccount,$fbUser->getGenID(),$travelerID);
					$nonDomAccount->delete();
					
					return self::SUCCESS;
				}
			}
			else{//if travelerID entered has no travelBio, we can insert directly
				//if the fbUser is currently synced with another gaUser, unsync it an resync the fbUser to the new gaUser 
				if($fbUser->getUserType() == User::$BOTH){
					self::unsyncUser($fbUser);
				}
				self::syncFbUser($fbUser,$fbUser->getGenID(),$travelerID);
				return self::SUCCESS;
			}
		}
		
		private static function syncFbUser($fbUser,$newGenID,$travelerID){
			$fbUser->setUserType(User::$BOTH);
			$fbUser->setGenID($newGenID);
			$fbUser->setTravelerID($travelerID);
			$fbUser->save();
			self::refreshFbUserProfileBox($fbUser);
		}
		
		public static function unsyncUser($fbUser){
			if($fbUser->getUserType() == User::$BOTH){
				$dupFBuser = self::createDuplicate($fbUser);
				$dupFBuser->setUserType(User::$GA);
				$dupFBuser->setGenID($dupFBuser->getTravelerID());
				$dupFBuser->save();
				//duplicate the answers
				self::duplicateAnswers($fbUser,$dupFBuser);
				//update the fbUser account, set the userType = 0;
				$fbUser->setUserType(User::$FB);
				$fbUser->save();
			}
			return array('fbUser'=>$fbUser,'gaUser'=>$dupFBuser);
		}
		
		private static function createDuplicate($fbUser){
			$newFbUser = new User();
			$newFbUser->setGenID($fbUser->getGenID());
			$newFbUser->setUserType($fbUser->getUserType());
			$newFbUser->setTravelerID($fbUser->getTravelerID());
			$newFbUser->setStatus($fbUser->getStatus());
			$newFbUser->setViewID($fbUser->getViewID());
			$newFbUser->getNumOfQDisplayed($fbUser->getNumOfQDisplayed());
			return $newFbUser;
		}
		
		private static function duplicateAnswers($srcUser,$targetUser){
			$origAnswers = $srcUser->getAnswers();
			foreach($origAnswers as $iAns){
				$newAns = $iAns->createDuplicate();
				$newAns->setSlambookUserID($targetUser->getSlambookUserID());
				$newAns->save();
			}
		}
	}
?>