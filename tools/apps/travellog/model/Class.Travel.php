<?php
/*
 * Created on Jul 26, 2006
 * Created by: Czarisse Daphne P. Dolina
 * 
 */
 	
	require_once("Class.Recordset.php");
	require_once("Class.FilterOp.php");
 	require_once("Class.Criteria2.php");
 	require_once("Class.iRecordset.php");
 	require_once('travellog/model/Class.Trip.php');
 	require_once('travellog/helper/Class.SqlUtil.php');
	require_once('travellog/model/Class.Group.php');
 	require_once('caching/memcache.class.php');
	require_once('travellog/model/Class.JournalPrivacyPreferenceType.php');
 	require_once('Cache/ganetCacheProvider.php');
 	require_once('Class.ConnectionProvider.php');
 	require_once('travellog/model/Class.TravelLogToVideo.php');		// added by neri: 10-20-08
 	
 	class Travel{  
 		
 		// vars for db connection
 		private $conn		 = NULL;
 		private $rs		 	 = NULL;
 		private $rs2		 	 = NULL;
 		
 		// vars for parent ID
 		private $travelerID  = NULL;
 		
 		//vars for parent classes
 		private $traveler    = NULL;
 		private $owner    = NULL;
 		
 		// vars for class attributes
 		private $travelID     = NULL;
 		private $locationID   = NULL; // where travel is confined to a location e.g. Asia, or China (world, continent,country, region etc...)
 		private $title        = NULL;
 		private $publish      = 0;
 		private $description  = NULL;
 		protected $rating     = NULL;
 		private $votescount  = NULL;
 		private $views        = NULL;
 		private $isFeatured   = false;
		private $deleted  	  = 0; // added by ianne: 01/20/2009
		private $mJournalEntryDraftsCount = null;
 		
 		protected $travellinkID  = 0;
 		private $refID  	   = 0;
 		private $reftype  	   = 0;
 		
 		private $lastupdated  = NULL;
 		private $entryCount   = 0;			// count of travellogs(journal entry) related to this travel(journal) 
 		
 		// vars for other methods
 		private $newrating    = NULL;
 		private $location     = NULL;
 		private $randphoto    = NULL;
 		private $arrdaterange = array();
 		private $arrtrip      = array();
 		private $primaryphoto = 0;
		private $privacypref  = 0;
		private $approved     = 0;
		private	$cache		=	null;	
 		
 		
 		// vars for static function
 		private $arrtravels   		= array();
 		private $arrfilteredtravels = array();
 		private $arrphotos 			= array();
 		
 		protected static $statvar;

		//added by Jul, to store data of a journal when that journal is instantiated more than once in a single page
		private static $loadedTravels = array();

		private $recentlyAdded = NULL;
 		
 		//constructor
 		
	 		function Travel($_travelID = 0,$data = null){
	 			//$this->cache 	=	ganetCacheProvider::instance()->getCache();
	 			$this->travelID = $_travelID;
	 			
	 			try {
					$this->rs   = new Recordset(ConnectionProvider::instance()->getConnection());
					
				}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				
		 		if ($this->travelID > 0){
		 			
		 			$found = false;
		 			
		 			if ($this->cache	!= 	null){

		 				$journal	=	$this->cache->get(get_class($this) . '_' . $_travelID);
		 				if ($journal){
		 					$found = true;
		 					$this->locationID 	= $journal->getLocationID(); 		
				 			$this->title 		= $journal->getTitle(); 		
				 			$this->description 	= $journal->getDescription(); 		
				 			$this->rating 		= $journal->getRating(); 	
				 			$this->votescount 	= $journal->getVotes();
				 			$this->views 		= $journal->getViews(); 
				 			$this->lastupdated 	= $journal->getLastUpdated();
				 			$this->primaryphoto = $journal->getPrimaryphoto();
							$this->privacypref  = $journal->getPrivacyPref();
							$this->publish      = $journal->getPublish();	
							$this->isFeatured   = $journal->isFeatured();	
							$this->entryCount   = $journal->getEntryCount(); 			 
				 			// set value to the parent ID
				 			$this->travelerID 	= $journal->getTravelerID(); 	
				 			$this->travellinkID 	= $journal->getTravelLinkID();
							$this->deleted      = $journal->getDeleted(); 	  
		 					
							$data["locID"] 	= $journal->getLocationID();  	
				 			$data["title"] 	= $journal->getTitle(); 
				 			$data["description"] = $journal->getDescription(); 		
				 			$data["rating"]	= $$journal->getRating();
				 			$data["votes"] = $journal->getVotes();
				 			$data["viewed"] = $journal->getViews(); 
				 			$data["lastupdated"] = $journal->getLastUpdated();
				 			$data["primaryphoto"] = $journal->getPrimaryphoto();
							$data["privacypref"] = $journal->getPrivacyPref();
							$data["publish"] = $journal->getPublish();	
							$data["isfeatured"] = $journal->isFeatured();	
							$data["entryCount"] = $journal->getEntryCount();  			 
				 			$data["travelerID"]	= $journal->getTravelerID(); 
				 			$data["travellinkID"] = $journal->getTravelLinkID();
							$data["deleted"] = $journal->getDeleted();
							self::$loadedTravels[$this->travelID] = $data;		
		 				}
		 			}
		 			
		 			if (!$found){
						if( isset(self::$loadedTravels[$this->travelID]) ){
							$data = self::$loadedTravels[$this->travelID];
							$this->locationID 	= $data["locID"]; 		
				 			$this->title 		= stripslashes($data["title"]); 		
				 			$this->description 	= stripslashes($data["description"]); 		
				 			$this->rating 		= $data["rating"]; 	
				 			$this->votescount 	= $data["votes"];
				 			$this->views 		= $data["viewed"]; 
				 			$this->lastupdated 	= $data["lastupdated"];
				 			$this->primaryphoto = $data["primaryphoto"];
							$this->privacypref  = $data["privacypref"];
							$this->publish      = $data["publish"];	
							$this->isFeatured   = $data["isfeatured"];	
							$this->entryCount   = $data["entryCount"]; 			 
				 			$this->travelerID 	= $data["travelerID"]; 	
				 			$this->travellinkID = $data["travellinkID"];
							$this->deleted      = $data["deleted"];
						}else{
		 					$sql = "SELECT * FROM tblTravel WHERE travelID = '$this->travelID' AND deleted = 0";
				 			$this->rs->Execute($sql);
		 			
			 				if ($this->rs->Recordcount() == 0){
			 					throw new Exception("Invalid TravelID");		// ID not valid so throw exception
			 				}
		 				
			 				// set values to its attributes
			 				$this->locationID 	= $this->rs->Result(0,"locID"); 		
				 			$this->title 		= stripslashes($this->rs->Result(0,"title")); 		
				 			$this->description 	= stripslashes($this->rs->Result(0,"description")); 		
				 			$this->rating 		= $this->rs->Result(0,"rating"); 	
				 			$this->votescount 	= $this->rs->Result(0,"votes");
				 			$this->views 		= $this->rs->Result(0,"viewed"); 
				 			$this->lastupdated 	= $this->rs->Result(0,"lastupdated");
				 			$this->primaryphoto = $this->rs->Result(0,"primaryphoto");
							$this->privacypref  = $this->rs->Result(0,"privacypref");
							$this->publish      = $this->rs->Result(0,"publish");	
							$this->isFeatured   = $this->rs->Result(0,"isfeatured");	
							$this->entryCount   = $this->rs->Result(0,"entryCount"); 			 
				 			// set value to the parent ID
				 			$this->travelerID 	= $this->rs->Result(0,"travelerID"); 	
				 			$this->travellinkID 	= $this->rs->Result(0,"travellinkID");
						
							$this->deleted      = $this->rs->Result(0,"deleted");

							if ($this->cache != null) {
								$this->cache->set(get_class($this) . '_' . $this->travelID,$this,array('EXPIRE'=>86400));
							
							}
							
							$data["locID"] 	= $this->rs->Result(0,"locID"); 	
				 			$data["title"] 	= $this->rs->Result(0,"title");
				 			$data["description"] = $this->rs->Result(0,"description");		
				 			$data["rating"]	= $this->rs->Result(0,"rating");
				 			$data["votes"] = $this->rs->Result(0,"votes");
				 			$data["viewed"] = $this->rs->Result(0,"viewed"); 
				 			$data["lastupdated"] = $this->rs->Result(0,"lastupdated");
				 			$data["primaryphoto"] = $this->rs->Result(0,"primaryphoto");
							$data["privacypref"] = $this->rs->Result(0,"privacypref");
							$data["publish"] = $this->rs->Result(0,"publish");	
							$data["isfeatured"] = $this->rs->Result(0,"isfeatured");	
							$data["entryCount"] = $this->rs->Result(0,"entryCount"); 			 
				 			$data["travelerID"]	= $this->rs->Result(0,"travelerID");
				 			$data["travellinkID"] = $this->rs->Result(0,"travellinkID");
							$data["deleted"] = $this->rs->Result(0,"deleted");
							self::$loadedTravels[$this->travelID] = $data;
						}
		 			}
	 				
	 				/*
	 				if(Cache::get('travel_cache-'.$this->travelID)){
					$ObjResult = Cache::get('travel_cache-'.$this->travelID);
					
					}else{
						$sql = "SELECT * FROM tblTravel WHERE travelID = '$this->travelID' ";
		 				$this->rs->Execute($sql);
						$ObjResult = $this->rs->FetchAsObject();
						
						if ($this->rs->Recordcount() == 0){
	 						throw new Exception("Invalid TravelID");		// ID not valid so throw exception
	 					}
	 					
						Cache::set('travel_cache-'.$this->travelID,$ObjResult);
					}
	 				
	 				// set values to its attributes
	 				$this->locationID 	= $ObjResult->locID; 		
		 			$this->title 		= stripslashes($ObjResult->title); 		
		 			$this->description 	= stripslashes($ObjResult->description); 		
		 			$this->rating 		= $ObjResult->rating; 	
		 			$this->votescount 	= $ObjResult->votes;
		 			$this->views 		= $ObjResult->viewed; 
		 			$this->lastupdated 	= $ObjResult->lastupdated;
		 			$this->primaryphoto = $ObjResult->primaryphoto;
		 			
		 			// set value to the parent ID
		 			$this->travelerID 	= $ObjResult->travelerID; 	
		 			
		 			$this->travellinkID = $ObjResult->travellinkID;
	 				*/		 				
		 		}elseif(is_array($data)){
	 				
	 				// set values to its attributes
	 				$this->locationID 	= $data["locID"]; 		
		 			$this->title 		= stripslashes($data["title"]); 		
		 			$this->description 	= stripslashes($data["description"]); 		
		 			$this->rating 		= $data["rating"]; 	
		 			$this->votescount 	= $data["votes"];
		 			$this->views 		= $data["viewed"]; 
		 			$this->lastupdated 	= $data["lastupdated"];
		 			$this->primaryphoto = $data["primaryphoto"];
					$this->privacypref  = $data["privacypref"];
					$this->publish      = $data["publish"];	
					$this->isFeatured   = $data["isfeatured"];	
					$this->entryCount   = $data["entryCount"]; 			 
					$this->travelID		= $data["travelID"];
		 			// set value to the parent ID
		 			$this->travelerID 	= $data["travelerID"]; 	
		 			$this->travellinkID = $data["travellinkID"];
					
		 			if ($this->cache != null) {
		 				$this->cache->set(get_class($this) . '_' . $this->travelID,$this,array('EXPIRE'=>86400));
							
					}
		 			
	 			}
	 		
	 				
	 		}
 		
 		//setter
 		
	 		function setTravelID($_travelID){
	 			$this->travelID = $_travelID;
	 		}
	 		
	 		function setLocationID($_locationID){
	 			$this->locationID = $_locationID;
	 		}
	 		
	 		function setTitle($_title){
	 			$this->title = $_title; 			
	 		}
	 		
	 		function setPublish($_publish){
	 			$this->publish = $_publish; 			
	 		}
	 		
	 		function setDescription($_description){
	 			$this->description = $_description;
	 		}
	 		
	 		function setVotes($_votescount){
	 			$this->votescount = $_votescount;
	 		}
	 		
	 		function setViews($_views){
	 			$this->views = $_views;
	 		}
	 		
	 		function setLastUpdated($_lastupdated){
	 			$this->lastupdated = $_lastupdated;
	 		}
	 		
	 		function setRefID($_refID){
	 			$this->refID = $_refID;
	 		}
	 		
	 		function setRefType($_reftype){
	 			$this->reftype = $_reftype;
	 		}
	 		
	 		function setPrimaryphoto($_primaryphoto){
	 			$this->primaryphoto = $_primaryphoto;
	 		}
	
			function setPrivacyPref($_privacypref){
	 			$this->privacypref = $_privacypref;
	 		}
	 		
	 		function setRating($rating){
	 			$this->rating = $rating;
	 		}
	 		
	 		function setApproved($_approved){
	 			$this->approved = $_approved;
	 		}
	
			function setTravelLinkID($_travellinkID){
	 			$this->travellinkID = $_travellinkID;
	 		}
	 		
	 		function setEntryCount($newCount){
	 			$this->entryCount = $newCount;
	 		}

			function setIsFeatured($_isFeatured){
				$this->isFeatured = $_isFeatured;
			}
			
			function setDeleted($_deleted){
				$this->deleted = $_deleted;
			}
			
 		//getter 
 		
	 		function getTravelID(){
	 			return $this->travelID;
	 		}
	 		
	 		function getLocationID(){	 			
	 			return $this->locationID;
	 		}
	 		
	 		function getTitle(){
	 			return $this->title;
	 		}
	 		
	 		function getPublish(){
	 			return $this->publish;
	 		}
	 		
	 		function getDescription(){
	 			return $this->description;
	 		}
	 		
	 		function getRating(){
	 			return $this->rating;
	 		}
	 		
	 		function getVotes(){
	 			return $this->votescount;
	 		}
	 		
	 		function getViews(){
	 			return $this->views;
	 		}
	 		
	 		function getLastUpdated(){
	 			return $this->lastupdated;
	 		}
	 		
	 		function getPrimaryphoto(){
	 			return $this->primaryphoto;
	 		}
	 		
			function getPrivacyPref(){
	 			return $this->privacypref;
	 		}
	 		
	 		function getApproved(){
	 			return $this->approved;
	 		}
	 		
	 		function isFeatured(){
	 			return $this->isFeatured;
	 		}
	 		
	 		function getEntryCount(){
	 			return $this->entryCount;
	 		}
	 		
			function getDeleted(){
				return $this->deleted;
			}
			
	 	// setter of parent ID's
	 	
	 		function setTravelerID ($_travelerID){
	 			$this->travelerID = $_travelerID;	
	 		}
	 		
	 	// getter of parent ID's
	 	
	 		function getTravelerID(){
	 			return $this->travelerID;
	 		}
	 		
	 	
	 	// getter of parent classes
	 	
	 		// get the traveler of this travel
	 		function getTraveler(){
		 		
		 		if ($this->traveler == NULL){
		 			try {
		 				$this->traveler = new Traveler($this->travelerID);
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->traveler;
	 			
	 		}	
	 		
	 		// get the owner of this travel
	 		function getOwner(){
		 		
		 		if ($this->owner == NULL){
		 			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
		 			$sql = "SELECT refID, reftype FROM tblTravelLink WHERE travellinkID = $this->travellinkID  " ;
	 				$this->rs->Execute($sql);
	 	
		 			try {
		 				if (1 == $this->rs->Result(0,"reftype"))
		 					$this->owner = new Traveler($this->rs->Result(0,"refID"));
		 				else if (2 == $this->rs->Result(0,"reftype")){
							$mGroup = GroupFactory::instance()->create( array($this->rs->Result(0,"refID")) );
							if(isset($mGroup[0]))
								$this->owner = $mGroup[0];
		 				}
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}		
		
	 			return $this->owner;
	 			
	 		}	
	 		
	 		
	 		// get the travellinkID of the travel's owner, based on the values set for refID and reftype
	 		function  getTravelLinkID(){
		 		
		 		if ($this->travellinkID == 0){
		 			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
		 			$sql = "SELECT travellinkID FROM tblTravelLink WHERE refID = $this->refID AND reftype = $this->reftype  " ;
	 				$this->rs->Execute($sql);
	 			
		 			if ($this->rs->Recordcount())
		 				$this->travellinkID = $this->rs->Result(0,"travellinkID");
		 			else {
		 				$sql = "INSERT into tblTravelLink (refID, reftype) VALUES ($this->refID, $this->reftype)";
			 			$this->rs->Execute($sql);		
			 			$this->travellinkID = $this->rs->GetCurrentID();
		 			}
		 		}			
	 			return $this->travellinkID;
	 			
	 		}	
	 				
 		// CRUD methods
 		
 			function Create(){
 				$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
 				$titl = addslashes($this->title);
 				$desc = addslashes($this->description);
 				
 				$_travellinkID = $this->getTravelLinkID();
 				
	 			$sql = "INSERT into tblTravel (travelerID, travellinkID, locID, title, description, rating, viewed, privacypref, publish) " .
	 					"VALUES ('$this->travelerID', $_travellinkID, '$this->locationID', '$titl', '$desc', '$this->rating', '$this->views' , '$this->privacypref', '$this->publish')";
	 			$this->rs->Execute($sql);		
	 			$this->travelID = $this->rs->GetCurrentID();
	 			$this->invalidateCacheEntry();	 	
	
				//added by Jul to create feed for this journal
				require_once("travellog/model/Class.ActivityFeed.php");
				try{
					ActivityFeed::create($this);
				}catch(exception $e){}	 
				$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;	
	 		}
	 		
	 		function Update($now=NULL){
	 			
	 			$titl = addslashes($this->title);
 				$desc = addslashes($this->description);
 				
 				if( $now == NULL ){
	 				// Added By: Aldwin S. Sabornido
	 				// if t-bud's account remain its current position from the journals page
	 				if( $this->travelerID == 1751){
	 					require_once('travellog/model/Class.Travel.php');
	 					$objTravel = new Travel($this->travelID);
	 					$now       = $objTravel->getLastUpdated(); 
	 				}else
		 				$now  = date("Y-m-d H:i:s");
 				}
	 			
 				$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$sql = "UPDATE tblTravel SET locID = '$this->locationID', title = '$titl', description = '$desc', rating = '$this->rating', viewed = '$this->views', lastupdated = '$now', primaryphoto = '$this->primaryphoto' , privacypref = '$this->privacypref', publish = '$this->publish', entryCount = '$this->entryCount' WHERE travelID = '$this->travelID'  " ;
	 			$this->rs->Execute($sql);         

	 			$this->invalidateCacheEntry();
	 			$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;
	 		}
	 		
	/**
	* edits by ianne
	*	commented out delete queries.
	*	flagged as deleted instead of actually deleting travels
	**/
	 		function Delete(){
	 				
	 			$mytrips = $this->getTrips();									//get trips of travel
	 			
	 			for($idx_tp=0;$idx_tp<count($mytrips);$idx_tp++){
	 				$mytrips[$idx_tp]->Delete();								// delete trips of travel
	 			}
	 			
	 			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 			
	 			//$sql = "DELETE FROM tblTravel where travelID = '$this->travelID' ";			// delete selected travel
				$sql = "UPDATE tblTravel SET deleted = 1 WHERE travelID = '$this->travelID' ";			// flag selected travel as deleted
	 			$this->rs->Execute($sql);
	 			
				$this->deleteRelatedPhotos();
				
				$this->invalidateCacheEntry();
	 			
	 			// added by K. Gordo. Delete entries in tblGroupApprovedJournals with this travelID
	 			//$sql = "DELETE FROM tblGroupApprovedJournals where travelID = '$this->travelID' ";
	 			//$this->rs->Execute($sql);	
	 			
	 			// delete entry in feature table
	 			//$sql = "DELETE FROM tblGroupFeaturedJournals where travelID = '$this->travelID' ";
	 			//$this->rs->Execute($sql);	
	 			
	 			// add by K. Gordo delete entry in tblTravellink table
	 			//$sql = "DELETE FROM tblTravelLink where travellinkID = '$this->travellinkID' ";
	 			//$this->rs->Execute($sql);	  
	 			
				//added Jul to delete feed of this journal
				require_once("travellog/model/Class.ActivityFeed.php");
				$JOURNAL 		= ActivityFeed::JOURNAL;
				$JOURNALENTRY	= ActivityFeed::JOURNALENTRY;
				$sql = "DELETE FROM tblActivityFeed 
						WHERE (feedSection = $JOURNAL AND sectionID = '$this->travelID')
							OR (feedSection = $JOURNALENTRY AND sectionRootID = '$this->travelID')";
				$this->rs->Execute($sql);
				$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;		
	 		}
	 		
	 		function Archive(){
	 			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$sql = "UPDATE tblTravel SET deleted = 1 WHERE travelID = '$this->travelID' ";			// flag selected travel as deleted
	 			$this->rs->Execute($sql);
				$this->deleteRelatedPhotos();
				
				$this->invalidateCacheEntry();
				
				//added Jul to delete feed of this journal
				require_once("travellog/model/Class.ActivityFeed.php");
				$JOURNAL 		= ActivityFeed::JOURNAL;
				$JOURNALENTRY	= ActivityFeed::JOURNALENTRY;
				$sql = "DELETE FROM tblActivityFeed 
						WHERE (feedSection = $JOURNAL AND sectionID = '$this->travelID')
							OR (feedSection = $JOURNALENTRY AND sectionRootID = '$this->travelID')";
				$this->rs->Execute($sql);
				$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;
	 		}
	 		
	 	// other methods
	 	
	 		//add rating for the travel
	 		function addRating($_newrating){
	 			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$this->newrating = $_newrating;
	 			
	 			$totalrating = $this->rating * $this->votescount;	//get total rating based on average rating and number of votes	
	 			
	 			$newtotalrating = $totalrating + $this->newrating;	//add the new rating to the original totalrating
	 			
	 			$newvotescount = $this->votescount + 1;			//increment the number of votes
	 			
	 			$averating = $newtotalrating / $newvotescount;		//get the average rating based on new total rating and new number of votes
	 			
	 			$sql = "UPDATE tblTravel SET rating = '$averating', votes = '$newvotescount' WHERE travelID = '$this->travelID'  " ;
	 			$this->rs->Execute($sql);
	 			$this->invalidateCacheEntry();
	 		}
	 		
	 		
	 		//add view for the travel
	 		function addView(){
	 			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$newviewscount = $this->views + 1;			//increment the number of views
	 			
	 			$sql = "UPDATE tblTravel SET viewed = '$newviewscount' WHERE travelID = '$this->travelID'  " ;
	 			$this->rs->Execute($sql);
	 			$this->invalidateCacheEntry();
				$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;
	 		}
	 		
	 		
	 		// get the location of the travel / journal
	 		function getLocation(){
	 			
	 			if ($this->location == NULL){
	 				try {	
	 					$factory = LocationFactory::instance();
	 					$this->location = $factory->create($this->locationID);	 
	 				}
					catch (Exception $e) {
					   throw $e;
					}				
	 			}
	 			return $this->location;
	 		}	
	 		
	 		
	 		// get any random photo from the travel logs / journal entries within the travel / journal
	 		function getRandomPhoto(){ 
	 			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());	
			 	if($this->getPrimaryphoto()){
			 		
			 		$sql = "SELECT tblTravelLog.travellogID FROM tblTravelLog WHERE" .
			 				" tblTravelLog.travelID = '$this->travelID'   " .
			 				"ORDER BY RAND() LIMIT 1";
					$this->rs->Execute($sql);	
					$recordset 	= mysql_fetch_array($this->rs->Resultset());
					
			 		$travellog 	= new TravelLog($recordset['travellogID'] );
					$photo 		= new Photo($travellog, $this->getPrimaryphoto());
					return $photo;
			 	
			 	}else{
			 			if ($this->randphoto == NULL){
			 				
			 				// get the number of photos to choose from
			 				$sql = "SELECT count(tblTravelLog.travellogID) as totalrec FROM tblTravelLog, tblTravelLogtoPhoto WHERE tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID AND tblTravelLog.deleted = 0 AND tblTravelLog.travelID = '$this->travelID' ";
			 				$this->rs->Execute($sql);
			 				
			 				if ($this->rs->Result(0,"totalrec")  == 0){
			 					try {	
				 					$travellog = new TravelLog();
				 					$this->randphoto = new Photo($travellog);
				 				}
								catch (Exception $e) {
								   throw $e;
								}	
			 				}
			 				
			 				else {
			 					
			 					$max = $this->rs->Result(0,"totalrec") - 1;
								
								$randidx = mt_rand(0 , $max );	 		// the index starting from top row
								
			 					$sql = "SELECT tblTravelLog.travellogID , tblTravelLogtoPhoto.photoID from tblTravelLog, tblTravelLogtoPhoto WHERE tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID AND tblTravelLog.deleted = 0 AND tblTravelLog.travelID = '$this->travelID' ORDER BY tblTravelLogtoPhoto.photoID LIMIT $randidx , 1" ;;
			 					$this->rs->Execute($sql);
			 					
			 					while ($recordset = mysql_fetch_array($this->rs->Resultset())) {				
				 					try {	
					 					$travellog = new TravelLog($recordset['travellogID'] );
					 					$this->randphoto = new Photo($travellog, $recordset['photoID']);
					 				}
									catch (Exception $e) {
									   throw $e;
									}	
			 					}		
			 				}
			 			}
			 			
			 			return $this->randphoto;
			 			
			 	}		
			 			
	 		}
	 		
	 		
	 		//get the date range of the entire travel/journal ; get the arrival of first and last trip of the travel
	 		function getDateRange(){
	 			
	 			if ($this->arrdaterange == NULL){
	 				$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 				//$sql = "SELECT arrival from tblTrips where travelID = '$this->travelID'  ORDER BY arrival ASC ";
	 				$sql = "SELECT arrival from tblTrips, tblTravelLog where  tblTrips.travelID = '$this->travelID'  AND  tblTrips.tripID = tblTravelLog.tripID  AND  tblTravelLog.deleted = 0 GROUP tblTrips.tripID BY ORDER BY  tblTrips.arrival ASC ";
	 				
	 				$this->rs->Execute($sql);
	 				
	 				if ($this->rs->Recordcount() == 0){
	 					return NULL;	 					
	 				}
	 				
	 				else {
		 				$this->arrdaterange[] = $this->rs->Result(0,"arrival");		// add to array the arrival of the FIRST trip
		 				
		 				$lastindex =  $this->rs->Recordcount() - 1;
		 				if ($lastindex > 0){
			 				$this->arrdaterange[] = $this->rs->Result($lastindex,"arrival");	// if more than one trip exists, add to array the arrival of the LAST trip			 				
		 				}
	 				}
	 			}
	 			
	 			return $this->arrdaterange;
	 			
	 		}
	 		
	 		// get all trips within the travel journal
	 		function getTrips($criteria = null){
	 			$this->cache	=	ganetCacheProvider::instance()->getCache();
	 			if ($criteria == null){
	 				$hashKey	=	'';
	 			}else{
	 				$hashKey	=	$criteria->getHash();
	 			}
	 			
	 			$found	=	false;
	 			//echo 'Travel_Trips_Relation_' . $this->travelID . '_' . $hashKey;echo '<br/>';
	 			if ($this->cache != null){
	 				$tripIDs	=	$this->cache->get('Travel_Trips_Relation_' . $this->travelID . '_' . $hashKey);
	 				if ($tripIDs){
	 					$found = true;
	 					foreach ($tripIDs as $tripID){
	 						if ($trip	=	$this->cache->get($tripID)) {
	 						 	$this->arrtrip[] = $trip;
	 						} else {	
								// one of the keys is not found so we have to hit the db
								$found = false;
	 							$this->arrtrip	=	array();
	 							break;
	 						}	
	 					}
	 				}
	 			}
	 			
	 			if (!$found){
		 			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
		 			if (null == $criteria){
		 				$criteria = new Criteria2();
		 				$criteria->setOrderBy("arrival, tripID"); 
		 			}
	
		 			if (0 == count($this->arrtrip)){
						$sql = "SELECT DISTINCT tblTrips.*, tblTravel.travelerID, tblTravel.travellinkID " .
								"FROM tblTravelLog, tblTrips, tblTravel " .
								"WHERE tblTrips.travelID = tblTravel.travelID " .
								"AND  tblTrips.tripID = tblTravelLog.tripID " .
								"AND tblTravelLog.deleted = 0 " .
								"AND tblTravel.deleted = 0 " .
								"AND tblTravel.travelID = " . $this->travelID . " " . $criteria->getCriteria2();
		 				$this->rs->Execute($sql);
	
						$arrTrip = array();
						$tripIDs	=	array();
		 				if (0 < $this->rs->Recordcount()){
			 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {
								$mData = array();
								$mData['arrival']		= $recordset['arrival'];
								$mData['travelId']		= $recordset['travelID'];
								$mData['locationId']	= $recordset['locID'];
								$mData['travelerId']	= $recordset['travelerID'];
								$mData['travelLinkId']	= $recordset['travellinkID'];
								$mData['tripId']	    = $recordset['tripID'];
	
			 					$arrTrip[] = new Trip(0, $mData);
			 					$tripIDs[]	=	'Trip_' . $recordset['tripID'];
			 					
			 					 
			 				}
			 				
			 				if ($this->cache != null){
			 					$key =	'Travel_Trips_Relation_' . $this->travelID . '_' . $hashKey;
			 					$keyForHashes =	'Travel_Trip_hashes_' . $this->travelID;
			 					$this->cache->set($key, $tripIDs, array('EXPIRE'=>86400));
			 					
			 					if ($hashes = $this->cache->get($keyForHashes)){
			 						$hashes[]	=	$key;
			 						$this->cache->set($keyForHashes,$hashes,array('EXPIRE'=>86400));
			 					} else {
			 						$this->cache->set($keyForHashes,array($key),array('EXPIRE'=>86400));
			 					}
			 				}
		 				}
						$this->arrtrip = $arrTrip;
		 			}
	 			}	

	 			return $this->arrtrip;
	 		}
	 		
	 		
	 		
	 	// static functions
	 		
	 		
	 		// get the travels given locationID of a country
	 		public static function getAllTravelsByCountryID($_locationID, $criteria = null){
	 			try {
					$mRs		= 	new Recordset(ConnectionProvider::instance()->getConnection());
				}
				catch (Exception $e) {				   
				   throw $e;
				}

				$strCriteria2 = (null != $criteria) ? $criteria->getCriteria2('AND') : '' ;

				// get locationID of cities in a country
				$sql = "SELECT DISTINCT locID " .
						"FROM " .
							"(" .
								"SELECT GoAbroad_Main.tbcity.locID " .
								"FROM GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry " .
							   	"WHERE GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID " .
								"AND GoAbroad_Main.tbcountry.approved = 1 " .
								"AND GoAbroad_Main.tbcountry.locID = " . $_locationID .
							   	" UNION " .
							   "SELECT tblNewCity.locID " .
								"FROM tblNewCity, GoAbroad_Main.tbcountry " .
							   	"WHERE tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID " .
								"AND GoAbroad_Main.tbcountry.approved = 1 " .
								"AND GoAbroad_Main.tbcountry.locID = " . $_locationID .
							") AS tblTemp";
				$mRs->Execute($sql);

				$arrlocID = array(); 
				while ( $row = mysql_fetch_array($mRs->Resultset()))
					$arrlocID[] = $row['locID'];

				$arrlocID[] = $_locationID;
				$arrlocstr  = implode(',',$arrlocID);

				// new code implements privacy preferences
				$sql2 = "SELECT tblTravel.travelID as travelID " .
						"FROM tblTravel,tblTrips, tblTravelLog " .
						"WHERE tblTravel.travelID = tblTrips.travelID " .
						"AND tblTravelLog.travelID = tblTravel.travelID " .
						"AND tblTravelLog.deleted = 0 " .
						"AND tblTravel.deleted = 0 " .
						"AND tblTravel.privacypref = 0 " .
						"AND tblTrips.locID IN ( ". $arrlocstr . " )"; 

				//if logged in
				if (isset($_SESSION['travelerID'])){
					//get journals of current traveler's friend's journals
					$unionStr1 = "SELECT tblTravel.travelID as travelID " .
								"FROM tblTravel, tblTravelLog, tblTrips, tblFriend , tblTravelLink " .
								"WHERE  tblTravel.travellinkID = tblTravelLink.travellinkID " .
								"AND tblTravelLink.reftype = 1 " .
								"AND tblTravelLink.refID = tblFriend.friendID " .
								"AND tblTravel.travelID = tblTrips.travelID " .
								"AND tblTrips.travelID = tblTravelLog.travelID " .
								"AND tblTravelLog.deleted = 0 " .
								"AND tblTravel.deleted = 0 " .
								"AND tblTrips.locID IN (". $arrlocstr . ") " .
								"AND tblFriend.travelerID = " . $_SESSION['travelerID'] . " " .
								"AND tblTravel.privacypref IN " .
									"(" .
										JournalPrivacyPreferenceType::$FRIENDS . "," .
										JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS .
									")";

					$unionStr2 = "SELECT tblTravel.travelID as travelID " .
								"FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink " .
								"WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
								"AND tblTravelLink.reftype = 2 " .
								"AND tblTravel.travelID = tblTrips.travelID " .
								"AND tblTrips.travelID = tblTravelLog.travelID " .
								"AND tblTravelLog.deleted = 0 " .
								"AND tblTravel.deleted = 0 " .
								"AND tblTravelLink.refID IN " .
									"(" .
										"SELECT groupID FROM tblGrouptoTraveler WHERE travelerID = " . $_SESSION['travelerID'] .
									") " .
							    "AND tblTravel.privacypref IN " .
									"(" .
										JournalPrivacyPreferenceType::$GROUP_MEMBERS . "," .
										JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS .
									") " .
								"AND tblTrips.locID IN ( ". $arrlocstr . " )";

					//require_once('travellog/model/Class.AdminGroup.php');
					$advGroupID = 0;//former AdminGroup::get.Advisor.GroupID($_SESSION['travelerID']);

					if (0 < $advGroupID) {
						// get your own journals as an advisor
						$unionStr3 = "SELECT tblTravel.travelID as travelID " .
									"FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink " .
									"WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
									"AND tblTravel.travelID = tblTrips.travelID " .
									"AND tblTrips.travelID = tblTravelLog.travelID " .
									"AND tblTravelLog.deleted = 0 " .
									"AND tblTravel.deleted = 0 " .
									"AND tblTrips.locID IN ( ". $arrlocstr . ") " .
									"AND tblTravelLink.reftype = 2 " .
									"AND " .
										"(" .
											"tblTravelLink.refID = " . $advGroupID . 
											" OR " .
											"tblTravelLink.refID IN " .
												"(" .
													"SELECT groupID FROM tblGroup WHERE parentID = " . $advGroupID .
												")" .
										")";
					}
					else {
						// get your own journals
						$unionStr3 = "SELECT tblTravel.travelID as travelID " .
									"FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink " .
									"WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
									"AND tblTravel.travelID = tblTrips.travelID " .
									"AND tblTrips.travelID = tblTravelLog.travelID " .
									"AND tblTravelLog.deleted = 0 " .
									"AND tblTravel.deleted = 0 " .
									"AND tblTravelLink.reftype = 1 " .
									"AND tblTravelLink.refID = "  . $_SESSION['travelerID'] . " " .									
									"AND tblTrips.locID IN (". $arrlocstr . ")";
					}
					$sql2 =  $sql2 . " UNION " . $unionStr1 . " UNION " . $unionStr2 . " UNION " . $unionStr3 ;
				}

				$sql = "SELECT * FROM " .
						"(" .
							"SELECT tblTravelLog.logdate as _date , tblTravel.*, tblTravelLog.publish as publishentry " . 
							"FROM tblTravel, tblTrips, tblTravelLog, tblTraveler " .
		 					"WHERE tblTrips.locID IN (" . $arrlocstr . ") " . 
							"AND tblTraveler.travelerID = tblTravel.travelerID " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblTravel.travelID = tblTravelLog.travelID " .
							"AND tblTravelLog.deleted = 0 " .
							"AND tblTravel.deleted = 0 " .
		 					"AND tblTrips.tripID = tblTravelLog.tripID " .
							"AND tblTravel.travelID IN (" . $sql2 . ")" . $strCriteria2 . " " .
							"ORDER BY _date DESC " .
						") as _tb " .
						"GROUP BY _tb.travelID " .
						"ORDER BY _tb._date DESC";
				
				$mRs->Execute($sql);

				$arrTravel = array();
				while ($recordset = mysql_fetch_array($mRs->Resultset())) {
					$mData = array();
					$mData["locID"]			= $recordset['locID'];
					$mData["votes"]			= $recordset['votes'];
					$mData["rating"]		= $recordset['rating'];
					$mData["viewed"]		= $recordset['viewed'];
					$mData["publish"]		= $recordset['publish'];
					$mData["travelID"]		= $recordset['travelID'];
					$mData["travelerID"]	= $recordset['travelerID'];
					$mData["isfeatured"]	= $recordset['isfeatured'];
					$mData["entryCount"]	= $recordset['entryCount'];
					$mData["privacypref"]	= $recordset['privacypref'];
					$mData["lastupdated"]	= $recordset['lastupdated'];
					$mData["primaryphoto"]	= $recordset['primaryphoto'];
					$mData["travellinkID"]	= $recordset['travellinkID'];
					$mData["title"]			= stripslashes($recordset['title']);
					$mData["description"]	= stripslashes($recordset['description']);

					$arrTravel[] = new Travel(0, $mData);
				}

				return $arrTravel;
	 		}

	 		/*
	 		// get the travels in specified location
	 		public static function getTravelsInLocation($_locationID){
	 			try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {
				   throw $e;
				}

		 		$sql = "SELECT tblTravel.* " .
						"FROM tblTravel, tblTrips, tblTravelLog " .
						"WHERE tblTravel.travelID = tblTrips.travelID " .
						"AND tblTrips.tripID = tblTravelLog.tripID " .
						"AND tblTrips.locID = " . $_locationID;		// tblTravel.locID dinhi before, ginchange ko to trips
 				$rs->Execute($sql);
	 			
	 			$arrTravel = array();
	 			if (0 < $rs->Recordcount()){
		 			while ($recordset = mysql_fetch_array($rs->Resultset())) {
		 				try {
	 						$travelsinlocation = new Travel (0, $recordset); 												
	 					}
						catch (Exception $e) {
						   throw $e;						   
						}						
	 					$arrTravel[] = $travelsinlocation;
	 				}
 				}
 				return $arrTravel;		 				
	 		}
	 		*/

	 		// get the photos in specified location
	 		public static function getPhotosInLocation($_locationID){
 				try {
					$mRs   = new Recordset(ConnectionProvider::instance()->getConnection());
	 			}
				catch (Exception $e) {	   
				   throw $e;
				}

 				$arrphotos = array();
	 			$mLocation  = LocationFactory::instance()->create($_locationID);

	 		 	if ( !strcasecmp(get_class($mLocation), "Country") ){	 		 
		 			foreach($mLocation->getCities() as $city) {
				 		$mLocId = $city->getlocationID();

						$sql = "SELECT tblTravelLogtoPhoto.photoID, tblTravelLogtoPhoto.travellogID, tblTravelLog.*, " .
									"tblTravel.travelerID, tblTravel.travellinkID " .
								"FROM tblTrips , tblTravelLog, tblTravelLogtoPhoto, tblTravel " .
								"WHERE  tblTrips.tripID = tblTravelLog.tripID " .
								"AND tblTravelLog.deleted = 0 " .
								"AND tblTravel.deleted = 0 " .
								"AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID " .
								"AND tblTravel.travelID = tblTravelLog.travelID " .
								"AND tblTrips.locID = " . $mLocId;
						
 						$mRs->Execute($sql);

						if (0 < $mRs->Recordcount()){
							$mTravelLogID = 0;
							while ($recordset = mysql_fetch_array($mRs->Resultset())) {
								try {
									if ( $recordset['travellogID'] != $mTravelLogID)
										$mTravelLog = new TravelLog(0, $recordset);
						 			$mPhotos = new Photo($mTravelLog, $recordset['photoID']);
									$mTravelLogID = $recordset['travellogID'];
						 		} 					
								catch (Exception $e) {
								   throw $e;
								}						
						 		$arrphotos[] = $mPhotos;
						 	}
					 	}
					}
	 		 	}

 				return $arrphotos;
		 	}
	 		
	 		// get travels based on filter conditions
	 		public static function getFilteredTravels($_filtercriteria = null, $_rowslimit = null, $_criteria = null){	 			
	 			try {
					$mRs	= new Recordset(ConnectionProvider::instance()->getConnection());
					$mRs2	= new Recordset(ConnectionProvider::instance()->getConnection());
	 			}
				catch (Exception $e) {
				   throw $e;
				}

				$mCondition	= '';
				$mLimitStr	= '';
				$arrFilteredTravels	= array();
				$strCriteria2 = (null != $_criteria) ? $_criteria->getCriteria2('AND') : '' ;

				if ($_filtercriteria) {
					$myconditions = $_filtercriteria->getConditions();	 			
	 				if ($myconditions) {
			 			$attname = $myconditions[0]->getAttributeName();
		 				$operation = $myconditions[0]->getOperation();
		 				
			 			switch ($operation){
			 				case FilterOp::$ORDER_BY :
			 					$mCondition = " ORDER BY tblTravel." . $attname ;
			 					break;
			 				case FilterOp::$ORDER_BY_DESC :
			 					$mCondition = " ORDER BY tblTravel." . $attname . " DESC " ;
			 					break;
			 			}
	 				}
				}
				
		 		if ($_rowslimit){
		 			$offset = $_rowslimit->getOffset();
		 			$rows =  $_rowslimit->getRows();
		 			$mLimitStr = " 	LIMIT " . $offset . " , " . $rows;
		 		}

				// new code implements privacy preferences
				$sql = "SELECT tblTravel.travelerID, tblTravel.travelID, tblTravel.locID, tblTravel.title, tblTravelLog.deleted, " .
							"tblTravel.description, rating, votes, viewed, tblTravel.publish, primaryphoto, privacypref, " .
							"tblTravel.isfeatured, entryCount, tblTravel.travellinkID, tblTravel.lastupdated " .
						"FROM tblTravel, tblTrips, tblTravelLog " .
						"WHERE  tblTravel.travelID = tblTrips.travelID " .
						"AND  tblTrips.tripID = tblTravelLog.tripID " .
						"AND tblTravelLog.deleted = 0 " .
						"AND tblTravel.deleted = 0 " .
						"AND tblTravel.privacypref = 0 ";

				//if logged in
				if (isset($_SESSION['travelerID'])){					
					//get journals of current traveler's friend's journals
					$unionStr1 = "SELECT tblTravel.travelerID, tblTravel.travelID, tblTravel.locID, tblTravel.title,tblTravelLog.deleted, " .
									"tblTravel.description, rating, votes, viewed, tblTravel.publish, primaryphoto, privacypref, " .
									"tblTravel.isfeatured, entryCount, tblTravel.travellinkID, tblTravel.lastupdated " .
								"FROM tblTravel, tblTrips, tblTravelLog, tblFriend , tblTravelLink " .
								"WHERE  tblTravel.travellinkID = tblTravelLink.travellinkID " .
								"AND tblTravel.travelID = tblTrips.travelID " .
								"AND  tblTrips.tripID = tblTravelLog.tripID " .
								"AND tblTravelLog.deleted = 0 " .
								"AND tblTravel.deleted = 0 " .
								"AND tblTravelLink.refID = tblFriend.friendID " .
								"AND tblTravelLink.reftype = 1 " .
								"AND tblFriend.travelerID = " . $_SESSION['travelerID'] . " " .
								"AND tblTravel.privacypref IN " .
									"(" .
										JournalPrivacyPreferenceType::$FRIENDS . "," .
										JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS .
									")";

					//get journals of current traveler's groups
					$unionStr2 = "SELECT tblTravel.travelerID, tblTravel.travelID, tblTravel.locID, tblTravel.title,tblTravelLog.deleted, " .
									"tblTravel.description, rating, votes, viewed, tblTravel.publish, primaryphoto, privacypref, " .
									"tblTravel.isfeatured, entryCount, tblTravel.travellinkID, tblTravel.lastupdated " .
								"FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink " .
								"WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
								"AND tblTravel.travelID = tblTrips.travelID " .
								"AND  tblTrips.tripID = tblTravelLog.tripID " .
								"AND tblTravelLog.deleted = 0 " .
								"AND tblTravel.deleted = 0 " .
								"AND tblTravelLink.reftype = 2 " .
								"AND tblTravelLink.refID IN " .
									"(" .
										"SELECT groupID FROM tblGrouptoTraveler WHERE travelerID = " . $_SESSION['travelerID'] .
									") " . 
							    "AND tblTravel.privacypref IN " .
									"(" .
										JournalPrivacyPreferenceType::$GROUP_MEMBERS . "," .
										JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS .
									")";
					
					//require_once('travellog/model/Class.AdminGroup.php');
					$advGroupID = 0;//former AdminGroup::get.Advisor.GroupID($_SESSION['travelerID']);
					
					if (0 < $advGroupID) {
						// get your own journals as an advisor
						$unionStr3 = "SELECT tblTravel.travelerID, tblTravel.travelID, tblTravel.locID, tblTravel.title,tblTravelLog.deleted, " .
										"tblTravel.description, rating, votes, viewed, tblTravel.publish, primaryphoto, privacypref, " .
										"tblTravel.isfeatured, entryCount, tblTravel.travellinkID, tblTravel.lastupdated " .
									"FROM tblTravel, tblTrips, tblTravelLog, tblTravelLink " .
									"WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
									"AND tblTravel.travelID = tblTrips.travelID " .
									"AND  tblTrips.tripID = tblTravelLog.tripID " .
									"AND tblTravelLog.deleted = 0 " .
									"AND tblTravel.deleted = 0 " .
									"AND tblTravelLink.reftype = 2 " .
									"AND " .
										"(" .
											"tblTravelLink.refID = " . $advGroupID . 
											" OR " .
											"tblTravelLink.refID IN " .
												"(" .
													"SELECT groupID FROM tblGroup WHERE parentID = " . $advGroupID .
												")" .
										")";
						
					}
					else {					
						// get your own journals
						$unionStr3 = "SELECT tblTravel.travelerID, tblTravel.travelID, tblTravel.locID, tblTravel.title, tblTravelLog.deleted," .
										"tblTravel.description, rating, votes, viewed, tblTravel.publish, primaryphoto, privacypref, " .
										"tblTravel.isfeatured, entryCount, tblTravel.travellinkID, tblTravel.lastupdated " .
									"FROM tblTravel, tblTrips , tblTravelLog, tblTravelLink " .
									"WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
									"AND tblTravel.travelID = tblTrips.travelID	" .
									"AND  tblTrips.tripID = tblTravelLog.tripID " .
									"AND tblTravelLog.deleted = 0 " .
									"AND tblTravel.deleted = 0 " .
									"AND tblTravelLink.reftype = 1 " .
									"AND tblTravelLink.refID = " . $_SESSION['travelerID'];
					}										
					$sql =  $sql . " UNION " . $unionStr1 . " UNION " . $unionStr2 . " UNION " . $unionStr3;					 
				}
			
				$sql = "SELECT SQL_CALC_FOUND_ROWS tblTravel.travelerID, tblTravel.travelID, tblTravel.locID, tblTravel.title,tblTravelLog.deleted, " .
								"tblTravel.description, rating, votes, viewed, tblTravel.publish, primaryphoto, privacypref, " .
								"tblTravel.isfeatured, entryCount, tblTravel.travellinkID, tblTravel.lastupdated " .
						" FROM ( " . $sql . " ) as tblTravel, tblTravelLog, tblTraveler " .
						" WHERE tblTravel.travelID = tblTravelLog.travelID AND  tblTravelLog.deleted = 0 " .
						" AND tblTravel.travelerID = tblTraveler.travelerID " .
						" AND tblTraveler.isSuspended = 0 " .
						" AND tblTravel.deleted = 0 " . $strCriteria2 . " " .
						"GROUP BY tblTravel.travelID " . $mCondition . $mLimitStr;
				$mRs->Execute($sql);
		 		
		 		$sql2 = "SELECT FOUND_ROWS() as totalrec";
		 		$mRs2->Execute($sql2);
				

		 		Travel::$statvar = $mRs2->Result(0, "totalrec");		// set recordcount to a static variable
 				while ($recordset = mysql_fetch_array($mRs->Resultset())){					
	 				try {
 						$filteredTravel	= new Travel(0, $recordset);
 					}
					catch (Exception $e) {
					   throw $e;
					}
 					$arrFilteredTravels[] = $filteredTravel;
 				}

 				return $arrFilteredTravels; 				
	 		}
	
			public static function getFilteredTravelsAndArticles($_filtercriteria = null, $_rowslimit = null, $_criteria = null){	 			
				try {
		 			$mConn	= new Connection();
					$mRs	= new Recordset($mConn);
					$mRs2	= new Recordset($mConn);
	 			}
				catch (Exception $e) {
				   throw $e;
				}

				$mCondition	= '';
				$mLimitStr	= '';
				$arrFilteredTravels	= array();
				$strCriteria2 = (null != $_criteria) ? $_criteria->getCriteria2('AND') : '' ;

				if ($_filtercriteria) {
					$myconditions = $_filtercriteria->getConditions();	 			
	 				if ($myconditions) {
			 			$attname = $myconditions[0]->getAttributeName();
		 				$operation = $myconditions[0]->getOperation();
		 				
			 			switch ($operation){
			 				case FilterOp::$ORDER_BY :
			 					$mCondition = " ORDER BY " . $attname ;
			 					break;
			 				case FilterOp::$ORDER_BY_DESC :
			 					$mCondition = " ORDER BY " . $attname . " DESC " ;
			 					break;
			 			}
	 				}
				}
				
		 		if ($_rowslimit){
		 			$offset = $_rowslimit->getOffset();
		 			$rows =  $_rowslimit->getRows();
		 			$mLimitStr = " 	LIMIT " . $offset . " , " . $rows;
		 		}
				// get travels	
				// new code implements privacy preferences
				$asql = "SELECT tblArticle.authorID, " . 
							"tblArticle.articleID, " . 
							"tblArticle.locID, " . 
							"tblArticle.title, " . 
							"tblArticle.description, " . 
							"0 as primaryphoto, " . 
							"0 as preference, " . 
							"tblArticle.lastedited as lastupdated, " . 							
							"tblArticle.logdate as _date, " . 							
							"'ARTICLE' as type, " .
							"tblArticle.deleted, " .
							"1 as publish, " .
							"1 as entryCount, " .
							"0 as views, " . 
							"tblArticle.groupID as travellinkID " .
						" FROM tblArticle, tblTraveler " .
						" WHERE tblArticle.deleted = 0 " .
						" AND tblArticle.authorID = tblTraveler.travelerID " .
						" AND tblTraveler.isSuspended = 0 " . 
						" GROUP BY tblArticle.articleID";
				
				$sql = "SELECT SQL_CALC_FOUND_ROWS tblTravel.travelerID, " . 
							"tblTravel.travelID as ID, " . 
							"tblTravel.locID, " . 
							"tblTravel.title, " . 
							"tblTravel.description, " . 
							"primaryphoto, " . 
							"privacypref, " . 
							"tblTravel.lastupdated as lastupdated, " .		
							"tblTravelLog.logdate as _date, " .		
							"'JOURNAL' as type, " . 
							"tblTravel.deleted, " .
							"tblTravel.publish, " .
							"tblTravel.entryCount, " .
							"tblTravel.viewed as views, " . 
							"tblTravel.travellinkID " .
						" FROM tblTravel, tblTravelLog, tblTraveler, tblTrips " .
						" WHERE tblTravel.travelID = tblTravelLog.travelID " . 
							" AND tblTravelLog.tripID = tblTrips.tripID " . 
							" AND tblTravelLog.deleted = 0 " .
							" AND tblTravel.deleted = 0 " .
							" AND tblTravel.privacypref = 0 " . 
							" AND tblTravel.travelerID = tblTraveler.travelerID " .
							" AND tblTraveler.isSuspended = 0 " .
							$strCriteria2 . " " .
							" GROUP BY ID ". 
							" UNION ". $asql . " " . 
							" " . $mCondition . $mLimitStr;
				
				$mRs->Execute($sql);
		 		
		 		$sql2 = "SELECT FOUND_ROWS() as totalrec";
		 		$mRs2->Execute($sql2);
				
				require_once("travellog/vo/Class.JournalsArticlesVO.php");
				require_once("travellog/model/Class.Article.php");
		 		Travel::$statvar = $mRs2->Result(0, "totalrec");		// set recordcount to a static variable
 				while ($recordset = mysql_fetch_assoc($mRs->Resultset())){					
	 				try {
 						$filteredTravel	= new JournalsArticlesVO();
						$filteredTravel->setTravelerID($recordset['travelerID']);
						$filteredTravel->setTravelID($recordset['ID']);
						$filteredTravel->setLocID($recordset['locID']);
						$filteredTravel->setTitle($recordset['title']);
						$filteredTravel->setDescription($recordset['description']);
						$filteredTravel->setPrimaryphoto($recordset['primaryphoto']);
						$filteredTravel->setPrivacyPref($recordset['privacypref']);
						$filteredTravel->setDate($recordset['_date']);
						$filteredTravel->setType($recordset['type']); 
						$filteredTravel->setPublish($recordset['publish']);
						$filteredTravel->setEntryCount($recordset['entryCount']);
						$filteredTravel->setViews($recordset['views']);
						if($filteredTravel->getType() == "JOURNAL"){
							$travel = new Travel(0, 
								array( 
									"locID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"rating" => NULL,
									"votes" => NULL,
									"viewed" => NULL,
									'lastupdated' => NULL,
									"primaryphoto" => NULL,
									"privacypref" => NULL,
									"publish" => NULL,
									"isfeatured" => NULL,
									"entryCount"  => NULL,
									"travelerID" => NULL,
									"travellinkID" => $recordset['travellinkID'], 
									"travelID" => $recordset['ID']
								)
							);
							$filteredTravel->setOwner($travel->getOwner());
						}else{
							$article = new Article(0, 
								array(
									"articleID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"callout" => NULL,
									"locID" => NULL,
									"editorsNote" => NULL,
									"articledate" => NULL,
									"logdate" => NULL,
									"lastedited" => NULL,
									"isfeatured" => NULL,
									"publish" => NULL,
									"deleted" => NULL,
									"groupID" => $recordset['travellinkID'], 
									"authorID" => $recordset['travelerID'])
								);
							$filteredTravel->setOwner($article->getOwner());
						}
					}
					catch (Exception $e) {
					   throw $e;
					}
 					$arrFilteredTravels[] = $filteredTravel;
 				}

 				return $arrFilteredTravels; 				
	 		}
	 		
			public static function getAllTravelsAndArticlesByCountryID($_locationID, $criteria = null){
				try {
					$mConn	= new Connection();
					$mRs		= new Recordset($mConn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}

				$strCriteria2 = (null != $criteria) ? $criteria->getCriteria2('AND') : '' ;

				// get locationID of cities in a country
				$sql = "SELECT DISTINCT locID " .
						"FROM " .
							"(" .
								"SELECT GoAbroad_Main.tbcity.locID " .
								"FROM GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry " .
							   	"WHERE GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID " .
								"AND GoAbroad_Main.tbcountry.approved = 1 " .
								"AND GoAbroad_Main.tbcountry.locID = " . $_locationID .
							   	" UNION " .
							   "SELECT tblNewCity.locID " .
								"FROM tblNewCity, GoAbroad_Main.tbcountry " .
							   	"WHERE tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID " .
								"AND GoAbroad_Main.tbcountry.approved = 1 " .
								"AND GoAbroad_Main.tbcountry.locID = " . $_locationID .
							") AS tblTemp";
				$mRs->Execute($sql);

				$arrlocID = array(); 
				while ( $row = mysql_fetch_array($mRs->Resultset()))
					$arrlocID[] = $row['locID'];

				$arrlocID[] = $_locationID;
				$arrlocstr  = implode(',',$arrlocID);

				$asql = "SELECT tblArticle.authorID, " . 
							"tblArticle.articleID, " . 
							"tblArticle.locID, " . 
							"tblArticle.title, " . 
							"tblArticle.description, " . 
							"0 as primaryphoto, " . 
							"0 as preference, " . 
							"tblArticle.lastedited as lastupdated, " .
							"tblArticle.logdate as _date, " . 							
							"'ARTICLE' as type, " .
							"tblArticle.deleted, " .
							"1 as publish, " .
							"1 as entryCount, " .
							"0 as views, " .
							"tblArticle.groupID as travellinkID " .
						" FROM tblArticle, tblTraveler " .
						" WHERE tblArticle.deleted = 0 " .
						" AND tblArticle.authorID = tblTraveler.travelerID " .
						" AND tblTraveler.isSuspended = 0 " . 
						" AND tblArticle.locID IN ( " .$arrlocstr. " ) " . 
						" GROUP BY tblArticle.articleID";
				
				$sql = 	"SELECT tblTravel.travelerID, " . 
							"tblTravel.travelID as ID, " . 
							"tblTravel.locID, " . 
							"tblTravel.title, " . 
							"tblTravel.description, " . 
							"primaryphoto, " . 
							"privacypref, " . 
							"tblTravel.lastupdated as lastupdated, " .
							"tblTravelLog.logdate as _date, " .
							"'JOURNAL' as type, " . 
							"tblTravel.deleted, " .
							"tblTravel.publish, " .
							"tblTravel.entryCount, " .
							"tblTravel.viewed as views, " .
							"tblTravel.travellinkID " .
						"FROM tblTravel, tblTrips, tblTravelLog, tblTraveler " .
		 				"WHERE tblTrips.locID IN (" . $arrlocstr . ") " . 
							"AND tblTraveler.travelerID = tblTravel.travelerID " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblTravel.travelID = tblTravelLog.travelID " .
							"AND tblTravelLog.deleted = 0 " .
							"AND tblTravel.deleted = 0 " .
		 					"AND tblTrips.tripID = tblTravelLog.tripID " . 
							"GROUP BY ID " . 
							"UNION " . $asql . 
							$strCriteria2 . " " .
							"ORDER BY lastupdated DESC ";
				
				$mRs->Execute($sql);
				
				require_once("travellog/vo/Class.JournalsArticlesVO.php");
				require_once("travellog/model/Class.Article.php");
				
				$arrTravel = array();
				while ($recordset = mysql_fetch_assoc($mRs->Resultset())){					
	 				try {
 						$filteredTravel	= new JournalsArticlesVO();
						$filteredTravel->setTravelerID($recordset['travelerID']);
						$filteredTravel->setTravelID($recordset['ID']);
						$filteredTravel->setLocID($recordset['locID']);
						$filteredTravel->setTitle($recordset['title']);
						$filteredTravel->setDescription($recordset['description']);
						$filteredTravel->setPrimaryphoto($recordset['primaryphoto']);
						$filteredTravel->setPrivacyPref($recordset['privacypref']);
						$filteredTravel->setDate($recordset['_date']);
						$filteredTravel->setType($recordset['type']); 
						$filteredTravel->setPublish($recordset['publish']);
						$filteredTravel->setEntryCount($recordset['entryCount']);
						$filteredTravel->setViews($recordset['views']);
						if($filteredTravel->getType() == "JOURNAL"){
							$travel = new Travel(0, 
								array( 
									"locID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"rating" => NULL,
									"votes" => NULL,
									"viewed" => NULL,
									'lastupdated' => NULL,
									"primaryphoto" => NULL,
									"privacypref" => NULL,
									"publish" => NULL,
									"isfeatured" => NULL,
									"entryCount"  => NULL,
									"travelerID" => NULL,
									"travellinkID" => $recordset['travellinkID'], 
									"travelID" => $recordset['ID']
								)
							);
							$filteredTravel->setOwner($travel->getOwner());
						}else{
							$article = new Article(0, 
								array(
									"articleID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"callout" => NULL,
									"locID" => NULL,
									"editorsNote" => NULL,
									"articledate" => NULL,
									"logdate" => NULL,
									"lastedited" => NULL,
									"isfeatured" => NULL,
									"publish" => NULL,
									"deleted" => NULL,
									"groupID" => $recordset['travellinkID'], 
									"authorID" => $recordset['travelerID'])
								);
							$filteredTravel->setOwner($article->getOwner());
						}
					}
					catch (Exception $e) {
					   throw $e;
					}
 					$arrTravel[] = $filteredTravel;
 				}
				return $arrTravel;
			}
			
			// actually we have tags for articles only.. just in case :D
			public static function getAllTravelsAndArticlesByTag($_tag, $criteria = null){
				try {
					$mConn = new Connection();
					$mRs = new Recordset($mConn);
					$mRs2 = new Recordset($mConn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}

				if(!is_null($criteria)){
					$start = $criteria['START'];
					$end = $criteria['END'];
				}				

				$asql = "SELECT SQL_CALC_FOUND_ROWS tblArticle.authorID as travelerID, " . 
							"tblArticle.articleID as ID, " . 
							"tblArticle.locID, " . 
							"tblArticle.title, " . 
							"tblArticle.description, " . 
							"0 as primaryphoto, " . 
							"0 as privacypref, " . 
							"tblArticle.lastedited as lastupdated, " .
							"tblArticle.logdate as _date, " .
							"'ARTICLE' as type, " .
							"tblArticle.deleted, " .
							"1 as publish, " .
							"1 as entryCount, " .
							"0 as views, " . 
							"tblArticle.groupID as travellinkID, " .
							"0 as t_ID " .
						" FROM tblArticle, tblTraveler, tblTags " .
						" WHERE tblTags.context = 0 " . 
						" AND tblTags.contextID = tblArticle.articleID " .
						" AND tblTags.tag = '".addslashes($_tag)."'" .
						" AND tblArticle.authorID = tblTraveler.travelerID " .
						" AND tblArticle.deleted = 0 " .
						" AND tblTraveler.isSuspended = 0 " . 
						" GROUP BY tblArticle.articleID " .
						"ORDER BY CASE WHEN lastupdated = '0000-00-00 00:00:00' THEN _date ELSE lastupdated END DESC ";
				
				if(isset($start) && isset($end))		
					$asql .= "LIMIT $start,$end";
				
				$mRs->Execute($asql);
				
				$mRs2->Execute('SELECT FOUND_ROWS() as totalrec');
				$row = mysql_fetch_assoc($mRs2->Resultset());
				Travel::$statvar = $mRs2->Result(0, "totalrec");
				
				require_once("travellog/vo/Class.JournalsArticlesVO.php");
				require_once("travellog/model/Class.Article.php");
				
				$arrTravel = array();
				while ($recordset = mysql_fetch_assoc($mRs->Resultset())){					
	 				try {
 						$filteredTravel	= new JournalsArticlesVO();
						$filteredTravel->setTravelerID($recordset['travelerID']);
						$filteredTravel->setTravelID($recordset['ID']);
						$filteredTravel->setID($recordset['t_ID']);
						$filteredTravel->setLocID($recordset['locID']);
						$filteredTravel->setTitle($recordset['title']);
						$filteredTravel->setDescription($recordset['description']);
						$filteredTravel->setPrimaryphoto($recordset['primaryphoto']);
						$filteredTravel->setPrivacyPref($recordset['privacypref']);
						$filteredTravel->setDate($recordset['_date']);
						$filteredTravel->setType($recordset['type']); 
						$filteredTravel->setPublish($recordset['publish']);
						$filteredTravel->setEntryCount($recordset['entryCount']);
						$filteredTravel->setViews($recordset['views']);
						if($filteredTravel->getType() == "JOURNAL"){
							$travel = new Travel(0, 
								array( 
									"locID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"rating" => NULL,
									"votes" => NULL,
									"viewed" => NULL,
									'lastupdated' => NULL,
									"primaryphoto" => NULL,
									"privacypref" => NULL,
									"publish" => NULL,
									"isfeatured" => NULL,
									"entryCount"  => NULL,
									"travelerID" => NULL,
									"travellinkID" => $recordset['travellinkID'], 
									"travelID" => $recordset['ID']
								)
							);
							$filteredTravel->setOwner($travel->getOwner());
						}else{
							$article = new Article(0, 
								array(
									"articleID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"callout" => NULL,
									"locID" => NULL,
									"editorsNote" => NULL,
									"articledate" => NULL,
									"logdate" => NULL,
									"lastedited" => NULL,
									"isfeatured" => NULL,
									"publish" => NULL,
									"deleted" => NULL,
									"groupID" => $recordset['travellinkID'], 
									"authorID" => $recordset['travelerID'])
								);
							$filteredTravel->setOwner($article->getOwner());
						}
					}
					catch (Exception $e) {
					   throw $e;
					}
 					$arrTravel[] = $filteredTravel;
 				}
				
				return $arrTravel;
			}
			
			public static function getEntriesAndArticles($cri=null, $countOnly=false){
				try {
					$mConn = new Connection();
					$mRs = new Recordset($mConn);
					$mRs2 = new Recordset($mConn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}

				if(!is_null($cri)){
					$start = $cri['START'];
					$end = $cri['END'];
				}
				
				$esql = 	"SELECT tblTravel.travelerID, " . 
							"tblTravelLog.travelLogID as ID, " . 
							"tblTravel.locID, " . 
							"tblTravelLog.title, " . 
							"tblTravelLog.description, " . 
							"tblTravelLog.lastedited as lastupdated, " .
							"tblTravelLog.logdate as _date, " .
							"tblTravelLog.tripID as trip, " . 
							"'JOURNAL' as type, " . 
							"tblTravel.viewed as views, " . 
							"tblTravel.travellinkID, " .
							"tblTravel.travelID as t_ID " .
						"FROM tblTravel, tblTrips, tblTravelLog, tblTraveler " .
		 				"WHERE tblTraveler.travelerID = tblTravel.travelerID " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblTraveler.deactivated = 0 " .
							"AND tblTravel.travelID = tblTravelLog.travelID " .
							"AND tblTravelLog.deleted = 0 " .
							"AND tblTravel.deleted = 0 " .
		 					"AND tblTrips.tripID = tblTravelLog.tripID " . 
							"GROUP BY ID ";
				
				$asql = "SELECT tblArticle.authorID, " . 
							"tblArticle.articleID, " . 
							"tblArticle.locID, " . 
							"tblArticle.title, " . 
							"tblArticle.description, " . 
							"tblArticle.lastedited as lastupdated, " .
							"tblArticle.logdate as _date, " . 							
							"0 as trip, " . 
							"'ARTICLE' as type, " . 
							"0 as views, " .
							"tblArticle.groupID as travellinkID, " .
							"0 as t_ID " .
						" FROM tblArticle, tblTraveler " .
						" WHERE tblArticle.deleted = 0 " .
						" AND tblArticle.authorID = tblTraveler.travelerID " .
						" AND tblTraveler.isSuspended = 0 " . 
						" AND tblTraveler.deactivated = 0 " .
						" GROUP BY tblArticle.articleID";
						
				$sql = 	"SELECT SQL_CALC_FOUND_ROWS tbl.travelerID, " . 
								"tbl.ID, " . 
								"tbl.locID, " . 
								"tbl.title, " . 
								"tbl.description, " . 
								"tbl.lastupdated, " .
								"tbl._date, " .
								"tbl.trip, " . 
								"tbl.type, " . 
								"tbl.views, " .
								"tbl.travellinkID, " .
								"tbl.t_ID " .
							"FROM ($esql UNION $asql ORDER BY CASE WHEN lastupdated = '0000-00-00 00:00:00' " . 
								"THEN _date " . 
								"ELSE lastupdated " . 
							"END DESC) as tbl ";

				
				if(isset($start) && isset($end))		
					$sql .= "LIMIT $start,$end";							
				
				$mRs->Execute($sql);
				
				$mRs2->Execute('SELECT FOUND_ROWS() as totalrec');
				if( $countOnly ){
					$row = mysql_fetch_assoc($mRs2->Resultset());
					return $row["totalrec"];
				}
				$row = mysql_fetch_assoc($mRs2->Resultset());
				Travel::$statvar = $mRs2->Result(0, "totalrec");
				
				require_once("travellog/vo/Class.JournalsArticlesVO.php");
				require_once("travellog/model/Class.Article.php");
				
				$arrTravel = array();
				while ($recordset = mysql_fetch_assoc($mRs->Resultset())){					
	 				try {
 						$filteredTravel	= new JournalsArticlesVO();
						$filteredTravel->setTravelerID($recordset['travelerID']);
						$filteredTravel->setTravelID($recordset['ID']);
						$filteredTravel->setID($recordset['t_ID']);
						$filteredTravel->setLocID($recordset['locID']);
						$filteredTravel->setTitle($recordset['title']);
						$filteredTravel->setDescription($recordset['description']);
						$filteredTravel->setDate($recordset['_date']);
						$filteredTravel->setType($recordset['type']); 
						$filteredTravel->setTrip(new Trip($recordset['trip']));
						$filteredTravel->setViews($recordset['views']);
						if($filteredTravel->getType() == "JOURNAL"){
							$travel = new Travel(0, 
								array( 
									"locID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"rating" => NULL,
									"votes" => NULL,
									"viewed" => NULL,
									'lastupdated' => NULL,
									"primaryphoto" => NULL,
									"privacypref" => NULL,
									"publish" => NULL,
									"isfeatured" => NULL,
									"entryCount"  => NULL,
									"travelerID" => NULL,
									"travellinkID" => $recordset['travellinkID'], 
									"travelID" => $recordset['ID']
								)
							);
							$filteredTravel->setOwner($travel->getOwner());
						}else{
							$article = new Article(0, 
								array(
									"articleID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"callout" => NULL,
									"locID" => NULL,
									"editorsNote" => NULL,
									"articledate" => NULL,
									"logdate" => NULL,
									"lastedited" => NULL,
									"isfeatured" => NULL,
									"publish" => NULL,
									"deleted" => NULL,
									"groupID" => $recordset['travellinkID'], 
									"authorID" => $recordset['travelerID'])
								);
							$filteredTravel->setOwner($article->getOwner());
						}
					}
					catch (Exception $e) {
					   throw $e;
					}
 					$arrTravel[] = $filteredTravel;
 				}
				return $arrTravel;
			}
			
			public static function getEntriesAndArticlesByCountryID($_locationID, $cri = null){
				try {
					$mConn = new Connection();
					$mRs = new Recordset($mConn);
					$mRs2 = new Recordset($mConn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}

				if(!is_null($cri)){
					$start = $cri['START'];
					$end = $cri['END'];
				}
				/*
				// if search by location
				$sql = "SELECT DISTINCT locID " .
						"FROM " .
							"(" .
								"SELECT GoAbroad_Main.tbcity.locID " .
								"FROM GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry " .
							   	"WHERE GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID " .
								"AND GoAbroad_Main.tbcountry.approved = 1 " .
								"AND GoAbroad_Main.tbcountry.locID = " . $_locationID .
							   	" UNION " .
							   "SELECT tblNewCity.locID " .
								"FROM tblNewCity, GoAbroad_Main.tbcountry " .
							   	"WHERE tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID " .
								"AND GoAbroad_Main.tbcountry.approved = 1 " .
								"AND GoAbroad_Main.tbcountry.locID = " . $_locationID .
							") AS tblTemp";
				$mRs->Execute($sql);

				$arrlocID = array(); 
				while ( $row = mysql_fetch_array($mRs->Resultset()))
					$arrlocID[] = $row['locID'];

				$arrlocID[] = $_locationID;
				$arrlocstr  = implode(',',$arrlocID);
				
				$esql = 	"SELECT tblTravel.travelerID, " . 
							"tblTravelLog.travelLogID as ID, " . 
							"tblTravel.locID, " . 
							"tblTravelLog.title, " . 
							"tblTravelLog.description, " . 
							"tblTravelLog.lastedited as lastupdated, " .
							"tblTravelLog.logdate as _date, " .
							"tblTravelLog.tripID as trip, " . 
							"'JOURNAL' as type, " . 
							"tblTravel.viewed as views, " . 
							"tblTravel.travellinkID, " .
							"tblTravel.travelID as t_ID " .
						"FROM tblTravel, tblTrips, tblTravelLog, tblTraveler " .
		 				"WHERE tblTraveler.travelerID = tblTravel.travelerID " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblTraveler.deactivated = 0 " .
							"AND tblTravel.travelID = tblTravelLog.travelID " .
							"AND tblTravelLog.deleted = 0 " .
							"AND tblTravel.deleted = 0 " .
		 					"AND tblTrips.tripID = tblTravelLog.tripID " . 
							"AND tblTrips.locID IN (" . $arrlocstr . ") " . 
							"GROUP BY ID ";
				
				$asql = "SELECT tblArticle.authorID, " . 
							"tblArticle.articleID, " . 
							"tblArticle.locID, " . 
							"tblArticle.title, " . 
							"tblArticle.description, " . 
							"tblArticle.lastedited as lastupdated, " .
							"tblArticle.logdate as _date, " . 							
							"0 as trip, " . 
							"'ARTICLE' as type, " .
							"0 as views, " .
							"tblArticle.groupID as travellinkID, " .
							"0 as t_ID " .
						" FROM tblArticle, tblTraveler " .
						" WHERE tblArticle.deleted = 0 " .
						" AND tblArticle.authorID = tblTraveler.travelerID " .
						" AND tblTraveler.isSuspended = 0 " . 
						" AND tblTraveler.deactivated = 0 " .
						" AND tblArticle.locID IN (" . $arrlocstr . ") " . 
						" GROUP BY tblArticle.articleID";*/


				$esql = <<<EOF

SELECT 
  tblTravel.travelerID,
  tblTravelLog.travelLogID as ID,
  tblTravel.locID,
  tblTravelLog.title,
  tblTravelLog.description,  
  tblTravelLog.lastedited as lastupdated,
  tblTravelLog.logdate as _date,
  tblTravelLog.tripID as trip,
  'JOURNAL' as type, 
  tblTravel.viewed as views, 
  tblTravel.travellinkID, 
  tblTravel.travelID as t_ID 


FROM 

GoAbroad_Main.tbcountry 

 LEFT JOIN(
    SELECT  cityID, countryID, city , tblNewCity.locID as cityLoc, tblTrips.locID as tripLoc , tblTrips.tripID
    FROM tblTrips JOIN tblNewCity  ON (tblTrips.locID = tblNewCity.locID )
    UNION
    SELECT cityID, countryID, city, GoAbroad_Main.tbcity.locID as cityLoc, tblTrips.locID as tripLoc, tblTrips.tripID
    FROM tblTrips JOIN  GoAbroad_Main.tbcity ON (tblTrips.locID  = GoAbroad_Main.tbcity.locID  )
   ) city
  ON (GoAbroad_Main.tbcountry.countryID = city.countryID)

JOIN tblTravelLog 
  ON (city.tripID = tblTravelLog.tripID  )
JOIN tblTravel
  ON (tblTravelLog.travelID = tblTravel.travelID)
JOIN tblTraveler
  ON (tblTravel.travelerID = tblTraveler.travelerID)

WHERE GoAbroad_Main.tbcountry.locID = $_locationID
AND tblTraveler.isSuspended = 0 
AND tblTraveler.deactivated = 0 
AND tblTravelLog.deleted = 0 
AND tblTravel.deleted = 0 

GROUP BY ID

EOF;

							
			$asql = <<<EOF

SELECT tblArticle.authorID,
  tblArticle.articleID,
  tblArticle.locID,
  tblArticle.title,
  tblArticle.description,
  tblArticle.lastedited as lastupdated,
  tblArticle.logdate as _date,
  0 as trip,
  'ARTICLE' as type,
  0 as views,
  tblArticle.groupID as travellinkID,
  0 as t_ID 

from tblArticle 
JOIN GoAbroad_Main.tbcity  ON (tblArticle.locID = GoAbroad_Main.tbcity.locID )
JOIN GoAbroad_Main.tbcountry ON (GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID )

WHERE GoAbroad_Main.tbcountry.locID = $_locationID

GROUP BY tblArticle.articleID

EOF;
					

						
				$sql = 	"SELECT SQL_CALC_FOUND_ROWS tbl.travelerID, " . 
								"tbl.ID, " . 
								"tbl.locID, " . 
								"tbl.title, " . 
								"tbl.description, " . 
								"tbl.lastupdated, " .
								"tbl._date, " .
								"tbl.trip, " . 
								"tbl.type, " . 
								"tbl.views, " .
								"tbl.travellinkID, " .
								"tbl.t_ID " .
							"FROM ($esql UNION $asql ORDER BY CASE WHEN lastupdated = '0000-00-00 00:00:00' " . 
								"THEN _date " . 
								"ELSE lastupdated " . 
							"END DESC) as tbl ";

				if(isset($start) && isset($end))		
					$sql .= "LIMIT $start,$end";							
				
				$mRs->Execute($sql);
				
				$mRs2->Execute('SELECT FOUND_ROWS() as totalrec');
				$row = mysql_fetch_assoc($mRs2->Resultset());
				Travel::$statvar = $mRs2->Result(0, "totalrec");
				
				require_once("travellog/vo/Class.JournalsArticlesVO.php");
				require_once("travellog/model/Class.Article.php");
				
				$arrTravel = array();
				while ($recordset = mysql_fetch_assoc($mRs->Resultset())){					
	 				try {
 						$filteredTravel	= new JournalsArticlesVO();
						$filteredTravel->setTravelerID($recordset['travelerID']);
						$filteredTravel->setTravelID($recordset['ID']);
						$filteredTravel->setID($recordset['t_ID']);
						$filteredTravel->setLocID($recordset['locID']);
						$filteredTravel->setTitle($recordset['title']);
						$filteredTravel->setDescription($recordset['description']);
						$filteredTravel->setDate($recordset['_date']);
						$filteredTravel->setType($recordset['type']); 
						$filteredTravel->setTrip(new Trip($recordset['trip']));
						$filteredTravel->setViews($recordset['views']);
						if($filteredTravel->getType() == "JOURNAL"){
							$travel = new Travel(0, 
								array( 
									"locID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"rating" => NULL,
									"votes" => NULL,
									"viewed" => NULL,
									'lastupdated' => NULL,
									"primaryphoto" => NULL,
									"privacypref" => NULL,
									"publish" => NULL,
									"isfeatured" => NULL,
									"entryCount"  => NULL,
									"travelerID" => NULL,
									"travellinkID" => $recordset['travellinkID'], 
									"travelID" => $recordset['ID']
								)
							);
							$filteredTravel->setOwner($travel->getOwner());
						}else{
							$article = new Article(0, 
								array(
									"articleID" => NULL,
									"title" => NULL,
									"description" => NULL,
									"callout" => NULL,
									"locID" => NULL,
									"editorsNote" => NULL,
									"articledate" => NULL,
									"logdate" => NULL,
									"lastedited" => NULL,
									"isfeatured" => NULL,
									"publish" => NULL,
									"deleted" => NULL,
									"groupID" => $recordset['travellinkID'], 
									"authorID" => $recordset['travelerID'])
								);
							$filteredTravel->setOwner($article->getOwner());
						}
					}
					catch (Exception $e) {
					   throw $e;
					}
 					$arrTravel[] = $filteredTravel;
 				}

				return $arrTravel;				
			}
			
	 		static function getMostPopularTravel(){
				$rs         = new Recordset(ConnectionProvider::instance()->getConnection());
				$collection = array();
	 			$sql        = 'SELECT travelID FROM tblTravel WHERE tblTravel.deleted = 0 ORDER BY viewed DESC LIMIT 2';
	 			
	 			$rs->Execute($sql);
	 			
	 			while ($recordset = mysql_fetch_array($rs->Resultset())) {
	 				try {
 						$objNewMember = new Travel ($recordset['travelID']); 												
 					} 					
					catch (Exception $e) {
					   throw $e;
					}
 					$collection[] = $objNewMember;
 				}
 				return $collection; 					
	 		}
	 		
	 		
	 		// gets the number of travels retrieved; when function for retrieving travels on filter conditions is called
	 		public static function getTotalrec(){
	 			return Travel::$statvar;
	 		}	
	 		
	 		/*
	 		 * Added By  : Aldwin S. Sabornido
	 		 * Date Added: July 11, 2007 11:05 AM
	 		 * Purpose   : Get all journal logs
	 		 */	
	 		public function getJournalLogs($c = null){
	 			require_once('travellog/model/Class.JournalLog.php');
	 			if( $c == null ){
	 				require_once('Class.Criteria2.php');
	 				$c = new Criteria2;
	 				$c->mustBeEqual('logtype', 2);
	 				$c->setLimit(2);
	 				$c->setOrderBy('travellogID  DESC');
	 			}
	 			$obj = new JournalLog;
	 			$obj->setTravelID($this->travelID);
	 			$arr = $obj->GetList($c);
	 			return $arr;
	 		}
	
			/* added by Jul, for new RSS Feeds */
			public function getTravelerTravelLogs(){//to get journal entries of a journal
				$owner = $this->getOwner();
				$tlogs = array();
				if( !($owner instanceof Traveler) ){
					return $tlogs;
				}
				
				$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT travellogID, tblTravelLog.title, tblTravelLog.description, tblTravelLog.callout, tblTravelLog.publish, tblTravelLog.logdate, tblTravelLog.templateType, tblTravelLog.tripID, tblTravel.travelerID, tblTravel.travelID, tblTravel.travellinkID " .
						"FROM tblTravelLog, tblTravel, tblTravelLink " .
						"WHERE tblTravelLog.travelID = ".$this->getTravelID()." ".
						"AND tblTravelLog.travelID = tblTravel.travelID " .
						"AND tblTravelLog.deleted = 0 " .
						"AND tblTravelLog.publish = 1 ".						
						"AND tblTravel.deleted = 0 " .
						"AND tblTravel.publish = 1 ".
						"AND tblTravel.travellinkID = tblTravelLink.travellinkID ".
						"AND tblTravelLink.reftype = 1 AND tblTravelLink.refID = " . $owner->getTravelerID()." ".
						"ORDER BY travellogID DESC";				
				
				$this->rs->Execute($sql);
 		
		 		if ($this->rs->Recordcount() > 0){	
		 			while ($rsar = mysql_fetch_array($this->rs->Resultset())) {
		 				try {
		 					$tlog = new Travellog();
		 					$tlog->setTravelLogID($rsar['travellogID']);
		 					$tlog->setTitle($rsar['title']);
		 					$tlog->setDescription($rsar['description']);
		 					$tlog->setCallOut($rsar['callout']);
		 					$tlog->setPublish($rsar['publish']);
		 					$tlog->setLogDate($rsar['logdate']);
		 					$tlog->setTemplateType($rsar['templateType']);
		 					$tlog->setTravelerID($rsar['travelerID']);
		 					$tlog->setTravelID($rsar['travelID']);
		 					$tlog->setTripID($rsar['tripID']);
		 					$tlog->setTravellinkID($rsar['travellinkID']);
		 					$tlogs[] = $tlog;

		 				} catch (Exception $e) {}
		 			}	 			
		 		}
		 		return $tlogs;
			}
			
			/**added by ianne**/
			function getTravelLogsByTravelID(){
				$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT tblTravelLog.*, tblTravel.travelerID FROM tblTravelLog, tblTravel WHERE tblTravel.travelID = tblTravelLog.travelID AND tblTravelLog.travelID = $this->travelID AND tblTravelLog.deleted = 0";
				$this->rs->Execute($sql);

				$tlogs = array();
		 		if ($this->rs->Recordcount() > 0){	
		 			while ($rsar = mysql_fetch_array($this->rs->Resultset())) {
		 				try {
		 					$tlog = new Travellog();
		 					$tlog->setTravelLogID($rsar['travellogID']);
		 					$tlog->setTitle($rsar['title']);
		 					$tlog->setDescription($rsar['description']);
		 					$tlog->setCallOut($rsar['callout']);
		 					$tlog->setPublish($rsar['publish']);
							$tlog->setDeleted($rsar['deleted']);
		 					$tlog->setLogDate($rsar['logdate']);
		 					$tlog->setTemplateType($rsar['templateType']);
		 					$tlog->setTravelID($rsar['travelID']);
		 					$tlog->setTripID($rsar['tripID']);
							$tlog->setTravelerID($rsar['travelerID']);
		 					$tlogs[] = $tlog;

		 				} catch (Exception $e) {}
		 			}	 			
		 		}
		 		return $tlogs;
			}
			
			function relateToGroup($gID){
				$this->getOwner();
				$owner = ( $this->owner instanceof Group ) ? $this->owner->getAdministrator() : $this->owner;
				
				$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = ' select distinct gID,gname from ( SELECT tblGroup.groupID AS gID, tblGroup.name AS gname ' .
	 												' FROM tblGroup, tblGrouptoTraveler ' .
													' WHERE tblGroup.groupID = tblGrouptoTraveler.groupID ' .
													' AND tblGroup.parentID = ' . $gID .
													' AND tblGrouptoTraveler.travelerID=' . $owner->getTravelerID() . ') as g ' .
	 			       ' left join tblGroupApprovedJournals as gaj on (gID = gaj.groupID and gaj.travelID=' . $this->travelID . ' ) where gaj.groupID is null or gaj.travelID is null' .
	 			       ' UNION ' .
	 			       ' select g.groupID,name from tblGroup as g,tblGrouptoTraveler as gtt,tblGroupApprovedJournals as gaj ' .
					   ' where g.parentID = ' . $gID . ' and g.groupID = gtt.groupID and gtt.travelerID=' . $owner->getTravelerID() . 
					   ' and g.groupID = gaj.groupID and gaj.approved=0';
				
				$this->rs->Execute($sql);
				
				if($this->rs->Recordcount() > 0)
					return true;
					
				return false;
			}
			
			function deleteRelatedPhotos($params=null){
				try{
					$travellogID = (isset($params['TRAVELLOG_ID'])) ? $params['TRAVELLOG_ID'] : null;
					$groupID = (isset($params['GROUP_ID'])) ? $params['GROUP_ID'] : null;
					
					$db = new dbHandler();
					$pIDs = $this->getRelatedPhotos($travellogID);
					$paIDs = $this->getRelatedPhotoAlbums($groupID);
					if(count($pIDs)){
						foreach($pIDs as $pID){
							$sql = "DELETE FROM tblPhotoAlbumtoPhoto 
									WHERE photoID = $pID AND photoalbumID IN (" . join(",", $paIDs) . ") ";
							$db->execute($sql);
						}
					}
				}catch(exception $e){}
			}
			
			function getRelatedPhotos($travellogID=null){
				$db = new dbHandler();
				
				if(is_null($travellogID)){
					$sql = "SELECT t1.photoID
							FROM tblPhotoAlbumtoPhoto AS t1, tblTravelLogtoPhoto AS t2, tblTravelLog as t3
							WHERE t1.photoID = t2.photoID
								AND t2.travellogID = t3.travellogID 
								AND t3.travelID = $this->travelID 
								GROUP BY t1.photoID";
				}else{
					$sql = "SELECT t1.photoID
							FROM tblPhotoAlbumtoPhoto AS t1, tblTravelLogtoPhoto AS t2, tblPhotoAlbum as t3
							WHERE t1.photoID = t2.photoID
								AND t2.travellogID = $travellogID
								GROUP BY t1.photoID";
				}
				$rs1 = new iRecordset($db->execute($sql));			
				
				if( !$rs1->retrieveRecordcount() )
					return array();

				return $rs1->retrieveColumn('photoID');	
			}
			
			function getRelatedPhotoAlbums($groupID=null){
				try{
					$db = new dbHandler();
					if(is_null($groupID)){
						$sql = "SELECT t2.photoalbumID 
								FROM tblGroupApprovedJournals as t1, tblPhotoAlbum as t2 
								WHERE t1.groupID = t2.groupID
									AND t1.travelID = ".$this->travelID;
					}else{
						$sql = "SELECT photoalbumID FROM tblPhotoAlbum WHERE groupID = $groupID";
					}
					
					$rs1 = new iRecordset($db->execute($sql));
					
					if( !$rs1->retrieveRecordcount() )
						return array();
						
					return $rs1->retrieveColumn('photoalbumID');	
				}catch(exception $e){}
			}
			/**end**/
			
	 		function isApproved($gID){
	 			//global $CONFIG;
	 			require_once('travellog/dao/Class.GroupApprovedJournals.php');
	 			//$gID = $CONFIG->getGroupID();
	 			return GroupApprovedJournals::isApproved($this->travelID, $gID); 
	 		}
	 		
	 		static function getTripsByLocation($travelID, $countryID){
				$rs        = new Recordset(ConnectionProvider::instance()->getConnection());
				$rs1       = clone ($rs);
				$str_locID = 0; 
				$arr       = array();
				$sql       = sprintf(
										'SELECT GoAbroad_Main.tbcountry.locID FROM GoAbroad_Main.tbcountry as tbcountry ' .
										'WHERE tbcountry.countryID = %d ' .
										'UNION ' .
										'SELECT GoAbroad_Main.tbcity.locID FROM GoAbroad_Main.tbcountry as tbcountry, GoAbroad_Main.tbcity as tbcity ' .
										'WHERE tbcountry.countryID      = %d ' .
										'AND GoAbroad_Main.tbcity.locID > 0 ' .
										'AND tbcountry.countryID        = tbcity.countryID ' .
										'GROUP BY locID'
										,$countryID
										,$countryID
							   	    );
				
				$rs->Execute($sql);
				 
				if( $rs->Recordcount() ){
					while($row = mysql_fetch_assoc($rs->Resultset())){
						$str_locID .= ',' . $row['locID'];
					}
					
					$sql = sprintf(
									'SELECT arrival FROM tblTrips, tblTravelLog ' .
									'WHERE locID IN (%s) ' .
									'AND locID > 0 ' .
									'AND tblTrips.tripID = tblTravelLog.tripID  ' .
									'AND tblTravelLog.deleted = 0  ' .
									'AND travelID = %d ' .
									'ORDER BY arrival, tripID'
									,$str_locID
									,$travelID
							 	 );
							 	 
					$rs1->Execute($sql);
					
					if( $rs1->Recordcount() ){
						while($row = mysql_fetch_assoc($rs1->Resultset())){
							$arr[] =  $row['arrival']; 
						}			
					}
				}
				
				return $arr; 
	 		}
	 		
	 		static function isLocationExists($travelID,$countryID){
				$rs        = new Recordset(ConnectionProvider::instance()->getConnection());
				$rs1       = clone ($rs);
				$str_locID = 0; 
				$arr       = array();
				$sql       = sprintf(
										'SELECT GoAbroad_Main.tbcountry.locID FROM GoAbroad_Main.tbcountry as tbcountry ' .
										'WHERE tbcountry.countryID = %d ' .
										'UNION ' .
										'SELECT GoAbroad_Main.tbcity.locID FROM GoAbroad_Main.tbcountry as tbcountry, GoAbroad_Main.tbcity as tbcity ' .
										'WHERE tbcountry.countryID      = %d ' .
										'AND GoAbroad_Main.tbcity.locID > 0 ' .
										'AND tbcountry.countryID        = tbcity.countryID ' .
										'GROUP BY locID'
										,$countryID
										,$countryID
							   	    );
				
				$rs->Execute($sql);
				
				if( $rs->Recordcount() ){
					while($row = mysql_fetch_assoc($rs->Resultset())){
						$str_locID .= ',' . $row['locID'];
					}
					
					$sql = sprintf(
									'SELECT locID FROM tblTrips, tblTravelLog ' .
									'WHERE locID IN (%s) ' .
									'AND locID > 0 ' .
									'AND tblTrips.tripID = tblTravelLog.tripID  ' .
									'AND tblTravelLog.deleted = 0  ' .
									'AND travelID = %d ' .
									'LIMIT 1'
									,$str_locID
									,$travelID
							 	 );
							 	 
					$rs1->Execute($sql);
					
					return ($rs1->Recordcount())? true : false;					
				}
				
				return false;
	 		}
	 		
	 		
	 		static function relateJournalPhotosToGroup($travelID,$groupID){
		 		$sql = sprintf('update tblGroupApprovedJournals set isphotorelate = 1 where travelID = %d and groupID = %d',$travelID,$groupID);
				$rs  = new Recordset(ConnectionProvider::instance()->getConnection());				   
				$rs->Execute($sql);		 		
		 	}
			
	 		
	 		function PublishUnpublishEntry(){
		 		$value = ( $this->publish )? 0 : 1;
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql  = 'UPDATE tblTravel SET publish = ' .$value. ' WHERE travelID = '.$this->travelID;   
				$rs->Execute($sql);
				
				// if this travel is unpublished all journal entries under this travel(journal) which are featured should be unfeatured
				if ($value == 0){
					$trips = $this->getTrips();
					$entriesAr = array();
					foreach ($trips as $trip){
						$entriesAr = array_merge($entriesAr,$trip->getTravelLogs());
					} 
					
					require_once('travellog/model/admin/Class.FeaturedSection.php');
					$fs = new FeaturedSection(FeaturedSection::$JOURNAL_ENTRY);					
					foreach($entriesAr as $je){
						$fs->unsetItemFeatured($je->getTravelLogID());	
					}
					
				}
		 	}    	
		 	
		 	function getCountEntries($obj_criteria = NULL){
		 		if( $obj_criteria == NULL ){
		 			require_once('Class.Criteria2.php');
		 			$obj_criteria = new Criteria2;
		 		}
		 		$obj_criteria->mustBeEqual('tblTravel.travelID', $this->travelID); 
		 		$obj_criteria->setLimit(0,1);
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql  = 'SELECT tblTravel.travelID FROM tblTravel, tblTrips, tblTravelLog WHERE tblTravel.travelID = tblTrips.travelID AND tblTrips.tripID = tblTravelLog.tripID AND tblTravelLog.deleted = 0 AND tblTravel.deleted = 0 ' . $obj_criteria->getCriteria2('AND');
				
				$rs->Execute($sql);
				return $rs->Recordcount();
		 	}
		 	
		 	/**
		 	 * Returns the number of journal entry drafts of this journal.
		 	 * 
		 	 * @return integer the number of journal entry drafts of this journal.
		 	 */
		 	function getJournalEntryDraftsCount(){
		 		if (is_null($this->mJournalEntryDraftsCount)) {
			 		require_once("Class.Criteria2.php");
			 		require_once("travellog/model/Class.JournalEntryDraftPeer.php");
			 		
			 		$criteria = new Criteria2();
			 		$criteria->add(JournalEntryDraftPeer::TRAVEL_ID, $this->travelID);
			 		
			 		$this->mJournalEntryDraftsCount = JournalEntryDraftPeer::doCount($criteria);
		 		}
		 		
		 		return $this->mJournalEntryDraftsCount;
		 	}
		 	
		 	/**
		 	 * Returns true if the journal has journal entry drafts.
		 	 * 
		 	 * @return boolean true if the journal has journal entry drafts; otherwise false.
		 	 */
		 	function hasJournalEntryDrafts(){
				return (0 < $this->getJournalEntryDraftsCount()) ? true : false; 
		 	}
		 	
			// added by daf; check if travel has any entry; regardless of published or not
			
			function hasAnyEntry(){
				return $this->entryCount;		// added by K. Gordo (we now have a count var for travellogs)
		 	}
		 	
		 	// edited ianne: added the travel to groupapprovedjournals.
		 	function CreateTravelGroup($groupID){
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql  = sprintf('INSERT INTO tblTraveltoGroup (travelID, groupID) VALUES (%d, %d)', $this->travelID, $groupID);
				$rs->Execute($sql);	
				$sql  = sprintf('INSERT INTO tblGroupApprovedJournals (travelID, groupID, approved) VALUES (%d, %d, %d)', $this->travelID, $groupID, 1);
				$rs->Execute($sql);
		 	}
	 		
	 		function DeleteTravelGroup($travelID){
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql  = sprintf('DELETE FROM tblTraveltoGroup WHERE travelID = %d', $travelID);
				$rs->Execute($sql);
	 		}		
	 		
	 		function RetrieveTravelGroup($travelID){
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql  = sprintf('SELECT groupID FROM tblTraveltoGroup WHERE travelID = %d', $travelID);
				$arr  = array();
				$rs->Execute($sql);
				if( $rs->Recordcount() ){
					while($row = mysql_fetch_assoc($rs->Resultset())){
						$arr[$row['groupID']] = $row['groupID']; 
					}
				}
				return $arr;
	 		}
	 		
	 		function getCountUnpublishEntries(){
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
				$sql  = sprintf('SELECT tblTravel.travelID FROM tblTravel, tblTravelLog WHERE tblTravel.travelID = tblTravelLog.travelID AND tblTravelLog.publish = 0 AND tblTravelLog.deleted = 0 AND tblTravel.deleted = 0 AND tblTravel.travelID = %d', $this->travelID);
				$rs->Execute($sql);
				return $rs->Recordcount(); 
			}
			
			static function getRelatedGroup($travelID){
				require_once('travellog/dao/Class.GroupApprovedJournals.php');
				return GroupApprovedJournals::getRelatedGroup($travelID);			
			}
			
			static function hasMoreThanOneJournal($groupID){
				require_once('travellog/dao/Class.GroupApprovedJournals.php');
	 			return GroupApprovedJournals::hasMoreThanOneJournal($groupID);  
			}
			
			/**
			 * method that gets all travel videos pertaining to a specific travel
			 * added by neri
			 * 10-20-08
			 */
			 
			function getTravelVideos () {
				$cache 	= ganetCacheProvider::instance()->getCache();
	 			$key = 'JournalVideos_'.$this->travelID;
		 		$mRs = new Recordset(ConnectionProvider::instance()->getConnection());
	 			
	 			if ($cache != null && $videos = $cache->get($key)) {
					if ($videos[0] == 'EMPTY')
						$videos = array();
					return $videos; 
				}
				
				$videos = array();
				
				$query = "SELECT tblTravelLogtoVideo.videoID FROM tblTravelLogtoVideo INNER JOIN tblTravelLog " .
	 					 "WHERE tblTravelLogtoVideo.travellogID = tblTravelLog.travellogID " .
	 					 "AND tblTravelLog.deleted = 0 " .
	 					 "AND tblTravelLog.travelID = $this->travelID ORDER BY tblTravelLog.travellogID ASC";
				
				$mRs->Execute($query);
				
				if (0 < $mRs->Recordcount()) {
	 				while ($recordset = mysql_fetch_array($mRs->Resultset())) {
	 			 		try {
	 			 			$video = new TravelLogToVideo($recordset['videoID']);
	 			 		}
	 			 		catch (exception $e) {						  
							throw new exception ($e->getMessage());
						}
						$videos[] = $video;	 		
	 			 	}	
	 			}
	 			
	 			if ($cache != null){
					if (!count($videos)) {
						// a blank array put to cache causes null when retreived back
						$cache->set($key, array('EMPTY'), array('EXPIRE'=>3600) );	
					}else {
						$cache->set($key, $videos, array('EXPIRE'=>3600) );
					}
	
				}
	 			
	 			return $videos;
			}
			
			/**
			 * Retrieves the recent travel log (journal entry) of this travel (journal).
			 * 
			 * @param RowsLimit $limit
			 * @return array Array of TravelLog objects
			 */
			function getRecentJournalEntries($limit = null){
				$handler = new dbHandler();
				
				$sql = "SELECT tblTravelLog.*, tblTravel.travelerID, tblTravel.travellinkID " .
					" FROM tblTravelLog, tblTravel " .
					" WHERE tblTravel.travelID = tblTravelLog.travelID " .
					"AND tblTravelLog.deleted = 0 " .
					"AND tblTravel.deleted = 0 " .
					" AND tblTravel.travelID = ".$handler->makeSqlSafeString($this->travelID).
          			" ORDER BY logdate DESC ";
				
				$sql .= (is_null($limit)) ? "" : SqlUtil::createLimitClause($limit);
				
				$rs = new iRecordset($handler->execute($sql));
				
				$entries = array();
				
				while (!$rs->EOF()) {
					$entry = new TravelLog(0, $rs->current());
					$entries[] = $entry;
					$rs->next();
				}
				
				return $entries;
			}

			public function getMostRecentTravellog($idOnly=TRUE){
				require_once("travellog/model/Class.TravelLog.php");
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				$travelID = $this->getTravelID();
				$sql = "SELECT tblTravelLog.*
						FROM tblTravelLog, tblTravel
						WHERE tblTravel.travelID = $travelID
							AND tblTravel.travelID = tblTravelLog.travelID
							AND tblTravelLog.deleted = 0
							AND tblTravelLog.publish = 1
						ORDER BY tblTravelLog.logdate DESC";
				$travellog = 0;
				try{
	 				$this->rs->Execute($sql);

	 				if (0 < $this->rs->Recordcount()) {
	 					$data = mysql_fetch_array($this->rs->Resultset());
	 					try{
							if( $idOnly ){
								$travellog = $data["travellogID"];
							}else{
								$travellog = new TravelLog($data["travellogID"]);
							}
	 					}catch (exception $e){}
	 				}
	 			}catch (exception $ex) {}

	 			return $travellog;
			}
			
			//added bu Jul for custom page header
			private $mQualifiedPhotosCount = 0;
			public function setQualifiedPhotosCount($count=0){
				$this->mQualifiedPhotosCount = $count;
			}
			public function getQualifiedPhotosCount(){
				return $this->mQualifiedPhotosCount;
			}
			public function getQualifiedCustomHeaderPhotos(){
				require_once("travellog/model/Class.Photo.php");
				require_once("travellog/model/Class.TravelLog.php");
				$travelID = $this->getTravelID();
				$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT tblTravelLog.*,
								tblTravel.travelerID,
								tblTravel.travellinkID,
								tblPhoto.*,
								tblTraveler.username
						FROM tblTravelLog, tblTravelLogtoPhoto, tblPhoto, tblTravel, tblTraveler
						WHERE tblTravelLog.travelID = $travelID
							AND tblTravel.travelID = $travelID
							AND tblTravel.deleted = 0
							AND tblTravelLog.deleted = 0
							AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID
							AND tblTravelLogtoPhoto.qualifiedCustomHeader = 1
							AND tblTravelLogtoPhoto.photoID = tblPhoto.photoID
							AND tblTravel.travelerID = tblTraveler.travelerID
						ORDER BY tblPhoto.photoID DESC";
				
				$photos = array();
				try{
	 				$this->rs->Execute($sql);
	 				if (0 < $this->rs->Recordcount()) {
	 					while ($data = mysql_fetch_array($this->rs->Resultset())) {
	 						try{
	 							$travellog = new TravelLog(0, $data);
								$photo = new Photo($travellog,0,$data);
								$photos[] = $photo;
	 						}catch (exception $e){}
	 					}
	 				}
	 			}catch (exception $ex){}
				return $photos;
			}
			public function getPrimaryPhotoInstance(){
				require_once("travellog/model/Class.Photo.php");
				require_once("travellog/model/Class.TravelLog.php");
				
				$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
				$photoID = $this->getPrimaryphoto();
				$sql = "SELECT travellogID
						FROM tblTravelLogtoPhoto
						WHERE tblTravelLogtoPhoto.photoID = $photoID";
				$photo = new Photo(NULL,$photoID);
				try{
	 				$this->rs->Execute($sql);

	 				if (0 < $this->rs->Recordcount()) {
	 					while ($data = mysql_fetch_array($this->rs->Resultset())) {
	 						try{
	 							$travellog = new TravelLog($data["travellogID"]);
								$photo = new Photo($travellog,$photoID);
	 						}catch (exception $e){}
	 					}
	 				}
	 			}catch (exception $ex) {}

	 			return $photo;
			}
			
		 	// invalidate entry in cache
			private function invalidateCacheEntry(){
				$this->cache	=	ganetCacheProvider::instance()->getCache();
				if ($this->cache != null) {
					$this->cache->delete(get_class($this) . '_' . $this->travelID);
					
					// invalidate relations which are cached
					$this->cache->delete('Traveler_Travels_Relation_' .  $this->travelerID);
					$this->cache->delete('TravelerCB_Travels_Relation_' .  $this->travelerID);
					$this->cache->delete('JournalVideos'.$this->travelID);	// added by neri: 06-10-09
					
					//added by Jul to delete cached photo collection for this journal
					try{
						$owner = $this->getOwner();
						if( $owner instanceof Traveler ){
							$this->cache->delete('Photo_Collection_Traveler_Owner_'.$owner->getTravelerID());
							$this->cache->delete('Photo_Collection_Traveler_Public_'.$owner->getTravelerID());
							$this->cache->delete('Photo_Collection_CBTraveler_Public_'.$owner->getTravelerID());
						}else if( $owner instanceof Group ){
							$this->cache->delete('Photo_Collection_Group_Admin_'.$owner->getGroupID());
							$this->cache->delete('Photo_Collection_Group_Public_'.$owner->getGroupID());
						}
					}catch(exception $e){
					}
				}
				// delete stored $loadedTravels
				unset(self::$loadedTravels[$this->travelID]);
				$this->invalidateJournalsPageCaching();
			}

			
			// updates used this instead of log reader
			public function getLastEntryUpdated(){
				require_once("travellog/vo/Class.JournalUpdatesVO.php");
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$this->recentlyAdded = $this->getRecentlyAddedEntry();
				$recent = (!is_null($this->recentlyAdded)) ? $this->recentlyAdded->getTravelLogID(): 0;
				
				$now = date('Y-m-d h:i:s');
				
				$travelID = $this->getTravelID();
				$sql = "SELECT t1.*
							FROM tblTravelLog as t1 
							WHERE t1.travelID = $travelID 
								AND t1.deleted = 0 
								AND t1.publish = 1
								AND t1.lastedited <> '0000-00-00 00:00:00' 
								AND TIMESTAMPDIFF(MONTH,t1.lastedited,'$now') <= 2
								AND t1.travelLogID != $recent
							ORDER BY t1.lastedited DESC LIMIT 0,1";
				
				$juVO = null;
				try{
	 				$this->rs->Execute($sql);

	 				if (0 < $this->rs->Recordcount()) {
	 					$data = mysql_fetch_assoc($this->rs->Resultset());
	 					try{
							$juVO = new JournalUpdatesVO;
							$juVO->setDate($data['lastedited']);
							$juVO->setTitle($data['title']);
							$juVO->setTravelLogID($data['travellogID']);
							$juVO->setAction("Last Entry Updated");
	 					}catch (exception $e){}
	 				}
	 			}catch (exception $ex) {}

	 			return $juVO;
			}
			
			public function getLatestPhotoAndVideoCountPerEntry(){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$travelID = $this->getTravelID();
				$sql = "SELECT 'PHOTO' AS type, 
							tl.travellogID AS tID, 
							tl.title,
							COUNT( p.photoID ) as cnt, 
							MAX( CAST( p.dateuploaded AS DATE ) ) AS maxdate, 
							p.dateuploaded
						FROM tblPhoto AS p, tblTravelLogtoPhoto AS tp, tblTravelLog AS tl
						WHERE tl.travelID = $travelID 
							AND tl.deleted = 0
							AND tl.travellogID = tp.travellogID
							AND tp.photoID = p.photoID
						GROUP BY CAST( p.dateuploaded AS DATE ) , tl.travellogID
						UNION ALL 
						SELECT 'VIDEO' AS type, 
							tl2.travellogID, 
							tl2.title, 
							COUNT( v.videoID ) as cnt2, 
							MAX( CAST( v.dateAdded AS DATE ) ) AS maxdate2, 
							v.dateAdded
						FROM tblVideo AS v, tblTravelLogtoVideo AS tv, tblTravelLog AS tl2
						WHERE tl2.travelID = $travelID 
							AND tl2.deleted = 0
							AND tl2.travellogID = tv.travellogID
							AND tv.videoID = v.videoID
						GROUP BY CAST( v.dateAdded AS DATE ) , tl2.travellogID
						ORDER BY maxdate DESC, tID DESC";

				try{
	 				$this->rs->Execute($sql);
					
					$updates = array();
					
					$keys = array();
					while($data = mysql_fetch_assoc($this->rs->Resultset())){
						$key = $data['tID']."_".$data['type'];
						if(!in_array($key, $keys)){
							$updates[$data['tID']][$data['type']] = $data;
							$keys[] = $key;
						}		
					}
	 			}catch (exception $ex) {}

	 			return $updates;
			}
		
		function getFirstJournalEntry(){
			require_once("travellog/model/Class.TravelLog.php");
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$travelID = $this->getTravelID();
			$sql = "SELECT tblTravelLog.*
					FROM tblTravelLog, tblTravel
					WHERE tblTravel.travelID = $travelID
						AND tblTravel.travelID = tblTravelLog.travelID
						AND tblTravelLog.deleted = 0
						AND tblTravelLog.publish = 1
					ORDER BY tblTravelLog.logdate ASC
					LIMIT 0, 1";
			$travellog = NULL;
			try{
 				$this->rs->Execute($sql);
 				if (0 < $this->rs->Recordcount()) {
 					$data = mysql_fetch_array($this->rs->Resultset());
 					try{
						$travellog = new TravelLog($data["travellogID"]);
 					}catch (exception $e){}
 				}
 			}catch (exception $ex) {}

 			return $travellog;
		}
		
		// updates used this instead of log reader
		public function getRecentlyAddedEntry(){
			$now = date('Y-m-d h:i:s');
			if(is_null($this->recentlyAdded)){
				require_once("travellog/vo/Class.JournalUpdatesVO.php");
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$travelID = $this->getTravelID();
				$sql = "SELECT t1.*
							FROM tblTravelLog as t1 
							WHERE t1.travelID = $travelID 
								AND t1.deleted = 0 
								AND t1.publish = 1
								AND TIMESTAMPDIFF(MONTH,t1.logdate,'$now') <= 2
						ORDER BY t1.logdate DESC LIMIT 0,1";
				
				$juVO = null;
				try{
	 				$this->rs->Execute($sql);

	 				if (0 < $this->rs->Recordcount()) {
	 					$data = mysql_fetch_assoc($this->rs->Resultset());
	 					try{
							$juVO = new JournalUpdatesVO;
							$juVO->setDate($data['logdate']);
							$juVO->setTitle($data['title']);
							$juVO->setTravelLogID($data['travellogID']);
							$juVO->setAction("Recently Added Entry");
							$this->recentlyAdded = $juVO;
	 					}catch (exception $e){}
	 				}
	 			}catch (exception $ex) {}
				
	 		}
			return $this->recentlyAdded;
		}
		
		public function isGroupJournal(){
			$this->rs =	new Recordset(ConnectionProvider::instance()->getConnection());
 			$sql = "SELECT reftype FROM tblTravelLink WHERE travellinkID = $this->travellinkID  " ;
			$this->rs->Execute($sql);
			if( 0 < $this->rs->RecordCount() ){
				$data = mysql_fetch_array($this->rs->Resultset());
				$this->reftype = $data["reftype"];
				if( 2 == $this->reftype ){
					return TRUE;
				}
			}else{
				$owner = $this->getOwner();
				if( $owner instanceof Group ){
					return TRUE;
				}
			}
			return FALSE;
		}
		
		/**
		* function that invalidates page caching of journals
		* possible uses: after approve/unapprove of journals, feature, delete journal entry and delete journals
		**/		
		public function invalidateJournalsPageCaching(){
			$this->cache = ganetCacheProvider::instance()->getCache();
			if($this->cache != null){
				// added by Ianne: invalidate cache keys for journals page caching
				require_once('travellog/dao/Class.GroupApprovedJournals.php');
				$relatedGroups = GroupApprovedJournals::getGroupsWhereTravelIsApproved($this->travelID);
				require_once('travellog/model/Class.Featured.php');
				$featuredGroups = Featured::getGroupsWhereTravelIsFeatured($this->travelID);
				try{
					$owner = $this->getOwner();
					// invaldiate caching in cobrands and ganet main
					$ganetmaincache = true;
					foreach($relatedGroups as $group){
						$parent = $group->getParent();
						$siteID = (!is_null($parent)) ? $parent->getGroupID() : $group->getGroupID();
						if( $owner instanceof Traveler ){
							// traveler profile - MAIN
							$this->cache->delete('Journal_Profile_Context_0_' . $owner->getTravelerID());
							// traveler profile - COBRAND
							$this->cache->delete('Journal_Profile_Context_' . $siteID . '_' . $owner->getTravelerID());
						}

						// featured journals
						if(in_array($group->getGroupID(), $featuredGroups)){
							// MAIN
							$this->cache->delete('Journal_Group_Context_PUBLIC_' . $group->getGroupID() . '_0/group.phpgID=' .$group->getGroupID()); 
							// COBRANDS
							$this->cache->delete('Journal_Group_Context_PUBLIC_' . $group->getGroupID() . '_' . $siteID . '/group.phpgID=' .$group->getGroupID()); 
							$this->cache->delete('Journal_Group_Context_PUBLIC_' . $group->getGroupID() . '_' . $siteID . '/group.php'); 
							$this->cache->delete('Journal_Group_Context_PUBLIC_' . $group->getGroupID() . '_' . $siteID . '/index.php'); 
						}
						
						// main journals tab - COBRAND approved, public view
						$this->cache->delete('Journal_URL_' . $siteID . '/journal.php_0');
						// journals tab - COBRAND approved including subgroups, public view
						$this->cache->delete('Journal_URL_' . $siteID . '/journal.phpaction=groupJournals&gID=' . $group->getGroupID() . '_0');
						$this->cache->delete('Journal_URL_' . $siteID . '/journal.phpaction=groupjournals&gID=' . $group->getGroupID() . '_0');
						// journals tab - MAIN including subgroups, public view
						$this->cache->delete('Journal_URL_0/journal.phpaction=groupJournals&gID=' . $group->getGroupID() . '_0');
					}
					
					// travel is not approved/related to any group - newly added
					if( $owner instanceof Traveler ){
						// traveler profile
						$this->cache->delete('Journal_Profile_Context_0_' . $owner->getTravelerID());
					}
					// main journals tab - MAIN, public view
					$this->cache->delete('Journal_URL_0/journal.php_0');
					// end invalidate cache keys for journals page caching
				}catch(exception $e){}
			}	
		}
		
	}
