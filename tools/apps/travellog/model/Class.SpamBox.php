<?php
	/****************************************************************/
	/*	@author Cheryl Ivy Q. Go									*/
	/*	Created On 28 April 2008									*/
	/*	Purpose: Displays all spam messages sent to all travelers	*/
	/****************************************************************/

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.RowsLimit.php';
	require_once 'travellog/model/Class.SpamMessages.php';
	require_once 'travellog/model/Class.SendableFactory.php';

	class SpamBox{
		private $mRs	= null;
		private $mRs2	= null;
		private $mRs3	= null;
		private $mConn	= null;

		private $mRecordcount	= 0;

		function SpamBox(){
			try{
				$this->mConn = new Connection();
				$this->mRs 	= new Recordset($this->mConn);
				$this->mRs2 = new Recordset($this->mConn);
				$this->mRs3 = new Recordset($this->mConn);
			}
			catch(exception $e){
				throw $e;
			}

			return $this;
		}

		function getTotalRecords(){
			return $this->mRecordcount;
		}
		
		function getAllSpam(RowsLimit $_rowslimit = null){
			require_once 'travellog/model/admin/Class.GanetSpamMessage.php';
			
			if ($_rowslimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowslimit ->getRows();
				$offset = ($_rowslimit->getOffset() * $row);
				$queryLimit = " LIMIT " . $offset . ", ". $row;
			}
			
			$sql = "SELECT DISTINCT SQL_CALC_FOUND_ROWS " .
					"tblPersonalMessage.id FROM " .
					"tblPersonalMessage, tblPersonalMessageRecipients " .
					"WHERE tblPersonalMessage.id = tblPersonalMessageRecipients.messageID " .
					"AND tblPersonalMessageRecipients.isSpam = 1 " .
					"AND tblPersonalMessage.isDeleted = 0 " .
					"AND tblPersonalMessageRecipients.isDeleted = 0 " . $queryLimit;
			$this->mRs->Execute($sql);
			
			$idArr = array();
			while($id = mysql_fetch_assoc($this->mRs->Resultset()))
				$idArr[] = $id['id'];
			
			$messageID = array();
			$messageArr = array();
			
			if (0 == $this->mRecordcount){
				$sql2 = "SELECT FOUND_ROWS() as totalRecords";
				$this->mRs2->Execute($sql2);
				$this->mRecordcount = $this->mRs2->Result(0, "totalRecords");
			}
			if(0 < $this->mRecordcount){			
				$sql3 = "SELECT " .
					"tblPersonalMessage.id, " .
					"tblPersonalMessage.dateCreated, " .
					"tblPersonalMessage.senderID, " .
					"tblPersonalMessage.subject, " .
					"tblPersonalMessage.body, " .
					"tblPersonalMessage.isMessageIncludedInNotif, " .
					"tblPersonalMessageRecipients.recipientID  " .
					"FROM " .
					"tblPersonalMessage, tblPersonalMessageRecipients " .
					"WHERE tblPersonalMessage.id = tblPersonalMessageRecipients.messageID " .
					"AND tblPersonalMessageRecipients.isSpam = 1 " .
					"AND tblPersonalMessage.isDeleted = 0 " .
					"AND tblPersonalMessageRecipients.isDeleted = 0 " .
					"AND tblPersonalMessage.id IN (" . implode(",", $idArr) . ") " .
					"ORDER BY tblPersonalMessage.dateCreated ASC ";
				$this->mRs3->Execute($sql3);
								
				while($result = mysql_fetch_assoc($this->mRs3->Resultset())){
					if(!in_array($result['id'], $messageID)){
						$message = new GanetSpamMessage();
						$message->initialize($result);
						$messageArr[$result['id']] = $message;
						$messageID[] = $result['id'];
					}
					else{
						if($messageArr[$result['id']] instanceof GanetSpamMessage)
							$messageArr[$result['id']]->addRecipientID($result['recipientID']);
					}
				}
			}
			return $messageArr;
		}

		//TODO: A spam message can have lots of recipients - i'll make a new method getAllSpam()
		//		coz this method returns array of one message for one recipient
		function getAllSpamMessages(RowsLimit $_rowslimit = null){
			if ($_rowslimit == NULL)
				$queryLimit = " ";			
			else{				
				$row = $_rowslimit ->getRows();
				$offset = ($_rowslimit->getOffset() * $row);
				$queryLimit = " LIMIT " . $offset . ", ". $row;
			}

			/*$sql = "SELECT SQL_CALC_FOUND_ROWS tblMessageToAttribute.recipientID, tblMessageToAttribute.messageType, " .
					"tblMessages.discriminator, tblMessages.messageID, tblMessages.senderID, " .
					"tblMessages.title, tblMessages.message, tblMessages.dateCreated " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
					"AND tblMessageToAttribute.messageType = 5 " .
					"AND tblMessageToAttribute.trashed = 0 " .
					"AND tblMessages.discriminator = 3 " .
					"GROUP BY tblMessages.messageID " . $queryLimit;*/
			$sql = "SELECT SQL_CALC_FOUND_ROWS " .
					"tblPersonalMessage.id, " .
					"tblPersonalMessage.dateCreated, " .
					"tblPersonalMessage.senderID, " .
					"tblPersonalMessage.subject, " .
					"tblPersonalMessage.body, " .
					"tblPersonalMessage.isMessageIncludedInNotif, " .
					"tblPersonalMessageRecipients.recipientID  " .
					"FROM " .
					"tblPersonalMessage, tblPersonalMessageRecipients " .
					"WHERE tblPersonalMessage.id = tblPersonalMessageRecipients.messageID " .
					"AND tblPersonalMessageRecipients.isSpam = 1 " .
					"AND tblPersonalMessage.isDeleted = 0 " .
					"AND tblPersonalMessageRecipients.isDeleted = 0 " .
					"ORDER BY tblPersonalMessage.dateCreated ASC " . $queryLimit;
			$this->mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() as totalRecords";
			$this->mRs2->Execute($sql2);

			$arrSpam = array();
			$this->mRecordcount = $this->mRs2->Result(0, "totalRecords");

			while($Spam = mysql_fetch_assoc($this->mRs->Resultset())){
				$temp = new SpamMessages();
				$temp->setTitle(stripslashes($Spam['subject']));
				$temp->setMessage(stripslashes($Spam['body']));
				$temp->setDateCreated($Spam['dateCreated']);
				$temp->setID($Spam['id']);
				$temp->setIsIncludedInNotif($Spam['isMessageIncludedInNotif']);
				//$temp->setDiscriminator($Spam['discriminator']);
				//$temp->setMessageTypeID($Spam['messageType']);

				// get source || TRAVELER/STAFF/GROUP			
				//try{
				//	$factory = SendableFactory::Instance();
				//	$_source = $factory->Create($Spam['senderID']);
				//}				
				//catch (exception $e){
				//	throw $e;
				//}
				
				$temp->setSenderID($Spam['senderID']);

				// get recipients of message
				//$sql3 = "SELECT recipientID " .
				//		"FROM tblMessageToAttribute " .
				//		"WHERE messageType = 5 " .
				//		"AND messageID = " . $Spam['messageID'];
				//$this->mRs3->Execute($sql3);

				//$arrRecipient = array();
				//while($Dest = mysql_fetch_assoc($this->mRs3->Resultset())){
					// get recipient || TRAVELER/STAFF/GROUP			
				//	try{
				//		$factory = SendableFactory::Instance();
				//		$_recipient = $factory->Create($Dest['recipientID']);
				//	}				
				//	catch (exception $e){
				//		throw $e;
				//	}
				//	$arrRecipient[] = $_recipient;
				//}
				
				//$_recipient = gaTravelerMapper::getTraveler($Spam['recipientID']);
				//$temp->setDestination(array($_recipient));
				$temp->setRecipientID($Spam['recipientID']);
				$arrSpam[] = $temp;
			}

			return $arrSpam;
		}
		
		function getSpamMessagesByUsername($username){
			require_once("travellog/model/Class.Traveler.php");
			$id = Traveler::getTravelerIDByUsername($username);
			if($id > 0){
				require_once 'travellog/model/admin/Class.GanetSpamMessage.php';

				$sql = "SELECT DISTINCT " .
						"tblPersonalMessage.id FROM " .
						"tblPersonalMessage, tblPersonalMessageRecipients " .
						"WHERE tblPersonalMessage.senderID = ". $id ." ".
						"AND tblPersonalMessage.id = tblPersonalMessageRecipients.messageID " .
						"AND tblPersonalMessageRecipients.isSpam = 1 " .
						"AND tblPersonalMessage.isDeleted = 0 " .
						"AND tblPersonalMessageRecipients.isDeleted = 0 ";
				$this->mRs->Execute($sql);

				$idArr = array();
				while($id = mysql_fetch_assoc($this->mRs->Resultset()))
					$idArr[] = $id['id'];

				$messageID = array();
				$messageArr = array();

				if(0 < $this->mRs->Recordcount()){			
					$sql3 = "SELECT " .
						"tblPersonalMessage.id, " .
						"tblPersonalMessage.dateCreated, " .
						"tblPersonalMessage.senderID, " .
						"tblPersonalMessage.subject, " .
						"tblPersonalMessage.body, " .
						"tblPersonalMessage.isMessageIncludedInNotif, " .
						"tblPersonalMessageRecipients.recipientID  " .
						"FROM " .
						"tblPersonalMessage, tblPersonalMessageRecipients " .
						"WHERE tblPersonalMessage.id = tblPersonalMessageRecipients.messageID " .
						"AND tblPersonalMessageRecipients.isSpam = 1 " .
						"AND tblPersonalMessage.isDeleted = 0 " .
						"AND tblPersonalMessageRecipients.isDeleted = 0 " .
						"AND tblPersonalMessage.id IN (" . implode(",", $idArr) . ") " .
						"ORDER BY tblPersonalMessage.dateCreated ASC ";
					$this->mRs3->Execute($sql3);

					while($result = mysql_fetch_assoc($this->mRs3->Resultset())){
						if(!in_array($result['id'], $messageID)){
							$message = new GanetSpamMessage();
							$message->initialize($result);
							$messageArr[$result['id']] = $message;
							$messageID[] = $result['id'];
						}
						else{
							if($messageArr[$result['id']] instanceof GanetSpamMessage)
								$messageArr[$result['id']]->addRecipientID($result['recipientID']);
						}
					}
				}
				return $messageArr;
			}
		}
	}
?>