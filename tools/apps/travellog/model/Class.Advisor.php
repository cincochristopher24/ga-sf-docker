<?php
	/*
	 * Class.Advisor.php
	 * Created on Jan 17, 2008
	 * created by marc
	 */
	 
	 require_once('travellog/model/Class.Traveler.php');
	 require_once('travellog/model/Class.GroupFactory.php');
	 require_once('Class.Recordset.php');
	 require_once('Class.Connection.php');
	 
	 class Advisor extends Traveler{
	 	
	 	private $myAdvisorGroup;
	 	private $myRs;
	 	private $myConn;
	 	
	 	function __construct($advisorID){
	 		 
	 		$this->myConn = new Connection();
	 		$this->myRs = new Recordset($this->myConn);
	 		
	 		$this->myRs->Execute("SELECT groupID FROM tblGrouptoAdvisor WHERE travelerID = $advisorID");
	 		if( $this->myRs->RecordCount() ){
	 			parent::__construct($advisorID);
	 			$mGroup = GroupFactory::instance()->create(array($this->myRs->Result(0,'groupID')));
	 			$this->setAdvisorGroup($mGroup[0]);
	 		}
	 		else
	 			throw new exception("Advisor does not exist: $advisorID");
	 		return $this;
	 	}
	 	
	 	function getAdvisorGroup(){
	 		return $this->myAdvisorGroup;	
	 	}
	 	
	 	function setAdvisorGroup( Group $group ){
	 		$this->myAdvisorGroup = $group; 
	 	}

		/**
		 *	@author		Miss CIG
		 *	Date		4 November 2009
		 *	Returns all travelers that is an advisor to a group
		 */
		public static function retrieveAllGroupAdvisor( RowsLimit $_rowsLimit = NULL, $_groupName = '' )
		{
			$mConn	= new Connection();
	 		$mRs	= new Recordset( $mConn );
			$mRs2	= new Recordset( $mConn );

			$queryLimit	 = "";
			$mCondition	 = "";
			$arrAdvisors = array();

			if ( !is_null($_rowsLimit) )
			{
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);			
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			if ( 0 < strlen( $_groupName ) )
			{
				$mCondition = " AND tblGroup.name LIKE '%" . $_groupName . "%' ";
			}

			$query = " SELECT SQL_CALC_FOUND_ROWS tblTraveler.travelerID, tblTraveler.username, tblTraveler.email, " .
						" CONCAT(tblTraveler.firstName, ' ', tblTraveler.lastName) as fullname, tblGroup.name, tblGroup.groupID " .
						" FROM tblTraveler, tblShowAccountChangeNoticetoAdvisor, tblGroup " .
						" WHERE tblTraveler.travelerID = tblShowAccountChangeNoticetoAdvisor.travelerID " .
						" AND tblShowAccountChangeNoticetoAdvisor.groupID = tblGroup.groupID " . $mCondition .
						" GROUP BY tblTraveler.travelerID " .
						" ORDER BY tblTraveler.travelerID " . $queryLimit;
			$mRs->Execute( $query );

			$query2 = "SELECT FOUND_ROWS() as totalRecords";
			$mRs2->Execute( $query2 );

			while ( $resultset = mysql_fetch_assoc($mRs->Resultset()) )
			{
				$arrTemp = array(
					'travelerID'	=> $resultset['travelerID'],
					'username'		=> $resultset['username'],
					'email'			=> $resultset['email'],
					'fullname'		=> $resultset['fullname'],
					'groupID'		=> $resultset['groupID'],
					'groupname'		=> $resultset['name']
				);

				$arrAdvisors[] = $arrTemp;
			}

			return array(
				'advisors'		=> $arrAdvisors,
				'total'			=> $mRs2->Result(0, "totalRecords")
			);
		}
	 } 
?>
