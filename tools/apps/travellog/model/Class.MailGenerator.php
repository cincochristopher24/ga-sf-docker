<?php
	/*
	 * Class.MailGenerator.php
	 * Created on Jun 28, 2007
	 * created by marc
	 * @use sends mail to goabroad.net member/group administrator
	 */
	 
	 require_once('travellog/model/Class.AdminGroup.php');
	 require_once('class.phpmailer.php');
	 require_once("Class.Connection.php");
	 require_once("Class.Recordset.php");
	 
	 define("ACCOUNT_ACTIVATION",0);
	 define("SUBSCRIBE_NEWSLETTER",1);
	 define("MAIL_ADMIN",2);
	 define("WELCOME",3);
	 define("RESEND_ACTIVATION_EMAIL",4);
	 define("FORGOT_PASSWORD",5);
	  
	 class MailGenerator{
	 	
	 	protected $travelerID;
	 	protected $message;
	 	protected $subject;
	 	protected $sendTo;
	 	protected $email;
	 	protected $from = "admin@goabroad.net";
	 	protected $fromName = "The GoAbroad Network";
	 	protected $replyTo = "admin@goabroad.net";
		protected $messageType = ACCOUNT_ACTIVATION;
	 	
	 	function __construct($travelerID,$messageType = ACCOUNT_ACTIVATION,$cobrandData = null){
	 		$conn = new Connection();
	 		$rs = new Recordset($conn);
			
			$fromGACOM = strpos($_SERVER["SERVER_NAME"], 'goabroad.com'); // check if from GACOM
			$uri = ($fromGACOM !== false) ? "www.goabroad.net" : $_SERVER["SERVER_NAME"];
			
	 		$this->messageType = $messageType;
	
	 		$sqlquery = "SELECT firstname,lastname,email,username,password " .
	 				"FROM tblTraveler " .
	 				"WHERE travelerID = $travelerID";
	 		$query = $rs->execute($sqlquery);
	 		if( $tmpRow = mysql_fetch_array($query) ){
	 			$this->sendTo =  $tmpRow["firstname"] . " " . $tmpRow["lastname"];
				$this->email = $tmpRow["email"];	
				
				if( in_array($messageType,array(ACCOUNT_ACTIVATION,RESEND_ACTIVATION_EMAIL,FORGOT_PASSWORD)) ){
					// encrypt travelerID for confirmation
					require_once("Class.Crypt.php");
					$crypt=  new Crypt();
					$cryptTravID = $crypt->encrypt($travelerID);
					$cryptTravID = urlencode(substr($cryptTravID,0,strlen($cryptTravID)-1));
				}
				
				switch($messageType){
					case ACCOUNT_ACTIVATION:
						
						$varurl = "http://".$uri."/login.php?ref=" . $cryptTravID . " ";
						
						$this->subject = "Please complete your registration on GoAbroad Network";
						$this->message = "Dear $this->sendTo,

You recently signed up for the GoAbroad Network using this email address.

To complete the registration process, please click on the link below:

$varurl

Thank you.

Happy travels,

The GoAbroad Network Team
---
If you did not recently sign up for GoAbroad Network, or believe that you got this email by mistake, you may disregard this message. However, receiving this email means that someone recently created a GoAbroad Network account and entered your email address while doing so. If you believe this is the case, don't worry because they won't be able to complete their registration. If you want to create your own account on Goabroad Network, please visit:
http://www.goabroad.net";
						break;
					
					case SUBSCRIBE_NEWSLETTER:
						$this->from = "newsletter@goabroad.com";
						$this->fromName = "GoAbroad.com Newsletter";
						$this->replyTo = $this->from;
						$this->subject = "Newsletter subscription confirmation";
						$this->message = "
This is an automatic response. Please do not reply to this message.

Thank you for subscribing to the FREE weekly GoAbroad.com Newsletter. Please be assured that we will only use your email address to communicate with you about our services and promotions. We do not sell or rent information to third parties. Newsletters are usually sent on weekends. You will receive your first edition soon.

The GoAbroad.com newsletter is the comprehensive on-line directory for study abroad programs, language schools, internships, international volunteer positions, international teaching positions, universities, eco-travel and a whole lot more!

You may search archives of our newsletter by visiting: http://www.goabroad.com/newsletter.cfm

If you are not interested in these newsletters and you don't mind missing out on valuable international information we'll be sending in the future, simply click the following LINK:
http://www.goabroad.com/cf_act_Newsletter.cfm?email=".trim($tmpRow["email"])."
or by visiting: http://www.goabroad.com/remove.cfm.

IMPORTANT NOTE FOR HOTMAIL, YAHOO OR WEB BASED EMAIL USERS

Please ensure that your account is maintained and that you indicate that you wish to receive emails from newsletter@goabroad.com.
If not, we may not be able to send you the newsletter or communicate with you in cooperation with new anti spam regulations. We suggest you add newsletter@goabroad.com to your address book. This will help the servers at HOTMAIL recognize us as non spam. WE ONLY SEND INFORMATION THAT YOU HAVE REQUESTED, WE DO NOT SEND SPAM.

If you have any questions please contact me.

Sincerely, 

Troy Peden 

See the world! 
-------------------------------------------------- 
Troy Peden 
troy@goabroad.com 
http://www.GoAbroad.com ";
						break;	
					case MAIL_ADMIN:
						$this->from = $this->email;
						$this->fromName = $this->sendTo;
						$this->replyTo = $this->from;
						$this->email = "marc@goabroad.com";
						break;	
					case WELCOME:
						$siteName = "GoAbroad";
						$signature = "The GoAbroad Network Team";
						if( !is_null($cobrandData) ){
							$siteName = $cobrandData["cobrandSiteName"];
							$signature = $cobrandData["cobrandEmailName"];
							$this->from = ($fromGACOM !== false) ? 'admin@goabroad.net' : 'admin@'.$_SERVER['SERVER_NAME'];
							// $this->from = 'admin@'.$_SERVER['SERVER_NAME'];
							$this->fromName = $cobrandData['cobrandEmailName'];
						}
						$this->subject = "Welcome to the $siteName Network";
						$this->message = "Dear $this->sendTo, 

Welcome to the $siteName Network!

Thank you for creating an account.

";

	$this->message .= "Now, you're a part of our worldwide network of travelers. As a member, you will be able to

    * meet and be friends with travelers from all over the world
    * blog about your travels in your very own travel journals
    * post and share travel photos
    * join/create groups of like minded travelers
    * post messages for your friends and groupmates
    * search for programs for your travels abroad 

    * and lots of other features rolling out in the following weeks!

";

$cond1 = new Condition;
$cond1->setAttributeName("administrator");
$cond1->setOperation(FilterOp::$EQUAL);
$cond1->setValue($travelerID);
$filterCriteria2 = new FilterCriteria2();
$filterCriteria2->addBooleanOp("AND");
$filterCriteria2->addCondition($cond1);

$rowsLimit = new RowsLimit(1,0);

$groupsCreatedByTraveler = GroupPeer::getGroups($filterCriteria2, $orderCriteria2=NULL, $rowsLimit, $asObject=TRUE);

// groupsCreatedByTraveler should contain an array of AdminGroup if registration is for an advisor account
// in that case, the traveler has currently at most 1 advisor group
if( count($groupsCreatedByTraveler) ){
	$groupName = $groupsCreatedByTraveler[0]->getName();
	
	$this->message .= "You have just created the group $groupName. As an Online Site Advisor, you will be able to

    * manage your group and send timely advice, messages, alerts and updates to members
    * blog about your group's activities
    * post and share your group's travel photos
    * keep families updated and informed through simple email broadcasts

";
}

$this->message .= "You can start by editing your profile or you might want to check out who's already on $siteName Network. Click on the following link to Login:
http://" . $uri . "/login.php
For more information, please check out the Frequently Asked Questions by clicking on the following link:
http://" . $uri . "/faq.php  
	
Happy travels,

$signature";
						break;	
					case RESEND_ACTIVATION_EMAIL:
						
						$this->subject = "You requested an activation email from GoAbroad Network";
						$this->message = "Hi! 

You recently created an account on GoAbroad Network, and we are sorry you are having trouble activating it. 

To activate your account, please click on the link below: 
		
http://" . $uri . "/login.php?ref=" . $cryptTravID . " 

Thank you for your patience. 

Happy travels, 
The GoAbroad Network Team

 --- 
If you did not recently sign up for GoAbroad Network, or believe that you got this email by mistake, you may disregard this message. However, receiving this email means that someone recently created a GoAbroad Network account and entered your email address while doing so. If you believe this is the case, don't worry because they won't be able to complete their registration. If you want to create your own account on Goabroad Network, please visit: http://www.goabroad.net.";
						break;	
					case FORGOT_PASSWORD:
						
						/*****************************************************************************************
						 * edits of neri when case is forgot password and user is logged in in cobrand	
						 * 		changed third parameter of constructor into an array type: 	01-07-09
	 					 * 		added cobrandEmailName variable and set it as value of 
	 					 * 			fromName and signature:									01-07-09
	 					 * 		added cobrandSiteName variable and set it as value of
	 					 * 			subject:												01-07-09
						 * 		set from to admin of cobrand:								12-22-08
						 * 		set subject to password recovery of cobrand group name:		12-22-08
						 * 		set signature to cobrand group name: 						12-22-08
						 *****************************************************************************************/
						
						$this->from = ($fromGACOM !== false) ? 'admin@goabroad.net' : 'admin@'.$_SERVER['SERVER_NAME'];
						
						// edited by chris: use site context to get admin email
						require_once('travellog/model/Class.SiteContext.php');
						$this->from = SiteContext::getInstance()->getGanetAdminEmail();
						$this->replyTo = SiteContext::getInstance()->getAdminEmail();
						
						
						if (!is_null($cobrandData)) {
							$this->subject = $cobrandData['cobrandSiteName']." Password Recovery";
							$this->fromName = $cobrandData['cobrandEmailName'];
						}
							
						else 
							$this->subject = "GoAbroad Network Password Recovery";
							
						$this->message = "Dear $this->sendTo,

We apologize for the inconvenience, but for your security, you will be unable to retrieve your old password. Instead, you will need to change it by clicking on the link below:
http://" . $uri . "/changepassword.php?ref=" . $cryptTravID . "&action=entry";

						if (!is_null($cobrandData)) 
							$this->message .= "\n\nWe appreciate your patience, and thank you for using ".$cobrandData['cobrandSiteName'].".\n\nMany thanks, \n\n".$cobrandData['cobrandEmailName']."\nhttp://".$uri;	
						else
							$this->message .= "\n\nWe appreciate your patience, and thank you for using GoAbroad Network.\n\nThe GoAbroad Network Team"; 
						break;	
				}
	 		} 
	 	
	 	}
	 	
	 	function setMessage($message = ""){
	 		$this->message = $message;
	 	}
	 	
	 	function setSubject($subject = ""){
	 		$this->subject = $subject;
	 	}
	 	
	 	function setFrom($from= ""){
	 		$this->from = $from;
	 	}
	 	
	 	function setFromName($fromName = ""){
	 		$this->fromName = $fromName;
	 	}
	 	
	 	function send(){
	 		
			if( ACCOUNT_ACTIVATION == $this->messageType ){
				//don't send account activation notification anymore
				return true;
			}
	
	 		$mail = new PHPMailer();
			$mail->IsSMTP();  // send via SMTP
		
			$mail->From     = $this->from;
			$mail->FromName = $this->fromName;
			//$mail->AddAddress($this->email); 
			
			// word wrap commented out by neri: 02-05-09
			//$mail->WordWrap = 80;                              // set word wrap
			$mail->IsHTML(false);                               // send as HTML
			$mail->Subject = $this->subject;
			$mail->Body = $this->message;
			$mail->AddReplyTo($this->replyTo, $this->fromName);
			
			$developers = array(
				//'jul.garcia@goabroad.com',
				'gapdaphne.notifs@gmail.com'
			);

			$arrSplit = preg_split('/\./', $_SERVER['SERVER_NAME']);
			// if it is in dev or in local send to developers
			if( in_array('dev', $arrSplit) || in_array('local', $arrSplit) ){
				foreach($developers as $iRecipient){
					$mail->AddAddress($iRecipient);
				}
			}else{
				//only send to actual emails in prod
				$mail->AddAddress($this->email);
				// bcc developers
				foreach($developers as $iRecipient){
					$mail->AddBCC($iRecipient);
				}
			}
			
			/**
			* prompt programmer if error while sending email occurred
			*/
			
			if(!$mail->Send()){
			   mail('gapdaphne.notifs@gmail.com', 'error in mail generator', "<p>Mailer Error: " . $mail->ErrorInfo . " <p> to:" . $this->email . "</p><p>from: " . $this->from);
			   return false;
			}
			
			return true;
	 	}
	 	
	 }
	 
?>
