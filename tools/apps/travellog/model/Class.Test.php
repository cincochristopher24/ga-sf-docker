<?php
	/*
	if(!function_exists('__autoload')){
		 function __autoload($class_name){
		 	require_once "Class.". $class_name . '.php';
		 }
	}
	
	class Test{
		private $rs = null;
		private $conn = null;
		
		function Test(){
			try{ 				
 				$this-> conn = new Connection();
				$this-> rs   = new Recordset($this->conn);
				$this-> rs2  = new Recordset($this->conn);
				$this-> rs3  = new Recordset($this->conn);
				$this-> rs4  = new Recordset($this->conn);
			}
			
			catch (exception $e){				   
			   throw $e;
			}
		}
		
		// MAY
		function getMayActiveTravelers(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-05-25' and '2007-05-31' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getMayActiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-05-25' and '2007-05-31' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		function getMayInactiveTravelers(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-05-25' and '2007-05-31' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getMayInactiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-05-25' and '2007-05-31' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		// JUNE
		function getJuneActiveTravelers(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-06-01' and '2007-06-30' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getJuneActiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-06-01' and '2007-06-30' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		function getJuneInactiveTravelers(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-06-01' and '2007-06-30' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getJuneInactiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-06-01' and '2007-06-30' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		// JULY
		function getJulyActiveTravelers(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-07-01' and '2007-07-31' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getJulyActiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-07-01' and '2007-07-31' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		function getJulyInactiveTravelers(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-07-01' and '2007-07-31' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getJulyInactiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-07-01' and '2007-07-31' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		// AUGUST
		function getAugActiveTravelers(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-08-01' and '2007-08-31' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getAugActiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-08-01' and '2007-08-31' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		function getAugInactiveTravelers(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-08-01' and '2007-08-31' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getAugInactiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-08-01' and '2007-08-31' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		// SEPTEMBER
		function getSeptActiveTravelers(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-09-01' and '2007-09-11' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getSeptActiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active > 0 " .
					"and date(dateregistered) between '2007-09-01' and '2007-09-11' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}

		function getSeptInactiveTravelers(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-09-01' and '2007-09-11' and " .
					"travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
		
		function getSeptInactiveAdvisors(){
			$sql = "select travelerID from tblTraveler where active = 0 " .
					"and date(dateregistered) between '2007-09-01' and '2007-09-11' and " .
					"travelerID in (select distinct travelerID from tblGrouptoAdvisor)";
			$this-> rs-> Execute($sql);

			$travelers = array();
			while ($traveler = mysql_fetch_array($this->rs->Resultset())){
					try{
						$travelers[] = new Traveler ($traveler['travelerID']);
					} 
	
					catch (exception $e) {		
						throw $e;
					}
				}
				return $travelers;
		}
	}

	*/
		if(!function_exists('__autoload')){
			 function __autoload($class_name){
			 	require_once "Class.". $class_name . '.php';
			 }
		}

		class Test{
			private $startTime	= 0;
			private $totalTime	= 0;

			function Test(){}

			function startTimer(){
		   		$mtime = microtime();
				$mtime = explode(" ", $mtime);
				$mtime = $mtime[1] + $mtime[0];
				$this->startTime = $mtime;
			}
			function endTimer(){
				$mtime = microtime();
				$mtime = explode(" ", $mtime);
				$mtime = $mtime[1] + $mtime[0];
				$endtime = $mtime;
				$this->totalTime = ($endtime - $this->startTime);

				return "This page was created in ".$this->totalTime." seconds";
			}
		}
?>