<?php
	/**
	 * @(#) Class.Journal.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 24, 2009
	 */
	
	require_once('travellog/model/base/Class.BaseJournal.php');
	
	/**
	 * Model class for journal.
	 */
	class Journal extends BaseJournal {
		
		/**
		 * The journal entry drafts.
		 * 
		 * @var array
		 */
		private $mEntryDrafts = array();
		
		/**
		 * Adds a new journal entry drafts.
		 * 
		 * @param JournalEntryDraft $draft The journal entry draft to be added.
		 * 
		 * @return void
		 */
		function addJournalEntryDraft(JournalEntryDraft $draft){
			$this->mEntryDrafts[] = $draft;
		}
		
		/**
		 * Returns the journal entry drafts of the journal.
		 * 
		 * @return array the journal entry drafts of the journal.
		 */
		function getJournalEntryDrafts(){
			return $this->mEntryDrafts;
		}
		
		/**
		 * Sets the journal entry drafts of the journal.
		 * 
		 * @param array $drafts the journal entry drafts to be set as the drafts of the journal.
		 * 
		 * @return void 
		 */
		function setJournalEntryDrafts(array $drafts){
			$this->mEntryDrafts = array_merge($this->mEntryDrafts, $drafts);
		}
	}