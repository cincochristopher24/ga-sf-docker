<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	require_once('travellog/model/Class.AddressBookEntryPreferenceAction.php');

	class AddressBookEntryPreference
	{
		const GROUP = 0;
		const TRAVELER = 1;
		
		const ON_ADD_JOURNAL = 0;
		const ON_ADD_PHOTO	 = 1;
		
		private $mDb 							= null;	
		private $mAddressBookEntryPreferenceID 	= null;
		private $mAddressBookEntryID 			= null;
		private $mDoerType 						= null;
		private $mDoerID						= null;
				
		function __construct($param=null,$data=null){
			$this->mDb = new dbHandler();
			if(!is_null($param)){
				$qry = 	'SELECT addressBookEntryPreferenceID, addressBookEntryID, doerType, doerID ' .
						'FROM tblAddressBookEntryPreference ' .
						'WHERE addressBookEntryPreferenceID = '.ToolMan::makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(0==$rs->retrieveRecordCount()){
					throw new exception('Invalid AddressBookEntryPreferenceID passed to object of type AddressBookPreferenceID.');		
				}
				$this->init($rs->retrieveRow(0));			
			}
			
			if(is_array($data)){
				$this->init($data);
			}
			
		}
		
		private function init($data){
			$this->setAddressBookEntryPreferenceID($data['addressBookEntryPreferenceID']);
			$this->setAddressBookEntryID($data['addressBookEntryID']);
			$this->setDoerType($data['doerType']);
			$this->setDoerID($data['doerID']);
		}
		
		//setters		
		private function setAddressBookEntryPreferenceID($param=null){
			$this->mAddressBookEntryPreferenceID = $param;
		}
		
		public function setAddressBookEntryID($param=null){
			$this->mAddressBookEntryID = $param;
		}
		
		public function setDoerAsTraveler($param=null){
			$this->setDoerType(self::TRAVELER);
			$this->setDoerID($param);
		}
		
		public function setDoerAsGroup($param=null){
			$this->setDoerType(self::GROUP);
			$this->setDoerID($param);
		}
		
		private function setDoerType($param){
			$this->mDoerType = $param;
		}
		
		private function setDoerID($param){
			$this->mDoerID = $param;
		}
		
		//getters
		public function getAddressBookEntryPreferenceID(){
			return $this->mAddressBookEntryPreferenceID;
		}
		
		public function getAddressBookEntryID(){
			return $this->mAddressBookEntryID;
		}
		
		public function getDoerType(){
			return $this->mDoerType;	
		}
		
		public function getDoerID(){
			return $this->mDoerID;
		}
		
		public function getActions(){
			return AddressBookEntryPreferenceAction::getActionsByAddressBookEntryPreferenceID($this->getAddressBookEntryPreferenceID());
		}
		
		public function isNotifiableOnAddJournal(){
			$actions = $this->getActions();
			foreach($actions as $iAction){
				if($iAction->getAction() == AddressBookEntryPreferenceAction::ACTION_ADD_JOURNAL){
					return true;
				}
			}
			return false;
		}
		
		public function isNotifiableOnAddPhoto(){
			$actions = $this->getActions();
			foreach($actions as $iAction){
				if($iAction->getAction() == AddressBookEntryPreferenceAction::ACTION_ADD_PHOTO){
					return true;
				}
			}
			return false;
		}
		
		//CRUDE		
		public function save(){
			if(!self::isValidDoerType($this->getDoerType())){
				throw new exception('Cannot perform save operation. Invalid doerType '.$this->getDoerType().' passed to object of type AddressBookEntryPreference.');
			}
			if( is_null($this->getAddressBookEntryID()) || 0==$this->getAddressBookEntryID() ){
				throw new exception('Cannot perform save operation. Invalid addresBookEntryID '.$this->getAddressBookEntryID().' passed to object of type AddressBookEntryPreference.');
			}
			if( is_null($this->getDoerID()) || 0==$this->getDoerID() ){
				throw new exception('Cannot perform save operation. Invalid doerID '.$this->getDoerID().' passed to object of type AddressBookEntryPreference.');
			}
			if(is_null( $this->getAddressBookEntryPreferenceID() )){//add
				$qry = 	'INSERT INTO tblAddressBookEntryPreference (' .
							'addressBookEntryID,' .
							'doerType,' .
							'doerID' .
						')' .
						'VALUES (' .
							ToolMan::makeSqlSafeString( $this->getAddressBookEntryID() ).','.
							ToolMan::makeSqlSafeString( $this->getDoerType() ).','.
							ToolMan::makeSqlSafeString( $this->getDoerID() ).
						')';
				$this->mDb->execQuery($qry);
				$this->setAddressBookEntryPreferenceID($this->mDb->getLastInsertedID());
			}
			else{//update
				$qry = 	'UPDATE tblAddressBookEntryPreference SET ' .
							'addressBookEntryID = '.ToolMan::makeSqlSafeString( $this->getAddressBookEntryID() ).','.
							'doerType			= '.ToolMan::makeSqlSafeString( $this->getDoerType() ).','.
							'doerID				= '.ToolMan::makeSqlSafeString( $this->getDoerID() ).' ' .
						'WHERE addressBookEntryPreferenceID = '.ToolMan::makeSqlSafeString( $this->getAddressBookEntryPreferenceID() );
				$this->mDb->execQuery($qry);
			}
		}
		
		public function delete(){
			$qry = 	'DELETE FROM tblAddressBookEntryPreference ' .
					'WHERE addressBookEntryPreferenceID = '.ToolMan::makeSqlSafeString( $this->getAddressBookEntryPreferenceID() );
			$this->clearActions();
			$this->mDb->execQuery($qry);
			$this->clearFields();
		}
		
		public function clearActions(){
			AddressBookEntryPreferenceAction::deleteActionsByAddressBookEntryPreferenceID($this->getAddressBookEntryPreferenceID());	
		}
		
		public function notifyOnAddJournal($flag){
			//check first if there is already an action
			$action = AddressBookEntryPreferenceAction::getActionByAddressBookEntryPreferenceIDAndAction($this->getAddressBookEntryPreferenceID(), AddressBookEntryPreferenceAction::ACTION_ADD_JOURNAL);
			if($flag && is_null($action)){	
				$prefAction = new AddressBookEntryPreferenceAction();
				$prefAction->setAddressBookEntryPreferenceID($this->getAddressBookEntryPreferenceID());
				$prefAction->setAction(AddressBookEntryPreferenceAction::ACTION_ADD_JOURNAL);
				$prefAction->save();
				return true;
			}
			elseif(!$flag && !is_null($action)){
				$action->delete();
				return true;
			}
			return false;
		}
		
		public function notifyOnAddPhoto($flag){
			$action = AddressBookEntryPreferenceAction::getActionByAddressBookEntryPreferenceIDAndAction($this->getAddressBookEntryPreferenceID(), AddressBookEntryPreferenceAction::ACTION_ADD_PHOTO); 
			if($flag && is_null($action)){
				$prefAction = new AddressBookEntryPreferenceAction();
				$prefAction->setAddressBookEntryPreferenceID($this->getAddressBookEntryPreferenceID());
				$prefAction->setAction(AddressBookEntryPreferenceAction::ACTION_ADD_PHOTO);
				$prefAction->save();
				return true;
			}
			elseif(!$flag && !is_null($action)){
				$action->delete();
				return true;
			}
			return false;	
		}
		
		public function clearFields(){
			$this->setAddressBookEntryPreferenceID(null);
			$this->setAddressBookEntryID(null);
			$this->setDoerType(null);
			$this->setDoerID(null);
		}
		
		/*** STATIC FUNCTIONS ***/
		public static function isValidDoerType($param){
			return $param === self::GROUP || $param === self::TRAVELER;
		}
		
		public static function getPreferencesByAddressBookEntryID($addressBookEntryID){
			$db = new dbHandler();
			$qry = 	'SELECT addressBookEntryPreferenceID, addressBookEntryID, doerType, doerID ' .
					'FROM tblAddressBookEntryPreference ' .
					'WHERE addressBookEntryID = '.ToolMan::makeSqlSafeString($addressBookEntryID);
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new AddressBookEntryPreference(null,$rs->retrieveRow());
				}
			}
			return $ar;
		}
		
		public static function getPreferenceByAddressBookEntryIDAndDoerTypAndDoer($entryID,$doerType,$doerID){
			$db = new dbHandler();
			$qry = 	'SELECT addressBookEntryPreferenceID, addressBookEntryID, doerType, doerID ' .
					'FROM tblAddressBookEntryPreference ' .
					'WHERE addressBookEntryID = '.ToolMan::makeSqlSafeString($entryID).' ' .
					'AND doerType = '.ToolMan::makeSqlSafeString($doerType).' ' .
					'AND doerID = '.ToolMan::makeSqlSafeString($doerID);
			$rs = new iRecordset($db->execQuery($qry));
			if($rs->retrieveRecordCount() > 0){
				return new AddressBookEntryPreference(null,$rs->retrieveRow(0));
			}
			return null;
		}
		
		public static function clearPreferencesByAddressBookEntryID($addressBookEntryID){
			$prefs = self::getPreferencesByAddressBookEntryID($addressBookEntryID);
			foreach($prefs as $iPref){
				$iPref->delete();
			}
		}
	}	
?>