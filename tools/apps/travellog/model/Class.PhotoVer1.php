<?php
/**
 * Created on Jul 26, 2006
 * Updated on Sep 7, 2006
 * Created by: Czarisse Daphne P. Dolina
 */
 
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.PhotoType.php");
	require_once("Class.PathManager.php");
	
	class Photo{
		
		// vars for db connection
		private $conn		  = NULL;
 		private $rs		 	  = NULL;
 		
 		// var for context parameter (parameter is an object)
 		private $context		  = NULL;
		
		// vars for class attributes
		private $photoID       = NULL;
		private $phototypeID   = NULL;
		private $caption       = NULL;
		private $filename      = NULL;
		private $thumbfilename = NULL;
		private $primaryphoto  = NULL;
		
		// vars for other methods
        private $arrcomment    = array();
        
		//constructor
		
			function Photo($_context, $_photoID=0){
				$this->context = $_context;
				$this->photoID = $_photoID;
						
				try {
					$this->conn = new Connection();
					$this->rs   = new Recordset($this->conn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				if ($this->photoID > 0){  
					
					$sql = "SELECT * from tblPhoto where photoID = '$this->photoID' ORDER BY photoID DESC";
		 			$this->rs->Execute($sql);
		 			
	 				if ($this->rs->Recordcount() == 0){
	 					throw new Exception("Invalid Photo ID");		// ID not valid so throw exception
	 				}
	 					
	 				// set values to its attributes	 		 			
		 			$this->phototypeID 	= $this->rs->Result(0,"phototypeID"); 
		 			$this->caption 		= stripslashes($this->rs->Result(0,"caption")); 
		 			$this->filename 		= stripslashes($this->rs->Result(0,"filename")); 
		 			$this->thumbfilename = stripslashes($this->rs->Result(0,"thumbfilename"));
					$this->primaryphoto 	= $this->rs->Result(0,"primaryphoto");
						 				
	 			}
	 			
			}
			
			
		//setter
		
			function setPhotoID($_photoID){
				$this->photoID = $_photoID;
			}
			
			function setPhotoTypeID($_phototypeID){
				$this->phototypeID = $_phototypeID;
			}
			
			function setCaption($_caption){
				$this->caption = $_caption;
			}
			
			function setFileName($_filename){
				$this->filename = $_filename;			 
			}
			
			function setThumbFileName($_thumbfilename){
				$this->thumbfilename = $_thumbfilename;			 
			}
			
			function setPrimaryPhoto($_primaryphoto){
				$this->primaryphoto = $_primaryphoto;			 
			}
			
		//getter
		
			function getPhotoID(){
				return $this->photoID;
			}
			
			function getPhotoTypeID(){
				return $this->phototypeID;
			}
			
			function getCaption(){
				return $this->caption;
			}
			
			function getFileName(){
				return $this->filename;	
			}
			
			function getThumbFileName(){
				return $this->thumbfilename;	
			}
			
			function getPrimaryPhoto(){
				return $this->primaryphoto;
			}
			
		//methods
		
			function Create(){ 		
				
				$cap  = addslashes($this->caption);
 				$file = addslashes($this->filename);
 				$thumbfile = addslashes($this->thumbfilename);
				
	 			$sql = "INSERT into tblPhoto (phototypeID, caption, filename, thumbfilename, primaryphoto) VALUES ('$this->phototypeID', '$cap', '$file', '$thumbfile', '$this->primaryphoto' )";
	 			$this->rs->Execute($sql);
	 			
				$this->photoID = $this->rs->GetCurrentID();
				
				if ($this->phototypeID == PhotoType::$PROFILE){
		 			$sql = "INSERT into tblTravelertoPhoto (travelerID, photoID) VALUES (".$this->context->getTravelerID().", '$this->photoID' )";
		 			$this->rs->Execute($sql);
	 			}
	 			else if ($this->phototypeID == PhotoType::$TRAVELLOG){
									
		 			$sql = "INSERT into tblTravelLogtoPhoto (travelLogID, photoID) VALUES (".$this->context->getTravelLogID().", '$this->photoID' )";
		 			$this->rs->Execute($sql);
	 			}
				else if ($this->phototypeID == PhotoType::$RESUME){
									
		 			$sql = "INSERT into tblResumetoPhoto (travelerID, photoID) VALUES (".$this->context->gettravelerID().", '$this->photoID' )";
		 			$this->rs->Execute($sql);
						
	 			}
	 			
				 			 
	 		}
	 		
	 		
	 		function Update(){ 		
	 			
	 			// if selected photo is set to primary, unset other photos (of the same profile OR journal entry) from being primary
		 		if ($this->primaryphoto == 1){
		 			if ($this->phototypeID == PhotoType::$PROFILE){
			 			$sql = "UPDATE tblPhoto, tblTravelertoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelertoPhoto.photoID AND tblTravelertoPhoto.travelerID = '$this->context->getTravelerID()'" ;
		 				$this->rs->Execute($sql);
		 			}
		 			else if ($this->phototypeID == PhotoType::$TRAVELLOG){
			 			$sql = "UPDATE tblPhoto,tblTravelLogtoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelLogtoPhoto.photoID AND tblTravelLogtoPhoto.travelLogID = '$this->context->getTravelLogID()'" ;
		 				$this->rs->Execute($sql); 
		 			}
		 		}
		 		
		 		$cap  = addslashes($this->caption);
 				$file = addslashes($this->filename);
 				
		 		$sql = "UPDATE tblPhoto SET caption = '$cap', filename = '$file', primaryphoto = '$this->primaryphoto' WHERE photoID = '$this->photoID'" ;
	 			$this->rs->Execute($sql);				
	 		}
	 		
	 		
	 		function Delete(){
	 			
	 			if ($this->phototypeID == PhotoType::$PROFILE){
		 			$sql = "DELETE FROM tblTravelertoPhoto where photoID = '$this->photoID' ";
		 			$this->rs->Execute($sql);
		 			
		 			// if deleted photo was a primary photo
	 				if ($this->primaryphoto == 1){
	 					$newobjcontext = new TravelerProfile($this->context->getTravelerID());
	 					if ($newobjcontext->getRandomPhoto())
	 						$newobjcontext->getRandomPhoto()->setAsPrimaryPhoto();
	 				}
	 				
	 			}
	 			else if ($this->phototypeID == PhotoType::$TRAVELLOG){
		 			$sql = "DELETE FROM tblTravelLogtoPhoto where photoID = '$this->photoID' ";
		 			$this->rs->Execute($sql); 
		 			
		 			// if deleted photo was a primary photo
	 				if ($this->primaryphoto == 1){
	 					$newobjcontext = new TravelLog($this->context->getTravelLogID());
	 					if ($newobjcontext->getRandomPhoto())
	 						$newobjcontext->getRandomPhoto()->setAsPrimaryPhoto();
	 				}
	 				
	 			}
				else if ($this->phototypeID == PhotoType::$RESUME){
		 			$sql = "DELETE FROM tblResumetoPhoto where photoID = '$this->photoID' ";
		 			$this->rs->Execute($sql); 
	 			}
	 			
	 			$sql = "DELETE FROM tblPhoto where photoID = '$this->photoID' ";
	 			$this->rs->Execute($sql);
	 				 			
	 		}
	 		 
			 
			function setAsPrimaryPhoto(){
				
				switch($this->phototypeID){
					
					case PhotoType::$PROFILE:					
						$sql = "UPDATE tblPhoto, tblTravelertoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelertoPhoto.photoID AND tblTravelertoPhoto.travelerID = '".$this->context->getTravelerID()."' " ;
		 				$this->rs->Execute($sql);	
                        
                         // set the primary photo as avatar
                         $fullpath =  $this->getThumbnailPhotoLink();
                         $sql = "UPDATE phpbb_users SET user_avatar_type = 3, user_avatar = '$fullpath' WHERE user_id = '".$this->context->getTravelerID()."' " ;
                         $this->rs->Execute($sql);                         
                        					
					break;
					
					case PhotoType::$TRAVELLOG:					
						$sql = "UPDATE tblPhoto,tblTravelLogtoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelLogtoPhoto.photoID AND tblTravelLogtoPhoto.travelLogID = '".$this->context->getTravelLogID()."' " ;
		 				$this->rs->Execute($sql); 
					break;
					
					case PhotoType::$RESUME:					
						$sql = "UPDATE tblPhoto,tblResumetoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblResumetoPhoto.photoID AND tblResumetoPhoto.resumeID = '$this->context->gettravelerID()' " ;
		 				$this->rs->Execute($sql); 
					break;				
				}				
				
				$sql = "UPDATE tblPhoto SET primaryphoto = 1 WHERE photoID = '$this->photoID'" ;
	 			$this->rs->Execute($sql);	
							
			}
	 		
	 	// other methods
	 		
	 		// get photo link 
	 		function getPhotoLink(){
		 		
		 		$showdefimg = true;			// set show default image to true
		 		
		 		if ($this->photoID > 0){
		 			
		 			try {
		 				 $path = new PathManager($this->context);
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$fullpath =  $path->GetPathRelative() . $this->filename;
		 			
		 			if (file_exists($fullpath)){	// if image to be displayed exists in URL. then proceed to display 
		 				$showdefimg = false;
		 				return $fullpath;
		 			}			 			
		 		}
		 		
		 		if ($showdefimg){		// if value is true; show default image
		 			switch (get_class($this->context)){
		 				case "TravelerProfile":
		 					$defaultfile = "profilepic.gif";
		 					break;
		 				case "Resume":
		 					$defaultfile = "profilepic.gif";
		 					break;
		 				case "TravelLog":
		 					$defaultfile = "thumb.gif";
		 					break;
		 			}
		 			return "http://".$_SERVER['SERVER_NAME']."/images/". $defaultfile;
		 		}
		 			 		
	 		}	
	 		
	 		
	 		//get photo link of thumbnail
	 		function getThumbnailPhotoLink(){
		 		
		 		$showdefimg = true;			// set show default image to true
		 		
		 		if ($this->photoID > 0){
		 			try {
		 				 $path = new PathManager($this->context);
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$fullpath =  $path->GetPathRelative() . $this->thumbfilename;
		 			
		 			if (file_exists($fullpath)){	// if image to be displayed exists in URL. then proceed to display 
		 				$showdefimg = false;
		 				return $fullpath;
		 			}		 			
		 		}
		 		
		 	
		 		if ($showdefimg){		// if value is true; show default image
		 			switch (get_class($this->context)){
		 				case "TravelerProfile":
		 					$defaultfile = "profilepic_thumb.gif";
		 					break;
		 				case "Resume":
		 					$defaultfile = "profilepic_thumb.gif";
		 					break;
		 				case "TravelLog":
		 					$defaultfile = "thumb.gif";
		 					break;
		 			}
		 			return "http://".$_SERVER['SERVER_NAME']."/images/" . $defaultfile;
		 		}
	 		}
           
	}
?>