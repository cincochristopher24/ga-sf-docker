<?php
	require_once("Class.Paging.php");
	require_once("travellog/model/Class.GanetDbHandler.php");
	require_once("travellog/model/Class.IModel.php");
	require_once("Class.Criteria2.php");
	
	class NewestTravelerModel implements IModel{
		function GetFilterLists(&$props){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				/*$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM tblTraveler " .
					" WHERE tblTraveler.isSuspended = 0 " .
					" AND tblTraveler.active = 1 " .
					" AND tblTraveler.travelerID NOT IN " .
					"	( " .
					"		SELECT travelerID FROM tblGrouptoAdvisor " .
					" ) ";*/
				$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM tblTraveler " .
					" WHERE tblTraveler.isSuspended = 0 " .
					" AND tblTraveler.active = 1 ";
				if($props['is_login']) {
					$sql .= " AND travelerID <> " .$handler->makeSqlSafeString($props['travelerID']).
						" AND travelerID NOT IN " .
						" ( " .
						"		SELECT userID as travelerID FROM tblBlockedUsers " .
						"		WHERE travelerID = ".$handler->makeSqlSafeString($props['travelerID'])."" .
						" ) ";
				}			
				$sql .= "	ORDER BY tblTraveler.dateregistered DESC " .
					"	LIMIT ".$props['offset'].", ".$props['rows'];

				$props['travelers'] = new ResourceIterator($handler->execute($sql), "Traveler");
				
				$sql = "SELECT FOUND_ROWS() as rows";
				
				$rs = new ResourceIterator($handler->execute($sql));
				$props['travelers_count'] = $rs->getRows();
				$props['obj_paging'] = new Paging($props['travelers_count'], $props['page'],'action=Newest', $props['rows'] );
				$props['view_type'] = 3;
			}
			catch (exception $ex) {}
		}
	}