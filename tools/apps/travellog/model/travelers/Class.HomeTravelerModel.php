<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/model/Class.LocationFactory.php');
require_once('travellog/model/Class.Traveler.php');
require_once("travellog/model/Class.FilterCriteria2.php");
require_once("travellog/model/Class.Condition.php");
require_once('travellog/model/Class.IModel.php');
class HomeTravelerModel implements IModel{
	function GetFilterLists(&$props){
		$obj_factory  = LocationFactory::instance();
		$obj_country  = $obj_factory->create($props['locationID']);
		$col_cities   = $obj_country->getCities();
		$obj_criteria = NULL;
		$arr          = array();
		
		if( count($col_cities) ){
			$arr[] = $props['locationID'];
			foreach( $col_cities as $obj_city ){
				$arr[] = $obj_city->getLocationID();
			}
			
			$obj_condition = new Condition;
			$obj_condition->setAttributeName("htlocationID");
			$obj_condition->setOperation(FilterOp::$IN);
			$obj_condition->setValue( implode( ',', $arr ) );
			
			$obj_criteria = new FilterCriteria2();
			$obj_criteria->addBooleanOp('AND');
			$obj_criteria->addCondition( $obj_condition );
		}  
		
		$col_travelers         = Traveler::getAllTravelers( $obj_criteria );
		
		$props['obj_paging']   = new Paging        ( count($col_travelers), $props['page'],'action=Hometown&locationID='.$props['locationID'], $props['rows'] );
		$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging']                                                                 );
		$props['view_type']    = 1;
	}
}
?>
