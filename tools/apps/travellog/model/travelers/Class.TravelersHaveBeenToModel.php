<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/repository/Class.TravelersHaveBeenTo.php');
require_once('travellog/model/Class.IModel.php');
require_once("Class.Criteria2.php");
class TravelersHaveBeenToModel implements IModel{
	
	function GetFilterLists(&$props){
		$props['obj_criteria'] = new Criteria2;
		if( $props['is_login'] ) $props['obj_criteria']->mustNotBeEqual('travelerID', $props['travelerID']);
		$props['obj_criteria']->setGroupBy('travelerID');
		$props['obj_criteria']->setOrderBy('ranking, travelerID');
		$props['obj_criteria']->setLimit($props['offset'], $props['rows']); 
		$obj_traveler          = new TravelersHaveBeenTo;	
		$props['travelers'] = $obj_traveler->GetFilterList($props);
		$props['travelers_count'] = $obj_traveler->getTotalRecords();
		
		$props['obj_paging']   = new Paging        ($props['travelers_count'], $props['page'],'action=HaveBeenTo&locationID='.$props['locationID'], $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                                    );
		$props['view_type']    = 8;
	}
	
	function GetTravelersCountryWithPastTravels(){
		$obj_traveler = new TravelersHaveBeenTo;
		return $obj_traveler->GetTravelersCountryWithPastTravels();	
	}
	
	function GetTravelersCountryWithPastTravelsJSON(){
		$obj_traveler = new TravelersHaveBeenTo;
		return $obj_traveler->GetTravelersCountryWithPastTravelsJSON();	
	}
}
?>
