<?php
  /**
   * @(#) Class.SubGroupSortType.php
   * 
   * The sort types of subgroups.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 01 5, 09
   */
   
  class SubGroupSortType {
		const ALPHABETICAL = 2;
		const MOST_RECENT_CREATED = 3;
		const CUSTOM = 1;
		const CATEGORY_RANK = 4;
		const SUBGROUP_RANK = 5;
  }
