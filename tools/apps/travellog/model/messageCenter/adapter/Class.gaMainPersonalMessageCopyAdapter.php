<?php

class gaMainPersonalMessageCopyAdapter extends gaMainPersonalMessageAdapter {
	
	public function isRead() {
		return $this->_mOriginalMessage->isRead();
	}
	
	public function getID() {
		return $this->getFormattedID();
	}
	
	public function getFormattedID() {
		return $this->_mOriginalMessage->getID() . '-' . $this->getContentType() . '-' . $this->isRead();
	}
	
}