<?php

class gaNewsCopyMessageAdapter extends gaNewsMessageAdapter {
	
	public function __construct($message) {
		$this->_mOriginalMessage = $message;
	}
	
	public function isRead() {
		return $this->_mOriginalMessage->isRead();
	}
	
	public function getID() {
		return $this->_mOriginalMessage->getID() . '-' . $this->getContentType() . '-' . $this->isRead();
	}
	
}