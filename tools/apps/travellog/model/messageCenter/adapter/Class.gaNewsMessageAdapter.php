<?php

abstract class gaNewsMessageAdapter implements gaMessageAdapterInterface {
	
	protected $_mOriginalMessage;
	
	public function getSenderLink() {
		throw new Exception("Incorrect usage.");
	}
	
	public function __construct($message) {
		$this->_mOriginalMessage = $message;
	}
	
	public function getID() {
		return $this->_mOriginalMessage->getID();
	}
	
	public function getCssClass() {
		if ($this->_mOriginalMessage->isPrioritized()) {
			return 'alerts';
		}
		
		return 'news';
	}
	
	public function getDateCreated() {
		return $this->_mOriginalMessage->getDateCreated();
	}
	
	public function getTitle() {
		return $this->_mOriginalMessage->getTitle();
	}
	
	public function getContent() {
		return $this->_mOriginalMessage->getContent();
	}
	
	public function hasContent() {
		return true;
	}
	
	public function isReplyAllowed() {
		return false;
	}
	
	public function getContentType() {
		return 2;	// gaMessage::NEWS
	}
	
	public function getSender() {
		return $this->_mOriginalMessage->getSender()->getName();
	}
	
	public function getSenderID() {
		return $this->_mOriginalMessage->getSender()->getAdministratorID();
	}
	
	public function getRecipientsLink() {
		// currently unused
		return '';
	}
	
	public function getSenderRealName() {
		return '';
	}
	
	public function getRecipientsRealNameLink() {
		// temporary
		return '';
	}
	
	public function isSenderGAMember(){
		return TRUE;
	}
	
}