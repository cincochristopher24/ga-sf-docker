<?php

class gaCobrandNewsMessageAdapter extends gaNewsMessageAdapter {
	
	protected $_mGroup;
	
	public function __construct(gaNews $news) {
		require_once('travellog/model/Class.SiteContext.php');
		
		$siteContext = SiteContext::getInstance();
		
		$this->_mGroup = gaGroupMapper::getGroup($siteContext->getGroupID());
		parent::__construct($news);
	}
	
	public function getSenderLink() {
		if (!in_array($this->_mOriginalMessage->getSender()->getName(), gaGroupMapper::retrieveCoveredGroupNames($this->_mGroup)))
			return $this->_mOriginalMessage->getSender()->getName();
		else
			return '<a href="' . $this->_mOriginalMessage->getSender()->getFriendlyURL() . '">' . $this->_mOriginalMessage->getSender()->getName() . '</a>';
	}
	
}