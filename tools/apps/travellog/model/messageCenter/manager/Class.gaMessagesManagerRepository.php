<?php

class gaMessagesManagerRepository {
	
	protected static $identityMap = array('1' => array(), '2' => array());
	protected static $groupIdentityMap = array('1' => array(), '2' => array());
	
	private function __construct() {
		
	}
	
	public static function load($ownerID, $siteAccessType) {
		if (!is_numeric($ownerID) || !is_numeric($siteAccessType))
			throw new Exception("Incorrect parameter data types.");
		
		if (array_key_exists($ownerID, self::$identityMap[$siteAccessType])) {
			$manager = self::$identityMap[$siteAccessType][$ownerID];
		} else {
			$manager = gaMessagesManagerFactory::create($siteAccessType);
			// dependency injection
			$manager->setOwnerID($ownerID);
			$manager->init();
			
			self::$identityMap[$siteAccessType][$ownerID] = $manager;
		}
		
		return $manager;
	}
	
	public static function loadGroup($ownerID, $siteAccessType, gaGroup $group){
		if (!is_numeric($ownerID) || !is_numeric($siteAccessType) || !($group instanceof gaGroup) )
			throw new Exception("Incorrect parameter data types.");
			
		if (array_key_exists($group->getID(), self::$groupIdentityMap[$siteAccessType])) {
			$manager = self::$groupIdentityMap[$siteAccessType][$group->getID()];
		} else {
			$manager = gaMessagesManagerFactory::createGroup($siteAccessType,$group);
			// dependency injection
			$manager->setOwnerID($ownerID);
			$manager->init();

			self::$groupIdentityMap[$siteAccessType][$group->getID()] = $manager;
		}

		return $manager;
	}
	
}

/**
 * autoloading is a PHP feature that is so helpful so programmers could concentrate on
 * programming and no longer burden themselves on including files. It's just natural that
 * we should use it.
 */
function messageCenterAutoLoader($class_name)
{
    //class directories
    $modelPath = realpath(dirname(__FILE__) . '/..');
    $classPrefix = 'Class.';
    
    $directories = array (
        '/',
        '/adapter/',
        '/data_mapper/',
        '/manager/',
        '/personal_message/',
        '/news/',
    );

    // scan all valid directories
    foreach($directories as $directory)
    {
        if(file_exists($modelPath . $directory . $classPrefix . $class_name . '.php'))
        {
            require_once($modelPath . $directory . $classPrefix . $class_name . '.php');
            return;
        }
    }
}

spl_autoload_register("messageCenterAutoLoader");