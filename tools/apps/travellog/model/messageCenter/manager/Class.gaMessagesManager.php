<?php

abstract class gaMessagesManager {
	
	protected $_mOwnerID;
	
	protected $_mCollPersonalMessages = array();
	protected $_mCollSentMessages = array();
	
	protected $_mCollOrdinaryNews = array();
	protected $_mCollPrioritizedNews = array();
	
	protected $_mCollShoutOuts = array();
	protected $_mCollSurveryAlerts = array();
	protected $_mCollResourceUploadAlerts = array();
	
	abstract protected function loadShoutOuts();
	abstract protected function loadReceivedMessages();
	abstract protected function loadSentMessages();
	abstract protected function wrapNews(array $collNews);
	
	public function init() {
		$this->loadPersonalMessages();
		$this->loadNews();
		$this->loadShoutOuts();
	}
	
	public function getMessages(array $messageTypes) {
		foreach ($messageTypes as $key => $type) {
			$messageTypes[$key] = strtoupper($type);
		}
		
		$collMessages = array();
		
		if (in_array('NEWS', $messageTypes))
			$collMessages = array_merge($collMessages, $this->_mCollOrdinaryNews);
		
		if (in_array('PERSONAL', $messageTypes))
			$collMessages = array_merge($collMessages, $this->_mCollPersonalMessages);
		
		if (in_array('SHOUTOUT', $messageTypes))
			$collMessages = array_merge($collMessages, $this->_mCollShoutOuts);
		
		usort($collMessages, array('gaMessageDM', 'orderByDate'));
		$collMessages = array_reverse($collMessages);
		
		if (in_array('NEWS', $messageTypes))
			$collMessages = array_merge($this->_mCollPrioritizedNews, $collMessages);
		
		return $collMessages;
	}
	
	protected function loadNews() {
		$collNews = gaNewsDM::retrieveNews($this->_mOwnerID);
		$this->wrapNews($collNews);
		
		usort($this->_mCollPrioritizedNews, array('gaMessageDM', 'orderByDate'));
		$this->_mCollPrioritizedNews = array_reverse($this->_mCollPrioritizedNews);
		
		usort($this->_mCollOrdinaryNews, array('gaMessageDM', 'orderByDate'));
		$this->_mCollOrdinaryNews = array_reverse($this->_mCollOrdinaryNews);
	}
	
	protected function loadPersonalMessages() {
		$this->loadReceivedMessages();
		$this->loadSentMessages();
	}
	
	public function getNewsOnly() {
		return array_merge($this->_mCollPrioritizedNews, $this->_mCollOrdinaryNews); 
	}
	
	public function getInboxMessages() {
		$collMessages = array_merge(
							$this->_mCollPersonalMessages,
							$this->_mCollOrdinaryNews,
							$this->_mCollShoutOuts
						);
						
		usort($collMessages, array('gaMessageDM', 'orderByDate'));
		$collMessages = array_reverse($collMessages);
		
		return array_merge($this->_mCollPrioritizedNews, $collMessages);
	}
	
	public function getInboxWihtoutShoutOutMessages() {
		$collMessages = array_merge(
							$this->_mCollPersonalMessages,
							$this->_mCollOrdinaryNews
						);
						
		usort($collMessages, array('gaMessageDM', 'orderByDate'));
		$collMessages = array_reverse($collMessages);
		
		return array_merge($this->_mCollPrioritizedNews, $collMessages);
	}
	
	public function getSentMessages() {
		return $this->_mCollSentMessages;
	}

	public function setOwnerID($ownerID) {
		if (!is_numeric($ownerID))
			throw new Exception('Integer value expected.');
		
		$this->_mOwnerID = (int) $ownerID;
	}
	
	public function countUnreadMessages() {
		$ctr = 0;
		
		foreach ($this->getInboxMessages() as $message) {
			if (!$message->isRead())
				$ctr++;
		}
		
		return $ctr;
	}
	
	public function countUnreadNews() {
		$ctr = 0;
		
		foreach ($this->getNewsOnly() as $message) {
			if (!$message->isRead())
				$ctr++;
		}
		
		return $ctr;
	}
		
}