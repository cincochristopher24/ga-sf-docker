<?php

class gaMessagesManagerFactory {
	
	private function __construct() {
		
	}
	
	public static function create($siteAccessType) {
		if (!is_numeric($siteAccessType))
			throw new Exception('Integer value expected.');
		
		switch ((int) $siteAccessType) {
			case 1:		// MAIN
				return new gaMainMessagesManager();
				
			case 2:		// COBRAND
				return new gaCobrandMessagesManager();
			
			default:
				throw new Exception('Invalid value.');
		}
	}
	
	public static function createGroup($siteAccessType, gaGroup $group){
		if (!is_numeric($siteAccessType)){
			throw new Exception('Integer value expected.');
		}
		if( !($group instanceof gaGroup) ){
			throw new Exception('Expected instance of gaGroup.');
		}
		switch ((int) $siteAccessType) {
			case 1:		// MAIN
				return new gaMainGroupMessagesManager($group);
				
			case 2:		// COBRAND
				return new gaCobrandGroupMessagesManager($group);
			
			default:
				throw new Exception('Invalid value.');
		}
	}
	
}