<?php

class gaCobrandMessagesManager extends gaMessagesManager {
	
	protected function wrapNews(array $collNews) {
		foreach ($collNews as $message) {
			if ($message->isPrioritized()) {
				$this->_mCollPrioritizedNews[] = new gaCobrandNewsCopyMessageAdapter($message); 
			} else {
				$this->_mCollOrdinaryNews[] = new gaCobrandNewsCopyMessageAdapter($message);
			}
		}
	}
	
	protected function loadShoutOuts() {
		$collShoutOuts = gaMessageDM::retrieveTravelerShoutOuts($this->_mOwnerID);
		foreach ($collShoutOuts as $message) {
			$this->_mCollShoutOuts[] = new gaCobrandShoutOutMessageAdapter($message);
		}
	}
	
	protected function loadReceivedMessages() {
		$collReceivedMessages = gaPersonalMessageDM::retrievePersonalMessages($this->_mOwnerID);
		
		foreach ($collReceivedMessages as $message) {
			$this->_mCollPersonalMessages[] = new gaCobrandPersonalMessageCopyAdapter($message);
		}
		
		usort($this->_mCollPersonalMessages, array('gaMessageDM', 'orderByDate'));
		$this->_mCollPersonalMessages = array_reverse($this->_mCollPersonalMessages);
	}
	
	protected function loadSentMessages() {
		$collSentMessages = gaPersonalMessageDM::retrieveSentMessages($this->_mOwnerID);
		
		foreach ($collSentMessages as $message) {
			$this->_mCollSentMessages[] = new gaCobrandPersonalMessageAdapter($message);
		}
		
		usort($this->_mCollSentMessages, array('gaMessageDM', 'orderByDate'));
		$this->_mCollSentMessages = array_reverse($this->_mCollSentMessages);
	}
	
}