<?php

require_once 'Class.Connection.php';
require_once 'Class.Recordset.php';

require_once 'Class.gaGroup.php';

class gaGroupMapper {

	protected static $identityMap = array();
	

	public static function retrieveSubGroupIDs($groupID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT groupID " .
				 "FROM tblGroup " .
				 "WHERE parentID = $groupID";
		$rs->Execute($query);
		
		$collIDs = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_assoc($rs->Resultset())) {
				$collIDs[] = $row['groupID'];
			}
		}
		
		return $collIDs;
	}
	
	public static function retrieveMemberIDs($groupID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT t.travelerID " .
				 "FROM tblGrouptoTraveler gt " .
				 "INNER JOIN tblTraveler t " .
				 "ON gt.travelerID = t.travelerID " .
				 "WHERE gt.groupID = $groupID " .
				 "AND t.isSuspended = 0 ".
				 "AND t.deactivated = 0";
		$rs->Execute($query);
		
		$memberIDs = array();
		if ($rs->Recordcount() > 0) {
			while($row = mysql_fetch_assoc($rs->Resultset())) {
				$memberIDs[] = $row['travelerID'];
			}
		}
		
		return $memberIDs;
	}
	
	public static function isGroupMember($travelerID, $groupID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT travelerID " .
				 "FROM tblGrouptoTraveler " .
				 "WHERE groupID = $groupID " .
				 "AND travelerID = $travelerID " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			return true;	// member
		}
		
		return false;
	}
	
	public static function retrieveGroupsByGroupNames(array $collGroupNames) {
		$rs = new Recordset(new Connection);
		
		$cleanedGroupNames = array();
		foreach ($collGroupNames as $name) {
			$cleanedGroupNames[] = "'" . mysql_real_escape_string($name) . "'";
		}
		
		$query = "SELECT * " .
				 "FROM tblGroup " .
				 "WHERE name IN (" . join(',', $cleanedGroupNames) . ")";
		$rs->Execute($query);
		
		$collGroups = array();
		if ($rs->Recordcount() > 0) {
			$collGroups = self::loadFromRS($rs);
		}
		
		return $collGroups;
	}
	
	public static function retrieveCoveredGroupNames($group) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT name " .
				 "FROM tblGroup " .
				 "WHERE parentID = {$group->getID()} " .
				 "OR groupID = {$group->getID()}";
		$rs->Execute($query);
		
		$collGroupNames = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_assoc($rs->Resultset())) {
				$collGroupNames[] = $row['name'];
			}
		}
		
		return $collGroupNames;
	}
	
	/**
	 * retrieves the group being advised by the traveler
	 */
	public static function retrieveAdvisedGroup($travelerID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT * " .	
				 "FROM tblGroup " .
				 "WHERE administrator = $travelerID " .
				 "AND discriminator = 2 " .			// 2 = ADMIN GROUP
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() <= 0) {
			return null;
		}
		
		$group = self::loadFromRS($rs); 
		
		return $group[0];
	}
	
	/**
	 * checks to see if the traveler is an advisor of any group
	 * 
	 * @param travelerID of the traveler
	 * 
	 * @return boolean
	 */
	public static function isAnAdvisor($travelerID) {
		$group = self::retrieveAdvisedGroup($travelerID);
		
		if (!is_null($group)) {
			return true;
		}
		
		return false;
	}
	
	public static function getGroupWithSendableID($sendableID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT * " .
				 "FROM tblGroup " .
				 "WHERE sendableID = $sendableID " .
				 "LIMIT 1";

		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			$group = self::loadFromRS($rs);
			
			return $group[0];
		} else {
			throw new Exception ("Sendable ID: $sendableID not found in tblGroup.");
		}
	}
	
	public static function getGroupsBySendableIDs(array $keys) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT * " .
				 "FROM tblGroup " .
				 "WHERE sendableID IN (" . join(',', $keys) . ")";
		$rs->Execute($query);
		
		return self::loadFromRS($rs);
	}
	
	public static function getGroupsByPrimaryKeys(array $keys) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT * " .
				 "FROM tblGroup " .
				 "WHERE groupID IN (" . join(',', $keys) . ")";
		$rs->Execute($query);
		
		return self::loadFromRS($rs);
	}
	
	public static function getGroup($groupID) {
		if (array_key_exists($groupID, self::$identityMap)) {
			return self::$identityMap[$groupID];
		}
		
		$rs = new Recordset(new Connection);
		
		$query = "SELECT * " .	
				 "FROM tblGroup " .
				 "WHERE groupID = $groupID " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() <= 0) {
			return null;
		}
		
		$group = self::loadFromRS($rs); 
		
		return $group[0];
	}
	
	public static function getGroupOfAdvisor($advisor) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT * " .
				 "FROM tblGroup " .
				 "WHERE administrator = {$advisor->getID()}";
		
		$rs->Execute($query);
		
		if ($rs->Recordcount() <= 0) {
			throw new Exception ("Could not find group of advisor with travelerID {$advisor->getID()}.");
		}
		
		$group = self::loadFromRS($rs);
		
		return $group[0];
	}
	
	public static function getAdministeredGroups($administrator){
		$rs = new Recordset(new Connection);
		
		$query = "SELECT *
					FROM tblGroup
					WHERE administrator = {$administrator->getID()}
						AND parentID = 0";
		
		$rs->Execute($query);
		
		$groups = self::loadFromRS($rs);
		
		return $groups;
	}
	
	public static function getMainGroupsJoined($traveler){
		$rs = new Recordset(new Connection);
		
		$query = "SELECT tblGroup.*
					FROM tblGrouptoTraveler, tblGroup
					WHERE tblGrouptoTraveler.travelerID = {$traveler->getID()}
						AND tblGrouptoTraveler.groupID = tblGroup.groupID
						AND tblGroup.parentID = 0
					GROUP BY tblGroup.groupID";
		
		$rs->Execute($query);
		
		$groups = self::loadFromRS($rs);
		
		return $groups;
	}
	
	public static function loadFromRS($rs) {
		$collGroups = array();
		while ($row = mysql_fetch_assoc($rs->Resultset())) {
			$collGroups[] = self::doLoad($row);
		}
		
		return $collGroups;
	}
	
	protected function doLoad($row) {	
		$group = new gaGroup();
		
		$group->setID              ( $row['groupID']       );
		$group->setName            ( $row['name']          );
		$group->setSendableID      ( $row['sendableID']    );
		$group->setAdministratorID ( $row['administrator'] );
		$group->setDiscriminator   ( $row['discriminator'] );
		$group->setMemberIDs       ( self::retrieveMemberIDs($row['groupID']) );
		
		if ($row['parentID'] == 0) {
			$group->setIsParent (TRUE);
		} else {
			$group->setIsParent (FALSE);
		}
		
		if (!$group->isParent()) {
			$group->setParentGroup(self::getGroup($row['parentID']));
		}
		
		// NOTE: There is a serious data flaw in tblGrouptoFacilitator because the travelerID
		// of a superstaff is replicated to every subgroup thus creating massive duplication of data
		require_once 'Class.gaTravelerMapper.php';
		$group->setStaffs(gaTravelerMapper::getGroupStaffs($group));
		
		self::$identityMap[$group->getID()] = $group;
		
		return $group;
	}
	
}