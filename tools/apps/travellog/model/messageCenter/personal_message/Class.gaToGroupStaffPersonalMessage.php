<?php

class gaToGroupStaffPersonalMessage extends gaPersonalMessage {
	
	protected $_mCollGroupIDs = array();
	protected $_mCollGroups = array();
	
	public function getGroupIDs() {
		return $this->_mCollGroupIDs;
	}
	
	public function getGroups() {
		return $this->_mCollGroups;
	}
	
	// SETTERS
	public function setGroupIDs(array $v) {
		$this->_mCollGroupIDs = $v;
	}
	
	public function setGroups(array $v) {
		$this->_mCollGroups = $v;
	}
	
	public function getDisplayedRecipients() {
		return $this->getGroups();
	}
	
	public function send() {
		gaPersonalMessageDM::sendToGroupStaff($this);
	}
	
}