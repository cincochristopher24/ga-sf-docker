<?php

require_once 'Class.Connection.php';
require_once 'Class.Recordset.php';
require_once('Cache/ganetCacheProvider.php');
require_once 'travellog/model/Class.Article.php';
require_once 'travellog/model/Class.TravelLog.php';

class gaMessageDM {
	
	/**
	 * retrieves shoutouts/comments of a traveler from his profile
	 * to his photos and journal entries
	 * 
	 * @param travelerID - the traveler
	 * 
	 * @return collection of shoutouts/comments
	 */
	public static function retrieveTravelerShoutOuts($travelerID) {
		require_once 'travellog/model/Class.Traveler.php';
		
		$collMessages = array();
		
		// profile
		$traveler = new Traveler($travelerID);
		$travelerProfile = $traveler->getTravelerProfile();
		
		$collMessages = $travelerProfile->getComments(null, true);
		
		// journal-entry
 	 	$tlogs = Travellog::getTravellogs($traveler);
 	 	foreach ($tlogs as $tlog){
 	 		$collMessages = array_merge($collMessages, $tlog->getComments(null, true));
 	 	}

		// get photo comments
		/*$photos = $travelerProfile-> getPhotos();
 	 	foreach ($photos as $photo){
 	 		$collMessages = array_merge($collMessages, $photo->getComments(null, true));
 	 	}
 	 	
 	 	foreach ($collMessages as $message) {
 	 		$message->setTravelerRecipientID($travelerID);
 	 		$message->setIsRead(true);
 	 	}*/
 	 	
		// articles
		$dummy = new Article();
		$articles = $dummy->getArticles('author',$travelerID); 
		foreach($articles as $article){
			$collMessages = array_merge($collMessages, $article->getComments());
		}

		$group = gaGroupMapper::retrieveAdvisedGroup($travelerID);
		
		if (!is_null($group)) {
			return array_merge($collMessages, self::retrieveGroupShoutOuts($group->getID()));
		}
 	 	
 	 	return $collMessages;
    }
    
    public static function retrieveGroupShoutOuts($groupID) {
    	require_once 'travellog/model/Class.AdminGroup.php';
		
		$group = new AdminGroup($groupID);
		
		$collMessages = array();

		$tlogs = Travellog::getTravellogs($group);

		// get journal-entry comments
 	 	foreach ($tlogs as $tlog){
 	 		// journal entry comments
 	 		$collMessages = array_merge($collMessages, $tlog->getComments(null, true));

			// journal entry photo comments
			/*$photos = $tlog->getPhotos();
			foreach ($photos as $photo){
				$collMessages = array_merge($collMessages, $photo->getComments(null, true));
			}*/
 	 	}

		// articles
		$dummy = new Article();
		$articles = $dummy->getArticles('group',$groupID); 
		foreach($articles as $article){
			$collMessages = array_merge($collMessages, $article->getComments());
		}

		// get comments for photos in photo album
		/*$album = $group->getPhotoAlbums();
		foreach ($album as $indAlbum){
			$photos = $indAlbum->getPhotos();
			foreach ($photos as $photo){
				$collMessages = array_merge($collMessages, $photo->getComments(null, true));
			}
		}*/
 
		return $collMessages;
    }
	
	public function orderByDate($l, $r){
		if ($l->getDateCreated() == $r->getDateCreated())
		   return 0;
		elseif ($l->getDateCreated() < $r->getDateCreated())
			return -1;
		else 
			return 1;	  
	}
	
	public static function markCopyAsRead($ownerID, $messageID){
		$rs = new Recordset(new Connection);
		
		$query = "UPDATE tblComment SET isread = 1 WHERE commentID = '$messageID'";
		$rs->Execute($query);
		
		/*$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache))
			$cache->delete('ganet_newscopy_' . $travelerID);*/	
	}
	
}