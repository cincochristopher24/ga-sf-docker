<?php

require_once 'Class.Connection.php';
require_once 'Class.Recordset.php';
require_once('Cache/ganetCacheProvider.php');

/**
 * MEMCACHE KEY FORMAT: ganet_pmcopy_{travelerID}, ganet_sentpm_{travelerID}
 */

class gaPersonalMessageDM {
	
	//############################### START MIGRATION CODE ########################################333
	
	public static $noValidRecipients = 0;
	
	/**
	 * WARNING: TO AVOID THIS MIGRATION CODE TO BE EXECUTED UNNECESSARILY,
	 * PLEASE SET THE ACCESS TYPE OF THE FUNCTION TO PRIVATE AFTER A SUCCESSFUL
	 * MIGRATION - THANK YOU
	 * 
	 * naldz version
	 */
	public static function batchMigratePersonalMessages() {
		$rs = new Recordset(new Connection);
		
		$query = "TRUNCATE tblPersonalMessage";
		$rs->Execute($query);
		
		$query = "TRUNCATE tblPersonalMessageRecipients";
		$rs->Execute($query);
		
		$query = "TRUNCATE tblPersonalMessageToGroup";
		$rs->Execute($query);
		
		$query = "SELECT m.*, a.recipientID, a.messageType, a.isRead, s.sendableType, s2.sendableType AS rSendableType " .
				 "FROM tblMessages m " .
				 "INNER JOIN tblMessageToAttribute a " .
				 "ON m.messageID = a.messageID " .
				 "INNER JOIN tblSendable s " .
				 "ON m.senderID = s.sendableID " .
				 "INNER JOIN tblSendable s2 " .
				 "ON a.recipientID = s2.sendableID " .
				 "WHERE m.discriminator = 3 " .
				 "AND (a.messageType = 1 " .	// non-spam
				 "OR a.messageType = 5) " .		// spam
				 "ORDER BY m.messageID";
		$rs->Execute($query);

		$oldMessageID = 0;
		$recipientIDs = array();

		$oldRow = mysql_fetch_assoc($rs->Resultset());
		$oldMessageID = $oldRow['messageID'];
		mysql_data_seek($rs->Resultset(), 0);
		while ($row = mysql_fetch_assoc($rs->Resultset())) {
			if ($oldMessageID != $row['messageID']) {
				//get the senderID
				$senderID = self::getTravelerIdBySendableID($oldRow['sendableType'], $oldRow['senderID']);

				//migrate now
				self::migrate($oldRow, $senderID, $recipientIDs);
				//reinitialize vars
				$recipientIDs = array();
				$oldMessageID = $row['messageID'];
				$oldRow = $row;
			}
			
			//keep on stacking the recipient IDs and message status
			$travelerRecipientID = self::getTravelerIdBySendableID($row['rSendableType'], $row['recipientID']);
			if(!is_null($travelerRecipientID) && !array_key_exists($travelerRecipientID, $recipientIDs)){
				$recipientIDs[$travelerRecipientID]["isRead"] = $row['isRead'];
				$recipientIDs[$travelerRecipientID]["isSpam"] = $row["messageType"] == 5 ? 1 : 0;
			}
		}
		
		
		// 5. MIGRATE LAST ROW DATA
		$senderID = self::getTravelerIdBySendableID($oldRow['sendableType'], $oldRow['senderID']);
		self::migrate($oldRow, $senderID, $recipientIDs);
	}
	
	private static function migrate($row, $senderID, array $recipientIDs) {
		if (count($recipientIDs) == 0) {
			self::$noValidRecipients++;
		}
		
		$rs  = new Recordset(new Connection);
		$rs2 = new Recordset(new Connection);
		
		$query = "INSERT INTO tblPersonalMessage (dateCreated, senderID, subject, body) " .
				 "VALUES ('%s', %d, '%s', '%s')";
		$query = sprintf($query,
					mysql_real_escape_string($row['dateCreated']),
					$senderID,
					mysql_real_escape_string($row['title']),
					mysql_real_escape_string($row['message'])
				);
		$rs->Execute($query);
		
		// insert recipients
		foreach ($recipientIDs as $travelerID => $recipient) {
			$query = "INSERT INTO tblPersonalMessageRecipients (messageID, recipientID, isRead, isSpam) " .
				 	 "VALUES (LAST_INSERT_ID(), $travelerID, {$recipient['isRead']}, {$recipient['isSpam']})";
			$rs->Execute($query);
		}
		
		// check if there are group recipients
		$query = "SELECT g.groupID " .
				 "FROM tblMessageToAttribute a " .
				 "INNER JOIN tblGroup g " .
				 "ON a.recipientID = g.sendableID " .
				 "AND a.messageType = 1 " .
				 "AND a.messageID = " . $row['messageID'];
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			while ($record = mysql_fetch_assoc($rs->Resultset())) {
				$query = "INSERT INTO tblPersonalMessageToGroup(messageID, groupID) " .
						 "VALUES (LAST_INSERT_ID(), %d)";
				$query = sprintf($query, $record['groupID']);
				$rs2->Execute($query);
			}
		}
		
	}
	
	private function getTravelerIdBySendableId($type, $sendableID) {
		$rs = new Recordset(new Connection);
		
		if ($type == 1 || $type == 3) {
			$query = "SELECT travelerID " .
					 "FROM tblTraveler " .
					 "WHERE sendableID = " . $sendableID." ".
					 "LIMIT 0,1";
			$rs->Execute($query);
		} else {
			$query = "SELECT administrator AS travelerID " .
					 "FROM tblGroup " .
					 "WHERE sendableID = " . $sendableID." ".
					 "LIMIT 0,1";
			$rs->execute($query);
		}
		
		$row = mysql_fetch_assoc($rs->Resultset());
		
		return $row['travelerID'];
	}
	
	//################################## END MIGRATION CODE #####################################
	
	public static function delete($ownerID, $messageID) {
		$rs = new Recordset(new Connection);
		
		$query = "UPDATE tblPersonalMessage " .
				 "SET isDeleted = 1 " .
				 "WHERE senderID = $ownerID " .
				 "AND id = $messageID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache)){
			$cache->delete('ganet_pmcopy_' . $ownerID);
			$cache->delete('ganet_sentpm_' . $ownerID);
		}
	}
	
	public static function batchDelete($ownerID, array $collMessageIDs) {
		$rs = new Recordset(new Connection);
		
		foreach ($collMessageIDs as $messageID) {
			$query = "UPDATE tblPersonalMessage " .
				 	 "SET isDeleted = 1 " .
				 	 "WHERE senderID = $ownerID " .
				 	 "AND id = $messageID";
			$rs->Execute($query);
		}
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache)){
			$cache->delete('ganet_pmcopy_' . $ownerID);
			$cache->delete('ganet_sentpm_' . $ownerID);
		}
	}

	/**
	 * removes a personal message copy
	 * 
	 * @param ownerID - the owner of the copy
	 * @param messageID - the message copy to be removed
	 */
	public static function deleteCopy($ownerID, $messageID) {
		$rs = new Recordset(new Connection);
		
		/*$query = "DELETE FROM tblPersonalMessageRecipients " .
				 "WHERE messageID = $messageID " .
				 "AND recipientID = $ownerID";*/
		$query = "UPDATE tblPersonalMessageRecipients
				  SET isDeleted = 1
				  WHERE messageID = $messageID
				  AND recipientID = $ownerID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache)){
			$cache->delete('ganet_pmcopy_' . $ownerID);
			$cache->delete('ganet_sentpm_' . $ownerID);
		}
	}
	
	/**
	 * removes a copy of a personal message
	 * 
	 * @param ownerID - the owner of the message
	 * @param collMessageIDs - the messages to be deletee
	 */
	public static function batchDeleteCopy($ownerID, array $collMessageIDs) {
		$rs = new Recordset(new Connection);
		
		/*$query = "DELETE FROM " .
				 "tblPersonalMessageRecipients " .
				 "WHERE messageID IN (" . join(',', $collMessageIDs) . ") " .
				 "AND recipientID = $ownerID";*/
		$messageIDs = join(',', $collMessageIDs);
		$query = "UPDATE tblPersonalMessageRecipients
				  SET isDeleted = 1
				  WHERE messageID IN ($messageIDs)
				  AND recipientID = $ownerID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache)){
			$cache->delete('ganet_pmcopy_' . $ownerID);
			$cache->delete('ganet_sentpm_' . $ownerID);
		}
	}
	
	/**
	 * mark personal messages as unread by batch
	 * 
	 * @param ownerID - the owner of the message copy
	 * @param collMessageIDs - primary keys of the messages to be processed
	 */
	public static function batchMarkCopyAsUnread($ownerID, array $collMessageIDs) {
		self::batchMarkCopy($ownerID, $collMessageIDs, 0);
	}
	
	/**
	 * mark personal messages as read by batch
	 * 
	 * @param ownerID - the owner of the message copy
	 * @param collMessageIDs - primary keys of the messages to be processed
	 */
	public static function batchMarkCopyAsRead($ownerID, array $collMessageIDs) {
		self::batchMarkCopy($ownerID, $collMessageIDs, 1);
	}

	private static function batchMarkCopy($ownerID, array $collMessageIDs, $readStatus) {
		$rs = new Recordset(new Connection());
		
		$query = "UPDATE tblPersonalMessageRecipients SET " .
				 "isRead = $readStatus " .
				 "WHERE messageID IN (" . join(',', $collMessageIDs) . ") " .
				 "AND recipientID = $ownerID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache))
			$cache->delete('ganet_pmcopy_' . $ownerID);
	}
	
	/**
	 * mark a personal message as read
	 * 
	 * @param ownerID - the owner of the message copy
	 * @param messageID of the personal message
	 */
	public static function markCopyAsRead($ownerID, $messageID) {
		$rs = new Recordset(new Connection());
		
		$query = "UPDATE tblPersonalMessageRecipients SET " .
				 "isRead = 1 " .
				 "WHERE messageID = $messageID " .
				 "AND recipientID = $ownerID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache))
			$cache->delete('ganet_pmcopy_' . $ownerID);
	}
	
	/**
     * @author jonas
     * retrieves personal messages of a traveler
     * 
     * Note: This is part of a refactoring effort to clean and organize personal messages
     * It is now fetching data from tblPersonalMessage and tblPersonalMessageRecipients
     * 
     * @param travelerID - the owner of the messages
     * 
     * @return collection of personal messages
     */
    public static function retrievePersonalMessages($travelerID) {
    	$cache = ganetCacheProvider::instance()->getCache();
		
		if (!is_null($cache) && ($messages = $cache->get('ganet_pmcopy_' . $travelerID)))
    		;
    	else {
	    	$rs = new Recordset(new Connection());
	    	
	    	$query = "SELECT DISTINCT m.*, r.isRead, g.groupID " .
	    			 "FROM tblPersonalMessage AS m " .
	    			 "INNER JOIN tblPersonalMessageRecipients AS r " .
	    			 "ON m.id = r.messageID " .
	    			 "INNER JOIN tblTraveler AS t " .
	    			 "ON t.travelerID = m.senderID " .
	    			 "INNER JOIN tblTraveler AS st " .
	    			 "ON m.senderID = st.travelerID " .
	    			 "LEFT JOIN tblPersonalMessageToGroup AS g " .
	    			 "ON m.id = g.messageID " .
	    			 "WHERE r.recipientID = $travelerID " .
	    			 "AND st.isSuspended = 0 " .			// message sender must not be suspended
	    			 //"AND st.travelerID NOT IN (SELECT travelerID FROM tblBlockedUsers) " .
	    			 "AND r.isSpam = 0 " .					// do not retrieve spam messages
					 "AND r.isDeleted = 0 ".
					 "AND m.senderID != {$travelerID} ".
					 "GROUP BY m.id ".
	    			 "ORDER BY m.id DESC, m.dateCreated DESC";
	    	$rs->Execute($query);
	
			$blockedUserIDs = gaTravelerMapper::getUserIDsBlockedByTraveler($travelerID);
	
	    	$messages = array();
	    	if ($rs->Recordcount() > 0) {
	    		$previousMessageID = 0;
	    		while ($row = mysql_fetch_assoc($rs->Resultset())) {
					if( in_array($row["senderID"],$blockedUserIDs) ){
						continue;
					}
	    			if (is_null($row['groupID'])) {
	    				$message = self::loadMessageToTraveler($travelerID, $row);
	    			} else {
	    				// LOAD TO GROUP STAFF MESSAGE
	    				if ($previousMessageID != $row['id']) {
	    					$previousMessageID = $row['id'];
							
							$message = gaPersonalMessageCopyFactory::create(gaPersonalMessage::TO_GROUP_STAFF);
							self::loadMessage($travelerID, $message, $row);
							
							$message->setGroupIDs(array($row['groupID']));
							$message->setGroups(array(gaGroupMapper::getGroup($row['groupID'])));
	    				} else {
	    					$message->setGroupIDs(array_merge(array($row['groupID']), $message->getGroupIDs()));
	    					$message->setGroups(array_merge(array(gaGroupMapper::getGroup($row['groupID'])), $message->getGroups()));
	    				}
	    			}
	    			
	    			$messages[] = $message;
	    		}
	    	}
	    	
	    	if (!is_null($cache))
	    		$cache->set('ganet_pmcopy_' . $travelerID, $messages);
    	}
    	
    	return $messages;
    }
    
    private static function loadMessageToTraveler($travelerID, $record) {
		$message = gaPersonalMessageCopyFactory::create(gaPersonalMessage::TO_TRAVELER);
		self::loadMessage($travelerID, $message, $record);

		return $message;
    }
    
    private static function loadMessage($travelerID, $message, $record) {
    	$message->setID          ( $record['id']          );
		$message->setSenderID    ( $record['senderID']    );
		$message->setSubject     ( $record['subject']     );
		$message->setBody        ( $record['body']        );
		$message->setDateCreated ( $record['dateCreated'] );
		$message->setReadStatus  ( $record['isRead']      );
		
		$message->setIsMessageIncludedInNotif($record['isMessageIncludedInNotif']);
		
		$message->setSender      ( gaTravelerMapper::getTraveler($record['senderID']) );
		
		return $message;
    }
    
    /**
     * retrieves the messages sent by a traveler to another traveler or to a group
     * 
     * @param travelerID of the traveler
     * 
     * @return collection of personal messages
     */
    public static function retrieveSentMessages($travelerID) {
    	$cache = ganetCacheProvider::instance()->getCache();
    	
    	if (!is_null($cache) && ($messages = $cache->get('ganet_sentpm_' . $travelerID)))
    		;
    	else {
	    	$rs = new Recordset(new Connection());
	    	
	    	$query = "SELECT m.*, r.recipientID, g.groupID " .
	    			 "FROM tblPersonalMessage AS m " .
	    			 "INNER JOIN tblPersonalMessageRecipients AS r " .
	    			 "ON r.messageID = m.id " .
	    			 "INNER JOIN tblTraveler AS rt " .
	    			 "ON r.recipientID = rt.travelerID " .
					 "LEFT JOIN tblPersonalMessageToGroup AS g " .
	    			 "ON g.messageID = m.id " .
	    			 "WHERE m.senderID = $travelerID " .
	    			 "AND rt.isSuspended = 0 " .
	    			 //"AND rt.travelerID NOT IN (SELECT travelerID FROM tblBlockedUsers) " .
	    			 "AND m.isDeleted = 0 " .
	    			 "ORDER BY m.id DESC, m.dateCreated DESC";
	    	$rs->Execute($query);
	
	    	$messages = array();
	    	if ($rs->Recordcount() > 0) {
	    		$oldMessage = 0;
	    		$oldRecipientID = 0;
	    		$oldGroupID = 0;
	    		while($row = mysql_fetch_assoc($rs->Resultset())) {
	    			if ($row['id'] != $oldMessage) {
	    				// new message
	    				$oldMessage = $row['id'];
	    				$oldRecipientID = $row['recipientID'];
	    				
	    				if (is_null($row['groupID'])) {
	    					$message = gaPersonalMessageFactory::create(gaPersonalMessage::TO_TRAVELER);
	    				} else {
	    					$oldGroupID = $row['groupID'];
	    					
	    					$message = gaPersonalMessageFactory::create(gaPersonalMessage::TO_GROUP_STAFF);
	    					
	    					$message->setGroupIDs(array($row['groupID']));
		    				$message->setGroups(array(gaGroupMapper::getGroup($row['groupID'])));
	    				}
	    			
		    			$message->setID          ( $row['id']          );
		    			
		    			$message->setSenderID    ( $row['senderID']    );
		    			$message->setSender      ( gaTravelerMapper::getTraveler($row['senderID']));
		    			
		    			$message->setSubject     ( $row['subject']     );
		    			$message->setBody        ( $row['body']        );
		    			$message->setDateCreated ( $row['dateCreated'] );
		    			
		    			$message->setRecipientIDs(array($row['recipientID']));
		    			$message->setRecipients(array(gaTravelerMapper::getTraveler($row['recipientID'])));
		
						$message->setIsMessageIncludedInNotif($row['isMessageIncludedInNotif']);   			
		
		    			$messages[] = $message;
	    			} else {
	    				if ($oldRecipientID != $row['recipientID'] && !in_array($row['recipientID'],$message->getRecipientIDs())) {
	    					$oldRecipientID = $row['recipientID'];
	    					
	    					$message->setRecipientIDs(array_merge(array($row['recipientID']), $message->getRecipientIDs()));
	    					$message->setRecipients(array_merge(array(gaTravelerMapper::getTraveler($row['recipientID'])), $message->getRecipients()));
	    				} elseif ($oldGroupID != $row['groupID'] && !in_array($row['groupID'],$message->getGroupIDs())) {
							$oldGroupID = $row['groupID'];
							
							$message->setGroupIDs(array_merge(array($row['groupID']), $message->getGroupIDs()));
							$message->setGroups(array_merge(array(gaGroupMapper::getGroup($row['groupID'])), $message->getGroups()));
	    				}
	    			}
	    		}
	    	}
	    	
	    	if (!is_null($cache))
	    		$cache->set('ganet_sentpm_' . $travelerID, $messages);	
    	}
    	
    	return $messages;
    }
	
	/**
	 * does the actual sending of messages
	 * 
	 * @param message to be sent
	 */
	public static function send(gaPersonalMessage $message) {
		$rs = new Recordset(new Connection);

		$query = "INSERT INTO tblPersonalMessage (dateCreated, senderID, subject, body, isMessageIncludedInNotif) " .
				 "VALUES ('%s', %d, '%s', '%s', '%d')";
		$query = sprintf($query,
					date('Y-m-d H:i:s'),
					$message->getSenderID(),
					mysql_real_escape_string($message->getSubject()),
					mysql_real_escape_string($message->getBody()),
					$message->getIsMessageIncludedInNotif()
				);
		$rs->Execute($query);
		
		$message->setID(mysql_insert_id());
		
		// send recipients a copy
		foreach ($message->getRecipientSettings() as $recipientID => $setting) {
			$query = "INSERT INTO tblPersonalMessageRecipients (messageID, recipientID, isRead, isSpam) " .
				 	 "VALUES ({$message->getID()}, $recipientID, 0, {$setting['isSpam']})";
			$rs->Execute($query);
		}
		
		$cache = ganetCacheProvider::instance()->getCache();
		
		if (!is_null($cache)) {
			$cache->delete('ganet_sentpm_' . $message->getSenderID());
			
			foreach ($message->getRecipientIDs() as $recipientID) {
				$cache->delete('ganet_pmcopy_' . $recipientID);
			}
		} 
	}
	
	public static function sendToGroupStaff(gaToGroupStaffPersonalMessage $message) {
		self::send($message);
		self::registerMessageToGroup($message->getID(), $message->getGroupIDs());
	}
	
	/**
	 * saves the group ID of the group where the message was sent to determine what group the
	 * message was sent to since the message recipients are now the staff/super staff/admin and
	 * not the group
	 */
	public static function registerMessageToGroup($messageID, array $collGroupIDs) {
		$rs = new Recordset(new Connection());
		
		foreach ($collGroupIDs as $groupID) {
			$query = "INSERT INTO tblPersonalMessageToGroup (messageID, groupID) " .
				 "VALUES ($messageID, $groupID)";
			$rs->Execute($query);
		}
	}
	
	/**
	 * returns an array of groupIDs of groups to which the message was sent 
	 * returns an empty array if message is not for a group
	 */
	public static function getMessageGroupRecipients($messageID=0,$returnObjects=FALSE){
		$rs = new Recordset(new Connection());
		
		$query = "SELECT groupID
				  FROM tblPersonalMessageToGroup
				  WHERE messageID = {$messageID}
				  ORDER BY groupID";
		
		$rs->Execute($query);

		$groups = array();
		while( $row = mysql_fetch_assoc($rs->Resultset()) ){
			$groups[] = $returnObjects ? gaGroupMapper::getGroup($row["groupID"]) : $row["groupID"];
		}
		
		return $groups;
	}
	
	/**
	 * checks if message is to be included in the notification sent to the recipient's email
	 * particularly used when approving spam messages
	 */
	public static function isMessageIncludedInNotif($messageID=0){
		$rs = new Recordset(new Connection());
		
		$query = "SELECT isMessageIncludedInNotif
				  FROM tblPersonalMessage
				  WHERE id = {$messageID}
				  LIMIT 0,1";
		
		$rs->Execute($query);
		
		$isIncluded = TRUE;
		if( 0 < $rs->Recordcount() ){
			$row = mysql_fetch_assoc($rs->Resultset());
			$isIncluded = (1 ==$row["isMessageIncludedInNotif"]) ? TRUE : FALSE;
		}
		
		return $isIncluded;
	}
	
}