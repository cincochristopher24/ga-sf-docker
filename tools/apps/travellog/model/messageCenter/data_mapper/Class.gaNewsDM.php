<?php

require_once 'Class.Connection.php';
require_once 'Class.Recordset.php';
require_once('Cache/ganetCacheProvider.php');

/**
 * MEMCACHE KEY FORMAT: ganet_newscopy_{travelerID}
 */
class gaNewsDM {
	
	/**
	 * currently unused. Please beware that if a news item is deleted,
	 * the function should remove memcache values for the news recipients
	 */
	public static function delete($newsID) {
    	$rs = new Recordset(new Connection);
    	
    	$query = 	"DELETE `tblNews`, `tblNewsRecipients` FROM `tblNews`, `tblNewsRecipients` " .
    				"WHERE `tblNews`.`newsID` = `tblNewsRecipients`.`newsID` " .
    				"AND `tblNews`.`newsID` = $newsID";
    	 
    	$rs->Execute($query);
    }
	
	protected static function doSave($news, $authorID) {
		$rs = new Recordset(new Connection);
		
		$query = "INSERT INTO tblNews (groupID, title, content, " .
				 (is_null($news->getExpiration()) ? "" : "expiration, ") .
				 "dateCreated) " .
				 "VALUES (" .
				 $news->getGroupID() . ", " .
				 "'" . mysql_real_escape_string($news->getTitle()) . "', " .
				 "'" . mysql_real_escape_string($news->getContent()) . "', " .
				 (is_null($news->getExpiration()) ? "" : ("'" . mysql_real_escape_string($news->getExpiration()) . "', ")) .
				 "'" . date('Y-m-d H:i:s') . "')";
		$rs->Execute($query);
		
		$news->setID(mysql_insert_id());
		
		// insert into tblNewsRecipients
		$query = "SELECT travelerID " .
				 "FROM tblGrouptoTraveler " .
				 "WHERE groupID = " . $news->getGroupID();
		$rs->Execute($query);
		
		$recipientIDs = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_row($rs->Resultset())) {
				$recipientIDs[] = $row[0];
			}
		}
		
		// add author to list of recipients
		$recipientIDs[] = $authorID;
		$recipientIDs = array_unique($recipientIDs);
		
		foreach ($recipientIDs as $recipientID) {
			$query = "INSERT INTO tblNewsRecipients (newsID, travelerID, isRead) " .
				 	 "VALUES ({$news->getID()}, $recipientID, 0)";
			$rs->Execute($query);
		}
		
		// clear cache
		$cache = ganetCacheProvider::instance()->getCache();
		
		if (!is_null($cache)) {
			foreach ($recipientIDs as $recipientID) {
				$cache->delete('ganet_newscopy_' . $recipientID);
			}
			self::clearGroupCache($cache,$news->getGroupID());
		}
	}
	
	/**
	 * updates an existing news
	 * Note: for future use only when a news item could be edited
	 */
	protected static function doUpdate($news, $authorID) {
		
	}
	
	public static function save($news, $authorID) {
		if (is_null($news->getID())) {
			self::doSave($news, $authorID);
		} else {
			self::doUpdate($news, $authorID);
		}
	}
	
	public static function retrieveByPk($newsID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT * " .
				 "FROM tblNews " .
				 "WHERE newsID = $newsID " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() <= 0)
			return null;
		
		$row = mysql_fetch_assoc($rs->Resultset());
		
		$news = new gaNews;
		$news = self::doLoad($news, $row);
		
		return $news;
	}
	
	public static function batchDeleteCopy($travelerID, $collNewsCopyIDs) {
		$rs = new Recordset(new Connection);
		
		$query = "DELETE FROM tblNewsRecipients " .
				 "WHERE newsID IN (" . join(',', $collNewsCopyIDs) . ") " .
				 "AND travelerID = $travelerID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache)){
			$cache->delete('ganet_newscopy_' . $travelerID);
			self::clearGroupCache($cache);
		}
	}
	
	public static function batchMarkAsUnread($travelerID, $newsIDs) {
		self::batchFlagMessage($travelerID, $newsIDs, 0);
	}
	
	public static function batchFlagMessage($travelerID, $newsIDs, $flag) {
		$rs = new Recordset(new Connection);
		
		$query = "UPDATE tblNewsRecipients SET " .
				 "isRead = $flag " .
				 "WHERE newsID IN (" . join(',', $newsIDs) . ") " .
				 "AND travelerID = $travelerID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache)){
			$cache->delete('ganet_newscopy_' . $travelerID);
			self::clearGroupCache($cache);
		}
	}
	
	public static function batchMarkAsRead($travelerID, $newsIDs) {
		self::batchFlagMessage($travelerID, $newsIDs, 1);
	}
	
	public static function markCopyAsRead($newsID, $travelerID) {
		$rs = new Recordset(new Connection);
		
		$query = "UPDATE tblNewsRecipients SET " .
				 "isRead = 1 " .
				 "WHERE newsID = $newsID " .
				 "AND travelerID = $travelerID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache)){
			$cache->delete('ganet_newscopy_' . $travelerID);
			self::clearGroupCache($cache);
		}
	}
	
	public static function deleteCopy($travelerID, $newsID) {
		$rs = new Recordset(new Connection);
		
		$query = "DELETE FROM tblNewsRecipients " .
				 "WHERE newsID = $newsID " .
				 "AND travelerID = $travelerID";
		$rs->Execute($query);
		
		$cache = ganetCacheProvider::instance()->getCache();
		if (!is_null($cache)){
			$cache->delete('ganet_newscopy_' . $travelerID);
			self::clearGroupCache($cache);
		}
	}
	
	public static function retrieveNews($travelerID) {
		$cache = ganetCacheProvider::instance()->getCache();
		
		if (!is_null($cache) && ($collNews = $cache->get('ganet_newscopy_' . $travelerID)))
			;
		else {
			$rs = new Recordset(new Connection);
			
			$query = "SELECT n.*, r.isRead " .
					 "FROM tblNews n " .
					 "INNER JOIN tblNewsRecipients r " .
					 "ON n.newsID = r.newsID " .
					 "INNER JOIN tblGroup g " .
					 "ON n.groupID = g.groupID " .
	//				 "INNER JOIN tblTraveler t " .
	//				 "ON g.administrator = t.travelerID " .
					 "WHERE r.travelerID = $travelerID " .
	//				 "AND t.travelerID <> $travelerID " .
					 "AND g.isSuspended = 0 ".
					 "GROUP BY n.newsID ".
					 "ORDER BY n.dateCreated";
			$rs->Execute($query);
	
			$collNews = array();
			if ($rs->Recordcount() > 0) {
				$collNews = self::loadCopyFromRS($rs);
			}
	
			if (!is_null($cache))
				$cache->set('ganet_newscopy_' . $travelerID, $collNews);	
		}
		
		return $collNews;
	}
	
	public static function retrieveNewsByGroup($travelerID,$groupID) {
		$cache = ganetCacheProvider::instance()->getCache();
		
		if (!is_null($cache) && ($collNews = $cache->get('ganet_newscopy_group_' . $groupID)))
			;
		else {
			$rs = new Recordset(new Connection);
			
			$query = "SELECT n.*, r.isRead " .
					 "FROM tblNews n " .
					 "INNER JOIN tblNewsRecipients r " .
					 "ON n.newsID = r.newsID " .
					 "INNER JOIN tblGroup g " .
					 "ON n.groupID = $groupID " .
	//				 "INNER JOIN tblTraveler t " .
	//				 "ON g.administrator = t.travelerID " .
					 "WHERE r.travelerID = $travelerID " .
	//				 "AND t.travelerID <> $travelerID " .
					 "AND g.isSuspended = 0 ".
					 "GROUP BY n.newsID ".
					 "ORDER BY n.dateCreated ";
			$rs->Execute($query);
	
			$collNews = array();
			if ($rs->Recordcount() > 0) {
				$collNews = self::loadCopyFromRS($rs);
			}
	
			if (!is_null($cache))
				$cache->set('ganet_newscopy_group_' . $groupID, $collNews);	
		}
		
		return $collNews;
	}
	
	private static function loadCopyFromRS(Recordset $rs) {
		$collNewsCopies = array();
		while ($row = mysql_fetch_assoc($rs->Resultset())) {
			$news = new gaNewsCopy;
			
			$newsCopy = self::doLoad($news, $row);
			$newsCopy->setReadStatus($row['isRead']);

			$collNewsCopies[] = $newsCopy;
		}
		
		return $collNewsCopies;
	}
	
	private static function doLoad(gaNews $news, $record) {
		$news->setID          ( $record['newsID']      );
		$news->setTitle       ( $record['title']       );
		$news->setContent     ( $record['content']     );
		$news->setGroupID     ( $record['groupID']     );
		$news->setExpiration  ( $record['expiration']  );
		$news->setDateCreated ( $record['dateCreated'] );
			
		$news->setSender(gaGroupMapper::getGroup($record['groupID']));
		
		return $news;
	}
	
	private static function clearGroupCache($cache,$groupID=NULL){
		if( !is_null($groupID) ){
			$cache->delete('ganet_newscopy_group_' . $groupID);
		}elseif( isset($_GET["gID"]) ){
			$cache->delete('ganet_newscopy_group_' . $_GET["gID"]);
		}elseif( isset($_POST["gID"]) ){
			$cache->delete('ganet_newscopy_group_' . $_POST["gID"]);
		}
	}
}