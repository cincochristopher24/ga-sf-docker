<?php

require_once('Class.Template.php');
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/model/Class.Condition.php');
require_once('travellog/model/Class.FilterCriteria2.php');
 
class OldGroupMembersPanel{
	
	public static $ALLMEMBERS 			= 1;
 	public static $MEMBERS 				= 2;
 	public static $PENDINGINVITES   	= 3;
 	public static $PENDINGEMAILINVITES	= 4;
 	public static $PENDINGREQUESTS  	= 5;
	public static $MEMUPDATES       	= 6;
	public static $DENIEDINVITES    	= 7;
	public static $SEARCHMEMBERS    	= 8;
	public static $INVITEMEMBERS     	= 9;
	public static $SEARCHMEMBERSFORM   	= 10;
	public static $INVITEMEMBERSFORM   	= 11;
	public static $STAFF 				= 12;
	public static $MODERATORS			= 13;
	public static $POSTACTIONMSG		= 14;
	
	private $page 			= 1;
	private $group = null;
	private $selectSubGroup = null;
	private $isMemberLogged = false;
	private $isAdminLogged = false;
	
	private $searchType = '';
	private $searchKey   = '';
	private $searchKey2   = '';
	private $searchKeyUName   = '';
	private $panelView   = '';
	private $cmbSelectSubGroupID   = '';
		
	private $txaEmail = '';
	private $arrEmails = array();
	
	private $postValues = array();
	
	private $searchResultsPanel = null;
	
	private $arrNewlyAdded = array();
	private $arrInvited = array();
	
	private $innerMessagePanel = null;
	
	private $arrInvitedReport = array();
	
	public function setPage($_page){
		$this->page = $_page;
	}
	
	public function setGroup($_group){
		$this->group = $_group;
	}
	
	public function setSelectSubGroup($_selectSubGroup){
		$this->selectSubGroup = $_selectSubGroup;
	}
	
	public function setIsMemberLogged($_isMemberLogged){
		$this->isMemberLogged = $_isMemberLogged;
	}
	
	public function setIsAdminLogged($_isAdminLogged){
		$this->isAdminLogged = $_isAdminLogged;
	}

	public function setEmailMultiple($_emailmultiple){
		$this->txaEmail = $_emailmultiple;
	}
	
	public function setPostValues($_postValues){
		$this->postValues = $_postValues;
	
		$this->searchType = (isset($_postValues['searchType'])) ? $_postValues['searchType'] : '';
		$this->searchKey2 = (isset($_postValues['txtLastName'])) ? $_postValues['txtLastName'] : '';
		if ( 'txtName' == $this->searchType)
			$this->searchKey =  (isset($_postValues['txtFirstName'])) ? $_postValues['txtFirstName'] : '';
		else
			$this->searchKey =  (isset($_postValues['txaEmail'])) ? $_postValues['txaEmail'] : '';
		
		$this->searchKeyUName = (isset($_postValues['txtUserName'])) ? $_postValues['txtUserName'] : '';
		$this->panelView =  (isset($_postValues['panelView'])) ? $_postValues['panelView'] : 'all';
		$this->cmbSelectSubGroupID =  (isset($_postValues['cmbSelectSubGroupID'])) ? $_postValues['cmbSelectSubGroupID'] : 0;
	}
	
	public function setArrNewlyAdded($_arr){
		$this->arrNewlyAdded = $_arr;
	}
	
	public function setArrInvited($_arr){
		$this->arrInvited= $_arr;
	}
	
	public function setInnerMessagePanel($_msgPanel){
		$this->innerMessagePanel = $_msgPanel;
	}
	
	public function setArrInviteReport($arrInvitedReport){
		$this->arrInvitedReport = $arrInvitedReport;
	}
	
	public function fetch($memberTabType = 1) {
 		ob_start();
 		$this->render($memberTabType);
 		$code = ob_get_contents();
 		ob_end_clean();
 		return $code;
 	}
 	
	
	/**
 	 * function to display the Members
 	 * @param integer $memberTabType type of message to render 
 	 */
 	public function render($memberTabType = 1){ 		
 	
 		switch ($memberTabType){
 			case self::$ALLMEMBERS :
 				$this->renderAllMembers();
 				break;
 			case self::$MEMBERS:
 				$this->renderMembers();
 				break;
 			case self::$PENDINGINVITES:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderPendingInvites();
 				break;
 			case self::$PENDINGEMAILINVITES:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderPendingEmailInvites();
 				break;
 			case self::$PENDINGREQUESTS:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderPendingRequests();
 				break;
 			case self::$MEMUPDATES:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderMembershipUpdates();
 				break;
 			case self::$DENIEDINVITES:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderDeniedInvites();
 				break;
 			case self::$SEARCHMEMBERS:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderSearchMembers();
 				break;
 			case self::$INVITEMEMBERS:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderInviteMembers();
 				break;
 			case self::$SEARCHMEMBERSFORM:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderSearchForm();
 				break;
 			case self::$INVITEMEMBERSFORM:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderInviteForm();
 				break;
			case self::$STAFF:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderStaff();
 				break;
			case self::$MODERATORS:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderModerators();
 				break;
			case self::$POSTACTIONMSG:
 				if (TRUE == $this->isAdminLogged)
 					$this->renderActionMessagePanel();
 				break;
 			default: 
 				$this->renderAllMembers();
 				break;				 			
 		}	 
 	}
	
	/**
 	 * display all members, including pending requests and pending invites & email invites
 	 */
 	private function renderAllMembers(){
 		
 		$allMembers = array();
 		
 		$requestList 	= array();
 		$inviteList 	= array();
 		$grpMembersList = array();
 		/* 
 		if  (TRUE == $this->isAdminLogged ) {
 			// get list of travelers invited to join this group
			$cond = new Condition;
			$cond->setAttributeName("status");
			$cond->setOperation(FilterOp::$EQUAL);
			$cond->setValue(0);
	
			$filter = new FilterCriteria2();
			$filter->addCondition($cond);
		
			$inviteList = $this->group->getInviteList($filter);	
			if (count($inviteList))
				$allMembers = array_merge($allMembers,$inviteList);
			
			// get list of travelers requesting to join this group
			$requestList = $this->group->getRequestList();
			if (count($requestList)) 
				 $allMembers = array_merge($allMembers,$requestList);
			
		}
		
		$grpMembersList = $this->group->getMembers();
		if (count($grpMembersList))
			$allMembers = array_merge($allMembers,$grpMembersList);								
 		
 		if (count($allMembers)){
 			usort($allMembers,array('Group','orderByName'));	// sort the members by name	 		
 		}
 	
 		$rowsperpage   = 100;*/
 		$rowsperpage = 25;
 		$nRowsLimit= new RowsLimit($rowsperpage, 1 < $this->page ? $this->page : 1);
 		$members = Group::getMembersInvitesAndRequests($this->group->getGroupID(), $this->isAdminLogged, $nRowsLimit);
 		$allMembers = $members['members'];
		$paging        = new Paging( $members['count'], $this->page, 'grpID=' . $this->group->getGroupID(), $rowsperpage );
		$paging->setOnclick('manager._allMembers');
		$iterator      = new ObjectIterator( $allMembers, $paging );
		
		$template = new Template();
 		$template->set('isAdminLogged',$this->isAdminLogged);
 		$template->set('allMembers',$allMembers);
		$template->set('subGroupList',$this->prepareSubGroups($allMembers));		
	//	if  ($this->isAdminLogged )
			$template->set('assignedSubGroupList',$this->prepareAssignedSubGroups($allMembers));	
 		$template->set('group',$this->group);
 		$template->set('memberTabType',self::$ALLMEMBERS);	
 		$template->set( 'paging'        , $paging         );
		$template->set( 'iterator'      , $iterator       );
		$template->set( 'totalrec'      , $members['count'] );
		$template->set('countSubGroups',count($this->group->getSubGroups()));
		
 		$template->out('travellog/views/tpl.ViewMembersPanel.php');
 				
 	}
 	
 	
 	/**
 	 * display all (official) members of the group
 	 */
 	private function renderMembers(){
 		
 		$grpMembersList = $this->group->getMembers();
	
 		if (count($grpMembersList)){
 			usort($grpMembersList,array('Group','orderByLastName'));	// sort the members by name	 		
 		}
 	
 		$rowsperpage   = 100;
		$paging        = new Paging( count($grpMembersList), $this->page, 'grpID=' . $this->group->getGroupID(), $rowsperpage );
		$paging->setOnclick('manager._members');
		$iterator      = new ObjectIterator( $grpMembersList, $paging );
		
		$template = new Template();
		$template->set('isAdminLogged',$this->isAdminLogged);
 		$template->set('allMembers',$grpMembersList);
		$template->set('subGroupList',$this->prepareSubGroups($grpMembersList));
 		$template->set('group',$this->group);
 		$template->set('memberTabType',self::$MEMBERS);	
 		$template->set( 'paging'        , $paging         );
		$template->set( 'iterator'      , $iterator       );
		$template->set( 'totalrec'      , count($grpMembersList) );
		$template->set('countSubGroups',count($this->group->getSubGroups()));
		
 		$template->out('travellog/views/tpl.ViewMembersPanel.php');
 				
 	}
 	
 	/**
 	 * display the GA NET travelers invited by the admin to join group
 	 */
 	private function renderPendingInvites(){
 		
 		// get list of travelers invited to join this group
		$cond = new Condition;
		$cond->setAttributeName("status");
		$cond->setOperation(FilterOp::$EQUAL);
		$cond->setValue(0);

		$filter = new FilterCriteria2();
		$filter->addCondition($cond);
	
		$inviteList = $this->group->getInviteList($filter);	
	
 		if (count($inviteList)){
 			usort($inviteList,array('Group','orderByName'));	// sort the members by name	 		
 		}
 		

		// get email invite list 
		$emailInviteList = $this->group->getEmailInviteList();	
		
 		$rowsperpage   = 100;
		$paging        = new Paging( count($inviteList), $this->page, 'grpID=' . $this->group->getGroupID(), $rowsperpage );
		$paging->setOnclick('manager._pendingInvites');
		$iterator      = new ObjectIterator( $inviteList, $paging );
		
		$template = new Template();
		$template->set('isAdminLogged',$this->isAdminLogged);
 		$template->set('allMembers',$inviteList);
		$template->set('assignedSubGroupList',$this->prepareAssignedSubGroups($inviteList));	
 		$template->set('group',$this->group);
 		$template->set('memberTabType',self::$PENDINGINVITES);	
 		$template->set( 'paging'        , $paging         );
		$template->set( 'iterator'      , $iterator       );
		$template->set( 'totalrec'      , count($inviteList) );
		$template->set('countSubGroups',count($this->group->getSubGroups()));
		
		$tplEmailInviteList = new Template();
 		$tplEmailInviteList->set('allMembers',$emailInviteList);
 		$tplEmailInviteList->set('group',$this->group);
 		
		$template->set('hasEmailInviteList', count($emailInviteList));
		$template->set( 'emailInviteList' , $tplEmailInviteList->fetch('travellog/views/tpl.ViewEmailInviteMembersPanel.php'));
		
 		$template->out('travellog/views/tpl.ViewMembersPanel.php');
 				
 	}
 	
 	
 	/**
 	 * display email address invited by the admin to join group
 	 */
 	
	private function renderPendingEmailInvites(){
 		
 		$emailInviteList = $this->group->getEmailInviteList();	
		
 		$template = new Template();
 		$template->set('isAdminLogged',$this->isAdminLogged);
 		$template->set('allMembers',$emailInviteList);
 		$template->set('group',$this->group);
 		$template->set('memberTabType',self::$PENDINGEMAILINVITES);	
 		$template->set( 'totalrec'      , count($emailInviteList) );
		
 		$template->out('travellog/views/tpl.ViewEmailInviteMembersPanel.php');
 				
 	}
 	

 	/**
 	 * display GA NET Travelers requesting to join group
 	 */
 	private function renderPendingRequests(){
 		
 		$requestList = $this->group->getRequestList();
		
 		if (count($requestList)){
 			usort($requestList,array('Group','orderByName'));	// sort the members by name	 		
 		}
 		
 		$rowsperpage   = 100;
		$paging        = new Paging( count($requestList), $this->page, 'grpID=' . $this->group->getGroupID(), $rowsperpage );
		$paging->setOnclick('manager._pendingRequests');
		$iterator      = new ObjectIterator( $requestList, $paging );
		
		$template = new Template();
		$template->set('isAdminLogged',$this->isAdminLogged);
 		$template->set('allMembers',$requestList);
 		$template->set('group',$this->group);
 		$template->set('memberTabType',self::$PENDINGREQUESTS);	
 		$template->set( 'paging'        , $paging         );
		$template->set( 'iterator'      , $iterator       );
		$template->set( 'totalrec'      , count($requestList) );
		
 		$template->out('travellog/views/tpl.ViewMembersPanel.php');
 				
 	}
 	
 	
 	/**
 	 * display Membership Updates for this Group
 	 */
 	private function renderMembershipUpdates(){
 	
		require_once('travellog/model/updates/Class.GroupMembershipUpdates.php');
		require_once('travellog/model/Class.GroupFactory.php');
		$memUpdates = new GroupMembershipUpdates($this->group);
		echo $memUpdates->render(array('groupMembershipUpdates' => $memUpdates->getUpdates()));
	
 	}
 	
 	
 	/**
 	 * display the GA NET travelers who declined invitation of admin to join group
 	 */
 	private function renderDeniedInvites(){
 		
 		// get list of travelers who declined invitation sent by group
		$cond = new Condition;
		$cond->setAttributeName("status");
		$cond->setOperation(FilterOp::$EQUAL);
		$cond->setValue(1);

		$filter = new FilterCriteria2();
		$filter->addCondition($cond);
	
		$deniedInviteList = $this->group->getInviteList($filter);	
		
 		if (count($deniedInviteList)){
 			usort($deniedInviteList,array('Group','orderByName'));	// sort the members by name	 		
 		}
 		
 		$rowsperpage   = 6;
		$paging        = new Paging( count($deniedInviteList), $this->page, 'grpID=' . $this->group->getGroupID(), $rowsperpage );
		$paging->setOnclick('manager._deniedInvites');
		$iterator      = new ObjectIterator( $deniedInviteList, $paging );
		
		$template = new Template();
		$template->set('isAdminLogged',$this->isAdminLogged);
 		$template->set('allMembers',$deniedInviteList);
 		$template->set('group',$this->group);
 		$template->set('memberTabType',self::$DENIEDINVITES);	
 		$template->set( 'paging'        , $paging         );
		$template->set( 'iterator'      , $iterator       );
		$template->set( 'totalrec'      , count($deniedInviteList) );
		
 		$template->out('travellog/views/tpl.ViewMembersPanel.php');
 				
 	}
 	
 	/**
 	 * display results when searching for GA NET travelers / email address of prospective GA NET Members
 	 */
  	private function renderSearchMembers(){
 		
 		$traveler = NULL;
		$arrResultID = array();
		$arrResultIDByName = array();
		$arrResultIDByUName = array();
		$arrResultTrav = array();
		$arrResultTravByUName = array();
		$arrResultEmail = array();
		$searchKey = '';
		$searchKey2 = '';
		$searchKeyUName = '';
		$searchForm = false;
	
 		if ( strlen($this->searchKey) || strlen($this->searchKey2) || strlen($this->searchKeyUName))	{
			if ( $this->searchType == 'txtName'){	
				
				$arrResultIDByName = self::getTravelerIDByLikeName($this->searchKey, $this->searchKey2);	
				$arrResultIDByUName = self::getTravelerIDByLikeUserName($this->searchKeyUName);
				$arrResultID = array_unique(array_merge($arrResultIDByName,$arrResultIDByUName));
				
				foreach($arrResultID  as $eachID){
					$objTrav = new Traveler($eachID);
					if ($objTrav->isActive())
						$arrResultTrav[] = $objTrav;		
				}
			
			}
			elseif ( $this->searchType == 'txtEmail') {
			
				$this->arrEmails = array_unique(explode(",",$this->searchKey));
			
		 		if (count($this->arrEmails)) {
					foreach($this->arrEmails as $each){
						$each = trim($each);
						if ($each) {
							if (count(TRAVELER::getTravelerIDByEmail($each))){
								$arrTravID = TRAVELER::getTravelerIDByEmail($each);
								$arrResultID = array_merge($arrResultID, $arrTravID);
								$objTrav =  new Traveler($arrTravID[0]);
								if ($objTrav->isActive())
									$arrResultTrav[] = $objTrav;
								
							} else{
								$arrResultEmail[] = $each;										
							}
						}
					}	
				}
			
			}
					
		}
		else
			$searchForm = true;
		
		$arrVars = array();
		$arrVars['arrResultID']    = $arrResultID;
		$arrVars['arrResultEmail'] = $arrResultEmail;
		$arrVars['searchType']     = $this->searchType;
		$arrVars['searchKey']      = $this->searchKey;
		$arrVars['searchKey2']     = $this->searchKey2;
		$arrVars['searchKeyUName']     = $this->searchKeyUName;
		$arrVars['cmbSelectSubGroupID']     = (null!= $this->selectSubGroup) ? $this->selectSubGroup->getGroupID() : 0;
	
	
		$this->searchResultsPanel = new Template();
		
		$this->searchResultsPanel->set('arrVars', $arrVars);
		$this->searchResultsPanel->set('subGroupList',$this->prepareSubGroups($arrResultTrav));	
 		$this->searchResultsPanel->set('group',$this->group);
		$this->searchResultsPanel->set('subGroup',$this->selectSubGroup);
 		$this->searchResultsPanel->set('showConfirm1', false);
		$this->searchResultsPanel->set('showConfirm2', false);
	
		$this->searchResultsPanel->set( 'arrNewlyAdded'      , $this->arrNewlyAdded );
		$this->searchResultsPanel->set( 'arrInvited'      , $this->arrInvited );
		
		
		$this->searchResultsPanel->setTemplate('travellog/views/tpl.ViewSearchResultsPanel.php');
		
		if ('all' == $this->panelView):
			
			if (0 < count($arrResultID))
				$activeTab = 'members';
			else
				$activeTab = 'nonMembers';
			$template = new Template();				
	 		$template->set('searchResultsPanel' , $this->searchResultsPanel );		
			$template->set('grpID', $this->group->getGroupID());
			$template->set('arrVars', $arrVars);
								
			$template->out('travellog/views/tpl.ViewSearchMembersGrid.php');
			
		else:
		
			$this->searchResultsPanel->set('activeTab', $this->panelView );
			
			$this->searchResultsPanel->out('travellog/views/tpl.ViewSearchResultsPanel.php');
		endif;
		
 				
 	}
 	
 	/**
 	 * display search form when searching inviting people to join group (or join GA Net first), by sending out an email invitation
 	 */

 	/**
 	 * display search form 
 	 */
 	private function renderSearchForm(){
 		
		$condorder = new Condition;
		$condorder->setAttributeName("name");
	  		
		$order = new FilterCriteria2();
		$order->addCondition($condorder);
		
		$template = new Template();
		$template->set('group',$this->group);
		if ($this->group->isSubGroup()){
			$nonSGMembersofParent = array();
			$grpMembers = $this->group->getParent()->getMembers();
			foreach($grpMembers as $eachMember):
				if (!$this->group->isMember($eachMember)) {
					$nonSGMembersofParent[] =  $eachMember;
				} 
			endforeach;
			usort($nonSGMembersofParent, array("Group","orderByName")); 
			$template->set('nonSGMembersofParent',$nonSGMembersofParent);
		}
		$template->set('arrSubGroups',$this->group->getSubGroups(null, $order));
		$template->set('txaEmail','');
 		$template->set('memberTabType',self::$SEARCHMEMBERSFORM);
		$template->set('subGroup',$this->selectSubGroup);
		$arrInvites = array('emails' => array(), 'users' => array());
		
		/**
		 * checks if arrInvitedReport is not an empty array
		 * solves undefined variables in the template 
		 * if statement added by neri
		 * 10-31-08
		 */
		
		if (0 < count($this->arrInvitedReport)) {
			if( count($this->arrInvitedReport['emails']) ){
				foreach( $this->arrInvitedReport['emails'] as $email){
					if( !in_array($email['emailName'], $arrInvites['emails']) )
						$arrInvites['emails'][] = $email['emailName'];
				}
			}	
			if( count($this->arrInvitedReport['users']) ){
				foreach( $this->arrInvitedReport['users'] as $user){
					if( !in_array($user['username'], $arrInvites['users']) )
						$arrInvites['users'][] = $user['username'];
				}	
			}
		}
				
		$template->set('sentInvites',$arrInvites);
		
		$template->set('obj_message_panel',$this->innerMessagePanel);
		$template->set('arrNewlyAdded',$this->arrNewlyAdded);
 		$template->out('travellog/views/tpl.ViewSearchMembersForm.php');
 				
 	}
 

	/**
 	 * display all staff of the group
 	 */
 	private function renderStaff(){
 		
 		$grpStaffList = $this->group->getStaff();
		
 		if (count($grpStaffList)){
 			usort($grpStaffList,array('Group','orderByName'));	// sort the members by name	 		
 		}
 		
 		$rowsperpage   = 100;
		$paging        = new Paging( count($grpStaffList), $this->page, 'grpID=' . $this->group->getGroupID(), $rowsperpage );
		$paging->setOnclick('manager._members');
		$iterator      = new ObjectIterator( $grpStaffList, $paging );
		
		$template = new Template();
		$template->set('isAdminLogged',$this->isAdminLogged);
 		$template->set('allMembers',$grpStaffList);
		$template->set('subGroupList',$this->prepareSubGroups($grpStaffList));
 		$template->set('group',$this->group);
 		$template->set('memberTabType',self::$STAFF);	
 		$template->set( 'paging'        , $paging         );
		$template->set( 'iterator'      , $iterator       );
		$template->set( 'totalrec'      , count($grpStaffList) );
		
 		$template->out('travellog/views/tpl.ViewMembersPanel.php');
 				
 	}


	/*
	*	get traveler's subgroups
	*/
	
	private function prepareSubGroups($membersList = array()) {
		$mySubGroupList = array();
		foreach($membersList as $eachMember){			
			$arrSubGroup = $this->group->getSubGroupsByTravelerID($eachMember->getTravelerID(), true);
			$mySubGroupList[$eachMember->getTravelerID()] = $arrSubGroup;
		}
		return $mySubGroupList;
	}
	
	/*
	*	get traveler's assigned subgroups
	*/
	
	private function prepareAssignedSubGroups($inviteList = array()) {
		$myAssignedSubGroupList = array();
		foreach($inviteList as $eachInvite){			
			$arrAssignedSubGroup = array();			
			$arrAssignedSubGroup = $this->group->getSubGroupInvitationsByTravelerID ($eachInvite->getTravelerID(), true);
			$myAssignedSubGroupList[$eachInvite->getTravelerID()] = $arrAssignedSubGroup;
		}
		return $myAssignedSubGroupList;
	}
	
	
 	public static function getTravelerIDByLikeName($_firstName = '', $_lastName = ''){
			
		try{ 				
			$conn = new Connection();
			$rs   = new Recordset($conn);
		}
		
		catch (exception $e){				   
		   throw $e;
		}
		
		$arrTrav = array();
		$_lastName = trim(strtolower($_lastName));
		$_firstName = trim(strtolower($_firstName));
		
		if (0 < strlen($_firstName) && 0 < strlen($_lastName)){
			$sqlQuery = "select `travelerID` from tblTraveler where `firstname` LIKE '%$_firstName%' and `lastname` LIKE '%$_lastName%' " .
					" and travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
			$rs-> Execute($sqlQuery);
		}
		
		else if (0 < strlen($_firstName) && 0 == strlen($_lastName)){
			$sqlQuery = "select `travelerID` from tblTraveler where `firstname` LIKE '%$_firstName%' and travelerID not in " .
					"(select distinct travelerID from tblGrouptoAdvisor)";
			$rs-> Execute($sqlQuery);
		}
		
		else if (0 == strlen($_firstName) && 0 < strlen($_lastName)){
			$sqlQuery = "select `travelerID` from tblTraveler where `lastname` LIKE '%$_lastName%' and travelerID not in " .
					"(select distinct travelerID from tblGrouptoAdvisor)";
			$rs-> Execute($sqlQuery);
		}
		
		if (0 != strlen($_firstName) || 0 != strlen($_lastName)){
			
			while($traveler = mysql_fetch_array($rs->Resultset())){
				$arrTrav[] = $traveler['travelerID'];
			}
		}
		
		return $arrTrav;
	}
 
	public static function getTravelerIDByLikeUserName($_username = ''){
	  	
	  	try{ 				
			$conn = new Connection();
			$rs   = new Recordset($conn);
		}
		
		catch (exception $e){				   
		   throw $e;
		}
		
		$arrTrav = array();
		$_username = trim(strtolower($_username));
		
		if (strlen($_username )) {
			
			$sqlQuery = "select `travelerID` from tblTraveler where `username` LIKE '%$_username%' and travelerID not in (select distinct travelerID from tblGrouptoAdvisor)";
		  	$rs-> Execute($sqlQuery);
	  	
		  	if (0 < $rs->Recordcount())	{	  		
		  		while($traveler = mysql_fetch_array($rs->Resultset())){
					$arrTrav[] = $traveler['travelerID'];
				}
			}
		}
		
		return $arrTrav;	
		
	}

}

?>