<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	ATHENARetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for ATHENA	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ATHENA/views'));
		$file_factory->setPath('CSS', '/custom/ATHENA/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ATHENA/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}