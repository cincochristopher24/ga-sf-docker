<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class ATHENASubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new ATHENA subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ATHENA/views"));
			$this->file_factory->setPath("CSS", "/custom/ATHENA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ATHENA/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}