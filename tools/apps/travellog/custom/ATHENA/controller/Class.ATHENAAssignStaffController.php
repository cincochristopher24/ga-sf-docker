<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');
	
	class ATHENAAssignStaffController extends COBRANDAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ATHENA/views'));
			$this->mFactory->setPath('CSS', '/custom/ATHENA/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/ATHENA/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}