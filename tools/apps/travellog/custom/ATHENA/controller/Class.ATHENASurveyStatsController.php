<?php 
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyStatsController.php');
	
 	class ATHENASurveyStatsController extends COBRANDSurveyStatsController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ATHENA/views'));
			$this->file_factory->setPath('CSS', '/custom/ATHENA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ATHENA/views/');
 		}
 	}