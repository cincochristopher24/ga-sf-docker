<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDBulletinManagementController.php');
class MBCBulletinManagementController extends COBRANDBulletinManagementController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/MBC/views'));
		$this->file_factory->setPath('CSS', '/custom/MBC/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/MBC/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>