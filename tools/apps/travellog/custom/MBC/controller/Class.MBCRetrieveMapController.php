<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	MBCRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for MBC	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/MBC/views'));
		$file_factory->setPath('CSS', '/custom/MBC/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/MBC/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}