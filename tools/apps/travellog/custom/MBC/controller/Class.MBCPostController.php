<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostController.php');
	
 	class MBCPostController extends COBRANDPostController {
 		function __construct(){
	 		parent::__construct();
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/MBC/views'));
			$file_factory->setPath('CSS', '/custom/MBC/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/MBC/views/');
 		}
 		
 	}