<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDJournalDraftController.php");
	
	/**
	 * The activation support controller of a cobrand.
	 */
	class MBCJournalDraftController extends COBRANDJournalDraftController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/MBC/views"));
			$this->file_factory->setPath("CSS", "/custom/MBC/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/MBC/views/");
		}
		
	}
