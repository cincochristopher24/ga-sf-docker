<?php
/*
 * Created on 12 23, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

	require_once('travellog/custom/COBRAND/controller/Class.COBRANDChangePasswordController.php');
	
 	class SAMFORDChangePasswordController extends COBRANDChangePasswordController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SAMFORD/views'));
			$this->file_factory->setPath('CSS', '/custom/SAMFORD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SAMFORD/views/');
 		}
 		
 		function performAction(){ 
			parent::performAction();
		}
 	}