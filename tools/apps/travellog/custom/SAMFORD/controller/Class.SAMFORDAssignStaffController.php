<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class SAMFORDAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SAMFORD/views'));
			$this->mFactory->setPath('CSS', '/custom/SAMFORD/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/SAMFORD/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>