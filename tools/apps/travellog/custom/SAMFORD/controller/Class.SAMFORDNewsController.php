<?php
	/*
	 * Class.COBRANDMessagesController.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class SAMFORDNewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SAMFORD/views'));
			$this->file_factory->setPath('CSS', '/custom/SAMFORD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SAMFORD/views/');
		}
	 	
	 }
?>