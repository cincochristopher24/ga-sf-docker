<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');
	
	class UFIBSAssignStaffController extends COBRANDAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/UFIBS/views'));
			$this->mFactory->setPath('CSS', '/custom/UFIBS/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/UFIBS/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}