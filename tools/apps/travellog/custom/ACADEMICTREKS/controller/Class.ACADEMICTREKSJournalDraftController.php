<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDJournalDraftController.php");
	
	/**
	 * The activation support controller of a cobrand.
	 */
	class ACADEMICTREKSJournalDraftController extends COBRANDJournalDraftController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ACADEMICTREKS/views"));
			$this->file_factory->setPath("CSS", "/custom/ACADEMICTREKS/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ACADEMICTREKS/views/");
		}
		
	}
