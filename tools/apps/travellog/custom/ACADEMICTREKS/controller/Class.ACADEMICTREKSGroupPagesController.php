<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDGroupPagesController.php");
	
	/**
	 * The group pages controller of a cobrand.
	 */
	class ACADEMICTREKSGroupPagesController extends COBRANDGroupPagesController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ACADEMICTREKS/views"));
			$this->file_factory->setPath("CSS", "/custom/ACADEMICTREKS/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ACADEMICTREKS/views/");
		}
		
	}
	
