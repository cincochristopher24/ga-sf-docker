<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class ACADEMICTREKSAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ACADEMICTREKS/views'));
			$this->mFactory->setPath('CSS', '/custom/ACADEMICTREKS/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/ACADEMICTREKS/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>