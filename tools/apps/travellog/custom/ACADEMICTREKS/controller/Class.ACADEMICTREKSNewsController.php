<?php
	/*
	 * Class.COBRANDMessagesController.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class ACADEMICTREKSNewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ACADEMICTREKS/views'));
			$this->file_factory->setPath('CSS', '/custom/ACADEMICTREKS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ACADEMICTREKS/views/');
		}
	 	
	 }
?>