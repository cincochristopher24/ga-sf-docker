<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDGroupsController.php');
class QUINNIPIACGroupsController extends COBRANDGroupsController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/QUINNIPIAC/views'));
		$this->file_factory->setPath('CSS', '/custom/QUINNIPIAC/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/QUINNIPIAC/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>