<?php
	/*
	 * Class.COBRANDMessagesController.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class GVINewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GVI/views'));
			$this->file_factory->setPath('CSS', '/custom/GVI/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GVI/views/');
		}
	 	
	 }
?>