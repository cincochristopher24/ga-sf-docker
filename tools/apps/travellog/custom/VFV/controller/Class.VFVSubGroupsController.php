<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class VFVSubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new VFV subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/VFV/views"));
			$this->file_factory->setPath("CSS", "/custom/VFV/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/VFV/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}