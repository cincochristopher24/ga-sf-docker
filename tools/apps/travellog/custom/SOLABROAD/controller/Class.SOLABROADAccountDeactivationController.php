<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDAccountDeactivationController.php');

class SOLABROADAccountDeactivationController extends COBRANDAccountDeactivationController
{
	public function __construct()
	{
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/SOLABROAD/views'));
		$this->file_factory->setPath('CSS', '/custom/SOLABROAD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SOLABROAD/views/'); 
	}
}