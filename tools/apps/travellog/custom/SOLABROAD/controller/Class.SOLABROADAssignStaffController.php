<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class SOLABROADAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SOLABROAD/views'));
			$this->mFactory->setPath('CSS', '/custom/SOLABROAD/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/SOLABROAD/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>