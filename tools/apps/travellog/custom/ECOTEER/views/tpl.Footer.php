<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	<div id="footer_inner_wrap">
	
		<div id="footer_links">
			<div id="inner_footer_links">
				<div id="innermost_footer_links">
					<ul>
						<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>"> Ecoteer </a></li>
						<?= NavigationHelper::displayGlobalNavigation() ?>
					</ul>

					<ul>
		        		<?= NavigationHelper::displayMainNavigation($page_location); ?>
						<li class="last">
							<?= HelperGlobal::displayLoginLink(); ?>
						</li>
					</ul>
				</div>
			</div>
			
			<div id="footlet_wrapper">
				<p id="footlet">Copyright &copy; <? echo date("Y"); ?> Ecoteer and GoAbroad.net&reg;</p>
			</div>
		</div>
	</div>
</div>