<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDGroupsController.php');
	
	class ECOTEERGroupsController extends COBRANDGroupsController{
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ECOTEER/views'));
			$this->file_factory->setPath('CSS', '/custom/ECOTEER/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ECOTEER/views/'); 
		}
		 
		function performAction(){ 
			parent::performAction();
		}
	}