<?php
	
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTicketController.php');
	
 	class UCBOULDERTicketController extends COBRANDTicketController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/UCBOULDER/views'));
			$this->file_factory->setPath('CSS', '/custom/UCBOULDER/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/UCBOULDER/views/');
 		}
 		
 		function performAction(){ 
			parent::performAction();
		}
 	}