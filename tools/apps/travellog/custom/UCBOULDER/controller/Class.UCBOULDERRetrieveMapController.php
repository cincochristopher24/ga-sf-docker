<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	UCBOULDERRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for UCBOULDER	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/UCBOULDER/views'));
		$file_factory->setPath('CSS', '/custom/UCBOULDER/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/UCBOULDER/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}