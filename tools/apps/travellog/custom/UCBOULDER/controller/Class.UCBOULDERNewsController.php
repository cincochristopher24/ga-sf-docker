<?php
	/*
	 * Class.COBRANDMessagesController.php
	 * Created on Dec 16, 2007
	 * created by 
	 */
	 
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class UCBOULDERNewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/UCBOULDER/views'));
			$this->file_factory->setPath('CSS', '/custom/UCBOULDER/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/UCBOULDER/views/');
		}
	 	
	 }
?>