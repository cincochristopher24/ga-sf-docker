<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
	
	class ISLAPagesController extends COBRANDPagesController {
	
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISLA/views'));
			$this->file_factory->registerTemplate('PrivacyPolicy',array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->registerTemplate('TermsDefault', array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->registerTemplate('TermsAdvisor', array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/ISLA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISLA/views/');
		}
	
		function performAction(){
			parent::performAction();
		}
	
	}