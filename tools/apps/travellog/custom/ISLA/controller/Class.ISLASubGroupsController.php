<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class ISLASubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new ISLA subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ISLA/views"));
			$this->file_factory->setPath("CSS", "/custom/ISLA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ISLA/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}