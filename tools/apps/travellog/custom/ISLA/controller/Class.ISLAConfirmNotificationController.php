<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDConfirmNotificationController.php');
	
 	class ISLAConfirmNotificationController extends COBRANDConfirmNotificationController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISLA/views'));
			$this->file_factory->setPath('CSS', '/custom/ISLA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISLA/views/');
 		}
 	}