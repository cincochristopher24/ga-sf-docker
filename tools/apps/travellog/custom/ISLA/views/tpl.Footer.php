<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	<div id="footer_inner_wrap">
		<div id="isla_filler">&nbsp;</div>
		<div id="footer_links">
			<div>
			<ul>
				<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>"> Isla </a></li>
				<?= NavigationHelper::displayGlobalNavigation() ?>
			</ul>

			<ul>
        		<?= NavigationHelper::displayMainNavigation($page_location); ?>
				<li class="last">
					<?= HelperGlobal::displayLoginLink(); ?>
				</li>
			</ul>
			<p id="footlet">Copyright &copy; <? echo date("Y"); ?> Isla and GoAbroad.net&reg;</p>
			</div>
		</div>
	</div>
</div>