<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDFeedbackController.php');
class CISDENVERFeedbackController extends COBRANDFeedbackController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CISDENVER/views'));
		$this->file_factory->setPath('CSS', '/custom/CISDENVER/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CISDENVER/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>