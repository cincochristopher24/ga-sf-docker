<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewMemberController.php');
	
	class CISDENVERNewMemberController extends COBRANDNewMemberController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CISDENVER/views'));
			$this->file_factory->setPath('CSS', '/custom/CISDENVER/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CISDENVER/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
