<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class CISDENVERAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CISDENVER/views'));
			$this->mFactory->setPath('CSS', '/custom/CISDENVER/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/CISDENVER/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>