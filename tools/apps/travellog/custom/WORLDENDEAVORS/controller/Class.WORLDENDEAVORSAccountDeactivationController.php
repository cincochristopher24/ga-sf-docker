<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDAccountDeactivationController.php');

class WORLDENDEAVORSAccountDeactivationController extends COBRANDAccountDeactivationController
{
	public function __construct()
	{
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/WORLDENDEAVORS/views'));
		$this->file_factory->setPath('CSS', '/custom/WORLDENDEAVORS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/WORLDENDEAVORS/views/'); 
	}
}