<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelerEventController.php');
class WORLDENDEAVORSTravelerEventController extends COBRANDTravelerEventController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/WORLDENDEAVORS/views'));
		$this->file_factory->setPath('CSS', '/custom/WORLDENDEAVORS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/WORLDENDEAVORS/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>