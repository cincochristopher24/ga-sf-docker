<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	WORLDENDEAVORSRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for WORLDENDEAVORS	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/WORLDENDEAVORS/views'));
		$file_factory->setPath('CSS', '/custom/WORLDENDEAVORS/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/WORLDENDEAVORS/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}