<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');
	
	class WORLDENDEAVORSAssignStaffController extends COBRANDAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/WORLDENDEAVORS/views'));
			$this->mFactory->setPath('CSS', '/custom/WORLDENDEAVORS/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/WORLDENDEAVORS/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
