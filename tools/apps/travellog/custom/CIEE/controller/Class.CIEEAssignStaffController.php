<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class CIEEAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIEE/views'));
			$this->mFactory->setPath('CSS', '/custom/CIEE/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/CIEE/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>