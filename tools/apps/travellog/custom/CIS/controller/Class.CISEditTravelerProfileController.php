<?php
 require_once('travellog/custom/COBRAND/controller/Class.COBRANDEditTravelerProfileController.php');
 
 class CISEditTravelerProfileController extends COBRANDEditTravelerProfileController {
 	function __construct(){
 		parent::__construct();
 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views'));
 		//$this->file_factory->registerTemplate('FrmEditTravelerProfile', array( 'path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/CIS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/'); 
		
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 }
?>