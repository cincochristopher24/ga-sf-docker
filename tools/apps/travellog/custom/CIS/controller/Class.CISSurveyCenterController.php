<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyCenterController.php');
	
 	class CISSurveyCenterController extends COBRANDSurveyCenterController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views'));
			$this->file_factory->setPath('CSS', '/custom/CIS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/');
 		}
 	}
?>