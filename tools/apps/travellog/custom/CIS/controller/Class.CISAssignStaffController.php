<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');
	
	class CISAssignStaffController extends COBRANDAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views'));
			$this->mFactory->setPath('CSS', '/custom/CIS/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
