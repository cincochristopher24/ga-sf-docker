<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDGroupsController.php');
class CISGroupsController extends COBRANDGroupsController{
	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views'));
		$this->file_factory->setPath('CSS', '/custom/CIS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/'); 
	}
	 
	function performAction(){ 

		parent::performAction();
		
	}
}

?>