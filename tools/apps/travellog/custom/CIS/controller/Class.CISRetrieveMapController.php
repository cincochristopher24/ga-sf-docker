<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	CISRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for CIS	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/CIS/views'));
		$file_factory->setPath('CSS', '/custom/CIS/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}