<?php
/*
	Filename:		Class.CISTravelPlansController.php
	Author:			Jonas Tandinco
	Date Created:	Nov/27/2007
	Putpose:		controller implementation for CIS co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelPlansController.php');

class CISTravelPlansController extends COBRANDTravelPlansController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views'));
		$this->file_factory->setPath('CSS', '/custom/CIS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/');   
	}
	
	public function performAction(){ 
		parent::performAction();
	}
}
?>
