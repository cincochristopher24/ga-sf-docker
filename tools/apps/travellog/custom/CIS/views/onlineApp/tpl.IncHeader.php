<?php
require_once('Class.HelperGlobal.php');
require_once('travellog/helper/Class.NavigationHelper.php');
if (!isset($page_location)) {
	$page_location = '';
}
?>
<div id="header">		
	<div id="heading_and_login_status" class="line">
		<h1><a href="http://www.cisabroad.com/" target="_blank"><span>Center for International Studies</span></a></h1>
		<h2 class="header_text">Online Application</h2>
	</div>
</div>
