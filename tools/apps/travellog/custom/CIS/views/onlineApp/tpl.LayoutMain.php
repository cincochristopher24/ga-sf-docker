<?php
require_once('travellog/factory/Class.FileFactory.php');

$obj_factory = FileFactory::getInstance();

// DEFAULTS
if (!isset($title)) 		  $title 			= 'CIS : Online Community for Travelers';
else
$title = str_replace('GoAbroad Network', 'CIS', $title);
if (!isset($metaDescription)) $metaDescription  = '';
if (!isset($metaKeywords))    $metaKeywords     = 'travel community, travel network, travel online, travel website, travel web site, travel blog, travel note, free travel blogs, travel blogs, free travel blog, personal travel blogs, travel notes, travel journal, travel community website, travel notes blog, free travel journal, free online travel journal, travel journal online, blogs, journals, travel diaries, travel journals, travel diary, traveler, travelers community, travelers community, traveler community, free travel journals, free online travel journals, online travelers community, travel review, 
travel comments, travel observations, trip journals, travelogues, travel reviews, travel guide, travel advice, travel photos, travel maps';
if (!isset($layoutID)) 		  $layoutID 		= 'basic';
if (!isset($page_location))	  $page_location 	= '';

//header('Content-Type: text/html, charset=utf-8');


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=$title?></title>
<meta name="description" content="<?=$metaDescription ?>" />
<meta name="keywords" content="<?=$metaKeywords?>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link type="text/css" rel="stylesheet" href="/min/b=css&amp;f=reset.css,type.css,layout.css,widget.css,section.css,forms.css" />


<style type="text/css">
@import url("/css/<?=$layoutID?>.css");
</style>

<link href="<?=$obj_factory->getPath('CSS')?>main.css" rel="stylesheet" type="text/css" />

<?//php Template::includeDependentCss("/min/f=css/cobrand_customhome.css"); ?>

<link type="text/css" rel="stylesheet" href="/min/f=css/cobrand_customhome.css" />

<?php
	/** Additional meta tags needed only in /index.php - icasimpan Apr 25,2007
	 *  y_key     => Yahoo Site Explorer
	 *  verify_v1 => Google Sitemaps
	 *  NOODP     => Dmoz title not to be used in search engines results pages
	 *  NOYDIR    => Yahoo directory title not to be used in search engines results pages
	 */ 
	if ($_SERVER['SCRIPT_NAME'] == '/index.php'):
		echo '<meta name="y_key" content="9f51662a4b1147d3" />' . "\n";
		echo '<meta name="verify-v1" content="t7aF38KKCgN0Y6Z6vX+t5jnjJ+8J1oR/rCT1mQz1EOs=" />' . "\n";
		echo '<meta name="robots" content="NOODP" />'  . "\n";
		echo '<meta name="robots" content="NOYDIR" />' . "\n";		 
	endif;
        // NOTE: __utm.js =>  GoAbroad in-house Urchin Web Analytics - iaacasimpan Nov 7,2007
?>
<!-- <script src="/__utm.js" type="text/javascript"></script> -->

<?php 
	//Template::includeDependentJs('/js/prototype.js', array('include_here' => true));
	Template::includeDependentJs('/js/custompopup-1.2.js', array('include_here' => true)); // added by ianne - 11/27/2008
?>


<!-- compliance patch for microsoft browsers -->
<!--[if lt IE 8]>
<link href="/css/main.ie.fix.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
//Fix image cache problem with IE 6
try {
  document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
</script> 

<![endif]-->



</head>
<body id="<?=$layoutID?>">

<div id="wrapper">
	<div id="inner_wrapper">
	<?php include($obj_factory->getPath('HEADER_FOOTER').'tpl.IncHeader.php'); ?>
	
	<div id="body">
		
		<?=$contents?>
		
	</div>
	  
	
		<?php include($obj_factory->getPath('HEADER_FOOTER').'tpl.Footer.php'); ?>
	</div>
</div>
</body>
</html>
