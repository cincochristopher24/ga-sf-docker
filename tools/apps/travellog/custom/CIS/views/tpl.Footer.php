<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	<div id="footer_inner_wrap">
	
		<div id="footer_links">
			<div>
			<ul>
				<li class="first"><a href="http://www.cisabroad.com/">Center for International Studies</a></li>
				<li><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>">My CIS</a></li>
				<?= NavigationHelper::displayGlobalNavigation() ?>
			</ul>

			<ul>
			    <?= NavigationHelper::displayMainNavigation($page_location); ?>
				<li class="last">
					<?= HelperGlobal::displayLoginLink(); ?>
				</li>
			</ul>
			<p id="footlet">Copyright &copy; <? echo date("Y"); ?> Center for International Studies and GoAbroad.net&reg;</p>
			</div>
		</div>
	</div>
</div>
	
