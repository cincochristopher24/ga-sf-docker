<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTopicController.php');

	class APITopicController extends COBRANDTopicController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/API/views/'));
			$file_factory->setPath('CSS', '/custom/API/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/API/views/');
		}
	}