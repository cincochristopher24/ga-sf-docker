<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class APISubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new API subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/API/views"));
			$this->file_factory->setPath("CSS", "/custom/API/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/API/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}