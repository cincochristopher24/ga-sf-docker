<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	ADELANTERetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for ADELANTE	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ADELANTE/views'));
		$file_factory->setPath('CSS', '/custom/ADELANTE/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADELANTE/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}