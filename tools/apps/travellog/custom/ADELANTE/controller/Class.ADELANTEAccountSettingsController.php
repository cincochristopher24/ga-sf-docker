<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDAccountSettingsController.php');

class ADELANTEAccountSettingsController extends COBRANDAccountSettingsController
{
	public function __construct()
	{
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ADELANTE/views'));
		$this->file_factory->setPath('CSS', '/custom/ADELANTE/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADELANTE/views/'); 
	}
}