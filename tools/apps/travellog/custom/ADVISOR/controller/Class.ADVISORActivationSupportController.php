<?php
	/*
	 * Class.ADVISORActivationSupportController.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
	
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDActivationSupportController.php");
	
	class ADVISORActivationSupportController extends COBRANDActivationSupportController{
		
		function __construct(){
			
			parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ADVISOR/views'));
			$this->file_factory->setPath('CSS', '/custom/ADVISOR/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADVISOR/views/');
				
		}
		
	} 
	 
?>
