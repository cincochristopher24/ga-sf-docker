<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDConfirmNotificationController.php');
	
 	class ADVISORConfirmNotificationController extends COBRANDConfirmNotificationController
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ADVISOR/views'));
			$this->file_factory->setPath('CSS', '/custom/ADVISOR/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADVISOR/views/');
 		}
 	}
?>