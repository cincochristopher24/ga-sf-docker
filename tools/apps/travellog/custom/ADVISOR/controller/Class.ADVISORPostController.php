<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostController.php');
	
 	class ADVISORPostController extends COBRANDPostController {
 		function __construct(){
	 		parent::__construct();
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ADVISOR/views'));
			$file_factory->setPath('CSS', '/custom/ADVISOR/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADVISOR/views/');
 		}
 		
 	}