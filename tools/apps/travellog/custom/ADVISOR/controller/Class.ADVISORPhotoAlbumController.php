<?php
/*
 * Created on 11 13, 07
 * 
 * Author: J. Llano
 * Purpose: Photo module controller for ADVISOR 
 * 
 */
 
 require_once('travellog/custom/COBRAND/controller/Class.COBRANDPhotoAlbumController.php');
 
 class ADVISORPhotoAlbumController extends COBRANDPhotoAlbumController {
 	function __construct(){
 		parent::__construct();
 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ADVISOR/views'));
		$this->file_factory->setPath('CSS', '/custom/ADVISOR/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADVISOR/views/'); 
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 }