<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDProgramStepsController.php');
class ADVISORProgramStepsController extends COBRANDProgramStepsController{
	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ADVISOR/views'));
		$this->file_factory->setPath('CSS', '/custom/ADVISOR/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADVISOR/views/');   
	}
	 
	function performAction(){  
	
		parent::performAction(); 
		
	}
	
	function _defineCommonAttributes($_params = array()){
		
		parent::_defineCommonAttributes($_params);
		
		$this->tpl->set_path("travellog/custom/ADVISOR/views/");
		
	}
}
?>