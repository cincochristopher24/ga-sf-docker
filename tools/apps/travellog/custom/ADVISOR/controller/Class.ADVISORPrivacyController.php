<?php
/*
 * Created on 16-Nov-07
 *
 *  Author: K. Gordo
 */
 
 require_once('travellog/custom/COBRAND/controller/Class.COBRANDPrivacyController.php');
 
 class ADVISORPrivacyController extends COBRANDPrivacyController {
 	function __construct(){
 		parent::__construct();
 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ADVISOR/views'));
		$this->file_factory->setPath('CSS', '/custom/ADVISOR/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADVISOR/views/'); 
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 }
  
?>