<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
class ADVISORPagesController extends COBRANDPagesController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ADVISOR/views'));
		$this->file_factory->registerTemplate('PrivacyPolicy', array( 'path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsAdivsor', array( 'path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsDefault', array( 'path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/ADVISOR/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ADVISOR/views/');
	}

	function performAction(){  
		parent::performAction();
	}
	
	// Redirect links to about-us page to index.php
	protected function _viewAboutUs(){ 
		//echo ;
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/index.php');
	}
	

}
?>