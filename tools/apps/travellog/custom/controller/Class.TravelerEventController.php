<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelerEventController.php');
	
	class TravelerEventController extends COBRANDTravelerEventController {
	
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom//views'));
			$this->file_factory->setPath('CSS', '/custom//css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom//views/');
		}
	
		function performAction(){
			parent::performAction();
		}
	
	}