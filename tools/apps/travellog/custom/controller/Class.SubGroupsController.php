<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class SubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new  subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom//views"));
			$this->file_factory->setPath("CSS", "/custom//css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom//views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}