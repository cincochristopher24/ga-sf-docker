<?php
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class NewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom//views'));
			$this->file_factory->setPath('CSS', '/custom//css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom//views/');
		}
	 	
	 }