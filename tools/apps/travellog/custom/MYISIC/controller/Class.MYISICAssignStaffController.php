<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class MYISICAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/MYISIC/views'));
			$this->mFactory->setPath('CSS', '/custom/MYISIC/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/MYISIC/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>