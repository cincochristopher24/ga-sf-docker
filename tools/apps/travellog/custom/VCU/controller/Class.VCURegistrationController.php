<?php
	 //require_once("travellog/controller/Class.TravelerRegistrationController.php");
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDRegistrationController.php");
	
	//class VCURegistrationController extends TravelerRegistrationController{
	class VCURegistrationController extends COBRANDRegistrationController{

		const FOLDER_NAME = 'UCDENVER';

		function __construct(){
			parent::__construct();

			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/'.self::FOLDER_NAME.'/views'));
			$this->file_factory->setPath('CSS', '/custom/'.self::FOLDER_NAME.'/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/'.self::FOLDER_NAME.'/views/');
		}

		/*function __construct(){
			parent::__construct();
			$this->file_factory->registerClass('Config');
			$this->file_factory->registerClass('AdminGroup', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('COBRANDRegisterHelper', array('path' => 'travellog/helper/'));
			$this->file_factory->registerClass('COBRANDLoginController', array('path' => 'travellog/custom/COBRAND/controller'));
			$this->file_factory->registerClass('MailGeneratorCB', array('path' => 'travellog/custom/COBRAND/model'));
			$GLOBALS['CONFIG'] = $this->config = $this->file_factory->getClass('Config');

			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
			$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/'); 

			$this->register_helper = $this->file_factory->invokeStaticClass('COBRANDRegisterHelper', 'getInstance');
		}

		function performAction(){

			switch( $this->register_helper->getAction() ){

				case constants::SUBMIT_DETAILS:
					return $this->submitDetails();
					break;

				case constants::CONFIRM_DETAILS:
					return $this->registerUser();
					break;		

				case constants::BACK:
					return $this->modifyDetails();
					break;

				default:
					return $this->viewRegistrationForm();
			}
		}

		function saveUserDetails($tmpRow){

			$travKeys = parent::saveUserDetails($tmpRow);
			$traveler = $this->file_factory->getClass("Traveler",array($travKeys["travelerID"]));
			$myGroup = $this->file_factory->getClass("AdminGroup", array($this->config->getGroupID()));
			$myGroup->addMember($traveler);
			return array("travelerID" => $traveler->getTravelerID());

		}

		function saveDetailsToTempTable( &$travelerData = array() ){
			parent::saveDetailsToTempTable($travelerData);
			$travelerData["user"] = constants::TRAVELERCB;
		}

		function viewRegistrationForm($content = array()){
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			$security = $this->file_factory->invokeStaticClass("Captcha","createImage");

			$travText = '<p>As a ' . $this->config->getSiteName() . ' Network Traveler, you will be able to</p>
				<ul style="margin-left:23px;list-style-type:disc;">
					<li>meet and be friends with travelers from all over the world</li>
					<li>blog about your travels in your very own travel journals</li>
					<li>post and share travel photos</li>
					<li>join groups of like minded travelers</li>
					<li>post messages for your friends and groupmates</li>
					<li>search for programs for your travels abroad</li>
					<li>and lots of other features rolling out in the following weeks!</li>
				</ul>';

			$nContent = array(
				"arrErrorMessages"	=>	isset($content["arrErrorMessages"]) ? $content["arrErrorMessages"] : array(),		
				"lastname"			=>	isset($content["lastname"]) ? trim($content["lastname"]) : "",
				"firstname"			=>	isset($content["firstname"]) ? trim($content["firstname"]) : "",
				"usrname"			=>	isset($content["username"]) ? trim($content["username"]) : "",
				"password"			=>	isset($content["password"]) ? trim($content["password"]) : "",
				"emailAdd"			=>	isset($content["emailAdd"]) ? trim($content["emailAdd"]) : "",
				"subscribeNL"		=>	isset($content["subscribeNL"]) ? $content["subscribeNL"] : 0,
				"regKey"			=>	isset($content["regKey"]) ? trim($content["regKey"]) : "",
				"regID"				=>	isset($content["regID"]) ? trim($content["regID"]) : "",
				'university'		=>	isset($content['university']) ? trim($content['university']) : '',
				'chkStudent'		=>	isset($content['chkStudent']) ? trim($content['chkStudent']) : '',				
				"security"			=>	$security,
				'travText'			=>	$travText,
				'agreeTerm'			=>	isset($content['agreeTerm']) ? $content['agreeTerm'] : 0,
				'siteName'			=> 	$this->config->getSiteName()
			);

			Template::includeDependentJs('/js/interactive.form.js');
			Template::includeDependentJs('js/tools/common.js');
			Template::includeDependentJs('js/utils/json.js');
			Template::includeDependentJs('captcha/captcha.js');
			$code = <<<STYLE
			<script type="text/javascript">
				var Captcha = new CaptchaLoader({					
					imageTagID	: 'captchaImg',
					onLoad		: function(params){			
						$('encSecCode').value  = params['code'];
					},
					imgAttributes : {
						width : '200',
						height: '50'	
					}
				});

				function changeStatusOnClick(){
					if( document.getElementById('chkStudent').checked )
						document.getElementById('liStudentBox').style.display = 'block';
					else
						document.getElementById('liStudentBox').style.display = 'none';
				}
			</script>
STYLE;

			Template::includeDependent($code);
			Template::setMainTemplateVar('layoutID', 'page_default');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
			Template::includeDependentJs('/js/popup.js');
			Template::setMainTemplateVar('title', 'Register as Traveler - Join the ' . $this->config->getSiteName() . ' Network!');
			Template::setMainTemplateVar('metaDescription', 'Become friends with other travelers on the site, post messages for each other, join interesting forums and form clubs with those who share your interests.');

			$fcd = $this->file_factory->getClass('RegisterFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveTravelerRegistrationForm()) );
			$obj_view->render();

		}

		function saveCommonUserDetails($travelerID,$email,$subscribeNL = 0){

			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));

			// remove user from invite list

			$sqlquery = "SELECT groupID FROM tblEmailInviteList WHERE email = '" . addslashes($email) . "'";
			$myquery = $rs->Execute($sqlquery);
			if( mysql_num_rows($myquery) ){
				$tmpTraveler = $this->file_factory->getClass("Traveler",array($travelerID));
				$factory = $this->file_factory->invokeStaticClass("GroupFactory","instance");
				while( $tmpGroupID = mysql_fetch_array($myquery) ){
					$group = $factory->create( array($tmpGroupID["groupID"]) );
					if( $tmpGroupID != $this->config->getGroupID() )
						$group[0]->addInvites( array($tmpTraveler) );	
					$group[0]->removeFromEmailInviteList($email);
				}
			}

			$mailGenerator = $this->file_factory->getClass("MailGeneratorCB", array($travelerID, 0, $this->config));
			$mailGenerator->send();

			// if user wants to subscribe in GoAbroad Newsletter

			if ( $subscribeNL ){

				// check if user is already a subscriber
				// subscribe the user to the GoAbroad Newsletter if not a subscriber and send a confirmation letter

				$sqlquery = "select studentemailID from GoAbroad_Main.tbstudentemail where email = '" . addslashes($email) . "'";
				$myquery = $rs->Execute($sqlquery);
				if ( mysql_num_rows($myquery) == 0 ){

					$sqlquery = "insert into GoAbroad_Main.tbstudentemail (email, send, datecreated)
								  VALUES ('" . addslashes($email) . "', 1, '" . date('c') . "' )";
					$rs->Execute($sqlquery);
					unset($mailGenerator);
					$mailGenerator = $this->file_factory->getClass("MailGenerator", array($travelerID,1,$this->config));
					$mailGenerator->send();	

				}
			}		

		}

		function registerUser(){

			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));

			$travelerData = $this->register_helper->readTravelerData();

			$sqlquery = "SELECT * FROM tblTmpRegister " .
				"WHERE registerID = '" . addslashes($travelerData["regID"]) . "' " .  
				" AND regKey = '" . addslashes($travelerData["regKey"]) . "'";
			$myquery = $rs->Execute($sqlquery);

			if($tmpRow = mysql_fetch_array($myquery)){
				$arrTrav = $this->saveUserDetails($tmpRow);		
				$crypt = $this->file_factory->getClass("Crypt");
				$cryptTravID = $crypt->encrypt($arrTrav['travelerID']);
				$cryptTravID = substr($cryptTravID,0,strlen($cryptTravID)-1);
				$registerLoginController = $this->file_factory->getClass('COBRANDLoginController');
				return $registerLoginController->activateAndLoginNewAccount($cryptTravID); 
			}
			else{
				header("location:" . $this->register_helper->getRedirectPage(constants::REGISTER_PAGE));
			}

		}
		*/
		
	}