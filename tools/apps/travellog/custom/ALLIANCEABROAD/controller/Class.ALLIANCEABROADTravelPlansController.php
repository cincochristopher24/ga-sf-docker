<?php
/*
	Filename:		Class.ALLIANCEABROADTravelPlansController.php
	Author:			Jonas Tandinco
	Date Created:	Dec/13/2007
	Putpose:		controller implementation for ALLIANCEABROAD co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelPlansController.php');

class ALLIANCEABROADTravelPlansController extends COBRANDTravelPlansController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ALLIANCEABROAD/views'));
		$this->file_factory->setPath('CSS', '/custom/ALLIANCEABROAD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ALLIANCEABROAD/views/'); 
	}
	
	public function performAction() {
		parent::performAction();
	}
}
?>