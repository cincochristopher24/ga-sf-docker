<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyCenterController.php');
	
 	class ALLIANCEABROADSurveyCenterController extends COBRANDSurveyCenterController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ALLIANCEABROAD/views'));
			$this->file_factory->setPath('CSS', '/custom/ALLIANCEABROAD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ALLIANCEABROAD/views/');
 		}
 	}
?>