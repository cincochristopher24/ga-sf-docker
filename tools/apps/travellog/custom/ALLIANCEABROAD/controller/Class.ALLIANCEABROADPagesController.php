<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
class ALLIANCEABROADPagesController extends COBRANDPagesController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ALLIANCEABROAD/views'));
		$this->file_factory->registerTemplate('PrivacyPolicy',array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsDefault', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsAdvisor', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/ALLIANCEABROAD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ALLIANCEABROAD/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>