<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class ALLIANCEABROADAssignStaffController extends COBRANDAssignStaffController{

		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ALLIANCEABROAD/views'));
			$this->mFactory->setPath('CSS', '/custom/PROWORLD/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/ALLIANCEABROAD/views/');
		}

		public function performAction(){
			parent::performAction();
		}
	}
?>
