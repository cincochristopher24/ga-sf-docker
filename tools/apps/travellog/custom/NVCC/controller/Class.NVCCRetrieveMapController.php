<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	NVCCRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for NVCC	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/NVCC/views'));
		$file_factory->setPath('CSS', '/custom/NVCC/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/NVCC/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}