<?php  
	/**
	 * @(#) Class.NVCCTemplateStaffController.php
	 * 
	 * Autogenerated by GoAbroad Code Generator 1.0
	 *
	 * @version 1.0 Jun 06, 2009 
	 */	
	
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTemplateStaffController.php');
	
	/**
	 * TemplateStaff cobrand controller class for NVCC.
	 */
	class NVCCTemplateStaffController extends COBRANDTemplateStaffController {
		
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/NVCC/views'));
			$this->file_factory->setPath('CSS', '/custom/NVCC/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/NVCC/views/'); 
		}
		
	}