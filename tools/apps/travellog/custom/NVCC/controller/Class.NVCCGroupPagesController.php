<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDGroupPagesController.php');
class NVCCGroupPagesController extends COBRANDGroupPagesController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/NVCC/views'));
		$this->file_factory->setPath('CSS', '/custom/NVCC/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/NVCC/views/');
	}

	function performAction(){
		parent::performAction();
	}

}