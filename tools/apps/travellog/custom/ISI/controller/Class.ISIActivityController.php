<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDActivityController.php');
class ISIActivityController extends COBRANDActivityController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISI/views'));
		$this->file_factory->setPath('CSS', '/custom/ISI/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISI/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>