<?php

	require_once('travellog/custom/COBRAND/controller/Class.COBRANDCustomGroupSectionController.php');

	class ISICustomGroupSectionController extends COBRANDCustomGroupSectionController {

		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ISI/views/'));
			$this->file_factory->setPath('CSS', '/custom/ISI/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISI/views/');
		}
	}
?>