<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionController.php');

	class ISIDiscussionController extends COBRANDDiscussionController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ISI/views/'));
			$file_factory->setPath('CSS', '/custom/ISI/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISI/views/');
		}
	}