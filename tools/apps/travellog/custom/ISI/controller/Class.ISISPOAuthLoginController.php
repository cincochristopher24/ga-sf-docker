<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSPOAuthLoginController.php");
	
	/**
	 * The SP OAuth login controller of a cobrand.
	 */
	class ISISPOAuthLoginController extends COBRANDSPOAuthLoginController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ISI/views"));
			$this->file_factory->setPath("CSS", "/custom/ISI/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ISI/views/");
		}
		
	}
	
