<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDActivationSupportController.php');
class CENTRALCOLLEGEActivationSupportController extends COBRANDActivationSupportController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CENTRALCOLLEGE/views'));
		$this->file_factory->setPath('CSS', '/custom/CENTRALCOLLEGE/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CENTRALCOLLEGE/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>