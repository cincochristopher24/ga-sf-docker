<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class CENTRALCOLLEGEAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CENTRALCOLLEGE/views'));
			$this->mFactory->setPath('CSS', '/custom/CENTRALCOLLEGE/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/CENTRALCOLLEGE/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>