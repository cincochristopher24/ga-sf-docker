<?php
require_once('Class.HelperGlobal.php');
require_once('travellog/helper/Class.NavigationHelper.php');
if (!isset($page_location)) {
	$page_location = '';
}
?>

<div id="header">
		
		<div id="heading_and_login_status">
			<h1><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>"><span>Central College</span></a></h1>
		</div>
		
		<ul id="skip">
			<?php if ($page_location === 'Home'): ?>
				<li><a href="#a_login">Skip to Login</a></li>
			<?php endif; ?>
			<li><a href="#a_content">Skip to Content Area</a></li>
		</ul>
		
		<ul id="global_navigation">
			<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>"> Central College </a></li>
			<?= NavigationHelper::displayGlobalNavigation() ?>
		</ul>
		
		<ul id="main_nav">
			<?	
				echo NavigationHelper::displayMainNavigation($page_location);
			?>			
		</ul>
		<div class="clear"></div>
		
		<? if (!isset($_REQUEST['ref'])): // login fields won't show in change password if user has forgot his password: neri 12-23-08 ?>
			<? if (HelperGlobal::isLogin()):?>
			<ul id="access_links">
			<li class="last">
				Welcome back, <strong><?= HelperGlobal::getEmailAddress() ?></strong> | 
			<?= HelperGlobal::displayLoginLink(); ?>
			</li>
			</ul>	
			<? elseif ( '/login.php' != $_SERVER['SCRIPT_NAME']): ?>
			
				<?
						$obj_controllerfactory = ControllerFactory::getInstance();
						$viewLogin = $obj_controllerfactory->createController('Login')->performAction();
						echo $viewLogin->render();
				?>
			
			<? endif; ?>
		<? endif; ?>
</div>
