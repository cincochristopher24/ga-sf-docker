<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostController.php');

 	class ISVPostController extends COBRANDPostController {

 		function __construct()
 		{
	 		parent::__construct();

	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISV/views'));
			$file_factory->setPath('CSS', '/custom/ISV/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISV/views/');

			//Quick fix (HAM 16May2011):
			//apparently this line is causing the error when replying to a discussion topic.
			//other cobrand's controllers don't have this line; why is this here? it's possible
			//that something else will break by commenting out this line
			//Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));
 		}

 	}