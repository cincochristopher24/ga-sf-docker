<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDTagManagementController.php');
class ISVTagManagementController extends COBRANDTagManagementController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISV/views'));
		$this->file_factory->setPath('CSS', '/custom/ISV/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISV/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>