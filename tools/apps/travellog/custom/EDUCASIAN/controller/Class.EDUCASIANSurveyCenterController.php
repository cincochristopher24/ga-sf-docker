<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyCenterController.php');
class EDUCASIANSurveyCenterController extends COBRANDSurveyCenterController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/EDUCASIAN/views'));
		$this->file_factory->setPath('CSS', '/custom/EDUCASIAN/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/EDUCASIAN/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>