<?php
/*
 * Created on 12 03, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
	
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostCardController.php');
	
 	class EDUCASIANPostCardController extends COBRANDPostCardController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/EDUCASIAN/views'));
			$this->file_factory->setPath('CSS', '/custom/EDUCASIAN/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/EDUCASIAN/views/');
 		}
 		
 		function performAction(){ 
			parent::performAction();
		}
 	}