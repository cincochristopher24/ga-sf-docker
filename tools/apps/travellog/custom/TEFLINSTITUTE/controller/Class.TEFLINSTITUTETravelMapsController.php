<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');
	
	class TEFLINSTITUTETravelMapsController extends COBRANDTravelMapsController{
		
		function __construct(){
			parent::__construct();
			
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/TEFLINSTITUTE/views'));
			$this->file_factory->setPath('CSS', '/custom/TEFLINSTITUTE/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/TEFLINSTITUTE/views/');   
		}
		
		function performAction(){ 
			parent::performAction();
		}
	}