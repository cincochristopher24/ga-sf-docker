<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	BARCELONASAERetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for BARCELONASAE	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/BARCELONASAE/views'));
		$file_factory->setPath('CSS', '/custom/BARCELONASAE/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/BARCELONASAE/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}