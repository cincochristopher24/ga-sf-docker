<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class BARCELONASAESubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new BARCELONASAE subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/BARCELONASAE/views"));
			$this->file_factory->setPath("CSS", "/custom/BARCELONASAE/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/BARCELONASAE/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}