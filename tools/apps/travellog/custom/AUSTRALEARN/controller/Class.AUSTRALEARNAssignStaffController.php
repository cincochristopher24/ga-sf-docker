<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class AUSTRALEARNAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AUSTRALEARN/views'));
			$this->mFactory->setPath('CSS', '/custom/AUSTRALEARN/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/AUSTRALEARN/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>