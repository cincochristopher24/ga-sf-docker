<?php

	require_once('travellog/custom/COBRAND/controller/Class.COBRANDCustomGroupSectionController.php');

	class AUSTRALEARNCustomGroupSectionController extends COBRANDCustomGroupSectionController {

		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/AUSTRALEARN/views/'));
			$this->file_factory->setPath('CSS', '/custom/AUSTRALEARN/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AUSTRALEARN/views/');
		}
	}
?>