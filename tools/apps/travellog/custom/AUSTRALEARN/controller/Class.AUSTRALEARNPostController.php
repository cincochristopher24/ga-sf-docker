<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostController.php');
	
 	class AUSTRALEARNPostController extends COBRANDPostController {
 		function __construct(){
	 		parent::__construct();
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AUSTRALEARN/views'));
			$file_factory->setPath('CSS', '/custom/AUSTRALEARN/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AUSTRALEARN/views/');
 		}
 		
 	}