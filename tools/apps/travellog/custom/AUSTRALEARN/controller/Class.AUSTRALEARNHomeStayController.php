<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDHomeStayController.php');
class AUSTRALEARNHomeStayController extends COBRANDHomeStayController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AUSTRALEARN/views'));
		$this->file_factory->setPath('CSS', '/custom/AUSTRALEARN/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AUSTRALEARN/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>