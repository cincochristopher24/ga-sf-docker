<?php

Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
Template::setMainTemplateVar('layoutID', 'admin_group');
Template::setMainTemplateVar('page_location', 'Home'); 

if (isset($homeStay)):
 ?>

<table width="560" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#D8E0EC" bgcolor="#E8EBF4" class="Arial12px" >

	<tr>
	    <td align="right" valign="middle">Name</td>
	    <td align="left" valign="middle" style="bgcolor: #FFFFFF"><?= $homeStay->getName() ?></td>
	  </tr>
	  <tr>
	    <td align="right" valign="middle">Address</td>
	    <td align="left" valign="middle" bgcolor="#FFFFFF"><?= $homeStay->getAddress() ?></td>
	  </tr>
	  <tr>
	    <td align="right" valign="middle">Email</td>
	    <td align="left" valign="middle" bgcolor="#FFFFFF"><?= $homeStay->getEmail() ?></td>
	  </tr>
	  <tr>
	    <td align="right" valign="middle">Phone Number</td>
	    <td align="left" valign="middle" bgcolor="#FFFFFF"><?= $homeStay->getPhone() ?></td>
	  </tr>
	  <tr>
	    <td width="50%" align="right" valign="middle">Family Names </td>
	    <td width="50%" align="left" valign="middle" bgcolor="#FFFFFF"><?= $homeStay->getFamilyNames() ?></td>
	  </tr>
	  <tr>
	    <td align="right" valign="middle">Description</td>
	    <td align="left" valign="middle" bgcolor="#FFFFFF"><?= $homeStay->getDescription() ?></td>
	  </tr>	 
	<? if ($isAdminLogged) { ?>
	  <tr>
		<td colspan="2" align="center">
			<a href="homestay.php?mode=edit&amp;hsID=<?=$homeStay->getHomeStayID()?>">Edit</a>  
			| 
			<? if (null==$homeStay->getAssignedTraveler()) : ?>
				<a href="homestay.php?mode=delete&amp;hsID=<?=$homeStay->getHomeStayID()?>" onClick="return confirm('Delete HomeStay?')">Delete</a>
			<? else : ?>
				<a href="homestay.php?mode=delete&amp;hsID=<?=$homeStay->getHomeStayID()?>" onClick="return confirm('Homestay has assigned traveler. Proceed?')">Delete</a>
			<? endif; ?>
		</td>
	  </tr>		
	<? } ?> 
</table>

<? 
else:
	echo 'You do not have a HomeStay yet.';
endif ; ?>