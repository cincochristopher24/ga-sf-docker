<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	CEARetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for CEA	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/CEA/views'));
		$file_factory->setPath('CSS', '/custom/CEA/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CEA/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}