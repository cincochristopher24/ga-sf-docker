<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAlertManagementController.php');
	
 	class CEAAlertManagementController extends COBRANDAlertManagementController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CEA/views'));
			$this->file_factory->setPath('CSS', '/custom/CEA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CEA/views/');
 		}
 	}
?>