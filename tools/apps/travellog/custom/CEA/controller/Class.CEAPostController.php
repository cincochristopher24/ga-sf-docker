<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostController.php');
	
 	class CEAPostController extends COBRANDPostController {
 		function __construct(){
	 		parent::__construct();
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CEA/views'));
			$file_factory->setPath('CSS', '/custom/CEA/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CEA/views/');
 		}
 		
 	}