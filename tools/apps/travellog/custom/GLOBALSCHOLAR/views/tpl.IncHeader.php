<?php
require_once('Class.HelperGlobal.php');
require_once('travellog/helper/Class.NavigationHelper.php');
require_once('travellog/helper/Class.LoginHelper.php');
if (!isset($page_location)) {
	$page_location = '';
}
?>

<div id="header">
		
		<div id="heading_and_login_status">
			<h1>
				<a id="course_desc" href="http://www.globalscholar.us/course_description.asp"><span>Global Scholar</span></a>
				<a id="global_educ" href="http://globaled.us/"><span>Global Scholar</span></a>
				<a id="fipse" href="http://www.ed.gov/about/offices/list/ope/fipse/index.html"><span>Global Scholar</span></a>
				<a id="lmu" href="http://globaled.us/"><span>Global Scholar</span></a>
			</h1>
		</div>
		
		<ul id="skip">
			<?php if ($page_location === 'Home'): ?>
				<li><a href="#a_login">Skip to Login</a></li>
			<?php endif; ?>
			<li><a href="#a_content">Skip to Content Area</a></li>
		</ul>
		
		<ul id="global_navigation">
			<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>"> Global Scholar </a></li>
			<?= NavigationHelper::displayGlobalNavigation() ?>
		</ul>
		
		<? if(!isset($_REQUEST['ref'])): // login fields won't show in change password if user has forgot his password: neri 12-23-08 ?>
			<? if (HelperGlobal::isLogin()):?>
			<ul id="access_links">
			<li class="last">
				Welcome back, <strong><?= HelperGlobal::getEmailAddress() ?></strong> | 
			<?= HelperGlobal::displayLoginLink(); ?>
			</li>
			</ul>	
			<? elseif ( '/login.php' != $_SERVER['SCRIPT_NAME']): ?>
			
				<?
						$obj_controllerfactory = ControllerFactory::getInstance();
						$viewLogin = $obj_controllerfactory->createController('Login')->performAction();
						echo $viewLogin->render();
						LoginHelper::useLabelToggleJs();
				?>
			
			<? endif; ?>
		<? endif; ?>
		
		<ul id="main_nav">
			<?	
				echo NavigationHelper::displayMainNavigation($page_location);
			?>		
		</ul>
		<div class="clear"></div>
		
</div>
