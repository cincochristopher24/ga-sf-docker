<?php
/*
	Filename:		Class.GLOBALSCHOLARTravelMapsController.php
	Author:			Jonas Tandinco
	Date Created:	Jan/03/2008
	Putpose:		controller implementation for GLOBALSCHOLAR co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');

class GLOBALSCHOLARTravelMapsController extends COBRANDTravelMapsController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GLOBALSCHOLAR/views'));
		$this->file_factory->setPath('CSS', '/custom/GLOBALSCHOLAR/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSCHOLAR/views/');   
	}
	
	public function performAction(){ 
		parent::performAction();
	}
}
?>
