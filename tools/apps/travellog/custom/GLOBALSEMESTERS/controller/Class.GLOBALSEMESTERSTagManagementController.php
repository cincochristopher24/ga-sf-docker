<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDTagManagementController.php');
class GLOBALSEMESTERSTagManagementController extends COBRANDTagManagementController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GLOBALSEMESTERS/views'));
		$this->file_factory->setPath('CSS', '/custom/GLOBALSEMESTERS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSEMESTERS/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>