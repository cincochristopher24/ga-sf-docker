<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
class GLOBALSEMESTERSPagesController extends COBRANDPagesController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GLOBALSEMESTERS/views'));
		$this->file_factory->registerTemplate('PrivacyPolicy',array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsDefault', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsAdvisor', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/GLOBALSEMESTERS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSEMESTERS/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>