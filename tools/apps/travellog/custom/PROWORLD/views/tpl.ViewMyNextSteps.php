<?php
Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
Template::setMainTemplateVar('layoutID', 'admin_group');
Template::setMainTemplateVar('page_location', 'Home');
?>
<form action="programsteps.php?mode=save" method="POST">
<table width="50%" align="center"  border="0" cellspacing="0" cellpadding="0">
     <tr>
        <td width="15" height="10"><img src="images/pixel.gif" width="15" height="10"></td>
        <td><img src="images/pixel.gif" width="1" height="1"></td>
      </tr>
      <tr>
         <td><img src="images/pixel.gif" width="1" height="1"></td>
         <td height="250" align="left" valign="top">
			<table width="100%"  border="0" cellpadding="15" cellspacing="0" class="T12">
               	<tr>
                  <td align="left" valign="top">
			<p>Use this page to guide you through the steps involved in your <strong>ProWorld</strong> program.  This is a self service page.  Please check off each task as it is completed</p>
                     <table width="100%" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#3C6A9D" bgcolor="#5285BE" class="T12">
                       <tr>
                         <td align="left" valign="middle"><strong>Next Steps</strong></td>
                       </tr>
                     </table>
                     <table width="100%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#D8E0EC" bgcolor="#E8EBF4" class="T12">
                   
				<?  foreach($allProgramSteps as $eachStep): ?>
					<tr>
						<td align="right" valign="middle" class="T10" width="25%">
							<font color="#1C4889">
								<input type="checkbox" name="programStep[]" value="<?=$eachStep->getProgramStepID()?>" <?= ($loggedUser->hasAddedProgramStep($eachStep->getProgramStepID()) ? 'checked' : '' ) ?>>
							</font>
						</td>
						<td align="left" valign="middle" bgcolor="#FFFFFF"><?=$eachStep->getProgramStepInfo()?></td>
					</tr>
				<?	endforeach; ?>
					<tr><td colspan="2" align="center"><input type="submit" name="btnSubmit" value="Save Changes"></td>
					</tr>
                     </table>
				</td>
             </tr>
           </table>
		</td>
      </tr>
</table>
</form>