<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 27 November 2007
	 * Class.PROWORLDEditGATravelBioController.php
	 */

	require_once 'travellog/custom/COBRAND/controller/Class.COBRANDEditGATravelBioController.php';
	
	 class PROWORLDEditGATravelBioController extends COBRANDEditGATravelBioController {
	 	function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
			$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/'); 
	 	}
	 }
?>