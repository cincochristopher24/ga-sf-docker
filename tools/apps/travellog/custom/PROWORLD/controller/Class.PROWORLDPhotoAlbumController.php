<?php
/*
 * Created on 11 13, 07
 * 
 * Author: J. Llano
 * Purpose: Photo module controller for PROWORLD 
 * 
 */
 
 require_once('travellog/custom/COBRAND/controller/Class.COBRANDPhotoAlbumController.php');
 
 class PROWORLDPhotoAlbumController extends COBRANDPhotoAlbumController {
 	function __construct(){
 		parent::__construct();
 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
		$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/'); 
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 }