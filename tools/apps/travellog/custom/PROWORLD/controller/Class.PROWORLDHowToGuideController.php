<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDHowToGuideController.php');
class PROWORLDHowToGuideController extends COBRANDHowToGuideController{
	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
		$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/'); 
	}
	 
	function performAction(){ 
		
		$path = "users/resourcefiles/2007/May/1601/admingroup/MyProWorld.NET-How-To-Guide.pdf";
		if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$path))
			header("location: /".$path);
		else{
			require_once('travellog/factory/Class.ControllerFactory.php');
			$obj_factory = ControllerFactory::getInstance();
			$_GET['action'] = 'file_not_found';
			$obj_factory->createController('Pages')->performAction();
		}
		
	}
}