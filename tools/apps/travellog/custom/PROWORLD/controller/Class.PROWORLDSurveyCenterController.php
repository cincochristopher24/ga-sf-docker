<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyCenterController.php');
	
 	class PROWORLDSurveyCenterController extends COBRANDSurveyCenterController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
			$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/');
 		}
 	}
?>