<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDHomeStayController.php');
class PROWORLDHomeStayController extends COBRANDHomeStayController{
	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
		$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/');   
	}
	 
	function performAction(){  
	
		parent::performAction();
		
	}
	
	function _defineCommonAttributes($_params = array()){
		
		parent::_defineCommonAttributes($_params);
		
		$this->tpl->set_path("travellog/custom/PROWORLD/views/");
		
	}
	
}
?>