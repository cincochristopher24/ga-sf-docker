<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
class PROWORLDPagesController extends COBRANDPagesController {

	function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
		$this->file_factory->registerTemplate('PrivacyPolicy', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsAdvisor' , array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsDefault' , array('path' => 'travellog/custom/COBRAND/views'));
		//$this->file_factory->registerTemplate('TermsAdivsor', array( 'path' => 'travellog/custom/PROWORLD/views'));
		//$this->file_factory->registerTemplate('TermsDefault', array( 'path' => 'travellog/custom/PROWORLD/views'));
		$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/');
	}

	function performAction(){  
		parent::performAction();
	}
	
	// Redirect links to about-us page to index.php
	protected function _viewAboutUs(){ 
		//echo ;
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/index.php');
	}
	

}
?>