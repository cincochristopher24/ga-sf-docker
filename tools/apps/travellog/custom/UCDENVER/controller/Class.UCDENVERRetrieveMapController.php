<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	UCDENVERRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for UCDENVER	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/UCDENVER/views'));
		$file_factory->setPath('CSS', '/custom/UCDENVER/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/UCDENVER/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}