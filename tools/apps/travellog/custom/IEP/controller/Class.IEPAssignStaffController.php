<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');
	
	class IEPAssignStaffController extends COBRANDAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/IEP/views'));
			$this->mFactory->setPath('CSS', '/custom/IEP/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/IEP/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
