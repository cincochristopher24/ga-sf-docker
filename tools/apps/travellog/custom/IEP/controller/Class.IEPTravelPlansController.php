<?php
/*
	Filename:		Class.IEPTravelPlansController.php
	Author:			Jonas Tandinco
	Date Created:	Nov/27/2007
	Putpose:		controller implementation for IEP co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelPlansController.php');

class IEPTravelPlansController extends COBRANDTravelPlansController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/IEP/views'));
		$this->file_factory->setPath('CSS', '/custom/IEP/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/IEP/views/');   
	}
	
	public function performAction(){ 
		parent::performAction();
	}
}
?>