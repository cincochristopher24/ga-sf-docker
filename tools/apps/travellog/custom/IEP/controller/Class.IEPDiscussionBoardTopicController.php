<?php

	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionBoardTopicController.php');

	class IEPDiscussionBoardTopicController extends COBRANDDiscussionBoardTopicController {

		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/IEP/views/'));
			$this->file_factory->setPath('CSS', '/custom/IEP/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/IEP/views/');
		}
	}
?>