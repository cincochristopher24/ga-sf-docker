<?php
/*
 * Created on 11 13, 07
 * 
 * Author: J. Llano
 * Purpose: Photo module controller for PROWORLD 
 * 
 */
 
 require_once('travellog/custom/COBRAND/controller/Class.COBRANDResourceFileController.php');
 
 class IEPResourceFileController extends COBRANDResourceFileController {
 	function __construct(){
 		parent::__construct();
 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/IEP/views'));
		$this->file_factory->setPath('CSS', '/custom/IEP/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/IEP/views/'); 
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 }