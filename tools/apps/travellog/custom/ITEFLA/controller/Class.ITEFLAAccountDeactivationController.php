<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDAccountDeactivationController.php");
	
	/**
	 * The account deactivation controller of a cobrand.
	 */
	class ITEFLAAccountDeactivationController extends COBRANDAccountDeactivationController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ITEFLA/views"));
			$this->file_factory->setPath("CSS", "/custom/ITEFLA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ITEFLA/views/");
		}
		
	}
	
