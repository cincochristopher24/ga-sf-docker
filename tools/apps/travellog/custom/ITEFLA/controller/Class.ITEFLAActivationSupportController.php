<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDActivationSupportController.php");
	
	/**
	 * The activation support controller of a cobrand.
	 */
	class ITEFLAActivationSupportController extends COBRANDActivationSupportController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ITEFLA/views"));
			$this->file_factory->setPath("CSS", "/custom/ITEFLA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ITEFLA/views/");
		}
		
	}
	
