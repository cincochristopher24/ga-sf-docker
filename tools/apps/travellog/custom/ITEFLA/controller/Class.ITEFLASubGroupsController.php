<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class ITEFLASubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new ITEFLA subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ITEFLA/views"));
			$this->file_factory->setPath("CSS", "/custom/ITEFLA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ITEFLA/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}