<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostController.php');
	
 	class ITEFLAPostController extends COBRANDPostController {
 		
 		function __construct()
 		{
	 		parent::__construct();
	 		
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ITEFLA/views'));
			$file_factory->setPath('CSS', '/custom/ITEFLA/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ITEFLA/views/');
 		
			Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));	 		
 		}
 		
 	}