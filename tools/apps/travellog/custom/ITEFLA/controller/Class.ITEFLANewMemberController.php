<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewMemberController.php');
	
	class ITEFLANewMemberController extends COBRANDNewMemberController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ITEFLA/views'));
			$this->file_factory->setPath('CSS', '/custom/ITEFLA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ITEFLA/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}