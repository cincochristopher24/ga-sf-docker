<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class VFCSubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new VFC subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/VFC/views"));
			$this->file_factory->setPath("CSS", "/custom/VFC/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/VFC/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}