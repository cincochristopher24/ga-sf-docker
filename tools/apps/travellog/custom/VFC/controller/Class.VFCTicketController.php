<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTicketController.php');
	
 	class VFCTicketController extends COBRANDTicketController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/VFC/views'));
			$this->file_factory->setPath('CSS', '/custom/VFC/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VFC/views/');
 		}
 		
 		function performAction(){ 
			parent::performAction();
		}
 	}