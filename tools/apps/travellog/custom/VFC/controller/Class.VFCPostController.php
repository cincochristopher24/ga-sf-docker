<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostController.php');
	
 	class VFCPostController extends COBRANDPostController {
 		
 		function __construct()
 		{
	 		parent::__construct();
	 		
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/VFC/views'));
			$file_factory->setPath('CSS', '/custom/VFC/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VFC/views/');
 		
			Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));	 		
 		}
 		
 	}