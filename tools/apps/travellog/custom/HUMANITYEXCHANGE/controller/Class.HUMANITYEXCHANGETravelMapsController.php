<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');
	
	class HUMANITYEXCHANGETravelMapsController extends COBRANDTravelMapsController{
		
		function __construct(){
			parent::__construct();
			
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/HUMANITYEXCHANGE/views'));
			$this->file_factory->setPath('CSS', '/custom/HUMANITYEXCHANGE/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/HUMANITYEXCHANGE/views/');   
		}
		
		function performAction(){ 
			parent::performAction();
		}
	}