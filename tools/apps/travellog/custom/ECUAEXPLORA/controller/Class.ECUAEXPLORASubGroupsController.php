<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class ECUAEXPLORASubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new ECUAEXPLORA subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ECUAEXPLORA/views"));
			$this->file_factory->setPath("CSS", "/custom/ECUAEXPLORA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ECUAEXPLORA/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}