<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class FEASubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new FEA subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/FEA/views"));
			$this->file_factory->setPath("CSS", "/custom/FEA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/FEA/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}