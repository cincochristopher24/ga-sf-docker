<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSPLauncherController.php");
	
	/**
	 * The SP launcher controller of a cobrand.
	 */
	class FEASPLauncherController extends COBRANDSPLauncherController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/FEA/views"));
			$this->file_factory->setPath("CSS", "/custom/FEA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/FEA/views/");
		}
		
	}
	
