<?php
	/*
	 * Class.COBRANDMessagesController.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class ELIABROADNewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ELIABROAD/views'));
			$this->file_factory->setPath('CSS', '/custom/ELIABROAD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ELIABROAD/views/');
		}
	 	
	 }
?>