<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDMessagesController.php');
class ELIABROADMessagesController extends COBRANDMessageController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ELIABROAD/views'));
		$this->file_factory->setPath('CSS', '/custom/ELIABROAD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ELIABROAD/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>