<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class ELIABROADAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ELIABROAD/views'));
			$this->mFactory->setPath('CSS', '/custom/ELIABROAD/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/ELIABROAD/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>