<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTopicController.php');

	class DSETopicController extends COBRANDTopicController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/DSE/views/'));
			$file_factory->setPath('CSS', '/custom/DSE/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/DSE/views/');
		}
	}