<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');
	
	class KENNESAWAssignStaffController extends COBRANDAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/KENNESAW/views'));
			$this->mFactory->setPath('CSS', '/custom/KENNESAW/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/KENNESAW/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}