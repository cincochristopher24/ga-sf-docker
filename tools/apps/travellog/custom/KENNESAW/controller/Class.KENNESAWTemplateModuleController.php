<?php  
	/**
	 * @(#) Class.KENNESAWTemplateModuleController.php
	 * 
	 * Autogenerated by GoAbroad Code Generator 1.0
	 *
	 * @version 1.0 Jun 06, 2009 
	 */	
	
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTemplateModuleController.php');
	
	/**
	 * TemplateModule cobrand controller class for KENNESAW.
	 */
	class KENNESAWTemplateModuleController extends COBRANDTemplateModuleController {
		
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/KENNESAW/views'));
			$this->file_factory->setPath('CSS', '/custom/KENNESAW/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/KENNESAW/views/'); 
		}
		
	}