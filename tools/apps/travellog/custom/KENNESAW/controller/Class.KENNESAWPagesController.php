<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
	
	class KENNESAWPagesController extends COBRANDPagesController {
	
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/KENNESAW/views'));
			$this->file_factory->registerTemplate('PrivacyPolicy',array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->registerTemplate('TermsDefault', array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->registerTemplate('TermsAdvisor', array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/KENNESAW/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/KENNESAW/views/');
		}
	
		function performAction(){
			parent::performAction();
		}
	
	}