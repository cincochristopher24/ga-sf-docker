<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDVideoController.php');
	
 	class KENNESAWVideoController extends COBRANDVideoController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/KENNESAW/views'));
			$this->file_factory->setPath('CSS', '/custom/KENNESAW/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/KENNESAW/views/');
 		}
 		
 		function performAction(){ 
			parent::performAction();
		}
 	}