<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDMessagesController.php');
class YHAMessagesController extends COBRANDMessageController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/YHA/views'));
		$this->file_factory->setPath('CSS', '/custom/YHA/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/YHA/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>