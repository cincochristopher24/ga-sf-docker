<?php
$file_factory  = FileFactory::getInstance();
//$obj_criteria  = $file_factory->getClass('Criteria2');
//$obj_criteria1 = clone($obj_criteria);
//$obj_criteria->setOrderBy('travellogID DESC');
//$obj_criteria1->setOrderBy('arrival, tripID');
require_once('Class.HtmlHelpers.php');
if( $props['is_login'] && isset($props['is_owner']) ) 
	echo $objSubNavigation->show();
?>


<div id="intro_container">
	
</div>


<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
    	<div id="journals_summary" class="section">
    		<?php if( $props['is_login'] && isset($props['is_owner']) ):?>
	    		<h1 class="big_header">
					<span>
				    		<?
				    			if( $props['is_owner'] )
				    				echo $props['journal_page_owner'];
				    			else
				    				echo ( substr(strtolower($props['journal_page_owner']), -1) != 's' )? $props['journal_page_owner'] . "&rsquo;s" : $props['journal_page_owner'] . "'";
				    		?> Journals
				    </span>	
				</h1>
			<?php else:?>	
				<div class="head_left"><div class="head_right"></div></div>	
			<?php endif;?>
  			
			<div class="content">
				
  				<?php echo $obj_journal_lists_view->render();?>
  				
			
				<div class="pagination"> 
					
					<p>Showing <?php echo ($objPaging->getStartRow()+1)?> - <?php echo $objPaging->getEndRow()?> of <?php echo $objPaging->getRecordCount()?> Entries</p>
					<div class="pagination_pages">
						<?php $objPaging->getFirst()?>
						<?php $objPaging->getPrevious()?>
						<?php $objPaging->getLinks()?>
						<?php $objPaging->getNext()?>
						<?php $objPaging->getLast()?>
					</div>
				</div>								
			</div>		
		
			<div class="foot"></div>	
            <div class="clear"></div>
		
		</div>
	</div>
	<div id="narrow_column">
		<?php if( isset($props['action']) && strtolower($props['action']) == 'myjournals'):?>
			<div id="quick_tasks" class="section"> 
				<h2>
				<span>Quick Task</span>
				</h2>
				<!--div class="head_left"><div class="head_right"></div></div-->
				<div class="content">
              		<ul class="actions">
              			<?if( isset($props['is_owner']) && $props['is_owner'] ):?>
	              			<?if( $objPaging != NULL ):?>
	              				<?if( strtolower($props['action']) == 'myjournals'):?>
	                				<li <?if( strtolower($props['action']) != 'groupmembersjournal' ):?>id="sel"<?endif;?>><a class="button" href="/journal.php?action=MyJournals<?=$props['query_string']?>">Manage Journals</a></li>
	                			<?else:?>
	                				<li <?if( strtolower($props['action']) != 'groupmembersjournal' ):?>id="sel"<?endif;?>><a class="button" href="/journal.php?action=GroupJournals<?=$props['query_string']?>">Manage Journals</a></li>
	                			<?endif;?>
	                		<?endif;?> 
                			<?if( $props['is_login'] && isset($props['is_parent']) && $props['is_parent'] ):?>
                				<li <?if( strtolower($props['action']) == 'groupmembersjournal' ):?>id="sel"<?endif;?>><a class="button" href="/journal.php?action=GroupMembersJournal">Members Journals</a></li>
                			<?endif;?> 
		                	<li><a class="button" href="/travel.php?action=add">Create a Travel Journal</a></li> 
			            <?endif;?>

        		    </ul>
              		<div class="clear"></div>
			    </div>
				<div class="foot"></div>
		  	</div>
		<?php else:?>
			<div id="search" class="section">
				<h2>
				<span>Search</span>
				</h2>
	            <div class="content" >
					<form action="journal.php" method="get" onsubmit="return false;">
						<fieldset class="fieldset1">
	              	        <p> <span>Search by:</span>
			                <select name="countryID" id="countryID" class="countries">
			                	<?php foreach( $collectionCountry as $obj_country ):?>
			                		<option value="<?php echo $obj_country->getLocationID()?>" <?php if( $props['locationID'] == $obj_country->getLocationID() ): ?>selected="selected"<?php endif;?>><?php echo $obj_country->getName()?></option>
								<?endforeach;?>
								<option value="0" selected="selected">Choose a Country </option>
			                </select>
			                </p> 
							
							<ul class="actions">
			                <li><input id="btnFilter" name="btnFilter" onclick="search()" value="Show Journals" class="jour_submit" type="submit" /></li>
			                </ul>
	
		
		                </fieldset>
	                </form>
				              
		        </div>
				<div class="foot"></div>
			</div>
			
			<!-- Map for Journals -->
			
				<?php echo $obj_map_view->render()?>
				
			<!--     End         -->	
	
		<?php endif;?>	
	</div>
	<div class="clear"></div>
 </div>

























<?php
$file_factory  = FileFactory::getInstance();
$obj_criteria  = $file_factory->getClass('Criteria2');
$obj_criteria1 = clone($obj_criteria);
$obj_criteria->setOrderBy('travellogID DESC');
$obj_criteria1->setOrderBy('arrival, tripID');

if( $objSubNavigation->getDoNotShow() )echo $objSubNavigation->show();
?>

<div id="content_wrapper" class="layout_2">
        <div id="wide_column">
         
          <div id="journals_summary" class="section">
          <h2><span>Travel Journals</span></h2>
			
          <div class="content">
            <? if( $objPaging != NULL ): ?>
	      		<? 
	      			$objObjectIterator->rewind();
	      			while( $objObjectIterator->valid() ): ?>
			      							
						<?
					   		$entry_lists       = '';
					   		$ctr               = 0;
					   		$exceedFromTotal   = false;
					   		$arrTrips          = $objObjectIterator->current()->getTrips($obj_criteria1);
					   		$ctrTrips          = count($arrTrips);
					   		$latestLocation    = NULL;
					   		$latestEntry       = NULL;
					   		$latestDescription = NULL;
					   		$latestDate        = NULL;
					   		$latestCountry     = NULL;
					   		$latestPhoto       = NULL;
					   		$tempPhoto         = NULL;
					   		$arr_journals      = array();
					   		$collectionLogs    = $objObjectIterator->current()->getJournalLogs();
					   		$jID               = array();      
					   		if( $ctrTrips ){
						   		//foreach( $arrTrips as $objTrips ){
						   		for($i = ($ctrTrips-1); $i >= 0; $i--){	
						   			$arrJournalEntry = $arrTrips[$i]->getTravelLogs($obj_criteria);
						   			foreach( $arrJournalEntry as $objJournalEntry ){
						   				$arr_journals[$objJournalEntry->getTravelLogID()] = $objJournalEntry;
						   				if( $tempPhoto == NULL && $objJournalEntry->countPhotos() ){
					   						$tempPhoto = $objJournalEntry->getPrimaryPhoto(); 
					   					}
						   				if( $ctr < 2 ){
						   					$count_photos = ( $objJournalEntry->countPhotos() )? '&nbsp;|&nbsp;'.$objJournalEntry->countPhotos():'';
						   					$entry_lists .= '<div class="entry_list"><a href="/journal-entry.php?action=view&amp;travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'&nbsp;&nbsp;</a><p class="entry_date_photo">'.date('d F',strtotime($objJournalEntry->getTrip()->getArrival())).$count_photos;
						   					//$entry_lists .= '<div class="entry_list"><a href="/journal-entry.php?action=view&travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'&nbsp;&nbsp;</a><p class="entry_date_photo">'.date('d F',strtotime($objJournalEntry->getTrip()->getArrival())).' &nbsp;|&nbsp;'.$objJournalEntry->countPhotos();
						   					if( $objJournalEntry->countPhotos() > 1 ){
						   						if( array_key_exists($objJournalEntry->getTravelLogID(), $collectionLogs) ){
						   							$entry_lists .= ' Photos<span class="span_new"><img src="/images/icons_new.gif" alt="new" /></span>';
						   							$jID[]        = $objJournalEntry->getTravelLogID();
						   						}else
						   							$entry_lists .= ' Photos';
						   					}elseif( $objJournalEntry->countPhotos() ){
						   						if( array_key_exists($objJournalEntry->getTravelLogID(), $collectionLogs) ){
						   							$entry_lists .= ' Photo<span class="span_new"><img src="/images/icons_new.gif" alt="new" /></span>';
						   							$jID[]        = $objJournalEntry->getTravelLogID();
						   						}else
						   							$entry_lists .= ' Photo';
						   					}
						   					$entry_lists .=	' </p></div>';
						   				}else
						   						$exceedFromTotal = true;
						   				$ctr++;
						   			}
						   		}
						   		$objTrips = $arrTrips[$ctrTrips-1];
						   		if( is_object($objTrips->getLocation()) ){
									if( strlen(trim($objTrips->getLocation()->getName())) ){
										$latestLocation = $objTrips->getLocation()->getName();
										$latestCountry  = $objTrips->getLocation()->getCountry()->getName();
										if( $latestLocation != $latestCountry){
											$latestCountry = ', '. $latestCountry;
										}else{
											$latestCountry = NULL;
										}
									}
								}
						   		$arrJournalEntry   = $objTrips->getTravelLogs($obj_criteria);
						   		$objJournalEntry   = $arrJournalEntry[0];
				   				$latestEntry       = '<a href="/journal-entry.php?action=view&amp;travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'</a>';
				   				$latestDescription = $objJournalEntry->getDescription();
				   				$latestPhoto       = $objJournalEntry->getPrimaryPhoto(); 
				   				$latestDate        = $d->set($objJournalEntry->getTrip()->getArrival())->friendlyFormat();

					   			if( $latestPhoto->getPhotoID() == 0 && $tempPhoto != NULL ){
					   				$latestPhoto = $tempPhoto;
					   			}
					   		}else
					   			$entry_lists .= 'No Entries Available';
						?>
						
						<div class="container">
							<div class="journal_box_wrap">
							<div class="header">
								<h2><a href="/journal-entry.php?action=view&amp;travelID=<?=$objObjectIterator->current()->getTravelID()?>">
									<?echo $objObjectIterator->current()->getTitle();?>
								</a></h2>
								
								
								
								<div class="author_info">
								
								
									<?
									//===============================================================================//
									if( $objObjectIterator->current()->getTraveler()->isAdvisor() ){
										$groupID            = AdminGroup::getAdvisorGroupID( $objObjectIterator->current()->getTravelerID() );
										$objAdminGroup      = new AdminGroup($groupID);
										$trav               = NULL;
										$username           = $objAdminGroup->getName();  
									}else{
										$objTravelerProfile = $objObjectIterator->current()->getTraveler()->getTravelerProfile();
										$trav 				= $objObjectIterator->current()->getTraveler()->getTravelerType();
										$username           = $objTravelerProfile->getUserName();
									}	
									
								
								?>
								
							
							
								<? if( $objObjectIterator->current()->getTraveler()->isAdvisor() ): ?>
								<div class="div_username2">	
									<p><img src="<?=$objAdminGroup->getGroupPhoto()->getPhotoLink()?>" width="37px" height="37px" /></p>
								  </div>
								  <div class="div_username1">
									<p class="para_one"><a href="/group.php?gID=<?=$groupID?>"><?=$username?></a></p>
									<p class="para_two"><?=$trav?></p>
								  </div>
								  
								<?else:
									$_tmpUsernamePossessive = (strlen($username)-1 == strripos($username,'s'))?$username."'":$username."'s";
								 ?>										
								
								 <div class="div_username2">	  
								    <p><a href="/<?=$username?>" title="Read more of <?=$_tmpUsernamePossessive?> travel experiences and adventure!"><img src="<?=$objObjectIterator->current()->getTraveler()->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink()?>" width="37px" height="37px" alt="Traveler: <?=$username?>" /></a></p>
	  							  </div>
								
								  <div class="div_username1">
									<p class="para_one"><a href="/<?=$username?>" title="Read more of <?=$_tmpUsernamePossessive?> travel experiences and adventure!"><?=$username?></a></p>
								    <p class="para_two"><?=$trav?></p> 
								  </div>
								 
								<?endif;?>
								</div>
					
							</div>
						
							
						<div class="content">	
						<?if (false): // Do not show for now. The extra markup here is messing up the html code?>
						<!--<div class="entry_box">
						<div class="inside_entry">
			
							   <?= $entry_lists ?>
					
						   <? if( $exceedFromTotal ): ?>
						   <? endif; ?> 
					       </div>
					       	<div class="clear"></div>	

  				    		  <div class="inside_entry">
  				    	    <?
					    	
					    	$ctr1 = 0;
					  				    	
					    	foreach( $collectionLogs as $objLogs ){
					    
					    		if( array_key_exists( $objLogs->getTravellogID(), $arr_journals) && !in_array($objLogs->getTravellogID(), $jID) ){
					    			$obj_jounral_entry = $arr_journals[$objLogs->getTravellogID()];
					    			echo '<div class="inside_date_photo"><p class="threedots"><img src="images/threedots.gif" /></p></div>';
					    			echo '<div class="entry_list"><a href="/journal-entry.php?action=view&amp;travellogID='.$obj_jounral_entry->getTravelLogID().'">'.$obj_jounral_entry->getTitle().'</a>';
					    			echo '<p class="entry_date_photo">'.date('d F',strtotime($obj_jounral_entry->getTrip()->getArrival())). '&nbsp;|&nbsp;' . $obj_jounral_entry->countPhotos().' Photo';
					    			
				    				if( $obj_jounral_entry->countPhotos() > 1 )
				    					echo 's<span class="span_new"><img src="/images/icons_new.gif" /></span>';
				    				else
				    			 		echo '<span class="span_new"><img src="/images/icons_new.gif" /></span>';
				    			 	echo '</p></div>';
				    			 	$ctr1++;
					    		}
					    		
					    	}
					    ?></div>
					      <div class="inside_viewall">
					      	<?if( $exceedFromTotal ):
				      			$more_entry = NULL;
				      			if( ($ctr+$ctr1) > (2+$ctr1) ) $more_entry = $ctr - ($ctr1+2);
					      	?>
					  			<?if( $more_entry ):?>
					  				<a class="more" href="/journal-entry.php?action=view&amp;travelID=<?=$objObjectIterator->current()->getTravelID()?>" title="Read <?=$_tmpUsernamePossessive?> <?=$more_entry?> other journals."><?=$more_entry?> More...</a>
					  			<?endif;?> 
					  		<?endif;?>     
					      </div>	
					  		 
						</div>-->
						<?endif;?>
					
					
				
							<?if( $latestPhoto->getPhotoID() ):?> 
							<div class="photo">
								 
									<a href="/journal-entry.php?action=view&amp;travelID=<?=$objObjectIterator->current()->getTravelID()?>">
										<img src="<?=$latestPhoto->getPhotoLink()?>" alt="journal photo" />
									</a>
							</div>
							<? endif; ?>
							
								<?
									//===============================================================================//
									if( $objObjectIterator->current()->getTraveler()->isAdvisor() ){
										$groupID            = AdminGroup::getAdvisorGroupID( $objObjectIterator->current()->getTravelerID() );
										$objAdminGroup      = new AdminGroup($groupID);
										$username           = $objAdminGroup->getName();  
									}else{
										$objTravelerProfile = $objObjectIterator->current()->getTraveler()->getTravelerProfile();
										$username           = $objTravelerProfile->getUserName();
									}	
									if( strlen(trim($latestEntry)) ) echo '<span class="entry_title">'.$latestEntry.'</span>';
								?>
								
								<? if( strlen(trim($latestLocation)) ): ?>
								
									<span class="entry_detail"><?= $latestLocation.$latestCountry .' | ' .$latestDate .' | '?>
									
								<? endif; ?>
								
								<?='Views: '.$objObjectIterator->current()->getViews().'<br/></span>';?>
								
							

								<p>
									<?
										if( strlen(trim($latestDescription)) == 0  ) $latestDescription = $objObjectIterator->current()->getDescription();
										echo nl2br(substr($latestDescription, 0, 250));
										if( strlen(trim($latestDescription)) > 250 ) echo '...'; 	
									?>
								</p>
							
							
							</div>
							
							<div class="foot"></div>
						
						</div>
						
						<div class="entry_box">

								<div id="up-arrow" class="journal_button_top">PREVIOUS</div>

								<div id="mycarousel" class="inside_entry">


								        <ul class="carousel-list">
								            <li id="mycarousel-item-1">
												<a href="#">
												<strong>
												1
												</strong>
												<span class="entry_date_photo">
												30 January | 5 Photos NEW
												</span>
												</a>
											</li>
										</ul>	
								</div>
								<div id="down-arrow" class="journal_button_bottom">NEXT</div>			

					    </div>

						<div class="clear"></div>
						
						
						</div>
						
			      
			       
		        <? 
		        	$objObjectIterator->next();
		        	endwhile; ?>

						<div class="pagination">
											<p>Showing <?php echo ($objPaging->getStartRow()+1)?> - <?php echo $objPaging->getEndRow()?> of <?php echo $objPaging->getRecordCount()?> Entries</p>
											<div class="pagination_pages">
												<?php $objPaging->getFirst()?>
												<?php $objPaging->getPrevious()?>
												<?php $objPaging->getLinks()?>
												<?php $objPaging->getNext()?>
												<?php $objPaging->getLast()?>
											</div>
						</div>            
	
	
	            <!--<? if ($objPaging->getTotalPages() > 1): ?>
	            	<div class="pagination"> <span class="disabled"><?$objPaging->getFirst()?></span>&nbsp; | <span class="disabled"><?$objPaging->getPrevious()?></span>&nbsp; | <?$objPaging->getLinks()?> | <span><?$objPaging->getNext()?></span>&nbsp; | <span><?$objPaging->getLast()?></span> </div>
	            <? endif; ?>
	        <?else:?>
	        	<p class="help_text">
	        		<?if( $props['locationID'] ):?>
	        			No journals for this location.
	        		<?endif;?>
	        	</p>
			<? endif; ?>-->
			
          </div>
		<div class="foot"></div>
		<div class="clear"></div>
		</div>
		

        </div>
   
      <!--? if( !$formvars['mytravels'] ): ?-->   
	      
	<!--? endif; ?-->
	
	<div id="narrow_column">
		
		<? if( $props['is_login'] ): ?>
		<div id="option" class="section">
			<div class="head_left"><div class="head_right"></div></div>
			<div class="content">
					<ul class="actions"><li><a href="/travel.php?action=add" class="button">Create a Travel Journal</a></li></ul>
			</div>
			<div class="foot"></div>
		</div>
		<? endif; ?>	
		
		 <div id="search" class="section">
			<h2><span>Search</span></h2>
			<div class="content">
            <!--? if( !$formvars['mytravels'] ): ?-->
	            <ul class="actions">
	              <? if ($props['locationID'] === 0):?>
	              	<li class="top_1" id="sel"><a class="button" href=""><strong>Newest Journals</strong></a></li>
	              <? else: ?>
	              	<li class="top_1"><a class="button" href="/journal.php">Newest Journals</a></li>
	              <? endif; ?>
	            </ul>
				<br/>
		          <form action="journal.php" method="get" onsubmit="return false;">
			               
			                
			      <? if ($props['locationID'] === 0):?>
			                
							<fieldset class="fieldset1">
	              	        
							<p><span>Search by:</span>
			                <select name="countryID" id="countryID" class="countries">
			                	
								<? 
									if( count($collectionCountry) ):
										foreach( $collectionCountry as $objCountry ): ?>
										<?php if(!is_null($objCountry)): ?>
					                		<option value="<?=$objCountry->getLocationID()?>" <?if( $objCountry->getLocationID()==$props['locationID'] ) echo 'selected="selected"';?> ><?=$objCountry->getName()?></option>
										<?php endif; ?>		
			                	<? 		endforeach; 
			                		endif;?>
			                	
			                	<option value="0" <?if( 0==$props['locationID'] ) echo 'selected="selected"';?>>Choose a Country </option>	
			                </select>
							</p>
								
			                <ul class="actions">
							<li>
			                <input id="btnFilter" name="btnFilter" onclick="search()" value="Show Journals" class="jour_submit" type="submit" />
			                </li>
							</ul>
							
			                 </fieldset>
			               
	              <? else: ?> 
	                       <fieldset class="fieldset2">
		
	                       <p><span>Search by:</span>
			                <select name="countryID" id="countryID" class="countries">
			                	<option value="0" <?if( 0==$props['locationID'] ) echo 'selected="selected"';?>>Choose a Country </option>	
								<?  if( count($collectionCountry) ):
										foreach( $collectionCountry as $objCountry ): ?>
										<?php if($objCountry instanceOf Country): ?>
			                		<option value="<?=$objCountry->getLocationID()?>" <?if( $objCountry->getLocationID()==$props['locationID'] ) echo 'selected="selected"';?> ><?=$objCountry->getName()?></option>
										<?php endif; ?>
			                	<? 		endforeach; 
			                		endif;?>
			                </select>
							</p>
							
			                <ul class="actions">
							<li>
			                <input id="btnFilter" name="btnFilter" onclick="search()" value="Show Journals" class="jour_submit" type="submit" />
			                </li>
							</ul>
							
			                 </fieldset>
	              	   
	              <? endif; ?>
			             
			                
		          </form>

			</div>
			<div class="foot"></div>
 			<!--? endif; ?-->
            <div class="clear"></div>

          </div>
		
	</div>
	
	<div class="clear"></div>
    </div>



