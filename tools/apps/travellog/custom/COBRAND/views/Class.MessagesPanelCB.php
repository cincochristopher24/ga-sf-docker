<?php
/*
 * Created on 07 27, 07
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 require_once('travellog/custom/COBRAND/views/Class.PokesViewCB.php');
 require_once('travellog/views/Class.MessagesPanel.php');
 require_once('travellog/model/Class.AdminGroup.php');
 
 class MessagesPanelCB extends MessagesPanel{
 	protected $groupID;			// group id of present cobranded site
 	protected $groupMembers = array();	// an array of travelerIDs of group members	
 	
 	public function setGroupID($groupID){
 		$this->groupID = $groupID;
 		$group = new AdminGroup($groupID);
 		$members = $group->getMembers();
 		foreach ($members as $member){
 			$this->groupMembers[] = $member->getTravelerID();	
 		} 		  
 	}
 	
 	
 	public function render($messType = 1){ 		
 		if (is_a($this->context,'Traveler')) { 		
	 		if (0 != $this->context->getTravelerID()){		 		
		 		switch ($messType  ){
		 			case MessagesPanel::$ALL :
		 				$this->renderAll();
		 				break;
		 			case MessagesPanel::$PERSONAL_MESSAGE :
		 				$this->renderPersonalMessages();
		 				break;
		 			case MessagesPanel::$BULLETIN:
		 				$this->renderBulletin();
		 				break;
		 			case MessagesPanel::$INQUIRY:
		 				$this->renderInquiries();
		 				break;
					case MessagesPanel::$GREET:
						$this->renderGreetings();
						break;
					case MessagesPanel::$SURVEY:
		 				$this->renderSurveys();
		 				break;
		 			default: 
		 				$this->renderAll();
		 				break;				 			
		 		}
	 		}	
 		} elseif (is_a($this->context,'Group')) {
 			$this->renderBulletin();
 		}	
 	}


 	/** display all messages side by side sorted by date
 	 * 	 Bulletin: show from current date, Personal: show unread, Inquiries: showall 
 	 */
 	protected function renderAll(){
		/********* START: LAST EDITED BY: Cheryl Ivy Q. Go *************/
		$messages = array();
 		$ms = new MessageSpace();

 		// get bulletins (Filter:NEW)
 		$tempMessages = $ms->getBulletins($this->context->getSendableID());

		foreach($tempMessages as $indMessage){
			if (!$indMessage->isRead())
				$messages[] = $indMessage;
		}				
		/********* END: LAST EDITED BY: Cheryl Ivy Q. Go *************/

 		// get personal messages (unread)
 		$messages = array_merge($messages,$this->context->getInbox()->getNewMessages());

 		// get inquiries
 		$messages = array_merge($messages,$this->context->getInquiryBox()->getMessages(null,'unread'));

		/******************* START: GET GREETINGS, SALUTATIONS AND COMMENTS **********************/
		// profile
		if (0 < count($this->context->getTravelerProfile()->getComments(null, true))){
			$comments = $this->context->getTravelerProfile()->getComments(null, true);
	 	 	$comments = $this->filterComments($comments);
			if (0 < count($messages))
				$messages = array_merge($messages, $comments);
			else
				$messages = $comments;
		}

		// journal-entry
 	 	$tlogs = Travellog::getTravellogs($this->context);
		if (null != $tlogs) {
	 	 	foreach ($tlogs as $tlog){
	 	 		if ($tlog->getComments(null, true) != null){
	 	 			if (0 < count($tlog-> getComments(null, true))){
	 	 				$comments = $tlog-> getComments(null, true);
	 	 				$comments = $this->filterComments($comments);
	 	 				if (0 < count($messages))
	 	 					$messages = array_merge($messages, $comments);
	 	 				else
	 	 					$messages = $comments;
	 	 			}	 	 			
	 	 		}
	 	 	}
 	 	}

		// get photo comments
		$photos = $this-> context-> getTravelerProfile()-> getPhotos();
 	 	if (null != $photos) {
	 	 	foreach ($photos as $photo){
	 	 		if (0 < count($photo->getComments(null, true))){
	 	 			$comments = $photo->getComments(null, true);
	 	 			$comments = $this->filterComments($comments);
	 	 			if (0 < count($messages))
	 	 				$messages = array_merge($messages, $comments);
	 	 			else
	 	 				$messages = $comments;
	 	 		}
	 	 	}
 	 	}
		/****************** END: GET GREETINGS, SALUTATIONS AND COMMENTS **********************/
		
		
		/******** START: Get Alerts ********/	 	
		$alerts = $this->getAlerts();
		if(!is_null($alerts)){
			$messages = array_merge($messages, $alerts);
		}
		/******** END: Get Alerts ********/
		
		/******** START: Get Surveys ********/
		$surveys = $this->getSurveys();
		$messages = array_merge($messages, $surveys);
		/******** END: Get Surveys ********/

		if (0 < count($messages)){
	 		// sort the messages by date
	 		usort($messages,array('Message','orderByDate'));
	 		// reverse it for descending order
	 		$messages = array_reverse($messages);
		}
	
 		if (count($messages) > $this->maxMessages)
 			$messages = array_slice($messages,0,$this->maxMessages);
 		
		$this->totalNumberOfNewMessages = count($messages);

 		$template = new Template();
 		$template->set('messages',$messages);
 		$template->set('messType',MessagesPanel::$ALL);
 		$template->set('groupID',$this->groupID); 
 		$template->set('members',$this->groupMembers);		
 		echo $template->fetch('travellog/custom/COBRAND/views/tpl.MessagesPanel.php');
 				
 	}

 	/**
 	 * display all Bulletin message type
 	 */
 	protected function renderBulletin(){

 		$template = new Template();
 		$template->set('messages',	$this->msgs);
 		$template->set('messType',	MessagesPanel::$BULLETIN);
 		$template->set('groupID',	$this->groupID); 	
 		$template->set('members',	$this->groupMembers);	
 		echo $template->fetch('travellog/custom/COBRAND/views/tpl.MessagesPanel.php');
 	}
 	
 	/**
 	 * display Personal messages
 	 */
 	protected function renderPersonalMessages() {
 		/*/$ms = new MessageSpace();
 		 		
 		// get personal messages (unread)
 		$messages = $this->context->getInbox()->getMessageList();
 		
 		if (count($messages) > $this->maxMessages)
 			$messages = array_slice($messages, 0, $this->maxMessages); 		
 		*/
		 		
 		$template = new Template();
 		$template->set('messages',	$this->msgs);
 		$template->set('messType',	MessagesPanel::$PERSONAL_MESSAGE);
 		$template->set('groupID',	$this->groupID); 	
 		$template->set('members',	$this->groupMembers);	
 		echo $template->fetch('travellog/custom/COBRAND/views/tpl.MessagesPanel.php');
 		
 	}
 	
 	protected function renderGreetings(){
		$template = new Template();
		$rl = new RowsLimit($this->maxMessages);

		// profile
		$messages = $this->context->getTravelerProfile()->getComments(null, true);

		// journal-entry
 	 	$tlogs = Travellog::getTravellogs($this->context);
		if (null != $tlogs) {
	 	 	foreach ($tlogs as $tlog){
	 	 		if ($tlog->getComments($rl, true) != null){
	 	 			if (0 < count($tlog-> getComments($rl, true))){
	 	 				if (0 < count($messages))	 	 				
	 	 					$messages = array_merge($messages, $tlog-> getComments($rl, true));
	 	 				else
	 	 					$messages = $tlog-> getComments($rl, true);
	 	 			}
	 	 		}
	 	 	}
 	 	}

		// get photo comments
		$photos = $this-> context-> getTravelerProfile()-> getPhotos();
 	 	if (null != $photos) {
	 	 	foreach ($photos as $photo){
	 	 		if (0 < count($photo->getComments($rl, true))){
	 	 			if (0 < count($messages))
	 	 				$messages = array_merge($messages, $photo->getComments($rl, true));
	 	 			else
			 		 	$messages = $photo->getComments($rl, true);
	 	 		}
	 	 	}
 	 	}

		if (0 < count($messages)){
			// sort the messages by date
	 		usort($messages, array('Comment','orderByDate'));
	 		// reverse it for descending order
	 		$messages = array_reverse($messages);
		}

		$template = new Template();
 	 	$template->set('messages', $messages);	 		
 		$template->set('messType', MessagesPanel::$GREET);
		$template->set('groupID',$this->groupID); 
 		$template->set('members',$this->groupMembers);
 		echo $template->fetch('travellog/custom/COBRAND/views/tpl.MessagesPanel.php');
	} 	
	
	// get Bulletins for the current group or members of this group
	protected function getBulletins(){
		$ms = new MessageSpace();
 		// get bulletins
 		$messages = $ms->getBulletins($this->context->getSendableID()); 		
 		
 		$fMessages = array();
 		// filter now the messages array
 		for($i = 0; $i < count($messages);$i++){
 			$mSrc = $messages[$i]->getSource();
 			if ($mSrc instanceof Traveler){
 				if(in_array($mSrc->getTravelerID(),$this->groupMembers)){
 					$fMessages[] = $messages[$i];
 				}
 			} 
 			if ($mSrc instanceof Group){
 				if ($this->groupID == $mSrc->getGroupID()){
 					$fMessages[] = $messages[$i];
 				}
 			}			
 		}
 		
 		if (count($fMessages) > $this->maxMessages)
 			$fMessages = array_slice($fMessages,0,$this->maxMessages); 	
 		 		 		
 		return $fMessages;
	}
	private function getAlerts(){
 		require_once('travellog/model/Class.SessionManager.php');
		$obj_session = SessionManager::getInstance();
		$messageSpace = new MessageSpace();
		return $messageSpace->getAlerts($this->context->getSendableID());
 	}
	private function renderAlerts(){
		$alerts = $this->getAlerts();

		$template = new Template();
		$template->set('messages', $alerts);	 		
 		$template->set('messType', MessagesPanel::$ALERT);
		$template->set('groupID',$this->groupID); 
 		$template->set('members',$this->groupMembers);
		echo $template->fetch('travellog/custom/COBRAND/views/tpl.MessagesPanel.php');
	}
	protected function getSurveys(){
		//get the survey forms of this traveler that needs his participation.
		require_once('travellog/model/formeditor/Class.SurveyForm.php');
		$tempSurveys = SurveyForm::getUnparticipatedSurveyFormsByTraveler($this->context->getTravelerID());
		$surveys = array(); 			
		foreach($tempSurveys as $iSurvey){
			if($iSurvey['surveyForm']->getGroupID() == $this->groupID){
				$surveys[] = $iSurvey['surveyForm'];
			}
		}
		return $surveys;
	}
	protected function renderSurveys(){
		$surveys = $this->getSurveys();

		$template = new Template();
 		$template->set('messages', $surveys);
 		$template->set('messType', MessagesPanel::$SURVEY);
		$template->set('groupID',$this->groupID); 
 		$template->set('members',$this->groupMembers);
		echo $template->fetch('travellog/custom/COBRAND/views/tpl.MessagesPanel.php');
	}
 }
 
?>