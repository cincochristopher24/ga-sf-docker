<?php
/*
 * Created on 09 22, 2006
 * @author Kerwin Gordo
 * Purpose: subtemplate for groups of a traveler (used in passport for COBRAND sites)
 * Difference with GA.net counterpart: control for joining/creating a club is gone, 
 * 										parent group will not be displayed
 */
?>

<?
	$accountType = '';								// GA.net account types 'REGULAR','UPGRADED','ADVISOR'
	$hasAdvisorGroup = false;												
	$gID = $GLOBALS['CONFIG']->getGroupID();		// groupID of cobrand site
	
	if (isset($groups) && $groups != null){
		for($i = 0; $i < count($groups);$i++ ){
			if ($groups[$i] != null  && $groups[$i]->getGroupID() == $gID ){
				unset($groups[$i]);					// remove the parent group whos id is the same as cobrands group id
				break;	
			}
		}		
	}	
?>

<div class="section" id="groups">
	<h2>
	  <span>Groups <?php if ($showGroupCount) echo '('.$groupCount.')'; ?></span>
	  <? if (5 < $groupCount): ?>
	    <span class="header_actions">
        <a  href="/group.php?travelerID=<?=$traveler->getTravelerID()?>">View All</a>
      </span>
    <? endif; ?>
	</h2>
	
	<div class="content">
		
		<ul id="groups_container" class="groups">
			
			<? if ($groups)   : ?>
				<? foreach($groups as $group) : ?>
					<li>
						
						<a class="thumb" href="/group.php?gID=<?=$group->getGroupID()?>" title="Visit the travel community <?=$group->getName() ?>">
								<img  class="pic" src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail')?>" height="65" width="65" alt="Travel community <?=$group->getName() ?>" />
						</a>
						
						<div class="details">
							<strong><a class="groupname" href="/group.php?gID=<?=$group->getGroupID()?>">  <?= $group->getName()?> </a></strong>
							
							<?/* if ($group->getAdministrator()->getTravelerID() == $traveler->getTravelerID()):?>
								<span class="meta"><strong>(&nbsp;Admin&nbsp;)</strong></span>	
							<? endif;*/?>
							
						</div>
						
						<div class="clear"></div>	
					</li>
				<? endforeach; ?>
	    <? else : ?>

	    	<p class="help_text">
				  <span><?=isset($helpTextprofile)?$traveler->getUsername()." ". $helpText:$helpText?></span>	
			  </p>    	
	    <? endif; ?>
	  </ul>
  </div>
  <div class="foot"></div>
</div>
