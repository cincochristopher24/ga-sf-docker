<? if ( $paging->getTotalPages() == 0): ?>	
  
  <? if (isset($search) && TRUE == $search): ?>
		<div class="containerbox">Your search has yielded no results.</div>
  <? elseif (TRUE == $mygroups): ?>
		<p class="help_text">
   		<span>You have not joined nor created groups/clubs yet</span>. <br /> Check out the <a href="/group.php">groups/clubs on GoAbroad Network</a> or <a href="/group.php?mode=add">create your own club</a>!.
    </p>
	<? endif; ?>	

<? endif; ?>

  <ul>
    <? foreach($group_list as $eachgroup): ?>
			<? $grpCategoryList = $eachgroup->getGroupCategory(true);
				 $type = "";
				 foreach($grpCategoryList as $key => $value)
					 $type .= $value.", ";
					  
				 $type = preg_replace('/, $/','',$type);		
			?>
			
			<li>	
				<a class="group_thumb" href="<?= $eachgroup->getFriendlyURL()?>">
				  <img src="<?= $eachgroup->getGroupPhoto()->getPhotoLink('standard'); ?>" alt="<?= $eachgroup->getName(); ?>" >
				</a>					
				
				<div class="groups_info">
					<strong>
					  <a href="<?= $eachgroup->getFriendlyURL(); ?>">
					    <?= $eachgroup->getName(); ?>
					  </a>
					</strong>
					
					<p>
						<?= StringFormattingHelper::wordTrim($eachgroup->getDescription(), 40)?>
						  <? if (40 < count(explode(' ', strip_tags($eachgroup->getDescription()) ) ) ) : ?>
						    <?='&#8230;' ?>
						  <? endif ; ?> 
						  <a href="<?= $eachgroup->getFriendlyURL() ?>" title="Read more about the <?= $eachgroup->getName() ?> travel community">
						    Read More
						  </a>
					</p>
					
					<? if (count($grpCategoryList)) : ?>
						<p>
						  <span class="gray">
						    <strong>Type :</strong>
						    <?= $type ?>
						  </span>
						</p>
				  <? endif; ?>			 															
					
					<? if (trim($eachgroup->getGroupMembershipLink('grouplist')) !== ''):?>
   					<p class="action"><?=$eachgroup->getGroupMembershipLink('grouplist')?></p>
   				<? endif; ?>
				</div>
  
			</li>	     
    <? endforeach;?>
	</ul>
	
  <? $paging->showPagination(array('label'=>' Groups')); ?>