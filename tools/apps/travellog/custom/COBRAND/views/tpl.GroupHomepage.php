<?php	
	
	// Added custom title & meta description - icasimpan Apr 25, 2007
	if ($group->getDiscriminator() == 1)
		$titleAndDesciptionPhrase = $group->getName() . ' Club';
	else if ($group->getDiscriminator() == 2)
		$titleAndDesciptionPhrase = $group->getName() . ' Group';
	
	Template::setMainTemplateVar('title', $titleAndDesciptionPhrase . ' - Online Community for Travelers');
	Template::setMainTemplateVar('metaDescription', 'Connect with the travelers on GoAbroad Network by joining the ' . $titleAndDesciptionPhrase); 

	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
	Template::setMainTemplateVar('layoutID', 'admin_group');
	if (isset($group)){
		Template::setMainTemplateVar('page_location', 'Home');
		Template::setMainTemplateVar('group', $group);
	}
	else
		Template::setMainTemplateVar('page_location', 'Groups');
	
	if (isset($advisor)){
		Template::setMainTemplateVar('advisor', $advisor);
	}	
		
	Template::includeDependentJs("/js/tools/tools.js");

// Don't know if these js files are used. Uncomment this
// if it is used.		
//	$code = <<<BOF
//<script type="text/javascript">
//	jQuery(document).ready(function(){
//		var manager = new groupMessageManager();
//		var membersManager = new groupMembersBoxManager();
//	});
// </script>		 
// BOF;

// Template::includeDependentJs("/js/jquery-1.1.4.pack.js");
// Template::includeDependentJs("/js/moo1.ajax.js");
// Template::includeDependentJs("/js/groupMessageManager.js",array("bottom"=>true));
// Template::includeDependentJs("/js/groupMembersBoxManager.js",array("bottom"=>true));
//Template::includeDependent($map->getJsToInclude());
//Template::includeDependent($code);

//if (null != $viewAlbums ) :
	


	// Template::includeDependentJs('/min/f=js/importantlinks.js',array("bottom"=>true));
	// Template::includeDependentJs('/min/f=js/utils/Class.Request.js',array("bottom"=>true));
	//Template::includeDependentCss('/css/importantlinks.css',array("bottom"=>true)); // not found
	Template::includeDependentCss("/css/cobrand_customhome.css");
	Template::includeDependentCss("/css/vsgStandardized.css");
	Template::includeDependentCss("/css/modalBox.css");
	Template::includeDependentCss("/css/Thickbox.css");
	Template::includeDependentJs('/min/g=GroupMainJs');
	if($viewImportantLinks){
	$iLink =<<<BOF
		<script type = "text/javascript">
			//<!--
			window.onload = function(){
				ImportantLinks.initialize();
				ImportantLinks.setGroupId($grpID);
				ImportantLinks.createBox();
			}
			//-->
		</script>
BOF;

	Template::includeDependent($iLink);
	}
?>	
<script type="text/javascript">
		jQuery.noConflict();
</script>

<? if ($isLogged): ?>
  <?= $mainGroupInfo_view->render();?>	
<? endif; ?>

<? $subNavigation->show() ?>
<div id="content_wrapper" class="yui-skin-sam">
<?php if($isParent && $isAdminLogged): echo $viewGroupSteps->render(); endif; ?>
</div>	

<?/*	if( 2031 == $grpID && !$isLogged ): ?>

	<div id="top_wrap" class="cobrand_feat">
		<div id="intro">
			<!-- start rotating photo -->
			<img src="/images/istudent_homestatic_img.jpg" style="display: block; clear: both;" />			
		</div>
		<div class="cobrand_welcome">
			<div class="wide">
				<h1>Welcome to the International Student Online Community!</h1>
				<p> 
					Join our international student community -- signing up is quick, easy, and gives you a state-of-the-art portal where you can upload your travel blogs, photos, videos and more.
				</p>
			</div>
		
			<div class="narrow" id="signup_ON">
				<a href="/register.php" title="Sign-Up Fast and Easy!">Sign-Up</a>
				<strong> Absolutely Free!</strong>
				<p>Sign Up fast and easy.</p>				
			</div>	
								
		</div>		

	</div>
<?	endif; */?>

<!-- Rotating Photo -->
<?//	if( (241 == $grpID || 633 == $grpID || 2031 == $grpID || 3 == $grpID || 245 == $grpID || 2961 == $grpID || 3301 == $grpID ) && (!$isLogged || $isAdminLogged || $isSuperStaffLogged) && !$group->isSubgroup() ): ?>
<?	if( (SiteContext::getInstance()->allowCustomPageHeader() ) && (!$isLogged || $isAdminLogged || $isSuperStaffLogged) && !$group->isSubgroup() ): ?>
	<?=$grpRotatingPhoto->render($isAdminLogged||$isSuperStaffLogged)?>
<?	endif; ?>

<div id="content_wrapper" class="yui-skin-sam">
	<div id="wide_column" class="layout2">
		<?php if(!$isParent) :?>
			<?= $grpInfoTemplate ?>
			
		<?php endif;?>
		
		<? // for the subgroups section ?>
		<? if ( $isAdminLogged && $viewSubGroups && isset($grpSubGroupsTemplate) ): ?>
		  <?= $grpSubGroupsTemplate;	?>
		<? endif; ?>
	  <? // end of subgroups ?>
		<!---photoalbum container -->
			<div id="photoalbum">
				<?= $viewAlbums ?>
			</div>
		<!---end photoalbum container -->
		
		<div id="recent_videos">
			<?= $grpRecentVideosTemplate; ?>
		</div>
		
		<? if (isset($grpJournalsTemplate))  { ?>
			<?= $grpJournalsTemplate->render();	?>
		<? } ?>
		
		<? if($viewGroupArticles): ?>
			<?= $grpArticlesTemplate->render();	?>						
		<? endif; ?>
				
		<!-- BEGIN MESSAGE CENTER -->
         <?php if ($showMessageCenter) : ?>
         <div id="message_center" class="section">
         	<h2>
         		<span>Message Center</span>
				<span class="header_actions">
					<?php if ($isPowerful) : ?>
					<ul id="message_actions">
						<?php if ($thereAreMoreMessages) : ?>
						<a href="/messages.php?gID=<?php echo $grpID; ?>">View All</a>
						|
						<?php endif; ?>
						<a href="/news.php?gID=<?php echo $grpID; ?>">Post News</a>
						|
						<?	if( $isParent ): ?>
							<a href="/messages.php?act=messageStaffOfManyGroups&gID=<?php echo $grpID; ?>">Compose Message</a>
						<?	else: ?>
							<a href="/messages.php?act=messageGroupStaff&gID=<?php echo $grpID; ?>">Compose Message</a>
						<?	endif; ?>
					</ul>
					<?php endif; ?>
				</span>
         	</h2>
			<div class="content">
		 		<?php echo $messageCenterView ?>
			</div>
         </div>
         <?php endif; ?>

		<!-- END MESSAGE CENTER -->

		<?= $grpDiscussionsTemplate; ?>		
	
	</div>
	<div id="narrow_column">

	  <?php if (!$isAdminLogged && $isMemberLogged) : ?>
			<div id="groupmessage" class="section">
				<div class="head_left">
					<div class="head_right"></div>
				</div>
				<div class="content">
					<ul class="actions">
			 			<li><a class="button" href="/messages.php?act=messageGroupStaff&gID=<?= $grpID ?>">Message Staff</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="foot"></div>
			</div>	
		<? endif; ?>
		
		
		<? if (Group::ADMIN == $grpDiscriminator && $viewResourceFiles && isset($grpFilesTemplate)) : ?>
			<?= $grpFilesTemplate; ?>  
		<? endif;	?>
		
		<? if ( TRUE == $viewImportantLinks) : ?>
		  <div class="section" id="implinks">
			  <h2>
			    <span> Important Links </span> 
							
				  <? if ($isAdminLogged) : ?>
					  <span class="header_actions">
						  <a href = "javascript:void(0)" onclick = "ImportantLinks.setAction('add'); ImportantLinks.showAddBox()">Add Link</a>
					  </span>
				  <? endif; ?>
				</h2>
						
				<div class="content">
					<div id = "statusPanel2" class = "widePanel" style = "display: none;">
						<p class = "loading_message">
							<span id = "imgLoading">
								<img alt = "Loading" src = "/images/loading.gif"/>
							</span>
							
							<em id = "statusCaption">Loading links please wait...</em>
						</p>
					</div>
					
					<?=$grpImportantLinksTemplate->render()?>
				</div>	
			</div>
		<? endif; ?>
		<? if (isset($groupTagCloud) && !is_null($groupTagCloud)): ?>
			<?= $groupTagCloud; ?>
		<? endif; ?>

		 	<? if (Group::ADMIN == $grpDiscriminator && !$group->isSubgroup()): ?> 			  
				
				<? if ( !$isAdminLogged && $viewSubGroups && isset($grpSubGroupsTemplate) ): ?>
				  <?= $grpSubGroupsTemplate;	?>
				<? endif; ?>
			  
			  <? if (isset($memberTemplate)): ?>
			    <?php echo $memberTemplate; ?>
			  <? endif; ?>
			  
			  <? if (isset($grpSurveyTemplate)): ?>
				  <?php echo $grpSurveyTemplate; ?>
				<? endif; ?>
					
			<? else : ?>
				<? if (isset($memberTemplate)): ?>
				  <?= $memberTemplate; ?>
				<? endif; ?>
				
				<? if (isset($grpSurveyTemplate)): ?>
				  <?= $grpSurveyTemplate; ?>
				<? endif; ?>
					
			<? endif; ?> 					
									
			<? if ( TRUE == $viewCalendar && isset($grpCalendarTemplate)) : ?>
				<?= $grpCalendarTemplate; ?>
			<? endif; ?> 
					
					
	</div>
</div>

<?php
	if( isset($_SESSION["COBRAND_HEADER_CUSTOMIZED"]) && $_SESSION["COBRAND_HEADER_CUSTOMIZED"] ){
		?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				CustomPopup.initPrompt("You have successfully customized the header photos of your homepage.");
				CustomPopup.createPopup();
			});
		</script>
		<?
		unset($_SESSION["COBRAND_HEADER_CUSTOMIZED"]);
	}
?>

