<?php
	/**
	 * Created on 09 25, 2006
	 * Purpose: template to list comments
	 * Last edited by: Cheryl Ivy Q. Go  27 September 2007
	 * For Greetings, Salutations and Comments
	 */

	$d = new GaDateTime();

	$t = 1;
	$ictr = 0;
	$default = 4;
	$isViewAll = false;

	if ($photoCat !== '' && CommentType::$PHOTO == $context)
		$divID = 'commentscontainer';
	else
		$divID = 'comments';
	$link = 'http://' . $_SERVER['HTTP_HOST'];

?>
<h2><span>Shout-out</span>

	<? if (isset($context) && isset($contextID) && $travelerID > 0 && $canAddComment): ?>			
			<? if ($photoCat !== '' && CommentType::$PHOTO == $context): ?>
				
					<span class="header_actions "><a class = "add" href = "javascript:void(0)" onclick = "Comment.showCommentBox();">Post a Shout-out</a></span>
				
			<? else: ?>

					<span class="header_actions "><a class = "add" href = "javascript:void(0)" onclick = "Comment.showCommentBox();">Post a Shout-out</a></span>

			<? endif; ?>
	<? endif; ?>

</h2>

<div class="content">
	<p id = "comment_status_zone" class = "hide_status_zone">
		<span id = "imgLoading">
			<img alt = "Loading" src = "<?=$link?>/images/loading.gif"/>
		</span>
		<em id = "statusCaption">Loading please wait...</em>
	</p>
	<ul id = "display_comments_content">
    	<? if (0 < count($comments)) : ?>  	  	  	
	  		
		  		<?php 
					foreach($comments as $comment ) : 
			  			try {
			  		    	$tp = new TravelerProfile($comment->getAuthor());
			  			}	
			  		    catch (Exception $exc) {}
		  		    
						$li_attributes = '';
					
			  		if($ictr >= $default){
							$li_attributes .= ' id="greeting'.$t.'" style="display: none;"';
							$t +=1;
							$isViewAll = true;
						}
				?>
					<li <?= $li_attributes ?> >

							<? $gID = 0;?>										
			 				<?if ($gID > 0 ):?>
			 					<?$ag = new AdminGroup($gID);?>
			 					<a href="<?=$link?>/group.php?gID=<?=$gID?>"  class="thumb"><img src="<?=$ag->getGroupPhoto()->getPhotoLink('thumbnail') ?>" height="37" width="37" alt="User Profile Photo" /></a>					
			 				<?else:?>					
								<a href="<?=$link?>/<?=$tp->getUserName()?>" class="thumb"><img src="<?=$tp->getPrimaryPhoto()->getPhotoLink('thumbnail') ?>" height="37" width="37" alt="User Profile Photo" /></a>					
						 		<? if (strlen($tp->getUserName()) <= 10): ?>
						 			<? $name = $tp->getUserName() ?>
						 		<? else: ?>
						 			<? $name = substr($tp->getUserName(),0,8) . '...' ?>
						 		<?endif;?>
						 	<?endif;?>
						 	<? $authorId = $tp->getTravelerID(); ?>

					 	<div class="comments_content">
					 			<?
					 				$Poke = new PokeType($comment->getPokeType());

									$pokesview = new PokesViewCB();
									if (($gID > 0) && ($groupId == $gID))
										$pokesview-> setIsMember(TRUE);
									else if (array_search($tp->getTravelerID(), $members))
										$pokesview-> setIsMember(TRUE);
					 				$pokesview-> setVisitorId($travelerID);		// travelerId of logged-in user
					 				$pokesview-> setAuthorId($authorId);		// travelerId or groupId of the greeting owner
					 				$pokesview-> setOwnerId($ownerID);			// travelerId or groupId of profile/photo/journal-entry
					 				$pokesview-> setPokeTypeId($Poke->getPokeTypeID());
					 				$pokesview-> setContextId($contextID);
					 				$pokesview-> setContext($context);
					 			?>
				 				<div class="comment_info"> <strong><?=$pokesview->getMessage()?></strong> <span><?= $d->set($comment->getCreated())->friendlyFormat() ?></span>					 							
								</div>
				 			<?
								if (trim($comment->getTheText()) !== '')
									$tmpClassName = 'comment_text';
								else
									$tmpClassName = '';
							?>
							<? if (0 == $comment->getPokeType()) : ?>
								<div id="thecommenttext<?=$comment->getCommentID()?>" class="<?=$tmpClassName?>">	
									<?= HtmlHelpers::Textile($comment->getTheText()) ?>
								</div>
							<? else : ?>
								<? $class_name = preg_replace('/.png$/','',$Poke->getPokeImage())?>
								<div class="shout do_<?=$class_name?>">do <?= $class_name ?></div>
								<div id="thecommenttext<?=$comment->getCommentID()?>" class="<?=$tmpClassName?>">	
									<?= HtmlHelpers::Textile($comment->getTheText()) ?>
								</div>
							<? endif; ?>

							<? if ($travelerID == $tp->getTravelerID() || isset($okToDelete)) : ?>
								<div id="actions<?=$comment->getCommentID()?>" class="actions">
							 			<? if ($travelerID == $tp->getTravelerID()) : ?>
							 					<a href="javascript:void(0)" onclick="Comment.setDivId('<?=$divID?>');Comment.showedit(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>,<?=$travelerID?>)">Edit</a> |
												<a href="javascript:void(0)" onclick="Comment.setDivId('<?=$divID?>');Comment.deletecomments(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>,<?=$travelerID?>)">Delete</a>
							 			<? elseif (isset($okToDelete)): ?>
							 					<a href="javascript:void(0)" onclick="Comment.setDivId('<?=$divID?>');Comment.deletecomments(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>,<?=$travelerID?>)">Delete</a>
							 			<? endif; ?>
							 	</div>
							<? endif; ?>
						 	<div id="commenteditloading<?=$comment->getCommentID()?>" style="display:none;color:red;padding-top:10px;">
						 		<img src="<?=$link?>/images/loadingAnimation.gif"/>
						 	</div>
					 	

						 </div>
		 	
					</li>
					<? $ictr += 1; ?>
		  		<? endforeach; ?>
			</ul>  		
		<? else:?>	  		
	  		<div class="help_text">
				
				<?php
					$owner = new TravelerProfile($ownerID);
					$gID = 0;

					if ($travelerID == $owner->getTravelerID())
						echo "<span>No one has left you a shout-out yet</span>. <a href=\"/travelers.php\"><strong>Post a shout-out</strong></a> to other travelers!";
					else{
						if ($gID > 0 ){
							$ag = new AdminGroup($gID);
							echo "Be the first to post on " . $ag->getName() . "'s travel page!";
						}
						else
							echo "Be the first to post on <span>" . $owner->getUserName() . "'s</span> travel page!";
					}
				?>
				
	  		</div>	  		
	  	<? endif; ?>
	  <? $all_link_style = ((isset($isViewAll) && $isViewAll)) ? "display:block;" : "display:none;"; ?>
		
		<div class="section_foot" style="<?=$all_link_style?>"> 
		  <a id="greet_layer" href="javascript:void(0)" onclick = "Comment.ecollapse(<?=$t?>, 'greet_layer')" > 
		    View All 
		  </a> 
		</div>
	  
	  <? if (0 == $travelerID) : ?>
			<p class="help_text">
				<span>You have to be <a href="<?=$link?>/login.php?redirect=<?=$envURL?>">logged in</a> to leave a shout-out</span>.<br />
				Not yet a member? <a href="<?=$link?>/register.php" style = "padding-left: 0px">Register</a> now&#8212;it&#8217;s fast, easy and totally free.
			</p>
		<? endif; ?>
	</ul>
</div>

<? // no effect to UI ?>
<script type="text/javascript">
  jQuery(document).ready(function(){
    Comment.setDivId("<?=$divID?>");
    Comment.setContextId(<?=$contextID?>);
    Comment.setContext(<?=$context?>);
    Comment.setPhotoCat('photo');
  });
</script>
<? // end of no effect to UI ?>