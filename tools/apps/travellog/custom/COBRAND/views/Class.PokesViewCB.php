<?php
	/**
	 * Created on 31 October 2007
	 * Class.PokesViewCB.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'travellog/views/Class.PokesView.php';

	class PokesViewCB extends PokesView{
		private $isMember = FALSE;

		function PokesView(){}

		function setAuthor(){
			if( 0 == $this->mAuthorId ){
				$this->mAuthor = $this->mAuthorObj->getName();
				$this->mAuthorPath = $this->mAuthor;
				return;
			}
			
			$tp = new TravelerProfile($this->mAuthorId);
			$gID = 0;

			if ($this->isMember){
				if ($gID > 0 ){
					$ag = new AdminGroup($gID);
					$this-> mAuthor = $ag->getName();
					$this-> mAuthorPath = '<a href="http://'.$_SERVER['HTTP_HOST'].'/group.php?gID='.$gID.'">'.$ag->getName().'</a>';
				}
				else{
					$this-> mAuthor = $tp->getUserName();
					$this-> mAuthorPath = '<a href="http://'.$_SERVER['HTTP_HOST'].'/'.$tp->getUserName().'">'.$tp->getUserName().'</a>';
				}
			}
			else{
				if ($gID > 0 ){
					$ag = new AdminGroup($gID);
					$this-> mAuthor = $ag->getAdministrator()->getName();
					$this-> mAuthorPath = '<a href="/'.$ag->getAdministrator()->getName().'">'.$ag->getAdministrator()->getName().'</a>';
				}
				else{
					$this-> mAuthor = $tp->getUserName();
					$this-> mAuthorPath = '<a href="'.$tp->getUserName().'">'.$tp->getUserName().'</a>';
				}
			}
		}

		function setIsMember($_isMember = FALSE){
			$this-> isMember = $_isMember;
		}
	}
?>