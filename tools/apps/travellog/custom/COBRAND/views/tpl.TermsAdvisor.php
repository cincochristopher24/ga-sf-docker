<p>An online site advisor provides travel advice to members of his/her advisor group. 
Since this capability is tied to great responsibility, GoAbroad Network only allows people 
whose authority can be verified, to be online site advisors.</p>

<h2>Eligibility</h2>

<p>You must be part of a legitimate organization, business or institution. If we deem it necessary, 
you must be ready to provide documents as part of our requirements for the verification of your 
eligibility to be an Advisor. If eligibility is not verifiable by preliminary assessment, we require 
applicants to meet at least 2 of the following requirements:</p>

<ul class="toc">
	<li>* Business registration</li>
	<li>* Positive response to test email inquiry</li>
	<li>* Phone call</li>
	<li>* Membership in professional organizations</li>
</ul>
 
<p>We reserve the right to verify the information that you submit to us and should we find it 
inadequate, we have the right to demand for more information. If we find any misrepresentation 
on your part, we reserve the right to reject your application and/or remove your group/club and 
your account from the Network.</p>

<h2>Disclaimer</h2>

<p>You are responsible for the advice that you give, or whether the advice is incorrect or inaccurate. 
We will not endorse you to our members, nor will we be responsible for personal injury or death 
resulting from anyone's use of your advisor group, the GoAbroad Network, its content, or our service.
GoAbroad Network cannot guarantee and do not promise any specific results from use of the Website 
or the Service.</p>

<h2>Commercial Use</h2>

<p>The GoAbroad Network Advisor Group feature is intended to aid in the efficient communication 
between travelers and study abroad providers, student advisors, and travel organizations. The features 
should not be used in ways other than what they are intended for. Conspicuous marketing is a 
misuse of the site and we reserve the right to remove any data or information in the groups which we deem 
to be a violation of this term.</p>

<p>You must also agree with the <a href="/terms.php">Terms of Use</a> of GoAbroad Network.