<?php
/*
 * Created on 01 12, 07
 *
 * @author Daphne 
 * Purpose:  template for Group Messages Panel (Bulletin, Alerts, News) For cobrand sites
 * 
 */
  
?>
<?php //$this->useMainTemplate(false); ?>

<? $this->doNotUseMainTemplate(); ?>

<?if (0 < count($messages)) : ?>
 		<ul class="bulletin_list">
 		<?foreach($messages as $message) :?>
 			<?if ('BulletinMessage' == get_class($message)) : ?>		 		

	 			<li<?php if(1==$messType): ?> class="bulletin"<?endif;?>>
	 				<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
			 			<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
			 			<? $suffix = $context->isSubGroup() ? '&groupID=' . $context->getGroupID() : '&group' ?>
			 			<a class="title" title="View Post" href="/bulletinmanagement.php?hlEntryID=<?=$message->getAttributeID()?><?=$suffix?>"><?=$message->getTitle()?></a><br /> 
			 			<p class="details">
			 				<a class="username" href="<?=$message->getMessageSource()->getFriendlyUrl() ?>" ><?=$message->getMessageSource()->getName() ?></a>
							<?php 
								$recipientGroup = $message->getMessageSource()->getRecipientGroup();
								if(!is_null($recipientGroup) && !$message->getMessageSource()->postedAsAdministrator()):
							?>			 			
				 			 |
				 			 <a href="<?=$message->getMessageSource()->getRecipientGroup()->getFriendlyUrl()?>">
				 			 	<?=$message->getMessageSource()->getRecipientGroup()->getName() ?>
				 			 </a>
				 			<?php endif; ?>
			 			</p>
			 		</div>
			 		<div class="clear"></div>
				</li>
			 <?endif; ?>	 	
			 
			 <?if ('PersonalMessage' == get_class($message)) :?>
			 	<li<?php if(1==$messType): ?> class="messages"<?endif;?>>
					<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
						<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
				 		<a class="title" href="/messages.php?view&amp;active=inbox&amp;messageID=<?=$message->getAttributeID()?>"><?=$message->getTitle()?>-[Personal Message]</a>
				 		<p class="details">
			 				<a class="username" href="/<?=$message->getSource()->getUsername() ?>" ><?=$message->getSource()->getUsername() ?></a>
			 			</p>
					</div>
			 		<div class="clear"></div>
			 	</li>
			 <?endif;?>
			 
			 <?if ('InquiryMessage' == get_class($message)) :?>
			 	<li<?php if(1==$messType): ?> class="inquiry"<?endif;?>>
					<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
						<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
				 		<a class="title" href="/messages.php?view&amp;active=inquiry&amp;messageID=<?=$message->getAttributeID()?>"><?=$message->getTitle()?>-[Inquiry]</a>
				 		<p class="details">
			 				<a class="username" href="/<?=$message->getSource()->getUsername() ?>" ><?=$message->getSource()->getUsername() ?></a>
			 			</p>
					</div>			 		
			 	</li>			 	
			 <?endif;?>
			 
			 <? // added by daf 12.22.06 ?>
			 <?if ('News' == get_class($message)) :?>
			 	<li class="news">
					<div class="details_container">
						<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
				 		<a class="title" href="/newsmanagement.php?hlEntryID=<?= $message->getNewsID() ?>"><?= $message->getTitle();?></a>
					</div>
			 		<div class="clear"></div>
			 	</li>			 	
			 <?endif;?>
			 
			 <? // added by daf 12.22.06 ?>
			 <?if ('AlertMessage' == get_class($message)) :?>
			 	<li<?php if(1==$messType): ?> class="alert"<?endif;?>>
					<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
			 			<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
						<p class="alert_text"><?= GaString::activateUrlLinks($message->getText()) ?></p>
					</div>
					<div class="clear"></div>
			 	</li>			 	
			 <?endif;?>
			 
			 <?
			 	// added by: cheryl ivy q. go  09 august 2007
			 	if ('Comment' == get_class($message)){
					$tp = new TravelerProfile($message->getAuthor());
					$gID = 0;

					$Poke = new PokeType($message->getPokeType());
					$Poke-> setPath('/images/poke/');
					
					$pokesview = new PokesViewCB();
					if (($gID > 0) && ($groupID == $gID))
						$pokesview-> setIsMember(TRUE);
					else if (array_search($tp->getTravelerID(), $members))
						$pokesview-> setIsMember(TRUE);
	 				$pokesview-> setAuthorId($tp->getTravelerID());		// travelerId or groupId of the greeting owner

					$Group = GroupFactory::instance()->create( array($groupId) );
					$travelerId = $Group[0]->getAdministratorID();
					$pokesview->setOwnerId($travelerId);			// travelerId or groupId of profile/photo/journal-entry
	 				$pokesview->setPokeTypeId($Poke->getPokeTypeID());
					$pokesview->setMessage($message->getTheText());

					if (!strcasecmp("TravelLog", get_class($message->getContext()))){
						$pokesview-> setContextId($message->getContext()->getTravelLogID());
						$pokesview-> setContext(3);
					}
	 				elseif (!strcasecmp("Photo", get_class($message->getContext()))){
	 					$pokesview-> setContextId($message->getContext()->getPhotoID());
	 					$pokesview-> setContext(2);
	 				}
	 				else{
	 					$pokesview-> setContextId($message->getContext()->getTravelerID());
	 					$pokesview-> setContext(1);
	 				}

	 				if (0 < strlen($message-> getTheText()))
	 					$pokesview-> setHasMessage(true);
	 				else
	 					$pokesview-> setHasMessage(false);
	 				$pokesview-> setAuthor();
	 				$pokesview-> setOwner();
	 			?>
					<li class = "greetings">
						<? if ( $messType == MessagesPanel::$ALL ) : ?>
							<div class = "details_container viewall">
								<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
								<div class="details">
									<? if (0 < $message->getPokeType()) : ?>							
										<img src = "<?='http://'. $_SERVER['HTTP_HOST'].'/images/poke/'.$Poke->getPokeImage()?>" height = "22" width = "22" />
									<? endif; ?>
									<div class="message"><?=$pokesview->getPassportMessage()?></div>
									<div class="clear"></div>
								</div>
							</div>
						<? else : ?>
							<div class = "details_container" style = "background-image: none;">
								<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
								<div class="details">
									<? if (0 < $message->getPokeType()) : ?>							
										<img src = "<?='http://'. $_SERVER['HTTP_HOST'].'/images/poke/'.$Poke->getPokeImage()?>" height = "22" width = "22" />
									<? endif; ?>
									<div class="message"><?=$pokesview->getPassportMessage()?></div>
									<div class="clear"></div>
								</div>
							</div>
						<? endif; ?>	
						<div class="clear"></div>
					</li>
				<?
			 	}	
 			endforeach;?>
 		</ul>		 	 
<? else : ?>
  <p class="help_text"> <span> 	
  	 <? if (MessagesPanel::$ALL ==  $messType){ 		  	   
	  	   echo nl2br($ht->getHelpText('group-messagecenter'));
  	 	} 
  	 	
  	 	if (MessagesPanel::$BULLETIN == $messType) {
  	 	 echo nl2br($ht->getHelpText('group-bulletin'));
  	 	} 
  	 	
  	 	if (GroupMessagePanel::$ALERTS == $messType) {
  	 	   echo nl2br($ht->getHelpText('group-alerts'));
  	 	} 
  	 	
  	 	if (GroupMessagePanel::$NEWS == $messType) {
  	 	   echo nl2br($ht->getHelpText('group-news'));
  	 	}

  	 	if (MessagesPanel::$GREET == $messType) {
		   echo 'You have no greeting.';
  	 	}
  	 	  	 	
	  ?>
	</span>	 
  </p>
<? endif; ?>


