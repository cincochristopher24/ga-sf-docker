<ul class="toc">
	<li><a href="#about">About <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net</a></li>
	<li><a href="#collection">Collection and Use of Information by <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net</a> </li>
	<li><a href="#reg">Registration</a></li>
	<li><a href="#travel">Travel Journals and Travel Logs</a></li>
	<li><a href="#updates"><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net Updates</a></li>
	<li><a href="#cookies">Use of Cookies</a></li>
	<li><a href="#children">Children</a></li>
	<li><a href="#third">Third Party Advertising</a></li>
	<li><a href="#links">Links to Other Sites</a></li>
	<li><a href="#sharing">Sharing and Disclosure of Information <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.Net Collects</a></li>
	<li><a href="#confidentiality">Confidentiality and Security</a></li>
	<li><a href="#changes">Changes in Our Privacy Policy</a></li>
	<li><a href="#contact">Contact <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net</li>
</ul> 
<a name="about"></a><h2>About <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net</h2>
<p>
<?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad Network is the ultimate site for travelers, students, singles, couples and everyone who is on the go. It is a Social Directory, Travel Journal and Traveler Network rolled into one. <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net members set up their unique individual profiles and their personal information is displayed to allow our users to identify each other and expand their network of friends. Members may change their profile information at any time and can control how other members and the service communicates with them.
</p>

<a name="collection"></a><h2>Collection and Use of Information by <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net</h2>
<p>
<strong>*** <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net does not send spam or sell email addresses.</strong>
</p>

<a name="reg"></a><h2>Registration</h2>
<p>When you register with <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net, you submit personal information including,
but not limited to: your name, age, gender, address, email address, phone number 
and other similar personal information that identifies you to us as well as our 
users. Any visitor of <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net can view your profile information and photos 
that you have uploaded, however, you can control what part of your profile 
information is visible. To facilitate searching and finding friends and 
acquaintances on the service, <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net allows users to search for other members.
</p>

<a name="travel"></a><h2>Travel Journals and Travel Logs</h2>
<p>Members should be aware that when posting personally identifiable information
in areas of the site accessible to other users such as travel journals, these
information can be read, collected, or used by other users of this website and
could be used improperly to send you unsolicited messages. 
We <strong>strongly</strong> recommend <strong>never</strong> adding you <strong>email
address, contact information or personal details</strong> to your written journal entries.
Members are solely
responsible for these type of information they choose to submit. <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net
is not liable for any misuse or improper collection of such information such as 
email harvesters that search every page on the internet mining for email addresses to send spam to. Any
misuse or improper collection of information provided on <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net is a violation
of the <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net <a href="terms.php">Terms of Use</a>. <a href="feedback.php">Contact
us</a> to report abuse of personal information.
</p>

<a name="updates"></a><h2><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net Updates</h2>
<p><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net may use a member's name and email address to send updates or news regarding the service. Members may choose whether or not they wish to receive this type of email.
</p>

<a name="cookies"></a><h2>Use of Cookies</h2>
<p>Once you register with <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net and use our services, you are no longer
	anonymous to us. If you are browsing the website without registering, you are
	anonymous to us, but we will still collect information like browser type and
	IP address. These data are collected for all <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net visitors and are non-personally
	identifiable and are used to manage, monitor and track usage and improve
website services.</p>
<p>In addition, we store certain information from your browser
	using "cookies." A
	cookie is a piece of data stored on the user's computer tied to information
	about the user. We use session ID cookies to confirm that users are logged
in and are removed once the user closes the browser. </p>
<a name="children"></a><h2>Children</h2>
<p>Children under the age of thirteen (13) are not allowed to register as a member
of <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net or use the website. Please read the <a href="terms.php">Terms
of Use</a> for eligibility. No one under age 13 is allowed to provide any personal
information to or on <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net. In the event that we learn that we have collected
personal information from a child under age 13 without verification of parental
consent, we will delete that information as quickly as possible. If you believe
that we might have any information from or about a child under 13, please <a href="feedback.php">contact
us</a>.
</p>

<a name="third"></a><h2>Third Party Advertising</h2>
<p>
<?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net may share profile information and aggregate usage information in a non-personally identifiable manner with partners and other third parties in order to present to members and other users of the <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net website more targeted advertising, products and services. We assure you that in such situations we will never disclose information that would personally identify you. 
</p>



<a name="links"></a><h2>Links to Other Sites</h2>
<p>Our provision of a link (such as in advertisements) to any other website or location is for your convenience and does not signify our endorsement of such other website or location or its contents. We have no control over, do not review, and cannot be responsible for these outside websites or their content. Please be aware that the terms of our Privacy Policy does not apply to these outside websites.
</p>

<a name="sharing"></a><h2>Sharing and Disclosure of Information <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net Collects</h2>
<p>
Except as otherwise stated in this privacy statement, <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net will not disclose personal information to any third party unless we believe that disclosure is necessary: (1) to conform to legal requirements or to respond to a subpoena, search warrant or other legal process received by <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net, whether or not a response is required by applicable law; (2) to enforce the <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net <a href="terms.php">Terms of Use</a> or to protect our rights; or (3) to protect the safety of members of the public and users of the service.
</p>

<a name="confidentiality"></a><h2>Confidentiality and Security</h2>
<p>
The <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net account of every member is password-protected. We take every
precaution to protect the information of all members, as well as information
collected from other users of the <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net website. We use industry standard
measures to protect all information that is stored on our servers and within
our database. We limit the access to these information to those employees who
need access to perform their job function such as our customer service personnel.
If you have any questions about the security of our website, please <a href="feedback.php">contact
us</a>.
</p>

<a name="changes"></a><h2>Changes in Our Privacy Policy</h2>
<p>
<?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net may update this policy from time to time. We will notify you about significant changes by sending a notice to your contact email address or by placing a prominent notice on our website. If you object to any such changes, you may cease using our website. Continued use of <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net following notice of any such changes shall indicate your acknowledgment of such changes and agreement to be bound by the terms and conditions of such changes.
</p>

<a name="contact"></a><h2>Contact <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net</h2>
<p>
If you have any questions about this privacy policy, the practices of this website,
or your dealings with this website, please <a href="feedback.php">contact us</a>.
</p>