<?
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Groups / Clubs');
?>

  <div id="intro_container">
	  <div id="intro">
		  <? $ht = new HelpText();?>
		  <h1><?= $ht->getHelpText('GROUP-TAB-SUBHEAD')?></h1>
		  <div class="content">
		    <?= HtmlHelpers::Textile($ht->getHelpText('groups/clubs-tab')); ?>			
		  </div>
	  </div>
  </div>

	<div class="layout_2" id="content_wrapper">
		<div id="wide_column">
			<div id="groups_and_clubs" class="section">	
        <h2><span>Groups</span></h2>
        
        <div class="content" id="subGroupsContent">
          <div>
			      <? if (strlen($searchKey) && $subgroups_count): ?>
				      <p>Showing all results found for search of '<?=$searchKey?>' !</p>
			      <? endif; ?>
		      </div>
          
          <ul id="groups_list_main">
            <? while($subgroups->hasNext()): ?>
      			<?php
            	$group = $subgroups->next(); 
							$subgroup_name = str_replace('-', '_', $group->getName());
							$subgroup_name = str_replace(' ', '-', $subgroup_name);
							$group_name = str_replace('-', '_', $parent->getName());
							$group_name = str_replace(' ', '-', $group_name);
								
							$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;	            	
            ?>
              
              <li itemid="<?= $group->getGroupID() ?>">	
                <a class="group_thumb" href="<?= $friendlyURL ?>">
                  <img src="<?= $group->getGroupPhoto()->getPhotoLink('standard'); ?>" alt="<?= $group->getName(); ?>" >
                </a>					
				
			          <div class="groups_info">
					        <strong>
					          <a href="<?= $friendlyURL ?>">
					            <?= $group->getName(); ?>
					          </a>
					        </strong>
					
					        <p>
						        <?= StringFormattingHelper::wordTrim($group->getDescription(), 40)?>
						
						        <? if (40 < count(explode(' ', strip_tags($group->getDescription()) ) ) ) : ?>
						          <?='&#8230;' ?>
						        <? endif ; ?> 
						
						        <a href="<?= $friendlyURL ?>" title="Read more about the <?= $group->getName() ?> travel community">
						          Read More
						        </a>
					        </p>
					
					        <? if (trim($group->getGroupMembershipLink('all_subgroups',$current_page)) !== ''):?>
   				          <p class="action">
   				            <?=$group->getGroupMembershipLink('all_subgroups',$current_page)?>
   				          </p>
                  <? endif; ?>
				        </div>
			        </li>		  
		        <? endwhile; ?>
	        </ul>
	       
        <? $paging->showPagination(); ?>
        
			  <? if (0 == $subgroups_count): ?>
				  <? if (strlen($searchKey)): ?>
					  <p class="help_text"> <span>No matching results found for search of '<?=$searchKey?>'<span>.</p>
				  <? else: ?>
					  <p class="help_text"> <span>There are no groups</span>.</p>
				  <? endif; ?>
			  <? endif; ?>
        
			  </div>
			</div>
		</div>

    <div id="narrow_column">
		  <div class="section" id="quick_tasks2">
        <h2><span>Search</span></h2>
        
        <div class="content">
          <ul class="actions">
            <form action="subgroups.php" method="post" id="searchGroupForm">
              <span>
                <input type="text" class="text" id="txtGrpName" name="txtGrpName" value="<?=$searchKey?>" />				
              </span>
							<span>		
                <input type="submit" alt="search button" title="Search" value="Search group" name="btnSearchName" id="btnSearchName">
              </span>
            </form>
          </ul>
        </div>				
      </div>
    </div>
  
  </div>

<!--added by ianne - 11/20/2008 success message after confirm-->
<?if(isset($success)):?>
	<script type="text/javascript">
		CustomPopup.initPrompt("<?=$success?>");
		window.onload = function(){
			CustomPopup.createPopup();
		}
	</script>
<?endif;?>