<?php
	/**
	 * Created on 31 October 2007
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'travellog/views/Class.CommentsView.php';
	require_once 'travellog/custom/COBRAND/views/Class.PokesViewCB.php';

	class CommentsViewCB extends CommentsView{
		private $groupId = 0;
		private $members = array();

		function CommentsViewCB(){}

		function setGroupId($_groupId = 0){
			$this-> groupId = $_groupId;
		}

		function getGroupId(){
			return $this-> groupId;
		}

		function getGroupMembers(){
			if (0 < $this->groupId){
				$Group = new AdminGroup($this->groupId);
		 		$members = $Group-> getMembers();
		 		foreach ($members as $member){
		 			$this->members[] = $member->getTravelerID();	
		 		}
			}
			return $this-> members;
		}

		/**
	 	 * renders the ajax comments to the page
	 	 */
	 	function renderAjax() {
	 		$networkName = (isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad";
	 		$template = new Template();
	 		if ($this->mOkToDelete)
	 			$template->set("okToDelete",$this->mOkToDelete);
	 		$template->set("message", $this->mMessage);
	 		$template->set("comments",$this->comments);
	 		$template->set("travelerID",$this->mTravelerID);
	 		$template->set("context",$this->mContext);
	 		$template->set("contextID",$this->mContextID);
	 		$template->set("ownerID", $this->ownerId);
	 		$template->set("canAddComment",$this->mCanAddComment);
	 		$template->set("photoCat",$this->mPhotoCat);
	 		$template->set("genID",$this->mGenID);
	 		$template->set("envURL",$this->mEnvURL);
	 		$template->set("submitmode",$this->submitmode);
	 		$template->set("jsName",$this->jsName);
			$template->set("groupId", $this->groupId);
 		  $template->set("showConfirmationMessageOnly",$this->mShowConfirmationMessageOnly);
			$group = new AdminGroup($this->groupId);
			$template->set("directSignupAllowed",$group->directSignupAllowed());
      
	 	return $template->fetch('travellog/views/tpl.ViewAjaxComments.php');
	 	}
	}

?>