<?php
/*
 * Created on 12 19, 06
 *
 * @author Kerwin Gordo
 * Purpose:  template for Messages Panel (Personal Messages,Bulletin,Inquiry) For Cobrand Sites.
 */
?>
<?if (0 < count($messages)) : ?>
 		<ul class="bulletin_list"> 		
 		<?foreach($messages as $message) :?>
 			<?if ('BulletinMessage' == get_class($message)) : ?>		 		
	 			<li<?php if(1==$messType): ?> class="bulletin"<?endif;?>>
					<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
						<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
			 			<a class="title" title="View Post" href="bulletinmanagement.php?hlEntryID=<?=$message->getAttributeID()?>"><?=$message->getTitle()?></a><br /> 
			 			<p class="details">
			 				<a class="username" href="<?=$message->getMessageSource()->getFriendlyUrl() ?>" ><?=$message->getMessageSource()->getName() ?></a>
							<?php
								$groupLinks = array();
			 					$destinations = $message->getDestination();
			 					foreach($destinations as $dest) {
			 						if ("Group" == get_parent_class($dest)) 
			 							$groupLinks[] = '| <a href="group.php?gID=' . $dest->getGroupID() . '">' . $dest->getName() . '</a>';
								}
			 					$gstr = implode(", ",$groupLinks);
			 				?>
			 				<?=$gstr?>
				 		</p>
					</div>
		 			<div class="clear">
				</li>
			 <?endif; ?>
			<?if ('AlertMessage' == get_class($message)):?>
				<li<?php if(1==$messType): ?> class="alert"<?endif;?>>
					<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
		 				<p class="date">
							<?=date('M j, Y',strtotime($message->getCreated()))?>
							<br />
							<a href="javascript:void(0)" alt="Delete Alert" title="Delete Alert" onclick="jQuery(this).mtDelAlert(<?= $message->getAttributeID() ?>)"><span>Delete</span></a>
						</p>
		 				<p class="title"><?=$message->getText()?> </p>
		 				<p class="details">
			 				<?
								$groupLinks = array();
			 					$destinations = $message->getDestination();
								$i = 0;
			 					foreach($destinations as $dest) {
			 						if ("Group" == get_parent_class($dest)) {
			 							$groupLinks[] = '<a href="group.php?gID=' . $dest->getGroupID() . '">' . $dest->getName() . '</a>';
									}
								}
			 					$gstr = implode(", ",$groupLinks);
			 				?>
			 				<?=$gstr?>
		 				</p>
					</div>
					<div class="clear"></div>
				</li>
			<?endif; ?>
			<? if('SurveyForm' == get_class($message)): ?>
				<? require_once('Class.GaDateTime.php');
					$d = new GaDateTime();
					$surveyGroup = $message->getOwnerGroup();
				?>
				<li<?php if(1==$messType): ?> class="survey"<?endif;?>>
					<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
		 				<p class="date"><?= $d->set($message->getDateCreated())->friendlyFormat(); ?></p>
		 				<p class="title">
							<a href="/surveyform.php?frmID=<?= $message->getSurveyFormID() ?>&gID=<?= $surveyGroup->getGroupID() ?>">
								<strong><?= $message->getName() ?></strong>
							</a>
						</p>
		 				<p class="details">
			 				<a href="/group.php?gID=<?=$surveyGroup->getGroupID()?>"><?= $surveyGroup->getName() ?></a>
		 				</p>
					</div>
					<div class="clear"></div>
				</li>
			<?endif;?>	
			 <?if ('PersonalMessage' == get_class($message)) :?>
			 	<li<?php if(1==$messType): ?> class="messages"<?endif;?>>
					<div class="details_container<?php if(1==$messType): ?> viewall<?php endif;?>">
						<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
				 		<a class="title" href="messages.php?view&amp;active=inbox&amp;messageID=<?=$message->getAttributeID()?>"><?=$message->getTitle()?>-[Personal Message]</a>
				 		<p class="details">
				 			<?if (is_a($message->getSource(),'Traveler')):?>

				 				<?if (!array_search($message->getSource()->getTravelerID(),$members)):?>
				 					<?=$message->getSource()->getUsername() ?>
				 				<?else:?>
			 						<a class="username" href="/<?=$message->getSource()->getUsername() ?>" ><?=$message->getSource()->getUsername() ?></a>
			 					<?endif;?>
			 				<?elseif (is_a($message->getSource(),'Group')) : ?>
			 					<?if ($message->getSource()->getGroupID()==$groupID):?>
			 						<?$suffix = $message->getSource()->isSubGroup() ? '?gID=' . $message->getSource()->getGroupID() : '' ; ?>
									<a class="username" href="group.php<?=$suffix?>" ><?=$message->getSource()->getName() ?></a>
			 					<?else:?>
			 						<?=$message->getSource()->getName() ?>	
			 					<?endif;?>	
			 				<?endif;?>
			 			</p>
					</div>
			 		<div class="clear"></div>
			 	</li>
			 <?endif;?>

			 <?if ('InquiryMessage' == get_class($message)) :?>
			 	<!--- display nothing -->
			 <?endif;?>
			 
			 <?
			 	// added by: cheryl ivy q. go  09 august 2007
			 	if ('Comment' == get_class($message)){
			 		$tp = new TravelerProfile($message->getAuthor());
					$gID = 0;

					$Poke = new PokeType($message->getPokeType());
					$Poke-> setPath('/commons/images/poke/');

					$pokesview = new PokesViewCB();
					if (($gID > 0) && ($groupID == $gID))
						$pokesview-> setIsMember(TRUE);
					else if (array_search($tp->getTravelerID(), $members))
						$pokesview-> setIsMember(TRUE);
	 				$pokesview->setAuthorId($tp->getTravelerID());		// travelerId or groupId of the greeting owner
	 				$pokesview->setOwnerId($_SESSION['travelerID']);	// travelerId or groupId of profile/photo/journal-entry
	 				$pokesview->setPokeTypeId($Poke->getPokeTypeID());
					$pokesview->setMessage($message->getTheText());

					if (!strcasecmp("TravelLog", get_class($message->getContext()))){
						$pokesview-> setContextId($message->getContext()->getTravelLogID());
						$pokesview-> setContext(3);
					}
	 				elseif (!strcasecmp("Photo", get_class($message->getContext()))){
	 					$pokesview-> setContextId($message->getContext()->getPhotoID());
	 					$pokesview-> setContext(2);
	 				}
	 				else{
	 					$pokesview-> setContextId($message->getContext()->getTravelerID());
	 					$pokesview-> setContext(1);
	 				}

	 				if (0 < strlen($message-> getTheText()))
	 					$pokesview-> setHasMessage(true);
	 				else
	 					$pokesview-> setHasMessage(false);
	 				$pokesview-> setAuthor();
	 				$pokesview-> setOwner();
			 	?>
					<li class = "greetings">
						<? if ( $messType == MessagesPanel::$ALL ) : ?>
							<div class = "details_container viewall">
								<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
								<div class="details">
									<? if (0 < $message->getPokeType()) : ?>							
										<img src = "<?='http://'. $_SERVER['HTTP_HOST'].'/images/poke/'.$Poke->getPokeImage()?>" height = "22" width = "22" />
									<? endif; ?>
									<div class="message"><?=$pokesview->getPassportMessage()?></div>
									<div class="clear"></div>
								</div>
							</div>
						<? else : ?>
							<div class = "details_container" style = "background-image: none;">
								<p class="date"><?=date('M j, Y',strtotime($message->getCreated()))?></p>
								<div class="details">
									<? if (0 < $message->getPokeType()) : ?>							
										<img src = "<?='http://'. $_SERVER['HTTP_HOST'].'/images/poke/'.$Poke->getPokeImage()?>" height = "22" width = "22" />
									<? endif; ?>
									<div class="message"><?=$pokesview->getPassportMessage()?></div>
									<div class="clear"></div>
								</div>
							</div>
						<? endif; ?>	
						<div class="clear"></div>
					</li>
				<?
			 	}
 			endforeach;?>
 		
 		</ul>		 	 
<? else : ?>
  <p class="help_text">
	<span>  	
  	 <? if (MessagesPanel::$ALL ==  $messType){ 		  	   
	  	   $ht = new HelpText();
		   echo nl2br($ht->getHelpText('dashboard-messagecenter'));
		   echo '<br /><br /><span class="actions"><a class="add" href="bulletinmanagement.php?method=compose">New Post</a></span>';
  	 	}
  	 	if (MessagesPanel::$BULLETIN == $messType) {
			echo 'No bulletin.';
		}  	 	
  	 	if (MessagesPanel::$PERSONAL_MESSAGE == $messType) {
  	 		echo 'You have no unread messages.';
  	 	}  	 	
  	 	if (MessagesPanel::$GREET == $messType) {
	  		echo 'You have no greeting.';
	  	}
		if (MessagesPanel::$ALERT == $messType) {
  	 		echo 'You have no alert messages.';
  	 	}
		if (MessagesPanel::$SURVEY == $messType) {
  	 		echo 'You have no survey forms to fill out.';
  	 	}
	  ?>
	</span>	 
  </p>
<? endif; ?>
