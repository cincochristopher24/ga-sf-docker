<?php
/*
 * Created on 03 13, 08 
 */
 
 require_once('travellog/views/Class.TravelsView.php');
 require_once('travellog/custom/COBRAND/model/Class.TravelCB.php');
 
 class TravelsViewCB extends TravelsView {
 	
 	
 	function render(){
 		if ($this->mOwner != null){
 			
			$jpp = new JournalPrivacyPreference();
			
			$ownerTravID = $this->mOwner->getAdministrator()->getTravelerID();
						
			$privacycriteria = $jpp->getPrivacyCriteria2s($this->mTravelerID , $ownerTravID );
			
			$cr = new Criteria2();
			
			$cr->setGroupBy('travelID');
			
			$travels = TravelCB::getFilteredGroupTravels($cr);
			//echo '<pre>';
			//print_r($travels); exit;
						
			$tlogs = TravelLog::getTravellogs($this->mOwner);			
	
 			$tt = new Template();
 				
 			//$tcount = count($travels);
 			$hasMoreJournals = false;
 				 			
 			
	 			
	 			$tt->set('canEdit',$this->canEdit);
	 			if ($travels == null || count($travels) == 0 ){
	 				$ht = new HelpText();
	 				$tt->set('helptext',$ht->getHelpText('dashboard-traveljournals'));
	 			}
	 				 				 			
	 				
				// load admin, staff and moderators of this group to determine if controls could be editable
				$grpOfficers = array();
				$grpOfficers[] = 'padding';      // we insert a value at 0 index so that 0 offset will not be used and be mistaken as false when calling array_search
				$grpOfficers[] = $this->mOwner->getAdministrator()->getTravelerID();
				$staffers = $this->mOwner->getStaff();
				foreach($staffers as $staff){
					$grpOfficers[] = $staff->getTravelerID();
				}
				
				if (array_search($this->mTravelerID,$grpOfficers)) {					 					
					$tt->set('addTravelLink','journal.php?action=add');
					$this->canEdit = true;
					$tt->set('canEdit',$this->canEdit);		
				}		
											 				 						
	 			
	 			$travels = $this->filterTravels($travels);
	 			$tlogs = $this->filterTravelEntries($tlogs);
	 			$hasTravelEntriesAr = $this->createJournalHasContentsArray($travels,$tlogs);
	 			
	 			$tt->set('hasTravelEntriesAr',$hasTravelEntriesAr);
	 			$tt->set('tlogs',$tlogs);
	 			$tt->set('travels_array',$travels);
	 					
	 			$tt->out($this->mTemplatePath . 'tpl.IncTravelSummary.php');	 				 					 			 	
 		}
 	}
 	
 }
 
 
?>
