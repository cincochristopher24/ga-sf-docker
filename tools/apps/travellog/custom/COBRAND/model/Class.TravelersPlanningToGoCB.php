<?php
class TravelersPlanningToGoCB{
	
	private $total_records = 0;
	
	/**********************************************************************************
	 * edits of neri to GetFilterList():
	 * 		filtered data so as not to display certain traveler(s)
	 * 			to blocked users: 											12-03-08
	 * 		deleted online attribute from query: 							12-04-08
	 * 		modified way of setting traveler's attributes and checked if 
	 * 			traveler is suspended:										12-17-08
	 **********************************************************************************/
	
	function GetFilterList($props){
		require_once('travellog/custom/COBRAND/model/Class.TravelerCB.php');
		require_once('travellog/model/Class.TravelDestination.php');
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn = new Connection;
		$rs1  = new Recordset( $conn );
		$rs2  = clone($rs1);
		$arr  = array();
		
		/*$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS qryTravelers.travelerID, locID, username, ranking, startdate, enddate, country, isOnline FROM ' .
						'(' .
							'SELECT qryPlanningToGoJournals.travelerID, locID, username, ranking, startdate, enddate, country, trim(loginTime) AS isOnline ' .
							'FROM ' .
							'qryPlanningToGoJournals LEFT JOIN tblSessions ON (tblSessions.session_user_id = qryPlanningToGoJournals.travelerID AND `domain` = \'' . $GLOBALS['CONFIG']->getServerName() . '\') ' .
							'WHERE locID IN (%s) ' .
							'UNION ' .
							'SELECT qryPlanningToGoTravelPlans.travelerID, locID, username, ranking, startdate, enddate, country, trim(loginTime) AS isOnline ' .
							'FROM ' .
							'qryPlanningToGoTravelPlans LEFT JOIN tblSessions ON (tblSessions.session_user_id = qryPlanningToGoTravelPlans.travelerID AND `domain` = \'' . $GLOBALS['CONFIG']->getServerName() . '\') ' .
							'WHERE locID IN (%s) ' .
						') ' .
						'qryTravelers, tblGrouptoTraveler ' .
						'WHERE 1 = 1 %s '
						,$props['locationIDList']
						,$props['locationIDList']
						,$props['obj_criteria']->getCriteria2('AND')
					);*/ 
		
		$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS qryTravelers.travelerID, locID, username, ranking, startdate, enddate, country FROM ' .
						'(' .
							'SELECT qryPlanningToGoJournals.travelerID, locID, username, ranking, startdate, enddate, country ' .
							'FROM ' .
							'qryPlanningToGoJournals ' .
							'WHERE locID IN (%s) ' .
							'UNION ' .
							'SELECT qryPlanningToGoTravelPlans.travelerID, locID, username, ranking, startdate, enddate, country ' .
							'FROM ' .
							'qryPlanningToGoTravelPlans ' .
							'WHERE locID IN (%s) ' .
						') ' .
						'qryTravelers, tblGrouptoTraveler ' .
						'WHERE 1 = 1 %s '
						,$props['locationIDList']
						,$props['locationIDList']
						,$props['obj_criteria']->getCriteria2('AND')
					);
		
		$rs1->Execute($sql1);
		
		if( $rs1->Recordcount() ){
			
			$sql2 = 'SELECT FOUND_ROWS() AS totalrecords';
			$rs2->Execute($sql2);
			$this->total_records = $rs2->Result(0, "totalrecords");
			
			while( $row = mysql_fetch_assoc($rs1->Resultset()) ){
				$objNewMember1 = new TravelDestination;
				$objNewMember1->setStartDate       ( $row['startdate']  );
				$objNewMember1->setEndDate         ( $row['enddate']    );
				$objNewMember1->setCountry         ( $row['country']    );			
			
				/*$online = $row['isOnline'] ? 1 : 0;
				$objNewMember  = new TravelerCB;
				$objNewMember->setTravelerID       ( $row['travelerID'] );
				$objNewMember->setUserName         ( $row['username']   );
				$objNewMember->setOnLine           ( $online            );*/
				
				$objNewMember  = new TravelerCB($row['travelerID']);
				$objNewMember->setTravelDestination( $objNewMember1     );
				
				if(!$objNewMember->isSuspended()) {
					if (0 < $props['travelerID'] && !$objNewMember->isBlocked($props['travelerID']))			
						$arr[] = $objNewMember;  
					else if (0 == $props['travelerID'])
						$arr[] = $objNewMember;
				} 
			}
		}

		return $arr;
	}
	
	function GetTravelersCountryWithFutureTravels(){
		global $CONFIG;
		
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn         = new Connection;
		$rs1          = new Recordset( $conn );
		$arr_contents = array();
		
		$sql1 = sprintf
					(
						'SELECT locID, country FROM ' .
						'(' .
							'SELECT locID, country ' .
							'FROM ' .
							'qryPlanningToGoJournals, tblGrouptoTraveler  ' .
							'WHERE qryPlanningToGoJournals.travelerID = tblGrouptoTraveler.travelerID ' .
							'AND tblGrouptoTraveler.groupID           = %d ' .
							'UNION ' .
							'SELECT GoAbroad_Main.tbcountry.locID AS locID, country ' .
							'FROM tblTraveler, tblTravelerRanking, tblGrouptoTraveler, SocialApplication.tbUser, SocialApplication.tbTravelWish, SocialApplication.tbTravelWishToCountry, GoAbroad_Main.tbcountry ' .
							'WHERE tblTraveler.travelerID                     = tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID                = SocialApplication.tbUser.travelerID ' .
							'AND tblTravelerRanking.travelerID           NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
							'AND tblTravelerRanking.travelerID                = tblGrouptoTraveler.travelerID ' .
							'AND tblGrouptoTraveler.groupID                   = %d ' .
							'AND SocialApplication.tbUser.userID                    = SocialApplication.tbTravelWish.userID ' .
							'AND SocialApplication.tbTravelWish.travelWishID        = SocialApplication.tbTravelWishToCountry.travelWishID ' .
							'AND SocialApplication.tbTravelWishToCountry.startdate >= NOW() ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  = GoAbroad_Main.tbcountry.countryID ' .
						') ' .
						'qryTravelers ' . 
						'GROUP BY locID ' .
						'ORDER BY country ' 
						,$CONFIG->getGroupID()
						,$CONFIG->getGroupID()
					); 
		
		
		$rs1->Execute($sql1);
		
		if( $rs1->Recordcount() ){
			$arr_contents[] = array('locationID' => 0, 'name' => '- select a country -');
			while( $row = mysql_fetch_assoc($rs1->Resultset()) ){
				$arr_contents[] = array('locationID' => $row['locID'], 'name' => $row['country']); 
			}
		}
		
		return $arr_contents;
	} 
	
	function GetTravelersCountryWithFutureTravelsJSON(){
		require_once('JSON.php');
		$json     = new Services_JSON(SERVICES_JSON_IN_ARR);
		$contents = $json->encode( $this->GetTravelersCountryWithFutureTravels() );
		return $contents;  
	}
	
	function getTotalRecords(){
		return $this->total_records;
	}
}
?>
