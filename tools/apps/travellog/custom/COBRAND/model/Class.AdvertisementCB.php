<?php
/**
 * Created on Aug. 14, 2007
 * @author Czarisse Daphne P. Dolina
 * Purpose: Show rotating ads on website
 */
 
require_once("Class.Connection.php");
require_once("Class.Recordset.php");
require_once('travellog/model/Class.Advertisement.php');
	 
class AdvertisementCB extends Advertisement{
	
	public static function getRandom($offset){
		
		$CONFIG = $GLOBALS['CONFIG'];
		$cID = $CONFIG->getClientID();
		$clientClause = "";
		
		try {
            $conn = new Connection();
            $rs   = new Recordset($conn);
        }
        catch (Exception $e) {                 
           throw $e;
        }
        
        if (self::IsAdvertiser())
        	$clientClause = "AND GoAbroad_Main.ads.clientID = " . $cID ;
        	
		$sql = "SELECT tblAdvertisement.*, GoAbroad_Main.ads.filename, GoAbroad_Main.ads.alt, GoAbroad_Main.ads.href, GoAbroad_Main.ads.clientID " .
					"FROM Travel_Logs.tblAdvertisement, GoAbroad_Main.ads " .
					"WHERE Travel_Logs.tblAdvertisement.bannerID = GoAbroad_Main.ads.ad_ID " . $clientClause .
					" LIMIT $offset , 1 ";
			
		$rs->Execute($sql);
		
		if ($rs->RecordCount())
			$randomAdID  	= $rs->Result(0,'advertisementID');
		else
			$randomAdID	= 	0;
			
		return new Advertisement($randomAdID);
	}
	
	
	public static function getAllAds(){
		
		$CONFIG = $GLOBALS['CONFIG'];
		$cID = $CONFIG->getClientID();
		$clientClause = "";
		
		try {
            $conn = new Connection();
            $rs   = new Recordset($conn);
        }
        catch (Exception $e) {                 
           throw $e;
        }
        
        if (self::IsAdvertiser())
        	$clientClause = "AND GoAbroad_Main.ads.clientID = " . $cID ;
        	
		$sql = "SELECT * FROM Travel_Logs.tblAdvertisement, GoAbroad_Main.ads " .
				"WHERE Travel_Logs.tblAdvertisement.bannerID = GoAbroad_Main.ads.ad_ID " . $clientClause .
				" ORDER BY bannerID";
		$rs->Execute($sql);
	
		$arrAds = array();
		
		while ($recordset = mysql_fetch_array($rs->ResultSet())):
			try {
				$arrAds[] = new Advertisement($recordset['advertisementID']);
			}
			catch (Exception $e) {
               throw $e;
            }			
		endwhile;
		
		
		return $arrAds;
		
	}
	
	
	public static function IsAdvertiser(){
		
		$CONFIG = $GLOBALS['CONFIG'];
		$cID = $CONFIG->getClientID();
		
		try {
            $conn = new Connection();
            $rs   = new Recordset($conn);
        }
        catch (Exception $e) {                 
           throw $e;
        }
        
		$sql = "SELECT tblAdvertisement.advertisementID " .
					"FROM Travel_Logs.tblAdvertisement, GoAbroad_Main.ads " .
					"WHERE Travel_Logs.tblAdvertisement.bannerID = GoAbroad_Main.ads.ad_ID " .
					"AND GoAbroad_Main.ads.clientID = " . $cID . 
					" LIMIT 0 ,1 ";
		
		$rs->Execute($sql);	
		
		if ($rs->RecordCount())
			return true;
		else
			return false;
	}
	
	static function getAd($adID = 0) {
		
		$rand_idx = rand(0, count(self::getAllAds()) -1);
		
		$ad = self::getRandom($rand_idx);
		
		return $ad;
	}
	
	
}

?>
