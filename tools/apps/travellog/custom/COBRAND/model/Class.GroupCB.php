<?php
	require_once('travellog/model/Class.Group.php');

	class GroupCB extends Group {
		function getGroupMembershipLink($referer=''){			
			$strLink = '';
			$refLink = '';

			// check if logged in
			if (isset($_SESSION['travelerID'])) {
				$loggedUser = new Traveler($_SESSION['travelerID']);

				//check if not advsor and this is an open or closed group
				if (!$loggedUser->isAdvisor() && $loggedUser->getTravelerID() != $this->getAdministrator()->getTravelerID()){
					//create ref link if referer paramater is passed, string to be added to URL
					if (strlen($referer))
						$refLink = "&amp;ref=". $referer;
					
					if (in_array($this->getGroupAccess(), array(GroupAccess::$OPEN, GroupAccess::$OPEN_APPROVAL))) {						
						// check if is in request list already
						if ($this->isInRequestList($loggedUser))
							$strLink = '<a href="confirm.php?mode=cancel&amp;gID='.$this->mGroupID. $refLink . '">Cancel Request to Join</a>';
						//if member already
						elseif (!$this->isMember($loggedUser))
								$strLink = '<a href="join.php?gID='.$this->mGroupID.'">Join this ' .$this->getLabel() . '</a>';
					} 	
					//if not yet member
					if ($this->isMember($loggedUser))
						$strLink = '<a href="confirm.php?mode=leave&amp;gID='.$this->mGroupID.'">Leave ' . $this->getLabel() .'</a>';					
				}
			}

			return $strLink;			
		}

		public static function getFilteredGroups($filter = null, $rowslimit = null, $order = null, $groupsql = null, $loggeduserID = null){
			$arrGroup = array(); 			
			$gID = $GLOBALS['CONFIG']->getGroupID();

 			try {
	 			$mConn	= new Connection();
				$mRs	= new Recordset($mConn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}			

			$condition	= array();
			$filterstr	= ''; 
			$limitstr	= ''; 
			$orderstr	= ''; 
			$groupbystr	= '';

			if (NULL != $filter){
			   	$condition = $filter->getConditions();	
				$booleanop = $filter->getBooleanOps();

				for($i=0;$i<count($condition);$i++){
					$attname   = $condition[$i]->getAttributeName();                    			
					$value 	   = $condition[$i]->getValue();

					switch($condition[$i]->getOperation()){

						case FilterOp::$EQUAL:
							$operator = " = ";
							break;
						case FilterOp::$NOT_EQUAL:
							$operator = " <> ";
							break;
						case FilterOp::$GREATER_THAN_OR_EQUAL:
							$operator = " >= ";
							break;
						case FilterOp::$LIKE:
							$operator = " LIKE ";
							$value    = "%" . $value . "%"; 
							break;
						default:
							$operator = " = ";
							break;
					}
					$filterstr .=  " " . $booleanop[$i] . " ". $attname . $operator . " '$value' ";
				}
			}

			if (NULL != $order){
				$condition = $order->getConditions();
				$attname   = $condition[0]->getAttributeName();                    			

				switch($condition[0]->getOperation()){
					case FilterOp::$ORDER_BY:
						$sortorder = " ASC ";
						break;
					case FilterOp::$ORDER_BY_DESC:
						$sortorder = " DESC ";
						break;
					default:
						$sortorder = " ASC ";
						break;
				}
				$orderstr = " ORDER BY " . $attname . $sortorder;
			}

			if (NULL != $rowslimit){
				$offset = $rowslimit->getOffset();
				$rows =  $rowslimit->getRows();
				$limitstr = " 	LIMIT " . $offset . " , " . $rows;
			}	

			if (NULL != $groupsql){
				$condition = $groupsql->getConditions();	
				$attname   = $condition[0]->getAttributeName();   
				$groupbystr = " GROUP BY " . $attname ;
			}

	 		if (NULL != $loggeduserID){
	 			$sql = "SELECT DISTINCT tblGroup.groupID " .
						"FROM tblGroup, tblGrouptoTraveler, tblTraveler " .
	 					"WHERE tblGroup.groupID = tblGrouptoTraveler.groupID " .
						"AND tblTraveler.travelerID = tblGroup.administrator " .
	 					"AND tblGrouptoTraveler.travelerID = " . $loggeduserID . " " .
						"AND tblTraveler.active = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
	 					"AND tblGroup.isSuspended =  0 " .
	 					"AND tblGroup.parentID = " . $gID .
	 					 $filterstr . $groupbystr .
	 					 $orderstr . $limitstr;
			}
	 		else{
	 			$sql = "SELECT DISTINCT tblGroup.groupID " .
	 					"FROM tblGroup, tblTraveler " .
	 					"WHERE tblTraveler.travelerID = tblGroup.administrator " .
						"AND tblTraveler.active = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblGroup.isSuspended =  0 " . 
						"AND tblGroup.parentID = " . $gID .
	 					$filterstr . $groupbystr .
	 					$orderstr . $limitstr;
			}
	 		$mRs->Execute($sql);
			
			$arrGroupId = array();
	 		while ($recordset = mysql_fetch_array($mRs->Resultset()))
				$arrGroupId[] = $recordset['groupID'];
			$arrGroup = GroupFactory::instance()->create($arrGroupId);

			return $arrGroup;				
 		}
	}		
?>