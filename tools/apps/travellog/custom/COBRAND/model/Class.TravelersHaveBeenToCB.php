<?php
class TravelersHaveBeenToCB{ 
	
	private $total_records = 0;
	
	/********************************************************
	 * edits of neri to GetFilterList():
	 * 		deleted online attribute from query: 	12-04-08
	 * 		filtered retrieved data: 				12-17-08
	 ********************************************************/
	
	function GetFilterList($props){
		global $CONFIG;
		
		require_once('travellog/model/Class.Traveler.php');
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn = new Connection;
		$rs1  = new Recordset( $conn );
		$rs2  = clone($rs1);
		$arr  = array();
		
		/*$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS travelerID, username, trim(loginTime) AS isOnline FROM ' .
						'( ' .
							'SELECT tblTraveler.travelerID, username, ranking ' .
							'FROM tblTraveler, tblTravelerRanking, tblGrouptoTraveler, SocialApplication.tbUser, SocialApplication.tbTravelWish, SocialApplication.tbTravelWishToCountry, GoAbroad_Main.tbcountry ' .
							'WHERE ' .
							'tblTraveler.travelerID                           = tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID                = tblGrouptoTraveler.travelerID ' .
							'AND tblGrouptoTraveler.groupID                   = %d ' .
							'AND tblTravelerRanking.travelerID                = SocialApplication.tbUser.travelerID ' .
							'AND SocialApplication.tbUser.userID                    = SocialApplication.tbTravelWish.userID ' .
							'AND SocialApplication.tbTravelWish.travelWishID        = SocialApplication.tbTravelWishToCountry.travelWishID ' .
							'AND SocialApplication.tbTravelWishToCountry.startdate >= NOW() ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  = GoAbroad_Main.tbcountry.countryID ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  = %d ' .
							'AND tblTraveler.active                           > 0 ' .
							'AND tblTraveler.travelerID                  NOT IN (SELECT travelerID FROM tblGrouptoAdvisor) ' .  
							'UNION ' .
							'SELECT tblTraveler.travelerID, username, ranking ' .
							'FROM ' .
							'tblTraveler, tblTravelerRanking, tblGrouptoTraveler, tblTravel, tblTrips ' .
							'WHERE ' .
							'tblTraveler.travelerID            = tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID = tblGrouptoTraveler.travelerID ' .
							'AND tblGrouptoTraveler.groupID    = %d ' .
							'AND tblTravelerRanking.travelerID = tblTravel.travelerID ' .
							'AND tblTravel.travelID            = tblTrips.travelID ' .
							'AND arrival                       < NOW() ' .
							'AND (tblTrips.locID               IN (%s) OR tblTraveler.currlocationID IN (%s)) ' .
							'AND tblTraveler.active            > 0 ' .
							'AND tblTraveler.travelerID   NOT IN (SELECT travelerID FROM tblGrouptoAdvisor) ' .
						') ' .
						'qryHaveBeenTo LEFT JOIN tblSessions ON (tblSessions.session_user_id = qryHaveBeenTo.travelerID AND `domain` = \'' . $GLOBALS['CONFIG']->getServerName() . '\') ' .
						'WHERE 1 = 1 ' . 
						'%s ' 
						,$CONFIG->getGroupID()
						,$props['countryID']
						,$CONFIG->getGroupID()
						,$props['locationIDList'] 
						,$props['locationIDList']
						,$props['obj_criteria']->getCriteria2('AND')
					);*/
		
		/*$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS travelerID, username FROM ' .
						'( ' .
							'SELECT tblTraveler.travelerID, username, ranking ' .
							'FROM tblTraveler, tblTravelerRanking, tblGrouptoTraveler, SocialApplication.tbUser, SocialApplication.tbTravelWish, SocialApplication.tbTravelWishToCountry, GoAbroad_Main.tbcountry ' .
							'WHERE ' .
							'tblTraveler.travelerID                           = tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID                = tblGrouptoTraveler.travelerID ' .
							'AND tblGrouptoTraveler.groupID                   = %d ' .
							'AND tblTravelerRanking.travelerID                = SocialApplication.tbUser.travelerID ' .
							'AND SocialApplication.tbUser.userID                    = SocialApplication.tbTravelWish.userID ' .
							'AND SocialApplication.tbTravelWish.travelWishID        = SocialApplication.tbTravelWishToCountry.travelWishID ' .
							'AND SocialApplication.tbTravelWishToCountry.startdate >= NOW() ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  = GoAbroad_Main.tbcountry.countryID ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  = %d ' .
							'AND tblTraveler.active                           > 0 ' .
							'AND tblTraveler.travelerID                  NOT IN (SELECT travelerID FROM tblGrouptoAdvisor) ' .  
							'UNION ' .
							'SELECT tblTraveler.travelerID, username, ranking ' .
							'FROM ' .
							'tblTraveler, tblTravelerRanking, tblGrouptoTraveler, tblTravel, tblTrips ' .
							'WHERE ' .
							'tblTraveler.travelerID            = tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID = tblGrouptoTraveler.travelerID ' .
							'AND tblGrouptoTraveler.groupID    = %d ' .
							'AND tblTravelerRanking.travelerID = tblTravel.travelerID ' .
							'AND tblTravel.travelID            = tblTrips.travelID ' .
							'AND arrival                       < NOW() ' .
							'AND (tblTrips.locID               IN (%s) OR tblTraveler.currlocationID IN (%s)) ' .
							'AND tblTraveler.active            > 0 ' .
							'AND tblTraveler.travelerID   NOT IN (SELECT travelerID FROM tblGrouptoAdvisor) ' .
						') ' .
						'qryHaveBeenTo ' .
						'WHERE 1 = 1 ' . 
						'%s ' 
						,$CONFIG->getGroupID()
						,$props['countryID']
						,$CONFIG->getGroupID()
						,$props['locationIDList'] 
						,$props['locationIDList']
						,$props['obj_criteria']->getCriteria2('AND')
					);*/
		$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS travelerID, username FROM ' .
						'( ' .
							'SELECT tblTraveler.travelerID, username, ranking ' .
							'FROM tblTraveler, tblTravelerRanking, tblGrouptoTraveler, FacebookApp.tbUser, FacebookApp.tbTravelWish, FacebookApp.tbTravelWishToCountry, GoAbroad_Main.tbcountry ' .
							'WHERE ' .
							'tblTraveler.travelerID                           = tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID                = tblGrouptoTraveler.travelerID ' .
							'AND tblGrouptoTraveler.groupID                   = %d ' .
							'AND tblTravelerRanking.travelerID                = FacebookApp.tbUser.travelerID ' .
							'AND FacebookApp.tbUser.userID                    = FacebookApp.tbTravelWish.userID ' .
							'AND FacebookApp.tbTravelWish.travelWishID        = FacebookApp.tbTravelWishToCountry.travelWishID ' .
							'AND FacebookApp.tbTravelWishToCountry.startdate >= NOW() ' .
							'AND FacebookApp.tbTravelWishToCountry.countryID  = GoAbroad_Main.tbcountry.countryID ' .
							'AND FacebookApp.tbTravelWishToCountry.countryID  = %d ' .
							'AND tblTraveler.active                           > 0 ' .
							'UNION ' .
							'SELECT tblTraveler.travelerID, username, ranking ' .
							'FROM ' .
							'tblTraveler, tblTravelerRanking, tblGrouptoTraveler, tblTravel, tblTrips ' .
							'WHERE ' .
							'tblTraveler.travelerID            = tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID = tblGrouptoTraveler.travelerID ' .
							'AND tblGrouptoTraveler.groupID    = %d ' .
							'AND tblTravelerRanking.travelerID = tblTravel.travelerID ' .
							'AND tblTravel.travelID            = tblTrips.travelID ' .
							'AND arrival                       < NOW() ' .
							'AND (tblTrips.locID               IN (%s) OR tblTraveler.currlocationID IN (%s)) ' .
							'AND tblTraveler.active            > 0 ' .
						') ' .
						'qryHaveBeenTo ' .
						'WHERE 1 = 1 ' . 
						'%s ' 
						,$CONFIG->getGroupID()
						,$props['countryID']
						,$CONFIG->getGroupID()
						,$props['locationIDList'] 
						,$props['locationIDList']
						,$props['obj_criteria']->getCriteria2('AND')
					);
					
		$rs1->Execute($sql1);
		if( $rs1->Recordcount() ){
			
			$sql2 = 'SELECT FOUND_ROWS() AS totalrecords';
			$rs2->Execute($sql2);
			$this->total_records = $rs2->Result(0, "totalrecords");
			
			while($row = mysql_fetch_assoc($rs1->Resultset())){
				/*$obj_traveler = new Traveler;
				$obj_traveler->setTravelerID( $row['travelerID']        );
				$obj_traveler->setUsername  ( $row['username']          );	
				$obj_traveler->setOnline    ( $row['isOnline'] ? 1 : 0 );*/
				
				$obj_traveler = new Traveler($row['travelerID']);
				
				if (!$obj_traveler->isBlocked($props['travelerID']) && !$obj_traveler->isSuspended())		
					$arr[] = $obj_traveler; 
			}	
		} 
		return $arr;
	}
	
	function GetTravelersCountryWithPastTravels(){
		global $CONFIG;
		
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn         = new Connection;
		$rs1          = new Recordset( $conn );
		$arr_contents = array();
		$sql1 = sprintf
					(
						'SELECT locID, country, qryTravelersPastTravels.travelerID FROM ' . 
						'(' .
							'SELECT GoAbroad_Main.tbcountry.locID, country, tblTraveler.travelerID ' . 
							'FROM tblTraveler, tblTravelerRanking, SocialApplication.tbUser, SocialApplication.tbTravelWish, SocialApplication.tbTravelWishToCountry, GoAbroad_Main.tbcountry ' .
							'WHERE ' .
							'tblTraveler.travelerID                           = tblTravelerRanking.travelerID ' .
							'AND tblTravelerRanking.travelerID                = SocialApplication.tbUser.travelerID ' .
							'AND SocialApplication.tbUser.userID                    = SocialApplication.tbTravelWish.userID ' .
							'AND SocialApplication.tbTravelWish.travelWishID        = SocialApplication.tbTravelWishToCountry.travelWishID ' .
							'AND SocialApplication.tbTravelWishToCountry.startdate >= NOW() ' .
							'AND SocialApplication.tbTravelWishToCountry.countryID  = GoAbroad_Main.tbcountry.countryID ' .
							'AND tblTraveler.active                           > 0 ' .
							'AND tblTraveler.travelerID                  NOT IN (SELECT travelerID FROM tblGrouptoAdvisor) ' .
							'UNION ' .
							'SELECT locID, country, qryLocations.travelerID FROM ' .  
							'( ' .
								'SELECT GoAbroad_Main.tbcountry.locID, country, tblTraveler.travelerID ' .
								'FROM ' .
								'tblTraveler, tblTravelerRanking, tblTravel, tblTrips, GoAbroad_Main.tbcountry ' .
								'WHERE ' .
								'tblTraveler.travelerID            = tblTravelerRanking.travelerID ' .
								'AND tblTravelerRanking.travelerID = tblTravel.travelerID ' .
								'AND tblTravel.travelID            = tblTrips.travelID ' .
								'AND arrival                       < NOW() ' .
								'AND tblTrips.locID                = GoAbroad_Main.tbcountry.locID ' .
								'AND tblTraveler.active            > 0 ' .
								'AND tblTraveler.travelerID   NOT IN (SELECT travelerID FROM tblGrouptoAdvisor) ' .
								'UNION ' .
								'SELECT GoAbroad_Main.tbcountry.locID, country, tblTraveler.travelerID ' .
								'FROM ' .
								'tblTraveler, tblTravelerRanking, tblTravel, tblTrips, GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry ' .
								'WHERE ' .
								'tblTraveler.travelerID             = tblTravelerRanking.travelerID ' .
								'AND tblTravelerRanking.travelerID  = tblTravel.travelerID ' .
								'AND tblTravel.travelID             = tblTrips.travelID ' .
								'AND arrival                        < NOW() ' .
								'AND tblTrips.locID                 = GoAbroad_Main.tbcity.locID ' .
								'AND GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID ' . 
								'AND tblTraveler.active             > 0 ' .
								'AND tblTraveler.travelerID   NOT IN (SELECT travelerID FROM tblGrouptoAdvisor) ' .
							') ' .
							'qryLocations ' .
						') ' .
						'qryTravelersPastTravels, tblGrouptoTraveler ' .
						'WHERE ' .
						'qryTravelersPastTravels.travelerID = tblGrouptoTraveler.travelerID ' . 	
						'AND tblGrouptoTraveler.groupID     = %d ' .
						'GROUP BY locID ' .
						'ORDER BY country ' 
						,$CONFIG->getGroupID()
					); 
		
		
		$rs1->Execute($sql1);
		
		if( $rs1->Recordcount() ){
			$arr_contents[] = array('locationID' => 0, 'name' => '- select a country -');
			while( $row = mysql_fetch_assoc($rs1->Resultset()) ){
				$arr_contents[] = array('locationID' => $row['locID'], 'name' => $row['country']); 
			}
		}
		return $arr_contents;
	} 
	
	function GetTravelersCountryWithPastTravelsJSON(){
		require_once('JSON.php');
		$json     = new Services_JSON(SERVICES_JSON_IN_ARR);
		$contents = $json->encode( $this->GetTravelersCountryWithPastTravels() );
		return $contents;  
	}
	
	function getTotalRecords(){
		return $this->total_records;
	}
}
?>
