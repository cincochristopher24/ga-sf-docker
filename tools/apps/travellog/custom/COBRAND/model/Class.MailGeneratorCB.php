<?php
	/**
	 * Class.MailGeneratorB.php
	 * created on April 22, 08
	 */
	 
	require_once 'travellog/model/Class.MailGenerator.php';
	
	class MailGeneratorCB extends MailGenerator{ 
	
		function __construct($travelerID,$messageType,Config $config){
	 		
	 		$conn = new Connection();
	 		$rs = new Recordset($conn);
	 		
	 		$this->from = $config->getGanetAdminEmail();
	 		$this->fromName = $config->getEmailName();
	 		$this->messageType = $messageType;
	 		$this->replyTo = $config->getAdminEmail();
	
	 		$sqlquery = "SELECT firstname,lastname,email,username,password " .
	 				"FROM tblTraveler, tblGrouptoTraveler " .
	 				"WHERE tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
	 				"AND tblTraveler.travelerID = $travelerID " .
	 				"AND tblGrouptoTraveler.groupID = {$config->getGroupID()}";
	 				
	 		$query = $rs->execute($sqlquery);
	 		if( $tmpRow = mysql_fetch_array($query) ){
	 			$this->sendTo =  $tmpRow["firstname"] . " " . $tmpRow["lastname"];
				$this->email = $tmpRow["email"];	
				
				if( in_array($messageType,array(ACCOUNT_ACTIVATION,RESEND_ACTIVATION_EMAIL,FORGOT_PASSWORD)) ){
					// encrypt travelerID for confirmation
					require_once("Class.Crypt.php");
					$crypt=  new Crypt();
					$cryptTravID = $crypt->encrypt($travelerID);
					$cryptTravID = urlencode(substr($cryptTravID,0,strlen($cryptTravID)-1));
				}
				
				switch($messageType){
					case 0:
						
						$varurl = "http://" . $config->getServerName() . "/login.php?ref=" . $cryptTravID . "";
						
						$this->subject = "Please complete your registration on {$config->getSiteName()} Network";
						$this->message = "Dear $this->sendTo,

You recently signed up for the {$config->getSiteName()} Network using this email address.

To complete the registration process, please click on the link below:

{$varurl}

Thank you.

Happy travels,

{$config->getEmailName()}
---
If you did not recently sign up for {$config->getSiteName()} Network, or believe that you got 
this email by mistake, you may disregard this message. However, receiving this email means that 
someone recently created a {$config->getSiteName()} Network account and entered your email address 
while doing so. If you believe this is the case, don't worry because they won't be able to complete 
their registration."; 
						break;
						
					case 2:
						$this->from = $this->email;
						$this->fromName = $this->sendTo;
						$this->replyTo = $this->email;
						$this->email = "marc@goabroad.com";
						break;	
					
					case 3:
						$this->subject = "Welcome to the {$config->getSiteName()} Network";
						$this->message = "Dear $this->sendTo, 

Welcome to the {$config->getSiteName()} Network!

Thank you for creating an account.

";

	$this->message .= "Now, you're a part of our worldwide network of travelers. As a member, you will be able to

    * meet and be friends with travelers from all over the world
    * blog about your travels in your very own travel journals
    * post and share travel photos
    * post messages for your friends and groupmates
    * search for programs for your travels abroad
    * and lots of other features rolling out in the following weeks!

";
    
$this->message .= "You can start by editing your profile or you might want to check out who's already on {$config->getSiteName()} Network. Click on the following link to Login:
http://{$config->getServerName()}/login.php
For more information, please check out the Frequently Asked Questions by clicking on the following link:
http://{$config->getServerName()}/faq.php  
	
Happy travels,

{$config->getEmailName()}";
						break;	
					
					case 4:
						
						$this->subject = "You requested an activation email from {$config->getSiteName()} Network";
						$this->message = "Hi! 

You recently created an account on {$config->getSiteName()} Network, and we are sorry you are having trouble activating it. 

To activate your account, please click on the link below: 
		
http://{$config->getServerName()}/login.php?ref=" . $cryptTravID . " 

Thank you for your patience. 

Happy travels, 
		
{$config->getEmailName()}

---
If you did not recently sign up for {$config->getSiteName()} Network, or believe that you got 
this email by mistake, you may disregard this message. However, receiving this email means that 
someone recently created a {$config->getSiteName()} Network account and entered your email address 
while doing so. If you believe this is the case, don't worry because they won't be able to complete 
their registration."; 
						break;	
					
					case 5:
						
						$this->subject = "{$config->getSiteName()} Password Recovery";
						$this->message = "Dear $this->sendTo,

We apologize for the inconvenience, but for your security, you will be unable to retrieve your old password. Instead, you will need to change it by clicking on the link below:
http://{$config->getServerName()}/changepassword.php?ref=" . $cryptTravID . "&action=entry

We appreciate your patience, and thank you for using {$config->getSiteName()} Network.

{$config->getEmailName()}"; 
						break;	
				}
	 		} 
	 	
	 	}
	 	
	 	function getMessage(){
	 		return $this->message;
	 	}
	}
?>
