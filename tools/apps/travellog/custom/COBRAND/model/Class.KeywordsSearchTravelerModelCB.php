<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once("Class.Criteria2.php");
require_once('travellog/model/Class.IModel.php');
class KeywordsSearchTravelerModelCB implements IModel{
	function GetFilterLists(&$props){
		
		/***************************************************************************
		 * edits by neri: 	
		 * 	 	if traveler is logged in, set travelerID as 2nd parameter
		 * 			in performSearch() and getAllTravelers(): 			11-28-08
		 * 		excluded interest in categories of search by:			12-03-08
		 * 		added action=search in query string for paging			12-05-08 
		 * 		added addslashes to keyword parameter if
		 * 			search_by = 1										12-17-08
		 ***************************************************************************/ 
		
		global $CONFIG;
		
		$obj_criteria = new Criteria2;
		$obj_criteria->setLimit($props['offset'], $props['rows']);
		
		$obj_criteria->mustBeEqual('tblTraveler.travelerID'    , 'tblGrouptoTraveler.travelerID');
		$obj_criteria->mustBeEqual('tblGrouptoTraveler.groupID' , $CONFIG->getGroupID());	
		
		if(isset($props['keywords'])) $props['keywords'] = trim($props['keywords']);
		
		if( $props['search_by'] == 1 ){
			$obj_criteria->setOrderBy( 'username' );
			$obj_criteria->mustBeLike( 'username', addslashes($props['keywords']) );
		}
		elseif( $props['search_by'] == 2 ){
			$obj_criteria->setOrderBy( 'email' );
			$obj_criteria->mustBeLike( 'email', $props['keywords'] );
		}
		/*elseif( $props['search_by'] == 3 ){
			$obj_criteria->setOrderBy( 'interests' );
			$obj_criteria->mustBeLike( 'interests', $props['keywords'] );
		}*/
		
		if( $props['search_by'] == 4 ){
			require_once('travellog/custom/COBRAND/model/Class.SearchFirstandLastnameServiceCB.php');
			$obj_traveler  = new SearchFirstandLastnameServiceCB;
			
			if ($props['is_login']) 		
				$props['travelers'] = $obj_traveler->performSearch($props['keywords'], $props['travelerID']);	
			else
				$props['travelers'] = $obj_traveler->performSearch($props['keywords']);
			$props['travelers_count']  = count($props['travelers']);  	
		}
		else{
			require_once('travellog/custom/COBRAND/model/Class.TravelerSearchCriteriaCB.php');
			$obj_traveler  = new TravelerSearchCriteriaCB;
			
			if ($props['is_login']) 		
				$props['travelers'] = $obj_traveler->getAllTravelers($obj_criteria, $props['travelerID']);			
			else
				$props['travelers'] = $obj_traveler->getAllTravelers($obj_criteria); 
			$props['travelers_count']  = $obj_traveler->getTotalRecords();  
		}
		
		$props['obj_paging'] = new Paging($props['travelers_count'] , $props['page'],'action=search&keywords='.$props['keywords'].'&searchby='.$props['search_by'], $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                                                                    );
	}
}
?>
