<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/custom/COBRAND/model/Class.TravelerCB.php');
require_once("travellog/model/Class.FilterCriteria2.php");
require_once("travellog/model/Class.Condition.php");
require_once('travellog/model/Class.IModel.php');
class CurrentTravelerModelCB implements IModel{
	function GetFilterLists(&$props){
		$obj_criteria = NULL;
		$arr          = array();
		$obj_condition = new Condition;
		$obj_condition->setAttributeName("currlocationID");
		$obj_condition->setOperation(FilterOp::$IN);
		$obj_condition->setValue( $props['locationIDList'] ); 
		
		$obj_criteria = new FilterCriteria2();
		$obj_criteria->addBooleanOp('AND');
		$obj_criteria->addCondition( $obj_condition );
		
		/************************************************************
		 * edits by neri: 	11-28-08
		 * 	 	if traveler is logged in, set travelerID 
		 ************************************************************/ 
		
		if( $props['is_login'] ) 		
			$props['travelers'] = TravelerCB::getAllTravelers( $obj_criteria, NULL, $props['travelerID'] );
		else
			$props['travelers'] = TravelerCB::getAllTravelers( $obj_criteria );
		
		$props['travelers_count'] = count($props['travelers']);
		$props['obj_paging']    = new Paging        ($props['travelers_count'], $props['page'],'action=CurrentlyIn&locationID='.$props['locationID'], $props['rows'] );
		//$props['obj_iterator']  = new ObjectIterator( $col_travelers, $props['obj_paging']                                                                    );
		$props['view_type']     = 6;
	}
	
	function GetTravelersCountryWithCurrentLocation(){
		global $CONFIG;   
		
		require_once("Class.Connection.php");
		require_once("Class.Recordset.php");
		$conn          = new Connection();
		$rs            = new Recordset($conn);
		$arr_contents  = array();
		$sql  = 'SELECT locID, country FROM ' .
				'( ' .
					'SELECT GoAbroad_Main.tbcountry.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler, tblGrouptoTraveler, GoAbroad_Main.tbcountry ' .
					'WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcountry.locID ' .
					'AND tblTraveler.travelerID       = tblGrouptoTraveler.travelerID ' .
					'AND tblGrouptoTraveler.groupID   = ' . $CONFIG->getGroupID() . ' ' .
					'AND tblTraveler.travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
					'AND GoAbroad_Main.tbcountry.locID > 0 ' .
					'GROUP BY GoAbroad_Main.tbcountry.locID ' .
					'UNION '.
					'SELECT GoAbroad_Main.tbcountry.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler, tblGrouptoTraveler, GoAbroad_Main.tbcountry, GoAbroad_Main.tbcity ' .
					'WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcity.locID ' .
					'AND tblTraveler.travelerID       = tblGrouptoTraveler.travelerID ' .
					'AND tblGrouptoTraveler.groupID   = ' . $CONFIG->getGroupID() . ' ' .
					'AND GoAbroad_Main.tbcountry.countryID = GoAbroad_Main.tbcity.countryID '.
					'AND GoAbroad_Main.tbcity.locID > 0 ' .
					'AND tblTraveler.travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
					'UNION '.
					'SELECT GoAbroad_Main.tbcountry.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler, tblGrouptoTraveler, GoAbroad_Main.tbcountry, GoAbroad_Main.tbstate ' .
					'WHERE tblTraveler.currlocationID = GoAbroad_Main.tbstate.locID ' .
					'AND tblTraveler.travelerID 	  = tblGrouptoTraveler.travelerID ' .
					'AND tblGrouptoTraveler.groupID   = ' . $CONFIG->getGroupID() . ' ' .
					'AND GoAbroad_Main.tbcountry.countryID = 91 ' .
					'AND GoAbroad_Main.tbstate.locID > 0 ' .
					'AND tblTraveler.travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
				') ' .
				'qryTravelersInLocation ' . 
				'GROUP BY locID ' .
				'ORDER BY country';
		$rs->Execute($sql);
		
		if ($rs->Recordcount()){
			$arr_contents[] = array('locationID' => 0, 'name' => '- select a country -');
			while($row = mysql_fetch_assoc($rs->Resultset())){
				if( $row['locID'] > 0){
					$arr_contents[] = array('locationID' => $row['locID'], 'name' => $row['country']); 
				}
			}
		}	
		
		return $arr_contents;
	}
	
	function GetCountriesWithCurrentLocationJSON(){
		require_once('JSON.php');
		$json     = new Services_JSON(SERVICES_JSON_IN_ARR);
		$contents = $json->encode( $this->GetTravelersCountryWithCurrentLocation() );
		return $contents;   
		
		/*require_once("Class.Connection.php");
		require_once("Class.Recordset.php");
		require_once('JSON.php');
		$conn          = new Connection();
		$rs            = new Recordset($conn);
		$arr_contents  = array();
		$json          = new Services_JSON(SERVICES_JSON_IN_ARR);
		$sql  = 'SELECT locID, country FROM ' .
				'( ' .
					'SELECT GoAbroad_Main.tbcountry.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler,GoAbroad_Main.tbcountry ' .
					'WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcountry.locID ' .
					'AND travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
					'AND GoAbroad_Main.tbcountry.locID > 0 ' .
					'GROUP BY GoAbroad_Main.tbcountry.locID ' .
					'UNION '.
					'SELECT GoAbroad_Main.tbcountry.locID,GoAbroad_Main.tbcountry.country FROM tblTraveler,GoAbroad_Main.tbcountry,GoAbroad_Main.tbcity ' .
					'WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcity.locID ' .
					'AND GoAbroad_Main.tbcountry.countryID = GoAbroad_Main.tbcity.countryID '.
					'AND GoAbroad_Main.tbcity.locID > 0 ' .
					'AND travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
				') ' .
				'qryTravelersInLocation ' . 
				'GROUP BY locID ' .
				'ORDER BY country';
		$rs->Execute($sql);
		if ($rs->Recordcount()){
			while($row = mysql_fetch_assoc($rs->Resultset())){
				if( $row['locID'] > 0){
					$arr_contents[] = array('locationID' => $row['locID'], 'name' => $row['country']); 
				}
			}
		}	
		$contents = $json->encode( $arr_contents );
		return $contents;*/
	}
}
?>
