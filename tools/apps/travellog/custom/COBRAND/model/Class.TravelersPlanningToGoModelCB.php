<?php
require_once('travellog/custom/COBRAND/model/Class.TravelersPlanningToGoCB.php');
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/model/Class.IModel.php');
require_once("Class.Criteria2.php");
class TravelersPlanningToGoModelCB implements IModel{
	function GetFilterLists(&$props){
		global $CONFIG;
		
		$props['obj_criteria'] = new Criteria2;
		if( $props['is_login'] ) $props['obj_criteria']->mustNotBeEqual('qryTravelers.travelerID', $props['travelerID']);
		
		$props['obj_criteria']->mustBeEqual('qryTravelers.travelerID'    , 'tblGrouptoTraveler.travelerID');
		$props['obj_criteria']->mustBeEqual('tblGrouptoTraveler.groupID' , $CONFIG->getGroupID()          );
		
		$props['obj_criteria']->setGroupBy('qryTravelers.travelerID');
		$props['obj_criteria']->setOrderBy('ranking, qryTravelers.travelerID');
		$props['obj_criteria']->setLimit($props['offset'], $props['rows']);
		$obj_traveler = new TravelersPlanningToGoCB;	
		$props['travelers'] = $obj_traveler->GetFilterList($props);
		$props['travelers_count'] = $obj_traveler->getTotalRecords();
		$props['obj_paging']   = new Paging        ($props['travelers_count'], $props['page'],'action=PlanningToGo', $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                                    );
		$props['view_type']    = 7;
	}
	
	function GetTravelersCountryWithFutureTravels(){
		$obj_traveler = new TravelersPlanningToGoCB;	
		return $obj_traveler->GetTravelersCountryWithFutureTravels();
	}
	
	function GetTravelersCountryWithFutureTravelsJSON(){
		$obj_traveler = new TravelersPlanningToGoCB;	
		return $obj_traveler->GetTravelersCountryWithFutureTravelsJSON();
	}
}
?>
