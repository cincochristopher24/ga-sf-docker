<?php
require_once('travellog/model/Class.TravelLog.php');

class TravelLogCB extends TravelLog{
	static function getCountriesWithLogs( $groupID ){
		$gIDs    = self::getAdminSubGroups  ( $groupID );	
		$travIDs = self::getGroupsToTraveler( $gIDs    );
		$arr     = array();
		$conn    = new Connection();
		$rs      = new Recordset($conn);		
		$sql     = sprintf(
							'SELECT countryID FROM tblTravel, tblTrips, tblTravelLog, GoAbroad_Main.tbcountry, tblGroupApprovedJournals, tblGrouptoTraveler, tblGroup
							 WHERE 
							 	 tblTravel.travelID   =  tblTrips.travelID
							 AND tblTrips.tripID    =  tblTravelLog.tripID
							 AND tblTravelLog.deleted = 0 
							 AND tblTravel.deleted = 0 
							 AND tblTravel.travelerID = tblGrouptoTraveler.travelerID
							 AND tblGrouptoTraveler.groupID = tblGroup.groupID
							 AND (tblGroupApprovedJournals.groupID = tblGroup.groupID OR tblGroupApprovedJournals.groupID = tblGroup.parentID)
							 AND tblGroupApprovedJournals.travelID = tblTravel.travelID
							 AND tblGroupApprovedJournals.approved = 1 
							 AND tblTrips.locID       =  GoAbroad_Main.tbcountry.locID
							 AND tblGroup.groupID = %d
							 GROUP BY countryID
							 UNION         
							 SELECT countryID FROM tblTravel, tblTrips, tblTravelLog, GoAbroad_Main.tbcity, tblGroupApprovedJournals, tblGrouptoTraveler, tblGroup
							 WHERE 
							 	 tblTravel.travelID   =  tblTrips.travelID
							 AND tblTrips.tripID    =  tblTravelLog.tripID
							 AND tblTravelLog.deleted = 0 
							 AND tblTravel.deleted = 0 
							 AND tblTravel.travelerID = tblGrouptoTraveler.travelerID
							 AND (tblGroupApprovedJournals.groupID = tblGroup.groupID OR tblGroupApprovedJournals.groupID = tblGroup.parentID)
							 AND tblGroupApprovedJournals.travelID = tblTravel.travelID
							 AND tblGroupApprovedJournals.approved = 1
							 AND tblTrips.locID       =  GoAbroad_Main.tbcity.locID
							 AND tblGroup.groupID = %d
							 GROUP BY countryID'
							 ,$groupID
							 ,$groupID
						  );
		$rs->Execute( $sql );
		
		if ($rs->Recordcount() == 0){
			return NULL;	 	
		}else {
			require_once("travellog/model/Class.Country.php");
			while ($row = mysql_fetch_array($rs->Resultset())) {
				try {
					$obj_country = new Country($row['countryID']);
					$arr[] = $obj_country;
				} catch (Exception $e) { }
			}
		}
		
		usort($arr,array("TravelLog","orderByCountryName"));
				
		return $arr;
	}
	
	private static function getAdminSubGroups( $groupID ){
		require_once("travellog/model/Class.AdminGroup.php");
		$obj_admin_group   = new AdminGroup( $groupID );
		$col_groups        = $obj_admin_group->getSubGroups();
		$col_sub_groupID   = array();
		$col_sub_groupID[] = $groupID;
		if( count($col_groups) ){
			foreach( $col_groups as $obj_group ){
				$col_sub_groupID[] = $obj_group->getGroupID();		
			}
		}
		return implode(',', $col_sub_groupID); 
	}
	
	private static function getGroupsToTraveler( $groupIDs ){
		$conn = new Connection();
		$rs   = new Recordset($conn);
		$arr  = array();
		$sql  = sprintf('SELECT travelerID FROM tblGrouptoTraveler WHERE groupID IN (%s)', $groupIDs);
		$rs->Execute( $sql );
		if( count( $rs->Resultset() ) ){
			while( $row = mysql_fetch_assoc( $rs->Resultset() ) ){
				$arr[] = $row['travelerID'];	
			}
		}
		return implode(',', $arr);
	}
}
?>
