<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once("travellog/model/Class.RowsLimit.php");
require_once('travellog/custom/COBRAND/model/Class.TravelerCB.php');
require_once('travellog/model/Class.IModel.php');
class NewestTravelerModelCB implements IModel{
	function GetFilterLists(&$props){
		$obj_rows_limit        = new RowsLimit($props['rows'], $props['offset']);
		
		if ($props['is_login'])
			$props['travelers']         = TravelerCB::getNewestMembers($obj_rows_limit, $props['travelerID']);				// neri added the travelerID as a parameter if traveler is logged in: 11-28-08
		else
			$props['travelers']        = TravelerCB::getNewestMembers($obj_rows_limit);
		
		$props['travelers_count'] = count($props['travelers']);
		$props['obj_paging']   = new Paging        ($props['travelers_count'] , $props['page'],'action=Newest', $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                            );
		$props['view_type']    = 3;
	}
}
?>
