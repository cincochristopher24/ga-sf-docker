<?php
require_once('travellog/custom/COBRAND/model/Class.TravelerCB.php');
require_once('travellog/model/Class.TravelerSearchCriteria2.php');

class TravelerSearchCriteriaCB extends TravelerSearchCriteria2{
	
	function performSearch(){
		$traveler_collection = TravelerCB::getAllTravelers();
		foreach( $traveler_collection as $traveler_object ){
			if ( $traveler_object->getCountTravelJournals() &&  $traveler_object->isProfileCompleted()){
				$this->arr_with_journal_and_completed_profile[] = $traveler_object;
			}elseif ( !$traveler_object->getCountTravelJournals() &&  !$traveler_object->isProfileCompleted()){
				$this->arr_no_journal_and_incomplete_profile[] = $traveler_object;
			}elseif ( $traveler_object->getCountTravelJournals() ){
				$this->arr_with_journal[] = $traveler_object;
			}elseif ( $traveler_object->isProfileCompleted() ){
				$this->arr_completed_profile[] = $traveler_object;
			}
		} 

		$array1 = $this->_sort($this->arr_with_journal_and_completed_profile);
		$array2 = $this->_sort($this->arr_with_journal);
		
		$results = array_merge( $array1, $array2, $this->arr_completed_profile, $this->arr_no_journal_and_incomplete_profile );
		return $results; 
	}

	/*********************************************************************
	 * edits by neri to getAllTravelers():
	 *		added loggedTravelerID so as not to display certain 
	 *			travelers' profiles to blocked users: 			11-28-08
	 *		modified way of setting the traveler's attributes 
	 *			and checked if traveler is suspended:	 		12-17-08	
	 *********************************************************************/

	function getAllTravelers($c, $loggedTravelerID = 0){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn  = new Connection;
		$rs    = new Recordset($conn);
		$rs1   = new Recordset($conn);
		$arr   = array();
		/*$sql  = 'SELECT SQL_CALC_FOUND_ROWS qryTravelers.travelerID,username,ranking, trim(logintime) AS isOnline ' .
				'FROM (qryTravelers, tblGrouptoTraveler) LEFT JOIN tblSessions ' .
				'ON ( qryTravelers.travelerID = tblSessions.session_user_id AND domain = "' . $GLOBALS['CONFIG']->getServerName() . '")  ' .
				'WHERE 1=1 AND qryTravelers.travelerID = tblGrouptoTraveler.travelerID '.$c->getCriteria('AND');*/
		
	/**	$sql  = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.travelerID, username , trim(tblSessions.loginTime) AS isOnline " .
				"FROM tblGrouptoTraveler " . 
				"INNER JOIN (tblTraveler LEFT JOIN tblSessions ON (tblSessions.session_user_id = tblTraveler.travelerID AND domain = '".$_SERVER['SERVER_NAME']."') ) " .
				"ON tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
				"WHERE 1 = 1 " .
				"AND tblTraveler.travelerID NOT IN (select distinct travelerID from tblGrouptoAdvisor) " . $c->getCriteria('AND'); 		
		*/
		
			// edited by daf; 26Nov2008; removed left join to tblSessions since online status of traveler no longer needed
			
			$sql  = "SELECT DISTINCT SQL_CALC_FOUND_ROWS tblTraveler.travelerID, username  " .
					"FROM tblGrouptoTraveler " . 
					"INNER JOIN tblTraveler " .
					"ON tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"WHERE 1 = 1 " .
					$c->getCriteria2('AND');
			
					
		$rs->Execute($sql);		
			   
		if ($rs->Recordcount()){
			$sql     = 'SELECT FOUND_ROWS() AS totalrecords';
			$results = $rs1->Execute($sql);
			$this->totalrecords = $rs1->Result(0, "totalrecords"); 
			while ($row = mysql_fetch_assoc($rs->Resultset())){
				/*$online = $row['isOnline']!=NULL ? 1 : 0;
				$objNewMember = new TravelerCB ();
				$objNewMember->setTravelerID( $row['travelerID'] );
				$objNewMember->setUserName  ( $row['username']   );
				$objNewMember->setOnLine    ( $online            );*/
				
				$objNewMember = new TravelerCB ($row['travelerID']);
				
				if (!$objNewMember->isSuspended()) {
					if (0 < $loggedTravelerID && !$objNewMember->isBlocked($loggedTravelerID))				// added by neri if traveler is logged in: 11-28-08
						$arr[] = $objNewMember; 
					else if (0 == $loggedTravelerID)
						$arr[] = $objNewMember;
				} 
			}
		}
		return $arr;				
	}
	
	
}
?>
