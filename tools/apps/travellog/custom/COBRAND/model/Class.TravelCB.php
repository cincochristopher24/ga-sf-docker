<?php
require_once('travellog/model/Class.Travel.php');
class TravelCB extends Travel{
	
	static function getCountriesWithGroupLogs( $groupID ){
		$gIDs    = self::getAdminSubGroups  ( $groupID );	
		$travIDs = self::getGroupsToTraveler( $gIDs    );
		$arr     = array();
		$conn    = new Connection();
		$rs      = new Recordset($conn);		
		$sql     = sprintf(
							'SELECT countryID FROM tblTravel, tblTrips, tblTravelLog, GoAbroad_Main.tbcountry
							 WHERE 
							 	 tblTravel.travelID   =  tblTrips.travelID
							 AND tblTrips.tripID    =  tblTravelLog.tripID
							 AND tblTravelLog.deleted = 0	 
							 AND tblTravel.deleted = 0 
							 AND tblTravel.travelerID IN ( %s ) 
							 AND tblTrips.locID       =  GoAbroad_Main.tbcountry.locID
							 UNION         
							 SELECT countryID FROM tblTravel, tblTrips, tblTravelLog, GoAbroad_Main.tbcity
							 WHERE 
							 	 tblTravel.travelID   =  tblTrips.travelID
							 AND tblTrips.tripID    =  tblTravelLog.tripID
							 AND tblTravelLog.deleted = 0 
						 	 AND tblTravel.deleted = 0 
							 AND tblTravel.travelerID IN ( %s ) 
							 AND tblTrips.locID       =  GoAbroad_Main.tbcity.locID'
							 ,$travIDs
							 ,$travIDs
						  ); 
		$rs->Execute( $sql );
		if ($rs->Recordcount() == 0){
			return NULL;	 	
		}else {
			require_once("travellog/model/Class.Country.php");
			while ($row = mysql_fetch_array($rs->Resultset())) {
				try {
					$obj_country = new Country($row['countryID']);
					$arr[] = $obj_country;
				} catch (Exception $e) { }
			}
		}
		
		usort($arr,array("TravelLog","orderByCountryName"));
				
		return $arr;
	}
	
	static function getAllGroupMemberTravelsByLocationID($arr_vars){
		$groupID = $arr_vars['groupID'];
		$locationID = $arr_vars['locationID'];
		if (isset($arr_vars['privacyCriteria2']))
			$privacyCriteria2 = $arr_vars['privacyCriteria2'];
		else
			$privacyCriteria2 = '';	
		
		$conn    = new Connection();
		$rs     = new Recordset($conn);
		$locIDs  = self::getLocationByCountryID( $locationID );
		
		$travels = array();
		$group = new AdminGroup($groupID);
		
		$sql = 'select * from tblGroupApprovedJournals as gaj,tblTravel as t, tblTrips as trp, tblTravelLog as tlog
							where gaj.groupID = %d and gaj.approved = 1 
							and	  gaj.travelID = t.travelID	and t.travelID = trp.travelID	
							and   t.deleted = 0  	
							and   trp.locID  in  (%s)
							and   trp.tripID = tlog.tripID ';
		
		if ($privacyCriteria2 != '')
			$sql .= ' and t.privacypref in (%s) ';
			
		$sql .= ' order by tlog.logdate DESC';						
				
		$sql = sprintf( $sql 		
						 , $groupID
						 , $locIDs
						 , $privacyCriteria2);
		//echo $sql;exit;
	 	$rs->Execute( $sql );
	 	
	 	while( $row = mysql_fetch_assoc( $rs->Resultset() ) ){
			$tr = new TravelCB;
			$tr->setTravelID    ( $row['travelID']     );
			$tr->setTitle       ( $row['title']        );
			$tr->setDescription ( $row['description']  );
			$tr->setRating      ( $row['rating']       );
			$tr->setVotes       ( $row['votes']        );
			$tr->setViews       ( $row['viewed']       );
			$tr->setLastUpdated ( $row['lastupdated']  );
			$tr->setPrimaryphoto( $row['primaryphoto'] );
			$tr->setPrivacyPref ( $row['privacypref']  );			
			$tr->setPublish		( $row['publish']	   );
			$tr->setEntryCount 	( $row['entryCount']   );	
			$tr->setTravelerID  ( $row['travelerID']   );
			$tr->setTravelLinkID( $row['travellinkID'] );
			
			$travels[] = $tr;
		}      
	 			
		
		return $travels;
	}
	
	static function getAllGroupTravelsByCountryID($arr_vars){
		$gIDs    = self::getAdminSubGroups     ( $arr_vars['groupID']    );
		$travIDs = self::getGroupsToTraveler   ( $gIDs                   );
		$locIDs  = self::getLocationByCountryID( $arr_vars['locationID'] );
		$conn    = new Connection();
		$rs1     = new Recordset($conn);
		$rs2     = new Recordset($conn);
		$arr     = array();
		$sql2    = 'SELECT FOUND_ROWS() AS totalrec';
		
		if (array_key_exists('offset',$arr_vars) && array_key_exists('rows',$arr_vars) ){
			$sql1    = sprintf(
								'SELECT SQL_CALC_FOUND_ROWS tblTravel.* FROM tblTravel, tblTrips, tblTravelLog
								 WHERE 
								 	 tblTravel.travelID         = tblTrips.travelID
								 AND tblTravel.travelerID       IN ( %s ) 
								 AND tblTravel.deleted          = 0 
								 AND tblTrips.locID             IN ( %s )
								 AND tblTrips.tripID            = tblTravelLog.tripID 
								 GROUP BY tblTravel.travelID 
								 ORDER BY tblTravelLog.logdate DESC
								 LIMIT %d, %d'
								 ,$travIDs
								 ,$locIDs
								 ,$arr_vars['offset']
								 ,$arr_vars['rows']
							  );
		} else {
			$sql1    = sprintf(
								'SELECT SQL_CALC_FOUND_ROWS tblTravel.* FROM tblTravel, tblTrips, tblTravelLog
								 WHERE 
								 	 tblTravel.travelID         = tblTrips.travelID
								 AND tblTravel.travelerID       IN ( %s ) 
								 AND tblTravel.deleted          = 0 
								 AND tblTrips.locID             IN ( %s )
								 AND tblTrips.tripID            = tblTravelLog.tripID 
								 GROUP BY tblTravel.travelID 
								 ORDER BY tblTravelLog.logdate DESC'								 
								 ,$travIDs
								 ,$locIDs								 
							  );
		}
		$rs1->Execute( $sql1 );
		$rs2->Execute( $sql2 );
		
		Travel::$statvar = $rs2->Result(0, "totalrec");
		while( $row = mysql_fetch_assoc( $rs1->Resultset() ) ){
			$objNewMember = new TravelCB;
			$objNewMember->setTravelID    ( $row['travelID']     );
			$objNewMember->setTitle       ( $row['title']        );
			$objNewMember->setDescription ( $row['description']  );
			$objNewMember->setRating      ( $row['rating']       );
			$objNewMember->setVotes       ( $row['votes']        );
			$objNewMember->setViews       ( $row['viewed']       );
			$objNewMember->setLastUpdated ( $row['lastupdated']  );
			$objNewMember->setPrimaryphoto( $row['primaryphoto'] );
			$objNewMember->setTravelerID  ( $row['travelerID']   );
			$objNewMember->setTravelLinkID( $row['travellinkID'] );
			$arr[] = $objNewMember;
		}      
		return $arr;						  
	}
	
	static function getAllGroupTravelsAndArticlesByCountryID($arr_vars){
		$gIDs    = self::getAdminSubGroups     ( $arr_vars['groupID']    );
		$travIDs = self::getGroupsToTraveler   ( $gIDs                   );
		$locIDs  = self::getLocationByCountryID( $arr_vars['locationID'] );
		$conn    = new Connection();
		$rs1     = new Recordset($conn);
		$rs2     = new Recordset($conn);
		$arr     = array();
		$sql2    = 'SELECT FOUND_ROWS() AS totalrec';
		
		$asql = "SELECT tblArticle.authorID, " . 
					"tblArticle.articleID, " . 
					"tblArticle.locID, " . 
					"tblArticle.title, " . 
					"tblArticle.description, " . 
					"0 as primaryphoto, " . 
					"0 as preference, " . 
					"tblArticle.lastedited as lastupdated, " .
					"tblArticle.logdate as _date, " . 							
					"'ARTICLE' as type, " .
					"tblArticle.deleted, " .
					"1 as publish, " .
					"1 as entryCount, " .
					"0 as views, " .
					"tblArticle.groupID as travellinkID " .
				" FROM tblArticle " .
				" WHERE tblArticle.deleted = 0 " .
				" AND tblArticle.authorID IN ( " .$travIDs. " ) " .
				" AND tblArticle.locID IN ( " .$locIDs. " ) " . 
				"GROUP BY tblArticle.articleID";
		
		$sql1 = "SELECT SQL_CALC_FOUND_ROWS tblTravel.travelerID, " . 
					"tblTravel.travelID as ID, " . 
					"tblTravel.locID, " . 
					"tblTravel.title, " . 
					"tblTravel.description, " . 
					"primaryphoto, " . 
					"privacypref, " . 
					"tblTravel.lastupdated as lastupdated, " .
					"tblTravelLog.logdate as _date, " .
					"'JOURNAL' as type, " . 
					"tblTravel.deleted, " .
					"tblTravel.publish, " .
					"tblTravel.entryCount, " .
					"tblTravel.viewed as views, " .
					"tblTravel.travellinkID " .
				"FROM tblTravel, tblTrips, tblTravelLog ".
				"WHERE tblTravel.travelID = tblTrips.travelID ".
					 "AND tblTravel.travelerID IN ( " .$travIDs. " ) " .
					 "AND tblTravel.deleted = 0 " . 
					 "AND tblTrips.locID IN ( " .$locIDs. " )" . 
					 "AND tblTrips.tripID = tblTravelLog.tripID " .
					 "GROUP BY ID " . 
					 "UNION " . $asql . " " . 
					 "ORDER BY _date DESC ";
		
		if (array_key_exists('offset',$arr_vars) && array_key_exists('rows',$arr_vars) )
			$sql1 .= "LIMIT " .$arr_vars['offset']. ", " .$arr_vars['rows'];
		
		$rs1->Execute( $sql1 );
		$rs2->Execute( $sql2 );
		
		Travel::$statvar = $rs2->Result(0, "totalrec");
		require_once("travellog/vo/Class.JournalsArticlesVO.php");
		require_once("travellog/model/Class.Article.php");
		
		while ($recordset = mysql_fetch_assoc($rs1->Resultset())){					
			try {
				$filteredTravel	= new JournalsArticlesVO();
				$filteredTravel->setTravelerID($recordset['travelerID']);
				$filteredTravel->setTravelID($recordset['ID']);
				$filteredTravel->setLocID($recordset['locID']);
				$filteredTravel->setTitle($recordset['title']);
				$filteredTravel->setDescription($recordset['description']);
				$filteredTravel->setPrimaryphoto($recordset['primaryphoto']);
				$filteredTravel->setPrivacyPref($recordset['privacypref']);
				$filteredTravel->setDate($recordset['_date']);
				$filteredTravel->setType($recordset['type']); 
				$filteredTravel->setPublish($recordset['publish']);
				$filteredTravel->setEntryCount($recordset['entryCount']);
				$filteredTravel->setViews($recordset['views']);
				if($filteredTravel->getType() == "JOURNAL"){
					$travel = new Travel(0, 
						array( 
							"locID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"rating" => NULL,
							"votes" => NULL,
							"viewed" => NULL,
							'lastupdated' => NULL,
							"primaryphoto" => NULL,
							"privacypref" => NULL,
							"publish" => NULL,
							"isfeatured" => NULL,
							"entryCount"  => NULL,
							"travelerID" => NULL,
							"travellinkID" => $recordset['travellinkID'], 
							"travelID" => $recordset['ID']
						)
					);
					$filteredTravel->setOwner($travel->getOwner());
				}else{
					$article = new Article(0, 
						array(
							"articleID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"callout" => NULL,
							"locID" => NULL,
							"editorsNote" => NULL,
							"articledate" => NULL,
							"logdate" => NULL,
							"lastedited" => NULL,
							"isfeatured" => NULL,
							"publish" => NULL,
							"deleted" => NULL,
							"groupID" => $recordset['travellinkID'], 
							"authorID" => $recordset['travelerID'])
						);
					$filteredTravel->setOwner($article->getOwner());
				}
			}
			catch (Exception $e) {
			   throw $e;
			}
			$arr[] = $filteredTravel;
		} 
		return $arr;						  
	}

	static function getAllGroupMemberTravelsAndArticlesByLocationID($arr_vars){
		$groupID = $arr_vars['groupID'];
		$locationID = $arr_vars['locationID'];
		if (isset($arr_vars['privacyCriteria2']))
			$privacyCriteria2 = $arr_vars['privacyCriteria2'];
		else
			$privacyCriteria2 = '';	
		
		$conn    = new Connection();
		$rs     = new Recordset($conn);
		$locIDs  = self::getLocationByCountryID( $locationID );
		
		$arr = array();
		$group = new AdminGroup($groupID);
		
		$asql = "SELECT tblArticle.authorID, " . 
					"tblArticle.articleID, " . 
					"tblArticle.locID, " . 
					"tblArticle.title, " . 
					"tblArticle.description, " . 
					"0 as primaryphoto, " . 
					"0 as preference, " . 
					"tblArticle.lastedited as lastupdated, " .
					"tblArticle.logdate as _date, " . 							
					"'ARTICLE' as type, " .
					"tblArticle.deleted, " .
					"1 as publish, " .
					"1 as entryCount, " .
					"0 as views, " . 
					"tblArticle.groupID as travellinkID " .
				" FROM tblArticle " .
				" WHERE tblArticle.deleted = 0 " .
				" AND tblArticle.groupID = " .$groupID. 
				" AND tblArticle.locID IN ( " .$locIDs. " ) ".
				" GROUP BY tblArticle.articleID";

		$sql = "SELECT SQL_CALC_FOUND_ROWS tblTravel.travelerID, " . 
					"tblTravel.travelID as ID, " . 
					"tblTravel.locID, " . 
					"tblTravel.title, " . 
					"tblTravel.description, " . 
					"primaryphoto, " . 
					"privacypref, " . 
					"tblTravel.lastupdated as lastupdated, " .
					"tblTravelLog.logdate as _date, " .
					"'JOURNAL' as type, " . 
					"tblTravel.deleted, " .
					"tblTravel.publish, " .
					"tblTravel.entryCount, " .
					"tblTravel.viewed as views, " . 
					"tblTravel.travellinkID " .
				"FROM tblGroupApprovedJournals as gaj,tblTravel, tblTrips, tblTravelLog " . 
				"WHERE gaj.groupID = " .$groupID . " AND gaj.approved = 1 " . 
					"AND gaj.travelID = tblTravel.travelID " . 
					"AND tblTravel.travelID = tblTrips.travelID " . 
					 "AND tblTravel.deleted = 0 " . 
					 "AND tblTrips.locID IN ( " .$locIDs. " ) " . 
					 "AND tblTrips.tripID = tblTravelLog.tripID ";
		
		if ($privacyCriteria2 != '')
			$sql .= "AND tblTravel.privacypref IN (" .$privacyCriteria2. ") ";

		$sql .= "UNION " . $asql ." " . 
				"ORDER BY _date DESC ";
	 	$rs->Execute( $sql );
	 	
	 	require_once("travellog/vo/Class.JournalsArticlesVO.php");
		require_once("travellog/model/Class.Article.php");
		
		while ($recordset = mysql_fetch_assoc($rs->Resultset())){					
			try {
				$filteredTravel	= new JournalsArticlesVO();
				$filteredTravel->setTravelerID($recordset['travelerID']);
				$filteredTravel->setTravelID($recordset['ID']);
				$filteredTravel->setLocID($recordset['locID']);
				$filteredTravel->setTitle($recordset['title']);
				$filteredTravel->setDescription($recordset['description']);
				$filteredTravel->setPrimaryphoto($recordset['primaryphoto']);
				$filteredTravel->setPrivacyPref($recordset['privacypref']);
				$filteredTravel->setDate($recordset['_date']);
				$filteredTravel->setType($recordset['type']); 
				$filteredTravel->setPublish($recordset['publish']);
				$filteredTravel->setEntryCount($recordset['entryCount']);
				$filteredTravel->setViews($recordset['views']);
				if($filteredTravel->getType() == "JOURNAL"){
					$travel = new Travel(0, 
						array( 
							"locID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"rating" => NULL,
							"votes" => NULL,
							"viewed" => NULL,
							'lastupdated' => NULL,
							"primaryphoto" => NULL,
							"privacypref" => NULL,
							"publish" => NULL,
							"isfeatured" => NULL,
							"entryCount"  => NULL,
							"travelerID" => NULL,
							"travellinkID" => $recordset['travellinkID'], 
							"travelID" => $recordset['ID']
						)
					);
					$filteredTravel->setOwner($travel->getOwner());
				}else{
					$article = new Article(0, 
						array(
							"articleID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"callout" => NULL,
							"locID" => NULL,
							"editorsNote" => NULL,
							"articledate" => NULL,
							"logdate" => NULL,
							"lastedited" => NULL,
							"isfeatured" => NULL,
							"publish" => NULL,
							"deleted" => NULL,
							"groupID" => $recordset['travellinkID'], 
							"authorID" => $recordset['travelerID'])
						);
					$filteredTravel->setOwner($article->getOwner());
				}
			}
			catch (Exception $e) {
			   throw $e;
			}
			$arr[] = $filteredTravel;
		} 
		return $arr;
	}	
	
	static function getEntriesAndArticlesByLocationID($arr_vars){
		$gIDs    = self::getAdminSubGroups     ( $arr_vars['groupID']    );
		$travIDs = self::getGroupsToTraveler   ( $gIDs                   );
		$locIDs  = self::getLocationByCountryID( $arr_vars['locationID'] );
		$groupID = $arr_vars['groupID'];
		
		$start = $arr_vars['START'];
		$end = $arr_vars['END'];
		
		$conn    = new Connection();
		$rs1     = new Recordset($conn);
		$rs2     = new Recordset($conn);
		$arr     = array();
		
		$asql1 = "SELECT tblArticle.authorID, " . 
					"tblArticle.articleID, " . 
					"tblArticle.locID, " . 
					"tblArticle.title, " . 
					"tblArticle.description, " . 
					"tblArticle.lastedited as lastupdated, " .
					"tblArticle.logdate as _date, " . 							
					"0 as trip, " . 
					"'ARTICLE' as type, " .
					"0 as views, " . 
					"tblArticle.groupID as travellinkID, " .
					"0 as t_ID " .
				" FROM tblArticle " .
				" WHERE tblArticle.deleted = 0 " .
				" AND tblArticle.authorID IN ( " .$travIDs. " ) " .
				" AND tblArticle.locID IN ( " .$locIDs. " ) " . 
				"GROUP BY tblArticle.articleID";
		
		
		$asql = "SELECT tblArticle.authorID, " . 
					"tblArticle.articleID, " . 
					"tblArticle.locID, " . 
					"tblArticle.title, " . 
					"tblArticle.description, " . 
					"tblArticle.lastedited as lastupdated, " .
					"tblArticle.logdate as _date, " . 							
					"0 as trip, " . 
					"'ARTICLE' as type, " .
					"0 as views, " .
					"tblArticle.groupID as travellinkID, " .
					"0 as t_ID " .
				" FROM tblArticle " .
				" WHERE tblArticle.deleted = 0 " .
				" AND tblArticle.groupID = " .$groupID. 
				" AND tblArticle.locID IN ( " .$locIDs. " ) ".
				" GROUP BY tblArticle.articleID UNION ". $asql1;	

		$esql = "SELECT tblTravel.travelerID, " . 
					"tblTravelLog.travelLogID as ID, " . 
					"tblTravel.locID, " . 
					"tblTravelLog.title, " . 
					"tblTravelLog.description, " . 
					"tblTravelLog.lastedited as lastupdated, " .
					"tblTravelLog.logdate as _date, " .
					"tblTravelLog.tripID as trip, " . 
					"'JOURNAL' as type, " . 
					"tblTravel.viewed as views, " .
					"tblTravel.travellinkID, " .
					"tblTravel.travelID as t_ID " .
				"FROM tblGroupApprovedJournals as gaj,tblTravel, tblTrips, tblTravelLog " . 
				"WHERE gaj.groupID IN ( " .$gIDs. " ) " .
					"AND gaj.approved = 1 " . 
					"AND gaj.travelID = tblTravel.travelID " . 
					"AND tblTravel.travelID = tblTrips.travelID " . 
					"AND tblTravel.deleted = 0 " .
					"AND tblTravel.travelerID IN ( " .$travIDs. " ) " .
					"AND tblTravelLog.deleted = 0 " . 
					"AND tblTrips.locID IN ( " .$locIDs. " ) " . 
					"AND tblTrips.tripID = tblTravelLog.tripID " . 
					"GROUP BY ID ";
		
		$sql = 	"SELECT SQL_CALC_FOUND_ROWS tbl.travelerID, " . 
						"tbl.ID, " . 
						"tbl.locID, " . 
						"tbl.title, " . 
						"tbl.description, " . 
						"tbl.lastupdated, " .
						"tbl._date, " .
						"tbl.trip, " . 
						"tbl.type, " . 
						"tbl.views, " .
						"tbl.travellinkID, " .
						"tbl.t_ID " .
					"FROM ($esql UNION $asql ORDER BY CASE WHEN lastupdated = '0000-00-00 00:00:00' " . 
						"THEN _date " . 
						"ELSE lastupdated " . 
					"END DESC) as tbl ";
		
		if(isset($start) && isset($end))		
			$sql .= "LIMIT $start,$end";							

		$rs1->Execute($sql);

		$rs2->Execute('SELECT FOUND_ROWS() as totalrec');
		$row = mysql_fetch_assoc($rs2->Resultset());
		Travel::$statvar = $row['totalrec'];

		require_once("travellog/vo/Class.JournalsArticlesVO.php");
		require_once("travellog/model/Class.Article.php");

		$arrTravel = array();
		while ($recordset = mysql_fetch_assoc($rs1->Resultset())){					
			try {
				$filteredTravel	= new JournalsArticlesVO();
				$filteredTravel->setTravelerID($recordset['travelerID']);
				$filteredTravel->setTravelID($recordset['ID']);
				$filteredTravel->setID($recordset['t_ID']);
				$filteredTravel->setLocID($recordset['locID']);
				$filteredTravel->setTitle($recordset['title']);
				$filteredTravel->setDescription($recordset['description']);
				$filteredTravel->setDate($recordset['_date']);
				$filteredTravel->setType($recordset['type']); 
				$filteredTravel->setTrip(new Trip($recordset['trip']));
				$filteredTravel->setViews($recordset['views']);
				if($filteredTravel->getType() == "JOURNAL"){
					$travel = new Travel(0, 
						array( 
							"locID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"rating" => NULL,
							"votes" => NULL,
							"viewed" => NULL,
							'lastupdated' => NULL,
							"primaryphoto" => NULL,
							"privacypref" => NULL,
							"publish" => NULL,
							"isfeatured" => NULL,
							"entryCount"  => NULL,
							"travelerID" => NULL,
							"travellinkID" => $recordset['travellinkID'], 
							"travelID" => $recordset['ID']
						)
					);
					$filteredTravel->setOwner($travel->getOwner());
				}else{
					$article = new Article(0, 
						array(
							"articleID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"callout" => NULL,
							"locID" => NULL,
							"editorsNote" => NULL,
							"articledate" => NULL,
							"logdate" => NULL,
							"lastedited" => NULL,
							"isfeatured" => NULL,
							"publish" => NULL,
							"deleted" => NULL,
							"groupID" => $recordset['travellinkID'], 
							"authorID" => $recordset['travelerID'])
						);
					$filteredTravel->setOwner($article->getOwner());
				}
			}
			catch (Exception $e) {
			   throw $e;
			}
				$arrTravel[] = $filteredTravel;
		}
		return $arrTravel;			
	}

	static function getEntriesAndArticlesByLocationID2($arr_vars){
		$gIDs    = self::getAdminSubGroups     ( $arr_vars['groupID']    );
		$travIDs = self::getGroupsToTraveler   ( $gIDs                   );
		$locIDs  = self::getLocationByCountryID( $arr_vars['locationID'] );
		$locationID = $arr_vars['locationID'];
		$groupID = $arr_vars['groupID'];
		
		$start = $arr_vars['START'];
		$end = $arr_vars['END'];
		
		$conn    = new Connection();
		$rs1     = new Recordset($conn);
		$rs2     = new Recordset($conn);
		$arr     = array();
		

	$asql1 =<<<EOF

SELECT 
	
	tblArticle.authorID,  
	tblArticle.articleID ,  
	tblArticle.locID,  
	tblArticle.title,  
	tblArticle.description,  
	tblArticle.lastedited as lastupdated, 
	tblArticle.logdate as _date,  							
	0 as trip,  
	'ARTICLE' as type, 
	0 as views,  
	tblArticle.groupID as travellinkID, 
	0 as t_ID 


FROM tblArticle

JOIN (
SELECT gt.travelerID
FROM tblGrouptoTraveler gt
JOIN tblGroup ON ( tblGroup.groupID = gt.groupID
AND parentID =$groupID ) 
JOIN tblTraveler t
 on (gt.travelerID = t.travelerID  AND t.isSuspended = 0)

UNION 

SELECT gt.travelerID 
FROM tblGrouptoTraveler gt
JOIN tblTraveler t
 on (gt.travelerID = t.travelerID  AND t.isSuspended = 0 AND groupID = $groupID)

) T
on (tblArticle.authorID = T.travelerID)


JOIN (select GoAbroad_Main.tbcity.locID as locID from GoAbroad_Main.tbcity 
		JOIN GoAbroad_Main.tbcountry on (GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID)
		where GoAbroad_Main.tbcountry.locID = $locationID
		UNION
	select tblNewCity.locID as locID from tblNewCity
		join GoAbroad_Main.tbcountry ON (tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID)
		where GoAbroad_Main.tbcountry.locID = $locationID
	
		UNION select tblArticle.locID as locID 
		from tblArticle where tblArticle.locID = $locationID

	) city

	ON (tblArticle.locID = city.locID )

WHERE tblArticle.deleted = 0

EOF;
		$asql = <<<EOF

		SELECT tblArticle.authorID,  
	tblArticle.articleID,  
					tblArticle.locID,  
					tblArticle.title,  
					tblArticle.description,  
					tblArticle.lastedited as lastupdated, 
					tblArticle.logdate as _date,  							
					0 as trip,  
					'ARTICLE' as type, 
					0 as views, 
					tblArticle.groupID as travellinkID, 
					0 as t_ID 
					
				 FROM tblArticle 

					JOIN (select GoAbroad_Main.tbcity.locID as locID from GoAbroad_Main.tbcity 
							JOIN GoAbroad_Main.tbcountry on (GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID)
							where GoAbroad_Main.tbcountry.locID = $locationID
							UNION
						select tblNewCity.locID as locID from tblNewCity
							join GoAbroad_Main.tbcountry ON (tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID)
							where GoAbroad_Main.tbcountry.locID = $locationID
						
							UNION select tblArticle.locID as locID 
							from tblArticle where tblArticle.locID = $locationID

						) city

						ON (tblArticle.locID = city.locID )


				 WHERE tblArticle.deleted = 0 
				 AND tblArticle.groupID = $groupID 

				 UNION $asql1

EOF;


	$esql = <<<EOF

			SELECT tblTravel.travelerID,  
					tblTravelLog.travelLogID as ID,  
					tblTravel.locID,  
					tblTravelLog.title,  
					tblTravelLog.description,  
					tblTravelLog.lastedited as lastupdated, 
					tblTravelLog.logdate as _date, 
					tblTravelLog.tripID as trip,
					'JOURNAL' as type,  
					tblTravel.viewed as views, 
					tblTravel.travellinkID, 
					tblTravel.travelID as t_ID
	
	FROM tblGroupApprovedJournals gaj0

	JOIN (select tblGroup.groupID
			from tblGroup WHERE tblGroup.parentID = $groupID 
			union 
		  select gajX.groupID
			from tblGroupApprovedJournals gajX
			where gajX.groupID = $groupID

	) GAJ2
	
	on (GAJ2.groupID = gaj0.groupID)
	
	JOIN tblTravel on (gaj0.travelID = tblTravel.travelID)

	JOIN (
		SELECT gt.travelerID
		FROM tblGrouptoTraveler gt
		JOIN tblGroup ON ( tblGroup.groupID = gt.groupID
		AND parentID =$groupID ) 
		JOIN tblTraveler t
		 on (gt.travelerID = t.travelerID  AND t.isSuspended = 0)

		UNION 
		
		SELECT gt.travelerID 
		FROM tblGrouptoTraveler gt
		JOIN tblTraveler t
		 on (gt.travelerID = t.travelerID  AND t.isSuspended = 0 AND groupID = $groupID)

		) T
		on (tblTravel.travelerID = T.travelerID)

	
	JOIN tblTrips on (tblTravel.travelID = tblTrips.travelID)
	JOIN tblTravelLog on (tblTrips.tripID = tblTravelLog.tripID)
			
	JOIN (select GoAbroad_Main.tbcity.locID as locID from GoAbroad_Main.tbcity 
			JOIN GoAbroad_Main.tbcountry on (GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID)
			where GoAbroad_Main.tbcountry.locID = $locationID
			UNION
			select tblNewCity.locID as locID from tblNewCity
			join GoAbroad_Main.tbcountry ON (tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID)
			where GoAbroad_Main.tbcountry.locID = $locationID
			
			UNION 
			select tblTrips.locID as locID from tblTrips 
			where tblTrips.locID = $locationID
	) city
	ON (tblTrips.locID = city.locID )

	WHERE gaj0.approved = 1  
	AND tblTravel.deleted = 0 
	AND tblTravelLog.deleted = 0
	
		

EOF;

	/*var_dump($esql);
	exit;*/
		$sql = 	"SELECT SQL_CALC_FOUND_ROWS tbl.travelerID, " . 
						"tbl.ID, " . 
						"tbl.locID, " . 
						"tbl.title, " . 
						"tbl.description, " . 
						"tbl.lastupdated, " .
						"tbl._date, " .
						"tbl.trip, " . 
						"tbl.type, " . 
						"tbl.views, " .
						"tbl.travellinkID, " .
						"tbl.t_ID " .
					"FROM ($esql UNION $asql ORDER BY CASE WHEN lastupdated = '0000-00-00 00:00:00' " . 
						"THEN _date " . 
						"ELSE lastupdated " . 
					"END DESC) as tbl ";
		
		if(isset($start) && isset($end))		
			$sql .= "LIMIT $start,$end";							

		$rs1->Execute($sql);

		$rs2->Execute('SELECT FOUND_ROWS() as totalrec');
		$row = mysql_fetch_assoc($rs2->Resultset());

		Travel::$statvar = $row['totalrec'];

		require_once("travellog/vo/Class.JournalsArticlesVO.php");
		require_once("travellog/model/Class.Article.php");

		$arrTravel = array();
		while ($recordset = mysql_fetch_assoc($rs1->Resultset())){					
			try {
				$filteredTravel	= new JournalsArticlesVO();
				$filteredTravel->setTravelerID($recordset['travelerID']);
				$filteredTravel->setTravelID($recordset['ID']);
				$filteredTravel->setID($recordset['t_ID']);
				$filteredTravel->setLocID($recordset['locID']);
				$filteredTravel->setTitle($recordset['title']);
				$filteredTravel->setDescription($recordset['description']);
				$filteredTravel->setDate($recordset['_date']);
				$filteredTravel->setType($recordset['type']); 
				$filteredTravel->setTrip(new Trip($recordset['trip']));
				$filteredTravel->setViews($recordset['views']);
				if($filteredTravel->getType() == "JOURNAL"){
					$travel = new Travel(0, 
						array( 
							"locID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"rating" => NULL,
							"votes" => NULL,
							"viewed" => NULL,
							'lastupdated' => NULL,
							"primaryphoto" => NULL,
							"privacypref" => NULL,
							"publish" => NULL,
							"isfeatured" => NULL,
							"entryCount"  => NULL,
							"travelerID" => NULL,
							"travellinkID" => $recordset['travellinkID'], 
							"travelID" => $recordset['ID']
						)
					);
					$filteredTravel->setOwner($travel->getOwner());
				}else{
					$article = new Article(0, 
						array(
							"articleID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"callout" => NULL,
							"locID" => NULL,
							"editorsNote" => NULL,
							"articledate" => NULL,
							"logdate" => NULL,
							"lastedited" => NULL,
							"isfeatured" => NULL,
							"publish" => NULL,
							"deleted" => NULL,
							"groupID" => $recordset['travellinkID'], 
							"authorID" => $recordset['travelerID'])
						);
					$filteredTravel->setOwner($article->getOwner());
				}
			}
			catch (Exception $e) {
			   throw $e;
			}
				$arrTravel[] = $filteredTravel;
		}
		return $arrTravel;			
	}
	
	static function getAllGroupTravelsAndArticlesByTag($arr_vars){
		$groupID = $arr_vars['groupID'];
		$gIDs    = self::getAdminSubGroups     ( $arr_vars['groupID']    );
		$travIDs = self::getGroupsToTraveler   ( $gIDs                   );
		$tag = $arr_vars['tag'];
		
		$start = $arr_vars['START'];
		$end = $arr_vars['END'];
		
		$conn    = new Connection();
		$rs     = new Recordset($conn);
		$rs2     = new Recordset($conn);
		
		$arr = array();
		$group = new AdminGroup($groupID);
		
		$asql = "SELECT SQL_CALC_FOUND_ROWS tblArticle.authorID as travelerID, " . 
					"tblArticle.articleID as ID, " . 
					"tblArticle.locID, " . 
					"tblArticle.title, " . 
					"tblArticle.description, " . 
					"0 as primaryphoto, " . 
					"0 as privacypref, " . 
					"tblArticle.lastedited as lastupdated, " .
					"tblArticle.logdate as _date, " . 							
					"'ARTICLE' as type, " .
					"tblArticle.deleted, " .
					"1 as publish, " .
					"1 as entryCount, " .
					"0 as views, " . 
					"tblArticle.groupID as travellinkID, " .
					"0 as t_ID " .
				" FROM tblArticle, tblTags " .
				" WHERE tblTags.context = 0 " . 
				" AND tblTags.contextID = tblArticle.articleID " . 
				" AND tblTags.tag = '" .addslashes($tag). "' ".
				" AND tblArticle.deleted = 0 " .
				" AND (tblArticle.groupID = " .$groupID. 
				" OR tblArticle.authorID IN ( " .$travIDs. " )) ".
				" GROUP BY tblArticle.articleID" . 
				" ORDER BY CASE WHEN lastupdated = '0000-00-00 00:00:00' " . 
					"THEN _date " . 
					"ELSE lastupdated " . 
				"END DESC ";

		
		if(isset($start) && isset($end))		
			$asql .= "LIMIT $start,$end";
		
		$rs->Execute( $asql );
		
		$rs2->Execute('SELECT FOUND_ROWS() as totalrec');
		$row = mysql_fetch_assoc($rs2->Resultset());
		Travel::$statvar = $rs2->Result(0, "totalrec");
	 		 	
	 	require_once("travellog/vo/Class.JournalsArticlesVO.php");
		require_once("travellog/model/Class.Article.php");
		
		while ($recordset = mysql_fetch_assoc($rs->Resultset())){					
			try {
				$filteredTravel	= new JournalsArticlesVO();
				$filteredTravel->setTravelerID($recordset['travelerID']);
				$filteredTravel->setTravelID($recordset['ID']);			
				$filteredTravel->setID($recordset['t_ID']);
				$filteredTravel->setLocID($recordset['locID']);
				$filteredTravel->setTitle($recordset['title']);
				$filteredTravel->setDescription($recordset['description']);
				$filteredTravel->setPrimaryphoto($recordset['primaryphoto']);
				$filteredTravel->setPrivacyPref($recordset['privacypref']);
				$filteredTravel->setDate($recordset['_date']);
				$filteredTravel->setType($recordset['type']); 
				$filteredTravel->setPublish($recordset['publish']);
				$filteredTravel->setEntryCount($recordset['entryCount']);
				$filteredTravel->setViews($recordset['views']);
				if($filteredTravel->getType() == "JOURNAL"){
					$travel = new Travel(0, 
						array( 
							"locID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"rating" => NULL,
							"votes" => NULL,
							"viewed" => NULL,
							'lastupdated' => NULL,
							"primaryphoto" => NULL,
							"privacypref" => NULL,
							"publish" => NULL,
							"isfeatured" => NULL,
							"entryCount"  => NULL,
							"travelerID" => NULL,
							"travellinkID" => $recordset['travellinkID'], 
							"travelID" => $recordset['ID']
						)
					);
					$filteredTravel->setOwner($travel->getOwner());
				}else{
					$article = new Article(0, 
						array(
							"articleID" => NULL,
							"title" => NULL,
							"description" => NULL,
							"callout" => NULL,
							"locID" => NULL,
							"editorsNote" => NULL,
							"articledate" => NULL,
							"logdate" => NULL,
							"lastedited" => NULL,
							"isfeatured" => NULL,
							"publish" => NULL,
							"deleted" => NULL,
							"groupID" => $recordset['travellinkID'], 
							"authorID" => $recordset['travelerID'])
						);
					$filteredTravel->setOwner($article->getOwner());
				}
			}
			catch (Exception $e) {
			   throw $e;
			}
			$arr[] = $filteredTravel;
		} 
		return $arr;
	}
		
	static function getFilteredGroupTravels(Criteria2 $c , $criteria = null){
		global $CONFIG;
		$conn    = new Connection();
		$rs1     = new Recordset($conn);
		$rs2     = new Recordset($conn);
		$arr     = array();
		
		if (null == $criteria):
		
			$sql1    = sprintf
						(
							'SELECT SQL_CALC_FOUND_ROWS * FROM ' .
							'( '.
								'SELECT qryGroupTravel.* ' .
								'FROM qryGroupTravel, tblTrips,tblGroupApprovedJournals ' .
								'WHERE qryGroupTravel.travelID = tblGroupApprovedJournals.travelID ' .
								'AND qryGroupTravel.travelID NOT IN ( SELECT travelID FROM tblTravel WHERE deleted = 1 ) ' .
								'AND tblGroupApprovedJournals.travelID = tblTrips.travelID ' . 
								'AND (privacypref = 0 OR privacypref = 3) ' .
								'AND tblGroupApprovedJournals.approved = 1 ' .
								'AND tblGroupApprovedJournals.groupID  = %d ' .
								'UNION ' .
								'SELECT qryGroupTravel.* ' .
								'FROM qryGroupTravel, tblTrips ' .
								'WHERE ' .
								'travelerID       = %d ' .
								'AND (privacypref = 0 OR privacypref = 3) ' .
								'AND qryGroupTravel.travelID = tblTrips.travelID ' .
								'AND qryGroupTravel.travelID NOT IN ( SELECT travelID FROM tblTravel WHERE deleted = 1 ) ' .
								'AND qryGroupTravel.travelID NOT IN ( SELECT travelID FROM tblGroupApprovedJournals WHERE groupID = %d AND approved = 1 ) ' .
							') qryGroupJournals '.
							'%s ' 
							, $CONFIG->getGroupID()
							, $CONFIG->getAdminGroup()->getAdministratorID()
							, $CONFIG->getGroupID()
							, $c->getCriteria2()
						);
						
		else:
			
			$strCriteria2 = (null != $criteria) ? $criteria->getCriteria2('AND') : '' ;
			
			$sql1    = sprintf
						(
							'SELECT SQL_CALC_FOUND_ROWS * FROM ' .
							'( '.
								'SELECT qryGroupTravel.* ' .
								'FROM qryGroupTravel, tblTrips,tblGroupApprovedJournals, tblTravel, tblTravelLog ' .
								'WHERE qryGroupTravel.travelID = tblGroupApprovedJournals.travelID ' .
								'AND qryGroupTravel.travelID NOT IN ( SELECT travelID FROM tblTravel WHERE deleted = 1 ) ' .
								'AND tblGroupApprovedJournals.travelID = tblTrips.travelID ' . 
								'AND tblTrips.travelID = tblTravel.travelID ' . 
								'AND tblTravel.travelID = tblTravelLog.travelID ' . 
								'AND (qryGroupTravel.privacypref = 0 OR qryGroupTravel.privacypref = 3) ' .
								'AND tblGroupApprovedJournals.approved = 1 ' .
								'AND tblGroupApprovedJournals.groupID  = %d ' .
								$strCriteria2  .
								'UNION ' .
								'SELECT qryGroupTravel.* ' .
								'FROM qryGroupTravel, tblTrips, tblTravel, tblTravelLog, tblTravelLink  ' .
								'WHERE (qryGroupTravel.privacypref = 0 OR qryGroupTravel.privacypref = 3) ' .
								//'AND qryGroupTravel.travelerID       = %d ' .
								'AND qryGroupTravel.travelID = tblTrips.travelID ' .
								'AND tblTrips.travelID = tblTravel.travelID ' . 
								'AND tblTravel.travelID = tblTravelLog.travelID ' .								
								'AND tblTravel.deleted = 0 ' .
								'AND tblTravelLink.travelLinkID = tblTravel.travellinkID '.
								'AND tblTravelLink.refType = 2 '.
								'AND tblTravelLink.refID = %d '.
								'AND qryGroupTravel.travelID NOT IN ( SELECT travelID FROM tblGroupApprovedJournals WHERE groupID = %d AND approved = 1 ) ' .
								$strCriteria2 .
							') qryGroupJournals '.
							'%s ' 
							, $CONFIG->getGroupID()
							, $CONFIG->getGroupID()
							, $CONFIG->getGroupID()
							, $c->getCriteria2()
						);
			
		endif;			
		$results = $rs1->Execute($sql1); 
		$sql2    = 'SELECT FOUND_ROWS() AS totalrec';
		$rs2->Execute($sql2);
		
		
		Travel::$statvar = $rs2->Result(0, "totalrec");
		while( $row = mysql_fetch_assoc( $results ) ){
			$objNewMember = new TravelCB;
			$objNewMember->setTravelID    ( $row['travelID']     );
			$objNewMember->setTitle       ( $row['title']        );
			$objNewMember->setDescription  ( $row['description']  );
			$objNewMember->setRating      ( $row['rating']       );
			$objNewMember->setVotes       ( $row['votes']        );
			$objNewMember->setViews       ( $row['viewed']       );
			$objNewMember->setLastUpdated ( $row['lastupdated']  );
			$objNewMember->setPrimaryphoto( $row['primaryphoto'] );
			$objNewMember->setTravelerID  ( $row['travelerID']   );
			$objNewMember->setTravelLinkID( $row['travellinkID'] );
			$arr[] = $objNewMember;
		}      
		
		return $arr;
	}
	
	private static function getAdminSubGroups( $groupID ){
		require_once("travellog/model/Class.AdminGroup.php");
		$obj_admin_group   = new AdminGroup( $groupID );
		$col_groups        = $obj_admin_group->getSubGroups();
		$col_sub_groupID   = array();
		$col_sub_groupID[] = $groupID;
		if( count($col_groups) ){
			foreach( $col_groups as $obj_group ){
				$col_sub_groupID[] = $obj_group->getGroupID();		
			}
		}
		return implode(',', $col_sub_groupID); 
	}
	
	private static function getGroupsToTraveler( $groupIDs ){
		$conn = new Connection();
		$rs   = new Recordset($conn);
		$arr  = array();
		$sql  = sprintf('SELECT gt.travelerID FROM tblGrouptoTraveler as gt, tblTraveler as t WHERE gt.travelerID = t.travelerID AND t.isSuspended = 0 AND gt.groupID IN (%s)', $groupIDs);
		$rs->Execute( $sql );
		if( count( $rs->Resultset() ) ){
			while( $row = mysql_fetch_assoc( $rs->Resultset() ) ){
				$arr[] = $row['travelerID'];	
			}
		}
		return implode(',', $arr);
	}
	
	private static function getLocationByCountryID( $_locationID ){
		$conn      = new Connection();
		$rs        = new Recordset($conn);
		$col_locID = array();
		$sql  = "SELECT GoAbroad_Main.tbcity.locID FROM GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry " .
			    "WHERE GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID AND GoAbroad_Main.tbcountry.locID=" . $_locationID .
			    " UNION " .
			    "SELECT tblNewCity.locID FROM tblNewCity, GoAbroad_Main.tbcountry " .
			    "WHERE tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID AND GoAbroad_Main.tbcountry.locID=" . $_locationID;
		$rs->Execute( $sql );
		if( $rs->Recordcount() ){
			while( $row = mysql_fetch_assoc( $rs->Resultset() ) ){
				$col_locID[] = $row['locID']; 
			}
		}
		$col_locID[] = $_locationID;
		return implode(',',$col_locID);
	}
	
	function setRating($rating){
		$this->rating = $rating;
	}
	
	function setTravelLinkID($travellinkID){
		$this->travellinkID = $travellinkID;
	}
}
?>
