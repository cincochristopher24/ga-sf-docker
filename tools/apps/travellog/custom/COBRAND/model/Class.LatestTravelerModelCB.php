<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/custom/COBRAND/model/Class.TravelerSearchCriteriaCB.php');
require_once("Class.Criteria2.php");
require_once('travellog/model/Class.IModel.php');
class LatestTravelerModelCB implements IModel{
	function GetFilterLists(&$props){
		global $CONFIG;
		
		$obj_traveler = new TravelerSearchCriteriaCB;
		$obj_criteria = new Criteria2; 
		
		$obj_criteria->setOrderBy( 'latestlogin DESC'                   );
		$obj_criteria->setLimit  ( $props['offset'].', '.$props['rows'] ); 
		
		$obj_criteria->mustBeEqual('tblGrouptoTraveler.groupID' , $CONFIG->getGroupID());  
		
		if( $props['is_login'] ) {
			$obj_criteria->mustNotBeEqual( 'tblTraveler.travelerID', $props['travelerID'] );
			$props['travelers']         = $obj_traveler->getAllTravelers($obj_criteria, $props['travelerID']); 			// neri added travelerID as parameter if traveler is logged in: 11-28-08		
		}  
		
		else
			$props['travelers']         = $obj_traveler->getAllTravelers($obj_criteria);
		
		$props['travelers_count'] = $obj_traveler->getTotalRecords();
		$props['obj_paging']   = new Paging        ($props['travelers_count'], $props['page'],'action=LastLogin', $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                                          );
		$props['view_type']    = 5;
	}
}
?>
