<?php
	/*
	 * Class.COBRANDLoginController.php
	 * Created on Nov 23, 2007
	 * created by marc
	 */
	
	require_once("travellog/controller/Class.AbstractLoginController.php");
 	
 	class COBRANDLoginController extends AbstractLoginController{
 		
 		static $adminGroup = NULL;
 		
 		function __construct(){
 			
 			parent::__construct();
 			
 			$this->file_factory->registerClass('Config');
 			$this->file_factory->registerClass('TravelerCB', array("path" => "travellog/custom/COBRAND/model"));
 			$this->file_factory->registerClass('Session', array("path" => "travellog/model"));
 			//$this->file_factory->registerClass("HelperGlobal", array("path" => ""));
 			$this->file_factory->registerClass('MailGeneratorCB', array('path' => 'travellog/custom/COBRAND/model'));
 			
	 		$GLOBALS['CONFIG'] = $this->config = $this->file_factory->getClass('Config');
	 		$this->adminGroup = $this->file_factory->getClass("AdminGroup", array($this->config->getGroupID()));
	 		
	 	}
 		
 		function activateAndLogin(){
 			
 			$ref = $this->login_helper->getActivationKey();
	 		$crypt = $this->file_factory->getClass("Crypt");
			$myString = str_replace(" ","+",$ref);
			$travID = $crypt->decrypt($myString);
			
			if( !is_numeric($travID) ){
				ob_end_clean();
	 			header("location: register.php");
	 			exit;
	 		}	
	 		$traveler = $this->file_factory->getClass("TravelerCB",array($travID));
	 		
 			$this->adminGroup->addMember($traveler);
			$this->adminGroup->removeFromInviteList($traveler->getTravelerID());
		    $this->adminGroup->removeFromRequestList($traveler->getTravelerID());
			
	 		$crypt = $this->file_factory->getClass("Crypt");
			$cryptTravID = $crypt->encrypt($travID);
			$cryptTravID = substr($cryptTravID,0,strlen($cryptTravID)-1);
			return $this->startSession($travID,$cryptTravID);
			
 		}
 		
 		function validateEmailPassword(){
 			/**
	 		* create database utility object
	 		* check for user using the same username and password
	 		*/
 			$nConnection = $this->file_factory->getClass("Connection");
	 		$nRecordset  = $this->file_factory->getClass("Recordset",array($nConnection));
	 		$nRecordset1 = $this->file_factory->getClass("Recordset",array($nConnection));
	 		$status = array("loginStatus" => constants::DISALLOW_ERROR, "travelerID" => 0);
			
			$loginData = $this->login_helper->readLoginData();
			$sqlquery = " SELECT travelerID,active,isSuspended,deactivated FROM (" .
	 					" SELECT tblTraveler.travelerID,active,isSuspended,tblTraveler.deactivated FROM tblTraveler,tblGrouptoTraveler" .
			 			" WHERE email             = '" . addslashes($loginData["email"]) .	"'" .
			 			" AND password               = '" . addslashes($loginData["password"]) . "'".
						" AND tblTraveler.isSuspended = 0 ".
						" AND tblTraveler.deactivated = 0 ".
			 			" AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
			 			" AND groupID                = " . $this->config->getGroupID() .
			 			" UNION ".
						" SELECT tblTraveler.travelerID,active,tblTraveler.isSuspended,tblTraveler.deactivated " .
			 			" FROM tblInviteList,tblTraveler,tblGroup " .
						" WHERE (tblTraveler.email = '". addslashes($loginData["email"]) ."'".
						" OR tblTraveler.username = '". addslashes($loginData["email"]) ."')".
						" AND tblTraveler.password = '". addslashes($loginData["password"]) ."'".
						" AND tblTraveler.isSuspended = 0 ".
						" AND tblTraveler.deactivated = 0 ".
						" AND tblTraveler.travelerID = tblInviteList.travelerID ".
						" AND (tblInviteList.status = 0 OR tblInviteList.status = 2) ".
						" AND (tblInviteList.groupID = ". $this->config->getGroupID() .
						" OR (tblInviteList.groupID = tblGroup.groupID".
						" AND tblGroup.parentID = ". $this->config->getGroupID() ."))".
			 			" ) AS qryLogin " .
			 			" GROUP BY travelerID";
 			   
	 		$myquery = $nRecordset->Execute($sqlquery);    
	 		
			if(0 == $nRecordset->Recordcount()){
				$sqlquery = " SELECT travelerID,active,isSuspended,deactivated FROM (" .
		 					" SELECT tblTraveler.travelerID,active,isSuspended,tblTraveler.deactivated FROM tblTraveler,tblGrouptoTraveler" .
				 			" WHERE email             = '" . addslashes($loginData["email"]) .	"'" .
				 			" AND password               = '" . addslashes($loginData["password"]) . "'".
				 			" AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
				 			" AND groupID                = " . $this->config->getGroupID() .
				 			" UNION ".
							" SELECT tblTraveler.travelerID,active,tblTraveler.isSuspended,tblTraveler.deactivated " .
				 			" FROM tblInviteList,tblTraveler,tblGroup " .
							" WHERE (tblTraveler.email = '". addslashes($loginData["email"]) ."'".
							" OR tblTraveler.username = '". addslashes($loginData["email"]) ."')".
							" AND tblTraveler.password = '". addslashes($loginData["password"]) ."'".
							" AND tblTraveler.travelerID = tblInviteList.travelerID ".
							" AND (tblInviteList.status = 0 OR tblInviteList.status = 2) ".
							" AND (tblInviteList.groupID = ". $this->config->getGroupID() .
							" OR (tblInviteList.groupID = tblGroup.groupID".
							" AND tblGroup.parentID = ". $this->config->getGroupID() ."))".
				 			" ) AS qryLogin " .
				 			" GROUP BY travelerID";
				$myquery = $nRecordset->Execute($sqlquery);
			}
	 		
	 		
 			
	 		if( $tmptrav = mysql_fetch_array($myquery) ){
	 			$status["travelerID"] = $tmptrav["travelerID"];
	 			if( $tmptrav['isSuspended'] )
	 				$status["loginStatus"] = constants::DISALLOW_SUSPENDED;
	 			elseif( !$tmptrav["active"] )
	 				$status["loginStatus"] = constants::DISALLOW_INACTIVE;
				elseif( $tmptrav["deactivated"] )
	 				$status["loginStatus"] = constants::DISALLOW_DEACTIVATED;
	 			else
	 				$status["loginStatus"] = constants::ALLOW;
	 		}
	 			
 			return $status;
 		}
 		
 		function startSession($travelerID,$sessionKey = ""){
 			
 			$crypt = $this->file_factory->getClass("Crypt");
	 		if( $travelerID != $crypt->decrypt($sessionKey) ){
	 			return $this->viewLoginPage();
	 		}
 			
 			$nConnection = $this->file_factory->getClass("Connection");
	 		$nRecordset = $this->file_factory->getClass("Recordset",array($nConnection));
		
			/** 
			* update database info for user
			* set session variables
			*/
			$appendQuery = "";
				if( $this->login_helper->isNewlyRegistered() )
					$appendQuery = ", active = 1 ";
			
			$sqlquery = "UPDATE tblTraveler set latestlogin='" . date('c') . "' " . $appendQuery . " WHERE travelerID = " . $travelerID;
			$nRecordset->Execute($sqlquery);
			
			$sessionManager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");
			$sessionManager->set("isLogin",1);
			$sessionManager->set("travelerID",$travelerID);
			$sessionManager->set("login",crypt($travelerID,CRYPT_BLOWFISH));
			$sessionManager->set("domain", $_SERVER["HTTP_HOST"]);
			
			// set session
			$session = $this->file_factory->invokeStaticClass("Session","getInstance",array($travelerID));
			$session->startSession();
			
			if( "" != $this->login_helper->getReferer() ){
				ob_end_clean();
		 		header("location: " . str_replace(array('^','\\'),array('#',''),$this->login_helper->getReferer()));
				exit;
			}
			
			ob_end_clean();
			header("location: passport.php");
		 	exit;	
		 		
 		}	
 		
 		function readyLoginForm($loginData = array()){
	 		
	 		$content = array(
				"username"				=>	isset($loginData["username"]) ? trim($loginData["username"]) : "",
				"password"				=>	isset($loginData["password"]) ? trim($loginData["password"]) : "",
				"loginType"				=>	isset($loginData["loginType"]) ? $loginData["loginType"] : constants::TRAVELER,
				"referer"				=>	$this->login_helper->getReferer(),
				"msg"					=>	isset($loginData["msg"]) ? trim($loginData["msg"]) : "",				
				"isRegister"			=>	$this->login_helper->isNewlyRegistered(),
				//"action"				=>	preg_replace("/\//","",$_SERVER["PHP_SELF"]),
				"action"				=>	'/login.php',
				"failed"				=>	isset($loginData["failed"]) ? $loginData["failed"] : false,
				"siteName"				=>	$this->config->getSiteName(),
				'displayRegisterLink'	=> false,
				'login_page'			=> 	$loginData['login_page']
			);
	 		
	 		return $content;
	 	}
	 	
	 	function viewLoginPage($loginData = array()){
		
			//added code fragment to redirect to dashboard if user is already logged in
			$session_manager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");
			if( $session_manager->getLogin() ){
				ob_end_clean();
				header("location: /travelerdashboard.php");
				exit;
			}
		
	 		$loginData['login_page'] = "true";
	 		
	 		$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('layoutID', 'page_default');
			Template::setMainTemplateVar('title', "Students and Advisor Login - {$this->config->getSiteName()} Network");
			Template::setMainTemplateVar('metaDescription',"Login area for {$this->config->getSiteName()} Network student travelers and advisors");
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );   
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			Template::includeDependentCss('/css/Thickbox.css');
			Template::includeDependentJs('/js/thickbox.js');
			
			$fcd = $this->file_factory->getClass('LoginFcd',$this->readyLoginForm($loginData), false);
			$obj_view->setContents( array('contents' => $fcd->retrieveLoginPage()) );
			$obj_view->render();
	 	}
	 	
	 	function activateAndLoginNewAccount($myString){
 			
 			$crypt = $this->file_factory->getClass("Crypt");
			$myString = str_replace(" ","+",$myString);
			$travID = $crypt->decrypt($myString);
			if( !is_numeric($travID) ){
				ob_end_clean();
	 			header("location: register.php");
	 			exit;
	 		}	
	 		$traveler = $this->file_factory->getClass("TravelerCB",array($travID));
	 		
	 		$this->adminGroup->addMember($traveler);
			$this->adminGroup->removeFromInviteList($traveler->getTravelerID());
		    $this->adminGroup->removeFromRequestList($traveler->getTravelerID());
	 		
	 		$nConnection = $this->file_factory->getClass("Connection");
	 		$nRecordset = $this->file_factory->getClass("Recordset",array($nConnection));
		
			/** 
			* update database info for user
			* set session variables
			*/
			
			$sqlquery = "UPDATE tblTraveler set latestlogin='" . date('c') . "', active = 1 WHERE travelerID = " . $traveler->getTravelerID();
			$nRecordset->Execute($sqlquery);
			
			$sessionManager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");
			$sessionManager->set("isLogin",1);
			$sessionManager->set("travelerID",$traveler->getTravelerID());
			$sessionManager->set("login",crypt($traveler->getTravelerID(),CRYPT_BLOWFISH));
			$sessionManager->set("domain", $_SERVER["HTTP_HOST"]);
			
			// set session
			$session = $this->file_factory->invokeStaticClass("Session","getInstance",array($traveler->getTravelerID()));
			$session->startSession();
		
			ob_end_clean();
			header("location: passport.php");
		 	exit;	
			
 		}
	 	
	}
?>