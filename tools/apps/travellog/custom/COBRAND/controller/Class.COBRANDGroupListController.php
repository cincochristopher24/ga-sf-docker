<?

require_once('travellog/controller/Class.GroupListController.php');
require_once('travellog/custom/COBRAND/model/Class.GroupCB.php');

class COBRANDGroupListController extends GroupListController
{
	
	function __construct(){
		
		parent::__construct();
		
	}
	
	
	function showMyGroupsPage($profile,$isAdminLogged=FALSE){
		/**
		$ordercond = new Condition;
		$ordercond->setAttributeName("adate");
		$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
		
		$order = new FilterCriteria2();
		$order->addCondition($ordercond);
					
		$grpFilteredGroups = $this->loggedUser->getGroups($order);
		
		$paging    =  new Paging( count($grpFilteredGroups), $this->page, 'mode=mygroups' , $this->numrecords );
		$paging->setOnclick('showLoading();manager.filterSearchGroup');
		$iterator  =  new ObjectIterator( $grpFilteredGroups,$paging    );
		
		$tpl 	= clone $this->main_tpl;
		$tpl->set_path("travellog/custom/COBRAND/views/");
		$tpl->set( 'paging'        , $paging         );
		$tpl->set( 'iterator'      , $iterator       );
				
		$tpl->set("loggedUser",$this->loggedUser);
		$tpl->set('mygroups', $this->mygroups);
		$tpl->set('totalrec', count($grpFilteredGroups));
		
		$tpl->out("tpl.ViewGroupList.php");	
		*/
		
		$this->main_tpl->set_path("travellog/custom/COBRAND/views/");
		$this->main_tpl->set("isAdminLogged",$isAdminLogged);
		parent::showMyGroupsPage($profile);
	}
	
	function viewMoreGroups()
	{
		
		$this->main_tpl->set_path("travellog/custom/COBRAND/views/");
		
		parent::viewMoreGroups();
		
	}


	function viewBySearchGroup()
	{
		
		//search group by name
		$cond = new Condition;
		$cond->setAttributeName("name");
		$cond->setOperation(FilterOp::$LIKE);
		$cond->setValue(addslashes($this->grpName));
		
		$filter = new FilterCriteria2();
		$filter->addCondition($cond);
		$filter->addBooleanOp('AND');
		
		// sort by date of membership desc  
		$ordercond = new Condition;
		$ordercond->setAttributeName("adate");
		$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
		
		$order = new FilterCriteria2();
		$order->addCondition($ordercond);
	
		if (NULL != $this->traveler)
			$filtertravID = $this->traveler->getTravelerID();
		else
			$filtertravID = $this->loggedUserID;
		
		$cond2 = new Condition;
		$cond2->setAttributeName("parentID");
		$cond2->setOperation(FilterOp::$NOT_EQUAL);
		$cond2->setValue(0);
		
		$filter->addCondition($cond2);
		$filter->addBooleanOp('AND');
		
		$grpFilteredGroups = GroupCB::getFilteredGroups($filter, null, $order, null, $filtertravID);
		
		if (TRUE == $this->mygroups)
			$qrystr = 'mode=mygroups';
		else
			$qrystr ='';
			
		$paging        =  new Paging( count($grpFilteredGroups), $this->page, $qrystr , $this->numrecords );
		$paging->setOnclick('showLoading();manager.filterSearchGroup');
		$iterator      =  new ObjectIterator( $grpFilteredGroups,$paging    );
		
		$tpl 	= clone $this->main_tpl;
		$tpl->set_path("travellog/custom/COBRAND/views/");
		
		$tpl->set( 'paging'        , $paging         );
		$tpl->set( 'iterator'      , $iterator       );
					
		// set values for group categories in dropdown box
		$tpl->set("grpCategories",$this->formHelpers->CreateOptions(GroupCategory::getGroupCategoriesList(), $this->grpType ) );	
		
		$tpl->set("loggedUser",$this->loggedUser);
		$tpl->set('mygroups', $this->mygroups);
		$tpl->set('totalrec', count($grpFilteredGroups));
		
		$tpl->set('search', true);
		
		$tpl->out("tpl.ViewGroupList.php");	
		
	}
	
}				
?>