<?php
	require_once 'travellog/controller/Class.AbstractAssignStaffController.php';

	class COBRANDAssignStaffController extends AbstractAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerClass('Config');
	 		$CONFIG            = $this->mFactory->getClass('Config');
			$GLOBALS['CONFIG'] = $CONFIG;
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
