<?php
/*
 * Created on 11 27, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

	require_once('travellog/controller/Class.AbstractVideoController.php');
	
 	class COBRANDVideoController extends AbstractVideoController {
 		
 		function __construct() {
 			parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
			$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
 		}
 		
 		function performAction() {
 			parent::performAction();
 		}
 	}