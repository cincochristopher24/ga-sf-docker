<?php
/*
 * Created on 11 12, 07
 * 
 * Author: J. LLano
 * Purpose: the default controller for "Profile" module for ganet cobrand sites
 * 
 */
 
 require_once('travellog/controller/Class.AbstractProfileController.php');
 
 class COBRANDProfileController extends AbstractProfileController {
 	
 	function __construct(){
 		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('Traveler',array('path'=>'travellog/custom/COBRAND/model/','file'=>'TravelerCB'));
		$this->file_factory->registerClass('SubNavigation',array('path'=>'travellog/custom/COBRAND/model/','file'=>'SubNavigationCB'));
		
		$this->file_factory->registerTemplate('IncMyGroups', array('path'=>'travellog/custom/COBRAND/views/','file'=>'IncMyGroups'));
		
		$this->file_factory->registerClass('Advertisement',array('path'=>'travellog/custom/COBRAND/model/','file'=>'AdvertisementCB'));
		// START -- Added By: Cheryl Ivy Q. Go   26 November 2007
		$this->file_factory->registerClass('CommentsView',array('path'=>'travellog/custom/COBRAND/views/','file'=>'CommentsViewCB'));
		$this->file_factory->registerClassInitCall('CommentsView',array('func'=>'setGroupId','params' => array('groupID'=>$GLOBALS['CONFIG']->getGroupID())));
		// End --  Added By: Cheryl Ivy Q. Go   26 November 2007
		
		// Do not show Ads for CoBranded Sites
		$this->showAds = FALSE;
	}
 	
 	function performAction() {
 		global $CONFIG;
 		$param['tID'] = $_GET['travelerID'];
		$param['gID'] = $CONFIG->getGroupID(); 
		
		$obj_group = $this->file_factory->getClass('AdminGroup');
		$obj_group->setGroupID($param['gID']);
		
		$travelerID = isset($_SESSION["travelerID"]) ? $_SESSION["travelerID"] : 0;
		$isStaffLogged = $obj_group->isStaffInNetwork($travelerID);
		$isAdminLogged = $CONFIG->isAdministrator($travelerID);
		
		if( $isStaffLogged || $isAdminLogged || $obj_group->isMemberOfGroupNetwork($param) ){
		
		}else if( !$obj_group->isMemberOfGroupNetwork($param) 
				|| (!$obj_group->isPendingMemberOfGroupNetwork($param['tID']) && !($isStaffLogged || $isAdminLogged)) ){
			header('Location:/index.php');
			exit;
		}
 		
 		parent::performAction();
 		
 	}
 	
 	
 }
 
?>
