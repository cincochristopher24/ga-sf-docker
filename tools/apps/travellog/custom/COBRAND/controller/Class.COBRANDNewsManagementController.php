<?php
	require_once('travellog/controller/Class.AbstractNewsManagementController.php');
 	class COBRANDNewsManagementController extends AbstractNewsManagementController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
			$this->pageTitle = $GLOBALS['CONFIG']->getSiteName().' News';		
 		}
 		
 		public function onloadView(){
			//if groupID is defined in the URL(that means we are in a cobrand subgroup context), 
			//check if that group is a subgroup of the cobrand group.
			if(array_key_exists('groupID',$_GET)){
				if($_GET['groupID'] != $GLOBALS['CONFIG']->getGroupID()){
					//check if the groupID belongs to a subgroup of the current cobrand
					try{
						$currentGroup = $this->file_factory->getClass('AdminGroup',array($_GET['groupID']));
					}
					catch(exception $e){
						$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
					}
					if($currentGroup->isSubGroup()){
						if($currentGroup->getParentID() != $GLOBALS['CONFIG']->getGroupID()){
							$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
						}
					}
					else{
						$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
					}
				}
			}
			else{
				$_GET['groupID'] = $GLOBALS['CONFIG']->getGroupID();
			}
			parent::onloadView();
 		}
 		
 	}
?>