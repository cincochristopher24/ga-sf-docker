<?php 

	require_once('travellog/controller/Class.AbstractCustomGroupSectionController.php');

	class COBRANDCustomGroupSectionController extends AbstractCustomGroupSectionController {

		public function __construct(){
			parent::__construct();
			$this->file_factory->registerClass('Config');
			$config = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $config;
		}
	}
?>