<?php
/*
 * Created on 11 12, 07
 * 
 * Author: J. LLano
 * Purpose: the default controller for "Photo" module for ganet cobrand sites
 * 
 */
 
 require_once('travellog/controller/Class.AbstractEditTravelerProfileController.php');
 
 class COBRANDEditTravelerProfileController extends AbstractEditTravelerProfileController {
 	
 	function __construct(){
 		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG; 
		
		$this->file_factory->registerTemplate('FrmEditTravelerProfile', array( 'path' => 'travellog/custom/COBRAND/views'));
		
		$this->file_factory->registerClass('Traveler',array('path'=>'travellog/custom/COBRAND/model/','file'=>'TravelerCB'));
		$this->file_factory->registerClass('SubNavigation',array('path'=>'travellog/custom/COBRAND/model/','file'=>'SubNavigationCB'));
	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 	
 	
 }
 
?>
