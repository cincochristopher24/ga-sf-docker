<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 26 November 2007
	 * Class.COBRANDCommentRequestController.php
	 */

	require_once 'travellog/controller/Class.AbstractCommentRequestController.php';

	 class COBRANDCommentRequestController extends AbstractCommentRequestController {

	 	function __construct(){

			parent::__construct();

			$this->file_factory->registerClass('Config');
	 		$CONFIG            = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $CONFIG;

			$this->file_factory->registerClass('CommentRequestController', array('path'=>'travellog/service/comments', 'file'=>'CommentRequestController'));
			$this->file_factory->registerClassInitCall('CommentRequestController', array('func'=>'setGroupID', 'params' => array('groupID'=>$GLOBALS['CONFIG']->getGroupID())));
		}

	 	function performAction() {
	 		parent::performAction();
	 	}
	 }
?>