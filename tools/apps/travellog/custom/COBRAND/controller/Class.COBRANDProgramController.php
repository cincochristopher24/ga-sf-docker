<?php
	require_once('travellog/controller/Class.AbstractProgramController.php');
	class COBRANDProgramController extends AbstractProgramController{
	
		function __construct(){
		
			parent::__construct();
		
			$this->file_factory->registerClass('Config');
	 		$CONFIG            = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $CONFIG;
		
			$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'SubNavigationCB'));
			$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
		}
	
		function performAction(){
			global $CONFIG;
			$this->data           = array_merge($_GET, $_POST);
			$this->data['gID']    = ( isset($this->data['gID']) )? $this->data['gID'] : $CONFIG->getGroupID();
			$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'View';
			if( isset($this->data['context']) ){
				$activity_controller = $this->file_factory->getClass('ActivityController');
				$activity_controller->performAction();   				
			}
			else{
				$this->__applyRules();
				parent::performAction();
			}	        
		} 
		
		protected function __applyRules(){ 
			global $CONFIG;
			if( $this->data['gID'] != $CONFIG->getGroupID() ){
				$obj_group_factory = $this->file_factory->invokeStaticClass('GroupFactory');
				$obj_group         = $obj_group_factory->create( array($this->data['gID']) );
				if( get_class($obj_group[0]) == 'AdminGroup' && $obj_group[0]->getParentID() != $CONFIG->getGroupID() ){
					ToolMan::redirect('/group.php');  
					exit;
				}
			}
			parent::__applyRules();   
		}
	}
?>