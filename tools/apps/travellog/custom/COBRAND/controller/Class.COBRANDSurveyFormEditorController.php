<?php
	require_once('travellog/controller/Class.AbstractSurveyFormEditorController.php');
 	class COBRANDSurveyFormEditorController extends AbstractSurveyFormEditorController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
 		}
 	}
?>