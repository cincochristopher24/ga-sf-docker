<?php
	/*
	 * Class.COBRANDMessagesController.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/controller/Class.AbstractNewsController.php");
	 
	 class COBRANDNewsController extends AbstractNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerClass('Config');
	 		$this->file_factory->registerClass('TravelerCB', array("path" => "travellog/custom/COBRAND/model"));
	 		$this->file_factory->registerClass('SubNavigationCB', array("path" => "travellog/custom/COBRAND/model"));
	 		$this->file_factory->registerClass('AdminGroup', array("path" => "travellog/model"));
	 		
	 		$this->traveler = $this->file_factory->getClass("TravelerCB",array($this->session_manager->get("travelerID")));
	 		$GLOBALS['CONFIG'] = $this->config = $this->file_factory->getClass('Config');
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');
		}
		
	}
?>