<?

require_once('travellog/controller/Class.GroupController.php');
require_once('travellog/custom/COBRAND/model/Class.GroupMessagePanelCB.php');
require_once('travellog/custom/COBRAND/model/Class.SubNavigationCB.php');

class COBRANDGroupViewController extends GroupController
{
	
	function __construct(){
		
		parent::__construct();
		
		$this->main_tpl->set_path("travellog/custom/COBRAND/views/");
		
	}
	
	public function prepareMessageCenter() {
		parent::prepareMessageCenter();
	}

//	function prepareMessageCenter(){
//	
//		$mp = new GroupMessagePanelCB();
//   		$mp->setContext($this->group); 
//   		$mp->setIsMemberLogged($this->isMemberLogged);  
//   		$mp->setIsAdminLogged($this->isAdminLogged);  		
//   		$this->IncMessageCenter = $mp;
//	}
	
	
	function prepareLogin() {
		
		$obj_factory = ControllerFactory::getInstance();
		$this->IncGroupLogin = $obj_factory->createController('Login')->performAction();
		
	}
	
	
	function prepareGroupAlbums() {
		if ($this->group) {
			
			require_once('travellog/model/Class.PhotoAlbumAdapter.php');
			$PhotoAlbumAdapter = new PhotoAlbumAdapter();
			$PhotoAlbumAdapter->setLoguserID($this->loggedUserID);
			
			
			$grpPhotoAlbums = $PhotoAlbumAdapter->getAlbums($this->group);
			$this->hasPhotos = count($grpPhotoAlbums) ? TRUE : FALSE;
			$featuredAlbums = $PhotoAlbumAdapter->getFeaturedAlbums($this->group);
			if( count($featuredAlbums) ){
				$grpPhotoAlbums = $featuredAlbums;
			}
			
			$grpIncAlbum = clone $this->main_tpl;
			$grpIncAlbum->set_path("travellog/views/");
			$grpIncAlbum->set("isAdminLogged",$this->isAdminLogged);
			$grpIncAlbum->set("grpID",$this->groupID);
			$grpIncAlbum->set("loggedUserID",$this->loggedUserID);
			$grpIncAlbum->set("grpPhotoAlbums",$grpPhotoAlbums);
			
			$photos = array();
			if(count($grpPhotoAlbums)>0){
				foreach($grpPhotoAlbums as $i =>$album){
					if($album['context']=="photoalbum"){
						if($album['numphotos'] > 0){
							//if ($this->loggedUserID == $album['creator'] || $album['publish'])
								$photos[] = count($photos)+$album['numphotos'];	
						}
					}else{
						if ($this->loggedUserID == $album['creator'] || $album['publish']){
								$photos[] = count($photos)+$album['numphotos'];	
						}
					}
				}
			}
				
			if($this->isAdminLogged || count($photos) >0){
				$this->IncGroupAlbums = $grpIncAlbum->fetch("tpl.IncGroupPhotoAlbums.php");	
				
				require_once("travellog/UIComponent/Photo/facade/PhotoUIFacade.php");
				$Photo = new PhotoUIFacade();
				$Photo->setContext('group');
				$Photo->setGenID($this->groupID);
				//$Photo->setLoginID($this->loggedUser?$this->loggedUser->getTravelerID():0);
				$Photo->setLoginID(0);
				$Photo->setGroupHasPhotosInAlbum($this->isShowDependentjScript);
				$Photo->build();
				
			
			
			}else{
				$this->isShowDependentjScript = false;
			}
			
		}
	}
	
	/**
	function prepareSubGroups(){
		
		$origCountSubGroups = 0;
		
		$rowslimitnum = 5;
		$rowslimit = new RowsLimit($rowslimitnum, 0);
		
		$cond = new Condition;
		$cond->setAttributeName("subgrouprank");
		$cond->setOperation(FilterOp::$ORDER_BY);
	
		$cond2 = new Condition;
		$cond2->setAttributeName("datecreated");
		$cond2->setOperation(FilterOp::$ORDER_BY_DESC);
		
		$order = new FilterCriteria2();
		$order->addCondition($cond);
		$order->addCondition($cond2);
		
		$grpSubGroups =  $this->group->getSubGroups($rowslimit, $order);
		
		//get count subgroups
		$origCountSubGroups = $this->group->getSubGroups(null,null,true);
		
		
		if ( TRUE == $this->group->isParent() && (TRUE == $this->isMemberLogged || TRUE == $this->viewPrefGroups ) &&  (0 < count($grpSubGroups)  || $this->isAdminLogged ) ) {
			
			$grpSubGroupsTemplate = new Template();
			$grpSubGroupsTemplate->set_path("travellog/views/");
			$grpSubGroupsTemplate->set("loggedUser",$this->loggedUser);	
			$grpSubGroupsTemplate->set("isLogged",$this->isLogged);	
			$grpSubGroupsTemplate->set("isAdminLogged",$this->isAdminLogged);
			$grpSubGroupsTemplate->set("grpID",$this->groupID);
			
			$grpSubGroupsTemplate->set("origCountSubGroups",$origCountSubGroups);
			
			if ($this->isAdminLogged)
				$grpSubGroupsLinkLabel = 'Manage Groups' ;
			else if ($origCountSubGroups > $rowslimitnum)	
				$grpSubGroupsLinkLabel = 'View All ' . $origCountSubGroups  . ' Groups' ;
					
			$grpSubGroupsTemplate->set("grpSubGroupsLinkLabel",(isset($grpSubGroupsLinkLabel))? $grpSubGroupsLinkLabel : '' );
			
			$grpSubGroupsTemplate->set("grpSubGroups",$grpSubGroups);
			
			$this->IncGroupSubGroups = $grpSubGroupsTemplate->fetch("tpl.IncGroupSubGroups.php");
			$this->viewSubGroups = true;
		}
		
	}
	*/
	
	public function prepareGroupSteps(){
		parent::prepareGroupSteps();
	}
		
	function viewGroup() {
		$this->main_tpl->set("grpID"			  ,  $this->groupID);
		$this->main_tpl->set("group"			  ,  $this->group);
		$this->main_tpl->set("grpDiscriminator"	  ,	 $this->grpDiscrim);

		$this->main_tpl->set("isLogged"           ,  $this->isLogged);	
		$this->main_tpl->set("isMemberLogged"     ,  $this->isMemberLogged);	
		$this->main_tpl->set("isAdminLogged"      ,  $this->isAdminLogged);	
		
		$this->main_tpl->set("loggedUser",$this->loggedUser);
		
		$this->main_tpl->set('grpInfoTemplate'    ,  $this->IncGroupInfo);
		$this->main_tpl->set('memberTemplate'     ,  $this->IncGroupMembers);
		$this->main_tpl->set('mainGroupInfo_view' , $this->mainGroupInfo); 
		$this->main_tpl->set('grpDiscussionsTemplate', $this->IncGroupDiscussions); // added by chris

		$this->main_tpl->set('grpCalendarTemplate',  $this->IncGroupCalendar);
		$this->main_tpl->set('grpMsgCenterTemplate',  $this->IncMessageCenter);
		
		$this->main_tpl->set("isParent"            ,  $this->group->isParent());
		$this->main_tpl->set('grpSurveyTemplate',  $this->IncGroupSurveys);
		$this->main_tpl->set('grpFilesTemplate'    ,  $this->IncGroupFiles);
		$this->main_tpl->set('grpSubGroupsTemplate',  $this->IncGroupSubGroups);
		$this->main_tpl->set('grpCustomSectionTemplate',  $this->IncGroupCustomSection);
		$this->main_tpl->set('grpArticlesTemplate',  $this->IncGroupArticles);
		$this->main_tpl->set('viewGroupArticles',  $this->viewGroupArticles);
		
		$this->main_tpl->set('groupTagCloud', $this->IncGroupTagCloud);
		
		/****************************************************************************************
		 * displayed the journals template only if viewFeaturedJournals is true: neri - 01/22/09
		 ****************************************************************************************/
		
		if ($this->viewFeaturedJournals)
			$this->main_tpl->set('grpJournalsTemplate',  $this->IncGroupJournals);
		
		$this->main_tpl->set('grpImportantLinksTemplate',  $this->IncGroupImportantLinks);
		$this->main_tpl->set('grpRecentVideosTemplate',  $this->IncGroupRecentVideos);	// added by neri: 06-10-09
		$this->main_tpl->set("viewCalendar"		 , $this->viewCalendar);	
		$this->main_tpl->set("viewResourceFiles" , $this->viewResourceFiles);
		$this->main_tpl->set("viewSubGroups"	 , $this->viewSubGroups);	
		$this->main_tpl->set('viewGroupSteps',  $this->viewGroupSteps);
		//$this->main_tpl->set("viewCustomSection"	 , $this->viewCustomSection); //not needed in tpl	
		//$this->main_tpl->set('viewLogin'          , $this->IncGroupLogin); //not needed in tpl
		$this->main_tpl->set('viewAlbums'        , $this->IncGroupAlbums);	
		$this->main_tpl->set("viewImportantLinks" , $this->viewImportantLinks);
		$this->main_tpl->set('isShowDependentjScript',  $this->isShowDependentjScript);
		$this->main_tpl->set("subNavigation",$this->subNavigation);			

		if( $this->isLogged && !$this->isAdminLogged ){
			$this->isSuperStaffLogged = $this->group->isSuperStaff($this->loggedUserID);
		}
		$this->main_tpl->set("isSuperStaffLogged",$this->isSuperStaffLogged);
		if( !$this->isLogged || $this->isAdminLogged || $this->isSuperStaffLogged ){//added by Jul for custom page header photos
			require_once("travellog/views/custom_pageheader/Class.ViewRotatingPhoto.php");
			$grpRotatingPhoto = new ViewRotatingPhoto();
			$grpRotatingPhoto->setGroup($this->group);
			$this->main_tpl->set("grpRotatingPhoto",$grpRotatingPhoto);
		}
		
		$this->main_tpl->out("tpl.GroupHomepage.php");	

	}
	
	/*function prepareGroupJournals(){ 		
 		require_once('travellog/custom/COBRAND/views/Class.TravelsViewCB.php');
 		$grpJournals = new TravelsViewCB($this->group);
		$grpJournals->setTravelerID($this->loggedUserID);
		
		$this->IncGroupJournals = $grpJournals;
		$this->viewGroupJournals = (0 < count($this->group->getTravels()) || $this->isAdminLogged ) ? true : false;
 	}*/
	
	
}				
?>