<?php
	require_once 'travellog/controller/Class.AbstractJournalDraftController.php';
	
	class COBRANDJournalDraftController extends AbstractJournalDraftController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerClass('Config');
	 		$CONFIG            = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $CONFIG;
		}
		
	}
	
