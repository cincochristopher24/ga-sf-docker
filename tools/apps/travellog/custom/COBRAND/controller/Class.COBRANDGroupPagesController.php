<?php
require_once('travellog/controller/Class.AbstractGroupPagesController.php');
class COBRANDGroupPagesController extends AbstractGroupPagesController{
	
	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
	
		$this->file_factory->registerClass('GroupController' , array('path' => 'travellog/custom/COBRAND/controller/', 'file'=>'COBRANDGroupViewController'));
		
		$this->file_factory->registerClass('GroupListController' , array('path' => 'travellog/custom/COBRAND/controller/', 'file'=>'COBRANDGroupListController'));
		
		$this->file_factory->registerClass('GroupMessagePanel' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'GroupMessagePanelCB'));
		
		$this->file_factory->registerClass('Traveler' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'TravelerCB'));
		
		$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
		
		$this->file_factory->registerClass('PokesView' , array('path' => 'travellog/custom/COBRAND/views/', 'file'=>'PokesViewCB'));
		
	}
	
	function performAction(){
		
		// set the config groupID as get variable, if not groupID is defined
		$gID = (isset($_GET['gID'])) ? $_GET['gID'] : 0;
		$gID = (isset($_POST['gID'])) ? $_POST['gID'] : $gID;
		$_POST['gID'] = (0 < $gID) ? $gID : $GLOBALS['CONFIG']->getGroupID() ;
		
		/*added by Jul to restrict non-cobrand members*/
		if( $this->obj_session->getLogin() ){
			$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($GLOBALS['CONFIG']->getGroupID()));
			$this->group = $_group[0];
			if( !$this->group->isMember(new Traveler($this->obj_session->get("travelerID"))) ){
				header("location: /membership.php?gID=".$GLOBALS['CONFIG']->getGroupID()."&mode=acceptinvite");
			}
		}
		
		parent::performAction();
			
	}
	
	function _defineCommonAttributes(){
		$this->params["gID"] = $GLOBALS["CONFIG"]->getGroupID();
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		Template::setMainTemplateVar('page_location','Groups / Clubs');
		
		if( is_null($this->group) ){
			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($this->params['gID']));
			$this->group = $mGroup[0];
		}
		$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? TRUE : FALSE;
		$this->loggedUser = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );	
	
		if( isset($this->params["search"]) || !$this->isAdminLogged ){
			Template::setMainTemplateVar('page_location','Groups / Clubs');
		}else{
			Template::setMainTemplateVar('page_location','Home');
		}
		
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		
		$this->subNav = $this->file_factory->getClass('SubNavigation');
		$this->subNav->setContext('GROUP');
		$this->subNav->setContextID($this->params['gID']);
		$this->subNav->setLinkToHighlight('SUBGROUPS');
		
		$this->profile = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		$this->profile->init($this->group->getGroupID());
		
		$this->groupTags = GanetPageTagPeer::retrieveByGroupID($this->group->getGroupID());
		$groupTagData = array();
		foreach($this->groupTags as $iGroupTag){
			$groupTagData[] = $iGroupTag->getName();
		}
		$this->tpl->set('groupTagData',$groupTagData);
	}
	
	function _viewGroupPagesSearch(){
		$this->_defineCommonAttributes();
		parent::_prepareDependencies();
		
		$groupCount = GroupPeer::countSubgroups($this->group);
		$this->tpl->set("searchForm",parent::_fetchSearchForm());
		
		if( count($this->tags) ){
			$rowsLimit = $this->pagesHelper->getRowsLimit($this->params["page"]);
			$limitStr = "LIMIT " . $rowsLimit->getOffset() . ", " . $rowsLimit->getRows();
			
			$taggedGroups = GroupPeer::getTaggedGroups($this->group, $this->tags, $limitStr);
			$totalTaggedGroupsCount = GroupPeer::countTaggedGroups($this->group,$this->tags);
			
			$this->tpl->set("breadcrumbs",parent::_fetchBreadCrumbs($totalTaggedGroupsCount));
			$this->tpl->set("defaultGroups",parent::_fetchTaggedGroups($taggedGroups,$totalTaggedGroupsCount));
			$this->tpl->set("narrowTagCloud",parent::_fetchTagCloud('narrow'));
		
			if( !$this->isAdminLogged || isset($this->params["search"]) ){
				$this->tpl->set("randomSections",parent::_fetchRandomSections());
			}
		}else{
			$this->tpl->set("searchResultsHeader",parent::_fetchSearchResultsHeader($groupCount));
			
			$featuredGroups = array();
			$otherGroups = array();
			if( $this->isAdminLogged && !isset($this->params["search"]) ){
				$featuredGroups = GroupPeer::getFeaturedGroups($this->group);
				$otherGroups = GroupPeer::getRecentSubgroups($this->group);
			}else{
				$otherGroups = GroupPeer::getRecentSubgroups($this->group,$includeFeatured=TRUE);
			}
			$this->tpl->set("defaultGroups",parent::_fetchDefaultGroups($featuredGroups,$otherGroups));
			$this->tpl->set("narrowTagCloud",parent::_fetchTagCloud('wide'));
		}
		
		$this->tpl->set("configScript",parent::_fetchGroupPagesConfigScript());
		
		$this->tpl->out("tpl.GroupPagesSearch.php");
	}
	
	function _cobrandFindByName(){
		$this->_defineCommonAttributes();
		parent::_prepareDependencies();
		
		$this->tpl->set("searchForm",parent::_fetchSearchForm());
		
		$results = $this->group->getSubGroups($this->pagesHelper->getRowsLimit($page=1), $order = NULL, $isCount = false, $searchKey=$this->params["keyword"]);
		$resultCount = $this->group->getSubGroups(NULL, $order = NULL, $isCount = TRUE, $searchKey=$this->params["keyword"]);
		
		$this->tpl->set("defaultGroups",parent::_fetchDefaultGroupsByName($results,$resultCount));
		$this->tpl->set("narrowTagCloud",parent::_fetchTagCloud('wide'));
		
		$this->tpl->set("searchResultsHeader",parent::_fetchSearchResultsHeader($resultCount));
		
		$this->tpl->set("configScript",parent::_fetchGroupPagesConfigScript());
		
		$this->tpl->out("tpl.GroupPagesSearch.php");
	}
}