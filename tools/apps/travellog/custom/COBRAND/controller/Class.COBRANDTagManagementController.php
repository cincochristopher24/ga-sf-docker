<?php
require_once('travellog/controller/pageTags/Class.AbstractTagManagementController.php');
class COBRANDTagManagementController extends AbstractTagManagementController{
	
	function __construct(){
		parent::__construct();
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'SubNavigationCB'));
	}
	
	function performAction(){
		$this->data           = array_merge($_GET, $_POST);
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'view';  
		parent::performAction();
	} 
}
?>
