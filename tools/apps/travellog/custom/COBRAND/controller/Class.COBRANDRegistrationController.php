<?php
	/*
	 * Class.COBRANDRegistrationController.php
	 * Created on Nov 22, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/controller/Class.TravelerRegistrationController.php");
	
	class COBRANDRegistrationController extends TravelerRegistrationController{
		
		function __construct(){
			parent::__construct();
			$this->file_factory->registerClass('Config');
			$this->file_factory->registerClass('AdminGroup', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('COBRANDRegisterHelper', array('path' => 'travellog/helper/'));
			$this->file_factory->registerClass('COBRANDLoginController', array('path' => 'travellog/custom/COBRAND/controller'));
			$this->file_factory->registerClass('MailGeneratorCB', array('path' => 'travellog/custom/COBRAND/model'));
			$GLOBALS['CONFIG'] = $this->config = $this->file_factory->getClass('Config');
			
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/'); 
			
			$this->register_helper = $this->file_factory->invokeStaticClass('COBRANDRegisterHelper', 'getInstance');
		}
		
		function performAction(){
			
			$myGroup = $this->file_factory->getClass("AdminGroup", array($this->config->getGroupID()));
			
			switch( $this->register_helper->getAction() ){
				
				case constants::SUBMIT_DETAILS:
					return $this->submitDetails();
					break;
					
				case constants::CONFIRM_DETAILS:
					return $this->registerUser();
					break;		
					
				case constants::BACK:
					return $this->modifyDetails();
					break;
						
				default:
					
					$myEmail = $this->register_helper->getInvitedEmail();
					if( $attributes = $myGroup->getEmailInviteAttribute($myEmail, true) ){
						return $this->viewRegistrationForm(
								array(	"emailAdd"	=>	$myEmail,
										"firstname"	=>	$attributes['firstname'],
										"lastname"	=>	$attributes['lastname'],
										"fromEmailInvite" => TRUE));
					}
					else{
						if( $myGroup->directSignupAllowed() ){
							//check if login via service provider (twitter/google)
							$userData = $this->getServiceProviderAuthorization();
							if( $userData ){
								return $this->viewRegistrationForm(array(
													"emailAdd" 	=> 	$userData["email"],
													"username"	=>	TravelerPeer::composeUniqueUsername($userData["screen_name"]),
													"firstname"	=>	$userData["first_name"],
													"lastname"	=>	$userData["last_name"],
													"service_provider"	=>	$userData["service_provider"] ));
							}else{
								return $this->viewRegistrationForm();
							}
						}else{
							header("location: /");
							exit;
						}
					}
			}
		}
		
		function saveUserDetails($tmpRow){
			
			//$travKeys = parent::saveUserDetails($tmpRow);
			$travKeys = $this->beforeSaveCommonUserDetails($tmpRow);
			
			$traveler = $this->file_factory->getClass("Traveler",array($travKeys["travelerID"]));
			// add to group
			$group = $this->file_factory->getClass("AdminGroup",array($this->config->getGroupID()));
			$group->addMember($traveler);
			//$group->addInvites(array($traveler));
			
			$cobrandData = array( 	"cobrandSiteName" 	=> $GLOBALS['CONFIG']->getSiteName(),
									"cobrandEmailName" 	=> $GLOBALS['CONFIG']->getEmailName() );
									
			parent::saveCommonUserDetails($traveler->getTravelerID(),$traveler->getEmail(),$cobrandData);
			
			return array("travelerID" => $traveler->getTravelerID());
		}
		
		function beforeSaveCommonUserDetails($tmpRow){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
						
			$rs->execute("DELETE FROM tblTmpRegister " .
				"WHERE registerID = '" . mysql_real_escape_string($tmpRow["registerID"]) . "' " .  
				"AND regKey = '" . mysql_real_escape_string($tmpRow["regKey"]) . "'");
			$sqlquery = "INSERT INTO tblSendable(sendabletype) VALUES(1)";
			$myquery = $rs->Execute($sqlquery); 
			$sendableID = mysql_insert_id($conn->GetConnection()); 
			$sqlquery = "INSERT INTO tblTraveler SET " .
					"firstname = '" . mysql_real_escape_string($tmpRow["genName"]) . "', " .
					"lastname = '" . mysql_real_escape_string($tmpRow["lastName"]) . "', " . 
					"username = '" . mysql_real_escape_string($tmpRow["username"]) . "', " .
					"password = '" . mysql_real_escape_string($tmpRow["password"]) . "', " .
					"email = '" . mysql_real_escape_string($tmpRow["email"]) . "', " . 
					"university = '" . mysql_real_escape_string($tmpRow["university"]) . "', " . 
					"sendableID = $sendableID, " .
					"dateregistered = now()"; //dateregistered added by ianne - 11/25/2008
			$myquery = $rs->Execute($sqlquery);
			$travelerID = mysql_insert_id();
			/**
			 * get the travelerID of the user
			 * insert needed information in database for forums
			 */
						
			// insert traveler ranking
			$sqlquery = "INSERT INTO tblTravelerRanking SET travelerID = $travelerID";
			$rs->Execute($sqlquery);	
			
			// insert privacy preference
			$rs->Execute("INSERT INTO tblPrivacyPreference SET " .
					"preferencetype = 18, " .
					"preference = 5," .
					"travelerID = {$travelerID} ");		
			
			if( FALSE === strpos($_SERVER["SERVER_NAME"],"dev") 
				&& FALSE === strpos($_SERVER["SERVER_NAME"],".local")
				&& FALSE === strpos($_SERVER["SERVER_NAME"],"test.goabroad.net")
				&& FALSE === strpos($_SERVER["SERVER_NAME"],"staging.goabroad.net") ){//please retain this condition to test registration in local and dev                                                                   
			// added by bert
			WebServiceUser::addTravelerToWebServiceUser($travelerID);
			// added by nash
			WebServiceUser::addTravelerToTravelBioUser($travelerID);
			}
			
			/*$this->saveCommonUserDetails($travelerID,$tmpRow["email"]);
			
			if($tmpRow["subscribeNL"])
				$this->subscribeNL($tmpRow["email"]);*/
				
			return array("travelerID" => $travelerID);
		
		}
		
		function saveDetailsToTempTable( &$travelerData = array() ){
			parent::saveDetailsToTempTable($travelerData);
			$travelerData["user"] = constants::TRAVELERCB;
		}
		
		function viewRegistrationForm($content = array()){
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			$security = $this->file_factory->invokeStaticClass("Captcha","createImage");
			
			$travText = '<p>As a ' . $this->config->getSiteName() . ' Network Traveler, you will be able to</p>
				<ul style="margin-left:23px;list-style-type:disc;">
					<li>meet and be friends with travelers from all over the world</li>
					<li>blog about your travels in your very own travel journals</li>
					<li>post and share travel photos</li>
					<li>join groups of like minded travelers</li>
					<li>post messages for your friends and groupmates</li>
					<li>search for programs for your travels abroad</li>
					<li>and lots of other features rolling out in the following weeks!</li>
				</ul>';
				
			$nContent = array(
				"arrErrorMessages"	=>	isset($content["arrErrorMessages"]) ? $content["arrErrorMessages"] : array(),		
				"lastname"			=>	isset($content["lastname"]) ? trim($content["lastname"]) : "",
				"firstname"			=>	isset($content["firstname"]) ? trim($content["firstname"]) : "",
				"usrname"			=>	isset($content["username"]) ? trim($content["username"]) : "",
				"password"			=>	isset($content["password"]) ? trim($content["password"]) : "",
				"emailAdd"			=>	isset($content["emailAdd"]) ? trim($content["emailAdd"]) : "",
				"subscribeNL"		=>	isset($content["subscribeNL"]) ? $content["subscribeNL"] : 0,
				"regKey"			=>	isset($content["regKey"]) ? trim($content["regKey"]) : "",
				"regID"				=>	isset($content["regID"]) ? trim($content["regID"]) : "",
				'university'		=>	isset($content['university']) ? trim($content['university']) : '',
				'chkStudent'		=>	isset($content['chkStudent']) ? trim($content['chkStudent']) : '',				
				"security"			=>	$security,
				'travText'			=>	$travText,
				'agreeTerm'			=>	isset($content['agreeTerm']) ? $content['agreeTerm'] : 0,
				'siteName'			=> 	$this->config->getSiteName(),
				'service_provider'	=> isset($content['service_provider']) ? $content['service_provider'] : NULL,
				'fromEmailInvite'	=> isset($content['fromEmailInvite']) ? $content['fromEmailInvite'] : FALSE,
			);
			
			Template::includeDependentCss('/min/g=RegistrationCss');
			Template::includeDependentJs('/min/g=RegistrationJs');
			Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
			Template::includeDependentJs('/js/interactive.form.js');
			Template::includeDependentJs('js/tools/common.js');
			Template::includeDependentJs('js/utils/json.js');
			Template::includeDependentJs('captcha/captcha.js');
			$code = <<<STYLE
			<script type="text/javascript">
				var Captcha = new CaptchaLoader({					
					imageTagID	: 'captchaImg',
					onLoad		: function(params){			
						$('encSecCode').value  = params['code'];
					},
					imgAttributes : {
						width : '200',
						height: '50'	
					}
				});
				
				function changeStatusOnClick(){
					if( document.getElementById('chkStudent').checked )
						document.getElementById('liStudentBox').style.display = 'block';
					else
						document.getElementById('liStudentBox').style.display = 'none';
				}
			</script>
STYLE;

			Template::includeDependent($code);
			Template::setMainTemplateVar('layoutID', 'page_default');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
			//Template::includeDependentJs('/js/popup.js');
			Template::setMainTemplateVar('title', 'Register as Traveler - Join the ' . $this->config->getSiteName() . ' Network!');
			Template::setMainTemplateVar('metaDescription', 'Become friends with other travelers on the site, post messages for each other, join interesting forums and form clubs with those who share your interests.');
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			
			$fcd = $this->file_factory->getClass('RegisterFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveTravelerRegistrationForm()) );
			$obj_view->render();
			
		}
		
		function saveCommonUserDetails($travelerID,$email,$subscribeNL = 0){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			
			/**
			 * remove user from invite list
			 */
			$sqlquery = "SELECT groupID FROM tblEmailInviteList WHERE email = '" . addslashes($email) . "'";
			$myquery = $rs->Execute($sqlquery);
			if( mysql_num_rows($myquery) ){
				$tmpTraveler = $this->file_factory->getClass("Traveler",array($travelerID));
				$factory = $this->file_factory->invokeStaticClass("GroupFactory","instance");
				while( $tmpGroupID = mysql_fetch_array($myquery) ){
					$group = $factory->create( array($tmpGroupID["groupID"]) );
					if( $tmpGroupID != $this->config->getGroupID() )
						$group[0]->addInvites( array($tmpTraveler) );	
					$group[0]->removeFromEmailInviteList($email);
				}
			}
			 
			$mailGenerator = $this->file_factory->getClass("MailGeneratorCB", array($travelerID, 0, $this->config));
			$mailGenerator->send();
			
			/**
			* if user wants to subscribe in GoAbroad Newsletter
			*/
			
			if ( $subscribeNL ){
				/**
				  * check if user is already a subscriber
				  * subscribe the user to the GoAbroad Newsletter if not a subscriber and send a confirmation letter
				  */								
				$sqlquery = "select studentemailID from GoAbroad_Main.tbstudentemail where email = '" . addslashes($email) . "'";
				$myquery = $rs->Execute($sqlquery);
				if ( mysql_num_rows($myquery) == 0 ){
					
					$sqlquery = "insert into GoAbroad_Main.tbstudentemail (email, send, datecreated)
								  VALUES ('" . addslashes($email) . "', 1, '" . date('c') . "' )";
					$rs->Execute($sqlquery);
					unset($mailGenerator);
					$mailGenerator = $this->file_factory->getClass("MailGenerator", array($travelerID,1,$this->config));
					$mailGenerator->send();	
					
				}
			}		
			
		}
		
		function registerUser(){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			
			$travelerData = $this->register_helper->readTravelerData();
			
			$sqlquery = "SELECT * FROM tblTmpRegister " .
				"WHERE registerID = '" . addslashes($travelerData["regID"]) . "' " .  
				" AND regKey = '" . addslashes($travelerData["regKey"]) . "'";
			$myquery = $rs->Execute($sqlquery);
			
			if($tmpRow = mysql_fetch_array($myquery)){
				$arrTrav = $this->saveUserDetails($tmpRow);		
				$crypt = $this->file_factory->getClass("Crypt");
				$cryptTravID = $crypt->encrypt($arrTrav['travelerID']);
				$cryptTravID = substr($cryptTravID,0,strlen($cryptTravID)-1);
				$registerLoginController = $this->file_factory->getClass('COBRANDLoginController');
				return $registerLoginController->activateAndLoginNewAccount($cryptTravID); 
			}
			else{
				header("location:" . $this->register_helper->getRedirectPage(constants::REGISTER_PAGE));
			}
		
		}
		
		protected function getServiceProviderAuthorization(){
			$context = isset($_GET["context"]) ? $_GET["context"] : NULL;
			if( !in_array($context,array("twitter","google","facebook")) ){
				return FALSE;
			}
			$oauth_token = isset($_GET["oauth_token"]) ? $_GET["oauth_token"] : NULL;
			if( is_null($oauth_token) ){
				return FALSE;
			}
			
			$oauth_api = OAuthAPIFactory::getInstance()->create($context);
			$credentials = $oauth_api->getCredentials();
			if( !$credentials ){
				$credentials = $oauth_api->validateUser($oauth_token);
			}
			if( $credentials && $credentials["registered_traveler_id"] ){
				if( !$this->loginAuthorizedUser($credentials["registered_traveler_id"],$oauth_api) ){
					$myGroup = $this->file_factory->getClass("AdminGroup", array($this->config->getGroupID()));
					$groupAccess = $myGroup->getGroupAccess();
					$directSignupAllowed = $myGroup->directSignupAllowed();
					
					if( !$directSignupAllowed				//direct signup not allowed
						|| 4 == $groupAccess 				//group access CLOSE
						|| 3 == $groupAccess				//group access INVITE ONLY
						|| 2 == $groupAccess ){				//group access OPEN_APPROVAL
						$oauth_api->clearCredentials();
						header("location: /");
						exit;
					}
				}
			}
			return $credentials;
		}
		
		protected function loginAuthorizedUser($registered_traveler_id,$oauth_api){
			$this->file_factory->registerClass('COBRANDSPOAuthLoginController', array('path' => 'travellog/custom/COBRAND/controller/'));
			$controller = $this->file_factory->getClass("COBRANDSPOAuthLoginController");
			return $controller->loginUser($registered_traveler_id,$oauth_api);
		}
	} 
?>