<?php
require_once('travellog/controller/Class.AbstractGroupsController.php');
class COBRANDGroupsController extends AbstractGroupsController{
	
	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
	
		$this->file_factory->registerClass('GroupController' , array('path' => 'travellog/custom/COBRAND/controller/', 'file'=>'COBRANDGroupViewController'));
		
		$this->file_factory->registerClass('GroupListController' , array('path' => 'travellog/custom/COBRAND/controller/', 'file'=>'COBRANDGroupListController'));
		
		$this->file_factory->registerClass('GroupMessagePanel' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'GroupMessagePanelCB'));
		
		$this->file_factory->registerClass('Traveler' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'TravelerCB'));
		
		$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
		
		$this->file_factory->registerClass('PokesView' , array('path' => 'travellog/custom/COBRAND/views/', 'file'=>'PokesViewCB'));
		
		
	}
	
	function performAction(){
		
		// set the config groupID as get variable, if not groupID is defined
		$gID = (isset($_GET['gID'])) ? $_GET['gID'] : 0;
		$gID = (isset($_POST['gID'])) ? $_POST['gID'] : $gID;
		$_POST['gID'] = (0 < $gID) ? $gID : $GLOBALS['CONFIG']->getGroupID() ;
		
		/*added by Jul to restrict non-cobrand members*/
		if( $this->obj_session->getLogin() ){
			$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($GLOBALS['CONFIG']->getGroupID()));
			$group = $_group[0];
			if( !$group->isMember(new Traveler($this->obj_session->get("travelerID"))) ){
				header("location: /membership.php?gID=".$GLOBALS['CONFIG']->getGroupID()."&mode=acceptinvite");
			}
		}
		
		parent::performAction();
			
	}
	
	function _addGroup($_params = array()) {
		$this->loggedUser = $this->file_factory->getClass ('Traveler',array($this->obj_session->get('travelerID')));
		
		if( $this->loggedUser->isAdvisor() || $GLOBALS['CONFIG']->getAdminGroup()->isStaff($this->loggedUser->getTravelerID()) ){
			parent::_addGroup($_params);
		}
		else{
			header("location:index.php");
		}
	
	}
	
		function _showMyGroups($_params){			
			$this->_defineCommonAttributes($_params);
			
			//check if user is currently logged in
			if ($this->obj_session->get('travelerID') > 0) {
				// insert code for logger system
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($this->loggedUser->getTravelerID(),'TRAVELER');				
			}
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 

			//$this->_allowAdminPrivileges($this->isLogged && !$this->loggedUser->isAdvisor()); // if not logged in, redirect
			$this->_allowAdminPrivileges($this->isLogged);
			
			$page = (isset($_GET['page']))        ? $_GET['page']        : 1;
			$page = (isset($_POST['page']))       ? $_POST['page']       : $page;
			$name = (isset($_GET['txtGrpName']))  ? $_GET['txtGrpName']  : "";
			$name = (isset($_POST['txtGrpName'])) ? $_POST['txtGrpName'] : $name;
			
			$obj_controller = $this->file_factory->getClass('GroupListController');
			$obj_controller->setLoggedUser($this->loggedUser);
			$obj_controller->setMyGroups(true);
			$obj_controller->setPage($page);
			$obj_controller->setGroupName($name);
			 
			$this->profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$this->profile_comp->init($this->obj_session->get('travelerID'));
			
			$obj_controller->showMyGroupsPage($this->profile_comp,$this->isAdminLogged);
		}
	
	function _viewGroup($_params){
		$this->_defineCommonAttributes($_params);
		
		$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
		
		// if this group is the group based on config, or a subgroup of that group
		$this->_allowAdminPrivileges($this->group->getGroupID() == $GLOBALS['CONFIG']->getGroupID() ||  ( $this->group->isSubGroup() && $this->group->getParent()->getGroupID() == $GLOBALS['CONFIG']->getGroupID() ) );		
		
		$obj_controller = $this->file_factory->getClass('GroupController');
		
		$obj_controller->setGroupDetails($this->group);
		$obj_controller->setIsLogged($this->isLogged);
		$obj_controller->setIsMemberLogged($this->isMemberLogged);
		$obj_controller->setIsAdminLogged($this->isAdminLogged);
		
		if (NULL!= $this->loggedUser)
			$obj_controller->setLoggedUser($this->loggedUser);
		
		$obj_controller->preparePreferences();
		$obj_controller->prepareGroupInfo();
		$obj_controller->prepareGroupMembers();	
		$obj_controller->prepareGroupCalendar();
		$obj_controller->prepareMessageCenter();
		$obj_controller->prepareLogin();
		$obj_controller->prepareGroupDiscussions(); // added by chris
		$obj_controller->prepareMainGroupInfo(); //added by nash
		$obj_controller->prepareSubNavigation();
		
		$obj_controller->prepareGroupTags();
		
		$obj_controller->prepareGroupSurveys();
		$obj_controller->prepareGroupFiles();
		$obj_controller->prepareSubGroups();
		//$obj_controller->prepareCustomGroupSection();
		$obj_controller->prepareGroupJournals();	
		$obj_controller->prepareGroupAlbums();
		$obj_controller->prepareImportantLinks();
		$obj_controller->prepareGroupArticles();
		$obj_controller->prepareGroupVideos();		// added by neri: 06-10-09
		$obj_controller->prepareGroupSteps();
		$obj_controller->viewGroup();
		
	}
	
	function _showMessagePanel($_params = array() ) {

		$this->_defineCommonAttributes($_params);
		$this->_allowAdminPrivileges(isset($this->group)); // if group is not set, redirect

		$viewType 		= (isset($_POST['viewType']))? $_POST['viewType'] : 'ViewList';
		
		$obj_message_panel = $this->file_factory->getClass('GroupMessagePanel');

		$obj_message_panel->setContext($this->group);
		$obj_message_panel->setIsMemberLogged($this->isMemberLogged);
		$obj_message_panel->setIsAdminLogged($this->isAdminLogged);
		
		$isCoBrandSubGroup = $this->group->isSubGroup() && $this->group->getParent()->getGroupID() == $GLOBALS['CONFIG']->getGroupID() ;
		
		if ($this->isAdminLogged) :
		    if (in_array($viewType, array(1,3,6))) echo '<div class="header_actions">';
			switch ($viewType) {
			
				case 1:		// Bulletin add link
					echo '<span class="actions"><a class="add bulletin_post" href="/bulletinmanagement.php?method=compose&amp;groupID=' .$this->group->getGroupID(). '">Post New Bulletin</a></span>';
					echo ' | ';
					echo '<span class="actions"><a class="add" href="/alertmanagement.php?method=compose&amp;groupID=' .$this->group->getGroupID(). '">Add New Alert</a></span>';
					break;
				case 3:		// Bulletin add link
					$grpStr = ( $isCoBrandSubGroup ) ? 'ID='. $this->group->getGroupID() : '' ;
					echo '<span class="actions"><a class="add bulletin_post" href="/bulletinmanagement.php?method=compose&amp;group' . $grpStr . '">New Post</a></span>';
					break;
				case 6:		// Alerts add link
					$grpStr = ( $isCoBrandSubGroup ) ? '&amp;groupID='. $this->group->getGroupID() : '' ;
					echo '<span class="actions"><a class="add" href="/alertmanagement.php?method=compose' .$grpStr. '">Add New Alert</a></span>';
					break;	
				default:
					echo '';
			}
			if (in_array($viewType, array(1,3,6))) echo '</div>';
		endif;	
		echo $obj_message_panel->render($viewType);

		echo '<div class="section_foot">';
			switch ($viewType) {

			   case 3:       // bulletin view link    	
					if ($obj_message_panel->IsMessageRendered()) {
						$grpStr = ( $isCoBrandSubGroup ) ? 'ID='. $this->group->getGroupID() : '' ;
						echo '<a href="/bulletinmanagement.php?group' .$grpStr. '" class="more">View all</a>';
					}	
					break;
			   case 5:		 // news view link
			   		if ($obj_message_panel->IsMessageRendered()) {
			   			$grpStr = ( $isCoBrandSubGroup ) ? '?groupID='. $this->group->getGroupID() : '' ;
						echo '<a href="/newsmanagement.php' .$grpStr. '" class="more">View all</a>';	
					}
			   		break;
			   default:
			   		echo '';				
			}		
		echo '</div>';				
	}
}
?>