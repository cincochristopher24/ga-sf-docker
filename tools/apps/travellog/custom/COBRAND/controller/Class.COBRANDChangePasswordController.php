<?php
/*
 * Created on 12 23, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

 	require_once('travellog/controller/Class.AbstractChangePasswordController.php');
	
 	class COBRANDChangePasswordController extends AbstractChangePasswordController {
 		
 		function __construct() {
 			parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
			$this->cobrand = true;		// aded by neri: 01-06-09
 		}
 		
 		function performAction() {
 			parent::performAction();
 		}
 	}