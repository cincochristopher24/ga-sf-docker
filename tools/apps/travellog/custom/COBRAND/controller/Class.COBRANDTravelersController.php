<?php
require_once('travellog/controller/Class.AbstractTravelersController.php');
class COBRANDTravelersController extends AbstractTravelersController{
	
	function __construct(){
		
		//$CONFIG            = new Con_fig();
		//$GLOBALS['CONFIG'] = $CONFIG; 
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		$this->file_factory->registerClass('AllTravelers'         , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'AllTravelersModelCB'));
		$this->file_factory->registerClass('NewestTraveler'       , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'NewestTravelerModelCB'));
		$this->file_factory->registerClass('LastLoginTravelers'   , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'LatestTravelerModelCB'));
		$this->file_factory->registerClass('PopularTravelers'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'PopularTravelerModelCB'));
		$this->file_factory->registerClass('InMyHometownTravelers', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'InMyHometownTravelerModelCB'));
		$this->file_factory->registerClass('HometownTravelers'    , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'HomeTravelerModelCB'));
		$this->file_factory->registerClass('CurrentTravelers'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'CurrentTravelerModelCB'));
		$this->file_factory->registerClass('SearchTravelers'      , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'KeywordsSearchTravelerModelCB'));
		$this->file_factory->registerClass('KeywordsSearch'       , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'KeywordsSearchModelCB'));
		$this->file_factory->registerClass('PlanningToGoTo'       , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelersPlanningToGoModelCB'));
		$this->file_factory->registerClass('HaveBeenTo'           , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelersHaveBeenToModelCB'));
		$this->file_factory->registerClass('TravelerLocationCount', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerLocationCountCB'));
		$this->file_factory->registerClass('CountryPhotosCount'   , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'CountryPhotosCountCB'));
		$this->file_factory->registerClass('SubNavigation');
	}
	
	function performAction(){
		$_POST['showSubNav'] = true;
		parent::performAction();
	}
}
?>