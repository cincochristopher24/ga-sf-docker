<?php
	require_once('travellog/controller/Class.AbstractFeedbackController.php');
 	class COBRANDFeedbackController extends AbstractFeedbackController 
 	{
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerClass('Config');
			$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
		}
		
 		public function performAction(){
 			$this->file_factory->registerClass('ToolMan',array('path'=>''));
 			//$this->file_factory->invokeStaticClass('ToolMan','redirect',array('index.php'));
 			
			$_GET['groupID'] = $GLOBALS['CONFIG']->getGroupID();			
 			parent::performAction();
 		}
 	}
?>