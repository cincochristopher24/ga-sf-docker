<?php
require_once('travellog/controller/Class.AbstractGroupPrivacyController.php');
class COBRANDGroupPrivacyController extends AbstractGroupPrivacyController{
	
	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
	
		$this->file_factory->registerClass('GroupController' , array('path' => 'travellog/custom/COBRAND/controller/', 'file'=>'COBRANDGroupViewController'));
		
		$this->file_factory->registerClass('GroupMessagePanel' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'GroupMessagePanelCB'));
		
		$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
		
		$this->file_factory->registerClass('PokesView' , array('path' => 'travellog/custom/COBRAND/views/', 'file'=>'PokesViewCB'));
		
		
	}
	
	function performAction(){
		
		// set the config groupID as get variable, if not groupID is defined
		$gID = (isset($_GET['gID'])) ? $_GET['gID'] : 0;
		$gID = (isset($_POST['gID'])) ? $_POST['gID'] : $gID;
		$_POST['gID'] = (0 < $gID) ? $gID : $GLOBALS['CONFIG']->getGroupID() ;
		
		parent::performAction();
			
	}

}
?>