<?php
require_once('travellog/controller/Class.AbstractEntryController.php');
class COBRANDEntryController extends AbstractEntryController{  
	
	function __construct(){ 
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'SubNavigationCB'));
		$this->file_factory->registerClass('CommentsView' , array('path' => 'travellog/custom/COBRAND/views/', 'file' => 'CommentsViewCB'));
		$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
		$this->file_factory->registerClass('Travel'       , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelCB'));
		$this->file_factory->registerClass('TravelLog'    , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelLogCB'));
	}
	
	function performAction(){
		$this->data         = array_merge($_GET, $_POST);
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'view';
		$this->__applyRules(); 
		parent::performAction();
	}   
	
	protected function __applyRules(){
		global $CONFIG; 
		
		if( strtolower($this->data['action']) == 'view' ){
			if( isset($this->data['travellogID']) && $this->data['travellogID'] ){
				$obj_entry = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
				$this->data['tID'] = $obj_entry->getTrip()->getTravel()->getTravelerID();
			}
			else{
				$obj_journal = $this->file_factory->getClass('Travel', array($this->data['travelID']));
				$this->data['tID'] = $obj_journal->getTravelerID();
			} 
			$this->data['gID'] = $CONFIG->getGroupID();  
			$obj_group         = $this->file_factory->getClass('AdminGroup');
			if( !$obj_group->isMemberOfGroupNetwork($this->data) ){
				$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['tID']));
				if( $obj_traveler->isAdvisor() && $this->obj_session->getLogin() )
					ToolMan::redirect('/journal.php?action=GroupJournals');
				elseif( !$obj_traveler->isAdvisor() && $this->obj_session->getLogin() )
					ToolMan::redirect('/journal.php?action=MyJournals');
				else
					ToolMan::redirect('/journal.php');
				exit; 
			}    
		} 
		else
			parent::__applyRules();		
	}
}
?>
