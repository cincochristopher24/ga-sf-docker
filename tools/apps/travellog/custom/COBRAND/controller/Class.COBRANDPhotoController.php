<?php
/*
 * Created on 11 12, 07
 * 
 * Author: J. LLano
 * Purpose: the default controller for "Photo" module for ganet cobrand sites
 * 
 */
 
 require_once('travellog/controller/Class.AbstractPhotoController.php');
  
 class COBRANDPhotoController extends AbstractPhotoController {
 	
 	function __construct(){
 		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;

		// START -- Added By: Cheryl Ivy Q. Go   27 November 2007
		$this->file_factory->registerClass('CommentsView', array('path'=>'travellog/custom/COBRAND/views/','file'=>'CommentsViewCB'));
		$this->file_factory->registerClassInitCall('CommentsView', array('func'=>'setGroupId','params' => array('groupID'=>$GLOBALS['CONFIG']->getGroupID())));
		// END -- Added By: Cheryl Ivy Q. Go   27 November 2007
		
		$this->file_factory->registerClass('SubNavigation', array('path'=>'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
		$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 	
 	
 }
 
?>
