<?php
	require_once('travellog/controller/Class.AbstractBulletinManagementController.php');
	
 	class COBRANDBulletinManagementController extends AbstractBulletinManagementController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
			$this->pageTitle = $GLOBALS['CONFIG']->getSiteName().' Bulletins';
 		}
 		
 		
 		public function performAction(){
 			$factory = $this->file_factory->invokeStaticClass('GroupFactory','instance',null,false);
			try{
				$parentGroup = $factory->create( array($GLOBALS['CONFIG']->getGroupID()) );
			}
			catch(exception $e){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/passport.php'));
			}
			$subGroups = $parentGroup[0]->getSubGroups();
			$travelerID = $this->obj_session->get('travelerID');
			
			if(array_key_exists('group',$_GET)){
				$_GET['groupID'] = $GLOBALS['CONFIG']->getGroupID();
			}
			else{
				if(array_key_exists('groupID',$_GET)){
					if($_GET['groupID'] != $GLOBALS['CONFIG']->getGroupID()){
						$isSubGroup = false;
						foreach($subGroups as $iSubGroup){
							if($iSubGroup->getGroupID() == $_GET['groupID']){
								$isSubGroup = true;
								break;
							}
						}
						if(!$isSubGroup){
							$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/passport.php'));
						}
	 				}	
				}
				else{
					//make sure that the logged traveler is a member of the parent group or one of its subgroups
				}
			}
			/****
 			if(!array_key_exists('groupID',$_GET)){
				//$_GET['groupID'] = $GLOBALS['CONFIG']->getGroupID();
				//check if the current logged traveler is a member of the parent group or one of its sub groups
				if(!$parentGroup->isMember){
					
				}
 			}
 			else{
 				//check if the groupID is an id of the subgroup of the current group
 				if($_GET['groupID'] != $GLOBALS['CONFIG']->getGroupID()){
					$isSubGroup = false;
					foreach($subGroups as $iSubGroup){
						if($iSubGroup->getGroupID() == $_GET['groupID']){
							$isSubGroup = true;
							break;
						}
					}
					if(!$isSubGroup){
						$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/passport.php'));
					}
 				}
 			}
 			***/
 			parent::performAction();
 		}
 		
 		
 	}
?>