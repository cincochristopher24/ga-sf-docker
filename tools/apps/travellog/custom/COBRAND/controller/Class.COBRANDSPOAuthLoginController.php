<?php

require_once("travellog/controller/Class.AbstractSPOAuthLoginController.php");

class COBRANDSPOAuthLoginController extends AbstractSPOAuthLoginController{
	
	protected $config;
	protected $adminGroup;
	
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$this->file_factory->registerClass('TravelerCB', array("path" => "travellog/custom/COBRAND/model"));
		$this->file_factory->registerClass("AdminGroup", array("path" => "travellog/model/"));
		
 		$this->config = $this->file_factory->getClass('Config');
		$this->adminGroup = $this->file_factory->getClass("AdminGroup", array($this->config->getGroupID()));
	}
	
	public function loginUser($travelerID=0,$oauth_api=NULL){
		$traveler = $this->file_factory->getClass("TravelerCB",array($travelerID));
		
		if( $this->adminGroup->isMember($traveler) ){
			if( $this->startSession($travelerID) ){
				$oauth_api->login();
				$oauth_api->clearCredentials();
				header("location: /passport.php");	
				exit;
			}
		}elseif( $this->adminGroup->isInInviteList($traveler) ){
			if( $this->startSession($travelerID) ){
				$oauth_api->login();
				$oauth_api->clearCredentials();
				header("location: /membership.php?gID=".$this->adminGroup->getID()."&mode=acceptinvite");
				exit;
			}
		}
		return FALSE;
	}
}