<?php
	require_once('travellog/controller/Class.AbstractSurveyCenterController.php');
 	class COBRANDSurveyCenterController extends AbstractSurveyCenterController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
			$this->pageTitle = $GLOBALS['CONFIG']->getSiteName().' Survey Center';
 		}
 	}
?>