<?php 

	require_once('travellog/controller/Class.AbstractTopicController.php');

	class COBRANDTopicController extends AbstractTopicController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('Config');
			$GLOBALS['CONFIG'] = $file_factory->getClass('Config');
		}
	}
?>