<?php
require_once('travellog/controller/Class.AbstractJournalController.php');
class COBRANDJournalController extends AbstractJournalController{
	
	function __construct(){
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'SubNavigationCB'));
		$this->file_factory->registerClass('TravelLog'    , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelLogCB'));
		$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
		 
	}
	 
	function performAction(){  
		
		/*added by jul for pending cobrand members*/
		if( isset($GLOBALS['CONFIG']) && $this->obj_session->getLogin() ){
			$this->file_factory->registerClass('GroupFactory');
			$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($GLOBALS['CONFIG']->getGroupID()));
			$group = $_group[0];
			if( !$group->isMember(new Traveler($this->obj_session->get("travelerID"))) ){
				header("location: /membership.php?gID=".$GLOBALS['CONFIG']->getGroupID()."&mode=acceptinvite");
			}
		}
		
		$this->data           = array_merge($_GET, $_POST);
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'All';
		$this->__applyRules();
		switch( strtolower($this->data['action']) ){
			case "all":
				$this->_viewAllJournals();
			break;
			default:
				parent::performAction();
		}
	} 
	
	protected function __applyRules(){
		/*if( strtolower($this->data['action']) == 'groupmembersjournal' ){
			ToolMan::redirect('/journal.php');
		}*/
		parent::__applyRules();
	}
	
	protected function _viewAllJournals(){
		require_once('travellog/views/Class.JournalsComp.php');
		require_once('travellog/views/Class.EntriesArticlesComp.php');
		global $CONFIG;
		
		$grpID = $CONFIG->getGroupID();
		$props                    = array();
		$props['page']            = ( isset($this->data['page'])       )? $this->data['page']        : 1;
		$props['query_string']    = ( isset($this->data['travelerID']) )? '&travelerID='.$this->data['travelerID'] : '';
		$props['locationID']      = ( isset($this->data['locationID']) )? $this->data['locationID']  : 0;
		$props['tag']      = ( isset($this->data['tag']) )? $this->data['tag']  : NULL;
		$this->data['travelerID'] = ( $this->obj_session->getLogin()   )? $this->obj_session->get('travelerID') : 0;
		$props['is_login']        = $this->obj_session->getLogin();
		$props['travelerID']      = $this->data['travelerID'];
		$props['action']          = $this->data['action'];
		$props['show_author']     = true;
		$obj_paging               = NULL;
		$obj_iterator             = NULL; 
		$country_name             = NULL; 		
		$arr_contents             = array();
		$obj_condition            = $this->file_factory->getClass('Condition');
		$obj_filter               = $this->file_factory->getClass('FilterCriteria2');
		$jCompContextAr 				=	$this->file_factory->invokeStaticClass('JournalsComp','convertURLParamToSetupArray');
		
		// setup for viewing all journals
		if (count($jCompContextAr) == 0){
			$jCompContextAr['VIEWMODE'] 			=	'GROUP';				
			$jCompContextAr['SECURITY_CONTEXT'] 	=	'PUBLIC';
			$jCompContextAr['VIEW_TAB'] 			=	'STAFF_ADMIN_APPROVED';
		}
		//if(isset($this->data['COMBINE']))
			$jCompContextAr['JOURNALS_ARTICLES'] = true;
		// $journalsComp				=	$this->file_factory->getClass('JournalsComp',array($jCompContextAr,$grpID));
		$journalsComp				=	$this->file_factory->getClass('EntriesArticlesComp',array($jCompContextAr,$grpID,$this->obj_session->get('travelerID')));
		 					
		// if( $props['locationID'] ){			
		// 	$obj_factory            = $this->file_factory->invokeStaticClass('LocationFactory');
		// 	$obj_location           = $obj_factory->create($props['locationID']);
		// 	$country_name           = $obj_location->getName();
		// 	$journalsComp->setJournalCriteria2(array('LOCATIONID' => $props['locationID']));
		// 	
		// }
		// 
		// if( $props['tag'] ){
		// 	$journalsComp->setJournalCriteria2(array('TAG' => $props['tag']));
		// }
		
		if( $props['locationID'] ){			
			$obj_factory            = $this->file_factory->invokeStaticClass('LocationFactory');
			$obj_location           = $obj_factory->create($props['locationID']);
			$country_name           = $obj_location->getName();
			$journalsComp->setSearchCriteria2(array('LOCATIONID' => $props['locationID']));
		}
		
		if( $props['tag'] ){
			$journalsComp->setSearchCriteria2(array('TAG' => $props['tag']));
		}
		
		$col_countries        =  $this->file_factory->invokeStaticClass( 'TravelLog', 'getCountriesWithLogs', array($grpID) );
		$obj_navigation       =  $this->file_factory->getClass('SubNavigation');
		
		$obj_navigation->setContext        ( 'GROUP'                );
		$obj_navigation->setContextID      ( $grpID );
		$obj_navigation->setLinkToHighlight( 'MY_JOURNALS'             );
		$obj_navigation->setDoNotShow(true);
		
		
		$obj_navigation->setDoNotShow(true);
		
		$obj_template = $this->file_factory->getClass('Template');
		$arr_contents['col_locations']        = $col_countries;
		$obj_map_view                         = $this->file_factory->getClass('GAMapView');
		$obj_map_view->setContents($arr_contents);
			
		$obj_template->set_vars
		( array(
			'journalsComp'			   => $journalsComp,		       		
			'obj_map_view'             => $obj_map_view,
			'props'                    => $props,
			'objSubNavigation'         => $obj_navigation,
			'collectionCountry'        => $col_countries,
			'objHelpText'              => $this->file_factory->getClass('HelpText')
		));
		
		//if(!isset($this->data['COMBINE']))
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
		var search = function(){			
			window.location.href = '/journal.php?action=All&locationID=' + jQuery("#countryID").attr("value");
		}		
//]]>   
</script>
BOF;
		/*else
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
		var search = function(){			
			window.location.href = '/journal.php?COMBINE&action=All&locationID=' + jQuery("#countryID").attr("value");
		}		
//]]>   
</script>
BOF;*/
	
		// Template::includeDependentCss( "/js/yahooCarousel/carousel.css"  );
		// 
		// Template::includeDependentJs("/js/jquery-1.1.4.pack.js"); 
		// Template::includeDependentJs("/js/yahoo/yahoo-dom-event.js");
		// Template::includeDependentJs("/js/yahoo/utilities.js");
		// Template::includeDependentJs("/js/yahoo/dragdrop-min.js");
		// Template::includeDependentJs("/js/yahoo/container_core-min.js");
		// Template::includeDependentJs("/js/yahooCarousel/carousel.js");
				
		Template::includeDependent  ( $code );
		Template::setMainTemplateVar('title', $country_name .' Travel Journals, Travel Blogs and Travel Experiences of GoAbroad Network Travelers');
		Template::setMainTemplateVar('metaDescription', 'View the ' .$country_name. ' travel blogs and journals of GoAbroad Network travelers and be inspired to do the same.');  
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		Template::setMainTemplateVar('layoutID', 'main_pages');
		Template::setMainTemplateVar('page_location', 'Journals');  
		
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentCss('/css/Thickbox.css');
		Template::includeDependentJs('/js/thickbox.js');
		
		$obj_template->out($this->file_factory->getTemplate('TravelView'));
		
		
		
		
	}
	
}
?>
