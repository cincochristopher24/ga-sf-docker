<?php
	require_once('travellog/controller/Class.AbstractAlertManagementController.php');
 	class COBRANDAlertManagementController extends AbstractAlertManagementController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
	
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
			
 		}
 		
 		public function performAction(){
 			//$_GET['groupID'] = $GLOBALS['CONFIG']->getGroupID();
			if(array_key_exists('groupID',$_GET)){
				if($_GET['groupID'] != $GLOBALS['CONFIG']->getGroupID()){
					//check if the groupID belongs to a subgroup of the current cobrand
					try{
						$currentGroup = $this->file_factory->getClass('AdminGroup',array($_GET['groupID']));
					}
					catch(exception $e){
						$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
					}
					if($currentGroup->isSubGroup()){
						if($currentGroup->getParentID() != $GLOBALS['CONFIG']->getGroupID()){
							$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
						}
					}
					else{
						$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
					}
				}
			}
			else{
				//$_GET['groupID'] = $GLOBALS['CONFIG']->getGroupID();
			}
 			parent::performAction();
 		}
 		
 	}
?>