<?php
/*
	Filename:		Class.COBRANDTravelPlansController.php
	Author:			Jonas Tandinco
	Date Created:	Nov/12/2007
	Putpose:		controller implementation for commond co-brand functionality

	EDIT HISTORY:
	
	Nov/27/2007 by Jonas Tandinco
		1.	modified constructor to override subnavigation
*/

require_once('travellog/controller/Class.AbstractTravelPlansController.php');

class COBRANDTravelPlansController extends AbstractTravelPlansController{
	public function __construct() {
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
		$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
	}
	
	public function performAction(){
		parent::performAction();
	}
}
?>