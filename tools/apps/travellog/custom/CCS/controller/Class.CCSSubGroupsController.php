<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class CCSSubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new CCS subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/CCS/views"));
			$this->file_factory->setPath("CSS", "/custom/CCS/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/CCS/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}