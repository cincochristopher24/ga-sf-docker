<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class KAYASubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new KAYA subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/KAYA/views"));
			$this->file_factory->setPath("CSS", "/custom/KAYA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/KAYA/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}