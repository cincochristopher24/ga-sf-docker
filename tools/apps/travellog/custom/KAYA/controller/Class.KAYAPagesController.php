<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
	
	class KAYAPagesController extends COBRANDPagesController {
	
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/KAYA/views'));
			$this->file_factory->registerTemplate('PrivacyPolicy',array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->registerTemplate('TermsDefault', array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->registerTemplate('TermsAdvisor', array('path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/KAYA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/KAYA/views/');
		}
	
		function performAction(){
			parent::performAction();
		}
	
	}