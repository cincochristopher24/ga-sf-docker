<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDMembershipController.php');
	
	class KAYAMembershipController extends COBRANDMembershipController{
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/KAYA/views'));
			$this->file_factory->setPath('CSS', '/custom/KAYA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/KAYA/views/'); 
		}
		 
		function performAction(){ 
		
			parent::performAction(); 
			
		}
	}