<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyCenterController.php');
	
 	class KAYASurveyCenterController extends COBRANDSurveyCenterController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/KAYA/views'));
			$this->file_factory->setPath('CSS', '/custom/KAYA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/KAYA/views/');
 		}
 	}