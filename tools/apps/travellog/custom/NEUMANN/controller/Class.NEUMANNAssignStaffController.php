<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class NEUMANNAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/NEUMANN/views'));
			$this->mFactory->setPath('CSS', '/custom/NEUMANN/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/NEUMANN/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>