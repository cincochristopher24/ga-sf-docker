<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDTagManagementController.php');
class NEUMANNTagManagementController extends COBRANDTagManagementController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/NEUMANN/views'));
		$this->file_factory->setPath('CSS', '/custom/NEUMANN/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/NEUMANN/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>