<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDGroupPrivacyController.php');
class NEUMANNGroupPrivacyController extends COBRANDGroupPrivacyController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/NEUMANN/views'));
		$this->file_factory->setPath('CSS', '/custom/NEUMANN/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/NEUMANN/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>