<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	AIFSRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for AIFS	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/AIFS/views'));
		$file_factory->setPath('CSS', '/custom/AIFS/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}