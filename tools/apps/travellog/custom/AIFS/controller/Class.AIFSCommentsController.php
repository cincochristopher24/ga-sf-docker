<?php
/*
 * Created on 11 15, 07
 * 
 * Author:
 * Purpose: 
 * 
 */
 
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDCommentsController.php');
 

	class AIFSCommentsController extends COBRANDCommentsController {
		function __construct(){		
			parent::__construct();	
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AIFS/views'));
			$this->file_factory->setPath('CSS', '/custom/AIFS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/');
 		}
 	
		function performAction() {
			parent::performAction();
 		}
 }
 
?>
