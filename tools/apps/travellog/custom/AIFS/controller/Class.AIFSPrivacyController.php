<?php
/*
 * Created on 16-Nov-07
 *
 *  Author: K. Gordo
 */
 
 require_once('travellog/custom/COBRAND/controller/Class.COBRANDPrivacyController.php');
 
 class AIFSPrivacyController extends COBRANDPrivacyController {
 	function __construct(){
 		parent::__construct();
 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AIFS/views'));
		$this->file_factory->setPath('CSS', '/custom/AIFS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/'); 
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 }
  
?>