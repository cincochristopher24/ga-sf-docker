<?php
/*
 * Created on 11 13, 07
 * 
 * Author: L. Llano
 * Purpose: Photo module controller for AIFS 
 * 
 */
 
 require_once('travellog/custom/COBRAND/controller/Class.COBRANDPhotoController.php');
 
 class AIFSPhotoController extends COBRANDPhotoController {
 	function __construct(){
 		parent::__construct();
 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AIFS/views'));
		$this->file_factory->setPath('CSS', '/custom/AIFS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/'); 
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 }
 
?>
