<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	
	<ul>
		<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>">AIFS</a></li>
		<?= NavigationHelper::displayGlobalNavigation() ?>
	</ul>

	<ul>		
		<?= NavigationHelper::displayMainNavigation($page_location); ?>
		<li class="last">
			<?= HelperGlobal::displayLoginLink(); ?>
		</li>
	</ul>
	<p id="footlet">
 		Copyright &copy; <? echo date("Y"); ?> AIFS and GoAbroad.net&reg;
	</p>
</div>
	
