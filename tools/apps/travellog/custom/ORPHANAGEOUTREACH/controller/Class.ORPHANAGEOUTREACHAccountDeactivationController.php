<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDAccountDeactivationController.php");
	
	/**
	 * The account deactivation controller of a cobrand.
	 */
	class ORPHANAGEOUTREACHAccountDeactivationController extends COBRANDAccountDeactivationController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ORPHANAGEOUTREACH/views"));
			$this->file_factory->setPath("CSS", "/custom/ORPHANAGEOUTREACH/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ORPHANAGEOUTREACH/views/");
		}
		
	}
	
