<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class ORPHANAGEOUTREACHSubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new ORPHANAGEOUTREACH subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ORPHANAGEOUTREACH/views"));
			$this->file_factory->setPath("CSS", "/custom/ORPHANAGEOUTREACH/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ORPHANAGEOUTREACH/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}