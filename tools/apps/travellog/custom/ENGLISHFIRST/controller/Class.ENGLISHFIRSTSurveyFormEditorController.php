<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyFormEditorController.php');
class ENGLISHFIRSTSurveyFormEditorController extends COBRANDSurveyFormEditorController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ENGLISHFIRST/views'));
		$this->file_factory->setPath('CSS', '/custom/ENGLISHFIRST/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ENGLISHFIRST/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>