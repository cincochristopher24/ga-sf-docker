<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	<div id="footer_inner_wrap">
	
		<div id="footer_links">
			<div>
				<ul>
					<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>">English First</a></li>
					<?= NavigationHelper::displayGlobalNavigation() ?>
				</ul>

				<ul id="footer_main_links">
            		<?= NavigationHelper::displayMainNavigation($page_location); ?>
					<li class="last">
						<?= HelperGlobal::displayLoginLink(); ?>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
		<p id="footlet">Copyright &copy; <? echo date("Y"); ?> My English First and GoAbroad.net&reg;</p>
	</div>
</div>
	
