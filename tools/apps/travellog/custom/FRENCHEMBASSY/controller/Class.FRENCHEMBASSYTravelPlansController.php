<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelPlansController.php');
class FRENCHEMBASSYTravelPlansController extends COBRANDTravelPlansController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/FRENCHEMBASSY/views'));
		$this->file_factory->setPath('CSS', '/custom/FRENCHEMBASSY/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/FRENCHEMBASSY/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>