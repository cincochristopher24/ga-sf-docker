<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPostController.php');
	
 	class FRENCHEMBASSYPostController extends COBRANDPostController {
 		function __construct(){
	 		parent::__construct();
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/FRENCHEMBASSY/views'));
			$file_factory->setPath('CSS', '/custom/FRENCHEMBASSY/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/FRENCHEMBASSY/views/');
 		}
 		
 	}