<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class WISubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new WI subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/WI/views"));
			$this->file_factory->setPath("CSS", "/custom/WI/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/WI/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}