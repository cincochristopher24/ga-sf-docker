<?php
	/*
	 * Class.RegisterHelper.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	
	require_once 'Class.Crypt.php';
	require_once("Class.Constants.php");
	 
	class RegisterHelper{
		
		static $instance;
		
		static function getInstance(){
			if( self::$instance == NULL ) self::$instance = new RegisterHelper;
			return self::$instance;
		}
		
		function getRedirectPage( $redirect = "" ){
			switch($redirect){
				case constants::WELCOME_PAGE:
					return "welcome.php";
					break;
				case constants::LOGGED_IN_PAGE:
					return "passport.php";
					break;
				case constants::HOME_PAGE:
					return "index.php";
					break;	
				default:
					return "register.php";			
				
			}
		}
		
		function getAction(){
			if( (isset($_GET["gcID"]) && isset($_GET["ref"])) || isset($_GET["traveler"]) || isset($_GET["advisor"]) || $this->getInvitedEmail() )
				$action = constants::FILL_UP;
			elseif( isset($_POST["btnAdvisorRegister"]) || isset($_POST["btnRegister"]) )
				$action = constants::SUBMIT_DETAILS;
			elseif( isset($_POST["btnContinueRegister"]) )
				$action = constants::CONFIRM_DETAILS;
			elseif( isset($_POST["btnBackRegister"]) )
				$action = constants::BACK;	
			else
				$action = constants::CHOOSE_REGISTER_TYPE;
			return $action;				
		}
		
		function getUser(){
			
			if( (isset($_GET['gcID']) && isset($_GET['ref'])) || ( isset($_POST["hdnMode"]) && constants::GACOM_CLIENT == $_POST["hdnMode"] ) || (isset($_POST["btnAdvisorRegister"]) && 0 < (int)$_POST["hdnGcID"]) )
				$user = constants::GACOM_CLIENT;
			//elseif( isset($_GET["advisor"]) || ( isset($_POST["hdnMode"]) && constants::GROUP == $_POST["hdnMode"] ) || isset($_POST["btnAdvisorRegister"]) )
			elseif( ( isset($_POST["hdnMode"]) && constants::GROUP == $_POST["hdnMode"] ) || isset($_POST["btnAdvisorRegister"]) )
				$user = constants::GROUP;
			elseif( isset($GLOBALS["CONFIG"]) || ( isset($_POST["hdnMode"]) && constants::TRAVELERCB == $_POST["hdnMode"] ) )
				$user = constants::TRAVELERCB;	
			else
				$user = constants::TRAVELER;
			return $user;		
		}
		
		function readTravelerData(){ 
			
			return $travelerData = array(
				"firstname"		=>	isset($_POST["txtFirstName"]) ? trim($_POST["txtFirstName"]) : "",
				"lastname"		=>	isset($_POST["txtLastName"]) ? trim($_POST["txtLastName"]) : "",
				"username"		=>	isset($_POST["txtUsrName"]) ? trim($_POST["txtUsrName"]) : "",
				"password"		=>	isset($_POST["pwdPasWrd"]) ? trim($_POST["pwdPasWrd"]) : "",
				"pwdConfirm"	=>	isset($_POST["pwdConfirmPasWrd"]) ? trim($_POST["pwdConfirmPasWrd"]) : "",
				"emailAdd"		=>	isset($_POST["txtEmail"]) ? trim($_POST["txtEmail"]) : "",
				"code"			=>	isset($_POST["encSecCode"]) ? $_POST["encSecCode"] : "",
				"securityCode"	=>	isset($_POST["securityCode"]) ? $_POST["securityCode"] : "",
				"regKey"		=>	isset($_POST["hdnRegKey"]) ? trim($_POST["hdnRegKey"]) : "",
				"regID"			=>	isset($_POST["hdnRegID"]) ? trim($_POST["hdnRegID"]) : "",
				"subscribeNL"	=>	isset($_POST["chkSubscribe"]) ? 1 : 0,
				"agreeTerm"		=>	isset($_POST["chkAgree"]) ? $_POST["chkAgree"] : false,
				'university'	=>	isset($_POST["txtStudentUniversity"]) ? trim($_POST["txtStudentUniversity"]) : '',
				'chkStudent'	=>	isset($_POST['chkStudent']) ? true : false,
				'service_provider'	=>	isset($_POST["hdnAuthorizedSP"]) && in_array($_POST["hdnAuthorizedSP"],array("twitter","google","facebook")) ? $_POST["hdnAuthorizedSP"] : NULL,
				'fromEmailInvite'	=>	isset($_POST["hdnFromEmailInvite"]) ? $_POST["hdnFromEmailInvite"] : FALSE,
			);
			
			
		}
		
		function readAdvisorData(){ 
			
			return $advisorData = array_merge($this->readClientKeys(),array(
				"instname"		=>	isset($_POST["txtAdvisorName"]) ? trim($_POST["txtAdvisorName"]) : "",
				"firstname"		=>	isset($_POST["txtFirstName"]) ? trim($_POST["txtFirstName"]) : "",
				"lastname"		=>	isset($_POST["txtLastName"]) ? trim($_POST["txtLastName"]) : "",
				"username"		=>	isset($_POST["txtAdvisorUsrName"]) ? trim($_POST["txtAdvisorUsrName"]) : "",
				"password"		=>	isset($_POST["pwdAdvisorPasWrd"]) ? trim($_POST["pwdAdvisorPasWrd"]) : "",
				"pwdConfirm"	=>	isset($_POST["pwdConfirmAdvisorPasWrd"]) ? trim($_POST["pwdConfirmAdvisorPasWrd"]) : "",
				"emailAdd"		=>	isset($_POST["txtAdvisorEmail"]) ? trim($_POST["txtAdvisorEmail"]) : "",
				"code"			=>	isset($_POST["encSecCode"]) ? $_POST["encSecCode"] : "",
				"securityCode"	=>	isset($_POST["securityCode"]) ? $_POST["securityCode"] : "",
				"regKey"		=>	isset($_POST["hdnRegKey"]) ? trim($_POST["hdnRegKey"]) : "",
				"regID"			=>	isset($_POST["hdnRegID"]) ? trim($_POST["hdnRegID"]) : "",
				"subscribeNL"	=>	isset($_POST["chkAdvisorSubscribe"]) ? 1 : 0,
				"agreeTerm"		=>	isset($_POST["chkAdvisorAgree"]) ? 1 : false,
			));
		}
		
		function readClientKeys(){
			$content = array(
				"gcID"	=>	"",
				"ref"	=>	""
			);
			if( isset($_POST["btnAdvisorRegister"]) ){
				$content = array(
					"gcID"	=>	$_POST['hdnGcID'],
					"ref"	=>	$_POST['hdnRef']
				);
			}
			elseif( isset($_GET['gcID']) && isset($_GET['ref']) ){
				$content = array(
					"gcID"	=>	$_GET['gcID'],
					"ref"	=>	$_GET['ref']
				);
			}
			return $content;
		}
		
		public function getInvitedEmail(){
			$myCrypt = new Crypt();
			$qString = explode('&',$myCrypt->decrypt(urldecode($_SERVER['QUERY_STRING'])));
			if( 2 == count($qString) && in_array('traveler',$qString) && preg_match("/email=/",$qString[1]) )
				return preg_replace("/email=/","",$qString[1]);
			else
				return NULL;	
		}
		
	}
?>
