<?php
	/**
 	 * Class.AutoApproveFeatureJournalHelper.php
	 * handles auto approve/feature of journals according to 
	 * group journal preference
	**/
	
	class AutoApproveFeatureJournalHelper {
		
		private $mTraveler = null; // author of journal
		private $mTravel = null; // the journal to be approved/featured
		
		public function __construct($travel, $traveler){
			$this->mTravel = $travel;
			$this->mTraveler = $traveler;
		}
		
		public function setTravel($travel){
			$this->mTravel = $travel;
		}
		
		public function setTraveler($traveler){
			$this->mTraveler = $traveler;
		}
		
		/**
		* get groups where travelerID is a member
		* @return AdminGroup[]
		**/
		public function getGroupsOfTraveler(){
			require_once('travellog/model/Class.GroupFactory.php');
			require_once('Class.ConnectionProvider.php');
			$rs = ConnectionProvider::instance()->getRecordset();
			
			$sql = "SELECT DISTINCT tblGroup.groupID AS groupID " .
			 			"FROM tblGrouptoTraveler, tblGroup " .
						"WHERE tblGroup.groupID = tblGrouptoTraveler.groupID " .
						"AND tblGroup.isSuspended = 0 " .
						"AND tblGrouptoTraveler.travelerID = " . $this->mTraveler->getTravelerID();
						
			$rs	=	ConnectionProvider::instance()->getRecordset();
			$rs->execute($sql);

			$arrGroup	= array();
			$arrGroupId	= array();

			if (0 < $rs->Recordcount()){
				while($mGroup = mysql_fetch_array($rs->Resultset()))
					$arrGroupId[] = $mGroup['groupID'];
				$arrGroup = GroupFactory::instance()->create($arrGroupId);
			}
			return $arrGroup;
		} 
		
		public function autoApproveAndFeature(){
			$travelID = $this->mTravel->getTravelID();
			$groups = $this->getGroupsOfTraveler();
			
			require_once('travellog/dao/Class.GroupApprovedJournals.php');
			$relatedGroups = GroupApprovedJournals::getGroupsWhereTravelIsApproved($travelID);
			$relatedGroups = array_keys($relatedGroups);
			
			require_once('travellog/model/Class.Featured.php');
			$featuredGroups = Featured::getGroupsWhereTravelIsFeatured($travelID);

			foreach($groups as $group){
				// auto approve/feature journal not applicable to clubs
				if($group->getDiscriminator() == 1){ 
					continue; 
				}
				
				$groupID = $group->getGroupID();
				$automode = (!is_null($group->getParent())) ? $group->getParent()->getPrivacyPreference()->isAutoModeGroupJournals() : $group->getPrivacyPreference()->isAutoModeGroupJournals();
				if($automode){ // if auto approve preference is set to true
					// if not approved, approve
					if(!in_array($groupID, $relatedGroups)){
						Featured::setFeaturedTravelInGroup($groupID,$travelID);
						$featuredGroups[$groupID] = $groupID;
						$this->mTravel->invalidateJournalsPageCaching();
					}
					// if not featured, feature
					if(!in_array($groupID, $featuredGroups)){
						Featured::setFeaturedTravelInGroup($groupID,$travelID);
						$this->mTravel->invalidateJournalsPageCaching();
					}
				}	
			}
		}
		
		public static function autoApproveAndFeatureForGroups($travel,$groups=array()){
			foreach($groups as $groupID){
				// if not approved, approve
				if(!Featured::isFeaturedTravelInGroup($groupID, $travel->getTravelID())){
					Featured::setFeaturedTravelInGroup($groupID,$travel->getTravelID());
					$travel->invalidateJournalsPageCaching();
				}
			}
		}
		
		/**
		 * retrieves groups with auto approve and feature preference
 		 * @param $travelerID, default = 0: get all groups
		 * @param $travelID, default
		 * @return Group[]
		 **/
		public static function getAutoModeGroups($travelerID = 0, $travelID = 0){
			require_once('travellog/model/Class.GroupFactory.php');
			require_once('Class.ConnectionProvider.php');
			$rs = ConnectionProvider::instance()->getRecordset();
			
			$sql = "SELECT DISTINCT tblGroup.groupID AS groupID FROM tblGroup 
					JOIN tblGroupPrivacyPreference ON 
						(tblGroup.groupID = tblGroupPrivacyPreference.groupID
						AND tblGroupPrivacyPreference.preferencetype = 9
						AND tblGroupPrivacyPreference.preference = 1) ";

			if($travelerID > 0){
				$sql .= "JOIN tblGrouptoTraveler ON 
							(tblGroup.groupID = tblGrouptoTraveler.groupID
							AND tblGrouptoTraveler.travelerID = " . $travelerID.") ";
			}
			
			if($travelID > 0){
				$sql .= "LEFT JOIN tblGroupApprovedJournals ON 
							(tblGroup.groupID = tblGroupApprovedJournals.groupID
							AND tblGroupApprovedJournals.travelID = " . $travelID.") 
						 LEFT JOIN tblGroupFeaturedJournals ON 
							(tblGroup.groupID = tblGroupFeaturedJournals.groupID
							AND tblGroupFeaturedJournals.travelID = " . $travelID.") 
						 LEFT JOIN tblJournalGroupPermissions ON
							(tblGroup.groupID = tblJournalGroupPermissions.groupID
							AND tblJournalGroupPermissions.travelID = " . $travelID.") 
						 ";
			}
			
			
			$sql .= "WHERE tblGroup.isSuspended = 0 AND parentID = 0 ";
			
			if($travelID > 0){
				$sql .= "AND tblGroupApprovedJournals.groupID IS NULL 
						 AND tblGroupFeaturedJournals.groupID IS NULL
						 AND tblJournalGroupPermissions.ID IS NULL";
			}
			$rs	=	ConnectionProvider::instance()->getRecordset();
			$rs->execute($sql);

			$arrGroup	= array();
			$arrGroupId	= array();

			if (0 < $rs->Recordcount()){
				while($mGroup = mysql_fetch_array($rs->Resultset()))
					$arrGroupId[] = $mGroup['groupID'];
				$arrGroup = GroupFactory::instance()->create($arrGroupId);
			}
			return $arrGroup;
		}
		
		/**
		 * retrieves groups with auto approve and feature preference
 		 * @param $travelerID, default = 0: get all groups
		 * @param $travelID, default
		 * @return Group[]
		 **/
		public static function countAutoModeGroups($travelerID = 0, $travelID = 0){
			require_once('Class.ConnectionProvider.php');
			$rs = ConnectionProvider::instance()->getRecordset();
			
			$sql = "SELECT DISTINCT COUNT(tblGroup.groupID) AS count FROM tblGroup 
					JOIN tblGroupPrivacyPreference ON 
						(tblGroup.groupID = tblGroupPrivacyPreference.groupID
						AND tblGroupPrivacyPreference.preferencetype = 9
						AND tblGroupPrivacyPreference.preference = 1) ";

			if($travelerID > 0){
				$sql .= "JOIN tblGrouptoTraveler ON 
							(tblGroup.groupID = tblGrouptoTraveler.groupID
							AND tblGrouptoTraveler.travelerID = " . $travelerID.") ";
			}
			
			if($travelID > 0){
				$sql .= "LEFT JOIN tblGroupApprovedJournals ON 
							(tblGroup.groupID = tblGroupApprovedJournals.groupID
							AND tblGroupApprovedJournals.travelID = " . $travelID.") 
						 LEFT JOIN	tblGroupFeaturedJournals ON 
							(tblGroup.groupID = tblGroupFeaturedJournals.groupID
							AND tblGroupFeaturedJournals.travelID = " . $travelID.")
						 LEFT JOIN tblJournalGroupPermissions ON
							(tblGroup.groupID = tblJournalGroupPermissions.groupID
							AND tblJournalGroupPermissions.travelID = " . $travelID.") 
						 ";
			}
			
			
			$sql .= "WHERE tblGroup.isSuspended = 0 AND parentID = 0 ";
			
			if($travelID > 0){
				$sql .= "AND tblGroupApprovedJournals.groupID IS NULL 
						 AND tblGroupFeaturedJournals.groupID IS NULL
						 AND tblJournalGroupPermissions.ID IS NULL";
			}
			$rs	=	ConnectionProvider::instance()->getRecordset();
			$rs->execute($sql);

			$row = mysql_fetch_array($rs->Resultset());
			return $row['count'];
		}
		
		/**
		 * retrieves groups where $travelID is approved
		 * @param $travelID, default
		 * @return Group[]
		 **/
		public static function getApprovedGroups($travelID = 0){
			require_once('travellog/model/Class.GroupFactory.php');
			require_once('Class.ConnectionProvider.php');
			$rs = ConnectionProvider::instance()->getRecordset();
			
			$sql = "SELECT DISTINCT tblGroup.groupID AS groupID FROM tblGroup ";

			if($travelID > 0){
				$sql .= "JOIN tblGroupApprovedJournals ON 
							(tblGroup.groupID = tblGroupApprovedJournals.groupID
							AND tblGroupApprovedJournals.travelID = " . $travelID.") ";
			}
			
			
			$sql .= "WHERE tblGroup.isSuspended = 0 AND parentID = 0 ";
			
			$rs	=	ConnectionProvider::instance()->getRecordset();
			$rs->execute($sql);

			$arrGroup	= array();
			$arrGroupId	= array();

			if (0 < $rs->Recordcount()){
				while($mGroup = mysql_fetch_array($rs->Resultset()))
					$arrGroupId[] = $mGroup['groupID'];
				$arrGroup = GroupFactory::instance()->create($arrGroupId);
			}
			return $arrGroup;
		}
		
		/**
		 * count groups where $travelID is approved
		 * @param $travelID, default
		 * @return int
		 **/
		public static function countApprovedGroups($travelID = 0){
			require_once('travellog/model/Class.GroupFactory.php');
			require_once('Class.ConnectionProvider.php');
			$rs = ConnectionProvider::instance()->getRecordset();
			
			$sql = "SELECT COUNT(tblGroup.groupID) AS count FROM tblGroup ";

			if($travelID > 0){
				$sql .= "JOIN tblGroupApprovedJournals ON 
							(tblGroup.groupID = tblGroupApprovedJournals.groupID
							AND tblGroupApprovedJournals.travelID = " . $travelID.") ";
			}
			
			
			$sql .= "WHERE tblGroup.isSuspended = 0 AND parentID = 0 GROUP BY tblGroup.groupID";
			
			$rs	=	ConnectionProvider::instance()->getRecordset();
			$rs->execute($sql);

			$row = mysql_fetch_array($rs->Resultset());
			return $row['count'];
		}
	}
