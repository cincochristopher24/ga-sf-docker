<?php

	require_once("travellog/helper/Class.AbstractFeatureChangeNoticeHelper.php");
	
	class AdvisorAccountChangeNoticeHelper extends AbstractFeatureChangeNoticeHelper{
		
		public function isFeatureChangeNoticeShown($travelerID=0){
			$sql = "SELECT travelerID, groupID
					FROM tblShowAccountChangeNoticetoAdvisor
					WHERE travelerID = $travelerID
						AND showNotice = 1
					LIMIT 0,1";
			
			$db = new dbHandler("db=Travel_Logs");
			$rs = $db->Execute($sql);
			
			if( 0 < mysql_num_rows($rs) ){
				$data = mysql_fetch_assoc($rs);
				return $data["groupID"];
			}
			return FALSE;
		}
		
		public function setFeatureChangeNoticeHidden($travelerID=0){
			$sql = "UPDATE tblShowAccountChangeNoticetoAdvisor SET showNotice = 0 WHERE travelerID = $travelerID";
			$db = new dbHandler("db=Travel_Logs");
			$db->Execute($sql);
		}
		
		public function getFeatureChangeNoticeTemplateView($traveler,$group){
			require_once("travellog/views/Class.ViewFeatureChangeNoticeTemplate.php");
			require_once("travellog/model/Class.SiteContext.php");
			
			$noticeView = new ViewFeatureChangeNoticeTemplate();
			if(isset($GLOBALS["CONFIG"])){
				$noticeView->setVars(array("traveler"=>$traveler, "group"=>$group, "siteContext"=>SiteContext::getInstance(), "template"=>"travellog/views/tpl.AdvisorAccountChangeNotice.php"));
			}else{
				$noticeView->setVars(array("traveler"=>$traveler, "group"=>$group, "template"=>"travellog/views/tpl.AdvisorAccountChangeNotice.php"));
			}
			return $noticeView;
		}
		
	}