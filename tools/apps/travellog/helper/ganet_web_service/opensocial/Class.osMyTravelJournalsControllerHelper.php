<?
	require_once 'travellog/helper/ganet_web_service/Class.AbstractMyTravelJournalsControllerHelper.php';
		
	class osMyTravelJournalsControllerHelper extends AbstractMyTravelJournalsControllerHelper {

        public $appViewType;
		
		function __construct(){

	     	$this->action		= isset($_POST['action']) ? $_POST['action'] : null;
			$this->domain		= isset($_POST['domain']) ? $_POST['domain'] : null;	

			$this->ownerID		= isset($_POST['ownerID']) ? $_POST['ownerID'] : null;
			if(!is_numeric($this->ownerID)) 
				$this->ownerID  = self::convertContainerIDWithCharToNumeric($this->ownerID);

			$this->viewerID 	= isset($_POST['viewerID']) ? $_POST['viewerID'] : null;
			if(!is_numeric($this->viewerID)) 
				$this->viewerID = self::convertContainerIDWithCharToNumeric($this->viewerID);

			$this->username 	= isset($_POST['username']) ? $_POST['username'] : null;
			$this->password 	= isset($_POST['password']) ? $_POST['password'] : null;
			$this->ownerName	= isset($_POST['ownerName']) ? $_POST['ownerName'] : null;
			$this->viewType		= isset($_POST['viewType']) ? $_POST['viewType'] : null;
			$this->appViewType	= isset($_POST['appViewType']) ? $_POST['appViewType'] : null;							            

			if(isset($_POST['travelID']))
				$this->travelIDs  = trim($_POST['travelID']) != '' ? explode(' ', trim($_POST['travelID'])) : null;
			else
				$this->travelIDs = null;
		}
     
		function getAppViewType(){
			return $this->appViewType;
		}
		
		private static function convertContainerIDWithCharToNumeric($containerID) {
			$convertedContainerID = '';
			for($i=0; $i< strlen(substr($containerID,2)); $i++){
				$char = substr($containerID, $i,1);
				$convertedContainerID .= is_numeric($char) ? (string)$char : (string)self::convertCharToNumber($char);
			} 
			return $convertedContainerID;
		}

		private static function convertCharToNumber($char){
			$value=10;
			$arrayChar = array();
			for ($i = 'a'; $i !== 'aa'; $i++) {
				$arrayChar[$i] = $value;
				$value++;
			}
			return $arrayChar[$char];
		}
		
	} 
 ?>	