<?
	require_once('Class.Template.php'); 
	require_once('travellog/model/Class.InquiryMessage.php');
	require_once("travellog/model/Class.GroupFactory.php");
	require_once('Class.Constants.php');
	
	function typeorganizer($arrToDisplay,$topHeader,$topHeader1 = '',$convertPluralForm = 0,$tbWidth = 0){
	
		if( count($arrToDisplay) > 7 )
			$reccount = ceil(count($arrToDisplay) / 3);
		else
			$reccount = 7;
		
		$tdspan = '';
		
		if( count($arrToDisplay) > 8 )
			$tdspan = "colspan = 2";
		elseif( count($arrToDisplay) > 16 )	 
			$tdspan = "colspan = 3";
	
		echo '<table width="' . $tbWidth . '" style="margin-top:2px; clear:both;" cellspacing="0">';
		echo "<tr><td " . $tdspan . " align=\"left\" ><strong>";
		echo $topHeader;
		if( $convertPluralForm == 0){
			if( count($arrToDisplay) > 1 ){
				if( $topHeader{strlen(trim($topHeader)) -1} != 's' )
					echo 's';
				else
					echo 'es';
			}
		}
		echo " " . $topHeader1 . " :</strong></td></tr>";
		echo '<tr><td style="padding-left:10px" valign="top"><ul>';
		$loopIdx = 1;
		foreach( $arrToDisplay as $element)	{
			echo "<li>$element</li>";
			if( $loopIdx % $reccount == 0)
				echo "</ul></td><td style=\"padding-left:10px\" valign=\"top\"><ul>";
			$loopIdx++;		
		}
		echo "</ul></td></tr></table>";

	}
	
	/**
	 * function recurseReply
	 * for displaying message replies
	 * @param object $message - see Class.PersonalMessage.php | Class.InquiryMessage.php
	 * @param string inquiryView 
	 */ 
	function recurseReply($message,$inquiryView = "studentView"){
		$classname = ( $message->getParentID() == 0 ) ? "msg_parent" : "msg_child";
		echo "<div class='$classname'>";
		$tmpMsg = new Template();
		$tmpMsg->set_vars( array(
			'msg'=>$message,
			'inquiryView'=>$inquiryView
		));
		$tmpMsg->out('travellog/views/tpl.ViewIndividualMessage.php');
		while($arrRepMsg = $message->getRepliesToMessage()){
			foreach($arrRepMsg as $repMsg){
				recurseReply($repMsg,$inquiryView);
			}
			echo "</div>";
			return;
		}
		echo "</div>";
	}
	
	/**
	 * function displayMessageStaffLink
	 * displays the link for destination
	 * @param array recipients
	 * @param object group
	 * @param numeric ownerID - the travelerID of the message source 
	 */
	function displayMessageStaffLink($recipients, $group, $ownerID = 0){
		$msg = "";
		$rootLink = "";
		$config = isset($GLOBALS["CONFIG"]) ? $GLOBALS["CONFIG"] : NULL;
		
		if( constants::ADMIN_TRAVELER_ID == $ownerID )
			$rootLink = "../";
		
		foreach($recipients as $iRecipient){
			$msg .= "<a href='" . $rootLink . "group.php?gID=" . $group->getGroupID() .  "'>";
			$msg .= $iRecipient->getUsername() . " [ " . $group->getName() . " ]";
			$msg .= "</a> ";
		}
		return $msg;
	}
	
	
	/**
	 * function displayLink
	 * displays the link for message source or destination
	 * @param object objLinkMe - the source | reciepient of the message
	 * @param numeric owner - the travelerID of the message source 
	 */ 
	function displayLink($objLinkMe,$ownerID = 0,$folder = constants::INBOX){
		$msg = "";
		$rootLink = "";
		$config = isset($GLOBALS["CONFIG"]) ? $GLOBALS["CONFIG"] : NULL;
		
		if( constants::ADMIN_TRAVELER_ID == $ownerID )
			$rootLink = "../";
		
		if( get_class($objLinkMe) == 'Staff' ){
			if( $objLinkMe->getStatus() && !$objLinkMe->getAdvisorGroup()->isSuspended() )
				$msg .= "<a href='" . $rootLink . "group.php?gID=" . $objLinkMe->getAdvisorGroup()->getGroupID() .  "'>";
			$msg .= $objLinkMe->getUsername() . " [ " . $objLinkMe->getAdvisorGroup()->getName() . " ]";
			if( $objLinkMe->getStatus() && !$objLinkMe->getAdvisorGroup()->isSuspended() )
				$msg .= "</a>";	
		}
		elseif( $objLinkMe instanceof Group ){
			$showLink = false;
			if( $config && ($objLinkMe->getGroupID() == $config->getGroupID() || $config->getGroupID() == $objLinkMe->getParentID()) )
				$showLink = true;
			if( $showLink && !($objLinkMe->isSuspended()) )
				$showLink = true;	
			if( $showLink )
				$msg .= "<a href='" . $rootLink . "group.php?gID=" . $objLinkMe->getGroupID() .  "'>";
			$msg .= "[ " . $objLinkMe->getName() . " ]"; 	
			if( $showLink )
				$msg .= "</a>";	
		}
		elseif( $objLinkMe instanceof Traveler ){
			//if( $config )
				$gFactory = GroupFactory::Instance();
			if( !$objLinkMe->isSuspended() && ( $ownerID > 0 && !$objLinkMe->isBlocked($ownerID) && $objLinkMe->getTravelerID() != $ownerID ) && $objLinkMe->getTravelerID() != constants::ADMIN_TRAVELER_ID ){
				if ($config){
					$group = $gFactory->create( array($config->getGroupID()) );
					$mGroup = $group[0]->isMember($objLinkMe);
				}if( !$config || $mGroup )
					$msg .= "<a href='" . $rootLink . $objLinkMe->getUserName() . "'>";
			}
			if(constants::INQUIRY == $folder)
				$msg .= $objLinkMe->getTravelerProfile()->getLastName() . ", " . $objLinkMe->getTravelerProfile()->getFirstName();
			else	
				$msg .= $objLinkMe->getUserName();	
			if( !$objLinkMe->isSuspended() && ( $ownerID > 0 && !$objLinkMe->isBlocked($ownerID) && $objLinkMe->getTravelerID() != $ownerID ) && $objLinkMe->getTravelerID() != constants::ADMIN_TRAVELER_ID ){
				if( $config )
					$group = $gFactory->create( array($config->getGroupID()) );
				if( !$config || ($config && $group[0]->isMember($objLinkMe)) )
					$msg .= "</a>";
			}		
		}
		elseif( $objLinkMe instanceof gaGroup ){
			$msg .= "<a href='" . $rootLink . "group.php?gID=" . $objLinkMe->getGroupID() .  "'>";
			$msg .= "[ " . $objLinkMe->getName() . " Staffs ]"; 
			$msg .= "</a>";
		}
		elseif( $objLinkMe instanceof client )
			$msg .= $objLinkMe->getInstName();
		elseif( $objLinkMe instanceof gaTraveler ){
			$msg .= "<a href='". $rootLink . $objLinkMe->getUsername() ."'>";
			$msg .= $objLinkMe->getUsername();
			$msg .="</a>";
		}
		return $msg;
	}
	
	/**
	 * function parseSubjectRE
	 * returns Re[n]: $subject
	 * @param string subject
	 */
	function parseSubjectRE( $subject = ""){
		$reCount = 1;
		//if( ereg('^Re: ',$subject) ) {
		if( preg_match('/^Re: /',$subject) ) {
			//while( ereg('^Re: ',$subject)){
			while( preg_match('/^Re: /',$subject)){
				//$subject = ereg_replace('^Re: ','',$subject);
				$subject = preg_replace('/^Re: /','',$subject);
				$reCount++;
			}	
		}
		//elseif( ereg('^Re\[[0-9]+\]:',$subject) ){
		elseif( preg_match('/^Re\[[0-9]+\]:/',$subject) ){
			preg_match('/^Re\[([0-9]+)\]:/i',$subject,$matches);
			$reCount = $matches[1] + 1;
			$subject = preg_replace('/^Re\[([0-9]+)\]: /i',"",$subject);
		}
		if( $reCount > 1 )
			$subject = "Re[" . $reCount. "]: $subject";
		else	
			$subject = "Re: " . $subject;
		return $subject;	
	} 
	
?>