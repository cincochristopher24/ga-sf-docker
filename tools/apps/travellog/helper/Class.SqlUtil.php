<?php
  /**
   * @(#) Class.SqlUtil.php
   * 
   * Converter of a given RowsLimit, Condition or FilterCriteria2 to be
   * a sql clause. 
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 12 11, 08
   */
  
  require_once "Class.dbHandler.php";
  require_once "travellog/model/Class.FilterOp.php";
  
  class SqlUtil {
  	/**
  	 * Converts a given RowsLimit object to a LIMIT clause
  	 * 
  	 * @param RowsLimit $limit
  	 * @return string
  	 */
  	static function createLimitClause($limit){
  		return "LIMIT " . $limit->getOffset() . ", " . $limit->getRows();
  	}
  	
  	/**
  	 * Converts a given SqlFields object to field clause
  	 * or shall we say the fields where the data will be get.
  	 * For example:
  	 *   In SELECT a.grpID, a.name, b.name as name2 FROM tblGroup as a, tblCustomizedGroup as b sql statement,
  	 *   the field clause where `a.grpID`, `a.name` and `b.name as name2`
  	 * 
  	 * @param SqlFields $columns
  	 * @return string
  	 */
  	static function createFieldClause($columns){
  		$columns = $columns->getAllSqlFields();

  		if (0 === count($columns)) {
  			return "*";
  		}
  		
  		$clause = "";
  		
  		foreach($columns as $column){
  			$db = ("" == trim($column->getDbName())) ? "" : "`".$column->getDbName()."`.";
  			$tb = ("" == trim($column->getTableName())) ? "" : "`".$column->getTableName()."`.";
  			$fld = ("*" == trim($column->getFieldName()) || "" == trim($column->getFieldName())) ? "*" : "`".$column->getFieldName()."`";
  			$as = ("" == trim($column->getAlias())) ? "" : " as ".$column->getAlias();
  			$clause .= $db.$tb.$fld.$as.", ";
  		}
  		
  		return preg_replace('/, $/', '', $clause);
  	}
  	
  	/**
  	 * Converts a given SqlFields object to table clause or the database
  	 * tables sql clause.
  	 * For example in select statement below:
  	 *   SELECT * FROM tblGroup as a, tblTraveler as b WHERE a.grpID = b.travID 
  	 * the table clause where `tblGroup as a` and `tblCustomizedGroup as b`.
  	 */
  	static function createTableClause($columns){
  		$columns = $columns->getAllSqlFields();
  		
  		if (0 === count($columns)) {
  			return "";
  		}
  		
  		$clause = "";
  		
  		foreach($columns as $column){
  			$db = ("" == trim($column->getDbName())) ? "" : "`".$column->getDbName()."`.";
  			$tb = ("" == trim($column->getTableName())) ? "" : "`".$column->getTableName()."`";
  			$as = ("" == trim($column->getAlias())) ? "" : " as ".$column->getAlias();
  			$clause .= $db.$tb.$as.", ";
  		}
  		
  		return preg_replace('/, $/', '', $clause);
  	}
  	
  	/**
  	 * Converts a given FilterCriteria2 to ORDER BY clause
  	 * 
  	 * @param FilterCriteria2 $order
  	 * @return string
  	 */
  	static function createOrderByClause($order){
      $order_string = "";
      $condition = $order->getConditions();
		  
		  foreach ($condition as $eachcondition) {
			  $attname  = $eachcondition->getAttributeName();                   			
        
        switch($eachcondition->getOperation()){	        				
          case FilterOp::$ORDER_BY:
            $sortorder = " ASC ";
            break;
          case FilterOp::$ORDER_BY_DESC:
            $sortorder = " DESC ";
            break;
          default:
            $sortorder = " ASC ";
            break;
        }
        
			  $order_string .= $attname.$sortorder.", ";
      }
      
      return " ORDER BY ".preg_replace('/, $/','',$order_string); 		
  	}
  	
  	/**
  	 * Converts a given FilterCriteria2 to conditions with boolean operators clause.
  	 * This handles the =, <>, <, >, LIKE, IN, NOT IN and >= conditions. For example, AND tbY.x=5 AND tbX.x=tbY.y... 
  	 * 
  	 * @param FilterCriteria2 $filter
  	 * @return string returns empty string if the FilterCriteria2 passed is malformed
  	 */
  	static function createConditionClause($filter){
      $handler = new dbHandler();
      $filterstr = "";
      
      $condition = $filter->getConditions();	
      $booleanop = $filter->getBooleanOps();
    			
      for($i=0; count($condition) > $i; $i++){
        
        $attname = $condition[$i]->getAttributeName();
        $attname = (preg_match("/\./",$attname)) ? " ".$attname : " `".$attname."` ";                    			
        $op = $condition[$i]->getOperation();
        $value 	 = $condition[$i]->getValue();
        $value = (preg_match("/\./",$value) OR $op == FilterOp::$IN OR $op == FilterOp::$NOT_IN) ? $value : $handler->makeSqlSafeString($value);
        
        $filterstr .= $booleanop[$i].$attname;
        	
        switch($op){
          case FilterOp::$EQUAL:
        	  $filterstr .= " = ".$value." ";
        		break;
        	case FilterOp::$NOT_EQUAL:
        		$filterstr .= " <> ".$value." ";
        		break;
        	case FilterOp::$GREATER_THAN_OR_EQUAL:
        		$filterstr .= " >= ".$value." ";
        		break;
        	case FilterOp::$GREATER_THAN:
        		$filterstr .= " > ".$value." ";
        		break;
        	case FilterOp::$LESS_THAN:
        		$filterstr .= " < ".$value." ";
        		break;
        	case FilterOp::$LIKE:
        		$filterstr .= " LIKE ".$value." ";
        		break;
        	case FilterOp::$IN:
        		$filterstr .= " IN ($value) ";
        		break;
        	case FilterOp::$NOT_IN:
        		$filterstr .= " NOT IN ($value) ";
        		break;
        	default:
        		$filterstr .= " = ".$value." ";
        		break;
        }
      }
      
      return preg_replace('/, $/','',$filterstr);	
    }
  }
