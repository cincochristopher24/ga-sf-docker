<?php
	/**
	 * @(#) Class.DiscussionHelper.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 18, 2009
	 */
	
	require_once('Class.Template.php');
	require_once('Class.GaDateTime.php');
	
	/**
	 * Helper class for showing the featured and unfeatured discussions.
	 */
	class DiscussionHelper {
		
		/**
		 * The template file.
		 * 
		 * @var string
		 */
		private $mTemplateFile = 'tpl.IncDiscussion.php';
		
		/**
		 * The processed featured discussion template.
		 * 
		 * @var string
		 */
		private $mFeaturedTemplate = "";
		
		/**
		 * The processed unfeatured discussion template.
		 * 
		 * @var string
		 */
		private $mUnFeaturedTemplate = "";
		
		/**
		 * The flag whether the admin is logged.
		 * 
		 * @var boolean
		 */
		private $mIsAdminLogged = false;
		
		/**
		 * Creates a new discussion helper.
		 * 
		 * @param boolean $is_admin_logged The flag whether the admin is logged
		 */
		function __construct($is_admin_logged){
			$this->mIsAdminLogged = $is_admin_logged;
		}

		/**
		 * Adds a discussion to be rendered.
		 * 
		 * @param Discussion[] $discussion	The discussions to be added
		 * @param int 				$count				The number of discussions post
		 * 
		 * @return void
		 */		
		function addDiscussions(array $discussions){
			foreach ($discussions as $discussion) {
				$this->addDiscussion($discussion);
			}
		}
		
		/**
		 * Adds a discussion to be rendered.
		 * 
		 * @param Discussion	$discussion The discussion to be added
		 * @param int 				$count 			The number of discussions post
		 * 
		 * @return void
		 */
		function addDiscussion(Discussion $discussion){
			
			$posts_count = $discussion->getPostsCount();
			
			if( 0 == $posts_count ){
				return null;
			}
			
			$latest_post = (1 < $posts_count) ? $discussion->getLatestPost() : null;
			$latest_poster = (is_null($latest_post)) ? null : $latest_post->getPoster();
			
			$firstPost 		= $discussion->getFirstPost();
			$firstPoster 	= is_null($firstPost) ? null : $firstPost->getPoster(); 
			
			$tpl = new Template();
			$tpl->set('discussion', $discussion);
			$tpl->set('first_post', $firstPost);
			$tpl->set('first_poster', $firstPoster);
			$tpl->set('posts_count', $posts_count);
			$tpl->set('latest_post', $latest_post);
			$tpl->set('latest_poster', $latest_poster);
			$tpl->set('is_admin_logged', $this->mIsAdminLogged);
			$tpl->set('title', $discussion->getTitle());
			$tpl->set('discussion_id', $discussion->getId());
			$tpl->set('topic_id', $discussion->getTopic()->getId());
			$tpl->set('topic', $discussion->getTopic());
			$tpl->set('is_subgroup', (0 < $discussion->getTopic()->getGroup()->getParentID()));
			
			if (($discussion->isFeatured())) {
				$this->mFeaturedTemplate .= $tpl->fetch('travellog/views/discussionboard/'.$this->mTemplateFile);
			}
			else {
				$this->mUnFeaturedTemplate .= $tpl->fetch('travellog/views/discussionboard/'.$this->mTemplateFile);
			}
		}
		
		/**
		 * Returns true if there are featured data to be shown.
		 * 
		 * @return true if there are featured data to be shown.
		 */
		function hasFeaturedDiscussions(){
			return (empty($this->mFeaturedTemplate)) ? false : true;
		}
		
		/**
		 * Returns true if there are unfeatured data to be shown.
		 * 
		 * @return true if there are unfeatured data to be shown.
		 */
		function hasUnFeaturedDiscussions(){
			return (empty($this->mUnFeaturedTemplate)) ? false : true;
		}
		
		/**
		 * Renders the template of featured discussions.
		 * 
		 * @return void
		 */
		function renderUnFeaturedDiscussions(){
			echo $this->mUnFeaturedTemplate;
		}
		
		/**
		 * Renders the template of featured discussions.
		 * 
		 * @return void
		 */
		function renderFeaturedDiscussions(){
			echo $this->mFeaturedTemplate;
		}
		
		/**
		 * Sets the template file to be used.
		 * 
		 * @param string $template The template file to be used.
		 * 
		 * @return void
		 */
		function setTemplate($template){
			$this->mTemplateFile = $template;
		}
	}