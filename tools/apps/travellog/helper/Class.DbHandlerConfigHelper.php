<?php
/*
 * Created on 10 6, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

	require_once('Class.dbHandler.php');
	
	class DbHandlerConfigHelper {
		public static function getDbHandler() {
			$handler = new dbHandler();
			
			if (isset($_REQUEST['test']))
				$handler->setDB('GanetVideoTestDB');
			else
				$handler->setDB('Travel_Logs');
				
			return $handler;
		}
	}