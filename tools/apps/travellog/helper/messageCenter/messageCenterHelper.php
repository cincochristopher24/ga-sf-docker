<?php

/**
 * manually checks if the system is being called in cobrand context
 * 
 * @return TRUE is in cobrand
 * @return FALSE otherwise
 */
function isInCoBrand() {
	if (isset($GLOBALS["CONFIG"])) {
		return TRUE;
	}
	
	return FALSE;
}

/**
 * retrieves the cobrand group if the system is
 * called in cobrand context
 * 
 * @return the cobranded group
 */
function getCoBrandGroup() {
	if (!isInCoBrand()) {
		throw new Exception ("This function applies only in cobrand context.");
	}
	
	require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
	
	$config = $GLOBALS['CONFIG'];
	
	return gaGroupMapper::getGroup($config->getGroupID());
}

/**
 * provides a convenient way of including a partial view
 * 
 * @param $filename - name of the view file (not the actual filename)
 * @param $variables - optional array of varialbes needed by the view file
 * 
 * @return displays the output of the view file
 */
function includeView($filename, $variables = array()) {
	$defaultViewFolder = 'travellog/views/messageCenter/';
	
	// determine filename through convention _$filename.php
	$filename = $defaultViewFolder . '_' . $filename . '.php';
	
//	if (!file_exists($filename)) {
//		throw new Exception ("The view file $filename does not exist.");
//	}
	
	// make variables available
	foreach ($variables as $variable => $value) {
		$$variable = $value;
	}
	
	require $filename;
	
}

function includeComponent() {
	
}

function wrapInboxMessages(array $messages, $ownerID, $hasGroupPowers = FALSE) {
	$wrappedMessages = array();
	foreach ($messages as $message) {
		$wrappedMessages[] = wrapInboxMessage($message, $ownerID, $hasGroupPowers);
	}
	
	return $wrappedMessages;
}

function wrapOutboxMessages(array $messages, $hasGroupPowers = FALSE) {
	$wrappedMessages = array();
	foreach ($messages as $message) {
		$wrappedMessages[] = wrapOutboxMessage($message, $hasGroupPowers);
	}
	
	return $wrappedMessages;
}

function wrapOutboxMessage($theMessage, $hasGroupPowers = FALSE) {
	require_once 'Class.gaParameterHolder.php';
	require_once 'travellog/model/messageCenter/Class.gaUserMapper.php';
	
	$message = new gaParameterHolder();
	
	$message->set ( 'contentType', gaMessage::PERSONAL                                              );
	$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-0' );
	
	$senderLinks = array();
	
	foreach ($theMessage->getDisplayedRecipients() as $recipient) {
//	foreach ($theMessage->getRecipients() as $recipient) {
		if (!isInCoBrand()) {
			if (get_class($recipient) == 'gaTraveler') {
				if ($hasGroupPowers) {
					$senderLinks[] = "<a href=\"{$recipient->getSenderLink()}\">{$recipient->getDisplayName()}</a>" . ' <span style="font-color:gray"><em>' . $recipient->getName() . '</em></span>';
				} else {
					$senderLinks[] = "<a href=\"{$recipient->getSenderLink()}\">{$recipient->getDisplayName()}</a>";
				}
			} else {
				$senderLinks[] = "<a href=\"{$recipient->getSenderLink()}\">{$recipient->getDisplayName()}</a>";
			}
		} else {
			if (get_class($recipient) == 'gaTraveler') {
				if (gaUserMapper::isMemberOfGroup($recipient, getCoBrandGroup())) {
					if ($hasGroupPowers) {
						$senderLinks[] = "<a href=\"{$recipient->getSenderLink()}\">{$recipient->getUserName()}</a> " . ' <span style="font-color:gray"><em>' . $recipient->getName() . '</em></span>';
					} else {
						$senderLinks[] = "<a href=\"{$recipient->getSenderLink()}\">{$recipient->getUserName()}</a>";
					}
				} else {
					if ($hasGroupPowers) {
						$senderLinks[] = $recipient->getUserName() . ' <span style="font-color:gray"><em>' . $recipient->getName() . '</em></span>';
					} else {
						$senderLinks[] = $recipient->getUserName();
					}
				}
			} else {
				if ($recipient->getID() == getCoBrandGroup()->getID() ||
					$recipient->getParentGroup()->getID() == getCoBrandGroup()->getID()) {
					
					$link = '<a href="%s">%s</a>';
					$senderLinks[] = sprintf($link, $recipient->getSenderLink(), $recipient->getDisplayName()); 
				} else {
					$senderLinks[] = $recipient->getDisplayName();
				}
			}
		}
	}
	
	$message->set ( 'senderLink',  join(',', $senderLinks) );
	$message->set ( 'dateCreated', date('F j, Y', strtotime($theMessage->getCreated())) );
	$message->set ( 'title',       $theMessage->getTitle()                              );
	$message->set ( 'content',     nl2br($theMessage->getMessage())                     );
	$message->set ( 'cssClass',    'message'                                            );
	$message->set ( 'allowReply',  FALSE                                                );
	$message->set ( 'senderID',    $theMessage->getSenderID()                           );
	
	return $message;
}

function wrapInboxMessage($theMessage, $ownerID, $hasGroupPowers) {
	require_once 'travellog/model/messageCenter/Class.gaUserMapper.php';
	require_once 'Class.gaParameterHolder.php';
	
	$message = new gaParameterHolder();
	
	switch (get_class($theMessage)) {
		case 'Comment':
			/////////////// START CHERYL'S CODE ///////////////////
			require_once 'travellog/model/Class.PokeType.php';
			if (!isInCoBrand()) {
				require_once 'travellog/views/Class.PokesView.php';
			} else {
				require_once 'travellog/custom/COBRAND/views/Class.PokesViewCB.php';
			}
			
		
			$Poke      = new PokeType($theMessage->getPokeType());
			$pokesview = new PokesView();

			$pokesview->setAuthorId     ( $theMessage->getAuthor()       );
  			$pokesview->setAuthorObject ( $theMessage->getAuthorObject() );
			$pokesview->setOwnerId      ( $ownerID                       );
			$pokesview->setPokeTypeId   ( $Poke->getPokeTypeID()         );
			$pokesview->setMessage      ( $theMessage->getTheText()      );

			if (!strcasecmp("TravelLog", get_class($theMessage->getContext()))){
				$pokesview-> setContextId($theMessage->getContext()->getTravelLogID());
				$pokesview-> setContext(3);
			}
			elseif (!strcasecmp("Photo", get_class($theMessage->getContext()))){
				$pokesview-> setContextId($theMessage->getContext()->getPhotoID());
				$pokesview-> setContext(2);
			}
			else{
				$pokesview-> setContextId($theMessage->getContext()->getTravelerID());
				$pokesview-> setContext(1);
			}

			if (0 < strlen($theMessage-> getTheText()))
				$pokesview-> setHasMessage(true);
			else
				$pokesview-> setHasMessage(false);
			$pokesview->setAuthor();
			$pokesview->setOwner();
			////////////// END CHERYL'S CODE ////////////////////////////////////////						
			
			$message->set ( 'contentType', gaMessage::SHOUTOUT );
			$message->set ( 'ID',          $theMessage->getCommentID() . '-' . $message->get('contentType') . '-' . $theMessage->isRead() );
			
			if (!isInCoBrand()) {
				if ($hasGroupPowers && $theMessage->getAuthorType() == 2) {
					$message->set('senderLink',  $pokesview->getSenderPath() . ' <span><em>' . $pokesview->getAuthorFullName() .  ' </em></span>');
				} else {
					$message->set('senderLink',  $pokesview->getSenderPath());
				}
			} else {
				if ($hasGroupPowers && $theMessage->getAuthorType() == 2) {
					$message->set('senderLink',  $pokesview->getAuthorName() . ' <span><em>' . $pokesview->getAuthorFullName() .  ' </em></span>');
				} else {
					$message->set('senderLink',  $pokesview->getAuthorName());
				}
			}
			$message->set ( 'dateCreated', strtotime($theMessage->getCreated()) ? date('F j, Y', strtotime($theMessage->getCreated())):'' );
			$message->set ( 'title',       $pokesview->getPassportMessage()                     );
			$message->set ( 'content',     ''                                                   );
			$message->set ( 'cssClass',    'shoutout'                                           );
			$message->set ( 'allowReply',  FALSE                                                );
			$message->set ( 'isRead',      TRUE                                                 );

			break;
		
		case 'gaTravelerPersonalMessage':
		case 'gaToGroupStaffPersonalMessageCopy':
		case 'gaToTravelerPersonalMessageCopy':
		case 'gaPersonalMessage':
			$message->set ( 'contentType', gaMessage::PERSONAL                                                                     );
			$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-' . $theMessage->isRead() );
			
			if ($hasGroupPowers) {
				$message->set ( 'sender', $theMessage->getSender()->getUserName() . ' <span style="font-color:gray"><em>' . $theMessage->getSender()->getName() . '</em></span>');
			} else {
				$message->set ( 'sender', $theMessage->getSender()->getUserName());
			}
			
			if ( (!isInCoBrand() || gaUserMapper::isMemberOfGroup($theMessage->getSender(), getCoBrandGroup())) && $theMessage->getSender()->getUserName() != 'The GoAbroad Network Team') {
				if ($hasGroupPowers) {
					$message->set ( 'senderLink', "<a href=\"/{$theMessage->getSender()->getUserName()}\">{$theMessage->getSender()->getUserName()}</a>" . ' <span style="font-color:gray"><em>' . $theMessage->getSender()->getName() . '</em></span>');
				} else {
					$message->set ( 'senderLink', "<a href=\"/{$theMessage->getSender()->getUserName()}\">{$theMessage->getSender()->getUserName()}</a>" );
				}
			} else {
				if ($hasGroupPowers) {
					$message->set ( 'senderLink', $theMessage->getSender()->getUserName() . '<span style="font-color:gray"><em>' . $theMessage->getSender()->getName() . '</em></span>');
				} else {
					$message->set ( 'senderLink', $theMessage->getSender()->getUserName());
				}
			}
			$message->set ( 'dateCreated', strtotime($theMessage->getCreated()) ? date('F j, Y', strtotime($theMessage->getCreated())):'' );
			$message->set ( 'title',       $theMessage->getTitle()                              );
			$message->set ( 'content',     nl2br($theMessage->getMessage())                     );
			$message->set ( 'cssClass',    'message'                                            );
			$message->set ( 'allowReply',  TRUE                                                 );
			$message->set ( 'senderID',    $theMessage->getSenderID()                           );
			$message->set ( 'isRead',      $theMessage->isRead()                                );

			break;
			
		case 'gaGroupPersonalMessage':
			$message->set ( 'contentType', gaMessage::PERSONAL                                                                     );
			$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-' . $theMessage->isRead() );
			$message->set ( 'sender',      $theMessage->getSender()->getName()                                                     );

			if ((!isInCoBrand() || 
				$theMessage->getSender()->getID() == getCoBrandGroup()->getID() ||
				$theMessage->getSender()->getParentGroup()->getID() == getCoBrandGroup()->getID()) && $theMessage->getSender()->getName() != 'The GoAbroad Network Team') {

				$message->set ( 'senderLink', "<a href=\"{$theMessage->getSender()->getFriendlyURL()}\">{$theMessage->getSender()->getName()}</a>" );
			} else {
				$message->set ( 'senderLink', $theMessage->getSender()->getName() );
			}
			$message->set ( 'dateCreated', strtotime($theMessage->getCreated()) ? date('F j, Y', strtotime($theMessage->getCreated())):'' );
			$message->set ( 'title',       $theMessage->getTitle()                              );
			$message->set ( 'content',     nl2br($theMessage->getMessage())                     );
			$message->set ( 'cssClass',    'message'                                            );
			$message->set ( 'allowReply',  TRUE                                                 );
			$message->set ( 'senderID',    $theMessage->getSenderID()                           );
			$message->set ( 'isRead',      $theMessage->isRead()                                );
			
			break;
		
		case 'gaNews':
			$message->set ( 'contentType', gaMessage::NEWS                                                                         );
			$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-' . $theMessage->isRead() );

			$message->set ( 'sender',      $theMessage->getSender()->getName()                                                     );
			if (!isInCoBrand() || 
				$theMessage->getSender()->getID() == getCoBrandGroup()->getID() ||
				$theMessage->getSender()->getParentGroup()->getID() == getCoBrandGroup()->getID()) {

				$message->set ( 'senderLink', "<a href=\"{$theMessage->getSender()->getFriendlyURL()}\">{$theMessage->getSender()->getName()}</a>" );
			} else {
				$message->set ( 'senderLink', $theMessage->getSender()->getName() );
			}
			
			$message->set ( 'dateCreated', strtotime($theMessage->getCreated()) ? date('F j, Y', strtotime($theMessage->getCreated())):'' );
			$message->set ( 'title',       $theMessage->getTitle()                              );
			$message->set ( 'content',     nl2br($theMessage->getContent())                     );

			if ($theMessage->isPrioritized()) {
				$message->set ( 'cssClass', 'alerts');
			} else {
				$message->set ( 'cssClass', 'news');
			}
			
			$message->set ( 'allowReply',  FALSE                 );
			$message->set ( 'isRead',      $theMessage->isRead() );
			
			break;
	}
	
	return $message;
}