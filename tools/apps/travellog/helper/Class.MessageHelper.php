<?php

	/*
	 * Class.MessageHelper.php
	 * Created on Nov 14, 2007
	 * created by marc
	 */
	
	require_once("Class.Constants.php");
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/model/Class.RowsLimit.php");
		 
	class MessageHelper{
		
		static $instance;
		
		static function getInstance(){
			if( self::$instance == NULL ) self::$instance = new MessageHelper;
			return self::$instance;
		}
		
		function getUserGroupID(){
			$tmpGID = 0;
			if( 0 < SessionManager::get("gID") )
				$tmpGID = SessionManager::get("gID");
			elseif( isset($_POST["hdnSourceID"]) )
				$tmpGID = $_POST["hdnSourceID"];
			elseif( isset($_GET["sourceID"]) )
				$tmpGID	= (int)$_GET["sourceID"];
			return $tmpGID;	
		}
		
		function getFolder(){
			if( isset($_POST['hdnActive']) && strlen(trim($_POST['hdnActive'])) )
				$FOLDER = $_POST['hdnActive'];
			elseif( isset($_GET['sent']) || ( isset($_GET['active']) && 0 == strcmp($_GET['active'],'sent') ) )
				$FOLDER = constants::SENT_ITEMS;
			elseif( isset($_GET['drafts']) || ( isset($_GET['active']) && 0 == strcmp($_GET['active'],'drafts') ) )
				$FOLDER = constants::DRAFTS;
			elseif( isset($_GET['trash']) || ( isset($_GET['active']) && 0 == strcmp($_GET['active'],'trash') ) )			
				$FOLDER = constants::TRASH;
			elseif( isset($_GET['inquiry'])|| isset($_GET['inquire']) || ( isset($_GET['active']) && 0 == strcmp($_GET['active'],'inquiry') ) )
				$FOLDER = constants::INQUIRY;
			else
				$FOLDER = constants::INBOX;
			return $FOLDER;	
		}
		
		function getAction(){
			
			if ( isset($_POST['btnBack']) || ( isset($_POST["hdnAction"]) && $_POST["hdnAction"] == "BACK") )
				$ACTION = constants::BACK;
			elseif( isset($_POST['btnDraft']) || ( isset($_POST["hdnAction"]) && $_POST["hdnAction"] == "SAVE_AS_DRAFT") )
				$ACTION = constants::SAVE_AS_DRAFT;
			elseif( isset($_POST['btnTrash']) || ( isset($_POST["hdnAction"]) && $_POST["hdnAction"] == "MOVE_TO_TRASH") )
				$ACTION = constants::MOVE_TO_TRASH;
			elseif( isset($_GET['delete']) || isset($_POST['btnDelete']) || ( isset($_POST["hdnAction"]) && $_POST["hdnAction"] == "DELETE") )
				$ACTION = constants::DELETE;
			elseif( isset($_POST['btnReply']) || isset( $_GET['reply']) || ( isset($_POST["hdnAction"]) && $_POST["hdnAction"] == "REPLY") )
				$ACTION = constants::REPLY;
			elseif( isset( $_POST['btnEdit']) || ( isset($_POST["hdnAction"]) && $_POST["hdnAction"] == "EDIT") )
				$ACTION = constants::EDIT;
			elseif( isset( $_POST['btnSend']) || ( isset($_POST["hdnAction"]) && $_POST["hdnAction"] == "SEND") )
				$ACTION = constants::SEND;	
			elseif( isset($_GET['compose']) )
				$ACTION = constants::COMPOSE;
			elseif( isset( $_GET['inquire']) )
				$ACTION = constants::INQUIRE;
			elseif( isset($_GET['view']) )
				$ACTION = constants::VIEW;
			else
				$ACTION = constants::VIEW_MESSAGE_LIST;
			return $ACTION;	
		}
		
		function getUserMode(){
			if( 0 < SessionManager::get("gID") || isset($_GET['groupTo']) || isset($_GET['group']) )	
				$MODE = constants::GROUP;
			else
				$MODE = constants::TRAVELER;
			return $MODE;	
		}
		
		function getReferer(){
			$REFERER = ""; 
			if( isset($_POST['hdnReferer']) && strlen(trim($_POST['hdnReferer'])) )
				$REFERER = $_POST['hdnReferer'];
			elseif( isset($_SERVER['HTTP_REFERER']) && strlen(trim($_SERVER['HTTP_REFERER'])) )
				$REFERER = $_SERVER['HTTP_REFERER'];
			return $REFERER;	
		}
		
		function getListingID(){
			if( isset($_POST["listingID"]) )
				$listingID = (int)$_POST["listingID"];
			elseif( isset($_GET["listingID"]) )
				$listingID = (int)$_GET["listingID"];
			else
				$listingID = 0;
			return $listingID;				
		}
		
		function getAttributeID(){
			$messageID = 0;
			if( isset($_POST["hdnMessageID"]) )
				$messageID = (int)$_POST["hdnMessageID"];
			elseif( isset($_GET["messageID"]) )
				$messageID = (int)$_GET["messageID"];
			return $messageID;			
		}
		
		function getTravelerSignature($traveler,$dest = NULL){
			//if( is_subclass_of($traveler,"Traveler") && strlen(trim($traveler->getTravelerProfile()->getSignature())) && !$traveler->isAdvisor() ){
			if( $traveler instanceof Traveler && strlen(trim($traveler->getTravelerProfile()->getSignature())) && !$traveler->isAdvisor() ){
				//if( is_subclass_of($dest,"Group") && $dest->getAdministratorID() == $traveler->getTravelerID() )
				if( $dest instanceof Group && $dest->getAdministratorID() == $traveler->getTravelerID() )
					return "";
				else
					return "\n\n\n\n\n__________________\n\n" . $traveler->getTravelerProfile()->getSignature();	
			}
			else
				return "";
		}
		
		function getRecipient(){
			$RECIPIENT = constants::TRAVELER;
			if( isset($_POST["hdnRecipientFlag"]) && (int)$_POST["hdnRecipientFlag"] == constants::TRAVELER )
				$RECIPIENT = constants::TRAVELER;
			elseif( ( isset($_POST["hdnRecipientFlag"]) && (int)$_POST["hdnRecipientFlag"] == constants::GROUP ) || ($this->getUserMode() == constants::GROUP || ( isset($_GET['groupTo']) || isset($_GET['group']) || isset($_GET['gID']))) && !isset($_GET["travTo"]) )
				$RECIPIENT = constants::GROUP;
			return 	$RECIPIENT;
		}
		
		function getRecipientID(){
			$recipientID = 0;
			if( isset($_GET['travTo']) )
				$recipientID = (int)$_GET["travTo"];
			elseif( isset($_GET['groupTo']) )
				$recipientID = (int)$_GET["groupTo"];
			elseif( isset($_GET['gID']) )
				$recipientID = (int)$_GET["gID"];
			return $recipientID;		
		}
		
		function getActionPage(){
			$actionpage = "messages.php";
			if(isset($_GET['gID']))
				$actionpage .= "?group&gID=".$_GET['gID'];
			elseif(isset($_GET['groupTo']))
				$actionpage .= "?group&groupTo=".$_GET['groupTo'];
			elseif( isset($_GET['travTo']) )
				$actionpage .= "?group&gID=".$_GET['travTo'];
			return $actionpage;	
		}
		
		function readMessageData(){
			$dest = "";
			if(isset($_POST['txtDestination']))
				$dest = preg_replace("/(^\s*)|(\s*$)/","",preg_replace("/\s*,\s*/",",",strtolower($_POST["txtDestination"])));
			elseif(isset($_POST['hdnTravTo']))
				$dest = strtolower($_POST['hdnTravTo']);
			
			return array(
				"destination"			=>	array_unique(array_filter(explode(",",$dest))),
				"subject"				=>	isset($_POST["txtSubject"]) ? trim($_POST["txtSubject"]) : "",
				"message"				=>	isset($_POST["txaMsgText"]) ? trim($_POST["txaMsgText"]) : "",
				"attributeID"			=>	isset($_POST["hdnMessageID"]) ? (int)$_POST["hdnMessageID"] : 0,
				"listingID"				=>	isset($_POST["hdnListingID"]) ? (int)$_POST["hdnListingID"] : 0,
				"isReply"				=>	isset($_POST["hdnReplyFlag"]) ? (int)$_POST["hdnReplyFlag"] : 0,
				"lockDestination"		=>	isset($_POST["hdnLockDestination"]) ? (int)$_POST["hdnLockDestination"] : 0,
				"travTo"				=>	isset($_POST["hdnTravTo"]) ? $_POST["hdnTravTo"] : "",
				'recipientSendableID'	=>	isset($_POST['hdnRecipientSendableID']) ? $_POST['hdnRecipientSendableID'] : 0,
				'sendNotification'		=>	isset($_POST['chkSendNotification']) ? true : false
			); 
		}
		
		function getSourceID(){
			return ( isset($_GET["sourceID"]) ) ? (int)$_GET["sourceID"] : 0; 
		}
		
		function getCurrPage(){
			return ( isset($_GET["page"]) ) ? (int)$_GET["page"] : 1;
		}
		
		function getRowsPerPage(){
			return ( isset($_GET["rowsperpage"]) ) ? (int)$_GET["rowsperpage"] : 10;
		}
		
		function getRedirectPage(){
			return "passport.php";
		}
		
		function getMessageIDs(){
			return (isset($_POST['chkMessageID'])) ? $_POST['chkMessageID'] : array();
		}
		
		function isDeleteAll(){
			return isset($_GET['deleteAll']);
		}
		
		function getValidEditMessage(){
			return isset($_POST["hdnMessageID"]);
		}
		
		function getValidInquiry(){
			return isset($_GET['listingID']);
		}
		
		function hasSentMessage() {
			return isset($_SESSION['messValidDest']);
		}
		
	} 
?>
