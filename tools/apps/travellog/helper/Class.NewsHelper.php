<?php
	
	require_once 'travellog/controller/Class.AbstractNewsController.php';
	
	class NewsHelper{
		
		static $instance;
		
		private function __construct() {}
		
		function getInstance() {
			if( null == self::$instance )
				self::$instance = new NewsHelper;
			return self::$instance;	
		}
		
		function getAction() {
			if( isset($_POST['btnSaveNews']) )
				$action = AbstractNewsController::SUBMIT;
			elseif( isset($_GET['nID']) && isset($_GET['gID']) )
				$action = AbstractNewsController::EDIT;
			else
				$action = AbstractNewsController::COMPOSE;
			return $action;			 
		}
		
		function readComposeData() {
			if( isset($_POST['hdnGroupID']) )
				$gID = $_POST['hdnGroupID'];
			else
				$gID = isset($_GET['gID']) ? $_GET['gID'] : 0;
				
			if( isset($_POST['hdnNewsID']) )
				$nID = $_POST['hdnNewsID'];
			else
				$nID = isset($_GET['nID']) ? $_GET['nID'] : 0;		
			
			return array(
				'newsID'		=> $nID,
				'groupID'		=> $gID,
				'title'			=> isset($_POST['txtTitle']) ? trim($_POST['txtTitle']) : '',
				'content'		=> isset($_POST['txaAlertMessage']) ? trim($_POST['txaAlertMessage']) : '',
				'expiration'	=> isset($_POST['txtExpirationDate']) ? trim($_POST['txtExpirationDate']) : '',
				'prioritize'    => isset($_POST['chkEmphasize']),
				'includeMessage'=> isset($_POST['chkIncludeMsg']),
				'sendNotification'	=> isset($_POST['chkNotifyGroupMembers']),
				'errCodes'		=> array()
			);
		}
		
	}
?>