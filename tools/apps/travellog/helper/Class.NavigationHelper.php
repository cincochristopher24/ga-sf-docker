<?php
require_once('Class.GaString.php');
require_once('Class.HelperGlobal.php');
require_once('travellog/model/Class.Config.php');
require_once('travellog/model/Class.SessionManager.php');
require_once 'travellog/model/Class.GroupPrivacyPreference.php';

/**
 * Helper Class for displaying the main navigation 
 * (tabs) on .net website
 * @todo Cleanup for better modularity and encapsulation. Should not be dependent on HelperGlobal
 **/
class NavigationHelper {
	
	/**
	 * Variable to hold the collection of navigational links
	 *
	 * @var array
	 **/
	private static $links = array();
	
	/**
	 * What link to activate
	 *
	 * @var string
	 **/
	private static $active_link = '';
	
	/**
	 * Cache output
	 *
	 * @var string
	 **/
	private static $cache = '';
	
	/**
	 * Sets the default navigational links
	 *
	 * @return void
	 **/
	public static function setDefaultNavigation() {
		self::setLink('Home',array( // Home -> Homepage
			'text'=>'Home',
			'url' => '/',
			'id' =>'nav_home'));

		self::setLink('My Passport', array( // My Passport
			'text' =>'My Passport',
			'url'=> '/passport.php',
			'id'=>'nav_passport'));

		self::setLink('Travelers', array( // Travelers
			'text'=>'Travelers',
			'url'=>'/travelers.php',
			'id'=>'nav_travelers'));

		self::setLink('Journals', array( // Journals Link
			'text'=>'Journals',
			'url'=>'/journal.php',
			'id'=>'nav_journals'));

		self::setLink('Groups / Clubs', array( // Groups Link
			'text'=>'Groups',
			'url'=>'/group.php',
			'id'=>'nav_groups',
			'alias' => 'Groups'));
			
		self::setLink('What\'s New?', array( // Groups Link
			'text'=>'What\'s New?',
			'url'=>'/feature.php',
			'id'=>'whats_new_link',
			'alias' => 'WhatsNew'));
	}
	
	
	/**
	 * Alias for setDefaultNavigation()
	 *
	 * @return void
	 **/
	public static function setMainNavigation() {
		self::setDefaultNavigation();
	}
	
	
	/**
	 * Sets the navigational links for cobranded sites.
	 * This method changes some links defined in setDefaultNavigation()
	 * and removes some others
	 *
	 * @return void
	 **/
	public static function setCoBrandNavigation() {
		$temp_links = self::$links;
		self::$links = array();
		
		self::$links['Home'] = array( // Home -> Homepage
			'text'=>'Community',
			'url' => '/',
			'id' =>'nav_home'
		);

		self::$links['My Passport'] = array( // My Passport
			'text' =>'My Passport',
			'url'=> '/passport.php',
			'id'=>'nav_passport'
		);

		self::$links['Travelers'] = array( // Travelers | Students | Volunteers | Participants
			'text'=>'Students',
			'url'=>'/travelers.php',
			'id'=>'nav_travelers',
			'alias' => 'Students'
		);		

		self::$links['Journals'] = array( // Journals Link
			'text'=>'Journals',
			'url'=>'/journal.php',
			'id'=>'nav_journals'
		);

		self::$links['Groups / Clubs'] = array( // Groups Link
			'text'=>'Subgroups',
			'url'=>'/group-pages.php?search',
			'id'=>'nav_groups',
			'alias' => 'Groups'
		);
		
		self::setLink('What\'s New?', array( // Groups Link
			'text'=>'What\'s New?',
			'url'=>'/feature.php',
			'id'=>'whats_new_link',
			'alias' => 'WhatsNew'));
		
		foreach ($temp_links as $key => $props) {
			if (is_array($props)) {
				self::$links[$key] = array_merge(self::$links[$key], $props);
			}
		}
	}
	
	
	/**
	 * Clears the links store
	 *
	 * @return void
	 **/
	public static function resetLinks()	{
		self::$links = array();
	}
	
	
	public static function setLink($name, array $properties){
		if (array_key_exists($name, self::$links)) {
			self::$links[$name] = array_merge(self::$links[$name], $properties);
		} else {
			self::$links[$name] = $properties;
		}
			
	}
	
	
	public static function getLink($name) {
		if (array_key_exists($name, self::$links)) {
			return self::$links[$name];
		} else {
			return FALSE;
		}
	}
	
	
	/**
	 * Returns all the links that are currently set for the helper
	 *
	 * @return array
	 **/
	public static function getLinks() {
		return self::$links;
	}
	
	
	/**
	 * Removes a link by it's identifier (name)
	 *
	 * @return void
	 **/
	public static function removeLink($name) {
		unset(self::$links[$name]);
		
	}
	
	
	/**
	 * Changes the links collection for Advisor Group Administrator
	 * This replaces the 'My Passport' link to point to the group
	 * advisor's group homepage when she/he is logged in.
	 *
	 * @return void
	 **/
	private static function resolveAdvisorLinks()
	{
		
		if (HelperGlobal::isAdvisor()){
			if (self::isCoBrand()) {
				$home_activelink = 'Home';
			} else {
				$home_activelink = 'Groups / Clubs';
			}
		
			// Retrieve the groupID specified in the URL (GET) paramater.
			// Some pages use 'gID' while others use 'groupID'
			if (isset($_GET['gID'])) {
				$GET_gID = $_GET['gID'];
			} elseif (isset($_GET['groupID'])) {
				$GET_gID = $_GET['groupID'];
			} else {
				$GET_gID = NULL;
			}

		}
	}
	
	public static function isCobrand() {
		return array_key_exists('CONFIG', $GLOBALS) && $GLOBALS['CONFIG']->getConfigID() > 0;
	}
	
	
	/**
	 * Outputs the links as a series of anchors inside list-elements
	 *
	 * @return string
	 **/
	public static function displayMainNavigation($active = '') {
		// If we've already generated an output, use the cache
		if (self::$cache) {
		    return self::$cache;
		}
		
		if(self::isCoBrand()) {
			// Cobranded Site!
			self::setCoBrandNavigation();
			
			// If the privacy preference of the group does not allow the display
			// of group links to the outside world, and no one is logged in,
			// hide the groups link.
			$privacypref = new GroupPrivacyPreference($GLOBALS['CONFIG']->getGroupID());
			if ( !($privacypref->getViewGroups() || SessionManager::getInstance()->get('travelerID') > 0)) {
				self::removeLink('Groups / Clubs');
			}
		}
		if (!(count(self::$links) > 0)) {
			self::setDefaultNavigation();
		}
		self::$active_link = $active;
		self::resolveAdvisorLinks();
		
		self::$cache = self::generateList(self::$links, self::$active_link);
		return self::$cache;
	}
	
	private static function generateList($links, $active_link = '') {
	    $output = '';
	    $count = count($links);
	    $i = 0;
	    foreach($links as $key => $link) {
	        $i++;
			$class = array();
			if ($active_link === $key) {
				$class[] = 'active';
			}
			if ($i == 1) {
			    $class[] = 'first';
			}
			if ($i == $count) {
			    $class[] = 'last';
			}
			if (count($class)) {
			    $class = ' class="'.implode($class, ' ').'"';
			} else {
			    $class = '';
			}
			$output .= "<li id=\"${link['id']}\"".$class."><a href=\"${link['url']}\"><span>${link['text']}</span></a></li>";
		}
		return $output;
	}
	
	public static function displayGlobalNavigation() {
		return '
		<li><a href="/faq.php"> FAQ </a></li>
		<li><a href="/feedback.php"> Feedback </a></li>
		<li><a href="/privacy-policy.php"> Privacy Policy </a></li>
		<li class="last"><a href="/terms.php"> Terms of use </a></li>';
	}
	
	/**
	 * A synonym for displayMainNavigation()
	 *
	 * @return string
	 **/
	public static function display($active = '') {
		return self::displayMainNavigation($active);
	}
}

?>