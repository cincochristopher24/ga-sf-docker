<?php
	/*
	 * Class.ActivationSupportHelper.php
	 * Created on Nov 24, 2007
	 * created by marc
	 */
	
	require_once("Class.Constants.php");
	 
	class ActivationSupportHelper{
		
		static $instance = NULL;
		
		static function getInstance(){
			if( self::$instance == NULL ) self::$instance = new ActivationSupportHelper;
			return self::$instance;
		}
		
		function readASData(){
			
			return array(
				"email"		=>	isset($_POST["txtEmail"]) ? trim($_POST["txtEmail"]) : "",
				"message"	=>	isset($_POST["txaMessage"]) ? trim($_POST["txaMessage"]) : "",
				"code"		=>	isset($_POST["securityCode"]) ? trim($_POST["securityCode"]) : "",
				"secCode"	=>	isset($_POST["encSecCode"]) ? trim($_POST["encSecCode"]) : "",
				"arrError"	=>	array()
			);
		}
		
		function getAction(){
			if( isset($_POST["btnActivate"]) )
				$action = constants::ACTIVATE_ACCOUNT;
			elseif( isset($_POST["btnHelp"]) )
				$action = constants::HELP_ACTIVATE;
			else
				$action = constants::FILL_UP;
			return $action;			
		}	
		
		function getTravelerID(){
			return isset($_GET["tID"]) ? trim($_GET["tID"]) : 0;
		}
		
		
		
	} 
		
?>
