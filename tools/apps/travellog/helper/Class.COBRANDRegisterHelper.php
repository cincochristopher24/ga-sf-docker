<?php
	/**
	 * Class.COBRANDRegisterHelper.php
	 * created on April 21, 08
	 */
	 
	require_once 'travellog/helper/Class.RegisterHelper.php';
	require_once 'Class.Crypt.php';
	
	class COBRANDRegisterHelper extends RegisterHelper{
		
		static $instance;
		
		static function getInstance(){
			if(self::$instance == NULL ) self::$instance = new COBRANDRegisterHelper;
			return self::$instance;
		}
		
		public function getInvitedEmail(){
			$myCrypt = new Crypt();
			$qString = explode('&',$myCrypt->decrypt(urldecode($_SERVER['QUERY_STRING'])));
			if( 2 == count($qString) && in_array('traveler',$qString) && preg_match("/email=/",$qString[1]) )
				return preg_replace("/email=/","",$qString[1]);
			else
				return NULL;	
		}
		
	} 
?>
