<?php
	/*
	 * Class.DestinationHelper.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	 
	 class DestinationHelper{
	 	
	 	static $instance;
		
		static function getInstance(){
			if( self::$instance == NULL ) self::$instance = new DestinationHelper;
			return self::$instance;
		}
		
	 	function getAction(){
	 		
	 		if(isset($_GET["view"]))
	 			return constants::VIEW_COUNTRY;
	 		else
	 			return constants::VIEW_ALL_COUNTRIES;
	 				
	 	}
	 	
	 	function getCountryID(){
	 		return isset($_GET["countryID"]) ? $_GET["countryID"] : 0;
	 	}
	 	
	 }
?>
