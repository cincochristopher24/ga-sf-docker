<?php
require_once("travellog/views/Class.AbstractView.php");
class GroupProfileCompView extends AbstractView{ 
	function render(){
		// do not show group profile when the user is not logged in for COBRANDs
		if (!$this->contents['is_login']) {
			$server_name = $_SERVER['SERVER_NAME'];
			if ("fast.goabroad.net" != $server_name && "goabroad.net.local" != $server_name && "dev.goabroad.net" != $server_name && "www.goabroad.net" != $server_name && "test.goabroad.net" != $server_name && "staging.goabroad.net" != $server_name) {
				return "";
			}
		}
		
		return $this->obj_template->fetch('tpl.GroupProfile.php');
	}
}
?>

