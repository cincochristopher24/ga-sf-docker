<?php
require_once('travellog/components/profile/controller/Class.AbstractProfileCompController.php');
require_once 'travellog/model/Class.Group.php';
require_once 'travellog/model/Class.Traveler.php';
require_once 'Class.Template.php';
require_once('travellog/model/Class.GroupFactory.php');

class GroupProfileCompController extends AbstractProfileCompController{
		
	function init($curr_id, $curr_page=NULL){ 
		$this->props['curr_group_id'] = $curr_id;  
		$this->props['curr_page']     = $curr_page;  
		$group_factory                = GroupFactory::instance();
		
		$arr_groups                   = $group_factory->create(array($this->props['curr_group_id']));
		$this->props['obj_group']     = $arr_groups[0];
		
		/********************************************************************************************
		 * edits by neri: 	
		 * 		added if statement to avoid error if group is a club					11-05-08
		 * 		added if statement if current page is edit privacy preference			11-10-08
		 * 		set curr_group_id to parentID if group is a subgroup					11-10-08
		 * 			- shows profile of its parent group
		 * 		added a condition in the if statement									11-11-08
		 * 			- profile of the paranet group will only be displayed 
		 * 				if group is an admin group 
		 * 		required the Traveler class												11-14-08
		 * 		get the number of messages for a group by getting the number
		 * 			of new messages of the traveler logged in
		 * 		made a new function is_adminLogged										11-18-08
		 * 		added a new variable is_adminLogged & is_advisor
		 * 		added to condition that evaluates the value of can_upload				12-15-08
		 * 			if script_name is common_controller		
		 * 		displayed change logo if in cobrand index.php							06-11-09 			
		 ********************************************************************************************/
		
		if (Group::ADMIN == $this->props['obj_group']->getDiscriminator() && 0 < $this->props['obj_group']->getParentID()) {
			$this->props['curr_group_id'] = $this->props['obj_group']->getParentID();
			$arr_groups                   = $group_factory->create(array($this->props['curr_group_id']));
			$this->props['obj_group']     = $arr_groups[0];
		}
		
		if (isset($GLOBALS['CONFIG']))
			$this->props['can_upload']    	= ($_SERVER['SCRIPT_NAME']=="/group.php" || $_SERVER['SCRIPT_NAME']=="/common_controller.php" || $_SERVER['SCRIPT_NAME']=="/index.php")?true:false;
		else
			$this->props['can_upload']      = ($_SERVER['SCRIPT_NAME']=="/group.php" || $_SERVER['SCRIPT_NAME']=="/common_controller.php")?true:false;
		
		$this->props['traveler_id']       	= $this->obj_session->get('travelerID');
		$this->props['is_login']          	= $this->obj_session->getLogin();
		$this->props['is_owner']          	= $this->is_owner();
		$this->props['is_adminLogged']		= $this->is_adminLogged();
		$this->props['is_superStaffLogged'] = $this->is_superStaffLogged();
		$this->props['is_member']           = true;  //$this->props['obj_group']->isMember();
		$this->props['show_profile_link'] 	= false;
		$this->props['show_privacy_link'] 	= false;
		
		if( ($this->props['is_owner'] || $this->props['is_superStaffLogged']) && $curr_page != AbstractProfileCompController::$EDIT_PROFILE )
			$this->props['show_profile_link'] = true;
		
		if( ($this->props['is_owner'] || $this->props['is_superStaffLogged']) && $curr_page != AbstractProfileCompController::$EDIT_PRIVACY_PREFERENCES )
			$this->props['show_privacy_link'] = true;
			
		if (Group::ADMIN == $this->props['obj_group']->getDiscriminator()) {
			$this->props['web_url']          = $this->props['obj_group']->getInstitutionInfo()->getWebURL();
		  $this->props['institution_info'] = $this->props['obj_group']->getInstitutionInfo();
		  
		  $address = $this->props['institution_info']->getAddress1();
			$address = ("" != trim($this->props['institution_info']->getAddress2())) ? $address.", ".$this->props['institution_info']->getAddress2() : $address;
			$address = ("" != trim($this->props['institution_info']->getAddress3())) ? $address.", ".$this->props['institution_info']->getAddress3() : $address;
		  
		  $this->props['address'] = $address;
		  
		  // institution info
		  $institution_info_tpl = new Template();
		  $institution_info_tpl->set("grpInstitutionInfo",$this->props['obj_group']->getInstitutionInfo());	
		  $this->props['group_inst_info']     = $institution_info_tpl->fetch('travellog/views/tpl.IncGroupInstInfo.php');
		}
		
		$this->props['group_name']         = $this->props['obj_group']->getName();
		if( is_object($this->props['obj_group']->getGroupCategory()) )
			$this->props['group_category'] = $this->props['obj_group']->getGroupCategory()->getName(); 
		else
			$this->props['group_category'] = '';
		$this->props['mod_group_name']       = ( substr($this->props['group_name'],-1) == 's' )? $this->props['group_name'] . "'": $this->props['group_name'] . "'s";
		$this->props['group_desc']           = stripslashes($this->props['obj_group']->getDescription()); 		// stripslashes added by neri: 11-04-08 					
	 	$this->props['group_descriminator']  = $this->props['obj_group']->getDiscriminator();
	 	$this->props['photo']                = $this->props['obj_group']->getGroupPhoto()->getPhotoLink(); 
		
		if( is_object($this->props['obj_group']->getGroupPhoto()->getPrimaryPhoto()) ) 	
			$this->props['photo_id']       = $this->props['obj_group']->getGroupPhoto()->getPrimaryPhoto()->getPhotoID();
		else
			$this->props['photo_id']       = 0;
		
		//$traveler = new Traveler($this->props['traveler_id']);
		//$this->props['is_advisor'] = $traveler->isAdvisor();
		$this->props['is_advisor'] = $this->props['obj_group']->getAdministratorID() == $this->props['traveler_id'];
		
		if ($this->props['is_owner'] || $this->props['is_superStaffLogged']) { 
			require_once 'travellog/model/Class.JournalEntryDraftPeer.php';
			require_once 'Class.Criteria2.php';
			require_once('travellog/model/Class.SiteContext.php');
			require_once 'travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php';
			
			$siteContext = SiteContext::getInstance();
			$siteAccessType = $siteContext->isInCobrand() ? 2 : 1;
			
			$group = gaGroupMapper::getGroup($this->props['obj_group']->getGroupID());
			
			//$messagesManager = gaMessagesManagerRepository::load($group->getAdministratorID(), $siteAccessType);
			$messagesManager = gaMessagesManagerRepository::loadGroup($group->getAdministratorID(), $siteAccessType, $group);
			$messages = $messagesManager->getMessages(array('NEWS', 'SHOUTOUT'));
			
			$newMsgsCount = 0;
			foreach ($messages as $message) {
				if (!$message->isRead()) {
					$newMsgsCount++;
				}
			}
			
			$this->props['count_messages'] = $newMsgsCount;
			
			$this->props['count_request_list'] = count($this->props['obj_group']->getRequestList());
			
			$criteria = new Criteria2();
			$criteria->add(JournalEntryDraftPeer::IS_AUTO_SAVED, 1);
			$this->props['num_entry_drafts'] = JournalEntryDraftPeer::retrieveDraftEntryCount($this->props['obj_group']->getGroupID(), 2, $criteria);
		}
	}
	
	function get_view(){
		require_once('travellog/components/profile/views/Class.GroupProfileCompView.php'); 
		$obj_view = new GroupProfileCompView;
		$obj_view->setPath('travellog/components/profile/views/'); 
		
		$obj_view->setContents( $this->props );
		
		return $obj_view; 
	}
	
	function is_owner(){
		return ( $this->props['obj_group']->getAdministratorID() == $this->props['traveler_id'] || (2 == $this->props['obj_group']->getDiscriminator() AND $this->props['obj_group']->isStaff($this->props['traveler_id']) || $this->props['obj_group']->isSuperStaff($this->props['traveler_id'])))? true: false;
	}
	
	function is_adminLogged() {
		return ( $this->props['obj_group']->getAdministratorID() == $this->obj_session->get('travelerID') || $this->props['obj_group']->isStaff($this->obj_session->get('travelerID')));
	}
	
	function is_superStaffLogged(){
		if( !$this->props['is_login'] ){
			return FALSE;
		}
		return (Group::ADMIN == $this->props['obj_group']->getDiscriminator()) ? $this->props['obj_group']->isSuperStaff($this->props['traveler_id']) : (($this->props['traveler_id'] == $this->props['obj_group']->getAdministratorID()) ? true : false);
	}
}
?>
