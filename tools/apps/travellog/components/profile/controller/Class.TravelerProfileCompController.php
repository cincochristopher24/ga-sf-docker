<?php
require_once('travellog/components/profile/controller/Class.AbstractProfileCompController.php');
class TravelerProfileCompController extends AbstractProfileCompController{
	
	/*************************************************************************
		 * edits by neri: 
		 * 		exchanged the values of hometown and country		11-10-08
		 * 		changed value of show_messages_link to true			11-11-08
		 * 		instead that the current traveler will be checked 
		 * 			if he is blocked by the traveler he is viewing,
		 * 			the other way around has been implemented		11-17-08
		 * 		modified function called to get new messages of 
		 * 			traveler										12-08-08
		 * 		added show_name_preference, show_email_preference
		 * 			and answered_mtb_questions						01-09-09
		 * 		added rotating text	and subnavigation in init()		01-12-09
		 * 		added get_preference and fill_up_rotating_text
		 * 			functions										01-12-09
		 * 		added CONFIG variable in $props						01-15-09
		 *************************************************************************/
	
	function init($curr_id, $curr_page=NULL, $include_notifications=TRUE){
		parent::__construct();
		
		if (isset($GLOBALS['CONFIG']))
			$this->props['cobrand'] 			 = true;
		else
			$this->props['cobrand'] 			 = false;
		
		$this->props['curr_traveler_id']         = $curr_id;  
		$this->props['curr_page']                = $curr_page;
		$this->props['traveler_id']              = $this->obj_session->get('travelerID');
		$this->props['is_login']                 = $this->obj_session->getLogin();
		$this->props['obj_traveler']             = $this->file_factory->getClass('Traveler', array($this->props['curr_traveler_id']));
		$this->props['obj_viewer_traveler']      = $this->file_factory->getClass('Traveler', array());
		$this->props['obj_viewer_traveler']->setTravelerID($this->props['traveler_id']);
		
		$this->props['is_owner']                 = ( $this->props['curr_traveler_id'] == $this->props['traveler_id'] )? true: false;
		
		$this->props['obj_profile']              = $this->props['obj_traveler']->getTravelerProfile();
		$this->props['username']                 = $this->props['obj_traveler']->getUsername();
		$this->props['email']                    = $this->props['obj_traveler']->getTravelerProfile()->getEmail(); 
		$this->props['imageEmail']               = $this->props['obj_traveler']->getImageEmail(); 
		$this->props['mod_username']             = ( substr($this->props['username'],-1) == 's' )? $this->props['username'] . "'": $this->props['username'] . "'s";
		$this->props['count_profile_photos']     = count($this->props['obj_profile']->getCountPhotos());
		$this->props['show_email_to_public']     = $this->props['obj_traveler']->getPrivacyPreference()->canViewEmail( $this->props['traveler_id'] );
		$this->props['show_name_to_public']      = $this->props['obj_traveler']->getPrivacyPreference()->canViewNames( $this->props['traveler_id'] ); 			// added by neri: 11-18-08
		$this->props['age']                      = $this->props['obj_profile']->getAge();
		//$this->props['traveler_type']            = $this->props['obj_profile']->getTravType()->gettravelertype();
		$this->props['photo']	                 = $this->props['obj_profile']->getPrimaryPhoto()->getPhotoLink();
		$this->props['photo_id']	             = $this->props['obj_profile']->getPrimaryPhoto()->getPhotoID();
		$this->props['real_name']	             = $this->props['obj_profile']->getFirstname() . ' ' . $this->props['obj_profile']->getLastname();
		$this->props['obj_travel_bio']           = $this->file_factory->getClass('RandomTravelBioView');
		
		$this->props['country']                  = NULL;
		$this->props['hometown']                 = NULL;
		$this->props['curr_country']             = NULL;
		$this->props['curr_hometown']            = NULL;
		
		$this->props['show_name_preference'] 	 = $this->get_preference($this->props['obj_traveler']->getPrivacyPreference()->showNamesTo());
		$this->props['show_email_preference'] 	 = $this->get_preference($this->props['obj_traveler']->getPrivacyPreference()->showEmailTo());
		$this->props['answered_mtb_questions']   = ProfileQuestion::getNumberofQuestionsWithAnswer($this->props['curr_traveler_id']);
		
		$rotating_text_array  					 = $this->fill_up_rotating_text();
		$generated_random 						 = array_rand($rotating_text_array);
		$this->props['rotating_text']			 = $rotating_text_array[$generated_random];
		$this->props['rotating_text_link']		 = $this->get_rotating_text_link($generated_random);
		
		if( $this->props['obj_profile']->getHTLocationID() ){
			$location = $this->props['obj_profile']->getCity();
			$country = (!is_null($location)) ? $location->getCountry() : null;
			if(!is_null($location) && !is_null($country)){	
				$this->props['hometown']	             = $location->getName();
				$this->props['country']     	    	 = $country->getName();	
			}	
		}
		
		if( $this->props['obj_profile']->getCurrLocationID() ){
			$location = $this->props['obj_profile']->getCurrLocation();
			$country = (!is_null($location)) ? $location->getCountry() : null;
			if(!is_null($location) && !is_null($country)){	
				$this->props['curr_hometown']         = $location->getName();
				$this->props['curr_country']  	     = $country->getName(); 	
			}	
		}
		
		$this->props['show_friend_request_link'] = false; 
		$this->props['show_profile_link']        = false;
		$this->props['show_privacy_link']        = false;
		$this->props['show_account_settings_link'] = false;
		$this->props['show_messages_link']       = false;
		$this->props['is_friend']                = false;
		$this->props['is_block']                 = false;
		$this->props['has_pending_request']      = false;
		$this->props['has_friend_request']       = false;
		
		if( $this->props['is_owner'] && $curr_page != AbstractProfileCompController::$EDIT_PROFILE )
			$this->props['show_profile_link']    = true;
			
		if( $this->props['is_owner'] && $curr_page != AbstractProfileCompController::$EDIT_PRIVACY_PREFERENCES )
			$this->props['show_privacy_link']    = true;	
		
		if( $this->props['is_owner'] && $curr_page != AbstractProfileCompController::$EDIT_ACCOUNT_SETTINGS )
			$this->props['show_account_settings_link']    = true;
		
		if( !$this->props['is_owner'] && $this->props['traveler_id'] && $curr_page != AbstractProfileCompController::$COMPOSE_MESSAGES )
			$this->props['show_messages_link']   = true;			
						
		if( $this->props['traveler_id'] && $curr_page != AbstractProfileCompController::$FRIENDS_PAGE ){
			
			$this->props['is_friend']            = $this->props['obj_traveler']->isFriend         ( $this->props['traveler_id'] );
			
			$this->props['is_block']             = $this->props['obj_viewer_traveler']->isBlocked        ( $this->props['curr_traveler_id'] );			
			$this->props['has_pending_request']  = $this->props['obj_viewer_traveler']->hasPendingRequest( $this->props['curr_traveler_id'] );
			$this->props['has_friend_request']   = $this->props['obj_viewer_traveler']->hasFriendRequest ( $this->props['curr_traveler_id'] );
						
			//if( $this->props['is_login'] && !$this->props['has_pending_request'] && !$this->props['has_friend_request'] && !$this->props['is_owner'] && !$this->props['is_friend'] && !$this->props['is_block'] && !$this->props['obj_viewer_traveler']->isAdvisor()){
			if( $this->props['is_login'] && !$this->props['has_pending_request'] && !$this->props['has_friend_request'] && !$this->props['is_owner'] && !$this->props['is_friend'] && !$this->props['is_block'] ){
				$this->props['show_friend_request_link'] = true;  
			}
		} 
		
		if ($this->props['is_owner']) {
			require_once 'travellog/model/travelbio/Class.ProfileQuestion.php';
			require_once 'travellog/model/Class.JournalEntryDraftPeer.php';
			require_once 'Class.Criteria2.php';
			
			$this->props['include_notifications'] = $include_notifications;
			if( $include_notifications ){
				require_once 'travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php';
				require_once('travellog/model/Class.SiteContext.php');
	
				$siteContext = SiteContext::getInstance();
				$siteAccessType = $siteContext->isInCobrand() ? 2 : 1;
			
				$manager = gaMessagesManagerRepository::load($this->props['traveler_id'], $siteAccessType);

				$numNewMessages = $manager->countUnreadMessages();	
				$this->props['num_new_messages'] = $numNewMessages > 0 ? $numNewMessages : null;
		
				$countFriendReqs = count($this->props['obj_traveler']->getFriendRequests());
				$this->props['num_friend_requests'] 		= (0 < $countFriendReqs) ? $countFriendReqs : NULL;
		
				$countGroupInvites = $this->props['obj_traveler']->getNumberOfGroupInvites();
				$this->props['num_group_invites']			= (0 < $countGroupInvites) ? $countGroupInvites : NULL;
		
				$countGroupReqs = $this->props['obj_traveler']->getNumberOfGroupRequests();
				$this->props['num_group_requests']			= (0 < $countGroupReqs) ? $countGroupReqs : NULL;
		
				$countDeniedGroupReqs = $this->props['obj_traveler']->getNumberOfDeniedGroupRequests();
				$this->props['num_denied_group_requests']	= (0 < $countDeniedGroupReqs) ? $countDeniedGroupReqs : NULL;

				$criteria = new Criteria2();
				$criteria->add(JournalEntryDraftPeer::IS_AUTO_SAVED, 1);
				$entry_drafts_count = JournalEntryDraftPeer::retrieveDraftEntryCount($this->props['obj_traveler']->getTravelerID(), 1, $criteria);
				$this->props['num_entry_drafts'] = (0 < $entry_drafts_count) ? $entry_drafts_count : null;
			}
		}
		
		$this->props["isAdminStaffViewer"] = FALSE;
		if( 0 < $this->props['traveler_id'] && !$this->props['is_owner'] ){
			if( $this->props["cobrand"] ){
				$cobrandGroup = new AdminGroup($GLOBALS['CONFIG']->getGroupID());
				if( $cobrandGroup->getAdministratorID() == $this->props['traveler_id']
					|| $cobrandGroup->isStaff($this->props['traveler_id']) ){
					$this->props["isAdminStaffViewer"] = TRUE;
				}else{
					$groupsOfTraveler = $this->props['obj_traveler']->getGroups();
					foreach($groupsOfTraveler AS $group){
						if( $group->isStaff($this->props['traveler_id']) ){
							$this->props["isAdminStaffViewer"] = TRUE;
							break;
						}
					}
				}
			}else{
				$groupsOfTraveler = $this->props['obj_traveler']->getGroups();
				foreach($groupsOfTraveler AS $group){
					if( $group->getAdministratorID() == $this->props['traveler_id']
						|| $group->isStaff($this->props['traveler_id']) ){
						$this->props["isAdminStaffViewer"] = TRUE;
						break;
					}
				}
			}
		}
		
		$this->props['obj_travel_bio']->setOwnerId($this->props['curr_traveler_id']);
		$this->props['obj_travel_bio']->readTravelBio();
		
		$this->props['fb_like_control'] = $this->retrieveFBLikeControl($this->props);
	}
	
	function get_view(){
		require_once('travellog/components/profile/views/Class.TravelerProfileCompView.php'); 
		$obj_view = new TravelerProfileCompView;
		$obj_view->setPath('travellog/components/profile/views/');
		$obj_view->setContents( $this->props );
		
		return $obj_view; 
	}
	
	// displays the traveler's preference in displaying his email and name 
	
	function get_preference($preference) {
		switch ($preference) {
			case 0: return 'Hidden from Public';
			case 1: return 'Shown to Public';
			case 2: return 'Shown Only to Friends';
			case 3: return 'Shown Only to Groupmates';
			case 4: return 'Shown Only to Groupmates and Friends';
		}
	}
	
	// defines the text that will be displayed in the informative text for the profile component
	
	function fill_up_rotating_text() {
		$rotatingText = array();
		
		$rotatingText[] = 'GoAbroad Network gives you unlimited space for your travel journals and photos!';
		$rotatingText[] = 'On GoAbroad Network, you can do travel blogs or photo journals - or both.';
		$rotatingText[] = 'You can create an e-postcard out of your journal entry!';
		$rotatingText[] = 'You can add your videos to your travel journals!';
		$rotatingText[] = 'You can bring your travel journals, travel map, travel plans and your traveler bio to your Facebook page!';
		
		return $rotatingText;
	}
	
	// gets the link used when user clicks Find out More 
	
	function get_rotating_text_link($random) {
		$link = '';
		switch ($random) {
			case 0:
			case 1:
				$link = '/faq.php#travel';
				break;
			case 2:
				$link = '/faq.php#postcard_rss';
				break;
			case 3:
				$link = '/faq.php#video';
				break;
			case 4:
				$link = '/faq.php#facebook';
				break;
		}
		return $link;
	}
	
	private function retrieveFBLikeControl($props){
		require_once('Class.StringFormattingHelper.php');
		require_once('Class.Template.php');
		$possessive_name = StringFormattingHelper::possessive($props['obj_traveler']->getUsername());
		$siteContext = SiteContext::getInstance();
		$site_name = isset($GLOBALS["CONFIG"]) ? $siteContext->getSiteName()." Network" : "GoAbroad Network";
		$fbMetaTags = array(	'<meta property="og:title" content="'.$possessive_name.' profile"/>',
								'<meta property="og:site_name" content="'.$site_name.'"/>');
		Template::clearFBRequiredMetaTags();
		Template::setFBRequiredMetaTags($fbMetaTags);
		
		require_once("travellog/views/Class.FBLikeControlView.php");
		$obj_view = new FBLikeControlView();

		$permalink 	= "http://".$_SERVER['HTTP_HOST'].$this->props["obj_traveler"]->getFriendlyURL();
		$title 		= $this->props["obj_traveler"]->getFriendlyURL();

		$arr_contents = array(
			'permalink' => $permalink,
			'title'		=> $title,
			'left'		=> TRUE,
			'name'		=> 'profile'
		);

		$obj_view->setContents($arr_contents);
		return $obj_view;
	}
}
?>
