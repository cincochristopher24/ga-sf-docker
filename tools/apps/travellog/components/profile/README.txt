# Author: Aldwin S. Sabornido
# Created On: October 21, 2008
# Version: 1.0

===================== HOW TO USE THIS COMPONENT =====================

1) Include this library in the controller.

$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
$this->file_factory->registerClass('GroupFactory');

OR

require_once('travellog/components/profile/controller/Class.ProfileCompFactory.php');
require_once('travellog/model/Class.GroupFactory.php');
require_once('travellog/views/Class.RandomTravelBioView.php');


2) Instantiate component factory and pass what context do you want to view

$factory = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());

FOR TRAVELER CONTEXT:
	$profile_comp = $factory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
	$profile_comp->init($travelerID);
	$obj_view     = $profile_comp->get_view();

FOR GROUP CONTEXT:
	$profile_comp = $factory->create( ProfileCompFactory::$GROUP_CONTEXT );
	$profile_comp->init($groupID);
	$obj_view     = $profile_comp->get_view();


3) And lastly, pass the object view to your template and call render function.

<?php echo $obj_view->render();?>


NOTE:

	Some of the variables there are user defined. You can change it to whatever you like as long
as it will work during the deployment.

	If you have any suggestions, comments or violent reactions, please feel free to email me at aldwin.sabornido@goabroad.com

