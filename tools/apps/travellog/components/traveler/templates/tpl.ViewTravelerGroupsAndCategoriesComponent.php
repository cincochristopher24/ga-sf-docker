<div id="content_to_hide">
	<div class="content" id="subGroupsContent">
		<? if (0 < $categories->size()): ?>
			<?php $categories->rewind() ?>
			<ul id="category_list" class="groups">
				<? while($categories->hasNext()): ?>
					<?php 
					  $data['num_per_page'] = 6;
						$data['category'] = $categories->next();
						$data['groups_count'] = $data['category']->getSubGroupsCountForTraveler($traveler); 
						$data['groups'] = $data['category']->getSubGroupsForTraveler($traveler, null, null, " categorysubgrouprank", new RowsLimit($data['num_per_page'], 0)); 
						$data['parent_group'] = $parent_group;
						
						$query_string = "categoryID=".$data['category']->getCategoryID()."&amp;travelerID=".$traveler_id."&amp;action_type=ajax_show_groups";
						$query_string = ($is_searching) ? $query_string."&amp;keyword=".urlencode($keyword) : $query_string;
						$query_string .= "', '".$data['category']->getCategoryID();
						
						$data['paging'] = new Paging($data['groups_count'], 1, $query_string, $data['num_per_page'], true);
						$data['paging']->setOnclick("my_paging");
						$data['page'] = 1;
						
						$tpl = new Template();
						$tpl->setVars($data);									
					?>
					<?php $has_groups_shown = true; ?>
					<li class="category_box" id="li_category_id_<?= $data['category']->getCategoryID() ?>">
						<div class="category_header">
							<h3 id="cat_head<?php echo $data['category']->getCategoryID()?>" > 
									<?php echo $data['category']->getName(); ?>
							</h3>
							<? if (0 < $data['groups_count'] ): ?>
								<a class="clipper collapse" href="javascript:void(0)" title="Collapse/Clip this category" onclick="SubGroupManager.slide(<?= $data['category']->getCategoryID() ?>, this)">&raquo;</a>	
								<input type="hidden" id="slider<?= $data['category']->getCategoryID() ?>_slide" value="0" />		
							<? endif; ?>
												
						</div>
						<div id="loader<?php echo $data['category']->getCategoryID(); ?>" class="widePanel content" style="display: none;">
							<p class="loading_message">
								<span id="imgLoading">
									<img alt="Loading" src="/images/loading.gif"/>
								</span>
								<em id="statusCaption">Loading data please wait...</em>
							</p>
						</div>
						
						<div id="slider<?=$data['category']->getCategoryID()?>" >				
							<?php echo preg_replace('/\s+/', ' ', $tpl->fetch('travellog/views/subgroups/tpl.ViewAjaxSearch.php')); ?>
						</div>
					</li>
				<? endwhile; ?>
			</ul>
		<?php elseif ($is_searching): ?>
		  <p class="help_text"> <span>No matching results found for search of category name '<?php echo $keyword; ?>'<span>.</p>
		<?php else: ?>
		  <p class="help_text"> <span>There are no categories</span>.</p>
		<?php endif; ?>
	</div>
</div>

<script type="text/javascript">
  function my_paging(url, id){
    SubGroupManager.paging(id, "group.php?"+url);	
  }
</script>