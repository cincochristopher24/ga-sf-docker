<?php	

require_once("travellog/components/Class.GanetComponent.php");

/**
 * 
 */
class TravelerGroupsAndCategoriesComponent extends GanetComponent 
{ 
  /**
   * @var  Traveler  The traveler
   */
  private $mTraveler = null;
  
  /**
   * @var  Group  The current cobrand group
   */
  private $mGroup = null;
	
	public function __construct(Traveler $traveler, Group $group)
	{
    $this->mTraveler = $traveler;
    $this->mGroup = $group;
	}
	
  public function setUp()
  {
    if (false == $this->get('categories', false))
    {
      $this->set('categories', $this->mTraveler->getSubGroupCategories($this->get('keyword', '')));
    } 
  }
  
  public function fetchView()
  {
    $tpl = new Template(); 
    $tpl->set('categories', $this->get('categories'));
    $tpl->set('keyword', $this->get('keyword', ''));
    $tpl->set('traveler', $this->mTraveler);
    $tpl->set('traveler_id', $this->mTraveler->getTravelerID());
    $tpl->set('parent_group', $this->mGroup);
    $tpl->set('is_searching', $this->get('is_searching', false));
    return $tpl->fetch(realpath(dirname(__FILE__).'/templates').'/tpl.ViewTravelerGroupsAndCategoriesComponent.php');
  }  
}
	
