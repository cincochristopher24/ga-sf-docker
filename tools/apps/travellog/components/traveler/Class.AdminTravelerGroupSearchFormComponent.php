<?php	

require_once("travellog/components/Class.GanetComponent.php");
require_once("Class.Template.php");

/**
 * Component for traveler admin traveler group search form.
 */
class AdminTravelerGroupSearchFormComponent extends GanetComponent
{
  /**
   * @var  Traveler  The traveler.
   */
  private $mTraveler = null;
  
	public function __construct(Traveler $traveler)
	{
		$this->mTraveler = $traveler;
	}
	
  public function setUp()
  {
    
  }
  
  public function fetchView()
  {
    $tpl = new Template(); 
    $tpl->set('travelerID', $this->mTraveler->getTravelerID());
    $tpl->set('keyword', $this->get('keyword', ''));
    $tpl->set('selected_search_by', $this->get('selected_search_by', TravelerGroupsAction::CATEGORY_SEARCH));
    return $tpl->fetch(realpath(dirname(__FILE__).'/templates').'/tpl.ViewAdminGroupSearchFormComponent.php');
  }
}
	
