<?php	

require_once("travellog/components/Class.GanetComponent.php");

/**
 * 
 */
class TravelerGroupsComponent extends GanetComponent 
{
  /**
   * @var  Traveler  The traveler
   */
  private $mTraveler = null;
	
	public function __construct(Traveler $traveler)
	{
    $this->mTraveler = $traveler;
	}
	
  public function setUp()
  {
  	if (false == $this->get('groups', false))
  	{
      $this->set('groups', $this->mTraveler->getGroups(null, new RowsLimit(6, (($this->get('page', 1)-1)*6)), $this->get('keyword', '')));  
  	}
  	
  	if (false == $this->get('groups_count', false))
  	{
      $this->set('groups_count', $this->mTraveler->getGroupsCount($this->get('keyword', '')));  
  	}

    $query_string = "travelerID=".$this->mTraveler->getTravelerID()."&amp;search_by=".TravelerGroupsAction::GROUP_SEARCH;
    $query_string = ($this->get('is_searching', false) AND (0 < strlen($this->get('keyword', '')))) ? $query_string."&amp;keyword=".$this->get('keyword', '') : $query_string;
			
    $this->set('paging', new Paging($this->get('groups_count'), $this->get('page', 1), $query_string, 6)); 
  }
  
  public function fetchView()
  {
    $tpl = new Template(); 
    $tpl->set('groups_count', $this->get('groups_count'));
    $tpl->set('groups', $this->get('groups'));
    $tpl->set('traveler', $this->mTraveler);
    $tpl->set('paging', $this->get('paging'));
    $tpl->set('is_searching', $this->get('is_searching', false));
    $tpl->set('page', $this->get('page', 1));
    return $tpl->fetch(realpath(dirname(__FILE__).'/templates').'/tpl.ViewTravelerGroupsComponent.php');
  } 
}
	
