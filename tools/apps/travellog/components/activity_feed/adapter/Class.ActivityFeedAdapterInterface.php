<?php

interface ActivityFeedAdapterInterface {
	
	function getID();
	
	function getDoerPhotoThumbnail();
	function getDoerFullName();
	function getDoerUserName();
	function getDoerLink();
	function getItemTitle();
	function getItemContent();
	function getItemDate();
	function getTitleClass();
	function itemExists();
	function render();
}