<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");
	require_once('travellog/model/Class.PhotoAlbum.php');

	class PhotoFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mPhotoAlbum = NULL;
		protected $mPhotos = NULL;
		protected $mUploadCount = 0;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			
			try{
				$this->mPhotoAlbum = new PhotoAlbum($this->mFeed->getSectionRootID());
				if( !($this->mPhotoAlbum instanceof PhotoAlbum) ){
					$this->mPhotoAlbum = NULL;
				}
			}catch(exception $e){
				$this->mPhotoAlbum = NULL;
			}
			
			if( !is_null($this->mPhotoAlbum) ){
				$rowsLimit = new RowsLimit(10,0);
				$photosArray = $this->mPhotoAlbum->getPhotosUploadedByDay($rowsLimit,$this->mFeed->getFeedDate());
				$this->mPhotos = $photosArray["photos"];
				$this->mUploadCount  = $photosArray["recordCount"];
			}
			if( !$this->mUploadCount ){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			
			if( is_null($this->mPhotoAlbum) ){
				return "";
			}else{
				if( !$this->mUploadCount ){
					$text = " uploaded Photos to PHOTO ALBUM";
				}else{
					$text = " uploaded ";
					$text .= 1 < $this->mUploadCount ? $this->mUploadCount." Photos to PHOTO ALBUM" : $this->mUploadCount." Photo to PHOTO ALBUM";
				}
				
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text",$text);
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mPhotoAlbum->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",$this->mPhotoAlbum->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}
		}
		
		function getItemContent(){
			if( is_null($this->mPhotoAlbum) ){
				return "";
			}else{
				$itemContentTpl = new Template();
				$itemContentTpl->set("hasPhotos",0<$this->mUploadCount);
				$itemContentTpl->set("photos",$this->mPhotos);
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentPhotoComponent.php");
			}
		}
		
		function getTitleClass(){
			return "feedPhoto";
		}
	}