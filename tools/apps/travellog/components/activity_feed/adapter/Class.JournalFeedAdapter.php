<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");

	class JournalFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mJournal = NULL;
		protected $mFirstEntry = NULL;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			try{
				$this->file_factory->registerClass('Travel');
				$this->mJournal = $this->file_factory->getClass('Travel', array($this->mFeed->getSectionID()));
			
				if( is_null($this->mJournal) ){
					$this->mItemExists = FALSE;
				}else{
					$this->mFirstEntry = $this->mJournal->getFirstJournalEntry();
					if( is_null($this->mFirstEntry) ){
						$this->mItemExists = FALSE;
					}
				}
			}catch(exception $e){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			
			if( !is_null($this->mGroupOwner) && !is_null($this->mDoer) && !is_null($this->mJournal) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","created a JOURNAL");
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mJournal->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",is_null($this->mFirstEntry) ? "" : $this->mFirstEntry->getFriendlyURL());
				$feedItemTitleTpl->set("ownerGroup",$this->mGroupOwner);
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else if( !is_null($this->mDoer) && !is_null($this->mJournal) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","created a JOURNAL");
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mJournal->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink", is_null($this->mFirstEntry) ? "" : $this->mFirstEntry->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else{
				return "";
			}
		}
		
		function getItemContent(){
			if( is_null($this->mJournal) ){
				return "";
			}else{
				$journalDescription = htmlspecialchars(stripslashes($this->mJournal->getDescription()),ENT_QUOTES);
				
				$itemContentTpl = new Template();
				$itemContentTpl->set("feedID",$this->mFeed->getFeedID());
				$itemContentTpl->set("travelID",$this->mJournal->getTravelID());
				$itemContentTpl->set("journalDescription",$journalDescription);
				$itemContentTpl->set("hasEntry", !is_null($this->mFirstEntry));
				
				if( !is_null($this->mFirstEntry) ){
					$itemContentTpl->set("entryLink",$this->mFirstEntry->getFriendlyURL());
					$itemContentTpl->set("entryTitle",htmlspecialchars(stripslashes($this->mFirstEntry->getTitle()),ENT_QUOTES));
					$itemContentTpl->set("entryDescription",HtmlHelpers::TruncateText(strip_tags($this->mFirstEntry->getDescription()), 50),ENT_QUOTES);
					if( !is_null($this->mGroupOwner) ){
						$itemContentTpl->set("ownerGroup",$this->mGroupOwner);
					}
					$itemContentTpl->set("currentGroup",$this->mCurrentGroup);
				}
				$itemContentTpl->set("actionLinksVisible",$this->actionLinksVisible());
				if( $this->actionLinksVisible() ){
					$journalStatus = $this->mCurrentGroup->getGroupJournalStatus($this->mJournal->getTravelID());
					$approveLinkLabel = $journalStatus["approved"] ? "Unapprove" : "Approve";
					$approveLinkClass = $journalStatus["approved"] ? "Approved" : "Unapproved";
					$featureLinkLabel = $journalStatus["featured"] ? "Remove from Featured List" : "Feature";
					$featureLinkClass = $journalStatus["featured"] ? "Featured" : "Unfeatured";
					$itemContentTpl->set("featureLinkLabel",$featureLinkLabel);
					$itemContentTpl->set("featureLinkClass",$featureLinkClass);
					$itemContentTpl->set("approveLinkLabel",$approveLinkLabel);
					$itemContentTpl->set("approveLinkClass",$approveLinkClass);
					$itemContentTpl->set("related",$journalStatus["related"]);
					$itemContentTpl->set("onClickActions",$this->getOnClickActions());
					if($this->mViewMode == "GROUP_JOURNALS"){
						Template::includeDependentJs('/js/manageJournals.js');
					}
				}
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentJournalComponent.php");
			}
		}
		
		function getTitleClass(){
			return "feedJournal";
		}
		
		function getJournal(){
			$this->mJournal;
		}
		
		function getItem(){
			return $this->mJournal;
		}
		
	}