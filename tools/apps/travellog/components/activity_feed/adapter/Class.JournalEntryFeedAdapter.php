<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");

	class JournalEntryFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mJournalEntry = NULL;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			
			try{
				$this->file_factory->registerClass('TravelLog');
				$this->mJournalEntry = $this->file_factory->getClass('TravelLog', array($this->mFeed->getSectionID()));
				if( !($this->mJournalEntry instanceof TravelLog) ){
					$this->mJournalEntry = NULL;
				}
			}catch(exception $e){
				$this->mJournalEntry = NULL;
			}		
			if( is_null($this->mJournalEntry) ){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			
			if( !is_null($this->mGroupOwner) && !is_null($this->mDoer) && !is_null($this->mJournalEntry) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","created a JOURNAL ENTRY");
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mJournalEntry->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",$this->mJournalEntry->getFriendlyURL());
				$feedItemTitleTpl->set("ownerGroup",$this->mGroupOwner);
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else if( !is_null($this->mDoer) && !is_null($this->mJournalEntry) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","created a JOURNAL ENTRY");
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mJournalEntry->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",$this->mJournalEntry->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else{
				return "";
			}
		}
		
		function getItemContent(){
			if( is_null($this->mJournalEntry) ){
				return "";
			}else{
				$itemContentTpl = new Template();
				$itemContentTpl->set("entryDescription",HtmlHelpers::TruncateText(strip_tags($this->mJournalEntry->getDescription()), 50),ENT_QUOTES);
				
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentJournalEntryComponent.php");
			}
		}
		
		function getTitleClass(){
			return "feedJournal";
		}
		
		function getJournalEntry(){
			return $this->mJournalEntry;
		}
		
		function getItem(){
			return $this->mJournalEntry;
		}
		
	}