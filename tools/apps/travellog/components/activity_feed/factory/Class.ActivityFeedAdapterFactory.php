<?php

	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.DiscussionFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.DiscussionPostFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.PhotoAlbumFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.VideoAlbumFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.JournalFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.JournalEntryFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.JoinGroupRequestFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.NewMemberFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.PhotoFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.VideoFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.EntryPhotoFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.EntryVideoFeedAdapter.php");
	require_once("travellog/components/activity_feed/adapter/Class.UnknownFeedAdapter.php");

	class ActivityFeedAdapterFactory{
		
		public static function create(ActivityFeed $feed){
			switch($feed->getFeedSection()){
				case ActivityFeed::DISCUSSION 			: 	return new DiscussionFeedAdapter($feed); break;
				case ActivityFeed::DISCUSSIONPOST 		: 	return new DiscussionPostFeedAdapter($feed); break;
				case ActivityFeed::PHOTOALBUM 			:	return new PhotoAlbumFeedAdapter($feed); break;
				case ActivityFeed::VIDEOALBUM 			:	return new VideoAlbumFeedAdapter($feed); break;
				case ActivityFeed::JOURNAL 				:	return new JournalFeedAdapter($feed); break;
				case ActivityFeed::JOURNALENTRY 		:	return new JournalEntryFeedAdapter($feed); break;
				case ActivityFeed::JOINGROUPREQUEST 	:	return new JoinGroupRequestFeedAdapter($feed); break;
				case ActivityFeed::NEWMEMBER 			:	return new NewMemberFeedAdapter($feed); break;
				case ActivityFeed::PHOTO 				:	return new PhotoFeedAdapter($feed); break;
				case ActivityFeed::VIDEO 				:	return new VideoFeedAdapter($feed); break;
				case ActivityFeed::ENTRYPHOTO 			:	return new EntryPhotoFeedAdapter($feed); break;
				case ActivityFeed::ENTRYVIDEO 			:	return new EntryVideoFeedAdapter($feed); break;
				default									:	return new UnknownFeedAdapter($feed); break;
			}
		}
		
	}