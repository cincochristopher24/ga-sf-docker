<?php if( isset($doer) ): ?>
	<?php if( isset($doerviewer) && $doerviewer ): ?>
		<strong><i>You</i></strong>
	<?php else: ?>
		<a href="<?=$doer->getFriendlyURL()?>"><strong><?=htmlspecialchars(stripslashes($doer->getUserName()),ENT_QUOTES)?></strong></a> 
		<strong><i><?=htmlspecialchars(stripslashes($doer->getFullName()),ENT_QUOTES)?></i></strong> 
	<?php endif; ?>
<?php endif; ?>

<?=$text?> 

<a href="<?=$subjectLink?>" <?php if(""==trim($subjectLink)):?>onclick="return false;"<?php endif;?> ><?=$subjectTitle?></a>

<?php if( isset($ownerGroup) ): ?>
	for <a href="<?=$ownerGroup->getFriendlyURL()?>"><?=htmlspecialchars(stripslashes($ownerGroup->getName()),ENT_QUOTES)?></a>
<?php endif; ?>

<?php if( isset($discussionGroup) ): ?>
	in <a href="<?=$discussionGroup->getFriendlyURL()?>"><?=htmlspecialchars(stripslashes($discussionGroup->getName()),ENT_QUOTES)?></a>
<?php endif; ?>