<li class="container ajax_journal" id="feed_<?=$feed->getID()?>" <?if(!$feed->itemExists()):?>style="display:none;"<?endif;?> >
	<?php if($feed->itemExists()): ?>
		<a href="<?=$feed->getDoerLink()?>">
			<img class="jLeft" src="<?=$feed->getDoerPhotoThumbnail()?>" border="" align="left" alt="" width="65" height="65"/>
		</a>
		<span class="box">
			<p class="highlights <?=$feed->getTitleClass()?>">
				<?=$feed->getItemTitle()?>
			</p>
			<div class="discuss jLeft">
				<?=$feed->getItemContent()?>
			</div>
			<p class="jRight eventime"><?=$feed->getItemDate()?></p>
		</span>
	<?php endif; ?>
</li>