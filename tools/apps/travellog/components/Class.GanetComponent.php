<?php	

	/**
	* @author acgvelarde
	**/
	
	abstract class GanetComponent {
		
		protected $vars = array();
		protected $templateName;
		protected $dto=null;
		protected $loggedTraveler = null;
		protected $failedSetUp = false;
		
		/**
		* Set a component variable.
		*
		* @param String $key
		* @param Mixed $value
		**/
		public function set($key, $value){
			$this->vars[$key] = $value;
		}
		
		/**
		* Get the value of a component variable. 
		* If variable does not exist, $default will be returned
		*
		* @param String $key
		* @param Mixed $default
		* @return Mixed
		**/
		public function get($key, $default=null){
			return $this->hasVar($key) ? $this->vars[$key] : $default;
		}
		
		/**
		* Checks if $key exists in the list of component variables.
		*
		* @return Boolean
		**/
		public function hasVar($key){
			return array_key_exists($key, $this->vars);
		}
		
		/**
		* Set the currently logged traveler using this component
		**/
		final public function setLoggedTraveler(GanetTraveler $traveler){
			$this->loggedTraveler = $traveler;
		}
		
		/**
		* Get the currently logged traveler using this component
		**/
		final public function getLoggedTraveler(){
			return $this->loggedTraveler;
		}
		
		/**
		* Do logical preparations here prior to rendering.
		**/
		abstract public function setUp();
		
		/**
		* Fetch the component's template.
		*
		* @return String
		**/
		abstract public function fetchView();
		
		/**
		* Render the component
		**/
		final public function render(){
			$this->setUp();
			echo $this->fetchView();
		}
	}
	
