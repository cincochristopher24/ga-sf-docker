<?php
/**
* <b>TravelTips</b> class with integrated CRUD methods.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2007-2008
*/

require_once("Class.Connection.php");
require_once("Class.Recordset.php");
class TravelTips{
	
	private 
	
	$mConn = NULL,
	
	$mRs   = NULL;
	
	function __construct(){
		$this->mConn = new Connection;
		$this->mRs   = new Recordset($this->mConn);	
	}
		
	function Get( TravelTipsVO &$obj_travel_tips ){
		$sql = sprintf(
						"SELECT travellogID, description FROM tblTravelTips WHERE travelTipID = %d"
						,$obj_travel_tips->getTravelTipID()
					  );
		$this->mRs->Execute($sql);
		if( $this->mRs->Recordcount() ){
			$obj_travel_tips->setTravelLogID( $this->mRs->Result(0, "travellogID") );
			$obj_travel_tips->setDescription( $this->mRs->Result(0, "description") );
		} 
	}
	
	function GetList( Criteria2 $c ){
		$arr = array();
		$sql = sprintf(
 					    "SELECT travelTipID, travellogID, description FROM tblTravelTips WHERE 1 = 1 %s"
 					    ,$c->getCriteria2('AND')
					  );
		$this->mRs->Execute($sql);
		
		if( $this->mRs->Recordcount() ){
			require_once("travellog/vo/Class.TravelTipsVO.php");
			while( $row = mysql_fetch_assoc( $this->mRs->Resultset() ) ){
				$objNewMember = new	TravelTipsVO;
				$objNewMember->setTravelLogID( $row["travellogID"] );	
				$objNewMember->setDescription( $row["description"] );
				$objNewMember->setTravelTipID( $row["travelTipID"] );
				$arr[] = $objNewMember; 
			}
		}
		return $arr;
	}
	
	function Save( TravelTipsVO &$obj_travel_tips ){
		$sql = sprintf(
 					    "INSERT INTO tblTravelTips (travellogID, description) VALUES (%d, '%s')"
 					    ,$obj_travel_tips->getTravelLogID()
 					    ,addslashes($obj_travel_tips->getDescription())
					  );
		$this->mRs->Execute($sql);
	}
	
	function Update( TravelTipsVO &$obj_travel_tips ){
		$sql = sprintf(
 					    "UPDATE tblTravelTips SET description = '%s' WHERE travelTipID = %d"
 					    ,addslashes($obj_travel_tips->getDescription()) 
 					    ,$obj_travel_tips->getTravelTipID() 
 					    
					  );
		$this->mRs->Execute($sql);
	}
	
	function Delete( Criteria2 $c ){
		$sql = sprintf(
 					    "DELETE FROM tblTravelTips WHERE %s"
 					    ,$c->getCriteria2()
					  );
		$this->mRs->Execute($sql);
	}
}
?>

