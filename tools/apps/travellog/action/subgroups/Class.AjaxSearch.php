<?php
	/**
	 * @(#) Class.AjaxSearch.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 24, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.CustomizedSubGroupCategory.php');
	
	/**
	 * Action class for searching for a subgroup using ajax.
	 */
	class AjaxSearch extends ControllerAction {
	
		function execute(){
			if ($this->validate()) {
				$file_factory = FileFactory::getInstance();
				
				$this->mData['num_per_page'] = 6;
				$this->mData['groups_count'] = $this->mData['category']->getSubGroupsCount(); 
				$this->mData['groups'] = $this->mData['category']->getSubGroups(null, null, " categorysubgrouprank", new RowsLimit($this->mData['num_per_page'], ($this->mData['num_per_page']*($this->mData['page']-1)))); 
				
				$this->mData['paging'] = new Paging($this->mData['groups_count'], $this->mData['page'], $this->mData['category']->getCategoryID()."','/subgroups/ajax_search/".$this->mData['category']->getCategoryID().'/'.$this->mData['keyword'], $this->mData['num_per_page'], true, true);
				$this->mData['paging']->setOnclick("SubGroupManager.paging");
				
				$this->mData['parent_group'] = $this->mData['category']->getParentGroup();
				
				$tpl = $file_factory->getClass('Template');
				$tpl->setVars($this->mData);
				echo preg_replace('/\s+/', ' ', $tpl->fetch('travellog/views/subgroups/tpl.ViewAjaxSearch.php'));
			}
			else {
				if ($this->isAjaxRequest()) {
					echo "There are no groups.";
				}
				else {
					$this->showFileNotFound();
				}
			}
		}
		
		function validate(){
			if ($this->isAjaxRequest()) {
				if (isset($this->mData['category_id'])) {
					$this->mData['category'] = new CustomizedSubGroupCategory($this->mData['category_id']);
					if (0 < $this->mData['category']->getCategoryID()) {
						$this->mData['parent_group'] = $this->mData['category']->getParentGroup();
						$privacy_preference = new GroupPrivacyPreference($this->mData['parent_group']->getGroupID());
						if ($privacy_preference->getViewGroups() || 0 < $this->mData['logger_id']) {
							return true;
						}
					}
				}
			}
			
			return false;
		}
	
	}