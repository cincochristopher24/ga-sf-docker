<?php
	/**
	 * @(#) Class.Delete.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 22, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.TopicPeer.php');
	
	/**
	 * Action class for deleting a topic.
	 */
	class Delete extends ControllerAction {
		 
		function execute(){
			if ($this->validate()) {
				if ($this->mData['topic']->delete()) {
					$session = SessionManager::getInstance();			
					$session->set('custompopup_message', "You have successfully deleted the topic <strong>".$this->mData['topic']->getTitle()."</strong>.");					
					
					$this->redirect('/discussion_board/home/'.$this->mData['group']->getGroupID());
				}
			}
			
			$this->showFileNotFound();
		}
		
		/**
		 * General Rules:
		 * 	1. The given topic ID must be a valid ID of the topic.
		 * 	2. The current user must be an administrator or staff of the related group of the topic.
		 * 	3. The number of topic's discussions must be equal to 0.
		 */
		function validate(){
			if (isset($this->mData['topic_id']) && 0 < $this->mData['logger_id']) {
				$this->mData['topic'] = TopicPeer::retrieveByPk($this->mData['topic_id']);
				
				if (!is_null($this->mData['topic']) AND 0 == $this->mData['topic']->getDiscussionsCount()) {
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
					if ($this->mData['is_admin_logged']) {
						return true;
					}
				}
			}
			
			return false;
		}
	}