<?php
	/**
	 * @(#) Class.SavePrivacySetting.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - May 29, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	require_once('travellog/model/Class.TopicPeer.php');
	
	/**
	 * Action class for saving the new privacy preference of the topic.
	 */
	class SavePrivacySetting extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				if (TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING == $this->mData['privacy_setting'] OR TopicPeer::SUBGROUP_PUBLIC_SETTING == $this->mData['privacy_setting']) {
					$criteria = new SqlCriteria2();
					$criteria->add(DiscussionPeer::ADDED_TO_KNOWLEDGE_BASE, DiscussionPeer::KNOWLEDGE_BASE_ADDED);
					if (0 < $this->mData['topic']->getDiscussionsCount($criteria)) {
						if (TopicPeer::SUBGROUP_PUBLIC_SETTING == $this->mData['topic']->getPrivacySetting()) {
							echo "({ isSuccessful: true, validate: true, message: 'The topic has discussions that currently appears on the Knowledge Base. Applying the restriction Posting for Group Only will remove it\'s discussions from the Knowledge Base.' })";
						}
						else {
							echo "({ isSuccessful: true, validate: true, message: 'The topic has discussions that currently appears on the Knowledge Base. Applying the restriction Group Members Only will remove it\'s discussions from the Knowledge Base.' })";
						}
						exit;
					}
				}
				
				$this->mData['topic']->setPrivacySetting($this->mData['privacy_setting']);
				if ($this->mData['topic']->save()) {
					echo "({ isSuccessful: true, validate: false, message: 'You have successfully updated the privacy setting of the topic.' })";
				}
				else {
					echo "({ isSuccessful: false, message: 'An error have occured while changing the privacy setting of the topic.' })";
				}
			}
			else {
				echo "({ isSuccessful: false, message: 'An error have occured while changing the privacy setting of the topic.' })";
			}
		}
		
		/**
		 * General rules:
		 * 	1. Only the admin(administrator and staff) can add posts.
		 * 	2. 
		 */
		function validate(){
			if (isset($this->mData['topic_id']) && 0 < $this->mData['logger_id']) {
				$this->mData['topic'] = TopicPeer::retrieveByPk($this->mData['topic_id']);
				
				if (!is_null($this->mData['topic'])) {
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
					if ($this->mData['is_admin_logged']) {
						return true;
					}
				}
			}
			
			return false;
		}
	}