<?php
	/**
	 * @(#) Class.Edit.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 23, 2009
	 */
	
	require_once('travellog/model/Class.TopicPeer.php');
	require_once('travellog/action/Class.ControllerAction.php');
	
	/**
	 * Action class for showing the edit form of topic.
	 */
	class Edit extends ControllerAction {
	
		function execute(){
			if ($this->validate()) {
				$file_factory = FileFactory::getInstance();
				
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
				
				$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
				$this->mData['sub_navigation']->setContext('GROUP');
				$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
				$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
			
				$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mData['group']->getGroupID());
				$this->mData['profile'] = $profile_comp->get_view();
				
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
				
				// Temporary data
				$this->mData['topic_id'] = $this->mData['topic']->getId();
				$this->mData['title'] = $this->mData['topic']->getTitle();
				$this->mData['description'] = $this->mData['topic']->getDescription();
				$this->mData['privacy_setting'] = $this->mData['topic']->getPrivacySetting();
				$this->mData['group_id'] = $this->mData['group']->getGroupId();
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/discussionboard/tpl.FrmTopic.php');
				exit;
			}
			
			$this->showFileNotFound();
		}
		
		/**
		 * General Rules:
		 * 	1. The given topic ID must be a valid ID of the topic.
		 * 	2. The current user must be an administrator or staff of the related group of the topic.
		 * 	3. The number of topic's discussions must be equal to 0.
		 */
		function validate(){
			if (isset($this->mData['topic_id']) && 0 < $this->mData['topic_id'] && 0 < $this->mData['logger_id']) {
				$this->mData['topic'] = TopicPeer::retrieveByPk($this->mData['topic_id']);
				
				if (!is_null($this->mData['topic'])) {
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
					if ($this->mData['is_admin_logged']) {
						return true;
					}
				}
			}
			
			return false;
		}	
	
	}