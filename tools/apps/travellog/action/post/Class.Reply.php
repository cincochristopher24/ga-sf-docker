<?php
	/**
	 * @(#) Class.Reply.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 18, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	require_once('Class.Template.php');
	
	/**
	 * Action class for showing the reply form.
	 */
	class Reply extends ControllerAction  {
	
		function execute(){
			if ($this->validate()) {
				$file_factory = FileFactory::getInstance();
				
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
				
				$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
				$this->mData['sub_navigation']->setContext('GROUP');
				$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
				$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
			
				$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mData['group']->getGroupID());
				$this->mData['profile'] = $profile_comp->get_view();
				
				// Default values
				$this->mData['post_id'] = 0;
				$this->mData['message'] = "";
				
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/discussionboard/tpl.FrmPost.php');
				exit;
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * General Rules:
		 * 	1. The url must be /post/edit/<discussion_id>
		 * 	2. <discussion_id> must be a valid ID of a discussion.
		 * 	3. The current user is :
		 * 		3.1 A staff or administrator of the group in which the discussion is related.
		 * 		3.2 A member of the group(Applicable for subgroup and root group).
		 * 		3.3 A member of the root group if the discussions topic is set to open for all
		 * 			members of the root group which also includes the members of the subgroup.
		 */		
		function validate(){
			if (isset($this->mData['discussion_id']) && 0 < $this->mData['discussion_id'] && 0 < $this->mData['logger_id']) {
				$this->mData['discussion'] = DiscussionPeer::retrieveByPk($this->mData['discussion_id']);
				
				if (!is_null($this->mData['discussion'])) {
					$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
					
					if (!is_null($this->mData['traveler'])) {
						$this->mData['topic'] = $this->mData['discussion']->getTopic();
						$this->mData['group'] = $this->mData['topic']->getGroup();
						$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
						$this->mData['parent_group'] = ($this->mData['is_subgroup']) ? $this->mData['group']->getParent() : $this->mData['group'];
						$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
						$this->mData['is_member_logged'] = ($this->mData['is_admin_logged'] || $this->mData['group']->isMember($this->mData['traveler']));
						$this->mData['is_root_member_logged'] = ($this->mData['is_subgroup']) ? ($this->mData['is_member_logged'] || $this->mData['parent_group']->isMember($this->mData['traveler'])) : $this->mData['is_member_logged'];

						switch ($this->mData['topic']->getPrivacySetting()) {
							case TopicPeer::PUBLIC_SETTING:
							case TopicPeer::MEMBERS_ONLY_SETTING:
								if ($this->mData['is_root_member_logged']) {
									return true;
								}
							break;						
							case TopicPeer::SUBGROUP_PUBLIC_SETTING:
							case TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING:
								if ($this->mData['is_member_logged']) {
									return true;
								}						
							break;
						}
					}
				}
			}
			
			return false;				
		}	
	
	}