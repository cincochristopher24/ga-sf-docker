<?php
	/**
	 * @(#) Class.Edit.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 19, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.PostPeer.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	require_once('Class.Template.php');
	
	/**
	 * Action class for showing the edit form of post.
	 */
	class Edit extends ControllerAction {
		
		function execute(){
			require_once('gaLogs/Class.GaLogger.php'); 
			GaLogger::create()->start($this->mData['logger_id'],'TRAVELER');   
			
			if ($this->validate()) {
				$file_factory = FileFactory::getInstance();
				
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
				
				$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
				$this->mData['sub_navigation']->setContext('GROUP');
				$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
				$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
			
				$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mData['group']->getGroupID());
				$this->mData['profile'] = $profile_comp->get_view();
				
				// Default values
				$this->mData['message'] = $this->mData['post']->getMessage();
				$this->mData['discussion_id'] = $this->mData['discussion']->getId();
				
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/discussionboard/tpl.FrmPost.php');
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * General rules:
		 * 	1. The current user must be logged.
		 * 	2. The url must be /post/edit/<post_id>.
		 * 	3. <post_id> must be a valid ID of a post.
		 * 	4. The current user must be a staff or the administrator of the group in which the 
		 * 		post is related or the current user is the poster of the post.
		 * 
		 */
		function validate(){
			if (isset($this->mData['post_id']) && 0 < $this->mData['post_id'] && 0 < $this->mData['logger_id']) {
				$this->mData['post'] = PostPeer::retrieveByPk($this->mData['post_id']);
				
				if (!is_null($this->mData['post'])) {
					$this->mData['discussion'] = $this->mData['post']->getDiscussion();
					$this->mData['topic'] = $this->mData['discussion']->getTopic();
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
					$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));							
					
					if ($this->mData['logger_id'] == $this->mData['post']->getPosterId() OR $this->mData['is_admin_logged']) {
						return true;
					}
				}
			}
			
			return false;				
		}			
		
	}