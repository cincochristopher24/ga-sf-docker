<?php	

require_once("travellog/action/Class.ControllerAction.php");
require_once("travellog/model/Class.SiteContext.php");
require_once("Class.Template.php");
require_once("travellog/model/Class.CustomizedSubGroupCategory.php");
require_once("travellog/model/Class.SubNavigation.php");
require_once("travellog/model/traveler/Class.TravelerUser.php");
require_once("travellog/model/Class.TravelerPeer.php");
require_once("travellog/model/Class.GroupPeer.php");
require_once("travellog/components/profile/controller/Class.ProfileCompFactory.php");
require_once("Class.Paging.php");
require_once("Class.StringFormattingHelper.php");

/**
 * Action class for showing the groups of the category.
 */
class TravelerViewGroupsOfSubGroupCategoryAction extends ControllerAction {
	/***  Constants  ***/
	const CATEGORY_SEARCH = 1;
	const GROUP_SEARCH = 2;
	
  /**
   * @var  Traveler  The owner of the groups viewed.
   */
  private $mTraveler = null;
  
  /**
   * @var  Group  The cobrand group if we are in cobrand context.
   */
  private $mGroup = null;
  
  /**
   * @var  TravelerUser  The current user of ganet
   */
  private $mUser = null;
  
  /**
   * @var  CustomizedSubGroupCategory  The subgroup category
   */
  private $mCategory = null;
  
  public function __construct(array $data = null) {
    parent::__construct($data);
    $this->mData['is_searching'] = (isset($_GET['keyword']));
  }
  
  public function validate() {
    $file_factory = FileFactory::getInstance();
  	$this->mTraveler = $file_factory->getClass('Traveler', array($this->mData['travelerID']));
  	$this->mCategory = new CustomizedSubGroupCategory($this->mData['categoryID']);
  	$this->mUser = TravelerUser::getInstance();
    $site_context = SiteContext::getInstance();
  	
  	if (!is_null($this->mTraveler)) {
			$this->mGroup = GroupPeer::retrieveByPK($site_context->getGroupID()); 
			if (!is_null($this->mGroup)) {
			  return true;
			} 
  	}
    
    return false;
  }
  
  public function execute() {    
		$this->mData['groups_count'] = $this->mCategory->getSubGroupsCountForTraveler($this->mTraveler); 
		$this->mData['groups'] = $this->mCategory->getSubGroupsForTraveler($this->mTraveler, null, null, " categorysubgrouprank", new RowsLimit(6, (($this->mData['page'] - 1) * 6))); 
		$this->mData['num_per_page'] = 6; 
		$this->mData['category'] = $this->mCategory;
		 
		$query_string = "categoryID=".$this->mCategory->getCategoryID()."&amp;travelerID=".$this->mTraveler->getTravelerID()."&amp;action_type=ajax_show_groups";
		$query_string = ($this->mData['is_searching']) ? $query_string."&amp;keyword=".urlencode($this->mData['keyword']) : $query_string;
		$query_string .= "', '".$this->mCategory->getCategoryID();
		
		$this->mData['paging'] = new Paging($this->mData['groups_count'], $this->mData['page'], $query_string, $this->mData['num_per_page'], true);
		$this->mData['paging']->setOnclick("my_paging");
		
		$this->mData['parent_group'] = $this->mCategory->getParentGroup();
		
		$tpl = new Template();
		$tpl->setVars($this->mData);
		echo preg_replace('/\s+/', ' ', $tpl->fetch('travellog/views/subgroups/tpl.ViewAjaxSearch.php'));   	
		exit;
  }
  
  public function onValidationFailure()
  {
    $this->redirect('/');
  }
}
	
