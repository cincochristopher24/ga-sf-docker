<?php
	/**
	 * @(#) Class.AddToKnowledgeBase.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 3, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	
	/**
	 * Action class for adding a discussion to knowledge base.
	 */
	class AddToKnowledgeBase extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				$this->mData['discussion']->setAddedToKnowledgeBase(DiscussionPeer::KNOWLEDGE_BASE_ADDED);
				if ($this->mData['discussion']->save()) {
					echo "({isSuccessful: true, 'message': '"."You have successfully added the discussion to the knowledge base.'})";
				}
				else {
					echo "({isSuccessful: false, 'message': 'An error have occured while adding the discussion to the knowledge base.'})";
				}
			}
			else {
				if ($this->isAjaxRequest()) {
					echo "({isSuccessful: false, 'message': 'An error have occured while adding the discussion to the knowledge base.'})";
				}
				else {
					$this->showFileNotFound();
				}
			}
		}
		
		/**
		 * General Rules:
		 * 	1. The action must be an ajax call.
		 * 	2. The url must be /discussion/add_to_knowledge_base/<discussion_id>
		 * 	3. <discussion_id> must be a valid ID of a discussion.
		 * 	4. The current user is a staff or administrator of the group in which the discussion is
		 * 		related.
		 * 	5. The related group must be a subgroup.
		 */	
		function validate(){
			if ($this->isAjaxRequest()) {
				if (0 < $this->mData['logger_id'] AND isset($this->mData['discussion_id'])) {
					$this->mData['discussion'] = DiscussionPeer::retrieveByPk($this->mData['discussion_id']);
					
					if (!is_null($this->mData['discussion'])) {
						$this->mData['topic'] = $this->mData['discussion']->getTopic();
						$this->mData['group'] = $this->mData['topic']->getGroup();
						if (0 < $this->mData['group']->getParentID()) {
							$this->mData['isAdmin'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
							if ($this->mData['isAdmin']) {
								return true;
							}
						}
					}				
				}
			}
			
			return false;					
		}
	}