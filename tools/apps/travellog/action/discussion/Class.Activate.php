<?php
	/**
	 * @(#) Class.Activate.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 20, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	
	/**
	 * Action class for activating discussion.
	 */
	class Activate extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				$this->mData['discussion']->setStatus(DiscussionPeer::ACTIVE);
				if ($this->mData['discussion']->save()) {				
					echo "({isSuccessful: true, 'message': '"."You have successfully activated the discussion.'})";
				}
				else {
					echo "({isSuccessful: false, 'message': 'An error have occured while activating the discussion.'})";
				}
			}
			else {
				if ($this->isAjaxRequest()) {
					echo "({isSuccessful: false, 'message': 'An error have occured while activating the discussion.'})";
				}
				else {
					$this->showFileNotFound();
				}
			}
		}
		
		/**
		 * General Rules:
		 * 	1. This must be an ajax request.
		 * 	2. The url must be /discussion/activate/<discussion_id>
		 * 	3. <discussion_id> must be a valid ID of a discussion.
		 * 	4. The current user is a staff or administrator of the group in which the discussion is
		 * 		related.
		 */		
		function validate(){
			if ($this->isAjaxRequest()) {
				if (0 < $this->mData['logger_id'] AND isset($this->mData['discussion_id'])) {
					$this->mData['discussion'] = DiscussionPeer::retrieveByPk($this->mData['discussion_id']);
					
					if (!is_null($this->mData['discussion'])) {
						$this->mData['topic'] = $this->mData['discussion']->getTopic();
						$this->mData['group'] = $this->mData['topic']->getGroup();
						$this->mData['isAdmin'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
						if ($this->mData['isAdmin']) {
							return true;
						}
					}				
				}
			}
			
			return false;	
		}
		
	}