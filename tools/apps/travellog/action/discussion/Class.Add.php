<?php
	/**
	 * @(#) Class.Add.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - 06 19, 09
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.TopicPeer.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	
	/**
	 * Controller action for adding a new discussion.
	 */
	class Add extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				$file_factory = FileFactory::getInstance();
				
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
				
				$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
				$this->mData['sub_navigation']->setContext('GROUP');
				$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
				$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
			
				$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mData['group']->getGroupID());
				$this->mData['profile'] = $profile_comp->get_view();
				
				// Default values
				$this->mData['group_id'] = $this->mData['group']->getGroupID();
				$this->mData['topic_id'] = $this->mData['topic']->getId();
				$this->mData['topic_title'] = $this->mData['topic']->getTitle();
				$this->mData['message'] = "";
				$this->mData['title'] = "";
				
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/discussionboard/tpl.FrmDiscussion.php');
				exit;
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * General Rules:
		 * 	1. The url must be /discussion/add/<topic_id>
		 * 	2. <topic_id> must be a valid ID of a topic.
		 * 	3. The current user is :
		 * 		3.1 A staff or administrator of the group in which the topic is related.
		 * 		3.2 A member of the group(applicable for subgroup and root group) if the 
		 * 			topic is set to open for members only(for subgroup members only means 
		 * 			the members of the subgroup only).
		 * 		3.3 A member of the root group if the topic is set to open to 
		 * 			members of the root group which also includes the members of the subgroup.
		 */		
		function validate(){
			if (isset($this->mData['topic_id']) && 0 < $this->mData['topic_id'] && 0 < $this->mData['logger_id']) {
				$this->mData['topic'] = TopicPeer::retrieveByPk($this->mData['topic_id']);
				
				if (!is_null($this->mData['topic'])) {
					$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
					$this->mData['parent_group'] = ($this->mData['is_subgroup']) ? $this->mData['group']->getParent() : $this->mData['group'];
					$this->mData['is_admin_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id'])));
					$this->mData['is_member_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['is_admin_logged'] || $this->mData['group']->isMember($this->mData['traveler'])));
					$this->mData['is_root_member_logged'] = ($this->mData['is_subgroup']) ? (0 < $this->mData['logger_id'] && ($this->mData['is_member_logged'] || $this->mData['parent_group']->isMember($this->mData['traveler']))) : $this->mData['is_member_logged'];
					
					switch ($this->mData['topic']->getPrivacySetting()) {
						case TopicPeer::PUBLIC_SETTING:
						case TopicPeer::MEMBERS_ONLY_SETTING:						
							if ($this->mData['is_root_member_logged']) {
								return true;
							}
						break;
						case TopicPeer::SUBGROUP_PUBLIC_SETTING:						
						case TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING:
							if ($this->mData['is_member_logged']) {
								return true;
							}						
						break;
					}
				}
			}
			
			return false;				
		}					
		
	}
	
