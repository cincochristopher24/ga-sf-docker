<?php
	/**
	 * @(#) Class.Posts.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 19, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	require_once('travellog/model/Class.PostPeer.php');
		
	/**
	 * Action class for showing the posts of the discussion.
	 */
	class Posts extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				
				$file_factory = FileFactory::getInstance();
				
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
				
				$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
				$this->mData['sub_navigation']->setContext('GROUP');
				$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
				$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
			
				$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mData['group']->getGroupID());
				$this->mData['profile'] = $profile_comp->get_view();
				
				// Default values
				$this->mData['privacy'] = $this->mData['topic']->getPrivacySetting();
				$this->mData['group_id'] = $this->mData['group']->getGroupID();
				
				$criteria = new SqlCriteria2();
				$criteria->addOrderByColumn(PostPeer::ID, SqlCriteria2::ASC);
				$this->mData['posts'] = $this->mData['discussion']->getPosts($criteria);
				
				if( empty($this->mData['posts']) ) {
					require_once('Class.ToolMan.php');
					ToolMan::redirect('/discussion_board/home/'.$this->mData['group']->getGroupID());
				}
				
				$this->mData['first_post'] = $this->mData['posts'][0];
				$this->mData['total_posts_count'] = count($this->mData['posts']);
				
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/discussionboard/tpl.ViewPosts.php');
				exit;
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * General Rules:
		 * 	1. The url must be /discussion/show_posts/<discussion_id>
		 * 	2. <discussion_id> must be a valid ID of a discussion.
		 * 	3. The current user is :
		 * 		3.1 A staff or administrator of the group in which the discussion is related.
		 * 		3.2 A member of the group(Applicable for subgroup and root group) if the discussions 
		 * 			related topic is set to show to members only.
		 * 		3.3 A member of the root group if the discussions topic is set to show to 
		 * 			members of the root group which also includes the members of the subgroup.
		 * 		3.4 Show to public if the discussions topic is set to show to public.
		 */		
		function validate(){
			if (isset($this->mData['discussion_id']) && 0 < $this->mData['discussion_id']) {
				$this->mData['discussion'] = DiscussionPeer::retrieveByPk($this->mData['discussion_id']);
				
				if (!is_null($this->mData['discussion'])) {
					$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
					$this->mData['topic'] = $this->mData['discussion']->getTopic();
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
					$this->mData['parent_group'] = ($this->mData['is_subgroup']) ? $this->mData['group']->getParent() : $this->mData['group'];
					$this->mData['is_admin_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id'])));
					$this->mData['is_member_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['is_admin_logged'] || $this->mData['group']->isMember($this->mData['traveler'])));
					$this->mData['is_root_member_logged'] = ($this->mData['is_subgroup']) ? (0 < $this->mData['logger_id'] && ($this->mData['is_member_logged'] || $this->mData['parent_group']->isMember($this->mData['traveler']))) : $this->mData['is_member_logged'];
					$this->mData['can_reply'] = false;
					
					switch ($this->mData['topic']->getPrivacySetting()) {
						case TopicPeer::PUBLIC_SETTING:
							if ($this->mData['is_root_member_logged']) {
								$this->mData['can_reply'] = true;
							}							
							return true;
						break;
						case TopicPeer::SUBGROUP_PUBLIC_SETTING:
							if ($this->mData['is_member_logged']) {
								$this->mData['can_reply'] = true;
							}
							return true;
						break;
						case TopicPeer::MEMBERS_ONLY_SETTING:
							if ($this->mData['is_root_member_logged']) {
								$this->mData['can_reply'] = true;
								return true;
							}
						break;						
						case TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING:
							if ($this->mData['is_member_logged']) {
								$this->mData['can_reply'] = true;
								return true;
							}						
						break;
						default:
							return true;
						break;
					}
					
					if (0 == $this->mData['logger_id']) {
						$this->redirect('/login.php?redirect=/discussion/posts/'.$this->mData['discussion']->getID());
					}
				}
			}
			
			return false;				
		}			
		
	}