<?php
	/**
	 * @(#) Class.Save.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - 06 19, 09
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.TopicPeer.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	
	/**
	 * Action class for saving a newly created discussion.
	 */
	class Save extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				if (isset($this->mData['errors'])) {
					$file_factory = FileFactory::getInstance();
					
					$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
					$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
					
					$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
					$this->mData['sub_navigation']->setContext('GROUP');
					$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
					$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
				
					$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
					$profile_comp->init($this->mData['group']->getGroupID());
					$this->mData['profile'] = $profile_comp->get_view();
					
					// Default values
					$this->mData['group_id'] = $this->mData['group']->getGroupID();
					$this->mData['topic_id'] = $this->mData['topic']->getId();
					$this->mData['topic_title'] = $this->mData['topic']->getTitle();
					
					$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
					
					$tpl = new Template();
					$tpl->setVars($this->mData);
					$tpl->out('travellog/views/discussionboard/tpl.FrmDiscussion.php');
				}
				else {
					require_once('travellog/model/Class.Discussion.php');
					require_once('travellog/model/Class.Post.php');
					
					$date = date('Y-m-d H:i:s');
					// Create a new discussion
					$discussion = new Discussion();
					$discussion->setTopicId($this->mData['topic']->getId());
					$discussion->setFeatured(DiscussionPeer::UNFEATURE);
					$discussion->setTitle($this->mData['title']);
					$discussion->setStatus(DiscussionPeer::ACTIVE);
					$discussion->setDateUpdated($date);
					$discussion->setDateCreated($date);
					if ($discussion->save()) {			
						// Create new post
						$post = new Post();
						$post->setMessage($this->mData['message']);
						$post->setDiscussionId($discussion->getId());
						$post->setPosterId($this->mData['logger_id']);
						$post->setDateCreated($date);	
						$post->save();
						
						$this->mData['topic']->setDateUpdated($date);
						$this->mData['topic']->save();
						
						require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
						RealTimeNotifier::getInstantNotification(RealTimeNotifier::ADD_DISCUSSION_POST, 
							array(
								'POST'			=> $post,
								'DISCUSSION'	=> $discussion 
						))->send();
						
						$this->redirect("/discussion/posts/".$discussion->getId()."#".$post->getId());
					}								
				}
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * General Rules:
		 * 	1. $_POST['txaMessage'] exists.
		 * 	2. $_POST['txtTitle'] exists.
		 * 	3. $_POST['hidTopicId'] exists.
		 * 	4. $_POST['hidTopicId'] must be a valid ID of a topic.
		 * 	3. The current user is :
		 * 		3.1 A staff or administrator of the group in which the topic is related.
		 * 		3.2 A member of the group(applicable for subgroup and root group) if the 
		 * 			topic is set to open for members only(for subgroup members only means 
		 * 			the members of the subgroup only).
		 * 		3.3 A member of the root group if the topic is set to open to 
		 * 			members of the root group which also includes the members of the subgroup.
		 */		
		function validate(){
			if (isset($_POST['hidTopicId']) && isset($_POST['txaMessage']) && isset($_POST['txtTitle']) && 0 < $this->mData['logger_id']) {
				// Validate message
				$this->mData['message'] = trim($_POST['txaMessage']);
				if (empty($this->mData['message'])) {
					$this->mData['errors']['message'] = "Please enter a valid message.";
				}
				// Validate title
				$this->mData['title'] = trim($_POST['txtTitle']);
				if (empty($this->mData['title'])) {
					$this->mData['errors']['title'] = "Please enter a valid title.";
				}				
								
				$this->mData['topic'] = TopicPeer::retrieveByPk($_POST['hidTopicId']);
				
				if (!is_null($this->mData['topic'])) {
					$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
					$this->mData['parent_group'] = ($this->mData['is_subgroup']) ? $this->mData['group']->getParent() : $this->mData['group'];
					$this->mData['is_admin_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id'])));
					$this->mData['is_member_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['is_admin_logged'] || $this->mData['group']->isMember($this->mData['traveler'])));
					$this->mData['is_root_member_logged'] = ($this->mData['is_subgroup']) ? (0 < $this->mData['logger_id'] && ($this->mData['is_member_logged'] || $this->mData['parent_group']->isMember($this->mData['traveler']))) : $this->mData['is_member_logged'];
					
					switch ($this->mData['topic']->getPrivacySetting()) {
						case TopicPeer::PUBLIC_SETTING:
						case TopicPeer::MEMBERS_ONLY_SETTING:						
							if ($this->mData['is_root_member_logged']) {
								return true;
							}
						break;
						case TopicPeer::SUBGROUP_PUBLIC_SETTING:						
						case TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING:
							if ($this->mData['is_member_logged']) {
								return true;
							}						
						break;
					}
				}
			}
			
			return false;				
		}
		
	}
	
