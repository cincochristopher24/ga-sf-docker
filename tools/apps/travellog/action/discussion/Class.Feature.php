<?php
	/**
	 * @(#) Class.Feature.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - May 28, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	require_once('travellog/helper/Class.DiscussionHelper.php');
	
	/**
	 * Action class for featuring discussion.
	 */
	class Feature extends ControllerAction {
	
		function execute(){
			if ($this->validate()) {
				$this->mData['discussion']->setFeatured(DiscussionPeer::FEATURE);
				if ($this->mData['discussion']->save()) {
					$criteria = new SqlCriteria2();
					$criteria->addOrderByColumn(DiscussionPeer::DATE_UPDATED, SqlCriteria2::DESC);
					$criteria->addLimit(3);					
					
					switch ($this->mData['mode']) {
						case 1:
							$this->mData['discussion_helper'] = new DiscussionHelper($this->mData['is_admin_logged']);
							$this->mData['discussion_helper']->setTemplate('tpl.IncDiscussion2.php');
							$this->mData['discussion_helper']->addDiscussions($this->mData['topic']->getDiscussions($criteria));						
						
							$tpl = new Template();
							$tpl->setVars($this->mData);
							$message = preg_replace('/\'/', '\\\'', $tpl->fetch('travellog/views/discussionboard/tpl.ViewAjaxDiscussions.php'));
							echo "({'isSuccessful': true, 'message': '".preg_replace('/\s+/', ' ', $message)."' })";
						break;
						case 2:
							$this->mData['discussion_helper'] = new DiscussionHelper($this->mData['is_admin_logged']);
							$this->mData['discussion_helper']->setTemplate('tpl.IncDiscussion2.php');
							$this->mData['discussion_helper']->addDiscussions($this->mData['topic']->getDiscussions($criteria));						
						
							$tpl = new Template();
							$tpl->setVars($this->mData);
							$message = preg_replace('/\'/', '\\\'', $tpl->fetch('travellog/views/discussionboard/tpl.ViewAjaxDiscussions.php'));
							echo "({'isSuccessful': true, 'message': '".preg_replace('/\s+/', ' ', $message)."' })";
						break;
						case 3:
							$criteria->addLimit(5);	
							$this->mData['discussion_helper'] = new DiscussionHelper($this->mData['is_admin_logged']);
							$this->mData['discussion_helper']->setTemplate('tpl.IncDiscussion3.php');
							$this->mData['discussion_helper']->addDiscussions($this->mData['topic']->getDiscussions($criteria));
													
							$tpl = new Template();
							$tpl->set('discussion_helper', $this->mData['discussion_helper']);
							$message = preg_replace('/\'/', '\\\'', $tpl->fetch('travellog/views/discussionboard/tpl.ViewAjaxSubGroupHomePageDiscussions.php'));
							echo "({'isSuccessful': true, 'message': '".preg_replace('/\s+/', ' ', $message)."' })";
						break;
						case 4:
							$this->mData['discussion_helper'] = new DiscussionHelper($this->mData['is_admin_logged']);
							$this->mData['discussion_helper']->setTemplate('tpl.IncDiscussion4.php');
							$this->mData['discussion_helper']->addDiscussions($this->mData['topic']->getDiscussions());						
						
							$tpl = new Template();
							$tpl->setVars($this->mData);
							$message = preg_replace('/\'/', '\\\'', $tpl->fetch('travellog/views/discussionboard/tpl.ViewAjaxDiscussions.php'));
							echo "({'isSuccessful': true, 'message': '".preg_replace('/\s+/', ' ', $message)."' })";
						break;
						case 0:
						default:
							echo "({'isSuccessful': true, 'message': '"."You have successfully pinned the discussion!' })";
					}
				}
				else {
					echo "({'isSuccessful': false, 'message': 'An error have occured while featuring the discussion.'})";
				}
			}
			else {
				if ($this->isAjaxRequest()) {
					echo "({'isSuccessful': false, 'message': 'An error have occured while featuring the discussion.'})";
				}
				else {
					$this->showFileNotFound();
				}
			}
		}
		
		/**
		 * General Rules:
		 * 	1. This must be an ajax request.
		 * 	2. The url must be /discussion/activate/<discussion_id>
		 * 	3. <discussion_id> must be a valid ID of a discussion.
		 * 	4. The current user is a staff or administrator of the group in which the discussion is
		 * 		related.
		 */		
		function validate(){
			if ($this->isAjaxRequest()) {
				if (0 < $this->mData['logger_id'] AND isset($this->mData['discussion_id'])) {
					$this->mData['discussion'] = DiscussionPeer::retrieveByPk($this->mData['discussion_id']);
					if (!is_null($this->mData['discussion'])) {
						$this->mData['topic'] = $this->mData['discussion']->getTopic();
						$this->mData['group'] = $this->mData['topic']->getGroup();
						$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
						if ($this->mData['is_admin_logged']) {
							return true;
						}
					}				
				}
			}
			
			return false;	
		}
	
	}