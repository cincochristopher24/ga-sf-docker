<?php
	/**
	 * @(#) Class.Edit.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - 06 19, 09
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.TopicPeer.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	
	/**
	 * Controller action for adding a new discussion.
	 */
	class Edit extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				$this->mData['discussion']->setTitle($_POST['group_template_name']);
				$this->mData['discussion']->save();
				
				echo json_encode(array(
					'is_successful' => true,
					'title'         => $this->mData['discussion']->getTitle() 
				));
				
				exit;
			}
			else {
				echo json_encode(array('is_successful' => false));
			}
		}
		
		/**
		 * General Rules:
		 * 	1. The url must be /discussion/edit/<discussion_id>
		 * 	2. <discussion_id> must be a valid ID of a discussion.
		 * 	3. The current user is :
		 * 		3.1 A staff or administrator of the group in which the topic is related.
		 * 		3.2 A member of the group(applicable for subgroup and root group) if the 
		 * 			topic is set to open for members only(for subgroup members only means 
		 * 			the members of the subgroup only).
		 * 		3.3 A member of the root group if the topic is set to open to 
		 * 			members of the root group which also includes the members of the subgroup.
		 */		
		function validate(){
			if (isset($this->mData['discussion_id']) && 0 < $this->mData['discussion_id'] && 0 < $this->mData['logger_id']) {
				if (($this->mData['discussion'] = DiscussionPeer::retrieveByPk($this->mData['discussion_id']))) {
					$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
					$this->mData['topic'] = $this->mData['discussion']->getTopic();
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
					$this->mData['parent_group'] = ($this->mData['is_subgroup']) ? $this->mData['group']->getParent() : $this->mData['group'];
					$this->mData['is_admin_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id'])));
					$this->mData['is_member_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['is_admin_logged'] || $this->mData['group']->isMember($this->mData['traveler'])));
					$this->mData['is_root_member_logged'] = ($this->mData['is_subgroup']) ? (0 < $this->mData['logger_id'] && ($this->mData['is_member_logged'] || $this->mData['parent_group']->isMember($this->mData['traveler']))) : $this->mData['is_member_logged'];
					
					switch ($this->mData['topic']->getPrivacySetting()) {
						case TopicPeer::PUBLIC_SETTING:
						case TopicPeer::MEMBERS_ONLY_SETTING:						
							if ($this->mData['is_root_member_logged']) {
								return true;
							}
						break;
						case TopicPeer::SUBGROUP_PUBLIC_SETTING:						
						case TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING:
							if ($this->mData['is_member_logged']) {
								return true;
							}						
						break;
					}
				}
			}
			
			return false;				
		}					
		
	}
	
