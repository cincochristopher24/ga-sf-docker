<?php  
	/**
	 * @(#) Class.ControllerAction.php
<<<<<<< .working
	 *
	 * @version 1.0 June 06, 2009 
=======
	 * 
	 * Autogenerated by GoAbroad Code Generator 1.0 
	 *
	 * @version 1.0 Jun 06, 2009 
>>>>>>> .merge-right.r13552
	 */
	
	require_once('travellog/model/Class.SessionManager.php');
	
	/**
	 * Abstract class of action.
	 */
	abstract class ControllerAction {
		
		/**
		 * The handler for $_GET, $_POST, $_REQUEST, $_SESSION and other means of setting the data.
		 * 
		 * @var mixed[]
		 */
		protected $mData = array();
		
		/**
		 * Constructs the action object for adding discussion to knowledge base.
		 * 
		 * @param array $data The data that will be used by the action object to add
		 * 										a discussion to the knowledge base.
		 */
		function __construct(array $data = null){
			$this->mData = is_null($data) ? array() : $data;
			
			$session = SessionManager::getInstance();
			$this->mData['logger_id'] = $session->get('travelerID');			
			
		}
		
		/**
		 * Executes the action
		 * 
		 * @return boolean true if the action has been executed successfully.
		 */
		abstract function execute();
		
		/**
		 * Validates the given data.
		 * 
		 * @return boolean true if the data are valid.
		 */
		abstract function validate();
		
		/**
		 * Returns true if the current request comes from an ajax request. This is only
		 * compatible if you are using jQuery as an ajax request.
		 * 
		 * @return boolean true if the current request comes from an ajax request.
		 */
		static function isAjaxRequest(){
			$headers = apache_request_headers();
			
			return (isset($headers['X-Requested-With']) AND "XMLHttpRequest" == trim($headers['X-Requested-With']));			
		}
		
		/**
		 * The handler for the file not found error.
		 * 
		 * @return void
		 */
		static function showFileNotFound(){
			header ("HTTP/1.0 404 Not Found");
			header ("Status 404 Not Found"); 
			require_once("FileNotFound.php");
			exit;
		}
		
		/**
		 * Redirects to the given url.
		 * 
		 * @param string $url The url where the page will be redirected.
		 * 
		 * @return void
		 */
		static function redirect($url){
			header ("Location: ".$url);
			exit;
		}
	}