<?php
/*
 * Created on Oct 24, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 class PhotoUIFacade{
 		
 		private $context 	= "profile";
 		private $genID		=	0;
 		private $loginID		= 	0;
 		private $groupHasPhotosInAlbum = false;

		private $isAdmin	= false;
 		
 		
 		function setContext($context){
 			$this->context = $context;
 		}
 		
 		function setGenID($genID){
 			$this->genID = $genID;
 		}
 		
 		function setLoginID($loginID){
 			$this->loginID = $loginID;
 		}

		function setGAAdmin($_isAdmin = false){
			$this->isAdmin = $_isAdmin;
		}
 		
 		function setGroupHasPhotosInAlbum($groupHasPhotosInAlbum){
 			$this->groupHasPhotosInAlbum = $groupHasPhotosInAlbum;
 		}
 		
 		function build(){
 			
 				require_once('Class.Template.php');
 				require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
 				require_once('travellog/UIComponent/Photo/factory/Class.PhotoLayoutFactory.php');
 				
 				$isOwner 		= false;
 				$ObjContext	= array();
 				
				if($this->context !== "home"){ 
 					$Pfactory = new PhotoFactory($this->genID,$this->context);
 					$ObjContext	= $Pfactory->create(); 	
 					$isOwner = $ObjContext->isOwner($this->loginID);
 				}
				
				//hack owner permission
				if($this->context == "group"){
					$obj_session  = SessionManager::getInstance();
					$adminGroup = new AdminGroup();
					$adminGroup->setAdminGroupData(array('groupID'=>$this->genID));
					if(in_array($obj_session->get('travelerID'),$adminGroup->getStaff(true)) || $ObjContext->getPHOTOcontext()->getAdministrator()->gettravelerID() == $obj_session->get('travelerID')){
						$isOwner = true;
					}
				} 
				
				
				$template = new Template();
				$template->set_path('travellog/UIComponent/Photo/views/');
				$template->set('isowner', $isOwner);
				$template->set('context', $this->context);
				$template->set('loginID', $this->loginID);
				$template->set('genID', $this->genID);
 				$template->set('ObjContext', $ObjContext);
				$template->set('IsGroupHasPhotosInAlbum',$this->groupHasPhotosInAlbum);
				$template->set('isGAAdmin', $this->isAdmin);

				if($this->context !== "group")
		    		echo $template->fetch('tpl.PhotoUIFacade.php');
		    	else
		    		echo $template->fetch('tpl.groupPhotoUIFacade.php');
		    	
		    	//require_once("travellog/UIComponent/Photo/command/Class.PhotoCacheTOjsCommand.php");
				//PhotoCacheTOjsCommand::doCache($Pfactory,$parentData);
 				 				
 				
 		}
 		
 		
 	
 }
 
 
?>
