<?php
/*
 * Created on Oct 23, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class GaCollectionDTO{

	private $genid				= 0;
	private $title					= NULL;
	private $context				= NULL;
	private $primaryphoto	= NULL;
	private $num_photos		= 0;			  

	
	function setGenID($genid){
		$this->genid = $genid;
	}
	
	function setTitle($title){
		$this->title = $title;
	}
	
	function setContext($context){
		$this->context = $context;
	}
	
	function setPrimaryPhoto(Photo $primaryphoto){
		$this->primaryphoto = $primaryphoto;
	}
	
	function setNumPhotos($num_photos){
		$this->num_photos = $num_photos;
	}
	
	
	
	
	function getGenID(){
		return $this->genid;
	}
	
	function getTitle(){
		return $this->title;
	}
	
	function getContext(){
		return $this->context;
	}
	
	function getPrimaryPhoto(){
		return $this->primaryphoto;
	}
	
	function getNumPhotos(){
		return $this->num_photos;
	}
	
	
	
}


?>