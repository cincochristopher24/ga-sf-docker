<?php
/**  
 * IPhotoManager.php  2007-08-15 02:43:56Z root $
 **/

/**
 * IPhotoManager is an interface class for manipulating Photo.
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 **/
 
interface IPhotoManager{
	
	/**
	 * Save/Upload Photo
	 */
	function save(UploadManager $UPM);
	
	/**
	 * Change Photo
	 * @param photoid
	 * @return 
	 */
	function change(UploadManager $UPM);
	
	/**
	 * Rotate Photo by angle
	 * @param photoid
	 * @return 
	 */
	function rotate(PhotoFactory $Pfac, $photoID, $rotation);
	
	/**
	 * Rearrange Photo in Order
	 * @param $photoIDs
	 * @return 
	 */
	function rearrange(PhotoFactory $Pfac, $photoIDs);
	
	/**
	 * Set to Primary Photo
	 * @param photoID
	 * @return 
	 */
	function setASPrimary(PhotoFactory $Pfac, $photoID);
	
	/**
	 * Delete Photo
	 * @param photoID
	 * @return 
	 */
	function delete(PhotoFactory $Pfac, $photoID);
	
	/**
	 * Add Caption of Photo
	 * @param photoid
	 * @return 
	 */
	function updatecaption(PhotoFactory $Pfac, $photoID, $caption);
	

} 
?>
