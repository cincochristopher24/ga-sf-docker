<?


class PhotoDeletephoto {
	
	public $observeraction = "delete";
	
    function PhotoDeletephoto($request){
    	
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
		require_once("travellog/UIComponent/Photo/observer/Class.HeaderPhotoObserver.php");
		require_once("travellog/UIComponent/Photo/observer/Class.PhotoEventObserver.php");  
		require_once('Class.PhotoOnloadgallery.php');
		
		$photoID = $request['pID'];
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		PhotoManager::delete($Pfactory,$photoID);
		//HeaderPhotoObserver::onChanged($this, PhotoFactory::create()->getPHOTOcontext());
		echo "<script>";
			PhotoEventObserver::onChanged($request, PhotoFactory::create()->getPHOTOcontext());
		echo "</script>";
		$request['pID'] = 0;
		return new PhotoOnloadgallery($request);
		
	}
}

?>
