<?php


class PhotoDoprimarytravel {

    function PhotoDoprimarytravel($request){
    	
    	$photoID = $request['pID'];
		require_once('travellog/model/Class.TravelLog.php');   
		require_once('travellog/model/Class.Travel.php');  
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		$entry_primary_id = PhotoFactory::create()->getPHOTOcontext()->getPrimaryPhoto()->getphotoID();
		
		
		$travelprimaryphoto = 0;
		
		$travellog 	= new TravelLog($request['genid']);
		$travel 	= new Travel($travellog->getTravelID());
		
		if($travel->getPrimaryphoto() >0 ){
				$travelprimaryphoto = $travel->getPrimaryphoto();
		}
		
			
		$travel->setPrimaryphoto($photoID);
		$travel->update();	
			
		
		$is_photo_primary_entry = 0;
		
		?>
		
		
		<? if($entry_primary_id == $photoID): ?>
			jQuery("#optionprimary").html("");
			<? $is_photo_primary_entry = 1;	?>
		<? endif; ?>
		
		<? if($travelprimaryphoto >0 ): ?>
				
				jQuery("#thumb<?= $travelprimaryphoto?>").attr({onclick: "PhotoService.viewfullsize('fullsize_<? echo $request['context']; ?>',1,<?= $travelprimaryphoto;?>,<?= $is_photo_primary_entry; ?>,1)"});
		
		<? else:?>
				
				jQuery("#thumb<?= $photoID?>").attr({onclick: "PhotoService.viewfullsize('fullsize_<? echo $request['context']; ?>',1,<?= $photoID;?>,<?= $is_photo_primary_entry; ?>,0)"});
				
		<? endif; ?>
		
		
			
	<?
		
		
		
		
		
	}
}



?>
