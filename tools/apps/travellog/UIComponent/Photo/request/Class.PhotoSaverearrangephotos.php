<?php


class PhotoSaverearrangephotos {

    function PhotoSaverearrangephotos($request){
		
		//$photoIDs = explode("&",str_replace("sortable[]=", "", $request['txtsortval']));
       
       	$photoIDs = explode(",",$request['txtsortval']);
       
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");		
		require_once('Class.PhotoOnloadgallery.php');
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		PhotoManager::rearrange($Pfactory,array_reverse($photoIDs));
		$request['pID'] = 0;
		return new PhotoOnloadgallery($request);
    }
}

 
?>
