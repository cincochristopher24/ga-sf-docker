<?php


class PhotoExeccrop {

    function PhotoExeccrop($request){
    
	    require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
	    require_once('travellog/model/Class.Photo.php');
	    
	    $Pfactory = new PhotoFactory($request['genid'],$request['context']);
	        	
	    $photo = new Photo($Pfactory->create()->getPHOTOcontext(),$request['pID']);
		$imgpath = $photo->getPhotoLink('fullsize');
		$imgpath_pos	=strpos($imgpath,"/users/")?substr($imgpath,strpos($imgpath,'/users',1)):substr($imgpath,strpos($imgpath,'/images/',1));
		list($cwidth, $cheight) = getimagesize($_SERVER['DOCUMENT_ROOT'].$imgpath_pos);
		$imgcurrrent = array('width'=>$cwidth,'height'=>$cheight);  	
        	
        $dim = $imgcurrrent['width']>$imgcurrrent['height']?$imgcurrrent['height']:$imgcurrrent['width']	
        	
        	
	?>
			
			
			PhotoService.curCrop = 
			new Cropper.ImgWithPreview('cropImage',{
					onEndCrop:function(coords, dimensions){
						jQuery('#cropX').val(coords.x1);
						jQuery('#cropY').val(coords.y1);
						jQuery('#cropWidth').val(dimensions.width);
				   		jQuery('#cropHeight').val(dimensions.height);
					},
					
					<? if($request['picsize'] == "thumbnail"):?>
					
						previewWrap: 'previewWrap<?= $request["pID"]?>',
						ratioDim: { x: <?= $dim?>, y: <?= $dim?> },
						minWidth: 65, 
						minHeight:65
				  
				  <? else: ?>
				  		
				  		previewWrap: 'photo',
						ratioDim: { x: <?= $dim?>, y: <?= $dim?> },
						minWidth: 120, 
						minHeight:120
				  		
				   <? endif; ?>
						
			})
			
			PhotoService.islastCropthumb		= <?= $request["pID"]?>;
			PhotoService.lastCropthumb		= "<?= $photo->getPhotolink('thumbnail') ?>";
			
			jQuery('#cropWidth').val(<?= $imgcurrrent['width']; ?>);
			jQuery('#cropHeight').val(<?= $imgcurrrent['height']; ?>);
			
			
			
	<?
					
		
	}
}



?>
