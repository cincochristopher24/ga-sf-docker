<?php

require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
require_once("travellog/model/Class.UploadManager.php");
require_once('travellog/model/Class.PathManager.php');
require_once("travellog/UIComponent/Photo/model/Class.ParsePERLUploadParams.php");
require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoJournalLogCommand.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoTravelLastUpdatedCommand.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoNotifierCommand.php");


class PhotoAddphoto {

    function PhotoAddphoto($request){
		
		$ids 		= explode("-",$request['id']);
		$loginid	= $ids[0];
		
		new PhotoFactory($request['genID'],$request['cat']);
		new ParsePERLUploadParams($request['tmp_sid']);
		
		$path = new PathManager(PhotoFactory::create()->getPHOTOcontext());	
		
		$U_Manager = new UploadManager();
		$U_Manager->setContext($request['cat']);			
		$U_Manager->setDestination($path);			

		$U_Manager->setFiles(ParsePERLUploadParams::getFiles());
		$U_Manager->setTypeUpload('image');
		$U_Manager->upload();		
		
		
		$P_Manager = new PhotoManager();
		$P_Manager->setLoginID($loginid);
		$P_Manager->setGenID($request['genID']);
		$P_Manager->setContext($request['cat']);
		$P_Manager->setIsGroupChecked(ParsePERLUploadParams::isGroupNotified());
		$P_Manager->setIsAddressChecked(ParsePERLUploadParams::isAddressBookNotified());
		//set Command to execute after new Photo Uploaded
		
		$P_Manager->addCommand(new PhotoJournalLogCommand);			//Log to Journal
		$P_Manager->addCommand(new PhotoTravelLastUpdatedCommand);	//update Travel
		$P_Manager->addCommand(new PhotoNotifierCommand);			//Send Notice
			
		$P_Manager->Save($U_Manager);
		
		require_once('Class.Template.php');	
		$template = new Template();
		$template->set_path('travellog/UIComponent/Photo/views/');
		
					
		if($P_Manager->getValidFiles()){	
			// insert code for logger system
			//require_once('gaLogs/Class.GaLogger.php');
			//GaLogger::create()->start($loginid,'TRAVELER');
			
			
			$PhotoIDs = array();
			foreach($P_Manager->getVALIDUploadPHOTOIDsnFilenames() as $PFilenames)
				$PhotoIDs[]	= $PFilenames['photoID'];
			
			echo  implode(",", $PhotoIDs);;  
		
		}else{
			echo "invalid";  
		}	
		
    }
}



?>
