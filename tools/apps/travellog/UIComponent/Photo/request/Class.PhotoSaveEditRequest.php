<?php


class PhotoSaveEditRequest {

    function PhotoSaveEditRequest($request){
    	
    	require_once('travellog/controller/Class.PhotoController.php');	
		
		$ids = explode("-",$request['id']);
		
		$context 	= $request['cat'];
		$loginid	= $ids[0];
		$genid		= $request['genID'];
		
		$PhotoController = new PhotoController();
		$PhotoController->setContext($context);				
		$PhotoController->setLoginID($loginid);
		$PhotoController->setGenericID($genid);
				
		$PhotoController->initialize();
		
		$PhotoController->parseParam($request['tmp_sid']);
		$PhotoController->Update($ids[1]);	
		
		if($PhotoController->getValidFiles()){	
			$PhotoController->initialize();
			$photos	= $PhotoController->getPhotos();
			
			$pIDs = array();
			
			if($photos){
				foreach($photos as $IDs){
					$pIDs[] = $IDs->getphotoid();
				}
			}
			//get pagenumber of photo
			$pnum = array_search($ids[1],$pIDs) +1;
			
			echo $pnum; 
			//echo $PLayout->createLayout()->render();
			
		}else{
			require_once('Class.Template.php');	
			$template = new Template();
			$template->set_path('travellog/UIComponent/Photo/views/');
			$template->set("error",$PhotoController->getInvalidFiles());
			echo $template->fetch('tpl.UploadError.php');
		}
		
    }
}



?>
