<?php


class PhotoDoprimary {
	
	public $observeraction = "primary";
	
    function PhotoDoprimary($request){
    	require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
		require_once("travellog/UIComponent/Photo/observer/Class.HeaderPhotoObserver.php"); 
		require_once('Class.PhotoFullview.php');
		
		$photoID = $request['pID'];
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		
		$prev_primary_id = PhotoFactory::create()->getPHOTOcontext()->getPrimaryPhoto()->getphotoID();
		
		PhotoManager::setASPrimary($Pfactory,$photoID);
		$objcontext = PhotoFactory::create()->getPHOTOcontext();
		
		
		
		
		$travelprimaryphoto = 0;
		
		if($request['context'] == "travellog"){
			require_once('travellog/model/Class.TravelLog.php');   
			require_once('travellog/model/Class.Travel.php');  
			$travellog 	= new TravelLog($request['genid']);
			$travel 	= new Travel($travellog->getTravelID());
			if($travel->getPrimaryphoto() == $request['pID']){
				$travelprimaryphoto = 1;
			}
		}
		
		require_once("travellog/UIComponent/Photo/observer/Class.PhotoEventObserver.php");  
		PhotoEventObserver::onChanged($request, $objcontext);
		
	?>
	
		//alert(<?= $objcontext->getPrimaryPhoto()->getphotoID()?>);
		//alert(<?= $photoID?>);
		jQuery("#thumb<?= $prev_primary_id?>").attr({onclick: "PhotoService.viewfullsize('fullsize_<? echo $request['context']; ?>',1,<?= $prev_primary_id;?>,0,<?= $travelprimaryphoto; ?>)"});
		jQuery("#optionprimary").html("");
		jQuery("#thumb<?= $photoID?>").attr({onclick: "PhotoService.viewfullsize('fullsize_<? echo $request['context']; ?>',1,<?= $photoID?>,1,<?= $travelprimaryphoto; ?>)"});
		
	<?
		
	}
}



?>
