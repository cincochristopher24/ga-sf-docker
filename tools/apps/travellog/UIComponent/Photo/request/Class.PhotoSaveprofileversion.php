<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


class PhotoSaveprofileversion{
	
	function PhotoSaveprofileversion($request){
		
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
		require_once('Class.PhotoOnloadgallery.php');
		require_once('Class.PhotoFullview.php');
		require_once('Class.PhotoShowcropafterupload.php');
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		
		//echo "<script>alert(333);</script>";
		
		
		PhotoManager::crop($Pfactory, $request['photoid'], $request['cropWidth'], $request['cropHeight'], $request['cropX'], $request['cropY'], $request['picsize']);
		
		/*
		require_once("travellog/UIComponent/Photo/request/Class.PhotoOnloadgallery.php");
		$request['pID'] = 0;
		return new PhotoOnloadgallery($request);
		*/
		
	}	
	
	
}


?>
