<?php

/*
 *  AbstractPhotoLayout.php  2007-08-15 02:43:56Z root $
 */

/**
 * PhotoLayout is used to defined as abstract Photo layout.
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */
abstract class PhotoLayout {
	
	
	/**
	 * check if admin/owner
	 * 
	 */
	function setIsAdmin($isadmin){
		return $isadmin;
	}
	
		
	function setChildData($data =array()){
		
	}
	
	/**
	 * Create An Abstract Layout
	 *  
	 */
	abstract function createLayout();
}