<?php
/*
 * Created on Sep 28, 2007
 *
 */
 
 
 require_once("travellog/UIComponent/Photo/interface/IPhotoCommand.php");
 
 class PhotoJournalLogCommand implements IPhotoCommand{
 	
 	function onCommand(PhotoManager $PManager, $args = array() ){
    	
    	if ( get_class($PManager->getContext()) != 'TravelLog' ) return false;
	    	require_once('travellog/model/Class.JournalLog.php');
			require_once('travellog/model/Class.JournalLogType.php');
			$obj_log = new JournalLog;
			$obj_log->setTravelID($PManager->getContext()->getTravelID()); //travellogID
			$obj_log->setLogType(JournalLogType::$ADDED_NEW_PHOTOS);
			$obj_log->setLinkID($args['photoID']); //last photoID of Travellog
			$obj_log->setTravellogID($PManager->getContext()->getTravelLogID());
			$obj_log->Save();
    		
    		return true;
  	}
 	
 	
 }
 
?>
