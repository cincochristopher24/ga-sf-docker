<?if(count($filenames) < 2):?>
	<?if($isOwner):?>
	<?else:?>
		<div class="tabarea">
	<?endif;?>
<?endif;?>

<div class="photocontainer">
 	<div id="bigthumb" style="height:<?=$currentimgsize['height']+20?>px;">
 		<div id="fullsize">
			 <img id="cropImage<?=$filenames[$start]['pID'];?>" src="<?=$filenames[$start]['fullsizephoto'];?>"/>
		</div>
	</div> 
	
	
	<?if(count($filenames) < 2):?>
	<?if($isOwner):?>
	<?else:?>
		</div>
	<?endif;?>
<?endif;?>


<div style="text-align:center;background-color:#aaa;padding:10px 0;display:none;margin: 0 0 10px 0;" id="cropcontrols">
	<input type="button" name="btn_cancel_crop" value="Cancel" onclick="PhotoService.cancelCROPPING(<?=$filenames[$start]['pID'];?>,'<?=$filenames[$start]['thumbsizephoto'];?>')" />
	<input type="button" name="btn_save_crop" value="Save thumbnail version" onclick="PhotoService.savethumbnailversion('true')"/>
		
</div>

<div style="text-align:center;background-color:#aaa;padding:10px 0;display:none;margin: 0 0 10px 0; font-size: 14px; font-weight: bold;" id="cropsuccess">
			
</div>

<div id="controlcontainer">

	<? if (trim($filenames[$start]['caption']) !== ''): ?>
	<div class="phototitle">
	  <p><em><?=$filenames[$start]['caption'];?></em></p>
	</div>
	<? endif; ?>
	
		
			
		
      		<?if($isOwner):?>
    					
    					<? if($parentData['context'] != "travelerjournalentries" && $parentData['context'] != "travelergroup"): ?>
    					
					    		<?if(!$filenames[$start]['primary'] || ($parentData['context'] == "travellog" && !$travelprimaryphoto)): ?>
				    		
						    		<div class="optionprimary">
											<span>Set as primary photo for </span>
											
											
													<select id="primary" onchange="PhotoService.setprimary(<?=$filenames[$start]['pID']?>,<?=$start+1?>)">
												
															<option value="0">--select--</option>
												
													
													<?if(!$filenames[$start]['primary']):?>
															<option value="1">
																<? if($parentData['context'] == 'profile'):?>
																	my profile 
																<? elseif($parentData['context'] == 'photoalbum'):?>
																	this album 
																<? else: ?>
																	this journal entry 
																<? endif; ?>
															</option>
													<?endif;?>
													      		
													<?if($parentData['context'] == "travellog" && !$travelprimaryphoto):?> 
															<option value="2"><?=$traveltitle?></option>
													<?endif;?>	
													</select>
													
											
									</div>
								
								<?endif;?>
								
						<?endif;?>
    			<div class="optioncontainer"> 
						<ul>
						
							<li><a href="javascript:void(0)" onclick="PhotoService.rotatephoto(<?=$filenames[$start]['pID']?>,'left',<?=$start+1?>)">
							      		<span>Rotate</span>
							</a></li> 
							<!--
							<li class="change"><a href="javascript:void(0)" onclick="PhotoService.editphoto(<?=$filenames[$start]['pID']?>)">
							      		Change Photo
							</a></li>  
							-->
							<li>
							<?if (trim($filenames[$start]['caption']) !== ""):?>
						      			<a href="javascript:void(0)" onclick="PhotoService.editcaption(<?=$filenames[$start]['pID']?>,<?=$start+1?>)"><span>Edit photo Caption</span></a>
						      		<?else:?>
						      			<a href="javascript:void(0)" onclick="PhotoService.editcaption(<?=$filenames[$start]['pID']?>,<?=$start+1?>)"><span>Add photo Caption</span></a>
						      		<?endif;?>
							</li>		

						    <?$dim = $currentimgsize['width']>$currentimgsize['height']?$currentimgsize['height']:$currentimgsize['width'] ?>
						    
						    <li>
						    	
						    	<!--- temporary remove cropping for profile version
						    	<? if($parentData['context'] == "profile"): ?>
								    <? if($filenames[$start]['primary']):?>
								    	<a href="javascript:void(0)" onclick="PhotoService.execprofileversion(<?=$filenames[$start]['pID']?>,<?=$dim?>)">
									      	<span>Set profile version</span>
										</a>
									<?else :?>	
										<a href="javascript:void(0)" onclick="PhotoService.execthumbnailversion(<?=$filenames[$start]['pID']?>,<?=$dim?>)">
								      		<span>Set Thumbnail</span>
										</a>
									<? endif; ?>
								<?else :?>	
										<a href="javascript:void(0)" onclick="PhotoService.execthumbnailversion(<?=$filenames[$start]['pID']?>,<?=$dim?>)">
									      	<span>Set Thumbnail</span>
										</a>
								<? endif; ?>
								-->
								
								<a href="javascript:void(0)" onclick="PhotoService.execthumbnailversion(<?=$filenames[$start]['pID']?>,<?=$dim?>)">
									      	<span>Set Thumbnail</span>
								</a>
								
								
							</li>

							<li><a href="javascript:void(0)" onclick="PhotoService.deletephoto(<?=$filenames[$start]['pID']?>)">
						      			<span>Delete</span>
						    </a></li> 
						    
							<!--
							<li class="rotateright"><a href="javascript:void(0)" onclick="PhotoService.rotatephoto(<?=$filenames[$start]['pID']?>,'right',<?=$start+1?>)">
							      		Rotate right
							</a></li>  
			    			-->
			    		</ul>
    			
    			
				</div>
				
				<form id="frmthumbversion" name="frmthumbversion" class="frmCrop">
				
					<input id="txtcropcount" 	name="txtcropcount" 	type="hidden" 		value="1"/>
					<input type="hidden"  	 	name="cropX" 			id="cropX" 			value="0" />
			        <input type="hidden"  	 	name="cropY" 			id="cropY" 			value="0" />
			        <input type="hidden"  	 	name="cropWidth" 		id="cropWidth" 		value="<?=$currentimgsize['width']?>" />
			        <input type="hidden"  	 	name="cropHeight" 		id="cropHeight" 	value="<?=$currentimgsize['height']?>" />
					<input type="hidden" 		name="photoid" 			id= "photoid"   	value="<?=$filenames[$start]['pID']?>"/>
					<input type="hidden"		name="tofullview" 		type="tofullview" 	value="1"/>
					<input type="hidden"  	name="action"  			id= "action"   		value="savethumbversion"/>
					
					<input type="hidden" name="LASTPic" id= "LASTPic" value="1" />
					
					
					<!--- temporary remove cropping for profile version
					<? if($parentData['context'] == "profile" && $filenames[$start]['primary']): ?>
					    	<input type="hidden" name="picsize" id= "picsize" value="profile" />
					<?else :?>	
							<input type="hidden" name="picsize" id= "picsize" value="thumbnail" />
					<? endif; ?>
					-->	
					
					<input type="hidden" name="picsize" id= "picsize" value="thumbnail" />
					
					<input type="hidden"  	name="context" 			id= "context" 		value="<?=$parentData['context'];?>" />
					<input type="hidden"  	name="genid" 			id= "genid" 		value="<?=$parentData['genid'];?>" />
					<input type="hidden"  	name="loginid" 			id= "loginid"   	value="<?=$parentData['loginid'];?>"/>
					<input type="hidden" 	name="layout" 			id= "layout"   		value="1"/>
					<input type="hidden" 	name="isowner" 			id= "isowner"   	value="1"/>
					<input type="hidden" 	name="startrow" 		id= "startrow"  	value="<?=$start+1?>"/>
					<input type="hidden" 	name="pagenum" 			id= "pagenum"   	value="<?=$start+1?>"/>
					
					
				</form> 			
	    	<?endif;?>
	   	
</div>
 

    <div id="loading" style="display:none;padding:3px;">
	    	<img src="/images/loading.gif"/>
	</div>
    
    <div id="actionscontrol">
    	
    </div>


<div class="clear"></div>
	
	
	
</div>

