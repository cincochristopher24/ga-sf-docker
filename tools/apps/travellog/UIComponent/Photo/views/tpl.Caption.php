<div class="phototitle">
  <p><i><?=$caption;?></i></p>
</div>

<?if($isOwner):?>
	<a href="/photomanagement.php?action=vfullsize&cat=<?=$context;?>&genID=<?=$genid;?>&photoID=<?=$photoid;?>">&raquo; Manage Photos</a> 
	<div class="clear"></div>
	
	<table border="0" width="100%">  
	  <tr>
	      <td width="">
	      		<?if(!$primary):?>
	      			<a href="javascript:void(0)" onclick="PhotoService.setprimary(<?=$photoid?>)">
	      				Set as primary photo<?if($context == "travellog"):?> for this journal entry<?endif;?>
	      			</a>
	      			<br/>
	      		<?endif;?>
	      		
	      		<?if($context == "travellog" && !$travelprimaryphoto):?> 
	      			<a href="javascript:void(0)" onclick="PhotoService.setprimarytravel(<?=$photoid?>)">
	      				Set as Primary Photo for <?=$traveltitle?>
	      			</a>
	      		<?endif;?>	
	      </td>
	  </tr>
	</table>
	
	<table border="0" width="100%">  
	  <tr>
	      <td width="">
	      		<?if (trim($caption) !== ""):?>
	      			<a href="javascript:void(0)" onclick="PhotoService.editcaption(<?=$photoid;?>)">Edit Caption</a>
	      		<?else:?>
	      			<a href="javascript:void(0)" onclick="PhotoService.editcaption(<?=$photoid;?>)">Add Caption</a>
	      		<?endif;?>
	      </td>
	      <td width="">
	      		<a href="javascript:void(0)" onclick="PhotoService.deletephoto(<?=$photoid?>)">
	      			Delete
	      		</a>	
	      </td>
	      <td width="">
		      	<a href="javascript:void(0)" onclick="PhotoService.rotatephoto(<?=$photoid?>,'left',<?=$pnum?>)">
		      		&raquo; Rotate left
		      	</a> 
		      	<br>
		      	<a href="javascript:void(0)" onclick="PhotoService.rotatephoto(<?=$photoid?>,'right',<?=$pnum?>)">
		      		&raquo; Rotate right
		      	</a> 
		  </td>
	      <td width="">
	      		<a href="">
		      		&raquo; Rearrange
		      	</a> 
		  </td>
		  <td width="">
	      		<a href="javascript:void(0)" onclick="PhotoService.uploadphoto()">
		      		&raquo; Upload
		      	</a> 
		  </td>
		  <td width="">
	      		<a href="javascript:void(0)" onclick="PhotoService.editphoto(<?=$photoid;?>)">
		      		&raquo; Change Photo
		      	</a> 
		  </td>
	  </tr>    
	</table>
<?endif;?>