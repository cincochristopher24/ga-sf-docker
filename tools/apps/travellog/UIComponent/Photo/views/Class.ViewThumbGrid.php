<?php

require_once("travellog/views/Class.AbstractView.php");
require_once("travellog/UIComponent/Photo/factory/Class.PhotoUIRequestFactory.php");

class ViewThumbGrid extends AbstractView{
	
	private $mRequest;
	private $mContents = array();
	
	public function __construct($request=array()){
		$default = array(	"action"	=>	"onloadgallery",
							"layout"	=>	8,
							"context"	=>	"travellog",
							"loginid"	=>	0,
							"genid"		=>	0,
							"pID"		=>	0	); 
		$this->mRequest = array_merge($default,$request);
		$_REQUEST["genid"] = $this->mRequest["genid"];
		$_REQUEST["loginid"] = $this->mRequest["loginid"];
		$_REQUEST["context"] = $this->mRequest["context"];
		if( isset($this->mRequest["group_id"]) ){
			$_REQUEST["group_id"] = $this->mRequest["group_id"];
			$_REQUEST["ownerLogged"] = ($this->mRequest["group_id"] == $this->mRequest["ownerid"] && 0 != $this->mRequest["loginid"])? true : false;
			if( $_REQUEST["ownerLogged"] ){
				try{
					$group = new AdminGroup($this->mRequest["group_id"]);
					if( $group instanceof AdminGroup){
						$_REQUEST["ownerLogged"] = $group->getAdministratorID() == $this->mRequest["loginid"] || $group->isStaff($this->mRequest["loginid"]) ? TRUE : FALSE;
					}
				}catch(exception $e){}
			}
		}else{
			$_REQUEST["ownerLogged"] = ($this->mRequest["loginid"] == $this->mRequest["travelerid"] && $this->mRequest["ownerid"] == $this->mRequest["loginid"])? true : false;
		}
	}
	
	public function render(){
		PhotoUIRequestFactory::create($this->mRequest);
	}
}