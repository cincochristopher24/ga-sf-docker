<?$tmp_sid = md5(uniqid(mt_rand(), true));?>
<table class="table_popbox">
	<tbody>
		<tr>
			<td class="popbox_topLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_topRight"></td>
		</tr>
		<tr>
			<td class="popbox_border"></td>
			<td class="popbox_content">
				<h4 id="popup_header" class="header"><strong>Upload a Photo?</strong></h4>
				<div id="popup_confirm" class="confirm">
					<div id="uploadcallback" style="float:left;"></div> 
					<div id="process_container" class="confirm">
						<div id="upload_container">
								<div id="uploadnew">
									<span>Upload your <?if($context =="group"):?> Group Logo <? else :?>Photos <? endif; ?></span>
									<p>Select image files from your computer and upload it on your entries.</p>
								</div>

								<div id="uploadrules">
									<p>
										- You can upload up to a total size of 2MB/image. <br />
										- The following image file formats are allowed: jpg, gif, png. 
									</p>
								</div>
	
								<? for($x=0; $x<$defaultnumberofpic; $x++): ?>					
									<div class="file_upload">
										<?=$x+1; ?><input onchange="PhotoService.checkpfile()"  type="file" id ="upfile_<?=$x?>" name="upfile_<?=$x?>" size="30" />
									</div>
								<? endfor; ?>
						</div>
					</div>	
					</div>
				</div>	
				<div id="ccontrols popup_buttons" class="buttons_box">
					<div id="hdvar">
						<input type="hidden" name="txtcat" 			value="<?=$context?>">
						<input type="hidden" name="txtaction" 		value="addphoto">
						<input type="hidden" name="txtgenid" 		value="<?=$genid?>">
						<input type="hidden" name="txtid"			value="<?=$loginid?>-0-<?=$layout?>">
						<input type="hidden" name="txtscriptname" 	value="/ajaxpages/PhotoUILayout.php">
					</div>	
					
					<!--
					<?if($photo):?>
						<?if($context =="group"):?>
							<input type="button" class="prompt_button" name="cancel" value="Cancel" onclick="tb_remove()"/>
						<?else :?>
				 		 	<input type="button" class="prompt_button" name="cancel" value="Cancel" onclick="PhotoService.cancelaction()" />
				  		<?endif; ?>
					<?else:?>
					<script>
						if(PhotoService.uploadsucces_container == "lboxgallerycontainer"){
							jQuery("#ccontrols").append('<input type="button" class="prompt_button" name="cancel" value="Cancel" onclick="tb_remove()"/>');
					}
					</script>
					<?endif;?>
					-->
							<input type="button" class="prompt_button" name="cancel" value="Cancel" onclick="tb_remove()"/>
							<input type="button" disabled="true" class="prompt_button" id="upload" name="upload" value="Upload" onclick="return PhotoService.ajaxFileUpload('<?=$tmp_sid?>','new')" />
		
		<!-- Upload loading proccess container -->
		<div style="text-align:center;background-color:#666;margin-top:10px;margin-bottom: 0; display:none;padding: 10px 0 10px 0;color:#fff;font-size: 14px; font-weight:bold; clear:both;" id="uploadprocess">
			Uploading photos, Please Wait...								
		</div>
		

					
				</div>
			</td>
			<td class="popbox_border"></td>
		</tr>
		<tr>
			<td class="popbox_bottomLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_bottomRight"></td>
		</tr>
	</tbody>
</table>