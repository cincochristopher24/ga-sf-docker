<style>									
	#sortable {width:400px; display:block;}
	#sortable li {cursor: move;position: }
</style>					



<div id="process_container">
	<div class="rearrange_wrapper">
		<span>Rearrange Photos</span>
		<p>Rearrange the photos by dragging the thumbnail to the desired position.</p>

		<div class="help_text">
			<p>Be sure to hit "Save Changes" when you're done to save the changes you made.</p>
		</div>	
		
		<div id="rearrange_photos">	
			<ul id="sortable">		

				<? foreach($photos as $x => $photo): ?>
					<li  id="<?=$photo->getPhotoID();?>" class="sortableitem">										
						<img   src="<?=$photo->getPhotolink('thumbnail');?>"/>																	
					</li>
				<?endforeach;?>		
				<div class="clear"></div>
			</ul>
			
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
	<script type="text/javascript">
	
	</script>

	<form id="frmarrange" name="frmarrange">
		<input type="hidden" id="context" 	name="context" value="<?=$parentData['context']?>">
		<input type="hidden" id="action" 	name="action" value="saverearrangephotos">
		<input type="hidden" id="genid"   	name="genid" value="<?=$parentData['genid']?>">
		<input type="hidden" id="loginid" 	name="loginid" value="<?=$parentData['loginid']?>">
		<input type="hidden" id="layout" 	name="layout" value="1">
		<input type="hidden" id="startrow" 	name="startrow" value="1">
		<input type="hidden" id="pagenum" 	name="pagenum" value="1">
		<input type="hidden" name="isowner"  	id= "isowner"   	value="<?=$isOwner?>"/>
	
		<input type="hidden" name="txtsortval" id="txtsortval" value="" />
		<div class="controls">
			<img src="/images/loading.gif" style="display:none;position:relative;left:-144px;" id="img_loading">	
			<input type="button" value="Cancel" name="cancel" onclick="jQuery('#arrange_control_<?=$parentData['context']?>').css({display:'none'});jQuery('#photogallery_<?=$parentData['context']?>').css({display:'block'});" />
			<input type="button" value="Save changes" name="save" onclick="PhotoService.saverearrangephotos('sortable')" />
		</div>

	</form>
