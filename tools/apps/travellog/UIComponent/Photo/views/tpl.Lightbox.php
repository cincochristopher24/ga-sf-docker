
<div id="gallery">

	<table border="1">
		<tr>
			<td>
				<?= $page->getPrevious();	?>	
			</td>
				<? for($x=$start; $x<$endrow; $x++): ?>
					<td>
						<a href="javascript:void(0)" onclick="PhotoService.viewfullsize('<?=$filenames[$x]['fullsizephoto'];?>','<?=$filenames[$x]['caption'];?>')">
							<img src="<?=$filenames[$x]['thumbsizephoto'];?>">
						</a>
					</td>
				<?endfor;?>
			<td>
				<?= $page->getNext(); ?>	
			</td>
			
		</tr>
	</table>
	
	<div id="bigthumb">
		<table border="1">
			<tr>
				<td>
					
					<?for($p=0; $p<$photocount; $p++): ?>
							<a href="<?=$filenames[$p]['fullsizephoto'];?>"  rel="lightbox[roadtrip]" title="<?if($filenames[$p]['caption'] != ''):?><?=$filenames[$p]['caption'];?><?else:?> <?endif;?>"/>					
					<?endfor;?>
					
					
					<div id="fullsize">					
						<a href="<?=$filenames[$start]['fullsizephoto'];?>" rel="lightbox[roadtrip]"  title="<?if($filenames[$start]['caption'] != ''):?><?=$filenames[$start]['caption'];?><?else:?><?endif;?>">
							<img src="<?=$filenames[$start]['fullsizephoto'];?>">
						</a>
						
					</div>
				</td>
			</tr>
		</table>
	</div>

</div>
