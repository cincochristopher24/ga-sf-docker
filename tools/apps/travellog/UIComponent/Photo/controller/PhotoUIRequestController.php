<?php

/**  
 * PhotoUIRequestController.php  2007-08-15 02:43:56Z root $
 **/

/**
 * PhotoUIRequestController handle the HTTP GET and POST requests.
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */


class PhotoUIRequestController{
	public static function getRequest($request){
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoUIRequestFactory.php");
		if (!isset($_REQUEST['redirect'])) {
			session_start();
		}
		return PhotoUIRequestFactory::create($request);
	}
	
}


?>