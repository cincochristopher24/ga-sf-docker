<?php


class PhotoUIRequestFactory{

	public static function create($request){
		//echo "<script> alert('$request[action]'); </script>";
		require_once("travellog/UIComponent/Photo/request/Class.Photo".ucfirst($request['action']).".php");
	 	$photo = new ReflectionClass("Photo".ucfirst($request['action']));
	 	return $photo->newInstance($request);
	}
		
}

?>
