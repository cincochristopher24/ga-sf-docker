<?php
/*
 * Created on Oct 23, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class GaCollectionPhotos{
	
	private $genID	= 0;
	private $type		= 0;
	
	
	function GaCollectionPhotos(){
		return $this;
	}
	
	function setGenID($genID){
		$this->genID = $genID;
	}
	
	function setType($type){
		$this->type = $type; //(traveler or group);
	}
	
	function doCollect(){
		require_once('travellog/model/Class.Traveler.php');	
		require_once("travellog/UIComponent/Photo/model/Class.GA_".ucfirst($this->type)."CollectionPhotos.php");
	 	$photo = new ReflectionClass("GA_".ucfirst($this->type)."CollectionPhotos");
	 	return $photo->newInstance($this->genID);
	 	
	}
	
	function hasPhoto(){
		return count($this->doCollect()->loadCollection()) >0?true:	false;
	}
		
}




?>
