<?php
/*
 * Created on Nov 5, 2007
 * Class.PhotoBuilderFactory.php
 * 
 * Responsible of creating different sizes of Photo based on Context
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 class PhotoBuilderFactory{
 	
 	public static function create($context,ImageBuilder $IMBLDR){
 		require_once("travellog/UIComponent/Photo/model/Class.Photo".ucfirst($context).".php");
 		$C_PHOTO = new ReflectionClass("Photo".ucfirst($context));
 		$instance = $C_PHOTO->newInstance();
 		return $instance->createImage($IMBLDR);
 	}
 	
 	
 }
 
?>
