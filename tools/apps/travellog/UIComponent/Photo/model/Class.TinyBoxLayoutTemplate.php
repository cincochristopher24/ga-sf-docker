<?

/*
 *  ClassThickboxLayoutTemplate.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.TinyBoxLayoutTemplate .
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */


class TinyBoxLayoutTemplate{
	
    /**
	 * 
	 * @return String 
	 */
	
	private $template = NULL;
	 
   	function __construct(TinyBoxLayout $tinyboxlayout){
   		
   		require_once('travellog/model/nPaging.php');
		require_once('Class.Template.php');	
		
		$PhotoFileNames = $tinyboxlayout->getPhotoFileNames();
		
		$photocount = count($PhotoFileNames);
		
		Paging::Setajax(true);															
		$page = new Paging($photocount,$tinyboxlayout->getStartrow(),$tinyboxlayout->getPagenum(),3);
		$page->setJSname('PhotoService.gallerypage');
		
		$start	= $tinyboxlayout->getStartrow() - 1;										
		$endrow	= ($tinyboxlayout->getStartrow() + 3) -1;
		$endrow = $endrow > $photocount?  $photocount: $endrow;
		
		$this->template = new Template();
		$this->template->set_path('travellog/UIComponent/Photo/views/');
		
		/**
		 *get the fullsize
		 */
		//$fullphoto = str_replace("thumbnail", "fullsize", $PhotoFileNames[$start]);
		
		$this->template->set('start',$start);	
		$this->template->set('endrow',$endrow);
		$this->template->set('page',$page);
		$this->template->set('filenames', $PhotoFileNames);
		//$template->set('fullphoto', $fullphoto);
		$this->template->set('photocount', $photocount);
		$this->template->set('parentData', $tinyboxlayout->getParentData());
		$this->template->set('isOwner', $tinyboxlayout->getIsAdmin());
		
		$imgcurrrent	= array();
		
		$imgprev 		= array();
		$imgnext 		= array();
		
		//get size of current image
		if(array_key_exists($start,$PhotoFileNames)){
			$imgpath = $PhotoFileNames[$start]['standardsizephoto'];
			$imgpath_pos	=strpos($imgpath,"/users/")?substr($imgpath,strpos($imgpath,'/users',1)):substr($imgpath,strpos($imgpath,'/images/',1));
			list($cwidth, $cheight) = getimagesize($_SERVER['DOCUMENT_ROOT'].$imgpath_pos);
			$imgcurrrent = array('width'=>$cwidth,'height'=>$cheight);
		}
		
		//get size of previous image
		if(array_key_exists($start-1,$PhotoFileNames)){
			$imgpath = $PhotoFileNames[$start-1]['standardsizephoto'];
			$imgpath_pos	=strpos($imgpath,"/users/")?substr($imgpath,strpos($imgpath,'/users',1)):substr($imgpath,strpos($imgpath,'/images/',1));
			list($pwidth, $pheight) = getimagesize($_SERVER['DOCUMENT_ROOT'].$imgpath_pos);
			$imgprev = array('width'=>$pwidth,'height'=>$pheight);
		}
		
		//get size next image
		if(array_key_exists($start+1,$PhotoFileNames)){
			$imgpath = $PhotoFileNames[$start+1]['standardsizephoto'];
			$imgpath_pos	=strpos($imgpath,"/users/")?substr($imgpath,strpos($imgpath,'/users',1)):substr($imgpath,strpos($imgpath,'/images/',1));
			list($nwidth, $nheight) = getimagesize($_SERVER['DOCUMENT_ROOT'].$imgpath_pos);
			$imgnext = array('width'=>$nwidth,'height'=>$nheight);
		}
		
		$this->template->set('currentimgsize',$imgcurrrent);	
		$this->template->set('previmgsize',$imgprev);	
		$this->template->set('nextimgsize',$imgnext);
		
		
		//$tmp = $this->template->fetch('tpl.Thickbox.php');
		
		//return $tmp;
	}
	
	function render(){
		return $this->template->fetch('tpl.Tinybox.php');
	}
		
	function renderFullSize(){
		return $this->template->fetch('tpl.TinyboxFullsize.php');
	}		
}

