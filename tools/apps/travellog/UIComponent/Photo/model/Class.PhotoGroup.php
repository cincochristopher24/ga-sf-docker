<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoProfile.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoGroup extends AbstractPhoto{
	
	private $GROUPID	= 0;
	private $PHOTOcontext		= NULL;
	public $NUMPHOTOSTOUPLOAD 	= 1;
	static $photos = array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->GROUPID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		require_once('travellog/model/Class.Group.php');
		require_once('travellog/model/Class.AdminGroup.php');
		
		try{
			$adgroup	= new AdminGroup($id);
		  	$group		= new Group();
		  	$group->setGroupID($id);
		  	$group->setAdministratorID($adgroup->getAdministratorID());
			self::$photos	= $group->getPhotos();
			$this->PHOTOcontext = $group;;
		}
		catch(Exception $e){
		}
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	function getPhotos(){
		return self::$photos;	
	}
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){	
		
		require_once('travellog/model/Class.GroupFactory.php');
		
		$groupFactory = GroupFactory::instance();
	 	$Cgroup = $groupFactory->Create(array($this->GROUPID));
		
		if(in_array($loginID,$Cgroup[0]->getStaff(true)) || $Cgroup[0]->getAdministrator()->gettravelerID() == $loginID)
			return true;
		else
			return false;	
		
		
		/*
		require_once('travellog/model/Class.FunGroup.php');
		$fungroup 	= new FunGroup($this->GROUPID);
		if($fungroup->getAdministrator()->gettravelerID() == $loginID)
			return true;
		else
			return false;
		*/
	}
		
	function getHeaderCaption(){
		require_once('travellog/model/Class.FunGroup.php');
		$fungroup 	= new FunGroup($this->GROUPID);
		return  $fungroup->getName();
	}
	
	function getBackLink(){
		require_once('travellog/model/Class.FunGroup.php');
		$fungroup 	= new FunGroup($this->GROUPID);
		return "/group.php?gID=".$this->GROUPID;
	} 
	
	function getBackCaption(){
		require_once('travellog/model/Class.FunGroup.php');
		$fungroup 	= new FunGroup($this->GROUPID);
		if(strlen($fungroup->getName())-1 == strripos($fungroup->getName(),'s'))
			return $fungroup->getName()."'  Group";						
		else
			return $fungroup->getName()."'s  Group";
	} 
	function getNorecordlabel(){
		if(!count(self::$photos))
			return "There are no Photos in Group!";	
		
	} 
	
	function getSUBNavigation(SubNavigation $SUbnav, $loginID = NULL){
		$SUbnav->setContextID($this->GROUPID);
		$SUbnav->setContext('GROUP');
		$SUbnav->setGroupType('');									
		$SUbnav->setLinkToHighlight('GROUP_NAME');
		
		return $SUbnav;
	}
	
	function createImage(ImageBuilder $ImgBuilder){
		
		$ImgBuilder->create('fullsize',500,500);
							
							
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'fullsize'.$ImgBuilder->getFilename());								
		//$ImgBuilder->crop_me('standard',120,153);
		$ImgBuilder->create('standard',153,153);
		$ImgBuilder->crop_me('featured',164,123);
		
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'standard'.$ImgBuilder->getFilename());	
		//$ImgBuilder->crop_me('thumbnail',65,65);
		$ImgBuilder->create('thumbnail',65,65);
		
		//copy orig file to destination if new upload file not rotate
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename())){
			rename($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename(),$ImgBuilder->getDestination()."orig-".$ImgBuilder->getFilename());
			
			//create txt file for rotating photo purpose.
			$filelocation=$ImgBuilder->getFilename().".txt"; 
			$newfile = fopen($ImgBuilder->getDestination().$filelocation,"a+"); 
			fwrite($newfile, '360'); 
			fclose($newfile); 
		}
		
		//send to cloudfront
		$rawFiles = array();
		$rawFiles[] = 'fullsize'.$ImgBuilder->getFilename();
		$rawFiles[] = 'standard'.$ImgBuilder->getFilename();
		$rawFiles[] = 'featured'.$ImgBuilder->getFilename();
		$rawFiles[] = 'thumbnail'.$ImgBuilder->getFilename();
		$rawFiles[] = 'orig-'.$ImgBuilder->getFilename();
		
		$subPath = $ImgBuilder->getRelativePath();
		
		$ganetAWS = GANETAmazonS3WebService::getInstance();
		$ganetAWS->pushRawFilestoS3($rawFiles,$subPath);
	}
} 
 
?>
