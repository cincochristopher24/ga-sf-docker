<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoProfile.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoBYLocation extends AbstractPhoto{
	
	private $LOCATIONID	 	= 0;
	private $PHOTOcontext		= NULL;
	public  $NUMPHOTOSTOUPLOAD 	= 5;
	static  $photos 			= array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->LOCATIONID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		require_once('travellog/model/Class.Travel.php'); 
		
		try{
		  	$travel = new Travel();
			$travel->setLocationID($id);
			self::$photos	= Travel::getPhotosInLocation($id);
			$this->PHOTOcontext = $travel;
		}
		catch(Exception $e){
		}
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	function getPhotos(){
		return self::$photos;	
	}
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){
		return false;
	}
		
	function getHeaderCaption(){
		$caption = ($this->getCOUNTPhotos()>1)?"photos":"photo";
		$caption = $caption. " in ". $this->PHOTOcontext->getlocation()->getname();
		return $caption;
	}
	
	function getBackLink(){
		return "/destination.php?view&countryID=".$this->PHOTOcontext->getlocation()->getCountryID();
	} 
	
	function getBackCaption(){
		return $this->PHOTOcontext->getlocation()->getname(). " info";
	} 
	function getNorecordlabel(){
		if(!count(self::$photos))
			return "There are no Photos in ".$this->PHOTOcontext->getlocation()->getname()."!";		
		
	} 
	
	function getSUBNavigation($loginID = NULL){
		require_once("travellog/model/Class.SubNavigation.php");
		$subNavigation = new SubNavigation();
		return $subNavigation;
	}
} 
 
?>
