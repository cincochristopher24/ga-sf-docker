<?php

/*
 *  ClassThickBoxLayout.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.ThickBoxLayout .
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */

require_once('travellog/UIComponent/Photo/abstract/AbstractPhotoLayout.php');
require_once('travellog/UIComponent/Photo/interface/IPhotoLayout.php');

class ThickBoxLayout extends PhotoLayout implements IPhotoLayout {
	
	private $filenames 	= array();
	private $pagenum 		= 1;
	private $startrow 		= 1;
	private $isadmin		= false;
	private $parentData 	= array();
	private $photos			= array();
	
    /**
     *
     * @param $filenames, array Filenames of Photos   
     */
    function loadPhotos(PhotoFactory $PhotoFactory){	
    	
    	$this->PhotoCONTEXT = $PhotoFactory->create();
    	
    	/*
    	if(isset($_SESSION['isSiteAdminLogin']))
    		$this->isadmin = true;
    	else
    		$this->isadmin = $PhotoFactory->create()->isOwner($this->parentData['loginid']);
    	*/
    	
    	$this->isadmin = false;
    	
    	//$this->photos = $this->PhotoCONTEXT->getPhotos();
    	
    	/*
    	//filter photos
		if($this->isadmin || $this->parentData['context'] !== "travellog"){
			$this->photos = $photos;
		}else{
			$this->photos = $this->PhotoCONTEXT->getNotPrimaryPhotos();
		}	
    	*/
    	
    	
    	$photos = $PhotoFactory->create()->getPhotos();
    	if($photos){
			foreach($photos as $p){
					if(!$p->getPrimaryPhoto())
						$this->photos[] = $p;
			}
		}
		
    }
    
    function getPhotoCONTEXT(){
    	return $this->PhotoCONTEXT;
    }
	
	function getPhotos(){
    	return $this->photos;
    }
    
    function getPhotoFileNames(){
    	return $this->filenames;
    }
    function setPagenum($pagenum){
    	$this->pagenum = $pagenum;
    }
    function getPagenum(){
    	return $this->pagenum;
    }
    function setStartrow($startrow){
    	$this->startrow = $startrow;
    }
    function getStartrow(){
    	return $this->startrow;
    }
    
    function setIsAdmin($isadmin){
    	$this->isadmin = $isadmin;
    }
    
    function getIsAdmin(){
    	return $this->isadmin;
    }
    
    function setParentData($data){
    	$this->parentData = $data;
    }
    
    function getParentData(){
    	return $this->parentData;
    }
    
    /**
	 * Create LightBox Layout
	 * @return LightBox Layout Template 
	 */
	function createLayout(){
		require_once('travellog/UIComponent/Photo/model/Class.ThickboxLayoutTemplate.php');
		return new ThickboxLayoutTemplate($this);
	}
	
	
}