<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoTravellog.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoJournal extends AbstractPhoto{
	
	private $TRAVELLOGID	 		= 0;
	private $PHOTOcontext		= NULL;
	public $NUMPHOTOSTOUPLOAD 	= 5;
	static $photos = array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->TRAVELLOGID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		
		require_once 'Class.Connection.php';
		require_once 'Class.Recordset.php';
		
		$conn = new Connection();
		$rs   = new Recordset($conn);
		
		$sql = "SELECT tblTravel.travelerID, tblTravel.primaryphoto, tblTravel.title as travel_title, tblTravel.*, tblTravelLog.*, tblPhoto.*, tblTravelLogtoPhoto.position FROM tblTravel,  tblTravelLog, tblPhoto, tblTravelLogtoPhoto 
 				   	WHERE tblTravel.travelID = $id
 				  	AND tblTravel.travelID = tblTravelLog.travelID
 				 	AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID
 					AND tblTravelLog.deleted = 0
 				 	AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID 
 				 	GROUP BY tblPhoto.photoID
 				   	ORDER BY tblTravelLogtoPhoto.position DESC"; 
 		$rs->Execute($sql); 
		
		require_once('travellog/model/Class.TravelLog.php'); 
		
		$photo = array();
		if( 0 == $rs->Recordcount() ){
			$this->PHOTOcontext = new TravelLog($this->TRAVELLOGID);
		}else{
			while( $row = mysql_fetch_assoc($rs->Resultset()) ){
					$TravelLog = new TravelLog(0,$row);
					$photo[] = new Photo($TravelLog,0,$row);
					$this->PHOTOcontext = $TravelLog;
			}
		}			
		self::$photos  = $photo;
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	function getPhotos(){
		return self::$photos;	
	}
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){
		return false;
	}
		
	
} 
 
?>
