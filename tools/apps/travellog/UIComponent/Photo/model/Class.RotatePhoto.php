<?php
/*
 * Created on Nov 13, 2007
 * Class.RotatePhoto.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 class RotatePhoto{
 	 
	 private $classcontext = NULL; 	 
 	 
 	 function __construct(PhotoFactory $Pfac){
 	 	$this->classcontext = $Pfac;
 	 }
 	 
 	 
	 function rotate($photoID,$rotation){
	 		
 			require_once('travellog/model/Class.UploadManager.php');
 			require_once('travellog/model/Class.PathManager.php');
 			require_once('travellog/model/Class.Photo.php');	
			require_once 'Image/Transform.php';
			require_once('travellog/model/Class.PhotoSizes.php');
			require_once('travellog/model/Class.ImageBuilder.php');
			
			$Photo =  new Photo($this->classcontext->create()->getPHOTOcontext(),$photoID);
			
			$filename = $_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."fullsize".$Photo->getFilename();
			
			$img = getimagesize($filename);													
											
			switch(substr($img['mime'],6,4)){
				case "jpeg":
						$ufilename = uniqid().time().".jpg";						
				break;										
				case "gif":
						$ufilename = uniqid().time().".gif";															
				break;										
				case "png":
						$ufilename = uniqid().time().".png";						
				break;				
														
			}
			
			
			if(file_exists($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto().$Photo->getFileName().".txt")){
				$current_angle = file_get_contents($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto().$Photo->getFileName().".txt");
				
				if($rotation == 'right'){
					$angle =$current_angle+90;
					if($angle > 360){
						$angle = 90;
					}
				}else{
					$angle =$current_angle-90;
					if($angle ==0){
						$angle = 360;
					}
				}
				
				$i = Image_Transform::factory('IM');				
				$i->load($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."orig-".$Photo->getFileName());
				$i->fit(500,500);
				
				$i->rotate($angle);
				$i->save($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."fullsize".$ufilename);
				
			}else{
				if($rotation == 'right'){
					$angle =90;
				}else{
					$angle =270;
				}
				
				$i = Image_Transform::factory('IM');				
				$i->load($filename);
				$i->rotate($angle);
				$i->save($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."fullsize".$ufilename);
					
			}
			
			$path = new PathManager($this->classcontext->create()->getPHOTOcontext());	
		
			$uploadmanager = new UploadManager();
			$uploadmanager->setContext($this->classcontext->getContext());	
			$uploadmanager->setTypeUpload('image');
			$uploadmanager->setDestination($path);
						
			$ImgBuilder = ImageBuilder::getInstance();													
			$ImgBuilder->setFilename($ufilename);
			$ImgBuilder->setImageFile($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."fullsize".$ufilename);
			$ImgBuilder->setDestination($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto());
			$ImgBuilder->bake($uploadmanager);
			
			
								
			if(file_exists($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."orig-".$Photo->getFileName())){
				copy($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."orig-".$Photo->getFileName(),$_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."orig-".$ufilename);
			}else{
				if(file_exists($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."fullsize".$Photo->getFileName())){	
					copy($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."fullsize".$Photo->getFileName(),$_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto()."orig-".$ufilename);
				}
			}
			
			$filelocation=$ufilename.".txt"; 
			$newfile = fopen($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto().$filelocation,"a+"); 
			fwrite($newfile, $angle); 
			fclose($newfile); 
			
			$uploadmanager->deletefile($Photo->getFileName());
			
			$Photo->setFilename($ufilename);												
			$Photo->Update();
							
	}
	 
 } 
 
?>
