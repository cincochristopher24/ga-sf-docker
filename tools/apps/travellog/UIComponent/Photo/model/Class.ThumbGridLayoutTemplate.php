<?

/*
 *  Class.ThumbGridLayoutTemplate.php  2009-01-08 00:00:00Z root $
 */


class ThumbGridLayoutTemplate{
	
    /**
	 * 
	 * @return String 
	 */
	
	private $template = NULL;
	 
   	function __construct(ThumbGridLayout $thumbgridlayout){
   		
   		require_once('travellog/model/nPaging.php');
		require_once('Class.Template.php');	
		
		$PhotoCONTEXT = $thumbgridlayout->getPhotoCONTEXT();
		$Photos = $thumbgridlayout->getPhotos();
		
		$photocount = count($Photos);
		$parentData = $thumbgridlayout->getParentData();
		
		
		$this->template = new Template();
		$this->template->set_path('travellog/UIComponent/Photo/views/');
		
		$this->template->set('photos', $Photos);
		$this->template->set('photocount', $photocount);
		$this->template->set('parentData', $parentData);
		$this->template->set('isOwner', $thumbgridlayout->getIsAdmin());
		$this->template->set('PhotoCONTEXT', $PhotoCONTEXT);
		
		$current_full_photo				= array();
		
		if($photocount > 0 ){
				if($parentData['pID']==0){
					$current_full_photo	= $Photos[0];
				}else{
					for($i=0;$i<$photocount;$i++){
						if($Photos[$i]->getPhotoID() ==$parentData['pID']){
							$current_full_photo	= $Photos[$i];
							break;
						}
					}
				}	
		}
		
		$this->template->set('currentPhoto',$current_full_photo);
	}
	
	function render(){
		return $this->template->fetch('tpl.ThumbGrid.php');
	}
		
}

