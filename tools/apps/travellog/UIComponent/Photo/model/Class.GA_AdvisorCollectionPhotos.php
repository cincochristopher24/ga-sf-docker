<?php
/*
 * Created on Oct 23, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
class  GA_AdvisorCollectionPhotos{
	
		static $photos		= array();
		static $travelerID	= 0;
		
		function GA_AdvisorCollectionPhotos($travelerID){
				self::$travelerID = $travelerID;
		}  
		
		function doAlbum(){
			require_once('travellog/model/Class.Advisor.php');	
			require_once('travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php');
			
			$Advisor = new Advisor(self::$travelerID);
			$group =$Advisor->getAdvisorGroup();
			/*
			$GRPAlbum	= $group->getPhotoAlbums();
			if(count($GRPAlbum)>0){
				foreach($GRPAlbum as $i => $palbum ){
					$GaCollectionDTO = new GaCollectionDTO();
					$GaCollectionDTO->setGenID($palbum->getPhotoalbumid());
					$GaCollectionDTO->setTitle($palbum->getTitle());
					$GaCollectionDTO->setContext('photoalbum');
					$GaCollectionDTO->setPrimaryPhoto($palbum->getPrimaryPhoto());
					$GaCollectionDTO->setNumPhotos($palbum->getCountPhotos());
					self::$photos[] = $GaCollectionDTO;
					
				}
			}
			*/
			
			
			require_once('travellog/model/Class.PhotoAlbumAdapter.php');
			$PhotoAlbumAdapter = new PhotoAlbumAdapter();
			$PhotoAlbumAdapter->setLoguserID(0);
			$GRPAlbum	= $PhotoAlbumAdapter->getAlbums($group);
			
			
			
			if(count($GRPAlbum)>0){
				foreach($GRPAlbum as $i => $palbum ){
					
					
						$creator 				= $palbum["creator"];
						$context 				= $palbum["context"];
						$genID					= $palbum["genID"];
						$title					= $palbum["title"];
						$numphotos			= $palbum["numphotos"];
						$publish				= $palbum["publish"];
						$primaryphoto		= $palbum["primaryphoto"];
					
						if($numphotos > 0){
							
						
								$GaCollectionDTO = new GaCollectionDTO();
								$GaCollectionDTO->setGenID($genID);
								$GaCollectionDTO->setTitle($title);
								$GaCollectionDTO->setContext($context);
								$GaCollectionDTO->setPrimaryPhoto($primaryphoto);
								$GaCollectionDTO->setNumPhotos($numphotos);
								self::$photos[] = $GaCollectionDTO;
						}
				}
			}
			
				
		}
		
		function doJournal(){
				
				$travelerID  = self::$travelerID;
				
				require_once 'Class.Connection.php';
 				require_once 'Class.Recordset.php';
				
				$conn = new Connection();
				$rs   = new Recordset($conn);
				
				$sql = "SELECT tblTravel.travelerID, tblTravel.primaryphoto, tblTravel.title as travel_title, tblTravelLog.*, tblPhoto.*, tblTravelLogtoPhoto.position FROM tblTravel,  tblTravelLog, tblPhoto, tblTravelLogtoPhoto 
		 				   	WHERE tblTravel.travelerID = $travelerID
		 				  	AND tblTravel.travelID = tblTravelLog.travelID
							AND tblTravelLog.deleted = 0
		 				 	AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID 
		 				 	AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID 
		 				 	GROUP BY tblPhoto.photoID
		 				   	ORDER BY tblTravelLog.travellogID DESC"; 
		 		$rs->Execute($sql); 
				
				$tmp_holder 			= array();
				$holder 			= array();
				
				if( $rs->Recordcount() ){
					require_once('travellog/model/Class.TravelLog.php');	
					require_once('travellog/model/Class.Photo.php');	
					require_once('travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php');	
					//filter
					while( $row = mysql_fetch_assoc($rs->Resultset()) ){
							$tmp_holder[$row['travelID']][$row['photoID']] = $row;
							if(!in_array($row['travelID'],$holder)){			
								$TravelLog = new TravelLog(0,$row);
								
								$current_photos	= $tmp_holder[$row['travelID']];
								if($row['primaryphoto']>0){
										$photo = new Photo($TravelLog,0,$row);
								}else{
										$random_photos = array_rand($current_photos);
										$photo = new Photo($TravelLog,0,$current_photos[$random_photos]);
								}
								
								$GaCollectionDTO = new GaCollectionDTO();
								$GaCollectionDTO->setGenID($row['travelID']);
								$GaCollectionDTO->setTitle($row['travel_title']);
								$GaCollectionDTO->setContext('journal');
								$GaCollectionDTO->setPrimaryPhoto($photo);
								$GaCollectionDTO->setNumPhotos(count($tmp_holder[$row['travelID']]));
								self::$photos[] = $GaCollectionDTO;
								
						}
						$holder[] = $row['travelID'];
					}
					/*
					$rs->Execute($sql); 
					while( $row = mysql_fetch_assoc($rs->Resultset()) ){
						if(!in_array($row['travelID'],$holder)){			
								$TravelLog = new TravelLog(0,$row);
								
								$current_photos	= $tmp_holder[$row['travelID']];
								if($row['primaryphoto']>0){
										$photo = new Photo($TravelLog,0,$row);
								}else{
										$random_photos = array_rand($current_photos);
										$photo = new Photo($TravelLog,0,$current_photos[$random_photos]);
								}
								
								$GaCollectionDTO = new GaCollectionDTO();
								$GaCollectionDTO->setGenID($row['travelID']);
								$GaCollectionDTO->setTitle($row['travel_title']);
								$GaCollectionDTO->setContext('journal');
								$GaCollectionDTO->setPrimaryPhoto($photo);
								$GaCollectionDTO->setNumPhotos(count($tmp_holder[$row['travelID']]));
								self::$photos[] = $GaCollectionDTO;
								
						}
						$holder[] = $row['travelID'];
					}
					*/
				}
		}
		
		private static $photosLoaded = FALSE;
		function loadCollection(){
			if( !self::$photosLoaded ){
				$this->doAlbum();
				self::$photosLoaded = TRUE;
			}
			//$this->doJournal();
			return self::$photos;
		}
		
		function loadGallery(){
			
				if(count(self::$photos)>0){
								
						require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
						require_once('travellog/UIComponent/Photo/factory/Class.PhotoLayoutFactory.php');
						
						$PLayout = PhotoLayoutFactory::create(1);	
								
						foreach(self::$photos as $photo){
								$Pfactory = new PhotoFactory($photo->getGenID(),$photo->getContext());
								
								$parentData = array();
								$parentData['context'] 	= $photo->getContext();
								$parentData['loginid'] 		= 0;
								$parentData['genid'] 		= $photo->getGenID();
								$parentData['pID'] 			= 0;
								
								$PLayout->setParentData($parentData);
								$PLayout->loadPhotos($Pfactory);
								break;
						}
						return $PLayout->createLayout()->render();
				}
			
		}
	
		function loadCacheGallery(){
			
				if(count(self::$photos)>0){
								
						require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
						require_once('travellog/UIComponent/Photo/factory/Class.PhotoLayoutFactory.php');
						
						foreach(self::$photos as $photo){
								$Pfactory = new PhotoFactory($photo->getGenID(),$photo->getContext());
								$parentData = array();
								$parentData['context'] 	= $photo->getContext();
								$parentData['loginid'] 		= 0;
								$parentData['genid'] 		= $photo->getGenID();
								$parentData['pID'] 			= 0;
								$parentData['layout'] 		= 1;
								
								require_once("travellog/UIComponent/Photo/command/Class.PhotoCacheTOjsCommand.php");
								PhotoCacheTOjsCommand::doCache($Pfactory,$parentData);
								break;
						}
				}
			
		}
	
}

 
?>

