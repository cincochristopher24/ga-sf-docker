<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoProfile.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoProgram extends AbstractPhoto{
	
	private $PROGRAMID = 0;
	private $PHOTOcontext		= NULL;
	public $NUMPHOTOSTOUPLOAD 	= 5;
	static $photos = array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->PROGRAMID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		require_once('travellog/model/Class.Program.php');
		
		try{
		  	$program		= new Program($id);
			self::$photos	= $program->getPhotos();
			$this->PHOTOcontext =$program;
		}
		catch(Exception $e){
		}
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	function getPhotos(){
		return self::$photos;	
	}
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){
		require_once('travellog/model/Class.AdminGroup.php');
		$group		 = new AdminGroup($this->PHOTOcontext->getGroupID());						
		$travelerID = $group->getAdministrator()->gettravelerID();
		
		if($travelerID == $loginID)
			return true;
		else
			return false;
	}
		
	function getHeaderCaption(){
		return $this->PHOTOcontext->getTitle();
	}
	
	function getBackLink(){
		return "/program.php?page=1@1&action=view&gID=".$this->PHOTOcontext->getGroupID()."&pID=".$this->PROGRAMID;
	} 
	
	function getBackCaption(){
		return "Itineraries";
	} 
	function getNorecordlabel(){
		if(!count(self::$photos))
			return "There are no Photos in Itineraries!";	
	}
	
	function getSUBNavigation(SubNavigation $SUbnav, $loginID = NULL){
		$SUbnav->setContextID($this->PHOTOcontext->getGroupID());
		$SUbnav->setContext('GROUP');
		$SUbnav->setGroupType('ADMIN_GROUP');									
		$SUbnav->setLinkToHighlight('ITINERARIES');	
		
		return $SUbnav;
	} 
	
	function createImage(ImageBuilder $ImgBuilder){
		
		$ImgBuilder->create('fullsize',500,500);
							
							
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'fullsize'.$ImgBuilder->getFilename());								
		$ImgBuilder->crop_me('standard',164,123);
		$ImgBuilder->crop_me('featured',164,123);
		
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'standard'.$ImgBuilder->getFilename());	
		$ImgBuilder->crop_me('thumbnail',65,65);
		
		//copy orig file to destination if new upload file not rotate
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename())){
			rename($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename(),$ImgBuilder->getDestination()."orig-".$ImgBuilder->getFilename());
			
			//create txt file for rotating photo purpose.
			$filelocation=$ImgBuilder->getFilename().".txt"; 
			$newfile = fopen($ImgBuilder->getDestination().$filelocation,"a+"); 
			fwrite($newfile, '360'); 
			fclose($newfile); 
		}	
	}
} 
 
?>
