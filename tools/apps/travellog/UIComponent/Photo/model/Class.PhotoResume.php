<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoProfile.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoResume extends AbstractPhoto{
	
	private $TRAVELERID	 	= 0;
	private $PHOTOcontext		= NULL;
	public $NUMPHOTOSTOUPLOAD 	= 1;
	static $photos = array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->TRAVELERID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		require_once('travellog/model/Class.Resume.php');
		
		try{
		  	$resume		= new Resume($id);
			self::$photos	= $resume->getPhotos();
			$this->PHOTOcontext =$resume;
		}
		catch(Exception $e){
		}
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	function getPhotos(){
		return self::$photos;	
	}
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){
		if($this->TRAVELERID == $loginID)
			return true;
		else
			return false;
	}
		
	function getHeaderCaption(){
		require_once('travellog/model/Class.TravelerProfile.php');	
		$x = count(self::$photos)>1? "photos": "photo";
		$profile	= new TravelerProfile($this->TRAVELERID);									
		return  $profile->getUserName()."'s  Resume  ".$x;
	}
	
	function getBackLink(){
		return "/resume.php?action=view";
	} 
	
	function getBackCaption(){
		require_once('travellog/model/Class.TravelerProfile.php');	
		$profile	= new TravelerProfile($this->TRAVELERID);	
		return $profile->getUserName()."'s  "."Resume";
	} 
	
	function getNorecordlabel(){
		if(!count(self::$photos))
			return "There are no Photos in Resume!";	
		
	} 
	
	function getSUBNavigation(SubNavigation $SUbnav, $loginID = NULL){
		if($loginID == $this->TRAVELERID)									
			$SUbnav->setContextID($this->TRAVELERID);
			$SUbnav->setContext('TRAVELER');
			$SUbnav->setLinkToHighlight('RESUME');
		
		return $SUbnav;
	}
	
	function createImage(ImageBuilder $ImgBuilder){
		
		$ImgBuilder->create('fullsize',500,500);
							
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'fullsize'.$ImgBuilder->getFilename());								
		$ImgBuilder->crop_me('standard',150,200);
		$ImgBuilder->crop_me('featured',164,123);
		
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'standard'.$ImgBuilder->getFilename());	
		$ImgBuilder->crop_me('thumbnail',65,65);
		
		//copy orig file to destination if new upload file not rotate
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename())){
			rename($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename(),$ImgBuilder->getDestination()."orig-".$ImgBuilder->getFilename());
			
			//create txt file for rotating photo purpose.
			$filelocation=$ImgBuilder->getFilename().".txt"; 
			$newfile = fopen($ImgBuilder->getDestination().$filelocation,"a+"); 
			fwrite($newfile, '360'); 
			fclose($newfile); 
		}	
		 
	}
} 
 
?>
