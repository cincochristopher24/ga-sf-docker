<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoProfile.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoTravelerGroup extends AbstractPhoto{
	
	private $PROFILEID	 		= 0;
	private $PHOTOcontext		= NULL;
	public  $NUMPHOTOSTOUPLOAD 	= 5;
	static  $photos 			= array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->PROFILEID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		require_once('travellog/model/Class.TravelerProfile.php');
		
		try{
		  	$profile		= new TravelerProfile($id);
			self::$photos	= $profile->getTravelerGroupPhotos();
			
			$fungroup		= $profile->getTraveler()->getGroups();
			
			if($fungroup){
				$photocontext   = $fungroup[0]; //to return funGroup Object Context
				$this->PHOTOcontext = $photocontext;
			}
		}
		catch(Exception $e){
		}
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	function getPhotos(){
		return self::$photos;	
	}
	
	/**
	 * Traveler Profile Photos
	 */
	function getTravelerProfilePhotos(){
		$profile = new TravelerProfile($this->PROFILEID);
		return $profile->getPhotos();	
	}
	/**
	 * Traveler Journal Entries Photos
	 */
	function getTravelerEntriesPhotos(){
		$profile = new TravelerProfile($this->PROFILEID);
		return $profile->getTravelerJournalEntriesPhotos();	
	}
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){
		if($this->PROFILEID == $loginID)
			return true;
		else
			return false;
	}
		
	function getHeaderCaption(){
		return $this->PHOTOcontext->getName();
	}
	
	function getBackLink(){
		return "/group.php?gID=".$this->PHOTOcontext->getGroupid();
	} 
	
	function getBackCaption(){
		if(strlen($this->PHOTOcontext->getName())-1 == strripos($this->PHOTOcontext->getName(),'s'))		// edited by daf feb.27.07
			return $this->PHOTOcontext->getName()."'  Group";	
		else
			return $this->PHOTOcontext->getName()."'s  Group";	
	} 
	function getNorecordlabel(){
		if(!count(self::$photos))
			return "There are no Photos in Group/Club!";	
		
	}
	
	function getSUBNavigation(SubNavigation $SUbnav, $loginID = NULL){
		$SUbnav->setContextID($this->PHOTOcontext->getGroupid());
		$SUbnav->setContext('GROUP');
		$SUbnav->setGroupType('FUN_GROUP');									
		$SUbnav->setLinkToHighlight('GROUP_NAME');
		
		return $SUbnav;
	}
	
	function createImage(ImageBuilder $ImgBuilder){
		
		$ImgBuilder->create('fullsize',500,500);
							
							
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'fullsize'.$ImgBuilder->getFilename());								
		$ImgBuilder->crop_me('standard',164,123);
		$ImgBuilder->crop_me('featured',164,123);
		
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'standard'.$ImgBuilder->getFilename());	
		$ImgBuilder->crop_me('thumbnail',65,65);
		
		//copy orig file to destination if new upload file not rotate
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename())){
			rename($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename(),$ImgBuilder->getDestination()."orig-".$ImgBuilder->getFilename());
			
			//create txt file for rotating photo purpose.
			$filelocation=$ImgBuilder->getFilename().".txt"; 
			$newfile = fopen($ImgBuilder->getDestination().$filelocation,"a+"); 
			fwrite($newfile, '360'); 
			fclose($newfile); 
		}	 
	}
	
} 
 
?>
