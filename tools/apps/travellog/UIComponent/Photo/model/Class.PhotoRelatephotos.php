<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoProfile.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoRelatephotos extends AbstractPhoto{
	
	private $TRAVELID	 		= 0;
	private $PHOTOcontext		= NULL;
	public  $NUMPHOTOSTOUPLOAD 	= 5;
	static  $photos 			= array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->TRAVELID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		
		require_once("travellog/model/Class.Travel.php");
		
		try{
		  	
		  	$travel = new Travel ($id);
			$travellog = array();
			require_once("travellog/model/Class.TravelLog.php");	
			$hasentries			= array();
			$related_photos		= array();
			
			if(count($travel->getTrips())){
				foreach($travel->getTrips() as $trip){
					if(count($trip->getTravelLogs())){
						foreach($trip->getTravelLogs() as $travellog){
							$hasentries[] = $travellog;
							if(count($travellog->getPhotos())){
								foreach($travellog->getPhotos() as $photo)
									$related_photos[] = $photo;
							}
						}
					}
				}
			}
			
			self::$photos	= $related_photos;	
			$photocontext   = $hasentries[0]; //to return TravelLog Object Context
			$this->PHOTOcontext = $photocontext;
		}
		catch(Exception $e){
		}
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	function getPhotos(){
		return self::$photos;	
	}
	
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){
		return false;
	}
		
	function getHeaderCaption(){
		if($this->PHOTOcontext->getTraveler()->isAdvisor())
			return $this->PHOTOcontext->getOwner()->getName();
		else
			return $this->PHOTOcontext->getTraveler()->getUserName();
	}
	
	function getBackLink(){
		return "/travellog.php?action=view&travellogID=".$this->TRAVELLOGID;
	} 
	
	function getNorecordlabel(){
		if(!count(self::$photos))
			return "There are no Photos in Journal!";	
		
	} 
	
		
} 
 
?>
