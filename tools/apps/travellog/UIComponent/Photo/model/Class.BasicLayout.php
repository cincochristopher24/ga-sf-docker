<?php

/*
 *  ClassBasicLayout.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.BasicLayout .
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */

require_once('travellog/UIComponent/Photo/abstract/AbstractPhotoLayout.php');
require_once('travellog/UIComponent/Photo/interface/IPhotoLayout.php');

class BasicLayout extends PhotoLayout implements IPhotoLayout {
	
	private $filenames  	= array();
	private $pagenum 		= 1;
	private $startrow 		= 1;
    private $isadmin		= false;
    private $parentData 	= array();
    private $photos			= array();
    
    private $PhotoCONTEXT	= NULL; 
    
    /**
     *
     * @param $filenames, array Filenames of Photos   
     */
    function loadPhotos(PhotoFactory $PhotoFactory){	
    	
    	$this->PhotoCONTEXT = $PhotoFactory->create();
    	
    	/*
    	echo "<pre>";
    		print_r($this->parentData);
    	echo "</pre>";
    	*/
    	
    	if(isset($_SESSION['isSiteAdminLogin'])){
    		$this->isadmin = true;
    	}else{
    		
    		//$this->parentData['loginid'] = isset($_SESSION['travelerID'])?$_SESSION['travelerID']:0;
    		$this->isadmin = $PhotoFactory->create()->isOwner($this->parentData['loginid']);
    	}
    	
    	$this->photos = $this->PhotoCONTEXT->getPhotos();
    	
    	/*
    	$photos = $this->PhotoCONTEXT->getPhotos();  
    	//filter photos
		if($this->isadmin || $this->parentData['context'] !== "travellog"){
			$this->photos = $photos;
		}else{
			$this->photos = $this->PhotoCONTEXT->getNotPrimaryPhotos();
		}	
		*/
		
		
		/*
		echo "<pre>";
			print_r($photos);
		echo "</pre>";	
		*/	
			/*
			foreach($photos as $p){
				if($this->isadmin){
					$this->filenames[] = array('fullsizephoto'=>$p->getPhotoLink('fullsize'),'standardsizephoto'=>$p->getPhotoLink('standard'),'thumbsizephoto'=>$p->getPhotoLink('thumbnail'),'caption'=>$p->getCaption(),'pID' =>$p->getphotoid(),'primary' =>$p->getPrimaryPhoto());
				}else{
					if($this->parentData['context'] == "travellog"){
						if(!$p->getPrimaryPhoto()){
							$this->filenames[] = array('fullsizephoto'=>$p->getPhotoLink('fullsize'),'standardsizephoto'=>$p->getPhotoLink('standard'),'thumbsizephoto'=>$p->getPhotoLink('thumbnail'),'caption'=>$p->getCaption(),'pID' =>$p->getphotoid(),'primary' =>$p->getPrimaryPhoto());
						}
					}else{
						$this->filenames[] = array('fullsizephoto'=>$p->getPhotoLink('fullsize'),'standardsizephoto'=>$p->getPhotoLink('standard'),'thumbsizephoto'=>$p->getPhotoLink('thumbnail'),'caption'=>$p->getCaption(),'pID' =>$p->getphotoid(),'primary' =>$p->getPrimaryPhoto());
					}
				}
			}
			*/
		
	
		
		
		
	}
	
	function getPhotoCONTEXT(){
    	return $this->PhotoCONTEXT;
    }
	
	function getPhotos(){
    	return $this->photos;
    }
	
    function getPhotoFileNames(){
    	return $this->filenames;
    }
    function setPagenum($pagenum){
    	$this->pagenum = $pagenum;
    }
    function getPagenum(){
    	return $this->pagenum;
    }
    function setStartrow($startrow){
    	$this->startrow = $startrow;
    }
    function getStartrow(){
    	return $this->startrow;
    }
    
    function setIsAdmin($isadmin){
    	$this->isadmin = $isadmin;
    }
    
    function getIsAdmin(){
    	return $this->isadmin;
    }
    
    function setParentData($data){
    	$this->parentData = $data;
    }
    
    function getParentData(){
    	return $this->parentData;
    }
    
    /**
	 * Create LightBox Layout
	 * @return LightBox Layout Template 
	 */
	function createLayout(){
		require_once('travellog/UIComponent/Photo/model/Class.BasicLayoutTemplate.php');
		return new BasicLayoutTemplate($this);
	}
	
	
}