<?php

/*
 *  ClassThickBoxLayout.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.TinyBoxLayout.
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */

require_once('travellog/UIComponent/Photo/abstract/AbstractPhotoLayout.php');
require_once('travellog/UIComponent/Photo/interface/IPhotoLayout.php');

class TinyBoxLayout extends PhotoLayout implements IPhotoLayout {
	
	private $filenames 	= array();
	private $pagenum 		= 1;
	private $startrow 		= 1;
	private $isadmin		= false;
	private $parentData 	= array();
	
    /**
     *
     * @param $filenames, array Filenames of Photos   
     */
    function loadPhotos(PhotoFactory $PhotoFactory){	
    	$photos = $PhotoFactory->create()->getPhotos();
    	if($photos){
			foreach($photos as $p){
				$this->filenames[] = array('fullsizephoto'=>$p->getFullsizePhotoLink(),'standardsizephoto'=>$p->getPhotoLink('standard'),'thumbsizephoto'=>$p->getGalleryThumbnailPhotoLink(),'caption'=>$p->getCaption(),'pID' =>$p->getphotoid(),'primary' =>$p->getPrimaryPhoto());
			}
		}
    }
    function getPhotoFileNames(){
    	return $this->filenames;
    }
    function setPagenum($pagenum){
    	$this->pagenum = $pagenum;
    }
    function getPagenum(){
    	return $this->pagenum;
    }
    function setStartrow($startrow){
    	$this->startrow = $startrow;
    }
    function getStartrow(){
    	return $this->startrow;
    }
    
    function setIsAdmin($isadmin){
    	$this->isadmin = $isadmin;
    }
    
    function getIsAdmin(){
    	return $this->isadmin;
    }
    
    function setParentData($data){
    	$this->parentData = $data;
    }
    
    function getParentData(){
    	return $this->parentData;
    }
    
    /**
	 * Create LightBox Layout
	 * @return LightBox Layout Template 
	 */
	function createLayout(){
		require_once('travellog/UIComponent/Photo/model/Class.TinyBoxLayoutTemplate.php');
		return new TinyBoxLayoutTemplate($this);
	}
	
	
}