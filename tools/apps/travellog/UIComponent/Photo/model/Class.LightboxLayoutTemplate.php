<?

/*
 *  ClassLightboxLayoutTemplate.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.LightboxLayoutTemplate .
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */


class LightboxLayoutTemplate{
	
    /**
	 * 
	 * @return String 
	 */
	
	private $template = NULL;
	 
   	function __construct(LightBoxLayout $lightboxlayout){
   		
   		require_once('travellog/model/nPaging.php');
		require_once('Class.Template.php');	
		
		$PhotoFileNames = $lightboxlayout->getPhotoFileNames();
		
		$photocount = count($PhotoFileNames);
		
		Paging::Setajax(true);															
		$page = new Paging($photocount,$lightboxlayout->getStartrow(),$lightboxlayout->getPagenum(),4);
		$page->setJSname('PhotoService.gallerypage');
		
		$start	= $lightboxlayout->getStartrow() - 1;										
		$endrow	= ($lightboxlayout->getStartrow() + 4) -1;
		$endrow = $endrow > $photocount?  $photocount: $endrow;
		
		$this->template = new Template();
		$this->template->set_path('travellog/UIComponent/Photo/views/');
		
		/**
		 *get the fullsize
		 */
		//$fullphoto = str_replace("thumbnail", "fullsize", $PhotoFileNames[$start]);
		
		$this->template->set('start',$start);	
		$this->template->set('endrow',$endrow);
		$this->template->set('page',$page);
		$this->template->set('filenames', $PhotoFileNames);
		//$template->set('fullphoto', $fullphoto);
		$this->template->set('photocount', $photocount);
		
		//$tmp = $this->template->fetch('tpl.Lightbox.php');
		
		//return $tmp;
	}
	
	function render(){
		return $this->template->fetch('tpl.Lightbox.php');
	}
		
		
}

