<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


require_once('travellog/model/Class.AdminGroup.php');		


class ResourceFileRequestDROPVIEW{
	
	function __construct(FileFactory $FFactory){
		
		$template 	 = $FFactory->getClass('Template');
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$_REQUEST['travelerID'] = $travelerID;
		
		$subgroup 		= new AdminGroup($_REQUEST['subgID']);
		 
		$template->set('subgroup',$subgroup);
		$template->set('reload',false);
	 	
	 	echo $template->out('tpl.incViewManageResourceFilesSubgroups.php');		
	}
	
}


?>
