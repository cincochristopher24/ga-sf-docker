<?php
/*
 * Created on Nov 23, 2009
 * Class.ResourceFileRequestUPDATE.php
 * 
 * @author 
 * @version: 
 * @package: 
 */

require_once('travellog/model/Class.Traveler.php');
require_once('travellog/service/ResourcefileService.php');
require_once('Class.Template.php');
require_once("travellog/model/Class.ResourceFiles.php");
require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
require_once("travellog/model/Class.UploadManager.php");
require_once('travellog/model/Class.PathManager.php');
require_once("travellog/UIComponent/Resourcefiles/model/Class.ParsePERLUploadParams.php");
require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileManager.php");

class ResourceFileRequestUPDATE{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		
		$category       = isset($_REQUEST['cat']) ? $_REQUEST['cat'] : 'admingroup';
		
		$resourcefileID = isset($_REQUEST['resourcefileID']) ? $_REQUEST['resourcefileID'] : 0;
		$genID          = isset($_REQUEST['genID']) ? $_REQUEST['genID'] : 0;
		$caption        = isset($_REQUEST['caption']) ? $_REQUEST['caption'] : '';
		$assignedGroups = isset($_REQUEST['group_select_list']) ? $_REQUEST['group_select_list'] : null;
		
		$fileprvpref    = isset($_REQUEST['fileprvpref']) ? $_REQUEST['fileprvpref'] : null;
		
		if(0 == $genID ){
			header("location: /index.php");
			exit;
		}
		
		if(0 == $travelerID || 0 == $resourcefileID){
			header("location: /resourcefiles.php?action=view&cat=$category&genID=$genID");
			exit;
		}
		
		// insert code for logger system
		require_once('gaLogs/Class.GaLogger.php');
		GaLogger::create()->start($travelerID,'TRAVELER');
		
		$resourceFile = new ResourceFiles($resourcefileID);
		
		$resourceFile->setCaption($caption);
		$resourceFile->setFileName($resourceFile->getFileName());
		$resourceFile->setPrivacypreference($fileprvpref);
		
		$resourceFile->Update();
		
		//must check if edit resource file is done in a subgroup or parent group
		//only allow editing of assigned groups from within parent group context
		$isParentGroup = FALSE;
		try{
			$currentGroup = new AdminGroup($genID);
			if( $currentGroup instanceof AdminGroup ){
				$isParentGroup = $currentGroup->isParent();
			}
		}catch(exception $e){
		}
		
		//TODO: put this in single query
		if( $isParentGroup ){
			$groups = $resourceFile->getAssignedGroups();
			$assignedGroups = array_unique($assignedGroups);
			foreach($groups as $group){
				$pos = array_search($group->getGroupID(), $assignedGroups);
				if(!$pos){
					$group->removeResourceFile($resourcefileID);
				}
				else{
					unset($assignedGroups[$pos]);
				}
			}
			if(0 < count($assignedGroups)){
				foreach($assignedGroups as $id){
					$adminGroup = new AdminGroup($id);
					$adminGroup->addResourceFile($resourceFile);
				}
			}
		}
		
		header("location: /resourcefiles.php?action=view&cat=$category&genID=$genID");
		
	}	
		
}


?>
