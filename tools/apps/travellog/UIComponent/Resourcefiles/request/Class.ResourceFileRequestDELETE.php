<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/ResourcefileService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");

class ResourceFileRequestDELETE{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
	   	
	    require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileManager.php");
	    
	   	$rfileID = isset($REQUEST_VAR['resourcefileID'])?$REQUEST_VAR['resourcefileID']:$REQUEST_VAR['rfileID'];
		
		$Rfactory = new ResourceFileFactory(isset($REQUEST_VAR['gID'])?$REQUEST_VAR['gID']:$REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
	    
	    ResourceFileManager::delete($Rfactory,$rfileID);
		
		if(isset($REQUEST_VAR['resourcefileID'])){
			new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
			$RFile = ResourceFileFactory::create();
			$RFile->setLoginID($travelerID);
			require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileLayoutFactory.php");
			ResourceFileLayoutFactory::create($RFile, $REQUEST_VAR);
		}else{
			header("Location:".$_SERVER['HTTP_REFERER']);	
		}	
	   	
	}
	
}


?>
