<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/ResourcefileService.php');

class ResourceFileRequestVIEW{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR = array()){
		
		$template 		= $FFactory->getClass('Template');
		$template->set_path("travellog/UIComponent/Resourcefiles/views/");
		
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$REQUEST_VAR['travelerID'] = $travelerID;
		
		require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
	    new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
	    $Rfactory = ResourceFileFactory::create();
	    
	   	/****************************************************
		 * edits by neri: 
		 * 		added code for displaying the profile component		11-06-08
		 ****************************************************/
	   
	    $profile_comp = $FFactory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		$profile_comp->init($REQUEST_VAR['genID']);
	    
		/***
			Added by naldz:
				Code to get the parent group name
		***/
		require_once('travellog/model/Class.GroupFactory.php');
		$parentGroup = GroupFactory::instance()->create(array($REQUEST_VAR['genID']));
		
		$isAdminLogged =	(0 < $sessMan->get('travelerID') AND ($parentGroup[0]->getAdministratorID() == $sessMan->get('travelerID') || $parentGroup[0]->isStaff($sessMan->get('travelerID')))) ? true : false;	
		$isMember = (0 < $sessMan->get('travelerID') AND $parentGroup[0]->isMember(new Traveler($sessMan->get('travelerID')))) ? true : false;
		
		$template->set('subNavigation',$Rfactory->getSUBNavigation($Subnavigation));
		$template->set('headercaption',$Rfactory->getHeaderCaption());
		$template->set('genID',$REQUEST_VAR['genID']);	
		$template->set('context',$REQUEST_VAR['cat']);
		$template->set('loginID',$travelerID);
		$template->set('isAdminLogged', $isAdminLogged);
		$template->set('isMember', $isMember);
		$template->set('layoutMain',$FFactory->getTemplate('LayoutMain'));
		$template->set('group',$REQUEST_VAR['cat'] =='admingroup'?$Rfactory->getRESOURCEFILEcontext()->getName():false);
		$template->set('profile', $profile_comp->get_view());
		$template->set('parentGroup',$parentGroup[0]);
		$template->set('copying_notice_component', new GanetGroupCopyingNoticeComponent($parentGroup[0]));
		$template->set('isAdmin',($parentGroup[0]->getAdministratorID() == $sessMan->get('travelerID')) ? true : false );
		
		$template->out('tpl.ViewResourceFiles.php');	
	}
	
}


?>
