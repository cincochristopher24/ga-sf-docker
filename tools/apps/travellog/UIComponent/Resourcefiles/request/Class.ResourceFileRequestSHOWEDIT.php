<?php
/*
 * Created on Nov 20, 2009
 * Class.ResourceFileRequestSHOWEDIT.php
 * 
 * @author  
 * @version: 
 * @package: 
 */

require_once('travellog/service/ResourcefileService.php');

class ResourceFileRequestSHOWEDIT{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR = array()){
		
		$template 		= $FFactory->getClass('Template');
		$template->set_path("travellog/UIComponent/Resourcefiles/views/");
		
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		$genericID      = isset($REQUEST_VAR['genID'])?$REQUEST_VAR['genID']:0;
		$category       = isset($REQUEST_VAR['cat'])?$REQUEST_VAR['cat']:'admingroup';
		$resourcefileID = isset($REQUEST_VAR['id'])?$REQUEST_VAR['id']:0;
		
		if(0 == $genericID ){
			header("location: /index.php");
			exit;
		}
		
		if(0 == $travelerID || 0 == $resourcefileID){
			header("location: /resourcefiles.php?action=view&cat=$category&genID=$genericID");
			exit;
		}
		
		/***
			Added by naldz:
				Code to get the parent group name
		***/
		require_once('travellog/model/Class.GroupFactory.php');
		$parentGroup = GroupFactory::instance()->create(array($genericID));
		
		$isAdminLogged = 0 < $travelerID AND ($parentGroup[0]->getAdministratorID() == $travelerID || $parentGroup[0]->isStaff($travelerID));
		
		if(!$isAdminLogged){
			header("location: /resourcefiles.php?action=view&cat=$category&genID=$genericID");
			exit;
		}
		
		require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
	    new ResourceFileFactory($genericID,$category);
	    $Rfactory = ResourceFileFactory::create();
	    
	   	/****************************************************
		 * edits by neri: 
		 * 		added code for displaying the profile component		11-06-08
		 ****************************************************/
	   
	    $profile_comp = $FFactory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		$profile_comp->init($genericID);
	    
		
		if($parentGroup[0]->isParent())
			$groups = array_merge(array($parentGroup[0]),$parentGroup[0]->getSubgroups());
		else
			$groups = array($parentGroup[0]);//array_merge(array($parentGroup[0]->getParent()),$parentGroup[0]->getParent()->getSubgroups());
		
		$resourcefile = new ResourceFiles($REQUEST_VAR['id']);
		$resourcefile->setContext($parentGroup[0]);

	    $template->set('subNavigation',$Rfactory->getSUBNavigation($Subnavigation));
	   	$template->set('headercaption',$Rfactory->getHeaderCaption());
	   	$template->set('genID',$REQUEST_VAR['genID']);	
		$template->set('context',$REQUEST_VAR['cat']);
		$template->set('loginID',$travelerID);
		$template->set('isAdminLogged', $isAdminLogged);
		$template->set('layoutMain',$FFactory->getTemplate('LayoutMain'));
		$template->set('group',$category =='admingroup'?$Rfactory->getRESOURCEFILEcontext()->getName():false);
		$template->set('profile', $profile_comp->get_view());
		$template->set('parentGroup',$parentGroup[0]);
		$template->set('isAdmin',($parentGroup[0]->getAdministratorID() == $sessMan->get('travelerID')) ? true : false );
		$template->set('resourcefile', $resourcefile);
		$template->set('groups',$groups);
		$template->set('siteName',SiteContext::getInstance()->getSiteName());
		$template->set('isParent',$parentGroup[0]->isParent());
		
	   	echo $template->out('tpl.ViewEditResourceFile.php');
	}
	
}


?>
