<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/model/Class.Traveler.php');
require_once('travellog/service/ResourcefileService.php');
require_once('Class.Template.php');
require_once("travellog/model/Class.ResourceFiles.php");
require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
require_once("travellog/model/Class.UploadManager.php");
require_once('travellog/model/Class.PathManager.php');
require_once("travellog/UIComponent/Resourcefiles/model/Class.ParsePERLUploadParams.php");
require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileManager.php");

class ResourceFileRequestEDIT{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		
		$template 		= $FFactory->getClass('Template');
		$template->set_path("travellog/UIComponent/Resourcefiles/views/");
		
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		$resourcefileid = isset($REQUEST_VAR['rfileID'])?$REQUEST_VAR['rfileID']:$REQUEST_VAR['id'];
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$_REQUEST['travelerID'] = $travelerID;
			
		new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
		$RFFactory = ResourceFileFactory::create();
		$RFFactory->setLoginID($travelerID);
		
		
		$ResourceFiles = new ResourceFiles($resourcefileid);
		$ResourceFiles->setContext($RFFactory->getRESOURCEFILEcontext());		
		
		if(!$RFFactory->isOwner())
			return header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
		
		
		$error 	= NULL;
		
		
		if(isset($_REQUEST['tmp_sid'])){											
			
			new ParsePERLUploadParams($REQUEST_VAR['tmp_sid']);
			
			$path = new PathManager($RFFactory->getRESOURCEFILEcontext());	
		
			$U_Manager = new UploadManager();
			$U_Manager->setContext($REQUEST_VAR['cat']);			
			$U_Manager->setDestination($path);	
					
			$U_Manager->setFiles(ParsePERLUploadParams::getFiles());
			$U_Manager->setTypeUpload('PDF');
			$U_Manager->upload();		
			
			$R_Manager = new ResourceFileManager();
			$R_Manager->setResourceFileID($resourcefileid);
			$R_Manager->setGenID($REQUEST_VAR['genID']);
			$R_Manager->setLoginID($travelerID);
			
			
			$R_Manager->change($U_Manager);
			
			if($R_Manager->getValidFiles()){	
				header("Location:http://".$_SERVER['SERVER_NAME']."/resourcefiles.php?cat=".$REQUEST_VAR['cat']."&action=view&genID=".$REQUEST_VAR['genID']);	
			}else{
				$error = $R_Manager->getInvalidFiles();
			}
		}
		
		
		
		$template->set('subNavigation',$RFFactory->getSUBNavigation($Subnavigation));
		
		/****************************************************
		 * edits by neri: 11-07-08
		 * added code for displaying th profile component
		 ****************************************************/
		
		$profile_comp = $FFactory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		$profile_comp->init($REQUEST_VAR['genID']);
		$template->set('profile', $profile_comp->get_view());
		
		$template->set('error',$error);
		$template->set('resourcefileid',$resourcefileid);
		$template->set('context',$REQUEST_VAR['cat']);
		$template->set('genID',$REQUEST_VAR['genID']);
		$template->set('layoutMain',$FFactory->getTemplate('LayoutMain'));
		$template->set('resourcefile', $ResourceFiles);
		$template->set('group', $REQUEST_VAR['cat'] =='admingroup'?$RFFactory->getRESOURCEFILEcontext()->getName():false);
								
		echo $template->out('tpl.EditResourceFile.php');	
		
	
	}	
		
}


?>
