<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/ResourcefileService.php');

class ResourceFileRequestMANAGE{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR = array()){
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
	   	new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
	    $Rfactory = ResourceFileFactory::create();
	    $Rfactory->setLoginID($travelerID);
	    
	    require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileLayoutFactory.php");
	    ResourceFileLayoutFactory::create($Rfactory, $REQUEST_VAR);
		
		
		
		
	}
	
}


?>
