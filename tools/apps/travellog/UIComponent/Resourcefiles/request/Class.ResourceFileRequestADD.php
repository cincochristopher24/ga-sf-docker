<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/model/Class.Traveler.php');
require_once('travellog/service/PhotoService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
require_once("travellog/model/Class.UploadManager.php");
require_once('travellog/model/Class.PathManager.php');
require_once("travellog/UIComponent/Resourcefiles/model/Class.ParsePERLUploadParams.php");
require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileManager.php");

class ResourceFileRequestADD{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		
		$template 		= $FFactory->getClass('Template');
		$template->set_path("travellog/UIComponent/Resourcefiles/views/");
		
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$REQUEST_VAR['travelerID'] = $travelerID;
			
		new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
		$RFFactory = ResourceFileFactory::create();
		$RFFactory->setLoginID($travelerID);
		
		if(!$RFFactory->isOwner())
			return header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
		
		// insert code for logger system
		require_once('gaLogs/Class.GaLogger.php');
		GaLogger::create()->start($travelerID,'TRAVELER');
		
		$error 				= NULL;
		$new  				= false;
		$submit 			= false;
		$prvcpref			= NULL;
		$sel_groups			= array();
		$ismembersnotify	= true;
		
		$groups = array();
		
		if($REQUEST_VAR['cat'] == "admingroup"){
			
			if($RFFactory->getRESOURCEFILEcontext()->isSubGroup())
				$parentGroup = $RFFactory->getRESOURCEFILEcontext()->getParent();
			else	
				$parentGroup = $RFFactory->getRESOURCEFILEcontext();
			$groups = array_merge(array($parentGroup),$parentGroup->getSubGroups());
		}
		
		if(isset($REQUEST_VAR['tmp_sid'])){
						
			new ParsePERLUploadParams($REQUEST_VAR['tmp_sid']);
			$path = new PathManager($RFFactory->getRESOURCEFILEcontext());	
					
			$U_Manager = new UploadManager();
			$U_Manager->setContext($REQUEST_VAR['cat']);			
			$U_Manager->setDestination($path);	
			//$U_Manager->setContext($RFFactory->getRESOURCEFILEcontext());			
			
			$U_Manager->setFiles(ParsePERLUploadParams::getFiles());
			$U_Manager->setTypeUpload('PDF');
			$U_Manager->upload();		
			
			
			$R_Manager = new ResourceFileManager();
			$R_Manager->setGenID($REQUEST_VAR['genID']);
			$R_Manager->setLoginID($travelerID);
			$R_Manager->setCaption(ParsePERLUploadParams::getCaption());
			$R_Manager->setPrivacypref(ParsePERLUploadParams::getPrivacyPreference());
			$R_Manager->setAssigngrpIDS(ParsePERLUploadParams::getAssignGroup());
			$R_Manager->setIsMembersNotify(ParsePERLUploadParams::isMembersNotify());
			$R_Manager->setUploadedBy($travelerID);
			
			require_once("travellog/UIComponent/Resourcefiles/command/Class.ResourceFileAssignGroupCommand.php");
			require_once("travellog/UIComponent/Resourcefiles/command/Class.ResourceFileNotifyMembersCommand.php");
			$R_Manager->addCommand(new ResourceFileAssignGroupCommand);	// if Assign 
			$R_Manager->addCommand(new ResourceFileNotifyMembersCommand);// if Members Notify
			
			
			$R_Manager->Save($U_Manager);
			
			if($R_Manager->getValidFiles()){	
				header("Location:http://".$_SERVER['SERVER_NAME']."/resourcefiles.php?cat=".$REQUEST_VAR['cat']."&action=view&genID=".$REQUEST_VAR['genID']);	
			}else{
				$error = $R_Manager->getInvalidFiles();
				if($REQUEST_VAR['cat'] == 'admingroup'){
					$prvcpref 			= ParsePERLUploadParams::getPrivacyPreference();
					$sel_groups			= ParsePERLUploadParams::getAssignGroup();
					$ismembersnotify	= ParsePERLUploadParams::isMembersNotify();
				}
			}
			
		}
		
		$template->set('subNavigation',$RFFactory->getSUBNavigation($Subnavigation));
		
		/****************************************************
		 * edits by neri: 11-07-08
		 * added code for displaying th profile component
		 ****************************************************/
		
		$profile_comp = $FFactory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		$profile_comp->init($REQUEST_VAR['genID']);
		
		$template->set_vars(array('error'          => $error,
								  'prvcpref'       => $prvcpref,
								  'sel_groups'     => $sel_groups,
								  'ismembersnotify'=> $ismembersnotify,		
								  'varfile'        => 'resourcefile[]',
								  'group'          => $REQUEST_VAR['cat'] =='admingroup'?$RFFactory->getRESOURCEFILEcontext()->getName():false,
								  'cat'            => $REQUEST_VAR['cat'],
								  'action'         => 'add',
								  'layoutMain'     => $FFactory->getTemplate('LayoutMain'),
								  'genID'          => $REQUEST_VAR['genID'],
								  'groups'	       => $groups,
								  'ResourceFile'   => $RFFactory->getRESOURCEFILEcontext(),
								  'traveler'       => new Traveler($travelerID),
								  'profile'        => $profile_comp->get_view()
					)); 
		echo $template->out('tpl.ViewAddResourceFile.php');	
											
		
		
			
	}
	
}


?>
