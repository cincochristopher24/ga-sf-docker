<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


class ResourceFileRequestDROPVIEWALLFILES{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$_REQUEST['travelerID'] = $travelerID;
		
		require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
	    new ResourceFileFactory($_REQUEST['genID'],$_REQUEST['cat']);
	    $RFFactory = ResourceFileFactory::create();
	    $RFFactory->setLoginID($travelerID);
	    
	    
	    $template->set('allresourcefiles',$RFFactory->getAllFiles());
	    
	    $template->out('tpl.IncViewManageResourceFileAllfiles.php');	
			
	}
	
}


?>
