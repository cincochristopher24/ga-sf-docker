<?php

require_once('travellog/model/Class.Traveler.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
require_once("travellog/model/Class.UploadManager.php");
require_once('travellog/model/Class.PathManager.php');
require_once("travellog/UIComponent/Resourcefiles/model/Class.ParsePERLUploadParams.php");
require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileManager.php");

class ResourceFileRequestUPLOAD{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		
		$template 		= $FFactory->getClass('Template');
		$template->set_path("travellog/UIComponent/Resourcefiles/views/");
		
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$REQUEST_VAR['travelerID'] = $travelerID;
			
		new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
		$RFFactory = ResourceFileFactory::create();
		$RFFactory->setLoginID($travelerID);
		
		if(!$RFFactory->isOwner())
			return header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
		
		// insert code for logger system
		require_once('gaLogs/Class.GaLogger.php');
		GaLogger::create()->start($travelerID,'TRAVELER');
		
		$error 				= NULL;
		$new  				= false;
		$submit 			= false;
		$prvcpref			= 2;
		$sel_groups			= array();
		$ismembersnotify	= true;
		
		$groups = array();
		
		if($REQUEST_VAR['cat'] == "admingroup"){
			
			if($RFFactory->getRESOURCEFILEcontext()->isSubGroup()){
				$parentGroup = $RFFactory->getRESOURCEFILEcontext()->getParent();
				$groups = array($RFFactory->getRESOURCEFILEcontext());
			}else{	
				$parentGroup = $RFFactory->getRESOURCEFILEcontext();
				$groups = array_merge(array($parentGroup),$parentGroup->getSubGroups());
			}
		}
		
		$domainConfig 	= SiteContext::getInstance();
		$emailName		= $domainConfig->getEmailName();
		$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
		$siteUrl 		= $domainConfig->getServerName();
		
		try{
			$siteConfig = Config::findByGroupID($parentGroup->getGroupID());
			if( $siteConfig instanceof Config ){
				$domainConfig   = $siteConfig;
				$emailName		= $domainConfig->getEmailName();
				$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				$siteUrl 		= $domainConfig->getServerName();
				if( FALSE === strpos($siteUrl,"dev.")
					&& (strpos($_SERVER["SERVER_NAME"],"dev.") || 0 === strpos($_SERVER["SERVER_NAME"],"dev.")) ){
					$siteUrlTokens = explode(".",$siteUrl);
					$temp = array();
					foreach($siteUrlTokens AS $token){
						if( "goabroad" == strtolower($token) ){
							$temp[] = "dev";
						}		
						$temp[] = strtolower($token);
					}
					$siteUrl = join(".",$temp);
				}
			}
		}catch(exception $e){
		}
		
		$defaultCustomMessage	= "Hi [name],

Your group administrator for ".$parentGroup->getName()." has uploaded new resource file(s).

To view the file(s), please follow the link(s) below:

[resource_files_list]



Many thanks,


The $siteName Team

http://$siteUrl";
		
		if(isset($REQUEST_VAR['tmp_sid']) && 0 < count($_FILES) && isset($_FILES["upfile"]) ){
			$this->processUploadedFiles($REQUEST_VAR);
						
			new ParsePERLUploadParams($REQUEST_VAR['tmp_sid']);
			$path = new PathManager($RFFactory->getRESOURCEFILEcontext());	
					
			$U_Manager = new UploadManager();
			$U_Manager->setContext($REQUEST_VAR['cat']);			
			$U_Manager->setDestination($path);				
			$U_Manager->setFiles(ParsePERLUploadParams::getFiles());
			$U_Manager->setDocumentTitles(ParsePERLUploadParams::getCaption());
			$U_Manager->setTypeUpload('PDF');
			$U_Manager->upload();		
			
			$R_Manager = new ResourceFileManager();
			$R_Manager->setGenID($REQUEST_VAR['genID']);
			$R_Manager->setLoginID($travelerID);
			//$R_Manager->setCaption(ParsePERLUploadParams::getCaption());
			$R_Manager->setPrivacypref(ParsePERLUploadParams::getPrivacyPreference());
			$R_Manager->setAssigngrpIDS(ParsePERLUploadParams::getAssignGroup());
			$R_Manager->setIsMembersNotify(ParsePERLUploadParams::isMembersNotify());
			$R_Manager->setUploadedBy($travelerID);
			$R_Manager->setCustomMessage(ParsePERLUploadParams::getCustomMessage());
			$R_Manager->setIsBatchUpload(TRUE);
			
			require_once("travellog/UIComponent/Resourcefiles/command/Class.ResourceFileAssignGroupCommand.php");
			require_once("travellog/UIComponent/Resourcefiles/command/Class.ResourceFileInstantNotifyMembersCommand.php");
			$R_Manager->addCommand(new ResourceFileAssignGroupCommand);	// if Assign 
			$R_Manager->addCommand(new ResourceFileInstantNotifyMembersCommand);// if Members Notify
			
			
			$R_Manager->Save($U_Manager);
			
			if($R_Manager->getValidFiles()){	
				header("Location:http://".$_SERVER['SERVER_NAME']."/resourcefiles.php?cat=".$REQUEST_VAR['cat']."&action=view&genID=".$REQUEST_VAR['genID']);	
			}else{
				$error = $R_Manager->getInvalidFiles();
				if($REQUEST_VAR['cat'] == 'admingroup'){
					$prvcpref 			= ParsePERLUploadParams::getPrivacyPreference();
					$sel_groups			= ParsePERLUploadParams::getAssignGroup();
					$ismembersnotify	= ParsePERLUploadParams::isMembersNotify();
					$defaultCustomMessage = ParsePERLUploadParams::getCustomMessage();
				}
			}
			
		}
		
		$template->set('subNavigation',$RFFactory->getSUBNavigation($Subnavigation));
		
		$profile_comp = $FFactory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		$profile_comp->init($REQUEST_VAR['genID']);
		
		$template->set_vars(array('error'          => $error,
								  'prvcpref'       => $prvcpref,
								  'sel_groups'     => $sel_groups,
								  'ismembersnotify'=> $ismembersnotify,		
								  'varfile'        => 'resourcefile[]',
								  'group'          => $REQUEST_VAR['cat'] =='admingroup'?$RFFactory->getRESOURCEFILEcontext()->getName():false,
								  'cat'            => $REQUEST_VAR['cat'],
								  'action'         => 'add',
								  'layoutMain'     => $FFactory->getTemplate('LayoutMain'),
								  'genID'          => $REQUEST_VAR['genID'],
								  'groups'	       => $groups,
								  'ResourceFile'   => $RFFactory->getRESOURCEFILEcontext(),
								  'traveler'       => new Traveler($travelerID),
								  'profile'        => $profile_comp->get_view(),
								  'siteName'	   => SiteContext::getInstance()->getSiteName(),
								  'isParent'	   => $RFFactory->getRESOURCEFILEcontext()->isParent(),
								  'defaultCustomMessage' => $defaultCustomMessage
					));
		echo $template->out('tpl.ViewUploadResourceFile.php');			
	}
	
	//function to emulate uu_upload.pl
	//creating an ini file for ParsePERLUploadParams
	private function processUploadedFiles($REQUEST_VARS){
		$files = $_FILES["upfile"];
		$validCaption = array();
		$validTempFile = array();
		foreach($files["tmp_name"] AS $key => $tmp_name){
			if( 0 == $files["error"][$key] && 'application/pdf' == strtolower($files["type"][$key]) ){
				$validCaption[] = $REQUEST_VARS["txtcaption"][$key];
				$validTempFile[] = $tmp_name;
			}
		}
		
		$filenames = array();
		foreach($validTempFile AS $tmp){
			$filename = uniqid().time().".pdf";
			move_uploaded_file($tmp, PathManager::$temporarypath.$filename);
			$filenames[] = $filename;
		}
		
		$paramfile = PathManager::$temporarypath.$REQUEST_VARS["tmp_sid"].".params"; 
		$handle = fopen($paramfile, 'w');
		
		$data = ":parameters for file resource upload\r\n";
		
		$data .= "\r\n[filename]\r\n";
		foreach($filenames AS $name){
			$data .= "upfile[] = $name\r\n";
		}
		$data .= "\r\n[document_title]\r\n";
		foreach($validCaption AS $caption){
			$data .= "txtcaption[] = \"$caption\"\r\n";
		}
		$data .= "\r\n[groups_to_be_assigned]\r\n";
		foreach(array_keys($REQUEST_VARS) AS $key){
			if(strpos($key,'grpid_') !== false){
				$data .= $key." = ".$REQUEST_VARS[$key]."\r\n";
			}
		}
		$data .= "\r\n[others]\r\n";
		if( array_key_exists('txtCustomMessage',$REQUEST_VARS) ){
			$data .= "txtCustomMessage = \"".$REQUEST_VARS["txtCustomMessage"]."\"\r\n";
		}
		if( array_key_exists('chkNotifyMembers',$REQUEST_VARS) ){
			$data .= "ismembersnotify = ".$REQUEST_VARS["chkNotifyMembers"]."\r\n";
		}
		if( array_key_exists('txtfileprvpref',$REQUEST_VARS) ){
			$data .= "txtfileprvpref = ".$REQUEST_VARS["txtfileprvpref"]."\r\n";
		}
		fwrite($handle,$data);
		fclose($handle);
	}
}
