<?php
/*
 * Created on Mar 6, 2008
 * Class.ResourceFileLayoutFactory.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


 class ResourceFileLayoutFactory {
 	
 	public static function create(AbstractResourceFile $AbsRfile, $REQUEST_VAR = array()){
 		
 		//echo $AbsRfile->contextIDENTITY;
 		
 		require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFile".$AbsRfile->contextIDENTITY."Layout.php");
 		$resLayout = new ReflectionClass("ResourceFile".$AbsRfile->contextIDENTITY."Layout");
 		$instance = $resLayout->newInstance();
 		return $instance->render($AbsRfile,$REQUEST_VAR);
 		
 	}
 	
 }	
	
	
?>
