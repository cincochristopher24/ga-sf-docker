<?php
/*
 * Created on Oct 4, 2007
 * IResourceFileFactory.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
interface IResourceFileFactory{
	
	static function create();	
	
} 

?>
