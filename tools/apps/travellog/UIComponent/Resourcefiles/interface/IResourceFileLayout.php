<?php
/*
 * Created on Mar 6, 2008
 * IResourceFileLayout.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 interface IResourceFileLayout{
 	
 	function render(AbstractResourceFile $AbsRfile);
 	
 	
 }
 
 
?>
