<?php
/*
 * Created on Mar 6, 2008
 * Class.ResourceFileAdmingroupLayout.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


require_once("travellog/UIComponent/Resourcefiles/interface/IResourceFileLayout.php");

class ResourceFileAdmingroupLayout implements IResourceFileLayout{
	
	function  render(AbstractResourceFile $AbsRfile, $REQUEST_VAR = array()){
		
		//echo "<pre>";
		//	print_r($REQUEST_VAR);
		//echo "</pre>";
		
		if($REQUEST_VAR['action'] == "load"){
			if($AbsRfile->getRESOURCEFILEcontext()->isParent()){
				$REQUEST_VAR['showsubgroup_select'] = true;
				require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileAdmingroupParentLayout.php");
				return new ResourceFileAdmingroupParentLayout($AbsRfile, $REQUEST_VAR);
			}else{
				require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileAdmingroupSubgroupLayout.php");
				return new ResourceFileAdmingroupSubgroupLayout($AbsRfile, $REQUEST_VAR);
			}
		}else{
			
			
			if($AbsRfile->getRESOURCEFILEcontext()->isParent()){
				require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileAdmingroupParentLayout.php");
				$REQUEST_VAR['showsubgroup_select'] = false;
				return new ResourceFileAdmingroupParentLayout($AbsRfile, $REQUEST_VAR);
			}else{
				require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileAdmingroupManageLayout.php");
				return new ResourceFileAdmingroupManageLayout($AbsRfile, $REQUEST_VAR);
			}
			
			
			
			
		
		}	
	
	}
} 

?>


<?
	/*
	echo "<script>";
		//echo "ResourceFiles.filter = false;";
		echo "alert(43)";
	echo "</script>";
	*/			
?>

