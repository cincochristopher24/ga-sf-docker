<?php
/*
 * Created on Oct 4, 2007
 * Class.ResourceFileAdminGroup.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Resourcefiles/abstract/AbstractResourceFile.php");

class ResourceFileAdmingroup extends AbstractResourceFile{
	
	private     $loginID             = 0;
	private     $GROUPID             = 0;
	private     $RESOURCEFILEcontext = NULL;
	private     $view                = NULL;
	private     $loggedTraveler      = NULL;
	public      $NUMfileTOUPLOAD     = 1;
	static      $resourcefiles       = array();
	static      $allresourcefiles    = NULL;
	
	public      $contextIDENTITY     = "Admingroup";
	
	function __construct($id){
		self::setRESOURCEFILEcontext($id);
		$this->GROUPID = $id;
		return $this;
	}
	
	function setLoginID($loginID){
		$this->loginID = $loginID;
	}
	
	function setRESOURCEFILEcontext($id){
		require_once('travellog/model/Class.AdminGroup.php');
		
		try{
		  	$admingroup		= new AdminGroup($id);
			//self::$resourcefiles	= $admingroup->getResourceFiles();
			$this->RESOURCEFILEcontext = $admingroup;
		}
		catch(Exception $e){
		}
	}
	
	function setVIEW($view){
		switch($view){
			case 'public_only':
				$this->view = 2;
				break;
			case 'member_only':
				$this->view = 1;
				break;
			case 'hidden_only':
				$this->view = 0;
				break;
			default:
				$this->view = NULL;
		}
	}
	
	function getGroupID(){
		return $this->GROUPID;	
	}
	
	function getRESOURCEFILEcontext(){
		return $this->RESOURCEFILEcontext;	
	}
	
	function getResourceFiles(){
		
		//to do: create filtered
		/*
		$ul = new ResourceFileList( self::$resourcefiles);
		return $ul->getFilteredResourceFiles(new FilteredResourceFiles($travelerID));
		
		
		return FilteredResourceFile::create('filtered1')->filter($travelerID);
		*/
		
		if($this->isOwner()){
			//return self::$resourcefiles;
			return $this->RESOURCEFILEcontext->getResourceFiles(NULL,NULL,$this->view);
		}else{
						
			//check if member of the group
			//if member: get resourcefile where privacypreference eq member and show all
			if($this->isMember()){
				return $this->RESOURCEFILEcontext->getResourceFiles(NULL,1,$this->view);
			}else{  //else get files eq show all only
				return $this->RESOURCEFILEcontext->getResourceFiles(NULL,2,$this->view); 
			}
		}
		
		
		
		
	}
	
	function getCOUNTResourceFiles(){
		return count($this->getResourceFiles());	
	}
	
	function isOwner($group = NULL){
		if(is_null($group))
			$group = $this->RESOURCEFILEcontext;
		if($group->getAdministrator()->gettravelerID() == $this->loginID || in_array($this->loginID,$group->getStaff(true)))
			return true;
		else
			return false;
		/*	
		if($this->RESOURCEFILEcontext->getAdministrator()->gettravelerID() == $this->loginID)
			return true;
		else
			return false;
		*/
	}
	
	function isMember($group = NULL){
		if( is_null($this->loggedTraveler) ){
			//check if member of the group
			require_once('travellog/model/Class.Traveler.php');
			$this->loggedTraveler =  new Traveler($this->loginID);
		}
		if(is_null($group))
			return $this->RESOURCEFILEcontext->isMember($this->loggedTraveler);
		else
			return $group->isMember($this->loggedTraveler);
	}
		
	function getHeaderCaption(){
		return "Resource Files";
	}
	
	function getBackLink(){
		return "/group.php?gID=".$this->GROUPID;
	} 
	
	function getBackCaption(){
		return $this->RESOURCEFILEcontext->getName()."'s  Homepage";
	} 
	function getNorecordlabel(){
		if(!$this->getCOUNTResourceFiles())
			return "There are no resource files yet!";	
		
	} 
	
	function getSUBNavigation(SubNavigation $SUbnav){
		$SUbnav->setContextID($this->GROUPID);
		$SUbnav->setContext('GROUP');
		$SUbnav->setGroupType('ADMIN_GROUP');									
		$SUbnav->setLinkToHighlight('RESOURCE_FILES');	
		
		return $SUbnav;
	}
	
	
	function getAllFiles(){
		
		if( !is_null(self::$allresourcefiles) && is_array(self::$allresourcefiles) ){
			return self::$allresourcefiles;
		}
		
		$subfiles	= array();
		$allfiles	= array();			 
		$allfileids	= array();
		$prgfiles	= array();
		
		if($this->isOwner())
			$privacyPref = 0;
		else{
			if($this->isMember())
				$privacyPref = 1;
			else
				$privacyPref = 2;	
		}
		
		if(count($this->RESOURCEFILEcontext->getSubgroups())){
				foreach($this->RESOURCEFILEcontext->getSubgroups() as $subgroup){
					$subgroupPrivacyPref = (1 == $privacyPref && !$this->isMember($subgroup)) ? 2 : $privacyPref;
					if($subgroup->getResourceFiles(NULL,$subgroupPrivacyPref,$this->view)){
						foreach($subgroup->getResourceFiles(NULL,$subgroupPrivacyPref,$this->view) as $rfiles){
							$subfiles[] = $rfiles; 
						}				
					}
				}
		}
		if($this->RESOURCEFILEcontext->isParent()){ //if not parentgroup get files from parent
			if($this->getResourceFiles()){
				$arrfiles =  array_merge($this->getResourceFiles(),$subfiles);
			}else{
				$arrfiles =  $subfiles;	
			}
		}else{
			
			$parentgroup = $this->RESOURCEFILEcontext->getParent();
			
			$parentgroup 	= new AdminGroup($parentgroup->getGroupID());
			$parentPrivacyPref = (0 == $privacyPref && !$this->isOwner($parentgroup)) ? 1 : $privacyPref;
			if($parentgroup->getResourceFiles(NULL,$parentPrivacyPref,$this->view)){
				foreach($parentgroup->getResourceFiles(NULL,$parentPrivacyPref,$this->view) as $prfiles){
					$prgfiles[] = $prfiles; 
				}				
			}
			
			$arrfiles =  array_merge($subfiles,$prgfiles);
			
		}
		
		if(count($arrfiles)){
			
			foreach($arrfiles as $rfiles){
					if(!in_array($rfiles->getResourceFileID(),$allfileids)){
							array_push($allfiles,$rfiles);
							array_push($allfileids,$rfiles->getResourceFileID());
					}
			}	
		}	

		self::$allresourcefiles = $allfiles;
				
		return	$allfiles;			
	}
	
} 
 
?>
