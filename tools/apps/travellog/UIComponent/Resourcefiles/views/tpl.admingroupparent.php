<?  
	/**********************************************************
	 * edits of neri:	01-20-09
	 * 		commented Edit File and Download links
	 * 		changed "Assign" text to "Assign to Groups"
	 **********************************************************/
?>			
<?php if($owner && $showsubgroup_select): ?>
	<div class="section_small_details">			
		<ul class="list_small_details" id="file_list">			
			<li class="options_text">									
				<? if(count($subgroups)): ?>							
							<label> In Group </label>
							<select id="group_selector" class="group_selector" onchange="ResourceFiles.loadResourcefileLists(this.options[this.selectedIndex].value,this.options[this.selectedIndex].text)">
									<!--<option value="0">--Select group--</option>-->
									<option value="all">-- All Groups --</option>
									<!--<option value="<?= $group->getGroupID()?>"><?= $group->getName()?> &raquo; Main</option>-->
									<? foreach($subgroups as $subg):?>
										<option value="<?= $subg->getGroupID()?>"> <?=$subg->getName();?> </option>
									<? endforeach;?>
							</select>
							&nbsp; &nbsp;		
				<? endif; ?>
				<? if($owner):?>
						<label> Show </label>				
						
						<select id="groupfile_filter_selector" class="groupfile_filter_selector" onchange="ResourceFiles.loadResourcefileLists()"> 
							<option value="all_files"> All Files </option>
							<option value="member_only">Members Only</option>							
							<option value="public_only">Public File</option>
							<option value="hidden_only">Hidden</option>														
						</select>
				<? endif;?>
			</li>
			<li class="options_actions">
				<a href="#" id="uploadFileLink"> &uarr; Upload File</a>					
			</li>					
		</ul>
	</div>	
<? endif; ?>

<div class="content" id="resourcefile">						
	<? if($reccount): ?>							
		<ul class="resource_files">
			<?php for($x=$start; $x<$endrow; $x++): ?>								
				<li>							
					<div class="file_name"><img align="bottom" src="images/pdf.gif" alt="" /> <?  echo $resourcefiles[$x]->getCaption();?></div>
					<? //if($owner):?>
						<span class="file_visibility">
						<? if($resourcefiles[$x]->getPrivacypreference() ==0):?>Hidden<? endif;?>
						<? if($resourcefiles[$x]->getPrivacypreference() ==1):?>Members only<? endif;?>
						<? if($resourcefiles[$x]->getPrivacypreference() ==2):?>Public File<? endif;?>
						</span>
					<? //endif; ?>
					
					<div class="actions">											
						<a href="<? echo $resourcefiles[$x]->getResourcefileLink(); ?>" target="_blank">&darr; Download</a>	
						<? if($owner):?>
							 |
							<a href="/resourcefiles.php?action=showedit&cat=admingroup&genID=<? echo $genID;?>&id=<?= $resourcefiles[$x]->getResourceFileID();?>"> Edit </a>
							 |
							<a href="javascript:void(0)" onclick="ResourceFiles.Delete(<?= $resourcefiles[$x]->getResourceFileID(); ?>,<?= $resourcefiles[$x]->getContext()->getGroupID();?>)" class="negative_action"> Delete </a>
							
						<? endif; ?>
						
					</div>												
				</li>
			<? endfor; ?>									
		</ul>
		<? $page->showPagination(array('label' => 'Files')); ?>							
	<? else: ?>
			<div class="help_text">
				<p>													
					<?=$norecfound; ?>												
					<? if($owner): ?>
						Do you want to <a href="<?= $uploadrfiles ?>" 
						class="up_photo">upload</a> a file?
					<? endif; ?>		
				</p>																			
			</div>
	<? endif; ?>
</div> <!-- end content -->

			
<div class="foot"></div>