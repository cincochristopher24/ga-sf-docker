<?php
/*
 * Created on Sep 28, 2007
 *
 */
 
 
 require_once("travellog/UIComponent/Resourcefiles/interface/IResourceFileCommand.php");
 
 class ResourceFileNotifyMembersCommand implements IResourceFileCommand{
 	
 	function onCommand(ResourceFileManager $RFileManager, $args = array() ){
    	
    	$resourcefileID = $args['resourcefileID'];
    	
    	if ( get_class($RFileManager->getContext()) == 'AdminGroup' ) 
	    	if($RFileManager->IsMembersNotify()){
	    		
	    		require_once('travellog/model/Class.NotificationSetting.php');
				$notificationSetting = new NotificationSetting();
				$notificationSetting->setContext(NotificationSetting::GROUP_RESOURCE_CONTEXT); //GROUP_RESOURCE_CONTEXT for resource files 
				$notificationSetting->setContextID($resourcefileID); //the ID of the event or resource file.
				$notificationSetting->setNotifyGroupMembers(true);//boolean
				$notificationSetting->save();
	    		
				
	    		return true;
	    	}else{
	    		return false;
	    	}
	    else	
    		return false;
  	}
 	
 	
 }
 
?>
