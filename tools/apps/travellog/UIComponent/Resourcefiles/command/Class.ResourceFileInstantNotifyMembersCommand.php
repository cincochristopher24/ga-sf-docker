<?php
 
 require_once("travellog/UIComponent/Resourcefiles/interface/IResourceFileCommand.php");
 
 class ResourceFileInstantNotifyMembersCommand implements IResourceFileCommand{
 	
 	function onCommand(ResourceFileManager $RFileManager, $args = array() ){  	
    	if ( get_class($RFileManager->getContext()) == 'AdminGroup' ){ 
	    	if($RFileManager->IsMembersNotify()){
	    		
				$this->sendEmailNotification($args);
				
	    		return true;
	    	}else{
	    		return false;
	    	}
	    }else{	
    		return false;
		}
  	}
 	
	private function sendEmailNotification($args){
		require_once('travellog/model/notifications/Class.Notification.php');
		require_once('travellog/model/notifications/Class.NotificationComposer.php');
		require_once('travellog/model/notifications/Class.NotificationSender.php');
		
		require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
		require_once('travellog/factory/Class.FileFactory.php');
						
		$domainConfig 	= SiteContext::getInstance();
		$emailName		= $domainConfig->getEmailName();
		$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
		$siteUrl 		= $domainConfig->getServerName();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerClass('GroupFactory');
		
		//get groups to where the file(s) are assigned
		$groupsAssigned = $file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array_unique($args["groupIDs"]));
		
		$toGroup = NULL;	
		if( 0 < count($groupsAssigned) ){
			$toGroup = $groupsAssigned[0]->isSubgroup() ? $groupsAssigned[0]->getParent() : $groupsAssigned[0];
			try{
				$siteConfig = Config::findByGroupID($toGroup->getID());
				if( $siteConfig instanceof Config ){
					$domainConfig   = $siteConfig;
					$emailName		= $domainConfig->getEmailName();
					$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
					$siteUrl 		= $domainConfig->getServerName();
					if( FALSE === strpos($siteUrl,"dev.")
						&& (strpos($_SERVER["SERVER_NAME"],"dev.") || 0 === strpos($_SERVER["SERVER_NAME"],"dev.")) ){
						$siteUrlTokens = explode(".",$siteUrl);
						$temp = array();
						foreach($siteUrlTokens AS $token){
							if( "goabroad" == strtolower($token) ){
								$temp[] = "dev";
							}		
							$temp[] = strtolower($token);
						}
						$siteUrl = join(".",$temp);
					}
				}
			}catch(exception $e){
			}
		}
		
		//get members of groups who can receive notifs
		$members = array();
		$temp = array();
		foreach($groupsAssigned AS $group){
			try{
				$temp = $group->getMembers();
				foreach($temp as $member){
					if( !array_key_exists($member->getTravelerID(),$members) ){
						$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($member->getTravelerID());
						if( FALSE == $canReceiveNotification ){
							continue;
						}
						$members[$member->getTravelerID()] = $member;
					}
				}
			}catch(exception $e){
			}
		}
		
		if( !count($members) ){
			return;
		}
		
		//set proper footer variable, check if the recipient is the admin/staff of in one of the assigned groups
		$footerVars = array(
			'siteName'	=> $siteName,
			'siteUrl'	=> $siteUrl,
			'group'		=> $toGroup,
			'isAdvisor'	=> FALSE
		);
		
		//compose notif content
		$tempContent = $args["batchUpload"]->getCustomMessage();
		$tempContent = str_replace("[resource_files_list]",$this->generateResourceFileLinkListString($args["resource_files"],$siteUrl),$tempContent);
				
		//create notifications		
		$mNotifications = array();
		$recipients = array();
		foreach($members as $memberID => $recipient){
			if( in_array($memberID,$recipients) ){
				continue;
			}
			$recipients[] = $memberID;
			
			$content = $tempContent;
			$content = str_replace("[name]",$recipient->getName(),$content);		
			$vars = array_merge($footerVars, array(
				'content'			=> $content,
				'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
			));
		
			$body = NotificationComposer::compose($vars, 'tpl.GroupInstantResourceFileNotificationTemplate.php');
			$subject = "New Resource File ";
			$subject .= 1 < count($args["resource_files"]) ? "Updates on $siteName" : "Update on $siteName";
			
			$notification = new Notification;
			$notification->setMessage($body);
			$notification->setSubject($subject);
			$notification->setSender($domainConfig->getGanetAdminEmail());
			$notification->setSenderName($emailName);
			$notification->addReplyTo($domainConfig->getAdminEmail(), $emailName);
			$notification->addRecipient($recipient->getTravelerProfile()->getEmail());

			$mNotifications[] = $notification;
		}
		
		if( 0 < count($mNotifications) ){
			NotificationSender::getInstance()->send($mNotifications,true);
		}	
	}
	
	private function generateResourceFileLinkListString($files,$siteUrl){
		$str = "";
		foreach($files AS $file){
			$str .= $file->getCaption()."
http://$siteUrl/".$file->getResourcefileLink()."

";
		}
		return $str;
	}

 }