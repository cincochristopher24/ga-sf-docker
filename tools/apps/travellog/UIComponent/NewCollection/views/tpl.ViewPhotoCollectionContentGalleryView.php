<div id="gallery_view" style="display:none;" class="viewer content">
	<?php if( $isPrivileged ): ?>
		<div id="photo_control" class="uploaded">
			<a id="delete_photo_control" class="delete" href="" onclick="collectionControl.deletePhoto(); return false;"><strong>Delete</strong></a>
			<a id="remove_photo_control" class="delete" href="" onclick="collectionControl.removeGrabbedPhoto(); return false;" style="display:none;"><strong>Remove</strong></a>
			<span id="image_process_control">
				<input type="button" value="Rotate Right" class="button_v3" onclick="collectionControl.rotateRight(); return false;" />
				<input type="button" value="Rotate Left" class="button_v3" onclick="collectionControl.rotateLeft(); return false;" />
				<?php if (PhotoCollectionConstants::GROUP_TEMPLATE_CONTEXT !== $context): ?>
				<input type="button" value="Thumbnail Crop" class="button_v3" onclick="collectionControl.loadCropper(); return false;" />
				<?php endif; ?>
			</span>
			<input type="button" id="btnSetAsPrimary" value="Set as Primary" class="button_v3" onclick="collectionControl.setAsPrimary(); return false;" />
			<?	if( PhotoCollectionConstants::JOURNAL_CONTEXT == $context || PhotoCollectionConstants::TRAVELLOG_CONTEXT == $context ): ?>
				<input type="button" id="btnSetAsJournalPrimary" value="Set as Journal Primary" class="button_v3" onclick="collectionControl.setAsJournalPrimary(); return false;"/>
			<?	endif; ?>
		</div>
		<div id="request_status" style="display:none;">
			<img src="/images/load_gray.gif" />Sending request. Please wait...
		</div>
	<?php endif; ?>
	
	<div class="viewed_image">
		<?php if( 1 < count($photos) ): ?>
			<div class="viewed_navi">
				<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
				<p name="photo_position_marker" class="jLeft"></p>
				<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
			</div>
		<?php endif; ?>
		<div class="viewed_holder">
			<div class="navhover" id="navhover">
					<a class="prev_photo" id="prev_photo" onclick="collectionControl.movePrev(); return false;" href="javascript:void(0)"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>
					<a class="next_photo" id="next_photo" onclick="collectionControl.moveNext(); return false;" href="javascript:void(0)"><span class="txtblock">Next<span class="imgblock"></span></span></a>
			</div>
			<img id="active_photo" src="" align="middle" border="0" />
		</div>
		<?php if( 1 < count($photos) ): ?>
			<div class="viewed_navi">
				<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
				<p name="photo_position_marker" class="jLeft"></p>
				<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
			</div>
		<?php endif; ?>	
	</div>
</div>