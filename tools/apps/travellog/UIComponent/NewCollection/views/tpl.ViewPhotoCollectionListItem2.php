<?php
	$context = $collectionItem->getContext();
	$href = $collectionItem->getLink();
	$title = stripslashes(strip_tags($collectionItem->getTitle()));
	$title = 10 < strlen($title) ? substr($title,0,10)."..." : $title;
	$title = htmlspecialchars($title,ENT_QUOTES);
	$photocount = $collectionItem->getPhotoCount();
	
	$photo_image_renderer = new GanetPhotoImageRendererComponent($primary, GanetPhotoService::SIZE_THUMBNAIL);
	$photo_image_renderer->set('attributes', array('width'=>'65', 'height'=>'65', 'id'=>'imgPrimary_'.$context.'_'.$collectionItem->getGenID()));
?>

<li id="collection_item_<?=$context?>_<?=$collectionItem->getGenID()?>" name="collection_item" <? if($isActive):?>class="active"<?endif;?>>
	<div style="height: 77px;">
		<span class="album_name">
			<a href="<?=$href?>">
				<?php $photo_image_renderer->render(); ?>
			</a>
		</span>
		<h3>
			<strong id="album_name_<?=$context?>_<?=$collectionItem->getGenID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($collectionItem->getTitle())),ENT_QUOTES)?>" >
				<a id="album_title_<?=$context?>_<?=$collectionItem->getGenID()?>" href="<?=$href?>" >
						<?=$title?>
				</a>
			</strong>
			<?php if( 0 < $userLevel): ?>
				<span id="albumNameForm_<?=$context?>_<?=$collectionItem->getGenID()?>" style="display:none;">
					<form id="frmAlbumName<?=$context?>_<?=$collectionItem->getGenID()?>" onsubmit="collectionControl.saveAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" >
						<input type="text" id="txtAlbumName_<?=$context?>_<?=$collectionItem->getGenID()?>" size="12" value="<?=htmlspecialchars(stripslashes($collectionItem->getTitle()),ENT_QUOTES)?>" />
						<input type="button" id="btnSaveAlbumName" value="Save" onclick="collectionControl.saveAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" />
						<input type="button" id="btnCancelAlbumName" value="Cancel" onclick="collectionControl.cancelEditAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" />
						<input type="hidden" id="curAlbumName_<?=$context?>_<?=$collectionItem->getGenID()?>" value="<?=htmlspecialchars(stripslashes($collectionItem->getTitle()),ENT_QUOTES)?>" />
					</form>
				</span>
			<?php endif; ?>
		</h3>
		<span id="album_info_<?=$context?>_<?=$collectionItem->getGenID()?>" class="album_control">
					<?php if( 0 < $userLevel): ?>
							<a href="javascript:void(0)" onclick="collectionControl.editAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;">Edit Title</a>&nbsp;
							<a href="javascript:void(0)" onclick="collectionControl.deleteAlbum('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;">Delete Album</a>
					<?php endif; ?>
		</span>
	
		<p id="album_photocount_<?=$context?>_<?=$collectionItem->getGenID()?>" style="width:100px;overflow:hidden;">
			<?=(0 < $photocount)? ((1<$photocount)? $photocount." Photos" : $photocount." Photo") : '' ?>
		</p>
	</div>
</li>