<h1>
	<span id="travelTitle"> <?=$title?> </span>
	<? 	if(("photoalbum"==$context || "group_template"==$context) && 1 < $userLevel): ?>
		<div id="album_control" style="float: right;">
			<input class="button_v3" type="button" value="Add Member Photos" onclick="window.location.replace('/collection.php?type=<?=$type?>&ID=<?=$ID?>&context=<?=$context?>&genID=<?=$genID?>&action=6');"/>
		</div>
	<?	endif; ?>
</h1>
<div class="photocontainer">

	<div id="new_uploader">
		
		<?
			$uploadUrl = "/collection.php?type=$type&ID=$ID&context=$context&genID=$genID&action=4&mode=1&batchUploadID=$batchUploadID";
			$redirectUrl = "/collection.php?type=$type&ID=$ID&context=$context&genID=$genID&action=8";
		?>
		<?php if (false): // expired certificate for jumploader applet ?>
		<div class="notifier">
			<p style="margin-bottom:5px;">
			The photo uploader may take a few moments to load. A prompt will pop up. Depending on what computer you are on, you may see the buttons <strong style="font-size:12px;">Trust</strong>, <strong style="font-size:12px;">Run</strong> or <strong style="font-size:12px;">OK</strong>, please click on it to enable the uploader to work.
			</p>
			<p style="margin-bottom:5px;">
				<strong style="font-size:12px;">Just drag and drop!</strong> <a id="lnkInfoDnD" href="" onclick="return false;">Click for more instructions&raquo;</a>
				<div id="info_dnd" style="margin-left:20px; margin-bottom:5px; display: none;">
					Simply drag your photos from your photo managing application and drop them onto the lower right box of the uploader - we strongly recommend this for <strong>iPhoto</strong> and <strong>Aperture</strong> users. 
					<div style="margin-left: 20px;">
						<ul>
							<li>
								- You can also drag your photos from your <strong>Windows Explorer</strong> or <strong>Finder</strong> window.
							</li>
							<li>
								- If you are unable to drop a photo, please check if the photo is within the <strong>2 MB limit per image</strong>. 
							</li>
						</ul>
					</div>
				</div>
			</p>
			<p style="margin-bottom:5px;">
				<strong style="font-size:12px;">Add photos to your album</strong> <a id="lnkInfoAddPhoto" href="" onclick="return false;">Click for more instructions&raquo;</a>
				<div id="info_add_photo" style="margin-bottom:5px; display:none;">
					<div style="margin-left: 20px;">
						<ul>
							<li>
								1. On the right, <strong>select your Pictures or Photos folder</strong>, or the folder that contains the photos that you want to upload.
							</li>
							<li>
								2. On the photo preview, <strong>click on the green plus</strong> or <strong>drag the image</strong> to the lower right box to select it. 
							</li>
							<li>
								3. After selecting photos, <strong>click on the green arrow button</strong> to start uploading.
							</li>
						</ul>
					</div>
					You can upload up to <strong>60 photos</strong> at a time.
				</div>
			</p>
			<p style="margin-bottom:5px;">
			If you are unable to see the tool or your photo previews, <a id="lnkInfoJava" href="" onclick="return false;">Click for more instructions&raquo;</a>
				<span id="info_java" style="margin-left:20px; display:none;">You will need <strong style="font-size:12px;">Java</strong> for the uploader to work. You can <a href="http://www.java.com/en/download/index.jsp">get a quick and free Java download here</a>. If you are on Windows and you see a Java icon [<img src="/images/java_icon.jpg" width="20" height="20" />] on the lower right corner of your screen, simply click it and it will guide you through the installation in a few clicks.</span>
			</p>
		</div>
		<div id="upload_notification" class="upload_notification" style="display: none; margin: 10px 0; text-align: center;">
			<p id="upload_success_notif" style="display:none;">
			<strong>Successfully uploaded <span id="numUploaded"></span></strong>
			</p>
			<p id="upload_fail_notif" style="display:none;">
			<strong>Sorry, the upload utility failed uploading <span id="numFailed"></span> Please try again by clicking on the retry icon. <a href="javascript:void(0)" onclick="retryFailed(); return false;"><img src="/images/uploader_retry.png" title="Retry Upload"/></a></strong>
			</p>
			<p id="upload_unrecognized_type" style="display:none;">
			<strong>We are sorry to inform you that the upload tool failed to process the following image(s): <span id="unrecognizedFiles"></span>. Please use the classic uploader to upload the said file(s).</strong>
			</p>
			<div id="upload_success_action" style="display:none;">
				<p>
				To upload more photos, continue to select and upload using the tool below.
				</p>
				<p>
				To edit or view your album, click on the buttons below.
				</p>
				<p>
					<input type="button" class="button_v3" value="Manage Uploaded Photos" onclick="collectionControl.manageUploadedPhotos(); return false;" id="btnManageUploadedPhotos"/>
					<?php if( "travellog" == $context ): ?>
						<input type="button" class="button_v3" value="Go to Album" onclick="window.location.replace('<?=$collectionDTO->getJournalCollectionLink()?>'); return false;"/>
					<?php else: ?>
						<input type="button" class="button_v3" value="Go to Album" onclick="collectionControl.cancelUpload(); return false;"/>
					<?php endif; ?>
				</p>
			</div>
		</div>
		<div style="text-align: center;margin-top:10px;">
			
			
			<?//	if( "traveler" == $type && 8810 == $ID ): ?>
			<applet name="jumpLoaderApplet"
					codebase="/jumploader/v2.15.0"
					code="jmaster.jumploader.app.JumpLoaderApplet.class"
					archive="./jumploader_messages_properties.zip,jumploader_z.jar"
					width="711"
					height="500" 
					mayscript>
				<param name="uc_uploadUrl" value="<?=$uploadUrl?>" />
				<param name="uc_fileNamePattern" value="^.+\.(?i)((jpg)|(jpe)|(jpeg)|(pjpeg)|(gif)|(png))$" />
				<param name="uc_addImagesOnly" value="true" />
				<param name="uc_maxFiles" value="60" />
				<!--param name="uc_maxFileLength" value="2048000" /-->
				<param name="uc_minimumImageDimension" value="65x65" />
				<param name="uc_imageRotateEnabled" value="false" />
				<param name="uc_imageEditorEnabled" value="false" />
				<param name="uc_fileParameterName" value="SourceFile" />
				<param name="uc_duplicateFileEnabled" value="false" />
				<param name="uc_useLosslessJpegTransformations" value="true" />
			
				<?//	if( "traveler" == $type && 8810 == $ID ): ?>
					<param name="uc_uploadOriginalImage" value="false" />
					<param name="uc_uploadScaledImages" value="true" />
					<param name="uc_scaledInstanceNames" value="SourceFile" />
					<param name="uc_scaledInstanceDimensions" value="1024x1024" />
					<param name="uc_scaledInstanceQualityFactors" value="1000" />
				<?//	endif; ?>
			
				<param name="vc_fileNamePattern" value="^.+\.(?i)((jpg)|(jpe)|(jpeg)|(pjpeg)|(gif)|(png))$" />
				<param name="vc_mainViewFileTreeViewVisible" value="true" />
				<param name="vc_mainViewFileTreeViewWidthPercent" value="30" />
				<param name="vc_mainViewFileListViewVisible" value="true" />
				<param name="vc_mainViewFileListViewHeightPercent" value="40" />
				<param name="vc_disableLocalFileSystem" value="false" />
				<param name="vc_fileListQueueFileOnDblClick" value="true" />
				<param name="vc_fileTreeViewShowFileLength" value="true" />
				<param name="vc_fileTreeViewShowFiles" value="true" />
				<param name="vc_fileListViewShowFolders" value="false" />
				<param name="vc_fileListViewUseThumbs" value="true" />
				<param name="vc_fileListViewLocationBarVisible" value="false" />
				<param name="vc_uploadViewPasteActionVisible" value="false" />
				<param name="vc_uploadViewUseThumbs" value="true" />
				<param name="vc_uploadViewAutoscrollToUploadingFile" value="true" />
				<param name="vc_useThumbs" value="true" /> 
				<param name="vc_lookAndFeel" value="system" />
				<param name="vc_uiDefaults" value="
					FileChooser.openDialogTitleText=Add Photos;
					FileChooser.openButtonText=Add;
					FileChooser.openButtonToolTipText=Add selected photo(s);"/>
 			
				<param name="ac_fireUploaderStatusChanged" value="true" />
				<param name="ac_fireUploaderFileStatusChanged" value="true" />
			
			</applet>
			
		</div>
	<?php endif; ?>
	</div>
	
	<script type="text/javascript">
		var successUploads = 0;
		var failedUploads = 0;
		var unrecognizedFiles = [];
		var _uploader = null;
		function uploaderFileStatusChanged( uploader, file ) {
			if( 3 == file.getStatus() ){
				failedUploads++;
			}else if( 2 == file.getStatus() ){//console.log(file.getResponseContent());
				if( "UNRECOGNIZED_FILE_TYPE" == file.getResponseContent() ){
					unrecognizedFiles.push(file.getName());
				}else{
					successUploads++;
				
					var photo_count = 1;
					var label = jQuery.trim(jQuery("#"+collectionControl.getActiveCollection().getCollectionListItemPhotoCountElementID()).html());
					if( "" != label ){
						photo_count += parseInt(label.split(" ")[0]);
					}
					collectionControl.getActiveCollection().updatePhotoCountLabel(photo_count);
				}
				//alert(file.getResponseContent());
				//uploader.removeFile(file);
			}
		}
		function uploaderStatusChanged( uploader ) {
			if( 1 == uploader.getStatus() ){
				jQuery("#upload_notification").hide();
				jQuery("#upload_fail_notif").hide();
				jQuery("#upload_success_notif").hide();
				jQuery("#upload_success_action").hide();
			}
			if( 0 == uploader.getStatus() ){
				jQuery("#upload_notification").show();
				if( 0 < successUploads ){
					var labelSuccess = "photo.";
					if( 1 < successUploads ){
						labelSuccess = "photos.";
					}
					jQuery("#numUploaded").html(successUploads+" "+labelSuccess);
					jQuery("#upload_success_notif").show();
					jQuery("#upload_success_action").show();
				}
				failedUploads = uploader.getFileCountByStatus(3);
				if( 0 < failedUploads ){
					var labelFailed = "photo.";
					if( 1 < failedUploads ){
						labelFailed = "photos.";
					}
					jQuery("#numFailed").html(failedUploads+" "+labelFailed);
					jQuery("#upload_fail_notif").show();
					_uploader = uploader;
				}
				if( 0 < unrecognizedFiles.length ){
					if( "" == jQuery.trim(jQuery("#unrecognizedFiles").html()) ){
						jQuery("#unrecognizedFiles").html(unrecognizedFiles.join(", "));
					}else{
						jQuery("#unrecognizedFiles").html(jQuery("#unrecognizedFiles").html()+", "+unrecognizedFiles.join(", "));
					}
					jQuery("#upload_unrecognized_type").show();
				}else{
					jQuery("#upload_unrecognized_type").hide();
				}
				unrecognizedFiles = [];
				
				self.scrollTo(0,0);
			}
		}
		function uploaderRetryFileUpload( file ) {
			if( file != null && file.isRetryable() ) {
				var error = _uploader.retryFileUpload( file );
				if( error != null ) {
					alert( error );
				}
			}
		}
		function retryFailed(){
			
			jQuery("#upload_notification").hide();
			jQuery("#upload_fail_notif").hide();
			jQuery("#upload_success_notif").hide();
			jQuery("#upload_success_action").hide();
			
			for(var i=0; i<failedUploads; i++){
				uploaderRetryFileUpload(_uploader.getFile(i));
			}
		}
		
	//	jQuery(document).ready(function(){
	//		collectionControl.showClassicUploader();
	//	});
		
	</script>
	<div id="classic_uploader">
	<?/*	<div class="notifier" style="margin-bottom: 10px;">
			<p>We are upgrading our photo uploader to better serve you. Sorry for the inconvenience. Please use our Classic Uploader while we complete the upgrade.</p>
		</div>
	*/?>
		<div id="classic_uploader_form" style="">
			<div id="uploadnew">
				<span>Upload your photos.</span>
				<p>Select image files from your computer.</p>
			</div>

			<div id="uploadrules">
				<p>
					- You can upload up to a total size of 4MB/image. <br />
					- The following image file formats are allowed: jpg, gif, png. 
				</p>
			</div>
			
			<form method="post" id="frmClassicUploader" name="frmClassicUploader" enctype="multipart/form-data">
				<div id="upload_section">				
						<? for($x=1; $x<=5; $x++): ?>								
							<div class="file_upload">
								<?=$x?><input onchange="collectionControl.validateImageFile(this); return false;"  type="file" id ="SourceFile_<?=$x?>" name="SourceFile_<?=$x?>" size="30" />
							</div>
						<? endfor; ?>
				
					<span id="upload_control">
						<input type="button" class="button_v3" value="Submit" onclick="collectionControl.sendFiles(); return false;"/>
						<?php if( "travellog" == $context ): ?>
							<input type="button" class="button_v3" value="Go to Album" onclick="window.location.replace('<?=$collectionDTO->getJournalCollectionLink()?>'); return false;"/>
						<?php else: ?>
							<input type="button" class="button_v3" value="Go to Album" onclick="collectionControl.cancelUpload(); return false;"/>
						<?php endif; ?>
						<?php if( ("photoalbum"==$context || "group_template"==$context) && 1 < $userLevel ): ?>
							<input class="button_v3" type="button" value="Add Member Photos" onclick="window.location.replace('/collection.php?type=<?=$type?>&ID=<?=$ID?>&context=<?=$context?>&genID=<?=$genID?>&action=6');"/>
						<?php endif; ?>
					</span>
				</div>
				<input type="hidden" id="hdnBatchUploadID" value="<?php echo $batchUploadID;?>" />
			</form>
			<div id="upload_status" style="display:none;">
				<p>
					<img src="/images/load_gray.gif" /> Sending files. Please wait...
				</p>
			</div>
			
		</div>
		<?php if (false): ?>
		<div id="uploader_support" class="notifier" class="margin-top: 12px;">
			<p>
				<strong>Trouble uploading photos?</strong>
			</p>
			<p>
				If you have a slow internet connection, you may want to use our <a href="javascript:void(0)" onclick="jQuery('#uploader_support').hide(); collectionControl.showClassicUploader(); return false;"><strong>Classic Uploader</strong></a> instead.
			</p>
			<p>
				Can't find your <strong>iPhoto</strong> or <strong>Aperture</strong> images? <a id="lnkInfoFindPhoto" href="" onclick="return false;">Here's how&raquo;</a>
				<div id="info_find_photo" style="margin-left: 20px; display:none;">
					Aperture and iPhoto versions 08 and newer
					<ul>
						<li>
							1. Select all the photos that you want to upload by holding down Apple ⌘ while clicking on each photo.
						</li>
						<li>
							2. When you have selected all the photos that you want to upload, go to File>>Export.
						</li>
						<li>
							3. On the pop-up, select:
							<div style="margin-left: 20px;">
								<ul>
									<li>
										-Click on the File Export tab
									</li>
									<li>
										-Under Kind, select JPEG.
									</li>
									<li>
										-Under JPEG Quality, select High
									</li>
									<li>
										-Under Size. select, Large
									</li>
									<li>
										-Click Export.
									</li>
								</ul>
							</div>
						</li>
						<li>
							4. You will be asked to select the location that you want to save the photo to. After selecting a folder, click OK.
						</li>
						<li>
							5. Go back to your GoAbroad Network page and open the photo uploader again. Select the folder that you saved your exported images to and select the images you want to upload.
						</li>
					</ul>
				</div>
			</p>
			<p>
				Need more help? <a href="/feedback.php">Email us &rarr;</a>
			</p>
		</div>
	<?php endif; ?>
	</div>
	
</div>
<script type="text/javascript">
	jQuery("#lnkInfoJava").toggle(
		function(){
			jQuery(this).html("&laquo;Hide instructions");
			jQuery("#info_java").slideDown("fast");
		},
		function(){
			jQuery(this).html("Click for more instructions&raquo;");
			jQuery("#info_java").slideUp("fast");
		}
	);
	jQuery("#lnkInfoDnD").toggle(
		function(){
			jQuery(this).html("&laquo;Hide instructions");
			jQuery("#info_dnd").slideDown("fast");
		},
		function(){
			jQuery(this).html("Click for more instructions&raquo;");
			jQuery("#info_dnd").slideUp("fast");
		}
	);
	
	jQuery("#lnkInfoAddPhoto").toggle(
		function(){
			jQuery(this).html("&laquo;Hide instructions");
			jQuery("#info_add_photo").slideDown("fast");
		},
		function(){
			jQuery(this).html("Click for more instructions&raquo;");
			jQuery("#info_add_photo").slideUp("fast");
		}
	);

	jQuery("#lnkInfoFindPhoto").toggle(
		function(){
			jQuery(this).html("&laquo;Hide");
			jQuery("#info_find_photo").slideDown("fast");
		},
		function(){
			jQuery(this).html("Here's how&raquo;");
			jQuery("#info_find_photo").slideUp("fast");
		}
	);
</script>