<div id="collection_summary_header" class="notification">
	<?php if( 0 == $collectionCount ): ?>  
		<h2>You have not yet uploaded photos.</h2>
		<p>
			To get started, you can add a photo album below.
		</p>
		<p>
			<?php if( PhotoCollectionConstants::TRAVELER_TYPE == $type ): ?>
			To make your photos appear on a journal entry, you first need to <a href="/journal.php?action=add">write a journal</a> and add your photos to a journal entry.
			<?php elseif ( PhotoCollectionConstants::GROUP_TYPE == $type ): ?>
			To make your photos appear on a journal entry, you first need to <a href="/journal.php?action=add&amp;contextGroupID=<?=$ID?>">write a journal</a> and add your photos to a journal entry.
			<?php endif; ?>
		</p>
		<?php if( PhotoCollectionConstants::GROUP_TYPE == $type ): ?>
			<p>
				To add photos uploaded by your members, create an album first. On the next step, you will have the option to choose the member photos that you want to add.
			</p>
		<?php endif; ?>
	<?php else: ?>
		<h1>
			<?php if( 0 < $photocount ): ?>
				You have <?=$collectionCount?> Photo Album<?if(1<$collectionCount):?>s<?endif;?> with <?=$photocount?> Picture<?if(1<$photocount):?>s<?endif;?> all in all.
			<?php else: ?>
				You have <?=$collectionCount?> Photo Album<?if(1<$collectionCount):?>s<?endif;?> without pictures.
			<?php endif; ?>
		</h1>
		<p>
			To add a photo to an existing album, select the album on the right.
		</p>
		<p>
			To add a new album, please use the option below.
		<p>
	<?php endif; ?>
</div>