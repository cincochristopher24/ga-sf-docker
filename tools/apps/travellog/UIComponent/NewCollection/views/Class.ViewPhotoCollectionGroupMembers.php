<?php

	require_once("Class.Template.php");
	require_once('Class.Paging.php');
	require_once('Class.ObjectIterator.php');
	require_once('travellog/model/Class.AdminGroup.php');
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterOp.php');
	require_once('travellog/model/Class.FilterCriteria2.php');
	require_once('travellog/model/Class.RowsLimit.php');
	
	class ViewPhotoCollectionGroupMembers{
		
		private $mCollectionHelper = NULL;
		
		private $mRpp	=	15;
		private $mPage	=	1;
		
		private $mGroupMembers	=	array();
		private $mGroupMembersCount	=	0;
		
		private $mMember	=	NULL;
		
		function setCollectionHelper($collectionHelper=NULL){
			$this->mCollectionHelper = $collectionHelper;
		}
		
		function setPage($page=1){
			$this->mPage = $page;
		}
		
		function setMember($member=NULL){
			$this->mMember = $member;
		}
		
		function render(){
			$this->searchMembers();

			$tpl = new Template();
			$tpl->set("updatesNotifierView",$this->getGroupMemberUpdatesNotifierView());
			$tpl->set("membersListView",$this->getGroupMembersListView());
			$tpl->set("title",htmlspecialchars(stripslashes($this->mCollectionHelper->getActiveCollectionItem()->getTitle()),ENT_QUOTES));
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionGroupMembers.php");
		}
		
		function renderFetchedMembers(){
			$this->searchMembers();
			
			$membersListView = $this->getGroupMembersListView();
			$membersListView->render();
		}
		
		function getGroupMembersListView(){
			$paging = new Paging( $this->mGroupMembersCount, $this->mPage, 'grpID=' . $this->mCollectionHelper->getGroup()->getGroupID(), $this->mRpp );
			$paging->setOnclick("collectionControl.fetchMembers");
			$iterator = new ObjectIterator( $this->mGroupMembers, $paging );
			
			require_once("travellog/UIComponent/NewCollection/views/Class.ViewPhotoCollectionGroupMembersList.php");
			$viewMembers = new ViewPhotoCollectionGroupMembersList();
			$viewMembers->setPaging($paging,$iterator);
			$viewMembers->setGroup($this->mCollectionHelper->getGroup());
			$viewMembers->setMembers($this->mGroupMembers);
			
			return $viewMembers;
		}
		
		function getGroupMemberUpdatesNotifierView(){
			require_once("travellog/UIComponent/NewCollection/views/Class.ViewPhotoCollectionGroupMemberUpdatesNotifier.php");
			$viewNotif = new ViewPhotoCollectionGroupMemberUpdatesNotifier();
			$viewNotif->setGroup($this->mCollectionHelper->getGroup());
			return $viewNotif;
		}
		
		function searchMembers(){
			$orderBy = FilterOp::$ORDER_BY;
			$cond = new Condition;
			$cond->setAttributeName("tblTraveler.firstname");
			$cond->setOperation($orderBy);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			$sp_array = $this->mCollectionHelper->getGroup()->getGroupMembers(NULL,NULL,$order);
			
			$membersWithPhotos = $this->filterMembersWithPhotos($sp_array[0],$this->mCollectionHelper->getGroup()->getGroupID());
			$this->mGroupMembers = $this->limitMembers($this->filterMembersWithPhotos($sp_array[0],$this->mCollectionHelper->getGroup()->getGroupID()),$this->mPage,$this->mRpp);
			$this->mGroupMembersCount = count($membersWithPhotos);
		}
		
		function filterMembersWithPhotos($members=array(),$groupID=0){
			$temp = array();
			foreach($members as $member){
				$photocount = $member->countPhotosToGrabByGroup($groupID);
				if( 0 < $photocount ){
					$temp[] = $member;
				}
			}
			return $temp;
		}
		
		function limitMembers($members=array(), $page=1, $rpp=15){
			$temp = array();
			$count = count($members);
			$offset = $this->calcOffset($page);
			for($i=$offset, $ctr=1; $i<$count && $ctr <= $rpp; $i++,$ctr++ ){
				$temp[] = $members[$i];
			}
			return $temp;
		}
		
		private function calcOffset($page=1){
			return ($page-1) * $this->mRpp;
		}
	}