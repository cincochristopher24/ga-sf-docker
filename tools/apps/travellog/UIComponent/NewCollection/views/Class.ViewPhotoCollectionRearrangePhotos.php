<?php

	require_once("Class.Template.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");

	class ViewPhotoCollectionRearrangePhotos implements PhotoCollectionConstants{
		
		private $mCollectionHelper = NULL;
		
		function setCollectionHelper($collectionHelper=NULL){
			$this->mCollectionHelper = $collectionHelper;
		}
		
		function render(){
			$tpl = new Template();
			if( self::VIEWER == $this->mCollectionHelper->getUserLevel() ){
				$tpl->set("hasError",TRUE);
			}elseif( self::JOURNAL_CONTEXT == $this->mCollectionHelper->getContext() && is_null($this->mCollectionHelper->getTravellogID()) ){
				$tpl->set("hasError",TRUE);
			}else{	
				if( !$this->mCollectionHelper->getActiveCollectionItem() ){
					$tpl->set("hasError",TRUE);
				}else{
					$collectionDTO = $this->mCollectionHelper->getActiveCollectionItem();
					$contextObject = $collectionDTO->getContextObject();
					if( self::PROFILE_CONTEXT != $collectionDTO->getContext() ){
						$collectionDTO->setTitle($contextObject->getTitle());
					}
					$tpl->set("title",htmlspecialchars(stripslashes(strip_tags($collectionDTO->getTitle())),ENT_QUOTES));
					$tpl->set("photos",$contextObject->getPhotos());
					$tpl->set("hasError",FALSE);	
				}				
			}
			$tpl->set("mode",$this->mCollectionHelper->getMode());
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionRearrangePhotos.php");
		}
	}