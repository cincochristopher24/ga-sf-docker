<?php

	require_once("Class.Template.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	
	class ViewPhotoCollectionGroupMemberPhotoList{
		
		private $mMember = NULL;
		private $mGroupPhotoAlbum = NULL;
		private $mGroup = NULL;
		
		function setMember($member=NULL){
			$this->mMember = $member;
		}
		
		function setGroupPhotoAlbum($album=NULL){
			$this->mGroupPhotoAlbum = $album;
		}
		
		function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("member",$this->mMember);
			$tpl->set("groupAlbum",$this->mGroupPhotoAlbum);
			$tpl->set("photos",$this->mMember->getPhotosToGrabByGroup($this->mGroup->getGroupID()));
			$tpl->set("title",htmlspecialchars(stripslashes($this->mGroupPhotoAlbum->getTitle()),ENT_QUOTES));
			if( $this->mGroupPhotoAlbum instanceof GanetPhotoAlbum ){
				//existing should include source photoIDs of grabbed photos
				$tpl->set("existing",array_merge($this->mGroupPhotoAlbum->getPhotoIDs(),$this->mGroupPhotoAlbum->getGrabbedPhotoIDs()));
			}else{
				$tpl->set("existing",$this->mGroupPhotoAlbum->getPhotoIDs());
			}
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionGroupMemberPhotoList.php");
		}
	}