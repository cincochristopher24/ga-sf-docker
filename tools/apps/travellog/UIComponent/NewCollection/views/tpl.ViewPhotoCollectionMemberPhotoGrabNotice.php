<ul class="notifier">
	<li class="last" >
		<?php	if("ADD" == $mode): ?>
			<span><strong><?=$message?></strong> to <strong><?=$title?></strong>.</span>
			<a href="javascript:void(0)" onclick="collectionControl.removeMemberPhoto(); return false;">Remove</a>
			<img src="/images/loading_small.gif" class="remove_grabbed_requesting" style="display:none;"/>
		<?php else: ?>
			<span><strong><?=$message?></strong> from <strong><?=$title?></strong>.</span>
		<?php endif; ?>
	</li>
</ul>