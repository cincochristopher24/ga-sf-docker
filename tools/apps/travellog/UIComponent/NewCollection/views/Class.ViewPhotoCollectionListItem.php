<?php
	require_once("Class.Template.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/views/Class.ViewPhotoCollectionListItem.php");
	
	class ViewPhotoCollectionListItem implements PhotoCollectionConstants{
		
		private $mCollectionItem = FALSE;
		private $mIsActive = FALSE;
		
		function setCollectionItem($collectionItem=FALSE){
			$this->mCollectionItem = $collectionItem;
		}
		function setIsActive($isActive=FALSE){
			$this->mIsActive = $isActive;
		}
		function setUserLevel($userLevel=0){
			$this->mUserLevel = $userLevel;
		}
		
		
		
		function render(){
			$primary = $this->mCollectionItem->getPrimaryPhoto();
			$template_file = ($primary instanceof GanetPhoto || is_null($primary)) ? "travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionListItem2.php" : "travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionListItem.php"; 
			
			$tpl = new Template();
			$tpl->set("collectionItem",$this->mCollectionItem);
			$tpl->set("isActive",$this->mIsActive);
			$tpl->set("userLevel",$this->mUserLevel);
			$tpl->set("primary", $this->mCollectionItem->getPrimaryPhoto());
			$tpl->out($template_file);
		}
		
	}