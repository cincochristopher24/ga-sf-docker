<div id="group_members">
	<div class="media_control">
		<p id="travelTitle" class="jLeft">
			<strong>Adding Member Photos to</strong> <?=$title?>
		</p>
			<input class="button_v3" type="button" value="Back to Album" onclick="window.location.replace(collectionControl.getActiveCollection().getActionPage());"/>
	</div>
	<div class="photocontainer">
		<?=$updatesNotifierView->render()?>
		<div id="members_list">
			<?=$membersListView->render()?>
		</div>
	</div>
	<input type="hidden" id="group_album_title" value="<?=$title?>" />
</div>