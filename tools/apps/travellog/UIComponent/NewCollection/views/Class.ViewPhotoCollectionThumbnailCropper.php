<?php

	require_once("Class.Template.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");

	class ViewPhotoCollectionThumbnailCropper implements PhotoCollectionConstants{
		
		private $mCollectionHelper = NULL;
		
		function setCollectionHelper($collectionHelper=NULL){
			$this->mCollectionHelper = $collectionHelper;
		}
		
		function render(){
			$tpl = new Template();
			if( self::VIEWER == $this->mCollectionHelper->getUserLevel() ){
				$tpl->set("hasError",TRUE);
			}else{
				$params = $this->mCollectionHelper->getParams();
				if( !isset($params["photoID"]) ){
					$tpl->set("hasError",TRUE);
				}else{
					$mCollection = $this->mCollectionHelper->getActiveCollectionItem();

					$mPhotoContext = $mCollection->getContextObject();

					$file_factory = FileFactory::getInstance();
					$mPhoto = $file_factory->getClass('Photo', array($mPhotoContext,$params["photoID"]));
					
					if( $mPhoto instanceof Photo ){
						
						$fullsize = $mPhoto->getPhotoLink('fullsize');
						
						$imagetype = getimagesize($fullsize);		
						$image = FALSE;

						switch (substr($imagetype['mime'],6,4)) {
								case self::JPEG	:
									   	$image = imagecreatefromjpeg($fullsize); 
										break;
								case self::GIF	:					
									   	$image = imagecreatefromgif($fullsize); 	
										break;
								case self::PNG	:
										$image = imagecreatefrompng($fullsize); 						
									  	break;
								default	:
										$image = imagecreatefromjpeg($fullsize); 
										break;		
						}
						if( $image ){
							$fullWidth = imagesx($image);
							$fullHeight = imagesy($image);
							imagedestroy($image);
							
							$tpl->set("hasError",FALSE);
							$tpl->set("Fullsize",$fullsize);
							$tpl->set("Thumbnail",$mPhoto->getPhotoLink('thumbnail'));
							$tpl->set("fullWidth",$fullWidth);
							$tpl->set("fullHeight",$fullHeight);
						}else{
							$tpl->set("hasError",TRUE);
						}
					}else{
						$tpl->set("hasError",TRUE);
					}
				}
			}
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionThumbnailCropper.php");
		}
	}