<?php

	require_once("Class.Template.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	
	class ViewPhotoCollectionContentControl implements PhotoCollectionConstants{
		
		private $mCollectionHelper = NULL;
		
		function setCollectionHelper($collectionHelper=NULL){
			$this->mCollectionHelper = $collectionHelper;
		}
		
		function render(){
			if( PhotoCollectionConstants::VIEW_COLLECTION == $this->mCollectionHelper->getAction() ){
				if( PhotoCollectionConstants::VIEWER < $this->mCollectionHelper->getUserLevel() && is_null($this->mCollectionHelper->getGenID()) ){
					return;
				}
				$tpl = new Template();
				$tpl->set("collectionItem",$this->mCollectionHelper->getActiveCollectionItem());
				$tpl->set("userLevel",$this->mCollectionHelper->getUserLevel());
				$tpl->set("action",$this->mCollectionHelper->getAction());
				$tpl->set("group",$this->mCollectionHelper->getGroup());
				$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentControl.php");
			}
		}
	}