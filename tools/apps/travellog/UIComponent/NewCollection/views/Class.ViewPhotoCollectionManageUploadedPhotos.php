<?php

	require_once("Class.Template.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");

	class ViewPhotoCollectionManageUploadedPhotos implements PhotoCollectionConstants{
		
		private $mCollectionHelper = NULL;
		
		function setCollectionHelper($collectionHelper=NULL){
			$this->mCollectionHelper = $collectionHelper;
		}
		
		function render(){
			$content = new Template();
			$content->set("userLevel",$this->mCollectionHelper->getUserLevel());
			$content->set("type",$this->mCollectionHelper->getType());
			$content->set("ID",$this->mCollectionHelper->getID());
			$content->set("context",$this->mCollectionHelper->getContext());
			$content->set("genID",$this->mCollectionHelper->getGenID());
			$content->set("title",htmlspecialchars(stripslashes($this->mCollectionHelper->getActiveCollectionItem()->getTitle()),ENT_QUOTES));
			$content->set("photoContext",$this->mCollectionHelper->getActiveCollectionItem()->getContextObject());
			$content->set("photos",$this->mCollectionHelper->getActiveCollectionItem()->getContextObject()->getPhotos());
			$content->set("collectionDTO",$this->mCollectionHelper->getActiveCollectionItem());
			$content->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionManageUploadedPhotos.php");
		}
	}