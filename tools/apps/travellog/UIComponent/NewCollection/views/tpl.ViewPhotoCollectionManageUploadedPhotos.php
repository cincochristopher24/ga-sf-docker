<h1><span id="travelTitle"> <?=stripslashes($title)?> </span></h1>
<div class="content">
	<form id="frmManagePhotos" action="" method="post">
		<ul class="uploadlist">
			<?php	
				foreach($photos as $x => $photo): 
					$grabbed = FALSE;
					if( PhotoCollectionConstants::ALBUM_CONTEXT==$context 
							&& PhotoCollectionConstants::GROUP_TYPE == $type 
							&& (PhotoCollectionConstants::PHOTOALBUM != $photo->getPhototypeID() 
								|| $photoContext->isGrabbedPhoto($photo->getPhotoID())) ){
						$grabbed = TRUE;			
					}
					$isPrimary = FALSE;
					if( "travellog" == $context || "profile" == $context || "article" == $context){
						$isPrimary = ( 1==$photo->getPrimaryPhoto() )? TRUE : FALSE;
					}else{
						$isPrimary = ( $photo->getPhotoID() == $photoContext->getPrimary() )? TRUE : FALSE;
					}
			?>
				<li id="uploaded_photo_<?=$photo->getPhotoID()?>" <?if( 0 == ($x % 2) ): ?>class="clear"<?endif;?> >
					<div class="uploaded_photo" style="border: none;">
						<img id="standard_<?=$photo->getPhotoID()?>" src="<?=$photo->getPhotoLink('standard')?>" style="border: 2px solid #eee;" />
					</div>
					<div name="image_control" class="thumb_option">
						<img id="thumbnail_<?=$photo->getPhotoID()?>" src="<?=$photo->getPhotoLink('thumbnail')?>">
						<img id="manage_status_<?=$photo->getPhotoID()?>" src="/images/loading_small.gif" style="display:none;" />
						<?	if( !$grabbed ): ?>
							
							<?php if (PhotoCollectionConstants::GROUP_TEMPLATE_CONTEXT !== $context): ?>
								<input type="button" value="Thumbnail Crop" class="button_v3" onclick="collectionControl.loadCropper(<?=$photo->getPhotoID()?>); return false;"/>
							<?php endif; ?>
							
							<input type="button" value="Rotate Left" class="button_v3" onclick="collectionControl.rotateLeft2(<?=$photo->getPhotoID()?>); return false;"/>
							<input type="button" value="Rotate Right" class="button_v3" onclick="collectionControl.rotateRight2(<?=$photo->getPhotoID()?>); return false;" />
						<?	endif; ?>
					</div>
					<div class="comment_field">
						<?php if( $isPrimary ): ?>
							<span><input type="radio" name="radPrimary" value="<?=$photo->getPhotoID()?>" checked="checked" /> Set as Primary Photo</span>
						<?php else: ?>
							<span><input type="radio" name="radPrimary" value="<?=$photo->getPhotoID()?>" /> Set as Primary Photo</span>
						<?php endif; ?>
						
						<?php if( !$grabbed ): ?>
							<p><input type="checkbox" name="chkDeletePhoto" value="<?=$photo->getPhotoID()?>" onclick="collectionControl.updateDeleteMarkedButton(this);" />Mark for Deletion</p>
						<?php endif; ?>
						
						<?php $caption = htmlspecialchars(stripslashes($photo->getCaption()),ENT_QUOTES); ?>
							<?php if( $grabbed ):  ?>
								<?php if( 0 < strlen($caption) ): ?>
									<div class="grabbed_caption"><?=$caption?></div>
								<?php endif; ?>
							<?php else: ?>
								<textarea style="width:275px;height:50px;" name="txtCaption_<?=$photo->getPhotoID()?>" onchange="collectionControl.captionChanged(this); return false;" onkeyup="collectionControl.captionChanged(this); return false;" ><?=$caption?></textarea>
							<?php endif; ?>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>		
		<div class="photoaction_bottom">
			<p id="manage_status" style="display:none;">
				<img src="/images/load_gray.gif" />Sending request. Please wait...
			</p>
			<p id="manage_control">
				<input id="btnDeleteMarked" class="button_v3" type="button" value="Delete Marked Photos" onclick="collectionControl.deleteMarked(); return false;" style="display:none;"/>&nbsp;&nbsp;			
				<input class="button_v3" type="button" value="Save Changes" onclick="collectionControl.saveChanges(); return false;"/>
				<input class="button_v3" type="button" value="Upload More" onclick="collectionControl.loadUploader(); return false;">	
				<?php if( "travellog" == $context ): ?>
					<input type="button" class="button_v3" value="Back to Album View" onclick="window.location.replace('<?=$collectionDTO->getJournalCollectionLink()?>'); return false;"/>
				<?php else: ?>
					<input type="button" class="button_v3" value="Back to Album View" onclick="collectionControl.cancelUpload(); return false;"/>
				<?php endif; ?>
			</p>
		</div>
	</form>	
</div>
<script type="text/javascript">
	collectionControl.setAction(8);
</script>