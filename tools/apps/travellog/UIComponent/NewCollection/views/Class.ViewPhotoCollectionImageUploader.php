<?php

	require_once("Class.Template.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	
	class ViewPhotoCollectionImageUploader implements PhotoCollectionConstants{
		
		private $mCollectionHelper = NULL;
		
		function setCollectionHelper($collectionHelper=NULL){
			$this->mCollectionHelper = $collectionHelper;
		}
		
		function render(){
			$activeCollection = $this->mCollectionHelper->getActiveCollectionItem();
			$tpl = new Template();
			$tpl->set("title",htmlspecialchars(stripslashes($activeCollection->getTitle()),ENT_QUOTES));
			$tpl->set("type",$activeCollection->getType());
			$tpl->set("ID",$activeCollection->getID());
			$tpl->set("context",$activeCollection->getContext());
			$tpl->set("genID",$activeCollection->getGenID());
			$tpl->set("loggedID",$this->mCollectionHelper->getLoggedID());
			$tpl->set("userLevel",$this->mCollectionHelper->getUserLevel());
			$tpl->set("collectionDTO",$activeCollection);
			$tpl->set("batchUploadID",uniqid().time());
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionImageUploader.php");
		}
	}