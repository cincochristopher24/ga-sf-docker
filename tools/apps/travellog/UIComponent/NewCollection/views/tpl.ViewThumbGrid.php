<?	if($photocount >0):
		$pID = $photos[0]->getPhotoID();
?>
		<div class="journalviewer">
			<ul class="photolist">
				<?	for( $i=0; $i<3 && $i<$photocount; $i++){
						$photo = $photos[$i];
				?>												
						<li>
							<a href="javascript:void(0)" onclick="thumbGridControl.showGallery(<?=$photo->getPhotoID()?>,<?=$params['genid']?>,<?=$params['loginid']?>,'<?=$params['context']?>'); return false;">
								<img src="<?=$photo->getPhotolink('thumbnail');?>" border="0" align="left" width="65" height="65"/>
							</a>
						</li>
				<? 	} ?>
			</ul>

			<?	if( $photocount > 3 ): ?>

						<ul id="grid_expansion" class="photolist" style="display:none;">
							<? 	for( $i=3; $i<$photocount; $i++){ 
									$photo = $photos[$i];
							?>
									<li>
										<a href="javascript:void(0)" onclick="thumbGridControl.showGallery(<?=$photo->getPhotoID()?>,<?=$params['genid']?>,<?=$params['loginid']?>,'<?=$params['context']?>'); return false;">
											<img src="<?=$photo->getPhotolink('thumbnail');?>" border="0" align="left" width="65" height="65"/>
										</a>
									</li>
							<? 	} ?>
						</ul>

			<?	endif; ?>


			<?	if( $photocount > 0 ): ?>
				<div class="journalsmallcontrol">
					<? if( $params["ownerLogged"] ): ?>
						<?	if( isset($params["group_id"]) ){
								$link = "/collection.php?type=group&ID=".$params['group_id']."&context=".$params['context']."&genID=".$params['genid'];
							}else{
								$link = "/collection.php?type=traveler&ID=".$params['loginid']."&context=".$params['context']."&genID=".$params['genid'];
							}
						?>
						<a href="<?=$link?>" class="leftside">
							manage entry photos
						</a>
					<? else: ?>
						<a href="javascript:void(0)" onclick="thumbGridControl.showGallery(<?=$pID?>,<?=$params['genid']?>,<?=$params['loginid']?>,'<?=$params['context']?>'); return false;" class="leftside">
							go to photo gallery
						</a>
					<? endif; ?>
					
					<? if( $photocount > 3 ): ?>
						<span id="expand_grid"><a href="javascript:void(0)" onclick="thumbGridControl.expandGrid(); return false;" class="rightside">view more +</a></span>
						<span id="collapse_grid" style="display: none;"><a href="javascript:void(0)" onclick="thumbGridControl.collapseGrid(); return false;" class="rightside">minimize -</a></span>
					<? endif; ?>
				</div>
			<?endif;?>
		</div>
<?	endif; ?>