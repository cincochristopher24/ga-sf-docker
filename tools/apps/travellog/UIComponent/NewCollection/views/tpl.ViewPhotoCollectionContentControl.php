<?php if( PhotoCollectionConstants::VIEW_COLLECTION == $action && $collectionItem ): ?>
	
	<?php
		function truncateText($text,$max){
			if( $max < strlen($text) ){
				return substr($text,0,$max)."...";
			}
			return $text;
		}
		
		$albumIsFeatured = 0;
		$photoContext = $collectionItem->getContextObject();
		$photoCount = $collectionItem->getPhotoCount();
		$isPrivileged = PhotoCollectionConstants::VIEWER < $userLevel;
		$type = $collectionItem->getType();
		$ID = $collectionItem->getID();
		$context = $collectionItem->getContext();
		$genID = $collectionItem->getGenID();
		$photos = $collectionItem->getPhotoInstances();
	?>
	
	<div id="album_menu" <?if(PhotoCollectionConstants::VIEWER==$userLevel && PhotoCollectionConstants::JOURNAL_CONTEXT!=$context):?>style="display:none;"<?endif;?> class="media_control">
		<?php if( $isPrivileged && 0 < $photoCount ): ?>
			<input type="button" id="btnRearrange" value="Rearrange Photos" class="button_v3" class="button_v3" onclick="collectionControl.loadRearrangeView(); return false;" <?if(PhotoCollectionConstants::JOURNAL_CONTEXT==$context):?>style="display:none;"<?endif;?>/>
		<?php endif; ?>
	
		<?php if( PhotoCollectionConstants::ALBUM_CONTEXT == $context && $isPrivileged ): ?>
			<?php
				$albumIsFeatured = $photoContext->getIsFeatured(); 
				$featuredAlbumCount = $group->getFeaturedAlbumCount();
			?>
			<?php if( $albumIsFeatured || !$albumIsFeatured && $featuredAlbumCount < 3 ): ?>
				<input class="button_v3" type="button" value="<?=$photoContext->isFeatured()? 'Remove from Featured' : 'Feature in Homepage'?>" id="btnFeatureAlbum" onclick="collectionControl.featureAlbum(); return false;"/>
			<?php endif; ?>
			<script type="text/javascript">
				<?php if( $albumIsFeatured ): ?>
					collectionControl.albumIsFeatured = 1;
				<?php endif; ?>
				collectionControl.featuredAlbumCount = <?=$featuredAlbumCount?>;
			</script>
			<input class="button_v3" type="button" value="Add Member Photos" onclick="window.location.replace('/collection.php?type=<?=$type?>&amp;ID=<?=$ID?>&amp;context=<?=$context?>&amp;genID=<?=$genID?>&amp;action=6');"/>
		<?php endif; ?>
		
		<?php if( PhotoCollectionConstants::GROUP_TEMPLATE_CONTEXT == $context && $isPrivileged ): ?>
			<input class="button_v3" type="button" value="Add Member Photos" onclick="window.location.replace('/collection.php?type=<?=$type?>&amp;ID=<?=$ID?>&amp;context=<?=$context?>&amp;genID=<?=$genID?>&amp;action=6');"/>
		<?php endif; ?>
		
		<?php if( PhotoCollectionConstants::TRAVELLOG_CONTEXT == $context && $isPrivileged ): ?>
			<input type="button" class="button_v3" value="Go to Journal" onclick="window.location.replace('/travellog.php?action=view&amp;travellogID=<?=$genID?>')"/>
		<?php endif; ?>
	
		<?php if( PhotoCollectionConstants::ARTICLE_CONTEXT == $context && $isPrivileged ): ?>
			<input type="button" class="button_v3" value="Go to Article" onclick="window.location.replace('/article.php?action=view&amp;articleID=<?=$genID?>')"/>
		<?php endif; ?>
	
		<?php if( $isPrivileged ): ?>
			<?php if( PhotoCollectionConstants::JOURNAL_CONTEXT == $context ): ?>
				<input type="button" id="btnUpload" class="button_v3" value="<?=(0==count($photos))? 'Add Photos' : 'Add More Photos';?>" style="display:none;"onclick="collectionControl.loadUploader(); return false;"/>
			<?php else: ?>
				<input type="button" id="btnUpload" class="button_v3" value="Add More Photos" onclick="collectionControl.loadUploader(); return false;"/>
			<?php endif; ?>
		<?php endif; ?>
	
		<?php if( PhotoCollectionConstants::JOURNAL_CONTEXT == $context ): ?>
				<div>
					<strong>Select an Entry:</strong>
					<select id="cboTravelLogs" onchange="collectionControl.setActiveTravelLog(); return false;">
						<option value="0" selected>-- Displaying all entries --</option>
					<?php foreach($collectionItem->getTravellogObjects() as $travellog): ?>
						<option id="travellog_option_<?=$travellog->getTravelLogID()?>" value="<?=$travellog->getTravelLogID()?>"><?=truncateText(htmlspecialchars(stripslashes($travellog->getTitle()),ENT_QUOTES),40)?></option>
					<?php endforeach; ?>
					</select>
					<?php foreach($collectionItem->getTravellogObjects() as $travellog): ?>
						<input type="hidden" id="travellog_title_<?=$travellog->getTravelLogID()?>" value="<?=htmlspecialchars(stripslashes(strip_tags($travellog->getTitle())),ENT_QUOTES)?>" />
						<input type="hidden" id="travellog_link_<?=$travellog->getTravelLogID()?>" value="<?=$travellog->getFriendlyURL()?>" />
					<?php endforeach; ?>
				</div>
		<?php elseif( PhotoCollectionConstants::TRAVELLOG_CONTEXT == $context ): ?>
			<input type="hidden" id="travellog_title_<?=$collectionItem->getContextObject()->getTravelLogID()?>" value="<?=htmlspecialchars(stripslashes(strip_tags($collectionItem->getContextObject()->getTitle())),ENT_QUOTES)?>" />
			<input type="hidden" id="travellog_link_<?=$collectionItem->getContextObject()->getTravelLogID()?>" value="<?=$collectionItem->getContextObject()->getFriendlyURL()?>" />
		<?php elseif( PhotoCollectionConstants::ARTICLE_CONTEXT == $context ): ?>
			<input type="hidden" id="article_title_<?=$photoContext->getArticleID()?>" value="<?=htmlspecialchars(stripslashes(strip_tags($photoContext->getTitle())),ENT_QUOTES)?>" />
			<input type="hidden" id="article_link_<?=$photoContext->getArticleID()?>" value="/article.php?action=view&amp;articleID=<?=$photoContext->getArticleID()?>" />
		<?php endif; ?>
	</div>

<?php endif; ?>