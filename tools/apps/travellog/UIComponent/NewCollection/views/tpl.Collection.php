<?=$contextInfo->render()?>		
<?=$subNavigation->show()?>

<?php AbstractNewCollectionController::resolveCollectionHelper(); ?>

			<div class="" style="margin: 0 auto; padding: 20px; width: 980px;">
				<?php if( isset($fbLike) ): ?>
					<?php echo $fbLike->render(); ?>
				<?php endif; ?>
				<?php if( isset($sharebar) ): ?>
					<?php echo $sharebar->render(); ?>
				<?php endif; ?>
			</div>
			
<div id = "content_wrapper" class="control_divider" >
	<div id = "rundown">
		<?php if ($this->hasVar('copying_notice_component')): ?>
			<?php $this->getVar('copying_notice_component')->render(); ?>
		<?php endif; ?>
		
		<div id = "wide_column" class = "layout2" >
			<?	if( $isPrivileged ): ?>
				<div id="delete_album_notice" style="margin-bottom: 10px; display:none;" class="errors">
					<p align="center">
						<strong>WARNING!!!</strong>
					</p>
					<p>
						The album <strong><span id="title_of_album_to_delete"></span></strong> has <strong><span id="album_photo_count"></span></strong> in it. Deleting the album will erase <span id="photo_count_description"></span>.
					</p>
					<p>
						Check this box <input type="checkbox" id="chkDeleteNonEmptyAlbum" /> if you are sure you want to do this.
					</p>
					<p>
						Click on the <strong>Delete Album</strong> button below to continue.
					</p>
					<div id="delete_album_links">
						<input type="button" id="btnContinueDeleteNonEmptyAlbum" value="Delete Album" class="button_v3" onclick="collectionControl.continueDeleteNonEmptyAlbum(); return false;"/>
						<input type="button" id="btnCancelDeleteNonEmptyAlbum" value="Cancel" class="button_v3" onclick="collectionControl.cancelDeleteNonEmptyAlbum(); return false;"/>
					</div>
					<div id="sendingDeleteNonEmptyAlbumStatus" style="display:none;">
						<img src="/images/load_gray.gif"/> Please wait while sending request...
					</div>
				</div>
			<?	endif; ?>
			
			<?php if( isset($collectionControl) ): ?>
				<?=$collectionControl->render()?>
			<?php endif; ?>
			
			<div id="photo_main_view" class="section">
					<?=$content->render()?>
			</div>
			
			<div id="rearrange_loader" class="section" style="display:none;">
				<img src="/images/load_gray.gif" />
			</div>
			<div id="rearrange_area" class="section">
			</div>
			
		</div>
		<div id = "narrow_column" >
			<div class = "section nbsection" >
				<?=$collection->render()?>
			</div>
		</div>
		<div id="cache_element"></div>
	</div>
</div>