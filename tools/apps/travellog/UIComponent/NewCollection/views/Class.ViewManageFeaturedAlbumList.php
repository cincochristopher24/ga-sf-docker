<?php

	require_once('Class.Template.php');
	
	class ViewManageFeaturedAlbumList{
		private $mAlbums = array();
		private $mIsFeatured = FALSE;
		
		function setAlbums($albums=array()){
			$this->mAlbums = $albums;
		}
		function setIsFeatured($featured=FALSE){
			$this->mIsFeatured = $featured;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("albums",$this->mAlbums);
			$tpl->set("isFeatured",$this->mIsFeatured);
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewManageFeaturedAlbumList.php");
		}
	}