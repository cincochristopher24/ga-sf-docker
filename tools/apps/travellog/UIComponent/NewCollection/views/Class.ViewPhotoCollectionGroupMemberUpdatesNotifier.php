<?php
	require_once("Class.Template.php");

	class ViewPhotoCollectionGroupMemberUpdatesNotifier{
		
		private $mGroup = NULL;
		
		function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		
		function render(){
			require_once('travellog/model/Class.ActivityFeed.php');
			$rowsLimit = new RowsLimit(5,0);
			$params = array("FEED_SECTION"		=>	ActivityFeed::ENTRYPHOTO,
							"GROUP_OBJECT"		=>	$this->mGroup,
							"ROWS_LIMIT"		=>	$rowsLimit );
			$feeds = ActivityFeed::getRecentMemberUpdates($params);
			$tpl = new Template();			
			$tpl->set("updates",$feeds["activityFeeds"]);
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionGroupMemberUpdatesNotifier.php");
		}
	}