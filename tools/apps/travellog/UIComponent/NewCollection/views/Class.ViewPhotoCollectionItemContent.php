<?php
	require_once("Class.Template.php");
	require_once("travellog/UIComponent/NewCollection/helper/Class.PhotoCollectionHelper.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/views/Class.ViewPhotoCollectionContentControl.php");
	
	class ViewPhotoCollectionItemContent implements PhotoCollectionConstants{
		
		private $mCollectionHelper = NULL;
		
		function setCollectionHelper($collectionHelper=NULL){
			$this->mCollectionHelper = $collectionHelper;
		}
		
		function getCollectionControlView(){
			$controlView = new ViewPhotoCollectionContentControl();
			$controlView->setCollectionHelper($this->mCollectionHelper);
			return $controlView;
		}
		
		function render(){
			if( self::VIEWER == $this->mCollectionHelper->getUserLevel() ){
				$this->renderContent();
			}else{
				$this->renderPrivilegedView();
			}
		}
		
		function renderContent(){
			$this->renderContentHead();
			$this->renderContentPhotos();
			$this->renderContentCaption();
		}
		
		function renderPrivilegedView(){
			$collectionItem = $this->mCollectionHelper->getActiveCollectionItem();

			if( is_null($this->mCollectionHelper->getGenID()) ){
				$content = new Template();
				$content->set("type",$this->mCollectionHelper->getType());
				$content->set("ID",$this->mCollectionHelper->getID());
				$content->set("context",$this->mCollectionHelper->getContext());
				$content->set("genID",$this->mCollectionHelper->getGenID());
				
				$content->set("collectionCount",count($this->mCollectionHelper->getCollection()));
				$content->set("photocount",$this->mCollectionHelper->getAllCollectionPhotoCount());
				$content->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentSummaryHeader.php");
				
				$createAlbumTpl = new Template();
				$createAlbumTpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionFormCreateAlbum.php");
				
				if( self::GROUP_TYPE == $this->mCollectionHelper->getType() ){
					$content = new Template();
					$content->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionGroupFeaturedAlbums.php");
				}
			}else{
				if( 0 < $collectionItem->getPhotoCount() ){
					$this->renderContent();
				}else{
					if( self::JOURNAL_CONTEXT == $collectionItem->getContext() ){
						$travellogs = $collectionItem->getTravellogObjects();
						if( 1 == count($travellogs) ){
							$travellog = $travellogs[0];
							header("location: /collection.php?type=".$collectionItem->getType()."&ID=".$collectionItem->getID()."&context=travellog&genID=".$travellog->getTravelLogID()."&action=3");
						}
						$journalSummaryTpl = new Template();
						$journalSummaryTpl->set("travellogs",$travellogs);
						$journalSummaryTpl->set("type",$collectionItem->getType());
						$journalSummaryTpl->set("ID",$collectionItem->getID());
						$journalSummaryTpl->set("genID",$collectionItem->getGenID());
						$journalSummaryTpl->set("context",$collectionItem->getContext());
						$journalSummaryTpl->set("userLevel",$this->mCollectionHelper->getUserLevel());
						$journalSummaryTpl->set("title",htmlspecialchars(stripslashes($collectionItem->getTitle()),ENT_QUOTES));
						$journalSummaryTpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentJournalSummaryHeader.php");
					}else{
						header("location: /collection.php?type=".$collectionItem->getType()."&ID=".$collectionItem->getID()."&context=".$collectionItem->getContext()."&genID=".$collectionItem->getGenID()."&action=3");
					}
				}
			}
		}
		
		function renderContentHead(){
			$collectionItem = $this->mCollectionHelper->getActiveCollectionItem();
			
			$headTpl = new Template();
			$headTpl->set("photos",$collectionItem->getPhotoIDs());
			$headTpl->set("title",$collectionItem->getTitle());
			$headTpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentHead.php");
		}
		
		function renderContentPhotos(){
			$collectionItem = $this->mCollectionHelper->getActiveCollectionItem();
			
			$listViewTpl = new Template();
			$listViewTpl->set("userLevel",$this->mCollectionHelper->getUserLevel());
			$listViewTpl->set("collectionItem",$collectionItem);
			$listViewTpl->set("context",$collectionItem->getContext());
			$listViewTpl->set("photos",$collectionItem->getPhotoInstances());
			$listViewTpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentThumbnailListView.php");
		
			$galleryViewTpl = new Template();
			$galleryViewTpl->set("isPrivileged",self::VIEWER < $this->mCollectionHelper->getUserLevel());
			$galleryViewTpl->set("context",$collectionItem->getContext());
			$galleryViewTpl->set("photos",$collectionItem->getPhotoIDs());
			$galleryViewTpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentGalleryView.php");
		}
		
		function renderContentCaption(){
			$collectionItem = $this->mCollectionHelper->getActiveCollectionItem();
			$captionTpl = new Template();
			$captionTpl->set("isPrivileged",self::VIEWER < $this->mCollectionHelper->getUserLevel());
			$captionTpl->set("context",$collectionItem->getContext());
			$captionTpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentPhotoCaption.php");
		}
	}