<?php
require_once("travellog/UIComponent/NewCollection/model/Class.AbstractUploadedPhoto.php");

class UploadedPhotoGroupTemplate extends AbstractUploadedPhoto {
		
	public function __construct() {
		$this->mContext = self::GROUP_TEMPLATE_CONTEXT;
	}
	
	public function makeResizedCopy($oldPathFilename, $imageCopy, $attrib, $size, $newPathFilename, $fit = FALSE) {
		ini_set("memory_limit",$this->getMemoryForImage($oldPathFilename)."M");
		
		$new_attrib = ($fit) ? array('width' => $size, 'height' => $size) : $this->getRescaledDimension($attrib["width"], $attrib["height"], $size);
		$image_p = @imagecreatetruecolor($new_attrib['width'], $new_attrib['height']);
		imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $new_attrib['width'], $new_attrib['height'], $attrib['width'], $attrib['height']);  
						
		switch (exif_imagetype($oldPathFilename)) {
			case IMAGETYPE_JPEG:    	
				imagejpeg($image_p, $newPathFilename, 90);
			break;
			
			case IMAGETYPE_GIF:					   			
				imagegif($image_p, $newPathFilename);
			break;
					
			case IMAGETYPE_PNG:						     	
				imagepng($image_p, $newPathFilename);
			break;
					
			default:    	
				imagejpeg($image_p, $newPathFilename, 90);		
		}
		
		imagedestroy($image_p);
	}
	
	public function processFile(PathManager $pm, $context = null) {
		
	}
	
	public function processFile2($context_object) {
		$this->mContextObject = $context_object;
		$photo_context = GanetPhotoContextBuilderRegistry::getInstance()->getPhotoContext($context_object->getContextID(), 3);
		
		$this->mFilename = uniqid().time().image_type_to_extension(exif_imagetype($this->mFile["tmp_name"]));
		$this->mRelPath = $photo_context->getRelativePath().'/';
		$this->mPathFilename = $this->mFile["tmp_name"];
		chmod($photo_context->getAbsolutePath(), 0777);
			
		$this->mImageCopy = $this->getImageCopy($this->mFile["tmp_name"]);
		$this->mAttrib = $this->getDimension($this->mFile["tmp_name"]);

		$this->makeResizedCopy($this->mPathFilename, $this->mImageCopy, $this->mAttrib, self::FULLSIZE, $this->mRelPath."fullsize".$this->mFilename);
		$this->createStandard();
		$this->createFeatured();
			
		imagedestroy($this->mImageCopy);
		
		$this->sendS3RawFiles();
		
		$photo = new GanetPhoto();
		$photo->setFilename($this->mFilename);
		$photo->setAlbumID($this->mContextObject->getID());
		$photo->setDateAdded(new DateTime());
		$photo->save();
		
		$this->mContextObject->setPhotosCount($this->mContextObject->getPhotosCount() + 1);
		$this->mContextObject->setDateUpdated(new DateTime());
		if (0 == $this->mContextObject->getPrimaryPhotoID()) {
			$this->mContextObject->setPrimaryPhotoID($photo->getID());
		}
		$this->mContextObject->save();
	}
	
	public function getFileName() {
		return $this->mFilename;
	}
	
	public function rotatePhoto2(GanetPhotoAlbum $context_object, GanetPhoto $photo, $angle) {
		$this->mContextObject = $context_object;
		$photo_context = GanetPhotoContextBuilderRegistry::getInstance()->getPhotoContext($context_object->getContextID(), 3);
		$relPath = $photo_context->getRelativePath()."/";
		$filename = $photo->getFileName();
		
		//fullsize
		$fullsize_file = $relPath."fullsize".$filename;
		$fullsize_copy = $this->getImageCopy($fullsize_file);
		$this->rotateImage($fullsize_copy,$angle,$fullsize_file);
		imagedestroy($fullsize_copy);
		
		//standard
		$standard_file = $relPath."standard".$filename;
		$standard_copy = $this->getImageCopy($standard_file);
		$this->rotateImage($standard_copy,$angle,$standard_file);
		imagedestroy($standard_copy);
		
		//thumbnail
		$thumbnail_file = $relPath."thumbnail".$filename;
		$thumbnail_copy = $this->getImageCopy($thumbnail_file);
		$this->rotateImage($thumbnail_copy,$angle,$thumbnail_file);
		imagedestroy($thumbnail_copy);
		
		//prepare a new filename
		$imagetype = $this->getImageType($relPath."thumbnail".$filename);
		$newFilename = $this->generateNewFilename(substr($imagetype['mime'],6,4));
		
		$this->renameFile($relPath,$filename,$newFilename);
		
		$photo->setFileName($newFilename);
		$photo->save();
	}
	
	function rotatePhoto(PathManager $pm, $context_obj, $photo_obj, $angle=0){

	}
	
	public function getShareLink(Photo $photo){
		return FALSE;
	}
}