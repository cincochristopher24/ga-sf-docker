<?php
	require_once("travellog/UIComponent/NewCollection/model/Class.AbstractUploadedPhoto.php");

	class UploadedPhotoCustomHeader extends AbstractUploadedPhoto{
		
		public function __construct(){
			$this->mContext = self::CUSTOMHEADER_CONTEXT;
		}
		
		public function setFile($file=NULL){
			$this->mFile = $file;
		}
		
		public function processFile(PathManager $pm, $objContext=NULL){
			if( !$this->prepareFile($pm,$objContext) ){
				return FALSE;
			}
			
			if( !$this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
				unlink($this->mPathFilename);
				return FALSE;
			}
			$this->createCustomPageHeaderFullsize();
			$this->createCustomPageHeaderStandard();
			$this->createThumbnail();
			
			imagedestroy($this->mImageCopy);
			
			$this->sendS3RawFiles();
			
			//create photo
			$photo = new Photo($objContext);
			$photo->setPhotoTypeID(self::CUSTOMHEADER);			
			$photo->setCaption($this->mCaption);			
			$photo->setFileName($this->mFilename);
			$photo->Create();
			
			return $photo;
		}
		
		public function cropCustomPageHeader($pm,$mPhoto,$x,$y){
			$width = self::HEADER_MIN_WIDTH;
			$height = self::HEADER_MIN_HEIGHT;
			
			$relPath = $pm->GetPathrelative();
			$filename = $mPhoto->getFileName();
			
			$fullsize_file = $relPath."customheaderfullsize".$filename;
			$fullsize_copy = $this->getImageCopy($fullsize_file);
			
			$attrib = array("width"=>imagesx($fullsize_copy),"height"=>imagesy($fullsize_copy));
			
			$cropped_copy =  $this->makeCroppedImage($fullsize_file,$fullsize_copy,$attrib,$x,$y,$width,$height);
			
			$newFilename = "";
			$imageInfo = $this->mImageInfo[$fullsize_file];
			switch(substr($imageInfo['mime'],6,4)){
				case "jpeg"	:	$newFilename = uniqid().time().".jpg";
								break;
				case "gif"	:	$newFilename = uniqid().time().".gif";
								break;
				case "png"	:	$newFilename = uniqid().time().".png";
								break;
				case "pjpeg":	$newFilename = uniqid().time().".jpg";
								break;
				default		:	$newFilename = uniqid().time().".jpg";
								break;
			}
			
			$tempRawFilesToAdd = array();
			$tempRawFilesToRemove = array();
			
			$orig_file = $relPath."orig-".$filename;
			$newOrigFilename = $relPath."orig-".$newFilename;
			$orig_copy = $this->getImageCopy($orig_file);
			$this->createImage($orig_file,$orig_copy,$newOrigFilename);
			imagedestroy($orig_copy);
			unlink($orig_file);
			
			$tempRawFilesToAdd[] = "orig-".$newFilename;
			$tempRawFilesToRemove[] = "orig-".$filename;
			
			$standard_file = $relPath."customheaderstandard".$filename;
			$newStandardFilename = $relPath."customheaderstandard".$newFilename;
			$this->createImage($fullsize_file,$cropped_copy,$newStandardFilename);
			imagedestroy($cropped_copy);
			unlink($standard_file);
			
			$tempRawFilesToAdd[] = "customheaderstandard".$newFilename;
			$tempRawFilesToRemove[] = "customheaderstandard".$filename;
			
			$newFullsizeFilename = $relPath."customheaderfullsize".$newFilename;
			$this->createImage($fullsize_file,$fullsize_copy,$newFullsizeFilename);
			imagedestroy($fullsize_copy);
			unlink($fullsize_file);
			
			$tempRawFilesToAdd[] = "customheaderfullsize".$newFilename;
			$tempRawFilesToRemove[] = "customheaderfullsize".$filename;
			
			$mPhoto->saveImageProcess("CC",$newFilename);//thumbnail crop
			
			$this->mRelPath = $relPath;
			
			$this->mS3RawFiles = $tempRawFilesToRemove;
			$this->removeS3RawFiles();
			
			$this->mS3RawFiles = $tempRawFilesToAdd;
			$this->sendS3RawFiles();
		}
		
		public function getShareLink(Photo $photo){
			return FALSE;
		}
	}