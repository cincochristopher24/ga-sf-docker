<?php
	require_once("travellog/UIComponent/NewCollection/model/Class.AbstractUploadedPhoto.php");

	class UploadedPhotoTravellog extends AbstractUploadedPhoto{
		
		public function __construct(){
			$this->mContext = self::TRAVELLOG_CONTEXT;
		}
		
		public function processFile(PathManager $pm, $objContext=NULL){
			if( !$this->prepareFile($pm,$objContext) ){
				return FALSE;
			}
			
			$this->createFullsize();
			$this->createStandard();
			$this->createFeatured();
			
			$this->createEntryHeader();
			if( $this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
				$this->createCustomPageHeaderFullsize();
				$this->createCustomPageHeaderStandard();
			}
			
			imagedestroy($this->mImageCopy);
			
			$this->sendS3RawFiles();
			
			//create photo
			$photo = new Photo($objContext);
			$photo->setPhotoTypeID(self::TRAVELLOG);		
			$photo->setCaption($this->mCaption);			
			$photo->setFileName($this->mFilename);
			$photo->Create();
			
			$travel = $objContext->getTravel();
			if( 0 == $travel->getPrimaryphoto() ){
				$photo->invalidateCacheEntry();
				$travel->setPrimaryphoto($photo->getPhotoID());
				$travel->Update();
			}

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: aug. 10, 2009
			require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($_SESSION['travelerID'],4);

			
			//create a bacjup of the photo in case the journal is deleted with all its entry photos deleted
			//this bacjup photo won't be deleted unless the photo is deleted iindividually or by batch in the photos tab
			$backupFilename = $this->mRelPath."backup-".$objContext->getTravellogID()."-".$this->mFilename;
			copy($this->mPathFilename,$backupFilename);
			
			if( $this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
				$objContext->setQualifiedCustomHeaderPhoto($photo->getPhotoID(),TRUE);
			}
			return $photo;
		}
		
		public function fixBlackPhoto(PathManager $pm, Photo $photo){
			try{
				$this->prepareFixBlackPhoto($pm,$photo);
			
				$this->createFullsize();
				$this->createStandard();
				$this->createFeatured();

				$this->createEntryHeader();
				if( $this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
					$this->createCustomPageHeaderFullsize();
					$this->createCustomPageHeaderStandard();
				}
				
				imagedestroy($this->mImageCopy);
				
				$this->sendS3RawFiles();
			}catch(exception $e){
				throw $e;
			}
		}
		
	}