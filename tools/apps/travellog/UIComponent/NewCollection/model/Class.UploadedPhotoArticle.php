<?php
	require_once("travellog/UIComponent/NewCollection/model/Class.AbstractUploadedPhoto.php");

	class UploadedPhotoArticle extends AbstractUploadedPhoto{
		
		public function __construct(){
			$this->mContext = self::ARTICLE_CONTEXT;
		}
		
		public function processFile(PathManager $pm, $objContext=NULL){
			if( !$this->prepareFile($pm,$objContext) ){
				return FALSE;
			}
			
			$this->createFullsize();
			$this->createStandard();
			$this->createFeatured();
			$this->createEntryHeader();
			
			imagedestroy($this->mImageCopy);
			
			$this->sendS3RawFiles();
			
			//create photo
			$photo = new Photo($objContext);
			$photo->setPhotoTypeID(self::ARTICLE);
			$photo->setCaption($this->mCaption);			
			$photo->setFileName($this->mFilename);
			$photo->Create();
			$photo->invalidateCacheEntry();
			
			return $photo;
		}
		
		public function fixBlackPhoto(PathManager $pm, Photo $photo){			
			try{
				$this->prepareFixBlackPhoto($pm,$photo);
			
				$this->createFullsize();
				$this->createStandard();
				$this->createFeatured();
				$this->createEntryHeader();
				
				imagedestroy($this->mImageCopy);
				
				$this->sendS3RawFiles();
			}catch(exception $e){
				throw $e;
			}
		}
	}