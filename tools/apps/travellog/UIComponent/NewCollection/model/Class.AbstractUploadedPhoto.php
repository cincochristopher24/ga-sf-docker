<?php
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.PathManager.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/helper/Class.PhotoCollectionHelper.php");
	require_once("amazon/Class.GANETAmazonS3WebService.php");

	abstract class AbstractUploadedPhoto implements PhotoCollectionConstants{
		
		protected $mContext	=	NULL;
		protected $mContextObject	=	NULL;
		
		protected $mFile		=	NULL;//image file sent via HTTP POST
		protected $mAttrib	=	array(	"width"		=>	0,
										"height"	=>	0	);
		protected $mRotationAngle	=	0;
		protected $mCaption	=	"";
		protected $mFilename	=	"";
		protected $mRelPath	=	"";
		protected $mPathFilename = "";
		
		protected $mImageCopy	= FALSE;
		
		protected $mImageInfo = array();
		
		protected $mPhotoInstance = NULL;
		
		protected $mS3RawFiles = array();
		
		abstract public function processFile(PathManager $pm, $objContext=NULL);
		
		function setContext($context=NULL){
			$this->mContext = $context;
		}
		function setGenID($genID=NULL){
			$this->mGenID = $genID;
		}
		function setFile($file=NULL){
			$this->mFile = $file;
		}
		function setCaption($caption=""){
			$this->mCaption = $caption;
		}
		function setRotationAngle($angle=0){
			$this->mRotationAngle = $angle;
		}
		
		function prepareFile(PathManager $pm, $objContext=NULL){
			if( is_null($objContext) ){
				return FALSE;
			}
			$this->mRelPath = $pm->GetPathrelative();
			switch($this->mFile["type"]){
				case "image/jpeg"	:	$this->mFilename = uniqid().time().".jpg";
										break;
				case "image/gif"	:	$this->mFilename = uniqid().time().".gif";
										break;
				case "image/png"	:	$this->mFilename = uniqid().time().".png";
										break;
				case "image/pjpeg"	:	$this->mFilename = uniqid().time().".jpg";
										break;
				default				:	$this->mFilename = uniqid().time().".jpg";
										break;
			}
			$absolutePath = $pm->getAbsolutePath();
			chmod($absolutePath,0777);
			$isUploaded = move_uploaded_file( $this->mFile["tmp_name"], $absolutePath."orig-".$this->mFilename);
						
			if( !$isUploaded ){
				return FALSE;
			}
			
			$this->mPathFilename = $this->mRelPath."orig-".$this->mFilename;
			if( !$this->resizeOriginal($this->mPathFilename,self::ORIGINAL_SIZE) ){
				unlink($this->mPathFilename);
				return FALSE;
			}
			$this->addS3RawFile("orig-".$this->mFilename);

			$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			if( !$this->mImageCopy ){
				unlink($this->mPathFilename);
				return FALSE;
			}
			if( 0 != $this->mRotationAngle ){
				$this->rotateImage($this->mImageCopy,$this->mRotationAngle,$this->mPathFilename);
				imagedestroy($this->mImageCopy);
				unset($this->mImageInfo[$this->mPathFilename]);
				$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			}
			
			$origDim = $this->getDimension($this->mPathFilename);
			if( !$origDim ){
				unlink($this->mPathFilename);
				return FALSE;
			}
			
			$this->mAttrib["width"] = $origDim["width"];
			$this->mAttrib["height"] = $origDim["height"];
			
			$this->mContextObject = $objContext;
			
			return TRUE;
		}
		
		function addS3RawFile($file=""){
			$this->mS3RawFiles[] = $file;
		}
		
		function sendS3RawFiles(){
			if(count($this->mS3RawFiles)){
				$ganetAWS = GANETAmazonS3WebService::getInstance();
				$ganetAWS->pushRawFilestoS3($this->mS3RawFiles,$this->mRelPath);
			}
		}
		
		function removeS3RawFiles(){
			if(count($this->mS3RawFiles)){
				$ganetAWS = GANETAmazonS3WebService::getInstance();
				$ganetAWS->removeRawFilesFromS3($this->mS3RawFiles,$this->mRelPath);
			}
		}
		
		function createFullsize(){
			$newPathFilename = $this->mRelPath."fullsize".$this->mFilename;
			$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::FULLSIZE,$newPathFilename);
			$this->addS3RawFile("fullsize".$this->mFilename);
		}
		function createStandard(){
			$newPathFilename = $this->mRelPath."standard".$this->mFilename;
			$size = ( self::PROFILE_CONTEXT == $this->mContext ) ? self::STANDARD_PROFILE : self::STANDARD; 
			//$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,$size,$newPathFilename);
			$cropPoint = $this->getCropPoint($this->mAttrib["width"],$this->mAttrib["height"]);
			$length = ($this->mAttrib["width"] > $this->mAttrib["height"])? $this->mAttrib["height"] : $this->mAttrib["width"];
			$croppedImage = $this->makeCroppedImage($this->mPathFilename,$this->mImageCopy,$this->mAttrib,$cropPoint["x1"],$cropPoint["y1"],$length,$length);
			$this->makeResizedCopy($this->mPathFilename,$croppedImage,array("width"=>$length,"height"=>$length),$size,$newPathFilename);
			$this->addS3RawFile("standard".$this->mFilename);
			
			//create thumbnail from standard
			$newPathFilename = $this->mRelPath."thumbnail".$this->mFilename;
			$this->makeResizedCopy($this->mPathFilename,$croppedImage,array("width"=>$length,"height"=>$length),self::THUMBNAIL,$newPathFilename);	
			imagedestroy($croppedImage);
			$this->addS3RawFile("thumbnail".$this->mFilename);
		}
		function createFeatured(){
			$newPathFilename = $this->mRelPath."featured".$this->mFilename;
			$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::FEATURED,$newPathFilename);
			$this->addS3RawFile("featured".$this->mFilename);
		}
		function createThumbnail(){
			$newPathFilename = $this->mRelPath."thumbnail".$this->mFilename;
			$cropPoint = $this->getCropPoint($this->mAttrib["width"],$this->mAttrib["height"]);
			$length = ($this->mAttrib["width"] > $this->mAttrib["height"])? $this->mAttrib["height"] : $this->mAttrib["width"];
			$croppedImage = $this->makeCroppedImage($this->mPathFilename,$this->mImageCopy,$this->mAttrib,$cropPoint["x1"],$cropPoint["y1"],$length,$length);
			$this->makeResizedCopy($this->mPathFilename,$croppedImage,array("width"=>$length,"height"=>$length),self::THUMBNAIL,$newPathFilename);
			imagedestroy($croppedImage);
			$this->addS3RawFile("thumbnail".$this->mFilename);
		}
		
		/*
		Journal Entry Primary Photo Resizing and Cropping Rules
		1. Check photo orientation
		- if photo has a landscape orientation, proceed to Step 2 (square photos are treated as landscape)
		- if photo has a portrait orientation, proceed to Step 3
		2. Check landscape photo size
		- if photo width is wider than or equal to 751px, proceed to Step 4
		- if photo width is shorter than 751px but greater than 385px, proceed to Step 5
		- if photo width is shorter than or equal to 385px, leave as is; We're done
		3. Check portrait photo size
		- if photo height is taller than 385px, proceed to Step 6
		- if photo height is shorter than or equal to 385px, leave as is; We're done
		4. Unless width is 751px already, resize photo so that width is 751px 
			(height and width ratio must remain the same) and check height
		- if photo height is taller than 493px, crop height to 493px (cut-off top and bottom evenly). We're done.
		- if photo height is shorter than or equal to 493px, leave as is; We're done.
		5. Resize photo so that the width is 385px (height and width ratio must remain the same); We're done.
		6. Resize photo so that the height is 385px (height and width ration must remain the same); We're done.
		*/
		function createEntryHeader(){
			$newPathFilename = $this->mRelPath."jentryheader".$this->mFilename;
			if( $this->isPortrait() ){//portrait
				if( self::JENTRY_HEIGHT < $this->mAttrib["height"] ){//step 3-a
					$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::JENTRY_HEIGHT,$newPathFilename);
				}else{//step 3-b
					$this->createImage($this->mPathFilename,$this->mImageCopy,$newPathFilename);
				}
			}else{//landscape
				if( self::JENTRY_WIDTH < $this->mAttrib["width"]){//step 2-a
					//step 4
					$tempImage = $this->createResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::JENTRY_WIDTH);
					$width = imagesx($tempImage);
					$height = imagesy($tempImage);
					if( self::JENTRY_HEIGHT2 < $height ){//step 4-a
						$cropY = floor($height / 2) - floor(self::JENTRY_HEIGHT2/2);
						$tempImage = $this->makeCroppedImage($this->mPathFilename,$tempImage,array("width"=>$width,"height"=>$height),0,$cropY,$width,self::JENTRY_HEIGHT2);
					}
					//save $tempImage
					$this->createImage($this->mPathFilename,$tempImage,$newPathFilename);
					imagedestroy($tempImage);
				}else if( self::JENTRY_WIDTH > $this->mAttrib["width"] && self::JENTRY_HEIGHT < $this->mAttrib["width"] ){//step 2-b
					$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::JENTRY_HEIGHT,$newPathFilename);
				}else{//step 2-c
					$this->createImage($this->mPathFilename,$this->mImageCopy,$newPathFilename);
				}
			}
			$this->addS3RawFile("jentryheader".$this->mFilename);
		}
		function createImage($origPathFilename,$imageCopy,$newPathFilename){
			$imagetype = $this->getImageType($origPathFilename);//getimagesize($origPathFilename);
			ini_set("memory_limit",$this->getMemoryForImage($origPathFilename)."M");
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagejpeg($imageCopy, $newPathFilename, 90);
						 	break;
					case self::GIF	:					
							imagegif($imageCopy, $newPathFilename);
						   	break;
					case self::PNG	:						
							imagepng($imageCopy, $newPathFilename);
						  	break;
					default:
							imagejpeg($imageCopy, $newPathFilename, 90);
							break;		
			}
		}
		private function isPortrait(){
			return ($this->mAttrib["width"] <= $this->mAttrib["height"])? TRUE : FALSE;
		}
		
		function getDimension($pathFilename){
			try{
				if( !isset($this->mImageInfo[$pathFilename]) ){
					$this->mImageInfo[$pathFilename] = getimagesize($pathFilename);
				}
				$size = list($width, $height, $type, $attr) = $this->mImageInfo[$pathFilename];
				if ( !$size ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			return array("width"=>$width, "height"=>$height, "type"=>$type);
		}
		
		function getImageType($pathFilename){
			try{
				if( !isset($this->mImageInfo[$pathFilename]) ){
					$this->mImageInfo[$pathFilename] = getimagesize($pathFilename);
				}
				$imagetype = $this->mImageInfo[$pathFilename];
				if( !$imagetype ){
					return FALSE;
				}
				return $imagetype;
			}catch(exception $e){
				return FALSE;
			}
		}
		
		function getRescaledDimension($width,$height,$targetSize){
			$ratio = 1;
			if ( $width > $targetSize && $width == $height ){//square
				$ratio = $targetSize / $width;
			}else if ( $width > $targetSize && $width > $height ){//landscape
				$ratio = $targetSize / $width;
			}else if ( $height > $targetSize && $height > $width ){//portrait
				$ratio = $targetSize / $height;
			}
			$height = round($height*$ratio);
			$width = round($width*$ratio);
			
			return array("width"=>$width, "height"=>$height);
		}
		function getImageCopy($origPathFilename){
			
			ini_set("memory_limit",$this->getMemoryForImage($origPathFilename)."M");				
			
			$imagetype = $this->getImageType($origPathFilename);//getimagesize($origPathFilename);		
			$image = FALSE;
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
						   	$image = imagecreatefromjpeg($origPathFilename); 
							break;
					case self::GIF	:					
						   	$image = imagecreatefromgif($origPathFilename); 	
							break;
					case self::PNG	:
							$image = imagecreatefrompng($origPathFilename); 						
						  	break;
					case self::BMP	:
							$image = $this->ImageCreateFromBMP($origPathFilename);
							break;
					default	: 
							$image = imagecreatefromjpeg($origPathFilename); 
							break;		
			}
			return $image;
		}
		function makeResizedCopy($oldPathFilename,$imageCopy,$attrib,$size,$newPathFilename,$fit=FALSE){//saves the resized copy
			
			$imagetype = $this->getImageType($oldPathFilename);//getimagesize($oldPathFilename);
			
			$oldWidth = $attrib["width"];
			$oldHeight = $attrib["height"];
			
			if( $fit ){
				$newWidth = $size;
				$newHeight = $size;
			}else{
				$newDimension = $this->getRescaledDimension($oldWidth,$oldHeight,$size);
				$newWidth = $newDimension["width"];
				$newHeight = $newDimension["height"];
			}
			
			ini_set("memory_limit",$this->getMemoryForImage($oldPathFilename)."M");
			
			$image_p = @imagecreatetruecolor($newWidth, $newHeight);
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagejpeg($image_p, $newPathFilename, 90);
						 	break;
					case self::GIF	:					
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      			
							imagegif($image_p, $newPathFilename);
						   	break;
					case self::PNG	:						
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagepng($image_p, $newPathFilename);
						  	break;
					default:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagejpeg($image_p, $newPathFilename, 90);
							break;		
			}
			imagedestroy($image_p);
		}
		function createResizedCopy($oldPathFilename,$imageCopy,$attrib,$size){
			$imagetype = $this->getImageType($oldPathFilename);//getimagesize($oldPathFilename);
			
			$oldWidth = $attrib["width"];
			$oldHeight = $attrib["height"];
			
			$newDimension = $this->getRescaledDimension($oldWidth,$oldHeight,$size);
			$newWidth = $newDimension["width"];
			$newHeight = $newDimension["height"];
			
			ini_set("memory_limit",$this->getMemoryForImage($oldPathFilename)."M");
			
			$image_p = @imagecreatetruecolor($newWidth, $newHeight);
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
						 	break;
					case self::GIF	:					
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      			
						   	break;
					case self::PNG	:						
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
						  	break;
					default:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							break;		
			}
			return $image_p;
		}
		function makeCroppedImage($oldPathFilename,$imageCopy,$attrib,$x1,$y1,$width,$height){
			
			$imagetype = $this->getImageType($oldPathFilename);//getimagesize($oldPathFilename);
			
			$oldWidth = $attrib["width"];
			$oldHeight = $attrib["height"];
			
			ini_set("memory_limit",$this->getMemoryForImage($oldPathFilename)."M");
			
			$image_p = @imagecreatetruecolor($width, $height);
			if ( !$image_p ){
				return FALSE;
			}	

			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagecopy($image_p, $imageCopy, 0, 0, $x1, $y1, $oldWidth, $oldHeight);      	
						 	break;
					case self::GIF	:				
						   	imagecopy($image_p, $imageCopy, 0, 0, $x1, $y1, $oldWidth, $oldHeight);      			
						   	break;
					case self::PNG	:					
							imagecopy($image_p, $imageCopy, 0, 0, $x1, $y1, $oldWidth, $oldHeight);      	
						  	break;
					default	:
							imagecopy($image_p, $imageCopy, 0, 0, $x1, $y1, $oldWidth, $oldHeight);   
							break;		
			}
			
			return $image_p;
		}
		function getCropPoint($width,$height){		
			$x1 = 0;
			$y1 = 0;
			
			if( $width < $height ){//portrait
				$x1 = 0;
				$y1 = floor(($height-$width)/2);
			}else{//landscape
				$y1 = 0;
				$x1 = floor(($width-$height)/2);
			}
			return array("x1" => $x1, "y1" => $y1);
		}
		function rotateImage($imageCopy,$angle,$pathFilename){
			$imagetype = $this->getImageType($pathFilename);//getimagesize($pathFilename);
			
			ini_set("memory_limit",$this->getMemoryForImage($pathFilename)."M");
			
			$image_p = imagerotate($imageCopy,$angle*-1,0);
			
			chmod($pathFilename,0777);
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagejpeg($image_p, $pathFilename, 90);
						 	break;
					case self::GIF	:					
							imagegif($image_p, $pathFilename);
						   	break;
					case self::PNG	:						
							imagepng($image_p, $pathFilename);
						  	break;
					default:
							imagejpeg($image_p, $pathFilename, 90);
							break;		
			}
			imagedestroy($image_p);
		}
		
		
		function rotatePhoto(PathManager $pm, $context_obj, $photo_obj, $angle=0){
			$relPath = $pm->GetPathrelative();
			$filename = $photo_obj->getFileName();
						
			//original
			$orig_file = $relPath."orig-".$filename;
			if( file_exists($orig_file) ){
				$this->resizeOriginal($orig_file,self::ORIGINAL_SIZE);
				$orig_copy = $this->getImageCopy($orig_file);
				$this->rotateImage($orig_copy,$angle,$orig_file);
				imagedestroy($orig_copy);
			}
			
			//fullsize
			$fullsize_file = $relPath."fullsize".$filename;
			$fullsize_copy = $this->getImageCopy($fullsize_file);
			$this->rotateImage($fullsize_copy,$angle,$fullsize_file);
			imagedestroy($fullsize_copy);
			
			//standard
			$standard_file = $relPath."standard".$filename;
			$standard_copy = $this->getImageCopy($standard_file);
			$this->rotateImage($standard_copy,$angle,$standard_file);
			imagedestroy($standard_copy);
			
			//featured
			$context_class = get_class($context_obj);
			if( "PhotoAlbum" == $context_class || "TravelLog" == $context_class ){
				$featured_file = $relPath."featured".$filename;
				$featured_copy = $this->getImageCopy($featured_file);
				$this->rotateImage($featured_copy,$angle,$featured_file);
				imagedestroy($featured_copy);
				
				if( "TravelLog" == $context_class ){
					$this->mRelPath = $relPath;
					$this->mFilename = $filename;
					unset($this->mImageInfo[$orig_file]);
					$this->mAttrib = $this->getDimension($orig_file);
					$this->mPathFilename = $this->mRelPath."orig-".$this->mFilename;
					$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
					if( file_exists($this->mRelPath."jentryheader".$this->mFilename) ){
						unlink($this->mRelPath."jentryheader".$this->mFilename);
					}
					$this->createEntryHeader();
					
					if( $this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
						$this->createCustomPageHeaderFullsize();
						$this->createCustomPageHeaderStandard();
						$context_obj->setQualifiedCustomHeaderPhoto($photo_obj->getPhotoID(),TRUE);
					}else{
						if(file_exists($this->mRelPath."customheaderstandard".$this->mFilename)){
							unlink($this->mRelPath."customheaderstandard".$this->mFilename);
						}
						if(file_exists($this->mRelPath."customheaderfullsize".$this->mFilename)){
							unlink($this->mRelPath."customheaderfullsize".$this->mFilename);
						}
						$context_obj->setQualifiedCustomHeaderPhoto($photo_obj->getPhotoID(),FALSE);
					}
					imagedestroy($this->mImageCopy);
				}
			}
			
			//thumbnail
			$thumbnail_file = $relPath."thumbnail".$filename;
			$thumbnail_copy = $this->getImageCopy($thumbnail_file);
			$this->rotateImage($thumbnail_copy,$angle,$thumbnail_file);
			imagedestroy($thumbnail_copy);
			
			//prepare a new filename
			$imagetype = $this->getImageType($relPath."thumbnail".$filename);
			$newFilename = $this->generateNewFilename(substr($imagetype['mime'],6,4));
			
			$this->renameFile($relPath,$filename,$newFilename);
			
			if( 90 == $angle ){
				$photo_obj->saveImageProcess("RR",$newFilename);//rotate right
			}else if( 270 == $angle ){
				$photo_obj->saveImageProcess("RL",$newFilename);//rotate left
			}
		}
		
		function setNewThumbnail($pm,$mPhotoContext,$mPhoto,$x,$y,$width,$height){
			$relPath = $pm->GetPathrelative();
			$filename = $mPhoto->getFileName();
			
			$fullsize_file = $relPath."fullsize".$filename;
			$fullsize_copy = $this->getImageCopy($fullsize_file);
			
			//prepare a new filename
			$imagetype = $this->getImageType($fullsize_file);
			$newFilename = $this->generateNewFilename(substr($imagetype['mime'],6,4));				
			
			$attrib = array("width"=>imagesx($fullsize_copy),"height"=>imagesy($fullsize_copy));
			
			$cropped_copy =  $this->makeCroppedImage($fullsize_file,$fullsize_copy,$attrib,$x,$y,$width,$height);
			
			$thumbnail_file = $relPath."thumbnail".$filename;
			$standard_file = $relPath."standard".$filename;
			if( $mPhotoContext instanceof TravelerProfile ){
				$this->makeResizedCopy($fullsize_file,$cropped_copy,array("width"=>$width,"height"=>$height),self::STANDARD_PROFILE,$standard_file,TRUE);
			}else{
				$this->makeResizedCopy($fullsize_file,$cropped_copy,array("width"=>$width,"height"=>$height),self::STANDARD,$standard_file,TRUE);
			}
			$this->makeResizedCopy($fullsize_file,$cropped_copy,array("width"=>$width,"height"=>$height),self::THUMBNAIL,$thumbnail_file,TRUE);
			
			imagedestroy($fullsize_copy);
			imagedestroy($cropped_copy);
			
			$this->renameFile($relPath,$filename,$newFilename);
			
			$mPhoto->saveImageProcess("TC",$newFilename);//thumbnail crop
		}
		
		function renameFile($relPath="",$filename="",$newFilename=""){
			$tempRawFilesToAdd = array();
			$tempRawFilesToRemove = array();
			
			$orig_file = $relPath."orig-".$filename;
			if( file_exists($orig_file) ){
				$tempRawFilesToAdd[] = "orig-".$newFilename;
				$tempRawFilesToRemove[] = "orig-".$filename;
				$newName = $relPath."orig-".$newFilename;
				rename($orig_file,$newName);
			}
			$fullsize_file = $relPath."fullsize".$filename;
			if( file_exists($fullsize_file) ){
				$tempRawFilesToAdd[] = "fullsize".$newFilename;
				$tempRawFilesToRemove[] = "fullsize".$filename;
				$newName = $relPath."fullsize".$newFilename;
				rename($fullsize_file,$newName);
			}
			$standard_file = $relPath."standard".$filename;
			if( file_exists($standard_file) ){
				$tempRawFilesToAdd[] = "standard".$newFilename;
				$tempRawFilesToRemove[] = "standard".$filename;
				$newName = $relPath."standard".$newFilename;
				rename($standard_file,$newName);
			}
			$featured_file = $relPath."featured".$filename;
			if( file_exists($featured_file) ){
				$tempRawFilesToAdd[] = "featured".$newFilename;
				$tempRawFilesToRemove[] = "featured".$filename;
				$newName = $relPath."featured".$newFilename;
				rename($featured_file,$newName);
			}
			$jentry_header_file = $relPath."jentryheader".$filename;
			if( file_exists($jentry_header_file) ){
				$tempRawFilesToAdd[] = "jentryheader".$newFilename;
				$tempRawFilesToRemove[] = "jentryheader".$filename;
				$newName = $relPath."jentryheader".$newFilename;
				rename($jentry_header_file,$newName);
			}
			$thumbnail_file = $relPath."thumbnail".$filename;
			if( file_exists($thumbnail_file) ){
				$tempRawFilesToAdd[] = "thumbnail".$newFilename;
				$tempRawFilesToRemove[] = "thumbnail".$filename;
				$newName = $relPath."thumbnail".$newFilename;
				rename($thumbnail_file,$newName);
			}
			$customheader_standard_file = $relPath."customheaderstandard".$filename;
			if( file_exists($customheader_standard_file) ){
				$tempRawFilesToAdd[] = "customheaderstandard".$newFilename;
				$tempRawFilesToRemove[] = "customheaderstandard".$filename;
				$newName = $relPath."customheaderstandard".$newFilename;
				rename($customheader_standard_file,$newName);
			}
			$customheader_fullsize_file = $relPath."customheaderfullsize".$filename;
			if( file_exists($customheader_fullsize_file) ){
				$tempRawFilesToAdd[] = "customheaderfullsize".$newFilename;
				$tempRawFilesToRemove[] = "customheaderfullsize".$filename;
				$newName = $relPath."customheaderfullsize".$newFilename;
				rename($customheader_fullsize_file,$newName);
			}
			
			$this->mRelPath = $relPath;
			
			$this->mS3RawFiles = $tempRawFilesToRemove;
			$this->removeS3RawFiles();
			
			$this->mS3RawFiles = $tempRawFilesToAdd;
			$this->sendS3RawFiles();
		}
		
		function generateNewFilename($imageType){
			$newFilename = "";
			switch ($imageType) {
					case "image/jpeg"	:	$newFilename = uniqid().time().".jpg";
											break;
					case "image/gif"	:	$newFilename = uniqid().time().".gif";
											break;
					case "image/png"	:	$newFilename = uniqid().time().".png";
											break;
					case "image/pjpeg"	:	$newFilename = uniqid().time().".jpg";
											break;
					default				:	$newFilename = uniqid().time().".jpg";
											break;
			}
			return $newFilename;
		}
		
		function getMemoryForImage( $filename ){
		    $imageInfo = $this->getImageType($filename);
			$requiredMemoryMB = 35;
			if( $imageInfo ){
				$channels = 4;
				if( isset($imageInfo['channels']) ){
					$channels = $imageInfo['channels'];
				}
				$requiredMemoryMB = round(( $imageInfo[0] * $imageInfo[1] * ($imageInfo['bits'] / 8) * $channels * 2.5 ) / 1024);
			}
		    return $requiredMemoryMB;
		}
		
		function createCustomPageHeaderFullsize(){
			$newPathFilename = $this->mRelPath."customheaderfullsize".$this->mFilename;
			$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::HEADER_MIN_WIDTH,$newPathFilename);
			$this->addS3RawFile("customheaderfullsize".$this->mFilename);
		}
		
		function createCustomPageHeaderStandard(){
			$newPathFilename = $this->mRelPath."customheaderstandard".$this->mFilename;
			$tempImage = $this->getImageCopy($this->mRelPath."customheaderfullsize".$this->mFilename);
			$width = imagesx($tempImage);
			$height = imagesy($tempImage);
			if( self::HEADER_MIN_HEIGHT < $height ){//step 4-a
				$cropY = floor($height / 2) - floor(self::HEADER_MIN_HEIGHT/2);
				$tempImage = $this->makeCroppedImage($this->mPathFilename,$tempImage,array("width"=>$width,"height"=>$height),0,$cropY,$width,self::HEADER_MIN_HEIGHT);
			}
			//save $tempImage
			$this->createImage($this->mPathFilename,$tempImage,$newPathFilename);
			imagedestroy($tempImage);
			$this->addS3RawFile("customheaderstandard".$this->mFilename);
		}
		
		function _create_cobrandcustomsize_script($pm,$photo,$travellog){//to create page header sized of previously uploaded travellog photos
			
			if( !($photo instanceof Photo || $travellog instanceof TravelLog) ){
				return FALSE;
			}
			
			$this->mRelPath = $pm->GetPathrelative();
			$this->mFilename = $photo->getFilename();
			$this->mPathFilename = $this->mRelPath."orig-".$this->mFilename;
						
			if( !file_exists($this->mPathFilename) ){
				return FALSE;
			}
			
			$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			
			$origDim = $this->getDimension($this->mPathFilename);
			if( !$origDim ){
				return FALSE;
			}
			
			$this->mAttrib["width"] = $origDim["width"];
			$this->mAttrib["height"] = $origDim["height"];
			
			if( $this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
				$this->createCustomPageHeaderFullsize();
				$this->createCustomPageHeaderStandard();
				return TRUE;
			}
			
			return FALSE;
		}
		
		function isValidCustomHeaderDimension($width=0,$height=0){
			//reduce dimension first to customheader dimension
			if( 1 > $width || 1 > $height ){
				return FALSE;
			}
			$rescaledDim = $this->getRescaledDimension($width,$height,self::HEADER_MIN_WIDTH);
			if( self::HEADER_MIN_WIDTH > $rescaledDim["width"] ){
				return FALSE;
			}
			if( self::HEADER_MIN_HEIGHT > $rescaledDim["height"] ){
				return FALSE;
			}
			return TRUE;
		}
		
		function resizeOriginal($origPathFilename="",$origSize=1024){
			ini_set("memory_limit",$this->getMemoryForImage($origPathFilename)."M");
			
			$dimension = $this->getDimension($origPathFilename);
			if( !$dimension ){
				return FALSE;
			}
			
			$oldWidth = $dimension["width"];
			$oldHeight = $dimension["height"];
			
			if( $oldWidth <= $origSize && $oldHeight <= $origSize ){
				return TRUE;
			}
			
			$newDimension = $this->getRescaledDimension($oldWidth,$oldHeight,$origSize);
			$newWidth = $newDimension["width"];
			$newHeight = $newDimension["height"];
			
			$imageCopy = $this->getImageCopy($origPathFilename);
			if( !$imageCopy ){
				return FALSE;
			}
			
			$imagetype = $this->getImageType($origPathFilename);//getimagesize($oldPathFilename);
			if( !$imagetype ){
				return FALSE;
			}
			
			$image_p = @imagecreatetruecolor($newWidth, $newHeight);
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagejpeg($image_p, $origPathFilename, 90);
						 	break;
					case self::GIF	:					
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      			
							imagegif($image_p, $origPathFilename);
						   	break;
					case self::PNG	:						
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagepng($image_p, $origPathFilename);
						  	break;
					default:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagejpeg($image_p, $origPathFilename, 90);
							break;		
			}
			imagedestroy($imageCopy);
			imagedestroy($image_p);
			unset($this->mImageInfo[$origPathFilename]);
			return TRUE;
		}
		
		function fixGroupLogoThumbnail($pm,$photo){
			if( !($photo instanceof Photo) ){
				throw new exception("UploadedPhoto::fixGroupLogoThumbnail(pm,photo): photo must be an instance of Photo.");
			}
			if( !($pm instanceof PathManager) ){
				throw new exception("UploadedPhoto::fixGroupLogoThumbnail(pm,photo): pm must be an instance of PathManager.");
			}
			
			$this->mRelPath = $pm->GetPathrelative();
			$this->mFilename = $photo->getFilename();
			$this->mPathFilename = $this->mRelPath."standard".$this->mFilename;
						
			if( !file_exists($this->mPathFilename) ){
				throw new exception("Image ".$this->mPathFilename." does not exist.");
			}
			
			$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			
			$origDim = $this->getDimension($this->mPathFilename);
			if( !$origDim ){
				throw new exception("Unable to read image ".$this->mPathFilename);
			}
			
			$this->mAttrib["width"] = $origDim["width"];
			$this->mAttrib["height"] = $origDim["height"];
			
			try{
				$this->createThumbnail();
				$this->removeS3RawFiles();
				$this->sendS3RawFiles();
				$thumbname = $this->mRelPath."thumbnail".$this->mFilename;
				return '<p><a href="'.$photo->getPhotoLink('standard').'" target="_blank"><img src="'.$photo->getPhotoLink('thumbnail').'" /></a></p>';
			}catch(exception $e){
				throw $e;
			}
		}
		
		protected function prepareFixBlackPhoto($pm, $photo){
			$this->mRelPath = $pm->GetPathrelative();
			$this->mFilename = $photo->getFilename();
			$this->mPathFilename = $this->mRelPath."orig-".$this->mFilename;
						
			if( !file_exists($this->mPathFilename) ){
				throw new exception("Image ".$this->mPathFilename." does not exist.");
			}
			
			$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			
			$origDim = $this->getDimension($this->mPathFilename);
			if( !$origDim ){
				throw new exception("Unable to read image ".$this->mPathFilename);
			}
			
			$this->mAttrib["width"] = $origDim["width"];
			$this->mAttrib["height"] = $origDim["height"];
		}
		
		function ImageCreateFromBMP($filename)
		{
		 //Ouverture du fichier en mode binaire
		   if (! $f1 = fopen($filename,"rb")) return FALSE;

		 //1 : Chargement des ent�tes FICHIER
		   $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
		   if ($FILE['file_type'] != 19778) return FALSE;

		 //2 : Chargement des ent�tes BMP
		   $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
		                 '/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
		                 '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
		   $BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
		   if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
		   $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
		   $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
		   $BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
		   $BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
		   $BMP['decal'] = 4-(4*$BMP['decal']);
		   if ($BMP['decal'] == 4) $BMP['decal'] = 0;

		 //3 : Chargement des couleurs de la palette
		   $PALETTE = array();
		   if ($BMP['colors'] < 16777216)
		   {
		    $PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
		   }

		 //4 : Cr�ation de l'image
		   $IMG = fread($f1,$BMP['size_bitmap']);
		   $VIDE = chr(0);

		   $res = imagecreatetruecolor($BMP['width'],$BMP['height']);
		   $P = 0;
		   $Y = $BMP['height']-1;
		   while ($Y >= 0)
		   {
		    $X=0;
		    while ($X < $BMP['width'])
		    {
		     if ($BMP['bits_per_pixel'] == 24)
		        $COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
		     elseif ($BMP['bits_per_pixel'] == 16)
		     {  
		        $COLOR = unpack("n",substr($IMG,$P,2));
		        $COLOR[1] = $PALETTE[$COLOR[1]+1];
		     }
		     elseif ($BMP['bits_per_pixel'] == 8)
		     {  
		        $COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
		        $COLOR[1] = $PALETTE[$COLOR[1]+1];
		     }
		     elseif ($BMP['bits_per_pixel'] == 4)
		     {
		        $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
		        if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
		        $COLOR[1] = $PALETTE[$COLOR[1]+1];
		     }
		     elseif ($BMP['bits_per_pixel'] == 1)
		     {
		        $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
		        if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
		        elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
		        elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
		        elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
		        elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
		        elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
		        elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
		        elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
		        $COLOR[1] = $PALETTE[$COLOR[1]+1];
		     }
		     else
		        return FALSE;
		     imagesetpixel($res,$X,$Y,$COLOR[1]);
		     $X++;
		     $P += $BMP['bytes_per_pixel'];
		    }
		    $Y--;
		    $P+=$BMP['decal'];
		   }

		 //Fermeture du fichier
		   fclose($f1);

		 return $res;
		}
		
		public function getCollectionUrl(){
			$helper = PhotoCollectionHelper::getInstance();
			return "/collection.php?type=".$helper->getType()."&ID=".$helper->getID()."&context=".$helper->getContext()."&genID=".$helper->getGenID();
		}
		
		public function getShareLink(Photo $photo){
			return $this->getCollectionUrl()."#".$photo->getID();
		}
	}