<?php
	require_once("travellog/UIComponent/NewCollection/model/Class.AbstractUploadedPhoto.php");

	class UploadedPhotoGroupAlbum extends AbstractUploadedPhoto{
		
		public function __construct(){
			$this->mContext = self::ALBUM_CONTEXT;
		}
		
		public function processFile(PathManager $pm, $objContext=NULL){
			if( !$this->prepareFile($pm,$objContext) ){
				return FALSE;
			}
			
			$this->createFullsize();
			$this->createStandard();
			$this->createFeatured();

			imagedestroy($this->mImageCopy);
			
			$this->sendS3RawFiles();
			
			//create photo
			$photo = new Photo($objContext);
			$photo->setPhotoTypeID(self::PHOTOALBUM);
			$photo->setCaption($this->mCaption);			
			$photo->setFileName($this->mFilename);
			$photo->Create();

			if( !$this->mContextObject->getPrimary() || is_null($this->mContextObject->getPrimary()) ){
				$this->mContextObject->setAsPhotoAlbumPrimary($photo->getPhotoID());
			}
			
			return $photo;
		}
		
		public function fixBlackPhoto(PathManager $pm, Photo $photo){
			try{
				$this->prepareFixBlackPhoto($pm,$photo);
			
				$this->createFullsize();
				$this->createStandard();
				$this->createFeatured();
				
				imagedestroy($this->mImageCopy);
				
				$this->sendS3RawFiles();
			}catch(exception $e){
				throw $e;
			}
		}
	}