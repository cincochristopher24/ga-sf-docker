<?php

require_once("travellog/UIComponent/NewCollection/loader/Class.AbstractPhotoCollectionLoader.php");
	
class MainGroupTemplatePhotoCollectionLoader extends AbstractPhotoCollectionLoader {
		
	protected function loadCollection() {
		$albums = GanetPhotoService::getPhotoAlbums($this->getOwner()->getID(), 3);
		$dto_factory = PhotoCollectionDTOFactory::getInstance();
		
		foreach ($albums as $album) {
			$collection = $dto_factory->create(self::GROUP_TEMPLATE_CONTEXT);			
			$collection->setType(self::GROUP_TEMPLATE_TYPE);
			$collection->setID($this->getOwner()->getID());
			$collection->setGenID($album->getID());
			$collection->setTitle($album->getTitle());
			$collection->setPrimaryPhotoID($album->getPrimaryPhotoID());
			$photoIDs = array();
			foreach ($album->getGanetPhotos() as $photo) {
				$photoIDs[] = $photo->getID();
			}
			$collection->setPhotoIDs($photoIDs);
			
			$this->addCollection($collection);
		}
	}
}