<?php

	require_once("travellog/UIComponent/NewCollection/loader/Class.AbstractPhotoCollectionLoader.php");
	
	abstract class AbstractTravelerPhotoCollectionLoader extends AbstractPhotoCollectionLoader{
		
		private $mProfilePhotoCollection = array();
		
		abstract protected function loadJournalPhotoCollectionForPublic();
		
		protected function loadCollection(){
			$collectionLoaded = FALSE;
			$key = $this->generateCacheKey();
			$cache	=	ganetCacheProvider::instance()->getCache();
			if ( $cache != null ) {
				if ( $collectionArray = $cache->get($key) ){
					foreach($collectionArray AS $collection){
						$this->addCollection($collection);
					}
					$collectionLoaded = TRUE;
				}
			}
			if( !$collectionLoaded ){
				$this->loadProfilePhotoCollection();
				if( $this->isAdminView() ){
					$this->loadAlbumPhotoCollectionForOwner();
					$this->loadJournalPhotoCollectionForOwner();
					$this->loadArticlePhotoCollectionForOwner();
				}else{
					$this->loadAlbumPhotoCollectionForPublic();
					$this->loadJournalPhotoCollectionForPublic();
					$this->loadArticlePhotoCollectionForPublic();
				}
				if ($cache != null) {
					$collectionArray = $this->getCollection();
					$cache->set($key,$collectionArray,array('EXPIRE'=>10800));
				}
			}
		}
		
		protected function addCollection($collection){
			if( $collection->getContext() == self::PROFILE_CONTEXT ){
				$this->mProfilePhotoCollection[] = $collection;
			}else{
				parent::addCollection($collection);
			}
		}
		
		public function hasCollection(){
			return 0 < $this->getCollectionCount();
		}
		
		public function getCollectionCount(){
			return count($this->getCollection());
		}
		
		public function getCollection(){
			$collection = array();
			foreach($this->mProfilePhotoCollection AS $temp){
				$collection[] = $temp;
			}
			return array_merge($collection,parent::getCollection());
		}
		
		public function getCollectionItem($context=NULL,$genID=NULL){
			$collectionItem = FALSE;
			if( self::PROFILE_CONTEXT == $context ){
				$collectionItem = empty($this->mProfilePhotoCollection) ? $collectionItem : $this->mProfilePhotoCollection[0];
			}else if( is_null($context) && is_null($genID) ){
				$collectionItem = $this->getTopCollectionItem();
			}else{
				$collectionItem = parent::getCollectionItem($context,$genID);
			}
			return $collectionItem;
		}
		
		public function getTopCollectionItem(){
			$topCollectionItem = FALSE;
			if( count($this->mProfilePhotoCollection) ){
				$topCollectionItem = $this->mProfilePhotoCollection[0];
			}else{
				$topCollectionItem = parent::getTopCollectionItem();
			}
			return $topCollectionItem;
		}
		
		private function loadProfilePhotoCollection(){
			$ownerID = $this->getOwner()->getTravelerID();
					
			$sql = "SELECT tblPhoto.photoID, tblPhoto.primaryphoto
					FROM tblPhoto, tblTravelertoPhoto
					WHERE tblPhoto.phototypeID = 1
						AND tblPhoto.photoID = tblTravelertoPhoto.photoID
						AND tblTravelertoPhoto.travelerID = $ownerID
					GROUP BY tblPhoto.photoID
					ORDER BY tblTravelertoPhoto.position";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
			
			$photoIDs = array();
			$primaryPhotoID = 0;
			while($data=mysql_fetch_array($rs->Resultset())){
				$photoIDs[] = $data["photoID"];
				$primaryPhotoID = (1 == $data["primaryphoto"]) ? $data["photoID"] : $primaryPhotoID;
			}
			
			if( !empty($photoIDs) || $this->isAdminView() ){
				$profilePhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::PROFILE_CONTEXT);
				$profilePhotoCollection->setID($ownerID);
				$profilePhotoCollection->setGenID($ownerID);
				$profilePhotoCollection->setPhotoIDs($photoIDs);
				$profilePhotoCollection->setPrimaryPhotoID($primaryPhotoID);
				
				$this->addCollection($profilePhotoCollection);
			}
		}
		
		protected function loadJournalPhotoCollectionForOwner(){
			$ownerID = $this->getOwner()->getTravelerID();
			
			$sql = "SELECT tblTravel.travelID, tblTravel.primaryphoto, tblTravel.title AS journalTitle, 
						tblTravelLog.travellogID AS travellogID, tblTravelLog.title AS entryTitle
					FROM tblTravel, tblTravelLink, tblTravelLog
					WHERE tblTravel.travelerID = $ownerID
						AND tblTravel.publish = 1 
						AND tblTravel.deleted = 0
						AND tblTravel.travellinkID = tblTravelLink.travellinkID
						AND tblTravelLink.refType = 1
						AND tblTravelLink.refID = $ownerID
						AND tblTravel.travelID = tblTravelLog.travelID
						AND tblTravelLog.publish = 1
						AND tblTravelLog.deleted = 0
					GROUP BY travellogID
					ORDER BY tblTravel.travelID DESC, travellogID DESC";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
			
			$primaryPhotoID = 0;
			$previousTravelID = NULL;
			$journalPhotoCollection = NULL;
			while($data=mysql_fetch_array($rs->Resultset())){
				if( $previousTravelID != $data["travelID"] ){
					if( !is_null($journalPhotoCollection) ){
						$this->addCollection($journalPhotoCollection);
					}
					$previousTravelID = $data["travelID"];
					$journalPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::JOURNAL_CONTEXT);
					$journalPhotoCollection->setType(self::TRAVELER_TYPE);
					$journalPhotoCollection->setID($ownerID);
					$journalPhotoCollection->setGenID($data["travelID"]);
					$journalPhotoCollection->setTitle($data["journalTitle"]);
					$journalPhotoCollection->setPrimaryPhotoID($data["primaryphoto"]);
				}
				
				$travellogID = $data["travellogID"];
				$entryTitle = $data["entryTitle"];
				
				$sql2 = "SELECT tblPhoto.photoID, tblPhoto.primaryphoto
						 FROM tblPhoto, tblTravelLogtoPhoto
						 WHERE tblPhoto.phototypeID = 2
							AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID
							AND tblTravelLogtoPhoto.travellogID = $travellogID
						 GROUP BY tblPhoto.photoID
						 ORDER BY tblTravelLogtoPhoto.position";
				
				$rs2 = $this->mConnectionProvider->getRecordset2();
				$rs2->Execute($sql2);
				
				$photoIDs = array();
				$primaryPhotoID2 = 0;
				while($data2=mysql_fetch_array($rs2->Resultset())){
					$photoIDs[] = $data2["photoID"];
					$primaryPhotoID2 = (1 == $data2["primaryphoto"]) ? $data2["photoID"] : $primaryPhotoID2;
				}
				
				$travellogPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::TRAVELLOG_CONTEXT);
				$travellogPhotoCollection->setType(self::TRAVELER_TYPE);
				$travellogPhotoCollection->setID($ownerID);
				$travellogPhotoCollection->setGenID($travellogID);
				$travellogPhotoCollection->setTitle($entryTitle);
				$travellogPhotoCollection->setPrimaryPhotoID($primaryPhotoID2);
				$travellogPhotoCollection->setPhotoIDs($photoIDs);
				
				$journalPhotoCollection->addTravellogCollection($travellogPhotoCollection);
			}
			if( !is_null($journalPhotoCollection) ){
				$this->addCollection($journalPhotoCollection);
			}
		}
		
		private function loadAlbumPhotoCollectionForOwner(){
			$ownerID = $this->getOwner()->getTravelerID();
			
			$sql = "SELECT tblPhotoAlbum.photoalbumID, tblPhotoAlbum.title, tblPhotoAlbum.primaryPhoto AS primaryphoto
					FROM tblPhotoAlbum
					WHERE tblPhotoAlbum.creator = $ownerID
						AND tblPhotoAlbum.groupID = 0
					GROUP BY tblPhotoAlbum.photoalbumID
					ORDER BY tblPhotoAlbum.photoalbumID DESC";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
			
			while($data=mysql_fetch_array($rs->Resultset())){
				$albumID = $data["photoalbumID"];
				$albumPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::TRAVALBUM_CONTEXT);
				$albumPhotoCollection->setID($ownerID);
				$albumPhotoCollection->setGenID($albumID);
				$albumPhotoCollection->setTitle($data["title"]);
				$albumPhotoCollection->setPrimaryPhotoID($data["primaryphoto"]);
				
				$sql2 = "SELECT tblPhoto.photoID
						 FROM tblPhoto, tblPhotoAlbumtoPhoto
						 WHERE tblPhoto.phototypeID = 7
						 	AND tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID
							AND tblPhotoAlbumtoPhoto.photoalbumID = $albumID
						 GROUP BY tblPhoto.photoID
						 ORDER BY tblPhotoAlbumtoPhoto.position ASC";
				
				$rs2 = $this->mConnectionProvider->getRecordset2();
				$rs2->Execute($sql2);
				
				$photoIDs = array();
				while($data2=mysql_fetch_array($rs2->Resultset())){
					$photoIDs[] = $data2["photoID"];
				}
				
				$albumPhotoCollection->setPhotoIDs($photoIDs);
				
				$this->addCollection($albumPhotoCollection);
			}
		}
		
		private function loadAlbumPhotoCollectionForPublic(){
			$ownerID = $this->getOwner()->getTravelerID();
			
			$sql = "SELECT tblPhotoAlbum.photoalbumID AS albumID, tblPhotoAlbum.title, 
						tblPhoto.primaryPhoto AS primaryphoto, tblPhoto.photoID
					FROM tblPhoto, tblPhotoAlbum, tblPhotoAlbumtoPhoto
					WHERE tblPhoto.phototypeID = 7
						AND tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID
						AND tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID
						AND tblPhotoAlbum.creator = $ownerID
						AND tblPhotoAlbum.groupID = 0
					GROUP BY tblPhoto.photoID
					ORDER BY albumID DESC, tblPhotoAlbumtoPhoto.position";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
				
			$photoIDs = array();
			$primaryPhotoID = 0;
			$previousAlbumID = NULL;
			$albumPhotoCollection = NULL;
			while($data=mysql_fetch_array($rs->Resultset())){
				if( $previousAlbumID != $data["albumID"] ){
					if( !is_null($albumPhotoCollection) ){
						$albumPhotoCollection->setPhotoIDs($photoIDs);
						$albumPhotoCollection->setPrimaryPhotoID($primaryPhotoID);
						
						$this->addCollection($albumPhotoCollection);
					}
					
					$previousAlbumID = $data["albumID"];
					$photoIDs = array();
					$albumPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::TRAVALBUM_CONTEXT);
					$albumPhotoCollection->setID($ownerID);
					$albumPhotoCollection->setGenID($previousAlbumID);
					$albumPhotoCollection->setTitle($data["title"]);
				}
				$photoIDs[] = $data["photoID"];
				$primaryPhotoID = (1 == $data["primaryphoto"]) ? $data["photoID"] : $primaryPhotoID;
			}
			if( !is_null($albumPhotoCollection) ){
				$albumPhotoCollection->setPhotoIDs($photoIDs);
				$albumPhotoCollection->setPrimaryPhotoID($primaryPhotoID);
				
				$this->addCollection($albumPhotoCollection);
			}
		}
		
		private function loadArticlePhotoCollectionForOwner(){
			$ownerID = $this->getOwner()->getTravelerID();
			
			$sql = "SELECT tblArticle.articleID, tblArticle.title
					FROM tblArticle
					WHERE tblArticle.authorID = $ownerID
						AND tblArticle.groupID = 0
						AND tblArticle.deleted = 0
					GROUP BY tblArticle.articleID
					ORDER BY tblArticle.articleID DESC";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);

			while($data=mysql_fetch_array($rs->Resultset())){
				$articleID = $data["articleID"];
				$articlePhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::ARTICLE_CONTEXT);
				$articlePhotoCollection->setType(self::TRAVELER_TYPE);
				$articlePhotoCollection->setID($ownerID);
				$articlePhotoCollection->setGenID($articleID);
				$articlePhotoCollection->setTitle($data["title"]);

				$sql2 = "SELECT tblPhoto.photoID, tblPhoto.primaryphoto
						 FROM tblPhoto, tblArticletoPhoto
						 WHERE tblPhoto.phototypeID = 11
						 	AND tblPhoto.photoID = tblArticletoPhoto.photoID
							AND tblArticletoPhoto.articleID = $articleID
						 GROUP BY tblPhoto.photoID
						 ORDER BY tblArticletoPhoto.position";
				
				$rs2 = $this->mConnectionProvider->getRecordset2();
				$rs2->Execute($sql2);

				$photoIDs = array();
				$primaryPhotoID = 0;
				while($data2=mysql_fetch_array($rs2->Resultset())){
					$photoIDs[] = $data2["photoID"];
					$primaryPhotoID = (1 == $data2["primaryphoto"]) ? $data2["photoID"] : $primaryPhotoID;
				}

				$articlePhotoCollection->setPhotoIDs($photoIDs);
				$articlePhotoCollection->setPrimaryPhotoID($primaryPhotoID);
				
				$this->addCollection($articlePhotoCollection);
			}
		}
		
		private function loadArticlePhotoCollectionForPublic(){
			$ownerID = $this->getOwner()->getTravelerID();
			
			$sql = "SELECT tblArticle.title, tblArticletoPhoto.articleID, 
						tblPhoto.photoID, tblPhoto.primaryphoto
					FROM tblArticle, tblArticletoPhoto, tblPhoto
					WHERE tblArticle.authorID = $ownerID
						AND tblArticle.groupID = 0
						AND tblArticle.deleted = 0
						AND tblArticle.articleID = tblArticletoPhoto.articleID
						AND tblArticletoPhoto.photoID = tblPhoto.photoID
						AND tblPhoto.phototypeID = 11
					GROUP BY tblPhoto.photoID
					ORDER BY tblArticletoPhoto.articleID DESC, tblArticletoPhoto.position";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);

			$photoIDs = array();
			$primaryPhotoID = 0;
			$previousArticleID = NULL;
			$articlePhotoCollection = NULL;
			while($data=mysql_fetch_array($rs->Resultset())){
				if( $previousArticleID != $data["articleID"] ){
					if( !is_null($articlePhotoCollection) ){
						$articlePhotoCollection->setPhotoIDs($photoIDs);
						$articlePhotoCollection->setPrimaryPhotoID($primaryPhotoID);
						
						$this->addCollection($articlePhotoCollection);
					}
					
					$previousArticleID = $data["articleID"];
					$photoIDs = array();
					$articlePhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::ARTICLE_CONTEXT);
					$articlePhotoCollection->setType(self::TRAVELER_TYPE);
					$articlePhotoCollection->setID($ownerID);
					$articlePhotoCollection->setGenID($previousArticleID);
					$articlePhotoCollection->setTitle($data["title"]);
				}
				$photoIDs[] = $data["photoID"];
				$primaryPhotoID = (1 == $data["primaryphoto"]) ? $data["photoID"] : $primaryPhotoID;
			}
			if( !is_null($articlePhotoCollection) ){
				$articlePhotoCollection->setPhotoIDs($photoIDs);
				$articlePhotoCollection->setPrimaryPhotoID($primaryPhotoID);
				
				$this->addCollection($articlePhotoCollection);
			}
		}
	}