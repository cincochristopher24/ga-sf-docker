<?php

	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");

	class PhotoCollectionHelper implements PhotoCollectionConstants{
		private static $instance = NULL;
		
		private $mType		=	NULL;
		private $mID		=	NULL;
		private $mContext	=	NULL;
		private $mGenID		=	NULL;
		
		private $mTravellogID	=	NULL;
		private $mMode		=	NULL;
		
		private $mUserLevel	=	self::VIEWER;
		private $mLoggedID	=	0;
		
		private $mTraveler	=	NULL;
		private $mGroup		=	NULL;
		
		private $mAction	=	self::VIEW_COLLECTION;
		
		private $mCollection	=	array();
		private $mActiveCollectionItem = NULL;
		
		private $mParams	=	array();
		
		private function __construct(){
		}
		
		public static function getInstance(){
			if( is_null(self::$instance) ){
				self::$instance = new PhotoCollectionHelper();
			}
			return self::$instance;
		}
		
		public function setType($type=NULL){
			$this->mType = $type;
		}
		public function setID($ID=NULL){
			$this->mID = $ID;
		}
		public function setContext($context=NULL){
			$this->mContext = $context; 
		}
		public function setGenID($genID=NULL){
			$this->mGenID = $genID;
		}
		public function setUserLevel($userLevel=0){
			$this->mUserLevel = $userLevel;
		}
		public function setLoggedID($loggedID=0){
			$this->mLoggedID = $loggedID;
		}
		public function setTraveler($traveler=NULL){
			$this->mTraveler = $traveler;
		}
		public function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		public function setAction($action=0){
			$this->mAction = $action;
		}
		public function setTravellogID($tlogID=NULL){
			$this->mTravellogID = $tlogID;
		}
		public function setMode($mode=NULL){
			$this->mMode = $mode;
		}
		
		public function getType(){
			return $this->mType;
		}
		public function getID(){
			return $this->mID;
		}
		public function getContext(){
			return $this->mContext;
		}
		public function getGenID(){
			return $this->mGenID;
		}
		public function getUserLevel(){
			return $this->mUserLevel;
		}
		public function getLoggedID(){
			return $this->mLoggedID;
		}
		public function getTraveler(){
			return $this->mTraveler;
		}
		public function getGroup(){
			return $this->mGroup;
		}
		public function getAction(){
			return $this->mAction;
		}
		public function getTravellogID(){
			return $this->mTravellogID;
		}
		public function getMode(){
			return $this->mMode;
		}
		
		public function getOwner(){
			if( self::TRAVELER_TYPE == $this->getType() ){
				return $this->getTraveler();
			}else if( self::GROUP_TYPE == $this->getType() ){
				return $this->getGroup();
			}else if (self::GROUP_TEMPLATE_TYPE == $this->getType()) {
				return GanetGroupTemplatePeer::retrieveByPK($this->getID());
			}
		}
		public function viewerHasAdminPrivileges(){
			if( self::TRAVELER_TYPE == $this->getType() ){
				return self::VIEWER != $this->getUserLevel();
			}else if( self::GROUP_TYPE == $this->getType() ){
				return self::STAFF <= $this->getUserLevel();
			}else if(self::GROUP_TEMPLATE_TYPE == $this->getType()){
				return self::STAFF <= $this->getUserLevel();
			}
			return FALSE;
		}
		public function _isAjaxRequest(){
			if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
				return TRUE;
			}
			if(	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				return TRUE;
			}
			return FALSE;
		}
		public function setCollection($collection=array()){
			$this->mCollection = $collection;
		}
		public function getCollection(){
			return $this->mCollection;
		}
		public function getAllCollectionPhotoCount(){
			$photocount = 0;
			foreach($this->getCollection() AS $collection){
				$photocount += $collection->getPhotoCount();
			}
			return $photocount;
		}
		public function setActiveCollectionItem($collectionItem=NULL){
			$this->mActiveCollectionItem = $collectionItem;
		}
		public function getActiveCollectionItem(){
			return $this->mActiveCollectionItem;
		}
		public function validContext($context=NULL){
			$contexts = array(	self::PROFILE_CONTEXT,
								self::JOURNAL_CONTEXT,
								self::TRAVELLOG_CONTEXT,
								self::ALBUM_CONTEXT,
								self::TRAVALBUM_CONTEXT,
								self::ARTICLE_CONTEXT, self::GROUP_TEMPLATE_CONTEXT);
			if( is_null($context) ){
				return in_array($this->getContext(),$contexts);
			}else{
				return in_array($context,$contexts);
			}
		}
		public function validGenID($genID=NULL){
			if( is_null($genID) ){
				return is_numeric($this->getGenID());
			}else{
				return is_numeric($genID);
			}
		}
		public function setParams($params=array()){
			$this->mParams = $params;
		}
		public function getParams(){
			return $this->mParams;
		}
	}