<?php

	require_once("travellog/UIComponent/NewCollection/dto/Class.AbstractPhotoCollectionDTO.php");

	class TravellogPhotoCollectionDTO extends AbstractPhotoCollectionDTO{
		
		public function __construct(){
			$this->mContext = self::TRAVELLOG_CONTEXT;
		}
		
		public function getContextObject(){
			if( is_null($this->mContextObject) ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass('TravelLog');
				$this->mContextObject = $file_factory->getClass('TravelLog', array($this->getGenID()));
			}
			return $this->mContextObject;
		}
		
		public function getJournalCollectionLink(){
			return "/collection.php?type=".$this->getType()."&ID=".$this->getID()."&context=journal&genID=".$this->getContextObject()->getTravel()->getTravelID();
		}
	}