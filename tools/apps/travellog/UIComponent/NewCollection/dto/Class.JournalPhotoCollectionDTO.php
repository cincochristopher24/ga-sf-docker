<?php

	require_once("travellog/UIComponent/NewCollection/dto/Class.AbstractPhotoCollectionDTO.php");

	class JournalPhotoCollectionDTO extends AbstractPhotoCollectionDTO{
		
		private $mTravellogCollection = array();
		private $mTravellogContexts = NULL;
		
		public function __construct(){
			$this->mContext = self::JOURNAL_CONTEXT;
		}
		
		public function getPhotoCount(){
			$photocount = 0;
			foreach($this->getTravellogCollection() AS $travellogCollection){
				$photocount += $travellogCollection->getPhotoCount();
			}
			return $photocount;
		}
		
		public function hasPhotos(){
			return 0 < $this->getPhotoCount();
		}
		
		public function getContextObject(){
			if( is_null($this->mContextObject) ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass('Travel');
				$this->mContextObject = $file_factory->getClass('Travel', array($this->getGenID()));
			}
			return $this->mContextObject;
		}
		
		public function addTravellogCollection(TravellogPhotoCollectionDTO $collection){
			$this->mTravellogCollection[] = $collection;
			$this->mPhotoIDs = array_merge($this->getPhotoIDs(),$collection->getPhotoIDs());
		}
		
		public function getTravellogCollection(){
			return $this->mTravellogCollection;
		}
		
		public function getPrimaryPhoto(){
			return $this->getContextObject()->getPrimaryPhotoInstance();
		}
		
		public function getPhotoInstances(){
			if( is_null($this->mPhotoInstances) ){
				$this->mPhotoInstances = array();
				foreach($this->getTravellogCollection() AS $travellogCollection){
					$this->mPhotoInstances = array_merge($this->mPhotoInstances,$travellogCollection->getPhotoInstances());
				}
			}
			return $this->mPhotoInstances;
		}
		
		public function getTravellogObjects(){
			if( is_null($this->mTravellogContexts) ){
				$this->mTravellogContexts = array();
				foreach($this->getTravellogCollection() AS $travellogCollection){
					$this->mTravellogContexts[] = $travellogCollection->getContextObject();
				}
			}
			return $this->mTravellogContexts;
		}
	}