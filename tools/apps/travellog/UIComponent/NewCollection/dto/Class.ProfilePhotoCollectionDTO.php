<?php

	require_once("travellog/UIComponent/NewCollection/dto/Class.AbstractPhotoCollectionDTO.php");

	class ProfilePhotoCollectionDTO extends AbstractPhotoCollectionDTO{
		
		public function __construct(){
			$this->mType = self::TRAVELER_TYPE;
			$this->mContext = self::PROFILE_CONTEXT;
			$this->mTitle = "Profile Photos";
		}
		
		public function getContextObject(){
			if( is_null($this->mContextObject) ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass('Traveler');
				$traveler = $file_factory->getClass('Traveler', array($this->getGenID()));
				$this->mContextObject = $traveler->getTravelerProfile();
			}
			return $this->mContextObject;
		}
	}