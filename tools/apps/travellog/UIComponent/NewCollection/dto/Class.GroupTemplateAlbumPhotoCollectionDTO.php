<?php

	require_once("travellog/UIComponent/NewCollection/dto/Class.AbstractPhotoCollectionDTO.php");

	class GroupTemplateAlbumPhotoCollectionDTO extends AbstractPhotoCollectionDTO{
		
		public function __construct(){
			$this->mType = self::GROUP_TEMPLATE_TYPE;
			$this->mContext = self::GROUP_TEMPLATE_CONTEXT;
		}
		
		public function getContextObject(){
			if (is_null($this->mContextObject)) {
				$this->mContextObject = GanetPhotoAlbumPeer::retrieveByPK($this->getGenID());
			}
			
			return $this->mContextObject;
		}

		public function getPhotoCount() {
			return $this->getContextObject()->getPhotosCount();
		}
		
		public function getPhotoInstances(){
			if (is_null($this->mPhotoInstances)) {
				$this->mPhotoInstances = GanetPhotoAlbumPeer::retrieveByPK($this->getGenID())->getGanetPhotos();
			}
			
			return $this->mPhotoInstances;
		}
	}