<?php
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/model/Class.UploadedPhotoProfile.php");
	require_once("travellog/UIComponent/NewCollection/model/Class.UploadedPhotoTravelerAlbum.php");
	require_once("travellog/UIComponent/NewCollection/model/Class.UploadedPhotoGroupAlbum.php");
	require_once("travellog/UIComponent/NewCollection/model/Class.UploadedPhotoTravellog.php");
	require_once("travellog/UIComponent/NewCollection/model/Class.UploadedPhotoArticle.php");
	require_once("travellog/UIComponent/NewCollection/model/Class.UploadedPhotoCustomHeader.php");
	require_once("travellog/UIComponent/NewCollection/model/Class.UploadedPhotoGeneric.php");
	require_once("travellog/UIComponent/NewCollection/model/Class.UploadedPhotoGroupTemplate.php");

	class PhotoCollectionUploadedPhotoFactory implements PhotoCollectionConstants{
		
		private static $instance = NULL;
		
		private function __construct(){
			
		}
		
		public static function getInstance(){
			if( is_null(self::$instance) ){
				self::$instance = new PhotoCollectionUploadedPhotoFactory();
			}
			return self::$instance;
		}
		
		function create($context = NULL) {
			if( !in_array($context,array('profile','traveleralbum','photoalbum','travellog','article','custompageheader','generic', PhotoCollectionConstants::GROUP_TEMPLATE_CONTEXT)) ){
				throw new Exception("Invalid argument to PhotoCollectoinUploadedPhotoFactory->create(). Argument should be in {'profile','traveleralbum','photoalbum','travellog','article', PhotoCollectionConstants::GROUP_TEMPLATE_CONTEXT}");
			}
					
			switch ($context) {
				case self::PROFILE_CONTEXT:
					return new UploadedPhotoProfile();

				case self::TRAVALBUM_CONTEXT:	
					return new UploadedPhotoTravelerAlbum();

				case self::ALBUM_CONTEXT:
					return new UploadedPhotoGroupAlbum();

				case self::TRAVELLOG_CONTEXT:
					return new UploadedPhotoTravellog();
				
				case self::ARTICLE_CONTEXT:
					return new UploadedPhotoArticle();

				case self::CUSTOMHEADER_CONTEXT:
					return new UploadedPhotoCustomHeader();
							
				case self::GENERIC_CONTEXT:
					return new UploadedPhotoGeneric();

				case self::GROUP_TEMPLATE_CONTEXT:
					return new UploadedPhotoGroupTemplate();
			}
		}
	}