<?php
	/**
	 * convenience class to generate the generate the cache key and cache invalidation key for 
	 *   collections component in order to prevent duplication of code
	 * K. Gordo - 06-09-09  
	 *   
	 */

	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");

	class CollectionCacheKey extends RequestData {
		
		function generateCacheKey($name){
			
			if ($name == 'content')
				$key	=	'Collection_Content_' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . $_SERVER['QUERY_STRING'];
			
			if ($name == 'list')	
				$key	=	'Collection_List_' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . $_SERVER['QUERY_STRING'];	
			
			return $key;
		}
		
		
		function generateInvalidationKey(){
			return 'Collection_Keys_' . $this->getType() .  '_' . $this->getID();
		}
		
	}


?>