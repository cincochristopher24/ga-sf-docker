<?php
	//ini_set("memory_limit","32M");
	
	//require_once("Image/Transform.php");
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.PathManager.php");
	require_once("travellog/UIComponent/Collection/interface/CollectionConstants.php");

	class UploadedPhoto implements CollectionConstants{
		
		private $mContext	=	NULL;
		private $mGenID		=	NULL;
		
		private $mFile		=	NULL;//image file sent via HTTP POST
		private $mAttrib	=	array(	"width"		=>	0,
										"height"	=>	0	);
		private $mRotationAngle	=	0;
		private $mCaption	=	"";
		private $mFilename	=	"";
		private $mRelPath	=	"";
		private $mPathFilename = "";
		
		private $mImageCopy	= FALSE;
		
		private $mImageInfo = array();
		
		private $mPhotoInstance = NULL;
		
		function setContext($context=NULL){
			$this->mContext = $context;
		}
		function setGenID($genID=NULL){
			$this->mGenID = $genID;
		}
		function setFile($file=NULL){
			$this->mFile = $file;
		}
		function setCaption($caption=""){
			$this->mCaption = $caption;
		}
		function setRotationAngle($angle=0){
			$this->mRotationAngle = $angle;
		}
		function processFile(PathManager $pm, $objContext=NULL){
			if( is_null($objContext) ){
				return FALSE;
			}
			$this->mRelPath = $pm->GetPathrelative();
			switch($this->mFile["type"]){
				case "image/jpeg"	:	$this->mFilename = uniqid().time().".jpg";
										break;
				case "image/gif"	:	$this->mFilename = uniqid().time().".gif";
										break;
				case "image/png"	:	$this->mFilename = uniqid().time().".png";
										break;
				case "image/pjpeg"	:	$this->mFilename = uniqid().time().".jpg";
										break;
				default				:	$this->mFilename = uniqid().time().".jpg";
										break;
			}
			$absolutePath = $pm->getAbsolutePath();
			chmod($absolutePath,0777);
			$isUploaded = move_uploaded_file( $this->mFile["tmp_name"], $absolutePath."orig-".$this->mFilename);
						
			if( !$isUploaded ){
				return FALSE;
			}
			
			$this->mPathFilename = $this->mRelPath."orig-".$this->mFilename;
			if( !$this->resizeOriginal($this->mPathFilename,self::ORIGINAL_SIZE) ){
				unlink($this->mPathFilename);
				return FALSE;
			}

			$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			if( 0 != $this->mRotationAngle ){
				$this->rotateImage($this->mImageCopy,$this->mRotationAngle,$this->mPathFilename);
				imagedestroy($this->mImageCopy);
				unset($this->mImageInfo[$this->mPathFilename]);
				$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			}
			
			$origDim = $this->getDimension($this->mPathFilename);
			if( !$origDim ){
				unlink($this->mPathFilename);
				return FALSE;
			}
			
			$this->mAttrib["width"] = $origDim["width"];
			$this->mAttrib["height"] = $origDim["height"];
			
			if( self::CUSTOMHEADER_CONTEXT != $this->mContext ){
				$this->createFullsize();
				$this->createStandard();
				if( self::ALBUM_CONTEXT == $this->mContext ||
					self::TRAVELLOG_CONTEXT == $this->mContext ||
					self::JOURNAL_CONTEXT == $this->mContext ||
					self::TRAVALBUM_CONTEXT == $this->mContext || 
					self::ARTICLE_CONTEXT == $this->mContext ){
					$this->createFeatured();
				}
				if( self::TRAVELLOG_CONTEXT == $this->mContext ||
					self::JOURNAL_CONTEXT == $this->mContext ||
					self::ARTICLE_CONTEXT == $this->mContext ){
					$this->createEntryHeader();
				}
				if( self::TRAVELLOG_CONTEXT == $this->mContext &&
					$this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
					$this->createCustomPageHeaderFullsize();
					$this->createCustomPageHeaderStandard();
				}
				//$this->createThumbnail(); //commented, create thumbnail in createStandard function right away. no need to look for crop points. just rescale standard to thumbnail
			}else{//custom page header context
				if( !$this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
					unlink($this->mPathFilename);
					return FALSE;
				}
				$this->createCustomPageHeaderFullsize();
				$this->createCustomPageHeaderStandard();
				$this->createThumbnail();
			}
			imagedestroy($this->mImageCopy);
			
			//create photo
			$photo = new Photo($objContext);
			
			switch($this->mContext){
				case self::PROFILE_CONTEXT		:	$photo->setPhotoTypeID(self::PROFILE);
													break;
				case self::ALBUM_CONTEXT		:	$photo->setPhotoTypeID(self::PHOTOALBUM);
													break;
				case self::TRAVALBUM_CONTEXT	:	$photo->setPhotoTypeID(self::PHOTOALBUM);
													break;
				case self::TRAVELLOG_CONTEXT	:	$photo->setPhotoTypeID(self::TRAVELLOG);
													break;
				case self::JOURNAL_CONTEXT		:	$photo->setPhotoTypeID(self::TRAVELLOG);
													break;
				case self::ARTICLE_CONTEXT		:	$photo->setPhotoTypeID(self::ARTICLE);
													break;	
				case self::CUSTOMHEADER_CONTEXT	:	$photo->setPhotoTypeID(self::CUSTOMHEADER);
													break;
			}
			
			$photo->setCaption($this->mCaption);			
			$photo->setFileName($this->mFilename);

			$photo->Create();
			
			if( self::TRAVELLOG_CONTEXT == $this->mContext ){
				$travel = $objContext->getTravel();
				if( 0 == $travel->getPrimaryphoto() ){
					$photo->invalidateCacheEntry();
					$travel->setPrimaryphoto($photo->getPhotoID());
					$travel->Update();
				}

				// Added by: Adelbert Silla
				// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
				//           This is a preperation for updating application static views in facebook
				// Date added: aug. 10, 2009
				require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');
				// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
				fbNeedToUpdateAccountController::saveNewlyEditedAccount($_SESSION['travelerID'],4);

				
				//create a bacjup of the photo in case the journal is deleted with all its entry photos deleted
				//this bacjup photo won't be deleted unless the photo is deleted iindividually or by batch in the photos tab
				$backupFilename = $this->mRelPath."backup-".$objContext->getTravellogID()."-".$this->mFilename;
				copy($this->mPathFilename,$backupFilename);
			}
			
			if( self::ARTICLE_CONTEXT == $this->mContext ){
				$photo->invalidateCacheEntry();
			}
			
			if( self::TRAVELLOG_CONTEXT == $this->mContext &&
				$this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
				$objContext->setQualifiedCustomHeaderPhoto($photo->getPhotoID(),TRUE);
			}
			
			if( self::ALBUM_CONTEXT == $this->mContext
				|| self::TRAVALBUM_CONTEXT == $this->mContext ){
				try{
					require_once("travellog/model/Class.PhotoAlbum.php");
					$album = new PhotoAlbum($this->mGenID);
					if( $album instanceof PhotoAlbum && (!$album->getPrimary() || is_null($album->getPrimary())) ){
						$album->setAsPhotoAlbumPrimary($photo->getPhotoID());
					}
				}catch(exception $e){
				}
			}
			
			return $photo;
		}
		
		function createFullsize(){
			$newPathFilename = $this->mRelPath."fullsize".$this->mFilename;
			$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::FULLSIZE,$newPathFilename);
		}
		function createStandard(){
			$newPathFilename = $this->mRelPath."standard".$this->mFilename;
			$size = ( self::PROFILE_CONTEXT == $this->mContext ) ? self::STANDARD_PROFILE : self::STANDARD; 
			//$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,$size,$newPathFilename);
			$cropPoint = $this->getCropPoint($this->mAttrib["width"],$this->mAttrib["height"]);
			$length = ($this->mAttrib["width"] > $this->mAttrib["height"])? $this->mAttrib["height"] : $this->mAttrib["width"];
			$croppedImage = $this->makeCroppedImage($this->mPathFilename,$this->mImageCopy,$this->mAttrib,$cropPoint["x1"],$cropPoint["y1"],$length,$length);
			$this->makeResizedCopy($this->mPathFilename,$croppedImage,array("width"=>$length,"height"=>$length),$size,$newPathFilename);
			
			//create thumbnail from standard
			$newPathFilename = $this->mRelPath."thumbnail".$this->mFilename;
			$this->makeResizedCopy($this->mPathFilename,$croppedImage,array("width"=>$length,"height"=>$length),self::THUMBNAIL,$newPathFilename);	
			imagedestroy($croppedImage);
		}
		function createFeatured(){
			$newPathFilename = $this->mRelPath."featured".$this->mFilename;
			$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::FEATURED,$newPathFilename);
		}
		function createThumbnail(){
			$newPathFilename = $this->mRelPath."thumbnail".$this->mFilename;
			$cropPoint = $this->getCropPoint($this->mAttrib["width"],$this->mAttrib["height"]);
			$length = ($this->mAttrib["width"] > $this->mAttrib["height"])? $this->mAttrib["height"] : $this->mAttrib["width"];
			$croppedImage = $this->makeCroppedImage($this->mPathFilename,$this->mImageCopy,$this->mAttrib,$cropPoint["x1"],$cropPoint["y1"],$length,$length);
			$this->makeResizedCopy($this->mPathFilename,$croppedImage,array("width"=>$length,"height"=>$length),self::THUMBNAIL,$newPathFilename);
			imagedestroy($croppedImage);
		}
		
		/*
		Journal Entry Primary Photo Resizing and Cropping Rules
		1. Check photo orientation
		- if photo has a landscape orientation, proceed to Step 2 (square photos are treated as landscape)
		- if photo has a portrait orientation, proceed to Step 3
		2. Check landscape photo size
		- if photo width is wider than or equal to 751px, proceed to Step 4
		- if photo width is shorter than 751px but greater than 385px, proceed to Step 5
		- if photo width is shorter than or equal to 385px, leave as is; We're done
		3. Check portrait photo size
		- if photo height is taller than 385px, proceed to Step 6
		- if photo height is shorter than or equal to 385px, leave as is; We're done
		4. Unless width is 751px already, resize photo so that width is 751px 
			(height and width ratio must remain the same) and check height
		- if photo height is taller than 493px, crop height to 493px (cut-off top and bottom evenly). We're done.
		- if photo height is shorter than or equal to 493px, leave as is; We're done.
		5. Resize photo so that the width is 385px (height and width ratio must remain the same); We're done.
		6. Resize photo so that the height is 385px (height and width ration must remain the same); We're done.
		*/
		function createEntryHeader(){
			$newPathFilename = $this->mRelPath."jentryheader".$this->mFilename;
			if( $this->isPortrait() ){//portrait
				if( self::JENTRY_HEIGHT < $this->mAttrib["height"] ){//step 3-a
					$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::JENTRY_HEIGHT,$newPathFilename);
				}else{//step 3-b
					$this->createImage($this->mPathFilename,$this->mImageCopy,$newPathFilename);
				}
			}else{//landscape
				if( self::JENTRY_WIDTH < $this->mAttrib["width"]){//step 2-a
					//step 4
					$tempImage = $this->createResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::JENTRY_WIDTH);
					$width = imagesx($tempImage);
					$height = imagesy($tempImage);
					if( self::JENTRY_HEIGHT2 < $height ){//step 4-a
						$cropY = floor($height / 2) - floor(self::JENTRY_HEIGHT2/2);
						$tempImage = $this->makeCroppedImage($this->mPathFilename,$tempImage,array("width"=>$width,"height"=>$height),0,$cropY,$width,self::JENTRY_HEIGHT2);
					}
					//save $tempImage
					$this->createImage($this->mPathFilename,$tempImage,$newPathFilename);
					imagedestroy($tempImage);
				}else if( self::JENTRY_WIDTH > $this->mAttrib["width"] && self::JENTRY_HEIGHT < $this->mAttrib["width"] ){//step 2-b
					$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::JENTRY_HEIGHT,$newPathFilename);
				}else{//step 2-c
					$this->createImage($this->mPathFilename,$this->mImageCopy,$newPathFilename);
				}
			}
		}
		function createImage($origPathFilename,$imageCopy,$newPathFilename){
			$imagetype = $this->getImageType($origPathFilename);//getimagesize($origPathFilename);
			ini_set("memory_limit",$this->getMemoryForImage($origPathFilename)."M");
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagejpeg($imageCopy, $newPathFilename, 90);
						 	break;
					case self::GIF	:					
							imagegif($imageCopy, $newPathFilename);
						   	break;
					case self::PNG	:						
							imagepng($imageCopy, $newPathFilename);
						  	break;
					default:
							imagejpeg($imageCopy, $newPathFilename, 90);
							break;		
			}
		}
		private function isPortrait(){
			return ($this->mAttrib["width"] <= $this->mAttrib["height"])? TRUE : FALSE;
		}
		
		function getDimension($pathFilename){
			try{
				if( !isset($this->mImageInfo[$pathFilename]) ){
					$this->mImageInfo[$pathFilename] = getimagesize($pathFilename);
				}
				$size = list($width, $height, $type, $attr) = $this->mImageInfo[$pathFilename];
				if ( !$size ){
					return FALSE;
				}
			}catch(exception $e){
				return FALSE;
			}
			return array("width"=>$width, "height"=>$height, "type"=>$type);
		}
		
		function getImageType($pathFilename){
			try{
				if( !isset($this->mImageInfo[$pathFilename]) ){
					$this->mImageInfo[$pathFilename] = getimagesize($pathFilename);
				}
				$imagetype = $this->mImageInfo[$pathFilename];
				if( !$imagetype ){
					return FALSE;
				}
				return $imagetype;
			}catch(exception $e){
				return FALSE;
			}
		}
		
		function getRescaledDimension($width,$height,$targetSize){
			$ratio = 1;
			if ( $width > $targetSize && $width == $height ){//square
				$ratio = $targetSize / $width;
			}else if ( $width > $targetSize && $width > $height ){//landscape
				$ratio = $targetSize / $width;
			}else if ( $height > $targetSize && $height > $width ){//portrait
				$ratio = $targetSize / $height;
			}
			$height = round($height*$ratio);
			$width = round($width*$ratio);
			
			return array("width"=>$width, "height"=>$height);
		}
		function getImageCopy($origPathFilename){
			
			ini_set("memory_limit",$this->getMemoryForImage($origPathFilename)."M");				
			
			$imagetype = $this->getImageType($origPathFilename);//getimagesize($origPathFilename);		
			$image = FALSE;
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
						   	$image = imagecreatefromjpeg($origPathFilename); 
							break;
					case self::GIF	:					
						   	$image = imagecreatefromgif($origPathFilename); 	
							break;
					case self::PNG	:
							$image = imagecreatefrompng($origPathFilename); 						
						  	break;
					default	:
							$image = imagecreatefromjpeg($origPathFilename);
							break;		
			}
			return $image;
		}
		function makeResizedCopy($oldPathFilename,$imageCopy,$attrib,$size,$newPathFilename,$fit=FALSE){//saves the resized copy
			
			$imagetype = $this->getImageType($oldPathFilename);//getimagesize($oldPathFilename);
			
			$oldWidth = $attrib["width"];
			$oldHeight = $attrib["height"];
			
			if( $fit ){
				$newWidth = $size;
				$newHeight = $size;
			}else{
				$newDimension = $this->getRescaledDimension($oldWidth,$oldHeight,$size);
				$newWidth = $newDimension["width"];
				$newHeight = $newDimension["height"];
			}
			
			ini_set("memory_limit",$this->getMemoryForImage($oldPathFilename)."M");
			
			$image_p = @imagecreatetruecolor($newWidth, $newHeight);
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagejpeg($image_p, $newPathFilename, 90);
						 	break;
					case self::GIF	:					
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      			
							imagegif($image_p, $newPathFilename);
						   	break;
					case self::PNG	:						
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagepng($image_p, $newPathFilename);
						  	break;
					default:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagejpeg($image_p, $newPathFilename, 90);
							break;		
			}
			imagedestroy($image_p);
		}
		function createResizedCopy($oldPathFilename,$imageCopy,$attrib,$size){
			$imagetype = $this->getImageType($oldPathFilename);//getimagesize($oldPathFilename);
			
			$oldWidth = $attrib["width"];
			$oldHeight = $attrib["height"];
			
			$newDimension = $this->getRescaledDimension($oldWidth,$oldHeight,$size);
			$newWidth = $newDimension["width"];
			$newHeight = $newDimension["height"];
			
			ini_set("memory_limit",$this->getMemoryForImage($oldPathFilename)."M");
			
			$image_p = @imagecreatetruecolor($newWidth, $newHeight);
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
						 	break;
					case self::GIF	:					
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      			
						   	break;
					case self::PNG	:						
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
						  	break;
					default:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							break;		
			}
			return $image_p;
		}
		function makeCroppedImage($oldPathFilename,$imageCopy,$attrib,$x1,$y1,$width,$height){
			
			$imagetype = $this->getImageType($oldPathFilename);//getimagesize($oldPathFilename);
			
			$oldWidth = $attrib["width"];
			$oldHeight = $attrib["height"];
			
			ini_set("memory_limit",$this->getMemoryForImage($oldPathFilename)."M");
			
			$image_p = @imagecreatetruecolor($width, $height);
			if ( !$image_p ){
				return FALSE;
			}	

			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagecopy($image_p, $imageCopy, 0, 0, $x1, $y1, $oldWidth, $oldHeight);      	
						 	break;
					case self::GIF	:				
						   	imagecopy($image_p, $imageCopy, 0, 0, $x1, $y1, $oldWidth, $oldHeight);      			
						   	break;
					case self::PNG	:					
							imagecopy($image_p, $imageCopy, 0, 0, $x1, $y1, $oldWidth, $oldHeight);      	
						  	break;
					default	:
							imagecopy($image_p, $imageCopy, 0, 0, $x1, $y1, $oldWidth, $oldHeight);   
							break;		
			}
			
			return $image_p;
		}
		function getCropPoint($width,$height){		
			$x1 = 0;
			$y1 = 0;
			
			if( $width < $height ){//portrait
				$x1 = 0;
				$y1 = floor(($height-$width)/2);
			}else{//landscape
				$y1 = 0;
				$x1 = floor(($width-$height)/2);
			}
			return array("x1" => $x1, "y1" => $y1);
		}
		function rotateImage($imageCopy,$angle,$pathFilename){
			$imagetype = $this->getImageType($pathFilename);//getimagesize($pathFilename);
			
			ini_set("memory_limit",$this->getMemoryForImage($pathFilename)."M");
			
			$image_p = imagerotate($imageCopy,$angle*-1,0);
			
			chmod($pathFilename,0777);
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagejpeg($image_p, $pathFilename, 90);
						 	break;
					case self::GIF	:					
							imagegif($image_p, $pathFilename);
						   	break;
					case self::PNG	:						
							imagepng($image_p, $pathFilename);
						  	break;
					default:
							imagejpeg($image_p, $pathFilename, 90);
							break;		
			}
			imagedestroy($image_p);
		}
		
		
		function rotatePhoto(PathManager $pm, $context_obj, $photo_obj, $angle=0){
			$relPath = $pm->GetPathrelative();
			$filename = $photo_obj->getFileName();
			
			//original
			$orig_file = $relPath."orig-".$filename;
			if( file_exists($orig_file) ){
				$this->resizeOriginal($orig_file,self::ORIGINAL_SIZE);
				$orig_copy = $this->getImageCopy($orig_file);
				$this->rotateImage($orig_copy,$angle,$orig_file);
				imagedestroy($orig_copy);
			}
			
			//fullsize
			$fullsize_file = $relPath."fullsize".$filename;
			$fullsize_copy = $this->getImageCopy($fullsize_file);
			$this->rotateImage($fullsize_copy,$angle,$fullsize_file);
			imagedestroy($fullsize_copy);
			
			//standard
			$standard_file = $relPath."standard".$filename;
			$standard_copy = $this->getImageCopy($standard_file);
			$this->rotateImage($standard_copy,$angle,$standard_file);
			imagedestroy($standard_copy);
			
			//featured
			$context_class = get_class($context_obj);
			if( "PhotoAlbum" == $context_class || "TravelLog" == $context_class ){
				$featured_file = $relPath."featured".$filename;
				$featured_copy = $this->getImageCopy($featured_file);
				$this->rotateImage($featured_copy,$angle,$featured_file);
				imagedestroy($featured_copy);
				
				if( "TravelLog" == $context_class ){
					$this->mRelPath = $relPath;
					$this->mFilename = $filename;
					unset($this->mImageInfo[$orig_file]);
					$this->mAttrib = $this->getDimension($orig_file);
					$this->mPathFilename = $this->mRelPath."orig-".$this->mFilename;
					$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
					if( file_exists($this->mRelPath."jentryheader".$this->mFilename) ){
						unlink($this->mRelPath."jentryheader".$this->mFilename);
					}
					$this->createEntryHeader();
					
					if( $this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
						$this->createCustomPageHeaderFullsize();
						$this->createCustomPageHeaderStandard();
						$context_obj->setQualifiedCustomHeaderPhoto($photo_obj->getPhotoID(),TRUE);
					}else{
						if(file_exists($this->mRelPath."customheaderstandard".$this->mFilename)){
							unlink($this->mRelPath."customheaderstandard".$this->mFilename);
						}
						if(file_exists($this->mRelPath."customheaderfullsize".$this->mFilename)){
							unlink($this->mRelPath."customheaderfullsize".$this->mFilename);
						}
						$context_obj->setQualifiedCustomHeaderPhoto($photo_obj->getPhotoID(),FALSE);
					}
					imagedestroy($this->mImageCopy);
				}
			}
			
			//thumbnail
			$thumbnail_file = $relPath."thumbnail".$filename;
			$thumbnail_copy = $this->getImageCopy($thumbnail_file);
			$this->rotateImage($thumbnail_copy,$angle,$thumbnail_file);
			imagedestroy($thumbnail_copy);
			
			if( 90 == $angle ){
				$photo_obj->saveImageProcess("RR");//rotate right
			}else if( 270 == $angle ){
				$photo_obj->saveImageProcess("RL");//rotate left
			}
		}
		
		function setNewThumbnail($pm,$mPhotoContext,$mPhoto,$x,$y,$width,$height){
			$relPath = $pm->GetPathrelative();
			$filename = $mPhoto->getFileName();
			
			$fullsize_file = $relPath."fullsize".$filename;
			$fullsize_copy = $this->getImageCopy($fullsize_file);
			
			$attrib = array("width"=>imagesx($fullsize_copy),"height"=>imagesy($fullsize_copy));
			
			$cropped_copy =  $this->makeCroppedImage($fullsize_file,$fullsize_copy,$attrib,$x,$y,$width,$height);
			
			$thumbnail_file = $relPath."thumbnail".$filename;
			$standard_file = $relPath."standard".$filename;
			if( $mPhotoContext instanceof TravelerProfile ){
				$this->makeResizedCopy($fullsize_file,$cropped_copy,array("width"=>$width,"height"=>$height),self::STANDARD_PROFILE,$standard_file,TRUE);
			}else{
				$this->makeResizedCopy($fullsize_file,$cropped_copy,array("width"=>$width,"height"=>$height),self::STANDARD,$standard_file,TRUE);
			}
			$this->makeResizedCopy($fullsize_file,$cropped_copy,array("width"=>$width,"height"=>$height),self::THUMBNAIL,$thumbnail_file,TRUE);
			
			imagedestroy($fullsize_copy);
			imagedestroy($cropped_copy);
			
			$mPhoto->saveImageProcess("TC");//thumbnail crop
		}
		
		function getMemoryForImage( $filename ){
		    $imageInfo = $this->getImageType($filename);
			$requiredMemoryMB = 35;
			if( $imageInfo ){
				$channels = 4;
				if( isset($imageInfo['channels']) ){
					$channels = $imageInfo['channels'];
				}
				$requiredMemoryMB = round(( $imageInfo[0] * $imageInfo[1] * ($imageInfo['bits'] / 8) * $channels * 2.5 ) / 1024);
			}
		    return $requiredMemoryMB;
		}
		
		function createCustomPageHeaderFullsize(){
			$newPathFilename = $this->mRelPath."customheaderfullsize".$this->mFilename;
			$this->makeResizedCopy($this->mPathFilename,$this->mImageCopy,$this->mAttrib,self::HEADER_MIN_WIDTH,$newPathFilename);
		}
		
		function createCustomPageHeaderStandard(){
			$newPathFilename = $this->mRelPath."customheaderstandard".$this->mFilename;
			$tempImage = $this->getImageCopy($this->mRelPath."customheaderfullsize".$this->mFilename);
			$width = imagesx($tempImage);
			$height = imagesy($tempImage);
			if( self::HEADER_MIN_HEIGHT < $height ){//step 4-a
				$cropY = floor($height / 2) - floor(self::HEADER_MIN_HEIGHT/2);
				$tempImage = $this->makeCroppedImage($this->mPathFilename,$tempImage,array("width"=>$width,"height"=>$height),0,$cropY,$width,self::HEADER_MIN_HEIGHT);
			}
			//save $tempImage
			$this->createImage($this->mPathFilename,$tempImage,$newPathFilename);
			imagedestroy($tempImage);
		}
		
		function cropCustomPageHeader($pm,$mPhoto,$x,$y){
			$width = self::HEADER_MIN_WIDTH;
			$height = self::HEADER_MIN_HEIGHT;
			
			$relPath = $pm->GetPathrelative();
			$filename = $mPhoto->getFileName();
			
			$fullsize_file = $relPath."customheaderfullsize".$filename;
			$fullsize_copy = $this->getImageCopy($fullsize_file);
			
			$attrib = array("width"=>imagesx($fullsize_copy),"height"=>imagesy($fullsize_copy));
			
			$cropped_copy =  $this->makeCroppedImage($fullsize_file,$fullsize_copy,$attrib,$x,$y,$width,$height);
			
			$newFilename = "";
			$imageInfo = $this->mImageInfo[$fullsize_file];
			switch(substr($imageInfo['mime'],6,4)){
				case "jpeg"	:	$newFilename = uniqid().time().".jpg";
								break;
				case "gif"	:	$newFilename = uniqid().time().".gif";
								break;
				case "png"	:	$newFilename = uniqid().time().".png";
								break;
				case "pjpeg":	$newFilename = uniqid().time().".jpg";
								break;
				default		:	$newFilename = uniqid().time().".jpg";
								break;
			}
			
			$orig_file = $relPath."orig-".$filename;
			$newOrigFilename = $relPath."orig-".$newFilename;
			$orig_copy = $this->getImageCopy($orig_file);
			$this->createImage($orig_file,$orig_copy,$newOrigFilename);
			imagedestroy($orig_copy);
			unlink($orig_file);
			
			$standard_file = $relPath."customheaderstandard".$filename;
			$newStandardFilename = $relPath."customheaderstandard".$newFilename;
			$this->createImage($fullsize_file,$cropped_copy,$newStandardFilename);
			imagedestroy($cropped_copy);
			unlink($standard_file);
			
			$newFullsizeFilename = $relPath."customheaderfullsize".$newFilename;
			$this->createImage($fullsize_file,$fullsize_copy,$newFullsizeFilename);
			imagedestroy($fullsize_copy);
			unlink($fullsize_file);
			
			$mPhoto->saveImageProcess("CC",$newFilename);//thumbnail crop
		}
		
		function _create_cobrandcustomsize_script($pm,$photo,$travellog){//to create page header sized of previously uploaded travellog photos
			
			if( !($photo instanceof Photo || $travellog instanceof TravelLog) ){
				return FALSE;
			}
			
			$this->mRelPath = $pm->GetPathrelative();
			$this->mFilename = $photo->getFilename();
			$this->mPathFilename = $this->mRelPath."orig-".$this->mFilename;
						
			if( !file_exists($this->mPathFilename) ){
				return FALSE;
			}
			
			$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			
			$origDim = $this->getDimension($this->mPathFilename);
			if( !$origDim ){
				return FALSE;
			}
			
			$this->mAttrib["width"] = $origDim["width"];
			$this->mAttrib["height"] = $origDim["height"];
			
			if( $this->isValidCustomHeaderDimension($this->mAttrib["width"],$this->mAttrib["height"]) ){
				$this->createCustomPageHeaderFullsize();
				$this->createCustomPageHeaderStandard();
				return TRUE;
			}
			
			return FALSE;
		}
		function isValidCustomHeaderDimension($width=0,$height=0){
			//reduce dimension first to customheader dimension
			if( 1 > $width || 1 > $height ){
				return FALSE;
			}
			$rescaledDim = $this->getRescaledDimension($width,$height,self::HEADER_MIN_WIDTH);
			if( self::HEADER_MIN_WIDTH > $rescaledDim["width"] ){
				return FALSE;
			}
			if( self::HEADER_MIN_HEIGHT > $rescaledDim["height"] ){
				return FALSE;
			}
			return TRUE;
		}
		
		function resizeOriginal($origPathFilename="",$origSize=1024){
			ini_set("memory_limit",$this->getMemoryForImage($origPathFilename)."M");
			
			$dimension = $this->getDimension($origPathFilename);
			if( !$dimension ){
				return FALSE;
			}
			
			$oldWidth = $dimension["width"];
			$oldHeight = $dimension["height"];
			
			if( $oldWidth <= $origSize && $oldHeight <= $origSize ){
				return TRUE;
			}
			
			$newDimension = $this->getRescaledDimension($oldWidth,$oldHeight,$origSize);
			$newWidth = $newDimension["width"];
			$newHeight = $newDimension["height"];
			
			$imageCopy = $this->getImageCopy($origPathFilename);
			if( !$imageCopy ){
				return FALSE;
			}
			
			$imagetype = $this->getImageType($origPathFilename);//getimagesize($oldPathFilename);
			if( !$imagetype ){
				return FALSE;
			}
			
			$image_p = @imagecreatetruecolor($newWidth, $newHeight);
			
			switch (substr($imagetype['mime'],6,4)) {
					case self::JPEG	:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagejpeg($image_p, $origPathFilename, 90);
						 	break;
					case self::GIF	:					
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      			
							imagegif($image_p, $origPathFilename);
						   	break;
					case self::PNG	:						
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagepng($image_p, $origPathFilename);
						  	break;
					default:
							imagecopyresampled($image_p, $imageCopy, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);      	
							imagejpeg($image_p, $origPathFilename, 90);
							break;		
			}
			imagedestroy($imageCopy);
			imagedestroy($image_p);
			unset($this->mImageInfo[$origPathFilename]);
			return TRUE;
		}
		
		function fixGroupLogoThumbnail($pm,$photo){
			if( !($photo instanceof Photo) ){
				throw new exception("UploadedPhoto::fixGroupLogoThumbnail(pm,photo): photo must be an instance of Photo.");
			}
			if( !($pm instanceof PathManager) ){
				throw new exception("UploadedPhoto::fixGroupLogoThumbnail(pm,photo): pm must be an instance of PathManager.");
			}
			
			$this->mRelPath = $pm->GetPathrelative();
			$this->mFilename = $photo->getFilename();
			$this->mPathFilename = $this->mRelPath."standard".$this->mFilename;
						
			if( !file_exists($this->mPathFilename) ){
				throw new exception("Image ".$this->mPathFilename." does not exist.");
			}
			
			$this->mImageCopy = $this->getImageCopy($this->mPathFilename);
			
			$origDim = $this->getDimension($this->mPathFilename);
			if( !$origDim ){
				throw new exception("Unable to read image ".$this->mPathFilename);
			}
			
			$this->mAttrib["width"] = $origDim["width"];
			$this->mAttrib["height"] = $origDim["height"];
			
			try{
				$this->createThumbnail();
				$thumbname = $this->mRelPath."thumbnail".$this->mFilename;
				return '<p><a href="'.$photo->getPhotoLink('standard').'" target="_blank"><img src="'.$photo->getPhotoLink('thumbnail').'" /></a></p>';
			}catch(exception $e){
				throw $e;
			}
		}
	}