<?php

	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");

	class PhotoAlbumCreator extends RequestData{
		
		private $mName = "Untitled";
		
		function setAlbumName($name="Untitled"){
			$this->mName = $name;
		}
		
		function getAlbumName(){
			return $this->mName;
		}
		
		function createGroupAlbum(){
			require_once("travellog/model/Class.PhotoAlbum.php");
			$album = new PhotoAlbum();
			$album->setGroupID($this->getID());
			$album->setTitle($this->getAlbumName());
			$album->setCreator($this->getLoggedID());
			$album->Create();
			
			require_once("travellog/model/Class.ActivityFeed.php");
			try{
				ActivityFeed::create($album);
			}catch(exception $e){}
			
			header("location: collection.php?type=".$this->getType()."&ID=".$this->getID()."&context=photoalbum&genID=".$album->getPhotoAlbumID()."&action=3");
		}
		
		function createTravelerAlbum(){
			require_once("travellog/model/Class.PhotoAlbum.php");
			$album = new PhotoAlbum();
			$album->setGroupID(0);
			$album->setTitle($this->getAlbumName());
			$album->setCreator($this->getLoggedID());
			$album->Create();
			
			require_once("travellog/model/Class.ActivityFeed.php");
			try{
				ActivityFeed::create($album);
			}catch(exception $e){}
			
			header("location: collection.php?type=".$this->getType()."&ID=".$this->getID()."&context=traveleralbum&genID=".$album->getPhotoAlbumID()."&action=3");
		}
		
		function create(){
			switch( $this->getType() ){
				case self::TRAVELER_TYPE	:	$this->createTravelerAlbum();
												break;
				case self::GROUP_TYPE		:	$this->createGroupAlbum();
												break;
			}
		}
	}