<?php

	require_once("travellog/model/Class.PathManager.php");
	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");
	require_once("travellog/UIComponent/Collection/model/Class.UploadedPhoto.php");
	
	class PhotoCreator extends RequestData{
		
		private $mUploadMode	=	self::CLASSIC;
		private $mFiles			=	array();
		private $mData		=	array(	"FileCount"	=>	0,
		 								"tlID"		=>	0	);
		
		private $mLargeFiles		=	array();
		private $mValidFiles		=	array();
		
		function setUploadMode($mode=0){
			$this->mUploadMode = $mode;
		}
		function setFiles($files=array()){
			$this->mFiles = $files;
		}
		function setData($data=array()){
			$this->mData = array_merge($this->mData,$data);
			if( self::CLASSIC == $this->mUploadMode ){
				$this->mData["FileCount"] = 5;
			}
		}
		function processFiles(){
			$context = NULL;
			switch($this->getContext()){
				case self::PROFILE_CONTEXT		:	require_once("travellog/model/Class.Traveler.php");
													$context = new Traveler($this->getGenID());
													$context = $context->getTravelerProfile();
													break;
				case self::ALBUM_CONTEXT		:	require_once("travellog/model/Class.PhotoAlbum.php");
													$context = new PhotoAlbum($this->getGenID());
													break;
				case self::TRAVALBUM_CONTEXT	:	require_once("travellog/model/Class.PhotoAlbum.php");
													$context = new PhotoAlbum($this->getGenID());
													break;
				case self::TRAVELLOG_CONTEXT	:	require_once("travellog/model/Class.TravelLog.php");
													$context = new TravelLog($this->getGenID());
													break;
				case self::JOURNAL_CONTEXT		:	require_once("travellog/model/Class.TravelLog.php");
													$context = new TravelLog($this->mData["tlID"]);
													break;
				case self::ARTICLE_CONTEXT		:	require_once("travellog/model/Class.Article.php");
													$context = new Article($this->getGenID());
													break;									
			}
			$pathManager = new PathManager($context);
			$rel = $pathManager->GetPathrelative();
			if( !is_dir($rel) ){
				$pathManager->CreatePath();
			}
			$x= 1;
			foreach($this->mFiles as $file){//var_dump($this->mFiles);exit;
				if( "" != $file["name"] && 0 == $file["error"] && 2048000 >= $file["size"] ){
					$upPhoto = new UploadedPhoto();
					$upPhoto->setContext((self::JOURNAL_CONTEXT==$this->getContext())? self::TRAVELLOG_CONTEXT : $this->getContext());
					$upPhoto->setGenID((self::JOURNAL_CONTEXT==$this->getContext())? $this->mData["tlID"] : $this->getGenID());
					$upPhoto->setFile($file);
					if( isset($this->mData["Description_".$x]) ){
						$upPhoto->setCaption($this->mData["Description_".$x]);
					}
					if( isset($this->mData["Angle_".$x]) ){
						$upPhoto->setRotationAngle($this->mData["Angle_".$x]);
					}
					$upPhoto->processFile($pathManager,$context);
					$this->mValidFiles[] = $file["name"];
				}
				if( "" != $file["name"] && 2048000 < $file["size"]){
					$this->mLargeFiles[] = $file["name"];
				}
				$x++;
			}
			$this->checkUploadError();
		}
		
		function checkUploadError(){
			$_SESSION["UploadError"] = FALSE;
			if( 0 < count($this->mLargeFiles) ){
				$_SESSION["UploadError"] = TRUE;
				$_SESSION["LargeFiles"] = $this->mLargeFiles;
			}
			$_SESSION["ValidFiles"] = $this->mValidFiles;
		}
	}