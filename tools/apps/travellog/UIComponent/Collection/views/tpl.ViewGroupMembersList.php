<ul class="memberlist">
	<?	$ctr = 0;?>
	<?	foreach($members as $member): 
			$photocount = $member->countPhotosToGrabByGroup($groupID);
			$profile_link = 'http://'.$_SERVER["SERVER_NAME"].'/'.$member->getTravelerProfile()->getUsername();
	?>
			<li <?if( 0 == ($ctr % 3) ):?>style="clear: left;"<?endif;?> >
				<span class="member">
					<?	if(0<$photocount): ?>
						<a href="javascript:void(0)" onclick="collectionControl.viewMemberPhotos(<?=$member->getTravelerID()?>); return false;">
							<img width="65" height="65" src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" title="View Member Photos"/>
						</a>
					<?	else: ?>
						<a href="<?=$profile_link?>" >
							<img width="65" height="65" src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" title="Go to Profile"/>
						</a>
					<?	endif; ?>
				</span>
				<h3>
					<?	if(0<$photocount): ?>
						<a href="javascript:void(0)" onclick="collectionControl.viewMemberPhotos(<?=$member->getTravelerID()?>); return false;"><?=$member->getFullName()?></a>
					<?	else: ?>
						<?=$member->getFullName()?>
					<?	endif;?>
				</h3>
				<p><?=(0==$photocount)? "" : ((1<$photocount)? $photocount." Photos" : $photocount." Photo")  ?></p>
			</li>
	<?	$ctr++; ?>
	<?	endforeach; ?>
	</ul>
<? if ( $paging->getTotalPages() > 1 ):?>
	<? $start = $paging->getStartRow() + 1; 
	   $end = $paging->getEndRow(); 
	?>
	<? $paging->showPagination() ?>
<? endif; ?>
