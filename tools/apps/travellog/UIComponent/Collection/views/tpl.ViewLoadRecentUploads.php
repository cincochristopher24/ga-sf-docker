<div id="recentUploads">
	<div class="feds">
		<h2>
			<span>Most recent photo updates</span>
		</h2>
		<div class="notification">
			Loading recent photo upload updates... <img src="/images/load_gray.gif" />
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		var _timer_ = setTimeout(function(){
      	  	collectionControl.loadRecentUploads();
      	  clearTimeout(_timer_);
        },500);
		//collectionControl.loadRecentUploads();
	});
</script>