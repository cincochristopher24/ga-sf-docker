<?	if(!$hasError): ?>
<div class="photocrop">
	<div class="preview">
		<div>
			<p><strong>Current</strong></p>
			<div>
				<img src="<?=$Thumbnail?>" align="left" border="0" />
			</div>
		</div>
		<div>
			<p><strong>Preview</strong></p>
			<!--div style="width: 65px; height: 65px; overflow: hidden;">
				<img id="preview" src="<?//=$Fullsize?>"/>	
			</div-->
			<div id="preview">
			</div>
		</div>
		<div>
			<p id="cw"></p>
			<p id="ch"></p>
			<p id="cx"></p>
			<p id="cy"></p>
		</div>
		<div id="loading_status" style="display:none;" >
			<img src="/images/load_gray.gif" />
		</div>
	</div>
	<div class="cropview">
		<!--img id="cropbox" src="<?=$Fullsize?>" style="background-color: black; opacity: 0;"/-->
		<img id="cropbox" src="<?=$Fullsize?>" width="<?=$fullWidth?>" height="<?=$fullHeight?>"/> 
	</div>
	<div class="photoaction">
		<span id="crop_control">
			<input type="button" value="Cancel" class="button_v3" onclick="collectionControl.closeCropper(); return false;" />
			<input type="button" value="Save Thumbnail" class="button_v3" onclick="collectionControl.saveThumbnail(); return false;" />
		</span>
		<p id="cropNotice"></p>
	</div>
	<form id="frmCropper">
		<input type="hidden" id="x" name="x" />
		<input type="hidden" id="y" name="y" />
		<input type="hidden" id="width" name="width" />
		<input type="hidden" id="height" name="height" />
	</form>
</div>
<!--script type="text/javascript" src="js/cropper/lib/builder.js" language="javascript"> </script>
<script type="text/javascript" src="js/cropper/lib/dragdrop.js" language="javascript"> </script>
<script type="text/javascript" src="js/cropper/cropper.js" language="javascript"> </script-->
<script type="text/javascript">
	jQuery(document).ready(function(){
		collectionControl.setWidth(<?=$fullWidth?>);
		collectionControl.setHeight(<?=$fullHeight?>);
		collectionControl.initCropper();
	});
</script>
<?	else: ?>
	<script type="text/javascript">
		collectionControl.closeCropper();
	</script>
<?	endif; ?>