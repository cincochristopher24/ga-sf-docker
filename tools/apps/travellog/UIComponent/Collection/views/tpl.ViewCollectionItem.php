<?php
	$context = $collectionItem->getContext();
	$title = htmlspecialchars(stripslashes(strip_tags($collectionItem->getTitle())),ENT_QUOTES);
	$title = 10 < strlen($title) ? substr($title,0,10)."..." : $title;
?>
<li id="collection_item_<?=$context?>_<?=$collectionItem->getGenID()?>" name="collection_item" <? if($isActive):?>class="active"<?endif;?>>
	<div style="height: 77px;">
		<span class="album_name">
			<?	if($isActive): ?>
				<img id="imgPrimary_<?=$collectionItem->getGenID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($collectionItem->getTitle())),ENT_QUOTES)?>" width="65" height="65" src="<?=$primary->getPhotoLink('thumbnail')?>"/>
			<?	else: ?>
				<a href="<?=$href?>">
					<img id="imgPrimary_<?=$collectionItem->getGenID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($collectionItem->getTitle())),ENT_QUOTES)?>" width="65" height="65" src="<?=$primary->getPhotoLink('thumbnail')?>"/>
				</a>
			<?	endif; ?>
		</span>
		<h3>
			<strong id="album_name_<?=$context?>_<?=$collectionItem->getGenID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($collectionItem->getTitle())),ENT_QUOTES)?>" >
				<?	if($isActive): ?>
						<?=$title?>
				<?	else: ?>
					<a id="album_title_<?=$context?>_<?=$collectionItem->getGenID()?>" href="<?=$href?>" >
							<?=$title?>
					</a>
				<?	endif; ?>
			</strong>
		
			<?	if( 0 < $userLevel && ("traveleralbum"==$context || "photoalbum"==$context) ): ?>
				<span id="albumNameForm_<?=$context?>_<?=$collectionItem->getGenID()?>" style="display:none;">
					<form id="frmAlbumName<?=$context?>_<?=$collectionItem->getGenID()?>" onsubmit="collectionControl.saveAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" >
						<input type="text" id="txtAlbumName_<?=$context?>_<?=$collectionItem->getGenID()?>" size="12" value="<?=htmlspecialchars(stripslashes($collectionItem->getTitle()),ENT_QUOTES)?>" />
						<input type="button" id="btnSaveAlbumName" value="Save" onclick="collectionControl.saveAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" />
						<input type="button" id="btnCancelAlbumName" value="Cancel" onclick="collectionControl.cancelEditAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" />
						<input type="hidden" id="curAlbumName_<?=$context?>_<?=$collectionItem->getGenID()?>" value="<?=htmlspecialchars(stripslashes($collectionItem->getTitle()),ENT_QUOTES)?>" />
					</form>
				</span>
			<?	endif; ?>
		</h3>
		<span id="album_info_<?=$context?>_<?=$collectionItem->getGenID()?>" class="album_control">
					<?	if( 0 < $userLevel && ("photoalbum" == $context || "traveleralbum" == $context) ): ?>
							<a href="javascript:void(0)" onclick="collectionControl.editAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;">Edit Title</a>&nbsp;
							<?	if( 0 == $collectionItem->getNumPhotos() ): ?>
								<a href="javascript:void(0)" onclick="collectionControl.deleteAlbum(<?=$collectionItem->getGenID()?>,'<?=$context?>'); return false;">Delete Album</a>
							<?	elseif( 0 < $collectionItem->getNumPhotos() ): ?>
								<a href="javascript:void(0)" onclick="collectionControl.deleteNonEmptyAlbum(<?=$collectionItem->getGenID()?>,'<?=$context?>',<?=$collectionItem->getNumPhotos()?>); return false;" >Delete Album</a>
							<?	endif; ?>
					<?	endif; ?>
					<?	if( "journal"==$context ): 
						require_once("travellog/model/Class.Travel.php");
						$travel = new Travel($collectionItem->getGenID());
						$travellogID = $travel->getMostRecentTravellog(TRUE);
						$link = "/journal-entry.php?action=view&travellogID=".$travellogID."&travelerID=".$travel->getTravelerID();
					?>
						<a href="<?=$link?>">Go to Journal</a>
					<?	elseif ( "article" == $context ): ?>
						<a href="/article.php?action=view&articleID=<?=$collectionItem->getGenID()?>">Go to Article</a>
					<?endif;?>
		</span>
	
		<p style="width:100px;overflow:hidden;">
			<? $photocount = $collectionItem->getNumPhotos(); ?>
			<?=(0 < $photocount)? ((1<$photocount)? $photocount." Photos" : $photocount." Photo") : '' ?>
		</p>
	</div>
</li>