<?php
	require_once 'Class.Template.php';

	class ViewCollectionItem extends RequestData{
		
		private $mCollectionItem	=	NULL;
		
		function setCollectionItem($item=NULL){
			$this->mCollectionItem = $item;
		}

		function render(){
			$isActive = ($this->getGenID() == $this->mCollectionItem->getGenID())? TRUE : FALSE;
			$tpl = new Template();
			$tpl->set("collectionItem",$this->mCollectionItem);
			$tpl->set("isActive",$isActive);
			$tpl->set("userLevel",$this->getUserLevel());
			$tpl->set("href","/collection.php?type=".$this->getType()."&ID=".$this->getID()."&context=".$this->mCollectionItem->getContext()."&genID=".$this->mCollectionItem->getGenID());
			if( "journal" == $this->mCollectionItem->getContext() ){
				require_once("travellog/model/Class.Travel.php");
				require_once("travellog/model/Class.TravelLog.php");
				require_once("travellog/model/Class.Photo.php");
				$travel = new Travel($this->mCollectionItem->getGenID());
				$photoID = $travel->getPrimaryphoto();
				if( 0 == $photoID ){
					$photoID = $travel->getRandomPhoto()->getPhotoID();
					if( 0 < $photoID && self::isPrivileged() ){
						$travel->setPrimaryphoto($photoID);
						$travel->Update();
					}
				}
				$travellog = TravelLog::getTravelLogContext($photoID);
				$tpl->set("primary",new Photo($travellog,$photoID));
				$this->setContext($this->mCollectionItem->getContext());
			}else{
				$tpl->set("primary",$this->mCollectionItem->getPrimaryPhoto());
			}
			if( "journal"==$this->getContext() && isset($_GET["context"]) ){
				$tpl->set("photoContext",$this->getPhotoContext());
			}
			$tpl->out("travellog/UIComponent/Collection/views/tpl.ViewCollectionItem.php");
		}
	}