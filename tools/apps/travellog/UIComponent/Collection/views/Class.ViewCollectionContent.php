<?php
	require_once("Class.Template.php");
	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");
	
	class ViewCollectionContent extends RequestData{
		
		function render(){
			$collectionPhotos = $this->getGaCollectionDTO();
			if( 0 == $this->getUserLevel() ){//not logged or not the owner
				require_once("travellog/UIComponent/Collection/views/Class.ViewOldAlbum.php");
				require_once('Cache/ganetCacheProvider.php');
			
				$content = new ViewOldAlbum();
				if( is_null($this->getGenID()) ){
					$content->setCollectionItem($this->getTopCollection());
				}else{
					$content->setCollectionItem($this->getCollection($this->getGenID()));
				}
				ob_start();
				$content->render();
				$content = ob_get_contents();
				ob_end_flush();
				
			}else{//logged user but is owner, staff or admin
				if(is_null($this->getGenID())){
					
					$content = new Template();
					$content->set("type",$this->getType());
					$content->set("ID",$this->getID());
					$content->set("context",$this->getContext());
					$content->set("genID",$this->getGenID());
					
					$photocount = 0;
					foreach($collectionPhotos as $collection){
						$photocount += $collection->getNumPhotos();
					}
					$content->set("collectionCount",count($collectionPhotos));
					$content->set("photocount",$photocount);
					$content->set("viewLocation",self::OUTSIDE_COLLECTION);
					$content->out("travellog/UIComponent/Collection/views/tpl.CollectionSummaryHeader.php");
					
					$content = new Template();
					$content->out("travellog/UIComponent/Collection/views/tpl.FormCreateAlbum.php");
					if( self::GROUP_TYPE == $this->getType() ){
						$content = new Template();
						$content->out("travellog/UIComponent/Collection/views/tpl.ViewLoadManageFeaturedAlbums.php");
					}
				}else{
					$this->renderContents();
				}
			}
		}
		
			
		function renderContents(){
			$photoContext = $this->getPhotoContext();
			if( 0 == $photoContext->getCOUNTPhotos() ){
				$content = new Template();
				$content->set("type",$this->getType());
				$content->set("ID",$this->getID());
				$content->set("context",$this->getContext());
				$content->set("genID",$this->getGenID());
				if( "journal" == $this->getContext() ){
					$content->set("title",$this->getCollection($this->getGenID())->getTitle());
					require_once("travellog/model/Class.Travel.php");
					$journal = new Travel($this->getGenID());
					
					if( "traveler" == $this->getType() ){
						$travellogs = $journal->getTravelerTravelLogs();
					}else{
						$travellogs = $journal->getTravelLogsByTravelID();
					}
					
					if( 1 == $journal->getEntryCount() ){
						$travellog = $travellogs[0];
						header("location: /collection.php?type=".$this->getType()."&ID=".$this->getID()."&context=travellog&genID=".$travellog->getTravelLogID()."&action=3");
					}
					
					$content->set("travellogs",$travellogs);
				}else{
					header("location: /collection.php?type=".$this->getType()."&ID=".$this->getID()."&context=".$this->getContext()."&genID=".$this->getGenID()."&action=3");
					//$content->set("title",$photoContext->getHeaderCaption());
				}
				//$content->set("title",$photoContext->getHeaderCaption());
				$content->set("photocount",count($photoContext->getCOUNTPhotos()));
				$content->set("viewLocation",self::INSIDE_COLLECTION);
				$content->set("context",$this->getContext());
				$content->set("userLevel",$this->getUserLevel());
				$content->out("travellog/UIComponent/Collection/views/tpl.CollectionSummaryHeader.php");
			}else{
				$content = new Template();
				$content->set("userLevel",$this->getUserLevel());
				$content->set("type",$this->getType());
				$content->set("ID",$this->getID());
				$content->set("context",$this->getContext());
				$content->set("genID",$this->getGenID());
				if( "journal" == $this->getContext() ){
					$content->set("title",$this->getCollection($this->getGenID())->getTitle());
				}else{
					$content->set("title",$photoContext->getHeaderCaption());
				}
				$content->set("photoContext",$this->getPhotoContext()->getPHOTOContext());
				if( self::MANAGE_PHOTOS == $this->getAction() ){
					$content->set("hasUploadError",isset($_SESSION["UploadError"])?  $_SESSION["UploadError"] : FALSE );
					if( isset($_SESSION["UploadError"]) && $_SESSION["UploadError"] ){
						$message = "<p>There were errors encountered during the upload process.</p>";
						if( isset($_SESSION["LargeFiles"]) ){
							$count = count($_SESSION["LargeFiles"]);
							$label = (1<$count)? "files were" : "file was";
							$lv = (1<$count)? "were" : "was";
							$message .= "<p>  - The following $label too large: <strong>".implode(", ",$_SESSION["LargeFiles"])."</strong> hence $lv not included for saving.<br />   - Please be informed that we have set the <strong>maximum file size</strong> for each image at <strong>2MB</strong>.</p>";
							unset($_SESSION["LargeFiles"]);
						}
						$content->set("errorMessage",$message);
						unset($_SESSION["UploadError"]);
						unset($_SESSION["ValidFiles"]);
					}
					$content->set("photos",$photoContext->getPHOTOContext()->getPhotos());
					$content->out("travellog/UIComponent/Collection/views/tpl.ViewManagePhotos.php");
				}else{
					if( "journal" == $this->getContext() ){
						require_once("travellog/model/Class.Travel.php");
						$journal = new Travel($this->getGenID());
						if( "traveler" == $this->getType() ){
							$travellogs = $journal->getTravelerTravelLogs();
						}else{
							$travellogs = $journal->getTravelLogsByTravelID();
						}
						$journalPrimary = $journal->getPrimaryphoto();
						$photos = array();
						$entry_photos = array();
						$entryPrimaries = array();
						$tlog_temp = array();
						$tlog_photo_count = array();
						$travellogIDs = array();
						foreach($travellogs as $tlog){
							$temp = $tlog->getPhotos();
							if( is_array($temp) ){
								$entryPrimaries[] = $tlog->getPrimaryPhoto()->getPhotoID();
								$photos = array_merge($photos,$temp);
								$entry_photos[] = $temp;
								$tlog_temp[] = $tlog;
								$tlog_photo_count[] = count($temp);
							}else{
								$entryPrimaries[] = 0;
								$entry_photos[] = array();
								$tlog_temp[] = $tlog;
								$tlog_photo_count[] = 0;
							}
							$travellogIDs[] = $tlog->getTravelLogID();
						}
						$content->set("contextPrimary",$journal->getPrimaryphoto());
						$content->set("photos",$photos);
						$content->set("travellogs",$tlog_temp);
						$content->set("entry_photos",$entry_photos);
						$content->set("entryPrimaries",$entryPrimaries);
						$content->set("tlog_photo_count",$tlog_photo_count);
						$content->set("travellogIDs",$travellogIDs);
					}else{
						$content->set("contextPrimary",$photoContext->getPHOTOContext()->getPrimaryPhoto()->getPhotoID());
						$photos = $photoContext->getPHOTOContext()->getPhotos();
						$content->set("photos",$photos);
						$content->set("entryPrimaries",array());
						$content->set("travellogIDs",array());
						$content->set("tlog_photo_count",array(count($photos)));
						
						$journalPrimary = 0;
						if( "travellog" == $this->getContext() ){
							require_once("travellog/model/Class.Travel.php");
							require_once("travellog/model/Class.TravelLog.php");
							$travellog = new TravelLog($this->getGenID());
							$travel = new Travel($travellog->getTravelID());
							$journalPrimary = $travel->getPrimaryphoto();
						}
					}
					$content->set("journalPrimary",$journalPrimary);
					$content->set("isPrivileged",(0<$this->getUserLevel())? TRUE : FALSE);
					$content->set("group",$this->getGroup());
					$content->set("traveler",$this->getTraveler());
					$content->out("travellog/UIComponent/Collection/views/tpl.ViewPhotoList.php");
				}
			}
		}
	}