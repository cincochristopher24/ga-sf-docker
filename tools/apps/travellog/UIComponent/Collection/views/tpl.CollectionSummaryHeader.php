<?	if( 0 == $viewLocation ): ?>
	<div class="notification">
		<?	if( 0 == $collectionCount ): ?>  
			<h2>You have not yet uploaded photos.</h2>
			<p>
				To get started, you can add a photo album below.
			</p>
			<p>
				<?	if( "traveler" == $type ): ?>
				To make your photos appear on a journal entry, you first need to <a href="/journal.php?action=add">write a journal</a> and add your photos to a journal entry.
				<?	else: ?>
				To make your photos appear on a journal entry, you first need to <a href="/journal.php?action=add&amp;contextGroupID=<?=$ID?>">write a journal</a> and add your photos to a journal entry.
				<?	endif; ?>
			</p>
		<?	else: ?>
			<h1>
				<?	if( 0 < $photocount ): ?>
					You have <?=$collectionCount?> Photo Album<?if(1<$collectionCount):?>s<?endif;?> with <?=$photocount?> Picture<?if(1<$photocount):?>s<?endif;?> all in all.
				<?	else: ?>
					You have <?=$collectionCount?> Photo Album<?if(1<$collectionCount):?>s<?endif;?> without pictures.
				<?	endif; ?>
			</h1>
			<p>
				To add a photo to an existing album, select the album on the right.
			</p>
			<p>
				To add a new album, just type in the name of your album and click <strong>Add New Album</strong>.
			<p>
		<?	endif; ?>
	</div>
<?	else: ?>
	<h1>
		<span id="travelTitle"><?=$title?></span>
	</h1>
	<div class="notification middle">
		<?	if("profile"==$context): ?>
			<h2>Start adding profile photos</h2>
		<?	else: ?>
			<h2>Start adding photos into this <?if("journal"==$context):?>journal<?elseif("travellog"==$context):?>journal entry<?endif;?> album</h2>
		<?	endif;?>
		<p>
			<?	if( "photoalbum"==$context && 1 < $userLevel ): ?>
				You can upload the photos on your desktop or you might want to link photo from your members.
			<?	else: ?>
				<?	if("journal" == $context ): ?>
				<br />Select a journal entry 
					<select id="cboTravelLogs">
					<? foreach($travellogs as $tlog): ?>
						<option value="<?=$tlog->getTravelLogID()?>"><?=truncateText(htmlspecialchars(stripslashes($tlog->getTitle()),ENT_QUOTES),40)?></option>
					<?	endforeach; ?>
					</select>
					<br />
				<?	endif; ?>
				Click on the button below to activate the upload utility.
			<?	endif; ?>
		</p>
		<form action="">
			<input class="button_v3" type="button" value="Add Photos" onclick="collectionControl.loadUploader(); return false;"/>
			<?	if( "photoalbum"==$context && 1 < $userLevel ): ?>
				<input class="button_v3" type="button" value="Add Member Photos" onclick="window.location.replace('/collection.php?type=<?=$type?>&ID=<?=$ID?>&context=<?=$context?>&genID=<?=$genID?>&action=6');"/>
			<?	endif; ?>
		</form>
	</div>
<?	endif; ?>

<?php
	function truncateText($text,$max){
		if( $max < strlen($text) ){
			return substr($text,0,$max)."...";
		}
		return $text;
	}
?>