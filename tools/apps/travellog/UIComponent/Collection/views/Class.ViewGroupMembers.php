<?php

	require_once("Class.Template.php");
	require_once('Class.Paging.php');
	require_once('Class.ObjectIterator.php');
	require_once('travellog/model/Class.AdminGroup.php');
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterOp.php');
	require_once('travellog/model/Class.FilterCriteria2.php');
	require_once('travellog/model/Class.RowsLimit.php');
	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");
	
	class ViewGroupMembers extends RequestData{
		
		private $mRpp	=	15;
		private $mPage	=	1;
		
		private $mGroupMembers	=	array();
		private $mGroupMembersCount	=	0;
		
		private $mMember	=	NULL;
		
		function setPage($page=1){
			$this->mPage = $page;
		}
		
		function setMember($member=NULL){
			$this->mMember = $member;
		}
		
		function render(){
			$this->searchMembers();

			$tpl = new Template();
			$tpl->set("controller",$this);
			$tpl->set("title",$this->getPhotoContext()->getHeaderCaption());
			$tpl->out("travellog/UIComponent/Collection/views/tpl.ViewGroupMembers.php");
		}
		
		function renderMemberPhotos(){
			
			require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			$photoContext = new PhotoFactory($this->getGenID(),$this->getContext());
			$pContext = $photoContext->create();
			
			$tpl = new Template();
			$tpl->set("member",$this->mMember);
			$tpl->set("photos",$this->mMember->getPhotosToGrabByGroup($this->getGroup()->getGroupID()));
			$tpl->set("title",$pContext->getHeaderCaption());
			$tpl->set("existing",$this->getPhotoContext()->getPHOTOContext()->getPhotoIDs());
			$tpl->out("travellog/UIComponent/Collection/views/tpl.ViewMemberPhotoList.php");
		}
		
		function renderFetchedMembers(){
			$this->searchMembers();
			$this->renderGroupMembersList();
		}
		
		function renderGroupMembersList(){
			$paging = new Paging( $this->mGroupMembersCount, $this->mPage, 'grpID=' . $this->getGroup()->getGroupID(), $this->mRpp );
			$paging->setOnclick("collectionControl.fetchMembers");
			$iterator = new ObjectIterator( $this->mGroupMembers, $paging );
			
			$tpl = new Template();
			$tpl->set("groupID",$this->getGroup()->getGroupID());
			$tpl->set("members",$this->mGroupMembers);
			$tpl->set("paging",$paging);
			$tpl->set("iterator",$iterator);
			$tpl->out("travellog/UIComponent/Collection/views/tpl.ViewGroupMembersList.php");
		}
		
		function renderMemberUpdatesNotifier(){
			/*$loggedTraveler = new Traveler($this->getLoggedID());
			require_once('travellog/model/updates/Class.MemberRecentPhotos.php');
			$photoupdates = new MemberRecentPhotos($this->getGroup());
			$feeds = $photoupdates->prepareUpdates(
				array(
					'LOGGED_TRAVELER'		=> $loggedTraveler,
					'SORT_ORDER'			=> "DESC",
					'VIEWER_TYPE'			=> 6,
					'MAX_UPDATES'			=> 3
			));
			
			$tpl = new Template();			
			$tpl->set("member",$this->mMember);
			$tpl->set("updates",$feeds);
			$tpl->out("travellog/UIComponent/Collection/views/tpl.ViewMemberUpdatesNotifier.php");*/
							
			require_once('travellog/model/Class.ActivityFeed.php');
			$rowsLimit = new RowsLimit(5,0);
			$params = array("FEED_SECTION"		=>	ActivityFeed::ENTRYPHOTO,
							"GROUP_OBJECT"		=>	$this->getGroup(),
							"ROWS_LIMIT"		=>	$rowsLimit );
			$feeds = ActivityFeed::getRecentMemberUpdates($params);
			$tpl = new Template();			
			$tpl->set("updates",$feeds["activityFeeds"]);
			$tpl->out("travellog/UIComponent/Collection/views/tpl.ViewMemberUpdatesNotifier.php");
		}
		
		function searchMembers(){
			$orderBy = FilterOp::$ORDER_BY;
			$cond = new Condition;
			$cond->setAttributeName("tblTraveler.firstname");
			$cond->setOperation($orderBy);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			//$rowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			//$sp_array = $this->getGroup()->getGroupMembers($rowsLimit,NULL,$order);
			$sp_array = $this->getGroup()->getGroupMembers(NULL,NULL,$order);
			//$this->mGroupMembers = $sp_array[0];
			//$this->mGroupMembersCount = $sp_array[1];
			
			$membersWithPhotos = $this->filterMembersWithPhotos($sp_array[0],$this->getGroup()->getGroupID());
			$this->mGroupMembers = $this->limitMembers($this->filterMembersWithPhotos($sp_array[0],$this->getGroup()->getGroupID()),$this->mPage,$this->mRpp);
			$this->mGroupMembersCount = count($membersWithPhotos);
		}
		
		function filterMembersWithPhotos($members=array(),$groupID=0){
			$temp = array();
			foreach($members as $member){
				$photocount = $member->countPhotosToGrabByGroup($groupID);
				if( 0 < $photocount ){
					$temp[] = $member;
				}
			}
			return $temp;
		}
		
		function limitMembers($members=array(), $page=1, $rpp=15){
			$temp = array();
			$count = count($members);
			$offset = $this->calcOffset($page);
			for($i=$offset, $ctr=1; $i<$count && $ctr <= $rpp; $i++,$ctr++ ){
				$temp[] = $members[$i];
			}
			return $temp;
		}
		
		private function calcOffset($page=1){
			return ($page-1) * $this->mRpp;
		}
	}