<?	if( $hasError ): ?>
	<?	if( 0 == $mode ): ?>
		<script type="text/javascript">
			collectionControl.cancelRearrange();
		</script>
	<?	else: ?>
		<?	echo "ERROR"; exit; ?>
	<?	endif; ?>
	
<?	else: ?>

<?	if( 0 == $mode ): //Non-IE browsers ?>

	<table class="table_popbox">
		<tbody>
			<tr>
				<td class="popbox_topLeft"></td>
				<td class="popbox_border"></td>
				<td class="popbox_topRight"></td>
			</tr>
			<tr>
				<td class="popbox_border"></td>
				<td class="popbox_content">
					<h4 class="header" id="popup_header"><strong><?=$title?></strong></h4>
					<div class="confirm" id="popup_confirm">
						<div id = "loading_image" style = "display:none">
							<span> <center> <img src = "/images/loading.gif"/> </center> </span>
						</div>
						<div class = "formcont" id="formcont">
							<ul id="imageList">
								<?	foreach($photos as $photo): ?>
									<li id="photoID_<?=$photo->getPhotoID()?>" class="imageItem">
										<img src="<?=$photo->getPhotoLink('thumbnail')?>" class="imageHandle"/>
									</li>
								<?	endforeach; ?>
							</ul>
						</div>
					</div>	
					<div class="buttons_box" id="popup_buttons">
						<input type="button" class="prompt_button" value="Save Changes" onclick="collectionControl.saveRearrangedPhotos(); return false;" />
						<input type="button" class="prompt_button" value="Cancel" onclick="collectionControl.cancelRearrangePhotos(); return false;" />
						<span id="loading_status" style="display:none;" >
							<img src="http://<?=$_SERVER['SERVER_NAME']?>/images/load_gray.gif" />
						</span>
						<p id="rearrangeNotice">
						</p>
					</div>
				</td>
				<td class="popbox_border"></td>
			</tr>
			<tr>
				<td class="popbox_bottomLeft"></td>
				<td class="popbox_border"></td>
				<td class="popbox_bottomRight"></td>
			</tr>
		</tbody>
	</table>
	
<?	else: // IE browsers ?>

	<div id="photo_main_view" class="section">
		<h1>
			<span id="travelTitle"> <?=$title?> </span>
		</h1>
		<div id="thumbnail_view">
			<ul id="imageList">
				<?	foreach($photos as $photo): ?>
					<li id="photoID_<?=$photo->getPhotoID()?>" class="imageItem">
						<img src="<?=$photo->getPhotoLink('thumbnail')?>" class="imageHandle"/>
					</li>
				<?	endforeach; ?>
			</ul>
		<div class="photoaction" style="clear:both;text-align:right;">

			<span id="rearrange_control">
				<input type="button" class="button_v3" value="Save Changes" onclick="collectionControl.saveRearrangedPhotos(); return false;" />
				<input type="button" class="button_v3" value="Cancel" onclick="collectionControl.cancelRearrangePhotos(); return false;" />
			</span>
			<span id="loading_status" style="display:none;" >
				<img src="http://<?=$_SERVER['SERVER_NAME']?>/images/load_gray.gif" />
			</span>
			<p id="rearrangeNotice">
			</p>
		</div>

<?	endif; ?>

	<?	if( 0 == $mode ): ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				collectionControl.initRearrangePhotos();
			});
		</script>
	<?	endif; ?>
<?	endif; ?>