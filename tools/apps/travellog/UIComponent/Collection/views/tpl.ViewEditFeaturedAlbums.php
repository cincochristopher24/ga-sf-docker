<?	if( $hasError ): ?>
	
	<script type="text/javascript">
		collectionControl.cancelEditFeaturedAlbums();
	</script>
	
<?	else: ?>

	<table class="table_popbox">
		<tbody>
			<tr>
				<td class="popbox_topLeft"></td>
				<td class="popbox_border"></td>
				<td class="popbox_topRight"></td>
			</tr>
			<tr>
				<td class="popbox_border"></td>
				<td class="popbox_content">
					<h4 class="header" id="popup_header"><strong>Edit Featured Photo Albums</strong></h4>
					<div class="confirm" id="popup_confirm">
						<div id = "loading_image" style = "display:none">
							<span> <center> <img src = "/images/loading.gif"/> </center> </span>
						</div>
						
						<div class="content">
							<div class="photo_collections">
								<ul>
								<?	foreach($albums as $album): 
										$photoCount = $album->getCountPhotos();
										$photoCountLabel = 1 < $photoCount ? $photoCount." Photos" : $photoCount." Photo";
										$checked = $album->isFeatured() ? 'checked="true"' : "";
								?>
									<li>
										<span class="album_name">
											<img width="60" height="60" src="<?=$album->getPrimaryPhoto()->getPhotoLink('thumbnail')?>"/>
										</span>
										<h3>
											<strong><?=htmlspecialchars(stripslashes(strip_tags($album->getTitle())),ENT_QUOTES)?></strong>
										</h3>
										<span class="album_control">
											<?	if( 0 == $photoCount ): ?>
												This album is empty. You can't feature an empty album.
										</span>
											<?	else: ?>
												<?=$photoCountLabel?>
										</span>
										<p>
													<input type="checkbox" id="albumToFeature_<?=$album->getPhotoAlbumID()?>" name="albumToFeature" value="<?=$album->getPhotoAlbumID()?>" <?=$checked?> />
													Feature this album
										</p>
											<?	endif; ?>
									</li>
								<?	endforeach; ?>
								</ul>
							</div>
						</div>
						
					</div>	
					<div class="buttons_box" id="popup_buttons">
						<input type="button" class="prompt_button" value="Save Changes" onclick="collectionControl.saveFeaturedAlbums(); return false;" />
						<input type="button" class="prompt_button" value="Cancel" onclick="collectionControl.cancelEditFeaturedAlbums(); return false;" />
						<span id="loading_status" style="display:none;" >
							<img src="/images/load_gray.gif" />
						</span>
						<p id="editFeaturedAlbumNotice">
						</p>
					</div>
				</td>
				<td class="popbox_border"></td>
			</tr>
			<tr>
				<td class="popbox_bottomLeft"></td>
				<td class="popbox_border"></td>
				<td class="popbox_bottomRight"></td>
			</tr>
		</tbody>
	</table>

<?	endif; ?>