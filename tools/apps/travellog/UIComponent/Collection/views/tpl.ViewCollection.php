<h2 style="margin-bottom:10px;border:none;">
	<span>Collections</span>
	<?	if( 0 < $userLevel ): ?>
		<span name="lnkCreateAlbum" id="lnkCreateAlbum" class="header_actions" <? if(is_null($genID)): ?>style="display: none;"<? endif;?> >	
			<a href="/collection.php?type=<?=$type?>&ID=<?=$ID?>">+Add New Album</a>
		</span>
	<?	endif; ?>
</h2>
<div class="content">
	
	<?/*
	<ul id="collections" class="photo_collections">
		<?	if( 0 < count($collectionDTO) ): 
			require_once("travellog/UIComponent/Collection/views/Class.ViewCollectionItem.php");
			foreach($collectionDTO as $collection){//$collection here is an instance of GaCollectionDTO
				$item = new ViewCollectionItem();
				$item->setCollectionItem($collection);
				$item->render();
			}
			else: ?>
			<li class="active">
				<h3>Photo Collection is Empty</h3>
			</li>
		<?	endif; ?>
	</ul>
	*/?>
	
	<?	if( 0 < count($collectionDTO) ): ?>
	
		<?	
			$itemsPerStrip = 5;
			$stripCount = ceil(count($collectionDTO)/$itemsPerStrip); 
			$stripsVisible = 1;
			if( count($collectionDTO)<$itemsPerStrip ){
				$sliderHeight = (count($collectionDTO)*92)+(count($collectionDTO)*2)-1;
			}else{
				$sliderHeight = ($itemsPerStrip*92)+($itemsPerStrip*2)-1;
			}
		?>
		<div class="mediaviewer">

		<?	if( $stripsVisible < $stripCount ): ?>
			<div id="btnPrevStrip" class="topbuttons"><a href="" class="upmore" onclick="paginatedCollectionList.scrollToPrevStrip(); return false;"><span>up</span></a></div>
		<?	endif; ?>
		
			<div id="slider" class="box" style="height: <?=$sliderHeight?>px;">
				<div id="slider_content" class="slider_content" style="top: 0px; opacity: 1;">
				<?	
					$defaultIndex = 0;
					$strips = array();
					$curStripCount = 0;
					$hasDefaultItem = is_null($genID) && is_null($context) ? FALSE : TRUE;
				
					require_once("travellog/UIComponent/Collection/views/Class.ViewCollectionItem.php");
					foreach($collectionDTO as $x=> $collection): 
						$strips[] = $collection;
						if( $hasDefaultItem && $collection->getGenID() == $genID && $collection->getContext() == $context ){
							$defaultIndex = $x;
						}
				?>
					<?	if( 0 == (($x+1) % $itemsPerStrip) ): ?>
						<ul id="slider_section-<?=++$curStripCount?>" <? if($stripCount < $stripsVisible): ?>upper<?endif;?> style="height: <?=ceil(count($strips))*92+(ceil(count($strips))*2)+2?>px;">
							<?	foreach($strips as $y=> $collectionItem): ?>
								<?php
									$item = new ViewCollectionItem();
									$item->setCollectionItem($collectionItem);
									$item->render();
								?>
							<?	endforeach; ?>
						</ul>
					<?	$strips = array(); 
						endif; ?>
				<?	endforeach; ?>
				<?	if( 0 < count($strips) ): ?>
					<ul id="slider_section-<?=++$curStripCount?>" style="height: <?=ceil(count($strips))*92+(ceil(count($strips))*2)+2?>px;">
						<?	foreach($strips as $y=>$collectionItem): ?>
							<?php
								$item = new ViewCollectionItem();
								$item->setCollectionItem($collectionItem);
								$item->render();
							?>
						<?	endforeach; ?>
					</ul>
				<?	endif; ?>
				</div>
			</div>
		
		<?	if( $stripsVisible < $stripCount ): ?>
			<div id="btnNextStrip" class="bottombuttons"><a href="" class="downmore" onclick="paginatedCollectionList.scrollToNextStrip(); return false;"><span>down</span></a></div>
		<?	endif; ?>

		</div>
	
		<?	$startSection = floor($defaultIndex / $itemsPerStrip) + 1; ?>

		<script type="text/javascript">
			jQuery(document).ready(function(){
				paginatedCollectionList.setSlider(contentSlider,"slider_section",<?=$startSection?>,<?=$itemsPerStrip?>,<?=$stripCount?>);
			});
		</script>
	
	<?	else: ?>
		<ul id="collections" class="photo_collections">
			<li class="active">
				<h3>Photo Collection is Empty</h3>
			</li>
		</ul>
	<?	endif; ?>
	
	
	
	<?/*	if( !is_null($genID) && 0 < $userLevel && 5 < count($collectionDTO) ): ?>
	<div class="section_foot">
		<span name="lnkCreateAlbum" id="lnkCreateAlbum2" class="footer_actions" <? if(is_null($genID)): ?>style="display: none;"<? endif;?> >	
			<a href="/collection.php?type=<?=$type?>&ID=<?=$ID?>">+Add New Album</a>
		</span>
	</div>
	<?	endif; */?>	
</div>