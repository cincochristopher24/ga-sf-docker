<? 	if( !$hasPhotos ): ?>
	<div class="section" id="photo_main_view">
		<div class="content">
			<div class="notification">
				<h2>There are no photos in this collection yet.</h2>
				<p>Please check out later for uploaded photos.</p>
			</div>	
		</div>
	</div>
	<?	return; ?>
<? 	endif; ?>
<div class="section" id="photo_main_view">
	<h1>
		<span id="albumheader">
			<span><?=isset($title)? stripslashes($title) : ''?></span>
			<?if( isset($context) && "journal"==$context):?>
				<span class="header_actions"><a href="<?=$backlink?>">Go To Journal</a></span>
			<?endif; ?>
		</span>
	</h1>
	<div class="content">
		<div id="gallerycontainer">
			<?php
			$pos = 0;
			$startPos = 0;
			$defaultPos = 0;
			$photoLinks = "";
			$photoIDs = "";
			$currPhotoID = 0;
			$photoPrimaries = "";
			$isTravelPrimaries = "";
			$temp = 0;
			$photocount = count($Photos);
			$currentPhoto = $Photos[0];
			$travelprimaryphoto = 0;
			$current_travelprimaryphoto = 0;


			if($context == "travellog"){
				require_once('travellog/model/Class.TravelLog.php');   
				require_once('travellog/model/Class.Travel.php');  

				$travellog 	= new TravelLog($genID);
				$travel 	= new Travel($travellog->getTravelID());

				if($photocount){
					if($travel->getPrimaryphoto() != 0 && $travel->getPrimaryphoto() == $current_full_photo->getPhotoid()){
						$travelprimaryphoto = $travel->getPrimaryphoto();
					}
				}
			}
							
			?>
			<div id="photogallery_<?=$context?>">

				<?	if($photocount > 1):?>
					<div class="photocontrol">
				          <!--div class="photoborderup"></div-->
				    	<div class="photoimagebox">
				    		<div class="photoimagecontainer">
								<?	if( $photocount > 6): ?>
									<div id="carousel_prev" class="carousel-prev">
										<img id="BA_prev-arrow" class="left-button-image" src="/images/prev6_active.jpg" alt="Previous Button" />
									</div>
								<? endif;?>

								<div id="BA_mycarousel_<?=$context?>" class="carousel-area">		
									<ul class="carousel-list">
										<?	foreach($Photos as $x => $photo):
												$is_travelprimaryphoto = $photo->getPhotoId()==$current_travelprimaryphoto?1:0;
												$photoLinks .= $photo->getPhotolink('fullsize');
												$photoIDs .= $photo->getPhotoID();
												$photoPrimaries .= $photo->getPrimaryPhoto();
												$isTravelPrimaries .= $is_travelprimaryphoto;
												if( $temp < count($Photos)-1 ){
													$photoLinks .= "$";
													$photoIDs .= "$";
													$photoPrimaries .= "$";
													$isTravelPrimaries .= "$";
												}
												$temp++;
										?>
											<li id="BA_mycarousel_<?=$context?>-item-<?= $x+1; ?>" height="65" width="65">
												<a href="javascript:void(0)" id="thumb<?=$photo->getPhotoID();?>" onclick="PhotoService.viewfullsize('fullsize',1,<?=$photo->getPhotoID();?>,<?= $photo->getPrimaryPhoto(); ?>,<?= $is_travelprimaryphoto;?>);">
													<div id="previewWrap<?=$photo->getPhotoID();?>" > 
														<img height="65" width="65" src="<?=$photo->getPhotolink('thumbnail');?>" border="1"/>
													</div>
												</a>
											</li>
										<?endforeach;?>
										<? $startPos = ((int)($defaultPos / 6)) * 6;?>	
									</ul>
								</div>


								<? if( $photocount > 6): ?>
									<div id="carousel_next" class="carousel-next">
										<img id="BA_next-arrow" class="right-button-image" src="/images/next6_active.jpg" alt="Next Button" />
									</div>
								<? endif; ?>
							</div>

						</div>
				          
					</div>
				<?endif;?>

					<!-- This element id is Required-->
				<div id="fullview">

					<div class="clear"></div>

					<?if($photocount < 2):?>
						<div class="tabarea">
					<?endif;?>

							<!-- Required Container-->
					<div class="photocontainer">
					 	<div id="bigthumb">
					 		<div id="fullsize">
								<div id="image_container" class="image_container">
								<?	if( 1 < $photocount ): ?>
									<div id="navhover" class="navhover">
											<a id="prev_photo" class="prev_photo<?=(0==$defaultPos)? ' p_disabled' : ''?>" href="javascript:void(0)" onclick="PhotoService.scrollToPrevPhoto(); return false;"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>
											<a id="next_photo" class="next_photo<?=(($photocount-1)==$defaultPos)? ' p_disabled' : ''?>" href="javascript:void(0)" onclick="PhotoService.scrollToNextPhoto(); return false;"><span class="txtblock">Next<span class="imgblock"></span></span></span></a>
									</div>		
								<?	endif; ?>
									<img id="cropImage" src="<?=$currentPhoto->getPhotolink('fullsize');?>"/>
								</div>
							</div>
						</div>
					</div>

					<!-- Cache element Container -->  
					<div id="cache_fullview"></div>
					<!-- End Cache element Container -->  

					<?if($photocount < 2):?>
							</div>
					<?endif;?>
				</div>
				<div id="controlcontainer">					
					<div class="phototitle">
					  	<p><em><span id="caption_area"><?=$currentPhoto->getCaption();?></span></em></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Wide Column -->
	<!-- Begin Narrow Column -->
</div>

<script type="text/javascript">
	if( 'undefined' == typeof(PhotoService) ){
		jQuery.noConflict();
		PhotoService = new mPhotoService("<?=$context?>",0,<?=$genID?>);
		PhotoService.setLayout(1);
	}
				
	PhotoService.pCount = <?=$photocount?>;
	PhotoService.curPos = <?=$defaultPos?>;
	PhotoService.setPhotoArray('<?=$photoLinks?>','<?=$photoIDs?>','<?=$photoPrimaries?>','<?=$isTravelPrimaries?>');
	PhotoService.curStrip = <?=floor($startPos/6)+1?>;
						
	jQuery(document).ready(function() {

		jQuery(".carousel-area").show();
										
	    jQuery(".carousel-area").jCarouselLite({
	        btnNext: ".carousel-next",
	        btnPrev: ".carousel-prev",
			circular: false,
			visible: 6,
			start: <?=$startPos?>,
			scroll: 6
	    });	
		PhotoService.resetNavOnPhoto();
		if( <?=$startPos?> < 6 ){
			jQuery("#carousel_prev").addClass("carousel-disabled");
		}
		if( PhotoService.curStrip == Math.floor(<?=$photocount?>/6)+1 ){
			jQuery("#carousel_next").addClass("carousel-disabled");
		}
	
		jQuery("#cropImage").bind("load",
			function(){
				if( null != jQuery("#navhover") ){
					PhotoService.resetNavOnPhoto();
				}
			}
		);
	});
</script>