<? $profile_link = 'http://'.$_SERVER["SERVER_NAME"].'/'.$member->getTravelerProfile()->getUsername(); ?>
<h3>
	<span id="travelTitle"><strong>Adding Member Photos to</strong> <?=$title?> </span>
	<div class="photoaction_top" id="add_photo_control" style="display:none;">
		<img id="add_photo_requesting" src="/images/loading_small.gif" style="display:none;" />
		<input class="button_v3" type="button" value="Add this photo" onclick="collectionControl.addMemberPhoto(); return false;"/>
	</div>
</h3>
<div class="viewer content">
	<div class="member_control">
		<a class="addmem_option" href="javascript:void(0)" onclick="collectionControl.toAlbumView(); return false;" ><strong>Back to Album</strong>&nbsp;</a>
		<a class="addmem_option" href="javascript:void(0)" onclick="window.location.reload();">&nbsp;<strong>Back to Member List</strong></a>
		<div class="author">
			<div class="link">
				<strong><?=$member->getFullName()?></strong>
				<span id="lnkGalleryView">	
					<a href="javascript:void(0)" onclick="collectionControl.loadMemberPhotoGalleryView(); return false;">Switch to Gallery View</a>
				</span>
				<span id="lnkThumbnailView" style="display:none;">	
					<a href="javascript:void(0)" onclick="collectionControl.switchToThumbnailView(); return false;">Switch to Thumbnail View</a>
				</span>
			</div>
			<a href="<?=$profile_link?>" >
				<img width="37" height="37" src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" title="Go to Profile" alt="" class="pic"/>
			</a>
						
		</div>
	</div>
	<div id="thumbnail_view">
		<ul class="photo_list">
			
			<?	
				$photocount = count($photos);
				
				$photolinks = array(); 
				$thumblinks = array();
				$ids = array();
				$notices = array();
				$tlogs = array();
				
				$captions = array();
				//$dateUploaded = array();
				$journalLinks = array();
				$journalTitles = array();
				
				for($i=0; $i<$photocount; $i++):
					$photo = $photos[$i];
					
					$photocontext = $photo->getContext();
					if( !is_object($photocontext) ){
						$photocount--;
						continue;
					}
					$thumblink = $photo->getPhotoLink('thumbnail');
					
					$ids[] = $photo->getPhotoID();
					$photolinks[] = $photo->getPhotoLink('fullsize');
					$thumblinks[] = $thumblink;
					$notices[] = (in_array($photo->getPhotoID(),$existing)) ? "1" : "0";
					$tlogs[] = "0";
					
					$captions[] = htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES);
					//$dateUploaded[] = $photo->getDateuploaded();
					if( 2 == $photo->getPhototypeID() ){
						$journalLinks[] = $photo->getContext()->getFriendlyURL();
						$journalTitles[] = htmlspecialchars(stripslashes(strip_tags($photo->getContext()->getTitle())),ENT_QUOTES);
					}else{
						$journalLinks[] = "";
						$journalTitles[] = "";
					}
			?>
			
			<li>
				<a href="javascript:void(0)" onclick="collectionControl.loadMemberPhotoGalleryView(<?=$photo->getPhotoID()?>); return false;">
					<img width="65" height="65" src="<?=$thumblink?>" title="<?=$captions[$i]?>"/>
				</a>
			</li>
			
			<?	endfor; 
				$photolinks = implode(",",$photolinks); 
				$thumblinks = implode(",",$thumblinks);
				$ids = implode(",",$ids);
				$notices = implode(",",$notices);
				
				$uniqueLogs = implode(",",array_unique($tlogs));
				$tlogs = implode(",",$tlogs);
			?>
		</ul>
	</div>
	<div id="gallery_view" style="display:none;">
		<div id="photo_grab_notice" style="display:none;">
		</div>
		<div class="viewed_image">
			<?	if( 1 < count($photos) ): ?>
				<div class="viewed_navi">
					<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
					<p name="photo_position_marker" class="jLeft"></p>
					<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
				</div>
			<?	endif; ?>
			<div class="viewed_holder">
				<div class="navhover" id="navhover">
						<a class="prev_photo" id="prev_photo" onclick="collectionControl.movePrev(); return false;" href="javascript:void(0)"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>
						<a class="next_photo" id="next_photo" onclick="collectionControl.moveNext(); return false;" href="javascript:void(0)"><span class="txtblock">Next<span class="imgblock"></span></span></a>
				</div>
				<img id="active_photo" src="" align="middle" border="0">
			</div>
			<?	if( 1 < count($photos) ): ?>
				<div class="viewed_navi">
					<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
					<p name="photo_position_marker" class="jLeft"></p>
					<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
				</div>
			<?	endif; ?>
		</div>
		<p id="caption">						
		</p>
		<div class="comment_info">
			<img class="book_open" id="book_icon" alt="journal" src="/images/book_open.gif"/>
			<a class="journal_title" id="journal_link" title="" href="">
				<strong><span id="context_title"></span></strong>
			</a>
			<span id="date_uploaded"></span>
		</div>
	</div>								
</div>

<div id="collection_items_info" style="display: none;">
	<?	for($i=0; $i<count($captions); $i++): 
			$caption = $captions[$i];
			$link = $journalLinks[$i];
			//$uploaded = $dateUploaded[$i];
			$jtitle = $journalTitles[$i];
	?>
		<p id="caption_<?=$i?>"><?=$caption?></p>
		<p id="link_<?=$i?>"><?=$link?></p>
		<p id="jtitle_<?=$i?>"><?=$jtitle?></p>
		<?/*p id="uploaded_<?=$i?>"><?=$uploaded?></p?*/?>
	<?	endfor; ?>
</div>

<script type="text/javascript">
	collectionControl.initCollection(<?=$photocount?>,"<?=$ids?>","<?=$photolinks?>","<?=$thumblinks?>","<?=$notices?>","<?=$tlogs?>","<?=$uniqueLogs?>",1);

	jQuery("#active_photo").bind("load",
		function(){
			if( null != jQuery("#navhover") ){
				collectionControl.resetNavOnPhoto();
			}
		}
	);
	jQuery("#album_menu").hide();
</script>