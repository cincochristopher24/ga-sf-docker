<?php

	require_once("Class.Template.php");
	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");
	
	class ViewRecentUploads extends RequestData{
		
		private $mPhotoUpdates = null;
		private $mDeletedPhotoUpdates = null;
		
		function getRecentUploads(){
			$loggedTraveler = new Traveler($this->getLoggedID());
			require_once('travellog/model/updates/Class.GroupRecentPhotos.php');
			$photoupdates = new GroupRecentPhotos($this->getGroup());
			$feeds = $photoupdates->prepareUpdates(
				array(
					'LOGGED_TRAVELER'	=> $loggedTraveler,
					'SORT_ORDER'		=> "DESC",
					'VIEWER_TYPE'		=> 6,
					'MAX_UPDATES'		=> 3,
					'GET_PHOTO_ALBUM' 	=> true
			));
			
			$tmpFeeds = array();			
			foreach($feeds as $feed){
				$minifeeds = $photoupdates->prepareUpdates(
					array(
						'LOGGED_TRAVELER'	=> $loggedTraveler,
						'SORT_ORDER'		=> "DESC",
						'VIEWER_TYPE'		=> 6,
						'ALBUM_ID'			=> $feed->getPhotoAlbum()->getPhotoAlbumID(),
						'EXEC_DATE'			=> $feed->getExecutionDate()
				));
				
				$tmpFeeds['DETAILS'] = $minifeeds;
				$tmpFeeds['ALBUM']   = $feed;
				$this->mPhotoUpdates[] = $tmpFeeds;
			}
			//$this->mPhotoUpdates = $feeds;
		}
		
		function getDeletedPhotoUpdates(){
			$loggedTraveler = new Traveler($this->getLoggedID());
			require_once('travellog/model/updates/Class.MemberDeletedPhotos.php');
			$photoupdates = new MemberDeletedPhotos($this->getGroup());
			$feeds = $photoupdates->prepareUpdates(
				array(
					'LOGGED_TRAVELER'	=> $loggedTraveler,
					'SORT_ORDER'		=> "DESC",
					'VIEWER_TYPE'		=> 6,
					'MAX_UPDATES'		=> 5,
					'GET_PHOTO_ALBUM'	=> true 
			));
			
			$indUpdates = (count($feeds) > 2) ? 3 : 5;
			$minidelfeeds = array();
			
			foreach($feeds as $feed){
				$minifeeds = $photoupdates->prepareUpdates(
					array(
						'LOGGED_TRAVELER'	=> $loggedTraveler,
						'SORT_ORDER'		=> "DESC",
						'VIEWER_TYPE'		=> 6,
						'MAX_UPDATES'		=> $indUpdates,
						'ALBUM_ID'			=> $feed->getPhotoAlbum()->getPhotoAlbumID()
				));
				$minidelfeeds['DETAILS'] = $minifeeds;
				$minidelfeeds['ALBUM']   = $feed;
				$this->mDeletedPhotoUpdates[] = $minidelfeeds;
			}
		}
		
		function render(){
			//$this->getRecentUploads();
			//$this->getDeletedPhotoUpdates();
			
			/*$content = new Template();
			//$content->set("collectionDTO",$this->getGaCollectionDTO());
			$content->set("collectionDTO",$this->mPhotoUpdates);
			//$content->set("deleteUpdates",$this->mDeletedPhotoUpdates);
			$content->set("deleteUpdates",array());
			$content->out("travellog/UIComponent/Collection/views/tpl.ViewRecentUploads.php");*/
		}
	}