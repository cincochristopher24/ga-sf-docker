<h3>
	<span id="travelTitle"><strong>Adding Member Photos to</strong> <?=$title?></span>
	<div class="photoaction_top">
		<input class="button_v3" type="button" value="Back to Album" onclick="collectionControl.toAlbumView(); return false;"/>
	</div>
</h3>
<div class="photocontainer">
	<?=$controller->renderMemberUpdatesNotifier()?>
	<div id="members_list">
		<?=$controller->renderGroupMembersList()?>
	</div>
</div>