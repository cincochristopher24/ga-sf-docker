<?php
	$fullLink = $defaultPhoto->getPhotoLink('fullsize');
	list($width, $height, $type, $attr) = @getimagesize($fullLink);
	$default_width = $width + 250;
?>
<div class="close">
	<a href="javascript:void(0)" onclick="collectionPopup.closeGallery();"><span>close</span></a>
</div>
<div id="thickboxs" class="thickboxs" style="display;none;">

	<div class="container">
		<?	if( 1 < count($photos) ): ?>
			<div class="viewed_navi">
				<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
				<p name="photo_position_marker" class="jLeft"></p>
				<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
			</div>
		<?	endif; ?>
		
		<table class="clear">
			<tr>
				<td>
					<div class="viewed_holder" style="text-align: center;">
						<? if(1<count($photos)): ?>
						<div id="navhover" class="navhover" style="display:none;" align="center">							
							<a href="javascript:void(0)" onclick="collectionPopup.scrollToPrevPhoto(); return false;" id="prev_photo" class="prev_photo"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>							
							<a href="javascript:void(0)" onclick="collectionPopup.scrollToNextPhoto(); return false;" id="next_photo" class="next_photo"><span class="txtblock">Next<span class="imgblock"></span></span></a>									
						</div>
						<?	endif;?>
						<img id="loading_photo" src="/images/load_gray.gif" align="center" />	
						<img id="active_photo" src="" align="center" border="0" class="mainimage"/>
					</div>
				</td>
			</tr>
		</table>
		
		<?	if( 1 < count($photos) ): ?>
			<div class="viewed_navi">
				<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
				<p name="photo_position_marker" class="jLeft"></p>
				<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
			</div>
		<?	endif; ?>			
	</div>

	<?	
		$photosPerStrip = 15;
		$stripCount = ceil(count($photos)/$photosPerStrip); 
		$stripsVisible = 1;
	?>
	
	<div class="journalviewer" style="float: right; width:203px; overflow: hidden;">
		
	<?	if( $stripsVisible < $stripCount ): ?>
		<div id="btnPrevStrip" class="topbuttons"><a href="" class="upmore" onclick="collectionPopup.scrollToPrevStrip(); return false;"><span>up</span></a></div>
	<?	endif; ?>
	
		<div id="slider" style="position:relative; overflow:hidden; height: <?if(count($photos)<$photosPerStrip): echo (ceil(count($photos)/3)*65)+(ceil(count($photos)/3)*2)+2; else: echo (ceil($photosPerStrip/3)*65)+(ceil($photosPerStrip/3)*2)+2;endif;?>px;">
			<div id="slider_content" class="slider_content" style="top: 0px; opacity: 1;">
			<?	
				$photoIDs = array();
				$captions = array();
				$links = array();
				$defaultIndex = 0;
				$strips = array();
				$curStripCount = 0;
				$journalLinks = array();
				$journalTitles = array();
				//$dateUploaded = array();
				$temp_title = $title;//htmlspecialchars(stripslashes(strip_tags($title)),ENT_QUOTES);
				foreach($photos as $x=> $photo): 
					$photoIDs[] = $photo->getPhotoID();
					$caption = htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES);
					$captions[] = $caption;
					$link = $photo->getPhotoLink('fullsize');
					$links[] = $link;
					if( $defaultPhoto->getPhotoID() == $photo->getPhotoID() ){
						$defaultIndex = $x;
					}
					$strips[] = $photo;
					
					//$dateUploaded[] = $photo->getDateuploaded();
					if( 2 == $photo->getPhototypeID() ){
						$journalLinks[] = $photo->getContext()->getFriendlyURL();
						$journalTitles[] = $photo->getContext()->getTitle();
					}else{
						$journalLinks[] = "";
						$journalTitles[] = $temp_title;
					}
			?>
				<?	if( 0 == (($x+1) % $photosPerStrip) ): ?>
					<div id="slider_section-<?=++$curStripCount?>" class="slider_section <? if($stripCount < $stripsVisible): ?>upper<?endif;?>" style="height: <?=(ceil(count($strips)/3)*65)+(ceil(count($strips)/3)*2)+2?>px;">
						<?	foreach($strips as $y=> $item): ?>
							<a onclick="collectionPopup.setActivePhoto(<?=$item->getPhotoID()?>); return false;" href="javascript:void(0)">
								<img src="<?=$item->getPhotoLink('thumbnail')?>" width="65" height="65" border="0" align="left" style="margin-left:1px; margin-right:1px; margin-bottom:1px; margin-top:1px;" />
							</a>
						<?	endforeach; ?>
					</div>
				<?	$strips = array(); 
					endif; ?>
			<?	endforeach; ?>
			<?	if( 0 < count($strips) ): ?>
				<div id="slider_section-<?=++$curStripCount?>" class="slider_section" style="height: <?=(ceil(count($strips)/3)*65)+(ceil(count($strips)/3)*2)+2?>px;">
					<?	foreach($strips as $y=>$item): ?>
						<a onclick="collectionPopup.setActivePhoto(<?=$item->getPhotoID()?>); return false;" href="javascript:void(0)">
							<img src="<?=$item->getPhotoLink('thumbnail')?>" width="65" height="65" border="0" align="left" style="margin-left:1px; margin-right:1px; margin-bottom:1px; margin-top:1px;" />
						</a>
					<?	endforeach; ?>
				</div>
			<?	endif; ?>
			</div>
		</div>
	
		<?	if( $stripsVisible < $stripCount ): ?>
			<div id="btnNextStrip" class="bottombuttons"><a href="" class="downmore" onclick="collectionPopup.scrollToNextStrip(); return false;"><span>down</span></a></div>
		<?	endif; ?>
		
	</div>
</div>

<div id="photos_info" style="display: none;">
	<?	for($i=0; $i<count($captions); $i++): 
			$caption = $captions[$i];
			$link = $journalLinks[$i];
			//$uploaded = $dateUploaded[$i];
			$jtitle = $journalTitles[$i];
	?>
		<p id="caption_<?=$i?>"><?=$caption?></p>
		<p id="link_<?=$i?>"><?=$link?></p>
		<p id="jtitle_<?=$i?>"><?=htmlspecialchars(stripslashes(strip_tags($jtitle)),ENT_QUOTES)?></p>
		<?/*p id="uploaded_<?=$i?>"><?=$uploaded?></p*/?>+
	<?	endfor; ?>
</div>

<div class="boxComments">
	<div id="active_photo_caption" class="boxCaption"> 
		<?=htmlspecialchars(stripslashes(strip_tags($defaultPhoto->getCaption())),ENT_QUOTES)?> 
	</div>
	<strong style="clear:both;display:block;height:18px;">
		<?	if( 1 == $photo->getPhototypeID() ): ?>
			<img id="journal" src="/images/profilealbum.gif" class="jLeft"/>
		<? 	elseif( 2 == $photo->getPhototypeID() || 11 == $photo->getPhototypeID() ): ?>
			<img id="journal" src="/images/journal_icon2.gif" class="jLeft"/>
		<?	else: ?>
			<img id="journal" src="/images/photoalbum.gif" class="jLeft"/>
		<?	endif; ?>
		<span id="journal_title" class="divider jLeft" style="margin-right:10px;"><?=htmlspecialchars(stripslashes(strip_tags($title)),ENT_QUOTES)?></span>
		<img id="loading_photo_" src="/images/loading_small.gif" align="center" class="jLeft" style="display:none;"/>
	</strong>
	<?/*Uploaded: <span id="date_uploaded"><?=$defaultPhoto->getDateuploaded()?></span>*/?>
</div>

	
<?	$startPos = floor($defaultIndex / $photosPerStrip) + 1; ?>

<script type="text/javascript">
	jQuery(document).ready(function(){
		collectionPopup.setGalleryData(<?=$defaultIndex?>,<?=count($photos)?>,'<?=implode(",",$photoIDs)?>','<?=implode(",",$links)?>');
		collectionPopup.setSlider(contentSlider,"slider_section",<?=$startPos?>,<?=$photosPerStrip?>,<?=$stripCount?>);
		collectionPopup.setActivePhoto(<?=$defaultPhoto->getPhotoID()?>);
		
		jQuery("#active_photo").bind("load",
			function(){
				if( null != jQuery("#navhover") ){
					collectionPopup.resetNavOnPhoto();
				}
			}
		);
		
	});
</script>