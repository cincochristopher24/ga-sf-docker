<?php
	require_once("Class.Template.php");
	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");

	class ViewOldAlbum extends RequestData{

		private $mCollectionItem	=	NULL;
		
		public function setCollectionItem($item=NULL){
			if( $item ){
				$this->setContext($item->getContext());
				$this->setGenID($item->getGenID());
				$this->mCollectionItem = $item;
			}
		}
		
		function render(){
			$photoContext = $this->getPhotoContext();
			$content = new Template();
			$content->set("userLevel",$this->getUserLevel());
			$content->set("type",$this->getType());
			$content->set("ID",$this->getID());
			$content->set("context",$this->getContext());
			$content->set("genID",$this->getGenID());
			if( "journal" == $this->getContext() ){
				$content->set("title",$this->getCollection($this->getGenID())->getTitle());
			}else{
				$content->set("title",$photoContext->getHeaderCaption());
			}
			$content->set("photoContext",$this->getPhotoContext()->getPHOTOContext());
			
			if( "journal" == $this->getContext() ){		
				require_once("travellog/model/Class.Travel.php");
				$journal = new Travel($this->getGenID());
				if( "traveler" == $this->getType() ){
					$travellogs = $journal->getTravelerTravelLogs();
				}else{
					$travellogs = $journal->getTravelLogsByTravelID();
				}
				$photos = array();
				$entry_photos = array();
				$entryPrimaries = array();
				$tlog_temp = array();
				$tlog_photo_count = array();
				$travellogIDs = array();
				foreach($travellogs as $tlog){
					$temp = $tlog->getPhotos();
					if( is_array($temp) ){
						$entryPrimaries[] = $tlog->getPrimaryPhoto()->getPhotoID();
						$photos = array_merge($photos,$temp);
						$entry_photos[] = $temp;
						$tlog_temp[] = $tlog;
						$tlog_photo_count[] = count($temp);
					}else{
						$entryPrimaries[] = 0;
						$entry_photos[] = array();
						$tlog_temp[] = $tlog;
						$tlog_photo_count[] = 0;
					}
					$travellogIDs[] = $tlog->getTravelLogID();
				}
				$content->set("contextPrimary",$journal->getPrimaryphoto());
				$content->set("photos",$photos);
				$content->set("travellogs",$tlog_temp);
				$content->set("entry_photos",$entry_photos);
				$content->set("entryPrimaries",$entryPrimaries);
				$content->set("tlog_photo_count",$tlog_photo_count);
				$content->set("travellogIDs",$travellogIDs);
			}else{
				$content->set("contextPrimary",$photoContext->getPHOTOContext()->getPrimaryPhoto()->getPhotoID());
				$photos = $photoContext->getPHOTOContext()->getPhotos();
				$content->set("photos",$photos);
				$content->set("entryPrimaries",array());
				$content->set("travellogIDs",array());
				$content->set("tlog_photo_count",array(count($photos)));
			}
			$content->set("journalPrimary",0);
			$content->set("isPrivileged",(0<$this->getUserLevel())? TRUE : FALSE);
			$content->out("travellog/UIComponent/Collection/views/tpl.ViewPhotoList.php");
		}
	}