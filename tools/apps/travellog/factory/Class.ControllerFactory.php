<?php

require_once('travellog/factory/Class.AbstractFactory.php');
//require_once("Class.Constants.php");

class ControllerFactory extends AbstractFactory{
	
	static $instance = NULL;
	
	//private $ALLOWED_COBRANDS_PROD = array('AIFS'); 
	
	static function getInstance(){
		if( self::$instance == NULL ) self::$instance = new ControllerFactory;
		return self::$instance;
	}
	
	
	function createController($name = 'Index'){

		require_once("Class.LastVisitedPages.php");			
		LastVisitedPages::insert("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

		/***
		require_once('Class.Ini_Config.php');
		require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');

		$ini = IniConfig::instance();
		$site   = $ini->getSite();
		
		$folder     = ('GANET' == $site) ? '/controller/' : '/custom/' . $site . '/controller/';
		$controller = $site . $name . 'Controller';
		***/
		
		require_once('travellog/model/Class.SiteContext.php');
		$siteContext = SiteContext::getInstance();

		if($siteContext->isInCobrand()){
			$folder = '/custom/' . strtoupper($siteContext->getFolderName()) . '/controller/';
		}
		else{
			$folder = '/controller/';
		}
		$controller = strtoupper($siteContext->getFolderName()) . $name . 'Controller';
		
		/**
		$host     = explode( '.' , $_SERVER['HTTP_HOST'] );
		$co_brand = strtoupper($host[0]);
		
		if( in_array( $co_brand, array_map('strtoupper', constants::$ALLOWED_COBRANDS ) ) ){
			// temp replace '-dev' string  to make it work in dev 
			//$co_brand    = str_replace('-dev', '', $co_brand); 
			$folder     = '/custom/' . $co_brand . '/controller/';
			$controller = $co_brand . $name . 'Controller';  
		}
		else{
			$folder     = '/controller/';
			$controller = 'GANET' . $name . 'Controller';
		}
		*/
	
		require_once('travellog'.$folder.'Class.'.$controller.'.php');
		if( !array_key_exists($name, $this->controllers) ) $this->controllers[$name] = new $controller;
		return $this->controllers[$name];
	}

}
?>
