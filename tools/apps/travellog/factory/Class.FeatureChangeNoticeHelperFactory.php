<?php

	require_once("Class.FileFactory.php");

	class FeatureChangeNoticeHelperFactory{
		
		private $CHANGED_FEATURES = array("ADVISOR_ACCOUNT","DASHBOARD_FEATURE");
		
		private static $instance = NULL;
		
		private function __construct(){
		}
		
		public static function instance(){
			if( is_null(self::$instance) ){
				self::$instance = new FeatureChangeNoticeHelperFactory();
			}
			return self::$instance;
		}
		
		public function create($feature=NULL){
			if( !in_array($feature,$this->CHANGED_FEATURES) ){
				throw new Exception("Unknown feature: {".$feature."} Feature must be in {".implode(",",$this->CHANGED_FEATURES)."}");
			}
			$file_factory = FileFactory::getInstance();
			$noticeHelper = NULL;
			if( "ADVISOR_ACCOUNT" == $feature ){
				require_once("travellog/helper/Class.AdvisorAccountChangeNoticeHelper.php");
				$noticeHelper = new AdvisorAccountChangeNoticeHelper();
			}else if( "DASHBOARD_FEATURE" == $feature ){
				require_once("travellog/helper/Class.DashboardFeatureNoticeHelper.php");
				$noticeHelper = new DashboardFeatureNoticeHelper();
			}
			return $noticeHelper;
		}
	}