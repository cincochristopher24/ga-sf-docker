<?php
require_once('travellog/factory/Class.AbstractFactory.php');
class COBRANDControllerFactory extends AbstractFactory{
	
	static $instance = NULL;
	
	static function getInstance(){
		if( self::$instance == NULL ) self::$instance = new COBRANDControllerFactory;
		return self::$instance;
	}
	
	function createController($name = 'Index'){
		$host     = explode( ',' , $_SERVER['HTTP_HOST'] );
		$co_brand = ( count($host) == 3 )? $host[0]: 'Ganet';
		if( count($host) == 3 ){
			$folder     = '/' . strtoupper($co_brand);
			$controller = ucfirst($host[0]) . $name . 'Controller';
		}
		else{
			$folder     = '';
			$controller = 'Ganet' . $name . 'Controller';
		}
		
		switch($name){
			case 'Travelers':
				require_once('travellog/controller'.$folder.'/Class.'.$controller.'.php');
				if( !array_key_exists($name, $this->controllers) ) $this->controllers[$name] = new $controller;
			break;
			 
			default:
				require_once('travellog/controller/Class.COBRANDIndexController.php');
				if( !array_key_exists($name, $this->controllers) ) $this->controllers[$name] = new COBRANDIndexController;
		}
		
		return $this->controllers[$name];
	}
}
?>
