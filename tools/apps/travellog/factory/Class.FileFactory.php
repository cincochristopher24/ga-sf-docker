<?php
class FileFactory{
	
	static $instance         = NULL;
	
	static $ENUM_VALID_PATH  = array('CSS', 'HEADER_FOOTER');
	
	private 
	
	$css_header_footer_path  = array(),
	
	$paths                   = array(),
	
	$css                     = array(),
	
	$javascript              = array(), 
	
	$call_backs              = array(),
	
	$templates               = array(),
	
	$classes                 = array();
	
	
	static function getInstance(){
		if( self::$instance == NULL ) self::$instance = new FileFactory;
		return self::$instance; 
	}
	
	/**
	 *  registers a class for future instantiation using the $name.
	 *  @param assoc-array options contains 'path' and 'file' index
	 */
	function registerClass($name, $options = array()){
		$defaults = array('path' => 'travellog/model/', 'file' => $name);
		$this->__register($name, $defaults, $options);
		//$path  = $options['path'];
		//$file  = $options['file'];
		//if( substr($path, -1) != '/' && $path != '') $path .= '/';
		//require_once($path.'Class.'.$file.'.php');
	}
	
	function getClass($name, $params = NULL, $list=true){
		$path  = $this->classes[$name]['path'];
		$file  = $this->classes[$name]['file'];
		$class = ( isset($this->classes[$name]['class']) )? $this->classes[$name]['class']: $file;
		if( substr($path, -1) != '/' && $path != '') $path .= '/';
		require_once($path.'Class.'.$file.'.php');
		if( $list ){
			if($params == NULL)
				$obj = new $class;
			elseif (count($params) == 1) {
				$obj = new $class($params[0]);
			}
			else{ 								
				$obj_reflection    = new ReflectionClass( $class );
				$obj =  $obj_reflection->newInstanceArgs($params); 								
			}
		}else 
			$obj = ($params == NULL)? new $class: new $class($params);
		
		$this->invokeCallbacks($name, $obj);			
		return $obj;
	}
	
	/**
	 *  call a static function on a class
	 * @param string $name name of the class
	 * @param string $func name of the function to call
	 * @param array  $params  parameters to pass to the function  
	 */
	function invokeStaticClass($name,  $func = 'instance', $params = NULL){
		$path  = $this->classes[$name]['path'];
		$file  = $this->classes[$name]['file'];
		$class = ( isset($this->classes[$name]['class']) )? $this->classes[$name]['class']: $file;
		if( substr($path, -1) != '/' && $path != '' ) $path .= '/';
		require_once($path.'Class.'.$file.'.php');
		$obj_reflection = new ReflectionClass( $file );
		$reflection_method = $obj_reflection->getMethod($func);  
		
		return ( $params == NULL )? $reflection_method->invoke( null ): $reflection_method->invokeArgs( null, $params );
	}
	
	
	/**
	 *  get the value of a static var
	 *  @param string $className the name of the class 
	 *  @param string $var the static variable to access
	 * 
	 */
	function invokeStaticVar($className,$var) {
		$path  = $this->classes[$className]['path'];
		$file  = $this->classes[$className]['file'];
		$class = ( isset($this->classes[$className]['class']) )? $this->classes[$className]['class']: $file;
		if( substr($path, -1) != '/' && $path != '' ) $path .= '/';
		require_once($path.'Class.'.$file.'.php');
		$obj_reflection = new ReflectionClass( $file );
		$ref_prop = $obj_reflection->getProperty($var);
		return $ref_prop->getValue(null);
		
	}
	
	function invokeCallbacks($name, $obj){
		if( array_key_exists($name ,$this->call_backs) ){
			foreach( $this->call_backs[$name] as $callback ){
				$func    = $callback['func'];
				$class   = $callback['class']; 
				$params  = $callback['params'];
				call_user_func_array(array($obj, $func), $params);
			}
		}
	}
	
	/**
	 * Registers methods to call after a class has been instantiated.
	 *   This could be used to override default class initializers (the class setters)
	 * @param string name 'The key name of the class to invoke after instantiation'
	 * @param assoc-array options 'contains "func" as the function name in string and "params" as an assoc array of method params'  
	 */
	function registerClassInitCall($name, $options = array()){
		$func                           = $options['func'];
		$options['class']               = ( array_key_exists('class' , $options) )? $options['class'] : $name;
		$options['params']              = ( array_key_exists('params', $options) )? $options['params'] : NULL;
		$this->call_backs[$name][$func] = $options;
	}
	
	function registerTemplate($name, $options = array()){
		$defaults = array('path' => 'travellog/views/', 'file' => $name);
		$this->__registerTemplate($name, $defaults, $options);
	}
	
	function getTemplate($name){
		$path = $this->templates[$name]['path'];
		$file = $this->templates[$name]['file'];
		if( substr($path, -1) != '/' ) $path .= '/';
		return $path.'tpl.'.$file.'.php';
	}
	
	private function __register($name, $defaults, &$options){
		$options  = array_merge($defaults, $options);
		$this->classes[$name] = $options;
	}
	
	private function __registerTemplate($name, $defaults,$options){
		$options  = array_merge($defaults, $options);
		$this->templates[$name] = $options;
	}
	
	function setPath($name, $path){
		try{
			if( in_array($name, self::$ENUM_VALID_PATH ) )
				$this->css_header_footer_path[$name] = $path;
			else
				throw new Exception('Invalid!!');
		}catch( Exception $e ){
			echo $e;
		}	
	}
	
	function getPath($name){
		
		return $this->css_header_footer_path[$name];
	}
}
?>