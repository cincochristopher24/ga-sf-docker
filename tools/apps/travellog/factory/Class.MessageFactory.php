<?php
	/*
	 * Class.MessageFactory.php
	 * Created on Nov 9, 2007
	 * created by marc
	 */
	 
	 require_once("Class.Connection.php");
	 require_once("Class.Recordset.php");
	 require_once("travellog/model/Class.Message.php");
	 
	 class MessageFactory{
	 	
	 	static $conn = NULL;
	 	static $rs = NULL;
	 	static $instance = NULL;
	 	
	 	static function getInstance(){
	 		
	 		if(  NULL == self::$instance ){
	 			self::$conn = new Connection;
	 			self::$rs = new Recordset(self::$conn);
	 			self::$instance = new self;
	 		}
	 		
	 		return self::$instance;
	 	}
	 	
	 	function create($msgID,$ownerID,$folder = 0){
	 		
	 		$myquery = self::$rs->Execute("SELECT discriminator FROM tblMessages,tblMessageToAttribute WHERE attributeID = $msgID AND tblMessages.messageID = tblMessageToAttribute.messageID");
			
			if( $row = mysql_fetch_array($myquery) ){
				if( 0 < $folder && !Message::isOwner($_GET['messageID'],$traveler->getSendableID(),$FOLDER ) )
					throw new Exception("Sorry, the message you requested does not exist");	
				
				switch($row['discriminator']){
					case 3 :
						return new PersonalMessage($msgID);
					case 4 :
						return new InquiryMessage($msgID);
					default:
						throw new Exception("Sorry, the message you requested does not exist");	
				}
			}
			else
				throw new Exception("Sorry, the message you requested does not exist");
	 	}
	 		
	 }
?>
