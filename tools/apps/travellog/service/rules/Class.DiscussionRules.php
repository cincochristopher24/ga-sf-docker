<?php
	/**
	 * @(#) Class.DiscussionRules.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - May 7, 2009
	 */
	
	require_once('travellog/model/Class.SessionManager.php');
	
	/**
	 * Handles the rules of the discussion.
	 */
	class DiscussionRules {
		
		/**
		 * Handles the validation for featuring a discussion.
		 * 
		 * @param array $data The data to be validated.
		 * 
		 * @return boolean true if the given data are valid. False otherwise.
		 */
		static function validateFeaturingDiscussion(array &$data){
			require_once("travellog/model/Class.DiscussionPeer.php");
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			if (!is_null($data['discussion']) AND 0 < $data['travelerID']) {
				return true;
			}
			
			return false;
		}
		
		/**
		 * Handles the validation for featuring a discussion.
		 * 
		 * @param array $data The data to be validated.
		 * 
		 * @return boolean true if the given data are valid. False otherwise.
		 */
		static function validateUnfeaturingDiscussion(array &$data){
			require_once("travellog/model/Class.DiscussionPeer.php");
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			if (!is_null($data['discussion']) AND 0 < $data['travelerID']) {
				return true;
			}
			
			return false;
		}
	}
	
