<?php
	/**
	 * @(#) Class.JournalEntryDraftRules.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 27, 2009
	 */
	
	require_once('travellog/model/Class.SessionManager.php');
	
	/**
	 * journal entry draft rules
	 */
	class JournalEntryDraftRules {
		const GROUP = 1;
		const TRAVELER = 2;
		
		/**
		 * Validates the data used for deleting a given journal entry draft.
		 * 
		 * @param array $data The basis for validation.
		 * 
		 * @return boolean true if the data were valid; false otherwise.
		 */
		static function validateDeleteDraft(&$data){
			require_once('travellog/model/Class.JournalPeer.php');
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			
			if (is_numeric($data['draftID']) AND 0 < $data['draftID']) {
				$data['draft'] = JournalEntryDraftPeer::retrieveByPk($data['draftID']);
				if (!is_null($data['draft'])) {
					$data['draft_owner'] = JournalPeer::getJournalLinkedOwner($data['draft']->getTravelID());
					if ($data['draft_owner'] instanceof AdminGroup AND ($data['draft_owner']->getAdministratorID() == $data['travelerID'] OR $data['draft_owner']->isStaff($data['travelerID']))) {
						return true;
					}
					else if ($data['draft_owner'] instanceof Traveler AND $data['draft_owner']->getTravelerID() == $data['travelerID']) {
						return true;
					}
				}
			}
			
			return false;			
		}
		
		/**
		 * Validates the data in viewing a journal entry draft.
		 * 
		 * @param array $data The basis for validation.
		 * 
		 * @return boolean true if the data were valid; false otherwise.
		 */
		static function validateViewDraft(&$data){
			require_once('travellog/model/Class.JournalPeer.php');
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			
			if (is_numeric($data['draftID']) AND 0 < $data['draftID']) {
				if ($data['draft'] = JournalEntryDraftPeer::retrieveByPk($data['draftID'])) {
					$data['journal'] = JournalPeer::retrieveByPk($data['draft']->getTravelID());
					$data['draft_owner'] = JournalPeer::getJournalLinkedOwner($data['draft']->getTravelID());
					if ($data['draft_owner'] instanceof AdminGroup AND ($data['draft_owner']->getAdministratorID() == $data['travelerID'] OR $data['draft_owner']->isStaff($data['travelerID']))) {
						return true;
					}
					else if ($data['draft_owner'] instanceof Traveler AND $data['draft_owner']->getTravelerID() == $data['travelerID']) {
						return true;
					}
				}
			}
			
			return false;		
		}
		
		/**
		 * Validates the data in viewing all saved journal entry drafts of a group or a 
		 * traveler.
		 * 
		 * @param array $data The basis for validation.
		 * 
		 * @return boolean true if the data were valid; false otherwise.
		 */
		static function validateViewSavedDrafts(&$data){
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			
			if (is_numeric($data['journalID']) AND 0 < $data['travelerID'] AND 'group' == $data['context'] AND 0 < $data['gID']) { // viewing journal entry drafts of a group.
				require_once("travellog/model/Class.GroupPeer.php");
				$data['group'] = GroupPeer::retrieveByPk($data['gID']);
				if (!is_null($data['group']) AND $data['group']->getAdministratorID() == $data['travelerID'] || $data['group']->isStaff($data['travelerID'])) {
					$data['context'] = self::GROUP;
					$data['gID'] = $data['gID'];
					return true;
				}
			}
			else if (is_numeric($data['journalID']) AND 0 < $data['travelerID'] AND 'group' != $data['context']) { // viewing journal entry drafts of a traveler.
				$data['context'] = self::TRAVELER;
				return true;
			}
			
			return false;			
		}
		
		/**
		 * Validates saving of drafts through ajax request.
		 * 
		 * @param array $data The data used for validation.
		 * 
		 * @return void
		 */
		static function validateSavingDraft(array &$data){
			require_once('travellog/model/Class.JournalPeer.php');
			
			if (isset($_POST['travelID'])) {				
				$session = SessionManager::getInstance();
				$data['travelerID'] = $session->get('travelerID');
				$data['title'] = isset($_POST['title']) ? $_POST['title'] : "";
				$data['countryID'] = isset($_POST['countryID']) ? $_POST['countryID'] : 0;
				$data['cityID'] = isset($_POST['cityID']) ? $_POST['cityID'] : 0;
				$data['logDate'] = isset($_POST['logDate']) ? $_POST['logDate'] : "";
				$data['callout'] = isset($_POST['callout']) ? $_POST['callout'] : "";
				$data['travelID'] = $_POST['travelID'];
				$data['description'] = isset($_POST['description']) ? $_POST['description'] : "";
				$data['isAutoSaved'] = isset($_POST['autoSaved']) ? $_POST['autoSaved'] : 0;
				$data['journalEntryDraftID'] = isset($_POST['draftID']) ? $_POST['draftID'] : 0;
				
				$data['draft_owner'] = JournalPeer::getJournalLinkedOwner($_POST['travelID']);
				if ($data['draft_owner'] instanceof AdminGroup AND ($data['draft_owner']->getAdministratorID() == $data['travelerID'] OR $data['draft_owner']->isStaff($data['travelerID']))) {
					return true;
				}
				else if ($data['draft_owner'] instanceof Traveler AND $data['draft_owner']->getTravelerID() == $data['travelerID']) {
					return true;
				}
			}
			
			return false;
		}
	}
	
