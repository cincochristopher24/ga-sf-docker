<?php
require_once('travellog/route/model/Class.gaAbstractRuleFactory.php');
require_once('travellog/controller/Class.gaActionType.php');
class FacebookAppActionRulesFactory extends gaAbstractRuleFactory{
	
	static function getInstance(){ 
		if( self::$instance == NULL ) self::$instance = new FacebookAppActionRulesFactory; 
		return self::$instance;
	}
	
	function createRules($props){
		$rules = array();
		
		switch( $props['action'] ){
			case gaActionType::$SAVE:
				require_once('travellog/rules/Class.FacebookAppSaveRules.php');
				require_once("travellog/rules/Class.FacebookAppAssessorActionFactory.php");
				$factory = FacebookAppAssessorActionFactory::getInstance();
				
				$rules['FacebookAppSaveRules'] = new FacebookAppSaveRules( $factory );
			break;
			
			case gaActionType::$UPDATE:
				require_once('travellog/rules/Class.FacebookAppUpdateRules.php');
				require_once("travellog/rules/Class.FacebookAppAssessorActionFactory.php");
				$factory = FacebookAppAssessorActionFactory::getInstance();
				
				$rules['FacebookAppUpdateRules'] = new FacebookAppUpdateRules( $factory );
			break;
			
			case gaActionType::$DELETE:
				require_once('travellog/rules/Class.FacebookAppDeleteRules.php');
				require_once("travellog/rules/Class.FacebookAppAssessorActionFactory.php");
				$factory = FacebookAppAssessorActionFactory::getInstance();
				 
				$rules['FacebookAppDeleteRules'] = new FacebookAppDeleteRules( $factory );
			break;
			
		}
		 
		return $rules;
	}
	
}
?>
