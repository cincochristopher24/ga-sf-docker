<?php
require_once("travellog/route/model/Class.gaIAction.php");
class DeleteTravelDestination implements gaIAction{
	function performAction( &$props ){
		require_once('travellog/model/Class.TravelSchedule.php');
		require_once('travellog/model/Class.TravelDestinationPeer.php');
		$props['travelWishID'] = TravelSchedule::getTravelWishIDByTravelID( $props['travelID'] );
		
		TravelDestinationPeer::deleteDestination($props['travelWishID'], $props['countryID']); 
	}
}
?>
