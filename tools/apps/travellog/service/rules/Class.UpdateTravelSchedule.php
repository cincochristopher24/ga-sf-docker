<?php
require_once("travellog/route/model/Class.gaIAction.php");
class UpdateTravelSchedule implements gaIAction{
	function performAction( &$props ){
		require_once('travellog/model/Class.TravelSchedule.php');
		$obj_item = TravelSchedule::retrieveByPk($props['travelWishID']);
		$obj_item->setTitle  ( $props['title']       );
		$obj_item->setDetails( $props['description'] ); 
		
		$obj_item->save(); 
	}
}
?>
