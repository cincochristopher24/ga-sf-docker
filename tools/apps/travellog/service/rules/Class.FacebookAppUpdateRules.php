<?php
require_once("travellog/route/model/Class.gaIRule.php");
class FacebookAppUpdateRules implements gaIRule{
	
	private $factory = null; 
	
	function __construct($factory){
		$this->factory = $factory;  
	}
	
	function applyRules( $props ){
		
		if( $props['type'] == 'journal' ){
		
			if( $this->factory->createAssessor('HasBeenAddedToTravelWish')->evaluate( $props ) )
				$this->factory->createAction('UpdateTravelSchedule')->performAction( $props ); 
		
		}
		else{
			if( $this->factory->createAssessor('IsDestinationExists')->evaluate( $props ) )
				$this->factory->createAction('UpdateTravelDestination')->performAction( $props );
			else
				$this->factory->createAction('SaveTravelDestination')->performAction( $props );
		} 
		
	}
}
?>
