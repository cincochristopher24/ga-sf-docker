<?php
require_once("travellog/route/model/Class.gaIAssessor.php");
class HasAnEntries implements gaIAssessor{
	function evaluate( $props ){
		require_once('travellog/model/Class.Travel.php');
		$obj_travel = new Travel($props['travelID']);
		$col_trips  = $obj_travel->getTrips();
		return ( count($col_trips) )? true : false;  
	}
}
?>
