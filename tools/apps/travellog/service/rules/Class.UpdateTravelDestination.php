<?php
require_once("travellog/route/model/Class.gaIAction.php");
class UpdateTravelDestination implements gaIAction{
	function performAction( &$props ){
		require_once('travellog/model/Class.TravelDestinationPeer.php');
		require_once('travellog/model/Class.TravelSchedule.php');
		require_once('travellog/model/Class.Travel.php');

		$props['travelWishID'] = TravelSchedule::getTravelWishIDByTravelID( $props['travelID'] );
		$obj_destination       = TravelDestinationPeer::getTravelDestination( $props['travelWishID'], $props['countryID'] );
		$dates                 = Travel::getTripsByLocation( $props['travelID'], $props['countryID'] );
		
		$obj_destination->setStartDate( $dates[0]               );
		$obj_destination->setEndDate  ( $dates[count($dates)-1] );
		
		TravelDestinationPeer::edit( $obj_destination );
	}
}
?>
