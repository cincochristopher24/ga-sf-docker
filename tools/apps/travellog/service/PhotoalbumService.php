<?

require_once('Class.Template.php');	
require_once('travellog/model/Class.PhotoAlbum.php');
require_once('travellog/model/Class.AdminGroup.php');
require_once('travellog/model/nPaging.php');
		
class PhotoalbumService{	
	
	
		private $template_path = 'travellog/views/'; 
		
		function setTemplateViews($template_path ='travellog/views/'){
			$this->template_path = $template_path;			
		}
		
		
		function getPhotoAlbum(){
			
			$travelerID = isset($_REQUEST['travelerID'])?$_REQUEST['travelerID']:0;
			$startrow 	= isset($_REQUEST['startrow'])?$_REQUEST['startrow']:1;
           	$pagenum 	= isset($_REQUEST['pagenum'])?$_REQUEST['pagenum']:1;
			
			$owner = false;
			
			$template = new Template();
		    $template->set_path($this->template_path);	
			
			$group = new AdminGroup($_REQUEST['groupID']);
			//$photoalbum = $group->getPhotoAlbums();	
			
			
			require_once('travellog/model/Class.PhotoAlbumAdapter.php');
			$PhotoAlbumAdapter = new PhotoAlbumAdapter();
			$PhotoAlbumAdapter->setLoguserID($travelerID);
			$photoalbum = $PhotoAlbumAdapter->getAlbums($group);
			
			
			if(in_array($travelerID,$group->getStaff(true)) || $group->getAdministrator()->gettravelerID() == $travelerID){
				$owner = true;
				$reccount = count($photoalbum);
			}else{
				//filter album with uploaded photo
				
				$photos = array();
				if(count($photoalbum)>0){
					foreach($photoalbum as $i =>$album){
							if($album['numphotos'] > 0){
									$photos[] =$album;	
							}
					}
					
					$reccount 		= count($photos);
					$photoalbum	= $photos;
				}
				
			}	
													
			//$reccount = count($photoalbum);
															
			nPaging::Setajax(true);															
			$page = new nPaging($reccount,$startrow,$pagenum,12);								
									
			$start= $startrow - 1;
			$endrow= ($startrow + 12) -1;
			($endrow > $reccount)? $endrow = $reccount: $endrow;
			
			require_once("travellog/UIComponent/Photo/facade/PhotoUIFacade.php");
			$Photo = new PhotoUIFacade();
			$Photo->setContext('group');
			$Photo->setGenID($_REQUEST['groupID']);
			$Photo->setLoginID(0);
			$Photo->setGroupHasPhotosInAlbum(true);
			$Photo->build();	
		
			
																					
			$template->set_vars(array(	
							'start' 				=> $start,
							'endrow'				=> $endrow,
							'reccount'				=> $reccount,
							'recordno' 				=> $startrow." - ".$endrow. " of ".$reccount,
							'page'					=> $page,
							'isAdminLogged'					=> $owner,
							'loggedUserID'		=>$travelerID,																
							'photoalbum'			=> $photoalbum,
							'groupID'			=> $_REQUEST['groupID'],
							'tophotolink'			=> "photomanagement.php?cat=photoalbum&action=view",
							'editlink'				=> "photoalbum.php?action=edit",
							'uploadlink'			=> "photomanagement.php?cat=photoalbum&action=add",
							'deletelink'			=>"photoalbum.php?action=delete"));	
			
			echo $template->fetch('tpl.IncViewPhotoAlbum.php');	
		
		}
		
		function Delete(){
			$photoalbum = new PhotoAlbum($_REQUEST['photoalbumid']);
			$photoalbum->Delete();
			$this->getPhotoAlbum();
		}
		
		function showedit(){
			$template = new Template();
		    $template->set_path($this->template_path);
		    
		    $photoalbum = new PhotoAlbum($_REQUEST['photoalbumid']);									
			$template->set('photoalbum',$photoalbum);			
			
			echo $template->fetch('tpl.EditPhotoAlbum.php');		
		}
		
		function update(){
			$photoalbum = new PhotoAlbum($_REQUEST['photoalbumid']);			
			$datecreated	=getdate();
			$datecreated = $datecreated['year']."-".$datecreated['mon']."-".$datecreated['mday']." ".$datecreated['hours'].":".$datecreated['minutes'].":".$datecreated['seconds'];
								
			$photoalbum->setTitle($_REQUEST['title']);
			//$photoalbum->setLastUpdate($datecreated);																	
			$photoalbum->Update();
			$this->getPhotoAlbum();
				
		}
		
		function isOwner($groupID){
			
			$group = new AdminGroup($groupID);
			$photoalbum = $group->getPhotoAlbums();	
			
			$travelerID = isset($_REQUEST['travelerID'])?$_REQUEST['travelerID']:0;
			
			if($group->getAdministrator()->gettravelerID() == $travelerID)				
				return true;
			else
				return false;	
				
		}
}
		
		
?>