<?php
/*
 * Created on 03 20, 09
 * 
 * Author:
 * Purpose: 
 * 
 */
 
 require_once 'travellog/vo/Class.ganetValueObject.php';
 
 abstract class AbstractTravelerService {
 	
 	abstract function retrieve(ganetValueObject $vo);
 	abstract function getList(ganetValueObject $context); 	
 	function blockUser(){}
 	function unblockUser(){}
 	function getStatus($travelerId,$context = array()){}				//status is if online, active , suspended etc
 
 }
 
 
 
?>
