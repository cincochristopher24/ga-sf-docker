<?	
		require_once('Class.Template.php');	
		require_once('travellog/controller/Class.PhotoController.php');
		require_once('travellog/model/nPaging.php');
		require_once('travellog/views/Class.CommentsView.php');		
		require_once('travellog/model/navigator/Class.BackLink.php');
		

	class PhotoService{
		
		private $template_path = 'travellog/views/'; 
		private $request = NULL;
		
		
		function PhotoService(){
			//$this->request = $request;
			
		}
		
		function setTemplateViews($template_path ='travellog/views/'){
			$this->template_path = $template_path;			
		}
		
		
		function getPhotos(){
			
			$imgpiccontainer = "piccontainer";
			
			$travelerID = isset($_REQUEST['travelerID'])?$_REQUEST['travelerID']:0;
			$tmpprev 	= isset($_REQUEST['tmpprev'])?$_REQUEST['tmpprev']:0;
			
			$startrow 	= isset($_REQUEST['startrow'])?$_REQUEST['startrow']:1;
           	$pagenum 	=  isset($_REQUEST['pagenum'])?$_REQUEST['pagenum']:1;
			
			$template = new Template();
		    $template->set_path($this->template_path);		    
		    
		    require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		    new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
		    $Pfactory = PhotoFactory::create();
		    
		    
		    $reccount 	= $Pfactory->getCOUNTPhotos();						  			
			$photos		= $Pfactory->getPhotos();
			$owner		= $Pfactory->isOwner($travelerID);	 
			
			$start 		= 0;
			$endrow 	= 0;
			$nextpage 	=  NULL;
			$prevpage	= NULL;
			
			if($reccount){
			
					if(isset($_REQUEST['photoID'])){		
							
							$temp = array();
							
							foreach($photos as $pic){						
									$temp[] = $pic->getPhotoId();							
							}					
							$currentKey 		= array_search($_REQUEST['photoID'],$temp);
							
							if(strpos($currentKey/12,'.')){					
									$pagenum		 	= ceil($currentKey/12);
									$tmpprev =$currentKey+1;					
									$start		= ($pagenum-1)*12;				
									$startrow	=$start+1;					
									$endrow= ($startrow + 12)-1;
									$endrow = $endrow > $reccount?  $reccount: $endrow;
							
									if($endrow == $currentKey+1){
										if($currentKey+1 <	$reccount){
											$nextpage	= 'photo.page('.($endrow+1).','.($pagenum+1).',0)';
										}
										$prevpage	= "photo.fullSizeView($endrow-2,$endrow,$pagenum,'$imgpiccontainer')";
									}else{
										$nxtphotoid = $photos[$currentKey+1]->getPhotoID();
										$nextpage	= "photo.fullSizeView($currentKey+1,$endrow,$pagenum,'$imgpiccontainer')";
										$prevpage	= "photo.fullSizeView($currentKey-1,$endrow,$pagenum,'$imgpiccontainer')";
									}
									
							}else{
									$pagenum		 	= ($currentKey/12)+1;
									$tmpprev =$currentKey+1;					
									$start		= ($pagenum-1)*12;				
									$startrow	=$start +1;					
									
									$endrow= ($startrow + 12-1);
									$endrow = $endrow > $reccount?  $reccount: $endrow;
									
											if($currentKey == $start){										
												$startrow <1?$startrow=1:$startrow=$startrow;										
												if($currentKey+1 <	$reccount){										
													$nextpage	= "photo.fullSizeView($start+1,$endrow,$pagenum,'$imgpiccontainer')";
												}										
												$prevpage	= 'photo.page('.($currentKey-12+1).','.($pagenum-1).','.$currentKey.')';										
											}
							}
							
					}else{
							$start= $startrow - 1;
										
							$endrow	= ($startrow + 12) -1;
							$endrow 	= $endrow > $reccount?  $reccount: $endrow;
							
									if($tmpprev){						
										$nextpage	= 'photo.page('.($endrow+1).','.($pagenum+1).',0)';
										$prevpage	= "photo.fullSizeView($endrow-2,$endrow,$pagenum,'$imgpiccontainer')";
									}else{
										
										if($start+1 < $reccount){
										
											$nextpage	= "photo.fullSizeView($startrow,$endrow,$pagenum,'$imgpiccontainer')";
										}
										
										$prevpage	= 'photo.page('.($startrow-12).','.($pagenum-1).','.$start.')';
									}	
					}
			
					
			}
			
						
			Paging::Setajax(true);															
			$page = new Paging($reccount,$startrow,$pagenum,12);								
			

			if($reccount){
						$file_factory = FileFactory::getInstance();			
						$commentsview = $file_factory->getClass('CommentsView');
						$commentsview->setContext(2,$photos[$tmpprev?$tmpprev-1:$start]->getphotoID());
						//$commentsview->setPhotoCategory($this->request['context']);
						$commentsview->setgenID($_REQUEST['genID']);
						$commentsview->setTravelerID($travelerID);
						
						require_once("Class.HelperGlobal.php");
						
						$commentsview->setEnvURL("photomanagement.php?cat=".$_REQUEST['cat']."&action=vfullsize&genID=".$_REQUEST['genID']."&photoID=".$photos[$tmpprev?$tmpprev-1:$start]->getphotoID()."^comment_form");
												   
						$commentsview->readComments();
						$template->set('commentsview',$commentsview);			
			
			
						$imgpath 		= $photos[$tmpprev?$tmpprev-1:$start]->getPhotoLink('fullsize');
						$img_thumb_path = $photos[$tmpprev?$tmpprev-1:$start]->getPhotoLink('thumbnail');
						
						
						if(strpos($imgpath,"/users/")){
							$imgpath_pos	=substr($imgpath,strpos($imgpath,'/users',1));
							$imginfo			= getimagesize($_SERVER['DOCUMENT_ROOT'].$imgpath_pos);
							$isfound = true;
						}else{
							$imgpath_pos	=substr($imgpath,strpos($imgpath,'/images/',1));
							$imginfo			= getimagesize($_SERVER['DOCUMENT_ROOT'].$imgpath_pos);
							$isfound = false;	
						}
						
						
						$template->set('imgwidth',$imginfo[0]);
						$template->set('imgheight',$imginfo[1]);	
						
						$template->set('imgpath',$imgpath);	
						$template->set('img_thumb_path',$img_thumb_path);
						$template->set('isfound',$isfound);	
						
						
						
			
			}
			
			
			//////////////////////////////////////////////
			//if context EQ travellog
			//check if currentphotoid is set as primary in Travel
			/////////////////////////////////////////////
			
			$travelprimaryphoto = false;
			
			if($_REQUEST['cat'] == "travellog"){
				require_once('travellog/model/Class.TravelLog.php');   
				require_once('travellog/model/Class.Travel.php');  
				
				$travellog 	= new TravelLog($_REQUEST['genID']);
				$travel 	= new Travel($travellog->getTravelID());
				
				$template->set('traveltitle',$travel->getTitle());	
				
				if($reccount){
					if($travel->getPrimaryphoto() == $photos[$tmpprev?$tmpprev-1:$start]->getphotoID()){
						$travelprimaryphoto = true;
					}
				}
			}
			
			/////////////////end//////////////////////////


			/***************** start get pokes recieved for photo ******************/
			/*if ($reccount){
				require_once 'travellog/views/Class.PokesView.php';
	
				if (!strcasecmp($this->request['context'], "travellog"))
					$recipient = $travellog-> getTraveler();
				else if (!strcasecmp($this->request['context'], "photoalbum")){
					$Photo = new Photo('group', $photos[$tmpprev?$tmpprev-1:$start]->getphotoID());
					$pContext = $Photo-> getContext();
					$recipient = $pContext-> getGroup();
				}
				else if (!strcasecmp($this->request['context'], "profile"))
					$recipient = new Traveler($this->request['genid']);
				else{
					$Group = GroupFactory::instance();
					$recipient = $Group-> create($this->request['genid']);
				}
	
				$pokesview = new PokesView();
				$Sender = new Traveler($_SESSION['travelerID']);
				if (!strcasecmp($this->request['context'], "profile"))
					$pokesview-> setContext('travelerprofile');
				else
					$pokesview-> setContext($this->request['context']);
				$pokesview-> setSection('photo');
				$pokesview-> setRecipient($recipient);
				$pokesview-> setGenID($photos[$tmpprev?$tmpprev-1:$start]->getphotoID());
				$pokesview-> setTemplatePath($this->template_path);
				(!strcasecmp("AdminGroup", get_class($recipient)))
					? $pokesview-> setUsername($recipient->getName())
					: $pokesview-> setUsername($recipient->getUsername());
				$pokesview-> setSender($Sender);
				$pokesview-> readPokes();
				$pokesview-> setHasAdded();
	
				$template-> set('pokesview', $pokesview);
				$template-> set('recipient', $recipient);
			}
*/
			/******************* end get pokes recieved for photo ********************/
			
			if ('photoalbum' == $_REQUEST['cat']) {
				$template->set('album', $Pfactory->getPHOTOcontext());
			}
			
			$template->set('context',$_REQUEST['cat']);	
			$template->set('genid',$_REQUEST['genID']);
			
			$template->set('travelprimaryphoto',$travelprimaryphoto);	
					  	
			$template->set('start',$start);	
			$template->set('endrow',$endrow);
			$template->set('pagenum',$pagenum);	
			$template->set('nextpage',$nextpage);
			$template->set('prevpage',$prevpage);
			$template->set('tmpprev',$tmpprev);						
			$template->set('photo',$photos);
			$template->set('reccount',$reccount);
			$template->set('recordno',$startrow." - ".$endrow. " of ".$reccount);
			$template->set('page',$page);
			$template->set('uploadphoto',"photomanagement.php?action=add&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID']);
			$template->set('rearrangephoto',"photomanagement.php?action=arrange&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID']);
			$template->set('owner',$owner);
			$template->set('edit_photolink',"photomanagement.php?cat=".$_REQUEST['cat']."&action=edit&genID=".$_REQUEST['genID']."&photoID=");
			$template->set('rotate_link',"photomanagement.php?cat=".$_REQUEST['cat']."&action=showrotate&genID=".$_REQUEST['genID']."&photoID=");
			
			$template->set('headercaption',$Pfactory->getHeaderCaption());
			$template->set('backlink',$Pfactory->getBackLink());	
			$template->set('backcaption',$Pfactory->getBackCaption());	
			$template->set('norecordlabel',$Pfactory->getNorecordlabel());
			
			
			if($reccount){
						$template->set('fullview',$template->fetch("tpl.IncPhotoFullview.php"));		
						$template->set('gallery',$template->fetch("tpl.IncPhotoGallery.php"));				
			}
			
			echo $template->fetch('tpl.incViewPhoto.php');
			
			
		 }
	
	
	
	function fullsizeview(){
			
			$imgpiccontainer = "piccontainer";
			
			$travelerID = isset($_REQUEST['travelerID'])?$_REQUEST['travelerID']:0;
			$offset 	= $_REQUEST['offset'];
			$endrow  	= $_REQUEST['endrow'];
			$pagenum	= $_REQUEST['pagenum'];
			
			$template = new Template();
		    $template->set_path($this->template_path);
		    
		  	require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		    
		    new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
		    $Pfactory = PhotoFactory::create();
		    
		    
		    $reccount 	= $Pfactory->getCOUNTPhotos();						  			
			$photos		= $Pfactory->getPhotos();
			$owner		= $Pfactory->isOwner($travelerID);	
			
			$nextpage 	=  NULL;
			$prevpage	=  NULL;
			
			if($offset + 1 ==$endrow){
				if($offset + 1 < $reccount){				
					$nextpage	= 'photo.page('.($endrow+1).','.($pagenum+1).',0)';
				}
			}else{
				$nextpage	= "photo.fullSizeView($offset+1,$endrow,$pagenum,'$imgpiccontainer')";
			}
			
			if(strpos($offset/12,'.')){
				$prevpage	= "photo.fullSizeView($offset-1,$endrow,$pagenum,'$imgpiccontainer')";
			}else{
				$prevpage	= 'photo.page('.(($offset-12)+1).','.($pagenum-1).','.($offset).')';
			}
						
			$file_factory = FileFactory::getInstance();			
			$commentsview = $file_factory->getClass('CommentsView');
			$commentsview->setContext(2,$photos[$offset]->getphotoID());
			$commentsview->setTravelerID($travelerID);
			//$commentsview->setPhotoCategory($this->request['context']);
			$commentsview->setgenID($_REQUEST['genID']);

			require_once("Class.HelperGlobal.php");
			$commentsview->setEnvURL("photomanagement.php?cat=".$_REQUEST['cat']."&action=vfullsize&genID=".$_REQUEST['genID']."&photoID=".$photos[$offset]->getphotoID()."^comment_form");
									   
			$commentsview->readComments();
			$template->set('commentsview',$commentsview);
			
			
			$imgpath 	= $photos[$offset]->getPhotoLink('fullsize');
			
			$img_thumb_path = $photos[$offset]->getPhotoLink('thumbnail');
			
			
			if(strpos($imgpath,"/users/")){
				$imgpath_pos	=substr($imgpath,strpos($imgpath,'/users',1));
				$imginfo		= getimagesize($_SERVER['DOCUMENT_ROOT'].$imgpath_pos);
				$isfound = true;	
			}else{
				$imgpath_pos	=substr($imgpath,strpos($imgpath,'/images/',1));
				$imginfo		= getimagesize($_SERVER['DOCUMENT_ROOT'].$imgpath_pos);	
				$isfound = false;	
			}
			
			//////////////////////////////////////////////
			//if context EQ travellog
			//check if currentphotoid is set as primary in Travel
			/////////////////////////////////////////////
			
			$travelprimaryphoto = false;
			
			if($_REQUEST['cat'] == "travellog"){
				require_once('travellog/model/Class.TravelLog.php');   
				require_once('travellog/model/Class.Travel.php');  
				
				$travellog 	= new TravelLog($_REQUEST['genID']);
				$travel 	= new Travel($travellog->getTravelID());
				
				$template->set('traveltitle',$travel->getTitle());	
				
				if($travel->getPrimaryphoto() == $photos[$offset]->getphotoID()){
					$travelprimaryphoto = true;
				}
				
			}
			
			/////////////////end//////////////////////////


			/***************** start get pokes recieved for photo ******************/
/*
			require_once 'travellog/views/Class.PokesView.php';

			if (!strcasecmp($this->request['context'], "travellog"))
				$recipient = $travellog-> getTraveler();
			else if (!strcasecmp($this->request['context'], "photoalbum")){
				$Photo = new Photo('group', $photos[$offset]->getphotoID());
				$pContext = $Photo-> getContext();
				$recipient = $pContext-> getGroup();
			}
			else if (!strcasecmp($this->request['context'], "profile"))
				$recipient = new Traveler($this->request['genid']);
			else{
				$Group = GroupFactory::instance();
				$recipient = $Group-> create($this->request['genid']);
			}

			$pokesview = new PokesView();
			$Sender = new Traveler($_SESSION['travelerID']);
			if (!strcasecmp($this->request['context'], "profile"))
				$pokesview-> setContext('travelerprofile');
			else
				$pokesview-> setContext($this->request['context']);
			$pokesview-> setSection('photo');
			$pokesview-> setRecipient($recipient);
			$pokesview-> setGenID($photos[$offset]->getphotoID());
			$pokesview-> setTemplatePath($this->template_path);
			(!strcasecmp("AdminGroup", get_class($recipient)))
				? $pokesview-> setUsername($recipient->getName())
				: $pokesview-> setUsername($recipient->getUsername());
			$pokesview-> setSender($Sender);
			$pokesview-> readPokes();
			$pokesview-> setHasAdded();

			$template-> set('pokesview', $pokesview);
			$template-> set('recipient', $recipient);
*/
			/******************* end get pokes recieved for photo ********************/

			$template->set_vars(array(							
							'imgwidth'			=>$imginfo[0],
							'imgheight'			=>$imginfo[1],
							'imgpath'			=>$imgpath,	
							'img_thumb_path'	=>$img_thumb_path,
							'isfound'			=>$isfound,
							'photo' 			=>$photos,
							'context'			=>$_REQUEST['cat'],	
							'genid'				=>$_REQUEST['genID'],	
							'travelprimaryphoto' =>$travelprimaryphoto,
							'start'				=>$offset,
							'tmpprev'			=>NULL,
							'pagenum'			=>$pagenum,	
							'endrow'			=>$endrow,
							'nextpage'			=>$nextpage,
							'prevpage'			=>$prevpage,	
							'reccount'			=>$reccount,
							'owner'				=>$owner,
							'backlink'			=>$Pfactory->getBackLink(),
							'norecordlabel'		=>$Pfactory->getNorecordlabel(),
							'backcaption'		=>$Pfactory->getBackCaption(),
							'uploadphoto'		=>"photomanagement.php?action=add&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID'],
							'rearrangephoto'=>"photomanagement.php?action=view&arrange&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID'],
							'edit_photolink'	=>"photomanagement.php?cat=".$_REQUEST['cat']."&action=edit&genID=".$_REQUEST['genID']."&photoID=",
							'rotate_link'		=>"photomanagement.php?cat=".$_REQUEST['cat']."&action=showrotate&genID=".$_REQUEST['genID']."&photoID="
							));
							
			echo $template->fetch('tpl.IncPhotoFullview.php');
			
			
		
	}
	
	
	function setAsPrimary($photoID){
			require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
			
			$Pfactory = new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
			PhotoManager::setASPrimary($Pfactory,$photoID);
			$this->fullsizeview();
	}
	
	function setAsTravelPrimary($photoID){
			global $cp;
			require_once('travellog/model/Class.TravelLog.php');   
			require_once('travellog/model/Class.Travel.php');  
				
			$travellog 	= new TravelLog($_REQUEST['genID']);
			$travel 	= new Travel($travellog->getTravelID());
			$travel->setPrimaryphoto($photoID);
			$travel->update();
			
			$this->fullsizeview();
			
	}
	
		
	function Delete($photoID){
			require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
			
			$Pfactory = new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
			PhotoManager::delete($Pfactory,$photoID);
			if(isset($_REQUEST['photoid']))
				$this->getPhotos();
			else
				header("Location:".$_SERVER['HTTP_REFERER']);
	}
	
	function showedit($photoID){
			require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			require_once('travellog/model/Class.Photo.php');
			
			$Pfactory = new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
		    $photo 	= new Photo($Pfactory->create()->getPHOTOcontext(),$photoID);
			
			$template = new Template();
		    $template->set_path($this->template_path);
			
			$template->set_vars(array(
							'error'		=>NULL,
							'action'		=>"photomanagement.php?cat=".$_REQUEST['cat']."&action=update&genID=".$_REQUEST['genID']."&photoID=".$photoID,
							'photoID'	=> $photo->getPhotoID(),
							'offset'	=>$_REQUEST['offset'],
							'endrow'	=>$_REQUEST['endrow'],
							'pagenum'	=>$_REQUEST['pagenum'],
							'context'	=>$_REQUEST['cat'],
							'loginid'	=>$_REQUEST['loginID'],
							'genid'		=>$_REQUEST['genID'],
							'caption'	=> $photo->getCaption()));
											
			echo $template->fetch('tpl.EditPhotoCaption.php');			
	}
	
	function Update($photoID){
			require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
			$Pfactory = new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
			PhotoManager::updatecaption($Pfactory,$photoID,$_REQUEST['txtcaption']);
			$this->fullsizeview();
	}
	
	
	function Rotate($photoID,$angle){
			require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
			
			$Pfactory = new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
			PhotoManager::rotate($Pfactory,$photoID,$angle);
			$photo 	= new Photo($Pfactory->create()->getPHOTOcontext(),$photoID);
			echo  '<img id="imgbox" src="'.$photo->getPhotoLink().'"/>';	
			
	}
	
	function showthumbversion($photoID){
			require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			require_once('travellog/model/Class.Photo.php');
			
			$Pfactory = new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
		    $photo 	= new Photo($Pfactory->create()->getPHOTOcontext(),$photoID);
			
			if(!file_exists($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."cropthumb".$photo->getFileName())){
				
				require_once('travellog/model/Class.ImageBuilder.php');
				
				$ImgBuilder = ImageBuilder::getInstance();													
				$ImgBuilder->setFilename($photo->getFilename());
				$ImgBuilder->setImageFile($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."fullsize".$photo->getFilename());
				$ImgBuilder->setDestination($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto());
				$ImgBuilder->create('cropthumb',100,100);
				
			}
			
			
			list($cwidth, $cheight) = getimagesize($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."cropthumb".$photo->getFilename());
			$imgdimension = array('width'=>$cwidth,'height'=>$cheight);
			
			
			$template = new Template();
		    $template->set_path($this->template_path);
			
			$template->set_vars(array(
							'error'		=>NULL,
							'action'		=>"photomanagement.php?cat=".$_REQUEST['cat']."&action=update&genID=".$_REQUEST['genID']."&photoID=".$photoID,
							'photoID'	=> $photo->getPhotoID(),
							'offset'	=>$_REQUEST['offset'],
							'endrow'	=>$_REQUEST['endrow'],
							'pagenum'	=>$_REQUEST['pagenum'],
							'context'	=>$_REQUEST['cat'],
							'loginid'	=>$_REQUEST['loginID'],
							'genid'		=>$_REQUEST['genID'],
							'imgdimension' =>$imgdimension,
							'photo'	=> $photo));
											
			echo $template->fetch('tpl.thumbversion.php');			
	}
	
	function crop(){
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
		
		
		$Pfactory = new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
		PhotoManager::crop($Pfactory,$_REQUEST['photoid'], $_REQUEST['cropWidth'], $_REQUEST['cropHeight'], $_REQUEST['cropX'], $_REQUEST['cropY']);
		
		//$this->getPhotos();
			
	}
	
	
	
	
}
	
	


?>