<?php
/*
 * Created on Oct 10, 2007
 * Class.ViewCommentRequest.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
class ViewCommentRequest{
	
	function ViewCommentRequest($request){
		require_once('travellog/views/Class.CommentsView.php');

		$contextid = $request['contextID'];

		$commentsview = new CommentsView();
		$commentsview->setContext($request['context'],$contextid);
		
		//photo category used only for comments on photo
		if(array_key_exists('photocategory',$request)){
			$commentsview->setPhotoCategory($request['photocategory']);
			$commentsview->setgenID($request['genid']);
		}

		$commentsview->setTravelerID($request['loginid']);
		$commentsview->readComments();

 		echo $commentsview->renderAjax();
	}
}
?>
