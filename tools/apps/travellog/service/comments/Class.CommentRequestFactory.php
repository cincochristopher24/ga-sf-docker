<?php

require_once('travellog/model/Class.Comment.php');
require_once("travellog/model/Class.SessionManager.php");

class CommentRequestFactory{

	public static function create($request){
		if (!CommentRequestFactory::isValidActionDoer($request)) {
			require_once('travellog/service/comments/Class.ViewCommentRequest.php');
			echo "<p>Sorry, for interrupting your actions. We have encounter a serious threat.";
			exit;
		}
		
		switch ($request['action']){
			case "view":
				require_once('travellog/service/comments/Class.ViewCommentRequest.php');
				return new ViewCommentRequest($request);
			break;
			case "showedit":
				require_once('travellog/service/comments/Class.ShowEditCommentRequest.php');
				return new ShowEditCommentRequest($request);
			break;
			case "saveedit":
				require_once('travellog/service/comments/Class.SaveEditCommentRequest.php');
				return new SaveEditCommentRequest($request);
			break;
			case "delete":
				require_once('travellog/service/comments/Class.DeleteCommentRequest.php');
				return new DeleteCommentRequest($request);
			break;
					
		}
	}
	
	/**
	 * Checks if the current logged user has a privilege to do the action 
	 * he/she wants to do. For actions, showing the edit form, saving the edits or 
	 * deleting the comment, this returns true when the current logged user is the
	 * author or the owner of the given comment. Returns false, otherwise.
	 * 
	 * @return boolean
	 */
	public static function isValidActionDoer($request){
		switch ($request['action']){
			case "view":
			  return true;
			default:
				$comment = new Comment($request['commentid']);
				$session = SessionManager::getInstance();
				return ($session->get('travelerID') == $comment->getAuthor() OR $comment->getContext()->hasActionPrivilege($session->get('travelerID'))) ? true : false; 	
		}
	}
		
}

?>
