<?php
/*
 * Created on Oct 10, 2007
 * Class.ShowEditCommentRequest.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
class ShowEditCommentRequest{
	
	function ShowEditCommentRequest($request){
		
		require_once('travellog/model/Class.Comment.php');
		require_once('Class.Template.php');	
		
		$comment = new Comment($request['commentid']);
 	 		
 		$template = new Template();
		$template->set_path('travellog/views/');	
 		
 		$photocat 	= NULL; //if photo
 		$photogenid	= 0; //photocatid ex: profileid, travellogid
 		
 		//photo category used only for comments on photo
		if(array_key_exists('photocategory',$request)){
			$photocat	= $request['photocategory'];
			$photogenid = $request['genid'];
		}
 															
 		$template->set("context",$request['context']);
 		$template->set("contextid",$request['contextID']); 
 		$template->set("commentid",$comment->getCommentID());						// for edits the contextID will be the commentID 
 		$template->set("comment",$comment->getTheText());
 		
 		$template->set("photocat",$photocat); 
 		$template->set("genid",$photogenid);
 		$template->set("loginid",$request['loginid']);  
		
		echo $template->fetch('tpl.EditAjaxComment.php');
	}
	
} 
 
?>
