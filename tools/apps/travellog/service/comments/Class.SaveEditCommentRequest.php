<?php
/*
 * Created on Oct 10, 2007
 * Class.SaveEditCommentRequest.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
class SaveEditCommentRequest{
	
	function SaveEditCommentRequest($request){
		
		echo "<pre>";
		
			//print_r($request);
		
		echo "</pre>";
		
		$comment = $request['comment']; 		
 		//$comment = ereg_replace('[\<\>]','&nbsp;',$comment);    // prevent scripts to be entered
		$comment = preg_replace('/[\<\>]/','&nbsp;',$comment);    // prevent scripts to be entered
 		$comment = htmlspecialchars($comment);
 		$contextID = $request['commentid'];
 		$type		=	$request['context'];	
 		
 		require_once('travellog/model/Class.Comment.php');	
 		
 		$commentObj = new Comment($contextID);
 		$commentObj->setTheText($comment);
 		$commentObj->setTheDate(date('Y-m-d G:i'));
 		$commentObj->Update();	
 		
 		// invalidate cached views(html) of comments
 		require_once('Cache/ganetCacheProvider.php');
 		$cache	=	ganetCacheProvider::instance()->getCache();
 		$key 	=	'CommentsView_hashes_' . $type . '_' . $commentObj->getContextId();

 		if ($cache != null){
 			$hashKeys	=	$cache->get($key);
 			if ($hashKeys){
 				// found array of keys to invalidate
 				foreach ($hashKeys as $hashKey){
 					$cache->delete($hashKey);
 				}
 				
 				$cache->delete($key);
 			} 
 		}
		
		require_once('travellog/service/comments/Class.ViewCommentRequest.php');
		return new ViewCommentRequest($request);
	}
	
} 
 
?>
