<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.PersonalInfoMapper.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.AddressInfoMapper.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.SchoolInfoMapper.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.AdditionalInfoMapper.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramInformationMapper.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.PersonalStatementMapper.php');

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OnlineAppToStudentDBMapper.php');

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.USUniversitiesPeer.php');

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.HousingOptionsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.DurationOptionsPeer.php');

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');

require_once('travellog/model/onlineApplications/CIS_onlineApplication/LoginLogger/Class.HQEditLogger.php');

/**
TODO: edit function executeToHQ when we have the landing page in hq
 **/ 

class gaCISOnlineAppWebService {
	// 'online_app_id', 'web_app_key',
	protected $accepted_params = array(	
										'first_name', 'last_name', 'preferred_name', 'gender', 'date_of_birth', 'primary_email', 'secondary_email', 
										'student_home_address_street_address', 'student_home_address_city', 'student_home_address_state', 
										'student_home_address_zip', 'phone_number', 'cell_phone',
										'university', 'field_of_study', 'current_year_in_school', 'gpa', 'inquiry_related_inquiry_source', 'learnfrom_others',
										'term', 'year', 'program', 'option', 'duration', 'housing', 'start_month', 'start_day_month', 'full_year_interest',
										'cis_custom_academic_or_career_goals', 'cis_custom_cultural_understanding', 'cis_custom_personal_growth', 'shirt_size'
										);
								
	protected $info_params = array(	'PersonalInfo'	=>	array(	'first_name' 		=> 'setFirstName',
																'last_name' 			=> 'setLastName',
																'preferred_name' 	=> 'setPreferredName',
																'gender' 			=> 'setGender',
																'date_of_birth' 		=> 'setBirthday',
																'primary_email' 			=> 'setEmail',
																'secondary_email' 		=> 'setAltEmail',
																'shirt_size'           => 'setShirtSize'
															),
									'AddressInfo' 	=>	array(	'student_home_address_street_address' 			=> 'setStreet1',
																'student_home_address_city' 				=> 'setCity1',
																'student_home_address_state' 			=> 'setState1',
																'student_home_address_zip' 				=> 'setZip1',
																'phone_number' 			=> 'setPhoneNumber1',
																'cell_phone' 		=> 'setCellphone'
															),
									'SchoolInfo'	=>	array(	'university' 		 => '',
																'field_of_study' 	 => 'setMajor',
																'current_year_in_school' 	 => 'setYearInSchool',
																'gpa' 				 => 'setGenAverage'
															),
									'AdditionalInfo' =>	array(	'inquiry_related_inquiry_source' 		 => 'setLearnProgramFrom',
																'learnfrom_others' 	 => 'setOthers'
															),
									'ProgramInformation' => array('inquiry_related_term' 			 => 'setTerm',
																'inquiry_related_year_abroad' 				 => 'setYear',
																//'cis_custom_program' 			 => 'setFirstProgramID',
																'program_selection_program_name' => 'setFirstProgramID',
																'cis_custom_program_option' 			 => 'setFirstOptionID',
																'cis_custom_duration_option'   		 => 'setDurationOptionID',
																'cis_custom_housing_option' 			 => 'setHousingOptionID', 
																'cis_custom_internship_start_month' 		 => 'setStartMonth',
																'cis_custom_day_to_start_internship' 	 => 'setStartDay',
																'full_year_interest' => 'setInterest'
															),
									'PersonalStatement'	=> array('cis_custom_academic_or_career_goals'		 => 'setStatement1',
																'cis_custom_cultural_understanding'		 => 'setStatement2',
																'cis_custom_personal_growth'		 => 'setStatement3'
															)
									);
 	protected $step_objects = array('step_1' => array('PersonalInfo', 'AddressInfo', 'SchoolInfo', 'AdditionalInfo'),
									'step_2' => array('ProgramInformation'),
									'step_3' => array('PersonalStatement'),
									'step_4' => array(),
									'step_5' => array(),
									'step_6' => array()
									);

	protected $objects = array();
	protected $objects_update_method = array();
	protected $online_app_id = null;
	
	public $errors = array();	
	public static $instance = null;
	
	public static function getInstance(){
		if( is_null(self::$instance) ){
			self::$instance = new self();
		}
		
		return self::$instance;
	}

	private function __construct(){}
	
	/**
	 functions for receiving data from HQ
	 **/ 
	public function processUpdateFromHQ(array $params){
		
		if(!isset($params['student_id']) OR $params['student_id'] == 0){
			$error = array("status" => "ERROR", "msg" => "The key 'student_id' was not set in the post parameters OR it has a 0 value!");
			echo json_encode($error);
			exit;
		}
		
		$this->online_app_id = $this->getAppUserID($params['student_id']);//$params['online_app_id'];
		//echo "hoy!"; exit;
		if(is_null($this->online_app_id)){
			$error = array("status" => "ERROR", "msg" => "Could not continue updating online app as there is no record with student_id = ".$params['student_id']."!");
			echo json_encode($error);
			exit;
		}
	//	echo "<pre>";
	//	var_dump($params);
	//	echo $this->online_app_id;
	//	echo "<br/><br/>";
		$refinedParams = $this->refineParams($params);
	//	var_dump($refinedParams); //exit;
		foreach($refinedParams as $p_key => $p_val){
			foreach($this->info_params as $info_key => $info_val){
				if(array_key_exists($p_key, $info_val) /*AND $p_val != ""*/){
					$method = $info_val[$p_key];
					$this->setObject($info_key, $method, $p_val);
				}
			}
		}
		
	//	exit;
	//	var_dump($this->objects); exit;
		$this->executeSaveObjects();
		
		$this->logProcess($params, 'updates from HQ');
	}
	
	private function lookUpSchoolName($params){
		$schoolName = $params['university'];
		$university = USUniversitiesPeer::retrieveByName($schoolName);
		if(!is_null($university)){
			$tmp_info_param = array('schoolID' => 'setSchoolID', 'other_university' => 'setOtherSchoolName');
			$this->info_params['SchoolInfo'] = array_merge($tmp_info_param, $this->info_params['SchoolInfo']);
			$params['schoolID'] = $university->getID();
			$params['other_university'] = "";
		}
		else {
			$tmp_info_param = array('schoolID' => 'setSchoolID', 'other_university' => 'setOtherSchoolName');
			$this->info_params['SchoolInfo'] = array_merge($tmp_info_param, $this->info_params['SchoolInfo']);
			$params['schoolID'] = 0;
			$params['other_university'] = $params['university'];
		}

		unset($params['university']);
		unset($this->info_params['SchoolInfo']['university']);
		
		return $params;
	}
	
	private function setObject($object_name, $method, $value){
		
		if(array_key_exists($object_name, $this->objects)){
			$obj = $this->objects[$object_name];
			$obj->{$method}($value);
		}
		else {
		//	$obj = null;
		//	if(!is_null($this->online_app_id)){
				$mapper_method = ($object_name == "AddressInfo") ? "retrieveByPersonalInfoIDWithType" : "retrieveByPersonalInfoID";
				$obj = call_user_func_array($object_name."Mapper::".$mapper_method, array($this->online_app_id));
				$update_method = 'update';
				
		//	}
			
			if(is_null($obj)){
				$obj = new $object_name;
				
			//	if(!is_null($this->online_app_id)){
					$obj->setPersonalInfoID($this->online_app_id);
			//	}
				$update_method = 'save';
			}
			
			
			$obj->{$method}($value);
			$this->objects[$object_name] = $obj;
			$this->objects_update_method[$object_name."_method"] = $update_method;
		}
		
	}
	
	private function executeSaveObjects(){
		foreach($this->objects as $key => $obj){
			$method = $this->objects_update_method[$key."_method"];
			$obj->{$method}();
		}
		
		
	}
	/**
	 END: functions for receiving data from HQ
	 **/
	
	/**
	 functions for sending data to HQ
	 **/
	public function postToGaHQ(array $params){
	//	echo "<pre>";
		$this->prepareObjects($params);
	//	var_dump($this->objects); exit;
		if(count($this->objects)){
			$postData = $this->preparePostData();
		//	var_dump($postData);
			$refinedPostData = $this->refinePostData($postData);
	//		var_dump($refinedPostData);
			$this->executePostToHQ($refinedPostData);
		}
		
	}
	
	private function prepareObjects(array $params){
		//echo '<br/>prepareObjects';
		//var_dump($params);
		$step = 'step_'.$params['step'];
		$this->online_app_id = $params['pID'];
		$object_names = $this->step_objects[$step];
		foreach($object_names as $obj_name){
			$mapper_method = ($obj_name == "AddressInfo") ? "retrieveByPersonalInfoIDWithType" : "retrieveByPersonalInfoID";
			$obj = call_user_func_array($obj_name."Mapper::".$mapper_method, array($this->online_app_id));
			
			if(!is_null($obj)){
				$this->objects[$obj_name] = $obj;
			}
			
		}
	
	}
	
	private function preparePostData(){
		$postData = array();
		foreach($this->objects as $key => $obj){
			$tmp_postData = array();
			foreach($this->info_params[$key] as $ip_key => $ip_val){
				if($ip_key == 'university'){
					$method_name = 'getSchoolName';
				}
				else {
					$method_name = 'get'.substr($ip_val, 3, strlen($ip_val));
				}
				
				$tmp_postData[$ip_key] = $obj->{$method_name}();
				
			}
			
			$postData = array_merge($tmp_postData,$postData);
		}
		
		// add student id to postData
		$studentID = $this->getStudentID($this->online_app_id);
		if(!is_null($studentID)){
			$postData = array_merge(array('student_id' => $studentID), $postData);
		}
		
		return $postData;
	}
	
	private function refinePostData(array $postData){
		foreach($postData as $key => $value){
			if($value == "" OR $value == "0" AND $key != "inquiry_related_inquiry_source"){
				unset($postData[$key]);
			}
		}
		
		$postData = $this->refineParams($postData, true);
		
		return $postData;
	}
	
	private function composeRequestURI(array $params){
		$req_uri = "";
		$cnt = 1;
		foreach($params as $key => $value){
			$req_uri .= $key.'='.urlencode($value);
			if(count($params) > $cnt){ $req_uri .= '&'; }
			$cnt++;
		}
		
		return $req_uri;
	}
	
	private function executePostToHQ(array $postData){
		$req_uri = $this->composeRequestURI($postData);
	//	$url = 'http://secure.goabroad.net.local/cis/myTest2.php';
	//	$url = 'http://secure.goabroad.net.local/cis/myTest2.php';
		if(isset($postData['student_id'])){
		//	$url = "http://jul.goabroadhq.com.local/websrvc/onlineapp/update";
			$url = "http://test.goabroadhq.com/websrvc/onlineapp/update";
		}
		else {
			$url = "http://test.goabroadhq.com/websrvc/onlineapp/create";
		}
		
		
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $req_uri);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
	//	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		
		$request_result = curl_exec($curl);
	//	$status = curl_getinfo($curl);
		curl_close($curl);
	//	var_dump($request_result); exit;
		$this->saveHQStudentID($request_result);
		
		$this->logProcess($postData, 'sent to HQ');
	}
	/**
	 END: functions for sending data to HQ
	 **/
	
	/**
	 utility functions
	 **/ 
	/**
	 * logger listener
	 **/ 
	private function logProcess(array $params, $action){
		HQEditLogger::getInstance()->saveProcess($params, $action);
	}
	
	/**
	 * $params - the curl response 
	 **/ 
	private function saveHQStudentID($response){
		$response = json_decode($response);
		// for some reasons i dont know, decoding once would just give me a string
		// not the expected json
		$response = json_decode($response); 
		//var_dump($response);
		$userID = PersonalInfoMapper::getUserIDByOnlineAppID($this->online_app_id);
		
	//	$obj = OnlineAppToStudentDBMapper::retrieveByOnlineAndStudentID($this->online_app_id, $response->studentID);
	//	$obj = OnlineAppToStudentDBMapper::retrieveByAppUserAndStudentID($userID, $response->student_id);
		$obj = OnlineAppToStudentDBMapper::retrieveByUserID($userID);
		if(is_null($obj)){
			$obj = new OnlineAppToStudentDB();
		//	$obj->setOnlineAppID($this->online_app_id);
		//	echo "<br/>student_id : ".$response->student_id;
			$obj->setOnlineAppUserID($userID);
			$obj->setStudentID($response->student_id);
			$obj->save();
		}
		
	}
	
	/**
	 * get student ID
	 **/
	private function getStudentID($onlineAppID = 0){
		
		$userID = PersonalInfoMapper::getUserIDByOnlineAppID($onlineAppID);
	//	echo $userID;
	//	$obj = OnlineAppToStudentDBMapper::retrieveByOnlineAppID($onlineAppID);
		$obj = OnlineAppToStudentDBMapper::retrieveByUserID($userID);
	//	var_dump($obj); exit;
		if(!is_null($obj)){
			return $obj->getStudentID();
		}
		return null;
	}
	/**
	 * get application ID
	 **/
	private function getAppUserID($studentID){
		$obj = OnlineAppToStudentDBMapper::retrieveByStudentID($studentID);
		if(!is_null($obj)){
			
			$application = PersonalInfoMapper::retrieveByUserID($obj->getOnlineAppUserID());
			if(!is_null($application)){
				return $application->getID();
			}
			
		}
		return null;
	}
	/**
	 * refine parameters to have readable values
	 **/
	private function refineParams(array $params, $postToHQ = false){
		if(isset($params['university']) and !$postToHQ){
			$params = $this->lookUpSchoolName($params);
		}
		
		//$static_method = ($postToHQ) ? 'retrieveByPk' : 'retrieveByName';
		//$getter_method = ($postToHQ) ? 'getName' : 'getID';
		$static_method = 'retrieveByPk';
		$getter_method = 'getID';
		
		if(isset($params['inquiry_related_term']) AND (int)$params['inquiry_related_term'] > 0){
			$term = call_user_func_array('TermsPeer::'.$static_method, array($params['inquiry_related_term']));
		//	$term = TermsPeer::retrieveByName($params['term']);
			if(!is_null($term)){
				$params['inquiry_related_term'] = $term->{$getter_method}();
			}
			else {
				unset($params['term']);
			}
			
		}
		
		// if(isset($params['cis_custom_program'])){
		// 	$program = call_user_func_array('ProgramsPeer::'.$static_method, array($params['cis_custom_program']));
		// 	if(!is_null($program)){
		// 		$params['cis_custom_program'] = $program->{$getter_method}();
		// 	}
		// 	else {
		// 		unset($params['cis_custom_program']);
		// 	}
		// }

		if(isset($params['program_selection_program_name']) AND (int)$params['program_selection_program_name'] > 0){
			$option = call_user_func_array('OptionsPeer::'.$static_method, array($params['program_selection_program_name']));
			if(!is_null($option)){
				$params['program_selection_program_name'] = $option->{$getter_method}();
			}
			else {
				unset($params['program_selection_program_name']);
			}
		}

		
		if(isset($params['cis_custom_program_option']) AND (int)$params['cis_custom_program_option'] > 0){
			$option = call_user_func_array('OptionsPeer::'.$static_method, array($params['cis_custom_program_option']));
			if(!is_null($option)){
				$params['cis_custom_program_option'] = $option->{$getter_method}();
			}
			else {
				unset($params['cis_custom_program_option']);
			}
		}
		
		if(isset($params['cis_custom_duration_option']) AND (int)$params['cis_custom_duration_option'] > 0){
			$duration = call_user_func_array('DurationOptionsPeer::'.$static_method, array($params['cis_custom_duration_option']));
		//	$duration = DurationOptionsPeer::retrieveByName($params['duration']);
			if(!is_null($duration)){
				$params['cis_custom_duration_option'] = $duration->{$getter_method}();
			}
			else {
				unset($params['cis_custom_duration_option']);
			}
		}
		
		if(isset($params['cis_custom_housing_option']) AND (int)$params['cis_custom_housing_option'] > 0){
			$housing = call_user_func_array('HousingOptionsPeer::'.$static_method, array($params['cis_custom_housing_option']));
		//	$housing = HousingOptionsPeer::retrieveByName($params['housing']);
			if(!is_null($housing)){
				$params['cis_custom_housing_option'] = $housing->{$getter_method}();
			}
			else {
				unset($params['cis_custom_housing_option']);
			}
		}
		
		if(isset($params['inquiry_related_inquiry_source'])){
			$learnfrom = CISOnlineAppConst::$addInfoOptKeys;
			if($postToHQ){
				$learn_val = $learnfrom[$params['inquiry_related_inquiry_source']];
				$params['inquiry_related_inquiry_source'] = $learn_val;
			}
			else {
				$key = array_keys($learnfrom, $params['inquiry_related_inquiry_source']);
				$learn_key = count($key) ? $key[0] : 0;
				if(!is_null($learn_key) AND (int)$learn_key > 0){
					$params['inquiry_related_inquiry_source'] = $learn_key;
					$params['learnfrom_others'] = "";
				}
			}
		}
		
		if(isset($params['gender'])){
		/*	$gender_values = array('1' => 'Male', '2' => 'Female');
			if($postToHQ){
				$gender = $gender_values[$params['gender']];
			}
			else {
				$key = array_keys($gender_values, $params['gender']);
				$gender = count($key) ? $key[0] : ((int)$params['gender']) ? $params['gender'] : 1;
			}
			
			$params['gender'] = $gender;*/
		}
		
		if(isset($params['current_year_in_school'])){
			$yrInSchool = array('1' => 'Freshman', '2' => 'Sophomore', '3' => 'Junior', 
								'4' => 'Senior', '5' => 'College/University Graduate', '6' => 'High School Graduate');
			if($postToHQ){
				$year_in_school = $yrInSchool[$params['current_year_in_school']];
			}
			else {
				$key = array_keys($yrInSchool, $params['current_year_in_school']);
				$year_in_school = count($key) ? $key[0] : 1;
			}
			
			$params['current_year_in_school'] = $year_in_school;
			
		}
		
		if(isset($params['date_of_birth'])){
			// format must be Y-m-d
			// if($postToHQ){
			// 	$bday = date('Y-m-d', strtotime($params['date_of_birth']));
			// }
			// else {
			// 	$bday = date('Y-m-d', strtotime($params['date_of_birth']));
			// }
			
			$bday = date('Y-m-d', strtotime($params['date_of_birth']));
			$params['date_of_birth'] = $bday;
		}
		
		return $params;
	}
	
	/**
	 * function to validate the POST parameters
	 **/ 
	private function validateParams(array $params){
		foreach($params as $key => $value){
			if(!in_array($key,$this->accepted_params)){
				$this->errors[] = array($key => "Index $key could not be accepted! Rejecting request!");
			}
		}
	}
	
}