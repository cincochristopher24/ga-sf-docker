<?php
require_once("travellog/model/Class.SessionManager.php");
require_once("Class.ToolMan.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");
//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
abstract class AbstractTravelerEventController implements IController{
	
	protected static $REQUIRES_ACTION_RULES = array('ADD', 'SAVE', 'EDIT', 'DELETE', 'REMOVE', 'MYJOURNALS', 'VIEW' );
	
	protected 
	
	$obj_session  = NULL,
	 
	$file_factory = NULL;
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance(); 
		$this->file_factory = FileFactory::getInstance(); 
		
		$this->file_factory->registerClass('GroupEventController', array('path' => 'travellog/controller/', 'file' => 'GANETGroupEventController'));
		$this->file_factory->registerClass('ActivityController', array('path' => 'travellog/controller/'  , 'file' => 'GANETActivityController'));
		$this->file_factory->registerClass('TravelerEvent');
		$this->file_factory->registerClass('Calendar');
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('GroupFactory');
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('Paging'         , array('path' => ''));
		$this->file_factory->registerClass('ObjectIterator' , array('path' => '')); 
		$this->file_factory->registerClass('Template'       , array('path' => ''));
		$this->file_factory->registerClass('HtmlHelpers'    , array('path' => ''));
		$this->file_factory->registerClass('GaDateTime'       , array('path' => ''));
		
		/**************************************************************************
		 * comments made by neri: 11-07-08
		 * this is to solve the error encountered upon clicking the calendar tab
		 * updated the class registered for displaying the profile component 
		 **************************************************************************/
		
		/*$this->file_factory->registerClass( 'ComponentProfileController' , array('path' => 'travellog/components/profile/controller/') );
		$this->file_factory->registerClass( 'ComponentProfileView'       , array('path' => 'travellog/components/profile/views/')      );*/
		
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
				
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->registerTemplate('Calendar');
		//$this->file_factory->registerTemplate('FrmGroupEvent'); 
		$this->file_factory->registerTemplate('FrmTravelerEvent');
		$this->file_factory->registerTemplate('Event');
		$this->file_factory->registerTemplate('TravelerCalendar');
		$this->file_factory->registerTemplate('TravelerEventLists');
		$this->file_factory->registerTemplate('TravelerEventContent');
		
		
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
	}
	
	function performAction(){
		$this->beforePerformAction();
		switch( strtolower($this->data['action']) ){
			case "modify":
			case "create":
				$this->__getData();
				$this->_viewAddForm();
			break;
			
			case "save":
				$this->__validateForm();
				$this->_save();
			break;
			
			case "travelereventlists":
				$this->_viewTravelerEventLists();
			break;
			
			case "eventdescription":
				$this->_viewEntryDescription();
			break;
			
			case "remove":
				$this->_remove();
			break;
			
			case "delete":
				$this->_delete();
			break;
			
			case "edit":
			case "add":
			default:
				$this->_viewTravelerEvents();
		}
	}	
	
	protected function _save(){
		if( $this->data['eID'] ){
			$obj_event = $this->file_factory->getClass('TravelerEvent', array($this->data['eID']));
			$obj_event->setTitle         ( $this->data['title']       );
			$obj_event->setTheDate       ( $this->data['event_date'] . ' ' . $this->data['event_time']);			
			$obj_event->setDescription   ( $this->data['description'] );
			$obj_event->setDisplayPublic ( $this->data['display']     ); 
			$obj_event->setTimeZoneID	 ( $this->data['timezone']	  );
			$obj_event->Update();
		}
		else{
			$obj_event = $this->file_factory->getClass('TravelerEvent');
			$obj_event->setTitle         ( $this->data['title']                  );
			$obj_event->setTheDate       ( $this->data['event_date'] . ' ' . $this->data['event_time']);			
			$obj_event->setTravelerID    ( $this->obj_session->get('travelerID') );
			$obj_event->setDescription   ( $this->data['description']            );
			$obj_event->setDisplayPublic ( $this->data['display']                ); 
			$obj_event->setTimeZoneID	 ( $this->data['timezone']	  			 );
			$obj_event->Create();
		}	
	}     
	
	protected function _viewAddForm(){
		$obj_template              = $this->file_factory->getClass('Template');
		$obj_inc_template          = $this->file_factory->getClass('Template');
		
		// added by chris - Jan. 22, 2009
		require_once('Class.TimeZone.php');
		$this->data['groupedTimezones'] = TimeZone::getGrouped();
		
		$obj_inc_template->set('props', $this->data);
		$obj_template->set('form' , $obj_inc_template->fetch($this->file_factory->getTemplate('FrmTravelerEvent')) );
		$obj_template->set('props', $this->data);
		
		Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
		
		echo $obj_template->fetch( $this->file_factory->getTemplate('Event') ); 
	}
	
	protected function _delete(){
		$obj_event = $this->file_factory->getClass('TravelerEvent', array($this->data['eID']));
		$obj_event->Delete();
	}
	
	protected function _remove(){
		$this->_delete();   
		$link = $this->data['from_where'];
		ToolMan::redirect("/$link.php");     
		exit; 
	}
	
	protected function _viewEntryDescription(){
		$obj_template                  = $this->file_factory->getClass('Template');
		$this->data['obj_event']       = $this->file_factory->getClass('TravelerEvent', array($this->data['eID']));
		$this->data['tID']             = $this->data['obj_event']->getTravelerID(); 
		//$this->data['is_owner']        = ( $this->obj_session->get('travelerID') == $this->data['tID'] )? true: false;
		$this->data['is_owner']        = ( $this->obj_session->get('travelerID') == $this->data['tID'] )? true: false;  
		$this->data['is_login']        = $this->obj_session->getLogin(); 
		
		// convert time to local time
		require_once('Class.TimeZone.php');
		$converted = TimeZone::convertTimeToRemoteTime($this->data['obj_event']->getTheDate(), $this->data['obj_event']->getTimeZone(), $this->data['offset'], "%Y-%m-%d %H:%M:%S");
		$this->data['local_datetime'] = $converted['dateTime'];
		$this->data['local_timezone'] = $converted['timezone'];
		$obj_template->set( 'props', $this->data ); 
		
		echo $obj_template->fetch($this->file_factory->getTemplate('TravelerEventContent'));
	}
	
	protected function _viewTravelerEvents(){
		$obj_navigation           = $this->file_factory->getClass('SubNavigation');
		$obj_template             = $this->file_factory->getClass('Template');
		$this->data['event_type'] = 'Calendar';
		$this->data['tID']        = ( isset($this->data['tID']) )? $this->data['tID']: $this->obj_session->get('travelerID');
		
		$traveler = $this->file_factory->getClass('Traveler',array($this->data['tID']));
		if(is_object($traveler) && ($traveler->isSuspended()||$traveler->isDeactivated())){
			header("location: /travelers.php");
			exit;
		}
		
		/*****************************************************************************
		 * comment made by neri: 11-07-08
		 * updated the method invoking the initialization of the profile component
		 *****************************************************************************/
		
		//$profile_component        = $this->file_factory->getClass( 'ComponentProfileController' , array($this->data['tID']) );
		
		$profile_component 			= $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
		$profile_component->init($this->data['tID']);
		
		$obj_view                 = $profile_component->get_view();
		
		$obj_calendar             = $this->__getCalendar();
		
		$powerful = ( $this->obj_session->get('travelerID') == $this->data['tID'] )? 1: 0;  
		$this->data['powerful']	= $powerful;
			
		$obj_navigation->setContext('TRAVELER');
		$obj_navigation->setContextID( $this->data['tID'] );
		$obj_navigation->setLinkToHighlight('CALENDAR');
		$obj_template->set( 'traveler_event_lists', $this->__getTravelerEventLists($obj_calendar) );
		$obj_template->set( 'props'               , $this->data                                   );
		$obj_template->set( 'event_type'          , 'Calendar'                                    );
		$obj_template->set( 'subNavigation'       , $obj_navigation                               );	
		$obj_template->set( 'obj_view'            , $obj_view                                     ); 
		 
		$travelerID = $this->data['tID'];
		$action     = strtolower($this->data['action']); 
		$eventID    = $this->data['eID'];
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	jQuery(document).ready(function(){   
		tID     = $travelerID;
		eventID = $eventID;  
		action  = "$action";
		powerful = $powerful;
		subject = "TRAVELER";

		jQuery('#events_calendar li').observeEvents();
		jQuery("#loading").ajaxStart(function(){
		   jQuery(this).css('display', 'inline');
		   jQuery('#event_details #event_view').empty();
		});
		jQuery("#loading").ajaxStop(function(){
		   jQuery(this).css('display', 'none'); 
		});  
	});  
//]]>  
</script>
BOF;
		
		//Template::includeDependentJs( '/js/jquery-1.1.4.pack.js'     );
		
		//Template::includeDependentJs( '/js/date.js'                  );
		//Template::includeDependentJs( '/js/jquery.datePicker.js'     );
		//Template::includeDependentJs( '/js/jquery.travelerEvent.js'  );
		//Template::includeDependentJs( '/js/jquery.timeentry.js'  	 );
		//Template::includeDependentJs( "/js/localtime.js"			 );
		//Template::includeDependentJs( '/js/jquery.json-1.3.min.js'	 );
		
		Template::includeDependentJs( '/min/g=EventMainJs'			 );

		Template::includeDependent  ( $code                          ); 
		Template::includeDependentCss( '/min/f=css/styles.css'   		     ); 
		Template::setMainTemplateVar( 'page_location', $travelerID == $this->obj_session->get('travelerID') ? 'My Passport' : 'Travelers' );
		//Template::setMainTemplateVar( 'page_location', 'Groups' );
		if( isset($GLOBALS['CONFIG']) )
			Template::setMainTemplateVar('title', $GLOBALS['CONFIG']->getSiteName() . ' Events');
		else
			Template::setMainTemplateVar('title', 'Events'); 
		Template::setMainTemplate   ( $this->file_factory->getTemplate('LayoutMain') );
		
		$obj_template->out($this->file_factory->getTemplate('TravelerCalendar'));
	}
	
	protected function _viewTravelerEventLists(){
		$obj_calendar = $this->__getCalendar();
		echo $this->__getTravelerEventLists( $obj_calendar );
	}
	 
	protected function __getTravelerEventLists( Calendar $obj_calendar ){
		$rows_per_page          = 5;
		$obj_template           = $this->file_factory->getClass('Template');
		$this->data['is_login'] = $this->obj_session->getLogin(); 
		$this->data['is_owner'] = ( $this->data['tID'] == $this->obj_session->get('travelerID') )? true: false; 
		$this->data['page']     = ( isset($this->data['page']) )? $this->data['page']: 1;
		$this->data['eID']      = ( isset($this->data['eID']) )? $this->data['eID'] : 0;
		
		$startdate = (!$this->data['is_owner']) ? date('Y-m-d') : NULL;
		
		$col_events             = $this->__filter($obj_calendar->getEvents(NULL,$startdate));
		// $col_events             = $this->__filter($obj_calendar->getEvents());
		$this->data['event_count'] = count($col_events);
		if( $this->data['eID'] ) $this->data['page'] = $this->__checkPosition($col_events, $rows_per_page);
		
		if( count($col_events) ){  
			$obj_paging   = $this->file_factory->getClass( 'Paging', array(count($col_events),$this->data['page'], 'action=TravelerEventLists&amp;tID='.$this->data['tID'], $rows_per_page) );
			$obj_iterator = $this->file_factory->getClass( 'ObjectIterator', array($col_events, $obj_paging) );
			$obj_template->set( 'obj_iterator', $obj_iterator );
			$obj_template->set( 'obj_paging'  , $obj_paging   ); 
			$obj_paging->setOnclick('jQuery(this).nextNprev');
		}
		$obj_template->set( 'props', $this->data );   
		
		return $obj_template->fetch($this->file_factory->getTemplate('TravelerEventLists'));
	}
	
	protected function __filter($col_events){
		$temp_events = array();
		if( !$this->obj_session->getLogin() && count($col_events) ){
			foreach ( $col_events as $obj_event){
				if ( $obj_event->getDisplayPublic() == 1 ) $temp_events[] = $obj_event; 
			}
		}
		elseif( count($col_events) ){
			$temp_events = $col_events;
		}
		
		return $temp_events;
	}
	
	protected function __checkPosition( $col_events, $rowsperpage ){
		$page = 1;
		for($ctr=0; $ctr < count($col_events); $ctr++ ){
			if ( $col_events[$ctr]->getEventID() == $this->data['eID'] ){
				$page = ceil(($ctr+1)/$rowsperpage); 
				break;
			}
		}
		return $page; 
	}
	
	protected function __getCalendar(){
		$obj_traveler  = $this->file_factory->getClass('Traveler', array($this->data['tID']));
		$obj_calendar  = $this->file_factory->getClass('Calendar', array($obj_traveler));
		
		//$obj_calendar->setPrivacyContext( $obj_traveler1 );
		$obj_calendar->setOrder(Calendar::$DATE_DESC); 
		if( $this->obj_session->getLogin() ){ 
			$obj_traveler1 = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
			$obj_calendar->setPrivacyContext( $obj_traveler1 ); 
		}
		
		return $obj_calendar;
	}
	
	protected function __getData(){
		require_once('Class.GaDateTime.php');
		if( array_key_exists('eID', $this->data) && $this->data['eID'] && strtolower($this->data['action']) != 'save' ){
			$obj_event                 = $this->file_factory->getClass('TravelerEvent', array($this->data['eID']));
			$this->data['title']       = $obj_event->getTitle();
			$this->data['description'] = $obj_event->getDescription();
			$this->data['display']     = $obj_event->getDisplayPublic();
			$this->data['tID']         = $obj_event->getTravelerID(); 
			$this->data['event_date']  = $obj_event->getTheDate();
			$this->data['event_time']  = date('h:i A', strtotime($this->data['event_date']));
			$this->data['event_date']  = GaDateTime::dbDateFormat(strtotime($this->data['event_date']));
			$this->data['timezone']	   = $obj_event->getTimeZoneID();
			$this->data['errors']      = array();
			$this->data['mode']        = 'edit';
		}
		else{
			$this->data['eID']         = ( isset($this->data['eID'])         )? $this->data['eID']        : 0;
			$this->data['title']       = ( isset($this->data['title'])       )? $this->data['title']      : '';
			$this->data['description'] = ( isset($this->data['description']) )? $this->data['description']: '';
			$this->data['display']     = ( isset($this->data['display'])     )? $this->data['display']    : 1;
			$this->data['tID']         = ( isset($this->data['tID'])         )? $this->data['tID']        : $this->obj_session->get('travelerID');
			$this->data['timezone']    = ( isset($this->data['timezone'])    )? $this->data['timezone']   : 0;
			$this->data['event_date']  = ( isset($this->data['event_date'])  )? $this->data['event_date'] : GaDateTime::dbDateTimeFormat();
			$this->data['event_time']  = date('h:i A', strtotime( isset($this->data['event_time']) ? $this->data['event_time'] : $this->data['event_date'] ));
			$this->data['event_date']  = GaDateTime::dbDateFormat(strtotime($this->data['event_date']));
			$this->data['errors']      = array();
			$this->data['mode']        = 'add';
		}
	}
	
	protected function __applyRules(){ 
		if( in_array( strtoupper($this->data['action']), AbstractTravelerEventController::$REQUIRES_ACTION_RULES) ){
			// TODO: If some actions requires a more complex rules use a rule object or method instead
			switch( strtolower($this->data['action']) ){
				case 'add':	
					if( !$this->obj_session->getLogin() ){
						ToolMan::redirect('/group.php?gID='.$this->data['gID']);
						exit;
					}
				break; 
				
				case 'save':
					if( !isset($_POST['action']) ){	
						echo '<h2>Requires a post method.</h2>';
						exit;
					}	
				break;
				case 'remove':
					if( !$this->obj_session->getLogin() ){
						ToolMan::redirect('/index.php');
						exit;
					}
					if( !isset($this->data['eID']) ){
						ToolMan::redirect('/passport.php');
						exit; 
					}
					$obj_event = $this->file_factory->getClass('TravelerEvent', array($this->data['eID']));
					if(  $obj_event->getTravelerID() != $this->obj_session->get('travelerID') ){
						$link = $this->data['from_where'];  
						ToolMan::redirect('/'.$link.'.php'); 
						exit;
					}	
				break;
				
				case 'delete':	
				case 'edit':
					if( !isset($_POST['action']) ){	
						//echo '<h2>Requires a post method.</h2>';
						//exit;
					}
					if( !isset($this->data['eID']) ){
						echo '<h2>Invalid eventID!!!</h2>';
						exit; 
					}
					$obj_event = $this->file_factory->getClass('TravelerEvent', array($this->data['eID']));
					if(  $obj_event->getTravelerID() != $this->obj_session->get('travelerID') ){
						echo '<h2>You are not allowed to '. $this->data['action'] .' this event.</h2>';
						exit;
					}	
				case 'create':
					if( !$this->obj_session->getLogin() ){
						echo '<h2>You are not allowed to add.</h2>';
						exit;
					}
				break; 
				
				case 'view':
					if( !$this->obj_session->getLogin() &&  !isset($this->data['tID']) ){
						ToolMan::redirect('/travelers.php');    
						exit;
					}
				break;
			}
		}
	}
	
	protected function __validateForm(){
		$this->__getData(); 
		
		$errors = array();
		$date   = explode('-', $this->data['event_date']);
		if( !preg_match('/.+/', $this->data['title']) ){
			$errors[] = 'Title is a required field!';
		}
		if( !preg_match('/.+/', $this->data['description']) ){
			$errors[] = 'Description is a required field!';
		}
		if( !preg_match('/.+/', $this->data['event_date']) ){ 
			$errors[] = 'Date is a required field!';
		}
		elseif( !checkdate($date[1],$date[2], $date[0]) ){
			$errors[] = 'Date is invalid!';
		}
		
		$this->data['errors'] = $errors;
		
		if( count($this->data['errors']) ){   
			$this->_viewAddForm();
			exit;	
		}
	}
	
	protected function beforePerformAction(){ 
		/**
		if( $this->obj_session->getLogin() ){
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($this->obj_session->get('travelerID'),'TRAVELER');  
		}
		**/
	}
} 
?>