<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	
	abstract class AbstractOldGroupMembersController implements iController{
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,

		$isLogged	  = false,

		$isMemberLogged = false,

		$isAdminLogged = false,

		$group 			= NULL,
		
		$traveler		= NULL,
		
		$subNav		=	NULL,
		
		$page		=	1 ,
		
		$tpl		=	NULL;
	
		function __construct(){
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('GroupMembersPanel');
			$this->file_factory->registerClass('SubNavigation');
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
		}
		
		function performAction(){
			
			// set variables
			$params = array();
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'ViewList';
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : 0 ;
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : $params['gID'];
			$params['tID']  = ( isset($_GET['travelerID']))? $_GET['travelerID'] : 0;
			$params['confirm']  = ( isset($_GET['confirm']))? $_GET['confirm'] : null;
			$params['requests'] = ( isset($_GET['newrequest'])) ? $_GET['newrequest'] : null ;
			
			$params['sentInvites'] = array();
			if( $params['sentInvites'] = $this->obj_session->get('sentInvites') ){
				$this->obj_session->unsetVar('sentInvites');
			}
		
			$params['arrNewlyAdded'] = array();
			if( 0 < ($this->obj_session->get('arrNewlyAdded')) ){
				$params['arrNewlyAdded'] = $this->obj_session->get('arrNewlyAdded');
				$this->obj_session->unsetVar('arrNewlyAdded');
			}
				
			switch($params['mode']){
			
				case "accept":
					$this->_acceptMember($params);
					break;
				
				case "deny":
					$this->_denyMember($params);
					break;
				
				case "remove":
					$this->_removeMember($params);
					break;
				
				case "invite":
					
					$this->_inviteMembers($params);
					break;
					
				default:
					$this->_viewMembers($params);
			}
		}
		
		
		function _defineCommonAttributes($_params = array()){
			
			//check if user is currently logged in
			if ($this->obj_session->get('travelerID') > 0) {
				$this->isLogged		=	true ;
				$this->loggedUser = $this->file_factory->getClass ('Traveler',array($this->obj_session->get('travelerID')));
				
				// insert code for logger system
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($this->loggedUser->getTravelerID(),'TRAVELER');
			}
		
			// if groupID is defined, instantiate group and check if logged user is member or adminstrator
			if (0 < $_params['gID']){
				$mGroup = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($_params['gID']));
				$this->group   = $mGroup[0];

				$this->isMemberLogged	=	( $this->isLogged ) ? $this->group->isMember($this->loggedUser) : false;
				$this->isAdminLogged	=	( $this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID'))) ? true : false;
			
				$this->subNav = $this->file_factory->getClass('SubNavigation');
				$this->subNav->setContext('GROUP');
				$this->subNav->setContextID($_params['gID']);
				$this->subNav->setLinkToHighlight('MEMBERS');
				
				(GROUP::ADMIN == $this->group->getDiscriminator() ) ? $this->subNav->setGroupType('ADMIN_GROUP') : $this->subNav->setGroupType('FUN_GROUP');
			}
			
			// if travelerID is defined, instantiate traveler
			if (0 < $_params['tID']) {
				$this->traveler = $this->file_factory->getClass('Traveler',array($_params['tID']));
			}
			
				$this->page		=	( isset($_GET['page']))? $_GET['page'] : 1;
				
				$this->tpl = $this->file_factory->getClass('Template');
				$this->tpl->set_path("travellog/views/");
			
				$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
			
		}
		
		
		function _allowAdminPrivileges($_privilege = false){
			if (!$_privilege){
				if (!$this->isLogged)
					header("location:login.php?redirect=".$_SERVER['REQUEST_URI']); 
				elseif (is_null($this->group)) 
					header("location:group.php"); 
				else
				 	header("location:members.php?gID=".$this->group->getGroupID());
			}
		}
		
		/**
			accept members to group
		*/
		function _acceptMember($_params = array() ){
			
			$this->_defineCommonAttributes($_params);
			
			/** To Accept Group Members, the ff must be true:
			 *	1. Admin/Group Owner is logged in
			 *	2. Group is defined
			 * 	3. Traveler is defined
			 *  4. Traveler is in group's request list
		     */
			$this->_allowAdminPrivileges($this->isAdminLogged && isset($this->group) && isset($this->traveler) && $this->group->isInRequestList($this->traveler));	
		
			if (isset($_params['confirm'])) {	
				
				$this->group->addMember($this->traveler);
			
				header("location:members.php?gID=".$this->group->getGroupID());
			}
			else{
			
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				$linkSource  = 'members.php?gID=' . $this->group->getGroupID() ;
				
				$this->tpl->set("msgConfirm",'You are about to accept request of <a href="/' . $this->traveler->getUserName() . '">' . $this->traveler->getUserName() . '</a> to join your group. ');
				$this->tpl->set("btnConfirm",'Accept');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'Cancel');
				$this->tpl->set("linkCancel", $linkSource);
				$this->tpl->set("subNavigation",$this->subNav);
			
				$this->tpl->out("tpl.GroupConfirmation.php");
			
			}		
		}
		
		/**
			deny members' request to join group
		*/
		
		function _denyMember($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged);	// if owner of group not logged in, redirect
			$this->_allowAdminPrivileges(isset($this->traveler));	// if no traveler selected, redirect						
			$this->_allowAdminPrivileges(isset($this->group));	// if no group selected, redirect		
			$this->_allowAdminPrivileges($this->group->isInRequestList($this->traveler));		// if traveler to be accepted in group is not in requestlist, redirect
			
			if (isset($_params['confirm'])) {	
				
				$this->group->denyRequest($this->traveler->getTravelerID());	
				
				header("location:members.php?gID=".$this->group->getGroupID());
			}

			else{
				
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				$linkSource  = 'members.php?gID=' . $this->group->getGroupID() ;
				
				$this->tpl->set("msgConfirm",'Are you sure you want to deny request of ' . $this->traveler->getUserName() . ' to join this group ?');
				$this->tpl->set("btnConfirm",'Deny');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'Cancel');
				$this->tpl->set("linkCancel", $linkSource);
				$this->tpl->set("subNavigation",$this->subNav);
				
				$this->tpl->out("tpl.GroupConfirmation.php");

			}
			
		}
		
		/**
			deny members' request to join group
		*/
		
		function _removeMember($_params = array()) {
			
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged);	// if owner of group not logged in, redirect
			$this->_allowAdminPrivileges(isset($this->traveler));	// if no traveler selected, redirect						
			$this->_allowAdminPrivileges(isset($this->group));	// if no group selected, redirect		
			$this->_allowAdminPrivileges($this->group->isMember($this->traveler));		// if traveler to be accepted in group is not in requestlist, redirect
			
			if (isset($_params['confirm'])) {	
				
				if ($this->group->isStaff($this->traveler->getTravelerID()))
					$this->group->removeStaff($this->traveler->getTravelerID());
				
				if ($this->group->isModerator($this->traveler->getTravelerID()))
					$this->group->removeModerator($this->traveler->getTravelerID());
						
				$this->group->removeMember($this->traveler->getTravelerID());
				
				header("location:members.php?gID=".$this->group->getGroupID());
			}
			else{
				
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				$linkSource  = 'members.php?gID=' . $this->group->getGroupID();
				
				$isStaff = $this->group->isStaff($this->loggedUser->getTravelerID());
				$isModerator = $this->group->isModerator($this->loggedUser->getTravelerID());
				
				if ($isStaff || $isModerator){
					$txtFacilitatorType = ($isStaff) ? 'staff' : '';
					$txtFacilitatorType = (strlen($txtFacilitatorType)==0 && $isModerator) ? 'moderator' : '';
					
					$_msgConfirm = "Are you sure you want to remove your membership from " . $this->group->getName() . ' ? ' . 'Leaving this subgroup will remove your ' . $txtFacilitatorType . ' privileges.';
				}else
					$_msgConfirm = "Are you sure you want to remove membership of " . $this->traveler->getUserName() . ' from this group ?';
				
				$this->tpl->set("msgConfirm",$_msgConfirm);
				$this->tpl->set("btnConfirm",'Remove');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'Cancel');
				$this->tpl->set("linkCancel", $linkSource);
				$this->tpl->set("subNavigation",$this->subNav);
				
				$this->tpl->out("tpl.GroupConfirmation.php");
			
			}
			
		}
		
		
		function _viewMembers($_params = array() ){
			
			$this->_defineCommonAttributes($_params);
			
			$mp = $this->file_factory->getClass('GroupMembersPanel');
			
			$mp->setPage($this->page);
	   		$mp->setGroup($this->group);
	   		$mp->setIsAdminLogged($this->isAdminLogged);  		
	  		$mp->setArrInviteReport($_params['sentInvites']);
	  		$mp->setArrNewlyAdded($_params['arrNewlyAdded']);
			
			$this->tpl->set('grpMemberPanelTemplate',$mp);
	 		$this->tpl->set('isAdminLogged', $this->isAdminLogged);
			$this->tpl->set('adminTravelerID',$this->group->getAdministrator()->getTravelerID() );		
			$this->tpl->set('grpID',$this->group->getGroupID());
			$this->tpl->set('sendableID',$this->group->getSendableID());
			$this->tpl->set('grpName',$this->group->getName());
			$this->tpl->set('grpAccess',$this->group->getGroupAccess());
			$this->tpl->set('isSubGroup',$this->group->isSubGroup());
			$this->tpl->set('newRequest',$_params['requests']);
			
			$this->tpl->set("subNavigation",$this->subNav);
			$this->tpl->out('tpl.GroupMembers.php');
		}
		
		function _inviteMembers($_params = array()){
		
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged);	// if owner of group not logged in, redirect
			
			$mp = $this->file_factory->getClass('GroupMembersPanel');
			
			$mp->setPage($this->page);
	   	$mp->setGroup($this->group); 
			
	   	$mp->setIsAdminLogged($this->isAdminLogged);  		
	  		
	  		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				/**
				 * comments made by neri
				 * this is to solve those undefined variables in the template
				 * 10-31-08
				 */
				
				$obj_message_panel = $this->file_factory->getClass('GroupMembersPanel');
				
				if (isset($_POST['cmbSubGroup'])){
					$sGID    = $_POST['cmbSubGroup'];
					$factory 	=  GroupFactory::instance();
					$obj_selectSubGroup  =  $factory->create(array($sGID)); 
					$obj_message_panel->setSelectSubGroup($obj_selectSubGroup[0]);   
					$mp->setSelectSubGroup($obj_selectSubGroup[0]);                                                                                                 
				}
					
				//$obj_message_panel = $this->file_factory->getClass('GroupMembersPanel');

				$obj_message_panel->setPage($this->page);
				$obj_message_panel->setGroup($this->group);
				//$obj_message_panel->setSelectSubGroup($obj_selectSubGroup[0]); 
				$obj_message_panel->setIsAdminLogged($this->isAdminLogged);
				$obj_message_panel->setPostValues($_POST);
				
				//$mp->setSelectSubGroup($obj_selectSubGroup[0]); 
				$mp->setInnerMessagePanel($obj_message_panel); 
				
			}
	
			$this->tpl->set('grpMemberPanelTemplate',$mp);
	 		$this->tpl->set('isAdminLogged', $this->isAdminLogged);
			$this->tpl->set('adminTravelerID',$this->group->getAdministrator()->getTravelerID() );		
			$this->tpl->set('grpID',$this->group->getGroupID());
			$this->tpl->set('sendableID',$this->group->getSendableID());
			$this->tpl->set('grpName',$this->group->getName());
			$this->tpl->set('grpAccess',$this->group->getGroupAccess());
			$this->tpl->set('isSubGroup',$this->group->isSubGroup());
			$this->tpl->set('newRequest',$_params['requests']);
			
			$this->tpl->set("subNavigation",$this->subNav);
			
			$this->tpl->out('tpl.GroupMembers.php');
		}
	}
?>