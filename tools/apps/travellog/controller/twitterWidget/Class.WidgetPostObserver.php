<?php
require_once('vendor/autoload/GanetAutoloader.php');
require_once("travellog/model/Class.SessionManager.php");
class WidgetPostObserver {

	const TWITTER_CONTEXT = 'twitter';
	const FACEBOOK_CONTEXT = 'facebook';
	
	/* $url - format must be e.g.: /journal-entry.php?travelerID=123&journalID=123&entryID=123
	 * 
	 * 
	 */ 
	public static function post($url, $params = array()){
		GanetAutoloader::getInstance()->register();
		GanetPropelRuntime::getInstance()->initPropel();
		
		$travelerID = SessionManager::getInstance()->get('travelerID');
		
		$trueURL = 'http://'.$_SERVER['SERVER_NAME'].$url;
		
		OAuthAPIFactory::getInstance()->create(self::TWITTER_CONTEXT)->post($trueURL, $travelerID, $params);

		OAuthAPIFactory::getInstance()->create(self::FACEBOOK_CONTEXT)->post($trueURL, $travelerID, $params);
	}
	
}