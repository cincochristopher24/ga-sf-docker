<?php
require_once('travellog/controller/Class.AbstractFriendController.php');
class GANETFriendController extends AbstractFriendController{
	
	function performAction(){
		$this->data           = array_merge($_GET, $_POST);
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'View';
		parent::__applyRules();
		parent::performAction();  
	}
}
?>
