<?php
	
	require_once ('Class.Paging.php');
	require_once ('Class.ObjectIterator.php');
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.dbHandler.php");
	require_once('Class.Template.php');
	
	abstract class AbstractGroupPagesController implements iController{
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,

		$isLogged	  = FALSE,

		$isMemberLogged = FALSE,

		$isAdminLogged = FALSE,

		$group 			= NULL,
		
		$subNav		=	NULL,
		
		$profile = NULL,  // the group profile that shows above the subnavigation
				
		$tpl		=	NULL,
		
		$params	= array(	"action"		=>	NULL,
							"gID"			=>	0,
							"tags"			=>	"",
							"keyword"		=>	"",
							"subgroupID"	=>	array(),
							"tagID"			=>	array(),
							"page"			=>	1,
							"count"			=>	0,
							"category"		=>	"" ),
		
		$tags = array(),
		
		$pagesHelper = NULL;
		
		function __construct(){
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template',array('path'=>''));
		
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('AdminGroup');
			
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('BackLink' , array('path' => 'travellog/model/navigator'));			
			$this->file_factory->registerClass('SubNavigation');
			
			$this->file_factory->registerClass('GroupPagesHelper',array('path'=>'travellog/helper'));
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->tpl = $this->file_factory->getClass('Template');
			$this->tpl->set_path("travellog/views/group-pages/");
			
		}
		
		function performAction(){
			$this->pagesHelper = $this->file_factory->invokeStaticClass("GroupPagesHelper","getInstance");
			$this->params = array_merge($this->params,$this->_extractParameters());
			$this->tags = $this->pagesHelper->readParameterTags($this->params["tags"]);			
			switch($this->params["action"]){
				case 'FETCH_RECENT'		:	$this->_fetchRecent();
											break;
				case 'FETCH_BY_NAME'	:	$this->_fetchByName();
											break;
				case 'FETCH_TAGGED'		:	$this->_fetchTagged();
											break;
				case 'FEATURE'			:	$this->_feature();
											break;
				case 'UNFEATURE'		:	$this->_unfeature();
											break;
				case 'REMOVE_FROM_PAGE'	:	$this->_removeFromPage();
											break;
				case 'DELETE_GROUP'		:	$this->_deleteSubgroup();
											break;
				case 'FIND_BY_NAME'		:	$this->_redirect($this->pagesHelper->getDefaultPageUrl($this->params["gID"])."&keyword=".$this->params["keyword"]);
											break;
				default					:	if( isset($GLOBALS["CONFIG"]) && isset($this->params["search"]) ){
												//cobrand sites main navigation groups tab
												if( isset($_GET["keyword"]) ){
													$this->_cobrandFindByName();
												}else{
													$this->_viewGroupPagesSearch();
												}
											}else{
												if( isset($_GET["keyword"]) ){
													$this->_findByName();
												}else{
													$this->_viewGroupPages();
												}
											}
											break;
			}
		}
		
		/**
		 * Checks whether the current logged in is an admin user or not. If it
		 * is not an admin user it redirects the page to custom group normal view mode.
		 * 
		 * @param boolean $_privilege
		 * @return void
		 */
		function _allowAdminPrivileges(){
			if (!$this->isAdminLogged){
				$this->_redirect("/group-pages.php?gID=".$this->group->getGroupID());
			}
		}
		
		function _extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}
		
		function _redirect($page=NULL){
			if( "main-home" == $page ){
				header("location: /");
			}elseif( "404" == $page || 404 == $page ){
				if( $this->pagesHelper->isAjaxRequest() ){
					header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
				}else{
					header("location: /FileNotFound.php");
				}
			}elseif( "main-groups" == $page ){
				header("location: /group.php");
			}elseif( "ajax_restricted" == $page ){
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
			}elseif( "group-home" == $page ){
				header("location: /group.php?gID=".$this->group->getGroupID());
			}elseif( "cobrand-subgroups" == $page ){
				header("location: /group-pages.php?search");
			}elseif( !is_null($page) ){
				header("location: ".$page);
			}else{
				header("location: /");
			}
			exit;
		}
		
		function _prepareDependencies(){
			Template::setMainTemplateVar('layoutID', 'subgroups');
			if( $this->isAdminLogged ){
				Template::includeDependentCss("/min/g=GroupPagesCss");
				Template::includeDependentJs("/min/g=GroupPagesJs");
			}else{
				Template::includeDependentCss("/min/g=GroupPagesCssPublic");
				Template::includeDependentJs("/min/g=GroupPagesJsPublic");
			}
		}
		
		function _defineCommonAttributes(){
			if( isset($GLOBALS["CONFIG"]) ){
				$this->params["gID"] = $GLOBALS["CONFIG"]->getGroupID();
			}
			
			if( 0 == $this->params["gID"] ){
				$this->_redirect("main-groups");
			}
			
			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($this->params['gID']));
			if( !count($mGroup) ){
				$this->_redirect("main-groups");
			}
			
			$this->group = $mGroup[0];
			if( !$this->group->isParent() ){
				$this->_redirect(404);
			}
			
			$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? TRUE : FALSE;
			$this->loggedUser = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
			
			$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			if( isset($GLOBALS["CONFIG"]) ){
				if( !$this->isAdminLogged ){
					$this->_redirect("main-home");
				}
				Template::setMainTemplateVar('page_location','Home');
			}else{
				Template::setMainTemplateVar('page_location','Groups / Clubs');
			}
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			
			$this->subNav = $this->file_factory->getClass('SubNavigation');
			$this->subNav->setContext('GROUP');
			$this->subNav->setContextID($this->params['gID']);
			$this->subNav->setLinkToHighlight('SUBGROUPS');
			
			$this->profile = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$this->profile->init($this->group->getGroupID());
			
			$this->groupTags = GanetPageTagPeer::retrieveByGroupID($this->group->getGroupID());
			$groupTagData = array();
			foreach($this->groupTags as $iGroupTag){
				$groupTagData[] = $iGroupTag->getName();
			}
			$this->tpl->set('groupTagData',$groupTagData);
		}
		
		function _defineCommonAttributes2($requireAdmin=TRUE){//for ajax requests
			if( !$this->pagesHelper->isAjaxRequest() ){
				$this->_redirect(404);
			}
			if( isset($GLOBALS["CONFIG"]) ){
				$this->params["gID"] = $GLOBALS["CONFIG"]->getGroupID();
			}elseif( 0 == $this->params["gID"] ){
				$this->_redirect("ajax_restricted");
			}
			
			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($this->params['gID']));
			if( !count($mGroup) ){
				$this->_redirect("ajax_restricted");
			}
			
			$this->group = $mGroup[0];
			if( !$this->group->isParent() ){
				$this->_redirect("ajax_restricted");
			}
			
			$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? TRUE : FALSE;
			$this->loggedUser = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
			
			if( $requireAdmin && !$this->isAdminLogged ){
				$this->_redirect("ajax_restricted");
			}
			
			if( $requireAdmin ){
				$this->_enableLogging();
			}
		}
		
		function _viewGroupPages(){
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			
			$groupCount = GroupPeer::countSubgroups($this->group);
			$this->tpl->set("searchForm",$this->_fetchSearchForm());
			
			if( count($this->tags) ){
				$rowsLimit = $this->pagesHelper->getRowsLimit($this->params["page"]);
				$limitStr = "LIMIT " . $rowsLimit->getOffset() . ", " . $rowsLimit->getRows();

				$taggedGroups = GroupPeer::getTaggedGroups($this->group, $this->tags, $limitStr);
				$totalTaggedGroupsCount = GroupPeer::countTaggedGroups($this->group,$this->tags);
				
				$this->tpl->set("breadcrumbs",$this->_fetchBreadCrumbs($totalTaggedGroupsCount));
				$this->tpl->set("defaultGroups",$this->_fetchTaggedGroups($taggedGroups,$totalTaggedGroupsCount));
				if( $this->isAdminLogged ){
					$this->tpl->set("wideTagCloud",$this->_fetchTagCloud('narrow'));
				}else{
					$this->tpl->set("randomSections",$this->_fetchRandomSections());
					$this->tpl->set("narrowTagCloud",$this->_fetchTagCloud('narrow'));
				}
				
				if( $this->isAdminLogged && !isset($this->params["search"]) ){
					$this->tpl->set("narrowSubgroupsInfo",$this->_fetchSubgroupsInfo("narrow"));
				}
			}else{
				if( !$this->isAdminLogged && !$groupCount ){
					$this->_redirect("group-home");
				}else if( !$this->isAdminLogged && isset($GLOBALS["CONFIG"]) ){
					$this->_redirect("cobrand-subgroups");
				}
				$this->tpl->set("searchResultsHeader",$this->_fetchSearchResultsHeader($groupCount));
				
				if( $this->isAdminLogged && !isset($this->params["search"]) && !count($this->tags) ){
					if( $groupCount ){
						$this->tpl->set("narrowSubgroupsInfo",$this->_fetchSubgroupsInfo("narrow"));
					}else{
						$this->tpl->set("wideSubgroupsInfo",$this->_fetchSubgroupsInfo("wide"));
					}
				}
				$featuredGroups = array();
				$otherGroups = array();
				if( $this->isAdminLogged && !isset($this->params["search"]) ){
					$featuredGroups = GroupPeer::getFeaturedGroups($this->group);
					$otherGroups = GroupPeer::getRecentSubgroups($this->group);
				}else{
					$otherGroups = GroupPeer::getRecentSubgroups($this->group,$includeFeatured=TRUE);
				}
				$this->tpl->set("defaultGroups",$this->_fetchDefaultGroups($featuredGroups,$otherGroups));
				if( $this->isAdminLogged ){
					$this->tpl->set("wideTagCloud",$this->_fetchTagCloud('wide'));
				}else{
					$this->tpl->set("narrowTagCloud",$this->_fetchTagCloud('wide'));
				}
			}
			if( $this->isAdminLogged ){
				$this->tpl->set("pageManagementLinks",$this->_fetchGroupPagesManagementLinks());
			}
			$this->tpl->set("configScript",$this->_fetchGroupPagesConfigScript());
			
			$this->tpl->set("isAdminLogged",$this->isAdminLogged);
			$this->tpl->set("profile",$this->profile->get_view());
			$this->tpl->set("subNavigation",$this->subNav);
			$this->tpl->set("isLandingPage",0==count($this->tags));
			$this->tpl->out("tpl.GroupPages.php");
		}
		
		function _fetchGroupPagesConfigScript(){
			$group_tags = GanetPageTagPeer::retrieveByGroupIDAndNames($this->group->getGroupID(),$this->tags);
			$last_tagID = 0;
			//find the last tag in the page breadcrumbs
			$group_tags_end = array();
			if( count($this->tags) ){
				$last_tag = $this->tags[count($this->tags)-1];
				foreach($group_tags AS $tag){
					if( $last_tag == $tag->getName() ){
						$group_tags_end[] = $tag;
						break;
					}
				}
			}
			$template = new Template();
			$template->set("group",$this->group);
			$template->set("tags",$group_tags_end);
			$template->set("allTags",$group_tags);
			$template->set("keyword",$this->params["keyword"]);
			$template->set("actionUrl", isset($this->params["search"]) ? "/group-pages.php?search" : "/group-pages.php");
			return $template->fetch("travellog/views/group-pages/tpl.GroupPagesConfigScript.php");
		}
		
		function _fetchSearchForm(){
			$template = new Template();
			$template->set("group",$this->group);
			return $template->fetch("travellog/views/group-pages/tpl.FrmGroupNameSearch.php");
		}
		
		function _fetchBreadCrumbs($taggedGroupCount=0){
			$template = new Template();
			$template->set("tags",$this->tags);
			$template->set("group",$this->group);
			$template->set("taggedGroupCount",$taggedGroupCount);
			$template->set("isAdminLogged",$this->isAdminLogged);
			$template->set("isCobrandSearchPage", isset($this->params["search"]) );
			return $template->fetch("travellog/views/group-pages/tpl.GroupPagesBreadCrumbs.php");
		}
		
		function _fetchDefaultGroups($featuredGroups=array(),$otherGroups=array()){
			require_once("travellog/model/Class.GroupPeer.php");
			$template = new Template();
			$template->set("isAdminLogged",$this->isAdminLogged);
			$template->set("loggedUser",$this->loggedUser);
			$template->set("featuredGroups",$featuredGroups);
			$template->set("otherGroups",$otherGroups);
			if( $this->isAdminLogged && !isset($this->params["search"]) ){
				$template->set("totalOtherGroupsCount",GroupPeer::countRecentSubgroups($this->group));
			}else{
				$template->set("totalOtherGroupsCount",GroupPeer::countRecentSubgroups($this->group,$includeFeatured=TRUE));
			}
			$template->set("isCobrandSearchPage", isset($this->params["search"]) );
			$template->set("contextTags",array());
			$template->set("group",$this->group);
			$template->set("page",$this->params["page"]);
			return $template->fetch("travellog/views/group-pages/tpl.DefaultGroupsComponent.php");
		}
		
		function _fetchTaggedGroups($taggedGroups=array(),$totalTaggedGroupsCount=NULL){
			$template = new Template();
			$template->set("isAdminLogged",$this->isAdminLogged);
			$template->set("loggedUser",$this->loggedUser);
			$template->set("taggedGroups",$taggedGroups);
			if( is_null($totalTaggedGroupsCount) ){
				$template->set("totalTaggedGroupsCount",GroupPeer::countTaggedGroups($this->group,$this->tags));
			}else{
				$template->set("totalTaggedGroupsCount",$totalTaggedGroupsCount);
			}
			$template->set("isCobrandSearchPage", isset($this->params["search"]) );
			$template->set("contextTags",$this->tags);
			$template->set("group",$this->group);
			$template->set("page",$this->params["page"]);
			return $template->fetch("travellog/views/group-pages/tpl.DefaultGroupsComponent.php");
		}
		
		function _fetchGroupPagesManagementLinks(){
			$template = new Template();
			$template->set("group",$this->group);
			return $template->fetch("travellog/views/group-pages/tpl.GroupPagesManagementLinks.php");
		}
		
		function _fetchTagCloud($size='wide'){
			$template = new Template();
			$gID = $this->group->getGroupID();
			$template->set('groupID',$gID);
			$template->set('tagContexts', $this->tags);	
			$template->set('isAdminLogged',$this->isAdminLogged);		
			$template->set('relatedTags', GanetPageTagPeer::retrieveByTagContext($gID,GanetPageTagPeer::retrieveByGroupIDAndNames($gID,$this->tags)));
			return $template->fetch("travellog/views/group-pages/tpl._{$size}TagCloud.php");
		}
		
		function _fetchSubgroupsInfo($layout="narrow"){
			$template = new Template();
			$template->set("group",$this->group);
			$template->set("layout",$layout);
			return $template->fetch("travellog/views/group-pages/tpl.SubgroupsInfo.php");;
		}
		
		function _fetchDefaultGroupsByName($results=array(),$resultCount=0){
			$template = new Template();
			$template->set("keyword",$this->params["keyword"]);
			$template->set("isAdminLogged",$this->isAdminLogged);
			$template->set("loggedUser",$this->loggedUser);
			$template->set("foundGroups",$results);
			$template->set("totalFoundGroupsCount",$resultCount);	
			$template->set("isCobrandSearchPage", isset($this->params["search"]) );
			$template->set("contextTags",array());
			$template->set("group",$this->group);
			$template->set("page",$this->params["page"]);
			return $template->fetch("travellog/views/group-pages/tpl.DefaultGroupsComponent.php");
		}
		
		function _fetchSearchResultsHeader($resultCount=0){
			$template = new Template();
			$template->set("resultCount",$resultCount);
			$template->set("group",$this->group);
			$template->set("keyword",$this->params["keyword"]);
			$template->set("isAdminLogged",$this->isAdminLogged);
			$template->set("isCobrandSearchPage", isset($this->params["search"]) );
			if( $this->isAdminLogged && !isset($this->params["search"]) && !count($this->tags) ){
				$template->set("defaultAdminHeader",TRUE);
			}else{
				$template->set("defaultAdminHeader",FALSE);
			}
			return $template->fetch("travellog/views/group-pages/tpl.SearchResultsHeader.php");
		}
		
		function _findByName(){
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			
			$this->tpl->set("searchForm",$this->_fetchSearchForm());
			
			$results = $this->group->getSubGroups($this->pagesHelper->getRowsLimit($page=1), $order = NULL, $isCount = false, $searchKey=$this->params["keyword"]);
			$resultCount = $this->group->getSubGroups(NULL, $order = NULL, $isCount = TRUE, $searchKey=$this->params["keyword"]);
			
			$this->tpl->set("defaultGroups",$this->_fetchDefaultGroupsByName($results,$resultCount));
			$this->tpl->set("searchResultsHeader",$this->_fetchSearchResultsHeader($resultCount));
			
			if( $this->isAdminLogged ){
				$this->tpl->set("pageManagementLinks",$this->_fetchGroupPagesManagementLinks());
				$this->tpl->set("wideTagCloud",$this->_fetchTagCloud('wide'));
			}else{
				$this->tpl->set("narrowTagCloud",$this->_fetchTagCloud('wide'));
			}
			
			$this->tpl->set("configScript",$this->_fetchGroupPagesConfigScript());
			
			$this->tpl->set("isAdminLogged",$this->isAdminLogged);
			$this->tpl->set("profile",$this->profile->get_view());
			$this->tpl->set("subNavigation",$this->subNav);
			$this->tpl->set("isLandingPage",FALSE);
			$this->tpl->out("tpl.GroupPages.php");
		}

		function _feature(){
			$this->_defineCommonAttributes2();
			if( !is_array($this->params["subgroupID"]) || !count($this->params["subgroupID"]) ){
				$this->_redirect("ajax_restricted");
			}
			if( !isset($this->params["category"]) && !in_array($this->params["category"],array("FEATURED","RECENT","TAGGED","SEARCH")) ){
				$this->_redirect("ajax_restricted");
			}
			$groups = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create($this->params["subgroupID"]);
			foreach($groups AS $group){
				$group->setGroupHomeFeatured(1);
				$group->UpdateGroupHomeFeatured();
			}
			$new_category = $this->params["category"];
			if( "RECENT" == $this->params["category"] ){
				$new_category = "FEATURED";
			}
			$this->_fetchAffectedGroups($groups,$new_category);
		}
		
		function _unfeature(){
			$this->_defineCommonAttributes2();
			if( !is_array($this->params["subgroupID"]) || !count($this->params["subgroupID"]) ){
				$this->_redirect("ajax_restricted");
			}
			$groups = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create($this->params["subgroupID"]);
			foreach($groups AS $group){
				$group->setGroupHomeFeatured(0);
				$group->UpdateGroupHomeFeatured();
			}
			$new_category = $this->params["category"];
			if( "FEATURED" == $this->params["category"] ){
				$new_category = "RECENT";
			}
			$this->_fetchAffectedGroups($groups,$new_category);
		}
		
		function _fetchAffectedGroups($groups,$category){
			foreach($groups AS $group){
				$tpl = new Template();
				$tpl->set("loggedUser",$this->loggedUser);
				$tpl->set("isAdminLogged",$this->isAdminLogged);
				$tpl->set("isCobrandSearchPage",FALSE);
				$tpl->set("category",$category);
				$tpl->set("group",$group);
				$tpl->out("travellog/views/group-pages/tpl.GroupListItem.php");
			}
		}
		
		function _removeFromPage(){
			$this->_defineCommonAttributes2();
			if( !is_array($this->params["subgroupID"]) || !count($this->params["subgroupID"]) ){
				$this->_redirect("ajax_restricted");
			}
			if( !is_array($this->params["tagID"]) || !count($this->params["tagID"]) ){
				$this->_redirect("ajax_restricted");
			}
			//remove only from the last tag
			GanetGroupTagPeer::removeGroupTags($this->params["subgroupID"],array($this->params["tagID"][count($this->params["tagID"])-1]));
		}
		
		function _deleteSubgroup(){
			$this->_defineCommonAttributes2();
			if( !is_array($this->params["subgroupID"]) || !count($this->params["subgroupID"]) ){
				$this->_redirect("ajax_restricted");
			}
			$groups = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create($this->params["subgroupID"]);
			if( !count($groups) ){
				$this->_redirect("ajax_restricted");
			}
			foreach($groups AS $group){
				GanetGroupTagPeer::removeGroupTagsByGroupID($group->getGroupID());
				$group->Delete();
			}
		}
		
		function _fetchRecent(){
			$this->_defineCommonAttributes2($requireAdmin=FALSE);
			
			$rowsLimit = $this->pagesHelper->getRowsLimit($this->params["page"]);
			$limitStr = "LIMIT " . $rowsLimit->getOffset() . ", " . $rowsLimit->getRows();
			
			if( $this->isAdminLogged && !isset($this->params["search"]) ){
				$groups = GroupPeer::getRecentSubgroups($this->group,$includeFeatured=FALSE,$limitStr);
			}else{
				$groups = GroupPeer::getRecentSubgroups($this->group,$includeFeatured=TRUE,$limitStr);
			}
			$paging = new Paging($this->params["count"], $this->params["page"], 'action=FETCH_RECENT&RECENT', GroupPagesHelper::RPP );
			$paging->setOnclick("jQuery.fn.loadPage");
			$iterator = new ObjectIterator($groups,$paging );
			
			$template = new Template();
			$template->set("category",$this->params["category"]);
			$template->set("groups",$groups);
			$template->set("loggedUser",$this->loggedUser);
			$template->set("isAdminLogged",$this->isAdminLogged);
			$template->set("isCobrandSearchPage",isset($this->params["search"]));
			$template->set("paging",$paging);
			$template->set("iterator",$iterator);
			if( $this->isAdminLogged && !isset($this->params["search"]) ){
				$template->out("travellog/views/group-pages/tpl.GroupListContent.php");
			}else{
				$template->out("travellog/views/group-pages/tpl.GroupListContentPublic.php");
			}
		}
		
		function _fetchByName(){
			$this->_defineCommonAttributes2($requireAdmin=FALSE);
			
			$groups = $this->group->getSubGroups($this->pagesHelper->getRowsLimit($this->params["page"]), $order = NULL, $isCount = false, $searchKey=$this->params["keyword"]);
			
			$paging = new Paging($this->params["count"], $this->params["page"], 'action=FETCH_BY_NAME&SEARCH', GroupPagesHelper::RPP );
			$paging->setOnclick("jQuery.fn.loadPage");
			$iterator = new ObjectIterator($groups,$paging );
			
			$template = new Template();
			$template->set("category",$this->params["category"]);
			$template->set("groups",$groups);
			$template->set("loggedUser",$this->loggedUser);
			$template->set("isAdminLogged",$this->isAdminLogged);
			$template->set("isCobrandSearchPage",isset($this->params["search"]));
			$template->set("paging",$paging);
			$template->set("iterator",$iterator);
			if( $this->isAdminLogged && !isset($this->params["search"]) ){
				$template->out("travellog/views/group-pages/tpl.GroupListContent.php");
			}else{
				$template->out("travellog/views/group-pages/tpl.GroupListContentPublic.php");
			}
		}
		
		function _fetchTagged(){
			$this->_defineCommonAttributes2($requireAdmin=FALSE);
			
			$rowsLimit = $this->pagesHelper->getRowsLimit($this->params["page"]);
			$limitStr = "LIMIT " . $rowsLimit->getOffset() . ", " . $rowsLimit->getRows();
			
			$groups = GroupPeer::getTaggedGroups($this->group, $this->tags, $limitStr);
			
			$paging = new Paging($this->params["count"], $this->params["page"], 'action=FETCH_TAGGED&TAGGED', GroupPagesHelper::RPP );
			$paging->setOnclick("jQuery.fn.loadPage");
			$iterator = new ObjectIterator($groups,$paging );
			
			$template = new Template();
			$template->set("category",$this->params["category"]);
			$template->set("groups",$groups);
			$template->set("loggedUser",$this->loggedUser);
			$template->set("isAdminLogged",$this->isAdminLogged);
			$template->set("isCobrandSearchPage",isset($this->params["search"]));
			$template->set("paging",$paging);
			$template->set("iterator",$iterator);
			if( $this->isAdminLogged && !isset($this->params["search"]) ){
				$template->out("travellog/views/group-pages/tpl.GroupListContent.php");
			}else{
				$template->out("travellog/views/group-pages/tpl.GroupListContentPublic.php");
			}
		}
		
		function _fetchRandomSections(){
			$sections = array($this->_fetchRandomJournalsSection(),$this->_fetchRandomPhotosSection(),$this->_fetchRandomVideosSection());
			return $sections;
		}
		
		function _fetchRandomJournalsSection(){
			$entries = GanetPageTagService::getPageRelatedJournalEntries($this->group, $this->tags, $limit=3);
			$template = new Template();
			$template->set("section","JOURNALS");
			$template->set("label",htmlspecialchars(implode(" ",$this->tags),ENT_QUOTES)." Journals");
			$template->set("items",$entries);
			return $template->fetch("travellog/views/group-pages/tpl.GroupPagesRandomSection.php");
		}
		
		function _fetchRandomPhotosSection(){
			$photos = GanetPageTagService::getPageRelatedPhotos($this->group, $this->tags, $limit=3);
			$template = new Template();
			$template->set("section","PHOTOS");
			$template->set("label",htmlspecialchars(implode(" ",$this->tags),ENT_QUOTES)." Photos");
			$template->set("items",$photos);
			return $template->fetch("travellog/views/group-pages/tpl.GroupPagesRandomSection.php");
		}
		
		function _fetchRandomVideosSection(){
			$videos = GanetPageTagService::getPageRelatedVideos($this->group, $this->tags, $limit=4);
			$template = new Template();
			$template->set("section","VIDEOS");
			$template->set("label",htmlspecialchars(implode(" ",$this->tags),ENT_QUOTES)." Videos");
			$template->set("items",$videos);
			return $template->fetch("travellog/views/group-pages/tpl.GroupPagesRandomSection.php");
		}
		
		function _enableLogging(){
			// check if user is currently logged in
			if ( $this->isAdminLogged ){
				// start GA Logger
				require_once 'gaLogs/Class.GaLogger.php';
				try{
					GaLogger::create()->start($this->loggedUser->getTravelerID(), 'TRAVELER');
				}
				catch(exception $e){
				}
			}
		}
	}