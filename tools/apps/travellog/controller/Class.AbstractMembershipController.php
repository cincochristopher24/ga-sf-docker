<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	abstract class AbstractMembershipController implements iController{
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser   = NULL,

		$isLogged	  = false,

		$group 		  = NULL,
		
		$subNav		  =	NULL,
		
		$tpl		  =	NULL;
		
		protected $mProfileComponent = NULL;
	
		function __construct(){
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			
			$this->file_factory->registerClass('GroupAcccess');
			$this->file_factory->registerClass('AdvisorAlert');
			
			$this->file_factory->registerClass('Condition');
			$this->file_factory->registerClass('FilterCriteria2');
			$this->file_factory->registerClass('FilterOp');
			
			/**
			 * added by: ianne -- 11/14/2208
			 * registered profilecompfactory and randomtravelbioview
			 */
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('BackLink' , array('path' => 'travellog/model/navigator/'));
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
			if( 0 < $this->obj_session->get("travelerID") ){
				$mProfileFactory = $this->file_factory->invokeStaticClass('ProfileCompFactory', 'getInstance', array());
				$mProfileComp	= $mProfileFactory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
				$mProfileComp->init($this->obj_session->get("travelerID"));
				$this->mProfileComponent = $mProfileComp->get_view();
			}
		}
		
		function performAction(){
			
			// set variables
			$params = array();
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : 'ViewList';
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'ViewList';
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : 0 ;
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : $params['gID'];
			$params['confirm']  = ( isset($_GET['confirm']))? $_GET['confirm'] : null;
			$params['page']  = ( isset($_GET['page']))? $_GET['page'] : 1;
			$params['page']  = ( isset($_POST['page']))? $_POST['page'] : $params['page'];
			
			switch($params['mode']){
			
				case "join":
					$this->_joinGroup($params);
					break;
				
				case "cancel":
					$this->_cancelRequest($params);
					break;
				
				case "leave":
					$this->_leaveGroup($params);
					break;
				
				case "accept":
					$this->_acceptInvite($params);
					break;
					
				case "deny":
					$this->_denyInvite($params);
					break;
		
				case "delete":
					$this->_deleteDeniedRequest($params);
					break;
					
				case "acceptinvite":
					$this->_acceptInvite2($params);
					break;
						
				default:
					$this->_viewMembershipInfo($params);
			}
		}
		
		// function added by ianne 11/20/2008 -- success prompt after confirm
		function _success(){
			
			$params = array();
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : 'ViewList';
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'ViewList';
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : 0 ;
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : $params['gID'];
			$params['confirm']  = ( isset($_GET['confirm']))? $_GET['confirm'] : null;
			
			switch($params['mode']){
				case "join":
					$this->obj_session->set('custompopup_message',"Your request has been successfully sent to the ".strtolower($this->group->getLabel())." ".$this->group->getName().".");
					if(GroupAccess::$OPEN == $this->group->getGroupAccess())
						$this->obj_session->set('custompopup_message',"You are now a member of the ".strtolower($this->group->getLabel())." ".$this->group->getName().".");
					break;
				case "cancel":
					$this->obj_session->set('custompopup_message',"You successfully canceled your request to join the ".strtolower($this->group->getLabel())." ".$this->group->getName().".");
					break;	
				case "leave":
					$this->obj_session->set('custompopup_message',"You are no longer a member of the ".strtolower($this->group->getLabel())." ".$this->group->getName().".");
					break;
				case "accept":	
					$this->obj_session->set('custompopup_message',"You are now a member of the ".strtolower($this->group->getLabel())." ".$this->group->getName().".");
					break;		
				case "deny":
					$this->obj_session->set('custompopup_message',"You successfully denied ".$this->group->getName()."&lsquo;s request to be a member of the ".strtolower($this->group->getLabel()).".");
					break;
				case "delete":
					$this->obj_session->set('custompopup_message',"You successfully removed the denied request to join the ".strtolower($this->group->getLabel())." ".$this->group->getName().".");
					break;
			}
		}
		
		function _defineCommonAttributes($_params = array()){
			
			//check if user is currently logged in
			if ($this->obj_session->get('travelerID') > 0) {
				$this->isLogged		=	true ;
				$this->loggedUser = $this->file_factory->getClass ('Traveler',array($this->obj_session->get('travelerID')));
				
				$this->subNav = $this->file_factory->getClass('SubNavigation');
				$this->subNav->setContext('TRAVELER');
				$this->subNav->setContextID($this->obj_session->get('travelerID'));
				$this->subNav->setLinkToHighlight('MY_GROUPS');
				
				// insert code for logger system
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($this->loggedUser->getTravelerID(),'TRAVELER');
			}
		
			// if groupID is defined, instantiate group and check if logged user is member or adminstrator
			if (0 < $_params['gID']){
				$mGroup = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($_params['gID']));
				$this->group = $mGroup[0];

				$this->isMemberLogged	=	( $this->isLogged ) ? $this->group->isMember($this->loggedUser) : false;
				$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') ? true : false;
				
			}
			
				$this->page		=	( isset($_GET['page']))? $_GET['page'] : 1;
				
				$this->tpl = $this->file_factory->getClass('Template');
				$this->tpl->set_path("travellog/views/");
				
				$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
			
		}
		
		
		function _allowPrivileges($_privilege = false){
			if (!$_privilege){
				if (!$this->isLogged)
					header("location:login.php?redirect=".$_SERVER['REQUEST_URI']); 
				elseif (is_null($this->group)) 
				 	header("location:group.php"); 
				else
				 	header("location:group.php?gID=".$this->group->getGroupID());
			}	
		}
		
		function _joinGroup($_params = array() ) {
		
			$this->_defineCommonAttributes($_params);
			
			/** To Join Group, all of the ff must be true:
			 *	1. User is logged in
			 *	2. Group is defined
			 * 	3. User is not yet a member of the group
			 *	4. Group is not Invite Only Type and not Closed Type
			 */
		
			$this->_allowPrivileges($this->isLogged && isset($this->group) && !$this->group->isMember($this->loggedUser) && !in_array($this->group->getGroupAccess(),array(GroupAccess::$INVITE_ONLY, GroupAccess::$CLOSE)));
			
			/** edited by ianne - 11/12/2008
		     *  redirected to referer page after action is completed
		     *  added ref = groupsubgroup; if link is from the parent's group home page
		     */
						
			if (isset($_GET['ref'])){
				if ('grouplist' == $_GET['ref']) 			$page = 'group.php?page='.$_params['page'];
				elseif ('grouphome' == $_GET['ref'])	  	$page = 'group.php?gID=' . $_GET['gID'];
				elseif ('groupsubgroup' == $_GET['ref'])	$page = 'subgroups.php?gID=' . $this->group->getParent()->getGroupID();
				elseif ('groupcustomgroup' == $_GET['ref'])	$page = 'customgroupsection.php?gID=' . $this->group->getParent()->getGroupID();
				elseif ('mygroups' == $_GET['ref'])         $page = 'group.php?mode=mygroups&page='.$_params['page'];
				else if ('cobrand_search' == $_GET['ref']) {
					$page  = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : 'membership.php' ;
				}
			    else 										$page = 'membership.php';
			} else
				$page = 'membership.php';
			
			if (isset($_params['confirm'])) {	
				
				// if open type group or traveler is already in invite list, then add as member
				if (GroupAccess::$OPEN == $this->group->getGroupAccess() || $this->group->isInInviteList($this->loggedUser)){
				    
					$this->group->addMember($this->loggedUser);
					//$this->tpl->set("message",'You are now a member of ' . $this->group->getName() . '.');
					
				}
				
				else{
				
					$this->group->requestToJoin($this->loggedUser);	
				
					//$this->tpl->set("message","Your request to join the group ".$this->group->getName() . " has been sent and is subject to the administrator's approval. You will be able to interact with this group once your request has been approved. Thank you.");
	        		
				}
				$this->_success();
				header("location:".$page);
				/*$this->tpl->set("grpID",$this->group->getGroupID());
				$this->tpl->set("grpName",$this->group->getName());
				$this->tpl->set("subNavigation", $this->subNav);
				$this->tpl->out("tpl.Join.php");*/
						
			}
			
			else {
				
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				$linkSource  = 'group.php?gID=' . $this->group->getGroupID() ;
		
				$this->tpl->set("msgConfirm","You are about to join the group " . $this->group->getName() . ". Click Join to proceed, click Cancel if you don't really want to join " . $this->group->getName() . ".");
				$this->tpl->set("btnConfirm",'Join');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'Cancel');
				$this->tpl->set("linkCancel", $linkSource);
				$this->tpl->set("subNavigation", $this->subNav);
		
				$this->tpl->out("tpl.GroupConfirmation.php");
			
			}		
		
		}
		
		
		function _cancelRequest($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			/** To Cancel Request to Join Group, all of the ff must be true:
			 *	1. User is logged in
			 *	2. Group is defined
			 * 	3. User is in group's request list
			 */
		
			$this->_allowPrivileges($this->isLogged && isset($this->group) && $this->group->isInRequestList($this->loggedUser));
			
			/** edited by ianne - 11/12/2008
		     *  redirected to referer page after action is completed
		     *  added ref = groupsubgroup; if link is from the parent's group home page
		     */
			
			if (isset($_GET['ref'])){
				if ('grouplist' == $_GET['ref']) 			$page = 'group.php?page='.$_params['page'];
				elseif ('grouphome' == $_GET['ref'])	  	$page = 'group.php?gID=' . $_GET['gID'];
				elseif ('groupsubgroup' == $_GET['ref'])	$page = 'group.php?gID=' . $this->group->getParent()->getGroupID();
				elseif ('mygroups' == $_GET['ref'])         $page = 'group.php?mode=mygroups&page='.$_params['page'];
			    else 										$page = 'membership.php';
			} else
				$page = 'membership.php';
			
			if (isset($_params['confirm'])) {	
				
				$invites = array();
				
				$this->group->removeFromRequestList($this->loggedUser->getTravelerID());		
			
				$this->_success();
				
				header("location:".$page);
				
			}
			
			else{
				
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				
				$this->tpl->set("msgConfirm",'Are you sure you want to cancel your request to join the group ' . $this->group->getName() . ' ?');
				$this->tpl->set("btnConfirm",'Yes');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'No');
				$this->tpl->set("linkCancel", $page);
				$this->tpl->set("subNavigation", $this->subNav);
				
				$this->tpl->out("tpl.GroupConfirmation.php");
			
			}
			
		}
		
		function _leaveGroup($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			/** To Leave Group, all of the ff must be true:
			 *	1. User is logged in but not the administrator
			 *	2. Group is defined
			 * 	3. User is a member of the group
			 */
		
			$this->_allowPrivileges($this->isLogged && !$this->isAdminLogged && isset($this->group) && $this->group->isMember($this->loggedUser));
			
			/** edited by ianne - 11/12/2008
		     *  redirected to referer page after action is completed
		     *  added ref = groupsubgroup; if link is from the parent's group home page
		     */
			if (isset($_GET['ref'])){
				if ('grouplist' == $_GET['ref']) 			$page = 'group.php?page='.$_params['page'];
				elseif ('grouphome' == $_GET['ref'])	  	$page = 'group.php?gID=' . $_GET['gID'];
				elseif ('groupsubgroup' == $_GET['ref'])	$page = 'group.php?gID=' . $this->group->getParent()->getGroupID();
				elseif ('mygroups' == $_GET['ref'])         $page = 'group.php?mode=mygroups&page='.$_params['page'];
				else if ('cobrand_search' == $_GET['ref']) {
					$page  = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : 'membership.php' ;
				}			   
			    else 										$page = 'membership.php';
			} else
				$page = 'membership.php';
			
			if (isset($_params['confirm'])) {	
				
				if ($this->group->isStaff($this->loggedUser->getTravelerID()))
					$this->group->removeStaff($this->loggedUser->getTravelerID());
				
				if ($this->group->isModerator($this->loggedUser->getTravelerID()))
					$this->group->removeModerator($this->loggedUser->getTravelerID());
						
				$this->group->removeMember($this->loggedUser->getTravelerID());
			
				//header("location:passport.php");
				$this->_success();
				header("location:".$page);
			
			}
			else{
				
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				$linkSource  = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : 'group.php?gID?' . $this->group->getGroupID() ;
				
				$alertForFacilitator = '';
				$facilitatortype = '';
				if ($this->group->isStaff($this->loggedUser->getTravelerID()) || $this->group->isModerator($this->loggedUser->getTravelerID())){
					$alertForFacilitator = "Leaving this subgroup will remove your ";
					if ($this->group->isStaff($this->loggedUser->getTravelerID()) && $this->group->isModerator($this->loggedUser->getTravelerID()))
						$facilitatortype = " Staff and Moderator ";
					else{
						$facilitatortype = ($this->group->isStaff($this->loggedUser->getTravelerID())) ? ' Staff ' : ' Moderator ' ;
					}
					$alertForFacilitator = $alertForFacilitator . $facilitatortype . 'privileges.';
				}
					
				$this->tpl->set("msgConfirm",'Are you sure you want to remove your membership from the group ' . $this->group->getName() . ' ? ' . $alertForFacilitator);
				$this->tpl->set("btnConfirm",'Remove');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'Cancel');
				$this->tpl->set("linkCancel", $linkSource);
				$this->tpl->set("subNavigation", $this->subNav);
				
				$this->tpl->out("tpl.GroupConfirmation.php");
				
			}
			
		}
		
		function _acceptInvite($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);

			/** To Accept Inivitation to Join Group, all of the ff must be true:
			 *	1. User is logged in
			 *	2. Group is defined
			 * 	3. User is in group's invite list
			 * 	4. User is not yet a group member
			 */
			$this->_allowPrivileges($this->isLogged && isset($this->group) && $this->group->isInInviteList($this->loggedUser) && !$this->group->isMember($this->loggedUser));

			if (isset($_params['confirm'])) {
				// get status of invitation (0 or 2=pending as staff of group)
				$iStatus = Group::getInvitationToGroupStatus($this->group->getGroupID(), $this->loggedUser->getTravelerID());

				/********************************************************************/
				/* Added By: Cheryl Ivy Q. Go										*/
				/* 11 April 2008													*/
				/* Comments:														*/
				/*			(1) give traveler copy of bulletins posted by advisor	*/
				/*				since the day he was invited to join group			*/
				/********************************************************************/

				MessageSpace::addBulletinsPostedByAdvisor($this->group, $this->loggedUser);
				$this->group->addMember($this->loggedUser);

				if (2 == $iStatus){
					// set as staff of subgroup
					$this->group->addStaff($this->loggedUser->getTravelerID());
					
					// check if group is parent group or subgroup
					if (Group::ADMIN == $this->group->getDiscriminator() && $this->group->isParent()){			

						$subGroups = $this->group->getAllSubGroups();
						foreach($subGroups as $indSubGroup){
							MessageSpace::addBulletinsPostedByAdvisor($indSubGroup, $this->loggedUser);
							$indSubGroup->addMember($this->loggedUser);
							// set as staff of subgroup
							$indSubGroup->addStaff($this->loggedUser->getTravelerID());
						}
					}				
				}

				if (2 == $iStatus){
					$this->tpl->set("message", "You have accepted invitation to join the group " . $this->group->getName() . ". You are now a staff of group " . $this->group->getName() . '!');
				}
				else
					$this->tpl->set("message","You have accepted invitation to join the group " . $this->group->getName() . ".");

				$this->tpl->set("grpID",$this->group->getGroupID());
				
				$this->_success();
				header("location:membership.php");
			}					
			else{
				/*$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				$linkSource  = 'membership.php';

				$this->tpl->set("msgConfirm","You are about to accept invitation from the group " . $this->group->getName() . ". Click Join to proceed, click Cancel if you don't really want to join the group " . $this->group->getName() . ".");
				$this->tpl->set("btnConfirm",'Join');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'Cancel');
				$this->tpl->set("linkCancel", $linkSource);*/
				
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?gID=".$this->group->getGroupID()."&mode=accept&confirm";
				$linkCancel  = $_SERVER['SCRIPT_NAME'] . "?gID=".$this->group->getGroupID()."&mode=deny&confirm";
				
				$confirmInviteTemplates = array();
				$inviteTpl = new Template();
				$inviteTpl->set("linkConfirm", $linkConfirm);
				$inviteTpl->set("linkCancel", $linkCancel);
				$inviteTpl->set("group", $this->group);
				$inviteTpl->set("travelerID",$this->loggedUser->getTravelerID());
				$inviteTpl->setTemplate("travellog/views/tpl.IncGroupInviteConfirmation.php");
				$confirmInviteTemplates[] = $inviteTpl;
				
				if( isset($GLOBALS["CONFIG"]) ){
					if( 0 < $this->group->getParentID() ){
						$parent = new AdminGroup($this->group->getParentID());
						if( $parent->isMember($this->loggedUser) ){
							$this->tpl->set("profile",$this->mProfileComponent);
							$this->tpl->set("subNavigation", $this->subNav);
						}
					}elseif( $this->group->isMember($this->loggedUser) ){
						$this->tpl->set("profile",$this->mProfileComponent);
						$this->tpl->set("subNavigation", $this->subNav);
					}
				}else{
					$this->tpl->set("profile",$this->mProfileComponent);
					$this->tpl->set("subNavigation", $this->subNav);
				}
				
				$this->tpl->set("confirmInviteTemplateArray",$confirmInviteTemplates);
				$this->tpl->out("tpl.GroupConfirmation.php");
			}
		
			
		}
		
		function _acceptInvite2($_params = array()){
			
			$this->_defineCommonAttributes($_params);
			
			$inviting_groups = array();
			if( isset($this->group) && 0 == $this->group->getParentID() ){
				$inviting_groups = $this->group->getPendingGroupNetworkInvitations($this->loggedUser);
			}
			$this->_allowPrivileges($this->isLogged && isset($this->group) && 0 == $this->group->getParentID() && !$this->group->isMember($this->loggedUser) );
			
			if( 0 == count($inviting_groups) ){
				header("location: logout.php");
			}
			
			$confirmInviteTemplates = array();
			foreach($inviting_groups AS $group){
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?gID=".$group->getGroupID()."&mode=accept&confirm";
				$linkCancel  = $_SERVER['SCRIPT_NAME'] . "?gID=".$group->getGroupID()."&mode=deny&confirm";
				
				$inviteTpl = new Template();
				$inviteTpl->set("linkConfirm", $linkConfirm);
				$inviteTpl->set("linkCancel", $linkCancel);
				$inviteTpl->set("group", $group);
				$inviteTpl->set("travelerID",$this->loggedUser->getTravelerID());
				$inviteTpl->setTemplate("travellog/views/tpl.IncGroupInviteConfirmation.php");
				$confirmInviteTemplates[] = $inviteTpl;
			}
			if( isset($GLOBALS["CONFIG"]) ){
				if( 0 < $this->group->getParentID() ){
					$parent = new AdminGroup($this->group->getParentID());
					if( $parent->isMember($this->loggedUser) ){
						$this->tpl->set("profile",$this->mProfileComponent);
						$this->tpl->set("subNavigation", $this->subNav);
					}
				}elseif( $this->group->isMember($this->loggedUser) ){
					$this->tpl->set("profile",$this->mProfileComponent);
					$this->tpl->set("subNavigation", $this->subNav);
				}
			}else{
				$this->tpl->set("profile",$this->mProfileComponent);
				$this->tpl->set("subNavigation", $this->subNav);
			}
			$this->tpl->set("confirmInviteTemplateArray",$confirmInviteTemplates);
			$this->tpl->out("tpl.GroupConfirmation.php");
			
		}
		
		function _denyInvite($_params = array() ) {
			
			
			$this->_defineCommonAttributes($_params);
			
			/** To Deny Invitation to Join Group, all of the ff must be true:
			 *	1. User is logged in
			 *	2. Group is defined
			 * 	3. User is in group's invite list
			 */
		
			$this->_allowPrivileges($this->isLogged && isset($this->group) && $this->group->isInInviteList($this->loggedUser));
			
				
			if (isset($_params['confirm'])) {	
				
			 	$this->group->denyInvitation($this->loggedUser->getTravelerID());
				
				$this->tpl->set("grpID",$this->group->getGroupID());	
				$this->tpl->set("message","You have denied invitation to join the group " . $this->group->getName() . ".");
				
				$this->_success();
				header("location:membership.php");
			
			}
			else{
				
				/*$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				$linkSource  = 'membership.php';
				
				$this->tpl->set("msgConfirm",'Are you sure you want to deny invitation from the group ' . $this->group->getName().' ?');
				$this->tpl->set("btnConfirm",'Deny');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'Cancel');
				$this->tpl->set("linkCancel", $linkSource);
				if( "dev.goabroad.net" != $_SERVER["SERVER_NAME"] 
					&& "goabroad.net" != $_SERVER["SERVER_NAME"] 
					&& "test.goabroad.net" != $_SERVER["SERVER_NAME"] ){
					if( 0 < $this->group->getParentID() ){
						$parent = new AdminGroup($this->group->getParentID());
						if( $parent->isMember($this->loggedUser) ){
							$this->tpl->set("subNavigation", $this->subNav);
						}
					}elseif( $this->group->isMember($this->loggedUser) ){
						$this->tpl->set("subNavigation", $this->subNav);
					}
				}else{
					$this->tpl->set("subNavigation", $this->subNav);
				}
				
				$this->tpl->out("tpl.GroupConfirmation.php");*/

				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?gID=".$this->group->getGroupID()."&mode=accept&confirm";
				$linkCancel  = $_SERVER['SCRIPT_NAME'] . "?gID=".$this->group->getGroupID()."&mode=deny&confirm";
				
				$confirmInviteTemplates = array();
				$inviteTpl = new Template();
				$inviteTpl->set("linkConfirm", $linkConfirm);
				$inviteTpl->set("linkCancel", $linkCancel);
				$inviteTpl->set("group", $this->group);
				$inviteTpl->set("travelerID",$this->loggedUser->getTravelerID());
				$inviteTpl->setTemplate("travellog/views/tpl.IncGroupInviteConfirmation.php");
				$confirmInviteTemplates[] = $inviteTpl;
				
				if( isset($GLOBALS["CONFIG"]) ){
					if( 0 < $this->group->getParentID() ){
						$parent = new AdminGroup($this->group->getParentID());
						if( $parent->isMember($this->loggedUser) ){
							$this->tpl->set("profile",$this->mProfileComponent);
							$this->tpl->set("subNavigation", $this->subNav);
						}
					}elseif( $this->group->isMember($this->loggedUser) ){
						$this->tpl->set("profile",$this->mProfileComponent);
						$this->tpl->set("subNavigation", $this->subNav);
					}
				}else{
					$this->tpl->set("profile",$this->mProfileComponent);
					$this->tpl->set("subNavigation", $this->subNav);
				}
				
				$this->tpl->set("confirmInviteTemplateArray",$confirmInviteTemplates);
				$this->tpl->out("tpl.GroupConfirmation.php");

			}
				
		}
		
		function _deleteDeniedRequest($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			/** To Delete denied request to Join Group, all of the ff must be true:
			 *	1. User is logged in
			 *	2. Group is defined
			 * 	3. User is in group's denied request list
			 */
		
			$this->_allowPrivileges($this->isLogged && isset($this->group) && $this->group->isInDeniedRequestList($this->loggedUser));
			
			if (isset($_params['confirm'])) {	
				
				$this->group->removeFromRequestList($this->loggedUser->getTravelerID());		
				
				$this->_success();
				header("location:membership.php");
				
			}
			else{
				
				$linkConfirm = $_SERVER['SCRIPT_NAME'] . "?". $_SERVER['QUERY_STRING'];
				$linkSource  = 'membership.php';
				
				$this->tpl->set("msgConfirm",'Are you sure you want to delete this denied request to join the group ' . $this->group->getName() . ' ?');
				$this->tpl->set("btnConfirm",'Yes');
				$this->tpl->set("linkConfirm", $linkConfirm . "&confirm");
				$this->tpl->set("btnCancel",'No');
				$this->tpl->set("linkCancel", $linkSource);
				$this->tpl->set("subNavigation", $this->subNav);
				
				$tplGrpConfirm->out("tpl.GroupConfirmation.php");
			
			}
					
		}
		
		function _viewMembershipInfo($_params = array() ) {
			$this->_defineCommonAttributes($_params);
			
			/** To View Membership Details, all of the ff must be true:
			 *	1. User is logged in
			 *	2. Group is defined
			 */
		
			$this->_allowPrivileges($this->isLogged );
			
			if( isset($GLOBALS['CONFIG']) && $this->obj_session->getLogin() ){
				$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($GLOBALS['CONFIG']->getGroupID()));
				$group = $_group[0];
				if( !$group->isMember(new Traveler($this->obj_session->get("travelerID"))) ){
					header("location: /membership.php?gID=".$GLOBALS['CONFIG']->getGroupID()."&mode=acceptinvite");
				}
			}
			
			// -------- get Group Invites --------- //
			$grpInvites = $this->loggedUser->getGroupInvites();
			$this->tpl->set("msgGroupInvite","You have these group invitations: ");
			$this->tpl->set('travID',$this->loggedUser->getTravelerID());
			$this->tpl->set('grpInvites',$grpInvites);
			// -------- end get Group Invites --------- //
			
			
			// -------- get Group Requests --------- //				
			$cond = $this->file_factory->getClass('Condition');
			$cond->setAttributeName("status");
			$cond->setOperation(FilterOp::$EQUAL);
			$cond->setValue(0);

			$filter = $this->file_factory->getClass('FilterCriteria2');
			$filter->addCondition($cond);
			
			$grpRequests = $this->loggedUser->getGroupRequests($filter);
			$this->tpl->set("msgGroupRequest","You requested to join these groups: ");
			$this->tpl->set('grpRequests',$grpRequests);
			// -------- end get Group Invites --------- //
			
			
			// -------- get Group Denied Requests --------- //					
			$cond = $this->file_factory->getClass('Condition');
			$cond->setAttributeName("status");
			$cond->setOperation(FilterOp::$EQUAL);
			$cond->setValue(1);

			$filter = $this->file_factory->getClass('FilterCriteria2');
			$filter->addCondition($cond);
								
			$grpDeniedRequests = $this->loggedUser->getGroupRequests($filter);
			$this->tpl->set("msgDeniedGroupRequest","Your request to join group has been denied.");
			$this->tpl->set('grpDeniedRequests',$grpDeniedRequests);
			// -------- end get Group Denied Requests --------- //				
			
			$backLink = $this->file_factory->getClass('BackLink', 'instance');
			$backLink->Create();
			
			// added profile component - ianne 11/14/2008
			//$profile = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			//$profile->init($this->loggedUser->getTravelerID());
			
			$this->tpl->set('success',NULL);
			if($this->obj_session->get('custompopup_message')){
				$this->tpl->set('success',$this->obj_session->get('custompopup_message'));
				$this->obj_session->unsetVar('custompopup_message');
			}	
			
			//$this->tpl->set('profile',$profile->get_view());
			$this->tpl->set('profile',$this->mProfileComponent);		
			$this->tpl->set('backLink',$backLink);
			$this->tpl->set("subNavigation", $this->subNav);

			$this->tpl->out("tpl.MembershipConfirm.php");
			
			
		}
		
	}
?>