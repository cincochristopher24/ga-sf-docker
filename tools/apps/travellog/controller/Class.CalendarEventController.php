<?php
/**
 * <b>Calendar Event</b> controller.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */

require_once("Class.ObjectIterator.php");
require_once("Class.Date.php");
require_once("Class.Template.php");
require_once("Class.Paging.php");
require_once("travellog/model/Class.Calendar.php");
require_once("travellog/model/Class.GroupEvent.php");
require_once("travellog/model/Class.GroupFactory.php");
require_once("travellog/model/Class.Program.php");
require_once("travellog/model/Class.TravelerEvent.php");
require_once("travellog/model/Class.Activity.php");
require_once('travellog/model/navigator/Class.BackLink.php');
require_once("travellog/model/Class.SubNavigation.php");
 
class CalendarEventController
{
	
	private static 
	
	$GROUP        = 1,
	
	$TRAVELER     = 2,
	
	$ACTIVITY     = 0;
	
	private 
	
	$travelerID   = NULL,
	
	$isOwner      = false,
	
	$main_tpl     = NULL,
	
	$obj_backlink = NULL,
	
	$primaryID    = NULL,
	
	$eventID      = NULL,
	
	$context      = NULL,
	
	$obj_date     = NULL,
	
	$mode         = NULL,
	
	$IsLogin      = false,
	
	$obj_calendar = NULL;
	
	
	function __construct( $obj = NULL ){
		$this->main_tpl     = new Template();
		$this->obj_date     = ClassDate::instance();
		$this->obj_backlink = BackLink::instance();
		$this->obj_calendar = new Calendar( $obj );
		$this->obj_calendar->setPrivacyContext( $obj );
		$this->obj_calendar->setOrder(Calendar::$DATE_DESC); 
		if ( $this->IsLogin ) $this->obj_calendar->setPrivacyContext( $obj );
	}
	
	function View( $page )
	{
		$proceed  = false;
		if( strcasecmp($this->mode,'edit') == 0 && $this->travelerID):
			$proceed = $this->checkIfAdmin();
		endif;

		$subNavigation = new SubNavigation();
		$subNavigation->setContext('TRAVELER');
		$subNavigation->setContextID($this->travelerID);
		$subNavigation->setLinkToHighlight('CALENDAR');
			
		$contents = $this->viewContents($page); 
		$this->obj_backlink->Create();
		$this->main_tpl->set( 'title'   , 'View Events'       );
		$this->main_tpl->set( 'group_title'  , ''                  );
		$this->main_tpl->set( 'obj_backlink' , $this->obj_backlink );
		$this->main_tpl->set( 'contents'     , $contents           );

		if($this->mode == 'edit' && $this->IsLogin && $proceed):
			switch($this->context){
				case self::$GROUP:
					$load = 'manager.displayGroupForm("mode=edit&amp;eID='.$this->primaryID.'");';
					break;
				case self::$TRAVELER:
					$load = 'manager.displayTravelerForm("mode=edit&amp;eID='.$this->primaryID.'");';
					break;
				case self::$ACTIVITY:
					$load = 'manager.displayActivityForm("mode=edit&amp;eID='.$this->primaryID.'");';
					break;
			}	
		elseif($this->mode == 'add' && $this->IsLogin):
			$load = 'manager.displayTravelerForm(\'mode=add\');';
		else:
			$load = 'manager.toggleView();';
		endif;
		$code = <<<BOF
<script type="text/javascript">
//<![CDATA[
	
	var manager = new TravelerEventManager();
	
	window.onload = function(){ 
		manager.useLoadingMessage('view1');
		$load
	}
//]]>
</script>		
BOF;
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		//Template::includeDependentJs("/js/prototype1.lite.js");
		Template::includeDependentJs("/min/f=js/prototype.js", array('top' => true));
		Template::includeDependentJs("/js/moo1.ajax.js");
		Template::includeDependentJs("/js/travelerEventManager.js");
		Template::includeDependentJs("/js/moo.fx.js");
		Template::includeDependentJs("/js/moo.fx.pack.js");
		Template::includeDependentJs("/js/Validation.js");
		Template::includeDependent($code);
		$this->main_tpl->set("subNavigation",$subNavigation);
		$this->main_tpl->out( 'travellog/views/tpl.Calendar.php' );
	}
	
	function checkIfAdmin(){
		$isAdmin = false;
		switch($this->context){
			case self::$GROUP:
				$obj_event = new GroupEvent( $this->primaryID );
				$factory   = GroupFactory::instance();
				$obj_group = $factory->create( array($obj_event->getGroupID()) );
				if ( $obj_group[0]->getAdministrator()->getTravelerID() == $this->travelerID ): 
					$isAdmin = true;
					if(strcasecmp($this->mode,"delete")==0): 
						$obj_event->Delete();
					endif;
				else:
					$this->primaryID = NULL;
				endif;
				break;
			case self::$TRAVELER:
				$obj_event   = new TravelerEvent( $this->primaryID );
				if ( $obj_event->getTravelerID() == $this->travelerID ): 
					$isAdmin = true;
					if(strcasecmp($this->mode,"delete")==0): 
						$obj_event->Delete();
					endif;
				else:
					$this->primaryID = NULL;
				endif;
				break;
			case self::$ACTIVITY:
				$factory     = GroupFactory::instance();
				$obj_event   = new Activity(  $this->primaryID );
				$obj_program = new Program( $obj_event->getProgramID() );
				$obj_group   = $factory->create( array($obj_program->getGroupID()) );
				if ( $obj_group[0]->getAdministrator()->getTravelerID() == $this->travelerID ): 
					$isAdmin = true;
					if(strcasecmp($this->mode,"delete")==0): 
						$obj_event->Delete();
					endif;
				else:
					$this->primaryID = NULL;
				endif;
				break;
		}	
	
		return $isAdmin;
		
	}
	
	function viewContents( $page )
	{
		$inc_tpl               = clone $this->main_tpl;
		$arr_obj_calendar      = NULL;
		
		$arr_obj_calendar_temp = $this->obj_calendar->getEvents();
				
		if ( !$this->IsLogin && count($arr_obj_calendar_temp) ):
			foreach( $arr_obj_calendar_temp as $obj_calendar_event  ):
				if ( $obj_calendar_event->getDisplayPublic() == 1 ): 
					$arr_obj_calendar[] = $obj_calendar_event;
				endif;
			endforeach;
		elseif ( count($arr_obj_calendar_temp) ):
			$arr_obj_calendar = $arr_obj_calendar_temp;
		endif;
	
		$this->obj_backlink->Create();
		
		$inc_tpl->set( 'action_new'   , "manager.displayTravelerForm('mode=add')" );
		$inc_tpl->set( 'event_type'   , 'Calendar'                                );
		$inc_tpl->set( 'obj_backlink' , $this->obj_backlink                       );
				
		if ( count($arr_obj_calendar) ):		
			if ( $this->primaryID != NULL  ) $page=$this->checkPosition($arr_obj_calendar,10);
			$obj_paging       = new Paging( count($arr_obj_calendar),$page,'mode=view&tID='.$this->travelerID,10 );
			$obj_iterator     = new ObjectIterator($arr_obj_calendar,$obj_paging);
			$obj_paging->setOnclick('manager.changeContents');
			$obj_iterator->rewind();
			if( $this->primaryID == NULL ):
				$this->primaryID = $obj_iterator->current()->getEventID();
				$this->context   = $obj_iterator->current()->getContext();
			endif;
			
			$inc_tpl->set( 'obj_iterator' , $obj_iterator                             );
			$inc_tpl->set( 'obj_paging'   , $obj_paging                               );
			$inc_tpl->set( 'context'      , $this->context                            );
			$inc_tpl->set( 'primaryID'    , $this->primaryID                          );
		else:
			if ($this->isOwner)
				$inc_tpl->set( 'message'  , 'You have no calendar events yet.'        );
			else	
				$inc_tpl->set( 'message'  , 'No calendar events yet.'                 );
		endif;
		$inc_tpl->set( 'isOwner'      , $this->isOwner                            );
		return $inc_tpl->fetch('travellog/views/tpl.ViewTravelerEventContent.php');
		
	}
	private function checkPosition( $arr_objects,$rowsperpage )
	{
		$page = 1;
		for($ctr=0; $ctr < count($arr_objects); $ctr++ ):
			if ( $arr_objects[$ctr]->getEventID() == $this->primaryID ):
				$page = ceil(($ctr+1)/$rowsperpage);
				$this->context = $arr_objects[$ctr]->getContext();				
				break;
			endif;
		endfor;
		return $page;
	}
	
	function setPrimaryID($primaryID)
	{
		$this->primaryID = $primaryID;
	}
	
	function setTravelerID($travelerID)
	{
		$this->travelerID = $travelerID;
	}
	
	function setIsOwner($owner)
	{
		$this->isOwner = $owner;
	}
	
	function setIsLogin($IsLogin)
	{
		$this->IsLogin = $IsLogin;
	}
	
	function setMode($mode)
	{
		$this->mode = $mode;
	}
	
	function setContext($context)
	{
		$this->context = $context;
	}
	
} 

 
?>


