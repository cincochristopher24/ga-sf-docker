<?php
/*
 * Created on 11 12, 07
 * 
 * Author:
 * Purpose: The default controller for "Passport" module in goabroad.net
 * 
 */
 
 require_once('travellog/controller/Class.AbstractPassportController.php');
 
 class GANETPassportController extends AbstractPassportController {
 	function performAction(){
		parent::performAction();
	}
 }
?>
