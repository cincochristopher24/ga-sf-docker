<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	/**
	  * - The user must be logged to participate in a survey
	  * - The user must be a member of the of one the group participants of the survey form
	  * - Depending on the survey form setting, a valid traveler participant can edit his survey applications.
	  * - The valid traveler participant cannot participate in a locked survey form.
	  */
	
	abstract class AbstractSurveyFormController
	{
		protected $obj_session 		= NULL;
		protected $file_factory = NULL;
		protected $surveyForm = NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('SurveyForm', array('path'=>'travellog/model/formeditor/'));
			$this->file_factory->registerClass('SurveyFormAnswer', array('path'=>'travellog/model/formeditor/'));
			$this->file_factory->registerClass('SurveyFieldAnswer', array('path'=>'travellog/model/formeditor/'));
			$this->file_factory->registerClass('SurveyFieldAnswerValue', array('path'=>'travellog/model/formeditor/'));
			
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Template',array('path'=>''));
			
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		}
		
		public function performAction(){
			//check if the user has logged in
			//$travelerID = (isset($_SESSION) && array_key_exists('travelerID',$_SESSION) && $_SESSION['travelerID'] !=0 ? $_SESSION['travelerID']:0 );
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/login.php'));
			}
			
			// insert code for ga logger
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($travelerID,'TRAVELER');
			
			//check if the frmID is available	
			if( (!isset($_GET['frmID']) || !is_numeric($_GET['frmID'])) && (!isset($_POST['frmID']) || !is_numeric($_POST['frmID'])) ){		
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			//check if the frmID is a valid survey form ID
			
			//check if the user is allowed to paricipate in the survey
			$oTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			
			$frmID = (isset($_GET['frmID']) 
				? $_GET['frmID'] 
				: (isset($_POST['frmID']) 
					? $_POST['frmID'] 
					: 0)
			);
			
			if(!array_key_exists('gID',$_GET)){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			try{
				$groups = $oGroupFactory->create(array($_GET['gID']));
				$oGroup = count($groups) ? $groups[0] : null;
			}
			catch(exception $e){
				$oGroup = null;
			}
			if(is_null($oGroup) || $oGroup->getDiscriminator() != GROUP::ADMIN){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			$isAdvisor = $oGroup->getAdministratorID() == $oTraveler->getTravelerID() || $oGroup->isStaff($oTraveler->getTravelerID());
			/***
			$gID =  (isset($_GET['gID']) 
				? $_GET['gID'] 
				: (isset($_POST['gID']) 
					? $_POST['gID'] 
					: 0)
			);
			***/
			
			/***
			$isAdvisor = $oTraveler->isAdvisor();
			if(!$isAdvisor){
				$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
				$oGroup = $oGroupFactory->create($gID);
				$isAdvisor = $oGroup->isSuperStaff($oTraveler->getTravelerID());
			}
			
			if(!$isAdvisor){
				if( (!isset($_GET['gID']) || !is_numeric($_GET['gID'])) && (!isset($_POST['gID']) || !is_numeric($_POST['gID'])) ){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));	
				}
			}
			***/
			
			if(!$this->file_factory->invokeStaticClass('SurveyForm','isExists',array($frmID))){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			$oSurveyForm = $this->file_factory->getClass('SurveyForm',array($frmID));
			$this->surveyForm = $oSurveyForm;
			
			//check the gID is valid
			/***
			require_once('travellog/model/Class.GroupFactory.php');
			$localFactory =  $this->file_factory->invokeStaticClass('GroupFactory','instance');
			try{
				$groupParticipant = $localFactory->create(array($gID));
			}
			catch(exception $e){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			***/
			$groupParticipant = $oGroup;
			
			//is the groupParticipant really a participant of the group?
			if(!$isAdvisor && !$oSurveyForm->isGroupParticipant($oGroup->getGroupID())){
				$isValid = false;
			}
			else{
				$isValid = $isAdvisor ? true : $groupParticipant->isMember($oTraveler);	
			}
			
			if(!$isValid){
				$this->showTemplateMessage(array('message'=>'Sorry, but you do not belong to any of the groups that are allowed to participate.'));
				exit;		
			}
			//check if the survey has been locked
			if($oSurveyForm->isLocked() && !$isAdvisor){
				$this->showTemplateMessage(array('message'=>'Sorry, but this survey is closed.'));
				exit;
			}
			
			//check if the participant has already participated in the survey.
			$svFrmAns = $this->file_factory->invokeStaticClass('SurveyFormAnswer','getSurveyFormAnswerBySurveyFormIDAndTravelerID',array($oSurveyForm->getSurveyFormID(),$oTraveler->getTravelerID()));
			if(!is_null($svFrmAns)){
				$this->showTemplateMessage(array('message'=>'Sorry, but you are not allowed to participate in this survey anymore since you have already participated.'));
				exit;
			}
			
			$defVars = array();
			$errFields = array();
			
			$formAnswer = $svFrmAns;
			
			$fieldAnswers = (!is_null($formAnswer) ? $formAnswer->getSurveyFieldAnswers() : array());
			
			$formFields = $oSurveyForm->getSurveyFormFields();
			$mappedFormFields = array();
			foreach($formFields as $iFormField){
				$mappedFormFields[$iFormField->getSurveyFieldID()] = $iFormField;
			}
			$mappedFieldAnswerValues = array();
			foreach($fieldAnswers as $iFldAnswer){
				$mappedFieldAnswerValues[$iFldAnswer->getSurveyFieldID()] = $iFldAnswer->getAnswerValues();
			}
			
			//set the answers of the fields
			//set the answers to the default values
			$formFieldValues = array();	
			
			if(count($fieldAnswers)){
				//set the answers to the saved values
				foreach($mappedFormFields as $key => $val){
					$tempValObjs = array_key_exists($key,$mappedFieldAnswerValues) ? $mappedFieldAnswerValues[$key] : array();			
					$tempVals = array();
					foreach($tempValObjs as $iValObj){
						$tempVals[] = $iValObj->getValue();
					}
					$formFieldValues[$key] = $tempVals;
				}
			}
			else{	
				foreach($mappedFormFields as $key => $iFld){
					$formFieldValues[$key] = array();
					$fldInputs = $iFld->getFieldInputs();
					switch($iFld->getFieldType()){
						case SurveyField::TEXTBOX:
							$formFieldValues[$key][] = $fldInputs[0]->getAttribute('value');
						break;
						case SurveyField::CHECKBOX:
							foreach($fldInputs as $iInput){
								if($iInput->getAttribute('isChecked')){
									$formFieldValues[$key][] = $iInput->getAttribute('text');
								}
							}
						break;
						case SurveyField::RADIO:
							foreach($fldInputs as $iInput){
								if($iInput->getAttribute('isChecked')){
									$formFieldValues[$key][] = $iInput->getAttribute('text');
								}
							}
						break;
						case SurveyField::COMBO:
							$selIdx = 0;
							$options = $fldInputs[0]->getAttribute('options');
							$formFieldValues[$key][] = $options[$fldInputs[0]->getAttribute('selectedIndex')];
						break;
					}
				}
			}
			
			//if submitting...
			if(isset($_POST['frmID'])){
				//set the answers to the submitted values
				$formFieldValues = array();
				foreach($mappedFormFields as $key => $val){
					$tempVal = array_key_exists('fld_'.$key,$_POST) ? $_POST['fld_'.$key] : array();
					$formFieldValues[$key] = (!is_array($tempVal) ? array($tempVal) : $tempVal);
				}
				//check for the required fields with no value
				
				foreach($mappedFormFields as $key => $iFld){
					if($iFld->isRequired()){
						if($iFld->getFieldType() == SurveyField::CHECKBOX || $iFld->getFieldType() == SurveyField::RADIO){
							if(!count($formFieldValues[$key])){
								$errFields[] = $key;		
							}				
						}
						elseif($iFld->getFieldType() == SurveyField::TEXTBOX || $iFld->getFieldType() == SurveyField::COMBO){
							if(!strlen($formFieldValues[$key][0])){
								$errFields[] = $key;
							}
						}
					}
				}
				
				
				if(!count($errFields)){					
					if(!array_key_exists('isAdvisor',$_POST)){
						if(is_null($formAnswer)){
							$surveyParticipant = $oSurveyForm->getGroupParticipant($oGroup->getGroupID());
							$formAnswer = $this->file_factory->getClass('SurveyFormAnswer');
							$formAnswer->setTravelerID($travelerID);
							$formAnswer->setSurveyParticipantID($surveyParticipant->getSurveyParticipantID());
							$formAnswer->save();
						}
						foreach($mappedFormFields as $key => $iFld){
							if(count($iFld->getFieldInputs())){
								$currentFldAnswer = $formAnswer->getSurveyFieldAnswerBySurveyFieldID($key);
								if(is_null($currentFldAnswer)){
									$currentFldAnswer = $this->file_factory->getClass('SurveyFieldAnswer');
									$currentFldAnswer->setSurveyFormAnswerID($formAnswer->getSurveyFormAnswerID());
									$currentFldAnswer->setSurveyFieldID($key);
									$currentFldAnswer->save();
								}
								else{
									$currentFldAnswer->deleteValues();
								}
								$fieldAnswerValues = $formFieldValues[$key];
								foreach($fieldAnswerValues as $iVal){
									$newAnswerValue = $this->file_factory->getClass('SurveyFieldAnswerValue');
									$newAnswerValue->setSurveyFieldAnswerID($currentFldAnswer->getSurveyFieldAnswerID());
									$newAnswerValue->setValue($iVal);
									$newAnswerValue->save();
								}
							}
						}
					}
					$msg = (!array_key_exists('isAdvisor',$_POST)
								?'Thank you for particpating in this survey.'
								:'Your test participation was successfully completed. Your answers were not saved since you are not a qualified participant.'
							);
					$this->showTemplateMessage(array('message'=>$msg, 'gID'=>$_GET['gID']));
					exit;
				}
			}
			
			$this->showFormTemplate(array(
				'gID' 				=> $oGroup->getGroupID(),
				'isAdvisor' 		=> $isAdvisor,
				'errFields'			=> $errFields,
				'formFieldValues'	=> $formFieldValues,
			));
		}
		
		public function createTemplate($vars=array()){
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/');
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');
			$subNavigation->setContextID($this->surveyForm->getGroupID());
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight('SURVEY_CENTER');	
			$tpl->set("subNavigation",$subNavigation);
			
			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($vars['gID']);
			$tpl->set('profile', $profile_comp->get_view());
			
			return $tpl;
		}
		
		
		public function showTemplateMessage($vars=array()){
			$tpl = $this->createTemplate($vars);
			$tpl->set('action','SHOW_MESSAGE');
			$tpl->set('message',$vars['message']);
			$tpl->out('tpl.SurveyForm.php');
		}
		
		public function showFormTemplate($vars=array()){
			//all is well, show the survey form
			$tpl = $this->createTemplate($vars);
			$tpl->set('surveyForm',$this->surveyForm);
			$tpl->set('action','SHOW_FORM');
			
			$tpl->set('gID',$vars['gID']);
			$tpl->set('isAdvisor',$vars['isAdvisor']);
			$tpl->set('errFields',$vars['errFields']);		
			$tpl->set('formFieldValues',$vars['formFieldValues']);
			
			/*$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($vars['gID']);
			$tpl->set('profile', $profile_comp->get_view());*/
			
			$tpl->out('tpl.SurveyForm.php');
		}
		
	}
?>