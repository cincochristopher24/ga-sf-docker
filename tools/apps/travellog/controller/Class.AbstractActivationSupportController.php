<?php
	/*
	 * Class.AbstractActivationSupportController.php
	 * Created on Nov 24, 2007
	 * created by marc
	 */
	 
	require_once("travellog/controller/Class.IController.php"); 
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Constants.php");
	 
	class AbstractActivationSupportController implements IController{
		
		static $file_factory = NULL;
		static $asHelper = NULL;
		
		function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass("Validator", array("path" => ""));
			$this->file_factory->registerClass("ActivationSupportHelper", array("path" => "travellog/helper"));
			$this->file_factory->registerClass("Captcha", array("path" => "captcha/"));
			$this->file_factory->registerClass("MailGenerator", array("path" => "travellog/model/"));
			$this->file_factory->registerClass("ActivationSupportFcd", array("path" => "travellog/facade/"));
			$this->file_factory->registerClass("MainLayoutView", array("path" => "travellog/views/"));
			$this->file_factory->registerClass("Traveler", array("path" => "travellog/model"));
			$this->file_factory->registerClass("Crypt", array("path" => ""));
			
			$this->asHelper = $this->file_factory->invokeStaticClass("ActivationSupportHelper", "getInstance");
			
			// views
			$this->file_factory->registerTemplate("LayoutMain");
			$this->file_factory->setPath("CSS", "/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/views/");
			
		}
		
		function performAction(){
			
			switch( $this->asHelper->getAction()){
				case constants::ACTIVATE_ACCOUNT:
					$this->activateAccount();
					break;
				case constants::HELP_ACTIVATE:
					$this->activateAccountHelp();
					break;
				default:
					$this->viewActivationSupportFormPage();
					break;		
			}
		}
		
		function validate(){
			
			$validator = $this->file_factory->getClass("Validator");
			$asData = $this->asHelper->readASData();
			
			if( !strlen($asData["email"]) )
				$asData["arrError"][] = constants::EMPTY_EMAIL_ADDRESS;
			elseif( !$validator->isEmail($asData["email"]) )	
				$asData["arrError"][] = constants::INVALID_EMAIL_FORMAT;
			elseif( !$asData["travelerID"] = $this->file_factory->invokeStaticClass("Traveler","exist", array(addslashes($asData["email"]))) )
				$asData["arrError"][] = constants::TRAVELER_DOES_NOT_EXIST;	
			if( !strlen($asData["message"]) && constants::HELP_ACTIVATE == $this->asHelper->getAction() )	
				$asData["arrError"][] = constants::EMPTY_MESSAGE;
			if( !($this->file_factory->invokeStaticClass("Captcha","validate",array($asData["secCode"],$asData["code"])) ))
				$asData["arrError"][] = constants::SECURITY_CODE_MISMATCH;
			return $asData;	
			
		}
		
		function sendActivationMail($travelerID){
			
			$mailGenerator = $this->file_factory->getClass("MailGenerator",array($travelerID,4));
			return $mailGenerator->send();
		}
		
		function helpActivateAccount($travelerID,$tmpMessage){
			
			$mailGenerator = $this->file_factory->getClass("MailGenerator",array($travelerID,2));
			$mailGenerator->setSubject("Support needed in account activation");
			$mailGenerator->setMessage($tmpMessage);
			return $mailGenerator->send();
		}
		
		function viewActivationSupportFormPage( $asData = array() ){
			
			$security = $this->file_factory->invokeStaticClass("Captcha","createImage");
			
			if( $travID = $this->asHelper->getTravelerID() ){
				// encrypt travelerID for confirmation
				$crypt = $this->file_factory->getClass("Crypt");
				try{
					$nTrav = $this->file_factory->getClass("Traveler",array($crypt->decrypt($travID)));
					$asData["email"] = $nTrav->getTravelerProfile()->getEmail();
				}
				catch( exception $e ){}
			}
			
			$content = array(
				"tmpEmail"		=>	isset($asData["email"]) ? $asData["email"] : "",
		 		"tmpMessage"	=> 	isset($asData["message"]) ? $asData["message"] : "",
		 		"arrError"		=> 	isset($asData["arrError"]) ? $asData["arrError"] : array(),
		 		"src"			=> 	$security["src"],
				"code"			=>	$security["code"]
			);
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			
			Template::includeDependentJs('/min/f=js/prototype.js', array('include_here' => true));
			Template::includeDependentJs('js/tools/common.js');
			Template::includeDependentJs('js/utils/json.js');
			Template::includeDependentJs('captcha/captcha.js');
						
			$script = <<<SCRIPT
			<script type="text/javascript">
			//<[!CDATA[
			
				function doSubmitOnce(currentForm){
					// Run through elements and disable submit buttons
					for(i=0;i<currentForm.length;i++)
					{
						var currentElement = currentForm.elements[i];
						if(currentElement.type.toLowerCase() == "submit")
						currentElement.disabled = true;
					}
				}
			
				// We need to register our submit once function on all forms
				if(document.all||document.getElementById) 
				{	
					// Run through all forms on page
					for(i=0;i<document.forms.length;i++)
					{
						var currentForm = document.forms[i];
						// Register event handler
						// Use quirksmode idea for flexible registration by copying existing events
						var oldEventCode = (currentForm.onsubmit) ? currentForm.onsubmit : function () {};
						currentForm.onsubmit = function () {oldEventCode(); doSubmitOnce(currentForm)};
					}
				}
				
				var Captcha = new CaptchaLoader({					
					imageTagID	: 'captchaImg',
					onLoad		: function(params){			
						$('encSecCode').value  = params['code'];
					},
					imgAttributes : {
						width : '200',
						height: '50'	
					}
				});
							
				//]]>
			</script>	
SCRIPT;

			Template::includeDependent($script);
			Template::setMainTemplateVar('layoutID', 'page_default');
			Template::includeDependentJs('js/interactive.form.js');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
			
			$fcd = $this->file_factory->getClass('ActivationSupportFcd', $content, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveActivationSupportFormPage()) );
			$obj_view->render();
			
		}
		
		function activateAccount(){
			
			$content = $this->validate();
			if( count($content["arrError"]) )
				$this->viewActivationSupportFormPage($content);	
			else{
				if( $sent = $this->sendActivationMail($content["travelerID"]) )
					$content["msg"] = "We have resent an activation email to <em>" . $content["email"] . "</em>. Please follow the instructions in the email to activate your account. Thank you.";					
				else
					$content["msg"] = "An error occurred, please <a href='activation-support.php'>try again...</a>";
						
				$obj_view = $this->file_factory->getClass('MainLayoutView');
				
				Template::setMainTemplateVar('layoutID', 'main_pages');
				Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );  
				
				$fcd = $this->file_factory->getClass('ActivationSupportFcd', $content, false);
				$obj_view->setContents( array('contents' => $fcd->retrieveActivationSupportConfirmationPage()) );
				$obj_view->render();
			}
		}
		
		function activateAccountHelp(){
			
			$content = $this->validate();
			if( count($content["arrError"]) )
				$this->viewActivationSupportFormPage($content);	
			else{
				if( $sent = $this->helpActivateAccount($content["travelerID"],$content["message"]) )
					$content["msg"] = "Thank you! Your email has been sent to the GoAbroad Network Team. You will get a response within 24 hours.";					
				else
					$content["msg"] = "An error occurred, please <a href='activation-support.php'>try again...</a>";
						
				$obj_view = $this->file_factory->getClass('MainLayoutView');
				
				Template::setMainTemplateVar('layoutID', 'main_pages');
				Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );  
				
				$fcd = $this->file_factory->getClass('ActivationSupportFcd', $content, false);
				$obj_view->setContents( array('contents' => $fcd->retrieveActivationSupportConfirmationPage()) );
				$obj_view->render();
			}
				
		}
				
	} 
?>
