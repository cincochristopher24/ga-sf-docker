<?php
/**
 * <b>Activity</b> controller.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
require_once("travellog/controller/Class.AbstractEvent.php");
require_once('Class.Template.php');
require_once('Class.Date.php');

class ActivityController extends AbstractEvent
{
	private 
	
	$activityID    = NULL;

	
	function isValidForm( $formvars ){
		if ( !STRLEN(TRIM($formvars['txtTitle']))       ) array_push( $this->errors,'Title is a requred field!'       );
		if ( !STRLEN(TRIM($formvars['txtDate']))        ) array_push( $this->errors,'Date is a requred field!'        );
		if ( !STRLEN(TRIM($formvars['txaDescription'])) ) array_push( $this->errors,'Description is a requred field!' );
		if ( !STRLEN(TRIM($formvars['txtVenue']))       ) array_push( $this->errors,'Venue is a requred field!'       );
		return ( count($this->errors) )? true : false;
	}
	
	function getData(){
		$arr_values   = array();
		
		$obj_activity = new Activity ( $this->activityID );
		$obj_program  = new Program($obj_activity->getProgramID());
		if ( count($obj_activity) ):
			$arr_values['txtTitle']       = $obj_activity->getTitle();
			$arr_values['txaDescription'] = $obj_activity->getDescription();
			$arr_values['txtVenue']       = $obj_activity->getVenue();
			$arr_values['radPublic']      = $obj_activity->getDisplayPublic();
			$arr_values['hidActivityID']  = $this->activityID;
			$arr_values['hidProgramID']   = $obj_activity->getProgramID();
			$arr_values['txtDate']        = FormHelpers::CreateRangeCalendar($obj_activity->getTheDate(),$obj_program->getStart(),$obj_program->getFinish());
		endif;
		return $arr_values;
	}
	
	function displayForm( $formvars = array() ){
		$form_tpl = clone $this->main_tpl;
		$form_tpl->set( 'post'              , $formvars                                               );
		$form_tpl->set( 'url_action'        , 'activity.php?action='.$this->mode                      );
		
		$this->main_tpl->set( 'title'       , 'Activity Form'                                         );
		$this->main_tpl->set( 'errors'      , $this->errors                                           );		
		$this->main_tpl->set( 'form_tpl'    , $form_tpl->fetch('travellog/views/tpl.FrmActivity.php') );
		
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		
		$this->main_tpl->out( 'travellog/views/tpl.Activity.php' );
	}
	
	function getForm($formvars = array()){
		$form_tpl = clone $this->main_tpl;
		$form_tpl->set( 'post'    , $formvars                        );
		$form_tpl->set( 'mode'    , $this->mode                      );
		$form_tpl->set( 'context' , 0                                );
		return $form_tpl->fetch('travellog/views/tpl.FrmActivity.php');
	}
	
	function addEntry( $formvars ){
		$obj_activity = new Activity();
		$obj_activity->setTitle         ( $formvars['txtTitle']                                       );
		$obj_activity->setTheDate       ( $this->obj_date->CreateODBCDate($formvars['txtDate'], '/' ) );			
		$obj_activity->setDescription   ( $formvars['txaDescription']                                 );
		$obj_activity->setVenue         ( $formvars['txtVenue']                                       );
		$obj_activity->setDisplayPublic ( $formvars['radPublic']                                      );
		$obj_activity->setProgramID     ( $formvars['hidProgramID']                                   );
		$obj_activity->Create();
	}
	
	function updateEntry( $formvars ){
		$obj_activity = new Activity    ( $this->activityID                                           );
		$obj_activity->setTitle         ( $formvars['txtTitle']                                       );
		$obj_activity->setTheDate       ( $this->obj_date->CreateODBCDate($formvars['txtDate'], '/')  );			
		$obj_activity->setDescription   ( $formvars['txaDescription']                                 );
		$obj_activity->setVenue         ( $formvars['txtVenue']                                       );
		$obj_activity->setDisplayPublic ( $formvars['radPublic']                                      );
		$obj_activity->Update();
	}
	
	function removeEntry(){
		$obj_activity = new Activity ( $this->activityID );
		$obj_activity->Delete();
	}
	
	function setActivityID( $activityID ){
		$this->activityID = $activityID;
	}
}

?>
