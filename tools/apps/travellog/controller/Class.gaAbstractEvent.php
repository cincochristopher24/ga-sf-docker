<?php
require_once("travellog/controller/Class.gaActionType.php");
require_once("travellog/controller/Class.gaIEvent.php");
class gaAbstractEvent implements gaIEvent{
	protected
	
	$source = NULL,
	
	$data   = NULL;
	
	function __construct($source, $data){
		$this->source = $source;
		$this->data   = $data;
	}
	
	function getData(){    
		return $this->data;
	} 
	
	function getSource(){
		return $this->source;
	}
} 
?>
