<?php
require_once("travellog/controller/Class.gaIObserver.php");
abstract class AbstractObserver implements gaIObserver{
	protected 
	
	$pkid   = 0,
	
	$action = 0;
	
	function setAction($action){
		$this->action = $action;
	}
	function getAction(){
		return $this->action;
	}
	
	function setPKID($pkid){
		$this->pkid = $pkid;
	}
	function getPKID(){
		return $this->pkid;
	}
}
?>
