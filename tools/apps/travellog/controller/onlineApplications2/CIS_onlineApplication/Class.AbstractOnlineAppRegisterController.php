<?php
require_once('travellog/model/Class.SessionManager.php');
require_once("travellog/factory/Class.FileFactory.php");
require_once('Class.Crypt.php');

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OnlineAppToStudentDBMapper.php');
class AbstractOnlineAppRegisterController {
	
	protected	$file_factory = NULL;
	protected	$session_manager = NULL;
	protected	$errMsg = array();
	
	public function __construct(){
		$this->session_manager = SessionManager::getInstance();
		
		$this->file_factory = Filefactory::getInstance();
		
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views/onlineApp'));
		$this->file_factory->setPath('CSS', '/custom/CIS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/onlineApp/');
		
		$this->file_factory->registerClass('HelperGlobal', array('path'=>''));
		$this->file_factory->registerClass('Template', array('path'=>''));
		$this->file_factory->registerClass('ToolMan', array('path'=>''));
		
		$this->file_factory->registerClass("OnlineAppUser", array("path" => "travellog/model/onlineApplications/CIS_onlineApplication"));
		
	//	$this->file_factory->registerClass("OnlineAppToStudentDB", array("path" => "travellog/model/onlineApplications/CIS_onlineApplication"));
	}
	
	public function performAction(){
		
		$action = (isset($_GET['action']) ? $_GET['action'] : 'showRegistrationForm');
		
		$method = $action;
		
		$this->{$method}();
	//	var_dump($action);
		
	}
	
	public function showRegistrationForm(){
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$this->file_factory->invokeStaticClass('Template','setMainTemplateVar', array('layoutID', 'online_application'));
		
		$tplLogin = $this->file_factory->getClass('Template');
		$tplLogin->set('txtEmail',(isset($_POST['txtEmail']) ? $_POST['txtEmail'] : ''));
		$tplLogin->set('psword',(isset($_POST['psword']) ? $_POST['psword'] : ''));
		$tplLogin->set('errMsg',(isset($this->errMsg['existingUser']) ? $this->errMsg['existingUser'] : ''));
		$tplLogin->out('travellog/views/onlineApplications/cis/tpl.registrationForm.php');
	}
	
	public function processRegistration(){
		
		$email = trim($_POST['txtEmail']);
		$pass  = trim($_POST['psword']);
		
		$token = isset($_GET['token']) ? $_GET['token'] : null;
	//	var_dump($pass);
		
		if(0 < $this->session_manager->get('onlineAppUserID')){
			$this->session_manager->unsetVar('onlineAppUserID');
		}
		
		$onlineAppUser = $this->file_factory->getClass('OnlineAppUser');
		
		
		if(!$onlineAppUser->isAlreadyUser($email) AND strlen($pass) > 0 AND strlen($email) > 0 ){
			$onlineAppUser->setEmail(    $email );
			$onlineAppUser->setPassword( $pass  );
			$onlineAppUser->save();
			
			if(!is_null($token)){
				$this->processToken($onlineAppUser, $token);
			}
			
			
			$this->session_manager->set('onlineAppUserID', $onlineAppUser->getID());

			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php'));
		}
		else {
			if(strlen($pass) == 0){
				$this->errMsg['existingUser'] = ' (Check your password length. It must not start with a space.)';
			}
			else if(strlen($email) == 0){
				$this->errMsg['existingUser'] = ' (Invalid Email.)';
			}
			else {
		//		echo 'email';
				$this->errMsg['existingUser'] = ' (Sorry, but it seems that the email you supplied is already in use.)';
			}
		//	var_dump($this->errMsg);
			$this->showRegistrationForm();
		}
		
		
	}
	
	private function processToken($onlineAppUser, $urlToken){
		$crypt = new Crypt();
		$param = explode('&', urldecode($crypt->decrypt($urlToken)));
		
		if(count($param) > 1){
			$token = explode('=',$param[0]);
			
			$studentID = (isset($token[1]) AND (int)$token[1] > 0) ? $token[1] : null;
			
			if(!is_null($studentID)){
				$obj = OnlineAppToStudentDBMapper::retrieveByStudentID($studentID);				
				if(is_null($obj)){
					$obj = new OnlineAppToStudentDB();
					$obj->setOnlineAppUserID($onlineAppUser->getID());
					$obj->setStudentID($studentID);
					$obj->save();
				}
			}
			
		}
		
		
	}
}
?>