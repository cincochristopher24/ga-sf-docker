<?php

require_once("travellog/factory/Class.FileFactory.php");
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
//require_once('travellog/helper/onlineApplications/Class.Pagination.php');
class AbstractOnlineAppAdminController {
	
	protected $pageTitle = 'Online Application';
	protected $file_factory = NULL;
	protected $errMsg = '';
	protected $baseLimit = 20;
	protected $list = array();
	protected $searchString = null;
	protected $filterOpt = null;
	
	const 	VIEW_PERSONAL_INFO_FORM = 'personalInformationForm',
			VIEW_PROGRAM_INFO_FORM  = 'programInformationForm',
			VIEW_PERSONAL_STATEMENT_FORM = 'personalStatementForm',
			VIEW_DOCUMENTS_FORM     = 'documentsToMailForm',
			VIEW_AGREEMENT_FORM     = 'agreementAndWaiverForm';
			
	private static $class_names = array(
								self::VIEW_PERSONAL_INFO_FORM => array('PersonalInfo',
																		'AddressInfo',
																		'PassportDetails',
																		'EmergencyContact',
																		'SchoolInfo',
																		'AdditionalInfo',
																		'BillingInfo'),
												
								self::VIEW_PROGRAM_INFO_FORM => array('ProgramInformation'),
								self::VIEW_PERSONAL_STATEMENT_FORM => array('PersonalStatement'),
								self::VIEW_AGREEMENT_FORM => array('Agreement')
								);
								
	
	public function __construct(){
		$this->file_factory = FileFactory::getInstance();
		$this->obj_session  = SessionManager::getInstance();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views/onlineApp'));
		$this->file_factory->setPath('CSS', '/custom/CIS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/onlineApp/');
		$this->file_factory->registerClass('HelperGlobal', array('path'=>''));
		$this->file_factory->registerClass('Template', array('path'=>''));
		
		$this->file_factory->registerClass('OnlineAppUserPeer', array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication/Mapper/'));
	}
	
	public function performAction(){

		$action = (isset($_GET['action']) ? $_GET['action'] : 'adminView');
	//	$action = 'view';	
		
		$method = $action;
		if(method_exists($this, $method)){
			$this->{$method}();
		}
		else {
			header('location: index.php');
		}
		
		
		
	}
	
	public function adminView(){
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$tplOnlineApplication = $this->file_factory->getClass('Template');
		$tplOnlineApplication->set('pageTitle',$this->pageTitle);
		$tplOnlineApplication->out('travellog/views/onlineApplications/cis/admin/tpl.adminView.php');
	}
	
	private function viewapplicants(){
		$this->view();
	}
	
	private function viewpartners(){
		
		require_once("travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.CISPartnerSchoolsPeer.php");
		$partners = CISPartnerSchoolsPeer::retrieveAll();
		
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$tplOnlineApplication = $this->file_factory->getClass('Template');
		$tplOnlineApplication->set('pageTitle',$this->pageTitle);
		$tplOnlineApplication->set('partners',$partners);
		$tplOnlineApplication->out('travellog/views/onlineApplications/cis/admin/tpl.partnerschools.php');
	}
	
	public function view(){
		
	//	$base = $this->baseLimit;
	//	$lowerLimit = isset($_GET['page']) ? ($base * ($_GET['page'] - 1) ) : 0;
	//	echo 'test';
	//	$this->list = $this->file_factory->invokeStaticClass('OnlineAppUserPeer','retrieveUsersInfo', array($lowerLimit));
	//	var_dump($this->list);
	//	$this->displayList();
		
		if(!isset($_GET['filterBy']) AND !isset($_POST['filterOpt'])){
			$_GET['filterBy'] = 'all';
		}
		$this->search();
	}
	
	public function search(){
		$opt = "";
		if(isset($_GET['filterBy'])){
			$opt = $_GET['filterBy'];
		}
		else if(isset($_POST['filterOpt'])) {
			$opt = $_POST['filterOpt'];
		}
		
		$param = array();
		$searchBy = isset($_POST['searchBy']) ? $_POST['searchBy'] : 'name';
		$qryString = isset($_POST['qryString']) ? $_POST['qryString'] : '';
		$filter = '';
		$functionName = 'retrieveUserInfoWithoutFilter';
		
		if($opt != 'all') {
			$functionName = 'retrieveUserInfoWithFilter';
			$filter = ('finished' == $opt) ? 1 : 0;
			
		}
		
		$base = $this->baseLimit;
		$lowerLimit = isset($_GET['page']) ? ($base * ($_GET['page'] - 1) ) : 0;
		
		
		$param = array($searchBy => $qryString, 'lowerLimit' => $lowerLimit, 'filter' => $filter);
		
		$this->list = $this->file_factory->invokeStaticClass('OnlineAppUserPeer',$functionName, array($param));
		
		$this->searchString = $qryString;
		$this->filterOpt = $opt;
		$this->displayList();
	
	}
	
	public function displayList(){
		
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$tplOnlineApplication = $this->file_factory->getClass('Template');
		
		$list = $this->list;
		
		$currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
		$paging   = new Paging( $list['totalRecord'], $currentPage,'', $this->baseLimit);

		$paging->setOnclick('pagination');
		$iterator = new ObjectIterator($list['usersList'],$paging,TRUE);
		
	
		$tplOnlineApplication->set('pageTitle',$this->pageTitle);
				
		$tplOnlineApplication->set('filterOpt',$this->filterOpt);
		$tplOnlineApplication->set('searchby', isset($_POST['searchBy']) ? $_POST['searchBy'] : 'name');
		$tplOnlineApplication->set('searchString',$this->searchString);
		$tplOnlineApplication->set('paging',$paging);
		$tplOnlineApplication->set('iterator',$iterator);
		
		$tplOnlineApplication->out('travellog/views/onlineApplications/cis/admin/tpl.list.php');
	}
	
	public function download(){
		$filename = $_GET['filename'];
		$appID = $_GET['appID'];
		
		$objArr = array();
		foreach(self::$class_names as $key => $values){
			foreach($values as $cls){
				$clsName = 'Class.'.$cls.'Mapper.php';
				$file = dirname(__FILE__).'/../../../model/onlineApplications/CIS_onlineApplication/Mapper/'.$clsName;
				if(file_exists($file)){
					require_once($file);
					$objArr[$cls] = call_user_func(array($cls.'Mapper', 'retrieveByPersonalInfoID'), $appID);
				}
			}
		}
		
		$fileNameLn = strlen($filename);
		if('.txt' == substr($filename,$fileNameLn-4,$fileNameLn)){
			//echo 'download txt file';
			$txtFileTemplate = $this->file_factory->getClass('Template');
			$txtFileTemplate->set('objArr', $objArr);
			$txtTmplate = $txtFileTemplate->fetch('travellog/views/onlineApplications/tpl.txtFile.php');

			// creating text file
		//	$txtFile = 'application_'.$applicantName.'_'.$id.'.txt';
			$txtFile = $filename;
			$flname = dirname(__FILE__).'/../../../../../goabroad.net/onlineapplication/admin/tmp/'.$filename;
			$ourFileHandle = fopen($flname, 'w') or die("cannot open file");
			fwrite($ourFileHandle, $txtTmplate);
			fclose($ourFileHandle);
			
			$this->performDownload($flname);
			
			
		}
		else if('.csv' == substr($filename,$fileNameLn-4,$fileNameLn)){
			
			require_once('travellog/helper/onlineApplications/Class.CIS_CSVListCreator.php');
			$csvList = CIS_CSVListCreator::create($objArr);

			// creating csv file
		//	$csvFile = 'application_'.$applicantName.'_'.$id.'.csv';
			$csvFile = $filename;
			$csvName  = dirname(__FILE__).'/../../../../../goabroad.net/onlineapplication/tmp/'.$csvFile;
			$csvFileHandle = fopen($csvName, 'w') or die("cannot open file");
			foreach ($csvList as $line) {
			    fputcsv($csvFileHandle, $line);
			}
			fclose($csvFileHandle);
			
			$this->performDownload($csvName);
			
		}
		
	}
	
	public function performDownload($file){
		// to download file
		$file = $file;
		  header('Content-Description: File Transfer');
		  // header('Content-Type: application/octet-stream');
		  header('Content-Type: application/force-download');
		  header('Content-Length: ' . filesize($file));
		  header('Content-Disposition: attachment; filename=' . basename($file));
		
		  header('Content-Transfer-Encoding: binary');
		  header('Connection: close');
		
		  readfile($file);
		
		// i dont know how but this method worked for me :)
		// the file is deleted after downloaded
		unlink($file);
		exit();
	}
	
}