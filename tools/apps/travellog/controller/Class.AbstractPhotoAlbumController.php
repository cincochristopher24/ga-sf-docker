<?php
require_once("travellog/model/Class.SessionManager.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");
abstract class AbstractPhotoAlbumController implements IController{
	
	public 
	
	$obj_session  = NULL,
	
	$file_factory = NULL;
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance();
		
		$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
		$this->file_factory->registerTemplate('LayoutMain');
		
		$this->file_factory->registerClass('PhotoAlbumRequestFactory',array('path'=>'travellog/controller/request/photoalbum/'));
		
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('GroupFactory');
		
		$this->file_factory->registerClass('Template',array('path'=>''));
		$this->file_factory->registerClass('SessionManager', array("path" => "travellog/model/"));
		$this->file_factory->registerClassInitCall('Template',array('func'=>'set_path','params' => array('path'=>"travellog/views/")));
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		
		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		
	}
	
	function performAction(){		
		$template = $this->file_factory->getClass('Template');
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		return $this->file_factory->getClass('PhotoAlbumRequestFactory');
	}
	
	
}

?>
