<?php
require_once('travellog/model/Class.SessionManager.php');
require_once("travellog/factory/Class.FileFactory.php");

class AbstractOnlineAppRegisterController {
	
	protected	$file_factory = NULL;
	protected	$session_manager = NULL;
	protected	$errMsg = array();
	
	public function __construct(){
		$this->session_manager = SessionManager::getInstance();
		
		$this->file_factory = Filefactory::getInstance();
		
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
		$this->file_factory->setPath('CSS', '/css/onlineApplications/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		$this->file_factory->registerClass('HelperGlobal', array('path'=>''));
		$this->file_factory->registerClass('Template', array('path'=>''));
		$this->file_factory->registerClass('ToolMan', array('path'=>''));
		
		$this->file_factory->registerClass("OnlineAppUser", array("path" => "travellog/model/onlineApplications/CIS_onlineApplication"));
	}
	
	public function performAction(){
		
		$action = (isset($_GET['action']) ? $_GET['action'] : 'showRegistrationForm');
		
		$method = $action;
		
		$this->{$method}();
	//	var_dump($action);
		
	}
	
	public function showRegistrationForm(){
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$this->file_factory->invokeStaticClass('Template','setMainTemplateVar', array('layoutID', 'online_application'));
		
		$tplLogin = $this->file_factory->getClass('Template');
		$tplLogin->set('errMsg',(isset($this->errMsg['existingUser']) ? $this->errMsg['existingUser'] : ''));
		$tplLogin->out('travellog/views/onlineApplications/tpl.registrationForm.php');
	}
	
	public function processRegistration(){
		
		$email = $_POST['txtEmail'];
		$pass  = $_POST['psword'];
		
		
		if(0 < $this->session_manager->get('onlineAppUserID')){
			$this->session_manager->unsetVar('onlineAppUserID');
		}
		
		$onlineAppUser = $this->file_factory->getClass('OnlineAppUser');
		
		
		if(!$onlineAppUser->isAlreadyUser($email)){
			$onlineAppUser->setEmail(    $email );
			$onlineAppUser->setPassword( $pass  );
			$onlineAppUser->save();
			
			$this->session_manager->set('onlineAppUserID', $onlineAppUser->getID());

			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php'));
		}
		else {
			$this->errMsg['existingUser'] = ' (Sorry, but it seems that the email you supplied is already in use.)';
			$this->showRegistrationForm();
		}
		
		
	}
}
?>