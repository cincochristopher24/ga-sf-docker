<?php
	/**
	 * Last Edited By: Cheryl Ivy Q. Go -- used SessionManager   27 November 2007
	 */
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");

	abstract class AbstractEditTravelerProfileController implements IController{
	
		public 
	
		$obj_session  = NULL,
	
		$file_factory = NULL;
	
		function __construct(){
			//$this->obj_session  = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('TravelerProfile');
			$this->file_factory->registerClass('Country');
		
			$this->file_factory->registerClass('TravelerType');
			$this->file_factory->registerClass('PurposeType');
			$this->file_factory->registerClass('ProgramType');
			$this->file_factory->registerClass('PreferredTravelType');
		
			$this->file_factory->registerClass('TravelBioUser' , array('path' => 'travellog/model/travelbio/'));
			$this->file_factory->registerClass('ProfileQuestion' , array('path' => 'travellog/model/travelbio/'));
			$this->file_factory->registerClass('SessionManager', array("path" => "travellog/model/"));
		
			$this->file_factory->registerClass('TravelSchedule');
			
			/*****************************************************
			 * added by neri: 11-06-08
			 * registers the class used by the profile component 
			 *****************************************************/
			 
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('GroupFactory');
				
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->file_factory->registerTemplate('FrmEditTravelerProfile');
		
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		
		}
	
		function performAction(){
			require_once('travellog/controller/Class.ProfileController.php');

			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
				
			// Added By: Cheryl Ivy Q. Go   -- checks if user is logged in
			$sessMan = $this->file_factory->invokeStaticClass('SessionManager','getInstance',array());
			$travelerID = $sessMan->get('travelerID');

			if($travelerID == 0){
				// edited by chris: redirect to login page
				require_once('Class.ToolMan.php');
				ToolMan::redirect('/login.php?redirect=edittravelerprofile.php');
				//header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
				//exit();	
			}
			
			// insert code for logger
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($travelerID,'TRAVELER');	
		
			//Saving...
			if(isset($_POST['save'])){			
			
			
				$err = NULL;
				
				if(!strlen(trim($_POST['txt_firstname']))){
					$err[1]= "First Name is Required";
				}			
				if(!strlen(trim($_POST['txt_lastname']))){
					$err[2]= "Last Name is Required";
				}
				// edits by ianne: transferred to change account setting 
				/*if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $_POST['txt_email'])) {
			 		$err[3]="Invalid email address.";
				}else{
					
					$conn = new Connection();
					$rs    = new Recordset($conn);
					$sql = "SELECT email FROM tblTraveler WHERE travelerID='$travelerID'";
					$rs->Execute($sql);
					
					$traveler 	= $rs->FetchAsObject(); 
					$email 		= $_POST['txt_email'];
					
					if($traveler->email !== $email){
						$conn = new Connection();
						$rs    = new Recordset($conn);
						$sql = "SELECT email FROM tblTraveler WHERE email='$email'";
						$rs->Execute($sql);
						if($rs->RecordCount()){
							$err[4]="Email Address is taken already";
						}	
					}
				}*/
					
				$formbday 	= $_POST['txtbyear']."-".$_POST['txtbmonth']."-".$_POST['txtbday'];
			
				require_once('Date.php');
				$pDate 		= new Date();	
				$bdaydiff 	= $pDate->getDate(1)-$formbday;
				if($bdaydiff <=12){
					$err[5] = "Age must 12 years old above.";
				}				
			
			
				/*
				if($_POST['countryID'] == 0){
					$err[5]="You didn't specify a Country of your residence.";
				}
				*/
				$tprofile = $this->file_factory->getClass('TravelerProfile',array($travelerID));
				if($tprofile->getShowPersonals()){
					if($_POST['rdoTraveled'] && !isset($_POST['chkPurpose'])){
				
						$err[6]="You didn’t specify a Purpose for your international travel.";
				
					}
				}
			
				if(isset($_POST['chkpreferother']) && !strlen(trim($_POST['txtprefer']))){
				
					$err[7]="specify what other do you prefer to do when you travel.";
				
				}
			
				/* added by ianne - 11/25/2008
				   check if username is valid and not empty
				   edits by ianne: travsferred to change account setting (change password) interface 
				*/
				/*if(!strlen(trim($_POST['txt_username']))){
					$err[8]="Username is Required.";
				}else{
					$conn = new Connection();
					$rs    = new Recordset($conn);
					$sql = "SELECT username FROM tblTraveler WHERE travelerID='$travelerID'";
					$rs->Execute($sql);
					
					$travelerobj 	= $rs->FetchAsObject();
					
					if( $travelerobj->username !== $_POST['txt_username'] ){
						$traveler = $this->file_factory->getClass('Traveler',array($travelerID));
						if(0 != $traveler->TravelerByUsername($_POST['txt_username'])){						
							$err[9]="Username is Already Taken.";
						}	
					}
				}*/
				
				if ($err == NULL) {
				
					require_once('Class.Connection.php');
					require_once('Class.Recordset.php');
				
					$travelerprofile = $this->file_factory->getClass('TravelerProfile',array($travelerID));
				
				
					$travelerprofile->setFirstName($_POST['txt_firstname']); 		
					$travelerprofile->setLastName($_POST['txt_lastname']);
					//$travelerprofile->setUserName($_POST['txt_username']);		// added by ianne - 11/25/2008
					//$travelerprofile->setEmail ($_POST['txt_email']);	
					$travelerprofile->setBirthDay($_POST['txtbyear']."-".$_POST['txtbmonth']."-".$_POST['txtbday']);
					$travelerprofile->setGender($_POST['gender']);
				
					if($_POST['cityID'] ==0){
						$conn = new Connection();
						$rs    = new Recordset($conn);
						$sql = "select locID from GoAbroad_Main.tbcountry where approved =1 AND countryID='".$_POST['countryID']."'";
						$Countries = $rs->Execute($sql);
						$results = mysql_fetch_assoc($Countries);
						$travelerprofile->setHTLocationID($results['locID']);			
					}else{
						$travelerprofile->setHTLocationID($_POST['cityID']);
					}
				
					if($_POST['currcityID'] ==0){
						$conn = new Connection();
						$rs    = new Recordset($conn);
						$sql = "select locID from GoAbroad_Main.tbcountry where approved =1 AND countryID='".$_POST['currcountryID']."'";
						$Countries = $rs->Execute($sql);
						$results = mysql_fetch_assoc($Countries);
						$travelerprofile->setCurrLocationID($results['locID']);			
					}else{
						$travelerprofile->setCurrLocationID($_POST['currcityID']);
					}
				
					//$travelerprofile->setInterests($_POST['txtarea_interests']);
					//$travelerprofile->setShortBio($_POST['txtarea_shortbio']);
					//$travelerprofile->setCulture($_POST['txtarea_iculture']);	 		
					//$travelerprofile->setAddress1($_POST['txtarea_address1']);
					//$travelerprofile->setAddress2($_POST['txtarea_address2']);
					//$travelerprofile->setPhone($_POST['txt_phone']);
					//$travelerprofile->setTravelertypeID($_POST['travelertype']);
				
					//for  travelled internationally/For what purpose
					if(isset($_POST['rdoTraveled'])){
						$travelerprofile->setIntrnltravel($_POST['rdoTraveled']);
						if(isset($_POST['chkPurpose'])){
							$travelerprofile->setPurpose($_POST['chkPurpose']);
						}		
					}
				
					$travelerprofile->setTripnexttwoyears($_POST['rdotripnexttwoyears']);
				
					if(isset($_POST['chkProgram'])){
						$travelerprofile->setProgramInterested($_POST['chkProgram']);
					}
				
					if(isset($_POST['chkPreferred'])){
						$travelerprofile->setPreferredTravel($_POST['chkPreferred']);
					}
				
					if(isset($_POST['chkpreferother'])){
						$travelerprofile->setOtherprefertravel($_POST['txtprefer']);
					}else{
						$travelerprofile->setOtherprefertravel('');	
					}
				
					if(isset($_POST['chkcountryinrested'])){
						$travelerprofile->setCountryinterested($_POST['chkcountryinrested']);
					}
				
					if(isset($_POST['chkcountrytravelled'])){
						$travelerprofile->setCountrytravelled($_POST['chkcountrytravelled']);
					}
				
					$travelerprofile->setIncome($_POST['cboIncome']);
					$travelerprofile->setSignature($_POST['txtarea_signature']);
				
					$travelerprofile->setShowPersonals(0);
					$travelerprofile->update();		
				
					/**####################
					 #Send Promo if profile Completed 
					####################*/
				
					/*
				
							$objtravelerprofile 	= new TravelerProfile($travelerID);
						
							if($objtravelerprofile->getTraveler()->isProfileCompleted() && !$objtravelerprofile->getIspromosend()){
							
									$htmlcontent 	= file_get_contents("admin/restricted/thankyou/ga_promo_thank_you.html");
									$textcontent 		= file_get_contents("admin/restricted/thankyou/ga_promo_thank_you.txt");
								
									$mail = new PHPMailer();
									$mail->IsSMTP();                                   // send via SMTP
								
									switch($_SERVER['HTTP_HOST']){
										case "dev.goabroad.net":
											$mail->Host     = '192.168.3.6';
											break;
										case "www.goabroad.net":
											$mail->Host     = '192.168.3.6';
											break;
										case "goabroad.net.local":
											$mail->Host= "ga-mail01.goabroad.com";
											break;
									}
								
								
									$mail->SMTPAuth = true;     // turn on SMTP authentication
									$mail->Username = "postmaster@goabroad.net";  // SMTP username
									$mail->Password = "Pr1nGl35"; // SMTP password
								
									$mail->From     = "admin@goabroad.net";
									$mail->FromName = "GoAbroad.net";
									$mail->AddAddress($objtravelerprofile->getEmail());               // optional name
									$mail->IsHTML(true);                               // send as HTML
								
									$mail->Subject  =  "GoAbroad Network thanks you - Enjoy a cut in your flight and travel gear rates!";
									$mail->Body		=  str_replace('[image]', $travelerID.'_2007-04-17', $htmlcontent);
									$mail->AltBody	=  str_replace('[image]', $travelerID.'_2007-04-17', $textcontent);
								
									if($mail->Send()){
									    $conn2  = new Connection();
										$rs2   	= new Recordset($conn2);
									
										$sql2 = "UPDATE tblTraveler set ispromosend =1 where travelerID ='$travelerID'";
										$rs2->Execute($sql2);
															
									}
		
							}
				
					*/
				
					/**####################
					#END Send Promo
					####################*/
				
				
					header("Location: profile.php?action=view&travelerID=".$travelerID);						
					exit;
			
				}
			
			
			
			
			
			}
		
		
		
			$countrylist = $this->file_factory->invokeStaticClass('Country','getCountryList');;
		
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation = new SubNavigation();
			$subNavigation->setContext('TRAVELER');
			$subNavigation->setContextID($travelerID);
			$subNavigation->setLinkToHighlight('MY_PROFILE');
			
			/**************************************************************************************
			 * edits by neri: 
			 * 		added code for displaying the profile component						11-06-08
			 * 		added curr_page parameter to init method of profile component		11-10-08
			 **************************************************************************************/
			
			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($travelerID, AbstractProfileCompController::$EDIT_PROFILE);
			
			$purposetypes 		= $this->file_factory->getClass('PurposeType');
			$programtypes 		= $this->file_factory->getClass('ProgramType');
			$prefferedtypes 	= $this->file_factory->getClass('PreferredTravelType');
				
			if (isset($_POST['save'])) {
		
				$firstname = $_POST['txt_firstname'];
				$lastname  = $_POST['txt_lastname'];
				
				$gender		= $_POST['gender'];	
				//$interests = stripslashes($_POST['txtarea_interests']);
				//$shortbio  = stripslashes($_POST['txtarea_shortbio']);
				//$iculture  = stripslashes($_POST['txtarea_iculture']);
				//$address1  = stripslashes($_POST['txtarea_address1']);
				//$address2  = stripslashes($_POST['txtarea_address2']);
				//$phone  = stripslashes($_POST['txt_phone']);
				$intrnltravel = $_POST['rdoTraveled'];
				$istripnexttwoyears    =	$_POST['rdotripnexttwoyears'];
				$income					= $_POST['cboIncome'];
			
				$signature	= $_POST['txtarea_signature'];
			
				if(isset($_POST['chkpreferother'])){
					$chkpreferother  =$_POST['chkpreferother'];
				}else{
					$chkpreferother = false;	
				}
				$otherprefertravel = $_POST['txtprefer'];
				
			
				$tppurpose 		= array(); 
				$tpprogram 		= array(); 
				$tppreffered 		= array(); 
				$tpcountry			= array();
				$tpcountrytravelled = array();
			
				//for  travelled internationally/For what purpose
				if(isset($_POST['chkPurpose'])){
					foreach($_POST['chkPurpose'] as $tptype){
							$tppurpose[] = $tptype;		
					}
				}
			
				//for  Program Interested
				if(isset($_POST['chkProgram'])){
					foreach($_POST['chkProgram'] as $pptype){
							$tpprogram[] = $pptype;		
					}
				}
			
				//for  Preferred Travel 
				if(isset($_POST['chkPreferred'])){
					foreach($_POST['chkPreferred'] as $preftype){
							$tppreffered[] = $preftype;		
					}
				}
			
			
				//for countries interested
				if(isset($_POST['chkcountryinrested'])){
					foreach($_POST['chkcountryinrested'] as $ccountry){
							$tpcountry[] = $ccountry;		
					}
				}
				//for countries travelled
				if(isset($_POST['chkcountrytravelled'])){
					foreach($_POST['chkcountrytravelled'] as $ccountry){
							$tpcountrytravelled[] = $ccountry;		
					}
				}
			
				$countryID = $_POST['countryID'];
				$cityID	   = $_POST['cityID'];
			
				$currcountryID = $_POST['currcountryID'];
				$currcityID	   = $_POST['currcityID'];
			
				//$email	   = $_POST['txt_email']; 
				$birthday		= $_POST['txtbyear']."-".$_POST['txtbmonth']."-".$_POST['txtbday'];	
			
				$bday      =array('year'=>$_POST['txtbyear'],'mon'=>$_POST['txtbmonth'],'mday'=>$_POST['txtbday'],0=>0);
			
				$days	   =ProfileController::getDaysInMonth($bday['year'],$bday['mon']);	  
			
			  	
				if($bday['0']	== 943891200){//invalid date
					$days	   	=ProfileController::getDaysInMonth(0,0);
				}else{
					$days	   	=ProfileController::getDaysInMonth($bday['year'],$bday['mon']);
				}
			
			
				$tmpcity   =1;	
			
				$travelerprofile 	= $this->file_factory->getClass('TravelerProfile',array($travelerID));
				$photos = $travelerprofile->getPhotos();
				$username  = $travelerprofile->getUserName();
				$email = $travelerprofile->getEmail();
				
				//$travelertypeid = $_POST['travelertype'];
			
			} else {
			
				$travelerprofile 	= $this->file_factory->getClass('TravelerProfile',array($travelerID));
				
				//prepare Traveler Purpose
				$trpurpose 		= $travelerprofile->getTravelerPurpose();	
				$tppurpose 		= array(); 
			
				if(count($trpurpose)){
					foreach($trpurpose as $tptype){
							$tppurpose[] = $tptype->gettpurposetypeid();		
					}
				}
			
				//prepare Traveler Program Interested
				$trprograminterested 		= $travelerprofile->getTravelerProgramInterested();	
				$tpprogram 		= array(); 
			
				if(count($trprograminterested)){
					foreach($trprograminterested as $pptype){
							$tpprogram[] = $pptype->getprogramid();		
					}
				}
			
				//prepare Traveler Preffred Travel
				$trprefferedtravel 		= $travelerprofile->getTravelerPrefferedTravelType();	
				$tppreffered 		= array(); 
			
				if(count($trprefferedtravel)){
					foreach($trprefferedtravel as $preftype){
							$tppreffered[] = $preftype->getpreferredtypeid();		
					}
				}
			
				//prepare Traveler Countrys Interested
				$trcountryinrested 	= $travelerprofile->getTravelerCountriesInterested();	
				$tpcountry 		= array(); 
			
				if(count($trcountryinrested)){
					foreach($trcountryinrested as $tcountry){
							$tpcountry[] = $tcountry->getcountryid();		
					}
				}
			
				//prepare Traveler Countrys Travelled
				$trcountrytravelled 	= $travelerprofile->getTravelerCountriesTravelled();	
				$tpcountrytravelled 	= array(); 
			
				if(count($trcountrytravelled)){
					foreach($trcountrytravelled as $tcountry){
							$tpcountrytravelled[] = $tcountry->getcountryid();		
					}
				}
				/*
				echo "<pre>";
					print_r($travelerprofile->getTraveler()->getCountrysTravelled());	
					print_r($tpcountrytravelled);
				echo "</pre>";
				*/
			
				$countrystravelled	= array(); 	
			 	if($travelerprofile->getTraveler()->getCountrysTravelled()){
			 		foreach($travelerprofile->getTraveler()->getCountrysTravelled() as $travelledcountries){ 					
							$countrystravelled[]	= $travelledcountries->getCountryID(); 						    					
					
			 		}		
			 	}
			
				$tpcountrytravelled = array_unique(array_merge($tpcountrytravelled,$countrystravelled));
			
			
			
			
				$username 		= $travelerprofile->getUserName();
				$email 				= $travelerprofile->getEmail();
				$age 				= $travelerprofile->getAge();
			
				
			
			
			
				if ($travelerprofile->getHTLocationID()) {
			
					$countryID	= $travelerprofile->getCity()->getCountry()->getcountryID();
					/*								
					if(method_exists($travelerprofile->getCity(),'getCityID')){
						$cityID		= $travelerprofile->getCity()->getCityID();	
					
					}else{
						$cityID = 0;
					}	
					*/
				
					$cityID = $travelerprofile->getHTLocationID();	
				} else {
			
					$cityID			= 0;				
					$countryID		= 0;
			
				}
			
			
			
			
				if ($travelerprofile->getCurrLocationID()) {
			
					$currcountryID	= $travelerprofile->getCurrLocation()->getCountry()->getcountryID();
					/*								
					if(method_exists($travelerprofile->getCurrLocation(),'getCityID')){
						$currcityID		= $travelerprofile->getCurrLocation()->getCityID();	
					}else{
						$currcityID = 0;
					}	
					*/
					$currcityID = $travelerprofile->getCurrLocationID();
					
				} else {
			
					$currcityID			= 0;				
					$currcountryID		= 0;
				}
			
			
			
				$photos = $travelerprofile->getPhotos();
			
				//$travelertypeid	= $travelerprofile->getTravType()->gettravelertypeid();
			
			
				$interests		= $travelerprofile->getInterests();
				//$shortbio		= $travelerprofile->getShortBio();
				$iculture		= $travelerprofile->getCulture();		
				$firstname	= $travelerprofile->getFirstName();
				$lastname		= $travelerprofile->getLastName();
				$birthday		= $travelerprofile->getBirthDay();	
				$gender			= $travelerprofile->getGender();
			
				$bday 			= getdate(strtotime($birthday));
				//print_r($bday);
				$days	   =ProfileController::getDaysInMonth($bday['year'],$bday['mon']);	
			  	
				if($birthday	=="0000-00-00"){//invalid date
					$days	   	=ProfileController::getDaysInMonth(0,0);
				}else{
					$days	   	=ProfileController::getDaysInMonth($bday['year'],$bday['mon']);
				}
			
					if ($gender == NULL){		 			
			 			$gender = 0;	
			 		}elseif($gender == "M"){ // male
			 			$gender = 1;
			 		}elseif($gender == "F"){ // female
			 			$gender = 2;		 				
			 		}
				
				
				//$address1		= $travelerprofile->getAddress1();
				//$address2		= $travelerprofile->getAddress2();
				//$phone		= $travelerprofile->getPhone();
			
				$intrnltravel		= $travelerprofile->getIntrnlTravel();
				$istripnexttwoyears = $travelerprofile->getTripnexttwoyears();
				$income			= $travelerprofile->getIncome();
				$otherprefertravel = $travelerprofile->getOtherprefertravel();
			
				$signature	= $travelerprofile->getSignature();
				
				if($otherprefertravel){	
					$chkpreferother = true;
				}else{
					$chkpreferother = false;	
				} 
			
				$tmpcity   =0;	
			}	
				
				$personalquestionsanswered = ($travelerprofile->getShowPersonals()) ? false : true;
			
				// edited by: Cheryl Ivy Q. Go  07 June 2007
				$htCities 		= ProfileController::getCities($countryID, $travelerID);
			
				$currlivCities 	= ProfileController::getCities($currcountryID, $travelerID);
			
			
			
				// added by: cheryl ivy q. go  10 July 2007
				// edited by naldz: Aug. 13, 2007
			
				$travelBioUser = $this->file_factory->invokeStaticClass('TravelBioUser','getTravelBioUserByTravelerID',array($travelerID));
				if (!is_null($travelBioUser)){
					$fbAction = 'Edit';
				}
				else{
					$fbAction = 'Add';
				}
				
				
				$template = $this->file_factory->getClass('Template');
				//$template->set_path("travellog/views/");
				
				$template->set_vars( array(	
										'travelerID' => $travelerID,
										'username' => $username,			// added username - ianne 11/25/2008
										'email' => $email,
										'age' => '0',
										'countryID' => $countryID, 			//ht countryID
										'cityID' => $cityID,	   			//ht cityID	
										'currcountryID' => $currcountryID,	//curliving countryID
										'currcityID' => $currcityID,		//curliving cityID
										/*'interests' => $interests,*/
										/*'shortbio' => $shortbio,*/
										/*'iculture' => $iculture,*/
										'firstname' => $firstname,
										'lastname' => $lastname,
										'birthday' => $birthday,						
										'bday' => $bday,
										'days' => $days,		
										'gender'   => $gender,
										/*'address1' => $address1,*/
										/*'address2' => $address2,*/
										/*'phone' => $phone,*/
										/*'travelertypeid' => $travelertypeid,*/
										/*'travtype' =>TravelerType::getAllTravelerType(),*/
										'tmpcity' => $tmpcity,
										'htCities' => $htCities,							
										'currlivCities' => $currlivCities,
										'EditPhotos' => count($photos) ? "edit": "add",
										'subNavigation' =>$subNavigation,
										'errors' => isset($err) ? $err : NULL,
										'intrnltravel' 	=>$intrnltravel, // personal
										'purposetypes' => $purposetypes,
										'travelerpurposetypes' => $tppurpose,
										'programtypes' =>$programtypes,
										'tpprograminterested' 	=> $tpprogram,
										'prefferedtypes' => $prefferedtypes,						
										'tppreffered' => 	$tppreffered, 
										'countrylist'	=> $countrylist,
										'tpcountry'		=> $tpcountry,
										'tpcountrytravelled' => $tpcountrytravelled,
										'istripnexttwoyears' =>$istripnexttwoyears,
										'income' => $income,
										'otherprefertravel' => $otherprefertravel,
										'chkpreferother' =>$chkpreferother,
										'signature' => $signature,
										'fbAction'		=> $fbAction,
										'profile' => $profile_comp->get_view(),						//added by neri: 11-06-08
										'personalquestionsanswered' => $personalquestionsanswered
										));
									
									
				//$template->out('tpl.FrmEditTravelerProfile.php');
				$template->out($this->file_factory->getTemplate('FrmEditTravelerProfile'));
		}
	}
?>