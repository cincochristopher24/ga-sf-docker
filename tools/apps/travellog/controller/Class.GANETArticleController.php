<?php
require_once('travellog/controller/Class.AbstractArticleController.php');
class GANETArticleController extends AbstractArticleController{
	
	function performAction(){
		$this->data           = array_merge($_GET, $_POST);
		$this->data['articleID'] = intval($this->data['articleID']); 
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'view'; 
		parent::__applyRules();
		parent::performAction(); 
	}
}
?>
