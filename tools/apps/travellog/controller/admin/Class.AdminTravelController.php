<?php
class AdminTravelController{
	
	function Update( $formvars ){
		require_once('travellog/model/Class.Travel.php');
		$obj_travel = new Travel($formvars["travelID"]);
		$obj_travel->setTitle      ($formvars["title"]);
		$obj_travel->setDescription($formvars["description"]);
		$arrdate = explode('-',$formvars['lastupdate']);
		$now     = NULL;
		if( count($arrdate) == 3 ){
			if( checkdate($arrdate[1], $arrdate[2], $arrdate[0]) ){
				$now = date("Y-m-d H:i:s", strtotime($formvars['lastupdate']) );	
			}
		}
		$obj_travel->Update($now);
	}
	
	function Edit( $formvars, $errors = array() ){
		require_once('travellog/model/Class.Travel.php');
		require_once('Class.Template.php');
		require_once('travellog/model/navigator/Class.BackLink.php');
		$inctraveltpl   = new Template();
		$maintpl        = new Template();
		$subNavigation  = new SubNavigation();
		$obj_backlink   = BackLink::instance();
		$obj_backlink->Create();
		if(!count($errors)){
			$travel                  = new Travel($formvars["travelID"]);
			$_locationID             = $travel->getLocationID();
			$formvars["description"] = $travel->getDescription();
			$formvars["title"]       = $travel->getTitle();
			$formvars["lastupdate"]  = $travel->getLastUpdated();
		}
		
		$inctraveltpl->set('action'       , "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?action=update");
		$inctraveltpl->set('title'        , $formvars["title"]);			
		$inctraveltpl->set('description'  , $formvars["description"]);
		$inctraveltpl->set('lastupdated'  , date( 'Y-m-d', strtotime($formvars['lastupdate']) ) );
		$inctraveltpl->set('travelID'     , $formvars["travelID"]);
		$inctraveltpl->set('context'      , $formvars["context"]);
		$inctraveltpl->set('mode'         , 'edit');
		$inctraveltpl->set('errors'       , $errors);
		$inctraveltpl->set('obj_backlink' , $obj_backlink);
		$inctraveltpl->set('hasJournals'  , 0);
		$inctraveltpl->set('isAdmin'     , true);
		
		if ($formvars["context"] == 2){
			$inctraveltpl->set('gID'           , $formvars["gID"]);
			$inctraveltpl->set('isSubGroup'    , $formvars["isSubGroup"]);
			$inctraveltpl->set('col_subgroups' , $formvars["col_subgroups"]);
		}	
		
		$maintpl->set_vars
		( array(
			'title'         => "Edit Travel",
			'form'          => $inctraveltpl->fetch('travellog/views/tpl.FrmTravel.php'),
			'context'       => $formvars["context"],
			'subNavigation' => $subNavigation
		));
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		$maintpl->out('travellog/views/tpl.Travel.php');
	}
	
}
?>
