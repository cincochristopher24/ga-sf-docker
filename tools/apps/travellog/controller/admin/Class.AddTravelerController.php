<?php
/**
*  Purpose: add travelers from csv or xls
**/

	require_once('travellog/factory/Class.FileFactory.php');
	require_once('Class.GaString.php');
	require_once('Class.GaDateTime.php');
	require_once('Class.iRecordset.php');
	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/model/Class.PrivacyPreference.php');
	
	class AddTravelerController {
	
		private $file_factory = NULL;
		private $data = NULL;
		private $sub_tpl = NULL;
		private $loggedTravelerID = null;
		private $obj_session = null;
		
		private $mDb = null;
		
		private static $headers = array('username', 'firstname', 'lastname', 'email');
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('dbHandler', array('path' => ''));
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('GroupFactory', array('path' => 'travellog/model/'));
			
			$this->file_factory->invokeStaticClass('Template', 'setMainTemplate', array('travellog/views/tpl.LayoutMain.php'));
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->mDb = $this->file_factory->getClass('dbHandler', array('db=Travel_Logs'));
			$this->obj_session = SessionManager::getInstance();
		}
		
		public function prepareData(){
			$this->data 						= $_REQUEST;
			$this->data['method'] 				= $this->getMethod();
			$this->data['travelersToBeAdded'] 	= isset($this->data['travelersToBeAdded']) ? $this->data['travelersToBeAdded'] : array();
			$this->data['traveleridtobeadded']	= isset($this->data['traveleridtobeadded']) ? $this->data['traveleridtobeadded'] : array();
			$this->data['upload_filename']		= isset($this->data['upload_filename']) ? $this->data['upload_filename'] : '';
			$this->data['gID'] 					= isset($this->data['gID']) ? $this->data['gID'] : null;
			$this->data['groupids'] 			= isset($this->data['groupids']) ? $this->data['groupids'] : array();
			$this->data['travelerids'] 			= isset($this->data['travelerids']) ? $this->data['travelerids'] : array();
			$this->data['generateUsername'] 	= isset($this->data['generateUsername']);
			$this->data['existingTravelers'] 	= isset($this->data['existingTravelers']) ? $this->data['existingTravelers'] : array();
			$this->data['existingTravelerIDs'] 	= isset($this->data['existingTravelerIDs']) ? explode(',',$this->data['existingTravelerIDs']) : array();
			$this->data['customPassword']		= isset($_POST['txtCustomPassword']) ? trim($_POST['txtCustomPassword']) : '';
		}
		
		public function performAction(){
			$this->sub_tpl = $this->file_factory->getClass('Template');
			$this->sub_tpl->set_path('travellog/views/admin/');
			
			$this->loggedTravelerID = $this->obj_session->get('travelerID');
			if( 0 < $this->loggedTravelerID ){
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($this->loggedTravelerID,'ADMIN');
			}
			
			$s = '_' . $this->data['method'];
			if(method_exists($this, $s)){
				try{
					$this->$s();
				}
				catch(exception $e){
					if(file_exists($this->data['upload_filename']))
						unlink(stripslashes($this->data['upload_filename'])); // delete temporary file rethrow exception
					throw $e;
				}
			}
			$this->sub_tpl->set('generateUsername', $this->data['generateUsername']);
			$this->sub_tpl->set('gID', $this->data['gID']);
			$code = <<<SCRIPT
				<script>
					function selectboxes(name, value, frmID){
						var frm = document.getElementById(frmID);
						var forminputs = frm.getElementsByTagName("input");
						for(var i = 0; i < forminputs.length; i++){
							var regex = new RegExp(name, "i");
							if (regex.test(forminputs[i].getAttribute('name'))) {
							      if (value == '1') {
							        forminputs[i].checked = true;
							      } else {
							        forminputs[i].checked = false;
							  		}
								}
						}
					}
				</script>
SCRIPT;
			Template::includeDependent($code);
			$this->sub_tpl->out('tpl.AddTraveler.php');
		}
		
		private function _load(){
			if('' == $_FILES['file']['name']) return;
			$ext = substr($_FILES['file']['name'], -3, 3);
			$this->data['uploaded_filename'] = 'upload/' . md5(preg_replace('/\.' . $ext . '$/', '', $_FILES['file']['name'])) . '.' . $ext;
			//$this->data['uploaded_filename'] = 'upload2/' . md5(preg_replace('/\.' . $ext . '$/', '', $_FILES['file']['name'])) . '.' . $ext;
			move_uploaded_file($_FILES["file"]["tmp_name"],$this->data['uploaded_filename']);
			$this->loadFile();
			$this->sub_tpl->setVars($this->data);
			$this->sub_tpl->set('uploaded_filename', $this->data['uploaded_filename']);
			$this->sub_tpl->set('travelersToBeAdded', $this->data['travelersToBeAdded']);
			$this->sub_tpl->set('loadfile', true );
		}
		
		private function _addtraveler(){
			if (isset($_REQUEST['hdnDuplicate'])){
				$this->resubmitDuplicate();
			}
			else{
				$this->loadFile();
			}
			
			$this->sub_tpl->set('travelersToBeAdded', array());
			
			$db = $this->file_factory->getClass('dbHandler', array('db=Travel_Logs'));
			
			$addedTravelers = array();
			$addedUsernames	= array();
			$addedEmails	= array();
			$duplicateEmail    = array();
			$duplicateUsername = array();
			
			// create travelers
			foreach($this->data['travelersToBeAdded'] as $k => $iTraveler){
				if(in_array($k, $this->data['traveleridtobeadded'])){ // add only selected travelers
					// duplicate username or blank username in list
					if( in_array(trim($iTraveler['username']), $addedUsernames) || '' == trim($iTraveler['username']) ){
						if (in_array(trim($iTraveler['username']), $addedUsernames)){
							$duplicateUsername[] = $iTraveler;
						}
						continue;
					}
					// duplicate email or blank email in list	
					if( in_array(trim($iTraveler['email']), $addedEmails) || '' == trim($iTraveler['email']) ){
						if (in_array(trim($iTraveler['email']), $addedEmails)){
							$duplicateEmail[] = $iTraveler;
						}
						continue;
					}
					$insertSendableSql = 'INSERT INTO `Travel_Logs`.`tblSendable` SET `sendabletype` = 1';
					$db->execute($insertSendableSql);
					$sendableID = $db->getLastInsertedID();

					$arr = array_merge(range(97, 122), range(49, 57) );
					$pass = array();
					for($ctr = 0; $ctr < 6; $ctr++){
						$pass[] = chr($arr[array_rand($arr)]);
					}
					$newPassword = isset($this->data['customPassword']) && '' != trim($this->data['customPassword']) ? $this->data['customPassword'] : implode($pass);
					
					// proworld members will be automated 2 so it will receive notification
					$isAutomated = 245 == $this->data['gID'] ? 2 : 1; 
					$insertTravelerSql = 'INSERT INTO `Travel_Logs`.`tblTraveler` SET ' .
											'`username` = ' . GaString::makeSqlSafe(trim($iTraveler['username'])) . ', ' .
											'`password` = ' . GaString::makeSqlSafe(trim($newPassword)) . ', ' .
											'`sendableID` = ' . GaString::makeSqlSafe($sendableID) . ', ' .
											'`firstname` = ' . GaString::makeSqlSafe((isset($iTraveler['firstname']) ? trim($iTraveler['firstname']) : '')) . ', ' .
											'`lastname` = ' . GaString::makeSqlSafe((isset($iTraveler['lastname']) ? trim($iTraveler['lastname']) : '')) . ', ' .
											'`email` = ' . GaString::makeSqlSafe((isset($iTraveler['email']) ? trim($iTraveler['email']) : '')) . ', ' .
											'`active` = 1, `isAutomatedTraveler` = ' . $isAutomated.', '.
											'`dateregistered` = '.GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat());
					$addedUsernames[] = trim($iTraveler['username']);
					$addedEmails[] = trim($iTraveler['email']);
					$db->execute($insertTravelerSql);
					$travelerID = $db->getLastInsertedID();
					
					if( 245 == $this->data['gID'] ){
						$prefQuery = 'INSERT INTO Travel_Logs.tblPrivacyPreference SET ' .
										'travelerID = ' . GaString::makeSqlSafe($travelerID) .
									 	',preference = ' .GaString::makeSqlSafe(PrivacyPreference::$RECEIVE) .
									 	',preferencetype = ' .GaString::makeSqlSafe(PrivacyPreferenceType::$RECEIVE_UPDATES); 
						$db->execute($prefQuery);
					}
					
					$sqlquery = "INSERT INTO tblTravelerRanking SET travelerID = $travelerID";
					$db->execute($sqlquery);

					//$createTemporarySql = 'INSERT INTO `Travel_Logs`.`tblAddedTravelerFromFile` SET `travelerID` = ' . GaString::makeSqlSafe($travelerID);
					//$db->execute($createTemporarySql);
					$addedTravelers[] = $this->file_factory->getClass('Traveler', array($travelerID));
				}
			}
			
			// delete temporary file
			if(file_exists($this->data['upload_filename']))
				unlink(stripslashes($this->data['upload_filename']));
			
			// initialize target groups
			$groups = array();
			if(null != $this->data['gID']){
				$group = $this->file_factory->invokeStaticClass('GroupFactory','instance',array())->create(array($this->data['gID']));
				
				if(($group[0] instanceof AdminGroup)){
					if($group[0]->isParent()){
						$groups[] = $group[0];
						$groups = array_merge($groups, $group[0]->getSubGroups());
					}
					else{
						$parent = $group[0]->getParent();
						$groups = $parent->getSubGroups();
						$groups[] = $parent;
					}
				}
				else{
					$groups[] = $group[0];
				}
			}
			$this->sub_tpl->set('groups', $groups);
			$this->sub_tpl->set('addedTravelers', $addedTravelers);
			$this->sub_tpl->set('existingTravelers', $this->data['existingTravelers']);
			$this->sub_tpl->set('existingTravelerIDs', $this->data['existingTravelerIDs']);
			$this->sub_tpl->set('duplicateUsername', $duplicateUsername);
			$this->sub_tpl->set('duplicateEmail', $duplicateEmail);
		}
		
		private function _joingroups(){
			$joinedgroups = array();
			$joinedtravelers = array();
			$this->data['travelerids'] = array_merge($this->data['travelerids']);
			foreach($this->data['travelerids'] as $iTravelerID){
				$joinedtravelers[] = $this->file_factory->getClass('Traveler', array($iTravelerID));
			}
			
			/*
			if (in_array(3,$this->data['groupids'])){ // for testing in group #3 only
			*/
				$parentGid = 0;
				$subgroups  = array();
				$joinedgroups = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create($this->data['groupids']);
			
				foreach($joinedgroups as $iGroup){				
					if ($iGroup->isParent()){
						$parentGid = $iGroup->getGroupID();
					}else{
						$subgroups[] = $iGroup;
					}				
					foreach($joinedtravelers as $iTraveler)
						$iGroup->addMember($iTraveler);
				}
			
				if (0 < $parentGid){				
					$notifGroupName = (0 < count($subgroups)) ?  $subgroups[0]->getName() :  '' ;				
					// send notification to these joined travelers
					require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
					RealTimeNotifier::getInstantNotification(RealTimeNotifier::CREATED_AUTOMATED_TRAVELER, array('TRAVELERS' => $joinedtravelers, 'GROUP_ID' => $parentGid, 'NOTIF_GROUPNAME' => $notifGroupName ))->send();				
				}
			
			/*	
			}else{
				
				foreach($this->data['groupids'] as $iGid){
					$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($iGid));

					// send notification to these joined travelers
					require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
					RealTimeNotifier::getInstantNotification(RealTimeNotifier::CREATED_AUTOMATED_TRAVELER, array('TRAVELERS' => $joinedtravelers, 'GROUP_ID' => $iGid))->send();

					foreach($joinedtravelers as $iTraveler)
						$group[0]->addMember($iTraveler);
					$joinedgroups[] = $group[0];
				}
				
			}
			*/
				
			
			$this->sub_tpl->set('gID', $this->data['gID']);
			$this->sub_tpl->set('joinedtravelers', $joinedtravelers);
			$this->sub_tpl->set('joinedgroups', $joinedgroups);
		}
		
		private function _delete(){
			//echo '<pre>'; print_r($this->data['travelerids']); echo '</pre>';
			$db = $this->file_factory->getClass('dbHandler', array('db=Travel_Logs'));
			$s = '\'' . implode('\', \'', $this->data['travelerids']) . '\'';
			$sql = 'DELETE FROM `Travel_Logs`.`tblTraveler` WHERE `travelerID` IN (' . $s . ')';
			$db->execute($sql);
			$this->sub_tpl->set('notice', 'The following travelers with ids {' . $s . '} were deleted!');
		}
		
		
		private function getMethod(){
			if(isset($this->data['method']))
				return $this->data['method'];
			elseif(isset($this->data['btnLoad']))
				return 'load';
			elseif(isset($this->data['btnCreate']))
				return 'addtraveler';
			elseif(isset($this->data['btnJoinGroups']))
				return 'joingroups';
			elseif(isset($this->data['btnDelete']))
				return 'delete';
			else
				return '';
		}
		
		private function resubmitDuplicate(){
			$dupUsername = $_REQUEST['duplicateUsername'];
			$dupPassword = $_REQUEST['duplicatePassword'];
			$dupFirstname = $_REQUEST['duplicateFirstname'];
			$dupLastname = $_REQUEST['duplicateLastname'];
			$dupEmail = $_REQUEST['duplicateEmail'];
			foreach ($dupUsername as $k => $dUsername){
			    $this->data['travelersToBeAdded'][$k]['username']  = $dUsername;
			    $this->data['travelersToBeAdded'][$k]['password']  = isset($dupPassword[$k]) ? $dupPassword[$k] : '';
			    $this->data['travelersToBeAdded'][$k]['firstname'] = isset($dupFirstname[$k]) ? $dupFirstname[$k] : '';
			    $this->data['travelersToBeAdded'][$k]['lastname']  = isset($dupLastname[$k]) ? $dupLastname[$k] : '';
			    $this->data['travelersToBeAdded'][$k]['email']     = isset($dupEmail[$k]) ? $dupEmail[$k] : '';
			    $this->data['traveleridtobeadded'][] = $k;
			}
		}
		
		private function loadFile(){
			$filenameExtension = substr($this->data['uploaded_filename'], -3, 3);
			if(FALSE == ($fileHandler = fopen($this->data['uploaded_filename'], 'r')))
				throw new exception ($this->data['uploaded_filename'] . ' not found!');
				
			if('csv' == $filenameExtension){ // load information from a csv file (comma-separated csv file)
				$ctr = 0;
				while(FALSE != ($data = fgetcsv($fileHandler,1000, ','))){
					$dataCtr = 0;
					foreach($data as $iData){
						$this->data['travelersToBeAdded'][$ctr][(array_key_exists($dataCtr, self::$headers) ? self::$headers[$dataCtr]: $dataCtr)] = $iData;
						$dataCtr++;
					}
					$ctr ++;
				}
			}
			elseif('xls' == $filenameExtension){ // load information from an excel file
				require_once('phpExcelReader/reader.php');
				$excelReader = new Spreadsheet_Excel_Reader;
				$excelReader->setOutputEncoding('CP1251');
				$excelReader->read($this->data['uploaded_filename']);  // throws exception if file is not readable
				
				/** START: Don't edit : mejo tricky **/
				$entryCtr = 1;
				
				// loop through every worksheet
				for($sheetCtr = 0; $sheetCtr < count($excelReader->sheets); $sheetCtr++){
					for ($rowCtr = 1; $rowCtr <= $excelReader->sheets[$sheetCtr]['numRows']; $rowCtr++, $entryCtr++) {
						if(isset($excelReader->sheets[$sheetCtr]['cells'][$rowCtr])){
							for ($colCtr = 1; $colCtr <= $excelReader->sheets[$sheetCtr]['numCols']+1; $colCtr++) {
								if($this->data['generateUsername']){
									if($colCtr > 4) // just take first 3 columns
										break;	
									// generate username from firstname and lastname, remove unfriendly chars
									$this->data['travelersToBeAdded'][$entryCtr-1][self::$headers[0]] = preg_replace('/[^a-zA-Z0-9]/', '',ucfirst((isset($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][1]) ? trim($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][1]) : '')) . ucfirst((isset($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][2]) ? trim($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][2]) : '')));
									// move succeeding columns to col+1
									$this->data['travelersToBeAdded'][$entryCtr-1][(array_key_exists($colCtr-1, self::$headers) ? self::$headers[$colCtr-1]: $colCtr)] = isset($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][$colCtr-1]) ? trim($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][$colCtr-1]) : '';
								}
								else{
									if($colCtr > 4) // just take first 4 columns
										break;
									$this->data['travelersToBeAdded'][$entryCtr-1][(array_key_exists($colCtr-1, self::$headers) ? self::$headers[$colCtr-1]: $colCtr)] = isset($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][$colCtr]) ? trim($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][$colCtr]) : '';
								}
							}

							//check if email or generated username is in traveler
							$email = isset($this->data['travelersToBeAdded'][$entryCtr-1]['email']) ? $this->data['travelersToBeAdded'][$entryCtr-1]['email'] : '';
							$sql = 'SELECT `travelerID` FROM `Travel_Logs`.`tblTraveler` WHERE (TRIM(`email`) != "" AND `tblTraveler`.`email` = ' .  GaString::makeSqlSafe($email) . ') OR `username` = ' . GaString::makeSqlSafe($this->data['travelersToBeAdded'][$entryCtr-1]['username']) . ' LIMIT 0,1';
							//$sql = 'SELECT `travelerID` FROM `Travel_Logs`.`tblTraveler` WHERE (TRIM(`email`) != "" AND `tblTraveler`.`email` = ' .  GaString::makeSqlSafe($email) . ') LIMIT 0,1';
							//$sql = 'SELECT `travelerID` FROM `Travel_Logs`.`tblTraveler` WHERE `username` = ' . GaString::makeSqlSafe($this->data['travelersToBeAdded'][$entryCtr-1]['username']);
							$rs = new iRecordset($this->mDb->execute($sql));
							if(0 < $rs->retrieveRecordcount()){
								$this->data['existingTravelers'][] = $this->file_factory->getClass('Traveler', array($rs->gettravelerID(0)));
								$this->data['existingTravelerIDs'][] = $rs->gettravelerID(0);
								unset($this->data['travelersToBeAdded'][$entryCtr-1]);

							}
						}
					}
				}
				//echo '<pre>'; print_r($this->data['travelersToBeAdded']); echo '</pre>';
				/** END: Don't edit : mejo tricky **/
			}
			else{
				$code = <<< SCRIPT
				<script>
					Alert('Must be an MS-Excel file or a .csv file')
				</script>
SCRIPT;
				Template::includeDependent($code);
			}
		}// end of function load file
	}
?>
