<?php
	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/factory/Class.FileFactory.php');
	require_once('Class.ToolMan.php');
	require_once('Class.GaString.php');
	require_once('travellog/model/admin/Class.AdminUserAccount.php');

	class AddGroupsController {
		
		private $session_obj 	= null;
		private $file_factory	= null;
		private $cobrandGroups	= array();
		private $mDb			= null;
		
		public function __construct(){
			
			$this->session_obj 	= SessionManager::getInstance();
			$this->file_factory	= FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('dbHandler', array('path' => ''));
			$this->file_factory->registerClass('iRecordset', array('path' => ''));
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('GroupPrivacyPreference');
			
			$this->mDb = $this->file_factory->getClass('dbHandler');
			$this->file_factory->invokeStaticClass('Template', 'setMainTemplate', array('travellog/views/tpl.LayoutMain.php'));
			$this->file_factory->invokeStaticClass('Template', 'includeDependentJs', array('/min/f=/js/prototype.js'));
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		}
		
		public function performAction(){
			if(!$this->session_obj->isSiteAdmin())
				ToolMan::redirect('/admin/login.php?failedLogin&redirect=addgroups.php');
			if (!AdminUserAccount::validateAdminView($this->session_obj->get('userAccess'))){
							header("location:index.php");
							exit;
				}
				
			//require_once('gaLogs/Class.GaLogger.php'); 
			//GaLogger::create()->start($this->session_obj->get('travelerID'),'ADMIN');
			
			$script = <<<SCRIPT
				<script type="text/javascript">
					//<[!CDATA[
					function deleteColumn(tblID, colNum){
						var allRows = $(tblID).rows;
						for(var ctr = 0; ctr<allRows.length; ++ctr){
							if (allRows[ctr].cells.length > 1){
								allRows[ctr].deleteCell(colNum-1);
							}
						}
						if(allRows[0].cells.length > 1){
							for(ctr = 0; ctr<allRows[0].cells.length; ++ctr){
								var s = "select#grpFields" + (allRows[0].cells[ctr].id)
								var b = $$(s);
								if(b.length){
									b[0].id = "grpFields" + (ctr+1);
								}
								allRows[0].cells[ctr].id = ctr+1;
							}
						}
					}
					
					function prepareData(tblID, action){
						
						var obj = new Object;
						var chk = $$('.chkDetail');
						for (chkCtr = 0; chkCtr < chk.length; ++chkCtr){
							if(chk[chkCtr].checked){
								var t = $$('.row' + chk[chkCtr].value);
								var b = new Object;
								for (x = 0; x < t.length; ++x){
									var sel = $('grpFields' + (x+2));
									b[x] = [sel.value, t[x].innerHTML ];
								}
								obj[chkCtr] = b;
							}
						}
						$('hidData').value = Object.toJSON(obj);
						$('action').value = action;
						return confirm('Are you sure you want to create groups?');
					}
					
					function toggleCheckbox(checked, clsName){
						var b = $$('.'+clsName);
						for( ctr = 0; ctr < b.length; ++ctr){
							b[ctr].checked = checked;
						}
					}
					//]]>
				</script>
SCRIPT;
			$this->file_factory->invokeStaticClass('Template', 'includeDependent', array($script));
			$rs = $this->file_factory->getClass('iRecordset', array($this->mDb->execute(
				'SELECT DISTINCT `groupID` FROM `Travel_Logs`.`tblConfig` WHERE groupID > 0 '
			)));
			foreach($rs as $row){
				$mGroup = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($row['groupID']));
				if($grp = $mGroup[0])
					$this->cobrandGroups[] = $grp;
			}
			$this->{isset($_REQUEST['action']) ? $_REQUEST['action'] : 'showForm'}();
		}
		
		private function showForm($vars = array()){
			$vars = array_merge(array('details' => array()), $vars);
			$this->showTemplate('tpl.AddGroups.php', $vars);
		}
		
		private function loadFile(){
			$errors = array();
			
			if(empty($_FILES) or (isset($_FILES['file']) && '' == trim($_FILES['file']['name'])))
				$errors[] = 'No file selected.';
			elseif(isset($_FILES['file']) && 'xls' != substr($_FILES['file']['name'],-3, 3))
				$errors[] = 'Please select an MS Excel File.';
		
			if(empty($errors)){
				
				$ext = substr($_FILES['file']['name'],-3, 3);
				$uploadedFile = 'upload/' . md5(preg_replace('/\.' . $ext . '$/', '', $_FILES['file']['name'])) . '.' . $ext;
				//$uploadedFile = 'upload2/' . md5(preg_replace('/\.' . $ext . '$/', '', $_FILES['file']['name'])) . '.' . $ext;
				move_uploaded_file($_FILES["file"]["tmp_name"],$uploadedFile);
				$vars = array();
				
				require_once('phpExcelReader/reader.php');
				$excelReader = new Spreadsheet_Excel_Reader;
				$excelReader->setOutputEncoding('CP1251');
				
				try{
					$excelReader->read($uploadedFile);
				}
				catch(exception $e){
					$errors[] = 'An unexpected error occured! ' . $e->getMessage();
				}
				
				$details = array();
				// loop through every worksheet
				$numCols = 0;
				for($sheetCtr = 0; $sheetCtr < count($excelReader->sheets); ++$sheetCtr){
					
					// loop through all rows
					for ($rowCtr = 1; $rowCtr <= $excelReader->sheets[$sheetCtr]['numRows']; ++$rowCtr){
						
						if(!isset($excelReader->sheets[$sheetCtr]['cells'][$rowCtr])) // empty-celled row
							continue;
						
						$numCols = ($excelReader->sheets[$sheetCtr]['numCols']+1) > $numCols ? $excelReader->sheets[$sheetCtr]['numCols']+1 : $numCols;
						$rowCells = array();
						for ($colCtr = 1; $colCtr < $excelReader->sheets[$sheetCtr]['numCols']+1; ++$colCtr){
							$rowCells[] = $excelReader->sheets[$sheetCtr]['cells'][$rowCtr][$colCtr];
						}
						$details[] = $rowCells;
					}
					
				}
				
				$sql = 	'SELECT `COLUMN_NAME` as fields FROM `information_schema`.`Columns` ' .
						'WHERE ' . 
							'`TABLE_SCHEMA` = ' . GaString::makeSqlSafe('Travel_Logs') . ' ' .
							'AND `TABLE_NAME` = ' . GaString::makeSqlSafe('tblGroup') . ' ' .
							'AND `COLUMN_NAME` IN (\'name\', \'description\', \'parentID\') ';
				$rs = $this->file_factory->getClass('iRecordset', array($this->mDb->execute($sql)));
				$vars = array(
					'details' => $details,
					'numCols' => $numCols,
					'fields' => $rs->retrieveRecordcount() ? $rs->retrieveColumn('fields') : array()
				);
			}
			$vars['errors'] = $errors;
			$vars['cobrandGroups'] = $this->cobrandGroups;
			$this->showForm($vars);
		}
		
		private function createGroups(){
			require_once('JSON.php');
			require_once('Class.GaDateTime.php');
			$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
			$data = array();
			if(isset($_POST['hidData'])){
				$data = get_magic_quotes_gpc() ? $json->decode(stripslashes($_POST['hidData'])) : $json->decode($_POST['hidData']);
			}
			
			$parentGroupID = isset($_POST['parentGroup']) ? $_POST['parentGroup'] : 0;
			$parentGroup = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($parentGroupID));
			
			if(!$parentGroup[0])
				$this->showForm(array('errors' => array('Please choose a parent group!')));
			
			$createdGroups = array();
			foreach( $data as $k => $v ){
				$group = new AdminGroup;
				foreach($v as $x => $detail){
					if(method_exists($group, 'set' . ucfirst($detail[0])))
						$group->{'set' . ucfirst($detail[0])}($detail[1]);
				}
				
				$group->setAdministratorID($parentGroup[0]->getAdministratorID());
				$group->setParentID($parentGroup[0]->getGroupID());
				$group->setDiscriminator(Group::ADMIN);
				$group->Create();
				
				$groupPref = $this->file_factory->getClass('GroupPrivacyPreference');
				$groupPref->setGroupID($group->getGroupID());
				$groupPref->setViewAlerts(1);
				$groupPref->setDownloadFiles(1);
				$groupPref->setViewGroups(1);
				$groupPref->setViewBulletin(1);
				$groupPref->setViewCalendar(1);
				$groupPref->update();
				
				$this->mDb->execute(
					'INSERT INTO `Travel_Logs`.`tblAutomatedGroups` SET ' .
					'`groupID` = ' . GaString::makeSqlSafe($group->getGroupID()) . ', ' .
					'`dateGenerated` = ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat())
				);
				$createdGroups[] = $group;
			}
			$this->showForm(array('parentGroup' => $parentGroup[0], 'createdGroups' => $createdGroups, 'status' => 'Created'));
		}
		
		private function searchGroups(){
			
			require_once('JSON.php');
			$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
			$data = array();
			if(isset($_POST['hidData'])){
				$data = get_magic_quotes_gpc() ? $json->decode(stripslashes($_POST['hidData'])) : $json->decode($_POST['hidData']);
			}
			
			$createdGroups = array();
			foreach( $data as $k => $v ){
				
				$sql = 'SELECT * FROM `Travel_Logs`.`tblGroup` WHERE 1=1 ';
				foreach($v as $x => $detail){
					$sql .= 'AND `tblGroup`.`' . $detail[0] . '` = ' . GaString::makeSqlSafe($detail[1]) . ' ';
				}
				$rs = $this->file_factory->getClass('iRecordset', array($this->mDb->execute($sql)));
				foreach($rs as $row){
					$group = new AdminGroup;
					$group->setAdminGroupData($row);
					$createdGroups[] = $group;
				}
			}
			$this->showForm(array('parentGroup' => null, 'createdGroups' => $createdGroups, 'status' => 'Matched'));
		}
		
		private function manage(){
			switch($_POST['manage']){
				case 'delete' :
					$this->_deleteGroups();
					break;
				default;
			}
			ToolMan::redirect('addgroups.php');
		}
		
		
		private function _deleteGroups(){
			foreach( $_POST['chkGroup'] as $gID){
				$mGroup = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($gID));
				$mGroup[0]->Delete();
			}
			ToolMan::redirect('addgroups.php');
		}
		
		private function showTemplate($tplName, $vars = array(), $method = 'out'){
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/admin/');
			$tpl->setVars($vars);
			$tpl->{$method}($tplName);
		}
	}
	
?>