<?php
/***
 * Created on Dec 10, 08
 *
 * @author 
 * Purpose: 
 */

	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/factory/Class.FileFactory.php');
	require_once('travellog/model/Class.RotatingPhoto.php');
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterCriteria2.php');
	require_once('travellog/model/Class.Group.php');
	require_once('travellog/model/Class.Traveler.php');
	require_once 'Class.Paging.php';
	require_once('travellog/model/admin/Class.AdminUserAccount.php');
	
	
	class RotatingPhotoController {
				
		private $obj_session	= null;
		private $file_factory	= null;
		private $mDb			= null;
		
		private $mPhotosPerPage = 10;
		private $mFilterBy		= "status";
		private $mFilter		= "0";
		private $mOrderBy		= "dateUploaded";
		private $mOrder			= "DESC";
		
		public function __construct() {
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory	= FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
		}
		
		public function performAction() {
			if (!$this->obj_session->isSiteAdmin()) {
				header('location:login.php?failedLogin&redirect=rotatingPhoto.php');
				exit;
			}
			if (!AdminUserAccount::validateAdminView($this->obj_session->get('userAccess'))){
						header("location:index.php");
						exit;
			}
			$_REQUEST['action'] = (!empty($_REQUEST['action']))? $_REQUEST['action']: '';
			switch($_REQUEST['action']) {
				case 'view':
					$this->displayRotatingPhotos($_REQUEST);
					break;
				case 'upload':
					$photos = $this->UploadPhoto();
					$this->displayUploadedPhotos($photos);
					RotatingPhoto::instance()->invalidateCacheEntry();					
					break;
				case 'displayUploadForm':
					$this->displayMain();
					break;
				case 'edit':
					$this->displayUploadedPhotos($this->editPhotos($_REQUEST));
					break;
				case 'editPhoto':
					$photo = array(new RotatingPhoto($_REQUEST['id']));
					$this->displayUploadedPhotos($photo);
					RotatingPhoto::instance()->invalidateCacheEntry();
					break;
				case 'displayEditPreference':
					$this->displayEditPreferenceForm();
					break;
				case 'editPreference':
					$this->editPreference();
					break;
				case 'replacePhoto':
					$uploads_dir = "../../goabroad.net/images/rotating";
					$old_name = $_REQUEST['hidPhotoName'];
					if($_FILES["photo"]["error"]==UPLOAD_ERR_OK) {
						if (($_FILES["photo"]["type"] == "image/gif")
	  					|| ($_FILES["photo"]["type"] == "image/jpeg")
	  					|| ($_FILES["photo"]["type"] == "image/pjpeg")
	  					|| ($_FILES["photo"]["type"] == "image/jpg")
	  					|| ($_FILES["photo"]["type"] == "image/png" )
	  					&& ($_FILES["photo"]["size"] < 2000000)) {
	  						unlink("$uploads_dir/$old_name");
	  						$tmp_name = $_FILES["photo"]["tmp_name"];
					        $ext = explode(".",$_FILES["photo"]["name"]);
					        $name = basename($_FILES["photo"]["name"]);
					        $photo = new RotatingPhoto($_REQUEST['hidPhotoID']);
					        $photo->setFilename("$name");
					        $photo->save();
					        $photo->savePhotoName();
					        
					        move_uploaded_file($tmp_name, "$uploads_dir/$old_name");
	  					}
					}
					RotatingPhoto::instance()->invalidateCacheEntry();
					header("location:rotatingPhoto.php?action=view");
					break;		
				case 'search':
					$this->search($_REQUEST);
					break;
				case 'linkPhoto':
					$photo = $this->edit($_REQUEST);
					echo $photo->getFrom();
					echo "<<<<>>>>";
					echo $photo->getTypeID();
					break;
				case 'showFilter':
					$this->displayTravelerFilter($_REQUEST["i"]);
					break;
				default:
					$this->displayMain();
					break;
			}
		}
		
		private function displayTravelerFilter($i) {
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/admin/');
			$tpl->setVars(array('i'=>$i));
			
			$tpl->out('tpl.IncTravelerComboSearch.php');
		}
		
		private function searchTraveler($keyword,$filter_type) {
			$arrays = array();
			$cond = new Condition;
			$cond->setAttributeName($filter_type);
			$cond->setOperation(FilterOp::$LIKE);
			$cond->setValue($keyword);
			
			$cond2 = new Condition;
			$cond2->setAttributeName("isSuspended");
			$cond2->setOperation(FilterOp::$EQUAL);
			$cond2->setValue(0);
		
			$filter = new FilterCriteria2();
			$filter->addCondition($cond);
			$filter->addBooleanOp('AND');
			$filter->addCondition($cond2);
			$filter->addBooleanOp('AND');
			
			$travelers = Traveler::getAllTravelers($filter,"",$this->obj_session->get('travelerID'));
			foreach($travelers as $traveler) {
				
				$array['id'] = $traveler->getTravelerID();
				$array['name'] = $traveler->getUserName();
				$array['link'] = "/".$traveler->getUserName();
				$arrays[] = $array;
			}
			return $arrays;
		}
		
		private function searchGroup($keyword) {
			$arrays = array();
			$cond = new Condition;
			$cond->setAttributeName("name");
			$cond->setOperation(FilterOp::$LIKE);
			$cond->setValue($keyword);
			
			$cond2 = new Condition;
			$cond2->setAttributeName("isSuspended");
			$cond2->setOperation(FilterOp::$EQUAL);
			$cond2->setValue(0);
		
			$filter = new FilterCriteria2();
			$filter->addCondition($cond);
			$filter->addBooleanOp('AND');
	
			$grpFilteredGroups = Group::getFilteredGroups($filter);
			foreach($grpFilteredGroups as $group) {
				$array['id'] = $group->getGroupID();
				$array['name'] = $group->getName();
				$array['link'] = $group->getFriendlyUrl();
				$arrays[] = $array;
			}
			return $arrays; 
		}
		
		private function search($values) {
			$keyword = $values["keyword"];
			switch($values["typeID"]) {
				case '0':
					$users = $this->searchTraveler($keyword,$values["filter"]);
					break;
				case '1':
					$users = $this->searchGroup($keyword);
					break;
			}
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/admin/');
			$tpl->setVars(array('users' => $users,'num'=>$values["num"]));
			$tpl->out('tpl.RotatingPhotoUserSearch.php');
		}
		
		private function edit($values) {
			$photo = new RotatingPhoto($values["photoID"]);
			$photo->setTypeID($values["typeID"]);
			$photo->setUserID($values["userID"]);
			$photo->save();
			return $photo;
		}
		
		private function editPhotos($values) {
			$photos = array();
			for($i=0;$i<$values["count"];$i++) {
				$photo = new RotatingPhoto($values["hidPhotoID-$i"]);
				$photo->setCaption(($values["txtCaption-$i"]));
				$photo->setStatus($values["radStatus-$i"]);
				$photo->setUserID($values["hidUserID-$i"]);
				$photo->setTypeID($values["radType-$i"]);
				$photo->save();
				$photos[] = $photo;
			}
			return  $photos;
		}
		
		private function displayMain() {
			$this->showTemplate('tpl.UploadRotatingPhotos.php');
		}
		
		private function UploadPhoto() {
			
			$uploads_dir = "../../goabroad.net/images/rotating";
			if (!is_dir($uploads_dir)) {
				throw new exception("Directory does not exist: $uploads_dir");
				//mkdir($uploads_dir,0777);
			}
			$photos = array();
			foreach ($_FILES["photos"]["error"] as $key => $error) {
			    if (($_FILES["photos"]["type"][$key] == "image/gif")
  					|| ($_FILES["photos"]["type"][$key] == "image/jpeg")
  					|| ($_FILES["photos"]["type"][$key] == "image/pjpeg")
  					|| ($_FILES["photos"]["type"][$key] == "image/jpg")
  					|| ($_FILES["photos"]["type"][$key] == "image/png" )
  					&& ($_FILES["photos"]["size"][$key] < 2000000)) {
			    	if ($error == UPLOAD_ERR_OK) {
			        	
				        $tmp_name = $_FILES["photos"]["tmp_name"][$key];
				        $ext = explode(".",$_FILES["photos"]["name"][$key]);
				        $name = basename($_FILES["photos"]["name"][$key]);
				        $photo = new RotatingPhoto();
				        $photo->setFilename("$name");
				        $photo->setDateUploaded(date("Y-m-d"));
				        $photo->setStatus(0);
				        $photo->save();
				        
				        $photo_name = RotatingPhoto::PHOTO_NAME.$photo->getPhotoID().".".$ext[count($ext)-1];
				        $photo->savePhotoName();
				        move_uploaded_file($tmp_name, "$uploads_dir/$photo_name");
				        $photos[] = $photo;
  					}
			    }
			}
			return $photos;
		}
		
		private function displayRotatingPhotos($_REQUEST=null) {
			$photo = new RotatingPhoto();
			
			$page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
			$rows = (isset($_REQUEST['rows'])) ? $_REQUEST['rows'] : $this->mPhotosPerPage;
			
			$paging = array("page"=>$page,"rows"=>$rows);
			
			$this->mOrderBy		= (isset($_REQUEST['order_by'])) ? $_REQUEST['order_by'] : $this->mOrderBy;
			$this->mOrder		= (isset($_REQUEST['order'])) ? $_REQUEST['order'] : $this->mOrder;
			$this->mFilterBy 	= (isset($_REQUEST['filter_by'])) ? $_REQUEST['filter_by'] : $this->mFilterBy;
			$this->mFilter		= (isset($_REQUEST['filter'])) ? $_REQUEST['filter'] : $this->mFilter;
			$this->mPhotosPerPage = (isset($_REQUEST['rows'])) ? $_REQUEST['rows'] : $this->mPhotosPerPage;
			
			$photos = $photo->getPhotos($paging,array("order_by"=>$this->mOrderBy,"order"=>$this->mOrder),array("filter_by"=>$this->mFilterBy,"filter"=>$this->mFilter));
			return $this->viewRotatingPhotos($photos,$page);
		}
		
		private function viewRotatingPhotos($photos = null,$page=null) {
			if ($photos==null || !is_array($photos)) {
				$this->displayMain();
				exit;
			}
			$photo = new RotatingPhoto();
			$count = $photos["num_records"];
			unset($photos["num_records"]);
			$page = (!is_null($page))? $page:1;
			$qrystr = "action=view";
			if (isset($_REQUEST['filter_by']) && isset($_REQUEST['filter'])) {
				$qrystr .= "&filter_by=".$_REQUEST['filter_by'];
				$qrystr .= "&filter=".$_REQUEST['filter'];
			}
			if (isset($_REQUEST['order_by']) && isset($_REQUEST['order'])) {
				$qrystr .= "&order_by=".$_REQUEST['order_by'];
				$qrystr .= "&order=".$_REQUEST['order'];
			}
			if (isset($_REQUEST['rows']))
				$qrystr .= "&rows=".$_REQUEST['rows'];
			$paging = new Paging( $count, $page, $qrystr, $this->mPhotosPerPage);
			
			$activeRP = $photo->getPhotos(array(),array(),array("filter_by"=>"status","filter"=>0));
			$active = $activeRP["num_records"];
			unset($activeRP["num_records"]);
			
			$inactiveRP = $photo->getPhotos(array(),array(),array("filter_by"=>"status","filter"=>1));
			$inactive =  $inactiveRP["num_records"];
			unset($inactiveRP["num_records"]);
			
			$vars = array(
					'photos'				=>	$photos,
					'traveler_type_id'		=>	RotatingPhoto::TRAVELER_TYPE_ID,
					'group_type_id'			=>	RotatingPhoto::GROUP_TYPE_ID,
					'photo_active_status'	=>	RotatingPhoto::PHOTO_ACTIVE_STATUS,
					'photo_inactive_status'	=>	RotatingPhoto::PHOTO_INACTIVE_STATUS,
					'photos_per_page'		=>	$this->mPhotosPerPage,
					'order'					=>	array("order_by"=>$this->mOrderBy,"order"=>$this->mOrder),
					'filter'				=>	array("filter_by"=>$this->mFilterBy,"filter"=>$this->mFilter),
					'count'					=>	$count,
					'editPreferenceForm'	=>	$this->displayEditPreferenceForm(),
					'active'				=>	$active,
					'inactive'				=>	$inactive,
					'paging'				=>	$paging);
			$this->showTemplate('tpl.ViewRotatingPhotos.php',$vars);
		}
		
		private function displayUploadedPhotos($photos = null) {
			if ($photos==null || !is_array($photos)) {
				$this->displayMain();
				exit;
			}
			$vars = array(
					'photos'				=>	$photos,
					'traveler_type_id'		=>	RotatingPhoto::TRAVELER_TYPE_ID,
					'group_type_id'			=>	RotatingPhoto::GROUP_TYPE_ID,
					'photo_active_status'	=>	RotatingPhoto::PHOTO_ACTIVE_STATUS,
					'photo_inactive_status'	=>	RotatingPhoto::PHOTO_INACTIVE_STATUS
			);
			$this->showTemplate('tpl.ViewUploadedRotatingPhotos.php',$vars);
		}
		
		private function displayEditPreferenceForm() {
			
			/*$this->mOrderBy = (isset($_REQUEST['order_by'])) ? $_REQUEST['order_by'] : $this->mOrderBy;
			$this->mOrder = (isset($_REQUEST['order'])) ? $_REQUEST['order'] : $this->mOrder;
			$this->mFilterBy = (isset($_REQUEST['filter_by'])) ? $_REQUEST['filter_by'] : $this->mFilterBy;
			$this->mFilter = (isset($_REQUEST['filter'])) ? $_REQUEST['filter'] : $this->mFilter;*/
			
			$vars = array(
					'photos_per_page'		=> $this->mPhotosPerPage,
					'order'					=>	array("order_by"=>$this->mOrderBy,"order"=>$this->mOrder),
					'filter'				=>	array("filter_by"=>$this->mFilterBy,"filter"=>$this->mFilter),
			);
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/admin/');
			$tpl->setVars($vars);
			$tpl->setTemplate('tpl.FrmRotatingPhotoPreference.php');
			return $tpl;
		}
		
		private function editPreference() {
			$this->mPhotosPerPage 	= (isset($_REQUEST['txtMaxPhotoPerPage']) && !empty($_REQUEST['txtMaxPhotoPerPage'])) ? $_REQUEST['txtMaxPhotoPerPage'] : $this->mPhotosPerPage;
			$this->mFilterBy		= (isset($_REQUEST['hidFilterBy'])) ? $_REQUEST['hidFilterBy'] : $this->mFilterBy;
			$this->mFilter			= (isset($_REQUEST['cmbFilter'])) ? $_REQUEST['cmbFilter'] : $this->mFilter;
			$this->mOrderBy			= (isset($_REQUEST['hidOrderBy'])) ? $_REQUEST['hidOrderBy'] : $this->mOrderBy;
			$this->mOrder			= (isset($_REQUEST['cmbOrder'])) ? $_REQUEST['cmbOrder'] : $this->mOrder;
			
			$_REQUEST['rows']		= $this->mPhotosPerPage;
			$_REQUEST['filter_by']	= $this->mFilterBy;
			$_REQUEST['filter']		= $this->mFilter;
			$_REQUEST['order_by']	= $this->mOrderBy;
			$_REQUEST['order']		= $this->mOrder;
			
			$this->displayRotatingPhotos($_REQUEST);
		}
		
		private function showTemplate($tplName, $vars = array(), $method = 'out') {
			$this->file_factory->invokeStaticClass('Template', 'setMainTemplate', array('travellog/views/tpl.LayoutMain.php'));
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$tpl = $this->file_factory->getClass('Template');
			$tpl->includeDependentJs('js/rotatingPhoto.js');
			$tpl->set_path('travellog/views/admin/');
			$tpl->setVars($vars);
			$tpl->{$method}($tplName);
		}
	}
?>