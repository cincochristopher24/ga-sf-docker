<?php
	require_once('travellog/factory/Class.FileFactory.php');
	require_once('Class.GaString.php');
	require_once('Class.iRecordset.php');
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('Class.ToolMan.php');
	require_once('travellog/model/Class.SessionManager.php');
	
	class ManageNewTravelerController {
		
		private $file_factory 	= null;
		private $traveler 		= null;
		private $searchBy		= null;
		private $keyword		= null;
		private $group			= null;
		private $groupID		= null;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('dbHandler', array('path' => ''));
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
			
			$this->file_factory->invokeStaticClass('Template', 'setMainTemplate', array('travellog/views/tpl.LayoutMain.php'));
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->mDb = $this->file_factory->getClass('dbHandler', array('db=Travel_Logs'));
		}
		
		public function performAction(){
			$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', 'Manage New Traveler'));
			$travelerID 	= isset($_REQUEST['tID']) ? $_REQUEST['tID'] : 0;
			$this->groupID	= isset($_REQUEST['gID']) ? $_REQUEST['gID'] : 0;
			$this->searchBy	= isset($_REQUEST['searchBy']) ? $_REQUEST['searchBy'] : 'username';
			$this->keyword 	= isset($_REQUEST['txtKeyword']) ? $_REQUEST['txtKeyword'] : null;
						
			$session		= SessionManager::getInstance();
			
			$loggedTravelerID = $session->get('travelerID');
			if( $loggedTravelerID ){
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($loggedTravelerID,'ADMIN');
			}
			
			try{
				$this->traveler = $this->file_factory->getClass('Traveler', array($travelerID));
				$arr			= GroupFactory::instance()->create(array($this->groupID));
				$this->group	= $arr[0];
			}
			catch(exception $e){}
			$this->{isset($_REQUEST['action']) ? $_REQUEST['action'] : 'showForm'}();
		}
		
		private function showForm($vars = array()){
			$default = array(
				'searchBy'	=> $this->searchBy,
				'keyword'	=> $this->keyword,
				'group'		=> $this->group,
				'gID'		=> $this->groupID,
				'data'		=> null,
				'errors'	=> array()
			);
			$vars = array_merge($default, $vars);
			$this->_showTemplate('tpl.ManageNewTraveler.php', $vars);
		}
		
		private function searchTravelers(){
			$rs = null;
			if( !is_null($this->keyword) && '' != trim($this->keyword) ){
				$sql 	= 'SELECT * FROM `tblTraveler` WHERE ' . $this->searchBy . ' = ' . GaString::makeSqlSafe($this->keyword);
				$rs 	= new iRecordset($this->mDb->execute($sql));
			}
			$this->showForm(array('data' => $rs));
		}
		
		private function updateEmail(){
			$email = isset($_POST['txtEmail']) ? $_POST['txtEmail'] : '';
			$errors = array();
			if(!GaString::isValidEmailAddress($email))
				$errors[] = 'Email Address is not valid';
			
			$sql = 'SELECT * FROM `tblTraveler` WHERE `email` = ' . GaString::makeSqlSafe($email);
			$rs = new iRecordset($this->mDb->execute($sql));
			if( $rs->retrieveRecordcount() )
				$errors[] = $email . ' is a duplicate email.';
			
			if( empty($errors) ){
				$tp = $this->traveler->getTravelerProfile();
				$tp->setEmail($email);
				$tp->UpdateAccountSettings();
			}
			
			
			$vars = array('errors' => $errors);
			$this->showForm($vars);
		}
		
		private function updateTravelerRanking(){
			$confirm = array_key_exists('confirm', $_POST);
			$sql = 	'SELECT DISTINCT `tblTraveler`.* ' . 
					'FROM `tblTraveler` WHERE 1=1 ' .
						'AND `isAutomatedTraveler` >= 1 AND `travelerID` NOT IN (' . 
						'SELECT DISTINCT `travelerID` FROM `tblTravelerRanking`) ';
			$data = new iRecordset($this->mDb->execute($sql));
			$vars = array('confirm' => $confirm);
			if( $confirm ){
				$insertedTravelers = array();
				foreach( $data as $row ){
					try{
						$insertSql = 	'INSERT INTO `tblTravelerRanking` SET ' .
										'`travelerID` = ' .	GaString::makeSqlSafe($row['travelerID']);
						$this->mDb->execute($insertSql);
						$insertedTravelers[] = array(
							'travelerID'	=> $row['travelerID'],
							'username'		=> $row['username'],
							'password'		=> $row['password'],
							'isAutomatedTraveler' => $row['isAutomatedTraveler']
						);
					}
					catch(exception $e){}
				}
				$vars['insertedTravelers'] = $insertedTravelers;
				
				require_once('class.phpmailer.php');
				$mail = new PHPMailer();
				$mail->IsSMTP();
				$mail->IsHTML(true);
				$mail->From 	= 'admin@goabroad.net';
				$mail->FromName = 'GoAbroad.net Admin';
				$mail->Subject 	= 'Traveler Ranking update';
				$mail->Body 	= $this->_showTemplate('tpl.UpdateTravelerRanking.php', $vars, 'fetch');
				$recipients 	= array(
					//'chris.velarde@goabroad.com',
					'gapdaphne.notifs@gmail.com'
				);

				foreach($recipients as $iRecipient){
					$mail->AddAddress($iRecipient);
				}
				$mail->Send();
			}
			else{
				$vars['data'] = $data;
				$this->_showTemplate('tpl.UpdateTravelerRanking.php', $vars);
			}
		}
		
		private function emailNotification(){
			// send notification
			require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
			RealTimeNotifier::getInstantNotification(RealTimeNotifier::CREATED_AUTOMATED_TRAVELER, array('TRAVELERS' => array($this->traveler), 'GROUP_ID' => $this->groupID, 'FORCE_RESEND' => true))->send();
			ToolMan::redirect('manageNewTraveler.php?gID=' . $this->groupID);
		}
		
		private function _showTemplate($tplName, $vars=array(), $method='out'){
			$tpl = $this->file_factory->getClass('Template');
			$tpl->setVars($vars);
			$tpl->set_path('travellog/views/admin/');
			return $tpl->{$method}($tplName);
		}
	}

?>