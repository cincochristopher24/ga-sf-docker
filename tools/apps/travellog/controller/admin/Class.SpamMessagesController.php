<?php
	/**
	 * Class.AdminMessagesController.php
	 * created on 04 28, 08
	 * edits: added check/uncheck all functionality -- 03/20/2009 -- ianne
	 */
	 
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Constants.php");
	require_once("travellog/helper/DisplayHelper.php");
	require_once("travellog/model/admin/Class.AdminUserAccount.php");
	  
	class SpamMessagesController{
		
		public static $instance = NULL;
		
		static function getInstance(){
			if( NULL == self::$instance )
				self::$instance = new SpamMessagesController;
			return self::$instance;	 
		}
		
		function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass('SessionManager', array('path' => 'travellog/model/'));
			//require_once("travellog_/domains/" . $_SERVER['SERVER_NAME'] . ".php");
			
			$this->session_manager = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			
			if( !$this->session_manager->getLogIn() || !$this->session_manager->isSiteAdmin()){
				header("location: login.php?failedLogin&redirect=spam-messages.php?" . $_SERVER['QUERY_STRING']);
				exit;			 
			}
			if (!AdminUserAccount::validateAdminView($this->session_manager->get('userAccess'), array(
					AdminUserAccount::ACCESS_MODERATOR))){
						header("location:index.php");
						exit;
			}
			
			
			$this->file_factory->registerClass('SpamMessagesHelper', array('path' => 'travellog/helper/'));
			$this->file_factory->registerClass('RowsLimit', array('path' => 'travellog/model/'));
			//$this->file_factory->registerClass('SpamMessages', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('GanetSpamMessage', array('path' => 'travellog/model/admin'));
			$this->file_factory->registerClass('GanetSpamMessagePeer', array('path' => 'travellog/model/admin'));
			$this->file_factory->registerClass('SpamBox', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('MessagesFcd'  , array('path' => 'travellog/facade/'));
			$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('Paging', array('path' => ''));
			
			$this->traveler = $this->file_factory->getClass('Traveler',array($this->session_manager->get('travelerID')));
			$this->helper = $this->file_factory->invokeStaticClass('SpamMessagesHelper', 'getInstance');
			
			// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
		}
		
		function performAction(){
			
			switch( $this->helper->getAction() ){
				case constants::SEND:
					return $this->sendSpam();
					break;
				case constants::DELETE:
					return $this->deleteSpam();
					break;
				case 'search':
					return $this->searchSpamMessages();
					break;
				default:
					return $this->viewSpamMessages();
					break;		
			}	
		
		}
		
		function viewSpamMessages(){
			
			$rowsPerPage = $this->helper->getRowsPerPage();
			$page = $this->helper->getCurrPage();
			$rowsLimit = $this->file_factory->getClass('RowsLimit',array($rowsPerPage,$page - 1),true);
			$msgList = $this->file_factory->getClass('SpamBox');
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplateVar('page_location', 'Admin');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
			$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();
	jQuery(document).ready(function(){
		jQuery(".checked").toggle(                    
		    function() {
		        jQuery('.chkbox').attr('checked','checked'); 
				jQuery('.checked').html("Uncheck All");  
		    },
		    function () {
		        jQuery('.chkbox').removeAttr("checked");  
				jQuery('.checked').html("Check All");  
		    }
		);
	});
//]]>  
</script>
BOF;
			Template::includeDependent($code,array("bottom"));
			$nContent = array(
					'spamList' => $msgList->getAllSpam($rowsLimit),
					'ownerID' => $this->traveler->getTravelerID(),
					'obj_paging' => $this->file_factory->getClass('Paging',array( $msgList->getTotalRecords(), $page, constants::PENDING_MESSAGES, $rowsPerPage, false ),true));
			
			$fcd = $this->file_factory->getClass('MessagesFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveSpamList()) );
			$obj_view->render();
			
		}

		function deleteSpam(){
			
			foreach( $this->helper->getMessageIds() as $id ){
				
				$msg = $this->file_factory->invokeStaticClass('GanetSpamMessagePeer','retrieveByPk', array($id));
				$msg->delete();
			}
			
			$this->viewSpamMessages();
		}
		
		function sendSpam(){
			
			foreach( $this->helper->getMessageIds() as $id ){
				
				$msg = $this->file_factory->invokeStaticClass('GanetSpamMessagePeer','retrieveByPk', array($id));
				
				//$msg->setIsIncludedInNotif(gaPersonalMessageDM::isMessageIncludedInNotif($id[0]));
				
				// added by chris: send instant notification
				require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
				RealTimeNotifier::getInstantNotification(RealTimeNotifier::APPROVE_SPAM_MESSAGE, 
					array(
						'MESSAGE' => $msg
					))->send();
				// end add
				
				$msg->approve();
			}
			$this->viewSpamMessages();
		}
		
		function searchSpamMessages(){
			$msgList = $this->file_factory->getClass('SpamBox');

			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplateVar('page_location', 'Admin');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
			$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();
	jQuery(document).ready(function(){
		jQuery(".checked").toggle(                    
		    function() {
		        jQuery('.chkbox').attr('checked','checked'); 
				jQuery('.checked').html("Uncheck All");  
		    },
		    function () {
		        jQuery('.chkbox').removeAttr("checked");  
				jQuery('.checked').html("Check All");  
		    }
		);
	});
//]]>  
</script>
BOF;
			Template::includeDependent($code,array("bottom"));
			$nContent = array(
					'spamList' => $msgList->getSpamMessagesByUsername($this->helper->getKeyword()),
					'ownerID' => $this->traveler->getTravelerID()
			);

			$fcd = $this->file_factory->getClass('MessagesFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveSpamList(false)) );
			$obj_view->render();
		}
		
	}
	 
?>
