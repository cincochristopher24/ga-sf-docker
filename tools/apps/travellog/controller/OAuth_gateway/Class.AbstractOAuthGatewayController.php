<?php
/* 
 * created March 18, 2010
 * 
 */
require_once("travellog/controller/Class.IController.php");
require_once("Class.CookieManager.php");
class AbstractOAuthGatewayController implements iController {
	
	protected $travelerAccountID = 0;
	protected $apiObj = null;
	protected $cookie_manager = null;
	private $destination = null;
	private $domain = null;
	
	public function __construct(){
		$this->cookie_manager = CookieManager::getInstance();
	}
	
	public function performAction(){
		$action_index = $_GET['context'].'_action';
		$server_index = $_GET['context'].'_server';
		
		$serverName = $this->cookie_manager->get($server_index);
		$action = $this->cookie_manager->get($action_index);
		
		$domain = FALSE === strpos($_SERVER["SERVER_NAME"],".local") ? ".goabroad.net" : ".goabroad.net.local";
		$this->cookie_manager->clear($action_index, "/", $domain);
		
		switch($action){
			case "login":
				$this->login();
				break;
			case "connect":
				$this->connect();
				break;
			case "link":
				$this->link();
				break;
			default:
				header("location: http://www.goabroad.net/login.php");
				break;
		}
	//	$this->{$action}();
	}
	
	/* 
	 * private functions
	 */ 
	
	
	private function link(){
		$this->requestWidgetLogin();
	}
	
	private function login(){
		$this->requestWidgetLogin();
	}

	private function requestWidgetLogin() {
		$context = $_GET['context'];
		$server_index = $context.'_server';
		
		$oauth_token = null;
		if($context == 'twitter' AND isset($_GET['oauth_token'])){
			$oauth_token = $_GET['oauth_token'];
		}elseif($context == 'google' AND isset($_REQUEST['openid_ext2_request_token'])){
			$oauth_token = $_REQUEST['openid_ext2_request_token'];
		}elseif($context == 'facebook' AND isset($_GET['code'])){
			$oauth_token = OAuthFacebookAPI::getInstance()->getAccessToken($_GET['code']);
		}		

		$server = $this->cookie_manager->get($server_index);
		$domain = FALSE === strpos($_SERVER["SERVER_NAME"],".local") ? ".goabroad.net" : ".goabroad.net.local";
		$this->cookie_manager->clear($server_index, "/", $domain);
		
		if(is_null($oauth_token)){
			header("location: http://$server/register.php");
			exit;
		}
		
		header("location: http://$server/register.php?context=$context&oauth_token=$oauth_token");
	}
	
	private function connect(){
		$oauth_token = isset($_GET['oauth_token']) ? $_GET['oauth_token'] : null;

		if(is_null($oauth_token) && isset($_GET['code'])) {
			$oauth_token = OAuthFacebookAPI::getInstance()->getAccessToken($_GET['code']);
		}

		$context = $_GET['context'];
		$server_index = $context.'_server';
		
		$serverName = $this->cookie_manager->get($server_index);
		$domain = FALSE === strpos($_SERVER["SERVER_NAME"],".local") ? ".goabroad.net" : ".goabroad.net.local";
		$this->cookie_manager->clear($server_index, "/", $domain);
		
		if(is_null($oauth_token)){
			header("location: http://$serverName/widget.php");
			exit;
		}
		
		
		$server = str_replace('.','_',$serverName);
		$cookieName = $context.'_oauth_token';
		//$domain = $_SERVER['SERVER_NAME'];
		$this->cookie_manager->set($cookieName, $oauth_token, 0, '/', $domain);
		
		
		
		header("location: http://$serverName/widget.php?context=$context");
	}
}