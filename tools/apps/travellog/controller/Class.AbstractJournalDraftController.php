<?php
	/**
	 * @(#) Class.AbstractJournalDraftController.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 27, 2009
	 */
	
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	/**
	 * Journal draft controller
	 */
	class AbstractJournalDraftController {
		
		/**
		 * Constructs a new journal draft controller.
		 */
		function __construct(){
			$session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));		
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('Travel');
			$this->file_factory->registerClass('Map');
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			
			$this->file_factory->registerTemplate('FrmTravelLog');
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');	
		}
		
		/**
		 * Handles the deletion of draft.
		 * 
		 * @return void
		 */
		function delete($draftID = 0){
			require_once('travellog/rules/Class.JournalEntryDraftRules.php');
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			
			$params = array();
			$params['draftID'] = $draftID;
			
			if (JournalEntryDraftRules::validateDeleteDraft($params)) {
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($params['travelerID'], 'TRAVELER');				
				$params['draft']->delete();
				if (JournalEntryDraftRules::GROUP == $params['context']) {
					$drafts_count = JournalEntryDraftPeer::retrieveJournalEntryDraftsCount($params['draft_owner']->getGroupID(), 2);
					$url = '/journal.php?action=groupJournals&gID='.$params['draft_owner']->getGroupID();
				}
				else {
					$drafts_count = JournalEntryDraftPeer::retrieveJournalEntryDraftsCount($params['travelerID'], 1);
					$url = '/journal.php?action=myjournals&travelerID='.$params['travelerID'];
				}
				echo "{'isSuccessful' : true, 'drafts_count' : $drafts_count, 'url': '$url', 'message' : 'Journal entry draft successfully deleted!'}";			
			}
			else {
				echo "{'isSuccessful' : false, 'drafts_count' : 0, 'message' : 'An error occured while trying to delete draft!'}";	
			}
			exit;
		}
		
		/**
		 * The default method called.
		 */
		function index(){
			$this->viewSavedDrafts(0, 'traveler');
		}
		
		/**
		 * Handles viewing of draft form.
		 * 
		 * @param integer $draftID The ID of the draft to be viewed.
		 * 
		 * @return void
		 */
		function viewDraft($draftID = 0){
			require_once('travellog/rules/Class.JournalEntryDraftRules.php');
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			
			$params = array();
			$params['draftID'] = $draftID;
			
			if (JournalEntryDraftRules::validateViewDraft($params)) {
				require_once('travellog/model/Class.Country.php');
				
				$obj_traveler = $this->file_factory->getClass('Traveler', array($params['travelerID']));
				$col_countries = Country::getCountryList();
				$params['obj_map'] = $this->file_factory->getClass('Map'); 
				$factory = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
				$journalOwnerID = $params['travelerID'];
		
				$params['sub_nav'] = $this->file_factory->getClass('SubNavigation');
				$params['sub_nav']->setLinkToHighlight('MY_JOURNALS');
				$params['view_drafts_link'] = "/journal_draft/view_saved_drafts";
				$params['show_auto_save_alert'] = false;	
		
				if (JournalEntryDraftRules::TRAVELER == $params['context']) {
					$params['sub_nav']->setContext('TRAVELER');
					$params['sub_nav']->setContextID($params['travelerID']);
					
					$profile_comp = $factory->create(ProfileCompFactory::$TRAVELER_CONTEXT);
					$profile_comp->init($params['travelerID']);
				}
				else {
					$params['sub_nav']->setContext('GROUP');
					$params['sub_nav']->setContextID($params['draft_owner']->getGroupID());
					
					$profile_comp = $factory->create(ProfileCompFactory::$GROUP_CONTEXT);
					$profile_comp->init($params['draft_owner']->getGroupID());
					
					$journalOwnerID = $params['draft_owner']->getGroupID();
					
					if (0 < $params['draft_owner']->getParentID()) {
						$criteria = new Criteria2();
						$criteria->add(JournalEntryDraftPeer::IS_AUTO_SAVED, 1);
						$criteria->add(JournalEntryDraftPeer::JOURNAL_ENTRY_DRAFT_ID, $params['draftID'], Criteria2::NOT_EQUAL);
						$params['unfinished_entries_count'] = JournalEntryDraftPeer::retrieveDraftEntryCount($params['draft_owner']->getGroupID(), 2, $criteria);
						$params['show_auto_save_alert'] = (0 < $params['unfinished_entries_count']) ? true : false;
						$params['view_drafts_link'] = "/journal_draft/view_saved_drafts/group/$journalOwnerID";
					}
				}
				$params['profile'] = $profile_comp->get_view();
				
				Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
				
				$tpl = $this->file_factory->getClass('Template');
				$tpl->setTemplate('travellog/views/journal_draft/tpl.FrmJournalEntryDraft.php');
				$tpl->set('props', $params);
				$tpl->set('cityID', $params['draft']->getCityID());
				$tpl->set('col_countries', $col_countries);
				$tpl->set('draftID', $params['draftID']);
				$tpl->set('journalOwnerID', $journalOwnerID);
				$tpl->set('back', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
				$tpl->out();				
			}
			else {
				header("Location: /journal.php");
			}
		}
		
		/**
		 * Handles the viewing of saved draft.
		 * 
		 * @param string  $journalID The ID of the journal of whom the drafts will be viewed.
		 * @param string  $context   Can be traveler/group.
		 * @param integer $gID       The ID of the group if the context is group. 
		 * 
		 * @return void
		 */
		function viewSavedDrafts($context = 'traveler', $gID = 0){
			require_once('travellog/rules/Class.JournalEntryDraftRules.php');
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			require_once('Class.Criteria2.php');
			
			$params = array();
			$params['journalID'] = is_numeric($context) ? $context : 0;
			$params['context'] = is_numeric($context) ? 'traveler' : 'group';
			$params['gID'] = (is_numeric($gID)) ? $gID : 0;
			
			if (JournalEntryDraftRules::validateViewSavedDrafts($params)) {
				$profile_factory = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
				$sub_navigation = $this->file_factory->getClass('SubNavigation');
				$sub_navigation->setLinkToHighlight('MY_JOURNALS');
				$criteria = new Criteria2();
				if (0 < $params['journalID']) {
					$criteria->add(JournalEntryDraftPeer::TRAVEL_ID, $params['journalID']);
				}
				
				if (JournalEntryDraftRules::GROUP == $params['context']) {
					$profile_comp = $profile_factory->create(ProfileCompFactory::$GROUP_CONTEXT);
					$profile_comp->init($params['gID']);
					
					$sub_navigation->setContext('GROUP');
					$sub_navigation->setContextID($params['gID']);
					
					$journals = JournalEntryDraftPeer::retrieveJournalsWithDraftsEntry($params['gID'], 2, $criteria);				
				}
				else {
					$profile_comp = $profile_factory->create(ProfileCompFactory::$TRAVELER_CONTEXT);
					$profile_comp->init($params['travelerID']);
					
					$sub_navigation->setContext('TRAVELER');
					$sub_navigation->setContextID($params['travelerID']);
					
					$journals = JournalEntryDraftPeer::retrieveJournalsWithDraftsEntry($params['travelerID'], 1, $criteria);
				}
				
				$tpl = $this->file_factory->getClass('Template');
				Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
				$tpl->setTemplate('travellog/views/journal_draft/tpl.ViewJournalDrafts.php');
				$tpl->set('profile', $profile_comp->get_view());
				$tpl->set('sub_navigation', $sub_navigation);
				$tpl->set('journals', $journals);
				$tpl->set('back', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
				$tpl->out();				
			}
			else {
				header("Location: /journal.php");
			}
		}
		
		/**
		 * Handles saving of a journal entry draft with only the travellogID and the new description as the given data.
		 * 
		 * @return void
		 */
		function saveEntryDraftDescription(){
			require_once('travellog/rules/Class.JournalEntryDraftRules.php');
			require_once('travellog/model/Class.JournalEntryDraft.php');
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			require_once('Class.Criteria2.php');
			
			$params = array();
			if (JournalEntryDraftRules::validateSaveEntryDraftDescription($params)) {
				$criteria = new Criteria2();
				$criteria->add(JournalEntryDraftPeer::TRAVEL_ID, $params['travelID']);
				
				if (0 == $params['journalEntryDraftID'] OR is_null($draft = JournalEntryDraftPeer::retrieveByPk($params['journalEntryDraftID']))) {
					$draft = new JournalEntryDraft();
				}
				
				$draft->initialize($params);
				$draft->save();
				
				echo "{'isSuccessful': true, 'draftID': ".$draft->getJournalEntryDraftID()."}";
			}
			else {
				echo "{'isSuccessful': false, 'draftID': 0}";
			}
			exit;
		}
		
		/**
		 * Handles saving of a journal entry draft
		 * 
		 * @return void
		 */
		function saveEntryDraft(){
			require_once('travellog/rules/Class.JournalEntryDraftRules.php');
			require_once('travellog/model/Class.JournalEntryDraft.php');
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			require_once('Class.Criteria2.php');
			
			$params = array();
			if (JournalEntryDraftRules::validateSavingDraft($params)) {
				$criteria = new Criteria2();
				$criteria->add(JournalEntryDraftPeer::TRAVEL_ID, $params['travelID']);
				
				if (is_null($draft = JournalEntryDraftPeer::retrieveByPk($params['journalEntryDraftID']))) {
					$draft = new JournalEntryDraft();
				}
				
				$params['isAutoSaved'] = (0 == $params['journalEntryDraftID'] OR 0 == $params['isAutoSaved']) ? $params['isAutoSaved'] : $draft->getIsAutoSaved();
				$draft->initialize($params);
				$draft->setTitle(("" == trim($params['title'])) ? 'Unfinished Entry '.(JournalEntryDraftPeer::doCount($criteria) +1) : $params['title']);
				$draft->save();
				
				echo "{'isSuccessful': true, 'message': 'Journal entry draft successfully saved!', 'draftID':".$draft->getJournalEntryDraftID()." }";
			}
			else {
				echo "{'isSuccessful': false, 'message': 'Validation error!'}";
			}
			exit;
		}
		
		function __call($method, $arguments){
			$this->viewSavedDrafts(0, 'traveler');
		}
	}
	
