<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	abstract class AbstractFacilitatorController implements iController{
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,

		$loggedUser  = NULL,

		$isLogged	  = false,
		
		$isSuperAdminLogged = false,
		
		$group 			= NULL,
		
		$subNav		=	NULL,
		
		$tpl			= NULL;
		
		function __construct(){
			
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template' , array( 'path' => '' ));
			
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('FunGroup');
			$this->file_factory->registerClass('AdminGroup');
			
			$this->file_factory->registerClass('SubNavigation');
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
				
		}
		
		function performAction(){
			
			$params = array();
			
			$params['type'] = ( isset($_GET['type']))? $_GET['type'] : 'Facilitator';	
			$params['type'] = ( isset($_POST['type']))? $_POST['type'] : $params['type'];
			
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'Edit';	
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
		
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : 0;		
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : $params['gID'] ;
			
			$params['tID']  = ( isset($_GET['tID']))? $_GET['tID'] : 0;		
			$params['tID']  = ( isset($_POST['tID']))? $_POST['tID'] : $params['tID'] ;
			
			switch($params['mode']){
				
				case 'edit':
				
					$this->_Edit($params);
					break;
				
				case 'save':
					$this->_Save($params);
					break;
				
				case 'removeStaff':
					$this->_RemoveStaff($params);
					break;
					
				case 'removeModerator':
					$this->_RemoveModerator($params);
					break;
					
				default:
					$this->_Edit($params);
			
			}
			
		}
		
		
		function _defineCommonAttributes($_params = array()){
			
			//check if user is currently logged in
			if ($this->obj_session->get('travelerID') > 0) {
				$this->isLogged		=	true ;
				$this->loggedUser = $this->file_factory->getClass ('Traveler',array($this->obj_session->get('travelerID')));
				
			}	
			
			// if groupID is defined, instantiate group and check if logged user is member or adminstrator
			if (0 < $_params['gID']){
				$mGroup = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($_params['gID']));
				$this->group = $mGroup[0];

				$this->isSuperAdminLogged	= 	( $this->group->getAdministratorID() == $this->obj_session->get('travelerID')) ? true : false;
				
				$this->subNav = $this->file_factory->getClass('SubNavigation');
				$this->subNav->setContext('GROUP');
				$this->subNav->setContextID($_params['gID']);
				$this->subNav->setLinkToHighlight('MEMBERS');
				
				(GROUP::ADMIN == $this->group->getDiscriminator() ) ? $this->subNav->setGroupType('ADMIN_GROUP') : $this->subNav->setGroupType('FUN_GROUP');
				
			}
			
			//  create generic template
				$this->tpl = $this->file_factory->getClass('Template');
				$this->tpl->set_path("travellog/views/");
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
				
		}
		
		
		function _allowAdminPrivileges($_privilege = false){
			if (!$_privilege){
				header("location:group.php");
				exit;
			}
		}
		
		
		function _Save($_params = array()){
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges(isset($_POST));	// if post variables are not set, redirect
			
			if ($_params['type'] == 'Facilitator' || $_params['type'] == 'Staff') {
				//manage group staff
				$grpStaffKeyArray = array();
				$currGroupStaffList = $this->group->getStaff(true);
				$grpStaffKeyArray = explode(",",ltrim($_POST['grpStaffKeyList'],","));
				if (isset($_POST['grpStaffKeyList']) && strlen($_POST['grpStaffKeyList'])){
					foreach($grpStaffKeyArray as $eachkey){
						if (!array_key_exists($eachkey,$currGroupStaffList))
							$this->group->addStaff($eachkey);
					}
				}
				foreach($currGroupStaffList as $key => $value){
					if (!in_array($key,$grpStaffKeyArray))
						$this->group->removeStaff($key);
				}
				// end manage group staff
			}
			
		
			header("location:group.php?gID=". $_params['gID'] );
			
		}
		
			
		function _Edit($_params = array()){
			
			$this->_defineCommonAttributes($_params);
			
			// if no groupID is defined; will not allow edit
			if (!(array_key_exists('gID', $_params ) && 0 < $_params['gID'] && $this->group->isSubGroup()))
				$this->_allowAdminPrivileges(false);
			
			$this->_allowAdminPrivileges($this->isSuperAdminLogged);
			
			$allMembers = array();
			foreach($this->group->getMembers() as $eachGroupMember){
				$allMembers[$eachGroupMember->getTravelerID()] = $eachGroupMember->getUserName();
			}
			$this->tpl->set("allMembers", $allMembers);
			
			if ($_params['type'] == 'Facilitator' || $_params['type'] == 'Staff') {
				//prepare group staff
				$selGroupStaff= array();
				foreach($this->group->getStaff() as $eachGroupStaff){
					$selGroupStaff[$eachGroupStaff->getTravelerID()] = $eachGroupStaff->getUserName();
				}
				$this->tpl->set("selGroupStaff", $selGroupStaff );
			}
			
			$this->tpl->set("gID", $_params['gID'] );	
			$this->tpl->set("type", $_params['type'] );	
			$this->tpl->set("subNavigation", $this->subNav);
			$this->tpl->out("tpl.FrmFacilitator.php");
			
		}
		
		function _RemoveModerator($_params = array()){
			
			$this->_defineCommonAttributes($_params);
			
			// if no groupID is defined; will not allow edit
			if (!(array_key_exists('gID', $_params ) && 0 < $_params['gID'] && $this->group->isSubGroup()))
				$this->_allowAdminPrivileges(false);
			
			$this->_allowAdminPrivileges($this->isSuperAdminLogged);
			
			$this->group->removeModerator($_params['tID']);
		
			header("location:group.php?gID=". $_params['gID'] );
			
		}
		
		function _RemoveStaff($_params = array()){
			
			$this->_defineCommonAttributes($_params);
			
			// if no groupID is defined; will not allow edit
			if (!(array_key_exists('gID', $_params ) && 0 < $_params['gID'] && $this->group->isSubGroup()))
				$this->_allowAdminPrivileges(false);
			
			$this->_allowAdminPrivileges($this->isSuperAdminLogged);
			
			$this->group->removeStaff($_params['tID']);
		
			header("location:group.php?gID=". $_params['gID'] );
			
		}
		
	}
	
?>