<?php
	/**
	 * Class.AbstractSessionHandleController.php
	 * created on 06 26, 08
	 */
	 
	require_once("travellog/model/Class.SessionManager.php");
	
	class SessionHandleController{
		
		static $instance = NULL;
		private $session_manager;
		
		private function __construct(){}
		
		static function getInstance(){	
			if( NULL === self::$instance ){
				self::$instance = new SessionHandleController;
			}
			return self::$instance;
		}
		
		public function performAction(){

			$this->session_manager = SessionManager::getInstance();
						
			if( isset($_GET['getLogin']) ){
				if( $this->session_manager->getLogIn() )
					echo 'true';
				else
					echo 'false';	 
			}
		}
		
	} 
?>
