<?php
require_once("travellog/model/Class.SessionManager.php");
class TravelerController{
	
	private $obj_session = NULL;
	
	function __construct(){
		$this->obj_session = SessionManager::getInstance();
	}
	
	/**
	 * @access public
	 * @param Array page|search_by|keywords|type|locationID
	 */
	function getContents($props){
		require_once("travellog/views/Class.MainLayoutView.php");
		require_once("travellog/facade/Class.TravelersFcd.php");
		require_once("travellog/model/Class.HelpText.php");
		require_once("travellog/model/Class.Traveler.php");
		require_once("travellog/model/Class.Map.php");
		
		$obj_view = new MainLayoutView;
		$obj_map  = new Map;
				
		$props['travelerID']    = $this->obj_session->get('travelerID');
		$props['is_login']      = $this->obj_session->getLogin();
		$props['obj_help_text'] = new HelpText;
		if( $props['is_login'] ){
			$props['obj_traveler']  = new Traveler( $props['travelerID'] );
			$props['is_advisor']    = $props['obj_traveler']->isAdvisor();
		}
		else $props['is_advisor'] = false;
		$props['rows']          = 12;
		$props['offset']        = ($props['page']*$props['rows'])-$props['rows'];
		$props['view_type']     = 1; 
		$props['location_type'] = 0;
		$locationID             = $props['locationID'];
		
		ob_start();
			$obj_map->includeJs();
			$google_map_include = ob_get_contents(); 
		ob_end_clean(); 
		
		$code = <<<BOF
		<script type="text/javascript">
		//<![CDATA[
			locationID = $locationID;
		//]]>
		</script>
BOF;
		
		Template::setMainTemplateVar( 'title'          , 'Meet Interesting Travelers on the GoAbroad Network');
		Template::setMainTemplateVar( 'metaDescription', 'Travelers with various interests constantly add their journeys to the Network. Browse the growing travelers list and meet interesting people.');
		Template::setMainTemplateVar( 'layoutID'       , 'travelers/travelers' );
		Template::setMainTemplateVar( 'page_location'  , 'Travelers'           );
		Template::includeDependentJs( "/js/jquery-1.1.4.pack.js"	   	       );
		Template::includeDependent  ( $code                                    );
		Template::includeDependent  ( $google_map_include                      );
		Template::includeDependentJs( "/js/jquery.checkbox.js"	      	       );
		Template::includeDependentJs( "/js/traveler/search_form.js"	           );
		Template::setMainTemplate   ( "travellog/views/tpl.LayoutMain.php"     );
		
		$fcd = new TravelersFcd($props);
		$obj_view->setContents( array('contents' => $fcd->retrieveByContextView()) ); 
		
		$obj_view->render(); 
	}
}
?>