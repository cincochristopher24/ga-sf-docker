<?php
require_once("travellog/controller/Class.AppController.php");
require_once("travellog/model/Class.RowsLimit.php");
require_once("Class.Paging.php");
require_once("Class.ObjectIterator.php");
abstract class AbstractTravelersController extends AppController{
	
	private 
	
	$obj_view = null,
	
	$props    = array();
	
	abstract function retrieveView();
	
	function beforeRender(){
		require_once("travellog/model/Class.HelpText.php");
		require_once("travellog/model/Class.Traveler.php");
		
		$this->props['current_travelerID'] = $this->obj_session->get('travelerID');
		$this->props['obj_help_text']      = new HelpText;
		$this->props['obj_traveler']       = new Traveler( $this->props['current_travelerID'] );
		$this->props['is_login']           = $this->obj_session->get('isLogin');
		$this->props['is_advisor']         = $this->props['obj_traveler']->isAdvisor();   
	
		Template::includeDependentJs("/min/f=js/prototype.js", array("include_here" => true));
		Template::includeDependentJs( "/js/moo2.ajax.js"                   );
		Template::includeDependentJs( "/js/utils/json.js"                  );
		Template::includeDependentJs( "/js/countryLoader.js"               );
		Template::setMainTemplate   ( "travellog/views/tpl.LayoutMain.php" );
	}
	
	function retrieveTravelersSearchFormView( &$props ){
		require_once("travellog/views/travelers/Class.TravelersSearchFormView.php");
		$obj_view = new TravelersSearchFormView;
		$obj_view->setContents( &$props );
		
		return $obj_view; 
	}
	
	function retrieveTravelersMapView( &$props ){
		require_once("travellog/views/travelers/Class.TravelersMapView.php");
		$obj_view = new TravelersMapView;
		$obj_view->setContents( $props );
		
		return $obj_view;
	}
	
	function retrieveTravelersListView( &$props ){
		require_once("travellog/views/travelers/Class.TravelersListsView.php");
		if( !array_key_exists( 'col_travelers', $props ) ) $this->retrieve( $props );
		
		$obj_view = new TravelersListsView;
		
		$obj_view->setContents( $props );
		
		return $obj_view; 
	}
	
	protected function afterFilter(){
		$this->obj_view->addSubView( $this->retrieveTravelersSearchFormView( $this->props ), 'TRAVELERS_SEARCH_FORM_VIEW' );
		$this->obj_view->addSubView( $this->retrieveTravelersMapView( $this->props )       , 'TRAVELERS_MAP_VIEW'         );
		$this->obj_view->addSubView( $this->retrieveTravelersListsView( $this->props )     , 'TRAVELERS_LISTS_VIEW'       );
	}
}
?>
