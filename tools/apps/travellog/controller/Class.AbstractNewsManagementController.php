<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	abstract class AbstractNewsManagementController
	{
		protected $obj_session = NULL;
		protected $file_factory = NULL;
		protected $pageTitle = NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			//redirect to index since this page will not be used anymore.
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Template',array('path'=>''));
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		}
		
		public function performAction($action='onloadView'){
			$this->$action();
		}
		
		protected function onloadView(){
			//$travelerID = (isset($_SESSION) && array_key_exists('travelerID',$_SESSION) && $_SESSION['travelerID'] !=0 ? $_SESSION['travelerID']:0 );
			$travelerID = $this->obj_session->get('travelerID');
			$localGroupID = $this->setLocalVar('groupID,hidGroupID',0,'NUMERIC');
			if(!$localGroupID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			try{
				$localAdminGroup = $this->file_factory->getClass('AdminGroup',array($localGroupID));
			}
			catch(exception $e){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			if(!strlen($this->pageTitle)){
				$this->pageTitle = $localAdminGroup->getName().' News';
			}
			
			$localIsGroupMember = 0;
			$localIsAdmin 		= 0;
			$localMethod 		= $this->setLocalVar("method,hidMethod","view");	
			$localHlEntryID 	= $this->setLocalVar('hidHlEntryID,hlEntryID',0,"NUMERIC");
			$localPage 			= $this->setLocalVar("page,hidpage",1,"NUMERIC");	
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$localTemplate 		= $this->file_factory->getClass('Template');
			
			if($travelerID != 0){
				//check if the logged traveler is a member of the group		
				$localTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
				$localIsGroupMember = ($localAdminGroup->isMember($localTraveler))?"true":"false";		
				//check if the logged traveler is the administrator of the group
				$localIsAdmin = (
					$travelerID==$localAdminGroup->getAdministrator()->getTravelerID()
					|| $localAdminGroup->isStaff($travelerID) 
					|| $localAdminGroup->isModerator($travelerID) 
				);					
			}
			else{
				$localIsAdmin = false;	
			}
			
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');	
			$subNavigation->setContextID($localGroupID);
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight('NEWS');	
			//$subNavigation->setDoNotShow(!$localIsAdmin);
			
			$localTemplate->set("subNavigation",$subNavigation);
				
			$mode = ($localIsAdmin)?'READ_WRITE':'READ_ONLY';	
			$localTemplate->set_path("travellog/views/");
			$localTemplate->set("method",$localMethod);
			$localTemplate->set("groupID",$localGroupID);
			$localTemplate->set("isAdmin",$localIsAdmin);
			$localTemplate->set("hlEntryID",$localHlEntryID);
			$localTemplate->set("isGroupMember",$localIsGroupMember);
			$localTemplate->set("mode",$mode);
			$localTemplate->set("itemsPerPage",10);
			$localTemplate->set("pageNavPerPage",5);
			$localTemplate->set("pageTitle",$this->pageTitle);
			$localTemplate->out("tpl.NewsManagement.php");
			
		}
		
		protected function setLocalVar($listVarNames,$defVal,$varType="STRING"){
			//explode the varname		
			$arVarNames = explode(",",$listVarNames);		
			foreach($arVarNames as $varName){			
				if(isset($_GET[$varName]) || isset($_POST[$varName])){			
					$localVar = (isset($_GET[$varName]))?$_GET[$varName]:$_POST[$varName];			
					switch($varType){
						case "NUMERIC":										
							if(is_numeric($localVar)) return $localVar;
							break;
						default://STRING
							return $localVar;
					} 			
				}
			}
			return $defVal;
		}
					
	}
	
	
?>