<?php 
require_once("Class.ToolMan.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");
//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
abstract class AbstractEntryController implements IController{
	
	protected static $REQUIRES_ACTION_RULES = array('ADD', 'SAVE', 'EDIT', 'DELETE', 'PUBLISHUNPUBLISH', 'VIEW');
	
	protected 
	
	$data         = array(),
	
	$obj_session  = NULL,
	
	$file_factory = NULL;
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance(); 
		
		$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('LocationFactory');
		$this->file_factory->registerClass('CommentsView' , array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('TravelLog');
		$this->file_factory->registerClass('EmailNotification');
		$this->file_factory->registerClass('NewCity');
		$this->file_factory->registerClass('Coordinates');
		$this->file_factory->registerClass('JournalLog');
		$this->file_factory->registerClass('MailImplController'  , array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('JournalEventHandler'  , array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('gaSubject'           , array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('gaAbstractEvent'     , array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('gaActionType'        , array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('JournalManagementFcd', array('path' => 'travellog/facade/'));
		$this->file_factory->registerClass('MainLayoutView'      , array('path' => 'travellog/views/'));
		
		$this->file_factory->registerClass('Travel');
		$this->file_factory->registerClass('Trip');
		$this->file_factory->registerClass('Traveler'); 
		$this->file_factory->registerClass('Map');
				
		$this->file_factory->registerClass('BackLink'               , array('path' => 'travellog/model/navigator/'));
		
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('Template'               , array('path' => ''));
		$this->file_factory->registerClass('GaDateTime'               , array('path' => ''));
		$this->file_factory->registerClass('StringFormattingHelper' , array('path' => ''));
		$this->file_factory->registerClass('HtmlHelpers'            , array('path' => ''));
		$this->file_factory->registerClass('Criteria2'               , array('path' => ''));
		$this->file_factory->registerClass('GroupFactory');
				
		
		$this->file_factory->registerTemplate('FrmTravelLog');
		$this->file_factory->registerTemplate('TravelLog');
		
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->registerClass('HelpText');
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');  
	}
	
	function performAction(){
		$this->beforePerformAction();
		switch( strtolower($this->data['action']) ){
			case "edit":
			case "add":
				$this->__getData();
				$this->_viewAddForm();
			break;
			
			case "addcity":
				$this->_saveCity();
			break;
			
			// changed from create entry to publish entry -  ianne 10/09/09
			case "publish entry":
			case "save":
				$this->__validateForm();
				$this->_save();
			break;
			
			case "publishunpublish":
				$this->_publishUnpublish();
			break;
			
			case "loadcities":
				$this->_getCities();
			break;
			
			case "load_cities":
				$this->_getCities();
			break;
				
			case "delete":
				$this->_delete();
			break;
			
			case "view":
				$this->_viewEntry();
			break;
			
			default: 
				Toolman::redirect('/journal.php'); exit;
		}
	}
	
	protected function loadCities(){
		$html = '';
		try{
			$obj_factory     = $this->file_factory->invokeStaticClass('LocationFactory'); 
			$obj_location    = $obj_factory->create($_GET['countryID']);
			$col_cities      = $obj_location->getCities($this->obj_session->get('travelerID'));
			if( count($col_cities) ){
				foreach( $col_cities as $obj_city ){
					$selected = ( $obj_city->getLocationID() == $_GET['cityID'] )? 'selected=true' : '';
					$html    .= '<option value="'.$obj_city->getLocationID().'" '.$selected.'>'.$obj_city->getName().'</option>';
				}	
			}
		}catch(Exception $e){}
		
		$type = ($_GET['countryID'] == 7758)	?	"State"	:	"City";
		$selected = ( 0 == $_GET['cityID'] )? 'selected=true' : '';
		$html    .= '<option value="0" '.$selected.'>Choose a '.$type.'</option>'; 
		$html    .= '<option value="">-Other City-</option>';    
		echo $html . '|' . $_GET['cityID']; 		
	}
	
	protected function _saveCity(){
		$txtLat  	             = $this->data['lat'];
		$txtLong 	             = $this->data['long'];
		$locationID              = $this->data['locID'];
		$txtCity 	             = $this->data['cityName'];
		$this->data['countryID'] = $this->data['locID'];
		
		$location = $this->file_factory->invokeStaticClass('LocationFactory');// LocationFactory::instance(); 
		$Country  = $location-> create($locationID);

		$City = $this->file_factory->getClass('NewCity'); //new NewCity();
		$City-> setCountryID($Country->getCountryID());
		$City-> setTravelerID( $this->obj_session->get('travelerID') );
		$City-> setName($txtCity);
		$City-> Save();
		$this->data['cityID'] = $City->getLocationID(); 
		 
		$Coords = $this->file_factory->getClass('Coordinates', array($City->getLocationID())); //new Coordinates($City->getLocationID());
		$Coords-> setX($txtLong);
		$Coords-> setY($txtLat);
		$Coords-> save(); 

		$path = "http://" . $_SERVER['SERVER_NAME'] . "/admin/approveCity.php?countryLocID=" . $locationID;
		$EmailNotice = $this->file_factory->getClass('EmailNotification', array(3)); //new EmailNotification(3);
		$EmailNotice-> setTo('admin@goabroad.net');
		$EmailNotice-> setToName('GoAbroad.net Administrator');
		$EmailNotice-> setFrom('admin@goabroad.net');
		$EmailNotice-> setFromName('GoAbroad.net');
		$EmailNotice-> setSubject('New city added in GoAbroad.Net');
		$EmailNotice-> setMessage('<br /> Hi! <br /> <br /> A new city, <strong>' . $txtCity . '</strong> was added to the country <strong>' . 
				$Country-> getName() . '</strong> through the <strong>Add City Tool</strong>. Please confirm if this is a valid city by ' .
				'clicking on the link below: <br /> <br /> <a href = "'.$path.'"> Approve New City </a> <br /> <br /> <br /> <br /> Thanks, ' .
				'<br /> <strong>The GoAbroad Tech Team</strong>');
		$EmailNotice-> Send();
		
		 
		$this->_getCities();
	}
	
	protected function _viewEntry(){
		
		
		$fcd          = $this->file_factory->getClass('JournalManagementFcd');  
		$obj_map      = $this->file_factory->getClass('Map');
		$obj_view     = $this->file_factory->getClass('MainLayoutView');
		$arr_entries  = array();
		$obj_journal  = $fcd->retrieve( $this->data['travelID'] );
		$photo_layout = 1;

		if( $this->data['travellogID'] == 0 ){
			$arr_entries = $fcd->getEntries( $this->data );
			if( count($arr_entries) ){ 
				$this->data['travellogID'] = $arr_entries[0]->getTravelLogID();
				$obj_entry                 = $arr_entries[0];
			}
		}
		else{
			$obj_entry   = $fcd->retrieveEntry( $this->data['travellogID'] );
			$arr_entries = $fcd->getEntries( $this->data );   
		}
		
		$pos          = $fcd->getCurrentPosition();
		$isAdmin      = ( $fcd->showAdminControls( $this->data ) )?1: 0;
		$travelerID   = $this->data['travelerID'];
		
		$obj_travellog = $this->file_factory->getClass('TravelLog', array($this->data['travellogID'])); 
		
		$entry_owner = NULL;
		try{
			$entry_owner   = $obj_travellog->getOwner();
		}catch(exception $e){
			header("location: /FileNotFound.php");
			exit;
		}
		
		if(get_class($entry_owner)=='Traveler' && ($entry_owner->isSuspended()||$entry_owner->isDeactivated())){
			header("location: /FileNotFound.php");
			exit;
		}
		
		if( get_class($entry_owner) != 'Traveler' ){
			$this->data['group_id'] = $entry_owner->getGroupID();
		}
				
		$travellogID  = $this->data['travellogID'];
		
		if( isset($obj_entry) ) $photo_layout = ( $obj_entry->getTemplateType()== 0)? 4: 1;
		
		ob_start();
			$obj_map->includeJs();
			$google_map_include = ob_get_contents(); 
		ob_end_clean();  
		
		$obj_travel = $this->file_factory->getClass('Travel', array($this->data['travelID'])); 
		if( !$isAdmin ){
			//$obj_travel = $this->file_factory->getClass('Travel', array($this->data['travelID']));
			$obj_travel->addView();
		}
		
	if( $isAdmin ){
		
		$code = <<<BOF
<script type="text/javascript">   
tinyMCE_GZ.init({
	plugins : 'style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,'+ 
        'searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras',
	themes : 'simple,advanced',
	languages : 'en',
	disk_cache : true,
	debug : false
});
</script>
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();
	travellogID = $travellogID;
	isAdmin     = $isAdmin; 
	currPos     = $pos;	
	is_tinymce  = 0;
	
	EntryManager.travellogID = travellogID;
	
	jQuery(document).ready(function(){
		var popUIEntry = new PopUI(); 
		popUIEntry.set_url                ( '/ajaxpages/journal_entry_page.php' );	   
		popUIEntry.set_form_label_elements( ['contentLabel']                    );
		popUIEntry.set_form_elements      ( ['content','TEXTAREA']              );
		//popUIEntry.set_cancel_element     ( 'cancel'                            ); 
		
		
		jQuery('#JournalDescriptionLink').click(function(){ 
			popUIEntry.set_form_label_values  ( ['Journal Description']                       );
			popUIEntry.set_hidden_field_value ( 'JOURNALDESCRIPTION'                          );
			popUIEntry.set_height             ( ['300px']                                     );
			// popUIEntry.set_uses_tinymce       ( true                                          );
			popUIEntry.set_querystring        ( 'tID='+travellogID+'&type=JOURNALDESCRIPTION' );
			
			popUIEntry.set_before_save_validation ( 
				function(field){ 
					if(!(jQuery.trim(field.replace(/(<p>(\&nbsp;)+<\/p>)/g,""))).length){
						alert('Invalid description. It must not be empty!');
						return false;
					}
					return true;
				}
			);
			popUIEntry.set_character_count_callback( function(){jQuery('.charcount').wordCount(150); jQuery('.charcount').keyup();}, "word" );
			popUIEntry.render();
		});
		
		jQuery('#JournalTitleLink').click(function(){
			popUIEntry.set_form_label_values  ( ['Journal Title']                       );
			popUIEntry.set_hidden_field_value ( 'JOURNALTITLE'                          );
			popUIEntry.set_uses_tinymce       ( false                                   );
			popUIEntry.set_querystring        ( 'tID='+travellogID+'&type=JOURNALTITLE' );
			popUIEntry.set_height             ( ['28px']                                );			
			popUIEntry.set_before_save_validation ( 
				function(field){ 
					if(!(jQuery.trim(field)).length){
						alert('Invalid title. It must not be empty!');
						return false;
					}
					return true;
				}
			);		
			popUIEntry.set_character_count_callback( function(){jQuery('.charcount').characterCount(250); jQuery('.charcount').keyup();}, "character" );
			popUIEntry.render();
		});
		
		jQuery('#EntryTitleLink').click(function(){
			popUIEntry.set_form_label_values  ( ['Entry Title']                       );
			popUIEntry.set_hidden_field_value ( 'ENTRYTITLE'                          );
			popUIEntry.set_uses_tinymce       ( false                                 );
			popUIEntry.set_querystring        ( 'tID='+travellogID+'&type=ENTRYTITLE' );
			popUIEntry.set_height             ( ['28px']                              );
			popUIEntry.set_on_save_callback   ( function(field){window.location.href = '/journal-entry.php?action=view&travellogID='+travellogID;} );
			popUIEntry.set_character_count_callback( function(){jQuery('.charcount').characterCount(250); jQuery('.charcount').keyup();}, "character" );
			
			popUIEntry.set_before_save_validation ( 
				function(field){
					var duplicate = false;
					if(!(jQuery.trim(field)).length){
						alert('Invalid title. It must not be empty!');
						return false;
					}
					jQuery.ajax({
						type      : 'POST',
						data      : 'action=validateEntryTitle&title='+jQuery('#content').val()+'&'+this.querystring,
						url       : '/ajaxpages/journal_entry_page.php',
						async	  : false,
						success   : function(response){
							if(response == "true"){
								alert("Duplicate entry title for this journal!");
								duplicate = true;
							}
						}
					});
					
					if(duplicate)
						return false;
					return true;
				}
			);
			
			popUIEntry.render();
		});

		jQuery('#EntryCalloutLink').click(function(){
			popUIEntry.set_form_label_values  ( ['Entry Callout']                       );
			popUIEntry.set_hidden_field_value ( 'ENTRYCALLOUT'                          );
			popUIEntry.set_uses_tinymce       ( false                                   );
			popUIEntry.set_querystring        ( 'tID='+travellogID+'&type=ENTRYCALLOUT' );
			popUIEntry.set_height             ( ['28px']                                );
			popUIEntry.set_character_count_callback( function(){jQuery('.charcount').characterCount(250); jQuery('.charcount').keyup();}, "character" );
			popUIEntry.render();
		});
		
		jQuery('#EntryDescriptionLink').click(function(){ 
			popUIEntry.set_form_label_values  ( ['Entry Content']                           );
			popUIEntry.set_hidden_field_value ( 'ENTRYDESCRIPTION'                          );
			popUIEntry.set_uses_tinymce       ( true                                        );
			popUIEntry.set_tinymce_custom_cleanup( 'custom_callback' );
			popUIEntry.set_querystring        ('tID='+travellogID+'&type=ENTRYDESCRIPTION');
			popUIEntry.set_height             ( ['300px']                                   );
			
			popUIEntry.set_before_save_validation ( 
				function(field){
					if(!(jQuery.trim(field.replace(/(<p>(\&nbsp;)+<\/p>)/g,"")).length)){
						alert('Invalid title. It must not be empty!');
						return false;
					}
					EntryManager.deleteDraft();
					return true;
				}
			);
			
			popUIEntry.set_character_count_callback( null, null );
			
			popUIEntry.render();
			EntryManager.startAutoSaving();
			setTimeout(function(){
				jQuery('#save').unbind('click');
				jQuery('#save').bind('click', function(){
					EntryManager.stopAutoSaving();
					jQuery('#save').unbind('click');
					if(popUIEntry._save()){
						popUIEntry._clear(); 
					}			
				});
				jQuery('#cancel').unbind('click');
				jQuery('#cancel').bind('click', function(){
					EntryManager.stopAutoSaving();
					popUIEntry._clear();
					jQuery('#cancel').unbind('click');				
				});			
			}, 10);
		}); 
		
		
		var popUITips = new PopUI(); 
		popUITips.set_url                ( '/ajaxpages/journal_entry_tips.php' );	   
		popUITips.set_uses_tinymce       ( true                                );
		popUITips.set_form_label_elements( ['contentLabel']                    );
		popUITips.set_form_elements      ( ['content','TEXTAREA']              );
		popUITips.set_hidden_field_value ( 'ENTRYTIPSLIST'                     );
		
		
		var popUIHighlights = new PopUI(); 
		popUIHighlights.set_url                ( '/ajaxpages/journal_entry_highlights.php' );	   
		popUIHighlights.set_uses_tinymce       ( true                                      );
		popUIHighlights.set_form_label_elements( ['contentLabel']                          );
		popUIHighlights.set_form_elements      ( ['content','TEXTAREA']                    );
		popUIHighlights.set_hidden_field_value ( 'ENTRYHIGHLIGHTSLIST'                     ); 
		
		
		jQuery('#EntryTipsLink').click(function(){ 
			popUITips.set_form_label_values( ['Entry Tips']             ); 
			popUITips.set_mode             ( 'ADD'                      ); 
			popUITips.set_action           ( 'save'                     );
			popUITips.set_querystring      ( 'travellogID='+travellogID ); 
			popUITips.set_on_save_callback( function(field, content){ jQuery('#TipsHelpText').hide(); } );
			popUITips.render();
			
		});
		
		jQuery('#EntryHighlightsLink').click(function(){ 
			popUIHighlights.set_form_label_values( ['Entry Highlights'] ); 
			popUIHighlights.set_mode             ( 'ADD'                ); 
			popUIHighlights.set_action           ( 'save'               );
			popUIHighlights.set_querystring      ( 'tID='+travellogID   );
			popUIHighlights.set_on_save_callback( function(field, content){ jQuery('#HighlightsHelpText').hide(); } ); 
			popUIHighlights.set_help_text        ( '<p>You can add as many travel highlights to your journal as you want. After entering a highlight in the text editor, click <strong>Save</strong>. Repeat the process to add another highlight.</p>');
			popUIHighlights.render();
		});
		
		jQuery.extend({
			
			edit_tip: function(id){
				popUITips.set_form_label_values( ['Entry Tips']                        ); 
				popUITips.set_mode             ( 'EDIT'                                ); 
				popUITips.set_action           ( 'update'                              );
				popUITips.set_querystring      ( 'tID='+id+'&travellogID='+travellogID );
				popUITips.render();
			},
			
			remove_tip: function(id){
				popUITips.set_querystring( 'tID='+id+'&travellogID='+travellogID );
				popUITips.set_after_delete_callback( function(len){ if( len == 0 ){ jQuery('#TipsHelpText').show(); } } );
				popUITips.remove_data(id, 'Are you sure you want to remove this travel tip?');
			},
			
			edit_highlight: function(id){
				popUIHighlights.set_form_label_values( ['Entry Highlights']          );  
				popUIHighlights.set_mode             ( 'EDIT'                        ); 
				popUIHighlights.set_action           ( 'update'                      );
				popUIHighlights.set_querystring      ( 'hID='+id+'&tID='+travellogID );
				popUIHighlights.render();
			},
			
			remove_highlight: function(id){
				popUIHighlights.set_querystring( 'hID='+id+'&tID='+travellogID );
				popUIHighlights.set_after_delete_callback( function(len){ if( len == 0 ){ jQuery('#HighlightsHelpText').show(); } } );
				popUIHighlights.remove_data(id, 'Are you sure you want to remove this travel highlights?');
			}
		});
	});
	
	function custom_callback(type, value){
		switch (type) {
			case "get_from_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s|[<span>])*<\/p>)/gi,'');
				break;
			case "insert_to_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s|[<span>])*<\/p>)/gi,'');
				break;
			case "submit_content":
				break;
			case "get_from_editor_dom":
				break;
			case "insert_to_editor_dom":
				break;
			case "setup_content_dom":
				break;
			case "submit_content_dom":
				break;
		}

		return value;
	}

	jQuery(document).ready(function(){  
		initmb();
	});
	
//]]>  
</script>
BOF;
	}
	elseif( $this->obj_session->getLogin() ){
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();
	travellogID = $travellogID;
	isAdmin     = $isAdmin; 
	currPos     = $pos;
	
//]]>  
</script>
BOF;
	}
	elseif( isset($photo_layout) ){
	$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();
	travellogID = $travellogID;
	isAdmin     = $isAdmin; 
	currPos     = $pos;
	
//]]>  
</script>
BOF;
	}	
	else{
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();
	travellogID = $travellogID;
	isAdmin     = $isAdmin; 
	currPos     = $pos;
//]]>  
</script>
BOF;
	}
		$city = (strtolower($obj_entry->getCity()) != strtolower($obj_entry->getCountry())) ? $obj_entry->getCity().", " : "";
		
		/* Modified the location css declaration > programmers try to include the css first before declaring your js, so far i manage to moderately fix this problem hopefully i can find a better solution right now this is the best we can do*/				

		/* Template::includeDependentCss( "/min/b=css&amp;f=video.css,styles.css"); */
		Template::includeDependentCss( "/min/g=AbstractEntryController");
		
		if( isset($obj_entry) ){
			$tmpJournalEntryTitlePhrase = $obj_entry->getTitle(). ' - '. $obj_journal->getTitle() .' - '.$city. $obj_entry->getCountry() . ' - GoAbroad Network';
			$tmpMetaDescription = $obj_entry->getDescription();
		}
		else{
			$tmpJournalEntryTitlePhrase = ' - GoAbroad Network';
			$tmpMetaDescription = $obj_journal->getDescription();
		}

		// get first paragraph of description and set it as meta description
		$dom = new DOMDocument;
	    @$dom->loadHTML(trim($tmpMetaDescription)); 
	    $dom->preserveWhiteSpace = false; 
		$ps = $dom->getElementsByTagName('p');
		
		$tmpMetaDescription = strip_tags($tmpMetaDescription);
		$metaDescription = substr($tmpMetaDescription, 0, 140);
		$metaDescription = strlen($tmpMetaDescription) > 140 ? $metaDescription.'...' : $metaDescription;
		$metaDescription = htmlspecialchars($metaDescription, ENT_QUOTES);
		
		$metaDescription = $ps ? htmlentities($ps->item(0)->nodeValue, ENT_QUOTES) : $metaDescription;
		
		
		Template::setMainTemplateVar ( 'title', strip_tags($tmpJournalEntryTitlePhrase) );
		Template::setMainTemplateVar ( 'metaDescription', strip_tags($metaDescription)              );	
		Template::setMainTemplate    ( $this->file_factory->getTemplate('LayoutMain')   );

		Template::includeDependent   ( $google_map_include,array("bottom"=>true));
		
		if( $this->obj_session->getLogin() ){	
			Template::includeDependentJs ( "/min/g=JournalEntryLoginJs"   , array('bottom'=> true));		
			//Template::includeDependentJs ( "/js/addnewcity.js", array('bottom'=> true));
			//Template::includeDependentJs ( "/js/modaldbox.js"   , array('bottom'=> true));
		}
		if( $isAdmin ){ 
			Template::includeDependentJs ( "/min/g=JournalEntryAdminJs" , array('bottom'=> true));
			Template::includeDependentJs ( "/js/tiny_mce/tiny_mce_gzip.js" , array('bottom'=> true));
			//Template::includeDependentJs ( "/min/f=js/jquery.characterCount.js" , array('bottom'=> true));
			//Template::includeDependentJs ( "/js/popUI.js" , array('bottom'=> true));
		}

		// set robots to noindex for travel journal entries used in gacom articles. 2014-06-23
		/*$noIndexEntries = array(26329, 27152, 27626, 29175, 29296, 29411, 29803, 29849, 29901);
		if( in_array($travellogID, $noIndexEntries) ){
			Template::setMainTemplateVar ( 'robots', 'noindex, follow' );
		}*/
		
		/***********************************************
		 * includes the js and css files for video in this template
		 * added by neri
		 * 10-14-08
		 ***********************************************/
		//Template::includeDependentJs('/js/video.js', array('bottom'=> true)); > transfer to /min/groupsConfig.php - JournalMapJs
		/***********************************************/
		
		//if( $isAdmin ){
		//	Template::includeDependentJs (
		//		array(
		//			"/js/date.js",
		//			"/js/jquery.datePicker.js",
		//			"/js/jquery.checkbox.js",
		//			"/js/entry_location.js",
		//			'/js/entryManager-1.0.js'
		//		), array('bottom'=> TRUE)
		//	);
		//}
		Template::includeDependent   ( $code                                , array('bottom'=> true));
		
		if( isset($obj_entry) && $obj_entry->getTemplateType() == 0 )  
			Template::setMainTemplateVar('layoutID', 'journals');
		else
			Template::setMainTemplateVar('layoutID', 'journals');
		
		$pageLocation = 'Traveler' != get_class($entry_owner) ? 'Groups / Clubs'  : 'Travelers';
		if( 'Traveler' == get_class($entry_owner) && $this->obj_session->get('travelerID') == $entry_owner->getTravelerID()){
			$pageLocation = 'My Passport';
		}elseif( 'Traveler' != get_class($entry_owner) && isset($GLOBALS["CONFIG"]) ){
			if( $obj_travel->isGroupJournal() && 0 < $this->obj_session->get('travelerID') ){
				$pageLocation = 'Home';
			}else{
				$pageLocation = 'Journals';
			}
		}
		Template::setMainTemplateVar ( 'page_location', $pageLocation ); 
		
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentJs("http://apis.google.com/js/plusone.js");
		
		$arr_contents['contents'] = $fcd->retrieveEntryByFilterView( $this->data );
		
		$obj_view->setContents( $arr_contents );
		
		require_once("travellog/UIComponent/Photo/facade/PhotoUIFacade.php");
		$Photo = new PhotoUIFacade();
		$Photo->setContext('travellog');
		$Photo->setGenID($travellogID);
		$Photo->setLoginID(0);
		$Photo->build();
		
		echo $obj_view->render();
	} 
	
	protected function _getCities(){
		$html = '';
		try{
			$obj_factory     = $this->file_factory->invokeStaticClass('LocationFactory'); 
			$obj_location    = $obj_factory->create($this->data['countryID']);
			$col_cities      = $obj_location->getCities($this->obj_session->get('travelerID'));
			if( count($col_cities) ){
				foreach( $col_cities as $obj_city ){
					$selected = ( $obj_city->getLocationID() == $this->data['cityID'] )? 'selected=true' : '';
					$html    .= '<option value="'.$obj_city->getLocationID().'" '.$selected.'>'.$obj_city->getName().'</option>';
				}	
			}
		}catch(Exception $e){
			
		}
		$type = ($this->data['countryID'] == 7758)	?	"State"	:	"City";
		$selected = ( 0 == $this->data['cityID'] )? 'selected=true' : '';
		$html    .= '<option value="0" '.$selected.'>Choose a '.$type.'</option>'; 
		$html    .= '<option value="">-Other City-</option>';    
		echo $html . '|' . $this->data['cityID'];    
	}
	
	protected function _viewAddForm(){
		require_once('travellog/model/Class.JournalEntryDraftPeer.php');
		require_once('Class.Criteria2.php');
		
		$travelerID = $this->obj_session->get('travelerID');
		$obj_traveler = $this->file_factory->getClass('Traveler', array($travelerID));
		$this->data['is_advisor'] = $obj_traveler->isAdvisor();
		$this->data['is_administrator'] = $obj_traveler->isAdministrator();
		$this->data['isAdmin'] = $this->obj_session->isSiteAdmin();
		$this->data['show_auto_save_alert'] = false;
		$col_countries = Country::getCountryList();
		$obj_map = $this->file_factory->getClass('Map'); 
		$factory = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
		$obj_journal = $this->file_factory->getClass('Travel', array($this->data['travelID']));
		$journalOwner = $obj_journal->getOwner();
		$journalOwnerID = $this->obj_session->get('travelerID');
		
		$obj_navigation = $this->file_factory->getClass('SubNavigation');
		$obj_navigation->setLinkToHighlight('MY_JOURNALS');	
		
		if($journalOwner instanceof Traveler){
			$obj_navigation->setContext('TRAVELER');
			$obj_navigation->setContextID($travelerID);
			
			$profile_comp = $factory->create(ProfileCompFactory::$TRAVELER_CONTEXT);
			$profile_comp->init($travelerID);
			$this->data["isGroupJournal"] = FALSE;
		}
		else{
			$obj_navigation->setContext('GROUP');
			$obj_navigation->setContextID($journalOwner->getGroupID());
			
			$profile_comp = $factory->create(ProfileCompFactory::$GROUP_CONTEXT);
			$profile_comp->init($journalOwner->getGroupID());
			
			$journalOwnerID = $journalOwner->getGroupID();
			
			if (0 < $journalOwner->getParentID()) {
				$criteria = new Criteria2();
				$criteria->add(JournalEntryDraftPeer::IS_AUTO_SAVED, 1);
				$this->data['unfinished_entries_count'] = JournalEntryDraftPeer::retrieveDraftEntryCount($journalOwnerID, 2, $criteria);
				$this->data['show_auto_save_alert'] = (0 < $this->data['unfinished_entries_count']) ? true : false;
				$this->data['view_drafts_link'] = "/journal_draft/view_saved_drafts/group/$journalOwnerID";
			}
			$this->data["isGroupJournal"] = TRUE;
		}
		
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		
		require_once('travellog/helper/Class.AutoApproveFeatureJournalHelper.php');
		$this->data['groupIDsCount'] = AutoApproveFeatureJournalHelper::countAutoModeGroups($travelerID, $this->data['travelID']);
		
		$this->data['groupIDStr'] = isset($this->data['groupIDStr']) ? $this->data['groupIDStr'] : '';
		$this->data['notGroupIDStr'] = isset($this->data['notGroupIDStr']) ? $this->data['notGroupIDStr'] : '';
		$this->data['travelerID'] = isset($this->data['travelerID']) ? $this->data['travelerID'] : $travelerID;
		
		$tpl = $this->file_factory->getClass('Template');
		$tpl->set('sub_navigation', $obj_navigation);
		$tpl->set('props', $this->data);
		$tpl->set('cityID', $this->data['cityID']);
		$tpl->set('col_countries', $col_countries);
		$tpl->set('profile', $profile_comp->get_view());
		$tpl->set('journalOwnerID', $journalOwnerID);
		$tpl->set('obj_map', $obj_map);
		$tpl->out($this->file_factory->getTemplate('FrmTravelLog'));
	}
	
	protected function _save(){
	//	var_dump($this->data); exit;
		$locationID = ( $this->data['cityID'] )? $this->data['cityID']: $this->data['countryID'];

		$this->data['description'] = preg_replace("#<style[^>]*?>(.|\n)*?</style>#","",$this->data['description']);
		$this->data['description'] = preg_replace("#&lt;(.|\n)*?&gt;#","",strip_tags($this->data['description'],'<p><b><strong><i><em><u><span>'));
		// in cases where js is disabled or the arrival date is empty, we set arrival date to current date
		$this->data['arrival'] = (empty($this->data['arrival'])) ? date('Y-m-d') : $this->data['arrival'];
		if( $this->data['travellogID'] ){
			
			$obj_entry  = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
			
			$obj_trip = $this->file_factory->getClass('Trip', array($obj_entry->getTripID()));
			$tripID   = Trip::TripExists( $this->data['travelID'], $locationID, $this->data['arrival']);
			
			if( $tripID == 0 ){
				$obj_trip->setLocationID( $locationID             );
				$obj_trip->setArrival   ( $this->data['arrival']  ); 
				if( count($obj_trip->getTravelLogs()) > 1 )
					$obj_trip->Create();
				else{
					$obj_trip->setTripID($obj_entry->getTripID());
					$obj_trip->Update();
				}
				$tripID = $obj_trip->getTripID();	
			}  
			else{
				$obj_trip->setLocationID( $locationID             );
				$obj_trip->setTripID    ( $obj_entry->getTripID() );
				$obj_trip->setArrival   ( $this->data['arrival']  ); 
				$obj_trip->Update();
				$tripID = $obj_entry->getTripID();	
			}
			
			$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
			
			if($this->data['current_location'])
				$obj_traveler->setPresentLocation( $locationID );
			elseif( $obj_traveler->getPresentLocation() != NULL ){ 
				if( $obj_traveler->getPresentLocation()->getLocationID() == $locationID ) $obj_traveler->setPresentLocation(0);
			}	
			
			$obj_entry->setTitle       ( str_replace('/', '', $this->data['title']) );
			$obj_entry->setTripID      ( $tripID                                    );
			$obj_entry->setDescription ( $this->data['description']                 );
			$obj_entry->setPublish     ( $this->data['publish']                     );
			$obj_entry->setCallout     ( $this->data['callout']                     ); 
			$obj_entry->setTemplateType( $this->data['template_theme']              );
			$obj_entry->Update();
		}	
		else{
			$this->file_factory->getClass('gaActionType');
			$obj_entry  = $this->file_factory->getClass('TravelLog');
			$obj_trip   = $this->file_factory->getClass('Trip');
			$tripID     = Trip::TripExists( $this->data['travelID'], $locationID, $this->data['arrival']);
			
			if( $tripID == 0 ){
				$obj_trip->setLocationID( $locationID             );
				$obj_trip->setArrival   ( $this->data['arrival']  ); 
				$obj_trip->setTravelID  ( $this->data['travelID'] );
				$obj_trip->Create();
				$tripID = $obj_trip->getTripID();	
			}  
			
			if($this->data['current_location']){
				$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
				$obj_traveler->setPresentLocation($locationID);
			}
			
			//$obj_entry->setTitle       ( ereg_replace("[&\/\'\"]", "", strip_tags($this->data['title'])) );   
			// $obj_entry->setTitle       ( preg_replace("/[&\/\'\"]/", "", strip_tags($this->data['title'])) );   
			$obj_entry->setTitle       ( stripslashes(strip_tags($this->data['title'])) );
			$obj_entry->setTripID      ( $tripID                                    );
			$obj_entry->setTravelID    ( $this->data['travelID']                    );
			$obj_entry->setDescription ( $this->data['description']                 );
			$obj_entry->setPublish     ( $this->data['publish']                     );
			$obj_entry->setCallout     ( $this->data['callout']                     ); 
			$obj_entry->setTemplateType( $this->data['template_theme']              );
			
			$obj_entry->Create();
			
			$this->data['travellogID'] = $obj_entry->getTravelLogID(); 
			
			$col_trips         = $obj_entry->getTravel()->getTrips();
			if( count($col_trips) ){
				$has_publish_entry = false;
				foreach( $col_trips as $obj_trip ){
					foreach( $obj_trip->getTravelLogs() as $obj_entry ){
						if( $obj_entry->getPublish() ){
							$has_publish_entry = true;
							break;			
						}	
					}
					if( $has_publish_entry ) break;
				}
				
				$obj_travel = $this->file_factory->getClass('Travel', array($this->data['travelID']) );
				if( $has_publish_entry ){
					if( $obj_travel->getPublish() == 0) $obj_travel->PublishUnpublishEntry();
				}
				else{
					if( $obj_travel->getPublish() == 1) $obj_travel->PublishUnpublishEntry();
				}
			}
			else{
				$obj_travel = $this->file_factory->getClass('Travel', array($this->data['travelID']) );
				if( $obj_travel->getPublish() == 1) $obj_travel->PublishUnpublishEntry();
			}

			$obj_factory                      = $this->file_factory->invokeStaticClass('LocationFactory');
			$obj_location                     = $obj_factory->create($locationID);
			$props                            = array();
			$obj_subject                      = $this->file_factory->getClass('gaSubject');
			//$obj_mail                         = $this->file_factory->getClass('MailImplController');
			$obj_journal_event_handler		  = $this->file_factory->getClass('JournalEventHandler');
			
			$props['travelID']                = $this->data['travelID'];
			$props['locationID']              = $locationID;  
			$props['travelerID']              = $this->obj_session->get('travelerID');
			$props['description']             = $this->data['description'];
			$props['arrivaldate']             = $this->data['arrival'];
			$props['groupmembers']            = $this->data['chkGroup'];
			$props['addressbook']             = $this->data['chkAddressBook'];
			$props['travellogID']             = $obj_entry->getTravelLogID();
			$props['groupmembersaddressbook'] = $this->data['chkGroupAddressBook'];
			$props['countryID']               = $obj_location->getCountry()->getCountryID();
			$props['action']                  = gaActionType::$SAVE;
			
			$obj_event = $this->file_factory->getClass('gaAbstractEvent', array(null, $props));
			//$obj_subject->registerObserver($obj_mail);
			$obj_subject->registerObserver($obj_journal_event_handler);
			$obj_subject->notifyObservers($obj_event);
			
			$obj_log = $this->file_factory->getClass('JournalLog');
			$obj_log->setTravelID    ( $this->data['travelID']          );
			$obj_log->setLogType     ( JournalLogType::$ADDED_NEW_ENTRY );
			$obj_log->setTravelLogID ( $obj_entry->getTravelLogID()     );
			$obj_log->Save();
			
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			if (isset($_POST['draftID']) AND is_numeric($_POST['draftID'])) {
				if ($draft = JournalEntryDraftPeer::retrieveByPk($_POST['draftID'])) {
					$draft->delete();
				}
			}
			
		}
		
		if($this->data['allowAutoApprove'] == 1){
			// auto approve for selected groups
			$groups = isset($this->data['groupIDStr']) ? $this->data['groupIDStr'] : '';
			$notpermittedgroups = isset($this->data['notGroupIDStr']) ? $this->data['notGroupIDStr'] : '';
			
			$groups = (!empty($groups)) ? explode(',', $groups) : array();
			$notpermittedgroups = (!empty($notpermittedgroups)) ? explode(',', $notpermittedgroups) : array();
			
			if(!empty($groups)){
				require_once('travellog/helper/Class.AutoApproveFeatureJournalHelper.php');
				AutoApproveFeatureJournalHelper::autoApproveAndFeatureForGroups($obj_travel, $groups);
			}
			
			require_once('travellog/model/Class.JournalGroupPermissions.php');
			JournalGroupPermissions::applyPermissions($groups,$notpermittedgroups,$obj_travel->getTravelID());
		}
		
		// Added by: Adelbert Silla
		// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
		//           This is a preperation for updating application static views in facebook
		// Date added: aug. 10, 2009
		require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');
		// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
		fbNeedToUpdateAccountController::saveNewlyEditedAccount($this->obj_session->get('travelerID'),4);
		
		// added by nash - March 22, 2010
		// twitter widget observer
		// note: this also posts to facebook!!!
		require_once('travellog/controller/twitterWidget/Class.WidgetPostObserver.php');
		$url = '/journal-entry.php?action=view&travellogID='.$this->data['travellogID'];
		$params = array();
		
		if ($this->obj_session->get('travelerID') == 6397){
			$params = array(
				'title'   => $this->data['title'],
				'message' => 'I\'ve written a new journal',
			);
		}
		
		WidgetPostObserver::post($url, $params);
		
		ToolMan::redirect($url);
	}
	
	protected function _publishUnpublish(){
		$obj_entry = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
		$obj_entry->PublishUnpublishEntry();
		$travellogID = $this->data['travellogID'];
		ToolMan::redirect('/journal-entry.php?action=view&travellogID=' . $travellogID); 
		//ToolMan::redirect($_SERVER['HTTP_REFERER']);
	}
	
	protected function _delete(){
		$obj_entry = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']) );
		//$obj_entry->Delete();
		$obj_entry->Archive();
		
		$username = $obj_entry->getTraveler()->getUsername();
	 	
	 	$obj_log = $this->file_factory->getClass('JournalLog');
		$obj_log->setLogType( 1                          );
		$obj_log->setLinkID ( $this->data['travellogID'] );   
		$obj_log->Delete();

		// Added by: Adelbert Silla
		// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
		//           This is a preperation for updating application static views in facebook
		// Date added: aug. 10, 2009
		require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');
		// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
		fbNeedToUpdateAccountController::saveNewlyEditedAccount($this->obj_session->get('travelerID'),4);
		
		// For site admin only
		if( $this->obj_session->isSiteAdmin() ){
			require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
			RealTimeNotifier::getInstantNotification(RealTimeNotifier::ADMIN_DELETE_ARTICLE_ENTRY, 
				array(
					'ARTICLE_ENTRY' => $obj_entry
				))->send();
		}
		
		if( $obj_entry->getOwner() instanceof Traveler )   
			ToolMan::redirect('/'.$username);
		else
			ToolMan::redirect('/travel.php?action=groupJournals&gID='.$obj_entry->getOwner()->getGroupID());
	}
	
	protected function __applyRules(){
		try{
			if( in_array( strtoupper($this->data['action']), AbstractEntryController::$REQUIRES_ACTION_RULES) && !$this->obj_session->isSiteAdmin() ){
				// TODO: If some actions requires a more complex rules use a rule object or method instead  
				switch( strtolower($this->data['action']) ){
					case 'save':
						if( !$this->obj_session->getLogin() || !isset($_POST['action']) ){	
							ToolMan::redirect('/journal.php'); 
							exit;
						}
					break;	  	 
					case 'delete':  			  
					case 'edit':
						if( !$this->obj_session->getLogin() ) ToolMan::redirect('/journal.php');
						$obj_entry  = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
						$travelerID = $obj_entry->getTrip()->getTravel()->getTravelerID();
						if(  $travelerID != $this->obj_session->get('travelerID') )
							ToolMan::redirect('/journal.php');    
						break;
					case 'publishunpublish':
						if( !$this->obj_session->getLogin() ) ToolMan::redirect('/journal.php');
						$obj_entry  = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
						$travelerID = $obj_entry->getTrip()->getTravel()->getTravelerID();
						if(  $travelerID != $this->obj_session->get('travelerID') )
							ToolMan::redirect('/journal.php');    
						break;
					case 'view':
					
						if( !isset($this->data['travelID']) ) ToolMan::redirect('/journal.php');
					  
						$obj_journal  = $this->file_factory->getClass('Travel', array($this->data['travelID']));
						$travelerID   = $obj_journal->getTravelerID();
					
						if($obj_journal->getTraveler()->isSuspended() && !$this->obj_session->isSiteAdmin()){
							ToolMan::redirect('/journal.php');
						} 
					
						$obj_criteria =  $this->file_factory->getClass('Criteria2');
						if( $travelerID != $this->obj_session->get('travelerID') ){
							$obj_criteria->mustBeEqual('tblTravelLog.publish', 1); 
						} 
						if( $obj_journal->getCountEntries($obj_criteria) == 0 ) ToolMan::redirect('/journal.php');  
						//if( $obj_journal->getPublish() == 0 && $travelerID != $this->obj_session->get('travelerID') )
						//	ToolMan::redirect('/journal.php');      
						if( $this->data['travellogID'] ){
							$obj_entry  = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
							//if( ( $travelerID != $this->obj_session->get('travelerID') ) && $obj_entry->getPublish() == 0 )
								//ToolMan::redirect('/journal.php');    
						}
					  
						break;
					case 'add':
						if( !$this->obj_session->getLogin() ) ToolMan::redirect('/journal.php');
						if( !isset($this->data['travelID']) ) ToolMan::redirect('/journal.php?action=MyJournals');  
						$obj_journal = $this->file_factory->getClass('Travel', array($this->data['travelID']));
						if(  $obj_journal->getTravelerID() != $this->obj_session->get('travelerID') )
							ToolMan::redirect('/journal.php?action=MyJournals');  
					break;
				}
			}
		}catch(exception $e){ ToolMan::redirect('/journal.php');   }	
	}
	
	protected function __getData(){
		if( array_key_exists('travellogID', $this->data) && $this->data['travellogID'] && strtolower($this->data['action']) != 'save' ){
			$obj_entry                    = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
			$obj_trip                     = $this->file_factory->getClass('Trip', array($obj_entry->getTripID()));
			$obj_journal                  = $this->file_factory->getClass('Travel', array($obj_trip->getTravelID()));
			$obj_traveler                 = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );     
			$obj_factory                  = $this->file_factory->invokeStaticClass('LocationFactory');
			$obj_location                 = $obj_factory->create($obj_trip->getLocationID());
			
			if( get_class($obj_location) == 'Country' ){
				$this->data['countryID']  = $obj_trip->getLocationID();
				$this->data['cityID']     = 0;
				$locationID               = $obj_trip->getLocationID();
			}
			elseif( get_class($obj_location) == 'City' ){
				$this->data['countryID']  = $obj_location->getCountry()->getLocationID();
				$this->data['cityID']     = $obj_trip->getLocationID();
				$locationID               = $obj_trip->getLocationID();
			}
			$curr_locationID               = $obj_traveler->getTravelerProfile()->getCurrLocationID();
			$this->data['current_location']= ( $locationID == $curr_locationID )? 1 : 0; 
			
			$this->data['journal_title']  = $obj_journal->getTitle();
			$this->data['title']          = $obj_entry->getTitle();
			$this->data['publish']        = $obj_entry->getPublish();
			$this->data['description']    = $obj_entry->getDescription();
			$this->data['callout']        = $obj_entry->getCallout();
			$this->data['travelID']       = $obj_entry->getTravelID();
			$this->data['arrival']        = $obj_trip->getArrival();
			$this->data['template_theme'] = $obj_entry->getTemplateType(); 
			$this->data['errors']         = array();
			$this->data['action']         = '/journal.php'; 
			$this->data['mode']           = 'edit';
		}
		else{  
			$obj_journal                       = $this->file_factory->getClass('Travel', array($this->data['travelID']));
			$col_trips                         = $obj_journal->getTrips();
			
			if( count($col_trips) ){
				$obj_trip                          = $col_trips[count($col_trips)-1];
				$obj_factory                       = $this->file_factory->invokeStaticClass('LocationFactory');
				$obj_location                      = $obj_factory->create($obj_trip->getLocationID());
				
				if( get_class($obj_location) == 'Country' ){
					$this->data['countryID']  = ( isset($this->data['countryID']) )? $this->data['countryID'] : $obj_trip->getLocationID();
					$this->data['cityID']     = ( isset($this->data['cityID'])    )? $this->data['cityID']    : 0;
				}
				elseif( get_class($obj_location) == 'City' ){
					$this->data['countryID']  = ( isset($this->data['countryID']) )? $this->data['countryID'] : $obj_location->getCountry()->getLocationID();  
					$this->data['cityID']     = ( isset($this->data['cityID']) )? $this->data['cityID'] : $obj_trip->getLocationID();
				}
				elseif( get_class($obj_location) == 'NewCity' || $obj_location instanceof State ){
					$this->data['countryID']  = ( isset($this->data['countryID']) )? $this->data['countryID'] : $obj_location->getCountry()->getLocationID();  
					$this->data['cityID']     = ( isset($this->data['cityID']) )? $this->data['cityID'] : $obj_trip->getLocationID();
				}
				
			}
			else{
				$this->data['countryID']           = ( isset($this->data['countryID'])           )? $this->data['countryID']           : 0;
				$this->data['cityID']              = ( isset($this->data['cityID'])              )? $this->data['cityID']              : 0;
			}
			$this->data['journal_title']       = $obj_journal->getTitle();
			$this->data['title']               = ( isset($this->data['title'])               )? trim($this->data['title'])         : '';
			$this->data['publish']             = ( isset($this->data['publish'])             )? $this->data['publish']             : 1;
			$this->data['description']         = ( isset($this->data['description'])         )? $this->data['description']         : '';
			$this->data['callout']             = ( isset($this->data['callout'])             )? $this->data['callout']             : '';
			$this->data['travelID']            = ( isset($this->data['travelID'])            )? $this->data['travelID']            : 0;
			$this->data['travellogID']         = ( isset($this->data['travellogID'])         )? $this->data['travellogID']         : 0;
			$this->data['arrival']             = ( isset($this->data['arrival'])             )? $this->data['arrival']             : '';
			//$this->data['countryID']           = ( isset($this->data['countryID'])           )? $this->data['countryID']           : 0;
			//$this->data['cityID']              = ( isset($this->data['cityID'])              )? $this->data['cityID']              : 0;
			$this->data['current_location']    = ( isset($this->data['current_location'])    )? $this->data['current_location']    : 0;
			$this->data['template_theme']      = ( isset($this->data['template_theme'])      )? $this->data['template_theme']      : 0;
			$this->data['chkGroup']            = ( isset($this->data['chkGroup'])            )? $this->data['chkGroup']            : 1;
			$this->data['chkGroupAddressBook'] = ( isset($this->data['chkGroupAddressBook']) )? $this->data['chkGroupAddressBook'] : 1;
			$this->data['chkAddressBook']      = ( isset($this->data['chkAddressBook'])      )? $this->data['chkAddressBook']      : 1;
			$this->data['errors']              = array();
			$this->data['action']              = '/journal.php';
			$this->data['mode']                = 'add';
		}	
	}
	
	protected function __validateForm(){
		$this->__getData();
		$errors = array();
		if( !preg_match('/.+/', $this->data['title']) ){
			$errors[] = 'Title is a required field!';
		}
		if( !preg_match('/.+/', $this->data['description']) ){
			$errors[] = 'Description is a required field!';
		}
		if( $this->data['countryID'] == 0 && $this->data['cityID'] == 0 ){
			$errors[] = 'Location of Journal Entry is a required field!';   
		}
		if( TravelLog::titleExistsInJournal($this->data['title'],$this->data['travelID'],$this->data['travellogID']) ){
			$errors[] = 'Duplicate entry title for this journal!';
		} 
		
		$this->data['errors'] = $errors;
		
		if( count($this->data['errors']) ){ 
			$this->_viewAddForm();
			exit;	
		}
	}
	
	protected function beforePerformAction(){
		if( $this->obj_session->getLogin() ){ 
			require_once('gaLogs/Class.GaLogger.php'); 
			GaLogger::create()->start($this->obj_session->get('travelerID'),'TRAVELER');   
		}
	}
}
?>
