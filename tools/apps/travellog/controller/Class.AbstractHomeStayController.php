<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	abstract class AbstractHomeStayController implements iController{
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,
		
		$isLogged	  = false,
		
		$isMemberLogged = false,
		
		$isAdminLogged = false,
		
		$group 			= NULL,
		
		$homeStay		= NULL,
		
		$users			= array(),
		
		$tpl			= NULL;
	
		
		function __construct(){
			
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template' , array ( 'path' => '' ));
			
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('HomeStay');
			
			$this->file_factory->registerClass('Paging' ,array('path'=> ''));
			$this->file_factory->registerClass('ObjectIterator' ,array('path'=> ''));
	
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
		}
		
		function performAction(){
			$params = array();
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'ViewList';	
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
			
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : 0 ;
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : $params['gID'];
			
			$params['hsID'] = ( isset($_GET['hsID']))? $_GET['hsID'] : 0;	
			$params['hsID'] = ( isset($_POST['hsID']))? $_POST['hsID'] : $params['hsID'];
		
			switch($params['mode']){
				case "add":
					$this->_addHomeStay($params);
					break;

				case "edit":
					$this->_editHomeStay($params);
					break;
					
				case "save":
					$this->_saveHomeStay($params);
					break;
			
				case "delete":
					$this->_deleteHomeStay($params);
					break;
				
				case "ajaxcall":
					$actiontype = ( isset($_POST['actiontype']))? $_POST['actiontype'] : '' ;
					if ('combo' == $actiontype)
						$this->_showComboUsers($params);
					elseif ('update' == $actiontype)
						$this->_updateComboUsers($params);												
					break;
				default:
					$actiontype = ( isset($_POST['actiontype']))? $_POST['actiontype'] : '' ;
					if (0 < $params['hsID'])
						$this->_viewHomeStay($params);
					else
						$this->_viewHomeStays($params);
					break;
				
			}		
		
		}
		
		
		function _defineCommonAttributes($_params = array()){
			
			//check if user is currently logged in
			if ($this->obj_session->get('travelerID') > 0) {
				$this->isLogged		=	true ;
				$this->loggedUser = $this->file_factory->getClass ('Traveler',array($this->obj_session->get('travelerID')));
			
			}
	
			// if groupID is defined, instantiate group and check if logged user is member or adminstrator
			if (0 < $_params['gID']){
				$mGroup	= $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($_params['gID']));
				$this->group = $mGroup[0];

				$this->isMemberLogged	=	( $this->isLogged ) ? $this->group->isMember($this->loggedUser) : false;
				$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') ? true : false;
			
			}
			
			
			// if homestayID is defined, instatiate
			
			if (0 < $_params['gID']) {
				
				$this->homeStay = $this->file_factory->getClass('HomeStay', array($_params['hsID']));
				
			}
			
			//  get all travelers			
				$this->users = $this->file_factory->invokeStaticClass('Traveler','getAllTravelers');
				
			//  create generic template
				$this->tpl = $this->file_factory->getClass('Template');
				$this->tpl->set_path("travellog/views/");
			
		}
		
		function _allowAdminPrivileges($_privilege = false){
			if (!$_privilege){
				header("location:index.php");
				exit;
			}
		}
		
		
		function _addHomeStay($_params = array() ){
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged); // if not logged in, redirect
			
			$this->tpl->set('users',$this->users);
			$this->tpl->out('tpl.FrmHomeStay.php');
			
		}
		
		
		function _editHomeStay($_params = array() ){
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged); // if not logged in, redirect
			
			if (isset($this->homeStay)):
				$this->tpl->set('txtName',trim($this->homeStay->getName()));
				$this->tpl->set('txaAddress',trim($this->homeStay->getAddress()));
				$this->tpl->set('txtEmail',trim($this->homeStay->getEmail()));
				$this->tpl->set('txtPhone',trim($this->homeStay->getPhone()));
				$this->tpl->set('txaFamNames',trim($this->homeStay->getFamilyNames()));
				$this->tpl->set('txaDesc',trim($this->homeStay->getDescription()));
				$this->tpl->set('hsID',$this->homeStay->getHomeStayID());
				$this->tpl->set('users',$this->users);
				if (null!=$this->homeStay->getAssignedTraveler())
					$this->tpl->set('assignedStudentID', $this->homeStay->getAssignedTraveler()->getTravelerID());							
				$this->tpl->out('tpl.FrmHomeStay.php');				
			endif;
				
		}
		
		
		function _showComboUsers($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged); // if not logged in, redirect
			
			$this->file_factory->invokeStaticClass('HomeStay','initConn');
			$unAssignedTravelers = $this->file_factory->invokeStaticClass('HomeStay','getAllUnassignedTravelers');
			
			$this->tpl->set('homeStayID' , $this->homeStay->getHomeStayID());
			$this->tpl->set('currAssignedTraveler', $this->homeStay->getAssignedTraveler() );
			$this->tpl->set('unAssignedTravelers', $unAssignedTravelers);
			$this->tpl->out('tpl.cmbStudent.php');

		}
		
		
		function _updateComboUsers($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged); // if not logged in, redirect
			
			$userID =  (isset($_POST['userID'])) ? $_POST['userID'] : 0 ;

			if (0 < $_params['hsID']):
				$this->homeStay = $this->file_factory->getClass('HomeStay', array($_params['hsID']));
				$this->homeStay->setAssignedTraveler($userID);
				if (0<$userID)
					echo '<a href="/'.$this->homeStay->getAssignedTraveler()->getUserName() . '">' . $this->homeStay->getAssignedTraveler()->getUserName(). "</a>";
				else
					echo 'No assigned traveler';
			endif;
			
			
		}
		function _saveHomeStay($_params = array() ){
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged); // if not logged in, redirect
			
			
			$errormsg = array();
			
			if (0 == strlen($_POST['txtName'])):
				$errormsg['txtName'] = "<li>Name must be filled out.</li>";
			endif;
			
			if(0 < count($errormsg)):
			
				$this->tpl->set('mode',$mode);
				$this->tpl->set('errormsg',$errormsg);
				$this->tpl->set('txtName'		,	strlen($_POST['txtName']) 		? $_POST['txtName'] 	: '');
				$this->tpl->set('txaAddress'	,	strlen($_POST['txaAddress']) 	? $_POST['txaAddress'] 	: '');
				$this->tpl->set('txtEmail'	,	strlen($_POST['txtEmail']) 		? $_POST['txtEmail'] 	: '');
				$this->tpl->set('txtPhone'	,	strlen($_POST['txtPhone']) 		? $_POST['txtPhone'] 	: '');
				$this->tpl->set('txaFamNames'	,	strlen($_POST['txaFamNames']) 	? $_POST['txaFamNames'] : '');
				$this->tpl->set('txaDesc'		,	strlen($_POST['txaDesc']) 		? $_POST['txaDesc'] 	: '');
				$this->tpl->set('hsID',$this->homeStay->getHomeStayID());
				$this->tpl->set('users',$this->users);
				$this->tpl->set('assignedStudentID'	,	$_POST['cmbStudent']);						
				$this->tpl->out('tpl.FrmHomeStay.php');		
		
			else:
				
				$this->homeStay->setName($_POST['txtName']);
				$this->homeStay->setAddress($_POST['txaAddress']);
				$this->homeStay->setEmail($_POST['txtEmail']);
				$this->homeStay->setPhone($_POST['txtPhone']);
				$this->homeStay->setFamilyNames($_POST['txaFamNames']);
				$this->homeStay->setDescription($_POST['txaDesc']);
				$this->homeStay->Save();
				
				$this->homeStay->setAssignedTraveler($_POST['cmbStudent']);
				
				header('location: homestay.php');
				exit;
			
			endif;
					
		}
		
		
		function _deleteHomeStay($_params = array() ){
			
			$this->_defineCommonAttributes($_params);
			$this->_allowAdminPrivileges($this->isAdminLogged); // if not logged in, redirect
			
			$this->homeStay->Delete();
			$this->homeStay->removedAssignedTraveler();	
			
			header('location: homestay.php');
			exit;
			
		}
			
		function _viewHomeStay($_params = array() ){
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged); // if not logged in, redirect
			
			$this->tpl->set('homeStay', $this->homeStay);
			$this->tpl->set('isAdminLogged', $this->isAdminLogged);
			$this->tpl->out('tpl.ViewHomeStay.php');
			
		}
		
	
		
		function _viewHomeStays($_params = array() ){
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged); // if not logged in, redirect
			
			$this->file_factory->invokeStaticClass('HomeStay','initConn');
			$arrHomeStays =$this->file_factory->invokeStaticClass('HomeStay','getAllHomeStays');
		
			$page = ( isset($_GET['page']) )? $_GET['page'] : 1;

			$paging    =  $this->file_factory->getClass('Paging', array(count($arrHomeStays),$page,'',5));
			$iterator  =  $this->file_factory->getClass('ObjectIterator', array($arrHomeStays,$paging));
			
			$this->tpl->set('paging',$paging);
			$this->tpl->set('iterator',$iterator);
			$this->tpl->set('totalrec', count($arrHomeStays));
			$this->tpl->out('tpl.HomeStayList.php');
			
		}
		
	}
?>