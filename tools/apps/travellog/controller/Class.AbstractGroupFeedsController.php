<?php

	require_once("travellog/factory/Class.FileFactory.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("Class.Template.php");
	
	require_once("travellog/model/Class.ActivityFeed.php");
	
	class AbstractGroupFeedsController implements iController{
		protected $mSession			=	NULL;
		protected $file_factory		=	NULL;
		protected $mTemplate		=	NULL;
		protected $mSubNavigation	=	NULL;
		protected $mContextInfo		=	NULL;
		
		protected $mGroup			=	NULL;
		protected $mTraveler		=	NULL;
		
		protected $mParams			=	array(	"action"	=>	NULL,
		  										"mode"		=>	NULL,
												"gID"		=>	NULL,
		 										"sgID"		=>	NULL);
		
		function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('SessionManager');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('GroupFactory');
			
			$this->file_factory->registerClass('ViewGroupFeeds', array('path' => 'travellog/views/groupfeeds/'));
			$this->file_factory->registerClass('ViewGroupFeedsSearch', array('path' => 'travellog/views/groupfeeds/'));
			$this->file_factory->registerClass('ProfileCompFactory', array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerTemplate('LayoutMain');

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->mSession = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			$this->mLoggedID = $this->mSession->get('travelerID');
		}
		
		function performAction(){
			$this->mParams = array_merge($this->mParams,$this->extractParameters());

			$validActions = array("","search");
			$this->mParams["action"] = (!is_null($this->mParams["action"]) && in_array($this->mParams["action"],$validActions))? $this->mParams["action"] : "default";
			
			$validModes = array("ALL","JOURNALS","MULTIMEDIA","DISCUSSION","MEMBERSHIP");
			$this->mParams["mode"] = in_array($this->mParams["mode"],$validModes)? $this->mParams["mode"] : "ALL";			
			
			switch($this->mParams["action"]){
				case "search"	:	$this->_search();
									break;
				case "archive"	:	$this->_archiveDiscussion();
									break;
				case "activate"	:	$this->_activateDiscussion();
									break;
				case "addtobase":	$this->_addtobaseDiscussion();
									break;
				case "removefrombase":	$this->_removefrombaseDiscussion();
										break;
				case "pin"		:	$this->_pinDiscussion();
									break;
				case "unpin"	:	$this->_unpinDiscussion();
									break;
				case "approve"	:	$this->_approveJournal();
									break;
				case "unapprove":	$this->_unapproveJournal();
									break;
				case "feature"	:	$this->_featureJournal();
									break;
				case "unfeature":	$this->_unfeatureJournal();
									break;
				case "accept"	:	$this->_acceptRequest();
									break;
				case "deny"		:	$this->_denyRequest();
									break;
				default			:	$this->_render();
									break;
			}
		}
		
		function extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}
		
		function _defineCommonAttributes(){
			$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->mTemplate = $this->file_factory->getClass('Template');
			$this->mTemplate->set_path('travellog/views/groupfeeds/');
			
			Template::setMainTemplateVar('page_location','Group Feeds');
			
			$this->mSubNavigation = $this->file_factory->getClass('SubNavigation');
			$this->mSubNavigation->setContext('GROUP');
			$this->mSubNavigation->setContextID($this->mGroup->getGroupID());
			$this->mSubNavigation->setLinkToHighlight('GROUP_FEEDS');
			if ($this->mGroup->getDiscriminator() == GROUP::ADMIN){
				$this->mSubNavigation->setGroupType('ADMIN_GROUP');
			}else if($this->mGroup->getDiscriminator() == GROUP::FUN){
				$this->mSubNavigation->setGroupType('FUN_GROUP');
			}
			
			$mProfileFactory = $this->file_factory->invokeStaticClass('ProfileCompFactory', 'getInstance', array());
			$mProfileComp	= $mProfileFactory->create( ProfileCompFactory::$GROUP_CONTEXT );
			//$mProfileComp->init($this->mGroup->getGroupID());

			if(2 == $this->mGroup->getDiscriminator() && (1 < $this->mGroup->getParentID()))
				$mProfileComp->init($this->mGroup->getParentID());
			else
				$mProfileComp->init($this->mGroup->getGroupID());

			$this->mContextInfo = $mProfileComp->get_view();
		}
		
		function _checkPrivileges($redirect=FALSE){
			// check if user is currently logged in
			$this->mTraveler = $this->file_factory->getClass('Traveler', array($this->mLoggedID));
			
			
			/*if( $this->mTraveler->isAdvisor() ){
				if( isset($_GET["gID"]) && $_GET["gID"] != $this->mTraveler->getAdvisorGroup() ){
					$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($_GET["gID"]));
				}else{
					$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($this->mTraveler->getAdvisorGroup()));
				}
				$this->mGroup = $group[0];
				$this->mParams["gID"] = $this->mGroup->getGroupID();
				$this->mIsPrivileged = TRUE;
				
			}else{
				if( isset($_GET["gID"]) && "" != $_GET["gID"] && is_numeric($_GET["gID"]) && 0 < intval($_GET["gID"]) ){
					$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($_GET["gID"]));
					$this->mGroup = $group[0];
					if( ($this->mGroup instanceof AdminGroup || $this->mGroup instanceof Group)
						&& $this->mGroup->isStaff($this->mLoggedID) ){
						$this->mIsPrivileged = TRUE;
					}
				}else{
					try{
						$config = new Config($_SERVER["SERVER_NAME"]);
						if( $config instanceof Config ){
							$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($config->getGroupID()));
							$this->mGroup = $group[0];
							$this->mParams["gID"] = $this->mGroup->getGroupID();
							if( $this->mGroup->isStaff($this->mLoggedID) ){
								$this->mIsPrivileged = TRUE;
							}
						}
					}catch(exception $e){
					}
				}
			}*/
			
			if( isset($_GET["gID"]) && "" != $_GET["gID"] && is_numeric($_GET["gID"]) && 0 < intval($_GET["gID"]) ){
				$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($_GET["gID"]));
				$this->mGroup = $group[0];
				if( ($this->mGroup instanceof AdminGroup || $this->mGroup instanceof Group)
					&& ( $this->mGroup->getAdministratorID() == $this->mLoggedID || $this->mGroup->isStaff($this->mLoggedID)) ){
					$this->mIsPrivileged = TRUE;
				}
			}elseif( isset($GLOBALS["CONFIG"]) ){
				$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($GLOBALS["CONFIG"]->getGroupID()));
				$this->mGroup = $group[0];
				if( $this->mGroup->getAdministratorID() == $this->mLoggedID || $this->mGroup->isSuperStaff($this->mLoggedID) ){
					$this->mIsPrivileged = TRUE;
				}
			}
			
			if( !($this->mGroup instanceof AdminGroup) ){
				header("Location: /FileNotFound.php");
				exit;
			}

			if( !$this->mGroup->isParent() ||
				(!is_null($this->mParams["sgID"]) 
				&& !is_null($this->mParams["gID"]) 
				&& $this->mParams["sgID"] == $this->mParams["gID"]) ){
				$this->mParams["sgID"] = NULL;
			}
			
			if ( $this->mIsPrivileged ) {
				// start GA Logger
				require_once 'gaLogs/Class.GaLogger.php';
				try{
					GaLogger::create()->start($this->mLoggedID, 'TRAVELER');
				}
				catch(exception $e){
				}
			}elseif($redirect){
				header("Location: /index.php");
				exit;
			}
		}
		
		function _prepareDependencies(){
			Template::setMainTemplateVar('layoutID', 'group_feeds');
			Template::includeDependentJs("/min/f=js/groupfeeds-1.1.js");	
		}
		
		function _render(){
			$this->_checkPrivileges(TRUE);
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			
			$mContent = $this->file_factory->getClass('ViewGroupFeeds');
			$mContent->setParams($this->mParams);
			$mContent->setGroup($this->mGroup);
			
			$this->mTemplate->set("content",		$mContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->out("tpl.ViewGroupFeeds.php");
		}
		
		function _search(){
			$this->_checkPrivileges();

			$feedsSearch = $this->file_factory->getClass('ViewGroupFeedsSearch');
			$feedsSearch->setParams($this->mParams);
			$feedsSearch->setGroup($this->mGroup);
			$feedsSearch->setLoggedUser($this->mTraveler);
			$feedsSearch->retrieve();
			
			$feedsSearch->render();
		}		
	}