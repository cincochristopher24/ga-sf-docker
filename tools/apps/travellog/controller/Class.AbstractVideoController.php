<?php
 	/*
 	 * Created on 10 15, 08
 	 *
 	 * To change the template for this generated file go to
 	 * Window - Preferences - PHPeclipse - PHP - Code Templates
 	 */
 	
 	    require_once('travellog/model/Class.SessionManager.php');
 	    require_once('travellog/helper/Class.VideoHelper.php');
 	    require_once('travellog/controller/Class.IController.php');
 	    require_once('travellog/factory/Class.FileFactory.php');
		
 	    abstract class AbstractVideoController implements IController {
 	        
 	        protected 
 	        	$obj_session	= null,
 	        	$file_factory 	= null,
 	        	$loggedTraveler = null,
 	        	$params 		= null,
 	        	$traveler 		= null,
 	        	$group 			= null,
 	        	$travel 		= null,
 	        	$travelLog 		= null,
 	        	$album			= null,
 	        	$video 			= null,
 	        	$tpl 			= null;
 	        	
 	        function __construct() {
 	        	$this->obj_session = SessionManager::getInstance();
 	        	$this->file_factory = FileFactory::getInstance();
 	        	
 	        	$this->file_factory->registerClass('Template' , array ( 'path' => '' ));
 	            $this->file_factory->registerClass('SubNavigation');
 	            $this->file_factory->registerClass('ToolMan', array('path' => ''));
 	            $this->file_factory->registerClass('Traveler');
 	            $this->file_factory->registerClass('Travel');
 	            $this->file_factory->registerClass('TravelLog');
 	            $this->file_factory->registerClass('TravelLogToVideo');
 	            $this->file_factory->registerClass('VideoAlbum');
 	            $this->file_factory->registerClass('VideoAlbumToVideo');
 	            $this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
 	            $this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
 	            $this->file_factory->registerClass('JournalsComp', array('path' => 'travellog/views/','file'=>'JournalsComp'));
 	            $this->file_factory->registerClass('GroupFactory');
				$this->file_factory->registerClass('CommentsView' , array('path' => 'travellog/views/'));
 	            
 	            $this->file_factory->registerTemplate('LayoutMain');
 	            $this->file_factory->setPath('CSS', '/css/');
 	            $this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
 	        }
 	        
 	        /** performs the required action **/
 	        
 	        function performAction() {
 	        	$this->initData();
 	        	
				switch($this->params['action']) {
 	        		case 'getTravelerVideos':
						$this->getContextVideos('traveler');
 	        			break;
 	        		case 'getGroupVideos':
 	        			$this->getContextVideos('group');
 	        			break;
 	        		case 'showPrompt':
 	                    $this->showPrompt();
 	                    break;
 	                case 'saveVideo':
 	                	$this->saveVideo();
 	                	break;
 	                case 'deleteVideo':
 	                    $this->deleteVideo();
 	                    break;
 	                case 'playEntryVideo':
 	                    $this->playVideo('entry');
 	                    break;
 	                case 'playAlbumVideo':
 	                	$this->playVideo('album');
 	                    break;
 	        	}
 	        }
 	        
 	        /** initializes the data needed by each function **/
 	        
 	        function initData() {
 	        	$this->params['groupID']		= (isset($_REQUEST['gID'])) ? $_REQUEST['gID'] : 0;
 	        	$this->params['travelerID']		= (isset($_REQUEST['travelerID'])) ? $_REQUEST['travelerID'] : $this->obj_session->get('travelerID');
 	        	$this->params['travelID']		= (isset($_REQUEST['travelID'])) ? $_REQUEST['travelID'] : 0;
 	        	$this->params['travelLogID']	= (isset($_REQUEST['travelLogID'])) ? $_REQUEST['travelLogID'] : 0;
 	        	$this->params['type']           = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : '';

				if (0 < $this->params['groupID']) {
 	        		$this->params['action']		= 'getGroupVideos';
 	        		$group_factory              = $this->file_factory->invokeStaticClass('GroupFactory', 'instance', array());
 	                $arr_groups                 = $group_factory->create(array($this->params['groupID']));
 	                $this->group                = $arr_groups[0];
 	        	}
 	        	else if (0 < $this->params['travelerID']) {
 	        		$this->params['action']		= 'getTravelerVideos';
					$this->traveler 			= $this->file_factory->getClass('Traveler', array($this->params['travelerID']));
					
					if(is_object($this->traveler) && ($this->traveler->isSuspended()||$this->traveler->isDeactivated())){
						header("location: /travelers.php");
						exit;
					}
				}
				
				$this->loggedTraveler 			= $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
 	        	$this->params['action']			= (isset($_REQUEST['action'])) ? $_REQUEST['action'] : $this->params['action'];
 	        	
 	        	$this->params['albumID']		= (isset($_REQUEST['albumID'])) ? $_REQUEST['albumID'] : 0;
 	        	$this->album                    = $this->file_factory->getClass('VideoAlbum', array($this->params['albumID']));
 	            $this->params['albumTitle']     = ($this->params['albumID']) ? $this->album->getTitle() : '';
 	            
 	            $this->params['videoID']        = (isset($_REQUEST['videoID'])) ? $_REQUEST['videoID'] : 0;
 	            $this->video                    = ($this->params['albumID']) ? $this->file_factory->getClass('VideoAlbumToVideo', array($this->params['videoID'])) : $this->file_factory->getClass('TravelLogToVideo', array($this->params['videoID']));
	            $this->params['videoUrl']       = (isset($_REQUEST['videoUrl'])) ? $_REQUEST['videoUrl'] : $this->video->getVideoUrl();
	            $this->params['videoCaption']   = (isset($_REQUEST['caption'])) ? $_REQUEST['caption'] : $this->video->getCaption();
	            $this->params['videoTitle']     = (isset($_REQUEST['title'])) ? $_REQUEST['title'] : $this->video->getTitle();
	
				if ('VideoAlbumToVideo' != get_class($this->video)) {
 	                $this->travelLog				= $this->file_factory->getClass('TravelLog', array($this->params['travelLogID']));
 	                $this->params['travelLogTitle'] = ($this->params['travelLogID']) ? $this->travelLog->getTitle() : '';
	                
 	                $this->travel					= $this->file_factory->getClass('Travel', array($this->params['travelID']));
 	                $this->params['travelTitle']    = ($this->params['travelID']) ? $this->travel->getTitle() : '';
 	            }

				$this->params['hasCreatedAnAlbum']		= (isset($_REQUEST['hasCreatedAnAlbum'])) ? $_REQUEST['hasCreatedAnAlbum'] : 0;
 	           	$this->params['hasEditedAnAlbum']   	= (isset($_REQUEST['hasEditedAnAlbum'])) ? $_REQUEST['hasEditedAnAlbum'] : 0;
 	            $this->params['hasDeletedAnAlbum']  	= (isset($_REQUEST['hasDeletedAnAlbum'])) ? $_REQUEST['hasDeletedAnAlbum'] : 0;
 	            $this->params['hasFailedSavingAnAlbum'] = (isset($_REQUEST['hasFailedSavingAnAlbum'])) ? $_REQUEST['hasFailedSavingAnAlbum'] : 0;
 	            $this->params['hasCreatedAVideo']		= (isset($_REQUEST['hasCreatedAVideo'])) ? $_REQUEST['hasCreatedAVideo'] : 0;
 	           	$this->params['hasEditedAVideo']  		= (isset($_REQUEST['hasEditedAVideo'])) ? $_REQUEST['hasEditedAVideo'] : 0;
 	            $this->params['hasDeletedAVideo']  		= (isset($_REQUEST['hasDeletedAVideo'])) ? $_REQUEST['hasDeletedAVideo'] : 0;
 	            
 	            $this->params['fail']           = (isset($_REQUEST['fail'])) ? $_REQUEST['fail'] : 0;
 	            $this->params['jeVideos']       = (isset($_REQUEST['jeVideos'])) ? $_REQUEST['jeVideos'] : 0;
 	            $this->params['redirect']		= (isset($_REQUEST['redirect'])) ? $_REQUEST['redirect'] : '';
 	        }
 	        
 	        /** initializes the template to be used **/
 	        
 	        function initTemplate() {
 	     		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
 	            $this->tpl = $this->file_factory->getClass('Template');
 	            $this->tpl->set_path('travellog/views/');
 	            
 	            $subNavigation = $this->file_factory->getClass('SubNavigation');
 	            $subNavigation->setLinkToHighlight('MY_VIDEOS');
 	            
 	            if (is_null($this->group)) {
 	            	$subNavigation->setContext('TRAVELER');
 	                $subNavigation->setContextID($this->traveler->getTravelerID());
 	                Template::setMainTemplateVar('title',  'GoAbroad Network: Online Community for Travelers');
					 	                
	                if ($this->loggedTraveler->getTravelerID() == $this->traveler->getTravelerID()) 
	                    Template::setMainTemplateVar('page_location', 'My Passport');
 	                else 
 	                	Template::setMainTemplateVar('page_location', 'Travelers');
 	            } 
 	            
 	            else {
 	            	require_once('travellog/helper/Class.NavigationHelper.php');
 	            	
 	            	$subNavigation->setContext('GROUP');
 	                $subNavigation->setContextID($this->group->getGroupID());
 	                Template::setMainTemplateVar('title',  'GoAbroad Network: Online Community for Travelers');
 	                
 	                if(NavigationHelper::isCobrand())
						Template::setMainTemplateVar('page_location', 'Home');
					else
						Template::setMainTemplateVar('page_location', 'Groups / Clubs');    
 	            }  	

				Template::includeDependentCss("/css/vsgStandardized.css");
				Template::includeDependentCss("/css/modalBox.css");
				Template::includeDependentCss("/css/Thickbox.css");
				Template::includeDependentJs("/js/thickbox.js");
				
 	            $this->tpl['subNavigation'] = $subNavigation;
 	        }
 	        
 	        /** 
 	         * 	sets the variables for the main template
 	         *  @param array of template variables 
 	         */
 	        
 	        function setTemplateVariables ($tplVars = array()) {
 	            foreach ($tplVars as $key => $val)
 	                $this->tpl->set($key, $val);
	        }
	        
	        function getVideoAndTravelAlbumsOfContext($context = '') {
	        	switch ($context) {
	        		case 'traveler':
	        			$videoAlbums = $this->traveler->getVideoAlbums();
						if( isset($GLOBALS["CONFIG"]) && $this->loggedTraveler->getTravelerID() != $this->traveler->getTravelerID() ){
 	            			$travels	 = $this->traveler->getCBTravels($privacycriteria = 0, $_isOwner = 0, $GLOBALS["CONFIG"]->getGroupID());
	        			}else{
							$travels	 = $this->traveler->getTravels();
						}
						break;
	        		case 'group':
	        			/************************************************************************************************************
			             * incorporated from Class.AbstractJournalsController.php to get staff/admin journals
		 	             * instead of loggedTravelerID is passed to parameter of JournalsComp, it's the administratorID of the group
		 	             ************************************************************************************************************/
	        			$jCompContextAr                    	= array();
		 	            $jCompContextAr['VIEWMODE']         = 'GROUP.MANAGE';
		 	            $jCompContextAr['SECURITY_CONTEXT'] = 'LOGIN';
		 	            $jCompContextAr['VIEW_TAB']         = 'STAFF_ADMIN';
		 	            
		 	            $journalsComp = $this->file_factory->getClass('JournalsComp',array($jCompContextAr,$this->params['groupID'],$this->group->getAdministrator()->getTravelerID()));
		 	            $travels      = $journalsComp->getStaffAdminJournalsOnlyForVideos();
		 	            $videoAlbums  = $this->group->getVideoAlbums();
		 	            $tempArray 	  = array();
		 	            
		 	            foreach ($travels as $travel) 
		 	            	$tempArray[] = $travel;
		 	            $travels = $tempArray;
	        			break;
	        	}
	        	return array('videoAlbums' => $videoAlbums, 'travels' => $travels);
	        }
	        
	        /**
	         * displays the main view under the videos tab
	         * @param context (i.e. traveler or group)
	         */
	        
	        function getContextVideos($context = '') {
				$playVideoTemplate			= $this->file_factory->getClass('Template');
 	            $videosListTemplate         = $this->file_factory->getClass('Template');
 	            $collectionsListTemplate    = $this->file_factory->getClass('Template');
				
				$videos = $travelsWithVideos = $albumsWithVideos = $collection = $journalEntries = $tempTravels = $tempAlbums = array();
				$videoUrl = $videoTitle = $videoCaption = '';
				$hasVideos 	  = false;
				$collectionID = 0;
				
				$travelerID = isset($this->params['travelerID']) ? $this->params['travelerID'] : 0;
				$groupID = isset($this->params['groupID']) ? $this->params['groupID'] : 0;
				
				$owner = NULL;
				
				switch ($context) {
					case 'traveler':
						$isOwner 	 = ($this->loggedTraveler->getTravelerID() == $this->traveler->getTravelerID());
						$videosListTemplate['travelerID'] 	   = $this->params['travelerID'];
 	            		$collectionsListTemplate['travelerID'] = $this->params['travelerID'];
 	            		
						$owner = $this->traveler;

 	            		$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
		 	            $profile_comp->init($this->params['travelerID']);
		 	            $this->initTemplate();
						break;
					case 'group':
						/** clubs don't have videos **/
						if (1 == $this->group->getDiscriminator())		
 	                		$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/groups/'.$this->group->getName()));
						
						$isOwner = ($this->loggedTraveler->getTravelerID() == $this->group->getAdministratorID() || $this->group->isStaff($this->loggedTraveler->getTravelerID()) || $this->group->isSuperStaff($this->loggedTraveler->getTravelerID()) || $this->group->isMember($this->loggedTraveler));
						$videosListTemplate['groupID'] 		= $this->params['groupID'];
		 	            $collectionsListTemplate['groupID'] = $this->params['groupID'];
		 	            
						$owner = $this->group;
		
		 	            $profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		 	            $profile_comp->init($this->params['groupID']);
		 	            $this->initTemplate();
		 	            break;
				}
				
				$collectionsArray = $this->getVideoAndTravelAlbumsOfContext($context);
				$videoAlbums = $collectionsArray['videoAlbums'];
				$travels = $collectionsArray['travels'];
				
				if (!$isOwner) {
					/** both for loops filter the video albums and travel journals that has videos **/
					foreach ($videoAlbums as $album)
						if (0 < count($album->getVideos()))
							$tempAlbums[] = $album;	
					$videoAlbums = $tempAlbums;
					
					foreach ($travels as $travel)
						if (0 < count($travel->getTravelVideos()))
							$tempTravels[] = $travel;
					$travels = $tempTravels;
					
					/** gets the journal entries of the current travel journal **/
					if ($this->params['travelID']){
						foreach ($this->travel->getTravelLogsByTravelID() as $log){
							if (0 < count($log->getTravelLogVideos()))          
								$journalEntries[] = $log;	
						}
					} 
					else{
						if (0 < count($travels)){
							foreach ($travels[0]->getTravelLogsByTravelID() as $log){
								if (0 < count($log->getTravelLogVideos()))
									$journalEntries[] = $log;
							}
						}
					}       
				} 
				else {
					if (0 < count($travels)) {
						foreach ($travels as $travel)
							if (0 < count($travel->getTravelLogsByTravelID())) 
								$tempTravels[] = $travel;
						$travels = $tempTravels;
						
						if (0 < count($travels))
							$journalEntries = ($this->params['travelID']) ? $this->travel->getTravelLogsByTravelID() : $travels[0]->getTravelLogsByTravelID();
					}  
				}	
				
				if (!$this->params['type']) {
					if (0 < count($videoAlbums)) {
						$collection 			= $videoAlbums[0];
						$collectionID 			= (!$isOwner) ? $videoAlbums[0]->getAlbumID() : 0;
						$videos 				= $videoAlbums[0]->getVideos();
						//$this->params['type']	= (!$isOwner) ? 'album' : $this->params['type'];
						$this->params['type']   = 'album';
					}
					else if (0 < count($travels)) {
						$collection 			= $travels[0];
						$collectionID 			= (!$isOwner) ? $travels[0]->getTravelID() : 0;
						$videos 				= $travels[0]->getTravelVideos();
						//$this->params['type']	= (!$isOwner) ? 'journal' : $this->params['type'];
						$this->params['type']   = 'journal';
						
						if (!$isOwner) {
 	            			$this->params['travelID'] = $travels[0]->getTravelID();
 	            			$this->travel             = $this->file_factory->getClass('Travel', array($this->params['travelID']));	
 	            		}
					}
				}
				else {
					switch ($this->params['type']) {
						case 'album':
							$collection   = $this->album;
							$collectionID = $this->album->getAlbumID();
							$videos 	  = $this->album->getVideos();
							break;
						case 'journal':
							$collection   = $this->travel;
							$collectionID = $this->travel->getTravelID();
							$videos 	  = $this->travel->getTravelVideos();
							
							/** videos to be displayed should only be from the current journal entry being viewed **/
							if ($this->params['jeVideos']) {		
								$videos   					 	= $this->travelLog->getTravelLogVideos();
								$this->params['travelLogID'] 	= $this->travelLog->getTravelLogID();
								$this->params['travelLogTitle']	= $this->travelLog->getTitle();
							}
							break;
					}
					//$hasVideos = (0 < count($videos));
				}
				
				$hasVideos = (0 < count($videos));
				
				if (0 < count($videos)) {
					$videosListTemplate['isOwner']        = $isOwner;
					$videosListTemplate['videos']         = $videos;
 	                $videosListTemplate['videoID']		  = $this->params['videoID'];
	               	$videosListTemplate['travelID']       = $this->params['travelID'];
 	                $videosListTemplate['travelLogID']    = $this->params['travelLogID'];
 	                $videosListTemplate['collectionType'] = $this->params['type'];
 	                $videosListTemplate['jeVideos']       = $this->params['jeVideos'];

					if ('traveler'==$context):
						if ('album' == $this->params['type']):
							$url = "/video.php?action=getTravelerVideos&travelerID=$travelerID&albumID=".$videos[0]->getParentAlbum()->getAlbumID()."&videoID=".$videos[0]->getVideoID()."&type=".$this->params['type'];
						else:
							if( $this->params['jeVideos'] ):
								$url = "/video.php?action=getTravelerVideos&travelerID=$travelerID&travelID=".$videos[0]->getParentTravelLog()->getTravelID()."&travelLogID=".$videos[0]->getParentTravelLog()->getTravelLogID()."&videoID=".$videos[0]->getVideoID()."&type=journal&jeVideos=1";
							else:
								$url = "/video.php?action=getTravelerVideos&travelerID=$travelerID&travelID=".$videos[0]->getParentTravelLog()->getTravelID()."&travelLogID=".$videos[0]->getParentTravelLog()->getTravelLogID()."&videoID=".$videos[0]->getVideoID()."&type=journal";
							endif;
						endif;
					else:
						if ('album' == $this->params['type']):
							$url = "/video.php?action=getGroupVideos&gID=$groupID&albumID=".$videos[0]->getParentAlbum()->getAlbumID()."&videoID=".$videos[0]->getVideoID()."&type=".$this->params['type'];
						else:
							if( $this->params['jeVideos'] ):
								$url = "/video.php?action=getGroupVideos&gID=$groupID&travelID=".$videos[0]->getParentTravelLog()->getTravelID()."&travelLogID=".$videos[0]->getParentTravelLog()->getTravelLogID()."&videoID=".$videos[0]->getVideoID()."&type=journal&jeVideos=1";
							else:
								$url = "/video.php?action=getGroupVideos&gID=$groupID&travelID=".$videos[0]->getParentTravelLog()->getTravelID()."&travelLogID=".$videos[0]->getParentTravelLog()->getTravelLogID()."&videoID=".$videos[0]->getVideoID()."&type=journal";
							endif;
						endif;
					endif;

					require_once('Class.StringFormattingHelper.php');
					require_once('Class.Template.php');
					$possessive_name = StringFormattingHelper::possessive($owner->getName());
					$siteContext = SiteContext::getInstance();
					$site_name = isset($GLOBALS["CONFIG"]) ? $siteContext->getSiteName()." Network" : "GoAbroad Network";
					$fbMetaTags = array(	'<meta name="title" content="'.$possessive_name.' '.$videos[0]->getTitle().' video"/>',
											'<meta property="og:title" content="'.$possessive_name.' '.$videos[0]->getTitle().' video"/>',
											'<meta property="og:site_name" content="'.$site_name.'"/>');
					Template::clearFBRequiredMetaTags();
					Template::hideProfileFBLike();
					Template::setFBRequiredMetaTags($fbMetaTags);

					//for sharebar
					$contents = array(	"permalink"	=>	"http://".$_SERVER["SERVER_NAME"].$url,
										"title"		=>	$videos[0]->getTitle()." video ",
										"name"		=>	"video");
					
					require_once("travellog/views/Class.FBLikeControlView.php");
					$fbLike = new FBLikeControlView();
					$fbLike->setContents($contents);
					$this->tpl->set("fbLike",$fbLike);
					
					require_once("travellog/views/Class.BookmarkControlsView.php");
					$sharebar = new BookmarkControlsView();
					$sharebar->setContents($contents);
					$sharebar->prepareURLParameters();
					$this->tpl->set("sharebar",$sharebar);
				} 
				else {
					if (!$isOwner) {
	                    switch ($context) {
	                    	case 'traveler':
	                    		$this->file_factory->invokeStaticClass('ToolMan', 'redirect', array('/'.$this->traveler->getUsername()));
	                    		break;
	                 		case 'group':
	                 		   	$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/groups/'.$this->group->getName()));	
	                 			break;
	                    }
					}
				}
				        
	            if (0 < $this->params['videoID']) {
 	            	$videoUrl 						= $this->params['videoUrl'];
 	            	$videoTitle						= $this->params['videoTitle'];
 	            	$videoCaption					= $this->params['videoCaption']; 
 	            	$playVideoTemplate['videoUrl']  = $this->params['videoUrl'];

					$ownerID = ( $owner instanceof Traveler ) ? $owner->getTravelerID() : $owner->getID();

					$commentsview = $this->file_factory->getClass('CommentsView');
					$commentsview->setContext(CommentType::$VIDEO, $this->params['videoID']);
					$commentsview->setTravelerID($this->loggedTraveler->getTravelerID());
					$commentsview->setOwnerID($ownerID);
					$commentsview->setEnvURL($_SERVER['REQUEST_URI']."^comment_form");
					$commentsview->readComments();
 	            }
				else{
					$commentsview = null;
				}
				
				$collectionsListTemplate['isOwner']           = $isOwner;
 	            $collectionsListTemplate['albumCollections']  = $videoAlbums;
 	            $collectionsListTemplate['travelCollections'] = $travels;
 	            $collectionsListTemplate['collectionID']      = $collectionID;
 	            
 	            if (0 < $this->params['albumID'])
 	            	$collectionsListTemplate['albumID']  = $this->params['albumID'];
 	            else if (0 < $this->params['travelID'])
 	            	$collectionsListTemplate['travelID'] = $this->params['travelID'];
 	            
	            $mainTemplateVariables = array(
	            	'profile'          		 => $profile_comp->get_view(),
 	                'collections'            => $collectionsListTemplate,
 	                'playVideo'              => $playVideoTemplate,
 	                'videosList'             => $videosListTemplate,
	                'collectionTitle'        => ($collection) ? $collection->getTitle() : '',
 	                'cntCollections'         => count($videoAlbums) + count($travels),
 	                'cntTravelAlbums'		 => count($travels),
 	                'cntVideoAlbums'		 => count($videoAlbums),
					'hasVideos'              => $hasVideos,
 	                'isOwner'                => $isOwner,
 	                'video'					 => $this->video,
					'journalEntries'		 => $journalEntries,
					'albumID'                => (!$this->params['albumID']) ? $collectionID : $this->params['albumID'],
 	                'videoID'				 => $this->params['videoID'],
					'travelID'               => $this->params['travelID'],
 	                'type'					 => $this->params['type'],
 	                'hasCreatedAnAlbum'      => $this->params['hasCreatedAnAlbum'],
 	                'hasEditedAnAlbum'       => $this->params['hasEditedAnAlbum'],
 	                'hasDeletedAnAlbum'      => $this->params['hasDeletedAnAlbum'],
 	                'hasFailedSavingAnAlbum' => $this->params['hasFailedSavingAnAlbum'],
 	                'hasCreatedAVideo'       => $this->params['hasCreatedAVideo'],
 	                'hasEditedAVideo'        => $this->params['hasEditedAVideo'],
 	                'hasDeletedAVideo'       => $this->params['hasDeletedAVideo'],
 	                'fail'					 => $this->params['fail'],
					'commentsview'			 => $commentsview
	            );
	            
	            if ('traveler' == $context) {
	            	$mainTemplateVariables['travelerID'] = $this->params['travelerID'];
	            	$mainTemplateVariables['cntTravels'] = count($this->traveler->getTravels(0,1));
	            }
	            else if ('group' == $context) {
	            	$mainTemplateVariables['groupID']	 = $this->params['groupID'];
	            	$mainTemplateVariables['cntTravels'] = count($travels);
	            	$mainTemplateVariables['copying_notice_component'] = new GanetGroupCopyingNoticeComponent($this->group);
	            }
	            
	            if ('journal' == $this->params['type']) {
		          	if (isset($_REQUEST['travelLogID']))
		            	$mainTemplateVariables['travelLogID']	= $this->params['travelLogID'];
	            	else
	            		$mainTemplateVariables['travelLogID']	= (1 == count($journalEntries)) ? $journalEntries[0]->getTravelLogID() : 0;	
	            }
	            
 	            $this->setTemplateVariables($mainTemplateVariables);
 	            $playVideoTemplate->setTemplate('travellog/views/tpl.IncPlayVideo.php');
 	            $videosListTemplate->setTemplate('travellog/views/tpl.IncViewVideosHorizontalList.php');
 	            $collectionsListTemplate->setTemplate('travellog/views/tpl.IncViewCollectionsList.php');

				
/*
 	            Template::includeDependentJs('/js/video.js',array('bottom' => true));
				Template::includeDependentJs('/js/videoAlbum.js',array('bottom' => true));
				Template::includeDependentJs('/js/contentSlider.js',array('bottom' => true));
				Template::includeDependentJs('/js/collectionPaginatedList.js',array('bottom' => true));
*/
				Template::includeDependentJs('/min/g=ViewVideo',array('bottom' => true));
				
				
				Template::setMainTemplateVar('layoutID', 'page_photos');
				
 	            if ('traveler' == $context)
 	            	$this->tpl->out('tpl.ViewTravelerVideos.php');
 	            else{
					$this->tpl->out('tpl.ViewGroupVideos.php');
				}
	        }
	        
	        /** saves video **/
	        
	        function saveVideo() {
	        	$title = (get_magic_quotes_gpc()) ? trim(stripslashes($this->params['videoTitle'])) : trim(htmlentities(strip_tags($this->params['videoTitle']), ENT_NOQUOTES));
	        	$title = str_replace('@', '&', $title);
	        	
	        	$caption = (get_magic_quotes_gpc()) ? trim(stripslashes($this->params['videoCaption'])) : trim(htmlentities(strip_tags($this->params['videoCaption']), ENT_NOQUOTES));
	        	$caption = str_replace('@', '&', $caption);
	        	
	        	$details = VideoHelper::getInstance()->getVideoAttributes(str_replace('@', '&', str_replace('~', '#', $this->params['videoUrl'])));
	        	$this->video->setVideoUrl($details['url']);
	        	$this->video->setVideoImageUrl($details['thumbnail']);
	        	$this->video->setDuration($details['duration']);
	        	$this->video->setCaption($caption);
	        	$this->video->setTitle($title);
	        	
	        	switch ($this->params['type']) {
	        		case 'album':
	        			$this->video->setParentAlbum($this->album);
	        			$this->album->setLastDateUpdated(date('Y-m-d h:i:s'));
	        			$this->album->update();
	        			break;
	        		case 'journal':
	        			$this->video->setParentTravelLog($this->travelLog);
	        			break;
	        	}
	        	
	        	$reload = 'video.php?action=';
			    
		        if ($this->params['groupID'])
	        		$reload .= 'getGroupVideos&gID='.$this->params['groupID'];
	        	else if ($this->params['travelerID'])
	        		$reload .= 'getTravelerVideos&travelerID='.$this->params['travelerID'];
	        	
	        	if ($this->params['albumID'])
        			$reload .= '&type=album&albumID='.$this->params['albumID'];
        		else if ($this->params['travelLogID'] || $this->params['travelID']) {
        			$reload .= '&type=journal&travelID='.$this->params['travelID'];
        			
        			if ($this->params['travelLogID']) 
        				$reload .= '&jeVideos=1&travelLogID='.$this->params['travelLogID'];
        		}
	        	
	        	if ('' != $details['thumbnail'] && '00:00' != $details['duration']) {
		        	if (!$this->params['videoID']) {
		        		$this->video->setDateAdded(date('Y-m-d H:i:s'));
		 	            $this->video->create();
						
						// added by nash - March 22, 2010
						// twitter widget observer
						require_once('travellog/controller/twitterWidget/Class.WidgetPostObserver.php');
						$url = '/'.$reload.'&videoID='.$this->video->getVideoID();
						WidgetPostObserver::post($url);
						
		 	            $reload .= '&hasCreatedAVideo=1';
		        	}
		        	else {
		        		$this->video->update();
		        		$reload .= '&hasEditedAVideo=1';
		        	}	
		        	$this->params['videoID'] = $this->video->getVideoID();	
		        	$reload .= '&videoID='.$this->params['videoID'];	
	        	}
	        	else {
	        		if ($this->params['videoID'])
	        			$reload .= '&videoID='.$this->params['videoID'];
	        		$reload .= '&fail=1';
	        	}
	        	
	        	/** value of reload is modified if video has been added as first video of a journal entry in the journal entry page **/
	        	if ('' != $this->params['redirect']) {
	        		if (1 < count($this->travelLog->getTravelLogVideos())) {
	        			if ($this->params['groupID'])
	                		$reload = '/video.php?action=getGroupVideos&gID='.$this->params['groupID'];
	                	else
	                		$reload = '/video.php?action=getTravelerVideos&travelerID='.$this->params['travelerID'];
	                	$reload .= '&travelID='.$this->params['travelID'].'&travelLogID='.$this->params['travelLogID'].'&videoID='.$this->params['videoID'].'&type=journal&jeVideos=1';		
	        		}
	        		else
	        			$reload = '/'.str_replace('@', '&', $this->params['redirect']);
	        			
	        		if ('' == $details['thumbnail'] || '00:00' == $details['duration'])
	        			$reload .= '&fail=1';
	        	}
				
	        	$this->file_factory->invokeStaticClass('ToolMan','redirect',array($reload));
	        }
	        
	        /** deletes video **/
	        
	        function deleteVideo() {
	        	$videos = ($this->params['albumID']) ? $this->album->getVideos() : $this->travelLog->getTravelLogVideos();
	        	
	        	if ($this->video->isPrimaryVideo()) {
	        		if (1 < count($videos)) {
	        			$newPrimaryVideoID = ($this->video->getVideoID() != $videos[0]->getVideoID()) ? $videos[0]->getVideoID() : $videos[1]->getVideoID();
	        			$newPrimaryVideo = ($this->params['albumID']) ? $this->file_factory->getClass('VideoAlbumToVideo', array($newPrimaryVideoID)) : $this->file_factory->getClass('TravelLogToVideo', array($newPrimaryVideoID));
	        			$newPrimaryVideo->setAsPrimaryVideo(1);
 	                    $newPrimaryVideo->update();
	        		}
	        		else {
	        			/** redirects to another journal entry if there are no longer videos for the curretn journal entry **/
	        			if ( !$this->params['albumID']) {
	        				$this->video->delete();
	        				$location = 'video.php?action=';
	        				$location .= ($this->params['groupID']) ? 'getGroupVideos&gID='.$this->params['groupID'] : 'getTravelerVideos&travelerID='.$this->params['travelerID'];
	        				$location .= (0 < count($this->travel->getTravelVideos())) ? '&type=journal&travelID='.$this->params['travelID'] : '';
	        				$location .= '&hasDeletedAVideo=1';
	        				$this->file_factory->invokeStaticClass('ToolMan','redirect',array($location));
 	                        exit; 
	        			}
	        		}
	        	}
	        	else
	        		$this->params['videoID'] = ($this->params['albumID']) ? $this->video->getParentAlbum()->getPrimaryVideo()->getVideoID() : $this->video->getParentTravelLog()->getPrimaryVideo()->getVideoID();
	        	
	        	$this->video->delete();
	        	$location = '/video.php?action=';
	        	$location .= ($this->params['groupID']) ? 'getGroupVideos&gID='.$this->params['groupID'] : 'getTravelerVideos&travelerID='.$this->params['travelerID'];
	        	$location .= ($this->params['albumID']) ? '&type=album&albumID='.$this->params['albumID'] : '&type=journal&travelID='.$this->params['travelID'];
	        	
	        	if ($this->params['jeVideos']) {
 	                if ($this->params['travelLogID'])
 	                	$location .= '&travelLogID='.$this->params['travelLogID'];
 	                $location .= '&jeVideos='.$this->params['jeVideos'];
 	            }
 	            
 	            $location .= '&hasDeletedAVideo=1';
 	            $this->file_factory->invokeStaticClass('ToolMan','redirect',array($location));
	        }
	        
	        /** plays video in thickbox **/
	        
	        function playVideo($type = '') {
	        	$obj_template 			   = $this->file_factory->getClass('Template');
 	            $obj_video    			   = ('entry' == $type) ? $this->file_factory->getClass('TravelLogToVideo', array($this->params['videoID'])) : $this->file_factory->getClass('VideoAlbumToVideo', array($this->params['videoID']));
 	            $obj_template['video_url'] = $obj_video->getVideoUrl();
 	            echo $obj_template->fetch('travellog/views/tpl.IncMiniVideoPlayer.php');
	        }
	        
	        /** displays prompt in thickbox **/
	        
	        function showPrompt() {
	        	$obj_template 				  = $this->file_factory->getClass('Template');
	        	$obj_template['travelID']	  = $this->params['travelID'];
	        	$obj_template['travelLogID']  = $this->params['travelLogID'];
	        	$obj_template['redirect'] 	  = $this->params['redirect'];
	        	
	        	if ($this->params['groupID'])
 	                $obj_template['groupID']  	= $this->params['groupID'];
 	            else
 	                $obj_template['travelerID'] = $this->params['travelerID'];
 	            echo $obj_template->fetch('travellog/views/tpl.IncVideoPopUp.php');
	        }
	        
	        /**
			 * Activates the __call magic function.
			 * 
			 * @return void
			 */
			public function __call($method, $arguments) {
				$actions = include(realpath(dirname(__FILE__).'/../../../').'/lib/services/video/actions/action.info.php');
				
				if (isset($actions[$method])) {
					$action = new $actions[$method]['class']($arguments[0]);
					if ($action->validate()) {
						$action->execute();
					}
					else {
						$action->onValidationFailure();
					}
				}
				else {
					include_once('FileNotFound.php');
					exit;
				}
			}
 	    }        