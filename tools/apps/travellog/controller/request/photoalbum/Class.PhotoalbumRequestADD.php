<?php
	/*
	 * Created on Nov 14, 2007
	 * Class.PhotoRequestVIEW.php
	 * 
	 * @author Joel C. Llano <joel.llano@goabroad.com> 
	 * @version: 
	 * @package: 
	 */

	require_once('travellog/model/Class.PhotoAlbum.php');
	require_once('travellog/service/PhotoalbumService.php');

	class PhotoalbumRequestADD{
	
		function __construct(FileFactory $FFactory){
		
			/*****************************************************
			 * last edit by neri: 11-18-08
			 * displayed the profile component on the template
			 *****************************************************/
		
			$error = NULL;		
		
			$template 		= $FFactory->getClass('Template');
			$Subnavigation 	= $FFactory->getClass('SubNavigation');
			
			$sessMan 		=	$FFactory->getClass('SessionManager');
			$travelerID 	= $sessMan->get('travelerID');
		
			$profile_comp = $FFactory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($_REQUEST['groupID']);
		
			$_REQUEST['travelerID'] = $travelerID;
		
			$PhotoAlbumService = new PhotoAlbumService();
			$PhotoAlbumService->setTemplateViews($template->getPath());
		
			$Subnavigation->setContext('GROUP');
			$Subnavigation->setContextID($_REQUEST['groupID']);
			$Subnavigation->setGroupType('ADMIN_GROUP');
			//$Subnavigation->setLinkToHighlight('GROUP_NAME');
			$Subnavigation->setLinkToHighlight('PHOTOS');
		
			/*
			//if not the owner/creator redirect 				
			if(!$PhotoAlbumService->isOwner($_REQUEST['groupID']))
					header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");													
			*/		
		
			require_once('travellog/model/Class.GroupFactory.php');
		
			$groupFactory = GroupFactory::instance();
		 	$Cgroup = $groupFactory->Create(array($_REQUEST['groupID']));
		
			if(in_array($travelerID,$Cgroup[0]->getStaff(true)) || $Cgroup[0]->getAdministrator()->gettravelerID() == $travelerID){
				if(isset($_POST['btnnext'])){												
					if(!strlen(trim($_POST['txttitle']))){																	
						$error = "Title of album is required";			
					}else{
						$datecreated	=getdate();
						$datecreated = $datecreated['year']."-".$datecreated['mon']."-".$datecreated['mday']." ".$datecreated['hours'].":".$datecreated['minutes'].":".$datecreated['seconds'];
					
						$PhotoAlbum = new PhotoAlbum();
						$PhotoAlbum->setTitle($_POST['txttitle']);
						$PhotoAlbum->setLastUpdate($datecreated);
						$PhotoAlbum->setGroupID($_GET['groupID']);
						$PhotoAlbum->setCreator($_REQUEST['travelerID']);
						$PhotoAlbum->Create();																	
						$photoalbumID =  $PhotoAlbum->getPhotoAlbumID();					//current photoalbumID
						//header("Location:http://".$_SERVER['SERVER_NAME']."/photomanagement.php?cat=photoalbum&action=add&groupID=".$_GET['groupID']."&genID=".$photoalbumID);
						header("Location:http://".$_SERVER['SERVER_NAME']."/photoalbum.php?action=view&groupID=".$_GET['groupID']);
					}	
				}
			
				$template->set('subNavigation',$Subnavigation);
				$template->set('profile', $profile_comp->get_view());
				$template->set('error',$error);				
				echo $template->out('tpl.AddPhotoAlbum.php');
		
			}
			else{
				header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
			}		
		}	
	}
?>