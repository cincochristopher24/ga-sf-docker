<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/PhotoService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");

class PhotoRequestSETPRIMARY{
	
	function __construct(FileFactory $FFactory){
		$template 		= $FFactory->getClass('Template');
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		$sessMan =	$FFactory->getClass('SessionManager');
		$travelerID = $sessMan->get('travelerID');
		
		
		$_REQUEST['travelerID'] = $travelerID;
		
		
		$PhotoService = new PhotoService();
		$PhotoService->setTemplateViews($template->getPath());
		$PhotoService->setAsPrimary($_REQUEST['photoid']);
	}
	
}


?>
