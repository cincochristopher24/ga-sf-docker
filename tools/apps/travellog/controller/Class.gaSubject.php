<?php
require_once("travellog/controller/Class.gaISubject.php");
class gaSubject implements gaISubject{
	private $observers = array();
	
	function registerObserver(gaIObserver $observer){
		$this->observers[get_class($observer)] = $observer;  
	}
	
	function removeObserver(gaIObserver $observer){
		$this->observers[] = $observer;
	}
	
	function notifyObservers(gaAbstractEvent $evt){
		foreach( $this->observers as $observer ){
			$observer->notify($evt); 
		}
	}
}
?>
