<?php
	//This controller maybe accessed through the group homepage or through the dashboard.
	//If it has been accessed through the group homepage, only alerts that belongs to the current group, may be viewed by the user.
	
	/***
	 * RULES:
	 * 		There is only one context for alerts and it is the GROUP context.
	 * 		Public here are the GAnet members who are not members of the group
	 * 		Only the group administrator can use these alertManagement page so no need to use the groupPrivacyPreference class.
	 */
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	abstract class AbstractAlertManagementController
	{
		protected $file_factory 	= NULL;
		protected $obj_session  	= NULL;
		protected $mLoggedTraveler 	= NULL;
		protected $mSubjectGroup	= NULL;	
		protected $mFilter			= NULL;
		protected $mMethod			= NULL;
		
		public function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('HtmlHelpers', array('path'=>''));
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('Paging',array('path'=>''));
			$this->file_factory->registerClass('ObjectIterator',array('path'=>''));
			$this->file_factory->registerClass('MessageSpace');
			$this->file_factory->registerClass('AlertMessage');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
		}
		
		public function performAction(){
			//figure out what is the desired action
			//the user must be logged
			//$travelerID = (isset($_SESSION) && array_key_exists('travelerID',$_SESSION) && $_SESSION['travelerID'] !=0 ? $_SESSION['travelerID']:0 );
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));		
			}
			try{
				$this->mLoggedTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			}
			catch(exception $e){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($travelerID,'TRAVELER');
			
			//set the method 
			$this->mMethod = (isset($_GET['method']))?$_GET['method']:"viewAll"; 
			$factory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			
			if(isset($_GET['groupID']) || isset($_POST['groupID'])){
				$localGroupID = (isset($_GET['groupID']))?$_GET['groupID']:$_POST['groupID'];
				//if the user has logged in, check if he/she is the administrator of the current group
				try{
					$mGroup = $factory->create(array($localGroupID));
					$this->mSubjectGroup = $mGroup[0];
				}
				catch(exception $e){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				if($this->mSubjectGroup->getAdministrator()->getTravelerId() != $this->mLoggedTraveler->getTravelerID() 
					&& !$this->mSubjectGroup->isModerator($this->mLoggedTraveler->getTravelerID())
					&& !$this->mSubjectGroup->isStaff($this->mLoggedTraveler->getTravelerID())
				){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
			}
			else{
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));	
			}
			if(isset($_GET['filter']) || isset($_POST['filter'])){
				$this->mFilter = (isset($_POST['filter']))?$_POST['filter']:$_GET['filter'];
			}
			else{
				$this->mFilter = "all";
			}
			//var_dump($this->mSubjectGroup);
			$this->{$this->mMethod}();
		}
		
		protected function viewAll(){
			//if groupId is set, get the alerts of the group.						
			//get the sendableId of the current group
			$sendableID = $this->mSubjectGroup->getSendableID();
			$msgSpace = $this->file_factory->getClass('MessageSpace'); 
			$alertMessages = $msgSpace->getAlerts($sendableID,$this->mFilter);
			for($i=0;$i<count($alertMessages);$i++){
				//$alertMessages[$i]->setText(formatString($alertMessages[$i]->getText()));
				$alertMessages[$i]->setText(htmlentities($alertMessages[$i]->getText()));
			}
			$totalRecords = $msgSpace->getTotalRecords();			
 			$qryString = "method=viewAll&groupID=".$this->mSubjectGroup->getGroupID()."&filter=".$this->mFilter;
 			
 			$page = (isset($_GET['page']))?$_GET['page'] : 1;
			//*********
			$paging = $this->file_factory->getClass('Paging',array($totalRecords,$page,$qryString,10));
 			$msgIterator = $this->file_factory->getClass('ObjectIterator',array($alertMessages,$paging));
			//*********
			$this->showTemplate(array(
				'alertMessages' => $alertMessages,
				'filter'		=> $this->mFilter,
				'numRec'		=> $totalRecords,
				'paging'		=> $paging,
				'msgIterator'	=> $msgIterator
			));
		}
		
		protected function compose(){
			//check if groupID is defined and valid, and check if the user traveler is the administrator of the group
			$this->showTemplate(array(
				'filter'		=> $this->mFilter,
				'alertMessage'	=> '',
				'expirationDate'=> ''
			));
		}
	
		protected function save(){
			if(isset($_POST['submit'])){
				//validate the inputs
				$errCodes = array();
				if(!$this->file_factory->invokeStaticClass('ToolMan','isValidDate',array($_POST['txtExpirationDate']))){
					$errCodes[] = "1";
				}				
				if(!strlen($_POST['txaAlertMessage'])){
					$errCodes[]="2";	
				}
				if(!count($errCodes)){									
					require_once "travellog/model/Class.AlertMessage.php";
					require_once "travellog/model/Class.Traveler.php";
					$arGroup = array($this->mSubjectGroup);
					$alert = $this->file_factory->getClass('AlertMessage');
					$alert->setExpiry(date("Y-m-d",strtotime($_POST['txtExpirationDate'])));
					$alert->setText($_POST['txaAlertMessage']);
					//var_dump($this->mSubjectGroup);
					//check if the logged traveler is the moderator of the group
					if($this->mSubjectGroup->getAdministratorID() == $this->mLoggedTraveler->getTravelerID()){
						//get the administered group of administrator
						if($this->mSubjectGroup->isSubGroup()){
							$alert->setSource($this->mSubjectGroup->getParent());
						}
						else{
							$alert->setSource($this->mSubjectGroup);
						}
					}
					elseif($this->mSubjectGroup->isStaff($this->mLoggedTraveler->getTravelerID())){
						require_once('travellog/model/Class.Staff.php');
						$alert->setSource(new Staff($this->mLoggedTraveler->getTravelerID(),$this->mSubjectGroup->getGroupID()));
					}
					elseif($this->mSubjectGroup->isModerator($this->mLoggedTraveler->getTravelerID())){
						require_once('travellog/model/Class.Moderator.php');
						$alert->setSource(new Moderator($this->mLoggedTraveler->getTravelerID(),$this->mSubjectGroup->getGroupID()));
					}
					else{
						$alert->setSource($this->mLoggedTraveler);
					}
					$alert->setDestination($arGroup);
					$alert->Send();
					
					require_once('travellog/model/Class.NotificationSetting.php');
					$notificationSetting = new NotificationSetting();
					$notificationSetting->setContext(NotificationSetting::GROUP_ALERT_CONTEXT);
					$notificationSetting->setContextID($alert->getMessageID());
					$notificationSetting->setMessageIncluded(array_key_exists('chkIncludeMsg',$_POST));
					$notificationSetting->setNotifyGroupMembers(array_key_exists('chkNotifyGroupMembers',$_POST));
					$notificationSetting->save();
					
					$qryStr = "groupID=".$this->mSubjectGroup->getGroupID()."&filter=".$this->mFilter;
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/alertmanagement.php?'.$qryStr));	
				}
				else{
					$this->showTemplate(array(
						'errCodes' 		=> $errCodes,
						'alertMessage' 	=>stripslashes($_POST['txaAlertMessage']),
						'expirationDate'=> stripslashes($_POST['txtExpirationDate']),
						'filter'		=> $this->mFilter,
						'method'		=> 'compose'
					));
				}
			}
		}
		
		protected function delete(){
			if(isset($_GET['attributeID'])){
				//if($alertMsg->getSource()->getSendableID() == $this->mLoggedTraveler->getSendableID()){
				if($this->file_factory->invokeStaticClass('MessageSpace', 'isOwner', array($_GET['attributeID'], $this->mSubjectGroup->getSendableID(), 'alert'), true)){
					$alertMsg = $this->file_factory->getClass('AlertMessage', array($_GET['attributeID']));

					$msgSpace = $this->file_factory->getClass('MessageSpace');
					$msgSpace->delete($_GET['attributeID'], $alertMsg->getDiscriminator(), $alertMsg->getMessageId());
					/***
					if(array_key_exists('debug',$_GET)){
						GaLogger::create()->performShutDown();
						exit;
					}
					***/
					$qryStr = 'groupID='.$this->mSubjectGroup->getGroupID().'&filter='.$this->mFilter;
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/alertmanagement.php?'.$qryStr));
				}
				else{
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));					
				}
			}
			else{
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));				
			}
		}
		
		protected function showTemplate($tlpVars=array()){
			
			$localGroupType = ($this->mSubjectGroup->getDiscriminator() == GROUP::FUN)?'FUN_GROUP':'ADMIN_GROUP';	
			$subNavigation = $this->file_factory->getClass('SubNavigation'); 
			$subNavigation->setContext('GROUP');
			$subNavigation->setContextID($this->mSubjectGroup->getGroupID());
			$subNavigation->setGroupType($localGroupType);
			$subNavigation->setLinkToHighlight('ALERTS');
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tplAlertManagement = $this->file_factory->getClass('Template'); 
			$tplAlertManagement->set_path('travellog/views/');
	
			$tplAlertManagement->set('subNavigation',$subNavigation);
			$tplAlertManagement->set('groupID',$this->mSubjectGroup->getGroupID());
			$tplAlertManagement->set('method',$this->mMethod);
			
			foreach($tlpVars as $key => $val){
				$tplAlertManagement->set($key,$val);
			}
			$tplAlertManagement->set('title','Alert Management');
			$tplAlertManagement->out('tpl.AlertManagement.php');
		}
	}
	
?>