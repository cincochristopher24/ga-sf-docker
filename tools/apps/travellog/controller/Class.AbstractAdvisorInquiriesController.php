<?php
	/*
	 * Class.AbstractAdvisorInquiriesController.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
	 
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Constants.php");
	require_once('travellog/helper/DisplayHelper.php');
	
	class AbstractAdvisorInquiriesController{
		
		static $file_factory = NULL;
		static $session_manager = NULL;
		static $aiHelper = NULL;
		static $traveler = NULL;
		
		
		function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass("SessionManager", array("path" => "travellog/model"));
			$this->session_manager = $this->file_factory->invokeStaticClass("SessionManager", "getInstance");
			
			if( !$this->session_manager->getLogin() ){
				header("location: login.php?failedLogin");
			}
			
			$this->file_factory->registerClass("AdminGroup", array("path" => "travellog/model"));
			
			if( !$gID = $this->file_factory->invokeStaticClass('AdminGroup','getAdvisorGroupID',array($this->session_manager->get('travelerID'))) )
				header('location: passport.php');
			
			$this->file_factory->registerClass("Listing", array("path" => "travellog/model"));
			$this->file_factory->registerClass("Client", array("path" => "travellog/model"));
			$this->file_factory->registerClass("RowsLimit", array("path" => "travellog/model"));
			$this->file_factory->registerClass("Traveler", array("path" => "travellog/model"));
			$this->file_factory->registerClass("HelpText", array("path" => "travellog/model"));
			$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass("AdvisorInquiriesHelper", array("path" => "travellog/helper"));
			$this->file_factory->registerClass("HybridPaging", array("path" => ""));
			$this->file_factory->registerClass("MainLayoutView", array("path" => "travellog/views/"));
			$this->file_factory->registerClass("AbstractView", array("path" => "travellog/views/"));
			$this->file_factory->registerClass("AdvisorInquiriesFcd", array("path" => "travellog/facade/"));
			$this->file_factory->registerClass("Connection", array("path" => ""));
			$this->file_factory->registerClass("Recordset", array("path" => ""));
			$this->file_factory->registerClass("FormHelpers", array("path" => ""));
			
			$this->aiHelper = $this->file_factory->invokeStaticClass("AdvisorInquiriesHelper", "getInstance");
			$this->traveler = $this->file_factory->getClass('Traveler', array($this->session_manager->get('travelerID')));
			$this->advisor = $this->file_factory->getClass("AdminGroup", array($gID));
			
			// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['HTTP_HOST'].'.php');
			
		}
		
		function performAction(){
			
			switch( $this->aiHelper->getAction() ){
				
				case constants::VIEW_MESSAGES_PER_STUDENT_AND_LISTING:
					$this->getAdminMessagesPerStudentAndListing();
					break;
				
				case constants::CHANGE_VIEW:
					if( constants::VIEW_BY_LISTING == $this->aiHelper->getAdvisorView() )
						$this->getListingsList();
					else
						$this->getStudentsList();
					break;		
					
				default:
					return $this->viewMessageList();	
			}
		}
		
		function retrieveStudentsData(){
			
			$aiData = $this->aiHelper->readAiData();
			$msgList = $this->advisor->getInquiryBox();
	 
	 		return $nContent = array(
				'obj_paging'		=>	$this->file_factory->getClass('HybridPaging',array($msgList->getTotalRecords(), $aiData['page'], '', $aiData['rowsperpage'], constants::MODE_AJAX, 'admin_messages', 'admincommpage.php')),				
				'listStudentID'	=>	$msgList->getStudents(0,$this->file_factory->getClass('RowsLimit',array($aiData['rowsperpage'],$aiData['page']-1))),
				'inquiryBox'	=>	$msgList,
				'changeView'	=>	true	
			);
			
		}
		
		function retrieveListingsData(){
			
			$aiData = $this->aiHelper->readAiData();
			$msgList = $this->advisor->getInquiryBox();
	 
	 		return $nContent = array(
				'obj_paging'	=>	$this->file_factory->getClass('HybridPaging',array($msgList->getTotalRecords(), $aiData['page'], '', $aiData['rowsperpage'], constants::MODE_AJAX, 'admin_messages', 'admincommpage.php')),				
				'listListingID'	=>	$msgList->getListings2(0,$this->file_factory->getClass('RowsLimit',array($aiData['rowsperpage'],$aiData['page']-1))),
				'inquiryBox'	=>	$msgList,
				'changeView'	=>	true	
			);
			
		}
		
		function viewMessageList(){
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			if( constants::VIEW_BY_STUDENT == $this->aiHelper->getAdvisorView() )
				$nContent = $this->retrieveStudentsData();
			else
				$nContent = $this->retrieveListingsData();	
			
			$errString = '';
			if( 0 >= $this->file_factory->invokeStaticClass('AdminGroup','getAdvisorClientID',array($this->session_manager->get('travelerID'))) )
			 	$errString = 'Permission denied in accessing this page.';
			
			$nContent = array_merge( $nContent, array(
				'errString'		=>	$errString,
				'groupID'		=>	$this->file_factory->invokeStaticClass('AdminGroup','getAdvisorGroupID',array($this->session_manager->get('travelerID'))),
				'subNavigation'	=>	$this->getSubNavigation(),
				'viewMode'		=>	$this->aiHelper->getAdvisorView(),
				'changeView'	=>	false 	
			));
			
			Template::includeDependentJs("/js/prototype.js");   
	 		Template::includeDependentJs("/js/moo.ajax.js");   
			Template::includeDependentJs("/js/student-communication.js");
			Template::includeDependentCss("/css/style_communication.css");  	
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );   
			
			$fcd = $this->file_factory->getClass('AdvisorInquiriesFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveMessageList()) );
			$obj_view->render();
		}
		
		function getSubNavigation(){
			
			// sub navigation
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext("TRAVELER");
			$subNavigation->setContextID($this->traveler->getTravelerID());
			$subNavigation->setLinkToHighlight('PARTICIPANT_INQUIRIES');
			
			return $subNavigation; 
		}
		
		function getListingsList(){
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			$fcd = $this->file_factory->getClass('AdvisorInquiriesFcd', $this->retrieveListingsData(), false);
			echo $fcd->retrieveListingsList()->render();
		}
		
		function getStudentsList(){
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			$fcd = $this->file_factory->getClass('AdvisorInquiriesFcd', $this->retrieveStudentsData(), false);
			echo $fcd->retrieveStudentsList()->render();
		}
		
		function getAdminMessagesPerStudentAndListing(){
			
			$content = $this->aiHelper->readAiMessageData(); 
			foreach( $this->advisor->getInquiryBox()->getAdminMessagesPerStudentAndListing($content['listingID'],$this->advisor->getSendableID()) as $msg ) 
			 	recurseReply($msg,$content['inquiryView']);
		
		}
		
	}
?>