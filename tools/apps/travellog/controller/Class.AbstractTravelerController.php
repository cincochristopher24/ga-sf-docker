<?php
require_once('travellog/model/Class.Traveler.php');
require_once("travellog/model/Class.RowsLimit.php");
require_once("Class.Paging.php");
require_once("Class.Template.php");
require_once("Class.ObjectIterator.php");
require_once ("travellog/model/Class.PrivacyPreference.php");
require_once('travellog/model/Class.HelpText.php');
require_once('travellog/model/Class.Featured.php');
require_once("travellog/model/admin/Class.FeaturedSection.php");
require_once('travellog/helper/Class.HelperTraveler.php');
abstract class AbstractTravelerController{
	
	protected 
	
	$travelerID,
	
	$rows         = 12,
	
	$offset       = 0,
	
	$view_type    = 0,
	
	$locationID   = 0,
	
	$query_string = NULL,
	
	$page         = 1,
	
	$main_tpl     = false,
	
	$isAdvisor    = false,
	
	$keywords     = NULL,
	
	$searchby     = 0,
	
	$IsLogin      = false;


	abstract function viewContents();	
	

	function __construct(){
		$this->main_tpl = new Template();
	}
	
	function viewMain(){
		$objTravelerSection       = new FeaturedSection(FeaturedSection::$TRAVELER);
		$inc_tpl                  = clone $this->main_tpl;
		$obj_travelers            = array();
		$travelers_count          = 0;
		$hometown_travelers_count = 0;
		$newest_traveler_count    = 0;
		$search_template          = NULL;
		$obj_help                 = new HelpText();
		$this->offset             = ($this->page*12)-12; 
		$this->rows               = 12;
		$obj_featured             = new Featured;
		$arr_featured_travelers   = $objTravelerSection->getFeaturedItems(); //$obj_featured->getFeaturedTraveler();  
		//$obj_rows_limit           = new RowsLImit(5, 0);
		$arr_popular_travelers    = Traveler::getMostPopularInRandomOrder();//Traveler::getMostPopular($obj_rows_limit);
		
		if ($this->IsLogin){
			$obj_traveler = new Traveler($this->travelerID);
			if ( $obj_traveler->getPresentLocation() != NULL ) $hometown_travelers_count = 1;
			if ($obj_traveler->isAdvisor()) $this->isAdvisor = true;
			$this->main_tpl->set( 'obj_friend' , $obj_traveler );
		} 
		$newest_traveler_count = Traveler::getCountNewestMembers();
		$travelers_count       = Traveler::getCountAllTravelers();

		$code = <<<BOF
<script type = "text/javascript">
//<![CDATA[
	
	var redirect = function(){
		if( document.getElementById('action').value != 'location' ){
			window.location.href = '/travelers.php?action='+document.getElementById('action').value;
			document.getElementById('COUNTRYSEARCH').style.display = 'none';
		}else{ 
			
			document.getElementById('COUNTRYSEARCH').style.display = 'block';
			if( $('locationID').options.length == 0){
				var newopt = new Option();
				$('locationID').disabled = true;
				newopt.text  = 'Loading. Please Wait...';
		        newopt.value = 0;
		        $('locationID').appendChild(newopt);
			}
			manager.getCountriesWithTravelers();
		}
	}
	
	var manager = new CountryLoaderManager();
	
	var redirectsearch = function(){
		window.location.href = '/travelers.php?action=search&searchby='+document.getElementById('searchby').value+'&keywords='+document.getElementById('keywords').value;
	}
	
	var redirect1 = function(){
		if( document.getElementById('locationID').value > 0 )
			window.location.href = '/travelers.php?action=current&locationID='+document.getElementById('locationID').value;
	}
//]]>
</script>
BOF;
		Template::includeDependentJs("/js/moo2.ajax.js");
		Template::includeDependentJs("js/utils/json.js");
		Template::includeDependentJs("/js/countryLoader.js");
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		Template::includeDependent($code);
	
		$inc_tpl->set( 'keywords' , $this->keywords );
		$inc_tpl->set( 'searchby' , $this->searchby );
		$search_template = $inc_tpl->fetch('travellog/views/tpl.FrmTravelerSearch.php');
	
		$this->main_tpl->set( 'travelers_count'          , $travelers_count                        );
		$this->main_tpl->set( 'obj_help'                 , $obj_help                               );
		$this->main_tpl->set( 'hometown_travelers_count' , $hometown_travelers_count               );
		$this->main_tpl->set( 'newest_traveler_count'    , $newest_traveler_count                  );
		$this->main_tpl->set( 'obj_present_locations'    , $this->getTravelersInPresentLocation()  );
		$this->main_tpl->set( 'search_template'          , $search_template                        );
		$this->main_tpl->set( 'isLogin'                  , $this->IsLogin                          );
		$this->main_tpl->set( 'incLocation'              , $this->getLocation()                    );
		$this->main_tpl->set( 'viewType'                 , $this->view_type                        );
		$this->main_tpl->set( 'currTravelerID'           , $this->travelerID                       );	
		$this->main_tpl->set( 'arr_featured_travelers'   , $arr_featured_travelers                 );
		$this->main_tpl->set( 'arr_popular_travelers'    , $arr_popular_travelers                  );
		
	} 
	
	private function getTravelersInPresentLocation(){
		if ( $this->IsLogin ){
			$obj_traveler = new Traveler($this->travelerID);
			if ( $obj_traveler->getPresentLocation() != NULL )
				return $obj_traveler->getOtherTravelersInPresentLocation();
		}else return NULL;
	}
	
	function getLocation(){
		if( $this->locationID ){
			require_once("travellog/model/Class.SearchTravelerInLocationService.php");
			$objNewMember = new SearchTravelerInLocationService();
			$col_country  = $objNewMember->performSearch();
			$selected     = NULL;
			if (count($col_country)){
				$Return = '<select name="locationID" id="locationID" onchange="redirect1()">';
				foreach($col_country as $obj_country){
					if( $this->locationID == $obj_country->getLocationID() ) $selected = "selected";
					$Return .= '<option value="'.$obj_country->getLocationID().'" '.$selected.'>'.$obj_country->getName().'</option>';
					$selected     = NULL;
				}
				if( $this->locationID == 0 ) $selected = "selected";
				$Return .= '<option value="0" '.$selected.'>- choose a country -</option>';
				$Return .= '</select>';
			}
		}else
			$Return = '<select name="locationID" id="locationID" onchange="redirect1()"></select>';
		return $Return;
	}
		
	function setTravelerID($travelerID){ $this->travelerID = $travelerID; }
	
	function setPage($page)            { $this->page       = $page;       }
	
	function setIsLogin($isLogin)      { $this->IsLogin    = $isLogin;    }
	
	function setViewType($viewType)    { $this->view_type  = $viewType;   }
	
	function setLocationID( $locationID ){}
	function setSearchBy  ( $searchby   ){}
	function setKeywords  ( $keywords   ){}
}
?>
