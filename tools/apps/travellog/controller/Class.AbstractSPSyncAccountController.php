<?php

require_once("travellog/factory/Class.FileFactory.php");
require_once("travellog/controller/Class.IController.php");

abstract class AbstractSPSyncAccountController implements IController{
	
	protected $file_factory;
	protected $sp_context;
	protected $redirectAfterLogin;
	
	protected $accountSynched = FALSE;
	
	abstract protected function autoLogin($travelerID=0);
	
	public function __construct(){
		$this->file_factory = FileFactory::getInstance();
		
 		$this->file_factory->registerClass("SessionManager", array("path" => "travellog/model/"));
 		$this->file_factory->registerClass('Session', array('path' => 'travellog/model'));
 		$this->file_factory->registerClass("Connection", array("path" => ""));
 		$this->file_factory->registerClass("Recordset", array("path" => ""));
	}
	
	public function redirect($location){
		header("location: ".$location);
		exit;
	}
	
	public function performAction(){
		if( !$this->isAjaxRequest()){
			$this->redirect("/");
		}
		
		$this->sp_context = isset($_POST["context"]) ? $_POST["context"] : NULL;
		if( !$this->isValidContext($this->sp_context) ){
			ob_end_clean();
			echo "UNRECOGNIZED_PROVIDER";
			exit;
		}
		
		$email = isset($_POST["email"]) ? $_POST["email"] : NULL;
		$password = isset($_POST["pwd"]) ? $_POST["pwd"] : NULL;
		
		require_once("travellog/model/Class.TravelerPeer.php");
		
		$travelerID = TravelerPeer::getTravelerIDByEmailAndPassword($email,$password);
		if( !$travelerID ){
			ob_end_clean();
			echo "INCORRECT_CREDENTIALS";
			exit;
		}
		
		$oauth_api = $this->syncAccount($travelerID,$this->sp_context);
		if( !$oauth_api ){
			ob_end_clean();
			echo "CREDENTIALS_NOT_FOUND";
			exit;
		}
		if( !$this->accountSynched ){
			ob_end_clean();
			echo "ACCOUNT_UNAVAILABLE";
			exit;
		}
		
		if( $this->autoLogin($travelerID) ){
			$oauth_api->clearCredentials();
		}else{
			$oauth_api->clearCredentials();
		}
		
		ob_end_clean();
		echo $this->redirectAfterLogin;
		exit;
	}
	
	protected function syncAccount($travelerID,$context){
		$oauth_api = OAuthAPIFactory::getInstance()->create($context);
		
		$credentials = $oauth_api->getCredentials();
		if( !$credentials ){
			return FALSE;
		}
		$credentials["registered_traveler_id"] = $travelerID;
		$this->accountSynched = $oauth_api->syncTravelerAccount($travelerID,$credentials["service_provider_userid"]);
		if( $this->accountSynched ){
			$oauth_api->storeCredentials($credentials);
		}
		return $oauth_api;
	}
	
	protected function isValidContext($context=NULL){
		return in_array($context,array("twitter","google","facebook"));
	}
	
	protected function isAjaxRequest(){
		if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) 
			&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
			return TRUE;
		}
		if(	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
			&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			return TRUE;
		}
		return FALSE;
	}
}