<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('travellog/model/Class.SessionManager.php');
require_once('travellog/factory/Class.FileFactory.php');

class AbstractOnlineApplicationController {
	
	protected $file_factory 	= NULL;
	protected $obj_session  	= NULL;
	protected $loggedTraveler 	= NULL;
	protected $mMethod			= NULL;
	protected $mGroups			= array();
	protected $mGroupID			= 0;
	protected $mGroup 			= null;
	protected $mOwnerID			= 0;
	protected $pageTitle		= 'Online Application';
	protected $mTemplate		= null;
	protected $selectedStep     = 1;
	protected $lastInsertedPersonalInfoID;
	protected $action_class_names = array();
	
	const	VIEW_FORM               = 'viewOnlineAppForm',
			VIEW_PERSONAL_INFO_FORM = 'viewPersonalInfoForm',
			VIEW_PROGRAM_INFO_FORM  = 'viewProgramInfoForm',
			VIEW_PERSONAL_STATEMENT_FORM = 'viewPersonalStatement',
			VIEW_DOCUMENTS_FORM     = 'viewDocumentsForm',
			VIEW_AGREEMENT_FORM     = 'viewAgreementForm',
			VIEW_BILLING_FORM       = 'viewBillingForm',
			SAVE_PERSONAL_INFO      = 'savePersonalInfo',
			SAVE_PROGRAM_INFO       = 'saveProgramInfo',
			SAVE_PERSONAL_STATEMENT = 'savePersonalStatement',
			SAVE_AGREEMENT          = 'saveAgreement',
			SAVE_BILLING_INFO       = 'saveBillingInfo',
			
			RUN_SCRIPT = 'populate';
	
	
	public function __construct(){
		
		$this->file_factory = FileFactory::getInstance();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
		
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory->setPath('CSS', '/css/onlineApplications/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		$this->file_factory->registerClass('ToolMan', array('path'=>''));
		$this->file_factory->registerClass('HelperGlobal', array('path'=>''));
		$this->file_factory->registerClass('Template', array('path'=>''));
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('SubNavigation');
		
	//	$this->file_factory->registerClass('CISOnlineAppFieldsMapper',      array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('PersonalInfo',         array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('AddressInfo',          array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('PassportDetails',      array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('EmergencyContact',     array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('SchoolInfo',           array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('AdditionalInfo',       array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('ProgramInformation',   array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('PersonalInfoToProgram',array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('BillingInfo',          array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('PersonalStatement',    array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('Agreement',            array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		
		$this->file_factory->registerClass('Country');
	}
	
	public function performAction(){
		$this->action = (isset($_GET['action']) ? $_GET['action'] : '');
		$travelerID = $this->obj_session->get('travelerID');
	//	if(!$travelerID){
	//		$this->file_factory->invokeStaticClass('HelperGlobal', 'getLoginPage');
		//	$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));		
	//	}
		// https://www.ecsi.net/cgi-bin/webx.exe
		//require_once('gaLogs/Class.GaLogger.php');
		//GaLogger::create()->start($travelerID,'TRAVELER');
	//	var_dump($_GET);
	//	var_dump($_POST); exit;
		
		$this->lastInsertedPersonalInfoID = array('PersonalInfoID' => (isset($_GET['pID']) ? $_GET['pID'] : ''));
	//	var_dump($this->action);
	//	var_dump(CISOnlineAppFieldsMapper::$personalInfoFieldMap);
		switch($this->action){
			case self::VIEW_PERSONAL_INFO_FORM:
				$this->selectedStep = 1;
				$this->action_class_names = array('PersonalInfo', 'AddressInfo', 'PassportDetails', 'EmergencyContact', 'SchoolInfo', 'AdditionalInfo');
				$this->mTemplate = 'tpl.personalInformationForm.php';
				$this->showTemplate();
				break;
			case self::VIEW_PROGRAM_INFO_FORM:
				$this->selectedStep = 2;
				$this->action_class_names = array('ProgramInformation');
				$this->mTemplate = 'tpl.programInformationForm.php';
				$this->showTemplate();
				break;
			case self::VIEW_PERSONAL_STATEMENT_FORM:
				$this->selectedStep = 3;
				$this->action_class_names = array('PersonalStatement');
				$this->mTemplate = 'tpl.personalStatementForm.php';
				$this->showTemplate();
				break;
			case self::VIEW_DOCUMENTS_FORM:
				$this->selectedStep = 4;
				$this->mTemplate = 'tpl.documentsToMailForm.php';
				$this->showTemplate();
				break;
			case self::VIEW_AGREEMENT_FORM:
				$this->selectedStep = 5;
				$this->action_class_names = array('Agreement');
				$this->mTemplate = 'tpl.agreementAndWaiverForm.php';
				$this->showTemplate();
				break;
			case self::VIEW_BILLING_FORM:
				$this->selectedStep = 6;
				$this->action_class_names = array('BillingInfo', 'AddressInfo');
				$this->mTemplate = 'tpl.billingAndPaymentForm.php';
				$this->showTemplate();
				break;
			case self::SAVE_PERSONAL_INFO:
				$this->doPreparePersonalInfoValues();
				$this->redirect(self::VIEW_PROGRAM_INFO_FORM);
				break;
			case self::SAVE_PROGRAM_INFO:
				$this->doPrepareProgramInfoValues();
				$this->redirect(self::VIEW_PERSONAL_STATEMENT_FORM);
				break;
			case self::SAVE_PERSONAL_STATEMENT:
				$this->doPreparePersonalStatementValues();
				$this->redirect(self::VIEW_DOCUMENTS_FORM);
				break;
			case self::SAVE_AGREEMENT:
				$this->doPrepareAgreementValues();
				$this->redirect(self::VIEW_BILLING_FORM);
				break;
			case self::SAVE_BILLING_INFO:
				$this->doPrepareBillingInfoValues();
				header("Location: https://www.ecsi.net/cgi-bin/webx.exe");
			//	$this->file_factory->invokeStaticClass('ToolMan','redirect',array('index.php'));
				break;
			
			case self::RUN_SCRIPT:
				$this->populateProgramsTable();
				break;
			
			
			default:
				$this->selectedStep = 1;
				$this->mTemplate = 'tpl.personalInformationForm.php';
				$this->showTemplate();
			//	$this->showDefault();
				break;
		}
	}
	
	public function redirect($action){
		if(!isset($_GET['saveAndExit'])){
			$personalInfoID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?action='.$action.'&pID='.$personalInfoID));
		}
		else {
			//$this->showDefault();
			$this->file_factory->invokeStaticClass('ToolMan','redirect', array('index.php'));
		}
		
	}
	
	public function doPreparePersonalInfoValues(){
		// first page is divided into 5 parts
		
		// save first the personalInfo part ** note: passport details is included here
		$personalInfo = $_POST['PersonalInfo'];
		$this->doSavePersonalInfo($personalInfo);
		
		// save student address details ** note: i separated this for more convenience
		$addressInfo = $_POST['AddressInfo'];
		$this->doSaveAddressInfo($addressInfo);
		
		
		// save passport details
		$this->doSavePassportDetails($_POST['PassportInfo']);
		
		// save emergency contact
		$emergencyContact = $_POST['EmergencyContact'];
		$this->doSaveEmergencyContact($emergencyContact);
		
		// save last attended school details/information
		$schoolInfo = $_POST['SchoolInfo'];
		$this->doSaveSchoolInfo($schoolInfo);
		
		// save additional information
		$additionalInfo = array_merge($_POST['AdditionalInfo'], $_POST['SpecialNeedsInfo']);
		$this->doSaveAdditionalInfo($additionalInfo);
		
	}
	
	public function doSavePersonalInfo(array $personalInfo){
		if(!empty($personalInfo)){
			$birthday = array('Birthday' => $personalInfo['yyyy'].'-'.$personalInfo['mm'].'-'.$personalInfo['dd']);
			$travelerID = array('TravelerID' => $this->obj_session->get('travelerID'));
			$personalInfoMerged = array_merge($birthday, $travelerID, $personalInfo);
			$personalInfoObj = $this->file_factory->getClass('PersonalInfo');
			$personalInfoObj->setValues($personalInfoMerged);
			
		//	var_dump($_POST); exit;
			if ($_POST['Submit']){
				$personalInfoObj->save();
			}
			else if ($_POST['Edit']){
				$personalInfoObj->setID($_POST['pID']);
				$personalInfoObj->update();
			}
			
			
			$this->lastInsertedPersonalInfoID = array('PersonalInfoID' => $personalInfoObj->getID());
			
		}
		
		
	}
	
	public function doSaveAddressInfo(array $addressInfo){
		if(!empty($addressInfo)){
			$validUntil = array();
			$addressInfo = array_filter($addressInfo);
			
			if(isset($addressInfo['yyyy']) and isset($addressInfo['mm']) and isset($addressInfo['dd'])){
				$validUntil = array('ValidUntil' => $addressInfo['yyyy'].'-'.$addressInfo['mm'].'-'.$addressInfo['dd']);
			}
			
			$addressInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $addressInfo, $validUntil);

			$addressInfoObj = $this->file_factory->getClass('AddressInfo');
			$addressInfoObj->setValues($addressInfoMerged);
			
			if($_POST['Submit']){
				$addressInfoObj->save();
			}
			else if ($_POST['Edit']){
				$addressInfoObj->update();
			}
			
		}
		
		
		
	}
	
	public function doSavePassportDetails(array $passportInfo){
		if(!self::isEmpty($passportInfo)){
			$expiryDate = array('ExpiryDate' => $passportInfo['yyyy'].'-'.$passportInfo['mm'].'-00');

			$passportInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $passportInfo, $expiryDate);
			$passportDetailsObj = $this->file_factory->getClass('PassportDetails');
			$passportDetailsObj->setValues($passportInfoMerged);
			
			if($_POST['Submit']){
				$passportDetailsObj->save();
			}
			else if ($_POST['Edit']){
				$passportDetailsObj->update();
			}
		}
		
		
	}
	
	public function doSaveEmergencyContact(array $emergencyContact){
		if(!empty($emergencyContact)){
			$emergencyContactMerged = array_merge($this->lastInsertedPersonalInfoID, $emergencyContact);
			
			$addressInfo = array_merge(array(CISOnlineAppFieldsMapper::ADDRESS_TYPE => 2,
											CISOnlineAppFieldsMapper::PERMANENT_ADDRESS => 1), $emergencyContact );


			$this->doSaveAddressInfo($addressInfo);
			$emergencyContactObj = $this->file_factory->getClass('EmergencyContact');
			$emergencyContactObj->setValues($emergencyContactMerged);
			
			if($_POST['Submit']){
				$emergencyContactObj->save();
			}
			else if ($_POST['Edit']){
				$emergencyContactObj->update();
			}
		}
		
		
	}
	
	public function doSaveSchoolInfo(array $schoolInfo){
		if(!empty($schoolInfo)){
			$schoolInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $_POST['SchoolInfo']);
			$schoolInfoObj = $this->file_factory->getClass('SchoolInfo');
			$schoolInfoObj->setValues($schoolInfoMerged);
			
			if($_POST['Submit']){
				$schoolInfoObj->save();
			}
			else if ($_POST['Edit']){
				$schoolInfoObj->update();
			}
		}
		
		
	}
	
	public function doSaveAdditionalInfo(array $additionalInfo){
		if(!empty($additionalInfo)){
			$additionalInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $additionalInfo);
			$additionalInfoObj = $this->file_factory->getClass('AdditionalInfo');
			$additionalInfoObj->setValues($additionalInfoMerged);
			
			if($_POST['Submit']){
				$additionalInfoObj->save();
			}
			else if ($_POST['Edit']){
				$additionalInfoObj->update();
			}
		}
		
	}
	
	
	// ######## END OF PERSONAL INFORMATION SECTION #########
	
	// ######## PROGRAM INFORMATION SECTION ######	
	public function doPrepareProgramInfoValues(){
		// NOTE: when next is clicked in the in PERSONAL INFORMATION page, PersonalInfoID must be posted in url 
		// 
		
		$programInfo = $_POST['ProgramInfo'];
		$this->doSaveProgramInfo($programInfo);
	}
	// TODO:
	public function doSaveProgramInfo(array $programInfo){
		if(!empty($programInfo)){
			$programInfoMerged = array_merge($this->lastInsertedPersonalInfoID,$programInfo);
			$progInfo = $this->file_factory->getClass('ProgramInformation');
			$progInfo->setValues($programInfoMerged);
			
			if ($_POST['Submit']){
				$progInfo->save();
			}
			else if ($_POST['Edit']){
				$progInfo->update($progInfo->getPersonalInfoID());
			}
			
		//	var_dump($_POST);
			
		//	$toProgramValues = array_merge($this->lastInsertedPersonalInfoID,
		//		 							array(CISOnlineAppFieldsMapper::PROGRAM_INFO_ID  => $progInfo->getID()) );
			
			
		//	var_dump($_POST);
		//	var_dump($progInfo); 
		//	var_dump($toProgramValues);
		//	exit;
				
		//	$personalInfoToProgram = $this->file_factory->getClass('PersonalInfoToProgram');
		//	$personalInfoToProgram->setValues($toProgramValues);
			
		//	if($_POST['Submit']){
		//		$personalInfoToProgram->save();
		//	}
			
			
		}
		
		
		
	}
	// ######## END PROGRAM INFORMATION SECTION ######
	
	// ######## PERSONAL STATEMENT SECTION ######
	public function doPreparePersonalStatementValues(){
		
		$personalStatement = $_POST['PersonalStatement'];
		$this->doSavePersonalStatemet($personalStatement);
	}
	public function doSavePersonalStatemet(array $personalStatement){
		if(!self::isEmpty($personalStatement)){
			$statementMerged = array_merge($this->lastInsertedPersonalInfoID, $personalStatement);
			$statement = $this->file_factory->getClass('PersonalStatement');
			$statement->setValues($statementMerged);
			
			if($_POST['Submit']){
				$statement->save();
			}
			else if($_POST['Edit']){
				$statement->update();
			}
			
	//		var_dump($statement);
	//		exit;
		}
		
		
		
	}
	// ######## END PERSONAL STATEMENT SECTION ######
	
	// ###### AGREEMENT AND WAIVER SECTION ######
	public function doPrepareAgreementValues(){
	//	$CISOnlineAppFieldsMapper = $this->file_factory->invokeStaticVar('CISOnlineAppFieldsMapper', 'agreementFieldMap');
		
		$agreement = $_POST['Agreement'];
	//	$agreement[CISOnlineAppFieldsMapper::APPLICATION_DATE] = Date("Y-m-d H:i:s");
		$this->doSaveAgreement($agreement);
	}
	
	public function doSaveAgreement(array $agreement){
		if(!empty($agreement)){
			$agreementDate = array(CISOnlineAppFieldsMapper::APPLICATION_DATE => Date("Y-m-d H:i:s"));
			$agreementMerged = array_merge($this->lastInsertedPersonalInfoID, $agreement, $agreementDate);
			$agreementObj = $this->file_factory->getClass('Agreement');
			$agreementObj->setValues($agreementMerged);
			
			if ($_POST['Submit']){
				$agreementObj->save();
			}
			else if ($_POST['Edit']){
				$agreementObj->setLastEdited(Date("Y-m-d H:i:s"));
				$agreementObj->update();
			}
			
		}
		
		
		
	}
	// ###### AGREEMENT AND WAIVER SECTION ######
	
	// ###### BILLING AND PAYMENT SECTION #####
	public function doPrepareBillingInfoValues(){
//		$CISOnlineAppFieldsMapper = $this->file_factory->invokeStaticVar('CISOnlineAppFieldsMapper', 'billingInfoFieldMap');
		
		$billingInfo = $_POST['BillingInfo'];
		
		$cnt = 0;
		foreach($billingInfo[CISOnlineAppFieldsMapper::PAYING_PERSONS] as $person):
			$cnt = $cnt + (int)$person;
		endforeach;
		
		$billingInfo[CISOnlineAppFieldsMapper::PAYING_PERSONS] = $cnt;

		$this->doSaveBillingInfo($billingInfo);
	}
	
	public function doSaveBillingInfo(array $billingInfo){
		if(!empty($billingInfo)){
			$billingInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $billingInfo);
			
			$addressInfo = array_merge(array(CISOnlineAppFieldsMapper::ADDRESS_TYPE => 3,
											CISOnlineAppFieldsMapper::PERMANENT_ADDRESS => 1), $billingInfoMerged );
			
			$this->doSaveAddressInfo($addressInfo);
			$billing = $this->file_factory->getClass('BillingInfo');
			$billing->setValues($billingInfoMerged);
			
			if($_POST['Submit']){
				$billing->save();
			}
			else if ($_POST['Edit']){
				$billing->update();
			}
			
			
		}
		
		
	}
	
	public function showTemplate(){
		
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$this->file_factory->invokeStaticClass('Template','setMainTemplateVar', array('layoutID', 'online_application'));
		
		$tplOnlineApplication = $this->file_factory->getClass('Template');
		
		$applicationProgressMenu = $this->prepareApplicationProgressMenu();
		
		$countries = $this->prepareCountryList();
	//	var_dump($this->action);
	//	var_dump($this->mTemplate);
		
		if(isset($_GET['pID'])){
			$objArr = array();
			$personalInfoID = $_GET['pID'];
			foreach($this->action_class_names as $cls){
				$clsName = 'Class.'.$cls.'Mapper.php';
		//		var_dump($clsName);
				$file = dirname(__FILE__).'/../model/onlineApplications/CIS_onlineApplication/Mapper/'.$clsName;
				if(file_exists($file)){
		//			echo '<p>file exists</p>';
					require_once($file);
					$objArr[$cls] = call_user_func(array($cls.'Mapper', 'retrieveByPersonalInfoID'), $personalInfoID);
					
				//	var_dump($objArr[$cls]);
				}
				
			}
			
	//		var_dump($objArr['PersonalInfo'][0]->getEmail());
			
	//		var_dump($objArr);
	//		self::concatObjects($objArr);
	//		var_dump($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1());
	//		exit;
			// 0 when not empty
			// 1 when empty
			if(self::isEmpty($objArr) == 0){
				$tplOnlineApplication->set('buttonName','Edit');
				$tplOnlineApplication->set('objArr',$objArr);
			}
			
		}
		
		
		
		
		$tplOnlineApplication->set('pageTitle',$this->pageTitle);
		$tplOnlineApplication->set('pID',$this->lastInsertedPersonalInfoID['PersonalInfoID']);
		
		$tplOnlineApplication->set('applicationProgressMenu',$applicationProgressMenu);
		$tplOnlineApplication->set('countries',$countries);
		$tplOnlineApplication->out('travellog/views/onlineApplications/'.$this->mTemplate);
	}
	
	public function prepareCountryList(){
		$countryList = $this->file_factory->invokeStaticClass('Country','getCountryList');
		return $countryList;
	}
	
	public function prepareApplicationProgressMenu(){
		$progressMenu = $this->file_factory->getClass('Template');
		$progressMenu->set('selectedStep', $this->selectedStep);
		return $progressMenu->fetch('travellog/views/onlineApplications/tpl.applicationProgressMenu.php');

	}
	
	public function showDefault(){
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$this->file_factory->invokeStaticClass('Template','setMainTemplateVar', array('layoutID', 'online_application'));
		$tplOnlineApplication = $this->file_factory->getClass('Template');
		$tplOnlineApplication->out('travellog/views/onlineApplications/tpl.cisOnlineAppDefault.php');
		
	}
	
	// miscellaneous functions
	private static function isEmpty(array $array ){
		$my_not_empty = create_function('$v', 'return strlen($v) > 0;');
		return (count(array_filter($array, $my_not_empty)) == 0) ? 1 : 0;
		
	}
	
	private static function concatObjects(array $objArr){
		foreach($objArr as $key => $val){
			
			var_dump($val);
			
		}
	}
	
	// script function
	// populate program table
	
	private function populateProgramsTable(){
		$terms = array(
					'Summer' => 1,
					'Winter' => 2,
					'Fall'   => 3,
					'Spring' => 4,
					'Winter Trimester' => 5
					);
		
		
		$entries = array(
			'Bond Univers ity' => array($terms['Summer'], $terms['Fall'], $terms['Spring']),
			'LaTrobe University at Melbourne' => array($terms['Fall'], $terms['Spring']),
			'LaTrobe University at Bendigo'   => array($terms['Fall'], $terms['Spring']),
			'Edge of the Outback - Summer Photography Program' => array($terms['Summer']),
			'Macquarie University' => array()
			);
			
		foreach($programs as $key => $values){
			echo "<p>insert into programTable set name = ".$key."</p>";
			echo "<em>get last inserted id</em>";
			foreach($values as $value){
				echo "<p>insert into termToProgramTable set termID = ".$value.", progID = <em>lastinsertedID</em></p>";
			}
			
		}
	}
	
	
}
?>