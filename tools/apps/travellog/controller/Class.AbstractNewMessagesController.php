<?php
	
	/*
	 * Class.AbstractMessagesController.php
	 * Created on Nov 9, 2007
	 * created by marc
	 */
	 
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/helper/DisplayHelper.php");
	require_once("travellog/helper/Class.MessageHelper.php");
	require_once("travellog/model/Class.Message.php");
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/model/Class.GroupFactory.php");
	require_once("travellog/model/Class.MessageFactory.php");
	require_once("Class.Constants.php");
	
	require_once 'travellog/model/messageCenter/Class.gaMessage.php';
	require_once 'travellog/model/messageCenter/Class.gaMessageMapper.php';
	 
	abstract class AbstractMessagesController implements IController{
		
		protected $file_factory = NULL;
		protected $session_manager = NULL;
		protected $message_helper = NULL;
		protected $traveler = NULL;
		
		function __construct(){
			
			$this->session_manager = SessionManager::getInstance();
			$this->message_helper = MessageHelper::getInstance(); 
			
			if( !$this->session_manager->getLogIn() ){
				header("location: login.php?failedLogin&redirect=messages.php?" . $_SERVER['QUERY_STRING']);
				exit;			 
			}
	
			$this->file_factory = FileFactory::getInstance();
			//require_once("travellog_/domains/" . $_SERVER['SERVER_NAME'] . ".php");
			
			// models
			$this->file_factory->registerClass('RowsLimit', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('HelpText', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('InquiryMessage', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('PersonalMessage', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('SendableFactory', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('Staff', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('Advisor', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('Moderator', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('Listing', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('Client', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('MessagesFcd'  , array('path' => 'travellog/facade/'));
			$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('Paging', array('path' => ''));
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('Connection', array('path' => ''));
			$this->file_factory->registerClass('Recordset', array('path' => ''));

			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));

			$this->file_factory->registerClass('MessagesPanel', array('path' => 'travellog/views/'));
			
			// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->traveler = $this->file_factory->getClass('Traveler',array($this->session_manager->get('travelerID')));
			
			//start logger
			if($this->session_manager->get('travelerID')){
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($this->session_manager->get('travelerID'),'TRAVELER');
			}	
		}
		
		function performAction( ){
		
			if (isset($_GET['action']) && $_GET['action'] == 'compose') {
				$this->composeNewMessage();
			} elseif (isset($_GET['action']) && $_GET['action'] == 'markAsRead') {
				$this->markAsRead();
			} elseif (isset($_GET['action']) && $_GET['action'] == 'markAsUnread') {
				$this->markAsUnread();
			} elseif (isset($_GET['action']) && $_GET['action'] == 'delete') {
				$this->doDeleteMessage();
			} elseif (isset($_GET['action']) && $_GET['action'] == 'batchDelete') {
				$this->batchDelete();
			} elseif (isset($_GET['action']) && $_GET['action'] == 'batchMarkAsRead') {
				$this->batchMarkAsRead();
			} elseif (isset($_GET['action']) && $_GET['action'] == 'batchMarkAsUnread') {
				$this->batchMarkAsUnread();
			} elseif (isset($_GET['action']) && $_GET['action'] == 'replyToMessage') {
				$this->doReplyToMessage();
			} else {
				switch( $this->message_helper->getAction() ){
					
					// redirect to referer if "back" has been clicked
					case constants::BACK:
						$redirectPage = ( strlen($this->message_helper->getReferer()) ) ? $this->message_helper->getReferer() : $this->message_helper->getRedirectPage();
						header('location:' . $redirectPage );
						exit;
						break;
					
					// compose new message
					case constants::COMPOSE:
						return $this->composeMessage();
						break;
					
					// browse a message	
					case constants::VIEW:
						return $this->viewMessage();
						break;
						
					case constants::REPLY:
						return $this->replyToMessage();
						break;	
					
					case constants::SEND:
						return $this->sendMessage();
						break;	
					
					case constants::SAVE_AS_DRAFT:
						return $this->saveMessage();
						break;	
					
					case constants::MOVE_TO_TRASH:
						return $this->moveMessageToTrash();
						break;	
					
					case constants::DELETE:
						return $this->deleteMessage();
						break;	
					
					case constants::EDIT:
						return $this->editMessage();
						break;	
					
					case constants::INQUIRE:
						return $this->sendInquiry();
						break;
						
					default:
						//return $this->listMessagesOldWay(); // new message center using existing code
						return $this->listMessages();	//refactored code for new message center by Jonas
						//return $this->viewMessageList(); // old code of Marc (personal messages only)
				}
			}
			
			
			
		}
		
		function retrieveFolderMessages(){
			
			$nMsgList = array();
			$nContent = array(); 
			$arrListings = array();
			$folder = $this->message_helper->getFolder();
			$page = $this->message_helper->getCurrPage();
			$rowsPerPage = $this->message_helper->getRowsPerPage();
			$listingID = $this->message_helper->getListingID();
			
			// set needed variables
			switch( $folder ){
				// sent items
				case constants::SENT_ITEMS :
					$msgList = $this->traveler->getSentitems();
					$myFunc = 'getMessageList';
					break;
				
				// drafts
				case constants::DRAFTS :
					$msgList = $this->traveler->getDrafts();
					$myFunc = 'getDraftsList';
					break;
					
				// trash
				case constants::TRASH :	
					$msgList = $this->traveler->getTrash();
					$myFunc = 'getTrashList';
					break;
				
				// inquiry	
				case constants::INQUIRY :

					$msgList = $this->traveler->getInquiryBox();
					$arrListingID = $msgList->getListings1();
					
					$listingID = !in_array($listingID,$arrListingID) && count($arrListingID) ? $arrListingID[0] : 0; 
					
					$arrListings = array();
					if( count( $arrListingID ) ){
						foreach( $arrListingID as $nListingID )
							$arrListings[] = new Listing($nListingID);
					}
					
					$myFunc = 'getMessagesPerListing';
					break;		
					
				default :
					$msgList = $this->traveler->getInbox();
					$myFunc = 'getMessageList';
					break;	
			}
			
			// get message list
			$rowsLimit = $this->file_factory->getClass('RowsLimit',array($rowsPerPage,$page - 1),true);
			
			while( 0 < $page ){
				
				if( $folder == constants::INQUIRY )
					$nMsgList = $msgList->$myFunc($listingID,$rowsLimit);
				else
					$nMsgList = $msgList->$myFunc($rowsLimit);	
					
				if( count($nMsgList) )
					break;
				else{
					$page = ceil( $msgList->getTotalRecords()/$rowsPerPage );
					$rowsLimit = $this->file_factory->getClass('RowsLimit',array($rowsPerPage,$page-1),true);
				}
			} 
			
			$params = array( $msgList->getTotalRecords(), $page, $folder, $rowsPerPage );
			$paging = $this->file_factory->getClass('Paging',$params,true);
			$title = ( $folder == constants::SENT_ITEMS ) ? "sent items" : $folder;
			
			$nContent = array_merge($this->getMessageStats(),array(
				"title"			=>	$title . ' of ' .  $this->traveler->getTravelerProfile()->getUserName(),
				"messageList"	=>	$nMsgList,
				"active"		=>	$folder,
				"arrListings"	=>	( isset($arrListings) ) ? $arrListings : array(),
				"viewFolders"	=>	$this->getValidMessageFolders(),
				"ownerID"		=>	$this->traveler->getTravelerID(),
				"obj_paging"	=>	$paging,
				"arrListings"	=>	$arrListings,
				"subNavigation"	=>	$this->getSubNavigation(),
				'listingID'		=>	( isset($listingID) ) ? $listingID : 0,
				'msgList'		=>	( isset($msgList) ) ? $msgList : array(),
				'page'			=>	$page
			));
			 
			return $nContent; 
		}
		
		function composeMessage(){
			
			$errString = '';
			$destination = array();
			$lockDestination = false;
			$nDest = NULL;
			$recipient = $this->message_helper->getRecipient(); 
			$recipientID = $this->message_helper->getRecipientID();
			
			$factory =  GroupFactory::instance();
			if( $recipient == constants::TRAVELER && 0 < $recipientID ){
				
				try{
					$nDest = $this->file_factory->getClass("Traveler",array($recipientID));
					$destination[] = $nDest->getUserName();
					$lockDestination = true;
				}
				catch( Exception $e ){
					$errString = 'Unknown message destination.';
				}
			}
			elseif( $recipient == constants::GROUP && 0 < $recipientID ){
				
				try{
					$tempDest = $factory->create(array($recipientID));
					$nDest = $tempDest[0];
					$destination[] = $nDest->getName();
					if( $nDest->getAdministratorID() != $this->traveler->getTravelerID()) {
						if( 0 == count($destination) )
							$errString = 'Permission denied in creating message.';
						else
							$lockDestination = true;
					}
					 	
				}
				catch( Exception $e ){
					$destination = array();
				}
			} 
			
			$nContent = array(
					"active"			=>	constants::INBOX,
					"actionPage"		=>	( $recipient == constants::TRAVELER ) ? 'messages.php' : 'messages.php?group',
					"text"				=>	$this->message_helper->getTravelerSignature($this->traveler,$nDest),
					"recipientFlag"		=>	$recipient
			);
			
			if( !strlen($errString) ){
				$mySource = 0;
				if (0 != $this->message_helper->getSourceID()){
					$tempSource = $factory->create(array($this->message_helper->getSourceID()));
					$nSource = $tempSource[0];

					if( $nSource instanceof Group && $nSource->isStaff($this->traveler->getTravelerID()) )
						$mySource = $this->file_factory->getClass("Staff",array($this->traveler->getTravelerID(),$nSource->getGroupID()))->getSendableID() . "_sID";
					else
						$mySource = $this->message_helper->getSourceID();	
				}
				$nContent = array_merge($nContent,array(
					"lockDestination"	=>	$lockDestination,
					"destination"		=> 	$destination,
					"sourceID"			=>	$mySource
				));
			}
			else
				$nContent["errString"] = $errString;
			
			return $this->viewMessageForm($nContent);
			
		}
		
		function replyToMessage(){
			
			$errString = '';
			$nContent = array("title" => "reply");
			$messageFactory = MessageFactory::getInstance();
			
			if( $messageFactory->create($this->message_helper->getAttributeID()) && Message::IsOwner($this->message_helper->getAttributeID(),$this->traveler->getSendableID(),$this->message_helper->getFolder()) ){
				$nMessage = $messageFactory->create($this->message_helper->getAttributeID(), $this->traveler->getSendableID());
				if( $nMessage->getSource() instanceof Traveler ){
					if( 'Moderator' == get_class($nMessage->getSource()) )
						$destination = array( $nMessage->getSource()->getUserName() . ' [' . $nMessage->getSource()->getAdvisorGroup()->getName() . ']');
					else
						$destination = array($nMessage->getSource()->getUserName());
					$action = 'messages.php';
					$text = trim($nMessage->getSource()->getUsername()) . " wrote:  \n\n" . $nMessage->getText();
					$recipientFlag = constants::TRAVELER;
				}
				else{
					$destination = array($nMessage->getSource()->getName());
					$action = 'messages.php?group';
					$text = trim($nMessage->getSource()->getName()) . " wrote:  \n\n" . $nMessage->getText();
					$recipientFlag = constants::GROUP;
				}
				
				$tempDestReply = array_shift($nMessage->getDestination());
				$text .= $this->message_helper->getTravelerSignature($this->traveler,$tempDestReply);
				$nContent = array_merge($nContent,array(
					"destination"			=>	$destination,
					"messageID"				=>	$nMessage->getAttributeID(),
					"lockDestination"		=>	true,
					"subject"				=>	parseSubjectRE($nMessage->getTitle()),
					"actionPage"			=>	$action,
					"recipientFlag"			=>	$recipientFlag,
					"text"					=>	$text,
					'recipientSendableID'	=>	$nMessage->getSource()->getSendableID()	
				));
				
				if( $tempDestReply instanceof Group )
					$nContent["sourceID"] = $tempDestReply->getGroupID();
				else
					$nContent["sourceID"] = $tempDestReply->getSendableID() . "_sID";	  
				if( get_class($nMessage) == 'InquiryMessage' ){
					$nContent["listingID"] = $nMessage->getListingID();
					$nContent["active"] = constants::INQUIRY;
				}
					
			}
			else
				$nContent["errString"] = "Sorry, you may have improperly accessed this page";
				
			return $this->viewMessageForm($nContent);
		}
		
		function viewMessageForm( $content = array() ){
			
			$ht = $this->file_factory->getClass('HelpText');
			
			$nContent = array_merge($this->getMessageStats(),array(
				"text"					=>	isset($content["text"]) ? $content["text"] : "",				
				"title"					=>	isset($content["title"]) ? trim($content["title"]) : "",
				"subject"				=>	isset($content["subject"]) ? trim($content["subject"]) : "",
				"errString"				=>	isset($content["errString"]) ? trim($content["errString"]) : "",
				"sourceID"				=>	isset($content["sourceID"]) ? $content["sourceID"] : 0,
				"listingID"				=>	isset($content["listingID"]) ? $content["listingID"] : 0,
				"messageID"				=>	isset($content["messageID"]) ? $content["messageID"] : 0,
				"lockDestination"		=>	isset($content["lockDestination"]) ? $content["lockDestination"] : false,
				"arrError"				=>	isset($content["arrError"]) ? $content["arrError"] : array(),
				"travTo"				=>	isset($content["destination"]) ? $content["destination"] : array(),
				"active"				=>	$this->message_helper->getAction() != constants::COMPOSE || ( isset($content['isReply']) && $content['isReply'] ) ? (isset($content["active"]) ? $content["active"] : $this->message_helper->getFolder()) : NULL,
				"actionPage"			=>	isset($content["actionPage"]) ? $content["actionPage"] : "messages.php",
				"hdnRecipientFlag"		=>	isset($content["recipientFlag"]) ? $content["recipientFlag"] : $this->message_helper->getRecipient(),
				"isSent"				=>	isset($content["isSent"]) ? $content["isSent"] : 0,		
				"recipientHelpText"		=>	isset($content["recipientFlag"]) &&  constants::GROUP == $content["recipientFlag"] ? nl2br($ht->getHelpText('messages-recipient-group')) : nl2br($ht->getHelpText('messages-recipient-traveler')),
				"filteredDestination"	=>	isset($content["filteredDestination"]) ? $content["filteredDestination"] : array(),
				'recipientSendableID'	=>	isset($content['recipientSendableID']) ? $content['recipientSendableID'] : 0,				
				"addressbookHelpText"	=>	$this->message_helper->getUserMode() == constants::GROUP ? nl2br($ht->getHelpText('addressbook-message-launcher')) : "",
				
				"action"				=>	isset($content["action"]) ? $content["action"] : $this->message_helper->getAction(),
				"ownerID"				=>	$this->traveler->getTravelerID(),
				"user"					=>	$this->message_helper->getUserMode(), 				
				"traveler"				=>	$this->traveler,
				"subNavigation"			=> 	$this->getSubNavigation(),
				"viewFolders"			=>	$this->getValidMessageFolders(),
				"suggestList"			=>	$this->getDestinationSuggestions(),
				"referer"				=>	$this->message_helper->getReferer()	
			));
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');

			if( in_array($nContent["action"],array(constants::REPLY,constants::EDIT)) ){
				$code = <<<SCRIPT
				<script type="text/javascript">
					//<[!CDATA[
						window.onload = function(){ setFocus(); }	   		
					//]]>
				</script>	
SCRIPT;
			Template::includeDependent($code);
			}
						
			Template::includeDependentJs("/js/messages.js");
			Template::includeDependentJs("js/actb.js");
			Template::includeDependentJs("js/commons.js");
			Template::includeDependentJs("js/prototype.compressed.js");
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );   
			
			$fcd = $this->file_factory->getClass('MessagesFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveMessageForm()) );
			$obj_view->render();
			
		}
		
		function viewMessageList(){
			
			$listContent = $this->retrieveFolderMessages();
			$listContent['traveler'] = $this->traveler;
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::includeDependentJs("/travellog/js/messages.js");
			Template::setMainTemplate   ( $this->file_factory->getTemplate('LayoutMain') );   
			
			$fcd = $this->file_factory->getClass('MessagesFcd', $listContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveMessageList()) );
			$obj_view->render();
			
		}
		//###################################### REFACTORED CODE #########################################3
		/**
		 * sends a personal message to a list of travelers
		 */
		public function composeNewMessage() {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			require_once 'travellog/model/messageCenter/Class.gaPersonalMessageMapper.php';
			
			$traveler     = gaTravelerMapper::getTraveler($_SESSION['travelerID']); 
			$recipientIDs = gaTravelerMapper::getSendableIDsWithUserNames(explode(',', $_POST['txtTo']));
			
			if (count($recipientIDs) > 0) {
				$newMessage = new gaPersonalMessage();
			
				$newMessage->setTitle        ( $_POST['txtSubject']       );
				$newMessage->setMessage      ( $_POST['taMessage']        );
				$newMessage->setSenderID     ( $traveler->getSendableID() );
				
				gaPersonalMessageMapper::batchSendMessages($newMessage, $recipientIDs);
				
				echo 'Message sent';
			} else {
				echo 'Message not sent: No valid recipients found';
			}
		}
		
		/**
		 * displays all types of messages (personal, news, shout outs)
		 * @author	Jonas Tandinco (Nov/11/2008)
		 */
		public function listMessages() {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			require_once 'travellog/model/messageCenter/Class.gaPersonalMessageMapper.php';
			
			$messagesPerPage = 50;

			$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);
			
			// insert the different types of messages into a parameter holder
			$messages = $this->wrapMessages(gaMessageMapper::getAllTravelerMessages($_SESSION['travelerID']));
			$outboxMessages = $this->wrapOutboxMessages(gaPersonalMessageMapper::getOutboxMessages($traveler));

			// PAGING
			// count the number of pages
			$numberOfPages = count($messages) / $messagesPerPage;
			if ($numberOfPages - ((int) $numberOfPages) !== 0) {
				$numberOfPages = ((int) $numberOfPages) + 1;
			} else {
				$numberOfPages = (int) $numberOfPages;
			}
			
			// determine page number requested
			if (!isset($_GET['page']) || $_GET['page'] > $numberOfPages) {
				$pageNumber = 1;
			} else {
				$pageNumber = $_GET['page'];
			}
			
			// TODO: place a first, last, back, next page navigation
			$pageLinks = '';
			if (count($messages) > $messagesPerPage) {
				for ($i = 1; $i <= $numberOfPages; $i++) {
					if ($pageNumber == $i) {
						$pageLinks .= "$i ";
					} else {
						$pageLinks .= '<a href="messages.php?page=' . $i . '">' . $i . '</a> ';
					}
				}	
			}
			
			// limit number of messages displayed
			$messages = array_slice($messages, ($pageNumber - 1) * $messagesPerPage, $messagesPerPage);			
			
			
			Template::setMainTemplate ( $this->file_factory->getTemplate('LayoutMain') );
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::includeDependentJs('/js/messageCenter/messages.js');
			
			// PROFILE INFO 
			//$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			//$profile_comp->init($_SESSION['travelerID']);

			// SUBNAVIGATION
			$subNavigation = $this->file_factory->getClass('SubNavigation');		
			$subNavigation->setContext         ( 'TRAVELER'              );
			$subNavigation->setContextID       ( $_SESSION['travelerID'] );
			$subNavigation->setLinkToHighlight ( 'MESSAGES'              );

			$template = $this->file_factory->getClass('Template');
			$template->set_path('travellog/views/messageCenter/');

			//$template->set ( 'prof_comp',     $profile_comp->get_view());
			$template->set ( 'subNavigation',  $subNavigation  );
			$template->set ( 'pageLinks',      $pageLinks      );
			
			$template->set ( 'traveler',       $traveler       );
			$template->set ( 'messages',       $messages       );
			$template->set ( 'outboxMessages', $outboxMessages );
			
			$template->out ( 'tpl.ViewMessageCenterMain.php' );
		}
		
		protected function wrapOutboxMessages($theMessages) {
			require_once 'Class.gaParameterHolder.php';
			require_once 'travellog/helper/gaTextHelper.php';
			
			$messageContentLimit = 100;
			
			$messages = array();
			foreach ($theMessages as $theMessage) {
				$message = new gaParameterHolder();
				
				$message->set ( 'contentType', gaMessage::PERSONAL                                                                     );
				$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-0' );
				$message->set ( 'sender',      $theMessage->getRecipient()->getDisplayName()                                           );
				$message->set ( 'senderLink',  '/' . $theMessage->getRecipient()->getDisplayName()                                     );
				$message->set ( 'dateCreated', date('F j, Y g:i a', strtotime($theMessage->getCreated()))                              );
				$message->set ( 'title',       $theMessage->getTitle()                                                                 );
				$message->set ( 'snippet',     limitString($theMessage->getMessage(), $messageContentLimit)                            );
				$message->set ( 'content',     nl2br($theMessage->getMessage())                                                        );
				$message->set ( 'cssClass',    'message'                                                                               );
				$message->set ( 'allowReply',  FALSE                                                                                    );
				$message->set ( 'senderID',    $theMessage->getSenderID()                                                              );
				
				$messages[] = $message;
			}
			
			return $messages;
		}
		
		protected function wrapMessages($theMessages) {
			require_once 'Class.gaParameterHolder.php';
			require_once 'travellog/helper/gaTextHelper.php';
			
			$messageContentLimit = 100;
			
			$messages = array();
			foreach ($theMessages as $theMessage) {
				$message = new gaParameterHolder();
				
				switch (get_class($theMessage)) {
					case 'Comment':
						/////////////// START CHERYL'S CODE ///////////////////
						require_once 'travellog/model/Class.PokeType.php';
						require_once 'travellog/views/Class.PokesView.php';
					
						$tp = new TravelerProfile($theMessage->getAuthor());
						$Poke = new PokeType($theMessage->getPokeType());
	
						$pokesview = new PokesView();
		 				$pokesview->setAuthorId($tp->getTravelerID());		// travelerId or groupId of the greeting owner
		 				$pokesview->setOwnerId($_SESSION['travelerID']);	// travelerId or groupId of profile/photo/journal-entry
		 				$pokesview->setPokeTypeId($Poke->getPokeTypeID());
						$pokesview->setMessage($theMessage->getTheText());
	
						if (!strcasecmp("TravelLog", get_class($theMessage->getContext()))){
							$pokesview-> setContextId($theMessage->getContext()->getTravelLogID());
							$pokesview-> setContext(3);
						}
		 				elseif (!strcasecmp("Photo", get_class($theMessage->getContext()))){
		 					$pokesview-> setContextId($theMessage->getContext()->getPhotoID());
		 					$pokesview-> setContext(2);
		 				}
		 				else{
		 					$pokesview-> setContextId($theMessage->getContext()->getTravelerID());
		 					$pokesview-> setContext(1);
		 				}
	
		 				if (0 < strlen($theMessage-> getTheText()))
		 					$pokesview-> setHasMessage(true);
		 				else
		 					$pokesview-> setHasMessage(false);
		 				$pokesview->setAuthor();
		 				$pokesview->setOwner();
						////////////// END CHERYL'S CODE ////////////////////////////////////////						
						
						$message->set ( 'contentType', gaMessage::SHOUTOUT                                                     );
						$message->set ( 'ID',          $theMessage->getCommentID() . '-' . $message->get('contentType') . '-2' );
						$message->set ( 'sender',      ''                                                                      );
						$message->set ( 'senderLink',  ''                                                                      );
						$message->set ( 'dateCreated', date('F j, Y g:i a', strtotime($theMessage->getCreated()))              );
						$message->set ( 'title',       $pokesview->getPassportMessage()                                        );
						$message->set ( 'snippet',     ''                                                                      );
						$message->set ( 'content',     ''                                                                      );
						$message->set ( 'cssClass',    'shoutout'                                                              );
						$message->set ( 'allowReply',  FALSE                                                                   );
						
						break;
					
					case 'gaPersonalMessage':
						$message->set ( 'contentType', gaMessage::PERSONAL                                                                     );
						$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-' . $theMessage->isRead() );
						$message->set ( 'sender',      $theMessage->getSender()->getUserName()                                                 );
						$message->set ( 'senderLink',  '/' . $theMessage->getSender()->getUserName()                                           );
						$message->set ( 'dateCreated', date('F j, Y g:i a', strtotime($theMessage->getCreated()))                              );
						$message->set ( 'title',       $theMessage->getTitle()                                                                 );
						$message->set ( 'snippet',     limitString($theMessage->getMessage(), $messageContentLimit)                            );
						$message->set ( 'content',     nl2br($theMessage->getMessage())                                                        );
						$message->set ( 'cssClass',    'message'                                                                               );
						$message->set ( 'allowReply',  TRUE                                                                                    );
						$message->set ( 'senderID',    $theMessage->getSenderID()                                                              );
						
						break;
						
					case 'gaGroupPersonalMessage':
						$message->set ( 'contentType', gaMessage::PERSONAL                                                                     );
						$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-' . $theMessage->isRead() );
						$message->set ( 'sender',      $theMessage->getSender()->getName()                                                     );
						$message->set ( 'senderLink',  $theMessage->getSender()->getFriendlyURL()                                              );
						$message->set ( 'dateCreated', date('F j, Y g:i a', strtotime($theMessage->getCreated()))                              );
						$message->set ( 'title',       $theMessage->getTitle()                                                                 );
						$message->set ( 'snippet',     limitString($theMessage->getMessage(), $messageContentLimit)                            );
						$message->set ( 'content',     nl2br($theMessage->getMessage())                                                        );
						$message->set ( 'cssClass',    'message'                                                                               );
						$message->set ( 'allowReply',  TRUE                                                                                    );
						$message->set ( 'senderID',    $theMessage->getSenderID());
						
						break;
					
					case 'gaNews':
						$message->set ( 'contentType', gaMessage::NEWS                                                                         );
						$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-' . $theMessage->isRead() );
						$message->set ( 'sender',      $theMessage->getSender()->getName()                                                     );
						$message->set ( 'senderLink',  $theMessage->getSender()->getFriendlyURL()                                              );
						$message->set ( 'dateCreated', date('F j, Y g:i a', strtotime($theMessage->getCreated()))                              );
						$message->set ( 'title',       $theMessage->getTitle()                                                                 );
						$message->set ( 'snippet',     limitString($theMessage->getMessage(), $messageContentLimit)                            );
						$message->set ( 'content',     nl2br($theMessage->getMessage())                                                        );
						$message->set ( 'cssClass',    'news'                                                                                  );
						$message->set ( 'allowReply',  FALSE                                                                                   );
						
						break;
					case 'gaAlert':
						$message->set ( 'contentType', gaMessage::ALERT                                                                        );
						$message->set ( 'ID',          $theMessage->getID() . '-' . $message->get('contentType') . '-' . $theMessage->isRead() );
						$message->set ( 'sender',      $theMessage->getSender()->getName()                                                     );
						$message->set ( 'senderLink',  $theMessage->getSender()->getFriendlyURL()                                              );
						$message->set ( 'dateCreated', date('F j, Y g:i a', strtotime($theMessage->getCreated()))                              );
						$message->set ( 'title',       $theMessage->getTitle()                                                                 );
						$message->set ( 'snippet',     limitString($theMessage->getMessage(), $messageContentLimit)                            );
						$message->set ( 'content',     nl2br($theMessage->getMessage())                                                        );
						$message->set ( 'cssClass',    'alerts'                                                                                );
						$message->set ( 'allowReply',  FALSE                                                                                   );
						
						break;
				}
				
				$messages[] = $message;
			}
			
			return $messages;
		}
		
		public function markAsRead() {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			gaMessageMapper::markAsRead($_POST['messageID'], gaTravelerMapper::getSendableIDByTravelerID($_SESSION['travelerID']));
			echo 1;
		}
		
		public function markAsUnread() {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			gaMessageMapper::markAsUnread($_POST['messageID'], gaTravelerMapper::getSendableIDByTravelerID($_SESSION['travelerID']));
			echo 1;
		}
		
		public function doDeleteMessage() {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			gaMessageMapper::deleteMessage($_POST['messageID'], gaTravelerMapper::getSendableIDByTravelerID($_SESSION['travelerID']));
			
			echo 'Message deleted';
		}
		
		public function batchDelete() {
			// TODO: separate deletion of shoutouts from messages
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			$messageIDs = explode(',', $_POST['messageIDs']);
			gaMessageMapper::batchDelete($messageIDs, gaTravelerMapper::getSendableIDByTravelerID($_SESSION['travelerID']));
			
			echo 'Selected messages deleted';
		}
		
		public function batchMarkAsUnread() {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			$messageIDs = explode(',', $_POST['messageIDs']);
			gaMessageMapper::batchMarkAsUnread($messageIDs, gaTravelerMapper::getSendableIDByTravelerID($_SESSION['travelerID']));
			echo 1;
		}
		
		public function batchMarkAsRead() {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			$messageIDs = explode(',', $_POST['messageIDs']);
			gaMessageMapper::batchMarkAsRead($messageIDs, gaTravelerMapper::getSendableIDByTravelerID($_SESSION['travelerID']));
			echo 1;
		}
		
		public function doReplyToMessage() {
			require_once 'travellog/model/messageCenter/Class.gaPersonalMessageMapper.php';
			require_once 'travellog/model/messageCenter/Class.gaUserMapper.php';
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			
			$newMessage = new gaPersonalMessage();
			
			$newMessage->setTitle        ( $_POST['subject']                                                    );
			$newMessage->setMessage      ( $_POST['message']                                                    );
			$newMessage->setSenderID     ( gaTravelerMapper::getSendableIDByTravelerID($_SESSION['travelerID']) );
			$newMessage->setDiscriminator( gaMessage::PERSONAL                                                  );
			
			$message = gaPersonalMessageMapper::getMessage($_POST['messageID']);

			// determine message sender
			if (gaUserMapper::getUserType($message->getSenderID()) == gaUser::TRAVELER) {
				$newMessage->setRecipientID ( $_POST['recipientID'] );
			} else {
				$senderID = gaUserMapper::getSenderOfGroupMessage($_POST['messageID']);
				$newMessage->setRecipientID ( $senderID );
			}
			
			gaPersonalMessageMapper::sendMessage($newMessage);
			
			echo 'Message reply sent';
		}
		//##################################### END REFACTORED CODE #####################################
		
		public function listMessagesOldWay() {
			// TODO: check if user is logged in, if not redirect to login page
			
			Template::setMainTemplate ( $this->file_factory->getTemplate('LayoutMain') );
			Template::setMainTemplateVar('page_location', 'My Passport');
			
			$mp = $this->file_factory->getClass('MessagesPanel');
			$mp->setContext(new Traveler($_SESSION['travelerID']));
		
			$template = $this->file_factory->getClass('Template');
			$template->set_path('travellog/views/');
			
			$template->set('owner', TRUE);
			$template->set('messages', $mp);
			$template->out('tpl.ViewMessageCenterMain.php');
		}
		
		/**
		 * view individual message
		 * only in viewing message, $destination is array of strings
		 */ 
		
		function retrieveMessage(){
			
			$folder = $this->message_helper->getFolder();
			$attributeID = $this->message_helper->getAttributeID();
			$messageFactory = MessageFactory::getInstance();
			$errString = "";
			$nContent = array();
			
			try{
				$nMessage = $messageFactory->create($attributeID,$this->traveler->getSendableID(),$folder);
				if( Message::IsOwner($attributeID,$this->traveler->getSendableID(),$folder)){
					$nMessage->setRead(1);
					$destination = array();
					foreach( $nMessage->getDestination() as $indDestination )
						$destination[] = displayLink($indDestination,$this->traveler->getTravelerID());
					$source = displayLink($nMessage->getSource(),$this->traveler->getTravelerID());
					$mainSource = $nMessage->getSource();	
				}
				else
					throw new exception("Sorry, you may have improperly accessed this page.");
			} 
			catch (Exception $e){
				$errString = "Sorry, you may have improperly accessed this page.";	
			}
			
			$nContent = array_merge($nContent, $this->getMessageStats(), array(
				"errString"			=>	$errString,
				"active"			=>	$folder,
				"referer"			=>	$this->message_helper->getReferer(),
				"title"				=>	"MY messages",
				"ownerID"			=>	$this->traveler->getTravelerID(),
				"viewFolders"		=>	$this->getValidMessageFolders(),
				"subNavigation"		=>	$this->getSubNavigation(),
				"message"			=>	isset($nMessage) ? $nMessage : NULL,
				"destination"		=>	isset($destination) ? $destination : array(),
				"source"			=>	isset($source) ? $source : "",
				"mainSource"		=>	isset($mainSource) ? $mainSource : NULL,
				"messageActions"	=> $this->getValidMessageActions($nMessage)
			));
			
			return $nContent;
			
		} 
		 
		function viewMessage(){
			
			$listContent = $this->retrieveMessage();
			$listContent['traveler'] = $this->traveler;
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			$code = <<<SCRIPT
			<script type="text/javascript">
				//<[!CDATA[
					function doSubmitOnce(currentForm)
					{
						// Run through elements and disable submit buttons
						for(i=0;i<currentForm.length;i++)
						{
							var currentElement = currentForm.elements[i];
							if(currentElement.type.toLowerCase() == "submit")
							currentElement.disabled = true;
						}
					}
			
					// We need to register our submit once function on all forms
					if(document.all||document.getElementById) 
					{	
						// Run through all forms on page
						for(i=0;i<document.forms.length;i++)
						{
							var currentForm = document.forms[i];
							// Register event handler
							// Use quirksmode idea for flexible registration by copying existing events
							var oldEventCode = (currentForm.onsubmit) ? currentForm.onsubmit : function () {};
							currentForm.onsubmit = function () {oldEventCode(); doSubmitOnce(currentForm)};
						}
					}
				//]]>
			</script>
SCRIPT;
			Template::includeDependent($code);
			//Template::setMainTemplate("travellog/views/tpl.LayoutMain.php");
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') ); 
			Template::includeDependentJs("/travellog/js/messages.js");
			Template::setMainTemplateVar('page_location', 'My Passport');
			
			$fcd = $this->file_factory->getClass('MessagesFcd', $listContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveMessage()) );
			$obj_view->render();
			
		}
		
		function sendMessage(){
			
			$errMsg = '';
			$nContent = array();
			$arrError = array();
			$msgData = $this->message_helper->readMessageData();
			$nContent["recipientFlag"] = $this->message_helper->getRecipient();
			$nContent["active"] = $this->message_helper->getFolder();
			
			/* set destination */
			$sendableFactory = $this->file_factory->invokeStaticClass('SendableFactory');
			if( $msgData['isReply'] && $msgData['recipientSendableID'] )
				$filteredDestination = $this->filterValidDestinations($msgData['recipientSendableID'],0,true);
			else
				$filteredDestination = $this->filterValidDestinations($msgData["destination"],$nContent["recipientFlag"]);
			
			// set source
			$groupFactory = GroupFactory::instance();
			if( preg_match('/([0-9]*)_sID/',$this->message_helper->getUserGroupID(), $matches) )
				$nContent["msgSource"] = $sendableFactory->Create($matches[1]);
			elseif($this->message_helper->getUserGroupID()){
				try{
					$nContent["msgSource"] = $groupFactory->create( array($this->message_helper->getUserGroupID()));					
				}
				catch( Exception $e){
					$nContent["msgSource"] = null;
				}
			}
			if( !isset($nContent["msgSource"]) || !$nContent["msgSource"] )
				$nContent["msgSource"] = $this->traveler;
				
			// remove self from destination if usermode is traveler
			if( constants::TRAVELER == $nContent["recipientFlag"] && in_array($this->traveler->getUsername(),$filteredDestination["failedDest"]) )
				unset($filteredDestination["failedDest"][array_search($this->traveler->getUserName(),$filteredDestination["failedDest"])]);
			$nContent["filteredDestination"] = $filteredDestination;
			if( 1 == count($filteredDestination['validDest']) && $filteredDestination['validDest'][0] instanceof Group ){
				$tmpGroup = $filteredDestination['validDest'][0];
				if( $tmpGroup->isModerator($this->session_manager->get('travelerID')) ){
					$nContent['msgSource'] = $this->file_factory->getClass('Moderator',array($this->session_manager->get('travelerID'),$tmpGroup->getGroupID()));
				}
			}
			
			// send message if required fields are met
			if( strlen($msgData["subject"]) && strlen($msgData["message"]) && ( count($filteredDestination["validDest"]) || constants::INQUIRY == $nContent["active"] ) ){
				// create folder for the message
				if( constants::INQUIRY == $nContent["active"] )
					$nMessage = $this->file_factory->getClass("InquiryMessage");
				elseif( constants::DRAFTS == $nContent["active"])
					$nMessage = $this->file_factory->getClass("PersonalMessage",array($msgData["attributeID"]));
				else
					$nMessage = $this->file_factory->getClass("PersonalMessage");	
					
				$nMessage->setTitle( htmlspecialchars($msgData["subject"]) );		
				$nMessage->setText( htmlspecialchars($msgData["message"]) );		
				if( constants::INQUIRY == $nContent["active"] ){ 
					$nMessage-> setMessageTypeID(constants::MESSAGE_TYPE_INQUIRY);
					$nMessage-> setListingID($msgData["listingID"]);
					if( $msgData["attributeID"] )
						$nMessage->setParentID($msgData["attributeID"]);
					$filteredDestination["failedDest"] = array();
				}
				elseif(constants::INBOX == $nContent["active"] && $msgData["isReply"] ){
					$nMessage-> setMessageTypeID(constants::MESSAGE_TYPE_INBOX);
					$nMessage->setParentID($msgData["attributeID"]);
				}
				elseif( constants::DRAFTS == $nContent["active"] )
					$nMessage-> setMessageTypeID(constants::MESSAGE_TYPE_DRAFTS);
				else
					$nMessage-> setMessageTypeID(constants::MESSAGE_TYPE_INBOX);
				$nMessage-> setSource($nContent["msgSource"]);
				$nMessage-> setDestination($filteredDestination["trueDest"]);
				$nMessage-> Send();
				
				require_once('travellog/model/Class.NotificationSetting.php');
				$notificationSetting = new NotificationSetting();
				$notificationSetting->setContext(NotificationSetting::MESSAGE_CONTEXT);
				$notificationSetting->setContextID($nMessage->getMessageID());
				$notificationSetting->setMessageIncluded( $msgData['sendNotification']);
				$notificationSetting->save();
				
				// added by chris: send instant notification
				require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
				RealTimeNotifier::getInstantNotification(RealTimeNotifier::PERSONAL_MESSAGE, 
					array(
						'MESSAGE'			=> $nMessage,
						'INCLUDE_MESSAGE'	=> $msgData['sendNotification'] == 1
					))->send();
				// end add
				
				/* for display purposes only */
				if( constants::INQUIRY == $nContent["active"] && 0 == count($filteredDestination["validDest"]) ){
					$Listing = $this->file_factory->getClass("Listing", array($msgData["listingID"]));
					$nContent["destination"] = array($Listing->getClient());
					$nContent["filteredDestination"]["validDest"] = array($Listing->getClient());
					$nContent["filteredDestination"]["failedDest"] = array();
				}
				else
					$nContent["destination"] = $filteredDestination["validDest"];
				
				$nContent["isSent"] = 1;
				
			}
			else{
				
				if( in_array($this->traveler->getUserName(),$msgData["destination"]) )
					$arrError[] = constants::ERR_SEND_TO_SELF;
				if( count($filteredDestination["blockedByDest"]) )
					$arrError[] = constants::ERR_BLOCKED;
				if( count($filteredDestination["suspendedDest"]) )
					$arrError[] = constants::ERR_SUSPENDED;	
				if( count($filteredDestination["failedDest"]) && constants::INQUIRY != $nContent["active"] )
					$arrError[] = constants::ERR_FAILED;
				if( !count($msgData["destination"]) )
					$arrError[] = constants::ERR_UNDEFINED_DEST;
				if( !strlen($msgData["subject"]) && !strlen($msgData["message"]) )
					$arrError[] = constants::ERR_SUBJECT_MESSAGE;
				elseif( !strlen($msgData["subject"]) )
					$arrError[] = constants::ERR_SUBJECT;
				elseif( !strlen($msgData["message"]) )
					$arrError[] = constants::ERR_MESSAGE;
				$nContent["lockDestination"] = $msgData["lockDestination"];
				$nContent["destination"] = $msgData["destination"];
				$nContent["arrError"] = $arrError;
				$nContent["filteredDestination"] = $filteredDestination;
				$nContent["action"] = $msgData["isReply"] ? constants::REPLY : constants::COMPOSE;
			}
				
			$nContent = array_merge($nContent,array(
				"subject"		=>	$msgData["subject"],
				"text"			=>	$msgData["message"],
				"messageID"		=>	$msgData["attributeID"],
				"travTo"		=>	$msgData["travTo"]
			));
			
			if( count($arrError) )
				$this->viewMessageForm( $nContent );
			else
				$this->viewMessageConfirmation( $nContent );
		}
		
		function saveMessage(){
			
			$nContent = array();
			$arrError = array();
			$msgData = $this->message_helper->readMessageData();
			$nContent["recipientFlag"] = $this->message_helper->getRecipient();
			$nContent["active"] = $this->message_helper->getFolder();
			$filteredDestination = $this->filterValidDestinations($msgData["destination"],$nContent["recipientFlag"]);

			// set message source
			$groupFactory = GroupFactory::instance();
			try{
				$nContent["msgSource"] = $groupFactory->create( array($this->message_helper->getUserGroupID()));
			}
			catch( Exception $e ){
				$nContent["msgSource"] = $this->traveler;
			}
						
			if(  strlen($msgData["subject"]) || strlen($msgData["message"]) ){
				if( constants::INQUIRY == $nContent["active"] )
					$nMessage = $this->file_factory->getClass("InquiryMessage");
				elseif( constants::DRAFTS == $nContent["active"])
					$nMessage = $this->file_factory->getClass("PersonalMessage",array($msgData["attributeID"]));
				else
					$nMessage = $this->file_factory->getClass("PersonalMessage");
				$nMessage->setTitle( htmlspecialchars($msgData["subject"]) );		
				$nMessage->setText( htmlspecialchars($msgData["message"]) );
				$nMessage-> setMessageTypeID(constants::MESSAGE_TYPE_DRAFTS);		
				if( constants::INQUIRY == $nContent["active"] )
					$PersonalMessage->setListingID($msgData["listingID"]);
				$nMessage-> setSource($this->traveler);
				$nMessage-> setDestination($filteredDestination["trueDest"]);
				$this->traveler->SaveAsDraft($nMessage);
			}	
			
			// redirect to proper folder
			header('location: messages.php?drafts');
			exit;
			break;
		}
		
		function moveMessageToTrash(){
			
			$appendQstring = '';
			$msgData = $this->message_helper->readMessageData();
			$folder = $this->message_helper->getFolder(); 
			switch( $folder ){
				case constants::SENT_ITEMS :
					$msgList = $this->traveler-> getSentitems();
					$appendQstring .= constants::SENT_ITEMS;
					break;
				case constants::INBOX :
					$msgList = $this->traveler-> getInbox();
					$appendQstring .= constants::INBOX;	
					break;	
				case constants::DRAFTS :
					$msgList = $this->traveler-> getDrafts();
					$appendQstring .= constants::DRAFTS;	
					break;	
				case constants::INQUIRY :
					$msgList = $this->traveler-> getInquiryBox();
					$appendQstring .= "&listingID=" . $this->message_helper->getListingID();	
					break;		
			} 
			
			
			if( count($this->message_helper->getMessageIDs()) ){
				foreach ( $this->message_helper->getMessageIDs() as $indMsgID ){
					if( constants::DRAFTS == $folder ){
						$tmpMsg = new PersonalMessage($indMsgID);
						$indMsgID = $tmpMsg->getMessageID();	
					}
					$msgList->Delete($indMsgID);
				}
			}
			else{
				if( constants::DRAFTS == $folder ){
					$tmpMsg = new PersonalMessage($msgData["attributeID"]);
					$msgData["attributeID"] = $tmpMsg->getMessageID();	
				}
				$msgList->Delete($msgData["attributeID"]);
			}
			
			//  redirect to referer
			header('location:messages.php?' . $appendQstring );
			exit;
		}
		
		function deleteMessage(){
			$msgList = $this->traveler->getTrash();
			$msgData = $this->message_helper->readMessageData();
			
			if( count($this->message_helper->getMessageIDs()) ){
				foreach ( $this->message_helper->getMessageIDs() as $indMsgID )
					$msgList->Delete( $indMsgID );
			}
			elseif( $this->message_helper->isDeleteAll() )
				$msgList->deleteAll();
			else
				$msgList->Delete( $msgData["attributeID"] );
			
			// redirect to referer
			header('location: messages.php?' . $this->message_helper->getFolder() );
			exit;
		}

		function editMessage(){
			
			$errString = '';
			$nContent = array();
			$msgData = $this->message_helper->readMessageData();
			// redirect to messages.php for unknown/inexistent messages
			if ( !$this->message_helper->getValidEditMessage() ) {
				$errString .= 'Uknown message source';
			}
			try{
				$nMessage = new PersonalMessage($msgData["attributeID"],$this->traveler->getSendableID());
			}catch (Exception $e){
				$errString .= 'Message does not exist';
			}
			if( !strlen(trim($errString)) ){
				$destination = array();
				foreach( $nMessage->getDestination() as $indDestination ){
					if( $indDestination instanceof Traveler )
						$destination[] = $indDestination->getUserName();
					elseif( $indDestination instanceof Group )
						$destination[] = $indDestination->getName();
					else
						$destination[] = $indDestination->getInstName();
				}
				$nContent = array(
					"destination"	=>	$destination,
					"subject"		=>	$nMessage->getTitle(),
					"text"			=>	$nMessage->getText(),
					"messageID"		=>	$nMessage->getAttributeID(),
					"actionPage"	=>	"messages.php"			
				);
				if( constants::INQUIRY == $nMessage->getDiscriminator() ){
					$nContent["active"]	= constants::INQUIRY;
					$listingID = $nMessage->getListingID();
				}
			}
			$nContent = array_merge($this->getMessageStats(),$nContent);
			return $this->viewMessageForm($nContent);
			
		}
		
		function sendInquiry(){
			
			$nConn = $this->file_factory->getClass("Connection");
			$nRecordset = $this->file_factory->getClass("Recordset",array($nConn));
			$errString = '';
			$nContent = array();
			$destination = array();
			
			if( $this->message_helper->getValidInquiry() ){
				// query for listingname
				$myquery = 	$nRecordset->Execute("SELECT listingname,tbclient.clientID,instname " . 
					"FROM GoAbroad_Main.tblisting,Travel_Logs.tblTravelertoListing,GoAbroad_Main.tbclient " .
					"WHERE tblTravelertoListing.listingID = " . $this->message_helper->getListingID() . 
					" AND tblisting.clientID = tbclient.clientID " .
					" AND tblisting.listingID = tblTravelertoListing.listingID " .
					" AND travelerID = " . $this->traveler->getTravelerID() );
				if( $qryListing = mysql_fetch_array($myquery) )
					$destination[] = $qryListing['instname'];
				else
					$errString .= 'Permission denied on sending an inquiry.';
			}
			else
				$errString .= 'Unknown referred listing.';	
			
			if( !strlen(trim($errString)) ){
				$nContent = array(
					"destination"	=>	$destination,
					"subject"		=>	"Student Inquiry for " . $qryListing['listingname'],
					"actionPage"	=>	"messages.php",
					"listingID"		=>	$this->message_helper->getListingID(),
					"text"			=>	$this->message_helper->getTravelerSignature($this->traveler),
					'recipientFlag'	=>	constants::GROUP		
				); 	
			}
			
			$nContent = array_merge($this->getMessageStats(),$nContent,array(
				"errString"		=>	$errString
			));
			
			$this->viewMessageForm($nContent);	
		}
		
		function viewMessageConfirmation( $content = array() ){
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			$nContent = array_merge($this->getMessageStats(),array(
				"isSent"				=>	$content["isSent"],
				"filteredDestination"	=>	$content["filteredDestination"],				
				"travTo"				=>	$content["travTo"],
				"active"				=>	$content["active"],
				"user"					=>	$this->message_helper->getUserMode(),
				"referer"				=>	$this->message_helper->getReferer(),
				"subNavigation"			=>	$this->getSubNavigation(),
				"ownerID"				=>	$this->traveler->getTravelerID(),
				"gID"					=>	$this->session_manager->get("gID"),
				"viewFolders"			=>	$this->getValidMessageFolders(),
				'redirect'				=>	constants::ADMIN_TRAVELER_ID == $this->traveler->getTravelerID() ? array('page'=>'index.php','text'=>'Return To Admin Panel' ) : array('page'=>'passport.php','text'=>'Return To Passport Page')  
			));
			
			//Template::setMainTemplate("travellog/views/tpl.LayoutMain.php");
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') ); 
			Template::includeDependentJs("/travellog/js/messages.js");
			
			$fcd = $this->file_factory->getClass('MessagesFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveMessageConfirmation()) );
			$obj_view->render();
		}
		
		/**
		 * start of misc functions
		 */ 
		function getValidMessageFolders(){
			
			if( 0 < $this->session_manager->get("clientID") )
				$viewFolders = array(constants::INBOX,constants::SENT_ITEMS,constants::TRASH);
			elseif( 0 < $this->session_manager->get("gID") )
				$viewFolders = array(constants::INBOX,constants::SENT_ITEMS,constants::TRASH);
			elseif( constants::ADMIN_TRAVELER_ID == $this->session_manager->get('travelerID') )
				$viewFolders = array(constants::INBOX,constants::SENT_ITEMS,constants::TRASH);	
			else
				$viewFolders = array(constants::INBOX,constants::SENT_ITEMS,constants::DRAFTS,constants::TRASH);
			
			return $viewFolders;	
		}
		
		function getMessageStats(){
			
			$nContent = array(
				"nNewMsg"		=>	$this->traveler->getInbox()->getNumberOfNewMessages(),
				"nIncNewMsg"	=>	$this->traveler->getInquiryBox()->getNumberOfNewMessages(),
				"nTrashMsg"		=>	$this->traveler->getTrash()->getTotalNumberOfTrash()
			);
			
			return $nContent;
			
		}
		
		function getSubNavigation(){
			
			// sub navigation
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			
			if( 0 < $this->session_manager->get("gID") ){
				$tmpGID = $this->message_helper->getUserGroupID();
				$subNavigation->setContext("GROUP");
				$subNavigation->setContextID($tmpGID);
				if( 0 < $this->session_manager->get("gID") )
					$subNavigation->setGroupType("ADMIN_GROUP");
				else
					$subNavigation->setGroupType("FUN_GROUP");	
			}
			else{
				$subNavigation->setContext("TRAVELER");
				$subNavigation->setContextID($this->traveler->getTravelerID());
			}
			$subNavigation->setLinkToHighlight('MESSAGES');
			return $subNavigation;
		}
		
		/**
		 * function filterValidDestinations
		 * @param array dest array of username/groupname of the message's destination
		 * @param numeric destClass determines if the destination of the message is of class Traveler(0) or Group(1)
		 * @return array returns an array of objects with three main elements ("validDest","BlockedByDest","SuspendedDest")
		 */
		function filterValidDestinations($dest, $destClass = constants::TRAVELER, $isReply = false ){
			
			$conn = $this->file_factory->getClass('Connection');
			$rs = $this->file_factory->getClass('Recordset',array($conn));
			/**
			 * validDest 		: valid recipients (display purposes)
			 * blockedByDest 	: sender is blocked by message recipient
			 * suspendedDest 	: suspended recipients
			 * failedDest		: unknown recepients
			 * trueDest			: recipients that would be entered in the database
			 */
			$filteredDest = array("validDest" => array(), "blockedByDest" => array(), "suspendedDest" => array(), "failedDest" => array(), 'trueDest' => array() );
			
			if( $isReply ){
				$sendableFactory = $this->file_factory->invokeStaticClass('SendableFactory');
				$tmpDest = $sendableFactory->Create($dest);
				
				if( in_array(get_class($tmpDest),array('Moderator','AdminGroup','FunGroup')) )
					$filteredDest['validDest'] = $filteredDest['trueDest'] = array($tmpDest);
				else{
					if( $tmpDest->isSuspended() )
						$filteredDest["suspendedDest"][] = $tmpDest->getUsername();
					elseif( $tmpDest->isBlocked($this->traveler->getTravelerID()) )
						$filteredDest['blockedByDest'][] = $tmpDest->getUsername();
					else
						$filteredDest['validDest'] = $filteredDest['trueDest'] = array($tmpDest);	
				}	
				
				$dest = array();	
				
			}
			else{
				if( constants::GROUP == $destClass ){

					$tmpDest = preg_grep("/^([a-zA-Z0-9\-_\s\.]*)$/",$dest);
					$filteredDest["failedDest"] = array_diff($dest,$tmpDest);
					$dest = preg_replace('/\s*,\s*/','","',$tmpDest);
					
					if( count($dest) ){
						$sqlquery = "SELECT groupID,name,isSuspended FROM tblGroup " .
							"WHERE name IN (" . implode(",",preg_replace("/^([a-zA-Z0-9\-_\s\.]*)$/","\"" . addslashes("$1") . "\"",$dest)) . ")";
						$myQuery = $rs->Execute($sqlquery);
						
						$groupFactory =  GroupFactory::instance();
						while( $row = mysql_fetch_array($myQuery)){
							if($row["isSuspended"])
								$filteredDest["suspendedDest"][] = $row["name"];
							else{
							 	$nGroup = $groupFactory->create( array($row["groupID"]) );
								$filteredDest["validDest"][] = $nGroup[0];
								$filteredDest["trueDest"][] =  $nGroup[0];
							}	
							$dest = array_diff($dest,array($row["name"]));	
						}
					}		
					
				}
				else{
					
					$tmpDest = preg_grep("/^([a-zA-Z0-9\-_\.]*)$/",$dest);
					$filteredDest["failedDest"] = array_diff($dest,$tmpDest);
					$dest = preg_replace('/\s/','',$tmpDest);
					
					if( count($dest) ){
						$sqlquery = "SELECT tblTraveler.travelerID,username,userID,tblTraveler.isSuspended FROM tblTraveler " .
							"LEFT JOIN tblBlockedUsers ON ( tblTraveler.travelerID = tblBlockedUsers.travelerID AND tblBlockedUsers.userID = " . $this->traveler->getTravelerID() . ") 
							WHERE username IN (" . implode(",",preg_replace("/^([a-zA-Z0-9\-_]*)$/","\"" . addslashes("$1") . "\"",$dest)) . ") 
							AND tblTraveler.travelerID NOT in (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor)
							AND username <> '" . $this->traveler->getUserName() . "'
							GROUP BY tblTraveler.travelerID" ;

						$myQuery = $rs->Execute($sqlquery);
						while( $row = mysql_fetch_array($myQuery)){
							 if($row["isSuspended"])
							 	$filteredDest["suspendedDest"][] = $row["username"];
							 elseif( 0 < $row["userID"] )
							 	$filteredDest["blockedByDest"][] = $row["username"];
							 else{
							 	$nTraveler = $this->file_factory->getClass("Traveler",array($row["travelerID"]));
								$filteredDest["validDest"][] = $nTraveler;
								$filteredDest["trueDest"][] = $nTraveler;	
							 }	
							 $dest = array_diff($dest,array($row["username"]));			
						}	
					}
				}
			}
			$filteredDest["failedDest"] = array_merge($filteredDest["failedDest"],$dest);
			return $filteredDest;
			
		}
		
		function getDestinationSuggestions(){
			
			$suggestList = array();
			if( 0 < $this->session_manager->get("gID") ){
				 $factory =  GroupFactory::instance();
				 $sGroup = $factory->create($this->session_manager->get("gID"));
				 $suggestList[] = '"' . $sGroup->getName() . '"';
				 //get the subgroups
				 $subGroups = $sGroup->getSubGroups();
				 if(is_array($subGroups)){
					foreach($subGroups as $subGrp)
						$suggestList[] = '"' . $subGrp->getName() . '"';
				 }
			}	
			else{
				foreach( $this->traveler->getFriends() as $myFriend)
					$suggestList[] = '"' . $myFriend->getUserName() . '"';
			}
			return $suggestList;
			
		}
		
		function getValidMessageActions($message){
			
			$folder = $this->message_helper->getFolder();
			$messageActions = array(constants::BACK);
			
			if( in_array($folder,array(constants::INBOX,constants::INQUIRY)) && ( $message->getSource() instanceof Traveler || ( $message->getSource() instanceof Group && $message->getSource()->getAdministrator()->getTravelerID() != $this->traveler->getTravelerID())) )
				$messageActions[] = constants::REPLY;
			if( constants::DRAFTS == $folder )
				$messageActions[] = constants::EDIT;
			if( constants::TRASH != $folder )
				$messageActions[] = constants::MOVE_TO_TRASH;
			else
				$messageActions[] = constants::DELETE;	
			return $messageActions;
		}
		
		/**
		 * end of misc functions
		 */ 
		
	}
	 
	 
?>
