<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	abstract class AbstractProgramStepsController implements iController{
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,
		
		$isLogged	  = false,
		
		$isMemberLogged = false,
		
		$isAdminLogged = false,
		
		$tpl			= NULL;
	
		
		function __construct(){
			
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template' , array ( 'path' => '' ));
			
			$this->file_factory->registerClass('ProgramStep');
			$this->file_factory->registerClass('Traveler');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
		}
		
		function performAction(){
		
			
			$params = array();
			$params['mode']  = ( isset($_POST['mode']))? $_POST['mode'] : 'ViewForm' ;
			$params['mode']  = ( isset($_GET['mode']))? $_GET['mode'] : $params['mode'];
		
			$params['programStep']  = ( isset($_POST['programStep']))? $_POST['programStep'] : array() ;
			
			switch($params['mode']){

				case "save":
					
					
					$this->_saveProgramSteps($params);
					
					break;

				default:
				
					$this->_viewProgramSteps($params);
								
			}		
		
		}
		
		
		function _defineCommonAttributes($_params = array()){
			
			//check if user is currently logged in
			if ($this->obj_session->get('travelerID') > 0) {
				$this->isLogged		=	true ;
				$this->loggedUser = $this->file_factory->getClass ('Traveler',array($this->obj_session->get('travelerID')));
			
			}
		
				
			//  create generic template
				$this->tpl = $this->file_factory->getClass('Template');
				$this->tpl->set_path("travellog/views/");
			
		}
		
		function _allowAdminPrivileges($_privilege = false){
			if (!$_privilege){
				header("location:index.php");
				exit;
			}
		}
		
	
		function _saveProgramSteps($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			// add new steps
			foreach($_params['programStep'] as $eachStepID){
				$this->loggedUser->addProgramStep($eachStepID);
			}

			// removed currently added steps, if already unchecked
			foreach($this->loggedUser->getAllAddedProgramSteps() as $eachAddedStep){
				if (!in_array($eachAddedStep->getProgramStepID() , $_params['programStep']))	
					$this->loggedUser->removeProgramStep($eachAddedStep->getProgramStepID());
			}
		
			header("location:programsteps.php");
				
		}
		
		
		function _viewProgramSteps($_params = array() ) {
		
			$this->_defineCommonAttributes($_params);
			
			$this->file_factory->invokeStaticClass('ProgramStep','initConn');
			$allProgramSteps = $this->file_factory->invokeStaticClass('ProgramStep','getAllProgramSteps');

			$this->tpl->set('loggedUser', $this->loggedUser);
			$this->tpl->set('allProgramSteps', $allProgramSteps);

			$this->tpl->out('tpl.ViewMyNextSteps.php');
			
		}
		
	}
?>