<?php
	/*
	 * Class.RegisterLoginController.php
	 * Created on Apr 3, 2008
	 */
	
	require_once 'Class.Constants.php';
	require_once 'travellog/controller/Class.AbstractLoginController.php';
	
	class RegisterLoginController extends AbstractLoginController{
		
		function __construct( $args = array() ){
			parent::__construct();
			$this->file_factory->registerClass("RegisterLoginHelper", array("path" => "travellog/helper/"));
			
			$this->login_helper = $this->file_factory->invokeStaticClass("RegisterLoginHelper","getInstance");
			$this->login_helper->setAttributes($args);
		}
		
		function performAction(){
			
			switch( $this->login_helper->getAction() ){
				case constants::AUTO_LOGIN:
					return $this->activateAndLogin();
	 				break;
	 			default:
	 				header('location: register.php');
	 				exit;
	 				break;		
			}	
				
		}
		
		function activateAndLogin(){
			
			$ref = $this->login_helper->getActivationKey();
	 		$crypt = $this->file_factory->getClass("Crypt");
			$myString = str_replace(" ","+",$ref);
			$travID = $crypt->decrypt($myString); 
			
			if( !is_numeric($travID) ){
	 			header("location: register.php");
	 			exit;
	 		}	
			
			$traveler = $this->file_factory->getClass("Traveler",array($travID));
	 		if( !$traveler->isActive() ){
	 			// email user
				try{
					$mailGenerator = $this->file_factory->getClass("MailGenerator", array($travID,3));
					$mailGenerator->send();	
				}
				catch( exception $e ){}	
	 		}
	 			
			$crypt = $this->file_factory->getClass("Crypt");
			$cryptTravID = $crypt->encrypt($travID);
			$cryptTravID = substr($cryptTravID,0,strlen($cryptTravID)-1);
			return $this->startSession($travID,$cryptTravID);	
		}
		
	}
	 
?>
