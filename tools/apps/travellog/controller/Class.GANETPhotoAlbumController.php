<?php
/*
 * Created on 11 12, 07
 * 
 * Author: J. LLano
 * Purpose: The default controller for "Photo" module in goabroad.net
 * 
 */
 
 require_once('travellog/controller/Class.AbstractPhotoAlbumController.php');
 
 class GANETPhotoAlbumController extends AbstractPhotoAlbumController {
 	function performAction(){
		parent::performAction();
	}
 }
?>
