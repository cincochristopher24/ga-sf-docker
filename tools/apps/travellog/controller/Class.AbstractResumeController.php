<?php
/*
 * Created on Nov 27, 2007
 */
 
ob_start();
require_once('travellog/model/Class.SessionManager.php');
require_once('travellog/controller/Class.IController.php');
require_once('travellog/factory/Class.FileFactory.php');
require_once('Class.Template.php');
require_once('Class.FormHelpers.php');
require_once('travellog/model/Class.Country.php');
require_once('travellog/model/Class.ResumeOptions.php');
require_once('travellog/model/Class.Resume.php');
require_once('travellog/model/Class.WorkExperience.php');
require_once('travellog/model/Class.TravelerProfile.php');
require_once('travellog/model/Class.SubNavigation.php');
require_once('travellog/model/Class.HelpText.php');


if(isset($_GET['weID']))$workexperienceID = $_GET['weID'];
$errorcode = '';





if(!isset($_GET['action']))$_GET['action']='view';



class AbstractResumeController {  
	
	protected $file_factory = NULL;
	
	
	public function __construct() {
		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');

		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance();
		
		// set default path for templates, css, and header footer views
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
	}
	
	
	function performAction(){
		
		try
		{
			
			// Form Options/Checkboxes/Radios

			$pref_employ = array(
													1 => 'Part Time',
													2 => 'Full Time',
													3 => 'Part Time or Full Time');
			
			// function call for static functions used in select boxes
			$selcountry = Country::getCountryList();
			$selacadDegree = ResumeOptions::getAcadDegreeList();
			$major = ResumeOptions::getInternTypeList();
			$teach_pos = ResumeOptions::getTeachingPositionList();
			$engprof = ResumeOptions::getEnglishProfList();
			$teach_yrs = ResumeOptions::getTeachYearsList();
			$pref_teachlevel = ResumeOptions::getTeachingLevelList();
			$job = ResumeOptions::getInternTypeList();
			$dateedited = date('Y-m-j h:i:s');
			
					
		switch($_GET["action"])
		{
		
				
				case 'view':
					
					
					if($this->obj_session->get('travelerID') == 0 ) header("location: /index.php");
					
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');		
					
					$travelerprofile = new TravelerProfile($this->obj_session->get('travelerID')); // for traveler profile
					$resume = new Resume($this->obj_session->get('travelerID')); // for resume
					
					$workexpval = $resume->getWorkExpList(); // gets list of work experience(s) 
					
					$gender = $travelerprofile->getGender();
					$age = $travelerprofile->getAge();
					
					if ($gender == NULL){		 			
				 			$gender = "";	
			 		}elseif($gender == "M"){ // male
			 			$gender = "Male";
			 		}elseif($gender == "F"){ // female
			 			$gender = "Female";		 				
			 		}
			 		
			 		$viewresumetmp = new Template();
					$viewresumetmp->set('fname', $travelerprofile->getFirstName());
					$viewresumetmp->set('lname', $travelerprofile->getLastName());
					$viewresumetmp->set('email', $travelerprofile->getEmail());
			 		$viewresumetmp->set('age', $age);
			 		$viewresumetmp->set('gender', $gender);
			 		$viewresumetmp->set('phone', $travelerprofile->getPhone());
			 		$viewresumetmp->set('address1', $travelerprofile->getAddress1());
			 		$viewresumetmp->set('address2', $travelerprofile->getAddress2());
					$viewresumetmp->set('acadDegree', $resume->getacaddegree());
					$viewresumetmp->set('interntype', $resume->getmajor());
					$viewresumetmp->set('univName', $resume->getunivName());
					$viewresumetmp->set('univlocation', $resume->getunivcountry());
					$viewresumetmp->set('acadDegree2', $resume->getacaddegree2());
					$viewresumetmp->set('interntype2', $resume->getmajor2());
					$viewresumetmp->set('univName2', $resume->getunivName2());
					$viewresumetmp->set('univlocation2', $resume->getunivcountry2());
					$viewresumetmp->set('viewemploy', $resume->getisCurrEmployed());
					$viewresumetmp->set('viewteach', $resume->getisTeacher());
					$viewresumetmp->set('currJob', $resume->getcurrJob());
					$viewresumetmp->set('englishproficiency', $resume->getengprof());
					$viewresumetmp->set('teachingyears', $resume->getteachyrs());
					
					$certnames = array();
					if($resume->getcertName() != NULL && $resume->getyearObtained() != NULL)
					{
						$certnames[] = $resume->getcertName();
						$certnames[] = $resume->getyearObtained();
					}
					if($resume->getcertName2() != NULL && $resume->getyearObtained2() != NULL)
					{
						$certnames[] = $resume->getcertName2();
						$certnames[] = $resume->getyearObtained2();
					}	
					if($resume->getcertName3() != NULL && $resume->getyearObtained3() != NULL)
					{
						$certnames[] = $resume->getcertName3();
						$certnames[] = $resume->getyearObtained3();
					}
					
					$viewresumetmp->set('certnames', $certnames);
					if($resume->getteachingpositionID() != 34)
						$viewresumetmp->set('teachingposition', $resume->getteachpos());
					else
						$viewresumetmp->set('teachingposition', $resume->getotherteachingposition());
					
					if($resume->getpreferredteachinglevelID() != 9)
						$viewresumetmp->set('preferredteachinglevel', $resume->getprefteachlevel());	
					else
						$viewresumetmp->set('preferredteachinglevel', $resume->getotherteachinglevel());
						
					$viewresumetmp->set('prefEmployment', $resume->getprefEmployment());
					$viewresumetmp->set('jobtype', $resume->getjobtype());
					$viewresumetmp->set('prefCountry', $resume->getprefcountry());
					$viewresumetmp->set('prefCity', $resume->getprefCity());
					$viewresumetmp->set('workWhen', $resume->getworkWhen());
					$viewresumetmp->set('moreDetails', $resume->getmoreDetails());
					$viewresumetmp->set('workexpcount', $resume->getWorkExpCount());
					$viewresumetmp->set('qworkexp', $resume->getWorkExpList());
					$viewresumetmp->set('workexperience', $workexpval);
					$viewresumetmp->set('subNavigation', $subNavigation);
					$viewresumetmp->set('travelerID', $this->obj_session->get('travelerID'));
					$viewresumetmp->set('resourcefilecount', $resume->getResumeResourceFile());
					
					// function call for resume photo
					$viewresumetmp->set('tmpphoto', $resume->getPrimaryPhoto()->getPhotoLink());
					
					
					$viewresumetmp->out('travellog/views/tpl.ViewResume.php');
					
					
					break;
					
					
				case 'add':
			
					if($this->obj_session->get('travelerID') == 0 ) header("location: /index.php");	
					
					$helptext = new HelpText();
					
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');
					
					$teachproftmp = new Template(); // template instantiation for included teacher's profile template
					$teachproftmp->set_vars( array(
						'teachposOptions' => FormHelpers::CreateOptions($teach_pos, ''),
						'txtOtherCurrTeachJob' => (isset($txtOtherCurrTeachJob))? $txtOtherCurrTeachJob : '',
						'engprofOptions' => FormHelpers::CreateOptions($engprof, ''),
						'teachyrsOptions' => FormHelpers::CreateOptions($teach_yrs, ''),
						'prefteachinglevelOptions' => FormHelpers::CreateOptions($pref_teachlevel, ''),
						'txtOtherPrefTeachingLevel' => (isset($txtOtherPrefTeachingLevel))? $txtOtherPrefTeachingLevel : '', 
						'lstCurrTeachJob' => (isset($lstCurrTeachJob))? $lstCurrTeachJob : '',
						'lstPrefTeachingLevel' => (isset($lstPrefTeachingLevel))? $lstPrefTeachingLevel : ''
						
					));
					
					
					$newslettersubstmp = new Template(); // template instantiation for included show resume button template
					$newslettersubstmp->set_vars( array(
						
						'showresume' =>FormHelpers::CreateCheckbox( array(1 => 'Show Resume'), 'chkShowResume', array(2))
					
					));
					
			
					$travelerprofile = new TravelerProfile($this->obj_session->get('travelerID')); // for traveler profile
					$birthday = $travelerprofile->getBirthDay();	
					$gender	= $travelerprofile->getGender();
					
					// from the given birthday, the values are segregated to fill in the birthdate listboxes
					$bday = explode('-', $birthday);
					$varyear = $bday[0];
					$varmonth = $bday[1];
					$varday = $bday[2];
					
			
					if ($gender == NULL){		 			
				 			$gender = 0;	
			 		}elseif($gender == "M"){ // male
			 			$gender = 1;
			 		}elseif($gender == "F"){ // female
			 			$gender = 2;		 				
			 		}
				 		
					
					$formtmp = new Template(); // template instantiation for the main template
					$formtmp->set_vars( array(
						'act' => 'create',
						'errors' => array(),
						'labelaction' => 'Add Resume',
						'errorcode' => '0',
						'helptext' => $helptext->getHelpText('resume'),
						'txtFirstName' => $travelerprofile->getFirstName(),
						'txtLastName' => $travelerprofile->getLastName(),
						'txtEmail' => $travelerprofile->getEmail(),
						'monthOptions' => $varmonth,
						'lstDay' => $varday,
						'lstYear' => $varyear,
						'lstGender' => $gender,
						'txtPhone' => (isset($txtPhone))? $txtPhone : '',
						'txaAddress1' => (isset($txaAddress1))? $txaAddress1 : '',
						'txaAddress2' => (isset($txaAddress2))? $txaAddress2 : '',
						'acaddegreeOptions' => FormHelpers::CreateOptions($selacadDegree, ''),
						'acaddegreeOptions2' => FormHelpers::CreateOptions($selacadDegree, ''),
						'majorOptions' => FormHelpers::CreateOptions($major, ''),
						'majorOptions2' => FormHelpers::CreateOptions($major, ''),
						'txtUnivName' => (isset($txtUnivName))? $txtUnivName : '',
						'txtUnivName2' => (isset($txtUnivName2))? $txtUnivName2 : '',
						'univlocation' => FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName'),
						'univlocation2' => FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName'),
						'txtcertname' => (isset($txtcertname))? $txtcertname : '',
						'txtyearobtained' => (isset($txtyearobtained))? $txtyearobtained : '',
						'txtcertname2' => (isset($txtcertname2))? $txtcertname2 : '',
						'txtyearobtained2' => (isset($txtyearobtained2))? $txtyearobtained2 : '',
						'txtcertname3' => (isset($txtcertname3))? $txtcertname3 : '',
						'txtyearobtained3' => (isset($txtyearobtained3))? $txtyearobtained3 : '',
						'radIsCurrEmployed' => (isset($radIsCurrEmployed))? $radIsCurrEmployed : '', 
						'radIsTeacher' => (isset($radIsTeacher))? $radIsTeacher : '',
						'txtCurrJob' => (isset($txtCurrJob))? $txtCurrJob : '',
						'prefemployment' => FormHelpers::CreateOptions($pref_employ, ''),
						'jobtypeOptions' => FormHelpers::CreateOptions($job, ''),
						'prefcountryOptions' =>FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName'),
						'txtPrefCity' => (isset($txtPrefCity))? $txtPrefCity : '',
						'txtWorkWhen' => (isset($txtWorkWhen))? $txtWorkWhen : '',
						'txaMoreDetails' => (isset($txaMoreDetails))? $txaMoreDetails : '',
						'subNavigation' => $subNavigation,
						'btnSubmit' => 'Next',
						
						
						'newsletterSubscribe' => $newslettersubstmp->fetch('travellog/views/tpl.IncFrmNewsletterSubscribe.php'),
						
						'teacherProfile' =>$teachproftmp->fetch('travellog/views/tpl.IncFrmTeacherProfile.php')
						));
						
					$formtmp->out('travellog/views/tpl.FrmResumeName.php');
				
					break;
			
				
				case 'create':
			
					if($this->obj_session->get('travelerID') == 0 ) header("location: /index.php");	
					
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');
					
					$errors = array(); // used to hold error messages
					
					if (strlen(trim($_POST['txtFirstName'])) == 0) $errors[] = 'First Name is a required field!';
					if (strlen(trim($_POST['txtLastName'])) == 0) $errors[] = 'Last Name is a required field!';
					if ($_POST['monthOptions'] == 0 || $_POST['lstDay'] == 0 || $_POST['lstYear'] == 0) $errors[] = 'Birth Date is a required field!';
					if ($_POST['lstGender'] == 0)  $errors[] = 'Gender is a required field!';
					if ($_POST['lstSelAcadDegree'] == 0) $errors[] = 'Highest Educational Level is a required field!';
					if ($_POST['lstMajor'] == 0) $errors[] = 'Major is a required field!';
					if (strlen(trim($_POST['txtUnivName'])) == 0) $errors[] = 'University is a required field!';
					if ($_POST['lstUnivCountry'] == 0) $errors[] = 'Location of University is a required field!';
					if (!isset($_POST['radIsCurrEmployed'])) $errors[] = 'Current Employment is a required field!';
					if (isset($_POST['radIsCurrEmployed']) && $_POST['radIsCurrEmployed'] == 1 && !isset($_POST['radIsTeacher'])) $errors[] = 'Teacher is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 2 && strlen(trim($_POST['txtCurrJob'])) == 0) $errors[] = 'Current Job is a required field!';
					if ($_POST['lstPrefEmployment'] == 0)  $errors[] = 'Preferred Employment is a required field!';
					if ($_POST['lstJobType'] == 0) $errors[] = 'Job Type is a required field!';
					if ($_POST['lstPrefCountry'] == 0) $errors[] = 'Preferred Country is a required field!';
					if (strlen(trim($_POST['txtWorkWhen'])) == 0) $errors[] = 'Begin work is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstCurrTeachJob'] == 0) $errors[] = 'Teaching position is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstCurrTeachJob'] == 34 && strlen(trim($_POST['txtOtherCurrTeachJob'])) == 0) $errors[] = 'Other teaching position is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstEnglishProf'] == 0)  $errors[] = 'English proficiency is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstYearsTeachExperience'] == 0)  $errors[] = 'Years of experience is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstPrefTeachingLevel'] == 0) $errors[] = 'Teaching level is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstPrefTeachingLevel'] == 9 && strlen(trim($_POST['txtOtherPrefTeachingLevel'])) == 0) $errors[] = 'Other teaching level is a required field!';
					
					
					if(count($errors)) // returns the previous page if error was encountered
					{
							
						$helptext = new HelpText();
						
						$newslettersubstmp = new Template(); // template instantiation for included show resume template
						$newslettersubstmp->set('showresume', FormHelpers::CreateCheckbox( array(1 => 'Show Resume'), 'chkShowResume', array(isset($_POST['chkShowResume']))));
						
						$teachproftmp = new Template(); // template instantiation for included teacher's profile template
						$teachproftmp->set('teachposOptions', FormHelpers::CreateOptions($teach_pos, $_POST['lstCurrTeachJob']));
						$teachproftmp->set('txtOtherCurrTeachJob', isset($_POST['txtOtherCurrTeachJob'])? $_POST['txtOtherCurrTeachJob'] : '');
						$teachproftmp->set('lstCurrTeachJob', $_POST['lstCurrTeachJob']);
						$teachproftmp->set('engprofOptions', FormHelpers::CreateOptions($engprof, $_POST['lstEnglishProf']));
						$teachproftmp->set('teachyrsOptions', FormHelpers::CreateOptions($teach_yrs, $_POST['lstYearsTeachExperience']));
						$teachproftmp->set('prefteachinglevelOptions', FormHelpers::CreateOptions($pref_teachlevel, $_POST['lstPrefTeachingLevel']));
						$teachproftmp->set('txtOtherPrefTeachingLevel', isset($_POST['txtOtherPrefTeachingLevel'])? $_POST['txtOtherPrefTeachingLevel'] : '');
						$teachproftmp->set('lstPrefTeachingLevel', $_POST['lstPrefTeachingLevel']);
						
						
						$workproftmp = new Template(); // template instantiation for the main template
						$workproftmp->set('act', "create");
						$workproftmp->set('labelaction', "Add Resume");
						$workproftmp->set('errorcode', '1');
						$workproftmp->set('helptext', $helptext->getHelpText('resume'));
						$workproftmp->set('txtFirstName', $_POST['txtFirstName']);
						$workproftmp->set('txtLastName', $_POST['txtLastName']);
						$workproftmp->set('txtEmail', $_POST['txtEmail']);
						$workproftmp->set('monthOptions', $_POST['monthOptions']);
						$workproftmp->set('lstDay', $_POST['lstDay']);
						$workproftmp->set('lstYear', $_POST['lstYear']);
						$workproftmp->set('lstGender', $_POST['lstGender']);
						$workproftmp->set('txtPhone', $_POST['txtPhone']);
						$workproftmp->set('txaAddress1', $_POST['txaAddress1']);
						$workproftmp->set('txaAddress2', $_POST['txaAddress2']);
						$workproftmp->set('acaddegreeOptions', FormHelpers::CreateOptions($selacadDegree, $_POST['lstSelAcadDegree']));
						$workproftmp->set('majorOptions', FormHelpers::CreateOptions($major, $_POST['lstMajor']));
						$workproftmp->set('txtUnivName', $_POST['txtUnivName']);
						$workproftmp->set('univlocation', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $_POST['lstUnivCountry']));
						$workproftmp->set('acaddegreeOptions2', FormHelpers::CreateOptions($selacadDegree, $_POST['lstSelAcadDegree2']));
						$workproftmp->set('majorOptions2', FormHelpers::CreateOptions($major, $_POST['lstMajor2']));
						$workproftmp->set('txtUnivName2', $_POST['txtUnivName2']);
						$workproftmp->set('univlocation2', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $_POST['lstUnivCountry2']));
						$workproftmp->set('txtcertname', $_POST['txtcertname']);
						$workproftmp->set('txtyearobtained', $_POST['txtyearobtained']);
						$workproftmp->set('txtcertname2', $_POST['txtcertname2']);
						$workproftmp->set('txtyearobtained2', $_POST['txtyearobtained2']);
						$workproftmp->set('txtcertname3', $_POST['txtcertname3']);
						$workproftmp->set('txtyearobtained3', $_POST['txtyearobtained3']);
						$workproftmp->set('radIsCurrEmployed', isset($_POST['radIsCurrEmployed'])? $_POST['radIsCurrEmployed'] : '');
						$workproftmp->set('radIsTeacher', isset($_POST['radIsTeacher'])? $_POST['radIsTeacher'] : '');
						$workproftmp->set('txtCurrJob', isset($_POST['txtCurrJob'])? $_POST['txtCurrJob'] : '');
						$workproftmp->set('prefemployment', FormHelpers::CreateOptions($pref_employ, $_POST['lstPrefEmployment']));
						$workproftmp->set('jobtypeOptions', FormHelpers::CreateOptions($job, $_POST['lstJobType']));
						$workproftmp->set('prefcountryOptions', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $_POST['lstPrefCountry']));
						$workproftmp->set('txtPrefCity', $_POST['txtPrefCity']);
						$workproftmp->set('txtWorkWhen', $_POST['txtWorkWhen']);
						$workproftmp->set('txaMoreDetails', $_POST['txaMoreDetails']);
						$workproftmp->set('subNavigation', $subNavigation);
						$workproftmp->set('btnSubmit', 'Next');
						
						$workproftmp->set('newsletterSubscribe', $newslettersubstmp->fetch('travellog/views/tpl.IncFrmNewsletterSubscribe.php'));
						$workproftmp->set('teacherProfile', $teachproftmp->fetch('travellog/views/tpl.IncFrmTeacherProfile.php'));
						
						$workproftmp->set('errors', $errors);
						
						$workproftmp->out('travellog/views/tpl.FrmResumeName.php');
						
					}
					
					else // proceed if no error was encountered
					{
						$workproftmp = new Template(); // template instantiation for the main template
						$workproftmp->set('act', "workform");
						
						$travelerprofile = new TravelerProfile($this->obj_session->get('travelerID')); // for traveler profile
						$travelerprofile->setFirstName($_POST['txtFirstName']); 		
						$travelerprofile->setLastName($_POST['txtLastName']);
						$travelerprofile->setBirthDay($_POST['lstYear']."-".$_POST['monthOptions']."-".$_POST['lstDay']);
						$travelerprofile->setGender($_POST['lstGender']);
						$travelerprofile->setPhone(isset($_POST['txtPhone'])? $_POST['txtPhone'] : '');
						$travelerprofile->setAddress1(isset($_POST['txaAddress1'])? $_POST['txaAddress1'] : '');
						$travelerprofile->setAddress2(isset($_POST['txaAddress2'])? $_POST['txaAddress2'] : '');
						$travelerprofile->Update();	
						
						$resume = new Resume(); // for resume
						$resume->settravelerID($this->obj_session->get('travelerID'));
						//$resume->setphone(isset($_POST['txtPhone'])? $_POST['txtPhone'] : '');
						//$resume->setaddress1(isset($_POST['txaAddress1'])? $_POST['txaAddress1'] : '');
						//$resume->setaddress2(isset($_POST['txaAddress2'])? $_POST['txaAddress2'] : '');
						$resume->setacaddegreeID(isset($_POST['lstSelAcadDegree'])? $_POST['lstSelAcadDegree'] : '');
						$resume->setmajorID(isset($_POST['lstMajor'])? $_POST['lstMajor'] : '');
						$resume->setunivName(isset($_POST['txtUnivName'])? $_POST['txtUnivName'] : '');
						$resume->setunivCountryID(isset($_POST['lstUnivCountry'])? $_POST['lstUnivCountry'] : '');
						$resume->setacaddegreeID2(isset($_POST['lstSelAcadDegree2'])? $_POST['lstSelAcadDegree2'] : '');
						$resume->setmajorID2(isset($_POST['lstMajor2'])? $_POST['lstMajor2'] : '');
						$resume->setunivName2(isset($_POST['txtUnivName2'])? $_POST['txtUnivName2'] : '');
						$resume->setunivCountryID2(isset($_POST['lstUnivCountry2'])? $_POST['lstUnivCountry2'] : '');
						$resume->setcertName(isset($_POST['txtcertname'])? $_POST['txtcertname'] : '');
						$resume->setyearObtained(isset($_POST['txtyearobtained'])? $_POST['txtyearobtained'] : '');
						$resume->setcertName2(isset($_POST['txtcertname2'])? $_POST['txtcertname2'] : '');
						$resume->setyearObtained2(isset($_POST['txtyearobtained2'])? $_POST['txtyearobtained2'] : '');
						$resume->setcertName3(isset($_POST['txtcertname3'])? $_POST['txtcertname3'] : '');
						$resume->setyearObtained3(isset($_POST['txtyearobtained3'])? $_POST['txtyearobtained3'] : '');
						$resume->setisCurrEmployed(isset($_POST['radIsCurrEmployed'])? $_POST['radIsCurrEmployed'] : '');
						$resume->setisTeacher(isset($_POST['radIsTeacher'])? $_POST['radIsTeacher'] : '');
						$resume->setcurrJob(isset($_POST['txtCurrJob'])? $_POST['txtCurrJob'] : '');
						$resume->setprefEmployment(isset($_POST['lstPrefEmployment'])? $_POST['lstPrefEmployment'] : '');
						$resume->setmoreDetails(isset($_POST['txaMoreDetails'])? $_POST['txaMoreDetails'] : '');
						$resume->setjobTypeID(isset($_POST['lstJobType'])? $_POST['lstJobType'] : '');
						$resume->setprefCountryID(isset($_POST['lstPrefCountry'])? $_POST['lstPrefCountry'] : '');
						$resume->setprefCity(isset($_POST['txtPrefCity'])? $_POST['txtPrefCity'] : '');
						$resume->setworkWhen(isset($_POST['txtWorkWhen'])? $_POST['txtWorkWhen'] : '');
						$resume->setenglishproficiencyID(isset($_POST['lstEnglishProf'])? $_POST['lstEnglishProf'] : '');
						$resume->setpreferredteachinglevelID(isset($_POST['lstPrefTeachingLevel'])? $_POST['lstPrefTeachingLevel'] : '');
						$resume->setteachingpositionID(isset($_POST['lstCurrTeachJob'])? $_POST['lstCurrTeachJob'] : '');
						$resume->setteachingyearsID(isset($_POST['lstYearsTeachExperience'])? $_POST['lstYearsTeachExperience'] : '');
						$resume->setotherteachingposition(isset($_POST['txtOtherCurrTeachJob'])? $_POST['txtOtherCurrTeachJob'] : '');
						$resume->setotherteachinglevel(isset($_POST['txtOtherPrefTeachingLevel'])? $_POST['txtOtherPrefTeachingLevel'] : '');
						$resume->setshowresume(isset($_POST['chkShowResume'])? $_POST['chkShowResume'] : '');
						$resume->setdatecreated($dateedited);
						$resume->Create();
						
						$workproftmp->set('subNavigation', $subNavigation);
						$workproftmp->set('travelerID', $this->obj_session->get('travelerID'));
						
						$workproftmp->out('travellog/views/tpl.FrmWorkPhoto.php');
					}
					
									
					break;
				
				case 'workform':
			
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');
					
					$workformtmp = new Template(); // template instantiation for the main template
					$workformtmp->set('act', "creatework");
					$workformtmp->set('subNavigation', $subNavigation);
					$workformtmp->set('labelwork', "Add Work Experience");
					$workformtmp->set('txtWE1Position1', isset(${"txtWE1Position1"})? ${"txtWE1Position1"}: ''); 
					$workformtmp->set('txtWE1Employer1', isset(${"txtWE1Employer1"})? ${"txtWE1Employer1"}: '');
					$workformtmp->set('txtWE1JobDesc1', isset(${"txtWE1JobDesc1"})? ${"txtWE1JobDesc1"}: '');
					$workformtmp->set('txtWE1LenServ1', isset(${"txtWE1LenServ1"})? ${"txtWE1LenServ1"}: '');
					
					
					$workformtmp->out('travellog/views/tpl.FrmAddWorkExp.php');
				
					break;
				
				
				case 'creatework':
			
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');
					
					
					// proceed if no error was encountered
					$resume = new Resume($this->obj_session->get('travelerID'));
					
					$workexperiences = array(); // used to hold values for work experience(s)
					for($i=1; $i<=$_POST['hidWID']; $i++)
					{
						$workexperiences[$i - 1] = new WorkExperience($this->obj_session->get('travelerID'));
						$workexperiences[$i - 1]->setJobPosition(isset($_POST['txtWE1Position'.$i])? $_POST['txtWE1Position'.$i] : '');
						$workexperiences[$i - 1]->setEmployer(isset($_POST['txtWE1Employer'.$i])? $_POST['txtWE1Employer'.$i] : '');
						$workexperiences[$i - 1]->setJobDescription(isset($_POST['txtWE1JobDesc'.$i])? $_POST['txtWE1JobDesc'.$i] : '');
						$workexperiences[$i - 1]->setLenServ(isset($_POST['txtWE1LenServ'.$i])? $_POST['txtWE1LenServ'.$i] : '');
						
						if($_POST['txtWE1Position'.$i] != '' || $_POST['txtWE1Employer'.$i] != '' || $_POST['txtWE1JobDesc'.$i] != '' || $_POST['txtWE1LenServ'.$i] != ''){
							$workexperiences[$i - 1]->CreateWork();
						}
					}
				
				
					ob_end_clean();
							
					header("location:resume.php?action=edit");
					
					break;
					
					
				case 'edit':	
				
					if( $this->obj_session->get('travelerID') == 0 ) header("location: /index.php");	
					
					$helptext = new HelpText();
					
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');
				
					$resume = new Resume($this->obj_session->get('travelerID')); // for resume
					$newslettersubstmp = new Template(); // template instantiation for included show resume template
					$newslettersubstmp->set('showresume', FormHelpers::CreateCheckbox( array(1 => 'Show Resume'), 'chkShowResume', array($resume->getshowresume())));
					
					$teachproftmp = new Template(); // template instantiation for included teacher's profile template
					$teachproftmp->set('teachposOptions', FormHelpers::CreateOptions($teach_pos, $resume->getteachingpositionID()));
					$teachproftmp->set('txtOtherCurrTeachJob', $resume->getotherteachingposition());
					$teachproftmp->set('engprofOptions', FormHelpers::CreateOptions($engprof, $resume->getenglishproficiencyID()));
					$teachproftmp->set('teachyrsOptions', FormHelpers::CreateOptions($teach_yrs, $resume->getteachingyearsID()));
					$teachproftmp->set('prefteachinglevelOptions', FormHelpers::CreateOptions($pref_teachlevel, $resume->getpreferredteachinglevelID()));
					$teachproftmp->set('txtOtherPrefTeachingLevel', $resume->getotherteachinglevel());
					$teachproftmp->set('lstCurrTeachJob', $resume->getteachingpositionID());
					$teachproftmp->set('lstPrefTeachingLevel', $resume->getpreferredteachinglevelID());
					
					
					$travelerprofile = new TravelerProfile($this->obj_session->get('travelerID')); // for traveler profile
					$birthday = $travelerprofile->getBirthDay();	
					$gender	= $travelerprofile->getGender();
					
					// value of birthday is segregated to fill in birthdate select boxes
					$bday = explode('-', $birthday);
					$varyear = $bday[0];
					$varmonth = $bday[1];
					$varday = $bday[2];
			
			
					if ($gender == NULL){		 			
				 			$gender = 0;	
			 		}elseif($gender == "M"){ // male
			 			$gender = 1;
			 		}elseif($gender == "F"){ // female
			 			$gender = 2;		 				
			 		}
			 		
					
					$formtmp = new Template(); // template instantiation for the main template
					$formtmp->set('act', "update");
					$formtmp->set('errors', array());
					$formtmp->set('labelaction', "Edit Resume");
					$formtmp->set('errorcode', '0');
					$formtmp->set('helptext', $helptext->getHelpText('resume'));
					$formtmp->set('txtFirstName', $travelerprofile->getFirstName());
					$formtmp->set('txtLastName', $travelerprofile->getLastName());
					$formtmp->set('txtEmail', $travelerprofile->getEmail());
					$formtmp->set('monthOptions', $varmonth);						
					$formtmp->set('lstDay', $varday);
					$formtmp->set('lstYear', $varyear);	
					$formtmp->set('lstGender', $gender);
					$formtmp->set('txtPhone', $travelerprofile->getPhone());
					$formtmp->set('txaAddress1', $travelerprofile->getAddress1());
					$formtmp->set('txaAddress2', $travelerprofile->getAddress2());
					$formtmp->set('acaddegreeOptions',FormHelpers::CreateOptions($selacadDegree,$resume->getacaddegreeID()));
					$formtmp->set('majorOptions',FormHelpers::CreateOptions($major, $resume->getmajorID()));
					$formtmp->set('txtUnivName', $resume->getunivName());
					$formtmp->set('univlocation', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $resume->getunivCountryID()));
					$formtmp->set('acaddegreeOptions2',FormHelpers::CreateOptions($selacadDegree,$resume->getacaddegreeID2()));
					$formtmp->set('majorOptions2',FormHelpers::CreateOptions($major, $resume->getmajorID2()));
					$formtmp->set('txtUnivName2', $resume->getunivName2());
					$formtmp->set('univlocation2', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $resume->getunivCountryID2()));
					$formtmp->set('txtcertname', $resume->getcertName());
					$formtmp->set('txtyearobtained', $resume->getyearObtained());
					$formtmp->set('txtcertname2', $resume->getcertName2());
					$formtmp->set('txtyearobtained2', $resume->getyearObtained2());
					$formtmp->set('txtcertname3', $resume->getcertName3());
					$formtmp->set('txtyearobtained3', $resume->getyearObtained3());
					$formtmp->set('radIsCurrEmployed', $resume->getisCurrEmployed());
					$formtmp->set('radIsTeacher', $resume->getisTeacher());
					$formtmp->set('txtCurrJob', $resume->getcurrJob());
					$formtmp->set('prefemployment', FormHelpers::CreateOptions($pref_employ, $resume->getprefEmployment()));
					$formtmp->set('jobtypeOptions', FormHelpers::CreateOptions($job, $resume->getjobTypeID()));
					$formtmp->set('prefcountryOptions', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $resume->getprefCountryID()));
					$formtmp->set('txtPrefCity', $resume->getprefCity());
					$formtmp->set('txtWorkWhen', $resume->getworkWhen());
					$formtmp->set('txaMoreDetails', $resume->getmoreDetails());
					$formtmp->set('workexpcount', $resume->getWorkExpCount());
					$formtmp->set('worklist', $resume->getWorkExpList());
					$formtmp->set('btnSubmit', 'Submit');
					
					
					/*$workexpetc = new Template(); // template instantiation for included work experience(s) and resourcefile template
					$workexpetc->set('act', "workform");
					$workexpetc->set('workexpcount', $resume->getWorkExpCount());
					$workexpetc->set('qworkexp', $resume->getWorkExpList());
					$workexpetc->set('resourcefilecount', $resume->getResumeResourceFile());
					$workexpetc->set('travID', $travelerID);
					*/
					
					$formtmp->set('newsletterSubscribe', $newslettersubstmp->fetch('travellog/views/tpl.IncFrmNewsletterSubscribe.php'));
					$formtmp->set('teacherProfile', $teachproftmp->fetch('travellog/views/tpl.IncFrmTeacherProfile.php'));
					//$formtmp->set('worketc', $workexpetc->fetch('travellog/views/tpl.IncFrmWorkExpEtc.php'));
					
					// function call for resume photo
					if($resume->getPhotos()){
						$photo =$resume->getPhotos(); 			
						$tmp = $photo[0]->getPhotoLink();
					}else{
						$tmp = '';	
								
					}	
					
					$tmpphoto = "$tmp";
					
					$formtmp->set('tmpphoto', $resume->getPrimaryPhoto()->getPhotoLink());
					$formtmp->set('travelerID', $this->obj_session->get('travelerID'));
					$formtmp->set('photocount', $resume->getPhotos());
					
					
					$formtmp->set('subNavigation', $subNavigation);
					$formtmp->set('resourcefilecount', $resume->getResumeResourceFile());
					$formtmp->out('travellog/views/tpl.FrmResumeName.php');
					
					
					
					break;	
					
				
				case 'update':
				
					if( $this->obj_session->get('travelerID') == 0 ) header("location: /index.php");	
					
					
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');
					
					$errors = array(); // used to hold error messages
					
					if (strlen(trim($_POST['txtFirstName'])) == 0) $errors[] = 'First Name is a required field!';
					if (strlen(trim($_POST['txtLastName'])) == 0) $errors[] = 'Last Name is a required field!';
					if ($_POST['monthOptions'] == 0 || $_POST['lstDay'] == 0 || $_POST['lstYear'] == 0) $errors[] = 'Birth Date is a required field!';
					if ($_POST['lstGender'] == 0)  $errors[] = 'Gender is a required field!';
					if ($_POST['lstSelAcadDegree'] == 0) $errors[] = 'Highest Educational Level is a required field!';
					if ($_POST['lstMajor'] == 0) $errors[] = 'Major is a required field!';
					if (strlen(trim($_POST['txtUnivName'])) == 0) $errors[] = 'University is a required field!';
					if ($_POST['lstUnivCountry'] == 0) $errors[] = 'Location of University is a required field!';
					if (!isset($_POST['radIsCurrEmployed']))  $errors[] = 'Current Employment is a required field!';
					if (isset($_POST['radIsCurrEmployed']) && $_POST['radIsCurrEmployed'] == 1 && !isset($_POST['radIsTeacher']))  $errors[] = 'Teacher is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 2 && strlen(trim($_POST['txtCurrJob'])) == 0)  $errors[] = 'Current Job is a required field!';
					if ($_POST['lstPrefEmployment'] == 0)  $errors[] = 'Preferred Employment is a required field!';
					if ($_POST['lstJobType'] == 0)  $errors[] = 'Job Type is a required field!';
					if ($_POST['lstPrefCountry'] == 0)  $errors[] = 'Preferred Country is a required field!';
					if (strlen(trim($_POST['txtWorkWhen'])) == 0)  $errors[] = 'Begin work is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstCurrTeachJob'] == 0) $errors[] = 'Teaching position is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstCurrTeachJob'] == 34 && strlen(trim($_POST['txtOtherCurrTeachJob'])) == 0) $errors[] = 'Other teaching position is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstEnglishProf'] == 0)  $errors[] = 'English proficiency is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstYearsTeachExperience'] == 0)  $errors[] = 'Years of experience is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstPrefTeachingLevel'] == 0) $errors[] = 'Teaching level is a required field!';
					if (isset($_POST['radIsTeacher']) && $_POST['radIsTeacher'] == 1 && $_POST['lstPrefTeachingLevel'] == 9 && strlen(trim($_POST['txtOtherPrefTeachingLevel'])) == 0) $errors[] = 'Other teaching level is a required field!';
					
					
					if(count($errors)) // returns the previous page if an error was encountered
					{
						
						$helptext = new HelpText();
						
						$resume = new Resume($this->obj_session->get('travelerID')); // for resume
						$newslettersubstmp = new Template(); // template instantiation for included show resume template
						$newslettersubstmp->set('showresume', FormHelpers::CreateCheckbox( array(1 => 'Show Resume'), 'chkShowResume', array(isset($_POST['chkShowResume']))));
						
						$teachproftmp = new Template(); // template instantiation for teacher's profile template
						$teachproftmp->set('teachposOptions', FormHelpers::CreateOptions($teach_pos, $_POST['lstCurrTeachJob']));
						$teachproftmp->set('txtOtherCurrTeachJob', isset($_POST['txtOtherCurrTeachJob'])? $_POST['txtOtherCurrTeachJob'] : '');
						$teachproftmp->set('lstCurrTeachJob', $_POST['lstCurrTeachJob']);
						$teachproftmp->set('engprofOptions', FormHelpers::CreateOptions($engprof, $_POST['lstEnglishProf']));
						$teachproftmp->set('teachyrsOptions', FormHelpers::CreateOptions($teach_yrs, $_POST['lstYearsTeachExperience']));
						$teachproftmp->set('prefteachinglevelOptions', FormHelpers::CreateOptions($pref_teachlevel, $_POST['lstPrefTeachingLevel']));
						$teachproftmp->set('txtOtherPrefTeachingLevel', isset($_POST['txtOtherPrefTeachingLevel'])? $_POST['txtOtherPrefTeachingLevel'] : '');
						$teachproftmp->set('lstPrefTeachingLevel', $_POST['lstPrefTeachingLevel']);
						
						
						$workproftmp = new Template(); // template instantiation for the main template
						$workproftmp->set('act', "update");
						$workproftmp->set('labelaction', "Edit Resume");
						$workproftmp->set('errorcode', '1');
						$workproftmp->set('helptext', $helptext->getHelpText('resume'));
						$workproftmp->set('txtFirstName', $_POST['txtFirstName']);
						$workproftmp->set('txtLastName', $_POST['txtLastName']);
						$workproftmp->set('txtEmail', $_POST['txtEmail']);
						$workproftmp->set('monthOptions', $_POST['monthOptions']);
						$workproftmp->set('lstDay', $_POST['lstDay']);
						$workproftmp->set('lstYear', $_POST['lstYear']);
						$workproftmp->set('lstGender', $_POST['lstGender']);
						$workproftmp->set('txtPhone', $_POST['txtPhone']);
						$workproftmp->set('txaAddress1', $_POST['txaAddress1']);
						$workproftmp->set('txaAddress2', $_POST['txaAddress2']);
						$workproftmp->set('acaddegreeOptions', FormHelpers::CreateOptions($selacadDegree, $_POST['lstSelAcadDegree']));
						$workproftmp->set('majorOptions', FormHelpers::CreateOptions($major, $_POST['lstMajor']));
						$workproftmp->set('txtUnivName', $_POST['txtUnivName']);
						$workproftmp->set('univlocation', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $_POST['lstUnivCountry']));
						$workproftmp->set('acaddegreeOptions2', FormHelpers::CreateOptions($selacadDegree, $_POST['lstSelAcadDegree2']));
						$workproftmp->set('majorOptions2', FormHelpers::CreateOptions($major, $_POST['lstMajor2']));
						$workproftmp->set('txtUnivName2', $_POST['txtUnivName2']);
						$workproftmp->set('univlocation2', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $_POST['lstUnivCountry2']));
						$workproftmp->set('txtcertname', $_POST['txtcertname']);
						$workproftmp->set('txtyearobtained', $_POST['txtyearobtained']);
						$workproftmp->set('txtcertname2', $_POST['txtcertname2']);
						$workproftmp->set('txtyearobtained2', $_POST['txtyearobtained2']);
						$workproftmp->set('txtcertname3', $_POST['txtcertname3']);
						$workproftmp->set('txtyearobtained3', $_POST['txtyearobtained3']);
						$workproftmp->set('radIsCurrEmployed', isset($_POST['radIsCurrEmployed'])? $_POST['radIsCurrEmployed'] : '');
						$workproftmp->set('radIsTeacher', isset($_POST['radIsTeacher'])? $_POST['radIsTeacher'] : '');
						$workproftmp->set('txtCurrJob', isset($_POST['txtCurrJob'])? $_POST['txtCurrJob'] : '');
						$workproftmp->set('prefemployment', FormHelpers::CreateOptions($pref_employ, $_POST['lstPrefEmployment']));
						$workproftmp->set('jobtypeOptions', FormHelpers::CreateOptions($job, $_POST['lstJobType']));
						$workproftmp->set('prefcountryOptions', FormHelpers::CreateCollectionOption($selcountry, 'getCountryId', 'getName', $_POST['lstPrefCountry']));
						$workproftmp->set('txtPrefCity', $_POST['txtPrefCity']);
						$workproftmp->set('txtWorkWhen', $_POST['txtWorkWhen']);
						$workproftmp->set('txaMoreDetails', $_POST['txaMoreDetails']);
						$workproftmp->set('subNavigation', $subNavigation);
						$workproftmp->set('workexpcount', $resume->getWorkExpCount());				
						$workproftmp->set('btnSubmit', 'Submit');
						
						$worklist = array();	
						
						for($i=1;$i<=$_POST['hidwecount'];$i++){
							
							$workexperience = new WorkExperience();
							$workexperience->setworkexperienceID(isset($_POST['hidwID'.$i])? $_POST['hidwID'.$i] : '');
							$workexperience->setJobPosition($_POST['txtWE1Position'.$i]);
							$workexperience->setEmployer($_POST['txtWE1Employer'.$i]);
							$workexperience->setJobDescription($_POST['txtWE1JobDesc'.$i]);
							$workexperience->setLenServ($_POST['txtWE1LenServ'.$i]);
							$worklist[] = $workexperience;	
								
						}
						$workproftmp->set('worklist', $worklist);
						
						
						/*$workexpetc = new Template(); // template instantiation for included work experience(s) and resourcefile template
						$workexpetc->set('act', "workform");
						$workexpetc->set('workexpcount', $resume->getWorkExpCount());
						$workexpetc->set('qworkexp', $resume->getWorkExpList());
						$workexpetc->set('resourcefilecount', $resume->getResumeResourceFile());
						$workexpetc->set('travID', $travelerID);
						*/
						
						$workproftmp->set('newsletterSubscribe', $newslettersubstmp->fetch('travellog/views/tpl.IncFrmNewsletterSubscribe.php'));
						$workproftmp->set('teacherProfile', $teachproftmp->fetch('travellog/views/tpl.IncFrmTeacherProfile.php'));
						//$workproftmp->set('worketc', $workexpetc->fetch('travellog/views/tpl.IncFrmWorkExpEtc.php'));
						
						$workproftmp->set('errors', $errors);
						
						// function call for resume photo
						if($resume->getPrimaryPhoto() != null){
							$tmp = $resume->getPrimaryPhoto()->getPhotoLink();
						}else{
							$tmp = '';	
									
						}	
						
						$tmpphoto = "$tmp";
						$workproftmp->set('tmpphoto', $tmpphoto);
						$workproftmp->set('travelerID', $this->obj_session->get('travelerID'));
						$workproftmp->set('resourcefilecount', $resume->getResumeResourceFile());
						$workproftmp->set('photocount', $resume->getPhotos());
					
					
						$workproftmp->out('travellog/views/tpl.FrmResumeName.php');
						
					}
					
					else
					{
						// proceed if no error was encountered
						
						$travelerprofile = new TravelerProfile($this->obj_session->get('travelerID'));
						if(isset($_POST['txtFirstName']))
							$travelerprofile->setFirstName($_POST['txtFirstName']);
						else
							$travelerprofile->setFirstName('');
						if(isset($_POST['txtLastName']))
							$travelerprofile->setLastName($_POST['txtLastName']);
						else
							$travelerprofile->setLastName('');
						if(isset($_POST['lstYear']) != 0 && isset($_POST['monthOptions']) != 0 && isset($_POST['lstDay']) != 0)
							$travelerprofile->setBirthDay($_POST['lstYear']."-".$_POST['monthOptions']."-".$_POST['lstDay']);
						else
							$travelerprofile->setBirthDay('0');
						if(isset($_POST['lstGender']) != 0)
							$travelerprofile->setGender($_POST['lstGender']);
						else
							$travelerprofile->setGender('0');
						if(isset($_POST['txtPhone']))	
							$travelerprofile->setPhone($_POST['txtPhone']);
						else
							$travelerprofile->setPhone('');
						if(isset($_POST['txaAddress1']))	
							$travelerprofile->setAddress1($_POST['txaAddress1']);
						else
							$travelerprofile->setAddress1('');
						if(isset($_POST['txaAddress2']))	
							$travelerprofile->setAddress2($_POST['txaAddress2']);
						else
							$travelerprofile->setAddress2('');
						$travelerprofile->Update();	
					
						
						$resume = new Resume($this->obj_session->get('travelerID'));
						
						if(isset($_POST['lstSelAcadDegree']) != 0)
							$resume->setacaddegreeID($_POST['lstSelAcadDegree']);
						else
							$resume->setacaddegreeID('0');
						if(isset($_POST['lstMajor']) != 0)
							$resume->setmajorID($_POST['lstMajor']);
						else
							$resume->setmajorID('0');
						if(isset($_POST['txtUnivName']))
							$resume->setunivName($_POST['txtUnivName']);
						else
							$resume->setunivName('');
						if(isset($_POST['lstUnivCountry']))
							$resume->setunivCountryID($_POST['lstUnivCountry']);
						else
							$resume->setunivCountryID('');
						if(isset($_POST['lstSelAcadDegree2']) != 0)
							$resume->setacaddegreeID2($_POST['lstSelAcadDegree2']);
						else
							$resume->setacaddegreeID2('0');
						if(isset($_POST['lstMajor2']) != 0)
							$resume->setmajorID2($_POST['lstMajor2']);
						else
							$resume->setmajorID2('0');
						if(isset($_POST['txtUnivName2']))
							$resume->setunivName2($_POST['txtUnivName2']);
						else
							$resume->setunivName2('');
						if(isset($_POST['lstUnivCountry2']))
							$resume->setunivCountryID2($_POST['lstUnivCountry2']);
						else
							$resume->setunivCountryID2('');
						if(isset($_POST['txtcertname']))
							$resume->setcertName($_POST['txtcertname']);
						else
							$resume->setcertName('');
						if(isset($_POST['txtyearobtained']))
							$resume->setyearObtained($_POST['txtyearobtained']);
						else
							$resume->setyearObtained('');
						if(isset($_POST['txtcertname2']))
							$resume->setcertName2($_POST['txtcertname2']);
						else
							$resume->setcertName2('');
						if(isset($_POST['txtyearobtained2']))
							$resume->setyearObtained2($_POST['txtyearobtained2']);
						else
							$resume->setyearObtained2('');
						if(isset($_POST['txtcertname3']))
							$resume->setcertName3($_POST['txtcertname3']);
						else
							$resume->setcertName3('');
						if(isset($_POST['txtyearobtained3']))
							$resume->setyearObtained3($_POST['txtyearobtained3']);
						else
							$resume->setyearObtained3('');
						if(isset($_POST['radIsCurrEmployed']))
							$resume->setisCurrEmployed($_POST['radIsCurrEmployed']);
						else
							$resume->setisCurrEmployed('');
						if(isset($_POST['radIsTeacher']))
							$resume->setisTeacher($_POST['radIsTeacher']);
						else
							$resume->setisTeacher('');
						if(isset($_POST['txtCurrJob']))
							$resume->setcurrJob($_POST['txtCurrJob']);
						else
							$resume->setcurrJob('');
						if(isset($_POST['lstPrefEmployment']) != 0)
							$resume->setprefEmployment($_POST['lstPrefEmployment']);
						else
							$resume->setprefEmployment('0');
						if(isset($_POST['txaMoreDetails']))
							$resume->setmoreDetails($_POST['txaMoreDetails']);
						else
							$resume->setmoreDetails('');
						if(isset($_POST['lstJobType']) != 0)
							$resume->setjobTypeID($_POST['lstJobType']);
						else
							$resume->setjobTypeID('0');
						if(isset($_POST['lstPrefCountry']) != 0)
							$resume->setprefCountryID($_POST['lstPrefCountry']);
						else
							$resume->setprefCountryID('0');
						if(isset($_POST['txtPrefCity']))
							$resume->setprefCity($_POST['txtPrefCity']);
						else
							$resume->setprefCity('');
						if(isset($_POST['txtWorkWhen']))
							$resume->setworkWhen($_POST['txtWorkWhen']);
						else
							$resume->setworkWhen('');
						if(isset($_POST['lstEnglishProf']) != 0)
							$resume->setenglishproficiencyID($_POST['lstEnglishProf']);
						else
							$resume->setenglishproficiencyID('0');
						if(isset($_POST['lstPrefTeachingLevel']) != 0)
							$resume->setpreferredteachinglevelID($_POST['lstPrefTeachingLevel']);
						else
							$resume->setpreferredteachinglevelID('0');
						if(isset($_POST['lstCurrTeachJob']) != 0)
							$resume->setteachingpositionID($_POST['lstCurrTeachJob']);
						else
							$resume->setteachingpositionID('0');
						if(isset($_POST['lstYearsTeachExperience']) != 0)
							$resume->setteachingyearsID($_POST['lstYearsTeachExperience']);
						else
							$resume->setteachingyearsID('0');
						
						$resume->setdateedited($dateedited);
						if(isset($_POST['chkShowResume']))
							$resume->setshowresume($_POST['chkShowResume']);
						else
							$resume->setshowresume('');
					
						if ( isset($_POST['txtOtherCurrTeachJob']) )
							$resume->setotherteachingposition($_POST['txtOtherCurrTeachJob']);
						else	
							$resume->setotherteachingposition('');
						
						if(isset($_POST['txtOtherPrefTeachingLevel']))
							$resume->setotherteachinglevel($_POST['txtOtherPrefTeachingLevel']);
						else
							$resume->setotherteachinglevel('');
							
						$resume->Update();
						
						for($i=1;$i<=$_POST['hidwecount'];$i++){
							
							$workexperience = new WorkExperience($this->obj_session->get('travelerID'), $_POST['hidwID'.$i]);
							
							$workexperience->setJobPosition($_POST['txtWE1Position'.$i]);
							$workexperience->setEmployer($_POST['txtWE1Employer'.$i]);	
							$workexperience->setJobDescription($_POST['txtWE1JobDesc'.$i]);	
							$workexperience->setLenServ($_POST['txtWE1LenServ'.$i]);	
							
							if($_POST['txtWE1Position'.$i] == '' && $_POST['txtWE1Employer'.$i] == '' && $_POST['txtWE1JobDesc'.$i] == '' && $_POST['txtWE1LenServ'.$i] == ''){	
								if($_POST['hidwID'.$i] > 0){								
									$workexperience->DeleteWork();
								}
							}
							else{
								if($_POST['hidwID'.$i] > 0){	
									$workexperience->UpdateWork();		
								}	
								else{
									$workexperience->CreateWork();
								}	
							}
						}
						
						
						
						ob_end_clean();
						
						header("location:resume.php?action=view");
						
					
					}
					
					
					break;
				
				
				case 'editworkform':
				
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');
					
					$workexperience = new WorkExperience($this->obj_session->get('travelerID'), $workexperienceID); // for work experience
					
					$editworkformtmp =  new Template(); // template instantiation for the main template
					$editworkformtmp->set('act', "updatework");
					$editworkformtmp->set('errors', array());
					$editworkformtmp->set('errorcode', '0');
					$editworkformtmp->set('workcount', "1");
					$editworkformtmp->set('workexperienceID', $_GET['weID']);
					$editworkformtmp->set('txtWE1Position1', $workexperience->getJobPosition());
					$editworkformtmp->set('txtWE1Employer1', $workexperience->getEmployer());
					$editworkformtmp->set('txtWE1JobDesc1', $workexperience->getJobDescription());
					$editworkformtmp->set('txtWE1LenServ1', $workexperience->getLenServ());
					$editworkformtmp->set('subNavigation', $subNavigation);
					$editworkformtmp->set('labelwork', "Edit Work Experience");
					$editworkformtmp->set('editparam', "&amp;weID=".$_GET['weID']);
					
					
						
					$editworkformtmp->out('travellog/views/tpl.FrmAddWorkExp.php');
					
					break;
				
				
				case 'updatework':
				
					$subNavigation = new SubNavigation(); // for subnavigation
					$subNavigation->setContext("TRAVELER");
					$subNavigation->setContextID($this->obj_session->get('travelerID'));
					$subNavigation->setLinkToHighlight('RESUME');
					
					$errors = array(); // used to hold error messages
					
					if (!strlen(trim($_POST['txtWE1Position1']))) $errors[] = "Position is a required field!";
					if (!strlen(trim($_POST['txtWE1Employer1']))) $errors[] = "Employer is a required field!";
					if (!strlen(trim($_POST['txtWE1JobDesc1']))) $errors[] = "Job Description is a required field!";
					if (!strlen(trim($_POST['txtWE1LenServ1']))) $errors[] = "Length of Service is a required field!";
					
					
					
					if (count($errors))
					{
						// returns the previous page if an error was encountered
						
						$updateworkexp = new Template(); // tempalte instantiation for the main template
						$updateworkexp->set('errorcode', '1');
						$updateworkexp->set('act', "updatework");
						$updateworkexp->set('labelwork', "Edit Work Experience");
						$updateworkexp->set('workcount', "1");
						$updateworkexp->set('editparam', "&amp;weID=".$_GET['weID']);
						$updateworkexp->set('txtWE1Position1', isset($_POST['txtWE1Position1'])? $_POST['txtWE1Position1'] : '');
						$updateworkexp->set('txtWE1Employer1', isset($_POST['txtWE1Employer1'])? $_POST['txtWE1Employer1'] : '');
						$updateworkexp->set('txtWE1JobDesc1', isset($_POST['txtWE1JobDesc1'])? $_POST['txtWE1JobDesc1'] : '');
						$updateworkexp->set('txtWE1LenServ1', isset($_POST['txtWE1LenServ1'])? $_POST['txtWE1LenServ1'] : '');
						
						$updateworkexp->set('subNavigation', $subNavigation);
						$updateworkexp->set('errors', $errors);
						
						$updateworkexp->out('travellog/views/tpl.FrmAddWorkExp.php');
					
					
					}
					
					
					else
					{
						// proceed if no error was encountered
						
						$workexperience = new WorkExperience($this->obj_session->get('travelerID'), $workexperienceID); // for work experience
						if(isset($_POST['txtWE1Position1']))
							$workexperience->setJobPosition($_POST['txtWE1Position1']);
						else
							$workexperience->setJobPosition('');
						if(isset($_POST['txtWE1Employer1']))
							$workexperience->setEmployer($_POST['txtWE1Employer1']);
						else
							$workexperience->setEmployer('');
						if(isset($_POST['txtWE1JobDesc1']))
							$workexperience->setJobDescription($_POST['txtWE1JobDesc1']);
						else
							$workexperience->setJobDescription('');
						if(isset($_POST['txtWE1LenServ1']))
							$workexperience->setLenServ($_POST['txtWE1LenServ1']);
						else
							$workexperience->setLenServ('');
							
						$workexperience->UpdateWork();
						
						
				
						ob_end_clean();
						
						header("location:resume.php?action=edit");
					
					}
			
					break;
					
					
				case 'delete';
				
					if( $this->obj_session->get('travelerID') == 0 ) header("location: /index.php");	
					
					
					$workexperience = new WorkExperience($this->obj_session->get('travelerID'), $_POST['workexperienceID']); // for work experience
					$workexperience->DeleteWork();
					//ob_end_clean();
					
					//header("location:resume.php?action=view");
					
			
					break;				
				
		}
		}
		
		catch(exception $e)
		{
			ob_end_clean();
			
			
			if($_GET["action"] == 'view' && $this->obj_session->get('travelerID') > 0 && 'Invalid Traveler ID' == $e->getMessage()){
				header("location: /resume.php?action=add");
				exit;
			}
			elseif ($_GET["action"] == 'view' && $this->obj_session->get('travelerID') > 0 && 'Invalid Traveler ID' != $e->getMessage()){
				header("location: /resume.php?action=view");
				exit;
			}
			elseif ( $this->obj_session->get('travelerID') > 0 ) {
				header("location: /passport.php");
				exit;	
			}
			else {
				header("location: /index.php");
				exit;	
			}
	
		}
	}
	
}
 
?>
