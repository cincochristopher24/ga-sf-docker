<?php
/**
 * <b>Friend</b> controller.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
require_once ('Class.Template.php');
require_once ('Class.Paging.php');
require_once ('Class.ObjectIterator.php');
require_once ("travellog/model/Class.PrivacyPreference.php");
require_once ("travellog/model/navigator/Class.BackLink.php");
require_once ('Class.Date.php');
require_once("travellog/model/Class.SubNavigation.php");

class FriendController {
	
	private static 
	
	$travelerID,
	
	$IsLogin  = NULL,
	
	$friendID = NULL;

	public static function View() 
	{
		$main_tpl      = new Template();
		$inc_tpl       = clone $main_tpl;
		
		$subNavigation = new SubNavigation();
		$subNavigation->setContext('TRAVELER');
		$subNavigation->setContextID(self::$travelerID);
		$subNavigation->setLinkToHighlight('MY_FRIENDS');
		
		$isOwner       = false;
		$traveler      = new Traveler( self::$travelerID );
		$count_friends = count($traveler->getFriends());
		
		if (isset($_SESSION['travelerID'])){
			if (self::$travelerID == $_SESSION['travelerID'] ):
				$isOwner = true;
			endif;
		}
	
		$querystring   = 'tID='.self::$travelerID.'&fID='.self::$friendID.'&page=1';

		if (self::$IsLogin && $isOwner){
			$inc_tpl->set( 'friends_requests' , $traveler->getFriendRequests()   );
			$inc_tpl->set( 'pending_requests' , $traveler->getPendingRequests()  );
			$inc_tpl->set( 'blocked_users'    , $traveler->getBlockedUsers()     );
		}
		$inc_tpl->set( 'querystring'      , $querystring             );
		$inc_tpl->set( 'IsLogin'          , self::$IsLogin           );
		$inc_tpl->set( "isOwner"          , $isOwner                 );
		$inc_tpl->set( "count_friends"    , $count_friends           );
		
		$inc_tpl->set( "username"         , $traveler->getUserName() );

		$code = <<<BOF
<script type="text/javascript">
//<![CDATA[
	var getFriendList = function(param)
	{
		//new ajax( 'ajaxpages/friends-list.php', {postBody: param, update: $('friends') });
		var obj_ajax = new ajax();
		obj_ajax.init();
		obj_ajax.postBody = param;
		obj_ajax.request('ajaxpages/friends-list.php');
		obj_ajax.update   = $('friends');
	}
//]]>
</script>		
BOF;
		
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		//Template::includeDependentJs("/js/prototype1.lite.js");
		Template::includeDependentJs("/min/f=js/prototype.js", array('top' => true));
		Template::includeDependentJs("/js/moo2.ajax.js");
		Template::includeDependentJs("/js/moo.fx.js");
		Template::includeDependentJs("/js/moo.fx.pack.js");
		Template::setMainTemplateVar('page_location', 'My Passport');
		Template::includeDependent($code);
		
		$main_tpl->set( 'title' , 'Friends Lists'                                   );
		$main_tpl->set( 'contents'   , $inc_tpl->fetch('travellog/views/tpl.Friend.php') );
		$main_tpl->set("subNavigation",$subNavigation);
		$main_tpl->set("isOwner",$isOwner);
		$main_tpl->out( 'travellog/views/tpl.MainTemplate.php');
	}
	
	public static function getFriendsList( $page )
	{
		$main_tpl = new Template();
		$canView  = true;
		 
		if ( self::$friendID != NULL ):
			$traveler       = new Traveler( self::$friendID );
			$allow_controls = false;
		else:
			$traveler       = new Traveler( self::$travelerID );
			$allow_controls = true;
		endif;
		
		$isOwner            = false;
		
		if (self::$IsLogin && self::$travelerID == $_SESSION['travelerID']):
			$obj_pref       = new PrivacyPreference( self::$friendID );
			$canView        = $obj_pref->canViewOnlineStatus(self::$travelerID);
			$isOwner        = true;
		endif;
		
		$friends_lists = $traveler->getFriends();
		$arr_lists     = array();
		$trav_name     = $traveler->getUserName(); 
		// recreate array of objects without index
		foreach($friends_lists as $friends_list):
			$arr_lists[] = $friends_list;
		endforeach;
		
		$paging        = new Paging( count($arr_lists),$page,'tID='.self::$travelerID.'&fID='.self::$friendID,12 );
		$iterator      = new ObjectIterator( $arr_lists,$paging    );
		$paging->setOnClick('getFriendList');
		$main_tpl->set( 'paging'         , $paging         );
		$main_tpl->set( 'iterator'       , $iterator       );
		$main_tpl->set( 'IsLogin'        , self::$IsLogin  );
		$main_tpl->set( 'canView'        , $canView        );
		$main_tpl->set( 'trav_name'      , $trav_name      );
		$main_tpl->set( 'isOwner'        , $isOwner        );
		$main_tpl->set( 'allow_controls' , $isOwner        );
		$main_tpl->out('travellog/views/tpl.ViewFriendsList.php');	
	}
	
	public static function Delete() 
	{
		$traveler = new Traveler( self::$travelerID );
		$traveler->removeFriend ( self::$friendID   );
	}
	
	public static function Block() 
	{
		$traveler = new Traveler( self::$travelerID );
		$traveler->blockUser    ( self::$friendID   );
	}
	
	public static function Unblock() 
	{
		$traveler = new Traveler( self::$travelerID );
		$traveler->unblockUser  ( self::$friendID   );
	}
	
	public static function Accept() 
	{
		$traveler = new Traveler( self::$travelerID );
		$traveler->addFriend    ( self::$friendID   );
	}
	
	public static function Reject() 
	{
		$traveler = new Traveler      ( self::$travelerID );
		$traveler->rejectFriendRequest( self::$friendID   );
	}
	
	public static function Cancel() 
	{
		$traveler = new Traveler      ( self::$travelerID );
		$traveler->cancelFriendRequest( self::$friendID   );
	}
	
	/*public static function Request( $formvars ) 
	{
		$traveler      = new Traveler( self::$travelerID );
		$main_tpl      = new Template();
		$inc_tpl       = clone $main_tpl;
		$invited_users = array();	
		$friends       = array();
		$notmembers    = array();
		
		foreach( $formvars as $username ):
			$travelerID = $traveler->getTravelerIDByUsername( $username );
			if ( $travelerID > 0 )
			{ 
				if ( !$traveler->isFriend($travelerID) && !$traveler->isBlocked($travelerID) )
				{
					$traveler->requestFriendship ( $travelerID );
					$invited_users[] = $username;
				}
				else
				{
					$friends[]  = $username;			
				}
			}
			else $notmembers[]   = $username;   
		endforeach;
		
		$inc_tpl->set('invited_users', $invited_users  );
		$inc_tpl->set('notmembers'   , $notmembers     );
		$inc_tpl->set('friends'      , $friends        );
		
		$main_tpl->set('main_title' , 'Friends Confirmation' );
		$main_tpl->set('contents'   , $inc_tpl->fetch('travellog/views/tpl.ViewSuccessInvite.php') );
		$main_tpl->out('travellog/views/tpl.MainTemplate.php');
	}*/
	
	public static function Request( $friendID ) 
	{
		$obj_traveler  = new Traveler( self::$travelerID );
		$obj_friend    = new Traveler( $friendID );
		$main_tpl      = new Template();
		$inc_tpl       = clone $main_tpl;
		
		$subNavigation = new SubNavigation();
		$subNavigation->setContext('TRAVELER');
		$subNavigation->setContextID(self::$travelerID);
		$subNavigation->setLinkToHighlight('MY_FRIENDS');
		$isFriend = $obj_traveler->isFriend($friendID);
		if ( !$isFriend && !$obj_traveler->isBlocked($friendID) ){
			require_once("class.phpmailer.php");
			$obj_traveler->requestFriendship ( $friendID );
			
			$msg = "Hi ".$obj_friend->getTravelerProfile()->getFirstname()."\n\n" .
				   $obj_traveler->getUserName() ." has requested you as a friend on GoAbroad Network. To \n".  
				   "confirm or deny this request, please click on the link below: \n\n" .
				   "http://www.goabroad.net/login.php?redirect=friend.php \n\n".
				   "Happy travels! \n\n" .
				   "GoAbroad Network \n" .
				   "http://www.GoAbroad.net";

			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->IsHTML(false);
			//$mail->From     = $obj_traveler->getTravelerProfile()->getEmail();
			//$mail->FromName = $obj_traveler->getTravelerProfile()->getFirstname();
			$mail->From     = 'admin@goabroad.net';
			$mail->FromName = 'GoAbroad Network';
			$mail->Subject  = 'You have a new friend request on GoAbroad Network!';
			$mail->Body     = $msg;
			$mail->AddAddress($obj_friend->getTravelerProfile()->getEmail());
			$mail->Send();
		}
		$isOwner = false;
		
		if (isset($_SESSION['travelerID'])){
			if (self::$travelerID == $_SESSION['travelerID'] ):
				$isOwner = true;
			endif;
		}
		
		$inc_tpl->set('friend' , $obj_friend->getUserName() );
		$inc_tpl->set('isFriend' , $isFriend );
		$inc_tpl->set('travID' , $obj_friend->getTravelerID() );
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		Template::setMainTemplateVar('page_location', 'My Passport');
		$main_tpl->set('title' , 'Friends Confirmation' );
		$main_tpl->set('contents'   , $inc_tpl->fetch('travellog/views/tpl.ViewSuccessInvite.php') );
		$main_tpl->set("subNavigation",$subNavigation);
		$main_tpl->set("isOwner",$isOwner);
		$main_tpl->out('travellog/views/tpl.MainTemplate.php');
	}
	
	public static function Invite() 
	{
		$main_tpl     = new Template();
		$inc_tpl      = clone $main_tpl;
		$obj_backlink = BackLink::instance();
		$obj_backlink->Create(); 
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		Template::setMainTemplateVar('page_location', 'My Passport');
		$inc_tpl->set ( 'obj_backlink' , $obj_backlink    );
		$main_tpl->set( 'title'   , 'Invite Friends' );
		$main_tpl->set( 'contents'     , $inc_tpl->fetch('travellog/views/tpl.FrmInvite.php') );
		$main_tpl->out( 'travellog/views/tpl.MainTemplate.php');
	}
	
	public static function setTravelerID( $travelerID )
	{
		self::$travelerID = $travelerID;
	}
	
	public static function setFriendID( $friendID )
	{
		self::$friendID = $friendID;
	}
	
	public static function setIsLogin( $IsLogin )
	{
		self::$IsLogin = $IsLogin;
	}
}
?>
