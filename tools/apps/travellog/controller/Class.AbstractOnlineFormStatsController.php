<?php
	require_once('travellog/factory/Class.FileFactory.php');
	require_once("travellog/model/Class.SessionManager.php");
	/***
	 * RULES: 
	 * - The user must be logged and only advisors can visit this page.
	 * - A surveyformID must be present in the url.
	 * - That surveyFormID must be owned by the advisor.
	 */
	abstract class AbstractOnlineFormStatsController
	{
		protected $obj_session 		= null;
		protected $file_factory 	= null;
		protected $loggedTraveler 	= null;
		protected $group			= null;
		protected $mOnlineForm 		= null;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('TravelerProfile');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('GaOnlineForm', array('path'=>'travellog/model/gaOnlineForms/'));
			$this->file_factory->registerClass('Template', array('path'=>''));
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->invokeStaticClass('Template','setMainTemplateVar',array('layoutID','survey'));
		}
		
		public function performAction(){
			$travelerID = $this->obj_session->get('travelerID');
			if(!isset($travelerID) || 0==$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/login.php'));
			}
			//check if the user is an advisor
			$this->loggedTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			if(!$this->loggedTraveler->isAdvisor()){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			//check if the onlineformID was given in the url
			if(!isset($_GET['frmID']) || !is_numeric($_GET['frmID'])){		
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			$frmID = $_GET['frmID'];
			
			//check if the onlineFormID is valid
			$this->mOnlineForm = $this->file_factory->invokeStaticClass('GaOnlineForm','getFormByOnlineFormID',array($frmID));
			if(is_null($this->mOnlineForm)) $this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			
			$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			$oGroup = $oGroupFactory->create($this->file_factory->invokeStaticClass('AdminGroup','getAdvisorGroupID',array($this->loggedTraveler->getTravelerID())));
			$this->group = $oGroup;
			
			//make sure that survey form is owned by the administered group 
			if($this->mOnlineForm->getGroupID() != $oGroup->getGroupID()){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			$view = (array_key_exists('view',$_GET) && 'entries'==$_GET['view'])?'entries':'stats';
			
			$vars = array(
				'onlineForm' 	=> $this->mOnlineForm,
				'group'			=> $oGroup,
				'formData'		=> $this->mOnlineForm->getFormData()
			);
			
			if('stats' == $view){
				$vars['statsData'] 	= $this->mOnlineForm->getStatistics();
				$this->showOverviewTemplate($vars);
			}
			else{
				//get all formvalues
				$vars['participations'] = $this->mOnlineForm->getAllParticipations();
				$this->showEntryTemplate($vars);
			}
		}
		
		protected function createTemplate(){
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/onlineForms/');
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');	
			$subNavigation->setContextID($this->group->getGroupID());
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight('ONLINE_FORMS');
			$tpl->set('subNavigation',$subNavigation);			
			return $tpl;
		}
		
		protected function showOverviewTemplate($vars=array()){
			$tpl = $this->createTemplate();
			$tpl->setVars($vars);				
			$tpl->out('tpl.Stats.php');
		}
		
		protected function showEntryTemplate($vars=array()){
			$tpl = $this->createTemplate();
			$tpl->setVars($vars);
			$tpl->out('tpl.Participation.php');
		}
		
	}
 ?>