<?php

require_once("Class.CookieManager.php");
require_once("travellog/model/Class.SessionManager.php");
require_once("travellog/controller/Class.IController.php");

abstract class AbstractSPLauncherController implements IController{
		
	protected $session_manager;
	protected $cookie_manager;
	
	protected $action;
	protected $sp_context;
	
	abstract protected function getDestinationServer();
	
	public function __construct(){
		$this->session_manager = SessionManager::getInstance();
		$this->cookie_manager = CookieManager::getInstance();
	}
	
	public function redirect($location){
		header("location: ".$location);
		exit;
	}
	
	public function performAction(){
		
		$this->action = isset($_GET["action"]) ? $_GET["action"] : NULL;
		$this->sp_context = isset($_GET["context"]) ? $_GET["context"] : NULL;
		
		if( !$this->isValidAction($this->action) || !$this->isValidContext($this->sp_context) ){
			$this->redirect("/");
		}
		
		switch($this->action){
			case "login"	:	$this->doLoginAction(); break;
			case "connect"	:	$this->doConnectAction(); break;
			case "link"		:	$this->doLinkAction(); break;
		}
		
		$this->authorizeSP();
	}
	
	protected function doLoginAction(){
		if( $this->session_manager->getLogin() ){
			$this->redirect("/travelerdashboard.php");
		}
	}
	
	protected function doConnectAction(){
		if( !$this->session_manager->getLogin() ){
			$this->redirect("/login.php?redirect=/splauncher.php?action=".$this->action."&context=".$this->sp_context);
		}
	}
	
	protected function doLinkAction(){
		if( !$this->session_manager->getLogin() ){
			$this->redirect("/login.php?redirect=/splauncher.php?action=".$this->action."&context=".$this->sp_context);
		}
		
		$this->session_manager->set("LAST_OAUTH_ACTION","LINK");
	}

	protected function isValidAction($action=NULL){
		return in_array($action,array("login","connect","link"));
	}
	
	protected function isValidContext($context=NULL){
		return in_array($context,array("twitter","google","facebook"));
	}
	
	protected function prepareCookieValues($cookieNamePrefix){
		//general cookie settings
		$path = "/";
		$expire = 0; 
		$domain = FALSE === strpos($_SERVER["SERVER_NAME"],".local") ? ".goabroad.net" : ".goabroad.net.local";
		
		//set action cookie
		$value = $this->action;
		$name = $cookieNamePrefix."_action";
		$this->cookie_manager->set($name,$value,$expire,$path,$domain);
		
		//set destination server cookie
		$name = $cookieNamePrefix."_server";
		$value = $this->getDestinationServer();
		$this->cookie_manager->set($name,$value,$expire,$path,$domain);
	}
	
	protected function authorizeSP(){
		$oauth_api = OAuthAPIFactory::getInstance()->create($this->sp_context);
		$oauth_api->clearCredentials();
		if($this->sp_context == 'twitter'){
			$authorizeUrl = $oauth_api->getAuthenticationUrl()."&force_login=1";
		}
		else {
			$authorizeUrl = $oauth_api->getAuthenticationUrl();
		}
		
		$cookieNamePrefix = $this->sp_context;   				

		$this->prepareCookieValues($cookieNamePrefix);
		$this->redirect($authorizeUrl);
	}
}