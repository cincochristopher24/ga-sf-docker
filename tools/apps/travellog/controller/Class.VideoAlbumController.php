<?php
/*
 * Created on 02 3, 09
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

	require_once('travellog/model/Class.VideoAlbum.php');
	require_once('travellog/model/Class.Traveler.php');
	require_once('travellog/model/Class.GroupFactory.php');
	
	require_once(realpath(dirname(__FILE__).'/../../../').'/lib/services/video_album/models/GanetVideoAlbumPeer.php');
	
	class VideoAlbumController {
		
		protected 
			$album 		= null,
			$params 	= null;
		
		function __construct () {
			$this->params['albumID']				= (isset($_REQUEST['albumID'])) ? $_REQUEST['albumID'] : 0;
			$this->album	 		  				= new VideoAlbum($this->params['albumID']);
			
			if (isset($_REQUEST['txtAlbumTitle']))
				$this->params['albumTitle']			= trim(htmlentities(strip_tags($_REQUEST['txtAlbumTitle']), ENT_NOQUOTES));
			else if (isset($_REQUEST['txtTitle'])) 
				$this->params['albumTitle']			= trim(htmlentities(strip_tags($_REQUEST['txtTitle']), ENT_NOQUOTES));
			else
				$this->params['albumTitle'] 		= trim(htmlentities(strip_tags($this->album->getTitle()), ENT_NOQUOTES)); 	
			
			if (get_magic_quotes_gpc())
				$this->params['albumTitle']			= trim(stripslashes($this->params['albumTitle']));
			
			$this->params['travelerID']				= (isset($_REQUEST['travelerID'])) ? $_REQUEST['travelerID'] : 0;
			$this->params['groupID']				= (isset($_REQUEST['gID'])) ? $_REQUEST['gID'] : 0;
			$this->params['creatorID']				= ($this->params['travelerID']) ? $this->params['travelerID'] : $this->params['groupID'];
			$this->params['creatorType']			= ($this->params['travelerID']) ? 'traveler' : 'group';
			$this->params['action']					= $_REQUEST['action'];
		}
		
		/** 
		 * performs the required action
		 */
		
		function performAction() {
			switch ($this->params['action']) {
				case 'saveAlbum':
					$this->_saveAlbum();
					break;
					
				case 'deleteAlbum':
					$this->_deleteAlbum();
					break;
			}
		}
		
		/**
		 * method to create or update a video album
		 */
		 
		function _saveAlbum () {
			$location = '/video.php?';
			$owner    = null;      
			   
			if ('traveler' === $this->params['creatorType']) {
				$owner 	  = new Traveler($this->params['creatorID']);
				$location .= 'action=getTravelerVideos&travelerID='.$this->params['creatorID'];	
			}
			else if ('group' === $this->params['creatorType']) {
				$groupArray = GroupFactory::instance()->create(array($this->params['creatorID']));
				if (count($groupArray) > 0)
					$owner = $groupArray[0];
				$location .= 'action=getGroupVideos&gID='.$this->params['creatorID'];	
			}
			
			if (!is_null($owner)) {
				if (!GanetVideoAlbumPeer::checkIfAlreadyAnAlbumOfOwner($this->params['albumTitle'], $owner) || ($this->album->getTitle() == $this->params['albumTitle'])) {
					$this->album->setTitle($this->params['albumTitle']);
					$this->album->setCreatorID($this->params['creatorID']);
					$this->album->setCreatorType($this->params['creatorType']);
					
					if (!$this->params['albumID']) { 
						$this->album->setDateCreated(date('Y-m-d H:i:s'));
						$this->album->setLastDateUpdated(date('Y-m-d H:i:s'));
						$this->album->create();
						$location .= '&hasCreatedAnAlbum=1';
					}
					
					else {
						$this->album->setLastDateUpdated(date('Y-m-d H:i:s'));
						$this->album->update();
						$location .= '&hasEditedAnAlbum=1';
					}
					
					$location .= '&albumID='.$this->album->getAlbumID().'&type=album';
				}
				else
					$location .= '&hasFailedSavingAnAlbum=1';	
			}
			else
				$location = '/index.php';
				
			header("location: $location");
		}
		
		/**
		 * method to delete a video album including its videos
		 */
		 
		function _deleteAlbum () {
			$this->album->delete();
			$location = '/video.php?';
			
			if ('traveler' === $this->params['creatorType'])
				$location .= 'action=getTravelerVideos&travelerID='.$this->params['creatorID'];
			else if ('group' === $this->params['creatorType'])
				$location .= 'action=getGroupVideos&gID='.$this->params['creatorID'];
				
			$location .= '&hasDeletedAnAlbum=1';
				
			header("location: $location");
		}
		
		/**
		 * method that checks browser used
		 */
		 
		function whatBrowser () {
			$myBrowser = get_browser(null, true);
			return $myBrowser['browser'];
		}
		
		/**
		 * Activates the __call magic function.
		 * 
		 * @return void
		 */
		public function __call($method, $arguments) {
			$actions = include(realpath(dirname(__FILE__).'/../../../').'/lib/services/video_album/actions/action.info.php');
			
			if (isset($actions[$method])) {
				$action = new $actions[$method]['class']($arguments[0]);
				
				if ($action->validate()) {
					$action->execute();
				}
				else {
					$action->onValidationFailure();
				}
			}
			else {
				include_once('FileNotFound.php');
				exit;
			}
		}
	}