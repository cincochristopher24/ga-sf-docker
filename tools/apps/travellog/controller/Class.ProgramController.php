<?php
require_once("travellog/model/Class.Activity.php");
require_once("travellog/model/Class.GroupFactory.php");
require_once("travellog/model/Class.Traveler.php");
require_once("Class.Paging.php");
require_once("Class.ObjectIterator.php");
require_once('Class.Template.php');
require_once('Class.Date.php');
require_once('travellog/model/Class.Condition.php');
require_once('travellog/model/Class.FilterCriteria2.php');
require_once('travellog/model/navigator/Class.BackLink.php');
require_once("travellog/model/Class.SubNavigation.php");
require_once('Class.FormHelpers.php');
class ProgramController{
	
	function add( $formvars ){
		$obj_template      = new Template();
		$obj_admin_group   = new AdminGroup( $formvars['groupID'] );
		$obj_template->set('hidGroupID'              , $formvars['groupID']                                                  );
		$obj_template->set('hidProgramID'            , ''                                                                    );
		$obj_template->set('txtTitle'                , ''                                                                    );
		$obj_template->set('txtStartDate'            , FormHelpers::CreateCalendar(NULL,'startday','startmonth','startyear') );
		$obj_template->set('txtFinishDate'           , FormHelpers::CreateCalendar(NULL,'endday'  ,'endmonth'  ,'endyear')   );
		$obj_template->set('txtDescription'          , ''                                                                    );
		$obj_template->set('chkSubGroups'            , $obj_admin_group->getSubGroups()                                      );
		$obj_template->set('radPublic'               , 1                                                                     );
		$obj_template->set('chkValue'                , array()                                                               );
		$obj_template->set('mode'                    , 'save'                                                                );
		$obj_template->set('button'                  , 'Add'                                                                 );
		$obj_template->out('travellog/views/tpl.FrmProgramManagement.php');
	}
	
	function edit( $formvars ){
		$obj_template    = new Template();
		$obj_program     = new Program   ( $formvars['programID']     );
		$obj_admin_group = new AdminGroup( $obj_program->getGroupID() );
		$vvalues         = array();
		$obj_date        = ClassDate::instance();
		$obj_template->set('hidGroupID'              ,$obj_program->getGroupID()       );
		$obj_template->set('hidProgramID'            ,$formvars['programID']           );
		$obj_template->set('mode'                    ,'update'                         );
		$obj_template->set('chkSubGroups'            ,$obj_admin_group->getSubGroups() );
		$obj_template->set('button'                  ,'Update'                         );
		
		/*if ( count( self::$errors ) ):
			$obj_template->set('txtTitle'            ,$formvars['title']                     );
			$obj_template->set('txtStartDate'        ,FormHelpers::CreateCalendar(NULL,'startday','startmonth','startyear') );
			$obj_template->set('txtFinishDate'       ,FormHelpers::CreateCalendar(NULL,'endday'  ,'endmonth'  ,'endyear')   );
			$obj_template->set('txtDescription'      ,$formvars['description']               );
			$obj_template->set('chkValue'            ,$formvars['chkSubGroups']              );
			$obj_template->set('radPublic'           ,$formvars['public']                    );
		else:*/
			foreach( $obj_program->getSubgroups() as $obj_subgroup ):
				$vvalues[] = $obj_subgroup->getGroupID();
			endforeach;
			
			$obj_template->set('txtTitle'            ,$obj_program->getTitle()         );
			$obj_template->set('txtStartDate'        ,FormHelpers::CreateCalendar($obj_program->getStart(),'startday','startmonth','startyear') );
			$obj_template->set('txtFinishDate'       ,FormHelpers::CreateCalendar($obj_program->getFinish(),'endday'  ,'endmonth'  ,'endyear')   );
			$obj_template->set('txtDescription'      ,$obj_program->getDescription()   );
			$obj_template->set('radPublic'           ,$obj_program->getDisplayPublic() );
			$obj_template->set('chkValue'            ,$vvalues                         );
		//endif; 
		
		$obj_template->out('travellog/views/tpl.FrmProgramManagement.php');
	}
	
	function save( &$formvars ){
		$obj_program = new Program();
		$obj_date    = ClassDate::instance();
		
		$obj_group   = new AdminGroup( $formvars['groupID'] );

		$obj_program->setTitle         ( $formvars['title']                                       );
		$obj_program->setStart         ( $obj_date->CreateODBCDate($formvars['start_date'], '/' ) );
		$obj_program->setFinish        ( $obj_date->CreateODBCDate($formvars['finish_date'], '/') );
		$obj_program->setDescription   ( $formvars['description']                                 );
		$obj_program->setGroupID       ( $formvars['groupID']                                     );
		$obj_program->setDisplayPublic ( $formvars['public']                                      );
		$obj_program->Create();
		
		//self::setProgramID( $obj_program->getProgramID() );
		if( $obj_group->isParent() )
			$selected_sub_groups = explode(',',$formvars['chkSubGroups']);
		else
			$selected_sub_groups = array($formvars['groupID']);
			
		$formvars['programID'] = $obj_program->getProgramID();
		if ( count($selected_sub_groups) ):
			foreach ( $selected_sub_groups as $subgroupID ):
				$obj_admin_group = new AdminGroup( $subgroupID  );
				$obj_admin_group->addProgram( $obj_program );
			endforeach;
		endif;
		
	}
	
	function update( $formvars ){
		$obj_date  = ClassDate::instance();
		$obj_program = new Program     ( $formvars['programID']   );
		$obj_program->setTitle         ( $formvars['title']       );
		$obj_program->setStart         ( $obj_date->CreateODBCDate($formvars['start_date'], '/' ) );
		$obj_program->setFinish        ( $obj_date->CreateODBCDate($formvars['finish_date'], '/') );
		$obj_program->setDescription   ( $formvars['description'] );
		$obj_program->setDisplayPublic ( $formvars['public']      );
		$obj_program->Update();
		
		if ( count($obj_program->getSubgroups()) ):
			foreach( $obj_program->getSubgroups() as $obj_subgroup ):
				$obj_admin_group = new AdminGroup( $obj_subgroup->getGroupID() );
				$obj_admin_group->removeProgram  ( $formvars['programID']       );
			endforeach;
		endif;
		
		$selected_sub_groups = explode(',',$formvars['chkSubGroups']);
				
		if ( count($selected_sub_groups) ):
			foreach ( $selected_sub_groups as $subgroupID ):
				$obj_admin_group = new AdminGroup( $subgroupID  );
				$obj_admin_group->addProgram( $obj_program );
			endforeach;
		endif;
	}
	
	function delete($formvars){
		$program = new Program( $formvars['programID'] );
		$program->Delete();
	}
	
	function view( $formvars ){
		$obj_template       = new Template;
		$load               = '';
		$obj_sub_navigation = new SubNavigation; 
		$obj_sub_navigation->setContext        ( 'GROUP'                );
		$obj_sub_navigation->setContextID      (  $formvars['groupID']  );
		$obj_sub_navigation->setLinkToHighlight( 'ITINERARIES'          );
		$obj_sub_navigation->setGroupType      ( 'ADMIN_GROUP'          );
		
		if ($this->_showAdministrativeControls($formvars)){
			if( $formvars['mode']=='add' )
				$load = 'manager.displayProgramForm("mode=add&groupID='.$formvars['groupID'].'")';
			elseif( $formvars['mode']=='edit' )
				$load = 'manager.displayProgramForm("mode=edit&programID='.$formvars['programID'].'")';
		}
			
		$obj_template->set( 'title'               , 'View Itineraries'                );
		$obj_template->set( 'obj_sub_navigation'  , $obj_sub_navigation               );
		$obj_template->set( 'contents'            , $this->getContent   ( $formvars ) );
		if ( $formvars['has_program'] ){
			$obj_template->set( 'programDetails'      , $this->getDetail    ( $formvars ) );
			$obj_template->set( 'programActivity'     , $this->getActivities( $formvars ) );
		}else{
			
			if ( $formvars['isLogin'] && $this->_showAdministrativeControls($formvars))
				$load = 'manager.displayProgramForm("mode=add&groupID='.$formvars['groupID'].'")';
			else
				$obj_template->set( 'programDetails'      , '<h2>No Itineraries yet.</h2>' );
		}	
		$obj_template->set( 'load'                , $load                                  );
		$obj_template->out( 'travellog/views/tpl.ViewProgram.php' );
		
		
	}
	
	function getContent( &$formvars ){
		$factory      = GroupFactory::instance();
		$obj_group    = $factory->create( array($formvars['groupID']) );
        $cond         = new Condition();
        $cond1        = new Condition();
		$filter       = NULL;
		$order        = NULL;
		$is_member    = false;
		$obj_template = new Template();
		
		if ( $formvars['isLogin'] ){
			$obj_traveler  = new Traveler($formvars['travelerID']);
			$is_member     = $obj_group[0]->isMember( $obj_traveler );
		}
		
		$showControls  = $this->_showAdministrativeControls( $formvars );
		
		if ( !$is_member || !$formvars['isLogin'] ){
			$filter  = new FilterCriteria2();
			$order   = new FilterCriteria2();
			$cond->setAttributeName("displaypublic");
			$cond->setOperation(FilterOp::$EQUAL);
			$cond->setValue(1);
			$filter->addBooleanOp('AND');
			$filter->addCondition($cond);
			$cond1->setAttributeName("tblProgram.programID");
			$cond1->setOperation(FilterOp::$ORDER_BY_DESC);
			$order->addCondition($cond1);
		}
		
		$obj_programs            = $obj_group[0]->getPrograms('',$filter,$order);
		$formvars['programID']   = 0;
		$formvars['has_program'] = false;
		$obj_template->set( 'showControls' , $showControls );
		if ( count($obj_programs) ){
			$formvars['has_program'] = true;
			if (isset($formvars['selected_programID'])) 
				$formvars['page'] = $this->checkPosition($obj_programs,10,$formvars['selected_programID']);

			
			$obj_program_paging   = new Paging(count($obj_programs),$formvars['page'],'mode=viewContent&programID=0&groupID='.$formvars['groupID'],10);
			$obj_program_iterator = new ObjectIterator( $obj_programs,$obj_program_paging );
			$obj_program_paging->setOnclick('manager.getContent');
			$obj_program_iterator->rewind();
 			if (isset($formvars['selected_programID']))
 				$formvars['programID']  = $formvars['selected_programID'];
 			else 
				$formvars['programID']  = ($formvars['programID']==0)? $obj_program_iterator->current()->getProgramID() : $formvars['programID'];
			$obj_template->set( 'obj_program_paging'     , $obj_program_paging    );
			$obj_template->set( 'obj_program_iterator'   , $obj_program_iterator  );
		}
		$obj_template->set( 'programID'              , $formvars['programID']   );
		$obj_template->set( 'groupID'                , $formvars['groupID']   );
				
		return $obj_template->fetch('travellog/views/tpl.ViewProgramContent.php');
	}
	
	function getDetail( $formvars ){
		$obj_template   = new Template();
		$factory        = GroupFactory::instance();
		$obj_program    = new Program ( $formvars['programID'] );
		$obj_group      = $factory->create( array($formvars['groupID']) );
		$subgroup_names = NULL;
		$ctr            = 1;
		
		if ( count( $obj_program->getSubgroups() ) ){
			foreach( $obj_program->getSubgroups() as $obj_subgroup ){
				if( $ctr == 1 )
					$subgroup_names = $obj_subgroup->getName();
				else{
					if($ctr != count( $obj_program->getSubgroups() ))
						$subgroup_names .= ', '.$obj_subgroup->getName();
					else
						$subgroup_names .= ' and '.$obj_subgroup->getName();
				}
				$ctr++;
			}
		}
		if ($obj_group[0]->getPrograms()){
			$showControls   = $this->_showAdministrativeControls( $formvars );
			
			$obj_template->set( 'programID'      , $formvars['programID'] );
			$obj_template->set( 'obj_program'    , $obj_program           );
			$obj_template->set( 'subgroup_names' , $subgroup_names        );
			$obj_template->set( 'showControls'   , $showControls          );
			$obj_template->set( 'isLogin'        , $formvars['isLogin']   );
			
			return $obj_template->fetch('travellog/views/tpl.ViewProgramDetail.php');
		}
	}
	
	function getActivities( $formvars ){
		$obj_template        = new Template();
		$obj_program         = new Program ( $formvars['programID'] );
		$formvars['groupID'] = $obj_program->getGroupID();  
		$obj_activities      = $obj_program->getActivities();
		$showControls        = $this->_showAdministrativeControls( $formvars );
		
		$obj_template->set( 'programID'      , $formvars['programID'] ); 
		$obj_template->set( 'groupID'        , $formvars['groupID']   );
		$obj_template->set( 'obj_activities' , $obj_activities        );
		$obj_template->set( 'showControls'   , $showControls          );
		
		return $obj_template->fetch('travellog/views/tpl.ViewProgramActivity.php'); 
	}
	
	function _showAdministrativeControls( $formvars ){
		if ( $formvars['isLogin'] ){
			$obj_group = new AdminGroup( $formvars['groupID'] );
			if ( $obj_group->getAdministrator()->getTravelerID() == $formvars['travelerID'] ) return true;
			else return false;
		}
	}
	
	private function checkPosition( $arr_objects,$rowsperpage, $programID ){
		$page = 1;
		for($ctr=0; $ctr < count($arr_objects); $ctr++ ){
			if ( $arr_objects[$ctr]->getProgramID() == $programID ){
				$page = ceil(($ctr+1)/$rowsperpage);
				break;
			}
		}
		return $page;
	}
	
}
?>