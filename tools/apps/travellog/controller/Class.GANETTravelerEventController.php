<?php
require_once('travellog/controller/Class.AbstractTravelerEventController.php');
class GANETTravelerEventController extends AbstractTravelerEventController{
	function performAction(){
		$this->data               = array_merge($_GET, $_POST);
		$this->data['action']     = ( isset($this->data['action']) )? $this->data['action']  : 'View';
		$this->data['context']    = ( isset($this->data['context']))? $this->data['context'] : 2;
		$this->data['from_where'] = 'passport';
		switch( $this->data['context'] ){
			case "0":
				$event_controller = $this->file_factory->getClass('ActivityController'); 
				$event_controller->performAction();	 
			break;
			case "1":
				$event_controller = $this->file_factory->getClass('GroupEventController');
				$event_controller->performAction();	 
			break;
			default:
				parent::__applyRules(); 	
				parent::performAction();  
		}
	}
}
?>