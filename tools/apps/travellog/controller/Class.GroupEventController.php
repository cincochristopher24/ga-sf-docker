<?php
/**
 * <b>Group Event</b> controller.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
require_once("travellog/controller/Class.AbstractEvent.php"); 
require_once("travellog/model/Class.Calendar.php");
require_once("Class.Template.php");
require_once("Class.Paging.php");
require_once("Class.ObjectIterator.php");
require_once("Class.Date.php");
require_once('travellog/model/navigator/Class.BackLink.php');
require_once("travellog/model/Class.SubNavigation.php");

class GroupEventController extends AbstractEvent
{
	private static
	
	$GROUP        = 1,
	
	$ACTIVITY     = 0;
	
	private 
	
	$travelerID = 0,
	
	$primaryID  = NULL;

	
	function isValidForm( $formvars )
	{
		if ( !STRLEN(TRIM($formvars['txtTitle']))       ) array_push( $this->errors,'Title is a requred field!'       );
		if ( !STRLEN(TRIM($formvars['txtDate']))        ) array_push( $this->errors,'Date is a requred field!'        );
		if ( !STRLEN(TRIM($formvars['txaDescription'])) ) array_push( $this->errors,'Description is a requred field!' );
		return ( count($this->errors) )? true : false;
	}
	
	function getData()
	{
		$arr_values = array();
		$obj_gevent = new GroupEvent ( $this->primaryID );
		if ( count($obj_gevent) ):
			$arr_values['txtTitle']       = $obj_gevent->getTitle();
			//$arr_values['txtDate']        = $this->obj_date->FormatSqlDate($obj_gevent->getTheDate(),'-');
			$arr_values['txtDate']        = FormHelpers::CreateCalendar($obj_gevent->getTheDate());
			$arr_values['txaDescription'] = $obj_gevent->getDescription();
			$arr_values['radPublic']      = $obj_gevent->getDisplayPublic();
			$arr_values['context']        = $obj_gevent->getContext();
			$arr_values['hidGenID']       = $obj_gevent->getGroupID();
			$arr_values['hidPrimaryID']   = $this->primaryID;
		endif;
		return $arr_values;
	}
	
	function displayForm( $formvars = array() )
	{
		$form_tpl = clone $this->main_tpl;
		$this->obj_backlink->Create();
		$form_tpl->set( 'post'              , $formvars                                            );
		$form_tpl->set( 'obj_backlink'      , $this->obj_backlink                                  );
		$form_tpl->set( 'mode'              , $this->mode                                          );
		
		$this->main_tpl->set( 'title'        , 'Group Event Form'                                   );
		$this->main_tpl->set( 'errors'       , $this->errors                                        );
		$this->main_tpl->set( 'form_tpl'     , $form_tpl->fetch('travellog/views/tpl.FrmGroupEvent.php') );
		
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		$this->main_tpl->out( 'travellog/views/tpl.Event.php' );
	}
	
	function getForm( $formvars = array() )
	{
		$this->obj_backlink->Create();
		$form_tpl = clone $this->main_tpl;
		$form_tpl->set( 'post'              , $formvars                                              );
		$form_tpl->set( 'mode'              , $this->mode                                            );
		$form_tpl->set( 'obj_backlink'      , $this->obj_backlink                                    );
		$form_tpl->set( 'action'            , 'manager.saveGroupEvent()'                             );
		$form_tpl->set( 'context'           , 1                                                      );
		if(strcasecmp($this->mode,'update') == 0):
			$form_tpl->set( 'cancel'        , 'manager.toggleEvent('.$formvars['hidPrimaryID'].','.$formvars['context'].')' );
		else:
			$form_tpl->set( 'cancel'        , 'manager.refreshPage()'                                );
		endif;
		
		$form_tpl->out('travellog/views/tpl.FrmGroupEvent.php' );
	}
	
	function addEntry( $formvars )
	{
		$obj_event = new GroupEvent();
		$obj_event->setGroupID       ( $formvars['hidGenID']                                       );
		$obj_event->setTitle         ( $formvars['txtTitle']                                       );
		$obj_event->setTheDate       ( $this->obj_date->CreateODBCDate($formvars['txtDate'], '/' ) );			
		$obj_event->setDescription   ( $formvars['txaDescription']                                 );
		$obj_event->setDisplayPublic ( $formvars['radPublic']                                      );
		$obj_event->Create();
	}
	
	function updateEntry( $formvars )
	{
		$obj_event = new GroupEvent  ( $this->primaryID                                            );
		$obj_event->setTitle         ( $formvars['txtTitle']                                       );
		$obj_event->setTheDate       ( $this->obj_date->CreateODBCDate($formvars['txtDate'], '/')  );			
		$obj_event->setDescription   ( $formvars['txaDescription']                                 );
		$obj_event->setDisplayPublic ( $formvars['radPublic']                                      );
		$obj_event->Update();
	}
	
	function removeEntry()
	{
		$obj_event = new GroupEvent( $this->primaryID );
		$obj_event->Delete();
	}
		
	public function View( $page )
	{
		$proceed  = false;
		if( strcasecmp($this->mode,'edit') == 0 && $this->travelerID):
			$proceed = $this->checkIfAdmin();
		endif;
		
		$factory         = GroupFactory::instance();
		$obj_group       = $factory->create(array($this->genericID));
		$subNavigation   = new SubNavigation();
		$subNavigation->setContext('GROUP');
		$subNavigation->setContextID($this->genericID);
		$subNavigation->setLinkToHighlight('EVENTS');
		
		if ( strcasecmp( get_class( $obj_group[0] ),'FunGroup') == 0 ):
			$subNavigation->setGroupType('FUN_GROUP');
		else:
			$subNavigation->setGroupType('ADMIN_GROUP');
		endif;
		
		$main_tpl = new Template();
		$contents = $this->viewContents($page);
		$main_tpl->set( 'contents'      , $contents );
		$main_tpl->set( 'gID'           , $this->genericID );	
		$main_tpl->set( 'subNavigation' , $subNavigation );	
		$main_tpl->set( 'group_title'   , $obj_group[0]->getName());	
			
		
		if($this->mode == 'edit' && $this->IsLogin && $proceed)
			$load = 'manager.displayGroupForm("mode=edit&amp;eID='.$this->primaryID.'");';
		elseif($this->mode == 'add' && $this->IsLogin)
			$load = 'manager.displayGroupForm("mode=add&amp;gID='.$this->genericID.'");';
		else
			$load = 'manager.toggleView();';
		
		
		$code = <<<BOF
<script type="text/javascript">
//<![CDATA[
	manager = new GroupEventManager();
	
	window.onload = function() 
	{
		manager.useLoadingMessage();
		$load
	}
//]]>
</script>		
BOF;
		
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		//Template::includeDependentJs("/js/prototype1.lite.js");
		Template::includeDependentJs("/min/f=js/prototype.js", array('top' => true));
		Template::includeDependentJs("/js/moo1.ajax.js");
		Template::includeDependentJs("/js/groupEventManager.js");
		Template::includeDependentJs("/js/moo.fx.js");
		Template::includeDependentJs("/js/moo.fx.pack.js");
		Template::includeDependentJs("/js/Validation.js");
		Template::setMainTemplateVar('page_location', 'Groups');
		Template::includeDependent($code);
			
		$main_tpl->out( 'travellog/views/tpl.Calendar.php'   );
	}
	
	function viewContents($page)
	{
		$factory        = GroupFactory::instance();
		switch($this->context){
			case self::$GROUP:
				$obj_event      = new GroupEvent( $this->primaryID );
				if( $this->genericID != NULL )
					$progID     = $this->genericID;
				else
					$progID     = $obj_event->getGroupID();
				break;
			case self::$ACTIVITY:
				$obj_event   = new Activity( $this->primaryID );
				$obj_program = new Program( $obj_event->getProgramID() );
				$progID      = $obj_program->getGroupID();	
				break;
		}
		
		$arr_obj_groups = array();
		$main_tpl       = new Template();
		$obj_group      = $factory->create( array($progID) );
			
		if ( strcasecmp( get_class( $obj_group[0] ),'FunGroup') == 0 ):	
			$obj_calendar   = new Calendar($obj_group[0]);
			if ($this->IsLogin){
				$obj_traveler   = new Traveler($this->travelerID);
				$obj_calendar->setPrivacyContext($obj_traveler);
			}
			$arr_obj_groups_temp = $obj_calendar->getEvents();
		else:
			$arr_obj_groups_temp = $obj_group[0]->getCalendarActivities();
		endif;
		
		if ( !$this->IsLogin && count($arr_obj_groups_temp) ):
			foreach ( $arr_obj_groups_temp as $arr_obj_group):
				if ( $arr_obj_group->getDisplayPublic() == 1 ) $arr_obj_groups[] = $arr_obj_group; 
			endforeach;
		elseif ( count($arr_obj_groups_temp) ):
			foreach ( $arr_obj_groups_temp as $arr_obj_group):
				$arr_obj_groups[] = $arr_obj_group; 
			endforeach;
		endif;
		
		//$arr_obj_groups = $arr_obj_groups_temp;
		
		if ( $this->primaryID ) $page = $this->checkPosition($arr_obj_groups, 10);

		$inc_tpl = clone $main_tpl;
		$this->obj_backlink = BackLink::instance();
		$this->obj_backlink->Create();
		
				
		$inc_tpl->set( 'obj_backlink' , $this->obj_backlink                                            );
		$inc_tpl->set( 'event_type'   , 'Calendar'                                                     );
		$inc_tpl->set( 'context'      , $this->context                                                 );
		
		$inc_tpl->set( 'action_new'   , "manager.displayGroupForm('mode=add&gID=$this->genericID')"    );
		
		if ( count($arr_obj_groups) ):
			
			$obj_paging     = new Paging( count($arr_obj_groups),$page,'action=view&gID='.$this->genericID.'&context='.$this->context,10 );
			$obj_iterator   = new ObjectIterator( $arr_obj_groups,$obj_paging );
			$obj_paging->setOnclick('manager.changeContents');
			
			$obj_iterator->rewind();
			$group_title = $obj_group[0]->getName();
			if( $this->primaryID == NULL ):
				$this->primaryID = $obj_iterator->current()->getEventID();
				$this->context   = $obj_iterator->current()->getContext(); 
			endif;
			$inc_tpl->set( 'primaryID'    , $this->primaryID                                               );
			$inc_tpl->set( 'gID'          , $this->genericID                                               );
			$inc_tpl->set( 'context'      , $this->context                                                 );
			$inc_tpl->set( 'obj_iterator' , $obj_iterator                                                  );
			$inc_tpl->set( 'obj_paging'   , $obj_paging                                                    );
			$inc_tpl->set( 'group_title'  , $group_title                                                   );
		endif;
		$inc_tpl->set( 'isOwner'      , $this->checkIfAdmin()                                          );
		return $inc_tpl->fetch('travellog/views/tpl.ViewTravelerEventContent.php');
	}
	
	function checkIfAdmin(){
		$isAdmin = false;
		$factory   = GroupFactory::instance();
		switch($this->context){
			case self::$GROUP:
				if($this->genericID == NULL){
					$obj_event = new GroupEvent( $this->primaryID );
					$obj_group = $factory->create( array($obj_event->getGroupID()) );
					$this->genericID = $obj_event->getGroupID();
				}else{
					$obj_group = $factory->create( array($this->genericID) );
				}
				if ( $obj_group[0]->getAdministrator()->getTravelerID() == $this->travelerID ): 
					$isAdmin = true;
					if(strcasecmp($this->mode,"delete")==0): 
						$obj_event->Delete();
					endif;
				else:
					$this->primaryID = NULL;
				endif;
				break;
			case self::$ACTIVITY:
				if($this->genericID == NULL){
					$obj_event   = new Activity(  $this->primaryID );
					$obj_program = new Program( $obj_event->getProgramID() );
					$obj_group   = $factory->create( array($obj_program->getGroupID()) );
					$this->genericID = $obj_program->getGroupID();
				}else{
					$obj_group = $factory->create( array($this->genericID) );
				}
				if ( $obj_group[0]->getAdministrator()->getTravelerID() == $this->travelerID ): 
					$isAdmin = true;
					if(strcasecmp($this->mode,"delete")==0): 
						$obj_event->Delete();
					endif;
				else:
					$this->primaryID = NULL;
				endif;
				break;
		}	
	
		return $isAdmin;
	}
	
	private function checkPosition( $arr_objects,$rowsperpage ){
		$page = 1;
		for($ctr=0; $ctr < count($arr_objects); $ctr++ ):
			if ( $arr_objects[$ctr]->getEventID() == $this->primaryID ):
				$page = ceil(($ctr+1)/$rowsperpage);
				$this->context = $arr_objects[$ctr]->getContext();				
				break;
			endif;
		endfor;
		return $page;
	}
	
	/**
	 * Setters and Getters
	 */
	public function setPrimaryID( $primaryID )
	{
		$this->primaryID = $primaryID;
	}
	public function getPrimaryID()
	{
		return $this->primaryID;
	}
	
	public function setTravelerID( $travelerID )
	{
		$this->travelerID = $travelerID;
	}
	
}
?>
