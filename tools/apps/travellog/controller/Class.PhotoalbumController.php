
<?

require_once('travellog/model/Class.PhotoAlbum.php');
require_once('travellog/model/Class.AdminGroup.php');


class PhotoAlbumController{
	
	private $photoalbum;	
	private $groupid;
	private $loginid;				//$travelerID
	private $owner = false;
	
	function initialize(){				
			$group = new AdminGroup($this->groupid);
			
			$this->photoalbum = $group->getPhotoAlbums();	
			
			if($group->getAdministrator()->gettravelerID() == $this->loginid){				
				$this->owner = true;
			}	
	}

	
	function __call($method, $arguments) {
				//prefix ->get and set <- to lowercase
		        $prefix = strtolower(substr($method, 0, 3));
		        $property = strtolower(substr($method, 3));
		
		        if (empty($prefix) || empty($property)) {
		            return;
		        }
		
		        if ($prefix == "get" && isset($this->$property)) {
		            return $this->$property;
		        }
		
		        if ($prefix == "set") {
		            $this->$property = $arguments[0];
		        }
    	}
	
	
	
}












?> 