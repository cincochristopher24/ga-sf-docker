<?php
interface gaISubject{
	function registerObserver(gaIObserver $observer);
	function removeObserver(gaIObserver $observer);
	function notifyObservers(gaAbstractEvent $evt); 
}
?>
