<?php
/*
 * Created on 11 12, 07
 * 
 * Author: J. LLano
 * Purpose: The default controller for "Photo" module in goabroad.net
 * 
 */
 
 require_once('travellog/controller/Class.AbstractProfileController.php');
 
 class GANETProfileController extends AbstractProfileController {
 	function performAction(){
		parent::performAction();
	}
 }
?>
