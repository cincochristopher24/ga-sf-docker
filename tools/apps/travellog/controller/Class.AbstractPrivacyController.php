<?php
/*
 * Created on 11 16, 07
 * 
 * Author: K. Gordo
 * Purpose: Controller for 'privacy' module
 * 
 */
 
 require_once("travellog/controller/Class.IController.php");
 require_once("travellog/factory/Class.FileFactory.php");
 
 class AbstractPrivacyController {
 	
 	/**************************************************************************************
 	 * edits of neri:
 	 * 		registers the class used by the profile component in constructor: 	11-06-08 
 	 * 		added codes for displaying the profile component and for 
 	 * 			highlighting the profile tab in performAction():				11-06-08
	 * 		added curr_page parameter to init method of profile component:		11-10-08
	 * 		excluded online status and resume in infos to be displayed:			11-18-08
	 * 		set value of receiveUpdates if case is updateprivacy:				01-08-09
	 * 		set cobrand to false in the constructor:							01-15-09
	 * 		added network as template variable:									01-15-09
	 * 		added receiveCoParticipantsNotification and 
	 * 			receiveSubgroupDiscussionActivites for discussion 
	 * 			notifications													01-26-09
	 * 		added functions for setting and getting the preference of the
	 * 			traveler in receiving notifications from group discussions 		01-26-09
	 * 		redirect user if not login to login.php								01-29-09
 	 **************************************************************************************/
 	
 	protected 
	
	$obj_session  = NULL,
	
	$file_factory = NULL;
	
	function __construct(){
		$this->session_manager = SessionManager::getInstance();
		
		$this->file_factory = FileFactory::getInstance();
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('SubNavigation');		
		$this->file_factory->registerClass('Template',array('path'=>''));
		$this->file_factory->registerClass('TravelBioUser' , array('path' => 'travellog/model/travelbio/'));
		$this->file_factory->registerClass('HelpText');
		$this->file_factory->registerClass('SessionManager');
		$this->file_factory->registerTemplate('LayoutMain');
		
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('GroupFactory');
		
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		
		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		
		$this->cobrand = false;	
	}
	
	function performAction(){
	 	  	if ($this->cobrand) 
				$network = $GLOBALS['CONFIG']->getSiteName(); 
			else
				$network = 'GoAbroad Network'; 
	 	  
	 	    $sessMan = $this->file_factory->invokeStaticClass('SessionManager','getInstance',array());
			$travelerID = $sessMan->get('travelerID');
		 		 
		 /***************************************************
		  * check if logged travelerid > 0
		  */
			/*if ($travelerID == 0)
		  		header("Location: /index.php");*/

			if( !$sessMan->getLogIn() && !$travelerID ){
				header("location: login.php?failedLogin&redirect=privacy.php?" . $_SERVER['QUERY_STRING']);
				exit;			 
			}
		  		
		 /****************************************************
		  *  initialization
		  */
		 	 $fileFact = $this->file_factory; 
			 $traveler = null;
			 try {			 	
			 	$traveler = $fileFact->getClass('Traveler',array($travelerID));
			 	
			 } catch (Exception $e){
			 	header("location : /index.php");	 	 	 	
			 }
			 
			 
			 $template = $fileFact->getClass('Template');
			 $template->set_path("travellog/views/");
		  	
		  	$action = '';
		  	
		  	if (isset($_REQUEST['action']) )
		  		$action = $_REQUEST['action'];
		  		
		  	if (isset($_REQUEST['username']))
		  		$username = $_REQUEST['username'];	
		  		
		  	$displayDiscussionThreadNotification = $traveler->isMemberOfAnAdminGroup();
		  	$displayActivityNotification = $traveler->isMemberOfASubgroup();
		 
		 /********************************************************
		  *	DISPATCH actions
		  */
		  	
		  	switch ($action) {
				case 'block' :
					$username = $_REQUEST['username'];			
					$tid = $traveler->getTravelerIDByUsername($username);

					// added by neri so that owner can't block himself: 12-02-08
					if ($tid == $travelerID){
						$template->set("error","You cannot block yourself");
					}else if(in_array($tid, $traveler->getBlockedUsers($idonly=true))){
						$template->set("error","You have already blocked " . $username ." from viewing your profile.");
					}else {
						if (0 == $tid )
							$template->set("error","Cannot find user '" . $username ."'");			
						else 
							$traveler->blockUser($tid);
					}
					
					break;
				case 'unblock':
					$username = $_REQUEST['username'];
					$tid = $traveler->getTravelerIDByUsername($username);	
					
					if (0 == $tid){
						echo "Cannot find user '" . $username ."'";
						// $template->set("error","Cannot find user '" . $username ."'");
					}else{
						$traveler->unblockUser($tid);			
					}
					exit;	
					break;
				case 'updateprivacy':
					$showNamesTo = $_REQUEST['names'];
					$showEmailTo = $_REQUEST['email'];
					$showGroupsTo = $_REQUEST['groups'];
					$showFriendsTo = $_REQUEST['friends'];
					//$showOnlineTo = $_REQUEST['online'];
					$showCalendarTo = $_REQUEST['calendar'];
					//$showResumeTo = $_REQUEST['resume'];
					$receiveUpdates = $_REQUEST['updates'];
					$receiveCoParticipantsNotification = $displayDiscussionThreadNotification ? $_REQUEST['coparticipants_notification'] : 8;
					$receiveSubgroupDiscussionActivites = $displayActivityNotification ? $_REQUEST['activity_notification'] : 10;
					
					$notify_on_new_je = false;
					$notify_on_edit_je = false;
					$notify_on_new_photo = false;
					
					if (isset($_REQUEST['notifynewjournalentry']))
						$notify_on_new_je = true;
					
					if (isset($_REQUEST['notifyeditjournalentry']))
						$notify_on_edit_je = true;
						
					if (isset($_REQUEST['notifynewphoto']))
						$notify_on_new_photo = true;		
						
					$pp = $traveler->getPrivacyPreference();
					$pp->setShowNamesTo($showNamesTo);
					$pp->setShowEmailTo($showEmailTo);
					$pp->setShowGroupsTo($showGroupsTo);
					$pp->setShowFriendsTo($showFriendsTo);
					//$pp->setShowOnlineTo($showOnlineTo);
					$pp->setShowCalendarTo($showCalendarTo);
					//$pp->setShowResumeTo($showResumeTo);
					$pp->setModeOfReceivingUpdates($receiveUpdates);
					$pp->setModeOfReceivingNotificationFromCoParticipants($receiveCoParticipantsNotification);
					$pp->setModeOfReceivingNotificationFromSubgroupDiscussionActivities($receiveSubgroupDiscussionActivites);
					
					$pp->setNotifyOnNewJE($notify_on_new_je);
					$pp->setNotifyOnEditJE($notify_on_edit_je);
					$pp->setNotifyOnNewPhoto($notify_on_new_photo);
					
					$pp->update();
					$template->set('message','Privacy Preferences updated.');
				case 'default':
					break;		
				
			}  
		  
			// setup main template
			Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::includeDependentJs('/min/f=js/prototype.js');
			Template::includeDependentJs('/js/interactive.form.js');
					  
		 	// set privacy preference
		  	$template->set("pp",$traveler->getPrivacyPreference());
		  	$template->set("blockedUsers",$traveler->getBlockedUsers());
		  	$template->set("network",$network);
		  	$template->set("displayDiscussionThreadNotification", $displayDiscussionThreadNotification);
		  	$template->set("displayActivityNotification", $displayActivityNotification);
		  	
		  	$snav = $fileFact->getClass('SubNavigation');
		 	$snav->setContext('TRAVELER');	
		 	$snav->setContextID($travelerID);
		 	$snav->setLinkToHighlight('MY_PROFILE');
		  	
		  	$template->set('snav',$snav);
		  	
		  	$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($travelerID, AbstractProfileCompController::$EDIT_PRIVACY_PREFERENCES);
			
			$template->set('profile', $profile_comp->get_view());
		  	
		  	// set text for blocked users
		  	$ht = $fileFact->getClass('HelpText');
			$template->set('privacy_blockedusers' ,$ht->getHelpText('privacy-blockedusers'));
		  	$template->set('travelerID' ,$travelerID);
			
			$travelBioUser = $this->file_factory->invokeStaticClass('TravelBioUser','getTravelBioUserByTravelerID',array($travelerID));
			if (!is_null($travelBioUser)){
				$fbAction = 'Edit';
			}
			else{
				$fbAction = 'Add';
			}
			$template->set('fbAction', $fbAction);
		  	
		  	$template->out('tpl.PrivacyPreference.php');
	
		
	}
	
 }
 
 
?>
