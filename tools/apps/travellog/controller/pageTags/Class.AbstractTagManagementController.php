<?php

require_once('travellog/model/Class.SessionManager.php');
require_once('travellog/factory/Class.FileFactory.php');
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
//require 'lib/models/pageTag/GanetPageTag.php';
class AbstractTagManagementController {
	
	protected 	$file_factory,
				$obj_session,
				$tags 		= null,
				$loggedUser = NULL,
				$isLogged	= false,
				$isMemberLogged = false,
				$isAdminLogged 	= false,
				$group		= NULL,
				$subNav		= NULL,
				$profile	= NULL,  // the group profile that shows above the subnavigation
				$tpl		= NULL,
				$params		= array("action" =>	NULL,
									"gID"	 =>	0	),
				$errMessage = null,
				$warningMessage = null,
				$groupTagData = array(),
				$lowerLimit = 0;
	
	// TODO: create a centralized orderby creation
	public function __construct(){
		$this->obj_session = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance();
		$this->file_factory->registerClass('Template',array('path'=>''));
	
		$this->file_factory->registerClass('GroupFactory');
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('AdminGroup');
		
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		$this->file_factory->registerClass('BackLink' , array('path' => 'travellog/model/navigator'));			
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('ToolMan', array('path'=>''));
		$this->file_factory->registerTemplate('LayoutMain');
						
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
	
		$this->tpl = $this->file_factory->getClass('Template');
		$this->tpl->set_path("travellog/views/pageTags/");
		
	}
	
	public function performAction(){
		$this->params = array_merge($this->params,$this->_extractParameters());
		
		$method = $this->params['action'];
		if($method != "" and method_exists($this, $method)){
			$this->$method();
		}
		else {
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			$this->prepareTags();
			$this->_viewTagManager();
		}
	}
	
	/**
	 * Checks whether the current logged in is an admin user or not. If it
	 * is not an admin user it redirects the page to custom group normal view mode.
	 * 
	 * @param boolean $_privilege
	 * @return void
	 */
	function _allowAdminPrivileges(){
		if (!$this->isAdminLogged){
			header("location: /group-pages.php?gID=".$this->group->getGroupID());
			exit;
		}
	}
	
	function _extractParameters(){
		$params = $_REQUEST;
		$paramArray = array();

		if( isset($params) ){
			foreach( $params as $key => $value){
				$paramArray[$key] = $value;
			}
		}
		return $paramArray;
	}
	
	function _redirect($page=NULL){
		if( "main-home" == $page ){
			header("location: /");
		}elseif( "404" == $page || 404 == $page ){
			header("location: /FileNotFound.php");
		}elseif( "main-groups" == $page ){
			header("location: /group.php");
		}
		exit;
	}
	
	function _prepareDependencies(){
		Template::setMainTemplateVar('layoutID', 'subgroups');
		Template::includeDependentCss("/min/g=GroupPagesCss");
		Template::includeDependentJs("/min/g=GroupPagesJs");
	//	Template::includeDependentJs("/min/g=TagManagerJs");
		
	}
	
	function _defineCommonAttributes(){
		if( isset($GLOBALS["CONFIG"]) ){
			$this->params["gID"] = $GLOBALS["CONFIG"]->getGroupID();
		}
		
		if( 0 == $this->params["gID"] ){
			$this->_redirect("main-home");
		}
		$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($this->params['gID']));
		if( !count($mGroup) ){
			$this->_redirect("main-groups");
		}
		
		$this->group = $mGroup[0];
		if( !$this->group->isParent() ){
			$this->_redirect(404);
		}
		
		$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? TRUE : FALSE;
		$this->loggedUser 		= 	$this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
		
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		
		if( !$this->isAdminLogged ){
			$this->_redirect("main-home");
		}
		else {
			if( isset($GLOBALS["CONFIG"]) ){
				Template::setMainTemplateVar('page_location','Home');
			}else{
				Template::setMainTemplateVar('page_location','Groups / Clubs');
			}
		}
		
		$this->subNav = $this->file_factory->getClass('SubNavigation');
		$this->subNav->setContext('GROUP');
		$this->subNav->setContextID($this->params['gID']);
		$this->subNav->setLinkToHighlight('SUBGROUPS');
		
		$this->profile = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		$this->profile->init($this->group->getGroupID());
		
		$this->groupTags = GanetPageTagPeer::retrieveByGroupID($this->group->getGroupID());
		
		foreach($this->groupTags as $iGroupTag){
			$this->groupTagData[] = $iGroupTag->getName();
		}
	}
	
	function _defineCommonAttributes2(){
		if( isset($GLOBALS["CONFIG"]) ){
			$this->params["gID"] = $GLOBALS["CONFIG"]->getGroupID();
		}
		
		if( 0 == $this->params["gID"] ){
			$this->_redirect("main-home");
		}
		$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($this->params['gID']));
		if( !count($mGroup) ){
			$this->_redirect("main-groups");
		}
		
		$this->group = $mGroup[0];
		if( !$this->group->isParent() ){
			$this->_redirect(404);
		}
		
		$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? TRUE : FALSE;
		$this->loggedUser 		= 	$this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
		
		if( !$this->isAdminLogged ){
			$this->_redirect("main-home");
		}
	}
	
	function _enableLogging(){
		// check if user is currently logged in
		if ( $this->isAdminLogged ){
			// start GA Logger
			require_once 'gaLogs/Class.GaLogger.php';
			try{
				GaLogger::create()->start($this->loggedUser->getTravelerID(), 'TRAVELER');
			}
			catch(exception $e){
			}
		}
	}
	
	function _prepareContent($ajaxRequest = false){
		$currentPage = isset($this->params['page']) ? $this->params['page'] : 1;
		$pagination = $this->_preparePaging($currentPage);	
		$tagsList = $this->_viewTagManagementList($pagination, 0, $currentPage, $ajaxRequest);
		
		return $tagsList;
	}
	
	function _viewTagManager(){
		$tplSrchTag = $this->file_factory->getClass('Template');
		$tplSrchTag->set("gID",$this->params['gID']);
		$tplSrchTag->set("searchTagKeyword", isset($this->params['searchTagKeyword']) ? $this->params['searchTagKeyword'] : '');
		$tplSearchTag = $tplSrchTag->fetch('travellog/views/pageTags/tpl._searchTag.php');
		
		
		$mytpl = $this->file_factory->getClass('Template');
		$mytpl->set('tagsList',$this->_prepareContent());
		$mytpl->set('tagCount',(!is_null($this->tags[0])) ? $this->tags[0]->getTotalRecordCount() : 0);
		$mytpl->set('groupTagData',$this->groupTagData);
		$mytpl->set("searchTagKeyword", isset($this->params['searchTagKeyword']) ? $this->params['searchTagKeyword'] : '');
		$mytpl->set("gID",$this->params['gID']);
		$tagManagement = $mytpl->fetch('travellog/views/pageTags/tpl.tagManagement.php');
		
		$this->tpl->set("gID",$this->params['gID']);
		$this->tpl->set("tagManagement",$tagManagement);
		$this->tpl->set("tplSearchTag",$tplSearchTag);
		$this->tpl->set("tplSearchTag",$tplSearchTag);
		$this->tpl->set("profile",$this->profile->get_view());
		$this->tpl->set("subNavigation",$this->subNav);
		$this->tpl->out("tpl.tagManagementMain.php");
	}
		
	private function _filterTag($tag){
		if(strlen($tag) < 3 ){
			$this->errMessage = 'Tagname "'.$tag.'" is too short and was not saved!';
			return false;
		}
		if(strlen($tag) > 20){
			$this->errMessage = 'Tagname "'.$tag.'" is too long and was not saved!';
			return false;
		}
		return true;
	}
	
	public function saveTag(){
		$this->_defineCommonAttributes2();
		$this->_enableLogging();
		
		$tags = $this->params['tag'];
		$tags = str_replace(",", " ", $tags);
		$tagsArr = explode(" ", $tags);
		$gID = $this->params['gID'];
		$tagIDs = array();
		$tagID = 0;
		foreach($tagsArr as $tag){
			$tag = trim($tag);
			if($tag != ""){
				$ganetPageTag = GanetPageTagPeer::retrievePageTagByNameAndGroupID($tag, $gID);
				if(is_null($ganetPageTag) AND $this->_filterTag($tag)){
					$tagID = $this->_doSaveTag($tag, $gID);
				}
			}
		}
		
		$this->_prepareDependencies();
		
		$orderCondition = 'weight DESC';
		$pos = GanetPageTagPeer::retrieveTagPosition($gID,$tagID,array('ORDER_BY' => $orderCondition));
		$pageNum = ceil($pos/15);
		$curPage = ($pageNum <= 0)?1:$pageNum;
		
		$this->prepareTags($gID, $curPage);
		$pagination = $this->_preparePaging($curPage);
		
		echo $this->_viewTagManagementList($pagination, $tagID, $curPage);
		
		
	}
		
	public function prepareTags($gID = 0, $curPage = 0, $nConditions = array()){
		$groupID = (0 == $gID) ? $this->params['gID'] : $gID;		
		$curPage = (0 < $curPage) ? $curPage : (isset($this->params['page']) ? $this->params['page'] : 1);		
		$lowerLimit = 0;
		$upperLimit = 15;
		if(1 < $curPage){
			$lowerLimit = $upperLimit * ($curPage - 1);
		}
		$conditionLimit = 'LIMIT '.$lowerLimit.', 15';
		$conditions = array('LIMIT' => $conditionLimit);
		if(!empty($nConditions)){
			$conditions = array_merge($conditions, $nConditions);
		}
		
		// assign default order by condition if not yet set
		if(!isset($conditions['ORDER_BY'])) {
			$conditions = array_merge($conditions, array('ORDER_BY' => 'weight DESC'));
		}
		
		$this->tags = GanetPageTagPeer::retrieveAllByGroupID($groupID, $conditions);
		
	}
	
	public function editTag(){
		$this->_defineCommonAttributes2();
		$this->_enableLogging();
		
		$id = $this->params['tagID'];
		$tag = $this->params['tag'];
		$gID = $this->params['gID'];
		$msg = "";
		$status = null;
		if(0 < $id){
			$ganetPageTag = GanetPageTagPeer::retrieveByPK($id);
			
			if($ganetPageTag->getName() != $tag){
				$pageTag = GanetPageTagPeer::retrievePageTagByNameAndGroupID($tag, $gID);
				if(!is_null($pageTag)){
					$status = false;
					$msg = "Tag name ".$pageTag[0]->getName()." already exist";
				}
				else {
					$prevName = $ganetPageTag->getName();
					if($this->_filterTag($tag)){
						$ganetPageTag->setName($tag);
						$ganetPageTag->save();
						$status = true;
						$msg = "You've successfully renamed <strong>".$prevName."</strong> to <strong>".$ganetPageTag->getName()."</strong>.";
					}
					else {
						$tag = $prevName;
						$msg = $this->errMessage;
						$status = false;
					}
					
				} 
			}
			
		}
		
		$data = array("tagStatus" => $status, "tagName" => $tag, "errMessage" => $msg);
		echo json_encode($data);
	}
	
	public function searchTag(){
		$this->_defineCommonAttributes();
		
		$tagName = isset($this->params['searchTagKeyword']) ? $this->params['searchTagKeyword'] : '';
		$gID = $this->params['gID'];
		$curPage = (isset($this->params['page']) ? $this->params['page'] : 1);
		
		$whereCondition = array();
		if($tagName != ""){
			$whereCondition = $this->_prepareSearchTagCondition($tagName);
		//	$this->_doSearchTag($tagName, $curPage);
		}
		
	//	if($tagName == "" OR $this->tags == null){
	//		echo 'test1';
	//		$this->prepareTags();
	//	}
		$this->_prepareDependencies();
		$this->prepareTags($gID, $curPage, $whereCondition);
		$this->_preparePaging($curPage);
	//	$pagination = $this->_preparePaging();
	//	var_dump($this->tags);
		$this->_viewTagManager();
	}
	
	private function _prepareSearchTagCondition($tagName = ""){
		$whereCondition = array();
		if($tagName != ""){
			$tagName = $tagName.'%';
			$condition = 'tblPageTags.name LIKE "'.$tagName.'"';
			$whereCondition = array('WHERE' => $condition);
		}
		
		return $whereCondition;
	}
	
	private function _doSearchTag($tagName = "", $curPage = 0){
		$gID = $this->params['gID'];
		if($tagName != "" AND $gID != 0){
			$tagName = $tagName.'%';
		//	$curPage = (isset($this->params['page']) ? $this->params['page'] : 1);
			$whereCondition = 'tblPageTags.name LIKE "'.$tagName.'"';
			$this->prepareTags($gID, $curPage, array('WHERE' => $whereCondition));
			
		}
		
	}
	
	public function deleteTag(){
		$this->_defineCommonAttributes2();
		$this->_enableLogging();
		
		$id = $this->params['tagID'];
		$gID = $this->params['gID'];
		if((0 < $id) AND (0 < $gID)){
			$obj = GanetPageTagPeer::retrieveByPK($id);
			if(!is_null($obj)){
				$obj->delete();
			}
		}
	//	$gID = isset($GLOBALS["CONFIG"]) ? $GLOBALS["CONFIG"]->getGroupID() : 0;
		
		$curPage = isset($this->params['page']) ? $this->params['page'] : 1;
		$orderBy = isset($this->params['orderBy']) ? array('ORDER_BY' => $this->params['orderBy']) : array() ;
		
		$this->_prepareDependencies();
		
		$this->prepareTags($gID, $curPage, $orderBy);
		
		
		$pagination = $this->_preparePaging($curPage);
		
		echo $this->_viewTagManagementList($pagination, 0, $curPage);
		
		
		
	//	$this->_viewTagManagementList();
		
	}
	
	public function assignTagsToGroup(){
		$this->_defineCommonAttributes();
		$this->_enableLogging();
		
		$tags = $this->params['tags'];
		$groupID = $this->params['groupID'];
		$gID = $this->params['gID'];
	//	var_dump($this->params);
		$tags = str_replace(",", " ", $tags);
		$tagsArr = explode(" ", $tags);
		$newTags = array();
		
		$this->doAssignTagsToGroup($tagsArr, $groupID, $gID);
		$mTag = $this->retrieveGroupTags($groupID, $gID);
		if(!empty($mTag)){
			$newTags[$groupID] = $mTag;
		}
		
		echo json_encode($newTags);
	}
	
	public function assignTagsToGroups(){
	//	var_dump($this->params);
		$this->_defineCommonAttributes();
		$this->_enableLogging();
	
		$groupIDs = $this->params['groupIDs'];
		$groupIDs = explode(' ',$groupIDs);
		
		$tags = $this->params['tags'];
		$tags = str_replace(",", " ", $tags);
		$tagsArr = explode(" ", $tags);
		
		$gID = $this->params['gID'];
		
		$newTags = array();
		foreach($groupIDs as $groupID){
			if(trim($groupID) != ""){
				$this->doAssignTagsToGroup($tagsArr, $groupID, $gID);
			}
			
			$mTag = $this->retrieveGroupTags($groupID, $gID);
			if(!empty($mTag)){
				$newTags[$groupID] = $mTag;
			}
		}
		//var_dump($newTags);
		echo json_encode($newTags);
	}
	
	private function doAssignTagsToGroup($tags = array(), $groupID = 0, $gID = 0){
		if(0 < $groupID):
		//	$newTags = array();
			foreach($tags as $tag):

				$tag = trim($tag);
				if($tag != "" AND $this->_filterTag($tag)):
					$ganetPageTag = GanetPageTagPeer::retrievePageTagByNameAndGroupID($tag, $gID);
					if(is_null($ganetPageTag) AND $this->_filterTag($tag)){
						$this->_doSaveTag($tag, $gID);
						// reinstantiate $ganetPageTag
						$ganetPageTag = GanetPageTagPeer::retrievePageTagByNameAndGroupID($tag, $gID);
					}

					$pageTagID = $ganetPageTag[0]->getID();
					$ganetGroupTag = GanetGroupTagPeer::retrieveByGroupIDAndPageTagID($groupID, $pageTagID);
					if(is_null($ganetGroupTag)){
						$ganetGroupTag = new GanetGroupTag();
						$ganetGroupTag->setGroupID($groupID);
						$ganetGroupTag->setPageTagID($pageTagID);
						$ganetGroupTag->save();
						
					}

				endif;
			endforeach;
			
		//	return $newTags;
		endif;
	}
	
	public function removeAssignedTag(){
		$this->_defineCommonAttributes();
		$this->_enableLogging();
		
		$tagID = $this->params['tagID'];
		$groupID = $this->params['groupID'];
		$gID = $this->params['gID'];
		$category = $this->params['category'];
		
		GanetGroupTagPeer::removeGroupTags(array($groupID), array($tagID));
		
		$newTags = array();
		if($category != 'TAGGED'){
			$mTag = $this->retrieveGroupTags($groupID, $gID);
			if(!empty($mTag)){
				$newTags[$groupID] = $mTag;
			}
		}
		
		echo json_encode($newTags);
	}
	
	public function viewSortedTags(){
		$gID = $this->params['gID'];
		//$orderBy = ('DEFAULT' == $this->params['orderBy']) ? 'tblPageTags.name' : 'weight '.$this->params['orderBy'];		
		/***
		$orderMap = array(
			'TAG_NAME_ASC' 	=> 'name ASC',
			'TAG_NAME_DESC' => 'name DESC',
			'WEIGHT_ASC'	=> 'weight ASC',
			'WEIGHT_DESC'	=> 'weight DESC'
		);		
		$orderBy = isset($this->params['orderBy']) && isset($orderMap[$this->params['orderBy']]) 
					? $orderBy = $orderMap[$this->params['orderBy']]
					: $orderBy = $orderMap['TAG_NAME_ASC'];
		***/
		$orderBy = !isset($this->params['orderBy']) ? 'tblPageTags.name ASC' : $this->params['orderBy'];
		$tagName = isset($this->params['searchTagKeyword']) ? $this->params['searchTagKeyword'] : '';
		$curPage = isset($this->params['page']) ? $this->params['page'] : 1;
		$this->_prepareDependencies();
		
		$searchTagCondition = array();
		if($tagName != ""){
			$searchTagCondition = $this->_prepareSearchTagCondition($tagName);
		//	$this->_doSearchTag($tagName, $curPage);
		}
		
		$conditions = array('ORDER_BY' => $orderBy);
		
		if(!empty($searchTagCondition)){
			$conditions = array_merge($conditions, $searchTagCondition);
		}
		
	//	$this->prepareTags($gID, $orderBy);
		
		$this->prepareTags($gID, $curPage, $conditions);
		
		$pagination = $this->_preparePaging($curPage);
		
		echo $this->_viewTagManagementList($pagination, 0, $curPage);
		
	//	$html = $this->_prepareContent(true);
	//	echo $html;
		
	}
	
	public function paginate(){
		$gID = $this->params['gID'];
		$orderBy = ('DEFAULT' == $this->params['orderBy']) ? 'tblPageTags.name' : $this->params['orderBy'];
		$tagName = isset($this->params['searchTagKeyword']) ? $this->params['searchTagKeyword'] : '';
		$curPage = isset($this->params['page']) ? $this->params['page'] : 1;
		$this->_prepareDependencies();
		//echo $this->params['orderBy']; exit;
		$searchTagCondition = array();
		if($tagName != ""){
			$searchTagCondition = $this->_prepareSearchTagCondition($tagName);
		//	$this->_doSearchTag($tagName, $curPage);
		}
		
		$conditions = array('ORDER_BY' => $orderBy);
		
		if(!empty($searchTagCondition)){
			$conditions = array_merge($conditions, $searchTagCondition);
		}
		
		$this->prepareTags($gID, $curPage, $conditions);
		
	/*	if($tagName != ""){
			$this->_doSearchTag($tagName, $curPage);
		}
		
		if($tagName == "" OR $this->tags == null){
			$this->prepareTags($gID, $orderBy);
		}	*/	
		
		
		$pagination = $this->_preparePaging($curPage);

		echo $this->_viewTagManagementList($pagination, 0, $curPage);
		
	//	$html = $this->_prepareContent(true);
	//	echo $html;
	}
	
	public function refreshGroupTagLabels(){
		$gID = $this->params['gID'];
		$groupTags = GanetPageTagPeer::retrieveByGroupID($gID);
		$groupTagData = array();
		foreach($groupTags as $iGroupTag){
			$groupTagData[] = $iGroupTag->getName();
		}
		
		echo json_encode($groupTagData);
	}
	
	private function _viewTagManagementList($pagination = array(), $tagID = 0, $currentPage = 1, $ajaxRequest = true){
		$tplTagsList = $this->file_factory->getClass('Template');
		$tplTagsList->set("paging", $pagination['paging']);
	    $tplTagsList->set("iterator", $pagination['iterator']);
		$tplTagsList->set('tags',$this->tags);
		$tplTagsList->set('tagID',$tagID);
		$tplTagsList->set('currentPage',$currentPage);
		$tplTagsList->set('ajaxRequest',$ajaxRequest);	
		$tplTagsList->set("searchTagKeyword", isset($this->params['searchTagKeyword']) ? $this->params['searchTagKeyword'] : '');	
		$tagsList = $tplTagsList->fetch('travellog/views/pageTags/tpl.tagManagementContent.php');
		return $tagsList;
	}
	
	private function _preparePaging($curPage = 1){
		$currentPage = $curPage;
		$totalRecords = (!is_null($this->tags[0])) ? $this->tags[0]->getTotalRecordCount() : 0;
		$paging = new Paging($totalRecords ,$currentPage,'', 15);
		$paging->setOnclick('jQuery.fn.pagination');
		$iterator = new ObjectIterator($this->tags,$paging,TRUE);
		
		return array('paging' => $paging, 'iterator' => $iterator);
	}
	
	private function _doSaveTag($tag, $gID){
		$pageTag = new GanetPageTag();
		$pageTag->setGroupID($gID);
		$pageTag->setName($tag);
		$pageTag->save();
		return $pageTag->getID();
	}
	
	private function retrieveGroupTags($groupID = 0, $gID = 0){
		
		$tags = GanetPageTagPeer::retrieveByGroup($groupID, $gID);
		$mTags = array();
		foreach($tags as $tag){
			$mTags[] = array("id" => $tag->getID(), "name" => $tag->getName());
		}
		return $mTags;
	}
	private function computeNewPageLocation(){
		
	}
}