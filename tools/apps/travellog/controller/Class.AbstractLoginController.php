<?php
	/*
	 * Class.AbstractLoginController.php
	 * Created on Nov 22, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/controller/Class.IController.php");
	 require_once("travellog/factory/Class.FileFactory.php");
	 require_once("travellog/model/Class.Session.php");
	 require_once("loginLogger/Class.LoginLogger.php");
	 class AbstractLoginController implements IController{
	 	
	 	protected $file_factory = NULL;
	 	protected $login_helper = NULL;
	 	
	 	function __construct(){
	 		
	 		$this->file_factory = Filefactory::getInstance();
	 		//require_once("travellog_/domains/" . $_SERVER['SERVER_NAME'] . ".php");
	 			 		
	 		$this->file_factory->registerClass("Traveler", array("path" => "travellog/model/"));
	 		$this->file_factory->registerClass("CacheFlags", array("path" => "travellog/model/"));
	 		$this->file_factory->registerClass("AdminGroup", array("path" => "travellog/model/"));
	 		$this->file_factory->registerClass("MailGenerator", array("path" => "travellog/model/"));
	 		$this->file_factory->registerClass("HelpText", array("path" => "travellog/model/"));
	 		$this->file_factory->registerClass("SessionManager", array("path" => "travellog/model/"));
	 		$this->file_factory->registerClass('Session', array('path' => 'travellog/model'));
	 		$this->file_factory->registerClass("LoginHelper", array("path" => "travellog/helper/"));
	 		$this->file_factory->registerClass("LoginFcd", array("path" => "travellog/facade/"));
	 		$this->file_factory->registerClass("MainLayoutView", array("path" => "travellog/views/"));
	 		$this->file_factory->registerClass("Crypt", array("path" => ""));
	 		$this->file_factory->registerClass("Connection", array("path" => ""));
	 		$this->file_factory->registerClass("Recordset", array("path" => ""));
	 		$this->file_factory->registerClass("HelperGlobal", array("path" => ""));
	 		
	 		$this->login_helper = $this->file_factory->invokeStaticClass("LoginHelper","getInstance");
	 			
	 		// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
				 	
	 	}
	 	
	 	function performAction(){
	 		
	 		switch( $this->login_helper->getAction() ){
	 			
	 			case constants::AUTO_LOGIN:
	 				return $this->activateAndLogin();
	 				break;
	 			
	 			case constants::LOGIN:
	 				return $this->loginUser();
	 				break;
	 			
	 			case constants::NON_LOGIN_PAGE_FILL_UP:
	 				return $this->viewLoginForm();
	 				break;	
	 			
	 			case constants::LOGOUT:
	 				$sessionManager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");
	 				$nAppendLink = mt_rand(111111,999999) . $sessionManager->get("travelerID");
	 				$this->endSession();
					ob_end_clean();
	 				header("Location: /index.php?logout=$nAppendLink");
					exit;
	 				break;
	 			
	 			case constants::FAILED_LOGIN:
	 				$ht = $this->file_factory->getClass("HelpText");
	 				// $loginData["msg"] = nl2br($ht->getHelpText('register-failedlogin'));
	 				$loginData["failed"] = true;
	 				$this->viewLoginPage($loginData);
	 				break;	
	 				
	 			default:
	 				$this->viewLoginPage();
	 		}
	 		
	 	}
	 	
	 	function activateAndLogin(){
	 		
	 		$ref = $this->login_helper->getActivationKey();
	 		$crypt = $this->file_factory->getClass("Crypt");
			$myString = str_replace(" ","+",$ref);
			$travID = $crypt->decrypt($myString); 
		
			if( !is_numeric($travID) ){
				ob_end_clean();
	 			header("location: register.php");
	 			exit;
	 		}	
			
			$traveler = $this->file_factory->getClass("Traveler",array($travID));
	 		if( !$traveler->isActive() ){
	 			// email welcome message 
				try{
					$mailGenerator = $this->file_factory->getClass("MailGenerator", array($travID,3));
					$mailGenerator->send();	
				}
				catch( exception $e ){}	
	 		}
	 			
			$crypt = $this->file_factory->getClass("Crypt");
			$cryptTravID = $crypt->encrypt($travID);
			$cryptTravID = substr($cryptTravID,0,strlen($cryptTravID)-1);
			return $this->startSession($travID,$cryptTravID);
			
		}
		
		function validateEmailPassword(){
			
			/**
	 		 * create database utility object
	 		 * check for user using the same username and password
	 		 */
	
			$nConnection = $this->file_factory->getClass("Connection");
	 		$nRecordset = $this->file_factory->getClass("Recordset",array($nConnection));
	 		$status = array("loginStatus" => constants::DISALLOW_ERROR, "travelerID" => 0);
	 		
	 		$loginData = $this->login_helper->readLoginData();
	 		
	 		if( trim($loginData["password"]) && trim($loginData["email"]) ) {
				$sqlquery = "SELECT travelerID,active,isSuspended,deactivated FROM tblTraveler WHERE email='" . addslashes($loginData["email"]) . 
		 			"' AND password='" . addslashes($loginData["password"]) . "' AND isSuspended = 0 AND deactivated = 0";
				$myquery = $nRecordset->Execute($sqlquery);
				if( 0 == $nRecordset->Recordcount()){
					$sqlquery = "SELECT travelerID,active,isSuspended,deactivated FROM tblTraveler WHERE email='" . addslashes($loginData["email"]) . 
			 			"' AND password='" . addslashes($loginData["password"]) . "'";
					$myquery = $nRecordset->Execute($sqlquery);
				}
		 		
		 		if( $tmptrav = mysql_fetch_array($myquery) ){
		 			$status["travelerID"] = $tmptrav["travelerID"];
		 			if( $tmptrav['isSuspended'] )
		 				$status["loginStatus"] = constants::DISALLOW_SUSPENDED;
		 			elseif( !$tmptrav["active"] )
		 				$status["loginStatus"] = constants::DISALLOW_INACTIVE;
					elseif( $tmptrav["deactivated"] )
						$status["loginStatus"] = constants::DISALLOW_DEACTIVATED;
		 			else
		 				$status["loginStatus"] = constants::ALLOW;
		 		}	
	 		}
	 		else
	 			$status['loginStatus'] = constants::DISALLOW_ERROR;
	 			
 			return $status;
		} 
		 
	 	function loginUser(){
			$this->logLogin(); // logger
			
	 		$status = $this->validateEmailPassword();
			switch($status["loginStatus"]){
				
				// inactive traveler
				case constants::DISALLOW_INACTIVE:
					$crypt = $this->file_factory->getClass("Crypt");
					$cryptTravID = $crypt->encrypt($status["travelerID"]);
					$cryptTravID = substr($cryptTravID,0,strlen($cryptTravID)-1);
					ob_end_clean();
					header("location: activation-support.php?tID=$cryptTravID");
					break;
				
				case constants::DISALLOW_SUSPENDED:
					$loginData["msg"] = 'Sorry, your account is currently suspended.  If you have questions <a href=\'feedback.php\'>contact us</a>.';
					if( $this->login_helper->getCurrentView() == constants::VIEW_LOGIN_PAGE  )
						$this->viewLoginPage($loginData);
					else
						return $this->viewLoginForm($loginData);
					break;	
					
				case constants::DISALLOW_DEACTIVATED:
					$loginData["msg"] = 'Sorry, but it appears that you have previously requested to deactivate your account.<br />If you have questions or you want to reactivate you it, <a href=\'feedback.php\'>contact us</a>.';
					if( $this->login_helper->getCurrentView() == constants::VIEW_LOGIN_PAGE  )
						$this->viewLoginPage($loginData);
					else
						return $this->viewLoginForm($loginData);
					break;
				
				case constants::ALLOW:
					$crypt = $this->file_factory->getClass("Crypt");
					$this->startSession($status["travelerID"],$crypt->encrypt($status["travelerID"]));		
					$GLOBALS['CACHE_FLAGS'] = $this->file_factory->getStaticClass("CacheFlags", "getInstance");
					$GLOBALS['CACHE_FLAGS']->resetAll();   
					break;
					
				default:
					$loginData["msg"] = "Either your email or password is wrong. Please make sure you typed your email address correctly.";
					if( $this->login_helper->getCurrentView() == constants::VIEW_LOGIN_PAGE  )
						$this->viewLoginPage($loginData);
					else
						return $this->viewLoginForm($loginData);
						
			}
	 	}
	 	
	 	function startSession($travelerID,$sessionKey = ""){
	 		
	 		$crypt = $this->file_factory->getClass("Crypt");	 		
	 		if( $travelerID != $crypt->decrypt($sessionKey) ){
	 			return $this->viewLoginPage();
	 		}
	 		
	 		$nConnection = $this->file_factory->getClass("Connection");
	 		$nRecordset = $this->file_factory->getClass("Recordset",array($nConnection));
			
			/** 
			* update database info for user
			* set session variables
			*/
			$appendQuery = "";
			if( $this->login_helper->isNewlyRegistered() )
				$appendQuery = ", active = 1 ";
			
			$sqlquery = "UPDATE tblTraveler set latestlogin='" . date('c') . "' " . $appendQuery . " WHERE travelerID = " . $travelerID;
			$nRecordset->Execute($sqlquery);
			
			$sessionManager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");
			$sessionManager->set("isLogin",1);
			$sessionManager->set("travelerID",$travelerID);
			$sessionManager->set("login",crypt($travelerID,CRYPT_BLOWFISH));
			$sessionManager->set("domain", $_SERVER["HTTP_HOST"]);
			

			//$session = $this->file_factory->invokeStaticClass("Session","getInstance",array($travelerID));
			$session = session::getInstance($travelerID);
			$session->startSession();

			if( "" != $this->login_helper->getReferer() ){
				$_SESSION['popup_shoutout'] = TRUE;
				ob_end_clean();
		 		header("location: " . str_replace(array('^','\\'),array('#',''),$this->login_helper->getReferer()));
				exit;
			}
			ob_end_clean();
			header("location: passport.php");
			exit;
				 		
		}
		
		function endSession( ){
			
			$sessionManager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");

			$session = $this->file_factory->invokeStaticClass("Session","getInstance",array($sessionManager->get("travelerID")));
			$session->killSession();

				
			session_unset();
			session_destroy();
			$_SESSION = array();	
		
			if( isset($_COOKIE['login']) )
				unset( $_COOKIE['login']);
			
		}
	 	
	 	function viewLoginPage($loginData = array()){
		
			//added code fragment to redirect to dashboard if user is already logged in
			$session_manager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");
			if( $session_manager->getLogin() ){
				ob_end_clean();
				header("location: /travelerdashboard.php");
				exit;
			}
		
	 		$loginData['login_page'] = true;
	 		$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('layoutID', 'page_default');
			Template::setMainTemplateVar('title', "Students and Advisor Login - GoAbroad Network");
			Template::setMainTemplateVar('metaDescription',"Login area for GoAbroad Network student travelers and advisors");
			Template::setMainTemplate   ( $this->file_factory->getTemplate('LayoutMain') );   
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			Template::includeDependentCss('/css/Thickbox.css');
			Template::includeDependentJs('/js/thickbox.js');
			
			$fcd = $this->file_factory->getClass('LoginFcd',$this->readyLoginForm($loginData), false);
			$obj_view->setContents( array('contents' => $fcd->retrieveLoginPage()) );
			$obj_view->render();
	 	}
	 	
	 	function readyLoginForm($loginData = array()){
		
	 		$content = array(
				"username"				=>	isset($loginData["username"]) ? trim($loginData["username"]) : "",
				"password"				=>	isset($loginData["password"]) ? trim($loginData["password"]) : "",
				"loginType"				=>	isset($loginData["loginType"]) ? $loginData["loginType"] : constants::TRAVELER,
				"referer"				=>	$this->login_helper->getReferer(),
				"msg"					=>	isset($loginData["msg"]) ? trim($loginData["msg"]) : "",				
				"isRegister"			=>	$this->login_helper->isNewlyRegistered(),
				"action"				=>	'/login.php',
				"failed"				=>	isset($loginData["failed"]) ? $loginData["failed"] : false,
				"siteName"				=>	"GoAbroad",
				'displayRegisterLink'	=> 	true,
				'login_page'			=> 	$loginData['login_page']
			);
	 		
	 		return $content;
	 	}
	 	
	 	function viewLoginForm($loginData = array()){
	 		$loginData['login_page'] = false;
	 		$loginData = $this->readyLoginForm($loginData);
			$fcd = $this->file_factory->getClass('LoginFcd', $loginData, false);
			return $fcd->retrieveLoginForm();
			
	 	}
	 	
	 	static function purgeExpiredSession(){
	 		
	 		require_once 'Class.Connection.php';
	 		require_once 'Class.Recordset.php';
	 		
	 		// 86400 = 1 day
	 		$conn = new Connection();
	 		$rs = new Recordset($conn);
	 		$rs->Execute('DELETE FROM `tblSessions` WHERE 86400 < ABS(sessionTime - loginTime)');
	 			
	 	}
		
		/** 
		 * @nash
		 * @purpose - to log the login action :)
		 **/ 
		private function logLogin(){
		//	echo "here"; exit;
			$loginData = $this->login_helper->readLoginData();
			$status = $this->validateEmailPassword();
			
			$params = array('email' => $loginData['email'],
							'password' => $loginData['password'],
							'remote_address' => $_SERVER['REMOTE_ADDR'], 
							'status' => $status['loginStatus']);
			
			LoginLogger::getInstance()->log($params);
		}
	 	
	 }
?>