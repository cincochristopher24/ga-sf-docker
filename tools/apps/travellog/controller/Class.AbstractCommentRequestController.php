<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 26 November 2007
	 * Class.AbstractCommentRequestController.php
	 */

	require_once 'travellog/controller/Class.IController.php';
	require_once 'travellog/factory/Class.FileFactory.php';

	class AbstractCommentRequestController{

		function __construct(){
			$this->file_factory = FileFactory::getInstance();
	 		$this->file_factory->registerClass('CommentRequestController', array('path'=>'travellog/service/comments/'));

			//require_once 'travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php';
		}

		function performAction(){
			$this->file_factory->invokeStaticClass('CommentRequestController', 'getRequest', array($_REQUEST));
		}
	}
?>