<?php

	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/controller/Class.IController.php';
	require_once 'Class.Template.php';
	require_once("travellog/UIComponent/Collection/interface/CollectionConstants.php");
    
	// Added by: Adelbert
	require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');

	abstract class AbstractCollectionController implements iController, CollectionConstants{
		
		protected $mSession		=	NULL;
		protected $file_factory	=	NULL;
		protected $mTemplate	=	NULL;
		protected $mSubNavigation	=	NULL;
		protected $mContextInfo	=	NULL;
		
		protected $mType		=	NULL;
		protected $mID			=	NULL;
		protected $mContext		=	NULL;
		protected $mGenID		=	NULL;
		
		protected $mLoggedID	=	0;
		protected $mUserLevel	=	0; //0=not logged, 1=owner, 2=staff 3=superstaff, 4=admin
		
		protected $mTraveler	=	NULL;
		protected $mGroup 		= 	NULL;
		
		protected $mParams		=	array();
		
		function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('SessionManager');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('PhotoAlbum');
			$this->file_factory->registerClass('Photo');
			$this->file_factory->registerClass('GroupFactory');
			
			$this->file_factory->registerClass('RequestData',				array('path' => 'travellog/UIComponent/Collection/model/'));
			$this->file_factory->registerClass('ViewCollection',			array('path' => 'travellog/UIComponent/Collection/views/'));
			$this->file_factory->registerClass('ViewCollectionContent',		array('path' => 'travellog/UIComponent/Collection/views/'));
			$this->file_factory->registerClass('ViewImageUploader',			array('path' => 'travellog/UIComponent/Collection/views/'));
			$this->file_factory->registerClass('ViewGroupMembers',			array('path' => 'travellog/UIComponent/Collection/views/'));
			$this->file_factory->registerClass('PhotoAlbumCreator',			array('path' => 'travellog/UIComponent/Collection/controller/'));
			$this->file_factory->registerClass('PhotoCreator',				array('path' => 'travellog/UIComponent/Collection/controller/'));
			
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('ProfileCompFactory',	array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerTemplate('LayoutMain');

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->mSession = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			$this->mLoggedID = $this->mSession->get('travelerID');
		}
		
		function performAction(){
			
			$this->mParams = array(	"type"		=>	NULL,
									"ID"		=>	NULL,
									"context"	=>	NULL,
									"genID"		=>	NULL,
									"action"	=>	self::VIEW_COLLECTION	);
			$this->mParams = array_merge($this->mParams,$this->extractParameters());
			
			$this->mType = $this->mParams["type"];
			$this->mID = $this->mParams["ID"];
			$this->mGenID = $this->mParams["genID"];
			$this->mContext = $this->mParams["context"];
			
			switch($this->mParams["action"]){
				case self::CREATE_ALBUM	:	$this->_createAlbum();
											break;
				case self::LOAD_UPLOADER:	$this->_loadUploader();
											break;
				case self::EDIT_TITLE	:	$this->_editTitle();
											break;
				case self::VIEW_MEMBERS	:	$this->_viewMembers();
											break;
				case self::FETCH_MEMBERS:	$this->_fetchMembers();
											break;
				case self::UPLOAD		:	$this->_upload();
											break;
				case self::VIEW_MEMBER_PHOTOS	:	$this->_viewMemberPhotos();
													break;
				case self::MANAGE_PHOTOS	:	$this->_managePhotos();
												break;
				case self::SAVE_CHANGES	:	$this->_saveChanges();
											break;
				case self::GRAB_PHOTO	:	$this->_grabPhoto();
											break;
				case self::DELETE_PHOTO	:	$this->_deletePhoto();
											break;
				case self::REMOVE_GRABBED	:	$this->_removeGrabbed();
												break;
				case self::REMOVE_MEMBER_PHOTO	:	$this->_removeMemberPhoto();
													break;
				case self::EDIT_CAPTION	:	$this->_editCaption();
											break;
				case self::ROTATE		:	$this->_rotate();
											break;
				case self::SET_PRIMARY	:	$this->_setAsPrimary();
											break;
				case self::SET_JOURNAL_PRIMARY	: 	$this->_setAsJournalPrimary();
													break;
				case self::LOAD_CROPPER	:	$this->_loadCropper();
											break;
				case self::SAVE_THUMBNAIL:	$this->_saveThumbnail();
											break;
				case self::DELETE_ALBUM	:	$this->_deleteAlbum();
											break;
				case self::LOAD_POPUP_GALLERY	:	$this->_loadPopupGallery();
													break;
				case self::LOAD_REARRANGE_VIEW	:	$this->_loadRearrangeView();
													break;
				case self::SAVE_PHOTO_ORDER		:	$this->_savePhotoOrder();
													break;
				case self::BATCH_DELETE	:	$this->_batchDelete();
											break;
				case self::GET_UPLOAD_ERRORS	:	$this->_getUploadErrors();
												break;
				case self::EDIT_FEATURED_ALBUMS	:	$this->_editFeaturedAlbums();
													break;
				case self::SAVE_FEATURED_ALBUMS	:	$this->_saveFeaturedAlbums();
													break;
				case self::UPDATE_MANAGE_FEATURED_ALBUMS_NOTICE	:	$this->_updateManageFeaturedAlbumsNotice();
																	break;
				case self::FEATURE_ALBUM		:	$this->_featureAlbum();
													break;
				case self::CREATE_COBRAND_HEADER	:	$this->_createCobrandCustomHeader();
														break;
				case self::FIXGROUPLOGOTHUMBNAIL	:	$this->_fixGroupLogoThumbnail();
													break;
				case self::LOAD_RECENT_UPLOADS	:	$this->_loadRecentUploads();
													break;
				default					:	$this->_renderCollection();
											break;
			}
			
		}
		
		function _defineCommonAttributes(){
			$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->mTemplate = $this->file_factory->getClass('Template');
			$this->mTemplate->set_path('travellog/UIComponent/Collection/views/');

			$this->_checkUserLevel();
			
			Template::setMainTemplateVar('page_location', self::TRAVELER_TYPE == $this->mType ? ($this->mLoggedID == $this->mID ? 'My Passport' : 'Travelers') : 'Groups / Clubs');
			
			$this->mSubNavigation = $this->file_factory->getClass('SubNavigation');
			if( self::GROUP_TYPE == $this->mType && 0 < $this->mID ){
				$this->mSubNavigation->setContext('GROUP');
				$this->mSubNavigation->setContextID($this->mID);
				$this->mSubNavigation->setLinkToHighlight('PHOTOS');
				//if ($this->mGroup->getDiscriminator() == GROUP::ADMIN){
					$this->mSubNavigation->setGroupType('ADMIN_GROUP');
				//}else if($this->mGroup->getDiscriminator() == GROUP::FUN){
				//	$this->mSubNavigation->setGroupType('FUN_GROUP');
				//}
				
				$mProfileFactory = $this->file_factory->invokeStaticClass('ProfileCompFactory', 'getInstance', array());
				$mProfileComp	= $mProfileFactory->create( ProfileCompFactory::$GROUP_CONTEXT );

				if(2 == $this->mGroup->getDiscriminator() && (1 < $this->mGroup->getParentID()))
					$mProfileComp->init($this->mGroup->getParentID());
				else
					$mProfileComp->init($this->mID);
			}else if( self::TRAVELER_TYPE == $this->mType && 0 < $this->mID ){
				$this->mSubNavigation->setContext('TRAVELER');
				$this->mSubNavigation->setContextID($this->mID);
				$this->mSubNavigation->setLinkToHighlight('MY_PHOTOS');
				
				$mProfileFactory = $this->file_factory->invokeStaticClass('ProfileCompFactory', 'getInstance', array());
				$mProfileComp	= $mProfileFactory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
				$mProfileComp->init($this->mID);
			}else{
				header("location: index.php");
				exit;
			}

			$this->mContextInfo = $mProfileComp->get_view();
			
		}
		
		function _checkUserLevel(){
			// check if user is currently logged in
			switch($this->mType){
				case self::TRAVELER_TYPE	:	$this->mUserLevel = ($this->mLoggedID==$this->mID)? self::OWNER : 0;
												$this->mTraveler = $this->file_factory->getClass('Traveler', array($this->mID));
												break;
				case self::GROUP_TYPE		:	$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($this->mID));
												$this->mGroup = $group[0];
												if( 0 < $this->mLoggedID && !$this->mGroup->isSuspended() && $this->mGroup->getDiscriminator() == GROUP::ADMIN){
													if( $this->mGroup->isSubGroup() ){
														$parent = $this->mGroup->getParent();
														if ($parent->getAdministratorID() == $this->mLoggedID){
															$this->mUserLevel = self::ADMIN;
														}else if ($parent->isSuperStaff($this->mLoggedID)){
															$this->mUserLevel = self::SUPERSTAFF;
														}else if( $this->mGroup->isStaff($this->mLoggedID)){
															$this->mUserLevel = self::STAFF;
														}
													}else{
														if ($this->mGroup->getAdministratorID() == $this->mLoggedID){
															$this->mUserLevel = self::ADMIN;
														}else if ($this->mGroup->isSuperStaff($this->mLoggedID)){
															$this->mUserLevel = self::SUPERSTAFF;
														}else if($this->mGroup->isStaff($this->mLoggedID)){
															$this->mUserLevel = self::STAFF;
														}
													}
												}
												break;
			}
			
			if ( self::VIEWER < $this->mUserLevel ) {
				// start GA Logger
				require_once 'gaLogs/Class.GaLogger.php';
				try{
					GaLogger::create()->start($this->mLoggedID, 'TRAVELER');
				}
				catch(exception $e){
				}
			}
		}
		
		function _prepareDependencies(){
			Template::setMainTemplateVar('layoutID', 'page_photos');
			if( 0 < $this->mUserLevel ){
				Template::includeDependentJs("/js/jQueryUI.min-1.1.js");
				Template::includeDependentJs("/js/ajaxfileupload.js");
				Template::includeDependentCss("/min/f=css/Thickbox.css");
				Template::includeDependentJs("/js/thickbox.js");
				
				Template::includeDependentCss('/js/cropper/cropper.css');
				Template::includeDependentJs("/min/f=js/prototype.js");
				Template::includeDependentJs("js/cropper/lib/builder.js");
				Template::includeDependentJs("js/cropper/lib/dragdrop.js");
				Template::includeDependentJs("js/cropper/cropper.js");

			}
			Template::includeDependentJs("/js/contentSlider.js");	
			Template::includeDependentJs("/js/collectionPopup-1.4.js");
			Template::includeDependentJs("/js/collectionPaginatedList.js");
			Template::includeDependentJs("/js/collection-1.6.js");
			
		}
		
		function _renderCollection(){
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			//$mCollection->setCacheKeySource($this->keySource);
			/** this part sets up static variables in RequestData
				the variables are used throughout the components used here */
			$mCollection->setType($this->mType);
			$mCollection->setID($this->mID);
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			$mCollection->setUserLevel($this->mUserLevel);
			$mCollection->setLoggedID($this->mLoggedID);
			$mCollection->setGroup($this->mGroup);
			$mCollection->setTraveler($this->mTraveler);
			$mCollection->setAction($this->mParams["action"]);
			/** RequestData has been set */
			$mCollection->retrieve();
			
			$mContent = $this->file_factory->getClass('ViewCollectionContent');
			//$mContent->setCacheKeySource($this->keySource);
			
			$this->mTemplate->set("collection",		$mCollection);
			$this->mTemplate->set("content",		$mContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->set("type",			$this->mType);
			$this->mTemplate->set("ID",				$this->mID);
			$this->mTemplate->set("context",		$this->mContext);
			$this->mTemplate->set("genID",			$this->mGenID);
			$this->mTemplate->set("isPrivileged",	(self::VIEWER < $this->mUserLevel)? 1 : 0);
			$this->mTemplate->set("isAdmin",		$this->mUserLevel == self::ADMIN ? 1 : 0);
			$this->mTemplate->out("tpl.Collection.php");
		}
				
		protected function _invalidateCacheEntry(){
			require_once('Cache/ganetCacheProvider.php');
			$cache	=	ganetCacheProvider::instance()->getCache();
			if ($cache != null) {
				//delete cache for owner/admin view
				$key = self::TRAVELER_TYPE == $this->mType ? "Photo_Collection_Traveler_Owner_".$this->mID : "Photo_Collection_Group_Admin_".$this->mID;
				$cache->delete($key);
				
				//delete cache for public view
				$key = self::TRAVELER_TYPE == $this->mType ? "Photo_Collection_Traveler_Public_".$this->mID : "Photo_Collection_Group_Public_".$this->mID;
				$cache->delete($key);
			}
		}
		
		
		function _createAlbum(){			
			$this->_checkUserLevel();
			
			if( self::VIEWER == $this->mUserLevel ){
				header("location: index.php");
				exit;
			}
			
			$mCreator = $this->file_factory->getClass('PhotoAlbumCreator');
			$mCreator->setType($this->mType);
			$mCreator->setID($this->mID);
			$mCreator->setLoggedID($this->mLoggedID);
			$mCreator->setAlbumName($this->mParams["txtAlbumName"]);
			$mCreator->create();
		}
		
		function _loadUploader(){
			
			$this->_defineCommonAttributes();
			
			if( self::VIEWER == $this->mUserLevel ){
				header("location: index.php");
				exit;
			}
			
			$this->_prepareDependencies();
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			/** this part sets up static variables in RequestData
				the variables are used throughout the components used here */
			$mCollection->setType($this->mType);
			$mCollection->setID($this->mID);
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			$mCollection->setUserLevel($this->mUserLevel);
			$mCollection->setLoggedID($this->mLoggedID);
			$mCollection->setGroup($this->mGroup);
			$mCollection->setTraveler($this->mTraveler);
			$mCollection->setAction($this->mParams["action"]);
			/** RequestData has been set */
			$mCollection->retrieve();
			
			$mUploader = $this->file_factory->getClass('ViewImageUploader');
			//$mContent = $this->file_factory->getClass('ViewCollectionContent');
			
			$this->mTemplate->set("collection",		$mCollection);
			$this->mTemplate->set("content",		$mUploader);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->set("type",			$this->mType);
			$this->mTemplate->set("ID",				$this->mID);
			$this->mTemplate->set("context",		$this->mContext);
			$this->mTemplate->set("genID",			$this->mGenID);
			$this->mTemplate->set("isPrivileged",	(self::VIEWER < $this->mUserLevel)? 1 : 0);
			$this->mTemplate->set("isAdmin",		$this->mUserLevel == self::ADMIN ? 1 : 0);
			$this->mTemplate->out("tpl.Collection.php");
		}
		
		function _editTitle(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			if( !isset($this->mParams["txtAlbumName"]) || !isset($this->mParams["albumID"]) ){
				echo "ERROR";
				exit;
			}
			
			$mAlbum = $this->file_factory->getClass('PhotoAlbum', array($this->mParams["albumID"]));
			
			if( $mAlbum instanceof PhotoAlbum ){
				if( $mAlbum->getCreator() != $this->mLoggedID && $mAlbum->getGroupID() != $this->mID ){
					echo "RESTRICTED";
					exit;
				}
				$mAlbum->setTitle($this->mParams["txtAlbumName"]);
				$mAlbum->Update();
				$this->_invalidateCacheEntry();
			}else{
				echo "ERROR";
				exit;
			}

		}
		
		function _deleteAlbum(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			if( !isset($this->mParams["albumID"]) ){
				echo "ERROR";
				exit;
			}
			$mAlbum = $this->file_factory->getClass('PhotoAlbum', array($this->mParams["albumID"]));
			
			if( $mAlbum instanceof PhotoAlbum ){
				if( $mAlbum->getCreator() != $this->mLoggedID && $mAlbum->getGroupID() != $this->mID ){
					echo "RESTRICTED";
					exit;
				}
				$mAlbum->Delete();
				$this->_invalidateCacheEntry();
				ob_clean();	
			}else{
				echo "ERROR";
				exit;
			}
		}
		
		function _viewMembers(){
			$this->_defineCommonAttributes();
			
			if( self::VIEWER == $this->mUserLevel ){
				header("location: index.php");
				exit;
			}
			
			$this->_prepareDependencies();
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			/** this part sets up static variables in RequestData
				the variables are used throughout the components used here */
			$mCollection->setType($this->mType);
			$mCollection->setID($this->mID);
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			$mCollection->setUserLevel($this->mUserLevel);
			$mCollection->setLoggedID($this->mLoggedID);
			$mCollection->setGroup($this->mGroup);
			$mCollection->setTraveler($this->mTraveler);
			$mCollection->setAction($this->mParams["action"]);
			/** RequestData has been set */
			$mCollection->retrieve();
			
			$mContent = $this->file_factory->getClass('ViewGroupMembers');
			
			$this->mTemplate->set("collection",		$mCollection);
			$this->mTemplate->set("content",		$mContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->set("type",			$this->mType);
			$this->mTemplate->set("ID",				$this->mID);
			$this->mTemplate->set("context",		$this->mContext);
			$this->mTemplate->set("genID",			$this->mGenID);
			$this->mTemplate->set("isPrivileged",	(self::VIEWER < $this->mUserLevel)? 1 : 0);
			$this->mTemplate->set("isAdmin",		$this->mUserLevel == self::ADMIN ? 1 : 0);
			$this->mTemplate->out("tpl.Collection.php");
		}
		
		function _fetchMembers(){
			$this->_checkUserLevel();
			
			$mContent = $this->file_factory->getClass('ViewGroupMembers');
			$mContent->setPage($this->mParams["page"]);
			$mContent->setGroup($this->mGroup);
			$mContent->renderFetchedMembers();
		}
		
		function _viewMemberPhotos(){
			$this->_checkUserLevel();
			
			$mContent = $this->file_factory->getClass('ViewGroupMembers');
			if( !isset($this->mParams["tID"]) ){
				$this->mParams["tID"] = 0;
			}
			$member = $this->file_factory->getClass('Traveler', array($this->mParams["tID"]));
			$mContent->setContext($this->mContext);
			$mContent->setGenID($this->mGenID);
			$mContent->setGroup($this->mGroup);
			$mContent->setMember($member);
			$mContent->renderMemberPhotos();
		}
		
		function _upload(){//process uploaded files
			$this->_checkUserLevel();

			$server = $_SERVER["SERVER_NAME"];
			$tolerate = FALSE;
			if( 0 < strpos($server,".local") || 0 === strpos($server,"dev.goabroad.net") || 0 < strpos($server,"dev.goabroad.net") ){//is local or dev
				$tolerate = TRUE;
			}
			ob_clean();
			if( self::VIEWER == $this->mUserLevel && !$tolerate ){
				echo "RESTRICTED";
				exit;
			}
			
			$mCreator = $this->file_factory->getClass('PhotoCreator');
			$mCreator->setType($this->mType);
			$mCreator->setID($this->mID);
			$mCreator->setContext($this->mContext);
			$mCreator->setGenID($this->mGenID);
			$mCreator->setUploadMode($this->mParams["mode"]);
			$mCreator->setFiles($_FILES);
			$mCreator->setData($this->mParams);
			$mCreator->processFiles();
			ob_clean();
			//if( self::CLASSIC == $this->mParams["mode"] ){
			//	header("location: /collection.php?type=$this->mType&ID=$this->mID&context=$this->mContext&genID=$this->mGenID&action=".self::MANAGE_PHOTOS);
			//}
			
		}
		
		function _getUploadErrors(){
			$this->_checkUserLevel();
			ob_clean();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			
			$success = implode(",",$_SESSION["ValidFiles"]);
			$failed = "";
			if( isset($_SESSION["UploadError"]) && $_SESSION["UploadError"] ){
				$failed = implode(",",$_SESSION["LargeFiles"]);
				unset($_SESSION["LargeFiles"]);
			}
			unset($_SESSION["ValidFiles"]);
			unset($_SESSION["UploadError"]);
			
			if( "" == $failed ){//no failed uploads
				exit;
			}
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			/** this part sets up static variables in RequestData
				the variables are used throughout the components used here */
			$mCollection->setType($this->mType);
			$mCollection->setID($this->mID);
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			$mCollection->setUserLevel($this->mUserLevel);
			$mCollection->setLoggedID($this->mLoggedID);
			$mCollection->setGroup($this->mGroup);
			$mCollection->setTraveler($this->mTraveler);
			$mCollection->setAction($this->mParams["action"]);
			/** RequestData has been set */
			$mCollection->retrieve();
			
			$photoCount = $mCollection->getPhotoContext()->getCOUNTPhotos();
			
			echo $photoCount."?".$failed;
		}
		
		function _managePhotos(){//after uploading files
			
			$this->_defineCommonAttributes();
			
			if( self::VIEWER == $this->mUserLevel ){
				header("location: index.php");
				exit;
			}
			
			$this->_prepareDependencies();
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			/** this part sets up static variables in RequestData
				the variables are used throughout the components used here */
			$mCollection->setType($this->mType);
			$mCollection->setID($this->mID);
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			$mCollection->setUserLevel($this->mUserLevel);
			$mCollection->setLoggedID($this->mLoggedID);
			$mCollection->setGroup($this->mGroup);
			$mCollection->setTraveler($this->mTraveler);
			$mCollection->setAction($this->mParams["action"]);
			/** RequestData has been set */
			$mCollection->retrieve();
			
			
			$mContent = $this->file_factory->getClass('ViewCollectionContent');
			
			$this->mTemplate->set("collection",		$mCollection);
			$this->mTemplate->set("content",		$mContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->set("type",			$this->mType);
			$this->mTemplate->set("ID",				$this->mID);
			$this->mTemplate->set("context",		$this->mContext);
			$this->mTemplate->set("genID",			$this->mGenID);
			$this->mTemplate->set("isPrivileged",	(self::VIEWER < $this->mUserLevel)? 1 : 0);
			$this->mTemplate->set("isAdmin",		$this->mUserLevel == self::ADMIN ? 1 : 0);
			$this->mTemplate->out("tpl.Collection.php");
		}
		
		function _saveChanges(){
			$this->_checkUserLevel();
			
			if( self::VIEWER == $this->mUserLevel ){
				header("location: index.php");
				exit;
			}
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			
			$photoContext = $mCollection->getPhotoContext();
			$photos = $photoContext->getPhotos();
			foreach($photos as $photo){
				if( isset($this->mParams["txtCaption_".$photo->getPhotoID()]) 
					&& $photo->getCaption() != trim(strip_tags($this->mParams["txtCaption_".$photo->getPhotoID()])) ){
					$photo->setCaption(trim(strip_tags($this->mParams["txtCaption_".$photo->getPhotoID()])));
					$photo->updateCaption();
				}
				if( isset($this->mParams["radPrimary"]) && $photo->getPhotoID() == $this->mParams["radPrimary"] ){
					if( self::ALBUM_CONTEXT == $this->mContext ){
						require_once("travellog/model/Class.PhotoAlbum.php");
						$album = new PhotoAlbum($this->mGenID);
						$photo->setContext($album);
						$photo->setPhotoTypeID(7);
					}
					$photo->setAsPrimaryPhoto();
				}
			}
			$this->_invalidateCacheEntry();
			header("location: /collection.php?type=$this->mType&ID=$this->mID&context=$this->mContext&genID=$this->mGenID");
			
		}
		
		function _grabPhoto(){
			$this->_checkUserLevel();
			
			if( self::VIEWER == $this->mUserLevel ){
				echo "ERROR";
				exit;
			}
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			
			$mPhotoContext = $mCollection->getPhotoContext()->getPHOTOContext();
			$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["pID"]));
			
			if( !($mPhoto instanceof Photo) ){
				echo "ERROR";
				exit;
			}
			
			$mPhotoContext->addPhoto($mPhoto,$grabbed=TRUE);
			if( is_null($mPhotoContext->getPrimary()) || !$mPhotoContext->getPrimary() ){
				$mPhotoContext->setAsPhotoAlbumPrimary($mPhoto->getPhotoID());
			}
			
			$this->_invalidateCacheEntry();
			
			$mTpl = $this->file_factory->getClass('Template');
			$mTpl->set("title",$mPhotoContext->getTitle());
			$mTpl->set("mode","ADD");
			$mTpl->set("message","Successfully added");
			$mTpl->out("travellog/UIComponent/Collection/views/tpl.ViewGrabNotice.php");
		}
		
		function _deletePhoto(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			if( self::JOURNAL_CONTEXT == $this->mContext ){
				if( isset($this->mParams["tlogID"]) && isset($this->mParams["photoID"]) ){
					require_once("travellog/model/Class.TravelLog.php");
					require_once("travellog/model/Class.Photo.php");
					$context_obj = new TravelLog($this->mParams["tlogID"]);
					$photo_obj = new Photo($context_obj,$this->mParams["photoID"]);
					$photo_obj->Delete();
					$photo_obj->DeleteBackup($this->mParams["tlogID"]);
				}
			}elseif( isset($this->mParams["photoID"]) ){
				require_once("travellog/model/Class.Photo.php");
				switch($this->mContext){
					case self::TRAVELLOG_CONTEXT	:	require_once("travellog/model/Class.TravelLog.php");
														$context_obj = new TravelLog($this->mGenID);
														$photo_obj = new Photo($context_obj,$this->mParams["photoID"]);
														$photo_obj->Delete();
														$photo_obj->DeleteBackup($this->mGenID);
														break;
					case self::PROFILE_CONTEXT		: 	if( self::TRAVELER_TYPE == $this->mType ){
															require_once("travellog/model/Class.Traveler.php");
															$traveler = new Traveler($this->mID);
															$context_obj = $traveler->getTravelerProfile();
															$photo_obj = new Photo($context_obj,$this->mParams["photoID"]);
															$photo_obj->Delete();
														}
														break;
					case self::ALBUM_CONTEXT		:	require_once("travellog/model/Class.PhotoAlbum.php");
														$context_obj = new PhotoAlbum($this->mGenID);
														$photo_obj = new Photo($context_obj,$this->mParams["photoID"]);
														$photo_obj->Delete();
														break;
					case self::TRAVALBUM_CONTEXT	:	require_once("travellog/model/Class.PhotoAlbum.php");
														$context_obj = new PhotoAlbum($this->mGenID);
														$photo_obj = new Photo($context_obj,$this->mParams["photoID"]);
														$photo_obj->Delete();
														break;
					case self::ARTICLE_CONTEXT		:	require_once("travellog/model/Class.Article.php");
														$context_obj = new Article($this->mGenID);
														$photo_obj = new Photo($context_obj,$this->mParams["photoID"]);
														$photo_obj->Delete();
														break;
				}
			}

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: aug. 10, 2009
			
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($_SESSION['travelerID'],4);

			$this->_invalidateCacheEntry();
		}
		
		function _batchDelete(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			
			$context_obj = NULL;
			if( self::JOURNAL_CONTEXT == $this->mContext ){
				if( isset($this->mParams["tlogID"]) ){
					require_once("travellog/model/Class.TravelLog.php");
					$context_obj = new TravelLog($this->mParams["tlogID"]);
				}
			}else{
				switch($this->mContext){
					case self::TRAVELLOG_CONTEXT	:	require_once("travellog/model/Class.TravelLog.php");
														$context_obj = new TravelLog($this->mGenID);
														break;
					case self::PROFILE_CONTEXT		: 	if( self::TRAVELER_TYPE == $this->mType ){
															require_once("travellog/model/Class.Traveler.php");
															$traveler = new Traveler($this->mID);
															$context_obj = $traveler->getTravelerProfile();
														}
														break;
					case self::ALBUM_CONTEXT		:	require_once("travellog/model/Class.PhotoAlbum.php");
														$context_obj = new PhotoAlbum($this->mGenID);
														break;
					case self::TRAVALBUM_CONTEXT	:	require_once("travellog/model/Class.PhotoAlbum.php");
														$context_obj = new PhotoAlbum($this->mGenID);
														break;
					case self::ARTICLE_CONTEXT		:	require_once("travellog/model/Class.Article.php");
														$context_obj = new Article($this->mGenID);
														break;
				}
			}
			
			if( !is_null($context_obj) && is_array($this->mParams["pID"]) ){
				require_once("travellog/model/Class.Photo.php");
				foreach($this->mParams["pID"] as $pID){
					//check if photo is only a grabbed photo
					if( self::ALBUM_CONTEXT == $this->mContext && $context_obj->isGrabbedPhoto($pID) ){
						$context_obj->removeGrabbedPhoto($pID);
					}else{
						$photo_obj = new Photo($context_obj,$pID);
						$photo_obj->Delete();
						if( self::JOURNAL_CONTEXT == $this->mContext ){
							$photo_obj->DeleteBackup($this->mParams["tlogID"]);
						}elseif( self::TRAVELLOG_CONTEXT == $this->mContext ){
							$photo_obj->DeleteBackup($this->mGenID);
						}
					}
				}
			}
			
			$this->_invalidateCacheEntry();
			
		}
		
		function _removeGrabbed(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "ERROR";
				exit;
			}
			
			if( isset($this->mParams["photoID"]) &&  self::ALBUM_CONTEXT == $this->mContext ){
				require_once("travellog/model/Class.PhotoAlbum.php");
				$context_obj = new PhotoAlbum($this->mGenID);
				$context_obj->removeGrabbedPhoto($this->mParams["photoID"]);
				
				$this->_invalidateCacheEntry();
			}
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			$mCollection->setType($this->mType);
			$mCollection->setID($this->mID);
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			$mCollection->setUserLevel($this->mUserLevel);
			$mCollection->setLoggedID($this->mLoggedID);
			$mCollection->setGroup($this->mGroup);
			$mCollection->setTraveler($this->mTraveler);
			$mCollection->setAction($this->mParams["action"]);
			$mCollection->retrieve();
			
			$mContent = $this->file_factory->getClass('ViewCollectionContent');
			$mContent->renderContents();
		}
		
		function _removeMemberPhoto(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "ERROR";
				exit;
			}
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);
			
			$mPhotoContext = $mCollection->getPhotoContext()->getPHOTOContext();
			$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["pID"]));
			
			if( !($mPhoto instanceof Photo) ){
				echo "ERROR";
				exit;
			}
			
			$mPhotoContext->removeGrabbedPhoto($mPhoto->getPhotoID());
			$this->_invalidateCacheEntry();
			
			$mTpl = $this->file_factory->getClass('Template');
			$mTpl->set("title",$mPhotoContext->getTitle());
			$mTpl->set("mode","REMOVE");
			$mTpl->set("message","Successfully removed");
			$mTpl->out("travellog/UIComponent/Collection/views/tpl.ViewGrabNotice.php");
		}
		
		function _editCaption(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "ERROR";
				exit;
			}
			if( !isset($this->mParams["caption"]) ){
				echo "ERROR";
				exit;
			}
			$mCollection = $this->file_factory->getClass('ViewCollection');
			if( self::JOURNAL_CONTEXT == $this->mContext ){
				if( !isset($this->mParams["tlogID"]) ){
					echo "ERROR";
					exit;
				}
				$mCollection->setContext(self::TRAVELLOG_CONTEXT);
				$mCollection->setGenID($this->mParams["tlogID"]);
			}else{
				$mCollection->setContext($this->mContext);
				$mCollection->setGenID($this->mGenID);
			}
			
			$mPhotoContext = $mCollection->getPhotoContext()->getPHOTOContext();
			$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["pID"]));
			
			if( !($mPhoto instanceof Photo) ){
				echo "ERROR";
				exit;
			}
			
			$mPhoto->setCaption($this->mParams["caption"]);
			$mPhoto->updateCaption();
			$this->_invalidateCacheEntry();
		}
		
		function _rotate(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "ERROR";
				exit;
			}
			if( !isset($this->mParams["pID"]) || !isset($this->mParams["angle"]) ){
				echo "ERROR";
				exit;
			}
			if( 0 == $this->mParams["angle"] ){
				exit;
			}
			$mCollection = $this->file_factory->getClass('ViewCollection');
			if( self::JOURNAL_CONTEXT == $this->mContext ){
				if( !isset($this->mParams["tlogID"]) ){
					echo "ERROR";
					exit;
				}
				$mCollection->setContext(self::TRAVELLOG_CONTEXT);
				$mCollection->setGenID($this->mParams["tlogID"]);
			}else{
				$mCollection->setContext($this->mContext);
				$mCollection->setGenID($this->mGenID);
			}
			
			$mPhotoContext = $mCollection->getPhotoContext()->getPHOTOContext();
			$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["pID"]));
			
			if( !($mPhoto instanceof Photo) ){
				echo "ERROR";
				exit;
			}
			
			require_once("travellog/model/Class.PathManager.php");
			require_once("travellog/UIComponent/Collection/model/Class.UploadedPhoto.php");
			
			$pm = new PathManager($mPhotoContext);
			$uploaded = new UploadedPhoto();
			$uploaded->rotatePhoto($pm,$mPhotoContext,$mPhoto,$this->mParams["angle"]);
			$this->_invalidateCacheEntry();
		}
		
		function _setAsPrimary(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			if( !isset($this->mParams["pID"]) ){
				echo "ERROR";
				exit;
			}
			if( self::JOURNAL_CONTEXT == $this->mContext && !isset($this->mParams["tlogID"]) ){
				echo "ERROR";
				exit;
			}
			$mCollection = $this->file_factory->getClass('ViewCollection');
			if( self::JOURNAL_CONTEXT == $this->mContext ){
				$mCollection->setContext(self::TRAVELLOG_CONTEXT);
				$mCollection->setGenID($this->mParams["tlogID"]);
			}else{
				$mCollection->setContext($this->mContext);
				$mCollection->setGenID($this->mGenID);
			}

			$mPhotoContext = $mCollection->getPhotoContext()->getPHOTOContext();
			$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["pID"]));
			
			if( !($mPhoto instanceof Photo) ){
				echo "ERROR";
				exit;
			}
			
			require_once("travellog/model/Class.PhotoType.php");
			if ( self::ALBUM_CONTEXT == $this->mContext || self::TRAVALBUM_CONTEXT == $this->mContext ){
				//$mPhoto->setPhotoTypeID(PhotoType::$PHOTOALBUM);
				$mPhotoContext->setAsPhotoAlbumPrimary($mPhoto->getPhotoID());
			}else{
				$mPhoto->setAsPrimaryPhoto();
			}

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($_SESSION['travelerID'],4);

			$this->_invalidateCacheEntry();
		}
		
		function _setAsJournalPrimary(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			if( !isset($this->mParams["pID"]) ){
				echo "ERROR";
				exit;
			}
			if( self::JOURNAL_CONTEXT != $this->mContext && self::TRAVELLOG_CONTEXT != $this->mContext ){
				echo "ERROR";
				exit;
			}
			
			require_once("travellog/model/Class.Travel.php");
			require_once("travellog/model/Class.TravelLog.php");
			if( self::JOURNAL_CONTEXT == $this->mContext ){
				$travel = new Travel($this->mGenID);
			}else{
				$tlog = new TravelLog($this->mGenID);
				$travel = new Travel($tlog->getTravelID());
			}
			$travel->setPrimaryphoto($this->mParams["pID"]);
			$travel->Update();

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($_SESSION['travelerID'],4);

			$this->_invalidateCacheEntry();
		}
		
		function _loadCropper(){
			$this->_checkUserLevel();
			$mTpl = $this->file_factory->getClass('Template');
			if( self::VIEWER == $this->mUserLevel ){
				$mTpl->set("hasError",TRUE);
			}else{	
				if( !isset($this->mParams["pID"]) ){
					$mTpl->set("hasError",TRUE);
				}else{
					$mCollection = $this->file_factory->getClass('ViewCollection');
					$mCollection->setContext($this->mContext);
					$mCollection->setGenID($this->mGenID);

					$mPhotoContext = $mCollection->getPhotoContext()->getPHOTOContext();
					$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["pID"]));
					
					if( $mPhoto instanceof Photo ){
						
						$fullsize = $mPhoto->getPhotoLink('fullsize');	

						$imagetype = getimagesize($fullsize);		
						$image = FALSE;

						switch (substr($imagetype['mime'],6,4)) {
								case self::JPEG	:
									   	$image = imagecreatefromjpeg($fullsize); 
										break;
								case self::GIF	:					
									   	$image = imagecreatefromgif($fullsize); 	
										break;
								case self::PNG	:
										$image = imagecreatefrompng($fullsize); 						
									  	break;
								default	:
										$image = imagecreatefromjpeg($fullsize); 
										break;		
						}
						if( $image ){
							$fullWidth = imagesx($image);
							$fullHeight = imagesy($image);
							imagedestroy($image);
							
							$mTpl->set("hasError",FALSE);
							$mTpl->set("Fullsize",$fullsize);
							$mTpl->set("Thumbnail",$mPhoto->getPhotoLink('thumbnail'));
							$mTpl->set("fullWidth",$fullWidth);
							$mTpl->set("fullHeight",$fullHeight);
						}else{
							$mTpl->set("hasError",TRUE);
						}
					}else{
						$mTpl->set("hasError",TRUE);
					}
				}
			}
			$mTpl->out("travellog/UIComponent/Collection/views/tpl.ViewThumbnailCrop.php");
		}
		
		function _saveThumbnail(){
			$this->_checkUserLevel();

			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			
			$mCollection = $this->file_factory->getClass('ViewCollection');
			$mCollection->setContext($this->mContext);
			$mCollection->setGenID($this->mGenID);

			$mPhotoContext = $mCollection->getPhotoContext()->getPHOTOContext();
			$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["pID"]));
			
			require_once("travellog/model/Class.PathManager.php");
			require_once("travellog/UIComponent/Collection/model/Class.UploadedPhoto.php");
			
			$pm = new PathManager($mPhotoContext);
			$uploaded = new UploadedPhoto();
			$uploaded->setNewThumbnail($pm,$mPhotoContext,$mPhoto,$this->mParams["x"],$this->mParams["y"],$this->mParams["width"],$this->mParams["height"]);
			$this->_invalidateCacheEntry();
		}
		
		function _loadPopupGallery(){
			require_once("travellog/UIComponent/Collection/views/Class.ViewPopupGallery.php");
			
			$popup = new ViewPopupGallery();
			$popup->setContext($this->mContext);
			$popup->setGenID($this->mGenID);
			$mPhotoContext = $popup->getPhotoContext()->getPHOTOContext();
			$photos = $mPhotoContext->getPhotos();
			$popup->setPhotos($photos);
			if( isset($this->mParams["photoID"]) ){
				$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["photoID"]));
				if( $mPhoto instanceof $mPhoto ){
					$popup->setDefaultPhoto($mPhoto);
				}
			}else{
				$popup->setDefaultPhoto($photos[0]);
			}
			$popup->render();
		}
		
		function _loadRearrangeView(){
			$this->_checkUserLevel();
			$mTpl = $this->file_factory->getClass('Template');
			if( self::VIEWER == $this->mUserLevel ){
				$mTpl->set("hasError",TRUE);
			}elseif( self::JOURNAL_CONTEXT == $this->mContext && !isset($this->mParams["tlogID"]) ){
				$mTpl->set("hasError",TRUE);
			}else{	
				$mCollection = $this->file_factory->getClass('ViewCollection');
				if( self::JOURNAL_CONTEXT == $this->mContext ){
					$mCollection->setContext(self::TRAVELLOG_CONTEXT);
					$mCollection->setGenID($this->mParams["tlogID"]);
				}else{
					$mCollection->setContext($this->mContext);
					$mCollection->setGenID($this->mGenID);
				}

				$mPhotoContext = $mCollection->getPhotoContext();
				$mTpl->set("title",$mPhotoContext->getHeaderCaption());
				$mTpl->set("mode",$this->mParams["mode"]);
				$mTpl->set("photos",$mPhotoContext->getPhotos());
				$mTpl->set("hasError",FALSE);					
			}
			$mTpl->out("travellog/UIComponent/Collection/views/tpl.ViewRearrangePhotos.php");
		}
		
		function _savePhotoOrder(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			if( self::JOURNAL_CONTEXT == $this->mContext && !isset($this->mParams["tlogID"]) ){
				echo "ERROR";
				exit;
			}
			$mCollection = $this->file_factory->getClass('ViewCollection');
			if( self::JOURNAL_CONTEXT == $this->mContext ){
				$mCollection->setContext(self::TRAVELLOG_CONTEXT);
				$mCollection->setGenID($this->mParams["tlogID"]);
			}else{
				$mCollection->setContext($this->mContext);
				$mCollection->setGenID($this->mGenID);
			}
			$mPhotoContext = $mCollection->getPhotoContext();
			$mPhotoContext->getPHOTOContext()->reArrangePhoto($this->mParams["photoID"]);
			$this->_invalidateCacheEntry();
		}
		
		private function _editFeaturedAlbums(){
			$this->_checkUserLevel();
			$mTpl = $this->file_factory->getClass('Template');
			if( self::VIEWER == $this->mUserLevel ){
				$mTpl->set("hasError",TRUE);
			}else{
				$mTpl->set("albums",$this->mGroup->getPhotoAlbums());
				$mTpl->set("hasError",FALSE);					
			}
			$mTpl->out("travellog/UIComponent/Collection/views/tpl.ViewEditFeaturedAlbums.php");
		}
		
		private function _saveFeaturedAlbums(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			if( !isset($this->mParams["albums"]) ){
				echo "ERROR";
				exit;
			}
			$albumTokens = explode(",",$this->mParams["albums"]);
			require_once("travellog/model/Class.PhotoAlbum.php");
			foreach($albumTokens AS $token){
				$tokens = explode("_",$token);
				if( 2 != count($tokens) ){
					continue;
				}
				$albumID = $tokens[0];
				$feature = 1 == $tokens[1] ? TRUE : FALSE;
				try{
					$album = new PhotoAlbum($albumID);
					if( $album instanceof PhotoAlbum && $album->getGroupID() == $this->mID ){
						$album->Feature($feature,$this->mGroup);
					}	
				}catch(exception $e){
				}
			}
		}
		
		private function _updateManageFeaturedAlbumsNotice(){
			$this->_checkUserLevel();
			$content = new Template();
			$content->set("featuredAlbums",$this->mGroup->getFeaturedPhotoAlbums());
			$content->set("unfeaturedAlbums",$this->mGroup->getFeaturedPhotoAlbums($featured=FALSE));
			$content->set("albumCount",$this->mGroup->getPhotoAlbumCount());
			$content->out("travellog/UIComponent/Collection/views/tpl.ViewManageFeaturedAlbums.php");
		}
		
		private function _featureAlbum(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			if( !isset($this->mParams["feature"]) ){
				echo "ERROR";
				exit;
			}
			require_once("travellog/model/Class.PhotoAlbum.php");
			try{
				$album = new PhotoAlbum($this->mGenID);
				if( $album instanceof PhotoAlbum ){
					$album->Feature(1==$this->mParams["feature"],$this->mGroup);
				}
			}catch(exception $e){
			}
		}
		
		function _loadRecentUploads(){
			$this->_checkUserLevel();
			if( self::VIEWER == $this->mUserLevel ){
				echo "RESTRICTED";
				exit;
			}
			require_once("travellog/UIComponent/Collection/views/Class.ViewRecentUploads.php");
			$content = new ViewRecentUploads();
			$content->setType($this->mType);
			$content->setID($this->mID);
			$content->setContext($this->mContext);
			$content->setGenID($this->mGenID);
			$content->setUserLevel($this->mUserLevel);
			$content->setLoggedID($this->mLoggedID);
			$content->setGroup($this->mGroup);
			$content->setTraveler($this->mTraveler);
			$content->setAction($this->mParams["action"]);
			$content->render();
		}
		
		function extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}
		
		function _createCobrandCustomHeader(){
			ini_set('max_execution_time','1800');

			require_once("travellog/model/Class.PathManager.php");
			require_once("travellog/model/Class.TravelLog.php");
			require_once("travellog/UIComponent/Collection/model/Class.UploadedPhoto.php");

			$sql = "SELECT MAX(lastPhotoID) AS lastPhotoID
					FROM tblCustomPageHeaderConversionTemp";
			$conn 	= new Connection();
			$rs   	= new Recordset($conn);
			$rs->Execute($sql);
			$data = mysql_fetch_array($rs->Resultset());
			
			$photoStart = $data["lastPhotoID"]+1;
			$photoEnd = $photoStart+19;

			$sql2 = "SELECT tblPhoto.*,
						tblTravelLog.*,
						tblTravel.travelerID,
						tblTravel.travellinkID
					FROM tblPhoto,
						tblTravelLogtoPhoto,
						tblTravelLog,
						tblTravel
					WHERE tblPhoto.phototypeID = 2
						AND tblPhoto.photoID >= $photoStart
						AND tblPhoto.photoID <= $photoEnd
						AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID
						AND tblTravelLogtoPhoto.travellogID = tblTravelLog.travellogID
						AND tblTravelLog.travelID = tblTravel.travelID
					ORDER BY tblPhoto.photoID ASC";
			$conn2 	= new Connection();
			$rs2   	= new Recordset($conn2);
			$rs2->Execute($sql2);

			$qualifiedPhotos = 0;
			//$lastPhotoID = 0;
			$recordcount = $rs2->RecordCount();

			if( 0 == $recordcount ){
					
				$sql_ = "SELECT MAX(photoID) AS photoID
						FROM tblPhoto";
				$conn_ 	= new Connection();
				$rs_   	= new Recordset($conn_);
				$rs_->Execute($sql_);
				$data_ = mysql_fetch_array($rs_->Resultset());
				
				if( $photoEnd >= $data_["photoID"] ){
					$photoEnd = $data_["photoID"];
				}
				
				$sql4 = "INSERT INTO tblCustomPageHeaderConversionTemp(lastPhotoID,qualified) 
						VALUES($photoEnd,0)";
				$conn4 	= new Connection();
				$rs4   	= new Recordset($conn4);
				$rs4->Execute($sql4);
				
				if( $photoEnd == $data_["photoID"] ){
					echo "TERMINATE";
				}else{
					echo "TO CONTINUE...";
				}
	
				exit;
			}
			while( $data = mysql_fetch_array($rs2->Resultset()) ){
				try{
					$travellog = new TravelLog(0,$data);
					$photo = new Photo($travellog,0,$data);

					$photoID = $data["photoID"];
					$travellogID = $data["travellogID"];

					$sql4 = "INSERT INTO tblCustomPageHeaderConversionTemp(lastPhotoID,qualified) 
							VALUES($photoID,0)";
					$conn4 	= new Connection();
					$rs4   	= new Recordset($conn4);
					$rs4->Execute($sql4);

					$qualified = FALSE;
					
					$pm = new PathManager($travellog,'image');
					$relPath = $pm->GetPathrelative();
					$fileName = $photo->getFilename();
					$origPathFilename = $relPath."orig-".$fileName;
					
					if( file_exists($origPathFilename) ){

						echo "<div>".$travellog->getTitle()." &rarr; ".$photo->getPhotoID()." &rarr; Image[<a href='".$photo->getPhotoLink("orig-")."'>".$photo->getPhotoLink("orig-")."</a>]</div>";

						$upPhoto = new UploadedPhoto();
						$upPhoto->resizeOriginal($origPathFilename,1024);
						$qualified = $upPhoto->_create_cobrandcustomsize_script($pm,$photo,$travellog);
					}

					if( $qualified ){
						$qualifiedPhotos++;

						$sql4 = "UPDATE tblCustomPageHeaderConversionTemp
									SET qualified = 1
									WHERE lastPhotoID = $photoID"; 
						$conn4 	= new Connection();
						$rs4   	= new Recordset($conn4);
						$rs4->Execute($sql4);

						$sql3 = "UPDATE tblTravelLogtoPhoto
								SET qualifiedCustomHeader = 1
								WHERE travellogID = $travellogID
									AND photoID = $photoID";
						$conn3 	= new Connection();
						$rs3   	= new Recordset($conn3);
						$rs3->Execute($sql3);
						echo "<div>".$travellog->getTitle()." &rarr; ".$photo->getPhotoID()." &rarr; QUALIFIED Image[<a href='".$photo->getPhotoLink("customheaderstandard")."'>".$photo->getPhotoLink("customheaderstandard")."</a>]</div>";
					}else{
						echo "<div>".$travellog->getTitle()." &rarr; ".$photo->getPhotoID()." &rarr; NOT QUALIFIED</div>";
					}
					//$lastPhotoID = $photoID;
				}catch(exception $e){}
			}

			//echo "<br />COUNT OF QUALIFIED PHOTOS: $qualifiedPhotos<br /><br />";

			/*$sql5 = "SELECT COUNT(*) AS totalQualified
					FROM tblCustomPageHeaderConversionTemp
					WHERE tblCustomPageHeaderConversionTemp.qualified = 1";
			$conn5 	= new Connection();
			$rs5   	= new Recordset($conn5);
			$rs5->Execute($sql5);
			$data = mysql_fetch_array($rs5->Resultset());*/

			/*echo "RANGE: $photoStart - $photoEnd<br /><br />";
			echo "QUERY: ".$sql2."<br /><br />";
			echo "RECORDCOUNT: ".$recordcount."<br /><br />";
			echo "TOTAL QUALIFIED PHOTOS CONVERTED: ".$data["totalQualified"]."<br /><br />";

			if( 0 == $recordcount ){
				echo "<br /><strong>END</strong>";
				ini_set('max_execution_time','30');
				exit;
			}else{
				ini_set('max_execution_time','30');
			}*/
		}
		
		function _fixGroupLogoThumbnail(){//fix group logo thumbnails within a group networks
			require_once("travellog/model/Class.PathManager.php");
			require_once("travellog/model/Class.AdminGroup.php");
			require_once("travellog/model/Class.FunGroup.php");
			require_once("travellog/UIComponent/Collection/model/Class.UploadedPhoto.php");
			
			ob_clean();
			
			if( isset($_GET["gID"]) ){
				$gID = $_GET["gID"];
				$sql = "SELECT *
						FROM tblGroup
						WHERE groupID = '$gID'";
			}elseif( !isset($_GET["from"]) || !isset($_GET["to"]) ){
				echo "Specify gID range...";
			}else{
				$from = $_GET["from"];
				$to = $_GET["to"];
				$sql = "SELECT *
						FROM tblGroup
						WHERE parentID = 0
							AND groupID >= '$from'
							AND groupID < '$to' ";
			}
			
			$conn 	= new Connection();
			$rs   	= new Recordset($conn);
			$rs->Execute($sql);
			if( !$rs->Recordcount() ){
				$this->_dumpError("No group found...");
				exit;
			}
			$ctr = 0;
			while( $data = mysql_fetch_array($rs->Resultset()) ){
				$groupID = $data["groupID"];
				$discriminator = $data["discriminator"];
				$groupObj = NULL;
				if( Group::FUN == $discriminator ){
					try{
						$groupObj = new FunGroup($groupID);
						if( !($groupObj instanceof FunGroup)){
							$this->_dumpError("$groupID not a FunGroup");
							exit;
						}
					}catch(exception $e){
						$this->_dumpError("$groupID not a FunGroup");
						exit;
					}
				}else{
					try{
						$groupObj = new AdminGroup($groupID);
						if( !($groupObj instanceof AdminGroup)){
							$this->_dumpError("$groupID not an AdminGroup");
							exit;
						}
					}catch(exception $e){
						$this->_dumpError("$groupID not an AdminGroup");
						exit;
					}
				}
				$ctr++;
				$background = 0 == $ctr%2 ? "#FCFCFC" : "#CCCCCC";
				echo '<div id="main_group_'.$groupObj->getGroupID().'" style="background-color: '.$background.'">';
				echo '<strong><a href="'.$groupObj->getFriendlyURL().'" target="_blank">'.$groupObj->getName().'</a></strong>';
				
				if( 0 < $data["photoID"]){
					try{
						$mainPhoto = $groupObj->getGroupPhoto();
						if( !($mainPhoto instanceof Photo) ){
							$this->_dumpError($data["photoID"]." is not an instance of Photo.");
						}
						$this->_fixGroupLogo($groupObj,$mainPhoto);
					}catch(exception $e){
						$this->_dumpError($e->getMessage());
					}
				}else{
					$this->_dumpError("$groupID doesn't have a group logo");
				}
				
				//fix subgroup logos
				if( $groupObj instanceof AdminGroup && 0 == $data["parentID"] ){
					$sql2 = "SELECT *
							FROM tblGroup
							WHERE parentID = '$groupID'";
					$rs2   	= new Recordset($conn);
					$rs2->Execute($sql2);
					if( !$rs2->Recordcount() ){
						$this->_dumpError("No subgroup group found...");
						continue;
					}
					echo '<ul>';
					while( $data2 = mysql_fetch_array($rs2->Resultset()) ){
						$subgroupID = $data2["groupID"];
						echo '<li>';
						echo'<div id="subgroup_'.$subgroupID.'">';
						try{
							$subgroupObj = new AdminGroup($subgroupID);
							if( !($subgroupObj instanceof AdminGroup)){
								$this->_dumpError("$subgroupID not an AdminGroup");
								echo '</div>';
								echo '</li>';
								continue;
							}
						}catch(exception $e){
							$this->_dumpError("$subgroupID not an AdminGroup");
							echo '</div>';
							echo '</li>';
							continue;
						}
						echo '<strong><a href="'.$subgroupObj->getFriendlyURL().'" target="_blank">'.$subgroupObj->getName().'</a></strong>';
						
						if( 0 < $data2["photoID"]){
							try{
								$subgroupPhoto = $subgroupObj->getGroupPhoto();
								if( !($subgroupPhoto instanceof Photo) ){
									$this->_dumpError($data2["photoID"]." is not an instance of Photo.");
								}
								$this->_fixGroupLogo($subgroupObj,$subgroupPhoto);
							}catch(exception $e){
								$this->_dumpError($e->getMessage());
							}
						}else{
							$this->_dumpError("$groupID doesn't have a group logo");
						}
						
						echo '</div>';
						echo '</li>';
					}
					echo '</ul>';
				}
				echo '</div>';
			}
		}
		
		private function _fixGroupLogo($group=NULL,$photo=NULL){
			try{
				$pm = new PathManager($group,'image');
			
				$upPhoto = new UploadedPhoto();
				$result = $upPhoto->fixGroupLogoThumbnail($pm,$photo);
				echo $result;
			}catch(exception $e){
				throw $e;
			}
		}
		
		private function _dumpError($message=""){
			echo "<div>$message</div>";
		}
	}