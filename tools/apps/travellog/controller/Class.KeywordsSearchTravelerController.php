<?php
require_once('travellog/controller/Class.AbstractTravelerController.php');
require_once('travellog/model/Class.SearchFirstandLastnameService.php');
require_once('travellog/model/Class.TravelerSearchCriteria2.php');
require_once("Class.Criteria2.php");
class KeywordsSearchTravelerController extends AbstractTravelerController{
	function viewContents(){
		$this->viewMain();
		$cond         = new Condition;
		$c            = new Criteria2;
		$filter       = new FilterCriteria2();
		$query_string = 'keywords='.$this->keywords.'&searchby='.$this->searchby;

		$c->setLimit( $this->offset.','.$this->rows );
		
		if ( $this->searchby == 1 ){
			$c->setOrderBy('username');
			$c->mustBeLike('username', $this->keywords);
		}elseif ( $this->searchby == 2 ){
			$c->setOrderBy('email');
			$c->mustBeLike('email', $this->keywords);
		}elseif ( $this->searchby == 3 ){
			$c->setOrderBy('interests');
			$c->mustBeLike('interests', $this->keywords);
		}

		if ( $this->searchby == 4 ){
			$obj_search    = new SearchFirstandLastnameService(); 	
			$obj_travelers = $obj_search->performSearch($this->keywords);
			$obj_paging    = new Paging        (  count($obj_travelers), $this->page,$query_string, $this->rows );
			$obj_iterator  = new ObjectIterator( $obj_travelers, $obj_paging                                    );
		}else{
			$myobj         = new TravelerSearchCriteria2;
			$obj_travelers = $myobj->getAllTravelers($c);
			$obj_paging    = new Paging        ( $myobj->getTotalRecords(), $this->page,$query_string, $this->rows );
			$obj_iterator  = new ObjectIterator( $obj_travelers, $obj_paging, true                                 );
		}
		
		$this->main_tpl->set( 'obj_paging'   , $obj_paging       );
		$this->main_tpl->set( 'obj_iterator' , $obj_iterator     );
		$this->main_tpl->set( 'selected'     , 1                 );
		$this->main_tpl->set( 'travelerID'   , $this->travelerID );
		$this->main_tpl->set( 'isLogin'      , $this->IsLogin    );
		$this->main_tpl->set( 'isAdvisor'    , $this->isAdvisor  );
		
		$this->main_tpl->set( 'mainContent', $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php") );
		$this->main_tpl->out("travellog/views/tpl.Travelers.php");		
	}
	
	
	
	function setSearchBy  ( $searchby   ){ $this->searchby = $searchby; }
	
	function setKeywords  ( $keywords   ){ $this->keywords = $keywords; }
}
?>
