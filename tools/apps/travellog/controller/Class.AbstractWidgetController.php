<?php
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");

// Added by: Adelbert Silla
require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');

//TODO: refactor
abstract class AbstractWidgetController implements IController{

	protected $properties = array('showAds' => true);

	public  $file_factory = NULL; 
//	public $obj_session  = NULL; -- unused variable

	function __construct(){
		$this->file_factory = FileFactory::getInstance();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
		
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('TravelerProfile');
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));		
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));	
		$this->file_factory->registerClass('TravelJournalRSSFeeder2');
		$this->file_factory->registerClass('HelpText');
		$this->file_factory->registerClass('SessionManager', array("path" => "travellog/model/"));
		$this->file_factory->registerClass('HelperGlobal', array('path' => ''));
		$this->file_factory->registerClass('TravelMapPeer', array('path' => 'travellog/model/socialApps/mytravelmap/'));
		$this->file_factory->registerClass('Country');
		$this->file_factory->registerClass('TravelSchedule');
		$this->file_factory->registerClass('Advertisement');
		$this->file_factory->registerClass('Template',array('path'=>''));
		$this->file_factory->registerClass('SubNavigation');
		
		$this->file_factory->registerClass('TwitterWidget', array('path' => 'travellog/controller/twitterWidget/'));
		$this->file_factory->registerClass('FacebookWidget', array('path' => 'travellog/controller/facebookWidget/'));		

		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');

		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
	}

	function performAction() {
		
		$this->properties['travelerID'] = $this->file_factory->invokeStaticClass('SessionManager','getInstance',array())->get('travelerID');

		if (empty($this->properties['travelerID'])) {
			$this->file_factory->invokeStaticClass('HelperGlobal', 'getLoginPage');
			exit;
			
		} else if (isset($_REQUEST['app']) && isset($_REQUEST['action'])) {//$_REQUEST['app'] signals an AJAX Request
			//TODO: Logged in users can potentially spoof other users and issue AJAX requests HAM		
			// ANSWER: Use the session data
			// HAM: I think the above is a non-issue (it's a leftover from a previous iteration of the controller). 				
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($this->properties['travelerID'],'TRAVELER');
			$this->{'handleAjax'.strtoupper($_REQUEST['app'])}();
			exit;
		
		// Use the traveler on session instead of obtaining the user from $_GET
	    } else {
			try {
				$this->properties['traveler'] = $this->file_factory->getClass('Traveler', array($this->properties['travelerID']));
			} catch (Exception $e) {
				//TODO: determine exact behavior to exhibit
				header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
				exit;						
			}
			
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($this->properties['travelerID'],'TRAVELER');
			
			// Admins don't have widgets/apps
			$traveler = $this->properties['traveler'];
			// NOTE to SELF: I can't believe we can't do this in php: $this->properties['traveler']->isAdvisor()
			// I had to resort to creating a temporary variable			
			// HAM: Hey SELF. I do this "$arrayVariable['objectsArrayKey']->objectsMethod()" all the time. 
			// It's possible that $this->properties['traveler'] is not being set? The Traveler class could be 
			// swallowing up the exceptions or calls methods that have their own exception handlers. If so, we'll
			// have to replace the try-catch clause above. A simple value check for the traveler property would suffice. 			
			/*if ($traveler->isAdvisor()) {
			    header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
			}*/
			
			// We're using the travelerID stored in session. This may no longer be needed 
			// We're only keeping this for instances where the the travelerID is specified in the request params
			// HAM: I used the request parameter in the off chance that we may have to allow non-owners to view a user's
			// apps page. If we don't need to prepare for this eventuality, we can remove all references to $_GET['travelerID'].
			// Anyway this is trivial to put back in if needed.
			if (isset($_GET['travelerID']) && !($this->properties['owner'] = ($this->properties['travelerID'] == $_GET['travelerID']))) {
				//TODO: redirect to referrer?
				header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
				exit;				 
			}
			// We're forcing this one for now
			$this->properties['owner'] = true;
			
			$this->file_factory->getClass('TravelerProfile',array($this->properties['travelerID']));
			
			$this->properties['travelerprofile'] = $this->file_factory->getClass('TravelerProfile',array($this->properties['travelerID']));
		
			if ($this->properties['travelerprofile']->getIssuspended()) {
				//TODO: redirect to referrer?
				header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
				exit;				 
			}
			
			try {
				$this->properties['traveler'] = $this->file_factory->getClass('Traveler', array($this->properties['travelerID']));
			} catch (Exception $e) {
				//TODO: determine exact behavior to exhibit
				header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
				exit;						
			}
		}

		$this->prepareMainTemplate();
		$this->prepareDependencies();		

		//Main profile 
		$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
		$profile_comp->init($this->properties['travelerID']);

		$this->properties['template'] = $this->file_factory->getClass('Template');		
		$this->properties['template']->set_path('travellog/views/');
		$this->properties['template']->set('profile_comp', $profile_comp->get_view());
				
		//Template properties shared by the components 
		$this->properties['template']->set('owner', $this->properties['owner']);

		$this->prepareComponent('SubNavigation');
		$this->prepareComponent('ExternalWidgetsLinks');
		$this->prepareComponent('MyTravelMap');
		$this->prepareComponent('MyTravelBio');
		$this->prepareComponent('MyTravelPlans');
		$this->prepareComponent('TravelJournalRssFeeder');
		$this->prepareComponent('Advertisement');
		
		$this->prepareComponent('TwitterWidget');
		$this->prepareComponent('FacebookWidget');		
		
		$this->properties['template']->set('siteName', isset($GLOBALS['CONFIG']) ? $GLOBALS['CONFIG']->getSiteName() : 'GoAbroad');
		
		$this->properties['template']->out('tpl.ViewTravelerProfileWidgets.php');
	}
	
	private function prepareComponent($componentName) {
		$this->{'prepare'.$componentName}();
	}	

	private function prepareExternalWidgetsLinks() {
		// Edited by: bert 
		// Date: May 5, 2009  --> Added MySpace, Hi5! and Orkut links
		// Date: July 2, 2009 --> Completed all links, created url variables to avoid redundancy.
		// Date: July 8, 2009 --> Changed Links to FAQ bring_2_facebook

		$FAQLink = "http://{$_SERVER['SERVER_NAME']}/faq.php#bring_2_facebook";

		$containerLinks = array(
			'hi5!'		=> $FAQLink,
			'Orkut'		=> $FAQLink, 
			'MySpace'	=> $FAQLink, 
			'Facebook'	=> $FAQLink 
		);				 
		
		$links = array(
			'My Travel Bio' 	 => $containerLinks,
			'My Travel Map' 	 => $containerLinks,
			'My Travel Plans' 	 => $containerLinks,
			'My Travel Journals' => $containerLinks
		);
		
		$this->properties['template']->set('widgets', $links);		
	}
	
	private function prepareMainTemplate() {
		$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));		
		
		$travelerprofile = $this->properties['travelerprofile'];
		$username = $travelerprofile->getUsername();
		$travelertype = $travelerprofile->getTravType()->gettravelertype();
		
		$hometown = NULL;
		$country = NULL;
		$currhometown = NULL;
		$currcountry = NULL;
		
		if ($travelerprofile->getHTLocationID()) {
			$hometown = $travelerprofile->getCity()->getName();
			$country = $travelerprofile->getCity()->getCountry()->getName();
		}
		
		if ($travelerprofile->getCurrLocationID()) {
			$currhometown = $travelerprofile->getCurrLocation()->getName();
			$currcountry = $travelerprofile->getCurrLocation()->getCountry()->getName();
		}
				
		if ($this->properties['owner']) {
			Template::setMainTemplateVar('title',  'GoAbroad Network: Online Community for Travelers');
			Template::setMainTemplateVar('page_location', 'My Passport');
		} else {
			$tmpTitlePhrase = (strlen($username)-1 == strripos($username,'s'))?$username."'":$username."'s"; //TODO: don't know if this is still used elsewhere - icasimpan Aug 13, 2007
			$tmpUsernamePossessive = (strlen($username)-1 == strripos($username,'s'))?$username."'":$username."'s";
			$tmpBirthPlace = ($hometown!=$country)?"$hometown, $country":$country;
			$tmpDescriptionPhrase = ($travelertype)?$travelertype:'traveler';

			Template::setMainTemplateVar('title',  $tmpUsernamePossessive . ' Travel Journals - Travel Blogs - Travel Reviews');
	 		Template::setMainTemplateVar('metaDescription', $tmpUsernamePossessive . ' Travel Profile on GoAbroad Network. Check out the  wonderful travel experiences of this ' . $tmpDescriptionPhrase . ' from ' . $tmpBirthPlace);
			Template::setMainTemplateVar('metaKeywords', 'travel review, travel journal, travel blog, travel experiences, travel advice, travel photos, travel maps');
			Template::setMainTemplateVar('page_location', 'Travelers');
		}

		Template::setMainTemplateVar('layoutID', 'page_profile');				
	}
	
	private function prepareDependencies() {
		//Template::includeDependentCss('/css/myAlert.css');
		
		//NOTE: these are experimental libraries and are subject to modifications;		
		//TODO: combine these two and trim down myLib.js HAM
		//Template::includeDependentJs('/js/LIB.js'); // -> TravelBio in profile component already includes this library
		//Template::includeDependentJs('/js/myLib-min.js');
		Template::includeDependentJs('/min/g=AppsJs', array("bottom" => true));
		//Template::includeDependentJs('/js/myAlert.js');//myLib should go first		
	}
	
	private function prepareSubNavigation() {
		$subNavigation = $this->file_factory->getClass('SubNavigation');		
		$subNavigation->setContext('TRAVELER');
		$subNavigation->setContextID($this->properties['travelerID']);
		$subNavigation->setLinkToHighlight('WIDGETS');		

		$this->properties['template']->set("subNavigation",$subNavigation);
	}
	
	private function prepareAdvertisement() {
		if ($this->properties['showAds']) {
			$this->properties['template']->set('advertisement', $this->file_factory->invokeStaticClass('Advertisement','getAd'));
		}
	}

	private function prepareTravelJournalRSSFeeder() {
		$TravelJournalRSSFeeder = $this->file_factory->getClass('TravelJournalRSSFeeder2');
		$this->properties['template']->set('hasTravelupdate', $TravelJournalRSSFeeder->hasTravelupdate($this->properties['traveler']));
	}

	private function prepareMyTravelBio() {
		$template = $this->properties['template'];
		
		//TODO: refactor code to remove redundant data retrieval HAM
		$randomTravelBio = $this->file_factory->getClass('RandomTravelBioView');
		$randomTravelBio->setOwnerID($this->properties['travelerID']);
		$randomTravelBio->readTravelBio();
		$randomTravelBio->setupWidget($template);

		$this->file_factory->registerClass('Answer' , array('path' => 'travellog/model/travelbio/'));
		$this->file_factory->registerClass('TravelBioUser' , array('path' => 'travellog/model/travelbio/'));
		$this->file_factory->registerClass('ProfileQuestion', array('path' => 'travellog/model/travelbio/'));		
		
		$profileQuestions = $this->file_factory->invokeStaticClass('ProfileQuestion', 'getQuestions');
		$travelBioUser = $this->file_factory->invokeStaticClass('TravelBioUser', 'getTravelBioUserByTravelerID', array($this->properties['travelerID']));
		
		$answers = array();

		if ($travelBioUser) $answers = $travelBioUser->getAnswers();

		$structAns = array();
		foreach($answers as $iAns){
			$structAns[$iAns->getProfileQuestionID()] = $this->parseToXML($iAns->getValues());
		}

		$template->set('profileQuestions',$profileQuestions);		
		$template->set('structAns',$structAns);
		$template->set('editModeMTB', isset($_GET['action']) && ($_GET['action'] == 'editMTB'));
		$template->set('travelBio', $template->fetch("tpl.ViewTravelBioWidget.php"));
		
	}
	
	private function prepareMyTravelMap() {//uses the jquery lib included by default in main template

		$template = $this->properties['template'];
		// Since we're allowing only the owner to view her/his widget tab,
		// $this->properties['owner'] is always true
		$userMap = $this->file_factory->invokeStaticClass('TravelMapPeer', 'getTravelMap', array($this->properties['owner'] ? $this->properties['travelerID'] : $_GET['travelerID']));
		$fbStatus = OAuthAPIFactory::getInstance()->create('facebook')->checkWidgetConnection($this->properties['travelerID']);

		$template->set('userMap', $userMap);		
		$template->set('travelMapCount', count($userMap->getCountriesTraveled()));
		$template->set('countryList', $this->file_factory->invokeStaticClass('Country', 'getCountryList'));
		$template->set('editModeMTM', isset($_GET['action']) && ($_GET['action'] == 'editMTM')); 
		$template->set('pinSettings', Traveler::getMapPinSettings($this->properties['travelerID']));
		$template->set('travelerID', $this->properties['travelerID']);
		$template->set('fbStatus', $fbStatus == 'CONNECTED');
		$template->set('travelMap', $template->fetch('tpl.ViewTravelMapsWidget.php'));
				
		Template::includeDependentJs('http://maps.google.com/maps?file=api&v=2&key=' . $userMap->getGMapKey(), array('minify'=>false,'bottom'=>true));
	}

	private function prepareMyTravelPlans() {
		$template = $this->properties['template'];
		
		$travelPlansUser = $this->file_factory->invokeStaticClass('TravelSchedule',	'getTravelPlansUserWithTravelerID', array($this->properties['travelerID']));
		$itemsToDisplaySetting = isset($travelPlansUser['travelPlansDisplayed']) ? $travelPlansUser['travelPlansDisplayed'] : 5;
		$includePastTravelsSetting = isset($travelPlansUser['travelPlansIncludePast']) ? $travelPlansUser['travelPlansIncludePast'] : false;
		
		//TODO: minimize db queries
		$travelItems = $this->file_factory->invokeStaticClass('TravelSchedule', 'getOrderedTravels', array($travelPlansUser['userID']));
		$pastTravels = $this->file_factory->invokeStaticClass('TravelSchedule', 'getPastTravels', array($travelPlansUser['userID']));
		
		if ($includePastTravelsSetting) $travelItems = array_merge($travelItems, $pastTravels);
		
		$totalNumItems = count($travelItems);
	
		if ($totalNumItems > $itemsToDisplaySetting) {
			for ($i = 1, $j=$totalNumItems - $itemsToDisplaySetting; $i <= $j; $i++ ) {
				array_pop($travelItems);
			}
		}
		
		$template->set('showMyTravelPlans', $totalNumItems != 0);
		$template->set('travelItems', $travelItems);
		$template->set('totalNumItems', $totalNumItems);
		$template->set('itemsToDisplaySetting', $itemsToDisplaySetting);
		$template->set('pastTravelsCount' , count($pastTravels));
		$template->set('includePastTravelsSetting', $includePastTravelsSetting);		

		if (isset($_GET['action'])) {
			if ($_GET['action'] == 'editMTP') {
				$template->set('editModeMTP', true);
				$template->set('doAction', isset($_GET['do']) ? $_GET['do'] : '');
				$template->set('twID', isset($_GET['travelWishID']) ? $_GET['travelWishID'] : '');
			}
		}

		// TRAVEL WISH LIST
		$template->set('countryList',         $this->file_factory->invokeStaticClass('Country',        'getCountryList'));
		$template->set('countriesInterested', $this->file_factory->invokeStaticClass('TravelSchedule', 'getCountriesToTravel', array($this->properties['travelerID'])));
			
		$template->set('incTravelplans', $template->fetch("tpl.ViewTravelPlansWidget.php"));

		//Template::includeDependentJs('/js/profileWidgets-1.1.js', array('minify'=>false,'bottom'=>true));						

	}
	
	private function handleAjaxMTM() {
		require_once 'travellog/model/socialApps/mytravelmap/Class.TravelMapPeer.php';
				
		switch ($_REQUEST['action']) {
		case 'edit':
			$newCountries = isset($_POST['sel_countries']) ? $_POST['sel_countries'] : array();
			TravelMapPeer::setCountriesTraveled($this->properties['travelerID'], $newCountries);
		
			$this->invalidateMTMCache();
		
			$userMap = $this->file_factory->invokeStaticClass('TravelMapPeer', 'getTravelMap', array($this->properties['travelerID']));
			
			echo $userMap->getCountryNamesList();
		
			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($this->properties['travelerID'],2);
		
			return;
								
		case 'getMap':
			$userMap = $this->file_factory->invokeStaticClass('TravelMapPeer', 'getTravelMap', array($this->properties['travelerID']));
			$pinSettings = Traveler::getMapPinSettings($this->properties['travelerID']);
			$xml = "<markers>";
			foreach ($userMap->getCountriesTraveled() as $mapEntry) {
  				$countryName = $this->parseToXML($mapEntry->getCountry()->getName());

				$display = $pinSettings != 1 ? true : false;
				
  				$lat = $mapEntry->getCountry()->getLatitude();
  				$lng = $mapEntry->getCountry()->getLongitude();
  				$photos = '';//$country->getCountry()->getNumPhotos();
  				$journals = '';//$country->getNumJournalEntries();
  				$tips = '';//$country->getNumTravelTips();
				$randomPhoto = '';//$country->getNumPhotos() ? $country->getRandomPhoto() : '';
  				$xml .= "<marker country='$countryName' lat='$lat' lng='$lng' journals='$journals' photos='$photos' tips='$tips' randomPhoto='$randomPhoto' type='country' display='$display'/>";

				if($pinSettings != 0) {
					foreach ($mapEntry->getCountry()->getCityLocationList() as $entry) {
						$cityName = $entry->getName();
		  				$lat = $entry->getLatitude();
		  				$lng = $entry->getLongitude();						
	  					$xml .= "<marker country='$countryName' lat='$lat' lng='$lng' journals='' photos='' tips='' randomPhoto='' type='city' display='true'/>";
					}
				}
			}

			$xml .= "</markers>";

			header("Content-type: text/xml");

			echo $xml;
										
			return;	

		case 'showCities':

			self::requireFilesForMTM();
			
			$location = LocationFactory::instance();
			$location = $location->create($_REQUEST['locationID']);
			$cities = $location->getCountry()->getCities(0, false);

			$travelerID = $this->properties['travelerID'];
			$countryID = $location->getCountry()->getCountryID();
			$userID = gaOpenSocialTravelerMapper::getUserIDWithTravelerID($travelerID);
			
			$mapEntry = MyTravelMapDAO::__retrieveTravelMapEntry($countryID, $userID, $travelerID);


			if (is_object($mapEntry)) {
				$selectedCities = array_keys($mapEntry->getCountry()->getCityLocationList());
			} else {
				$selectedCities = array();
			}

			$template =	$this->file_factory->getClass('Template');		
			$template->set_path('travellog/views/mytravelmap/ganet/');
			
			$template->set('countryID', $countryID);
			$template->set('countryLocationID', $_REQUEST['locationID']);
		   	$template->set('selectedCities', $selectedCities);
			$template->set('latitude', $location->getCoordinates()->getY());
		   	$template->set('longitude', $location->getCoordinates()->getX());
		   	$template->set('cities', $cities);

			echo $template->fetch("mtm_cities.php");
			return;
		
		case 'addCities':

			self::requireFilesForMTM();

			$countryLocationID = $_REQUEST['countryLocationID'];
			$countryID = $_REQUEST['countryID'];
			$locationIDs = explode('.', substr($_REQUEST['stringLocationIDs'], 1));
			$travelerID = $this->properties['travelerID'];

			$userID = gaOpenSocialTravelerMapper::getUserIDWithTravelerID($travelerID);
			    		
			MyTravelMapDAO::_addSyncedTravelMapEntry($travelerID, $locationIDs, $countryID);
			//ganetCacheProvider::instance()->getCache()->delete('Traveler_Countries_Traveled_'.$travelerID);
			$this->invalidateMTMCache();

			$mapEntry = MyTravelMapDAO::__retrieveTravelMapEntry($countryID, $userID, $travelerID);

			$template =	$this->file_factory->getClass('Template');
			$template->set_path('travellog/views/mytravelmap/ganet/');
			
			$template->set('locationID', $countryLocationID);
			$template->set('mapEntry', $mapEntry);

			echo $template->fetch("mtm_addCities.php");
			return;

		case 'removeCity':
		
			self::requireFilesForMTM();
			$locationID = array($_REQUEST['locationID']);
    		$travelerID = $this->properties['travelerID'];
			MyTravelMapDAO::_removeSyncedTravelMapEntry($travelerID, $locationID);
			//ganetCacheProvider::instance()->getCache()->delete('Traveler_Countries_Traveled_'.$travelerID);
			$this->invalidateMTMCache();
			return;

		case 'removeCountry':
		
			self::requireFilesForMTM();
			$countryID = $_REQUEST['countryID'];
    		$travelerID = $this->properties['travelerID'];
			MyTravelMapDAO::_removeTravelMapEntryByCountryID($travelerID, $countryID);
			//ganetCacheProvider::instance()->getCache()->delete('Traveler_Countries_Traveled_'.$travelerID);
			$this->invalidateMTMCache();
			return;
		
		case 'dipsplayModalMap':
			$template =	$this->file_factory->getClass('Template');
			$template->set_path('travellog/views/mytravelmap/ganet/');
			$template->set('locationID', $_REQUEST['countryLocationID']);
			$template->set('countryID', $_REQUEST['countryID']);
			$template->set('latitude', $_REQUEST['latitude']);
			$template->set('longitude', $_REQUEST['longitude']);
			$template->set('isNewCountry', $_REQUEST['isNewCountry']);

			echo $template->fetch("mtm_modalMap.php");
			return;
	   
		case 'displayCustomAddLocationMap':
			$template =	$this->file_factory->getClass('Template');
			$template->set_path('travellog/views/mytravelmap/ganet/');
			$template->set('vars', $_REQUEST);
			$template->set('travelerID', $this->properties['travelerID']);
			$template->set('mapKey', TravelMapPeer::getGMapKey());
			echo $template->fetch("mtm_add_new_location.php");
			//Template::includeDependentJs('http://maps.google.com/maps?file=api&v=2&key=' . TravelMapPeer::getGMapKey(), array('minify'=>false,'bottom'=>true));
			return;

		case 'addLocation' : 

			self::requireFilesForMTM();
		  	require_once('travellog/model/Class.Coordinates.php');
		
			$name  = $_REQUEST['name'];
			$locID = $_REQUEST['locID'];
			$lat   = $_REQUEST['latitude'];
			$long  = $_REQUEST['longitude'];
			$countryID = $_REQUEST['countryID'];
			$isNewCountry = $_REQUEST['isNewCountry'];

		  	if(is_null($lat) || is_null($long) || !$locID || !$name)
		  		return false;
		
		  	$new_city = new FBNewCity();
		  	$new_city->setCountryID((!$countryID) ? FBLocationPeer::convertLocationIDtoCountryID($locID) : $countryID);
		  	$new_city->setName($name);
		  	$new_city->save();

		  	$coordinate = new Coordinates($new_city->getLocationID());
		  	$coordinate->setX($long);
		  	$coordinate->setY($lat);
		  	$coordinate->save();

		  	//$this->clearCache();
		  	$newLocationIDs[] = $new_city->getLocationID();

	    	if($isNewCountry && is_array($newLocationID)) $newLocationIDs[] = $locID;

    		$travelerID = $this->properties['travelerID'];
			MyTravelMapDAO::_addSyncedTravelMapEntry($travelerID, $newLocationIDs, $countryID);
			//ganetCacheProvider::instance()->getCache()->delete('Traveler_Countries_Traveled_'.$id);
			$this->invalidateMTMCache();
 
			return;

		case 'saveSettings' :
			$travelerID = $this->properties['travelerID'];
			if (isset($_REQUEST['mapPinSettings'])){
				$mapPinSettings = $_REQUEST['mapPinSettings'];
				Traveler::updateMapPinSettings($travelerID, $mapPinSettings);
			}
			return;
		}
		
	}
	
	private function handleAjaxMTB() {
		switch ($_REQUEST['action']) {
		case 'getRandomQuestion':
			$randomTravelBio = $this->file_factory->getClass('RandomTravelBioView');
			$randomTravelBio->setOwnerID($this->properties['travelerID']);
			if (isset($_REQUEST['qID'])) $randomTravelBio->setQuestionID($_REQUEST['qID']);
			$randomTravelBio->readTravelBio();
			$response = $randomTravelBio->setupWidget();		

			require_once 'JSON.php';			
			$parser = new Services_JSON(SERVICES_JSON_LOOSE_TYPE); 
  			header("Content-type:text/json");  
  			echo $parser->encode(array(
				'questionID' => $response['questionID'],
				'question' => $response['question'],
				'answer' => $response['answer']
			));
			
			return;
		case 'edit':
			
			$this->file_factory->registerClass('TravelBioUser', array( 'path' => 'travellog/model/travelbio'));
			$this->file_factory->registerClass('Answer', array('path' => 'travellog/model/travelbio'));						
			$travelBioUser = $this->file_factory->invokeStaticClass('TravelBioUser', 'getTravelBioUserByTravelerID', array($this->properties['travelerID']));

			if(is_null($travelBioUser)){
				$travelBioUser = $this->file_factory->getClass('TravelBioUser');
				$travelBioUser->setTravelerID($this->properties['travelerID']);
				$travelBioUser->Save();

			}

			require_once 'travellog/model/travelbio/Class.AnswerValue.php';
	
			$arPrefixes = array(
				'txt'=> AnswerValue::TEXT,
				'txa'=> AnswerValue::TEXTAREA,
				'chk'=> AnswerValue::CHECKBOX,
				'txtchk'=> AnswerValue::TEXT
			);

			//add the answers		
			$travelBioUser->clearAnswers();	
			$answerRepo = array();
			$withAns = false;
			$withOthers = false;
			foreach($_POST as $key => $val){
				$part = explode('_',$key);
				if(array_key_exists($part[0],$arPrefixes)){
					if((is_string($val) && strlen(trim($val))) || is_array($val)){					
						$answer = $this->file_factory->getClass('Answer');
						$answer->setProfileQuestionID($part[1]);
						$answer->setTravelBioUserID($travelBioUser->getTravelBioUserID());
						$val = (is_array($val))?$val:array($val);
						foreach($val as $iVal){
							if('Others'==$iVal){ $withOthers = true; }
							if( strlen(trim($iVal)) ){
								if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
									$withAns = true;
									$answer->addValue($arPrefixes[$part[0]],$iVal);
								}
							}
						}						
						if(!array_key_exists($part[1],$answerRepo)){
							if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
								$answerRepo[$part[1]] = $answer;
							}	
						}
						else{
							if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
								$answerRepo[$part[1]]->addValue($arPrefixes[$part[0]],$iVal);
							}
						}	
					}
				}
			}
			
			foreach($answerRepo as $iAns){
				$iAns->save();
			}

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($this->properties['travelerID'],1);


			$randomTravelBio = $this->file_factory->getClass('RandomTravelBioView');
			$randomTravelBio->setOwnerID($this->properties['travelerID']);
			$randomTravelBio->readTravelBio();
			$response = $randomTravelBio->setupWidget();		
			
			$json = "{
				questionID : '{$response['questionID']}',
				numQ : '{$response['numQ']}',
				percentDone : '{$response['percentDone']}'
			}";			
			
			echo $json;
			
			return;	
		}
	}
	
	private function handleAjaxMTP() {

		//TODO: not all cases needs this variable; in fact $travel is owverwritten in a few
		$travel = $this->composeTravelPlan();
		
		switch ($_REQUEST['action']) {
			
		case 'showAddForm':
			
			$template =	$this->file_factory->getClass('Template');		
			$template->set_path('travellog/views/');
			$template->set('countryList', $this->file_factory->invokeStaticClass('Country', 'getCountryList'));
			$template->set('travel', $travel);
			echo $template->fetch("tpl.FrmTravelPlansWidget.php");
			
			return;
			 	
		case 'showEditForm':

			$template =	$this->file_factory->getClass('Template');		
			$template->set_path('travellog/views/');
			$template->set('countryList', $this->file_factory->invokeStaticClass('Country', 'getCountryList'));
			$template->set('travel', $this->file_factory->invokeStaticClass('TravelSchedule', 'retrieveByPk', array($_REQUEST['travelWishID']))); 
			
			echo $template->fetch("tpl.FrmTravelPlansWidget.php");
			
			return;
				
		case 'delete':
			
			$this->file_factory->invokeStaticClass('TravelSchedule', 'doDelete', array($travel));
			
			$this->listItineraries($travel->getUserID());
			
			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($this->properties['travelerID'],3);

			return;

		case 'edit':
		case 'add':
		case 'save': //TODO: add and edit are currently handled by this branch			
			$travel->save();

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($this->properties['travelerID'],3);
			
			$this->listItineraries($travel->getUserID());
			
			return;

		case 'list':
			$this->listItineraries($travel->getUserID());
			
			return;			
			
		case 'editCountryList':
			$countryIDs = array();			
			if (isset($_POST['sel_countries'])) {
				foreach ($_POST['sel_countries'] as $countryID) {
					$countryIDs[] = $countryID;
				}
			}

			//TODO: use $travel to update instead of next line?
			$this->file_factory->invokeStaticClass('TravelSchedule', 'editCountriesToTravel', array($countryIDs, $this->properties['travelerID']));
			$countryList = $this->file_factory->invokeStaticClass('TravelSchedule', 'getCountriesToTravel', array($this->properties['travelerID']));
			$countries = $this->parseToXML(join(', ', $countryList['countryNames']));
			$numCountries = count($countryList['countryIDs']);
			
			$json = "{
				countries : '{$countries}',
				numCountries : '{$numCountries}'
			}";			
		
			echo $json;							
			
			return;

		case 'saveSettings':
			
			if (is_numeric($_REQUEST['inputSettings'])) {
				$this->file_factory->invokeStaticClass('TravelSchedule', 'editUserSettings', array(array(
					'travelerID' => $this->properties['travelerID'], 
					'travelPlansDisplayed' => $_REQUEST['inputSettings'],
					'travelPlansIncludePast' => isset($_REQUEST['inputPast']) ? 1 : 0)));
			}

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($this->properties['travelerID'],3);
			
			$this->listItineraries($travel->getUserID());
			
			return;
		}
	}
	
	//TODO: validation
	private function composeTravelPlan() {
		require_once('travellog/model/Class.TravelPlan.php');
		require_once('travellog/model/Class.TravelDestination.php');
		require_once('travellog/model/Class.Country.php');						
		
		$travel = new TravelPlan();
		$travel->setUserID($this->file_factory->invokeStaticClass('TravelSchedule', 'getUserIDWithTravelerID', array($this->properties['travelerID'])));
		
		if (isset($_POST['hdnTravelWishID'])) $travel->setTravelWishID($_POST['hdnTravelWishID']);
		else if (isset($_POST['travelWishID'])) $travel->setTravelWishID($_POST['travelWishID']);
		
		//TODO: replace with a more robust condition
		if (isset($_POST['title'])) {
			$travel->setTitle(trim($_POST['title']));
			$travel->setDetails(trim($_POST['details']));
	
			// transform destinations into objects
			$arrDestinations = array();
			
			//For AJAX requests
			$dests = $this->convertToArray($_POST['dest_descriptions'], true);
			$countries = $this->convertToArray($_POST['selCountries'], false);					
			
			for ($i=0, $l=count($countries); $i < $l; $i++) {
				$destination = new TravelDestination();
				
				$destination->setTravelWishID($travel->getTravelWishID());
				$destination->setCountry(new Country($countries[$i]));
				$destination->setDescription(trim($dests[$i]));
				
				// compose start date in 2007-01-01 format
				if (empty($_POST['start_years'])) {
					//TODO: this is just a test
					$destination->setStartDate('2009-01-01');					
				} else {
					$startDate = !$_POST['start_years'][$i] ? '000' : '';
					$startDate .= $_POST['start_years'][$i] . '-';
					$startDate .= strlen($_POST['start_months'][$i]) == 1 ? '0' : '';
					$startDate .= $_POST['start_months'][$i] . '-';
					$startDate .= strlen($_POST['start_days'][$i]) == 1 ? '0' : '';
					$startDate .= $_POST['start_days'][$i];
					
					$destination->setStartDate($startDate);
				}
					
				if (empty($_POST['end_years'])) {
					//TODO: this is just for testing
					$destination->setEndDate('2010-02-02');					
				} else {
					// compose end date in 2007-01-01 format
					$endDate = !$_POST['end_years'][$i] ? '000' : '';
					$endDate .= $_POST['end_years'][$i] . '-';
					$endDate .= strlen($_POST['end_months'][$i]) == 1 ? '0' : '';
					$endDate .= $_POST['end_months'][$i] . '-';
					$endDate .= strlen($_POST['end_days'][$i]) == 1 ? '0' : '';
					$endDate .= $_POST['end_days'][$i];
		
					$destination->setEndDate($endDate);
				}
					
				$arrDestinations[] = $destination;
			}
			
			$travel->setDestinations($arrDestinations);
		} else {//create a dummy travel item for add forms
			$travel->setDestinations(array(new TravelDestination()));
		}
			
		return $travel;
	}

	//TODO: refactor HAM
	private function listItineraries($userID) {

		$travelPlansUser = $this->file_factory->invokeStaticClass('TravelSchedule',	'getTravelPlansUserWithTravelerID', array($this->properties['travelerID']));
		$itemsToDisplaySetting = isset($travelPlansUser['travelPlansDisplayed']) ? $travelPlansUser['travelPlansDisplayed'] : 5;
		$includePastTravelsSetting = isset($travelPlansUser['travelPlansIncludePast']) ? $travelPlansUser['travelPlansIncludePast'] : false;
		
		//$travelItems = $this->file_factory->invokeStaticClass('TravelSchedule', 'getTravelItems', array($userID));
		$travelItems = $this->file_factory->invokeStaticClass('TravelSchedule', 'getOrderedTravels', array($userID));
		$pastTravels = $this->file_factory->invokeStaticClass('TravelSchedule', 'getPastTravels', array($userID));
		if ($includePastTravelsSetting) $travelItems = array_merge($travelItems, $pastTravels);
		
		$totalNumItems = count($travelItems);

		if (!(isset($_REQUEST['numItemsToDisplay']) && (strtolower($_REQUEST['numItemsToDisplay']) == 'all'))) {
			if ($totalNumItems > $itemsToDisplaySetting) {
				for ($i = 1, $j=$totalNumItems - $itemsToDisplaySetting; $i <= $j; $i++ ) {
					array_pop($travelItems);
				}
			}							
		}

		$template =	$this->file_factory->getClass('Template');		
		$template->set_path('travellog/views/');		
		$template->set('showMyTravelPlans', $totalNumItems != 0);
		$template->set('travelItems', $travelItems);
		$template->set('totalNumItems', $totalNumItems);
		$template->set('itemsToDisplaySetting', $itemsToDisplaySetting);
		$template->set('pastTravelsCount' , count($pastTravels));
		$template->set('includePastTravelsSetting', $includePastTravelsSetting);				
		$template->set('owner', true); 		
		
		// TRAVEL WISH LIST
		$template->set('countryList',         $this->file_factory->invokeStaticClass('Country',        'getCountryList'));
		$template->set('countriesInterested', $this->file_factory->invokeStaticClass('TravelSchedule', 'getCountriesToTravel', array($this->properties['travelerID'])));
			
		echo $template->fetch("tpl.ViewTravelPlansWidget.php");		
	}
	
  	private function convertToArray($commaDelimitedRequestVar, $hasSpecialDelimiters=false) {
  		if (is_array($commaDelimitedRequestVar)) return $commaDelimitedRequestVar;
  		
    	return $hasSpecialDelimiters ? explode('FixThisHack,', $commaDelimitedRequestVar) : explode(',', $commaDelimitedRequestVar);
  	}
  	
	private function parseToXML($htmlStr) {
  		$xmlStr = str_replace("&",'&amp;',$htmlStr);
	  	$xmlStr = str_replace('<','&lt;',$xmlStr);
	  	$xmlStr = str_replace('>','&gt;',$xmlStr);
	  	$xmlStr = str_replace('"','&quot;',$xmlStr);
	  	$xmlStr = str_replace("'",'&#39;',$xmlStr);
	  	
	  	return $xmlStr;
	}	
	static function requireFilesForMTM(){
		require_once("travellog/model/Class.LocationFactory.php");
		require_once 'travellog/model/socialApps/mytravelmap/gaOpenSocialTravelerMapper.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.TravelMapEntry.php';
        require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.MyTravelMapDAO.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCityLocation.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCountryLocation.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocation.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocationPeer.php';
		require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBNewCity.php';
	}
	
	private function invalidateMTMCache(){
		// invalidate countries travelled cache
		$cache = ganetCacheProvider::instance()->getCache();
		$cache->delete('Traveler_Countries_Traveled_' . $this->properties['travelerID']);
		
		//edit:10Feb2010 - invalidate cache if facebook user
		require_once 'travellog/model/socialApps/mytravelmap/Class.TravelMapUserPeer.php';
		if($mtmFbUser = TravelMapUserPeer::retrieveTravelMapUserByTravelerID($this->properties['travelerID'])) {
		    require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.MyTravelMapDAO.php';
		    $cache->delete(MyTravelMapDAO::getMtmUserCacheKey($mtmFbUser->getUserID()));
		}
	}
		
	/* 
	 * twitter widget
	 */ 
	private function prepareTwitterWidget(){
		$twitterWidget = $this->file_factory->getClass('TwitterWidget');
		$this->properties['template']->set('twitterWidget', $twitterWidget->render());
	}

	/* 
	 * facebook widget
	 */ 
	private function prepareFacebookWidget(){
		$facebookWidget = $this->file_factory->getClass('FacebookWidget');
		$this->properties['template']->set('facebookWidget', $facebookWidget->render());
	}
	
	private function handleAjaxTwitterWidget(){}

	private function handleAjaxFacebookWidget(){}	
}
?>