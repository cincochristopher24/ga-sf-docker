<?php
/*
	Author: Adelbert D. Silla
	Class.AbstractMyTravelJournalsController.php   
*/	
	require_once 'travellog/model/socialApps/mytraveljournals/Class.WebServiceUser.php';              
	require_once 'travellog/model/socialApps/mytraveljournals/Class.AbstractMyTravelJournal.php';  
	require_once 'Class.GaDateTime.php';
	require_once('travellog/model/Class.AdvisorGroupInfo.php');	
	require_once('travellog/helper/Class.EntryHelper.php');
	abstract class AbstractMyTravelJournalsController{
				
		public static $entryHelper;
		
		protected 
			$user,
			$helper,
			$domain;

		const			
			ADD_APP = "addApp",
			REMOVE_APP = 'removeapp',
			SYNCHRONIZE = 'synchronize',
			SHOW_JOURNALS = 'showJournals',
			UNSYNCHRONIZE = 'unSynchronize',
			INVITE_FRIENDS = 'invitefriends',
			SHOW_INDEX_PAGE = 'showIndexPage',
			CHANGE_SETTINGS = 'changeSettings',
			SHOW_SETTINGS_PAGE = 'showSettingsPage',
			SHOW_SYNCHRONIZE_FORM = 'showSynchronizeForm',

			FACEBOOK_DOMAIN = 'facebook.com',
			GOABROAD_DOTNET_URL = 'http://www.goabroad.net';
					
		function performAction() {
			if( !$this->user->getID() )
				$this->user->save();
			
			switch( $this->helper->getAction() ){
				case self::SYNCHRONIZE:
					$this->synchronize();
					break;
				case self::UNSYNCHRONIZE:
					$this->unSynchronize();
					break;
				case self::SHOW_SETTINGS_PAGE:
					if(AbstractMyTravelJournalApp::isSynchronized($this->user->getID()))
						$this->showSettingsPage();
					else 
						$this->showSyncForm();						
					break;
				case self::CHANGE_SETTINGS:
					$this->changeSettings();
					break;	
				case self::SHOW_SYNCHRONIZE_FORM:
						$this->showSyncForm();
					break;
				case self::SHOW_INDEX_PAGE:
					if( AbstractMyTravelJournalApp::isSynchronized($this->user->getID()) )
						$this->showJournals();					
				    else
				    	$this->showSyncForm();
					break;		
				case self::ADD_APP:
					$this->addAppUser();
					break;	
				case self::SHOW_JOURNALS:	
					$this->showJournals();
					break;
				case self::REMOVE_APP:	
					$this->removeApp();
					break;
			}
		
		}

		function synchronize() {
			$args = $this->helper->getSynchronizeArgs();

			$controllerArgs = array(
				'container'   => $this->domain, 
				'containerID' => $this->user->getWsID(), 
				'email' 	  => $args['username'], 
				'password' 	  => $args['password']
			);


			require_once 'travellog/controller/socialApps/Class.SynchronizeAppsController.php';  
			$synchronizeAppsController = new SynchronizeAppsController($controllerArgs);
			$result = $synchronizeAppsController->performAction('sync');

			if( is_object($result) ){
//				AbstractMyTravelJournalApp::addUser($result->getUserID());
				$this->user->setTravelerID($result->getTravelerID());
				$this->user->setID($result->getUserID());
				$showJournalTypeParams = array('userID'=>$this->user->getID(), 'appID'=>$this->user->getWsID(), 'showJournalType'=>'0');

				$myTravelJournalApp = new AbstractMyTravelJournalApp();
				$myTravelJournalApp->setUser($this->user);
				$myTravelJournalApp->updateShowJournalType($showJournalTypeParams);

				$this->showSettingsPage();

			}   
			else{
				$args['errorMsg'] = $result;
				$this->showSyncForm($args);
			}
		}

		function unSynchronize() {
			AbstractMyTravelJournalApp::deleteSelectedJournals($this->user->getID(),$this->user->getWsID());
			AbstractMyTravelJournalApp::removeJournalAppData($this->user->getID());
			$this->showSyncForm();
		}
		
		function changeSettings() {  

			$submittedInfo = $this->helper->getSubmittedInfo();
			$showJournalTypeParams = array('userID'=>$this->user->getID(), 'appID'=>$this->user->getWsID(), 'showJournalType'=>$submittedInfo['viewType']);
			$myTravelJournalApp = new AbstractMyTravelJournalApp;                 
			
			$myTravelJournalApp->setUser($this->user);			
			$myTravelJournalApp->updateShowJournalType($showJournalTypeParams);
			$myTravelJournalApp->setSelectedJournals($submittedInfo['travelIDs']);
			
			if(!$this->isFacebookDomain()){
				if($this->helper->getAppViewType() == 'home') $this->showSettingsPage();
				else $this->showJournals();
			} 
			else { $this->showJournals(); } 
		}
		
		function showSettingsPage(){
			$journals = AbstractMyTravelJournal::getUserJournals($this->user->getID(),$this->user->getDomain());

			$viewType0 = 'checked="checked"'; $viewType1 = '';
			$panelHeader0 = 'panel_header active'; $panelHeader1 = 'panel_header';
			$disabledStatus = 'disabled="true"';
			if(1 == AbstractMyTravelJournal::getShowJournalType($this->user->getID(),$this->user->getWsID()) ){
				$viewType0 = ''; $viewType1 = 'checked="checked"';				
				$panelHeader0 = 'panel_header'; $panelHeader1 = 'panel_header active';
				$disabledStatus = '';
			} 
			$args = array(
				'id'					 => $this->user->getID(),
				'userJournals'  		 => $journals,
				'viewerIsOwner'			 =>	$this->user->getWsID() == $this->helper->getAppViewerID(),
				'selectedJournal' 		 => AbstractMyTravelJournal::getUserSelectedJournals($this->user->getID(),$this->user->getWsID(),$this->domain),
				'isFacebookDomain'       => self::isFacebookDomain()
				
			);                                                      

			if(self::isFacebookDomain()) {
				$MainDashboard = new fbMainDashboard(4);    
			  	$settingsJS = htmlentities(file_get_contents('http://www.goabroad.net/facebook/mytraveljournals/js/settings.js', true));
				require_once('travellog/views/mytraveljournals/facebook_/tpl.mainView.php');
				require_once('travellog/views/mytraveljournals/facebook_/tpl.settings.php');			
			}				
			else{
				$appViewType = $this->helper->getAppViewType();
				require_once 'travellog/views/mytraveljournals/opensocial/tpl.FrmTravelJournalSettings.php';
			} 
		}
		
		// viewing functions
		
		function showSyncForm(array $args = array()){
						
				$args = array(
					'viewerIsOwner'	=> $this->user->getWsID() == $this->helper->getAppViewerID(),
					'username'	=> isset($args['username']) ? trim($args['username']) : '',
					'password'	=> '',
					'errorMsg'	=> isset($args['errorMsg']) ? trim($args['errorMsg']) : '',
					'ownerID'	=> $this->user->getWsID(),
					'ownerName' => $this->user->getDotNetUsername(),
					'wsName'	=> explode('.',$this->domain),
					'sync' 		=> AbstractMyTravelJournalApp::isSynchronized($this->user->getID()),
					'hasApp'	=> AbstractMyTravelJournalApp::hasMyTravelJournalApp($this->user->getWsID(),$this->domain),
					'dotNetEmail'=> $this->user->getDotNetEmail()
				);
		 	
			if(!$this->isFacebookDomain()) {				
				$appViewType = $this->helper->getAppViewType();
				require_once 'travellog/views/mytraveljournals/opensocial/tpl.FrmSync.php';
			}
			else {
				$MainDashboard = new fbMainDashboard(3);
				require_once('travellog/views/mytraveljournals/facebook_/tpl.mainView.php');
				require_once('travellog/views/mytraveljournals/facebook_/tpl.fbSync.php');
			} 
		}

		
		function showUnsyncForm(){
			$args = array(
				'ownerName' => $this->user->getDotNetUsername(),
				'dotNetEmail'=> $this->user->getDotNetEmail()
			);
			require_once 'travellog/views/mytraveljournals/opensocial/tpl.FrmUnsync.php';
		}
		
		// show journals page
		function showJournals(){
			if(0 == count(AbstractMyTravelJournal::getUserSelectedJournals($this->user->getID(),$this->user->getWsID(),$this->domain)) && AbstractMyTravelJournal::getShowJournalType($this->user->getID(), $this->user->getWsID()) == 1)
				$this->showSettingsPage();
			else {
				$dateTimeTool = new GaDateTime;
				$journals = AbstractMyTravelJournal::getShowJournalType($this->user->getID(), $this->user->getWsID()) == 0 
					? AbstractMyTravelJournal::getRecentUpdateJournal($this->user->getID(),$this->domain)
					: AbstractMyTravelJournal::getUserSelectedJournals($this->user->getID(),$this->user->getWsID(),$this->domain);

				$args = array(
					'name'					=> $this->helper->getOwnerName(),
					'journals'				=> $journals,
					'viewerIsOwner'			=> $this->user->getWsID() == $this->helper->getAppViewerID(),
					'hasApp'				=> AbstractMyTravelJournalApp::hasMyTravelJournalApp($this->user->getWsID(),$this->domain),
					'ownerDotNetUsername'	=> $this->user->getDotNetUsername(),
					'dotNetEmail'			=> $this->user->getDotNetEmail(),
					'userID'				=> $this->user->getWsID(),
					'domain'				=> $this->domain
				);	
				if(!$this->isFacebookDomain()){
					$appviewType = $this->helper->getAppViewType();
					require_once('travellog/views/mytraveljournals/opensocial/tpl.ShowJournalsInProfile.php');
				}				
				else{
					$myTravelJournalApp = new fbMyTravelJournalApp;
					$MainDashboard = new fbMainDashboard(1);
					$isTabView = $this->helper->getIsTabView();
					require_once('travellog/views/mytraveljournals/facebook_/tpl.mainView.php');
					require_once('travellog/views/mytraveljournals/facebook_/tpl.profile.php');
				}
			}	
			
		}                          
		
		public function removeApp() {
			AbstractMyTravelJournalApp::deleteSelectedJournals($this->user->getID(),$this->user->getWsID());			
			AbstractMyTravelJournalApp::removeJournalAppData($this->user->getID());
		}

		public function addAppUser(){
			if( 0 == $this->user->getID() )
				$this->user->setID( WebServiceUser::addUser($this->user->getWsID(), $this->domain) );
			if( !$this->user->hasApp(WebServiceIni::MY_TRAVEL_JOURNALS) )	
				AbstractMyTravelJournalApp::addUser($this->user->getID());
		}
								
		static function getValidActions(){
			return array(
				self::SYNCHRONIZE,
				self::UNSYNCHRONIZE,
				self::SHOW_SETTINGS_PAGE,
				self::CHANGE_SETTINGS,
				self::SHOW_INDEX_PAGE,
				self::ADD_APP,
				self::SHOW_JOURNALS,
				self::SHOW_SYNCHRONIZE_FORM,
				self::INVITE_FRIENDS,
				self::REMOVE_APP
			);
		}

		function isFacebookDomain(){
			return $this->domain == self::FACEBOOK_DOMAIN ? true : false;
		}		   		
		
		static function getLink(TravelLog $travelLog){
			return self::GOABROAD_DOTNET_URL.self::$entryHelper->getLink($travelLog);
		}
		
		static function getNewImageSize($image){                                           
			$width = '90px'; $height = '60px';                                             			
			list($imageWidth, $imageHeight, $imageType, $imageAttr) = getimagesize($image);
			if($imageHeight > $imageWidth) { $width = '60px'; $height = '90px'; }

			return array($width, $height);
		}
	} 
?>
