<?php
require_once('travellog/model/travelbio_opensocial/Class.ProfileQuestionOs.php');
require_once('travellog/model/travelbio_opensocial/Class.TravelBioUserOs.php');
require_once('travellog/model/travelbio_opensocial/Class.AnswerValueOs.php');
require_once('travellog/model/travelbio_opensocial/Class.AnswerOs.php');
require_once('ganet_web_services/model/Class.WebServiceUser.php');
require_once('travellog/controller/travelbio_os/Class.mtbDashboard.php');
require_once('Class.Template.php');

class MyTravelBioController{
	
	private $action,
			$container,
			$id,
			$isOwner,
			$view,
			$travelBio,
			$activeTab,
			$serviceUser;
	
	const
		TRAVELBIO 	= 'showBio',
		SYNC		= 'doSync',
		SYNCHRONIZE	= 'synchronize',
		EDITBIO		= 'edit',
		SAVEBIO		= 'saveBio',
		SETTINGS	= 'settings',
		KEEP_DOTNET	= 'keepDotNet',
		KEEP_OS		= 'keepOpenSocial',
		SAVESETTING	= 'saveSettings',
		NONE_OWNER	= 'friend_viewer';
		
		
		$this->setServiceUserObj();
		
	//	$this->serviceUser = new WebServiceuser($this->id,$this->container);
	}
	
	function setServiceUserObj(){
		$this->serviceUser = new WebServiceuser($this->id,$this->container);
		$this->travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
		
		if($this->isOwner == "true" || $this->isOwner == 1){
			$this->activeTab = true;
		}
		else {
			$this->activeTab = false;
		}
	}
	
	function performAction(){

		if( 0 == $this->serviceUser->getID()){
			$this->serviceUser->setID( WebServiceUser::addUser($this->serviceUser->getWsID(), $this->container) );
			$this->setServiceUserObj();
		}
		$x = $this->serviceUser->hasApp('myTravelBio');
	//	echo "has app? = ".$x;
		if(!$this->serviceUser->hasApp('myTravelBio')){
			MyTravelBioApp::addUser($this->serviceUser->getID());
			$this->setServiceUserObj();
		}
		
		$_isSync = WebServiceUser::isSynchronized($this->id,$this->container);

		if($this->isOwner == "false"){
			$this->action = "friend_viewer";
		}
		
		if($this->view == "profile" || $this->view == "profile.right" || $this->view == "home"){
		//	echo $this->view."<br/>";
		//	echo "this is profile";
			$this->activeTab = false;
			
			if(!$this->travelBio->getAnswers()){
				$this->action = "";
			}
			
			
		}
		
	//	if(!$this->id){
	//		require_once('Class.Toolman.php');
	//		Toolman::redirect('http://www.myspace.com/');
	//	}
	//	var_dump($this->activeTab);
			switch($this->action){
				case self::TRAVELBIO:
					if($_isSync){
						if(!MyTravelBioApp::hasTravelBioID($this->serviceUser->getID())){
							MyTravelBioApp::updateTravelBioUserOS($this->serviceUser->getTravelerID(),$this->serviceUser->getID());
						}
						$this->setServiceUserObj();
					//	if(isset($_POST['viewPage']) && $_POST['viewPage']=="home"){
					//		$this->showAppOwnerInfo();
					//		$html ="<div>
					//				<img src='http://www.goabroad.net/opensocial/myTraveBio/images/screenshot.png'/>
					//				</div>";
					//		echo $html;
					//	}
					//	else {
					//		$this->showIndex();
					//	}
					}
					//else {
					if($this->travelBio->getAnswers()){
						if($this->view == "home") $this->showAppOwnerInfo();
						elseif($this->view == "profile.right") $this->showIndex();
						elseif($this->view == "profile") $this->showIndex();
						else {
						//	echo $this->activeTab;
							if($this->activeTab){
								$dash = new mtbDashboard(1);
							}
							$this->showIndex();
						} 
					}
					else {
						$this->showEditBioForm();
					}
					//	$this->showSyncOrImage();
						
					//}
					break;
				case self::SYNCHRONIZE:
					$this->showSyncForm();
					break;
				case self::SYNC:
					$this->synchronizeUser();
					break;
				case self::SETTINGS:
					$this->showSettingsForm();
					break;
				case self::SAVESETTING:
					$this->saveBioSettings();
					break;
				case self::EDITBIO:
					$this->showEditBioForm();
					break;
				case self::SAVEBIO:
					$this->saveBio();
					break;
				case self::KEEP_DOTNET:
					$this->syncWithDotNetBio();
					break;
				case self::KEEP_OS:
					$this->syncWithOpenSocialBio();
					break;
				case self::NONE_OWNER:
					//if($this->travelBio->getAnswers()){
						$this->showIndex();
					//}
					//else {
						
					//}
					break;
				default:
					$this->showDefaultNoneSynchronizeProfile();
					break;
			} 
			
	}
	
	function showIndex(){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
		
		$structAns = array();
		$answers = $travelBio->getAnswers();

		foreach($answers as $iAns){
			if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
				$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
			}
		}
		
		$viewPage = isset($_POST['viewPage']) ? $_POST['viewPage'] : null;
		$ownerName = isset($_POST['ownerName']) ? $_POST['ownerName'] : null;
		
		$profileQuestions = ProfileQuestionOs::getQuestions();
		$tpl = new Template();
		$tpl->setPath('travellog/views/travelbio_os/');
		$tpl->set('travelBio',$travelBio);
		$tpl->set('profileQuestions',$profileQuestions);
		$tpl->set('isOwner',$this->isOwner);
		$tpl->set('ownerName',$ownerName);
		$tpl->set('viewPage',$viewPage);
		$tpl->set('structAns',$structAns);
		$tpl->out('tpl.mytravelbio.php');
	}
	
	function showSyncForm($param = array()){
		$tpl = new Template();
		
		if($this->activeTab){
			$dash = new mtbDashboard(3);
		}
		
		$tpl->setPath('travellog/views/travelbio_os/');
		
		if(WebServiceUser::isSynchronized($this->id,$this->container)){
			$tpl->set('dotNetUsername',$this->serviceUser->getDotNetUsername());
		}
		else {
			$tpl->set('dotNetUsername','');
		}
		
		//$tpl->set('message',$message);
		$tpl->set('param',$param);
		$tpl->set('container',$this->container);
		$tpl->out('tpl.SyncForm.php');
	}
	
	function showSettingsForm($message = ""){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
		
		if($this->activeTab){
			$dash = new mtbDashboard(4);
		}
		
		$profileQuestions = ProfileQuestionOs::getQuestions();
		$tpl = new Template();
		$tpl->setPath('travellog/views/travelbio_os/');
		$tpl->set('profileQuestions',$profileQuestions);
		$tpl->set('travelBio',$travelBio);
		$tpl->set('message',$message);
		$tpl->out('tpl.Settings.php');
	}
	
	function showEditBioForm($message = ""){
	//	var_dump($this->activeTab);
		if($this->activeTab){
	//		echo "bakit";
			$dash = new mtbDashboard(2);
		}	
		
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());

		$structAns = array();
		$answers = $travelBio->getAnswers();

		foreach($answers as $iAns){
			if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
				$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
			}
		}
		$profileQuestions = ProfileQuestionOs::getQuestions();
		

		$tpl = new Template();
		$tpl->setPath('travellog/views/travelbio_os/');
		$tpl->set('profileQuestions',$profileQuestions);
	//	$tpl->set('css',file_get_contents('http://www.goabroad.net/css/travelbio/fbedit.css'));
		$tpl->set('structAns',$structAns);
		$tpl->set('message', $message);
		$tpl->out('tpl.edit.php');
	}
	
	function synchronizeUser(){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
			
		$email = isset($_POST['email']) ? $_POST['email'] : "";
		$pass  = isset($_POST['pass']) ? $_POST['pass'] : "";
		$error = "";
		$reSync = false;
		if( (!empty($email)) && (!empty($pass)) ):
		
			$userInfo = array(
						"oldUserID" => $this->serviceUser->getID(),
						"bioUserID" => $travelBio->getTravelBioUserID(),
						"domain"    => $this->serviceUser->getDomain(),
						"osUserID"  => $this->serviceUser->getWsID()
						);

			$newUser = $this->serviceUser->validateGAAccountWithEmail($email, $pass);
			if(isset($newUser['travelerID']) && (0 < $newUser['travelerID']) ){
				$newUser['newBioUserId'] = $travelBio->getDotnetBioUserIdByTravelerID($newUser['travelerID']);
				
				// deprecated
				if(is_array($newUser) && ($newUser['isSuspended'] == 0) && ($newUser['isAdvisor'] == NULL)){

					// Added by: Bert
					// Date: May 16, 2009
					// Purpose: Check first if .net user account was already sync with the other user.
					// IF true then display message ELSE proceed to sync.

					// deprecated
					if(TravelBioUserOs::isGanetAccountAlreadySync($this->serviceUser->getDomain(),$newUser['travelerID'])){
						$error = "GoAbroad.net account was already synchronized with the other user.";						
					}				  

				else {
						// deprecated
						if(WebServiceUser::isSynchronized($this->serviceUser->getWsID(),$this->serviceUser->getDomain())){

							// deprecated
							$this->serviceUser->reSynchronize($newUser,$userInfo);

							// needed function
							TravelBioUserOs::reSyncOsUpdate($newUser,$userInfo);

							$reSync = true;
							$message = "Your accounts have been successfully synchronized!";
						}
						else {

							//just to double check that the user is existing in the tblTravelBioUserOs
							$user_exist = TravelBioUserOs::isUserExisting($newUser['userID']);
						//	var_dump($user_exist);
							if(!$user_exist){

								MyTravelBioApp::createOsUser($newUser);
							}
						//	var_dump($newUser);
						//	var_dump($userInfo); exit;
							$dotNetBio = TravelBioUserOs::getBioUserIdByServiceUserID($newUser['userID']);


						//	var_dump($dotNetBio);
							$open_answers = $this->travelBio->getAnswers();
							$dotnet_answers = $dotNetBio->getAnswers();
							if($open_answers && $dotnet_answers){

								if($this->activeTab){
									$dash = new mtbDashboard(3);
								}
							//	echo "both have answers";
								$tpl = new Template();
								$tpl->setPath('travellog/views/travelbio_os/');
								$tpl->set('travelerID', $newUser['travelerID']);
								$tpl->set('container',$this->container);
								$tpl->out('tpl.dualAccount.php');
							}
							elseif((!$dotnet_answers) && $open_answers ) {

							//	echo "no dotnet answers";
								$travelBio->delete();
								$travelBio->setTravelBioUserID($newUser['newBioUserId']);
								$travelBio->updateAnswers($userInfo['bioUserID']);

								$this->serviceUser->firstSynchronize($newUser,$userInfo);
								TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);
								$this->setServiceUserObj();
								$this->showSettingsForm();

							}
							else {
							//	echo "default";
								$travelBio->delete();
								$this->serviceUser->firstSynchronize($newUser,$userInfo);
								TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);
								$this->setServiceUserObj();
								$this->showSettingsForm();
							} 
							// 35787
							// 414094831							
						}											
					}
				}
				else{
					$error = "Sorry, process cannot be completed because the account that you are tryng to synchronize with ";
					if(0 < $newUser['isAdvisor']){
						$error .= "is an advisor account. Only traveler account is allowed!";
					}
					elseif($newUser['isSuspended'] == 1){
						$error .= "is suspended. Please try another account or contact Goabroad Network now.";
					}
					
				}
				
			}
			else{
				// overwrite error message
				$error = "No match found! Try again!";
			}
			
		
		endif;

		if( ($reSync) || ($error != "") ){
			$this->setServiceUserObj();
			if($error != ""){
				$param['error'] = $error;
				$this->showSyncForm($param);
			}
			elseif($message != ""){
				$param['message'] = $message;
				$this->showSyncForm($param);
			}
			
			
		}
		
		
	}
	
	function syncWithDotNetBio(){
		$travelerID = isset($_POST['travelerID']) ? $_POST['travelerID'] : 0;
		if(0 < $travelerID){
			$userInfo = array(
						"oldUserID" => $this->serviceUser->getID(),
						"bioUserID" => $this->travelBio->getTravelBioUserID(),
						"domain"    => $this->serviceUser->getDomain(),
						"osUserID"  => $this->serviceUser->getWsID()
						);
			$newUser['travelerID'] = $travelerID;
			$newUser['userID'] = $this->serviceUser->getWebServiceUserByTravelerID($travelerID);
			$newUser['newBioUserId'] = TravelBioUserOs::getDotnetBioUserIdByTravelerID($travelerID);
			
			$dotnet = TravelBioUserOs::getBioUserIdByServiceUserID($newUser['userID']);
			$opensocial = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
			
			$opensocial->delete();
			$opensocial->clearAnswers();

			$this->serviceUser->firstSynchronize($newUser,$userInfo);
			TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);
			$this->setServiceUserObj();
			$this->showSettingsForm();
			
		}
	}
	
	function syncWithOpenSocialBio(){
		$travelerID = isset($_POST['travelerID']) ? $_POST['travelerID'] : 0;
		if(0 < $travelerID){
			$userInfo = array(
						"oldUserID" => $this->serviceUser->getID(),
						"bioUserID" => $this->travelBio->getTravelBioUserID(),
						"domain"    => $this->serviceUser->getDomain(),
						"osUserID"  => $this->serviceUser->getWsID()
						);
			$newUser['travelerID'] = $travelerID;
			$newUser['userID'] = $this->serviceUser->getWebServiceUserByTravelerID($travelerID);
			$newUser['newBioUserId'] = TravelBioUserOs::getDotnetBioUserIdByTravelerID($travelerID);
			
			$dotnet = TravelBioUserOs::getBioUserIdByServiceUserID($newUser['userID']);
			$opensocial = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
			
			$opensocial->delete();
			$dotnet->clearAnswers();

			$opensocial->setTravelBioUserID($newUser['newBioUserId']);
			$opensocial->updateAnswers($userInfo['bioUserID']);

			$this->serviceUser->firstSynchronize($newUser,$userInfo);
			TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);

			$this->setServiceUserObj();
			$this->showSettingsForm();
		}
	}
	
	function saveBioSettings(){
		//echo "save settings";
		
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
		$profileQuestions = ProfileQuestionOs::getQuestions();
		if($_POST['numQuestionToDisplay'] == 'ALL'){
			$numQ = count($profileQuestions);
		}
		else{
			$numQ = $_POST['numQuestionToDisplay'];
		}
		//echo $numQ;
		//update the default profile box fbml of the current application user
		$travelBio->setNumOfQuestionsDisplayed($numQ);

		$travelBio->save();
		
		$message = "Changes to your settings have been successfully saved!";
		
		$this->showSettingsForm($message);
	}
	
	function saveBio(){
		//echo "save bio";
		
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());

		$arPrefixes = array(
			'txt'=>AnswerValueOs::TEXT,
			'txa'=>AnswerValueOs::TEXTAREA,
			'chk'=>AnswerValueOs::CHECKBOX,
			'txtchk'=>AnswerValueOs::TEXT
		);

		$travelBio->clearAnswers();

		$answerRepo = array();
		$withAns = false;
		$withOthers = false;

		foreach($_POST as $key => $val):

			$part = explode('_',$key);


			if(array_key_exists($part[0],$arPrefixes)):

				$answer = new AnswerOs();
				$answer->setProfileQuestionID($part[1]);
				$answer->setTravelBioUserID($travelBio->getTravelBioUserID());

				if( 'chk'==$part[0]){
					$val = explode(',',$val);
				}
				else {
					$val = array($val);
				}
				
				foreach($val as $iVal):
					if('Others'==$iVal){
						$withOthers = true;
					}
					if( strlen($iVal) ){
						if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
							$withAns = true;
							$answer->addValue($arPrefixes[$part[0]],$iVal);
						}
					}

				endforeach;

				if(!array_key_exists($part[1],$answerRepo)){
					$answerRepo[$part[1]] = $answer;
				}
				else{
					if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
						$answerRepo[$part[1]]->addValue($arPrefixes[$part[0]],$iVal);
					}
				}

			endif;

		endforeach;

		foreach($answerRepo as $iAns){
			if( 0 < count($iAns->getAddedValue()) ){
				$iAns->save();
			}
		}
		
		$message = "Changes successfully saved!";
		$this->showEditBioForm($message);
	}
	
	function showAppOwnerInfo(){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());

		$answers = $travelBio->getAnswers();
	//	echo count($answers);
		
		$html = "<div class='content' style='font-size: 16px;color:#444;'><p><strong>".$_POST['ownerName']."</strong> has answered <em>".count($answers)."</em> ";
		$html .="out of 16 items in <a href='javascript:void(0);' onclick='gotoCanvas();'>Travel Bio</a></p>";
		$html .="<p>If you want to edit any of your answers please go to <em>Update Bio</em> in the <a href='javascript:void(0);' onclick='gotoCanvas()'>CANVAS</a> page.</p></div>";
	//	var_dump($this->serviceUser);
	//	$this->serviceUser->getDotNetUsername();
	//	$_POST['ownerName'];
	/*	$html = "<div style='height:100px;color: #444;'>
				<img src='http://www.goabroad.net/opensocial/myTraveBio/images/screenshot2.png'/>
				<p style='font-weight:bold;padding:5px;'>This app is developed for travelers. Just a simple question and answer application.</p></div>";
	*/	echo $html;
	}
	
	function showSyncOrImage(){
		$viewPage = isset($_POST['viewPage']) ? $_POST['viewPage'] : "";
		if($viewPage == "canvas"){
			$this->showSyncForm();
		}
		else{
			$html = "<div>
					<img src='http://www.goabroad.net/opensocial/myTraveBio/images/screenshot.png'/>";
			$html .="<p>Please sync your account to your GoAbroad Network account <a href='javascript:void(0);' onclick='gotoCanvas();'>here</a>.</p>
					</div>";
			echo $html;
		}
	}
	
	function showDefaultNoneSynchronizeProfile(){
		$html = "<div>";
		$html .="<p style='font-weight:bold;font-size:16px;'>You have not answered your Travel Bio yet. When you're ready to answer, just go to the <a href='javascript:void(0);' onclick='gotoCanvas();'>canvas page</a> of your Travel Bio.</p>
				</div>";
		echo $html;
	}
	
}
?>
