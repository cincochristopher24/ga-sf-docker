<?php
require_once('Class.Template.php');

class mtbDashboard{
	
	private $selected_tab;
	
	function __construct($param = 0){
		if(0 < $param){
			$this->selected_tab = $param;
			$this->selectTab();
		}
	}
	
	function selectTab(){
		$tpl = new Template();
		$tpl->setPath('travellog/views/travelbio_os/');
		$tpl->set('selected_tab',$this->selected_tab);
		$tpl->out('tpl.Dashboard.php');
	}
}
?>