<?php
class TravelItemController extends AbstractController {

  private $controlVars;
  private $userCredentials;
  private $dao;

  function __construct($controlVars) {
    $this->dao = new TravelScheduleOS(array('container' => $controlVars['container'], 'containerID' => $controlVars['containerID']));
    $this->userCredentials = $this->dao->getUserCredentials();
    $this->controlVars = $controlVars;
  }

  function applyInputToModel() {
    $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'default';

    if ('save' == $action) {
      if (isset($_POST['travelItemID']) && !empty($_POST['travelItemID']) && $_POST['travelItemID'] != 0) $action = 'editTravelItem';
      else $action = 'addTravelItem';
    }
    if (method_exists($this, $action.'Action')) {
      return call_user_func(array('TravelItemController', $action.'Action'));
    } else {
      throw new Exception("Method is undefined.");
    }
  }
  /*****************************************************************************
   * ACTIONS
   *****************************************************************************/
  private function defaultAction() {

	if($this->controlVars['appViewType'] == 'home'){
		return $this->showSettingsAction();
	}   
	else {

		$travelerID = $this->userCredentials['travelerID'];
	    $wishList = !empty($travelerID) ? TravelScheduleOS::getCountriesToTravel($travelerID) : null;
		
	    $friendsCountryIDs = $this->controlVars['friends'] ? $this->dao->getFriendsCountryIDs($this->convertToArray($this->controlVars['friends'])) : array();
		$onGoingAndNoDatesTravels = TravelScheduleOS::getOrderedTravels($this->userCredentials['userID']);
		$pastTravels = TravelScheduleOS::getPastTravels($this->userCredentials['userID']);
		$includePastTravelsSetting = (bool)$this->userCredentials['travelPlansIncludePast'];
		$travelItems = $includePastTravelsSetting ? array_merge($onGoingAndNoDatesTravels, $pastTravels) : $onGoingAndNoDatesTravels;

	    $mav = new ModelAndView($this->controlVars['viewDir'].'mytravelplans.php');
	    $mav->setModelValue('wishList', $wishList);
	    $mav->setModelValue('travelItems', $travelItems);
	    $mav->setModelValue('showWishList', !empty($travelerID)); 
	    $mav->setModelValue('friendsCountryIDs', $friendsCountryIDs);
	    $mav->setModelValue('user', $this->userCredentials['userID']);
	    $mav->setModelValue('isOwner', $this->controlVars['isOwner']);
	    $mav->setModelValue('countryList', $this->dao->getCountryList()); 
	    $mav->setModelValue('appViewType', $this->controlVars['appViewType']);
    	$mav->setModelValue('containerID', $this->controlVars['containerID']);
	    $mav->setModelValue('numPresentTravels', count($onGoingAndNoDatesTravels));
	    $mav->setModelValue('includePastTravelsSetting', $includePastTravelsSetting);
    	$mav->setModelValue('container', explode('.',trim($this->controlVars['container'])));
	    $mav->setModelValue('itemsToDisplaySetting', $this->userCredentials['travelPlansDisplayed']);
	    $mav->setModelValue('imgBaseUrl', 'http://www.goabroad.net/facebook/mytravelplans/images/');

	    return $mav;		
	}

  }

  private function showAddFormAction() {
    // set parameters to empty/default
    $formParams = array();
    $formParams['action'] = 'add';
    $formParams['title'] = '';
    $formParams['details'] = '';
    $formParams['fromMonth'] = '';
    $formParams['fromDay'] = '';
    $formParams['fromYear'] = '';
    $formParams['toMonth'] = '';
    $formParams['toDay'] = '';
    $formParams['toYear'] = '';
    $formParams['countries'] = array();

    return $this->setupForm($formParams);
  }

  private function showEditFormAction() {
  	return $this->setupForm($this->initEditForm());
  }

  private function addTravelItemAction() {
    TravelScheduleOS::doInsert($this->composeTravelItem(), true);
    return $this->defaultAction();
  }

  private function deleteTravelItemAction() {
    if (TravelScheduleOS::isValid(trim($_GET['travelItemID']), $this->userCredentials['userID'])) {
	  $travelItem = TravelScheduleOS::retrieveByPk(trim($_GET['travelItemID']));
      TravelScheduleOS::doDelete($travelItem);
    }
    return $this->defaultAction();
  }

  private function editTravelItemAction() {
    TravelScheduleOS::doUpdate($this->composeTravelItem());

    return $this->defaultAction();
  }

  private function deleteTravelDestinationAction() {
	$travelWishID = $_REQUEST['travelItemID'];
  	if (TravelScheduleOS::isValid($travelWishID, $this->userCredentials['userID'])) {
	  TravelDestinationPeer::deleteDestination($travelWishID,$_REQUEST['countryID']);  	 
      $destinations = TravelDestinationPeer::getTravelDestinations($travelWishID,null);

      $dateFromArray = array();
      $dateToArray = array();
      foreach ($destinations as $dest) {
        $dateFromArray[] = $dest->getStartDate();
        $dateToArray[] = $dest->getEndDate();
      }
      //update the date fields in the associated travel item of the deleted travel destination
      $this->dao->updateDatesOfTravelItem($_REQUEST['travelItemID'], $dateFromArray, $dateToArray);
  	}
  	return $this->defaultAction();
  }

  private function editTravelWishListAction() {

    $wishList = array();
    if ($travelerID = $this->userCredentials['travelerID']) {
	  TravelScheduleOS::editCountriesToTravel($this->convertToArray($_POST['wishList']), $travelerID);
	  $wishList = TravelScheduleOS::getCountriesToTravel($travelerID);	  
    }

    $countries = implode(', ', $wishList['countryNames']);

    $mav = new ModelAndView($this->controlVars['viewDir'].'wishList.php');
    $mav->setModelValue('countries', $countries);
    $mav->setModelValue('wishCount', count($wishList));

    return $mav;

  }

  public function showSynchronizeFormAction() {

    $mav = new ModelAndView($this->controlVars['viewDir'].'synchronize.php');

    $mav->setModelValue('user', $this->userCredentials['userID']);
    $mav->setModelValue('ganetUserEmail', $this->userCredentials['email']);
    $mav->setModelValue('ganetUserName', $this->userCredentials['username']);

    $mav->setModelValue('container', explode('.',trim($this->controlVars['container'])));

	
    return $mav;
  }

  public function doSyncAction() {
	$controllerArgs = array( 
		'email' 	  => $_REQUEST['email'],
		'password'    => $_REQUEST['password'],
		'container'   => $_REQUEST['container'], 
		'containerID' => $_REQUEST['uid']
	);

	require_once 'travellog/controller/socialApps/Class.SynchronizeAppsController.php';  
	$synchronizeAppsController = new SynchronizeAppsController($controllerArgs);
	$status = $synchronizeAppsController->performAction('sync');
    
    $mav = new ModelAndView($this->controlVars['viewDir'].'doSync.php');
  	$mav->setModelValue('status', is_object($status) ? 1 : $status);

    return $mav;
  }

  public function showDestinationsAction() {
	$persons = array();
	$urls = $this->convertToArray($_REQUEST['urls']);
	$names = $this->convertToArray($_REQUEST['names']);
	$photos = $this->convertToArray($_REQUEST['photos']);
	$friends = $this->convertToArray($_REQUEST['friends']);	
    for($i=0; $i<count($friends); $i++){
		$persons[$friends[$i]] = array( 
			'url' => $urls[$i], 
			'name' => $names[$i], 
			'photo' => $photos[$i]
	    ); 
	}	
    $friends[] = $_REQUEST['uid'];	
	$destinations = $this->dao->getDestinationsWithCountryID($friends, $_REQUEST['countryID']);
  	$mav = new ModelAndView($this->controlVars['viewDir'].'dests.php');
  	$mav->setModelValue('persons', $persons);
  	$mav->setModelValue('destinations', $destinations);
  	$mav->setModelValue('ownName', $_REQUEST['ownName']);
  	$mav->setModelValue('ownPhoto', $_REQUEST['ownPhoto']);
    $mav->setModelValue('containerID', $_REQUEST['uid']);

  	return $mav;
  }

	// added by bert
	function showSettingsAction(){
	    $onGoingTravelsCount = count(TravelScheduleOS::getOrderedTravels($this->userCredentials['userID']));

		$pastTravelsCount = count(TravelScheduleOS::getPastTravels($this->userCredentials['userID']));

	    $mav = new ModelAndView($this->controlVars['viewDir'].'settings.php');

	    $mav->setModelValue('user', $this->userCredentials['userID']);
	    $mav->setModelValue('isOwner', $this->controlVars['isOwner']);
	    $mav->setModelValue('appViewType', $this->controlVars['appViewType']);
	    $mav->setModelValue('pastTravelsCount', $pastTravelsCount);
	    $mav->setModelValue('onGoingTravelsCount', $onGoingTravelsCount);
	    $mav->setModelValue('itemsToDisplaySetting', $this->userCredentials['travelPlansDisplayed']);
	    $mav->setModelValue('includePastTravelsSetting', $this->userCredentials['travelPlansIncludePast']);

	    return $mav;		
	}

	function saveSettingsAction(){
		TravelScheduleOS::editUserSettings( array(
			'travelPlansDisplayed'	 => $_REQUEST['displayTravelPlans'],
			'travelPlansIncludePast' => $_REQUEST['includePastTravel'], 
			'travelerID'			 => $this->userCredentials['travelerID'])
		);
	}

  // END ACTIONS //////////////////////////////////////////////////////////////


  /***************************************************************************
   * MISC FUNCTIONS
   ***************************************************************************/

  private function initEditForm() {
    // display form with existing travel item data
	// checked
    // prev code $travelItem = $this->dao->getTravelItem($_REQUEST['travelItemID']);
    $travelItem = TravelScheduleOS::retrieveByPk($_REQUEST['travelItemID']);

    $formParams = array();

    $formParams['action'] = 'edit';
    $formParams['travelItemID'] = $_REQUEST['travelItemID'];
    $formParams['title'] = $travelItem->getTitle();
    $formParams['details'] = $travelItem->getDetails();

    list($fromYear, $fromMonth, $fromDay) = explode('-', $travelItem->getDateFrom());
    //FIX-ME: day and month are optional
    if (checkdate($fromMonth, $fromDay, $fromYear)) {
      $formParams['fromMonth'] = $fromMonth;
      $formParams['fromDay'] = $fromDay;
      $formParams['fromYear'] = $fromYear;
    } else {
      $formParams['fromMonth'] = '';
      $formParams['fromDay'] = '';
      $formParams['fromYear'] = '';
    }
    list($toYear, $toMonth, $toDay) = explode('-', $travelItem->getDateTo());
    $formParams['toMonth'] = $toMonth;
    $formParams['toDay'] = $toDay;
    $formParams['toYear'] = $toYear;

    $formParams['destinations'] = $travelItem->getDestinations();

    return $formParams;
  }

  private function setupForm($formParams) {
  	$mav = new ModelAndView($this->controlVars['viewDir'].'form.php');
    $mav->setModelValue('ctryList', $this->dao->getCountryList());
    $mav->setModelValue('title', $formParams['title']);
    $mav->setModelValue('details', $formParams['details']);
    $mav->setModelValue('fromMonth', $formParams['fromMonth']);
    $mav->setModelValue('fromDay', $formParams['fromDay']);
    $mav->setModelValue('fromYear', $formParams['fromYear']);
    $mav->setModelValue('toMonth', $formParams['toMonth']);
    $mav->setModelValue('toDay', $formParams['toDay']);
    $mav->setModelValue('toYear', $formParams['toYear']);
    $mav->setModelValue('monthArray', array('', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'));
    $mav->setModelValue('yearArray', array(2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030));
    $mav->setModelValue('action', $formParams['action']);
    if (!empty($formParams['travelItemID']))
      $mav->setModelValue('travelItemID', $formParams['travelItemID']);
    if (!empty($formParams['destinations'])) {
      $mav->setModelValue('destinations', $formParams['destinations']);
    }

    return $mav;
  }

  private function convertToArray($commaDelimitedRequestVars, $hasSpecialDelimiters=false) {
    return $hasSpecialDelimiters ? explode('FixThisHack,', $commaDelimitedRequestVars) : explode(',', $commaDelimitedRequestVars);
  }

  private function composeTravelItem() {

	//checked
    $travelItem = new TravelPlan();
    $travelItem->setUserID($this->userCredentials['userID']);
    $travelItem->setTitle(trim($_POST['title']));
    $travelItem->setDetails(trim($_POST['details']));
    //////// end of checked
    // Not set when adding
    if (isset($_POST['travelItemID'])) {
      $travelItem->setTravelWishID($_POST['travelItemID']);
    }

    $countries = $this->convertToArray($_POST['countries']);
    $description = $this->convertToArray($_POST['description'], true);
    $fromMonth = $this->convertToArray($_POST['destFromMonth']);
    $fromDay = $this->convertToArray($_POST['destFromDay']);
    $fromYear = $this->convertToArray($_POST['destFromYear']);
    $toMonth = $this->convertToArray($_POST['destToMonth']);
    $toDay = $this->convertToArray($_POST['destToDay']);
    $toYear = $this->convertToArray($_POST['destToYear']);

    $earliestDate = TravelItemController::getEarliestDate($fromYear, $fromMonth, $fromDay);
    $latestDate = TravelItemController::getLatestDate($toYear, $toMonth, $toDay);

    if($this->countNonZeroDates($fromYear) > 1) {
      $latestFromDate = TravelItemController::getLatestDate($fromYear, $fromMonth, $fromDay);
      if (strtotime($latestFromDate) > strtotime($latestDate)) $latestDate = $latestFromDate;
    }

    $travelItem->setDateFrom($earliestDate);
    $travelItem->setDateTo($latestDate);

    $destinations = array();
    for ($i = 0; $i < count($countries); $i++) {
	  $country = new Country($countries[$i]);
      $country->setCountryID($countries[$i]);

      $travelDest = new AppTravelDestinations();
	  $travelDest->setTravelWishID($travelItem->getTravelWishID());
	  $travelDest->setCountry($country);
      $travelDest->setDescription($description[$i]);

      $fY = !empty($fromYear[$i])  ? $fromYear[$i]  : '0000';
      $fM = !empty($fromMonth[$i]) ? $fromMonth[$i] : '00';
      $fD = !empty($fromDay[$i])   ? $fromDay[$i]   : '00';
      $tY = !empty($toYear[$i])    ? $toYear[$i]    : '0000';
      $tM = !empty($toMonth[$i])   ? $toMonth[$i]   : '00';
      $tD = !empty($toDay[$i])     ? $toDay[$i]     : '00';

      $travelDest->setStartDate("$fY-$fM-$fD");
      $travelDest->setEndDate("$tY-$tM-$tD");

      $destinations[] = $travelDest;
    }

    $travelItem->setDestinations($destinations);	

    return $travelItem;
  }

  //TODO: use php's date functions
  public static function getEarliestDate($year, $month, $day) {
    $earliestYear = '9999';
    $earliestMonth = '13';
    $earliestDay = '32';
    for ($i=0; $i<count($year); $i++) {
      if ($year[$i] == '0000' || empty($year[$i])) continue;
      if ($year[$i] < $earliestYear) {
        $earliestYear = $year[$i];
        $earliestMonth = $month[$i];
        $earliestDay = $day[$i];
      } elseif ($year[$i] == $earliestYear) {
        if ($month[$i] < $earliestMonth) {
          $earliestMonth = $month[$i];
          $earliestDay = $day[$i];
        } elseif ($month[$i] == $earliestMonth) {
          if ($day[$i] < $earliestDay) $earliestDay = $day[$i];
        }
      }
    }
    if ($earliestYear == '9999') $earliestYear = '0000';
    if ($earliestMonth == '13') $earliestMonth = '00';
    if ($earliestDay == '32') $earliestDay = '00';

    return $earliestYear.'-'.$earliestMonth.'-'.$earliestDay;
  }

  public static function getLatestDate($year, $month, $day) {
    $latestYear = '0000';
    $latestMonth = '00';
    $latestDay = '00';
    for ($i=0; $i<count($year); $i++) {
      if ($year[$i] > $latestYear) {
        $latestYear = $year[$i];
        $latestMonth = $month[$i];
        $latestDay = $day[$i];
      } elseif ($year[$i] == $latestYear) {
        if ($month[$i] > $latestMonth) {
          $latestMonth = $month[$i];
          $latestDay = $day[$i];
        } elseif($month[$i] == $latestMonth) {
          if ($day[$i] > $latestDay) $latestDay = $day[$i];
        }
      }
    }

    return $latestYear.'-'.$latestMonth.'-'.$latestDay;
  }

  private function countNonZeroDates($dateArray) {
    $cntNonZeroDates = 0;
    foreach ($dateArray as $d) {
      if ($d != '0000' && $d != '') $cntNonZeroDates++;
    }
    return $cntNonZeroDates;
  }
}
?>