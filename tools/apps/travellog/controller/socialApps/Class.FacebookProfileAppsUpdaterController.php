<?php
require_once('travellog/model/FacebookPlatform/facebook.php');
require_once('travellog/model/socialApps/mytraveljournals/Class.WebServiceUser.php');
require_once('travellog/controller/socialApps/Class.FacebookStaticViewsUpdaterScriptController.php');


class FacebookProfileAppsUpdaterController {

	private static function getTravelerIDs(){
		return WebServiceUser::getTravelerIDsWithFacebookID();
	}

	private static function hasFacebookApp($travelerID){
		if(is_numeric($travelerID) && 0 != $travelerID){
			return WebServiceUser::getFacebookIDByTravelerID($travelerID);			
		} else return false;
	}


// Applications required files
// this is being separated to avoid redundancy of requiring files. 

	private static function myTravelBioRequiredFiles(){
		require_once('travellog/model/travelbio/Class.TravelBioUser.php');
		require_once('travellog/model/travelbio/Class.TravelBioManager.php');
	}

	// return facebookAPI object
	private static function myTravelMapRequiredFiles(){
		require_once 'travellog/controller/socialApps/mytravelmap/facebook/Class.MyTravelMapController.php';
	}

	private static function myTravelPlansRequiredFiles(){
		require_once('travellog/controller/socialApps/mytravelplans/facebook/Class.TravelItemController.php');
	}

	private static function myTravelJournalsRequiredFiles(){
		require_once('travellog/controller/socialApps/mytraveljournals/facebook/Class.fbMytravelJournalsController.php');		
	}
/*	
	private static function allAppsRequiredFiles(){
		self::myTravelBioRequiredFiles();
		self::myTravelMapRequiredFiles();
		self::myTravelPlansRequiredFiles();
		self::myTravelJournalsRequiredFiles();				
	}
*/
// End of Applications required files




// Applications methods of updating facebook static views
// this is being separated to avoid redundancy of calling methods. 

	private static function updateMyTravelBio($travelerID=null){
		$fbUser = TravelBioUser::getTravelBioUserByTravelerID($travelerID);
		if(is_object($fbUser)) TravelBioManager::refreshFbUserProfileBox($fbUser);		
	}

	private static function updateMyTravelMap($vars = array()){
		$control = new MyTravelMapController($vars);
   		$control->sendProfileMarkup();					
	}

	private static function updateMyTravelPlans($travelerID=null){
		TravelItemController::updateStaticProfile($travelerID);					
	}

//	private static function updateMyTravelJournals($travelerID=null){
//		$update = new fbMytravelJournalsController($travelerID);		
//	}

// End Applications methods of updating facebook static views
	
	

	
// Functions or methods for updating single facebook static views.  --> Param=$travelerID 
// Functions that should called when some edits are executed.  

	static function updateMyTravelBioStaticViewsInFacebook($travelerID = null){	   
		if(self::hasFacebookApp($travelerID)){
			self::myTravelBioRequiredFiles();
			self::updateMyTravelBio($travelerID);
		}
	}

	static function updateMyTravelMapStaticViewsInFacebook($travelerID = null){
		if($facebookID = self::hasFacebookApp($travelerID)){
			self::myTravelMapRequiredFiles(); 
			$vars = array('user' => $facebookID , 'facebook' => MyTravelMapController::getFacebookApi() );			
			$control = new MyTravelMapController($vars);
	   		$control->sendProfileMarkup();					
		}		
	}
	
	static function updateMyTravelPlansStaticViewsInFacebook($travelerID = null){
		if(self::hasFacebookApp($travelerID)){
			self::myTravelPlansRequiredFiles();
			self::updateMyTravelPlans($travelerID);
		}
	}

	static function updateMyTravelJournalsStaticViewsInFacebook($travelerID = null){
		if(self::hasFacebookApp($travelerID)){
			self::myTravelJournalsRequiredFiles();
			$update = new fbMytravelJournalsController($travelerID);
		}
	}

// Functions that should called when some edits are executed.


// Functions or methods that updates all accounts in facebook.
// Scripts for updating All apps users depending on the app.
	static function updateAllMyTravelBioStaticViews($numUsers = 20){
		self::myTravelBioRequiredFiles();
		$scriptController = new FacebookStaticViewsUpdaterScriptController('mytravelbio');
		$travelerID = TravelBioUser::isValidMTBUser($scriptController->getScriptLastUpdatedID());

		// ravelBioUser::getTravelBioUsers() --> return TravelBioUser objects
		if($travelBioUsers = TravelBioUser::getTravelBioUsers($travelerID, $numUsers)) {
			$newTravelBioUsers = end($travelBioUsers);
			TravelBioManager::refreshAllFbUserProfileBox($travelBioUsers);
			$scriptController->updateScriptLastUpdatedID($newTravelBioUsers->getTravelerID());
		} else {
			$scriptController->resetScriptLastUpdatedID();
			exit;
		}

	}

	static function updateAllMyTravelMapStaticViews($numUsers = 20){
		self::myTravelMapRequiredFiles();
		$scriptController = new FacebookStaticViewsUpdaterScriptController('mytravelmap');

		$userID = FacebookDAO::isMTMValidUserID($scriptController->getScriptLastUpdatedID());
		if($users = FacebookDAO::getMTMUsersCredenials($userID, $numUsers)){
			$newUser = end($users);
			MyTravelMapController::updateAllProfile($users);
			$scriptController->updateScriptLastUpdatedID($newUser['userID']);
		} else {
			$scriptController->resetScriptLastUpdatedID();
			exit;
		}
	}		


	static function updateAllMyTravelPlansStaticViews($numUsers = 20){
		self::myTravelPlansRequiredFiles();		
		$scriptController = new FacebookStaticViewsUpdaterScriptController('mytravelplans');

		$userID = TravelScheduleFB::isMTPValidUserID($scriptController->getScriptLastUpdatedID());
		if($users = TravelScheduleFB::getMTPUsersCredentials($userID, $numUsers)){
			$newUser = end($users);
			TravelItemController::updateAllStaticProfile($users);
			$scriptController->updateScriptLastUpdatedID($newUser['userID']);
		}
		else {
			$scriptController->resetScriptLastUpdatedID();
			exit;			
		}
	}

	static function updateAllMyTravelJournalsStaticViews($numUsers = 20){
		self::myTravelJournalsRequiredFiles();
		$scriptController = new FacebookStaticViewsUpdaterScriptController('mytraveljournals');
		$userID = WebServiceUser::isMTJValidUserID($scriptController->getScriptLastUpdatedID());
		if($users = WebServiceUser::getMTJUsersCredentials($userID, $numUsers)){
			$newUser = end($users);
			fbMytravelJournalsController::updateAllFbProfile($users);
			$scriptController->updateScriptLastUpdatedID($newUser['userID']);
		} else {
			$scriptController->resetScriptLastUpdatedID();
			exit;
		}
	}
/*	
	static function updateAllAppsStaticViews(){
		self::allAppsRequiredFiles();
		$travelerIDs = self::getTravelerIDs();
		for($i=0; $i<count($travelerIDs); $i++){
			$vars['user'] = MyTravelMapDAO::getFacebookIDByTravelerID($travelerIDs[$i]);
			self::updateMyTravelBio($travelerIDs[$i]);
			self::updateMyTravelMap($travelerIDs[$i]);
			self::updateMyTravelPlans($travelerIDs[$i]);
			self::updateMyTravelJournals($travelerIDs[$i]);
		}				
	}
1082993925
	1 --> facebookID=1244318742 --> Journals=1
	2 --> facebookID=668340698 --> Journals=0
	3 --> facebookID=517550177 --> Journals=0
	4 --> facebookID=1191458814 --> Journals=1
	5 --> facebookID=30608965 --> Journals=0
	6 --> facebookID=48909538 --> Journals=1
	7 --> facebookID=4946369 --> Journals=1
	8 --> facebookID=518366776 --> Journals=0
	9 --> facebookID=502647149 --> Journals=0
	10 --> facebookID=640673621 --> Journals=0
	11 --> facebookID=567415015 --> Journals=1
	12 --> facebookID=667601062 --> Journals=1
	13 --> facebookID=728276981 --> Journals=1
	14 --> facebookID=641980521 --> Journals=1
	15 --> facebookID=821559195 --> Journals=0
	16 --> facebookID=627261432 --> Journals=1
	17 --> facebookID=536785283 --> Journals=1
	18 --> facebookID=671830416 --> Journals=0
	19 --> facebookID=1509050 --> Journals=0
	20 --> facebookID=649293788 --> Journals=1
*/
// End of Scripts.  
} 
?>