<?php
class FacebookStaticViewsUpdaterScriptController{

	protected $id;
	protected $keys;
	protected $appName;
	protected $appArray = array();
		
	function __construct($appName = '') {
		if( in_array($appName, $keys = $this->getAppKeys()) ){

			$this->appName = $appName;
			$file = file('scriptData.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			$this->appArray = array_combine($keys, $file);
			$this->id = end(explode('=', $this->appArray[$appName]));

		} else return $this->__tostring();
	}

	private function updateScriptID($id = 0){
		$handle = fopen('scriptData.txt', 'w');
		$this->appArray[$this->appName] = $this->appName.'='.$id;
		$newData = implode("\n", $this->appArray);
		$result = fwrite($handle, $newData);
		fclose($handle);
		return $result ? true : false;					
	}

	function getScriptLastUpdatedID(){
		return $this->id;
	}
	
	function updateScriptLastUpdatedID($id = null) {
		if($id) $this->updateScriptID($id);
		return false;
	}

	function resetScriptLastUpdatedID() {		
		$this->updateScriptID();
	}
	
	private function getAppKeys(){
		return array('mytravelmap','mytravelbio','mytravelplans','mytraveljournals');
	}		 
}

?>