<?php
require_once('travellog/model/FacebookPlatform/facebook.php');

class FacebookAppsViewCheckerController {
	protected 
		$view, 
		$facebookApi, 
		$loggedinFacebookID, 
		$appOwnerFacebookID,
//		$ownerFriendsWithApp,
		$loggedinFriendsWithApp;
    
	const 
		PAGE_VIEW		 = 'PAGE_VIEW',
		CANVAS_VIEW		 = 'CANVAS_VIEW',		
		PROFILE_TAB_VIEW = 'PROFILE_TAB_VIEW';
		
	
	function __construct($appApikey = '147aa5f035170dd13d8670b15cb3f906', $apiSecret = '080705235dd747ed85d4c8ec9909c520') {
		$this->setFacebookApi($appApikey, $apiSecret);
		$this->executeChecker();
	}

	function executeChecker() {
		if (isset($_REQUEST['fb_sig_in_profile_tab'])) {
		    $this->loggedinFacebookID = null;
		    $this->appOwnerFacebookID = $_REQUEST['fb_sig_profile_user'];
			$this->view = 'PROFILE_TAB_VIEW';
			
		} elseif(isset($_GET['fb_page_id'])) {
		    $this->loggedinFacebookID = $this->getFacebookApi()->require_login();
		    $this->appOwnerFacebookID = $_REQUEST['fb_page_id'];
			$this->view = 'PAGE_VIEW';

		} else {
		    $this->loggedinFacebookID = $this->getFacebookApi()->require_login();
		    $this->appOwnerFacebookID = isset($_GET['id']) ? $_GET['id'] : $this->loggedinFacebookID;
			$this->view = 'CANVAS_VIEW';
		}

		$this->loggedinFriendsWithApp = $this->getFacebookApi()->api_client->friends_getAppUsers();
	}	

	function setFacebookApi($appApikey = null, $apiSecret = null) {
		if(!$appApikey || !$apiSecret) return null;
		$this->facebookApi = new Facebook($appApikey,$apiSecret);
	}

	function getAppView() {
		return $this->view;
	}
	
	function getFacebookApi() {
		return $this->facebookApi;
	}

	function getAppOwnerFacebookID() {
		return $this->appOwnerFacebookID;
	}	

	function getLoggedinFacebookID() {
		return $this->loggedinFacebookID;
	}

//	function getOwnerFriendsWithApp() {
//		return $this->ownerFriendsWithApp;
//	}	

	function getLoggedinFriendsWithApp() {
		return is_array($this->loggedinFriendsWithApp) ? $this->loggedinFriendsWithApp : array();
	}	
}
?>