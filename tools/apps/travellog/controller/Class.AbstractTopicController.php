<?php
	require_once('travellog/factory/Class.FileFactory.php');
	
	abstract class AbstractTopicController {
		
		/**
		 * Initializes the paths of the classes, views, utility files and templates.
		 */	
		function __construct(){
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$file_factory->registerClass('Traveler');
			$file_factory->registerClass('SubNavigation');
			$file_factory->registerClass('Template', array('path' => ''));
			$file_factory->registerTemplate('LayoutMain');
			$file_factory->setPath('CSS', '/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		}
		
		/**
		 * Shows the topic's add form.
		 * 
		 * @param int $id The ID of the topic's related group.
		 * 
		 * @return void
		 */
		function add($id = 0){
			require_once('travellog/action/topic/Class.Add.php');
			$params = array();
			$params['group_id'] = $id;
			
			$action = new Add($params);
			$action->execute();
			exit;	
		}
		
		/**
		 * Shows the topic's add form.
		 * 
		 * @param int $id The ID of the topic's related group.
		 * 
		 * @return void
		 */
		function edit($id = 0){
			require_once('travellog/action/topic/Class.Edit.php');
			$params = array();
			$params['topic_id'] = $id;
			
			$action = new Edit($params);
			$action->execute();
			exit;	
		}	
		
		/**
		 * Saves the edited or newly created topic.
		 * 
		 * @return void
		 */
		function save(){
			require_once('travellog/action/topic/Class.Save.php');
			
			$action = new Save();
			$action->execute();
			exit;	
		}		
		
		/**
		 * Deletes the given topic.
		 * 
		 * @param int $id The ID of the topic.
		 * 
		 * @return void
		 */
		function delete($id = 0){
			require_once('travellog/action/topic/Class.Delete.php');
			$params = array();
			$params['topic_id'] = $id;
			
			$action = new Delete($params);
			$action->execute();
			exit;	
		}
		
		/**
		 * Shows the discussions of the topic.
		 * 
		 * @param int $id The ID of the topic
		 * 
		 * @return void
		 */
		function discussions($id = 0){
			require_once('travellog/action/topic/Class.Discussions.php');
			
			$params = array();
			$params['topic_id'] = $id;
			
			$action = new Discussions($params);
			$action->execute();
		}
		
		/**
		 * Saves the privacy setting of the given topic.
		 * 
		 * @param integer $id				 			 The ID of the topic.
		 * @param integer $privacy_setting The privacy setting of the topic.
		 * 
		 * @return void
		 */
		function savePrivacySetting($id = 0, $privacy_setting = 0){
			require_once('travellog/action/topic/Class.SavePrivacySetting.php');
			$params = array();
			$params['topic_id'] = $id;
			$params['privacy_setting'] = $privacy_setting;
			
			$action = new SavePrivacySetting($params);
			$action->execute();
			exit;	
		}
		
		/**
		 * Removes the discussions from the knowledge base and sets the new privacy setting
		 * of the topic.
		 * 
		 * @param int $id 							The ID of the topic
		 * @param int $privacy_setting 	The new privacy setting
		 */
		function removeDiscussionsFromKnowledgeBase($id, $privacy_setting){
			require_once('travellog/action/topic/Class.RemoveDiscussionsFromKnowledgeBase.php');
			$params = array();
			$params['topic_id'] = $id;
			$params['privacy_setting'] = $privacy_setting;
			
			$action = new RemoveDiscussionsFromKnowledgeBase($params);
			$action->execute();
			exit;				
		}
		
		/**
		 * Activates the __call magic function. 
		 */
		function __call($method, $arguments){
			require_once('travellog/action/Class.ControllerAction.php');
			
			ControllerAction::showFileNotFound();
		}
	}