<?php
  require_once("travellog/model/Class.SessionManager.php");
  require_once("travellog/controller/Class.IController.php");
  require_once("travellog/factory/Class.FileFactory.php");
  
  class AbstractSubGroupCategoriesController implements IController {
  	
  	/**
  	 * Attributes
  	 */
  	protected 
  	
  	$subNav = NULL,	// the variable the holds the subnavigation object
  	
  	$isAdminLogged = false, // a flag variable whether an admin is logged or not
  	
  	$profileHandler = NULL // the variable that handles the group profile that shows above the subnavigation.
  	;	
  	
  	/**
  	 * constructor
  	 */
  	function __construct(){
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('CustomizedSubGroupCategory');
			
			$this->file_factory->registerClass('BackLink' , array('path' => 'travellog/model/navigator/'));
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('SubGroupCategoriesViewHandler', array('path' => 'travellog/views/subgroups'));
			$this->file_factory->registerClass('SubNavigation');
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->tpl = $this->file_factory->getClass('Template');
			$this->tpl->set_path("travellog/views/");		
  	}
  	
		/**
		 * Checks whether the current logged in is an admin user(staff is consider as an admin user) or not. If it
		 * is not an admin user it redirects the page to custom group normal view mode.
		 * 
		 * @param boolean $_privilege
		 * @return void
		 */
		function allowAdminPrivileges(){
			if (!$this->isAdminLogged){
				header("location:subgroups.php?gID=".$this->group->getGroupID());
				exit;
			}
		}

		/**
		 * Deletes the given category(category ID) from the database.
		 * 
		 * @param array $params The request data.
		 * @return void
		 */
		public function deleteCategory($params) {
			$this->allowAdminPrivileges();
			
			$category = $this->file_factory->getClass('CustomizedSubGroupCategory',array($params['sgcID']));
			$category->emptyCategory();
			$category->delete();
			
			$this->obj_session->set('custompopup_message', "You have successfully deleted the category <strong>".$category->getName()."</strong>.");
			
			header("location: ".$this->backLink->getBackURL());
		}
		
		/**
		 * Handles the featuring of the given category.
		 * 
		 * @param integer $categoryID The ID of the category to be featured.
		 * @param array $params The data from fetchGPCData.
		 * @return void
		 */
		function featureCategory($categoryID, array $params){
			$this->allowAdminPrivileges();
			
			$category = $this->file_factory->getClass('CustomizedSubGroupCategory' , array($categoryID));
			$category->setIsFeatured(CustomizedSubGroupCategory::FEATURED);
			$category->save();
			
			exit;
		}
  	
  	/**
  	 * Fetches the values passed through post, request, get,
  	 * session and cookie. 
  	 * 
  	 * @return array
  	 */
  	private function fetchGPCData(){
			$params = array();
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'view';
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : 0 ;
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : $params['gID'];
			
			$params['category'] = ( isset($_REQUEST['category']) )? $_REQUEST['category'] : null;
			
			$params['action']  = ( isset($_GET['action']))? $_GET['action'] : 'ViewList';
			$params['action']  = ( isset($_POST['action']))? $_POST['action'] : $params['action'];
     	
      $params['sgcID'] = ( isset($_REQUEST['sgcID']) )? $_REQUEST['sgcID'] : 0;
			$params['sgcName'] = ( isset($_POST['txtcName']) )? $_POST['txtcName'] : null;
			$params['submit'] = (isset($_POST['submit'])) ? $_POST['submit'] : "SAVE";
			
			$params['categoryIDs'] = (isset($_REQUEST['categoryIDs'])) ? $_REQUEST['categoryIDs'] : ""; 
			
			$params['searchKey']  = ( isset($_REQUEST['txtGrpName']))? $_REQUEST['txtGrpName'] : '';
			
			$params['tabAction'] = (isset($_REQUEST['tabAction'])) ? $_REQUEST['tabAction'] : 'showActive';
			$params['tabAction'] = ("showActive" != $params['tabAction'] AND "showInActive" != $params['tabAction']) ? "showActive" : $params['tabAction'];
			
			$params['radSort']  = ( isset($_REQUEST['radSort']))? $_REQUEST['radSort'] : 0 ;
			
			$params['back'] = (isset($_REQUEST['back'])) ? $_REQUEST['back'] : "";
			
			$params['curPage'] = (isset($_POST['page'])) ? $_POST['page'] : 1;
      $params['curPage'] = (isset($_GET['page']))  ? $_GET['page']  : $params['curPage'];
			
			$params['message'] = ($this->obj_session->get('custompopup_message')) ? $this->obj_session->get('custompopup_message') : NULL;
			$this->obj_session->unsetVar('custompopup_message');
			
			return $params;  
  	}
  	
  	/**
  	 * Initializes the common variables used in adding, updating and deletion.
  	 * 
  	 * @param array $params The data used to initialize the common variables.
  	 * @return void
  	 */
  	function initializeActionCommonAttributes(&$params){
			if (0 == $params['gID']){
				header("location: /index.php");
				exit;
			}
			
			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($params['gID']));
			$this->group = $mGroup[0];
			
			$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? true : false;			
  		$this->backLink = $this->file_factory->invokeStaticClass('BackLink','instance');
  	}
  	
  	/**
  	 * Initializes the common variables that are used in viewing.
  	 * 
  	 * @param array $params The data used to initialize the common variables.
  	 * @return void
  	 */
  	function initializeViewCommonAttributes(&$params){
			$this->subNav = $this->file_factory->getClass('SubNavigation');
			$this->subNav->setContext('GROUP');
			$this->subNav->setContextID($params['gID']);
			$this->subNav->setLinkToHighlight('SUBGROUPS');	
			
			$this->profileHandler = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$this->profileHandler->init($this->group->getGroupID());
  	}
  	
  	/**
  	 * The entry point of this class or shall I say this is the one called 
  	 * to use this class.
  	 * 
  	 * @return void
  	 */
  	function performAction(){
  		$params = $this->fetchGPCData();
  		$this->initializeActionCommonAttributes($params);
  		
			switch ($params['action']) {
				case 'updateCategory':
					$this->updateCategory($params);
				break;
				case 'deleteCategory':
					$this->deleteCategory($params);
				break;
				case 'featureCategory':
					$this->featureCategory($params['sgcID'], $params);
				break;
				case 'unfeatureCategory':
					$this->unfeatureCategory($params['sgcID'], $params);
				break;
			}
  		
			$this->initializeViewCommonAttributes($params);
			
			switch ($params['mode']) {
				case 'viewAddCategory':
					$params['sgcID'] = 0;
				case 'viewEditCategory':
					$this->viewUpdateCategory($params);
				break;	
			}
  	}
  	
		/**
		 * Handles the featuring of the given category.
		 * 
		 * @param integer $categoryID The ID of the category to be featured.
		 * @param array $params The data from fetchGPCData.
		 * @return void
		 */
		function unfeatureCategory($categoryID, array $params){
			$this->allowAdminPrivileges();
			
			$category = $this->file_factory->getClass('CustomizedSubGroupCategory' , array($categoryID));
			$category->setIsFeatured(CustomizedSubGroupCategory::UNFEATURED);
			$category->save();

			exit;
		}
  	
		/**
		 * Handles the adding and updating of a category in the database. 
		 * 
		 * @param array $_params The data of the new category will be get from here. This must be the request data.
		 * @return void 
		 */
		function updateCategory(&$_params) {
			$this->allowAdminPrivileges();
			
			// validate data
			$no_errors = true;
			$category = $this->file_factory->getClass('CustomizedSubGroupCategory',array($_params['sgcID']));
			
			if ("" == trim($_params['sgcName'])) {
				$no_errors = false;
				$_params['message'] = "Category name must be filled out.";
				$_params['mode'] = "viewAddCategory";
			}
			else {
				if (0 == $_params['sgcID'] AND 0 < $this->group->getSubGroupCategoriesCount(trim($_params['sgcName']))) {
					$no_errors = false;
					$_params['message'] = "Category name ".$_params['sgcName']." already exists.";
					$_params['mode'] = "viewAddCategory";
				}
			}
			// end validation
			
			if ($no_errors) {
				$category->setName($_params['sgcName']);
				$category->setParentID($_params['gID']);
				$category->setRankSettings((0 == $_params['sgcID']) ? 1 : $category->getRankSettings());
				$category->setCategoryRank((0 == $_params['sgcID']) ? 9999 : $category->getCategoryRank());
				$category->save();
				$category->emptyCategory();
			
				if (isset($_POST["subgroups"]) AND 0 < count($_POST["subgroups"])) {
					$category->addSubGroups($_POST["subgroups"]);
					$this->uncategorizedCategory = $this->group->getCategoryForUncategorizedSubGroups();
					if (!is_null($this->uncategorizedCategory)) {
						$this->uncategorizedCategory->removeSubGroups($_POST["subgroups"]);
					}
					try {
						$category->autoSortGroups($category->getRankSettings());
					}
					catch(exception $ex){}
				}
			
				$action = (0 == $_params['sgcID']) ? "added": "updated";
				$this->obj_session->set('custompopup_message', "You have successfully $action the category <strong>".$category->getName()."</strong>.");
				
				header("location: ".$_params['back']);
			}
		}
    
		/**
		 * Shows the form in Adding/Editing a subgroup category.
		 * 
		 * @param array $_params the request data
		 * @return void
		 */
		public function viewUpdateCategory($_params) {
			$this->allowAdminPrivileges();
			
			$this->uncategorizedCategory = $this->group->getCategoryForUncategorizedSubGroups();
			$factory = SubGroupFactory::instance($_params['gID']);
			$ids = $factory->getSubGroupIDsWithoutCategoryInfo();
			if (is_null($this->uncategorizedCategory)) {
				$this->uncategorizedCategory = new CustomizedSubGroupCategory();
				if (0 < count($ids)) {
					$this->uncategorizedCategory->setName("UnCategorized");
					$this->uncategorizedCategory->setParentID($_params['gID']);
					$this->uncategorizedCategory->setIsUncategorized(1);
					$this->uncategorizedCategory->setCategoryRank(99999);
					$this->uncategorizedCategory->save();
					$this->uncategorizedCategory->addSubGroups($ids);
				}
			}
			else if (!is_null($this->uncategorizedCategory) AND 0 == $this->uncategorizedCategory->getSubGroupsCount()) {
				$this->uncategorizedCategory->emptyCategory();
				$this->uncategorizedCategory->delete();
			}
			
			$this->backLink = $this->file_factory->invokeStaticClass('BackLink','instance');
			
			$category = $this->file_factory->getClass('CustomizedSubGroupCategory',array($_params['sgcID']));
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->tpl->set_path("travellog/views/subgroups/");
			$this->tpl->setVars( array(
				'subNavigation' => $this->subNav,
				'profile' => $this->profileHandler->get_view(),
				'id' => $_params['sgcID'],
				'name' => $category->getName(),
				'parentID' => $_params['gID'],
				'category_groups' => $category->getSubGroups(null, null, " name"),
				'non_category_groups' =>$this->uncategorizedCategory->getSubGroups(null, null, " name"),
				'message' => $_params['message'],
				'backLink' => $this->backLink
			) );
			$this->tpl->setTemplate('tpl.FrmCustomizedSubGroupCategory.php');
			$this->tpl->out();
		}
  }
