<?php
	
	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/model/Class.SessionManager.php';
	require_once 'travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php';
	require_once 'travellog/helper/Class.NewsHelper.php';
	
	abstract class AbstractNewsController{
		
		const 	VIEW_NEWS_LIST 		= 'viewNewsList',
				VIEW				= 'viewNews',
				EDIT				= 'edit',
				COMPOSE				= 'compose',
				SUBMIT				= 'submitNews',
				SAVE				= 'save',
				
				// errors
				UNKNOWN_SOURCE		= 'unknownSource',
				PERMISSION_DENIED	= 'permissionDenied',
				EMPTY_TITLE			= 'emptyTitle',
				EMPTY_CONTENT		= 'emptyContent';
		
				
		
		protected $helper,
				$user,
				$file_factory;
				
		function __construct() {
			
			$this->session_manager = SessionManager::getInstance();
			
			if( !$this->session_manager->getLogIn() ){
				header("location: login.php?failedLogin&redirect=news.php?" . $_SERVER['QUERY_STRING']);
				exit;			 
			}
			
			// make sure $_GET['gID'] is provided. If not, bring user to home page
			if (!isset($_GET['gID'])) {
				header("location: index.php");
				exit;
			}
			
			$this->file_factory = FileFactory::getInstance();
			//require_once("travellog_/domains/" . $_SERVER['SERVER_NAME'] . ".php");
			
			// models
			
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('GroupFactory', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('NotificationSetting', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('FrmNews', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('ViewNewsMessage', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('gaNews', array('path' => 'travellog/model/messageCenter/'));
			$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('ProfileCompFactory', array('path' => 'travellog/components/profile/controller/'));
			
			
			$this->helper = NewsHelper::getInstance();
			$this->user = $this->file_factory->getClass('Traveler',array($this->session_manager->get('travelerID')));
			
			// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//start logger
			if($this->session_manager->get('travelerID')){
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($this->session_manager->get('travelerID'),'TRAVELER');
			}	
		}
		
		function performAction() {
			
			switch( $this->helper->getAction() ){
				case self::EDIT:
					$this->editNews();
					break;
				case self::SUBMIT:
					$this->submitNews();
					break;	
				default:
					$this->showForm();
			}
			exit;
		}
		
		private function showForm($contents = array()) {
			
			// should be rafactored to check that user powers is applicable to this group
			if( !$this->user->isStaff() && !$this->user->isAdministrator() ) {
				header('location: passport.php');
				exit;
			}
						
			$gFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance',array());
			
			if( !$contents )
				$contents = $this->helper->readComposeData();
			$contents['subNavigation'] = $this->getSubNavigation();
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			if( !$contents['groupID'] && $this->user->isAdministrator() )
				$contents['groupID'] = $_GET['gID'];
			
			if( $group = $gFactory->create(array($contents['groupID'])) ){
				$group = $group[0];
				if( $group->isStaff($this->user->getTravelerID()) || $group->getAdministratorID() == $this->user->getTravelerID() ) {
					
					$pFactory = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
    				$pFactory->init($contents['groupID']);
    				$contents['pFactory'] = $pFactory;
   				
					Template::includeDependentJs( '/js/date.js'     );
					Template::includeDependentJs( '/js/jquery.datePicker.js'     );
					 
					$jscode = <<<JSCODE
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			
			$('#chkEmphasize').click(function(){
				$('#expirationDateDiv').toggle('slow');	
			});
			
		 	$('.date-pick').datePicker().val(new Date().asString()).trigger('change');
		});	
	})(jQuery);
</script>
JSCODE;
					Template::includeDependent( $jscode ); 
					$tpl = $this->file_factory->getClass('FrmNews', array());
					$contents['newsID'] = 0;
					$tpl->setContents($contents);
				}
				else {
					$contents['displayMessage'] = self::PERMISSION_DENIED; 
					$tpl = $this->file_factory->getClass('ViewNewsMessage', array());
					$tpl->setContents($contents);
				}	
			}
			else{
				$contents['displayMessage'] = self::UNKNOWN_SOURCE; 
				$tpl = $this->file_factory->getClass('ViewNewsMessage', array());
			}
			
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );   
			$obj_view->setContents( array('contents' => $tpl) );
			$obj_view->render();	
			
		}
		
		private function submitNews() {
			$content = $this->helper->readComposeData();
			$errCodes = array();

			// validate
			if( !$content['title'] )
				$errCodes[] = 1;
			if( !$content['content'])
				$errCodes[] = 2;
			if( 0 == count($errCodes) ){
				require_once 'travellog/model/messageCenter/news/Class.gaNews.php';
				
				$n = new gaNews;
				
				if ($content['newsID'] > 0) {
					$n->setID( $content['newsID'] );
				}
				
				$n->setGroupID( $content['groupID'] );
				$n->setTitle( $content['title'] );
				$n->setContent( $content['content'] );

				if ($content['prioritize'])
					$n->setExpiration( $content['expiration'] );

				$n->setAuthorID($_SESSION['travelerID']);
				$n->save();
				 
				$notificationSetting =  $this->file_factory->getClass('NotificationSetting');
				$notificationSetting->setContext(NotificationSetting::NEWS_CONTEXT);
				$notificationSetting->setContextID($n->getID());
				$notificationSetting->setMessageIncluded( $content['includeMessage'] ? 1 : 0);
				$notificationSetting->save();
				
				// added by chris: send instant notification
				if( $content['sendNotification'] ){
					require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
					RealTimeNotifier::getInstantNotification(RealTimeNotifier::SUBMIT_NEWS, 
						array(
							'NEWS'				=> $n,
							'INCLUDE_MESSAGE'	=> $content['includeMessage']
						))->send(); 
				}
				// end add
				
				// AT THIS POINT, USER SHOULD BE REDIRECTED TO THE GROUP MESSAGE CENTER
				// WITH CONFIRMATION MESSAGE
				
				$_SESSION['flashMessage'] = 'Your news article has been posted.';
				header("location: /messages.php?gID=" . $content['groupID']);
				exit;
			}
			else{
				$content['errCodes'] = $errCodes;
				$this->showForm($content);
			}
						
		}
		
		private function getSubNavigation(){
			
			// sub navigation
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$content = $this->helper->readComposeData();
			
			$subNavigation->setContext("GROUP");
			$subNavigation->setContextID($content['groupID']);
			if( 0 < $this->session_manager->get("gID") )
				$subNavigation->setGroupType("ADMIN_GROUP");
			else
				$subNavigation->setGroupType("FUN_GROUP");
			$subNavigation->setLinkToHighlight('MESSAGE_CENTER');
			return $subNavigation;
		}
	}
?>
