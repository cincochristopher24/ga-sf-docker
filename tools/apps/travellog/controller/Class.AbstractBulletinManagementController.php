<?php
	/***
	 * ##### Rules ######
	 *		Traveler Context:
	 * 			> VIEW
	 * 				- Only the owner traveler can view his/her bulletins.
	 * 				- Bulletins that belongs to a traveler refers to the bulletins that are owned by the traveler, sent to him by his 
	 * 					friend travelers or bulletins of the traveler's groups. 
	 * 			> COMPOSE
	 * 				- Any traveler can compose a bulletin.
	 * 				- Recipient of the composed message should be the traveler's network of friends and all his participated groups.
	 * 				- The Network of friends recipient option, should be checked by default. 
	 * 			> EDIT
	 * 				- Only the owner of the particular bulletin can edit that bulletin.
	 * 				- Recipient options follows the rules of composing an alert message in a traveler concept.
	 * 			> DELETE
	 * 				- Only the owner of the particular bulletin can delete that bulletin.
	 * 
	 * 		Group Context:
	 * 			> VIEW
	 * 				- Members of the groups(including the admin) can view the bulletins.
	 * 				- Depending on the privacy preference of the group, the bulletins can be viewed by the public.
	 * 				- Public viewers refers to viewers that are not members of the group.
	 * 			> COMPOSE
	 * 				- Follow the traveler rules of composing a bulletin message except for the third rule.
	 * 				- Proxy for the third rule is that the origin group must be checked by default.
	 * 			> EDIT
	 * 				- Only the owner of the particular bulletin and the administrator of the group can edit that bulletin.
	 * 				- Recipient options follows the rules of composing an alert message in a group concept.
	 * 			> DELETE
	 * 				- Only the owner of the particular bulletin and the administrator of the group can delete that bulletin.
	 *
	 * ##### Additional Rules (October 30, 2006)######
	 * 		Traveler Concept:
	 * 			> Compose
	 * 				- The recipient options should consist only of the groups where the traveler is a member and NOT an administrator.
	 * 				- The network of friends recipient option should be available. 
	 * 		Group Concept:
	 * 			> Compose
	 * 				- An administrator can only compose a new bulletin for a certain group in the new group homepage and not through his dashboard.
	 * 				- The recipient options should only consist of the subgroups of the current group.
	 * 				- If the current group is a subgroup, still the recipient options are the subgroup of its parent group.
	 * 				- The network of friends recipient option should not be available.
	 *				=== If the current group is a subgroup		
	 *						- Regardless if the logged traveler is the admin or just a member of that group, the recepient options should consist of the members of that group. (No Option)  
	 * 				=== If the current group is a main group
	 * 						=== If the logged traveler is the admin of the main group, the recipient options should consist of the subgroups of that current main group.
	 * 						=== If the logged traveler is just a member of the main group, the recipient options should consist of the subgroups of that current 
	 * 								main group in which the logged traveler is a member.
	 * 
	 * ##### Additional Rules (Feb. 19, 2007) ######
	 *		Group Concept:
	 *			> View
	 *				- Since bulletins of a traveler's group are also bulletins of the said traveler, only bulletins that has been created after that travelers join date
	 *					must be shown.
	 *
	 * #### Additional Rules (Feb. 22, 2007) #####
	 * 		Group Concept:	
	 * 			> View
	 * 				- If the group preference of the group is to show its bulletins to the public, then members can view the bulletins regardless of thier join date to
	 * 				  that group.
	 * 				- If the group preference of the group is to show its bulletins privately, then members can view the bulletins relative to thier join date.
	 **/
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.ToolMan.php");
	abstract class AbstractBulletinManagementController
	{
		protected $obj_session = NULL;
		protected $file_factory = NULL;
		protected $pageTitle  = '';
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->registerClass('ToolMan',array('path'=>''));
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('BulletinMessage');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('GroupPrivacyPreference');
			$this->file_factory->registerClass('MessageSpace');
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->pageTitle = 'GoAbroad.net Bulletins';
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		}
		
		public function performAction($action='onloadView'){
			$this->$action();
		}
		
		protected function onloadView(){
			//$travelerID = (isset($_SESSION) && array_key_exists('travelerID',$_SESSION) && $_SESSION['travelerID'] !=0 ? $_SESSION['travelerID']:0 );
			$travelerID = $this->obj_session->get('travelerID');
			//Objective of this template is to perform the validations whether the user is authorized to use this application
			//If user is authorized, the ajax app will handle the rest of the application.
			$localMethod 		= $this->setLocalVar("method,hidMethod","view");
			$localHlEntryID 	= $this->setLocalVar("hidHlEntryID,hlEntryID",0,"NUMERIC");
			//check if the locaHlEntryID is valid, if not redirect
			if($localHlEntryID != 0){
				try{
					$hlEntry = $this->file_factory->getClass('BulletinMessage',array($localHlEntryID));
				}
				catch(exception $e){
					ToolMan::redirect("/passport.php");
				}
				if(!$hlEntry->getMessageID()){
					ToolMan::redirect("/passport.php");
				}
			}			 
			//determine the context
			$localGroupID = $this->setLocalVar("groupID,hidGrpID",0,"NUMERIC");
			/***
			if(array_key_exists('CONFIG',$GLOBALS)){
				$localGroupID = $GLOBALS['CON_FIG']->getGroupID();
			}
			***/
			$localConcept = (!$localGroupID)?"TRAVELER":"GROUP";
			
			$localIsAdmin = "false";
			$localIsGroupMember = "false";
			$localMode = "READ_ONLY";
					
			//operate base on the concept
			$localSendableID 	= 0;
			$viewBulletinPref 	= true;
					
			$localIsSubgroup 	= "false";
			$localGroupType 	= 'NULL';
			
			if($localConcept ==  "GROUP"){
				//check if the logged traveler is a member of the group
				
				$factory = $this->file_factory->invokeStaticClass('GroupFactory','instance',null,false);
				try{
					$mGroup	= $factory->create(array($localGroupID));
					$group	= $mGroup[0];
				}
				catch(exception $e){
					ToolMan::redirect("/passport.php");
				}
				//check  if the current group is an admin or a fun group
				//if($group->getDiscriminator() == 1){//fun group, no subgroups
				if($group->getDiscriminator() == GROUP::FUN){//fun group, no subgroups
					$localGroupType = 'FUN_GROUP';
				}
				else{
					$localGroupType = 'ADMIN_GROUP';
					$localIsSubgroup = ($group->isSubGroup())?"true":"false";
				}
				
				//if the hlEntry is defined, we must be sure that it belongs to the viewed group and it is viewable to the public 
				if(isset($hlEntry)){
					if(!$this->file_factory->invokeStaticClass('MessageSpace','isOwner',array($hlEntry->getAttributeID(),$group->getSendableID(),'bulletin'),true)){
						ToolMan::redirect("/passport.php");	
					}
				}
				
				//if the logged traveler is not a member of the group 
				try {
					$privacypref		= $this->file_factory->getClass('GroupPrivacyPreference',array($localGroupID));
					$viewBulletinPref 	= $privacypref->getViewBulletin();			
				}
				catch(exception $e){
					$viewBulletinPref 	= true;
				}
				$localSendableID = $group->getSendableID();
				if(isset($travelerID) && $travelerID !=0){
					$localTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
					$groupAdmin = $group->getAdministrator();
					$localIsGroupMember = "true";
					if($groupAdmin->getTravelerID() == $localTraveler->getTravelerID() || $group->isStaff($localTraveler->getTravelerID()) || $group->isModerator($localTraveler->getTravelerID())){
						$localIsAdmin = "true";
						$localMode = "READ_WRITE";
					}
					elseif(!$group->isMember($localTraveler)){
						//check the pivacy preference of the group if they are to show thier bulletins
						if(!$viewBulletinPref) ToolMan::redirect("/passport.php");
						$localMode = "READ_ONLY";
						$localIsGroupMember = "false";
					}
				}
				else{
					//the user is not logged, check the group privacy pref if view public is enabled
					if(!$viewBulletinPref){
						//ToolMan::redirect("/passport.php");
						ToolMan::redirect("/login.php?redirect=".$_SERVER['REQUEST_URI']);
					}
				}
			}
			else{//Traveler concept
				//The user must be logged
				if(!$travelerID) {
					ToolMan::redirect("/login.php?redirect=".$_SERVER['REQUEST_URI']);
				}
				//added (January 17,2007) check if the traveler has already some friends or if the traveler is a member of a group.
				$localTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
				
				if(isset($hlEntry)){
					if(!$this->file_factory->invokeStaticClass('MessageSpace','isOwner',array($hlEntry->getAttributeID(),$localTraveler->getSendableID(),'bulletin'),true)){
						ToolMan::redirect("/passport.php");	
					}
				}
				
				$grps = $localTraveler->getGroups();
				
				//if(!count($grps)){
				//edited by daf feb.12.07; apply rules that bulletin can't be posted if no groups yet OR no friends yet
				if(!count($grps) && !count($localTraveler->getFriends())){	
					$localMode = "DISABLED";
				}
				else{
					//only the traveler can access his own bulletins
					$localMode = "READ_WRITE";
				}
			}
			
			//for the subnavigation
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext($localConcept);
			if('GROUP'==$localConcept){
				$pageLocation = 'Home';
				$subNavigation->setContextID($localGroupID);
				$subNavigation->setGroupType($localGroupType);
				//$subNavigation->setDoNotShow($localIsGroupMember=="false");
				$objContext = $group;
			}
			else{
				$pageLocation = 'My Passport';
				$subNavigation->setContextID($travelerID);	
				$objContext = $localTraveler;
			}
			$subNavigation->setLinkToHighlight('BULLETIN');
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$localTemplate 	= $template = $this->file_factory->getClass('Template');
			
			$localTemplate->set_path("travellog/views/");
			$localTemplate->set("objContext",$objContext);
			$localTemplate->set("method",$localMethod);
			$localTemplate->set("groupID",$localGroupID);
			$localTemplate->set("travelerID",$travelerID);
			$localTemplate->set("hlEntryID",$localHlEntryID);
			$localTemplate->set("sendableID",$localSendableID);
			$localTemplate->set("concept",$localConcept);
			$localTemplate->set("isAdmin",$localIsAdmin);
			$localTemplate->set("isGroupMember",$localIsGroupMember);
			$localTemplate->set("isSubgroup",$localIsSubgroup);
			$localTemplate->set("groupType",$localGroupType);
			$localTemplate->set("mode",$localMode);
			$localTemplate->set("itemsPerPage",10);
			$localTemplate->set("pageNavPerPage",5);	
			//$localTemplate->set("linkBack",$obj_linkback->CreateLink());
			$localTemplate->set("title",$this->pageTitle);
			$localTemplate->set("pageLocation",$pageLocation);
			$localTemplate->set("subNavigation",$subNavigation);
			$localTemplate->out("tpl.BulletinManagement.php");
		}
		
		protected function setLocalVar($listVarNames,$defVal,$varType="STRING"){
			//explode the varname		
			$arVarNames = explode(",",$listVarNames);		
			foreach($arVarNames as $varName){			
				if(isset($_GET[$varName]) || isset($_POST[$varName])){			
					$localVar = (isset($_GET[$varName]))?$_GET[$varName]:$_POST[$varName];			
					switch($varType){
						case "NUMERIC":										
							if(is_numeric($localVar)) return $localVar;
							break;
						default://STRING
							return $localVar;
					} 			
				}
			}
			return $defVal;
		}
		
	}
?>