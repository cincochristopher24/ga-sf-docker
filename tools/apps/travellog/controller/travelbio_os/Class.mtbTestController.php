<?php
require_once('travellog/model/travelbio_opensocial/testsFolder/Class.ProfileQuestionOs.php');
require_once('travellog/model/travelbio_opensocial/testsFolder/Class.TravelBioUserOs.php');
require_once('travellog/model/travelbio_opensocial/testsFolder/Class.AnswerValueOs.php');
require_once('travellog/model/travelbio_opensocial/testsFolder/Class.AnswerOs.php');
require_once('ganet_web_services/model/Class.WebServiceUser.php');
require_once('travellog/controller/travelbio_os/Class.mtbDashboard.php');
require_once('Class.Template.php');

class mtbTestController{
	
	private $action,
			$container,
			$id,
			$isOwner,
			$view,
			$travelBio,
			$activeTab,
			$serviceUser;
	
	const
		TRAVELBIO 	= 'showBio',
		SYNC		= 'doSync',
		SYNCHRONIZE	= 'synchronize',
		EDITBIO		= 'edit',
		SAVEBIO		= 'saveBio',
		SETTINGS	= 'settings',
		KEEP_DOTNET	= 'keepDotNet',
		KEEP_OS		= 'keepOpenSocial',
		SAVESETTING	= 'saveSettings',
		NONE_OWNER	= 'friend_viewer';
		
	function __construct(){
		var_dump($_POST);
		$this->action = isset($_GET['action']) ? $_GET['action'] : "default";
		$this->container = isset($_POST['domain']) ? $_POST['domain'] : NULL;
		$this->id = isset($_POST['id']) ? $_POST['id'] : NULL;
		$this->isOwner = isset($_POST['isOwner']) ? $_POST['isOwner'] : NULL;
		$this->view = isset($_POST['viewPage']) ? $_POST['viewPage'] : "";
		//echo $this->id;
		if(!is_numeric($this->id)){
			$this->id = preg_replace("/[^0-9]/", "", $this->id);
		}
		
		$this->setServiceUserObj();
		
	//	$this->serviceUser = new WebServiceuser($this->id,$this->container);
	}
	
	function setServiceUserObj(){
		$this->serviceUser = new WebServiceuser($this->id,$this->container);
		$this->travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
		
		if($this->isOwner == "true"){
			$this->activeTab = true;
		}
		else {
			$this->activeTab = false;
		}
	}
	
	function performAction(){

		if( 0 == $this->serviceUser->getID()){
			$this->serviceUser->setID( WebServiceUser::addUser($this->serviceUser->getWsID(), $this->container) );
			$this->setServiceUserObj();
		}
		$x = $this->serviceUser->hasApp('myTravelBio');
	//	echo "has app? = ".$x;
		if(!$this->serviceUser->hasApp('myTravelBio')){
			MyTravelBioApp::addUser($this->serviceUser->getID());
			$this->setServiceUserObj();
		}
		
		
	//	echo "<br>answer</br>";
	//	var_dump($_POST);
	//	echo "<br>structans</br>";
	//	var_dump($structAns);
		/*
				$conn_2 = new dbHandler();		
				$conn->setDB('SocialApplication');
				$sql_1 = "INSERT INTO `tblTravelBioUser` SET `dateAdded` = NOW()";
				$conn->execQuery($sql_1);
				echo $sql_1;
				$bioUserID = $conn->getLastInsertedID();
				echo $bioUserID;
		*/
		$_isSync = WebServiceUser::isSynchronized($this->id,$this->container);
	//	echo "<br/>isSync = ".WebServiceUser::isSynchronized($this->id,$this->container);
	/*	if(!$_isSync){
			$view = isset($_POST['viewPage']) ? $_POST['viewPage'] : "";
			if((!$this->travelBio->getAnswers()) && $view == "profile"){
				$this->action = "";
			}
		} */
		if($this->isOwner == "false" || $this->view == "profile"){
			$this->action = "friend_viewer";
		}
		
			switch($this->action){
				case self::TRAVELBIO:
					if($_isSync){
						if(!MyTravelBioApp::hasTravelBioID($this->serviceUser->getID())){
							MyTravelBioApp::updateTravelBioUserOS($this->serviceUser->getTravelerID(),$this->serviceUser->getID());
						}
						$this->setServiceUserObj();
					//	if(isset($_POST['viewPage']) && $_POST['viewPage']=="home"){
					//		$this->showAppOwnerInfo();
					//		$html ="<div>
					//				<img src='http://www.goabroad.net/opensocial/myTraveBio/images/screenshot.png'/>
					//				</div>";
					//		echo $html;
					//	}
					//	else {
					//		$this->showIndex();
					//	}
					}
					//else {
					if($this->travelBio->getAnswers()){
						$this->showIndex();
					}
					else {
						$this->showEditBioForm();
					}
					//	$this->showSyncOrImage();
						
					//}
					break;
				case self::SYNCHRONIZE:
					$this->showSyncForm();
					break;
				case self::SYNC:
					$this->synchronizeUser();
					break;
				case self::SETTINGS:
					$this->showSettingsForm();
					break;
				case self::SAVESETTING:
					$this->saveBioSettings();
					break;
				case self::EDITBIO:
					$this->showEditBioForm();
					break;
				case self::SAVEBIO:
					$this->saveBio();
					break;
				case self::KEEP_DOTNET:
					$this->syncWithDotNetBio();
					break;
				case self::KEEP_OS:
					$this->syncWithOpenSocialBio();
					break;
				case self::NONE_OWNER:
					//if($this->travelBio->getAnswers()){
						$this->showIndex();
					//}
					//else {
						
					//}
					break;
				default:
					$this->showDefaultNoneSynchronizeProfile();
					break;
			} 
			
	}
	
	function showIndex(){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
		
		if($this->activeTab){
			$dash = new mtbDashboard(1);
		}
		
		$structAns = array();
		$answers = $travelBio->getAnswers();

		foreach($answers as $iAns){
			if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
				$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
			}
		}
		
		$viewPage = isset($_POST['viewPage']) ? $_POST['viewPage'] : null;
		$ownerName = isset($_POST['ownerName']) ? $_POST['ownerName'] : null;
		
		$profileQuestions = ProfileQuestionOs::getQuestions();
		$tpl = new Template();
		$tpl->setPath('travellog/views/travelbio_os/');
		$tpl->set('travelBio',$travelBio);
		$tpl->set('profileQuestions',$profileQuestions);
		$tpl->set('isOwner',$this->isOwner);
		$tpl->set('ownerName',$ownerName);
		$tpl->set('viewPage',$viewPage);
		$tpl->set('structAns',$structAns);
		$tpl->out('tpl.mytravelbio.php');
	}
	
	function showSyncForm($error = ''){
		$tpl = new Template();
		
		if($this->activeTab){
			$dash = new mtbDashboard(3);
		}
		
		$tpl->setPath('travellog/views/travelbio_os/');
		
		if(WebServiceUser::isSynchronized($this->id,$this->container)){
			$tpl->set('dotNetUsername',$this->serviceUser->getDotNetUsername());
		}
		else {
			$tpl->set('dotNetUsername','');
		}
		
		$tpl->set('error',$error);
		$tpl->set('container',$this->container);
		$tpl->out('tpl.SyncForm.php');
	}
	
	function showSettingsForm($message = ""){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
		
		if($this->activeTab){
			$dash = new mtbDashboard(4);
		}
		
		$profileQuestions = ProfileQuestionOs::getQuestions();
		$tpl = new Template();
		$tpl->setPath('travellog/views/travelbio_os/');
		$tpl->set('profileQuestions',$profileQuestions);
		$tpl->set('travelBio',$travelBio);
		$tpl->set('message',$message);
		$tpl->out('tpl.Settings.php');
	}
	
	function showEditBioForm($message = ""){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());

		$structAns = array();
		$answers = $travelBio->getAnswers();

		foreach($answers as $iAns){
			if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
				$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
			}
		}
		$profileQuestions = ProfileQuestionOs::getQuestions();
		
		if($this->activeTab){
			$dash = new mtbDashboard(2);
		}
	//	echo "edit";
	//	var_dump($profileQuestions);
		$tpl = new Template();
		$tpl->setPath('travellog/views/travelbio_os/');
		$tpl->set('profileQuestions',$profileQuestions);
	//	$tpl->set('css',file_get_contents('http://www.goabroad.net/css/travelbio/fbedit.css'));
		$tpl->set('structAns',$structAns);
		$tpl->set('message', $message);
		$tpl->out('tpl.edit.php');
	}
	
	function synchronizeUser(){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
			
		$email = isset($_POST['email']) ? $_POST['email'] : "";
		$pass  = isset($_POST['pass']) ? $_POST['pass'] : "";
	//	echo "<br/>email = ".$email;
	//	echo "<br/>pass = ".$pass;
		$error = "";
		$reSync = false;
		if( (!empty($email)) && (!empty($pass)) ):
		
			$userInfo = array(
						"oldUserID" => $this->serviceUser->getID(),
						"bioUserID" => $travelBio->getTravelBioUserID(),
						"domain"    => $this->serviceUser->getDomain(),
						"osUserID"  => $this->serviceUser->getWsID()
						);

			$newUser = $this->serviceUser->validateGAAccountWithEmail($email, $pass);
		//	echo "<br/>new user = ";
		//	var_dump($newUser);
			if(isset($newUser['travelerID']) && (0 < $newUser['travelerID']) ){
				$newUser['newBioUserId'] = $travelBio->getDotnetBioUserIdByTravelerID($newUser['travelerID']);
				
				if(is_array($newUser) && ($newUser['isSuspended'] == 0) && ($newUser['isAdvisor'] == NULL)){

				//	$isSync = WebServiceUser::isSynchronized($this->serviceUser->getWsID(),$this->serviceUser->getDomain());
				//	echo "<br/>isSync?=".$isSync;
					if(WebServiceUser::isSynchronized($this->serviceUser->getWsID(),$this->serviceUser->getDomain())){
					//	echo "<br/>re-sync!";
						$this->serviceUser->reSynchronize($newUser,$userInfo);
						TravelBioUserOs::reSyncOsUpdate($newUser,$userInfo);
						$reSync = true;
					}
					else {
						$dotNetBio = TravelBioUserOs::getBioUserIdByServiceUserID($newUser['userID']);
						$open_answers = $travelBio->getAnswers();
						$dotnet_answers = $dotNetBio->getAnswers();
						if($open_answers && $dotnet_answers){
						//	echo "both have answers";
							if($this->activeTab){
								$dash = new mtbDashboard(3);
							}
							
							$tpl = new Template();
							$tpl->setPath('travellog/views/travelbio_os/');
							$tpl->set('travelerID', $newUser['travelerID']);
							$tpl->set('container',$this->container);
							$tpl->out('tpl.dualAccount.php');
						}
						elseif((!$dotnet_answers) && $open_answers ) {
							
						//	echo "no dotnet answers";
							$travelBio->delete();
							$travelBio->setTravelBioUserID($newUser['newBioUserId']);
							$travelBio->updateAnswers($userInfo['bioUserID']);

							$this->serviceUser->firstSynchronize($newUser,$userInfo);
							TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);
							$this->setServiceUserObj();
							$this->showSettingsForm();
					
						}
						else {
						//	echo "default";
							$travelBio->delete();
							$this->serviceUser->firstSynchronize($newUser,$userInfo);
							TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);
							$this->setServiceUserObj();
							$this->showSettingsForm();
						}
						
					//	$this->serviceUser->firstSynchronize($newUser,$userInfo);
					//	TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);
					//	echo "<br/>first timer!";
					// reset serviceUser object to point to the newly sync
					//	$this->serviceUser = new WebServiceuser($this->id,$this->container);
					//	$this->setServiceUserObj();
					//	$this->showSettingsForm();
					}
					
				}
				else{
					$error = "Sorry, process cannot be completed because the account that you are tryng to synchronize with ";
					if(0 < $newUser['isAdvisor']){
						$error .= "is an advisor account. Only traveler account is allowed!";
					}
					elseif($newUser['isSuspended'] == 1){
						$error .= "is suspended. Please try another account or contact Goabroad Network now.";
					}
					
				}
				
			}
			else{
				// overwrite error message
				$error = "No match found! Try again!";
			}
			
		
		endif;

		if( ($reSync) || ($error != "") ){
			$this->setServiceUserObj();
			$this->showSyncForm($error);
		}
		
		
	}
	
	function syncWithDotNetBio(){
		$travelerID = isset($_POST['travelerID']) ? $_POST['travelerID'] : 0;
		if(0 < $travelerID){
			$userInfo = array(
						"oldUserID" => $this->serviceUser->getID(),
						"bioUserID" => $this->travelBio->getTravelBioUserID(),
						"domain"    => $this->serviceUser->getDomain(),
						"osUserID"  => $this->serviceUser->getWsID()
						);
			$newUser['travelerID'] = $travelerID;
			$newUser['userID'] = $this->serviceUser->getWebServiceUserByTravelerID($travelerID);
			$newUser['newBioUserId'] = TravelBioUserOs::getDotnetBioUserIdByTravelerID($travelerID);
			
			$dotnet = TravelBioUserOs::getBioUserIdByServiceUserID($newUser['userID']);
			$opensocial = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
			
			$opensocial->delete();
			$opensocial->clearAnswers();

			$this->serviceUser->firstSynchronize($newUser,$userInfo);
			TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);
			$this->setServiceUserObj();
			$this->showSettingsForm();
			
		}
	}
	
	function syncWithOpenSocialBio(){
		$travelerID = isset($_POST['travelerID']) ? $_POST['travelerID'] : 0;
		if(0 < $travelerID){
			$userInfo = array(
						"oldUserID" => $this->serviceUser->getID(),
						"bioUserID" => $this->travelBio->getTravelBioUserID(),
						"domain"    => $this->serviceUser->getDomain(),
						"osUserID"  => $this->serviceUser->getWsID()
						);
			$newUser['travelerID'] = $travelerID;
			$newUser['userID'] = $this->serviceUser->getWebServiceUserByTravelerID($travelerID);
			$newUser['newBioUserId'] = TravelBioUserOs::getDotnetBioUserIdByTravelerID($travelerID);
			
			$dotnet = TravelBioUserOs::getBioUserIdByServiceUserID($newUser['userID']);
			$opensocial = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
			
			$opensocial->delete();
			$dotnet->clearAnswers();

			$opensocial->setTravelBioUserID($newUser['newBioUserId']);
			$opensocial->updateAnswers($userInfo['bioUserID']);

			$this->serviceUser->firstSynchronize($newUser,$userInfo);
			TravelBioUserOs::firstOsSyncUpdate($newUser,$userInfo);

			$this->setServiceUserObj();
			$this->showSettingsForm();
		}
	}
	
	function saveBioSettings(){
		//echo "save settings";
		
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());
		$profileQuestions = ProfileQuestionOs::getQuestions();
		if($_POST['numQuestionToDisplay'] == 'ALL'){
			$numQ = count($profileQuestions);
		}
		else{
			$numQ = $_POST['numQuestionToDisplay'];
		}
		//echo $numQ;
		//update the default profile box fbml of the current application user
		$travelBio->setNumOfQuestionsDisplayed($numQ);

		$travelBio->save();
		
		$message = "Changes to your settings have been successfully saved!";
		
		$this->showSettingsForm($message);
	}
	
	function saveBio(){
		//echo "save bio";
		
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());

		$arPrefixes = array(
			'txt'=>AnswerValueOs::TEXT,
			'txa'=>AnswerValueOs::TEXTAREA,
			'chk'=>AnswerValueOs::CHECKBOX,
			'txtchk'=>AnswerValueOs::TEXT
		);

		$travelBio->clearAnswers();

		$answerRepo = array();
		$withAns = false;
		$withOthers = false;

		foreach($_POST as $key => $val):

			$part = explode('_',$key);


			if(array_key_exists($part[0],$arPrefixes)):

				$answer = new AnswerOs();
				$answer->setProfileQuestionID($part[1]);
				$answer->setTravelBioUserID($travelBio->getTravelBioUserID());

				if( 'chk'==$part[0]){
					$val = explode(',',$val);
				}
				else {
					$val = array($val);
				}
				
				foreach($val as $iVal):
					if('Others'==$iVal){
						$withOthers = true;
					}
					if( strlen($iVal) ){
						if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
							$withAns = true;
							$answer->addValue($arPrefixes[$part[0]],$iVal);
						}
					}

				endforeach;

				if(!array_key_exists($part[1],$answerRepo)){
					$answerRepo[$part[1]] = $answer;
				}
				else{
					if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
						$answerRepo[$part[1]]->addValue($arPrefixes[$part[0]],$iVal);
					}
				}

			endif;

		endforeach;

		foreach($answerRepo as $iAns){
			if( 0 < count($iAns->getAddedValue()) ){
				$iAns->save();
			}
		}
		
		$message = "Changes successfully saved!";
		$this->showEditBioForm($message);
	}
	
	function showAppOwnerInfo(){
		$travelBio = TravelBioUserOs::getBioUserIdByServiceUserID($this->serviceUser->getID());

		$answers = $travelBio->getAnswers();
	//	echo count($answers);
		
		$html = "<div class='content' style='font-size: 16px;color:#444;'><p><strong>".$_POST['ownerName']."</strong> has answered <em>".count($answers)."</em> ";
		$html .="out of 16 items in <a href='javascript:void(0);' onclick='gotoCanvas();'>Travel Bio</a></p>";
		$html .="<p>If you want to edit any of your answers please go to <em>Update Bio</em> in the <a href='javascript:void(0);' onclick='gotoCanvas()'>CANVAS</a> page.</p></div>";
	//	var_dump($this->serviceUser);
	//	$this->serviceUser->getDotNetUsername();
	//	$_POST['ownerName'];
	/*	$html = "<div style='height:100px;color: #444;'>
				<img src='http://www.goabroad.net/opensocial/myTraveBio/images/screenshot2.png'/>
				<p style='font-weight:bold;padding:5px;'>This app is developed for travelers. Just a simple question and answer application.</p></div>";
	*/	echo $html;
	}
	
	function showSyncOrImage(){
		$viewPage = isset($_POST['viewPage']) ? $_POST['viewPage'] : "";
		if($viewPage == "canvas"){
			$this->showSyncForm();
		}
		else{
			$html = "<div>
					<img src='http://www.goabroad.net/opensocial/myTraveBio/images/screenshot.png'/>";
			$html .="<p>Please sync your account to your GoAbroad Network account <a href='javascript:void(0);' onclick='gotoCanvas();'>here</a>.</p>
					</div>";
			echo $html;
		}
	}
	
	function showDefaultNoneSynchronizeProfile(){
		$html = "<div>";
		$html .="<p style='font-weight:bold;font-size:16px;'>You have not answered your Travel Bio yet. When you're ready to answer, just go to the <a href='javascript:void(0);' onclick='gotoCanvas();'>canvas page</a> of your Travel Bio.</p>
				</div>";
		echo $html;
	}
	
}
?>