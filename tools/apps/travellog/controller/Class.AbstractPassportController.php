<?php
	//session_cache_limiter('public,must-revalidate');
	require_once ("travellog/controller/Class.IController.php");
	require_once ("travellog/factory/Class.FileFactory.php");

	class AbstractPassportController {
		protected $obj_session = NULL, $file_factory = NULL;

		function __construct() {
			
			$this->file_factory = FileFactory :: getInstance();
			$this->file_factory->registerClass('Template', array (
				'path' => ''
			));
			$this->file_factory->registerClass('HelperGlobal', array (
				'path' => ''
			));
			$this->file_factory->registerClass('SessionManager', array (
				"path" => "travellog/model/"
			));

			/*************** START -- Added BY: Cheryl Ivy Q. Go  20 February 2008 ********************/
			//$this->file_factory->registerClass('TravelBioUser' , array('path' => 'travellog/model/travelbio/'));
			//$this->file_factory->registerClass('ProfileQuestion' , array('path' => 'travellog/model/travelbio/'));
			/*************** END -- Added BY: Cheryl Ivy Q. Go  20 February 2008 ********************/

			$this->file_factory->registerTemplate('LayoutMain');

			$this->file_factory->setPath('CSS', '/min/f=css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		}

		function performAction() {
			/***************************************************
			 * check if logged travelerid > 0
			 */
			$sessMan = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance', array ());
		
		
			/*added by jul for pending cobrand members*/
			if( isset($GLOBALS['CONFIG']) && $sessMan->getLogin() ){
				$this->file_factory->registerClass('GroupFactory');
				$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($GLOBALS['CONFIG']->getGroupID()));
				if( count($_group) ){
					$group = $_group[0];
					if( !$group->isMember(new Traveler($sessMan->get("travelerID"))) ){
						header("location: /membership.php?gID=".$GLOBALS['CONFIG']->getGroupID()."&mode=acceptinvite");
						exit;
					}
				}
			}
		
			// edited by: marc
			if ( !$sessMan->getLogin()) {
				$this->file_factory->invokeStaticClass('HelperGlobal', 'getLoginPage');
			}
			else{
				$this->checkForFeatureChangeNotice($sessMan);
			}
			
			// TEMP forward everything to profile
			//header('location: /profile.php?travelerID='.$travelerID );
			
			//move to traveler dashboard
			header('location: /travelerdashboard.php');
			exit;	
		}

		protected function performAjaxAction() {
		
			$sessMan = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance', array ());
			$travelerID = $sessMan->get('travelerID');
		
			isset ($_REQUEST['action']) ? $action = $_REQUEST['action'] : $action = '';
		
			if (!$sessMan->getLogin()) {
				// allow loadjournalentries action to be able to run even if user is not logged in
				if ($action != 'loadjournalentries') {
					echo 'Session already expired. Click my passport tab to relogin.';
					return;
				}
			}
	
			/*********************************************
			 *  for Message Center (display of messages)
			 */
			if ($action == 'showmessages') {
				$viewType = $_POST['viewType'];

				$obj_traveler = $this->file_factory->getClass('Traveler', array (
					$travelerID
				));
				$obj_message_panel = $this->file_factory->getClass('MessagesPanel');
				$obj_message_panel->setContext($obj_traveler);

				/******** last edited by: cheryl go			28 august 2008 ***********/
				/******* purpose: remove view all link if there is no message ********/

				if ($viewType == MessagesPanel::$BULLETIN)
					$obj_message_panel->getBulletinsForMessagesPanel();
				else if ($viewType == MessagesPanel::$PERSONAL_MESSAGE)
					$obj_message_panel->getPersonalMessagesForMessagesPanel();

				switch ($viewType) {
					case 1:
						echo '<div class="header_actions"><a class="add compose_msg" title="Send your friends private messages." href="messages.php?compose">Compose Message</a>&nbsp;|&nbsp;<a class="add" href="bulletinmanagement.php?method=compose">New Bulletin Post</a></div>';

						break;
					case 2 : // Personal messages add link
						echo '<div class="header_actions">
								<a class="add compose_msg" title="Send your friends private messages." href="messages.php?compose">Compose new message</a>';
						if (0 < $obj_message_panel->getTotalNumberOfMessages())		
							echo '&nbsp;|&nbsp;<a href="messages.php" class="more">View all</a>';
						echo '</div>';

						break;
					case 3 :
						echo '<div class="header_actions">
								<a class="add bulletin_post" title="Post news or updates you want to share with your friends." href="bulletinmanagement.php?method=compose">Post new bulletin</a>';
						if (0 < $obj_message_panel->getTotalNumberOfMessages())
							echo '&nbsp;|&nbsp;<a href="bulletinmanagement.php" class="more">View all</a>';
						echo '</div>';

						break;
					default :
						echo '';

				}
				echo $obj_message_panel->render($viewType);

				/************************ end of edit ****************************/

				if(1==$viewType){
					echo '<script>jQuery("#msgTotal").html("'.$obj_message_panel->getTotalNumberOfNewMessages().'");</script>';
				}
			}
		
			if ($action == 'loadjournalentries'){
				require_once('travellog/views/Class.TravelsView.php');
				$tid = $_REQUEST['travelid'];			
				$tv = new TravelsView($this->file_factory->getClass('Traveler', array ($travelerID)));
				echo $tv->renderAjTravelEntries($tid);
			}
		
			if ($action == 'deletejournalentry'){
				require_once('travellog/model/Class.TravelLog.php');
				$tlogid = $_REQUEST['travellogID'];
				$tl = new TravelLog($tlogid);
				try {
					$tl->Delete();
					$t = 1;
				}catch (Exception $e){
					header("HTTP/1.0 500 Internal Server Error");
					exit;
				}
				header("HTTP/1.0 200 OK");			
			}
		
			if($action == 'deleteAlert'){
				try{	
					require_once('travellog/model/Class.AlertMessage.php');
					$msg = new AlertMessage($_POST['attrID']);
				}
				catch(exception $e){
					header("HTTP/1.0 500 Internal Server Error");
					exit;
				}

				$messageSpace = $this->file_factory->getClass('MessageSpace');
				$messageSpace->delete($_POST['attrID'], $msg->getDiscriminator(), $msg->getMessageId());
				$_REQUEST['action'] = 'showmessages';
				$this->performAction();
			}
		}
		
		protected function checkForFeatureChangeNotice(SessionManager $sessMan){
			$travelerID = $sessMan->get('travelerID');
			$this->file_factory->registerClass('Traveler');
			$traveler = $this->file_factory->getClass('Traveler', array($travelerID));
			if( !$sessMan->get("FEATURE_CHANGE_NOTICE_SHOWN") ){
				try{
					$group = FALSE;
					if( isset($GLOBALS['CONFIG']) ){
						$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($GLOBALS['CONFIG']->getGroupID()));
						if( count($_group) ){
							$group = $_group[0];
						}
					}
					if( $traveler->getIsAdvisor() ){
						$this->file_factory->registerClass('FeatureChangeNoticeHelperFactory', array("path" => "travellog/factory/"));
						$noticeHelper = $this->file_factory->invokeStaticClass ('FeatureChangeNoticeHelperFactory','instance')->create("ADVISOR_ACCOUNT");
						if( $advisorGroupID = $noticeHelper->isFeatureChangeNoticeShown($travelerID) ){
							if( !isset($GLOBALS['CONFIG']) ){
								$this->file_factory->registerClass('GroupFactory');
								$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($advisorGroupID));
								if( count($_group) ){
									$group = $_group[0];
								}
							}
							if( $group ){
								$noticeTemplateView = $noticeHelper->getFeatureChangeNoticeTemplateView($traveler,$group);
								$this->showFeatureChangeNotice($noticeTemplateView);
								exit;
							}
						}else{
							$noticeHelper = $this->file_factory->invokeStaticClass ('FeatureChangeNoticeHelperFactory','instance')->create("DASHBOARD_FEATURE");
							if( $noticeHelper->isFeatureChangeNoticeShown($travelerID) ){
								$noticeTemplateView = $noticeHelper->getFeatureChangeNoticeTemplateView($traveler,$group);
								$this->showFeatureChangeNotice($noticeTemplateView);
								exit;
							}
						}
					}else{
						$this->file_factory->registerClass('FeatureChangeNoticeHelperFactory', array("path" => "travellog/factory/"));
						$noticeHelper = $this->file_factory->invokeStaticClass ('FeatureChangeNoticeHelperFactory','instance')->create("DASHBOARD_FEATURE");
						if( $noticeHelper->isFeatureChangeNoticeShown($travelerID) ){
							$noticeTemplateView = $noticeHelper->getFeatureChangeNoticeTemplateView($traveler,$group);
							$this->showFeatureChangeNotice($noticeTemplateView);
							exit;
						}
					}
				}catch(exception $e){
				}
			}
		}
		
		protected function showFeatureChangeNotice($noticeTemplateView){			
			$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$featureChangeNoticeTemplate = $this->file_factory->getClass('Template');
			$featureChangeNoticeTemplate->set_path('travellog/views/');
			
			Template::setMainTemplateVar('page_location', 'My Passport');
			
			$featureChangeNoticeTemplate->set("noticeTemplateView",$noticeTemplateView);
			$featureChangeNoticeTemplate->out("tpl.FeatureChangeNoticeTemplate.php");
			
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('SessionManager', array("path" => "travellog/model/"));
			$session_manager = $this->file_factory->invokeStaticClass('SessionManager','getInstance',array());
			$session_manager->set("FEATURE_CHANGE_NOTICE_SHOWN",TRUE);
		}
	}
?>