<?php
require_once("travellog/controller/Class.IController.php"); 
require_once("travellog/factory/Class.FileFactory.php");
abstract class AbstractTravelersController implements IController{
	
	protected static $VALID_ACTIONS = array('LASTLOGIN', 'INMYHOMETOWN', 'NEWEST', 'MOSTPOPULAR', 'CURRENTLYIN', 'PLANNINGTOGO', 'HAVEBEENTO', 'SEARCH'); 
	
	protected 
	
	$obj_session  = NULL,
	
	$file_factory = NULL; 
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance(); 
		
		$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('TravelersFcd'  , array('path' => 'travellog/facade/'));
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('HelpText');
		$this->file_factory->registerClass('Map');
		
		$this->file_factory->registerClass('AllTravelers'         , array('path' => 'travellog/model/travelers/', 'file' => 'AllTravelersModel'));
		$this->file_factory->registerClass('NewestTraveler'       , array('path' => 'travellog/model/travelers/', 'file' => 'NewestTravelerModel'));
		$this->file_factory->registerClass('PopularTravelers'     , array('path' => 'travellog/model/travelers/', 'file' => 'PopularTravelerModel'));
		$this->file_factory->registerClass('SearchTravelers'      , array('path' => 'travellog/model/travelers/', 'file' => 'KeywordsSearchTravelerModel'));
		$this->file_factory->registerClass('LastLoginTravelers'   , array('path' => 'travellog/model/travelers/', 'file' => 'LatestTravelerModel'));
		$this->file_factory->registerClass('InMyHometownTravelers', array('path' => 'travellog/model/travelers/', 'file' => 'InMyHometownTravelerModel'));
		$this->file_factory->registerClass('HometownTravelers'    , array('path' => 'travellog/model/travelers/', 'file' => 'HomeTravelerModel'));
		$this->file_factory->registerClass('CurrentTravelers'     , array('path' => 'travellog/model/travelers/', 'file' => 'CurrentTravelerModel'));
		$this->file_factory->registerClass('PlanningToGoTo'       , array('path' => 'travellog/model/travelers/', 'file' => 'TravelersPlanningToGoModel'));
		$this->file_factory->registerClass('KeywordsSearch'       , array('path' => 'travellog/model/travelers/', 'file' => 'KeywordsSearchModel'));
		$this->file_factory->registerClass('LocationIDList'       , array('path' => 'travellog/repository/'     , 'file' => 'LocationIDListBaseOnCountryLocationID'));
		$this->file_factory->registerClass('CountryPhotosCount'   , array('path' => 'travellog/repository/'     , 'file' => 'CountryPhotosCount')); 
		$this->file_factory->registerClass('TravelerLocationCount', array('path' => 'travellog/repository/'     , 'file' => 'TravelerLocationCount'));
		$this->file_factory->registerClass('EntriesWritenCount'   , array('path' => 'travellog/repository/'     , 'file' => 'EntriesWritenCount'));
		$this->file_factory->registerClass('TravelersMap'         , array('file' => 'MapModel'));
		$this->file_factory->registerClass('MapFrame'             , array('file' => 'Map', 'class' => 'MapFrame'));
		$this->file_factory->registerClass('HaveBeenTo'           , array('path' => 'travellog/model/travelers/', 'file' => 'TravelersHaveBeenToModel'));
				
		$this->file_factory->registerTemplate('LayoutMain');
		
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		
		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
	}
	
	function performAction(){
		
		/*added by jul for pending cobrand members*/
		if( isset($GLOBALS['CONFIG']) && $this->obj_session->getLogin() ){
			$this->file_factory->registerClass('GroupFactory');
			$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($GLOBALS['CONFIG']->getGroupID()));
			$group = $_group[0];
			if( !$group->isMember(new Traveler($this->obj_session->get("travelerID"))) ){
				header("location: /membership.php?gID=".$GLOBALS['CONFIG']->getGroupID()."&mode=acceptinvite"); exit;
			}
		}
		
		$_GET['action'] = ( isset($_GET['action']))? $_GET['action'] : 'LastLogin';
		switch($_GET['action']){
			case "PlanningToGoJSONData":
				$this->_viewPlanningToGoJSONData(); 
			break;
			
			case "CurrentlyInJSONData":
				$this->_viewCurrentlyInJSONData(); 
			break;
			
			case "HaveBeenToJSONData":
				$this->_viewHaveBeenToJSONData(); 
			break;
			
			case "view_map":
				$this->_viewMap();
			break;
			default:
				$this->_viewTravelers();
		}
	}
	
	protected function _viewHaveBeenToJSONData(){
		$obj_location = $this->file_factory->getClass('HaveBeenTo');		
		echo $obj_location->GetTravelersCountryWithPastTravelsJSON();
	}
	
	protected function _viewCurrentlyInJSONData(){
		$obj_location = $this->file_factory->getClass('CurrentTravelers');	
		echo $obj_location->GetCountriesWithCurrentLocationJSON();
	}
	
	protected function _viewPlanningToGoJSONData(){
		$obj_location = $this->file_factory->getClass('PlanningToGoTo');
		echo $obj_location->GetTravelersCountryWithFutureTravelsJSON();
	}
	
	protected function _viewMap(){
		require_once("travellog/model/Class.Country.php");
		//
		$props = array();
		$props['view_type'] = isset($_GET['view_type']) ? $_GET['view_type'] : 1; 
		$props['type'] = ( isset($_GET['action']))? $_GET['action'] : 'LastLogin';
		$this->file_factory->getClass('TravelersMap')->GetTravelerMap($props);
		$map_frame = $this->file_factory->getClass('MapFrame');
		$map_frame->setConnectMarkers(false);		
		$map = $this->file_factory->getClass('Map');
		echo $map->plotLocationsThroughAjax($props['arr_marker'], $map_frame, "_map0");
	}
	
	protected function _viewTravelers(){
		$props               = array();
		$props['page']       = ( isset($_GET['page']))? $_GET['page'] : 1;
		$props['type']       = ( isset($_GET['action']))? $_GET['action'] : 'LastLogin';
		$props['locationID'] = ( isset($_GET['locationID']) && $_GET['locationID'] )? $_GET['locationID'] : 0;
		
		if (0 == intval($props['page'])){
			header("location: /travelers.php"); exit;
		}
		$type = strtoupper($props['type']);	
		if (!in_array($type, self::$VALID_ACTIONS)){
			header("location: /travelers.php"); exit;
		}
		for ($i=4; $i<7; $i++){
			if ($type === self::$VALID_ACTIONS[$i] && 0 == $props['locationID']){
				header("location: /travelers.php"); exit;
			}
		}
		$props['search_by']  = ( isset($_GET['searchby']) && $_GET['searchby'] )? $_GET['searchby']: 1;
		$props['keywords']   = ( isset($_GET['keywords']) )? $_GET['keywords']: '';
		
		$obj_view = $this->file_factory->getClass('MainLayoutView');
		$obj_map  = $this->file_factory->getClass('Map');
				
		$props['travelerID']    = $this->obj_session->get('travelerID');
		$props['is_login']      = $this->obj_session->getLogin();
		//$props['obj_help_text'] = $this->file_factory->getClass('HelpText');
		if( $props['is_login'] ){
			$props['obj_traveler'] = $this->file_factory->getClass('Traveler', array($props['travelerID']));
			$props['is_advisor']   = $props['obj_traveler']->isAdvisor(); 
		}
		else $props['is_advisor'] = false;
		$props['rows']          = 12;
		$props['offset']        = ($props['page']*$props['rows'])-$props['rows'];
		$props['view_type']     = 1; 
		$props['location_type'] = 0;
		$locationID             = $props['locationID'];
		
		//  edit by: Nash, Oct. 12, 2011
		//  reason: the sql in the GetTravelersCountryWithCurrentLocation method is causing the db to lock 
		//  		when in cobrand mode
		if(isset($GLOBALS['CONFIG']) AND $GLOBALS['CONFIG']->getGroupID() > 0){
			$props['numTravelersCurrentlyIn']	 	= 0;
			$props['numTravelersPlanningToGoTo'] 	= 0;
			$props['numTravelersHaveBeenTo'] 		= 0;
		}
		else {
			$props['numTravelersCurrentlyIn']	 	= count($this->file_factory->getClass('CurrentTravelers')->GetTravelersCountryWithCurrentLocation());
			$props['numTravelersPlanningToGoTo'] 	= count($this->file_factory->getClass('PlanningToGoTo')->GetTravelersCountryWithFutureTravels());
			$props['numTravelersHaveBeenTo'] 		= count($this->file_factory->getClass('HaveBeenTo')->GetTravelersCountryWithPastTravels());
		}
		
		ob_start();
			$obj_map->includeJs();
			$google_map_include = ob_get_contents(); 
		ob_end_clean(); 
		
		$code = <<<BOF
		<script type="text/javascript">
		//<![CDATA[
			locationID = $locationID;
		//]]>
		</script>
BOF;
		if( isset($GLOBALS['CONFIG']) )  
			Template::setMainTemplateVar('title', 'Meet Interesting Travelers on the ' .$GLOBALS['CONFIG']->getSiteName(). ' Network');      
		else
			Template::setMainTemplateVar( 'title'          , 'Meet Interesting Travelers on the GoAbroad Network');
			
		Template::setMainTemplateVar( 'metaDescription', 'Travelers with various interests constantly add their journeys to the Network. Browse the growing travelers list and meet interesting people.');
		Template::setMainTemplateVar( 'layoutID'       , 'travelers/travelers'       );
		Template::setMainTemplateVar( 'page_location'  , 'Travelers'                 );
		//Template::includeDependentJs( "/js/jquery-1.1.4.pack.js", array( "bottom" => true));
		Template::includeDependent  ( $code, array( "bottom" => true));
		Template::includeDependent  ( $google_map_include, array("bottom" => true));
		Template::includeDependentJs( "/js/jquery.checkbox.js", array("bottom" => true));
		Template::includeDependentJs( "/js/traveler/search_form.js", array("bottom" => true));
		Template::setMainTemplate   ( $this->file_factory->getTemplate('LayoutMain') );

		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentCss('/css/Thickbox.css');
		Template::includeDependentJs('/js/thickbox.js');

		$fcd = $this->file_factory->getClass('TravelersFcd', $props, false);
		$obj_view->setContents( array('contents' => $fcd->retrieveByContextView()) ); 
		
		$obj_view->render();
	}
}
?>
