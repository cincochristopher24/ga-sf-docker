<?php
/*
 * Created on 11 12, 07
 * 
 * Author: J. LLano
 * Purpose: The default controller for "Photo" module in goabroad.net
 * 
 */
 
 require_once('travellog/controller/Class.AbstractPhotoController.php');
 
 class GANETPhotoController extends AbstractPhotoController {
 	function performAction(){
		parent::performAction();
	}
 }
?>
