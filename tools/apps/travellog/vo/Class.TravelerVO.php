<?php
require_once("travellog/vo/Class.AbstractProfileVO.php");
class TravelerVO extends AbstractProfileVO{
	private
	
	$mIsAdvisor = false,
	
	$mUserName  = NULL,
	
	$mAge       = 0,
	
	$mCity      = NULL,
	
	$mCountry   = NULL,
	
	$mInterests = NULL; 
	
	function __call($method, $arguments) {
        $prefix   = substr($method, 0, 3);
        $property = 'm'.substr($method, 3);
		
		if (empty($prefix) || empty($property)) {
            return;
        }
		if (property_exists($this,$property)){
	        if ($prefix == "get" && isset($this->$property)) {
	            return $this->$property;
	        }
	        if ($prefix == "set") {
	            $this->$property = $arguments[0];
	        }
		}else throw new exception ('Undefined property name '.$property.'!');
	}
}
?>
