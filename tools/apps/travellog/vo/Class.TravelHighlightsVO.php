<?php
/**
* <b>TravelHighlightsVO</b> value object.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2007-2008
*/
require_once("travellog/vo/Class.SetGet.php");
class TravelHighlightsVO extends SetGet{
	protected 
	
	$mTravelHighlightID = 0,
	
	$mPhotoID           = 0,
	
	$mTravelLogID       = 0,
	
	$mDescription       = NULL,
	
	$mPhoto             = NULL;  
}
?>
