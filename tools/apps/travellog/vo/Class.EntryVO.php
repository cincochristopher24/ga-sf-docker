<?php
class EntryVO{
	private
	
	$mTravelLogID    = 0,
	
	$mTravelID       = 0,
	
	$mCountryID      = 0,
	
	$mCountPhotos    = 0,
	
	$mPrimaryPhotoID = 0,
	
	$mTemplateType   = 0,
	
	$mIsAdvisor    = false,
	
	$mTitle        = NULL,
	
	$mPublish      = 0,
	
	$mGroupName    = NULL,
	
	$mSubGroupName = NULL,
	
	$mDescription  = NULL,
	
	$mFlag         = NULL,
	
	$mUsername     = NULL,
	
	$mLocation     = NULL,
	
	$mCity         = NULL,
	
	$mCountry      = NULL,
	
	$mArrival      = NULL,
	
	$mPrimaryPhoto = NULL,
	
	$mPrimaryPhotoObj = NULL, 
		
	$mCallout      = NULL,
	
	$mUrlAlias	   = NULL, 
	/***************************************************************
	 * additional attibutes for travel log obtained from GANET video
	 * added by neri
	 * 10-14-08
	 ***************************************************************/
	
	$mPrimaryVideo = NULL,
	
	$mVideos 	   = NULL,
	
	$mOwner		   = NULL,
	
	$mCountVideos  = 0;
	
	/***************************************************************/
	
	function __call($method, $arguments) {
        $prefix   = substr($method, 0, 3);
        $property = 'm'.substr($method, 3);
		
		if (empty($prefix) || empty($property)) {
            return;
        }
		if (property_exists($this,$property)){
	        if ($prefix == "get" && isset($this->$property)) {
	            return $this->$property;
	        }
	        if ($prefix == "set") {
	            $this->$property = $arguments[0];
	        }
		}else throw new exception ('Undefined property name '.$property.'!');
	}
	
	function getFriendlyURL(){
		require_once("Class.FriendlyUrl.php");
		if(isset($this->mGroupName)) {
			$friendlyURL = 'groups/'.$this->mGroupName;
			$friendlyURL .= (isset($this->mSubGroupName)) ? '/subgroup/'.$this->mSubGroupName : '';
			$friendlyURL = FriendlyUrl::encode($friendlyURL.'/journals/'.$this->mTravelID);
		}
		else if(isset($this->mUsername)){
			$friendlyURL = $this->mUsername.'/journals/'.$this->mTravelID;
		}
		// return $friendlyURL.'/'.urlencode(FriendlyUrl::encode($this->mTitle));
		return $friendlyURL.'/'.$this->mUrlAlias;
	}
}
?>
