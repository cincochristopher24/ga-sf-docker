<?php
require_once("travellog/vo/Class.SetGet.php");
class JournalsArticlesVO extends SetGet{

	protected	
		$mTravelID = 0, // the travellogID for journals entries and articleID for articles
		$mTravelerID = 0,
		$mLocID = 0,
		$mTitle = NULL,
		$mDescription = NULL,
		$mPrimaryphoto = 0,
		$mPrivacyPref = 0,
		$mDate = NULL,
		$mPublish = NULL,
		$mEntryCount = 0,
		$mOwner = NULL,
		$mApproved = NULL,
		$mTrip = NULL,
		$mID = NULL, // the travelID for journal entries AND 0 for articles
		$mViews = NULL,
		$mType = NULL;
		
	public function isApproved($gID){
		if($this->getType() == "JOURNAL"){
			require_once('travellog/dao/Class.GroupApprovedJournals.php');
			//$gID = $CONFIG->getGroupID();
			return GroupApprovedJournals::isApproved($this->getTravelID(), $gID);
		}
		
		return true;
	}	
	
	// get all trips within the travel journal
	function getTrips($criteria = null){
		try {
 			$mConn	= new Connection();
			$mRs	= new Recordset($mConn);
		}
		catch (Exception $e) {
		   throw $e;
		}
		$arrTrip = array();
		if($this->getType() == "JOURNAL"){
			if (null == $criteria){
				$criteria = new Criteria2();
				$criteria->setOrderBy("arrival, tripID"); 
			}
	
			// query modified by neri: 		01-16-09
			// added DISTINCT keyword
			$sql = "SELECT DISTINCT tblTrips.*, tblTravel.travelerID, tblTravel.travellinkID " .
					"FROM tblTravelLog, tblTrips, tblTravel " .
					"WHERE tblTrips.travelID = tblTravel.travelID " .
					"AND  tblTrips.tripID = tblTravelLog.tripID " .
					"AND tblTravelLog.deleted = 0 " .
					"AND tblTravel.deleted = 0 " .
					"AND tblTravel.travelID = " . $this->getTravelID() . " " . $criteria->getCriteria2();
			$mRs->Execute($sql);
		
			$arrTrip = array();
			if (0 < $mRs->Recordcount()){
 				while ($recordset = mysql_fetch_array($mRs->Resultset())) {
					$mData = array();
					$mData['arrival']		= $recordset['arrival'];
					$mData['travelId']		= $recordset['travelID'];
					$mData['locationId']	= $recordset['locID'];
					$mData['travelerId']	= $recordset['travelerID'];
					$mData['travelLinkId']	= $recordset['travellinkID'];
					$mData['tripId']	    = $recordset['tripID'];

 					$arrTrip[] = new Trip(0, $mData);
 				}
			}		
		}
		return $arrTrip;
	}
	
	function getTraveler(){
		return new Traveler($this->getTravelerID());
	}
	
	function isGroupJournalArticle(){
		return ($this->mOwner instanceof AdminGroup);
	}
}
?>
