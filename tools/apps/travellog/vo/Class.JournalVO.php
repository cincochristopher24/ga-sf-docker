<?php
require_once("travellog/vo/Class.SetGet.php");
class JournalVO extends SetGet{
	protected 
	
	$mTravelID              = 0,
	
	$mTravelerID            = 0,
	
	$mTitle                 = NULL,
	
	$mTrips                 = NULL,
	
	$mDescription           = NULL,
	
	$mGroup                 = NULL,
	
	$mHasMoreThanOneJournal = false,
	
	$mIsGroupApproved       = false,
	
	$mOwner                 = NULL,
	
	$mDraftsCount = null;
}
?>
