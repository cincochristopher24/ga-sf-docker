<?php
require_once("travellog/vo/Class.SetGet.php");
class TemplateContent extends SetGet{
	protected
	
	$travelerID            = 0,
	
	$travelID              = 0,
	
	$journal_title         = NULL,
	
	$journal_desc          = NULL,
	
	$entry_highlights      = NULL,
	
	$tpl_travel_tips       = NULL,
	
	$tpl_map               = NULL,
	
	$tpl_comments          = NULL,
	
	$tpl_author            = NULL,
	
	$tpl_photo             = NULL,
	
	$entry_table_of_contents = NULL;
	
	function setTravelerID( $travelerID ){ 
		$this->travelerID = $travelerID; 
	}
	function getTravelerID(){ 
		return $this->travelerID; 
	}
		
	function setJournalTitle( $journal_title ){ 
		$this->journal_title = $journal_title; 
	}
	function getJournalTitle(){ 
		return $this->journal_title; 
	}
	
	function setJournalDescription( $journal_desc ){ 
		$this->journal_desc = $journal_desc; 
	}
	function getJournalDescription(){ 
		$this->journal_desc; 
	}
	
	function setEntryHighlights( EntryHighlights $entry_highlights ){
		$this->entry_highlights = $entry_highlights;
	}
	function getEntryHighlights(){
		return $this->entry_highlights;
	}
	
	function setTemplateTravelTips( $tpl_travel_tips ){
		$this->tpl_travel_tips = $tpl_travel_tips;
	}
	function getTemplateTravelTips(){
		return $this->tpl_travel_tips;
	}
	
	function setTemplateMap( $tpl_map ){
		$this->tpl_map = $tpl_map; 
	}
	function getTemplateMap(){
		return $this->tpl_map; 
	}
	
	function setEntryTableOfContents( EntryTableOfContents $entry_table_of_contents ){
		$this->entry_table_of_contents = $entry_table_of_contents; 
	}
	function getEntryTableOfContents(){
		return $this->entry_table_of_contents; 
	}
	
	function setTemplateComments( $tpl_comments ){
		$this->tpl_comments = $tpl_comments; 
	}
	function getTemplateComments(){
		return $this->tpl_comments; 
	}
	
	function setTemplateAuthor( $tpl_author ){
		$this->tpl_author = $tpl_author; 
	}
	function getTemplateAuthor(){
		return $this->tpl_author; 
	}
	
	function setTemplatePhoto( $tpl_photo ){
		$this->tpl_photo = $tpl_photo; 
	}
	function getTemplatePhoto(){
		return $this->tpl_photo; 
	}
}
?>
