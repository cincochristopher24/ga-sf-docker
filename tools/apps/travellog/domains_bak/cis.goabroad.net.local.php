<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalCIS implements Initialization {
		
		private $initializedValues = array (
				'GMapKey' => '',
				'Site'	=>	'CIS'
			);

		public function getIdentifier(){
			return 'cis.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalCIS());
		
?>