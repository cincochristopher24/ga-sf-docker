<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevNEUMANN implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQ-Zkr7mreSrJyblMjOnqoYbtlbwRRFThQorB6mXr6xAG_QFUhkXYy-Dw',
			'Site'	=>	'NEUMANN'
		);

		public function getIdentifier(){
			return 'neumann.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevNEUMANN());

?>