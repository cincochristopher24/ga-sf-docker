<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdMYISIC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhS4TDcOIM3geiQD1JANgZFys9VCEBS2VTLf9dbK9_1zhXnCdZjd9Dq3ww',
			'Site'	=>	'MYISIC'
		);

		public function getIdentifier(){
			return 'myisic.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdMYISIC());

?>