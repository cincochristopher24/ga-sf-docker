<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalProWorld implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshT0AQ4JaWZJmbsPJMtIG5NdDiiQvBQmMl5KIO4fUF5oE9FHBZmg3DEdxw',
				'Site'	=> 'PROWORLD'
			);

		public function getIdentifier(){
			return 'myproworld.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalProWorld());
		
?>