<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevQUINNIPIAC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQCpU0kn8_bcqF4k0ONo8ewteDbWBQD_XhWiNG41OI5Azip7UcLwNRgqA',
			'Site'	=>	'QUINNIPIAC'
		);

		public function getIdentifier(){
			return 'quinnipiac.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevQUINNIPIAC());

?>