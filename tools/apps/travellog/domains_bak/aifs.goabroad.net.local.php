<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalAIFS implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRR7Mtn6aYuvBXvfml3eYH7YKBZuDhQRfH8k1NUAUnqZzm3th50k06OOnQ',
				'Site'	=>	'AIFS'
			);

		public function getIdentifier(){
			return 'aifs.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalAIFS());
		
?>