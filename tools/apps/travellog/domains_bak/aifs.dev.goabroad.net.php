<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevAIFS implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRSuhWSY3v2csAEBjf9N5IzGMINBMBR-j0EguIP45CKb1y9na3WTrQvD2Q',
				'Site'	=>	'AIFS'
			);
	
		public function getIdentifier(){
			return 'aifs.dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevAIFS()); 
	
?>