<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevELIABROAD implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshSdqTkI2DQ8Xii_5sYXG1QFW9vknhTallGrtjBZ0SkBVwJdGbwvleGwsQ',
				'Site'	=>	'ELIABROAD'
			);
	
		public function getIdentifier(){
			return 'eliabroad.dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevELIABROAD()); 
	
?>