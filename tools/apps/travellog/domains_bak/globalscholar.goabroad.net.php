<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdGLOBALSCHOLAR implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshT9IgfytLo2zzbIFchIfKWoPfGvvBR78mg851j30uaEQpKRQ7_ESlRRIA',
				'Site'	=>	'GLOBALSCHOLAR'
			);
	
		public function getIdentifier(){
			return 'globalscholar.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdGLOBALSCHOLAR());
	
?>