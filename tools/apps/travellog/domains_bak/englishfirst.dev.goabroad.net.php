<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevENGLISHFIRST implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQ04mVbSS9YEJcfUqyI2sbIio74SRTjYd7imWrAZD2qTUnyAg5SXeZX6Q',
			'Site'	=>	'ENGLISHFIRST'
		);

		public function getIdentifier(){
			return 'englishfirst.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevENGLISHFIRST());

?>