<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalENGLISHFIRST implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'ENGLISHFIRST'
		);

		public function getIdentifier(){
			return 'englishfirst.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalENGLISHFIRST());

?>