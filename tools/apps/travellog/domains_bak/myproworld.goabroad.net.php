<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdProWorld implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshSVwSnKHPARBmqvBZSHqVeSvuorNBSxbWwT1xyiay4OjoXXo0bL8xrI5g',
				'Site'	=>	'PROWORLD'
			);
	
		public function getIdentifier(){
			return 'myproworld.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdProWorld());
	
?>