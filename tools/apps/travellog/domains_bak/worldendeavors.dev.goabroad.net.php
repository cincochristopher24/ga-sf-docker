<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevWORLDENDEAVORS implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshQjWB1CL4Sut7tZlNEh8SnFf8kOGxRKctLvC8cnTwcvsIeUiJaeY0aYDA',
				'Site'	=>	'WORLDENDEAVORS'
			);

		public function getIdentifier(){
			return 'worldendeavors.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevWORLDENDEAVORS());
		
?>