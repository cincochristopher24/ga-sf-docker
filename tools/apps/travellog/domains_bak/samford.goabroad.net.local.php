<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalSAMFORD implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'SAMFORD'
		);

		public function getIdentifier(){
			return 'samford.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalSAMFORD());

?>