<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalIEP implements Initialization {

		private $initializedValues = array (
				'GMapKey' => '',
				'Site'	=>	'IEP'
			);

		public function getIdentifier(){
			return 'iep.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalIEP());
		
?>