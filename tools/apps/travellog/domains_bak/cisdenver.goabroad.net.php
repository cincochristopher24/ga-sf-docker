<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdCISDENVER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhRgK1cRO_ydH6XF66KlSA676Lp9PRR6UqDvJU6nFDJF3aqz6DR5Zq1Cdg',
			'Site'	=>	'CISDENVER'
		);

		public function getIdentifier(){
			return 'cisdenver.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdCISDENVER());

?>