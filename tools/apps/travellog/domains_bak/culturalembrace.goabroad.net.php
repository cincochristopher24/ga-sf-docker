<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdCULTURALEMBRACE implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshTFhhtEQKuuM9hAgBNYaROU0DxbwBQbu050xoJPWl3rntHPcaE0FDX5HQ',
				'Site'	=>	'CULTURALEMBRACE'
			);
	
		public function getIdentifier(){
			return 'culturalembrace.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdCULTURALEMBRACE());
	
?>