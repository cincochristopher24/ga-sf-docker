<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalCEA implements Initialization {

		private $initializedValues = array (
				'GMapKey' => '',
				'Site'	=>	'CEA'
			);

		public function getIdentifier(){
			return 'cea.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalCEA());
		
?>