<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdISI implements Initialization {

		private $initializedValues = array (
			//'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSrnoWKuNFuOnB87clxAEkEZ-3HuhR1k71euwHGTAlNS_s3svEQp5sypg',
			'GMapKey' => 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RTSLFXcXnfoN54L8eFKyvlGyILP5BTuHfFTyvfjz-Q6KMmGVyvV-0wWyQ',
			'Site'	=>	'ISI'
		);

		public function getIdentifier(){
			return 'internationalstudent.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdISI());

?>