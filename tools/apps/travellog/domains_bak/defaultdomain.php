<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDefaultDomain implements Initialization {
	
		private $initializedValues = array (				
			);
	
		public function getIdentifier(){
			return '';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
		
	IniConfig::register(new IniDefaultDomain());
	
?>