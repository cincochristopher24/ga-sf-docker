<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalELIABROAD implements Initialization {

		private $initializedValues = array (
				'GMapKey' => '',
				'Site'	=>	'ELIABROAD'
			);

		public function getIdentifier(){
			return 'eliabroad.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalELIABROAD());
		
?>