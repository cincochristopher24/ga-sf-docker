<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalNVCC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'NVCC'
		);

		public function getIdentifier(){
			return 'nvcc.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalNVCC());

?>