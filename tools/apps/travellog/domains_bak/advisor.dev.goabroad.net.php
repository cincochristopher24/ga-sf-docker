<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevADVISOR implements Initialization {
	
		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshQvCoTZ4IzUh7fUmRmBs67xIfeeoBS1Q3UVhdcMIMTLRLL8DnW8iC6bOA',
			'Site'	=>	'ADVISOR'
		);
	
		public function getIdentifier(){
			return 'advisor.dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevADVISOR()); 
	
?>