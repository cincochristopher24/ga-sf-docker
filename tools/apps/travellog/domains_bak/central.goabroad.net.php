<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdCENTRALCOLLEGE implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQjA4A7s7JSmKDO30SLeg_3meM4EBQWR1NAXWYFOCTFxBb_dt2a_jKbCA',
			'Site'	=>	'CENTRALCOLLEGE'
		);

		public function getIdentifier(){
			return 'central.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdCENTRALCOLLEGE());

?>