<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevUCDENVER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQNcFXhduhEveMvc__oMg4TIEBRUhTmNYH14fF8oQY5LJgRHCseX7cUmA',
			'Site'	=>	'UCDENVER'
		);

		public function getIdentifier(){
			return 'ucdenver.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevUCDENVER());

?>