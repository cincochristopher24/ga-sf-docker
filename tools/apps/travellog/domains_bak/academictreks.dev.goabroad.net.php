<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevACADEMICTREKS implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSLNTfo0IKu44E9G3cm0eAc34JfhhSNoQHb0BdYKSPq-xCxT-fEZV9NRQ',
			'Site'	=>	'ACADEMICTREKS'
		);

		public function getIdentifier(){
			return 'academictreks.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevACADEMICTREKS());

?>