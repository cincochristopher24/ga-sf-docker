<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevNVCC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhTvKEn1UB59jTJ8x2e86R3EyVX5ZRRAWTKEUdH0Dto8_5j14sg_MHT6pw',
			'Site'	=>	'NVCC'
		);

		public function getIdentifier(){
			return 'nvcc.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevNVCC());

?>