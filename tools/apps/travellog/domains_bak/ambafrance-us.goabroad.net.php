<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdFRENCHEMBASSY implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhRmjizqkpz2CenHjqnL0R_Nad4rgxR7fa9d3Ijp27XiHsbplhOtYc7PeA',
			'Site'	=>	'FRENCHEMBASSY'
		);

		public function getIdentifier(){
			return 'ambafrance-us.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdFRENCHEMBASSY());

?>