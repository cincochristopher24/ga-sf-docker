<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdEDUCASIAN implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshSCET_Yfo5Xwkys-Do71CYTkAMc5xRvVhyxzHswqfJ99PfXhO-ALpqc-g',
				'Site'	=>	'EDUCASIAN'
			);
	
		public function getIdentifier(){
			return 'educasian.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdEDUCASIAN()); 
	
?>