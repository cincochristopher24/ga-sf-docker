<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevIEP implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshQbUfwZ_4rfKk07ONx50eA9GNISMRQVdq6OtYIMhZOIEXUwqMt_baJiCA',
				'Site'	=>	'IEP'
			);

		public function getIdentifier(){
			return 'iep.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevIEP());
		
?>