<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdACADEMICTREKS implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhR8v1PeAL4EJWrsBknWX8fCZ7Kj4RTD-Wwt3DWctIKdBdSzlm_eyz0L-Q',
			'Site'	=>	'ACADEMICTREKS'
		);

		public function getIdentifier(){
			return 'academictreks.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdACADEMICTREKS());

?>