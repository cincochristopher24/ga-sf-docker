<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdNEUMANN implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhRzFqHz__N82Gp45ngfSAvSd8m_1RTonxdGXNusxs9b3wuQWU0PsUUT3Q',
			'Site'	=>	'NEUMANN'
		);

		public function getIdentifier(){
			return 'neumann.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdNEUMANN());

?>