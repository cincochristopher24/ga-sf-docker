<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevSAMFORD implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQzxP7yrmqNdTlktampWaN76YxqqRTZkxTw1p0Ei_vYLLpbK2d36IrXnA',
			'Site'	=>	'SAMFORD'
		);

		public function getIdentifier(){
			return 'samford.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevSAMFORD());

?>