<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalSOLABROAD implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'SOLABROAD'
		);

		public function getIdentifier(){
			return 'solabroad.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalSOLABROAD());

?>