<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevCEA implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshRkRJyu465uEj4am6SB1nvX22VxIRRbJNuJug3k_NPtS6k4iUl14Oiz3Q',
				'Site'	=>	'CEA'
			);

		public function getIdentifier(){
			return 'cea.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevCEA());
		
?>