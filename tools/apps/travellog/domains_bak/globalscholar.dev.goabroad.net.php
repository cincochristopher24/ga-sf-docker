<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevGLOBALSCHOLAR implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshSzVb-5939dhYk9bmWwPrHOn4kA-hRVoPj9jqXNSnTInVnVRgoCzxBd0A',
				'Site'	=>	'GLOBALSCHOLAR'
			);

		public function getIdentifier(){
			return 'globalscholar.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevGLOBALSCHOLAR());
		
?>