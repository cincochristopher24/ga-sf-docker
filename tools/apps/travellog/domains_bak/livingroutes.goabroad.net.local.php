<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalLIVINGROUTES implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSx1otDQeJ60GT6Z3sDXbiZx9EwuxQaK2N1K14MKJVgNXbLfl0F4463VQ',
				'Site'	=>	'LIVINGROUTES'
			);

		public function getIdentifier(){
			return 'livingroutes.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalLIVINGROUTES());
		
?>