<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdELIABROAD implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshTQZl6_eekvKpsC3-EffZuR0MSovhRaBnnBqzXNj57_EARr0EqdKJUwYw',
				'Site'	=>	'ELIABROAD'
			);
	
		public function getIdentifier(){
			return 'eliabroad.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdELIABROAD()); 
	
?>