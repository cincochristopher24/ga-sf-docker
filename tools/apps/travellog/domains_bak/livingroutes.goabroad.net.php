<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdLIVINGROUTES implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQ6CZb7JZoFfgLjNQG6Qi1WNNd6_hSrjnJgdBWz43KHbUPcDm1k15kwyg',
				'Site'	=>	'LIVINGROUTES'
			);
	
		public function getIdentifier(){
			return 'livingroutes.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdLIVINGROUTES()); 
	
?>