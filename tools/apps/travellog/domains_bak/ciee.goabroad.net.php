<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdCIEE implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSlk7XCN_ykTGOpuO82gVJT1rkyyRQ8eAh54Bd7lCFtx04DA1aGyLI-Ag',
			'Site'	=>	'CIEE'
		);

		public function getIdentifier(){
			return 'ciee.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdCIEE());

?>