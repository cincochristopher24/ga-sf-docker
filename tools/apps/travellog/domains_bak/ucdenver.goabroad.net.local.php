<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalUCDENVER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'UCDENVER'
		);

		public function getIdentifier(){
			return 'ucdenver.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalUCDENVER());

?>