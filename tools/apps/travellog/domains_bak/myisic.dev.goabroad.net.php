<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevMYISIC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhRK4duacdx4RyE0ubMjNt7y3TTZdRSZJ4ia4RF3eNHU4XsVYts5YxoeMA',
			'Site'	=>	'MYISIC'
		);

		public function getIdentifier(){
			return 'myisic.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevMYISIC());

?>