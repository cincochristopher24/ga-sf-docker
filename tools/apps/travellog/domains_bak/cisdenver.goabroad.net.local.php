<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalCISDENVER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'CISDENVER'
		);

		public function getIdentifier(){
			return 'cisdenver.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalCISDENVER());

?>