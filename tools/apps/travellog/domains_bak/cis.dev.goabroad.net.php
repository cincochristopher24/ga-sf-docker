<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevCIS implements Initialization {

		private $initializedValues = array (
				//'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshS5NInwoiiqE9iUywq8Xw7ia1ANaBTP3lpNArZ1xnxnPNHBONXLPhE9Yw',
				'GMapKey' => 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RTSLFXcXnfoN54L8eFKyvlGyILP5BTuHfFTyvfjz-Q6KMmGVyvV-0wWyQ',
				'Site'	=>	'CIS'
			);

		public function getIdentifier(){
			return 'cis.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevCIS());
		
?>