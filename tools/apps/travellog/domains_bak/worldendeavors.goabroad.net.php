<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdWORLDENDEAVORS implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshSDseuVyVT8IgqdJUeBlg7utUGrfBT9fYsaruXvhXHwSsQvvXjPNMsB6A',
				'Site'	=>	'WORLDENDEAVORS'
			);
	
		public function getIdentifier(){
			return 'worldendeavors.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdWORLDENDEAVORS());
?>