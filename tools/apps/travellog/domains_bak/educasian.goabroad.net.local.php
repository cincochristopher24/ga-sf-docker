<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalEDUCASIAN implements Initialization {

		private $initializedValues = array (
				'GMapKey' => '',
				'Site'	=>	'EDUCASIAN'
			);

		public function getIdentifier(){
			return 'educasian.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalEDUCASIAN());
		
?>