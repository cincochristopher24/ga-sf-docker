<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalUCBOULDER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'UCBOULDER'
		);

		public function getIdentifier(){
			return 'ucboulder.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalUCBOULDER());

?>