<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdQUINNIPIAC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhS_JYnIzNvvfboWrbGktKcsbccLPRRHeTySOdVioEr8qBK8Uh6QY-6FIQ',
			'Site'	=>	'QUINNIPIAC'
		);

		public function getIdentifier(){
			return 'quinnipiac.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdQUINNIPIAC());

?>