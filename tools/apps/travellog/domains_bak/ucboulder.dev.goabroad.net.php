<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevUCBOULDER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSnFDj9KA3za2FchaMqSmKmkGS3VxS-ric6hWkd53HT3KkTDNxXzhsCQQ',
			'Site'	=>	'UCBOULDER'
		);

		public function getIdentifier(){
			return 'ucboulder.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevUCBOULDER());

?>