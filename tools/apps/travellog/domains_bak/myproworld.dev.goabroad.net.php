<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevProWorld implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshQOQMdl1LDzRcmCjMAqFUQrBskquRSf88a7Rx7BvW-ASyHnNT60uNfV7Q',
				'Site'	=>	'PROWORLD'
			);
	
		public function getIdentifier(){
			return 'myproworld.dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevProWorld());
	
?>