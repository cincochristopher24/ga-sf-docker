<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevCIEE implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSk7tDTGv9FpaMV_7V3y_CoR5haehRZQc55bHY9VqnxbCmP8a9HgarUsw',
			'Site'	=>	'CIEE'
		);

		public function getIdentifier(){
			return 'ciee.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevCIEE());

?>