<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalMYISIC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'MYISIC'
		);

		public function getIdentifier(){
			return 'myisic.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalMYISIC());

?>