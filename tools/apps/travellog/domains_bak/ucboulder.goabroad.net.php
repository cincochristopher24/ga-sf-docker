<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdUCBOULDER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQ3QyMo8Ml4Zd0BH_YY2HHET3P3NRT6w9GxjhoLl7eR2mskN9o3djqIBg',
			'Site'	=>	'UCBOULDER'
		);

		public function getIdentifier(){
			return 'ucboulder.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdUCBOULDER());

?>