<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdTEST implements Initialization {
	
		private $initializedValues = array (
				//'GMapKey' => 'ABQIAAAA5UgvN_LjjXZl7EcbhHlcxxThc8huphRfTZhCAFwcw9vOKp4exhQImWMMhSog3qTBmS4MOxKsuM7pzQ',
				'GMapKey' => 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RTSLFXcXnfoN54L8eFKyvlGyILP5BTuHfFTyvfjz-Q6KMmGVyvV-0wWyQ',
				'Site'	=>	'GANET'
			);
	
		public function getIdentifier(){
			return 'test.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdTEST()); 
?>
