<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdNVCC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhRmssIDUp5amjownNaKuEdpVJqm0hQdQyRqX9JyjyEK2OfG5KjStwZL8Q',
			'Site'	=>	'NVCC'
		);

		public function getIdentifier(){
			return 'nvcc.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdNVCC());

?>