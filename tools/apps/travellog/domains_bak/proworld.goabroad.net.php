<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdProWorld implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRSMkUCtVtwEkgi-LACZFk1vfOFK1RSuKN6AYHgsfrMUXtWGP4BloC_ZIQ',
				'Site'	=>	'PROWORLD'
			);
	
		public function getIdentifier(){
			return 'proworld.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdProWorld());
	
?>