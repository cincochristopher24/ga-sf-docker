<?php

	require_once("travellog/ganetapi/constant/Class.GanetApiConstant.php");
	
	
	/**
	 * GanetApiTravelerSearch.
	 *
	 */
	class GanetApiTravelerSearch {
		
		/**
		 * Search travelers array.
		 *
		 * @param array $data
		 * @return array 
		 * @access public
		 */
		public static function search($data) {
			$query = self::createQuery($data);
			$connection = Propel::getConnection();
			
			$statement = $connection->prepare($query);
			$statement->execute();
			
			$travelers = array();
			while ($resultset = $statement->fetch(PDO::FETCH_OBJ)){
				$travelers[] = array(
					'id' => $resultset->travelerID,
					'username' => $resultset->username,
					'photo' => self::getPhotoLink($resultset)
				);
			}
			
			return $travelers;
		}
		
		/**
		 * Count travelers.
		 *
		 * @param array $data
		 * @return int
		 * @access public
		 */
		public static function count($data) {
			$query = self::createCountQuery($data);
			$connection = Propel::getConnection();
			
			$statement = $connection->prepare($query);
			$statement->execute();
			
			if ($row = $statement->fetch(PDO::FETCH_NUM)) {
				$count = (int) $row[0];
			} else {
				$count = 0; // no rows returned; we infer that means 0 matches.
			}
			$statement->closeCursor();
			
			return $count;
		}
		
		/**
		 * Create traveler search query.
		 *
		 * @param array $data
		 * @return string
		 * @access protected
		 */
		protected static function createQuery($data) {
			if (is_numeric($data[GanetApiConstant::KEYWORD_LOCATION]) && $data[GanetApiConstant::KEYWORD_LOCATION] > 0) {
				$locIDs  = self::getLocationByCountryID($data[GanetApiConstant::KEYWORD_LOCATION]);
			}
			
			$showDefaultPhoto = $data[GanetApiConstant::KEYWORD_DEFAULT_PHOTO] ? true : false;
			$status = $data[GanetApiConstant::KEYWORD_STATUS];
			
			$query = "SELECT DISTINCT tblTraveler.travelerID, 
			                 tblTraveler.dateregistered, 
							 tblTraveler.username, 
							 tblTraveler.email, 
							 tblPhoto.filename  ";
			
			if ($showDefaultPhoto) {
				$query .= "
					FROM  Travel_Logs.tblTraveler
					LEFT JOIN Travel_Logs.tblTravelertoPhoto
						ON (tblTraveler.travelerID = tblTravelertoPhoto.travelerID)
					LEFT JOIN Travel_Logs.tblPhoto
						ON (tblTravelertoPhoto.photoID = tblPhoto.photoID
							AND tblPhoto.primaryphoto = 1
						)
				";
			}
			else {
				$query .="
					FROM Travel_Logs.tblTraveler, Travel_Logs.tblTravelertoPhoto, Travel_Logs.tblPhoto 
				";
			}
					
			
			if ((0 < $data[GanetApiConstant::APP_KEY]) || (is_numeric($data[GanetApiConstant::KEYWORD_SUBGROUP]) && $data[GanetApiConstant::KEYWORD_SUBGROUP] > 0)) {
				$query .= ", Travel_Logs.tblGrouptoTraveler ";	
			}
			
			$query .= "
					WHERE active = 1 AND isSuspended = 0 
						AND tblTraveler.travelerID NOT IN (
							SELECT DISTINCT travelerID FROM Travel_Logs.tblGrouptoAdvisor 
						)";
			
			if (!$showDefaultPhoto) {
				$query .= "
						AND tblTraveler.travelerID = tblTravelertoPhoto.travelerID 
						AND tblTravelertoPhoto.photoID = tblPhoto.photoID 
						AND tblPhoto.primaryphoto = 1 
				";
			}
			
			if (is_numeric($data[GanetApiConstant::KEYWORD_LOCATION]) && $data[GanetApiConstant::KEYWORD_LOCATION] > 0) {
				$query .= " AND (tblTraveler.htlocationID IN ( ".$locIDs. " ) ";
				$query .= " OR tblTraveler.currlocationID IN ( ".$locIDs. " ) ";
				$query .= ") ";
			}
			
			if (is_numeric($data[GanetApiConstant::KEYWORD_SUBGROUP]) && $data[GanetApiConstant::KEYWORD_SUBGROUP] > 0) {
				$query .= " 
						AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID 
						AND tblGrouptoTraveler.groupID = ".mysql_real_escape_string($data[GanetApiConstant::KEYWORD_SUBGROUP])." 
				";
			}
			else if (0 < $data[GanetApiConstant::APP_KEY]) {
				$query .= " AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID 
					AND tblGrouptoTraveler.groupID = ".mysql_real_escape_string($data[GanetApiConstant::APP_KEY])." 
				";
			}
			if ($status) {
				$query .= " 
						AND tblTraveler.travelerStatus = 1 
				";
			}
			$query .= "GROUP BY tblTraveler.travelerID ";
			
			if (is_numeric($data[GanetApiConstant::KEYWORD_COUNT])) {
				$limit = abs($data[GanetApiConstant::KEYWORD_COUNT]);
			}
			else {
				$limit = GanetApiConstant::DEFAULT_MEMBER_COUNT;
			}
			
			if ($data[GanetApiConstant::KEYWORD_SORT] == GanetApiConstant::SORT_RECENT) {
				$query .= "ORDER BY latestlogin DESC ";
			}
			else {
				$query .= "ORDER BY RAND() ";
			}
			
			$query .= "LIMIT 0,".$limit;
			
			return $query;
		}
		
		/**
		 * Create traveler search count query.
		 *
		 * @param array $data
		 * @result string
		 * @access protected
		 */
		protected static function createCountQuery($data) {
			if (is_numeric($data[GanetApiConstant::KEYWORD_LOCATION]) && $data[GanetApiConstant::KEYWORD_LOCATION] > 0) {
				$locIDs  = self::getLocationByCountryID($data[GanetApiConstant::KEYWORD_LOCATION]);
			}
			
			$query = "SELECT COUNT(DISTINCT tblTraveler.travelerID)  
					FROM Travel_Logs.tblTraveler ";
			
			if ((0 < $data[GanetApiConstant::APP_KEY]) || (is_numeric($data[GanetApiConstant::KEYWORD_SUBGROUP]) && $data[GanetApiConstant::KEYWORD_SUBGROUP] > 0)) {
				$query .= ", Travel_Logs.tblGrouptoTraveler ";	
			}
			
			$query .= " 
					WHERE active = 1 AND isSuspended = 0  
						AND tblTraveler.travelerID NOT IN(
							SELECT DISTINCT travelerID FROM Travel_Logs.tblGrouptoAdvisor 
						) ";
			
			if (is_numeric($data[GanetApiConstant::KEYWORD_LOCATION]) && $data[GanetApiConstant::KEYWORD_LOCATION] > 0) {
				$query .= " AND (tblTraveler.htlocationID  IN ( ".$locIDs. " ) ";
				$query .= " OR tblTraveler.currlocationID  IN ( ".$locIDs. " ) ";
				$query .= ") ";
			}
			
			if (is_numeric($data[GanetApiConstant::KEYWORD_SUBGROUP]) && $data[GanetApiConstant::KEYWORD_SUBGROUP] > 0) {
				$query .= " AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID 
					AND tblGrouptoTraveler.groupID = ".mysql_real_escape_string($data[GanetApiConstant::KEYWORD_SUBGROUP])." 
				";
			}
			else if (0 < $data[GanetApiConstant::APP_KEY]) {
				$query .= " AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID 
					AND tblGrouptoTraveler.groupID = ".mysql_real_escape_string($data[GanetApiConstant::APP_KEY])." 
				";
			}
			
			return $query;
		}
		
		/**
		 * Retrieve Photo link.
		 *
		 * @param array $resultset
		 * @result string
		 * @access protected
		 */
		protected static function getPhotoLink($resultset) {
			if ($resultset->filename) {
				$date_joined = getdate(strtotime($resultset->dateregistered));
				
				$path = 'https://dfe1zrvl08k3v.cloudfront.net/';
				$path .= "users/photos/".$date_joined['year']."/".$date_joined['month']."/".$resultset->travelerID.'/profile/thumbnail'.$resultset->filename;
			}
			else {
				$path = "http://images.goabroad.net/images/default_images/user_thumb.gif";
			}
			
			return $path;
		}
		
		/**
		 * Retrieve LocationIDs from locationID of country.
		 * From Class TravelCB
		 *
		 * @param int $locationID The locationID of country
		 * @result string The locationIDs of city/newcity/country of countryID passed
		 * @access private
		 */
		private static function getLocationByCountryID($locationID) {
			$connection = Propel::getConnection();
			
			$query = "SELECT GoAbroad_Main.tbcity.locID FROM GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry " .
				    "WHERE GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID AND GoAbroad_Main.tbcountry.locID=" . mysql_real_escape_string($locationID) .
				    " UNION " .
				    "SELECT tblNewCity.locID FROM tblNewCity, GoAbroad_Main.tbcountry " .
				    "WHERE tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID AND GoAbroad_Main.tbcountry.locID=" . mysql_real_escape_string($locationID);
			
			$statement = $connection->prepare($query);
			$statement->execute();
			
			$col_locID = array();
			while ($resultset = $statement->fetch(PDO::FETCH_OBJ)){
				$col_locID[] = $resultset->locID; 
			}
			$col_locID[] = $locationID;
			
			return implode(',', $col_locID);
		}
		
	}