<?php

class GanetApiCriteriaConstant {
	
	const LIMIT            = 'limit';
	const ORDER            = 'order'; //table field
	const ORDER_BY         = 'orderby'; // asc|desc
	const ORDER_ASCENDING  = 'asc';
	const ORDER_DESCENDING = 'desc';
	const RANDOMIZE        = 'rand';
	
	//traveler key constants
	const ISFEATURED       = 'isfeatured';
	const LOCATION         = 'location';
	const HOMETOWN         = 'hometown';
	const HAS_PHOTO        = 'hasphoto';
	
	//traveler value constants
	const TRAVELER_DEFAULT_LIMIT = 6;
	const TRAVELER_LIMIT         = 50;
	const TRAVELER_NOT_FEATURED  = 0;
	const TRAVELER_IS_FEATURED   = 1;
	const TRAVELER_NOT_SUSPENDED = 0;
	const TRAVELER_IS_SUSPENDED  = 1;
	const TRAVELER_NOT_ACTIVE    = 0;
	const TRAVELER_IS_ACTIVE     = 1;
}