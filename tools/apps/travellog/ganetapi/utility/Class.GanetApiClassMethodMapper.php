<?php

	/**
	* @package ganetapi
	* @todo update valid classes
	*
	* Maps function to respective class. 
	* This assumes that a function only exist in one class
	*/
	class GanetApiClassMethodMapper{

		private static $instance = null;
		private static $validPeerClasses = array(
			"GanetApiTravelerPeer", "GanetApiTravelEntryPeer", "GanetApiGroupPeer", "GanetApiDiscussionBoardPeer");
		private static $validModelClasses = array("GanetApiTraveler", "GanetApiTravelEntry", "GanetApiGroup", "GanetApiDiscussionBoard");
		
		/**
		* @param void
		* @return GanetApiClassMethodMapper object
		* 
		* Instantiates a class if its not instantiated yet.
		**/
		public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new self();
			}
			return self::$instance;
		}

		/**
		* @param String
		* @return object to which the function is matched
		* @return false if function does not exist
		*
		* Checks and maps a function to a valid class.
		**/
		public function mapFunction($function){
			foreach(self::$validPeerClasses as $class){
				if(in_array($function, get_class_methods($class))){
					return new $class;
				}
			}
			return false;
		}
		
		public function getValidClasses(){
			return self::$validClasses;
		}
		
		public function isModelClassValid($class){
			return in_array($class, self::$validModelClasses);
		}
		
		public function isPeerClassValid($class){
			return in_array($class, self::$validPeerClasses);
		}
	}