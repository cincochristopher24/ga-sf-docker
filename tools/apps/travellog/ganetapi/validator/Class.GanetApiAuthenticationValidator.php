<?php

class GanetApiAuthenticationValidator extends AbstractGanetApiValidator {
	static $instance = null;
	private $groupID = null;
	
	public function getInstance() {
		if(self::$instance === null)
			self::$instance = new self();
		return self::$instance;
	}
	
	/**
	 * Check authentication. If authenticated and has a group account in dotNet,
	 * add groupID to $args.
	 * 
	 * @throws GanetApiException
	 */
	public function validate() {
		$auth = GanetApiAuthenticationPeer::retrieveByKeyAndSecret($this->args['apikey'], $this->args['secret']);
		if ( !$auth )
			throw new GanetApiException(GanetApiErrorCodes::$api_error_descriptions[GanetApiErrorCodes::GANET_API_AUTHENTICATION]);
		else if ( $auth->getGroupID() )
			$this->groupID = $auth->getGroupID();
	}
	
	public function getGroupID() {
		return $this->groupID;
	}
}