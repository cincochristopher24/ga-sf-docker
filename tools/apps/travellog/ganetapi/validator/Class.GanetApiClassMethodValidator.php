<?php

class GanetApiClassMethodValidator extends AbstractGanetApiValidator {
	static $instance        = null;
	private $generatedClass = null;
	private $method         = null;
	
	public function getInstance() {
		if(self::$instance === null)
			self::$instance = new self();
		return self::$instance;
	}
	
	/**
	 * Check if class exists.
	 *
	 * @see    GanetApiPeerClassFactory
	 * @throws GanetApiException 
	 */
	public function validate() {
		if( !$this->generatedClass = GanetApiClassMethodMapper::getInstance()->mapFunction($this->args['method']) )
			throw new GanetApiException(GanetApiErrorCodes::$api_error_descriptions[GanetApiErrorCodes::GANET_API_CLASS_METHOD]);
		$this->method = $this->args['method'];
	}
	
	public function getClass() {
		return $this->generatedClass;
	}
	
	public function getMethod() {
		return $this->method;
	}
}