<?php

class GanetApiRequestVarsValidator extends AbstractGanetApiValidator {
	static $instance = null;
	
	public function getInstance() {
		if(self::$instance === null)
			self::$instance = new self();
		return self::$instance;
	}
	
	/**
	 * Validates request variables
	 *
	 * @return void
	 * @throws GanetApiException
	 */
	public function validate() {
		if (empty($this->args)) {
			throw new GanetApiException(GanetApiErrorCodes::$api_error_descriptions[GanetApiErrorCodes::GANET_API_PARAMETER]);
		} else if (is_array($this->args) && !array_key_exists('apikey', $this->args)) {
			throw new GanetApiException(GanetApiErrorCodes::$api_error_descriptions[GanetApiErrorCodes::GANET_API_KEY_PARAM]);
		} else if (is_array($this->args) && !array_key_exists('secret', $this->args)) {
			throw new GanetApiException(GanetApiErrorCodes::$api_error_descriptions[GanetApiErrorCodes::GANET_API_SECRET_PARAM]);
		} else if (is_array($this->args) && !array_key_exists('method', $this->args)) {
			throw new GanetApiException(GanetApiErrorCodes::$api_error_descriptions[GanetApiErrorCodes::GANET_API_METHOD_PARAM]);
		}
	}
}