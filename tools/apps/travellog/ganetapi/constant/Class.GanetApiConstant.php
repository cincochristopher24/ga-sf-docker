<?php
	class GanetApiConstant {
		
		const DEFAULT_APP_KEY = 0;
		
		const DEFAULT_MEMBER_COUNT = 10;
		const DEFAULT_MEMBER_WIDTH = 292;
		const DEFAULT_MEMBER_HEIGHT = 276;
		const DEFAULT_MEMBER_SORT = 'random';
		const DEFAULT_MEMBER_DEFAULT_PHOTO = 0;
		
		const DEFAULT_BLOG_COUNT = 3;
		const DEFAULT_BLOG_WIDTH = 292;
		const DEFAULT_BLOG_HEIGHT = 418;
		const DEFAULT_BLOG_HEADER = 'Travel Blogs';
		const DEFAULT_BLOG_SORT = 'recent';
		
		const VERSION_1 = 1;
		
		const DEFAULT_HEADER = 'GoAbroad Network Community';
		const DEFAULT_DESCRIPTION = 'Share your journeys with the world!';
		
		const KEYWORD_COUNT = 'count';
		const KEYWORD_LOCATION = 'locID';
		const KEYWORD_SUBGROUP = 'gID';
		const KEYWORD_STATUS = 'status';
		
		const KEYWORD_WIDTH = 'width';
		const KEYWORD_TITLE = 'title';
		const KEYWORD_DESCRIPTION = 'desc';
		const KEYWORD_FONT = 'font';
		const KEYWORD_SCROLLBAR = 'scrollbar';
		const KEYWORD_SORT = 'sort';
		const KEYWORD_DEFAULT_PHOTO = 'dphoto';
		const KEYWORD_WITH_ARTICLE = 'wArticle';
		
		const SORT_RECENT = 'recent';
		const SORT_RANDOM = 'random';
		
		const MODE = 'mode';
		const APP_KEY = 'apiKey';
		const VERSION = 'v';
		const SITE = 's';

		const MAIN_SITE = 1;
		const COBRAND_SITE = 2; // default
		
		const TRAVELER = 'traveler';
		const BLOG = 'blog';
		
		const TRAVELER_FLAG = 1;
		const BLOG_FLAG = 2;
	}