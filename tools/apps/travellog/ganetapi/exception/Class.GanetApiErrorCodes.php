<?php

class GanetApiErrorCodes {

	const GANET_API_GENERAL = 0;
	
	//parameter
	const GANET_API_PARAMETER    = 1;
	const GANET_API_KEY_PARAM    = 2;
	const GANET_API_SECRET_PARAM = 3;
	const GANET_API_METHOD_PARAM = 4;
	
	//authentication
	const GANET_API_AUTHENTICATION = 5;
	
	//class
	const GANET_API_CLASS_METHOD = 6;
	
	//method
	const GANET_API_METHOD = 7;
			
	public static $api_error_descriptions = array(
		self::GANET_API_GENERAL        => 'Something went wrong.',
		self::GANET_API_PARAMETER      => 'No parameters provided.',
		self::GANET_API_KEY_PARAM      => 'No api key provided.',
		self::GANET_API_SECRET_PARAM   => 'No api secret provided.',
		self::GANET_API_METHOD_PARAM   => 'No method provided.',
		self::GANET_API_AUTHENTICATION => 'You are not allowed to access these data.',
		self::GANET_API_CLASS_METHOD   => 'Function does not exist.'
	);
	
	public static function onError() {
		$isError = false;
        if ($error = error_get_last()){
            switch($error['type']){
                case E_ERROR:
                case E_CORE_ERROR:
                case E_COMPILE_ERROR:
                case E_USER_ERROR:
                    $isError = true;
                    break;
            }
        }
        if ($isError)
            return "ERROR! " . GanetApiErrorCodes::$api_error_descriptions[GanetApiErrorCodes::GANET_API_GENERAL] . " " . $error['message'];
        return false;
	}
}