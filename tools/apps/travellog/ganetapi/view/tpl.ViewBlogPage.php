<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href="/min/g=LayoutMainCss" rel="stylesheet" type="text/css">
		<link href="/min/f=/css/main.css" rel="stylesheet" type="text/css">
	</head>
	
	<?php $font = $font ? ' font-family: '.htmlspecialchars($font).';' : ''; ?>
	<?//php $overflowY = $scrollbar == 'yes' ? ' overflow-y: scroll;' : 'overflow-y: hidden;'; ?>
	
	<body id="widget_body" style="background-color: transparent; width:<?php echo $width; ?>px;<?php echo $font; ?>">
		
		<div id="widget_header" class="border_bottom pad_bottom_10" style="width:<?php echo $width; ?>px;">
			<div class="section" id="text_elements" style="width:<?php echo $width; ?>px;">
    			<h3><a href="<?php echo $link; ?>" target="_blank"><?php echo htmlspecialchars($title); ?></a></h3>
    			<span class="italic" style="display:block; word-wrap:break-word; width:<?php echo $width; ?>px"><?php echo htmlspecialchars(truncateText($description, 50, '', false)); ?></span>
    		</div>
    		
    		<!--div id="groupProfile_thumb">
    			<img src="#">
    		</div-->
    	</div>
    	
    	<ul id="contents" class="blog_contents clearfix" style="<?//php echo $overflowY; ?> width:<?php echo $width; ?>px; ">
    	
    		<?php foreach ($blogs as $iBlog): ?>
    		
    		<li class="border_bottom pad_verts_5 width_full">
    			<?php if ($iBlog['photo']): ?>
    			<div class="blog_thumbnail">
    				<img src="<?php echo $iBlog['photo']; ?>" />
    			</div>
    			<?php endif;?>
    			<div class="blog_details">
    				<div id="blog_title" class="bold"><span><a href="<?php echo $absoluteDomainLink.$iBlog['blog_url']; ?>" title="<?php echo htmlspecialchars($iBlog['title']); ?>" target="_blank"><?php echo ($iBlog['title']); ?></a></span></div>
    				<div class="bold"><span><a href="<?php echo $absoluteDomainLink.$iBlog['owner_url']; ?>" title="<?php echo htmlspecialchars($iBlog['owner']); ?>" target="_blank"><?php echo $iBlog['owner']; ?></a></span> | <span><?php echo $iBlog['location']; ?></span></div>
    				<div>
    					<p>
    						<?php echo utf8_encode(truncateText($iBlog['content'])); ?>
    					</p>
    				</div>
    			</div>
    		</li>
    		
    		<?php endforeach; ?>
    		
    	</ul>
    	
    	<div id="widget_footer" class="clearfix">
    		<div id="join_btn">
    			<a href="<?php echo $homepageLink; ?>" title="Join Our Community" target="_blank">Join Our Community</a>
    		</div>
    		
    		<!--div id="branding">
    			powered by <span id="ganet"><span class="blue">Go</span>Abroad.net</span>
    		</div-->
    	</div>
    	
	</body>
</html>