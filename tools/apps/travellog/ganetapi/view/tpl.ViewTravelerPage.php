<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href="/min/g=LayoutMainCss" rel="stylesheet" type="text/css">
		<link href="/min/f=/css/main.css" rel="stylesheet" type="text/css">
	</head>
	
	<?php $font = $font ? ' font-family: '.htmlspecialchars($font).';' : ''; ?>
	<?php $overflowY = $scrollbar == 'yes' ? ' overflow-y: scroll;' : ''; ?>
	
	<body style="background-color: transparent; width:<?php echo $width; ?>px;<?php echo $font; ?>">
		
		<div id="widget_header" class="border_bottom" style="overflow:hidden; width:<?php echo $width; ?>px;">
		    <div class="section" id="text_elements" style="width:<?php echo $width; ?>px;">
		    	<h3><a href="<?php echo $link; ?>" title="<?php echo htmlspecialchars($title); ?>" target="_blank"><?php echo htmlspecialchars($title); ?></a></h3> 
		    	<span class="italic" style="display:block; word-wrap:break-word; width:<?php echo $width; ?>px"><?php echo htmlspecialchars(truncateText($description, 50, '', false)); ?></span>
		    	<p><span id="travelerCount"><?php echo number_format($count); ?></span> Member<?php if ($count > 1): ?>s<?php endif; ?></p>
		    </div>
		    
		    <!--div id="groupProfile_thumb">
		    	<img src="#">
		    </div-->
		</div>
		
		<ul id="contents" class="clearfix" style="width:<?php echo $width; ?>px;">
		    <?php foreach ($travelers as $iTraveler): ?>
		    <li>
		    	<a href="<?php echo $absoluteDomainLink; ?>/<?php echo $iTraveler['username']; ?>" title="<?php echo htmlspecialchars($iTraveler['username']); ?>" target="_blank">
		    		<div class="user_thumbnail">
		    			<img src="<?php echo htmlspecialchars($iTraveler['photo']); ?>" />
		    			<span><?php echo truncateText($iTraveler['username'], 10, ''); ?></span>
		    		</div>
		    	</a>
		    </li>
		    <?php endforeach; ?>
		</ul>
		
		<div id="widget_footer" class="clearfix">
		    <div id="join_btn">
		    	<a href="<?php echo $homepageLink; ?>" title="Join Our Community" target="_blank">Join Our Community</a>
		    </div>
	    	
	    	<!--div id="branding">
	    		powered by <span id="ganet"><span class="blue">Go</span>Abroad.net</span>
	    	</div-->
		</div>
    	
	</body>
</html>