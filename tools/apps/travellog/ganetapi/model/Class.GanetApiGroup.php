<?php
	
	require_once ("Class.ConnectionProvider.php");
	require_once('Class.GanetApiTraveler.php');
	class GanetApiGroup {
		public function toArray(){
			return array(
				'ID' 		=> 1,
				'name'		=> 'Group Name',
				'advisor'	=> new GanetApiTraveler()
			);
		}
	}