<?php

class GanetApiTraveler extends GanetApiPropelTraveler {
	
	/**
	 * Initializes internal state of GanetApiTraveler object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}
	
	/**
	* @param void
    * @return array - formatted array for valid function calls for GanetApiTraveler
    *
    * convert traveler properties, that would be made available to the public, to an associative array
    */
    public function toArray(){
    	return parent::toArray();
    }
}