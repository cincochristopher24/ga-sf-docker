<?php

class GanetApiTravelerPeer {
	
	public static function getTravelers($args = array()) {
		return GanetApiPropelTravelerPeer::getTravelers($args);
	}
	
	public static function getFeaturedTravelers($args = array()) {
		return GanetApiPropelTravelerPeer::getFeaturedTravelers($args);
	}
	
	public static function getRecentTravelers($args = array()) {
		return GanetApiPropelTravelerPeer::getRecentTravelers($args);
	}
	
	public static function getLatestLoginTravelers($args = array()) {
		return GanetApiPropelTravelerPeer::getLatestLoginTravelers($args);
	}
	
	public static function getRandomTravelers($args = array()) {
		return GanetApiPropelTravelerPeer::getRandomTravelers($args);
	}
	
}