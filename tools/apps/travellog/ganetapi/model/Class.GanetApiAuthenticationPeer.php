<?php

class GanetApiAuthenticationPeer {
	static public function retrieveByKeyAndSecret($key = null, $secret = null) {
		if($key === null || $secret === null)
			return false;
		
		return GanetApiPropelAuthenticationPeer::retrieveByKeyAndSecret($key, $secret);
	}
}