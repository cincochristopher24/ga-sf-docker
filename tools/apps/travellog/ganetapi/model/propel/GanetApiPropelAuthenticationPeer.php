<?php

/**
 * Skeleton subclass for performing query and update operations on the 'tblGanetApi' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    apps.travellog.ganetapi.model.propel
 */
class GanetApiPropelAuthenticationPeer extends BaseGanetApiPropelAuthenticationPeer {
	static public function retrieveByKeyAndSecret($key = null, $secret = null) {
		$criteria = new Criteria();
		$criteria->add(self::KEY, $key, Criteria::EQUAL);
		$criteria->add(self::SECRET, $secret, Criteria::EQUAL);
		
		if(self::doCount($criteria) != 1)
			return false;
		return self::doSelectOne($criteria);
	}
} // GanetApiPropelAuthenticationPeer
