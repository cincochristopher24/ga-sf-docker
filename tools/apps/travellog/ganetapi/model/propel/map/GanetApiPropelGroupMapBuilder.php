<?php


/**
 * This class adds structure of 'tblGroup' table to 'Travel_Logs' DatabaseMap object.
 *
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    apps.travellog.ganetapi.model.propel.map
 */
class GanetApiPropelGroupMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'apps.travellog.ganetapi.model.propel.map.GanetApiPropelGroupMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(GanetApiPropelGroupPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(GanetApiPropelGroupPeer::TABLE_NAME);
		$tMap->setPhpName('GanetApiPropelGroup');
		$tMap->setClassname('GanetApiPropelGroup');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('GROUPID', 'ID', 'INTEGER', true, null);

		$tMap->addColumn('NAME', 'Name', 'VARCHAR', false, 100);

		$tMap->addColumn('DESCRIPTION', 'Description', 'VARCHAR', false, 255);

		$tMap->addColumn('GROUPACCESS', 'GroupAccess', 'TINYINT', false, 4);

		$tMap->addColumn('DISCRIMINATOR', 'Discriminator', 'TINYINT', false, 4);

		$tMap->addColumn('MODERATOR', 'Moderator', 'BIGINT', false, 20);

		$tMap->addColumn('PARENTID', 'ParentID', 'INTEGER', false, 9);

		$tMap->addColumn('PHOTOID', 'PhotoID', 'BIGINT', false, 20);

		$tMap->addColumn('DATECREATED', 'DateCreated', 'TIMESTAMP', false, null);

		$tMap->addColumn('ISFEATURED', 'IsFeatured', 'TINYINT', false, 1);

		$tMap->addColumn('ISSUSPENDED', 'IsSuspended', 'TINYINT', false, 1);

	} // doBuild()

} // GanetApiPropelGroupMapBuilder
