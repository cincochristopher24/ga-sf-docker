<?php


/**
 * This class adds structure of 'tblTraveler' table to 'Travel_Logs' DatabaseMap object.
 *
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    apps.travellog.ganetapi.model.propel.map
 */
class GanetApiPropelTravelerMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'apps.travellog.ganetapi.model.propel.map.GanetApiPropelTravelerMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(GanetApiPropelTravelerPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(GanetApiPropelTravelerPeer::TABLE_NAME);
		$tMap->setPhpName('GanetApiPropelTraveler');
		$tMap->setClassname('GanetApiPropelTraveler');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('TRAVELERID', 'ID', 'INTEGER', true, null);

		$tMap->addColumn('USERNAME', 'UserName', 'VARCHAR', false, 35);

		$tMap->addColumn('FIRSTNAME', 'FirstName', 'VARCHAR', false, 30);

		$tMap->addColumn('LASTNAME', 'LastName', 'VARCHAR', false, 30);

		$tMap->addColumn('GENDER', 'Gender', 'TINYINT', false, 4);

		$tMap->addColumn('EMAIL', 'Email', 'VARCHAR', false, 100);

		$tMap->addColumn('PHONE', 'Phone', 'VARCHAR', false, 36);

		$tMap->addForeignKey('HTLOCATIONID', 'HomeTownLocationID', 'BIGINT', 'tblLocation', 'LOCID', false, 20);

		$tMap->addColumn('TRAVELERTYPEID', 'TravelerTypeID', 'TINYINT', false, 11);

		$tMap->addColumn('DATEREGISTERED', 'DateRegistered', 'TIMESTAMP', false, null);

		$tMap->addColumn('LATESTLOGIN', 'LatestLogin', 'TIMESTAMP', false, null);

		$tMap->addForeignKey('CURRLOCATIONID', 'CurrentLocationID', 'BIGINT', 'tblLocation', 'LOCID', false, 20);

		$tMap->addColumn('VIEWS', 'Views', 'BIGINT', false, 11);

		$tMap->addColumn('ISADVISOR', 'IsAdvisor', 'TINYINT', false, 1);

		$tMap->addColumn('ISFEATURED', 'IsFeatured', 'TINYINT', false, 1);

		$tMap->addColumn('ISSUSPENDED', 'IsSuspended', 'TINYINT', false, 11);

		$tMap->addColumn('ACTIVE', 'IsActive', 'TINYINT', false, 1);

		$tMap->addColumn('TRAVELERSTATUS', 'TravelerStatus', 'TINYINT', false, 1);

	} // doBuild()

} // GanetApiPropelTravelerMapBuilder
