<?php

class GanetApiPropelTopic extends BaseGanetApiPropelTopic {
	
	/**
	 * Initializes internal state of GanetApiPropelPost object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}

	public function getDiscussions() {

	}

	/**
	* @param void
    * @return array - formatted array for valid function calls for GanetApiTopic
    *
    * convert traveler properties, that would be made available to the public, to an associative array
    */
    public function toArray(){    	
    	return array(
    		'ID' 				=> $this->getID(),
    		'groupID'			=> $this->getGroupID(),
    		'creatorID'			=> $this->getCreatorID(),
    		'title'				=> $this->getTitle(),
    		'description'		=> $this->getDescription()
    		'dateCreated'		=> $this->getDateCreated(),
    		'dateUpdated'		=> $this->getDateUpdated(),
    		'privacySetting'	=> $this->getPrivacySetting(),
    		'status'		 	=> $this->getStatus(),
    		'discussionsCount'	=> $this->getDiscussionsCount()

    	);
    }

	

}
