<?php

class GanetApiPropelPost extends BaseGanetApiPropelPost {
	
	/**
	 * Initializes internal state of GanetApiPropelPost object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}

	/**
	* @param void
    * @return array - formatted array for valid function calls for GanetApiPost
    *
    * convert traveler properties, that would be made available to the public, to an associative array
    */
    public function toArray(){    	
    	return array(
    		'ID' 			=> $this->getID(),
    		'discussionID'	=> $this->getDiscussionID(),
    		'posterID'		=> $this->getPosterID(),
    		'message'		=> $this->getMessage(),
    		'dateCreated'	=> $this->getDateCreated()
    	);
    }

}
