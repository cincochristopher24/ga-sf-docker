<?php

/**
 * Skeleton subclass for performing query and update operations on the 'tblTraveler' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    apps.travellog.ganetapi.model.propel
 */
class GanetApiPropelTravelerPeer extends BaseGanetApiPropelTravelerPeer {
	
	public static function doSelect(Criteria $criteria, PropelPDO $con = null) {
		$arr = parent::doSelect($criteria, $con);
		$newArr = array();
		foreach($arr as $obj) {
			$newArr[] = $obj->toGanetApiTraveler();
		}
		return $newArr;
	}
	
	public static function getTravelers($args = array()) { 		
		$criteria = new Criteria();
		self::setCriteriaFromArgs($criteria, $args);
		
		return self::doSelect($criteria);
	}
	
	public static function getFeaturedTravelers($args = array()) {
		$criteria = new Criteria();
		self::setCriteriaFromArgs($criteria, $args);
		$criteria->add(self::ISFEATURED, GanetApiCriteriaConstant::TRAVELER_IS_FEATURED);
		
		return self::doSelect($criteria);
	}
	
	public static function getRecentTravelers($args = array()) {
		$criteria = new Criteria();
		$args[GanetApiCriteriaConstant::ORDER_BY] = array(self::DATEREGISTERED, GanetApiCriteriaConstant::ORDER_DESCENDING);
		self::setCriteriaFromArgs($criteria, $args);
		
		return self::doSelect($criteria);
	}
	
	public static function getLatestLoginTravelers($args = array()) {
		$criteria = new Criteria();
		$args[GanetApiCriteriaConstant::ORDER_BY] = array(self::LATESTLOGIN, GanetApiCriteriaConstant::ORDER_DESCENDING);
		self::setCriteriaFromArgs($criteria, $args);
		
		return self::doSelect($criteria);
	}
	
	public static function getRandomTravelers($args = array()) {
		$criteria = new Criteria();
		if(array_key_exists(GanetApiCriteriaConstant::ORDER, $args)) {
			unset($args[GanetApiCriteriaConstant::ORDER]);
		}
		if(array_key_exists(GanetApiCriteriaConstant::ORDER_BY, $args)) {
			unset($args[GanetApiCriteriaConstant::ORDER_BY]);
		}	
		$criteria->addAscendingOrderByColumn('RAND()');
		self::setCriteriaFromArgs($criteria, $args);
		
		return self::doSelect($criteria);
	}
	
	private static function setCriteriaFromArgs(Criteria $criteria, $args = array()) {
	
		$criteria->add(self::ISSUSPENDED, GanetApiCriteriaConstant::TRAVELER_NOT_SUSPENDED);
		$criteria->setLimit(self::setDefaultLimit($args));
		
		if(array_key_exists(GanetApiCriteriaConstant::ORDER, $args)) {
			$order = $args[GanetApiCriteriaConstant::ORDER];
			if(in_array($order, self::getFieldNames())) {
				if(array_key_exists(GanetApiCriteriaConstant::ORDER_BY, $args)) {
					if(strtolower($args[GanetApiCriteriaConstant::ORDER_BY]) == GanetApiCriteriaConstant::ORDER_DESCENDING)
						$criteria->addDescendingOrderByColumn($order);
					else
						$criteria->addAscendingOrderByColumn($order);	
				}
				else
					$criteria->addAscendingOrderByColumn($order);
			}
		}
		
		//todo: groupID
		if($groupID = GanetApiAuthenticationValidator::getInstance()->getGroupID()) {
			
		}		
	}
	
	/**
	 * sets limit to default value if it is not set,
	 * if it is, then limits to 50.
	 */
	private static function setDefaultLimit($args) {
		
		$limit = array_key_exists(GanetApiCriteriaConstant::LIMIT, $args) 
			? ($args[GanetApiCriteriaConstant::LIMIT] > GanetApiCriteriaConstant::TRAVELER_LIMIT 
				? GanetApiCriteriaConstant::TRAVELER_LIMIT 
				: $args[GanetApiCriteriaConstant::LIMIT])
			: GanetApiCriteriaConstant::TRAVELER_DEFAULT_LIMIT;
		
		return $limit;
	}
		
} // GanetApiPropelTravelerPeer
