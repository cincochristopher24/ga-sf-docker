<?php

/**
 * Base class that represents a row from the 'tblLocation' table.
 *
 * 
 *
 * @package    apps.travellog.ganetapi.model.propel.om
 */
abstract class BaseGanetApiPropelLocation extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        GanetApiPropelLocationPeer
	 */
	protected static $peer;

	/**
	 * The value for the locid field.
	 * @var        string
	 */
	protected $locid;

	/**
	 * The value for the locationtypeid field.
	 * @var        int
	 */
	protected $locationtypeid;

	/**
	 * The value for the showontravellog field.
	 * @var        int
	 */
	protected $showontravellog;

	/**
	 * @var        array GanetApiPropelTraveler[] Collection to store aggregation of GanetApiPropelTraveler objects.
	 */
	protected $collGanetApiPropelTravelersRelatedByHomeTownLocationID;

	/**
	 * @var        Criteria The criteria used to select the current contents of collGanetApiPropelTravelersRelatedByHomeTownLocationID.
	 */
	private $lastGanetApiPropelTravelerRelatedByHomeTownLocationIDCriteria = null;

	/**
	 * @var        array GanetApiPropelTraveler[] Collection to store aggregation of GanetApiPropelTraveler objects.
	 */
	protected $collGanetApiPropelTravelersRelatedByCurrentLocationID;

	/**
	 * @var        Criteria The criteria used to select the current contents of collGanetApiPropelTravelersRelatedByCurrentLocationID.
	 */
	private $lastGanetApiPropelTravelerRelatedByCurrentLocationIDCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Initializes internal state of BaseGanetApiPropelLocation object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
	}

	/**
	 * Get the [locid] column value.
	 * 
	 * @return     string
	 */
	public function getID()
	{
		return $this->locid;
	}

	/**
	 * Get the [locationtypeid] column value.
	 * 
	 * @return     int
	 */
	public function getLocationTypeID()
	{
		return $this->locationtypeid;
	}

	/**
	 * Get the [showontravellog] column value.
	 * 
	 * @return     int
	 */
	public function getShowOnTravellog()
	{
		return $this->showontravellog;
	}

	/**
	 * Set the value of [locid] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelLocation The current object (for fluent API support)
	 */
	public function setID($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->locid !== $v) {
			$this->locid = $v;
			$this->modifiedColumns[] = GanetApiPropelLocationPeer::LOCID;
		}

		return $this;
	} // setID()

	/**
	 * Set the value of [locationtypeid] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelLocation The current object (for fluent API support)
	 */
	public function setLocationTypeID($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->locationtypeid !== $v) {
			$this->locationtypeid = $v;
			$this->modifiedColumns[] = GanetApiPropelLocationPeer::LOCATIONTYPEID;
		}

		return $this;
	} // setLocationTypeID()

	/**
	 * Set the value of [showontravellog] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelLocation The current object (for fluent API support)
	 */
	public function setShowOnTravellog($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->showontravellog !== $v) {
			$this->showontravellog = $v;
			$this->modifiedColumns[] = GanetApiPropelLocationPeer::SHOWONTRAVELLOG;
		}

		return $this;
	} // setShowOnTravellog()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			// First, ensure that we don't have any columns that have been modified which aren't default columns.
			if (array_diff($this->modifiedColumns, array())) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->locid = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
			$this->locationtypeid = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->showontravellog = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 3; // 3 = GanetApiPropelLocationPeer::NUM_COLUMNS - GanetApiPropelLocationPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating GanetApiPropelLocation object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelLocationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = GanetApiPropelLocationPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->collGanetApiPropelTravelersRelatedByHomeTownLocationID = null;
			$this->lastGanetApiPropelTravelerRelatedByHomeTownLocationIDCriteria = null;

			$this->collGanetApiPropelTravelersRelatedByCurrentLocationID = null;
			$this->lastGanetApiPropelTravelerRelatedByCurrentLocationIDCriteria = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelLocationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			GanetApiPropelLocationPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelLocationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			$affectedRows = $this->doSave($con);
			$con->commit();
			GanetApiPropelLocationPeer::addInstanceToPool($this);
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			if ($this->isNew() ) {
				$this->modifiedColumns[] = GanetApiPropelLocationPeer::LOCID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GanetApiPropelLocationPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setID($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += GanetApiPropelLocationPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID !== null) {
				foreach ($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collGanetApiPropelTravelersRelatedByCurrentLocationID !== null) {
				foreach ($this->collGanetApiPropelTravelersRelatedByCurrentLocationID as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = GanetApiPropelLocationPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID !== null) {
					foreach ($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collGanetApiPropelTravelersRelatedByCurrentLocationID !== null) {
					foreach ($this->collGanetApiPropelTravelersRelatedByCurrentLocationID as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(GanetApiPropelLocationPeer::DATABASE_NAME);

		if ($this->isColumnModified(GanetApiPropelLocationPeer::LOCID)) $criteria->add(GanetApiPropelLocationPeer::LOCID, $this->locid);
		if ($this->isColumnModified(GanetApiPropelLocationPeer::LOCATIONTYPEID)) $criteria->add(GanetApiPropelLocationPeer::LOCATIONTYPEID, $this->locationtypeid);
		if ($this->isColumnModified(GanetApiPropelLocationPeer::SHOWONTRAVELLOG)) $criteria->add(GanetApiPropelLocationPeer::SHOWONTRAVELLOG, $this->showontravellog);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GanetApiPropelLocationPeer::DATABASE_NAME);

		$criteria->add(GanetApiPropelLocationPeer::LOCID, $this->locid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     string
	 */
	public function getPrimaryKey()
	{
		return $this->getID();
	}

	/**
	 * Generic method to set the primary key (locid column).
	 *
	 * @param      string $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setID($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of GanetApiPropelLocation (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setLocationTypeID($this->locationtypeid);

		$copyObj->setShowOnTravellog($this->showontravellog);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getGanetApiPropelTravelersRelatedByHomeTownLocationID() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addGanetApiPropelTravelerRelatedByHomeTownLocationID($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getGanetApiPropelTravelersRelatedByCurrentLocationID() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addGanetApiPropelTravelerRelatedByCurrentLocationID($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setID(NULL); // this is a auto-increment column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     GanetApiPropelLocation Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     GanetApiPropelLocationPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GanetApiPropelLocationPeer();
		}
		return self::$peer;
	}

	/**
	 * Clears out the collGanetApiPropelTravelersRelatedByHomeTownLocationID collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addGanetApiPropelTravelersRelatedByHomeTownLocationID()
	 */
	public function clearGanetApiPropelTravelersRelatedByHomeTownLocationID()
	{
		$this->collGanetApiPropelTravelersRelatedByHomeTownLocationID = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collGanetApiPropelTravelersRelatedByHomeTownLocationID collection (array).
	 *
	 * By default this just sets the collGanetApiPropelTravelersRelatedByHomeTownLocationID collection to an empty array (like clearcollGanetApiPropelTravelersRelatedByHomeTownLocationID());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initGanetApiPropelTravelersRelatedByHomeTownLocationID()
	{
		$this->collGanetApiPropelTravelersRelatedByHomeTownLocationID = array();
	}

	/**
	 * Gets an array of GanetApiPropelTraveler objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this GanetApiPropelLocation has previously been saved, it will retrieve
	 * related GanetApiPropelTravelersRelatedByHomeTownLocationID from storage. If this GanetApiPropelLocation is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array GanetApiPropelTraveler[]
	 * @throws     PropelException
	 */
	public function getGanetApiPropelTravelersRelatedByHomeTownLocationID($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(GanetApiPropelLocationPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID === null) {
			if ($this->isNew()) {
			   $this->collGanetApiPropelTravelersRelatedByHomeTownLocationID = array();
			} else {

				$criteria->add(GanetApiPropelTravelerPeer::HTLOCATIONID, $this->locid);

				GanetApiPropelTravelerPeer::addSelectColumns($criteria);
				$this->collGanetApiPropelTravelersRelatedByHomeTownLocationID = GanetApiPropelTravelerPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(GanetApiPropelTravelerPeer::HTLOCATIONID, $this->locid);

				GanetApiPropelTravelerPeer::addSelectColumns($criteria);
				if (!isset($this->lastGanetApiPropelTravelerRelatedByHomeTownLocationIDCriteria) || !$this->lastGanetApiPropelTravelerRelatedByHomeTownLocationIDCriteria->equals($criteria)) {
					$this->collGanetApiPropelTravelersRelatedByHomeTownLocationID = GanetApiPropelTravelerPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastGanetApiPropelTravelerRelatedByHomeTownLocationIDCriteria = $criteria;
		return $this->collGanetApiPropelTravelersRelatedByHomeTownLocationID;
	}

	/**
	 * Returns the number of related GanetApiPropelTraveler objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related GanetApiPropelTraveler objects.
	 * @throws     PropelException
	 */
	public function countGanetApiPropelTravelersRelatedByHomeTownLocationID(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(GanetApiPropelLocationPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(GanetApiPropelTravelerPeer::HTLOCATIONID, $this->locid);

				$count = GanetApiPropelTravelerPeer::doCount($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(GanetApiPropelTravelerPeer::HTLOCATIONID, $this->locid);

				if (!isset($this->lastGanetApiPropelTravelerRelatedByHomeTownLocationIDCriteria) || !$this->lastGanetApiPropelTravelerRelatedByHomeTownLocationIDCriteria->equals($criteria)) {
					$count = GanetApiPropelTravelerPeer::doCount($criteria, $con);
				} else {
					$count = count($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID);
				}
			} else {
				$count = count($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID);
			}
		}
		$this->lastGanetApiPropelTravelerRelatedByHomeTownLocationIDCriteria = $criteria;
		return $count;
	}

	/**
	 * Method called to associate a GanetApiPropelTraveler object to this object
	 * through the GanetApiPropelTraveler foreign key attribute.
	 *
	 * @param      GanetApiPropelTraveler $l GanetApiPropelTraveler
	 * @return     void
	 * @throws     PropelException
	 */
	public function addGanetApiPropelTravelerRelatedByHomeTownLocationID(GanetApiPropelTraveler $l)
	{
		if ($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID === null) {
			$this->initGanetApiPropelTravelersRelatedByHomeTownLocationID();
		}
		if (!in_array($l, $this->collGanetApiPropelTravelersRelatedByHomeTownLocationID, true)) { // only add it if the **same** object is not already associated
			array_push($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID, $l);
			$l->setGanetApiPropelLocationRelatedByHomeTownLocationID($this);
		}
	}

	/**
	 * Clears out the collGanetApiPropelTravelersRelatedByCurrentLocationID collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addGanetApiPropelTravelersRelatedByCurrentLocationID()
	 */
	public function clearGanetApiPropelTravelersRelatedByCurrentLocationID()
	{
		$this->collGanetApiPropelTravelersRelatedByCurrentLocationID = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collGanetApiPropelTravelersRelatedByCurrentLocationID collection (array).
	 *
	 * By default this just sets the collGanetApiPropelTravelersRelatedByCurrentLocationID collection to an empty array (like clearcollGanetApiPropelTravelersRelatedByCurrentLocationID());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initGanetApiPropelTravelersRelatedByCurrentLocationID()
	{
		$this->collGanetApiPropelTravelersRelatedByCurrentLocationID = array();
	}

	/**
	 * Gets an array of GanetApiPropelTraveler objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this GanetApiPropelLocation has previously been saved, it will retrieve
	 * related GanetApiPropelTravelersRelatedByCurrentLocationID from storage. If this GanetApiPropelLocation is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array GanetApiPropelTraveler[]
	 * @throws     PropelException
	 */
	public function getGanetApiPropelTravelersRelatedByCurrentLocationID($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(GanetApiPropelLocationPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collGanetApiPropelTravelersRelatedByCurrentLocationID === null) {
			if ($this->isNew()) {
			   $this->collGanetApiPropelTravelersRelatedByCurrentLocationID = array();
			} else {

				$criteria->add(GanetApiPropelTravelerPeer::CURRLOCATIONID, $this->locid);

				GanetApiPropelTravelerPeer::addSelectColumns($criteria);
				$this->collGanetApiPropelTravelersRelatedByCurrentLocationID = GanetApiPropelTravelerPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(GanetApiPropelTravelerPeer::CURRLOCATIONID, $this->locid);

				GanetApiPropelTravelerPeer::addSelectColumns($criteria);
				if (!isset($this->lastGanetApiPropelTravelerRelatedByCurrentLocationIDCriteria) || !$this->lastGanetApiPropelTravelerRelatedByCurrentLocationIDCriteria->equals($criteria)) {
					$this->collGanetApiPropelTravelersRelatedByCurrentLocationID = GanetApiPropelTravelerPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastGanetApiPropelTravelerRelatedByCurrentLocationIDCriteria = $criteria;
		return $this->collGanetApiPropelTravelersRelatedByCurrentLocationID;
	}

	/**
	 * Returns the number of related GanetApiPropelTraveler objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related GanetApiPropelTraveler objects.
	 * @throws     PropelException
	 */
	public function countGanetApiPropelTravelersRelatedByCurrentLocationID(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(GanetApiPropelLocationPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collGanetApiPropelTravelersRelatedByCurrentLocationID === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(GanetApiPropelTravelerPeer::CURRLOCATIONID, $this->locid);

				$count = GanetApiPropelTravelerPeer::doCount($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(GanetApiPropelTravelerPeer::CURRLOCATIONID, $this->locid);

				if (!isset($this->lastGanetApiPropelTravelerRelatedByCurrentLocationIDCriteria) || !$this->lastGanetApiPropelTravelerRelatedByCurrentLocationIDCriteria->equals($criteria)) {
					$count = GanetApiPropelTravelerPeer::doCount($criteria, $con);
				} else {
					$count = count($this->collGanetApiPropelTravelersRelatedByCurrentLocationID);
				}
			} else {
				$count = count($this->collGanetApiPropelTravelersRelatedByCurrentLocationID);
			}
		}
		$this->lastGanetApiPropelTravelerRelatedByCurrentLocationIDCriteria = $criteria;
		return $count;
	}

	/**
	 * Method called to associate a GanetApiPropelTraveler object to this object
	 * through the GanetApiPropelTraveler foreign key attribute.
	 *
	 * @param      GanetApiPropelTraveler $l GanetApiPropelTraveler
	 * @return     void
	 * @throws     PropelException
	 */
	public function addGanetApiPropelTravelerRelatedByCurrentLocationID(GanetApiPropelTraveler $l)
	{
		if ($this->collGanetApiPropelTravelersRelatedByCurrentLocationID === null) {
			$this->initGanetApiPropelTravelersRelatedByCurrentLocationID();
		}
		if (!in_array($l, $this->collGanetApiPropelTravelersRelatedByCurrentLocationID, true)) { // only add it if the **same** object is not already associated
			array_push($this->collGanetApiPropelTravelersRelatedByCurrentLocationID, $l);
			$l->setGanetApiPropelLocationRelatedByCurrentLocationID($this);
		}
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collGanetApiPropelTravelersRelatedByHomeTownLocationID) {
				foreach ((array) $this->collGanetApiPropelTravelersRelatedByHomeTownLocationID as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collGanetApiPropelTravelersRelatedByCurrentLocationID) {
				foreach ((array) $this->collGanetApiPropelTravelersRelatedByCurrentLocationID as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		$this->collGanetApiPropelTravelersRelatedByHomeTownLocationID = null;
		$this->collGanetApiPropelTravelersRelatedByCurrentLocationID = null;
	}

} // BaseGanetApiPropelLocation
