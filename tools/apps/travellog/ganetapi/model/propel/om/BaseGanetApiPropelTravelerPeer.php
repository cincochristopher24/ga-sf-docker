<?php

/**
 * Base static class for performing query and update operations on the 'tblTraveler' table.
 *
 * 
 *
 * @package    apps.travellog.ganetapi.model.propel.om
 */
abstract class BaseGanetApiPropelTravelerPeer {

	/** the default database name for this class */
	const DATABASE_NAME = 'Travel_Logs';

	/** the table name for this class */
	const TABLE_NAME = 'tblTraveler';

	/** A class that can be returned by this peer. */
	const CLASS_DEFAULT = 'apps.travellog.ganetapi.model.propel.GanetApiPropelTraveler';

	/** The total number of columns. */
	const NUM_COLUMNS = 18;

	/** The number of lazy-loaded columns. */
	const NUM_LAZY_LOAD_COLUMNS = 0;

	/** the column name for the TRAVELERID field */
	const TRAVELERID = 'tblTraveler.TRAVELERID';

	/** the column name for the USERNAME field */
	const USERNAME = 'tblTraveler.USERNAME';

	/** the column name for the FIRSTNAME field */
	const FIRSTNAME = 'tblTraveler.FIRSTNAME';

	/** the column name for the LASTNAME field */
	const LASTNAME = 'tblTraveler.LASTNAME';

	/** the column name for the GENDER field */
	const GENDER = 'tblTraveler.GENDER';

	/** the column name for the EMAIL field */
	const EMAIL = 'tblTraveler.EMAIL';

	/** the column name for the PHONE field */
	const PHONE = 'tblTraveler.PHONE';

	/** the column name for the HTLOCATIONID field */
	const HTLOCATIONID = 'tblTraveler.HTLOCATIONID';

	/** the column name for the TRAVELERTYPEID field */
	const TRAVELERTYPEID = 'tblTraveler.TRAVELERTYPEID';

	/** the column name for the DATEREGISTERED field */
	const DATEREGISTERED = 'tblTraveler.DATEREGISTERED';

	/** the column name for the LATESTLOGIN field */
	const LATESTLOGIN = 'tblTraveler.LATESTLOGIN';

	/** the column name for the CURRLOCATIONID field */
	const CURRLOCATIONID = 'tblTraveler.CURRLOCATIONID';

	/** the column name for the VIEWS field */
	const VIEWS = 'tblTraveler.VIEWS';

	/** the column name for the ISADVISOR field */
	const ISADVISOR = 'tblTraveler.ISADVISOR';

	/** the column name for the ISFEATURED field */
	const ISFEATURED = 'tblTraveler.ISFEATURED';

	/** the column name for the ISSUSPENDED field */
	const ISSUSPENDED = 'tblTraveler.ISSUSPENDED';

	/** the column name for the ACTIVE field */
	const ACTIVE = 'tblTraveler.ACTIVE';

	/** the column name for the TRAVELERSTATUS field */
	const TRAVELERSTATUS = 'tblTraveler.TRAVELERSTATUS';

	/**
	 * An identiy map to hold any loaded instances of GanetApiPropelTraveler objects.
	 * This must be public so that other peer classes can access this when hydrating from JOIN
	 * queries.
	 * @var        array GanetApiPropelTraveler[]
	 */
	public static $instances = array();

	/**
	 * The MapBuilder instance for this peer.
	 * @var        MapBuilder
	 */
	private static $mapBuilder = null;

	/**
	 * holds an array of fieldnames
	 *
	 * first dimension keys are the type constants
	 * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
	 */
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('ID', 'UserName', 'FirstName', 'LastName', 'Gender', 'Email', 'Phone', 'HomeTownLocationID', 'TravelerTypeID', 'DateRegistered', 'LatestLogin', 'CurrentLocationID', 'Views', 'IsAdvisor', 'IsFeatured', 'IsSuspended', 'IsActive', 'TravelerStatus', ),
		BasePeer::TYPE_STUDLYPHPNAME => array ('iD', 'userName', 'firstName', 'lastName', 'gender', 'email', 'phone', 'homeTownLocationID', 'travelerTypeID', 'dateRegistered', 'latestLogin', 'currentLocationID', 'views', 'isAdvisor', 'isFeatured', 'isSuspended', 'isActive', 'travelerStatus', ),
		BasePeer::TYPE_COLNAME => array (self::TRAVELERID, self::USERNAME, self::FIRSTNAME, self::LASTNAME, self::GENDER, self::EMAIL, self::PHONE, self::HTLOCATIONID, self::TRAVELERTYPEID, self::DATEREGISTERED, self::LATESTLOGIN, self::CURRLOCATIONID, self::VIEWS, self::ISADVISOR, self::ISFEATURED, self::ISSUSPENDED, self::ACTIVE, self::TRAVELERSTATUS, ),
		BasePeer::TYPE_FIELDNAME => array ('travelerID', 'username', 'firstname', 'lastname', 'gender', 'email', 'phone', 'htlocationID', 'travelertypeID', 'dateregistered', 'latestlogin', 'currlocationID', 'views', 'isadvisor', 'isfeatured', 'isSuspended', 'active', 'travelerStatus', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
	);

	/**
	 * holds an array of keys for quick access to the fieldnames array
	 *
	 * first dimension keys are the type constants
	 * e.g. self::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
	 */
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('ID' => 0, 'UserName' => 1, 'FirstName' => 2, 'LastName' => 3, 'Gender' => 4, 'Email' => 5, 'Phone' => 6, 'HomeTownLocationID' => 7, 'TravelerTypeID' => 8, 'DateRegistered' => 9, 'LatestLogin' => 10, 'CurrentLocationID' => 11, 'Views' => 12, 'IsAdvisor' => 13, 'IsFeatured' => 14, 'IsSuspended' => 15, 'IsActive' => 16, 'TravelerStatus' => 17, ),
		BasePeer::TYPE_STUDLYPHPNAME => array ('iD' => 0, 'userName' => 1, 'firstName' => 2, 'lastName' => 3, 'gender' => 4, 'email' => 5, 'phone' => 6, 'homeTownLocationID' => 7, 'travelerTypeID' => 8, 'dateRegistered' => 9, 'latestLogin' => 10, 'currentLocationID' => 11, 'views' => 12, 'isAdvisor' => 13, 'isFeatured' => 14, 'isSuspended' => 15, 'isActive' => 16, 'travelerStatus' => 17, ),
		BasePeer::TYPE_COLNAME => array (self::TRAVELERID => 0, self::USERNAME => 1, self::FIRSTNAME => 2, self::LASTNAME => 3, self::GENDER => 4, self::EMAIL => 5, self::PHONE => 6, self::HTLOCATIONID => 7, self::TRAVELERTYPEID => 8, self::DATEREGISTERED => 9, self::LATESTLOGIN => 10, self::CURRLOCATIONID => 11, self::VIEWS => 12, self::ISADVISOR => 13, self::ISFEATURED => 14, self::ISSUSPENDED => 15, self::ACTIVE => 16, self::TRAVELERSTATUS => 17, ),
		BasePeer::TYPE_FIELDNAME => array ('travelerID' => 0, 'username' => 1, 'firstname' => 2, 'lastname' => 3, 'gender' => 4, 'email' => 5, 'phone' => 6, 'htlocationID' => 7, 'travelertypeID' => 8, 'dateregistered' => 9, 'latestlogin' => 10, 'currlocationID' => 11, 'views' => 12, 'isadvisor' => 13, 'isfeatured' => 14, 'isSuspended' => 15, 'active' => 16, 'travelerStatus' => 17, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
	);

	/**
	 * Get a (singleton) instance of the MapBuilder for this peer class.
	 * @return     MapBuilder The map builder for this peer
	 */
	public static function getMapBuilder()
	{
		if (self::$mapBuilder === null) {
			self::$mapBuilder = new GanetApiPropelTravelerMapBuilder();
		}
		return self::$mapBuilder;
	}
	/**
	 * Translates a fieldname to another type
	 *
	 * @param      string $name field name
	 * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @param      string $toType   One of the class type constants
	 * @return     string translated name of the field.
	 * @throws     PropelException - if the specified name could not be found in the fieldname mappings.
	 */
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	/**
	 * Returns an array of field names.
	 *
	 * @param      string $type The type of fieldnames to return:
	 *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     array A list of field names
	 */

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	/**
	 * Convenience method which changes table.column to alias.column.
	 *
	 * Using this method you can maintain SQL abstraction while using column aliases.
	 * <code>
	 *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
	 *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
	 * </code>
	 * @param      string $alias The alias for the current table.
	 * @param      string $column The column name for current table. (i.e. GanetApiPropelTravelerPeer::COLUMN_NAME).
	 * @return     string
	 */
	public static function alias($alias, $column)
	{
		return str_replace(GanetApiPropelTravelerPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	/**
	 * Add all the columns needed to create a new object.
	 *
	 * Note: any columns that were marked with lazyLoad="true" in the
	 * XML schema will not be added to the select list and only loaded
	 * on demand.
	 *
	 * @param      criteria object containing the columns to add.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::TRAVELERID);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::USERNAME);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::FIRSTNAME);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::LASTNAME);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::GENDER);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::EMAIL);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::PHONE);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::HTLOCATIONID);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::TRAVELERTYPEID);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::DATEREGISTERED);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::LATESTLOGIN);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::CURRLOCATIONID);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::VIEWS);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::ISADVISOR);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::ISFEATURED);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::ISSUSPENDED);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::ACTIVE);

		$criteria->addSelectColumn(GanetApiPropelTravelerPeer::TRAVELERSTATUS);

	}

	/**
	 * Returns the number of rows matching criteria.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @return     int Number of matching rows.
	 */
	public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
	{
		// we may modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(GanetApiPropelTravelerPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			GanetApiPropelTravelerPeer::addSelectColumns($criteria);
		}

		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		$criteria->setDbName(self::DATABASE_NAME); // Set the correct dbName

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		// BasePeer returns a PDOStatement
		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}
	/**
	 * Method to select one object from the DB.
	 *
	 * @param      Criteria $criteria object used to create the SELECT statement.
	 * @param      PropelPDO $con
	 * @return     GanetApiPropelTraveler
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = GanetApiPropelTravelerPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	/**
	 * Method to do selects.
	 *
	 * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
	 * @param      PropelPDO $con
	 * @return     array Array of selected Objects
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelect(Criteria $criteria, PropelPDO $con = null)
	{
		return GanetApiPropelTravelerPeer::populateObjects(GanetApiPropelTravelerPeer::doSelectStmt($criteria, $con));
	}
	/**
	 * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
	 *
	 * Use this method directly if you want to work with an executed statement durirectly (for example
	 * to perform your own object hydration).
	 *
	 * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
	 * @param      PropelPDO $con The connection to use
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 * @return     PDOStatement The executed PDOStatement object.
	 * @see        BasePeer::doSelect()
	 */
	public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		if (!$criteria->hasSelectClause()) {
			$criteria = clone $criteria;
			GanetApiPropelTravelerPeer::addSelectColumns($criteria);
		}

		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		// BasePeer returns a PDOStatement
		return BasePeer::doSelect($criteria, $con);
	}
	/**
	 * Adds an object to the instance pool.
	 *
	 * Propel keeps cached copies of objects in an instance pool when they are retrieved
	 * from the database.  In some cases -- especially when you override doSelect*()
	 * methods in your stub classes -- you may need to explicitly add objects
	 * to the cache in order to ensure that the same objects are always returned by doSelect*()
	 * and retrieveByPK*() calls.
	 *
	 * @param      GanetApiPropelTraveler $value A GanetApiPropelTraveler object.
	 * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
	 */
	public static function addInstanceToPool(GanetApiPropelTraveler $obj, $key = null)
	{
		if (Propel::isInstancePoolingEnabled()) {
			if ($key === null) {
				$key = (string) $obj->getID();
			} // if key === null
			self::$instances[$key] = $obj;
		}
	}

	/**
	 * Removes an object from the instance pool.
	 *
	 * Propel keeps cached copies of objects in an instance pool when they are retrieved
	 * from the database.  In some cases -- especially when you override doDelete
	 * methods in your stub classes -- you may need to explicitly remove objects
	 * from the cache in order to prevent returning objects that no longer exist.
	 *
	 * @param      mixed $value A GanetApiPropelTraveler object or a primary key value.
	 */
	public static function removeInstanceFromPool($value)
	{
		if (Propel::isInstancePoolingEnabled() && $value !== null) {
			if (is_object($value) && $value instanceof GanetApiPropelTraveler) {
				$key = (string) $value->getID();
			} elseif (is_scalar($value)) {
				// assume we've been passed a primary key
				$key = (string) $value;
			} else {
				$e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or GanetApiPropelTraveler object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
				throw $e;
			}

			unset(self::$instances[$key]);
		}
	} // removeInstanceFromPool()

	/**
	 * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
	 *
	 * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
	 * a multi-column primary key, a serialize()d version of the primary key will be returned.
	 *
	 * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
	 * @return     GanetApiPropelTraveler Found object or NULL if 1) no instance exists for specified key or 2) instance pooling has been disabled.
	 * @see        getPrimaryKeyHash()
	 */
	public static function getInstanceFromPool($key)
	{
		if (Propel::isInstancePoolingEnabled()) {
			if (isset(self::$instances[$key])) {
				return self::$instances[$key];
			}
		}
		return null; // just to be explicit
	}
	
	/**
	 * Clear the instance pool.
	 *
	 * @return     void
	 */
	public static function clearInstancePool()
	{
		self::$instances = array();
	}
	
	/**
	 * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
	 *
	 * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
	 * a multi-column primary key, a serialize()d version of the primary key will be returned.
	 *
	 * @param      array $row PropelPDO resultset row.
	 * @param      int $startcol The 0-based offset for reading from the resultset row.
	 * @return     string A string version of PK or NULL if the components of primary key in result array are all null.
	 */
	public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
	{
		// If the PK cannot be derived from the row, return NULL.
		if ($row[$startcol + 0] === null) {
			return null;
		}
		return (string) $row[$startcol + 0];
	}

	/**
	 * The returned array will contain objects of the default type or
	 * objects that inherit from the default.
	 *
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function populateObjects(PDOStatement $stmt)
	{
		$results = array();
	
		// set the class once to avoid overhead in the loop
		$cls = GanetApiPropelTravelerPeer::getOMClass();
		$cls = substr('.'.$cls, strrpos('.'.$cls, '.') + 1);
		// populate the object(s)
		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key = GanetApiPropelTravelerPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj = GanetApiPropelTravelerPeer::getInstanceFromPool($key))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj->hydrate($row, 0, true); // rehydrate
				$results[] = $obj;
			} else {
		
				$obj = new $cls();
				$obj->hydrate($row);
				$results[] = $obj;
				GanetApiPropelTravelerPeer::addInstanceToPool($obj, $key);
			} // if key exists
		}
		$stmt->closeCursor();
		return $results;
	}

	/**
	 * Returns the number of rows matching criteria, joining the related GanetApiPropelLocationRelatedByHomeTownLocationID table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinGanetApiPropelLocationRelatedByHomeTownLocationID(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(GanetApiPropelTravelerPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			GanetApiPropelTravelerPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(array(GanetApiPropelTravelerPeer::HTLOCATIONID,), array(GanetApiPropelLocationPeer::LOCID,), $join_behavior);
		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related GanetApiPropelLocationRelatedByCurrentLocationID table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinGanetApiPropelLocationRelatedByCurrentLocationID(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(GanetApiPropelTravelerPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			GanetApiPropelTravelerPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(array(GanetApiPropelTravelerPeer::CURRLOCATIONID,), array(GanetApiPropelLocationPeer::LOCID,), $join_behavior);
		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Selects a collection of GanetApiPropelTraveler objects pre-filled with their GanetApiPropelLocation objects.
	 * @param      Criteria  $c
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of GanetApiPropelTraveler objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinGanetApiPropelLocationRelatedByHomeTownLocationID(Criteria $c, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		GanetApiPropelTravelerPeer::addSelectColumns($c);
		$startcol = (GanetApiPropelTravelerPeer::NUM_COLUMNS - GanetApiPropelTravelerPeer::NUM_LAZY_LOAD_COLUMNS);
		GanetApiPropelLocationPeer::addSelectColumns($c);

		$c->addJoin(array(GanetApiPropelTravelerPeer::HTLOCATIONID,), array(GanetApiPropelLocationPeer::LOCID,), $join_behavior);
		$stmt = BasePeer::doSelect($c, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = GanetApiPropelTravelerPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = GanetApiPropelTravelerPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$omClass = GanetApiPropelTravelerPeer::getOMClass();

				$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
				$obj1 = new $cls();
				$obj1->hydrate($row);
				GanetApiPropelTravelerPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = GanetApiPropelLocationPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = GanetApiPropelLocationPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$omClass = GanetApiPropelLocationPeer::getOMClass();

					$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					GanetApiPropelLocationPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded

				// Add the $obj1 (GanetApiPropelTraveler) to $obj2 (GanetApiPropelLocation)
				$obj2->addGanetApiPropelTravelerRelatedByHomeTownLocationID($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of GanetApiPropelTraveler objects pre-filled with their GanetApiPropelLocation objects.
	 * @param      Criteria  $c
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of GanetApiPropelTraveler objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinGanetApiPropelLocationRelatedByCurrentLocationID(Criteria $c, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		GanetApiPropelTravelerPeer::addSelectColumns($c);
		$startcol = (GanetApiPropelTravelerPeer::NUM_COLUMNS - GanetApiPropelTravelerPeer::NUM_LAZY_LOAD_COLUMNS);
		GanetApiPropelLocationPeer::addSelectColumns($c);

		$c->addJoin(array(GanetApiPropelTravelerPeer::CURRLOCATIONID,), array(GanetApiPropelLocationPeer::LOCID,), $join_behavior);
		$stmt = BasePeer::doSelect($c, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = GanetApiPropelTravelerPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = GanetApiPropelTravelerPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$omClass = GanetApiPropelTravelerPeer::getOMClass();

				$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
				$obj1 = new $cls();
				$obj1->hydrate($row);
				GanetApiPropelTravelerPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = GanetApiPropelLocationPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = GanetApiPropelLocationPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$omClass = GanetApiPropelLocationPeer::getOMClass();

					$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					GanetApiPropelLocationPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded

				// Add the $obj1 (GanetApiPropelTraveler) to $obj2 (GanetApiPropelLocation)
				$obj2->addGanetApiPropelTravelerRelatedByCurrentLocationID($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Returns the number of rows matching criteria, joining all related tables
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(GanetApiPropelTravelerPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			GanetApiPropelTravelerPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(array(GanetApiPropelTravelerPeer::HTLOCATIONID,), array(GanetApiPropelLocationPeer::LOCID,), $join_behavior);
		$criteria->addJoin(array(GanetApiPropelTravelerPeer::CURRLOCATIONID,), array(GanetApiPropelLocationPeer::LOCID,), $join_behavior);
		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}

	/**
	 * Selects a collection of GanetApiPropelTraveler objects pre-filled with all related objects.
	 *
	 * @param      Criteria  $c
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of GanetApiPropelTraveler objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAll(Criteria $c, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		GanetApiPropelTravelerPeer::addSelectColumns($c);
		$startcol2 = (GanetApiPropelTravelerPeer::NUM_COLUMNS - GanetApiPropelTravelerPeer::NUM_LAZY_LOAD_COLUMNS);

		GanetApiPropelLocationPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + (GanetApiPropelLocationPeer::NUM_COLUMNS - GanetApiPropelLocationPeer::NUM_LAZY_LOAD_COLUMNS);

		GanetApiPropelLocationPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + (GanetApiPropelLocationPeer::NUM_COLUMNS - GanetApiPropelLocationPeer::NUM_LAZY_LOAD_COLUMNS);

		$c->addJoin(array(GanetApiPropelTravelerPeer::HTLOCATIONID,), array(GanetApiPropelLocationPeer::LOCID,), $join_behavior);
		$c->addJoin(array(GanetApiPropelTravelerPeer::CURRLOCATIONID,), array(GanetApiPropelLocationPeer::LOCID,), $join_behavior);
		$stmt = BasePeer::doSelect($c, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = GanetApiPropelTravelerPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = GanetApiPropelTravelerPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$omClass = GanetApiPropelTravelerPeer::getOMClass();

				$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
				$obj1 = new $cls();
				$obj1->hydrate($row);
				GanetApiPropelTravelerPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

			// Add objects for joined GanetApiPropelLocation rows

			$key2 = GanetApiPropelLocationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
			if ($key2 !== null) {
				$obj2 = GanetApiPropelLocationPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$omClass = GanetApiPropelLocationPeer::getOMClass();


					$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					GanetApiPropelLocationPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 loaded

				// Add the $obj1 (GanetApiPropelTraveler) to the collection in $obj2 (GanetApiPropelLocation)
				$obj2->addGanetApiPropelTravelerRelatedByHomeTownLocationID($obj1);
			} // if joined row not null

			// Add objects for joined GanetApiPropelLocation rows

			$key3 = GanetApiPropelLocationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
			if ($key3 !== null) {
				$obj3 = GanetApiPropelLocationPeer::getInstanceFromPool($key3);
				if (!$obj3) {

					$omClass = GanetApiPropelLocationPeer::getOMClass();


					$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					GanetApiPropelLocationPeer::addInstanceToPool($obj3, $key3);
				} // if obj3 loaded

				// Add the $obj1 (GanetApiPropelTraveler) to the collection in $obj3 (GanetApiPropelLocation)
				$obj3->addGanetApiPropelTravelerRelatedByCurrentLocationID($obj1);
			} // if joined row not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related GanetApiPropelLocationRelatedByHomeTownLocationID table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptGanetApiPropelLocationRelatedByHomeTownLocationID(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(GanetApiPropelTravelerPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			GanetApiPropelTravelerPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related GanetApiPropelLocationRelatedByCurrentLocationID table
	 *
	 * @param      Criteria $c
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptGanetApiPropelLocationRelatedByCurrentLocationID(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(GanetApiPropelTravelerPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			GanetApiPropelTravelerPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Selects a collection of GanetApiPropelTraveler objects pre-filled with all related objects except GanetApiPropelLocationRelatedByHomeTownLocationID.
	 *
	 * @param      Criteria  $c
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of GanetApiPropelTraveler objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptGanetApiPropelLocationRelatedByHomeTownLocationID(Criteria $c, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		// $c->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		GanetApiPropelTravelerPeer::addSelectColumns($c);
		$startcol2 = (GanetApiPropelTravelerPeer::NUM_COLUMNS - GanetApiPropelTravelerPeer::NUM_LAZY_LOAD_COLUMNS);


		$stmt = BasePeer::doSelect($c, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = GanetApiPropelTravelerPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = GanetApiPropelTravelerPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$omClass = GanetApiPropelTravelerPeer::getOMClass();

				$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
				$obj1 = new $cls();
				$obj1->hydrate($row);
				GanetApiPropelTravelerPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of GanetApiPropelTraveler objects pre-filled with all related objects except GanetApiPropelLocationRelatedByCurrentLocationID.
	 *
	 * @param      Criteria  $c
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of GanetApiPropelTraveler objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptGanetApiPropelLocationRelatedByCurrentLocationID(Criteria $c, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$c = clone $c;

		// Set the correct dbName if it has not been overridden
		// $c->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		GanetApiPropelTravelerPeer::addSelectColumns($c);
		$startcol2 = (GanetApiPropelTravelerPeer::NUM_COLUMNS - GanetApiPropelTravelerPeer::NUM_LAZY_LOAD_COLUMNS);


		$stmt = BasePeer::doSelect($c, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = GanetApiPropelTravelerPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = GanetApiPropelTravelerPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$omClass = GanetApiPropelTravelerPeer::getOMClass();

				$cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);
				$obj1 = new $cls();
				$obj1->hydrate($row);
				GanetApiPropelTravelerPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}

	/**
	 * Returns the TableMap related to this peer.
	 * This method is not needed for general use but a specific application could have a need.
	 * @return     TableMap
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	/**
	 * The class that the Peer will make instances of.
	 *
	 * This uses a dot-path notation which is tranalted into a path
	 * relative to a location on the PHP include_path.
	 * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
	 *
	 * @return     string path.to.ClassName
	 */
	public static function getOMClass()
	{
		return GanetApiPropelTravelerPeer::CLASS_DEFAULT;
	}

	/**
	 * Method perform an INSERT on the database, given a GanetApiPropelTraveler or Criteria object.
	 *
	 * @param      mixed $values Criteria or GanetApiPropelTraveler object containing data that is used to create the INSERT statement.
	 * @param      PropelPDO $con the PropelPDO connection to use
	 * @return     mixed The new primary key.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doInsert($values, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity
		} else {
			$criteria = $values->buildCriteria(); // build Criteria from GanetApiPropelTraveler object
		}

		if ($criteria->containsKey(GanetApiPropelTravelerPeer::TRAVELERID) && $criteria->keyContainsValue(GanetApiPropelTravelerPeer::TRAVELERID) ) {
			throw new PropelException('Cannot insert a value for auto-increment primary key ('.GanetApiPropelTravelerPeer::TRAVELERID.')');
		}


		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		try {
			// use transaction because $criteria could contain info
			// for more than one table (I guess, conceivably)
			$con->beginTransaction();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollBack();
			throw $e;
		}

		return $pk;
	}

	/**
	 * Method perform an UPDATE on the database, given a GanetApiPropelTraveler or Criteria object.
	 *
	 * @param      mixed $values Criteria or GanetApiPropelTraveler object containing data that is used to create the UPDATE statement.
	 * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
	 * @return     int The number of affected rows (if supported by underlying database driver).
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doUpdate($values, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity

			$comparison = $criteria->getComparison(GanetApiPropelTravelerPeer::TRAVELERID);
			$selectCriteria->add(GanetApiPropelTravelerPeer::TRAVELERID, $criteria->remove(GanetApiPropelTravelerPeer::TRAVELERID), $comparison);

		} else { // $values is GanetApiPropelTraveler object
			$criteria = $values->buildCriteria(); // gets full criteria
			$selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
		}

		// set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	/**
	 * Method to DELETE all rows from the tblTraveler table.
	 *
	 * @return     int The number of affected rows (if supported by underlying database driver).
	 */
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		$affectedRows = 0; // initialize var to track total num of affected rows
		try {
			// use transaction because $criteria could contain info
			// for more than one table or we could emulating ON DELETE CASCADE, etc.
			$con->beginTransaction();
			$affectedRows += BasePeer::doDeleteAll(GanetApiPropelTravelerPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Method perform a DELETE on the database, given a GanetApiPropelTraveler or Criteria object OR a primary key value.
	 *
	 * @param      mixed $values Criteria or GanetApiPropelTraveler object or primary key or array of primary keys
	 *              which is used to create the DELETE statement
	 * @param      PropelPDO $con the connection to use
	 * @return     int 	The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
	 *				if supported by native driver or if emulated using Propel.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	 public static function doDelete($values, PropelPDO $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		if ($values instanceof Criteria) {
			// invalidate the cache for all objects of this type, since we have no
			// way of knowing (without running a query) what objects should be invalidated
			// from the cache based on this Criteria.
			GanetApiPropelTravelerPeer::clearInstancePool();

			// rename for clarity
			$criteria = clone $values;
		} elseif ($values instanceof GanetApiPropelTraveler) {
			// invalidate the cache for this single object
			GanetApiPropelTravelerPeer::removeInstanceFromPool($values);
			// create criteria based on pk values
			$criteria = $values->buildPkeyCriteria();
		} else {
			// it must be the primary key



			$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(GanetApiPropelTravelerPeer::TRAVELERID, (array) $values, Criteria::IN);

			foreach ((array) $values as $singleval) {
				// we can invalidate the cache for this single object
				GanetApiPropelTravelerPeer::removeInstanceFromPool($singleval);
			}
		}

		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; // initialize var to track total num of affected rows

		try {
			// use transaction because $criteria could contain info
			// for more than one table or we could emulating ON DELETE CASCADE, etc.
			$con->beginTransaction();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);

			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Validates all modified columns of given GanetApiPropelTraveler object.
	 * If parameter $columns is either a single column name or an array of column names
	 * than only those columns are validated.
	 *
	 * NOTICE: This does not apply to primary or foreign keys for now.
	 *
	 * @param      GanetApiPropelTraveler $obj The object to validate.
	 * @param      mixed $cols Column name or array of column names.
	 *
	 * @return     mixed TRUE if all columns are valid or the error message of the first invalid column.
	 */
	public static function doValidate(GanetApiPropelTraveler $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(GanetApiPropelTravelerPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(GanetApiPropelTravelerPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach ($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		return BasePeer::doValidate(GanetApiPropelTravelerPeer::DATABASE_NAME, GanetApiPropelTravelerPeer::TABLE_NAME, $columns);
	}

	/**
	 * Retrieve a single object by pkey.
	 *
	 * @param      int $pk the primary key.
	 * @param      PropelPDO $con the connection to use
	 * @return     GanetApiPropelTraveler
	 */
	public static function retrieveByPK($pk, PropelPDO $con = null)
	{

		if (null !== ($obj = GanetApiPropelTravelerPeer::getInstanceFromPool((string) $pk))) {
			return $obj;
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria = new Criteria(GanetApiPropelTravelerPeer::DATABASE_NAME);
		$criteria->add(GanetApiPropelTravelerPeer::TRAVELERID, $pk);

		$v = GanetApiPropelTravelerPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	/**
	 * Retrieve multiple objects by pkey.
	 *
	 * @param      array $pks List of primary keys
	 * @param      PropelPDO $con the connection to use
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function retrieveByPKs($pks, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria(GanetApiPropelTravelerPeer::DATABASE_NAME);
			$criteria->add(GanetApiPropelTravelerPeer::TRAVELERID, $pks, Criteria::IN);
			$objs = GanetApiPropelTravelerPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} // BaseGanetApiPropelTravelerPeer

// This is the static code needed to register the MapBuilder for this table with the main Propel class.
//
// NOTE: This static code cannot call methods on the GanetApiPropelTravelerPeer class, because it is not defined yet.
// If you need to use overridden methods, you can add this code to the bottom of the GanetApiPropelTravelerPeer class:
//
// Propel::getDatabaseMap(GanetApiPropelTravelerPeer::DATABASE_NAME)->addTableBuilder(GanetApiPropelTravelerPeer::TABLE_NAME, GanetApiPropelTravelerPeer::getMapBuilder());
//
// Doing so will effectively overwrite the registration below.

Propel::getDatabaseMap(BaseGanetApiPropelTravelerPeer::DATABASE_NAME)->addTableBuilder(BaseGanetApiPropelTravelerPeer::TABLE_NAME, BaseGanetApiPropelTravelerPeer::getMapBuilder());

