<?php

/**
 * Skeleton subclass for representing a row from the 'tblTraveler' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    apps.travellog.ganetapi.model.propel
 */
class GanetApiPropelTraveler extends BaseGanetApiPropelTraveler {

	/**
	 * Initializes internal state of GanetApiPropelTraveler object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}
	
	/**
	* @param void
    * @return array - formatted array for valid function calls for GanetApiPropelTraveler
    *
    * convert traveler properties, that would be made available to the public, to an associative array
    */
    public function toArray(){
    	$url      = 'url';
    	$photo    = 'photo';
    	$location = 'location';
    	
    	return array(
    		'ID' 			=> $this->getID(),
    		'UserName' 		=> $this->getUserName(),
    		'Url'			=> $url,
    		'Photo'			=> $photo,
    		'Location'		=> $location
    	);
    }
    
    /**
    * @param void
    * @return GanetApiTraveler Object
    *
    * converts to GanetApiTraveler Object
    */
    public function toGanetApiTraveler() {
    	if(get_class($this) == 'GanetApiTraveler')
    		return $this;
    	$obj = new GanetApiTraveler();
    	$this->copyInto($obj);
    	$obj->setID($this->getID());
    	return $obj;
    }

} // GanetApiPropelTraveler
