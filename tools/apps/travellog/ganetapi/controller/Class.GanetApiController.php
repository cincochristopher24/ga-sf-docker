<?php

class GanetApiController {

	//parameter holder
	private $args  = null;
	
	//validator
	private $validator = null;
	
	/**
	 * Class Constructor - accepts parameter (usually get parameters) and
	 * sets it to class property $args. Also initializes GanetApiValidator
	 *
	 * @param array $param
	 */
	public function __construct($param = array()) {
		$this->args      = $param;
		$this->validator = GanetApiValidator::getInstance();
	}
	
	/**
	 * Validator
	 *
	 * @see GanetApiValidator
	 */
	private function validate() {
		if(!$this->validator->isValidated()) {
			$this->validator->setParameters($this->args);
			$this->validator->validate();
		}
	}
	
	/**
	 * Check for errors. if not yet validated, invokes validate() method.
	 *
	 * @return String error | Boolean false
	 */
	private function checkError() {
		if(!$this->validator->isValidated())
			$this->validate();
		$error = $this->validator->getErrorMessage();
		return $error ? $error : false;
	}	
			
	/**
	 * Executes
	 *
	 * @return result of $class->{method}($this->args) | Error Message
	 */
	public function execute() {
		$this->validate();
		$error = $this->checkError();
		if ( $error !== false ) {
			$result = "ERROR! $error";
		}
		else {
			$class  = GanetApiClassMethodValidator::getInstance()->getClass();
			$method = GanetApiClassMethodValidator::getInstance()->getMethod();
			$result = $class->$method($this->args);
			$result = GanetApiDataConverter::getInstance()->objectToArray($result);
		}
		return $result;
	}
}