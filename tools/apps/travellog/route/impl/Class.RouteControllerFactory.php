<?php
require_once('travellog/route/impl/Class.ControllerType.php');
require_once('travellog/route/model/Class.gaAbstractControllerFactory.php');
class RouteControllerFactory extends gaAbstractControllerFactory{
	static function instance(){
		if( self::$instance == NULL ) self::$instance = new RouteControllerFactory;
		return self::$instance;
	}	
	
	function create( $controller_type ){
		switch( $controller_type ){
			case ControllerType::$TRAVELER:
				require_once("travellog/route/impl/Class.TravelerController.php");
				$obj = new TravelerController;
			break;
				
			case ControllerType::$JOURNALS:
				require_once("travellog/route/impl/Class.JournalController.php");
				$obj = new JournalController;
			break;
			
			case ControllerType::$GROUPS:
				require_once("travellog/route/impl/Class.GroupsController.php");
				$obj = new GroupsController;
			break;
			
			case ControllerType::$SUBGROUPS:
				require_once("travellog/route/impl/Class.SubGroupController.php");
				$obj = new SubGroupController;
			break;
		}
		return $obj; 
	}
}
?>
