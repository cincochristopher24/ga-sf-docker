<?php
require_once("travellog/route/model/Class.gaAbstractAssessorActionFactory.php");
abstract class gaAbstractSubAssessorActionFactory extends gaAbstractAssessorActionFactory{
	
	protected $assessors = array(), $actions = array();
	
	function createAssessors(){}
	
	function createActions(){}
	
	abstract function createAssessor($name);
	
	abstract function createAction($name); 	
}
?>
