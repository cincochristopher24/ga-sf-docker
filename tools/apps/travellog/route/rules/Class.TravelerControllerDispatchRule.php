<?php
require_once("travellog/route/model/Class.gaIRule.php");
require_once("travellog/route/rules/Class.TravelerAssessorActionFactory.php");
class TravelerControllerDispatchRule implements gaIRule{
	function applyRules( $props ){
		
		$factory   = TravelerAssessorActionFactory::getInstance();
		$assessors = $factory->createAssessors(); 
		
		if( !$assessors['IsATraveler']->evaluate( $props ) )
			throw new Exception('Invalid traveler.');
			
		if( $assessors['IsAGroupAdvisor']->evaluate( $props ) )
			throw new Exception('Traveler is a group advisor.');
		
	}
}
?>
