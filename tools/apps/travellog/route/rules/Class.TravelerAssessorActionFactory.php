<?php
require_once("travellog/route/model/Class.gaAbstractAssessorActionFactory.php");
require_once("travellog/route/rules/Class.IsATraveler.php");
require_once("travellog/route/rules/Class.IsAGroupAdvisor.php");
class TravelerAssessorActionFactory extends gaAbstractAssessorActionFactory{
	function getInstance(){
		if( self::$instance == NULL ) self::$instance = new TravelerAssessorActionFactory;
		return self::$instance;  
	}	
	
	function createAssessors(){
		$assessors = array();
		
		$assessors['IsATraveler']     = new IsATraveler;
		$assessors['IsAGroupAdvisor'] = new IsAGroupAdvisor;
		
		return $assessors; 
	}
	
	function createActions(){} 
}
?>
