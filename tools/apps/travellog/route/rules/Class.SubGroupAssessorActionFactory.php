<?php
require_once("travellog/route/model/Class.gaAbstractAssessorActionFactory.php");
require_once("travellog/route/rules/Class.IsSubGroupOwner.php");
require_once("travellog/route/rules/Class.IsValidGroup.php");
class SubGroupAssessorActionFactory extends gaAbstractAssessorActionFactory{
	static function getInstance(){
		if( self::$instance == NULL ) self::$instance = new SubGroupAssessorActionFactory;
		return self::$instance;   
	}
	
	function createAssessors(){
		$assessors = array();
		
		$assessors['IsValidGroup']    = new IsValidGroup;
		$assessors['IsSubGroupOwner'] = new IsSubGroupOwner;
		
		return $assessors;
	}
	
	function createActions(){}
}
?>
