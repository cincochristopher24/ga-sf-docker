<?php
require_once("travellog/route/model/Class.gaIAssessor.php");
class HasAParameterOf implements gaIAssessor{
	
	private $param = NULL;
	
	function evaluate( $props ){
		return ( array_key_exists( $this->param, $props ) )? true: false;
	}
	
	function setParam( $param ){
		$this->param = $param;
	}
}
?>
