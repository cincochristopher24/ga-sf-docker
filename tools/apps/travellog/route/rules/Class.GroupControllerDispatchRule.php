<?php
require_once("travellog/route/model/Class.gaIRule.php");
require_once("travellog/route/rules/Class.IsValidGroup.php");
class GroupControllerDispatchRule implements gaIRule{
	function applyRules( $props ){
		
		$assessor = new IsValidGroup;
				
		if( !$assessor->evaluate( $props ) )
			throw new Exception('Invalid group name.');
	}
}
?> 
