<?php
require_once("travellog/route/model/Class.gaIRule.php");
require_once("travellog/route/rules/Class.JournalAssessorActionFactory.php");
class JournalControllerDispatchRule implements gaIRule{
	
	function applyRules( $props ){
		
		$factory   = JournalAssessorActionFactory::getInstance();
		$assessors = $factory->createAssessors(); 
		
		$assessors['HasAParameterOf']->setParam('groupID');
		
		if( $assessors['HasAParameterOf']->evaluate( $props ) ){
			
			$assessors['HasAParameterOf']->setParam('parentID');
			
			if( $assessors['HasAParameterOf']->evaluate( $props ) ){
				
				if( !$assessors['IsSubGroupOwner']->evaluate( $props ) )
					throw new Exception('Group is not the owner of sub group.');
			
				if( !$assessors['IsSubGroupJournalOwner']->evaluate( $props ) )
					throw new Exception('Sub Group is not the owner of this journal.');
							
			}else{
				
				if( !$assessors['IsGroupJournalOwner']->evaluate( $props ) )
					throw new Exception('Group is not the owner of this journal.');
				
			}  
			
		}else{
		
			if( !$assessors['IsTravelerJournalOwner']->evaluate( $props ) )
				throw new Exception('Traveler is not the owner of this journal.');		
		
		}
		
		
		if( !$assessors['HasAnEntry']->evaluate( $props ) )
				throw new Exception('Invalid entry for this journal.'); 
		
	}
}
?>

