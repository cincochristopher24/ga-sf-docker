<?php
	/**
	 * @(#) Class.TravelerRouteMaps.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 10, 2009
	 */
	 
	require_once("travellog/model/Class.SessionManager.php");
	require_once('travellog/route/routeMaps/Class.AbstractRouteMaps.php');
	
	/**
	 * Route class for travelers
	 */
	class TravelerRouteMaps extends AbstractRouteMaps {
		
		/**
		 * Handles the setting of correct keys to $_GET variable and calling the
		 * correct page(file).
		 * 
		 * @return void
		 */
		function dispatch(){
			require_once('vendor/autoload/GanetAutoloader.php');
				
			GanetAutoloader::getInstance()->register();
			GanetPropelRuntime::getInstance()->initPropel();
			
			if (1 <= count($this->mParsedUri)) {
				require_once("travellog/model/Class.TravelerPeer.php");
				$travTemp = explode("?", $this->mParsedUri[0]);
				$this->mParsedUri[0] = count($travTemp) ? $travTemp[0] : $this->mParsedUri[0];
				
				$traveler = TravelerPeer::getTravelerByUserName(FriendlyUrl::encode($this->mParsedUri[0]));
				
				if (!is_null($traveler)) {
					$context = (isset($this->mParsedUri[1])) ? $this->mParsedUri[1] : "";
					
					switch($context){
						case "journals":
							$this->dispatchJournal($traveler);
						break;
						case "groups":
							$this->dispatchGroups($traveler);
						break;
						default:
							$_GET['travelerID'] = $traveler->getTravelerID();
							$factory = ControllerFactory::getInstance();
							$factory->createController('Profile')->performAction();
							exit;							
					}
				}
			}
		}
		
		/**
		 * Assign the correct controller for viewing groups.
		 * 
		 * @param Traveler $traveler The owner of the groups viewed.
		 * 
		 * @return void
		 */
		function dispatchGroups($traveler){
			if (2 <= count($this->mParsedUri)) {
				$_GET['travelerID'] = $traveler->getTravelerID();
				$_GET['keyword'] = isset($this->mParsedUri[2]) ? $this->mParsedUri[2] : "";
				$_GET['page'] = isset($this->mParsedUri[3]) ? $this->mParsedUri[3] : 1;
				$_SERVER['PHP_SELF'] = "/group.php";
				
				require_once('travellog/factory/Class.ControllerFactory.php');
				$ti = microtime(true);
				$obj_factory = ControllerFactory::getInstance();
				$obj_factory->createController('Groups')->performAction();
				
				$tf = microtime(true);
				if ($_SERVER['REMOTE_ADDR'] == '222.127.68.158' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ){
					echo ($tf - $ti) . ' sec';
				}
				exit;
			}
		}
		
		/**
		 * Assign the correct controller for viewing a journal entry.
		 * 
		 * @param Traveler $traveler The owner of the journal viewed.
		 * 
		 * @return void
		 */
		function dispatchJournal($traveler){
			if (3 <= count($this->mParsedUri)) {
				$session = SessionManager::getInstance();
				// temporary fix. for shared links with ?______ (?------ when converted) array at the end 
				$_GET['entry_title'] = (isset($this->mParsedUri[3])) ? str_replace("?------array","",$this->mParsedUri[3]) : "";
				$_GET['ownerID'] = $traveler->getTravelerID();
				$_GET['travelID'] = (is_numeric($this->mParsedUri[2])) ? $this->mParsedUri[2] : 0;
				$_GET['travelerID'] = $session->get('travelerID');
				$_GET['is_login'] = (0 == $session->get('travelerID')) ? false : true;
				//$_GET['fail'] = (isset($this->mParsedUri[5]) AND "fail" == $this->mParsedUri[5]) ? 1 : 0;
				$_GET['fail'] = (isset($this->mParsedUri[4]) AND "fail" == $this->mParsedUri[4]) ? 1 : 0; 
				
				$_GET['urlAlias'] = (isset($this->mParsedUri['JOURNAL_ENTRY_URL_ALIAS'])) ? $this->mParsedUri['JOURNAL_ENTRY_URL_ALIAS'] : "";
				$_GET['urlAlias'] = str_replace("?______array", "", $_GET['urlAlias']);
				
				$temp = explode("?", $_GET['urlAlias']);
				$_GET['urlAlias'] = count($temp) ? $temp[0] : $_GET['urlAlias'];
				
				require_once("travellog/model/Class.EntryModel.php");
				$entry = new EntryModel();
				$_GET['travellogID'] = $entry->permalink($_GET);
				
				$obj_factory = ControllerFactory::getInstance();
				$obj_factory->createController('Entry')->performAction();
				exit;
			}
		}
	}
	
