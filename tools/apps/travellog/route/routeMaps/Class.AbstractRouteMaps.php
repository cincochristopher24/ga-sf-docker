<?php
	/**
	 * @(#) Class.AbstractRouteMaps.php
	 * 
	 * @package ganet.routes
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 10, 2009
	 */
	
	/**
	 * abstract class for route maps.
	 */
	abstract class AbstractRouteMaps {
		
		/**
		 * Constructs a new route map object.
		 * 
		 * @param array $parsedUri The parsed URI based on the Friendly URL consortium of GoAbroad.Net.
		 */
		function __construct(array $parsedUri){
			$this->mParsedUri = $parsedUri;
		}		
		
		/**
		 * The parsed array from the URI. For example if the URI is equal to
		 * 'groups/GROUP/Basta', this array contains (0 => 'GROUP', 1 => 'Basta').
		 * 
		 * @var array
		 */
		protected $mParsedUri = array();
		
		/**
		 * Sets the parsed array from the URI.
		 * 
		 * @param array $parsedUri The parsed array from the URI.
		 * 
		 * @return void 
		 */
		function setParsedUri(array $parsedUri){
			$this->mParsedUri = $parsedUri;
		}
		
		/**
		 * Returns the parsed array from the URI.
		 * 
		 * @return array
		 */
		function getParsedUri(){
			return $this->mParsedUri;
		}
		
		/**
		 * Handles the setting of correct keys to $_GET variable and calling the
		 * correct page(file).
		 * 
		 * @return void
		 */
		function dispatch() {
			$start = microtime(true);
			require_once('travellog/factory/Class.ControllerFactory.php');
			
			$control = ControllerFactory::getInstance()->createController($this->mParsedUri['ROUTER_CLASS_NAME']);
			$method = (isset($this->mParsedUri[1])) ? uc_implode(explode("-", $this->mParsedUri[1])) : 'index';
			$params = array_slice($this->mParsedUri, 2, (count($this->mParsedUri) - 3));
			call_user_func_array(array(&$control, $method), $params);			
			//echo "time: ".(microtime(true)-$start);
			exit;
		}
	}
	
	/**
	 * Implodes the array and converts to uppercase the first letter of each 
	 * word that will be implode.
	 * 
	 * @param array  $pieces  The pieces to be glued.
	 * @param string $glue    The glue of the imploded string. Defaults to an empty string.
	 * 
	 * @return string The imploded string.
	 */
	function uc_implode($pieces, $glue = ""){
		$glued_string = "";
		
		foreach ($pieces as $piece) {
			$glued_string = $glued_string.ucfirst($piece).$glue;
		}
		
		return preg_replace("/$glue$/", '', $glued_string);
	}
	
