<?php
	/**
	 * @(#) Class.DiscussionRouteMaps.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - May 7, 2009
	 */
	
	require_once('travellog/route/routeMaps/Class.AbstractRouteMaps.php');
	
	/**
	 * Route maps for discussion.
	 */
	class DiscussionRouteMaps extends AbstractRouteMaps {
	
	}
	
