<?php
/*
 * Created on 04 1, 09
 * 
 * Author:
 * Purpose: 
 * 
 */
 
	 require_once	'Cache/ganetCacheProvider.php';
	 require_once 	'Class.Connection.php';
	 require_once	'Class.ConnectionProvider.php';
	 require_once 	'Class.Recordset.php';
	 require_once 	'travellog/vo/Class.ganetValueObject.php';
	 
	 
	 class ganetTravelerRepo {
	 	protected $cache;
		protected $cacheConnected = true;
	
	 	function ganetTravelerRepo(){
			
			$this->cache = ganetCacheProvider::instance()->getCache();
			if ($this->cache == null)
				$this->cacheConnected = false;
			
	 	}
	 	
	 	function retrieve(ganetValueObject $vo){
	 		$travelerID =	$vo->get('id');
			$className	=	$vo->get('CLASS_NAME');
			$traveler = false;
			
			// check if existing in cache
			if ($this->cacheConnected){
				$traveler = $this->cache->get($className . '_' . $travelerID );      // Traveler_xxx is used for name spacing and naming keys for traveler objects
			} 
	
			if (!$traveler){
				// traveler is false so we fetch from db
				$sql = "select * from tblTraveler where travelerID='$travelerID'";
				$rs =  ConnectionProvider::instance()->getRecordset();
				$rs->Execute($sql);
				
				if (0 == $rs->Recordcount()){
					throw new Exception('Traveler id:' . $travelerID . ' not found.' );
				} 	 
				
				$data = array();
				$data['travelerID'] 	= $rs->Result(0, 'travelerID');
				$data['isadvisor']		= $rs->Result(0, 'isadvisor');
				$data['active'] 		= $rs->Result(0, 'active');
				$data['isSuspended'] 	= $rs->Result(0, 'isSuspended');
				$data['currlocationID'] = $rs->Result(0, 'currlocationID');
				$data['sendableID'] 	= $rs->Result(0,'sendableID');
				$data['username'] 		= $rs->Result(0, 'username');
				$data['password'] 		= $rs->Result(0, 'password');
				$data['showsteps'] 		= $rs->Result(0, 'showsteps');
				$data['deactivated']	= $rs->Result(0, 'deactivated');
				
				$traveler = new Traveler(0,$data);
				
				$tp = new TravelerProfile();
				
				$tp->setIsAdvisor($rs->Result(0, 'isadvisor'));
				$tp->setUserName(stripslashes($rs->Result(0,"username"))); 		 
				$tp->setFirstName(stripslashes($rs->Result(0,"firstname"))); 
	 			$tp->setLastName(stripslashes($rs->Result(0,"lastname"))); 
	 			$tp->setEmail($rs->Result(0,"email")); 
	 			$tp->setGender($rs->Result(0,"gender"));
	 			
	 			$tp->setHTLocationID($rs->Result(0,"htlocationID"));
	 			$tp->setCurrLocationID($rs->Result(0,"currlocationID")); 
	 			$tp->setAddress1(stripslashes($rs->Result(0,"address1"))); 
	 			$tp->setAddress2(stripslashes($rs->Result(0,"address2")));
	 			$tp->setPhone(stripslashes($rs->Result(0,"phone"))); 
	 			
	 			$tp->setTravelertypeID($rs->Result(0,"travelertypeID")); 
	 			
	 			$tp->setBirthDay($rs->Result(0,"birthdate")); 
	 			$tp->setInterests(stripslashes($rs->Result(0,"interests"))); 
	 			$tp->setShortBio(stripslashes($rs->Result(0,"shortbio"))); 
	 			$tp->setCulture(stripslashes($rs->Result(0,"iculture"))); 
	 			$tp->setDateJoined($rs->Result(0,"dateregistered"));
	 			$tp->setLastLogin($rs->Result(0,"latestlogin"));
	 			//$tp->views 			= $rs->Result(0,"views");  
	 			$tp->setIntrnltravel($rs->Result(0,"intrnltravel"));
	 			$tp->setTripnexttwoyears($rs->Result(0,"istripnexttwoyears"));
	 			$tp->setIncome($rs->Result(0,"income"));	
	 			$tp->setOtherprefertravel($rs->Result(0,"otherprefertravel"));	
	 			$tp->setIspromosend($rs->Result(0,"ispromosend"));
	 			$tp->setSignature($rs->Result(0,"signature"));
	 			$tp->setIssuspended($rs->Result(0,"Issuspended"));
				$tp->setShowPersonals($rs->Result(0,"showpersonals"));
	 			$tp->setTravelerID($travelerID);
	 				
	 			$traveler->setTravelerProfile($tp);
	 				
				// we put this traveler object in cache
				if ($this->cache != null){
					$this->cache->set($className . '_' . $travelerID, $traveler,array('EXPIRE'=>86400));
					//echo '<pre>setting traveler to cache'; print_r($traveler);
				} 
			}
	
			return $traveler;
	}
	 	
	 	function createNew(){
	 		
	 	}
	 	
	 	function getList(ganetValueObject $vo){
	 		/* main contents of $vo CONTEXT,FILTER which are assoc arrays and COMMAND which is a string */
	 		$context 	=	$vo->IsKeyExists('CONTEXT')?$vo->get('CONTEXT'):null;
			$filter 	=	$vo->IsKeyExists('FILTER')?$vo->get('FILTER'):null; 		
	 		$command 	=	$vo->IsKeyExists('COMMAND')?$vo->get('COMMAND'):'';
			
			$travelers 	=	array();
			
			switch ($command) {
				case 'GROUP_MEMBERS':
					$travelers	=	$this->getGroupMembers($context,$filter);
					break;
				case 'GROUP_MEMBERS_WITH_PHOTO_PRIORITIZED':
					$travelers  =	$this->getGroupMembersWithPhotoPrioritized($context,$filter);
					break;
				case 'TRAVELER_FRIENDS':
					$travelers	=	$this->getTravelerFriends($context,$filter);
					break;	
				case 'GROUP_MEMBERS_WITH_COUNT':
					// transferred from Group->getGroupMembers function
					$travelers	=	$this->getGroupMembersWithCount($context,$filter);
					break;			
				case 'TRAVELER_FRIENDS_PRIORITIZE_WITH_PHOTO':
					$travelers = $this->getTravelerFriendsWithPhotoPrioritized($context,$filter);
					break;
				default:
					break;
			}		 		
 			return $travelers;	
	 	}
	 	
	 	/*********************
	 	 * action functions
	 	 */
	 	 
	 	 protected function getTravelerFriends($context,$filter){
	 	 	$filtercriteria = null;
	 	 	$travelerID	=	$context->get('TRAVELER_ID');
			$hashKey	=	'';
			$found	=	false;
			
	 	 	if ($filter->IsKeyExists('FILTER_CRITERIA')){
	 	 		$filtercriteria	=	$filter->get('FILTER_CRITERIA');
	 	 		if ($filtercriteria){
	 	 			$hashKey	=	$filtercriteria->getHash();
	 	 		}
	 	 	}
	
			if ($this->cache != null){
	 	 		$travelerIDs	=	$this->cache->get('Traveler_Friends_' . $travelerID . '_' . $hashKey );
	 	 		if ($travelerIDs){
					$friends = array();
					$found = true;
					foreach($travelerIDs as $key){
						if($trav = $this->cache->get('Traveler_'.$key)){
							$friends[] = $trav;
						}else{
							$vo = new ganetValueObject();
		 	 				$vo->set('id',$key);
		 	 				$vo->set('CLASS_NAME','Traveler');
		 	 				$friends[] = $this->retrieve($vo);
						}
					}
				// $friends = $this->cache->get($travelerIDs);
				// if (count($friends) == count($travelerIDs)){
				// 	$found	=	true;
				// 
				// 	// recreate $friends array to use its id as its key
				// 	// NOTE : Please research if friend id is still used......
				// 	/*$ctr = 0;
				// 	foreach($friends as $friend){
				// 	$friends[]
				// 	}*/
				// 
				// 	}
				}	
	 	 	}
	 	 	
	 	 	if (!$found){	
		 	 	if ($filtercriteria){				
					$hashKey	=	$filtercriteria->getHash();
		 	 		$myconditions = $filtercriteria-> getConditions();
		 			$attname = $myconditions[0]-> getAttributeName();
					$operation = $myconditions[0]-> getOperation();
	
		 			switch ($operation){
		 				case FilterOp::$ORDER_BY :
		 					$condition = " ORDER BY " . $attname ;
		 					break;
		 				case FilterOp::$ORDER_BY_DESC :
		 					$condition = " ORDER BY " . $attname . " DESC " ;
		 					break;
		 			}
				}			
				else
					$condition = " ORDER BY username";
			 	
		 	  	// get startrow and rows per page
	            if ($filtercriteria && $filtercriteria->getStartRow() >= 0 && $filtercriteria->getRowsPerPage() > 0 ){
	                 $offset   = $filtercriteria->getStartRow();
	                 $rows     = $filtercriteria->getRowsPerPage();
	                 $limitstr = " LIMIT " . $offset . " , " . $rows;
	             } else {
	             	$limitstr = "";
	             }	
					
				/*$sql = "SELECT SQL_CALC_FOUND_ROWS tblFriend.friendID, tblTraveler.* " .
						"FROM tblFriend, tblTraveler " .
						"WHERE tblTraveler.travelerID = tblFriend.friendID " .
						"AND tblTraveler.active  = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblFriend.travelerID = $travelerID " .
						"AND tblFriend.friendID NOT IN " .
							"(select distinct travelerID from tblGrouptoAdvisor) " .
						$condition . $limitstr;*/
				$sql = "SELECT SQL_CALC_FOUND_ROWS tblFriend.friendID, tblTraveler.* " .
						"FROM tblFriend, tblTraveler " .
						"WHERE tblTraveler.travelerID != $travelerID ".
						"AND tblTraveler.travelerID = tblFriend.friendID " .
						"AND tblFriend.travelerID = $travelerID " .
						"AND tblTraveler.active = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 " .
						$condition . $limitstr;
				$rs	=	ConnectionProvider::instance()->getRecordset();
				$rs2 =	ConnectionProvider::instance()->getRecordset2();
				$rs->Execute($sql);
	
				$sql2 = "select FOUND_ROWS() as totalRecords";
				$rs2->Execute($sql2);
	
				$friends = array();
				
				// set no. of friends for traveler as done in previous code  NOTE: UNCOMMENT THE CODE BELOW IF THE FRIEND COUNT BECOMES ZERO
				//$traveler	=	new Traveler($travelerID);
				//$traveler->setNumberOfFriends($rs2->Result(0, "totalRecords"));				
	
				//if ($this->cache != null){
				//	$this->cache->set($className . '_' . $travelerID,$traveler,array('EXPIRE'=>86400));				
				//}
				//-
	
				if (0 < $rs2->Result(0, "totalRecords")){				
					$travelerIDs = array();
					while ($friend = mysql_fetch_array($rs->Resultset())){
							$obj_traveler = new Traveler;
							$obj_traveler->setTravelerData($friend);
							$friends[$friend['friendID']] = $obj_traveler;
							// $travelerIDs[]	=	'Traveler_' . $friend['travelerID'];
							$travelerIDs[]	=	$friend['travelerID'];
							
							// write traveler to cache
							// if ($this->cache != null){
							// 	$this->cache->set('Traveler_' . $friend['travelerID'],$obj_traveler,array('EXPIRE'=>3600));
							// }
					}
				}
				
				/*if ($this->cache != null){
					$this->cache->set('Traveler_Friends_' . $travelerID . '_' . $hashKey,$travelerIDs,array('EXPIRE'=>3600));
					
					// store also the hashKeys in cache as array to be used for invalidation
                	if ($hashes = $this->cache->get('Traveler_Friends_hashes_' . $travelerID )) {
                		if (!in_array('Traveler_Friends_' . $travelerID . '_' . $hashKey,$hashes)){
                			$hashes[]	=	'Traveler_Friends_' . $travelerID . '_' . $hashKey;
                			$this->cache->set('Traveler_Friends_hashes_' . $travelerID,$hashes,array('EXPIRE' => 3600));	
                		}
                		
                	} else {
                		$this->cache->set('Traveler_Friends_hashes_' . $travelerID,array('Traveler_Friends_' . $travelerID . '_' . $hashKey),array('EXPIRE'=>3600));
                	}
                	//---
				}*/
	 	 	}
	 	 		
			return $friends;
	 	 	
	 	 }
	 	
	 	 protected function getGroupMembers($context,$filter){
	 	 	
	 	 	$groupID	=	$context->get('GROUP_ID');
	 	 	$parentID	=	$context->get('PARENT_ID');
	 	 	$count		=	$context->get('COUNT');
	 	 	$groupType	=	$context->get('GROUP_TYPE');
	 	 	
	 	 	try {
	 	 		$filtercriteria	=	$filter->get('FILTER_CRITERIA');
	 	 	} catch (Exception $e){
	 	 		echo $e->getMessage(); exit;
	 	 	}
	 	 	
	 	 	
	 	 	$hashKey = $filtercriteria->getHash();  
	 	 	
	 	 	if ($count) {
	 	 		$key	=	'Group_to_Traveler_Count_' . $groupID . '_' . $hashKey;	
	 	 	} else {
	 	 		$key	=	'Group_to_Traveler_' . $groupID . '_' . $hashKey;
	 	 	}
	 	 	//echo 'key used:' . $key . '<br/>';
	 	 	if ($this->cache != null && ( $tIDs = $this->cache->get($key)  )){
	 	 		//echo 'Group_to_Traveler_' . $groupID . '_' . $hashKey . '<br/>';
				if (!$count) {
	 	 			$members = array();
		 	 		foreach($tIDs as $tID){
		 	 			if ($trav	=	$this->cache->get('Traveler_' . $tID))
		 	 				$members[]	=	$trav;
		 	 			else {	 	 				
		 	 				$vo = new ganetValueObject();
		 	 				$vo->set('id',$tID);
		 	 				$vo->set('CLASS_NAME','Traveler');
		 	 				$members[]	=	$this->retrieve($vo);
		 	 			}	
		 	 		}
		 	 		
	 	 			return $members;
				} else {
					$recordCount	=	$tIDs;				// sorry for the mixed use of a variable
					return $recordCount;
				}					
	 	 	} else {
               
	 	 	//----- access through DB ----//
            	$limitstr  = '';
                $filterstr = '';
                $orderstr  = '';
                
                $conditions 	=	$filtercriteria->getConditions();
                // get startrow and rows per page
                if ($filtercriteria->getStartRow() >= 0 && $filtercriteria->getRowsPerPage() > 0 ){
                    $offset   = $filtercriteria->getStartRow();
                    $rows     = $filtercriteria->getRowsPerPage();
                    $limitstr = " LIMIT " . $offset . " , " . $rows;
                }

	 	 		// get ORDER BY condition
             	if ($filtercriteria->getOrderBy() != '' ){
	        			$attname   = $filtercriteria->getOrderBy();                    			
	        			$orderstr = " ORDER BY " . $attname;
             	}
                
                // get filter conditions 
               foreach ($conditions as $condition){
               	
                	$condition = $filtercriteria->getConditions();	
        			$attname   = $condition[0]->getAttributeName();                    			
        			$value 	   = $condition[0]->getValue();
        			
        			switch($condition[0]->getOperation()){
        				
        				case FilterOp::$EQUAL:
        					$operator	= " = ";
        					break;
        				case FilterOp::$NOT_EQUAL:
        					$operator = " <> ";
        					break;
        				default:
        					$operator = " = ";
        					break;
        			}
        			
        			$filterstr .= " AND " . $attname . $operator . $value;
                	
                }
                
               //$filterstr .= " AND tblTraveler.travelerID not in (select distinct travelerID from tblGrouptoAdvisor) " ;
               $groupByStr = " GROUP BY tblTraveler.travelerID ";	
               
				$sql = "SELECT tblTraveler.* " .
						"FROM tblGrouptoTraveler, tblTraveler " .
						"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
						"AND tblTraveler.active  = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 ".
						"AND tblGrouptoTraveler.groupID = " . $groupID; 

	 	 		if (Group::ADMIN == $groupType AND 0 == $parentID) {
					$sql .= " AND tblTraveler.travelerID NOT IN " .
						"	 ( " .
						"		SELECT tblGrouptoFacilitator.travelerID FROM tblGrouptoFacilitator, tblGroup " .
						"		WHERE status = 1 AND type = 1 " .
						"		AND " .
						"		(" .
						"		tblGroup.parentID = " . $groupID ." || tblGroup.groupID = " .$groupID.
						"		)" .
						"   AND tblGrouptoFacilitator.groupID = tblGroup.groupID ".
						"  ) ";
				}
				else if (Group::ADMIN == $groupType) {
					$sql .= " AND tblTraveler.travelerID NOT IN " .
						"	 ( " .
						"		SELECT travelerID FROM tblGrouptoFacilitator " .
						"		WHERE status = 1 AND type = 1 " .
						"		AND groupID = ". $groupID . "" .
						"  ) ";					
				}		
						
				$sql .= $filterstr . $groupByStr . $orderstr . $limitstr;			
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
				$this->mRs->Execute($sql);

				if ($count){
					$recordCount	=	$this->mRs->Recordcount(); 
					if ($this->cache != null){
						$this->cache->set($key,$recordCount,array('EXPIRE'=>86400));
						
						// store also the hashKeys in cache as array to be used for invalidation
	                	if ($hashes = $this->cache->get('Group_to_Traveler_Count_hashes_' . $groupID )) {
	                		if (!in_array($key,$hashes)){
	                			$hashes[]	=	$key;
	                			$this->cache->set('Group_to_Traveler_Count_hashes_' . $groupID,$hashes,array('EXPIRE'=>86400));
	                		}
	                	} else {
	                		$this->cache->set('Group_to_Traveler_Count_hashes_' . $groupID,array($key),array('EXPIRE'=>86400));
	                	}
					}
					
					return $recordCount ;
				}
				else {	
	               		$tIDs	=	array();
						$this->mMembers = array();
						while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
			                  try {
									$travelerData = array();
									$travelerData["travelerID"] = $recordset['travelerID'];
									$travelerData["isadvisor"] = $recordset['isadvisor'];
									$travelerData["active"] = $recordset['active'];
									$travelerData["isSuspended"] = $recordset['isSuspended'];
						 			$travelerData["currlocationID"] = $recordset['currlocationID'];
						 			$travelerData["sendableID"] = $recordset['sendableID'];
						 			$travelerData["username"] = $recordset['username'];
						 			$travelerData["password"] = $recordset['password'];
						 			$travelerData["showsteps"] = $recordset['showsteps'];
									$travelerData["deactivated"] = $recordset['deactivated'];
															
									$traveler = new Traveler;
									$traveler-> setTravelerData($travelerData);
									
									// set profile data in array and set to profile; minimize DB query
									$profileData = array();								
									$profileData["travelerID"] = $recordset['travelerID'];
									$profileData["isadvisor"] = $recordset['isadvisor'];
									$profileData["username"] = $recordset['username'];
									$profileData["firstname"] = $recordset['firstname'];
						 			$profileData["lastname"] = $recordset['lastname'];
						 			$profileData["email"] = $recordset['email'];
						 			$profileData["gender"] = $recordset['gender'];
						 			$profileData["htlocationID"] = $recordset['htlocationID'];
						 			$profileData["currlocationID"] = $recordset['currlocationID'];
						 			$profileData["address1"] = $recordset['address1'];
						 			$profileData["address2"] = $recordset['address2'];
						 			$profileData["phone"] = $recordset['phone'];
						 			$profileData["travelertypeID"] = $recordset['travelertypeID'];
						 			$profileData["birthdate"] = $recordset['birthdate'];
						 			$profileData["interests"] = $recordset['interests'];
						 			$profileData["shortbio"] = $recordset['shortbio'];
						 			$profileData["iculture"] = $recordset['iculture'];
						 			$profileData["dateregistered"] = $recordset['dateregistered'];
						 			$profileData["latestlogin"] = $recordset['latestlogin'];
						 			$profileData["views"] =  $recordset['views'];
						 			$profileData["intrnltravel"] = $recordset['intrnltravel'];
						 			$profileData["istripnexttwoyears"] = $recordset['istripnexttwoyears'];
						 			$profileData["income"] =	 $recordset['income'];
						 			$profileData["otherprefertravel"] =	 $recordset['otherprefertravel'];
						 			$profileData["ispromosend"] = $recordset['ispromosend'];
						 			$profileData["signature"] = $recordset['signature'];
				
									$profile = new TravelerProfile;
									$profile->setProfileData($profileData);

									$traveler->setTravelerProfile($profile);								
			                   	}
			                   	catch (Exception $e) {
			                    	throw $e;
			                   	}

								$this->mMembers[] = $traveler;
								$tIDs[]	=	$recordset['travelerID'];
		                }
		                
		                if ($this->cache != null){
		                	$this->cache->set($key,$tIDs,array('EXPIRE'=>86400));
				                	
		                	// store also the hashKeys in cache as array to be used for invalidation
		                	if ($hashes = $this->cache->get('Group_to_Traveler_hashes_' . $groupID )) {
		                		if (!in_array($key,$hashes)){
		                			$hashes[]	=	$key;
		                			$this->cache->set('Group_to_Traveler_hashes_' . $groupID,$hashes,array('EXPIRE'=>86400));
		                		}
		                	} else {
		                		$this->cache->set('Group_to_Traveler_hashes_' . $groupID,array($key),array('EXPIRE'=>86400));
		                	}
		                	//---
							$this->cache->set('Group_Members_List_Hashkey_'.$groupID,$key,array('EXPIRE'=>86400));
		                }
		        
		            	return $this->mMembers;
	             }
				
	 	 }
	 	
	 }
	
	protected function getGroupMembersWithPhotoPrioritized($context,$filter){
 	 	$groupID	=	$context->get('GROUP_ID');
 	 	$parentID	=	$context->get('PARENT_ID');
 	 	$count		=	$context->get('COUNT');
 	 	$groupType	=	$context->get('GROUP_TYPE');
 	 	
 	 	try {
 	 		$filtercriteria	=	$filter->get('FILTER_CRITERIA');
 	 	} catch (Exception $e){
 	 		echo $e->getMessage(); exit;
 	 	}

 	 	$hashKey = $filtercriteria->getHash();  
 	 	
 	 	if ($count) {
 	 		$key	=	'Group_to_Traveler_Count_Prioritized' . $groupID . '_' . $hashKey;	
 	 	} else {
 	 		$key	=	'Group_to_Traveler_Prioritized' . $groupID . '_' . $hashKey;
 	 	}
 	 	//echo 'key used:' . $key . '<br/>';
 	 	if ($this->cache != null && ( $tIDs = $this->cache->get($key)  )){
 	 		//echo 'Group_to_Traveler_' . $groupID . '_' . $hashKey . '<br/>';
			if (!$count) {
 	 			$members = array();
	 	 		foreach($tIDs as $tID){
	 	 			if ($trav	=	$this->cache->get('Traveler_' . $tID))
	 	 				$members[]	=	$trav;
	 	 			else {	 	 				
	 	 				$vo = new ganetValueObject();
	 	 				$vo->set('id',$tID);
	 	 				$vo->set('CLASS_NAME','Traveler');
	 	 				$members[]	=	$this->retrieve($vo);
	 	 			}	
	 	 		}
	 	 		
 	 			return $members;
			} else {
				$recordCount	=	$tIDs;				// sorry for the mixed use of a variable
				return $recordCount;
			}					
 	 	} else { 
	 	 	//----- access through DB ----//
			$limitstr  = '';
			$filterstr = '';
			$orderstr  = '';

			$conditions 	=	$filtercriteria->getConditions();
			// get startrow and rows per page
			if ($filtercriteria->getStartRow() >= 0 && $filtercriteria->getRowsPerPage() > 0 ){
			    $offset   = $filtercriteria->getStartRow();
			    $rows     = $filtercriteria->getRowsPerPage();
			    $limitstr = " LIMIT " . $offset . " , " . $rows;
			}

			// get ORDER BY condition
			if ($filtercriteria->getOrderBy() != '' ){
				$attname   = $filtercriteria->getOrderBy();                    			
				$orderstr = " ORDER BY photoID ASC, " . $attname;
			}else{
				$orderstr = " ORDER BY photoID ASC ";
			}              
			// get filter conditions 
			foreach ($conditions as $condition){

				$condition = $filtercriteria->getConditions();	
				$attname   = $condition[0]->getAttributeName();                    			
				$value 	   = $condition[0]->getValue();

				switch($condition[0]->getOperation()){
					case FilterOp::$EQUAL:
						$operator	= " = ";
						break;
					case FilterOp::$NOT_EQUAL:
						$operator = " <> ";
						break;
					default:
						$operator = " = ";
						break;
				}

				$filterstr .= " AND " . $attname . $operator . $value;

			}
              
			$groupByStr = " GROUP BY tblTraveler.travelerID ";	
             
			$sql = "SELECT tblTraveler.*, tblTravelertoPhoto.photoID IS NULL AS photoID " .
					"FROM tblGrouptoTraveler, tblTraveler " .
					"LEFT JOIN tblTravelertoPhoto ".
					"ON tblTravelertoPhoto.travelerID = tblTraveler.travelerID ".
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblTraveler.active  = 1 " .
					"AND tblTraveler.isSuspended = 0 " .
					"AND tblTraveler.deactivated = 0 " .
					"AND tblGrouptoTraveler.groupID = " . $groupID; 

			if (Group::ADMIN == $groupType AND 0 == $parentID) {
				$sql .= " AND tblTraveler.travelerID NOT IN " .
						"	 ( " .
						"		SELECT tblGrouptoFacilitator.travelerID FROM tblGrouptoFacilitator, tblGroup " .
						"		WHERE status = 1 AND type = 1 " .
						"		AND " .
						"		(" .
						"		tblGroup.parentID = " . $groupID ." || tblGroup.groupID = " .$groupID.
						"		)" .
						"   AND tblGrouptoFacilitator.groupID = tblGroup.groupID ".
						"  ) ";
			}
			else if (Group::ADMIN == $groupType) {
				$sql .= " AND tblTraveler.travelerID NOT IN " .
						"	 ( " .
						"		SELECT travelerID FROM tblGrouptoFacilitator " .
						"		WHERE status = 1 AND type = 1 " .
						"		AND groupID = ". $groupID . "" .
						"  ) ";					
			}		

			$sql .= $filterstr . $groupByStr . $orderstr . $limitstr;
			$this->mRs	=	ConnectionProvider::instance()->getRecordset();
			$this->mRs->Execute($sql);

			if ($count){
				$recordCount	=	$this->mRs->Recordcount(); 			
				return $recordCount ;
				
				$recordCount	=	$this->mRs->Recordcount(); 
				if ($this->cache != null){
					$this->cache->set($key,$recordCount,array('EXPIRE'=>86400));
					
					// store also the hashKeys in cache as array to be used for invalidation
                	if ($hashes = $this->cache->get('Group_to_Traveler_Count_Prioritized_hashes_' . $groupID )) {
                		if (!in_array($key,$hashes)){
                			$hashes[]	=	$key;
                			$this->cache->set('Group_to_Traveler_Count_Prioritized_hashes_' . $groupID,$hashes,array('EXPIRE'=>86400));
                		}
                	} else {
                		$this->cache->set('Group_to_Traveler_Count_Prioritized_hashes_' . $groupID,array($key),array('EXPIRE'=>86400));
                	}
				}
				return $recordCount ;
			} else {	
				$tIDs	=	array();
				$this->mMembers = array();
				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
					try {
						$travelerData = array();
						$travelerData["travelerID"] = $recordset['travelerID'];
						$travelerData["isadvisor"] = $recordset['isadvisor'];
						$travelerData["active"] = $recordset['active'];
						$travelerData["isSuspended"] = $recordset['isSuspended'];
						$travelerData["currlocationID"] = $recordset['currlocationID'];
						$travelerData["sendableID"] = $recordset['sendableID'];
						$travelerData["username"] = $recordset['username'];
						$travelerData["password"] = $recordset['password'];
						$travelerData["showsteps"] = $recordset['showsteps'];
						$travelerData["deactivated"] = $recordset['deactivated'];

						$traveler = new Traveler;
						$traveler-> setTravelerData($travelerData);

						// set profile data in array and set to profile; minimize DB query
						$profileData = array();								
						$profileData["travelerID"] = $recordset['travelerID'];
						$profileData["isadvisor"] = $recordset['isadvisor'];
						$profileData["username"] = $recordset['username'];
						$profileData["firstname"] = $recordset['firstname'];
						$profileData["lastname"] = $recordset['lastname'];
						$profileData["email"] = $recordset['email'];
						$profileData["gender"] = $recordset['gender'];
						$profileData["htlocationID"] = $recordset['htlocationID'];
						$profileData["currlocationID"] = $recordset['currlocationID'];
						$profileData["address1"] = $recordset['address1'];
						$profileData["address2"] = $recordset['address2'];
						$profileData["phone"] = $recordset['phone'];
						$profileData["travelertypeID"] = $recordset['travelertypeID'];
						$profileData["birthdate"] = $recordset['birthdate'];
						$profileData["interests"] = $recordset['interests'];
						$profileData["shortbio"] = $recordset['shortbio'];
						$profileData["iculture"] = $recordset['iculture'];
						$profileData["dateregistered"] = $recordset['dateregistered'];
						$profileData["latestlogin"] = $recordset['latestlogin'];
						$profileData["views"] =  $recordset['views'];
						$profileData["intrnltravel"] = $recordset['intrnltravel'];
						$profileData["istripnexttwoyears"] = $recordset['istripnexttwoyears'];
						$profileData["income"] =	 $recordset['income'];
						$profileData["otherprefertravel"] =	 $recordset['otherprefertravel'];
						$profileData["ispromosend"] = $recordset['ispromosend'];
						$profileData["signature"] = $recordset['signature'];

						$profile = new TravelerProfile;
						$profile->setProfileData($profileData);

						$traveler->setTravelerProfile($profile);								
					}catch (Exception $e) {
					}

					$this->mMembers[] = $traveler;
					$tIDs[]	=	$recordset['travelerID'];
				}
				
				if ($this->cache != null){
                	$this->cache->set($key,$tIDs,array('EXPIRE'=>86400));
		                	
                	// store also the hashKeys in cache as array to be used for invalidation
                	if ($hashes = $this->cache->get('Group_to_Traveler_Prioritized_hashes_' . $groupID )) {
                		if (!in_array($key,$hashes)){
                			$hashes[]	=	$key;
                			$this->cache->set('Group_to_Traveler_Prioritized_hashes_' . $groupID,$hashes,array('EXPIRE'=>86400));
                		}
                	} else {
                		$this->cache->set('Group_to_Traveler_Prioritized_hashes_' . $groupID,array($key),array('EXPIRE'=>86400));
                	}
                	//---
					$this->cache->set('Group_Members_List_Hashkey_'.$groupID,$key,array('EXPIRE'=>86400));
                }

				return $this->mMembers;
			}
		}	
	}
 	
	 protected function getGroupMembersWithCount($context,$filter){
	 	
	 	$groupID	=	$context->get('GROUP_ID');
	 	$unassigned	=	$context->get('UNASSIGNED');
	 	
	 	try {
	 		$filtercriteria	=	$filter->get('FILTER_CRITERIA');
	 	} catch (Exception $e){
	 		echo $e->getMessage(); exit;
	 	}
	 	
	 //----- access through DB ----//
         $limitstr  = '';
         $filterstr = '';
         $orderstr  = '';
                
         $conditions 	=	$filtercriteria->getConditions();
         // get startrow and rows per page
         if ($filtercriteria->getStartRow() >= 0 && $filtercriteria->getRowsPerPage() > 0 ){
               $offset   = $filtercriteria->getStartRow();
               $rows     = $filtercriteria->getRowsPerPage();
               $limitstr = " LIMIT " . $offset . " , " . $rows;
         }

	 	 // get ORDER BY condition
         if ($filtercriteria->getOrderBy() != '' ){
	     		$attname   = $filtercriteria->getOrderBy();                    			
	     		$orderstr = " ORDER BY " . $attname;
         }
	 	
		 foreach ($conditions as $condition){
               	
            	$condition = $filtercriteria->getConditions();	
    			$attname   = $condition->getAttributeName();                    			
    			$value 	   = $condition->getValue();
    			
    			switch($condition->getOperation()){
    				
    				case FilterOp::$EQUAL:
    					$operator	= " = ";
    					break;
    				case FilterOp::$NOT_EQUAL:
    					$operator = " <> ";
    					break;
					case FilterOp::$LIKE:
						$operator = " LIKE ";
						break;
					case FilterOp::$GREATER_THAN_OR_EQUAL:
						$operator = " >= ";
						break;
					case FilterOp::$LESS_THAN_OR_EQUAL:
						$operator = " <= ";
						break;
    				default:
    					$operator = " = ";
    					break;
    			}
    			
    			$filterstr .= " AND " . $attname . $operator . $value;
            }

           $mRs	= 	ConnectionProvider::instance()->getRecordset(); 
            
           //$filterstr .= " AND tblTraveler.travelerID not in (select distinct travelerID from tblGrouptoAdvisor) " ;
           $groupByStr = " GROUP BY tblTraveler.travelerID ";

			if( $unassigned ){
				/*$tempSQL = "CREATE TEMPORARY TABLE assigned(
				 				`travelerID` INT( 11 ) NOT NULL ,
								KEY ( travelerID )
							) SELECT DISTINCT t.travelerID
								FROM tblTraveler AS t
								WHERE t.isSuspended =0
									AND t.travelerID IN (
										SELECT tgt.travelerID
										FROM tblGrouptoTraveler AS tgt
										WHERE tgt.groupID IN (
											SELECT tg.groupID
											FROM tblGroup AS tg
											WHERE tg.parentID =" . $groupID.")
										)
									AND t.travelerID NOT IN (
										SELECT DISTINCT travelerID
										FROM tblGrouptoAdvisor);";*/
				$tempSQL = "CREATE TEMPORARY TABLE assigned(
				 				`travelerID` INT( 11 ) NOT NULL ,
								KEY ( travelerID )
							) SELECT DISTINCT t.travelerID
								FROM tblTraveler AS t
								WHERE t.isSuspended = 0
									AND t.deactivated = 0
									AND t.travelerID IN (
										SELECT tgt.travelerID
										FROM tblGrouptoTraveler AS tgt
										WHERE tgt.groupID IN (
											SELECT tg.groupID
											FROM tblGroup AS tg
											WHERE tg.parentID =" . $groupID.")
										)";
				$mRs->Execute($tempSQL);
				
				$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.*
						FROM tblTraveler
						WHERE tblTraveler.travelerID NOT IN (
							SELECT travelerID
							FROM assigned
							)
							AND tblTraveler.travelerID IN (
								SELECT t3.travelerID
								FROM tblGrouptoTraveler AS t3
								WHERE t3.groupID =". $groupID .")".
					$filterstr . $groupByStr . $orderstr . $limitstr.";";
				//echo $sql."<br /><br />";
			}else{
				if( $includePending ){
					$sql = "(SELECT SQL_CALC_FOUND_ROWS tblTraveler.*, 0 AS isPending
							FROM tblGrouptoTraveler, tblTraveler 
							WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID 
								AND tblTraveler.active  = 1 
								AND tblTraveler.isSuspended = 0 
								AND tblTraveler.deactivated = 0 
								AND tblGrouptoTraveler.groupID =  $groupID
							$filterstr $groupByStr)
							UNION ALL
							(SELECT tblTraveler.*, 1 AS isPending
							FROM tblTraveler, tblInviteList
							WHERE tblInviteList.groupID = $groupID
								AND tblInviteList.status != 1
								AND tblTraveler.travelerID = tblInviteList.travelerID
								AND tblTraveler.active = 1
								AND tblTraveler.isSuspended = 0
								AND tblTraveler.deactivated = 0
							$filterstr $groupByStr)
							$orderstr $limitstr";
				}else{
					$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* " .
						"FROM tblGrouptoTraveler, tblTraveler " .
						"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
						"AND tblTraveler.active  = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 " .
						"AND tblGrouptoTraveler.groupID = " . $groupID .
						$filterstr . $groupByStr . $orderstr . $limitstr;
				}
			}
      		
			$mRs->Execute($sql);
            ///////////////////////////// STOPPPPEDDDDDDDDD HEREEEEEEEEEEE /////////////
			if ($mRs->Recordcount() != $this->mMemberCount){
	            $mMembers = array();
				while ($recordset = mysql_fetch_array($mRs->Resultset())) {
	                  try {
							$travelerData = array();
							$travelerData["travelerID"] = $recordset['travelerID'];
							$travelerData["isadvisor"] = $recordset['isadvisor'];
							$travelerData["active"] = $recordset['active'];
							$travelerData["isSuspended"] = $recordset['isSuspended'];
				 			$travelerData["currlocationID"] = $recordset['currlocationID'];
				 			$travelerData["sendableID"] = $recordset['sendableID'];
				 			$travelerData["username"] = $recordset['username'];
				 			$travelerData["password"] = $recordset['password'];
				 			$travelerData["showsteps"] = $recordset['showsteps'];
							$travelerData["deactivated"] = $recordset['deactivated'];
												
							$traveler = new Traveler;
							$traveler-> setTravelerData($travelerData);

							// set profile data in array and set to profile; minimize DB query
							$profileData = array();								
							$profileData["travelerID"] = $recordset['travelerID'];
							$profileData["isadvisor"] = $recordset['isadvisor'];
							$profileData["username"] = $recordset['username'];
							$profileData["firstname"] = $recordset['firstname'];
				 			$profileData["lastname"] = $recordset['lastname'];
				 			$profileData["email"] = $recordset['email'];
				 			$profileData["gender"] = $recordset['gender'];
				 			$profileData["htlocationID"] = $recordset['htlocationID'];
				 			$profileData["currlocationID"] = $recordset['currlocationID'];
				 			$profileData["address1"] = $recordset['address1'];
				 			$profileData["address2"] = $recordset['address2'];
				 			$profileData["phone"] = $recordset['phone'];
				 			$profileData["travelertypeID"] = $recordset['travelertypeID'];
				 			$profileData["birthdate"] = $recordset['birthdate'];
				 			$profileData["interests"] = $recordset['interests'];
				 			$profileData["shortbio"] = $recordset['shortbio'];
				 			$profileData["iculture"] = $recordset['iculture'];
				 			$profileData["dateregistered"] = $recordset['dateregistered'];
				 			$profileData["latestlogin"] = $recordset['latestlogin'];
				 			$profileData["views"] =  $recordset['views'];
				 			$profileData["intrnltravel"] = $recordset['intrnltravel'];
				 			$profileData["istripnexttwoyears"] = $recordset['istripnexttwoyears'];
				 			$profileData["income"] =	 $recordset['income'];
				 			$profileData["otherprefertravel"] =	 $recordset['otherprefertravel'];
				 			$profileData["ispromosend"] = $recordset['ispromosend'];
				 			$profileData["signature"] = $recordset['signature'];
	
							$profile = new TravelerProfile;
							$profile->setProfileData($profileData);

							$traveler->setTravelerProfile($profile);								
	                   	}
	                   	catch (Exception $e) {
	                    	throw $e;
	                   	}

						$mMembers[] = $traveler;
	               }
				}

			$sql2 = "SELECT FOUND_ROWS() AS `total_records`";
			$mRs->Execute($sql2);
			$recordset = mysql_fetch_array($mRs->Resultset());
			return array($mMembers,$recordset["total_records"]);               
	 	
	 } 
	
	function getTravelerFriendsWithPhotoPrioritized($context,$filter){
		$filtercriteria = null;
		$travelerID	=	$context->get('TRAVELER_ID');
		$hashKey	=	'';
		$found	=	false;

	 	if ($filter->IsKeyExists('FILTER_CRITERIA')){
	 		$filtercriteria	=	$filter->get('FILTER_CRITERIA');
	 		if ($filtercriteria){
	 			$hashKey	=	$filtercriteria->getHash();
	 		}
	 	}
 	
	 	if (!$found){	
		 	if ($filtercriteria){				
			$hashKey	=	$filtercriteria->getHash();
		 		$myconditions = $filtercriteria-> getConditions();
				$attname = $myconditions[0]-> getAttributeName();
			$operation = $myconditions[0]-> getOperation();

				switch ($operation){
					case FilterOp::$ORDER_BY :
						$condition = " ORDER BY " . $attname ;
						break;
					case FilterOp::$ORDER_BY_DESC :
						$condition = " ORDER BY " . $attname . " DESC " ;
						break;
				}
		}			
		else
			$condition = " GROUP BY travelerID ORDER BY photoID, latestlogin DESC ";
	
			// get startrow and rows per page
			if ($filtercriteria && $filtercriteria->getStartRow() >= 0 && $filtercriteria->getRowsPerPage() > 0 ){
			    $offset   = $filtercriteria->getStartRow();
			    $rows     = $filtercriteria->getRowsPerPage();
			    $limitstr = " LIMIT " . $offset . " , " . $rows;
			} else {
				$limitstr = "";
			}
	
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblFriend.friendID, tblTraveler.*, tblTravelertoPhoto.photoID IS NULL AS photoID " .
					"FROM tblFriend, tblTraveler " .
					"LEFT JOIN tblTravelertoPhoto ".
					"ON tblTravelertoPhoto.travelerID = tblTraveler.travelerID ".
					"WHERE tblTraveler.travelerID != $travelerID ".
					"AND tblTraveler.travelerID = tblFriend.friendID " .
					"AND tblFriend.travelerID = $travelerID " .
					"AND tblTraveler.active = 1 " .
					"AND tblTraveler.isSuspended = 0 " .
					"AND tblTraveler.deactivated = 0 " .
					$condition . $limitstr;
			$rs	=	ConnectionProvider::instance()->getRecordset();
			$rs2 =	ConnectionProvider::instance()->getRecordset2();
			$rs->Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$rs2->Execute($sql2);

			$friends = array();

			if (0 < $rs2->Result(0, "totalRecords")){				
				$travelerIDs = array();
				while ($friend = mysql_fetch_array($rs->Resultset())){
						$obj_traveler = new Traveler;
						$obj_traveler->setTravelerData($friend);
						$friends[$friend['friendID']] = $obj_traveler;
						$travelerIDs[]	=	'Traveler_' . $friend['travelerID'];			
				}
			}

	 	}
 		
		return $friends;
	}
	 
 	/************** 	 
 	 * setter functions
 	 */
 	function setCache($cache){
 		$this->cache = $cache;
 	}
 }
?>
