<?php

	require_once	'Cache/ganetCacheProvider.php';
	require_once 	'travellog/vo/Class.ganetValueObject.php';
	require_once	'Class.ConnectionProvider.php';
	
	class ganetRotatingPhotoRepo {
		protected $cache	=	null;
		protected static $instance	= 	null;
		
		private function ganetRotatingPhotoRepo(){
			$this->cache = ganetCacheProvider::instance()->getCache();	
		}
		
		public static function instance(){
			if (self::$instance == null){
				self::$instance	=	new ganetRotatingPhotoRepo();
			}
			return self::$instance;
		}
		
		public function retrieve(ganetValueObject $vo){
			$id = $vo->IsKeyExists('id') ? $vo->get('id') : null;
			$foundInCache = false;
			
			if ($this->cache != null){
				$photoArr = $this->cache->get('Rotating_Photo_Array');
				if ($photoArr){
					$foundInCache = true;
					$photo = $this->retrieveRandom($photoArr, $id);
				}	
			}
			
			if (!$foundInCache){
				$status = $vo->IsKeyExists('status') ? $vo->get('status') : RotatingPhoto::PHOTO_ACTIVE_STATUS;
				
				$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$sql = "SELECT * FROM tblRotatingPhoto WHERE status = $status";
	 			$rs->Execute($sql);
	 			
 				if ($rs->Recordcount() == 0){
 					//throw new Exception("Invalid Photo ID");		// ID not valid so throw exception	 					
 				}
 				
 				$photoArr= array();
 				while ($recordset = mysql_fetch_array($rs->Resultset())) {
 					$photoArr[] = new RotatingPhoto(null, $recordset);
 				}
 				
 				// then set RotatingPhoto in cache
				if ($this->cache != null) {
					$this->cache->set('Rotating_Photo_Array', $photoArr, array('EXPIRE'=>86400) );
				}
				
				$photo = $this->retrieveRandom($photoArr, $id);
			}
			
			return $photo instanceof RotatingPhoto ? $photo : false;
		}
		
		protected function retrieveRandom($photoArr, $id){
			$same_id = true;
			$photo = false;
			while ($same_id && !empty($photoArr)){
				$rand_key = array_rand($photoArr);
				$photo = $photoArr[$rand_key];
				if(!$photo->isExistingUser() && $photo->isSuspendedUser()) {
					unset($photoArr[$rand_key]);
				}
				else {
					if($id != $photo->getPhotoID() || is_null($id))
						$same_id = false;
					else {
						unset($photoArr[$rand_key]);
					}	
				}
			}
			return $photo;
		}
	}