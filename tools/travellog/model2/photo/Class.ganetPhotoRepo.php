<?php

	require_once	'Cache/ganetCacheProvider.php';
	require_once 	'travellog/vo/Class.ganetValueObject.php';
	require_once	'travellog/model/Class.PhotoType.php';
	require_once	'Class.ConnectionProvider.php';

	class ganetPhotoRepo {
		protected $cache	=	null;
		protected static $instance	= 	null;
		
		private function ganetPhotoRepo(){
			$this->cache = ganetCacheProvider::instance()->getCache();	
		}
		
		public static function instance(){
			if (self::$instance == null){
				self::$instance	=	new ganetPhotoRepo();
			}
			return self::$instance;
		}
		
		function retrieve(ganetValueObject $vo){
			/* main contents of $vo CONTEXT,FILTER which are assoc arrays and COMMAND which is a string */
	 		$context 	=	$vo->IsKeyExists('CONTEXT')?$vo->get('CONTEXT'):null;
			$filter 	=	$vo->IsKeyExists('FILTER')?$vo->get('FILTER'):null; 		
	 		$command 	=	$vo->IsKeyExists('COMMAND')?$vo->get('COMMAND'):'';
	 		
	 		$photo	 	=	null;
			
			switch ($command) {
				case 'PRIMARY_PHOTO':
					$photo	=	$this->getPrimaryPhoto($context,$filter);
					break;		
				default:
					// COMMAND is '' so we just instantiate a Photo object based on id
					$foundInCache	=	false;
					
					$photoID		=	$context->get('PHOTO_ID');
					$photoContext	=	$context->get('PHOTO_CONTEXT');
					
					if ($this->cache != null) {
						$photo	=	$this->cache->get('Photo_' . get_class($photoContext) . '_' . $photoID );
						if ($photo){
							$foundInCache	=	true;
						}	
					}
					
					////////////////////////////////

					if (!$foundInCache){
						$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
						$sql = "SELECT * from tblPhoto where photoID = '$photoID' ORDER BY photoID DESC";
			 			$rs->Execute($sql);
			 			
		 				if ($rs->Recordcount() == 0){
		 					//throw new Exception("Invalid Photo ID");		// ID not valid so throw exception	 					
		 				}
		 					
		 				$photo	=	new Photo($photoContext,0,mysql_fetch_array($rs->Resultset()));
		 				
		 				// then set Photo in cache
						if ($this->cache != null) {
							$this->cache->set('Photo_' . get_class($photoContext) . '_' . $photoID,$photo,array('EXPIRE'=>86400) );
						}
					}
	 				
	 				// set values to its attributes	 		 			
		 			/*$this->phototypeID 	= $rs->Result(0,"phototypeID"); 
		 			$this->caption 		= stripslashes($rs->Result(0,"caption")); 
		 			$this->filename 		= stripslashes($rs->Result(0,"filename")); 
		 			$this->thumbfilename = stripslashes($rs->Result(0,"thumbfilename"));
					$this->primaryphoto 	= $rs->Result(0,"primaryphoto");
					$this->dateuploaded 	= $rs->Result(0,"dateuploaded");
					*/
					break;
			}		 		
 			return $photo;	
	 		
		}	
		
		function createNew(){
			
		}
		
		function getList(ganetValueObject $vo){
			
		}
		
		
		/*********************
	 	 * action functions
	 	 */
		
		protected function getPrimaryPhoto($context,$filter){
			$photoType	=	$context->get('PHOTO_TYPE');
			
			switch ($photoType) {
				case PhotoType::$PHOTOALBUM :
					return $this->getPhotoAlbumPrimaryPhoto($context,$filter);
					break;
				case PhotoType::$PROFILE :
					return $this->getProfilePrimaryPhoto($context);		
				default:
					break;	
			}
		}
		
		/***************************
		 * third level functions
		 */
		
		protected function getPhotoAlbumPrimaryPhoto($context,$filter){
			$albumID = $context->get('PHOTO_ALBUM_ID');
			$photoAlbum	=	$context->get('PHOTO_ALBUM');
			$primphoto	=	null;
			
			// check if photo is in cache
			if ($this->cache != null){
				$primphoto	=	$this->cache->get('PhotoAlbum_PrimaryPhoto_' . $albumID );
				if ($primphoto){
					return $primphoto;
				}				
			}
			
			$mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
 			
			$sql = "SELECT tblPhotoAlbum.primaryPhoto as photoID, tblPhoto.phototypeID as phototypeID
					FROM tblPhotoAlbum, tblPhoto
					WHERE tblPhotoAlbum.photoalbumID = $albumID AND
						tblPhotoAlbum.primaryPhoto = tblPhoto.photoID
					GROUP BY tblPhotoAlbum.primaryPhoto";
			$mRs->Execute($sql);
			
			if( $mRs->Recordcount() == 0 ){
				$sql		= "SELECT tblPhoto.photoID as photoID, tblPhoto.phototypeID as phototypeID from tblPhotoAlbumtoPhoto,tblPhoto 
						 		WHERE tblPhotoAlbumtoPhoto.photoID = tblPhoto.photoID AND tblPhoto.primaryphoto = 1
						 		AND tblPhotoAlbumtoPhoto.photoalbumID = '".$albumID."' ";
				$mRs->Execute($sql);
				if ($mRs->Recordcount() == 0){	// if no photo records yet, set to default value zero
 					$rand = $this->getPhotoAlbumRandomPhoto($photoAlbum);
					if( $rand ){
						$_photoID = $rand->getPhotoID();
	 					$_phototypeID = $rand->getPhotoTypeID();
					}else{
						$_photoID = 0;
	 					$_phototypeID = 7;
					}
 				}else{ 
					$_photoID = $mRs->Result(0,"photoID");
					$_phototypeID = $mRs->Result(0,"phototypeID");
					$sql = "UPDATE tblPhotoAlbum
							SET primaryPhoto = $_photoID
							WHERE photoalbumID = ".$albumID;
					$mRs->Execute($sql);
				}
			}else{
				$_photoID = $mRs->Result(0,"photoID");
				$_phototypeID = $mRs->Result(0,"phototypeID");
			}
			
           try {
				switch($_phototypeID){
					case PhotoType::$PROFILE	:	require_once("travellog/model/Class.TravelerProfile.php");
													$context = TravelerProfile::getTravelerContext($_photoID);
													$primphoto = new Photo($context,$_photoID);
													break;
					case PhotoType::$TRAVELLOG	:	require_once("travellog/model/Class.TravelLog.php");
													$context = TravelLog::getTravelLogContext($_photoID);
													$primphoto = new Photo($context,$_photoID);
													break;
					default						:	//$primphoto = new Photo($photoAlbum,$_photoID);
													$context = PhotoAlbum::getPhotoAlbumContext($albumID,$_photoID);
													if( !$context ){
														$context = $photoAlbum;
													}
													$primphoto = new Photo($context,$_photoID);
													break;
				}
			}catch (Exception $e) {
                 throw $e;
            }
            
            // set photo in cache
            if ($this->cache != null){
            	$this->cache->set('PhotoAlbum_PrimaryPhoto_' . $albumID,$primphoto,86400);
            }
 	
 			return $primphoto;
		}
		
		protected function getPhotoAlbumRandomPhoto($photoAlbum){
			$albumID	=	$photoAlbum->getPhotoAlbumID();
			$rs = new Recordset(ConnectionProvider::instance()->getConnection());
	 		
			try {
				$randphoto = new Photo($photoAlbum);
			}
			catch (Exception $e) {						  
			   throw $e;
			}
					 				
 			// get the number of photos to choose from
 			$sql = "SELECT count(photoID) as totalrec from tblPhotoAlbumtoPhoto WHERE photoalbumID = '$albumID' ";
 			$rs->Execute($sql);
 			
 			if ($rs->Result(0,"totalrec") == 0){
 				try {
 					$randphoto = new Photo($photoAlbum);
 				}
				catch (Exception $e) {						  
				   throw $e;
				}
 			}	 				
 			else {
 				
 				$max = $rs->Result(0,"totalrec") - 1;
				
				$randidx = mt_rand(0 , $max );	 		// the index starting from top row
				
 				$sql = "SELECT tblPhotoAlbumtoPhoto.photoID as photoID, tblPhoto.phototypeID as phototypeID
						FROM tblPhotoAlbumtoPhoto, tblPhoto
						WHERE tblPhotoAlbumtoPhoto.photoalbumID = '$albumID' AND
							tblPhotoAlbumtoPhoto.photoID = tblPhoto.photoID
					 	ORDER BY tblPhotoAlbumtoPhoto.photoID LIMIT $randidx , 1"  ;
 				$rs->Execute($sql);
 				
				while ($recordset = mysql_fetch_array($rs->Resultset())) {
					require_once("travellog/model/Class.PhotoType.php");
                        try {
						switch($recordset['phototypeID']){
							case PhotoType::$PROFILE	:	require_once("travellog/model/Class.TravelerProfile.php");
															$context = TravelerProfile::getTravelerContext($recordset['photoID']);
															$randphoto = new Photo($context,$recordset['photoID']);
															break;
							case PhotoType::$TRAVELLOG	:	require_once("travellog/model/Class.TravelLog.php");
															$context = TravelLog::getTravelLogContext($recordset['photoID']);
															$randphoto = new Photo($context,$recordset['photoID']);
															break;
							default						:	//$randphoto = new Photo($photoAlbum,$recordset['photoID']);
															$context = PhotoAlbum::getPhotoAlbumContext($albumID,$recordset['photoID']);
															if( !$context ){
																$context = $photoAlbum;
															}
															$randphoto = new Photo($context,$recordset['photoID']);
															break;
						}
					}catch (Exception $e) {
                        throw $e;
                    }
				}
			}
			
			return $randphoto;
		}
		
		function getProfilePrimaryPhoto($context){
			require_once('Class.dbHandler.php');
			require_once('travellog/model2/traveler/Class.ganetTravelerRepo.php');
			
			$travelerID			=	$context->get('TRAVELER_ID');
			$travelerProfile	=	$context->get('TRAVELER_PROFILE');	
			try {
 				$handler = GanetDbHandler::getDbHandler();
 				
 				$sql = "SELECT tblPhoto.* FROM tblTravelertoPhoto, tblPhoto " .
 					" WHERE tblTravelertoPhoto.photoID = tblPhoto.photoID " .
 					" AND tblPhoto.primaryphoto = 1 " .
 					" AND tblTravelertoPhoto.travelerID = ".$handler->makeSqlSafeString($travelerID);
 				
 				$rs = new ResourceIterator($handler->execute($sql));
 				$primphoto = ($rs->valid()) ? new Photo($travelerProfile, 0, $rs->current()) : new Photo($travelerProfile, 0);	
 				
 				// to cache this primary photo we have to set this to the travelerprofile object, set it to traveler object and put it back to cache 
 				if ($this->cache != null && ($traveler = $this->cache->get('Traveler_' . $travelerID)) ) {
 					$travelerProfile->setPrimaryPhoto($primphoto);
 					$traveler->setTravelerProfile($travelerProfile);
 					$this->cache->set('Traveler_' . $travelerID, $traveler, array('EXPIRE'=>86400));
 				}
 			}
 			catch (exception $ex) {}
			return $primphoto;
		}
		
	}
?>