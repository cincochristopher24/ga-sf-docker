<?php
 require_once 	'Class.ConnectionProvider.php';
 require_once 	'Class.Recordset.php';
 require_once 	'travellog/model2/dao/Class.ganetAbstractDao.php';
 require_once 	'travellog/model2/dao/Class.ganetGroupDao.php';
 require_once	'Class.dbHandler.php';
 require_once	'Class.SqlResourceIterator.php';
 require_once 	'Cache/ganetCacheProvider.php';
 
 
 class ganetFunGroupDao extends ganetGroupDao{
	protected $cache;
	protected $cacheConnected = true;
	
	function ganetFunGroupDao(){
	 	
	 	$this->cache = ganetCacheProvider::instance()->getCache();
		if ($this->cache == null)
			$this->cacheConnected = false;
	}
	
	function retrieve($id){
		
	}
	function create($group){
		
	}
	
	function update($group){
	  
	  if ($this->cacheConnected){
	  		$this->cache->delete('Group_' . $group->getGroupID());
	  		$this->cache->set('Group_' . $group->getGroupID(),$group);																																																				
	  }	
	  
	  	$handler = new dbHandler();
	    $aname         = $handler->makeSqlSafeString($group->getName());
	    $adescription  = $handler->makeSqlSafeString($group->getDescription());
	    
	    $sql = "UPDATE tblGroup SET name = $aname, description = $adescription, groupaccess = '".$group->getGroupAccess()."' WHERE groupID = '".$group->getGroupID()."'  " ;
	    $rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	    $rs->Execute($sql);	
			
	  
	}
	
	function delete($group){
		if ($this->cacheConnected){
	  		$this->cache->delete('Group_' . $group->getGroupID());
		}
		$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		
		$sql = "DELETE FROM tblGroup WHERE groupID = " . $group->getGroupID();
	    $rs->Execute($sql);

		$sql = "DELETE FROM tblSendable WHERE sendableType = 2 AND sendableID = " . $group->getSendableID();
		$rs->Execute($sql);
		
	}
	
 }
 
 
?>
