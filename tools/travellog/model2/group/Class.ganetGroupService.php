<?php
/*
 * Created on 04 4, 09
 * 
 * Author:
 * Purpose: 
 * 
 */

require_once('travellog/model/Class.FilterCriteria2.php');
require_once('travellog/model2/group/Class.ganetGroupRepo.php');
require_once('travellog/vo/Class.ganetValueObject.php');
 
class ganetGroupService {
	protected $repo;
 	function ganetGroupService(){
 		$this->repo = new ganetGroupRepo();
 		$this->travRepo	= new ganetTravelerRepo();
 	}
 	
 	function retrieve(ganetValueObject $vo){
 		$t = $this->repo->retrieve($vo);
 		
 		return $t;			// 4/1/09, this is temporary, in the final impl the group object t should be converted 
 							//	 into a value object, as of now an empty traveler object will receive $t
 		
 	}
 	
 	
 	function update(ganetValueObject $vo){
 		
 	}
 	

	function getList(ganetValueObject $vo){
		
	}
 	
 	
 	function setRepo($repo){
 		$this->repo = $repo;
 	}
 	
 	
 	
 	function addMember($travelerID){
 		//Traveler
 	}
 	
 	function removeMember($travelerID){
 		
 	}
 	
 	function getMembers(ganetValueObject $vo, $prioritizeMembersWithPhotos=FALSE){
 		//echo 'ganetGroupService->getMembers <br/>';
 		// add command to vo
		if( $prioritizeMembersWithPhotos ){
			$vo->set('COMMAND','GROUP_MEMBERS_WITH_PHOTO_PRIORITIZED');
		}else{
 			$vo->set('COMMAND','GROUP_MEMBERS');				// means to get list of group members
 		}

 		$members = $this->travRepo->getList($vo); 
 		
 		return $members;
 	}
 	
 	function countMembers(FilterCriteria2 $cri){
 		
 	}
 	
 	
 	
}
?>
