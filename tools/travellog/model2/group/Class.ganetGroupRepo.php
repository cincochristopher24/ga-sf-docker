<?php
/*
 * Created on 04 4, 09
 * 
 * Author:
 * Purpose: 
 * 
 */
 
 require_once	'Cache/ganetCacheProvider.php';
 require_once	'Class.ConnectionProvider.php';
 require_once 	'Class.Recordset.php';
 require_once 	'travellog/vo/Class.ganetValueObject.php';
 require_once	'Class.dbHandler.php';
 require_once	'Class.SqlResourceIterator.php';
 
 class ganetGroupRepo {
 	protected $cache = null;
	protected $cacheConnected = true;
 	protected $conn;

 	function ganetGroupRepo(){
		//$this->conn = new Connection();
		
		
	 	
	 	$this->cache = ganetCacheProvider::instance()->getCache();
		if ($this->cache == null)
			$this->cacheConnected = false;
	 	
 	}
 	
	protected static $groupDiscriminators = array();

 	function retrieve(ganetValueObject $vo){
 		$group 	= null;
 		$id 	= $vo->get('id');
 		
 		if ($this->cacheConnected){
 			$group = $this->cache->get($id);
 		}
 		
 		if (!$group){
			// check type of group of existing
			if ($vo->isKeyExists('type')){
				
				switch ($vo->get('type')){
					case 'ADMIN' :
						$group = $this->instantiateAdminGroup($id);
						break;						
					case 'FUN':
						$group = $this->instantiateFunGroup($id);
						break;
					default:
						throw new Exception($vo->get('type') . ' is not defined as a type of a group!');
				}
				
			} else {
				// no key 'type' exists so we need to get the type of group
				if( isset(self::$groupDiscriminators[$id]) ){
					switch(self::$groupDiscriminators[$id]){
						case 1:
							$group = $this->instantiateFunGroup($id);
							break;
						case 2:
							$group = $this->instantiateAdminGroup($id);
							break;
						default:	
							throw new Exception('Descriminator value is not defined as a type of a group!');
					}
				}else{
					$sql	= 	"select discriminator from tblGroup where groupID='$id'";
					$rs		= 	new Recordset(ConnectionProvider::instance()->getConnection());
					$rs->Execute($sql);
					if ($rs->Recordcount() > 0){
						switch ($rs->Result(0,'discriminator')){
							case 1:
								$group = $this->instantiateFunGroup($id);
								break;
							case 2:
								$group = $this->instantiateAdminGroup($id);
								break;
							default:	
								throw new Exception('Descriminator value is not defined as a type of a group!');	
						}
						/*if ($rs->Result(0,'discriminator') == 2){
							$group = $this->instantiateAdminGroup($id);
						
						} else if ($rs->Result(0,'discriminator') == 1){
							$group = $this->instantiateFunGroup($id);
						}*/
						self::$groupDiscriminators[$id] = $rs->Result(0,'discriminator');
					} else {
						throw new Exception('Group id ' . $id . ' not found in tblGroup');
					}
				}	
				
			}
			
			// set group in cache
			if ($this->cacheConnected){
 				$this->cache->set('Group_' . $id, $group);
 			} 			
 		} else {
 			//echo "> got group $group from cache <br/>";
 		}
 		
 		
 		return $group;
 	}
 	
	// added by Jul, to store group object if that group is instantiated more than once in a single page 
 	protected static $instantiatedGroups = array();

 	protected function instantiateAdminGroup($id){
 		// ADMIN group (note: I just followed the code from the instatiation code of Admin Group)
						
		$group	=	new AdminGroup(); 		
		$handler = new dbHandler();

		if( isset(self::$instantiatedGroups[$id]) ){
			$group = self::$instantiatedGroups[$id];
		}else{

			$sql = "SELECT * FROM tblGroup WHERE " .
				" `groupID` = ". $id;
			
			$it = new SqlResourceIterator($handler->execute($sql));
		
			if ($it->hasNext()) {
				$group->setAdminGroupData($it->next());
			
				$sql = "SELECT clientID FROM tblGrouptoAdvisor WHERE " .
					" `groupID` = ".$id;
			
				$it = new SqlResourceIterator($handler->execute($sql));
			
				if ($it->hasNext()) {
					$group->setAdminGroupData($it->next());
				}
			}
			self::$instantiatedGroups[$id] = $group;
		}
		return $group;
 	}
 	
 	function getList(){
 		
 		
 		
 		 
 	}
 	
 	
 	/*************************
 	 * worker functions
 	 * 
 	 */
 	
 	
 	/**************************
 	 * helper functions
 	 * 
 	 */
 	protected function instantiateFunGroup($id){
		if( isset(self::$instantiatedGroups[$id]) ){
			$group = self::$instantiatedGroups[$id];
		}else{
			$group = new FunGroup(); 	
			$handler = new dbHandler();	
		
	       	$sql 	= "SELECT * FROM tblGroup WHERE groupID = '$id' ";
			$it 	= new SqlResourceIterator($handler->execute($sql));       	
       	
	   		if ($it->hasNext()){    	
	 			$group->setFunGroupData($it->next());                   
 		
	   		} else {
	   			throw new Exception("Invalid GroupID " . $id);        // ID not valid so throw exception
	   		}
			self::$instantiatedGroups[$id] = $group;
		}
 		return $group;
 		
 	}
 	
 	
 	
 	
 	
 	
 }
