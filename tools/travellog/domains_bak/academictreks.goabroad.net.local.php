<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalACADEMICTREKS implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'ACADEMICTREKS'
		);

		public function getIdentifier(){
			return 'academictreks.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalACADEMICTREKS());

?>