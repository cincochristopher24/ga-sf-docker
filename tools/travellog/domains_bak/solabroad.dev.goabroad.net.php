<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevSOLABROAD implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhRg9IWDYwaA3-2UAeGRXtAMoIwx8BSZJY6V8DT6rcn3kBWlU8M-Ko6UnQ',
			'Site'	=>	'SOLABROAD'
		);

		public function getIdentifier(){
			return 'solabroad.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevSOLABROAD());

?>