<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdGLOBALSEMESTERS implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhReun0mv98cc9pdB4teYmXsJWKYJBSzbwafpOqQ2z1qe2jEfZXZvAtOrw',
				'Site'	=>	'GLOBALSEMESTERS'
			);
	
		public function getIdentifier(){
			return 'globalsemesters.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdGLOBALSEMESTERS()); 
	
?>