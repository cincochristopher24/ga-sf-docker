<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalProWorld implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RQtcwVHkahEeNntdiEeJ6IMKMlPPRQwQEy8XR8rb__z3RaRuM5Xy8KmVA',
				'Site'	=> 'PROWORLD'
			);

		public function getIdentifier(){
			return 'proworld.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalProWorld());
		
?>