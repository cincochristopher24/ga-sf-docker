<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalFRENCHEMBASSY implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'FRENCHEMBASSY'
		);

		public function getIdentifier(){
			return 'ambafrance-us.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalFRENCHEMBASSY());

?>