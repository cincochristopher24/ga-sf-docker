<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdAIFS implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRQrvjx87JdXXCOXbdVxwlvWKrY8WxQ_WbjonOfN_EJS8NRSfe9yAbgR9Q',
				'Site'	=>	'AIFS'
			);
	
		public function getIdentifier(){
			return 'aifs.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues; 
		}
	}
	
	IniConfig::register(new IniProdAIFS());
	
?>