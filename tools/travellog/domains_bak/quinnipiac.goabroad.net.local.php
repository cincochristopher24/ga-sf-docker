<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalQUINNIPIAC implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'QUINNIPIAC'
		);

		public function getIdentifier(){
			return 'quinnipiac.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalQUINNIPIAC());

?>