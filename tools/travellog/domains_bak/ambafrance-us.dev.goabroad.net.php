<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevFRENCHEMBASSY implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhRsWW3HqYkXgMyT9yZVO3yUTCIUZhS-9zVg8HV2qBv_rnHdBy6V_eFtjA',
			'Site'	=>	'FRENCHEMBASSY'
		);

		public function getIdentifier(){
			return 'ambafrance-us.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevFRENCHEMBASSY());

?>