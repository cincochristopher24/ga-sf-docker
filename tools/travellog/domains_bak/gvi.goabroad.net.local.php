<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalGVI implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQHGOrlzmXyhXuuMySEpb2nC2FgfBSUu23bXygYCe-ZnPCEi_OjV3JeCA',
			'Site'	=>	'GVI'
		);

		public function getIdentifier(){
			return 'gvi.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalGVI());

?>