<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevLIVINGROUTES implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSVoUXZt8mOBrDszlW-T16TIvYEghS2L_DE3i8FpNbsdjwpJISXKDRR2g',
				'Site'	=>	'LIVINGROUTES'
			);
	
		public function getIdentifier(){
			return 'livingroutes.dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevLIVINGROUTES()); 
	
?>