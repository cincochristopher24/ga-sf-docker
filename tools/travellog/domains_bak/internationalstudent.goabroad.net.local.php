<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalISI implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQIr3jU-HwqY02dRWapf5ODvB0P_RQE92EBJSUMowzT4q8xVl_FfGBUgA',
			'Site'	=>	'ISI'
		);

		public function getIdentifier(){
			return 'internationalstudent.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalISI());

?>