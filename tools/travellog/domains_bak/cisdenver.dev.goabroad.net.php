<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevCISDENVER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhTGQz4rdzv8YsEhsWa7YvBhrlbrHxRLCzjjRU26eni8vj96Y88D8Tu-rA',
			'Site'	=>	'CISDENVER'
		);

		public function getIdentifier(){
			return 'cisdenver.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevCISDENVER());

?>