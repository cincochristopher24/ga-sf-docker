<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevEDUCASIAN implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshRU9e474f5YZroRSFfW3Ic61902ORQeOyCRGNBQQwZMydNRvddvcF2NkQ',
				'Site'	=>	'EDUCASIAN'
			);
	
		public function getIdentifier(){
			return 'educasian.dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevEDUCASIAN()); 
	
?>