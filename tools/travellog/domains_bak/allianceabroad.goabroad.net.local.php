<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalALLIANCEABROAD implements Initialization {

		private $initializedValues = array (
				'GMapKey' => '',
				'Site'	=>	'ALLIANCEABROAD'
			);

		public function getIdentifier(){
			return 'allianceabroad.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalALLIANCEABROAD());
		
?>