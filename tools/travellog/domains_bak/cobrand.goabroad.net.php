<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevCOBRAND implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRSuhWSY3v2csAEBjf9N5IzGMINBMBR-j0EguIP45CKb1y9na3WTrQvD2Q',
				'Site'	=>	'COBRAND'
			);
	
		public function getIdentifier(){
			return 'cobrand.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevCOBRAND()); 
	
?>