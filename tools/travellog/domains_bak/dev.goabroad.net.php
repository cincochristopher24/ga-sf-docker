<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevDotNet implements Initialization {
	
		private $initializedValues = array (
				//'GMapKey' => 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RSxAnUob8CCeJxJdtYJBdK6zFkHgxQ5xa2hmJL9BHBK116ssslR4r05yA',
				'GMapKey' => 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RTSLFXcXnfoN54L8eFKyvlGyILP5BTuHfFTyvfjz-Q6KMmGVyvV-0wWyQ',
				'Site'	=>	'GANET'
			);
	
		public function getIdentifier(){
			return 'dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevDotNet());	
?>