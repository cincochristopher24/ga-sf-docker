<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalCENTRALCOLLEGE implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'CENTRALCOLLEGE'
		);

		public function getIdentifier(){
			return 'central.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalCENTRALCOLLEGE());

?>