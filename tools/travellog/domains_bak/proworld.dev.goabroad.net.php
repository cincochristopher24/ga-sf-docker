<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevProWorld implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRQJPjEz3m8Ep2cXZt6oHWplRmafRhTQp25ZSB4TusPW_bKomQnBGkA4IA',
				'Site'	=>	'PROWORLD'
			);
	
		public function getIdentifier(){
			return 'proworld.dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevProWorld());
	
?>