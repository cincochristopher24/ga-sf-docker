<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevISI implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhS_GqSyjWvJwK1b1JOgz8zzWQ3rjBR5nKyMQxtR77MvBIdPlSDK5OZiFg',
			'Site'	=>	'ISI'
		);

		public function getIdentifier(){
			return 'internationalstudent.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevISI());

?>