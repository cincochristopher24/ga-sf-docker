<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalGLOBALSCHOLAR implements Initialization {

		private $initializedValues = array (
				'GMapKey' => '',
				'Site'	=>	'GLOBALSCHOLAR'
			);

		public function getIdentifier(){
			return 'globalscholar.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalGLOBALSCHOLAR());
		
?>