<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalGLOBALSEMESTERS implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhS3JJwTEqbalduU9xSrilrZkXnHbRTp1uA3MNwE2nMFiqp3KzcVFPo19Q',
				'Site'	=>	'GLOBALSEMESTERS'
			);

		public function getIdentifier(){
			return 'globalsemesters.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalGLOBALSEMESTERS());
		
?>