<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdGVI implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhRin3bIgOnKi3LY4s4r4P53tmmlVBSb9ZYDhaOZqDwIic0lJVmiAwOGKQ',
			'Site'	=>	'GVI'
		);

		public function getIdentifier(){
			return 'gvi.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdGVI());

?>