<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalDotNet implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAxssX44ErD-uqyd-GW74yDBS_IWFy692u9pZ7fVeFegkeYwDfHRQ0DmyB5o0hSF4zV4IdIwGBmzCXEw',
				'Site'	=>	'GANET'
			);
	
		public function getIdentifier(){
			return 'goabroad.net.local';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalDotNet());
?>