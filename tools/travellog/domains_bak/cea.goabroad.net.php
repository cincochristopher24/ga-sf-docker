<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdCEA implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshSQ8yQevSo67iuqWW07iLNbIyUaqRR2k88GBFgAPVwA6XKubkx7f73PtA',
				'Site'	=>	'CEA'
			);
	
		public function getIdentifier(){
			return 'cea.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdCEA());
	
?>