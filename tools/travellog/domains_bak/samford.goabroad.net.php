<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdSAMFORD implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQJ0_7W0XohDMpfw86Yo3YgcTFbbhSNGd1hHV0UoO2z37l1TMWY6rhSsA',
			'Site'	=>	'SAMFORD'
		);

		public function getIdentifier(){
			return 'samford.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdSAMFORD());

?>