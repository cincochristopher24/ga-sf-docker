<?php
	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdDotNet implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RQwbP-JBhpjGqlbK3A4xF5aLrtY6RSWOoaOMJXoU0oq2GqMOlflVMN-hg',
				'Site'	=>	'GANET'
			);

		public function getIdentifier(){
			return 'www.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdDotNet());	
?>