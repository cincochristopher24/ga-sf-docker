<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdALLIANCEABROAD implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshQz3lOPONyrhwM_Y39uIBLQBrdXhBQNHUBX5nMtTbbhRbLRV2WUg4DFug',
				'Site'	=>	'ALLIANCEABROAD'
			);
	
		public function getIdentifier(){
			return 'allianceabroad.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdALLIANCEABROAD());
	
?>