<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdISI implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSrnoWKuNFuOnB87clxAEkEZ-3HuhR1k71euwHGTAlNS_s3svEQp5sypg',
			'Site'	=>	'ISI'
		);

		public function getIdentifier(){
			return 'isi.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdISI());

?>