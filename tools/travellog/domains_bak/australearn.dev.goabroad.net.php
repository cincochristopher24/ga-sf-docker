<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniDevAUSTRALEARN implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhSR_beOvBZf2bpQvXzg4-reBwAvLhSZGg_ivshXERroPih9G7aZygkWtg',
			'Site'	=>	'AUSTRALEARN'
		);

		public function getIdentifier(){
			return 'australearn.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniDevAUSTRALEARN());

?>