<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevGLOBALSEMESTERS implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQRT_YBpUUNpoJojtQeFd0PpFsyexTQnT9K8QuxckTXDWlwvuTV0A026w',
				'Site'	=>	'GLOBALSEMESTERS'
			);
	
		public function getIdentifier(){
			return 'globalsemesters.dev.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevGLOBALSEMESTERS()); 
	
?>