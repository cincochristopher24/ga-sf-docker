<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdCIS implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshTQXuIblzS4E31jZiJ5wQ60pxhl7xTE0Z6rBZcArkroxyGO7GfMOhihtw',
				'Site'	=>	'CIS'
			);
	
		public function getIdentifier(){
			return 'cis.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdCIS());