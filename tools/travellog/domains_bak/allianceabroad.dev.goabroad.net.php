<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevALLIANCEABROAD implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshTavSWdK5IbBBy8jIpoigPfS5HqNhR5jqyYE6N1gmAhEg0e_EGdu4RZjA',
				'Site'	=>	'ALLIANCEABROAD'
			);

		public function getIdentifier(){
			return 'allianceabroad.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevALLIANCEABROAD());
		
?>