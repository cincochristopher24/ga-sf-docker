<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniDevCULTURALEMBRACE implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshTwKOFsAx8_YYgMajk5Tt-c1qbHYRT7SeSXum3_GPhYCWfSglcOayDoyA',
				'Site'	=>	'CULTURALEMBRACE'
			);

		public function getIdentifier(){
			return 'culturalembrace.dev.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniDevCULTURALEMBRACE());
		
?>