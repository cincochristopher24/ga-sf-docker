<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniLocalADVISOR implements Initialization {

		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshQ_J_8meMKI7berMq2m3gYA1YHnmxT8uV0Ywvkd7f_ZmPfL0XD8F8faWQ',
				'Site'	=>	'ADVISOR'
			);

		public function getIdentifier(){
			return 'advisor.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniLocalADVISOR());
		
?>