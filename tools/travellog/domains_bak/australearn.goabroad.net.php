<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdAUSTRALEARN implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhTqDEjPz4AfwPK76edM_syzLFiOZhQfDyZgRvJZ1tllIgpv8Djw0C5f9Q',
			'Site'	=>	'AUSTRALEARN'
		);

		public function getIdentifier(){
			return 'australearn.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdAUSTRALEARN());

?>