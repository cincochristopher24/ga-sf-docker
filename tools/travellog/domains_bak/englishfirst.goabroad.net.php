<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdENGLISHFIRST implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhTp_n8prhoD7hFh4ED5mNI_i19s_BSa4QmuiJunNLWs81ayHwe7_gOWGg',
			'Site'	=>	'ENGLISHFIRST'
		);

		public function getIdentifier(){
			return 'englishfirst.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdENGLISHFIRST());

?>