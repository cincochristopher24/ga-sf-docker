<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalCIEE implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'CIEE'
		);

		public function getIdentifier(){
			return 'ciee.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalCIEE());

?>