<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdIEP implements Initialization {
	
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshSsCVaGFSzcFIs0qTDOvG6htOv1-xRpiMsZXNs310iOy_Jro-qkzguYgg',
				'Site'	=>	'IEP'
			);
	
		public function getIdentifier(){
			return 'iep.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdIEP());