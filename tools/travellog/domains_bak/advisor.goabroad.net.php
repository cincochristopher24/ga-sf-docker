<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');
	
	class IniProdADVISOR implements Initialization {
		private $initializedValues = array (
				'GMapKey' => 'ABQIAAAA9Vn5-X-JMj65adcOD2KxshRYZZw3GzZmR9V9iTUUPYqtHbZj2hQUWaY9HgSMrhwlRlmHGm40Rr3Iyg',
				'Site'	=>	'ADVISOR'
			);
		
		public function getIdentifier(){
			return 'advisor.goabroad.net';
		}
	
		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}
	
	IniConfig::register(new IniProdADVISOR()); 
	
?>