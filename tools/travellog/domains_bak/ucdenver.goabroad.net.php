<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniProdUCDENVER implements Initialization {

		private $initializedValues = array (
			'GMapKey' => 'ABQIAAAAD83G3W1uhIbOSgWY4b2-VhQcNspfxSxUCuzNVh8nSWIAoRkUxRRTK9Tgy0uSNAop5rtt1Z6IpS3QYw',
			'Site'	=>	'UCDENVER'
		);

		public function getIdentifier(){
			return 'ucdenver.goabroad.net';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniProdUCDENVER());

?>