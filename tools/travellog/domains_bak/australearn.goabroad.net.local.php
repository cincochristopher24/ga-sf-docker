<?php

	require_once('Class.Initialization.php');
	require_once('Class.IniConfig.php');

	class IniLocalAUSTRALEARN implements Initialization {

		private $initializedValues = array (
			'GMapKey' => '',
			'Site'	=>	'AUSTRALEARN'
		);

		public function getIdentifier(){
			return 'australearn.goabroad.net.local';
		}

		public function getAvailableInitializationValues(){
			return $this->initializedValues;
		}
	}

	IniConfig::register(new IniLocalAUSTRALEARN());

?>