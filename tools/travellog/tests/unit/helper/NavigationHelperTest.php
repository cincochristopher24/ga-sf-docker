<?php
/**
 * Created on Jul 25, 2007
 * 
 * @author     Wayne Duran
 */

include_once('../../config.php');
// The following are workarounds
$_SERVER['SERVER_NAME'] = 'goabroad.net.local';
$_SERVER['HTTP_HOST'] = 'goabroad.net.local';
@ require_once(PATH_TO_HELPERS.'Class.NavigationHelper.php');

class NavigationHelperTest extends PHPUnit_Framework_TestCase {
	
	protected function setUp() {
		NavigationHelper::resetLinks();
	}
	
	protected function setUpFixture()
	{
		// Setup test data
		NavigationHelper::setLink('A', array('text'=>'a_text', 'url'=>'a.php', 'id'=>'a_id'));
		NavigationHelper::setLink('B', array('text'=>'b_text', 'url'=>'b.php', 'id'=>'b_id'));
		NavigationHelper::setLink('C', array('text'=>'c_text', 'url'=>'c.php', 'id'=>'c_id'));
	}
	
	public function testBasicSettingOfLink()
	{
		$name    = 'Name_of_Link';
		$url     = 'url_link.php';
		$text    = 'Text to appear in between anchor tags';
		$list_id = 'list_id';
		NavigationHelper::setLink($name, array('text' => $text, 'url'=> $url, 'id'=> $list_id));
		
		$link = NavigationHelper::getLink($name);
		
		$this->assertEquals($url, $link['url'], 'URL in link did not match test');
		$this->assertEquals($text, $link['text'], 'Text in link did not match test');
		$this->assertEquals($list_id, $link['id'], 'Id in link did not match test');
		
	}
	
	public function testAnotherBasicSettingOfLink()
	{
		$name           = 'Name_of_another_Link';
		$test_link_data = array(
			'url'     => 'another_url_link.php',
			'text'    => 'Another text to appear in between anchor tags',
			'id' => 'another_list_id'
		);
		NavigationHelper::setLink($name, $test_link_data);
		
		$link = NavigationHelper::getLink($name);
		
		$this->assertEquals($test_link_data['url'], $link['url'], 'URL in link did not match test');
		$this->assertEquals($test_link_data['text'], $link['text'], 'Text in link did not match test');
		$this->assertEquals($test_link_data['id'], $link['id'], 'Id in link did not match test');
	}
	
	public function testGettingLinkInWithAnUknownIndexMustReturnFalse()
	{
		$this->setUpFixture();
		$this->assertFalse(NavigationHelper::getLink('D'), 'Non-existent key for link must return false');
		
	}
	
	public function testSetUpFixture()
	{
		$this->setUpFixture();
		$alink = NavigationHelper::getLink('A');
		$blink = NavigationHelper::getLink('B');
		$clink = NavigationHelper::getLink('C');
		
		
		$this->assertEquals('array', gettype($alink), 'Link returned must be an Array');
		$this->assertEquals('a_text', $alink['text'], 'Fixture was not properly set');
	}
	
	public function testResettingLinks()
	{
		$this->setUpFixture();
		NavigationHelper::resetLinks();
		$this->assertFalse(is_array(NavigationHelper::getLink('A')), 'There must be no link returned');
	}
	
	public function testDefaultNavigationLinks() {
		
		NavigationHelper::setDefaultNavigation();
		$home = NavigationHelper::getLink('Home');
		$passport = NavigationHelper::getLink('My Passport');
		$travelers = NavigationHelper::getLink('Travelers');
		$journals = NavigationHelper::getLink('Journals');
		$groups = NavigationHelper::getLink('Groups / Clubs');
		$desti  = NavigationHelper::getLink('Destinations');
		$forums = NavigationHelper::getLink('Forums');
		
		$this->assertEquals('Home', $home['text'], 'Home link text must be \'Home\'');
		$this->assertEquals('/', $home['url'], 'Home link url must be \'/\'');
		$this->assertEquals('nav_home', $home['id'], "Home link id must be 'nav_home'");
		
		$this->assertEquals('My Passport', $passport['text'], "Passport link text must be 'My Passport'");
		$this->assertEquals('/passport.php', $passport['url'], "Passport link url must be '/passport.php'");
		$this->assertEquals('nav_passport', $passport['id'], "Passport link id must be 'nav_passport'");
		
		$this->assertEquals('Travelers', $travelers['text'], "Travelers link text must be 'Travelers'");
		$this->assertEquals('/travelers.php', $travelers['url'], "Travelers link url must be '/travelers.php'");
		$this->assertEquals('nav_travelers', $travelers['id'], "Travelers link id must be 'nav_travelers'");
		
		$this->assertEquals('Journals', $journals['text'], "Journals link text must be 'Journals'");
		$this->assertEquals('/journal.php', $journals['url'], "Journals link url must be '/journal.php'");
		$this->assertEquals('nav_journals', $journals['id'], "Journals link id must be 'nav_journals'");
		
		$this->assertEquals('Groups / Clubs', $groups['text'], "Groups / Clubs link text must be 'Groups / Clubs'");
		$this->assertEquals('/group.php', $groups['url'], "Groups / Clubs link url must be '/group.php'");
		$this->assertEquals('nav_groups', $groups['id'], "Groups / Clubs link id must be 'nav_groups'");
		
		$this->assertEquals('Destinations', $desti['text'], "Destinations link text must be 'Destinations'");
		$this->assertEquals('/destination.php', $desti['url'], "Destinations link url must be '/destination.php'");
		$this->assertEquals('nav_destinations', $desti['id'], "Destinations link id must be 'nav_destinations'");
		
		$this->assertEquals('Forums', $forums['text'], "Forums link text must be 'Forums'");
		$this->assertEquals('/forums/index.php', $forums['url'], "Forums link url must be '/forums/index.php'");
		$this->assertEquals('nav_forums', $forums['id'], "Forums link id must be 'nav_forums'");
	}
	
	public function testCoBrandNavigationLinks() {
		
		NavigationHelper::setCoBrandNavigation();
		$home = NavigationHelper::getLink('Home');
		$passport = NavigationHelper::getLink('My Passport');
		$travelers = NavigationHelper::getLink('Travelers');
		$journals = NavigationHelper::getLink('Journals');
		$groups = NavigationHelper::getLink('Groups / Clubs');
		
		$this->assertEquals('Community', $home['text'], 'Home link text must be \'Community\'');
		$this->assertEquals('/', $home['url'], 'Home link url must be \'/\'');
		$this->assertEquals('nav_home', $home['id'], "Home link id must be 'nav_home'");
		
		$this->assertEquals('My Passport', $passport['text'], "Passport link text must be 'My Passport'");
		$this->assertEquals('/passport.php', $passport['url'], "Passport link url must be '/passport.php'");
		$this->assertEquals('nav_passport', $passport['id'], "Passport link id must be 'nav_passport'");
		
		$this->assertEquals('Students', $travelers['text'], "Travelers link text must be 'Students'");
		$this->assertEquals('/travelers.php', $travelers['url'], "Travelers link url must be '/travelers.php'");
		$this->assertEquals('nav_travelers', $travelers['id'], "Travelers link id must be 'nav_travelers'");
		
		$this->assertEquals('Journals', $journals['text'], "Journals link text must be 'Journals'");
		$this->assertEquals('/journal.php', $journals['url'], "Journals link url must be '/journal.php'");
		$this->assertEquals('nav_journals', $journals['id'], "Journals link id must be 'nav_journals'");
		
		$this->assertEquals('Groups', $groups['text'], "Groups / Clubs link text must be 'Groups'");
		$this->assertEquals('/subgroups.php', $groups['url'], "Groups / Clubs link url must be '/group.php'");
		$this->assertEquals('nav_groups', $groups['id'], "Groups / Clubs link id must be 'nav_groups'");
		
		$this->assertEquals(5, count(NavigationHelper::getLinks()), 'The total number of links must be 5');
		/**/
	}
	
	public function testGettingAllLinks()
	{
		$this->setUpFixture();
		$links = NavigationHelper::getLinks();
		
		$this->assertEquals('array', gettype($links), 'Links must be a collection/array of links');
		$this->assertEquals(3, count($links), 'Links must contain 3 items');
		$this->assertEquals('a_text', $links['A']['text'], 'Did not find expected link');
		
	}
	
	public function testLinksMustBeInOrder()
	{
		$this->setUpFixture();
		$links = NavigationHelper::getLinks();
		$a = current($links);
		$b = next($links);
		$c = next($links);
		
		$this->assertEquals('a_text', $a['text'], 'Unexpected value for the first item');
		$this->assertEquals('b_text', $b['text'], 'Unexpected value for the first item');
		$this->assertEquals('c_text', $c['text'], 'Unexpected value for the first item');
	}
	
	public function testDisplay()
	{
		$this->setUpFixture();
		$output = '<li id="a_id"><a href="a.php"><span>a_text</span></a></li>'.
		          '<li id="b_id"><a href="b.php"><span>b_text</span></a></li>'.
		          '<li id="c_id"><a href="c.php"><span>c_text</span></a></li>';
		
		$this->assertEquals($output, NavigationHelper::displayMainNavigation(), 'Output did not match expectation');
	}
	
	public function testDisplayWithSpecifyingAnActiveLink()
	{
		$this->setUpFixture();
		$output = '<li id="a_id"><a href="a.php"><span>a_text</span></a></li>'.
		          '<li id="b_id" class="active"><a href="b.php"><span>b_text</span></a></li>'.
		          '<li id="c_id"><a href="c.php"><span>c_text</span></a></li>';
		
		$this->assertEquals($output, NavigationHelper::displayMainNavigation('B'), 'Output did not match expectation');
	}
	
	public function testDisplayAlternativeFunction()
	{
		$this->setUpFixture();
		$output = '<li id="a_id"><a href="a.php"><span>a_text</span></a></li>'.
		          '<li id="b_id"><a href="b.php"><span>b_text</span></a></li>'.
		          '<li id="c_id"><a href="c.php"><span>c_text</span></a></li>';
		$this->assertEquals($output, NavigationHelper::display(), 'Output did not match expectation');
	}
	
	public function testDisplayingWithoutSpecifyingAnyLinksShouldDefaultToTheDefaultNavigationLinks()
	{
		$output = '<li id="nav_home"><a href="/"><span>Home</span></a></li>'.
		          '<li id="nav_passport"><a href="/passport.php"><span>My Passport</span></a></li>'.
		          '<li id="nav_travelers"><a href="/travelers.php"><span>Travelers</span></a></li>'.
		          '<li id="nav_journals"><a href="/journal.php"><span>Journals</span></a></li>'.
		          '<li id="nav_groups"><a href="/group.php"><span>Groups / Clubs</span></a></li>'.
		          '<li id="nav_destinations"><a href="/destination.php"><span>Destinations</span></a></li>'.
		          '<li id="nav_forums"><a href="/forums/index.php"><span>Forums</span></a></li>';
		
		$this->assertEquals($output, NavigationHelper::display(), 'Output did not match expectation');
	}
	
	public function testRemovingALink()
	{
		$this->setUpFixture();
		$output = '<li id="b_id"><a href="b.php"><span>b_text</span></a></li>'.
		          '<li id="c_id"><a href="c.php"><span>c_text</span></a></li>';
		NavigationHelper::removeLink('A');
		$this->assertEquals($output, NavigationHelper::displayMainNavigation(), 'Output did not match expectation');
	}
	
	public function testSettingAnExistingLinkWithADifferentProperty() {
		$this->setUpFixture();
		NavigationHelper::setLink('A', array('url' => 'a_different_url.php'));
		$a_link = NavigationHelper::getLink('A');
		$this->assertEquals('a_text', $a_link['text'], 'Text should stay the same');
		$this->assertEquals('a_different_url.php', $a_link['url'], 'URL property should be different');
	}
	
	public function testChangingLinkPropertyUsingCoBrandNavigation() {
		NavigationHelper::setCoBrandNavigation();
		NavigationHelper::setLink('Travelers', array('text' => 'Volunteers'));
		$link = NavigationHelper::getLink('Travelers');
		$this->assertEquals('Volunteers', $link['text'], 'Text must be "Volunteers"');
	}
	
	public function testCoBrandNavigation() {
		NavigationHelper::setCoBrandNavigation();
		$output = '<li id="nav_home"><a href="/"><span>Community</span></a></li>'.
		          '<li id="nav_passport"><a href="/passport.php"><span>My Passport</span></a></li>'.
		          '<li id="nav_travelers"><a href="/travelers.php"><span>Students</span></a></li>'.
		          '<li id="nav_journals"><a href="/journal.php"><span>Journals</span></a></li>'.
		          '<li id="nav_groups"><a href="/subgroups.php"><span>Groups</span></a></li>';

		$this->assertEquals($output, NavigationHelper::display(), 'Output did not match expectation');
	}
	
	
	public function testCoBrandNavigationWithCustomTextForTravelersLink() {
		NavigationHelper::setLink('Travelers', array('text' => 'Volunteers'));
		NavigationHelper::setCoBrandNavigation();
		$output = '<li id="nav_home"><a href="/"><span>Community</span></a></li>'.
		          '<li id="nav_passport"><a href="/passport.php"><span>My Passport</span></a></li>'.
		          '<li id="nav_travelers"><a href="/travelers.php"><span>Volunteers</span></a></li>'.
		          '<li id="nav_journals"><a href="/journal.php"><span>Journals</span></a></li>'.
		          '<li id="nav_groups"><a href="/subgroups.php"><span>Groups</span></a></li>';

		$this->assertEquals($output, NavigationHelper::display(), 'Output did not match expectation');
	}
	
	public function testIfAdvisorRemoveMyPassportLink()
	{
		$globalhelper = $this->getMock('HelperGlobal', array('isAdvisor', 'getGroup', 'isGroupAdvisor'));
		// var_dump($globalhelper);
		/*$globalhelper->isAdvisor();*/
		// print get_class($globalhelper);
		$this->markTestIncomplete('Need to figure out how to implement this test');
	}
	
	
}
?>
