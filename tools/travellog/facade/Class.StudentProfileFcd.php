<?php
	/*
	 * Class.StudentProfileFcd.php
	 * Created on Nov 29, 2007
	 * created by marc
	 */
	
	require_once("Class.Constants.php");
	 
	 class StudentProfileFcd{
	 	
	 	private
	
		$props          = array(),
		        
		$view_factory   = array(),
		
		$models_factory = array();
		
		function __construct( $props ){
			require_once('travellog/views/Class.StudentProfileViewsFactory.php');
			$this->view_factory  = StudentProfileViewsFactory::getInstance();
			$this->props         = $props;
		}
		
		function retrieveStudentProfilePage(){
			$obj_view = $this->view_factory->createView(constants::VIEW_STUDENT_PROFILE_PAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->addSubView( $this->retrieveReferedListings(), 'REFERED_LISTINGS');
			$obj_view->addSubView( $this->retrieveStudentPreferences(), 'STUDENT_PREFERENCE');
			$obj_view->addSubView( $this->retrieveCountryPreferences(),	'COUNTRY_PREFERENCE');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
		function retrieveReferedListings(){
			$obj_view = $this->view_factory->createView(constants::VIEW_REFERED_LISTINGS);
			$obj_view->setPath('travellog/views/');
			$obj_view->addSubView( $this->retrievePaging(),	'PAGING');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveStudentPreferences(){
			$obj_view = $this->view_factory->createView(constants::VIEW_STUDENT_PREFERENCE);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveCountryPreferences(){
			$obj_view = $this->view_factory->createView(constants::VIEW_COUNTRY_PREFERENCE);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrievePaging(){
			$obj_view = $this->view_factory->createView(constants::PAGING);
			$obj_view->setPath('travellog/views/travelers/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveStudentPreferenceForm(){
			$obj_view = $this->view_factory->createView(constants::VIEW_STUDENT_PREFERENCE_FORM);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveStudentProfileForm(){
			$obj_view = $this->view_factory->createView(constants::VIEW_STUDENT_PROFILE_FORM);
			$obj_view->addSubView( $this->retrieveStudentPreferenceForm(),	'STUDENT_PREFERENCE');
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveStudentCommunications(){
			$obj_view = $this->view_factory->createView(constants::VIEW_STUDENT_COMMUNICATIONS);
			$obj_view->setPath('travellog/views/');
			$obj_view->addSubView( $this->retrievePaging(),	'PAGING');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveListing(){
			$obj_view = $this->view_factory->createView(constants::VIEW_LISTING);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveClient(){
			$obj_view = $this->view_factory->createView(constants::VIEW_CLIENT);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
	 } 
?>
