<?php
	/*
	 * Class.AdvisorInquiriesFcd.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
	 
	 require_once("Class.Constants.php");
	 
	 class AdvisorInquiriesFcd{
	 	
	 	private
			$props          = array(),
			$view_factory   = array();
		
		function __construct( $props ){
			require_once('travellog/views/Class.AdvisorInquiriesViewsFactory.php');
			$this->view_factory  = AdvisorInquiriesViewsFactory::getInstance();
			$this->props         = $props;
		}
		
		function retrieveStudentsList(){
			$obj_view = $this->view_factory->createView(constants::VIEW_STUDENTS_LIST);
			$obj_view->addSubView( $this->retrievePaging(),	'PAGING');
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
		function retrieveListingsList(){
			$obj_view = $this->view_factory->createView(constants::VIEW_LISTINGS_LIST);
			$obj_view->addSubView( $this->retrievePaging(),	'PAGING');
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
		function retrievePaging(){
			$obj_view = $this->view_factory->createView(constants::PAGING);
			$obj_view->setPath('travellog/views/travelers/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
		function retrieveMessageList(){
			$obj_view = $this->view_factory->createView(constants::VIEW_MESSAGE_LIST);
			if( constants::VIEW_BY_STUDENT == $this->props['viewMode'] )
				$obj_view->addSubView( $this->retrieveStudentsList(),	'MESSAGE_LIST');
			else
				$obj_view->addSubView( $this->retrieveListingsList(),	'MESSAGE_LIST');
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents($this->props);
			return $obj_view;
		}
		
		function retrieveMessagesPerStudentAndListing(){
			$obj_view = $this->view_factory->createView(constants::VIEW_MESSAGE_PER_STUDENT_AND_LISTING);
			$obj_view->setPath('travellog/views/travelers/');
			$obj_view->setContents( $this->props);
			return $obj_view;
		}
		
	 } 
?>