<?php

	require_once("Class.Template.php");
	require_once("travellog/model/Class.Article.php");

	class ArticleFcd{
	
		private $articleID = null;
		private $enableControls = null;
		
		function setArticleID($id){
			$this->articleID = $id;
		}
		
		// other articles by author and related articles 
		function getOtherArticlesByAuthor(){
			$obj_article = new Article($this->articleID);
			return $obj_article->getOtherArticlesByAuthor($obj_article->getAuthorID());
		}
		
		function getOtherArticlesByGroup(){
			$obj_article = new Article($this->articleID);
			return $obj_article->getOtherArticlesByGroup($obj_article->getGroupID());
		}
		
		function getRelatedArticles(){
			$obj_article = new Article($this->articleID);
			if(isset($GLOBALS['CONFIG']))
				return $obj_article->getRelatedArticlesByGroup($GLOBALS['CONFIG']->getGroupID());
			return $obj_article->getRelatedArticles();
		}
		
		function retrieveOtherArticles($arr_filter){
			require_once("travellog/views/Class.OtherArticlesView.php");
			$obj_views = new OtherArticlesView();
			
			
			$arr_contents = array();  
			if(isset($arr_filter['group_id'])){
				$arr_contents["group_articles"] = $this->getOtherArticlesByGroup();
				$arr_contents["authors_articles"] = array();
			}else{
				$arr_contents["authors_articles"] = $this->getOtherArticlesByAuthor();
				$arr_contents["group_articles"] = array();
			}	
			$arr_contents["related_articles"] = $this->getRelatedArticles();
			$arr_contents["limit"] = 5;
			
			$obj_views->setContents( $arr_contents );
			return $obj_views;
		}
		
		// map of article locations
		function retrieveMapByArticle(){
			require_once("travellog/model/Class.MapModel.php");
			
			$obj_model = new MapModel;
			
			$obj_article = new Article($this->articleID);
			
			return $obj_model->GetArticleMap( array($obj_article) );
		}

		function retrieveArticleMap(){
			require_once("travellog/views/Class.MapView.php");
			$obj_views = new MapView;
			
			$arr_contents = array();  
			$arr_contents['arr_marker']  = $this->retrieveMapByArticle();
			$arr_contents['travellogID'] = 1;
			$obj_views->setContents( $arr_contents );

			return $obj_views;
		}

		// ads
		function retrieveAds(){
			require_once("travellog/facade/Class.AdsFcd.php");
			$ads_fcd   = new AdsFcd;
			
			return $ads_fcd->retrieveEntryAdsView();
		}
		
		// primary photo
		function retrievePrimaryPhoto( $arr_filter ){ // adapting travellogs
			$obj_article = new Article($this->articleID);
			
			require_once("travellog/views/Class.EntryPrimaryPhotoView.php");
			$obj_view                            = new EntryPrimaryPhotoView;
			
			$arr_contents                        = array();   
			$arr_contents['show_admin_controls'] = $this->enableControls(); 
			$arr_contents['obj_entry']           = $obj_article;
			if( isset($arr_filter['group_id']) )
				$arr_contents['group_id']        = $arr_filter['group_id'];
			$arr_contents['traveler_id']         = $arr_filter['travelerID'];
			$obj_view->setContents( $arr_contents );

			return $obj_view;
		}
		
		// thumbnail photos
		function retrievePhotoThumbGrid( $arr_filter ){
			require_once("travellog/UIComponent/NewCollection/views/Class.ViewThumbGrid.php");
			$request = array(	"loginid"	=>	$arr_filter["travelerID"],
								"genid"		=>	$arr_filter["articleID"],
								"ownerid"	=>	$arr_filter["authorID"],
								"context"   =>  "article",
								"travelerid"=>	$arr_filter["travelerID"]	);
			if( isset($arr_filter["group_id"]) ){
				$request["group_id"] = $arr_filter["group_id"];
			}
			return new ViewThumbGrid($request);
		}
		
		// comments
		function retrieveArticleComments( $arr_filter ){			
			$file_factory = FileFactory::getInstance();
			$obj_comments = $file_factory->getClass('CommentsView'); 
			$obj_comments->setContext(4, $arr_filter['articleID']);
			$obj_comments->setTravelerID( $arr_filter['travelerID'] );
			$obj_comments->setEnvURL( $_SERVER['REQUEST_URI']."^comment_form" );
			$obj_comments->readComments();

			return $obj_comments;   
		}
		
		//retrive bookmarks control
		function retrieveBookmarkControls(){
			require_once("travellog/views/Class.BookmarkControlsView.php");
			$obj_view = new BookmarkControlsView();
			
			$obj_article = new Article($this->articleID);
			
			$group 		= ($obj_article->getGroupID() != 0) ? "&gID=".$obj_article->getGroupID() : "";
			$permalink 	= "http://".$_SERVER['SERVER_NAME']."/article.php?action=view&articleID=".$obj_article->getArticleID().$group;
			$title 		= $obj_article->getTitle();
			
			$arr_contents = array(
				'permalink' => $permalink,
				'title'		=> $title
			);
			
			$obj_view->setContents($arr_contents);
			$obj_view->prepareURLParameters();
			return $obj_view;
		}
		
		//retrieve FB Like button control
		function retrieveFBLikeControl(){
			require_once("travellog/views/Class.FBLikeControlView.php");
			$obj_view = new FBLikeControlView();
			
			$obj_article = new Article($this->articleID);
			
			require_once('Class.Template.php');
			$siteContext = SiteContext::getInstance();
			$site_name = isset($GLOBALS["CONFIG"]) ? $siteContext->getSiteName()." Network" : "GoAbroad Network";
			$primaryPhoto = $obj_article->getPrimaryPhoto();
			
			// check if article has primary photo else use dotnet footer logo unless if article 
			// is in cobrand then leave it blank to show no photo

			$dotnetlogo = 'http://www.goabroad.net/images/footerdotnetlogo.gif';
			$primaryPhotoLink = (($obj_article->getPrimaryPhotoID() > 0) && $primaryPhoto instanceof Photo) ? $primaryPhoto->getPhotoLink("featured") : $dotnetlogo;
			$primaryPhotoLink = ($primaryPhotoLink == $dotnetlogo && $siteContext->isInCobrand()) ? '' : $primaryPhotoLink;
				
			$fbMetaTags = array(	'<meta name="title" content="'.$obj_article->getTitle().'"/>',
									'<meta property="og:title" content="'.$obj_article->getTitle().'"/>',
									'<meta property="og:site_name" content="'.$site_name.'"/>',
									'<meta property="og:image" content="'.$primaryPhotoLink.'"/>',
									'<link rel="image_src" href="'.$primaryPhotoLink.'" />');
									
			Template::clearFBRequiredMetaTags();
			Template::hideProfileFBLike();
			Template::setFBRequiredMetaTags($fbMetaTags);
			
			$group 		= ($obj_article->getGroupID() != 0) ? "&gID=".$obj_article->getGroupID() : "";
			$permalink 	= "http://".$_SERVER['HTTP_HOST']."/article.php?action=view&articleID=".$obj_article->getArticleID().$group;
			$title 		= $obj_article->getTitle();
			
			$arr_contents = array(
				'permalink' => $permalink,
				'title'		=> $title,
				'name'		=> 'article'
			);
			
			$obj_view->setContents($arr_contents);
			return $obj_view;
		}
		
		function retrieveArticlePhotoDescriptionCallout($arr_filter){
			require_once("travellog/views/Class.ArticlePhotoDescriptionCalloutView.php");
			$obj_view = new ArticlePhotoDescriptionCalloutView();
			$obj_article = new Article($this->articleID);
			
			require_once("Class.ColumnsUtility.php");
			$colsutil = ColumnsUtility::createInstance();
			$colsutil->setDefaults(array("content" => $obj_article->getDescription()));
			
			$arr_contents = array(
				"article" => $obj_article,
				"desc" => array(),//$colsutil->divideContentToColumns(),
				"enable_ctrls" => $this->enableControls()
			);
			$obj_view->setContents($arr_contents); 
			$obj_view->addSubView($this->retrievePrimaryPhoto($arr_filter),'primary_photo');
			
			return $obj_view;
		}
		
		// controls
		function enableControls(){
			$obj_session = SessionManager::getInstance();
			$obj_article = new Article($this->articleID);
			$owner = $obj_article->getOwner();
			
			if( $obj_session->isSiteAdmin() )
				return true;
			if( $owner instanceOf Traveler )
				return ($obj_article->getAuthorID() == $obj_session->get('travelerID')) ? true : false;
			return (($owner->getAdministratorID() == $obj_session->get('travelerID')) || $owner->isStaff($obj_session->get('travelerID'))) ? true : false;	
		}
		
		// retrieve the article as a whole
		function retrieveArticle($arr_filter){
			require_once("travellog/views/Class.ArticleView.php");
			$obj_views = new ArticleView();
			
			$obj_article = new Article($this->articleID);
			
			$arr_contents = array(
				"article"		=> $obj_article,
				"tags"			=> $arr_filter['tags'],
				"profile_view"	=> $arr_filter['profile'],
				"sub_navigation"=> $arr_filter['subnav'],
				"enable_ctrls" => $this->enableControls(),
				"thumbnail_view"=> $this->retrievePhotoThumbGrid($arr_filter),
				"comments_view" => $this->retrieveArticleComments($arr_filter)
			);
			
			$obj_views->addSubView($this->retrieveOtherArticles($arr_filter),'other_articles');
			$obj_views->addSubView($this->retrieveArticleMap(),'article_map');
			$obj_views->addSubView($this->retrieveArticlePhotoDescriptionCallout($arr_filter),'photo_desc_callout');
			$obj_views->addSubView($this->retrieveAds(),'ads_view');
			$obj_views->addSubView($this->retrieveBookmarkControls(),'bookmark_controls');
			$obj_views->addSubView($this->retrieveFBLikeControl(),'fb_like_control');
			
			$obj_views->setContents( $arr_contents );
			
			return $obj_views;
		}
	}