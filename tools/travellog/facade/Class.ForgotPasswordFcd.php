<?php
	/*
	 * Class.ForgotPasswordFcd.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	
	require_once("Class.Constants.php");
	 
	class ForgotPasswordFcd{
	 	
	 	private
	
		$props          = array(),
		        
		$view_factory   = array(),
		
		$models_factory = array();
		
		function __construct( $props ){
			require_once('travellog/views/Class.ForgotPasswordViewsFactory.php');
			$this->view_factory  = ForgotPasswordViewsFactory::getInstance();
			$this->props         = $props;
		}
		
		function retrieveForgotPasswordFormPage(){
			$obj_view = $this->view_factory->createView(constants::VIEW_FORGOT_PASSWORD_FORM_PAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
		function retrieveForgotPasswordConfirmationPAge(){
			$obj_view = $this->view_factory->createView(constants::VIEW_FORGOT_PASSWORD_CONFIRMATION_PAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
	 }	
?>