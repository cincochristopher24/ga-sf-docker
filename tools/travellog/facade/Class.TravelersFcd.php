<?php
	require_once("travellog/model/travelers/Class.TravelersDataHandler.php");
	
class TravelersFcd{
	
	private
	
	$props          = array(),
	        
	$view_factory   = array(),
	
	$mTravelersOtherInfoView = null,
	
	$models_factory = array();
	
	function __construct( $props ){
		require_once('travellog/model/travelers/Class.TravelersModelFactory.php');
		require_once('travellog/views/travelers/Class.TravelersViewFactory.php');
		$this->model_factory = TravelersModelFactory::getInstance();
		$this->view_factory  = TravelersViewFactory::getInstance();
		$this->props         = $props;
	}
	
	function retrieve( $travelerID = 0 ){
		$this->props['obj_traveler'] = $this->model_factory->createModel('traveler');
		
		if( $travelerID ) $this->props['obj_traveler']->setTravelerID( $travelerID ); 
		
		return $this->props['obj_traveler'];
	}
	
	function retrieveByContext(){
		(0 < $this->props['locationID']) ? $this->retrieveLocationIDListBaseOnCountryLocationID() : "";
		$this->props['obj_model'] = $this->model_factory->createModel($this->props['type']);
		$this->props['obj_model']->GetFilterLists($this->props);
		
		$traveler_ids = "";
		foreach ($this->props['travelers'] as $traveler) {
			$traveler_ids .= $traveler->getTravelerID().", ";
		}
		$traveler_ids = preg_replace('/, $/', '', $traveler_ids);
		
		$this->props['journals_count'] = TravelersDataHandler::getJournalEntriesCount($traveler_ids);
		if( isset($GLOBALS["CONFIG"]) ){
			$this->props['videos_count'] = TravelersDataHandler::getTotalCountOfVideos($traveler_ids,$GLOBALS["CONFIG"]->getGroupID());
			$this->props['photos_count'] = TravelersDataHandler::getTotalCountOfPhotos($traveler_ids,$GLOBALS["CONFIG"]->getGroupID());	
		}else{
			$this->props['videos_count'] = TravelersDataHandler::getTotalCountOfVideos($traveler_ids);
			$this->props['photos_count'] = TravelersDataHandler::getTotalCountOfPhotos($traveler_ids);
		}
		$this->props['primary_photos'] = TravelersDataHandler::getPrimaryPhotos($traveler_ids);
	}
	
	function retrieveByContextView(){
		
		$obj_view = $this->view_factory->createView('TravelersView');
		
		$obj_view->addSubView( $this->retrieveListView()      , 'LISTS_VIEW'       );
		$obj_view->addSubView( $this->retrieveSearchFormView(), 'SEARCH_FORM_VIEW' );
		//$obj_view->addSubView( $this->retrieveMapView()       , 'MAP_VIEW'         );
		
		$obj_view->setPath('travellog/views/travelers/');
		
		$obj_view->setContents( $this->props );
		
		return $obj_view; 
	}
	/*
	function retrieveMapView(){
		$obj_view = $this->view_factory->createView('MapView');
		$this->model_factory->createModel('TravelersMap')->GetTravelerMap($this->props);
		
		$obj_view->setPath('travellog/views/travelers/');
		
		$this->props['obj_map']       = $this->model_factory->createModel('Map');
		$this->props['obj_map_frame'] = $this->model_factory->createModel('MapFrame');
		
		$obj_view->setContents( $this->props );
		
		return $obj_view;
	}
  */
	function retrieveLocationIDListBaseOnCountryLocationID(){
		if( !array_key_exists('locationIDList', $this->props) ){
			$this->model_factory->createModel('LocationIDList');
			$this->model_factory->createModel('LocationIDList')->GetFilterList($this->props);
		}
	}
	
	function retrieveNumberOfCountryPhotos(){
		if(!isset($this->props['locationIDList'])) {
			$this->retrieveLocationIDListBaseOnCountryLocationID();
		}
		$this->model_factory->createModel('CountryPhotosCount')->GetFilterList($this->props);
	}
	
	function retrieveNumberOfTravelerInLocation(){
		if(!isset($this->props['locationIDList'])) {
			$this->retrieveLocationIDListBaseOnCountryLocationID();
		}
		$this->model_factory->createModel('TravelerLocationCount')->GetFilterList($this->props);
	}
	
	function retrieveNumberOfEntriesWriten(){
		if(!isset($this->props['locationIDList'])) {
			$this->retrieveLocationIDListBaseOnCountryLocationID();
		}
		$this->model_factory->createModel('EntriesWritenCount')->GetFilterList($this->props);
	}
		
	function retrieveTravelersOtherInfoView(){
		if (is_null($this->mTravelersOtherInfoView)) {
			if( !array_key_exists('obj_location', $this->props)) 
				$this->retrieveLocation();
			
			$this->mTravelersOtherInfoView = $this->view_factory->createView('OtherInfoView');
			
			$this->retrieveNumberOfCountryPhotos();
			$this->retrieveNumberOfTravelerInLocation();
			$this->retrieveNumberOfEntriesWriten();
			
			$this->mTravelersOtherInfoView->setPath('travellog/views/travelers/');
			
			$this->mTravelersOtherInfoView->setContents( $this->props );
		}
		return $this->mTravelersOtherInfoView;
	}
	
	function retrieveLocation(){
		if(array_key_exists('locationID', $this->props) && $this->props['locationID'] && !isset($this->props['obj_location'])){
			require_once('travellog/model/Class.LocationFactory.php');
			$obj_factory                 = LocationFactory::instance();
			$this->props['obj_location'] = $obj_factory->create($this->props['locationID']); 
		}
	}
	
	function retrieveSearchFormView(){
		$obj_view = $this->view_factory->createView('SearchFormView');
		$obj_view->setPath('travellog/views/travelers/');
		$obj_view->setContents( $this->props );
				
		return $obj_view;
	}
	
	function retrieveLocationMessageView(){
		$obj_view = $this->view_factory->createView('LocationMessageView');
		$obj_view->addSubView( $this->retrieveTravelersOtherInfoView(), 'OTHER_INFO_VIEW' );
		$obj_view->setPath('travellog/views/travelers/');
		$this->retrieveLocation();
		$obj_view->setContents( $this->props );
		
		return $obj_view;
	}
	
	function retrieveListView(){
		$this->retrieveByContext();
		$obj_view = $this->view_factory->createView('ListsView');
		$obj_view->setPath('travellog/views/travelers/');
		
		$obj_view->setContents( $this->props );
		if (0 < $this->props['locationID']) {
			$obj_view->addSubView( $this->retrieveLocationMessageView(), 'LOCATION_MESSAGE_VIEW' );
		}
		$obj_view->addSubView( $this->retrievePagingListView()     , 'PAGING_LIST_VIEW'      );
		
		return $obj_view;
	}
	
	function retrievePagingListView(){
		$obj_view = $this->view_factory->createView('PagingListsView');
		$obj_view->setPath('travellog/views/travelers/');
		
		$obj_view->setContents( $this->props );
		
		return $obj_view;
	}
}
?>
