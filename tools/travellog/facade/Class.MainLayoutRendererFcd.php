<?php
//require_once("Application.php");
require_once("travellog/views/Class.MainLayoutView.php");
class MainLayoutRendererFcd{
	function retrieveMainEntryView( $arr_vars ){
		require_once("travellog/facade/Class.JournalManagementFcd.php");
		require_once("travellog/model/Class.Map.php");
  		
		$fcd         = new JournalManagementFcd;  
		$obj_map     = new Map;
		$obj_views   = new MainLayoutView;
		$arr_entries = array();
		$obj_journal = $fcd->retrieve( $arr_vars['travelID'] );
				
		
		if( $arr_vars['travellogID'] == 0 ){
			$arr_entries = $fcd->getEntries( $arr_vars );
			if( count($arr_entries) ){ 
				$arr_vars['travellogID'] = $arr_entries[0]->getTravelLogID();
				$obj_entry               = $arr_entries[0];	
			}
		}
		else{
			$obj_entry   = $fcd->retrieveEntry( $arr_vars['travellogID'] );
			$arr_entries = $fcd->getEntries( $arr_vars );   
		}
		$pos          = $fcd->getCurrentPosition();
		$isAdmin      = ( $fcd->showAdminControls( $arr_vars ) )?1: 0;
		$photo_layout = ( $obj_entry->getTemplateType()== 0)? 4: 1; 
		$travelerID   = $arr_vars['travelerID'];
		$travellogID  = $arr_vars['travellogID'];
		
		ob_start();
			$obj_map->includeJs();
			$google_map_include = ob_get_contents(); 
		ob_end_clean(); 
		
		if( !$isAdmin ){
			require_once("travellog/model/Class.Travel.php");
			$obj_travel = new Travel($arr_vars['travelID']);
			$obj_travel->addView();
		} 
		
		 
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();
	travellogID = $travellogID;
	isAdmin     = $isAdmin; 
	currPos     = $pos;
	jQuery("#gallerycontainer").ready(function(){    
	    PhotoService = new mPhotoService('travellog', $travelerID, $travellogID);
		PhotoService.setLayout($photo_layout); 
		PhotoService.setisOwner(isAdmin); 
		PhotoService.loadgallery();
		Comment.initialize();
		Comment.setImagePath('/images/poke/');
		Comment.createCommentBox();		
		
		
		/*jQuery('jeditable-entry-title-link').click(function(){
			 jQuery.blockUI({ 
		        message: jQuery('#c'), 
		        css: { top: '20%' } 
		    });
		});*/
		
		/*jQuery("#jeditable-entry-title-link")
	    .jeditable({ 
	        url: '/ajaxpages/JournalEntry.php',
	        container: 'jeditable-entry-title',
	        querystring: 'mode=updateEntryTitle',
	        type: 'text', 
	        width: '500px'
	    });
		
		jQuery("#jeditable-entry-description-link")
	    .jeditable({ 
	        url: '/ajaxpages/JournalEntry.php',
	        container: 'jeditable-entry-description',
	        querystring: 'mode=updateEntryDescription',
	        type: 'textarea', 
	        width: '500px',
	        height: '400px' 
	    });
	    
	    jQuery("#jeditable-entry-callout-link")
	    .jeditable({ 
	        url: '/ajaxpages/JournalEntry.php',
	        querystring: 'mode=updateEntryCallout',
	        container: 'jeditable-entry-callout',  
	        afterUpdateCallback: changeLabel,
	        type: 'textarea',  
	        width: '300px',
	        height: '100px' 
	    });
	    
	    jQuery("#jeditable-journal-description-link")
	    .jeditable({ 
	        url: '/ajaxpages/JournalEntry.php',
	        querystring: 'mode=updateJournalDescription',
	        container: 'jeditable-journal-description',  
	        type: 'textarea',   
	        width: '400px',
	        height: '60px'  
	    });
	    
	    jQuery("#jeditable-journal-title-link")
	    .jeditable({ 
	        url: '/ajaxpages/JournalEntry.php',
	        querystring: 'mode=updateJournalTitle',
	        container: 'jeditable-journal-title',  
	        type: 'text',  
	        width: '400px'  
	    });*/
	});
	
	var changeLabel = function(){
		jQuery('#jeditable-entry-callout-link').html('<span>Edit Entry Callout</span>'); 
	}
		
	function ecollapse(t, pre, divId){
		for(i=1;i<t;i++){
			if(Element.getStyle(pre+i, 'display')=='none'){
				Effect.BlindDown(pre+i);
				$(divId).innerHTML = "Hide";
			}
			else{
				Effect.BlindUp(pre+i);
				$(divId).innerHTML = "View all";
			}	
		}
	}
//]]>  
</script>
<!--[if IE]><script type="text/javascript" src="jquery.bgiframe.js"></script><![endif]-->
BOF;
		
		
		$tmpJournalEntryTitlePhrase = $obj_entry->getTitle(). ' - '. $obj_journal->getTitle() .' - '. $obj_entry->getCity() .', '. $obj_entry->getCountry() . ' - GoAbroad Network';
		
		Template::includeDependent   ( $google_map_include                  );  
		
		Template::setMainTemplateVar ( 'title', $tmpJournalEntryTitlePhrase                             );
		Template::setMainTemplateVar ( 'metaDescription', substr($obj_journal->getDescription(), 0, 140));	
		Template::setMainTemplate    ( 'travellog/views/tpl.LayoutMain.php'                             );
		//Template::includeDependentJs ( "/lightwindow/javascript/scriptaculous.js?load=effects,builder,dragdrop"          );
		Template::includeDependentJs (
			array(
				'/js/scriptaculous/scriptaculous.js',
				'/js/scriptaculous/effects.js',
				'/js/scriptaculous/builder.js',
				'/js/scriptaculous/dragdrop.js'
			)
		);
		//Template::includeDependentJs ( "/lightwindow/javascript/lightwindow.js"                         );

		
		//Template::includeDependentJs ( "/js/jquery-1.1.4.pack.js"           );
		Template::includeDependentJs ( "/js/jquery.blockUI.js"              );
		Template::includeDependentCss( '/min/f=js/block.css'                      );
		
		Template::includeDependentCss('/js/imgcropper/css/cropper.css');
		
		Template::includeDependentJs("/js/imgcropper/lib/cropper.js");
		Template::includeDependentJs("/js/imgcropper/lib/init_cropper.js");
	
		
		Template::includeDependentJs ( "/js/interface/interface.js"	        );
		
		Template::includeDependentJs ( "/js/interface/source/idrag.js"	   	);
		Template::includeDependentJs ( "/js/interface/source/idrop.js"	    );
		Template::includeDependentJs ( "/js/interface/source/isortables.js" );
		//Template::includeDependentJs ( "/js/jcarousel-lite.js"              );
		Template::includeDependentCss( "/css/skins/ganet/skin.css"			); 
		Template::includeDependentCss( "/css/skins/smallalbum/skin.css"		); 
		Template::includeDependentCss( "/css/skins/ie7/skin.css"			); 
		Template::includeDependentJs ( "/js/entry_tableofcontents.js"       ); 
		Template::includeDependentJs ( "/js/ajaxfileupload.js"       		);  
		Template::includeDependentJs ( "/js/jeditable.js"          		    ); 
		Template::includeDependentJs ( "/js/PhotoUILayout.js"               );
		Template::includeDependentJs ( "/js/thickbox.js"                    );
		Template::includeDependentCss( "/min/f=css/Thickbox.css"                  ); 
		//Template::includeDependentCss( "/css/entry_tableofcontents.css"     ); 
		
		Template::includeDependentJs ( "/lytebox/lytebox.js"                );
		Template::includeDependentCss( "/lytebox/lytebox.css"                );
		
		
		//Template::includeDependentCss( "/lightwindow/css/lightwindow.css" 	);
		Template::includeDependentJs ( "/js/date.js"                        );
		Template::includeDependentJs ( "/js/jquery.datePicker.js"           );
		Template::includeDependentJs ( "/js/jquery.checkbox.js"             );
		Template::includeDependentJs ( "/js/entry_location.js"              );
		Template::includeDependent   ( $code                                );
		Template::includeDependentJs ( "/js/utils/json.js"                  );  
		Template::includeDependentJs ( "/js/entry_highlights.js"            ); 
		Template::includeDependentJs ( "/js/entry_tips.js"                  );
		// added by: cheryl ivy q. go    28 sept 2007 -- for greetings, salutations and comments
		Template::includeDependentJs("/js/utils/Class.Request.js"); 
		Template::includeDependentJs("/js/comments.js");
		Template::includeDependentCss( "/min/f=css/styles.css"   			        );
		// end
		
		if( $obj_entry->getTemplateType() == 0 )  
			Template::setMainTemplateVar('layoutID', 'journals');
		else
			Template::setMainTemplateVar('layoutID', 'journals');
	
		$arr_contents['contents'] = $fcd->retrieveEntryByFilterView( $arr_vars );
		
		$obj_views->setContents( $arr_contents );
		
		return $obj_views;
	}
	
	function permalink( $arr_vars ){
		require_once("travellog/facade/Class.JournalManagementFcd.php");
		$fcd = new JournalManagementFcd;
		return $fcd->permalink( $arr_vars );
	}
}
?>
