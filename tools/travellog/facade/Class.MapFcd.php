<?php
require_once("travellog/model/Class.MapModel.php");
require_once("travellog/views/Class.MapViews.php");
class MapFcd{
	private
	
	$model = NULL,
	
	$views = NULL;
	
	function __construct(){
		$this->views = new MapViews;
		$this->model = new MapModel;
	}
	
	/**
	 * @author Aldwin S. Sabornido
	 * @access public
	 * @param array travellogID, collection trips
	 * @return string  
	 */
	function retrieveByFilterView( $arr_filter ){
		$arr_contents['arr_marker'] = $this->model->Get( $arr_filter );
		$this->views->setContents( $arr_contents );
		return $this->views->render();
	} 
}
?>
