<?php
	/*
	 * Class.RegisterFcd.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	  require_once("Class.Constants.php");
	 
	 class RegisterFcd{
	 	
	 	private
	
		$props          = array(),
		        
		$view_factory   = array(),
		
		$models_factory = array();
		
		function __construct( $props ){
			require_once('travellog/views/Class.RegisterViewsFactory.php');
			$this->view_factory  = RegisterViewsFactory::getInstance();
			$this->props         = $props;
		}
		
		function retrieveChooseRegister(){
			$obj_view = $this->view_factory->createView();
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
		function retrieveTravelerRegistrationForm(){
			$obj_view = $this->view_factory->createView(constants::VIEW_TRAVELER_REGISTRATION_FORM);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveGroupRegistrationForm(){
			$obj_view = $this->view_factory->createView(constants::VIEW_GROUP_REGISTRATION_FORM);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveRegistrationDetails(){
			$obj_view = $this->view_factory->createView(constants::VIEW_REGISTRATION_DETAILS);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveWelcomePage(){
			$obj_view = $this->view_factory->createView(constants::VIEW_WELCOME_PAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
	 }
?>
