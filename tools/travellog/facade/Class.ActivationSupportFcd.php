<?php
	/*
	 * Class.ActivationSupportFcd.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
	 
	 require_once("Class.Constants.php");
	 
	 class ActivationSupportFcd{
	 	
	 	private
	
		$props          = array(),
		        
		$as_factory   = array(),
		
		$models_factory = array();
		
		function __construct( $props ){
			require_once('travellog/views/Class.ActivationSupportViewsFactory.php');
			$this->view_factory  = ActivationSupportViewsFactory::getInstance();
			$this->props         = $props;
		}
		
		function retrieveActivationSupportFormPage(){
			$obj_view = $this->view_factory->createView(constants::VIEW_ACTIVATION_SUPPORT_FORM_PAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
		function retrieveActivationSupportConfirmationPage(){
			$obj_view = $this->view_factory->createView(constants::VIEW_ACTIVATION_SUPPORT_CONFIRMATION_PAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
	 }
?>
