<?php
	/*
	 * Class.DestinationFcd.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	 
	require_once("Class.Constants.php");
	
	class DestinationFcd{
	 	
	 	private
	
		$props          = array(),
		        
		$view_factory   = array(),
		
		$models_factory = array();
		
		function __construct( $props ){
			require_once('travellog/views/Class.DestinationViewsFactory.php');
			$this->view_factory  = DestinationViewsFactory::getInstance();
			$this->props         = $props;
		}
		
		function retrieveAllCountriesPage(){
			$obj_view = $this->view_factory->createView(constants::VIEW_ALL_COUNTRIES_PAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view; 
		}
		
		function retrieveCountryPage(){
			$obj_view = $this->view_factory->createView(constants::VIEW_COUNTRY_PAGE);
			$obj_view->setPath('travellog/views/');
			$obj_view->addSubView( $this->retrieveTravelersLivingInCountry(),	'TRAVELERS_LIVING_IN_COUNTRY');
			$obj_view->addSubView( $this->retrieveTravelersCurrentlyInCountry(),	'TRAVELERS_CURRENTLY_IN_COUNTRY');
			$obj_view->addSubView( $this->retrieveJournalsInCountry(),	'JOURNALS_IN_COUNTRY');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveTravelersLivingInCountry(){
			$obj_view = $this->view_factory->createView(constants::VIEW_TRAVELERS_LIVING_IN_COUNTRY);
			$obj_view->setPath('travellog/views/');
			$this->props["travelers"] = $this->props["livingTravelers"];
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveTravelersCurrentlyInCountry(){
			$obj_view = $this->view_factory->createView(constants::VIEW_TRAVELERS_CURRENTLY_IN_COUNTRY);
			$obj_view->setPath('travellog/views/');
			$this->props["travelers"] = $this->props["currentTravelers"];
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function retrieveJournalsInCountry(){
			$obj_view = $this->view_factory->createView(constants::VIEW_JOURNALS_IN_COUNTRY);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
		function viewCountryList(){
			$obj_view = $this->view_factory->createView(constants::VIEW_COUNTRY_LIST);
			$obj_view->setPath('travellog/views/');
			$obj_view->setContents( $this->props );
			return $obj_view;
		}
		
	}
?>
