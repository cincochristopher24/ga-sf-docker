<?php
require_once("travellog/model/Class.AdsModel.php");
require_once("travellog/views/Class.AdsView.php");
class AdsFcd{
	function retrieve(){
		$obj_model = new AdsModel;
		return $obj_model->GetList();
	}
	
	function retrieveEntryAdsView(){
		$obj_views               = new AdsView;
		$arr_contents['obj_ads'] = $this->retrieve();
		$obj_views->setContents( $arr_contents );
		
		return $obj_views; 
	}
}
?>
