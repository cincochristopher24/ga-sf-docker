<?php

	require_once("Class.dbHandler.php");
	require_once("Class.Template.php");

	abstract class AbstractFeatureChangeNoticeHelper{
		
		abstract public function isFeatureChangeNoticeShown($travelerID=0);
		abstract public function getFeatureChangeNoticeTemplateView($traveler,$group);
		abstract public function setFeatureChangeNoticeHidden($travelerID=0);
		
	}