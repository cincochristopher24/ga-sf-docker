<?php
/**
 * @author		Jonas Tandinco
 * created		Nov/14/2008
 */


/**
 * limits a string to the set number of characters
 * 
 * @param $string to be processed
 * @param $limit - the number of characters to be returned
 * 
 * @return $string - modified to the length of the limit
 */
function limitString($string, $limit) {
	if (strlen($string) > $limit) {
      $string = substr($string, 0, strrpos(substr($string, 0, $limit), ' ')) . '...';
	}

	return $string;
}

/**
 * Truncates +text+ to the length of +length+ and replaces the last three characters with the +truncate_string+
 * if the +text+ is longer than +length+.
 * 
 * @param $text to be processed
 * @param $length - the number of characters to be returned
 * @param $truncate_string - the number of characters to be returned
 * @param $truncate_lastspace - the number of characters to be returned
 * @return $string - modified to the length of the limit
 */
function truncateText($text, $length = 100, $truncate_string = '...', $truncate_lastspace = true) {
	if ($text == '') {
    	return '';
    }
    
    $mbstring = extension_loaded('mbstring');
    if ($mbstring) {
    	$old_encoding = mb_internal_encoding();
    	@mb_internal_encoding(mb_detect_encoding($text));
    }
    
    $strlen = ($mbstring) ? 'mb_strlen' : 'strlen';
    $substr = ($mbstring) ? 'mb_substr' : 'substr';
    
    if ($strlen($text) > $length) {
    	$truncate_text = $substr($text, 0, $length - $strlen($truncate_string));
    	if ($truncate_lastspace) {
    		$truncate_text = preg_replace('/\s+?(\S+)?$/', '', $truncate_text);
    	}
    	$text = $truncate_text.$truncate_string;
    }
    
    if ($mbstring) {
    	@mb_internal_encoding($old_encoding);
    }
    
    return $text;
}