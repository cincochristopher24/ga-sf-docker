<?php
	/*
	 * Class.StudentProfileHelper.php
	 * Created on Nov 29, 2007
	 * created by marc
	 */
	
	require_once("Class.Constants.php");
	 
	class StudentProfileHelper{
		
		static $instance;
		private $programs = array();
		private $programTypes = array();
		
		static function getInstance(){
			if( self::$instance == NULL ) self::$instance = new StudentProfileHelper;
			return self::$instance;
		}
		
		function getRedirectPage( $redirect = "" ){
			switch($redirect){
				case constants::WELCOME_PAGE:
					return "welcome.php";
					break;
				case constants::LOGGED_IN_PAGE:
					return "passport.php";
					break;
				case constants::HOME_PAGE:
					return "index.php";
					break;	
				default:
					return "register.php";			
				
			}
		}
		
		function getAction(){
			if( isset($_GET["delete"]) )
				$action = constants::DELETE;
			elseif( isset($_GET["update"]) )
				$action = constants::SUBMIT_DETAILS;	
			elseif( isset($_GET["qry"]) )
				$action = constants::EDIT;	
			elseif( isset($_GET["refList"]) )
				$action = constants::VIEW_REFERED_LISTINGS;	
			elseif( isset($_GET["viewComm"]) )
				$action = constants::VIEW_STUDENT_COMMUNICATIONS;
			elseif( isset($_GET["viewListing"]) )
				$action = constants::VIEW_LISTING;
			elseif( isset($_GET["viewClient"]) )
				$action = constants::VIEW_CLIENT;			
			else 
				$action = constants::VIEW;	
			return $action;				
		}
		
		function readSPData(){
			if( isset($_POST) ){
				array_walk($_POST,array($this,'getPrograms') );
				array_walk($_POST,array($this,'getProgramTypes'));
			}
			
			return array(
				"firstOption"		=>	isset($_POST['cmbFirstOption']) ? $_POST['cmbFirstOption'] : 0,
				"secondOption"		=>	isset($_POST['cmbSecondOption']) ? $_POST['cmbSecondOption'] : 0,
				"thirdOption"		=>	isset($_POST['cmbThirdOption']) ? $_POST['cmbThirdOption'] : 0,
				"programs"			=>	$this->programs,
				"programTypes"		=>	$this->programTypes
			);
			
		}		
		
		function getPrograms($value, $key){
			if( preg_match("/chkProgram_([0-9])/",$key,$matches) )
				$this->programs[] = $matches[1];
		}
		
		function getProgramTypes($value, $key){
			if( preg_match("/chkType_([0-9])/",$key,$matches) ){
				if( !isset($this->programTypes[$matches[1]]) )
					$this->programTypes[$matches[1]] = array();
				$this->programTypes[$matches[1]] = $value;	
			}
		}
		
		function getPage(){
			$page = 1;
			if( isset($_GET['page']) )
				$page = $_GET['page'];
			elseif( isset($_POST['page']) )
				$page = $_POST['page'];
			return $page;		
		}
		
		function readViewCommData(){
			
			return array(
				'page'			=>	isset($_POST['page']) ? $_POST['page'] : 1,
				'rowsperpage'	=>	isset($_POST['rowsperpage']) ? $_POST['roswperpage'] : 5,
				'listingID'		=>	isset($_POST['listingID']) ? $_POST['listingID'] : 0,
				'clientID'		=>	isset($_POST['clientID']) ? $_POST['clientID'] : 0
			);
		}
		
		
	} 
?>
