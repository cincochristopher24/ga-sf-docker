<?php
	
	/*
	 * Class.LoginHelper.php
	 * Created on Nov 22, 2007
	 * created by marc
	 */
	 
	require_once("Class.Constants.php");
	
	class LoginHelper{
		
		static $instance = NULL;
		
		static function getInstance(){
			if( self::$instance == NULL ) self::$instance = new LoginHelper;
			return self::$instance;
		}
		
		function getAction(){
			
			if( isset($_GET["ref"]) )
				$action = constants::AUTO_LOGIN;
			elseif(0 == strcmp("logout.php",preg_replace("/\//","",$_SERVER["PHP_SELF"])))
				$action = constants::LOGOUT;
			elseif( isset($_GET["failedLogin"]) )
				$action = constants::FAILED_LOGIN;	
			elseif( !(isset($_POST["btnLoginSubmit"]) || isset($_POST['btnLoginSubmit_x']) ) && 0 <> strcmp("login.php",preg_replace("/\//","",$_SERVER["PHP_SELF"])) )
				$action = constants::NON_LOGIN_PAGE_FILL_UP;	
			elseif( isset($_POST["btnLoginSubmit"]) || isset($_POST['btnLoginSubmit_x'] ) )
				$action = constants::LOGIN;
			else	
				$action = constants::FILL_UP;
			return $action;
				
		}
		
		function getUser(){
			
			if( isset($_GET["advisor"]) || ( isset($_POST["hdnLoginType"]) && constants::GROUP == $_POST["hdnLoginType"]) )
				$user = constants::GROUP;
			else
				$user = constants::TRAVELER;
			return $user;
					
		}
		
		function readLoginData(){
			
			return $content = array(
				"email"			=>	isset($_POST["txtEmail"]) ? trim($_POST["txtEmail"]) : "",
				"password"		=>	isset($_POST["pwdPasWrd"]) ? trim($_POST["pwdPasWrd"]) : "",
				"loginType"		=>	isset($_POST["hdnLoginType"]) ? $_POST["hdnLoginType"] : constants::TRAVELER,
				"referer"		=>	$this->getReferer()
			);
			
		}
		
		function isNewlyRegistered(){
			return isset($_GET["isRegister"]) || isset($_GET["ref"]);
		}
		
		function getCurrentView(){
			
			if( 0 == strcmp(preg_replace("/\//","",$_SERVER["PHP_SELF"]),"login.php") )
				return constants::VIEW_LOGIN_PAGE;
			else
				return constants::VIEW_LOGIN_FORM;	
		
		}
		
		function getReferer(){
			$hdnReferer = "";	
			if( isset($_GET["redirect"]) && strlen(trim($_GET["redirect"])) ){
		 		$_SERVER["QUERY_STRING"] = str_replace("^", "#", $_SERVER["QUERY_STRING"]);
		 		$hdnReferer              = substr($_SERVER["QUERY_STRING"],strpos($_SERVER["QUERY_STRING"],"redirect=")+9);
		 	}	
		 	elseif( isset($_POST["hdnReferer"]) && strlen(trim($_POST["hdnReferer"])) )
		 		$hdnReferer = $_POST["hdnReferer"];	
		 		
			return $hdnReferer;
		}
		
		function getActivationKey(){
			return isset($_GET["ref"]) ? trim($_GET["ref"]) : "";
		}
		
		public static function useLabelToggleJs() {
		    require_once 'Class.Template.php';
		    Template::includeDependent(<<<EOF
<script type="text/javascript">
// <!-- <![CDATA[
jQuery.noConflict();
(function() {
var $ = jQuery;
$(document).ready(function() {
var inputs = $('#header #main_login_form input.text');
var labels = $('#header #main_login_form label');
var hideLabel = function(e) {
var input = (e.currentTarget) ? $(e.currentTarget) : $(e.target);
var label = $(input.siblings('label').get(0));
label.css('display', 'none');
};

var hideThisLabel = function(e) {
var label = (e.currentTarget) ? $(e.currentTarget) : $(e.target);
var input = $(label.siblings('input').get(0));
label.css('display', 'none');
input.focus();
}

var showLabel = function(e) {
var input = (e.currentTarget) ? $(e.currentTarget) : $(e.target);
var label = $(input.siblings('label').get(0));

if (!input.attr('value')) {
    label.css('display', 'block');
} else {
    label.css('display', 'none');
}
};

labels.click(hideThisLabel);
inputs.click(hideLabel);
inputs.focus(hideLabel);
inputs.blur(showLabel);
inputs.blur();
});
})();
// ]]> -->
</script>
EOF
                , array('bottom'=>'true')
	        );
		}
		
	} 
?>
