<?php
	/*
	 * Class.AdvisorInquiriesHelper.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
	
	require_once("Class.Constants.php");
	 
	class AdvisorInquiriesHelper{
		
		static $instance;
		
		static function getInstance(){
			if( self::$instance == NULL ) self::$instance = new AdvisorInquiriesHelper;
			return self::$instance;
		}
		
		function getAction(){
			
			if( isset($_POST['changeView']) )
				$action = constants::CHANGE_VIEW;	
			elseif( isset($_POST['viewMessagesPerStudentListing']) )
				$action = constants::VIEW_MESSAGES_PER_STUDENT_AND_LISTING;	
			else
				$action = constants::VIEW_MESSAGE_LIST;
			return $action;	
		}
		
		function getAdvisorView(){
			
			return isset($_POST['viewMode']) && constants::VIEW_BY_LISTING == $_POST['viewMode'] ? constants::VIEW_BY_LISTING : constants::VIEW_BY_STUDENT; 
		}
		
		function readAiData(){
			
			return array(
				'page'			=>	isset($_POST['page']) ? $_POST['page'] : 1,
				'rowsperpage'	=>	isset($_POST['rowsperpage']) ? $_POST['roswperpage'] : 5,
				'listingID'		=>	isset($_POST['listingID']) ? $_POST['listingID'] : 0,
				'clientID'		=>	isset($_POST['clientID']) ? $_POST['clientID'] : 0
			);
		}
		
		function readAiMessageData(){
			
			return array(
				'travelerID'	=>	$_POST['travelerID'],
				'inquiryView'	=>	$_POST['inquiryView'],
				'listingID'		=>	$_POST['listingID']
			);
			
		}	
	
	}	 
?>
