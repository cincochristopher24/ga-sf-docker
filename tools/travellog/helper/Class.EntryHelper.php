<?php
class EntryHelper{
	function getLink($obj_travellog){
		//$entry_title = str_replace( " ","-", $obj_travellog->getTitle() ).'-'.$obj_travellog->getTravelLogID();
		try{
			if( is_object($obj_travellog) ){
				$entry_title = str_replace( "-","_", $obj_travellog->getTitle() );
				$entry_title = str_replace( " ","-", $entry_title );
			
				if( get_class($obj_travellog) == 'EntryVO'){
					if( $obj_travellog->getGroupName()){
						$group_name = str_replace( "-","_", $obj_travellog->getGroupName() );
						$group_name = str_replace( " ","-", $group_name                    ); 
						$link  = '/groups/' . $group_name;
						if( strlen( $obj_travellog->getSubGroupName() ) ){
							$sub_group_name = str_replace( "-","_", $obj_travellog->getSubGroupName() );
							$sub_group_name = str_replace( " ","-", $sub_group_name                   );
							$link .= '/subgroup/'. $sub_group_name;
						}
						
						$link .= '/journals/' . $obj_travellog->getTravelID() . '/' . $entry_title;
					}else
						$link= '/' . $obj_travellog->getUsername() . '/journals/' . $obj_travellog->getTravelID() . '/' . $entry_title;
				}else if( get_class($obj_travellog) == 'TravelLog' ){
					if( $obj_travellog->getTravel()->isGroupJournal() ){
						$group_name = str_replace( "-","_", !$obj_travellog->getOwner()->isSubGroup() ? $obj_travellog->getOwner()->getName():$obj_travellog->getOwner()->getParent()->getName() );
						$group_name = str_replace( " ","-", $group_name                    ); 
						$link  = '/groups/' . $group_name;
						if( $obj_travellog->getOwner()->isSubGroup() ){
							$sub_group_name = str_replace( "-","_", $obj_travellog->getOwner()->getName() );
							$sub_group_name = str_replace( " ","-", $sub_group_name                   );
							$link .= '/subgroup/'. $sub_group_name;
						}	
						$link .= '/journals/' . $obj_travellog->getTravelID() . '/' . $entry_title;
					}
					else
						$link    = '/' . $obj_travellog->getTraveler()->getUsername() . '/journals/' . $obj_travellog->getTravelID() . '/' . $entry_title;
				}else{
					$link    = '/' . $obj_travellog->getTraveler()->getUsername() . '/journals/' . $obj_travellog->getTravelID() . '/' . $entry_title;
				}
			}
			else $link = '';
		}catch(exception $e){ $link = ''; }	
		return $link;          
	}
}

