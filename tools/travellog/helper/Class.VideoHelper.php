<?php

	/**
	 * @author Nherrisa Mae Uy Celeste
	 * @since 1.0 - Nov 21, 2009
	 * @since 2.0 - Jan 19, 2010
	 * 		- edit: allowed videos from vimeo
	 * @since 3.0 - March 11, 2010
	 * @since 4.0 - April 13, 2011
	 * 		- edit: allowed youtube videos' shortened url
	 */

	class VideoHelper {
		
		private static $instance;
		
		const YOUTUBE = 1;
		const GOOGLE  = 2;
		const VIMEO   = 3;
		
		public static function getInstance() {
			if( self::$instance == NULL ){
				self::$instance = new VideoHelper;
			}
			
			return self::$instance;
		}
		
		/**
		 * retrieves video attributes that are not provided by the user when saving a video
		 * 
		 * @param string $url
		 * @return Array
		 */
		
		public function getVideoAttributes( $url )
		{
			$attributes = array('url' => $url);
			
			switch( $this->getVideoType($url) ){
				case self::YOUTUBE:		return array_merge($attributes, $this->getYoutubeVideoAttributes($url));
				case self::GOOGLE:		return array_merge($attributes, $this->getGoogleVideoAttributes($url));
				case self::VIMEO:		return array_merge($attributes, $this->getVimeoVideoAttributes($url));
				default:				return array();
			}
		}
		
		/**
		 * retrieves a compatible url used for playing a video
		 * 
		 * @param string $url
		 * @return string
		 */
		
		public function getUrlForVideoPlaying( $url )
		{
			switch( $this->getVideoType($url) ){
				case self::YOUTUBE:	return 'http://www.youtube.com/v/'.$this->getYoutubeVideoId($url).'&hl=en&fs=1&showinfo=0&rel=0';
				case self::GOOGLE:	return 'http://video.google.com/googleplayer.swf?docid='.$this->getGoogleVideoId($url).'&hl=en&fs=true';
				case self::VIMEO:	return 'http://vimeo.com/moogaloop.swf?clip_id='.$this->getVimeoVideoId($url).'&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=00adef&amp;fullscreen=1';
				default:			return '';
			}
		}
		
		/**
		 * checks if video is a youtube, google or vimeo video
		 *
		 * @param string $url
		 * @return integer constant
		 */
		
		private function getVideoType( $url )
		{
			if( strpos($url, 'youtube.com') || strpos($url, 'youtu.be') ){
				return self::YOUTUBE;
			}
			else if( strpos($url, 'video.google.com') ){
				return self::GOOGLE;
			}
			else if( strpos($url, 'vimeo.com') ){
				return self::VIMEO;
			}
			
			return 0;
		}
		
		/**
		 * retrieves a youtube video ID
		 *
		 * @param string $url
		 * @return string video ID
		 */
		
		private function getYoutubeVideoId( $url )
		{
			$videoID = '';
			
			if( strpos($url, 'youtube.com') > -1 ){
				if( strpos($url, 'user') > -1 ){
        			$urlArray = explode('/', $url);
        			$videoID = $urlArray[count($urlArray) - 1];
        		}
        		else{
        			if( !strpos($url,'&') || (strpos($url,'&') < strpos($url, 'v')) ){
						$videoID = substr($url, strpos($url, 'v') + 2);
					}
	                else if( strpos($url,'&') && (strpos($url,'&') > strpos($url,'v')) ){
						$videoID = substr($url, strpos($url, 'v') + 2, intval(strpos($url, '&')) - intval(strpos($url, 'v') + 2));
					}	
        		}
			}
			// for shortened url
			else if( strpos($url, 'youtu.be') > -1 ){
				$videoID = substr($url, strpos($url, 'youtu.be') + 9);
			}
			
			return $videoID;
		}
		
		/**
		 * retrieves a google video ID
		 *
		 * @param string $url
		 * @return string video ID
		 */
		
		private function getGoogleVideoId( $url )
		{
			// this is applied when given a url for a related video
			if( substr_count($url, 'docid') > 1 ){
				return substr($url, strpos($url, '#') + 7);
			}
			
			return (strpos($url, '&')) ? 
						substr($url,strpos($url,'docid') + 6, intval(strpos($url,'&')) - intval(strPos($url,'docid') + 6)) : 
						substr($url,strPos($url,'docid') + 6, strlen($url)); 
		}
		
		/**
		 * retrieves a vimeo video ID
		 *
		 * @param string $url
		 * @return string video ID
		 */
		
		private function getVimeoVideoId( $url )
		{
			return ( strpos($url, 'clip_id') ) ? 
						substr($url, strpos($url, 'clip_id') + 8, strpos($url, '&') - strpos($url, '=') - 1) : 
						substr($url, strrpos($url, '/') + 1, strlen($url) - strrpos($url, '/') + 1);
		}
		
		/**
		 * retrieve youtube video attributes (i.e. thumbnail and duration)
		 *
		 * @param string $url
		 * @return array 
		 */
		
		private function getYoutubeVideoAttributes( $url )
		{
			$thumbnail = '';
			$duration = 0;
			
			try {
				
				$html = $this->curlDocumentDom('http://gdata.youtube.com/feeds/api/videos/'.$this->getYoutubeVideoId($url));
            	$doc = new DOMDocument();
				@$doc->loadHtml($html);
                // @$doc->load('http://gdata.youtube.com/feeds/api/videos/'.$this->getYoutubeVideoId($url));
                   
                foreach( $doc->getElementsByTagName('duration') as $node ){
					$duration = (int) $node->getAttribute('seconds');
				}
				foreach( $doc->getElementsByTagName('thumbnail') as $thumbNailNode ){
                	if( $thumbNailNode->getAttribute('height') > 90 || $thumbNailNode->getAttribute('width') > 120 ){
						$thumbnail = $thumbNailNode->getAttribute('url');
					}
                }
                
                if( 0 == strlen($thumbnail) ){
					$thumbnail = 'http://i2.ytimg.com/vi/'.$videoSubstr.'/default.jpg';
				}
            }
            catch (exception $e) {}
			
			return array('thumbnail' => $thumbnail, 'duration' => $this->formatDuration($duration));
		}
		
		/**
		 * retrieve google video attributes (i.e. thumbnail and duration)
		 *
		 * @param string $url
		 * @return array 
		 */
		
		private function getGoogleVideoAttributes( $url )
		{
			$thumbnail = '';
			$duration = 0;
			
			if( strpos($url, 'docId') > -1 ){
				$url = substr_replace($url, 'docid', strpos($url, 'docId'), 5);
			}
			
			try {
				$html = $this->curlDocumentDom('http://video.google.com/videofeed?docid='.$this->getGoogleVideoId($url));
            	$doc = new DOMDocument();
				@$doc->loadHtml($html);
                // @$doc->load('http://video.google.com/videofeed?docid='.$this->getGoogleVideoId($url));

                foreach( $doc->getElementsByTagName('content') as $linkNode ){
					$duration = $linkNode->getAttribute('duration');
				}
                foreach( $doc->getElementsByTagName('thumbnail') as $thumbNailNode ){
					$thumbnail = $thumbNailNode->getAttribute('url');
				}
            }
            catch (exception $e) {}

			return array('thumbnail' => $thumbnail, 'duration' => $this->formatDuration($duration));
		}
		
		/**
		 * retrieve vimeo video attributes (i.e. thumbnail and duration)
		 *
		 * @param string $url
		 * @return array 
		 */
		
		private function getVimeoVideoAttributes( $url )
		{
			$thumbnail = '';
			$duration = 0;
	    		
	   		try {
				$html = $this->curlDocumentDom('http://vimeo.com/api/v2/video/'.$this->getVimeoVideoId($url).'.xml');
            	$doc = new DOMDocument();
				@$doc->loadHtml($html);
    			// @$doc->load('http://vimeo.com/api/v2/video/'.$this->getVimeoVideoId($url).'.xml');

    			foreach( $doc->getElementsByTagName('duration') as $linkNode ){
					$duration = $linkNode->nodeValue;
				}
    			foreach( $doc->getElementsByTagName('thumbnail_medium') as $thumbnailNode ){
					$thumbnail = $thumbnailNode->nodeValue;	
				}
    		}
    		catch (exception $e) {}
			
			return array('thumbnail' => $thumbnail, 'duration' => $this->formatDuration($duration));
		}
		
		/**
		 * converts $duration to the format 'minute:second' 
		 *
		 * @param integer $duration
		 * @return string
		 */
		
		private function formatDuration( $duration )
		{
			if( $duration > 0 ){
				$time = $duration;
				$hour = floor($time / 3600);
				$time = $duration % 3600;
				$minute = floor($time / 60);
				$time = $duration - ( $hour * 3600 ) - ( $minute * 60 );
				$second = $time % 60;
				
				$formattedDuration  = ( $hour > 0 ) ? $hour.':' : '';
				$formattedDuration .= ( $minute > 9 ) ? $minute : '0'.$minute;
				$formattedDuration .= ':';
				$formattedDuration .= ( $second > 9 ) ? $second : '0'.$second;
				
				return $formattedDuration;
			}
			
			return '00:00';
		}
		
		private function curlDocumentDom($url){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "$url");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);				
			curl_close($ch);

			return $result;
		}
	}