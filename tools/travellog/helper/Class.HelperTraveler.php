<?
class HelperTraveler
{
		
		public static function displayTravelerPhotoSrc($traveler) {
			if ($traveler->getTravelerProfile()->getPrimaryPhoto())
			{ 
				return $traveler->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink();
			}
			else
			{
				return '/travellog/images/paw_print.gif';
			}
			
		}
		
		
		
		public static function displayTravelerPhotoCaption($traveler) {
			if ($traveler->getTravelerProfile()->getPrimaryPhoto())
			{ 
				return $traveler->getTravelerProfile()->getPrimaryPhoto()->getCaption();
			}
		}
		
		
		
		public static function displayAllTravelers($travelers)
		{
				if ($travelers):
				
						$ctr = 0;
						foreach ($travelers as $traveler):
						
								$ctr++;
								echo '<td>';
								
								if ($traveler->getTravelerProfile()->getPrimaryPhoto())
								{ 
										echo '<img src="'.$traveler->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink().'" /><br />';
										echo $traveler->getTravelerProfile()->getPrimaryPhoto()->getCaption()."<br />";
								}
								 
								echo  $traveler->getUsername().'<br />';
								echo '<a href="#">view profile</a><br>';
								echo $traveler->getCountTravelJournals().' Travel Journals<br />';
								echo $traveler->getCountTravelLogs().' Travel Logs';
								
								echo '</td>';
								 
								if( ($ctr%2) == 0 ) echo '</tr><tr>';
								
						endforeach;
						
				endif;
		}
		
		public static function displayPopularTravelers($travelers)
		{
				if ($travelers):
						
						echo '<table border="1"><tr><td>Most Popular</td></tr>';
						
						foreach ($travelers as $traveler):
						
								echo '<tr><td>';
										
								if ($traveler->getTravelerProfile()->getPrimaryPhoto())
								{ 
										echo '<img src="'.$traveler->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink().'" /><br />';
								} 
								echo $traveler->getUsername();
															
								echo '</td></tr>';
								
						endforeach;
						
						echo '</table>';
						
				endif;
		}
		
		public static function displayTravelersOnMyLocation($travelers)
		{
				if ($travelers):
						
						echo '<table border="1"><tr><td>Other people on my location</td></tr>';
						
						foreach ($travelers as $traveler):
						
								echo '<tr><td>';
										
								if ($traveler->getTravelerProfile()->getPrimaryPhoto())
								{ 
										echo '<img src="'.$traveler->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink().'" /><br />';
								} 
								echo $traveler->getUsername();
															
								echo '</td></tr>';
								
						endforeach;
						
						echo '</table>';
						
				endif;
		}	
		
		// Created By : Adelbert Silla
		// Date : March 25 2010
		// Purpose : Text to image converter
		// Param : $text - String
		// TODO : Modify the code (for flexibility) if necessary.
		public static function convertTextToImage($text = null) {
			$domain = $_SERVER['SERVER_NAME'];
			$file = "http://$domain/textToImageConverter.php?text=$text";
			return "<img src='$file'>";
		}
}
?>
