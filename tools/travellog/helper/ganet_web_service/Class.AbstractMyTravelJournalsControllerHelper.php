<?php
	require_once 'travellog/controller/socialApps/mytraveljournals/Class.AbstractMyTravelJournalsController.php';
	
	class AbstractMyTravelJournalsControllerHelper{
	
		public $action;
		public $domain;
		public $ownerID;
		public $viewerID;
		public $username;
		public $password;
		public $ownerName;
		public $journalsShown;
		public $emailAdd;
		public $viewType;
		public $travelIDs = array();
	
		function getAction(){
//			echo 'userAction = '.$this->action; exit;
/*			if( isset($this->ownerID) && $this->action == AbstractMyTravelJournalsController::ADD_APP )
				$action = AbstractMyTravelJournalsController::ADD_APP;
			elseif( $this->viewerID == $this->ownerID ){
				if( isset($this->action) && in_array($this->action, AbstractMyTravelJournalsController::getValidActions() ) )
					$action = $this->action;
				else{
					$action = AbstractMyTravelJournalsController::SHOW_INDEX_PAGE;
				}	
			}
			else{
				$action = AbstractMyTravelJournalsController::SHOW_JOURNALS;
			}			
			return $action; 
*/
			return $this->action;
		}
		
		function getSynchronizeArgs(){			
			return array(
				'username'	=>	trim($this->username),
				'password'	=>	trim($this->password),
				'ownerID'	=>	$this->ownerID,
				'viewerID'	=>	$this->viewerID,
				'domain'	=>	$this->domain,	
				'emailadd'	=>	trim($this->emailAdd)
			);
		}
		
		function getSubmittedInfo(){
			return array(
				'travelJournalsShown' => $this->journalsShown,
				'travelIDs' => $this->travelIDs,
				'userID' =>	$this->ownerID,
				'viewType' => $this->viewType
			);
		}
		
		function getAppViewerID(){
			return $this->viewerID;
		}
		
		function getOwnerName(){
			return $this->ownerName;
		}
				
	} 
 ?>	