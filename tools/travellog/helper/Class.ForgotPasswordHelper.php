<?php
	/*
	 * Class.ForgotPasswordHelper.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	
	require_once("Class.Constants.php");
	
	class ForgotPasswordHelper{
		
		static $instance = NULL;
		
		static function getInstance(){
			if( self::$instance == NULL ) self::$instance = new ForgotPasswordHelper;
			return self::$instance;
		}
		
		function getAction(){
			
			if( isset($_POST["btnSubmit"]) )
				return constants::SUBMIT_DETAILS;
			else
				return constants::FILL_UP;	
		}
		
		function readForgotpasswordData(){
			
			return array(
				"email"		=>	isset($_POST["txtFindString"]) ? trim($_POST["txtFindString"]) : "",
				"code"		=>	isset($_POST['encSecCode']) ? trim($_POST['encSecCode']) : "",
				"secCode"	=>	isset($_POST['securityCode']) ? trim($_POST['securityCode']) : "",
				"arrError"	=>	array()
			);
		}
	} 
?>
