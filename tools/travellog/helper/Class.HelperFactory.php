<?php
require_once("travellog/helper/Class.HelperType.php");
class HelperFactory{
	static $instance = NULL;
	
	static function instance(){
		if( self::$instance == NULL ) self::$instance = new HelperFactory;
		return self::$instance;
	}
	
	function create($helper_type){
		switch($helper_type){
			case HelperType::$JOURNAL_ENTRY:
				require_once("travellog/helper/Class.EntryHelper.php");
				$obj = new EntryHelper;
			break;
			case HelperType::$PROFILE:
				require_once("travellog/helper/Class.ProfileHelper.php");
				$obj = new ProfileHelper;
			break;
		}
		return $obj;
	}
}
?>
