<?php
	/**
	 * Class.SpamMessagesHelper.php
	 * created on 04 28, 08
	 */
	
	require_once 'Class.Constants.php';
	 
	class SpamMessagesHelper{
		
		public static $instance = NULL;
		
		public static function getInstance(){
			if( NULL == self::$instance )
				self::$instance = new SpamMessagesHelper;
			return self::$instance;	
		} 
		
		function getAction(){
			if( isset($_POST['btnDelete']) )
				return constants::DELETE;
			elseif( isset($_POST['btnSend']) )
				return constants::SEND;
			elseif( isset($_GET['action']) )	
				return $_GET['action'];
			else		
				return constants::MESSAGE_LIST;	
		}
		
		function getCurrPage(){
			return ( isset($_GET["page"]) ) ? (int)$_GET["page"] : 1;
		}
		
		function getRowsPerPage(){
			return ( isset($_GET["rowsperpage"]) ) ? (int)$_GET["rowsperpage"] : 5;
		}
		
		function getMessageIds(){
			return isset( $_POST['chkMsgId']) ? $_POST['chkMsgId'] : array();
		}
		
		function getKeyword(){
			return isset( $_REQUEST['keyword']) ? $_REQUEST['keyword'] : null;
		}
		
	} 
?>
