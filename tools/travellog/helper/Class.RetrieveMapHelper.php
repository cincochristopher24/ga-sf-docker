<?php
	 
	/**
	 * Class.RetrieveMapHelper.php
	 * handles retrieving of google maps through ajax
	**/
	class RetrieveMapHelper {
		
		const MY_TRAVEL_MAP 	= 0;
		const JOURNAL_ENTRY_MAP = 1;
		const JOURNALS_MAP 		= 2;
		const ARTICLE_MAP 		= 3;
		
		public function getJournalEntryMap($arr_filter){
			require_once("travellog/model/Class.JournalModel.php");
			$obj_journal = new JournalModel;
			$obj_journal = $obj_journal->Get($arr_filter['travelID'] );
			$arr_filter['arr_trips']     = $obj_journal->getTrips();
			
			require_once("travellog/model/Class.MapModel.php");
			$obj_model = new MapModel;
			$arr_contents['arr_marker'] = $obj_model->Get( $arr_filter );
			
			require_once("travellog/model/Class.Map.php");
			$map_frame = new MapFrame;
			$map_frame->setWidth(203);
			$map_frame->setHeight(203);
			
			$map = new Map;
			return $map->plotLocationsThroughAjax($arr_contents['arr_marker'], $map_frame, "travel_map");
		}
		
		public function getArticleMap($arr_filter){
			require_once("travellog/model/Class.MapModel.php");
			require_once("travellog/model/Class.Article.php");

			$obj_model = new MapModel;
			$obj_article = new Article($arr_filter['articleID']);

			$arr_contents['arr_marker']  = $obj_model->GetArticleMap( array($obj_article) );

			require_once("travellog/model/Class.Map.php");
			$map_frame = new MapFrame;
			$map_frame->setWidth(203);
			$map_frame->setHeight(203);
			
			$map = new Map;
			return $map->plotLocationsThroughAjax($arr_contents['arr_marker'], $map_frame, "travel_map");
		}
		
		public function getJournalsMap($col_countries){
			$arr_contents['col_locations'] = $col_countries;
			require_once("travellog/views/Class.GAMapView.php");
			$obj_map_view = new GAMapView;
			$obj_map_view->setContents($arr_contents);
			return $obj_map_view->render();
		}
		
		public function getMyTravelMap($arr_filter){
			require_once('travellog/model/socialApps/mytravelmap/Class.TravelMapPeer.php');
			require_once('travellog/model/Class.Traveler.php');
			$userMap = TravelMapPeer::getTravelMap($arr_filter['ID']);
			$mapPinSettings = Traveler::getMapPinSettings($arr_filter['ID']);
			$loop = "";
			foreach($userMap->getCountriesTraveled() as $country):
				if($mapPinSettings != 1) {
					
	      		$loop .= 'var latlng = new GLatLng('.$country->getCountry()->getLatitude().', '.$country->getCountry()->getLongitude().');';

	            // compose html display for info window
				// if (!$country->getNumPhotos() && !$country->getNumJournalEntries() && !$country->getNumTravelTips()) :
					$loop .= 'var myHtml = "";';
					$loop .= 'var isClickable = false;';
				/*else:
	        		$loop .= 'var myHtml = \'<div id="bubble_details_container">\';';
					$loop .= 'var isClickable = true;';

	        		if ($country->getNumPhotos() > 0) :
	        			$loop .= 'myHtml += \'<img id="photo" src="<?php echo $country->getRandomPhoto() ?>" />\';';
	        		endif;
	        		$loop .= 'myHtml += \'<div id="bubble_details">\';';
					$loop .= 'myHtml += \'<h4>'.$country->getCountryName().'</h4>\';';
					$loop .= 'myHtml += \'<div id="container">\';';
	        		if ($country->getNumJournalEntries() == 1) :
	        			$loop .= 'myHtml += \'<p><span>1</span> Journal Entry<p>\';';
	        		elseif ($country->getNumJournalEntries() > 1) :
	        			$loop .= 'myHtml += \'<p><span>'.$country->getNumJournalEntries().'</span> Journal Entries</p>\';';
	        		endif;

	        		if ($country->getNumPhotos() == 1) :
	        			$loop .= 'myHtml += \'<p><span>1</span> Travel Photo</p>\';';
	        		elseif ($country->getNumPhotos() > 1) :
	        			$loop .= 'myHtml += \'<p><span>'.$country->getNumPhotos().'</span> Travel Photos</p>\';';
	        		endif;

	        		if ($country->getNumTravelTips() == 1) :
	    				$loop .= 'myHtml += \'<p><span>1</span> Travel Tip</p>\';';
	    			elseif ($country->getNumTravelTips() > 1) :
	    				$loop .= 'myHtml += \'<p><span>'.$country->getNumTravelTips().'</span> Travel Tips</p>\';';
	        		endif;
	        		$loop .= 'myHtml += \'<div style="clear: both;"></div></div></div><div class="clear"></div></div>\';';
				endif;
                */

					$loop .= 'map.addOverlay(createMarker(latlng, myHtml, \''.$country->getCountry()->getName().'\', isClickable, "country"));';
				}

				if($mapPinSettings != 0) {				
			 	   foreach ($country->getCountry()->getCityLocationList() as $city) {
		      			$loop .= 'var latlng = new GLatLng('.$city->getLatitude().', '.$city->getLongitude().');';
						$loop .= 'var myHtml = "";';
						$loop .= 'var isClickable = false;';
						$loop .= 'map.addOverlay(createMarker(latlng, myHtml, \''.$city->getName().'\', isClickable, "city"));';
			    	}
				}

			endforeach;
			
			$jsCode = <<<BOF
				jQuery(document).ready(function () {initialize();});

				function initialize() {
					if (GBrowserIsCompatible()) {
				    	var map = new GMap2(document.getElementById("map_canvas"));

						map.setCenter(new GLatLng(0, 0), 1);
						map.addControl(new GSmallMapControl());
						map.setZoom(0);

					    var blueImage = '/images/ga_markerTransparent.png';	
					    var shadowImage = '/images/ga_marker_shadowTransparent.png';

					    iconBlue = new GIcon();
					    iconBlue.image = blueImage;
					    iconBlue.shadow = shadowImage;
					    iconBlue.iconSize = new GSize(12, 20);
					    iconBlue.shadowSize = new GSize(22, 20);
					    iconBlue.iconAnchor = new GPoint(6, 20);
					    iconBlue.infoWindowAnchor = new GPoint(5, 1);

					    var tinyBlueImage = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_tinymarkerBlueTransparent.png';
					    iconTinyBlue = new GIcon();
					    iconTinyBlue.image = tinyBlueImage;
						iconTinyBlue.iconSize = new GSize(12, 12);
						iconTinyBlue.shadowSize = new GSize(22, 12);
					    iconTinyBlue.iconAnchor = new GPoint(6, 12);
					    iconTinyBlue.infoWindowAnchor = new GPoint(5, 1);

						// ORIG PIN
						// Create a base icon for all of our markers that specifies the
				        // shadow, icon dimensions, etc.
				        // var baseIcon = new GIcon();
				        // 
				        // baseIcon.image = "/images/ga_markerTransparent.png"
				        // baseIcon.shadow = "/images/ga_marker_shadowTransparent.png";
				        // baseIcon.iconSize = new GSize(12, 20);
				        // baseIcon.shadowSize = new GSize(22, 20);
				        // baseIcon.iconAnchor = new GPoint(6, 20);
				        // baseIcon.infoWindowAnchor = new GPoint(6, 2);
				        // baseIcon.infoShadowAnchor = new GPoint(18, 25);

					    // Creates a marker at the given point
				    	// Clicking the marker will hide it
				    	function createMarker(latlng, myHtml, countryName, isClickable, type) {
							baseIcon = type == 'country' ? iconBlue : iconTinyBlue;
				      		var marker = new GMarker(latlng, {title:countryName, icon:baseIcon, clickable:isClickable});

				      		GEvent.addListener(marker, 'click', function() {
				      			document.getElementById('country_info').innerHTML = myHtml;
				      		});

				      		return marker;
						}
				        // add countries traveled markers	
						$loop
					}
			    }
				
BOF;
			return $jsCode;
		}
		
	}