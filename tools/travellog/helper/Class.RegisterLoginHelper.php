<?php
	/*
	 * Class.RegisterLoginHelper.php
	 * Created on Apr 3, 2008
	 */
	 
	 require_once 'Class.Constants.php';
	 
	 class RegisterLoginHelper{
	 
	 	static $instance = NULL;
	 	private $attr = array();
	 	
	 	static function getInstance(){
			if( self::$instance == NULL ){
				self::$instance = new RegisterLoginHelper;
			} 
			return self::$instance;
		}
		
		function setAttributes(array $args = array() ){
			$this->attr = $args;
		}
		
	 	function getAction(){
	 		
	 		if( 0 == count($this->attr) )
	 			return NULL;
	 		elseif( array_key_exists('ref', $this->attr) && 1 == count($this->attr) ){
	 			$this->attr['action'] = constants::AUTO_LOGIN;
	 		}	
	 		return $this->attr['action'];
	 	}
	 	
	 	function getActivationKey(){
	 		
	 		if( array_key_exists('ref', $this->attr) ){
	 			return $this->attr['ref'];
	 		}
	 		else
	 			return NULL;
	 	}
	 	
	 	function isNewlyRegistered(){
	 		return true;
	 	}
	 	
	 	function getReferer(){
			/**
			 *	@author		Miss CIG
			 *	Date		28 October 2009	
			 *	Purpose		This is in accordance to the new requirements in comments. After registering, a user must be redirected
			 *					back to the page where he was supposed to leave a comment.
			 */
			$hdnReferer = "";	
			if( isset($_GET["redirect"]) && strlen(trim($_GET["redirect"])) ){
		 		$_SERVER["QUERY_STRING"] = str_replace("^", "#", $_SERVER["QUERY_STRING"]);
		 		$hdnReferer              = substr($_SERVER["QUERY_STRING"],strpos($_SERVER["QUERY_STRING"],"redirect=")+9);
		 	}
		 		
			return $hdnReferer;
	 	}
	 	
	 	function getUser(){
	 		if( isset($_POST["hdnMode"]) && constants::GROUP == $_POST["hdnMode"] )
	 			return constants::GROUP;
	 		else
	 			return constants::TRAVELER;	
	 	}
	 
	 }
?>
