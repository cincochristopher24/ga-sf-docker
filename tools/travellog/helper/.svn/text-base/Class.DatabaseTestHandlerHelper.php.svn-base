<?php
/*
 * Created on 10 6, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 	require_once('Class.GaString.php');
	require_once('Class.iRecordset.php');
	require_once('Class.dbHandler.php');

  	/**
  	 * Class that handles database testing. This also handles
  	 * insert and truncate of data in the database.
  	 */
	
	class DatabaseTestHandlerHelper {
		private static $handler = null;
		private static $instance = null;
		private $mDump = "\n";
		private $mMultiDump = "\n";
		
		/**
		 * Method to get the instance of this class. This is done 
		 * to follow the singleton pattern
		 */
		
		public static function getInstance(){
			if ( self::$instance == NULL ) 
				self::$instance = new DatabaseTestHandlerHelper;
			
			return self::$instance;
		}
		
		public static function setDatabaseHandler($databaseHandler){
			self::$handler = $databaseHandler;
		}
		
		/**
		 * Method for checking if two multidimensional array or shall i say an array of 
		 * associative array has the same values
		 * @access public
		 * @param array $array1
		 * @param array $array2
		 * @return boolean if they have the same value returns true and false otherwise
		 */
		
		public function isMultiArrayValuesSame($array1, $array2){
			if (count($array1) != count($array2))
				return false;
	
			$this->mMultiDump = "\n";
			
		    for ($i = 0; $i < count($array1); $i++){
		    	if (!$this->isArrayValuesSame($array1[$i], $array2[$i])) 
		    		return false;
		
		    	$this->mMultiDump = $this->mMultiDump + $this->mDump;
		    }
		    
		    return true;
		}
		
		public function isArrayValuesSame($array1, $array2){
			$this->mDump = "\n";
			
			try {
				foreach (array_keys($array1) as $key) {
					$this->mDump .= "key: $key  - val1: ".stripslashes($array2[$key])."  val2: ".stripslashes($array1[$key])."\n";
					
					if (stripslashes($array2[$key]) != stripslashes($array1[$key]))
						return false;
				}	
			}
			
			catch (exception $ex){
				echo $ex->getMessage();
				return false;
			}
			
			return true;
		}
		
		/**
		 * Method to truncate data from the given table name
		 * @access public
		 * @param string $table_name
		 * @throws SqlQueryException 
		 */
		
		public function truncateTable($table_name){
			$query = "TRUNCATE TABLE $table_name";
 			
 			try {
				self::$handler->execute($query);
			}
			
			catch (exception $ex){
				throw new SqlQueryException($ex->getMessage());
			}			
		}	
		
		/**
		 * Method to insert test data to database
		 * @access public
		 * @param string $table
		 */
		 
		public function insertToDB($table, $data){
			$ctr = 0;
			$query = "INSERT INTO $table("; 
			
			foreach (array_keys($data) as $key){
				$query .= $key;
				
				if (($ctr + 1) < count($data)) 
					$query = $query.", ";
				
				$ctr++;	
			}
			
			$query .= ") VALUES (";
			$ctr = 0; 
			
			foreach ($data as $datum){
				$query .= $datum;
				
				if (($ctr + 1) < count($data)) 
					$query = $query.", ";
				
				$ctr++;	
			}
						
			$query .= ")";
			
 			try {
				$result = self::$handler->execute($query);
			}
			
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}					
		}	
		
		/**
		 * Method to multiple insert data to the given table in the database.
		 * @access public
		 * @param string $table_name the table name where you want to insert
		 * @param array $insertData the data is an array of an associative array
		 * @throws SqlQueryException
		 */
		 
		public function insertMultipleDataToTable($table_name, $insertData){
			// make sure that the new data inserted is a fresh data
			$this->truncateTable($table_name);
			
			foreach ($insertData as $data) {
			  try {
				  $this->insertDataToTable($table_name,$data);
			  }
			  
			  catch (SqlQueryException $ex){
			  	throw new SqlQueryException($ex->getMessage());
			  }
			}				
		}
		
		/**
		 * Method to insert an associative array to the database.
		 * The keys in the associative array is the column names of the
		 * given table in the database.
		 * @access public
		 * @param string $table_name
		 * @param array $data an associative array
		 * @throws SqlQueryException
		 */
		 
		public function insertDataToTable($table_name,$data){
			$ctr = 0;
			$query = "INSERT INTO $table_name("; 
			
			foreach (array_keys($data) as $key) {
				$query .= "`".$key."`";
				
				if (($ctr + 1) < count($data)) 
					$query .= ", ";
				
				$ctr++;	
			}
			
			$query .= ") VALUES (";
			$ctr = 0; 
			
			foreach ($data as $datum) {
				if (!is_int($datum)) 
				  $query .= GaString::makeSqlSafe($datum);
				
				else 
					$query .= $datum;
				
				if (($ctr + 1) < count($data)) 
					$query .= ", ";
				
				$ctr++;	
			}			
			
			$query .= ")";
			
 			try{
				$result = self::$handler->execute($query);
			}
			
			catch(exception $ex){
				throw new SqlQueryException($ex->getMessage());
			}				
		}
		
		/**
		 * Method to show the activities done in comparing two arrays
		 * in isArrayValuesSame method
		 * @access public
		 */
		 
		public function showIsArrayValuesSameComparing() {
			echo $this->mDump;
		}
		
		/**
		 * Method to show the activities done in comparing two arrays
		 * in isMultiArrayValuesSame method
		 * @access public
		 */
		 
		public function showIsMultiArrayValuesSameComparing() {
			echo $this->mMultiDump;
		}		
	}
