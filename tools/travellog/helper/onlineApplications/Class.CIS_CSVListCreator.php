<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.HousingOptionsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.DurationOptionsPeer.php');

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.UserCouponPeer.php');


class CIS_CSVListCreator{


	public static function create($objArr){

		 require_once 'travellog/model/onlineApplications/CIS_onlineApplication/Class.ProgramInformation2.php';
		 $comment = new ProgramInformation2();
			$perId = $comment->getComment();
			$commentMess = $perId['comment'];

		 $learnfrom = new ProgramInformation2();
			$learnOther = $learnfrom->getlearnFromOthers();
			$learnF = $learnOther['Others'];

/*additional info */
		$getcampus = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getCampusRep();

		$getcampus2 = "";
		$getcampus3 = "";
		if ($getcampus == ""){
			$getcampus2 = "No";
		}
		else{
		$getcampus2 = "Yes";
		$getcampus3 = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getCampusRep();
		}

		$getfriendgrant =  $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFriendGrant();
		$getfriendgrant2 = "";
		$getfriendgrant3 = "";

		if ($getfriendgrant == ""){
			$getfriendgrant2 = "No";
		}
		else{
		$getfriendgrant2 = "Yes";
		$getfriendgrant3 = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFriendGrant();
		}



		$yrInSchool = array('1' => 'Freshman', '2' => 'Sophomore', '3' => 'Junior', '4' => 'Senior', '5' => 'College/University Graduate', '6' => 'High School Graduate');

		$learnFromArr = array(
							'0' => $learnF,
							'1' => 'CIS Alumni Referral', '2' => 'CIS Representative', 	'3' => 'CIS Website',
							'4' => 'Diversity Abroad',	 '5' => 'Faculty', 				'6' => 'GoAbroad.com',			'7'  => 'Golden Key',
							'8' => 'Google', 			'9' => 'IIE Passport', 			'10' => 'My Universities Website','11'  => 'Overseas University Website',
							'12' => 'Student Referral', 	'13' => 'Study Abroad Fair','14' => 'Study Abroad Office',	'15'  => 'StudyAbroad.com',
							'16' => 'Visit Import',		 '17' => 'Yahoo',				 '18' => 'Study Abroad Office',	'19'  => 'Other Website'
							);

		$housingOption = HousingOptionsPeer::retrieveByPk($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getHousingOptionID());

		$durationOption = DurationOptionsPeer::retrieveByPk($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getDurationOptionID());

		$dateCSVcreated = Date('d M Y');

		$dateApplied =Date("Y-M-d", strtotime($objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getApplicationDate()));


		$bday         = Date("F d, Y", strtotime($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBirthday()));




		$learn        = (0 < $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom() ? CISOnlineAppConst::$addInfoOptKeys[$objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom()] : "");

		$userCoupon = UserCouponPeer::retrieveByPersonalInfoID($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getID());
		$couponCode = "";
		if(!is_null($userCoupon)){
			$couponCode = $userCoupon->getName();
		}

		$field_val = array(
			'id_1' => 'CIS',
			'id_2' => 'Student',
			'id_3' => 'Study Abroad',
			'id_4' => 'Applicant',
			'id_5' => 'Applicant',
			'id_6' => $dateApplied,
			'id_7' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName(),
			'id_8' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName(),
			'id_9' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getPreferredName(),
			'id_10' => (1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getGender()) ? 'Male' : 'Female',
			'id_11' => $bday,
			'id_12' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail(),
			'id_13' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getAltEmail(),
			'id_14' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1(),
			'id_15' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1(),
			'id_16' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1(),
			'id_17' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1(),
			'id_18' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1(),
			'id_19' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName(),
			'id_20' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName(),
			'id_21' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName(),
			'id_22' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCellphone(),
			'id_23' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName(),
			'id_24' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getStateAbbr(),
			'id_65' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getContact(),
			'id_66' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getEmail(),
			'id_25' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMajor(),
			'id_26' => $yrInSchool[$objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getYearInSchool()],
			'id_27' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getGenAverage(),
			'id_28' => $learn,
			'id_29' => $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers(),
			'id_30' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName(),
			'id_31' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName(),
			'id_32' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName(),
			'id_33' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail(),
			'id_34' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTermName(),
			'id_35' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear(),
			'id_36' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear(),
			'id_37' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTermName(),
			'id_38' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramName(),
			'id_39' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionName(),
			'id_40' => (!is_null($housingOption)) ? $housingOption->getName() : "",
			'id_41' => (!is_null($durationOption)) ? $durationOption->getName() : "",
			'id_42' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartMonth(),
			'id_43' => (0 < $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartDay()) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartDay() : "",
			/*'id_44' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getInterest(),

			'id_45' => '', $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement1(),
			'id_46' => '',$objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement2(),
			'id_47' => '',	$objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement3(),
			'id_48' => '',	(1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getLateOption() ? 'Yes' : ''),
				*/
			'id_49' => $dateApplied,
			'id_50' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName(),
			'id_51' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName(),
			'id_52' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail(),
			'id_53' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName(),
			'id_54' => strval($dateCSVcreated),
			'id_55' => $couponCode,
			'id_56' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBillingEmail(),
			/*'id_57' => $learnFromArr[$objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom()], */
			'id_58' => (0 == $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFinancialAid()) ? 'No' : 'Yes',
			'id_59' => $getcampus2,
			'id_60' => $getcampus3,
			'id_61' => $getfriendgrant2,
			'id_62' => $getfriendgrant3,
			'id_63' => $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getGoAgainGrant(),
			'id_65' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getShirtSize(),
			'id_64' => $commentMess
			);

		$field_keys = array(
			'id_1' => 'CIS Type',
			'id_2' => 'Contact Type',
			'id_3' => 'Contact Sub Type',
			'id_4' => 'Status',
			'id_5' => 'Status Programs',
			'id_6' => 'Date of Application',
			'id_7' => 'First Name',
			'id_8' => 'Last Name',
			'id_9' => 'Preferred Name',
			'id_10' => 'Sex',
			'id_11' => 'Birthdate',
			'id_12' => 'Email',
			'id_13' => 'Email #2',
			'id_14' => 'Current Address1',
			'id_15' => 'Current City',
			'id_16' => 'Current State',
			'id_17' => 'Current Zip',
			'id_18' => 'Current Phone',
			'id_19' => 'School/Company ',
			'id_20' => 'First Name Repeat',
			'id_21' => 'Last Name Repeat',
			'id_22' => 'Cellphone',
			'id_23' => 'School/Company',
			'id_24' => 'School State',
			'id_65' => 'Name of your on-campus study abroad contact',
			'id_66' => 'Email',
			'id_25' => 'Field of study',
			'id_26' => 'Current year in school',
			'id_27' => 'Current GPA',
			'id_28' => 'Inquiry Source',
			'id_29' => 'Other',
			'id_30' => 'First Name Repeat',
			'id_31' => 'Last Name Repeat',
			'id_32' => 'School/Company Repeat',
			'id_33' => 'Email Repeat',
			'id_34' => 'Program Term',
			'id_35' => 'Program Year',
			'id_36' => 'Contact Year',
			'id_37' => 'Contact Term',
			'id_38' => 'Programs',
			'id_39' => 'Program Option',
			'id_40' => 'Housing Option',
			'id_41' => 'Duration Option',
			'id_42' => 'Month you would like to start your internship',
			'id_43' => 'Day of month to start your internship',
			/*'id_44' => 'Interested in Full Year',
			'id_45' => 'Academic/Career Goals',
			'id_46' => 'Cultural Understanding',
			'id_47' => 'Personal Growth',
			'id_48' => 'Answer Essay Later', */
			'id_49' => 'Date of Application',
			'id_50' => 'First Name Repeat',
			'id_51' => 'Last Name Repeat',
			'id_52' => 'Email Repeat',
			'id_53' => 'School/Company Repeat',
			'id_54' => 'Last Change Date',
			'id_55' => 'Coupon Code',
			'id_56' => 'Billing Email',
			/*'id_57' => 'How did you learn about this CIS program?', */
			'id_58' => 'Financial Aid',
			'id_59' => 'CIS Campus Rep/Alumni Referral',
			'id_60' => 'Name of CIS Campus Rep/Alumni',
			'id_61' => 'Bring a Friend Grant',
			'id_62' => 'Name of Friend',
			'id_63' => 'Go Again Grant',
			'id_65' => 'Shirt Size',
			'id_64' => 'Additional Comments'
			);


		$list = array(
			array_values($field_keys),
			array_values($field_val)
		);

		return $list;

	}
}

