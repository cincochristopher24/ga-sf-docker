<?php


require_once('travellog/model/Class.Country.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');


class CIS_CSVListCreator{
	
	public static function create($objArr){
		
		$country = new Country($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getCountryBirthPlace());
		$yrInSchool = array('1' => 'Freshman', '2' => 'Sophomore', '3' => 'Junior', '4' => 'Senior', '5' => 'College/University Graduate', '6' => 'High School Graduate');
		$priorityTxt = array('','Program 1 is my first choice, and Program 2 is my second choice.', 'I am equally interested in both Program 1 and Program 2.');


		/*
		$name         = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getName();
		$fname        = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName();
		$lname        = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName();
		$preferred    = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getPreferredName();
		$gender       = (1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getGender()) ? 'Male' : 'Female';
		$bday         = Date("F d, Y", strtotime($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBirthday()));
		$citizenship  = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getCitizenship();
		$bplace       = $country->getName();
		$ss           = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getSecNumber();
		$email        = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail();
		$email2       = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getAltEmail();
		$street       = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1();
		$city         = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1();
		$state        = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1();
		$zip          = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1();
		
		$permanent    = (1 == $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPermanentAddress()) ? 'Yes': 'No';
		$pstreet       = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet2();
		$pcity         = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity2();
		$pstate        = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState2();
		$pzip          = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip2();
		$phone        = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1();
		$phone2        = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber2();
		$cellPhone    = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCellphone();
		
		
		
		$eName        = $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getName();
		$efName        = $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getFirstName();
		$elName        = $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getLastName();
		$eRel         = $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getRelationship();
		$eStreet      = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getStreet1();
		$eCity        = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getCity1();
		$eState       = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getState1();
		$eZip         = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getZip1();
		$ePhone       = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getPhoneNumber1();
		$needs        = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getSpecialNeeds();
		
		$sName        = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName();
		$sMajor       = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMajor();
		$sMinor       = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMinor();
		$yrInSchool   = $yrInSchool[$objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getYearInSchool()];
		$average      = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getGenAverage();
		$courses      = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getCurrentCourses();
		$learn        = (0 < $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom() ? CISOnlineAppConst::$addInfoOptKeys[$objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom()] : "");
		$learnOther   = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers();
		
		$term         = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTermName();
		$yr           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear();
		$p1           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramName();
		$o1           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionName();
		$p2           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondProgramName();
		$o2           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondOptionName();
		$priority     = $priorityTxt[$objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getPriorityOption()];
		$fullYr       = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getInterest();
		$volunteer    = (2 == $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getVolunteering() ? 'Yes' : 'No');
		
		$statement1   = $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement1();
		$statement2   = $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement2();
		$statement3   = $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement3();

		*/
		
		$dateCSVcreated = Date('d M Y');

		$dateApplied = Date("Y-M-d", strtotime($objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getApplicationDate()));
		$bday         = Date("F d, Y", strtotime($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBirthday()));
		
		$permanent    = (1 == $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPermanentAddress()) ? 'Yes': 'No';
		$pstreet       = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet2();
		$pcity         = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity2();
		$pstate        = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState2();
		$pzip          = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip2();
		$phone2          = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber2();
		
		$passport     = isset($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]->getPassportNumber() : '';
		$expiry       = isset($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]) ? Date('Y-m',strtotime($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]->getExpiryDate())) : '';
		
		$learn        = (0 < $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom() ? CISOnlineAppConst::$addInfoOptKeys[$objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom()] : "");
		
		
		$payingPersons = (isset($objArr[CISOnlineAppFieldsMapper::BILLING_INFO]) ? $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getPayingPersons() : 0); 

		$yourSelf = (($yourSelf = $payingPersons & 1) == 1) ? 'You' : '';
		$parents  = (($parents  = $payingPersons & 2) == 2) ? 'Parent' : '';
		$aid      = (($aid      = $payingPersons & 4) == 4) ? 'Aid' : '';

		$pay       = $yourSelf.' '.$parents.' '.$aid;
		
		
		
		
		$field_val = array(
			'id_1' => 'CIS',
			'id_2' => 'Student',
			'id_3' => 'Study Abroad',
		//	'id_4' => 'Online Applicant', // changed to Applicant
		//	'id_5' => 'Online Applicant', // changed to Applicant
			'id_4' => 'Applicant',
			'id_5' => 'Applicant',
			'id_6' => $dateApplied,
			'id_7' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName(),
			'id_8' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName(),
			'id_9' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getPreferredName(),
			'id_10' => (1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getGender()) ? 'Male' : 'Female',
			'id_11' => $bday,
			'id_12' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getCitizenship(),
			'id_13' => $country->getName(),
			'id_14' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getSecNumber(),
			'id_15' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail(),
			'id_16' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getAltEmail(),
			'id_17' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1(),
			'id_18' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1(),
			'id_19' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1(),
			'id_20' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1(),
			'id_21' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1(),
			'id_22' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName(),
			'id_23' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName(),
			'id_24' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName(),
			'id_25' => $pstreet,
			'id_26' => $pcity,
			'id_27' => $pstate,
			'id_28' => $pzip,
			'id_29' => $phone2,
			'id_30' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCellphone(),
			'id_31' => $passport,
			'id_32' => $expiry,
			'id_33' => $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getFirstName(),
			'id_34' => $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getLastName(),
			'id_35' => $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getRelationship(),
			'id_36' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getStreet1(),
			'id_37' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getCity1(),
			'id_38' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getState1(),
			'id_39' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getZip1(),
			'id_40' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getPhoneNumber1(),
			'id_41' => $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getSpecialNeeds(),
			'id_42' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName(),			
			'id_43' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getStateAbbr(),			
			'id_44' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMajor(),
			'id_45' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMinor(),
			'id_46' => $yrInSchool[$objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getYearInSchool()],
			'id_47' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getGenAverage(),
			'id_48' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getCurrentCourses(),
			'id_49' => $learn,
			'id_50' => $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers(),
			'id_51' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName(),
			'id_52' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName(),
			'id_53' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName(),
			'id_54' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail(),
			'id_55' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTermName(),
			'id_56' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear(),
			'id_57' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear(),
			'id_58' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTermName(),
			'id_59' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramName(),
			'id_60' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionName(),
			'id_61' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondProgramName(),
			'id_62' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondOptionName(),
			'id_63' => $priorityTxt[$objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getPriorityOption()],
			'id_64' => $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getInterest(),
			'id_65' => (2 == $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getVolunteering() ? 'Yes' : 'No'),
			'id_66' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement1(),
			'id_67' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement2(),
			'id_68' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement3(),
			'id_69' => (1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getLateOption() ? 'Yes' : ''),
			'id_70' => $pay,
			'id_71' => $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getFirstName(),
			'id_72' => $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getLastName(),
			'id_73' => $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getRelationship(),
			'id_74' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getStreet1(),
			'id_75' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getCity1(),
			'id_76' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getState1(),
			'id_77' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getZip1(),
			'id_78' => $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getPhoneNumber1(),
			'id_79' => $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getEmail(),
			'id_80' => $dateApplied,
			'id_81' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName(),
			'id_82' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName(),
			'id_83' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail(),
			'id_84' => $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName(),
			'id_85' => strval($dateCSVcreated)
		);
		
		$field_keys = array(
			'id_1' => 'CIS Type',
			'id_2' => 'Contact Type',
			'id_3' => 'Contact Sub Type',
			'id_4' => 'Status',
			'id_5' => 'Status Programs',
			'id_6' => 'Date of Application',
			'id_7' => 'First Name',
			'id_8' => 'Last Name',
			'id_9' => 'Preferred Name',
			'id_10' => 'Sex',
			'id_11' => 'Birthdate',
			'id_12' => 'Citizenship',
			'id_13' => 'Country of Birth',
			'id_14' => 'SS#',
			'id_15' => 'Email',
			'id_16' => 'Email #2',
			'id_17' => 'Current Address1',
			'id_18' => 'Current City',
			'id_19' => 'Current State',
			'id_20' => 'Current Zip',
			'id_21' => 'Current Phone',
			'id_22' => 'School/Company ',
			'id_23' => 'First Name Repeat',
			'id_24' => 'Last Name Repeat',
			'id_25' => 'Permanent Street Address',
			'id_26' => 'Permanent City',
			'id_27' => 'Permanent State',
			'id_28' => 'Permanent Zip',
			'id_29' => 'Permanent Phone',
			'id_30' => 'Cellphone',
			'id_31' => 'PassportNumber',
			'id_32' => 'Passport Expiration Date',
			'id_33' => 'Emergency First Name',
			'id_34' => 'Emergency Last Name',
			'id_35' => 'Emergency Contact Relationship',
			'id_36' => 'Emergency Address',
			'id_37' => 'Emergency City',
			'id_38' => 'Emergency State',
			'id_39' => 'Emergency Zip',
			'id_40' => 'Emergency Phone #1',
			'id_41' => 'Special needs',
			'id_42' => 'School/Company',
			'id_43' => 'School State',
			'id_44' => 'Field of study',
			'id_45' => 'Minor',
			'id_46' => 'Current year in school',
			'id_47' => 'Current GPA',
			'id_48' => 'Courses not on Transcript',
			'id_49' => 'Inquiry Source',
			'id_50' => 'Other',
			'id_51' => 'First Name Repeat',
			'id_52' => 'Last Name Repeat',
			'id_53' => 'School/Company Repeat',
			'id_54' => 'Email Repeat',
			'id_55' => 'Program Term',
			'id_56' => 'Program Year',
			'id_57' => 'Contact Year',
			'id_58' => 'Contact Term',
			'id_59' => 'Programs',
			'id_60' => 'Program Option',
			'id_61' => 'Program2',
			'id_62' => 'Option2',
			'id_63' => 'Program Priority',
			'id_64' => 'Interested in Full Year',
			'id_65' => 'Interested in Green Volunteer',
			'id_66' => 'Academic/Career Goals',
			'id_67' => 'Cultural Understanding',
			'id_68' => 'Personal Growth',
			'id_69' => 'Answer Essay Later',
			'id_70' => 'Paying Bill',
			'id_71' => 'Billing First Name',
			'id_72' => 'Billing Last Name',
			'id_73' => 'Billing Contact Relationship',
			'id_74' => 'Billing Address',
			'id_75' => 'Billing City',
			'id_76' => 'Billing State',
			'id_77' => 'Billing Zip',
			'id_78' => 'Billing Phone #1',
			'id_79' => 'Billing Email',
			'id_80' => 'Date of Application',
			'id_81' => 'First Name Repeat',
			'id_82' => 'Last Name Repeat',
			'id_83' => 'Email Repeat',
			'id_84' => 'School/Company Repeat',
			'id_85' => 'Last Change Date'
		);
		
		$list = array(
			array_values($field_keys),
			array_values($field_val)
		);

		return $list;
	}
}

