<?php

	class GroupPagesHelper{
		
		const RPP = 10;
		
		private static $instance = NULL;
		
		private function __construct(){
		}
		
		public static function getInstance(){
			if( is_null(self::$instance) ){
				self::$instance = new GroupPagesHelper();
			}
			return self::$instance;
		}
		
		public function readParameterTags($tags_str=""){
			if( is_array($tags_str) ){
				$temp = $tags_str;
			}else{
				$temp = explode(" ",$tags_str);
			}
			$tag_array = array();
			foreach($temp AS $tag){
				if( "" != $tag && !$this->tagExists($tag_array,$tag) ){
					$tag_array[] = $tag;
				}
			}
			return $tag_array;
		}
		
		private function tagExists($tags,$tag){
			foreach($tags AS $t){
				if( strtolower($t) == strtolower($tag) ){
					return TRUE;
				}
			}
			return FALSE;
		}
		
		public function getTagUrl(GanetPageTag $tag){
			if(isset($GLOBALS["CONFIG"]) &&  isset($_GET["search"]) ){
				return "/group-pages.php?search&tags=".$tag->getName();
			}else{
				return "/group-pages.php?gID=".$tag->getGroupID()."&tags=".$tag->getName();
			}
		}
		
		public function getTraceTagsUrl($groupID=0, $trace_tags=array()){
			$tags_param = "&tags=".implode("+",$trace_tags);
			if(isset($GLOBALS["CONFIG"]) &&  isset($_GET["search"]) ){
				return "/group-pages.php?search".$tags_param;
			}else{
				return "/group-pages.php?gID=".$groupID.$tags_param;
			}
		}
		
		public function getDefaultPageUrl($groupID=0){
			if(isset($GLOBALS["CONFIG"]) &&  isset($_GET["search"]) ){
				return "/group-pages.php?search";
			}else{
				return "/group-pages.php?gID=".$groupID;
			}
		}
		
		public function getDefaultPageQueryString($groupID=0){
			if(isset($GLOBALS["CONFIG"]) &&  isset($_GET["search"]) ){
				return "?search";
			}else{
				return "?gID=".$groupID;
			}
		}
		
		public function isAjaxRequest(){
			if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
				return TRUE;
			}
			if(	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				return TRUE;
			}
			return FALSE;
		}
		
		public function getRowsLimit($page=1){
			$offset = ($page-1) * self::RPP;
			$offset = 0 > $offset ? 0 : $offset; 
			$rowsLimit = new RowsLimit(self::RPP, $offset);
			return $rowsLimit;
		}
	}