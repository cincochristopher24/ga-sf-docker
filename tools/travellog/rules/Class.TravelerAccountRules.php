<?php
	/**
	 * @(#) Class.TravelerAccountRules.php
	 * 
	 * @author
	 * @version 1.0 - September 10, 2009
	 */
	
	//require_once('travellog/model/Class.SessionManager.php');
	require_once("Class.Constants.php");
	require_once("travellog/model/Class.TravelerPeer.php");
	
	/**
	 * Rules of the traveler account
	 */
	class TravelerAccountRules {
		
		/**
		 * Validates the data for username if it already exists.
		 * 
		 * @param array $data The data used for the action validation.
		 * 
		 * @return bool
		 */
		static function validateTravelerUsername($username){
			$c = new Criteria2();
			$c->add('username' ,'"'.$username.'"', Criteria2::EQUAL);
			$c->add('deactivated' ,0, Criteria2::EQUAL);
			return !(0 < TravelerPeer::doCount($c));
		}
						
		/**
		 * Validates the data for email if it already exists.
		 * 
		 * @param array $data The data used for the action validation.
		 * 
		 * @return bool
		 */
		static function validateTravelerEmail($email){
			$c = new Criteria2();
			$c->add('email' ,'"'.$email.'"', Criteria2::EQUAL);
			$c->add('deactivated' ,0, Criteria2::EQUAL);
			return !(0 < TravelerPeer::doCount($c));
		}
		
		/**
		 * EDIT HISTORY
		 *
		 * Date Edited: Sept 11, 2009
		 * Edited By: Augustianne Laurenne L. Barreta
		 * added username limit; changed error messages
		**/
		static function getUsernameErrorMessage($username){
			$arrErrorMessage = array();
			$invalidUserNames = array(strtolower("travelBio"));
			
			if( 0 == strlen($username) )
				$arrErrorMessage[constants::EMPTY_USERNAME] = "Username must be filled out.";
			elseif(25 < strlen($username))
				$arrErrorMessage[constants::USERNAME_EXCEEDS_LIMIT] = "Username must not exceed 25 characters.";
			//elseif( strlen($username) != strlen(ereg_replace("[^A-Za-z0-9_]", "", $username)))
			elseif( strlen($username) != strlen(preg_replace("/[^A-Za-z0-9_]/", "", $username)))
				$arrErrorMessage[constants::INVALID_USERNAME_FORMAT] = "Username must be valid.";
			else{
				// check if username is already used and push in error array
				if (!self::validateTravelerUsername($username) || in_array(strtolower($username), $invalidUserNames))
					$arrErrorMessage[constants::USERNAME_ALREADY_USED] = "The username <strong><em>" .$username. "</em></strong> is not available.";
			}
			
			return $arrErrorMessage;
		}
		
		static function getPasswordErrorMessage($password, $pwdConfirm){
			$arrErrorMessage = array();
		
			if( 6 > strlen($password) )
				$arrErrorMessage[constants::INVALID_PASSWORD_FORMAT] = "Password must be at least 6 characters.";
			elseif( 0 <> strcmp($password,$pwdConfirm) )
				$arrErrorMessage[constants::PASSWORD_CONFIRMATION_FAILED] = "The passwords you have entered did not match. Please reenter your password.";
			
			return $arrErrorMessage;
		}
		
		static function getEmailErrorMessage($email){
			require_once("Class.Validator.php");
						
			$validator = Validator::getInstance();
			
			$arrErrorMessage = array();
			
			if (0 == strlen($email))
				$arrErrorMessage[constants::EMPTY_EMAIL_ADDRESS] = "Email must be filled out.";
			elseif ($validator->isEmail($email)){
				// check if email is not in use push in error array if used
				if (!self::validateTravelerEmail($email))
					$arrErrorMessage[constants::EMAIL_ALREADY_USED] = "An account has already been created using " . $email . ", please provide another.";
			}
			else
				$arrErrorMessage[constants::INVALID_EMAIL_FORMAT] = "Please check and try again.";
			
			return $arrErrorMessage;
		}
	}