<?php
	/**
	 * @(#) Class.DiscussionRules.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - May 7, 2009
	 */
	
	require_once("travellog/model/Class.SessionManager.php");
	
	/**
	 * Handles the rules and validation for discussions.
	 */
	class DiscussionRules {
		
		/**
		 * Validates the data in unfeaturing discussion.
		 * 
		 * @param array $data The data to be validated.
		 * 
		 * @return void
		 */
		static function validateActivatingDiscussion(&$data) {
			require_once("travellog/model/Class.DiscussionPeer.php");
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			
			if (0 < $data['travelerID'] AND !is_null($data['discussion'])) {
				$data['topic'] = $data['discussion']->getParentTopic();
				$data['group'] = $data['topic']->getParentGroup();
				$data['isAdmin'] = ($data['group']->getAdministratorID() == $data['travelerID'] || $data['group']->isStaff($data['travelerID']));
				if ($data['isAdmin']) {
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Validates the data in adding a discussion to the knowledge base.
		 * 
		 * @param array $data The data used for the action validation.
		 * 
		 * @return void
		 */
		static function validateAddingToKnowledgeBase(&$data){
			require_once("travellog/model/Class.DiscussionPeer.php");
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			
			if (0 < $data['travelerID'] AND !is_null($data['discussion'])) {
				$data['topic'] = $data['discussion']->getParentTopic();
				$data['group'] = $data['topic']->getParentGroup();
				$data['isAdmin'] = ($data['group']->getAdministratorID() == $data['travelerID'] || $data['group']->isStaff($data['travelerID']));
				if ($data['isAdmin'] AND 0 < $data['group']->getParentID()) {
					return true;
				}
			}
			
			return false;			
		}
		
		/**
		 * Validates the data in unfeaturing discussion.
		 * 
		 * @param array $data The data to be validated.
		 * 
		 * @return void
		 */
		static function validateArchivingDiscussion(&$data) {
			require_once("travellog/model/Class.DiscussionPeer.php");
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			
			if (0 < $data['travelerID'] AND !is_null($data['discussion'])) {
				$data['topic'] = $data['discussion']->getParentTopic();
				$data['group'] = $data['topic']->getParentGroup();
				$data['isAdmin'] = ($data['group']->getAdministratorID() == $data['travelerID'] || $data['group']->isStaff($data['travelerID']));
				if ($data['isAdmin']) {
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Validates the data in featuring a group.
		 * 
		 * @param array $data The data to be validated.
		 * 
		 * @return void
		 */
		static function validateFeaturingDiscussion(&$data) {
			require_once("travellog/model/Class.DiscussionPeer.php");
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			
			if (0 < $data['travelerID'] AND !is_null($data['discussion'])) {
				$data['topic'] = $data['discussion']->getParentTopic();
				$data['group'] = $data['topic']->getParentGroup();
				$data['isAdmin'] = ($data['group']->getAdministratorID() == $data['travelerID'] || $data['group']->isStaff($data['travelerID']));
				if ($data['isAdmin']) {
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Validates the data in removal of discussion from the knowledge base.
		 * 
		 * @param array $data The data used for the action validation.
		 * 
		 * @return void
		 */	
		static function validateRemovingFromKnowledgeBase(&$data){
			require_once("travellog/model/Class.DiscussionPeer.php");
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			
			if (0 < $data['travelerID'] AND !is_null($data['discussion'])) {
				$data['topic'] = $data['discussion']->getParentTopic();
				$data['group'] = $data['topic']->getParentGroup();
				$data['isAdmin'] = ($data['group']->getAdministratorID() == $data['travelerID'] || $data['group']->isStaff($data['travelerID']));
				if ($data['isAdmin'] AND 0 < $data['group']->getParentID()) {
					return true;
				}
			}
			
			return false;			
		}
		
		/**
		 * Validates the data in unfeaturing discussion.
		 * 
		 * @param array $data The data to be validated.
		 * 
		 * @return void
		 */
		static function validateUnfeaturingDiscussion(&$data) {
			require_once("travellog/model/Class.DiscussionPeer.php");
			
			$session = SessionManager::getInstance();
			$data['travelerID'] = $session->get('travelerID');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			
			if (0 < $data['travelerID'] AND !is_null($data['discussion'])) {
				$data['topic'] = $data['discussion']->getParentTopic();
				$data['group'] = $data['topic']->getParentGroup();
				$data['isAdmin'] = ($data['group']->getAdministratorID() == $data['travelerID'] || $data['group']->isStaff($data['travelerID']));
				if ($data['isAdmin']) {
					return true;
				}
			}
			
			return false;
		}
	}
	
