<?php
	/**
	 * @(#) Class.PostRules.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - May 8, 2009
	 */
	
	require_once('travellog/model/Class.SessionManager.php');
	
	/**
	 * Rules for discussion posts.
	 */
	class PostRules {
		/**
		 * Rules for deleting posts.
		 * 
		 * @param array $data The data where the validation of the post will be based.
		 * 
		 * @return boolean true if the data is valid; false otherwise.
		 */
		function validateDeletePost(&$data){
			if (isset($data['postID'])) {
				require_once("travellog/model/Class.PostPeer.php");
				
				$session = SessionManager::getInstance();
				$data['travelerID'] = $session->get('travelerID');
				$data['post'] = PostPeer::retrieveByPk($data['postID']);
				
				if (0 < $data['travelerID'] AND !is_null($data['post'])) {
					$data['discussion'] = $data['post']->getParentDiscussion();
					$data['topic'] = $data['discussion']->getParentTopic();
					$data['group'] = $data['topic']->getParentGroup();
					$data['isAdmin'] = ($data['group']->getAdministratorID() == $data['travelerID'] || $data['group']->isStaff($data['travelerID']));
					if ($data['isAdmin'] OR $data['travelerID'] == $data['post']->getPosterID()) {		
						return true;
					}
				}
			}
			
			return false;
		}
	}
	
