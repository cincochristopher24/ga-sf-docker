<?php
class Utils{
	
	static function getTravelerIDByUsername( &$props, $params ){
		if( array_key_exists('USERNAME', $params) ){
			require_once("travellog/model/Class.Traveler.php");	
			$props['obj_traveler'] = new Traveler;
			$props['travelerID']   = $props['obj_traveler']->TravelerByUsername( $params['USERNAME'] );
		}
	}
	
	static function getTravelerIDByGroupName( &$props, $params ){
		if( array_key_exists('GROUPS', $params) ){
			require_once("travellog/model/Class.AdminGroup.php");
			
			$group_name = str_replace('_', '-', $params['GROUPS'] );
			
			$props['groupID']        = AdminGroup::getGroupIDByName( $group_name );
			$props['group']          = new AdminGroup( $props['groupID'] );
			$props['group_parentID'] = (Group::FUN == $props['group']->getDiscriminator()) ? 0 : $props['group']->getParentID();
			$props['travelerID']     = $props['group']->getAdministratorID(); 
		}
	}
	
	static function getTravelerIDBySubGroupName( &$props, $params ){
		if( array_key_exists('SUBGROUP', $params) ){
			require_once("travellog/model/Class.AdminGroup.php");
			
			$subgroup_name = str_replace('_', '-', $params['SUBGROUP'] );
			$groups     = $props['group']->getSubGroups(NULL, NULL, false, $subgroup_name);
			$group 			= $groups[0];
			$props['parentID']   = $group->getParentID();
			$props['travelerID'] = $group->getAdministratorID();    
			$props['subgroupID'] = $group->getGroupID();
		}
	}
	
	static function getJournalOwner( &$props, $params ){
		if( array_key_exists('JOURNALS', $params) ){
			
			if( $props['travellogID'] ){
				require_once("travellog/model/Class.TravelLog.php");
				$obj_entry = new TravelLog( $props['travellogID'] );
				$obj       = $obj_entry->getOwner();
				if( get_class($obj) == 'Traveler' ){
					$props['ownerID'] = $obj->getTravelerID();
					$_GET['ownerID']  = $obj->getTravelerID();
				}else{
					$props['ownerID'] = $obj->getGroupID();
					$_GET['ownerID']  = $obj->getGroupID();
				}   
			}else{
				
				require_once("travellog/facade/Class.JournalManagementFcd.php");
				$fcd              = new JournalManagementFcd;
				$obj_journal      = $fcd->retrieve( $params['JOURNALS'] );
				$obj_owner        = $obj_journal->getOwner();
				if( get_class($obj_owner) == 'Traveler' ){
					$props['ownerID'] = $obj_owner->getTravelerID();
					$_GET['ownerID']  = $obj_owner->getTravelerID();
				}else{
					$props['ownerID'] = $obj_owner->getGroupID();
					$_GET['ownerID']  = $obj_owner->getGroupID();   
				}	  
			}
		}
	}
	
}
?>
