<?php
require_once("travellog/route/model/Class.gaAbstractAssessorActionFactory.php");
require_once("travellog/route/rules/Class.IsTravelerJournalOwner.php");
require_once("travellog/route/rules/Class.IsSubGroupJournalOwner.php");
require_once("travellog/route/rules/Class.IsGroupJournalOwner.php");
require_once("travellog/route/rules/Class.IsSubGroupOwner.php");
require_once("travellog/route/rules/Class.HasAnEntry.php");
require_once("travellog/route/rules/Class.HasAParameterOf.php");
class JournalAssessorActionFactory extends gaAbstractAssessorActionFactory{
	static function getInstance(){
		if( self::$instance == NULL ) self::$instance = new JournalAssessorActionFactory; 
		return self::$instance;
	}
	
	function createAssessors(){
		$assessors = array();
		
		$assessors['IsTravelerJournalOwner'] = new IsTravelerJournalOwner;
		$assessors['IsSubGroupJournalOwner'] = new IsSubGroupJournalOwner;
		$assessors['IsGroupJournalOwner']    = new IsGroupJournalOwner;
		$assessors['IsSubGroupOwner']        = new IsSubGroupOwner;
		$assessors['HasAnEntry']             = new HasAnEntry;
		$assessors['HasAParameterOf']        = new HasAParameterOf;
		
		return $assessors;		
	}
	
	function createActions(){}
}
?>
