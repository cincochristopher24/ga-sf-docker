<?php
require_once("travellog/route/model/Class.gaIRule.php");
require_once("travellog/route/rules/Class.SubGroupAssessorActionFactory.php");
class SubGroupControllerDispatchRule implements gaIRule{
	function applyRules( $props ){
		$factory   = SubGroupAssessorActionFactory::getInstance();
		$assessors = $factory->createAssessors();
		
		if( !$assessors['IsValidGroup']->evaluate( $props ) )
			throw new Exception('Invalid group name.');
			
		if( !$assessors['IsSubGroupOwner']->evaluate( $props ) )	
			throw new Exception('Group is not the owner of sub group.');
	}
}
?>
