<?php
	/**
	 * @(#) Class.GroupsRouteMaps.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 10, 2009
	 */
	
	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/route/routeMaps/Class.AbstractRouteMaps.php');
	require_once('Class.Criteria2.php');
	require_once('travellog/model/Class.GroupPeer.php');
	require_once('travellog/model/Class.GanetDbHandler.php');
	
	/**
	 * Route class for groups.
	 */
	class GroupsRouteMaps extends AbstractRouteMaps {
		
		/**
		 * Handles the setting of correct keys to $_GET variable and calling the
		 * correct page(file).
		 * 
		 * @return void
		 */
		function dispatch(){
			require_once('vendor/autoload/GanetAutoloader.php');
				
			GanetAutoloader::getInstance()->register();
			GanetPropelRuntime::getInstance()->initPropel();
			
			if (1 < count($this->mParsedUri)) {
				$handler = GanetDbHandler::getDbHandler();				
				
				$criteria = new Criteria2();
				$criteria->add(GroupPeer::NAME, $handler->makeSqlSafeString($this->mParsedUri[1]));
				$criteria->add(GroupPeer::PARENT_ID, 0);
				
				$groups = GroupPeer::doSelect($criteria);
				
				if (0 < count($groups)) { // check if the name of a parent group exists if viewing a subgroup.
					$group = $groups[0];
					
					$context = isset($this->mParsedUri[2]) ? $this->mParsedUri[2] : "default";
					switch($context){
						case "subgroup":
							$this->dispatchSubGroup($group);
						break;
						case "journals":
							$this->dispatchJournal($group);
						break;
						default:
							$_GET['gID'] = $group->getGroupID();
							$obj_factory = ControllerFactory::getInstance();
							$obj_factory->createController('Groups')->performAction();
							exit;
					}
				}
			}
		}
		
		/**
		 * Assign the correct controller for viewing a journal entry.
		 * 
		 * @param AdvisorGroup|NodeGroup $parent The root group of the subgroup to be viewed.
		 * 
		 * @return void
		 */
		function dispatchJournal($parent){
			if (4 <= count($this->mParsedUri) AND (Group::FUN <> $parent->getDiscriminator())) {
				$session = SessionManager::getInstance();
				$_GET['entry_title'] = (isset($this->mParsedUri[4])) ? $this->mParsedUri[4] : "";
				$_GET['ownerID'] = $parent->getGroupID();
				$_GET['travelID'] = (is_numeric($this->mParsedUri[3])) ? $this->mParsedUri[3] : 0;
				$_GET['travelerID'] = $session->get('travelerID');
				$_GET['is_login'] = (0 == $session->get('travelerID')) ? false : true;
				$_GET['fail'] = (isset($this->mParsedUri[5]) AND "fail" == $this->mParsedUri[5]) ? 1 : 0; 
				
				$_GET['urlAlias'] = (isset($this->mParsedUri['JOURNAL_ENTRY_URL_ALIAS'])) ? $this->mParsedUri['JOURNAL_ENTRY_URL_ALIAS'] : "";
				$_GET['urlAlias'] = str_replace("?______array", "", $_GET['urlAlias']);
				
				$temp = explode("?", $_GET['urlAlias']);
				$_GET['urlAlias'] = count($temp) ? $temp[0] : $_GET['urlAlias'];
				
				require_once('travellog/model/Class.EntryModel.php');
				$entry = new EntryModel();
				$_GET['travellogID'] = $entry->permalink($_GET);
				
				$obj_factory = ControllerFactory::getInstance();
				$obj_factory->createController('Entry')->performAction();
				exit;
			}
		}
		
		/**
		 * Assign the page for viewing a subgroup.
		 * 
		 * @param AdvisorGroup|NodeGroup $parent The group of the subgroup to be viewed.
		 * 
		 * @return void
		 */
		function dispatchSubGroup($parent){
			$urlAlias = (isset($this->mParsedUri['JOURNAL_ENTRY_URL_ALIAS'])) ? $this->mParsedUri['JOURNAL_ENTRY_URL_ALIAS'] : "";
			unset($this->mParsedUri['JOURNAL_ENTRY_URL_ALIAS']);
			if (8 >= count($this->mParsedUri) AND (Group::ADMIN == $parent->getDiscriminator())) {
				if (isset($this->mParsedUri[4])) {
					if (is_numeric($this->mParsedUri[4])) {
						$_GET['gID'] = $this->mParsedUri[4];
					}
					else if ('journals' == $this->mParsedUri[4] AND 7 <= count($this->mParsedUri)) {
						$handler = GanetDbHandler::getDbHandler();
						
						$criteria = new Criteria2();
						$criteria->add(GroupPeer::NAME, $handler->makeSqlSafeString($this->mParsedUri[3]));
						$criteria->add(GroupPeer::PARENT_ID, $handler->makeSqlSafeString($parent->getGroupID()));
						
						$groups = GroupPeer::doSelect($criteria);
						if (0 < count($groups)) {
						 $session = SessionManager::getInstance();
						
							$_GET['ownerID'] = $groups[0]->getGroupID();
							$_GET['entry_title'] = (isset($this->mParsedUri[6])) ? $this->mParsedUri[6] : "";
							$_GET['travelID'] = $this->mParsedUri[5];
							$_GET['travelerID'] = $session->get('travelerID');
							$_GET['is_login'] = (0 == $session->get('travelerID')) ? false : true;
							$_GET['fail'] = (isset($this->mParsedUri[7]) AND "fail" == $this->mParsedUri[7]) ? 1 : 0; 
							
							$_GET['urlAlias'] = $urlAlias;
							$_GET['urlAlias'] = str_replace("?______array", "", $_GET['urlAlias']);
							
							$temp = explode("?", $_GET['urlAlias']);
							$_GET['urlAlias'] = count($temp) ? $temp[0] : $_GET['urlAlias'];
							
							require_once('travellog/model/Class.EntryModel.php');
							$entry = new EntryModel();
							$_GET['travellogID'] = $entry->permalink($_GET);
							
							$obj_factory = ControllerFactory::getInstance();
							$obj_factory->createController('Entry')->performAction();
							
							exit;
						}
						else {
							return;
						}			
					}
					else {
						return;
					}
				}
				else {
					$handler = GanetDbHandler::getDbHandler();
					
					$criteria = new Criteria2();
					$criteria->add(GroupPeer::NAME, $handler->makeSqlSafeString($this->mParsedUri[3]));
					$criteria->add(GroupPeer::PARENT_ID, $handler->makeSqlSafeString($parent->getGroupID()));
					
					$groups = GroupPeer::doSelect($criteria);
					if (0 < count($groups)) {
						$_GET['gID'] = $groups[0]->getGroupID();
					}
					else {
						return;
					}
				}
				
				$obj_factory = ControllerFactory::getInstance();
				$obj_factory->createController('Groups')->performAction();
				exit;
			}
		}
	}
