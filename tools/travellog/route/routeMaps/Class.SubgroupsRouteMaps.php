<?php
	/**
	 * @(#) SubgroupsRouteMaps.php
	 * 
	 * @package ganet.route
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 10, 2009
	 */
	
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/route/routeMaps/Class.AbstractRouteMaps.php");
	require_once("Class.FriendlyUrl.php");
	
	/**
	 * Route class for subgroups search.
	 */
	class SubgroupsRouteMaps extends AbstractRouteMaps {
		
		/**
		 * Handles the setting of correct keys to $_GET variable and calling the
		 * correct controller.
		 * 
		 * @return void
		 */
		function dispatch(){
			require_once('vendor/autoload/GanetAutoloader.php');
				
			GanetAutoloader::getInstance()->register();
			GanetPropelRuntime::getInstance()->initPropel();
			
			if (1 < count($this->mParsedUri)) {
				require_once('travellog/factory/Class.ControllerFactory.php');
				
				$control = ControllerFactory::getInstance()->createController('SubGroups');
				$method = (isset($this->mParsedUri[1])) ? uc_implode(explode("-", $this->mParsedUri[1])) : 'index';
				$params = array_slice($this->mParsedUri, 2, (count($this->mParsedUri) - 3));
				call_user_func_array(array(&$control, $method), $params);
				exit;
			}
		}
	}