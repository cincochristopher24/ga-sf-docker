<?php
	/**
	 * @package ganet.route
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 16, 2009
	 */
	
	require_once("travellog/model/Class.SessionManager.php");
	require_once('travellog/route/routeMaps/Class.AbstractRouteMaps.php');
	
	/**
	 * Route class for ganet searches.
	 */
	class SearchRouteMaps extends AbstractRouteMaps {
		
		/**
		 * Handles the setting of correct keys to $_GET variable and calling the
		 * correct page(file).
		 * 
		 * @return void
		 */
		function dispatch(){
			if (2 <= count($this->mParsedUri)) {
				if ("groups" == $this->mParsedUri[1]) {
					$params = array_slice($this->mParsedUri, 3, (count($this->mParsedUri) - 4));
					$control = ControllerFactory::getInstance()->createController('SubGroups');
					$method = (isset($this->mParsedUri[2])) ? uc_implode(explode("-", $this->mParsedUri[2])) : 'index';
					require_once('travellog/factory/Class.ControllerFactory.php');
					
					call_user_func_array(array(&$control, $method), $params);
					exit;
				}
			}
		}
		
	}