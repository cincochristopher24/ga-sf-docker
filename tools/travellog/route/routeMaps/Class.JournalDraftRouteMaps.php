<?php
	/**
	 * @(#) Class.JournalDraft.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 27, 2009
	 */
	 
	require_once('travellog/route/routeMaps/Class.AbstractRouteMaps.php');
	
	/**
	 * Route map for journal draft.
	 */
	class JournalDraftRouteMaps extends AbstractRouteMaps {

	}
	
