<?php
	/**
	 * @(#) Class.DiscussionBoardRouteMaps.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 23, 2006
	 */
	
	require_once('travellog/route/routeMaps/Class.AbstractRouteMaps.php');
	
	/**
	 * Route maps of the discussion board.
	 */
	class DiscussionBoardRouteMaps extends AbstractRouteMaps {
	
	}