<?php
	require_once("travellog/route/model/Class.gaIParamBuilder.php");
	class ParamBuilder implements gaIParamBuilder{

		private $col_valid_context = array('JOURNALS', 'SUBGROUP', 'GROUPS', 'TRAVELER', 'PAGE', 'JCP', 'FAIL');

		
		function build($URI){
			// replaces all occurence of slashes at the end of a string.
			//$URI               = eregi_replace('/*$', '', $URI);
			$URI               = preg_replace('/\/*$/i', '', $URI);
			$params            = array();
			$col_uri           = explode("/", $URI);
			$params['CONTEXT'] = '';
			
			if( in_array('groups', $col_uri) ) array_splice($col_uri, 0, 1);
				
			$param_len = count($col_uri);

			if( $param_len == 2 && in_array('groups', $col_uri) ){
				$params['CONTEXT']  = 'GROUPS';
				$params['USERNAME'] = $col_uri[0];
				$value = str_replace('-',' ',$col_uri[1]);
				$value = str_replace('_','-',$value);
				$params['GROUPS']   = $value;
			}
			elseif( $param_len == 2 ){
				$params['CONTEXT']  = 'TRAVELER';
				$params['USERNAME'] = $col_uri[1];
			} 
			else{
				foreach( $this->col_valid_context as $valid_context ){
				
					for( $i=0; $i<$param_len; $i = $i+2 ){
						if( strtolower($valid_context) ==  $col_uri[$i] ){
							if( count($col_uri) > $i+1 ){
								$value = str_replace('-',' ',$col_uri[$i+1]);
								$value = str_replace('_','-',$value);
								$params[$valid_context] = $value;
							}
							else $params[$valid_context] = '';
							
							if( $valid_context == 'JOURNALS' && !isset($params['ENTRY_TITLE'])){ 
								$params['CONTEXT'] = 'JOURNALS';
								$offset            = $i+2;						
								if( $param_len > $offset ){
									$params['ENTRY_TITLE'] = $col_uri[$offset];
									$params['ENTRY_TITLE'] = str_replace('-',' ',$params['ENTRY_TITLE']);
									$params['ENTRY_TITLE'] = str_replace('_','-',$params['ENTRY_TITLE']);
									array_splice($col_uri, $offset, 1);
									$param_len = count($col_uri);
								}
							}
							elseif( strlen($params['CONTEXT']) == 0 && strtolower($valid_context) == 'groups' )
								$params['CONTEXT'] = $valid_context;
							elseif( strlen($params['CONTEXT']) == 0 && strtolower($valid_context) == 'subgroup' )
								$params['CONTEXT'] = $valid_context;
							break;
						}
						elseif( strlen($col_uri[$i]) == 0 && !isset($params['USERNAME']) ){
							$params['USERNAME'] = $col_uri[$i+1];
						}
					}					
				}
				
			}
			
			if( strlen(trim($params['CONTEXT'])) == 0 && !in_array('groups', $col_uri) )
            	$params['CONTEXT']  = 'TRAVELER';
            elseif( !array_key_exists('GROUPS', $params) )
            	$params['USERNAME'] = $col_uri[1];
				
			return $params; 
		}    
	}
?>

