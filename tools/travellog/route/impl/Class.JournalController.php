<?php
header("ETag: PUB" . time());   
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 10800) . ' GMT');    
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Keep-Alive: timeout=60, max=994'); 
header('Content-Transfer-Encoding: chunk');     
session_cache_limiter('private, must-revalidate');  
require_once("travellog/model/Class.SessionManager.php");
require_once("travellog/route/impl/Class.AbstractControllerDispatcher.php");
require_once("travellog/route/rules/Class.JournalControllerDispatchRule.php");
require_once("travellog/facade/Class.MainLayoutRendererFcd.php");
require_once('travellog/factory/Class.ControllerFactory.php');
class JournalController extends AbstractControllerDispatcher{
	  
	private 
	
	$obj_session  = NULL,
	
	$obj_rule     = NULL;
	
	function __construct(){
		$this->obj_rule = new JournalControllerDispatchRule;
		$this->obj_session = SessionManager::getInstance();
	}
	
	function dispatch(){
		$arr_vars                = array();
		$props                   = array();
		$arr_vars['travelID']    = $this->params['JOURNALS'];		
		$fcd                     = new MainLayoutRendererFcd;
		$arr_vars['travelerID']  = 0;
		$arr_vars['is_login']    = false;
		$arr_vars['travellogID'] = 0;	
		$arr_vars['entry_title'] = NULL;
		
		// added by neri if fail parameter is present in the passed friendly url: 11-26-08
		if( array_key_exists('FAIL' , $this->params) ) $arr_vars['fail']  = 1;
		if( array_key_exists('ENTRY_TITLE', $this->params)){
			//$arr_vars['entry_title'] = htmlspecialchars(urldecode($this->params['ENTRY_TITLE']));	
			$arr_vars['entry_title'] = $this->params['ENTRY_TITLE'];
			$arr_vars['travellogID'] = $fcd->permalink( $arr_vars ); 
		} 
		
		if(  $this->obj_session->getLogin() ){
			$arr_vars['travelerID'] = $this->obj_session->get('travelerID');
			$arr_vars['is_login']   = true; 
		} 
		
		$props['travellogID'] = $arr_vars['travellogID'];
		$props['entry_title'] = $arr_vars['entry_title'];
				
		if( strtolower($_SERVER['HTTP_HOST']) != 'www.goabroad.net' && strtolower($_SERVER['HTTP_HOST']) != 'dev.goabroad.net' && strtolower($_SERVER['HTTP_HOST']) != 'goabroad.net.local' )
			$arr_vars['folder'] = '';  
		else 
			$arr_vars['folder'] = '';     
		
		try{

			$this->buildProperties( $props );
			
			$this->obj_rule->applyRules( $props );
			$_GET = array_merge($_GET, $arr_vars); 
			$obj_factory = ControllerFactory::getInstance();
			$obj_factory->createController('Entry')->performAction();
			
		}catch(Exception $e){
			//require_once('Class.ErrorLog.php');
			//ErrorLog::setException($e);
			require_once('errorLogger/Class.ErrorLogger.php');
			ErrorLogger::getInstance()->handleException($e);
			$this->FileNotFound();
		}
	}
	
	private function buildProperties( &$props ){
		require_once("travellog/route/utils/Class.Utils.php");

		Utils::getTravelerIDByUsername    ( $props, $this->params );
		Utils::getTravelerIDByGroupName   ( $props, $this->params );
		Utils::getTravelerIDBySubGroupName( $props, $this->params );
		Utils::getJournalOwner            ( $props, $this->params );

	}
	
	private function hasAuthentication(){
		return ( isset( $_SESSION['isLogin'] ) )? true: false;
	}
}
?>

