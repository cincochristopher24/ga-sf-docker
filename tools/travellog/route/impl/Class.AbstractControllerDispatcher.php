<?php
require_once("travellog/route/model/Class.gaAbstractControllerDispatcher.php");
abstract class AbstractControllerDispatcher extends gaAbstractControllerDispatcher{
	protected function FileNotFound(){
		header ("HTTP/1.0 404 Not Found");
		header ("Status 404 Not Found"); 
		require("FileNotFound.php");
	}
	
	function dispatch(){}
}
?>
