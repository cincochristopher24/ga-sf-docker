<?php
require_once("travellog/route/impl/Class.AbstractControllerDispatcher.php");
require_once("travellog/route/rules/Class.SubGroupControllerDispatchRule.php");
class SubGroupController extends AbstractControllerDispatcher{
	
	private 
	
	$obj_rule = NULL;
	
	function __construct(){
		$this->obj_rule = new SubGroupControllerDispatchRule;
	}	
	
	function dispatch(){
		$props  = array();
		
		try{
			
			$this->buildProperties( $props );
			$_GET['gID']           = $props['subgroupID'];
			$_GET['friendly_url']  = true;
			if( array_key_exists('PAGE', $this->params) ) $_GET['page'] = $this->params['PAGE'];            
			if( array_key_exists('JCP' , $this->params) ) $_GET['jcp']  = $this->params['JCP'];
			
			$this->obj_rule->applyRules( $props );
			//var_dump($props['subgroupID']); exit;
			require_once("group.php");
			
		}catch( Exception $e ){
			require_once('errorLogger/Class.ErrorLogger.php');
			ErrorLogger::getInstance()->handleException($e);

			$this->FileNotFound();
		}
	}
	
	private function buildProperties( &$props ){
		require_once("travellog/route/utils/Class.Utils.php");
		
		Utils::getTravelerIDByGroupName   ( $props, $this->params );
		Utils::getTravelerIDBySubGroupName( $props, $this->params );
			
	}
}
?>
