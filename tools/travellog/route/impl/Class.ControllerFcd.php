<?php
require_once("travellog/route/impl/Class.ParamBuilder.php");
require_once("travellog/route/model/Class.gaIController.php");
class ControllerFcd extends ParamBuilder implements gaIController{
	static $instance = NULL;
	
	static function getInstance(){
		if( self::$instance == NULL ) self::$instance = new ControllerFcd;
		return self::$instance; 
	}
	
	function getController(){
		require_once("travellog/route/impl/Class.RouteControllerFactory.php");
		$URI       = urldecode($_SERVER['REQUEST_URI']);
		$params    = $this->build($URI);
		$factory   = RouteControllerFactory::instance();
		$obj       = $factory->create( $params['CONTEXT'] );
		$obj->setParams( $params );

		return $obj;
	}
} 
?> 
