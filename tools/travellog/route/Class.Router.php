<?php
	/**
	 * @(#) Class.Router.php
	 * 
	 * @package ganet.route
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 9, 2009
	 */
	
	require_once("Class.FriendlyUrl.php");
	require_once("travellog/route/routeMaps/Class.AbstractRouteMaps.php");
	require_once("travellog/factory/Class.ControllerFactory.php");
	require_once("Class.ToolMan.php");
	
	/**
	 * Handler class for routing a friendly url. This class is implemented as a
	 * singleton pattern.
	 */
	class Router {
		
		/**
		 * The instance of the router object.
		 * 
		 * @var Router
		 */
		private static $sInstance = null;
		
		/**
		 * Constructs the Router object.
		 */
		private function __construct(){}
		
		/**
		 * Returns the instance of this object.
		 * 
		 * @return Router
		 */
		static function instance(){
			if (is_null(self::$sInstance)) {
				self::$sInstance = new Router();
			}
			
			return self::$sInstance;
		}
		
		/**
		 * Executes the routing of the friendly url to the correct file.
		 * 
		 * @return void
		 */
		function execute(){
			if (isset($_SERVER['REQUEST_URI'])) {
				$uri = preg_replace("/^\/|\/$/", "", FriendlyUrl::decode(urldecode($_SERVER['REQUEST_URI'])));
				$params = explode("/", $uri);
				$name = preg_replace("/ /", "", ucwords(preg_replace("/-/", " ", $params[0])));
				$params['ROUTER_CLASS_NAME'] = $name;
				
				$rawUri = preg_replace("/^\/|\/$/", "", urldecode($_SERVER['REQUEST_URI']));
				$rawParams = explode("/", $rawUri);
				$params['JOURNAL_ENTRY_URL_ALIAS'] = end($rawParams);
				if (1 <= count($params)) {
					if (ToolMan::file_exists("travellog/route/routeMaps/Class.".$name."RouteMaps.php")) {						
						require_once ("travellog/route/routeMaps/Class.".$name."RouteMaps.php");
						$parentClass = new ReflectionClass("AbstractRouteMaps");
						
						$obj = new ReflectionClass($name."RouteMaps");						
						if ($obj->isSubclassOf($parentClass)) { // check if the file extends AbstractRouteMaps							
							$route = $obj->newInstanceArgs(array($params));
							$route->dispatch();							
							
						}
						else {
							echo "Router class expects $name"."RouteMaps to extend abstract class AbstractRouteMaps.";
							exit;
						}
					}
					else { // If there are no route maps, the default is traveler.
						require_once("travellog/route/routeMaps/Class.TravelerRouteMaps.php");
						$route = new TravelerRouteMaps($params);
						$route->dispatch();
					}
				}
			}
			
			header ("HTTP/1.0 404 Not Found");
			header ("Status 404 Not Found"); 
			require_once("FileNotFound.php");
		}
	}
	
