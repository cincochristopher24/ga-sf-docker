<?php
abstract class gaAbstractAssessorActionFactory{
	static protected $instance = NULL;
	
	abstract function createAssessors();
	
	abstract function createActions();  
} 
?>
