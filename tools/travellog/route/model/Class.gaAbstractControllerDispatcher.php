<?php
abstract class gaAbstractControllerDispatcher{
	protected 
	
	$params = NULL;
	
	function setParams( $params ){
		$this->params = $params;
	}
	
	protected function removeDash( $str ){
		return str_replace('-',' ', $str);
	}
	
	abstract protected function FileNotFound();
	abstract function dispatch();
}
?>
