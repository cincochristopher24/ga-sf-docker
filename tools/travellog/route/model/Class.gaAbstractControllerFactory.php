<?php
abstract class gaAbstractControllerFactory{
	protected static $instance = NULL;
	abstract static function instance();
	abstract function create( $route_type );
}
?>
