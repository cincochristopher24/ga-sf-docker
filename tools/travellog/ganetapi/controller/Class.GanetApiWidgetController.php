<?php

require_once("travellog/model/Class.SiteContext.php");
require_once("Class.Crypt.php");
require_once("travellog/ganetapi/constant/Class.GanetApiConstant.php");
require_once("Class.GATemplate.php");
require_once("travellog/model/Class.GroupPeer.php");
require_once("travellog/helper/gaTextHelper.php");


/**
 * GanetApiWidgetController.
 *
 */
class GanetApiWidgetController {
	
	/**
	 * @var $mGroup
	 */
	protected $mGroup;
	
	/**
	 * @var $mData
	 */
	protected $mData = array();
	
	/**
	 * Class Constructor.
	 *
	 */
	public function __construct() {
	
		$this->mData[GanetApiConstant::MODE] = isset($_REQUEST[GanetApiConstant::MODE]) ? $_REQUEST[GanetApiConstant::MODE] : null;
		
		$this->mData[GanetApiConstant::APP_KEY] = isset($_REQUEST[GanetApiConstant::APP_KEY]) ? $_REQUEST[GanetApiConstant::APP_KEY] : null;
		$this->mData[GanetApiConstant::APP_KEY] = $this->mData[GanetApiConstant::APP_KEY] ? $this->decryptGroupID($this->mData[GanetApiConstant::APP_KEY]) : GanetApiConstant::DEFAULT_APP_KEY;
		
		$this->mData[GanetApiConstant::KEYWORD_LOCATION] = isset($_REQUEST[GanetApiConstant::KEYWORD_LOCATION]) ? (int) $_REQUEST[GanetApiConstant::KEYWORD_LOCATION] : null;
		$this->mData[GanetApiConstant::KEYWORD_SUBGROUP] = isset($_REQUEST[GanetApiConstant::KEYWORD_SUBGROUP]) ? (int) $_REQUEST[GanetApiConstant::KEYWORD_SUBGROUP] : null;
		
		$this->mData[GanetApiConstant::KEYWORD_WIDTH] = isset($_REQUEST[GanetApiConstant::KEYWORD_WIDTH]) ? (int) $_REQUEST[GanetApiConstant::KEYWORD_WIDTH] : null;
		$this->mData[GanetApiConstant::KEYWORD_COUNT] = isset($_REQUEST[GanetApiConstant::KEYWORD_COUNT]) ? (int) $_REQUEST[GanetApiConstant::KEYWORD_COUNT] : null;
		$this->mData[GanetApiConstant::KEYWORD_FONT] = isset($_REQUEST[GanetApiConstant::KEYWORD_FONT]) ? $_REQUEST[GanetApiConstant::KEYWORD_FONT] : null;
		$this->mData[GanetApiConstant::KEYWORD_SCROLLBAR] = isset($_REQUEST[GanetApiConstant::KEYWORD_SCROLLBAR]) ? $_REQUEST[GanetApiConstant::KEYWORD_SCROLLBAR] : null;
		$this->mData[GanetApiConstant::KEYWORD_DEFAULT_PHOTO] = isset($_REQUEST[GanetApiConstant::KEYWORD_DEFAULT_PHOTO]) ? $_REQUEST[GanetApiConstant::KEYWORD_DEFAULT_PHOTO] : null;
		
		$this->mData[GanetApiConstant::KEYWORD_TITLE] = isset($_REQUEST[GanetApiConstant::KEYWORD_TITLE]) ? stripslashes($_REQUEST[GanetApiConstant::KEYWORD_TITLE]) : null;
		$this->mData[GanetApiConstant::KEYWORD_DESCRIPTION] = isset($_REQUEST[GanetApiConstant::KEYWORD_DESCRIPTION]) ? stripslashes($_REQUEST[GanetApiConstant::KEYWORD_DESCRIPTION]) : null;
		
		$this->mData[GanetApiConstant::KEYWORD_SORT] = isset($_REQUEST[GanetApiConstant::KEYWORD_SORT]) ? $_REQUEST[GanetApiConstant::KEYWORD_SORT] : null;
		
		$this->mData[GanetApiConstant::KEYWORD_STATUS] = isset($_REQUEST[GanetApiConstant::KEYWORD_STATUS]) ? $_REQUEST[GanetApiConstant::KEYWORD_STATUS] : null;
		$this->mData[GanetApiConstant::KEYWORD_WITH_ARTICLE] = isset($_REQUEST[GanetApiConstant::KEYWORD_WITH_ARTICLE]) ? $_REQUEST[GanetApiConstant::KEYWORD_WITH_ARTICLE] : null;
	}
	
	/**
	 * Executes this controller.
	 *
	 */
	public function execute() {
		
		// check url domain if matches with cobrand domain using groupID passed
		$this->site_context = SiteContext::getInstance();
		if ($this->site_context->isInCobrand()) {
			if ($this->site_context->getGroupID() !== $this->mData[GanetApiConstant::APP_KEY]) {
				$this->showErrorPage();
			}
		}
		
		switch ($this->mData[GanetApiConstant::MODE]) {
			case GanetApiConstant::TRAVELER: // public view
				$this->showTravelerWidget();
			break;
			
			case GanetApiConstant::BLOG: // public view
				$this->showBlogWidget();
			break;
			
			default:
				$this->showErrorPage();
		}
	}
	
	/**
	 * Render Error page.
	 *
	 */
	public function showErrorPage() {
		header ("HTTP/1.0 404 Not Found");
		header ("Status 404 Not Found");
		
		$tpl = new GATemplate();
		$tpl->setPath('travellog/ganetapi/view/');
		$tpl->setTemplate('tpl.ViewErrorPage.php');
		echo $tpl;
		
		exit;
	}
	
	/**
	 * Render Traveler Widget.
	 *
	 */
	public function showTravelerWidget() {
		require_once("travellog/ganetapi/search/Class.GanetApiTravelerSearch.php");
		
		$this->mData[GanetApiConstant::KEYWORD_WIDTH] = !is_null($this->mData[GanetApiConstant::KEYWORD_WIDTH]) ? $this->mData[GanetApiConstant::KEYWORD_WIDTH] : GanetApiConstant::DEFAULT_MEMBER_WIDTH;
		$this->mData[GanetApiConstant::KEYWORD_COUNT] = !is_null($this->mData[GanetApiConstant::KEYWORD_COUNT]) ? $this->mData[GanetApiConstant::KEYWORD_COUNT] : GanetApiConstant::DEFAULT_MEMBER_COUNT;
		
		$this->mData[GanetApiConstant::KEYWORD_TITLE] = !is_null($this->mData[GanetApiConstant::KEYWORD_TITLE]) ? $this->mData[GanetApiConstant::KEYWORD_TITLE] : $this->getDefaultTitle();
		$this->mData[GanetApiConstant::KEYWORD_DESCRIPTION] = !is_null($this->mData[GanetApiConstant::KEYWORD_DESCRIPTION]) ? $this->mData[GanetApiConstant::KEYWORD_DESCRIPTION] : $this->getDefaultDescription();
		
		$this->mData[GanetApiConstant::KEYWORD_SORT] = !is_null($this->mData[GanetApiConstant::KEYWORD_SORT]) ? $this->mData[GanetApiConstant::KEYWORD_SORT] : GanetApiConstant::DEFAULT_MEMBER_SORT;
		
		// TODO: this is just a temporary fix =[
		$isISV = $this->getGroup() 
			? ($this->getGroup()->getID() == 1760 ? true : false) : false;
		$this->mData[GanetApiConstant::KEYWORD_STATUS] = !is_null($this->mData[GanetApiConstant::KEYWORD_STATUS]) 
			? $this->mData[GanetApiConstant::KEYWORD_STATUS] 
			: ($isISV ? true : null);
		
		$travelers = GanetApiTravelerSearch::search($this->mData);
		$count = GanetApiTravelerSearch::count($this->mData);
		//$link = $this->getGroup() ? 'http://'.$_SERVER['HTTP_HOST'].'/members.php?gID='.$this->mData[GanetApiConstant::APP_KEY] : 'http://'.$_SERVER['HTTP_HOST'].'/travelers.php';
		$link = $this->site_context->isInCobrand() ? 'http://'.$_SERVER['HTTP_HOST'] : ($this->getGroup() ? 'http://www.GoAbroad.net'.$this->getGroup()->getFriendlyURL() : 'http://www.GoAbroad.net');
		$homepage = $this->site_context->isInCobrand() ? 'http://'.$_SERVER['HTTP_HOST'] : 'http://www.GoAbroad.net';
		
		$this->saveFlag(GanetApiConstant::TRAVELER_FLAG);
		
		$tpl = new GATemplate();
		$tpl->setPath('travellog/ganetapi/view/');
		$tpl->setTemplate('tpl.ViewTravelerPage.php');
		
		$tpl->setVars(array(
			'title'       => $this->mData[GanetApiConstant::KEYWORD_TITLE],
			'description' => $this->mData[GanetApiConstant::KEYWORD_DESCRIPTION],
			'width'       => $this->mData[GanetApiConstant::KEYWORD_WIDTH],
			'scrollbar'   => $this->mData[GanetApiConstant::KEYWORD_SCROLLBAR],
			'font'        => $this->mData[GanetApiConstant::KEYWORD_FONT],
			'travelers'   => $travelers,
			'count'       => $count,
			'link'        => $link,
			'homepageLink' => $homepage,
		));
		echo $tpl;
	}
	
	/**
	 * Render Blog Widget.
	 *
	 */
	public function showBlogWidget() {
		require_once("travellog/ganetapi/search/Class.GanetApiBlogSearch.php");

		$this->mData[GanetApiConstant::KEYWORD_WIDTH] = !is_null($this->mData[GanetApiConstant::KEYWORD_WIDTH]) ? $this->mData[GanetApiConstant::KEYWORD_WIDTH] : GanetApiConstant::DEFAULT_BLOG_WIDTH;
		$this->mData[GanetApiConstant::KEYWORD_COUNT] = !is_null($this->mData[GanetApiConstant::KEYWORD_COUNT]) ? $this->mData[GanetApiConstant::KEYWORD_COUNT] : GanetApiConstant::DEFAULT_BLOG_COUNT;
		
		$this->mData[GanetApiConstant::KEYWORD_TITLE] = !is_null($this->mData[GanetApiConstant::KEYWORD_TITLE]) ? $this->mData[GanetApiConstant::KEYWORD_TITLE] : GanetApiConstant::DEFAULT_BLOG_HEADER;
		$this->mData[GanetApiConstant::KEYWORD_DESCRIPTION] = !is_null($this->mData[GanetApiConstant::KEYWORD_DESCRIPTION]) ? $this->mData[GanetApiConstant::KEYWORD_DESCRIPTION] : $this->getDefaultDescription();
		
		// TODO: this is just a temporary fix =[
		$isISV = $this->getGroup() 
			? ($this->getGroup()->getID() == 1760 ? true : false) : false;
		$this->mData[GanetApiConstant::KEYWORD_DEFAULT_PHOTO] = !is_null($this->mData[GanetApiConstant::KEYWORD_DEFAULT_PHOTO]) 
			? $this->mData[GanetApiConstant::KEYWORD_DEFAULT_PHOTO] 
			: ($isISV ? true : null);
		$this->mData[GanetApiConstant::KEYWORD_WITH_ARTICLE] = !is_null($this->mData[GanetApiConstant::KEYWORD_WITH_ARTICLE]) 
			? $this->mData[GanetApiConstant::KEYWORD_WITH_ARTICLE] 
			: ($isISV ? null : true);
		
		$blogs = GanetApiBlogSearch::search($this->mData);
		//$link = $this->getGroup() ? 'http://'.$_SERVER['HTTP_HOST'].'/journal.php?action=groupJournals&gID='.$this->mData[GanetApiConstant::APP_KEY] : 'http://'.$_SERVER['HTTP_HOST'].'/journal.php';
		$link = $this->site_context->isInCobrand() ? 'http://'.$_SERVER['HTTP_HOST'] : ($this->getGroup() ? 'http://www.GoAbroad.net'.$this->getGroup()->getFriendlyURL() : 'http://www.GoAbroad.net');
		$homepage = $this->site_context->isInCobrand() ? 'http://'.$_SERVER['HTTP_HOST'] : 'http://www.GoAbroad.net';
		
		$this->saveFlag(GanetApiConstant::BLOG_FLAG);
		
		$tpl = new GATemplate();
		$tpl->setPath('travellog/ganetapi/view/');
		$tpl->setTemplate('tpl.ViewBlogPage.php');
		
		$tpl->setVars(array(
			'title'       => $this->mData[GanetApiConstant::KEYWORD_TITLE],
			'description' => $this->mData[GanetApiConstant::KEYWORD_DESCRIPTION],
			'width'       => $this->mData[GanetApiConstant::KEYWORD_WIDTH],
			'scrollbar'   => $this->mData[GanetApiConstant::KEYWORD_SCROLLBAR],
			'font'        => $this->mData[GanetApiConstant::KEYWORD_FONT],
			'blogs'       => $blogs,
			'link'        => $link,
			'homepageLink' => $homepage,
		));
		echo $tpl;
	}
	
	/**
	 * Retrieve default title which is the group name.
	 *
	 */
	public function getDefaultTitle() {
		return $this->getGroup() ? truncateText($this->getGroup()->getName(), 50, '') : GanetApiConstant::DEFAULT_HEADER;
	}
	
	/**
	 * Retrieve default description which is the group name.
	 *
	 */
	public function getDefaultDescription() {
		//return $this->getGroup() ? truncateText($this->getGroup()->getDescription(), 50, '...', false) : GanetApiConstant::DEFAULT_DESCRIPTION;
		return '';
	}
	
	/**
	 * Retrieve and set parent group.
	 *
	 */
	public function getGroup() {
  		if (!$this->mGroup){
  			if ($this->mGroup = GroupPeer::retrieveByPK($this->mData[GanetApiConstant::APP_KEY])) {
  				$this->mGroup = $this->mGroup->isParent() ? $this->mGroup : $this->mGroup->getParent();
  			}
  		}
  		
		return $this->mGroup;
  	}
  	
  	/**
	 * Save apiFlag.
	 *
	 */
  	protected function saveFlag($type) {
  		$flag = $this->getGroup()->getApiFlag();
  		if ($flag < (GanetApiConstant::TRAVELER_FLAG + GanetApiConstant::BLOG_FLAG)){
  			if ($type != $flag) {
  				$this->getGroup()->saveApiFlag($type+$flag);
  			}
  		}
  	}
  	
  	/**
	 * Decrypt groupID.
	 *
	 */
  	protected function decryptGroupID($groupID) {
  		$crypt = new Crypt; 
		
		$groupID = base64_decode($groupID);
		$groupID = $crypt->decrypt($groupID);
		
		return $groupID;
  	}
  	
}