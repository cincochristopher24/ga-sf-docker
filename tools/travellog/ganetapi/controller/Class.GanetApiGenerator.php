<?php
require_once("travellog/model/Class.SessionManager.php");
require_once("travellog/action/Class.ControllerAction.php");
require_once("travellog/model/Class.SiteContext.php");
require_once("travellog/model/traveler/Class.TravelerUser.php");
require_once("Class.Template.php");
require_once("travellog/components/profile/controller/Class.ProfileCompFactory.php");
require_once("travellog/model/Class.SubNavigation.php");
require_once("Class.Crypt.php");
require_once("travellog/model/Class.GroupPeer.php");
require_once("travellog/ganetapi/constant/Class.GanetApiConstant.php");
require_once("travellog/model/Class.Country.php");
require_once("travellog/helper/gaTextHelper.php");


/**
 * GanetApiGenerator.
 *
 */
class GanetApiGenerator extends ControllerAction {
	
	/**
	 * @var $mGroup
	 */
	protected $mGroup;
	
	/**
	 * @var $mContext
	 */
	protected $mContext;
	
	
	/**
	 * Class Constructor
	 *
	 */
	public function __construct($param = array()) {
		parent::__construct($param);
	}
	
	/**
	 * Executes this controller.
	 */
	public function execute() {
		if (!$this->mContext) {
			throw new Exception('Undefined context.');
		}
		$this->validate();
		$this->renderTemplate();
	}
	
	/**
	 * Set context.
	 */
	public function setContext($context) {
		if (in_array($context, array(GanetApiConstant::TRAVELER, GanetApiConstant::BLOG))){
			$this->mContext = $context;
		}
		else {
			throw new Exception(sprintf('Invalid context: %s.', $context));
		}
	}
	
	/**
	 * Validates.
	 */
	public function validate() {
		$this->mUser = TravelerUser::getInstance();
		
		if (!$this->mUser->isLogged()){
			$this->redirect("/login.php?redirect=".$_SERVER['REQUEST_URI']);
		}
		
		$file_factory = FileFactory::getInstance();
		$this->mTraveler = $file_factory->getClass('Traveler', array($this->mUser->getUserID()));
		
		if ($this->mTraveler->isSuspended() || $this->mTraveler->isDeactivated()) {
			header("location: /index.php");
			exit;
		}
		
		if ($this->getGroup()){
		    if ($this->mUser->getUserID() == $this->mGroup->getAdministratorID()){
		    	return true;
		    }
		    if ($this->mGroup->isStaff($this->mUser->getUserID())){
		    	return true;
		    }
		}
		$this->showFileNotFound();
  	}
  	
  	/**
  	 * Show Traveler Template.
  	 */
  	protected function renderTravelerTemplate(Template $tpl) {
    	$tpl->set('header', $this->getHeader());
    	$tpl->set('description', $this->getDescription());
    	$tpl->set('count', $this->getCount());
    	$tpl->set('width', $this->getWidth());
    	$tpl->set('subgroups', $this->getSubgroups());
    	$tpl->set('countries', $this->getCountries());
    	$tpl->out('travellog/views/ganetapi/tpl.ViewTravelerWidgetCodeGenerator.php');
  	}
  	
  	/**
  	 * Show Blog Template.
  	 */
  	protected function renderBlogTemplate(Template $tpl) {
    	$tpl->set('header', GanetApiConstant::DEFAULT_BLOG_HEADER);
    	$tpl->set('description', $this->getDescription());
    	$tpl->set('count', $this->getCount());
    	$tpl->set('width', $this->getWidth());
    	$tpl->set('subgroups', $this->getSubgroups());
    	$tpl->set('countries', $this->getCountries());
    	$tpl->out('travellog/views/ganetapi/tpl.ViewBlogWidgetCodeGenerator.php');
  	}
  	
  	/**
  	 * Show Template.
  	 */
  	protected function renderTemplate() {
  		$site_context = SiteContext::getInstance();
  	    
    	$file_factory = FileFactory::getInstance();
    	Template::setMainTemplate($file_factory->getTemplate('LayoutMain')); 
		
    	$contextID = $this->mGroup->isParent() ? $this->mGroup->getID() : $this->mGroup->getParentID();
    	
    	$subNavigation = new SubNavigation();
    	$subNavigation->setContext('GROUP');
    	$subNavigation->setContextID($contextID);
    	$subNavigation->setLinkToHighlight('WIDGET');
    	
    	$profile_comp = ProfileCompFactory::getInstance()->create(ProfileCompFactory::$GROUP_CONTEXT);
    	$profile_comp->init($this->mGroup->getID());
		
    	$tpl = new Template();
    	$tpl->includeDependentJs('/js/jquery.popup/jquery.popup.js');
    	$tpl->set('profile_component', $profile_comp->get_view());
    	$tpl->set('sub_navigation_component', $subNavigation);
    	$tpl->set('title', ($site_context->isInCobrand()) ? $site_context->getSiteName() : "GoAbroad Network");
    	$tpl->set('page_location', 'My Passport');
    	$tpl->set('context', $this->mContext);
    	$tpl->set('default_code', $this->getDefaultCode());
  		$tpl->set('travelerWidgetLink', $this->getTravelerWidgetLink());
  		$tpl->set('blogWidgetLink', $this->getBlogWidgetLink());
    	
  		if ($this->mContext == GanetApiConstant::TRAVELER) {
  			$this->renderTravelerTemplate($tpl);
  		}
  		else {
  			$this->renderBlogTemplate($tpl);
  		}
  	}
  	
  	/**
  	 * Get Default Code.
  	 */
  	protected function getDefaultCode() {
  		$server = $_SERVER['HTTP_HOST'];
  		
  		$codes[GanetApiConstant::MODE] = $this->mContext;
  		$codes[GanetApiConstant::APP_KEY] = $this->encryptGroupID();
  		$codes[GanetApiConstant::VERSION] = GanetApiConstant::VERSION_1;
  		
  		$src = 'http://'.$server.'/api.php?'.http_build_query($codes);
  		$width = $this->mContext == GanetApiConstant::TRAVELER ? GanetApiConstant::DEFAULT_MEMBER_WIDTH : GanetApiConstant::DEFAULT_BLOG_WIDTH;
  		$height = $this->mContext == GanetApiConstant::TRAVELER ? GanetApiConstant::DEFAULT_MEMBER_HEIGHT : GanetApiConstant::DEFAULT_BLOG_HEIGHT;
  		
  		return '<iframe id="ganetapi" src="'.htmlspecialchars($src).'" scrolling="no" frameborder="0" style="overflow:hidden; max-width:650px; padding:5px; border:1px solid #eee; width:'.$width.'px; " allowTransparency="true" height="'.$height.'px;"></iframe>';
  	}
  	
  	/**
  	 * Get Default Header.
  	 */
  	protected function getHeader() {
  		return $this->mGroup ? truncateText($this->mGroup->getName(), 50, '') : GanetApiConstant::DEFAULT_HEADER;
  	}
  	
  	/**
  	 * Get Default Description.
  	 */
  	protected function getDescription() {
  		//return $this->mGroup ? truncateText($this->mGroup->getDescription(), 50, '') : GanetApiConstant::DEFAULT_DESCRIPTION;
  		return '';
  	}
  	
  	/**
  	 * Get Default Count.
  	 */
  	protected function getCount() {
  		return $this->mContext == GanetApiConstant::TRAVELER ? GanetApiConstant::DEFAULT_MEMBER_COUNT : GanetApiConstant::DEFAULT_BLOG_COUNT;
  	}
  	
  	/**
  	 * Get Default Widget Width.
  	 */
  	protected function getWidth() {
  		return $this->mContext == GanetApiConstant::TRAVELER ? GanetApiConstant::DEFAULT_MEMBER_WIDTH : GanetApiConstant::DEFAULT_BLOG_WIDTH;
  	}
  	
  	/**
  	 * Get SubGroups Key-Value Pair.
  	 */
  	protected function getSubgroups() {
  		$subgroups = $this->mGroup ? $this->mGroup->getSubGroups() : array();
  		$keyValue = array();
  		foreach ($subgroups as $iGroup) {
  			$keyValue[$iGroup->getID()] = $iGroup->getName();
  		}
  		
  		return $keyValue;
  	}
  	
  	/**
  	 * Get All Approved Countries Key-Value Pair. locID => countryName
  	 */
  	protected function getCountries() {
  		$keyValue = array();
  		foreach (Country::getCountryList() as $iCountry) {
  			$keyValue[$iCountry->getLocationID()] = $iCountry->getName();
  		}
  		
  		return $keyValue;
  	}
  	
  	/**
  	 * Returns link of traveler code generator.
  	 */
  	protected function getTravelerWidgetLink() {
  		$link = '/group.php?mode=traveler_api';
  		
  		$site_context = SiteContext::getInstance();
  		if (!$site_context->isInCobrand()) {
  			$link .= '&gID='.$this->getGroup()->getID();
  		}
  		
  		return $link;
  	}
  	
  	/**
  	 * Returns link of blog code generator.
  	 */
  	protected function getBlogWidgetLink() {
  		$link = '/group.php?mode=blog_api';
  		
  		$site_context = SiteContext::getInstance();
  		if (!$site_context->isInCobrand()) {
  			$link .= '&gID='.$this->getGroup()->getID();
  		}
  		
  		return $link;
  	}
  	
  	/**
  	 * Get and Set the Parent Group.
  	 */
  	protected function getGroup() {
  		if (!$this->mGroup){
  			$this->mGroup = null;
  			
  			$site_context = SiteContext::getInstance();
  			if ($site_context->isInCobrand()) {
				if ($this->mGroup = GroupPeer::retrieveByPK($site_context->getGroupID())){
					$this->mGroup = $this->mGroup->isParent() ? $this->mGroup : $this->mGroup->getParent();
				}
			}
			else{
				if (isset($this->mData['gID'])) {
					if ($this->mGroup = GroupPeer::retrieveByPK($this->mData['gID'])){
						$this->mGroup = $this->mGroup->isParent() ? $this->mGroup : $this->mGroup->getParent();
					}
				}
			}
  		}
  		
		return $this->mGroup;
  	}
  	
  	/**
  	 * Encrypt Group ID.
  	 */
  	protected function encryptGroupID(){
  		$groupID = $this->getGroup()
  			? ($this->getGroup()->isParent() 
  				? $this->getGroup()->getID()
  				: $this->getGroup()->getParentID())
  			: 0;
  		$crypt = new Crypt; 
		
		$groupID = $crypt->encrypt($groupID);
		$groupID = base64_encode($groupID);
		
		return $groupID;
  	}
}