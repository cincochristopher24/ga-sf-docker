<?php

interface GanetApiIValidator {
	function validate();
	function isValid();
	function getErrorMessage();
}
