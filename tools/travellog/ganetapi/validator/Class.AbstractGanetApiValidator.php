<?php

class AbstractGanetApiValidator implements GanetApiIValidator {	
	protected $args;
	protected $error;
	
	public function validate() {}
	
	public function isValid() {
		try {
			$this->validate();
		}
		catch(Exception $e) {
			$this->error = $e->getMessage();
			return false;
		}
		return true;
	}
	
	public function getErrorMessage() {
		return $this->error;
	}
	
	public function setParameters($args = array()) {
		$this->args = $args;
		return $this;
	}
}