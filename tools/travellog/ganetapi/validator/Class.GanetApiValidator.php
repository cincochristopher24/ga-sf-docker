<?php

class GanetApiValidator extends AbstractGanetApiValidator {
	private $isValidated = false;
	static  $instance    = null;
	
	private function __construct() {}
	
	public function getInstance() {
		if(self::$instance === null)
			self::$instance = new self();
		return self::$instance;
	}
	
	/**
	 * Set parameters
	 *
	 * @return void
	 */
	public function setParameters($param = array()) {
		$this->args = $param;
		return $this;
	}
	
	/**
	 * Parameters
	 *
	 * @return array
	 */
	public function getParameters() {
		return $this->args;
	}
	
	/**
	 * Error
	 *
	 * @return String | Boolean false
	 */
	public function getErrorMessage() {
		return $this->error; 
	}
	
	/**
	 * Checks if already validated or not yet.
	 *
	 * @return Boolean
	 */
	public function isValidated() {
		return $this->isValidated;
	}
	
	/**
	 * Validator
	 *
	 * @see GanetApiException
	 * @see GanetApiErrorCodes
	 */
	public function validate() {
		$this->isValidated = true;
		try {
			$start = microtime();
			GanetApiRequestVarsValidator::getInstance()->setParameters($this->args)->validate();
			$request = microtime();
			GanetApiAuthenticationValidator::getInstance()->setParameters($this->args)->validate();
			$authentication = microtime();
			GanetApiClassMethodValidator::getInstance()->setParameters($this->args)->validate();
			$class = microtime();
			if(array_key_exists('debug', $this->args)) {
				var_dump($request - $start);
				var_dump($authentication - $request);
				var_dump($class - $authentication);
			}
		}
		catch(GanetApiException $e) {
			$this->error = $e->getMessage();
		}
		catch(Exception $e) {
			$this->error = GanetApiErrorCodes::$api_error_descriptions[GanetApiErrorCodes::GANET_API_GENERAL] ." ". $e->getMessage();
		}
	}
}
