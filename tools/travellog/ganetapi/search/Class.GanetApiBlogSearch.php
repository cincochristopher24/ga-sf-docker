<?php

	require_once("travellog/ganetapi/constant/Class.GanetApiConstant.php");
	require_once("travellog/helper/gaTextHelper.php");
	
	/**
	 * GanetApiBlogSearch.
	 *
	 */
	class GanetApiBlogSearch {
		
		
		/**
		 * Search travelers array.
		 *
		 * @param array $data
		 * @return array 
		 * @access public
		 */
		public static function search($data) {
			$query = self::createQuery($data);
			$connection = Propel::getConnection();
			
			$statement = $connection->prepare($query);
			$statement->execute();
			
			$blogs = $objs = array();
			
			while ($resultset = $statement->fetch(PDO::FETCH_OBJ)) {
				$objs[] = self::toObject($resultset);
			}
			
			//ugly code =(
			foreach ($objs as $iObj){
				$city = ''; $country = ''; $locationID = 0; $link = ''; $id = 0;
				if ($iObj instanceof TravelLog) {
    				$trip = $iObj->getTrip();
    				$location = $trip->getLocation();
    				$ctry = (!is_null($location)) ? $location->getCountry() : null;
    				if (!is_null($location) && !is_null($ctry)) {
    					$country = $ctry->getName();
    					$city = $location->getName();
    				}
    				$link = '/journal-entry.php?action=view&amp;travellogID='.$iObj->getID();
    				$id = $iObj->getID();
    			}
    			else if ($iObj instanceof Article) {
    				$location = LocationFactory::instance()->create($iObj->getLocID());
    				$ctry = (!is_null($location)) ? $location->getCountry() : null;
    				if (!is_null($location) && !is_null($ctry)) {
    					$country = $ctry->getName();
    					$city = $location->getName();
    				}
    				$link = '/article.php?action=view&amp;articleID='.$iObj->getArticleID();
    				$id = $iObj->getArticleID();
    			}
    			
    			$blogs[] = array(
					'id' => $id,
					'title' => utf8_encode($iObj->getTitle()),
					'blog_url' => 'http://'.$_SERVER['HTTP_HOST'].$link,
					'owner_url' =>  'http://'.$_SERVER['HTTP_HOST'].'/'.$iObj->getTraveler()->getUsername(),
					'owner' => $iObj->getTraveler()->getUsername(),
					'location' => strtolower($city) == strtolower($country) ? $country : $city . ', ' . $country,
					'content' => strip_tags($iObj->getDescription()),
					'photo' => self::getPhotoLink($iObj),
				);
			}
			
			return $blogs;
		}
		
		/**
		 * Transform array to object.
		 *
		 * @param array $data
		 * @return Article | TravelLog
		 * @access protected
		 */
		protected static function toObject($data){
			$obj = null;
			if ('ARTICLE' == $data->type) {
				$obj = self::toArticleObject($data);
			}
			else if ('JOURNAL' == $data->type) {
				$obj = self::toTravelLogObject($data);
			}
			return $obj;
		}
		
		/**
		 * Transform data array to Article object.
		 *
		 * @param array $data
		 * @return Article
		 * @access protected
		 */
		protected static function toArticleObject($data) {
			$arr = array(
				'articleID' => $data->ID,
				'groupID' => null,
				'authorID' => $data->travelerID,
				'title' => $data->title,
				'description' => $data->description,
				'callout' => null,
				'locID' => $data->locID,
				'editorsNote' => null,
				'articledate' => null,
				'logdate' => $data->_date,
				'lastedited' => $data->lastupdated,
				'isfeatured' => null,
				'publish' => null,
				'deleted' => null,
			);
			$obj = new Article(0, $arr);
			return $obj;
		}
		
		/**
		 * Transform data array to TravelLog object.
		 *
		 * @param array $data
		 * @return TravelLog
		 * @access protected
		 */
		protected static function toTravelLogObject($data) {
			$arr = array(
				'travellogID' => $data->ID,
				'title' => $data->title,
				'description' => $data->description,
				'callout' => null,
				'publish' => null,
				'logdate' => $data->lastupdated,
				'templateType' => null,
				'travelerID' => $data->travelerID,
				'travelID' => $data->t_ID,
				'tripID' => $data->trip,
				'travellinkID' => $data->travellinkID,
				'urlAlias' => null
			);
			$obj = new TravelLog(0, $arr);
			return $obj;
		}
		
		/**
		 * Create blog search query.
		 *
		 * @param array $data
		 * @return string
		 * @access protected
		 */
		protected static function createQuery($data) {
			if ($data[GanetApiConstant::APP_KEY]) {
				if (is_numeric($data[GanetApiConstant::KEYWORD_SUBGROUP]) && $data[GanetApiConstant::KEYWORD_SUBGROUP] > 0) {
					$groupID = $data[GanetApiConstant::KEYWORD_SUBGROUP];
				}
				else {
					$groupID = $data[GanetApiConstant::APP_KEY];
				}
				
				$gIDs = self::getAdminSubGroups($groupID);
				$travIDs = self::getGroupsToTraveler($gIDs);
			}
			if (is_numeric($data[GanetApiConstant::KEYWORD_LOCATION]) && $data[GanetApiConstant::KEYWORD_LOCATION] > 0) {
				$locIDs  = self::getLocationByCountryID($data[GanetApiConstant::KEYWORD_LOCATION]);
			}
			$limit   = is_numeric($data[GanetApiConstant::KEYWORD_COUNT]) ? abs($data[GanetApiConstant::KEYWORD_COUNT]) : GanetApiConstant::DEFAULT_BLOG_COUNT;
			
			$asql1 = "SELECT tblArticle.authorID as travelerID, " . 
						"tblArticle.articleID as ID, " . 
						"tblArticle.locID, " . 
						"tblArticle.title, " . 
						"tblArticle.description, " . 
						"tblArticle.lastedited as lastupdated, " .
						"tblArticle.logdate as _date, " . 							
						"0 as trip, " . 
						"'ARTICLE' as type, " .
						"0 as views, " . 
						"tblArticle.groupID as travellinkID, " .
						"0 as t_ID " .
					" FROM tblArticle ";
			if ($data[GanetApiConstant::KEYWORD_DEFAULT_PHOTO]) {
				$asql1 .=
					"INNER JOIN tblArticletoPhoto " .
						" ON (tblArticle.articleID = tblArticletoPhoto.articleID) ";
					"INNER JOIN tblPhoto " .
						" ON (tblArticletoPhoto.photoID = tblPhoto.photoID) " .
						" AND tblPhoto.primaryphoto = 1 ";
			}
			$asql1 .=
					" WHERE tblArticle.deleted = 0 ";
			if ($data[GanetApiConstant::APP_KEY]) {
				$asql1 .=
					" AND tblArticle.authorID IN ( " .$travIDs. " ) ";
			}
			if (is_numeric($data[GanetApiConstant::KEYWORD_LOCATION]) && $data[GanetApiConstant::KEYWORD_LOCATION] > 0) {
				$asql1 .=
					" AND tblArticle.locID IN ( ".$locIDs. " ) ";
			}
			$asql1 .=
					"GROUP BY tblArticle.articleID";
			
			$asql = "SELECT tblArticle.authorID as travelerID, " . 
						"tblArticle.articleID as ID, " . 
						"tblArticle.locID, " . 
						"tblArticle.title, " . 
						"tblArticle.description, " . 
						"tblArticle.lastedited as lastupdated, " .
						"tblArticle.logdate as _date, " . 							
						"0 as trip, " . 
						"'ARTICLE' as type, " .
						"0 as views, " .
						"tblArticle.groupID as travellinkID, " .
						"0 as t_ID " .
					" FROM tblArticle ";
			if ($data[GanetApiConstant::KEYWORD_DEFAULT_PHOTO]) {
				$asql .=
					"INNER JOIN tblArticletoPhoto " .
						" ON (tblArticle.articleID = tblArticletoPhoto.articleID) ";
					"INNER JOIN tblPhoto " .
						" ON (tblArticletoPhoto.photoID = tblPhoto.photoID) " .
						" AND tblPhoto.primaryphoto = 1 ";
			}
			$asql .=
				"WHERE tblArticle.deleted = 0 ";
			if ($data[GanetApiConstant::APP_KEY]) {
				$asql .=
					"AND tblArticle.groupID = " .mysql_real_escape_string($groupID).
					" ";
			}
			if (is_numeric($data[GanetApiConstant::KEYWORD_LOCATION]) && $data[GanetApiConstant::KEYWORD_LOCATION] > 0) {
				$asql .=
					" AND tblArticle.locID IN ( ".$locIDs. " ) ";
			}
			
			$asql .=
					" GROUP BY tblArticle.articleID UNION ".$asql1;	
			
			$esql = "SELECT tblTravel.travelerID as travelerID, " . 
						"tblTravelLog.travelLogID as ID, " . 
						"tblTravel.locID, " . 
						"tblTravelLog.title, " . 
						"tblTravelLog.description, " . 
						"tblTravelLog.lastedited as lastupdated, " .
						"tblTravelLog.logdate as _date, " .
						"tblTravelLog.tripID as trip, " . 
						"'JOURNAL' as type, " . 
						"tblTravel.viewed as views, " .
						"tblTravel.travellinkID, " .
						"tblTravel.travelID as t_ID " .
					"FROM ";
			if ($data[GanetApiConstant::APP_KEY]) {
				$esql .=
					"tblGroupApprovedJournals as gaj, ";
			}
			$esql .=
					"tblTravel, tblTrips, tblTravelLog " ;
			
			if ($data[GanetApiConstant::KEYWORD_DEFAULT_PHOTO]) {
				$esql .=
					"INNER JOIN tblTravelLogtoPhoto" .
						" ON (tblTravelLog.travelLogID = tblTravelLogtoPhoto.travelLogID) ".
					"INNER JOIN tblPhoto " .
						" ON (tblTravelLogtoPhoto.photoID = tblPhoto.photoID) " .
						" AND tblPhoto.primaryphoto = 1 ";
			}
			if ($data[GanetApiConstant::APP_KEY]) {
				$esql .=
					"WHERE gaj.groupID IN ( " .$gIDs. " ) " . 
						"AND gaj.approved = 1 " . 
						"AND gaj.travelID = tblTravel.travelID " . 
						"AND tblTravel.travelerID IN ( " .$travIDs. " ) ";
			} else {
				$esql .=
					"WHERE 1 ";
			}
			if (is_numeric($data[GanetApiConstant::KEYWORD_LOCATION]) && $data[GanetApiConstant::KEYWORD_LOCATION] > 0) {
				$esql .=
						"AND tblTrips.locID  IN ( ".$locIDs. " ) ";
			}
			
			$esql .=		
						"AND tblTravel.travelID = tblTrips.travelID " . 
						"AND tblTravel.deleted = 0 " .
						"AND tblTravelLog.deleted = 0 " .
						"AND tblTrips.tripID = tblTravelLog.tripID " .
						"GROUP BY ID ";
					
			$sql = 	"SELECT tbl.travelerID, " .
							"tbl.ID, " .
							"tbl.locID, " .
							"tbl.title, " .
							"tbl.description, " .
							"tbl.lastupdated, " .
							"tbl._date, " .
							"tbl.trip, " .
							"tbl.type, " .
							"tbl.views, " .
							"tbl.travellinkID, " .
							"tbl.t_ID " .
						"FROM ( ";
			if ($data[GanetApiConstant::KEYWORD_WITH_ARTICLE]){
				$sql .=	"$esql UNION $asql ";
			}
			else {
				$sql .=	"$esql ";
			}			
			
			if ($data[GanetApiConstant::KEYWORD_SORT] == GanetApiConstant::SORT_RANDOM) {
				$sql .=
						"ORDER BY RAND() ";
			}
			else {
				$sql .=
						"ORDER BY 
							CASE WHEN lastupdated = '0000-00-00 00:00:00' 
								THEN _date 
								ELSE lastupdated 
							END 
						DESC ";
			}
				
			$sql .=
						") as tbl " .
						"LIMIT 0, ".mysql_real_escape_string($limit);
			
			return $sql;	
		}
		
		/**
		 * Retrieve Photo link.
		 *
		 * @param Article | TravelLog
		 * @result string
		 * @access public
		 */
		public static function getPhotoLink($obj) {
			if ($obj->checkHasprimaryPhoto()) {
				return $obj->getPrimaryPhoto()->getPhotoLink('featured');
			} else {
				$randPhoto = $obj->getRandomPhoto();
				if ($randPhoto->getPhotoID() > 0) {
					return $randPhoto->getPhotoLink('featured');
				}
			}
			
			return '';
		}
		
		/**
		 * Retrieve SubgroupIDs.
		 * From Class TravelCB
		 *
		 * @param int $groupID
		 * @result string
		 * @access private
		 */
		private static function getAdminSubGroups($groupID) {
			require_once("travellog/model/Class.AdminGroup.php");
			$obj_admin_group   = new AdminGroup( $groupID );
			$col_groups        = $obj_admin_group->getSubGroups();
			$col_sub_groupID   = array();
			$col_sub_groupID[] = $groupID;
			if (count($col_groups)) {
				foreach ($col_groups as $obj_group){
					$col_sub_groupID[] = $obj_group->getGroupID();		
				}
			}
			return implode(',', $col_sub_groupID); 
		}
		
		/**
		 * Retrieve TravelerIDs from groupIDs passed.
		 * From Class TravelCB
		 *
		 * @param array $groupIDs
		 * @result string
		 * @access private
		 */
		private static function getGroupsToTraveler($groupIDs) {
			$connection = Propel::getConnection();
			
			$query  = sprintf('SELECT gt.travelerID FROM tblGrouptoTraveler as gt, tblTraveler as t WHERE gt.travelerID = t.travelerID AND t.isSuspended = 0 AND gt.groupID IN (%s)', $groupIDs);
			
			$statement = $connection->prepare($query);
			$statement->execute();
			
			$arr  = array();
			while ($resultset = $statement->fetch(PDO::FETCH_OBJ)){
				$arr[] = $resultset->travelerID;
			}
			
			return implode(',', $arr);
		}
		
		/**
		 * Retrieve LocationIDs from locationID of country.
		 * From Class TravelCB
		 *
		 * @param int $locationID The locationID of country
		 * @result string The locationIDs of city/newcity/country of countryID passed
		 * @access private
		 */
		private static function getLocationByCountryID($locationID) {
			$connection = Propel::getConnection();
			
			$query = "SELECT GoAbroad_Main.tbcity.locID FROM GoAbroad_Main.tbcity, GoAbroad_Main.tbcountry " .
				    "WHERE GoAbroad_Main.tbcity.countryID = GoAbroad_Main.tbcountry.countryID AND GoAbroad_Main.tbcountry.locID=" . mysql_real_escape_string($locationID) .
				    " UNION " .
				    "SELECT tblNewCity.locID FROM tblNewCity, GoAbroad_Main.tbcountry " .
				    "WHERE tblNewCity.countryID = GoAbroad_Main.tbcountry.countryID AND GoAbroad_Main.tbcountry.locID=" . mysql_real_escape_string($locationID);
			
			$statement = $connection->prepare($query);
			$statement->execute();
			
			$col_locID = array();
			while ($resultset = $statement->fetch(PDO::FETCH_OBJ)){
				$col_locID[] = $resultset->locID;
			}
			$col_locID[] = $locationID;
			
			return implode(',', $col_locID);
		}
		
	}