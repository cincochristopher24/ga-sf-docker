<?php
	
	/**
	* @package ganetapi
	* 
	* class that represents a row from tblTravelLog
	**/
	require_once('Class.DataModel.php');
	class GanetApiTravelEntry extends DataModel{
		
		private $mArrivalDate = null;
		private $mOwner		  = null;
		private $mViews		  = 0;
		private $mPhoto		  = null;
		private $mLocation	  = null;
		
		/**
		* @param int - ID of a travel entry
		* @param array - associative array for the attributes of a travel entry
		* @return void
		* 
		* instantiates GanetApiTravelEntry
		*/
		public function __construct($ID = null, $data = null){
			parent::initialize('Travel_Logs', 'tblTravelLog');
			if($ID and !$this->load($ID))
				throw new exception("Invalid ID {$ID} passed to " . __CLASS__ . " constructor.");
			if(is_array($data))
				$this->setFields($data);
		}
		
		/**
		* @param String - date specified by the user upon writing the entry
		* @return void
		*
		* sets the date for a travel entry
		*/
		public function setArrivalDate($date){
			$this->mArrivalDate = $date;
		}

		/**
		* @param void
		* @return String - date specified by the user upon writing the entry
		*
		* retrieves date of a travel entry
		*/
		public function getArrivalDate(){
			return $this->mArrivalDate;
		}

		/**
		* @param GanetApiGroup/GanetApiTraveler object
		* @return void
		*
		* assigns an owner to this entry
		*/
		public function setOwner($owner){
			$this->mOwner = $owner;
		}

		/**
		* @param void
		* @return GanetApiGroup/GanetApiTraveler object
		*
		* retrieves owner of this entry
		*/
		public function getOwner(){
			return new GanetApiGroup();
		}
		
		/**
		* @param int
		* @return void
		*
		* assigns travel entry views
		*/
		public function setViews($views){
			$this->mViews = $views;
		}
		
		/**
		* @param void
		* @return int
		*
		* retrieves number of views of this travel entry
		*/
		public function getViews(){
			return $this->mViews;
		}
		
		/**
		* @param String - url of primary photo of this entry
		* @return void
		*
		* assigns the url of the primary photo for this entry
		*/
		public function setPhoto($photo){
			$this->mPhoto = $photo;
		}

		/**
		* @param void
		* @return String - url of primary photo of this entry
		*
		* retrieves the url of the primary photo of this travel entry
		*/
		public function getPhoto(){
			return $this->mPhoto;
		}

		/**
		* @param array ('name' => 'location name', 'flag' => 'flag image address')
		* @return void
		*
		* sets the location where this entry is written
		*/
		public function setLocation($location){
			$this->mLocation = $location;
		}

		/**
		* @param void
		* @return array ('name' => 'location name', 'flag' => 'flag image address')
		*
		* retrieves location where this entry is written
		*/
		public function getLocation(){
			return array('name' => 'Philippines', 'flag' => 'http://linktolocationflagimage.com');
		}
		
		/**
		* @param void
		* @return String - url of the entry
		*
		* retrieves the url of this entry
		*/
		public function getUrl(){
			return 'http://www.goabroad.net/journal-entry.php?action=view&travellogID='.$this->getTravellogID();
		}
		
		/**
		* @param void
		* @return array - formatted array for valid function calls for GanetApiTravelEntry
		*
		* convert journal entry properties, that would be made available to the public, to an associative array
		*/
		public function toArray(){
			return array(
				'ID' 			=> $this->getTravellogID(),
				'title' 		=> $this->getTitle(),
				'description' 	=> $this->getDescription(),
				'url'			=> $this->getUrl(),
				'owner'			=> $this->getOwner(),
				'date'			=> $this->getArrivalDate(),
				'views'			=> $this->getViews(),
				'location'		=> $this->getLocation(),
				'photo'			=> $this->getPhoto()
			);
		}
		
	}