<?php


/**
 * This class adds structure of 'tblGanetApi' table to 'Travel_Logs' DatabaseMap object.
 *
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    apps.travellog.ganetapi.model.propel.map
 */
class GanetApiPropelAuthenticationMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'apps.travellog.ganetapi.model.propel.map.GanetApiPropelAuthenticationMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(GanetApiPropelAuthenticationPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(GanetApiPropelAuthenticationPeer::TABLE_NAME);
		$tMap->setPhpName('GanetApiPropelAuthentication');
		$tMap->setClassname('GanetApiPropelAuthentication');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'ID', 'INTEGER', true, null);

		$tMap->addColumn('KEY', 'Key', 'VARCHAR', false, 50);

		$tMap->addColumn('SECRET', 'Secret', 'VARCHAR', false, 50);

		$tMap->addColumn('GROUPID', 'GroupID', 'INTEGER', false, 11);

	} // doBuild()

} // GanetApiPropelAuthenticationMapBuilder
