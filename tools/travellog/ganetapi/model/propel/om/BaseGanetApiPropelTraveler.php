<?php

/**
 * Base class that represents a row from the 'tblTraveler' table.
 *
 * 
 *
 * @package    apps.travellog.ganetapi.model.propel.om
 */
abstract class BaseGanetApiPropelTraveler extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        GanetApiPropelTravelerPeer
	 */
	protected static $peer;

	/**
	 * The value for the travelerid field.
	 * @var        int
	 */
	protected $travelerid;

	/**
	 * The value for the username field.
	 * @var        string
	 */
	protected $username;

	/**
	 * The value for the firstname field.
	 * @var        string
	 */
	protected $firstname;

	/**
	 * The value for the lastname field.
	 * @var        string
	 */
	protected $lastname;

	/**
	 * The value for the gender field.
	 * @var        int
	 */
	protected $gender;

	/**
	 * The value for the email field.
	 * @var        string
	 */
	protected $email;

	/**
	 * The value for the phone field.
	 * @var        string
	 */
	protected $phone;

	/**
	 * The value for the htlocationid field.
	 * @var        string
	 */
	protected $htlocationid;

	/**
	 * The value for the travelertypeid field.
	 * @var        int
	 */
	protected $travelertypeid;

	/**
	 * The value for the dateregistered field.
	 * @var        string
	 */
	protected $dateregistered;

	/**
	 * The value for the latestlogin field.
	 * @var        string
	 */
	protected $latestlogin;

	/**
	 * The value for the currlocationid field.
	 * @var        string
	 */
	protected $currlocationid;

	/**
	 * The value for the views field.
	 * @var        string
	 */
	protected $views;

	/**
	 * The value for the isadvisor field.
	 * @var        int
	 */
	protected $isadvisor;

	/**
	 * The value for the isfeatured field.
	 * @var        int
	 */
	protected $isfeatured;

	/**
	 * The value for the issuspended field.
	 * @var        int
	 */
	protected $issuspended;

	/**
	 * The value for the active field.
	 * @var        int
	 */
	protected $active;

	/**
	 * The value for the travelerstatus field.
	 * @var        int
	 */
	protected $travelerstatus;

	/**
	 * @var        GanetApiPropelLocation
	 */
	protected $aGanetApiPropelLocationRelatedByHomeTownLocationID;

	/**
	 * @var        GanetApiPropelLocation
	 */
	protected $aGanetApiPropelLocationRelatedByCurrentLocationID;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Initializes internal state of BaseGanetApiPropelTraveler object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
	}

	/**
	 * Get the [travelerid] column value.
	 * 
	 * @return     int
	 */
	public function getID()
	{
		return $this->travelerid;
	}

	/**
	 * Get the [username] column value.
	 * 
	 * @return     string
	 */
	public function getUserName()
	{
		return $this->username;
	}

	/**
	 * Get the [firstname] column value.
	 * 
	 * @return     string
	 */
	public function getFirstName()
	{
		return $this->firstname;
	}

	/**
	 * Get the [lastname] column value.
	 * 
	 * @return     string
	 */
	public function getLastName()
	{
		return $this->lastname;
	}

	/**
	 * Get the [gender] column value.
	 * 
	 * @return     int
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * Get the [email] column value.
	 * 
	 * @return     string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Get the [phone] column value.
	 * 
	 * @return     string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * Get the [htlocationid] column value.
	 * 
	 * @return     string
	 */
	public function getHomeTownLocationID()
	{
		return $this->htlocationid;
	}

	/**
	 * Get the [travelertypeid] column value.
	 * 
	 * @return     int
	 */
	public function getTravelerTypeID()
	{
		return $this->travelertypeid;
	}

	/**
	 * Get the [optionally formatted] temporal [dateregistered] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getDateRegistered($format = 'Y-m-d H:i:s')
	{
		if ($this->dateregistered === null) {
			return null;
		}


		if ($this->dateregistered === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->dateregistered);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->dateregistered, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [optionally formatted] temporal [latestlogin] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getLatestLogin($format = 'Y-m-d H:i:s')
	{
		if ($this->latestlogin === null) {
			return null;
		}


		if ($this->latestlogin === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->latestlogin);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->latestlogin, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [currlocationid] column value.
	 * 
	 * @return     string
	 */
	public function getCurrentLocationID()
	{
		return $this->currlocationid;
	}

	/**
	 * Get the [views] column value.
	 * 
	 * @return     string
	 */
	public function getViews()
	{
		return $this->views;
	}

	/**
	 * Get the [isadvisor] column value.
	 * 
	 * @return     int
	 */
	public function getIsAdvisor()
	{
		return $this->isadvisor;
	}

	/**
	 * Get the [isfeatured] column value.
	 * 
	 * @return     int
	 */
	public function getIsFeatured()
	{
		return $this->isfeatured;
	}

	/**
	 * Get the [issuspended] column value.
	 * 
	 * @return     int
	 */
	public function getIsSuspended()
	{
		return $this->issuspended;
	}

	/**
	 * Get the [active] column value.
	 * 
	 * @return     int
	 */
	public function getIsActive()
	{
		return $this->active;
	}

	/**
	 * Get the [travelerstatus] column value.
	 * 
	 * @return     int
	 */
	public function getTravelerStatus()
	{
		return $this->travelerstatus;
	}

	/**
	 * Set the value of [travelerid] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setID($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->travelerid !== $v) {
			$this->travelerid = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::TRAVELERID;
		}

		return $this;
	} // setID()

	/**
	 * Set the value of [username] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setUserName($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->username !== $v) {
			$this->username = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::USERNAME;
		}

		return $this;
	} // setUserName()

	/**
	 * Set the value of [firstname] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setFirstName($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->firstname !== $v) {
			$this->firstname = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::FIRSTNAME;
		}

		return $this;
	} // setFirstName()

	/**
	 * Set the value of [lastname] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setLastName($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->lastname !== $v) {
			$this->lastname = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::LASTNAME;
		}

		return $this;
	} // setLastName()

	/**
	 * Set the value of [gender] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setGender($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->gender !== $v) {
			$this->gender = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::GENDER;
		}

		return $this;
	} // setGender()

	/**
	 * Set the value of [email] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setEmail($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::EMAIL;
		}

		return $this;
	} // setEmail()

	/**
	 * Set the value of [phone] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setPhone($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->phone !== $v) {
			$this->phone = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::PHONE;
		}

		return $this;
	} // setPhone()

	/**
	 * Set the value of [htlocationid] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setHomeTownLocationID($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->htlocationid !== $v) {
			$this->htlocationid = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::HTLOCATIONID;
		}

		if ($this->aGanetApiPropelLocationRelatedByHomeTownLocationID !== null && $this->aGanetApiPropelLocationRelatedByHomeTownLocationID->getID() !== $v) {
			$this->aGanetApiPropelLocationRelatedByHomeTownLocationID = null;
		}

		return $this;
	} // setHomeTownLocationID()

	/**
	 * Set the value of [travelertypeid] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setTravelerTypeID($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->travelertypeid !== $v) {
			$this->travelertypeid = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::TRAVELERTYPEID;
		}

		return $this;
	} // setTravelerTypeID()

	/**
	 * Sets the value of [dateregistered] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setDateRegistered($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->dateregistered !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->dateregistered !== null && $tmpDt = new DateTime($this->dateregistered)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->dateregistered = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = GanetApiPropelTravelerPeer::DATEREGISTERED;
			}
		} // if either are not null

		return $this;
	} // setDateRegistered()

	/**
	 * Sets the value of [latestlogin] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setLatestLogin($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->latestlogin !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->latestlogin !== null && $tmpDt = new DateTime($this->latestlogin)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->latestlogin = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = GanetApiPropelTravelerPeer::LATESTLOGIN;
			}
		} // if either are not null

		return $this;
	} // setLatestLogin()

	/**
	 * Set the value of [currlocationid] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setCurrentLocationID($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->currlocationid !== $v) {
			$this->currlocationid = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::CURRLOCATIONID;
		}

		if ($this->aGanetApiPropelLocationRelatedByCurrentLocationID !== null && $this->aGanetApiPropelLocationRelatedByCurrentLocationID->getID() !== $v) {
			$this->aGanetApiPropelLocationRelatedByCurrentLocationID = null;
		}

		return $this;
	} // setCurrentLocationID()

	/**
	 * Set the value of [views] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setViews($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->views !== $v) {
			$this->views = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::VIEWS;
		}

		return $this;
	} // setViews()

	/**
	 * Set the value of [isadvisor] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setIsAdvisor($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->isadvisor !== $v) {
			$this->isadvisor = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::ISADVISOR;
		}

		return $this;
	} // setIsAdvisor()

	/**
	 * Set the value of [isfeatured] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setIsFeatured($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->isfeatured !== $v) {
			$this->isfeatured = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::ISFEATURED;
		}

		return $this;
	} // setIsFeatured()

	/**
	 * Set the value of [issuspended] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setIsSuspended($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->issuspended !== $v) {
			$this->issuspended = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::ISSUSPENDED;
		}

		return $this;
	} // setIsSuspended()

	/**
	 * Set the value of [active] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setIsActive($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->active !== $v) {
			$this->active = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::ACTIVE;
		}

		return $this;
	} // setIsActive()

	/**
	 * Set the value of [travelerstatus] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 */
	public function setTravelerStatus($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->travelerstatus !== $v) {
			$this->travelerstatus = $v;
			$this->modifiedColumns[] = GanetApiPropelTravelerPeer::TRAVELERSTATUS;
		}

		return $this;
	} // setTravelerStatus()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			// First, ensure that we don't have any columns that have been modified which aren't default columns.
			if (array_diff($this->modifiedColumns, array())) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->travelerid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->username = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
			$this->firstname = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->lastname = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->gender = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
			$this->email = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->phone = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->htlocationid = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->travelertypeid = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
			$this->dateregistered = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
			$this->latestlogin = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
			$this->currlocationid = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
			$this->views = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
			$this->isadvisor = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
			$this->isfeatured = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
			$this->issuspended = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
			$this->active = ($row[$startcol + 16] !== null) ? (int) $row[$startcol + 16] : null;
			$this->travelerstatus = ($row[$startcol + 17] !== null) ? (int) $row[$startcol + 17] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 18; // 18 = GanetApiPropelTravelerPeer::NUM_COLUMNS - GanetApiPropelTravelerPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating GanetApiPropelTraveler object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aGanetApiPropelLocationRelatedByHomeTownLocationID !== null && $this->htlocationid !== $this->aGanetApiPropelLocationRelatedByHomeTownLocationID->getID()) {
			$this->aGanetApiPropelLocationRelatedByHomeTownLocationID = null;
		}
		if ($this->aGanetApiPropelLocationRelatedByCurrentLocationID !== null && $this->currlocationid !== $this->aGanetApiPropelLocationRelatedByCurrentLocationID->getID()) {
			$this->aGanetApiPropelLocationRelatedByCurrentLocationID = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = GanetApiPropelTravelerPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aGanetApiPropelLocationRelatedByHomeTownLocationID = null;
			$this->aGanetApiPropelLocationRelatedByCurrentLocationID = null;
		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			GanetApiPropelTravelerPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelTravelerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			$affectedRows = $this->doSave($con);
			$con->commit();
			GanetApiPropelTravelerPeer::addInstanceToPool($this);
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aGanetApiPropelLocationRelatedByHomeTownLocationID !== null) {
				if ($this->aGanetApiPropelLocationRelatedByHomeTownLocationID->isModified() || $this->aGanetApiPropelLocationRelatedByHomeTownLocationID->isNew()) {
					$affectedRows += $this->aGanetApiPropelLocationRelatedByHomeTownLocationID->save($con);
				}
				$this->setGanetApiPropelLocationRelatedByHomeTownLocationID($this->aGanetApiPropelLocationRelatedByHomeTownLocationID);
			}

			if ($this->aGanetApiPropelLocationRelatedByCurrentLocationID !== null) {
				if ($this->aGanetApiPropelLocationRelatedByCurrentLocationID->isModified() || $this->aGanetApiPropelLocationRelatedByCurrentLocationID->isNew()) {
					$affectedRows += $this->aGanetApiPropelLocationRelatedByCurrentLocationID->save($con);
				}
				$this->setGanetApiPropelLocationRelatedByCurrentLocationID($this->aGanetApiPropelLocationRelatedByCurrentLocationID);
			}

			if ($this->isNew() ) {
				$this->modifiedColumns[] = GanetApiPropelTravelerPeer::TRAVELERID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GanetApiPropelTravelerPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setID($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += GanetApiPropelTravelerPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aGanetApiPropelLocationRelatedByHomeTownLocationID !== null) {
				if (!$this->aGanetApiPropelLocationRelatedByHomeTownLocationID->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aGanetApiPropelLocationRelatedByHomeTownLocationID->getValidationFailures());
				}
			}

			if ($this->aGanetApiPropelLocationRelatedByCurrentLocationID !== null) {
				if (!$this->aGanetApiPropelLocationRelatedByCurrentLocationID->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aGanetApiPropelLocationRelatedByCurrentLocationID->getValidationFailures());
				}
			}


			if (($retval = GanetApiPropelTravelerPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(GanetApiPropelTravelerPeer::DATABASE_NAME);

		if ($this->isColumnModified(GanetApiPropelTravelerPeer::TRAVELERID)) $criteria->add(GanetApiPropelTravelerPeer::TRAVELERID, $this->travelerid);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::USERNAME)) $criteria->add(GanetApiPropelTravelerPeer::USERNAME, $this->username);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::FIRSTNAME)) $criteria->add(GanetApiPropelTravelerPeer::FIRSTNAME, $this->firstname);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::LASTNAME)) $criteria->add(GanetApiPropelTravelerPeer::LASTNAME, $this->lastname);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::GENDER)) $criteria->add(GanetApiPropelTravelerPeer::GENDER, $this->gender);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::EMAIL)) $criteria->add(GanetApiPropelTravelerPeer::EMAIL, $this->email);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::PHONE)) $criteria->add(GanetApiPropelTravelerPeer::PHONE, $this->phone);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::HTLOCATIONID)) $criteria->add(GanetApiPropelTravelerPeer::HTLOCATIONID, $this->htlocationid);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::TRAVELERTYPEID)) $criteria->add(GanetApiPropelTravelerPeer::TRAVELERTYPEID, $this->travelertypeid);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::DATEREGISTERED)) $criteria->add(GanetApiPropelTravelerPeer::DATEREGISTERED, $this->dateregistered);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::LATESTLOGIN)) $criteria->add(GanetApiPropelTravelerPeer::LATESTLOGIN, $this->latestlogin);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::CURRLOCATIONID)) $criteria->add(GanetApiPropelTravelerPeer::CURRLOCATIONID, $this->currlocationid);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::VIEWS)) $criteria->add(GanetApiPropelTravelerPeer::VIEWS, $this->views);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::ISADVISOR)) $criteria->add(GanetApiPropelTravelerPeer::ISADVISOR, $this->isadvisor);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::ISFEATURED)) $criteria->add(GanetApiPropelTravelerPeer::ISFEATURED, $this->isfeatured);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::ISSUSPENDED)) $criteria->add(GanetApiPropelTravelerPeer::ISSUSPENDED, $this->issuspended);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::ACTIVE)) $criteria->add(GanetApiPropelTravelerPeer::ACTIVE, $this->active);
		if ($this->isColumnModified(GanetApiPropelTravelerPeer::TRAVELERSTATUS)) $criteria->add(GanetApiPropelTravelerPeer::TRAVELERSTATUS, $this->travelerstatus);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GanetApiPropelTravelerPeer::DATABASE_NAME);

		$criteria->add(GanetApiPropelTravelerPeer::TRAVELERID, $this->travelerid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getID();
	}

	/**
	 * Generic method to set the primary key (travelerid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setID($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of GanetApiPropelTraveler (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserName($this->username);

		$copyObj->setFirstName($this->firstname);

		$copyObj->setLastName($this->lastname);

		$copyObj->setGender($this->gender);

		$copyObj->setEmail($this->email);

		$copyObj->setPhone($this->phone);

		$copyObj->setHomeTownLocationID($this->htlocationid);

		$copyObj->setTravelerTypeID($this->travelertypeid);

		$copyObj->setDateRegistered($this->dateregistered);

		$copyObj->setLatestLogin($this->latestlogin);

		$copyObj->setCurrentLocationID($this->currlocationid);

		$copyObj->setViews($this->views);

		$copyObj->setIsAdvisor($this->isadvisor);

		$copyObj->setIsFeatured($this->isfeatured);

		$copyObj->setIsSuspended($this->issuspended);

		$copyObj->setIsActive($this->active);

		$copyObj->setTravelerStatus($this->travelerstatus);


		$copyObj->setNew(true);

		$copyObj->setID(NULL); // this is a auto-increment column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     GanetApiPropelTraveler Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     GanetApiPropelTravelerPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GanetApiPropelTravelerPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a GanetApiPropelLocation object.
	 *
	 * @param      GanetApiPropelLocation $v
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setGanetApiPropelLocationRelatedByHomeTownLocationID(GanetApiPropelLocation $v = null)
	{
		if ($v === null) {
			$this->setHomeTownLocationID(NULL);
		} else {
			$this->setHomeTownLocationID($v->getID());
		}

		$this->aGanetApiPropelLocationRelatedByHomeTownLocationID = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the GanetApiPropelLocation object, it will not be re-added.
		if ($v !== null) {
			$v->addGanetApiPropelTravelerRelatedByHomeTownLocationID($this);
		}

		return $this;
	}


	/**
	 * Get the associated GanetApiPropelLocation object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     GanetApiPropelLocation The associated GanetApiPropelLocation object.
	 * @throws     PropelException
	 */
	public function getGanetApiPropelLocationRelatedByHomeTownLocationID(PropelPDO $con = null)
	{
		if ($this->aGanetApiPropelLocationRelatedByHomeTownLocationID === null && (($this->htlocationid !== "" && $this->htlocationid !== null))) {
			$this->aGanetApiPropelLocationRelatedByHomeTownLocationID = GanetApiPropelLocationPeer::retrieveByPK($this->htlocationid, $con);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->aGanetApiPropelLocationRelatedByHomeTownLocationID->addGanetApiPropelTravelersRelatedByHomeTownLocationID($this);
			 */
		}
		return $this->aGanetApiPropelLocationRelatedByHomeTownLocationID;
	}

	/**
	 * Declares an association between this object and a GanetApiPropelLocation object.
	 *
	 * @param      GanetApiPropelLocation $v
	 * @return     GanetApiPropelTraveler The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setGanetApiPropelLocationRelatedByCurrentLocationID(GanetApiPropelLocation $v = null)
	{
		if ($v === null) {
			$this->setCurrentLocationID(NULL);
		} else {
			$this->setCurrentLocationID($v->getID());
		}

		$this->aGanetApiPropelLocationRelatedByCurrentLocationID = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the GanetApiPropelLocation object, it will not be re-added.
		if ($v !== null) {
			$v->addGanetApiPropelTravelerRelatedByCurrentLocationID($this);
		}

		return $this;
	}


	/**
	 * Get the associated GanetApiPropelLocation object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     GanetApiPropelLocation The associated GanetApiPropelLocation object.
	 * @throws     PropelException
	 */
	public function getGanetApiPropelLocationRelatedByCurrentLocationID(PropelPDO $con = null)
	{
		if ($this->aGanetApiPropelLocationRelatedByCurrentLocationID === null && (($this->currlocationid !== "" && $this->currlocationid !== null))) {
			$this->aGanetApiPropelLocationRelatedByCurrentLocationID = GanetApiPropelLocationPeer::retrieveByPK($this->currlocationid, $con);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->aGanetApiPropelLocationRelatedByCurrentLocationID->addGanetApiPropelTravelersRelatedByCurrentLocationID($this);
			 */
		}
		return $this->aGanetApiPropelLocationRelatedByCurrentLocationID;
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
		} // if ($deep)

			$this->aGanetApiPropelLocationRelatedByHomeTownLocationID = null;
			$this->aGanetApiPropelLocationRelatedByCurrentLocationID = null;
	}

} // BaseGanetApiPropelTraveler
