<?php

/**
 * Base class that represents a row from the 'tblGroup' table.
 *
 * 
 *
 * @package    apps.travellog.ganetapi.model.propel.om
 */
abstract class BaseGanetApiPropelGroup extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        GanetApiPropelGroupPeer
	 */
	protected static $peer;

	/**
	 * The value for the groupid field.
	 * @var        int
	 */
	protected $groupid;

	/**
	 * The value for the name field.
	 * @var        string
	 */
	protected $name;

	/**
	 * The value for the description field.
	 * @var        string
	 */
	protected $description;

	/**
	 * The value for the groupaccess field.
	 * @var        int
	 */
	protected $groupaccess;

	/**
	 * The value for the discriminator field.
	 * @var        int
	 */
	protected $discriminator;

	/**
	 * The value for the moderator field.
	 * @var        string
	 */
	protected $moderator;

	/**
	 * The value for the parentid field.
	 * @var        int
	 */
	protected $parentid;

	/**
	 * The value for the photoid field.
	 * @var        string
	 */
	protected $photoid;

	/**
	 * The value for the datecreated field.
	 * @var        string
	 */
	protected $datecreated;

	/**
	 * The value for the isfeatured field.
	 * @var        int
	 */
	protected $isfeatured;

	/**
	 * The value for the issuspended field.
	 * @var        int
	 */
	protected $issuspended;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Initializes internal state of BaseGanetApiPropelGroup object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
	}

	/**
	 * Get the [groupid] column value.
	 * 
	 * @return     int
	 */
	public function getID()
	{
		return $this->groupid;
	}

	/**
	 * Get the [name] column value.
	 * 
	 * @return     string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Get the [description] column value.
	 * 
	 * @return     string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Get the [groupaccess] column value.
	 * 
	 * @return     int
	 */
	public function getGroupAccess()
	{
		return $this->groupaccess;
	}

	/**
	 * Get the [discriminator] column value.
	 * 
	 * @return     int
	 */
	public function getDiscriminator()
	{
		return $this->discriminator;
	}

	/**
	 * Get the [moderator] column value.
	 * 
	 * @return     string
	 */
	public function getModerator()
	{
		return $this->moderator;
	}

	/**
	 * Get the [parentid] column value.
	 * 
	 * @return     int
	 */
	public function getParentID()
	{
		return $this->parentid;
	}

	/**
	 * Get the [photoid] column value.
	 * 
	 * @return     string
	 */
	public function getPhotoID()
	{
		return $this->photoid;
	}

	/**
	 * Get the [optionally formatted] temporal [datecreated] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getDateCreated($format = 'Y-m-d H:i:s')
	{
		if ($this->datecreated === null) {
			return null;
		}


		if ($this->datecreated === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->datecreated);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->datecreated, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [isfeatured] column value.
	 * 
	 * @return     int
	 */
	public function getIsFeatured()
	{
		return $this->isfeatured;
	}

	/**
	 * Get the [issuspended] column value.
	 * 
	 * @return     int
	 */
	public function getIsSuspended()
	{
		return $this->issuspended;
	}

	/**
	 * Set the value of [groupid] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setID($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->groupid !== $v) {
			$this->groupid = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::GROUPID;
		}

		return $this;
	} // setID()

	/**
	 * Set the value of [name] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setName($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::NAME;
		}

		return $this;
	} // setName()

	/**
	 * Set the value of [description] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setDescription($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->description !== $v) {
			$this->description = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::DESCRIPTION;
		}

		return $this;
	} // setDescription()

	/**
	 * Set the value of [groupaccess] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setGroupAccess($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->groupaccess !== $v) {
			$this->groupaccess = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::GROUPACCESS;
		}

		return $this;
	} // setGroupAccess()

	/**
	 * Set the value of [discriminator] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setDiscriminator($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->discriminator !== $v) {
			$this->discriminator = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::DISCRIMINATOR;
		}

		return $this;
	} // setDiscriminator()

	/**
	 * Set the value of [moderator] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setModerator($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->moderator !== $v) {
			$this->moderator = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::MODERATOR;
		}

		return $this;
	} // setModerator()

	/**
	 * Set the value of [parentid] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setParentID($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->parentid !== $v) {
			$this->parentid = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::PARENTID;
		}

		return $this;
	} // setParentID()

	/**
	 * Set the value of [photoid] column.
	 * 
	 * @param      string $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setPhotoID($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->photoid !== $v) {
			$this->photoid = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::PHOTOID;
		}

		return $this;
	} // setPhotoID()

	/**
	 * Sets the value of [datecreated] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setDateCreated($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->datecreated !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->datecreated !== null && $tmpDt = new DateTime($this->datecreated)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->datecreated = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = GanetApiPropelGroupPeer::DATECREATED;
			}
		} // if either are not null

		return $this;
	} // setDateCreated()

	/**
	 * Set the value of [isfeatured] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setIsFeatured($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->isfeatured !== $v) {
			$this->isfeatured = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::ISFEATURED;
		}

		return $this;
	} // setIsFeatured()

	/**
	 * Set the value of [issuspended] column.
	 * 
	 * @param      int $v new value
	 * @return     GanetApiPropelGroup The current object (for fluent API support)
	 */
	public function setIsSuspended($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->issuspended !== $v) {
			$this->issuspended = $v;
			$this->modifiedColumns[] = GanetApiPropelGroupPeer::ISSUSPENDED;
		}

		return $this;
	} // setIsSuspended()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			// First, ensure that we don't have any columns that have been modified which aren't default columns.
			if (array_diff($this->modifiedColumns, array())) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->groupid = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
			$this->description = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->groupaccess = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
			$this->discriminator = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
			$this->moderator = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->parentid = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
			$this->photoid = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->datecreated = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->isfeatured = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
			$this->issuspended = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 11; // 11 = GanetApiPropelGroupPeer::NUM_COLUMNS - GanetApiPropelGroupPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating GanetApiPropelGroup object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelGroupPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = GanetApiPropelGroupPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelGroupPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			GanetApiPropelGroupPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GanetApiPropelGroupPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			$affectedRows = $this->doSave($con);
			$con->commit();
			GanetApiPropelGroupPeer::addInstanceToPool($this);
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			if ($this->isNew() ) {
				$this->modifiedColumns[] = GanetApiPropelGroupPeer::GROUPID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GanetApiPropelGroupPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setID($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += GanetApiPropelGroupPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = GanetApiPropelGroupPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(GanetApiPropelGroupPeer::DATABASE_NAME);

		if ($this->isColumnModified(GanetApiPropelGroupPeer::GROUPID)) $criteria->add(GanetApiPropelGroupPeer::GROUPID, $this->groupid);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::NAME)) $criteria->add(GanetApiPropelGroupPeer::NAME, $this->name);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::DESCRIPTION)) $criteria->add(GanetApiPropelGroupPeer::DESCRIPTION, $this->description);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::GROUPACCESS)) $criteria->add(GanetApiPropelGroupPeer::GROUPACCESS, $this->groupaccess);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::DISCRIMINATOR)) $criteria->add(GanetApiPropelGroupPeer::DISCRIMINATOR, $this->discriminator);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::MODERATOR)) $criteria->add(GanetApiPropelGroupPeer::MODERATOR, $this->moderator);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::PARENTID)) $criteria->add(GanetApiPropelGroupPeer::PARENTID, $this->parentid);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::PHOTOID)) $criteria->add(GanetApiPropelGroupPeer::PHOTOID, $this->photoid);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::DATECREATED)) $criteria->add(GanetApiPropelGroupPeer::DATECREATED, $this->datecreated);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::ISFEATURED)) $criteria->add(GanetApiPropelGroupPeer::ISFEATURED, $this->isfeatured);
		if ($this->isColumnModified(GanetApiPropelGroupPeer::ISSUSPENDED)) $criteria->add(GanetApiPropelGroupPeer::ISSUSPENDED, $this->issuspended);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GanetApiPropelGroupPeer::DATABASE_NAME);

		$criteria->add(GanetApiPropelGroupPeer::GROUPID, $this->groupid);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getID();
	}

	/**
	 * Generic method to set the primary key (groupid column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setID($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of GanetApiPropelGroup (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setName($this->name);

		$copyObj->setDescription($this->description);

		$copyObj->setGroupAccess($this->groupaccess);

		$copyObj->setDiscriminator($this->discriminator);

		$copyObj->setModerator($this->moderator);

		$copyObj->setParentID($this->parentid);

		$copyObj->setPhotoID($this->photoid);

		$copyObj->setDateCreated($this->datecreated);

		$copyObj->setIsFeatured($this->isfeatured);

		$copyObj->setIsSuspended($this->issuspended);


		$copyObj->setNew(true);

		$copyObj->setID(NULL); // this is a auto-increment column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     GanetApiPropelGroup Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     GanetApiPropelGroupPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GanetApiPropelGroupPeer();
		}
		return self::$peer;
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
		} // if ($deep)

	}

} // BaseGanetApiPropelGroup
