<?php

/**
 * Skeleton subclass for representing a row from the 'tblGroup' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    apps.travellog.ganetapi.model.propel
 */
class GanetApiPropelGroup extends BaseGanetApiPropelGroup {

	/**
	 * Initializes internal state of GanetApiPropelGroup object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}

} // GanetApiPropelGroup
