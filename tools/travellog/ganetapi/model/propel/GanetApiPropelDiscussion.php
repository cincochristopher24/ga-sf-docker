<?php

class GanetApiPropelDiscussion extends BaseGanetApiPropelDiscussion {

	/**
	 * Initializes internal state of GanetApiPropelDiscussion object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}

	/**
	* @param void
    * @return array - formatted array for valid function calls for GanetApiDiscussion
    *
    * convert traveler properties, that would be made available to the public, to an associative array
    */
    public function toArray(){    	
    	return array(
    		'ID' 			=> $this->getID(),
    		'isFeatured'	=> $this->getIsFeatured(),
    		'topicID'		=> $this->getTopicID(),
    		'title'			=> $this->getTitle(),
    		'status'		=> $this->getStatus(),
    		'dateCreated'	=> $this->getDateCreated(),
    		'homepageShown'	=> $this->getHomepageShown(),
    		'dateUpdated'	=> $this->getDateUpdated(),
    		'postsCount'	=> $this->getPostsCount()
    	);
    }

}
