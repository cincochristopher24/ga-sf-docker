<?php
	
	require_once ("Class.ConnectionProvider.php");
	/**
	* @package ganetapi.model
	* @todo implement other functions :)
	*
	* fetches data from tblTravelLog
	*/
	class GanetApiTravelEntryPeer {
		
		/**
		* @param array (limit,order) - filters query results
		* @return array of GanetApiTravelEntry
		*
		* retrieves travel entries/articles according to args passed
		**/
		public static function getTravelEntries($args=array()){
			$rs = ConnectionProvider::instance()->getRecordset();
			$sql = "SELECT * FROM tblTravelLog WHERE deleted = 0 LIMIT 0,3";
			$rs->Execute($sql);
			
			$travelers[] = array();
			while($row = mysql_fetch_assoc($rs->Resultset())){
				$obj = new GanetApiTravelEntry(0, $row);
				$travelEntries[] = $obj;
			}
			return $travelEntries;
		}
		
	}