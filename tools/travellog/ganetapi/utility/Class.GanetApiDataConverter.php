<?php
	
	/**
	* @package ganetapi.utility
	* @todo finish objectToArray todo
	*
	* converts data to GanetApi-valid formats
	*/
	class GanetApiDataConverter{
		
		private static $instance = null;
		private $objectValidator = null;
		
		/**
		* @param void
		* @return GanetApiDataConverter object
		* 
		* Instantiates a class if its not instantiated yet.
		**/
		public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new self;
			}
			return self::$instance;
		}
		
		/**
		* @param void
		* @return void
		*
		* instatiates object validator
		**/
		public function __construct(){
			$this->objectValidator = GanetApiClassMethodMapper::getInstance();
		}
		
		/**
		* @param object/array of objects
		* @return array - array of converted objects
		* @return null if unsuccessful
		* @todo accommodate method that returns array of mixed values(data, object)
		*
		* converts a multidimensional array of object or a single object into a GanetApi-valid array format
		**/
		public function objectToArray($data=null){
			$mainObjHolder = null;
			if(is_array($data)){
				foreach($data as $label => $object){
					if(is_object($object)){
						if($this->objectValidator->isModelClassValid(get_class($object))){
							$objectArray = $object->toArray();
							foreach($objectArray as $key => $value){
								if(is_object($value) || is_array($value)){
									$objectArray[$key] = $this->objectToArray($value);
								}
							}
							$mainObjHolder[get_class($object)] = (isset($mainObjHolder[get_class($object)])) ? $mainObjHolder[get_class($object)] : array();
							$mainObjHolder[get_class($object)][] = $objectArray;
						}	
					}else{
						$mainObjHolder[$label] = $this->objectToArray($object);
					}
				}
			}else if(is_object($data)){
				if($this->objectValidator->isModelClassValid(get_class($data))){
					$objectArray = $data->toArray();
					foreach($objectArray as $key => $value){
						if(is_object($value) || is_array($value)){
							$objectArray[$key] = $this->objectToArray($value);
						}
					}
					return array(get_class($data) => $objectArray);
				}	
			}else{
				return $data;
			}

			return $mainObjHolder;		
		}

		/**
		* @param GanetApi-valid array
		* @return array of objects
		* @return null if unsuccessful
		*
		* converts a a GanetApi-valid array format into an array of objects
		**/
		public function arrayToObject($array = array()){
			if(empty($array)) return null;

			if ( class_exists($className = key($array)) ) {

				if ($isArrayObj = is_int(key($array[$className])) ) {
					$array = array_values($array[$className]);
				}

				foreach ( $array as $value ) {

	            	$newArrObj = new $className($value);
					if ( is_array($value) ) {

						foreach ( $value as $key => $arrValues ) {

							if ( is_array($arrValues) && class_exists(key($arrValues)) ) {
								$newArrObj->data[$key] = $this->convertToObject($arrValues);
							}

						}
					}

	            	$arrObj[] = $newArrObj;
				}

				return $isArrayObj ? $arrObj : $newArrObj;

			} else return null;

		}
		
	}