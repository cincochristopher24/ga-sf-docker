<?php

/**
 * simple wrapper for the traveler web service, please improve
 * @author jonas
 *
 */
class gaTravelerWebService {
	
	private static $instance = NULL;
	
	protected $soapServer = NULL;
	protected $wsdl       = NULL;
	
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new gaTravelerWebService();
		}
		
		return self::$instance;
	}
	
	private function __construct() {
		$this->initialize();
	}
	
	public function __clone() {
		trigger_error('Cloning is not allowed for a singleton object', E_USER_ERROR);
	}
	
	public function initialize() {
		$this->wsdl = 'traveler.wsdl';
		$this->soapServer = new SoapServer($this->wsdl);
		
		$this->registerFunctions();
		
		// disable wsdl cache which is on by default
		ini_set("soap.wsdl_cache_enabled", "0");
	}
	
	public function registerFunctions() {
		$this->soapServer->addFunction(array(
			'getTravelerData',
			'getTravelerDataByID',
			'registerAccount',
			'eMailExists',
			'getTravelerDataByEmail',
		));
	}
	
	public function process() {
		$this->soapServer->handle();
	}
	
}