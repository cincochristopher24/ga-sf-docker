<?/*if(count($updates)):?>
	<ul class="notifier">
		<? $cnt = 0; ?>
		<?foreach($updates as $update):?>
			<li <?if($cnt == (count($updates)-1)):?>class="last"<?endif;?> >
				<span><?=GaDateTime::create($update->getExecutionDate())->commonDateFormat()?></span> 
				<a href="<?=$update->getDoer()->getUrl()?>"><strong><?=$update->getDoer()->getFullName()?></strong></a> <strong><?=$update->getAction()?></strong> 
				<a href="javascript:void(0)" onclick="collectionControl.viewMemberPhotos(<?=$update->getDoer()->getTraveler()->getTravelerID()?>); return false;">View collection.</a>
			</li>
			<? $cnt++; ?>
		<?endforeach;?>
	</ul>
<?endif;*/?>

<ul class="notifier">
<?	if(count($updates)): ?>
	<?	$cnt = 0; ?>
	<?	foreach($updates as $update):
			try{
				$doer = new Traveler($update->getTravelerID());
				$travellog = new TravelLog($update->getSectionRootID());
				if( !($doer instanceof Traveler) ){
					if( $_SERVER["SERVER_NAME"] == "ga-web03.goabroad.net" 
						|| $_SERVER["SERVER_NAME"] == "advisor.dev.goabroad.net"
						|| $_SERVER["SERVER_NAME"] == "dev.goabroad.net" ){
						echo "Invalid Traveler: ".$update->getTravelerID()." feedID: ".$update->getFeedID();
					}
					continue;
				}
				if( !($travellog instanceof TravelLog) ){
					if( $_SERVER["SERVER_NAME"] == "ga-web03.goabroad.net" 
						|| $_SERVER["SERVER_NAME"] == "advisor.dev.goabroad.net"
						|| $_SERVER["SERVER_NAME"] == "dev.goabroad.net" ){
						echo "Invalid TravelLog: ".$update->getSectionRootID()." feedID: ".$update->getFeedID();
					}
					continue;
				}
				
				$countPhotosUploaded = $travellog->getPhotosUploadedByDay($rowslimit=NULL,$date=$update->getFeedDate(),$countOnly=TRUE);
				if( 0 == $countPhotosUploaded ){
					continue;
				}
				$action = 1 < $countPhotosUploaded ? "$countPhotosUploaded Photos" : "$countPhotosUploaded Photo"; 
			}catch(exception $e){
				if( $_SERVER["SERVER_NAME"] == "ga-web03.goabroad.net" 
					|| $_SERVER["SERVER_NAME"] == "advisor.dev.goabroad.net"
					|| $_SERVER["SERVER_NAME"] == "dev.goabroad.net" ){
					echo $e->getMessage()." feedID: ".$update->getFeedID();
				}
				continue;
			}
	?>
		<li <?if($cnt == (count($updates)-1)):?>class="last"<?endif;?> >
			<span><?=GaDateTime::create($update->getFeedDate())->commonDateFormat()?></span>
			<a href="<?=$doer->getFriendlyUrl()?>"><strong><?=$doer->getName()?></strong></a> <strong><?=$doer->getFullName()?></strong> recently uploaded <strong><?=$action?></strong> 
			<a href="javascript:void(0)" onclick="collectionControl.viewMemberPhotos(<?=$update->getTravelerID()?>); return false;">View collection.</a>
		</li>
		<? $cnt++; ?>
	<?endforeach;?>
<?	else: ?>
	<li class="last">
		<span>No recent journal entry photo uploads from your members</span>
	</li>
<?	endif; ?>
</ul>