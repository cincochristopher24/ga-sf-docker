<?php
	require_once("Class.Template.php");
	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");
	//require_once("Cache/ganetCacheProvider.php");
	
	class ViewCollection extends RequestData{
		//private $keySource;
		
		function retrieve(){
			require_once('travellog/UIComponent/Photo/factory/Class.GaCollectionPhotos.php');
			$gaCollectionPhotos = new GaCollectionPhotos();
			$gaCollectionPhotos->setGenID($this->getID());
			$gaCollectionPhotos->setType($this->getType());
			
			$this->setGaCollectionPhotos($gaCollectionPhotos);
			$this->setFactoryPhotos($this->getGaCollectionPhotos()->doCollect());
			$this->setGaCollectionDTO($this->getFactoryPhotos()->loadCollection());
		}
		
		function render(){
			/* caching code */
			/*$cache	=	ganetCacheProvider::instance()->getCache();
			if ($cache != null && $this->getUserLevel() == 0) {
				if ($content = $cache->get($this->keySource->generateCacheKey('list'))){
					echo $content;
					return;
				}
			}*/
			/*		--		*/
			
			$tpl = new Template();
			$tpl->set("action",$this->getAction());
			$tpl->set("userLevel",$this->getUserLevel());
			$tpl->set("type",$this->getType());
			$tpl->set("ID",$this->getID());
			$tpl->set("context",$this->getContext());
			$tpl->set("genID",$this->getGenID());
			$tpl->set("collectionDTO",$this->getGaCollectionDTO());
			ob_start();
			$tpl->out("travellog/UIComponent/Collection/views/tpl.ViewCollection.php");
			$content = ob_get_contents();
			ob_end_flush();
			
			/* write content to cache*/
			/*if ($cache != null && $this->getUserLevel() == 0) {
				$key	=	$this->keySource->generateCacheKey('list');
				$cache->set($key,$content,array('EXPIRE'=>10800));	
				// set also a key based on traveler or group to be used for invalidation
				$invKey =	$this->keySource->generateInvalidationKey();	
				
				if ($hashes	=	$cache->get($invKey)){
					$hashes[]	=	$key;
					$cache->set($invKey,$hashes,array('EXPIRE'=>10800));
				} else {
					$cache->set($invKey,array($key),array('EXPIRE'=>10800));	
				}
			}*/
			/*		--		*/ 	
			
		}
		
		/*function setCacheKeySource($keySource){
			$this->keySource	=	$keySource;
		}*/
	}