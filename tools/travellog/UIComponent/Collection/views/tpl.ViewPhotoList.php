<?php
	function truncateText($text,$max){
		if( $max < strlen($text) ){
			return substr($text,0,$max)."...";
		}
		return $text;
	}
	$albumIsFeatured = 0;
?>

<h1>
	<span id="travelTitle"> <?=$title?> </span>
	<div class="photoaction_top">
		
		<div id="album_control" style="display:none;">
			<? 	if($isPrivileged && 0 < count($photos) ): ?>
				<input type="button" id="btnRearrange" value="Rearrange Photos" class="button_v3" class="button_v3" onclick="collectionControl.loadRearrangeView(); return false;" <?if("journal"==$context):?>style="display:none;"<?endif;?>/>
			<?	endif; ?>
			
			<?	if( "photoalbum" == $context && $isPrivileged ): ?>
				<?php
					$albumIsFeatured = $photoContext->getIsFeatured();
				?>
				<?	if( $albumIsFeatured || !$albumIsFeatured && $group->getFeaturedAlbumCount() < 3 ): ?>
					<input class="button_v3" type="button" value="<?=$photoContext->isFeatured()? 'Remove from Featured' : 'Feature in Homepage'?>" id="btnFeatureAlbum" onclick="collectionControl.featureAlbum(); return false;"/>
				<?	endif; ?>
				<input class="button_v3" type="button" value="Add Member Photos" onclick="window.location.replace('/collection.php?type=<?=$type?>&ID=<?=$ID?>&context=<?=$context?>&genID=<?=$genID?>&action=6');"/>
			<?	endif; ?>

			<?	if( "travellog" == $context && $isPrivileged ): ?>
				<input type="button" class="button_v3" value="Go to Journal" onclick="window.location.replace('/travellog.php?action=view&travellogID=<?=$genID?>')"/>
			<?	endif; ?>
			
			<?	if( "article" == $context && $isPrivileged ): ?>
				<input type="button" class="button_v3" value="Go to Article" onclick="window.location.replace('/article.php?action=view&articleID=<?=$genID?>')"/>
			<?	endif; ?>
			
			<?	if( $isPrivileged ): ?>
				<?	if( "journal" == $context ): ?>
					<input type="button" id="btnUpload" class="button_v3" value="<?=(0==count($photos))? 'Add Photos' : 'Add More Photos';?>" style="display:none;"onclick="collectionControl.loadUploader(); return false;"/>
				<?	else: ?>
					<input type="button" id="btnUpload" class="button_v3" value="Add More Photos" onclick="collectionControl.loadUploader(); return false;"/>
				<?	endif; ?>
			<?	endif; ?>
			
			<?	if( "journal" == $context ): ?>
					<div><strong>Select an Entry:</strong>
					<select id="cboTravelLogs" onchange="collectionControl.setActiveTravelLog(); return false;">
						<option value="0" selected>-- Displaying all entries --</option>
					<?	foreach($travellogs as $travellog): ?>
						<option value="<?=$travellog->getTravelLogID()?>"><?=truncateText(htmlspecialchars(stripslashes($travellog->getTitle()),ENT_QUOTES),40)?></option>
					<?	endforeach; ?>
					</select>
					</div>
			<?	endif; ?>
		</div>
		
		<?	if( 0 < count($photos) ): ?>
		<div id="view_mode_control">
			<input id="btnGalleryView" type="button" class="button_v3" value="Gallery View" onclick="collectionControl.loadGalleryView(); return false;"/>
			<input id="btnThumbnailView" type="button" class="button_v3" value="Thumbnail View" onclick="collectionControl.toThumbnailView(); return false;" style="display: none;"/>
		</div>
		<?	endif; ?>
		
	</div>
</h1>

	<div id="thumbnail_view">
		<ul class="photo_list">
			<?	
				$photocount = count($photos);
				$photolinks = array(); 
				$thumblinks = array();
				$ids = array();
				$grabbed = array();
				$tlogs = array();
				$captions = array();
				//$dateUploaded = array();
				$journalLinks = array();
				$journalTitles = array();
				
				if( "photoalbum"==$context && "group" == $type && $isPrivileged ){
					$original = $photoContext->getOriginalUploadedIDs();
				}
				
				foreach($photos as $photo):
					$photocontext = $photo->getContext();
					if( !is_object($photocontext) ){
						$photocount--;
						continue;
					}
				
					$thumblink = $photo->getPhotoLink('thumbnail');
					
					$ids[] = $photo->getPhotoID();
					$photolinks[] = $photo->getPhotoLink('fullsize');
					$thumblinks[] = $thumblink;
					
					if( "photoalbum"==$context && "group" == $type && $isPrivileged ){//check if the photo of a group album is grabbed from other traveler collection
						if( in_array($photo->getPhotoID(),$original) ){
							$grabbed[] = $photoContext->isGrabbedPhoto($photo->getPhotoID()) ? "1" : "0" ;
						}else{
							$grabbed[] = "1";
						}
						//$grabbed[] = in_array($photo->getPhotoID(),$original) ? "0" : "1" ;
					}else{
						$grabbed[] = "0";
					}
					
					if( "journal" == $context ){
						$tlogs[] = $photo->getContext()->getTravelLogID();
					}else{
						$tlogs[] = "0";
					}

					$captions[] = $photo->getCaption();
					//$dateUploaded[] = $photo->getDateuploaded();
					if( 2 == $photo->getPhototypeID() ){
						$journalLinks[] = $photo->getContext()->getFriendlyURL();
						$journalTitles[] = stripslashes($photo->getContext()->getTitle());
					}else if( 11 == $photo->getPhototypeID() ){
						$journalLinks[] = "/article.php?action=view&amp;articleID=$genID";
						$journalTitles[] = stripslashes($photo->getContext()->getTitle());
					}else{
						$journalLinks[] = "";
						$journalTitles[] = "";
					}
			?>
			
				<li>
					<a href="" onclick="collectionControl.loadGalleryView(<?=$photo->getPhotoID()?>); return false;"><img title="<?=htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES)?>" width="65" height="65" src="<?=$thumblink?>"/></a>
				</li>
			
			<?	endforeach;
					$photolinks = implode(",",$photolinks); 
					$thumblinks = implode(",",$thumblinks); 
					$ids = implode(",",$ids); 
					$grabbed = implode(",",$grabbed);
					$uniqueLogs = implode(",",array_unique(array_merge(array(0),$tlogs)));
					$tlogs = implode(",",$tlogs);
					$tlog_photo_count = implode(",",array_merge(array(0),$tlog_photo_count));
			 ?>
		</ul>
	</div>
	
	<div id="gallery_view" style="display:none;" class="viewer content">
		<?	if( $isPrivileged ): ?>
			<div id="photo_control" class="uploaded">
				<a id="delete_photo_control" class="delete" href="" onclick="collectionControl.deletePhoto(); return false;"><strong>Delete</strong></a>
				<a id="remove_photo_control" class="delete" href="" onclick="collectionControl.removeGrabbedPhoto(); return false;" style="display:none;"><strong>Remove</strong></a>
				<span id="image_process_control">
					<input type="button" value="Rotate Right" class="button_v3" onclick="collectionControl.rotateRight(); return false;" />
					<input type="button" value="Rotate Left" class="button_v3" onclick="collectionControl.rotateLeft(); return false;" />
					<input type="button" value="Thumbnail Crop" class="button_v3" onclick="collectionControl.loadCropper(); return false;" />
				</span>
				<input type="button" id="btnSetAsPrimary" value="Set as Primary" class="button_v3" onclick="collectionControl.setAsPrimary(); return false;" />
				<?	if( "journal" == $context || "travellog" == $context ): ?>
					<input type="button" id="btnSetAsJournalPrimary" value="Set as Journal Primary" class="button_v3" onclick="collectionControl.setAsJournalPrimary(); return false;"/>
				<?	endif; ?>
			</div>
			<div id="request_status" style="display:none;">
				<img src="/images/load_gray.gif" />Sending request. Please wait...
			</div>
		<?	endif; ?>
		
		<div class="viewed_image">
			<?	if( 1 < count($photos) ): ?>
				<div class="viewed_navi">
					<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
					<p name="photo_position_marker" class="jLeft"></p>
					<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
				</div>
			<?	endif; ?>
			<div class="viewed_holder">
				<div class="navhover" id="navhover">
						<a class="prev_photo" id="prev_photo" onclick="collectionControl.movePrev(); return false;" href="javascript:void(0)"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>
						<a class="next_photo" id="next_photo" onclick="collectionControl.moveNext(); return false;" href="javascript:void(0)"><span class="txtblock">Next<span class="imgblock"></span></span></a>
				</div>
				<img id="active_photo" src="" align="middle" border="0" />
			</div>
			<?	if( 1 < count($photos) ): ?>
				<div class="viewed_navi">
					<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
					<p name="photo_position_marker" class="jLeft"></p>
					<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
				</div>
			<?	endif; ?>	
		</div>
		
	</div>
	<div class="comment_info" id="comment_info">
			
			<?	if( $isPrivileged ): ?>
				<div id="caption_control" style="display:none;">
					<textarea style="width:709px;height:45px;" id="txtCaption" name="txtCaption" onchange="collectionControl.captionChanged(this); return false;" onkeyup="collectionControl.captionChanged(this); return false;"></textarea>
					<input type="button" value="Cancel" id="btnCancelEditCaption" class="button_v3" onclick="collectionControl.cancelEditCaption(); return false;" />
					<input type="button" value="Save" id="btnSaveCaption" class="button_v3" onclick="collectionControl.saveCaption(); return false;" />
				</div>
			<?	endif; ?>
			
			<p id="caption">						
			</p>

			<?	if( $isPrivileged ): ?>
				<input type="button" value="Edit Caption" id="btnEditCaption" onclick="collectionControl.editCaption(); return false;" class="button_v3" style="display:none;" />
			<?	endif; ?>
			
			<div class="comment_alt">
				<img class="book_open" id="book_icon" alt="journal" src="/images/book_open.gif" style="display: none;"/>
				<a class="journal_title" id="journal_link" title="" href="">
					<strong><span id="context_title"></span></strong>
				</a>
				<img id="loading_photo_" src="/images/loading_small.gif" align="center" style="display:none;"/>
				<?/*span id="date_uploaded"></span*/?>
			</div>
	</div>
	
<?	if( "journal" == $context && 0 < $photocount ) :	?>
	<div id="travellog_thumbnails" style="display: none;">
		<div id="travellog_empty" >
			<div class="notification middle">
				<h2>This journal entry doesn't have a photo yet</h2>
				<?	if( $isPrivileged ): ?>
					<p>
						Click on the Upload button above to upload photos.
					</p>
				<?	endif; ?>
			</div>
		</div>
		<?	foreach($entry_photos as $photos): ?>
			<?	if( 0 == count($photos) ): ?>
				<?	continue; ?>
			<?else: ?>
				<div id="travellog_<?=$photos[0]->getContext()->getTravelLogID()?>">
					<ul class="photo_list">
					<?	foreach( $photos as $photo): ?>
						<li>
							<a href="" onclick="collectionControl.loadGalleryView(<?=$photo->getPhotoID()?>); return false;"><img title="<?=htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES)?>" width="65" height="65" src="<?=$photo->getPhotoLink('thumbnail')?>"/></a>
						</li>
					<?	endforeach; ?>
					</ul>
				</div>
			<?	endif; ?>
		<?	endforeach; ?>
	</div>
<?	endif; ?>

<div id="collection_items_info" style="display: none;">
	<?	for($i=0; $i<count($captions); $i++): 
			$caption = $captions[$i];
			$link = $journalLinks[$i];
			//$uploaded = $dateUploaded[$i];
			$jtitle = $journalTitles[$i];
	?>
		<p id="caption_<?=$i?>"><?=htmlspecialchars(stripslashes(strip_tags($caption)),ENT_QUOTES)?></p>
		<p id="link_<?=$i?>"><?=$link?></p>
		<p id="jtitle_<?=$i?>"><?=htmlspecialchars(stripslashes(strip_tags($jtitle)),ENT_QUOTES)?></p>
		<?/*p id="uploaded_<?=$i?>"><?=$uploaded?></p*/?>
	<?	endfor; ?>
</div>
<script type="text/javascript">
	collectionControl.setContext("<?=$context?>");
	collectionControl.setPrivileged(<?=$userLevel?>);
	collectionControl.initCollection(<?=$photocount?>,"<?=$ids?>","<?=$photolinks?>","<?=$thumblinks?>","<?=$grabbed?>","<?=$tlogs?>","<?=$uniqueLogs?>",0,"<?=$tlog_photo_count?>");
	collectionControl.setContextPrimary(<?=$contextPrimary?>);
	collectionControl.setEntryPrimaries("<?=implode(',',$travellogIDs)?>","<?=implode(',',$entryPrimaries)?>");
	collectionControl.setJournalPrimary(<?=$journalPrimary?>);
	
	collectionControl.albumIsFeatured = <?=$albumIsFeatured?>;
	
	jQuery("#active_photo").bind("load",
		function(){
			if( null != jQuery("#navhover") ){
				collectionControl.resetNavOnPhoto();
			}
		}
	);
	
</script>