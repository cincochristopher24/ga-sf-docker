<?
	if (isset($type) && "group" == $type){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else{
			if(isset($isAdmin) && 1 == $isAdmin)
				Template::setMainTemplateVar('page_location', 'My Passport');
			else
				Template::setMainTemplateVar('page_location', 'Groups / Clubs');
		}
	}
?>
<?=$contextInfo->render()?>		
<?=$subNavigation->show()?>

<div id = "content_wrapper" class="control_divider">
	<div id = "rundown">
		<div id = "wide_column" class = "layout2">
			<?	if( $isPrivileged ): ?>
				<div id="delete_album_notice" style="margin-bottom: 10px; display:none;" class="errors">
					<p align="center">
						<strong>WARNING!!!</strong>
					</p>
					<p>
						The album <strong><span id="title_of_album_to_delete"></span></strong> has <strong><span id="album_photo_count"></span></strong> in it. Deleting the album will erase <span id="photo_count_description"></span>.
					</p>
					<p>
						Check this box <input type="checkbox" id="chkDeleteNonEmptyAlbum" /> if you are sure you want to do this.
					</p>
					<p>
						Click on the <strong>Delete Album</strong> button below to continue.
					</p>
					<div id="delete_album_links">
						<input type="button" id="btnContinueDeleteNonEmptyAlbum" value="Delete Album" class="button_v3" onclick="collectionControl.continueDeleteNonEmptyAlbum(); return false;"/>
						<input type="button" id="btnCancelDeleteNonEmptyAlbum" value="Cancel" class="button_v3" onclick="collectionControl.cancelDeleteNonEmptyAlbum(); return false;"/>
					</div>
					<div id="sendingDeleteNonEmptyAlbumStatus" style="display:none;">
						<img src="/images/load_gray.gif"/> Please wait while sending request...
					</div>
				</div>
			<?	endif; ?>
			<div id="album_menu" style="display:none;" class="media_control">
			</div>
			<div id="photo_main_view" class="section">
					<?=$content->render()?>
			</div>
			<div id="rearrange_loader" class="section" style="display:none;">
				<img src="/images/load_gray.gif" />
			</div>
			<div id="rearrange_area" class="section">
			</div>
		</div>
		<div id = "narrow_column">
			<div class = "section nbsection">
				<?=$collection->render()?>
			</div>
		</div>
		<div id="cache_element"></div>
		
		<div id="d_info" style="display:none;">
			<input type="hidden" id="d_type" value="<?=$type?>" />
			<input type="hidden" id="d_ID" value="<?=$ID?>" />
			<input type="hidden" id="d_context" value="<?=$context?>" />
			<input type="hidden" id="d_genID" value="<?=$genID?>" />
			<input type="hidden" id="d_privileged" value="<?=$isPrivileged?>" />
			<input type="hidden" id="d_server" value="<?='http://'.$_SERVER["SERVER_NAME"]?>" />
		</div>
	</div>
</div>

<script type="text/javascript">
	if( 'undefined' != typeof(collectionControl) ){
		collectionControl.setType(jQuery("#d_type").val());
		collectionControl.setID(jQuery("#d_ID").val());
		collectionControl.setContext(jQuery("#d_context").val());
		collectionControl.setGenID(jQuery("#d_genID").val());
		collectionControl.setPrivileged(jQuery("#d_privileged").val());
		collectionControl.setServer(jQuery("#d_server").val());
		jQuery("#d_info").remove();
	}
</script>