<div class="feds">
	<?if(count($collectionDTO)):?>
			<a name="anchor"></a>	
			<h2>
				<span>Most recent photo updates</span>
			</h2>

					<?	$albumWithUpdates = 0;
						foreach( $collectionDTO as $collection ):
							$addedPhotoCount = 0;
							$albumWithUpdates++;
					?>
						<? $album = $collection['ALBUM']; ?>
						<? $details = $collection['DETAILS'];?>
						<div class="fedslist">
							<p id="album_update_<?=$albumWithUpdates?>">
								<strong><a href="<?=$album->getContextLink()?>"><?=$album->getPhotoAlbum()->getTitle()?></a></strong>
								 on <?=GaDateTime::create($album->getExecutionDate())->commonDateFormat()?>
								<?//=$album->getAction()?>
							</p>
							
								<?foreach($details as $detail):?>
									<?if(count($detail->getPhotos())):?>
										<div class="update_info"><span><?=$detail->getAction()?></span></div>
										<? 	
											$photos = $detail->getPhotos();
											$addedPhotoCount += count($photos);
											$rnd = rand(0,(count($photos)-1));
											$used_nums = array();
											$max = (count($photos) < 10)	?	count($photos)	: 10;
											for($i = 0; $i < $max; ):
												if(!in_array($rnd,$used_nums)){
										?>			<a href="<?=$album->getContextLink()?>" class="item"><img src="<?=$photos[$rnd]->getPhotoLink('thumbnail')?>" alt="<?=htmlspecialchars(stripslashes($photos[$rnd]->getCaption()))?>"/></a>
										<?			$used_nums[] = $rnd;
													$i++;
												}
												$rnd = rand(0,(count($photos)-1));
											endfor;
										?>
									<?endif;?>	
								<?endforeach;?>
							<?php
								$label = 1 < $addedPhotoCount ? "<strong>$addedPhotoCount Photos</strong> were added to " : "<strong>$addedPhotoCount Photo</strong> was added to ";
							?>
							<script type="text/javascript">
								jQuery("#album_update_<?=$albumWithUpdates?>").html('<?=$label?>'+jQuery("#album_update_<?=$albumWithUpdates?>").html());
							</script>
						</div>	
					<?endforeach;?>

	<?endif;?>
	<?if(count($deleteUpdates)):?>
			<a name="anchor"></a>	
			
			<br/>
			
			<h2>
				<span>Recently Deleted Photos From Album</span>
			</h2>

					<?foreach( $deleteUpdates as $deleteUpdate ):?>
						<? $album = $deleteUpdate['ALBUM']; ?>
						<? $details = $deleteUpdate['DETAILS'];?>
						<div class="fedslist">
							<p><strong><a href="<?=$album->getContextLink()?>">
								<?=$album->getPhotoAlbum()->getTitle()?>
							</a></strong> <?=$album->getAction()?>
							<? $cnt = 0; ?>
								<?foreach($details as $detail):?>
									<span <?if($cnt == (count($details)-1)):?>class="last"<?endif;?>>
										<?=GaDateTime::create($detail->getExecutionDate())->commonDateFormat()?> - 
										<?$doer = ($detail->getDoer()->getTraveler()->getTravelerID() == $_SESSION['travelerID']) ? "<strong>You</strong>" : '<a href="'.$detail->getDoer()->getUrl().'"><strong>'.$detail->getDoer()->getFullName().'</strong></a>' ?>
										<?=$doer?> <?=$detail->getAction()?>
									<? $cnt++; ?></span>
								<?endforeach;?>
							</p>	
						</div>
					<?endforeach;?>

	<?endif;?>
</div>