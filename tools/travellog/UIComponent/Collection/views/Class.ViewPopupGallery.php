<?php
	require_once("Class.Template.php");
	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");

	class ViewPopupGallery extends RequestData{
		
		private $mDefaultPhoto = NULL;
		private $mPhotos = array();
		
		function setDefaultPhoto($photo=NULL){
			$this->mDefaultPhoto = $photo;
		}
		
		function setPhotos($photos=array()){
			$this->mPhotos = $photos;
		}
		
		function render(){
			$photoContext = $this->getPhotoContext();
			$content = new Template();
			$content->set("context",$this->getContext());
			$content->set("genID",$this->getGenID());
			$content->set("defaultPhoto",$this->mDefaultPhoto);
			$content->set("photos",$this->mPhotos);
			$content->set("title",$photoContext->getHeaderCaption());
			$content->out("travellog/UIComponent/Collection/views/tpl.ViewPopupGallery.php");
		}
	}