<h1><span id="travelTitle"> <?=stripslashes($title)?> </span></h1>
<div class="content">
	<?	if( $hasUploadError ): ?>
		<div id="uploadrules">
			<?=$errorMessage?>
		</div>
	<?	endif; ?>
	<form id="frmManagePhotos" action="" method="post">
		<ul class="uploadlist">
			<?	
				if( 1 < $userLevel && "photoalbum"==$context ){
					$original = $photoContext->getOriginalUploadedIDs();
				}
				foreach($photos as $x => $photo): 
					$grabbed = FALSE;
					if( 1 < $userLevel && "photoalbum"==$context ){
						if( in_array($photo->getPhotoID(),$original) ){
							$grabbed = $photoContext->isGrabbedPhoto($photo->getPhotoID());
						}else{
							$grabbed = in_array($photo->getPhotoID(),$original)? FALSE : TRUE;
						}
					}
					$isPrimary = FALSE;
					if( "travellog" == $context || "profile" == $context || "article" == $context){
						$isPrimary = ( 1==$photo->getPrimaryPhoto() )? TRUE : FALSE;
					}else{
						$isPrimary = ( $photo->getPhotoID() == $photoContext->getPrimary() )? TRUE : FALSE;
					}
			?>
				<li <?if( 0 == ($x % 2) ): ?>class="clear"<?endif;?> >
					<div class="uploaded_photo" style="border: none;">
						<img id="standard_<?=$photo->getPhotoID()?>" src="<?=$photo->getPhotoLink('standard')?>" style="border: 2px solid #eee;" />
					</div>
					<div name="image_control" class="thumb_option">
						<img id="thumbnail_<?=$photo->getPhotoID()?>" src="<?=$photo->getPhotoLink('thumbnail')?>">
						<?	if( !$grabbed ): ?>
							<input type="button" value="Thumbnail Crop" class="button_v3" onclick="collectionControl.loadCropper(<?=$photo->getPhotoID()?>); return false;"/>
							<input type="button" value="Rotate Left" class="button_v3" onclick="collectionControl.rotateLeft2(<?=$photo->getPhotoID()?>); return false;"/>
							<input type="button" value="Rotate Right" class="button_v3" onclick="collectionControl.rotateRight2(<?=$photo->getPhotoID()?>); return false;" />
						<?	endif; ?>
					</div>
					<div class="comment_field">
						<?	if( $isPrimary ): ?>
							<span><input type="radio" name="radPrimary" value="<?=$photo->getPhotoID()?>" checked="checked" /> Set as Primary Photo</span>
						<?	else: ?>
							<span><input type="radio" name="radPrimary" value="<?=$photo->getPhotoID()?>" /> Set as Primary Photo</span>
						<?	endif; ?>
						
						<?	if( !$grabbed ): ?>
							<p><input type="checkbox" name="chkDeletePhoto" value="<?=$photo->getPhotoID()?>" onclick="collectionControl.updateDeleteMarkedButton(this);" />Mark for Deletion</p>
						<?	endif; ?>
						
						<?	$caption = htmlspecialchars(stripslashes($photo->getCaption()),ENT_QUOTES);
							if( $grabbed ):  ?>
							<?	if( 0 < strlen($caption) ): ?>
								<div class="grabbed_caption"><?=$caption?></div>
							<?	endif; ?>
						<?	else: ?>
							<textarea style="width:275px;height:50px;" name="txtCaption_<?=$photo->getPhotoID()?>" onchange="collectionControl.captionChanged(this); return false;" onkeyup="collectionControl.captionChanged(this); return false;" ><?=$caption?></textarea>
						<?	endif; ?>
					</div>
				</li>
			<?	endforeach; ?>
		</ul>		
		<div class="photoaction_bottom">
			<p id="manage_status" style="display:none;">
				<img src="/images/load_gray.gif" />Sending request. Please wait...
			</p>
			<p id="manage_control">
				<input id="btnDeleteMarked" class="button_v3" type="button" value="Delete Marked Photos" onclick="collectionControl.deleteMarked(); return false;" style="display:none;"/>&nbsp;&nbsp;			
				<input class="button_v3" type="button" value="Save Changes" onclick="collectionControl.saveChanges(); return false;"/>
				<input class="button_v3" type="button" value="Upload More" onclick="collectionControl.loadUploader(); return false;">
				<input class="button_v3" type="button" value="Back to Album View" onclick="collectionControl.toAlbumView(); return false;">		
			</p>
		</div>
	</form>	
</div>
<script type="text/javascript">
	collectionControl.setPhotoCount(<?=count($photos)?>);
</script>