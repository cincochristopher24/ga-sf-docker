<?php
	$checked = $album->isFeatured() ? 'checked="true"' : "";
	$primary = $album->getPrimaryPhoto();
	$photoCount = $album->getCountPhotos();
	$divider = "divider";
	if( 1 == $itemNumber%3 ) {
	$divider = "";
	}
	

	$dimension = $primary->getAdjustedThumbnailDimension($descriptiveSize='featured', $targetSize=100);
?>
<div class="albumBox <?=$divider?>">
		<?	if( 0 < $photoCount ): ?>
		<a href="javascript:void(0)" onclick="collectionPopup.loadGallery('photoalbum',<?=$album->getPhotoAlbumID()?>,<?=$primary->getPhotoID()?>); return false;">
			<img class="photo_bg jRight" src="<?=$primary->getPhotoLink('featured')?>" width="<?=$dimension['width']?>" height="<?=$dimension['height']?>" />
		</a>
		<?	else: ?>
			<img class="photo_bg jRight" src="<?=$primary->getPhotoLink('featured')?>" width="<?=$dimension['width']?>" height="<?=$dimension['height']?>" />
		<?	endif; ?>
		<div class="text">
			<span>
				<strong><a href="javascript:void(0)" onclick="collectionPopup.loadGallery('photoalbum',<?=$album->getPhotoAlbumID()?>,<?=$primary->getPhotoID()?>); return false;"><?=$album->getTitle()?></a></strong>
				<br />
				<span style="font-size: 10px; text-align: right;"><?=1<$photoCount ? "$photoCount Photos" : "$photoCount Photo"?></span>
				<br />
				<p>
					<input type="checkbox" id="albumToFeature_<?=$album->getPhotoAlbumID()?>" name="albumToFeature" value="<?=$album->getPhotoAlbumID()?>" <?=$checked?> onchange="collectionControl.featuredAlbumsChanged(this);"/>
					Feature
				</p>
			</span>
		</div>
</div>