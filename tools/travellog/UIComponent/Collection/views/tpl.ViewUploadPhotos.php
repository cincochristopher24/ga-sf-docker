<h1>
	<span id="travelTitle"><?=$title?></span>
</h1>

<div class="content">
	<div class="notification middle">
		<?	if("group"==$type): ?>
			<h2>Start adding photos on this album</h2>
			<p>You can upload the photos straight from your desktop or you might want to link photo from your members</p>
		
			<form action="">
				<input class="button_v3" type="button" value="Upload Photos" onclick="collectionControl.loadUploader(); return false;"/>
				<input class="button_v3" type="button" value="Add Members Photos"/>
			</form>
		<?	elseif("traveler"==$type && "profile"==$context): ?>
			<h2>Start adding photos to your profile.</h2>
			<p>You can upload the photos straight from your desktop.</p>
	
			<form action="">
				<input class="button_v3" type="button" value="Upload Photos" onclick="collectionControl.loadUploader(); return false;"/>
			</form>
		<?	endif; ?>
	</div>			
</div>