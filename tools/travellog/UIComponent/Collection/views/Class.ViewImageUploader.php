<?php

	require_once("Class.Template.php");
	require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");

	class ViewImageUploader extends RequestData{
		
		function render(){
			$photoContext = $this->getPhotoContext();
			
			$tpl = new Template();
			if( "journal" == $this->getContext() ){
				$tpl->set("title",$this->getCollection($this->getGenID())->getTitle());
			}else{
				$tpl->set("title",$photoContext->getHeaderCaption());
			}
			$tpl->set("type",$this->getType());
			$tpl->set("ID",$this->getID());
			$tpl->set("context",$this->getContext());
			$tpl->set("genID",$this->getGenID());
			$tpl->set("server",$_SERVER["SERVER_NAME"]);
			$tpl->set("loggedID",$this->getLoggedID());
			$tpl->set("userLevel",$this->getUserLevel());
			$tpl->out("travellog/UIComponent/Collection/views/tpl.ViewImageUploader.php");
		}
	}