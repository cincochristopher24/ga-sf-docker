<div class="quick_create">
	<h2>Manage Featured Albums <img src="/images/load_gray.gif" id="updateManageFeaturedAlbumsViewStatus" style="display: none;"/></h2>
	
	<div class="notification">You can select up to 3 photo albums to feature on your home page. 
	<?	if( 0 == $albumCount ): ?>
		However, you don't have an album for your group yet. Create photo albums now!
	</div>
	<?	else: ?>
		You currently have <strong><?=$albumCount?> <?=1<$albumCount? "Albums" : "Album"?></strong>.
	</div>
		<form id="frmManageFeaturedAlbums">
			<?php
				if( 0 < count($featuredAlbums) || 0 < count($unfeaturedAlbums) ){
					require_once("travellog/UIComponent/Collection/views/Class.ViewManageFeaturedAlbumList.php");
				}
				if( 0 < count($featuredAlbums) ){
					$featuredListView = new ViewManageFeaturedAlbumList();
					$featuredListView->setAlbums($featuredAlbums);
					$featuredListView->setIsFeatured(TRUE);
					$featuredListView->render();
				?>
				<script type="text/javascript">
					collectionControl.featuredAlbumCount = <?=count($featuredAlbums)?>;
				</script>
				<?php
				}
				if( 0 < count($unfeaturedAlbums) ){
					$unfeaturedListView = new ViewManageFeaturedAlbumList();
					$unfeaturedListView->setAlbums($unfeaturedAlbums);
					$unfeaturedListView->setIsFeatured(FALSE);
					$unfeaturedListView->render();
				}
			?>
		</form>
		<div id="editFeaturedAlbumsControl" style="display: none;">				
			<!--input type="button" id="btnEditFeaturedAlbums" name="btnEditFeaturedAlbums" value="Edit Featured Albums" class="button_v3" onclick="collectionControl.editFeaturedAlbums(); return false;"-->
			<input type="button" id="btnSaveFeaturedAlbums" name="btnSaveFeaturedAlbums" value="Save Featured Albums" class="button_v3" onclick="collectionControl.saveManageFeaturedAlbums(); return false;">
			<input type="button" id="btnCancelEditFeaturedAlbums" name="btnCancelEditFeaturedAlbums" value="Cancel" class="button_v3" onclick="collectionControl.cancelManageFeaturedAlbums(); return false;">
		</div>
	<?	endif; ?>
</div>