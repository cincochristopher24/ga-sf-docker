<?	if( 0 < count($albums) ): ?>
	<div class="section">
		<?	if( $isFeatured ): ?>
			<?	if( 1 < count($albums) ): ?>
				<h3>These are your current featured albums:</h3>
			<?	else: ?>
				<h3>This is your current featured album:</h3>
			<?	endif; ?>
		<?	else: ?>
			<?	if( 1 < count($albums) ): ?>
				<h3>Here are the <?=count($albums)?> <?=1<count($albums)?"albums" : "album"?> that you can choose to feature:</h3>
			<?	else: ?>
				<h3>Here is the other album that you may choose to feature:</h3>
			<?	endif; ?>
		<?	endif; ?>
		<div>
			<?php	
				require_once("travellog/UIComponent/Collection/views/Class.ViewPhotoAlbumItem.php");
				$ctr = 0;
				foreach($albums AS $album): 
					$item = new ViewPhotoAlbumItem();
					$item->setAlbum($album);
					$item->setItemNumber(++$ctr);
					$item->render();
				endforeach; 
			?>
		</div>
	</div>
<?	endif; ?>