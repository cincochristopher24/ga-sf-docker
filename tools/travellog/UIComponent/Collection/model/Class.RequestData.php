<?php

	require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
	require_once("travellog/UIComponent/Collection/interface/CollectionConstants.php");
	require_once("travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php");

	class RequestData implements CollectionConstants{
		
		private static $mType		=	NULL;
		private static $mID			=	NULL;
		private static $mContext	=	NULL;
		private static $mGenID		=	NULL;
		
		private static $mUserLevel	=	self::VIEWER;
		private static $mLoggedID	=	0;
		
		private static $mTraveler	=	NULL;
		private static $mGroup		=	NULL;
		
		private static $mAction		=	self::VIEW_COLLECTION;
		private static $mLocationView	=	self::OUTSIDE_COLLECTION;
		
		private static $mGaCollectionPhotos	=	NULL;//to hold instance of GaCollectionPhotos
		private static $mGaCollectionDTO	=	array();//to hold array of GaCollectionDTO instances
		private static $mFactoryPhotos		=	NULL;//to hold either GA_TravelerCollectionPhotos or GA_GroupCollectionPhotos instance
		
		private static $mPhotoFactory	=	NULL;
		private static $mPhotoContext	=	NULL;
		
		public function setType($type=NULL){
			self::$mType = $type;
		}
		public function setID($ID=NULL){
			self::$mID = $ID;
		}
		public function setContext($context=NULL){
			self::$mContext = $context; 
		}
		public function setGenID($genID=NULL){
			self::$mGenID = $genID;
		}
		public function setUserLevel($userLevel=0){
			self::$mUserLevel = $userLevel;
		}
		public function setLoggedID($loggedID=0){
			self::$mLoggedID = $loggedID;
		}
		public function setTraveler($traveler=NULL){
			self::$mTraveler = $traveler;
		}
		public function setGroup($group=NULL){
			self::$mGroup = $group;
		}
		public function setAction($action=0){
			self::$mAction = $action;
		}
		public function setLocationView($locationView=self::OUTSIDE_COLLECTION){
			self::$mLocationView = $locationView;
		}
		public function getType(){
			return self::$mType;
		}
		public function getID(){
			return self::$mID;
		}
		public function getContext(){
			return self::$mContext;
		}
		public function getGenID(){
			return self::$mGenID;
		}
		public function getUserLevel(){
			return self::$mUserLevel;
		}
		public function getLoggedID(){
			return self::$mLoggedID;
		}
		public function getTraveler(){
			return self::$mTraveler;
		}
		public function getGroup(){
			return self::$mGroup;
		}
		public function getAction(){
			return self::$mAction;
		}
		public function getLocationView(){
			return self::$mLocationView;
		}
		
		public function setGaCollectionPhotos($gaCollectionPhotos=NULL){
			self::$mGaCollectionPhotos = $gaCollectionPhotos;
		}
		public function setGaCollectionDTO($gaCollectionDTO=array()){
			self::$mGaCollectionDTO = $gaCollectionDTO;
		}
		public function setFactoryPhotos($factoryPhotos=NULL){
			self::$mFactoryPhotos = $factoryPhotos;
		}
		public function getGaCollectionPhotos(){
			return self::$mGaCollectionPhotos;
		}
		public function getGaCollectionDTO(){
			return self::$mGaCollectionDTO;
		}
		public function getFactoryPhotos(){
			return self::$mFactoryPhotos;
		}
		public function getCollection($genID){
			foreach(self::$mGaCollectionDTO as $collection){
				if( $collection->getGenID() == $genID ){
					return $collection;
				}
			}
			return FALSE;
		}
		public function getTopCollection(){
			if( 0< count(self::$mGaCollectionDTO) ){
				return self::$mGaCollectionDTO[0];
			}
			return FALSE;
		}
		public function getPhotoContext(){
			if( is_null(self::$mPhotoContext) ){
				self::$mPhotoContext = new PhotoFactory(self::$mGenID,self::$mContext);
			}
			return PhotoFactory::create();
		}
		public function getPhotoFactory(){
			if( is_null(self::$mPhotoFactory) ){
				require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
				self::$mPhotoFactory = new PhotoFactory(self::$mGenID,self::$mContext);
			}
			return self::$mPhotoFactory;
		}
		
		public static function isPrivileged(){
			return (self::VIEWER < self::$mUserLevel)? TRUE : FALSE;
		}
		
		function generateCacheKey(){
			if( self::isPrivileged() ){
				return $this->getType() == self::TRAVELER_TYPE ? "Photo_Collection_Traveler_Owner_".$this->getID() : "Photo_Collection_Group_Admin_".$this->getID();
			}else{
				return $this->getType() == self::TRAVELER_TYPE ? "Photo_Collection_Traveler_Public_".$this->getID() : "Photo_Collection_Group_Public_".$this->getID();
			}
		}
	}