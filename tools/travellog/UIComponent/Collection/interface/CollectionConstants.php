<?php

	interface CollectionConstants {

		//type constants
		const TRAVELER_TYPE		=	"traveler";
		const GROUP_TYPE		=	"group";
		
		//context constants
		const PROFILE_CONTEXT	=	"profile";
		const ALBUM_CONTEXT		=	"photoalbum";
		const TRAVELLOG_CONTEXT	=	"travellog";
		const JOURNAL_CONTEXT	=	"journal";
		const TRAVALBUM_CONTEXT	=	"traveleralbum";
		const ARTICLE_CONTEXT	=   "article";
		
		const CUSTOMHEADER_CONTEXT	= "custompageheader";
		
		//user levels
		const VIEWER			=	0;
		const OWNER				=	1;
		const STAFF				=	2;
		const SUPERSTAFF		=	3;
		const ADMIN				=	4;
		
		//action constants
		const VIEW_COLLECTION		=	0;
		const MANAGE_COLLECTION		=	1;
		const CREATE_ALBUM			=	2;
		const LOAD_UPLOADER			=	3;
		const UPLOAD				=	4;
		const EDIT_TITLE			=	5;
		const VIEW_MEMBERS			=	6;
		const FETCH_MEMBERS			=	7;
		const MANAGE_PHOTOS			=	8;
		const VIEW_MEMBER_PHOTOS	=	9;
		const SAVE_CHANGES			=	10;
		const GRAB_PHOTO			=	11;
		const DELETE_PHOTO			=	12;
		const REMOVE_GRABBED		=	13;
		const REMOVE_MEMBER_PHOTO	=	14;
		const EDIT_CAPTION			=	15;
		const ROTATE				=	16;
		const SET_PRIMARY			=	17;
		const SET_JOURNAL_PRIMARY	=	18;
		const LOAD_CROPPER			=	19;
		const SAVE_THUMBNAIL		=	20;
		const DELETE_ALBUM			=	21;
		const LOAD_POPUP_GALLERY	=	22;
		const LOAD_REARRANGE_VIEW	=	23;
		const SAVE_PHOTO_ORDER		=	24;
		const BATCH_DELETE			=	25;
		const GET_UPLOAD_ERRORS		=	26;
		const EDIT_FEATURED_ALBUMS	=	27;
		const SAVE_FEATURED_ALBUMS	=	28;
		const UPDATE_MANAGE_FEATURED_ALBUMS_NOTICE = 29;
		const FEATURE_ALBUM			=	30;
		const LOAD_RECENT_UPLOADS	=	31;
		const FIXGROUPLOGOTHUMBNAIL =	101;
		const CREATE_COBRAND_HEADER	=	100;
		
		//view location relative to collection
		const OUTSIDE_COLLECTION	=	0;
		const INSIDE_COLLECTION		=	1;
		
		//upload modes
		const CLASSIC	=	0;
		const AURIGMA	=	1;
		
		//photo formats
		const JPEG	=	"jpeg";
		const GIF	=	"gif";
		const PNG	=	"png";
		
		//phototypeID constants
		const PROFILE   	= 1;
 		const TRAVELLOG  	= 2;
		const PHOTOALBUM	= 7;
		const ARTICLE		= 11;
		const CUSTOMHEADER	= 12;
		
		//photosizes
		const FULLSIZE			=	751;
		const STANDARD			=	164;
		const STANDARD_PROFILE	=	120;
		const THUMBNAIL			=	65;
		const FEATURED			=	164;
		const JENTRY_HEIGHT		=	385;
		const JENTRY_HEIGHT2	=	493;
		const JENTRY_WIDTH		=	751;
		const ORIGINAL_SIZE		=	1024;
		
		//const rotating constants
		const ROTATE_LEFT		=	270;
		const ROTATE_RIGHT		=	90;
		
		//custom header
		const HEADER_MIN_WIDTH = 980;
		const HEADER_MIN_HEIGHT = 324;
	}