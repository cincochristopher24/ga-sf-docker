<?php
/*
 * Created on Oct 4, 2007
 * Class.ResourceFileFactory.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 require_once("travellog/UIComponent/Resourcefiles/interface/IResourceFileFactory.php");
 
 class ResourceFileFactory implements IResourceFileFactory{
 	
 	static $id 		= NULL;
 	static $context 	= NULL;
 	
 	function ResourceFileFactory($id,$context){
 		self::$id 		= $id;
 		self::$context 	= $context;
 		return $this;
 	}
 	
 	static function ResourceFileFactoryPeer($id,$context){
 		self::$id 		= $id;
 		self::$context 	= $context;
 		return new ResourceFileFactory($id,$context);
 	}
 	
 	function getContext(){
 		return self::$context;
 	}
 	
 	public static function create(){
 		
 		require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFile".ucfirst(self::$context).".php");
 		$res = new ReflectionClass("ResourceFile".ucfirst(self::$context));
 		return $res->newInstance(self::$id);
 	}
 	
 }
?>
