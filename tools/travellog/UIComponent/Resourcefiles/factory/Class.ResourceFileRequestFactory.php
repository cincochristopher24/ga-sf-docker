<?php


class ResourceFileRequestFactory{
	
	private $file_factory = NULL;
	
	function __construct($REQUEST_VAR =array()){
		$this->file_factory = FileFactory::getInstance();
		$this->create($REQUEST_VAR);
	}
	
	function create($REQUEST_VAR =array()){
		require_once("travellog/UIComponent/Resourcefiles/request/Class.ResourceFileRequest".strtoupper($REQUEST_VAR['action']).".php");
	 	$ResFile = new ReflectionClass("ResourceFileRequest".strtoupper($REQUEST_VAR['action']));
	 	return $ResFile->newInstance($this->file_factory,$REQUEST_VAR);
		
	}
		
		
		
}

?>