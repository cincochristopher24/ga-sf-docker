<?php
/*
 * Created on Nov 16, 2007
 * IResourceFileStrategy.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
interface IResourceFileStrategy{
	
	/**
	 * @param array of Resourecefiles
	 */
	function filter($resourefiles);
}
 
?>
