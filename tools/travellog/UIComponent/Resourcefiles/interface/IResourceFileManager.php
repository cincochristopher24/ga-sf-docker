<?php
/**  
 * IResourceFileManager.php  2007-08-15 02:43:56Z root $
 **/

/**
 * IResourceFileManager.
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Resourcefile
 **/
 
interface IResourceFileManager{
	
	/**
	 * Save/Upload ResourceFile
	 */
	function save(UploadManager $UPM);
	
	/**
	 * Change ResourceFile
	 * @param UploadManager
	 * @return 
	 */
	function change(UploadManager $UPM);
	
	/**
	 * Delete ResourceFile
	 * @param ResourceFileFactory. photoID
	 * @return 
	 */
	function delete(ResourceFileFactory $Rfac, $resourcefileID);
	
	/**
	 * Add Caption  
	 * @param ResourceFileFactory, photoid
	 * @return 
	 */
	function updatecaption(ResourceFileFactory $Rfac, $resourcefileID, $caption);
	
	function setPrivacyPreference(ResourceFileFactory $Rfac, $resourcefileID, $pripref);

} 
?>
