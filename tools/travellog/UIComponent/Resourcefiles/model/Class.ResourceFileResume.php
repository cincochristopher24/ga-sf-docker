<?php
/*
 * Created on Oct 4, 2007
 * Class.ResourceFileAdminGroup.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Resourcefiles/abstract/AbstractResourceFile.php");

class ResourceFileResume extends AbstractResourceFile{
	
	private 	$loginID	= 0;
	private 	$TRAVELERID	 	= 0;
	private 	$RESOURCEFILEcontext	= NULL;
	public 		$NUMfileTOUPLOAD 		= 1;
	static 	$resourcefiles = array();
	
	function __construct($id){
		self::setRESOURCEFILEcontext($id);
		$this->TRAVELERID = $id;
		return $this;
	}
	
	function setLoginID($loginID){
		$this->loginID = $loginID;
	}
	
	function setRESOURCEFILEcontext($id){
		require_once('travellog/model/Class.Resume.php');
		
		try{
		  	$Resume		= new Resume($id);
			self::$resourcefiles	= $Resume->getResourceFiles();
			$this->RESOURCEFILEcontext =$Resume;
		}
		catch(Exception $e){
		}
	}
	
	function getRESOURCEFILEcontext(){
		return $this->RESOURCEFILEcontext;	
	}
	
	function getResourceFiles(){
		return self::$resourcefiles;
	}
	
	function getCOUNTResourceFiles(){
		return count($this->getResourceFiles());	
	}
	
	function isOwner(){
		if($this->TRAVELERID == $this->loginID)
			return true;
		else
			return false;
	}
		
	function getHeaderCaption(){
		require_once('travellog/model/Class.TravelerProfile.php');
		$profile	= new TravelerProfile($this->TRAVELERID);									
		return $profile->getUserName()."'s  uploaded Resume";
	}
	
	function getBackLink(){
		return "/resume.php?action=view";
	} 
	
	function getBackCaption(){
		require_once('travellog/model/Class.TravelerProfile.php');
		$profile	= new TravelerProfile($this->TRAVELERID);	
		return $profile->getUserName()."'s  "."Resume";
	} 
	function getNorecordlabel(){
		if(!$this->getCOUNTResourceFiles())
			return "There are no uploaded resume yet.";	
		
	} 
	
	function getSUBNavigation(SubNavigation $SUbnav){
		if($this->isOwner())			
			$SUbnav->setContextID($this->loginID);
			$SUbnav->setContext('TRAVELER');
			$SUbnav->setLinkToHighlight('RESUME');
		return $SUbnav;
	}
	
} 
 
?>
