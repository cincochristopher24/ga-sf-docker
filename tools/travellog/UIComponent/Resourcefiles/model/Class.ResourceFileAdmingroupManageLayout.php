<?php
/*
 * Created on Mar 11, 2008
 * ResourceFileAdmingroupManageLayout.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 class ResourceFileAdmingroupManageLayout{
	
	function ResourceFileAdmingroupManageLayout(ResourceFileAdmingroup $RFileAdmingroup, $REQUEST_VAR = array()){
		
		require_once('Class.Template.php');	
		require_once('travellog/model/nPaging.php');		
		
		$template = new Template();
		$template->set_path('travellog/UIComponent/Resourcefiles/views/');
		
		
		$startrow 	= isset($REQUEST_VAR['startrow'])?$REQUEST_VAR['startrow']:1;
        $pagenum 	= isset($REQUEST_VAR['pagenum'])?$REQUEST_VAR['pagenum']:1;
        $view       = isset($REQUEST_VAR['view'])?$REQUEST_VAR['view']:null;
		
		$RFileAdmingroup->setView($view);
		
		$reccount 		= count(isset($REQUEST_VAR['filter'])?$RFileAdmingroup->getAllFiles():$RFileAdmingroup->getResourceFiles());						  			
		$resourcefiles	= isset($REQUEST_VAR['filter'])?$RFileAdmingroup->getAllFiles():$RFileAdmingroup->getResourceFiles();
		
		//$reccount 		= count($RFileAdmingroup->getAllFiles());						  			
		//$resourcefiles	= $RFileAdmingroup->getAllFiles();
		
		$owner			= $RFileAdmingroup->isOwner();	 
	    
		nPaging::Setajax(true);															
		$page = new nPaging($reccount,$startrow,$pagenum,10);								
								
		$start	= $startrow - 1;
		$endrow	= ($startrow + 10) -1;
		($endrow > $reccount)? $endrow = $reccount: $endrow;
		
		
		$template->set_vars(array(
							'owner'			=> $RFileAdmingroup->isOwner(),
							'uploadrfiles'	=> "resourcefiles.php?action=upload&cat=admingroup&genID=".$RFileAdmingroup->getGroupID(),
							'edit_resourcefilelink'		=> "resourcefiles.php?action=edit&cat=".$REQUEST_VAR['cat']."&genID=".$REQUEST_VAR['genID']."&rfileID=",
							'delete_resourcefilelink'	=> "resourcefiles.php?cat=".$REQUEST_VAR['cat']."&action=delete&genID=".$REQUEST_VAR['genID']."&rfileID=",
							'subgroups'		=> $RFileAdmingroup->getRESOURCEFILEcontext()->getSubgroups(),
							'backlink'		=> $RFileAdmingroup->getBackLink(),
							'backcaption'	=> $RFileAdmingroup->getBackCaption(),
							'group'			=> $RFileAdmingroup->getRESOURCEFILEcontext()->getName(),
							'start' 		=> $start,
							'resourcefiles'	=> $resourcefiles,
							'rmode'			=> $REQUEST_VAR['rmode'],
							'endrow'		=> $endrow,
							'reccount'		=> $reccount,
							'recordno' 		=> $startrow." - ".$endrow. " of ".$reccount,
							'norecfound'	=> $RFileAdmingroup->getNorecordlabel(),
							'page'			=> $page,
							'genID'         => $REQUEST_VAR['genID']
						
							));																																										
		
		echo $template->out('tpl.admingroupmanage.php');	
	
	}
	
} 
 
?>