<?php
/*
 * Created on Oct 3, 2007
 * Class.ParsePERLUploadParams.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

class ParsePERLUploadParams{
		
	
	static 	$files 			= array();
	static 	$options		= array();
	static		$grpIDs			= array();
		
	static		$ismembersnotify		= false;
		
	function ParsePERLUploadParams($param){
		$resultparam = $_SERVER['DOCUMENT_ROOT'].'/users/'.$param.'.params';
		if(file_exists($resultparam)){				
			$params 		= parse_ini_file($resultparam);

			foreach($params as $p => $prs){
				if(strpos($p,'upfile') !== false){
					//array_push(self::$files,$prs);
					self::$files = $prs;//upfile is already an array of filenames, for batch upload
				}
				if(strpos($p,'grpid_') !== false){
					array_push(self::$grpIDs,$prs);
				}
				
				if(strpos($p,'ismembersnotify') !== false){
					self::$ismembersnotify = true;
				}
				
			}
			
			//check if Resourcefile Privacy Preference then get the value
			if(array_key_exists('txtfileprvpref',$params)){
				self::$options['resourcefileprvpref'] = $params['txtfileprvpref'];
			}
			
			if(array_key_exists('txtcaption', $params)){
				self::$options['caption'] = $params['txtcaption'];
			}
			
			if( array_key_exists('txtCustomMessage',$params) ){
				self::$options['customMessage'] = $params['txtCustomMessage'];
			}
			
		}	
		
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/users/'.$param.'.params')){
			unlink($_SERVER['DOCUMENT_ROOT'].'/users/'.$param.'.params');
		}
	}
	
	public static function getFiles(){
		return self::$files;
	}
	
	public static function getOptions(){
		return self::$options;
	}
		
	// Resource File Privacy Preference
	public static function getPrivacyPreference(){
		
		if(array_key_exists('resourcefileprvpref',self::$options))
			return self::$options['resourcefileprvpref'];
		else
			return 0;
	}
	
	// Resource File Assign file to group
	public static function getAssignGroup(){
		return array_unique(self::$grpIDs);
	}		
	
	// Notified members
	public static function isMembersNotify(){
		return self::$ismembersnotify;
	}
	
	// Caption
	public static function getCaption(){
		if(array_key_exists('caption',self::$options))
			return self::$options['caption'];
		else
			return '';
	}
	
	//Custom message
	public static function getCustomMessage(){
		if(array_key_exists('customMessage',self::$options))
			return self::$options['customMessage'];
		else
			return '';
	}
		
}

?>
