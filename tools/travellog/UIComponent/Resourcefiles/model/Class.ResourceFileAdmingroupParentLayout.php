<?php
/*
 * Created on Mar 6, 2008
 * Class.ResourceFileAdmingroupParentLayout.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
class ResourceFileAdmingroupParentLayout{
	
	function ResourceFileAdmingroupParentLayout(ResourceFileAdmingroup $RFileAdmingroup, $REQUEST_VAR = array()){
		
		require_once('Class.Template.php');	
		require_once('travellog/model/nPaging.php');		
		
		$template = new Template();
		$template->set_path('travellog/UIComponent/Resourcefiles/views/');
		
		
		$startrow 	= isset($REQUEST_VAR['startrow'])?$REQUEST_VAR['startrow']:1;
        $pagenum 	= isset($REQUEST_VAR['pagenum'])?$REQUEST_VAR['pagenum']:1;
		$view       = isset($REQUEST_VAR['view'])?$REQUEST_VAR['view']:null;
		
		$RFileAdmingroup->setView($view);
		
		// commented: to show only files of group
		$reccount 		= count($RFileAdmingroup->getAllFiles());						  			
		$resourcefiles	= $RFileAdmingroup->getAllFiles();
		
		//$reccount 		= count($RFileAdmingroup->getResourceFiles());						  			
		//$resourcefiles	= $RFileAdmingroup->getResourceFiles();
		
		
		//echo $reccount;
		$owner			= $RFileAdmingroup->isOwner();	 
	    
		nPaging::Setajax(true);															
		$page = new nPaging($reccount,$startrow,$pagenum,10);								
								
		$start	= $startrow - 1;
		$endrow	= ($startrow + 10) -1;
		($endrow > $reccount)? $endrow = $reccount: $endrow;
		
		$template->set_vars(array(
							'owner'			=> $RFileAdmingroup->isOwner(),
							'uploadrfiles'	=> "resourcefiles.php?action=upload&amp;cat=admingroup&amp;genID=".$RFileAdmingroup->getGroupID(),
							'edit_resourcefilelink'		=> "resourcefiles.php?action=edit&amp;cat=".$REQUEST_VAR['cat']."&amp;genID=".$REQUEST_VAR['genID']."&amp;rfileID=",
							'delete_resourcefilelink'	=> "resourcefiles.php?cat=".$REQUEST_VAR['cat']."&amp;action=delete&amp;genID=".$REQUEST_VAR['genID']."&amp;rfileID=",
							'subgroups'		=> $RFileAdmingroup->getRESOURCEFILEcontext()->getSubgroups(),
							'backlink'		=> $RFileAdmingroup->getBackLink(),
							'backcaption'	=> $RFileAdmingroup->getBackCaption(),
							'group'			=> $RFileAdmingroup->getRESOURCEFILEcontext(),
							'start' 		=> $start,
							'resourcefiles'	=> $resourcefiles,
							'endrow'		=> $endrow,
							'showsubgroup_select' => $REQUEST_VAR['showsubgroup_select'],
							'reccount'		=> $reccount,
							'recordno' 		=> $startrow." - ".$endrow. " of ".$reccount,
							'norecfound'	=> $RFileAdmingroup->getNorecordlabel(),
							'page'			=> $page,
							'genID'         => $REQUEST_VAR['genID']
						
							));																																										
		
		
		echo $template->out('tpl.admingroupparent.php');	
			
		//echo $template->fetch('tpl.IncViewResourceFiles.php');
			
				
	
	}
	
} 
 
?>
