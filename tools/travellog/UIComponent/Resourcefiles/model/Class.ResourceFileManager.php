<?php
/*
 * Created on Sep 28, 2007
 *
 * 
 */
 
 
 
 require_once("travellog/UIComponent/Resourcefiles/interface/IResourceFileManager.php");
 require_once("travellog/model/Class.ResourceFileBatchUpload.php");
 
 class ResourceFileManager implements IResourceFileManager{
 	
 	private $files				= NULL;
 	
 	private $commands 			= array();	
	
	private $context			= NULL;
	private $loginID			= 0;			//userID
	private $genID				= 0;			//ID Based on Context
 	private $resourcefileid	= 0;
 	
 	private $caption            = NULL;
 	private $resourcefiletype	= NULL;
 	private $validfiles		= array();
	private $invalidfiles		= array();
	
	private $privacypref		= 2; //default: show to all
	private $uploadedBy         = NULL;
	private $AssigngrpIDS		= array();
	private $isMembersNotify	= false;
	
	private $customMessage		= "";
	private $isBatchUpload		= FALSE;
	
	function setFiles($files){
		$this->files = $files;
	}
	
	function getFiles(){
		return $this->files;
	}
	
	function setContext($context){
		$this->context = $context;
	}
	
	function getContext(){
		return $this->context;
	}
	
	function setCaption($caption){
		$this->caption = $caption;
	}
	
	function getCaption(){
		return $this->caption;
	}
	
	function setLoginID($loginID){
		$this->loginID = $loginID;
	}
	
	function getLoginID(){
		return $this->loginID;
	}
	
	function setGenID($genID){
		$this->genID = $genID;
	}
	
	function getGenID(){
		return $this->genID;
	}
	
	function setResourceFileType($resourcefiletype){
		$this->resourcefiletype = $resourcefiletype;
	}
	
	function setResourceFileID($resourcefileid){
		$this->resourcefileid = $resourcefileid;
	}	
	
	function setPrivacypref($privacypref){
		$this->privacypref = $privacypref;
	}
	
	function getPrivacypref(){
		return $this->privacypref;
	}
	
	function setUploadedBy($uploadedBy){
		$this->uploadedBy = $uploadedBy;
	}
	
	function getUploadedBy(){
		return $this->uploadedBy;
	}
	
	function setCustomMessage($message){
		$this->customMessage = $message;
	}
	
	function getCustomMessage(){
		return $this->customMessage; 
	}
	
	function setIsBatchUpload($batch=TRUE){
		$this->isBatchUpload = $batch;
	}
	
	function setAssigngrpIDS($AssigngrpIDS){
		$this->AssigngrpIDS = $AssigngrpIDS;
	}
		
	function getAssigngrpIDS(){
		return $this->AssigngrpIDS;
	}
	
	function setIsMembersNotify($isMembersNotify){
		$this->isMembersNotify = $isMembersNotify;
	}
		
	function IsMembersNotify(){
		return $this->isMembersNotify;
	}
	
	
	
	
	/**
	 * Save Uploaded Resource File
	 */
	function save(UploadManager $UPM){
		
		require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
		require_once("travellog/model/Class.ResourceFiles.php");
		
		$this->context = ResourceFileFactory::ResourceFileFactoryPeer($this->genID,$UPM->getContext())->create()->getRESOURCEFILEcontext();
		
		//for sending to cloudfront
		$rawFiles = array();
		
		if($UPM->getValid()){
			$batchUpload = NULL;
			$resource_files = array();
			if( $this->isBatchUpload ){
				$batchUpload = new ResourceFileBatchUpload();
				$batchUpload->setCustomMessage($this->getCustomMessage());
				$batchUpload->setNotifyMembers($this->IsMembersNotify()? 1 : 0);
				$batchUpload->Save();
			}
			foreach($UPM->getValid() as $ufile){							
				$rfile	= new ResourceFiles();
				$rfile->setContext($this->context);
				$rfile->setResourceFileType($this->context->resourcefiletype);						
				$rfile->setTheType('PDF');
				$rfile->setFileName($ufile->getName());
				if( $this->isBatchUpload ){
					$rfile->setCaption($ufile->getDocumentTitle());
					$rfile->setBatchUploadID($batchUpload->getID());	
				}else{
					$rfile->setCaption($this->caption);									
				}
				$rfile->setPrivacypreference($this->privacypref);
				$rfile->setUploadedBy($this->uploadedBy);
				$rfile->Create();
				
				$rawFiles[] = $rfile->getFileName();
				
				if( $this->isBatchUpload ){
					$resource_files[] = $rfile;
				}								
				
				//set commands
				foreach($this->commands as $cmd ){
					if( $this->isBatchUpload ){
						if( $cmd instanceof ResourceFileAssignGroupCommand ){
							$cmd->onCommand($this,array('resourcefileID'=>$rfile->getResourceFileID()));
						}
					}else{
						if ($cmd->onCommand($this,array('resourcefileID'=>$rfile->getResourceFileID())));
					}
				}
			}
			if( $this->isBatchUpload ){
				foreach($this->commands as $cmd ){
					if( $cmd instanceof ResourceFileInstantNotifyMembersCommand ){
						$cmd->onCommand($this,array('resource_files'=>$resource_files,'batchUpload'=>$batchUpload,'groupIDs'=>$this->getAssigngrpIDS()));
					}
				}
			}
		}
		
		$this->validfiles 	= $UPM->getValid();
		$this->invalidfiles = $UPM->getInValid();
		
		//send to cloudfront
		if( count($rawFiles) ){
			$filePath = $UPM->getDestination()->GetPathrelative();
			$ganetAWS = GANETAmazonS3WebService::getInstance();
			$ganetAWS->pushRawFilestoS3($rawFiles,$filePath);
		}
	}
	
	/**
	 * Set new command to execute after adding resource Files
	 * @param array of commands
	 */
	function addCommand( $cmd ){
	   $this->commands[]= $cmd;
	}
	
	
	/**
	 * Change Photo
	 * @param photoid
	 * @return 
	 */
	function change(UploadManager $UPM){
		
		require_once("travellog/model/Class.ResourceFiles.php");
		$this->context = $UPM->getContext();
		if($UPM->getValid()){																									
			foreach($UPM->getValid() as $ufile){							
				$resourcefile = new ResourceFiles($this->resourcefileid);
				$resourcefile->setContext($this->context);
				
				$UPM->deletefile($resourcefile->getFileName());								
				
				$resourcefile->setFileName($ufile->getName());
				$resourcefile->setCaption($ufile->getCaption());
				$resourcefile->Update();	
			}
		}
			
		$this->validfiles 	= $UPM->getValid();
		$this->invalidfiles = $UPM->getInValid();		
	}
	
	
	function setPrivacyPreference(ResourceFileFactory $Rfac, $resourcefileID, $pripref){
		require_once('travellog/model/Class.ResourceFiles.php');
		$resourcefile 	= new ResourceFiles($resourcefileID);
		$resourcefile->setPrivacypreference($pripref);		
		$resourcefile->Update();		
	}
	

	
	/**
	 * Delete Photo
	 * @param PhotoFactory, photoID
	 * @return 
	 */
	function delete(ResourceFileFactory $Rfac, $resourcefileID){
		require_once('travellog/model/Class.ResourceFiles.php');
		$ResourceFiles 	= new ResourceFiles($resourcefileID);
		$ResourceFiles->setContext($Rfac->create()->getRESOURCEFILEcontext());
		$ResourceFiles->Delete();	
	}
	
	/**
	 * Add Caption of Photo
	 * @param photoid
	 * @return 
	 */
	function updatecaption(ResourceFileFactory $Rfac, $resourcefileID, $caption){
		require_once('travellog/model/Class.ResourceFiles.php');
		$resourcefile 	= new ResourceFiles($resourcefileID);
		$resourcefile-> setCaption($caption);		
		$resourcefile->Update();
	}
	
	function getValidFiles(){
		return $this->validfiles;
	}
	
	function getInValidFiles(){
		return $this->invalidfiles;
	}
	
 	
 }
 
 
?>
