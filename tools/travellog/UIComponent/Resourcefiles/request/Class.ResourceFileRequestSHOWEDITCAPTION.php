<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/ResourcefileService.php');

class ResourceFileRequestSHOWEDITCAPTION{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		
		$template 		= $FFactory->getClass('Template');
		$template->set_path("travellog/UIComponent/Resourcefiles/views/");
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		require_once("travellog/model/Class.ResourceFiles.php");
		$ResourceFile = new ResourceFiles($REQUEST_VAR['resourcefileID']);
   	 	$template->set_vars(array(
						'error'				=>NULL,							
						'resourcefileID'	=>$REQUEST_VAR['resourcefileID'],							
						'caption'			=>$ResourceFile->getCaption()));
		
		echo $template->fetch('tpl.EditResourceFileCaption.php');	
			
	}
	
}


?>
