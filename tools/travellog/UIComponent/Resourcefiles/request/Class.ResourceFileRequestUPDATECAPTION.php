<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


class ResourceFileRequestUPDATECAPTION{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
	   	$Rfactory = new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
	    
	   
	    
	    require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileManager.php");
	    ResourceFileManager::updatecaption($Rfactory,$REQUEST_VAR['resourcefileID'],$REQUEST_VAR['caption']);
		
		$RFile = ResourceFileFactory::create();
	    $RFile->setLoginID($travelerID);
		
	    require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileLayoutFactory.php");
	    ResourceFileLayoutFactory::create($RFile, $REQUEST_VAR);
		
		
		
		/*
		$REQUEST_VAR['travelerID'] = $travelerID;
		
		$PhotoService = new ResourcefileService();
		$PhotoService->setTemplateViews($template->getPath());
		$PhotoService->Update();
		*/
	}
	
}


?>
