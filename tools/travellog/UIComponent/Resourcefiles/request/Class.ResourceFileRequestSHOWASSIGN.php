<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


class ResourceFileRequestSHOWASSIGN{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		
		$template 		= $FFactory->getClass('Template');
		$template->set_path("travellog/UIComponent/Resourcefiles/views/");
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		require_once("travellog/model/Class.ResourceFiles.php");
		$ResourceFile = new ResourceFiles($REQUEST_VAR['resourcefileID']);
   	 	
   	 	require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
		new ResourceFileFactory($REQUEST_VAR['gID'],$REQUEST_VAR['cat']);
	    $RFile = ResourceFileFactory::create();
		
		/*check if subgroup, 
		* if subgroup. get parent, get subgroups of parent
		* else get subgroups  			
		*/
		$groups = array();
		
		if($RFile->getRESOURCEFILEcontext()->isSubGroup()){
			new ResourceFileFactory($RFile->getRESOURCEFILEcontext()->getParent()->getGroupID(),$REQUEST_VAR['cat']);
	    	$adRFile = ResourceFileFactory::create();
	    	$groups = array_merge(array($RFile->getRESOURCEFILEcontext()->getParent()), $adRFile->getRESOURCEFILEcontext()->getSubGroups());
	    }else{
			$groups = array_merge(array($RFile->getRESOURCEFILEcontext()),$RFile->getRESOURCEFILEcontext()->getSubGroups());
		}
   	 	
   	 	$assignGIDS = array();
   	 	foreach($ResourceFile->getAssignedGroups() as $group){
   	 		$assignGIDS[$group->getGroupID()] = $group;
   	 	}
   	 	$template->set_vars(array(
						'groups'			=>$groups,
						'assignGIDS'		=>$assignGIDS,
						'resourcefileid' 	=>$REQUEST_VAR['resourcefileID'],
						'REQUEST_VAR'		=>$REQUEST_VAR					
						
						));
		
		echo $template->fetch('tpl.showassign.php');	
			
	}
	
}


?>
