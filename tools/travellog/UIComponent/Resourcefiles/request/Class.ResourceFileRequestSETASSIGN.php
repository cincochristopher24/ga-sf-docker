<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


class ResourceFileRequestSETASSIGN{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
			
		if(isset($REQUEST_VAR['grpid'])){
			
			$resourceFileID  = $REQUEST_VAR['rfileid'];
			
			require_once("Class.Connection.php");
			require_once("Class.Recordset.php");
			
			$Conn = new Connection();
            $Rs   = new Recordset($Conn);
			
			//Delete data from tblGrouptoResourceFile
			$Rs->Execute("DELETE FROM tblGrouptoResourceFile WHERE resourcefileID = '$resourceFileID'");
			
			//Insert New Selected group 	
			foreach($REQUEST_VAR['grpid'] as $gID){
				$Rs->Execute("INSERT into tblGrouptoResourceFile (groupID, resourcefileID)VALUES ('$gID', '$resourceFileID')");
            }
						
			require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
		   	new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
		    
		    $RFile = ResourceFileFactory::create();
		    $RFile->setLoginID($travelerID);
			
		    require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileLayoutFactory.php");
		    ResourceFileLayoutFactory::create($RFile, $REQUEST_VAR);
		
			
				
		}else{
			echo "notset";
		}	
	}
	
}
?>