<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/ResourcefileService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");

class ResourceFileRequestSETPRIVACYPREFERENCE{
	
	function __construct(FileFactory $FFactory, $REQUEST_VAR =array()){
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileFactory.php");
	   	$Rfactory = new ResourceFileFactory($REQUEST_VAR['genID'],$REQUEST_VAR['cat']);
	    
	    require_once("travellog/UIComponent/Resourcefiles/model/Class.ResourceFileManager.php");
	    
	    $preference = explode('&',urldecode($REQUEST_VAR['privacypreference']));
		ResourceFileManager::setPrivacyPreference($Rfactory,$REQUEST_VAR['resourcefileID'],substr($preference[0],strpos($preference[0],'=')+1)); 
	    
	    $RFile = ResourceFileFactory::create();
	    $RFile->setLoginID($travelerID);
		
	    require_once("travellog/UIComponent/Resourcefiles/factory/Class.ResourceFileLayoutFactory.php");
	    ResourceFileLayoutFactory::create($RFile, $REQUEST_VAR);
	}
	
}


?>
