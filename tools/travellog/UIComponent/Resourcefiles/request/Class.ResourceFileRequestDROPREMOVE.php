<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/model/Class.ResourceFiles.php');
require_once('travellog/model/Class.AdminGroup.php');		


class ResourceFileRequestDROPREMOVE{
	
	function __construct(FileFactory $FFactory){
		
		$template 	 = $FFactory->getClass('Template');
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$_REQUEST['travelerID'] = $travelerID;
		
		$subgroup 		= new AdminGroup($_REQUEST['subgID']);
		    		
		$template = new Template();
	    $template->set_path("travellog/views/");
		
		if($_REQUEST['chkrid']){
				/**#################################
				 * split string  into array and replace g= to ''
				 * ex:g=140&g=141&g=142 
				 * equal to Array ( [0] => 140 [1] => 141 [2] => 142 )
				 *#################################*/
				$rid = explode("&",str_replace('g=','',$_REQUEST['chkrid']));	
				
				foreach($rid as $rfID){
					$resourceFiles 	= new ResourceFiles($rfID);
					$resourceFiles->setContext($subgroup);
					$resourceFiles->Delete();	
				}
				
		}
		
		
		
		$template->set('subgroup',$subgroup);
		$template->set('reload',true);
	 	echo $template->out('tpl.incViewManageResourceFilesSubgroups.php');	
	}
	
}


?>
