<?php
/*
 * Created on Mar 6, 2008
 * AbstractAdminGroupLayout.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 abstract class AdminGroupLayout{
 	
 	abstract function render();
 	
 }
 
?>
