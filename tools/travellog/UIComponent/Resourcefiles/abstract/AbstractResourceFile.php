<?php
/*
 * Created on Oct 4, 2007
 * AbstractResourceFile.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
abstract class AbstractResourceFile{
	
	abstract function setRESOURCEFILEcontext($id);
	abstract function getResourceFiles();
	
} 
 
?>
