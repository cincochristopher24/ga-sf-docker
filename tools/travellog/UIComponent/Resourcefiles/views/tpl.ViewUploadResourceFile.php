<?php
	ob_start();
	$tmp_sid = md5(uniqid(mt_rand(), true));
	
	Template::setMainTemplate($layoutMain);
	
	Template::includeDependentJs("/min/f=js/prototype.js", array("top" => true));
	Template::includeDependentCss("/min/f=css/resourcefiles.css");
	Template::includeDependentJs("/js/resourcefile.js"); 
		
	if($group){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else{
			if(isset($isAdminLogged) && true == $isAdminLogged)
				Template::setMainTemplateVar('page_location', 'My Passport');
			else
				Template::setMainTemplateVar('page_location', 'Groups / Clubs');
		}
		//Template::setMainTemplateVar('page_location', $group);
		Template::setMainTemplateVar('group', $group);
	}
	$groupName = addslashes($group);
	$loginID = $traveler->getTravelerID();
	$code =<<<BOF
	<script type = "text/javascript">
	jQuery.noConflict();
	
	ResourceFiles = new ResourceFile($genID,"$cat",$loginID,"$groupName");
		
		jQuery("#resourcefiles").ready(function(){
			ResourceFiles.addGroup();
			ResourceFiles.addGroupCounter();
			ResourceFiles.removeGroup();
		});
		
		function customMessageChanged(txt){
			var captionLength = txt.value.length;
			if( 500 < captionLength ){
				txt.value = txt.value.substring(0,500);
				captionLength = 0;
			}else{
				captionLength = 500 - captionLength;
			}
			var label = 1 < captionLength ? captionLength + " chars left" : captionLength + " char left";
			jQuery("#remaining_chars").html(label);
		}
		
		var toggleCustomMessage = function(dom){
			if( jQuery(dom).is(":checked") ){
				jQuery("#txtCustomMessage").attr("readonly",false);
			}else{
				jQuery("#txtCustomMessage").attr("readonly",true);
			}
		}
		
		var fileInputCount = 1;
		var fileInputCounterID = 1;
		function addFileInput(){
			var str =	'<tr id="inputFile_'+fileInputCounterID+'">'+
							'<td>'+
								'<input type="text" id="tctcaption_'+fileInputCounterID+'" name="txtcaption[]" class="textbox" value=""/>'+
							'</td>'+
							'<td>'+
								'<input type="file" size="42" name="upfile[]" id="upfile_'+fileInputCounterID+'" class="file" />'+
								'<a href="" onclick="removeFileInput('+fileInputCounterID+'); return false;" title="Remove"> &otimes; </a>'+
							'</td>'+
						'</tr>';
			jQuery("#inputFilesTable tbody").append(str);		

			fileInputCount++;
			fileInputCounterID++;
			if( fileInputCount >= 5 ){
				jQuery("#lnkAddFileInputContainer").hide();
			}
		}
		
		function fileValid(file) {
			var tokens = file.split('.');
			var ext = tokens[tokens.length -1];
			ext = ext.toLowerCase();
			if(ext != 'pdf') {
				return false;
			}else{
				return true;
			}
		}
		
		function checkFiles(){
			var invalidFiles = [];
			var validFileCount = 0;
			jQuery("#uploader :file").each(function(){
				if( !fileValid(jQuery(this).val()) ){
					if( "" != jQuery(this).val() ){
						invalidFiles.push(jQuery(this).val());
					}
				}else{
					validFileCount++;
				}
			});
			if( 0 < invalidFiles.length ){
				var msg = 1 == invalidFiles.length ? "Only PDF files are accepted. This file is not valid:" : "Only PDF files are accepted. These files are not valid:";
				msg += '<p><ul>';
				for(var i=0; i<invalidFiles.length; i++){
					msg += '<li><strong>'+invalidFiles[i]+'</strong><li>';
				}
				msg += '</ul></p>';
				CustomPopup.initPrompt(msg,"Invalid File","OK");
				CustomPopup.createPopup();
				return false;
			}
			if( 0 == validFileCount ){
				CustomPopup.initPrompt("You have not selected a valid PDF file!","File Missing","OK");
				CustomPopup.createPopup();
				return false;
			}
			
			var noTitleCount = 0;
			if( 0 < validFileCount ){
				jQuery("#uploader :text").each(function(){
					if( "" == jQuery.trim(jQuery(this).val()) ){
						noTitleCount++;
					}
				});
			}
			if( 0 < noTitleCount ){
				CustomPopup.initPrompt("Please supply missing document title to some files.","Title Missing","OK");
				CustomPopup.createPopup();
				return false;
			}
			
			jQuery("#btnGoUpload").attr("disabled",true);
			jQuery("#btnCancelUpload").attr("disabled",true);
			return true;
		}		
		
		function removeFileInput(id){
			jQuery("#inputFile_"+id).remove();
			fileInputCount--;
			if( fileInputCount < 5 ){
				jQuery("#lnkAddFileInputContainer").show();
			}
		}
		
		jQuery(document).ready(function(){
			customMessageChanged(document.getElementById("txtCustomMessage"));
			jQuery("#chkNotifyMembers").change(function(){
				toggleCustomMessage(this);
			});
			jQuery("#radFileHidden").change(function(){
				if( jQuery(this).is(":checked") ){
					jQuery("#chkNotifyMembers").attr("disabled",true);
				}else{
					jQuery("#chkNotifyMembers").attr("disabled",false);
				}
				jQuery("#txtCustomMessage").attr("readonly",true);
			});
			jQuery("#radFilePublic").change(function(){
				if( jQuery(this).is(":checked") ){
					jQuery("#chkNotifyMembers").attr("disabled",false);
				}else{
					jQuery("#chkNotifyMembers").attr("disabled",true);
				}
				toggleCustomMessage(jQuery("#chkNotifyMembers"));
			});
			jQuery("#radFileMembersOnly").change(function(){
				if( jQuery(this).is(":checked") ){
					jQuery("#chkNotifyMembers").attr("disabled",false);
				}else{
					jQuery("#chkNotifyMembers").attr("disabled",true);
				}
				toggleCustomMessage(jQuery("#chkNotifyMembers"));
			});
			jQuery("#lnkAddFileInput").click(function(){
				addFileInput();
			});
			jQuery("#btnCancelUpload").click(function(){
				jQuery("#btnGoUpload").attr("disabled",true);
				jQuery(this).attr("disabled",true);
			});
			
			toggleCustomMessage(jQuery("#chkNotifyMembers"));
		});
	</script>	
BOF;

		Template::includeDependent($code);
	
	ob_end_clean();
?>	

<!-- displays the profile component: added by neri 11-06-08 -->

<?= $profile->render()?>	

<!-- end -->	
	
	<? $subNavigation->show(); ?>
	
<div id="resourcefiles_rundown">
	<div id="content_wrapper" class="layout2">
	
	<div id="wide_column"> 
	
		<div class="section" id="section_uploadedit_file" style="display: block;">
			
			<h2> <span> Upload File</span> </h2>	
			<?php if ( $error ) : ?>
			<div class="error_notice">
				<ul>								
				<? foreach($error as $index => $err ): ?> 
					<li><?= $err->getMessage()." ".$err->getName()." (".round($err->getsize()/1024,2)." kb)" ?></li>
				<? endforeach; ?>			
				</ul>
			</div>
			<?php endif; ?>
			<div class="section_small_details"> 
				You can upload PDF files like application forms, brochures, flyers, syllabi, reading lists and etc. so that your members and other interested <?php echo $siteName == 'GANET' ? 'GoAbroad' : $siteName; ?> Network users will have easy access to them, 24/7.
			</div>
			
			<div class="content" id="up_edit_file_section">
			<form class="interactive_form" id="uploader" name= "uploader" action="/resourcefiles.php?action=upload&cat=<?php echo $cat;?>&genID=<?php echo $genID;?>" method="post" enctype="multipart/form-data" onsubmit="return checkFiles();">
			<ul class="form" id="upedit_form">
				<li>
					<table id="inputFilesTable">
						<tbody>
							<th>
								<label> Document Title </label>
								<p class="supplement">
									Any descriptive text to identify your file.
								</p>
							</th>
							<th>
								<label> File </label>
								<p class="supplement">
									As of the moment you can only upload PDF files (e.g filename.pdf)
								</p>
							</th>
							<tr>
								<td>
									<input type="text" id="txtcaption_0" name="txtcaption[]" class="textbox" value=""/>
								</td>
								<td>
									<input type="file" size="42" name="upfile[]" id="upfile_0" class="file" />
								</td>
							</tr>
						</tbody>
					</table>
					<span id="lnkAddFileInputContainer" style="border:1px dotted #dfdfdf; padding: 3px 5px;">
						<a href="" id="lnkAddFileInput" onclick="return false;"> + Add Another File</a>
					</span>
				</li>
				<?php /*
				<li> 
					<label> Document Title </label>
					<p class="supplement">
						Any descriptive text to identify your file.
					</p>
					<input type="text" id="" name="txtcaption" class="textbox" value=""/>					 
				</li>
				
				<li>
					<label> File </label>
					<p class="supplement" for="<?= $varfile ?>">
						As of the moment you can only upload PDF files (e.g filename.pdf)
					</p>	
					<input type="file" sizze="42" name="upfile_0" id="<?= $varfile ?>" class="file" />
				</li>	
				*/?>			
				
				<li style="margin-bottom: 12px;">
					<label> Assign to Group </label>
					<p class="supplement">
						<?php if( $isParent ): ?>
							Select the group(s) where you want your file(s) to be visible. You can also assign your file(s) to multiple groups.
						<?php else: ?>
							You will assign your file(s) to this subgroup.
						<?php endif; ?>
					</p>
					
					
					<ul class="group_list">
					<? if(0 == count($sel_groups)):?>
						<li>
							<select style="margin: 0;" class="select_group" name="grpid_<? echo $groups[0]->getGroupID()?>" >
							<? foreach($groups as $group):?>
								<option value="<? echo $group->getGroupID();?>" <? if($group->getGroupID() == $ResourceFile->getGroupID()):?>selected<? endif;?> > <? echo $group->getName();?> </option>
							<? endforeach;?>
							</select>
						</li>
					<? else: ?>	
						<? foreach($sel_groups as $ctr => $gID): ?>
						<li>
							<select style="margin: 0;" class="select_group" name="grpid_<? echo $gID?>">
							<? foreach($groups as $group):?>
								<option value="<? echo $group->getGroupID();?>" <? if($group->getGroupID() == $gID):?>selected<? endif;?> > <? echo $group->getName();?> </option>
							<? endforeach;?>
							</select>
						</li>
						<? endforeach;?>		
					<? endif;?>
						
					</ul>						
					<?php if( $isParent ): ?>
						<span style="border:1px dotted #dfdfdf; padding: 3px 5px;">
							<a href="#" class="add_group_link"> + Add Another Group</a>
						</span>
					<?php endif; ?>
				</li>
				<li>
					<label> Visibility Settings </label>
					<p class="supplement">
						You can set who will be able to see and download this file.
					</p>					
					<p>
						<label class="radio_label">
							<input type="radio" id="radFileMembersOnly" name="fileprvpref" class="radio_btn" value="1" onclick="$('txtfileprvpref').value=1" <?php if(1==$prvcpref):?>checked="checked"<?php endif;?>/>
							Members Only
						</label>							
						
						<label class="radio_label">
							<input type="radio" id="radFilePublic" name="fileprvpref" class="radio_btn" value="2" onclick="$('txtfileprvpref').value=2" <?php if(2==$prvcpref):?>checked="checked"<?php endif;?>/>						
							Public
						</label>							

						<label class="radio_label">
							<input type="radio" id="radFileHidden" name="fileprvpref" class="radio_btn" value="0" onclick="$('txtfileprvpref').value=0;" <?php if(0==$prvcpref):?>checked="checked"<?php endif;?>/>						
							Hidden
						</label>							
					</p>																										
				</li>
				
				<li>
					<label>
						<input type="checkbox" id="chkNotifyMembers" name="chkNotifyMembers" value="1" <?php if($ismembersnotify): ?>checked="checked"<?php endif;?> <?php if(0==$prvcpref):?>disabled="disabled"<?php endif;?>/>
						Notify Members 
					</label>
					<p class="supplement">
						With this option selected, your members will receive a notification email regarding the resource file(s) you are going to upload.
					</p>
				</li>
				
				<li>
					<label> Custom Message </label>
					<p class="supplement">
						You may customize the message in the email which will be sent to your members notifying them of this new resource file you are about to add. Please observe that you do not remove the <b>[name]</b> and <b>[resource_files_list]</b> placeholders.
					</p>
					<textarea id="txtCustomMessage" name="txtCustomMessage" style="height: 250px;"><?php echo $defaultCustomMessage;?></textarea>
					<?/*<p class="supplement" id="remaining_chars"></p>*/?>
				</li>
				
				<li>
					<p>
						<? $cancel_target = "/resourcefiles.php?cat=admingroup&action=view&genID=".$genID; ?>
						<input type="submit" id="btnGoUpload" class="submit" name="upload" value="Upload" />
						<input type="button" id="btnCancelUpload" class="button" name="cancel" value="Cancel" onclick="window.location='<?= $cancel_target; ?>'"  />
					</p>				
				</li>
				
			</ul>	
			<input type="hidden" name="txtcat" 		value="<?=$cat;?>" />
			<input type="hidden" name="txtaction" 	value="<?=$action;?>" />
			<input type="hidden" name="txtgenid" 	value="<?=$genID;?>" />
			<input type="hidden" name="txtid" 		value="0" />
			<input type="hidden" id="txtfileprvpref" name="txtfileprvpref" value="2" />
			<input type="hidden" name="txtscriptname" 		value="/resourcefiles.php" />
			<input type="hidden" name="tmp_sid" value="<?=$tmp_sid?>" />										
			</form>		
			</div>
		
			<div class="foot"> </div>
		</div>
	
	</div>	
	<!--   	 -->
		

	<div id="narrow_column">
		<?php /*
		<div class="section">
			
			<h2> <span> What are Resource Files?</span> </h2>	
			
			<div class="content">
				<p>
					You can upload PDF files like application forms, brochures, flyers, syllabi, reading lists and etc. so that your members and other interested <?=(isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad Network";?> users will have easy access to them, 24/7.
				</p>					
			</div>
		
			<div class="foot"> </div>
		</div>
		*/ ?>
	</div>	
		
	</div>
	<!-- END content wrapper	 -->
</div>