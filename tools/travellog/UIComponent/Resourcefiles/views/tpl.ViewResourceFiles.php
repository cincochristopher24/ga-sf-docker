<?php
	Template::setMainTemplate($layoutMain);
	
	//Template::includeDependentJs("/js/prototype.js"); 
	//Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");
	//Template::includeDependentJs ( "/js/jquery-1.1.4.pack.js"           );
	Template::includeDependentJs("/min/f=js/prototype.js", array("top" => true));
	Template::includeDependentCss("/min/f=css/resourcefiles.css");
	Template::includeDependentJs("/js/resourcefile.js"); 
		
	if($group){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else{
			if(isset($isAdmin) && true == $isAdmin)
				Template::setMainTemplateVar('page_location', 'My Passport');
			else
				Template::setMainTemplateVar('page_location', 'Groups / Clubs');
		}
		//Template::setMainTemplateVar('page_location', $group);
		Template::setMainTemplateVar('group', $group);
	}
	$groupName = addslashes($parentGroup->getName());
	$code =<<<BOF
	<script type = "text/javascript">
	jQuery.noConflict();
	
	ResourceFiles = new ResourceFile($genID,"$context",$loginID,"$groupName");
		
		jQuery("#resourcefiles").ready(function(){   
			ResourceFiles.loadResourcefileLists()		
		});
		
		function page(s,p){ResourceFiles.page(s,p);}
		
	</script>	
BOF;

		Template::includeDependent($code);
?>	

<!-- displays the profile component: added by neri 11-06-08 -->

<?= $profile->render()?>	

<!-- end -->	
	
	<? $subNavigation->show(); ?>
	
	

<div id="resourcefiles_rundown">
	<div id="content_wrapper" class="layout2">
	
	<?php if ($this->hasVar('copying_notice_component')): ?>
		<?php $this->getVar('copying_notice_component')->render(); ?>
	<?php endif; ?>
	
	<div id="wide_column"> 
	
		<div class="section" id="section_show_files" style="display: block;">
			
			<h2> <span> Resource Files </span> </h2>	
			
			<div id="resourcefiles">

			</div>
			
			<div id="resource_wrap">
				<div id="showedit" class="resource_popbox" style="position:absolute;display:none;">
					<h1>Edit Title</h1>	
					<div id="editcaption">
						loading...
					</div>		
				</div>
				
				<div id="showpreference" class="resource_popbox" style="position:absolute;display: none;">
					<h1>Edit Visibility Settings</h1>	
					<div id="privacypreference">
						loading...
					</div>		
				</div>				
				
				<div id="showassign" class="resource_popbox" style="position:absolute;display: none;">
					<h1>Assign File</h1>	
					<div id="assigngroup">
						loading...
					</div>
				</div>
			</div>
			
		</div>	
		
		<div class="section" id="section_uploadedit_file" style="display: none;">
			<div class="foot"> </div>
		</div>
	
	</div>	
	<!--   #end wide_column-->
		

	<div id="narrow_column">
		<div class="section">
			
			<h2> <span> What are Resource Files?</span> </h2>	
			
			<div class="content">
				<p>
					<? if ($isAdminLogged): ?>
						You can upload PDF files like application forms, brochures, flyers, syllabi, reading lists and etc. so that your members and other interested <?=(isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad Network";?> users will have easy access to them, 24/7.
					<? elseif ($isMember): ?>
						Here are the PDF files that your group administrator has uploaded for you. You will find application forms, brochures, flyers, syllabi, reading lists and others here.
					<? else: ?>
						Here you will find application forms, brochures, flyers, syllabi, reading lists and other PDF files about the group.
					<? endif; ?>
				</p>					
			</div>
		
			<div class="foot"> </div>
		</div>
	</div>	<!-- end #narrow_column -->
		
	</div>
	<!-- END content wrapper	 -->
</div>	
	




<div id="managefilelayer">

</div>



