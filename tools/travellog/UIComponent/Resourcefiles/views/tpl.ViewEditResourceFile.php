<?php
	Template::setMainTemplate($layoutMain);
	Template::includeDependentJs("/min/f=js/prototype.js", array("top" => true));
	Template::includeDependentCss("/min/f=css/resourcefiles.css");
	Template::includeDependentJs("/js/resourcefile.js"); 
		
	if($group){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else{
			Template::setMainTemplateVar('page_location', 'My Passport');
		}
		Template::setMainTemplateVar('group', $group);
	}
	$groupName = addslashes($parentGroup->getName());
	$code =<<<BOF
	<script type = "text/javascript">
	jQuery.noConflict();
	
	ResourceFiles = new ResourceFile($genID,"$context",$loginID,"$groupName");
		
		jQuery("#resourcefiles").ready(function(){
			ResourceFiles.addGroup();
			ResourceFiles.removeGroup();
		});
		
		jQuery(document).ready(function(){
			jQuery("#btnCancelUpdate").click(function(){
				jQuery("#btnGoUpdate").attr("disabled",true);
				jQuery(this).attr("disabled",true);
			});
			//jQuery("#btnGoUpdate").click(function(){
			//	jQuery("#btnCancelUpdate").attr("disabled",true);
			//	jQuery("#btnGoUpdate").attr("disabled",true);
			//});
			jQuery("#edit_form").submit(function(){
				jQuery("#btnCancelUpdate").attr("disabled",true);
				jQuery("#btnGoUpdate").attr("disabled",true);
			});
		});
		
	</script>	
BOF;

		Template::includeDependent($code);
?>	

<!-- displays the profile component: added by neri 11-06-08 -->

<?= $profile->render()?>	

<!-- end -->	
	
	<? $subNavigation->show(); ?>
	
<div id="resourcefiles_rundown">
	<div id="content_wrapper" class="layout2">
	
	<div id="wide_column"> 
	
		<div class="section" id="section_uploadedit_file" style="display: block;">
			
			<h2> <span> Edit Resource File</span> </h2>	
			<div class="section_small_details"> 
				You can upload PDF files like application forms, brochures, flyers, syllabi, reading lists and etc. so that your members and other interested <?php echo $siteName == 'GANET' ? 'GoAbroad' : $siteName; ?> Network users will have easy access to them, 24/7.
			</div>
			
			<div class="content" id="up_edit_file_section">
			<form id="edit_form" name="edit_form" action="/resourcefiles.php?action=update" method="POST">
			<ul class="form" id="upedit_form">
				<li> 
					<label> Document Title </label>
					<p class="supplement">
						Any descriptive text to identify your file.
					</p>
					<input type="text" name="caption" id="" class="textbox" value="<? echo $resourcefile->getCaption();?>"/>					 
				</li>
				
				<li style="margin-bottom: 12px;">
					<label> Assign to Group </label>
					<p class="supplement">
						<?php if( $isParent ): ?>
							Select the group(s) where you want your file(s) to be visible. You can also assign your file(s) to multiple groups.
						<?php else: ?>
							You will assign your file(s) to this subgroup.
						<?php endif; ?>
					</p>
					
					
					<ul name="group_list" class="group_list">
					<?php if( $isParent ): ?>
						<? foreach($resourcefile->getAssignedGroups() as $key => $aGroup):?>
							<li >
								<select style="margin: 0;" name="group_select_list[]">
								<? foreach($groups as $group):?>
									<option value="<? echo $group->getGroupID();?>" <? if($aGroup->getGroupID() == $group->getGroupID()):?>selected<? endif;?> > <? echo $group->getName();?> </option>
								<? endforeach;?>
								</select>
								<? if(0 < $key):?>
									<a href="#" class="remove_group_link <? echo $key?>"> &otimes; Remove </a> 
								<? endif; ?>
							</li>
						<? endforeach;?>
					<?php else: ?>
						<li >
							<select style="margin: 0;" name="group_select_list[]">
							<? foreach($groups as $group):?>
								<option value="<? echo $group->getGroupID();?>" selected="selected"> <? echo $group->getName();?> </option>
							<? endforeach;?>
							</select>
						</li>
					<?php endif; ?>
					</ul>						
					
					<?php if( $isParent ): ?>
						<span style="border:1px dotted #dfdfdf; padding: 3px 5px;">
							<a href="#" class="add_group_link"> + Add Another Group</a>
						</span>
					<?php endif; ?>
				</li>
				<li>
					<label> Visibility Settings </label>
					<p class="supplement">
						You can set who will be able to see and download this file.
					</p>					

						<label class="radio_label">
							<input type="radio" name="fileprvpref" value="1" class="radio_btn" <? if(1 == $resourcefile->getPrivacypreference()):?>checked<? endif;?>/>
							Members Only
						</label>							
						
						<label class="radio_label">
							<input type="radio" name="fileprvpref" value="2" class="radio_btn" <? if(2 == $resourcefile->getPrivacypreference()):?>checked<? endif;?>/>						
							Public
						</label>							

						<label class="radio_label">
							<input type="radio" name="fileprvpref" value="0" class="radio_btn" <? if(0 == $resourcefile->getPrivacypreference()):?>checked<? endif;?>/>						
							Hidden
						</label>							
					</p>																										
				</li>
				
				<li>
					<p>
						<? $cancel_target = "/resourcefiles.php?cat=admingroup&action=view&genID=".$genID; ?>
						<input type="submit" id="btnGoUpdate" value="Save Changes" name="avatar_submit" class="submit"/>
						<input type="button" id="btnCancelUpdate" value="Cancel" onclick="window.location='<?= $cancel_target; ?>'" class="button"/>		
					</p>				
				</li>
				
				<input type="hidden" name="genID" value="<? echo $genID?>"/>
				<input type="hidden" name="resourcefileID" value="<? echo $resourcefile->getResourceFileID()?>"/>
				
			</ul>											
			</form>		
			</div>
		
			<div class="foot"> </div>
		</div>
	
	</div>	
	<!--   	 -->
		

	<div id="narrow_column">
		<?php /*
		<div class="section">
			
			<h2> <span> What are Resource Files?</span> </h2>	
			
			<div class="content">
				<p>
					You can upload PDF files like application forms, brochures, flyers, syllabi, reading lists and etc. so that your members and other interested <?=(isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad Network";?> users will have easy access to them, 24/7.
				</p>					
			</div>
		
			<div class="foot"> </div>
		</div>
		*/ ?>
	</div>	
		
	</div>
	<!-- END content wrapper	 -->
</div>	
