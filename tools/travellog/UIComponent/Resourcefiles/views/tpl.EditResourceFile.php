<?php
	ob_start();
	
	$tmp_sid = md5(uniqid(mt_rand(), true));
	
	Template::setMainTemplate($layoutMain);
	
	$style = "<style>	
					#AjaxLoading{padding:3px;font-size:14px;width:200px;text-align:center;position:absolute;border:none solid #666;background-color:#FFF;}
				</style>";
	
	if($group){
		Template::setMainTemplateVar('page_location', $group);
		Template::setMainTemplateVar('group', $group);
	}
	Template::includeDependent($style); 
	
	Template::includeDependentJs("/js/prototype.js");
	Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");  
	Template::includeDependentJs("/js/Upload_Progress.js"); 
	
	//Template::includeDependentJs("/js/htmldb_html_elements.js"); 
	//Template::includeDependentJs("/js/submitprogress.js"); 
	
	$code =<<<BOF
	<script type = "text/javascript">
		var UploadMeter = new UploadProgress("$tmp_sid");	
	</script>
BOF;

	Template::includeDependent($code);
	
	ob_end_clean();
	
?>
		
<!-- displays the profile component: added by neri 11-07-08 -->

	<?= $profile->render()?>	

<!-- end -->		
		
	<? $subNavigation->show(); ?>
	
	<div id="content_wrapper">	
		
		<!-- <div class="section" id="intro"> -->
  		<div class="section" id="edit_resource_section"> 			
			
			<h1 class="big_header">
				<span> Change a Resource File </span>
			</h1>
			
			<div class="content">
								
					<?php if ( $error ) : ?>
						<div class="error_notice">
							<ul>
								<? foreach($error as $index => $err ): ?> 
									<li><?= $err->getMessage()." ".$err->getName()." (".round($err->getsize()/1024,2)." kb)" ?></li>
								<? endforeach; ?>			
							</ul>
						</div>
					<?php endif; ?>					
				
					<div class="step">
						<p class="step_title">
							You are about to change a Resource File 
						</p>
						
						<p class="step_details">
							Just like uploading, changing a Resource File is as easy as finding it in your computer. The details of the file you want to change is displayed below.
						</p>		
										
						<form id="uploader" name= "uploader" action="/cgi-bin/uu_upload.pl?tmp_sid=<?=$tmp_sid?>" method="post" enctype="multipart/form-data" >
								 	
									<p class="file_details">
										<span class="file_title"><?=  $resourcefile->getCaption(); ?></span>
										<a class="file_to_edit" href="<? echo "http://".$_SERVER['SERVER_NAME']."/".$resourcefile->getResourcefileLink(); ?>">
												<? echo $resourcefile->getFilename(); ?>
										</a>
								 	</p>				 
				 
								 	<p>
										<input type="file" name="upfile_0">
									</p>													
										
									<p class="actions">
										<input class="submit" type="button" name="Submit" value="Save" onclick="UploadMeter.startUpload()"  />
										<input class="submit" type="button" name="cancel" value="Cancel" onClick="window.location='<?= "http://".$_SERVER['SERVER_NAME']."/resourcefiles.php?action=view&cat=".$context."&genID=".$genID  ?>'">			
									</p>
					
									<input type="hidden" name="txtcat" 		value="<?=$context;?>">
									<input type="hidden" name="txtaction" 	value="edit">
									<input type="hidden" name="txtgenid" 	value="<?=$genID;?>">
									<input type="hidden" name="txtid" 		value="<?=$resourcefileid?>">
									<input type="hidden" name="txtscriptname" 		value="/resourcefiles.php">										
						</form>																																		
											   
				</div>	
				
						<div id="progress_bar_notice" style="display: none;">
							<p class="loading_message">
								 Saving your changes... Please wait
							</p>			
						</div>					
				
				<div class="clear"> </div>		
			</div> <!-- End Section Content -->
			
			<div class="foot"> </div>
		</div>	
		
	</div>	


