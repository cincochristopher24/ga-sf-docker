<?php
	ob_start();
	$tmp_sid = md5(uniqid(mt_rand(), true));
	
	Template::setMainTemplate($layoutMain);
	
	$style = "<style>	
					#AjaxLoading{padding:3px;font-size:14px;width:200px;text-align:center;position:absolute;border:none solid #666;background-color:#FFF;}
				</style>";

	if($group){
		Template::setMainTemplateVar('page_location', $group);
		Template::setMainTemplateVar('group', $group);
	}
	
	Template::includeDependent($style); 
	Template::includeDependentJs("/min/f=js/prototype.js");
	Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");  
	Template::includeDependentJs("/js/Upload_Progress.js"); 
	
	
	//Template::includeDependentJs("/js/htmldb_html_elements.js"); 	
	//Template::includeDependentJs("/js/submitprogress.js"); 
	
	$code =<<<BOF
	<script type = "text/javascript">
		var UploadMeter = new UploadProgress("$tmp_sid");	
	</script>
BOF;

	Template::includeDependent($code);
	
	
	ob_end_clean();
	
?>

<!-- displays the profile component: added by neri 11-07-08 -->

	<?= $profile->render()?>	

<!-- end -->	
		
	<? $subNavigation->show(); ?>
		
		<div class="area" id="content_wrapper">
					
			<div class="section" id="add_resource_section">
					<h1>Add a Resource File</h1>
					<div class="content">
	 	  				<?php if ( $error ) : ?>
								<div class="error_notice">
									<ul>								
											<? foreach($error as $index => $err ): ?> 
												<li><?= $err->getMessage()." ".$err->getName()." (".round($err->getsize()/1024,2)." kb)" ?></li>
											<? endforeach; ?>			
									</ul>
								</div>
						<?php endif; ?>	
													
								<form class="interactive_form" name= "uploader" action="/cgi-bin/uu_upload.pl?tmp_sid=<?=$tmp_sid?>" method="post" enctype="multipart/form-data">
										<div id="upload_file" class="step">									
											<p class="step_title" for="<?= $varfile ?>">
												Select a File 
											</p>
											
											<p class="step_details">
												Uploading a file is as easy as selecting it from your computer. Please note however that you can only upload files in PDF format as of the moment. 
											</p>	
												
											<input type="file" name="upfile_0" id="<?= $varfile ?>" />
										</div>
										
										<?if($cat == 'admingroup'):?>
											<?if($groups && $traveler->isAdministrator()):?>
												<div id="assign_file" class="step">													
														
											<p class="step_title">
												Assign it
											</p>
											
											<p class="step_details"> 
												Assign the file you've just selected by checking the name of the corresponding group in the list below. If the upload is successful, the file will then be shared.
											</p>																												
														
														<ul id="group_list">					
																
																<li> 
																	<input type="checkbox" checked="ckecked" disabled="true" />
																	<?= $ResourceFile->getName();?> 
																</li>				
																							
																<? foreach($groups as $group): ?>	
																	<li>
															  			<input type="checkbox" <?if(in_array($group->getGroupID(),$sel_groups)):?> checked="checked" <? endif; ?> name="grpid_<?= $group->getGroupID();?>" value="<?= $group->getGroupID();?>"/>
															  			<?=  $group->getName()?>
																	</li>
																<? endforeach; ?>
														</ul>																											
												</div>
											<?endif; ?>
											
											
											<div id="visibility_of_file" class="step">
												<p class="step_title">
													Set Visibility Settings 
												</p>
											
												<p class="step_details">
													You have complete control of your file's visibility settings, select one of the following options below and that's it.
												</p>
													
												<p>	
													<label>
														<input value="0" <?if($error):?><? if($prvcpref ==0):?>checked="checked"<?endif;?><?endif;?>name ="fileprvpref" id="fileprvpref" type="radio" onclick="$('txtfileprvpref').value=0">
														Don't Show
													</label>	
													
													<label>
														<input value="1" <?if($error):?><? if($prvcpref ==1):?>checked="checked"<?endif;?><?endif;?>name ="fileprvpref" id="fileprvpref" type="radio" onclick="$('txtfileprvpref').value=1">
														Members only
													</label>	
													
													<label>	
														<input value="2" <?if($error):?><? if($prvcpref ==2):?>checked="checked"<?endif;?><?else:?>checked="checked"<?endif;?> name ="fileprvpref" id="fileprvpref" type="radio" onclick="$('txtfileprvpref').value=2">
														Public
													</label>														
												</p>
											</div>
											
											
													
												<div id="sending_opt_file" class="step">
														<p class="step_title">
															Upload it 
														</p>
														
														<? if($ResourceFile->getMembers()): ?>
																<p class="step_details">
																	That's so easy right? Now in the final step, you have an option to notify the group members, just check the box and finally click upload.
																</p>
																													
																<p>
																	<label><input type="checkbox" <?if($ismembersnotify):?> checked="checked" <? endif; ?> name="ismembersnotify"/>Notify Members</label>
																</p>
														
														<? endif; ?>
														
														<div class="actions">
															<p>																								
																<? $cancel_target = $cat=="admingroup"?"/group.php?gID=".$genID:'/resume.php?action=view'; ?>
																<input type="button" class="submit" name="upload" value="Upload" onclick="UploadMeter.startUpload()"  />
																<input type="button" class="submit" name="cancel" value="Cancel" onclick="window.location='<?= $cancel_target; ?>'"  />												
															</p>
														</div>													
												</div>
													
																								
											
											
											
																																
										<?endif;?>
																													
	    								<div id="progress_bar_notice" style="display:none;">
	    									<p class="loading_message">
	    										Uploading your file... Please Wait
	    									</p>				
	    								</div>
									
									<input type="hidden" name="txtcat" 		value="<?=$cat;?>">
									<input type="hidden" name="txtaction" 	value="<?=$action;?>">
									<input type="hidden" name="txtgenid" 	value="<?=$genID;?>">
									<input type="hidden" name="txtid" 		value="0">
									<input type="hidden" id="txtfileprvpref" name="txtfileprvpref" 		value="2">
									<input type="hidden" name="txtscriptname" 		value="/resourcefiles.php">
									
								</form>																	
																	
							
					</div>
				</div>
		</div>
