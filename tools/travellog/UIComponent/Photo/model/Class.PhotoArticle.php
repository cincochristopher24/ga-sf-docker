<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoTravellog.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoArticle extends AbstractPhoto{
	
	private $ARTICLEID	 		= 0;
	private $PHOTOcontext		= NULL;
	public $NUMPHOTOSTOUPLOAD 	= 5;
	static $photos = array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->ARTICLEID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		require_once('travellog/model/Class.Article.php'); 
		try{
		  	$article		= new Article($id);
			self::$photos	= $article->getPhotos();
			$this->PHOTOcontext = $article;
		}
		catch(Exception $e){
		}
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	function getPhotos(){
		return self::$photos;	
	}
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){
		if($this->PHOTOcontext->getAuthorID() == $loginID)
			return true;
		else
			return false;
	}
		
	function getHeaderCaption(){
		
		return $this->PHOTOcontext->getTitle();
		
		/*
		if($this->PHOTOcontext->getTraveler()->isAdvisor())
			return $this->PHOTOcontext->getOwner()->getName();
		else
			return $this->PHOTOcontext->getTraveler()->getUserName();
		*/
	}
	
	function getBackLink(){
		return "/article.php?action=view&articleID=".$this->ARTICLEID;
	} 
	
	function getBackCaption(){
		return "Article";
	} 
	function getNorecordlabel(){
		if(!count(self::$photos))
			return "There are no Photos in this article!";	
		
	} 
	
	function getSUBNavigation(SubNavigation $SUbnav, $loginID = NULL){
		if($loginID == $this->PHOTOcontext->getTravelerID()){	
			if($this->PHOTOcontext->getTraveler()->isAdvisor()){
				$SUbnav->setContextID($this->PHOTOcontext->getOwner()->getGroupID());
				$SUbnav->setContext('GROUP');
				$SUbnav->setGroupType('');									
				$SUbnav->setLinkToHighlight('GROUP_NAME');	
			}else{
				$SUbnav->setContextID($loginID);
				$SUbnav->setContext('TRAVELER');
				$SUbnav->setLinkToHighlight('MY_JOURNALS');
			}
			
		}
		return $SUbnav;
	}
	
	function createImage(ImageBuilder $ImgBuilder){
		
		$ImgBuilder->create('fullsize',500,500);
							
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'fullsize'.$ImgBuilder->getFilename());								
		$ImgBuilder->create('jentryheader',385,385);
		$ImgBuilder->create('jentrylboxheader',230,230);
		$ImgBuilder->crop_me('featured',164,123);
		$ImgBuilder->crop_me('standard',164,123);
		
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'standard'.$ImgBuilder->getFilename());	
		$ImgBuilder->crop_me('thumbnail',65,65);
		
		//copy orig file to destination if new upload file not rotate
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename())){
			rename($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename(),$ImgBuilder->getDestination()."orig-".$ImgBuilder->getFilename());
		
			//create txt file for rotating photo purpose.
			$filelocation	= $ImgBuilder->getFilename().".txt"; 
			$newfile 		= fopen($ImgBuilder->getDestination().$filelocation,"a+"); 
			fwrite($newfile, '360'); 
			fclose($newfile); 
		}
	}
	
	
} 
 
?>
