<?php
/*
 * Created on Nov 13, 2007
 * Class.RotatePhoto.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 class CropPhoto{
 	 
	 private $classcontext = NULL; 	 
 	 
 	 function __construct(PhotoFactory $Pfac){
 	 	$this->classcontext = $Pfac->create()->getPHOTOcontext();
 	 }
 	 
 	 
	 function crop($photoID,$cropWidth, $cropHeight, $cropX, $cropY, $picsize){
	 		
 		require_once('travellog/model/Class.Photo.php');
		require_once 'Image/Transform.php';
		$photo 	= new Photo($this->classcontext,$photoID);

		
		
		$i = Image_Transform::factory('IM');				
		$i->load($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."fullsize".$photo->getFilename());
		$i->crop($cropWidth,$cropHeight,$cropX,$cropY);
		
		if($picsize == "thumbnail"){
			$i->save($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."thumbnail".$photo->getFilename());
			if($cropWidth >65 || $cropHeight >65){
				$i->load($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."thumbnail".$photo->getFilename());
				$i->fit(65,65);
				$i->save($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."thumbnail".$photo->getFilename());
			}
		}
	 	
	 	//old version, to crop with not same size
		/*
		if($picsize == "profile"){
			$i->save($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."standard".$photo->getFilename());
	 		$i->load($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."standard".$photo->getFilename());
	 		
	 		if($i->getImageWidth() >150 || $i->getImageHeight() >200){
	 			
	 			if($i->getImageWidth() >= 150){
					$i->fitY(200);
					$i->save($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."standard".$photo->getFilename());
					$i->load($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."standard".$photo->getFilename());
				}
				$i->crop(150,200, ($i->getImageWidth()-150)/2 , ($i->getImageHeight()-200)/2);
				$i->save($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."standard".$photo->getFilename());
	 		}
	 	}	
	 	*/
		
		if($picsize == "profile"){
			$i->save($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."standard".$photo->getFilename());
			if($cropWidth >120 || $cropHeight >120){
				$i->load($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."standard".$photo->getFilename());
				$i->fit(120,120);
				$i->save($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."standard".$photo->getFilename());
			}
		}
			
											
	}
	 
 } 
 
?>
