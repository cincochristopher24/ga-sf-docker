<?php
/*
 * Created on Sep 28, 2007
 *
 * 
 */
 
 
 
 require_once("travellog/UIComponent/Photo/interface/IPhotoManager.php");
 
 class PhotoManager implements IPhotoManager{
 	
 	private $files				= NULL;
 	
 	private $commands 			= array();
	
	private $context			= NULL;
	private $loginID			= 0;			//userID
	private $genID				= 0;			//ID Based on Context
 	private $photoid			= 0;
 	
 	private $primary 			= 0;
 	private $phototype			= NULL;
 	private $validfiles		= array();
	private $invalidfiles		= array();
	
	private $isGroupchecked 	= false;
	private $isAddresschecked 	= false;
	
	private $uploaded_photoIDSnFilenames	= array();		//Photo IDs and filenames of valid files Uploaded.
	
	function setFiles($files){
		$this->files = $files;
	}
	
	function getFiles(){
		return $this->files;
	}
	
	function setContext($context){
		$this->context = $context;
	}
	
	function getContext(){
		return $this->context;
	}
	
	function setLoginID($loginID){
		$this->loginID = $loginID;
	}
	
	function getLoginID(){
		return $this->loginID;
	}
	
	function setGenID($genID){
		$this->genID = $genID;
	}
	
	function getGenID(){
		return $this->genID;
	}
	
	function setPhotoType($phototype){
		$this->phototype = $phototype;
	}
	
	function setPhotoID($photoid){
		$this->photoid = $photoid;
	}	
	
	function setIsGroupChecked($isGroupchecked){
		$this->isGroupchecked = $isGroupchecked;
	}
	
	function getIsGroupChecked(){
		return $this->isGroupchecked;
	}
	
	function setIsAddressChecked($isAddresschecked){
		$this->isAddresschecked = $isAddresschecked;
	}
	
	function getIsAddressChecked(){
		return $this->isAddresschecked;
	}
	
	/**
	 * Save Uploaded Photo
	 */
	function save(UploadManager $UPM){
		
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/model/Class.Photo.php");
		
		$this->context = PhotoFactory::PhotoFactoryPeer($this->genID,$this->context)->create()->getPHOTOcontext();
		
		if($UPM->getValid()){
			foreach($UPM->getValid() as $ufile){							
				$photo	= new Photo($this->context);
				$photo->setPhotoTypeID($this->context->phototype);						
				$photo->setFilename($ufile->getName());	
				$photo->Create();
				$this->uploaded_photoIDSnFilenames[] = array("photoID"=>$photo->getPhotoID(),"filenames"=>$ufile->getName());								
			}
			//set commands
			foreach($this->commands as $cmd ){
				if ($cmd->onCommand($this,array('photoID'=>$photo->getPhotoID())));
			}
		}
		$this->validfiles 	= $UPM->getValid();
		$this->invalidfiles = $UPM->getInValid();
	}
	
	/**
	 * Set new command to execute after adding photos
	 * @param array of commands
	 */
	function addCommand( $cmd ){
	   $this->commands[]= $cmd;
	}
	
	
	/**
	 * Change Photo
	 * @param photoid
	 * @return 
	 */
	function change(UploadManager $UPM){
		
		require_once("travellog/model/Class.Photo.php");
		$this->context = $UPM->getContext();
		if($UPM->getValid()){																									
			foreach($UPM->getValid() as $ufile){							
				$photo 	= new Photo($this->context,$this->photoid);
				$UPM->deletefile($photo->getFileName());
				$photo->setFilename($ufile->getName());												
				$photo->Update();
			}
			foreach($this->commands as $cmd ){
				if ($cmd->onCommand($this,array('photoID'=>$photo->getPhotoID())));
			}
		}	
		$this->validfiles 	= $UPM->getValid();
		$this->invalidfiles = $UPM->getInValid();		
	}
	
	
	function setASPrimary(PhotoFactory $Pfac, $photoID){
		require_once('travellog/model/Class.Photo.php');
		$photo 	= new Photo($Pfac->create()->getPHOTOcontext(),$photoID);
		$photo->setAsPrimaryPhoto();		
	}
	
	/**
	 * Rotate Photo by angle
	 * @param PhotoFactory, photoid, rotation
	 * @return 
	 */
	function rotate(PhotoFactory $Pfac, $photoID, $rotation){
		require_once("travellog/UIComponent/Photo/model/Class.RotatePhoto.php");
		$RotatePhoto = new RotatePhoto($Pfac);
		$RotatePhoto->rotate($photoID,$rotation);
	}
	
	/**
	 * Rearrange Photo in Order
	 * @param $photoIDs
	 * @return 
	 */
	function rearrange(PhotoFactory $Pfac, $photoIDs){
		$Pfac->create()->getPHOTOcontext()->reArrangePhoto($photoIDs);
	}
	
	/**
	 * Delete Photo
	 * @param PhotoFactory, photoID
	 * @return 
	 */
	function delete(PhotoFactory $Pfac, $photoID){
		require_once('travellog/model/Class.Photo.php');
		$photo 	= new Photo($Pfac->create()->getPHOTOcontext(),$photoID);
		$photo->Delete();	
	}
	
	/**
	 * Add Caption of Photo
	 * @param photoid
	 * @return 
	 */
	function updatecaption(PhotoFactory $Pfac, $photoID, $caption){
		require_once('travellog/model/Class.Photo.php');
		$photo 	= new Photo($Pfac->create()->getPHOTOcontext(),$photoID);
		$photo-> setCaption($caption);		
		$photo->Update();
	}
	
	
	function crop(PhotoFactory $Pfac, $photoID, $cropWidth, $cropHeight, $cropX, $cropY, $picsize){
		require_once("travellog/UIComponent/Photo/model/Class.CropPhoto.php");
		$CropPhoto = new CropPhoto($Pfac);
		$CropPhoto->crop($photoID,$cropWidth, $cropHeight, $cropX, $cropY, $picsize);
	}
	
	function getValidFiles(){
		return $this->validfiles;
	}
	
	function getInValidFiles(){
		return $this->invalidfiles;
	}
	
 	
 	function getFilenames(){
 		$arr_filenames = array();
 		foreach($this->validfiles as $vVO){
			$arr_filenames[] = $vVO->getName();	
				
		}
 		return $arr_filenames;
	}
 	
 	function getVALIDUploadPHOTOIDsnFilenames(){
 		return $this->uploaded_photoIDSnFilenames;
 	}
 	
 }
 
 
?>
