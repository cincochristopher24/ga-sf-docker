<?php

/*
 *  ClassPhotoLayoutType.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.PhotoLayoutType - Type of Layout
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */

class PhotoUIRequestType{
	
	public static $ONLOADGALLERY 	= 1;
	public static $FULLVIEW 		= 2;
	public static $UPLOADPHOTO		= 3;
	public static $ADDPHOTO		= 4;
	public static $EDITPHOTO		= 5;   
	public static $SAVEEDITPHOTO	= 6;
	public static $ROTATEPHOTO		= 7;
	public static $DELETEPHOTO		= 8;
	public static $EDITCAPTION		= 9;
	public static $SAVECAPTION		= 10;
	public static $DOPRIMARY		= 11;
	public static $DOPRIMARYTRAVEL	= 12;
	
	
	        
}