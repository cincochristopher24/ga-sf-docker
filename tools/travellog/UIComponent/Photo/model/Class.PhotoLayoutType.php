<?php

/*
 *  ClassPhotoLayoutType.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.PhotoLayoutType - Type of Layout
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */

class PhotoLayoutType{
	
	public static $BASIC 		= 1;
	public static $LIGHTBOX 	= 2;
	public static $LYTEBOX		= 7; //problem in safari
	public static $THICKBOX	= 4; //4 before
	public static $LIGHTWINDOW	= 5;
	public static $TINYBOX		= 6;
	public static $JQMODAL		= 3; //7
	public static $THUMBGRID	= 8; //for new layout of journal entry photos in the journal page; added by JUL  
}