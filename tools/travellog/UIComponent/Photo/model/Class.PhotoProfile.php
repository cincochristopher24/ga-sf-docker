<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoProfile.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

require_once("travellog/UIComponent/Photo/abstract/AbstractPhoto.php");

class PhotoProfile extends AbstractPhoto{
	
	private $PROFILEID	 		= 0;
	private $PHOTOcontext		= NULL;
	public  $NUMPHOTOSTOUPLOAD 	= 5;
	static  $photos 			= array();
	
	function __construct($id=0){
		self::setPHOTOcontext($id);
		$this->PROFILEID = $id;
		return $this;
	}
	
	function setPHOTOcontext($id){
		require_once('travellog/model/Class.TravelerProfile.php');
		
		try{
		  	$profile		= new TravelerProfile($id);
			self::$photos	= $profile->getPhotos();
			$this->PHOTOcontext = $profile;
		}
		catch(Exception $e){
		}
	}
	
	function getPHOTOcontext(){
		return $this->PHOTOcontext;	
	}
	
	/**
	 * Profile Photos
	 */
	function getPhotos(){
		return self::$photos;	
	}
	/**
	 * Traveler Journal Entries Photos
	 */
	function getTravelerEntriesPhotos(){
		return $this->PHOTOcontext->getTravelerJournalEntriesPhotos();	
	}
	/**
	 * Traveler Group Photos (moderator/administrator only)
	 */
	function getTravelerGroupPhotos(){
		return $this->PHOTOcontext->getTravelerGroupPhotos();	
	}
	
	function getCOUNTPhotos(){
		return count(self::$photos);	
	}
	
	function isOwner($loginID){
		if($this->PROFILEID == $loginID)
			return true;
		else
			return false;
	}
		
	function getHeaderCaption(){
		$x = (count(self::$photos)>1)? "photos": "photo";	
		return "Profile ".$x;
		/*
		if(strlen($this->PHOTOcontext->getUserName())-1 == strripos($this->PHOTOcontext->getUserName(),'s'))
			return $this->PHOTOcontext->getUserName()."'  ".$x;
		else
			return $this->PHOTOcontext->getUserName()."'s  ".$x;
		*/
	}
	
	function getBackLink(){
		return "/".$this->PHOTOcontext->getUserName();
	} 
	
	function getBackCaption(){
		if(strlen($this->PHOTOcontext->getUserName())-1 == strripos($this->PHOTOcontext->getUserName(),'s'))
			return $this->PHOTOcontext->getUserName()."'  "."Profile";
		else
			return $this->PHOTOcontext->getUserName()."'s  "."Profile";
	} 
	function getNorecordlabel(){
		if(!count(self::$photos))
			return "There are no Photos in Profile!";	
		
	} 
	
	function getSUBNavigation(SubNavigation $SUbnav, $loginID = NULL){
		require_once("travellog/model/Class.SubNavigation.php");
		$SUbnav->setContextID($this->PROFILEID);
		$SUbnav->setContext('TRAVELER');
		$SUbnav->setLinkToHighlight('MY_PROFILE');
		return $SUbnav;
	}
	
	function createImage(ImageBuilder $ImgBuilder){
		
		$ImgBuilder->create('fullsize',500,500);
							
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'fullsize'.$ImgBuilder->getFilename());								
		$ImgBuilder->crop_me('standard',120,120);
		$ImgBuilder->crop_me('featured',164,123);
		$ImgBuilder->setImageFile($ImgBuilder->getDestination().'standard'.$ImgBuilder->getFilename());	
		$ImgBuilder->crop_me('thumbnail',65,65);
		
		//copy orig file to destination if new upload file not rotate
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename())){
			rename($_SERVER['DOCUMENT_ROOT']."/users/".$ImgBuilder->getFilename(),$ImgBuilder->getDestination()."orig-".$ImgBuilder->getFilename());
			
			//create txt file for rotating photo purpose.
			$filelocation=$ImgBuilder->getFilename().".txt"; 
			$newfile = fopen($ImgBuilder->getDestination().$filelocation,"a+"); 
			fwrite($newfile, '360'); 
			fclose($newfile); 
		}	
			
	}
	
} 
 
?>
