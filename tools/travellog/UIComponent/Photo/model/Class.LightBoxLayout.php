<?php

/*
 *  ClassLightBoxLayout.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.LightBoxLayout .
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */

require_once('travellog/UIComponent/Photo/abstract/AbstractPhotoLayout.php');
require_once('travellog/UIComponent/Photo/interface/IPhotoLayout.php');

class LightBoxLayout extends PhotoLayout implements IPhotoLayout {
	
	private $filenames = array();
	private $pagenum 	= 1;
	private $startrow 	= 1;
    /**
     *
     * @param $filenames, array Filenames of Photos   
     */
    function loadFileNames($filenames){	
    	$this->filenames = $filenames;
    }
    function getPhotoFileNames(){
    	return $this->filenames;
    }
    function setPagenum($pagenum){
    	$this->pagenum = $pagenum;
    }
    function getPagenum(){
    	return $this->pagenum;
    }
    function setStartrow($startrow){
    	$this->startrow = $startrow;
    }
    function getStartrow(){
    	return $this->startrow;
    }
    /**
	 * Create LightBox Layout
	 * @return LightBox Layout Template 
	 */
	function createLayout(){
		require_once('travellog/UIComponent/Photo/model/Class.LightboxLayoutTemplate.php');
		return new LightboxLayoutTemplate($this);
	}
	
	
}