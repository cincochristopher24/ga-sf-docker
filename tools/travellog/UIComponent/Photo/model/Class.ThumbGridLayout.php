<?php

/*
 *  Class.ThumbGridLayoutTemplate.php  2009-01-08 00:00:00Z root $
 */

/**
 * Class.ThumbGridLayout .
 * @author Jul Warren D. Garcia <jul.garcia@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */

require_once('travellog/UIComponent/Photo/abstract/AbstractPhotoLayout.php');
require_once('travellog/UIComponent/Photo/interface/IPhotoLayout.php');

class ThumbGridLayout extends PhotoLayout implements IPhotoLayout {
	
	private $filenames 	= array();
	private $pagenum 		= 1;
	private $startrow 		= 1;
	private $isadmin		= false;
	private $parentData 	= array();
	private $photos			= array();
	
    /**
     *
     * @param $filenames, array Filenames of Photos   
     */
    function loadPhotos(PhotoFactory $PhotoFactory){	
    	
    	$this->PhotoCONTEXT = $PhotoFactory->create();
    	
    	$this->isadmin = false;
    	
    	$photos = $PhotoFactory->create()->getPhotos();
    	if($photos){
			foreach($photos as $p){
				//	if(!$p->getPrimaryPhoto())
				$this->photos[] = $p;
			}
		}
		
    }
    
    function getPhotoCONTEXT(){
    	return $this->PhotoCONTEXT;
    }
	
	function getPhotos(){
    	return $this->photos;
    }
    
    function getPhotoFileNames(){
    	return $this->filenames;
    }
    function setPagenum($pagenum){
    	$this->pagenum = $pagenum;
    }
    function getPagenum(){
    	return $this->pagenum;
    }
    function setStartrow($startrow){
    	$this->startrow = $startrow;
    }
    function getStartrow(){
    	return $this->startrow;
    }
    
    function setIsAdmin($isadmin){
    	$this->isadmin = $isadmin;
    }
    
    function getIsAdmin(){
    	return $this->isadmin;
    }
    
    function setParentData($data){
    	$this->parentData = $data;
    }
    
    function getParentData(){
    	return $this->parentData;
    }
    
    /**
	 * Create ThumbGridLayout
	 * @return ThumbGridLayoutTemplate 
	 */
	function createLayout(){
		require_once('travellog/UIComponent/Photo/model/Class.ThumbGridLayoutTemplate.php');
		return new ThumbGridLayoutTemplate($this);
	}
	
	
}