<?php
/*
 * Created on Oct 3, 2007
 * Class.ParsePERLUploadParams.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

class ParsePERLUploadParams{
		
	
	static $files 		= array();
	static $options	= array();
		
	function ParsePERLUploadParams($param){
		$resultparam = $_SERVER['DOCUMENT_ROOT'].'/users/'.$param.'.params';
		if(file_exists($resultparam)){				
			$params 		= parse_ini_file($resultparam);				
			foreach($params as $p => $prs){
				if(strpos($p,'upfile') !== false){
					array_push(self::$files,$prs);
				}
			}
			if(array_key_exists('chkGroup',$params)){
				self::$options['group'] = true;
			}
			if(array_key_exists('chkAddressBook',$params)){
				self::$options['addressbook'] = true;
			}
			
		}	
		
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/users/'.$param.'.params')){
			unlink($_SERVER['DOCUMENT_ROOT'].'/users/'.$param.'.params');
		}
	}
	
	public static function getFiles(){
		return self::$files;
	}
	
	public static function getOptions(){
		return self::$options;
	}
	
	public static function isGroupNotified(){
		
		if(array_key_exists('group',self::$options))
			return true;
		else
			return false;
	}
	
	public static function isAddressBookNotified(){
		
		if(array_key_exists('addressbook',self::$options))
			return true;
		else
			return false;
	}
		
}

?>
