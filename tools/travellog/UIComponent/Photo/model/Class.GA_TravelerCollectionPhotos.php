<?php
/*
 * Created on Oct 23, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

require_once("Cache/ganetCacheProvider.php");
 
class  GA_TravelerCollectionPhotos{
		
		static $photos		= array();
		static $travelerID	= 0;
		
		function GA_TravelerCollectionPhotos($travelerID){
				self::$travelerID = $travelerID;
		}  
		
		function doProfile(){
			require_once('travellog/model/Class.TravelerProfile.php');	
			require_once('travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php');
			
			$Profile = new TravelerProfile(self::$travelerID);
			$count = $Profile->getCountPhotos();
			if( $this->mCountOnly ){
				$this->mCount += $count;
				return;
			}
			if($count>0){	
				$GaCollectionDTO = new GaCollectionDTO();
				$GaCollectionDTO->setGenID(self::$travelerID);
				$GaCollectionDTO->setTitle('Profile Photos');
				$GaCollectionDTO->setContext('profile');
				$GaCollectionDTO->setPrimaryPhoto($Profile->getPrimaryPhoto());
				$GaCollectionDTO->setNumPhotos($Profile->getCountPhotos());
				self::$photos[] = $GaCollectionDTO;
			}
				
		}
		
		function doJournal(){
				require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");
				
				$travelerID  = self::$travelerID;
				
				require_once 'Class.Connection.php';
 				require_once 'Class.Recordset.php';
				
				$conn = new Connection();
				$rs   = new Recordset($conn);
				
				$rs2   = new Recordset($conn);
				
				if( RequestData::isPrivileged() ){
					require_once('travellog/model/Class.Travel.php');
					require_once('travellog/model/Class.TravelLog.php');		
					require_once('travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php');
					
					$sql = "SELECT tblTravel.travelerID, tblTravel.travellinkID, tblTravel.travelID as travel_ID, tblTravel.primaryphoto, tblTravel.title as travel_title
							FROM tblTravel, tblTravelLink
							WHERE travelerID = $travelerID
								AND tblTravel.publish = 1 
								AND tblTravel.deleted = 0
								AND tblTravel.travellinkID = tblTravelLink.travellinkID
								AND tblTravelLink.refType = 1
								AND tblTravelLink.refID = $travelerID
							GROUP BY travel_ID
							ORDER BY travel_ID DESC";
					
					$rs->Execute($sql);
					
					if( $rs->Recordcount() ){
						while( $row = mysql_fetch_assoc($rs->Resultset()) ){
							$travel_ID = $row['travel_ID'];
							if( !$this->mCountOnly ){
								$travel = new Travel($travel_ID);
								$TravelLogs = $travel->getTravelLogsByTravelID();
								if( 0 == count($TravelLogs) ){
									continue;
								}
								$TravelLog = $TravelLogs[0];
							
								if( 0 < $row["primaryphoto"] ){
									$photo = new Photo($TravelLog,$row["primaryphoto"]);
								}else{
									try{
										$photo = $travel->getRandomPhoto();
									}catch(exception $e){
										$photo = new Photo($TravelLog,0);
									}
								}
							}
							$sql2 = "SELECT count(tblPhoto.photoID) as cnt_photo FROM tblTravel, tblTravelLog, tblPhoto, tblTravelLogtoPhoto 
					 				   	WHERE tblTravel.travelID = $travel_ID
					 				  	AND tblTravel.travelID = tblTravelLog.travelID
										AND tblTravelLog.deleted = 0
					 				 	AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID 
					 				 	AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID 
					 				 	GROUP BY tblPhoto.photoID";
					 		$rs2->Execute($sql2);
							if( $this->mCountOnly ){
								$this->mCount += $rs2->Recordcount();
								continue;
							}
							$GaCollectionDTO = new GaCollectionDTO();
							$GaCollectionDTO->setGenID($row['travel_ID']);
							$GaCollectionDTO->setTitle($row['travel_title']);
							$GaCollectionDTO->setContext('journal');
							$GaCollectionDTO->setPrimaryPhoto($photo);
							$GaCollectionDTO->setNumPhotos($rs2->Recordcount());
							self::$photos[] = $GaCollectionDTO;
						}
					}
				}else{
					$session_obj = SessionManager::getInstance();
					$loginID = $session_obj->get("travelerID");
					$isCobrandFiltered = FALSE;
					if( isset($GLOBALS['CONFIG']) && $loginID != $travelerID ){
						$isCobrandFiltered = TRUE;
						$config = $GLOBALS['CONFIG'];
						$groupID = $config->getGroupID();
						$sql = "SELECT tblTravel.travelerID, tblTravel.primaryphoto, tblTravel.title as travel_title, tblTravel.*, tblTravelLog.*, tblPhoto.*, tblTravelLogtoPhoto.position 
								FROM tblTravel,  tblTravelLog, tblPhoto, tblTravelLogtoPhoto, tblTravelLink
				 				WHERE tblTravel.travelerID = $travelerID
									AND tblTravel.travelID = tblTravelLog.travelID
									AND tblTravel.travellinkID = tblTravelLink.travellinkID
									AND tblTravelLink.refType = 1
									AND tblTravelLink.refID = $travelerID
									AND tblTravelLog.deleted = 0
				 				 	AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID 
				 				 	AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID 
				 				GROUP BY tblPhoto.photoID
				 				ORDER BY tblTravelLog.travellogID DESC";
					
						//search for approved travel journals within the group network
						$sqlApproved = "SELECT tblTravel.travelID AS travelID
										FROM tblTravel, tblGroup, tblGroupApprovedJournals
										WHERE (tblGroup.groupID = $groupID OR tblGroup.parentID = $groupID)
											AND tblGroupApprovedJournals.groupID = tblGroup.groupID
											AND tblGroupApprovedJournals.approved = 1
											AND tblTravel.travelID = tblGroupApprovedJournals.travelID
											AND tblTravel.deleted = 0
										GROUP BY travelID";
						
						$rsApproved  = new Recordset($conn);
						$rsApproved->Execute($sqlApproved);
						$approvedJournals = array();
						while( $row = mysql_fetch_assoc($rsApproved->ResultSet()) ){
							$approvedJournals[] = $row["travelID"];
						}
					}else{
						$sql = "SELECT tblTravel.travelerID, tblTravel.primaryphoto, tblTravel.title as travel_title, tblTravel.*, tblTravelLog.*, tblPhoto.*, tblTravelLogtoPhoto.position 
								FROM tblTravel,  tblTravelLog, tblPhoto, tblTravelLogtoPhoto, tblTravelLink 
				 				WHERE tblTravel.travelerID = $travelerID
				 				  	AND tblTravel.travelID = tblTravelLog.travelID
									AND tblTravel.travellinkID = tblTravelLink.travellinkID
									AND tblTravelLink.refType = 1
									AND tblTravelLink.refID = $travelerID
									AND tblTravelLog.deleted = 0
				 				 	AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID 
				 				 	AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID 
				 				GROUP BY tblPhoto.photoID
				 				ORDER BY tblTravelLog.travellogID DESC";
					}
			 		$rs->Execute($sql); 
				
					$tmp_holder 			= array();
					$holder 			= array();
				
					if( $rs->Recordcount() ){
							require_once('travellog/model/Class.TravelLog.php');	
							require_once('travellog/model/Class.Photo.php');	
							require_once('travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php');	
							//filter
							while( $row = mysql_fetch_assoc($rs->Resultset()) ){
										$tmp_holder[$row['travelID']][$row['photoID']] = $row;
										
										if( $isCobrandFiltered && !in_array($row["travelID"],$approvedJournals) ){
											continue;
										}
										
										if(!in_array($row['travelID'],$holder)){			
											
												$travelID = $row['travelID'];
											
												//count photos is journal
												$sql = "SELECT count(tblPhoto.photoID) as cnt_photo FROM tblTravel, tblTravelLog, tblPhoto, tblTravelLogtoPhoto 
										 				   	WHERE tblTravel.travelID = $travelID
										 				  	AND tblTravel.travelID = tblTravelLog.travelID
															AND tblTravelLog.deleted = 0
										 				 	AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID 
										 				 	AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID 
										 				 	GROUP BY tblPhoto.photoID";
										 		$rs2->Execute($sql); 
												
												if( $this->mCountOnly ){
													$this->mCount += $rs2->Recordcount();
													$holder[] = $row['travelID'];
													continue;
												}
												
												$TravelLog = new TravelLog(0,$row);
										
												$current_photos	= $tmp_holder[$row['travelID']];
												if($row['primaryphoto']>0){
														$photo = new Photo($TravelLog,0,$row);
												}else{
														$random_photos = array_rand($current_photos);
														$photo = new Photo($TravelLog,0,$current_photos[$random_photos]);
												}
												$GaCollectionDTO = new GaCollectionDTO();
												$GaCollectionDTO->setGenID($row['travelID']);
												$GaCollectionDTO->setTitle($row['travel_title']);
												$GaCollectionDTO->setContext('journal');
												$GaCollectionDTO->setPrimaryPhoto($photo);
												$GaCollectionDTO->setNumPhotos($rs2->Recordcount());
												self::$photos[] = $GaCollectionDTO;
									
										}
						
										$holder[] = $row['travelID'];
									
										//print_r($row['travelID']."<br>");
									
							}
						/*
						$rs->Execute($sql); 
						while( $row = mysql_fetch_assoc($rs->Resultset()) ){
							if(!in_array($row['travelID'],$holder)){			
									$TravelLog = new TravelLog(0,$row);
								
									$current_photos	= $tmp_holder[$row['travelID']];
									if($row['primaryphoto']>0){
											$photo = new Photo($TravelLog,0,$row);
									}else{
											$random_photos = array_rand($current_photos);
											$photo = new Photo($TravelLog,0,$current_photos[$random_photos]);
									}
								
									$GaCollectionDTO = new GaCollectionDTO();
									$GaCollectionDTO->setGenID($row['travelID']);
									$GaCollectionDTO->setTitle($row['travel_title']);
									$GaCollectionDTO->setContext('journal');
									$GaCollectionDTO->setPrimaryPhoto($photo);
									$GaCollectionDTO->setNumPhotos(count($tmp_holder[$row['travelID']]));
									self::$photos[] = $GaCollectionDTO;
								
							}
							$holder[] = $row['travelID'];
						}
						*/
					}
				}
		}
		
		function doArticle(){
				require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");
				
				$travelerID  = self::$travelerID;
				
				require_once 'Class.Connection.php';
 				require_once 'Class.Recordset.php';
				
				$conn = new Connection();
				$rs   = new Recordset($conn);
				
				$rs2   = new Recordset($conn);
				
				if( RequestData::isPrivileged() ){
					require_once('travellog/model/Class.Article.php');
					require_once('travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php');
					
					$sql = "SELECT * FROM tblArticle WHERE deleted = 0 AND authorID = $travelerID AND groupID = 0";
					$rs->Execute($sql);
					if( $rs->Recordcount() ){
						while( $row = mysql_fetch_assoc($rs->Resultset()) ){
							$articleID = $row['articleID'];
							
							$article = new Article($articleID);
							$primary = $article->getPrimaryPhotoID();
							if( 0 < $primary ){
								$photo = new Photo($article,$primary);
							}else{
								try{
									$photo = $article->getRandomPhoto();
								}catch(exception $e){
									$photo = new Photo($article,0);
								}
							}
							
							$sql2 = "SELECT count(tblPhoto.photoID) as cnt_photo FROM tblArticle, tblArticletoPhoto, tblPhoto  
					 				   	WHERE tblArticle.articleID = $articleID
										AND tblArticle.deleted = 0
										AND tblArticle.groupID = 0
					 				 	AND tblArticle.articleID = tblArticletoPhoto.articleID 
					 				 	AND tblPhoto.photoID = tblArticletoPhoto.photoID 
					 				 	GROUP BY tblPhoto.photoID";
					 		$rs2->Execute($sql2);
							
							$GaCollectionDTO = new GaCollectionDTO();
							$GaCollectionDTO->setGenID($row['articleID']);
							$GaCollectionDTO->setTitle($row['title']);
							$GaCollectionDTO->setContext('article');
							$GaCollectionDTO->setPrimaryPhoto($photo);
							$GaCollectionDTO->setNumPhotos($rs2->Recordcount());
							self::$photos[] = $GaCollectionDTO;
						}
					}
				}else{
					$sql = "SELECT a.*, p.*, atp.*
							FROM tblArticle as a, tblArticletoPhoto as atp, tblPhoto as p 
							WHERE a.deleted = 0 AND 
								a.groupID = 0 AND
								a.articleID = atp.articleID AND 
								atp.photoID = p.photoID AND 
								a.authorID = $travelerID 
								GROUP BY p.photoID";
			 		$rs->Execute($sql); 
				
					$tmp_holder 			= array();
					$holder 			= array();
				
					if( $rs->Recordcount() ){
							require_once('travellog/model/Class.Article.php');	
							require_once('travellog/model/Class.Photo.php');	
							require_once('travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php');	
							//filter
							while( $row = mysql_fetch_assoc($rs->Resultset()) ){
										$tmp_holder[$row['articleID']][$row['photoID']] = $row;
									
										if(!in_array($row['articleID'],$holder)){			
											
												$articleID = $row['articleID'];
											
												//count photos is journal
												$sql = "SELECT count(tblPhoto.photoID) as cnt_photo FROM tblArticle, tblPhoto, tblArticletoPhoto 
										 				   	WHERE tblArticle.articleID = $articleID
															AND tblArticle.deleted = 0
										 				 	AND tblArticle.articleID = tblArticletoPhoto.articleID 
										 				 	AND tblPhoto.photoID = tblArticletoPhoto.photoID 
										 				 	GROUP BY tblPhoto.photoID";
												$rs2->Execute($sql); 
											
												$article = new Article(0,$row);
												
												$primary = $article->getPrimaryPhotoID();
												
												$current_photos	= $tmp_holder[$row['articleID']];
												if($primary>0){
													$photo = new Photo($article,0,$row);
												}else{
													$random_photos = array_rand($current_photos);
													$photo = new Photo($article,0,$current_photos[$random_photos]);
												}
												$GaCollectionDTO = new GaCollectionDTO();
												$GaCollectionDTO->setGenID($row['articleID']);
												$GaCollectionDTO->setTitle($row['title']);
												$GaCollectionDTO->setContext('article');
												$GaCollectionDTO->setPrimaryPhoto($photo);
												$GaCollectionDTO->setNumPhotos($rs2->Recordcount());
												self::$photos[] = $GaCollectionDTO;
									
										}
						
										$holder[] = $row['articleID'];
									
										//print_r($row['travelID']."<br>");
									
							}
					}
				}
		}
		
		private static $photosLoaded = FALSE;
		private $mCountOnly = FALSE;
		private $mCount = 0;
		function loadCollection($countOnly=FALSE){//if $countOnly=TRUE, it will return the actual number of photos. if FALSE, returns the number of Collection
			$this->mCountOnly = $countOnly;
			if( $this->mCountOnly ){
				$this->doProfile();
				$this->doJournal();
				$this->doAlbum();
				$this->doArticle();
				return $this->mCount;
			}else{
				$key = $this->generateCacheKey();
				$cache	=	ganetCacheProvider::instance()->getCache();
				if ( $cache != null ) {
					if ( $content = $cache->get($key) ){
						self::$photos = $content;
						self::$photosLoaded = TRUE;
					}
				}
				if( !self::$photosLoaded ){
					$this->doProfile();
					$this->doJournal();
					$this->doAlbum();
					$this->doArticle();
					self::$photosLoaded = TRUE;
					
					if ($cache != null) {
						$content = self::$photos;
						$cache->set($key,$content,array('EXPIRE'=>10800));
					}
				}
				return self::$photos;
			}
		}
		
		function loadGallery(){
			
				if(count(self::$photos)>0){
								
						require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
						require_once('travellog/UIComponent/Photo/factory/Class.PhotoLayoutFactory.php');
						
						$PLayout = PhotoLayoutFactory::create(1);	
								
						foreach(self::$photos as $photo){
								$Pfactory = new PhotoFactory($photo->getGenID(),$photo->getContext());
								
								$parentData = array();
								$parentData['context'] 	= $photo->getContext();
								$parentData['loginid'] 		= 0;
								$parentData['genid'] 		= $photo->getGenID();
								$parentData['pID'] 			= 0;
								
								$PLayout->setParentData($parentData);
								$PLayout->loadPhotos($Pfactory);
								break;
						}
						return $PLayout->createLayout()->render();
				}
			
		}
	
		function loadCacheGallery(){
			
				if(count(self::$photos)>0){
								
						require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
						require_once('travellog/UIComponent/Photo/factory/Class.PhotoLayoutFactory.php');
						
						foreach(self::$photos as $photo){
								$Pfactory = new PhotoFactory($photo->getGenID(),$photo->getContext());
								$parentData = array();
								$parentData['context'] 	= $photo->getContext();
								$parentData['loginid'] 		= 0;
								$parentData['genid'] 		= $photo->getGenID();
								$parentData['pID'] 			= 0;
								$parentData['layout'] 		= 1;
								
								require_once("travellog/UIComponent/Photo/command/Class.PhotoCacheTOjsCommand.php");
								PhotoCacheTOjsCommand::doCache($Pfactory,$parentData);
								break;
						}
				}
			
		}
		
		function doAlbum(){
			require_once('travellog/model/Class.Traveler.php');	
			require_once('travellog/UIComponent/Photo/DTO/Class.GaCollectionDTO.php');
			require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");

			$traveler = new Traveler(self::$travelerID);

			$TRAVAlbum	= $traveler->getPhotoAlbums();

			if(count($TRAVAlbum)>0){
				foreach($TRAVAlbum as $album ){
					$photocount = count($album->getPhotos(NULL,$this->mCountOnly));
					if( $this->mCountOnly ){
						$this->mCount += $photocount;
						return;
					}
					if( 0 < $photocount || RequestData::isPrivileged() ){
						$GaCollectionDTO = new GaCollectionDTO();
						$GaCollectionDTO->setGenID($album->getPhotoAlbumID());
						$GaCollectionDTO->setTitle($album->getTitle());
						$GaCollectionDTO->setContext("traveleralbum");
						$GaCollectionDTO->setPrimaryPhoto($album->getPrimaryPhoto());
						$GaCollectionDTO->setNumPhotos($photocount);
						self::$photos[] = $GaCollectionDTO;
					}
				}
			}
		}
		
		function generateCacheKey(){
			require_once("travellog/UIComponent/Collection/model/Class.RequestData.php");
			if( RequestData::isPrivileged() ){
				return "Photo_Collection_Traveler_Owner_".self::$travelerID;
			}else{
				return "Photo_Collection_Traveler_Public_".self::$travelerID;
			}
		}
} 
 
 
 
?>
