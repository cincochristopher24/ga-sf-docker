<?php

/*
 *  IPhotoLayout.php  2007-08-15 02:43:56Z root $
 */

/**
 * IPhotoLayout is used to defined as abstract Photo layout.
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */
interface IPhotoLayout {

    /**
     *
     * @param $filenames array of filenames
     */
    function loadPhotos(PhotoFactory $PhotoFactory);		
	
	/**
	 * Set array of Data where Photo came from
	 */
	function setParentData($info);
	
	
}