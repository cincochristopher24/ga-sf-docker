<?php

/*
 *  IDataServiceProvider.php  2007-08-15 02:43:56Z root $
 */

/**
 * IDataServiceProvider
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */
interface IDataServiceProvider {

	function setData($data);		
	function setLayout($layout);
			
}