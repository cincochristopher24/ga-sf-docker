<?php
/*
 * Created on Oct 4, 2007
 * IPhotoFactory.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
interface IPhotoFactory{
	
	static function create();	
	
} 

?>
