<?php
/*
 * Created on Nov 7, 2007
 * Class.HeaderPhotoObserver.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 
require_once("travellog/UIComponent/Photo/interface/IPhotoObserver.php"); 
 
class HeaderPhotoObserver implements IPhotoObserver{
  
	  public static function onChanged( $sender, $args ){
	  
		  	if($args->getPrimaryPhoto()->getphotoID()){
				if($sender->observeraction == "add"){
					$fulllink = $args->getPrimaryPhoto()->getPhotoLink('jentryheader');
					$div = "<div id='titleprimaryphoto' class='header_img'>";
					$img = '<img id="primaryheader_photo" class="header_photo" src="'.$fulllink.'" border="0">';
					echo $div.$img."</div>";
					
				}else{
					$fulllink = $args->getPrimaryPhoto()->getPhotoLink('jentryheader');
					echo "<script  type='text/javascript'>";
					echo "jQuery('#primaryheader_photo').attr({src:'$fulllink'});";
					echo "</script>";
				}
				
			}else{
				echo "<script  type='text/javascript'>";
				echo "jQuery('#titleprimaryphoto').html('');";
				echo "</script>";
			}
	  	
	  }
}
 
 
?>
