<?php
/*
 * Created on Oct 4, 2007
 * AbstractPhoto.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
abstract class AbstractPhoto{
	
	abstract function setPHOTOcontext($id);
	abstract function getPhotos();
	
} 
 
?>
