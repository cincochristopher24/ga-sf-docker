<?php

class PhotoRotatephoto {
	
	public $observeraction = "rotate";
	
    function PhotoRotatephoto($request){
    	require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
    	require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
    	require_once("travellog/UIComponent/Photo/observer/Class.HeaderPhotoObserver.php"); 
    	require_once('Class.PhotoOnloadgallery.php');
    	
    	$photoID 	= $request['pID'];
		$rotation 	= $request['angle'];
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		PhotoManager::rotate($Pfactory,$photoID,$rotation);
		
		//HeaderPhotoObserver::onChanged($this, PhotoFactory::create()->getPHOTOcontext());
		
		$context = PhotoFactory::create()->getPHOTOcontext();
		
		require_once('travellog/model/Class.Photo.php');	
		$Photo =  new Photo($context,$photoID);
		
		$cachekey = "1_".$photoID;
		
		$fullsize = $Photo->getPhotolink('fullsize');
		//$fullsize 		= "<img  id='cropImage' src='$fullsize' />";
		$thumbnail	= $Photo->getPhotolink('thumbnail');
		
		?>
		
		jQuery("#fullsize").html("<img  id='cropImage' src='<? echo $fullsize; ?>' />");
		jQuery("#previewWrap"+<?= $photoID; ?>).html("<img src='<?= $thumbnail; ?>' />");
		
		GAJsCache['<? echo $cachekey; ?>'] ="<?= $fullsize; ?>";
		GAJsCache['<? echo $cachekey; ?>_thumb'] ="<?= $thumbnail; ?>";
		<?
		
		require_once("travellog/UIComponent/Photo/observer/Class.PhotoEventObserver.php");  
		PhotoEventObserver::onChanged($request, $context);
		
    }


}



?>
