<?php


class PhotoOnloadgallery{

    function PhotoOnloadgallery($request){
		
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once('travellog/UIComponent/Photo/factory/Class.PhotoLayoutFactory.php');
		require_once('travellog/UIComponent/Photo/model/Class.IsOwner.php');
		
		$PLayout = PhotoLayoutFactory::create($request['layout']);
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		
		$parentData = array();
		$parentData['context'] 	= $request['context'];
		$parentData['loginid'] 		= $request['loginid'];
		$parentData['genid'] 		= $request['genid'];
		$parentData['pID'] 			= $request['pID'];
		
		$PLayout->setParentData($parentData);
		//$PLayout->setStartrow($request['startrow']);
		//$PLayout->setPagenum($request['pagenum']);
		$PLayout->loadPhotos($Pfactory);
		
		/*
		echo "<pre>";
			print_r($parentData);
		echo "</pre>";
		*/
		
		echo $PLayout->createLayout()->render();
    	
    	require_once("travellog/UIComponent/Photo/command/Class.PhotoCacheTOjsCommand.php");
		PhotoCacheTOjsCommand::doCache($Pfactory,$request);
    	
    }
}

 
?>
