<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoShowRearrangePhotosRequest.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 class PhotoShowrearrangephotos {

    function PhotoShowrearrangephotos($request){
    	require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once('travellog/UIComponent/Photo/factory/Class.PhotoLayoutFactory.php');
		require_once('travellog/UIComponent/Photo/model/Class.IsOwner.php');
		
		$photos 	= new PhotoFactory($request['genid'],$request['context']);
		
		
		//$isadmin 	= new IsOwner($photos->,$request['loginid'])
		
		
		
		$PLayout = PhotoLayoutFactory::create($request['layout']);
		$PLayout->setIsAdmin($request['isowner']);	
		$PLayout->setParentData(array('context'=>$request['context'],'loginid'=>$request['loginid'],'genid'=>$request['genid'],'pID'=>0));
		$PLayout->loadPhotos($photos);
		//$PLayout->setStartrow($request['startrow']);
		//$PLayout->setPagenum($request['pagenum']);
		
		
		echo $PLayout->createLayout()->renderRearrange();
		
	}
}
 
 
 
 
 
 
?>
