<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/PhotoService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
require_once('travellog/model/Class.Photo.php');
			


class PhotoShowthumbversion{
	
	function PhotoShowthumbversion($request){
		
		$template = new Template();
		$template->set_path('travellog/UIComponent/Photo/views/');
			
		$Pfactory = new PhotoFactory($_REQUEST['genid'],$_REQUEST['context']);
	    $photo 	= new Photo($Pfactory->create()->getPHOTOcontext(),$_REQUEST['photoid']);
		
		if(!file_exists($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."cropthumb".$photo->getFileName())){
			
			require_once('travellog/model/Class.ImageBuilder.php');
			
			$ImgBuilder = ImageBuilder::getInstance();													
			$ImgBuilder->setFilename($photo->getFilename());
			$ImgBuilder->setImageFile($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."fullsize".$photo->getFilename());
			$ImgBuilder->setDestination($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto());
			$ImgBuilder->create('cropthumb',100,100);
			
		}
		
		
		list($cwidth, $cheight) = getimagesize($_SERVER['DOCUMENT_ROOT']."/".$photo->getPathOfPhoto()."cropthumb".$photo->getFilename());
		$imgdimension = array('width'=>$cwidth,'height'=>$cheight);
		
		
		$template->set_vars(array(
						'photoid'	=> $photo->getPhotoID(),
						'context'	=>$_REQUEST['context'],
						'loginid'	=>$_REQUEST['loginid'],
						'genid'		=>$_REQUEST['genid'],
						'layout'	=>$_REQUEST['layout'],
						'isowner'	=>$_REQUEST['isowner'],
						'imgdimension' =>$imgdimension,
						'photo'	=> $photo));
										
		echo $template->fetch('tpl.thumbversion.php');		
		
	
	}
	
}


?>
