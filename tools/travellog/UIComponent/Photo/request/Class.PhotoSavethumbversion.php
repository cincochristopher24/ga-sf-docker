<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


class PhotoSavethumbversion{
	
	function PhotoSavethumbversion($request){
		
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
		require_once('Class.PhotoOnloadgallery.php');
		require_once('Class.PhotoFullview.php');
		require_once('Class.PhotoShowcropafterupload.php');
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		/*
		for($i=0;$i<$request['txtcropcount'];$i++){
			PhotoManager::crop($Pfactory, $request['photoid'.$i], $request['cropWidth'.$i], $request['cropHeight'.$i], $request['cropX'.$i], $request['cropY'.$i]);
		}
		*/
		
		PhotoManager::crop($Pfactory, $request['photoid'], $request['cropWidth'], $request['cropHeight'], $request['cropX'], $request['cropY'], $request['picsize']);
		/*
		echo "<pre>";
			print_r($request);
		echo "</pre>";
		*/
		
		/*
		if(isset($request['pIDs']) )
			return new PhotoShowcropafterupload($request);
		else
			if(isset($request['tofullview']))
				return new PhotoFullview($request);
			else
				return new PhotoOnloadgallery($request);
		*/
		
		if(isset($request['pIDs']) )
			return new PhotoShowcropafterupload($request);
	}	
	
	
}


?>
