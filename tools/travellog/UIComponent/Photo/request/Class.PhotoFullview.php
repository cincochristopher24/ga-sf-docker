<?php


class PhotoFullview {

    function PhotoFullview($request){
    	
    
    	require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once('travellog/UIComponent/Photo/factory/Class.PhotoLayoutFactory.php');
		
		
		//$isadmin 	= new IsOwner($photos->,$request['loginid'])
		
		$PLayout = PhotoLayoutFactory::create($request['layout']);	
		$PLayout->setParentData(array('context'=>$request['context'],'loginid'=>$request['loginid'],'genid'=>$request['genid']));
		$PLayout->setStartrow($request['startrow']);
		$PLayout->setPagenum($request['pagenum']);
		$PLayout->setIsAdmin($request['isowner']);
		$PLayout->loadPhotos(new PhotoFactory($request['genid'],$request['context']));
		echo $PLayout->createLayout()->renderFullSize();
		
	}
}



?>
