<?


class PhotoDeletelogo {
	
	function PhotoDeletelogo($request){
    	
    	
    		
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
		
		//$photoID = $request['pID'];
		
		require_once('travellog/model/Class.GroupFactory.php');
		
		$arrGrupID = array();
		
		$arrGrupID[0] = $request['genid'];
		
		$group		= GroupFactory::instance();
		$grp = $group->create($arrGrupID);
		
		$photoID = $grp[0]->getGroupPhotoID();
		
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		PhotoManager::delete($Pfactory,$photoID);
		
		// Added by Antonio Pepito Cruda Jr. to redirect the page after uploading
		if (isset($_REQUEST['redirect'])) {
			require_once("travellog/model/navigator/Class.BackLink.php");
			$back = new BackLink();
			header("location: ".$back->getBackURL());
		}
	}
}

?>
