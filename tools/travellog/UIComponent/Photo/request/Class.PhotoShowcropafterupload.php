<?php
/*
 * Created on Dec 20, 2007
 * Class.PhotoRequestSHOWCROPAFTERUPLOAD.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

class PhotoShowcropafterupload {
	
	function PhotoShowcropafterupload($request){
			
			require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
			require_once('travellog/model/Class.PathManager.php');
			
			new PhotoFactory($request['genid'],$request['context']);
			
			$path = new PathManager(PhotoFactory::create()->getPHOTOcontext(),'image');
			
			$abspath 			= $_SERVER['DOCUMENT_ROOT']."/".$path->GetPathrelative();
			$imgdimension 		= array();
			$PhotoIDsnFilenames	= array();	
			
			$PhotoIDs = explode(",",$request['pIDs']);
			
			$pIDS =$PhotoIDs;
			
			foreach($PhotoIDs as $photoID){
				$Photo = new Photo(PhotoFactory::create()->getPHOTOcontext(),$photoID);
				list($cwidth, $cheight) = getimagesize($_SERVER['DOCUMENT_ROOT']."/".$Photo->getPathOfPhoto().'fullsize'.$Photo->getFileName());
				$imgdimension[] = array('width'=>$cwidth,'height'=>$cheight);
				$PhotoIDsnFilenames[]	= array("photoID"=>$photoID,"filenames"=>$Photo->getFileName());
			}
			
			
			//check if not primary photo then call loadgallery
			
			
			$Photo = new Photo(PhotoFactory::create()->getPHOTOcontext(),$PhotoIDsnFilenames[0]['photoID']);
			
			if($Photo->getPrimaryPhoto()){
			
					require_once('Class.Template.php');	
					$template = new Template();
					$template->set_path('travellog/UIComponent/Photo/views/');
					
					//if(count($pIDS)>1)
						array_shift($pIDS);
					
					//require_once("travellog/UIComponent/Photo/observer/Class.PhotoEventObserver.php");  
					//PhotoEventObserver::onChanged($request, PhotoFactory::create()->getPHOTOcontext());
					
					$template->set_vars(array(
						'context'	=>$request['context'],
						'loginid'	=>$request['loginid'],
						'genid'		=>$request['genid'],
						'layout'	=>1,
						'isowner'	=>1,
						'imgdimension' =>$imgdimension,
						'PhotoIDsnFilenames'	=>$PhotoIDsnFilenames,
						'path'		=>$path->GetPathrelative(),
						'pIDs'		=> count($pIDS)?implode(",", $pIDS):false
						));
														
					echo $template->fetch('tpl.cropthumbversion.php');	
	
			}else{
					require_once("travellog/UIComponent/Photo/request/Class.PhotoOnloadgallery.php");
					$request['pID'] = 0;
					return new PhotoOnloadgallery($request);
			}
	}
	
}
 
?>
