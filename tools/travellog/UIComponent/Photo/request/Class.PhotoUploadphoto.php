<?php


class PhotoUploadphoto {

    function PhotoUploadphoto($request){
		
		require_once('travellog/controller/Class.PhotoController.php');	
		require_once('Class.Template.php');
			
		$template = new Template();
		$template->set_path('travellog/UIComponent/Photo/views/');
 		
 		require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
 		
 		
 		new PhotoFactory($request['genid'],$request['context']);
 		$photo = PhotoFactory::create()->getPhotos();
 		$NUMPhotosTOupload = PhotoFactory::create()->NUMPHOTOSTOUPLOAD;
 		
		$template->set("context",$request['context']);
 		$template->set("genid",$request['genid']); 	 		
 		$template->set("loginid",$request['loginid']);
 		$template->set("defaultnumberofpic",$NUMPhotosTOupload);
 		$template->set("layout",$request['layout']);
		$template->set("photo",$photo);
		
		
		echo $template->fetch('tpl.Upload.php');
		
		
    }
}



?>
