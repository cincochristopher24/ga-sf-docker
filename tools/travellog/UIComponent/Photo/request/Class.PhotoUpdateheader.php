<?php


class PhotoUpdateheader {

	public $observeraction = "add";
	
    function PhotoUpdateheader($request){
    	
    	require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
		require_once("travellog/UIComponent/Photo/observer/Class.HeaderPhotoObserver.php"); 
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		HeaderPhotoObserver::onChanged($this, PhotoFactory::create()->getPHOTOcontext());
	}
}



?>
