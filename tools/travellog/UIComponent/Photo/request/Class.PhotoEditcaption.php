<?php


class PhotoEditcaption {

    function PhotoEditcaption($request){
    	require_once('travellog/controller/Class.PhotoController.php');	
		require_once('Class.Template.php');
		
		$template = new Template();
		$template->set_path('travellog/UIComponent/Photo/views/');
 		
 		require_once("travellog/model/Class.Photo.php");
 		$photoID = $request['pID'];
 		
 		$Photo = new Photo(NULL,$photoID);
 		
 		$template->set('caption',$Photo->getCaption());
		$template->set("context",$request['context']);
 		$template->set("genid",$request['genid']); 	 		
 		$template->set("loginid",$request['loginid']);
		$template->set("photoid",$photoID);
		$template->set("layout",$request['layout']);
		$template->set("startrow",$request['startrow']);
		$template->set("pagenum",$request['pagenum']);
		$template->set("isowner",$request['isowner']);
		
		
		echo $template->fetch('tpl.EditCaption.php');
		
	}
}



?>
