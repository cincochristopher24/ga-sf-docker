<?php


class PhotoEditRequest {

    function PhotoEditRequest($request){
    	
    	require_once('Class.Template.php');	
		
		$template = new Template();
		$template->set_path('travellog/UIComponent/Photo/views/');
 		
 		$photoID = $request['pID'];
 		
		$template->set("context",$request['context']);
 		$template->set("genid",$request['genid']); 	 		
 		$template->set("loginid",$request['loginid']);
 		$template->set("photoid",$photoID);
 		$template->set("layout",$request['layout']);
	
		echo $template->fetch('tpl.EditPhoto.php');
		
    }
}



?>
