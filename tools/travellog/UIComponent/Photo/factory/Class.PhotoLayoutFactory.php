<?php

/*
 *  ClassPhotoLayoutFactory.php  2007-08-15 02:43:56Z root $
 */

/**
 * Class.PhotoLayoutFactory .
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version $Revision: 
 * @package UIComponent.Photo
 */


class PhotoLayoutFactory{
	
    /**
	 * 
	 * @return PhotoLayout 
	 */
   	public static function create($layoutType){
		
		require_once('travellog/UIComponent/Photo/model/Class.PhotoLayoutType.php');
		
		/**
		 * Which Layout to Display
		 */	
		switch ($layoutType){
			case PhotoLayoutType::$BASIC:
				require_once('travellog/UIComponent/Photo/model/Class.BasicLayout.php');
				return new BasicLayout;
			break;
			case PhotoLayoutType::$THICKBOX:
				require_once('travellog/UIComponent/Photo/model/Class.ThickBoxLayout.php');
				return new ThickBoxLayout;
			break;
			case PhotoLayoutType::$THUMBGRID: //case added by JUL, 2009-01-08
				require_once('travellog/UIComponent/Photo/model/Class.ThumbGridLayout.php');
				return new ThumbGridLayout;
			break;
		}
		
		
	}
		
}