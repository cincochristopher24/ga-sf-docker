<?php
/*
 * Created on Oct 4, 2007
 * Class.PhotoFactory.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
 require_once("travellog/UIComponent/Photo/interface/IPhotoFactory.php");
 
 class PhotoFactory implements IPhotoFactory{
 	
 	static $id 			= NULL;
 	public static $context	= NULL;
 	
 	function PhotoFactory($id,$context){
 		self::$id 		= $id;
 		self::$context 	= $context;
 		return $this;
 	}
 	
 	static function PhotoFactoryPeer($id,$context){
 		self::$id 		= $id;
 		self::$context 	= $context;
 		return new PhotoFactory($id,$context);
 	}
 	
 	function getContext(){
 		return self::$context;
 	}
 	
 	public static function create(){
 		require_once("travellog/UIComponent/Photo/model/Class.Photo".ucfirst(self::$context).".php");
 		$photo = new ReflectionClass("Photo".ucfirst(self::$context));
 		return $photo->newInstance(self::$id);
 	}
 	
 }
?>
