<div id="uppermenu">
	<h1>Set your Profile version</h1>
</div>
<form id="frmcontrols" name="frmcontrols" class="frmCrop">
<div id="border_container">
	<div class="photo_wrapper">
		<h2>Drag and resize the box to adjust.</h2>
			
		
				<input id="txtcropcount" name="txtcropcount" type="hidden" value="1">	
				<input type="hidden" 	name="imageWidth" 		id="imageWidth" 	value="<?=$imgdimension[0]['width']?>" />
		        <input type="hidden"  	name="imageHeight" 	id="imageHeight" 	value="<?=$imgdimension[0]['height']?>" />
		        <input type="hidden"  	name="cropX" 			id="cropX" 		value="0" />
		        <input type="hidden"  	name="cropY" 			id="cropY" 		value="0" />
		        <input type="hidden"  	name="cropWidth" 		id="cropWidth" 	value="<?=$imgdimension[0]['width']?>" />
		        <input type="hidden"  	name="cropHeight" 		id="cropHeight" 	value="<?=$imgdimension[0]['height']?>" />
				<input type="hidden" 	name="photoid" 		id= "photoid"   	value="<?=$PhotoIDsnFilenames[0]['photoID']?>"/>	
				
				
				<?if($pIDs):?>	
				<input type="hidden" name="pIDs" id= "pIDs" value="<?=$pIDs?>" />
				<input type="hidden" name="LASTPic" id= "LASTPic" value="1" />
				<?else: ?>
					<input type="hidden" name="LASTPic" id= "LASTPic" value="0" />
				<?endif;?>
				
				<input type="hidden" name="picsize" id= "picsize" value="profile" />
				
				<input type="hidden"  	name="context" 			id= "context" 		value="<?=$context?>" />
				<input type="hidden"  	name="genid" 			id= "genid" 		value="<?=$genid?>" />
				<input type="hidden"  	name="loginid" 			id= "loginid"   	value="<?=$loginid?>"/>
				<input type="hidden" 	name="layout" 			id= "layout"   		value="<?=$layout?>"/>
				<input type="hidden" 	name="isowner" 			id= "isowner"   	value="<?=$isowner?>"/>
				<input type="hidden" 	name="startrow" 		id= "startrow"  	value="1"/>
				<input type="hidden" 	name="pagenum" 			id= "pagenum"   	value="1"/>
				<input type="hidden"  	name="action"  			id= "action"   		value="saveprofileversion"/>
				
				<input type="hidden" id="cropcount" name="cropcount" value="1">
				
				<!--
				<div id="generated_thumbnail">
					<div id="previewWrap"></div>
					<p>Preview</p>
				</div>
				-->
				
				<div id="fullview">
					<div class="clear"></div>
					<div class="photocontainer">
						
					 	<div id="bigthumb" style="height:<?=$imgdimension[0]['height']+10?>px;">
					 		<div id="fullsize">
								<img width="<?=$imgdimension[0]['width']?>" height="<?=$imgdimension[0]['height']?>"  src="/<?=$path.'fullsize'.$PhotoIDsnFilenames[0]['filenames']?>" id="cropImage">
							</div>
						</div> <div class="clear"></div>
					</div>
				</div>
	</div>
</div>

<div class="clear"></div>
<br/>
<div style="text-align:right;padding: 0;margin: 0 0 10px 0; width:510px;" id="cropcontrols" class="crop_controls">
	<input class="btn" type="button" name="btncancel" value="Cancel" onclick="PhotoService.cancelAfterUploadCrop()">			
	<input class="btn" type="button" name="btnsave" value="Save Profile version" onclick="PhotoService.saveprofileversion()">
</div>
<!-- cropping proccess container -->
<div style="text-align:center;background-color:#aaa;padding:10px 0;margin: 0; font-size: 14px; font-weight: bold;width:520px;display:none;" id="cropsuccess">
															
</div>
						

						<div id="loading" style="display:none;padding:3px;">
							    	<img src="/images/loading.gif" />
						</div>

				
			
			
</form>
	




	<script type="text/javascript">
				
			<?$dim = $imgdimension[0]['width']>$imgdimension[0]['height']?$imgdimension[0]['height']:$imgdimension[0]['width'] ?>
			
			PhotoService.curCrop = 
				new Cropper.ImgWithPreview( 
					'cropImage',
					{ 	
						previewWrap: 'photo',
						minWidth: 120, 
						minHeight: 120,
						ratioDim: { x: <?=$dim?>, y: <?=$dim?> },
						displayOnInit: true,
						onEndCrop: function( coords, dimensions ) {
										$('cropX').value = coords.x1;
										$('cropY').value = coords.y1;
										$('cropWidth').value = dimensions.width;
										$('cropHeight').value = dimensions.height;
										
									}
						
					} 
				)
						
		
	</script>
	
	<div style="visibility:hidden; position: absolute; top: 0; left: 0; z-index: -1;" id="hdcrop">
		<img src="/<?=$path.'fullsize'.$PhotoIDsnFilenames[0]['filenames']?>" id="hiddencropImage">
	</div>
