


<script>
	
	//jQuery("#lboxgallerycontainer").trigger("myEvent");
	
</script>
	<form>
		    <input type="hidden" value="<?=$photocount?>" id="hdlightboxpcount" name="hdlightboxpcount">
	</form>


<?if($photocount >0):?>			
	
	
		<div class="content2">			
			<!--<div class="mini_photo_title">Journal Photos</div>-->
			
			<!-- This element id is Required-->
			<div id="gallery_small">
		
				<?if($photocount > 1 || $isOwner):?>
					<div class="small_container">	
					
						<!-- Carousel Structure : 
				        	 Warning: all elements inside this structure is required to run the carousel 
				        -->
								<div id="th_mycarousel"  class="carousel-component" style="width:223px;">
									
									<div class="carousel-prev">
										<img id="THprev-arrow" class="left-button-image" src="/js/yahooUI/carousel/arrow_a_active.gif" alt="Previous Button">
									</div>
									
										
									<div class="carousel-next">
										<img id="THnext-arrow" class="right-button-image" src="/js/yahooUI/carousel/arrow_b_active.gif" alt="Next Button">
									</div>
									
									<div class="carousel-clip-region">
										<ul class="carousel-list carousel-horizontal">
											
											<? foreach($photos as $x => $photo): ?>
												 <li id="th_mycarousel-item-<?= $x+1; ?>" style="margin:3px;">
													
														<a href="javascript:void(0)" onclick="PhotoService.viewfullsize('tbfullsize',4,<?=$photo->getPhotoID();?>,0,0)">
															<img src="<?=$photo->getPhotolink('thumbnail');?>" border="1"/>
														</a>
													
												</li>
											<?endforeach;?>		
											
													
										</ul>
									</div>
									
								</div>
						<!--  End Carousel Structure -->
							
					</div>
				
				<?endif;?>
				
					<div class="clear"></div>
					
					<!-- Required Container-->
					<div class="photofloatb">
						<div id="tbfullview">
						 	<div id="bigthumb">
						 		<div id="tbfullsize">	
									<a id="jentrylboxheader" href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(<?=$currentPhoto->getPhotoID();?>,'gallery')">
										<img id="thickbox_jentry" src="<?=$currentPhoto->getPhotolink('jentrylboxheader');?>" />
									</a>
								</div>
							</div> 
							<?if($isOwner):?>
								<a class="manage_option" href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(<?=$currentPhoto->getPhotoID();?>,'gallery')">
									Manage Photo<?=$photocount >1?"s":''?>
								</a>
							<?endif;?>
						</div>
						
					</div>
					<!-- end Required Container-->
			</div>
		</div>    
		
		<?if($photocount > 1 || $isOwner):?>
			<script type="text/javascript">
							
				var THhandlePrevButtonState = function(type, args) {
					var enabling = args[0];
					var THYUIleftImage = args[1];
					if(enabling) {
						THYUIleftImage.src = "/js/yahooUI/carousel/arrow_a_active.gif";	
					} else {
						THYUIleftImage.src = "/js/yahooUI/carousel/arrow_a_notactive.gif";	
					}
					
				};
				var THhandleNextButtonState = function(type, args) {
					var enabling = args[0];
					var THYUIrightImage = args[1];
					
					if(enabling) {
						THYUIrightImage.src = "/js/yahooUI/carousel/arrow_b_active.gif";
					} else {
						THYUIrightImage.src = "/js/yahooUI/carousel/arrow_b_notactive.gif";
					}
					
				};
				
				var THnumVis = <?= $photocount<4?$photocount:3;?>; 
				
				THYUIcarousel = new YAHOO.extension.Carousel("th_mycarousel", 
						{
							numVisible:        THnumVis,
							animationSpeed:    0.15,
							revealAmount:	   0,	
							scrollInc:         3,
							navMargin:         5,
							prevElement:     "THprev-arrow",
							nextElement:     "THnext-arrow",
							size:              <?= $photocount ;?>,
							prevButtonStateHandler:   THhandlePrevButtonState,
							nextButtonStateHandler:   THhandleNextButtonState
						}
				);
				
			</script>

		<? endif;?>
		         
<?endif;?>

<?if($photocount == 0):?>
	<?if($isOwner):?>
		<div class="content2">
			<!--<div class="mini_photo_title">Journal Photos</div>-->
			<div class="clear"></div>
			<p>
				There are no photos yet for this entry.<br/>
				<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(0)">
		      	Upload Photos!
	      		</a>
	 		</p>
		</div>
	<?endif;?>
<?endif;?> 	

