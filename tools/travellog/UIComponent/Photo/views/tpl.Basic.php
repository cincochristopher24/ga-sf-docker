<?	$pos = 0;
	$startPos = 0;
	$defaultPos = 0;
	$photoLinks = "";
	$photoIDs = "";
	$currPhotoID = 0;
	$photoPrimaries = "";
	$isTravelPrimaries = "";
	$temp = 0;
	//cache Current FullImage
	if(count($currentPhoto)>0){
		
		$fullsize_source = $currentPhoto->getPhotolink('fullsize'); 
		
		echo "<script>";
				echo "var F_images=new Array();";
	    		echo "F_images['full']=new Image(500,500);";
				echo "F_images['full'].src='$fullsize_source';";
	    echo "</script>";
	}

?>

<div id="PhotocallbackContainer">

	<form>
		    <input type="hidden" value="<?=$photocount?>" id="hdbasicpcount" name="hdbasicpcount">
	</form>

	
	
<?if($photocount):?>
		
			
		<?if($isOwner):?>
	    <div class="optioncontainer" id="uppermenu"> 
	    	<!-- tabloading container -->
	    	<div id="tabloading" style="text-align:center">
				
			</div>
	    	<ul>
	    	<li>
				<a class="upload" href="javascript:void(0)" onclick="PhotoService.uploadphoto('_<? echo $parentData['context']; ?>')">
					<span>Upload Photos</span>
				</a>
			</li>
	    	
	    	
	    	<!--
	    	<? if($parentData['context'] == "travelerjournalentries" || $parentData['context'] == "travelergroup"):?>
	    	
		    	<a onclick="PhotoService.showuploadmenu('<?=  $parentData['context'] ?>')" class="upload" href="javascript:void(0)" >
					Upload Photos
				</a>
	    	
	    	<? else: ?>
	    		<a class="upload" href="javascript:void(0)" onclick="PhotoService.uploadphoto()">
					Upload Photos
				</a> 
	    	
	    	<? endif;?>
	    	-->
	    	
	    	
	    	<?if($photocount >1):?>
			<li>
		    	<a class="arrange" href="javascript:void(0)" onclick="PhotoService.showrearrangephotos('_<? echo $parentData['context']; ?>')">
					<span>Rearrange Photos</span>
				</a> 
			</li>
			<?endif;?>
			<div class="clear"></div>

		</div>
		<?endif;?>
			
			
			<!-- This element id is Required-->
			<div id="photogallery_<? echo $parentData['context']; ?>">
					
							
					<?if($photocount > 1 || $isOwner):?>
					<div class="photocontrol">
				          <!--div class="photoborderup"></div-->
				          <div class="photoimagebox">
				    	        <div class="photoimagecontainer">
									<?	if( (isset($_REQUEST["type"]) && isset($_REQUEST["ID"]) && isset($_REQUEST["context"])) || (isset($_REQUEST["action"]) && $_REQUEST["action"] != "onloadgallery") || isset($_REQUEST["manage"]) ): ?>
					            	<!-- Carousel Structure : 
					            		 Warning: all elements inside this structure are required to run the carousel 
					            	-->
										<div id="BA_mycarousel_<? echo $parentData['context']; ?>"  class="carousel-component">
											
											<div class="carousel-prev">
												<img id="BA_prev-arrow" class="left-button-image" src="/images/prev6_active.jpg" alt="Previous Button">
											</div>
											
												
											<div class="carousel-next">
												<img id="BA_next-arrow" class="right-button-image" src="/images/next6_active.jpg" alt="Next Button">
											</div>
											
											<div class="carousel-clip-region">
													<ul class="carousel-list carousel-horizontal">
														<? foreach($photos as $x => $photo): ?>
															<?
																$is_travelprimaryphoto = $photo->getPhotoId() ==$current_travelprimaryphoto?1:0;
																if( $currentPhoto->getPhotoID() == $photo->getPhotoID() ){
																	$startPos = $pos;
																}else{
																	$pos++;
																}
																$photoIDs .= $photo->getPhotoID();
																if( $temp < count($photos)-1 ){
																	$photoIDs .= "$";
																}
																$temp++;
															?>
															<li id="BA_mycarousel_<? echo $parentData['context']; ?>-item-<?= $x+1; ?>">
																<a href="javascript:void(0)" id="thumb<?=$photo->getPhotoID();?>" onclick="PhotoService.viewfullsize('fullsize',1,<?=$photo->getPhotoID();?>,<?= $photo->getPrimaryPhoto(); ?>,<?= $is_travelprimaryphoto;?>)">
																	<div id="previewWrap<?=$photo->getPhotoID();?>" > 
																		<img src="<?=$photo->getPhotolink('thumbnail');?>" border="1"/>
																	</div>
																</a>
															</li>
														<?endforeach;?>		
													</ul>
											</div>
											
										</div>
									<!--  End Carousel Structure -->
									<?	else: ?>

											<? if( $photocount > 6): ?>
												<div id="carousel_prev" class="carousel-prev">
													<img id="BA_prev-arrow" class="left-button-image" src="/images/prev6_active.jpg" alt="Previous Button" />
												</div>
											<? endif;?>
										
										<div id="BA_mycarousel_<? echo $parentData['context']; ?>"  <? if(!isset($_REQUEST["type"])): ?> style="display:none;"<? endif; ?> class="carousel-area">		
										

										
											<ul class="carousel-list">
												<? foreach($photos as $x => $photo): ?>
													<?
														$is_travelprimaryphoto = $photo->getPhotoId() ==$current_travelprimaryphoto?1:0;
														if( $currentPhoto->getPhotoID() == $photo->getPhotoID() ){
															$defaultPos = $pos; //$pos;
															$currPhotoID = $currentPhoto->getPhotoID();
														}else{
															$pos++;
														}
														$photoLinks .= $photo->getPhotolink('fullsize');
														$photoIDs .= $photo->getPhotoID();
														$photoPrimaries .= $photo->getPrimaryPhoto();
														$isTravelPrimaries .= $is_travelprimaryphoto;
														if( $temp < count($photos)-1 ){
															$photoLinks .= "$";
															$photoIDs .= "$";
															$photoPrimaries .= "$";
															$isTravelPrimaries .= "$";
														}
														$temp++;
													?>
													<li id="BA_mycarousel_<? echo $parentData['context']; ?>-item-<?= $x+1; ?>" height="65" width="65">
														<a href="javascript:void(0)" id="thumb<?=$photo->getPhotoID();?>" onclick="PhotoService.viewfullsize('fullsize',1,<?=$photo->getPhotoID();?>,<?= $photo->getPrimaryPhoto(); ?>,<?= $is_travelprimaryphoto;?>);">
															<div id="previewWrap<?=$photo->getPhotoID();?>" > 
																<img height="65" width="65" src="<?=$photo->getPhotolink('thumbnail');?>" border="1"/>
															</div>
														</a>
													</li>
												<?endforeach;?>
												<? $startPos = ((int)($defaultPos / 6)) * 6;?>	
											</ul>

										
										</div>

									
											<? if( $photocount > 6): ?>
												<div id="carousel_next" class="carousel-next">
													<img id="BA_next-arrow" class="right-button-image" src="/images/next6_active.jpg" alt="Next Button" />
												</div>
											<? endif; ?>
									
									<?	endif; ?>
							    </div>
					             
				          </div>
				      
				          <!--div class="photoborderdown"></div-->
					
					</div>
					<?endif;?>
					
					<!-- This element id is Required-->
					<div id="fullview">
					
						<div class="clear"></div>
						
						<?if($photocount < 2):?>
							<?if($isOwner):?>
							<?else:?>
								<div class="tabarea">
							<?endif;?>
						<?endif;?>
						
						<!-- Required Container-->
						<div class="photocontainer">
						 	<div id="bigthumb">
						 		<div id="fullsize">
									<div id="image_container" class="image_container">
									<?	if( 1 < $photocount && !(isset($_REQUEST["type"]) && isset($_REQUEST["ID"]) && isset($_REQUEST["context"])) && (!isset($_REQUEST["action"]) || $_REQUEST["action"] == "onloadgallery") && !isset($_REQUEST["manage"]) ): ?>
									<div id="navhover" class="navhover">
											<a id="prev_photo" class="prev_photo<?=(0==$defaultPos)? ' p_disabled' : ''?>" href="javascript:void(0)" onclick="PhotoService.scrollToPrevPhoto(); return false;"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>
											<a id="next_photo" class="next_photo<?=(($photocount-1)==$defaultPos)? ' p_disabled' : ''?>" href="javascript:void(0)" onclick="PhotoService.scrollToNextPhoto(); return false;"><span class="txtblock">Next<span class="imgblock"></span></span></span></a>
									</div>		
									<?	endif; ?>
									<img id="cropImage" src="<?=$currentPhoto->getPhotolink('fullsize');?>"/>
									</div>
								</div>
							</div>
						</div>
						
						<!-- Cache element Container -->  
						<div id="cache_fullview"></div>
						<!-- End Cache element Container -->  
						
						<?if($photocount < 2):?>
							<?if($isOwner):?>
							<?else:?>
								</div>
							<?endif;?>
						<?endif;?>
						<!-- end Required-->
						
						
						<div style="text-align:center;background-color:#aaa;padding:10px 0;display:none;margin: 0 0 10px 0;" id="cropcontrols">
										
									<input type="button" name="btn_save_crop" id="btn_save_crop" value="Cancel" onclick="PhotoService.cancelCROPPING(<?=$currentPhoto->getPhotoID();?>)" >
									<input type="button" name="btn_save_crop" value="Save" onclick="PhotoService.savethumbnailversion('true')">
											
						</div>
						
						<!-- cropping proccess container -->
							<div style="text-align:center;background-color:#aaa;padding:10px 0;display:none;margin: 0 0 10px 0; font-size: 14px; font-weight: bold;" id="cropsuccess">
									
							</div>
						<!-- end cropping -->
						
						<!-- This element id is Required-->
						<div id="controlcontainer">
													
										<div class="phototitle">
										  	<p><em><span id="caption_area"><?=$currentPhoto->getCaption();?></span></em></p>
										</div>
									
						      		<?if($isOwner):?>
						    					
						    					<div id="optionprimary">
								    					<? if($parentData['context'] != "travelerjournalentries" && $parentData['context'] != "travelergroup"): ?>
								    					
													    		<?if($parentData['context'] != "travellog"): ?>
												    						<?if(!$currentPhoto->getPrimaryPhoto()): ?>		
																		    		<div class="optionprimary">
																							<span>Set as primary photo for </span>
																							<select id="primary" onchange="PhotoService.setprimary(<?=$currentPhoto->getPhotoID();?>)">
																									<option value="0">--select--</option>
																									<?if(!$currentPhoto->getPrimaryPhoto()):?>
																											<option value="1">
																												<? if($parentData['context'] == 'profile'):?>
																													my profile 
																												<? elseif($parentData['context'] == 'photoalbum'):?>
																													this album 
																												<? endif; ?>
																											</option>
																									<?endif;?>
																							</select>
																					</div>
																			<?endif;?>
																<? else:?>
																			
																			<?if(!$currentPhoto->getPrimaryPhoto() || !$travelprimaryphoto): ?>															
																		    		
																		    		<div class="optionprimary">
																						<span>Set as primary photo for </span>
																						<select id="primary" onchange="PhotoService.setprimary(<?=$currentPhoto->getPhotoID();?>)">
																								<option value="0">--select--</option>
																								<?if(!$currentPhoto->getPrimaryPhoto()):?>
																										<option value="1">
																												this entry 
																										</option>
																								<?endif;?>
																								<?if(!$travelprimaryphoto):?> 
																										<option value="2">this journal</option>
																								<?endif;?>	
																						</select>
																					</div>
																					
																			<?endif;?>
																<?endif;?>
																
																
																
																
																
																
														<?endif;?>
												
												</div>
												
								    			<div id="optioncontainer" class="optioncontainer"> 
														<ul>
															<li>
																<a href="javascript:void(0)" id="ctr_rotate" onclick="PhotoService.rotatephoto(<?=$currentPhoto->getPhotoID();?>,'left')">
															      		<span>Rotate</span>
																</a>
															</li> 
															<li>
															  	<a href="javascript:void(0)"  id="ctr_editcaption" onclick="PhotoService.editcaption(<?=$currentPhoto->getPhotoID();?>)">
															  			<span>Add/Edit photo Caption</span>
															  	</a>
														    </li>		
														    
														    <? if($parentData['context'] == "profile" && $currentPhoto->getPrimaryPhoto()): ?>
																	    <li>
																	    	<a href="javascript:void(0)"  id="ctr_thumbnailversion" onclick="PhotoService.execprofileversion(<?=$currentPhoto->getPhotoID()?>)">
																				      	<span>Set Profile version</span>
																			</a>
																		</li>
															<? else: ?>
															
																		 <li>
																	    	<a href="javascript:void(0)"  id="ctr_thumbnailversion" onclick="PhotoService.execthumbnailversion(<?=$currentPhoto->getPhotoID()?>)">
																				      	<span>Set Thumbnail</span>
																			</a>
																		</li>
																		
															<? endif; ?>
															
															<li>
																<a href="javascript:void(0)" id="ctr_delete" onclick="PhotoService.deletephoto(<?=$currentPhoto->getPhotoID()?>)">
														      			<span>Delete</span>
														    	</a>
														    </li> 
														</ul>
												</div>
												
												<!-- Required container--> 
												<div id="cache_optioncontainer"> 
												
												</div>
												 <!--end Required -->
												 
												<!-- Required container--> 	    
											    <div id="loading" style="display:none;padding:3px;">
												    	<img src="/images/loading.gif" />
												</div>
											    <!--end Required -->
											     
											    <div id="actionscontrol" style="display:none">
												
														<form id="frmcontrols" name="frmcontrols" class="frmCrop">
															
															<div id="crop_control" style="display:none">															
																	<input type="hidden"		name="txtcropcount"	id="txtcropcount" 	value="1">
																	<input type="hidden"  	name="cropX" 				id="cropX" 				value="0" />
															        <input type="hidden"  	name="cropY" 				id="cropY" 				value="0" />
															        <input type="hidden"  	name="cropWidth" 		id="cropWidth" 		value="0" />
															        <input type="hidden"  	name="cropHeight" 	id="cropHeight" 		value="0" />
																	<input type="hidden"		name="tofullview" 		id="tofullview" 			value="1">
																	
																	
																	<input type="hidden" name="LASTPic" id= "LASTPic" value="1" />
																	<!--<input type="hidden" name="picsize" id= "picsize" value="thumbnail" />-->
																	
																	
																	<? if($parentData['context'] == "profile" && $currentPhoto->getPrimaryPhoto()): ?>
																	    	<input type="hidden" name="picsize" id= "picsize" value="profile" />
																	<?else :?>	
																			<input type="hidden" name="picsize" id= "picsize" value="thumbnail" />
																	<? endif; ?>
																		
															</div>
															
															<input type="hidden" name="action"  	id= "action"   		value=""/>
															
															
															
															<input type="hidden"  	name="context" 			id= "context" 			value="<?=$parentData['context'];?>" />
															<input type="hidden"  	name="genid" 				id= "genid" 				value="<?=$parentData['genid'];?>" />
															<input type="hidden"  	name="loginid" 			id= "loginid"   			value="<?=$parentData['loginid'];?>"/>
															<input type="hidden" 		name="layout" 			id= "layout"   			value="1"/>
															<input type="hidden" 		name="isowner" 			id= "isowner"   			value="1"/>
															<input type="hidden" 		name="startrow" 			id= "startrow"  			value="<?=isset($start)? $start+1 : 1 ;?>"/>
															<input type="hidden" 		name="pagenum" 		id= "pagenum"   		value="<?=isset($start)? $start+1 : 1 ;?>"/>
															<input type="hidden" 		name="photoid" 			id= "photoid"   			value="<?=$currentPhoto->getPhotoID()?>"/>
															
															<div id="caption_control" style="display:none">
																	<div class="title">
																			Add/Edit Caption
																	</div>
																	<p> 
																	<textarea  id="txtcaption" name="txtcaption" style="height: 60px; width: 498px;" maxlength="250" onkeyup="PhotoService.captionChanged();" onchange="PhotoService.captionChanged();"></textarea>
																	</p>
																	
																	<table style="padding-left:10px;">
																		<tr>
																			<td>
																				<input type="text" id="caption_counter" readonly="true" value="250" size="3" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				Remaining characters
																			</td>
																		</tr>
																	</table>
																		
																	<p style="text-align:right;">
																		<input type="button" name="btncancel" value="Cancel" onclick="jQuery('#actionscontrol').slideUp('slow')">
																		<input type="button" name="btnsave" value="Save" onclick="PhotoService.savecaption()">
																	</p>  		
															</div>
														
														</form>
													    			
												</div>
						    
						    
						    		<?endif;?>
						  			   	
						</div>
						<!--end Required -->
						
						
						<div class="clear"></div>
						<!-- START: Edited By: Cheryl Ivy Q. Go  :: Please don't change, tricky! -->
						<!--
					    <div class="photocomment comments"> 
					    	<a href="javascript:void(0)" id="viewcomments" onclick="Comment.viewcomments(2, <?=$currentPhoto->getPhotoID();?>, 'photo', <?=$parentData['genid']?>,<?=$parentData['loginid']?>)">
						    	<span id="tooglecomments">PHOTO GREETINGS, SALUTATIONS AND COMMENTS </span>
						    	<span id="tooglecomments" style="display: none"></span>
					    	</a>
							
					    	<div id="commentscontainer" style="display:none;"></div>
					    </div>
						-->
						<!-- END: Edited By: Cheryl Ivy Q. Go  :: Please don't change, tricky! -->
					</div>
			</div>
			
			<div id="upload_control_<? echo $parentData['context']; ?>" style="display:none;">
								
					<?$tmp_sid = md5(uniqid(mt_rand(), true));?>

					<div id="uploadcallback" style="float:left;"></div> 
					<div id="process_container">
					<div id="upload_container">
						<div class="wrapper">
							<div id="uploadnew">
								<span>Upload your Photos</span>
								<p>Select image files from your computer and upload it on your entries.</p>
							</div>
					
							<div id="uploadrules">
								<p>
									- You can upload up to a total size of 2MB/image. <br />
									- The following image file formats are allowed: jpg, gif, png. 
								</p>
							</div>
						
							<? for($x=0; $x<5; $x++): ?>					
								<div class="file_upload">
									<?=$x+1; ?><input onchange="PhotoService.checkpfile()"  type="file" id ="upfile_<?=$x?>" name="upfile_<?=$x?>" size="30" />
								</div>
							<? endfor; ?>
							</div>
						</div>
					</div>	
					<div id="hdvar">
						<input type="hidden" name="txtcat" 			value="<?=$parentData['context'];?>"/>
						<input type="hidden" name="txtaction" 		value="addphoto"/>
						<input type="hidden" name="txtgenid" 		value="<?=$parentData['genid'];?>"/>
						<input type="hidden" name="txtid"			value="<?=$parentData['loginid'];?>-0-1"/>
						<input type="hidden" name="txtscriptname" 	value="/ajaxpages/PhotoUILayout.php"/>
					</div>		
			
					<div class="controls" id="ccontrols">
						<input type="button" align="right" name="cancel" value="Cancel" onclick="jQuery('#upload_control_<? echo $parentData['context']; ?>').css({display:'none'});jQuery('#photogallery_<? echo $parentData['context']; ?>').css({display:'block'});" />
						<input type="button"  align="right" id="upload" name="upload" value="Upload" onclick="return PhotoService.ajaxFileUpload('<?=$tmp_sid?>','new')" />
					</div>
					<div class="clear"></div>
					<!-- Upload loading proccess container -->
					<div style="text-align:center;background-color:#666;margin-top:10px;margin-bottom: 0; display:none;padding: 10px 0 10px 0;color:#fff;font-size: 14px; font-weight:bold; clear:both;" id="uploadprocess">
						Uploading photos, Please Wait...								
					</div>
			</div>
			
			<div id="arrange_control_<? echo $parentData['context']; ?>" style="display:none;">
			
			</div>
			
			
			<div style="text-align:center;background-color:#666;margin-top:10px;margin-bottom: 0; display:none;padding: 10px 0;color:#fff;font-size: 14px; font-weight:bold; clear:both;" id="loadingmeter">
						Loading Please Wait...								
		  	</div>	
			
			<?if($photocount > 1 || $isOwner):?>
				<? $undefined = FALSE; ?>
				<script type="text/javascript"> 
					if( 'undefined' == typeof(PhotoService) ){
						<? $undefined = TRUE; ?>
					}
				</script>
				<? if( $undefined ): ?>
					<script type="text/javascript" src="/js/PhotoUILayout.js"> </script>
				<? endif; ?>
				<script type="text/javascript">
						
						var photo_captions = new PhotoCaptions();
						photo_captions.setPhotoIDs('<?=$photoIDs?>');
						photo_captions.setCurrentKey(<?=$currentPhoto->getPhotoID()?>);
						
						<?	if( (isset($_REQUEST["type"]) && isset($_REQUEST["ID"]) && isset($_REQUEST["context"])) || (isset($_REQUEST["action"]) && $_REQUEST["action"] != "onloadgallery") || isset($_REQUEST["manage"])): ?>
							
							var BA_handlePrevButtonState = function(type, args) {
									var enabling = args[0];
									var YUIleftImage = args[1];
									if(enabling) {
										YUIleftImage.src = "/images/prev6_active.jpg";	
									} else {
										YUIleftImage.src = "/images/prev6_inactive.jpg";	
									}
									
								};
								var BA_handleNextButtonState = function(type, args) {
									var enabling = args[0];
									var YUIrightImage = args[1];
									
									if(enabling) {
										YUIrightImage.src = "/images/next6_active.jpg";
									} else {
										YUIrightImage.src = "/images/next6_inactive.jpg";
									}
									
								};
								
								
								var numVis = <?= $photocount<7?$photocount:6;?>;  
								
								
								BAYUIcarousel = new YAHOO.extension.Carousel("BA_mycarousel_<? echo $parentData['context']; ?>", 
										{
											numVisible:        numVis,
											animationSpeed:    0.15,
											revealAmount:	   0,	
											scrollInc:         6,
											navMargin:         22,
											prevElement:     "BA_prev-arrow",
											nextElement:     "BA_next-arrow",
											size:              <?= $photocount;?>,
											prevButtonStateHandler:   BA_handlePrevButtonState,
											nextButtonStateHandler:   BA_handleNextButtonState
										}
								);
								
							<?	else: ?>	
								
								if( 'undefined' == typeof(PhotoService) ){
									jQuery.noConflict();
									PhotoService = new mPhotoService("<?=$parentData['context']?>",<?=$parentData["loginid"]?>,<?=$parentData["genid"]?>);
									PhotoService.setLayout(1);
								}
												
								PhotoService.pCount = <?=$photocount?>;
								PhotoService.curPos = <?=$defaultPos?>;
								PhotoService.setPhotoArray('<?=$photoLinks?>','<?=$photoIDs?>','<?=$photoPrimaries?>','<?=$isTravelPrimaries?>');
								PhotoService.curStrip = <?=floor($startPos/6)+1?>;
														
								jQuery(document).ready(function() {
									<? if( !isset($_REQUEST['type']) ): ?>
										jQuery(".carousel-area").show();
									<? endif; ?>
																		
								    jQuery(".carousel-area").jCarouselLite({
								        btnNext: ".carousel-next",
								        btnPrev: ".carousel-prev",
										circular: false,
										visible: 6,
										start: <?=$startPos?>,
										scroll: 6
								    });	
									PhotoService.resetNavOnPhoto();
									if( <?=$startPos?> < 6 ){
										jQuery("#carousel_prev").addClass("carousel-disabled");
									}
									if( PhotoService.curStrip == Math.floor(<?=$photocount?>/6)+1 ){
										jQuery("#carousel_next").addClass("carousel-disabled");
									}
									
									jQuery("#cropImage").bind("load",
										function(){
											if( null != jQuery("#navhover") ){
												PhotoService.resetNavOnPhoto();
											}
										}
									);
								});
								
							<?	endif; ?>
							
				</script>
				
			<? endif;?>
					

			
			

<?endif;?>

<?if($photocount < 1):?>
	
	<?if($isOwner):?>
		
		
		<? if($PhotoCONTEXT->getPHOTOcontext()): ?>
			<!-- Required container--> 	 
		  		<div id="photogallery">
					
					<div id="loading" style="display:none;padding:3px;float:left;">
					    	<img src="/images/loading.gif" />
					</div>
					
					<div id="actionscontrol" style="display:none;height:40px;background-color:#354A55;width:70%;">
					  	
					</div>
			  		
			  		<script type="text/javascript">
			      		//PhotoService = new mPhotoService('<?= $parentData['context']; ?>', <?=$parentData['loginid'];?>, <?=$parentData['genid'];?>);
						//PhotoService.uploadphoto();
			      	</script>
		      		
		      		<?$tmp_sid = md5(uniqid(mt_rand(), true));?>

					<div id="uploadcallback" style="float:left;"></div> 
					<div id="process_container">
					<div id="upload_container">
						<div class="wrapper">
							<div id="uploadnew">
								<span>Upload your Photos</span>
								<p>Select image files from your computer and upload it on your entries.</p>
							</div>
					
							<div id="uploadrules">
								<p>
									- You can upload up to a total size of 2MB/image. <br />
									- The following image file formats are allowed: jpg, gif, png. 
								</p>
							</div>
						
							<? for($x=0; $x<5; $x++): ?>					
								<div class="file_upload">
									<?=$x+1; ?><input onchange="PhotoService.checkpfile()"  type="file" id ="upfile_<?=$x?>" name="upfile_<?=$x?>" size="30" />
								</div>
							<? endfor; ?>
							</div>
						</div>
					</div>	
					<div id="hdvar">
						<input type="hidden" name="txtcat" 			value="<?=$parentData['context'];?>" />
						<input type="hidden" name="txtaction" 		value="addphoto" />
						<input type="hidden" name="txtgenid" 		value="<?=$parentData['genid'];?>" />
						<input type="hidden" name="txtid"			value="<?=$parentData['loginid'];?>-0-1" />
						<input type="hidden" name="txtscriptname" 	value="/ajaxpages/PhotoUILayout.php" />
					</div>		
			
					<div class="controls" id="ccontrols">
						<!--<input type="button" align="right" name="cancel" value="Cancel" onclick="tb_remove()" />-->
						<input type="button"  align="right" id="upload" name="upload" value="Upload" onclick="return PhotoService.ajaxFileUpload('<?=$tmp_sid?>','new')" />
					</div>
					<div class="clear"></div>
					<!-- Upload loading proccess container -->
					<div style="text-align:center;background-color:#666;margin-top:10px;margin-bottom: 0; display:none;padding: 10px 0 10px 0;color:#fff;font-size: 14px; font-weight:bold" id="uploadprocess">
						Uploading photos, please wait...								
					</div>	
		      			
		      			
		      		
		    </div>
		    <!--end Required --> 
		    
		   
		    
	    <? endif; ?>
	  
	  	<? if(!$PhotoCONTEXT->getPHOTOcontext()): ?>  
    			
    			<p id="no_photo_info" class="no_photo_info">
    				
    				<? if($parentData['context'] == "travelerjournalentries"):?>
    						You have not written any journals yet. 
    							<a href="/travel.php?action=add">
    								Create your first journal. »
    							</a>
    				<? endif; ?>
    				<? if($parentData['context'] == "travelergroup"):?>
    						<span>You have not joined nor created groups/clubs yet</span>.<br /> Check out the <a href="/group.php">groups/clubs on GoAbroad Network</a> or <a href="/group.php?mode=add">create your own club</a>!.
    				<? endif; ?>
    			</p>
    	
    	<? endif; ?>
    	
    <?endif;?>

<?endif;?>

</div>




