<?php
/*
 * Created on Sep 28, 2007
 *
 */
 
 
 require_once("travellog/UIComponent/Photo/interface/IPhotoCommand.php");
 
 class PhotoTravelLastUpdatedCommand implements IPhotoCommand{
 	
 	function onCommand(PhotoManager $PManager, $args = array() ){
	
		if (get_class($PManager->getContext()) != 'TravelLog' ) return false;
        	
	    	require_once('Date.php'); //PEAR Date Package
	    	require_once('travellog/model/Class.Travel.php');  
			$pearDate = new Date();	
			$date_now = $pearDate->getDate(1);
			
			$travel = new Travel($PManager->getContext()->getTravelID());
			$travel->setLastUpdated($date_now);
			$travel->Update();
    		
    		return true;
  	}
 	
 	
 }
 
?>
