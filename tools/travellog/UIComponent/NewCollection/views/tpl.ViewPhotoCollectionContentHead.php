<h1>
	<span id="travelTitle"><?=htmlspecialchars(stripslashes(strip_tags($title)),ENT_QUOTES)?> </span>
	<div class="photoaction_top">
		
		<?	if( 0 < count($photos) ): ?>
			<div id="view_mode_control">
				<input id="btnGalleryView" type="button" class="button_v3" value="Switch to Gallery View" onclick="collectionControl.loadGalleryView(); return false;"/>
				<input id="btnThumbnailView" type="button" class="button_v3" value="Switch to Thumbnails" onclick="collectionControl.toThumbnailView(); return false;" style="display: none;"/>
			</div>
		<?	endif; ?>
		
	</div>
</h1>