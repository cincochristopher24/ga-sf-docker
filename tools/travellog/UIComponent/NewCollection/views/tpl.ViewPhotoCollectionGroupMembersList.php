<ul class="memberlist">
	<?php
		require_once("travellog/UIComponent/NewCollection/views/Class.ViewPhotoCollectionGroupMembersListItem.php");
		$ctr = 0;
		foreach($members as $member){
			$memberListItem = new ViewPhotoCollectionGroupMembersListItem();
			$memberListItem->setMember($member);
			$memberListItem->setGroupID($groupID);
			$memberListItem->setOrderInList($ctr++);
			$memberListItem->render();
		}
	?>	
</ul>

<? if ( $paging->getTotalPages() > 1 ):?>
	<? $start = $paging->getStartRow() + 1; 
	   $end = $paging->getEndRow(); 
	?>
	<? $paging->showPagination() ?>
<? endif; ?>