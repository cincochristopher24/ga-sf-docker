<h1>
	<span id="travelTitle"><?=$title?></span>
</h1>
<div class="notification middle">
	<?php if(PhotoCollectionConstants::PROFILE_CONTEXT==$context): ?>
		<h2>Start adding profile photos</h2>
	<?php else: ?>
		<h2>Start adding photos into this <?if(PhotoCollectionConstants::JOURNAL_CONTEXT==$context):?>journal<?elseif(PhotoCollectionConstants::TRAVELLOG_CONTEXT==$context):?>journal entry<?endif;?> album</h2>
	<?php endif;?>
	<p>
		<?php if(PhotoCollectionConstants::ALBUM_CONTEXT==$context && PhotoCollectionConstants::OWNER < $userLevel ): ?>
			You can upload the photos on your desktop or you might want to link photo from your members.
		<?php else: ?>
			<?php if(PhotoCollectionConstants::JOURNAL_CONTEXT==$context ): ?>
				<br />Select a journal entry 
				<select id="cboTravelLogs2">
				<? foreach($travellogs as $tlog): ?>
					<option value="<?=$tlog->getTravelLogID()?>"><?=truncateText(htmlspecialchars(stripslashes($tlog->getTitle()),ENT_QUOTES),40)?></option>
				<?	endforeach; ?>
				</select>
				<br />
				<script type="text/javascript">
					jQuery("#album_menu").hide();
				</script>
			<?php endif; ?>
			Click on the button below to activate the upload utility.
		<?php endif; ?>
	</p>
	<form action="">
		<input class="button_v3" type="button" value="Add Photos" onclick="collectionControl.loadUploader2('travellog',(document.getElementById('cboTravelLogs2').options[document.getElementById('cboTravelLogs2').selectedIndex]).value); return false;"/>
		<?php if( PhotoCollectionConstants::ALBUM_CONTEXT==$context && PhotoCollectionConstants::OWNER < $userLevel ): ?>
			<input class="button_v3" type="button" value="Add Member Photos" onclick="window.location.replace('/collection.php?type=<?=$type?>&amp;ID=<?=$ID?>&amp;context=<?=$context?>&amp;genID=<?=$genID?>&amp;action=6');"/>
		<?php endif; ?>
	</form>
</div>