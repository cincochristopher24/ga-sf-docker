<?php

	require_once("Class.Template.php");
	
	class ViewPhotoCollectionGroupMembersList{
		
		private $mPaging = NULL;
		private $mIterator = NULL;
		private $mGroup = NULL;
		private $mMembers = array();
		
		function setPaging($pagingObject=NULL,$iterator=NULL){
			$this->mPaging = $pagingObject;
			$this->mIterator = $iterator;
		}
		
		function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		
		function setMembers($members){
			$this->mMembers = $members;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("groupID",$this->mGroup->getGroupID());
			$tpl->set("members",$this->mMembers);
			$tpl->set("paging",$this->mPaging);
			$tpl->set("iterator",$this->mIterator);
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionGroupMembersList.php");
		}
	}