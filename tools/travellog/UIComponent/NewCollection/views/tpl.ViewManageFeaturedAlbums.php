<div class="quick_create">
	<h2>Manage Featured Albums <img src="/images/load_gray.gif" id="updateManageFeaturedAlbumsViewStatus" style="display: none;"/></h2>
	
	<?php if( $isSubGroup ): ?>
		<div class="notification">You can feature up to 3 photo albums on your <strong><?=$groupName." "?></strong> home page.
	<?php else: ?>
		<div class="notification">You can feature up to 3 photo albums on your home page. 
	<?php endif; ?>
	<?php if( 0 == $albumCount ): ?>
		<?php if( $isSubGroup ):?>
			To get started, add your first photo album using the box above.
		<?php else: ?>
			To get started, simply add photo albums.
		<?php endif; ?>
	</div>
	<?php else: ?>
		You currently have <strong><?=$albumCount?> <?=1<$albumCount? "Albums" : "Album"?></strong>.
	</div>
		<form id="frmManageFeaturedAlbums">
			<?php
				if( 0 < count($featuredAlbums) || 0 < count($unfeaturedAlbums) ){
					require_once("travellog/UIComponent/NewCollection/views/Class.ViewManageFeaturedAlbumList.php");
				}
				if( 0 < count($featuredAlbums) ){
					$featuredListView = new ViewManageFeaturedAlbumList();
					$featuredListView->setAlbums($featuredAlbums);
					$featuredListView->setIsFeatured(TRUE);
					$featuredListView->render();
				}
				if( 0 < count($unfeaturedAlbums) ){
					$unfeaturedListView = new ViewManageFeaturedAlbumList();
					$unfeaturedListView->setAlbums($unfeaturedAlbums);
					$unfeaturedListView->setIsFeatured(FALSE);
					$unfeaturedListView->render();
				}
			?>
		</form>
		<div id="editFeaturedAlbumsControl" style="display: none;">				
			<input type="button" id="btnSaveFeaturedAlbums" name="btnSaveFeaturedAlbums" value="Save Featured Albums" class="button_v3" onclick="collectionControl.saveManageFeaturedAlbums(); return false;">
			<input type="button" id="btnCancelEditFeaturedAlbums" name="btnCancelEditFeaturedAlbums" value="Cancel" class="button_v3" onclick="collectionControl.cancelManageFeaturedAlbums(); return false;">
		</div>
	<?php endif; ?>
</div>