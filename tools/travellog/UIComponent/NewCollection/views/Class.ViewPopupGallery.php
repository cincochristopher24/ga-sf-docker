<?php
	require_once("Class.Template.php");

	class ViewPopupGallery{
		
		private $mDefaultPhoto = NULL;
		private $mPhotos = array();
		private $mCollectionDTO = NULL;
		private $mFbLike = NULL;
		
		function setDefaultPhoto($photo=NULL){
			$this->mDefaultPhoto = $photo;
		}
		
		function setPhotos($photos=array()){
			$this->mPhotos = $photos;
		}
		
		function setCollectionDTO($collectionDTO=NULL){
			$this->mCollectionDTO = $collectionDTO;
		}
		
		function setFbLikeView($fbLike){
			$this->mFbLike = $fbLike;
		}
		
		function render(){
			$photoContext = $this->mCollectionDTO->getContextObject();
			$content = new Template();
			$content->set("context",$this->mCollectionDTO->getContext());
			$content->set("genID",$this->mCollectionDTO->getGenID());
			$content->set("defaultPhoto",$this->mDefaultPhoto);
			$content->set("photos",$this->mPhotos);
			$content->set("title",htmlspecialchars(stripslashes($this->mCollectionDTO->getTitle()),ENT_QUOTES));
			$content->set("fbLike",$this->mFbLike);
			$content->out("travellog/UIComponent/NewCollection/views/tpl.ViewPopupGallery.php");
		}
	}