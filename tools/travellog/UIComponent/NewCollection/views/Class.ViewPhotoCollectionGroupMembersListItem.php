<?php

	require_once("Class.Template.php");
	
	class ViewPhotoCollectionGroupMembersListItem{
		
		private $mMember = NULl;
		private $mGroupID = 0;
		private $mOrderInList = 0;
		
		function setMember($member=NULL){
			$this->mMember = $member;
		}
		
		function setGroupID($groupID=0){
			$this->mGroupID = $groupID;
		}
		
		function setOrderInList($order=0){
			$this->mOrderInList = $order;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("member",$this->mMember);
			$tpl->set("photocount",$this->mMember->countPhotosToGrabByGroup($this->mGroupID));
			$tpl->set("nextLine", 0 == ($this->mOrderInList % 3) );
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionGroupMembersListItem.php");
		}
	}