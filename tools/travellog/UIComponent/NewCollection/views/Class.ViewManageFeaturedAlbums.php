<?php

	require_once("Class.Template.php");
	
	class ViewManageFeaturedAlbums{
		
		private $mGroup = NULL;
		
		function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		
		function render(){
			$content = new Template();
			$content->set("featuredAlbums",$this->mGroup->getFeaturedPhotoAlbums());
			$content->set("unfeaturedAlbums",$this->mGroup->getFeaturedPhotoAlbums($featured=FALSE));
			$content->set("albumCount",$this->mGroup->getPhotoAlbumCount());
			$content->set("isSubGroup",0<$this->mGroup->getParentID());
			$content->set("groupName",htmlspecialchars(stripslashes($this->mGroup->getName()),ENT_QUOTES));
			$content->out("travellog/UIComponent/NewCollection/views/tpl.ViewManageFeaturedAlbums.php");
		}
	}