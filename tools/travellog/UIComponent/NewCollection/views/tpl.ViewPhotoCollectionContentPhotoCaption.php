<div class="comment_info" id="comment_info">
		<?php if( $isPrivileged ): ?>
			<div id="caption_control" style="display:none;">
				<textarea style="width:709px;height:45px;" id="txtCaption" name="txtCaption" onchange="collectionControl.captionChanged(this); return false;" onkeyup="collectionControl.captionChanged(this); return false;"></textarea>
				<span id="remaining_chars"></span>
				<input type="button" value="Cancel" id="btnCancelEditCaption" class="button_v3" onclick="collectionControl.cancelEditCaption(); return false;" />
				<input type="button" value="Save" id="btnSaveCaption" class="button_v3" onclick="collectionControl.saveCaption(); return false;" />
			</div>
		<?php endif; ?>
		
		<p id="caption">						
		</p>

		<?php if( $isPrivileged ): ?>
			<input type="button" value="Edit Caption" id="btnEditCaption" onclick="collectionControl.editCaption(); return false;" class="button_v3" style="display:none;" />
		<?php endif; ?>
		
		<div class="comment_alt">
			<?php
				if( PhotoCollectionConstants::PROFILE_CONTEXT == $context ){
					$icon = "/images/profilealbum.gif";
				}else if( PhotoCollectionConstants::ALBUM_CONTEXT == $context || PhotoCollectionConstants::TRAVALBUM_CONTEXT == $context ){
					$icon = "/images/photoalbum.gif";
				}else{
					$icon = "/images/journal_icon2.gif";
				}
			?>
			<img class="book_open" id="book_icon" alt="journal" src="<?=$icon?>" style="display: none;"/>
			<a class="journal_title" id="journal_link" title="" href="">
				<strong><span id="context_title"></span></strong>
			</a>
			<img id="loading_photo_" src="/images/loading_small.gif" align="center" style="display:none;"/>
		</div>
</div>