<div id="group_member_photos">
	<div class="media_control">
		<p id="travelTitle" class="jLeft">
			<strong>Adding Member Photos to</strong> <em><?=$title?></em>
		</p>
		<input class="button_v3" type="button" onclick="jQuery('#group_member_photos').hide(); jQuery('#group_member_photos').remove(); jQuery('#group_members').show();" value="Back to Member List"/>
		<input class="button_v3" type="button" onclick="window.location.replace(collectionControl.getActionPage());" value="Back to Album"/>
	</div>
	<div class="viewer content">
		<div class="member_control uploaded">
		<img id="photo_grabber_requesting" src="/images/loading_small.gif" style="display:none;" />
		<div class="photoaction_top jLeft" id="add_photo_control" style="display:none;">
			<input class="buttonLarge" type="button" value="Add this photo" onclick="collectionControl.addMemberPhoto(); return false;"/>
		</div>
		<div class="notifications" id="add_photo_help_text">
			<strong>Click on a photo to view and/or select it</strong>
		</div>
			<div class="author">
				<div class="link">
					<strong><?=$member->getFullName()?></strong>
					<div id="lnkGalleryView">	
						<input class="buttonMini" type="button" onclick="collectionControl.loadMemberPhotoGalleryView(); return false;" value="Switch to Gallery View"/>
					</div>
					<div id="lnkThumbnailView" style="display:none;">	
						<input class="buttonMini" type="button" onclick="collectionControl.switchToThumbnailView(); return false;" value="Switch to Thumbnail View"/>
					</div>
				</div>
				<a href="<?=$member->getFriendlyURL()?>" >
					<img width="37" height="37" src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" title="Go to Profile" alt="" class="pic"/>
				</a>
						
			</div>
		</div>		
		<div id="thumbnail_view" class="clear">
			<ul class="photo_list">
				<?php $photoID_array = array(); ?>
				<?php foreach($photos as $x => $photo): ?>	
					<li alt="<?=$x?>" id="thumbnail_<?=$photo->getPhotoID()?>">
						<a href="" onclick="collectionControl.loadMemberPhotoGalleryView(<?=$photo->getPhotoID()?>); return false;">
							<img id="imgThumbnail_<?=$photo->getPhotoID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES)?>" width="65" height="65" src="<?=$photo->getPhotoLink('thumbnail')?>"/>
						</a>
						<input type="hidden" id="caption_<?=$photo->getPhotoID()?>" value="<?=htmlspecialchars($photo->getCaption(),ENT_QUOTES)?>" />
						<input type="hidden" id="fullsize_link_<?=$photo->getPhotoID()?>" value="<?=htmlspecialchars($photo->getPhotoLink('fullsize'))?>"/>								

						<?php if( PhotoCollectionConstants::TRAVELLOG == $photo->getPhototypeID() ): ?>
							<input type="hidden" id="phototype_<?=$photo->getPhotoID()?>" value="entry" />
							<input type="hidden" id="jlink_<?=$photo->getPhotoID()?>" value="<?=$photo->getContext()->getFriendlyURL()?>"/>
							<input type="hidden" id="jtitle_<?=$photo->getPhotoID()?>" value="<?=htmlspecialchars(stripslashes(strip_tags($photo->getContext()->getTitle())),ENT_QUOTES)?>" />
						<?php elseif( PhotoCollectionConstants::PHOTOALBUM == $photo->getPhototypeID() ): ?>
							<?php
								$album = $photo->getContext();
								if( 0 < $album->getGroupID() ){
									$link = "/collection.php?type=group&ID=".$album->getGroupID()."&context=photoalbum&genID=".$album->getPhotoAlbumID();
								}else{
									$link = "/collection.php?type=traveler&ID=".$album->getCreator()."&context=traveleralbum&genID=".$album->getPhotoAlbumID();
								}
							?>
							<input type="hidden" id="phototype_<?=$photo->getPhotoID()?>" value="album" />
							<input type="hidden" id="jlink_<?=$photo->getPhotoID()?>" value="<?=$link?>"/>
							<input type="hidden" id="jtitle_<?=$photo->getPhotoID()?>" value="<?=htmlspecialchars(stripslashes(strip_tags($photo->getContext()->getTitle())),ENT_QUOTES)?>" />
						<?php endif; ?>

						<?php if( in_array($photo->getPhotoID(),$existing) ): ?>
							<input type="hidden" id="grabbed_<?=$photo->getPhotoID()?>" value="1">
						<?php else: ?>
							<input type="hidden" id="grabbed_<?=$photo->getPhotoID()?>" value="0">
						<?php endif; ?>
					</li>
					<?php $photoID_array[] = $photo->getPhotoID(); ?>
				<?php endforeach; ?>
			</ul>
		</div>
		
		<div id="gallery_view" style="display:none;" class="viewer content clear">
			<div id="photo_grab_notice" style="display:none;">
			</div>
			<div class="viewed_image">
				<?php if( 1 < count($photos) ): ?>
					<div class="viewed_navi">
						<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
						<p name="photo_position_marker" class="jLeft"></p>
						<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
					</div>
				<?php endif; ?>
				<div class="viewed_holder">
					<div class="navhover" id="navhover">
							<a class="prev_photo" id="prev_photo" onclick="collectionControl.movePrevMemberPhoto(); return false;" href="javascript:void(0)"><span class="txtblock"><span class="imgblock"></span>Prev</span></a>
							<a class="next_photo" id="next_photo" onclick="collectionControl.moveNextMemberPhoto(); return false;" href="javascript:void(0)"><span class="txtblock">Next<span class="imgblock"></span></span></a>
					</div>
					<img id="active_photo" src="" align="middle" border="0" />
				</div>
				<?php if( 1 < count($photos) ): ?>
					<div class="viewed_navi">
						<a name="prev_button" href="" onclick="jQuery('#prev_photo').click(); return false;" class="jLeft button_v3"><strong>prev</strong></a>
						<p name="photo_position_marker" class="jLeft"></p>
						<a name="next_button" href="" onclick="jQuery('#next_photo').click(); return false;" class="jLeft button_v3"><strong>next</strong></a>
					</div>
				<?php endif; ?>	
			</div>
		</div>
	</div>

		<div class="comment_info" id="comment_info">
			<p id="caption">						
			</p>
			<div class="comment_alt" style="display:none;">
				<?php
					if( count($photos) && is_object($photos[0]) && PhotoCollectionConstants::TRAVELLOG == $photos[0]->getPhototypeID() ){
						$icon = "/images/journal_icon2.gif";
					}else{
						$icon = "/images/photoalbum.gif";
					}
				?>
				<img class="book_open" id="book_icon" alt="journal" src="<?=$icon?>" style="display: none;"/>
				<a class="journal_title" id="journal_link" title="" href="">
					<strong><span id="context_title"></span></strong>
				</a>
				<img id="loading_photo_" src="/images/loading_small.gif" align="center" style="display:none;"/>
			</div>
		</div>
		
		<script type="text/javascript">
			tempCollection = new MemberPhotoCollection();
			tempCollection.setPhotoID_array('<?=implode(",",$photoID_array)?>');
			collectionControl.setMemberPhotoCollection(tempCollection);
			tempCollection = null;
		</script>		
</div>