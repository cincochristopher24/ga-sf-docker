<li <?if($nextLine):?>style="clear: left;"<?endif;?> >
	<span class="member">
		<?	if(0<$photocount): ?>
			<a href="javascript:void(0)" onclick="collectionControl.viewMemberPhotos(<?=$member->getTravelerID()?>); return false;">
				<img width="65" height="65" src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" title="View Member Photos"/>
			</a>
		<?	else: ?>
			<a href="<?=$member->getFriendlyURL()?>" >
				<img width="65" height="65" src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" title="Go to Profile"/>
			</a>
		<?	endif; ?>
	</span>
	<h3>
		<?	if(0<$photocount): ?>
			<a href="javascript:void(0)" onclick="collectionControl.viewMemberPhotos(<?=$member->getTravelerID()?>); return false;"><?=$member->getFullName()?></a>
		<?	else: ?>
			<?=$member->getFullName()?>
		<?	endif;?>
	</h3>
	<p>
		<?=(0==$photocount)? "" : ((1<$photocount)? $photocount." Photos " : $photocount." Photo ")  ?>
		<?php if( 0 < $photocount ): ?>
			<img id="member_photos_loading_<?=$member->getTravelerID()?>" style="display:none;" src="/images/loading_small.gif" alt="load_stat" width="16" height="16" />
		<?php endif; ?>
	</p>
</li>