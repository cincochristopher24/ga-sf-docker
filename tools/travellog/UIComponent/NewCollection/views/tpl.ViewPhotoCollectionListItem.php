<?php
	$context = $collectionItem->getContext();
	$href = $collectionItem->getLink();
	$title = stripslashes(strip_tags($collectionItem->getTitle()));
	$title = 10 < strlen($title) ? substr($title,0,10)."..." : $title;
	$title = htmlspecialchars($title,ENT_QUOTES);
	$photocount = $collectionItem->getPhotoCount();
?>
<li id="collection_item_<?=$context?>_<?=$collectionItem->getGenID()?>" name="collection_item" <? if($isActive):?>class="active"<?endif;?>>
	<div style="height: 77px;">
		<span class="album_name">
			<?php if($isActive): ?>
				<img id="imgPrimary_<?=$context?>_<?=$collectionItem->getGenID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($collectionItem->getTitle())),ENT_QUOTES)?>" width="65" height="65" src="<?=$primary->getPhotoLink('thumbnail')?>"/>
			<?php else: ?>
				<a href="<?=$href?>">
					<img id="imgPrimary_<?=$context?>_<?=$collectionItem->getGenID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($collectionItem->getTitle())),ENT_QUOTES)?>" width="65" height="65" src="<?=$primary->getPhotoLink('thumbnail')?>"/>
				</a>
			<?php endif; ?>
		</span>
		<h3>
			<strong id="album_name_<?=$context?>_<?=$collectionItem->getGenID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($collectionItem->getTitle())),ENT_QUOTES)?>" >
				<?php if($isActive): ?>
						<?=$title?>
				<?php else: ?>
					<a id="album_title_<?=$context?>_<?=$collectionItem->getGenID()?>" href="<?=$href?>" >
							<?=$title?>
					</a>
				<?php endif; ?>
			</strong>
		
			<?php if( 0 < $userLevel && (PhotoCollectionConstants::TRAVALBUM_CONTEXT==$context || PhotoCollectionConstants::ALBUM_CONTEXT==$context) ): ?>
				<span id="albumNameForm_<?=$context?>_<?=$collectionItem->getGenID()?>" style="display:none;">
					<form id="frmAlbumName<?=$context?>_<?=$collectionItem->getGenID()?>" onsubmit="collectionControl.saveAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" >
						<input type="text" id="txtAlbumName_<?=$context?>_<?=$collectionItem->getGenID()?>" size="12" value="<?=htmlspecialchars(stripslashes($collectionItem->getTitle()),ENT_QUOTES)?>" />
						<input type="button" id="btnSaveAlbumName" value="Save" onclick="collectionControl.saveAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" />
						<input type="button" id="btnCancelAlbumName" value="Cancel" onclick="collectionControl.cancelEditAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;" />
						<input type="hidden" id="curAlbumName_<?=$context?>_<?=$collectionItem->getGenID()?>" value="<?=htmlspecialchars(stripslashes($collectionItem->getTitle()),ENT_QUOTES)?>" />
					</form>
				</span>
			<?php endif; ?>
		</h3>
		<span id="album_info_<?=$context?>_<?=$collectionItem->getGenID()?>" class="album_control">
					<?php if( 0 < $userLevel && (PhotoCollectionConstants::TRAVALBUM_CONTEXT==$context || PhotoCollectionConstants::ALBUM_CONTEXT==$context) ): ?>
							<a href="javascript:void(0)" onclick="collectionControl.editAlbumName('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;">Edit Title</a>&nbsp;
							<a href="javascript:void(0)" onclick="collectionControl.deleteAlbum('<?=$context?>',<?=$collectionItem->getGenID()?>); return false;">Delete Album</a>
					<?php endif; ?>
					<?php if( PhotoCollectionConstants::JOURNAL_CONTEXT==$context ): 
						$travel = $collectionItem->getContextObject();
						$travellog = $travel->getMostRecentTravellog(FALSE);
					?>
						<a href="<?=$travellog->getFriendlyURL()?>">Go to Journal</a>
					<?php elseif ( PhotoCollectionConstants::ARTICLE_CONTEXT == $context ): ?>
						<a href="/article.php?action=view&articleID=<?=$collectionItem->getGenID()?>">Go to Article</a>
					<?php endif;?>
		</span>
	
		<p id="album_photocount_<?=$context?>_<?=$collectionItem->getGenID()?>" style="width:100px;overflow:hidden;">
			<?=(0 < $photocount)? ((1<$photocount)? $photocount." Photos" : $photocount." Photo") : '' ?>
		</p>
	</div>
</li>