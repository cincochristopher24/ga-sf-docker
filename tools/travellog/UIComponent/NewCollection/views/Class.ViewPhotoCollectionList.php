<?php
	require_once("Class.Template.php");
	require_once("travellog/UIComponent/NewCollection/helper/Class.PhotoCollectionHelper.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/views/Class.ViewPhotoCollectionListItem.php");
	
	class ViewPhotoCollectionList implements PhotoCollectionConstants{
		
		private $mCollectionHelper = NULL;
		
		function setCollectionHelper($collectionHelper=NULL){
			$this->mCollectionHelper = $collectionHelper;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("userLevel",$this->mCollectionHelper->getUserLevel());
			$tpl->set("genID",$this->mCollectionHelper->getGenID());
			$tpl->set("action",$this->mCollectionHelper->getAction());
			$tpl->set("type",$this->mCollectionHelper->getType());
			$tpl->set("ID",$this->mCollectionHelper->getID());
			$tpl->set("context",$this->mCollectionHelper->getContext());
			$tpl->set("collectionDTO",$this->mCollectionHelper->getCollection());
			$tpl->set("activeCollectionItem",$this->mCollectionHelper->getActiveCollectionItem());
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionList.php");
		
			$this->setupJS();
		}
		
		function setupJS(){
			$tpl = new Template();
			$tpl->set("hasActiveItem",!is_null($this->mCollectionHelper->getGenID()) || self::VIEWER == $this->mCollectionHelper->getUserLevel() );
			$tpl->set("ownerType",$this->mCollectionHelper->getType());
			$tpl->set("ownerID",$this->mCollectionHelper->getID());
			$tpl->set("action",$this->mCollectionHelper->getAction());
			$tpl->set("activeCollectionItem",$this->mCollectionHelper->getActiveCollectionItem());
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentSetupJS.php");
		}
		
	}