<?php

	require_once("travellog/views/Class.AbstractView.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	class ViewThumbGrid extends AbstractView{

		private $mParams = array();

		public function __construct($request=array()){
			$default = array(	"context"	=>	"travellog",
								"loginid"	=>	0,
								"genid"		=>	0,
								"pID"		=>	0	); 
			$this->mParams = array_merge($default,$request);
			if( isset($this->mParams["group_id"]) ){
				$this->mParams["ownerLogged"] = ($this->mParams["group_id"] == $this->mParams["ownerid"] && 0 != $this->mParams["loginid"])? TRUE : FALSE;
				if( $this->mParams["ownerLogged"] ){
					try{
						$file_factory = FileFactory::getInstance();
						$file_factory->registerClass('GroupFactory');
						$tempGroups = $file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($this->mParams["group_id"]));
						$group = $tempGroups[0];
						if( $group instanceof AdminGroup){
							$this->mParams["ownerLogged"] = $group->getAdministratorID() == $this->mParams["loginid"] || $group->isStaff($this->mParams["loginid"]) ? TRUE : FALSE;
						}
					}catch(exception $e){}
				}
			}else{
				$this->mParams["ownerLogged"] = ($this->mParams["loginid"] == $this->mParams["travelerid"] && $this->mParams["ownerid"] == $this->mParams["loginid"])? TRUE : FALSE;
			}
		}

		public function render(){
			require_once("travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionDTOFactory.php");
			$collectionDTO = PhotoCollectionDTOFactory::getInstance()->create($this->mParams["context"]);
			$collectionDTO->setGenID($this->mParams["genid"]);
			$photos = $collectionDTO->getContextObject()->getPhotos();
			
			$tpl = new Template();
			$tpl->set("photos",$photos);
			$tpl->set("photocount",count($photos));
			$tpl->set("params",$this->mParams);
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewThumbGrid.php");
		}
	
	}