<?php
	require_once("Class.Template.php");

	class ViewPhotoAlbumItem{
		private $mAlbum = NULL;
		private $mItemNumber = 1;
		
		function setAlbum($album=NULL){
			$this->mAlbum = $album;
		}
		function setItemNumber($number=1){
			$this->mItemNumber = $number;
		}
		function render(){
			if( $this->mAlbum instanceof PhotoAlbum ){
				$tpl = new Template();
				$tpl->set("itemNumber",$this->mItemNumber);
				$tpl->set("album",$this->mAlbum);
				$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoAlbumItem.php");
			}
		}
	}