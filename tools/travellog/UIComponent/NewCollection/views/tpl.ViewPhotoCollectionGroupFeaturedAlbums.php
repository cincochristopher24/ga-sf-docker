<div id="manageFeaturedAlbumsView">
	<div class="quick_create">
		<h2>Manage Featured Albums</h2>
		<div class="notification">
			Loading albums to manage... <img src="/images/load_gray.gif" />
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		collectionControl.updateManageFeaturedAlbumsView();
	});
</script>