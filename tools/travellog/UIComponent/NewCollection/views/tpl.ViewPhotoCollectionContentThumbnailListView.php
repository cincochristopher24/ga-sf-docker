<div id="thumbnail_view">
	<ul class="photo_list">
		<?php foreach($photos as $x => $photo): ?>	
			<li alt="<?=$x?>" id="thumbnail_<?=$photo->getPhotoID()?>" <?if(PhotoCollectionConstants::JOURNAL_CONTEXT==$context):?>name="context_thumbnail_<?=$photo->getContext()->getTravellogID()?>"<?else:?>name="context_thumbnail_<?=$collectionItem->getGenID()?>"<?endif;?> >
				<?php if (PhotoCollectionConstants::GROUP_TEMPLATE_CONTEXT==$context): ?>
					<?php $photo_image_renderer = new GanetPhotoImageRendererComponent($photo, GanetPhotoService::SIZE_THUMBNAIL); ?>
					<?php $photo_image_renderer->set('attributes', array('width'=>'65', 'height'=>'65', 'id'=>'imgPrimary_'.$photo->getID(), 'title'=>htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES))); ?>
					<a href="#" onclick="collectionControl.loadGalleryView(<?=$photo->getPhotoID()?>); return false;">
						<?php $photo_image_renderer->render(); ?>
					</a>				
				<?php else: ?>
					<a href="#" onclick="collectionControl.loadGalleryView(<?=$photo->getPhotoID()?>); return false;">
						<img id="imgThumbnail_<?=$photo->getPhotoID()?>" title="<?=htmlspecialchars(stripslashes(strip_tags($photo->getCaption())),ENT_QUOTES)?>" width="65" height="65" src="<?=$photo->getPhotoLink('thumbnail')?>"/>
					</a>
				<?php endif; ?>
				<input type="hidden" id="caption_<?=$photo->getPhotoID()?>" value="<?=htmlspecialchars(stripslashes($photo->getCaption()),ENT_QUOTES)?>" />
				<input type="hidden" id="fullsize_link_<?=$photo->getPhotoID()?>" value="<?=htmlspecialchars($photo->getPhotoLink('fullsize'))?>"/>								
				
				<?php if( PhotoCollectionConstants::ALBUM_CONTEXT==$context 
						&& PhotoCollectionConstants::GROUP_TYPE == $collectionItem->getType() 
						&& (PhotoCollectionConstants::PHOTOALBUM != $photo->getPhototypeID() 
							|| $collectionItem->getContextObject()->isGrabbedPhoto($photo->getPhotoID())) ): ?>
							
					<input type="hidden" id="grabbed_<?=$photo->getPhotoID()?>" value="1">
					
					<?php if( PhotoCollectionConstants::TRAVELLOG == $photo->getPhototypeID() ): ?>
						<input type="hidden" id="phototype_<?=$photo->getPhotoID()?>" value="entry" />
						<input type="hidden" id="jlink_<?=$photo->getPhotoID()?>" value="<?=$photo->getContext()->getFriendlyURL()?>"/>
						<input type="hidden" id="jtitle_<?=$photo->getPhotoID()?>" value="<?=htmlspecialchars(stripslashes(strip_tags($photo->getContext()->getTitle())),ENT_QUOTES)?>" />
					<?php elseif( PhotoCollectionConstants::PHOTOALBUM == $photo->getPhototypeID() ): ?>
						<?php
							$album = $photo->getContext();
							if( 0 < $album->getGroupID() ){
								$link = "/collection.php?type=group&ID=".$album->getGroupID()."&context=photoalbum&genID=".$album->getPhotoAlbumID();
							}else{
								$link = "/collection.php?type=traveler&ID=".$album->getCreator()."&context=traveleralbum&genID=".$album->getPhotoAlbumID();
							}
						?>
						<input type="hidden" id="phototype_<?=$photo->getPhotoID()?>" value="album" />
						<input type="hidden" id="jlink_<?=$photo->getPhotoID()?>" value="<?=$link?>"/>
						<input type="hidden" id="jtitle_<?=$photo->getPhotoID()?>" value="<?=htmlspecialchars(stripslashes(strip_tags($photo->getContext()->getTitle())),ENT_QUOTES)?>" />
					<?php endif; ?>
				
				<?php elseif( PhotoCollectionConstants::ALBUM_CONTEXT==$context && PhotoCollectionConstants::GROUP_TYPE == $collectionItem->getType() ): ?>
					<input type="hidden" id="grabbed_<?=$photo->getPhotoID()?>" value="0">
				<?php endif; ?>
				
			</li>
		<?php endforeach; ?>
	</ul>
</div>

<?php if( PhotoCollectionConstants::VIEWER < $userLevel && PhotoCollectionConstants::JOURNAL_CONTEXT==$context ): ?>
	<div id="empty_collection_notice" style="display:none;">
		<div class="notification middle">
			<h2>This journal entry doesn't have photo yet.</h2>
			<p>
				Click on the Add Photos button above to upload photos.
			</p>
		</div>
	</div>
<?php elseif(PhotoCollectionConstants::VIEWER < $userLevel): ?>
	<div id="empty_collection_notice" style="display:none;">
		<div class="notification middle">
			<h2>You have deleted all the photos in this collection.</h2>
			<p>
				Click on the Add Photos button above to upload photos.
			</p>
		</div>
	</div>
<?php endif; ?>