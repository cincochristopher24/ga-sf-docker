<?php if( 0 < count($albums) ): ?>
	<div class="section">
		<?php if( $isFeatured ): ?>
			<?php if( 1 < count($albums) ): ?>
				<h3>These are your current featured albums:</h3>
			<?php else: ?>
				<h3>This is your current featured album:</h3>
			<?php endif; ?>
		<?php else: ?>
			<h3>Please click on the box to feature:</h3>
		<?php endif; ?>
		<div>
			<?php	
				require_once("travellog/UIComponent/NewCollection/views/Class.ViewPhotoAlbumItem.php");
				$ctr = 0;
				foreach($albums AS $album): 
					$item = new ViewPhotoAlbumItem();
					$item->setAlbum($album);
					$item->setItemNumber(++$ctr);
					$item->render();
				endforeach;
			?>
		</div>
	</div>
	<?php if( $isFeatured ): ?>
		<script type="text/javascript">
			collectionControl.featuredAlbumCount = <?=count($albums)?>;
		</script>
	<?php endif; ?>
<?php endif; ?>
<div class="clear"></div>