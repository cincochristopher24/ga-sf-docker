<?php
	require_once("travellog/UIComponent/NewCollection/model/Class.AbstractUploadedPhoto.php");

	class UploadedPhotoGeneric extends AbstractUploadedPhoto{
		
		public function __construct(){
			$this->mContext = self::GENERIC_CONTEXT;
		}
		
		public function processFile(PathManager $pm, $objContext=NULL){
			if( !$this->prepareFile($pm,$objContext) ){
				return FALSE;
			}
			$this->sendS3RawFiles();
		}
		
		public function getShareLink(Photo $photo){
			return FALSE;
		}
	}