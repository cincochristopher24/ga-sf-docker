<?php

	require_once("travellog/UIComponent/NewCollection/loader/Class.AbstractTravelerPhotoCollectionLoader.php");
	
	class MainTravelerPhotoCollectionLoader extends AbstractTravelerPhotoCollectionLoader{
		
		protected function loadJournalPhotoCollectionForPublic(){
			$ownerID = $this->getOwner()->getTravelerID();
			
			$sql = "SELECT tblTravel.travelID, tblTravel.primaryphoto AS travelprimary, tblTravel.title AS journalTitle,
						tblTravelLog.travellogID AS travellogID, tblTravelLog.title AS entryTitle,
						tblPhoto.photoID, tblPhoto.primaryphoto AS travellogprimary
					FROM tblTravel, tblTravelLink, tblTravelLog, 
						tblTravelLogtoPhoto, tblPhoto
					WHERE tblTravel.travelerID = $ownerID
						AND tblTravel.publish = 1 
						AND tblTravel.deleted = 0
						AND tblTravel.travellinkID = tblTravelLink.travellinkID
						AND tblTravelLink.refType = 1
						AND tblTravelLink.refID = $ownerID
						AND tblTravel.travelID = tblTravelLog.travelID
						AND tblTravelLog.publish = 1
						AND tblTravelLog.deleted = 0
						AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID
						AND tblTravelLogtoPhoto.photoID = tblPhoto.photoID
					GROUP BY tblPhoto.photoID
					ORDER BY tblTravel.travelID DESC, travellogID DESC, tblTravelLogtoPhoto.position";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
			
			$entryArray = array();
			$currentEntry = 0;
			$photoArray = array();
			while($data=mysql_fetch_array($rs->Resultset())){
				if( $currentEntry != $data["travellogID"] ){
					$currentEntry = $data["travellogID"];
					if( count($photoArray) ){
						$entryArray[] = $photoArray;
					}
					$photoArray = array();
				}
				$photoArray[] = $data;
			}
			if( count($photoArray) ){
				$entryArray[] = $photoArray;
			}
			
			$currentJournal = 0;
			$journalTitle = "";
			$journalPhotoCollection = NULL;
			foreach( $entryArray AS $entry ){
				$currentEntry = $entry[0]["travellogID"];
				$entryTitle = $entry[0]["entryTitle"];
				
				$travellogPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::TRAVELLOG_CONTEXT);
				$travellogPhotoCollection->setType(self::TRAVELER_TYPE);
				$travellogPhotoCollection->setID($ownerID);
				$travellogPhotoCollection->setGenID($currentEntry);
				$travellogPhotoCollection->setTitle($entryTitle);
				
				$photoIDs = array();
				$entryPrimary = 0;
				foreach( $entry AS $data ){
					$travelPrimary = $data["travelprimary"];
					$photoIDs[] = $data["photoID"];
					$entryPrimary = (1==$data["travellogprimary"]) ? $data["photoID"] : $entryPrimary;
					
					if( $currentJournal != $data["travelID"] ){
						if( !is_null($journalPhotoCollection) ){
							$this->addCollection($journalPhotoCollection);
						}
						
						$currentJournal = $data["travelID"];
						$journalTitle = $data["journalTitle"];
						
						$journalPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::JOURNAL_CONTEXT);
						$journalPhotoCollection->setType(self::TRAVELER_TYPE);
						$journalPhotoCollection->setID($ownerID);
						$journalPhotoCollection->setGenID($currentJournal);
						$journalPhotoCollection->setTitle($journalTitle);
						$journalPhotoCollection->setPrimaryPhotoID($travelPrimary);
					}
				}
				
				$travellogPhotoCollection->setPrimaryPhotoID($entryPrimary);
				$travellogPhotoCollection->setPhotoIDs($photoIDs);
				
				$journalPhotoCollection->addTravellogCollection($travellogPhotoCollection);
			}
			
			if( !is_null($journalPhotoCollection) ){
				$this->addCollection($journalPhotoCollection);
			}
		}
		
		protected function generateCacheKey(){
			if( $this->isAdminView() ){
				return "Photo_Collection_Traveler_Owner_".$this->getOwner()->getTravelerID();
			}else{
				return "Photo_Collection_Traveler_Public_".$this->getOwner()->getTravelerID();
			}
		}
	}