<?php
	
	require_once("travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionDTOFactory.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/helper/Class.PhotoCollectionHelper.php");
	require_once("Class.ConnectionProvider.php");
	require_once("Cache/ganetCacheProvider.php");

	abstract class AbstractPhotoCollectionLoader implements PhotoCollectionConstants{
		
		protected $mOwnerObject = NULL;
		protected $mIsAdminView = FALSE;
		protected $mConnectionProvider = NULL;
		protected $mPhotoCollectionDTOFactory = NULL;
		
		private $mAlbumPhotoCollection = array();
		private $mJournalPhotoCollection = array();
		private $mArticlePhotoCollection = array();
		
		abstract protected function loadCollection();
		
		public function initialize($ownerObject=NULL,$adminView=FALSE){
			$this->OwnerObject = $ownerObject;
			$this->mIsAdminView = $adminView;
			$this->mConnectionProvider = ConnectionProvider::instance();
			$this->mPhotoCollectionDTOFactory = PhotoCollectionDTOFactory::getInstance();
			$this->loadCollection();
		}
		
		protected function getOwner(){
			return $this->OwnerObject;
		}
		
		protected function isAdminView(){
			return $this->mIsAdminView;
		}
		
		protected function addCollection($collection){
			if( in_array($collection->getContext(), array(self::ALBUM_CONTEXT,self::TRAVALBUM_CONTEXT)) ){
				$this->mAlbumPhotoCollection[] = $collection;
			}else if( $collection->getContext() == self::JOURNAL_CONTEXT ){
				$this->mJournalPhotoCollection[] = $collection;
			}else if( $collection->getContext() == self::ARTICLE_CONTEXT ){
				$this->mArticlePhotoCollection[] = $collection;
			}else if ($collection->getContext() == self::GROUP_TEMPLATE_CONTEXT){
				$this->mAlbumPhotoCollection[] = $collection;
			}
		}
		
		public function hasCollection(){
			return 0 < $this->getCollectionCount();
		}
		
		public function getCollectionCount(){
			return count($this->getCollection());
		}
		
		public function hasPhotos(){
			foreach($this->getCollection() AS $collection){
				if( $collection->hasPhotos() ){
					return TRUE;
				}
			}
			return FALSE;
		}
		
		public function getCollection(){
			$collection = array();
			foreach($this->mAlbumPhotoCollection AS $temp){
				$collection[] = $temp;
			}
			foreach($this->mJournalPhotoCollection AS $temp){
				$collection[] = $temp;
			}
			foreach($this->mArticlePhotoCollection AS $temp){
				$collection[] = $temp;
			}
			return $collection;
		}
		
		public function getCollectionItem($context=NULL,$genID=NULL){
			$collectionItem = FALSE;
			$collectionArray = array();
			if( self::ALBUM_CONTEXT == $context || self::TRAVALBUM_CONTEXT == $context ){
				$collectionArray = $this->mAlbumPhotoCollection;
			}else if( self::JOURNAL_CONTEXT == $context ){
				$collectionArray = $this->mJournalPhotoCollection;
			}else if( self::ARTICLE_CONTEXT == $context ){
				$collectionArray = $this->mArticlePhotoCollection;
			}else if( self::TRAVELLOG_CONTEXT == $context ){
				foreach($this->mJournalPhotoCollection AS $journalCollection){
					$collectionArray = array_merge($collectionArray,$journalCollection->getTravellogCollection());
				}
			}else if( self::GROUP_TEMPLATE_CONTEXT == $context ){
				$collectionArray = $this->mAlbumPhotoCollection;
			}else if(is_null($context) && is_null($genID)){
				return $this->getTopCollectionItem();
			}
			foreach($collectionArray AS $collection){
				if( intval($collection->getGenID()) == $genID ){
					$collectionItem = $collection;
					break;
				}
			}
			return $collectionItem;
		}
		
		public function getTopCollectionItem(){
			$topCollectionItem = FALSE;
			if( count($this->mAlbumPhotoCollection) ){
				$topCollectionItem = $this->mAlbumPhotoCollection[0];
			}else if( count($this->mJournalPhotoCollection) ){
				$topCollectionItem = $this->mJournalPhotoCollection[0];
			}else if( count($this->mArticlePhotoCollection) ){
				$topCollectionItem = $this->mArticlePhotoCollection[0];
			}
			return $topCollectionItem;
		}
	}