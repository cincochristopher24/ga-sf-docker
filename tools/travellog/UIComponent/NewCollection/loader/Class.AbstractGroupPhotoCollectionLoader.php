<?php

	require_once("travellog/UIComponent/NewCollection/loader/Class.AbstractPhotoCollectionLoader.php");
	
	abstract class AbstractGroupPhotoCollectionLoader extends AbstractPhotoCollectionLoader{
		
		protected function loadCollection(){
			$collectionLoaded = FALSE;
			$key = $this->generateCacheKey();
			$cache	=	ganetCacheProvider::instance()->getCache();
			if ( $cache != null ) {
				if ( $collectionArray = $cache->get($key) ){
					foreach($collectionArray AS $collection){
						$this->addCollection($collection);
					}
					$collectionLoaded = TRUE;
				}
			}
			if( !$collectionLoaded ){
				if( $this->isAdminView() ){
					$this->loadGroupAlbumPhotoCollectionForAdmin();
					$this->loadGroupJournalPhotoCollectionForAdmin();
					$this->loadGroupArticlePhotoCollectionForAdmin();
				}else{
					$this->loadGroupAlbumPhotoCollectionForPublic();
					$this->loadGroupJournalPhotoCollectionForPublic();
					$this->loadGroupArticlePhotoCollectionForPublic();
				}
				if ($cache != null) {
					$collectionArray = $this->getCollection();
					$cache->set($key,$collectionArray,array('EXPIRE'=>10800));
				}
			}
		}
		
		private function generateCacheKey(){
			if( $this->isAdminView() ){
				return "Photo_Collection_Group_Admin_".$this->getOwner()->getGroupID();
			}else{
				return "Photo_Collection_Group_Public_".$this->getOwner()->getGroupID();
			}
			
		}
		
		private function loadGroupAlbumPhotoCollectionForAdmin(){
			$groupID = $this->getOwner()->getGroupID();
			
			$sql = "SELECT tblPhotoAlbum.photoalbumID, tblPhotoAlbum.title, tblPhotoAlbum.primaryPhoto AS primaryphoto
					FROM tblPhotoAlbum
					WHERE tblPhotoAlbum.groupID = $groupID
					GROUP BY tblPhotoAlbum.photoalbumID
					ORDER BY tblPhotoAlbum.photoalbumID DESC";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
			
			while($data=mysql_fetch_array($rs->Resultset())){
				$albumID = $data["photoalbumID"];
				$albumPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::ALBUM_CONTEXT);
				$albumPhotoCollection->setType(self::GROUP_TYPE);
				$albumPhotoCollection->setID($groupID);
				$albumPhotoCollection->setGenID($albumID);
				$albumPhotoCollection->setTitle($data["title"]);
				$albumPhotoCollection->setPrimaryPhotoID($data["primaryphoto"]);
				
				$sql2 = "SELECT tblPhoto.photoID
						 FROM tblPhoto, tblPhotoAlbumtoPhoto
						 WHERE tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID
							AND tblPhotoAlbumtoPhoto.photoalbumID = $albumID
						 GROUP BY tblPhoto.photoID
						 ORDER BY tblPhotoAlbumtoPhoto.position";
				
				$rs2 = $this->mConnectionProvider->getRecordset2();
				$rs2->Execute($sql2);
				
				$photoIDs = array();
				while($data2=mysql_fetch_array($rs2->Resultset())){
					$photoIDs[] = $data2["photoID"];
				}
				
				$albumPhotoCollection->setPhotoIDs($photoIDs);
				
				$this->addCollection($albumPhotoCollection);
			}
		}
		
		private function loadGroupAlbumPhotoCollectionForPublic(){
			$groupID = $this->getOwner()->getGroupID();
			
			$sql = "SELECT tblPhotoAlbum.photoalbumID AS albumID, tblPhotoAlbum.title, 
						tblPhotoAlbum.primaryPhoto AS primaryphoto, tblPhoto.photoID
					FROM tblPhoto, tblPhotoAlbum, tblPhotoAlbumtoPhoto
					WHERE tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID
						AND tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID
						AND tblPhotoAlbum.groupID = $groupID
					GROUP BY tblPhoto.photoID
					ORDER BY albumID DESC, tblPhotoAlbumtoPhoto.position";
			//echo $sql."<br />";
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
				
			$photoIDs = array();
			$primaryPhotoID = 0;
			$previousAlbumID = NULL;
			$albumPhotoCollection = NULL;
			while($data=mysql_fetch_array($rs->Resultset())){
				if( $previousAlbumID != $data["albumID"] ){
					if( !is_null($albumPhotoCollection) ){
						$albumPhotoCollection->setPhotoIDs($photoIDs);
						$albumPhotoCollection->setPrimaryPhotoID($primaryPhotoID);
						
						$this->addCollection($albumPhotoCollection);
					}
					
					$previousAlbumID = $data["albumID"];
					$photoIDs = array();
					$albumPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::ALBUM_CONTEXT);
					$albumPhotoCollection->setID($groupID);
					$albumPhotoCollection->setGenID($previousAlbumID);
					$albumPhotoCollection->setTitle($data["title"]);
				}
				$photoIDs[] = $data["photoID"];
				$primaryPhotoID = (1 == $data["primaryphoto"]) ? $data["photoID"] : $primaryPhotoID;
			}
			if( !is_null($albumPhotoCollection) ){
				$albumPhotoCollection->setPhotoIDs($photoIDs);
				$albumPhotoCollection->setPrimaryPhotoID($primaryPhotoID);
				
				$this->addCollection($albumPhotoCollection);
			}
		}
		
		private function loadGroupJournalPhotoCollectionForAdmin(){
			$groupID = $this->getOwner()->getGroupID();
			
			$sql = "SELECT tblTravel.travelID, tblTravel.primaryphoto, tblTravel.title AS journalTitle, 
						tblTravelLog.travellogID AS travellogID, tblTravelLog.title AS entryTitle
					FROM tblTravel, tblTravelLink, tblTravelLog
					WHERE tblTravel.publish = 1 
						AND tblTravel.deleted = 0
						AND tblTravel.travellinkID = tblTravelLink.travellinkID
						AND tblTravelLink.refType = 2
						AND tblTravelLink.refID = $groupID
						AND tblTravel.travelID = tblTravelLog.travelID
						AND tblTravelLog.publish = 1
						AND tblTravelLog.deleted = 0
					GROUP BY travellogID
					ORDER BY tblTravel.travelID DESC, travellogID DESC";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
			
			$primaryPhotoID = 0;
			$previousTravelID = NULL;
			$journalPhotoCollection = NULL;
			while($data=mysql_fetch_array($rs->Resultset())){
				if( $previousTravelID != $data["travelID"] ){
					if( !is_null($journalPhotoCollection) ){
						$this->addCollection($journalPhotoCollection);
					}
					$previousTravelID = $data["travelID"];
					$journalPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::JOURNAL_CONTEXT);
					$journalPhotoCollection->setType(self::GROUP_TYPE);
					$journalPhotoCollection->setID($groupID);
					$journalPhotoCollection->setGenID($data["travelID"]);
					$journalPhotoCollection->setTitle($data["journalTitle"]);
					$journalPhotoCollection->setPrimaryPhotoID($data["primaryphoto"]);
				}
				
				$travellogID = $data["travellogID"];
				$entryTitle = $data["entryTitle"];
				
				$sql2 = "SELECT tblPhoto.photoID, tblPhoto.primaryphoto
						 FROM tblPhoto, tblTravelLogtoPhoto
						 WHERE tblPhoto.phototypeID = 2
							AND tblPhoto.photoID = tblTravelLogtoPhoto.photoID
							AND tblTravelLogtoPhoto.travellogID = $travellogID
						 GROUP BY tblPhoto.photoID
						 ORDER BY tblTravelLogtoPhoto.position";
				
				$rs2 = $this->mConnectionProvider->getRecordset2();
				$rs2->Execute($sql2);
				
				$photoIDs = array();
				$primaryPhotoID2 = 0;
				while($data2=mysql_fetch_array($rs2->Resultset())){
					$photoIDs[] = $data2["photoID"];
					$primaryPhotoID2 = (1 == $data2["primaryphoto"]) ? $data2["photoID"] : $primaryPhotoID2;
				}
				
				$travellogPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::TRAVELLOG_CONTEXT);
				$travellogPhotoCollection->setType(self::GROUP_TYPE);
				$travellogPhotoCollection->setID($groupID);
				$travellogPhotoCollection->setGenID($travellogID);
				$travellogPhotoCollection->setTitle($entryTitle);
				$travellogPhotoCollection->setPrimaryPhotoID($primaryPhotoID2);
				$travellogPhotoCollection->setPhotoIDs($photoIDs);
				
				$journalPhotoCollection->addTravellogCollection($travellogPhotoCollection);
			}
			if( !is_null($journalPhotoCollection) ){
				$this->addCollection($journalPhotoCollection);
			}			
		}
		
		private function loadGroupJournalPhotoCollectionForPublic(){
			$groupID = $this->getOwner()->getGroupID();
			
			$sql = "SELECT tblTravel.travelID, tblTravel.primaryphoto AS travelprimary, tblTravel.title AS journalTitle,
						tblTravelLog.travellogID AS travellogID, tblTravelLog.title AS entryTitle, 
						tblPhoto.photoID, tblPhoto.primaryphoto AS travellogprimary
					FROM tblTravel, tblTravelLink, tblTravelLog, 
						tblTravelLogtoPhoto, tblPhoto
					WHERE tblTravel.publish = 1 
						AND tblTravel.deleted = 0
						AND tblTravel.travellinkID = tblTravelLink.travellinkID
						AND tblTravelLink.refType = 2
						AND tblTravelLink.refID = $groupID
						AND tblTravel.travelID = tblTravelLog.travelID
						AND tblTravelLog.publish = 1
						AND tblTravelLog.deleted = 0
						AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID
						AND tblTravelLogtoPhoto.photoID = tblPhoto.photoID
					GROUP BY tblPhoto.photoID
					ORDER BY tblTravel.travelID DESC, travellogID DESC, tblTravelLogtoPhoto.position";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);
			
			$entryArray = array();
			$currentEntry = 0;
			$photoArray = array();
			while($data=mysql_fetch_array($rs->Resultset())){
				if( $currentEntry != $data["travellogID"] ){
					$currentEntry = $data["travellogID"];
					if( count($photoArray) ){
						$entryArray[] = $photoArray;
					}
					$photoArray = array();
				}
				$photoArray[] = $data;
			}
			if( count($photoArray) ){
				$entryArray[] = $photoArray;
			}
			
			$currentJournal = 0;
			$journalTitle = "";
			$journalPhotoCollection = NULL;
			foreach( $entryArray AS $entry ){
				$currentEntry = $entry[0]["travellogID"];
				$entryTitle = $entry[0]["entryTitle"];
				
				$travellogPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::TRAVELLOG_CONTEXT);
				$travellogPhotoCollection->setType(self::GROUP_TYPE);
				$travellogPhotoCollection->setID($groupID);
				$travellogPhotoCollection->setGenID($currentEntry);
				$travellogPhotoCollection->setTitle($entryTitle);
				
				$photoIDs = array();
				$entryPrimary = 0;
				foreach( $entry AS $data ){
					$travelPrimary = $data["travelprimary"];
					$photoIDs[] = $data["photoID"];
					$entryPrimary = (1==$data["travellogprimary"]) ? $data["photoID"] : $entryPrimary;
					
					if( $currentJournal != $data["travelID"] ){
						if( !is_null($journalPhotoCollection) ){
							$this->addCollection($journalPhotoCollection);
						}
						
						$currentJournal = $data["travelID"];
						$journalTitle = $data["journalTitle"];
						
						$journalPhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::JOURNAL_CONTEXT);
						$journalPhotoCollection->setType(self::GROUP_TYPE);
						$journalPhotoCollection->setID($groupID);
						$journalPhotoCollection->setGenID($currentJournal);
						$journalPhotoCollection->setTitle($journalTitle);
						$journalPhotoCollection->setPrimaryPhotoID($travelPrimary);
					}
				}
				
				$travellogPhotoCollection->setPrimaryPhotoID($entryPrimary);
				$travellogPhotoCollection->setPhotoIDs($photoIDs);
				
				$journalPhotoCollection->addTravellogCollection($travellogPhotoCollection);
			}
			
			if( !is_null($journalPhotoCollection) ){
				$this->addCollection($journalPhotoCollection);
			}
		}
		
		private function loadGroupArticlePhotoCollectionForAdmin(){
			$groupID = $this->getOwner()->getGroupID();
			
			$sql = "SELECT tblArticle.articleID, tblArticle.title
					FROM tblArticle
					WHERE tblArticle.groupID = $groupID
						AND tblArticle.deleted = 0
					GROUP BY tblArticle.articleID
					ORDER BY tblArticle.articleID DESC";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);

			while($data=mysql_fetch_array($rs->Resultset())){
				$articleID = $data["articleID"];
				$articlePhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::ARTICLE_CONTEXT);
				$articlePhotoCollection->setType(self::GROUP_TYPE);
				$articlePhotoCollection->setID($groupID);
				$articlePhotoCollection->setGenID($articleID);
				$articlePhotoCollection->setTitle($data["title"]);

				$sql2 = "SELECT tblPhoto.photoID, tblPhoto.primaryphoto
						 FROM tblPhoto, tblArticletoPhoto
						 WHERE tblPhoto.phototypeID = 11
						 	AND tblPhoto.photoID = tblArticletoPhoto.photoID
							AND tblArticletoPhoto.articleID = $articleID
						 GROUP BY tblPhoto.photoID
						 ORDER BY tblArticletoPhoto.position";
				
				$rs2 = $this->mConnectionProvider->getRecordset2();
				$rs2->Execute($sql2);

				$photoIDs = array();
				$primaryPhotoID = 0;
				while($data2=mysql_fetch_array($rs2->Resultset())){
					$photoIDs[] = $data2["photoID"];
					$primaryPhotoID = (1 == $data2["primaryphoto"]) ? $data2["photoID"] : $primaryPhotoID;
				}

				$articlePhotoCollection->setPhotoIDs($photoIDs);
				$articlePhotoCollection->setPrimaryPhotoID($primaryPhotoID);
				
				$this->addCollection($articlePhotoCollection);
			}
		}
		
		private function loadGroupArticlePhotoCollectionForPublic(){
			$groupID = $this->getOwner()->getGroupID();
			
			$sql = "SELECT tblArticle.title, tblArticletoPhoto.articleID, 
						tblPhoto.photoID, tblPhoto.primaryphoto
					FROM tblArticle, tblArticletoPhoto, tblPhoto
					WHERE tblArticle.groupID = $groupID
						AND tblArticle.deleted = 0
						AND tblArticle.articleID = tblArticletoPhoto.articleID
						AND tblArticletoPhoto.photoID = tblPhoto.photoID
						AND tblPhoto.phototypeID = 11
					GROUP BY tblPhoto.photoID
					ORDER BY tblArticletoPhoto.articleID DESC, tblArticletoPhoto.position";
			
			$rs = $this->mConnectionProvider->getRecordset();
			$rs->Execute($sql);

			$photoIDs = array();
			$primaryPhotoID = 0;
			$previousArticleID = NULL;
			$articlePhotoCollection = NULL;
			while($data=mysql_fetch_array($rs->Resultset())){
				if( $previousArticleID != $data["articleID"] ){
					if( !is_null($articlePhotoCollection) ){
						$articlePhotoCollection->setPhotoIDs($photoIDs);
						$articlePhotoCollection->setPrimaryPhotoID($primaryPhotoID);
						
						$this->addCollection($articlePhotoCollection);
					}
					
					$previousArticleID = $data["articleID"];
					$photoIDs = array();
					$articlePhotoCollection = $this->mPhotoCollectionDTOFactory->create(self::ARTICLE_CONTEXT);
					$articlePhotoCollection->setType(self::GROUP_TYPE);
					$articlePhotoCollection->setID($groupID);
					$articlePhotoCollection->setGenID($previousArticleID);
					$articlePhotoCollection->setTitle($data["title"]);
				}
				$photoIDs[] = $data["photoID"];
				$primaryPhotoID = (1 == $data["primaryphoto"]) ? $data["photoID"] : $primaryPhotoID;
			}
			if( !is_null($articlePhotoCollection) ){
				$articlePhotoCollection->setPhotoIDs($photoIDs);
				$articlePhotoCollection->setPrimaryPhotoID($primaryPhotoID);
				
				$this->addCollection($articlePhotoCollection);
			}
		}
	}