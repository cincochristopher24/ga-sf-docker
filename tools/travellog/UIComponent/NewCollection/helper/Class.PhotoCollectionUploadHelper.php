<?php

	require_once("travellog/model/Class.PathManager.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionUploadedPhotoFactory.php");
	
	class PhotoCollectionUploadHelper implements PhotoCollectionConstants{
		
		private static $instance = NULL;
		
		private $mUploadMode	=	self::CLASSIC;
		private $mFiles			=	array();
		private $mData			=	array(	"FileCount"	=>	0	);
		
		private $mCollectionDTO		=	NULL;
		
		private $mLargeFiles		=	array();
		private $mInvalidFiles		=	array();
		private $mValidFiles		=	array();
		
		private $mBatchUploadID	=	NULL;
		
		private function __construct(){
		}
		
		public static function getInstance(){
			if( is_null(self::$instance) ){
				self::$instance = new PhotoCollectionUploadHelper();
			}
			return self::$instance;
		}
		
		function setUploadMode($mode=0){
			$this->mUploadMode = $mode;
		}
		function setFiles($files=array()){
			$this->mFiles = $files;
		}
		function setData($data=array()){
			$this->mData = array_merge($this->mData,$data);
			if( self::CLASSIC == $this->mUploadMode ){
				$this->mData["FileCount"] = 5;
			}//var_dump($this->mData);exit;
		}
		function setCollectionDTO($collectionDTO=NULL){
			$this->mCollectionDTO = $collectionDTO;
		}
		function setBatchUploadID($batchUploadID=NULL){
			$this->mBatchUploadID = $batchUploadID;
		}
		/** 
		 * NOTE: change maximum upload image size from 
		 * 			2048000 bytes to 
		 * 			4194304 bytes or 4mb 
		 * - change by : nash - Sept. 28, 2010
		 **/ 
		function processFiles(){//var_dump($this->mFiles);exit;
			if ($this->mCollectionDTO->getContextObject() instanceof GanetPhotoAlbum) {
				$context = GanetPhotoContextBuilderRegistry::getInstance()->getPhotoContext($this->mCollectionDTO->getContextObject()->getContextID(), 3);
				$rel = $context->getRelativePath();
				if (!file_exists($rel)) {
					$context->createPath();
				}				
				
				$x= 1;
				foreach($this->mFiles as $file){//var_dump($this->mFiles);exit;
				//	if( "" != $file["name"] && 0 == $file["error"] && 2048000 >= $file["size"] ){
					if( "" != $file["name"] && 0 == $file["error"] && 4194304 >= $file["size"] ){
						$upPhoto = PhotoCollectionUploadedPhotoFactory::getInstance()->create($this->mCollectionDTO->getContext());
						$upPhoto->setGenID($this->mCollectionDTO->getGenID());
						$upPhoto->setFile($file);
						if( isset($this->mData["Description_".$x]) ){
							$upPhoto->setCaption($this->mData["Description_".$x]);
						}
						if( isset($this->mData["Angle_".$x]) ){
							$upPhoto->setRotationAngle($this->mData["Angle_".$x]);
						}
						$upPhoto->processFile2($this->mCollectionDTO->getContextObject());
						$this->mValidFiles[] = $file["name"];
					}
				//	if( "" != $file["name"] && 2048000 < $file["size"]){
					if( "" != $file["name"] && 4194304 < $file["size"]){
						$this->mLargeFiles[] = $file["name"];
					}
					$x++;
				}
				$this->checkUploadError();	
			}
			else {
				$pathManager = new PathManager($this->mCollectionDTO->getContextObject());
				$rel = $pathManager->GetPathrelative();
				if( !is_dir($rel) ){
					$pathManager->CreatePath();
				}
				
				$feedParams = array();
				if (SessionManager::getInstance()->get('travelerID') == 6397){
					$feedParams = array(
						'title'   => $this->mCollectionDTO->getContextObject()->getTitle(),
						'message' => 'I\'ve posted new photos',
						'picture' => $this->mCollectionDTO->getContextObject()->getPrimaryPhoto()->getPhotoLink(),
					);
				}
				
				$x= 1;
				foreach($this->mFiles as $file){//var_dump($this->mFiles);exit;
					$valid = TRUE;
				//	if( "" != $file["name"] && 0 == $file["error"] && 2048000 >= $file["size"] ){
					if( "" != $file["name"] && 0 == $file["error"] && 4194304 >= $file["size"] ){
						if( "application/unknown" == $file["type"] ){
							$valid = FALSE;
							$this->mInvalidFiles[] = $file["name"];
						}else{
							$upPhoto = PhotoCollectionUploadedPhotoFactory::getInstance()->create($this->mCollectionDTO->getContext());
							$upPhoto->setGenID($this->mCollectionDTO->getGenID());
							$upPhoto->setFile($file);
							if( isset($this->mData["Description_".$x]) ){
								$upPhoto->setCaption($this->mData["Description_".$x]);
							}
							if( isset($this->mData["Angle_".$x]) ){
								$upPhoto->setRotationAngle($this->mData["Angle_".$x]);
							}
							$validFile = $upPhoto->processFile($pathManager,$this->mCollectionDTO->getContextObject());
							if( $validFile ){
								$this->mValidFiles[] = $file["name"];
								$this->shareUpload($this->mBatchUploadID,$upPhoto->getShareLink($validFile),$feedParams);
							}else{
								$valid = FALSE;
								$this->mInvalidFiles[] = $file["name"];
							}
						}
					}
				//	if( $valid && "" != $file["name"] && 2048000 < $file["size"]){
					if( $valid && "" != $file["name"] && 4194304 < $file["size"]){
						$this->mLargeFiles[] = $file["name"];
					}
					$x++;
				}
				$this->checkUploadError();	
			}
		}
		
		function checkUploadError(){
			$collectionName = "_".$this->mCollectionDTO->getContext()."_".$this->mCollectionDTO->getGenID();
			$_SESSION["UploadError$collectionName"] = FALSE;
			if( 0 < count($this->mLargeFiles) || 0 < count($this->mInvalidFiles) ){
				$_SESSION["UploadError$collectionName"] = TRUE;
				$_SESSION["LargeFiles$collectionName"] = $this->mLargeFiles;
				$_SESSION["InvalidFiles$collectionName"] = $this->mInvalidFiles;
			}
			$_SESSION["ValidFiles$collectionName"] = $this->mValidFiles;
		}
		
		private function shareUpload($batchUploadID,$linkStr,$params){
			if( is_null($batchUploadID) || !$linkStr ){
				return;
			}
			$batchUploadIDs = isset($_SESSION["batchUploadIDs"]) ? $_SESSION["batchUploadIDs"] : array();
			if( !in_array($batchUploadID,$batchUploadIDs) ){
				//post or tweet upload
				$batchUploadIDs[] = $batchUploadID;
				$_SESSION["batchUploadIDs"] = $batchUploadIDs;
			
				// twitter/facebook widget observer
				require_once('travellog/controller/twitterWidget/Class.WidgetPostObserver.php');
				WidgetPostObserver::post($linkStr, $params);
			}
		}
	}