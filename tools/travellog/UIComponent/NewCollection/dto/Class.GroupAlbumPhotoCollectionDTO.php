<?php

	require_once("travellog/UIComponent/NewCollection/dto/Class.AbstractPhotoCollectionDTO.php");

	class GroupAlbumPhotoCollectionDTO extends AbstractPhotoCollectionDTO{
		
		public function __construct(){
			$this->mType = self::GROUP_TYPE;
			$this->mContext = self::ALBUM_CONTEXT;
		}
		
		public function getContextObject(){
			if( is_null($this->mContextObject) ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass('PhotoAlbum');
				$this->mContextObject = $file_factory->getClass('PhotoAlbum', array($this->getGenID()));
			}
			return $this->mContextObject;
		}
		
		public function getPhotoInstances(){
			if( is_null($this->mPhotoInstances) ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass('Photo');
			
				$this->mPhotoInstances = array();
				foreach($this->getPhotoIDs() AS $photoID){
					$this->mPhotoInstances[] = $file_factory->getClass('Photo', array(NULL,$photoID));
				}
			}
			
			return $this->mPhotoInstances;
		}
	}