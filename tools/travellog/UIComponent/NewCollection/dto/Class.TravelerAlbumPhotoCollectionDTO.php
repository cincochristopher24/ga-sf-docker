<?php

	require_once("travellog/UIComponent/NewCollection/dto/Class.AbstractPhotoCollectionDTO.php");

	class TravelerAlbumPhotoCollectionDTO extends AbstractPhotoCollectionDTO{
		
		public function __construct(){
			$this->mType = self::TRAVELER_TYPE;
			$this->mContext = self::TRAVALBUM_CONTEXT;
		}
		
		public function getContextObject(){
			if( is_null($this->mContextObject) ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass('PhotoAlbum');
				$this->mContextObject = $file_factory->getClass('PhotoAlbum', array($this->getGenID()));
			}
			return $this->mContextObject;
		}
	}