<?php

	require_once("travellog/factory/Class.FileFactory.php");
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	
	abstract class AbstractPhotoCollectionDTO implements PhotoCollectionConstants{
		
		protected $mType = "";
		protected $mID = 0;
		protected $mContext = "";
		protected $mGenID = 0;
		
		protected $mTitle = NULL;
		
		protected $mPhotoIDs = array();
		protected $mPrimaryPhotoID = 0;
		
		protected $mPhotoInstances = NULL;
		
		protected $mContextObject = NULL;
		
		abstract public function getContextObject();
		
		public function setType($type){
			$this->mType = $type;
		}
		
		public function setID($ID){
			$this->mID = $ID;
		}
		
		public function setContext($context){
			$this->mContext = $context;
		}
		
		public function setGenID($genID){
			$this->mGenID = $genID;
		}
		
		public function setTitle($title=""){
			$this->mTitle = $title;
		}
		
		public function getType(){
			return $this->mType;
		}
		
		public function getID(){
			return $this->mID;
		}
		
		public function getContext(){
			return $this->mContext;
		}
		
		public function getGenID(){
			return $this->mGenID;
		}
		
		public function getTitle(){
			if( is_null($this->mTitle) ){
				$this->mTitle = $this->getContextObject()->getTitle();
			}
			return $this->mTitle;
		}
		
		public function setPhotoIDs($photoIDs=array()){
			$this->mPhotoIDs = $photoIDs;
		}
		
		public function getPhotoIDs(){
			return $this->mPhotoIDs;
		}
		
		public function setPrimaryPhotoID($photoID=0){
			$this->mPrimaryPhotoID = $photoID;
		}
		
		public function getPrimaryPhotoID(){
			return $this->mPrimaryPhotoID;
		}
		
		public function getPhotoCount(){
			return count($this->getPhotoIDs());
		}
		
		public function hasPhotos(){
			return 0 < $this->getPhotoCount();
		}
		
		public function getLink(){
			return "/collection.php?type=".$this->getType()."&ID=".$this->getID()."&context=".$this->getContext()."&genID=".$this->getGenID();
		}
		
		public function getPrimaryPhoto(){
			return $this->getContextObject()->getPrimaryPhoto();
		}
		
		public function getPhotoInstances(){
			if( is_null($this->mPhotoInstances) ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass('Photo');
			
				$this->mPhotoInstances = array();
				foreach($this->getPhotoIDs() AS $photoID){
					$this->mPhotoInstances[] = $file_factory->getClass('Photo', array($this->getContextObject(),$photoID));
				}
			}
			
			return $this->mPhotoInstances;
		}
		
	}