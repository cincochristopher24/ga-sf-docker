<?php

	require_once("travellog/UIComponent/NewCollection/dto/Class.AbstractPhotoCollectionDTO.php");

	class ArticlePhotoCollectionDTO extends AbstractPhotoCollectionDTO{
		
		public function __construct(){
			$this->mContext = self::ARTICLE_CONTEXT;
		}
		
		public function getContextObject(){
			if( is_null($this->mContextObject) ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass('Article');
				$this->mContextObject = $file_factory->getClass('Article', array($this->getGenID()));
			}
			return $this->mContextObject;
		}
	}