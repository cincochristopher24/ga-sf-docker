<?php
	
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/dto/Class.JournalPhotoCollectionDTO.php");
	require_once("travellog/UIComponent/NewCollection/dto/Class.TravellogPhotoCollectionDTO.php");
	require_once("travellog/UIComponent/NewCollection/dto/Class.ArticlePhotoCollectionDTO.php");
	require_once("travellog/UIComponent/NewCollection/dto/Class.ProfilePhotoCollectionDTO.php");
	require_once("travellog/UIComponent/NewCollection/dto/Class.GroupAlbumPhotoCollectionDTO.php");
	require_once("travellog/UIComponent/NewCollection/dto/Class.TravelerAlbumPhotoCollectionDTO.php");
	require_once("travellog/UIComponent/NewCollection/dto/Class.GroupTemplateAlbumPhotoCollectionDTO.php");

	class PhotoCollectionDTOFactory implements PhotoCollectionConstants{
		
		private static $instance = NULL;
		
		private function __construct(){
			
		}
		
		public static function getInstance(){
			if( is_null(self::$instance) ){
				self::$instance = new PhotoCollectionDTOFactory();
			}
			return self::$instance;
		}
		
		function create($context=NULL){
			if (!in_array($context,array('profile','traveleralbum','photoalbum','travellog','journal','article',PhotoCollectionConstants::GROUP_TEMPLATE_CONTEXT)) ){
				throw new Exception("Invalid argument to PhotoCollectoinDTOFactory->create(). Argument should be in {'profile','traveleralbum','photoalbum','travellog','journal','article'}");
			}			

			switch($context){
				case self::PROFILE_CONTEXT:	
					return new ProfilePhotoCollectionDTO();
			
				case self::TRAVALBUM_CONTEXT:	
					return new TravelerAlbumPhotoCollectionDTO();

				case self::ALBUM_CONTEXT:
					return new GroupAlbumPhotoCollectionDTO();

				case self::TRAVELLOG_CONTEXT:	
					return new TravellogPhotoCollectionDTO();

				case self::JOURNAL_CONTEXT:	
					return new JournalPhotoCollectionDTO();

				case self::ARTICLE_CONTEXT:	
					return new ArticlePhotoCollectionDTO();

				case self::GROUP_TEMPLATE_CONTEXT:
					$dto = new GroupTemplateAlbumPhotoCollectionDTO();			
			}
			return $dto;
		}
	}