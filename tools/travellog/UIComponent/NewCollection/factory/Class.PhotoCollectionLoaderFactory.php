<?php
	
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
	require_once("travellog/UIComponent/NewCollection/helper/Class.PhotoCollectionHelper.php");
	require_once("travellog/UIComponent/NewCollection/loader/Class.CBGroupPhotoCollectionLoader.php");
	require_once("travellog/UIComponent/NewCollection/loader/Class.CBTravelerPhotoCollectionLoader.php");
	require_once("travellog/UIComponent/NewCollection/loader/Class.MainGroupPhotoCollectionLoader.php");
	require_once("travellog/UIComponent/NewCollection/loader/Class.MainTravelerPhotoCollectionLoader.php");
	require_once("travellog/UIComponent/NewCollection/loader/Class.MainGroupTemplatePhotoCollectionLoader.php");

	class PhotoCollectionLoaderFactory implements PhotoCollectionConstants{
		
		private static $instance = NULL;
		
		private function __construct(){
			
		}
		
		public static function getInstance(){
			if( is_null(self::$instance) ){
				self::$instance = new PhotoCollectionLoaderFactory();
			}
			return self::$instance;
		}
		
		function create(PhotoCollectionHelper $collectionHelper){			
			if( self::TRAVELER_TYPE == $collectionHelper->getType() ){
				$loader = isset($GLOBALS["CONFIG"]) ? new CBTravelerPhotoCollectionLoader() : new MainTravelerPhotoCollectionLoader();
			}else if( self::GROUP_TYPE == $collectionHelper->getType() ){
				$loader = isset($GLOBALS["CONFIG"]) ? new CBGroupPhotoCollectionLoader() : new MainGroupPhotoCollectionLoader();
			}else if( self::GROUP_TEMPLATE_TYPE == $collectionHelper->getType() ){
				$loader = new MainGroupTemplatePhotoCollectionLoader();
			}else{
				throw new Exception("CollectionHelper might not have been set properly. Invalid type {".$collectionHelper->getType()."}");
			}
			
			$loader->initialize($collectionHelper->getOwner(),$collectionHelper->viewerHasAdminPrivileges());
			return $loader;
		}
	}