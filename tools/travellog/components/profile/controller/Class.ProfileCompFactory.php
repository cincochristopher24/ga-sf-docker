<?php
class ProfileCompFactory{
	
	protected static $instance = null; 
	
	public static $TRAVELER_CONTEXT = 1;
	
	public static $GROUP_CONTEXT    = 2;
		
	static function getInstance(){
		if( self::$instance == null ) self::$instance = new ProfileCompFactory;
		return self::$instance; 
	}
	
	function create($context){
		switch($context){
			case ProfileCompFactory::$TRAVELER_CONTEXT:
				require_once('travellog/components/profile/controller/Class.TravelerProfileCompController.php');
				$obj = new TravelerProfileCompController;			
			break;
			case ProfileCompFactory::$GROUP_CONTEXT:
				require_once('travellog/components/profile/controller/Class.GroupProfileCompController.php');
				$obj = new GroupProfileCompController;
			break;	
		}
		return $obj;	
	}
}
?>
