<?php
require_once("travellog/model/Class.SessionManager.php");
abstract class AbstractProfileCompController{ 
	
	static 
	
	$EDIT_PROFILE             = 1,
	
	$EDIT_PRIVACY_PREFERENCES = 2,
	
	$COMPOSE_MESSAGES         = 3,
	
	$FRIENDS_PAGE             = 4,
	
	$EDIT_ACCOUNT_SETTINGS  = 5;
		
	protected 
	
	$file_factory = NULL,
	
	$obj_session  = NULL,
	 
	$props        = array();  
			
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance();
	}
	
	abstract function init($curr_id, $curr_page=NULL);
	
	abstract function get_view();
}
?>
