<?
	// js scripts included by neri:01-13-09//edited by michael
	Template::includeDependentJs('/min/g=TravelerProfile2');
?>

<!-- Opening DIV for #traveler_top_wrapper -->
<div id="traveler_top_wrapper">


	<div id="top_profile">
		<div id="intro">
		
		
			<!-- Begin Photo -->
			<div id="photo">	
				<?php if( $contents['is_owner'] ):?>
					<!--a  href="#TB_inline?inlineId=profilegallerycontainer" id="primary_profile_id" class="thickbox" title="profile photos">-->
					<!--<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(0,'gallery',<?= $contents['curr_traveler_id'];?>,'profile')" id="primary_profile_id">-->
					<img id="profileprimaryphoto"  src="<?php echo $contents['photo']?>"  alt="<?php echo $contents['mod_username']?> Travel Journals" title="View <?php echo $contents['mod_username']?> Travel Diaries and Travel Notes" width="120" height="120"/>
					<!--</a>-->
				<?php else: ?>
					<img id="profileprimaryphoto"  src="<?php echo $contents['photo']?>"  alt="<?php echo $contents['mod_username']?> Travel Journals" title="View <?php echo $contents['mod_username']?> Travel Diaries and Travel Notes" width="120" height="120"/>							
				<?php endif; ?>
				
				<?php if ($contents['is_owner']): ?>								
							<a title="Change your Profile Photo" href="/collection.php?type=traveler&ID=<?php echo $contents['curr_traveler_id']?>&amp;context=profile&amp;genID=<?php echo $contents['curr_traveler_id']?>">Change Photo</a>
				<?php endif;?>								
			</div>
			
			<!-- End Photo -->
																																		
			
		
			<h1 id="traveler_name">
				<?php echo $contents['username']?>	
				
				<!-- check if current traveler is allowed to view  the traveler's name: added by neri: 11-18-08 -->
				<?php //if ( 'Hidden from Public' !== $contents['show_name_preference']):
						if ( $contents['show_name_to_public'] || $contents["isAdminStaffViewer"] ):
				?>
				    <span>
	    				 <?=$contents['real_name']?>
	    			</span>
				<?php endif; ?>			
			</h1>
			
			<!-- Profile Control Panel to shown on Account owner's profile when logged in -->	
			
			<?php if( $contents['is_owner'] ):?>							
	
			<div id="profile_controlpanel" style="display: block;">
				<div class="panel_header"> 
					Manage your Profile 
					
					<?php if( $contents['is_login'] ):?>
						<span id="profilemgt_action">
								<?php if( $contents['show_profile_link'] ):?>
									<a href="/edittravelerprofile.php">Edit Profile</a>
								<?php endif;?>	
								
								<?php if( $contents['show_privacy_link'] ):?>
									<a href="/privacy.php">Privacy Preferences</a>
								<?php endif;?>	
								
								<?/*php if ($contents['is_owner']): ?>
									<a href="javascript:void(0)" onclick="window.open('/changepassword.php?action=entry','ChangePassword','width=500,height=700,navigation toolbar=disabled,left=200,top=150')">Change Account Settings</a>	
								<?php endif;*/?>
								
								<?php if ($contents['show_account_settings_link']): ?>
									<a href="/account-settings.php">Change Account Settings</a>	
								<?php endif;?>
						</span>
					<?php endif;?>					
				</div>
								
				<div class="panel_content">
					<div id="private_info">
						<div>
							<strong> Real Name:</strong> 
							<span>
			    				 <?=$contents['real_name']?> 
			    			</span>
							<span class="this_privacy_status"> <?= $contents['show_name_preference'] ?> </span>												
							
						</div>
						
						<div>
							<strong> Email Address:</strong> 
							<?php if( isset($contents['email'])):?>							
								<?php echo $contents['email'] ?>
								<span class="this_privacy_status"> <?= $contents['show_email_preference'] ?> </span>																					
							<?php endif;?>							
						</div>						
						
						
						<!-- Begin Notifications -->									
						<?php $hasOtherNotices = isset($contents['num_friend_requests']) || isset($contents['num_group_invites']) || isset($contents['num_group_requests']) || isset($contents['num_denied_group_requests']) || isset($contents['num_entry_drafts']) ?>
						<?php $showNotifications = isset($contents['num_new_messages']) || $hasOtherNotices ?>
						<div id="notifications" style="display:<?php echo $showNotifications ? '' : 'none' ?>">
							<strong>You have</strong>
							<?php $newMessagesCount = isset($contents['num_new_messages']) ? $contents['num_new_messages'] : 0 ?>
							<span id="message_count_status" style="<?php if ($newMessagesCount < 1) : ?> display:none <?php endif; ?>">
								<a href = "/messages.php">
									<span id="new_messages_count">
										<?php $separator = $hasOtherNotices ? ',' : '' ?>
										<?php echo $newMessagesCount . ' New Message' . ($newMessagesCount > 1 ? ('s' . $separator) : $separator) ?>
									</span>
								</a>
							</span>
							
							<script type="text/javascript">
								var newMessagesCount = <?php echo $newMessagesCount ?>;
								var hasOtherNotices = <?php echo $hasOtherNotices ? 'true' : 'false' ?>;
							</script>
						
							<?php if($showNotifications && isset($contents['include_notifications']) && $contents['include_notifications']): ?>																				
									<?php if( isset($contents['num_friend_requests'])): ?>
										<a href = "/friend.php"><?= $contents['num_friend_requests'] ?> Friend Request<? if (1 < $contents['num_friend_requests']): ?>s<? endif; ?> </a>						
									<?php endif; ?>
									
									<?php if (isset($contents['num_friend_requests']) && (isset($contents['num_group_invites']) || isset($contents['num_group_requests']) || isset($contents['num_denied_group_requests']) || $contents['num_entry_drafts'])): ?>,<?php endif; ?>
									
									<?php if( isset($contents['num_group_invites'])): ?>
										<a href = "/membership.php"><?= $contents['num_group_invites'] ?> Group Invitation<? if (1 < $contents['num_group_invites']): ?>s<? endif; ?> </a>
									<?php endif; ?>
									
									<?php if (isset($contents['num_group_invites']) && (isset($contents['num_group_requests']) || isset($contents['num_denied_group_requests']) || $contents['num_entry_drafts'])): ?>,<?php endif; ?>
											
									<?php if( isset($contents['num_group_requests'])): ?>		
										<a href = "/membership.php"><?= $contents['num_group_requests'] ?> Group Request<? if (1 < $contents['num_group_requests']): ?>s<? endif; ?> </a>		
									<?php endif; ?>
									
									<?php if( isset($contents['num_group_requests']) && (isset($contents['num_denied_group_requests']) || $contents['num_entry_drafts'])): ?>,<?php endif; ?>
									
									<?php if( isset($contents['num_denied_group_requests'])): ?>
										<a href = "/membership.php"><?= $contents['num_denied_group_requests'] ?> Denied Group Request<? if (1 < $contents['num_denied_group_requests']): ?>s <? endif; ?> </a>
									<?php endif; ?>	
									
									<?php if( isset($contents['num_entry_drafts']) && isset($contents['num_denied_group_requests'])): ?>,<?php endif; ?>
									
									<?php if( isset($contents['num_entry_drafts'])): ?>
										<a href = "/journal_draft/view_saved_drafts"><?php echo $contents['num_entry_drafts']; ?> Unfinished Entr<? if (1 < $contents['num_entry_drafts']) { ?>ies <? } else { ?>y<? } ?> </a>
									<?php endif; ?>																	
							<? endif; ?>
						</div>
						<!-- End Notifications -->														
																
					</div>				
				</div>
				<div class="panel_footer"> 
					<!-- for travel bio-->
					
					<div id="mtb_status">
						<strong>You've answered <span id = 'numAnsweredQuestions'><?= $contents['answered_mtb_questions'] ?></span> out of 16 items in</strong> 								
						<a class="facebook_direct" target = "_blank" title = "Install this App in Facebook" href = "http://www.goabroad.net/faq.php#bring_2_facebook">
							<span class="mtb_my">my</span><span class="mtb_travel">travel</span><span class="mtb_bio">bio</span>  
						</a> 						
					</div>
					<div>
						<a id = "travelbio_link" href="javascript:void(0)" onclick = "TravelBio.setTravelerId(<?= $contents['curr_traveler_id'] ?>); TravelBio.editTravelBio(); return false;"><span id = 'mtb_action_label'><?php if(16 == $contents['answered_mtb_questions']): ?>Change your answers on MyTravelBio.	<?php else: ?>Complete your TravelBio.<?php endif; ?></span></a>	
						&nbsp;	
						<a href="/faq.php#bring_2_facebook" target="_blank"><span id = 'mtb_learn_why'><?php if(16 > $contents['answered_mtb_questions']): ?>How do I bring my profile to Facebook?<?php endif; ?></span></a>		
					</div>	
					
				</div>
			</div>		
			
			<?php else: ?>
				<?php if (0 < $contents['answered_mtb_questions']): ?>
					<!-- Begin Travel Bio -->
						<?= $contents['obj_travel_bio']->render() ?>
					<!-- End Travel Bio -->	
					
				<?php else: ?>	
					<!-- Begin InfoBox to be shown when there is no mtb			 -->
					<div class="informative_text" id="informative_text" style="display: block; <?php if ($contents['cobrand']): ?> height:150px; background-image:none;<?php endif; ?>">
						<?php if (!$contents['cobrand']): ?>
							<h2>Did you know?</h2>
							
							<p id="rolling_text">
								<?= $contents['rotating_text'] ?>
							</p>
							
							<div> 
								<a href="<?= $contents['rotating_text_link'] ?>" title="Find out more about GoAbroad Network!"> <strong>Find out more</strong></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								
								<?php if (0 == $contents['traveler_id']): ?>
									<a href="/register.php"> <strong>Sign-up for FREE!</strong></a>
								<?php endif; ?>				
							</div>	
						<?php endif; ?>		
					</div>				
					<!-- End InfoBox -->	
				<?php endif; ?>
			<?php endif; ?>				
						
			<!-- End Panel -->				
						
				
			<!-- Begin Details	 -->
			<div id="details">
				<!-- Begin Profile -->
				<ul class="profile">	
			
					<!-- condition if isset($contents['email']) has been added by neri: 11-04-08 -->
					<?php if( $contents['age'] ):?>
						<li>
							<?php echo $contents['age']?> years old
						</li>
					<?php endif;?>
				
					<?php if( strlen(trim($contents['country'])) ):?>
						<li>
							From
							<?php 
								if( $contents['hometown'] != $contents['country'] )
									echo $contents['hometown'] . ", ";
								
								echo $contents['country'];
							?>
						</li>
					<?php endif;?>
	
					<?php if( strlen(trim($contents['curr_country'])) ):?>
						<li>
							Currently in
							<?php
								if( $contents['curr_hometown'] != $contents['curr_country'] )
									echo $contents['curr_hometown'] . ", ";
								echo $contents['curr_country'];
							?>
						</li>
					<? endif; ?>
					
					
					<?php //if( 'Hidden to Public' !== $contents['show_email_preference'] && isset($contents['email'])):
							if( $contents['show_email_to_public'] && isset($contents['email'])):
								echo $contents['imageEmail'];
					?>					
						<!-- <li>
							<a href="mailto:<?php echo $contents['email']?>"><?php echo $contents['email']?></a>
						</li> -->
					<?php endif;?>				
											
				</ul>
				
				<?php if( $contents['is_login'] ):?>
				<!-- Begin Profile Actions -->
				<div id="profile_actions"   style="<?=($contents['is_owner']) ? 'border:none;' : ''?>">
					
					<ul id="actions" class="profile_actions">
					
						<?php if( $contents['show_friend_request_link'] ):?>
							<li>
								<!--edited by ianne - 11/11/2008-->
								<a href="javascript:void(0)" onclick="CustomPopup.initialize('Request <?=$contents['username']?> as a Friend ?','Are you sure you want to add <?=$contents['username']?> as a friend?','/friend.php?action=request&amp;fID=<?=$contents['curr_traveler_id']?>&amp;profile','Send Request','1');CustomPopup.createPopup();">Add as a Friend</a>
							</li>
						<?php endif;?>				
						
						<?php if( $contents['has_friend_request'] ):?> 
							<li class="clear_both">
								<!--edited by ianne - 11/11/2008-->
								<a href="javascript:void(0)" onclick="CustomPopup.initialize('Accept Friend Request ?','You are about to accept <?=$contents['username']?>&lsquo;s request. Click Accept to continue.','/friend.php?action=accept&amp;fID=<?=$contents['curr_traveler_id']?>&amp;profile','Accept','1');CustomPopup.createPopup();">Accept</a> or
								<a class="negative_action" href="javascript:void(0)" onclick="CustomPopup.initialize('Deny Friend Request ?','You are about to deny <?=$contents['username']?>&lsquo;s request. Click Deny to continue.','/friend.php?action=deny&amp;fID=<?=$contents['curr_traveler_id']?>&amp;profile','Deny','1');CustomPopup.createPopup();" class="deny">Deny</a>
								Friend Request
							</li>									
						<?php endif;?>	
						
						<?php if( $contents['has_pending_request'] ):?>
							<li class="clear_both">
								<!--edited by ianne - 11/11/2008-->
								<a href="javascript:void(0)" onclick="CustomPopup.initialize('Cancel Friend Request ?','Are you sure you want to cancel your request to add <?=$contents['username']?> as a friend?','/friend.php?action=cancel&amp;fID=<?=$contents['curr_traveler_id']?>&amp;profile','Yes','1');CustomPopup.createPopup();">Cancel Request</a>
							</li>
						<?php endif;?>							
						
						<?php if( $contents['show_messages_link'] ):?>
							<li>
								<a href="/messages.php?act=messageTraveler&id=<?php echo $contents['curr_traveler_id']?>">Send a Message</a>
							</li>
						<?php endif;?>																	
				
				<?php if( !$contents['is_owner'] ):?>
					
					
						<!-- Temporarily hid the link	TODO: Place somewhere else -->
						<?php if( $contents['is_login'] ):?>
							<?php if( !$contents['is_block'] ):?>
								<li class="first" style="display: none;">
									<a class="negative_action" href="javascript:void(0)" onclick="CustomPopup.initialize('Block <?=$contents['username']?> ?','Are you sure you want to block <?=$contents['username']?>?','/friend.php?action=block&amp;fID=<?=$contents['curr_traveler_id']?>&amp;confirm&amp;profile','Block','1');CustomPopup.createPopup();">Block User</a>
								</li>
							<?php else:?>
								<li style="display: none;">
									<a class="negative_action" href="javascript:void(0)" onclick="CustomPopup.initialize('Unblock <?=$contents['username']?> ?','Are you sure you want to unblock <?=$contents['username']?>?','/friend.php?action=unblock&amp;fID=<?=$contents['curr_traveler_id']?>&amp;confirm&amp;profile','Unblock','1');CustomPopup.createPopup();">Unblock User</a>
								</li>
							<?php endif;?>
						<?php endif;?>
						
						
							<!-- Temporarily hid the link	TODO: Place somewhere else -->						
							<li class="last" style="display:none"><a class="negative_action" href="/reportabuse.php?tID=<?php echo $contents['curr_traveler_id']?>">Report Offensive Profile</a></li> 
						
		
				
				<? endif; ?>						
						
						
											
					
					</ul>
				</div>	
				<? endif; ?>
				<?/*
				<!--
				
				<?php if( !$contents['is_owner'] ):?>
					
					<ul class="profile_actions">
					
						
						
						<?php if( $contents['is_login'] ):?>
							<?php if( !$contents['is_block'] ):?>
								<li class="first">
									<a class="negative_action" href="javascript:void(0)" onclick="CustomPopup.initialize('Block <?=$contents['username']?> ?','Are you sure you want to block <?=$contents['username']?>?','/friend.php?action=block&amp;fID=<?=$contents['curr_traveler_id']?>&amp;confirm&amp;profile','Block','1');CustomPopup.createPopup();">Block User</a>
								</li>
							<?php else:?>
								<li>
									<a class="negative_action" href="javascript:void(0)" onclick="CustomPopup.initialize('Unblock <?=$contents['username']?> ?','Are you sure you want to unblock <?=$contents['username']?>?','/friend.php?action=unblock&amp;fID=<?=$contents['curr_traveler_id']?>&amp;confirm&amp;profile','Unblock','1');CustomPopup.createPopup();">Unblock User</a>
								</li>
							<?php endif;?>
						<?php endif;?>
						<li class="last"><a class="negative_action" href="/reportabuse.php?tID=<?php echo $contents['curr_traveler_id']?>">Report Offensive Profile</a></li>
					</ul>
				
				<? endif; ?>
				
				-->
				*/?>
				<!-- End Profile Actions -->	
				
				<?php if(!$contents['is_owner']): ?>
    				<div id="rss_link"><a href="/journalfeed2.php?tid=<?php echo $contents['curr_traveler_id']?>"><span>Subscribe to my RSS Feed</span></a></div>
    			<?php endif; ?>				
				
				<?php if( Template::isProfileFBLikeVisible() && isset($contents["fb_like_control"]) ): ?>
					<?php 
						$contents["fb_like_control"]->setWidth(295);
						echo $contents["fb_like_control"]->render();
					?>
				<?php endif; ?>									
			</div>
			<!-- End Details -->			
		</div>
	</div>
	
	
<!--added by ianne - 11/20/2008 success message after confirm-->
<?if(isset($_SESSION['custompopup_message'])):?>
	<script type="text/javascript">
		window.onload = function(){
			CustomPopup.initPrompt("<?=$_SESSION['custompopup_message']?>");
			CustomPopup.createPopup();
		}
	</script>
	<?unset($_SESSION['custompopup_message']);?>
<?endif;?>

<!-- Closing DIV for #traveler_top_wrapper -->
</div>