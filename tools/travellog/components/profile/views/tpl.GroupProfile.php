<?php
	$truncated_content = '';
	if (100 < strlen(trim($contents['group_desc'])))
	  $truncated_content   = substr($contents['group_desc'], 0, 100);
	
	$full_content        = $contents['group_desc'];
	
	$url_label = (isset($contents['web_url']) AND "" !== trim($contents['web_url'])) ? $contents['group_category'].$contents['web_url'] : "";
  	$url_label = preg_match('/^(http:\/\/)/',$url_label) ? $url_label : 'http://'.$url_label;
  	
  	$url = (isset($contents['web_url']) AND "" !== trim($contents['web_url'])) ? $contents['web_url'] : "";
  	$url = preg_match('/^(http:\/\/)/',$url) ? $url : 'http://'.$url;
  	
  	$show_url  = ("" === trim($url) || $url == 'http://') ? false : true;
  
  	/*************************************************************************************************
  	 * edits of neri:
  	 * 		added the variable show_privacy_link to check whether the edit privacy 
  	 * 			preference link will be displayed: 											11-10-08
  	 * 		added link for sending a message to group: 										11-18-08
  	 * 		changed "Change Account Settings" to "Change Administrator Accounts Settings"	01-20-09
  	 *************************************************************************************************/
	
	//added by Jul, for custom homepage photos of cobrand
	$inCobrand = FALSE;
	/*if( isset($GLOBALS['CONFIG']) ){
		$inCobrand = TRUE;
	}*/
?>

	<div id="intro">
		<div id="group_photo">	
			<img id="profileprimaryphoto"  src="<?php echo $contents['photo']?>" alt="<?php echo $contents['mod_group_name']?> Travel Journals" title="View <?php echo $contents['mod_group_name']?> Travel Diaries and Travel Notes" />
		</div>
					
		<!-- Begin Details	 -->
		<div id="group_details">
			<h1 id="traveler_name">
				<?php echo $contents['group_name']?>
			</h1>
		<!-- Begin Profile Actions --> 
		<div id="profile_actions">			
			<ul>
			  <? if($show_url):?>
					<li>
					  <a href="<?=$url?>">
							<?= $url_label ?>
					  </a>
					</li>
			  <? endif; ?>
			
			  <? // added by Antonio Pepito Cruda Jr. ?>
			  <? if (isset($contents['group_inst_info']) AND "" != trim($contents['group_inst_info'])): ?>  
			    
			    <li id = "additional_info" style="display: none;">
			      <?= $contents['group_inst_info'] ?>
          		</li>
			  
			  <? endif; ?>
			  <? // end of line added by Antonio Pepito Cruda Jr. ?>
			
			<?/*	if( $inCobrand && $contents['is_superStaffLogged'] && FALSE === strpos($_SERVER["SCRIPT_NAME"],"/custom_pageheader.php") ): ?>
				<li>
				  <a href="/custom_pageheader.php">
					Customize Homepage
				  </a>
				</li>
			<?	endif; */?>
					
			  <? if( $contents['is_owner'] || $contents['is_superStaffLogged'] ):?>
					<?	if($inCobrand && FALSE === strpos($_SERVER["SCRIPT_NAME"],"/custom_pageheader.php") ): ?>
					<li>
						<a href="/custom_pageheader.php">Customize Homepage</a>
					</li>
					<?	endif; ?>
			
					<li>
					  <? if($contents['can_upload']):?>
							  <a onclick="tb_show('','/thickbox/thickbox.php?height=680&amp;width=529&amp;mode=upload&amp;context=group&amp;genID=<? echo $contents['curr_group_id']?>','');" href="javascript:void(0)">
							    Change Group Logo
							  </a> 
							  <? if($contents['obj_group']->getGroupPhotoID()):?>
							  	|
									<? $popup_link = "/ajaxpages/PhotoUILayout.php?action=deletelogo&amp;context=group&amp;redirect&amp;genid=".$contents['obj_group']->getGroupID() ?>
							  	<a href="javascript:void(0)" onclick="CustomPopup.initialize('Remove Group Logo ?','Are you sure you want to remove <?= addslashes($contents['obj_group']->getName()) ?> group logo?','<?=$popup_link?>','Remove','0');CustomPopup.createPopup();return false;">
							    	Remove Group Logo
							  	</a>
							  <? endif; ?>
					  <?php endif;?>
					  <?php if( $contents['show_profile_link'] ):?> 
					</li>
					<li>    
					    <a href="/group.php?gID=<?=$contents['curr_group_id']?>&amp;mode=edit">
					      Edit Group Profile
					    </a>
					  <?php endif;?>
					</li>
					
					<?php if( $contents['show_privacy_link'] ):?>
						<li>
						  <a href="/group-privacy.php?groupID=<?=$contents['curr_group_id']?>">
						    Edit Privacy Preference
						  </a>
						</li>
					<?php endif; ?>
					<?php if($contents['obj_group'] instanceOf AdminGroup && $contents['is_advisor']): ?>
						<!-- <li>
						  <a href="javascript:void(0)" onclick="window.open('/changepassword.php?action=entry&amp;group=<?//=$contents['curr_group_id']?>','ChangePassword','width=500,height=650,navigation toolbar=disabled,left=200,top=200')" >
						    Change Administrator Accounts Settings
						  </a>
						</li> -->
					<?php endif; ?>
					
			  <? elseif ($contents['is_login'] && !$contents['is_adminLogged'] && !$contents['is_advisor']): ?>		
				  	<li>
				  		<a href="/messages.php?act=messageGroupStaff&gID=<?php echo $contents['curr_group_id'] ?>">
						    Message Staff
						</a>
				  	</li>
			  <? endif;?>
				<? if ("" != trim($contents['obj_group']->getGroupMembershipLink('grouphome'))): ?>				
				   <li> 
					<?= $contents['obj_group']->getGroupMembershipLink('grouphome') ?>				
					</li>
				<? endif;?>
			</ul>			
			<!-- End Profile Actions -->					
		</div>
		<!-- End Details -->
		
		<?php $showNotifications = ($contents['is_owner'] || $contents['is_superStaffLogged']) && ($contents['count_request_list'] || $contents['count_messages'] || $contents['num_entry_drafts']) ?>
		<?php $hasOtherNotices = (isset($contents['count_request_list']) && $contents['count_request_list'] > 0) || (isset($contents['num_entry_drafts']) && $contents['num_entry_drafts'] > 0) ?>
		
		<div class="notifications" id="notifications" style="display:<?php echo $showNotifications ? '' : 'none' ?>">
			<strong>You have </strong>
			<?php $newMessagesCount = isset($contents['count_messages']) ? $contents['count_messages'] : 0 ?>
			<span id="message_count_status" style="<?php if ($newMessagesCount < 1) : ?> display:none <?php endif; ?>">
				<a href = "/messages.php?gID=<?php echo $contents['curr_group_id'] ?>">
					<span id="new_messages_count"><?php echo $newMessagesCount . ' New Message' . ($newMessagesCount > 1 ? 's' : '') ?></span></a><?php echo ($hasOtherNotices ? ',' : '') ?>
				
			</span>
			<script type="text/javascript">
				var newMessagesCount = <?php echo $newMessagesCount ?>;
				var hasOtherNotices = <?php echo $hasOtherNotices ? 'true' : 'false' ?>;
			</script>
			
			<? if ($showNotifications) : ?>
				
				<? if( $contents['count_request_list']):?>				
					  <a href="/members.php?gID=<?=$contents['curr_group_id']?>&amp;mode=6">
						 <?php echo $contents['count_request_list']?> request(s) to join the group
					  </a>				
				<? endif; ?>	
				
				<? if (($contents['count_request_list']) && $contents['num_entry_drafts']): ?>, <? endif; ?>	
				
				<? if($contents['num_entry_drafts']):?>				
					  <a href="/journal_draft/view_saved_drafts/group/<?php echo $contents['curr_group_id']; ?>">
					  	<?php echo $contents['num_entry_drafts']; ?> Unfinished Entr<? if (1 < $contents['num_entry_drafts']) { ?>ies <? } else { ?>y<? } ?>
					  </a>				
				<? endif;?>
			<? endif;?>
		</div>				
	  <div id="group_info">     
			<span id="morelesscontent">
			  <?=$this->_e($full_content);?>
			</span>			
			<div id="moreless_link" style="display:none;">
			  <a href="javascript:void(0)" id="moreless">More &darr;</a>
			</div>
	  </div>
  </div>
</div>

<? // no effect to UI ?>
<div id = "js_full_content" style = "display:none;">
  <?php echo nl2br($this->_e($full_content)) ?>
</div>
<div id = "truncated_content" style = "display:none;">
  <?php echo nl2br($this->_e($truncated_content)) ?>
</div>


<script type="text/javascript">
  //<![CDATA[
	//jQuery.noConflict();
  
	if (40 < parseInt(jQuery('#morelesscontent').height()) ) {
	  	document.getElementById('moreless_link').style.display = "";
	  	jQuery('#morelesscontent').html(jQuery('#truncated_content').html()+"...");
		//if( null == jQuery("#additional_info").html() ){
		//	jQuery("#moreless_link").hide();
		//}
	}

    (function($){
	    _is_shown = false;
	    $(document).ready(function(){
		    $('#moreless').click(function(){
		      if(!_is_shown){
		        $('#morelesscontent').slideDown('slow').html($('#js_full_content').html());
		        $('#additional_info').show();
				    $(this).html('Less &uarr;');
				    _is_shown = true;
			    }
			    else{
					if (40 < parseInt(jQuery('#morelesscontent').height()) ) {
				    	$('#morelesscontent').slideDown('slow').html($('#truncated_content').html()+'...');
					}    
					$('#additional_info').hide();
				    $(this).html('More &darr;');
				    _is_shown = false;
			    }
		    });
	    });
    })(jQuery);
  //]]>
  </script>
<? // end of no effect to UI ?>