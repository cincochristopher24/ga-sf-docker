<?php
	/**
	 * @(#) Class.SubGroupComponent.php
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - 02 10, 09
	 */
	
	class SubGroupComponent {
		/**
		 * Handles the initialization of categories with subgroups template.
		 * 
		 * @param array $props The values used in creation of the template.
		 * @return string Returns the created template with the data.
		 */
		static function fetchCategoriesWithSubGroupsTpl(array $props){
			$tpl = new Template();
			$tpl->set_path("travellog/views/subgroups/");
			$tpl->set("iterator", $props['iterator']);
			$tpl->set("message", $props['message']);
			$tpl->set("parent", $props['parent']);
			$tpl->set("access", $props['access']);
			$tpl->set("categoryCount", $props['categoryCount']);
			$tpl->set("subgroupsCount", $props['subgroups_count']);
			$tpl->set("isAdminLogged", $props['isAdminLogged']);
			$tpl->set("keyword", $props['searchKey']);
			$tpl->set("numPerPage", $props['numRows']);
			$tpl->set("category_name", isset($props['category_name']) ? $props['category_name'] : "");
			$tpl->set("is_category_searching", $props['is_category_searching']);
			$tpl->set("is_searching", $props['is_searching']);
			
			return preg_replace("/(\s)+/"," ", $tpl->fetch("tpl.IncCategoriesWithSubGroups.php"));
		}
		
		/**
		 * Handles the initialization of category assigner template.
		 * 
		 * @param SqlResourceIterator $iterator The sql resource iterator of categories.
		 * @return string Returns the created template with the data.
		 */
		static function fetchCategoryAssignerTpl(SqlResourceIterator $iterator){
			$tpl = new GAHtmlTemplate();
			$tpl->setTemplate("travellog/views/subgroups/tpl.IncCategoryAssigner.php");
			$tpl->set("iterator", $iterator);
			
			return $tpl->fetch();
		}
		
		/**
		 * Handles the initialization of featured subgroup template.
		 * 
		 * @param array $props The values used in creation of the template.
		 * @return string Returns the created template with the data.
		 */
		static function fetchFeaturedSubGroupsTpl(array $props){
			$tpl = new Template();
			$tpl->set_path("travellog/views/subgroups/");
			$tpl->set("iterator", $props['iterator']);
			$tpl->set("categoryCount", $props['categoryCount']);
			$tpl->set("grpID", $props['parent']->getGroupID());
			$tpl->set("parent", $props['parent']);
			$tpl->set("page", $props['page']);
			
			return preg_replace("/(\s)+/", " ", $tpl->fetch("tpl.IncViewModeFeaturedSubGroupList.php"));			
		}
		
		/**
		 * Handles the initialization of the subgroups template.
		 * 
		 * @param array $props The values used in creation of the template.
		 * @return string Returns the created template with data.
		 */
		static function fetchSubGroups(array $props){
			$paging = new Paging($props['subgroups_count'], $props['page'], "gID=".$props['parent']->getGroupID()."&mode=list&txtGrpName=".$props['searchKey']."&tabAction=".$props['tabAction'], $props['numRows']);			
			
			$tpl = new Template();
			$tpl->setTemplate("travellog/views/subgroups/tpl.IncSubGroupList.php");
			$tpl->set("iterator", $props['iterator']);
			$tpl->set("paging", $paging);
			$tpl->set("numPages", ceil($props['subgroups_count']/$props['numRows']));
			$tpl->set("parent", $props['parent']);
			$tpl->set("subgroupsCount", $props['subgroups_count']);
			$tpl->set("categoryCount", $props['categoryCount']);
			$tpl->set("keyword", $props['searchKey']);
			$tpl->set("access", $props['access']);
			$tpl->set("page", $props['page']);
			$tpl->set("isAdminLogged", $props['isAdminLogged']);
			$tpl->set("is_searching", $props['is_searching']);
			
			return preg_replace("/(\s)+/", " ", $tpl->fetch());			
		}
		
		/**
		 * Handles the addition or deletion of category for uncategorized subgroups.
		 * 
		 * @param AdminGroup $group The parent admin group.
		 * @return CustomizedSubGroupCategory Returns the uncategorized category of the given group.
		 */
		static function saveUncategorized(AdminGroup $group){
			$category = $group->getCategoryForUncategorizedSubGroups();
			$factory = SubGroupFactory::instance($group->getGroupID());
			$ids = $factory->getSubGroupIDsWithoutCategoryInfo();
			if (is_null($category) AND 0 < count($ids)) {
				$category = new CustomizedSubGroupCategory();
				$category->setName("Groups");
				$category->setParentID($group->getGroupID());
				$category->setIsUncategorized(1);
				$category->setCategoryRank(99999);
				$category->save();
				$category->addSubGroups($ids);
			}
			else if (!is_null($category) AND 0 < count($ids)) {
				$category->addSubGroups($ids);
			}
			else if (!is_null($category) AND 0 == $category->getSubGroupsCount()) {
				$category->emptyCategory();
				$category->delete();
			}
			
			return $category;			
		}
	}
	
