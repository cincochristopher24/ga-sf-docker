<p>
	<?=nl2br($journalDescription)?>
</p>

<p>
	<?php if( $hasEntry ): ?>
		<a href="<?=$entryLink?>"><?=$entryTitle?></a>
		<div>
			<?=nl2br($entryDescription)?>
		</div>
	<?php else: ?>
		Journal doesn't contain an entry.
	<?php endif; ?>
</p>

<?php if( $hasEntry && $actionLinksVisible ): ?>
	<p>
	
		<?php if( isset($ownerGroup) ): ?>
			<a id="lnkFeatureAdminJournal_<?=$travelID?>" href="javascript:void(0)" onclick="<?=$onClickActions['FEATURE_ACTION']?>; return false;" class="<?=$featureLinkClass?>"><?=$featureLinkLabel?></a>
		<?php else: ?>
			<a id="lnkApproveMemberJournal_<?=$travelID?>" href="javascript:void(0)" onclick="<?=$onClickActions['APPROVE_ACTION']?>; return false;" class="<?=$approveLinkClass?>"><span id="approve<?=$travelID?>"><?=$approveLinkLabel?></span></a>
			<a id="lnkFeatureMemberJournal_<?=$travelID?>" href="javascript:void(0)" onclick="<?=$onClickActions['FEATURE_ACTION']?>; return false;" class="<?=$featureLinkClass?>"><span id="feature<?=$travelID?>"><?=$featureLinkLabel?></span></a>
			<?php if( !$related ): ?>
				<a id="lnkToUnapprovedMemberJournal_<?=$travelID?>" href="javascript:void(0)" onclick="<?=$onClickActions['TO_UNAPPROVE_ACTION']?>; return false;">To Unapproved</a>
			<?php endif; ?>
		<?php endif; ?>
		<img src="/images/loading_small.gif" id="ProcessingStatus_<?=$feedID?>" style="display:none;" />
		<input type="hidden" id="JournalFeed_<?=$travelID?>" value="<?=$feedID?>" />
		
		<?php if( isset($ownerGroup) ): ?>
			<input type="hidden" id="privilegedGroupID_<?=$travelID?>" value="<?=$ownerGroup->getID()?>" />
		<?php else: ?>
			<input type="hidden" id="privilegedGroupID_<?=$travelID?>" value="<?=$currentGroup->getID()?>" />
		<?php endif; ?>
	
	</p>
<?php endif; ?>