<p>
	<?php if( $messageTooLong ): ?>
		<span id="messageHalf_<?=$feedID?>"><?=nl2br($messageHalf)?>...</span>
	<?php endif; ?>
	<span id="messageFull_<?=$feedID?>" <?if($messageTooLong):?>style="display:none;"<?endif;?> >
		<?=nl2br($message)?>
	</span>			
	<br />
	<span class="jLeft">
		<? 
		/*** 
		 * COMMENTED per NALDZ mandate 03-23 series of 2011 - nash
		 <a id="lnkDiscussionPostReply_<?=$postID?>" name="DiscussionReply_<?=$discussionID?>" href="/post/reply/<?=$discussionID?>" <?if($isArchived):?>style="display:none;"<?endif;?> >Reply</a> 
		 ***/
		?>
	</span>
	<span class="jRight">
		<?php if( $messageTooLong ): ?>
			<a id="expandMessage_<?=$feedID?>" href="" onclick="jQuery(this).hide(); jQuery('#messageHalf_<?=$feedID?>').hide('fast'); jQuery('#messageFull_<?=$feedID?>').slideDown('fast'); jQuery('#collapseMessage_<?=$feedID?>').show(); return false;"><img class="expand" src="/images/down-enabled.gif" align="right" border="0"></a>
			<a id="collapseMessage_<?=$feedID?>" href="" style="display:none;" onclick="jQuery(this).hide(); jQuery('#messageFull_<?=$feedID?>').slideUp('fast'); jQuery('#messageHalf_<?=$feedID?>').show('fast'); jQuery('#expandMessage_<?=$feedID?>').show(); return false;"><img class="expand" src="/images/up-enabled.gif" align="right" border="0"></a>
		<?php endif; ?>
	</span>
</p>