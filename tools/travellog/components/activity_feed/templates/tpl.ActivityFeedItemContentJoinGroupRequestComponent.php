<p>
	<div id="requestStatus_<?=$feedID?>" <?if(!$isDenied):?>style="display:none;"<?endif;?> >
		<?	if( $isDenied ): ?>
			This request has been denied.
		<?	endif; ?>
	</div>
	<span id="requestActionLinks_<?=$feedID?>" class="jRight">
		<a id="lnkApproveRequest_<?=$feedID?>" href="javascript:void(0)" onclick="groupFeeds.approveRequest(this.id); return false;">Approve</a> 
		<?	if( !$isDenied ): ?>
			<a id="lnkDenyRequest_<?=$feedID?>" href="javascript:void(0)" onclick="groupFeeds.denyRequest(this.id); return false;">Deny</a>
		<?	endif; ?>
	</span>
	<img src="/images/loading_small.gif" id="ProcessingStatus_<?=$feedID?>" style="display:none;" />
	<input type="hidden" id="requestedGroupID_<?=$feedID?>" value="<?=$groupID?>" />
	<input type="hidden" id="requestingTravelerID_<?=$feedID?>" value="<?=$travelerID?>" />
</p>