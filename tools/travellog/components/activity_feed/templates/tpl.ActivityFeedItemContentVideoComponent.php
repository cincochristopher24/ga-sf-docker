<?php if($hasVideos): ?>
	
	<?php foreach($videos AS $video): ?>
		<?php
			$onclick = "tb_open_new('','".$video->getPlayActionLink()."&height=360&width=510')";
		?>
		<div class="video jLeft">
			<a href="javascript: void(0)" onclick="<?=$onclick?>" class="jLeft">
				<div class="videomarker">
					<img src="/images/v3/discussions/videosign.gif" border="0" align="left"/>
				</div>
				<img width="92" height="62" border="0" src="<?=$video->getVideoImageUrl() ?>"/>
			</a>
			<a href="javascript: void(0)" onclick="<?=$onclick?>"><strong><?=$video->getTitle()?></strong></a><br>
			<span><?=$video->getDuration()?></span>
		</div>
	<?php endforeach; ?>
	
<?php elseif($create): ?>
	No videos were added when video album was created.
<?php else: ?>
	Videos uploaded were already deleted.
<?php endif; ?>