<?php if( $hasPhotos ): ?>
	<?php $cnt = 0;?>
	<?	foreach($photos AS $photo): ?>
		<div class="photo jLeft <?php if($cnt==5):?>clear<?php endif; ?>">
			<img width="<?=$photo->getAdjustedThumbnailWidth()?>" height="<?=$photo->getAdjustedThumbnailHeight()?>" border="0" src="<?=$photo->getPhotoLink('featured')?>"/>
		</div>
		<?php $cnt++; ?>
	<?	endforeach; ?>
<?php elseif( $create ): ?>
	No photos were uploaded when the album was created.
<?php else: ?>
	Photos uploaded were already deleted.	
<?php endif; ?>