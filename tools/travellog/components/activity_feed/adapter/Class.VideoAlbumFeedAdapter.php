<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");
	require_once("travellog/model/Class.VideoAlbum.php");

	class VideoAlbumFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mVideoAlbum = NULL;
		protected $mVideos = NULL;
		protected $mUploadCount = 0;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			
			try{
				$this->mVideoAlbum = new VideoAlbum($this->mFeed->getSectionID());
				if( !($this->mVideoAlbum instanceof VideoAlbum) ){
					$this->mVideoAlbum = NULL;
				}
			}catch(exception $e){
				$this->mVideoAlbum = NULL;
			}
			
			if( !is_null($this->mVideoAlbum) ){
				$rowsLimit = new RowsLimit(10,0);
				$videos =  $this->mVideoAlbum->getVideosUploadedByDay($rowsLimit,$this->mFeed->getFeedDate());
				$this->mVideos = $videos["videos"];
				$this->mUploadCount = $videos["recordCount"];
			}
			if( !$this->mUploadCount ){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			
			if( !is_null($this->mGroupOwner) && !is_null($this->mDoer) && !is_null($this->mVideoAlbum) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","created a VIDEO ALBUM");
				$feedItemTitleTpl->set("subjectTitle",$this->mVideoAlbum->getTitle());
				$feedItemTitleTpl->set("subjectLink",$this->mVideoAlbum->getFriendlyURL());
				$feedItemTitleTpl->set("ownerGroup",$this->mGroupOwner);
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else if( !is_null($this->mDoer) && !is_null($this->mVideoAlbum) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","created a VIDEO ALBUM");
				$feedItemTitleTpl->set("subjectTitle",$this->mVideoAlbum->getTitle());
				$feedItemTitleTpl->set("subjectLink",$this->mVideoAlbum->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else{
				return "";
			}
		}
		
		function getItemContent(){
			if( is_null($this->mVideoAlbum) ){
				return "";
			}else{
				$itemContentTpl = new Template();
				$itemContentTpl->set("hasVideos",0<$this->mUploadCount);
				$itemContentTpl->set("videos",$this->mVideos);
				$itemContentTpl->set("create",TRUE);
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentVideoComponent.php");
			}
		}
		
		function getTitleClass(){
			return "feedVideo";
		}
		
	}