<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");

	class UnknownFeedAdapter extends AbstractActivityFeedAdapter{
		public function __construct(){
			$this->mItemExists = FALSE;
		}
	}