<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");
	require_once("travellog/model/DiscussionBoard/Class.Post.php");
	
	class DiscussionPostFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mDiscussion = NULL;
		protected $mPost = NULL;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			
			try{
				$this->mPost = Post::getInstance($this->mFeed->getSectionID());
				$this->mDiscussion = $this->mPost->getParentDiscussion();
			}catch(Exception $e){
			}
				
			if( is_null($this->mDiscussion) || is_null($this->mPost) ){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			if( !is_null($this->mDoer) && !is_null($this->mDiscussion) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","replied to DISCUSSION");
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mDiscussion->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",$this->mDiscussion->getURL());
				$feedItemTitleTpl->set("discussionGroup",$this->mGroupOwner);
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else{
				return "";
			}
		}
		
		function getItemContent(){
			if( is_null($this->mPost) ){
				return "";
			}else{
				$message = htmlspecialchars(stripslashes(strip_tags($this->mPost->getMessage())),ENT_QUOTES);
				$messageTooLong = FALSE;
				if( strlen($message) > 250 ){
					$messageTooLong = TRUE;
					$messageHalf = substr($message,0,250);
				}
				$itemContentTpl = new Template();
				$itemContentTpl->set("feedID",$this->mFeed->getFeedID());
				$itemContentTpl->set("discussionID",$this->mDiscussion->getID());
				$itemContentTpl->set("isArchived",$this->mDiscussion->isArchived());
				$itemContentTpl->set("postID",$this->mPost->getID());
				$itemContentTpl->set("message",$message);
				$itemContentTpl->set("messageTooLong",$messageTooLong);
				$itemContentTpl->set("messageHalf", $messageTooLong ? $messageHalf : $message);
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentDiscussionPostComponent.php");
				
			}
		}
		
		function getItemDate(){
			return "Posted ".GaDateTime::descriptiveDifference($this->mFeed->getFeedDate(), date("Y-m-d H:i:s"))." ago";
		}
		
		function getTitleClass(){
			return "feedReply";
		}
	}