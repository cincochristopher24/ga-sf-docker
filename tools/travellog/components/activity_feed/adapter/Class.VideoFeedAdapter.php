<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");
	require_once("travellog/model/Class.VideoAlbum.php");
	
	class VideoFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mVideoAlbum = NULL;
		protected $mVideos = NULL;
		protected $mUploadCount = 0;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			
			try{
				$this->mVideoAlbum = new VideoAlbum($this->mFeed->getSectionRootID());
				if( !($this->mVideoAlbum instanceof VideoAlbum) ){
					$this->mVideoAlbum = NULL;
				}
			}catch(exception $e){
				$this->mVideoAlbum = NULL;
			}
			
			if( !is_null($this->mVideoAlbum) ){
				$rowsLimit = new RowsLimit(10,0);
				$videos =  $this->mVideoAlbum->getVideosUploadedByDay($rowsLimit,$this->mFeed->getFeedDate());
				$this->mVideos = $videos["videos"];
				$this->mUploadCount = $videos["recordCount"];
			}
			if( !$this->mUploadCount ){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			
			if( is_null($this->mVideoAlbum) ){
				return "";
			}else{
				if( !$this->mUploadCount ){
					$text = " added Videos to VIDEO ALBUM";
				}else{
					$text = " added ";
					$text .= 1 < $this->mUploadCount ? $this->mUploadCount." Videos to VIDEO ALBUM" : $this->mUploadCount." Video to VIDEO ALBUM";
				}
				
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text",$text);
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mVideoAlbum->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",$this->mVideoAlbum->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}
		}
		
		function getItemContent(){
			if( is_null($this->mVideoAlbum) ){
				return "";
			}else{
				$itemContentTpl = new Template();
				$itemContentTpl->set("hasVideos",0<$this->mUploadCount);
				$itemContentTpl->set("videos",$this->mVideos);
				$itemContentTpl->set("create",FALSE);
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentVideoComponent.php");
			}
		}
		
		function getTitleClass(){
			return "feedVideo";
		}
	}