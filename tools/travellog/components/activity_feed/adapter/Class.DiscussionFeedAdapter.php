<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");
	require_once("travellog/model/DiscussionBoard/Class.Discussion.php");
	
	class DiscussionFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mDiscussion = NULL;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			
			$discussionID = $this->mFeed->getSectionID();
			
			try{
				$this->mDiscussion = Discussion::createInstance($discussionID);
			}catch(Exception $e){	
			}
			
			if( is_null($this->mDiscussion) ){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			if( !is_null($this->mDoer) && !is_null($this->mDiscussion) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","created a DISCUSSION");
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mDiscussion->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",$this->mDiscussion->getURL());
				$feedItemTitleTpl->set("discussionGroup",$this->mGroupOwner);
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else{
				return "";
			}
		}
		
		function getItemContent(){
			if( is_null($this->mDiscussion) ){
				return "";
			}else{
				
				$topic = $this->mDiscussion->getParentTopic();
				$isArchived = ($this->mDiscussion->getStatus()==Discussion::ARCHIVED_STATUS || $topic->getStatus()==Topic::ARCHIVED_STATUS);
				
				if( $this->actionLinksVisible() ){
					$archiveLinkLabel = $isArchived ? "Activate" : "Archive";
					
					$pinLinkLabel = $this->mDiscussion->getIsFeatured() ? "Unpin Discussion" : "Pin Discussion";
					$pinLinkClass = $this->mDiscussion->getIsFeatured() ? "Unpin" : "Pin";
				
					$addToBaseLinkLabel = $this->mDiscussion->getHomepageShown() ? "Remove from Knowledge Base" : "Add to Knowledge Base";
					$addToBaseLinkClass = $this->mDiscussion->getHomepageShown() ? "Remove" : "Add";
									
					$showAddToKnowledgeBaseLink = FALSE;
					if( 0 < $this->mGroupOwner->getParentID() ){
						$loggedTraveler = $this->file_factory->getClass('Traveler',array($this->file_factory->invokeStaticClass('SessionManager', 'getInstance')->get('travelerID')));
						$isAdmin = $this->mGroupOwner->getAdministratorID() == $loggedTraveler->getTravelerID() || $this->mGroupOwner->isRegularStaff($loggedTraveler->getTravelerID());
						$showAddToKnowledgeBaseLink = $isAdmin ? TRUE : FALSE;
					}
				}
				
				$itemContentTpl = new Template();
				$itemContentTpl->set("feedID",$this->mFeed->getFeedID());
				$itemContentTpl->set("discussionID",$this->mDiscussion->getID());
				$itemContentTpl->set("isArchived",$this->mDiscussion->isArchived());
				
				if( $this->actionLinksVisible() ){
					$itemContentTpl->set("archiveLinkLabel",$archiveLinkLabel);
					$itemContentTpl->set("pinLinkLabel",$pinLinkLabel);
					$itemContentTpl->set("pinLinkClass",$pinLinkClass);
					$itemContentTpl->set("addToBaseLinkLabel",$addToBaseLinkLabel);
					$itemContentTpl->set("addToBaseLinkClass",$addToBaseLinkClass);
					$itemContentTpl->set("showAddToKnowledgeBaseLink", $showAddToKnowledgeBaseLink );
				}
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentDiscussionComponent.php");
			}
		}
		
		function getItemDate(){
			return "Posted ".GaDateTime::descriptiveDifference($this->mFeed->getFeedDate(), date("Y-m-d H:i:s"))." ago";
		}
		
		function getTitleClass(){
			return "feedReply";
		}
	}