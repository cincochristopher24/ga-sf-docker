<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");
	
	class JoinGroupRequestFeedAdapter extends AbstractActivityFeedAdapter{
		
		function getItemTitle(){
			if( !is_null($this->mGroupOwner) && !is_null($this->mDoer) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","requests to join");
				$feedItemTitleTpl->set("subjectTitle",$this->mGroupOwner->getName());
				$feedItemTitleTpl->set("subjectLink",$this->mGroupOwner->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else{
				return "";
			}
		}
		
		function getItemContent(){
			if( !is_null($this->mGroupOwner) && !is_null($this->mDoer) ){
				
				$isDenied = GanetRequestListService::hasGroupDeniedTravelerRequest($this->mDoer,$this->mGroupOwner);
				
				$itemContentTpl = new Template();
				$itemContentTpl->set("feedID",$this->mFeed->getID());
				$itemContentTpl->set("groupID",$this->mFeed->getGroupID());
				$itemContentTpl->set("travelerID",$this->mFeed->getTravelerID());
				$itemContentTpl->set("isDenied",$isDenied);
				
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentJoinGroupRequestComponent.php");
				
			}else{
				return "";
			}
		}
		
		function getTitleClass(){
			return "feedMember";
		}
	}