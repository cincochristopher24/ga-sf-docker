<?php

	require_once("travellog/factory/Class.FileFactory.php");
	require_once("travellog/model/Class.ActivityFeed.php");
	require_once("travellog/components/activity_feed/adapter/Class.ActivityFeedAdapterInterface.php");

	abstract class AbstractActivityFeedAdapter implements ActivityFeedAdapterInterface{
		protected $file_factory = NULL;
		
		protected $mFeed = NULL;
		protected $mDoer = NULL;
		protected $mGroupOwner = NULL;
		
		protected $mCurrentGroup = NULL;
		
		protected $mActionLinksVisible = TRUE;
		protected $mViewMode = "DASHBOARD";
		protected $mItemExists = TRUE;
		
		protected $mTimezone = 0;
		
		protected $mOnClickActions = NULL;
		
		public function __construct(ActivityFeed $feed){
			$this->mFeed = $feed;
			
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('SessionManager');
			
			if( 0 < $feed->getTravelerID() ){
				$this->mDoer = $this->file_factory->getClass('Traveler', array($feed->getTravelerID()));
			}
			if( 0 < $feed->getSectionType() && 0 < $feed->getGroupID() ){
				$groups = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($feed->getGroupID()));
				if( count($groups) ){
					$this->mGroupOwner = $groups[0];
				}else{
					$this->mItemExists = FALSE;
				}
			}
		}
		
		function getID(){
			return $this->mFeed->getFeedID();
		}
		
		function getDoerUserName(){
			if( is_null($this->mDoer) ){
				return "";
			}else{
				return $this->mDoer->getUserName();
			}
		}
		
		function getDoerFullName(){
			if( is_null($this->mDoer) ){
				return "";
			}else{
				return $this->mDoer->getFullName();
			}
		}
		
		function getDoerLink(){
			if( is_null($this->mDoer) ){
				return "";
			}else{
				return $this->mDoer->getFriendlyURL();
			}
		}
		
		function getDoerPhotoThumbnail(){
			if( is_null($this->mDoer) ){
				return "";
			}else{
				return $this->mDoer->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');
			}
		}
		
		function getItemTitle(){
			return "";
		}
		
		function getItemContent(){
			return "";
		}
		
		function getItemDate(){
			/*$dateTimeTokens = explode(" ",$this->mFeed->getFeedDate());
			$dateTokens = explode("-",$dateTimeTokens[0]);
			$timeTokens = explode(":",$dateTimeTokens[1]);
			$clientDate = date("Y-m-d H:i:s", mktime($timeTokens[0], $timeTokens[1], $timeTokens[2], $dateTokens[1], $dateTokens[2], $dateTokens[0]) + $this->getTimezone());
			$itemDate = GaDateTime::create($clientDate);
			return $itemDate->htmlDateFormat();*/
			return GaDateTime::descriptiveDifference($this->mFeed->getFeedDate(), date("Y-m-d H:i:s"))." ago";
		}
		
		function getTitleClass(){
			return "";
		}
		
		function itemExists(){
			return $this->mItemExists;
		}
		
		function isDoerLoggedViewer(){
			return $this->mDoer->getTravelerID() == $this->file_factory->invokeStaticClass('SessionManager', 'getInstance')->get('travelerID');
		}
		
		public function setTimezone($timezone=0){
			$this->mTimezone = 0;
		}
		
		public function getTimezone(){
			return $this->mTimezone;
		}
		
		public function setCurrentGroup(AdminGroup $group){
			$this->mCurrentGroup = $group;
		}
		
		public function hideActionLinks(){
			$this->mActionLinksVisible = FALSE;
		}
		
		public function showActionLinks(){
			$this->mActionLinksVisible = TRUE;
		}
		
		public function setViewMode($mode="DASHBOARD"){
			$this->mViewMode = $mode;
		}
		
		public function getViewMode(){
			return $this->mViewMode;
		}
		
		protected function actionLinksVisible(){
			return $this->mActionLinksVisible;
		}
		
		public function setItemExists($exists=TRUE){
			$this->mItemExists = $exists;
		}
		
		public function getItem(){
			return false;
		}
		
		public function getOnClickActions(){
			return $this->mOnClickActions;
		}
		
		public function setOnClickActions($actions){
			$this->mOnClickActions = $actions;
		}
		
		public function getGroupOwner(){
			return $this->mGroupOwner;
		}
		
		public function render(){
			$template = new Template();
			$template->set("feed",$this);
			$template->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemComponent.php");
		}
	}