<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");

	class NewMemberFeedAdapter extends AbstractActivityFeedAdapter{
		
		function getItemTitle(){
			if( !is_null($this->mGroupOwner) && !is_null($this->mDoer) ){
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text","became a member of");
				$feedItemTitleTpl->set("subjectTitle",$this->mGroupOwner->getName());
				$feedItemTitleTpl->set("subjectLink",$this->mGroupOwner->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}else{
				return "";
			}
		}
		
		function getItemDate(){
			if( "DASHBOARD" == $this->getViewMode() ){
				return parent::getItemDate();
			}else{
				$feedDate = GaDateTime::create($this->mFeed->getFeedDate());
				$auxDate = GaDateTime::create($this->mFeed->getAuxDate());
				switch($this->mFeed->getSectionRootID()){
					case ActivityFeed::MEMBER_BY_DEFAULT	:	$dateLabel = trim($feedDate->htmlDateFormat());
																break;
					case ActivityFeed::MEMBER_BY_INVITE		: 	$dateLabel = "Date invited: ".trim($auxDate->htmlDateFormat())."<br />Date invitation was accepted: ".trim($feedDate->htmlDateFormat());					
																break;
					case ActivityFeed::MEMBER_BY_REQUEST	:	$dateLabel = "Date requested to join group: ".trim($auxDate->htmlDateFormat())."<br />Date request was approved: ".trim($feedDate->htmlDateFormat());
																break;
				}
				return $dateLabel;
			}
		}
		
		function getTitleClass(){
			return "feedMember";
		}
	}