<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");

	class EntryVideoFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mJournalEntry = NULL;
		protected $mVideos = NULL;
		protected $mUploadCount = 0;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			
			try{
				$this->file_factory->registerClass('TravelLog');
				$this->mJournalEntry = $this->file_factory->getClass('TravelLog', array($this->mFeed->getSectionRootID()));
				if( !($this->mJournalEntry instanceof TravelLog) ){
					$this->mJournalEntry = NULL;
				}
			}catch(exception $e){
				$this->mJournalEntry = NULL;
			}			
			if( !is_null($this->mJournalEntry) ){
				$rowsLimit = new RowsLimit(10,0);
				$videosArray = $this->mJournalEntry->getVideosUploadedByDay($rowsLimit,$this->mFeed->getFeedDate());
				$this->mVideos = $videosArray["videos"];
				$this->mUploadCount  = $videosArray["recordCount"];
			}
			if( !$this->mUploadCount ){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			
			if( is_null($this->mJournalEntry) ){
				return "";
			}else{
				if( !$this->mUploadCount ){
					$text = "added Videos to JOURNAL ENTRY";
				}else{
					$text = " added ";
					$text .= 1 < $this->mUploadCount ? $this->mUploadCount." Videos to JOURNAL ENTRY" : $this->mUploadCount." Video to JOURNAL ENTRY";
				}
				
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text",$text);
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mJournalEntry->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",$this->mJournalEntry->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}
		}
		
		function getItemContent(){
			if( is_null($this->mJournalEntry) ){
				return "";
			}else{
				$itemContentTpl = new Template();
				$itemContentTpl->set("hasVideos",0<$this->mUploadCount);
				$itemContentTpl->set("videos",$this->mVideos);
				$itemContentTpl->set("create",FALSE);
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentVideoComponent.php");
			}
		}
		
		function getTitleClass(){
			return "feedVideo";
		}
		
		function getJournalEntry(){
			return $this->mJournalEntry;
		}
	}