<?php
	require_once("travellog/components/activity_feed/adapter/Class.AbstractActivityFeedAdapter.php");

	class EntryPhotoFeedAdapter extends AbstractActivityFeedAdapter{
		
		protected $mJournalEntry = NULL;
		protected $mPhotos = NULL;
		protected $mUploadCount = 0;
		
		public function __construct(ActivityFeed $feed){
			parent::__construct($feed);
			
			try{
				$this->file_factory->registerClass('TravelLog');
				$this->mJournalEntry = $this->file_factory->getClass('TravelLog', array($this->mFeed->getSectionRootID()));
				if( !($this->mJournalEntry instanceof TravelLog) ){
					$this->mJournalEntry = NULL;
				}
			}catch(exception $e){
				$this->mJournalEntry = NULL;
			}
			if( !is_null($this->mJournalEntry) ){
				$rowsLimit = new RowsLimit(10,0);
				$photosArray = $this->mJournalEntry->getPhotosUploadedByDay($rowsLimit,$this->mFeed->getFeedDate());
				$this->mPhotos = $photosArray["photos"];
				$this->mUploadCount  = $photosArray["recordCount"];
			}
			if( !$this->mUploadCount ){
				$this->mItemExists = FALSE;
			}
		}
		
		function getItemTitle(){
			
			if( is_null($this->mJournalEntry) ){
				return "";
			}else{
				if( !$this->mUploadCount ){
					$text = " uploaded Photos to JOURNAL ENTRY";
				}else{
					$text = " uploaded ";
					$text .= 1 < $this->mUploadCount ? $this->mUploadCount." Photos to JOURNAL ENTRY" : $this->mUploadCount." Photo to JOURNAL ENTRY";
				}
				
				$feedItemTitleTpl = new Template();
				$feedItemTitleTpl->set("doer",$this->mDoer);
				$feedItemTitleTpl->set("doerviewer",$this->isDoerLoggedViewer());
				$feedItemTitleTpl->set("text",$text);
				$feedItemTitleTpl->set("subjectTitle",htmlspecialchars(stripslashes($this->mJournalEntry->getTitle()),ENT_QUOTES));
				$feedItemTitleTpl->set("subjectLink",$this->mJournalEntry->getFriendlyURL());
				return $feedItemTitleTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemTitleComponent.php");
			}
		}
		
		function getItemContent(){
			if( is_null($this->mJournalEntry) ){
				return "";
			}else{
				$itemContentTpl = new Template();
				$itemContentTpl->set("hasPhotos",0<$this->mUploadCount);
				$itemContentTpl->set("photos",$this->mPhotos);
				$itemContentTpl->set("create",FALSE);
				return $itemContentTpl->out("travellog/components/activity_feed/templates/tpl.ActivityFeedItemContentPhotoComponent.php");
			}
		}
		
		function getTitleClass(){
			return "feedPhoto";
		}
		
		function getJournalEntry(){
			return $this->mJournalEntry;
		}
	}