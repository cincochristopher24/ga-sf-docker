<div class="content">
	<div id="groups_list_main">
    <?php if (0 == $groups_count): ?>	
      <?php if ($is_searching): ?>
        <div class="containerbox">Your search has yielded no results.</div>
      <?php else: ?>
        <p class="help_text">
          <span>You are not a member of any group yet</span>.<br /> Check out the <a href="/subgroups/category_search/"><strong>groups page</strong></a>!
        </p>          
      <?php endif; ?>	
    <?php endif; ?>
  
    <ul>
      <?php foreach($groups as $group): ?>

      <li>	
        <a class="group_thumb" href="<?php echo $group->getFriendlyURL(); ?>">
          <img src="<?php echo $group->getGroupPhoto()->getPhotoLink('standard'); ?>" alt="<?php echo $group->getName(); ?>" >
        </a>					
	
        <div class="groups_info">
          <strong>
            <a href="<?php echo $group->getFriendlyURL(); ?>">
              <?php echo $group->getName(); ?>
            </a>
          </strong>
      
          <p>
            <?php echo StringFormattingHelper::wordTrim($group->getDescription(), 40); ?>
            <?php if (40 < count(explode(' ', strip_tags($group->getDescription())))) : ?>
              <?php echo '&#8230;'; ?>
            <?php endif ; ?>
         
            <a href="<?php echo $group->getFriendlyURL(); ?>" title="Read more about the <?php echo $group->getName() ?> travel community">
              Read More
            </a>
          </p>
	  		 															
          <?php if (trim($group->getGroupMembershipLink('mygroups')) !== ''): ?>
            <p class="action">
              <?php echo $group->getGroupMembershipLink('mygroups', $page) ?>
            </p>
          <?php endif; ?>
        </div>
      </li>	     
    <?php endforeach; ?>
  </ul>

  <?php $paging->showPagination(array('label'=>' Groups')); ?>
  </div>
</div>

	
