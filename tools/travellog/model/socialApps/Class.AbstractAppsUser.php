<?php
	 
	abstract class AbstractAppsUser{
	
		protected $mUserID,
				  $mContainer,
				  $mContainerID,
				  $mTravelerID,
				  $mDotNetEmail,
				  $mDotNetUsername;
		
		// SETTERS
		function setUserID($userID=0){
			$this->mUserID = (int)$userID;	
		}
		
		function setContainer($container){
			$this->mContainer = $container;
		}

		function setContainerID($containerID=0){
			$this->mContainerID = (int)$containerID;	
		}

		function setTravelerID($travelerID=0){
			$this->mTravelerID = (int)$travelerID;
		}
		
		function setDotNetEmail($dotNetEmail){
			$this->mDotNetEmail = $dotNetEmail;
		}

		function setDotNetUsername($dontNetUsername){
			$this->mDotNetUsername = $dontNetUsername;
		}
		
		// GETTERS
		function getUserID($userID=0){
			return $this->mUserID;	
		}
		
		function getContainer(){
			return $this->mContainer;
		}

		function getContainerID(){
			return $this->mContainerID;	
		}

		function getTravelerID(){
			return $this->mTravelerID;
		}
		
		function getDotNetEmail(){
			return $this->mDotNetEmail;
		}

		function getDotNetUsername(){
			return $this->mDotNetUsername;
		}
	}
?>
