<?php
	/*
	 * Created on Jul 8, 2008
	 */
	 
	require_once 'utility/Class.dbHandler.php';
	require_once 'utility/Class.iRecordset.php';
	require_once 'travellog/helper/ganet_web_service/Class.wsDbHelper.php';    
	require_once 'travellog/helper/ganet_web_service/Class.wsHelper.php';      
	require_once 'travellog/model/socialApps/mytraveljournals/Class.AbstractMyTravelJournalApp.php';
	require_once 'Class.WebServiceIni.php';
	require_once 'Class.ToolMan.php';
	 
	class WebServiceUser{
	
		protected $id = 0,
				$travelerID = 0,
				$domain,
				$wsID,
				$name,
				$dotNetEmail,
				$dotNetUsername;
				
		protected $conn;		
		
		function __construct($id, $domain) {                    
			
			if( wsHelper::isValidDomain($domain) ){				
				$this->domain = $domain;
				$this->wsID = $id;

				if(0 < $id ){
					$field = wsDbHelper::getUserField($domain);
					$this->conn = new dbHandler();
					$this->conn->setDB('SocialApplication');
					$sql = "SELECT Travel_Logs.tblTraveler.email,`username`, `userID`,`tbUser`.`travelerID` 
							FROM tbUser LEFT JOIN Travel_Logs.tblTraveler 
							ON `tbUser`.`travelerID` = `tblTraveler`.`travelerID` 
							WHERE {$field} = {$id} 
							LIMIT 1";
//					$sql = "SELECT Travel_Logs.tblTraveler.email,`username`, `userID`,`tbUser`.`travelerID` FROM tbUser LEFT JOIN Travel_Logs.tblTraveler ON `tbUser`.`travelerID` = `tblTraveler`.`travelerID` WHERE {$field} = {$id} AND (SELECT count(*) FROM tblTravelJournalUsers WHERE tblTravelJournalUsers.userID = tbUser.userID) > 0 LIMIT 1";
					$rs = new iRecordSet($this->conn->execQuery($sql));
//				$this->id = $rs->retrieveRecordCount(); 			
					if( $rs->retrieveRecordCount() ) {
						$this->id = $rs->getUserID();
						$this->travelerID = $rs->getTravelerID();
						$this->dotNetUsername = $rs->getUsername();
						$this->dotNetEmail = $rs->getEmail();
				    }
					else
						$this->setID(0);
				}
				else
					$this->setID(0);
			}
			else{
				return false;
			}
			
		}
		
		function setID($id){
			$this->id = (int)$id;	
		}
		
		function setTravelerID($travelerID){
			$this->travelerID = (int)$travelerID;
		}
		
		function getDomain(){
			return $this->domain;
		}
		
		function getID(){
			return $this->id;
		}
		
		function getWsID(){
			return $this->wsID;
		}
		
		function getTravelerID(){
			return $this->travelerID;
		}
		
		function getDotNetUsername(){
			return $this->dotNetUsername;
		}

		function getDotNetEmail(){
			return $this->dotNetEmail;
		}
		
		function getMyTravelJournals(){
			return AbstractMyTravelJournal::getUserJournals($this->id,$this->domain);
		}
		
		function save(){
			$this->conn = new dbHandler();
			$this->conn->setDB('SocialApplication');
			$field = wsDbHelper::getUserField($this->domain);
			$sql =	"INSERT INTO `tbUser` SET " .
					"{$field} = {$this->wsID} " .
					"ON DUPLICATE KEY UPDATE " .
					"{$field} = $this->wsID";
			$this->conn->execQuery($sql);
			if( $id = $this->conn->getLastInsertedID() )
				$this->id = $id;	
			return $this->id;		
		}
		
		function addApp($appName){
			
			switch($appName){
				case WebServiceIni::MY_TRAVEL_MAP:
					break;
				case WebServiceIni::MY_TRAVEL_PLANS:
					break;
				case WebServiceIni::MY_TRAVEL_BIO:
//					MyTravelBioApp::addUser($this->id);
					break;
//				case WebServiceIni::MY_TRAVEL_JOURNALS:
//					AbstractMyTravelJournalApp::addUser($this->id);
//					break;
			}	
			
		}
		
		function hasApp($appName){
			
			$hasApp = false;
			
			switch($appName){
				case WebServiceIni::MY_TRAVEL_MAP:
					break;
				case WebServiceIni::MY_TRAVEL_PLANS:
					break;
				case WebServiceIni::MY_TRAVEL_BIO:
					break;
				case WebServiceIni::MY_TRAVEL_JOURNALS:
//					$hasApp = AbstractMyTravelJournalApp::hasMyTravelJournalApp($this->id);
					break;
			}
			
			return $hasApp;
			
		}
		
		/**
		 * static functions
		 */
		
		static function addUser($wsID, $domain){
			$user = new WebServiceUser($wsID, $domain);
			$user->save();
			return $user->getID();
		}
		 
/*		static function isSynchronized($id, $domain){		
			if( 0 < $id && wsHelper::isValidDomain($domain) ){
				$field = wsDbHelper::getUserField($domain);
				$conn = new dbHandler();
				$conn->setDB('SocialApplication');
				$sql = "SELECT `userID` FROM tbUser WHERE {$field} = {$id} AND `travelerID` <> 0 LIMIT 1 ";
				$rs = new iRecordSet($conn->execQuery($sql));
				if( $rs->retrieveRecordCount() ){
					return $rs->getUserID();
				}
			}
			return false;			
		}
*/
		static function isSynchronized($userID){
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT `userID` FROM tblTravelJournalUsers WHERE userID = {$userID} LIMIT 1";
			$rs = new iRecordSet($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() ){
				return $rs->getUserID();
			}
			return false;			
		}
										
		static function getWebServiceUserByTravelerID($id){			
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT `userID` FROM `tbUser` WHERE `travelerID` = {$id}";
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() ) 
				return $rs->getUserID(0);
			return false;				
		}

		static function getFacebookIDByTravelerID($id){			
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT `facebookID` FROM `tbUser` WHERE `travelerID` = {$id}";
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() ) 
				return $rs->getFacebookID(0);
			return false;				
		}
		
		static function getTravelerIDsWithFacebookID(){			
			$travelerIDs = array();
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT `travelerID` FROM `tbUser` WHERE `travelerID` != 0 AND facebookID != 0";
			$rs = new iRecordset($conn->execQuery($sql));
			for($i=0; $i<$rs->retrieveRecordCount(); $i++) 
				$travelerIDs[] = $rs->getTravelerID($i);
			
			return $travelerIDs;
		}
		
		static function getMTJUsersCredentials($userID = 0, $limit = 20){
			if(!$userID) $userID = 0;
			$user = array();
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT a.userID, a.travelerID, a.facebookID 
					FROM tbUser AS a
						LEFT JOIN tblTravelJournalUsers AS b
						ON a.userID = b.userID
					WHERE a.travelerID != 0 
						AND a.facebookID != 0 
						AND a.userID > $userID
					ORDER BY a.userID ASC LIMIT $limit";

			$rs = new iRecordset($conn->execQuery($sql));
			for($i=0; $i<$rs->retrieveRecordCount(); $i++) {
				$user[$i] = array(
					'userID' 	 => $rs->getUserID($i), 
					'travelerID' => $rs->getTravelerID($i),
					'facebookID' => $rs->getFacebookID($i)
				);			
			}
			return $user;				
		}

		static function isMTJValidUserID($userID = 0){
			if(!$userID) $userID = 0;
			$user = array();
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT a.userID AS userID FROM tbUser AS a
					LEFT JOIN tblTravelJournalUsers AS b ON a.userID = b.userID
					WHERE a.travelerID != 0 AND a.facebookID != 0 AND a.userID = $userID";
					
			$rs = new iRecordset($conn->execQuery($sql));
			if($rs->retrieveRecordCount()){
				return $rs->getUserID(0);
			} else return false;
		}
		
		static function getTravelerIDByUserID($userID){
			
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT `travelerID` FROM `tbUser` WHERE `userID` = {$userID}";
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() ) 
				return $rs->getTravelerID();
			return false;			
		}

		static function isOldUser($id){			
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT userID FROM tbUser WHERE travelerID = 0 AND userID = {$id}";
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount()) 
				return $rs->getUserID();
			return false;			
		}
		
		static function removeOldUser($id){			
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "DELETE FROM tbUser WHERE userID = {$id}";
			$conn->execQuery($sql);
			return $id;
		}
		
		// created by nash to check if the traveler account a third party user
		// wants to sync his account with is already sync or not yet to other accounts
		static function isTravelerSynchronized($travelerID,$domain){
			$field = wsDbHelper::getUserField($domain);
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "SELECT {$field} from `tbUser` where travelerID = {$travelerID} limit 1";
			$rs = new iRecordset($conn->execQuery($sql));
			if( $rs->retrieveRecordCount() > 0){
				foreach($rs as $row){
					if(0 < $row[$field]) return true;
					else return false;
				}
			}					
		}
		
		// this function use to add new traveler to web service user when new .net user has added
		static function addTravelerToWebServiceUser($travelerID) {
			$conn = new dbHandler();
			$conn->setDB('SocialApplication');
			$sql = "INSERT INTO tbUser SET travelerID={$travelerID}";
			$conn->execQuery($sql);
		}
		
		// added by nash
		static function addTravelerToTravelBioUser($travelerID = 0) {
			if(0 < $travelerID){
				require_once 'ganet_web_services/model/Class.MyTravelBioApp.php';
				MyTravelBioApp::addTravelerTravelBio($travelerID);
			}
		}
	} 
	
?>
