<?php
/**
*	Class.fbInitialization.php
*	Purpose: To get some important values from Facebook.
*	@author: Adelbert D. Silla
*	May 13, 2008
*/
	require_once('Class.GaDateTime.php');
	require_once('Class.ToolMan.php');
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');

	class fbInitialization{
		public static function dbConnect(){
			$db = new dbHandler('host=eth0.ga_database01;uid=dbroot;pwd=w1l*0b1_#hunt3r$');
			$db->setDB('SocialApplication');
			return $db;
		}
		public static function dbConnectToGANetwork(){
			$dbGANetwork = new dbHandler('eth0.ga_database01;uid=dbroot;pwd=w1l*0b1_#hunt3r$');
			$dbGANetwork->setDB('Travel_Logs');
			return $dbGANetwork;
		}		
		public static function getApiKey(){
			return '486268511ffe689eccf66f936bbd189c';
		//	return '5f354e06ee9eee0ca1256e364b77f8b3';
		}

		public static function getApiSecret(){
			return '2c21d8d11f710833971181ed645eba8e';
		//	return '87b8be26ccf8ea33f776e116ac6cfd3f';		
		}
		
		public static function getImagePath(){
			return 'http://images.goabroad.net/images/facebook/';
		}
		
		public static function getLogoPath(){
			return self::getImagePath().'mytravelbioicon.gif';
		}
		
		public static function getAnswerIconPath(){
			return self::getImagePath().'question.jpg';	
		}
			
		public static function getCallbackDirectory(){
			return 'http://www.goabroad.net/facebook/mytraveljournals/index.php';
		}
		
		public static function getFBRootDirectory(){
			return 'http://apps.facebook.com/mytraveljournals/';
		}
		
		public static function getAdminFbUserID(){
			return '1143327910';
		}
		
		public static function getCallbackHost(){
			return 'http://www.goabroad.net/journal.php';
		}
				 
		public static function getInfiniteSessionKey(){
			return '314603aaea73b1ab5e2ec214-1143327910';		
		}
	}
?>