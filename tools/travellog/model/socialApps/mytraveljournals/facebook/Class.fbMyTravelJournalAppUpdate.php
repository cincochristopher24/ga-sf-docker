<?php
require_once('travellog/helper/ganet_web_service/facebook/Class.fbMyTravelJournalsControllerHelper.php');
require_once('travellog/model/socialApps/mytraveljournals/facebook/Class.fbMyTravelJournalApp.php');
require_once('travellog/model/socialApps/mytraveljournals/facebook/Class.fbMyTravelJournal.php');
require_once('travellog/model/socialApps/mytraveljournals/Class.WebServiceUser.php');

require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class fbMyTravelJournalAppUpdate
{
	protected $db = null;
 	protected $dbGANetwork;
 	public $fbUser = null;
 	public $travelerID;
 	public $numberOfJournalDisplayed;
 	
 	public $Traveller;
 	public $TravelID;
 	public $TravelLoglID;
 	
	public function __construct($travelerID)
	{
		$this->db = new dbHandler('db=SocialApplication');
		//$this->dbGANetwork = fbInitialization::dbConnectToGANetwork();

		$this->travelerID = $travelerID;
		$qry = "SELECT * from tbUser where travelerID=".$this->travelerID." limit 1";
		$rs = new iRecordset($this->db->execQuery($qry));
		if(0 < $rs->retrieveRecordCount()){
			foreach($rs as $row)
			{
			$this->fbUser = $row['facebookID'];
			}
		$this->performUpdateScript();
		}
	//	return $this->travellerID;
	}

	function performUpdateScript() {
		if(0 < $this->fbUser){
			$user_id = $this->fbUser;
			
			$fbdomain = 'facebook.com';
			$user = new WebServiceUser($user_id, $fbdomain);
			$domain = $user->getDomain();

			$myTravelJournalApp = new fbMyTravelJournalApp();
			$journals = AbstractMyTravelJournal::getUserSelectedJournals($user->getID(),$user->getWsID(),$user->getDomain());
			$ownerDotNetUsername = $user->getDotNetUsername();

			require_once('travellog/views/mytraveljournals/facebook_/tpl.fbProfile.php');
			
		}
	}
	
	
}
?>