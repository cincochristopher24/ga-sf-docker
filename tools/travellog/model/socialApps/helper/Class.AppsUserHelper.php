<?php
//echo 'require_once = WebServiceIni<br>';	
	/*
	 * Created on Jul 8, 2008
	 */
	 
	class AppsUserHelper{
		
		// containers
		const 
			FACEBOOK = 'facebook.com',
			MYSPACE = 'myspace.com',
			ORKUT = 'orkut.com',
			NING = 'ning.com',
			HI5	= 'hi5.com';
		
		// apps
		const 
			MY_TRAVEL_BIO		=	'myTravelBio',
			MY_TRAVEL_MAP		=	'myTravelMap',
			MY_TRAVEL_PLANS		=	'myTravelPlans',
			MY_TRAVEL_JOURNALS 	=	'myTravelJournals';

		// error messages
		const
			UNDEFINED_USER			=	'Undefined user',
			NON_GA_MEMBER			=	'Not GoAbroad Network Member.',
			SUSPENDED_USER			= 	'Your GoAbroad Network account is suspended.',
			USING_USERNAME			= 	'Please use your email to sychronize your account to Goabroad Network.',
			ACCOUNT_ALREADY_SYNCED	= 	'Account already synchronized with another user.',
			ACCOUNT_NOT_SYNCED		= 	'You have to synchronize your accounts first before you can change you settings.';
			
		static function isValidContainer($container){
			$validContainers = array(
				self::FACEBOOK, 
				self::MYSPACE, 
				self::ORKUT, 
				self::NING, 
				self::HI5
			);
			return in_array(strtolower($container), $validContainers);
		}
		
		static function getContainerFieldNameByContainer($container){
			return self::isValidContainer($container)
				? strtolower(array_shift(explode('.',strtolower($container)))).'ID'
				: null;
		}
		
	} 
?>
