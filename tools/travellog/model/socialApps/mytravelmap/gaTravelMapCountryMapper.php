<?php
/**
	Filename:		gaTravelMapCountryMapper.php
	@author:		Jonas Tandinco
	Created:		Jan/10/2007
	Purpose:		data mapper for TravelMapCountry object
	
	EDIT HISTORY:
*/

class gaTravelMapCountryMapper {
 	
	/**
	 * forms an associative array of all countries with countryID as the key
	 * and countryName as the value
	 * 
	 * @return array
	 */
	public static function getCountryListData() {
		require_once 'travellog/model/Class.Country.php';
		
		$countryList = Country::getCountryList();
		
		$result = array();
		foreach ($countryList as $country) {
			$result[$country->getCountryID()] = $country->getName();
		}
		
		return $result;
	}
	
}
