<?php
/**
	filename:	gaOpenSocialTravelerMapper.php
	@author:	Jonas Tandinco
	created:	Sep/25/2008
	purpose:	data mapper for a traveler object
	
	EDIT HISTORY:
*/

require_once 'Class.Connection.php';
require_once 'Class.Recordset.php';

class gaOpenSocialTravelerMapper {
	
	public static function getTravelerFromCredentials($email, $password) {
		require_once 'gaOpenSocialTraveler.php';
		
		$rs = new Recordset(new Connection('Travel_Logs'));
		
		$query = "SELECT travelerID, isSuspended " .
				 "FROM tblTraveler " .
//				 "LEFT JOIN tblGrouptoAdvisor " .
//				 "ON tblTraveler.travelerID = tblGrouptoAdvisor.travelerID " .
				 "WHERE email = '$email' " .
				 "AND password = '$password' " .
				 "LIMIT 1";
		$rs->Execute($query);

		if ($rs->Recordcount() > 0) {
			$row = mysql_fetch_row($rs->Resultset());

			// check if advisor
			$query = "SELECT travelerID " .
					 "FROM tblGrouptoAdvisor " .
					 "WHERE travelerID = {$row[0]} " .
					 "LIMIT 1";
			$rs->Execute($query);
			
			if ($rs->Recordcount() > 0) {
				// user is an advisor
				return FALSE;
			}

			// create traveler object
			$traveler = new gaOpenSocialTraveler();
			
			$traveler->setID          ( $row[0]      );
			$traveler->setIsSuspended ( $row[1] == 1 );
			
			return $traveler;
		}

		return FALSE;
	}
	
	/**
	 * retrieves the travelerID given the container ID
	 * 
	 * @param container - the opensocial site (e.g. orkut.com)
	 * @param containerID - the opensocial account
	 * 
	 * @return travelerID
	 * @return exception if not found
	 */
	public static function getTravelerIDWithContainerID($container, $containerID) {
		$rs   = new Recordset(new Connection('SocialApplication'));
		
		$containerField = self::getContainerField($container);
		
		$query = "SELECT travelerID " .
				 "FROM tbUser " .
				 "WHERE $containerField = $containerID " .
				 "LIMIT 1";
		
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			$row = mysql_fetch_row($rs->Resultset());
			
			return $row[0];
		} else {
			throw new Exception('Record not found.');
		}
	}
	
	public static function getUserIDWithContainerID($container, $containerID) {
		$conn = new Connection('SocialApplication');
		$rs   = new Recordset($conn);
		
		$containerField = self::getContainerField($container);
		
		$query = "SELECT userID " .
				 "FROM tbUser " .
				 "WHERE $containerField = $containerID " .
				 "LIMIT 1";

		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			$row = mysql_fetch_row($rs->Resultset());
	
			return $row[0];
		} else {
			// new user
			return self::createUserWithContainerID($container, $containerID);
		}
	}

	/**
	 * retrieves the userID of a traveler
	 * 
	 * @param  travelerID
	 * @return userID
	 */
	public static function getUserIDWithTravelerID($travelerID) {
    	$rs = new Recordset(new Connection('SocialApplication'));
		
		$query = "SELECT userID " .
				 "FROM tbUser " .
				 "WHERE travelerID = $travelerID " .
				 "LIMIT 1";
		$rs->Execute($query);

		if ($rs->Recordcount() > 0) {
			$row = mysql_fetch_row($rs->Resultset());
			
			return $row[0];
		} else {
			return self::createNewUser($travelerID);
		}
	}
	
	/**
	 * checks whether the account from an opensocial container is
	 * already in sync with a goabroad.net traveler account
	 * 
	 * @param container (e.g. orkut.com, hi5.com, myspace.com)
	 * @param containerID - the ID provided by the opensocial site
	 * 
	 * @return TRUE if synchronized
	 * @return FALSE otherwise 
	 */
	public static function isSynchronized($container, $containerID) {
		if(!$containerID || !is_numeric($containerID)) return false;
		$rs   = new Recordset(new Connection('SocialApplication'));

		$containerField = self::getContainerField($container);

		$query = "SELECT travelerID " .
				 "FROM tbUser " .
				 "WHERE $containerField = $containerID " .
				 "AND travelerID <> 0 ".	// NOTE: unsynchronized accounts have a temporary record in tblUser with travelerID set to 0
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			return TRUE;
		}

		return FALSE;
	}
	
	/**
	 * retrieves the fieldname of the containerID (e.g. orkutID for orkut.com)
	 * 
	 * @param container - the opensocial site
	 * 
	 * @return the fieldname
	 */
	public static function getContainerField($container) {
		switch($container) {
			case 'myspace.com':
				return 'myspaceID';
			
			case 'orkut.com':
				return 'orkutID';
			
			case 'ning.com':
				return 'ningID';
			
			case 'hi5.com':
				return 'hi5ID';
			
			case 'linkedin.com':
				return 'linkedinID';
			
			default:
				throw new Exception('Invalid container parameter');
		}		
	}
	
	/**
	 * creates a new record for an opensocial account. This record is temporary
	 * and will be deleted once it is synchronized to a .net account
	 * 
	 * @param container - opensocial site (e.g. orkut.com)]
	 * @param containerID - provided by the opensocial site to identify the account
	 * 
	 * @return userID of the newly created record
	 */
	public static function createUserWithContainerID($container, $containerID) {
		$rs = new Recordset(new Connection('SocialApplication'));
		
		$containerField = self::getContainerField($container);
		
		$query = "INSERT INTO tbUser($containerField) " .
				 "VALUES ($containerID)";

		$rs->Execute($query);
		
		return mysql_insert_id();
	}
	
	/**
	 * creates a new record for a new goabroad.net account
	 * 
	 * @param travelerID
	 * 
	 * @return userID
	 */
	public static function createNewUser($travelerID) {
		$rs = new Recordset(new Connection('SocialApplication'));
		
		$query = "INSERT INTO tbUser (travelerID) " .
				 "VALUES ($travelerID)";
		$rs->Execute($query);
		
		return mysql_insert_id();
	}
	
}