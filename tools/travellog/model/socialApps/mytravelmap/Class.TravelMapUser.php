<?php

require_once 'Class.dbHandler.php';
require_once 'Class.iRecordset.php';

/**
 *  Class for MyTravelMap user
 */
class TravelMapUser {
	private $mUserID     = null;
	private $mFacebookID = null;
	private $mTravelerID = null;
	private $mPinSetting = null;
	private $mDisplayMapTip = null;
	
	
	public function setUserID($v) {
		if($v) {
			$this->mUserID = $v;
		}
		return $this;
	}
	
	public function getUserID() {
		return $this->mUserID;
	}
	
	public function setFacebookID($v) {
		if($v) {
			$this->mFacebookID = $v;
		}
		return $this;
	}
	
	public function getFacebookID() {
		return $this->mFacebookID;
	}
	
	public function setTravelerID($v) {
		if($v) {
			$this->mTravelerID = $v;
		}
		return $this;
	}
	
	public function getTravelerID() {
		return $this->mTravelerID;
	}
	
	public function setPinSetting($v) {
		if($v) {
			$this->mPinSetting = $v;
		}
		return $this;
	}
	
	public function getPinSetting() {
		return $this->mPinSetting;
	}

	public function setDisplayMapTip($v) {
		if($v) {
			$this->mDisplayMapTip = $v;
		}
		return $this;
	}
	
	public function getDisplayMapTip() {
		return $this->mDisplayMapTip;
	}

	
	public function initialize($row) {
		$this->mUserID = array_key_exists('userID', $row) ? $row['userID'] : null;
		$this->mFacebookID = array_key_exists('facebookID', $row) ? $row['facebookID'] : null;
		$this->mTravelerID = array_key_exists('travelerID', $row) ? $row['travelerID'] : null;
		$this->mPinSetting = array_key_exists('displayPinSetting', $row) ? $row['displayPinSetting'] : null;
		$this->mDisplayMapTip = array_key_exists('displayMapTip', $row) ? $row['displayMapTip'] : null;
	}
	
	public function editPinSetting($pinSetting) {
		$conn = self::getConnection('SocialApplication');
		$sql = "UPDATE tbUser 
	            SET displayPinSetting = $pinSetting 
	            WHERE userID = $this->mUserID";

		return $conn->execQuery($sql);
	}
}