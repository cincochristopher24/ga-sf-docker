<?php
/*
	Filename:		Class.TravelMap.php\
	Author:			Jonas Tandinco
	Created:		Dec/19/2007
	Purpose:		represents a traveler's travel map
	
	EDIT HISTORY:
	
	Jan/16/2008 by Jonas Tandinco
		1.	removed member variable countryIDs and replaced it with countriesTraveledIDs
		
	Jan/17/2008 by Jonas Tandinco
		1.	added countryNamesList member variable and its accessors
		2.	changed algorithm for getting the list of countriesTraveledIDs
		3.	removed travelerPhoto member variable and its accessors
		
	Feb/26/2008 by Jonas Tandinco
		1.	added startlocation attribute
*/

class TravelMap {

	// MEMBER VARIABLES
	protected
		$countriesTraveled        = array(),
		$gMapKey                  = NULL;

	// GETTERS
	public function getCountriesTraveled() {
		return $this->countriesTraveled;
	}
	
	public function getGMapKey() {
		return $this->gMapKey;
	}
	
	public function getStartLocationCoordinates() {
		return array('lat' => 0, 'long' => 0);
	}
	
	// SETTERS
	public function setCountriesTraveled($v) {
		$this->countriesTraveled = $v;
	}
	
	public function setGMapKey($v) {
		$this->gMapKey = $v;
	}
	
	public function getCountryNamesList() {
		if (count($this->countriesTraveled) > 0) {
			$countryNamesList = array();
			foreach($this->countriesTraveled as $country) {
				$countryNamesList[] = $country->getCountry()->getName();
			}
			
			asort($countryNamesList);
			
			return join(', ', $countryNamesList);
		}
		
		return '';
	}
	
	public function getCountriesTraveledIDs() {
		$countriesTraveledIDs = array();
		if (count($this->countriesTraveled) > 0) {
			foreach ($this->countriesTraveled as $country) {
				$countriesTraveledIDs[] = $country->getCountry()->getCountryID();
			}
		}
		
		return $countriesTraveledIDs;
	}
	
	public function getDisabledCountryIDs() {
		$disabledCountryIDs = array();
		if (count($this->countriesTraveled) > 0) {
			foreach ($this->countriesTraveled as $country) {
				if (!$country->isEditable()) {
					$disabledCountryIDs[] = $country->getCountryID();
				}
			}
		}
		
		return $disabledCountryIDs;
	}
	
	public function getCountryData() {
		$result = array();
		foreach ($this->countriesTraveled as $country) {
			$countryData = array();
			
			$countryData['countryName'] = $country->getCountryName();
			
			$countryData['lat']         = $country->getLatitude();
			$countryData['long']        = $country->getLongitude();
			
			if ($country->getNumPhotos() > 0) {
				$countryData['randomPhoto'] = $country->getRandomPhoto();
			} else {
				$countryData['randomPhoto'] = '';
			}
			
			$countryData['numJournals'] = $country->getNumJournalEntries();
			$countryData['numPhotos']   = $country->getNumPhotos();
			$countryData['numTips']     = $country->getNumTravelTips();
			
			$result[$country->getCountryID()] = $countryData;
		}
		
		return $result;
	}
	
}