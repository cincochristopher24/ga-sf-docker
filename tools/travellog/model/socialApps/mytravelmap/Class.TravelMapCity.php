<?php

require_once 'travellog/model/Class.Photo.php';

class TravelMapCity {
	
	private $latitude          = 0;
	private $longitude         = 0;
	private $countryID         = 0;
	private $countryName       = '';
	private $locationID        = NULL;
	protected $isEditable      = NULL;
	
	// GETTERS
	public function isEditable() {
		return $this->isEditable;
	}
				
	public function getCountryID() {
		return $this->countryID;
	}
	
	public function getCountryName() {
		return $this->countryName;
	}
			
	public function getLatitude() {
		return $this->latitude;
	}
	
	public function getLongitude() {
		return $this->longitude;
	}

	public function getLocationID() {
		return $this->locationID;
	}
	
	// SETTERS
	public function setIsEditable($v) {
		$this->isEditable = $v;
	}	
		
	public function setCountryID($countryID) {
		$this->countryID = $countryID;
	}
	
	public function setCountryName($countryName) {
		$this->countryName = $countryName;
	}
			
	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}
	
	public function setLongitude($longitude) {
		$this->longitude = $longitude;
	}
 
	public function setLocationID($locationID) {
		$this->locationID = $locationID;
	}
   	
}