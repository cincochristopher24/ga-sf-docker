<?php
/**
 * This is as basic as it can get. Only has a single method of note:
 * lngLatToXYCoords() that transforms longitude and latitudes to their
 * X and Y coordinates using the Mercator projection. Zoom support and
 * panning planned.
 *
 */
class Map {

  //defaults
  private $mapSize = array('x'=>256, 'y'=>256);
  private $markerSize = array('x'=>12, 'y'=>20);

  public function getMapSize() {
    return $this->mapSize;
  }
  public function setMapSize($mapSize) {
    $this->mapSize = $mapSize;
  }

  public function getMarkerSize() {
    return $this->markerSize;
  }
  public function setMarkerSize($markerSize) {
    $this->markerSize = $markerSize;
  }

  function __construct($mapSize = null, $markerSize = null) {
    if ($mapSize) $this->mapSize = $mapSize;
    if ($markerSize) $this->markerSize = $markerSize;
  }

  //lng-x lat-y
  public function lngLatToXYCoords($lng, $lat, $xxx = 'profile') {
    //convert to radians
    $x = (180.0 + $lng) / 360.0;
    $y = -$lat * M_PI / 180;

    //derives the x,y coordinates of a point on Mercator map from its lng, lat
    $y = 0.5 * log((1 + sin($y)) / (1 - sin($y)));

    //scale factor from radians to normalized
    $y *= 1.0 / (2 * M_PI);
    $y += 0.5;

    //$y = ($y * pow(2, 17 - $zoom));
    //$x = ($x * pow(2, 17 - $zoom));
    //$y = floor($y);
    //$x = floor($x);

	$x = floor($this->mapSize['x']*$x) - ($this->markerSize['x']/2);
	$y = floor($this->mapSize['y']*$y) - $this->markerSize['y']; 

	if ($xxx == 'profile') {

	    // FIXME: The following fix only applies to MyTravelMap Facebook app. For
	    // this class to be reusable remove the ff adjustments. This is to offset
	    // the inaccuracies possibly arising from custom made maps with skewed
	    // aspect ratios with respect to some pre-ordained projection, in this case,
	    // Mercator.
	    $x -= 10;
	    $y -= 42;//possible cause - cropped image?
	    if ($y > 206) $y = 206;//206 = 226(use image's actual size instead of $mapsize) less 20 (height of marker)

    } else if ($xxx == 'walltab') {
	    $x -= 5; 
	    $y -= 21;//possible cause - cropped image?
	    if ($y > 92) $y = 92;//183 = 226(use image's actual size instead of $mapsize) less 20 (height of marker)	
	}

    return array('x' => $x, 'y' => $y);
  }
}
?>