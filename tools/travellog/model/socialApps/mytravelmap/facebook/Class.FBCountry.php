<?php

class FBCountry {
  private $locationID = 0;
  private $countryID = 0;
  private $name = '';
  private $latitude, $longitude;

  function getLocationID() {return $this->locationID;}
  function setLocationID($locationID) {$this->locationID = $locationID;}

  function getCountryID() {return $this->countryID;}
  function setCountryID($countryID) {$this->countryID = $countryID;}

  function getName() {return $this->name;}
  function setName($name) {$this->name = $name;}

  function getLatitude() {
    return $this->latitude;
  }
  function setLatitude($latitude) {
    $this->latitude = $latitude;
  }

  function getLongitude() {
    return $this->longitude;
  }
  function setLongitude($longitude) {
    $this->longitude = $longitude;
  }

}

?>
