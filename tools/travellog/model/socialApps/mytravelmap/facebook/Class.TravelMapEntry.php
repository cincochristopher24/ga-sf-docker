<?php
class TravelMapEntry {
  private $travelMapID,
          $userID,
          $country,
          //used when synced with GaNET
          $tipsCount,
          $journalEntriesCount,
          $photosCount,
          $photos,
		  $travelID = 0,
		  $travelLogID = 0,
          $photo='',//filename
          $_location,
		  $isEditable=true;

  function getTravelID(){
	return $this->travelID;
  }

  function setTravelID($travelID){
	$this->travelID = $travelID;	
  }

  function getTravelLogID(){
	return $this->travelLogID;
  }

  function setTravelLogID($travelLogID){
	$this->travelLogID = $travelLogID;
  }

  function getTravelMapID() {
    return $this->travelMapID;
  }
  function setTravelMapID($travelMapID) {
    $this->travelMapID = $travelMapID;
  }

  function getUserID() {
    return $this->userID;
  }
  function setUserID($userID) {
    $this->userID = $userID;
  }

  function getCountry() {
    return $this->country;
  }
  function setCountry($country) {
    $this->country = $country;
  }
  
  function _getLocation() {
    return $this->_location;	
  }
  function _setLocation($location){
    $this->_location = $location;
  }

  function getTipsCount() {
    return $this->tipsCount;
  }
  function setTipsCount($tipsCount) {
    $this->tipsCount = $tipsCount;
  }

  function getJournalEntriesCount() {
    return $this->journalEntriesCount;
  }
  function setJournalEntriesCount($journalEntriesCount) {
    $this->journalEntriesCount = $journalEntriesCount;
  }

  function getPhotosCount() {
    return $this->photosCount;
  }
  function setPhotosCount($photosCount) {
    $this->photosCount = $photosCount;
  }

  function getIsEditable() {
    return $this->isEditable;
  }
  function setIsEditable($isEditable = true) {
    $this->isEditable = $isEditable;
  }


  function getPhoto() {
    //var_dump($this->selectPhoto());
    //return 'http://images.goabroad.net/users/photos/2007/January/Hazel/travellog/thumbnail3473b2394504961195058068.jpg';
    return $this->selectPhoto();
  }
  function setPhotos($photos) {
    $this->photos = $photos;
  }

  public function selectPhoto() {
    if (empty($this->photosCount)) return '';

    $index = mt_rand(0, $this->photosCount - 1);
    $photoID = $this->photos[$index]['photoID'];
    //$photoID = $this->photos[0]['photoID'];
    $journalEntryID = $this->photos[$index]['jID'];
    //$journalEntryID = $this->photos[0]['jID'];

//    $photoID = 14797;
//    $journalEntryID = 2121;

    require_once 'travellog/model/Class.Photo.php';
    require_once 'travellog/model/Class.TravelLog.php';
    $photo = new Photo(new TravelLog($journalEntryID), $photoID);

    return $photo->getPhotoLink('thumbnail');
  }
}
?>