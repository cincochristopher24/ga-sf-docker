<?php
//TODO: Reimplement; read up on whether ensuring only a single link is needed
// to facilitate

class SingletonConnection {
  private static $instance = NULL;

  // Prevent instantiation through constructor
  private function __construct() {
    // TODO: set database name here or connection details
  }

  // Override __clone
  private function __clone() {}

  public static function getInstance() {

	    // LOCAL
//	    $host = '192.168.1.120';
//	    $username = 'root';
//	    $password = '';
//	    $dbname = 'SocialApplication';

    // DEV
//    $host   = '192.168.3.9';
//    $username   = 'facebook';
//    $password   = 'n@w0_nG';
//    $dbname = 'SocialApplication';

    // PRODUCTION
//    $host = 'eth0.ga_database01';
//    $username = 'goabroad';
//    $password = 'g4pt4lip4p4';
//    $dbname = 'SocialApplication';

    if (!self::$instance) {
      self::$instance = new mysqli($host, $username, $password, $dbname);
      if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
      }
    }
    return self::$instance;
  }
}
?>