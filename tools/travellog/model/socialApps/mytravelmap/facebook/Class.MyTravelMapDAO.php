<?php
require_once 'Class.dbHandler.php';
require_once 'Class.iRecordset.php';

class MyTravelMapDAO {

  	public static function getCountryList() {
    	$countries = array();
		$conn = self::getConnection('GoAbroad_Main');
    	$sql = 'SELECT locID, countryID, country FROM tbcountry 
				WHERE approved=1 AND countryID NOT IN (14,219,240,104) ORDER BY country';

		$recordsets = new iRecordset($conn->execQuery($sql));
		
		foreach ($recordsets as $rs) {
    		$country = new FBCountry;
    		$country->setLocationID($rs['locID']);
    		$country->setCountryID($rs['countryID']);
    		$country->setName($rs['country']);
    		$countries[] = $country;
		}
    	return $countries;
  	}

	// Added by Adelbert
	// NOTE: I think we should add GROUP BY c.countryID inside the query to avoid duplicate records if necessary 
  	public static function getPastTravelsList($userID) {
		$countries = array();
		$conn = self::getConnection();
		$sql = "SELECT c.countryID, c.country
	          	FROM SocialApplication.tbTravelWish AS a
	          	LEFT JOIN SocialApplication.tbTravelWishToCountry AS b ON a.travelWishID=b.travelWishID
	          	LEFT JOIN GoAbroad_Main.tbcountry AS c ON b.countryID=c.countryID
	          	WHERE a.userID = $userID
	            	AND a.dateFrom != '0000-00-00'
	            	AND a.dateFrom < CURRENT_DATE()
	            	AND a.dateTo < CURRENT_DATE()";

		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
			$country = new FBCountry;
			$country->setCountryID($rs['countryID']);
			$country->setName($rs['country']);
			$countries[] = $country;
		}
		return $countries;  
	}

	function getJournalsForSyncedTravelMapEntries($travelerID = null, $locationID = 0){
    	$journals = array();
		if(!$travelerID) return $journals;

		$conn = self::getConnection();
    	$sql = "SELECT a.travelLogID, d.locID, d.lat, d.long, e.countryID, e.country
        		FROM Travel_Logs.tblTravelLog AS a
            	LEFT JOIN Travel_Logs.tblTravel AS b ON a.travelID = b.travelID
            	LEFT JOIN Travel_Logs.tblTrips AS c ON a.tripID = c.tripID
            	LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID=d.locID
            	RIGHT JOIN GoAbroad_Main.tbcountry AS e ON d.locID=e.locID
				WHERE a.deleted = 0 AND b.deleted = 0 AND b.travelerID=$travelerID ";
	    if($locationID)
	    	$sql .="AND d.locID=$locationID ";

	    $sql .="UNION

				SELECT a.travelLogID, f.locID, f.lat, f.long, e.countryID, e.country
				FROM Travel_Logs.tblTravelLog AS a
				LEFT JOIN Travel_Logs.tblTravel AS b ON a.travelID = b.travelID
				LEFT JOIN Travel_Logs.tblTrips AS c ON a.tripID = c.tripID
				RIGHT JOIN GoAbroad_Main.tbcity AS d ON c.locID = d.locID
				LEFT JOIN GoAbroad_Main.tbcountry AS e ON d.countryID = e.countryID
				LEFT JOIN Travel_Logs.tblCoordinate AS f ON d.locID = f.locID
				WHERE a.deleted = 0 AND b.deleted = 0 AND b.travelerID=$travelerID ";
	    if($locationID)
	    	$sql .="AND d.locID=$locationID ";

	    $sql .="ORDER BY countryID";

		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
	        if ($rs['travelLogID']) {
				$journals[] = array(
					'jID'		=> $rs['travelLogID'], 
					'locID'		=> $rs['locID'],
					'lat'		=> $rs['lat'],
					'lng'		=> $rs['long'], 
					'countryID'	=> $rs['countryID'], 
					'country'	=> $rs['country']);
			}
		}		
		return $journals;
	}


	function getTipsForSyncedTravelMapEntries($travelerID = null, $locationID = 0){
	    $tips = array();
		if(!$travelerID) return $tips;

		$conn = self::getConnection();
	    $sql = "SELECT a.travelTipID, a.travelLogID, e.countryID, e.country, e.locID
	            FROM Travel_Logs.tblTravelTips AS a
	            LEFT JOIN Travel_Logs.tblTravelLog AS b ON a.travelLogID=b.travelLogID
	            LEFT JOIN Travel_Logs.tblTravel AS c ON b.travelID=c.travelID
	            LEFT JOIN Travel_Logs.tblTrips AS d ON b.tripID=d.tripID
	            RIGHT JOIN GoAbroad_Main.tbcountry AS e ON d.locID=e.locID
	            WHERE b.deleted = 0 AND c.deleted = 0 AND c.travelerID=$travelerID ";
	    if($locationID)
	    	$sql .="AND d.locID=$locationID ";

	    $sql .="UNION

	            SELECT a.travelTipID, a.travelLogID, f.countryID, f.country, f.locID
	            FROM Travel_Logs.tblTravelTips AS a
	            LEFT JOIN Travel_Logs.tblTravelLog AS b ON a.travelLogID=b.travelLogID
	            LEFT JOIN Travel_Logs.tblTravel AS c ON b.travelID=c.travelID
	            LEFT JOIN Travel_Logs.tblTrips AS d ON b.tripID=d.tripID
	            RIGHT JOIN GoAbroad_Main.tbcity AS e ON d.locID=e.locID
	            LEFT JOIN GoAbroad_Main.tbcountry AS f ON e.countryID=f.countryID
	            LEFT JOIN Travel_Logs.tblCoordinate AS g ON f.locID= g.locID
	            WHERE b.deleted = 0 AND c.deleted = 0 AND c.travelerID=$travelerID ";
	    if($locationID)
	    	$sql .="AND d.locID=$locationID ";

	    $sql .="ORDER BY countryID";

		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
	        if ($rs['travelLogID']) {
				$tips[] = array(
					'travelTipID' => $rs['travelTipID'], 
					'jID'		  => $rs['travelLogID'],
					'countryID'	  => $rs['countryID'],
					'country'	  => $rs['country'],
					'locID'		  => $rs['locID']);
			}
		}		
		return $tips;
	}

	function getPhotosForSyncedTravelMapEntries($travelerID = null, $locationID = 0){
	    $photos = array();
		if(!$travelerID) return $photos;

		$conn = self::getConnection();
	    $sql = "SELECT a.photoID, a.travelLogID, e.countryID, e.country, e.locID
	            FROM Travel_Logs.tblTravelLogtoPhoto AS a
	            LEFT JOIN Travel_Logs.tblTravelLog AS b ON a.travelLogID=b.travelLogID
	            LEFT JOIN Travel_Logs.tblTravel AS c ON b.travelID=c.travelID
	            LEFT JOIN Travel_Logs.tblTrips AS d ON b.tripID=d.tripID
	            RIGHT JOIN GoAbroad_Main.tbcountry AS e ON d.locID=e.locID
	            WHERE b.deleted = 0 AND c.deleted = 0 AND c.travelerID=$travelerID ";
	    if($locationID)
	    	$sql .="AND d.locID=$locationID ";

	    $sql .="UNION

	            SELECT a.photoID, a.travelLogID, f.countryID, f.country, f.locID
	            FROM Travel_Logs.tblTravelLogtoPhoto AS a
	            LEFT JOIN Travel_Logs.tblTravelLog AS b ON a.travelLogID=b.travelLogID
	            LEFT JOIN Travel_Logs.tblTravel AS c ON b.travelID=c.travelID
	            LEFT JOIN Travel_Logs.tblTrips AS d ON b.tripID=d.tripID
	            RIGHT JOIN GoAbroad_Main.tbcity AS e ON d.locID=e.locID
	            LEFT JOIN GoAbroad_Main.tbcountry AS f ON e.countryID=f.countryID
	            LEFT JOIN Travel_Logs.tblCoordinate AS g ON f.locID= g.locID
	            WHERE b.deleted = 0 AND c.deleted = 0 AND c.travelerID=$travelerID ";
	    if($locationID)
	    	$sql .="AND d.locID=$locationID ";

	    $sql .="ORDER BY countryID";

		$recordsets = new iRecordset($conn->execQuery($sql));
		
		foreach ($recordsets as $rs) {
	        if ($rs['travelLogID']) {
				$photos[] = array(
					'photoID' 	=> $rs['photoID'],
					'jID'		=> $rs['travelLogID'],
					'countryID'	=> $rs['countryID'],
					'country'	=> $rs['country'],
					'locID'		=> $rs['locID']);
			}
		}		
		return $photos;
	}


  	public static function getTravelMapEntriesSynced($travelerID) {
	   /*
	    * Get all journals/photos/tips of a given travelerID in one go rather than
	    * retrieving them individually per journal or even per country as this could
	    * potentially jack up exponentionally the number of queries needed.
	    *
	    * UNION is necessary to separate those journals with locIDs that are found
	    * only in the tbcity table from those with locIDs found in tbcountry
	    * NOTE (Jan112008): there are entries in the tblTrips with locIDs that
	    * don't exist in tbcity AND tbcountry but do in tblCoordinate
	    */
		$tips = self::getTipsForSyncedTravelMapEntries($travelerID);
		$photos = self::getPhotosForSyncedTravelMapEntries($travelerID);
		$journals = self::getJournalsForSyncedTravelMapEntries($travelerID);

	    //TODO: move the following code to TravelMapEntry; reimplement
	    $travelMapEntries = array();
	    $entry;
	    $countryID = -1; // it is set to -1 coz it does not exist in table

	    // ASSUMPTIONS: $journals, $photos, $tips elements are sorted by countryID
		for ($i=0; $i<count($journals); $i++) {
			$journal = $journals[$i];

			if ($countryID != $journal['countryID']) {
				$entry = new TravelMapEntry();
				$countryID = $journal['countryID'];
				$countryIDs[] = $countryID;

				$country = new FBCountry();
				$country->setCountryID($journal['countryID']);
				$country->setName($journal['country']);
				$country->setLatitude($journal['lat']);
				$country->setLongitude($journal['lng']);

				$entry->setCountry($country);
				//we just need the count for now instead of the actual contents
				$entry->setJournalEntriesCount(self::getCountPerCountry($journals, $countryID));
				$entry->setTipsCount(self::getCountPerCountry($tips, $countryID));
				$entriesPerCountry = self::getEntriesPerCountry($photos, $countryID);
				$entry->setPhotosCount(count($entriesPerCountry));
				$entry->setPhotos($entriesPerCountry);

				// added by Adelbert
				$entry->setTravelLogID($journal['jID']);

				$travelMapEntries[] = $entry;
			}
		}
		return $travelMapEntries;
	}

	//TODO: refactor
	private function getEntriesPerCountry($entries, $countryID) {
		$subset = array();
		$cnt = count($entries);
		for ($i=0; $i<$cnt; $i++) {
			if ($countryID == $entries[$i]['countryID']) $subset[] = $entries[$i];
		}
		return $subset;
	}

  //$entries - a collection of associative arrays representing either a list of
  //journals, photos or tips. Said list should at least have an element named
  //countryID
  private function getCountPerCountry($entries, $countryID) {
    $total = 0;
    $cnt = count($entries);
    for ($i=0; $i<$cnt; $i++) {
      if ($countryID == $entries[$i]['countryID']) $total++;
    }
    return $total;
  }


  	public static function getTravelMapEntriesFromMyTravelPlans($userID = null) {
    	$entries = array();
		if(!$userID) return $entries;
		
		$conn = self::getConnection();
	    $sql = "SELECT c.countryID, c.country, d.lat, d.long
	            FROM SocialApplication.tbTravelWish AS a
	            LEFT JOIN SocialApplication.tbTravelWishToCountry AS b ON a.travelWishID=b.travelWishID
	            LEFT JOIN GoAbroad_Main.tbcountry AS c ON b.countryID=c.countryID
	            LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID=d.locID
	            WHERE a.userID=$userID AND b.startDate <> '0000-00-00' 
				AND b.startDate IS NOT NULL AND b.startDate <= CURRENT_DATE()";

				//    $sql = "SELECT c.countryID, c.country, d.lat, d.long
				//            FROM SocialApplication.tbTravelWish AS a
				//            LEFT JOIN SocialApplication.tbTravelWishToCountry AS b ON a.travelWishID=b.travelWishID
				//            LEFT JOIN GoAbroad_Main.tbcountry AS c ON b.countryID=c.countryID
				//            LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID=d.locID
				//            WHERE a.userID=? AND a.dateFrom <> '0000-00-00' AND a.dateFrom <= CURRENT_DATE()";

		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
	        $country = new FBCountry();
	        $country->setCountryID($rs['countryID']);
	        $country->setName($rs['country']);
	        $country->setLatitude($rs['lat']);
	        $country->setLongitude($rs['long']);

	        $entry = new TravelMapEntry();
	        $entry->setCountry($country);

	        $entries[] = $entry;
		}		

    	return $entries;
	}

  	public static function getTravelMapEntriesFromCountriesTraveledTo($travelerID=null) {
    	$entries = array();	
		if(!$travelerID) return $entries();
		
		$conn = self::getConnection();
    	$sql = "SELECT a.countryID, b.country, c.lat, c.long
            	FROM Travel_Logs.tblTravelertoCountriesTravelled AS a
            	LEFT JOIN GoAbroad_Main.tbcountry AS b ON a.countryID=b.countryID
            	LEFT JOIN Travel_Logs.tblCoordinate AS c ON b.locID=c.locID
            	WHERE a.travelerID=$travelerID";


		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
	        $country = new FBCountry();
	        $country->setCountryID($rs['countryID']);
	        $country->setName($rs['country']);
	        $country->setLatitude($rs['lat']);
	        $country->setLongitude($rs['long']);

	        $entry = new TravelMapEntry();
	        $entry->setCountry($country);

	        $entries[] = $entry;
		}		
    	return $entries;
  	}

  	public static function getTravelMapEntries($userID = null) {
    	$entries = array();
		if(!$userID) return $entries;
		
		$conn = self::getConnection();
	    $sql = "SELECT a.travelMapID, a.userID, a.countryID, b.country, c.lat, c.long
	            FROM SocialApplication.tbTravelMap AS a
	            LEFT JOIN GoAbroad_Main.tbcountry AS b ON a.countryID=b.countryID
	            LEFT JOIN Travel_Logs.tblCoordinate AS c ON b.locID=c.locID
	            WHERE a.userID=$userID";

		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
	        $country = new FBCountry();
	        $country->setCountryID($rs['countryID']);
	        $country->setName($rs['country']);
	        $country->setLatitude($rs['lat']);
	        $country->setLongitude($rs['long']);

	        $entry = new TravelMapEntry();
	        $entry->setCountry($country);

	        $entries[] = $entry;
		}		

    	return $entries;
  	}

  public static function getCoordinates($userID) {
    $sql = "SELECT c.locID, c.countryID, c.country, d.lat, d.long
                FROM SocialApplication.tbTravelMap AS a
                LEFT JOIN SocialApplication.tbTravelWishToCountry AS b ON a.travelWishID=b.travelWishID
                LEFT JOIN GoAbroad_Main.tbcountry AS c ON b.countryID=c.countryID
                LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID=d.locID
                WHERE a.userID = 119
                  AND a.dateFrom != '0000-00-00'
                  AND a.dateFrom < CURRENT_DATE()
                  AND a.dateTo < CURRENT_DATE()";
  }

  	public static function updateEntries($arrCountryID=array(), $userID=null) {
		if(!$userID) return false;
		$conn = self::getConnection('SocialApplication');

    	$sql = "DELETE FROM tbTravelMap WHERE userID=$userID";
		$conn->execQuery($sql);

		if ($count = count($arrCountryID)) {		
	        $sql = 'INSERT INTO tbTravelMap (userID, countryID, locationID) VALUES';
			foreach ($arrCountryID as $entry) {
				list($countryID,$locationID) = explode('_',$entry);
				$sql .= " ( $userID, $countryID, $locationID),";
			}		
			return $conn->execQuery(substr($sql,0,-1));
		}
	}

  	public static function getGanetUser($facebookID = null) {
		$user = array();
		if(!$facebookID) return $user;
		
		$conn = self::getConnection();    
	    $sql = "SELECT a.email, a.username, a.travelerID FROM Travel_Logs.tblTraveler AS a
	            LEFT JOIN SocialApplication.tbUser AS b ON a.travelerID = b.travelerID
	            WHERE b.facebookID=$facebookID LIMIT 1";

		$rs = new iRecordset($conn->execQuery($sql));

		if($rs->retrieveRecordCount()) {
			$user['email'] = $rs->getEmail(0);
			$user['username'] = $rs->getUsername(0);
			$user['travelerID'] = $rs->getTravelerID(0);  		  
		}		

   		return $user;
  }  
  
	// DEPRECATED
	public static function updateEntriesSynced($arrCountryID=array(), $travelerID=null) {
		if(!$travelerID) return false;
		$conn = self::getConnection('Travel_Logs');

		$sql = "DELETE FROM tblTravelertoCountriesTravelled WHERE travelerID=$travelerID";
		$conn->execQuery($sql);

		if ($count = count($arrCountryID)) {
	        $sql = 'INSERT INTO tblTravelertoCountriesTravelled (travelerID, countryID) VALUES';
			for ($i=0; $i <$count ; $i++) {
				$comma = $i == ($count-1) ? '' : ',';
				$sql .= " ( $travelerID, $arrCountryID[$i] ) $comma";
			}		
			return $conn->execQuery($sql);			
		}
	}

  	static function getCountryNameByID($countryID = null){
		$coutryName = null;
		if(!$countryID) return $coutryName;
  		$conn = self::getConnection('GoAbroad_Main');
	    $sql = "SELECT country FROM tbcountry WHERE countryID=$countryID LIMIT 1";
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount()) {
			return $coutryName = $rs->getCountry(0);
		}
		return $coutryName;		
  }

	private static function getConnection($dbName = null){
		$conn = new dbHandler();
		if(!empty($dbName))		
			$conn->setDB($dbName);

		return $conn;
	}
	
	
	/**********************************************************************************************/
	/************************************     NEW METHODS      ************************************/
	/**********************************************************************************************/
	
  
    /**
     *  new method to get the countryList
     *  @return array FBCountryLocation Objects instead of FBCountry Objects
     */
	public static function _getCountryList($enabledKey = false) {
    	require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocationPeer.php');
  	
	  	return FBLocationPeer::getCountryLocationList($enabledKey);
  	}
	
	/**
	 *  new method to retrieve city locations of a country in travel map
	 *  @param $locationID locationID of country
	 *  @param $userID
	 *
	 *  @return array FBCityLocation Objects  
	 */
	public static function _getCityLocationsListInMapEntry($locationID, $userID) {
		require_once('travellog/model/Class.LocationType.php');
		
    	$cities = array();
    	
		if(!$locationID) return $cities;
		
		$conn = self::getConnection();
		

		$sql = "SELECT b.cityID, b.countryID, b.city, b.locID, d.lat, d.long 
				FROM SocialApplication.tbTravelMap as a
				INNER JOIN GoAbroad_Main.tbcity as b ON a.locationID = b.locID 
				INNER JOIN GoAbroad_Main.tbcountry as c ON b.countryID = c.countryID 
				INNER JOIN Travel_Logs.tblCoordinate as d ON a.locationID = d.locID 
				WHERE b.approved = 1 AND c.locID = $locationID
				
				UNION
				
				SELECT b.cityID, b.countryID, b.city, b.locID, d.lat, d.long 
				FROM SocialApplication.tbTravelMap as a  
				INNER JOIN Travel_Logs.tblNewCity as b ON a.locationID = b.locID 
				INNER JOIN GoAbroad_Main.tbcountry as c ON b.countryID = c.countryID 
				INNER JOIN Travel_Logs.tblCoordinate as d ON a.locationID = d.locID
				WHERE c.locID = $locationID
				";
		
		$recordsets = new iRecordset($conn->execQuery($sql));
		
		foreach ($recordsets as $rs) {
    		$city = new FBCityLocation;
    		$city->setID($rs['locID']);
    		$city->setCityID($rs['cityID']);
    		$city->setName($rs['city']);
    		$city->setCountryID($rs['countryID']);
    		$city->setLocationType(LocationType::$CITY);
    		$city->setLatitude($rs['lat']);
    		$city->setLongitude($rs['long']);
    		$cities[$rs['locID']] = $city;
		}
    	return $cities;
	}

	function _getJournalsForSyncedTravelMapEntries($travelerID = null, $locationID = 0){
    	$journals = array();
		if(!$travelerID) return $journals;

		$conn = self::getConnection();
    	$sql = "SELECT a.travelLogID, d.locID, d.lat, d.long, e.countryID, e.country
        		FROM Travel_Logs.tblTravelLog AS a
            	LEFT JOIN Travel_Logs.tblTravel AS b ON a.travelID = b.travelID
            	LEFT JOIN Travel_Logs.tblTrips AS c ON a.tripID = c.tripID
            	LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID=d.locID
            	RIGHT JOIN GoAbroad_Main.tbcountry AS e ON d.locID=e.locID
				WHERE a.deleted = 0 AND b.deleted = 0 AND b.travelerID=$travelerID ";
	    if($locationID)
	    	$sql .="AND d.locID=$locationID ";

	    $sql .="UNION

				SELECT a.travelLogID, f.locID, f.lat, f.long, e.countryID, e.country
				FROM Travel_Logs.tblTravelLog AS a
				LEFT JOIN Travel_Logs.tblTravel AS b ON a.travelID = b.travelID
				LEFT JOIN Travel_Logs.tblTrips AS c ON a.tripID = c.tripID
				RIGHT JOIN GoAbroad_Main.tbcity AS d ON c.locID = d.locID
				LEFT JOIN GoAbroad_Main.tbcountry AS e ON d.countryID = e.countryID
				LEFT JOIN Travel_Logs.tblCoordinate AS f ON d.locID = f.locID
				WHERE a.deleted = 0 AND b.deleted = 0 AND b.travelerID=$travelerID ";
	    if($locationID)
	    	$sql .="AND d.locID=$locationID ";

		$sql .="UNION

				SELECT a.travelLogID, f.locID, f.lat, f.long, e.countryID, e.country
				FROM Travel_Logs.tblTravelLog AS a
				LEFT JOIN Travel_Logs.tblTravel AS b ON a.travelID = b.travelID
				LEFT JOIN Travel_Logs.tblTrips AS c ON a.tripID = c.tripID
				LEFT JOIN Travel_Logs.tblNewCity AS d ON c.locID = d.locID
				RIGHT JOIN GoAbroad_Main.tbcountry AS e ON d.countryID = e.countryID
				LEFT JOIN Travel_Logs.tblCoordinate AS f ON d.locID = f.locID
				WHERE a.deleted = 0 AND b.deleted = 0 AND b.travelerID=$travelerID ";
		if($locationID)
		$sql .="AND d.locID=$locationID ";

	    $sql .="ORDER BY countryID";

		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
	        if ($rs['travelLogID']) {
				$journals[] = array(
					'jID'		=> $rs['travelLogID'], 
					'locID'		=> $rs['locID'],
					'lat'		=> $rs['lat'],
					'lng'		=> $rs['long'], 
					'countryID'	=> $rs['countryID'], 
					'country'	=> $rs['country']);					
			}
		}		
		return $journals;
	}
  	public static function _getTravelMapEntriesSynced($travelerID, $locationID = 0) {
  	    /*
    	* Get all journals/photos/tips of a given travelerID in one go rather than
    	* retrieving them individually per journal or even per country as this could
    	* potentially jack up exponentionally the number of queries needed.
    	*
    	* UNION is necessary to separate those journals with locIDs that are found
    	* only in the tbcity table from those with locIDs found in tbcountry
    	* NOTE (Jan112008): there are entries in the tblTrips with locIDs that
    	* don't exist in tbcity AND tbcountry but do in tblCoordinate
    	*/
    	
    	require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocation.php');
		require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCountryLocation.php');
		require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCityLocation.php');
		require_once('travellog/model/socialApps/mytravelmap/facebook/Class.TravelMapEntry.php');
		require_once('travellog/model/Class.LocationType.php');

    	$journals = self::_getJournalsForSyncedTravelMapEntries($travelerID, $locationID);
    	if($journals) {
    		$tips = self::getTipsForSyncedTravelMapEntries($travelerID, $locationID);
    		$photos = self::getPhotosForSyncedTravelMapEntries($travelerID, $locationID);
    	}

    	//TODO: move the following code to TravelMapEntry; reimplement
    	$entries = array();
    	$entry = null;
    	$countryID = -1; // it is set to -1 coz it does not exist in table
    	$locID = -1;
    	$cityArr = array();
    	$countryIDs = array();

    	// ASSUMPTIONS: $journals, $photos, $tips elements are sorted by countryID
    	if($journals) {
    		for ($i=0; $i<count($journals); $i++) {
    			$journal = $journals[$i];

    			if ($locID != $journal['locID']) {

    				$fb_location = new FBLocation();

    	    		$fb_location->setID($journal['locID']);

    	    		$location = $fb_location->getLocation();
    	    		$type = $fb_location->getLocationType();

    	    		if($type  == LocationType::$COUNTRY) {
    	    			$countryID = $location->getCountryID();

    	    			if(!in_array($countryID, $countryIDs)) {
    	    				$country_location = new FBCountryLocation();

    	    				$country_location->setID($journal['locID']);
    	    				$country_location->setLatitude($journal['lat']);
    	    				$country_location->setLongitude($journal['lng']);
    	    				$country_location->setCountryID($countryID);
    	    				$country_location->setName($location->getName());
    	    				$country_location->setLocationType($type);

    	    		    	// push FBCityLocation that doesn't have FBCountryLocation
    	    		    	if(!empty($cityArr)) {
    	    		    		foreach($cityArr as $i => $fb_city) {
    	    		    			if($countryID == $fb_city->getCountryID()) {
    	    		    				$country_location->addCityLocation($fb_city);
    	    		    				// remove FBCityLocation from array
    	    		    				unset($cityArr[$i]);
    	    		    			}
    	    		    		}
    	    		    	}
    	    		    	$entry = new TravelMapEntry();
    						$entry->setIsEditable(false);
    	    		    	$entry->setCountry($country_location);

    	    		    	$countryIDs[] = $countryID;	

    						//we just need the count for now instead of the actual contents
    						$entry->setJournalEntriesCount(self::getCountPerCountry($journals, $countryID));
    						$entry->setTipsCount(self::getCountPerCountry($tips, $countryID));
    						$entriesPerCountry = self::getEntriesPerCountry($photos, $countryID);
    						$entry->setPhotosCount(count($entriesPerCountry));
    						$entry->setPhotos($entriesPerCountry);

    						// added by Adelbert
    						$entry->setTravelLogID($journal['jID']);

    						//indexed by locationID
    						$entries[$journal['locID']] = $entry;	       
    	    		    } 
    	    		}

    	    		// regardless of type whether newcity, city just push them into citylist
    	    		elseif($type == LocationType::$CITY || $type == LocationType::$NEWCITY) {
    	    			$city_location = new FBCityLocation();

    	    			//if($type == LocationType::$STATE) {// USA
    	    			//	$countryID = 91;
    	    			//	$city_location->setCityID($location->getStateID());
    	    			//}

    	    		    $countryID = $location->getCountryID(); 
    	    		    $city_location->setCityID($location->getCityID());

    	    		    $city_location->setID($journal['locID']);
    	    			$city_location->setLatitude($journal['lat']);
    	    			$city_location->setLongitude($journal['lng']);
    	    		    $city_location->setCountryID($countryID);
    	    			$city_location->setName($location->getName());	
    	    		    $city_location->setLocationType($type);
						$city_location->setIsEditable(false);

						if(in_array($countryID, $countryIDs)) {
    	    				$entries[$location->getCountry()->getLocationID()]->getCountry()->addCityLocation($city_location);
    	    			}
    	    			else {
    					     $city_location->setLocation($location);
    	    				 // if locationID of city was retrieved first before locationID of country,
    	    				 // add FBCityLocation to array which will be checked in the proceeding iterations
    	    				 $cityArr[] = $city_location;
    	    			}
    	    		}

    	    	}
    		}
    		if(!empty($cityArr)) {
    			foreach($cityArr as $cityLoc) {
    				$countryID = $cityLoc->getCountryID();

    				if(!in_array($countryID, $countryIDs)) {
    					$country_location = new FBCountryLocation();

    	    			$country_location->setID($cityLoc->getLocation()->getCountry()->getLocationID());
    	    			$country_location->setLatitude($cityLoc->getLocation()->getCountry()->getCoordinates()->getY());
    	    			$country_location->setLongitude($cityLoc->getLocation()->getCountry()->getCoordinates()->getX());
    	    			$country_location->setCountryID($countryID);
    	    			$country_location->setName($cityLoc->getLocation()->getCountry()->getName());
    	    			$country_location->setLocationType(LocationType::$COUNTRY);

    	    			// push FBCityLocation that doesn't have FBCountryLocation
    	    	    	if(!empty($cityArr)) {
    	    	    		foreach($cityArr as $i => $fb_city) {
    	    	    			if($countryID == $fb_city->getCountryID()) {
    	    	    				$country_location->addCityLocation($fb_city);
    	    	    				// remove FBCityLocation from array
    	    	    				unset($cityArr[$i]);
    	    	    			}
    	    	    		}
    	    	    	}

    	    	    	$entry = new TravelMapEntry();
    	    	    	$entry->setIsEditable(false);


    					//we just need the count for now instead of the actual contents
    					$entry->setJournalEntriesCount(self::getCountPerCountry($journals, $countryID));
    					$entry->setTipsCount(self::getCountPerCountry($tips, $countryID));
    					$entriesPerCountry = self::getEntriesPerCountry($photos, $countryID);
    					$entry->setPhotosCount(count($entriesPerCountry));
    					$entry->setPhotos($entriesPerCountry);

    					// added by Adelbert
    					$entry->setTravelLogID($journal['jID']);

    	    	    	$entry->setCountry($country_location);
						//indexed by locationID    	    			
						$entries[$cityLoc->getLocation()->getCountry()->getLocationID()] = $entry;
    	    	    	$countryIDs[] = $countryID;	

						
						
    				}
    			} 
    		}
    	}
    	return $entries;
    }

  	public static function _getTravelMapEntriesFromMyTravelPlans($userID = null, $locationID = 0) {
    	$entries = array();
		if(!$userID) return $entries;
		
		$conn = self::getConnection();
	    $sql = "SELECT c.countryID, c.country, c.locID, d.lat, d.long
	            FROM SocialApplication.tbTravelWish AS a
	            LEFT JOIN SocialApplication.tbTravelWishToCountry AS b ON a.travelWishID=b.travelWishID
	            LEFT JOIN GoAbroad_Main.tbcountry AS c ON b.countryID=c.countryID
	            LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID=d.locID
	            WHERE a.userID=$userID AND b.startDate <> '0000-00-00' 
				AND b.startDate IS NOT NULL AND b.startDate <= CURRENT_DATE() ";
		if($locationID)
			$sql .="AND c.locID=$locationID";

				//    $sql = "SELECT c.countryID, c.country, d.lat, d.long
				//            FROM SocialApplication.tbTravelWish AS a
				//            LEFT JOIN SocialApplication.tbTravelWishToCountry AS b ON a.travelWishID=b.travelWishID
				//            LEFT JOIN GoAbroad_Main.tbcountry AS c ON b.countryID=c.countryID
				//            LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID=d.locID
				//            WHERE a.userID=? AND a.dateFrom <> '0000-00-00' AND a.dateFrom <= CURRENT_DATE()";

		$recordsets = new iRecordset($conn->execQuery($sql));

		foreach ($recordsets as $rs) {
	        $country = new FBCountryLocation();
	        $country->setID($rs['locID']);
	        $country->setCountryID($rs['countryID']);
	        $country->setName($rs['country']);
	        $country->setLatitude($rs['lat']);
	        $country->setLongitude($rs['long']);

	        $entry = new TravelMapEntry();
			$entry->setIsEditable(false);
	        $entry->setCountry($country);
	        
	        //indexed by locationID
	        $entries[$rs['locID']] = $entry;
		}		

    	return $entries;
	}
	
  	private static function _retrieveTravelMapEntries($qry = null) {
		require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocation.php');
		require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCountryLocation.php');
		require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCityLocation.php');
		require_once('travellog/model/socialApps/mytravelmap/facebook/Class.TravelMapEntry.php');
		require_once('travellog/model/Class.LocationType.php');
		
  		$entries = array();
  		if(!$qry) return $entries;
  		
  		$countryIDs = array();
  		$cityArr = array();
  		
  		$conn = self::getConnection();
  		
  		$recordsets = new iRecordset($conn->execQuery($qry));
  		$iscountry = false;
  		$iscity = false;
  		foreach ($recordsets as $rs) {
			$locationIds[] = $rs['locationID'];
		    $fb_location = new FBLocation();
	        
	        $fb_location->setID($rs['locationID']);
	        
	        $location = $fb_location->getLocation();
	        $type = $fb_location->getLocationType();
	        
	        if($type  == LocationType::$COUNTRY) {
/*	        	if(!$iscountry) {
		        	var_dump($rs);
					$iscountry = true;
				} */
	        	if(!in_array($location->getCountryID(), $countryIDs)) {
	        		$country_location = new FBCountryLocation();
	        	
	        		$country_location->initialize($rs);
	        		$country_location->setCountryID($location->getCountryID());
	        		$country_location->setName($location->getName());
	        		$country_location->setLocationType($type);

		        	// push FBCityLocation that doesn't have FBCountryLocation
		        	if(!empty($cityArr)) {
		        		foreach($cityArr as $i => $fb_city) {
		        			if($location->getCountryID() == $fb_city->getCountryID()) {
		        				$country_location->addCityLocation($fb_city);
		        				// remove FBCityLocation from array
		        				unset($cityArr[$i]);
		        			}
		        		}
		        	}
		        
		        	$entry = new TravelMapEntry();
		        	$entry->setCountry($country_location);
		        
		        	$entries[$location->getLocationID()] = $entry;
		        	$countryIDs[] = $location->getCountryID();		       
		        } 
	        }
	        
	        // regardless of type whether newcity, city just push them into citylist
	        elseif($type == LocationType::$CITY || $type == LocationType::$NEWCITY) {
/*	        	if(!$iscity) {
					echo '<hr>';
		        	var_dump($rs);
					echo '<hr>';
					$iscity = true;
				} */
	        	$city_location = new FBCityLocation();
	        		        	
	        	//if($type == LocationType::$STATE) {// USA
	        	//	$countryID = 91;
	        	//	$city_location->setCityID($location->getStateID());
	        	//}
	        	
		        $countryID = $location->getCountryID(); 
		        $city_location->setCityID($location->getCityID());
		        
		        
	        	$city_location->initialize($rs);
		        $city_location->setCountryID($countryID);
	        	$city_location->setName($location->getName());	
		        $city_location->setLocationType($type);
				

	        	if(in_array($countryID, $countryIDs)) {
	        		$entries[$location->getCountry()->getLocationID()]->getCountry()->addCityLocation($city_location);
	        	}
	        	else {
				     $city_location->setLocation($location);
	        		 // if locationID of city was retrieved first before locationID of country,
	        		 // add FBCityLocation to array which will be checked in the proceeding iterations
	        		 $cityArr[] = $city_location;
	        	}
	        }
		}
		// if there are still city locations having no country location
		// meaning the locationID of country wasn't saved, only the locationID for the city/newcity/state,
		// create the country locations here
		if(!empty($cityArr)) {
			foreach($cityArr as $cityLoc) {
				if(!in_array($cityLoc->getCountryID(), $countryIDs)) {
					$country_location = new FBCountryLocation();
	        		
	        		$country_location->setID($cityLoc->getLocation()->getCountry()->getLocationID());
	        		$country_location->setLatitude($cityLoc->getLocation()->getCountry()->getCoordinates()->getY());
	        		$country_location->setLongitude($cityLoc->getLocation()->getCountry()->getCoordinates()->getX());
	        		$country_location->setCountryID($cityLoc->getLocation()->getCountry()->getCountryID());
	        		$country_location->setName($cityLoc->getLocation()->getCountry()->getName());
	        		$country_location->setLocationType(LocationType::$COUNTRY);

	        		// push FBCityLocation that doesn't have FBCountryLocation
		        	if(!empty($cityArr)) {
		        		foreach($cityArr as $i => $fb_city) {
							if($country_location->getCountryID() == $fb_city->getCountryID()) {
		        				$country_location->addCityLocation($fb_city);
		        				// remove FBCityLocation from array
		        				unset($cityArr[$i]);
		        			}
		        		}
		        	}
		        	
					if (!in_array($country_location->getID(),array_keys($entries))){
		        		$entry = new TravelMapEntry();
			        	$entry->setCountry($country_location);
		        		$entries[$country_location->getID()] = $entry;
			        	$countryIDs[] = $location->getCountryID();	
					}
					
				}
			} 
		}
		
		//ksort($entries); // sort by locationID
		//var_dump($entries[68]);
    	return $entries;
  	}
  	
  	/**
	 *  new method to retieve MyTravelMapEntry of sync-ed MyTravelMap users
	 *  @param $travelerID travelerID in dotnet
	 *
	 *  @return array of Travel Map Entries 
	 */
  	public static function _getTravelMapEntriesFromLocationsTraveledTo($travelerID=null, $locationID = 0) {
    	if(!$travelerID) return array();
    	
    	$sql = "SELECT a.locationID, b.lat, b.long
            	FROM Travel_Logs.tblTravelertoCountriesTravelled AS a
            	LEFT JOIN Travel_Logs.tblCoordinate AS b ON a.locationID=b.locID
            	WHERE a.travelerID=$travelerID ";
        if($locationID)
        	$sql .="AND a.locationID=$locationID";
		
		return self::_retrieveTravelMapEntries($sql);
  	}
  	
  	/**
	 * get travel map entries for users that are not sync-ed in dotnet
	 * 
	 * @param $userID
	 * @return array of Travel Map Entries
	 *
	 * How to use:
	 *      $map_entries = MyTravelMapDAO::_getTravelMapEntries($userID);
	 *      foreach($map_entries as $map_entry) {
	 *          $fb_country         = $map_entry->getCountry();      //returns FBCountryLocation Object
	 *
	 *          $country_loc_id     = $fb_country->getID();          //returns location id
	 *          $country_id         = $fb_country->getCountryID();   //returns id of country
	 *          $country_name       = $fb_country->getName();        //returns name of the country
	 *          $country_latitude   = $fb_country->getLatitude();    //returns latitude coordinate of country
	 *          $country_longitude  = $fb_country->getLongitude();   //returns longitude coordinate of country
	 *          $country_flag       = $fb_country->getFlag();        //returns link location of flag of country
	 *
	 *          $fb_cities = $fb_country->getCityLocationList();     //returns an array of FBCityLocation Objects of this country
	 *          foreach($fb_cities as $fb_city) {
	 *              $city_loc_id    = $fb_city->getID();             //returns location id
	 *              $city_name      = $fb_city->getName();           //returns name of city/newcity/state
	 *              $city_latitude  = $fb_city->getLatitude();       //returns latitude coordinate of city/newcity/state
	 *              $city_longitude = $fb_city->getLongitude();      //returns longitude coordinate of city/newcity/state
	 *              $type           = $fb_city->getLocationType();   //returns the type e.i. 4-city; 6-newcity; 7-state
	 *          }
	 *      }
	 *
	 */
  	public static function _getTravelMapEntries($userID = null) {
  		if(!$userID) return array();
  		  		
  		$sql = "SELECT a.travelMapID, a.userID, a.countryID, a.locationID, b.lat, b.long
	            FROM SocialApplication.tbTravelMap AS a 
	            LEFT JOIN Travel_Logs.tblCoordinate AS b ON a.locationID=b.locID
	            WHERE a.userID=$userID";
	    
    	return self::_retrieveTravelMapEntries($sql);
  	}
	
	/**
	 *  new method to update locationID field instead of countryID field
	 *  @param $arrLocationID
	 */
	public static function _updateEntries($arrLocationID=array(), $userID=null) {
		if(!$userID) return false;
		$conn = self::getConnection('SocialApplication');

    	$sql = "DELETE FROM tbTravelMap WHERE userID=$userID";
		$conn->execQuery($sql);

		if ($count = count($arrLocationID)) {		
	        $sql = 'INSERT INTO tbTravelMap (userID, locationID) VALUES';
			for ($i=0; $i <$count ; $i++) {
				$comma = $i == ($count-1) ? '' : ',';
				$sql .= " ( $userID, $arrLocationID[$i] ) $comma";
			}		
			return $conn->execQuery($sql);
		}
	}	
	
	/**
	 *  new method to filter facebookIDs given an array of locationIDs
	 */
	private static function _filterUsersHaveBeenToLocations($friends, $locationIDs) {
		if(!$friends || !$locationIDs) return array();
		
		$conn = self::getConnection('SocialApplication');
		
		$sql = "SELECT a.locationID, b.facebookID 
		        FROM tbTravelMap as a
		        INNER JOIN tbUser as b ON a.userID = b.userID
		        WHERE a.locationID in (". implode(',',$locationIDs) .") 
		        AND b.facebookID in (". implode(',',$friends) .")";
		
		$resource = $conn->execQuery($sql);
		$locArr = array();
		while($record = mysql_fetch_assoc($resource)) {
			$v = new FBLocation;
			$v->setID($record['locationID']);
			$location = $v->getLocation();
			// add all users in cities of country to country
			if(!is_null($location) && get_class($location) != 'Country'){
				$countryLocID = $location->getCountry()->getLocationID();
				if(!array_key_exists($countryLocID, $locArr))
					$locArr[$countryLocID] = array($record['facebookID']);
				else
					array_push($locArr[$countryLocID], $record['facebookID']);
			}
			
			if(!array_key_exists($record['locationID'], $locArr))
				$locArr[$record['locationID']] = array($record['facebookID']);
			else
				array_push($locArr[$record['locationID']], $record['facebookID']);
		}
		return $locArr;
	}
	
	/**
	 *  new method to filter facebookIDs given an array of locationIDs of a sync-ed my-travel-map account
	 */
	private static function _filterSyncedUsersHaveBeenToLocations($friends, $locationIDs) {
		if(!$friends || !$locationIDs) return array();
		
		$conn = self::getConnection();
		
		$sql = "SELECT a.locationID, b.facebookID 
		        FROM Travel_Logs.tblTravelertoCountriesTravelled as a 
		        INNER JOIN SocialApplication.tbUser  as b ON a.travelerID=b.travelerID 
		        WHERE a.locationID in (". implode(',',$locationIDs) .") 
		        AND b.facebookID in (". implode(',',$friends) .")";
		
		$resource = $conn->execQuery($sql);
		$locArr = array();
		while($record = mysql_fetch_assoc($resource)) {
			$v = new FBLocation;
			$v->setID($record['locationID']);
			$location = $v->getLocation();
			// add all users in cities of country to country
			if(!is_null($location) && get_class($location) != 'Country'){
				$countryLocID = $location->getCountry()->getLocationID();
				if(!array_key_exists($countryLocID, $locArr))
					$locArr[$countryLocID] = array($record['facebookID']);
				else
					array_push($locArr[$countryLocID], $record['facebookID']);
			}
			
			if(!array_key_exists($record['locationID'], $locArr))
				$locArr[$record['locationID']] = array($record['facebookID']);
			else
				array_push($locArr[$record['locationID']], $record['facebookID']);
		}
		return $locArr;
	}
	
	/**
	 *  new method to filter facebookIDs given an array of locationIDs from travelers with journal entries
	 */
	private static function _filterUsersWithJournalEntryInCountryLocations($friends, $locationIDs) {
		if(!$friends || !$locationIDs) return array();
		
		$conn = self::getConnection();
		
		$sql = "SELECT a.facebookID, d.locID
		        FROM SocialApplication.tbUser as a 
		        INNER JOIN Travel_Logs.tblTravel AS b ON a.travelerID = b.travelerID 
		        INNER JOIN Travel_Logs.tblTravelLog AS c ON b.travelID = c.travelID 
		        INNER JOIN Travel_Logs.tblTrips AS d ON c.tripID = d.tripID
		        WHERE d.locID in  (". implode(',',$locationIDs) .") 
		        AND a.facebookID in (". implode(',',$friends) .") 
		        AND b.deleted = 0 AND c.deleted = 0";
		
		$resource = $conn->execQuery($sql);
		$locArr = array();
		while($record = mysql_fetch_assoc($resource)) {
			$v = new FBLocation;
			$v->setID($record['locID']);
			$location = $v->getLocation();
			// add all users in cities of country to country
			if(!is_null($location) && get_class($location) != 'Country'){
				$countryLocID = $location->getCountry()->getLocationID();
				if(!array_key_exists($countryLocID, $locArr))
					$locArr[$countryLocID] = array($record['facebookID']);
				else
					array_push($locArr[$countryLocID], $record['facebookID']);
			}
			if(!array_key_exists($record['locID'], $locArr))
				$locArr[$record['locID']] = array($record['facebookID']);
			else
				array_push($locArr[$record['locID']], $record['facebookID']);
		}
		
		return $locArr;
	}
	
	/**
	 *  new method to filter facebookIDs given an array of locationIDs from users in myTravelPlans
	 */
	private static function _filterUsersFromMyTravelPlansLocations($friends, $locationIDs) {
		if(!$friends || !$locationIDs) return array();
		
		$conn = self::getConnection();
		
		$sql = "SELECT a.facebookID, d.locID 
		        FROM SocialApplication.tbUser as a 
		        INNER JOIN SocialApplication.tbTravelWish AS b ON a.userID=b.userID 
	            INNER JOIN SocialApplication.tbTravelWishToCountry AS c ON b.travelWishID=c.travelWishID
	            INNER JOIN GoAbroad_Main.tbcountry AS d ON c.countryID=d.countryID
	            WHERE d.locID in (". implode(',',$locationIDs) .")  
	            AND a.facebookID in (". implode(',',$friends) .") 
	            AND c.startDate <> '0000-00-00' 
				AND c.startDate IS NOT NULL AND c.startDate <= CURRENT_DATE() ";
		
		$resource = $conn->execQuery($sql);
		$locArr = array();
		while($record = mysql_fetch_assoc($resource)) {
			$v = new FBLocation;
			$v->setID($record['locID']);
			$location = $v->getLocation();
			// add all users in cities of country to country
			if(!is_null($location) && get_class($location) != 'Country'){
				$countryLocID = $location->getCountry()->getLocationID();
				if(!array_key_exists($countryLocID, $locArr))
					$locArr[$countryLocID] = array($record['facebookID']);
				else
					array_push($locArr[$countryLocID], $record['facebookID']);
			}
			
			if(!array_key_exists($record['locID'], $locArr))
				$locArr[$record['locID']] = array($record['facebookID']);
			else
				array_push($locArr[$record['locID']], $record['facebookID']);
		}
		return $locArr;
	}
	
	/**
     *  method that filters my-travel-map users who have been to this location (can either be city, country, etc)
     *  @param $friends array of facebookIDs
     *  @param $locationIDs array of locationID
     *  @param $isSync boolean
     *  
     *  @return array of locationIDs => array of facebookIDs
     */
    public static function _filterFriendsInLocations($friends = array(), $locationIDs = array(), $isSync = null) {
     	 if (is_null($isSync) || !$friends || !$locationIDs) return array();
     	 
     	 if (!$isSync) {
     	 	//for unsync-ed myTravelMap account, retrieve from SocialApplication.tbTravelMap
     	 	
     	 	return self::_filterUsersHaveBeenToLocations($friends, $locationIDs);
     	 }
     	 else {
     	 	//for sync-ed myTravelMap account, retrieve from Travel_Logs.tblTravelertoCountriesTravelled, travelers with journals and MyTravelPlans
     	 	
     		$countriesTravelledTo = self::_filterSyncedUsersHaveBeenToLocations($friends, $locationIDs);
     		
     		//from travelers with plans in this country
     		$myTravelPlans = self::_filterUsersFromMyTravelPlansLocations($friends, $locationIDs);
     	 	
     		//from travelers with journal entries in this location
     		$journalEntryInCountry = self::_filterUsersWithJournalEntryInCountryLocations($friends, $locationIDs);
     		
     		$arr = $countriesTravelledTo;
     		foreach($myTravelPlans as $key => $locArr) {
     			if(array_key_exists($key, $countriesTravelledTo))
					// $arr[$key] = array_unique(array_merge($arr[$key],$locArr));
					$arr[$key] = array_merge_recursive($countriesTravelledTo[$key],$locArr);
				else
					$arr[$key] = $locArr;
     		}
     		
     		$newArr = $arr;
     		foreach($journalEntryInCountry as $key => $locArr) {
     			if(array_key_exists($key, $arr))
					// $newArr[$key] = array_unique(array_merge($newArr[$key],$locArr));
					$newArr[$key] = array_merge_recursive($arr[$key],$locArr);
				else
					$newArr[$key] = $locArr;
     		}
     		
     		return $newArr;
     	 }
    }
    
	/**
	 *  new method to retrieve latest journal entry (travellog) and count of journal entries in the given location
	 *  @param $locationID
	 *
	 *  @return array count and Travellog Object
	 */
	public static function _getJournalEntryInLocation($locationID = 0, $travelerID = 0) {
		if(!$locationID) return array();
		
		$conn = self::getConnection('Travel_Logs');
		
		$sql = "SELECT a.*, b.travelerID, b.travellinkID 
        		FROM Travel_Logs.tblTravelLog AS a
            	INNER JOIN Travel_Logs.tblTravel AS b ON a.travelID = b.travelID
            	INNER JOIN Travel_Logs.tblTrips AS c ON a.tripID = c.tripID
            	WHERE a.deleted = 0 AND b.deleted = 0 AND c.locID=$locationID 
            	AND c.arrival <= CURRENT_DATE() ";
        if($travelerID)
        	$sql .= "AND b.travelerID = $travelerID ";
        $sql .= "ORDER BY CASE WHEN a.lastedited = '0000-00-00 00:00:00' 
            	  THEN a.logdate 
            	  ELSE a.lastedited 
            	END DESC";
		
        $rs = new iRecordset($conn->execQuery($sql));
        $count = $rs->retrieveRecordCount();
		$record = $rs->current();
		$travellog = new Travellog(0, $record);
		return array('count' => $count, 'object' => $travellog);		
	}
	
	/**
	 *  new methos to retrieve count of journal entries tips in the given location
	 *  @param $locationID
	 *
	 *  @return int
	 */
	public static function _getCountJournalEntriesTipsInLocation($locationID, $travelerID = 0) {
		if(!$locationID) return array();
		
		$conn = self::getConnection('Travel_Logs');
		
		$sql = "SELECT count(a.travelTipID) 
	            FROM Travel_Logs.tblTravelTips AS a
	            INNER JOIN Travel_Logs.tblTravelLog AS b ON a.travelLogID=b.travelLogID
	            INNER JOIN Travel_Logs.tblTravel AS c ON b.travelID=c.travelID
	            INNER JOIN Travel_Logs.tblTrips AS d ON b.tripID=d.tripID
	            WHERE b.deleted = 0 AND c.deleted = 0 AND d.locID=$locationID ";
        if($travelerID)
        	$sql .= "AND c.travelerID = $travelerID ";

	    $resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			return $record['count(a.travelTipID)'];
		}
		return 0;
	}
	
	/**
	 *  new method to retrieve count of journal entries photos in the given location
	 *  @param $locationID
	 *
	 *  @return int
	 */
	public static function _getCountJournalEntriesPhotosInLocation($locationID, $travelerID = 0) {
		if(!$locationID) return array();
		
		$conn = self::getConnection('Travel_Logs');
		
		$sql = "SELECT count(a.photoID)
	            FROM Travel_Logs.tblTravelLogtoPhoto AS a
	            LEFT JOIN Travel_Logs.tblTravelLog AS b ON a.travelLogID=b.travelLogID
	            LEFT JOIN Travel_Logs.tblTravel AS c ON b.travelID=c.travelID
	            LEFT JOIN Travel_Logs.tblTrips AS d ON b.tripID=d.tripID
	            WHERE b.deleted = 0 AND c.deleted = 0 AND d.locID=$locationID ";
        if($travelerID)
        	$sql .= "AND c.travelerID = $travelerID ";
	            
	    $resource = $conn->execQuery($sql);
		if($record = mysql_fetch_assoc($resource)) {
			return $record['count(a.photoID)'];
		}
		return 0;
	}
	
	/**
	 *  new method to get photos
	 *
	 *  @return array
	 */
	public static function _getPhotosForSyncedTravelMapEntriesInLocation($locationID, $travelerID = 0){
	    $photos = array();
		if(!$travelerID) return $photos;

		$conn = self::getConnection();
	    $sql = "SELECT a.photoID, a.travelLogID 
	            FROM Travel_Logs.tblTravelLogtoPhoto AS a
	            LEFT JOIN Travel_Logs.tblTravelLog AS b ON a.travelLogID=b.travelLogID
	            LEFT JOIN Travel_Logs.tblTravel AS c ON b.travelID=c.travelID
	            LEFT JOIN Travel_Logs.tblTrips AS d ON b.tripID=d.tripID
	            WHERE b.deleted = 0 AND c.deleted = 0 AND c.travelerID=$travelerID 
	            AND d.locID = $locationID";
		$recordsets = new iRecordset($conn->execQuery($sql));
		
		foreach ($recordsets as $rs) {
	        if ($rs['travelLogID']) {
				$photos[] = array(
					'photoID' 	=> $rs['photoID'],
					'jID'		=> $rs['travelLogID'],
					'locID'		=> $locationID
				);
			}
		}		
		return $photos;
	}
	
	/**
	 *  new method to merge entries
	 *  if entries have the same country, cities are merged for that country
	 *  @param $entriesA array of TravelMapEntry Objects
	 *  @param $entriesB array of TravelMapEntry Objects
	 *
	 *  @return array of TravelMapEntry Objects
	 */

	public static function _mergeEntries($entriesA, $entriesB) {
		foreach($entriesB as $key => $b) {
			if(array_key_exists($key, $entriesA)) {
				//meaning country in B is also in A, then merge cities
				$aCities = $entriesA[$key]->getCountry()->getCityLocationList();
				foreach($b->getCountry()->getCityLocationList() as $cityKey => $bCity) {
					if(!array_key_exists($cityKey, $aCities))
						$aCities[$cityKey] = $bCity;
				}
  				$entriesA[$key]->getCountry()->setCityLocations($aCities);

				//if Editable attribute in B is false, make sure to merge it to A
				if (!$b->getIsEditable)
					$entriesA[$key]->setIsEditable(false);
			}
			else {
				$entriesA[$key] = $b;
			}
		}
		return $entriesA;
	}
	
	/**
	 *  new method to retrieve TravelMapEntry
	 *  @param int $id
	 *  @param int $locationID
	 *  @param boolean $isSync
	 *
	 *  @return TravelMapEntry Object | NULL
	 */
	// DEPRECATED
	// Note by: Adelbert Silla

	public static function retrieveTravelMapEntry($id = 0, $locationID = 0, $isSync = null) {
		if(!$id || !$locationID || is_null($isSync)) return null;
		
		if($isSync)
			return self::doRetrieveSyncTravelMapEntry($id, $locationID);
		else
			return self::doRetrieveUnSyncTravelMapEntry($id, $locationID);
	}
	
	// DEPRECATED
	// Note by: Adelbert Silla
	private static function doRetrieveSyncTravelMapEntry($travelerID, $locationID) {
	    $entries = MyTravelMapDAO::_getTravelMapEntriesSynced($travelerID);
	    $entriesLocationsTravelled = MyTravelMapDAO::_getTravelMapEntriesFromLocationsTraveledTo($travelerID);
	    //convert $travelerID to $userID
	    //TODO: needs refactoring! should avoid this
	    require_once('travellog/model/socialApps/mytravelmap/gaOpenSocialTravelerMapper.php');
    	$userID = gaOpenSocialTravelerMapper::getUserIDWithTravelerID($travelerID);
    	$entriesFromTravelPlans = MyTravelMapDAO::_getTravelMapEntriesFromMyTravelPlans($userID);
    	
	    if($entries) {
	    	if($entriesLocationsTravelled)
	    		$entries = self::_mergeEntries($entries,$entriesLocationsTravelled);
	    	if($entriesFromTravelPlans)
	    		$entries = self::_mergeEntries($entries,$entriesFromTravelPlans);
	   	}
	   	else if($entriesLocationsTravelled){
	   		$entries = $entriesLocationsTravelled;
	   		if($entriesFromTravelPlans)
	    		$entries = self::_mergeEntries($entries,$entriesFromTravelPlans);
	   	}
	   	else
	   		$entries = $entriesFromTravelPlans;
	   	
	    if($entries) {
	    	return $entries[$locationID];
	    }
	    return null;
	}

	// DEPRECATED
	// Note by: Adelbert Silla
	private static function doRetrieveUnSyncTravelMapEntry($userID, $locationID) {
		$conn = self::getConnection('');
		
	    $sql = "SELECT a.travelMapID, a.userID, a.countryID, a.locationID, b.lat, b.long
	            FROM SocialApplication.tbTravelMap AS a 
	            LEFT JOIN Travel_Logs.tblCoordinate AS b ON a.locationID=b.locID
	            WHERE a.userID=$userID AND a.locationID=$locationID";
	    
	    $resource = $conn->execQuery($sql);
	    
		if($record = mysql_fetch_assoc($resource)) {
			require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocation.php');
			require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCountryLocation.php');
			require_once('travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCityLocation.php');
			require_once('travellog/model/socialApps/mytravelmap/facebook/Class.TravelMapEntry.php');
			require_once('travellog/model/Class.LocationType.php');
		
			$entry = new TravelMapEntry();
			$entry->setTravelMapID($record['travelMapID']);
			$entry->setUserID($record['userID']);
			$entry->_setLocation($record['locationID']);
			
			//query for country
			
			$fb_location = new FBLocation();
	        $fb_location->setID($record['locationID']);
	        
	        $location = $fb_location->getLocation();
	        $type = $fb_location->getLocationType();
	        
	        if($type  == LocationType::$COUNTRY) {
	        	$country_location = new FBCountryLocation();
	        	$country_location->setID($record['locationID']);
	        	$country_location->setLatitude($record['lat']);
	        	$country_location->setLongitude($record['long']);
	        	$country_location->setCountryID($location->getCountryID());
	        	$country_location->setName($location->getName());
	        	$country_location->setLocationType($type);
	        	
	        	//get cities
	        	$country_location->setCityLocations(self::_getCityLocationsListInMapEntry($record['locationID'], $record['userID']));
	        	
	        	$entry->setCountry($country_location);
	        }
	        if($type == LocationType::$CITY || $type == LocationType::$NEWCITY) {
	        	$country = $location->getCountry();
	        	$country_location = new FBCountryLocation();
	        	$country_location->setID($country->getLocationID());
	        	$country_location->setLatitude($country->getCoordinates()->getY());
	        	$country_location->setLongitude($country->getCoordinates()->getX());
	        	$country_location->setCountryID($country->getCountryID());
	        	$country_location->setName($country->getName());
	        	$country_location->setLocationType(LocationType::$COUNTRY);
	        	
	        	$city_location = new FBCityLocation();
	        	$city_location->setID($location->getLocationID());
	        	$city_location->setLatitude($record['lat']);
	        	$city_location->setLongitude($record['long']);
	        	$city_location->setCountryID($country->getCountryID());
	        	$city_location->setCityID($location->getCityID());
	        	$city_location->setName($country->getName());
	        	$city_location->setLocationType($type);
	        	
	        	$country_location->addCityLocation($city_location);
	        	
	        	$entry->setCountry($country_location);
	        }
			
			return $entry;
		}
		else {
			return null;
		}
	}	
	
	/**
     *  new method to add a travel map entry of a sync account
     *  @param $travelerID the ID of dotNet  traveler
     *  @param $locationIDs array of locationIDs
     */
    public static function _addSyncedTravelMapEntry($travelerID = 0, $locationIDs = array(), $countryID = 0) {
	
    	if(!$travelerID || !$locationIDs || !$countryID) return false;
    	
		$conn = self::getConnection('Travel_Logs');
    	if ($count = count($locationIDs)) {
	        $sql = 'INSERT INTO tblTravelertoCountriesTravelled (travelerID, countryID, locationID) VALUES';
			for ($i=0; $i <$count ; $i++) {
				$sql .= " ( $travelerID, $countryID, $locationIDs[$i] ),";
			}

			
			return $conn->execQuery(substr($sql,0,-1));			
		}
    }

    /**
     *  new method to remove a travel map entry by countryID
     *  @param $travelerID the ID of dotNet traveler
     *  @param $countryID 
     */
    public static function _removeTravelMapEntryByCountryID($travelerID = null, $countryID = null) {
    	if(!$travelerID || !$countryID) return false;
		$conn = self::getConnection('Travel_Logs');

		$sql = "DELETE FROM tblTravelertoCountriesTravelled WHERE travelerID=$travelerID AND countryID = $countryID";
		return $conn->execQuery($sql);
    }
      
    /**
     *  new method to remove a travel map entry of a sync account
     *  @param $travelerID the ID of dotNet  traveler
     *  @param $locationIDs array of locationIDs
     */
    public static function _removeSyncedTravelMapEntry($travelerID = 0, $locationIDs = array()) {
    	if(!$travelerID || !$locationIDs) return false;
		$conn = self::getConnection('Travel_Logs');

		$sql = "DELETE FROM tblTravelertoCountriesTravelled WHERE travelerID=$travelerID AND locationID IN (" .implode(',',$locationIDs). ")";
		return $conn->execQuery($sql);
    }
    
    /**
     *  new method to add a travel map entry of an unsync account
     *  @param $userID
     *  @param $locationIDs array of locationIDs
     */
    public static function _addUnSyncedTravelMapEntry($userID = 0, $locationIDs = array(), $countryID = 0) {
    	if(!$userID || !$locationIDs) return false;
		$conn = self::getConnection('SocialApplication');
		
    	if ($count = count($locationIDs)) {
	        $sql = 'INSERT INTO tbTravelMap (userID, countryID, locationID) VALUES';
			for ($i=0; $i <$count ; $i++) {
				$comma = $i == ($count-1) ? '' : ',';
				$sql .= " ( $userID, $countryID, $locationIDs[$i] ) $comma";
			}
			return $conn->execQuery($sql);			
		}
    }
    
    /**
     *  new method to remove a travel map entry of an unsync account
     *  @param $userID
     *  @param $locationIDs array of locationIDs
     */
    public static function _removeUnSyncedTravelMapEntry($userID = 0, $locationIDs = array()) {
    	if(!$userID || !$locationIDs) return false;
		$conn = self::getConnection('SocialApplication');

		$sql = "DELETE FROM tbTravelMap WHERE userID=$userID AND locationID IN (" .implode(',',$locationIDs). ")";
		return $conn->execQuery($sql);
    }

	// Created by: Adelbert D. Silla
	// New Method to retrieve Map Entries

	public static function __retrieveTravelMapEntry($countryID=null, $userID=null, $travelerID=null) {

		require_once 'Class.iRecordset.php';
		if(!$userID || !$countryID) return null;

		$mapEntry = $travelerID
		? self::__retrieveSyncMapEntries($userID, $travelerID, $countryID)
		: self::__retrieveUnsyncMapEntries($userID, $countryID);
		return isset($mapEntry[key($mapEntry)]) ? $mapEntry[key($mapEntry)] : null;
	}

    
	static function __retrieveSyncMapEntries($userID=null,$travelerID=null, $countryID=null) {
		list($entries, $countries, $cities) = self::getMapEntriesFromJournals($travelerID,$countryID);
		list($entries, $countries) = self::getMapEntriesFromPlans($userID, $entries, $countries, $countryID);
				
		$recordsets = self::getMapEntriesByIdAndSyncStatus($travelerID,true,$countryID);
		foreach ($recordsets as $record) {
	        if ($record['locationtypeID'] == 2 && !in_array($record['locationID'], array_keys($entries))) { 
           		$entry = self::getTravelMapEntry($record,$userID);
				if(!in_array($record['locationID'], array_values($entries))) {
					$entries[$record['locationID']] = $entry;
	        		$countries[$record['countryID']] = $record['locationID'];
				}
			} 
			elseif($record['locationtypeID'] != 2) {
				$city = self::getCityLocation($record);
				$cities[$record['countryID']][$record['locationID']] = $city;
			}
		}
        
		// echo '<hr><pre>';
		// var_dump(array_keys($entries));
		// echo '<hr>';		
		// var_dump(array_values($countries));
		// exit;
		foreach ($countries as $key => $country) {
			if(isset($cities[$key])) {
				$entries[$country]->getCountry()->setCityLocations($cities[$key]);				
			}

		}

		return $entries;					
	}

	
	static function __retrieveUnsyncMapEntries($userID=0,$countryID=0) {
		if($recordsets = self::getMapEntriesByIdAndSyncStatus($userID,false,$countryID)) {
			$cities = array(); $entries = array(); $countries = array();
			foreach ($recordsets as $record) {
		        if ($record['locationtypeID'] == 2) { 
             		$entry = self::getTravelMapEntry($record,$userID);
					$entries[$record['locationID']] = $entry;
	        		$countries[$record['countryID']] = $record['locationID'];
				} else {
					$city = self::getCityLocation($record);
					$cities[$record['countryID']][$record['locationID']] = $city;
				}
			}
			foreach ($countries as $key => $country) {
				$entries[$country]->getCountry()->setCityLocations($cities[$key]);
			}
			return $entries;					
		} else return array();
	}

	function getTravelMapEntry($record = array(),$id = null) {
		if(class_exists('TravelMapEntry'));
		
		$entry = new TravelMapEntry;
		$entry->setTravelMapID(isset($record['travelMapID']) ? $record['travelMapID'] : 0);
		$entry->setUserID($id);
		$entry->_setLocation($record['locationID']);
		$country_location = self::getCountryLocation($record);
    	$entry->setCountry($country_location);
		return $entry;
	}
	
	function getCountryLocation($record = array()) {
		// if(!isset($record['countryName'])) {
		// 	echo '<pre>';
		// 	var_dump($record);exit;
		// }
		$name = !isset($record['countryName']) ? $record['name'] : $record['countryName'];
    	$country_location = new FBCountryLocation;
		$locationID = isset($record['countryLocID']) ? $record['countryLocID'] : $record['locationID'];
    	$country_location->setID($locationID);
    	$country_location->setLatitude($record['lat']);
    	$country_location->setLongitude($record['long']);
    	$country_location->setCountryID($record['countryID']);
    	$country_location->setName($name);
    	$country_location->setLocationType($record['locationtypeID']);
		return $country_location;
	}

	function getCityLocation($record = array()) {
		$city = new FBCityLocation;
		$city->setID($record['locationID']);
		$city->setName($record['name']);
		$city->setCountryID($record['countryID']);
		$city->setLocationType($record['locationtypeID']);
		$city->setLatitude($record['lat']);
		$city->setLongitude($record['long']);
		return $city;
	}

	static function getMapEntriesByIdAndSyncStatus($id = null, $isSync = false, $countryID = 0) {
		if($id) {
			$conn = self::getConnection();
			if(!$isSync) {   
				$fieldNameUserID = 'a.userID';
				$fieldNameTableID = 'a.travelMapID';
				$tableName = 'SocialApplication.tbTravelMap';

			} else {
				$fieldNameUserID = 'a.travelerID';
				$fieldNameTableID = 'a.tctravelledid';
				$tableName = 'Travel_Logs.tblTravelertoCountriesTravelled';
			} 

			$addedCriteria = $countryID ? "AND c.countryID = $countryID " : '';
			$sql = "SELECT $fieldNameTableID AS mapID, c.countryID, a.locationID, d.lat, d.long, c.country AS name, b.locationtypeID FROM $tableName AS a
					LEFT JOIN Travel_Logs.tblLocation AS b ON a.locationID = b.locID 
					LEFT JOIN GoAbroad_Main.tbcountry AS c ON b.locID = c.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID = d.locID 
					WHERE $fieldNameUserID = $id AND b.locationtypeID = 2 $addedCriteria
					UNION
					SELECT $fieldNameTableID AS mapID, c.countryID, a.locationID, d.lat, d.long, c.city AS name, b.locationtypeID FROM $tableName AS a
					LEFT JOIN Travel_Logs.tblLocation AS b ON a.locationID = b.locID 
					LEFT JOIN GoAbroad_Main.tbcity AS c ON b.locID = c.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID = d.locID 
					WHERE $fieldNameUserID = $id AND b.locationtypeID = 4 $addedCriteria
					UNION
					SELECT $fieldNameTableID AS mapID, c.countryID, a.locationID, d.lat, d.long, c.city AS name, b.locationtypeID FROM $tableName AS a
					LEFT JOIN  Travel_Logs.tblLocation AS b ON a.locationID = b.locID 
					LEFT JOIN Travel_Logs.tblNewCity AS c ON b.locID = c.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID = d.locID 
					WHERE $fieldNameUserID = $id AND b.locationtypeID = 6 $addedCriteria";

		 	return new iRecordset($conn->execQuery($sql));
		} else return array();
	} 

	static function getMapEntriesFromJournals($travelerID=null,$countryID = 0) {
		$cities = array(); $entries = array(); $countries = array();
		if($travelerID) {
	  		$conn = self::getConnection();
			require_once 'travellog/model/Class.Traveler.php';
			$addedCriteria = $countryID ? "AND d.countryID = $countryID " : '';

			/*$sql = "SELECT a.arrival, b.travellogID, b.travelID, d.countryID, a.locID AS locationID, a.locID AS countryLocID, f.lat, f.long, d.country AS name, d.country AS countryName, e.locationtypeID FROM 
			 AS a 
					LEFT JOIN Travel_Logs.tblTravelLog 	AS b ON a.tripID = b.tripID
					LEFT JOIN Travel_Logs.tblTravel    	AS c ON b.travelID = c.travelID
					LEFT JOIN GoAbroad_Main.tbcountry  	AS d ON a.locID = d.locID 
					LEFT JOIN Travel_Logs.tblLocation  	AS e ON d.locID = e.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS f ON e.locID = f.locID 
					WHERE b.deleted = 0 AND e.locationtypeID = 2 AND c.deleted = 0 AND c.entryCount != 0 AND c.publish = 1 AND c.travelerID=$travelerID $addedCriteria GROUP BY a.locID
					UNION
					SELECT a.arrival, b.travellogID, b.travelID, d.countryID, a.locID AS locationID, e.locID AS countryLocID, g.lat, g.long, d.city AS name, e.country AS countryName, f.locationtypeID FROM Travel_Logs.tblTrips AS a 
					LEFT JOIN Travel_Logs.tblTravelLog 	AS b ON a.tripID = b.tripID
					LEFT JOIN Travel_Logs.tblTravel 	AS c ON b.travelID = c.travelID
					LEFT JOIN GoAbroad_Main.tbcity 		AS d ON a.locID = d.locID 
					LEFT JOIN GoAbroad_Main.tbcountry 	AS e ON d.countryID = e.countryID 
					LEFT JOIN Travel_Logs.tblLocation 	AS f ON d.locID = f.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS g ON f.locID = g.locID 
					WHERE b.deleted = 0 AND f.locationtypeID = 4 AND c.deleted = 0 AND c.entryCount != 0 AND c.publish = 1 AND c.travelerID=$travelerID $addedCriteria GROUP BY a.locID
					UNION
					SELECT a.arrival, b.travellogID, b.travelID, d.countryID, a.locID AS locationID, e.locID AS countryLocID, g.lat, g.long, d.city AS name, e.country AS countryName, f.locationtypeID FROM Travel_Logs.tblTrips AS a 
					LEFT JOIN Travel_Logs.tblTravelLog 	AS b ON a.tripID = b.tripID
					LEFT JOIN Travel_Logs.tblTravel 	AS c ON b.travelID = c.travelID
					LEFT JOIN Travel_Logs.tblNewCity 	AS d ON a.locID = d.locID 
					LEFT JOIN GoAbroad_Main.tbcountry 	AS e ON d.countryID = e.countryID 
					LEFT JOIN Travel_Logs.tblLocation 	AS f ON d.locID = f.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS g ON f.locID = g.locID 
					WHERE b.deleted = 0 AND f.locationtypeID = 6 AND c.deleted = 0 AND c.entryCount != 0 AND c.publish = 1 AND c.travelerID=$travelerID $addedCriteria GROUP BY a.locID";

			$result = new iRecordset($conn->execQuery($sql));
			foreach ($result as $record) {					
		        if ($record['locationtypeID'] == 2 || ($record['locationtypeID'] != 2 && !in_array($record['countryLocID'], array_keys($entries)))) { 
	         		$entry = self::getTravelMapEntry($record,$travelerID);
					$entry->setIsEditable(false);
					$entry->setTravelID($record['travelID']);
					$entry->setTravelLogID($record['travellogID']);
					$entries[$record['countryLocID']] = $entry;
	        		$countries[$record['countryID']] = $record['countryLocID'];
				} 
				if($record['locationtypeID'] != 2 ) {
					$city = self::getCityLocation($record);
					$city->setIsEditable(false);
					$cities[$record['countryID']][$record['locationID']] = $city;
				}
			}*/
			
			$sql = "SELECT a.arrival, b.travellogID, b.travelID, d.countryID, a.locID AS locationID, a.locID AS countryLocID, f.lat, f.long, d.country AS name, d.country AS countryName, e.locationtypeID FROM Travel_Logs.tblTrips AS a 
					LEFT JOIN Travel_Logs.tblTravelLog 	AS b ON a.tripID = b.tripID
					LEFT JOIN Travel_Logs.tblTravel    	AS c ON b.travelID = c.travelID
					LEFT JOIN Travel_Logs.tblTravelLink	AS x ON c.travellinkID = x.travellinkID
					LEFT JOIN GoAbroad_Main.tbcountry  	AS d ON a.locID = d.locID 
					LEFT JOIN Travel_Logs.tblLocation  	AS e ON d.locID = e.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS f ON e.locID = f.locID 
					WHERE b.deleted = 0 AND e.locationtypeID = 2 AND c.deleted = 0 AND c.entryCount != 0 AND c.publish = 1 AND x.reftype=1 AND c.travelerID=$travelerID $addedCriteria 
					UNION
					SELECT a.arrival, b.travellogID, b.travelID, d.countryID, a.locID AS locationID, e.locID AS countryLocID, g.lat, g.long, d.city AS name, e.country AS countryName, f.locationtypeID FROM Travel_Logs.tblTrips AS a 
					LEFT JOIN Travel_Logs.tblTravelLog 	AS b ON a.tripID = b.tripID
					LEFT JOIN Travel_Logs.tblTravel 	AS c ON b.travelID = c.travelID
					LEFT JOIN Travel_Logs.tblTravelLink	AS x ON c.travellinkID = x.travellinkID	
					LEFT JOIN GoAbroad_Main.tbcity 		AS d ON a.locID = d.locID 
					LEFT JOIN GoAbroad_Main.tbcountry 	AS e ON d.countryID = e.countryID 
					LEFT JOIN Travel_Logs.tblLocation 	AS f ON d.locID = f.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS g ON f.locID = g.locID 
					WHERE b.deleted = 0 AND f.locationtypeID = 4 AND c.deleted = 0 AND c.entryCount != 0 AND c.publish = 1 AND x.reftype=1 AND c.travelerID=$travelerID $addedCriteria 
					UNION
					SELECT a.arrival, b.travellogID, b.travelID, d.countryID, a.locID AS locationID, e.locID AS countryLocID, g.lat, g.long, d.city AS name, e.country AS countryName, f.locationtypeID FROM Travel_Logs.tblTrips AS a 
					LEFT JOIN Travel_Logs.tblTravelLog 	AS b ON a.tripID = b.tripID
					LEFT JOIN Travel_Logs.tblTravel 	AS c ON b.travelID = c.travelID
					LEFT JOIN Travel_Logs.tblTravelLink	AS x ON c.travellinkID = x.travellinkID	
					LEFT JOIN Travel_Logs.tblNewCity 	AS d ON a.locID = d.locID 
					LEFT JOIN GoAbroad_Main.tbcountry 	AS e ON d.countryID = e.countryID 
					LEFT JOIN Travel_Logs.tblLocation 	AS f ON d.locID = f.locID
					LEFT JOIN Travel_Logs.tblCoordinate AS g ON f.locID = g.locID 
					WHERE b.deleted = 0 AND f.locationtypeID = 6 AND c.deleted = 0 AND c.entryCount != 0 AND c.publish = 1 AND x.reftype=1 AND c.travelerID=$travelerID $addedCriteria 
					ORDER BY arrival DESC";
			
			$result = new iRecordset($conn->execQuery($sql));
			$arrLocIDs = array(); $arrJournalEntryCount = array();
			foreach ($result as $record) {			    
			    $existLocID = false;
                if (in_array($record['locationID'], $arrLocIDs))
                    $existLocID = true;
                else
                    $arrLocIDs[] = $record['locationID'];
			    if(isset($arrJournalEntryCount[$record['locationID']]))
			    	$arrJournalEntryCount[$record['locationID']] += 1;
			    else
			    	$arrJournalEntryCount[$record['locationID']] = 1;
			    if ($record['locationtypeID'] != 2) {
			    	if (isset($arrJournalEntryCount[$record['countryLocID']]))
			    		$arrJournalEntryCount[$record['countryLocID']] += 1;
			    	else
			    		$arrJournalEntryCount[$record['countryLocID']] = 1;
			    }
			    if ($record['locationtypeID'] == 2 || ($record['locationtypeID'] != 2 && !in_array($record['countryLocID'], array_keys($entries)))) {
    			    if (!$existLocID) {
    			        $entry = self::getTravelMapEntry($record, $travelerID);
    			        $entry->setIsEditable(false);
    			        $entry->getCountry()->setTravelLogID($record['travellogID']);
    			        $entry->getCountry()->setIsEditable(false);
    			        $entries[$record['countryLocID']] = $entry;
    			        $countries[$record['countryID']] = $record['countryLocID'];
    			    }
    			    $entries[$record['countryLocID']]->getCountry()->setJournalEntriesCount($arrJournalEntryCount[$record['countryLocID']]);
    			}
    			if ($record['locationtypeID'] != 2) {
    			    if (!$existLocID) {
    			        $city = self::getCityLocation($record);
    			        $city->setIsEditable(false);
    			        $city->setTravelLogID($record['travellogID']);
    			        $cities[$record['countryID']][$record['locationID']] = $city;
    			    }
    			    $cities[$record['countryID']][$record['locationID']]->setJournalEntriesCount($arrJournalEntryCount[$record['locationID']]);
                    $entries[$record['countryLocID']]->getCountry()->setJournalEntriesCount($arrJournalEntryCount[$record['countryLocID']]);
			    }
			}
		}

		return array($entries,$countries,$cities);
	}
	
	function getMapEntriesFromPlans($userID = null, $entries, $countries,$countryID=null) {
		if(!$userID) return false;
		$inArray = $countries ? "AND c.locID NOT IN(".implode(',',array_keys($countries)).")" : '';
		$addedCriteria = $countryID ? 'AND c.countryID = '.$countryID : '';
		$conn = self::getConnection();
		$sql = "SELECT c.countryID, c.locID AS locationID, d.lat, d.long, c.country AS name
	            FROM SocialApplication.tbTravelWish AS a
	            LEFT JOIN SocialApplication.tbTravelWishToCountry AS b ON a.travelWishID=b.travelWishID
	            LEFT JOIN GoAbroad_Main.tbcountry AS c ON b.countryID=c.countryID
	            LEFT JOIN Travel_Logs.tblCoordinate AS d ON c.locID=d.locID
	            WHERE a.userID=$userID AND b.startDate <> '0000-00-00' 
				AND b.startDate IS NOT NULL AND b.startDate <= CURRENT_DATE() $inArray $addedCriteria";
				
		$result = new iRecordset($conn->execQuery($sql));
		foreach ($result as $record) {					
			$record['locationtypeID'] = 2;
       		$entry = self::getTravelMapEntry($record,$userID);
            $entry->setIsEditable(false);
			$entries[$record['locationID']] = $entry;
       		$countries[$record['countryID']] = $record['locationID'];
		}
		
		return array($entries,$countries);
	}
	
	public static function getMtmUserCacheKey($id) {
		return 'TravelMapEntriesofUser_'.$id;
	}
	
	// public function getCities($travelerID,$locationID) {
	// 
	// 	$sql = "SELECT a. * FROM GoAbroad_Main.tbcity AS a
	// 	WHERE a.approved = 1 AND a.countryID = 111 AND a.locID NOT 
	// 	IN ( SELECT c.locationID FROM SocialApplication.tbTravelMap AS c
	// 	WHERE c.userID = 71699 AND c.countryID = 111 ) AND a.countryID = 111"
	// 
	// 	$sql = "SELECT a. * FROM GoAbroad_Main.tbcity AS a
	// 	WHERE a.approved = 1 AND a.countryID = 111 AND a.locID NOT 
	// 	IN ( SELECT c.locationID FROM Travel_Logs.tblTravelertoCountriesTravelled AS c
	// 	WHERE c.userID = 71699 AND c.countryID = 111 ) AND a.countryID = 111"
	// 	
	// 	$a= "SELECT a.locID FROM Travel_Logs.tblTrips AS a 
	// 		LEFT JOIN Travel_Logs.tblTravelLog 	AS b ON a.tripID = b.tripID 
	// 		LEFT JOIN Travel_Logs.tblTravel	AS c ON b.travelID = c.travelID 
	// 		where b.deleted = 0 AND c.deleted = 0 and c.travelerID=8815 "
	// 
	// 	$sql = "SELECT c.locationID FROM SocialApplication.tbTravelMap AS c WHERE c.userID = 71699 AND c.countryID = 111";
	// 
	// 
	// 	$sql = "SELECT c.locationID FROM Travel_Logs.tblTravelertoCountriesTravelled AS c
	// 	WHERE c.userID = 71699 AND c.countryID = 111"
	// 
	// }
}
?>