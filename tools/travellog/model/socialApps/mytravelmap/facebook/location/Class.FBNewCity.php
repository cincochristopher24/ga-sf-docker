<?php

require_once('travellog/model/Class.NewCity.php');

class FBNewCITY extends NewCity {

	private $mIsEditable = true;
	
	function save(){
		if (!$this->isExist()){
		    $this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		    $this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
		    $this->rs3	=	new Recordset(ConnectionProvider::instance()->getConnection());
		    
		    $sql = "SELECT * FROM tblNewCity " .
		    		"WHERE city LIKE '" . addslashes($this->getName()) . "' " .
		    		"AND countryID = " . $this->getCountryID() . " " .
		    		"GROUP BY cityID";
		    $this->rs->Execute($sql);

		    if (0 == $this->rs->Recordcount()){
		    	// insert a row first in tblLocation
		    	$sql2 = "INSERT INTO tblLocation (locationtypeID) " .
		    			"VALUES ('" . LocationType::$NEWCITY . "')";
		    	$this->rs2->Execute($sql2);

		    	$sql = "SELECT LAST_INSERT_ID() as lastId";
		    	$this->rs->Execute($sql);

		    	$this->setLocationID($this->rs->Result(0, "lastId"));

		    	// insert a row in tbcity
		    	$sql3 = "INSERT INTO tblNewCity (countryID, city, locID) " .
		    			"VALUES (" . $this->getCountryID() . ", '" . addslashes($this->getName()) . "', " . $this->getLocationID() . ")";
		    	$this->rs3-> Execute($sql3);
		    	$this->cityID = mysql_insert_id();
		    }
		    else{
		    	$this->cityID = $this->rs->Result(0, "cityID");
		    	$this->setLocationID($this->rs->Result(0, "locID"));
		    }
		}
		else
		    $this->update();
	}
		
	function update(){
	    if ($this->isExist()){
	    	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	    	$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
	    	
	    	// edit row in tblLocation
	    	$sql = "update tblLocation set locationtypeID = '" . LocationType::$CITY . "' where locID = " . $this->getLocationID();
	    	$this->rs-> Execute($sql);
	
	    	// delete row from tblNewCity
	    	$sql2 = "delete from tblNewCity where cityID = " . $this->getCityID();
	    	$this-> rs2-> Execute($sql2);
	
	    	// insert row new row in tbcity
	    	$sql = "insert into GoAbroad_Main.tbcity (countryID, city, approved, locID) values (". $this->getCountryID() . ", '" . addslashes($this->getName()) . "', " . 
	    			$this->getApprove() . ", " . $this->getLocationID() . ")";
	    	$this-> rs-> Execute($sql);
	    }
	    else
	    	$this-> save();
	}
	
	function delete(){
	    if ($this->isExist()){
	    	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	    	$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
	    	
	    	// delete row from tblNewCity
	    	$sql = "delete from tblNewCity where cityID = " . $this->getCityID();
	    	$this-> rs-> Execute($sql);
	
	    	// delete row from tblLocation
	    	$sql2 = "delete from tblLocation where locID = " . $this->getLocationID();
	    	$this-> rs2-> Execute($sql2);
	    }
	}
	
	function getIsEditable() {
	    return $this->mIsEditable;
	 }
  
	function setIsEditable($mIsEditable = true) {
    	$this->mIsEditable = $mIsEditable;
  	}

}

?>
