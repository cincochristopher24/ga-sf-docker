<?php

require_once('travellog/model/socialApps/mytravelmap/facebook/location/abstract/Class.AbstractFBLocation.php');

class FBCityLocation extends AbstractFBLocation {

	private $mCityID;
	private $mCountryRegionID;
	private $mCountryID;
	private $mCountryLocation;
	private $mContinentID;
	private $mState;
	
	public function setCityID($v) {
		if($v) {
			$this->mCityID = $v;
		}
		return $this;
	}
	
	public function getCityID() {
		return $this->mCityID;
	}
	
	public function setCountryRegionID($v) {
		if($v) {
			$this->mCountryRegionID = $v;
		}
		return $this;
	}
	
	public function getCountryRegionID() {
		return $this->mCountryRegionID;
	}
	
	public function setRegionID($v) {
		if($v) {
			$this->mCountryRegionID = $v;
		}
		return $this;
	}
	
	public function getRegionID() {
		return $this->mCountryRegionID;
	}
	
	public function setCountryID($v) {
		if($v) {
			$this->mCountryID = $v;
		}
		return $this;
	}
	
	public function getCountryID() {
		return $this->mCountryID;
	}
	
	public function setStateID($v) {
		if($v) {
			$this->mStateID = $v;
		}
		return $this;
	}
	
	public function getStateID() {
		return $this->mStateID;
	}
	
	public function setState($v) {
		if($v) {
			$this->mState = $v;
		}
		return $this;
	}
	
	public function getState() {
		return $this->mState;
	}
	
	public function setCountry($v) {
		if($v) {
			$this->mCountryLocation = $v;
		}
		return $this;
	}
	
	public function getCountry() {
		return $this->mCountryLocation;
	}
	
	public function setContinentID($v) {
		if($v) {
			$this->mContinentID = $v;
		}
		return $this;
	}
	
	public function getContinentID() {
		return $this->mContinentID;
	}	
}

?>
