<?php

require_once('travellog/model/socialApps/mytravelmap/facebook/location/abstract/Class.AbstractFBLocation.php');

class FBCountryLocation extends AbstractFBLocation {

	private $mCountryID;
	private $mCityLocationList = array();
	private	$arrCityListName = array();
	
	public function setCountryID($v) {
		if ($this->mCountryID != $v) {
			$this->mCountryID = $v;
		}
		return $this;
	}

	public function getCountryID() {
		return $this->mCountryID;
	}
	
	public function addCityLocation($v) {
		if($v) {
			$this->mCityLocationList[$v->getID()] = $v; 
		}
		return $this;
	}
	
	public function setCityLocations($v) {
		if($v) {
			$this->mCityLocationList = $v;
		}
		return $this;
	}
	
	public function getCityLocationList() {
		uasort($this->mCityLocationList, array("FBCountryLocation", "compareCityName"));
		return $this->mCityLocationList;
	}
	
	public function getFlag() {
		return 'http://www.goabroad.com/images/flags/flag'.$this->getCountryID().'.gif';
	}
	
	static function compareCityName($a, $b)
    {
   		return strcmp(strtolower($a->getName()), strtolower($b->getName()));
 	}

}

?>
