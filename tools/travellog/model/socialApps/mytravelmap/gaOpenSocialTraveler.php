<?php
/**
	filename:	gaOpenSocialTraveler.php
	@author:	Jonas Tandinco
	created:	Sep/25/2008
	purpose:	represents a traveler account used for opensocial apps only
	
	EDIT HISTORY:
*/

class gaOpenSocialTraveler {
	
	protected
		$ID          = NULL,
		$isSuspended = NULL;
		
	public function getID() {
		return $this->ID;
	}
	
	public function isSuspended() {
		return $this->isSuspended == 1;
	}
	
	public function setID($v) {
		$this->ID = $v;
	}
	
	public function setIsSuspended($v) {
		$this->isSuspended = $v;
	}
	
}