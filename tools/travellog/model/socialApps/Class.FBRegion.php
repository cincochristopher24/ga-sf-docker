<?php
	/*
	 * Class.FBRegion.php
	 * Created on Jul 12, 2007
	 * created by marc
	 */
	 
	require_once("gaexception/Class.InstantiationException.php");
	require_once("travellog/model/Class.Country.php");
 	require_once("Class.Connection.php");
 	require_once("Class.Recordset.php");
	 
	class fbRegion{
		
		private $fbRegionID;
		private $fbRegion;
		private $countries = array();
		private $conn;
		private $rs;
		
		
		function __construct( $fbRegionID = 0 ){
	 		
	 		$this->conn = new Connection();
	 		$this->rs = new Recordset($this->conn);
	 		
	 		if( $fbRegionID ){
	 			$sqlquery = "SELECT * FROM GoAbroad_Main.tbregion WHERE regionID = $fbRegionID";
	 			$this->rs->execute($sqlquery);
	 			
	 			if ($this->rs->Recordcount() == 0){				
					throw new InstantiationException("Region does not exist.");	
				}
				
				$this-> fbRegionID = $this->rs->Result(0, "regionID");
				$this-> fbRegion = $this->rs->Result(0, "region");
				
				// 104 = Multi-country
				$sqlquery = "SELECT countryID FROM GoAbroad_Main.tbcountry WHERE regionID = $fbRegionID AND approved = 1 AND countryID <> 104 ORDER BY country";
	 			$this->rs->execute($sqlquery);
	 			
	 			while( $tmpRow = mysql_fetch_array($this->rs->ResultSet()) )
	 				$this->countries[] = new Country($tmpRow["countryID"]);	
	 			
	 		}
	 	}
	 	
	 	function setFbRegion($fbRegion){
	 		$this->fbRegion = trim($fbRegion);
	 	}
	 	
	 	function setFbRegionID($fbRegionID){
	 		$this->regionID = $fbRegionID;
	 	}
	 	
	 	function getFbRegion(){
	 		return $this->fbRegion;
	 	}
	 	
	 	function getFbRegionID(){
	 		return $this->fbRegionID;
	 	}
	 	
	 	function getCountries(){
	 		return $this->countries;
	 	}
	 	
	 	function create(){
	 	
	 		$sqlQuery = "INSERT INTO GoAbroad_Main.tbregion " .
	 				"SET region = '" . $this->fbRegion . "'";
	 		$this->rs->execute($sqlQuery);
	 		$this->fbRegionID = mysql_insert_id();
	 					
	 	}
	 	
	 	function update(){
	 		$sqlQuery = "UPDATE GoAbroad_Main.tbregion " .
	 				"SET region = '" . $this->fbRegion . "' " .
	 				"WHERE regionID = " . $this->fbRegionID;
	 		$this->rs->execute($sqlQuery);
	 	}
	 	
	 	function delete(){
	 		$sqlQuery = "DELETE FROM GoAbroad_Main.tbregion " .
	 				"WHERE regionID = " . $this->fbRegionID;
	 		$this->rs->execute($sqlQuery);
	 	}
	 	
	 	static function getAllRegions(){
	 		
	 		$conn = new Connection();
	 		$rs = new Recordset($conn);
	 		$arrRegions = array();
	 		
	 		$sqlQuery = "SELECT regionID FROM GoAbroad_Main.tbregion ORDER BY region";
	 		$rs->execute($sqlQuery);
	 		while( $tmpRow = mysql_fetch_array($rs->Resultset()))
	 			$arrRegions[] = new fbRegion($tmpRow["regionID"]);
	 		
	 		return $arrRegions;
	 		
	 	}
		
	}
?>
