<?php
/*
	Filename:		Class.fbNeedToUpdateAccountPeer.php
	Author:			Adelbert Silla
	Date created:	Aug. 10, 2009
	Purpose:		contains functions for fbNeedToUpdateAccount CRUD and other related functions
*/

require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class fbNeedToUpdateAccountPeer {
	
	//CRUD
	static function create($fbNeedToUpdateAccount){
		$conn = self::getConnection();
		$qry = "INSERT INTO tblfbNeedToUpdateAccount(travelerID, application, status, dateUpdated) 
				VALUES(".$fbNeedToUpdateAccount->getTravelerID().
				",'".self::escapeString($fbNeedToUpdateAccount->getApplication()).
				"','".self::escapeString($fbNeedToUpdateAccount->getStatus()).
				"','".self::escapeString($fbNeedToUpdateAccount->getDateUpdated())."')";
				
		$conn->execQuery($qry);		
	}

	static function update($fbNeedToUpdateAccount){
		$conn = self::getConnection();
		$qry = "UPDATE tblfbNeedToUpdateAccount 
				SET travelerID=".$fbNeedToUpdateAccount->getTravelerID().
				", application='".self::escapeString($fbNeedToUpdateAccount->getApplication()).
				"', status='".self::escapeString($fbNeedToUpdateAccount->getStatus()).
				"', dateUpdated='".self::escapeString($fbNeedToUpdateAccount->getDateUpdated()).
				"' WHERE id=".$fbNeedToUpdateAccount->getID();

		$conn->execQuery($qry);				
	}

	static function delete($id){
		$conn = self::getConnection();
		$qry = "DELETE FROM tblfbNeedToUpdateAccount WHERE id=".$id;
		$conn->execQuery($qry);
	}
	//END OF CRUD

	static function getFbNeedToUpdateAccountByPk($id=0){
		if($id) {
			$conn = self::getConnection();
			$qry = "SELECT * FROM tblfbNeedToUpdateAccount WHERE id=$id LIMIT 1";
			$rs = new iRecordset($conn->execQuery($qry));

			if($rs->retrieveRecordCount()) {
				$fbNeedToUpdateAccount = new fbNeedToUpdateAccount;
				$fbNeedToUpdateAccount->setID($rs->getID(0));
				$fbNeedToUpdateAccount->setTravelerID($rs->getTravelerID(0));
				$fbNeedToUpdateAccount->setApplication($rs->getApplication(0));
				$fbNeedToUpdateAccount->setStatus($rs->getStatus(0));
				$fbNeedToUpdateAccount->setDateUpdated($rs->getDateUpdated(0));
				return $fbNeedToUpdateAccount;
			}   

		} else return null; 		
	}

	static function getAllNewlyEditedAccounts() {
		$conn = self::getConnection();
		$arrFbNeedToUpdateAccount = array();
		$qry = "SELECT * FROM tblfbNeedToUpdateAccount WHERE status='0' ORDER BY id ASC";
		$rs = new iRecordset($conn->execQuery($qry));

		for($i=0; $i<$rs->retrieveRecordCount(); $i++) {
			$fbNeedToUpdateAccount = new fbNeedToUpdateAccount;
			$fbNeedToUpdateAccount->setID($rs->getID($i));
			$fbNeedToUpdateAccount->setTravelerID($rs->getTravelerID($i));
			$fbNeedToUpdateAccount->setApplication($rs->getApplication($i));
			$fbNeedToUpdateAccount->setStatus($rs->getStatus($i));
			$fbNeedToUpdateAccount->setDateUpdated($rs->getDateUpdated($i));
			$arrFbNeedToUpdateAccount[] = $fbNeedToUpdateAccount;
		}   

		return $arrFbNeedToUpdateAccount;
	}

	static function getFbNeedToUpdateAccountByTravelerIDandApp($travelerID, $app) {
		if($travelerID) {
			$conn = self::getConnection();
			$qry = "SELECT * FROM tblfbNeedToUpdateAccount WHERE travelerID=$travelerID AND application='{$app}'";
			$rs = new iRecordset($conn->execQuery($qry));

			if($rs->retrieveRecordCount()) {
				$fbNeedToUpdateAccount = new fbNeedToUpdateAccount;
				$fbNeedToUpdateAccount->setID($rs->getID(0));
				$fbNeedToUpdateAccount->setTravelerID($rs->getTravelerID(0));
				$fbNeedToUpdateAccount->setApplication($rs->getApplication(0));
				$fbNeedToUpdateAccount->setStatus($rs->getStatus(0));
				$fbNeedToUpdateAccount->setDateUpdated($rs->getDateUpdated(0));
				return $fbNeedToUpdateAccount;
			}   

		} else return null; 		
	}

	static function isAccountExists($travelerID=0, $app) {
		if(!$travelerID) return false;
		$conn = self::getConnection();
		$qry = "SELECT count(*) AS isExists 
				FROM tblfbNeedToUpdateAccount 
				WHERE travelerID=$travelerID AND application='$app'";
		$rs = new iRecordset($conn->execQuery($qry));
		return $rs->getIsExists() ? self::getFbNeedToUpdateAccountByTravelerIDandApp($travelerID,$app) : false;
	}

	static function hasFacebookApps($travelerID,$app) {
		$conn = self::getConnection();		

		$tableName = 'tbUser';
		$fbFieldName = 'facebookID';
		if($app == '1') {
			$tableName = 'tblTravelBioUser';
			$fbFieldName = 'fbUserID';			
		}   		
		$qry = "SELECT count(*) AS isExists FROM $tableName WHERE $fbFieldName != 0 AND travelerID=$travelerID";
		$rs = new iRecordset($conn->execQuery($qry));
		return $rs->getIsExists();
	}


	private static function getConnection(){
		$conn = new dbHandler;
		$conn->setDB('SocialApplication');
		return $conn;
	}

	private static function escapeString($string = ''){
		return mysql_real_escape_string($string);
	}
}
?>