<?php
require_once 'Class.dbHandler.php';
require_once 'Class.iRecordset.php';
require_once('travellog/model/socialApps/mytravelplans/Class.AppTravelDestinations.php');
require_once('travellog/model/socialApps/mytravelplans/Class.AppCountry.php');
require_once 'travellog/model/Class.TravelSchedule.php';
require_once('travellog/model/Class.TravelPlan.php');
require_once('travellog/model/Class.Country.php');

class AppTravelSchedule extends TravelSchedule {
  private $conn;
  public $userCredentials;
  
  public function setUserCredentials($args) {
	$containerFieldName = self::getContainerFieldName($args['container']);
	$containerID = $args['containerID'];
	$conn = new dbHandler();
    $sql = "SELECT username, email, userID, tbUser.travelerID AS travelerID, travelPlansDisplayed, travelPlansIncludePast FROM SocialApplication.tbUser LEFT JOIN Travel_Logs.tblTraveler ON tbUser.travelerID = Travel_Logs.tblTraveler.travelerID WHERE $containerFieldName=$containerID";
	$rs = new iRecordset($conn->execQuery($sql));
	
	if($rs->retrieveRecordCount()){
		$userCredentials = array( 
		    'userID' => $rs->getUserID(),
		    'travelerID' => $rs->getTravelerID(),
		    'email' => $rs->getEmail(),
		    'username' => $rs->getUserName(),
			'containerID' => $containerID,
		    'travelPlansDisplayed' => $rs->getTravelPlansDisplayed(),
		    'travelPlansIncludePast' => $rs->getTravelPlansIncludePast(),
		    'tblColumnName' => $containerFieldName,
		    'message'=> 'ok'
		);		
	}
	else{
        $sql = "INSERT INTO SocialApplication.tbUser ($containerFieldName) VALUES ($containerID)";		
		$conn->execQuery($sql);		
        $userCredentials = array(
          'userID' => $conn->getLastInsertedID(),
          'containerID' => $containerID,
          'tblColumnName' => $containerFieldName,
          'message'=> 'Container ID does not exist. A new record inserted instead.'
        );		
	}	
	 $this->userCredentials = $userCredentials;
  }

  public function getUserCredentials() {
  	return $this->userCredentials;
  }

  public function getContainerFieldName($container){
	$container = explode('.',$container);
	return trim($container[0]).'ID';
  }


    // lifted from the Country class in GAnet
	static function getCountryList()
	{
		$conn = new dbHandler();
		$sql = "SELECT countryID, country FROM GoAbroad_Main.tbcountry WHERE approved=1 AND countryID <> 104 AND countryID NOT IN (14,27,812,77,95,65,219) ORDER BY country ASC";
		$rs = new iRecordset($conn->execQuery($sql));

		for($i=0; $i<$rs->retrieveRecordCount(); $i++){
			$country = new AppCountry();
			$country->setCountryID($rs->getCountryID($i));
			$country->setName($rs->getCountry($i));
 			$countries[] = $country;        	
		}
		return $countries;
	}

  //TODO: some logic need to be factored out of this method
  public static function updateDatesOfTravelItem($travelItemID, $fromDates, $toDates) {
    $fromYear = array();
    $fromMonth = array();
    $toYear = array();
    $toMonth = array();

    $dateCount = count($fromDates);
    for ($i = 0; $i < $dateCount; $i++) {
      list($fy, $fm, $fd) = explode('-', $fromDates[$i]);
      list($ty, $tm, $td) = explode('-', $toDates[$i]);
      $fromYear[]=$fy;
      $fromMonth[]=$fm;
      $toYear[]=$ty;
      $toMonth[]=$tm;
    }

    $fromDate = self::getEarliestDate($fromYear, $fromMonth);
    $toDate = self::getLatestDate($toYear, $toMonth);
  
    $latestFromDate = self::getLatestDate($fromYear, $fromMonth);
    $latestToDate = self::getLatestDate($toYear, $toMonth);

    list($latestFromYear, $latestFromMonth, $tmp) = explode('-', $latestFromDate);
    list($latestToYear, $latestToMonth, $tmp) = explode('-', $latestToDate);

    if (self::countNonZeroDates($fromYear) > 1) {

      $toDate = self::getLatestDate(array($latestFromYear, $latestToYear), array($latestFromMonth, $latestToMonth));

    }

   	$conn = new dbHandler();
    $sql = sprintf("UPDATE SocialApplication.tbTravelWish SET dateFrom='%s' ,dateTo='%s' WHERE travelWishID='%d'"
       ,mysql_real_escape_string($fromDate)
       ,mysql_real_escape_string($toDate)
       ,mysql_real_escape_string($travelItemID)
    );
	$conn->execQuery($sql);
  }

  // Move these to their own class
  private function getEarliestDate($year, $month) {
    $earliestYear = '9999';
    $earliestMonth = '13';
    for ($i=0; $i<count($year); $i++) {
      if ($year[$i] == '0000' || empty($year[$i])) continue;
      if ($year[$i] < $earliestYear) {
        $earliestYear = $year[$i];
        $earliestMonth = $month[$i];
      } elseif ($year[$i] == $earliestYear) {
        if ($month[$i] < $earliestMonth) $earliestMonth = $month[$i];
      }
    }
    if ($earliestYear == '9999') $earliestYear = '0000';
    if ($earliestMonth == '13') $earliestMonth = '00';

    return $earliestYear.'-'.$earliestMonth.'-'.'00';
  }

  private function getLatestDate($year, $month) {
    $latestYear = '0000';
    $latestMonth = '00';
    for ($i=0; $i<count($year); $i++) {
      if ($year[$i] > $latestYear) {
        $latestYear = $year[$i];
        $latestMonth = $month[$i];
      } elseif ($year[$i] == $latestYear) {
        if ($month[$i] > $latestMonth) $latestMonth = $month[$i];
      }
    }

    return $latestYear.'-'.$latestMonth.'-'.'00';
  }

  private function countNonZeroDates($dateArray) {
    $cntNonZeroDates = 0;
    foreach ($dateArray as $d) {
      if ($d != '0000' && $d != '') $cntNonZeroDates++;
    }
    return $cntNonZeroDates;
  }

  /*
   * $friends - comma delimited list of IDs
   */

	public function getFriendsCountryIDs($IDs) {
		$countryIDs = array();
		if(empty($IDs)) return $countryIDs;
		$conn = new dbHandler();
	  	$sql = "SELECT c.countryID AS countryID
				FROM SocialApplication.tbUser AS a
				LEFT JOIN SocialApplication.tbTravelWish AS b ON a.userID = b.userID
				LEFT JOIN SocialApplication.tbTravelWishToCountry AS c ON b.travelWishID = c.travelWishID
				WHERE a.{$this->userCredentials['tblColumnName']} != 0 
					AND a.{$this->userCredentials['tblColumnName']} IN (" . implode(',', $IDs) . ")
					AND c.travelWishID != 0 
					AND (c.startDate >= '".date('Y-m-d')."' OR c.endDate >= '".date('Y-m-d')."')
				GROUP BY c.countryID";
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount()){
			for($i=0; $i<$rs->retrieveRecordCount(); $i++){
				$countryIDs[] = $rs->getCountryID($i);
			}
		}
		return $countryIDs;
	}


  public function getDestinationsWithCountryID($IDs, $ctryID = '') {
    $destinations = array();
	if(empty($IDs)) return $destinations;	
    $dateNow = date('Y-m-d');
	$conn = new dbHandler();
    $sql =  "SELECT a.title AS title
                     ,a.travelWishID AS travelItemID
                     ,a.details AS details
                     ,b.countryID AS countryID
                     ,b.description AS description
                     ,b.startDate AS startDate
                     ,b.endDate AS endDate
                     ,c.{$this->userCredentials['tblColumnName']} AS containerID
                     ,d.country AS country
               FROM SocialApplication.tbTravelWish AS a
               LEFT JOIN SocialApplication.tbTravelWishToCountry AS b ON a.travelWishID = b.travelWishID
               LEFT JOIN SocialApplication.tbUser AS c ON a.userID = c.userID
               LEFT JOIN GoAbroad_Main.tbcountry AS d ON b.countryID = d.countryID
               WHERE b.countryID=$ctryID AND
					 ( 
						( b.endDate >= '{$dateNow}' OR b.startDate >= '{$dateNow}' ) 
						OR 
						( b.startDate = '0000-00-00') 
					 ) 
					 AND c.{$this->userCredentials['tblColumnName']} IN (" . implode(",", $IDs) . ") 
			  ORDER BY b.startDate ASC";


	$rs = new iRecordset($conn->execQuery($sql));

	for($i=0; $i<$rs->retrieveRecordCount(); $i++){
	    $d = new AppTravelDestinations();
	    $d->setTravelWishID($rs->getTravelItemID($i));
	    $d->setTitle($rs->getTitle($i));
	    $d->setDetails($rs->getDetails($i));
	    $d->setDescription($rs->getDescription($i));
	    $d->setCountry( new Country($rs->getCountryID($i)) );
	    $d->setStartDate($rs->getStartDate($i));
	    $d->setEndDate($rs->getEndDate($i));
	    $d->setContainerID($rs->getContainerID($i));
	    $destinations[] = $d;		
	}
    return $destinations;
  }
}

?>