<?php
class AppCountry {
  private $countryID = 0;
  private $name = '';
  private $locationID = 0;
  private $latitude, $longitude;

  function getCountryID() {return $this->countryID;}
  function setCountryID($countryID) {$this->countryID = $countryID;}

  function getName() {return $this->name;}
  function setName($name) {$this->name = $name;}

  function getLocationID() {return $this->locationID;}
  function setLocationID($locationID) {$this->locationID = $locationID;}

  function getLatitude() {return $this->latitude;}
  function setLatitude($latitude) {$this->latitude = $latitude;}

  function getLongitude() {return $this->longitude;}
  function setLongitude($longitude) {$this->longitude = $longitude;}
}
?>
