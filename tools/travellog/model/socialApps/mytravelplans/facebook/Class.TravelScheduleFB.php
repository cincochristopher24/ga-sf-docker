<?php 
require_once 'travellog/model/socialApps/mytravelplans/Class.AppTravelSchedule.php';

class TravelScheduleFB extends AppTravelSchedule {

	function __construct($params) {    	

		$this->setUserCredentials($params);
	}

	static function getUserCredentialsByTravelerID($travelerID){
		require_once('Class.dbHandler.php');
		require_once('Class.iRecordset.php');
		$userCredentials = array();
		$conn = new dbHandler();
	    $sql = "SELECT username, email, userID, tbUser.facebookID AS facebookID, travelPlansDisplayed, travelPlansIncludePast 
				FROM SocialApplication.tbUser 
				LEFT JOIN Travel_Logs.tblTraveler ON tbUser.travelerID = Travel_Logs.tblTraveler.travelerID 
				WHERE Travel_Logs.tblTraveler.travelerID=$travelerID";
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount()){
			$userCredentials = array( 
			    'email' => $rs->getEmail(),
			    'userID' => $rs->getUserID(),
			    'travelerID' => $travelerID,
			    'username' => $rs->getUserName(),
				'facebookID' => $rs->getFacebookID(),
			    'travelPlansDisplayed' => $rs->getTravelPlansDisplayed(),
			    'travelPlansIncludePast' => $rs->getTravelPlansIncludePast(),
			    'tblColumnName' => 'facebookID',
			    'message'=> 'ok'
			);
		}
		
		return $userCredentials;
	}

	static function getMTPUsersCredentials($userID = 0, $limit = 20){
		require_once('Class.dbHandler.php');
		require_once('Class.iRecordset.php');
		if(!$userID) $userID = 0;
		$userCredentials = array();
		$conn = new dbHandler;
		$sql = "SELECT a.userID, a.travelerID, a.facebookID, a.travelPlansDisplayed, a.travelPlansIncludePast, 	
					   c.username, c.email
				FROM SocialApplication.tbUser AS a
					LEFT JOIN SocialApplication.tbTravelWish AS b ON a.userID = b.userID
					LEFT JOIN Travel_Logs.tblTraveler AS c ON a.travelerID = c.travelerID
				WHERE a.travelerID != 0 
					AND a.facebookID != 0 
					AND a.userID > $userID
					AND b.userID > $userID
				GROUP BY b.userID ORDER BY a.userID ASC LIMIT $limit";

		$rs = new iRecordset($conn->execQuery($sql));

		for($i=0; $i<$rs->retrieveRecordCount(); $i++) {
			$userCredentials[] = array( 
			    'email' => $rs->getEmail($i),
			    'userID' => $rs->getUserID($i),
			    'travelerID' => $rs->getTravelerID($i),
			    'username' => $rs->getUserName($i),
				'facebookID' => $rs->getFacebookID($i),
			    'travelPlansDisplayed' => $rs->getTravelPlansDisplayed($i),
			    'travelPlansIncludePast' => $rs->getTravelPlansIncludePast($i),
			    'tblColumnName' => 'facebookID',
			    'message'=> 'ok'
			);
		}
		return $userCredentials;				
	}

	public static function isMTPValidUserID($userID = 0) {
		require_once('Class.dbHandler.php');
		require_once('Class.iRecordset.php');

		$conn = new dbHandler;
		$conn->setDB('SocialApplication');
  		$sql = "SELECT count(userID) AS isValid FROM tbTravelWish WHERE userID = $userID LIMIT 1";
				
		$rs = new iRecordset($conn->execQuery($sql));
		
		return $rs->getIsValid() ? $userID : false;
   }
}
?>