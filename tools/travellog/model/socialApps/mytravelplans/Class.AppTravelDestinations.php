<?php
require_once('travellog/model/Class.TravelDestination.php');

class AppTravelDestinations extends TravelDestination {
  private $title;
  private $details;
  private $containerID;

  public function getTitle() {
    return $this->title;
  }
  public function setTitle($title) {
    $this->title = $title;
  }

  public function getDetails() {
    return $this->details;
  }
  public function setDetails($details) {
    $this->details = $details;
  }

  public function getContainerID() {
  	return $this->containerID;
  }
  public function setContainerID($containerID) {
  	$this->containerID = $containerID;
  }
}

?>