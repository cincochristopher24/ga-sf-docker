<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: 
 */
 
    require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	
    class Activity{
        
        /**
         * Define variables for Class Member Attributes
         */                  
            private $mConn      = NULL;
            private $mRs        = NULL;  
            
            private $mProgramID    = NULL;
            
            private $mActivityID     = NULL;
            private $mTitle          = NULL;
            private $mDate           = NULL;
            private $mDescription    = NULL;
            private $mVenue          = NULL;
            private $mDisplayPublic  = NULL;
           
              
        /**
         * Constructor Function for this class
         */
            function Activity($activityID = 0){
                
                $this->mActivityID = $activityID;
                
                try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mActivityID){
                    
                    $sql = "SELECT * FROM tblActivity WHERE activityID = '$this->mActivityID' ";
                    $this->mRs->Execute($sql);
                
                    if (0 == $this->mRs->Recordcount()){
                        throw new Exception("Invalid ActivityID");        // ID not valid so throw exception
                    }
                    
                    // Sets values to its class member attributes.   
                    $this->mTitle        = stripslashes($this->mRs->Result(0,"title"));           
                    $this->mDescription  = stripslashes($this->mRs->Result(0,"description"));       
                    $this->mDate         = $this->mRs->Result(0,"adate");         
                    $this->mVenue        = stripslashes($this->mRs->Result(0,"venue"));
                    $this->mDisplayPublic  = $this->mRs->Result(0,"displaypublic");
                    
                    // Sets a value to its parent key.
                    $this->mProgramID    = $this->mRs->Result(0,"programID");                            
                }
                
            }
           
       /**
         * Setter Functions
         */       
            function setActivityID($activityID){
               $this->mActivityID = $activityID;
            }
            
            function setTitle($title){
               $this->mTitle = $title;
            }
           
            function setTheDate($date){
               $this->mDate = $date;
            }
           
            function setDescription($description){
               $this->mDescription = $description;
            }
           
            function setVenue($venue){
               $this->mVenue = $venue;
            }
            
            function setDisplayPublic($displaypublic){
               $this->mDisplayPublic = $displaypublic;
            }
           
       /**
         * Getter Functions
         */             
            function getActivityID(){
                return $this->mActivityID;
            }
            
            function getTitle(){
                return $this->mTitle;
            }
           
            function getTheDate(){
                return $this->mDate;
            }
           
            function getDescription(){
                return $this->mDescription;
            }
           
            function getVenue(){
                return $this->mVenue;
            }
            
            function getDisplayPublic(){
                return $this->mDisplayPublic;
            }
               
        
        /**
         * Setter Function for the Parent Key
         */
            function setProgramID($programID){
                $this->mProgramID = $programID;
            }     
            
     
        /**
         * Getter Function for the Parent Key
         */
            function getProgramID(){
                return $this->mProgramID;
            }
        
       
        /**
         * CRUD Methods (Create, Update , Delete)
         */
            function Create(){
               
                $Atitle       = addslashes($this->mTitle);
                $Adescription = addslashes($this->mDescription);
                $Avenue       = addslashes($this->mVenue);
                
                $sql = "INSERT into tblActivity (title, description, adate, venue, programID, displaypublic) " .
                        "VALUES ('$Atitle', '$Adescription', '$this->mDate', '$Avenue', '$this->mProgramID', '$this->mDisplayPublic')";
                $this->mRs->Execute($sql);       
                $this->mActivityID = $this->mRs->GetCurrentID();   
            }
            
            function Update(){
                
                $Atitle       = addslashes($this->mTitle);
                $Adescription = addslashes($this->mDescription);
                $Avenue       = addslashes($this->mVenue);
                
                $sql = "UPDATE tblActivity SET title = '$Atitle', description = '$Adescription', adate = '$this->mDate', " .
                		"venue = '$Avenue', displaypublic = '$this->mDisplayPublic' " .
                        "WHERE activityID = '$this->mActivityID' ";
                $this->mRs->Execute($sql);
            }
            
            function Delete(){
               
                $sql = "DELETE FROM tblActivity WHERE activityID = '$this->mActivityID' ";     
                $this->mRs->Execute($sql);
            }       
    }
    
?>
