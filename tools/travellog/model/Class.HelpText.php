<?php
/*
 * Created on 11 27, 07
 * @author Kerwin Gordo
 * purpose: New HelpText class for GANet and cobranded sites	
 */
 
require_once ('Class.Connection.php');
require_once ('Class.Recordset.php');
require_once ('travellog/model/Class.SiteContext.php'); 

 class HelpText { 	
 	private $con;						// connection class
 	private $siteContext;
 	private $conf;
 	
 	function HelpText(){
 		$this->con = new Connection();
 		//$this->conf = new Con_fig();
		$this->conf = SiteContext::getInstance();
 		$this->siteContext['siteName'] = $this->conf->getSiteName();
 	}
 	 	
 	/**
 	 * @param $key is a search key for the help information
 	 * @return returns string if associated help text is found and null if the key is not found
 	 * mechanism: retrieves one or more helptexts on ganet and cobranded sites and takes the appropriate one based on siteContext
 	 */
 	function getHelpText($key){ 	
		$query = "select text,siteName from tblHelpText as hm,tblHelpTextDetail as hd where hm.hkey='$key' and hm.hkey = hd.hkey ";
		$rs = new Recordset($this->con);
		$rs->Execute($query);
		$rAr = array();
		
		
		
		if ($rs->Recordcount() > 0) {
			
			// put recordset in array			
			while ($rcdAr = mysql_fetch_array($rs->Resultset())){
				$rAr[$rcdAr['siteName']] = $rcdAr;
			}			
			
			//echo '<pre>';print_r($rAr); print_r($this->siteContext);echo $this->siteContext['sitename']; exit;
						
			// check first if siteContext siteName is in array
			if (array_key_exists($this->siteContext['siteName'],$rAr))
				return stripslashes($rAr[$this->conf->getSiteName()]['text']);
				
			// if not found check CoBrand siteName
			if (array_key_exists('COBRAND',$rAr)){
				// find %CoBrandSite% word and replace with siteName
				$ht = str_replace('%CoBrandSite%',$this->siteContext['siteName'],$rAr['COBRAND']['text']);								
				return stripslashes($ht); 				
			}
							
			// if not found check GaNet siteName
			if (array_key_exists('GANET',$rAr)){
				return stripslashes($rAr['GANET']['text']);
			}		
				
			

			// put helptext in $helpList with the corresponding $key
			//$helpList[$key] = $rs->Result(0,'helptext');
			//return stripslashes($rs->Result(0,'helptext'));
		} 			 			
 		return null;	 		 		
 	}
 	
 	/**
 	 *  this function will insert a help text in the database with the corresponding key.
 	 * 		If key is existing it will return false and will not overwrite the value.
 	 *      We will also replace all GoAbroad GoAbroad.net or GoAbroad Network with %CoBrandSite% if $siteName is COBRAND
 	 */
 	function setHelpText($key,$text,$siteName = 'GANET') {
 		$query = "select text from tblHelpText as hm,tblHelpTextDetail as hd where hkey='$key' and hm.hkey = hd.hkey";
		$rs = new Recordset($this->con);
		$rs->Execute($query);
		
		$rAr = array();
		
		if ($rs->Recordcount() > 0) {
			// put the result in an array
			while ($rcdAr = mysql_fetch_array($rs->Resultset())){
				$rAr[$rcdAr['siteName']] = $rcdAr;
			}	
			
			if (!array_key_exists($siteName,$rAr)){		
				$htext = stripslashes($text);
				if ($siteName == 'COBRAND'){					
					$htext = $this->replaceGAText($htext);
				}
				$query = "insert into tblHelpTextDetail (hkey,text,siteName) values('$key','$htext','$siteName')";
				$rs = new Recordset($this->con);
				$rs->Execute($query);
			}	
			else 
				return false;
				
		} else {
			$htext = stripslashes($text);
			if ($siteName == 'COBRAND'){					
				$htext = $this->replaceGAText($htext);
			}
			$query = "insert into tblHelpText (hkey,helptext) values ('$key','$htext')";
			$rs->Execute($query);
			
			$query = "insert into tblHelpTextDetail (hkey,text,siteName) values ('$key','$htext','$siteName')";
			$rs->Execute($query);
			return true;	
		}
 	}
 	
 	
 	/**
 	 * this function will update an existing help text
 	 */
 	 function updateHelpText($key,$text) {
 	 			
		$htext = stripslashes($text);
		$query = "update tblHelpText set helptext = '$htext' where hkey='$key'";
		$rs = new Recordset($this->con);
		$rs->Execute($query);					
 	 }
 	 
 	 /**
 	  * @return array array of keys
 	  */
 	 function getKeys() {
 	 	$keys = array();
 	 	$query = "select hkey from tblHelpText order by hkey";
 	 	$rs = new Recordset($this->con);
		$rs->Execute($query);			
		
		if ($rs->Recordcount() > 0){
			while ($rsar = mysql_fetch_array($rs->Resultset())){
				$keys[] = $rsar['hkey'];				
			}			
		}
				
		return $keys;
 	 } 
 	 
 	 
 	 function replaceGAText($text){
 	 	$patterns = array('/(G|g)o(A|a)broad/','/(G|g)o(A|a)broad.(N|n)et/');	// text to be replaced w/ %CoBrandSite% 
 	 	$repls = array('%CoBrandSite%','%CoBrandSite%');
 	 	
 	 	return preg_replace($patterns,$repls,$text);
 	 }
 }
 
?>
