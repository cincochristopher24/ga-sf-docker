<?php
	/***
		Author: Reynaldo Castellano III
		Date: December 12, 2008
	***/
	
	class GMapApiKeyHolder
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)) self::$instance = new self;
			return self::$instance;
		}
		
		private $mKeys = array(
			'goabroad.net'			=> 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RTSLFXcXnfoN54L8eFKyvlGyILP5BTuHfFTyvfjz-Q6KMmGVyvV-0wWyQ',
			'goabroad.net.local' 	=> 'ABQIAAAAxssX44ErD-uqyd-GW74yDBS_IWFy692u9pZ7fVeFegkeYwDfHRQ0DmyB5o0hSF4zV4IdIwGBmzCXEw',
			'goabroad.com'			=> 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RRegAslM_uNvO33cfj6E9R27UkOThQ7nB5AV05mEZ6s2_P6zhTIRyxqzQ',
			'goabroad.com.local'	=> 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RR6DBl6YZLbvv1S2vKvuijxWdBh_BQdDmJvpxte_FboX2r4BFER96Vdrw'
		);
		
		private function __construct(){}
		public function getKey($domain=null){
			$revDomain = strrev($domain);
			foreach($this->mKeys as $iDomain => $apiKey){
				$revDomainKey = strrev($iDomain);
				if(substr($revDomain,0,strlen($revDomainKey)) == $revDomainKey){
					return $apiKey;
				}
			}
			return null;
		}
	}
?>