<?php
	/**
	 * @(#) Class.CommentContext.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 11, 2009
	 */
	
	/**
	 * Comment context inteface class.
	 */
	interface CommentContext {
		
		/**
		 * Returns true if the given traveler ID has action(add, edit or delete) privilege
		 * to the context.
		 * 
		 * @param integer $traveler_id The given traveler ID
		 * 
		 * @return boolean true if the given travelerID has action(add, edit or delete) privilege.
		 */
		function hasActionPrivilege($traveler_id);
		
		/**
		 * Returns the comments of the context.
		 * 
		 * @return Comment[] the comments of the context.
		 */
		function getComments();
		
		/**
		 * Removes the given comment from the context.
		 * 
		 * @param Comment $comment the given comment
		 * 
		 * @return void
		 */
		function removeComment($comment);
		
		/**
		 * Adds the given comment to the context.
		 * 
		 * @param Comment $comment the given comment
		 * 
		 * @return void
		 */
		function addComment($comment);
		
	}
	
