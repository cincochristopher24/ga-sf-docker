<?php
	/**
	 * Created on September 7, 2006
	 * Class.AlertMessage.php extends Class.Message.php
	 * @author Cheryl Ivy Q. Go
	 * 	
	 */

	require_once 'travellog/model/Class.Message.php';
	require_once 'travellog/model/Class.MessageSpace.php';
	require_once 'travellog/model/Class.DiscriminatorType.php';
	
	class AlertMessage extends Message{
		
		/**
		 * Constructor of class AlertMessage
		 * @param integer $_attributeID
		 * @return Message
		 */
		
		function AlertMessage($_attributeId = 0, $_sendableId = 0){

			$this->setSendableId($_sendableId);
			$this->setDiscriminator(DiscriminatorType::$ALERT);

			return $this->Message($_attributeId);
		}		

	 	/**
	 	 * Function name: Send
	 	 */

	 	function Send(){
			$msgSpace = new MessageSpace();
			$msgSpace->Store($this);
	 	}	 	

		/***************************** START: SETTERS *************************************/
		function setExpiry($_expiry){			
			$this->mExpiry = $_expiry;
		}		 		 
		function setSource($_src = NULL){
			$this->mSource = $_src;
		}
		function setSendableId($_sendableId = 0){
			$this->mSendableID = $_sendableId;
		}
	 	function setDestination($_group = array()){	 		
	 		$this->mDestination = $_group;
	 	}
		function setAlertRecipient(){
			$sql = "SELECT DISTINCT tblGroup.sendableID, tblGroup.discriminator, tblGroup.groupID, tblGroup.name " .
					"FROM tblMessageToAttribute, tblGroup " .
					"WHERE tblGroup.sendableID = tblMessageToAttribute.recipientID " .
					"AND tblMessageToAttribute.messageID = " . $this->getMessageId();
			$this->mRs->Execute($sql);

			if (0 < $this->mRs->Recordcount()){
				$mDiscriminator = $this->mRs->Result(0, "discriminator");

				switch($mDiscriminator){
					case GROUP::FUN :
						$groupData = array();
						$groupData["name"] = $this->mRs->Result(0, "name");
						$groupData["groupID"] = $this->mRs->Result(0, "groupID");
						$groupData["sendableID"] = $this->mRs->Result(0, "sendableID");
						$groupData["discriminator"] = $this->mRs->Result(0, "discriminator");

						$mRecipient = new FunGroup();
						$mRecipient->setFunGroupData($groupData);

		 				break;

		 			case GROUP::ADMIN :
						$groupData = array();
						$groupData["name"] = $this->mRs->Result(0, "name");
						$groupData["groupID"] = $this->mRs->Result(0, "groupID");
						$groupData["sendableID"] = $this->mRs->Result(0, "sendableID");
						$groupData["discriminator"] = $this->mRs->Result(0, "discriminator");

						$mRecipient = new AdminGroup();
						$mRecipient->setAdminGroupData($groupData);

		 				break;
				}
			}
			else{
				// if destination group no longer exists, set sender group as the recipient group
				if ($this->mSource instanceof GROUP)
					$mRecipient = $this->mSource;
			}
			$this->mDestination = array($mRecipient);
		}
		/******************************* END: SETTERS *************************************/

		public static function DeleteAllGroupAM($_sendableId = 0){
			// check if sendableID is of group
			$mFactory = MessageSendableFactory::Instance();
			$mSendable = $mFactory->Create($_sendableId);

			if ($mSendable instanceof Group){
				// If it's an Admin Group, check if it's a SG. For now, only subgroups can be deleted, therefore only PMs of SG will be removed
				// Clubs however can also be deleted, including all of its PMs

				if (($mSendable->getDiscriminator() == Group::ADMIN && $mSendable->isSubGroup()) || $mSendable->getDiscriminator() == Group::FUN){
					try{
						$mConn = new Connection();
						$mRs	= new Recordset($mConn);
						$mRs2	= new Recordset($mConn);
						$mRs3	= new Recordset($mConn);
					}
					catch(exception $e){
						throw $e;
					}
					$arrMessageId = array();

					// get messages where sender is the group to be deleted
					$sql = "SELECT DISTINCT messageID FROM tblMessages WHERE discriminator = 1 AND senderID = " . $_sendableId;
					$mRs->Execute($sql);

					if (0 < $mRs->Recordcount()){
						while($Message = mysql_fetch_assoc($mRs->Resultset()))
							$arrMessageId[] = $Message['messageID'];
					}

					// get messages where recipient is the group to be deleted
					$sql2 = "SELECT DISTINCT tblMessages.messageID AS messageID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 1 " .
							"AND tblMessageToAttribute.recipientID = " . $_sendableId;
					$mRs2->Execute($sql2);

					if (0 < $mRs2->Recordcount()){
						while($Message = mysql_fetch_assoc($mRs2->Resultset()))
							$arrMessageId[] = $Message['messageID'];
					}

					if (0 < count($arrMessageId)){
						$lstMessageId = implode(",", $arrMessageId);

						// delete messages
						$sql = "DELETE FROM tblMessages WHERE messageID IN (" . $lstMessageId . ")";
						$mRs->Execute($sql);

						$sql2 = "DELETE FROM tblMessageToAttribute WHERE messageID IN (" . $lstMessageId . ")";
						$mRs2->Execute($sql2);

						$sql3 = "DELETE FROM tblMessageToAlert WHERE messageID IN (" . $lstMessageId . ")";
						$mRs3->Execute($sql3);
					}
				}
			}
		}
	}	
?>