<?php

require_once("Class.Connection.php");
require_once("Class.Recordset.php");
require_once("travellog/custom/COBRAND/model/Class.TravelerCB.php");



class HomeStay{
	
	private $mConn     		 = null;
    private $mRs       		 = null;

	//define attributes here
	private $mAttribute = 'attribute';
	private $mHomeStayID=   0;
	private $mName		=	'';
	private $mAddress	=	'';
	private $mEmail 	=	'';
	private $mPhone		= 	'';
	private $mFamilyNames = '';
	private $mDescription = '';
	
	private $mAssignedTraveler = null;
	
	private static $conn = null;
	private static $rs = null;
	
	public function __construct($_homeStayID = 0){
		
		 try {
                $this->mConn = new Connection();
                $this->mRs   = new Recordset($this->mConn);
            }
            catch (Exception $e) {                 
               throw $e;
            }
			//$this->mHomeStayID = $_homeStayID;
			
			if (0 < $_homeStayID):
				$sql  = "SELECT * FROM tblHomeStay WHERE homestayID = " . $_homeStayID;
				$this->mRs->Execute($sql);
				
				if ($this->mRs->Recordcount()):
					$this->mHomeStayID = $_homeStayID;
					$this->mName = $this->mRs->Result(0,'name');
					$this->mAddress = $this->mRs->Result(0,'address');
					$this->mEmail = $this->mRs->Result(0,'email');
					$this->mPhone = $this->mRs->Result(0,'phone');
					$this->mFamilyNames = $this->mRs->Result(0,'familynames');
					$this->mDescription = $this->mRs->Result(0,'description');					
				endif;
			endif;	
			
	}
	
	public function __get($_attr){
		
		if (isset($this->$_attr))	
			return $this->$_attr;
		else
			throw new Exception('Undefined attribute '.$_attr);
		
	}
	
	public function __set($_attr, $_value){
		if (property_exists($this, $_attr))
			$this->$_attr = $_value;
	}
	
	
	//setters
	
	public function setName($_name){
		$this->mName = $_name;
	}
	
	public function setAddress($_address){
		$this->mAddress = $_address;
	}
	
	public function setEmail($_email){
		$this->mEmail = $_email;
	}
	
	public function setPhone($_phone){
		$this->mPhone = $_phone;
	}
	
	public function setFamilyNames($_famNames){
		$this->mFamilyNames = $_famNames;
	}
	
	public function setDescription($_desc){
		$this->mDescription = $_desc;
	}
	
	//getters
	public function getHomeStayID(){
		return $this->mHomeStayID;
	}
		
	public function getName(){
		return $this->mName;
	}
	
	public function getAddress(){
		return $this->mAddress;
	}
	
	public function getEmail(){
		return $this->mEmail;
	}
	
	public function getPhone(){
		return $this->mPhone;
	}
	
	public function getFamilyNames(){
		return $this->mFamilyNames;
	}
	
	public function getDescription(){
		return $this->mDescription;
	}
	
	public function Save(){
		
		if (0 < $this->mHomeStayID):
			$sql = "UPDATE tblHomeStay SET name = ' " . addslashes($this->mName) . "' , address = '" . addslashes($this->mAddress) . "' , email =  '" . addslashes($this->mEmail) . "' , phone = '" . addslashes($this->mPhone) . "' , familynames = '". addslashes($this->mFamilyNames). "' , description = '" .addslashes($this->mDescription). "' WHERE homestayID = " . $this->mHomeStayID ;
			$this->mRs->Execute($sql);
		else:
			$sql = "INSERT INTO tblHomeStay (name, address, email, phone, familynames, description) " . 
					"VALUES ('".addslashes($this->mName)."', '".addslashes($this->mAddress)."' , '".addslashes($this->mEmail)."' , '".addslashes($this->mPhone)."' , '".addslashes($this->mFamilyNames)."' , '".addslashes($this->mDescription)."')";
			$this->mRs->Execute($sql);
			$this->mHomeStayID = $this->mRs->GetCurrentID();
		endif;
		
	}
	
	public function Delete(){
		
		if (0 < $this->mHomeStayID):
			$sql = "DELETE FROM tblHomeStay WHERE homestayID = " . $this->mHomeStayID ;
			$this->mRs->Execute($sql);
		endif;
	}
	
	public function setAssignedTraveler($_travelerID){
	
		if (0 < $_travelerID):
			if (null==$this->getAssignedTraveler())
				$sql = "INSERT INTO tblTravelertoHomeStay (travelerID, homestayID) VALUES ( " . $_travelerID. " , " . $this->mHomeStayID .") " ;
			else
				$sql = "UPDATE tblTravelertoHomeStay SET travelerID = " . $_travelerID . " WHERE homestayID = " . $this->mHomeStayID ;
		
			$this->mRs->Execute($sql);
		
			$this->mAssignedTraveler = 	new TravelerCB ($_travelerID);
		else:
			$this->removedAssignedTraveler();
		endif;

	}
	
	public function removedAssignedTraveler($_travelerID = 0){
		
		$addsqlstr = '';
		if (0 < $_travelerID) 
			$addsqlstr = " AND travelerID = " . $_travelerID;

		$sql = "DELETE FROM tblTravelertoHomeStay WHERE homestayID = " . $this->mHomeStayID . $addsqlstr ;
		$this->mRs->Execute($sql);
		
	}

	public function getAssignedTraveler(){
		
		if (null==$this->mAssignedTraveler):
		
			$sql = "SELECT travelerID FROM tblTravelertoHomeStay WHERE homestayID = " . $this->mHomeStayID ;
			$this->mRs->Execute($sql);
		
			if($this->mRs->Recordcount()) :
				$this->mAssignedTraveler = new TravelerCB ($this->mRs->Result(0,'travelerID'));
			endif;
		
		endif;
		
		return $this->mAssignedTraveler;
		
	}

	public static function initConn(){
		
		if (self::$conn == null):
			self::$conn = new Connection();
			self::$rs = new Recordset(self::$conn);
		endif;
		
	}
	public static function getAllHomeStays(){
		
		$sql = "SELECT homestayID FROM tblHomeStay";
		self::$rs->Execute($sql);
		
		$arrHomeStays = array();
		while ($recordset = mysql_fetch_array(self::$rs->Resultset())){
			$arrHomeStays[] = new HomeStay($recordset['homestayID']);
		}
		
		return $arrHomeStays;
		
		
	}
	public static function getTravelersByHomeStayID($_homestayID){
		
		$sql = "SELECT travelerID FROM tblTravelertoHomeStay WHERE homestayID = " . $_homestayID;
		self::$rs->Execute($sql);
		
		$arrTravelers = array();
		while ($recordset = mysql_fetch_array(self::$rs->Resultset())){
			$arrTravelers[] = new TravelerCB ($recordset['travelerID']);
		}
		
		return $arrTravelers;
		
	}
	
	public static function getAllUnassignedTravelers(){
		
		$CONFIG = $GLOBALS['CONFIG'];
		$gID = $CONFIG->getGroupID();
		
		$sql = "select SQL_CALC_FOUND_ROWS distinct t.travelerID from tblTraveler as t,tblGrouptoTraveler as gt,tblGroup as g WHERE 1 = 1 and active > 0 and gt.travelerID = t.travelerID and gt.groupID = g.groupID and (g.groupID='$gID' or g.parentID='$gID') AND t.travelerID not in (select distinct travelerID from tblGrouptoAdvisor) AND t.travelerID not in (select distinct travelerID from tblTravelertoHomeStay)";
		
		self::$rs->Execute($sql);

		$arrTravelers = array();
		while ($recordset = mysql_fetch_array(self::$rs->Resultset())){
			$arrTravelers[] = new TravelerCB ($recordset['travelerID']);
		}

		return $arrTravelers;		
	}
	
}

?>