<?php
	/**
	 * Created on Jul 27, 2006
	 *	@author Kerwin Gordo
	 *	Purpose: A subclass of Location and represents a City
	 * 
	 * Last Edited By	: Cheryl Ivy Q. Go
	 * Purpose			: Class City has been modified to adjust with the new requirements of GA.net.
	 * 						Users can add new Cities to the database which will be verified by the day crew.
	 */
	
	require_once 'Class.Location.php';
	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'Class.LocationType.php';
	require_once 'Class.ConnectionProvider.php';
	
	class City extends Location {
	 	private $rs;
	 	private $rs2;
	 	private $conn;

	 	private $cityID;
	 	private $regionID;
	 	private $countryID;
	 	private $continentID;
	 	
	  	private $region;
	  	private $country;
	  	private $continent;  	
	  	private $approved = 1;

	  	// Constructor
	  	function City($cityID = 0){
	
			try{
				$this-> conn = ConnectionProvider::instance()->getConnection();
				$this-> rs   = new Recordset(ConnectionProvider::instance()->getConnection());
				$this-> rs2  = new Recordset(ConnectionProvider::instance()->getConnection());
			}
			catch(exception $e){
				throw $e;
			}

			
	  		$this->cityID = $cityID;
	  		
	  		if ($cityID > 0){
	  			
	  			$sql = "select * from GoAbroad_Main.tbcity where approved = 1 AND cityID = " . $cityID;
	  			$this-> rs-> Execute($sql);

	 			if (0 == $this->rs->Recordcount()) 
	  			 	throw new InstantiationException("city id " . $cityID . " is not a valid ID");
	  			
	  			$this-> setLocationID($this->rs->Result(0, "locID"));
	  			$this-> setName(stripslashes($this->rs->Result(0, "city")));
	  			$this-> continentID = $this->rs->Result(0, "continentID");
	  			$this-> countryID = $this->rs->Result(0, "countryID");
	  			$this-> approved = $this->rs->Result(0, "approved");  			  			 	
	  		}
	  		return;
		}

		// Setters
		function setRegionID($regionID = 0) {
	 		$this->regionID = $regionID;
	 	}
	 	
	 	function setCountryID($countryID = 0) {
	 		$this->countryID = $countryID;
	 	}
	 	
	 	function setContinentID($continentID = 0) {
			$this->continentID = $continentID;
		}
		
	 	function setApprove($_approved = 0){
	 		$this->approved = $_approved;
	 	}

		function setCityID($_cityID = 0){
			$this->cityID = $_cityID;
		}
	 	
	 	// Getters
	 	function getCityID() {
	 		return $this->cityID;
	 	}	 	
	 	
	 	function getCountryID() {
	 		return $this->countryID;
	 	}	
	 	
	 	function getApprove(){
	 		return $this-> approved;
	 	}
	 	
	 	// overrides
	 	
 	  	function getCountries() {return null;}	
 		function getCities() { return null; } 			 			
 		function getRegions() { return null; } 		
 		function getNonPopLocations() {return null; }

 		function getRegion() {
 			if ($this->region == null) {
 				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
 				$sql = "select * from tblRegion where regionID = " . $this->regionID;
 				$this-> rs-> Execute($sql);
 				
 				if (0 == $this->rs->Recordcount())
 					return null;
 					 				
 				$this-> region = new Region();
 				$this-> region-> setLocationID($this->rs->Result(0, "locID"));
 				$this-> region-> setName($this->rs->Result(0, "name"));
 				$this-> region-> setRegionID($this->rs->Result(0, "regionID"));
 				$this-> region-> setCountryID($this->rs->Result(0, "countryID"));
 				$this-> region-> setContinentID($this->rs->Result(0, "continentID")); 				 				
 			}

 			return $this->region; 			
 		}
 		function getCountry() {
 			if ($this->country == null) {
 				require_once("travellog/model/Class.CountryPeer.php");	
 				$this->country = CountryPeer::retrieveByPk($this->countryID);				
 			}

 			return $this->country;
 			/*if ($this->country == null) {
 				$sql = "select * from GoAbroad_Main.tbcountry where countryID = " . $this->countryID . " and approved = 1";
 				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
 				$this-> rs-> Execute($sql);
 				
 				$this->country = new Country();
 				$this->country-> setLocationID($this->rs->Result(0, "locID"));
 				$this->country-> setContinentID($this->rs->Result(0, "continentID"));
 				$this->country-> setCountryID($this->rs->Result(0, "countryID"));
 				$this->country-> setName($this->rs->Result(0, "country")); 						
 			}

 			return $this->country;*/	
 		}

 		function getContinent() {
 			if ($this->continent == null) {
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
 				$sql = "select * from tblContinent where continentID = " . $this->continentID;
				$this-> rs-> Execute($sql);
	 			
	 			$continent = new Continent();
	 			$continent-> setLocationID($this->rs->Result(0, "locID"));
	 			$continent-> setName($this->rs->Result(0, "name"));
	 			$continent-> setContinentID($this->rs->Result(0, "continentID"));
 			}

 			return $continent; 			
 		}	

 		// CRUD + list
 		public static function getCityList(){
 			try{
 				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
 			}
 			catch(exception $e){
 				throw $e;
 			}

	  		$travelerID = isset($_SESSION['travelerID']) ? $_SESSION['travelerID'] : 0;

			$sql = "(select * from GoAbroad_Main.tbcity where approved = 1 " .
					"UNION " .
					"select a.* from tblNewCity as a, tblTravelerToCity as b where b.cityID = a.cityID and b.travelerID = " . $travelerID .
					") order by city";
 			$rs-> Execute($sql);
			
	 		$arrCity = array();
			
			if (0 < $rs->Recordcount()){
		 		while ($cities = mysql_fetch_array($rs->Resultset()))
		 			$locationID = LocationFactory::instance();
		 			$arrCity[] = $location-> create($cities['locID']);
			}

 			return $arrCity; 			
 		}
 		
 		function save(){
 			if ($this->cityID > 0 || $this->getLocationID() > 0 ) {
 				throw new Exception("Cannot save!, cityID and locId should not have been set");
 			}
 			
 			if (count($this->getName()) == 0) {
 				throw new Exception("Cannot save!, city should have a name");
 			}
 			
 			if ($this->countryID == 0) {
 				throw new Exception("Cannot save!, no countryId supplied");
 			} 			
 			
 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
 			$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
 			
 			// insert a row first in tblLocation
 			$sql = "insert into tblLocation (locationtypeID) values('" . LocationType::$CITY . "')";
 			$this-> rs-> Execute($sql);
 			
 			$this-> setLocationID(mysql_insert_id());
 			 			
 			// insert a row in tbcity
 			$sql2 = "insert into GoAbroad_Main.tbcity (continentID, city, locID, countryID, approved) values (" . $this->continentID . 
					", '" . addslashes($this->getName()) . "', " . $this->getLocationID() . ", " . $this->countryID . ", " . $this-> approved . ")";
 			$this-> rs2-> Execute($sql2);
 			
 			$this-> cityID = mysql_insert_id();
 		}
 		
 		function update(){
			/* if ($this->countryID == 0 || $this->continentID == 0 || $this->cityID==0) {
				throw new Exception("Cannot update! either countryId,continentId or cityId is not set");
			} */
 			if ($this->cityID == 0)
 				throw new Exception("Cannot update! CityID is not set");

 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());	
 				
 			$sql = "update GoAbroad_Main.tbcity set city = '" . addslashes($this->getName()) . "', continentID = " . $this->continentID . ", approved = " . 
 					$this->approved . " where cityID = " . $this->cityID;
			$this-> rs-> Execute($sql);
 		} 
 		
 		function delete(){ 			
 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
 			
 			// delete record in tblLocation
 			$sql = "delete from tblLocation where locID = " . $this->getLocationID();
 			$this-> rs-> Execute($sql);
 			 			
 			// delete record in tbcity
 			$sql = "delete from GoAbroad_Main.tbcity where locID = " . $this->getLocationID();
 			$this-> rs-> Execute($sql);

 			$this-> setLocationID(0);
 			$this-> setName("");
 			$this-> setCountryID(0);
 			$this-> setContinentID(0);
 			$this-> cityID = 0;
 		}	 	
	 }  
?>