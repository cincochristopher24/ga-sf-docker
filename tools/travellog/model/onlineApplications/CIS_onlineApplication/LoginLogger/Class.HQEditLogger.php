<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
require_once('Class.iRecordset.php');
class HQEditLogger {
	
	protected $conn = null;
	public static $instance = null;
	public static function getInstance(){
		if( is_null(self::$instance) ){
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	private function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function saveProcess(array $params, $action){
		$data = addslashes(serialize($params));
		$string_data = $this->transformToString($params);
		$sql = "INSERT INTO `tblCISOnlineAppHQEditsLog` 
					SET `action` = ".ToolMan::makeSqlSafeString($action).", ".
					"`data` = ".ToolMan::makeSqlSafeString($data).", ".
					"`stringData` = ".ToolMan::makeSqlSafeString($string_data).", ".
					"`transactionDate` = now()";
	//	$sql = sprintf("INSERT INTO `tblCISOnlineAppHQEditsLog` SET `data` = '%s'")
	
		$this->conn->execQuery($sql);
	}
	
	public function getLogs(){
		$sql = "SELECT * FROM `tblCISOnlineAppHQEditsLog` WHERE 1";
		$rs = new iRecordset($this->conn->execQuery($sql));
		$logs = array();
		while(!$rs->EOF()){
			$logs[] = $rs->retrieveRow();
			$rs->moveNext();
		}
		return $logs;
	}
	
	private function transformToString($params){
		ob_start();
		print_r($params);
		$string_params = ob_get_contents();
		ob_end_clean();
		return $string_params;	
	}
	
}