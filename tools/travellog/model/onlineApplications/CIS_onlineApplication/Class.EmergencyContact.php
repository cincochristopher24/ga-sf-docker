<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class EmergencyContact {
	
	protected	$id,
				$personalInfoID,
				$firstName,
				$lastName,
				$relationship,
				$address,
				$zip,
				$phoneNumber,
				$specialNeeds;
	
	protected	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	
	/* $values must be an associative array
	 * example = $value('txtFirstName' => any value)
	 * 
	 * prefixes: text     - text
	 * 			 textArea - txtA
	 *			 checkbox - chck
	 *			 radio    - rBtn
	 *			 combo	  - cmbo
	 */ 
	
	public function setValues($values = array()){
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$emergencyContactFieldMap)){
				$functionName = 'set'.$arrKey;
				$this->$functionName($value);
			}
		}
		
		
	}
	
	// #### SAVE TO DATABASE ####
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppEmergencyContact SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'FirstName     ='.ToolMan::makeSqlSafeString( $this->getFirstName()      ).', '.
					'LastName      ='.ToolMan::makeSqlSafeString( $this->getLastName()       ).', '.
					'Relationship  ='.ToolMan::makeSqlSafeString( $this->getRelationship()   ).', '.
					'Address       ='.ToolMan::makeSqlSafeString( $this->getAddress()        ).', '.
					'Zip           ='.ToolMan::makeSqlSafeString( $this->getZip()            ).', '.
					'PhoneNumber   ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber()    );
		
		$this->conn->execQuery($sql);
	//	var_dump($sql);
	}
	
	public function update(){
		$sql = 'INSERT INTO tblCISOnlineAppEmergencyContact SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).' ,'.
					'FirstName     ='.ToolMan::makeSqlSafeString( $this->getFirstName()      ).', '.
					'LastName      ='.ToolMan::makeSqlSafeString( $this->getLastName()       ).', '.
					'Relationship  ='.ToolMan::makeSqlSafeString( $this->getRelationship()   ).', '.
					'Address       ='.ToolMan::makeSqlSafeString( $this->getAddress()        ).', '.
					'Zip           ='.ToolMan::makeSqlSafeString( $this->getZip()            ).', '.
					'PhoneNumber   ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber()    ).' '.
				'ON duplicate KEY UPDATE '.
					'FirstName     ='.ToolMan::makeSqlSafeString( $this->getFirstName()      ).', '.
					'LastName      ='.ToolMan::makeSqlSafeString( $this->getLastName()       ).', '.
					'Relationship  ='.ToolMan::makeSqlSafeString( $this->getRelationship()   ).', '.
					'Address       ='.ToolMan::makeSqlSafeString( $this->getAddress()        ).', '.
					'Zip           ='.ToolMan::makeSqlSafeString( $this->getZip()            ).', '.
					'PhoneNumber   ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber()    );
		$this->conn->execQuery($sql);
		
	}
	
	// #### SETTERS ####
	public function setID($v){
		$this->id = $v;
	}
	
	public function setPersonalInfoID($v){
		$this->personalInfoID = (!is_null($v) ? $v : '');
	}
	
	public function setFirstName($v){
		$this->firstName = (!is_null($v) ? $v : '');
	}
	
	public function setLastName($v){
		$this->lastName = (!is_null($v) ? $v : '');
	}
	
	public function setRelationship($v){
		$this->relationship = (!is_null($v) ? $v : '');
	}
	// deprecated
	public function setAddress($v){
		$this->address = (!is_null($v) ? $v : '');
	}
	
	public function setZip($v){
		$this->zip = (!is_null($v) ? $v : '');
	}
	
	public function setPhoneNumber($v){
		$this->phoneNumber = (!is_null($v) ? $v : '');
	}
	// deprecated * ends here
	public function setSpecialNeeds($v){
		$this->specialNeeds = (!is_null($v) ? $v : '');
	}
	
	// #### GETTERS ####
	public function getID(){
		return $this->id;
	}
	
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	
	public function getFirstName(){
		return $this->firstName;
	}
	
	public function getLastName(){
		return $this->lastName;
	}
	
	public function getRelationship(){
		return $this->relationship;
	}
	
	public function getAddress(){
		return $this->address;
	}
	
	public function getZip(){
		return $this->zip;
	}
	
	public function getPhoneNumber(){
		return $this->phoneNumber;
	}
	
	public function getSpecialNeeds(){
		return $this->specialNeeds;
	}
	
	public function getName(){
		return $this->firstName.' '.$this->lastName;
	}
	
	
}
?>