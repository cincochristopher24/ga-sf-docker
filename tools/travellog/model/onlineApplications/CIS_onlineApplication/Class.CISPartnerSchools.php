<?php

require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class CISPartnerSchools {
	
	
	protected	$id,
				$name,
				$stateID;
				
	protected	$conn;
	
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	// setters
	public function setID($v){
		$this->id = $v;
	}
	
	public function setName($v){
		$this->name = $v;
	}
	
	public function setStateID($v){
		$this->stateID = $v;
	}
	
	// getters
	public function getID(){
		return $this->id;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function getStateID(){
		return $this->stateID;
	}
	
	// save
	public function save(){}
	
	
	// retrieve
/*	public function retrieveAll(){
		$sql = 'SELECT * FROM tblCISOnlineAppPartnerSchools';
		$rs = new iRecordset($this->conn->execQuery($sql));
		
		$schools = array();
		while(!$rs->EOF()){
			$school = new self;
			$school->setID  ( $rs->getId()   );
			$school->setName( $rs->getName() );
			
			$schools[] = $school;
		}
		
		return $schools;
	} */
}
?>