<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class AdditionalInfo {
	
	protected 	$id,
				$personalInfoID,
				$specialNeeds,
				$learnProgramFrom,
				$others,
				$campusRep,
				$friendGrant,
				$goAgainGrant,
				$financialAid;
	
	protected $conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	
	public function setValues($values = array()){
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$additionalInfoFielMap)){
				$functionName = 'set'.$arrKey;
				$this->$functionName($value);
			}
		}
		
		
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppAdditionalInfo SET
					PersonalInfoID    ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()   ).', '.
					'SpecialNeeds     ='.ToolMan::makeSqlSafeString( $this->getSpecialNeeds()     ).', '.
					'LearnProgramFrom ='.ToolMan::makeSqlSafeString( $this->getLearnProgramFrom() ).', '.
					'Others           ='.ToolMan::makeSqlSafeString( $this->getOthers()           ).', '.
					'CampusRep        ='.ToolMan::makeSqlSafeString( $this->getCampusRep()        ).', '.
					'FriendGrant      ='.ToolMan::makeSqlSafeString( $this->getFriendGrant()      ).', '.
					'goAgainGrant      ='.ToolMan::makeSqlSafeString( $this->getGoAgainGrant()      ).', '.
					'FinancialAid     ='.ToolMan::makeSqlSafeString( $this->getFinancialAid()     );
		$this->conn->execQuery($sql);
	//	var_dump($sql);
	}
	
	public function update(){
		$sql = 'INSERT INTO tblCISOnlineAppAdditionalInfo SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()   ).', '.
					'SpecialNeeds      ='.ToolMan::makeSqlSafeString( $this->getSpecialNeeds()     ).', '.
					'LearnProgramFrom ='.ToolMan::makeSqlSafeString( $this->getLearnProgramFrom() ).', '.
					'Others           ='.ToolMan::makeSqlSafeString( $this->getOthers()           ).', '.
					'CampusRep        ='.ToolMan::makeSqlSafeString( $this->getCampusRep()        ).', '.
					'FriendGrant      ='.ToolMan::makeSqlSafeString( $this->getFriendGrant()      ).', '.
					'goAgainGrant      ='.ToolMan::makeSqlSafeString( $this->getGoAgainGrant()      ).', '.
					'FinancialAid     ='.ToolMan::makeSqlSafeString( $this->getFinancialAid()     ).' '.
				'ON duplicate KEY UPDATE '.
					'SpecialNeeds      ='.ToolMan::makeSqlSafeString( $this->getSpecialNeeds()     ).', '.
					'LearnProgramFrom ='.ToolMan::makeSqlSafeString( $this->getLearnProgramFrom() ).', '.
					'Others           ='.ToolMan::makeSqlSafeString( $this->getOthers()           ).', '.
					'CampusRep        ='.ToolMan::makeSqlSafeString( $this->getCampusRep()        ).', '.
					'FriendGrant      ='.ToolMan::makeSqlSafeString( $this->getFriendGrant()      ).', '.
					'goAgainGrant     ='.ToolMan::makeSqlSafeString( $this->getGoAgainGrant()      ).', '.
					'FinancialAid     ='.ToolMan::makeSqlSafeString( $this->getFinancialAid()     );
		$this->conn->execQuery($sql);
		
	}
	
	// #### SETTERS ####
	public function setID($v){
		$this->id = $v;
	}
	
	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}
	
	public function setSpecialNeeds($v){
		$this->specialNeeds = $v;
	}
	
	public function setLearnProgramFrom($v){
		$this->learnProgramFrom = $v;
	}
	
	public function setOthers($v){
		$this->others = $v;
	}
	
	public function setCampusRep($v){
		$this->campusRep = $v;
	}
	
	public function setFriendGrant($v){
		$this->friendGrant = $v;
	}
	
	public function setGoAgainGrant($v){
		$this->goAgainGrant = $v;
	}
	
	public function setFinancialAid($v){
		$this->financialAid = $v;
	}
	
	// #### GETTERS ####
	public function getID(){
		return $this->id;
	}
	
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	
	public function getSpecialNeeds(){
		return $this->specialNeeds;
	}
	
	public function getLearnProgramFrom(){
		return $this->learnProgramFrom;
	}
	
	public function getOthers(){
		return $this->others;
	}
	
	public function getCampusRep(){
		return $this->campusRep;
	}
	
	public function getFriendGrant(){
		return $this->friendGrant;
	}
	
	public function getGoAgainGrant(){
		return $this->goAgainGrant;
	}
	
	public function getFinancialAid(){
		return $this->financialAid;
	}
}
?>