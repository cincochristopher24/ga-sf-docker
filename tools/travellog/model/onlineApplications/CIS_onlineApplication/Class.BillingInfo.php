<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class BillingInfo {
	
	
	
	protected	$id,
				$personalInfoID,
				$payingPersons,
				$mainBillingContact,
				$fname,
				$lname,
				$relationship,
				$address,
				$phoneNumber,
				$email;
	
	protected $conn;
	
	private static $fielMap = array('PersonalInfoID', 'PayingPersons', 'MainBillingContact',
									'Relationship', 'Address', 'PhoneNumber', 'Email');
	
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function setValues($values = array()){
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$billingInfoFieldMap)){
				$functionName = 'set'.$arrKey;
				$this->$functionName($value);
			}
		}
		
		
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppBillingInfo SET
					PersonalInfoID      ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()     ).', '.
					'PayingPersons      ='.ToolMan::makeSqlSafeString( $this->getPayingPersons()      ).', '.					
					'MainBillingContact ='.ToolMan::makeSqlSafeString( $this->getMainBillingContact() ).', '.
					'FirstName          ='.ToolMan::makeSqlSafeString( $this->getFirstName()          ).', '.
					'LastName           ='.ToolMan::makeSqlSafeString( $this->getLastName()           ).', '.
					'Relationship       ='.ToolMan::makeSqlSafeString( $this->getRelationship()       ).', '.
					'Address            ='.ToolMan::makeSqlSafeString( $this->getAddress()            ).', '.
					'PhoneNumber        ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber()        ).', '.
					'Email              ='.ToolMan::makeSqlSafeString( $this->getEmail()              );
		$this->conn->execQuery($sql);
	//	echo '<br/>'.$sql;
	}
	
	public function update(){
		$sql = 'INSERT INTO tblCISOnlineAppBillingInfo SET
					PersonalInfoID      ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()     ).', '.
					'PayingPersons      ='.ToolMan::makeSqlSafeString( $this->getPayingPersons()      ).', '.
					'MainBillingContact ='.ToolMan::makeSqlSafeString( $this->getMainBillingContact() ).', '.
					'FirstName          ='.ToolMan::makeSqlSafeString( $this->getFirstName()          ).', '.
					'LastName           ='.ToolMan::makeSqlSafeString( $this->getLastName()           ).', '.
					'Relationship       ='.ToolMan::makeSqlSafeString( $this->getRelationship()       ).', '.
					'Address            ='.ToolMan::makeSqlSafeString( $this->getAddress()            ).', '.
					'PhoneNumber        ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber()        ).', '.
					'Email              ='.ToolMan::makeSqlSafeString( $this->getEmail()              ).' '.
				'ON duplicate KEY UPDATE '.
					'PayingPersons      ='.ToolMan::makeSqlSafeString( $this->getPayingPersons()      ).', '.
					'MainBillingContact ='.ToolMan::makeSqlSafeString( $this->getMainBillingContact() ).', '.
					'FirstName          ='.ToolMan::makeSqlSafeString( $this->getFirstName()          ).', '.
					'LastName           ='.ToolMan::makeSqlSafeString( $this->getLastName()           ).', '.
					'Relationship       ='.ToolMan::makeSqlSafeString( $this->getRelationship()       ).', '.
					'Address            ='.ToolMan::makeSqlSafeString( $this->getAddress()            ).', '.
					'PhoneNumber        ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber()        ).', '.
					'Email              ='.ToolMan::makeSqlSafeString( $this->getEmail()              );
		$this->conn->execQuery($sql);
	//	echo '<br/>'.$sql;
	}
	
	
	// #### SETTERS ####
	public function setID($v){
		$this->id = $v;
	}
	
	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}
	public function setPayingPersons($v){
		$this->payingPersons = $v;
	}
	public function setMainBillingContact($v){
		$this->mainBillingContact = $v;
	}
	
	public function setFirstName($v){ $this->fname = $v; }
	public function setLastName($v){ $this->lname = $v; }
	public function setRelationship($v){
		$this->relationship = $v;
	}
	public function setAddress($v){
		$this->address = $v;
	}
	public function setPhoneNumber($v){
		$this->phoneNumber = $v;
	}
	public function setEmail($v){
		$this->email = $v;
	}
	
	// #### GETTERS ####
	public function getID(){
		return $this->id;
	}
	
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	public function getPayingPersons(){
		return $this->payingPersons;
	}
	public function getMainBillingContact(){
		return $this->mainBillingContact;
	}
	
	public function getFirstName(){ return $this->fname; }
	public function getLastName(){ return $this->lname; }
	public function getRelationship(){
		return $this->relationship;
	}
	public function getAddress(){
		return $this->address;
	}
	public function getPhoneNumber(){
		return $this->phoneNumber;
	}
	public function getEmail(){
		return $this->email;
	}
	
}
?>