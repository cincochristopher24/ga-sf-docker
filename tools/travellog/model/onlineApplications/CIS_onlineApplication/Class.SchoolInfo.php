<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class SchoolInfo {


	private static $fieldMap = array(
								'PersonalInfoID', 'SchoolName', 'Major', 'Minor',
								'YearInSchool', 'GenAverage', 'CurrentCourses','contact','email'
								);


	protected 	$id,
				$personalInfoID,
				$schoolID,
				$type,
				$otherSchool,
				$major,
				$minor,
				$yearInSchool,
				$genAverage,
				$currentCourses,
				$stateAbbr = null,
				$contact,
				$email;

	protected $conn;


	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}



	public function setValues($values = array()){
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$schoolInfoFieldMap)){
				$functionName = 'set'.$arrKey;
				$this->$functionName($value);
			}
		}


	}

	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppSchoolInfo SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'SchoolID      ='.ToolMan::makeSqlSafeString( $this->getSchoolID()     ).', '.
					'Type          ='.ToolMan::makeSqlSafeString( $this->getType()     ).', '.
					'OtherSchoolName ='.ToolMan::makeSqlSafeString( $this->getOtherSchoolName()     ).', '.
					'Major         ='.ToolMan::makeSqlSafeString( $this->getMajor()          ).', '.
					'Minor         ='.ToolMan::makeSqlSafeString( $this->getMinor()          ).', '.
					'YearInSchool  ='.ToolMan::makeSqlSafeString( $this->getYearInSchool()   ).', '.
					'GenAverage    ='.ToolMan::makeSqlSafeString( $this->getGenAverage()     ).', '.
					'CurrentCourses='.ToolMan::makeSqlSafeString( $this->getCurrentCourses() );

		$this->conn->execQuery($sql);
	//	var_dump($sql);
	}

	public function update(){
		$sql = 'INSERT INTO tblCISOnlineAppSchoolInfo SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'SchoolID    ='.ToolMan::makeSqlSafeString( $this->getSchoolID()     ).', '.
					'Type          ='.ToolMan::makeSqlSafeString( $this->getType()     ).', '.
					'OtherSchoolName ='.ToolMan::makeSqlSafeString( $this->getOtherSchoolName()     ).', '.
					'Major         ='.ToolMan::makeSqlSafeString( $this->getMajor()          ).', '.
					'Minor         ='.ToolMan::makeSqlSafeString( $this->getMinor()          ).', '.
					'YearInSchool  ='.ToolMan::makeSqlSafeString( $this->getYearInSchool()   ).', '.
					'GenAverage    ='.ToolMan::makeSqlSafeString( $this->getGenAverage()     ).', '.
					'CurrentCourses='.ToolMan::makeSqlSafeString( $this->getCurrentCourses() ).' '.
				'ON duplicate KEY UPDATE '.
					'SchoolID    ='.ToolMan::makeSqlSafeString( $this->getSchoolID()     ).', '.
					'Type          ='.ToolMan::makeSqlSafeString( $this->getType()     ).', '.
					'OtherSchoolName ='.ToolMan::makeSqlSafeString( $this->getOtherSchoolName()     ).', '.
					'Major         ='.ToolMan::makeSqlSafeString( $this->getMajor()          ).', '.
					'Minor         ='.ToolMan::makeSqlSafeString( $this->getMinor()          ).', '.
					'YearInSchool  ='.ToolMan::makeSqlSafeString( $this->getYearInSchool()   ).', '.
					'GenAverage    ='.ToolMan::makeSqlSafeString( $this->getGenAverage()     ).', '.
					'CurrentCourses='.ToolMan::makeSqlSafeString( $this->getCurrentCourses() );
		$this->conn->execQuery($sql);
	}


	// #### SETTERS #####
	public function setID($v){
		$this->id = $v;
	}

	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}

	public function setSchoolID($v){
		$this->schoolID = $v;
	}

	public function setType($v){
		$this->type = $v;
	}

	public function setOtherSchoolName($v){
		$this->otherSchool = $v;
	}

	public function setMajor($v){
		$this->major = $v;
	}

	public function setMinor($v){
		$this->minor = $v;
	}

	public function setYearInSchool($v){
		$this->yearInSchool = $v;
	}

	public function setGenAverage($v){
		$this->genAverage = $v;
	}

	public function setCurrentCourses($v){
		$this->currentCourses = $v;
	}
	
	public function setContact($v){
		$this->contact = $v;
	}
	
	public function setEmail($v){
		$this->email = $v;
	}
	
	// #### GETTERS ####
	public function getID(){
		return $this->id;
	}

	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}

	public function getSchoolID(){
		return $this->schoolID;
	}

	public function getType(){
		return $this->type;
	}

	public function getOtherSchoolName(){
		return $this->otherSchool;
	}

	public function getMajor(){
		return $this->major;
	}

	public function getMinor(){
		return $this->minor;
	}

	public function getYearInSchool(){
		return $this->yearInSchool;
	}

	public function getGenAverage(){
		return $this->genAverage;
	}

	public function getCurrentCourses(){
		return $this->currentCourses;
	}

	public function getContact(){
		return $this->contact;
	}
	
	public function getEmail(){
		return $this->email;
	}
	
	public function getSchoolName(){
		if(0 < $this->schoolID){

			if($this->type == '0'){
				require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.USUniversitiesPeer.php');
				$school = USUniversitiesPeer::retrieveByPk($this->schoolID);
				// try fix bug
				if(!$school){
					$school = USUniversitiesPeer::retrieveFromOldDB($this->schoolID);
				}
				$this->stateAbbr = USUniversitiesPeer::getSchoolStateAbbrByStateID($school->getStateID());
				return $school->getName();

			}
			else if($this->type == '1') {
				require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.CISPartnerSchoolsPeer.php');
				$school = CISPartnerSchoolsPeer::retrieveByPk($this->schoolID);
				$this->stateAbbr = CISPartnerSchoolsPeer::getSchoolStateAbbrByStateID($school->getStateID());
				return $school->getName();
			}

		}
		else {

			return $this->otherSchool;
		}
	}

	// function to get the school state
	public function getStateAbbr(){
	/*	if(0 < $this->schoolID){

			if($this->type == '0'){
				require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.USUniversitiesPeer.php');

				$this->stateAbbr = USUniversitiesPeer::getSchoolStateAbbrByStateID($school->getStateID());


			}
			else if($this->type == '1') {
				require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.CISPartnerSchoolsPeer.php');

				$this->stateAbbr = CISPartnerSchoolsPeer::getSchoolStateAbbrByStateID($school->getStateID());

			}

		}*/

		return $this->stateAbbr;
	}

	public function getCpaSchools() {
		$cpaSchools = array();

		$sql = "SELECT * FROM `tblCISOnlineAppPartnerSchools_new`";
		$rs = new iRecordset($this->conn->execQuery($sql));

		while(!$rs->EOF()){
			$cpaSchools[] = $rs->retrieveRow();
			$rs->moveNext();
		}

		return $cpaSchools;
	}
}
?>