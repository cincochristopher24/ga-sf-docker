<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class ProgramInformation {
	
	
	protected	$id,
				$personalInfoID,
				$term,
				$year,
				$firstProgram,
				$option1,
				$secondProgram,
				$option2,
				$priority,
				$interest,
				$volunteering,
				$housing_options_id,
				$duration_id,
				$startMonth,
				$startDay;
	
	protected $conn;
	
	
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	
	public function setValues($values = array()){
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$programInfoFieldMap)){
				$functionName = 'set'.$arrKey;
				$this->$functionName($value);
			}
		}
		
		
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppProgramInfo SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'Term          ='.ToolMan::makeSqlSafeString( $this->getTerm()           ).', '.
					'Year          ='.ToolMan::makeSqlSafeString( $this->getYear()           ).', '.
					'FirstProgramID  ='.ToolMan::makeSqlSafeString( $this->getFirstProgramID() ).', '.
					'FirstOptionID   ='.ToolMan::makeSqlSafeString( $this->getFirstOptionID()  ).', '.
					'SecondProgramID ='.ToolMan::makeSqlSafeString( $this->getSecondProgramID()).', '.
					'SecondOptionID  ='.ToolMan::makeSqlSafeString( $this->getSecondOptionID() ).', '.
					'PriorityOption  ='.ToolMan::makeSqlSafeString( $this->getPriorityOption() ).', '.
					'Interest      ='.ToolMan::makeSqlSafeString( $this->getInterest()         ).', '.
					'Volunteering  ='.ToolMan::makeSqlSafeString( $this->getVolunteering()     ).', '.
					'housing_options_id ='.ToolMan::makeSqlSafeString( $this->getHousingOptionID() ).', '.
					'duration_id ='.ToolMan::makeSqlSafeString( $this->getDurationOptionID()     	).', '.
					'startMonth ='.ToolMan::makeSqlSafeString( $this->getStartMonth()     		).', '.
					'startDay ='.ToolMan::makeSqlSafeString( $this->getStartDay()    			);
		$this->conn->execQuery($sql);
		
	
		$this->setID($this->conn->getLastInsertedID());
		
	}
	
	public function update(){
		$sql = 'INSERT INTO tblCISOnlineAppProgramInfo SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'Term          ='.ToolMan::makeSqlSafeString( $this->getTerm()           ).', '.
					'Year          ='.ToolMan::makeSqlSafeString( $this->getYear()           ).', '.
					'FirstProgramID  ='.ToolMan::makeSqlSafeString( $this->getFirstProgramID() ).', '.
					'FirstOptionID   ='.ToolMan::makeSqlSafeString( $this->getFirstOptionID()  ).', '.
					'SecondProgramID ='.ToolMan::makeSqlSafeString( $this->getSecondProgramID()).', '.
					'SecondOptionID  ='.ToolMan::makeSqlSafeString( $this->getSecondOptionID() ).', '.
					'PriorityOption  ='.ToolMan::makeSqlSafeString( $this->getPriorityOption() ).', '.
					'Interest      ='.ToolMan::makeSqlSafeString( $this->getInterest()       ).', '.
					'Volunteering  ='.ToolMan::makeSqlSafeString( $this->getVolunteering()   ). ', '.
					'housing_options_id ='.ToolMan::makeSqlSafeString( $this->getHousingOptionID() ).', '.
					'duration_id ='.ToolMan::makeSqlSafeString( $this->getDurationOptionID()     	).', '.
					'startMonth ='.ToolMan::makeSqlSafeString( $this->getStartMonth()     		).', '.
					'startDay ='.ToolMan::makeSqlSafeString( $this->getStartDay()    			).' '.
				'ON duplicate KEY UPDATE '.
					'Term          ='.ToolMan::makeSqlSafeString( $this->getTerm()           ).', '.
					'Year          ='.ToolMan::makeSqlSafeString( $this->getYear()           ).', '.
					'FirstProgramID  ='.ToolMan::makeSqlSafeString( $this->getFirstProgramID()   ).', '.
					'FirstOptionID   ='.ToolMan::makeSqlSafeString( $this->getFirstOptionID()   ).', '.
					'SecondProgramID ='.ToolMan::makeSqlSafeString( $this->getSecondProgramID()  ).', '.
					'SecondOptionID  ='.ToolMan::makeSqlSafeString( $this->getSecondOptionID()  ).', '.
					'PriorityOption  ='.ToolMan::makeSqlSafeString( $this->getPriorityOption()  ).', '.
					'Interest      ='.ToolMan::makeSqlSafeString( $this->getInterest()       ).', '.
					'Volunteering  ='.ToolMan::makeSqlSafeString( $this->getVolunteering()   ).', '.
					'housing_options_id ='.ToolMan::makeSqlSafeString( $this->getHousingOptionID() ).', '.
					'duration_id ='.ToolMan::makeSqlSafeString( $this->getDurationOptionID()     	).', '.
					'startMonth ='.ToolMan::makeSqlSafeString( $this->getStartMonth()     		).', '.
					'startDay ='.ToolMan::makeSqlSafeString( $this->getStartDay()    			);
		$this->conn->execQuery($sql);
	}
	
	// #### SETTERS ####
	public function setID($v){
		$this->id = $v;
	}
	
	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}
	
	public function setTerm($v){
		$this->term = $v;
	}
	
	public function setYear($v){
		$this->year = $v;
	}
	
	public function setFirstProgramID($v){
		$this->firstProgram = $v;
	}
	
	public function setFirstOptionID($v){
		$this->option1 = $v;
	}
	
	public function setSecondProgramID($v){
		$this->secondProgram = $v;
	}
	
	public function setSecondOptionID($v){
		$this->option2 = $v;
	}
	
	public function setPriorityOption($v){
		$this->priority = $v;
	}
	
	public function setInterest($v){
		$this->interest = $v;
	}
	
	public function setVolunteering($v){
		$this->volunteering = $v;
	}
	
	public function setHousingOptionID($v){
		$this->housing_options_id = $v;
	}
	
	public function setDurationOptionID($v){
		$this->duration_id = $v;
	}
	
	public function setStartMonth($v){
		$this->startMonth = $v;
	}
	
	public function setStartDay($v){
		$this->startDay = $v;
	}
	
	// #### GETTERS ####
	public function getID(){
		return $this->id;
	}
	
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	
	public function getTerm(){
		return $this->term;
	}
	
	public function getYear(){
		return $this->year;
	}
	
	public function getFirstProgramID(){
		return $this->firstProgram;
	}
	
	public function getFirstOptionID(){
		return $this->option1;
	}
	
	public function getSecondProgramID(){
		return $this->secondProgram;
	}
	
	public function getSecondOptionID(){
		return $this->option2;
	}
	
	public function getPriorityOption(){
		return $this->priority;
	}
	
	public function getInterest(){
		return $this->interest;
	}
	
	public function getVolunteering(){
		return $this->volunteering;
	}
	
	public function getHousingOptionID(){
		return $this->housing_options_id;
	}
	
	public function getDurationOptionID(){
		return $this->duration_id;
	}
	
	public function getStartMonth(){
		return $this->startMonth;
	}
	
	public function getStartDay(){
		return $this->startDay;
	}
	
	// miscelaneous functions
	
	public function getTermName(){
		$name = "";
		if(0 < $this->term){
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
			$term = TermsPeer::retrieveByPk($this->term);
			$name = $term->getName();
		}
		return $name;
	}
	
	public function getFirstProgramName(){
		$name = "";
		if(0 < $this->firstProgram){
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
			$prog1 = ProgramsPeer::retrieveByPk($this->firstProgram);
			$name = $prog1->getName();
		}
		return $name;
	}
	
	public function getSecondProgramName(){
		$name = "";
		if(0 < $this->secondProgram){
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
			$prog2 = ProgramsPeer::retrieveByPk($this->secondProgram);
			$name = $prog2->getName();
		}
		return $name;
	}
	
	public function getFirstOptionName(){
		$name = "";
		if(0 < $this->option1){
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');
			$opt1 = OptionsPeer::retrieveByPk($this->option1);
			$name = $opt1->getName();
		}
		return $name;
	}
	
	public function getSecondOptionName(){
		$name = "";
		if(0 < $this->option2){
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');
			$opt2 = OptionsPeer::retrieveByPk($this->option2);
			$name = $opt2->getName();
		}
		return $name;
	}
	
	public function getHousingOption(){
		
	}
	
	
	
}
?>