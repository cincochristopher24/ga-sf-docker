<?php


class HousingOptions {
	
	protected 	$id,
				$name,
				$programID,
				$termID;
				
	
	public function __construct(){}
				
	// setterrs
	public function setID($v){
		$this->id = $v;
	}
	
	public function setName($v){
		$this->name = $v;
	}
	
	public function setProgramID($v){
		$this->programID = $v;
	}
	public function setTermID($v){
		$this->termID = $v;
	}
	// getters
	public function getID(){
		return $this->id;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function getProgramID(){
		return $this->programID;
	}
	public function getTermID(){
		return $this->termID;
	}
}