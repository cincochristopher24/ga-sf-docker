<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class AddressInfo {
	
	
	
	protected	$id,
				$personalInfoID,
				$type = 1,  // set the default value
				$street1,
				$city1,
				$state1,
				$zip1,
				$phoneNumber1,
				$cellphone,
				$street2,
				$city2,
				$state2,
				$zip2,
				$phoneNumber2,
				$permanentAddress,
				$validUntil;
				
	protected $conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function setValues($values = array()){
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$addressInfoFieldMap)){
				$functionName = 'set'.$arrKey;
				$this->$functionName($value);
			}
		}
		
		
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppAddressInfo SET
					PersonalInfoID    ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()   ).', '.
					'AddressType      ='.ToolMan::makeSqlSafeString( $this->getAddressType()      ).', '.
					'Street1  ='.ToolMan::makeSqlSafeString( $this->getStreet1()          ).', '.
					'City1    ='.ToolMan::makeSqlSafeString( $this->getCity1()            ).', '.
					'State1   ='.ToolMan::makeSqlSafeString( $this->getState1()           ).', '.
					'Zip1     ='.ToolMan::makeSqlSafeString( $this->getZip1()             ).', '.
					'PhoneNumber1 ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber1() ).', '.
					'Cellphone    ='.ToolMan::makeSqlSafeString( $this->getCellphone()    ).', '.
					'Street2  ='.ToolMan::makeSqlSafeString( $this->getStreet2()          ).', '.
					'City2    ='.ToolMan::makeSqlSafeString( $this->getCity2()            ).',  '.
					'State2   ='.ToolMan::makeSqlSafeString( $this->getState2()           ).', '.
					'Zip2     ='.ToolMan::makeSqlSafeString( $this->getZip2()             ).', '.
					'PhoneNumber2 ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber2() ).', '.
					'PermanentAddress ='.ToolMan::makeSqlSafeString( $this->getPermanentAddress() ).', '.
					'ValidUntil ='. ToolMan::makeSqlSafeString( $this->getValidUntil() );
		
		$this->conn->execQuery($sql);
	//	var_dump($sql);
	}
	
	public function update(){
		$sql = 'UPDATE tblCISOnlineAppAddressInfo SET
					Street1  ='.ToolMan::makeSqlSafeString( $this->getStreet1()          ).', '.
					'City1    ='.ToolMan::makeSqlSafeString( $this->getCity1()            ).', '.
					'State1   ='.ToolMan::makeSqlSafeString( $this->getState1()           ).', '.
					'Zip1     ='.ToolMan::makeSqlSafeString( $this->getZip1()             ).', '.
					'PhoneNumber1 ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber1() ).', '.
					'Cellphone    ='.ToolMan::makeSqlSafeString( $this->getCellphone()    ).', '.
					'Street2  ='.ToolMan::makeSqlSafeString( $this->getStreet2()          ).', '.
					'City2    ='.ToolMan::makeSqlSafeString( $this->getCity2()            ).',  '.
					'State2   ='.ToolMan::makeSqlSafeString( $this->getState2()           ).', '.
					'Zip2     ='.ToolMan::makeSqlSafeString( $this->getZip2()             ).', '.
					'PhoneNumber2 ='.ToolMan::makeSqlSafeString( $this->getPhoneNumber2() ).', '.
					'PermanentAddress ='.ToolMan::makeSqlSafeString( $this->getPermanentAddress() ).', '.
					'ValidUntil ='. ToolMan::makeSqlSafeString( $this->getValidUntil() ). ' '.
				'WHERE PersonalInfoID = '.ToolMan::makeSqlSafeString( $this->getPersonalInfoID()   ).' '.
				'AND AddressType      ='.ToolMan::makeSqlSafeString( $this->getAddressType()      );

		$this->conn->execQuery($sql);
		
		
		
//		echo $sql;
		
	}
	
	// #### SETTERS ####
	public function setID($v){
		$this->id = $v;
	}
	
	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}
	
	public function setAddressType($v){
		$this->type = $v;
	}
	
	public function setStreet1($v){
		$this->street1 = $v;
	//	var_dump('street1 set to ='.$v);
	}
	
	public function setCity1($v){
		$this->city1 = $v;
	}
	
	public function setState1($v){
		$this->state1 = $v;
	}
	
	public function setZip1($v){
		$this->zip1 = $v;
	}
	
	public function setPhoneNumber1($v){
		$this->phoneNumber1 = $v;
	}
	
	public function setCellphone($v){
		$this->cellphone = $v;
	}
	
	public function setStreet2($v){
		$this->street2 = $v;
	}
	
	public function setCity2($v){
		$this->city2 = $v;
	}
	
	public function setState2($v){
		$this->state2 = $v;
	}
	
	public function setZip2($v){
		$this->zip2 = $v;
	}
	
	public function setPhoneNumber2($v){
		$this->phoneNumber2 = $v;
	}
	
	public function setPermanentAddress($v){
		$this->permanentAddress = $v;
	}
	
	public function setValidUntil($v){
		$this->validUntil = $v;
	}
	
	// #### GETTERS ####
	public function getID(){
		return $this->id;
	}
	
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	
	public function getAddressType(){
		return $this->type;
	}
	
	public function getStreet1(){
	//	var_dump('this is what u get = '.$this->street1);
		return $this->street1;
	}
	
	public function getCity1(){
		return $this->city1;
	}
	
	public function getState1(){
		return $this->state1;
	}
	
	public function getZip1(){
		return $this->zip1;
	}
	
	public function getPhoneNumber1(){
		return $this->phoneNumber1;
	}
	
	public function getCellphone(){
		return $this->cellphone;
	}
	
	public function getStreet2(){
		return $this->street2;
	}
	
	public function getCity2(){
		return $this->city2;
	}
	
	public function getState2(){
		return $this->state2;
	}
	
	public function getZip2(){
		return $this->zip2;
	}
	
	public function getPhoneNumber2(){
		return $this->phoneNumber2;
	}
	
	public function getPermanentAddress(){
		return $this->permanentAddress;
	}
	
	public function getValidUntil(){
		return $this->validUntil;
	}
	
	
	
}
?>