<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.ProgramInfo.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class ProgramInfoMapper {
	
	public static function populateObjects(iRecordset $rs){
		$programInfos = array();
		while(!$rs->EOF()){
			$programInfo = new ProgramInfo();
			$programInfo->setID             ( $rs->getId()             );
			$programInfo->setPersonalInfoID ( $rs->getPersonalInfoID() );
			$programInfo->setTerm           ( $rs->getTerm()           );
			$programInfo->setYear           ( $rs->getYear()           );
			$programInfo->setFirstProgram   ( $rs->getFirstProgram()   );
			$programInfo->setSecondProgram  ( $rs->getSecondProgram()  );
			$programInfo->setFullYear       ( $rs->getFullYear()       );
			$programInfo->setVolunteering   ( $rs->getVolunteering()   );
			
			$programInfo->setHousingOptionID   ( $rs->getHousing_options_id()   );
			$programInfo->setDurationOptionID   ( $rs->getDuration_id()   );
			$programInfo->setStartMonth   ( $rs->getStartMonth()   );
			$programInfo->setStartDay   ( $rs->getStartDay()   );
			
			$programInfos[] = $programInfo;
			$rs->moveNext();
		}
		return $programInfos;
	}
	
	public static function populateObject(iRecordset $rs){
		$programInfo = self::populateObjects($rs);
		return $programInfo[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppProgramInfo WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$programInfoObj = self::populateObject($rs);
			return $programInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppProgramInfo WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$programInfoObjs = self::populateObjects($rs);
			return $programInfoObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppProgramInfo WHERE PersonalInfoID = '.$personalInfoID.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$programInfoObj = self::populateObject($rs);
			return $programInfoObj;
		}
		
		return null;
	}
}
?>