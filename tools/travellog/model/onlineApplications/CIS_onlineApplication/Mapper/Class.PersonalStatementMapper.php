<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.PersonalStatement.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class PersonalStatementMapper {
	
	
	public static function populateObjects(iRecordset $rs){
		$personalStatements = array();
		while(!$rs->EOF()){
			$personalStatement = new PersonalStatement();
			$personalStatement->setID             ( $rs->getId()             );
			$personalStatement->setPersonalInfoID ( $rs->getPersonalInfoID() );
			$personalStatement->setStatement1     ( $rs->getStatement1()     );
			$personalStatement->setStatement2     ( $rs->getStatement2()     );
			$personalStatement->setStatement3     ( $rs->getStatement3()     );
			$personalStatement->setLateOption     ( $rs->getLateOption()     );
			$personalStatements[] = $personalStatement;
			$rs->moveNext();
		}
		return $personalStatements;
	}
	
	public static function populateObject(iRecordset $rs){
		$personalStatement = self::populateObjects($rs);
		return $personalStatement[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalStatement WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$personalStatementObj = self::populateObject($rs);
			return $personalStatementObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalStatement WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$personalStatementObjs = self::populateObjects($rs);
			return $personalStatementObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalStatement WHERE PersonalInfoID = '.$personalInfoID;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$personalStatementObj = self::populateObject($rs);
			return $personalStatementObj;
		}
		
		return null;
	}
}
?>