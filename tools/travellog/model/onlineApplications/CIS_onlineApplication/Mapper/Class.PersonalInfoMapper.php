<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.PersonalInfo.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class PersonalInfoMapper {
	
	public static function populateObjects(iRecordset $rs){
		$personalInfos = array();
		while(!$rs->EOF()){
			$personalInfo = new PersonalInfo();
			$personalInfo->setID               ( $rs->getId()                );
			$personalInfo->setOnlineAppUserID  ( $rs->getOnlineAppUserID()   );
			$personalInfo->setFirstName        ( $rs->getFirstName()         );
			$personalInfo->setLastName         ( $rs->getLastName()          );
			$personalInfo->setPreferredName    ( $rs->getPreferredName()     );
			$personalInfo->setGender           ( $rs->getGender()            );
			$personalInfo->setBirthday         ( $rs->getBirthday()          );
			$personalInfo->setCitizenship      ( $rs->getCitizenship()       );
			$personalInfo->setCountryBirthPlace( $rs->getCountryBirthPlace() );
			$personalInfo->setSecNumber        ( $rs->getSecNumber()         );
			$personalInfo->setEmail            ( $rs->getEmail()             );
			$personalInfo->setAltEmail         ( $rs->getAltEmail()          );
			$personalInfo->setBillingEmail     ( $rs->getBillingEmail()      );
			$personalInfo->setShirtSize        ( $rs->getShirtSize()         );
			$personalInfos[] = $personalInfo;
			$rs->moveNext();
		}
		return $personalInfos;
	}
	
	public static function populateObject(iRecordset $rs){
		$personalInfo = self::populateObjects($rs);
		return $personalInfo[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalInfo WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$personalInfoObj = self::populateObject($rs);
			return $personalInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalInfo WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$personalInfoObjs = self::populateObjects($rs);
			return $personalInfoObjs;
		}
		
		return null;
	}
	
	public static function retrieveByUserID($userID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalInfo WHERE OnlineAppUserID = '.$userID.' ORDER BY id DESC LIMIT 1 ';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$personalInfoObj = self::populateObject($rs);
			return $personalInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalInfo WHERE id = '.$personalInfoID.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$personalInfoObj = self::populateObject($rs);
			return $personalInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveAll(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalInfo';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$personalInfoObjs = self::populateObjects($rs);
			return $personalInfoObjs;
		}
		
		return null;
	}
	
	public static function test(){
		return "test";
	}
	
	// temporary function in retrieving record count
	// used in test
	public static function retrieveRecordCount(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalInfo';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			
			return $rs->retrieveRecordCount();
		}
		
		return null;
	}
	
	public static function isProcessDone($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT DoneProcess FROM tblCISOnlineAppPersonalInfo WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			if($rs->getDoneProcess() > 0){
				return true;
			}
		}
		
		return false;
	}
	
	public static function isViewerOwner($id, $userID){
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPersonalInfo WHERE id = '.$id.' AND OnlineAppUserID = '.$userID.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
		//	if($rs->getDoneProcess() > 0){
				return true;
		//	}
		}
		
		return false;
	}
	
	public static function isUserDoneApplying($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT DoneProcess FROM tblCISOnlineAppPersonalInfo WHERE OnlineAppUserID = '.$id.' ORDER BY id DESC LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			if($rs->getDoneProcess() > 0){
				return true;
			}
		}
		
		return false;
	}
	
	public static function getUserIDByOnlineAppID($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT OnlineAppUserID FROM tblCISOnlineAppPersonalInfo WHERE id = '.$id.' ORDER BY id DESC LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			return $rs->getOnlineAppUserID();
		}
		
		return null;
	}
	
}
?>