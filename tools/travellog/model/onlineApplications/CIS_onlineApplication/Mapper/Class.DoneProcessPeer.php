<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.DoneProcess.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class DoneProcessPeer {
	
	public static function populateObjects(iRecordset $rs){
		$processes = array();
		while(!$rs->EOF()){
			$process = new DoneProcess();
		//	var_dump($term);
			$process->setID        ( $rs->getId()   );
			$process->setProcessID ( $rs->getApplicationID() );
			
			$processes[] = $process;
			$rs->moveNext();
		}
		return $processes;
	}
	
	public static function populateObject(iRecordset $rs){
		$process = self::populateObjects($rs);
		return $process[0];
	}
	
	public static function retrieveByApplicationID($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppDoneApplication WHERE applicationID = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$process = self::populateObject($rs);
			return $process;
		}
		
		return null;
	}
	
	public static function isProcessDone($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppDoneApplication WHERE applicationID = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
		//	$process = self::populateObject($rs);
			return true;
		}
		
		return false;
	}
}
?>