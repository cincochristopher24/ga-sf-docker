<?php
/*
 * this class is the get the US universities
 * for the online application
 * 
 * 
 */
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.USUniversities.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class USUniversitiesPeer {
	
	public static function retrieveUSUniversities(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT universityID, university FROM tblGaUniversities_new';
		$rs = new iRecordset($conn->execQuery($sql));
		
		$Options = array();
		while(!$rs->EOF()){
				$Option = new USUniversities();
				$Option->setID        ( $rs->getId()   );
				$Option->setName      ( $rs->getName()     );
				$Option->setStateID   ( $rs->getStateID() 		 );
				$Options[] = $Option;
			
			
			
			$rs->moveNext();
		}
		return $Options;
	}
	
	public static function retrieveByPk($id){
	//	var_dump($id);
	//	$conn = new dbHandler('db=GoAbroad_Main');
		$conn = new dbHandler('db=Travel_Logs');
	//	$sql = 'SELECT universityID, university FROM tbuniversity WHERE universityID = '.$id.' LIMIT 1';
	//	$sql = 'SELECT id, UniversityName FROM tblGaUniversities WHERE id = '.$id.' LIMIT 1';
		$sql = 'SELECT id, name, stateID FROM tblGaUniversities_new WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		//$Options = array();
		$Option = null;
		while(!$rs->EOF()){
				$Option = new USUniversities();
			//	$Option->setID        ( $rs->getUniversityID()   );
			//	$Option->setName      ( $rs->getUniversity()     );
				$Option->setID        ( $rs->getId()             );
				$Option->setName      ( $rs->getName() );	
				$Option->setStateID   ( $rs->getStateID() );

			//	$Options[] = $Option;
			
			$rs->moveNext();
		}
		return $Option;
	}
	
	static public function retriveUniversitiesForAutoComplete($name){
		$conn = new dbHandler('db=Travel_Logs');
		$name = $conn->makeSqlSafeString('%'.$name.'%');
		/*$sql = "
			SELECT universityID as univID, university AS univName, 'usu' AS isPartner
			FROM GoAbroad_Main.tbuniversity AS univ LEFT JOIN 
			Travel_Logs.tblCISOnlineAppPartnerSchools AS partner ON partner.name = univ.university
			WHERE ISNULL(partner.name) AND university LIKE $name
			UNION ALL			
			SELECT ID as univID, name AS univName, 'partner' AS isPartner
			FROM Travel_Logs.tblCISOnlineAppPartnerSchools
			WHERE name LIKE $name
			ORDER BY univName
		";*/
		
		/*$sql = "
			SELECT univ.id as univID, UniversityName AS univName, 'usu' AS isPartner
			FROM tblGaUniversities AS univ LEFT JOIN 
			tblCISOnlineAppPartnerSchools AS partner ON partner.name = univ.UniversityName
			WHERE ISNULL(partner.name) AND UniversityName LIKE $name
			UNION ALL			
			SELECT ID as univID, name AS univName, 'partner' AS isPartner
			FROM tblCISOnlineAppPartnerSchools
			WHERE name LIKE $name
			ORDER BY univName
		";*/
	/*	$sql = "SELECT univ.id as univID, univ.name AS univName, 'usu' AS isPartner
			FROM tblGaUniversities_new AS univ LEFT JOIN 
			tblCISOnlineAppPartnerSchools AS partner ON partner.name = univ.name
			WHERE ISNULL(partner.name) AND univ.name LIKE $name
			UNION ALL			
			SELECT ID as univID, name AS univName, 'partner' AS isPartner
			FROM tblCISOnlineAppPartnerSchools
			WHERE name LIKE $name
			ORDER BY univName
		";
		*/
		
		
		$sql = "SELECT univ.id as univID, univ.name AS univName, partner.id as isPartner
				FROM tblGaUniversities_new AS univ LEFT JOIN 
				tblCISOnlineAppPartnerSchools_new AS partner ON partner.university_id = univ.id
				WHERE univ.name LIKE $name AND univ.name NOT LIKE 'Springfield College'
				ORDER BY univName";
		
		
		$rs = new iRecordset($conn->execute($sql));
		$list = array();
		foreach($rs as $row){
			$isPartner = (is_null($row['isPartner'])) ? 'usu' : 'partner'; 
			$list[] = $row['univName'].'|'.$row['univID'].'|'.$isPartner."\n";
		}
		return $list;
	}
	
	static public function getSchoolStateAbbrByStateID($stateID = 0){
		$conn = new dbHandler('db=GoAbroad_Main');
		$sql = "SELECT stateabbr FROM tbstate where stateID = ".$stateID." LIMIT 1";
		$rs = new iRecordset($conn->execute($sql));
		$stateAbbr = null;
		while(!$rs->EOF()){
			$stateAbbr = $rs->getStateabbr();
			$rs->moveNext();
		}
		
		return $stateAbbr;
	}
	
	public static function retrieveFromOldDB($id){
			$conn = new dbHandler('db=Travel_Logs');
		//	$sql = 'SELECT universityID, university FROM tbuniversity WHERE universityID = '.$id.' LIMIT 1';
			$sql = 'SELECT id, UniversityName FROM tblGaUniversities WHERE id = '.$id.' LIMIT 1';
			$rs = new iRecordset($conn->execQuery($sql));
			$Options = array();
			while(!$rs->EOF()){
					$Option = new USUniversities();
				//	$Option->setID        ( $rs->getUniversityID()   );
				//	$Option->setName      ( $rs->getUniversity()     );
					$Option->setID        ( $rs->getId()             );
					$Option->setName      ( $rs->getName() );	
					$Option->setStateID   ( 0 );

					$Options[] = $Option;

				$rs->moveNext();
			}
			return $Options[0];
	}
	
	public static function retrieveAll(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "SELECT * FROM tblGaUniversities_new ORDER BY id ASC";
		$rs = new iRecordset($conn->execQuery($sql));
		
		$universities = array();
		while(!$rs->EOF()){
			$university = new USUniversities();
			$university->setID        ( $rs->getId()      );
			$university->setName      ( $rs->getName()    );	
			$university->setStateID   ( $rs->getStateID() );
			$universities[] = $university;
			$rs->moveNext();
		}
		
		return $universities;
	}
	
	public static function retrieveByName($name){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "SELECT * FROM tblGaUniversities_new WHERE name LIKE '".$name."' LIMIT 1";
	//	echo $sql;
		$rs = new iRecordset($conn->execQuery($sql));
	//	var_dump($rs); exit;
		$university = NULL;
		while(!$rs->EOF()){
			$university = new USUniversities();
			$university->setID        ( $rs->getId()      );
			$university->setName      ( $rs->getName()    );	
			$university->setStateID   ( $rs->getStateID() );
			$rs->moveNext();
		}
		
		return $university;
	}
}
?>