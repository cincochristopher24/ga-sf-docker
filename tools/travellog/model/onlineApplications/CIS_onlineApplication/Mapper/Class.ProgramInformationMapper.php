<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.ProgramInformation.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class ProgramInformationMapper {
	
	public static function populateObjects(iRecordset $rs){
		$programInfos = array();
		while(!$rs->EOF()){
			$programInfo = new ProgramInformation();
			$programInfo->setID             ( $rs->getId()             );
			$programInfo->setPersonalInfoID ( $rs->getPersonalInfoID() );
			$programInfo->setTerm           ( $rs->getTerm()           );
			$programInfo->setYear           ( $rs->getYear()           );
			$programInfo->setFirstProgramID ( $rs->getFirstProgramID()   );
			$programInfo->setFirstOptionID  ( $rs->getFirstOptionID()    );
			$programInfo->setSecondProgramID( $rs->getSecondProgramID()  );
			$programInfo->setSecondOptionID ( $rs->getSecondOptionID()   );
			$programInfo->setPriorityOption ( $rs->getPriorityOption()   );
			$programInfo->setInterest       ( $rs->getInterest()       );
			$programInfo->setVolunteering   ( $rs->getVolunteering()   );
			
			$programInfo->setHousingOptionID   ( $rs->getHousing_options_id()   );
			$programInfo->setDurationOptionID   ( $rs->getDuration_id ()   );
			$programInfo->setStartMonth   ( $rs->getStartMonth()   );
			$programInfo->setStartDay   ( $rs->getStartDay()   );
			
			$programInfos[] = $programInfo;
			$rs->moveNext();
		}
		return $programInfos;
	}
	
	public static function populateObject(iRecordset $rs){
		$programInfo = self::populateObjects($rs);
		return $programInfo[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppProgramInfo WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$programInfoObj = self::populateObject($rs);
			return $programInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppProgramInfo WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$programInfoObjs = self::populateObjects($rs);
			return $programInfoObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
	//	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.PersonalInfoToProgramMapper.php');
	//	$infoToProgObj = PersonalInfoToProgramMapper::retrieveByPersonalInfoID($personalInfoID);
	//	return self::retrieveByPk($infoToProgObj[0]->getProgramInfoID());
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppProgramInfo WHERE PersonalInfoID = '.$personalInfoID;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$programInfoObjs = self::populateObject($rs);
			return $programInfoObjs;
		}
		
		return null;
	}
}
?>