<?php

require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.OnlineAppUser.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.AgreementMapper.php');
class OnlineAppUserPeer {
	
	public static function populateObjects(iRecordset $rs){
		
		$onlineAppUsers = array();
		while(!$rs->EOF()){
			$onlineAppUser = new OnlineAppUser();
		//	var_dump($term);
			$onlineAppUser->setID       ( $rs->getId()       );
			$onlineAppUser->setEmail    ( $rs->getEmail()    );
			$onlineAppUser->setPassword ( $rs->getPassword() );
			$onlineAppUsers[] = $onlineAppUser;
			$rs->moveNext();
		}
		return $onlineAppUsers;
	}
	
	public static function populateObject(iRecordset $rs){
		$onlineAppUser = self::populateObjects($rs);
		return $onlineAppUser[0];
	}
	
	public static function retrieveAllUserWithInfo(){
		$conn = new dbHandler('db=GoAbroad_Main');
		$sql = '
			SELECT user . * , info.FirstName, info.LastName
			FROM tblCISOnlineAppUsers AS user
			LEFT JOIN tblCISOnlineAppPersonalInfo AS info ON info.OnlineAppUserID = user.id
			ORDER BY info.id DESC
		';
		$rs = new iRecordset($conn->execQuery($sql));
		
		$list = array();
		while(!$rs->EOF()){
			$list[] = array('FirstName' => $rs->getFirstName(),
							'LastName'  => $rs->getLastName(),
							'Username'  => $rs->getEmail(),
							
							);
			$rs->moveNext();
		}
	}
	
	public static function retrieveUserByEmail($email){
		$conn = new dbHandler('db=Travel_Logs');
	//	$sql = 'SELECT * from tblCISOnlineAppUsers WHERE Email LIKE '.$conn->makeSqlSafeString($email).' LIMIT 1';
		$sql = '
			SELECT user.*, info.FirstName, info.LastName from tblCISOnlineAppUsers as user, tblCISOnlineAppPersonalInfo as info
			WHERE user.Email LIKE '.$conn->makeSqlSafeString($email).' AND info.OnlineAppUserID = user.id
			order by info.id DESC limit 1
		';
		$rs = new iRecordset($conn->execQuery($sql));
		
		if($rs->retrieveRecordCount() > 0){
		//	$user = self::populateObject($rs);
			$user = new OnlineAppUser();
			$user->setID       ( $rs->getId()       );
			$user->setEmail    ( $rs->getEmail()    );
			$user->setPassword ( $rs->getPassword() );
			$user->setFirstname( $rs->getFirstName());
			$user->setLastName ( $rs->getLastName() );
			
			return $user;
			
		}
		
		return null;
	}
	
	
	// this function is used in the admin side of the CIS Online Application
	public static function retrieveUsersInfo($lowerLimit = 0){
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, b.ApplicationDate, c.Email, c.Password
				FROM tblCISOnlineAppPersonalInfo AS info
				LEFT JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
				LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
				WHERE info.FirstName NOT LIKE '' AND info.LastName NOT LIKE ''
				ORDER BY info.id DESC limit $lowerLimit, 20";
		
		$rs = new iRecordset($conn->execQuery($sql));
		$rsCount = new iRecordset($conn->execQuery('SELECT FOUND_ROWS() as totalRecord'));
		
		$usersList = array();
		if($rs->retrieveRecordCount() > 0){
			$usersList = self::populateUsersList($rs);
		//	return $usersList;
		}
		
		$args = array('usersList' => $usersList , 'totalRecord' => $rsCount->getTotalRecord());
		return $args;
		
	}
	/** 
	 DEPRECATED FUNCTIONS 
	 **/
/*	public static function retrieveUserInfoByName($name){
		$conn = new dbHandler('db=Travel_Logs');
		$name = $conn->makeSqlSafeString('%'.$name.'%');
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, b.ApplicationDate, c.Email, c.Password
			FROM tblCISOnlineAppPersonalInfo AS info
			LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
			LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
			WHERE concat( info.FirstName,  ' ', info.LastName )
			LIKE  $name
			";
		$rs = new iRecordset($conn->execQuery($sql));
		$rsCount = new iRecordset($conn->execQuery('SELECT FOUND_ROWS() as totalRecord'));
		
		$usersList = array();
		if($rs->retrieveRecordCount() > 0){
			$usersList = self::populateUsersList($rs);
			
		}
		$args = array('usersList' => $usersList , 'totalRecord' => $rsCount->getTotalRecord());
		return $args;
	}
	
	public static function retrieveUserInfoByEmail($email){
		$conn = new dbHandler('db=Travel_Logs');
		$email = $conn->makeSqlSafeString('%'.$email.'%');
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, b.ApplicationDate, c.Email, c.Password
			FROM tblCISOnlineAppPersonalInfo AS info
			LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
			LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
			WHERE c.Email
			LIKE  $email
			";
		$rs = new iRecordset($conn->execQuery($sql));
		$rsCount = new iRecordset($conn->execQuery('SELECT FOUND_ROWS() as totalRecord'));
		
		$usersList = array();
		if($rs->retrieveRecordCount() > 0){
			$usersList = self::populateUsersList($rs);
			
		}
		$args = array('usersList' => $usersList , 'totalRecord' => $rsCount->getTotalRecord());
		return $args;
	 
	
	public static function retrieveFinishedApplication($lowerLimit){
		$conn = new dbHandler('db=Travel_Logs');
		$email = $conn->makeSqlSafeString('%'.$email.'%');
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, b.ApplicationDate, c.Email, c.Password
			FROM tblCISOnlineAppPersonalInfo AS info
			LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
			LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
			WHERE info.DoneProcess = 1
			ORDER BY info.id DESC limit $lowerLimit, 20
			";
		$rs = new iRecordset($conn->execQuery($sql));
		$rsCount = new iRecordset($conn->execQuery('SELECT FOUND_ROWS() as totalRecord'));
		
		$usersList = array();
		if($rs->retrieveRecordCount() > 0){
			$usersList = self::populateUsersList($rs);
			
		}
		$args = array('usersList' => $usersList , 'totalRecord' => $rsCount->getTotalRecord());
		return $args;
	}
	
	public static function retrieveUnFinishedApplication($lowerLimit){
		$conn = new dbHandler('db=Travel_Logs');
		$email = $conn->makeSqlSafeString('%'.$email.'%');
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, b.ApplicationDate, c.Email, c.Password
			FROM tblCISOnlineAppPersonalInfo AS info
			LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
			LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
			WHERE info.DoneProcess = 0
			ORDER BY info.id DESC limit $lowerLimit, 20
			";
		$rs = new iRecordset($conn->execQuery($sql));
		$rsCount = new iRecordset($conn->execQuery('SELECT FOUND_ROWS() as totalRecord'));
		
		$usersList = array();
		if($rs->retrieveRecordCount() > 0){
			$usersList = self::populateUsersList($rs);
			
		}
		$args = array('usersList' => $usersList , 'totalRecord' => $rsCount->getTotalRecord());
		return $args;
	}
	/** 
	 END DEPRECATED FUNCTIONS 
	 **/
	public static function retrieveUserInfoWithFilter(array $param){
		if(empty($param)){
			return false;
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "";
		$filter = $param['filter'];
		$lowerLimit = $param['lowerLimit'];
		
		if(isset($param['email'])){
			$email = $conn->makeSqlSafeString('%'.$param['email'].'%');
			
			$sql = "
				SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, info.dateAdded, b.ApplicationDate, c.Email, c.Password
				FROM tblCISOnlineAppPersonalInfo AS info
				LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
				LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
				WHERE c.Email
				LIKE  $email
				AND info.DoneProcess = $filter
				ORDER BY info.id DESC limit $lowerLimit, 20
				";
		}
		else if(isset($param['name'])){
			$name = $conn->makeSqlSafeString('%'.$param['name'].'%');
			$sql = "
				SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, info.dateAdded, b.ApplicationDate, c.Email, c.Password
				FROM tblCISOnlineAppPersonalInfo AS info
				LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
				LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
				WHERE concat( info.FirstName,  ' ', info.LastName )
				LIKE $name
				AND info.DoneProcess = $filter
				ORDER BY info.id DESC limit $lowerLimit, 20
				";
		}
	//	echo $sql;
		$rs = new iRecordset($conn->execQuery($sql));
		$rsCount = new iRecordset($conn->execQuery('SELECT FOUND_ROWS() as totalRecord'));
		
		$usersList = array();
		if($rs->retrieveRecordCount() > 0){
			$usersList = self::populateUsersList($rs);
			
		}
		$args = array('usersList' => $usersList , 'totalRecord' => $rsCount->getTotalRecord());
		return $args;
		
	}
	
	public static function retrieveUserInfoWithoutFilter(array $param){
		if(empty($param)){
			return false;
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = "";
		$lowerLimit = $param['lowerLimit'];
		
		if(isset($param['email'])){
			$email = $conn->makeSqlSafeString('%'.$param['email'].'%');
			$sql = "
				SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, info.dateAdded, b.ApplicationDate, c.Email, c.Password
				FROM tblCISOnlineAppPersonalInfo AS info
				LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
				LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
				WHERE info.FirstName != '' and info.LastName != ''
				AND c.Email
				LIKE  $email
				ORDER BY info.id DESC limit $lowerLimit, 20
				";
		}
		else if(isset($param['name']) and $param['name'] != ""){
			$name = $conn->makeSqlSafeString('%'.$param['name'].'%');
			$sql = "
				SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, info.dateAdded, b.ApplicationDate, c.Email, c.Password
				FROM tblCISOnlineAppPersonalInfo AS info
				LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
				LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
				WHERE info.FirstName != '' and info.LastName != ''
				AND concat( info.FirstName,  ' ', info.LastName )
				LIKE  $name
				ORDER BY info.id DESC limit $lowerLimit, 20
				";
		}
		else {
			$sql = "
				SELECT SQL_CALC_FOUND_ROWS info.id, info.OnlineAppUserID, info.FirstName, info.LastName, info.DoneProcess, info.dateSubmitted, info.dateAdded, b.ApplicationDate, c.Email, c.Password
				FROM tblCISOnlineAppPersonalInfo AS info
				LEFT  JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
				LEFT JOIN tblCISOnlineAppAgreement AS b ON b.PersonalInfoID = info.id
				WHERE info.FirstName != '' and info.LastName != ''
				ORDER BY info.id DESC limit $lowerLimit, 20
				";
		}
		
		$rs = new iRecordset($conn->execQuery($sql));
		$rsCount = new iRecordset($conn->execQuery('SELECT FOUND_ROWS() as totalRecord'));
		
		$usersList = array();
		if($rs->retrieveRecordCount() > 0){
			$usersList = self::populateUsersList($rs);
			
		}
		$args = array('usersList' => $usersList , 'totalRecord' => $rsCount->getTotalRecord());
		return $args;
	}
	
	public static function populateUsersList(iRecordset $rs){
		$usersList = array();
		
		while(!$rs->EOF()){
			$user = new OnlineAppUser();
			$user->setPersonalInfoID($rs->getOnlineAppUserID());
			$user->setID       ( $rs->getId()       );
			$user->setEmail    ( $rs->getEmail()    );
			$user->setPassword ( $rs->getPassword() );
			$user->setFirstname( $rs->getFirstName());
			$user->setLastName ( $rs->getLastName() );
			$user->setIsDone   ( $rs->getDoneProcess() );
			$user->setDateSubmitted( $rs->getDateSubmitted() );
			// $user->setDateApplied( $date = (null != $rs->getApplicationDate()) ? $rs->getApplicationDate() : $rs->getDateAdded() );
			$user->setDateApplied( $rs->getDateAdded() );
		//	$user->setDateApplied( AgreementMapper::retrieveApplicationDateByGenID($rs->getOnlineAppUserID()) );
			
			$usersList[] = $user;
			
			$rs->moveNext();
		}
		return $usersList;
	}
}