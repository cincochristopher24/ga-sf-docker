<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.PassportDetails.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class PassportDetailsMapper {
	
	
	public static function populateObjects(iRecordset $rs){
		$PassportDetails = array();
		while(!$rs->EOF()){
			$PassportDetail = new PassportDetails();
			$PassportDetail->setID               ( $rs->getId()               );
			$PassportDetail->setPersonalInfoID   ( $rs->getPersonalInfoID()   );
			$PassportDetail->setPassportNumber	 ( $rs->getPassportNumber()   );
			$PassportDetail->setExpiryDate       ( $rs->getExpiryDate() );
			
			$PassportDetails[] = $PassportDetail;
			$rs->moveNext();
		}
		return $PassportDetails;
	}
	
	public static function populateObject(iRecordset $rs){
		$PassportDetail = self::populateObjects($rs);
		return $PassportDetail[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPassportDetails WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$PassportDetail = self::populateObject($rs);
			return $PassportDetail;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPassportDetails WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$PassportDetails = self::populateObjects($rs);
			return $PassportDetails;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppPassportDetails WHERE PersonalInfoID = '.$personalInfoID.' limit 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$PassportDetail = self::populateObject($rs);
			return $PassportDetail;
		}
		
		return null;
	}
}
?>