<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.AddressInfo.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class AddressInfoMapper {
	
	public static function populateObjects(iRecordset $rs){
		$addressInfos = array();
		while(!$rs->EOF()){
			$addressInfo = new AddressInfo();
			$addressInfo->setID               ( $rs->getId()               );
			$addressInfo->setPersonalInfoID   ( $rs->getPersonalInfoID()   );
			$addressInfo->setAddressType      ( $rs->getAddressType()      );
			$addressInfo->setStreet1          ( $rs->getStreet1()          );
			$addressInfo->setCity1            ( $rs->getCity1()            );
			$addressInfo->setState1           ( $rs->getState1()           );
			$addressInfo->setZip1             ( $rs->getZip1()             );
			$addressInfo->setPhoneNumber1     ( $rs->getPhoneNumber1()     );
			$addressInfo->setCellphone        ( $rs->getCellphone()        );
			$addressInfo->setStreet2          ( $rs->getStreet2()          );
			$addressInfo->setCity2            ( $rs->getCity2()            );
			$addressInfo->setState2           ( $rs->getState2()           );
			$addressInfo->setZip2             ( $rs->getZip2()             );
			$addressInfo->setPhoneNumber2     ( $rs->getPhoneNumber2()     );
			$addressInfo->setPermanentAddress ( $rs->getPermanentAddress() );
			$addressInfo->setValidUntil       ( $rs->getValidUntil()       );
			
			
			$addressInfos[] = $addressInfo;
			$rs->moveNext();
		}
		return $addressInfos;
	}
	
	public static function populateObject(iRecordset $rs){
		$addressInfo = self::populateObjects($rs);
		return $addressInfo[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAddressInfo WHERE id = '.$id.' LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$addressInfoObj = self::populateObject($rs);
			return $addressInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAddressInfo WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$addressInfoObjs = self::populateObjects($rs);
			return $addressInfoObjs;
		}
		
		return null;
	}
	
	public static function retrieveAll(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAddressInfo';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$addressInfoObjs = self::populateObjects($rs);
			return $addressInfoObjs;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoID($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAddressInfo WHERE PersonalInfoID = '.$personalInfoID;
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$addressInfoObj = self::populateObjects($rs);
			return $addressInfoObj;
		}
		
		return null;
	}
	
	public static function retrieveByPersonalInfoIDWithType($personalInfoID){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppAddressInfo WHERE PersonalInfoID = '.$personalInfoID.' AND AddressType = 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$addressInfoObj = self::populateObject($rs);
			return $addressInfoObj;
		}
		
		return null;
	}
}
?>