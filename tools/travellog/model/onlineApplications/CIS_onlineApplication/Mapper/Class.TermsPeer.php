<?php
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.Terms.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
class TermsPeer {
	
	public static function populateObjects(iRecordset $rs){
		$terms = array();
		while(!$rs->EOF()){
			$term = new Terms();
		//	var_dump($term);
			$term->setID   ( $rs->getId()   );
			$term->setName ( $rs->getTerm() );
			
			$terms[] = $term;
			$rs->moveNext();
		}
		return $terms;
	}
	
	public static function populateObject(iRecordset $rs){
		$term = self::populateObjects($rs);
		return $term[0];
	}
	
	public static function retrieveByPk($id){
		$conn = new dbHandler('db=Travel_Logs');
		
		$sql = 'SELECT * FROM tblCISOnlineAppTerms WHERE id = '.$id.' AND deferred = 0 LIMIT 1';
	//	$sql = 'SELECT * FROM tblCISOnlineAppTerms_new WHERE id = '.$id.' AND deferred = 0 LIMIT 1';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$term = self::populateObject($rs);
			return $term;
		}
		
		return null;
	}
	
	public static function retrieveByPks(array $ids){
		if(empty($ids)){
			throw new Exception("Incorrect parameter passed to function retrieveByPks!");
		}
		
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppTerms WHERE id IN ('.implode(",",$ids).') AND deferred = 0';
	//	$sql = 'SELECT * FROM tblCISOnlineAppTerms_new WHERE id IN ('.implode(",",$ids).')';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$terms = self::populateObjects($rs);
			return $terms;
		}
		
		return null;
	}
	
	public static function retrieveAll(){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppTerms WHERE deferred = 0';
	//	$sql = 'SELECT * FROM tblCISOnlineAppTerms_new';
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$terms = self::populateObjects($rs);
			return $terms;
		}
		
		return null;
	}
	
	public static function retrieveByName($term_name){
		$conn = new dbHandler('db=Travel_Logs');
		$sql = 'SELECT * FROM tblCISOnlineAppTerms WHERE `Term` LIKE "'.$term_name.'" AND `deferred` = 0 LIMIT 1';
		
		$rs = new iRecordset($conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			$terms = self::populateObject($rs);
			return $terms;
		}
		
		return null;
	}
}
?>