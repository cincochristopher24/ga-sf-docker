<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
require_once('Class.iRecordset.php');

class PassportDetails {
	
	
	protected 	$id,
				$personalInfoID,
				$passportNumber,
				$expiryDate;
				
	protected 	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function setValues($values = array()){
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$passportDetails)){
				$functionName = 'set'.$arrKey;
				$this->$functionName($value);
			}
		}
		
		
	}
	
	// #### SAVE TO DATABASE ####
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppPassportDetails SET
					PersonalInfoID  ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'PassportNumber ='.ToolMan::makeSqlSafeString( $this->getPassportNumber() ).', '.
					'ExpiryDate     ='.ToolMan::makeSqlSafeString( $this->getExpiryDate()     );
		/*
		'ON DUPLICATE KEY UPDATE '.
		 	'PassportNumber ='.ToolMan::makeSqlSafeString( $this->getPassportNumber() ).', '.
			'ExpiryDate     ='.ToolMan::makeSqlSafeString( $this->getExpiryDate()     )
		*/
		$this->conn->execQuery($sql);
	//	var_dump($sql);
	}
	
	public function update(){
		$sql = 'INSERT INTO tblCISOnlineAppPassportDetails SET
					PersonalInfoID  ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'PassportNumber ='.ToolMan::makeSqlSafeString( $this->getPassportNumber() ).', '.
					'ExpiryDate     ='.ToolMan::makeSqlSafeString( $this->getExpiryDate()     ). ' '.
				'ON duplicate KEY UPDATE '.
					'PassportNumber ='.ToolMan::makeSqlSafeString( $this->getPassportNumber() ).', '.
					'ExpiryDate     ='.ToolMan::makeSqlSafeString( $this->getExpiryDate()     );
		$this->conn->execQuery($sql);		
		
	}
	
	// #### SETTERS #####
	public function setID($v){
		$this->id = $v;
	}
	
	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}
	
	public function setPassportNumber($v){
		$this->passportNumber = $v;
	}
	
	public function setExpiryDate($v){
		$this->expiryDate = $v;
	}
	
	// #### GETTERS #####
	public function getID(){
		return $this->id;
	}
	
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	
	public function getPassportNumber(){
		return $this->passportNumber;
	}
	
	public function getExpiryDate(){
		return $this->expiryDate;
	}
	
	
}
?>