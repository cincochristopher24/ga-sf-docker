<?php

require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class PersonalStatement {
	
	protected 	$id,
				$personalInfoID,
				$statement1,
				$statement2,
				$statement3,
				$lateOption = '0';
	
	protected $conn;
	
	private static $fieldMap = array('PersonalInfoID', 'Statement1', 'Statement2', 'Statement3');
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function setValues($values = array()){
		foreach($values as $arrKey => $value){
			if(in_array($arrKey,CISOnlineAppFieldsMapper::$personalStatementFieldMap)){
				$functionName = 'set'.$arrKey;
				$this->$functionName($value);
			}
		}
		
		
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppPersonalStatement SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'Statement1    ='.ToolMan::makeSqlSafeString( $this->getStatement1()     ).', '.
					'Statement2    ='.ToolMan::makeSqlSafeString( $this->getStatement2()     ).', '.
					'Statement3    ='.ToolMan::makeSqlSafeString( $this->getStatement3()     ).', '.
					'lateOption    ='.ToolMan::makeSqlSafeString( $this->getLateOption()     );
		$this->conn->execQuery($sql);			
	//	var_dump($sql);
	}
	
	
	public function update(){
		$sql = 'INSERT INTO tblCISOnlineAppPersonalStatement SET
					PersonalInfoID ='.ToolMan::makeSqlSafeString( $this->getPersonalInfoID() ).', '.
					'Statement1    ='.ToolMan::makeSqlSafeString( $this->getStatement1()     ).', '.
					'Statement2    ='.ToolMan::makeSqlSafeString( $this->getStatement2()     ).', '.
					'Statement3    ='.ToolMan::makeSqlSafeString( $this->getStatement3()     ).', '.
					'lateOption    ='.ToolMan::makeSqlSafeString( $this->getLateOption()     ).' '.
				'ON duplicate KEY UPDATE '.
					'Statement1    ='.ToolMan::makeSqlSafeString( $this->getStatement1()     ).', '.
					'Statement2    ='.ToolMan::makeSqlSafeString( $this->getStatement2()     ).', '.
					'Statement3    ='.ToolMan::makeSqlSafeString( $this->getStatement3()     ).', '.
					'lateOption    ='.ToolMan::makeSqlSafeString( $this->getLateOption()     );
		
		$this->conn->execQuery($sql);
	}
	
	// #### SETTERS ####
	public function setID($v){
		$this->id = $v;
	}
	
	public function setPersonalInfoID($v){
		$this->personalInfoID = $v;
	}
	
	public function setStatement1($v){
		$this->statement1 = $v;
	}
	
	public function setStatement2($v){
		$this->statement2 = $v;
	}
	
	public function setStatement3($v){
		$this->statement3 = $v;
	}
	
	public function setLateOption($v){
		$this->lateOption = $v;
	}
	
	// #### GETTERS ####
	public function getID(){
		return $this->id;
	}
	
	public function getPersonalInfoID(){
		return $this->personalInfoID;
	}
	
	public function getStatement1(){
		return $this->statement1;
	}
	
	public function getStatement2(){
		return $this->statement2;
	}
	
	public function getStatement3(){
		return $this->statement3;
	}
	
	public function getLateOption(){
		return $this->lateOption;
	}
	
	
	
}
?>