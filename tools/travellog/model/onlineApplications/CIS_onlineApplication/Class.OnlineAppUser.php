<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');
require_once('Class.iRecordset.php');

class OnlineAppUser {
	
	protected	$id,
				$pID,
				$email,
				$password,
				$fName,
				$lName,
				$isDone,
				$dateSubmitted,
				$dateApplied;
				
	protected 	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppUsers SET
					Email     = '.ToolMan::makeSqlSafeString( $this->getEmail()    ).', '.
					'Password = '.ToolMan::makeSqlSafeString( $this->getPassword() );
		$this->conn->execQuery($sql);
		$this->setID($this->conn->getLastInsertedID());
	}
	
	// setters
	public function setID($v){
		$this->id = $v;
	}
	
	public function setEmail($v){
		$this->email = $v;
	}
	
	public function setPassword($v){
		$this->password = $v;
	}
	
	public function setPersonalInfoID($v){ $this->pID = $v; }
	public function setLastName($v){ $this->lName = $v; }
	public function setFirstname($v){ $this->fName = $v; }
	public function setIsDone($v){ $this->isDone = $v; }
	public function setDateSubmitted($v){ $this->dateSubmitted = $v; }
	public function setDateApplied($v){ $this->dateApplied = $v; }
	
	// getters
	public function getID(){
		return $this->id;
	}
	
	public function getEmail(){
		return $this->email;
	}
	
	public function getPassword(){
		return $this->password;
	}
	
	public function getPersonalInfoID(){ return $this->pID; }
	public function getLastName(){ return trim($this->lName); }
	public function getFirstName(){ return trim($this->fName); }
	
	public function getName(){
		$name = trim($this->fName).' '.trim($this->lName);
		return $name;
	}
	
	public function getIsDone(){ return $this->isDone; }
	public function getDateSubmitted(){ return $this->dateSubmitted; }
	public function getDateApplied(){ return $this->dateApplied; }
	
	public function validateEmailPassword($email, $password){
		$email = trim($email);
		$password = trim($password);
		$sql = 'SELECT id FROM tblCISOnlineAppUsers WHERE
					TRIM(`Email`) = '.ToolMan::makeSqlSafeString( $email ).' '.
					'AND TRIM(`Password`) = '.ToolMan::makeSqlSafeString( $password );
	//	echo $sql;
		$rs = new iRecordset($this->conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			return $rs->getId();
		}
		
		return FALSE;
		
	}
	
	public function isAlreadyUser($email){
		$email = trim($email);
		$sql = 'SELECT id FROM tblCISOnlineAppUsers WHERE
					TRIM(`Email`) = '.ToolMan::makeSqlSafeString( $email );
		
		$rs = new iRecordset($this->conn->execQuery($sql));
		if($rs->retrieveRecordCount() > 0){
			return $rs->getId();
		}
		
		return FALSE;
	}
}
?>