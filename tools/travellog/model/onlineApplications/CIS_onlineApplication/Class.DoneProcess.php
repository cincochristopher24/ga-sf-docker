<?php

require_once('Class.dbHandler.php');
require_once('Class.ToolMan.php');

class DoneProcess {
	
	protected	$id,
				$processID,
				$conn;
				
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	// setters
	public function setID($v)       { $this->id        = $v; }
	public function setProcessID($v){ $this->processID = $v; }
	
	// getters
	public function getID()       { return $this->id;        }
	public function getProcessID(){ return $this->processID; }
	
	// save
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppDoneApplication SET
					applicationID = '.ToolMan::makeSqlSafeString( $this->getProcessID() );
					
		$this->conn->execQuery($sql);
	}
}
?>