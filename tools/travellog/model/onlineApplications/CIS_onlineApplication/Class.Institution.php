<?php

class Institution {
	
	protected	$id,
				$name;
				
	protected	$conn;
	
	public function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function save(){
		$sql = 'INSERT INTO tblCISOnlineAppInstitution SET
					Name = '.ToolMan::makeSqlSafeString( $this->getName() );
	}
	
	// setters
	public function setID($v){
		$this->id = $v;
	}
	
	public function setName($v){
		$this->name = $v;
	}
	
	// getters
	public function getID(){
		return $this->id;
	}
	
	public function getName(){
		return $this->name;
	}
}
?>