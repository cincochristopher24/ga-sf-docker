<?php



class USUniversities {
	
	protected	$id,
				$name,
				$stateID;
				
	public function __construct(){}
	
	// setters
	public function setID($v){ $this->id = $v; }
	public function setName($v){ $this->name = $v; }
	public function setStateID($v){ $this->stateID = $v; }
	// getters
	public function getID(){ return $this->id; }
	public function getName(){ return $this->name; }
	public function getStateID(){ return $this->stateID; }
}
?>