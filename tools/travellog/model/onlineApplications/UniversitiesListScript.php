<?php

require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
require_once('Class.ToolMan.php');


$conn = new dbHandler('db=Travel_Logs');
//$name = $conn->makeSqlSafeString('%'.$name.'%');
/*$sql = "
		SELECT university AS univName
		FROM GoAbroad_Main.tbuniversity AS univ LEFT JOIN 
		Travel_Logs.tblCISOnlineAppPartnerSchools AS partner ON partner.name = univ.university
		WHERE ISNULL(partner.name) AND NOT ISNULL(univ.university)
		GROUP BY univName
		UNION ALL			
		SELECT name AS univName
		FROM Travel_Logs.tblCISOnlineAppPartnerSchools	
		GROUP BY univName
		ORDER BY univName
";
*/


$sql = "
		SELECT univ.id AS univID, UniversityName AS univName,  'usu' AS isPartner
		FROM tblGaUniversities AS univ
		LEFT  JOIN tblCISOnlineAppPartnerSchools AS partner ON partner.name = univ.UniversityName
		WHERE ISNULL( partner.name ) AND  NOT ISNULL( univ.UniversityName )
		UNION  ALL 
		SELECT ID AS univID, name AS univName,  'partner' AS isPartner
		FROM tblCISOnlineAppPartnerSchools
		ORDER  BY univName
	";


$rs = new iRecordset($conn->execute($sql));

$list = array();
$cnt = 1;
foreach($rs as $row){
	$list[] = $cnt.'. '.$row['univName'];
	$cnt++;
}

$txtFile = 'univ_name_list.txt';

$filename = dirname(__FILE__).'/../../../../goabroad.net/onlineapplication/tmp/'.$txtFile;

$fileHandle = fopen($filename, 'w') or die("cannot open file");

$linebreak = chr(13).chr(10);

foreach ($list as $line){
	fwrite($fileHandle, $line.$linebreak);
}

//fwrite($fileHandle, $tmp);

fclose($fileHandle);

require_once('class.phpmailer.php');
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->IsHTML(false);
	$mail->From = "nash.lesigon@goabroad.com";
	
	$mail->FromName = "Nash";
	$mail->Subject = "Universities Name List";
	$mail->Body = "Here is the list of the university names...";
	
	$mail->Addattachment($filename, $txtFile);
	
	$mail->AddAddress("reynaldo.castellano@goabroad.com");
//	$mail->AddAddress("nash.lesigon@goabroad.com");
	$mail->AddBCC("nash.lesigon@goabroad.com");
//	$this->saveDoneApplication();
	if($mail->Send()){
		unlink($filename);
		echo "Script successfully run";
	}
	else{
		//var_dump($mail->smtp->error);
	}

echo 'done running script';
?>