<?php

$terms = array(
			'Summer' => 1,
			'Winter' => 2,
			'Fall'   => 3,
			'Spring' => 4,
			'Winter Trimester' => 5,
			'Semester 2' => 6
			);


$programs = array(
	'Bond University' => array($terms['Summer'] => array('May Semester',
														'Australian Experience: 4-week program',
														'Australian Experience: 6-week program',
														'Australian Experience: 4 and 6-week programs combined'),
							$terms['Fall'] => array(),
							$terms['Spring'] => array()),
	'LaTrobe University at Melbourne' => array($terms['Fall'] => array('Study Abroad', 'Adventure Education Program'),
	 											$terms['Spring'] => array('Study Abroad', 'Adventure Education Program')),
	'LaTrobe University at Bendigo'   => array($terms['Fall'] => array('Study Abroad', 'Adventure Education Program'),
												$terms['Spring'] => array('Study Abroad', 'Adventure Education Program')),
	'Edge of the Outback - Summer Photography Program' => array($terms['Summer'] => array() ),
	'Macquarie University' => array($terms['Fall'] => array('Study Abroad', 'Study Abroad and Internship'),
									$terms['Spring'] => array('Study Abroad', 'Study Abroad and Internship')),
	'University of Newcastle' => array($terms['Fall'] => array(),
										$terms['Spring'] => array()),
	'Swinburne University of Technology' => array($terms['Fall'] => array(),
													$terms['Spring'] => array()),
	'Melbourne Design Experience-Swinburne University of Technology' => array($terms['Summer'] => array()),
	'Melbourne Film Experience-Swinburne University of Technology' => array($terms['Summer'] => array()),
	'University of Nottingham' => array($terms['Fall'] => array(),
										$terms['Spring'] => array()),
	'Roehampton University' => array($terms['Fall'] => array(),
									$terms['Spring'] => array()),
	'University of Westminster' => array($terms['Summer'] => array('Session 1', 'Session 2', 'Session 1 and 2'),
										$terms['Fall'] => array('Study Abroad', 'Study Abroad and Internship'),
										$terms['Spring'] => array('Study Abroad', 'Study Abroad and Internship')),
	'Middlesex University' => array($terms['Summer'] => array('3 weeks - 4 credits', '3 weeks - 8 credits', '3 weeks - number of credits undecided',
																'6 weeks - 4 credits', '6 weeks - 8 credits', '6 weeks - number of credits undecided')),
	'London Summer Internship' => array($terms['Summer'] => array()),
	'Summer in the British Isles' => array($terms['Summer'] => array('4 weeks - England only', '6 weeks - England Ireland Scotland')),
	'Dublin City University' => array($terms['Fall'] => array(),
										$terms['Spring'] => array()),
	'University of Limerick' => array($terms['Summer'] => array(),
										$terms['Fall'] => array('Study Abroad', 'Rugby Study Abroad'),
										$terms['Spring'] => array()),
	'University of Dublin Trinity College' => array($terms['Fall'] => array(), $terms['Spring'] => array()),
	'Dublin Summer Internship' => array($terms['Summer'] => array()),
	'Institute at Palazzo Rucellai' => array($terms['Summer'] => array(),
											$terms['Fall'] => array('Study Abroad',
																	'Study Abroad - Architecture Program',
																	'Universita di Firenze'),
											$terms['Spring'] => array('Study Abroad',
																	'Study Abroad - Architecture Program',
																	'Universita di Firenze')),
	'The Umbra Institute' => array($terms['Summer'] => array('3 credits', '6 credits', 'number of credits undecided'),
									$terms['Fall'] => array('General Studies Program',
															'Intensive Italian Language and Culture Program',
															'Independent Research Program',
															'Universita degli Studi di Perugia',
															'Honors Program'),
									$terms['Spring'] =>	array('General Studies Program',
																'Intensive Italian Language and Culture Program',
																'Independent Research Program',
																'Universita degli Studi di Perugia',
																'Honors Program') ),
	'The Pantheon Institute' => array($terms['Summer'] => array('3 credits', '6 credits', 'number of credits undecided'),
										$terms['Fall'] => array('General Studies Program',
																'Architecture/Landscape Architecture Studies',
																'Independent Research Program'),
										$terms['Spring'] => array('General Studies Program',
																'Architecture/Landscape Architecture Studies',
																'Independent Research Program')),
	'University of Auckland' => array($terms['Winter'] => array(), $terms['Fall'] => array(), $terms['Spring'] => array()),
	'Victoria University of Wellington' => array($terms['Fall'] => array(),
												$terms['Spring'] => array('Study Abroad', 'Rugby Study Abroad')),
	'University of Otago' => array($terms['Winter'] => array(), $terms['Fall'] => array(), $terms['Spring'] => array()),
	'Edinburgh Napier University' => array($terms['Fall'] => array(), $terms['Spring'] => array()),
	'University of Stirling' => array($terms['Summer'] => array('Session 1 - 6 credits',
																'Session 2 = 6 credits',
																'Session 1 and 2 - 9 credits',
																'Session 1 and 2 - number of credits undecided'),
										$terms['Fall'] => array(), $terms['Spring'] => array()),
	'Suffolk University Madrid' => array($terms['Summer'] => array(),
											$terms['Fall'] => array('Study Abroad', 'Study Abroad and Internship'), 
											$terms['Spring'] => array('Study Abroad', 'Study Abroad and Internship')),
	'Universitat Autonoma de Barcelona' => array($terms['Summer'] => array(),
													$terms['Fall'] => array('Language Culture and International Studies - 12 credits',
																			'Language Culture and International Studies - 15 credits',
																			'Language Culture and International Studies - credits undecided',
																			'Spanish Immersion'),
													$terms['Spring'] =>	array('Language Culture and International Studies - 12 credits',
																			'Language Culture and International Studies - 15 credits',
																			'Language Culture and International Studies - credits undecided',
																			'Spanish Immersion')),
	'Spain Internship Madrid' => array($terms['Summer'] => array()),
	'Spain Internship Barcelona' => array($terms['Summer'] => array()),
	'Spain Internship Marbella' => array($terms['Summer'] => array()),
	'Spain Internship Seville' => array($terms['Summer'] => array()),
	'University of International Business and Economics' => array($terms['Fall'] => array(), $terms['Spring'] => array()),
	'Universidad Internacional' => array($terms['Summer'] => array('Session 1',
																	'Session 2',
																	'Session 1 and 2',
																	'Internship'),
										$terms['Winter'] => array('4 weeks', '5 weeks'),
										$terms['Fall'] => array('Intensive Spanish Language - 12 weeks',
																'Language and International Studies - 16 weeks',
																'Internship'),
										$terms['Spring'] =>	array('Intensive Spanish Language - 12 weeks',
																'Language and International Studies - 16 weeks',
																'Internship')),
	'Universidad Veritas' => array($terms['Summer'] => array('Spanish Language Intensive - June',
															'Spanish Language Intensive - July',
															'Spanish Language Intensive - June and July',
															'Spanish for Health Care',
															'Language and Culture Exploration',
															'Tropical Marine Biology',
															'Tropical Ecology and Resource Management',
															'Internship - May-June',
															'Internship - June-July',
															'Internship - July-August'),
	 								$terms['Winter'] => array('Spanish Language Intensive',
															'Tropical Marine Biology',
															'Tropical Ecology & Resource Management'),
									$terms['Fall'] => array('Spanish Language Intensive',
															'Language and Culture Exploration',
															'Direct Enroll',
															'Internship'),
									$terms['Spring']=> array('Spanish Language Intensive',
														'Language and Culture Exploration',
														'Direct Enroll',
														'Internship'),
									$terms['Winter Trimester']=> array('Spanish Language Intensive',
																	'Language and Culture Exploration',
																	'Direct Enroll',
																	'Internship') ),
	'Multi-country Summer' => array($terms['Summer'] => array('Business and Law- Ireland and England',
																'Media and Visual Arts- Ireland and England',
																'Politics and Prose- Scotland and England')),
	'Les Roches Switzerland' => array(),
	'Les Roches Marbella' => array(),
	'Glion Institute of Higher Education' => array(),
	
	
	);
	
	
//	exit;
	// 
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	
	
	$conn = new dbHandler('db=Travel_Logs');
	
//	echo "<br/>clearing tables";
	// clear tables before adding entries
	$clearTblProgSql = 'TRUNCATE TABLE tblCISOnlineAppPrograms';
	$conn->execQuery($clearTblProgSql);
	$clearTblOptionsSql = 'TRUNCATE TABLE tblCISOnlineAppOptions';
	$conn->execQuery($clearTblOptionsSql);
	$clearTblOptToTermSql = 'TRUNCATE TABLE tblCISOnlineAppOptionToTerm';
	$conn->execQuery($clearTblOptToTermSql);
	$clearTblProgToTermSql = 'TRUNCATE TABLE tblCISOnlineAppProgramToTerm';
	$conn->execQuery($clearTblProgToTermSql);
//	echo "<br/>done clearing tables";
	
	foreach($programs as $key => $values){
		$programSql = 'INSERT INTO tblCISOnlineAppPrograms SET Name = '.ToolMan::makeSqlSafeString($key);
		$conn->execQuery($programSql);
		$lastInsertProgramID = $conn->getLastInsertedID();
	//	echo "<p>".$programSql."</p>";
	//	echo "<em>last inserted PROGRAM ID = ".$lastInsertProgramID."</em>";
		foreach($values as $optKey => $optValues){
			$termVal = $optKey;
	//		var_dump($termVal);
			if(!empty($optValues)){
				foreach($optValues as $value){
					$optionSql = 'INSERT INTO tblCISOnlineAppOptions SET 
										ProgramID = '.ToolMan::makeSqlSafeString($lastInsertProgramID).', '. 
										'Name     = '.ToolMan::makeSqlSafeString($value);
					$conn->execQuery($optionSql);
					$lastInsertOptionID = $conn->getLastInsertedID();
					
			//		echo "<p>".$optionSql."</p>";
			//		echo "<em>last inserted OPTIONS ID = ".$lastInsertOptionID."</em>";
					
					$optionToTermSql = 'INSERT INTO tblCISOnlineAppOptionToTerm SET 
											OptionID = '.ToolMan::makeSqlSafeString($lastInsertOptionID).', '.
											'TermID  = '.ToolMan::makeSqlSafeString($termVal);
					$conn->execQuery($optionToTermSql);
					
			//		echo "<p>".$optionToTermSql."</p>";
				}
			}
			
			$programToTermSql = 'INSERT INTO tblCISOnlineAppProgramToTerm SET
									ProgramID = '.ToolMan::makeSqlSafeString($lastInsertProgramID).', '.
									'TermID  = '.ToolMan::makeSqlSafeString($termVal);
			$conn->execQuery($programToTermSql);

		//	echo "<p>".$programToTermSql."</p>";
		}
		
		
		
	}

echo "done running script";

?>