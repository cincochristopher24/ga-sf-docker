<?php
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');
require_once('Class.Template.php');
require_once('class.phpmailer.php');

echo 'start';
echo '';
$conn = new dbHandler('db=Travel_Logs');

// get the list of unfinished applications
//$sql = 'SELECT id, PersonalInfoID as procID, Step FROM tblCISOnlineAppStepsDone WHERE dateChecked = 0000-00-00';


$sql = "
	SELECT info.id AS procID, OnlineAppUserID, FirstName, LastName, steps.id as stepID, steps.Step as Step, c.Email
	FROM tblCISOnlineAppPersonalInfo AS info
	LEFT JOIN tblCISOnlineAppStepsDone AS steps ON steps.PersonalInfoID = info.id
	LEFT JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
	WHERE steps.dateChecked = 0000-00-00
";



$rs = new iRecordset($conn->execute($sql));

$unfinishedProcess = array();
$cnt = 0;
foreach($rs as $row){
	$unfinishedProcess[$cnt] = array('stepID' => $row['stepID'],
										'procID' => $row['procID'],
										'name' => $row['FirstName'].' '.$row['LastName'],
										'email' => $row['Email'],
										'step' => $row['Step']);
	$cnt++;
}

// get the previously generated unfinished applications
/*$sql_prevNotDone = "
	SELECT FirstName, LastName from tblCISOnlineAppPersonalInfo as info LEFT JOIN
	tblCISOnlineAppStepsDone as steps on steps.PersonalInfoID = info.id
	WHERE steps.dateChecked != 0000-00-00
";*/
$sql_prevNotDone = "
	SELECT OnlineAppUserID, FirstName, LastName, steps.id, c.Email
	FROM tblCISOnlineAppPersonalInfo AS info
	LEFT JOIN tblCISOnlineAppStepsDone AS steps ON steps.PersonalInfoID = info.id
	LEFT JOIN tblCISOnlineAppUsers AS c ON c.id = info.OnlineAppUserID
	WHERE steps.dateChecked !=0000-00-00
	";

$rsOld = new iRecordset($conn->execute($sql_prevNotDone));
$oldNames = array();
foreach($rsOld as $r){
	//$oldNames[] = array('name' => $r['FirstName'].' '.$r['LastName']);
	$oldNames[] = array('name' => $r['FirstName'].' '.$r['LastName'],
						'email' => $r['Email']);
}

//echo "unfinished - unchecked";
//var_dump($unfinishedProcess);
//echo "unfinished - checked";
//var_dump($oldNames);


$class_names = array('PersonalInfo','AddressInfo','PassportDetails',
					'EmergencyContact','SchoolInfo','AdditionalInfo','BillingInfo',
					'ProgramInformation','PersonalStatement','Agreement');

// this is the part where we require the models we are goin to use to retrieve the users data
$objArr = array();
foreach($class_names as $key => $cls){

		$clsName = 'Class.'.$cls.'Mapper.php';
		$file = dirname(__FILE__).'/CIS_onlineApplication/Mapper/'.$clsName;
		
		if(file_exists($file)){
			require_once($file);
			//$objArr[$cls] = call_user_func(array($cls.'Mapper', 'retrieveByPersonalInfoID'), $pID);
		}
}

$applicants = array();
$newArr = array(); // for testing variable only
$cnt = 0;
foreach($unfinishedProcess as $process){
	foreach($class_names as $key => $cls){
		$objArr[$cls] = call_user_func(array($cls.'Mapper', 'retrieveByPersonalInfoID'), $process['procID']);
	}
	
	if(!is_null($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO])){
		// get the email address
		
		$tpl = new Template();
		$tpl->setPath('travellog/views/onlineApplications/');
		$tpl->set('objArr', $objArr);
		$tpl->set('stepsDone', $process['step']);
		$applicants[]  = array('name' => $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getName(),
								'email' => $process['email']);
	//	$applicants['name'] = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getName();
	//	$applicants['email'] = $process['email'];
		$tmp[] = $tpl->fetch('tpl.dailyScript.php');

	//	var_dump('file created');
	}
	$cnt++;
	$sql = 'UPDATE tblCISOnlineAppStepsDone SET dateChecked = now() WHERE id = '.$process['stepID'];
//	var_dump($sql);
	$conn->execute($sql);
	
	
}

$date = Date('Y-m-d');
$txtFile = 'unfinished_applications_'.$date.'.txt';
$linebreak = chr(13).chr(10);

$filename = dirname(__FILE__).'/../../../../goabroad.net/onlineapplication/tmp/'.$txtFile;

$fileHandle = fopen($filename, 'w') or die("cannot open file");

foreach ($tmp as $line){
	fwrite($fileHandle, $line.$linebreak.$linebreak.$linebreak);
}

fclose($fileHandle);

if(!empty($tmp)){
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->IsHTML(false);
	//$mail->From = "nash.lesigon@goabroad.com";
	$mail->From = "aheddens@cisabroad.com";
	//$mail->From = "info@cisabroad.com";
	$mail->FromName = "Online Application";
	$mail->Subject = "Unfinished Applications - ".$date;
	
	$body = "The list of unfinished applications for $date:".$linebreak.$linebreak;
	$body = $body.'New : '.$linebreak;
	$cnt = 1;
	foreach($applicants as $applicant){
		$body = $body.$cnt.'. '.$applicant['name'].' - '.$applicant['email'].$linebreak;
		$cnt++;
	}
	$cntOld = 1;
	$body = $body.$linebreak.'Previous : '.$linebreak;
	foreach($oldNames as $old){
		$body = $body.$cntOld.'. '.$old['name'].' - '.$old['email'].$linebreak;
		$cntOld++;
	}
//	echo $body;
	$mail->Body = $body;
	
	$mail->Addattachment($filename, $txtFile);
//	$mail->AddAddress("nash.lesigon@goabroad.com");
	$mail->AddAddress("info@cisabroad.com");
//	$mail->AddAddress("reynaldo.castellano@goabroad.com");
	$mail->AddBCC("reynaldo.castellano@goabroad.com");
	$mail->AddBCC("nash.lesigon@goabroad.com");
	$mail->AddBCC("virna.alvero@goabroad.com");
//	$this->saveDoneApplication();
	if($mail->Send()){
		unlink($filename);
		echo "Script successfully run";
	}
	else{
		//var_dump($mail->smtp->error);
	}
}

echo "done";
?>