<?php
	/**
	 * @(#) Class.SubGroup.php
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - Jan. 30, 2009
	 */
	
	require_once("travellog/model/Class.Group.php");
	require_once("travellog/model/Class.CustomizedSubGroupCategory.php");
	require_once("Class.SqlResourceIterator.php");
	
	class SubGroup extends Group {
		/**
		 * constants
		 */
		const ACTIVE = 1;
		const INACTIVE = 2;
		/**
		 * Properties
		 */
		private $mCategory = null;
		private $mCategoryID = null;
		private $mCategorySubGroupRank = 0;
		
		/**
		 * Constructs a SubGroup object using an ID for the subgroup or array of values for the properties of the subgroup.
		 * 
		 * @param integer|array The ID of the subgroup or values for the properties of this subgroup.
		 */
		function __construct($param = 0){
			parent::__construct();
			
			if (is_numeric($param) AND 0 < $param) {
				try {
					$handler = new dbHandler();
					
					$sql = "SELECT a.* FROM tblGroup as a " .
						" WHERE a.groupID = ".$handler->makeSqlSafeString($param)." " .
						" AND a.discriminator = 2 " .
						" AND a.parentID > 0 ";
					$it = new SqlResourceIterator($handler->execute($sql));
					if ($it->hasNext()) {
						$this->initialize($it->next());
					}
				}
				catch(exception $ex){}
			}
			else if (is_array($param)) {
				$this->initialize($param);
			}
		}
		
		/**
		 * Returns the customized category of this subgroup.
		 * 
		 * @throws exception
		 * @return CustomizedSubGroupCategory|null Returns the customized category of this subgroup if it exists. If this has no customized category, this returns null.
		 */
		function getCategory(){
			if (is_null($this->mCategory) AND is_null($this->mCategoryID)) {
				try {
					$handler = new dbHandler();
	  			
	  			$sql = "SELECT a.* FROM tblSubGroupCategory as a, tblGroupToSubGroupCategory as b " .
		  		  " WHERE b.groupID = ".$handler->makeSqlSafeString($this->mGroupID)." " .
		  		  " AND a.categoryID = b.categoryID ";
	  		 
					$it = new SqlResourceIterator($handler->execute($sql));
					if ($it->hasNext()) {
						$this->mCategory = new CustomizedSubGroupCategory($it->next());
					}
	  		}
	  		catch(exception $ex){
	  			throw $ex;
	  		}
			}
			else if (!is_null($this->mCategoryID)) {
				require_once("travellog/model/Class.SubGroupCategoryRepository.php");
				
				$this->mCategory = SubGroupCategoryRepository::createSubGroupCategory($this->mCategoryID);
			}
			
			return $this->mCategory;
		}
		
		/**
		 * Returns the ID customized category of this subgroup.
		 * 
		 * @throws exception
		 * @return integer Returns the ID customized category of this subgroup. Returns -1 when this subgroup has no category.
		 */
		function getCategoryID(){
			if (is_null($this->mCategoryID)) {
				try {
					$handler = new dbHandler();
	  			
	  			$sql = "SELECT categoryID FROM tblGroupToSubGroupCategory" .
		  		  " WHERE groupID = ".$handler->makeSqlSafeString($this->mGroupID);
	  		 
					$it = new SqlResourceIterator($handler->execute($sql));
					
					$this->mCategoryID = (0 < $it->size()) ? $it->categoryID() : null;
	  		}
	  		catch(exception $ex){
	  			throw $ex;
	  		}
			}
			
			return (is_null($this->mCategoryID)) ? -1 : $this->mCategoryID;
		}
		
		/**
		 * Returns the ranking of this subgroup to the list of this subgroup's category groups.
		 * 
		 * @return integer Returns the ranking of this subgroup to the list of this subgroup's category groups. If this subgroup has no category this returns -1.
		 */
		function getCategorySubGroupRank(){
			return ($this->hasCategory()) ? $this->mCategorySubGroupRank : -1;
    }
		
		/**
		 * Returns the ranking of this subgroup to the list of this subgroup's parent group featured groups.
		 * 
		 * @return integer Returns the featured subgroup rank of this subgroup. If this subgroup is not featured this returns -1.
		 */
		function getFeaturedSubGroupRank(){
			return ($this->isFeatured()) ? $this->mFeaturedSubGroupRank : -1;
		}
		
		/**
	   * Returns the number of members of this subgroup.
	   * 
	   * @throws exception
	   * @return integer Returns the number of member of this subgroup.
	   */
	  function getMembersCount(SqlCriteria2 $criteria = null){
			try {
				$handler = new dbHandler();
				$sql = "SELECT tblTraveler.travelerID " .
					" FROM tblGrouptoTraveler, tblTraveler " .
					" WHERE tblTraveler.active  = 1 " .
					" AND tblTraveler.isSuspended = 0 " .
					" AND tblGrouptoTraveler.groupID = " . $handler->makeSqlSafeString($this->mGroupID)."" .
					" AND tblTraveler.travelerID NOT IN (SELECT DISTINCT(travelerID) FROM tblGrouptoAdvisor) " .
					" AND tblTraveler.travelerID NOT IN " .
					"		(" .
					"			SELECT DISTINCT(travelerID) FROM tblGrouptoFacilitator " .
					"			WHERE tblGrouptoFacilitator.status = 1 " .
					"			AND tblGrouptoFacilitator.type = 1" .
					"			AND tblGrouptoFacilitator.groupID = " .$handler->makeSqlSafeString($this->mGroupID).
					"		) " .
					" AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					" GROUP BY tblTraveler.travelerID ";
				
				$sql = "SELECT COUNT(*) as count FROM ($sql) as temp";
				
				$iterator = new SqlResourceIterator($handler->execute($sql));
				
				return $iterator->count();
			}
			catch (exception $ex){
				throw $ex;
			}
		}
		
		/**
		 * Returns the parent group of this subgroup.
		 * 
		 * @return AdminGroup Returns the parent group of this subgroup.
		 */
    function getParent(){
			return new AdminGroup($this->mParentID);
    }
		
		/**
		 * Returns the ID of the parent group of this subgroup.
		 * 
		 * @return integer Returns the ID of the parent group.
		 */
    function getParentID(){
			return $this->mParentID;
    }
    
		/**
		 * Returns the number of photos of this subgroup.
		 * 
		 * @throws exception
		 * @return integer Returns the number of photos of this subgroup.
		 */
	  function getAlbumPhotosCount(){
			try {
				$handler = new dbHandler();
				
				$sql = " SELECT count(tblPhotoAlbumtoPhoto.photoID) as count " .
					" FROM tblPhotoAlbum, tblPhotoAlbumtoPhoto " .
					" WHERE tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID" .
					" AND tblPhotoAlbum.groupID = ".$handler->makeSqlSafeString($this->mGroupID);
				
				$iterator = new SqlResourceIterator($handler->execute($sql));
				
				return $iterator->count();
			}
			catch (exception $ex) {
				throw $ex;
			}        
		}
		
		/**
		 * Returns the number of staff of this subgroup.
		 * 
		 * @throws exception
		 * @return integer Returns the number of staff of this subgroup.
		 */
		function getStaffCount(){
			try {
				$handler = new dbHandler();
				
				$sql = "SELECT count(tblGrouptoFacilitator.travelerID) as count " .
					" FROM tblGrouptoFacilitator, tblTraveler " .
					" WHERE tblGrouptoFacilitator.travelerID = tblTraveler.travelerID " .
					" AND tblTraveler.active = 1 " .
					" AND tblTraveler.isSuspended = 0 " .
					" AND tblGrouptoFacilitator.status = 1 " .
					" AND tblGrouptoFacilitator.groupID = " .$handler->makeSqlSafeString($this->mGroupID). " " .
					" AND type = ".AdminGroup::STAFF;
				
				$iterator = new SqlResourceIterator($handler->execute($sql));
				
				return $iterator->count();			
			}
			catch(exception $ex){
				throw $ex;
			}
		}
		
		/**
		 * Returns the ranking of this subgroup to the list of this subgroup's parent group groups.
		 * 
		 * @return integer Returns the subgroup rank of this subgroup.
		 */
		function getSubGroupRank(){
			return $this->mSubGroupRank;
		}
		
		/**
		 * Returns the number of travel journals related to this subgroup.
		 * 
		 * @return integer
		 */
		function getJournalsCount(){
			try {
				$handler = new dbHandler();
				
				$sql = "SELECT count(*) as count FROM tblGroupApprovedJournals, tblTravel " .
					" WHERE tblGroupApprovedJournals.groupID = ".$handler->makeSqlSafeString($this->mGroupID)."" .
					" AND tblGroupApprovedJournals.approved = 1 " .
					" AND tblGroupApprovedJournals.travelID = tblTravel.travelID " .
					"	AND tblTravel.deleted = 0";

				$resource = $handler->execute($sql);
				
				$record = mysql_fetch_assoc($resource);
				
				return $record['count'];
			}
			catch(exception $ex){
				throw $ex;
			}
		}
		
		/**
		 * Returns the number of journal entries related to this subgroup.
		 * 
		 * @return integer
		 * @author Augustianne Laurenne L. Barreta
		 */
		function getJournalEntriesCount(){
			try {
				$handler = new dbHandler();
				
				$sql = "SELECT count(distinct(tblTravelLog.travellogID)) as count
						FROM qryGroupTravel, tblTravelLog, tblTravel, tblGroupApprovedJournals, tblTraveler
						WHERE tblTravelLog.deleted = 0 
							AND tblTravelLog.travelID = tblTravel.travelID
							AND tblTravel.deleted = 0 
							AND tblTravel.travelerID = tblTraveler.travelerID
							AND tblTraveler.isSuspended = 0 
							AND tblTraveler.deactivated = 0
							AND qryGroupTravel.travelID = tblGroupApprovedJournals.travelID
							AND tblGroupApprovedJournals.travelID = tblTravel.travelID
							AND tblGroupApprovedJournals.groupID = {$handler->makeSqlSafeString($this->mGroupID)}
							AND tblGroupApprovedJournals.approved = 1";
						
				$resource = $handler->execute($sql);
				
				$record = mysql_fetch_assoc($resource);
				
				return $record['count'];
			}
			catch(exception $ex){
				throw $ex;
			}
		}
		
		/**
		 * Returns the number of subgroup's related videos.
		 * 
		 * @return integer Returns the number of subgroup's related videos.
		 */
		function getVideosCount(){
			try {
				$handler = new dbHandler();
				
				$sql = "SELECT count(c.videoID) as count FROM tblGroupApprovedJournals as a, tblTravelLog as b, tblTravelLogtoVideo as c " .
					" WHERE a.groupID = ".$handler->makeSqlSafeString($this->mGroupID)." " .
					" AND c.travellogID = b.travellogID " .
					" AND b.travelID = a.travelID ";
				
				$iterator = new SqlResourceIterator($handler->execute($sql));
				
				return $iterator->count();			
			}
			catch(exception $ex){
				throw $ex;
			}			
		}
		
		/**
		 * Returns the number of subgroup's related articles.
		 * 
		 * @return integer Returns the number of subgroup's related articles.
		 */
		function getArticlesCount(){
			require_once("travellog/model/Class.Article.php");
			return Article::getArticleCount($this->mGroupID);
		}
    
    /**
     * Returns true if this subgroup has a category. Otherwise this returns false.
     * 
     * @return boolean Returns true if this subgroup has a category. Otherwise this returns false.
     */
    function hasCategory(){
    	return (0 < $this->mCategoryID) ? true : false;
    }
		
		/**
		 * Initializes the properties of this subgroups.
		 * 
		 * @param array $props The properties of this subgroups. This must be an associative array with keys equal to tblGroup.
		 * @return void
		 */
		protected function initialize(array $props){
			if (0 < count($props)) {
	 			parent::initialize($props);
	 			$this->mDiscriminator = 2;
	 			$this->mParentID = isset($props['parentID']) ? $props['parentID'] : 0;
				$this->mSubGroupRank = isset($props['subgrouprank']) ? $props['subgrouprank']  : 0;
				$this->mFeaturedSubGroupRank = isset($props['featuredsubgrouprank']) ? $props['featuredsubgrouprank']  : 0;
				$this->mIsFeatured = isset($props['isfeatured']) ? $props['isfeatured'] : 0;
				$this->mCategoryID = isset($props['categoryID']) ? $props['categoryID'] : $this->mCategoryID;
				$this->mCategorySubGroupRank = isset($props['categorysubgrouprank']) ? $props['categorysubgrouprank'] : 0;
			}
		}
		
		/**
		 * Returns the status of this subgroup whether it is featured or not by its parent group.
		 * 
		 * @return boolean Returns true if this subgroup is a featured group of its parent group. Otherwise this returns false.
		 */
		function isFeatured(){
			return (1 == $this->mIsFeatured || 0 < $this->mFeaturedSubGroupRank) ? true : false;
		}
		
		/**
		 * Inserts a new entry or updates an entry in the database. If groupID is greater
		 * than 0, this updates an entry in the database. If groupID is less than or equal
		 * to 0, this inserts a new entry in the database.
		 * 
		 * @throws exception Throws exception when the connection to host failed.
		 * 
		 * @return void 
		 */
		function save(){
			
		}
		
		/**
		 * Sets the ID of the category of this subgroup.
		 * 
		 * @param integer $id The id of this subgroup customized category.
		 * @return void
		 */
		function setCategoryID($id){
			$this->mCategoryID = $id;
		}
		
		/**
		 * Sets the ranking of this subgroup to the list of this subgroup's parent group featured groups.
		 * 
		 * @param integer $rank The featured subgroup rank of this subgroup.
		 * @return void
		 */
		function setFeaturedSubGroupRank($rank){
			$this->mFeaturedSubGroupRank = $rank;
		}
		
		/**
		 * Sets the ID of this subgroup's parent group.
		 * 
		 * @param integer $id The ID of this subgroup's parent group.
		 * @return void
		 */
    function setParentID($id){
			$this->mParentID = $id;
    }
		
		/**
		 * Sets the ranking of this subgroup to the list of this subgroup's parent group groups.
		 * 
		 * @param integer $rank The ranking of this subgroup to the list of this subgroup's parent group groups.
		 * @return void
		 */
		function setSubGroupRank($rank){
			$this->mSubGroupRank = $rank;
    }
    
		/**
		 * Returns the ranking of this subgroup to the list of this subgroup's category groups.
		 * 
		 * @param integer $rank The ranking of this subgroup to the list of this subgroup's category groups.
		 * @return void
		 */
		function setCategorySubGroupRank($rank){
			$this->mCategorySubGroupRank = $rank;
    }
	}