<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: 
 */

    require_once("Class.Connection.php");
    require_once("Class.Recordset.php");
    
    class GroupCategory{
        
        /**
         * Define variables for Class Member Attributes
         */                
            private $mConn         	= NULL;
            private $mRs           	= NULL;
           
            private $mCategoryID   	= NULL;
            private $mName         	= NULL;
            private $mDiscriminator = NULL;
           
                       
        /**
         * Constructor Function for this class
         */
            function GroupCategory($categoryID = 0){
                
                $this->mCategoryID = $categoryID;
                
                try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mCategoryID){
                    
                    $sql = "SELECT * FROM tblGroupCategory WHERE categoryID = '$this->mCategoryID' ";
                    $this->mRs->Execute($sql);
                
                    if (0 == $this->mRs->Recordcount()){
                        throw new Exception("Invalid GroupCategoryID");        // ID not valid so throw exception
                    }
                    
                    // Sets a value to its class member attributes.
                    $this->mName     		= stripslashes($this->mRs->Result(0,"name"));
                    $this->mDiscriminator   = $this->mRs->Result(0,"discriminator");           
                  
                }
            }
                   
        /**
         * Setter Functions
         */   
            function setCategoryID($categoryID){
               $this->mCategoryID = $categoryID;
            }
            
            function setName($name){
               $this->mName = $name;
            }
            
            function setDiscriminator($discriminator){
                $this->mDiscriminator = $discriminator;
            }
           
        /**
         * Getter Functions
         */   
            function getCategoryID(){
               return $this->mCategoryID;
            }
            
            function getName(){
                return $this->mName;
            }
      		
      		function getDiscriminator(){
                return $this->mDiscriminator;
            }
        
        /**
         * CRUD Methods (Create, Update , Delete)
         */ 
            function Create(){
                
                $Aname  = addslashes($this->mName);
                
                $sql = "INSERT into tblGroupCategory (name, discriminator) VALUES ('$Aname', '$this->mDiscriminator')";
                $this->mRs->Execute($sql);       
                $this->mCategoryID = $this->mRs->GetCurrentID();
            }
            
            function Update(){
                
                $Aname = addslashes($this->mName);
                
                $sql = "UPDATE tblGroupCategory SET name = '$Aname' , discriminator = '$this->mDiscriminator' WHERE categoryID = '$this->mCategoryID' ";
                $this->mRs->Execute($sql);
            }
            
            function Delete(){
                
                $sql = "DELETE FROM tblGrouptoCategory WHERE categoryID = '$this->mCategoryID' ";        
                $this->mRs->Execute($sql);
               
            }       
         
         
         /**
		 * Static Function
         * Purpose: Returns an array of all existing group categories         
         */ 
	 		public static function getAllGroupCategories(){
	 			
	 			$grpCategories = array();
	 			
	 			try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
					
		 		$sql = "SELECT categoryID FROM tblGroupCategory " ;
		 		$rs->Execute($sql);
		 		
		 		$grpCategories = array();
		 		
		 		while ($recordset = mysql_fetch_array($rs->Resultset())) {
	 				try {
 						$grpCategoryX = new GroupCategory($recordset['categoryID']);											
 					} 					
					catch (Exception $e) {
					   throw $e;
					   
					}
					$grpCategories[] = $grpCategoryX;
 				}
 				
 				return $grpCategories;
 				
	 		}   
	 		
	 	
	 	/**
		 * Static Function
         * Purpose: Returns a list of all existing group categories         
         */ 
	 		public static function getGroupCategoriesList($grpDiscrim = 0){
	 			
	 			$grpCategoriesList = array();
	 			
	 			try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
					
		 		if (0 == $grpDiscrim )	
		 			$sql = "SELECT * FROM tblGroupCategory ORDER by name " ;
		 		else
		 			$sql = "SELECT * FROM tblGroupCategory WHERE discriminator = '$grpDiscrim' ORDER by name " ;
		 			
		 		$rs->Execute($sql);
		 		
		 		while ($recordset = mysql_fetch_array($rs->Resultset())) {
	 				try {
 						$grpCategoriesList [$recordset['categoryID']] = $recordset['name'];
 					} 					
					catch (Exception $e) {
					   throw $e;
					   
					}
 				}
 			
 				return $grpCategoriesList;
 				
	 		} 
	 	
	 	
	 	/**
		 * Static Function
         * Purpose: Returns an array of object of all existing group categories     
         */ 
	 		public static function getGroupCategories($grpDiscrim = 0){
	 			
	 			$grpCategories = array();
	 			
	 			try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				if (0 == $grpDiscrim )	
		 			$sql = "SELECT * FROM tblGroupCategory ORDER by name " ;
		 		else
		 			$sql = "SELECT * FROM tblGroupCategory WHERE discriminator = '$grpDiscrim' ORDER by name " ;
		 		$rs->Execute($sql);
		 		
		 		while ($recordset = mysql_fetch_array($rs->Resultset())) {
	 				try {
 						$grpCategories [] = new GroupCategory($recordset['categoryID']);
 					} 					
					catch (Exception $e) {
					   throw $e;
					   
					}
 				}
 			
 				return $grpCategories;
 				
	 		} 
	

		/**
		 * Static Function
         * Purpose: Returns the category name given a categoryID  
         */ 
	 		public static function getGroupCategoryNameByCategoryID($categoryID = 0){

	 			try {
		 			$conn = new Connection();
					$rs   = new Recordset($conn);
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$grpCategoryName = '';
				
				$sql = "SELECT name FROM tblGroupCategory WHERE categoryID = '$categoryID' ";
		 		$rs->Execute($sql);
				
				if ($rs->Recordcount())
					$grpCategoryName = $rs->Result(0,'name');
				
				return $grpCategoryName;
		 		
	 		}	
	 		
    }
?>
