<?

/**
 * Created on Jul 31, 2006
 * @author Joel C. LLano
 * Purpose: 
 */



abstract class UploadValidator{
	
	abstract function setFiles($files);
	abstract function validate();
	abstract function getValidFiles();
	abstract function getInvalidFiles();

}



?>