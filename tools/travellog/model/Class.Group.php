<?php
	/**
	 * @(#) Class.Group.php
	 * 
	 * @package goabroad.net
	 * @subpackage model
	 * 
	 * @author Czarisse Daphne P. Dolina
	 * @version 1.0 September 6, 2006
	 * @version 2.0 February 18, 2009 Antonio Pepito Cruda Jr.
	 */

	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("class.phpmailer.php");
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.GroupCategory.php");
	require_once("travellog/model/Class.GroupAccess.php");
	require_once("travellog/model/Class.FilterOp.php");
	require_once("travellog/model/Class.Traveler.php");
	require_once("travellog/model/Class.GroupEvent.php");
	require_once("travellog/model/Class.GroupPrivacyPreference.php");
	require_once("gaexception/Class.InstantiationException.php");
	require_once("travellog/model2/group/Class.ganetGroupService.php");	// could be removed in future after refactoring the services
	require_once("travellog/vo/Class.ganetValueObject.php");
	require_once("travellog/model2/dao/Class.ganetGroupDao.php");
 	require_once("Class.ConnectionProvider.php");
	require_once("travellog/model/Class.GanetDbHandler.php");
	require_once('Cache/ganetCacheProvider.php');
	
 	/**
 	 * The root class of a group.
 	 */
	class Group {  
       
		/**
		 * Constant of a club group.
		 * 
		 * @var integer
		 */
		const FUN   =  1;
		
		/**
		 * Constant of an admin group.
		 * 
		 * @var integer
		 */
		const ADMIN =  2;
		
		/**
		 * Constant of an node group.
		 * 
		 * @var integer
		 */
		const NODE_GROUP =  3;
		
		/**
		 * Constant for open to public group access.
		 * 
		 * @var integer
		 */
		const OPEN = 1;
		
		/**
		 * Constant for open but requires moderator approval group access.
		 * 
		 * @var integer
		 */
		const MODERATOR_APPROVAL = 2;		
		
		/**
		 * Constant for invite only group access.
		 * 
		 * @var integer
		 */
		const INVITE_ONLY = 3;
		
		/**
		 * Constant for closed group access.
		 * 
		 * @var integer
		 */
		const CLOSE = 4;		
		
		/**
		 * The administrator of the Group.
		 * 
		 * @var integer 
		 */
		protected $mAdministrator = null;
		
		/**
		 * The ID of the administrator of the Group.
		 * 
		 * @var integer 
		 */
		protected $mAdministratorID = 0;
		
		/**
		 * The clientID of the administrator/advisor of the Group.
		 * 
		 * @var integer 
		 */
		protected $mAdministratorClientID = 0;
			
		/**
		 * The date when the group was created.
		 * 
		 * @var string 
		 */
		protected $mDateCreated = "CURRENT_TIMESTAMP";
			
		/**
		 * The description of the Group.
		 * 
		 * @var string 
		 */
		protected $mDescription = "";
			
		/**
		 * The discriminator of the Group.
		 * 
		 * @var integer 
		 */
		protected $mDiscriminator = 1;
			
		/**
		 * The group access of the Group.
		 * 
		 * @var integer 
		 */
		protected $mGroupAccess = Group::OPEN;
			
		/**
		 * The ID of the Group.
		 * 
		 * @var integer 
		 */
		protected $mGroupID = 0;
		
		/**
		 * The photo ID of the Group's logo.
		 * 
		 * @var integer 
		 */
		protected $mGroupPhotoID = 0;
			
		/**
		 * The rank of the Group
		 * 
		 * @var integer 
		 */
		protected $mGroupRank = 0;
		
		/**
		 * The logo of the group.
		 * 
		 * @var Photo
		 */
		protected $mLogo = null;
			
		/**
		 * The name attribute of Group
		 * 
		 * @var string 
		 */
		protected $mName = "";
			
		/**
		 * The sendableID attribute of Group
		 * 
		 * @var integer 
		 */
		protected $mSendableID = 0;
		
		/**
		 * The flag whether the contents of the group are still copying or not.
		 * 
		 * @var integer
		 */
		protected $mCopyingStatus = 1;
		
		/**
		 * The api flag whether the group uses the api widget.
		 * 
		 * @var integer
		 */
		protected $mApiFlag = null;
		
		protected $mShowSteps = 0;
            
		protected $mConn            = NULL;
		protected $mRs              = NULL;
		protected $dao				= NULL;
		
		protected $mLabelName       = '';
		   
		protected $mGroupCategory   = array();       
		protected $mMembers         = array();
		protected $mOnlineMembers   = array();
		protected $mMembersUpdates  = array();
		protected $mRequestList     = array();
		protected $mInviteList      = array();
		protected $mEmailInviteList = array();
		protected $mArrModerators   = array();
		protected $mArrStaff	    = array();
			
		protected $mMemberCount = 0;
				
		private $arrphotos   = array();
		
		protected $mGroupEvents     = array();
		
		protected $mTotalRecords    = 0;
		protected $mMembershipDate  = NULL;
		    
		public  $phototype	= 8;
		
		public static $statvar		= 0;
		 
		private $subject = "%groupname% invites you to join GoAbroad Network";
		private $msg = "Hi %name%,

Greetings from GoAbroad Network!

Your friends at %groupname% has invited you to join their group on GoAbroad Network. 

By joining this group, you can post alerts, updates, and photos as well as send email broadcasts to %groupname%'s members. You will be able to access program information and updates, itineraries, application forms and brochures from %groupname%.

GoAbroad Network also provides forums and bulletin boards that will let students discuss experiences or ask questions about programs. By becoming a member, you will be able to post surveys and topics for discussion. You will be updated with what's new about %groupname% through calendar events that will automatically appear on your calendar.

To join %groupname%, please follow the link below:

http://www.goabroad.net/register.php?traveler
		
		
Happy travels!



Many Thanks,

The GoAbroad Network Team" ;
         
			private $customMessage = "";
			
			
		private $mParentID = 0;
		
		function __construct(){
			$this->dao	=	ganetGroupDao::instance();
			try {
				//$this->mConn = new Connection();
				$this->mRs   = ConnectionProvider::instance()->getRecordset();
			}
			catch (Exception $e) {                 
				throw $e;
			}
		}   	
		
		function getID(){
			return $this->mGroupID;
		}
			
		/**
		 * Returns the ID of the administrator of this Group.
		 * 
		 * @return string
		 */
		function getAdministratorID(){
			return $this->mAdministratorID;
		}
		
		/**
		 * Returns the clientID of the administrator/advisor of this Group.
		 * 
		 * @return string
		 */
		function getAdministratorClientID(){
			return $this->mAdministratorClientID;
		}
			
		/**
		 * Returns the dateCreated of this Group.
		 * 
		 * @return string
		 */
		function getDateCreated(){
			return $this->mDateCreated;
		}
			
		/**
		 * Returns the description of this Group.
		 * 
		 * @return string
		 */
		function getDescription(){
			return $this->mDescription;
		}
			
		/**
		 * Returns the discriminator of this Group.
		 * 
		 * @return string
		 */
		function getDiscriminator(){
			return $this->mDiscriminator;
		}
			
		/**
		 * Returns the featuredSubgroupRank of this Group.
		 * 
		 * @return string
		 */
		function getFeaturedSubgroupRank(){
			return $this->mFeaturedSubgroupRank;
		}
			
		/**
		 * Returns the groupAccess of this Group.
		 * 
		 * @return string
		 */
		function getGroupAccess(){
			return $this->mGroupAccess;
		}
			
		/**
		 * Returns the groupID of this Group.
		 * 
		 * @return string
		 */
		function getGroupID(){
			return $this->mGroupID;
		}
		
		/**
		 * Returns the photoID of the photo of this Group.
		 * 
		 * @return string
		 */
		function getGroupPhotoID(){
			return $this->mGroupPhotoID;
		}
			
		/**
		 * Returns the rank of this Group.
		 * 
		 * @return string
		 */
		function getGroupRank(){              
			return $this->mGroupRank;
		}
		
		/**
		 * Returns the logo of the group.
		 * 
		 * @return Photo Returns the logo of the group.
		 */
		function getGroupPhoto(){
			if (is_null($this->mLogo)) {
				$this->mLogo = new Photo($this, $this->mGroupPhotoID);
			}
			
			return $this->mLogo;
		}
		
		/**
		 * Returns the appropriate term of the group. Returns Group when the group
		 * is an admin group. Otherwise this returns Club.
		 * 
		 * @return string
		 */
		function getLabel(){                            
			return (Group::FUN == $this->mDiscriminator) ? "Club" : "Group";            	
		}
		
		function showGroupSteps(){
			return $this->mShowSteps;
		}
		
		function setShowSteps($bol){
			$this->mRs	=	new Recordset(ConnectionProvider::instance()->getConnection());
		 	$this->mShowSteps = $bol;
		 	
		 	$sql = "update tblGroup set showSteps='$bol' where groupID='$this->mGroupID'";
		 	$this->mRs->Execute($sql);
		}
		
		/**
		 * Returns the number of group's members that satisfies the given criteria.
		 * 
		 * @param SqlCriteria2 $criteria The handler for additional condition in getting the count.
		 * 
		 * @return integer the number of group's members.
		 */
		function getMembersCount(SqlCriteria2 $criteria = null){
			require_once('travellog/model/Class.SqlCriteria2.php');
			require_once('travellog/model/Class.MemberPeer.php');
			
			$criteria2 = is_null($criteria) ? new SqlCriteria2() : clone $criteria;
			$criteria2->add(MemberPeer::GROUP_ID, $this->mGroupID);
			
			return MemberPeer::doCount($criteria2);
		}
			
		/**
		 * Returns the name of this Group.
		 * 
		 * @return string
		 */
		function getName(){
			return $this->mName;
		}
			
		/**
		 * Returns the sendableID of this Group.
		 * 
		 * @return string
		 */
		function getSendableID(){
			return $this->mSendableID;
		}
            
		/**
		 * Returns the trimmed name of this Group.
		 * 
		 * @param integer $_numChar The number of characters to remain.
		 * 
		 * @return void 
		 */
		function getTrimmedGroupName($_numChar = 20){
			return ($_numChar < strlen($this->mName) && !strpos($this->mName," ") ) ? substr($this->mName,0,$_numChar) . "..." : $this->mName;
		}
		
		/**
		 * Initializes the group's attributes using the given array.
		 * 
		 * @param array $props The values for the group's attributes.
		 * 
		 * @return void
		 */
		protected function initialize(array $props){
			if (0 < count($props)) {
				$this->mAdministratorID = isset($props['administrator']) ? $props['administrator'] : $this->mAdministratorID;
				$this->mDateCreated = isset($props['datecreated']) ? $props['datecreated'] : $this->mDateCreated;
				$this->mDescription = isset($props['description']) ? $props['description'] : $this->mDescription;
				$this->mDiscriminator = isset($props['discriminator']) ? $props['discriminator'] : $this->mDiscriminator;
				$this->mGroupAccess = isset($props['groupaccess']) ? $props['groupaccess'] : $this->mGroupAccess;
				$this->mGroupID = isset($props['groupID']) ? $props['groupID'] : $this->mGroupID;
				$this->mGroupRank = isset($props['inverserank']) ? $props['inverserank'] : $this->mGroupRank;
				$this->mName = isset($props['name']) ? $props['name'] : $this->mName;
				$this->mGroupPhotoID = isset($props['photoID']) ? $props['photoID'] : $this->mGroupPhotoID;
				$this->mSendableID = isset($props['sendableID']) ? $props['sendableID'] : $this->mSendableID;
				$this->mParentID = isset($props['parentID']) ? $props['parentID'] : $this->mParentID;
				$this->mShowSteps = isset($props['showSteps']) ? $props['showSteps'] : $this->mShowSteps;
				$this->mCopyingStatus = isset($props['copyingStatus']) ? $props['copyingStatus'] : $this->mCopyingStatus;
				$this->mApiFlag = isset($props['apiFlag']) ? $props['apiFlag'] : $this->mApiFlag;
			}			
		}
		
		/**
		 * Sets the administrator of this Group.
		 * 
		 * @return void
		 */
		function setAdministratorID($administrator){
			$this->mAdministratorID = $administrator;
		}
		
		/**
		 * Sets the clientID of the administrator/advisor of this Group.
		 * 
		 * @return void
		 */
		function setAdministratorClientID($administratorClientID){
			$this->mAdministratorClientID = $administratorClientID;
		}
	
		/**
		 * Sets the dateCreated of this Group.
		 * 
		 * @return void
		 */
		function setDateCreated($dateCreated){
			$this->mDateCreated = $dateCreated;
		}
	
		/**
		 * Sets the description of this Group.
		 * 
		 * @return void
		 */
		function setDescription($description){
			$this->mDescription = $description;
		}
	
		/**
		 * Sets the discriminator of this Group.
		 * 
		 * @return void
		 */
		function setDiscriminator($discriminator){
			$this->mDiscriminator = $discriminator;
		}
	
		/**
		 * Sets the groupAccess of this Group.
		 * 
		 * @return void
		 */
		function setGroupAccess($groupAccess){
			$this->mGroupAccess = $groupAccess;
		}
	
		/**
		 * Sets the groupID of this Group.
		 * 
		 * @return void
		 */
		function setGroupID($groupID){
			$this->mGroupID = $groupID;
		}
		
		/**
		 * Sets the ID of the group logo of this Group.
		 * 
		 * @return void
		 */
		function setGroupPhotoID($photoID){
			$this->mGroupPhotoID = $photoID;
		}
	
		/**
		 * Sets the rank of this Group.
		 * 
		 * @return void
		 */
		function setGroupRank($inverseRank){
			$this->mGroupRank = $inverseRank;
		}

		/**
		 * Sets the name of this Group.
		 * 
		 * @return void
		 */
		function setName($name){
			$this->mName = $name;
		}
	
		/**
		 * Sets the sendableID of this Group.
		 * 
		 * @return void
		 */
		function setSendableID($sendableID){
			$this->mSendableID = $sendableID;
		}
		
		/**
		 * Retrieves the status of copying.
		 * 
		 * @return	int
		 */
		public function getCopyingStatus() {
			return $this->mCopyingStatus;
		}
		
		/**
		 * Sets the status of copying for the group.
		 * 
		 * @param		int		$val 		The status of copying.
		 */
		public function setCopyingStatus($val) {
			$this->mCopyingStatus = $val;
		}
		
		/**
		 * Checks whether the contents of the group is still copying or not.
		 * 
		 * @return	boolean		true, if the group content's is still copying and false otherwise.
		 */
		public function isCopyingDone() {
			return (1 == $this->mCopyingStatus);
		}
      
      	/**
         * Purpose: Gets the total records of the last query performed, if applicable.
         */  
         
            function getTotalRecords(){                
                return $this->mTotalRecords;
            } 
            
        /**
         * Purpose: Sets the group photo of this group.
         */  
         
            function setGroupPhoto($photo){
                
                $this->setGroupPhotoID($photo->getPhotoID());
                $this->Update();
            }
            
        /**
         * Purpose: Sets the category of the group
         */
            function setGroupCategory($groupCategoryID=0){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $sql = "INSERT into tblGrouptoCategory (groupID, categoryID) VALUES ('$this->mGroupID', '$groupCategoryID')";
                $this->mRs->Execute($sql);
            }
        
        
        /**
         * Purpose: Removes the category of the group
         */
            function removeGroupCategory($groupCategoryID=0){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $sql = "DELETE FROM tblGrouptoCategory WHERE groupID = '$this->mGroupID' AND categoryID =  '$groupCategoryID' ";
                $this->mRs->Execute($sql);
            }
            
        /**
         * Purpose: Returns the category of the group in an array.
         */
            function getGroupCategory($list=false){     
                if (0 == count($this->mGroupCategory)){
                    
                    $sql = "SELECT tblGrouptoCategory.categoryID, name " .
							"FROM tblGrouptoCategory, tblGroupCategory " .
							"WHERE tblGrouptoCategory.categoryID = tblGroupCategory.categoryID " .
							"AND tblGrouptoCategory.groupID = " . $this->mGroupID;
                    //$this->mRs->Execute($sql);
                    $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                    $this->mRs->Execute($sql);
                    
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                      try {
                            if ($list)
								$this->mGroupCategory[$recordset['categoryID']] = $recordset['name'];
							else	
								$this->mGroupCategory[] = new GroupCategory($recordset['categoryID']);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }                            
                    }                      
                }                    
                return $this->mGroupCategory;                    
            }
            
           
        /**
         * Purpose: Returns the group's administrator.
         */
            function getAdministrator(){
               if (NULL == $this->mAdministrator){
                    if (0 == $this->mAdministratorID){
                        return NULL;            
                    }
                    else {
                        try {
                            $traveler = new Traveler($this->mAdministratorID);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                        $this->mAdministrator = $traveler;                  
                    }               
                }
                return $this->mAdministrator;                    
            }   
        
		/**
          Purpose: Returns the subgroup's staff.
         */
            function getStaff($_list = false){ return array();}

		/**
			Purpose: add a staff to a subgroup
		 */	
			function addStaff($_travelerID){}

		/**
			added by Jul
		*/
			function hasStaff(){ return FALSE;}

		/**
			Purpose: remove this traveler as subgroup staff
		 */	
			function removeStaff($_travelerID){}

		/**
			Purpose: check if traveler is a staff of this subgroup 
		*/
			function isStaff($_travelerID){ return false;}
		
		/**
			Purpose: check if traveler is an inactive staff of this group
		 */	
			function isInActiveStaff($_travelerID){	return false;}
        
        /**
          Purpose: Returns the group's moderators.
         */
            function getModerators($_list = false){ return array();}

		/**
			Purpose: add a moderator to a group
		 */	
			function addModerator($_travelerID){}
		
		/**
			Purpose: remove this traveler as group moderator
		 */	
			function removeModerator($_travelerID){}
			
		/**
			Purpose: check if traveler is a moderator of this group
		 */	
			function isModerator($_travelerID){ return false;}
			
        /**
			Purpose: check if traveler is an inactive moderator of this group
		 */	
			function isInActiveModerator($_travelerID){	return false;}
			
		/**
         * Purpose: Return apiFlag.
         */  
            function getApiFlag(){
                return $this->mApiFlag;
            }
	
		/**
		 * Sets the apiFlag of this Group.
		 * 
		 * @return void
		 */
			function setApiFlag($flag){
				$this->mApiFlag = $flag;
			}
            
         /**
         * Purpose: Save apiFlag.
         */  
            function saveApiFlag($flag){
                
                $this->setApiFlag($flag);
                $this->Update();
            }

			/*** Added by naldz (June 06, 2008) ***/
			public function getLastFeaturedDate(){
				require_once('gaLogs/Reader/Class.Reader.php');
				$reader = new Reader();
				$sql = 	'SELECT _EXECUTION_DATE_ as lastFeaturedDate '.
						'FROM Travel_Logs.tblFeaturedItems '.
						'WHERE _NEW_sectionID = 1 '.
						'AND _NEW_genID = '.GaString::makeSqlSafe($this->getGroupID()).' '.
						'AND _COMMAND_ = "INSERT" '.
						'ORDER BY lastFeaturedDate DESC ';
				$rs = $reader->getLogsBySql($sql);
				if($rs->retrieveRecordCount() > 0){
					return $rs->getLastFeaturedDate(0);
				}
				return null;
			}	

			/*** ##### Added By: Cheryl Ivy Q. Go	1 October 2008 ##### ***/
			function getDatesGroupIsFeatured(){
				require_once('gaLogs/Reader/Class.Reader.php');
				$mReader = new Reader();
				$mDates = array();

				$sql = 	'SELECT _EXECUTION_DATE_ as lastFeaturedDate '.
						'FROM Travel_Logs.tblFeaturedItems '.
						'WHERE _NEW_sectionID = 1 '.
						'AND _NEW_genID = ' . GaString::makeSqlSafe($this->getGroupID()) . ' ' .
						'AND _COMMAND_ = "INSERT" '.
						'ORDER BY lastFeaturedDate DESC ';
				$rs = $mReader->getLogsBySql($sql);

				if(0 < $rs->retrieveRecordCount())
					$mDates = $rs->retrieveColumn('lastFeaturedDate');

				return $mDates;
			}
			
        /**
         * Purpose: Send message as an AlertMessage or BulletinMessage.
         */
            function sendMessage(Message $message){

				$messageID  = $message-> getMessageID();
				$sendableID = $message->getSource()->getSendableID();
				$destination = $message-> getDestination();

				if (MessageType::$INBOX == $message->getMessageTypeID()){
					
					$Inbox = new Inbox($sendableID);
					$messageID = $Inbox-> Send($message);

					$SentItems = new SentItems($sendableID);
					$SentItems-> Store($messageID,$destination);
				}
				
				elseif (MessageType::$DRAFTS == $message->getMessageTypeID()){
					
					$Drafts = new Drafts($sendableID);
					$Drafts-> Send($message);
				}
				
				elseif (MessageType::$INQUIRYBOX == $message->getMessageTypeID()){
					
					$InquiryBox = new InquiryBox($sendableID);
					$messageID = $InquiryBox-> Send($message);

					$SentItems = new SentItems($sendableID);
					$SentItems-> Store($messageID,$destination);
				}

				else {
					$msgspace = new MessageSpace();
	                $msgspace-> Store($message);
				}
            }
         
         
		/**
         * Purpose: Send message as draft.
         */
        	function saveAsDraft(Message $_message = NULL){

				$Drafts = new Drafts();
				$Drafts-> Save($_message);
			}
        
		/**
		 * Returns true if the given traveler is a member of the group.
		 * 
		 * @return boolean true if the given traveler is a member of the group.
		 */
		function isMember($traveler){
			$traveler_id = $traveler->getTravelerID();
			if (0 == $traveler_id) {
				return false;
			}
			
			$this->mRs	=	ConnectionProvider::instance()->getRecordset(); 
			$sql = "SELECT travelerID from tblGrouptoTraveler WHERE groupID = '$this->mGroupID' AND travelerID = '$traveler_id' LIMIT 0,1";
			$this->mRs->Execute($sql);
			
			return (0 == $this->mRs->Recordcount()) ? false : true;
		}
            
            function getMembershipDate($traveler){
            	
            	$travID = $traveler->getTravelerID();
            	$this->mRs	=	ConnectionProvider::instance()->getRecordset();
            	$sql = "SELECT adate FROM tblGrouptoTraveler WHERE groupID = '$this->mGroupID' AND travelerID = '$travID' LIMIT 0,1";
            	$this->mRs->Execute($sql);
            	
            	while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {                       
                    $this->mMembershipDate = $recordset['adate'];
                }
                        
                return $this->mMembershipDate;      
                    
            }
        
        
        /**
         * Purpose: This is called when traveler is added as a member of a group.
         */
            function addMember($traveler){
                
				if (!$this->isMember($traveler)) {
               	 	$travID = $traveler->getTravelerID();  
	                $now  = date("Y-m-d H:i:s");
                         
	                // $sql = "INSERT into tblGrouptoTraveler (groupID, travelerID, adate) VALUES ('$this->mGroupID', '$travID', '$now')";
	                // $this->mRs->Execute($sql);
					
	            	////////////// NEEDS TO BE TRANSFERRED TO GROUP SERVICE
					$contextVO = new ganetValueObject();
					$contextVO->set('TRAVELER_ID',$travID);
					$contextVO->set('GROUP_ID',$this->mGroupID);

					$vo 	=	 new ganetValueObject();
					$vo->set('COMMAND','ADD_MEMBER');
					// $vo->set('CONTEXT',array('TRAVELER_ID'=>$travelerID,'GROUP_ID'=>$this->mGroupID));
					$vo->set('CONTEXT',$contextVO);
					$this->dao->executeCommand($vo);
					/////////////// NOTE NOT YET TESTED ///////////////////////////////////////////////
					
					/*require_once("travellog/model/Class.ActivityFeed.php");
					$memberBy = ActivityFeed::MEMBER_BY_DEFAULT;
					$date = $this->isInInviteList($traveler);
					if( $date  ){
						$memberBy = ActivityFeed::MEMBER_BY_INVITE;
					}else if( $this->isInRequestList($traveler) ){
						$params = array("FEED_SECTION"		=>	ActivityFeed::JOINGROUPREQUEST,
										"SECTION_TYPE"		=>	ActivityFeed::GROUP,
										"TRAVELER_ID"		=>	$travID,
										"GROUP_ID"			=>	$this->getGroupID() );
						$feeds = ActivityFeed::getInstances($params);
						if( 0 < $feeds["recordCount"] ){
							$date = $feeds["activityFeeds"][0]->getFeedDate();
						}
						$memberBy = ActivityFeed::MEMBER_BY_REQUEST;
					}else{
						$date = NULL;
					}
					
					try{
						ActivityFeed::create($traveler,$this,$memberBy,$date);
					}catch(exception $e){}*/

					$this->removeFromInviteList($traveler->getTravelerID());
					$this->removeFromRequestList($traveler->getTravelerID());
					$this->removeFromEmailInviteList($traveler->getTravelerProfile()->getEmail());

					// remove from tblEmailInviteList and insert to tblInviteList
					$this->convertEmailInviteList($traveler->getTravelerID(), $traveler->getTravelerProfile()->getEmail());
				
					$this->invalidateCacheKeys($this->getGroupID());
				}
            }
            

			function convertEmailInviteList($_travelerId = 0, $_email = ''){
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
				
				// check groupID where email has invitations
				$sql = "SELECT groupID FROM tblEmailInviteList WHERE email LIKE '$_email'";
				$this->mRs->Execute($sql);

				if (0 < $this->mRs->Recordcount()){
					$arrGroupId = array();
					while($Group = mysql_fetch_assoc($this->mRs->Resultset()))
						$arrGroupId[] = $Group['groupID'];

					// delete
					$sql = "DELETE FROM tblEmailInviteList WHERE email LIKE '$_email'";
					$this->mRs->Execute($sql);

					foreach ($arrGroupId as $gID){
						$sql = "SELECT travelerID FROM tblGrouptoTraveler WHERE groupID = " . $gID . " AND travelerID = " . $_travelerId;
						$this->mRs->Execute($sql);

						// if not yet a member, add to invite list
						if (0 == $this->mRs->Recordcount()){
							$now  = date("Y-m-d H:i:s");

							$sql = "INSERT into tblInviteList (groupID, travelerID, inviteDate) " .
									"VALUES (" . $gID . ", " . $_travelerId . ", '$now')";
			                $this->mRs->Execute($sql);
						}
					}
				}
			}
            
        /**
         * Purpose: This is called when traveler is removed as a member of a group.
         */
            function removeMember($travelerID){      
                
                // $sql = "DELETE FROM tblGrouptoTraveler WHERE groupID = '$this->mGroupID' AND travelerID = '$travelerID' ";
                // $this->mRs->Execute($sql);
            	
            	////////////// NEEDS TO BE TRANSFERRED TO GROUP SERVICE
				$contextVO = new ganetValueObject();
				$contextVO->set('TRAVELER_ID',$travelerID);
				$contextVO->set('GROUP_ID',$this->mGroupID);
				
				$vo 	=	 new ganetValueObject();
				$vo->set('COMMAND','REMOVE_MEMBER');
				// $vo->set('CONTEXT',array('TRAVELER_ID'=>$travelerID,'GROUP_ID'=>$this->mGroupID));
				$vo->set('CONTEXT',$contextVO);
				$this->dao->executeCommand($vo);
				/////////////// NOTE NOT YET TESTED ///////////////////////////////////////////////
            	

				$this->removeFromInviteList($travelerID);	 	
			    $this->removeFromRequestList($travelerID);
			
				//added by Jul to remove member feeds related to group
				/*require_once("travellog/model/Class.ActivityFeed.php");
				$GROUP = ActivityFeed::GROUP;
				$groupID = $this->getGroupID();
				$sql = "DELETE FROM tblActivityFeed
						WHERE sectionType = $GROUP AND groupID = $groupID AND travelerID = $travelerID";
				$this->mRs->Execute($sql); */
				
				$this->invalidateCacheKeys($this->getGroupID());
            }
            
                            
        /**
         * Purpose: Returns an array of members of this group. 
         * Rows Limit is enabled for this function.
         */
            function getMembers(RowsLimit $rowslimit = null, $filtercriteria = NULL, $order = NULL, $count = false, $prioritizeMembersWithPhotos=FALSE){            
                $criVO	=	new ganetValueObject();
                // set context value object
                $contextVO	=	new ganetValueObject();
                $contextVO->set('GROUP_ID',$this->getGroupID());
				if( self::ADMIN == $this->mDiscriminator ){
                	$contextVO->set('PARENT_ID',$this->mParentID);
            	}else{
					$contextVO->set('PARENT_ID',0);
				}
				$contextVO->set('GROUP_TYPE',$this->mDiscriminator);
                
                if ($count)
                	$contextVO->set('COUNT',1);
                else	
                	$contextVO->set('COUNT',0);
                
                // set filter value object
                $filterVO	=	new ganetValueObject();
                //$conditions =	$filtercriteria->getConditions();
                //- put rows limit inside filtercriteria
	            if (NULL != $rowslimit){	                    
                    if ($filtercriteria == NULL){
                    	$filtercriteria	=	new FilterCriteria2();
                    }
                    	
	            	$filtercriteria->setStartRow($rowslimit->getOffset());
                    $filtercriteria->setRowsPerPage($rowslimit->getRows());
                }
                
                //- put order inside filtercriteria
                if (NULL != $order){
                	
                	if ($filtercriteria == NULL){
                    	$filtercriteria	=	new FilterCriteria2();
                    }
                	
                	$conditions = $order->getConditions();	
        			
                	$orderAttr =	$conditions[0]->getAttributeName();
                	$oper = $conditions[0]->getOperation();
                	
                	if ($orderAttr != ''){
                		if ($oper == FilterOp::$ORDER_BY_DESC)	        			
	        				$filtercriteria->setOrderBy($orderAttr, 'DESC' );
	        			else 
	        				$filtercriteria->setOrderBy($orderAttr);
                	}		
                }

                //- put filtercriteria object to filterVO to pass to group service
                if (NULL != $filtercriteria){
                	$filterVO->set('FILTER_CRITERIA',$filtercriteria);
                } else {
                	$filterVO->set('FILTER_CRITERIA',new FilterCriteria2());
                }
                               
                //
                $criVO->set('CONTEXT',$contextVO);
                $criVO->set('FILTER',$filterVO);
                
                $groupServ	=	new ganetGroupService();
                $members	 = 	$groupServ->getMembers($criVO,$prioritizeMembersWithPhotos); 
                return $members;
            }
       
       /**
        * Method to get the number of photos of this group
        * You can specify if you want to include counting of photos 
        * from the photo album also
        * @param  boolean  $isPhotoAlbumPhotoIncluded flag whether to include counting the photos in photo albums or not
        * @return integer                             returns the number of photos of this group
        * 
        * Added by Antonio Pepito Cruda Jr.
        */
       function getNumberOfPhotos($isPhotoAlbumPhotoIncluded = false){
         $this->mRs	=	ConnectionProvider::instance()->getRecordset();
       	
       	 $sql = "SELECT tblPhoto.photoID from tblGroup,tblPhoto " .
         		" WHERE tblGroup.photoID = tblPhoto.photoID " .
         		" AND  groupID = '$this->mGroupID'";
         
         if ($isPhotoAlbumPhotoIncluded) {
         	 $sql = $sql." UNION " .
         	 		" SELECT tblPhotoAlbumtoPhoto.photoID " .
         	 		" FROM tblPhotoAlbum,tblPhotoAlbumtoPhoto " .
         	 		" WHERE tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID" .
         	 		" AND tblPhotoAlbum.groupID = '$this->mGroupID'";
         }
         
         $this->mRs->Execute($sql);
         
         return $this->mRs->Recordcount() ;        
       }
       
            
         
        /**
         * Purpose: Returns an array of online members of this group. 
         * Rows Limit is enabled for this function.
         */
            function getOnlineMembers($rowslimit = NULL){
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
                if (0 == count($this->mOnlineMembers)){
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    if (GROUP::ADMIN == $this->mDiscriminator )
                    	$advisorstr = "AND tblGrouptoTraveler.travelerID not in (select distinct travelerID from tblGrouptoAdvisor) ";
                    	
                    $sql = "SELECT trim(loginTime) AS isOnline from tblSessions, tblGrouptoTraveler, tblTraveler " .
                    		"WHERE tblSessions.session_user_id = tblGrouptoTraveler.travelerID " .
							"AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .                    		
							"AND tblGrouptoTraveler.groupID = '$this->mGroupID' " . $advisorstr .
							" AND domain = '{$_SERVER['SERVER_NAME']}' " . 
                    		"ORDER BY tblTraveler.username " . $limitstr;
                    $this->mRs->Execute($sql);
                    
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                        try {
                            $traveler = new Traveler($recordset['session_user_id']);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                        $this->mOnlineMembers[] = $traveler;
                    }                                   
                }    
                
                return $this->mOnlineMembers;                    
            }
		
		
		/**
         * Purpose: Adds updates for group membership.
         * To determine if traveler has joined or left a group.
         */
            function addMembershipUpdates($traveler = NULL, $source = 0, $activestatus = 1, $actiontype = 0){            
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $travID = $traveler->getTravelerID();
                $now  = date("Y-m-d H:i:s");
                
                $sql = "INSERT into tblGroupMembershipUpdates (groupID, travelerID, source, actiontype, activestatus, dateupdated) " .
                		"VALUES ('$this->mGroupID', '$travID', '$source', '$actiontype' , '$activestatus', '$now' )";
                $this->mRs->Execute($sql);                             
            }
            
            		
		/**
         * Purpose: This is called when membership updates of a group is removed.
         */
            function removeMembershipUpdates($groupID){                  
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $sql = "DELETE FROM tblGroupMembershipUpdates WHERE groupID = '$groupID' ";
                $this->mRs->Execute($sql);                                
            }
            
		
		/**
         * Purpose: Returns an array of membership updates.
         * Rows Limit is enabled for this function.
         */
            function getMembershipUpdates($filter = null , $rowslimit = null){                    
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
            	if (0 == count($this->mMembersUpdates)){                    
                    $limitstr = '';
                    $filterstr = '';

                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }
                    if (NULL != $filter){
	                	$condition = $filter->getConditions();	
	        			$booleanop = $filter->getBooleanOps();

	        			for($i=0;$i<count($condition);$i++){
		        			$attname   = $condition[$i]->getAttributeName();                    			
		        			$value 	   = $condition[$i]->getValue();
		        			
		        			switch($condition[$i]->getOperation()){		        				
		        				case FilterOp::$GREATER_THAN_OR_EQUAL:
		        					$operator = " >= ";
		        					break;
		        				default:
		        					$operator = " = ";
		        					break;
		        			}
		        			
		        			$filterstr .=  " " . $booleanop[$i] . " ". $attname . $operator . " '$value' ";
	        			}            		
	                }
                        
                    $sql = "SELECT travelerID, source, actiontype, activestatus, dateupdated " .
                    		"FROM tblGroupMembershipUpdates " .
                    		"WHERE groupID = '$this->mGroupID' " . $filterstr .
                    		" ORDER BY dateupdated DESC " . $limitstr;
                    $this->mRs->Execute($sql);
               
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                        try {
                            $traveler = new Traveler($recordset['travelerID']);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                        
                        $tmparray = array();
                        $tmparray['traveler'] = $traveler;
                        $tmparray['source']   = $recordset['source'];
                        $tmparray['actiontype']   = $recordset['actiontype'];
                        $tmparray['status']   = $recordset['activestatus'];
                        $tmparray['date']   = $recordset['dateupdated'];
                        
                        $this->mMembersUpdates[] = $tmparray;
                    }               
                }                    
                return $this->mMembersUpdates;                    
            }
            
                         
               
        /**
         * Purpose: This is called when traveler wants to join a group.
         * This is valid only for group with GroupAccess::OPEN_APPROVAL .
         * The traveler should be checked if valid for joining the group.
         * To check for validity: traveler must not be a member of this group yet, and traveler must not be in Request Membership List yet.
         * If traveler status is true in both conditions, then add name to Request Membership List.
         */
            function requestToJoin($traveler){     
            	$this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $travID = $traveler->getTravelerID(); 
                
                if (TRUE == $this->isInDeniedRequestList($traveler)){
                	$sql = "UPDATE tblRequestList SET status = 0 WHERE groupID = '$this->mGroupID' AND travelerID = '$travID' ";
                	$this->mRs->Execute($sql);
                }
                elseif (FALSE == $this->isInRequestList($traveler)){
                	$sql = "INSERT into tblRequestList (groupID, travelerID) VALUES ('$this->mGroupID', '$travID')";
                	$this->mRs->Execute($sql);

					require_once("travellog/model/Class.ActivityFeed.php");
					try{
						ActivityFeed::create($traveler,$this);
					}catch(exception $e){}

                }
                
             
            }
        
        
		/**
		 * Returns true if the given traveler is in the request list.
		 * 
		 * @param Traveler $traveler The traveler to be tested.
		 * 
		 * @return boolean true if the given traveler is in the request list.
		 */
		function isInRequestList($traveler){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
		    $travID = $traveler->getTravelerID();
		    $sql = "SELECT travelerID from tblRequestList WHERE groupID = '$this->mGroupID' AND travelerID = '$travID' AND status = 0";
		    
		    $resource = $handler->execute($sql);
		    
		    return (0 < mysql_num_rows($resource)) ? true : false;			
			}
			catch (exception $ex) {}
		}
            
            
        /**
         * Purpose: Checks if traveler is enlisted in Request List; but group has denied request of traveler to join group.
         * Returns a boolean value.
         */
            function isInDeniedRequestList($traveler){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
            	
                $travID = $traveler->getTravelerID();
                $sql = "SELECT travelerID from tblRequestList WHERE groupID = '$this->mGroupID' AND travelerID = '$travID' AND status = 1";
                $this->mRs->Execute($sql);
                
                if (0 == $this->mRs->Recordcount())
                    return false;  
                else 
                    return true;
            }
        
        
        /**
         * Purpose: This is called when traveler is removed from Request List after membership.
         */
            function removeFromRequestList($travelerID){                  
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $sql = "DELETE FROM tblRequestList WHERE groupID = '$this->mGroupID' AND travelerID = '$travelerID' ";
                $this->mRs->Execute($sql); 

				require_once("travellog/model/Class.ActivityFeed.php");
				$params = array("FEED_SECTION"	=>	ActivityFeed::JOINGROUPREQUEST,
								"SECTION_TYPE"	=>	ActivityFeed::GROUP,
								"GROUP_ID"		=>	$this->getGroupID(),
								"TRAVELER_ID"	=>	$travelerID);
				$feeds = ActivityFeed::getInstances($params);
				if( 0 < $feeds["recordCount"] ){
					$feeds["activityFeeds"][0]->Delete();
				}
                               
            }
            
         
         /**
         * Purpose: This is called when group administrator denies the traveler's request to join group.
         */
            function denyRequest($travelerID){                  
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $sql = "UPDATE tblRequestList SET status = 1 WHERE groupID = '$this->mGroupID' AND travelerID = '$travelerID' ";
                $this->mRs->Execute($sql);                                
            }
            
               
		/**
         * Purpose: Returns an array of travelers in the Request List; pending travelers not accepted in group yet
         */
            function getRequestList(){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
            	if (0 == count($this->mRequestList)){                    
                    $sql = "SELECT tblTraveler.* " .
							"FROM tblRequestList, tblTraveler " .
							"WHERE tblTraveler.travelerID = tblRequestList.travelerID " .
							"AND tblTraveler.active = 1 " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblRequestList.status = 0 " .
							"AND tblRequestList.groupID = " . $this->mGroupID;
                    $this->mRs->Execute($sql);

                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
						$mTraveler = new Traveler;
						$mTraveler->setTravelerData($recordset);
                        $this->mRequestList[] = $mTraveler;
                    }               
                }
                
                return $this->mRequestList;                                           
            }
        
        
        
        /**
         * Purpose: This is called when a traveler or a list of travelers is invited by administrator to join a group.
         * This is valid only for group with GroupAccess::INVITE_ONLY . 
         * Function parameter is an array of travelers. Each one should be checked if valid for invitation.
         * To check for validity: traveler must not be a member of this group yet; traveler must not be in InviteList yet; traveler must not be a member of the parent group (if subgroup)
         * If traveler status is true in all conditions, then add name to Invite List.
         */
            function addInvites($invites,$sendEmailNotif=TRUE){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                foreach ($invites as $eachinvite){
                	if (!$this->isMember($eachinvite) ){                 
                        $travID = $eachinvite->getTravelerID();
                        
						$proceed = true;
						// if subgroup and invitee is not yet a member of parent group; proceed INVITE
						if ($this->isSubGroup() && $this->getParent()->isMember($eachinvite))
							$proceed = false;
						
						if ($proceed){	
                        	if ($this->isInDeniedInviteList($eachinvite)){
	                        	$sql = "UPDATE tblInviteList SET status = 0 WHERE groupID = '$this->mGroupID' AND travelerID = '$travID' ";
	                        	$this->mRs->Execute($sql);                        	
	                        }
	                        elseif (!$this->isInInviteList($eachinvite)){                        	
	                        	$sql = "INSERT into tblInviteList (groupID, travelerID) VALUES ('$this->mGroupID', '$travID')";
	                        	$this->mRs->Execute($sql);
							}	
							if (strpos($_SERVER['SERVER_NAME'], '.local') > 0 || !$sendEmailNotif) {
								return;
							}
							//added by chris: send notification to invited Traveler
							require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
							RealTimeNotifier::getInstantNotification(RealTimeNotifier::GROUP_INVITE_TRAVELER, 
								array(
									'TRAVELER' 	=> $eachinvite,
									'GROUP'		=> $this,
									'CUSTOM_MESSAGE' => $this->getCustomMessage()
								))->send();
							//edited by Jul, to send notification to both cases when the invitation is denied or it is still pending
						}
						else {
							
							$this->addMember($eachinvite);
							
						}	
                    }   
                }
           
            }
        
        /**
         * Purpose: Checks if traveler is enlisted in Invite List.
         */
            function isInInviteList($traveler){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $travID = $traveler->getTravelerID();
                $sql = "SELECT tblTraveler.travelerID, CAST(tblInviteList.inviteDate AS DATETIME) AS `inviteDate`
						from tblInviteList, tblTraveler 
						WHERE tblInviteList.travelerID = tblTraveler.travelerID " .
                		//"AND tblTraveler.active = 1 AND tblInviteList.groupID = '$this->mGroupID' " .
						"AND tblInviteList.groupID = '$this->mGroupID' " .
                		"AND tblInviteList.travelerID = '$travID' AND (tblInviteList.status = 0 OR tblInviteList.status = 2) ".
						"LIMIT 0,1";
                $this->mRs->Execute($sql);
				
                if (0 == $this->mRs->Recordcount())
                    return false;  
                else{
					$data = mysql_fetch_assoc($this->mRs->Resultset());
					return $data["inviteDate"];
					//return true;
				}
            }

			//added by Jul to get the pending invitations of the traveler 
			function getPendingGroupNetworkInvitations($traveler){
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $travID = $traveler->getTravelerID();
				$sql = "SELECT tblInviteList.groupID AS groupID
						FROM tblInviteList, tblGroup 
						WHERE tblInviteList.travelerID = '$travID'
						AND tblInviteList.groupID = tblGroup.groupID
                		AND tblInviteList.travelerID = '$travID'
						AND tblInviteList.status != 1
						AND (tblGroup.groupID = '$this->mGroupID' OR tblGroup.parentID='$this->mGroupID')
						GROUP BY groupID";
                $this->mRs->Execute($sql);
				$inviting_groups = array();
				while($data = mysql_fetch_array($this->mRs->Resultset())){
					try{
						$group = new AdminGroup($data["groupID"]);
						if( $group instanceof AdminGroup ){
							$inviting_groups[] = $group;
						}
					}catch(exception $e){
					}
				}
				return $inviting_groups;
			}
            
            
       /**
         * Purpose: Checks if traveler is enlisted in Invite List; but has denied invitation sent by group.
         */
            function isInDeniedInviteList($traveler){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $travID = $traveler->getTravelerID();
                $sql = "SELECT tblTraveler.travelerID
						from tblInviteList, tblTraveler
						WHERE tblInviteList.travelerID = tblTraveler.travelerID " .
                		"AND tblTraveler.active = 1  AND tblInviteList.groupID = '$this->mGroupID' " .
                		"AND tblInviteList.travelerID = '$travID' AND tblInviteList.status = 1 ".
						"LIMIT 0,1";
                $this->mRs->Execute($sql);
                
                if (0 == $this->mRs->Recordcount())
                    return false;  
                else 
                    return true;
            }
        
        
        /**
         * Purpose: This is called when traveler is removed from Invite List after membership registration.
         */
            function removeFromInviteList($travelerID){     
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $sql = "DELETE FROM tblInviteList WHERE groupID = '$this->mGroupID' AND travelerID = '$travelerID' ";
                $this->mRs->Execute($sql);
            }
		
		/**
         * Purpose: This is called when traveler denies acceptance of invitation to join a group.
         */
            function denyInvitation($travelerID){                  
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $sql = "UPDATE tblInviteList SET status = 1 WHERE groupID = '$this->mGroupID' AND travelerID = '$travelerID' ";
                $this->mRs->Execute($sql);                                
            }
            
		/**
         * Purpose: Returns an array of travelers in the Invite List; pending travelers who did not approve invitation yet
         */
            function getInviteList($filtercriteria = NULL){
              	if ($filtercriteria === NULL && !empty($this->mInviteList)){
              		return $this->mInviteList;
              	}
              	
              	$filterstr = '';
              	$this->mInviteList = array();
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
  					            	
                if (NULL != $filtercriteria){
                	$condition = $filtercriteria->getConditions();	
        			$attname   = $condition[0]->getAttributeName();                    			
        			$value 	   = $condition[0]->getValue();

        			switch($condition[0]->getOperation()){        				
        				case FilterOp::$EQUAL:
        					$operator	= " = ";
        					break;
        				case FilterOp::$NOT_EQUAL:
        					$operator = " <> ";
        					break;
        				default:
        					$operator = " = ";
        					break;
        			}
        			$filterstr = " AND " . $attname . $operator . $value;                		
                }
                
                $sql = "SELECT tblTraveler.* " .
						"FROM tblInviteList, tblTraveler " .
						"WHERE tblInviteList.travelerID = tblTraveler.travelerID " .
                		"AND tblTraveler.active = 1 " .
 						"AND tblInviteList.groupID = " . $this->mGroupID . $filterstr;
                $this->mRs->Execute($sql);

                while ($recordset = mysql_fetch_array($this->mRs->Resultset())){
					$mTraveler = new Traveler;
					$mTraveler->setTravelerData($recordset);
                    $this->mInviteList[] = $mTraveler;
                }
                
                return $this->mInviteList;                                           
            }
		
        
        /**
         * Purpose: This is called when a list of email address owners are invited by administrator to join a group.
         * Function parameter is an array of email address. All email addresses must be owned by non-GA NET members or non-Group Members.
         */
            function addEmailInvites($emailInvites){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $inviteList = $this->getInviteList();
                foreach ($emailInvites as $each){ 
                	$each = trim($each);
                	/*if (FALSE == $this->isInEmailInviteList($each)):                        	
                    	$sql = "INSERT into tblEmailInviteList (groupID, email) VALUES ('$this->mGroupID', '$each')";
                    	$this->mRs->Execute($sql);
                    endif;	*/
                    if (!in_array($each, $inviteList)):
                    	$sql = "INSERT into tblEmailInviteList (groupID, email) VALUES ('$this->mGroupID', '$each')";
                    	$this->mRs->Execute($sql);
                    endif;
                }           
            }
        
        
        /**
         * Purpose: This is called when invitation email is sent to email address owners invited by administrator to join a group.
         * Function parameter is string email address.
         */
            function sendEmailInvite($email){
                
                $mail = new PHPMailer(); 
				$mail->IsSMTP(); 
				$mail->IsHTML(false);
				
				$mail->From = "admin@goabroad.net";
				$mail->FromName = "The GoAbroad Network";
				$mail->Subject = str_ireplace("%groupname%", $this->mName, $this->subject);
				$tempmsg = str_ireplace("%groupname%", $this->mName, $this->msg);
				$mail->Body = str_ireplace("%name%", $email, $tempmsg);
				$mail->AddAddress(trim($email)); 
				
				$mail->Send();
            }
            
            function sendEmailInviteDaf($email){
                
                $mail = new PHPMailer(); 
				$mail->IsSMTP(); 
				$mail->IsHTML(false);
				
				$mail->From = "admin@goabroad.net";
				$mail->FromName = "The GoAbroad Network";
				$mail->Subject = str_ireplace("%groupname%", $this->mName, $this->subject);
				$tempmsg2 = str_ireplace("%groupname%", $this->mName, $this->msg);
				$mail->Body = str_ireplace("%name%", $email, $tempmsg2);
				$mail->AddAddress("daphne.dolina@goabroad.com"); 
				
				$mail->Send();
            }
          
          /**
           * Purpose: This is called when an alert email need to be sent to the advisor.
           */  
           
            function sendAdvisorAlert($_objAdvisorAlert){
            	
            	$mail = new PHPMailer(); 
				$mail->IsSMTP(); 
				$mail->IsHTML(false);
				
				$mail->From = $_objAdvisorAlert->getFromEmail();
				$mail->FromName = $_objAdvisorAlert->getFromName();
				$mail->Subject = $_objAdvisorAlert->getSubject();
				$mail->Body = $_objAdvisorAlert->getBody();
				$mail->AddAddress($this->getAdministrator()->getTravelerProfile()->getEmail()); 
				$mail->AddBCC('daphne.dolina@goabroad.com'); 
				
				$mail->Send();
				
            }
            
        /**
         * Purpose: Checks if email add is enlisted in Email Invite List.
         */
            function isInEmailInviteList($email){
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
            	$email = trim($email);
                $sql = "SELECT email from tblEmailInviteList WHERE groupID = '$this->mGroupID' AND email = '$email' LIMIT 0,1";
                $this->mRs->Execute($sql);
                
                if (0 == $this->mRs->Recordcount())
                    return false;  
                else 
                    return true;
            }
            
        
        /**
         * Purpose: This is called when email add is removed from Email Invite List after registering with GoAbroad Network.
         */
            function removeFromEmailInviteList($email = ''){     
                $this->mRs	=	ConnectionProvider::instance()->getRecordset();
                $sql = "DELETE FROM tblEmailInviteList WHERE groupID = '$this->mGroupID' AND email = '$email' ";
                $this->mRs->Execute($sql);                                
            }
            
	            
		/**
         * Purpose: Returns an array of email adds in the Email Invite List; pending email adds who did not register with GoAbroad Network yet.
         */
            function getEmailInviteList($filtercriteria = NULL){                  
              	$this->mRs	=	ConnectionProvider::instance()->getRecordset();
              	$this->mEmailInviteList = array();
              	$filterstr = '';
              	
                if (NULL != $filtercriteria){

                	$condition = $filtercriteria->getConditions();	
        			$attname   = $condition[0]->getAttributeName();                    			
        			$value 	   = $condition[0]->getValue();
        			
        			switch($condition[0]->getOperation()){
        				
        				case FilterOp::$EQUAL:
        					$operator	= " = ";
        					break;
        				case FilterOp::$NOT_EQUAL:
        					$operator = " <> ";
        					break;
        				default:
        					$operator = " = ";
        					break;
        			}
        			
        			$filterstr = " AND " . $attname . $operator . $value;
                		
                }
                
                $sql = "SELECT email FROM tblEmailInviteList WHERE groupID = '$this->mGroupID' " . $filterstr;
                $this->mRs->Execute($sql);
                
   				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                    try {
                        $this->mEmailInviteList[] = $recordset['email'];
                    }
                    catch (Exception $e) {
                       throw $e;
                    }                        
                }
                
                return $this->mEmailInviteList;
                                           
            }
		
		
		/**
         * Purpose: This is called when an advisor invites a non-member using email address, and assigns first and last name
         * Function parameter is  email address, firstname and lastname.
         */
            function addEmailInviteAttribute($_email = '', $_firstName = '', $_lastName = ''){
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
				$_firstName = addslashes(trim($_firstName));
				$_lastName = addslashes(trim($_lastName));
               	$sql = "UPDATE  tblEmailInviteList SET firstname = '$_firstName' , lastname = '$_lastName' WHERE email = '$_email' AND groupID = '$this->mGroupID' ";
               $this->mRs->Execute($sql);
               
				if (strpos($_SERVER['SERVER_NAME'], '.local') > 0) {
					return;
				}
				// added by chris, send invite notification from subgroup
				
				$domainConfig 		= SiteContext::getInstance();
				$siteUrl 		= $domainConfig->getServerName();

				$crypt = new Crypt();
				$urlParam = rtrim($crypt->encrypt('ID='.$this->getEmailInviteID($_email)), '=');
				$registration_link = "http://$siteUrl/confirm.php?$urlParam";
				
				$this->setCustomMessage(str_replace("[registration_link]", $registration_link, $this->getCustomMessage()));
				
				require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
				RealTimeNotifier::getInstantNotification(RealTimeNotifier::GROUP_EMAIL_INVITATION, 
					array(
						'EMAIL' 	=> $_email,
						'GROUP'		=> $this,
						'CUSTOM_MESSAGE' => $this->getCustomMessage()
					))->send();
            }

	
        /**
         * Purpose: This is called when attributes for a given email invite is retrieved. 
         */
            function getEmailInviteAttribute($email, $checkSubGroups=false){     
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
            	
            	// edited by chris: Jan. 02, 2009 added option to also check subgroups
				if( !$checkSubGroups ){
					$sql = "SELECT * from  tblEmailInviteList WHERE email = '$email'  AND groupID = '$this->mGroupID'";
				}
				else{
					$sql = 	'SELECT `tblEmailInviteList`.* ' .
							'FROM `tblEmailInviteList`, `tblGroup` ' .
							'WHERE 1 ' .
								'AND `tblEmailInviteList`.`groupID` = `tblGroup`.`groupID` ' .
								'AND `tblEmailInviteList`.`email` = "' . $email . '" ' .
								'AND (`tblGroup`.`parentID` = "' . $this->mGroupID . '" OR `tblGroup`.`groupID` = "' . $this->mGroupID . '") ';
				}
                
                $this->mRs->Execute($sql);

				$emailInviteAttribute = array();
                if ($this->mRs->Recordcount()){
					$emailInviteAttribute['firstname'] = $this->mRs->Result(0,'firstname');
					$emailInviteAttribute['lastname'] = $this->mRs->Result(0,'lastname');
				}
				
				return $emailInviteAttribute;
                                             
            }
	
		
		/**
         * Purpose: This is called when an email invitation message is retrieved per email address invited to a group.
         */
            function getEmailInvitationMessagePerEmail($_email){     
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();	
            	
                $sql = "SELECT tblEmailInvitationMessage.message 
						FROM tblEmailInviteList,  tblEmailInvitationMessage 
						WHERE tblEmailInviteList.emailInvitationMessageID = tblEmailInvitationMessage.emailInvitationMessageID AND  tblEmailInviteList.email = '$_email'
							AND tblEmailInviteList.groupID = ".$this->getGroupID(); //edited by Jul: added the where condition for groupID since an email can be invited to different groups
                $this->mRs->Execute($sql);

				$emailInvitationMsg = '';
                if ($this->mRs->Recordcount()){
					$emailInvitationMsg = $this->mRs->Result(0,'message');
				}

				return $emailInvitationMsg;

            }
		
		/**
         * Purpose: This is called when an advisor adds a personalized message to be appended to the invitation
         * Function parameter is  message
         */
            function addEmailInvitationMessage($_message = ''){
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
            	$xmessage = addslashes($_message);
				$now  = date("Y-m-d H:i:s");
				$sql = "INSERT into tblEmailInvitationMessage (message, dateadded) VALUES ('$xmessage' , '$now' )";
               	$this->mRs->Execute($sql);
  				return $this->mRs->GetCurrentID();
            }

		/**
         * Purpose: This is called when an advisor adds a personalized message to be appended to the invitation
         * Function parameter is  email address and messageID
         */
            function addMessageToEmailInvite($_email = '', $_messageID = 0){
				$this->mRs	=	ConnectionProvider::instance()->getRecordset();
				$sql = "UPDATE tblEmailInviteList SET emailInvitationMessageID = '$_messageID'  WHERE email = '$_email' AND groupID = '$this->mGroupID' ";
               	$this->mRs->Execute($sql);

            }
	
	
        /**
         * Purpose: Adds a group event to this group.
         */ 
            function addGroupEvent($groupEvent){
                
                $groupEvent->setGroupID($this->mGroupID);                     // set this groupID for group event
                $groupEvent->Create();                                        // create group event
            }
            
        /**
         * Purpose: Removes a group event from this group.
         */ 
            function removeGroupEvent($groupEventID){
                
                $groupEvent = new GroupEvent($groupEventID);                  // instantiate group event
                $groupEvent->Delete();                                        // delete group event for fun group
            }
            
        /**
         * Purpose: Returns an array of group events of this fun group.
         * Rows Limit is enabled for this function.
         */ 
            function getGroupEvents($rowslimit = null, $startdate = null, $filter = null, $variance = false){
                
               // if (0 == count($this->mGroupEvents)){
               
               $this->mGroupEvents = array();
                    
                    $limitstr  = ''; 
                    $sqlstrdate = '';
                    $filterstr = '';
                    $sortorder = ' DESC ';
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    
                    if ($variance){
                    	$sqlstrdate = " AND CalendarEvent.adate < '$startdate' " ;
                    	$sortorder = " DESC ";
                    }
                    
                    else if (NULL != $startdate){
	                	$sqlstrdate = " AND CalendarEvent.adate >= '$startdate' " ;
	                
                    }
	                
	                if (NULL != $filter){
	
	                	$condition = $filter->getConditions();	
	        			$attname   = $condition[0]->getAttributeName();                    			
	        			$value 	   = $condition[0]->getValue();
	        			
	        			switch($condition[0]->getOperation()){
	        				
	        				case FilterOp::$EQUAL:
	        					$operator = " = ";
	        					break;
	        				case FilterOp::$NOT_EQUAL:
	        					$operator = " <> ";
	        					break;
	        				default:
	        					$operator = " = ";
	        					break;
	        			}
	        			
	        			$filterstr = " AND " . $attname . $operator . $value;
	                		
	                }
	                
                    //$sql = "SELECT groupeventID from tblGroupEvent WHERE groupID = '$this->mGroupID' " . $sqlstrdate . $filterstr . " ORDER BY groupeventID " . $limitstr;
                    $sql = "SELECT * from tblGroupEvent as CalendarEvent WHERE groupID = '$this->mGroupID' " . $sqlstrdate . $filterstr . " ORDER BY adate " . $sortorder . $limitstr;
                  
                    //$this->mRs->Execute($sql);
					$this->mRs	=	ConnectionProvider::instance()->getRecordset();
                    $this->mRs->Execute($sql);
                    
                    while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                        try {
                            $groupevent = new GroupEvent($recordset['groupeventID']);
                        }
                        catch (Exception $e) {
                           throw $e;
                        }
                        $this->mGroupEvents[] = $groupevent;
                    }
                
                return $this->mGroupEvents;
            } 
                       
		/**
		 * privacy preference for groups (added by: K. Gordo)
		 */
			public function getPrivacyPreference(){
				$pp = null;
				try {
					$pp = new GroupPrivacyPreference($this->mGroupID);
				} catch (InstantiationException $ie){
					$pp = new GroupPrivacyPreference();
					$pp->setGroupID($this->mGroupID);
				}	
				return $pp;
			}
		
		
		/**
		 * Static Function
     * Purpose: Returns an array of groups based on filter conditions
     * Rows Limit is enabled for this function.
     */ 
	 	public static function getFilteredGroups($filter = null, $rowslimit = null, $order = null, $groupsql = null, $loggeduserID = null){
			$arrFilteredGroup = array();

 			try {
	 			//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
				$mRs2	= ConnectionProvider::instance()->getRecordset2();
 			}
			catch (Exception $e){   
			   throw $e;
			}

			$mCondition	= array();
			$mFilterStr	= '';
			$mLimitStr	= '';
			$mOrderStr	= '';
		 	if (!is_null($order)) {
				require_once "travellog/model/Class.Condition.php";
		
				$mCondition = $order->getConditions();	
				$mBooleanOp = $order->getBooleanOps();
				$mAttName = $mCondition[0]->getAttributeName();                    			
				$mValue	= $mCondition[0]->getValue();
		
				switch($mCondition[0]->getOperation()){        				
					case FilterOp::$ORDER_BY_DESC:
						$mOrderStr = " ORDER BY " . $mAttName . " DESC ";
						break;
					default:
						$mOrderStr = " ORDER BY " . $mAttName . " ASC ";
				}
			}
			else {
				$mOrderStr = " ORDER BY name ASC ";
			}
			$mGroupBy	= '';

			if (NULL != $filter){
            	$mCondition = $filter->getConditions();	
       			$mBooleanOp = $filter->getBooleanOps();

       			for ($i = 0; $i < count($mCondition); $i++){
        			$mAttName = $mCondition[$i]->getAttributeName();                    			
        			$mValue	= $mCondition[$i]->getValue();

        			switch($mCondition[$i]->getOperation()){        				
        				case FilterOp::$EQUAL:
        					$mOperator = " = ";
        					break;
        				case FilterOp::$NOT_EQUAL:
        					$mOperator = " <> ";
        					break;
        				case FilterOp::$GREATER_THAN_OR_EQUAL:
        					$mOperator = " >= ";
        					break;
        				case FilterOp::$LIKE:
        					$mOperator = " LIKE ";
        					$mValue    = "%" . $mValue . "%"; 
        					break;
        				default:
        					$mOperator = " = ";
        					break;
        			}        			
        			$mFilterStr .=  " " . $mBooleanOp[$i] . " ". $mAttName . $mOperator . " '$mValue' ";
       			}       			             		
			}

			if (NULL != $rowslimit){
	 			$mOffset = $rowslimit->getOffset();
	 			$mRows =  $rowslimit->getRows();
	 			$mLimitStr = " 	LIMIT " . $mOffset . " , " . $mRows;
	 		}	
	 		
	 		if (NULL != $groupsql){
	 			$mCondition	= $groupsql->getConditions();	
       			$mAttName	= $mCondition[0]->getAttributeName();   
       			$mGroupBy	= " GROUP BY " . $mAttName;
	 		}	
	 		
	 		if (NULL != $loggeduserID){
	 			$sql = "SELECT tblGroup.* " .
						"FROM tblGroup, tblGrouptoTraveler, tblTraveler " .
	 					"WHERE tblGroup.groupID = tblGrouptoTraveler.groupID " .
						"AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID " .
	 					"AND tblGroup.isSuspended =  0 ".
						"AND tblTraveler.active = 1 " .
	 					"AND tblGrouptoTraveler.travelerID = " . $loggeduserID .
	 					 $mFilterStr . $mGroupBy .
	 					 $mOrderStr . $mLimitStr;
			}
	 		else{
	 			$sql = "SELECT tblGroup.* " .
	 					"FROM tblGroup, tblTraveler " .
	 					"WHERE tblGroup.administrator = tblTraveler.travelerID " . 
	 					"AND tblGroup.isSuspended =  0 ".					
						"AND tblTraveler.active = 1 " .
	 					$mFilterStr . $mGroupBy .
	 					$mOrderStr . $mLimitStr;
			}

	 		$mRs->Execute($sql);

	 		$factory	=	GroupFactory::instance();
			$arrGroupId = array();
	 		while ($recordset = mysql_fetch_array($mRs->Resultset())){
				/*if (0 == $recordset['parentID'] )
					$arrGroupId[] = $recordset['groupID'];
				else
					$arrGroupId[] = $recordset['parentID'];*/
	 			
	 			$arrFilteredGroup[]	=	$factory->createGroupObject($recordset);
	 			
			}
	
			/*if (is_null($order)) {
        		$cnd = new Condition;
			 	$cnd->setAttributeName("inverserank");
			  	$cnd->setOperation(FilterOp::$ORDER_BY_DESC);
      
        		$order = new FilterCriteria2();
			  	$order->addCondition($cnd);		
			}*/
			
			
			return $arrFilteredGroup;				
 		}
	 	
	 	
	 	/**
		 * Static Function
         * Purpose: Returns an array of suspended groups
         */ 
	 		public static function getSuspendedGroups(){
				$arrGroupId = array();
	 			$suspendedGroups = array();

	 			try {
		 			//$conn = ConnectionProvider::instance()->getConnection();
					$rs   = ConnectionProvider::instance()->getRecordset();
	 			}
				catch (Exception $e) {
				   throw $e;
				}

	 			$sql = "SELECT tblGroup.groupID, tblGroup.parentID " .
	 					"FROM tblGroup " .
	 					"WHERE tblGroup.isSuspended =  1 " ;	
		 		$rs->Execute($sql);
		 		
		 		while ($recordset = mysql_fetch_array($rs->Resultset()))
					$arrGroupId[] = $recordset['groupID'];
				$suspendedGroups = GroupFactory::instance()->create($arrGroupId);

 				return $suspendedGroups; 				
	 		}
	 		
	 	
	 	/**
		 * Static Function
         * Purpose: Returns an array of groups based on selected category
         * Rows Limit is enabled for this function.
         */ 
	 	public static function getGroupsByCategory($categoryID, $rowslimit = null, $order = null){	 
 			$arrGroupId = array();
			$arrGroupsByCategory = array();

 			try {
	 			//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {
			   throw $e;
			}

			$mLimitStr   = '';
			$mOrderStr   = ''; 
			
			if ($rowslimit){
	 			$offset = $rowslimit->getOffset();
	 			$rows =  $rowslimit->getRows();
	 			$mLimitStr = " 	LIMIT " . $offset . " , " . $rows;
	 		}

	 		$sql = "SELECT DISTINCT tblGroup.groupID " .
					"FROM tblGroup, tblGrouptoCategory " .
	 				"WHERE tblGroup.groupID = tblGrouptoCategory.groupID " .
	 				"AND tblGroup.parentID  = 0 " .
	 				"AND tblGroup.isSuspended =  0 " .
	 				"AND tblGrouptoCategory.categoryID = " . $categoryID . $mLimitStr;
	 		
	 		$mRs->Execute($sql);

	 		while ($recordset = mysql_fetch_array($mRs->Resultset()))
				$arrGroupId[] = $recordset['groupID'];
			
			if (is_null($order)) {
        $cnd = new Condition;
			  $cnd->setAttributeName("inverserank");
			  $cnd->setOperation(FilterOp::$ORDER_BY_DESC);
      
        $order = new FilterCriteria2();
			  $order->addCondition($cnd);		
			}	
		  
			$arrGroupsByCategory = GroupFactory::instance()->create($arrGroupId, $order);

			return $arrGroupsByCategory;
		}
	 		
	 		
		/**
		 * Static Function
         * Purpose: Returns the administratorID of the group created by this client.
         */ 
	 		public static function getClientGroup($clientID){
	 			try {
		 			//$mConn	= ConnectionProvider::instance()->getConnection();
					$mRs	= ConnectionProvider::instance()->getRecordset();
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT groupID FROM tblGrouptoAdvisor " .
		 				"WHERE clientID = '$clientID' ";
		 		$rs->Execute($sql);

	 			$administratorID = 0;

		 		if (0 < $rs->Recordcount()){
			 		while ($recordset = mysql_fetch_array($rs->Resultset())) {
		 				try {
	 						$clientgroup = GroupFactory::instance()->create(array($recordset['groupID']));
	 					}
						catch (Exception $e){
						   throw $e;
						}
						$administratorID = $clientgroup[0]->getAdministrator()->getTravelerID();						
	 				}
 				}

 				return $administratorID; 				
	 		}	
	 	
	 	/**
		 * Static Function
         * Purpose: Returns true if group name already exists; false if otherwise
         */ 
	 		public static function isExistGroupName($_name, $_groupID){
	 			
	 			try {
		 			//$conn = ConnectionProvider::instance()->getConnection();
					$rs   = ConnectionProvider::instance()->getRecordset();
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				$xname         = addslashes($_name);
				$sql = "SELECT groupID FROM tblGroup " .
		 				"WHERE name = '$xname' and groupID <> '$_groupID' ".
						"LIMIT 0,1";
		 		$rs->Execute($sql);
		 		
		 		if (0 == $rs->Recordcount())
                    return false;           
                else 
			 		return true;
	 		}	
	 			
		
		 /**
         * Purpose: Add photo for this group
         * .
         */
            function addPhoto($photo){
                
                try {
		 			//$conn = ConnectionProvider::instance()->getConnection();
					$rs   = ConnectionProvider::instance()->getRecordset();
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				//moved out from the if condition below by Jul
				require_once("travellog/model/Class.PathManager.php");
				$path = new PathManager($this,'image');
				
                if($this->getPhotos()){
               		$objphoto = $this->getPhotos();                		
                	require_once('travellog/model/Class.UploadManager.php');
               		$uploadmanager = new UploadManager();
					$uploadmanager->setDestination($path);	
					$uploadmanager->setTypeUpload('image');
					$uploadmanager->deletefile($objphoto[0]->getFileName());
                }
                
                
                $photoID = $photo->getPhotoID();
                $sql = "UPDATE tblGroup SET photoID = '$photoID' WHERE groupID ='$this->mGroupID'";
                $rs->Execute($sql);   

             	//added by Jul to fix distorted thumbnail	
				try{
					require_once("travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionUploadedPhotoFactory.php");
					$upPhoto = PhotoCollectionUploadedPhotoFactory::getInstance()->create('generic');
					$upPhoto->fixGroupLogoThumbnail($path,$this->getGroupPhoto());
            	}catch(exception $e){
				}
		}
          
	 	/**
	 	 * Retrieves the photos of this group
	 	 * 
	 	 * @param boolean $return_count
	 	 */
	 	function getPhotos($return_count = false){
	 			
	 			try {
		 			//$conn = ConnectionProvider::instance()->getConnection();
					$rs   = ConnectionProvider::instance()->getRecordset();
	 			}
				catch (Exception $e) {				   
				   throw $e;
				}
 			
 				$sql = "SELECT tblPhoto.photoID from tblGroup,tblPhoto WHERE tblGroup.photoID = tblPhoto.photoID AND  groupID = '$this->mGroupID'";
 				$rs->Execute($sql);
 			
 				$recordset = mysql_fetch_array($rs->Resultset());
 				
 				if ($return_count) {
 					return $rs->Recordcount();
 				}
 				else {		
 				  if($rs->Recordcount()){
 					  try {	
 						  $photo = new Photo($this, $recordset['photoID']);
 				 	  }
					  catch (Exception $e) {
					    throw $e;
					  }
 					  
 					  $this->arrphotos[] = $photo;
 				  }
 				}
				
				return $this->arrphotos;
	 		}
	 
	   function removePhoto($photoID){
                
            try {
	 			//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
            
            $sql = "UPDATE tblGroup SET photoID =0 WHERE groupID ='$this->mGroupID'";
	 		$rs->Execute($sql);   
       }
       
       
       /**
        * Remove all records related to this group to be deleted
        */
       function removeAllGroupLinks(){
       		try {
	 			//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}

			$sql = "DELETE FROM tblGrouptoAdvisor " .								// remove relation of group to GA client
					"WHERE groupID = '$this->mGroupID' " ;
			$rs->Execute($sql);

            $mygroupevents = $this->getGroupEvents();
            $mygroupmembers = $this->getMembers();
            $myrequestlist = $this->getRequestList();
            $myinvitelist = $this->getInviteList();

	        for($idx=0;$idx<count($mygroupevents);$idx++){
            	$eachgroupevent = $mygroupevents[$idx]; 
                $eachgroupevent->Delete();                                      	// delete events of group
            }

            for($idx=0;$idx<count($mygroupmembers);$idx++){
            	$eachgroupmember = $mygroupmembers[$idx]->getTravelerID(); 
                $this->removeMember($eachgroupmember);                                   	// delete members of group
            }

			// since, advisor is not a member of the group (in the query), we have to manually delete him from the group
			$this->removeMember($this->mAdministratorID);

            for($idx=0;$idx<count($myrequestlist);$idx++){
            	$eachrequest = $myrequestlist[$idx]->getTravelerID(); 
                $this->removeFromRequestList($eachrequest);                        	// delete request list of group
            }

            for($idx=0;$idx<count($myinvitelist);$idx++){
            	$eachinvite = $myinvitelist[$idx]->getTravelerID(); 
                $this->removeFromInviteList($eachinvite);                           // delete invite list of group
            }

            $this->removeMembershipUpdates($this->mGroupID);						// remove membership updates of group  
			$this->removeGroupCategory();											// remove relation of group to its category

			/******************* START:: Added By: Cheryl Ivy Q. Go		20 October 2008 ********************/
				// Delete all personal messages related to group
				PersonalMessage::DeleteAllGroupPM($this->mSendableID);

				// Delete all bulletin messages related to group
				BulletinMessage::DeleteAllGroupBM($this->mSendableID);

				// Delete all alert messages related to group
				AlertMessage::DeleteAllGroupAM($this->mSendableID);
			/******************** END:: Added By: Cheryl Ivy Q. Go		20 October 2008 ********************/

			/**
            $sql2 = "DELETE FROM tblGroupPrivacyPreference " .						// remove relation of group to its privacy preferences
					"WHERE groupID = '$this->mGroupID' " ;
			$rs->Execute($sql2);

			$sql3 = "DELETE FROM tblSendable " .									// remove relation of group to its sendableID
					"WHERE sendableiD = '$this->mSendableID' " ;
			$rs->Execute($sql3);
			*/
		}
       
       /**
        * Suspend group
        */
        
       function Suspend(){
       	
       		try {
	 			//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			
			$sql = "UPDATE tblGroup " .
					"SET isSuspended = 1 " .
					"WHERE groupID = '$this->mGroupID' " ;
       		$rs->Execute($sql);
       		
       }

		function isProfileComplete(){
			$complete = FALSE;
			if($this->getDescription() && ($this->getGroupPhoto() instanceOf Photo) && !$this->getPrivacyPreference()->isPreferenceDefault()){
				$complete = TRUE;
			}
			return $complete;
		}
       
       /**
        * Checks if group is suspended
        */
        
       function IsSuspended(){
       	
       		try {
	 			//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			
			$sql = "SELECT isSuspended FROM tblGroup " .
					"WHERE groupID = '$this->mGroupID' " .
					"AND isSuspended = 1 ".
					"LIMIT 0,1" ;
       		$rs->Execute($sql);
       		
       		 if (0 == $rs->Recordcount())
                return false;  
            else 
                return true;  
       		
       }
       
       /**
        * Activate group
        */
        
       function Activate(){
       	
       		try {
	 			//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			
			$sql = "UPDATE tblGroup " .
					"SET isSuspended = 0 " .
					"WHERE groupID = '$this->mGroupID' " ;
       		$rs->Execute($sql);
       		
       }
       
       
       function getTravels(){
       		return array();
       }
	 	
	 /**
		 * user defined sort function to be called when sorting by date
		 * edited by K. Gordo 
		 */
		 
		function orderByName($l,$r){
			if ($l->getUserName() == $r->getUserName())
			   return 0;
			elseif ($l->getUserName() < $r->getUserName())
				return -1;
			else 
				return 1;	  
		} 
		
		/**
		* user defined sort function to be called when sorting by lastname
		 * added by daf
		 */

		function orderByLastName($l,$r){
			
			$lProfileLastName = strtolower($l->getTravelerProfile()->getLastName());
			$rProfileLastName = strtolower($r->getTravelerProfile()->getLastName());
			
			if ($lProfileLastName == $rProfileLastName)
			   return 0;
			elseif ($lProfileLastName < $rProfileLastName)
				return -1;
			else 
				return 1;	  
		}
		
		
		
		function getAbsoluteRank($_rank = 0){
			
			try {
	 			//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			 $_rank = $_rank - 1;
			
			$sql = "SELECT inverserank FROM tblGroup WHERE parentID = 0 AND isSuspended = 0 " .
					"ORDER BY inverserank DESC " .
					"LIMIT $_rank , 1" ;
       		$rs->Execute($sql);
       		
       		return $rs->Result(0,'inverserank');
       		
		}
			
				
		function updateRankCascade($_uBound = 0, $_lBound = 0, $_operand = "+"){
			
			try {
	 			//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			
			$sql = "UPDATE tblGroup " .
					"SET inverserank = inverserank " . $_operand . " 1 " .
					"WHERE inverserank <= " . $_uBound . 
					" AND inverserank >= " . $_lBound . 
					" AND groupID <> " . $this->mGroupID;
       		$rs->Execute($sql);
       		
			
		}
		
		function updateNewRank($_newAbsRank){
			
			try {
	 			//$conn = ConnectionProvider::instance()->getConnection();
				$rs   = ConnectionProvider::instance()->getRecordset();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			
			$sql = "UPDATE tblGroup " .
					"SET inverserank = " . $_newAbsRank . 
					" WHERE groupID = " . $this->mGroupID;
       		$rs->Execute($sql);
       		
			
		}

		function getGroupMembershipLink($referer='',$page=1){
			$strLink = '';
			$refLink = '';
			$header = '';
			$message = '';
			$path = '';
			$action = ''; 
				
			// if logged in
			if (isset($_SESSION['travelerID'])) { 
				$loggedUser = new Traveler($_SESSION['travelerID']);
				$group_name = str_replace("'","&lsquo;",$this->getName());
				//check if not advsor and this is an open or closed group
				//if ( !$loggedUser->isAdvisor() && $loggedUser->getTravelerID() != $this->getAdministratorID()){
				if ( $loggedUser->getTravelerID() != $this->getAdministratorID()){

					//create ref link if referer paramater is passed, string to be added to URL
					if (strlen($referer))
						$refLink = "&amp;ref=". $referer;
					if (in_array($this->getGroupAccess(), array(GroupAccess::$OPEN, GroupAccess::$OPEN_APPROVAL))) {
						// check if is in request list already
						if ($this->isInRequestList($loggedUser)){
							$header  = 'Cancel Request ?';
							$confirmation = 'Are you sure you want to cancel your request to join the '.strtolower($this->getLabel()).' '.$group_name.'?';
							$path    = '/membership.php?mode=cancel&amp;gID='.$this->mGroupID. $refLink .'&confirm&page='.$page;
							$action  = 'Yes';
							$type    = '0';
							$strLink = '<a href="javascript:void(0)" onclick="CustomPopup.initialize(\''.$header.'\',\''.$confirmation.'\',\''.$path.'\',\''.$action.'\',\''.$type.'\');CustomPopup.createPopup();">Cancel Request to Join</a>';
						}
						// if not member
						else if (!$this->isMember($loggedUser)){
							$header  = 'Join '.$group_name.' ?';
							$confirmation = 'Are you sure you want to join the '.strtolower($this->getLabel()).' '.$group_name.'?';
							$path    = '/membership.php?mode=join&amp;gID='.$this->mGroupID. $refLink .'&confirm&page='.$page;
							$action  = 'Join';
							$type    = '0';
							$message = 'You are about to join the '.strtolower($this->getLabel()).' '.$group_name.'. Your request is subject to the administrator&lsquo;s approval and will be sent after clicking Join. You will be able to interact with this group once your request has been approved.';
							if($this->getGroupAccess() == GroupAccess::$OPEN)
								$message = 'You are about to join ' . $group_name.'. You will automatically be a member of the group after clicking Join.';
								
							$strLink = '<a href="javascript:void(0)" onclick="CustomPopup.initialize(\''.$header.'\',\''.$confirmation.'\',\''.$path.'\',\''.$action.'\',\''.$type.'\',\''.$message.'\');CustomPopup.createPopup();">Join this ' .$this->getLabel() . '</a>';
						}	
					} 
						
					// if member
					if ($this->isMember($loggedUser) AND (self::FUN == $this->getDiscriminator() || 0 == $this->getParentID() || (false == $this->getParent()->isStaff($_SESSION['travelerID'])))){
						$header  = 'Leave '.$group_name.' ?';
						$confirmation = 'Are you sure you want to remove your membership from the '.strtolower($this->getLabel()).' '.$group_name.'?';
						$path    = '/membership.php?mode=leave&gID='.$this->mGroupID. $refLink .'&confirm&page='.$page;
						$action  = 'Remove';
						$type    = '0';
						$strLink = '<a href="javascript:void(0)" onclick="CustomPopup.initialize(\''.$header.'\',\''.$confirmation.'\',\''.$path.'\',\''.$action.'\',\''.$type.'\');CustomPopup.createPopup();">Leave ' .$this->getLabel() . '</a>';
					}	
				}		
					
				if (($loggedUser->getTravelerID() == $this->getAdministratorID()) && (Group::FUN == $this->mDiscriminator)){
					if (strlen($referer))
						$refLink = "&amp;ref=". $referer;
					$header  = 'Delete '.$group_name.' ?';
					$confirmation = 'Are you sure you want to delete the '.strtolower($this->getLabel()).' '.$group_name.'?';
					$path    = '/group.php?mode=delete&amp;gID=' . $this->mGroupID. $refLink .'&confirm&page='.$page;
					$action  = 'Delete';
					$type    = '0';
					$strLink = '<a href="javascript:void(0)" onclick="CustomPopup.initialize(\''.$header.'\',\''.$confirmation.'\',\''.$path.'\',\''.$action.'\',\''.$type.'\');CustomPopup.createPopup();">Delete ' .$this->getLabel() . '</a>';
				}	
			}

			return $strLink ;			
		}
		
		function isMemberOfGroupNetwork($props){
			//$conn = ConnectionProvider::instance()->getConnection();
			$rs   = ConnectionProvider::instance()->getRecordset();
			$sql  = sprintf
					(
						'SELECT travelerID FROM tblGrouptoTraveler ' .
						'WHERE travelerID = %d ' .
						'AND groupID IN (SELECT groupID FROM tblGroup WHERE (parentID = %d OR groupID = %d) ) ' .
						'LIMIT 1'
						,$props['tID']
						,$props['gID']
						,$props['gID']
					);
			$rs->Execute($sql); 
			/*if( $_SERVER['REMOTE_ADDR'] == '121.96.35.154' ){
				echo "$sql<br />";
				echo "Recordcount " . $rs->Recordcount();
				exit;
			}*/
			return $rs->Recordcount();			
		}
		
		function isPendingMemberOfGroupNetwork($travelerID=0){
            $rs	=	ConnectionProvider::instance()->getRecordset();
			//check if has pending invitation
            $sql = "SELECT travelerID
					FROM tblInviteList, tblGroup
					WHERE tblInviteList.travelerID = '$travelerID'
						AND (tblGroup.groupID = '$this->mGroupID'
							OR tblGroup.parentID = '$this->mGroupID')
						AND tblInviteList.groupID = tblGroup.groupID
						AND (tblInviteList.status = 0 OR tblInviteList.status = 2)
					LIMIT 0,1";
            $rs->Execute($sql);
			
            if (0 == $rs->Recordcount()){
				//not invited, check if is requesting to join group
				$sql2 = "SELECT travelerID
						 FROM tblRequestList, tblGroup
						 WHERE travelerID = '$travelerID'
							AND status = 0
							AND (tblGroup.groupID = '$this->mGroupID'
								OR tblGroup.parentID = '$this->mGroupID')
							AND tblRequestList.groupID = tblGroup.groupID
						LIMIT 0,1";
			    $rs->Execute($sql2);
                return 0 < $rs->Recordcount(); 
            }else{
				return TRUE;
			}
        }
		
		function isSubGroup(){
			return false;
		}
		
		function getSubGroups(){
			return array();
		}
		
		function getFriendlyURL(){
			
			$subgroup_name = str_replace('-', '_', $this->mName);
			$subgroup_name = str_replace(' ', '-', $subgroup_name);
			
			if ($this->isSubGroup()){
				$group_name = str_replace('-', '_', $this->getParent()->getName());
				$group_name = str_replace(' ', '-', $group_name);
				
				$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;
			}
			else {
				$friendlyURL = "/groups/". str_replace(' ','-',$subgroup_name) ;
			}
			return $friendlyURL;
		}	
		
		/**
		 *	@author Cheryl Ivy Q. Go		11 March 2008
		 *	returns the status of invitation
		 */
        static function getInvitationToGroupStatus($_groupId = 0, $_travelerId = 0){
			try{
				//$mConn = ConnectionProvider::instance()->getConnection();
				$mRs   = ConnectionProvider::instance()->getRecordset();
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "SELECT status " .
					"FROM tblInviteList " .
					"WHERE groupID = " . $_groupId . " " .
					"AND travelerID = " . $_travelerId;
			$mRs->Execute($sql);

			if (0 < $mRs->Recordcount())
				return $mRs->Result(0, "status");
			else
				return 0;
		}
	
		function getParent(){
			return null;
		}
				
		function getStaffofSubGroups(){

			return array();
		}		 	
		
		function getSubGroupsByTravelerID(){

			return array();
		}		 	
		
		function getSubGroupInvitationsByTravelerID(){

			return array();
		}		 	
		
		
		public static function getEmailInvitedByGroups($email){
			
			$rs = ConnectionProvider::instance()->getRecordset();
			$sql = 	"SELECT groupID, firstname, lastname, email " .
					"FROM tblEmailInviteList " .
					"WHERE email = '{$email}'";
			$rs->execute($sql);
			$arrGroup = array();
			while( $row = mysql_fetch_assoc($rs->ResultSet()) )
				$arrGroup[] = $row;
			
			return $arrGroup;			
			
		}
		
		public static function getMembersInvitesAndRequests($groupID, $isAdminLogged, $rowsLimit = null){			
			try{
				//$mConn = ConnectionProvider::instance()->getConnection();
				$mRs   = ConnectionProvider::instance()->getRecordset();
			}
			catch(exception $e){
				throw $e;
			}
			
			$sql = "SELECT SQL_CALC_FOUND_ROWS username, tblTraveler.travelerID, trim(loginTime) AS isOnline " .
					"FROM tblTraveler LEFT JOIN tblSessions " .
					"ON (tblSessions.session_user_id = tblTraveler.travelerID " .
					"AND domain = '{$_SERVER['SERVER_NAME']}') WHERE tblTraveler.travelerID IN " .
					"( ";
			if($isAdminLogged)
				$sql .= "SELECT travelerID FROM tblRequestList WHERE groupID = '{$groupID}' AND status = 0 " .
					"	UNION " .
					"   SELECT travelerID FROM tblInviteList WHERE status = 0 AND groupID = '{$groupID}' " .
					"	UNION ";
			/*$sql .= "   SELECT travelerID FROM tblGrouptoTraveler WHERE groupID = '{$groupID}' ) 
					AND tblTraveler.travelerID NOT IN (select distinct travelerID from tblGrouptoAdvisor) 
					GROUP BY travelerID ORDER BY username LIMIT ";*/
			$sql .= "   SELECT travelerID FROM tblGrouptoTraveler WHERE groupID = '{$groupID}' )  
					GROUP BY travelerID ORDER BY username LIMIT ";
			$sql .=	(string)($rowsLimit->getOffset() * $rowsLimit->getRows()) - $rowsLimit->getRows() . "," . (string)$rowsLimit->getRows();
				
			$query = $mRs->Execute($sql);
			$arr = array(
				'members'	=>	array(),
				'count'		=>	0
			);
			
			while( $row = mysql_fetch_array($query) ){
				$nTrav = new Traveler;
				$nTrav->setTravelerID($row['travelerID']);
				$nTrav->setUsername($row['username']);
				$nTrav->setOnline(trim($row['isOnline']));
				$arr['members'][] = $nTrav;
			}
			
			$sql = "SELECT FOUND_ROWS() AS count";
			$mRs->Execute($sql);
			$arr['count'] = $mRs->Result(0, "count");
			
			return $arr;
					
		}

		public static function getAdminFeaturedGroupPhotos($_filterCriteria2 = null, $_rowsLimit = null){
 			try {
	 			//$mConn	= ConnectionProvider::instance()->getConnection();
				$mRs	= ConnectionProvider::instance()->getRecordset();
				$mRs2	= ConnectionProvider::instance()->getRecordset2();
 			}
			catch (Exception $e) {				   
			   throw $e;
			}

 			$arrGroupPhotos = array();

			if ($_filterCriteria2){
            	$mCondition = $_filterCriteria2->getConditions();	
    			$mBooleanOp = $_filterCriteria2->getBooleanOps();
    			
    			for($i = 0; $i < count($mCondition); $i++){
        			$mAttName	= $mCondition[$i]->getAttributeName();                    			
        			$mValue		= $mCondition[$i]->getValue();

        			switch($mCondition[$i]->getOperation()){
        				case FilterOp::$EQUAL:
        					$mOperator = " = ";
        					break;
        				case FilterOp::$NOT_EQUAL:
        					$mOperator = " <> ";
        					break;
        				case FilterOp::$GREATER_THAN_OR_EQUAL:
        					$mOperator = " >= ";
        					break;
        				case FilterOp::$LIKE:
        					$mOperator = " LIKE ";
        					$mValue    = "%" . $mValue . "%"; 
        					break;
        				default:
        					$mOperator = " = ";
        					break;
        			}
        			$mFilterStr =  " WHERE ". $mAttName . $mOperator . " '$mValue' ";
    			}
            }
			else
				$mFilterStr = "";

			if ($_rowsLimit){
	 			$mOffset = $_rowsLimit->getOffset();
	 			$mRows =  $_rowsLimit->getRows();
	 			$mLimitStr = " 	LIMIT " . $mOffset . " , " . $mRows;
	 		}
			else
				$mLimitStr = "";

			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " .
				"( " .
					"SELECT tblPhoto.*, tblPhotoAlbumtoPhoto.position AS position, tblPhotoAlbumtoPhoto.photoalbumID as genID, tblGroup.discriminator, tblGroup.inverserank, tblGroup.name " .
					"FROM tblPhotoAlbumtoPhoto, tblPhoto, tblPhotoAlbum, tblGroup " .
					"WHERE tblPhotoAlbumtoPhoto.photoID = tblPhoto.photoID " .
					"AND tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID " .
					"AND tblPhotoAlbum.groupID = tblGroup.groupID " .
					"AND tblPhotoAlbumtoPhoto.photoalbumID IN " .
					"(" .
						"SELECT DISTINCT photoalbumID FROM tblPhotoAlbum WHERE groupID IN " .
						"(" .
							"SELECT tblGroup.groupID " .
				 			"FROM tblGroup, tblTraveler " .
			 				"WHERE tblTraveler.travelerID = tblGroup.administrator " . 
							"AND tblGroup.isSuspended = 0 " .
							"AND tblGroup.discriminator = 2 " .
							"AND tblGroup.parentID = 0 " .
							"AND tblTraveler.active = 1 " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblTraveler.deactivated = 0 " .
						") " .
						" UNION " .
						"SELECT DISTINCT photoalbumID FROM tblPhotoAlbum WHERE groupID IN " .
						"(" .
							"SELECT tblGroup.parentID AS groupID " .
				 			"FROM tblGroup, tblTraveler " .
			 				"WHERE tblTraveler.travelerID = tblGroup.administrator " . 
							"AND tblGroup.isSuspended = 0 " .
							"AND tblGroup.discriminator = 2 " .
							"AND tblGroup.parentID > 0 " .
							"AND tblTraveler.active = 1 " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblTraveler.deactivated = 0 " .
						") " .
					")" .
					" UNION " .
					"SELECT tblPhoto.*, tblGrouptoPhoto.position AS position, tblGrouptoPhoto.groupID as genID, tblGroup.discriminator, tblGroup.inverserank, tblGroup.name " .
					"FROM tblGroup, tblTraveler, tblGrouptoPhoto, tblPhoto " .
					"WHERE tblPhoto.photoID = tblGrouptoPhoto.photoID " .
					"AND tblGrouptoPhoto.groupID = tblGroup.groupID " .
					"AND tblGroup.administrator = tblTraveler.travelerID " .
					"AND tblGroup.discriminator = 1 " .
					"AND tblGroup.isSuspended = 0 " .
					"AND tblTraveler.active = 1 " .
					"AND tblTraveler.isSuspended = 0 " .
					"AND tblTraveler.deactivated = 0 " .
				") as tblTemp2 " .
				$mFilterStr .
				"ORDER BY photoID DESC " .
				$mLimitStr;
			$mRs->Execute($sql);

			$sql2 = "SELECT FOUND_ROWS() AS totalrecords";
			$mRs2->Execute($sql2);

			Group::$statvar = $mRs2->Result(0, "totalrecords");
	 		$arrGroupPhotos = array();

	 		while ($recordset = mysql_fetch_assoc($mRs->Resultset())) {
				if ($recordset['discriminator'] == 2)
					$mContext = new PhotoAlbum($recordset['genID']);
				else
					$mContext = new FunGroup($recordset['genID']);

				$Photo = new Photo($mContext);
				$Photo->setPhotoID($recordset['photoID']);
				$Photo->setCaption($recordset['caption']);
				$Photo->setPhotoTypeID($recordset['phototypeID']);
				$Photo->setPrimaryPhoto($recordset['primaryphoto']);
				$Photo->setDateuploaded($recordset['dateuploaded']);
				$Photo->setFileName(stripslashes($recordset['filename']));
				$Photo->setThumbFileName(stripslashes($recordset['thumbfilename']));

				$arrGroupPhotos[] = $Photo;
			}

			return $arrGroupPhotos;
 		}

		/* added by Jul, 2008-11-14
			returns an array containing an array of objects AdminGroup and the result count
		*/


		function getGroupMembers($rowslimit = NULL, $filtercriteria = NULL, $order = NULL, $unassigned=FALSE,$includePending=FALSE, $countOnly=FALSE){            
			             	           
            $limitstr  = '';
            $filterstr = '';
            $orderstr  = '';
            
            if (NULL != $rowslimit){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }  
            
            if (NULL != $filtercriteria){

            	$condition = $filtercriteria->getConditions();	
    			$attname   = $condition[0]->getAttributeName();                    			
    			$value 	   = $condition[0]->getValue();
    			
    			switch($condition[0]->getOperation()){
    				
    				case FilterOp::$EQUAL:
    					$operator	= " = ";
    					break;
    				case FilterOp::$NOT_EQUAL:
    					$operator = " <> ";
    					break;
					case FilterOp::$LIKE:
						$operator = " LIKE ";
						break;
					case FilterOp::$GREATER_THAN_OR_EQUAL:
						$operator = " >= ";
						break;
					case FilterOp::$LESS_THAN_OR_EQUAL:
						$operator = " <= ";
						break;
    				default:
    					$operator = " = ";
    					break;
    			}
    			
    			$filterstr = " AND " . $attname . $operator . $value;
            		
            }

           $this->mRs	= 	ConnectionProvider::instance()->getRecordset(); 
            
           //$filterstr .= " AND tblTraveler.travelerID not in (select distinct travelerID from tblGrouptoAdvisor) " ;
           $groupByStr = " GROUP BY tblTraveler.travelerID ";
           
        	if (NULL != $order){

            	$condition = $order->getConditions();	
    			
    			$attname   = $condition[0]->getAttributeName();                    			
    				        			
    			switch($condition[0]->getOperation()){
    				
    				case FilterOp::$ORDER_BY:
    					$sortorder = " ASC ";
    					break;
    				case FilterOp::$ORDER_BY_DESC:
    					$sortorder = " DESC ";
    					break;
    				default:
    					$sortorder = " ASC ";
    					break;
    			}
    			
    			$orderstr = " ORDER BY " . $attname . $sortorder;             		
            }
			
			$administratorID = $this->mAdministratorID;
			
			if( $unassigned ){
				
				$tempSQL = "CREATE TEMPORARY TABLE assigned(
				 				`travelerID` INT( 11 ) NOT NULL ,
								KEY ( travelerID )
							) SELECT tblTraveler.travelerID
								FROM tblTraveler, tblGroup, tblGrouptoTraveler
								WHERE tblGroup.parentID = $this->mGroupID
									AND tblGrouptoTraveler.groupID = tblGroup.groupID
									AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID
									AND tblTraveler.active  = 1
									AND tblTraveler.isSuspended = 0
									AND tblTraveler.deactivated = 0
								GROUP BY tblTraveler.travelerID ";
				$this->mRs->Execute($tempSQL);				
				//echo $tempSQL."<br />";				
				
				$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.*
						FROM tblTraveler, tblGrouptoTraveler
						WHERE tblGrouptoTraveler.groupID = $this->mGroupID
							AND tblTraveler.travelerID = tblGrouptoTraveler.travelerID
							AND tblTraveler.isSuspended = 0
							AND tblTraveler.deactivated = 0
							AND tblTraveler.active  = 1
							AND tblTraveler.travelerID NOT IN (SELECT travelerID FROM assigned)
						$filterstr $groupByStr $orderstr $limitstr";
			}else{
				if( $includePending ){
					$sql = "(SELECT SQL_CALC_FOUND_ROWS tblTraveler.*, 0 AS isPending
							FROM tblGrouptoTraveler, tblTraveler
							WHERE tblGrouptoTraveler.groupID =  $this->mGroupID
								AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID 
								AND tblTraveler.active  = 1 
								AND tblTraveler.isSuspended = 0 
								AND tblTraveler.deactivated = 0 
							$filterstr $groupByStr)
							UNION ALL
							(SELECT tblTraveler.*, 1 AS isPending
							FROM tblTraveler, tblInviteList
							WHERE tblInviteList.groupID = $this->mGroupID
								AND tblInviteList.status != 1
								AND tblTraveler.travelerID = tblInviteList.travelerID
								AND tblTraveler.active = 1
								AND tblTraveler.isSuspended = 0
								AND tblTraveler.deactivated = 0
							$filterstr $groupByStr)
							$orderstr $limitstr";
				}else{
					$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* " .
						"FROM tblGrouptoTraveler, tblTraveler " .
						"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
						"AND tblGrouptoTraveler.groupID = " . $this->mGroupID .
						" AND tblTraveler.active  = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 " .
						$filterstr . $groupByStr . $orderstr . $limitstr;
				}
			}
      		
			$this->mRs->Execute($sql);
            
			if( $countOnly ){
				return $this->mRs->Recordcount();
			}

			if ($this->mRs->Recordcount() != $this->mMemberCount){
	             		$this->mMembers = array();
				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
	                  try {
							$travelerData = array();
							$travelerData["travelerID"] = $recordset['travelerID'];
							$travelerData["active"] = $recordset['active'];
							$travelerData["isSuspended"] = $recordset['isSuspended'];
				 			$travelerData["currlocationID"] = $recordset['currlocationID'];
				 			$travelerData["sendableID"] = $recordset['sendableID'];
				 			$travelerData["username"] = $recordset['username'];
				 			$travelerData["password"] = $recordset['password'];
				 			$travelerData["showsteps"] = $recordset['showsteps'];
							$travelerData["deactivated"] = $recordset['deactivated'];
												
							$traveler = new Traveler;
							$traveler-> setTravelerData($travelerData);

							// set profile data in array and set to profile; minimize DB query
							$profileData = array();								
							$profileData["travelerID"] = $recordset['travelerID'];
							$profileData["username"] = $recordset['username'];
							$profileData["firstname"] = $recordset['firstname'];
				 			$profileData["lastname"] = $recordset['lastname'];
				 			$profileData["email"] = $recordset['email'];
				 			$profileData["gender"] = $recordset['gender'];
				 			$profileData["htlocationID"] = $recordset['htlocationID'];
				 			$profileData["currlocationID"] = $recordset['currlocationID'];
				 			$profileData["address1"] = $recordset['address1'];
				 			$profileData["address2"] = $recordset['address2'];
				 			$profileData["phone"] = $recordset['phone'];
				 			$profileData["travelertypeID"] = $recordset['travelertypeID'];
				 			$profileData["birthdate"] = $recordset['birthdate'];
				 			$profileData["interests"] = $recordset['interests'];
				 			$profileData["shortbio"] = $recordset['shortbio'];
				 			$profileData["iculture"] = $recordset['iculture'];
				 			$profileData["dateregistered"] = $recordset['dateregistered'];
				 			$profileData["latestlogin"] = $recordset['latestlogin'];
				 			$profileData["views"] =  $recordset['views'];
				 			$profileData["intrnltravel"] = $recordset['intrnltravel'];
				 			$profileData["istripnexttwoyears"] = $recordset['istripnexttwoyears'];
				 			$profileData["income"] =	 $recordset['income'];
				 			$profileData["otherprefertravel"] =	 $recordset['otherprefertravel'];
				 			$profileData["ispromosend"] = $recordset['ispromosend'];
				 			$profileData["signature"] = $recordset['signature'];
	
							$profile = new TravelerProfile;
							$profile->setProfileData($profileData);

							$traveler->setTravelerProfile($profile);								
	                   	}
	                   	catch (Exception $e) {
	                    	throw $e;
	                   	}

						$this->mMembers[] = $traveler;
	               }
				}
			
			$sql2 = "SELECT FOUND_ROWS() AS `total_records`";
			$this->mRs->Execute($sql2);
			$recordset = mysql_fetch_array($this->mRs->Resultset());
			return array($this->mMembers,$recordset["total_records"]);               
        }

		function getCustomMessage(){
			return $this->customMessage;
		}
		
		function setCustomMessage($msg){
			$this->customMessage = $msg;
		}
		
		/**
		 * Method to get the join requests of a group
		 * Added by		:	Jul
		 * Date Added	:	2008-11-19
		 */
		function getJoinRequests($rowslimit=NULL){
			$this->mRs	=	ConnectionProvider::instance()->getRecordset();
			$limitstr = "";
			if (NULL != $rowslimit){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }
			
			$group_criteria = " = ".$this->mGroupID;
             
            $sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* 
					FROM tblRequestList, tblTraveler 
					WHERE tblTraveler.travelerID = tblRequestList.travelerID 
					AND tblTraveler.active = 1 
					AND tblTraveler.isSuspended = 0 
					AND tblTraveler.deactivated = 0 
					AND tblRequestList.status = 0 
					AND tblRequestList.groupID $group_criteria 
					GROUP BY tblTraveler.travelerID
					$limitstr";
			
			$this->mRs->Execute($sql);
			$request_list = array();
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
				$mTraveler = new Traveler;
				$mTraveler->setTravelerData($recordset);
                $request_list[] = $mTraveler;
			}               
            
			$sql2 = "SELECT FOUND_ROWS() AS `request_count`";
			$this->mRs->Execute($sql2);
			$recordset = mysql_fetch_array($this->mRs->Resultset());
			
            return array($request_list,$recordset["request_count"]);
		}
		
		function isStaffInvitation($travID = 0) {
			$id = array();
			$traveler = new Traveler($travID);
			$arrGroup = ($traveler->getPendingStaffInvitationsToGroup());
			foreach($arrGroup as $group) {
				$id[] = $group->getGroupID();
			}
			if(in_array($this->mGroupID,$id))
				return true;
			else
				return false;
		}
		
		//added by Jul to get travelers who were invited to join the group
		function getInvitedTravelers(){

			$sql = "SELECT tblTraveler.*
					FROM tblTraveler, tblInviteList
					WHERE tblInviteList.groupID = $this->mGroupID
						AND tblInviteList.status!=1
						AND tblTraveler.travelerID=tblInviteList.travelerID
						AND tblTraveler.active = 1
						AND tblTraveler.isSuspended = 0
						AND tblTraveler.deactivated = 0
					GROUP BY tblTraveler.travelerID";
			//echo "<br /><br />".$sql."<br /><br />";
			$this->mRs->Execute($sql);

			$registeredInvites = array();
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
            	try {
					$travelerData = array();
					$travelerData["travelerID"] = $recordset['travelerID'];
					$travelerData["active"] = $recordset['active'];
					$travelerData["isSuspended"] = $recordset['isSuspended'];
		 			$travelerData["currlocationID"] = $recordset['currlocationID'];
		 			$travelerData["sendableID"] = $recordset['sendableID'];
		 			$travelerData["username"] = $recordset['username'];
		 			$travelerData["password"] = $recordset['password'];
		 			$travelerData["showsteps"] = $recordset['showsteps'];
					$travelerData["deactivated"] = $recordset['deactivated'];

					$traveler = new Traveler;
					$traveler-> setTravelerData($travelerData);

					// set profile data in array and set to profile; minimize DB query
					$profileData = array();								
					$profileData["travelerID"] = $recordset['travelerID'];
					$profileData["username"] = $recordset['username'];
					$profileData["firstname"] = $recordset['firstname'];
		 			$profileData["lastname"] = $recordset['lastname'];
		 			$profileData["email"] = $recordset['email'];
		 			$profileData["gender"] = $recordset['gender'];
		 			$profileData["htlocationID"] = $recordset['htlocationID'];
		 			$profileData["currlocationID"] = $recordset['currlocationID'];
		 			$profileData["address1"] = $recordset['address1'];
		 			$profileData["address2"] = $recordset['address2'];
		 			$profileData["phone"] = $recordset['phone'];
		 			$profileData["travelertypeID"] = $recordset['travelertypeID'];
		 			$profileData["birthdate"] = $recordset['birthdate'];
		 			$profileData["interests"] = $recordset['interests'];
		 			$profileData["shortbio"] = $recordset['shortbio'];
		 			$profileData["iculture"] = $recordset['iculture'];
		 			$profileData["dateregistered"] = $recordset['dateregistered'];
		 			$profileData["latestlogin"] = $recordset['latestlogin'];
		 			$profileData["views"] =  $recordset['views'];
		 			$profileData["intrnltravel"] = $recordset['intrnltravel'];
		 			$profileData["istripnexttwoyears"] = $recordset['istripnexttwoyears'];
		 			$profileData["income"] =	 $recordset['income'];
		 			$profileData["otherprefertravel"] =	 $recordset['otherprefertravel'];
		 			$profileData["ispromosend"] = $recordset['ispromosend'];
		 			$profileData["signature"] = $recordset['signature'];

					$profile = new TravelerProfile;
					$profile->setProfileData($profileData);

					$traveler->setTravelerProfile($profile);	
					
					$registeredInvites[] = $traveler;							
                }catch (Exception $e) {
                	throw $e;
               	}
        	}
			
			return $registeredInvites;
		}
		
		//to check if a journal is a group approved and/or featured
		public function getGroupJournalStatus($travelID=0){
			$groupID = $this->getGroupID();
			$sql = "SELECT 	tblGroupApprovedJournals.approved AS approved
					FROM tblGroupApprovedJournals
					WHERE tblGroupApprovedJournals.travelID = $travelID
						AND tblGroupApprovedJournals.groupID = $groupID";
			//echo "<br /><br />".$sql."<br /><br />";
			$this->mRs->Execute($sql);
			
			$status = array("related"=>FALSE, "approved"=>FALSE, "featured"=>FALSE);
			if( 0 < $this->mRs->Recordcount() ){
				$data = mysql_fetch_assoc($this->mRs->Resultset());
				$status["related"] = TRUE;
				if( 1 == $data["approved"] ){
					$status["approved"] = TRUE;
				}else{
					$status["approved"] = FALSE;
				}
			}
			
			$sql = "SELECT 1
					FROM tblGroupFeaturedJournals
					WHERE tblGroupFeaturedJournals.travelID = $travelID
						AND tblGroupFeaturedJournals.groupID = $groupID";
			//echo "<br /><br />".$sql."<br /><br />";
			
			$this->mRs->Execute($sql);
			if( 0 < $this->mRs->Recordcount() ){
				$status["featured"] = TRUE;
			}
			return $status;
		}
		
		// empties group categories
		public function resetGroupCategory(){
			$this->mGroupCategory = array();
		}
		
		//added by Jul to invalidate cache in member-staff page
		protected function invalidateCacheKeys($groupID=0){
			$cache = ganetCacheProvider::instance()->getCache();
			
			if ($cache != null){
          		// get hash keys
				$hashKeys	=	$cache->get('Group_to_Traveler_hashes_' . $groupID);
				
				if ($hashKeys){
					//remove this hashKeys from cache
					foreach($hashKeys as $hashKey){
						$cache->delete('Group_to_Traveler_hashes_' . $groupID . '_' . $hashKey);
						$cache->delete('Group_to_Traveler_Count_hashes_' . $groupID . '_' . $hashKey);
						$cache->delete('Group_to_Traveler_' . $groupID . '_' . $hashKey);
						$cache->delete('Group_to_Traveler_Count_' . $groupID . '_' . $hashKey);			
					}
            		unset($hashKey);
					$membersListHashkey = $cache->get('Group_Members_List_Hashkey_'.$groupID);
					$cache->delete($membersListHashkey);
				}
				$cache->delete('Group_to_Traveler_hashes_' . $groupID);
				
				$hashKeys = $cache->get('Group_SearchMembers_hashes_' . $groupID);
				if( $hashKeys ){
					foreach($hashKeys as $hashKey){
						$cache->delete($hashKey);
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey);
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_members');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_members_adminview');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_search');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_search_adminview');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_searchsubgroups');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_searchsubgroups_adminview');		
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_groups');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_groups_adminview');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_projects');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_projects_adminview');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_staff');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_staff_adminview');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_subgroupsprojects');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_subgroupsprojects_adminview');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_newmembers');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_newmembers_adminview');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_recentactivity');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_recentactivity_adminview');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_unassignedmembers');
						$cache->delete('Group_SearchMembers_hashes_' . $groupID . '_' . $hashKey . '_unassignedmembers_adminview');
					}
					unset($hashKey);
				}
				$cache->delete('Group_SearchMembers_hashes_' . $groupID);
            }
		}
    
		public function getEmailInviteID($email){
			$sql = "SELECT * from  tblEmailInviteList WHERE email = '$email'  AND groupID = '$this->mGroupID'";
			$this->mRs->Execute($sql);

            if ($this->mRs->Recordcount()){
				return $this->mRs->Result(0,'ID');
			}
			
		}
}    
?>