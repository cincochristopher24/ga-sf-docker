<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	
	class PendingNotificationRecipient
	{
		const ADDRESS_BOOK_ENTRIES 					= 1;
		const GROUP_MEMBERS							= 2;
		const GROUP_MEMBERS_ADDRESS_BOOK_ENTRIES 	= 3;
		
		private $mPendingNotificationRecipientID 	= null;
		private $mPendingNotificationID 			= null;
		private $mRecipient 						= null;
		private $mDb								= null;
		
		/**
		 * Constructor function
		 */
		public function __construct($param=null,$data=null){
			$this->mDb = new dbHandler();
			if(!is_null($param)){
				$sql =	'SELECT `pendingNotificationRecipientID`, `pendingNotificationID`, `recipient` ' .
						'FROM `tblPendingNotificationRecipient` ' .
						'WHERE `pendingNotificationRecipientID` = '.ToolMan::makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount() > 0){
					$this->setPendingNotificationRecipientID($rs->getPendingNotificationRecipientID(0));
					$this->setPendingNotificationID($rs->getPendingNotificationID(0));
					$this->setRecipient($rs->getRecipient(0));
				}
				else{
					throw new exception('Invalid pendingNotificationRecipientID '.$param.' passed to constructor method of object of type PendingNotificationRecipientID.');
				}
			}
			if(is_array($data)){
				$this->setPendingNotificationRecipientID($data['pendingNotificationRecipientID']);
				$this->setPendingNotificationID($data['pendingNotificationID']);
				$this->setRecipient($data['recipient']);
			}
		}
		
		/*** SETTERS ***/
		
		/**
		 * Sets the keyID (pendingNotificationRecepientID) 
		 * @param int $param : The value which the pendingNotiicationRecipientID will be set to.
		 * @return void
		 */
		private function setPendingNotificationRecipientID($param=null){
			$this->mPendingNotificationRecipientID = $param;
		}
		
		/**
		 * Sets the foreignKey(pendingNotificationID)
		 * @param int $param : The value which the pendingNotificationID will be set to.
		 * @return void
		 */
		public function setPendingNotificationID($param=null){
			$this->mPendingNotificationID = $param;
		}
		
		/**
		 * Sets the recipient type (Check the constants)
		 */
		public function setRecipient($param){
			$this->mRecipient = $param;
		}
	
		/*** GETTERS ***/
		
		public function getPendingNotificationRecipientID(){
			return $this->mPendingNotificationRecipientID;	
		}
	
		public function getPendingNotificationID(){
			return $this->mPendingNotificationID;
		}
		
		public function getRecipient(){
			return $this->mRecipient;
		}
		
		private function isNewEntry(){
			return is_null($this->getPendingNotificationRecipientID());
		}
		
		/*** CRUDE ***/
		public function save(){
			if(!is_numeric($this->getPendingNotificationID())){
				throw new exception('Cannot perform save operation. Invalid PendingNotificationID '.$this->getPendingNotificationID().' passed to object of type PendingNotificationRecipient.');
			}
			if(!self::isValidRecipient($this->getRecipient())){
				throw new exception('Cannot perform save operation. Invalid recipient '.$this->getRecipient().' passed to object of type PendingNotificationRecipient.');
			}
			if($this->isNewEntry()){//add
				$sql =	'INSERT INTO `tblPendingNotificationRecipient`(' .
							'`pendingNotificationID`,' .
							'`recipient`' .
						')VALUES(' .
							ToolMan::makeSqlSafeString($this->getPendingNotificationID()).','.
							ToolMan::makeSqlSafeString($this->getRecipient()).
						')';
				$this->mDb->execQuery($sql);
				$this->setPendingNotificationRecipientID($this->mDb->getLastInsertedID());
			}
			else{//update
				$sql = 	'UPDATE `tblPendingNotificationRecipient` SET' .
							'`pendingNotificationID` = '.ToolMan::makeSqlSafeString($this->getPendingNotificationID()) .','.
							'`recipient` = ' .ToolMan::makeSqlSafeString($this->getRecipient()).' ' .
						'WHERE pendingNotificationRecipientID = '.ToolMan::makeSqlSafeString($this->getPendingNotificationRecipientID());
				$this->mDb->execQuery($sql);
			}
		}
		
		public function delete(){
			$sql =	'DELETE FROM `tblPendingNotificationRecipient` ' .
					'WHERE `pendingNotificationRecipient` = '.ToolMan::makeSqlSafeString($this->getPendingNotificationRecipientID());
			$this->mDb->execQuery($sql);
			
		}
		
		/*** Static functions  ***/
		
		public static function isValidRecipient($rec){
			return ($rec === self::ADDRESS_BOOK_ENTRIES || $rec === self::GROUP_MEMBERS || $rec === self::GROUP_MEMBERS_ADDRESS_BOOK_ENTRIES);
		}
		
		public static function isRecipientExists($recipient,$pendingNotificationID){
			$arRcpts = self::getRecipientsByPendingNotificationID($pendingNotificationID);
			foreach($arRcpts as $iRcpt){
				if($iRcpt->getRecipient() === $recipient){
					return true;
				}
			}	
			return false;
		}
		
		public static function getRecipientsByPendingNotificationID($pendingNotificationID){
			$db = new dbHandler();
			$sql =	'SELECT `pendingNotificationRecipientID`, `pendingNotificationID`, `recipient` ' .
					'FROM `tblPendingNotificationRecipient` ' .
					'WHERE `pendingNotificationID` = '.ToolMan::makeSqlSafeString($pendingNotificationID);
			$rs = new iRecordset($db->execQuery($sql));
			$ar = array();
			if($rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new PendingNotificationRecipient(null,$rs->retrieveRow());
				}
			}
			return $ar;
		}
		
		public static function deleteRecipientsByPendingNotificationID($pendingNotificationID){
			$db = new dbHandler();
			$sql =	'DELETE FROM `tblPendingNotificationRecipient` ' .
					'WHERE pendingNotificationID = '.ToolMan::makeSqlSafeString($pendingNotificationID);
			$db->execQuery($sql);
		}
		
	}
?>