<?php
/*
 * Created on Jul 20, 2006
 * Created by: Czarisse Daphne P. Dolina
 */
 
 	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
 	require_once("Class.LocationFactory.php");
	require_once("Class.Photo.php");
	require_once("Class.TravelerType.php");
	require_once("Class.PurposeType.php");
	require_once("Class.ProgramType.php");
	require_once("Class.PreferredTravelType.php");
	require_once("Class.Country.php");
	require_once("travellog/helper/Class.SqlUtil.php");
	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");
	require_once("Class.Comment.php");
	require_once 'travellog/model/Class.CommentNotification.php';
	require_once 'travellog/model2/dao/Class.ganetTravelerProfileDao.php';
	require_once("travellog/model/Class.GanetDbHandler.php");
	require_once("Class.ResourceIterator.php");
	require_once("Class.ConnectionProvider.php");
	require_once("travellog/model/Class.CommentContext.php");
	
 	class TravelerProfile implements CommentContext {
 		
 		// vars for db connection
 		private $conn		= NULL;
 		private $rs			= NULL;
		private $rs2		= NULL;
		protected $dao 		= NULL;
 		
 		//vars for parent classes
 		private $traveler    = NULL;
		private $isadvisor   = 0; 		

 		// vars for class attributes
 		private $travelerID = NULL;
		private $username    = NULL;	
 		private $firstname   = NULL;
 		private $lastname    = NULL;
 		private $email       = NULL;
 		private $gender      = NULL;
 		private $genderstr   = NULL;
 		private $age         = NULL;
		
 		// vars for class attributes
 		private $htlocationID  = NULL;
 		private $currlocationID  = NULL;
 		private $mCurrentLocation = NULL;
 		private $address1    = NULL;
 		private $address2    = NULL;
 		private $phone			= NULL;
 		private $travelertypeID = 0;
 		
 		// vars for class attributes
 		private $birthdate   = NULL;
 		private $interests   = NULL;
 		private $shortbio    = NULL;
 		private $iculture		= NULL;
 		private $datejoined		= NULL;
 		private $lastlogin		= NULL;
 		private $views    	= NULL;
 		
 		// vars for class attributes
 		private $stateprov   	= NULL;
 		private $postalcode  	= NULL;
 		private $intrnltravel 	= 0;
 		private $istripnexttwoyears = 0;
 		private $income = 0;
 		private $otherprefertravel = NULL;
 		
 		private $ispromosend = 0;
 		private $signature	= NULL;
 		
 		private $issuspended = 0;
		
		private $showpersonals = 1;
 		
 		// vars for other methods
 		private $profileurl  = NULL;
 		private $objcity     = NULL;
 		private $randphoto    = NULL;
 		private $primphoto   = NULL;
 		private $arrphotos   = array();
 		private $arrpurpose = NULL;
 		private $arrprograminterested = NULL;
 		private $arrpreferredtravel = NULL;
 		private $arrcountryinterested = NULL;
 		private $arrcountrytravelled = NULL;
		private $programInterestedChanged = false;
		private $preferredTravelChanged = false;	
	 	private $countryInterestedChanged = false;
		private $countryTravelChanged = false;
	 		 		 		
 		public  $phototype	= 1;
		private $preShowPersonals = 1; // value of show personals before setting showpersonals value
 		
 		// constructor
 		
 			function TravelerProfile($_travelerID = 0){
 				
 				$this->travelerID = $_travelerID;
 				
				$this->dao	=	new ganetTravelerProfileDao();
 				try {
	 				$this->conn = new Connection();
					$this->rs   = new Recordset($this->conn);
					$this->rs2	= new Recordset($this->conn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}

	 			if ($this->travelerID > 0){
	 				//echo '2';
	 				$sql = "SELECT * from tblTraveler where travelerID = '$this->travelerID' ";
		 			$this->rs->Execute($sql);
	 			
	 				if ($this->rs->Recordcount() == 0){
	 					throw new Exception("Invalid Traveler ID");	// ID not valid so throw exception
	 				}

					$this->isadvisor = 	$this->rs->Result(0,"isadvisor");
	
	 				// set values to its attributes
					$this->username 		= stripslashes($this->rs->Result(0,"username")); 
					$this->firstname 	= stripslashes($this->rs->Result(0,"firstname")); 
		 			$this->lastname 		= stripslashes($this->rs->Result(0,"lastname")); 
		 			$this->email 		= $this->rs->Result(0,"email"); 
		 			$this->gender 		= $this->rs->Result(0,"gender");
		 			
		 			$this->htlocationID 	= $this->rs->Result(0,"htlocationID");
		 			$this->currlocationID 	= $this->rs->Result(0,"currlocationID"); 
		 			$this->address1 		= stripslashes($this->rs->Result(0,"address1")); 
		 			$this->address2 		= stripslashes($this->rs->Result(0,"address2"));
		 			$this->phone 			= stripslashes($this->rs->Result(0,"phone")); 
		 			
		 			$this->travelertypeID = $this->rs->Result(0,"travelertypeID"); 
		 			
		 			$this->birthdate 	= $this->rs->Result(0,"birthdate"); 
		 			$this->interests 	= stripslashes($this->rs->Result(0,"interests")); 
		 			$this->shortbio 		= stripslashes($this->rs->Result(0,"shortbio")); 
		 			$this->iculture 		= stripslashes($this->rs->Result(0,"iculture")); 
		 			$this->datejoined 	= $this->rs->Result(0,"dateregistered");
		 			$this->lastlogin 	= $this->rs->Result(0,"latestlogin");
		 			$this->views 		= $this->rs->Result(0,"views");  
		 			$this->intrnltravel  =  $this->rs->Result(0,"intrnltravel");
		 			$this->istripnexttwoyears = $this->rs->Result(0,"istripnexttwoyears");
		 			$this->income  = $this->rs->Result(0,"income");	
		 			$this->otherprefertravel  = $this->rs->Result(0,"otherprefertravel");	
		 			$this->ispromosend	= $this->rs->Result(0,"ispromosend");
		 			$this->signature	= $this->rs->Result(0,"signature");
		 			$this->issuspended	= $this->rs->Result(0,"Issuspended");
					
					$this->showpersonals = $this->rs->Result(0,"showpersonals");
		 			$this->preShowPersonals = $this->showpersonals;
				}
					
 			}
 			
			// set profile data in array and assign values; minimize db quer
			function setProfileData($_data = array() ) {
				
				if (count($_data)){
					
					$this->travelerID	= isset($_data["travelerID"]) ? $_data["travelerID"] : 0;
					$this->isadvisor	= isset($_data["isadvisor"]) ? $_data["isadvisor"] : 0; 
					$this->username 	= isset($_data["username"]) ? $_data["username"] : ''; 
					$this->firstname 	= isset($_data["firstname"]) ? $_data["firstname"] : ''; 
		 			$this->lastname 	= isset($_data["lastname"]) ? $_data["lastname"] : ''; 
		 			$this->email 		= isset($_data["email"]) ? $_data["email"] : ''; 
		 			$this->gender 		= isset($_data["gender"]) ? $_data["gender"] : '';
		 			
		 			$this->htlocationID 	= isset($_data["htlocationID"]) ? $_data["htlocationID"] : 0;
		 			$this->currlocationID 	= isset($_data["currlocationID"]) ? $_data["currlocationID"] : 0; 
		 			$this->address1 		= isset($_data["address1"]) ? $_data["address1"] : ''; 
		 			$this->address2 		= isset($_data["address2"]) ? $_data["address2"] : '';
		 			$this->phone 			= isset($_data["phone"]) ? $_data["phone"] : ''; 
		 			
		 			$this->travelertypeID = isset($_data["travelertypeID"]) ? $_data["travelertypeID"] : 0; 
		 			
		 			$this->birthdate 	= isset($_data["birthdate"]) ? $_data["birthdate"] : ''; 
		 			$this->interests 	= isset($_data["interests"]) ? $_data["interests"] : ''; 
		 			$this->shortbio 	= isset($_data["shortbio"]) ? $_data["shortbio"] : ''; 
		 			$this->iculture 	= isset($_data["iculture"]) ? $_data["iculture"] : ''; 
		 			$this->datejoined 	= isset($_data["dateregistered"]) ? $_data["dateregistered"] : null;
		 			$this->lastlogin 	= isset($_data["latestlogin"]) ? $_data["latestlogin"] : null;
		 			$this->views 		= isset($_data["views"]) ? $_data["views"] : 0;  
		 			$this->intrnltravel  =  isset($_data["intrnltravel"]) ? $_data["intrnltravel"] : 0;
		 			$this->istripnexttwoyears = isset($_data["istripnexttwoyears"]) ? $_data["istripnexttwoyears"] : 0;
		 			$this->income  		= isset($_data["income"]) ? $_data["income"] : 0;	
		 			$this->otherprefertravel  = isset($_data["otherprefertravel"]) ? $_data["otherprefertravel"] : null;	
		 			$this->ispromosend	= isset($_data["ispromosend"]) ? $_data["ispromosend"] : 0;
		 			$this->signature	= isset($_data["signature"]) ? $_data["signature"] : null;
		 			$this->issuspended	= isset($_data["isSuspended"]) ? $_data["isSuspended"] : null;
					
					$this->showpersonals = isset($_data["showpersonals"]) ? $_data["showpersonals"] : 1;
		 			$this->preShowPersonals = $this->showpersonals;
					
				}
			}
 			
	public function getName() {
		return $this->getFirstName() . ' ' . $this->getLastName();
	}

 		//setters
 		
	 		function setTravelerID ($_travelerID){
	 			$this->travelerID = $_travelerID;	
	 		}
	
			function setIsAdvisor($advisor=0){//values: 0,1
				$this->isadvisor = $advisor;
			}

			function setUserName($_username){
	 			$this->username = $_username;
	 		}
				 		
	 		function setFirstName($_firstname){
	 			$this->firstname = $_firstname;
	 		}
	 		
	 		function setLastName($_lastname){
	 			$this->lastname = $_lastname;
	 		}
	 		
	 		function setEmail ($_email){
	 			$this->email = $_email; 			
	 		}
	 		
	 		function setGender ($_gender){
	 			$this->gender = $_gender; 			
	 		}
	 		
	 		function setBirthDay($_birthday){
	 			$this->birthdate = $_birthday;
	 		}
	 		
	 		function setInterests($_interests){
	 			$this->interests = $_interests;
	 		}
	 		
	 		function setShortBio($_shortBio){
	 			$this->shortbio = $_shortBio;
	 		}
	 		
	 		function setCulture($iculture){
	 			$this->iculture = $iculture;
	 		}	 	
	 		
	 		function setDateJoined($_datejoined){
	 			$this->datejoined = $_datejoined;
	 		}
	 		
	 		function setLastLogin($_lastlogin){
	 			$this->lastlogin = $_lastlogin;
	 		}
	 		
	 		function setHTLocationID($_htlocationID){
	 			$this->htlocationID = $_htlocationID;
	 		}
	 		
	 		function setCurrLocationID($_currlocationID){
	 			$this->currlocationID = $_currlocationID;
	 		}
 		
 			function setAddress1($_address1){
	 			$this->address1 = $_address1;
	 		}
	 		
	 		function setAddress2($_address2){
	 			$this->address2= $_address2;
	 		}
	 		
	 		
	 		function setPhone($_phone){
	 			$this->phone= $_phone;
	 		}
	 		
	 		function setTravelertypeID($travelertypeID){
	 			$this->travelertypeID= $travelertypeID;
	 		}
	 		
	 		function setIntrnltravel($intrnltravel){
	 			$this->intrnltravel = $intrnltravel;
	 		}
	 		
	 		function setPurpose($arrpurpose){
	 			$this->arrpurpose = $arrpurpose;	
	 		}
	 		
	 		function setProgramInterested($arrprograminterested){
				$this->arrprograminterested = (!is_array($this->arrprograminterested)) ? array() : $this->arrprograminterested;
	 			if (count(array_diff($this->arrprograminterested,$arrprograminterested)) > 0)
					$this->programInterestedChanged = true;
				$this->arrprograminterested = $arrprograminterested;	
	 		}
	 		
	 		function setPreferredTravel($arrpreferredtravel){
				$this->arrpreferredtravel = (!is_array($this->arrpreferredtravel)) ? array() : $this->arrpreferredtravel;
	 			if (count(array_diff($this->arrpreferredtravel,$arrpreferredtravel)) > 0)
					$this->preferredTravelChanged = true;
				$this->arrpreferredtravel = $arrpreferredtravel;	
	 		}
	 		
	 		function setCountryinterested($arrcountryinterested){
				$this->arrcountryinterested = (!is_array($this->arrcountryinterested)) ? array() : $this->arrcountryinterested;
	 			if (count(array_diff($this->arrcountryinterested,$arrcountryinterested)) > 0)
					$this->countryInterestedChanged = true;
				$this->arrcountryinterested = $arrcountryinterested;	
	 		}
	 		
	 		function setCountrytravelled($arrcountrytravelled){
				$this->arrcountrytravelled = (!is_array($this->arrcountrytravelled)) ? array() : $this->arrcountrytravelled;
	 			if (count(array_diff($this->arrcountrytravelled,$arrcountrytravelled)) > 0)
					$this->countryTravelChanged = true;
				$this->arrcountrytravelled = $arrcountrytravelled;	
	 		}
	 			 		
	 		function setTripnexttwoyears($istripnexttwoyears){
	 			$this->istripnexttwoyears = $istripnexttwoyears;	
	 		}
	 		
	 		function setIncome($income){
	 			$this->income = $income;	
	 		}
	 		
	 		function setOtherprefertravel($otherprefertravel){
	 			$this->otherprefertravel = $otherprefertravel;	
	 		}
	 		
	 		
	 		function setIspromosend($ispromosend){
	 			$this->ispromosend = $ispromosend;	
	 		}
	 		
	 		function setSignature($signature){
	 			$this->signature = $signature;	
	 		}
	 		
	 		function setIssuspended($issuspended){
	 			$this->issuspended = $issuspended;	
	 		}
	
			function setShowPersonals($showpersonals){
				$this->showpersonals = $showpersonals;
			}
			
			function setPrimaryPhoto($primphoto){
				$this->primphoto	=	$primphoto;
			}
	 		
 		//getters
 		 		
	 		function getTravelerID(){
	 			return $this->travelerID;
	 		}
	 		
			function getIsAdvisor(){
				return $this->isadvisor;
			}
	
			function getUserName(){
				return $this->username;
			}
			
	 		function getFirstName(){
	 			return $this->firstname;
	 		}
	 		
	 		function getLastName(){
	 			return $this->lastname;
	 		}
	 		
	 		function getEmail(){
	 			return $this->email; 			
	 		}
	 		
	 		function getBirthDay(){
	 			return $this->birthdate;
	 		}
	 		
	 		function getInterests(){
	 			return $this->interests;
	 		}
	 		
	 		function getShortBio(){
	 			return $this->shortbio;
	 		}
	 		
	 		function getCulture(){
	 			return $this->iculture;
	 		}	 		
	 		
	 		function getDateJoined(){
	 			return $this->datejoined;
	 		}
	 		
	 		function getLastLogin(){
	 			return $this->lastlogin;
	 		}
	 		
	 		function getHTLocationID(){
	 			return $this->htlocationID;
	 		}
	 		
	 		function getCurrLocationID(){
	 			return $this->currlocationID;
	 		}
 		
 			function getAddress1(){
	 			return $this->address1;
	 		}
	 		
	 		function getAddress2(){
	 			return $this->address2;
	 		}
	 		
	 		function getPhone(){
	 			return $this->phone;
	 		}
	 		
	 		function getTravelerTypeID(){
	 			return $this->travelertypeID;
	 		}
 		
 			function getViews(){
	 			return $this->views;
	 		}
	 		
	 		function getIntrnltravel(){
	 			return $this->intrnltravel;
	 		}
	 		
	 		function getTripnexttwoyears(){
	 			return $this->istripnexttwoyears;	
	 		}
	 		
	 		function getIncome(){
	 			return $this->income;	
	 		}
	 		
	 		function getOtherprefertravel(){
	 			return $this->otherprefertravel;	
	 		}
	 		
	 		function getIspromosend(){
	 			return $this->ispromosend;	
	 		}
	 		
	 		function getSignature(){
	 			return $this->signature;	
	 		}
	 		
	 		function getIssuspended(){
	 			return $this->issuspended;	
	 		}
	
			function getShowPersonals(){
				return $this->showpersonals;
			}
			
			protected function getProgramInterestedChanged(){
				return $this->programInterestedChanged;
			}
			
			protected function getPreferredTravelChanged(){
				return $this->preferredTravelChanged;
			}	
			
			protected function getCountryInterestedChanged() {
				return $this->countryInterestedChanged;
			}
		 	
			protected function getCountryTravelChanged(){
				return $this->countryTravelChanged;
			}
			
	 		
	 	// getter of parent class
	 	
	 		function getTraveler(){
				if ($this->traveler == NULL){
		 			try {
		 				$this->traveler = new Traveler($this->travelerID);
		 			}
					catch (Exception $e) {
					   throw $e;
					}	 
		 		}			
	 			return $this->traveler;
			}
				
 		// CRUD methods (w/o DELETE, actually)
 		
 			function Create(){ 	
 				
 				/*$fname = addslashes($this->firstname);
 				$lname = addslashes($this->lastname);
 				$int   = addslashes($this->interests);
 				$bio   = addslashes($this->shortbio);
 				$cul   = addslashes($this->iculture);
 				$add1  = addslashes($this->address1);
 				$add2  = addslashes($this->address2);
 				$phone = addslashes($this->phone);
 				$travelertypeID = $this->travelertypeID;
 				
				$dateregistered = addslashes("now()"); // added by ianne - 11/25/2008

	 			$sql = "INSERT into tblTraveler (firstname, lastname, email, htlocationID, travelertypeID, currlocationID, interests, shortbio, iculture, birthdate, address1, address2, phone, dateregistered) VALUES ('$fname', '$lname', '$this->email', '$this->htlocationID', '$travelertypeID',  '$this->currlocationID', '$int', '$bio', '$cul', '$this->birthdate', '$add1', '$add2', '$phone', '$dateregistered')";
	 			$this->rs->Execute($sql);
	 			$this->travelerID = $this->rs->GetCurrentID();	 		*/
	
				
				$this->dao->Create($this);
	 		}
	 		
	 		function Update(){
	 			
				// added by ianne - 11/25/2008
				//$uname = addslashes($this->username);
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			
	 			$fname = addslashes($this->firstname);
 				$lname = addslashes($this->lastname);
				$int   = addslashes($this->interests);
 				$bio   = addslashes($this->shortbio);
 				$cul   = addslashes($this->iculture);
 				$add1  = addslashes($this->address1);
 				$add2  = addslashes($this->address2);
 				$phone = addslashes($this->phone);				
				$travelertypeID = $this->travelertypeID;
				$intrnltravel = $this->intrnltravel;
				$istripnexttwoyears = $this->istripnexttwoyears;
				$income = $this->income;
				$otherprefertravel = $this->otherprefertravel;
				
				$showpersonals = $this->showpersonals;
				
				$signature	= addslashes($this->signature);
				
	 			$sql = "UPDATE tblTraveler SET firstname = '$fname', lastname = '$lname', htlocationID = '$this->htlocationID', travelertypeID= '$travelertypeID', " .
	 					"currlocationID = '$this->currlocationID', interests = '$int' , shortbio = '$bio', iculture = '$cul', birthdate = '$this->birthdate', " .
	 					"gender= '$this->gender', address1 = '$add1', address2 = '$add2', phone= '$phone',intrnltravel='$intrnltravel',istripnexttwoyears='$istripnexttwoyears'," .
	 					"income='$income',otherprefertravel = '$otherprefertravel', signature = '$signature', showpersonals = '$showpersonals'  WHERE travelerID = '$this->travelerID' " ;
	 			$this->rs->Execute($sql);
	 			
				if($savePersonals = $this->preShowPersonals){
					if($intrnltravel){
						if($this->getTravelerPurpose()){
							$sql = "DELETE  from tblTravelertoPurpose WHERE travelerID = '$this->travelerID'";
							$this->rs->Execute($sql);
						}

						for($x=0;$x<count($this->arrpurpose);$x++){
							$tpurposetypeID = $this->arrpurpose[$x];
							$sql = "INSERT INTO tblTravelertoPurpose(travelerID,tpurposetypeID) VALUES('$this->travelerID','$tpurposetypeID')";
							$this->rs->Execute($sql);		
						}
					}else{
						$sql = "DELETE  from tblTravelertoPurpose WHERE travelerID = '$this->travelerID'";
						$this->rs->Execute($sql);
					}		

					//program Iterested
					$sql = "DELETE  from tblTravelertoProgramsInterested WHERE travelerID = '$this->travelerID'";
					$this->rs->Execute($sql);

					if(count($this->arrprograminterested)){
						for($x=0;$x<count($this->arrprograminterested);$x++){
							$programtypeID = $this->arrprograminterested[$x];
							$sql = "INSERT INTO tblTravelertoProgramsInterested(travelerID,programtypeID) VALUES('$this->travelerID','$programtypeID')";
							$this->rs->Execute($sql);		
						}
					}

					//preferred Travel
					$sql = "DELETE  from tblTravelertoPreferredTravelType WHERE travelerID = '$this->travelerID'";
					$this->rs->Execute($sql);


					if(count($this->arrpreferredtravel)){
						for($x=0;$x<count($this->arrpreferredtravel);$x++){
							$preferredtypeID = $this->arrpreferredtravel[$x];
							$sql = "INSERT INTO tblTravelertoPreferredTravelType(travelerID,preferredtypeID) VALUES('$this->travelerID','$preferredtypeID')";
							$this->rs->Execute($sql);		
						}
					}

					//country Interested 					
					$sql = "DELETE  from tblTravelertoCountriesInterested WHERE travelerID = '$this->travelerID'";
					$this->rs->Execute($sql);


					if(count($this->arrcountryinterested)){
						for($x=0;$x<count($this->arrcountryinterested);$x++){
							$countryID = $this->arrcountryinterested[$x];
							$sql = "INSERT INTO tblTravelertoCountriesInterested(travelerID,countryID) VALUES('$this->travelerID','$countryID')";
							$this->rs->Execute($sql);		
						}
					}

					//country travelled 					
					$sql = "DELETE  from tblTravelertoCountriesTravelled WHERE travelerID = '$this->travelerID'";
					$this->rs->Execute($sql);

					if(count($this->arrcountrytravelled)){
						for($x=0;$x<count($this->arrcountrytravelled);$x++){
							$IDs = explode('_', $this->arrcountrytravelled[$x]);
							$locationID = 0;
							if(count($IDs) == 2){
								$countryID = $IDs[0];
								$locationID = $IDs[1];
							}else{
								$countryID = $this->arrcountrytravelled[$x];
								$country = new Country($countryID);
								$locationID = ($country instanceOf Country) ? $country->getLocationID() : $locationID;
							}
							$sql = "INSERT INTO tblTravelertoCountriesTravelled(travelerID,countryID,locationID) VALUES('$this->travelerID','$countryID', '$locationID')";
							$this->rs->Execute($sql);		
						}
					}
				}
	 			$this->invalidateCacheEntry();
	 		}
			
			// function update email and username
			function UpdateAccountSettings(){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				$uname = addslashes($this->username);
				$sql = "UPDATE tblTraveler SET username = '$uname', email = '$this->email' WHERE travelerID = '$this->travelerID' " ;
	 			$this->rs->Execute($sql);
				$this->invalidateCacheEntry();
			}
	 	
	 	
	 	// other methods
	 		
	 		
	 		// increment number of views for the profile
	 		function addViews(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$incviews = $this->views + 1;
	 			
	 			$sql = "UPDATE tblTraveler SET views = '$incviews' WHERE travelerID = '$this->travelerID'" ;
	 			$this->rs->Execute($sql);
	 			
	 		}
	 		
	 		
	 		// get the current age of the traveler
	 		function getAge(){
		 		
		 		if ($this->age == NULL){		 			
		 			
		 			$tmstamp_dtnow = time();							// current date in timestamp
		 			$tmstamp_brthdt = strtotime($this->birthdate);		// birthdate in timestamp	 			
		 			
		 			$dtnow   = getdate($tmstamp_dtnow);		// convert timestamp to arrays	
		 			$birthdt = getdate($tmstamp_brthdt);			
		 			
		 			// if profile has birthdate set in database; compute age
		 			if ($this->birthdate != '0000-00-00'){
		 				$birth_monthday = mktime (0 , 0 , 0 , $birthdt['mon'] , $birthdt['mday'] , $dtnow['year']);  // get timestamp birthdate this year
		 			
		 				$yeardiff = $dtnow['year'] - $birthdt['year'] ;	// get the diff btween brthdate and NOW  in years
		 			
		 				if ($tmstamp_dtnow < $birth_monthday)			// if today is before birthdate this year
		 					$yeardiff = $yeardiff - 1;				// subtract age by one year
		 				
		 				$this->age = $yeardiff;
		 			} 
		 		}
		 		
		 		return $this->age;
		 		
	 		}
	 		
	 		
	 		// get the gender of the traveler
	 		function getGender(){
		 		
		 		if ($this->genderstr == NULL){		 			
		 			
		 			if ($this->gender == 1) // male
		 				$this->genderstr = "M";
		 			else if ($this->gender == 2) // female
		 				$this->genderstr = "F";		 				
		 		}
		 		
		 		return $this->genderstr;
		 		
	 		}
	 		
	 		// modified by   : Aldwin Sabornido
	 		// date modified : Oct. 30, 2006
	 		// Change the exception. Instead an exception error, a NULL should be returned.
	 		// get the hometown location of the traveler
	 		function getCity(){
	 			if (is_null($this->objcity)){
	 				try {	
	 					$factory = LocationFactory::instance();
	 					$this->objcity = $factory->create($this->htlocationID);
	 				}
					catch (Exception $e) {
					   return NULL;
					}					 					 									
	 			}
	 		
	 			return $this->objcity; 
	 		}
	 		
	 		
	 		/**#######################################
	 		 # Added by: Joel
	 		 # Purpose: get current location of a traveler (city or by country)
	 		 # Nov 16 2006
	 		 ########################################*/
	 		function getCurrLocation(){
	 			if (is_null($this->mCurrentLocation)) { 			
	 				try {	
	 					$factory = LocationFactory::instance();
	 					$this->mCurrentLocation = $factory->create($this->currlocationID);
	 				}
					catch (Exception $e) {
					   return NULL;
					}	 					 					 									
	 			}
	 			
				return $this->mCurrentLocation;
	 		}
	 		 
	 		
	 		// get the URL of the profile
	 		function getProfileUrl(){
	 			
	 			if ($this->profileurl == NULL){
	 				$this->profileurl = ''; //  temp set to empty string
	 			}
	 			
	 			return $this->profileurl;
	 		} 
	 		
	 		
	 		// get random photo from the traveler profile
	 		function getRandomPhoto(){
	 			if ($this->randphoto == NULL){
	 				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 				
	 				// get the number of photos to choose from
	 				$sql = "SELECT count(photoID) as totalrec from tblTravelertoPhoto WHERE travelerID = '$this->travelerID' ";
	 				$this->rs->Execute($sql);
	 				
	 				if ($this->rs->Result(0,"totalrec") == 0){
	 					try {
	 						$this->randphoto = new Photo($this);
	 					}
						catch (Exception $e) {						  
						   throw $e;
						}
	 				}	 				
	 				else {
	 					
	 					$max = $this->rs->Result(0,"totalrec") - 1;
						
						$randidx = mt_rand(0 , $max );	 		// the index starting from top row
						
	 					$sql = "SELECT photoID FROM tblTravelertoPhoto WHERE travelerID = '$this->travelerID' ORDER BY photoID LIMIT $randidx , 1"  ;
	 					$this->rs->Execute($sql);
	 					
	 					while ($recordset = mysql_fetch_array($this->rs->Resultset())) {
			 				try {	
			 					$this->randphoto = new Photo($this, $recordset['photoID'] );
			 				}
							catch (Exception $e) {						  
							   throw $e;
							}
	 					}
	 				}
	 			}
	 			
	 			return $this->randphoto;
	 		}
	 		
	 		/**
	 		 * Returns the primary photo of the traveler.
	 		 * 
	 		 * @return Photo|null the primary photo of the traveler.
	 		 */
	 		function getPrimaryPhoto(){
	 			if (is_null($this->primphoto)){
		 			/*try {
		 				$handler = GanetDbHandler::getDbHandler();
		 				
		 				$sql = "SELECT tblPhoto.* FROM tblTravelertoPhoto, tblPhoto " .
		 					" WHERE tblTravelertoPhoto.photoID = tblPhoto.photoID " .
		 					" AND tblPhoto.primaryphoto = 1 " .
		 					" AND tblTravelertoPhoto.travelerID = ".$handler->makeSqlSafeString($this->travelerID);
		 				
		 				$rs = new ResourceIterator($handler->execute($sql));
		 				$this->primphoto = ($rs->valid()) ? new Photo($this, 0, $rs->current()) : new Photo($this, 0);	
		 				
		 			}
		 			catch (exception $ex) {}*/
	 				
	 				require_once('travellog/model2/photo/Class.ganetPhotoRepo.php');
	 				$pr	=	ganetPhotoRepo::instance();
	 				$context	=	new ganetValueObject();
	 				$context->set('TRAVELER_ID',$this->travelerID);
	 				$context->set('TRAVELER_PROFILE',$this);
	 				$context->set('PHOTO_TYPE',PhotoType::$PROFILE);
	 				
	 				$vo			=	new ganetValueObject();
	 				$vo->set('COMMAND','PRIMARY_PHOTO');
	 				$vo->set('CONTEXT',$context);
	 				
	 				$this->primphoto	=	$pr->retrieve($vo);
	 				
	 			}
	 			
	 			return $this->primphoto;
	 		}
	 		
	 		 	 		
	 		// get the profile photos of the traveler
	 		function getPhotos(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			if (count($this->arrphotos) == 0){
	 				
	 				$sql = 	'SELECT tblPhoto.* '.
					'FROM tblPhoto, tblTravelertoPhoto '.
					'WHERE tblTravelertoPhoto.photoID = tblPhoto.photoID '.
					"AND tblTravelertoPhoto.travelerID = ".$this->travelerID.' '.
					'ORDER BY tblTravelertoPhoto.position ASC';
	 				$this->rs->Execute($sql);
	 				
	 				
//	 				if ($this->rs->Recordcount() == 0){
//	 					return NULL;	 	
//	 				}
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {	
	 						$photo = new Photo($this, 0,$recordset);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$this->arrphotos[] = $photo;
	 				}
	 			
	 			}
	 			
	 			return $this->arrphotos;
	 		}
			
			function getCountPhotos(){
				require_once('Cache/ganetCacheProvider.php');
				$cache	=	ganetCacheProvider::instance()->getCache();
				$key	=	'TravelerProfile_Photo_Count_' . $this->travelerID;
				
				if ($cache != null && $count = $cache->get($key)) {
					return $count;
				}else {
				
					$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
					$sql = 	'SELECT tblPhoto.* '.
						'FROM tblPhoto, tblTravelertoPhoto '.
						'WHERE tblTravelertoPhoto.photoID = tblPhoto.photoID '.
						"AND tblTravelertoPhoto.travelerID = ".$this->travelerID.' '.
						'ORDER BY tblTravelertoPhoto.position DESC';
		 				$this->rs->Execute($sql);

					$count	=	$this->rs->Recordcount() ;
					
					if ($cache != null){
						$cache->set($key,$count,array('EXPIRE'=>3600));						
					}
		 			return $count;
				}
			}
			
	 		function getNotPrimaryPhotos(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			if (count($this->arrphotos) == 0){
	 				
	 				$sql = 	'SELECT tblPhoto.* '.
					'FROM tblPhoto, tblTravelertoPhoto '.
					'WHERE tblTravelertoPhoto.photoID = tblPhoto.photoID ' .
					'AND tblTravelertoPhoto.primary = 0'.
					"AND tblTravelertoPhoto.travelerID = ".$this->travelerID.' '.
					'ORDER BY tblTravelertoPhoto.position DESC';
	 				$this->rs->Execute($sql);
	 				
	 				
	 				if ($this->rs->Recordcount() == 0){
	 					return NULL;	 	
	 				}
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {	
	 						$photo = new Photo($this, $recordset['photoID'],$recordset);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$this->arrphotos[] = $photo;
	 				}
	 			
	 			}
	 			
	 			return $this->arrphotos;
	 		}
	 		
	 		// new functions
	 		
	 		
	        //Purpose: Adds photo for this traveler profile.
	         
            function addPhoto($photo){
            	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            	//editted by: joel
            	//################################
            	$sqlq = "SELECT max(position) as mposition from tblTravelertoPhoto WHERE travelerID = '$this->travelerID'";
	 			$this->rs->Execute($sqlq);
            	
            	
            	if($this->rs->Recordcount() > 0){
            			$rec = mysql_fetch_array($this->rs->Resultset());
            			$pos = $rec['mposition']+1;           			
            	}else{
            			$pos =0;	
            	}
            	//################################
            	
            	                
                $photoID 	= $photo->getPhotoID();                
                $sql 			= "INSERT into tblTravelertoPhoto (travelerID, photoID,position) VALUES ('$this->travelerID', '$photoID', '$pos' )";
		 		$this->rs->Execute($sql);
		 		
				$photo->invalidateCacheEntry();
            }
            
            // Purpose: Removes a photo from this photo album.
	        //  Deletes the photo as well as its link to this program.
	        function removePhoto($photoID){
                $this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection()); 
                $sql = "DELETE FROM tblTravelertoPhoto where photoID = '$photoID' ";
		 		$this->rs->Execute($sql);   
		
				$sql = "DELETE FROM tblPhotoAlbumtoPhoto where photoID = '$photoID' ";
		 		$this->rs->Execute($sql);
            }
            
            
            //added by: joel
            
            function reArrangePhoto($marrphotoID){
            	$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            	foreach($marrphotoID as $index => $photoID){
            			 $sql = "UPDATE tblTravelertoPhoto SET position = '$index' WHERE photoID =  '$photoID' ";
		 				$this->rs->Execute($sql);
            	}
            }        
            
      /**
       * Retrieves the comments of this traveler's profile.
       * 
       * @param RowsLimit $rowslimit This is for the LIMIT clause
       * @see RowsLimit
       * @param boolean $isPassport If this is true the comments that are retrieved are the comments whose author is not the owner of this traveler profile. If false, the comments that will be retrieved are all the comments of this traveler's profile.
       * @throws exception
       * @return array Array of Comment object  
       */
      function getComments($rowslimit = null, $isPassport = false) {
      	$handler = new dbHandler();
        $arrComment = array();

				$sql = "SELECT DISTINCT tc.* " .
				  " FROM tblTravelertoComment as ttc, tblComment as tc " .
				  " WHERE ttc.commentID = tc.commentID ".
				  " AND ttc.travelerID = ".$handler->makeSqlSafeString($this->travelerID).
				  " AND tc.isApproved = 1 ";
				$sql .= " ORDER BY tc.commentID DESC ";
				$sql .= (is_null($rowslimit)) ? "" : SqlUtil::createLimitClause($rowslimit);
				
        try {
          $rs = new iRecordset($handler->execute($sql));
          while (!$rs->EOF()) {
            $arrComment[] = new Comment($rs->current());
            try {
              $rs->next();
            }
            catch (exception $ex) {
              throw new exception($ex->getMessage);
            }
          }
  		  }
  		  catch(exception $ex){
  		  	throw new exception($ex->getMessage());
  		  }

				return $arrComment;
			}
			
      /**
       * Retrieves the number of comments of this traveler's profile.
       * 
       * @param boolean $isPassport If this is true the comments that are retrieved are the comments whose author is not the owner of this traveler profile. If false, the comments that will be retrieved are all the comments of this traveler's profile.
       * @throws exception
       * @return integer 
       */
      function getNumberOfComments($isPassport = false) {
      	$handler = new dbHandler();
        $arrComment = array();
        
        $sql = "SELECT COUNT(*) FROM (";
				$sql = "SELECT DISTINCT tc.* " .
				  " FROM tblTravelertoComment as ttc, tblComment as tc, tblTraveler as tt " .
				  " WHERE ttc.commentID = tc.commentID ".
				  " AND ttc.travelerID = ".$handler->makeSqlSafeString($this->travelerID).
				  " AND (tc.author = 0 OR (tt.travelerID = tc.author AND tt.isSuspended = 0)) " .
				  " AND tc.isApproved = 1 ";
				$sql .= ($isPassport) ? " AND tc.author <> ".$handler->makeSqlSafeString($this->travelerID).")" : ")"; 
				
			  try {				
			    $rs = new iRecordset($handler->execute($sql));
			    return $rs->getcnt();
			  }
			  catch(exception $ex){
				  throw new exception($ex->getMessage());
			  }
			}
            
      /**
       * Adds a comment to this traveler's profile.
       * 
       * @param Comment $comment
       * @see Comment
       * @throws exception
       * @return void
       */ 
      function addComment($comment){
        $comment->Create();
        $handler = new dbHandler();
             
        $sql = "INSERT INTO " .
          " tblTravelertoComment(travelerID, commentID) " .
          " VALUES (" .
          "".$handler->makeSqlSafeString($this->travelerID).",".
          "".$handler->makeSqlSafeString($comment->getCommentID()).
          ")";
        
        try {
          $handler->execute($sql);
          $comment->setCommentType(1);
        }
        catch(exception $ex){
        	throw new exception($ex->getMessage());
        }
      }
            
      /**
       * Removes a comment from the traveler's profile.
       * 
       * @param Comment $comment
       * @see Comment
       * @throws exception
       * @return void
       */
      function removeComment($comment){          
        $handler = new dbHandler();
        $commentID = $comment->getCommentID();
                
        $sql = "DELETE FROM tblTravelertoComment " .
          " WHERE `travelerID` = ".$handler->makeSqlSafeString($this->travelerID).
					" AND `commentID` = ".$handler->makeSqlSafeString($comment->getCommentID());    
        
        try {
        	$handler->execute($sql);
        } 
        catch(exception $ex){
        	throw new exception($ex->getMessage());
        }                                       
      }
	 		
	 		
	 		/**
	 		 * Get type of traveler
	 		 * by: Joel
	 		 * Nov. 23. 2006
	 		 */
	 		
	 		function getTravType(){
	 			
	 				try {	
	 					$travelertype = new TravelerType($this->travelertypeID);	 					
	 				}
					catch (Exception $e) {
					   return NULL;
					}	 					 					 									
	 			 		
	 				return $travelertype; 
	 				
	 				
	 		}
	 		
	 		/**
	 		 * Added by Joel 
	 		 * March 1 2007
	 		 * To check If travelerprofile has primary photo
	 		 * @return Boolean 
	 		 */
	 		 
	 		function checkHasprimaryphoto(){
	 			
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
	 			
	 			$sql = "SELECT primaryphoto from tblPhoto, tblTravelertoPhoto " .
	 					"WHERE tblTravelertoPhoto.photoID = tblPhoto.photoID " .
	 					"AND primaryphoto =1 " .
	 					"AND tblTravelertoPhoto.travelerID ='".$this->travelerID."'" ;
				$rs->Execute($sql);
				
				
				if($rs->recordcount() == 0){
					return false;	
				}else{
					return true;
				}
				        											
	 			
	 		}
	 		
	 		
	 		function getTravelerPurpose(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			if ($this->arrpurpose == NULL){
		 			$this->arrpurpose = array();
		 				
		 			if($this->intrnltravel){
		 				$sql = "SELECT tpurposeID,tpurposetypeID from tblTravelertoPurpose WHERE travelerID = '$this->travelerID' ORDER BY tpurposetypeID ASC";
		 				$this->rs->Execute($sql);
		 				
		 				if ($this->rs->Recordcount() == 0){
		 					return NULL;	 	
		 				}
		 				
		 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
		 					try {	
		 						$TravelerPurposeType = new PurposeType($recordset['tpurposetypeID']);
		 					}
							catch (Exception $e) {
							   throw $e;
							}
		 					$this->arrpurpose[] = $TravelerPurposeType;
		 				}	 			
		 			}	 			
	 			}
	 			
	 			return $this->arrpurpose;	 				
	 		}
	 		
	 		
	 		function getTravelerProgramInterested(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			if ($this->arrprograminterested == NULL){
	 			
	 				$this->arrprograminterested = array();
	 				
	 			
	 				$sql = "SELECT tprogintId,programtypeID from tblTravelertoProgramsInterested WHERE travelerID = '$this->travelerID' ";
	 				$this->rs->Execute($sql);
	 				
	 				if ($this->rs->Recordcount() == 0){
	 					return NULL;	 	
	 				}
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {	
	 						$TravelerProgramType = new ProgramType($recordset['programtypeID']);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$this->arrprograminterested[] = $TravelerProgramType;
	 				}	 			
	 				 			
	 			}
	 			return $this->arrprograminterested;	 				
	 		}
	 		
	 		function getTravelerPrefferedTravelType(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			if ($this->arrpreferredtravel == NULL){
	 			
	 				$this->arrpreferredtravel = array();
	 				
	 			
	 				$sql = "SELECT preferredtypeid from tblTravelertoPreferredTravelType WHERE travelerID = '$this->travelerID' ";
	 				$this->rs->Execute($sql);
	 				
	 				if ($this->rs->Recordcount() == 0){
	 					return NULL;	 	
	 				}
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {	
	 						$TravelerPrefferedType = new PreferredTravelType($recordset['preferredtypeid']);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$this->arrpreferredtravel[] = $TravelerPrefferedType;
	 				}	 			
	 			}	
	 				 			
	 			return $this->arrpreferredtravel;	 				
	 		}
	 		
	 		function getTravelerCountriesInterested(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			if ($this->arrcountryinterested == NULL){
	 			
	 				$this->arrcountryinterested = array();
	 				
	 			
	 				$sql = "SELECT * from tblTravelertoCountriesInterested WHERE travelerID = '$this->travelerID' ";
	 				$this->rs->Execute($sql);
	 				
	 				if ($this->rs->Recordcount() == 0){
	 					return NULL;	 	
	 				}
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {	
	 						$country = new Country($recordset['countryID']);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$this->arrcountryinterested[] = $country;
	 				}	 			
	 			}	 			
	 			return $this->arrcountryinterested;	 				
	 		}
	 		
	 		//Get Countries travelled by Traveler based on Selected Countries
	 		function getTravelerCountriesTravelled(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			if ($this->arrcountrytravelled == NULL) {
	 			
		 			$this->arrcountrytravelled = array();
		 			
	 				$sql = "SELECT * from tblTravelertoCountriesTravelled WHERE travelerID = '$this->travelerID' ";
	 				$this->rs->Execute($sql);
	 				
	 				if ($this->rs->Recordcount() == 0){
	 					return array();	 	
	 				}
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {	
	 						$country = new Country($recordset['countryID']);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$this->arrcountrytravelled[] = $country;
	 				}	 			
	 			}	
	 				 			
	 			return $this->arrcountrytravelled;	 				
	 		}
	 		
	 		/**############################################
		     * @return  array
		     * ###########################################*/
	 		function getTravelledCountriesJournalEntry(){
	 			$countrystravelled		= array();
		     	$countryids				= array();
		     	$travelids					= array();
		     	$countryjournalentry	= array();
		     	
		     	
		     	if(count($this->getTraveler()->getTravels())){
		     		foreach($this->getTraveler()->getTravels() as $travel){
		     			$trip = $travel->getTrips();
		     				if(count($trip)){     		
			     				foreach($trip as $trip){
			     					$countryId = $trip->getLocation()->getCountry()->getCountryID();
			     					if(!in_array($countryId,$countryids)){
			     						$countryids[]			 		= $trip->getLocation()->getCountry()->getCountryID();
			     						$countrystravelled[]  		= $trip->getLocation()->getCountry();
			     						$travelids[]					= $trip->getTravelID();
			     						$countryjournalentry[]	= array('countryid' => $trip->getLocation()->getCountry()->getCountryID(), 'travelid' =>$trip->getTravelID());    					
			     					}
			     				}
		     				}
		     		}		
		     	}
		     	
		     	return $countryjournalentry;
		     		
	 		}
	 		
	 		/**#######################################
	 		 # Added by: Aldwin S. Sabornido
	 		 # Purpose: get hometown location of a traveler (city or by country)
	 		 # July 25 2007
	 		 ########################################*/
	 		function getHometownLocation(){
	 				 			
	 				try {	
	 					$factory = LocationFactory::instance();
	 					$currloc = $factory->create($this->htlocationID);
	 				}
					catch (Exception $e) {
					   return NULL;
					}	 					 					 									
	 			 		
	 				return $currloc; 
	 		}
	 		
	 		function getCountEntriesPhotos(){
	 			try {
	 				$handler = GanetDbHandler::getDbHandler();
	 				
		 			$sql = "SELECT COUNT(DISTINCT(tblTravelLogtoPhoto.photoID)) as count FROM tblTravel, tblTravelLog, tblTravelLogtoPhoto " .
		 				" WHERE tblTravel.travelerID = " . $handler->makeSqlSafeString($this->travelerID) .
		 				" AND tblTravelLog.publish = 1 " .
						" AND tblTravel.publish = 1 ".
						" AND tblTravel.deleted = 0 ".
						" AND tblTravelLog.deleted = 0 ".
		 				" AND tblTravel.travelID = tblTravelLog.travelID " .
		 				" AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID ";
		 			
		 			$rs = new ResourceIterator($handler->execute($sql));
		 			
		 			return $rs->getCount();
	 			}
	 			catch (exception $ex) {}
	 		}
	 		
	 		function getTravelerJournalEntriesPhotos(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			
	 			$sql = 'SELECT tblTravelLog.travellogID,tblTravelLogtoPhoto.photoID FROM tblTravel, tblTrips, tblTravelLog, tblTravelLogtoPhoto ' .
	 				   'WHERE tblTravel.travelerID = ' . $this->travelerID . ' ' .
	 				   'AND tblTravel.travelID = tblTrips.travelID ' .
	 				   'AND tblTrips.tripID = tblTravelLog.tripID ' .
	 				   'AND tblTravelLog.publish     = 1 ' .
					   'AND tblTravel.publish        = 1 ' .   
	 				   'AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID ' .
	 				   'ORDER BY tblTravelLogtoPhoto.position DESC'; 
	 			
	 			$this->rs->Execute($sql);  
	 			
	 			
	 			if ($this->rs->Recordcount()){
	 				$jourphotos = array();
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {
	 						$travellog = new TravelLog($recordset['travellogID']);
	 						$photo = new Photo($travellog, $recordset['photoID']);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$jourphotos[] = $photo;
	 				}
	 					
	 					return $jourphotos;
	 				
	 			
	 			}else{
	 				
	 				return NULL;
	 			}
	 		}
	 		
	 		//get Traveler Group Photos..admin only 
	 		function getTravelerGroupPhotos(){
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			//$groups 	= $this->getTraveler()->getGroups();
	 			$gPhotos	= array();
	 			
	 			$sql = 'SELECT tblGroup.groupID,tblGrouptoPhoto.photoID FROM tblGroup, tblGrouptoPhoto ' .
	 				   'WHERE tblGroup.administrator = ' . $this->travelerID . ' ' .
	 				   'AND tblGroup.groupID = tblGrouptoPhoto.groupID ' .
	 				   'ORDER BY tblGrouptoPhoto.position DESC'; 
	 			
	 			$this->rs->Execute($sql);  
	 			
	 			
	 			if ($this->rs->Recordcount()){
	 				$gPhotos = array();
	 				
	 				while ($recordset = mysql_fetch_array($this->rs->Resultset())) {		
	 					try {
	 						$fungroup = new FunGroup($recordset['groupID']);
	 						$photo = new Photo($fungroup, $recordset['photoID']);
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$gPhotos[] = $photo;
	 				}
	 					
	 					return $gPhotos;
	 				
	 			
	 			}else{
	 				
	 				return NULL;
	 			}
	 			
	 			
	 			
	 			/*
	 			
	 			if(count($groups)> 0){
	 				foreach($groups as $group){
	 					
	 					if($group->getAdministrator()->getTravelerID() == $this->travelerID){
	 						
	 						$grpid = $group->getGroupID(); 
	 						
	 						$sql = "SELECT photoID from tblGrouptoPhoto WHERE groupID = '$grpid' ORDER BY position DESC ";
		                    $this->rs->Execute($sql);
		                    
		                    if ($this->rs->Recordcount()){
		                       while ($recordset = mysql_fetch_array($this->rs->Resultset())) {
		                            try {
		                                $photo = new Photo($group, $recordset['photoID']);
		                            }
		                            catch (Exception $e) {
		                               throw $e;
		                            }
		                            $gPhotos[] = $photo;
		                        }
		                    }        
	 						
	 						
	 					}
	 					
	 					
	 				} 
	 				
	 				if(count($gPhotos)>0)
	 					return $gPhotos;
	 				else
	 					return NULL;
	 				
	 			}else{
	 				
	 				return NULL;
	 			}
	 			*/
	 		}
	 		
	 		function getPlanningToGo(){
	 			require_once('travellog/model/Class.TravelDestination.php');
				require_once('Class.Connection.php');
				require_once('Class.Recordset.php');
				$conn = new Connection;
				$rs1  = new Recordset( $conn );
				$objNewMember = new TravelDestination;
			/**	$sql1 = sprintf
							(
								'SELECT  startdate, enddate, country FROM ' .
								'(' .
									'SELECT  travelerID, startdate, enddate, country ' .
									'FROM ' .
									'qryPlanningToGoJournals LEFT JOIN tblSessions ON (tblSessions.session_user_id = qryPlanningToGoJournals.travelerID AND domain = "' . $_SERVER['SERVER_NAME'] . '") ' .
									'UNION ' .
									'SELECT  travelerID, startdate, enddate, country ' .
									'FROM ' .
									'qryPlanningToGoTravelPlans LEFT JOIN tblSessions ON (tblSessions.session_user_id = qryPlanningToGoTravelPlans.travelerID AND domain = "' . $_SERVER['SERVER_NAME'] . '") ' .
								') ' .
								'qryTravelers ' .
								'WHERE 1 = 1 AND travelerID = %d GROUP BY travelerID ORDER BY startdate LIMIT 1'
								,$this->travelerID
							); 
					*/
				// edited by daf; nov13; removed left join to tblSessions since online status of traveler no longer needed	
						$sql1 = sprintf
							(
								'SELECT  startdate, enddate, country FROM ' .
								'(' .
									'SELECT  travelerID, startdate, enddate, country ' .
									'FROM ' .
									'qryPlanningToGoJournals  ' .
									'UNION ' .
									'SELECT  travelerID, startdate, enddate, country ' .
									'FROM ' .
									'qryPlanningToGoTravelPlans' .
								') ' .
								'qryTravelers2 ' .
								'WHERE 1 = 1 AND travelerID = %d GROUP BY travelerID ORDER BY startdate LIMIT 1'
								,$this->travelerID
							);			
			
				$rs1->Execute($sql1);
					
				if( $rs1->Recordcount() ){
					$objNewMember->setStartDate  ( $rs1->Result(0, 'startdate')  );
					$objNewMember->setEndDate    ( $rs1->Result(0, 'enddate')    );
					$objNewMember->setCountryName( $rs1->Result(0, 'country')    );
				}
				
				return $objNewMember;
	 		}
	 		
	 		public static function getTravelerContext($photoID){
				$mConn	= new Connection();
				$mRs	= new Recordset($mConn);
				
				$sql = "SELECT travelerID
						FROM tblTravelertoPhoto
						WHERE photoID = $photoID
						GROUP BY travelerID";
				$mRs->Execute($sql);
				
				$profile = FALSE;
				
				if( $mRs->Recordcount() ){
					require_once 'travellog/model/Class.Traveler.php';
					while( $row = mysql_fetch_assoc($mRs->Resultset()) ){
						$traveler = new Traveler($row["travelerID"]);
						$profile = $traveler->getTravelerProfile();
					}
				}
				return $profile;
			}
			
			function getOriginalUploadedIDs(){
				return array();
			}
			
			public function invalidateCacheEntry(){
				$this->getTraveler()->invalidateCacheEntry();
			}
	 		
			function hasActionPrivilege($traveler_id){
				return ($this->getTravelerID() == $traveler_id);	
			}	 		
 	}
 	
?>