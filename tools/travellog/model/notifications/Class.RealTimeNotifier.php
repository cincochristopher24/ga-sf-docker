<?php

	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');

	class RealTimeNotifier {
		
		const GROUP_EMAIL_INVITATION 	= 'GROUPEMAILINVITE';
		const GROUP_INVITE_STAFF		= 'GROUPINVITESTAFF';
		const GROUP_ASSIGN_STAFF_NON_GA	= 'GROUPASSIGNSTAFFNONGA';
		const GROUP_INVITE_TRAVELER		= 'GROUPINVITETRAVELER';
		const CREATED_AUTOMATED_TRAVELER= 'CREATEAUTOMATEDTRAVELER';
		const GROUP_ASSIGN_MEMBER		= 'GROUPASSIGNEDMEMBERSTOGROUP';
		const PERSONAL_MESSAGE			= 'PERSONALMESSAGE';
		const APPROVE_SPAM_MESSAGE		= 'APPROVESPAMMESSAGE';
		const APPROVED_JOURNAL			= 'APPROVEDJOURNAL';
		const RECEIVED_GREETING			= 'RECEIVEDGREETING';
		const ASSIGNED_MEMBER_STAFF		= 'ASSIGNEDMEMBERSTAFF';
		const SUBMIT_NEWS				= 'SUBMITNEWS';
		const POST_EVENT				= 'POSTEVENT';
		const ADD_ADDRESSBOOK_ENTRY		= 'ADDADDRESSBOOKENTRY';
		const REQUEST_FRIEND			= 'REQUESTFRIEND';
		const CONFIRM_ADDRESSBOOK_ENTRY	= 'CONFIRMADDRESSBOOKENTRY';
		const PROWORLD_ONE_TIME			= 'PROWORLDONETIME';
		const ADD_DISCUSSION_POST		= 'ADDDISCUSSIONPOST';
		const ADD_DISCUSSION_TOPIC		= 'ADDDISCUSSIONTOPIC';
		const ADMIN_DELETE_ARTICLE_ENTRY = 'ADMINDELETEARTICLEENTRY';
		const NEW_SURVEY                = 'NEWSURVEY';
		
		private static $instantNotifications = array();
		
		public static function getInstantNotification($key, $opts = array()){
			if(isset(self::$instantNotifications[strtoupper($key)])){
				// notifications
				self::$instantNotifications[strtoupper($key)]->createNotifications($opts);
				return self::$instantNotifications[strtoupper($key)];
			}
			return null;
		}
		
		public static function getAllInstantNotifications(){
			return self::$instantNotifications;
		}
		
		public static function registerInstantNotifications(){
			$arDir = explode(DIRECTORY_SEPARATOR,__FILE__);
			$strategyDir = implode(DIRECTORY_SEPARATOR, array_slice($arDir,0,count($arDir)-1)).DIRECTORY_SEPARATOR.'realtimenotifications';
			$strategyFiles = scandir($strategyDir);
			foreach($strategyFiles as $iFile){
				//check if it is a php file
				if('InstantNotification.php' == substr($iFile,strlen($iFile)-23, 23)){
					$comKey = strtoupper(str_replace(array('Class.','InstantNotification.php'),'',$iFile));
					$clsName = str_replace(array('Class.','.php'),'',$iFile);
					require_once('travellog/model/notifications/realtimenotifications/'.$iFile);
					self::$instantNotifications[$comKey] = new $clsName();
				}
			}
		}
	}
	RealTimeNotifier::registerInstantNotifications();
?>