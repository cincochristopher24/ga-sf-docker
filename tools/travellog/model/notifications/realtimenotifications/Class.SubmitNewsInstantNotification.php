<?php
	
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');

	class SubmitNewsInstantNotification implements InstantNotification {
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function createNotifications($options = array()){
			$default = array(
				'NEWS' 				=> null,
				'INCLUDE_MESSAGE'	=> false
			);
			$options 	= array_merge($default, $options);
			$news		= $options['NEWS'];
			if( is_null($news) )
				return;
			
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 	= SiteContext::getInstance();
			$emailName 		= $domainConfig->getEmailName();
			$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl 		= $domainConfig->getServerName();
			try{
				$group		= GroupFactory::instance()->create(array($news->getGroupID()));
				$group		= $group[0];
				$members	= $group->getMembers();
				
				foreach( $members as $iMember ){
					if($iMember->isSuspended() || $iMember->isDeactivated()){
						continue;
					}
					$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($iMember->getTravelerID());
					if( FALSE == $canReceiveNotification ){
						continue;
					}
					
					$footerVars = array(
						'isAdvisor'	=> false,
						'siteName'	=> $siteName,
						'siteUrl'	=> $siteUrl,
						'group'		=> $group
					);
					
					$tplVars = array_merge($footerVars,array(
						'recipient'	=> $iMember->getUsername(),
						'news'		=> $news,
						'emailName'	=> $emailName,
						'includeMessage' => $options['INCLUDE_MESSAGE'],
						'notificationFooter' => NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
					));
					
					$notification 	= new Notification;
					$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.SubmitNewsInstantNotificationTemplate.php'));
					$notification->setSubject("News from your group ".$group->getName());
					$notification->setSender($domainConfig->getAdminEmail());
					$notification->setSenderName($emailName);
					$notification->addRecipient($iMember->getTravelerProfile()->getEmail());
					$this->mNotifications[] = $notification;
				}
			}
			catch(exception $e){}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
	}

?>