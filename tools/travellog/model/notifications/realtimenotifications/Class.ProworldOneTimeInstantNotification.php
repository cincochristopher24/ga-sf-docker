<?php
	
	// this class is intented to send one time notifications for Proworld's requests
	
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/Class.AdminGroup.php');
	require_once('travellog/model/Class.Traveler.php');

	class ProworldOneTimeInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		private $mDb 			= null;
		
		const PROWORLD_GID		= 245;
		
		public function __construct(){
			$this->mDb = new dbHandler;
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
		
		public function createNotifications($options = array()){
			$default = array(
				'ACTION' => 'sendNewFeatureNotification'
			);
			$options = array_merge($default, $options);
			if( method_exists($this,$options['ACTION']) )
				$this->{$options['ACTION']}($options);
		}
		
		private function sendNewFeatureNotification($options=array()){
			// disabled send script already executed
			return;
			
			$config		= Config::findByGroupID(self::PROWORLD_GID);
			$siteName 	= $config->getEmailName();
			$siteUrl 	= $config->getServerName();
			$proworld	= $config->getAdminGroup();
			$members 	= $proworld->getMembers();
			
			// send notification to each member
			foreach( $members as $iMember ){
				$tplVars = array(
					'member'	=> $iMember,
					'proworld'	=> $proworld,
					'siteName'	=> $siteName,
					'siteUrl'	=> $siteUrl
				);
				
				$notification = new Notification;
				$notification->setSender($config->getAdminEmail());
				$notification->setSenderName($config->getEmailName());
				$notification->setSubject('ProWorld.NET Updates - New Features');
				$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.ProworldNewFeatureNotificationTemplate.php'));
				$notification->addRecipient($iMember->getTravelerProfile()->getEmail());
				$this->mNotifications[] = $notification;
			}
		}
		
		// for russell's request
		private function resendProworldInvitation($options=array()){
			// disabled send script already executed
			return;
			$default = array(
				'TRAVELER'	=> null
			);
			$options = array_merge($default,$options);
			
			$traveler		= $options['TRAVELER'];
			if( !$traveler instanceof Traveler ) return;
			$domainConfig 	= Config::findByGroupID(self::PROWORLD_GID);
			$siteName 		= 'ProWorld Service Corps';
			$siteUrl 		= 'www.MyProWorld.NET';
			$siteHomeUrl	= 'www.myproworld.org';
			$senderName		= 'ProWorld';
			$subject		= 'Your ProWorld.NET Invitation';
			$sender			= $domainConfig->getAdminEmail();
			
			$profile 		= $traveler->getTravelerProfile();
			$email 			= $profile->getEmail();
			$bcc			= 'admin@myproworld.org';
			
			if( GaString::isValidEmailAddress($email) ){
				
				$vars = array(
					'siteUrl' 		=> $siteUrl,
					'siteHomeUrl'	=> $siteHomeUrl,
					'siteName'		=> $siteName,
					'traveler'		=> $traveler,
					'profile'		=> $profile,
					'senderName'	=> $senderName,
					'extra'			=> 'Sorry for the confusion but we mistakenly created your personalize account on IEP the academic backer for our programs instead of for ProWorld. Below are the details for your corrected ProWorld.NET account.'
				);
				
				$notification = new Notification;
				$notification->setSender($sender);
				$notification->setSenderName($senderName);
				$notification->setMessage(NotificationComposer::compose($vars, 'tpl.CreateAutomatedTravelerInstantNotificationTemplate.php'));
				$notification->setSubject($subject);
				$notification->addRecipient($email);
				$notification->addBccRecipient($bcc);
				$notification->addBccRecipient('thea.moraleta@goabroad.com');
				$this->mNotifications[] = $notification;
			}
			
		}
		
	}

?>