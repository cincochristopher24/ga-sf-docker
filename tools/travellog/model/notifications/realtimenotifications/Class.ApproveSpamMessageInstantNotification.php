<?php
require_once ('travellog/model/notifications/Class.Notification.php');
require_once ('travellog/model/notifications/Class.NotificationComposer.php');
require_once ('travellog/model/notifications/Class.NotificationException.php');
require_once ('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
require_once ('travellog/model/notifications/Class.NotificationSender.php');

require_once ('travellog/model/Class.SiteContext.php');
require_once ('travellog/model/Class.NotificationSetting.php');
require_once ('Class.GaString.php');
require_once ('Class.dbHandler.php');

/*require_once ('travellog/model/Class.MessageSource.php');
require_once ('travellog/model/Class.MessageType.php');
require_once ('travellog/model/Class.SendableFactory.php');
require_once ('travellog/model/Class.MessageFactory.php');
require_once ('travellog/model/Class.GroupFactory.php');*/
require_once ('travellog/model/messageCenter/Class.gaGroup.php');
require_once ('travellog/model/messageCenter/Class.gaGroupMapper.php');
require_once ('travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php');

class ApproveSpamMessageInstantNotification implements InstantNotification {

	public function __construct() {
		$this->mDb = new dbHandler;
	}

	public function createNotifications($options = array ()) {
		$this->mNotifications = array ();
		$default = array (
			'MESSAGE' => null
		);
		$options = array_merge($default, $options);

		if (!($options['MESSAGE'] instanceof GanetSpamMessage))
			return;

		try {
			$message = $options['MESSAGE'];
			$messageSource = $message->getSender();
			$recipients = $message->getRecipients();
			$isForGroup = $message->isForGroupStaff();
			$domainConfig = SiteContext :: getInstance();
			$footerVars = array();
			$vars = array();
			
			foreach ($recipients as $iRecipient) {
				$allowNotif = true;
				
				$emailName = $domainConfig->getEmailName();
				$siteName = preg_replace(array (
					'/^(the)\s/i',
					'/\s(team)$/i'
				), '', $emailName);
				$siteUrl = $domainConfig->getServerName();
				$sender = $domainConfig->getAdminEmail();
				
				if ($isForGroup) {
					foreach ($message->getGroups() as $group) {
						$groupID = $group->getGroupID();
						
						//check if group is suspended
						$groupObj = new Group();
						$groupObj->setGroupID($groupID);
						if ($groupObj->isSuspended()) {
							$allowNotif = false;
						}
						else {
							if (in_array($iRecipient->getID(), $group->getOverSeerIDs())) {
								if ($iRecipient->getID() == $group->getAdministratorID()) {
									$footerVars['isAdvisor'] = true;
									/** 
									 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
									 *				cobrand site and recipient is admin of group, use cobrand
									 */
									$parentID = $group->isParent() ? $group->getID() : $group->getParentGroup()->getID();
									$cb = ($cb = Config :: findByGroupID($parentID)) ? $cb : null;
									if (!is_null($cb)) {
										$siteUrl = $cb->getServerName();
										$sender = $cb->getAdminEmail();
										$emailName = $cb->getEmailName();
										$siteName = preg_replace(array (
											'/^(the)\s/i',
											'/\s(team)$/i'
										), '', $emailName);
										if (FALSE === strpos($siteUrl, "dev.") && (strpos($_SERVER["SERVER_NAME"], "dev.") || 0 === strpos($_SERVER["SERVER_NAME"], "dev."))) {
											$siteUrlTokens = explode(".", $siteUrl);
											$temp = array ();
											foreach ($siteUrlTokens as $token) {
												if ("goabroad" == strtolower($token)) {
													$temp[] = "dev";
												}
												$temp[] = strtolower($token);
											}
											$siteUrl = join(".", $temp);
										}
									}
								} else {
									$footerVars['isStaff'] = true;
								}
								try {
									$footerVars['group'] = $group;
									if ($group->isSubGroup())
										$footerVars['isSubgroup'] = true;
			
								} catch (exception $e) {}
							}
							else {
								//meaning recipient is not staff anymore, just delete the message instead
								$allowNotif = false;
								$message->deleteOne($iRecipient->getID());
							}
						}
					}
				}
				
				//check if sender is blocked by recipient, just delete the message instead
				$messageSourceObj = new Traveler();
				$messageSourceObj->setTravelerData(array('travelerID' => $message->getSenderID()));
				if ($messageSourceObj->isBlocked($iRecipient->getID())) {
					$allowNotif = false;
					$message->deleteOne($iRecipient->getID());
				}
				
				//check if recipient is suspended
				$recipientObj = new Traveler();
				$recipientObj->setTravelerData(array('travelerID' => $iRecipient->getID()));
				if ($recipientObj->isSuspended() || $recipientObj->isDeactivated()){
					$allowNotif = false;
				}
					
					
				// check if recipient's notification preference is set to can receive notification
				require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
				$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($iRecipient->getID());
				if( FALSE == $canReceiveNotification ){
					return;
				}
				// end check
				
				
				if ($allowNotif) {
					$footerVars['siteName'] = $siteName;
					$footerVars['siteUrl'] = $siteUrl;
		
					$vars = array_merge($footerVars, array (
						'newMessage' => $message,
						'sender' => $messageSource,
						'recipient' => $iRecipient,
						'isMessageIncluded' => $message->getIsMessageIncludedInNotif(),
						'emailName' => $emailName,
						'notificationFooter' => NotificationComposer :: compose($footerVars, 'tpl.IncNotificationFooter.php')));
		
					$notification = new Notification;
					$notification->setMessage(NotificationComposer :: compose($vars, 'tpl.NewMessageCenterNotificationTemplate.php'));
					$notification->setSubject($messageSource->getName() . ' sent you a message on ' . preg_replace(array (
						'/^(the)\s/i',
						'/\s(team)$/i'
					), '', $siteName));
					$notification->setSender($sender);
					$notification->setSenderName($emailName);
					$notification->addRecipient($iRecipient->getEMailAddress());
					$this->mNotifications[] = $notification;
				}
				unset($footerVars);
				unset($vars);
			}
			 
		} catch (exception $e) {}
	}

	public function send($isInstant = true) {
		NotificationSender :: getInstance()->send($this->mNotifications, $isInstant);
		unset($this->mNotifications);
	}
}
?>