<?php
	require_once('travellog/model/Class.PrivacyPreference.php');
	require_once('travellog/model/Class.PosterPeer.php');
	
	require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
	
	
	class AddDiscussionPostInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function createNotifications( $options = array() ){
			
			$this->mNotifications = array();
			$default = array(
				'POST' 		=> null, // required
				'DISCUSSION'=> null
			);
			$options 		= array_merge($default, $options);
			if( is_null($options['POST']) ) // post is required
				return;
			
			$post = $options['POST'];
			$discussion = !is_null($options['DISCUSSION']) && $options['DISCUSSION'] instanceof Discussion ? $options['DISCUSSION'] : $post->getDiscussion();
			$topic	= $discussion->getTopic();
			$group = $topic->getGroup();
			
			$allPosters	= $discussion->getPosters();
			$poster	= $post->getPoster();
			
			$config 		= SiteContext::getInstance();
			$emailName		= $config->getEmailName();
			$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl		= $config->getServerName();
			$sender			= $config->getAdminEmail();
			
			$firstPost		= $discussion->getFirstPost();
			$isFirstPost 	= ($firstPost->getId() == $post->getId());
			$isAdvisor 		= ($group->getAdministratorID() == $post->getPosterId());
			
			if ($isFirstPost)
				$subject 	= 'New Discussion on '. GaString::makePossessive($group->getName()) .' Discussion Board';
			else
				$subject 	= 'New Reply Posted on '. GaString::makePossessive($group->getName()) .' Discussion Board';
				
			$template 		= 'tpl.AddDiscussionPostNotificationTemplate.php';
			$usedIDs 		= array();
			
			$footerVars		= array(
				'siteName'	=> $siteName,
				'emailName'	=> $emailName,
				'siteUrl'	=> $siteUrl,
				'group'		=> $group
			);
			
			$commonVars	= array_merge($footerVars, array(
				'poster'		=> (PosterPeer::TRAVELER == $poster->getPosterType()) ? $poster->getUserName() : $poster->getName(),
				'discussion'	=> $discussion->getTitle(),
				'postLink'		=> '/discussion/posts/'.$discussion->getId().'#'.$post->getId(),
				'dboardLink'	=> '/discussion_board/home/'.$group->getGroupID(),
				'isFirstPost' 	=> $isFirstPost
			));

			// if we are in a subgroup, notify members of the subgroup and the administrator of the parent group
			if( 0 < $group->getParentID() ){
				foreach( $group->getMembers() as $iMember ){
					// TODO: check privacy preference
					if( $iMember->getTravelerID() != $post->getPosterId() && !in_array($iMember->getTravelerID(), $usedIDs) && (9 == $iMember->getPrivacyPreference()->getModeOfReceivingNotificationFromSubgroupDiscussionActivities())){
						if($iMember->isSuspended() || $iMember->isDeactivated()){
							continue;
						}
						// check if recipient's notification preference is set to can receive notification
						$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($iMember->getTravelerID());
						if( FALSE == $canReceiveNotification ){
							continue;
						}
						// end check
						
						$isAdvisor 	= false;
						
						$isStaff = $group->isStaff($iMember->getTravelerID());
						$isSubgroup = true;
						
						$footerVars = array_merge($footerVars, array('isAdvisor' => $isAdvisor, 'isStaff' => $isStaff, 'isSubgroup'=>$isSubgroup));
						$tplVars 	= array_merge($commonVars, array(
							'recipient'			=> $iMember->getUsername(),
							'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php'),
						));
						$notification = new Notification;
						$notification->setMessage(NotificationComposer::compose($tplVars, $template));
						$notification->addRecipient($iMember->getTravelerProfile()->getEmail());
						$notification->setSubject($subject);
						$notification->setSender($sender);
						$notification->setSenderName($emailName);
						$this->mNotifications[] = $notification;
						
						$usedIDs[] = $iMember->getTravelerID();
					}
				}
			}
			
			// if we're in parent group, notify only those who are participants of the discussion
			else {
				foreach( $allPosters as $iPoster ){
					$travelerObject = (PosterPeer::TRAVELER == $iPoster->getPosterType()) ? $iPoster : $iPoster->getAdministrator(); 
					
					if( $travelerObject->getTravelerID() != $group->getAdministratorID() && $travelerObject->getTravelerID() != $post->getPosterId() && !in_array($travelerObject->getTravelerID(), $usedIDs) && (7 == $travelerObject->getPrivacyPreference()->getModeOfReceivingNotificationFromCoParticipants())){
						$isAdvisor 	= (PosterPeer::ADVISOR == $iPoster->getPosterType());
						
						if($travelerObject->isSuspended() || $travelerObject->isDeactivated()){
							continue;
						}
						
						// check if recipient's notification preference is set to can receive notification
						$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($travelerObject->getTravelerID());
						if( FALSE == $canReceiveNotification ){
							continue;
						}
						// end check
						
						$isStaff = false;
						$isSubgroup = false;
						if( !$isAdvisor ){
							$isStaff = $group->isSuperStaff($travelerObject->getTravelerID());
						}
						
						$footerVars = array_merge($footerVars, array('isAdvisor' => $isAdvisor, 'isStaff' => $isStaff, 'isSubgroup'=>$isSubgroup));
						$tplVars 	= array_merge($commonVars, array(
							'recipient'			=> (PosterPeer::TRAVELER == $poster->getPosterType()) ? $poster->getUserName() : $poster->getName(),
							'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
						));
						
						$notification = new Notification;
						$notification->setMessage(NotificationComposer::compose($tplVars, $template));
						$notification->addRecipient($travelerObject->getTravelerProfile()->getEmail());
						$notification->setSubject($subject);
						$notification->setSender($sender);
						$notification->setSenderName($siteName);
						$this->mNotifications[] = $notification;
						
						$usedIDs[] = $travelerObject->getTravelerID();
					}
				}	
			}
			
			$isSubgroup = (0 < $group->getParentID()) ? true : false;
			
			// Do not send email to administrator if it is in subgroup
			if ($isSubgroup AND in_array($group->getAdministratorID(), $usedIDs)) {
				$key = array_search($group->getAdministratorID(), $usedIDs);
				unset($usedIDs[$key]);
			}
			
			// send notification to group administrator
			if (!$isSubgroup && $group->getAdministratorID() != $post->getPosterId() && !in_array($group->getAdministratorID(), $usedIDs)) {
				$tempGroup 	= $group->getParentID() ? $group->getParent() : $group;
				$isAdvisor 	= true;
				
				// check if recipient's notification preference is set to can receive notification
				$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($group->getAdministratorID());
				if( FALSE == $canReceiveNotification ){
					continue;
				}
				// end check
				

				/** 
				 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
				 *				cobrand site and recipient is admin of group, use cobrand
				 */
				// if group has a cobrand site use respective cobrand, else, use dotNet
				$parentID = 0 < $group->getParentID() ? $group->getParentID() : $group->getGroupID(); 
				$cb = ( $cb	= Config::findByGroupID($parentID)) ? $cb : null;
				if ( !is_null($cb) ){
					$siteUrl	= $cb->getServerName();
					$sender 	= $cb->getAdminEmail();
					$emailName	= $cb->getEmailName();
					$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				}

				$footerVars		= array(
					'siteName'	=> $siteName,
					'emailName'	=> $emailName,
					'siteUrl'	=> $siteUrl,
					'group'		=> $group
				);

				$footerVars = array_merge($footerVars, array('isAdvisor' => $isAdvisor));
				$tplVars 	= array_merge($commonVars, array(
					'recipient'	=> $tempGroup->getName(),
					'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
				));
				$notification = new Notification;
				$notification->setMessage(NotificationComposer::compose($tplVars, $template));
				$notification->addRecipient($group->getAdministrator()->getTravelerProfile()->getEmail());
				$notification->setSubject($subject);
				$notification->setSender($sender);
				$notification->setSenderName($siteName);
				$this->mNotifications[] = $notification;
				
				$usedIDs[]	= $tempGroup->getAdministratorID();
			}
			
			// if subgroup, send notif to superstaff of parent group
			// Edited: to notify also staff of subgroup
			//TODO: edit after tree-structured group has been implemented
			$superStaffArr = array();
			if( 0 < $group->getParentID() ){
				$superStaffArr  = $group->getParent()->getAllGroupStaff();
				// notify staff of subgroup
				$superStaffArr = array_merge($superStaffArr, $group->getAllGroupStaff(null, true));
			}else{
				$superStaffArr = $group->getAllGroupStaff();
			}
			foreach( $superStaffArr as $superStaff ){
				// TODO: check privacy preference 
				if( $superStaff->getTravelerID() != $post->getPosterId() && !in_array($superStaff->getTravelerID(), $usedIDs) && (7 == $superStaff->getPrivacyPreference()->getModeOfReceivingNotificationFromCoParticipants()) ){
					
					// check if recipient's notification preference is set to can receive notification
					$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($superStaff->getTravelerID());
					if( FALSE == $canReceiveNotification ){
						continue;
					}
					// end check
					
					$isAdvisor = false;
					$isStaff = true;

					$footerVars = array_merge($footerVars, array('isAdvisor'=>$isAdvisor, 'isStaff' => $isStaff, 'isSubgroup'=>$isSubgroup));
					$tplVars 	= array_merge($commonVars, array(
						'recipient'			=> $superStaff->getName(),
						'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php'),
					));
					$notification = new Notification;
					$notification->setMessage(NotificationComposer::compose($tplVars, $template));
					$notification->addRecipient($superStaff->getTravelerProfile()->getEmail());
					$notification->setSubject($subject);
					$notification->setSender($sender);
					$notification->setSenderName($emailName);
					$this->mNotifications[] = $notification;
					
					$usedIDs[] = $superStaff->getTravelerID();
				}
			}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
	}

?>