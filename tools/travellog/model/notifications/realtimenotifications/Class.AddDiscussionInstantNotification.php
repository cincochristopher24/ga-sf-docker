<?php
	
	require_once('travellog/model/Class.Post.php');
	require_once('travellog/model/Class.Discussion.php');
	require_once('travellog/model/Class.PrivacyPreference.php');

	class AddDiscussionInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'DISCUSSION'=> null
			);
			$options 	= array_merge($default, $options);
			if( is_null($options['DISCUSSION']) ) // post is required
				return;
			$discussion	= $options['DISCUSSION'];
			$topic= $discussion->getTopic();
			$group= $topic->getGroup(); // the group owner of this topic and discussion
			foreach($group->getMembers() as $member){
				// TODO check preference of each member and create notification for each
			}
		}
		
		public function send(){
			NotificationSender::getInstance()->send($this->mNotifications,false);
		}
	}

?>