<?php

	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('Class.Crypt.php');
	require_once('travellog/model/Class.AddressBookEntry.php');
	require_once('travellog/model/Class.ConfirmationData.php');

	class AddAddressBookEntryInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'ADDRESSBOOK_ENTRY'	=> null,
				'LOGGED_TRAVELER'	=> null,
				'IS_MODERATOR'		=> false,
				'GROUP_ID'			=> 0
			);
			$options 			= array_merge($default, $options);
			$addressBookEntry 	= $options['ADDRESSBOOK_ENTRY'];
			$loggedTraveler		= $options['LOGGED_TRAVELER'];
			//$domainConfig		= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 		= SiteContext::getInstance();
			$siteName 			= $domainConfig->getEmailName();
			$siteUrl 			= $domainConfig->getServerName();
			
			if( !$loggedTraveler instanceof Traveler || !$addressBookEntry instanceof AddressBookEntry )
				return;
			
			if( $options['IS_MODERATOR'] ){
				$gender = 'their';
				try{
					$group 		= GroupFactory::instance()->create(array($options['GROUP_ID']));
					$group 		= $group[0];
					$username 	= $group->getName();
					$ownerName 	= $username;
					$profileLink= 'http://' . $siteUrl . $group->getFriendlyUrl();
				}
				catch(exception $e){return;}
			}
			else{
				$profile		= $loggedTraveler->getTravelerProfile();
				$gender	 		= !is_null($gender = $profile->getGender()) ? ('M' == $gender ? 'his' : 'her') : null;
				$username		= ucwords($loggedTraveler->getUsername());
				$ownerName		= $profile->getFirstName() . ' ' . $profile->getLastName() . '(' . $username . ')';
				$profileLink	= 'http://' . $siteUrl . $loggedTraveler->getFriendlyUrl();
			}
			
			$oID 				= ConfirmationData::getEncryptedTravelerID($addressBookEntry->getOwnerTravelerID());
			$eID 				= ConfirmationData::getEncryptedAddressBookEntryID($addressBookEntry->getAddressBookEntryID());
			$crypt 				= new Crypt;
			$cryptDate 			= $crypt->encrypt(date("Y-m-d"));
			$confirmationLink 	= "http://$siteUrl/confirmnotification.php?oID=$oID&eID=$eID&cnf=1&ds=".urlencode($cryptDate);
			$denyLink 			= "http://$siteUrl/confirmnotification.php?oID=$oID&eID=$eID&cnf=0&ds=".urlencode($cryptDate);
			$tplVars = array(
				'addressBookEntry'	=> $addressBookEntry,
				'ownerName'			=> $ownerName,
				'username'			=> $username,
				'gender'			=> $gender,
				'isModerator'		=> $options['IS_MODERATOR'],
				'profileLink'		=> $profileLink,
				'confirmationLink'	=> $confirmationLink,
				'denyLink'			=> $denyLink,
				'siteName'			=> preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName),
				'siteUrl'			=> $siteUrl
			);
			
			$notification = new Notification;
			$notification->setSender($domainConfig->getAdminEmail());
			$notification->setSenderName($domainConfig->getEmailName());
			$notification->setSubject("Please confirm to receive travel updates from $ownerName");
			$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.AddAddressBookEntryNotificationTemplate.php'));
			$notification->addRecipient($addressBookEntry->getEmailAddress());
			$this->mNotifications[] = $notification;
		}
	}

?>