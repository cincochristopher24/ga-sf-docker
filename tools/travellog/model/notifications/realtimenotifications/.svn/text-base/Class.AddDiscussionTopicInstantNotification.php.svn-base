<?php
	
	require_once('travellog/model/Class.Post.php');
	require_once('travellog/model/Class.Discussion.php');
	require_once('travellog/model/Class.PrivacyPreference.php');
	require_once('travellog/model/Class.Traveler.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/notifications/Class.TravelerNotificationPreference.php');
	
	/**
	 * @todo: Use the new models.
	 */
	
	class AddDiscussionTopicInstantNotification implements InstantNotification {
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function createNotifications( $options = array() ){
			$this->mNotifications = array();
			$default = array(
				'TOPIC'	=> null
			);
			$options 	= array_merge($default, $options);
			if( is_null($options['TOPIC']) ) // topic is required
				return;
			
			$topic				= $options['TOPIC'];
			$parentGroup		= $topic->getGroup(); // the group owner of this topic and discussion
			
			$config 			= SiteContext::getInstance();
			$emailName			= $config->getEmailName();
			$siteName 			= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl			= $config->getServerName();
			$sender				= $config->getAdminEmail();
			$subject 			= 'New topic on '.GaString::makePossessive($parentGroup->getName()) . ' Discussion Board';
			$usedIDs 			= array();
			
			$creator			= new Traveler($topic->getCreatorId());
				 
			if ($creator->isAdvisor()) {
				$creatorName 	= $parentGroup->getName();
				
				if ($parentGroup->getParentID()) 
					$creatorName = $parentGroup->getParent()->getName();
			}	 
			
			else
				 $creatorName 	= $creator->getName();
				 	
			$commonVars	= array(
				'siteName'		=> $siteName,
				'siteUrl'		=> $siteUrl,
				'emailName'		=> $emailName,
				'title'			=> $topic->getTitle(),
				'dboardLink'	=> $dboard->getHomePageUrl(),
				'group'			=> $parentGroup,
				'creator'		=> $creatorName
			);
			
			foreach( $parentGroup->getMembers() as $member ){
				if( $member->getTravelerID() != $topic->getCreatorId() && !in_array($member->getTravelerID(), $usedIDs) && (9 == $member->getPrivacyPreference()->getModeOfReceivingNotificationFromSubgroupDiscussionActivities())){
					if($member->isSuspended() || $member->isDeactivated()){
						continue;
					}
					// check if recipient's notification preference is set to can receive notification
					$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($member->getTravelerID());
					if( FALSE == $canReceiveNotification ){
						continue;
					}
					// end check
					
					if( 0 == $parentGroup->getParentID() ){
						$isStaff = $parentGroup->isSuperStaff($member->getTravelerID());
						$isSubgroup = false;
					}else{
						$isStaff = $parentGroup->isStaff($member->getTravelerID());
						$isSubgroup = true;
					}
					
					$footerVars = array_merge($commonVars, array('isAdvisor' => false, 'isStaff' => $isStaff, 'isSubgroup' => $isSubgroup));
					$tplVars = array_merge($commonVars, array(
						'recipient'			=> $member->getName(),
						'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
					));
					
					$notification = new Notification;
					$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.AddDiscussionTopicNotificationTemplate.php'));
					$notification->setSubject($subject);
					$notification->setSender($sender);
					$notification->setSenderName($siteName);
					$notification->addRecipient($member->getTravelerProfile()->getEmail());
					$this->mNotifications[] = $notification;
					
					$usedIDs[] = $member->getTravelerID();
				}
			}
			
			// send notification to group administrator
			if( $parentGroup->getAdministratorID() != $topic->getCreatorId() && !in_array($parentGroup->getAdministratorID(), $usedIDs) ){
				$tempGroup = $parentGroup->getParentID() ? $parentGroup->getParent() : $parentGroup;

				/** 
				 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
				 *				cobrand site and recipient is admin of group, use cobrand
				 */
				// if group has a cobrand site use respective cobrand, else, use dotNet
				$parentID = 0 < $tempGroup->getParentID() ? $tempGroup->getParentID() : $tempGroup->getGroupID(); 
				$cb = ( $cb	= Config::findByGroupID($parentID)) ? $cb : null;
				if ( !is_null($cb) ){
					$siteUrl	= $cb->getServerName();
					$sender 	= $cb->getAdminEmail();
					$emailName	= $cb->getEmailName();
					$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				}

				$commonVars2	= array(
					'siteName'		=> $siteName,
					'siteUrl'		=> $siteUrl,
					'emailName'		=> $emailName,
					'title'			=> $topic->getTitle(),
					'dboardLink'	=> $dboard->getHomePageUrl(),
					'group'			=> $parentGroup,
					'creator'		=> $creatorName
				);

				$footerVars = array_merge($commonVars2, array('isAdvisor' => true));
				$tplVars = array_merge($commonVars2, array(
					'recipient'			=> $tempGroup->getName(),
					'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
				));
				$notification = new Notification;
				$notification->setMessage(NotificationComposer::compose($tplVars, 'tpl.AddDiscussionTopicNotificationTemplate.php'));
				$notification->setSubject($subject);
				$notification->setSender($sender);
				$notification->setSenderName($siteName);
				$notification->addRecipient($parentGroup->getAdministrator()->getTravelerProfile()->getEmail());
				$this->mNotifications[] = $notification;
				$usedIDs[] = $parentGroup->getAdministratorID();
			}
			
			//send notif to superstaff of parent group
			//Edited: to also notify staff of subgroup
			//TODO: edit after tree-structured group has been implemented
			$isSubgroup = false;
			$tempGroup = $parentGroup;
			$superStaffArr = array();
			if( 0 < $parentGroup->getParentID() ){
				$tempGroup  = $parentGroup->getParent();
				$superStaffArr = array_merge($superStaffArr, $parentGroup->getAllGroupStaff(null, true));
				$isSubgroup = true;
			}
			$superStaffArr = array_merge($superStaffArr, $tempGroup->getAllGroupStaff());
			foreach( $superStaffArr as $superStaff ){
				// TODO: check privacy preference
				if( !in_array($superStaff->getTravelerID(), $usedIDs) && $superStaff->getTravelerID() != $topic->getCreatorId() && (7 == $superStaff->getPrivacyPreference()->getModeOfReceivingNotificationFromCoParticipants())){
					if($superStaff->isSuspended() || $superStaff->isDeactivated()){
						continue;
					}
					// check if recipient's notification preference is set to can receive notification
					$canReceiveNotification = TravelerNotificationPreference::getInstance()->travelerCanReceiveNotification($superStaff->getTravelerID());
					if( FALSE == $canReceiveNotification ){
						continue;
					}
					// end check
					
					$isAdvisor = false;
					$isStaff = true;

					$footerVars = array_merge($footerVars, array('isAdvisor'=>$isAdvisor, 'isStaff' => $isStaff, 'isSubgroup' => $isSubgroup));
					$tplVars 	= array_merge($commonVars, array(
						'recipient'			=> $superStaff->getName(),
						'notificationFooter'=> NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php'),
					));
					$notification = new Notification;
					$notification->setMessage(NotificationComposer::compose($tplVars, $template));
					$notification->addRecipient($superStaff->getTravelerProfile()->getEmail());
					$notification->setSubject($subject);
					$notification->setSender($sender);
					$notification->setSenderName($emailName);
					$this->mNotifications[] = $notification;
					
					$usedIDs[] = $superStaff->getTravelerID();
				}
			}
		}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
	}

?>