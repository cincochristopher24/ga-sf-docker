<?php
	
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	class GroupAssignStaffNonGaInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'TRAVELER' 	=> null,
				'GROUP'		=> null
			);
			$options 		= array_merge($default, $options);
			$traveler 		= $options['TRAVELER'];
			$group 			= $options['GROUP'];
			
			if( is_null($group) || is_null($traveler) )
				return;
			
			$parentGroup 	= !is_null($parentGroup = $group->getParent()) ? $parentGroup : $group;
			//$domainConfig 	= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 	= SiteContext::getInstance();
			$emailName 		= $domainConfig->getEmailName();
			$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
			$siteUrl 		= $domainConfig->getServerName();
			
			$vars = array(
				'traveler'	=> $traveler,
				'parent'	=> $parentGroup,
				'siteName'	=> $siteName,
				'emailName'	=> $emailName,
				'siteUrl'	=> $siteUrl
			);

			$notification = new Notification;
			$notification->setSender($domainConfig->getAdminEmail());
			$notification->setSenderName($emailName);
			$notification->setSubject($parentGroup->getName() . ' wants to add you as a staff');
			$notification->setMessage(NotificationComposer::compose($vars, 'tpl.GroupInviteStaffNonGAMemberNotificationTemplate.php'));
			$notification->addRecipient($traveler->getTravelerProfile()->getEmail());
			$this->mNotifications[] = $notification;
			
		}
	}

?>