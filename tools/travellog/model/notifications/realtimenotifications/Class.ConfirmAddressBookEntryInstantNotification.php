<?php

	// notification for confirming or denying consent

	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/interfaces/Interface.InstantNotification.php');
	require_once('travellog/model/notifications/Class.NotificationSender.php');
	
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/Class.AddressBookEntry.php');
	require_once('travellog/model/Class.GroupFactory.php');
	
	class ConfirmAddressBookEntryInstantNotification implements InstantNotification {
		
		private $mNotifications = array();
		
		public function __construct(){}
		
		public function send($isInstant = true){
			NotificationSender::getInstance()->send($this->mNotifications,$isInstant);
		}
		
		public function createNotifications($options = array()){
			$this->mNotifications = array();
			$default = array(
				'ADDRESSBOOK_ENTRY'	=> null,
				'OWNER'				=> null,
				'CONFIRMED'			=> null
			);
			$options 			= array_merge($default, $options);
			$addressBookEntry 	= $options['ADDRESSBOOK_ENTRY'];
			$owner	 			= $options['OWNER'];
			$ownerProfile		= $owner->getTravelerProfile();
			//$domainConfig		= new Con_fig($_SERVER['SERVER_NAME']);
			$domainConfig 		= SiteContext::getInstance();
			$siteName 			= $domainConfig->getEmailName();
			$siteUrl 			= $domainConfig->getServerName();
			
			if( !$addressBookEntry instanceof AddressBookEntry || !$owner instanceof Traveler || is_null($options['CONFIRMED']) )
				return;
			
			$groupID = $owner->getAdvisorGroup();
			if( 0 < $groupID ){
				try{
					$group		= GroupFactory::instance()->create(array($groupID));
					$group		= $group[0];
					$gender 	= 'they';
					$gender2	= 'their';
					$username	= $group->getName();
					$profileLink= $group->getFriendlyUrl();
				}
				catch(exception $e){return;} // invalid groupID
			}
			else{
				$gender 	= is_null($ownerProfile->getGender()) ? null : ('M' == $ownerProfile->getGender() ? 'he' : 'she');
				$gender2	= is_null($gender) ? 'his/her' : ('he' == $gender ? 'his' : 'her');
				$username	= $owner->getUsername();
				$profileLink= $owner->getFriendlyUrl();
			}
			
			$tplVars = array(
				'addressBookEntry'	=> $addressBookEntry,
				'username'			=> $username,
				'gender'			=> $gender,
				'gender2'			=> $gender2,
				'profileLink'		=> $profileLink,
				'siteName'			=> preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName),
				'siteUrl'			=> $siteUrl
			);
			$tplName = 'tpl.' . ($options['CONFIRMED'] ? 'Accept' : 'Deny') . 'AddressBookEntryNotificationTemplate.php';
			$notification = new Notification;
			$notification->setSender($domainConfig->getAdminEmail());
			$notification->setSenderName($domainConfig->getEmailName());
			$notification->setSubject("Notification Confirmation");
			$notification->setMessage(NotificationComposer::compose($tplVars, $tplName));
			$notification->addRecipient($addressBookEntry->getEmailAddress());
			$this->mNotifications[] = $notification;
		}
	}

?>