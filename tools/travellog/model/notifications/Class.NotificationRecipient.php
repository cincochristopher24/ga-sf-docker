<?php
	class NotificationRecipient
	{
		const TO_RECIPIENT 	= 0;
		const CC_RECIPIENT	= 1;
		const BCC_RECIPIENT	= 2;
		const REPLY_TO_RECIPIENT = 3;
		
		private $mEmailAddress 	= null;
		private $mName 		= null;
		
		public function __construct($emailAdd,$name=''){
			$this->mEmailAddress = $emailAdd;
			$this->mName = $name;
		}
		
		public function getEmailAddress(){
			return $this->mEmailAddress;
		}
		
		public function getName(){
			return $this->mName;
		}
		
	}
?>