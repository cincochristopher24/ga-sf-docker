<?php
	/****
		NOTE: This class only supports SMTP!
	****/
	require_once('class.phpmailer.php');
	require_once('Class.NotificationRecipient.php');
	
	class Notification
	{
		private $mSender 		= null;
		private $mSenderName	= null;
		private $mRecipients	= array();
		private $mCcRecipients	= array();
		private $mBccRecipients = array();
		private $mReplyTo		= array();
		private $mSubject		= null;
		private $mMessage		= null;
		private $mAltMessage	= null;
		
		/**SETTERS**/
		
		public function setSender($sender=null){
			$this->mSender = $sender;
		}
		
		public function setSenderName($name){
			$this->mSenderName = $name;
		}
		
		public function addRecipient($emailAdd,$name=''){
			$this->mRecipients[] = new NotificationRecipient($emailAdd,$name);
		}
		
		public function addCcRecipient($emailAdd,$name=''){
			$this->mCcRecipients[] = new NotificationRecipient($emailAdd,$name);
		}
		
		public function addBccRecipient($emailAdd,$name=''){
			$this->mBccRecipients[] = new NotificationRecipient($emailAdd,$name);
		}
		
		public function addReplyTo($emailAdd, $name=''){
			$this->mReplyTo[] = new NotificationRecipient($emailAdd,$name);
		}
		
		public function setSubject($subject=null){
			$this->mSubject = $subject;
		}
		
		public function setMessage($message=null){
			$this->mMessage = $message;
		}
		
		public function setAltMessage($altMessage=null){
			$this->mAltMessage = $altMessage;
		}
		
		/**GETTERS**/
		public function getSender(){
			return $this->mSender;
		}
		
		public function getSenderName(){
			return $this->mSenderName;
		}
		
		public function getRecipients(){
			return $this->mRecipients;
		}
		
		public function getCcRecipients(){
			return $this->mCcRecipients;
		}
		
		public function getBccRecipients(){
			return $this->mBccRecipients;
		}
		
		public function getReplyTo(){
			return $this->mReplyTo;
		}
		
		public function getSubject(){
			return $this->mSubject;
		}
		
		public function getMessage(){
			return $this->mMessage;
		}
		
		public function getAltMessage(){
			return $this->mAltMessage;
		}
		
		public function send($instantNotification=false){
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->IsHTML(false);
			$mail->From = $this->getSender();
			$mail->FromName = $this->getSenderName();
			$mail->Subject = $this->getSubject();
			$mail->Body = $this->getMessage();

			$developers = array(
				//'reynaldo.castellano@goabroad.com',
				//'chris.velarde@goabroad.com',
				'gapdaphne.notifs@gmail.com',
			//	'virna.alvero@goabroad.com',
				'mercy.honor@gmail.com',
				//'nherrisa.celeste@goabroad.com',
				//'jul.garcia@goabroad.com',
				'nash.lesigon@goabroad.com',
			//	'augustianne.barreta@goabroad.com',
//				'jonas.tandinco@goabroad.com',
			);

			$arrSplit = preg_split('/\./', $_SERVER['SERVER_NAME']);
			// if it is in dev or in local send to developers
			if( in_array('dev', $arrSplit) || in_array('local', $arrSplit) ){
				foreach($developers as $iRecipient){
					$mail->AddAddress($iRecipient);
				}
			}
			else{
				foreach($this->getRecipients() as $iRecipient){
					$mail->AddAddress($iRecipient->getEmailAddress(),$iRecipient->getName());
				}

				foreach($this->getCcRecipients() as $iRecipient){
					$mail->AddCC($iRecipient->getEmailAddress(),$iRecipient->getName());
				}

				foreach($this->getBccRecipients() as $iRecipient){
					$mail->AddBCC($iRecipient->getEmailAddress(),$iRecipient->getName());
				}

				foreach( $this->getReplyTo() as $iRecipient ){
					$mail->AddReplyTo($iRecipient->getEmailAddress(),$iRecipient->getName());
				}
				
				// bcc developers
				foreach($developers as $iRecipient){
					$mail->AddBCC($iRecipient);
				}
			}
			
			if(!$instantNotification){
				echo '<textarea cols="50" rows="30">';
				echo 'From: '.$this->getSender().chr(10);
				echo 'FromName: '.$this->getSenderName().chr(10);
				
				echo chr(10);
				echo 'Subject: '.$this->getSubject().chr(10);
				echo chr(10);
				echo $this->getMessage();
				echo '</textarea>';
				
				//echo  "<br /> WE WILL NOT BE SENDING...... <br />";
				// temporary for debugging, we will not send instant notifications
				//return array('status'=>1,'error'=>'');
				
			}
			
			if($mail->Send()){
				return array('status'=>1,'error'=>'');
			}
			else{
				return array(
					'status'=>0,
					'error' => (!is_null($mail->smtp) ? $mail->smtp->error['error'] : null)
				);
			}
		}
	}
?>