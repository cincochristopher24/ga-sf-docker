<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/Class.Traveler.php');
	
	class TravelerRequestJoinGroupNotificationQueue implements NotificationQueue, ClearableNotificationQueue{
		
		private $mReader 		= NULL;
		private $mNotifications = array();
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = "(logView._EXECUTION_DATE_ >= " . GaString::makeSqlSafe($startDate) . " AND " .
							  "logView._EXECUTION_DATE_ <= " . GaString::makeSqlSafe($endDate) . ")";
			$sql = 	'SELECT * ' .
					'FROM `Travel_Logs`.`tblRequestList` logView ' .
					'WHERE ' .
						'(logView.`_EVENT_` = 1 OR logView.`_EVENT_` = 2) AND ' . $rangeCriteria . ' ' . // also retrieve those who requested again if the group denied previous request
						'AND `_NEW_status` = 0 ' .
						'AND EXISTS (<$ ' .
						'SELECT `groupID` FROM `Travel_Logs`.`tblRequestList` ' .
						'WHERE ' .
							'logView.`_NEW_groupID` = `groupID` ' .
							'AND logView.`_NEW_travelerID` = `travelerID` ' .
							'AND `status` = 0 $>) ' .
					'ORDER BY logView._EXECUTION_DATE_ DESC';
			$rs = $this->mReader->getLogsBySql($sql);
			$rs->dump();
			$this->createNotification($this->groupData($rs->retrieveData()));
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['time']))
					$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['time'] = $row['_EXECUTION_DATE_'];
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['travelers']) OR !in_array($row['_NEW_travelerID'], $items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['travelers']))
					$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['travelers'][] =  $row['_NEW_travelerID'];
				//$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['travelers'] = array_unique($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']]['travelers']);
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
				$emailName 		= $domainConfig->getEmailName();
				$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
				$siteUrl 		= $domainConfig->getServerName();
				
				//add notification for each group
				foreach($iGroupedData as $gID => $details){
					$time = date('F d, Y g:i:s A',strtotime($details['time']));
					$travelers = array();
					try{
						$arrGrp = GroupFactory::instance()->create(array($gID));
						$group = $arrGrp[0];
						// combine all travelers who requested to join this group
						foreach($details['travelers'] as $travelerID){
							try{
								$travelers[] = new Traveler($travelerID);	
							}
							catch(exception $e){
								throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR,$trace[0], '[CLASS]Traveler: ' . $e->getMessage());
							}
						}

						/** 
						 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
						 *				cobrand site and recipient is admin of group, use cobrand
						 */
						// if group has a cobrand site use respective cobrand, else, use dotNet
						//$cb = ( $cb	= Config::findByGroupID($group->getGroupID())) ? $cb : Config::findByGroupID();
						$parentID = 0 < $group->getParentID() ? $group->getParentID() : $group->getGroupID(); 
						$cb = ( $cb	= Config::findByGroupID($parentID)) ? $cb : null;
						if ( !is_null($cb) ){
							$siteUrl	= $cb->getServerName();
							$sender 	= $cb->getAdminEmail();
							$emailName	= $cb->getEmailName();
							$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
						}
						else{
							$domainConfig 	= SiteContext::getInstanceByDomainName($domainName);
							$emailName 		= $domainConfig->getEmailName();
							$siteName		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
							$siteUrl 		= $domainConfig->getServerName();
						}

						$footerVars = array(
							'isAdvisor'	=> true,
							'group'		=> $group,
							'siteName'	=> $siteName,
							'siteUrl'	=> $siteUrl
						);
						
						$vars = array_merge($footerVars, array(	
							'recipient' => $group->getAdministrator()->getUserName(),
							'travelers' => $travelers,
							'time' 		=> $time,
							'emailName'	=> $emailName,
							'notificationFooter' => NotificationComposer::compose($footerVars, 'tpl.IncNotificationFooter.php')
						));
						$notification = new Notification();
						$notification->setSubject("New " . (count($travelers)>1 ? "Requests":"Request") . " to join " . (($group instanceof AdminGroup) ? "group ":"club ") . $group->getName());
						$notification->setSender($domainConfig->getAdminEmail());
						$notification->setSenderName($emailName);
						$notification->setMessage(NotificationComposer::compose($vars,'tpl.TravelerRequestJoinGroupTemplate.php'));
						$notification->addRecipient($group->getAdministrator()->getTravelerProfile()->getEmail());
						$this->mNotifications[] = $notification;
					}
					catch(exception $e){
						
					}
				}
			}
		}
	}
?>
