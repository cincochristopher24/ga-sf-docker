<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	
	require_once('travellog/model/Class.AddressBook.php');
	require_once('travellog/model/Class.TravelerProfile.php');
	require_once('travellog/model/Class.PendingNotification.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	
	require_once('Class.GaString.php');
	
	class TravelerJournalEntryNotificationQueue implements NotificationQueue, ClearableNotificationQueue
	{
		
		private $mDb = null;
		private $mLogReader = null;
		public function __construct(){
			$this->mDb = new dbHandler();
			$this->mLogReader = new Reader();
		}
	
		public function getNotifications($startDate,$endDate){
			/**
			$sql =	'SELECT * <$ , Travel_Logs.tblTravel.travelerID $> ' .
					'FROM `Travel_Logs`.`tblTravelLog` as gaLogTravellog <$, Travel_Logs.tblTravelLog, Travel_Logs.tblTravel $> ' .
					'WHERE _COMMAND_ = \'INSERT\' ' .
					'AND _EXECUTION_DATE_ >= '.GaString::makeSqlSafe($startDate).' ' .
					'AND _EXECUTION_DATE_ <= '.GaString::makeSqlSafe($endDate).' '.
					'AND <$ Travel_Logs.tblTravelLog.travellogID $> = gaLogTravellog._NEW_travellogID ' .
					'AND <$ Travel_Logs.tblTravelLog.travelID = Travel_Logs.tblTravel.travelID $> ' .
					'ORDER BY _EXECUTION_DATE_ DESC';
			$rs = $this->mReader->getLogsBySql($sql);
			**/
			/**
			$sql = 	'SELECT pendingNotificationID, contextID as travelerID, link, section ' .
					'FROM Travel_Logs.tblPendingNotification ' .
					'WHERE context =  '.GaString::makeSqlSafe(PendingNotification::TRAVELER_CONTEXT).' ' .
					'AND (section = \'JOURNAL_ENTRY\' || section = \'PHOTO_UPLOAD\')'.
					'AND dateAdded >= '.GaString::makeSqlSafe($startDate).' ' .
					'AND dateAdded <= '.GaString::makeSqlSafe($endDate);
			**/
			$sql = 	'SELECT _NEW_pendingNotificationID as pendingNotificationID, _NEW_contextID as travelerID, _NEW_link as link, _NEW_section as section , _DOMAIN_NAME_ as domain ' .
					'FROM Travel_Logs.tblPendingNotification ' .
					'WHERE _NEW_context = '.GaString::makeSqlSafe(PendingNotification::TRAVELER_CONTEXT).' ' .
					'AND (_NEW_section = \'JOURNAL_ENTRY\' || _NEW_section = \'PHOTO_UPLOAD\')'.
					'AND _NEW_dateAdded >= '.GaString::makeSqlSafe($startDate).' ' .
					'AND _NEW_dateAdded <= '.GaString::makeSqlSafe($endDate);
			
			$rs = $this->mLogReader->getLogsBySql($sql);
			$rs->dump();
			//group the update by traveler
			$travelerUpdates = array();						
			foreach($rs as $row){
				if(!array_key_exists($row['travelerID'],$travelerUpdates)){
					$travelerUpdates[$row['travelerID']] = array();
				}
				$travelerUpdates[$row['travelerID']][] = $row;
			}
			
			//group each traveler updates by domain
			$domainTravelerUpdates = array();
			foreach($travelerUpdates as $travelerID => $updates){
				$domainTravelerUpdates[$travelerID] = array();
				foreach($updates as $iUpdate){
					$domain = $iUpdate['domain'];
					if(!array_key_exists($domain,$domainTravelerUpdates[$travelerID])){
						$domainTravelerUpdates[$travelerID][$domain] = array(
							'photos' 			=> array(),
							'journalEntries'	=> array()
						);
					}
					if('PHOTO_UPLOAD' == $row['section']){
						$domainTravelerUpdates[$travelerID][$domain]['photos'][] = $row['link'];
					}
					else{
						$domainTravelerUpdates[$travelerID][$domain]['journalEntries'][] = $row['link'];
					}
				}
			}
			
			//get the recipients of each traveler. and compose the message
			$notifications = array();
			foreach($domainTravelerUpdates as $travelerID => $domainUpdates){
				foreach($domainUpdates as $domain => $updates){
					$addressBook 		= new AddressBook($travelerID);
					$addressBookEntries = $addressBook->getNotableAddressBookEntries();
					if(count($addressBookEntries)){
						//$domainConf = new Con_fig($domain);	
						$domainConf = SiteContext::getInstanceByDomainName($domain);
					}
					foreach($addressBookEntries as $iEntry){
						$profile = new TravelerProfile($travelerID);
						$vars = array(
							'recipientName' => (strlen(trim($iEntry->getName())) ? $iEntry->getName() : $iEntry->getEmailAddress()),
							'subjectName'	=> strlen(trim($profile->getFirstName())) ? $profile->getFirstName() : $profile->getUserName(),
							'journalPhotos'	=> $updates['photos'],
							'journalEntries'=> $updates['journalEntries']
						);
						$msg = NotificationComposer::compose($vars);
						$notification = new Notification();
						$notification->setSender($domainConf->getAdminEmail());
						$notification->setMessage($msg);
						$notification->setSubject('Update from your friend '.$profile->getFirstName().' on GoAbroad.net');
						$notification->addRecipient($iEntry->getEmailAddress());
						$notifications[] = $notification;
					}
				}
			}
			return $notifications;
		}
		
		public function clear(){
					
		}
	}
?>