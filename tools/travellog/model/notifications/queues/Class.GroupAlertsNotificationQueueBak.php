<?php
	
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	//require_once('travellog/model/Class.Config.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.AlertMessage.php');
	require_once('travellog/model/Class.SendableFactory.php');
	require_once('travellog/model/Class.MessageSource.php');
	
	require_once('travellog/model/Class.NotificationSetting.php');
	
	class GroupAlertsNotificationQueueBak implements NotificationQueue, ClearableNotificationQueue{
		
		private $mNotifications = array();
		
		public function getNotifications($startDate, $endDate){
			$this->query($startDate, $endDate);
			return $this->mNotifications;
		}
		
		public function clear(){
			NotificationSetting::deleteGroupAlertNotificationSettings();
		}
		
		private function query($startDate, $endDate){
			$trace = debug_backtrace();
			$reader = new Reader();
			$rangeCriteria = "(logView.`_EXECUTION_DATE_` >= " . GaString::makeSqlSafe($startDate) . " AND " .
							  "logView.`_EXECUTION_DATE_` <= " . GaString::makeSqlSafe($endDate) . ")";
			$sql = 	"SELECT logView.*, `tblNotificationSetting`.`_NEW_messageIncluded`,`tblNotificationSetting`.`_NEW_notifyGroupMembers` " .
					"FROM " .
						"`Travel_Logs`.`tblMessageToAttribute` logView LEFT JOIN " .
						"`Travel_Logs`.`tblNotificationSetting` ON " .
							"logView.`_NEW_messageID` = `tblNotificationSetting`.`_NEW_contextID` " .
							"AND `tblNotificationSetting`.`_NEW_context` = ".NotificationSetting::GROUP_ALERT_CONTEXT." ".
					"WHERE " .
						"`_NEW_attributeID` IN ( <$ " .
						"SELECT " .
							"t1.attributeID " .
						"FROM " .
							"`Travel_Logs`.`tblMessageToAttribute` t1, `Travel_Logs`.`tblMessages` t2 " .
						"WHERE " .
							"logView._NEW_attributeID = t1.attributeID " .
							"AND logView._NEW_messageID = t1.messageID " .
							"AND t1.messageID = t2.messageID " .
							"AND t2.discriminator = '1' " .
							"AND logView.`_NEW_recipientID` = t2.`senderID` $> ) " . // retrieve the group sender only
						"AND $rangeCriteria " .
						"AND logView.`_EVENT_` = 1 " .
						"AND tblNotificationSetting._NEW_notifyGroupMembers = 1 ".
					"ORDER BY logView._EXECUTION_DATE_ DESC";
			echo $sql;
			$rs = $reader->getLogsBySql($sql);
			
			$rs->dump();
			$this->createNotification($this->groupData($rs->retrieveData()));
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['time']))
					$items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['time'] = $row['_EXECUTION_DATE_'];
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['recipients'][$row['_NEW_recipientID']][$row['_NEW_attributeID']])) //OR !in_array($row['_NEW_attributeID'], $items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['recipients'][$row['_NEW_recipientID']]))
					$items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['recipients'][$row['_NEW_recipientID']][$row['_NEW_attributeID']] = array('attributeID' => $row['_NEW_attributeID'] , 'messageIncluded' => $row['_NEW_messageIncluded']) ;
				//if(!isset($items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['messages']) OR !in_array($row['_NEW_messageID'], $items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['messages']))
					//$items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['messages'][] = $row['_NEW_attributeID'];
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();
				
				foreach($iGroupedData as $doer => $details){
					foreach($details['recipients'] as $recipient => $attributes){
						// try block for non-existent groups or travelers
						try{
							$obj = SendableFactory::Instance()->Create($recipient);
							$alerts = array();
							$source = NULL;

							foreach($attributes as $iAttributes){
								try{
									$msg = new AlertMessage($iAttributes['attributeID']);	
								}
								catch(exception $e){
									throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR, $trace[0], ' [Class]AlertMessage: ' . $e->getMessage());
								}
								if(is_null($source)) // create only one message source since all alerts will have a common source
									$source = new MessageSource($msg);
								$alerts[] = array('alert' => $msg, 'isMessageIncluded' => ($iAttributes['messageIncluded'] == 1) ? TRUE : FALSE);  
							}
							if('Group' == get_parent_class($obj)){
								foreach($obj->getMembers() as $traveler){
									if($traveler->getTravelerID() != $doer){
										if(!$traveler->isAutomaticallyAdded() && !$traveler->isSuspended() && !$traveler->isDeactivated()){
											//TODO: format time with necessary format
											$time = date('F d, Y g:i:s A',strtotime($details['time']));
											$vars = array(	'group' 		=> $obj,
															'source' 		=> $source,
															'recipient' 	=> $traveler->getUsername(), 
															'time' 			=> $time,
															'siteName' 		=> $siteName,
															'siteUrl' 		=> $siteUrl,
															//'attributes' 	=> $groupedData[$domainName][$doer]['messages'],
															'alerts' 		=> $alerts);
											$notification = new Notification(); 
											$notification->setMessage(NotificationComposer::compose($vars,'tpl.GroupAlertNotificationTemplate.php'));
											$notification->setSubject("New " . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName) . " alert message from group " . $source->getName() . " -- " . $time);
											$notification->setSender($domainConfig->getAdminEmail());
											$notification->setSenderName($domainConfig->getEmailName());
											$notification->addRecipient($traveler->getTravelerProfile()->getEmail());
											$this->mNotifications[] = $notification;
										}
									}
								}
							}
						}
						catch(exception $e){}
					}
				}
			}
		}
	}
?>
