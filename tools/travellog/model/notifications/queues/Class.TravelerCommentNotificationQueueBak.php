<?php


	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/Class.NotificationSetting.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.Comment.php');
	require_once('travellog/model/Class.TravelerProfile.php');
	require_once('travellog/model/Class.AdminGroup.php');
	
	class TravelerCommentNotificationQueueBak implements NotificationQueue, ClearableNotificationQueue {
		
		private $mNotifications 	= array();
		private $mReader 			= NULL;
		
		const ADMIN_NOTIFICATION 	= 'admin';
		const USER_NOTIFICATION 	= 'user';
		
		public function __construct(){
			$this->mReader = new Reader;
		}
		
		public function getNotifications($startDate, $endDate){
			$sql = 	'SELECT ' . 
						'logView.*, `tblTravelertoComment`.`_NEW_travelerID` as TRAVELER_ID, `tblTravelLogtoComment`.`_NEW_travellogID` as TRAVELLOG_ID, `tblCommenttoPhoto`.`_NEW_photoID` as PHOTO_ID ' . 
					'FROM ' .
						'`Travel_Logs`.`tblComment` as logView ' .
						'LEFT JOIN `Travel_Logs`.`tblTravelertoComment` ON `tblTravelertoComment`.`_NEW_commentID` = `tblComment`.`_NEW_commentID` ' .
						'LEFT JOIN `Travel_Logs`.`tblTravelLogtoComment` ON `tblTravelLogtoComment`.`_NEW_commentID` = `tblComment`.`_NEW_commentID` ' .
						'LEFT JOIN `Travel_Logs`.`tblCommenttoPhoto` ON `tblCommenttoPhoto`.`_NEW_commentID` = `tblComment`.`_NEW_commentID` ' .
					'WHERE ' .
						'logView.`_EVENT_` = 1 ' .
						'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe($startDate) . ' AND ' . GaString::makeSqlSafe($endDate) . ' ' .
						'AND EXISTS (<$ SELECT commentID FROM `Travel_Logs`.`tblComment` WHERE logView._NEW_commentID = `tblComment`.`commentID`$>) ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->mReader->getLogsBySql($sql);
			$rs->dump();
			$this->createNotification($this->groupData($rs));
			return $this->mNotifications;
		}
		
		public function clear(){
			//NotificationSetting::deleteTravelerBulletinNotificationSettings();
		}
		
		private function groupData($resource){
			$items = array();
			foreach($resource as $row){
				$s = ('' != trim($row['TRAVELER_ID']) ? 'TRAVELER_ID' : ('' != trim($row['TRAVELLOG_ID']) ? 'TRAVELLOG_ID' : ('' != trim($row['PHOTO_ID']) ? 'PHOTO_ID' : 'default')));
				$items[$row['_DOMAIN_NAME_']][$row['_NEW_author']][$row['_NEW_commentID']] = array('context' => $s , 'contextID' => $row[$s]);
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			
			$contextArr = array('TRAVELER_ID' => CommentType::$PROFILE, 'PHOTO_ID' => CommentType::$PHOTO, 'TRAVELLOG_ID' => CommentType::$TRAVELLOG);
			
			foreach($groupedData as $domainName => $iGroupedData){
				
				//$domainConfig 	= new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName		= $domainConfig->getEmailName();
				$siteUrl		= $domainConfig->getServerName();
				
				foreach($iGroupedData as $author => $comments){

					$groupID = 0;
					if(0 < $groupID){
						$commentAuthor = new AdminGroup($groupID);
					}
					else{
						$commentAuthor = new TravelerProfile($author);
					}

					foreach($comments as $commentID => $iData){
						
						try{ // try-catch any exception thrown in instantiation of needed classes
							
							$comment = new Comment($commentID);
							$comment->setCommentType($contextArr[$iData['context']]);
							$contextObj = $comment->getContext();
							echo get_class($contextObj) . ' context of commentid: ' . $commentID . '<br>';
							$isAutomaticallyAdded = false;
							switch(get_class($contextObj)){
								case 'TravelerProfile' : // comments to traveler profile
									$isAutomaticallyAdded = $contextObj->getTraveler()->isAutomaticallyAdded();
									$recipientName = $contextObj->getUserName();
									$recipient = $contextObj->getEmail();
									$message = 'You received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName() );
									$adminMessage = ucfirst($recipientName) . ' received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName() );
									$link = $contextObj->getUserName();
									break;
								case 'Photo' : // comments to photos
									$photoContext = $contextObj->getContext();

									switch($contextObj->getPhotoTypeID()){
										case PhotoType::$PROFILE : // profile photo
											$link = 'photomanagement.php?action=vfullsize&cat=profile&genID=' . $photoContext->getTravelerID() . '&photoID=' . $contextObj->getPhotoID() ;
											$message = 'Your photo received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName());
											$recipientName = $photoContext->getUserName();
											$recipient = $photoContext->getEmail();
											$adminMessage = GaString::makePossessive(ucfirst($recipientName)) . ' photo received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName());
											$isAutomaticallyAdded = $photoContext->getTraveler()->isAutomaticallyAdded();
											break;
										case PhotoType::$TRAVELLOG : // journal-entry photo
											$owner = $photoContext->getOwner();
											$link = 'journal-entry.php?action=view&travellogID=' . $photoContext->getTravelLogID();
											if($owner instanceof Traveler){
												$recipientName = $owner->getUserName();
												$recipient = $owner->getTravelerProfile()->getEmail();
												$message = 'Your photo received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName());
												$adminMessage = GaString::makePossessive(ucfirst($recipientName)) . ' photo received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName());
												$isAutomaticallyAdded = $owner->isAutomaticallyAdded();
											}
											else{
												$recipientName = $owner->getName();
												$recipient = $owner->getAdministrator()->getTravelerProfile()->getEmail();
												$message = 'Your group ' . GaString::makePossessive($owner->getName()) . ' photo received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName());
												$adminMessage = 'Group ' . GaString::makePossessive($owner->getName()) . ' photo received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName());
												$isAutomaticallyAdded = $owner->getAdministrator()->isAutomaticallyAdded();
											}
											break;
										case PhotoType::$PHOTOALBUM : // photo album
											$owner = $photoContext->getGroup();
											$link = 'photomanagement.php?action=vfullsize&cat=photoalbum&groupID=' . $owner->getGroupID() . '&genID=' . $photoContext->getPhotoAlbumID() . '&photoID=' . $contextObj->getPhotoID();
											$recipient = $owner->getAdministrator()->getTravelerProfile()->getEmail();
											$recipientName = $owner->getAdministrator()->getUserName();
											$message = 'Your group ' . GaString::makePossessive($owner->getName()) . ' photo received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName());
											$adminMessage = 'Group ' . GaString::makePossessive($owner->getName()) . ' photo received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName());
											$isAutomaticallyAdded = $owner->getAdministrator()->isAutomaticallyAdded();
											break;
									}
									break;
								case 'TravelLog' : // comments to journal-entries
									$owner = $contextObj->getOwner();
									$link = 'journal-entry.php?action=view&travellogID=' . $contextObj->getTravelLogID();
									echo 'Travellog owner is a ' . get_class($owner) . '<BR>';
									if('Traveler' == get_class($owner)){
										$recipientName = $owner->getUserName();
										$recipient = $owner->getTravelerProfile()->getEmail();
										$message = 'Your journal-entry received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName() );
										$adminMessage = GaString::makePossessive(ucfirst($recipientName)) . ' journal-entry received a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName() );
										$isAutomaticallyAdded = $owner->isAutomaticallyAdded();
									}
									else{ // owner is group
										$recipientName = $owner->getAdministrator()->getUserName();
										$recipient = $owner->getAdministrator()->getTravelerProfile()->getEmail();
										$message = 'Your group ' . GaString::makePossessive($owner->getName()) . ' journal-entry receieved a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName() );
										$adminMessage = 'Group ' . GaString::makePossessive($owner->getName()) . ' journal-entry receieved a new greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : $commentAuthor->getUserName() );
										$isAutomaticallyAdded = $owner->getAdministrator()->isAutomaticallyAdded();
									}
									break;
							}
							if($isAutomaticallyAdded) // if recipient is currently flagged as automatically added, continue to next comment
								continue; 
							$vars = array(	'recipientName' 	=> array(self::ADMIN_NOTIFICATION => 'Administrator', self::USER_NOTIFICATION => $recipientName),
											'commentAuthor' 	=> $commentAuthor,
											'message'			=> array(self::ADMIN_NOTIFICATION => $adminMessage, self::USER_NOTIFICATION => $message),
											'comment'			=> $comment,
											'link'				=> $link,
											'siteName' 			=> $siteName,
											'siteUrl'			=> $siteUrl);

							// add notification for this greeting
							$notification = new Notification;
							$notification->setSubject('New ' . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName) . ' greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : ucfirst($commentAuthor->getUserName())));
							$notification->setSender($domainConfig->getAdminEmail());
							$notification->setSenderName($domainConfig->getEmailName());
							$notification->setMessage(NotificationComposer::compose(array_merge($vars, array('mode' => self::USER_NOTIFICATION)), 'tpl.TravelerCommentNotificationTemplate.php'));
							$notification->addRecipient($recipient);
							$this->mNotifications[] = $notification;

							// create a notification for GoAbroad.net Administrator
							$adminNotification = new Notification;
							$adminNotification->setSubject('New ' . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName) . ' greeting from ' . ($commentAuthor instanceof AdminGroup ? 'the group ' . $commentAuthor->getName() : ucfirst($commentAuthor->getUserName())));
							$adminNotification->setSender($domainConfig->getAdminEmail());
							$adminNotification->setSenderName($domainConfig->getEmailName());
							$adminNotification->setMessage(NotificationComposer::compose(array_merge($vars, array('mode' => self::ADMIN_NOTIFICATION))));
							$adminNotification->addRecipient('admin@goabroad.net');
							$this->mNotifications[] = $adminNotification;
						} // end try block
						
						catch(exception $e){
							// intentionally left blank
						} // end catch block
						
					} // end comments loop
					
				}// end author loop
				
			}// end  domain loop
			
		} // end function createNotification
		
	}// end class 
?>