<?php
	require_once('Class.dbHandler.php');
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	
	class BufferedNotificationQueue implements NotificationQueue
	{
		
		private $mDb = null;
		public function __construct(){
			$this->mDb = new dbHandler();
		}
		
		public function getNotifications($startDate,$endDate){
			return array();
			/***
			$sql = 	'SELECT * FROM tblBufferedNotification';
			$rs = new iRecordset($this->mDb->execute($sql));
			$arNotifications = array();
			foreach($rs as $row){
				$rcptSql = 'SELECT * FROM tblBufferedNotificationRecipient WHERE bufferedNotificationID = '.GaString::makeSqlSafe($row['bufferedNotificationID']);
				$rsRcpt = new iRecordset($this->mDb->execute($rcptSql));
				if($rsRcpt->retrieveRecordCount()){
					$iNotification = new Notification();
					$iNotification->setSender($row['senderEmailAddress']);
					$iNotification->setSenderName($row['senderName']);
					$iNotification->setSubject($row['subject']);
					$iNotification->setMessage($row['message']);
					$iNotification->setAltMessage($row['altMessage']);
					$iNotification->setBufferedNotificationID($row['bufferedNotificationID']);
					foreach($rsRcpt as $rcptRow){
						$iNotification->addRecipient($rcptRow['emailAddress']);
					}
					$arNotifications[] = $iNotification;
				}
			}
			return $arNotifications;
			***/
		}
		
		public static function addNotification(Notification $notification){
			/***
			if(!$notification->isBuffered()){
				$db = new dbHandler();
				$sql1 =	'INSERT INTO tblBufferedNotification SET '.
						'`senderEmailAddress` 	= '.GaString::makeSqlSafe($notification->getSender()).', '.
						'`senderName`			= '.GaString::makeSqlSafe($notification->getSenderName()).', '.
						'`subject`				= '.GaString::makeSqlSafe($notification->getSubject()).', '.
						'`message`				= '.GaString::makeSqlSafe($notification->getMessage()).', '.
						'`altMessage`			= '.GaString::makeSqlSafe($notification->getAltMessage());
				$db->execute($sql1);
				$bufferedNotificationID = $db->getLastInsertedID();
				$recipients = $notification->getRecipients();
				foreach($recipients as $iRecipient){
					$sql2 = 'INSERT INTO tblBufferedNotificationRecipient SET '.
							'`bufferedNotificationID` = '.GaString::makeSqlSafe($bufferedNotificationID).', '.
							'`emailAddress` = '.GaString::makeSqlSafe($iRecipient);
					$db->execute($sql2);
				}
			}
			***/
		}
		
		public static function removeNotification(Notification $notification){
			/***
			$db = new dbHandler();
			if($notification->isBuffered()){
				$sql = 'DELETE FROM tblBufferedNotification WHERE bufferedNotificationID = '.GaString::makeSqlSafe($notification->getBufferedNotificationID());
				$db->execute($sql);
				$sql = 'DELETE FROM tblBufferedNotificationRecipient WHERE bufferedNotificationID = '.GaString::makeSqlSafe($notification->getBufferedNotificationID());
				$db->execute($sql);
			}
			***/
		}
		
	}
?>