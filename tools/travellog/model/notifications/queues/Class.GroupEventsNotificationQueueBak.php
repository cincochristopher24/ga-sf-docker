<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('Class.GaString.php');
	require_once('travellog/model/Class.SiteContext.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/Class.GroupEvent.php');
	require_once('travellog/model/Class.Traveler.php');
	
	require_once('travellog/model/Class.NotificationSetting.php');
	
	class GroupEventsNotificationQueueBak implements NotificationQueue, ClearableNotificationQueue{
		
		private $mNotifications = array();
		private $mReader		= NULL;
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = '(logView.`_EXECUTION_DATE_` >= ' . GaString::makeSqlSafe($startDate) . ' ' .
							 'AND logView.`_EXECUTION_DATE_` <= ' . GaString::makeSqlSafe($endDate) . ')';
			
			$sql = 	"SELECT *, `tblNotificationSetting`.`_NEW_messageIncluded`,`tblNotificationSetting`.`_NEW_notifyGroupMembers` " .
					"FROM `Travel_Logs`.`tblGroupEvent` logView LEFT JOIN " .
					"`Travel_Logs`.`tblNotificationSetting` ON " .
						"logView.`_NEW_groupEventID` = `tblNotificationSetting`.`_NEW_contextID` " .
						"AND `tblNotificationSetting`.`_NEW_context` = ".NotificationSetting::GROUP_EVENT_CONTEXT." ".
					'WHERE ' .
						'(logView.`_EVENT_` = 1 AND logView.`_COMMAND_` = "INSERT") '.
						'AND ' . $rangeCriteria . ' ' .
						'AND EXISTS (<$ ' .
							'SELECT `groupeventID` FROM `Travel_Logs`.`tblGroupEvent` ' .
							'WHERE logView.`_NEW_groupeventID` = `tblGroupEvent`.`groupeventID` '.
						'$>) ' .
						"AND logView.`_EVENT_` = 1 " .
						'AND `tblNotificationSetting`.`_NEW_notifyGroupMembers` = 1 '.	
					'ORDER BY logView.`_EXECUTION_DATE_` DESC';
			$rs = $this->mReader->getLogsBySql($sql);
			$rs->dump();
			$this->createNotification($this->groupData($rs));
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
		
		private function groupData($resource){
			$items = array();
			foreach($resource as $row){
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_DOER_ID_']]['time']))
					$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_DOER_ID_']]['time'] = $row['_EXECUTION_DATE_'];
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_DOER_ID_']]['events'][$row['_NEW_groupeventID']]))
					$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_DOER_ID_']]['events'][$row['_NEW_groupeventID']] = $row['_NEW_groupeventID'];
				 	
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();
				foreach($iGroupedData as $groupID => $eventDetails){
					try{
						$arrGrp = GroupFactory::instance()->create(array($groupID));
						$group	= $arrGrp[0];
						foreach($eventDetails as $doerID => $iEventDetails){
							$events = array();
							$doer = new Traveler($doerID);
							foreach($iEventDetails['events'] as $eventID){
								$events[] = new GroupEvent($eventID);
							}

							// add notifications for each member
							foreach($group->getMembers() as $member){
								if(!$member->isAutomaticallyAdded() && !$traveler->isSuspended() && !$traveler->isDeactivated()){
									$vars = array('events' => $events, 
												  'group' => $group, 
												  'recipient' => $member,
												  'doer' => $doer, 
												  'siteName' => $siteName, 
												  'siteUrl' => $siteUrl);
									$notification = new Notification();
									$notification->addRecipient($member->getTravelerProfile()->getEmail());
									$notification->setSubject('New ' . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName) . ' Group Event Update' . (count($events) > 1 ? 's' : '')   );
									$notification->setSender($domainConfig->getAdminEmail());
									$notification->setSenderName($domainConfig->getEmailName());
									$notification->setMessage(NotificationComposer::compose($vars, 'tpl.GroupEventsNotificationTemplate.php'));
									$this->mNotifications[] = $notification;
								}
							}
						}
					}
					catch(exception $e){
						
					}
				}
			}
		}
	}
?>
