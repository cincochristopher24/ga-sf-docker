<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/Class.Traveler.php');
	
	class GroupCancelInviteStaffNotificationQueue implements NotificationQueue, ClearableNotificationQueue{
		
		private $mNotifications = array();
		private $mReader = NULL;
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$sql = 	'SELECT * ' .
					'FROM `Travel_Logs`.`tblInviteList` logView ' .
					'WHERE ' .
						'logView.`_EVENT_` = 3 AND logView.`_COMMAND_` = "DELETE" ' .
						'AND (logView.`_EXECUTION_DATE_` >= ' . GaString::makeSqlSafe($startDate) . ' AND logView.`_EXECUTION_DATE_` <= ' . GaString::makeSqlSafe($endDate) . ') ' .
						'AND NOT EXISTS (<$ SELECT grouptofacilitatorID FROM `Travel_Logs`.`tblGrouptoFacilitator` ' .
						'WHERE logView.`_OLD_groupID` = `groupID` AND logView.`_OLD_travelerID` = `travelerID`$>) ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC';
			$rs = $this->mReader->getLogsBySql($sql);
			//$rs->dump(); exit;
			$this->createNotification($this->groupData($rs->retrieveData()));
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_array($resource)){
				try{
					$arrGrp = GroupFactory::instance()->create(array($row['_OLD_groupID']));
					$tempGroup = $arrGrp[0];
					if($row['_DOER_ID_'] == $tempGroup->getAdministratorID()){
						if(!isset($items[$row['_DOMAIN_NAME_']][$row['_OLD_groupID']][$row['_OLD_travelerID']]))
							$items[$row['_DOMAIN_NAME_']][$row['_OLD_groupID']][$row['_OLD_travelerID']] = $row['_EXECUTION_DATE_'];
					}
				}
				catch(exception $e){
					
				}
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();
				foreach($iGroupedData as $groupID => $invites){
					try{
						$arrGrp = GroupFactory::instance()->create(array($groupID));
						$group = $arrGrp[0];
						foreach($invites as $travelerID => $time){
							try{
								$traveler = new Traveler($travelerID);
							}
							catch(exception $e){
								throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR,$trace[0], '[CLASS]Traveler: ' . $e->getMessage());
							}
							if(!$traveler->isAutomaticallyAdded()){
								$vars = array(	'recipient' => $traveler->getUserName(),
												'group' 	=> $group,
												'siteName'	=> $siteName,
												'siteUrl'	=> $siteUrl,
												'time'		=> $time);
								$notification = new Notification();
								$notification->setSender($domainConfig->getAdminEmail());
								$notification->setSenderName($domainConfig->getEmailName());
								$notification->setSubject(GaString::makeSqlSafe($group->getName()) . ' Staff Invitation cancelled by group');
								$notification->setMessage(NotificationComposer::compose($vars, 'tpl.GroupCancelInviteStaffNotificationTemplate.php'));
								$notification->addRecipient($traveler->getTravelerProfile()->getEmail());
								$this->mNotifications[] = $notification;
							}
						}
					}
					catch(exception $e){
						
					}
				}
			}		
		}
	}
?>
