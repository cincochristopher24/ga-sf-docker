<?php
	
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('travellog/model/Class.Travel.php');
	
	class GroupCreateJournalNotificationQueue implements NotificationQueue {
		
		private $mNotifications = array();
		
		public function getNotifications($startDate, $endDate){
			
			$reader = new Reader;
			$this->mNotifications = array();
			$rs = $reader->getLogsBySql(
				'SELECT * FROM `Travel_Logs`.`tblTravel` as logView <$, `Travel_Logs`.`tblTravel`$> ' .
				'WHERE ' .
					'logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe($startDate) . ' AND ' . GaString::makeSqlSafe($endDate) . ' ' . 
					'AND logView.`_EVENT_` = 1 ' .
					'AND logView.`_NEW_travelID` = <$ `tblTravel`.`travelID`$> ' .
				'ORDER BY `_EXECUTION_DATE_` DESC '
			);
			$rs->dump();
			$this->createNotification($this->groupData($rs));
			return $this->mNotifications;
		}
		
		
		private function groupData($rs){
			$items = array();
			foreach($rs as $row){
				$items[$row['_DOMAIN_NAME_']][] = $row['_NEW_travelID'];
				$items[$row['_DOMAIN_NAME_']] = array_unique($items[$row['_DOMAIN_NAME_']]);
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			
			
			foreach($groupedData as $domainName => $journals ){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName	= $domainConfig->getEmailName();
				$siteUrl	= $domainConfig->getServerName();
				$sender 	= $domainConfig->getAdminEmail();
				$emailName	= $domainConfig->getEmailName();
				
				foreach($journals as $id){
					try{
						$journal = new Travel($id);
						$owner = $journal->getOwner();
						if(!$owner->isAdvisor()){
						
							$groups = $owner->getGroups();
							// send notification to each admin group administrator in which this owner is a member of
							foreach($groups as $iGroup){
								if(!$iGroup instanceof AdminGroup)
									continue;

								/** 
								 *	New Rule: Even if location is not a cobrand site as long as that group has a corresponding
								 *				cobrand site and recipient is admin of group, use cobrand
								 */
								// if group has a cobrand site use respective cobrand, else, use dotNet
								$cb = ( $cb	= Config::findByGroupID($iGroup->getGroupID())) ? $cb : Config::findByGroupID();
								if ( !is_null($cb) ){
									$siteUrl	= $cb->getServerName();
									$sender		= $cb->getAdminEmail();
									$emailName	= $cb->getEmailName();
									$siteName 	= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
								}

								// create notification only if journal has entry
								if( 0 < $journal->getCountEntries()){
									$vars = array(
										'group'		=> $iGroup,
										'recipient'	=> $iGroup->getName(),
										'travel'	=> $journal,
										'siteName'	=> $siteName,
										'siteUrl'	=> $siteUrl,
										'owner'		=> $owner
									);
									$administrator = $iGroup->getAdministrator();
									$notification = new Notification;
									$notification->addRecipient($administrator->getTravelerProfile()->getEmail());
									$notification->setMessage(NotificationComposer::compose($vars, 'tpl.GroupCreateJournalNotificationTemplate.php'));
									$notification->setSubject($owner->getUsername() . ' has created a new journal');
									$notification->setSender($sender);
									$notification->setSenderName($emailName);
									$this->mNotifications[] = $notification;
									
								}
							}
						}
					}
					catch(exception $e){}
				}
			}
			
			
		}

	}

?>