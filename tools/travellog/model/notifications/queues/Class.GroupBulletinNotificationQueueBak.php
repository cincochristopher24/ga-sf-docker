<?php
	
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	//require_once('travellog/model/notifications/types/Class.BulletinNotification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	//require_once('travellog/model/notifications/Class.NotificationSetting.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.SendableFactory.php');
	require_once('travellog/model/Class.BulletinMessage.php');
	require_once('travellog/model/Class.MessageSource.php');
	
	require_once('travellog/model/Class.NotificationSetting.php');
	
	class GroupBulletinNotificationQueueBak implements NotificationQueue, ClearableNotificationQueue{
		
		private $mNotifications = array();
		private $mReader = NULL;
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$this->query($startDate, $endDate);
			return $this->mNotifications;
		}
		
		public function clear(){
			NotificationSetting::deleteGroupBulletinNotificationSettings();
		}
		
		private function query($startDate, $endDate){
			/***
			$rangeCriteria = "(logView.`_EXECUTION_DATE_` >= " . GaString::makeSqlSafe($startDate) . " AND " .
							  "logView.`_EXECUTION_DATE_` <= " . GaString::makeSqlSafe($endDate) . ")";
			$sql = 	'SELECT logView.*, tblNotificationSetting.`_NEW_messageIncluded` ' .
					'FROM ' .
						'`Travel_Logs`.`tblMessageToAttribute` logView LEFT JOIN `Travel_Logs`.`tblNotificationSetting` ' .
						'ON logView.`_NEW_messageID` = `tblNotificationSetting`.`_NEW_contextID` ' .
					'WHERE ' .
						'`_NEW_messageID` IN ( <$ ' .
						'SELECT `messageID` FROM `Travel_Logs`.`tblMessages` WHERE logView.`_NEW_messageID` = `messageID` AND `discriminator` = 2$> ) ' . // choose bulletin messages
						'AND `_NEW_messageID` IN (<$ ' .
						'SELECT `messageID` FROM `Travel_Logs`.`tblMessageToAttribute`, `Travel_Logs`.`tblSendable` WHERE logView.`_NEW_messageID` = `messageID` AND `sendableID` = `recipientID` AND `sendableType` = 2 $>) ' . // choose those messages in group context
						'AND logView.`_EVENT_` = 1 AND ' . $rangeCriteria . ' ' .
						'AND EXISTS (<$ SELECT `attributeID` FROM `Travel_Logs`.`tblMessageToAttribute` WHERE logView.`_NEW_attributeID` = `attributeID` $>) ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC';
			***/
			
			$rangeCriteria = "(logView.`_EXECUTION_DATE_` >= " . GaString::makeSqlSafe($startDate) . " AND " .
							  "logView.`_EXECUTION_DATE_` <= " . GaString::makeSqlSafe($endDate) . ")";
			$sql = 	'SELECT logView.*, tblNotificationSetting.`_NEW_messageIncluded`, tblNotificationSetting.`_NEW_notifyGroupMembers`  ' .
					'FROM `Travel_Logs`.`tblMessageToAttribute` logView LEFT JOIN `Travel_Logs`.`tblNotificationSetting` ON ' .
						'logView.`_NEW_messageID` = `tblNotificationSetting`.`_NEW_contextID` ' .
						'AND `tblNotificationSetting`.`_NEW_context` = '.NotificationSetting::GROUP_BULLETIN_CONTEXT.' '.
						'AND `tblNotificationSetting`.`_NEW_notifyGroupMembers` = 1 '.
					'WHERE ' .
						'`_NEW_messageID` IN ( <$ ' .
						'SELECT `messageID` FROM `Travel_Logs`.`tblMessages` WHERE logView.`_NEW_messageID` = `messageID` AND `discriminator` = 2$> ) ' . // choose bulletin messages
						'AND `_NEW_messageID` IN (<$ ' .
						'SELECT `messageID` FROM `Travel_Logs`.`tblMessageToAttribute`, `Travel_Logs`.`tblSendable` WHERE logView.`_NEW_messageID` = `messageID` AND `sendableID` = `recipientID` AND `sendableType` = 2 $>) ' . // choose those messages in group context
						'AND logView.`_EVENT_` = 1 AND ' . $rangeCriteria . ' ' .
						'AND EXISTS (<$ SELECT `attributeID` FROM `Travel_Logs`.`tblMessageToAttribute` WHERE logView.`_NEW_attributeID` = `attributeID` $>) ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC';
			$rs = $this->mReader->getLogsBySql($sql);
			//$rs->dump(); exit;
			$this->createNotification($this->groupData($rs->retrieveData()));
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['time']))
					$items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['time'] = $row['_EXECUTION_DATE_'];
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['recipients'][$row['_NEW_recipientID']][$row['_NEW_attributeID']]))
					$items[$row['_DOMAIN_NAME_']][$row['_DOER_ID_']]['recipients'][$row['_NEW_recipientID']][$row['_NEW_attributeID']] = array('attributeID' => $row['_NEW_attributeID'], 'messageIncluded' => $row['_NEW_messageIncluded']);
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();
				foreach($iGroupedData as $doer => $details){
					foreach($details['recipients'] as $recipient => $attributes){
						try{
							$obj = SendableFactory::Instance()->Create($recipient);
							if('Group' == get_parent_class($obj)){
								$messages = array();
								$source = NULL;
								//TODO: format time with necessary format
								$time = date('F d, Y g:i:s A',strtotime($details['time']));
								foreach($attributes as $iAttributes){
									try{
										$msg = new BulletinMessage($iAttributes['attributeID']);	
									}
									catch(exception $e){
										throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR,$trace[0],'[CLASS]BulletinMessage: ' . $e->getMessage());
									}					
									$source = new MessageSource($msg);
									//$temp = (!isset($temp) ? (($iAttributes['messageIncluded'] == 1) ? TRUE:FALSE) : !$temp);
									$messages[] = array('message' => $msg, 'isMessageIncluded' => ($iAttributes['messageIncluded']==1) ? TRUE : FALSE);
								}

								// add notification for each member of the group
								foreach($obj->getMembers() as $traveler){
									if($traveler->getTravelerID() != $doer AND $traveler->getTravelerID() != $obj->getAdministratorID() AND !$traveler->isSuspended() AND !$traveler->isDeactivated()){
										if(!$traveler->isAutomaticallyAdded()){
											$vars = array(	'recipient' => ucfirst($traveler->getUsername()),
															'messages'	=> $messages, 
															'source' 	=> $source,
															'group' 	=> $obj,
															'siteName' 	=> $siteName,
															'siteUrl' 	=> $siteUrl, 
															'time' 		=> $time
													);
											//$notification = new BulletinNotification();
											$notification = new Notification();
											$notification->setSubject("New " . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName) . " Bulletin " . (count($messages) > 1 ? "Messages":"Message") . " from " . $source->getName() . " -- " . $time);
											$notification->setSender($domainConfig->getAdminEmail());
											$notification->setSenderName($domainConfig->getEmailName());
											$notification->setMessage(NotificationComposer::compose($vars,'tpl.GroupBulletinNotificationTemplate.php'));
											$notification->addRecipient($traveler->getTravelerProfile()->getEmail());
											$this->mNotifications[] = $notification;
										}
									}
								}

								// add notification to group admin if admin is not the author
								$administrator = $obj->getAdministrator();
								if($doer != $administrator->getTravelerID()){

									$vars = array(	'recipient' => ucfirst($administrator->getUsername()),
													'messages'	=> $messages, 
													'source' 	=> $source,
													'group' 	=> $obj,
													'siteName' 	=> $siteName,
													'siteUrl' 	=> $siteUrl, 
													'time' 		=> $time
											);
									$notification = new Notification();
									$notification->setSubject("New " . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName) . " Bulletin " . (count($messages) > 1 ? "Messages":"Message") . " from " . $source->getName() . " -- " . $time);
									$notification->setSender($domainConfig->getAdminEmail());
									$notification->setSenderName($domainConfig->getEmailName());
									$notification->setMessage(NotificationComposer::compose($vars,'tpl.GroupBulletinNotificationTemplate.php'));
									$notification->addRecipient($administrator->getTravelerProfile()->getEmail());
									$this->mNotifications[] = $notification;
								}
							}
						}
						catch(exception $e){
							
						}
					}
				}
			}
		}
	}
?>