<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	require_once('Class.Crypt.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	
	class GroupEmailInvitationNotificationQueuebak implements NotificationQueue, ClearableNotificationQueue{
			
		private $mNotifications = array();
		private $mReader = NULL;
		
		public function __construct(){
			$this->mReader = new Reader();
		}	
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = '(logView.`_EXECUTION_DATE_` >= ' . GaString::makeSqlSafe($startDate) . ' AND ' .
							  'logView.`_EXECUTION_DATE_` <= ' . GaString::makeSqlSafe($endDate) .')';
			$sql = 	'SELECT * ' .
					'FROM `Travel_Logs`.`tblEmailInviteList` logView ' .
					'WHERE ' .
						'logView.`_EVENT_` = 1 ' .
						'AND ' . $rangeCriteria . ' ' .
						'AND EXISTS(<$ ' .
						'SELECT groupID ' .
						'FROM `Travel_Logs`.`tblEmailInviteList` ' .
						'WHERE ' .
							'logView.`_NEW_groupID` = `tblEmailInviteList`.`groupID` ' .
							'AND logView.`_NEW_email` = `tblEmailInviteList`.`email`$>) ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC';
			$rs = $this->mReader->getLogsBySql($sql);
			//$rs->dump(); 
			$this->createNotification($this->groupData($rs->retrieveData()));
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_array($resource)){
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_NEW_email']]))
					$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_NEW_email']] = $row['_EXECUTION_DATE_'];
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$crypt = new Crypt();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();
				//$useEncrypted = ($siteUrl == 'www.goabroad.net' OR $siteUrl == 'goabroad.net.local' OR $siteUrl == 'dev.goabroad.net');
				foreach($iGroupedData as $groupID => $emailInvites){
					try{
						$group = GroupFactory::instance()->create($groupID);
						foreach($emailInvites as $email => $time){
							$time = date('F d, Y g:i:s A',strtotime($time));
							$personalMessage = $group->getEmailInvitationMessagePerEmail($email);
							$emailAttribute = implode(' ', $group->getEmailInviteAttribute($email));
							$vars = array(	'group' 	=> $group,
											'recipient'	=> ('' == trim($emailAttribute) ? $email : $emailAttribute),
											'time'		=> $time,
											'siteName'	=> $siteName,
											'siteUrl'	=> $siteUrl,
											//'urlParam'	=> ($useEncrypted) ? 'traveler' : urlencode($crypt->encrypt('traveler&email=' . $email))
											'urlParam'	=> urlencode($crypt->encrypt('traveler&email=' . $email)),
											'personalMessage' => $personalMessage
										);
							$notification = new Notification();
							$notification->setSender($domainConfig->getAdminEmail());
							$notification->setSenderName($domainConfig->getEmailName());
							$notification->setSubject($group->getName() . ' invites you to join ' . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName));
							$notification->setMessage(NotificationComposer::compose($vars, 'tpl.GroupEmailInvitationNotificationTemplate.php'));
							$notification->addRecipient($email);
							$this->mNotifications[] = $notification;
						}
					}
					catch(exception $e){
						
					}
				}
			}
		}
	}
?>
