<?php

	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.Traveler.php');
	require_once('travellog/model/Class.GroupFactory.php');
	
	class GroupAssignStaffNotificationQueue implements NotificationQueue, ClearableNotificationQueue{
		
		private $mReader 		= NULL;
		private $mNotifications = array();
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = "(logView.`_EXECUTION_DATE_` >= " . GaString::makeSqlSafe($startDate) 	. " AND " .
							  "logView.`_EXECUTION_DATE_` <= " . GaString::makeSqlSafe($endDate) 	. ")";	
			$sql = 	"SELECT * " .
					"FROM `Travel_Logs`.`tblGrouptoFacilitator` logView " .
					"WHERE " .
						"logView.`_EVENT_` = 1 AND $rangeCriteria " .
						"AND EXISTS ( <$ SELECT grouptofacilitatorID FROM `Travel_Logs`.`tblGrouptoFacilitator` WHERE logView.`_NEW_grouptofacilitatorID` = grouptofacilitatorID AND `type` = 1 AND `status` = 1$>) " .
						"AND logView._NEW_type = 1 AND logView.`_NEW_status` = 1 " .
						'AND _NEW_travelerID NOT IN (<$ SELECT `travelerID` FROM `Travel_Logs`.`tblNewlyCreatedStaff`$>) ' .
					"ORDER BY logView.`_EXECUTION_DATE_` DESC";
			$rs = $this->mReader->getLogsBySql($sql);
			$rs->dump(); 
			$this->createNotification($this->groupData($rs->retrieveData()));
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				$temp = GroupFactory::instance()->create($row['_NEW_groupID']);
				$parentGroup = ($temp instanceof AdminGroup) ? (is_null($temp->getParent()) ? $temp : $temp->getParent()) : $temp;
				if(!isset($items[$row['_DOMAIN_NAME_']][$parentGroup->getGroupID()][$row['_NEW_travelerID']]['time']))
					$items[$row['_DOMAIN_NAME_']][$parentGroup->getGroupID()][$row['_NEW_travelerID']]['time'] = $row['_EXECUTION_DATE_'];
				if(!isset($items[$row['_DOMAIN_NAME_']][$parentGroup->getGroupID()][$row['_NEW_travelerID']]['groups'][$row['_NEW_groupID']]))
					$items[$row['_DOMAIN_NAME_']][$parentGroup->getGroupID()][$row['_NEW_travelerID']]['groups'][$row['_NEW_groupID']] = $row['_NEW_groupID'];
				//if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_NEW_travelerID']]))
				//	$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_NEW_travelerID']] = $row['_EXECUTION_DATE_'];
			}
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();
				foreach($iGroupedData as $parentGroupID => $travelers){
					//try block for non-existent groups
					try{
						$parentGroup = GroupFactory::instance()->create($parentGroupID);
						foreach($travelers as $travelerID => $iTraveler){
							try{
								$traveler = new Traveler($travelerID);
							}
							catch(exception $e){
								throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR,$trace[0], '[CLASS]Traveler: ' . $e->getMessage());
							}
							if(!$traveler->isAutomaticallyAdded() && !$traveler->isSuspended() && !$traveler->isDeactivated()){
								$time = date('F d, Y g:i:s A',strtotime($iTraveler['time']));
								$groups = array();
								foreach($iTraveler['groups'] as $groupID){
									$groups[] = GroupFactory::instance()->create($groupID);
								}
								$vars = array( 	'recipient' => $traveler->getUserName(),
												'subgroups'	=> $groups,
												'parent'	=> $parentGroup,
												'siteName'	=> $siteName,
												'siteUrl'	=> $siteUrl,
												'time'		=> $time);
								$notification = new Notification();
								$notification->setSender($domainConfig->getAdminEmail());
								$notification->setSenderName($domainConfig->getEmailName());
								$notification->setSubject($parentGroup->getName() . ' assigned you as a staff on your group!');
								$notification->setMessage(NotificationComposer::compose($vars, 'tpl.GroupAssignStaffNotificationTemplate.php'));
								$notification->addRecipient($traveler->getTravelerProfile()->getEmail());
								$this->mNotifications[] = $notification;
							}
						}
					}
					catch(exception $e){}
				}
			}
		}
	}
?>
