<?php
	require_once('travellog/model/notifications/interfaces/Interface.NotificationQueue.php');
	require_once('travellog/model/notifications/interfaces/Interface.ClearableNotificationQueue.php');
	require_once('travellog/model/notifications/Class.Notification.php');
	require_once('travellog/model/notifications/Class.NotificationComposer.php');
	require_once('travellog/model/notifications/Class.NotificationException.php');
	
	require_once('gaLogs/Reader/Class.Reader.php');
	require_once('travellog/model/Class.SiteContext.php');
	require_once('Class.GaString.php');
	
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/Class.TravelerProfile.php');
	
	class GroupInvitationNotificationQueueBak implements NotificationQueue, ClearableNotificationQueue{
		
		private $mReader 		= NULL;
		private $mNotifications = array();
		
		public function __construct(){
			$this->mReader = new Reader();
		}
		
		public function getNotifications($startDate, $endDate){
			$rangeCriteria = "(logView.`_EXECUTION_DATE_` >= " . GaString::makeSqlSafe($startDate) . " AND " .
							  "logView.`_EXECUTION_DATE_` <= " . GaString::makeSqlSafe($endDate) . ")";
			
			
			$sql = 	'SELECT * ' .
					'FROM ' .
						'`Travel_Logs`.`tblInviteList` logView ' .
					'WHERE ' .
						'logView.`_EVENT_` = 1 AND ' . $rangeCriteria . ' ' .
						'AND EXISTS (<$ ' .
						'SELECT `groupID` FROM `Travel_Logs`.`tblInviteList` WHERE ' .
						'logView.`_NEW_groupID` = `groupID` AND ' .
						'logView.`_NEW_travelerID` = `travelerID` ' .
						'AND status = 0 $>) ' . 
					'ORDER BY logView._EXECUTION_DATE_ DESC '; 
			$rs = $this->mReader->getLogsBySql($sql);
			//$rs->dump(); exit;
			$this->createNotification($this->groupData($rs->retrieveData()));
			return $this->mNotifications;
		}
		
		public function clear(){
			
		}
		
		private function groupData($resource){
			$items = array();
			while($row = mysql_fetch_assoc($resource)){
				if(!isset($items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_NEW_travelerID']]))
					$items[$row['_DOMAIN_NAME_']][$row['_NEW_groupID']][$row['_NEW_travelerID']] = $row['_EXECUTION_DATE_']; // save only the latest time of invite from a group to a certain traveler
			}
			//print_r($items); exit;
			return $items;
		}
		
		private function createNotification($groupedData){
			$trace = debug_backtrace();
			foreach($groupedData as $domainName => $iGroupedData){
				//$domainConfig = new Con_fig($domainName);
				$domainConfig = SiteContext::getInstanceByDomainName($domainName);
				$siteName = $domainConfig->getEmailName();
				$siteUrl = $domainConfig->getServerName();
				foreach($iGroupedData as $groupID => $invites){
					try{
						$group = GroupFactory::instance()->create($groupID);

						foreach($invites as $travelerID =>$time){
							//TODO: format time to necessary format
							$time = date('F d, Y g:i:s A',strtotime($time));
							try{
								$profile = new TravelerProfile($travelerID);
								$traveler = new Traveler($travelerID);
							}
							catch(exception $e){
								throw new NotificationException(NotificationException::OBJECT_INSTANTIATION_ERROR,$trace[0], '[CLASS]Traveler Profile: ' . $e->getMessage());
							}
							if(!$traveler->isAutomaticallyAdded() && !$traveler->isSuspended() && !$traveler->isDeactivated()){
								$vars = array(	'recipient' => $profile->getUserName(),
												'group' => $group,
												'time' => $time,
												'siteName' => $siteName,
												'siteUrl' => $siteUrl
											);
								$notification = new Notification();
								// if CIS
								if ( (241 == $group->getID()) || (241 == $group->getParentID()) )
									$notification->setMessage(NotificationComposer::compose($vars,'tpl.CISGroupInvitationNotificationTemplate.php'));
								else
									$notification->setMessage(NotificationComposer::compose($vars,'tpl.GroupInvitationNotificationTemplate.php'));
								
								$notification->setSubject("Invitation from group " . $group->getName());
								$notification->setSender($domainConfig->getAdminEmail());
								$notification->setSenderName($domainConfig->getEmailName());
								$notification->addRecipient($profile->getEmail());
								$this->mNotifications[] = $notification;
							}
						}
					}
					catch(exception $e){
						
					}
				}
			}
		}
	}
?>
