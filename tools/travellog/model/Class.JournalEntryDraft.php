<?php
	/**
	 * @(#) Class.JournalEntryDraft.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 24, 2009
	 */
	
	require_once('travellog/model/base/Class.BaseJournalEntryDraft.php');
	
	/**
	 * Model class for journal entry draft.
	 */
	class JournalEntryDraft extends BaseJournalEntryDraft {
		
	}