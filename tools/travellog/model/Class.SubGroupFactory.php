<?php
  /**
   * @(#) Class.SubGroupFactory.php
   * 
   * Creates subgroups of a given group. Follows the singleton pattern.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 12 9, 08
   */
   
  require_once "Class.dbHandler.php";
  require_once "Class.SqlResourceIterator.php";
  require_once "travellog/model/Class.SqlFields.php";
  require_once "travellog/model/Class.FilterCriteria2.php";
  require_once "travellog/model/Class.RowsLimit.php";
  require_once "travellog/helper/Class.SqlUtil.php";
  require_once "travellog/model/Class.AdminGroup.php";
   
  class SubGroupFactory {
  	private static $instances = array();
  	private $mParentID = 0;
  	private $mParent = null;
  	
  	/**
  	 * Creates an instance of this class
  	 * 
  	 * @param integer|AdminGroup $param the parent group ID|the parent group itself
  	 * @return SubGroupFactory
  	 */
  	static function instance($param){
  		$id = ($param instanceof AdminGroup) ? $param->getGroupID() : $param;
  		
  		if (!isset(self::$instances[$id])) {
  			self::$instances[$id] = new SubGroupFactory($param);
  		}
  		
  		return self::$instances[$id];
  	}
  	
  	/**
  	 * constructor
  	 * 
  	 * @param integer|AdminGroup $param The parent group ID|The parent group itself. 
  	 */
  	private function __construct($param){
  		$id = ($param instanceof AdminGroup) ? $param->getGroupID() : $param;
  		$this->mParent = ($param instanceof AdminGroup) ? $param : null;
  		$this->mParentID = $id;
  	}
  	
  	/**
  	 * Creates the subgroups of the given group.
  	 * How to use:
     *   $handler = new dbHandler();
     *
     *   // fields
     *   $fields = new SqlFields();
     *   $fields->addField("groupID");
     *   $fields->addField("name");
     *   $fields->addField("featuredsubgrouprank");
     *   $fields->addField("subgrouprank");
     *   $fields->addField("administrator");
     *   $fields->addField("groupaccess");
     *   $fields->addField("discriminator");
     *   $fields->addField("photoID"); 
     *   // end of fields
     *
     *   // filters
     *   $cnd = new Condition();
     *   $cnd->setAttributeName('name');
     *   $cnd->setOperation(FilterOp::$LIKE);
     *   $cnd->setValue("%$searchKey%");
     *   $cnd1 = new Condition();
     *   $cnd1->setAttributeName('groupID');
     *   $cnd1->setOperation(FilterOp::$NOT_IN);
     *   $cnd1->setValue("SELECT subgroupID FROM " .
     *     " tblCustomizedGroupSection AS a, " .
     *     " tblCustomizedGroupSectiontoSubGroups AS b " .
     *     " WHERE a.customizedGroupSectionID = b.customizedGroupSectionID " .
     *     " AND a.advisorGroupID = " . $handler->makeSqlSafeString($this->mGroupID));
     *
     *   $filter = new FilterCriteria2();
     *   $filter->addCondition($cnd);
     *   $filter->addBooleanOp("AND");
     *   $filter->addCondition($cnd1);
     *   $filter->addBooleanOp("AND");
     *   // end of filters
		 *
		 *   // filter the group access
		 *   $grpAccessList = "1,2,3";
		 *
		 *   $cnd2 = new Condition();
     *   $cnd2->setAttributeName('groupaccess');
     *   $cnd2->setOperation(FilterOp::$IN);
     *   $cnd2->setValue($grpAccessList);
     *
     *   $filter->addCondition($cnd2);
     *   $filter->addBooleanOp("AND");			
		 *   // end of filtering group access
		 *
		 *   // order
		 *   $cnd1 = new Condition();
     *   $cnd1->setAttributeName('subgrouprank');
     *   $cnd1->setOperation(FilterOp::$ORDER_BY);
		 *   $cnd2 = new Condition();
     *   $cnd2->setAttributeName('datecreated');
     *   $cnd2->setOperation(FilterOp::$ORDER_BY_DESC);
     *
     *   $order = new FilterCriteria2();
     *   $order->addCondition($cnd1);
     *   $order->addCondition($cnd2);
     *   // end of order
		 *
		 *   $arrSubGroupsBySection = SubGroupFactory::instance($this->mGroupID)->getSubGroups($fields, $limit, $order, $filter);
		 *
  	 * 
  	 * @param SqlFields $fields the fields on tblGroup and tables passed.
  	 * @param SqlFields $fields the tables where the the data will also be get.
  	 * @param RowsLimit $limit
  	 * @param FilterCriteria2 $order
  	 * @param FilterCriteria2 $filter
  	 * @see SqlFields
  	 * @see RowsLimit
  	 * @see FilterCriteria2
  	 * @return array Array of AdminGroup object
  	 */
  	public function getSubGroups($fields = null, $limit = NULL, $order = NULL, $filter = NULL, $tables = NULL){
  		$groups = array();
  		$handler = new dbHandler();
  		
			// get the alias of tblGroup if there are any
  		$tblGroup_alias = "`tblGroup`";
  		if (!is_null($tables)) {
  			$temp_tables = $tables->getAllSqlFields();
  		  foreach($temp_tables as $table) {
  			  if ("tblGroup" == trim($table->getTableName())) {
  			  	if ("" != trim($table->getAlias())) {
  			  		$tblGroup_alias = $table->getAlias();
  			  	}
  			  	break;
  			  }
  		  }
  		}
  		// end of getting the alias
			
  		$sql = "SELECT ";
  		$sql .= (is_null($fields)) ? "*" : SqlUtil::createFieldClause($fields)." ";
  		$sql .= (is_null($tables)) ? "FROM tblGroup " : " FROM ".SqlUtil::createTableClause($tables)." ";
  		$sql .= "WHERE ";
      $sql .= $tblGroup_alias.".`parentID` = ".$handler->makeSqlSafeString($this->mParentID)." ";
  		$sql .= (is_null($filter)) ? "" : SqlUtil::createConditionClause($filter)." ";
  		$sql .= (is_null($order)) ? "" : SqlUtil::createOrderByClause($order)." "; 
  		$sql .= (is_null($limit)) ? "" : SqlUtil::createLimitClause($limit)." ";
  		
  		try {
  		  $rs = new iRecordset($handler->execute($sql));
			
			  while (!$rs->EOF()) {
          $grp = new AdminGroup();
          $grp->setAdminGroupData($rs->current());
          $groups[] = $grp;
          
          try {
            $rs->next();
          }
          catch (exception $ex) {
            echo $ex->getMessage();
				  }
        }
  		}
  		catch(exception $ex){}
  		
  		return $groups;
  	}
  	
  	/**
  	 * Counts the subgroups of the given group.
  	 * For example:
  	 *   $cnd = new Condition();
  	 *   $cnd->setAttributeName('name');
  	 *   $cnd->setOperation(FilterOp::$LIKE);
  	 *   $cnd->setValue('%group%');
  	 *   $filter = new FilterCriteria2();
  	 *   $filter->addCondition($cnd);
  	 *   $filter->addBooleanOp("AND");
  	 *   SubGroupFactory::instance(4)->getSubGroupsCount($filter);
  	 * 
  	 * @param FilterCriteria2 $filter
  	 * @return integer
  	 */
  	public function getSubGroupsCount($filter = NULL, $tables = NULL){
  		$handler = new dbHandler();
  		
  		// get the alias of tblGroup if there are any
  		$tblGroup_alias = "`tblGroup`";
  		if (!is_null($tables)) {
  			$temp_tables = $tables->getAllSqlFields();
  		  foreach($temp_tables as $table) {
  			  if ("tblGroup" == trim($table->getTableName())) {
  			  	if ("" != trim($table->getAlias())) {
  			  		$tblGroup_alias = $table->getAlias();
  			  	}
  			  	break;
  			  }
  		  }
  		}
  		// end of getting the alias
  		
  		$sql = "SELECT count($tblGroup_alias.`groupID`) as count ";
  		$sql .= (is_null($tables)) ? "FROM tblGroup " : " FROM ".SqlUtil::createTableClause($tables)." ";
  		$sql .= "WHERE ";
      $sql .= $tblGroup_alias.".`parentID` = ".$handler->makeSqlSafeString($this->mParentID)." ";
  		$sql .= (is_null($filter)) ? "" : SqlUtil::createConditionClause($filter)." ";
  		
  		$rs = new iRecordset($handler->execute($sql));
  		$record = $rs->current();
  		
  		return $record['count']; 
  	}
  	
  	/**
  	 * Retrieves subgroups that are active, inactive or both and filtered by categories or not at all.
  	 * 
  	 * @param string $section The flag whether it is active, inactive or both. For value equal to null it means both.
  	 * @param string $category If uncategorized, subgroups that are uncategorized will be get, else if, all we get all
  	 *                         subgroups, else we convert it to SubGroupCategory class.
  	 *                           
  	 * @return array Returns array of AdminGroup.
  	 */
  	function retrieveFilteredSubGroups($section = null, $category = "all"){
  		
  	}
  	
  	/**
  	 * Returns the IDs of subgroups that has no entry in tblGroupToSubGroupCategory.
  	 * 
  	 * @return array Returns the IDs of subgroups that has no entry in tblGroupToSubGroupCategory.
  	 */
  	function getSubGroupIDsWithoutCategoryInfo(){
  		try {
	  		$groups = array();
	  		$handler = new dbHandler();
	  		
	  		$sql = "SELECT groupID " .
			    " FROM tblGroup ".
			    " WHERE parentID = ".$handler->makeSqlSafeString($this->mParentID).
          " AND groupID " .
			    " NOT IN (" .
			    "   SELECT a.groupID " .
			    "   FROM tblGroupToSubGroupCategory as a, tblSubGroupCategory as b " .
			    "   WHERE a.categoryID = b.categoryID AND b.parentID = ".$handler->makeSqlSafeString($this->mParentID).
			    " ) ";
	  		
  		  $it = new SqlResourceIterator($handler->execute($sql));
			
			  while ($it->hasNext()) {
			  	$it->next();
          $groups[] = $it->groupID();
        }
  		}
  		catch(exception $ex){}
  		
  		return $groups;		
  	}
  }
