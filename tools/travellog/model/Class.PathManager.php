<?php
/*
 * Created by : Joel C. LLano
 */

	
 	class PathManager{
 		
 		public static $temporarypath = "users/";
 		
		private $objcontext;
		private $event;
		
		private $path_username;
		private $datejoined;
				
		function __autoload($class_name) {
  		 		require_once('Class.'. $class_name . '.php');
		}
		
		function __construct($objcontext,$event=NULL){
				$this->objcontext = $objcontext;
				$this->event = $event;
				
				switch(get_class($objcontext)){
					case "TravelerPoster":
					case "TravelerProfile":
							$travelerID = $this->objcontext->getTravelerID();
							break;					
					case "TravelLog":					
							$travelerID = $this->objcontext->getTravelerID();				
							break;					
					case "Resume":					
							$travelerID = $this->objcontext->gettravelerID();
							break;
					case "SubGroup":					
					case "AdminGroup":	
					case "AdvisorPoster":				
							$travelerID = $this->objcontext->getAdministrator()->gettravelerID();
							break;
					case "FunGroup":					
							$travelerID = $this->objcontext->getAdministrator()->gettravelerID();							
							break;	
					case "PhotoAlbum":					
							$travelerID = $this->objcontext->getCreator();
							break;					
					case "Program":						
							$group		 = new AdminGroup($this->objcontext->getGroupID());						
							$travelerID = $group->getAdministrator()->gettravelerID();
							break;
					case "Group":					
							$travelerID = $this->objcontext->getAdministrator()->getTravelerID();							
							break;
					case "Article":
							$travelerID = $this->objcontext->getAuthorID();
							break;	
					case "GanetGroupTemplate":
							$group 		= new AdminGroup($this->objcontext->getParentGroupID());
							$travelerID = $group->getAdministrator()->getTravelerID();
							break;							
				}	
				
				$traveler = ("TravelerProfile" == get_class($objcontext)) ? $objcontext : new TravelerProfile($travelerID);	
				//$this->path_username 	= $traveler->getUserName();	
				if( isset($_SERVER['HTTP_HOST']) && ($_SERVER['HTTP_HOST'] == "cis.dev.goabroad.net" || $_SERVER['HTTP_HOST'] == "test.goabroad.net"  || $_SERVER['HTTP_HOST'] == "ucboulder.dev.goabroad.net")){
					$this->path_username 	= $traveler->getUserName();	
				}
				else{
					$this->path_username = $traveler->getTravelerID();
				}
				$this->datejoined 			=getdate(strtotime($traveler->getDateJoined()));
				
		}
		
		
		
		function CreatePath(){								
							
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/travellog/"); 
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/profile/"); 
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/resume/"); 
						
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/program/");
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/photoalbum/");
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/fungroup/");
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/group/");
							
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/resourcefiles/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/resume/");
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/resourcefiles/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/admingroup/");
							
							$this->makepath($_SERVER['DOCUMENT_ROOT']."/"."users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username."/article/");
							
							
		}

		
		
		function GetPathrelative(){ 			
					
				
 				switch (get_class($this->objcontext)){
			 				case "TravelerPoster":
			 				case "TravelerProfile":			 						
 									$uploaddir 		=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;																
				 					return $uploaddir."/profile/"; 
				 					break;			 										
			 				case "TravelLog":
			 						$uploaddir 		=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;								 				 					
				 					return $uploaddir."/travellog/";
									break;							
							case "Program":
									$uploaddir 		=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;										
									return $uploaddir."/program/";
									break;								
							case "Resume":									
									if($this->event == 'image'){
										$uploaddir 		=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;
										$uploaddir 		= $uploaddir."/resume/"; 
				 					}else{
				 						$uploaddir 		=	"users/resourcefiles/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;
										$uploaddir 		= $uploaddir."/resume/"; 
									}				 					
				 					return $uploaddir;
									break;							
							case "PhotoAlbum":
									$uploaddir 		=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;				
				 					return $uploaddir."/photoalbum/"; 
									break;									
							case "FunGroup":
									$uploaddir 		=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;							
									//$uploaddir 			= $uploaddir."/fungroup/"; 
									$uploaddir 			= $uploaddir."/group/";
				 					return $uploaddir;
									break;	
							
							case "Group":
									$uploaddir 		=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;							
									$uploaddir 			= $uploaddir."/group/"; 			 					
				 					return $uploaddir;
									break;
							case "AdminGroup":
							case "SubGroup":
							case "AdvisorPoster":
									/**$uploaddir 		=	"users/resourcefiles/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;														 					
				 					return $uploaddir."/admingroup/"; 
									*/
									if($this->event == 'image'){
										$uploaddir 		=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;							
										$uploaddir 			= $uploaddir."/group/";
				 					}else{
				 						$uploaddir 		=	"users/resourcefiles/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;
										$uploaddir 		= $uploaddir."/admingroup/"; 
									}
									return $uploaddir;
									break;
							case "Article":
			 						$uploaddir 			=	"users/photos/".$this->datejoined['year']."/".$this->datejoined['month']."/".$this->path_username;						 				 					
				 					return $uploaddir."/article/";
									break;	
							case "GanetGroupTemplate":
									$uploaddir 			=	"users/resourcefiles/templates/group/".$this->objcontext->getParentGroup()->getGroupID()."/";
									return $uploaddir; 	
									break;						
							default:
								return NULL;
 				}
 				
 		}
		
		function getAbsolutePath(){
			return $_SERVER['DOCUMENT_ROOT']."/".$this->GetPathrelative();
		}
		
		function makepath($path) {
				
				$dirs=array();
				$path	=	preg_replace('/(\/){2,}|(\\\){1,}/','/',$path); 
				$dirs	=	explode("/",$path);
				$path	=	"";
				
				foreach ($dirs as $element){
					   $path.=$element."/";
					   if(!is_dir($path))
						 {
							 if(!mkdir($path)){ 
							 		echo "something was wrong at : ".$path; return 0;
							 }
						 }         
				}				
		}
		
		
		
 	}
?>