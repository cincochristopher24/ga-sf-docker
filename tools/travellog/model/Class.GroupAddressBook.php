<?php
	require_once('travellog/model/Class.AddressBook.php');
	require_once('travellog/model/Class.GroupFactory.php');
	class GroupAddressBook extends AddressBook {
		private $mOwnergGroupID	= 0;
		public function __construct($ownerGroupID){
			$this->mOwnerGroupID = $ownerGroupID;	
		}
		public function getAddressBookEntries(){
			return AddressBookEntry::getAddressBookEntriesByOwnerGroupID($this->mOwnerGroupID);
		}
	}
?>