<?php
/**
 * @author Aldwin S. Sabornido
 */
require_once('Class.Connection.php');
require_once('Class.Recordset.php'); 
class BrowserCache{
	
	private static $instance = null; 
	
	const JOURNALS  = 1;
	
	const TRAVELERS = 2;
	
	const ENTRY     = 3;
	
	
	private 
	
	$mTimestamp = NULL,
	
	$mModified  = true,
	
	$mKey       = NULL;
	 
	
	private function __contruct(){
	}
	
	static function getInstance(){
		if( self::$instance == null ) self::$instance = new BrowserCache; 
		return self::$instance; 
	}
	
	function Get(){
		$conn = new Connection;
		$rs   = new Recordset($conn);
		$sql  = sprintf
					(
						"SELECT `timestamp` FROM tblBrowserCache WHERE `key` = '%s'"
						,$this->getHash($this->mKey)
					);
		$rs->Execute($sql);
		if( $rs->Recordcount() ){
			$this->mTimestamp = $rs->Result(0, 'timestamp');	
		}
	}
	
	function create(){
		$conn             = new Connection;
		$rs               = new Recordset($conn);
		$this->mTimestamp = time();
		
		$sql  = sprintf
				(
					"INSERT INTO tblBrowserCache (`key`, `timestamp`) VALUES ('%s', %d)"
					,$this->getHash($this->mKey)
					,$this->mTimestamp    
				);
		$rs->Execute($sql);
	}
	
	function checkKey(){
		$conn = new Connection;
		$rs   = new Recordset($conn);
		$sql  = sprintf
					(
						"SELECT `timestamp` FROM tblBrowserCache WHERE `key` = '%s'"
						,$this->getHash($this->mKey)
					);
					
		$rs->Execute($sql);
		if( $rs->Recordcount() == 0 ){
			$this->create();	
			return false;
		}
		else{ 
			$this->Get();
			return true;
		}
	}
	
	function setKey($key){
		$this->mKeys = $key;
	}
	
	function setModified($modified){
		$this->mModified = $modified;
	}
	function getModified(){
		return $this->mModified;
	}
	
	function setTimestamp($timestamp){
		$this->mTimestamp = $timestamp;
	}
	function getTimestamp(){
		return $this->mTimestamp;
	}
	
	function createHeader(){
	    $this->checkKey();
	    $lastModifiedGMT = gmdate('D, d M Y H:i:s', $this->mTimestamp).' GMT';
	    $etag            = $this->getHash($this->mKey).'_'.$this->mTimestamp;

	    if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
	    	if (strpos($_SERVER['HTTP_IF_NONE_MATCH'], $etag) !== false) {
	        	header("Last-Modified: $lastModifiedGMT", true, 304);
	        	exit;
	      	}
	    }

   	 	header('ETag: "'.$etag.'"');

	    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
	    	if ($this->mTimestamp <= strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
	        	header("Last-Modified: $lastModifiedGMT", true, 304);
	        	exit;
	      	}
	    }
    	header("Last-Modified: $lastModifiedGMT");
	}
	
	private function getHash() {
    	return hash('md5', $this->mKeys);
    }
}
?>