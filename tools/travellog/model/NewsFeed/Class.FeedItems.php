<?php
	
	require_once('travellog/model/Class.Traveler.php');
	require_once('travellog/model/Class.TravelerProfile.php');
	require_once('travellog/model/Class.AdminGroup.php');
	
	class FeedItems{
		
		protected $mItems		= null;
		protected $mItemDetail	= array();	// objects for each feed item context, set thru each feed context
		protected $mDoerID 		= null;
		protected $mDoer		= null;
		protected $mRecipientID = null;
		protected $mRecipient	= null;
		protected $mRecipientPath = '';
		protected $mDoerPath	= '';
		protected $mLatestTime 	= null; // latest time within grouped feed
		protected $mSection 	= null;
		protected $mEvent		= null;
		protected $mContext		= null;
		
		public function getDoerID(){
			return $this->mDoerID;
		}
		/**
		 * @return TravelerProfile doer of action
		 */
		public function getDoer(){
			return $this->mDoer;
		}
		
		public function getRecipientID(){
			return $this->mRecipientID;
		}
		/**
		 * @return TravelerProfile traveler subject of action made by doer
		 */
		public function getRecipient(){
			return $this->mRecipient;
		}
		
		public function getLatestTime(){
			return $this->mLatestTime;
		}
		
		public function getSection(){
			return $this->mSection;
		}
		
		/**
		 * @param boolean possessive "if link is in possessive form"
		 * @return string link to doer's profile page or group's home page
		 */
		public function getDoerLink($possessive = false){
			if($this->mDoer instanceof TravelerProfile){
				if($this->mDoerID != NewsFeed::getLoggedTraveler()->getTravelerID())
					return self::constructLink($this->mDoer->getUsername(), $this->mDoer->getUsername()) . ($possessive?"'s":"");
				else
					return 'You';
			} 
			else{
				return self::constructLink('group.php?gID=' . $this->mDoer->getGroupID(), $this->mDoer->getName());
			}
		}
		
		/**
		 * @param boolean possessive "if link is in possessive form"
		 * @return string link to recipient's profile page or group's home page
		 */
		public function getRecipientLink($possessive = false){
			if($this->mRecipient instanceof TravelerProfile){
				if($this->mRecipientID != NewsFeed::getLoggedTraveler()->getTravelerID())
					return self::constructLink($this->mRecipient->getUsername(), $this->mRecipient->getUsername()) . ($possessive?"'s":"");
				else
					return ($possessive) ? 'your' : 'you';
			} 
			else{
				return self::constructLink('group.php?gID=' . $this->mRecipient->getGroupID(), $this->mRecipient->getName());
			}
		}
		
		/**
		 * @param boolean isDoer
		 * @return string 'appropriate pronoun for doer or recipient'
		 */
		public function getPronoun($isDoer = TRUE, $isPossessive = TRUE){
			$subject = ($isDoer) ? $this->mDoer : $this->mRecipient;
			if($subject->getTravelerID() == NewsFeed::getLoggedTraveler()->getTravelerID())
				return ($isPossessive ? 'your' : 'You');
			switch($subject->getGender()){
				case 'M' :
					return ($isPossessive ? 'his' : 'He');
					break;
				case 'F' :
					return ($isPossessive ? 'her' : 'She');
					break;
				default:
					return ($isPossessive ? 'his/her' : 'He/She');
			}
		}
		
		/**
		 * @return string context of feed item
		 */
		public function getContext(){
			return $this->mContext;
		}
		
		/**
		 * @return array 'instance of whatever the item section is'
		 */
		public function getDetailOfItems(){
			return $this->mItemDetail;
		}
		
		/**
		 * @return int 'log event type'
		 */
		public function getEvent(){
			return $this->mEvent;
		}
		
		public function setLatestTime($time){
			$this->mLatestTime = $time;
		}
		
		/**
		 * @param int travelerID of Doer
		 */
		public function setDoerID($doerID){echo 'daf';exit;
			$this->mDoerID = $doerID;
			$adminGroup = $this->getAdvisorGroup($this->mDoerID);
			if(is_null($adminGroup))
				$this->mDoer = new TravelerProfile($doerID);
			else
				$this->mDoer = $adminGroup;
		}
		
		/**
		 * @param int travelerID of subject/recipient of action
		 */
		public function setRecipientID($recipientID){
			$this->mRecipientID = $recipientID;
			$adminGroup = $this->getAdvisorGroup($this->mRecipientID);
			if(is_null($adminGroup))
				$this->mRecipient = new TravelerProfile($this->mRecipientID);
			else
				$this->mRecipient = $adminGroup;
		}
		
		/**
		 * @param array 'semi-processed data from logs'
		 */
		public function setItems($data){
			$this->mItems = $data;
		}
		
		/**
		 * @param int 'event type'
		 */
		public function setEvent($event){
			$this->mEvent = $event;
		}
		
		/**
		 * @param string 'context for this feed item'
		 */
		public function setContext($context){
			$this->mContext = $context;
		}
		
		private function getAdvisorGroup($travelerID){
			$agID = AdminGroup::getAdvisorGroupID($this->mDoerID);
			if($agID > 0)
				return new AdminGroup($agID);
			else
				return null;
		}
		
		//static function getProfileLinkOfUser($username){
		//	return '<a href="http://' . $_SERVER['HTTP_HOST'] .'/' . $username . '">' . $username . '</a>' ;
		//}
		
		/**
		 * @param string url
		 * @param string item
		 * @return string hyperlink to item with given url
		 */
		static function constructLink($url, $item){
			return '<a href="'  . $url . '">' . $item . '</a>' ;
		}
		
	}
?>
