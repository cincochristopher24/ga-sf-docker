<?php
	class Sections{
		
		const TRAVEL_PLANS 		= 1;
		const TRAVEL_BIO		= 2;
		const COMMENTS			= 3;
		const GROUPS			= 4;
		const FRIEND			= 5;
		const JOURNAL_ENTRY		= 6;
		const PHOTO				= 7;
		const BULLETIN			= 8;
		const CALENDAR_EVENT	= 9;
		
		private static $updateHandlers	= array(); 
		
		static function registerUpdateHandler($section, $filePath, $name){
			require_once($filePath);
			self::$updateHandlers[$section] = $name;
		}
		
		static function getUpdateHandler($section){
			$name = self::$updateHandlers[$section];
			return new $name;
			
		}
	}
?>
