<?php
	/** 
	 * no longer used
	 */
	class UpdateHandler{
		
		private static $handlers = array(
				Sections::TRAVEL_PLANS 	=> array('FILE' => 'Class.TravelPlanUpdates.php', 	'CLASS' => 'TravelPlanUpdates'),
				Sections::COMMENTS		=> array('FILE' => 'Class.CommentUpdates.php', 		'CLASS' => 'CommentUpdates'),
				Sections::GROUPS 		=> array('FILE' => 'Class.GroupUpdates.php', 		'CLASS' => 'GroupUpdates'),
				Sections::FRIEND		=> array('FILE' => 'Class.FriendUpdates.php',		'CLASS' => 'FriendUpdates'),
				Sections::TRAVEL_BIO	=> array('FILE' => 'Class.TravelBioUpdates.php',	'CLASS' => 'TravelBioUpdates'),
				Sections::PHOTO			=> array('FILE' => 'Class.PhotoUpdates.php',		'CLASS' => 'PhotoUpdates'),
		 		Sections::BULLETIN		=> array('FILE' => 'Class.BulletinUpdates.php',		'CLASS' => 'BulletinUpdates'),
				Sections::JOURNAL_ENTRY	=> array('FILE' => 'Class.JournalEntryUpdates.php',	'CLASS' => 'JournalEntryUpdates'),
				Sections::CALENDAR_EVENT=> array('FILE' => 'Class.CalendarEventUpdates.php','CLASS' => 'CalendarEventUpdates'));
		
		public static function create_update_handler($section){
			require_once('travellog/model/NewsFeed/Contexts/' . self::$handlers[$section]['FILE']);
			$class = self::$handlers[$section]['CLASS'];
			return new $class;
		}
	}
?>