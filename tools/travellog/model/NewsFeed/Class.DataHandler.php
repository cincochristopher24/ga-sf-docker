<?php
	require_once('gaLogs/Reader/Class.Reader.php');
	
	class DataHandler{
		
		private static $groupedRawData 	= array();
		private static $processedData	= array();
		private static $feedSections 	= array();
		
		/** 
		 * @param string 'sql'
		 * @return iRecordset 'result of sql query from log reader'
		 */
		public static function retrieveLogFeeds($sql){
			$trace = debug_backtrace();
			$debugTrace = $trace[0];
			for($traceCtr = 0; $traceCtr < count($trace); ++$traceCtr){
				if(array_key_exists('function', $trace[$traceCtr]) && $trace[$traceCtr]['function'] == 'retrieveLogFeeds')
					$debugTrace = $trace[$traceCtr];
			}
			try{
				$reader = new Reader();
				$data = $reader->getLogsBySql($sql);
				return $data;
			}
			catch(exception $e){
				throw new exception(($e->getMessage() . '<BR>' . (is_array($debugTrace) ? 'Accessed from : FILE {' . $debugTrace['file'] . '} LINE {' . $debugTrace['line'] . '}' : '')));  
			}
			
		}
		
		/**
		 * @param resource 'raw data to be processed'
		 */
		public static function groupRawData($data, $context){
			$groupedRawData = array();
			while($row = mysql_fetch_assoc($data)){
				$datetime = array('TIME' => date("G:i:s", strtotime($row['_EXECUTION_DATE_'])), 'DATE' => date("Y-m-d", strtotime($row['_EXECUTION_DATE_'])));
				$feed = self::_retrieveApplicableFields($row, $row['_EVENT_']); // retrieve applicable fields for feed 
				
				// process data according to each update handler/context, different contexts may have different processing info
				self::$processedData[$datetime['DATE']][self::$feedSections[$context]] = Sections::getUpdateHandler(self::$feedSections[$context])->processUpdates
					(	array(	'DATA' => $feed, 'DOER_ID' => $row['_DOER_ID_'], 'COMMAND' => $row['_COMMAND_'], 'EVENT' => $row['_EVENT_'],
								'DATE' => $datetime['DATE'], 'TIME' => $datetime['TIME'], 'CONTEXT' => $context)); 
			}
		}
		
		/**
		 * sort, add detail to each semiprocessed data
		 */
		private static function _process(){
			$temp = array();
			foreach(self::$processedData as $date => $context){
				foreach($context as $section => $content){
					foreach($content as $time => $update){
						$obj_feed = Sections::getUpdateHandler($section);
						$obj_feed->setItems($update['ITEMS']);
						if(isset($update['RECIPIENT_ID']))
							$obj_feed->setRecipientID($update['RECIPIENT_ID']);
						$obj_feed->setDoerID($update['DOER_ID']);
						$obj_feed->setLatestTime($time);
						$obj_feed->setContext($update['CONTEXT']);
						$trace = debug_backtrace();
						$obj_feed->setEvent($update['EVENT']);
						try{
							$obj_feed->setDetailOfItems();	
						}
						catch(exception $e){
							throw new NewsFeedException(NewsFeedException::DETAIL_INSTANTIATION_ERROR, NULL, $e->getMessage());
						}
						if(count($obj_feed->getDetailOfItems()) > 0){
							$temp[$date][$time] = $obj_feed;
						}
					}
				}
				if(isset($temp[$date]))
					self::_sort($temp[$date]); // sort updates for this date order by time desc
			}
			self::_sort($temp, false); // sort updates for this date order by date desc
			self::$processedData = $temp; // replace semiprocessed data with newly processed
		}
		
		/**
		 * sort feeds for each date
		 */ 
		private static function _sort(&$arr, $time = true){
			$timeKeys = array_keys($arr);
			array_walk($timeKeys, create_function('&$val, $key', '$val = strtotime($val);')); // convert keys to timestamp
			rsort($timeKeys); // reverse sort
			if($time)
				array_walk($timeKeys, create_function('&$val, $key', '$val = date("G:i:s", $val);')); // convert timestamp to keys
			else
				array_walk($timeKeys, create_function('&$val, $key', '$val = date("Y-m-d", $val);')); // convert timestamp to keys
			$temp = array_flip($timeKeys);
			foreach($temp as $timeKey => $content)
				$temp[$timeKey] = $arr[$timeKey]; // replace;
			$arr = $temp;
		}
		
		/**
		 * retrieve only the fields for particular physical table and with regards to event
		 */
		private static function _retrieveApplicableFields($rowData, $event){
			$arr = array();
			if(is_array($rowData)){
				($event == EventType::UPDATE) ? $mode = '((_NEW_)|(_OLD_))' : (($event == EventType::INSERT) ? $mode = '(_NEW_)' : $mode = '(_OLD_)');
				foreach($rowData as $fieldName => $value){
					if(0 == preg_match('/^((_NEW_)|(_OLD_))/', $fieldName))
						break;
					if (preg_match("/^$mode/", $fieldName)){
						$field = preg_replace("/^($mode)/", '', $fieldName);
						$fieldValueType = preg_replace('/(' . addcslashes($field, ('(+.*-)')) . ')/', '', $fieldName);
						$arr[$fieldValueType][$field] = $value;
					} 
				}
			}
			return $arr;
		}
		
		
		public static function getGroupedRawData(){
			return self::$groupedRawData;
		}
		
		public static function getProcessedData(){
			self::_process();
			return self::$processedData;
		}
		
		public static function addContext($param){
			self::$feedSections[$param['CONTEXT']] = $param['SECTION'];
		}
		
	}
?>