<?php
	
	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	
	class PhotoUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates = array();
		private static $latestTime = null;
		
		public function __construct(){
			$this->mSection = Sections::PHOTO;
		}
		
		public function setDetailOfItems(){
			require_once('travellog/model/Class.Photo.php');
			if(!is_null($this->mItems)){
				foreach($this->mItems as $item){
					$context = null;
					switch($this->mContext){
						case 'PROFILE_PHOTO' :
							$context = 'profile';
							break;
						case 'JOURNAL_ENTRY_PHOTO' :
							$context = 'journal';
							break;
						default;
 					}
					$this->mItemDetail[] = new Photo($context, $item['photoID']); 
				}
			}
		}
		
		public function readUpdates($params = array()){
			$condition = $params['CONDITION'];
			
			// profile photos
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblTravelertoPhoto` WHERE " .
					"(_COMMAND_ = 'INSERT' AND _EVENT_ = 1) AND $condition " .
					"ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'PROFILE_PHOTO', 'SECTION' => Sections::PHOTO));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'PROFILE_PHOTO');
			
			// journal entry photos
			$sql =  "SELECT * FROM `Travel_Logs`.`tblTravelLogtoPhoto` WHERE " .
					"(_COMMAND_ = 'INSERT' AND _EVENT_ = 1) " .
					"AND $condition ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'JOURNAL_ENTRY_PHOTO', 'SECTION' => Sections::PHOTO));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'JOURNAL_ENTRY_PHOTO');
		}
		
		public function processUpdates($params = array()){
			if(!isset(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['COMMAND']]))
				self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['COMMAND']] = $params['TIME'];
			else{
				(strtotime(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['COMMAND']]) < strtotime($params['TIME']))
					? self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['COMMAND']] = $params['TIME']
					: self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['COMMAND']]; 
			}
			$feed = &self::$updates[$params['DATE']][$params['DOER_ID']][$params['COMMAND']]['FEED'];
			$feed['ITEMS'][$params['DATA']['_NEW_']['photoID']] = $params['DATA']['_NEW_']; 
			$feed['ITEMS'][$params['DATA']['_NEW_']['photoID']]['EXEC_TIME'] = $params['TIME'];
			$feed['DOER_ID'] 	= $params['DOER_ID'];
			$feed['CONTEXT']	= $params['CONTEXT'];
			$feed['EVENT']		= $params['COMMAND'];
			$arr = array();
			foreach(self::$updates[$params['DATE']] as $doerID => $command){
				foreach($command as $cmd => $doerUpdates)
					$arr[self::$latestTime[$params['DATE']][$doerID][$cmd]] = $doerUpdates['FEED'];
			}
			return $arr;
		}
	}
?>