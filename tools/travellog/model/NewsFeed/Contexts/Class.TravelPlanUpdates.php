<?php
	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	
	class TravelPLanUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates = array();
		private static $latestTime = null;
		
		public function __construct(){
			$this->mSection = Sections::TRAVEL_PLANS;
		}
		
		public function setDetailOfItems(){
			require_once('travellog/model/Class.TravelSchedule.php');

			foreach($this->mItems as $key => $item){
				//$this->mItemDetail[$item['travelWishID']] = $item['title']; // titles only
				
				// check if userid of travelwish is not the same with the doer
				if($item['userID'] != TravelSchedule::getUserIDWithTravelerID($this->mDoerID))
					unset($this->mItems[$key]); // feed is invalid, so remove it
				
				// set detail of this item to TravelPlan obj
				$this->mItemDetail[] = TravelSchedule::retrieveByPk($item['travelWishID']);
			} 
		}
		
		public function readUpdates($params = array()){
			$condition = $params['CONDITION'];
			// only retrieve those feeds with existing tbwishid in physical table
			$sql =	"SELECT * FROM `SocialApplication`.`tbTravelWish` AS logTbWish WHERE $condition AND " .
					"(_EVENT_ = '" . EventType::INSERT . "' OR _EVENT_ = '" . EventType::UPDATE . "') " .
					"AND EXISTS (<$ SELECT `travelWishID` FROM `SocialApplication`.`tbTravelWish` phyTbWish " .
					"WHERE logTbWish.`_NEW_travelWishID` = phyTbWish.`travelWishID` LIMIT 0, 1$>) " .
					"ORDER BY _EXECUTION_DATE_ DESC";
			
			DataHandler::addContext(array('CONTEXT' => 'TRAVEL_PLANS', 'SECTION' => Sections::TRAVEL_PLANS));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'TRAVEL_PLANS');
		}
		
		public function processUpdates($params = array()){
			if(!isset(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]))
				self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']] = $params['TIME'];
			else{
				(strtotime(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]) < strtotime($params['TIME']))
					? self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']] = $params['TIME']
					: self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['EVENT']]; 
			}
				
			$feed = &self::$updates[$params['DATE']][$params['DOER_ID']][$params['EVENT']]['FEED'];
			if(!isset($feed['ITEMS'][$params['DATA']['_NEW_']['travelWishID']]))
				$feed['ITEMS'][$params['DATA']['_NEW_']['travelWishID']] = $params['DATA']['_NEW_'];
			$feed['DOER_ID'] = $params['DOER_ID'];
			$feed['EVENT']	= $params['EVENT'];
			$feed['CONTEXT'] = $params['CONTEXT'];
			$arr = array();
			
			foreach(self::$updates[$params['DATE']] as $doerID => $command){
				foreach($command as $cmd => $doerUpdates)
					$arr[self::$latestTime[$params['DATE']][$doerID][$cmd]] = $doerUpdates['FEED'];
			}
			return $arr;
		}
	}
?>