<?php
	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	
	class GroupUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates 	= array();
		private static $latestTime = null;
		
		public function __construct(){
			$this->mSection = Sections::GROUPS;
		}
		
		// sets the groups joined/or created per item
		public function setDetailOfItems(){
			$arrGroupId = array();
			foreach($this->mItems as $item)
				$arrGroupId[] = $item['groupID'];

			$this->mItemDetail = GroupFactory::instance()->create($arrGroupId);
		}
		
		public function readUpdates($params = array()){
			$condition = $params['CONDITION'];
			// group/club memberships
			//$sql = 	"SELECT * FROM `Travel_Logs`.`tblGrouptoTraveler` WHERE $condition AND _EVENT_ = '" . EventType::INSERT . "' ORDER BY _EXECUTION_DATE_ DESC";
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblGrouptoTraveler` t1 WHERE `_NEW_travelerID` IN (" . implode(", ", $params['SUBJECTS']) . ") " .
					"AND $condition AND _EVENT_ = '" . EventType::INSERT . "' AND _NEW_travelerID NOT IN " .
					"(<$ SELECT administrator FROM `Travel_Logs`.`tblGroup` t2 WHERE t2.groupID = t1._NEW_groupID $>) ORDER BY _EXECUTION_DATE_ DESC";
			
			
			DataHandler::addContext(array('CONTEXT' => 'GROUP_MEMBERSHIP', 'SECTION' => Sections::GROUPS));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'GROUP_MEMBERSHIP');
			
			// group/club creation
			//$sql = 	"SELECT * FROM `Travel_Logs`.`tblGroup` WHERE $condition AND _EVENT_ = '" . EventType::INSERT . "' ORDER BY _EXECUTION_DATE_ DESC";
			$sql = 	"SELECT * FROM `Travel_Logs`.`tblGroup` WHERE _DOER_ID_ IN (" . implode(", ", $params['SUBJECTS']) . ") " .
					"AND $condition AND _EVENT_ = '" . EventType::INSERT . "' ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'GROUP_CREATION', 'SECTION' => Sections::GROUPS));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'GROUP_CREATION');
		}
		
		// context = membership or creation
		public function processUpdates($params = array()){
			if('GROUP_MEMBERSHIP' == $params['CONTEXT'])
				$params['DOER_ID'] = $params['DATA']['_NEW_']['travelerID']; // will occur if logged user is the one approving the invite
			if(!isset(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['CONTEXT']][$params['EVENT']]))
				self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['CONTEXT']][$params['EVENT']] = $params['TIME'];
			else{
				(strtotime(self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['CONTEXT']][$params['EVENT']]) < strtotime($params['TIME']))
					? self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['CONTEXT']][$params['EVENT']] = $params['TIME']
					: self::$latestTime[$params['DATE']][$params['DOER_ID']][$params['CONTEXT']][$params['EVENT']]; 
			}
			$feed = &self::$updates[$params['DATE']][$params['DOER_ID']][$params['CONTEXT']][$params['EVENT']]['FEED'];
			
			$feed['ITEMS'][$params['DATA']['_NEW_']['groupID']] = $params['DATA']['_NEW_']; // key = groupID
			$feed['ITEMS'][$params['DATA']['_NEW_']['groupID']]['CONTEXT'] = $params['CONTEXT'];
			$feed['DOER_ID'] 	= $params['DOER_ID'];
			$feed['CONTEXT']	= $params['CONTEXT'];
			$feed['EVENT']		= $params['EVENT'];
			$arr 				= array();
			foreach(self::$updates[$params['DATE']] as $doerID => $context){
				foreach($context as $key => $command){
					foreach($command as $cmd => $doerUpdates)
						$arr[self::$latestTime[$params['DATE']][$doerID][$key][$cmd]] = $doerUpdates['FEED'];
				}
			}
			return $arr;
		}
		
	}
?>