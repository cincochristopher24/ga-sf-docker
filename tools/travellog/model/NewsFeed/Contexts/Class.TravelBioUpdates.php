<?php
	require_once('travellog/model/NewsFeed/Interface.UpdateContexts.php');
	
	class TravelBioUpdates extends FeedItems implements UpdateContexts{
		
		private static $updates 	= array();
		private static $latestTime 	= null;
		
		public function __construct(){
			$this->mSection = Sections::TRAVEL_BIO;
		}
		
		public function setDetailOfItems(){
			require_once('travellog/model/travelbio/Class.AnswerValue.php');
			foreach($this->mItems as $item)
				$this->mItemDetail[] = new AnswerValue($item['answerValueID']);
		}
		
		public function readUpdates($params = array()){
			$condition = $params['CONDITION'];
			/*$sql =	"SELECT * FROM `SocialApplication`.`tblAnswerValue` t1 WHERE $condition AND " .
					"_COMMAND_ = 'INSERT' AND t1._NEW_value NOT IN " .
					"(SELECT _OLD_value FROM `SocialApplication`.`tblAnswerValue` t2 " .
					"WHERE _COMMAND_ = 'DELETE' AND t1._LOG_ID_ = t2._LOG_ID_) " .
					"ORDER BY _EXECUTION_DATE_ DESC";*/
			// check also in the real table if record in log still exists
			$sql =	"SELECT * FROM `SocialApplication`.`tblAnswerValue` logView WHERE $condition " .
					"AND _COMMAND_ = 'INSERT' AND EXISTS " .
					"( <$ SELECT * FROM `SocialApplication`.`tblAnswerValue` realTable WHERE realTable.answerValueID = logView._NEW_answerValueID $> ) " .
					"ORDER BY _EXECUTION_DATE_ DESC";
			DataHandler::addContext(array('CONTEXT' => 'TRAVEL_BIO', 'SECTION' => Sections::TRAVEL_BIO));
			DataHandler::groupRawData(DataHandler::retrieveLogFeeds($sql)->retrieveData(), 'TRAVEL_BIO');
		}
		
		public function processUpdates($params = array()){
			if(!isset(self::$latestTime[$params['DATE']][$params['DOER_ID']])){
				self::$latestTime[$params['DATE']][$params['DOER_ID']] = $params['TIME'];				
			}
			$feed = &self::$updates[$params['DATE']][$params['DOER_ID']][self::$latestTime[$params['DATE']][$params['DOER_ID']]]['FEED'];
			if($params['TIME'] == self::$latestTime[$params['DATE']][$params['DOER_ID']]){
				$feed['ITEMS'][] 	= $params['DATA']['_NEW_'];
				$feed['DOER_ID'] 	= $params['DOER_ID'];
				$feed['EVENT']		= $params['COMMAND'];
				$feed['CONTEXT']	= $params['CONTEXT'];
			}
			$arr 					= array();
			foreach(self::$updates[$params['DATE']] as $doerID => $latestTime){
				foreach($latestTime as $cmd => $doerUpdates)
					$arr[self::$latestTime[$params['DATE']][$doerID]] = $doerUpdates['FEED'];
			}
			return $arr;
		}
	}
?>