<?php
	class NewsFeedException extends Exception{
		
		const DETAIL_INSTANTIATION_ERROR 	= 1001;
		const READLOG_ERROR 				= 2001;
		const INVALID_CONSTRUCTOR_PARAM 	= 3001;
		
		private $exceptionMessage = array(
			self::DETAIL_INSTANTIATION_ERROR 	=> 'Error occured while instantiating detail of item.', 
			self::READLOG_ERROR 				=> 'Error occured while reading feeds from GaLogs.',
			self::INVALID_CONSTRUCTOR_PARAM		=> 'Invalid parameter passed to NewsFeed constructor.');
		
		public function __construct($errorCode, $debugTrace, $description = NULL){
			$message = "ERROR $errorCode: " . $this->exceptionMessage[$errorCode] . ' ' . (is_array($debugTrace) ? 'Accessed from FILE: ' . $debugTrace['file'] . ' LINE: ' . $debugTrace['line']:'');
			if(FALSE == is_null($description))
				$message .= " DETAIL << $description >>";
			parent::__construct($message, $errorCode);
		}
		
		public function __toString(){
			return __CLASS__ . ' ' . $this->getMessage();
		}
	}
?>
