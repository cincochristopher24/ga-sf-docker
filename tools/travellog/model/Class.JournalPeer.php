<?php
	/**
	 * @(#) Class.JournalPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 24, 2009
	 */
	
	require_once('travellog/model/base/Class.BaseJournalPeer.php');
	
	/**
	 * Model peer class for journal.
	 */
	class JournalPeer extends BaseJournalPeer {
		
		/**
		 * Returns the traveler/group where the given journal is linked.
		 * 
		 * @param integer $travelID The ID of the journal where we the data for the traveler|group link will be get.
		 * 
		 * @return Traveler|Group the traveler/group where the given journal is linked.
		 */
		function getJournalLinkedOwner($travelID){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "SELECT * FROM tblTravel, tblTravelLink " .
					" WHERE tblTravel.travellinkID = tblTravelLink.travellinkID " .
					" AND tblTravel.travelID = ".$handler->makeSqlSafeString($travelID);
				
				$resource = $handler->execute($sql);
				if ($record = mysql_fetch_assoc($resource)) {
					if (2 == $record['reftype']) {
						require_once('travellog/model/Class.AdminGroup.php');
						return new AdminGroup($record['refID']);
					}
					else {
						require_once('travellog/model/Class.Traveler.php');
						return new Traveler($record['refID']);
					}
				}
			}
			catch (exception $ex) {}
		}
		
		/**
		  * Returns the latest journal added by a traveler
		  * @param	Traveler	$traveler	The traveler in focus
		  * @return Travel		an instance of class Travel
		  **/
		public static function getLatestJournalAdded(Traveler $traveler){
			$travelerID = $traveler->getTravelerID();
			$sql = "SELECT tblTravel.*
					FROM tblTravel, tblTravelLink
					WHERE tblTravel.deleted = 0
						AND tblTravel.travellinkID = tblTravelLink.travellinkID
						AND tblTravelLink.reftype = 1
						AND tblTravelLink.refID = $travelerID
					ORDER BY tblTravel.travelID DESC
					LIMIT 0,1";
			
			$rs	=	ConnectionProvider::instance()->getRecordset();
			$rs->execute($sql);

			$journal = NULL;
			if (0 < $rs->Recordcount()){
				$data = mysql_fetch_array($rs->Resultset());
				$journal = new Travel($data["travelID"]);
			}

			return $journal;
		}
	}
	
