<?php
/**
 * Created on Jul 31, 2006
 * @author Joel C. LLano
 * Purpose: 
 */

include_once('Class.ValidatorOuput.php'); 
include_once('Class.UploadValidator.php'); 
include_once('Class.PathManager.php');

class ImageValidator extends UploadValidator{ 
		
		private $arr_files				= array();
		private $arr_validatoroutput	= array(); 
		private $arr_validfiles		= array();
		private $arr_invalidfiles		= array();
		
		
		
		function setFiles($files){
			$this->arr_files = $files;
		}
		
		function  validate(){
			
				$files = $this->arr_files;
				$cnt = 1;
				
				//$temppath = PathManager::$temporarypath;
				
				$temppath = $_SERVER['DOCUMENT_ROOT']."/users/";
				
				if($files){
				
					foreach($files as $ufile){
							
							$objval_ouput = new ValidatorOutput();						
							$objval_ouput->setSize($ufile->getFilesize());
							$objval_ouput->setFType($ufile->getFiletype());
							
							
							
							if(file_exists($temppath.$ufile->getFilename())){
							
								$img = NULL;
										
																	
								$img = getimagesize($temppath.$ufile->getFilename());													
							
								if($img){	
										switch(substr($img['mime'],6,4)){
											case "jpeg":
													$ufilename = $cnt.uniqid().time().".jpg";
													rename($temppath.$ufile->getFilename(),$temppath.$ufilename);
													$objval_ouput->setName($ufilename);
													$objval_ouput->setMessage("Successfully uploaded!");
													$objval_ouput->setStatus(true);	
											break;										
											case "gif":
													$ufilename = $cnt.uniqid().time().".gif";
													rename($temppath.$ufile->getFilename(),$temppath.$ufilename);
													$objval_ouput->setName($ufilename);
													$objval_ouput->setMessage("Successfully uploaded!");
													$objval_ouput->setStatus(true);										
											break;										
											case "png":
													$ufilename = $cnt.uniqid().time().".png";
													rename($temppath.$ufile->getFilename(),$temppath.$ufilename);
													$objval_ouput->setName($ufilename);
													$objval_ouput->setMessage("Successfully uploaded!");
													$objval_ouput->setStatus(true);	
											break;
											
											default:
													$objval_ouput->setName($ufile->getFilename());
													$objval_ouput->setMessage("Invalid type of Image");
													//delete from  temporary destination			
													unlink($temppath.$cnt.time().$ufile->getFilename());
											break;											
										}
								
								}else{
										//invalid Type of Image
										$objval_ouput->setName($ufile->getFilename());
										$objval_ouput->setMessage("Invalid type of Image");
										unlink($temppath.$ufile->getFilename());	
								}
							
							}else{
								
								//invalid Type of Image
								$objval_ouput->setName($ufile->getFilename());
								$objval_ouput->setMessage("Invalid type of Image");
							}
							
					$this->arr_validatoroutput[] = $objval_ouput;
					
					$cnt = $cnt + 1;
					
					}
					
				}else{
					$objval_ouput = new ValidatorOutput();						
					$objval_ouput->setSize(0);
					$objval_ouput->setFType(NULL);
					//Null Values
					$objval_ouput->setName(NULL);
					$objval_ouput->setMessage("Please select photos!");
					
					$this->arr_validatoroutput[] = $objval_ouput;
					
				}
				
		}
		
		
		
		
		public function getValidFiles(){
			foreach($this->arr_validatoroutput as $outfiles)
				if($outfiles->getStatus())$this->arr_validfiles[] =	$outfiles;
			return $this->arr_validfiles;
		}
		
		public function getInValidFiles(){
			foreach($this->arr_validatoroutput as $outfiles)
				if(!$outfiles->getStatus())$this->arr_invalidfiles[] = 	$outfiles;
			return $this->arr_invalidfiles;
		}

		public function getResults(){
			return $this->arr_validatoroutput;	
		}
		
		




}

?>
