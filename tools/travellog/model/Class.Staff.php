<?php
	/*
	 * Class.Staff.php
	 * Created on Jan 23, 2008
	 * @author Cheryl Ivy Q. Go
	 */

	 require_once 'travellog/model/Class.Advisor.php';
	 
	 class Staff extends Advisor {
	 	
	 	private $mStatus = 0;
		private $mConn	= NULL;
		private $mRs	= NULL;
	 	
	 	function __construct($_travelerID = 0, $_groupID = 0){
	 		try{
				$this->mConn = new Connection();
		 		$this->mRs = new Recordset($this->mConn);
			}
			catch(exception $e){
				throw $e;
			}
	 		
			$sqlQuery = "SELECT sendableID, status " .
						"FROM tblGrouptoFacilitator " .
						"WHERE groupID = " . $_groupID . " " .
						"AND travelerID = " . $_travelerID . " " .
						"AND type = 1";
			$this->mRs->Execute($sqlQuery);

			if (0 < $this->mRs->Recordcount()){
				parent::Traveler($_travelerID);
	 			$groupFactory = GroupFactory::instance();
				$mGroup = $groupFactory->Create(array($_groupID));
	 			$this->setAdvisorGroup($mGroup[0]);
	 			$this->setSendableID($this->mRs->Result(0,'sendableID'));
	 			$this->setStatus($this->mRs->Result(0,'status'));
			}
			else
				throw new exception("Staff does not exist: travelerID = $_travelerID, groupID = $_groupID");

	 		return $this;
	 	}
	 	
	 	function setStatus($status){
	 		$this->status = $status;
	 	}
	 	
	 	function getStatus(){
	 		return $this->status;
	 	}

		public static function DeleteAllGroupStaff($_sendableId = 0){
			// check if sendableID is of group
			$mFactory = MessageSendableFactory::Instance();
			$mSendable = $mFactory->Create($_sendableId);

			if ($mSendable instanceof Group){
				// If it's an Admin Group, check if it's a SG. For now, only subgroups can be deleted, therefore only SG staff will be deleted

				if ($mSendable->getDiscriminator() == Group::ADMIN && $mSendable->isSubGroup()){
					try{
						$mConn = new Connection();
						$mRs	= new Recordset($mConn);
						$mRs2	= new Recordset($mConn);
					}
					catch(exception $e){
						throw $e;
					}

					$sql = "DELETE FROM tblSendable WHERE sendableType = 3 AND sendableID IN " .
							"(" .
								"SELECT sendableID FROM tblGrouptoFacilitator WHERE groupID = " . $mSendable->getGroupID() .
							")";
					$mRs->Execute($sql);

					$sql2 = "DELETE FROM tblGrouptoFacilitator WHERE groupID = " . $mSendable->getGroupID();
					$mRs2->Execute($sql2);
				}
			}
		}
	 }
?>
