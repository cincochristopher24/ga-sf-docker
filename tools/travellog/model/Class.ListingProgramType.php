<?
	/**
	 * Class.ListingProgramType.php
	 * @author marc
	 * program type of GA listing
	 */
	 
	require_once('Class.Connection.php');
 	require_once('Class.Recordset.php');
 	
 	/**
 	 * @var int programTypeID
 	 * @var string programType
 	 * @var object nConnection - see Class.Connection.php
 	 * @var onject nRecordset - see Class.Recordset.php
 	 */
	class listingProgramType{
		
		private $programTypeID;
		private $programType;
		private $nConnection;
		private $nRecordset;
				
		/** 
		 * constructor
		 * @param int programTypeID - ID of program Type
		 * @param int incProgramID - ID of program
		 * @return object self
		 */
		
		function listingProgramType($programTypeID = 0,$incProgramID = 0){
			
			try {
 				$this->nConnection = new Connection();
				$this->nRecordset   = new Recordset($this->nConnection);
			}
			catch (Exception $e) {				   
			   throw $e;
			}
					
			if ($incProgramID == "2")
				return;
			if ($programTypeID > 0 && $incProgramID > 0){
				switch($incProgramID){
		
					//Volunteer Abroad		:: tbvolunteertype
					case "1": 
						$sqlquery = "SELECT volunteertypeID AS programtypeID, volunteertype AS programtype FROM GoAbroad_Main.tbvolunteertype " . 
						"WHERE volunteertypeID=" . $programTypeID;
						break;
		
					// Intrenships Abroad & Jobs Abroad		:: tbinterntype 		
					case "3":				
					case "4": 
						$sqlquery = "SELECT interntypeID AS programtypeID, interntype AS programtype FROM GoAbroad_Main.tbinterntype " . 
						"WHERE interntypeID=" . $programTypeID;
						break;
				
					//Study Abroad    :: tbdegprog
					case "5": 
						$sqlquery = "SELECT degprogID AS programtypeID, degprog AS programtype FROM GoAbroad_Main.tbdegprog " . 
						"WHERE degprogID=" . $programTypeID;
						break;
										
					// Languages Abroad		:: tblanguage		
					case "6":
						$sqlquery = "SELECT languageID AS programtypeID, language AS programtype FROM GoAbroad_Main.tblanguage " . 
						"WHERE languageID=" . $programTypeID;
						break;
									
				}
				$myquery = $this->nRecordset->Execute($sqlquery);
				if (mysql_num_rows($myquery) == 0)
					die ('Invalid Program Type');
					$result = mysql_fetch_array($myquery);
				$this->setProgramTypeID($result["programtypeID"]);
				$this->setProgramType($result["programtype"]);	
			}
			
			return $this;
			
		}
			
		/**
		 * @param int programTypeID
		 */
		function setProgramTypeID($programTypeID){
			$this->programTypeID = $programTypeID;
		}	
		
		/**
		 * @return int programTypeID
		 */
		function getProgramTypeID(){
			return $this->programTypeID;
		}
		
		/**
		 * @param string programType
		 */
		function setProgramType($programType){
			$this->programType = $programType;
		}
		
		/**
		 * @return string programType
		 */
		function getProgramType(){
			return $this->programType;
		}
		
	}
?>