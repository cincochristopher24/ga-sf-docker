<?php
	class LocationMarker{
		private $loc;
		private $alink;
		private $txt;
		private $visible;
		private $isFocused = false;
		
		function __construct(){
			$this->loc = null;
			$this->alink = null;
			$this->txt = null;
			$this->visible = true;
		}
		
		public function setLocation($vloc){
			$this->loc = $vloc;			
		}
		public function getLocation(){
			return $this->loc;
		}
		public function setLink($link){
			$this->alink =  $link;
		}
		public function getLink(){
			return $this->alink;
		}
		public function setTxt($txt){
			$this->txt = $txt;
		}
		public function getTxt(){
			return $this->txt;
		}
		public function setVisibility($vis){
			//$this->visible = ($vis)?'true':'false';
			$this->visible = $vis;
		}
		public function getVisibility(){
			return $this->visible;
		}
		
		public function isFocused(){
			return $this->isFocused;
		}
		
		public function setFocus(){
			$this->isFocused = true; 
		}
		
		public function removeFocus(){
			$this->isFocused = false;
		}
	}
?>
