<?php
	/**
	 * @(#) Class.BaseJournalEntryDraft.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 24, 2009
	 */
	
	require_once("travellog/model/Class.GanetDbHandler.php");
	
	/**
	 * Base class of journal entry draft.
	 */
	abstract class BaseJournalEntryDraft {
	 
		/**
		 * The callout of the journal entry draft.
		 * 
		 * @var string 
		 */
		protected $mCallout = "";
	 
		/**
		 * The city ID of the journal entry draft.
		 * 
		 * @var integer 
		 */
		protected $mCityID = 0;
	 
		/**
		 * The country ID of the journal entry draft.
		 * 
		 * @var integer 
		 */
		protected $mCountryID = 0;
		
		/**
		 * The creation date of the journal entry draft.
		 * 
		 * @var string
		 */
		protected $mDateCreated = "0000-00-00 00:00:00";
	 
		/**
		 * The description of the journal entry draft.
		 * 
		 * @var string 
		 */
		protected $mDescription = "";
	 
		/**
		 * The is auto saved of the journal entry draft.
		 * 
		 * @var integer 
		 */
		protected $mIsAutoSaved = 0;
	 
		/**
		 * The journal entry draft ID of the journal entry draft.
		 * 
		 * @var integer 
		 */
		protected $mJournalEntryDraftID = 0;
	 
		/**
		 * The log date of the journal entry draft.
		 * 
		 * @var string 
		 */
		protected $mLogDate = "0000-00-00 00:00:00";
	 
		/**
		 * The title of the journal entry draft.
		 * 
		 * @var string 
		 */
		protected $mTitle = "";
	 
		/**
		 * The travel ID of the journal entry draft.
		 * 
		 * @var integer 
		 */
		protected $mTravelID = 0;
			  
		/**
		 * Deletes the journal entry draft in the database.
		 *
		 * @return void
		 */
		function delete(){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "DELETE FROM tblJournalEntryDraft WHERE `journalEntryDraftID` = ".$handler->makeSqlSafeString($this->mJournalEntryDraftID);
				
				$handler->execute($sql);
			}
			catch(exception $ex){}
		}
	 
		/**
		 * Returns the callout of the journal entry draft.
		 * 
		 * @return string the callout of the journal entry draft.
		 */
		function getCallout(){
			return $this->mCallout;
		}	
	 
		/**
		 * Returns the city ID of the journal entry draft.
		 * 
		 * @return integer the city ID of the journal entry draft.
		 */
		function getCityID(){
			return $this->mCityID;
		}	
	 
		/**
		 * Returns the country ID of the journal entry draft.
		 * 
		 * @return integer the country ID of the journal entry draft.
		 */
		function getCountryID(){
			return $this->mCountryID;
		}
		
		/**
		 * Returns the creation date of the journal entry draft.
		 * 
		 * @return string
		 */
		function getDateCreated($format = "M d, Y"){
			return date($format, strtotime($this->mDateCreated));
		}
	 
		/**
		 * Returns the description of the journal entry draft.
		 * 
		 * @return string the description of the journal entry draft.
		 */
		function getDescription(){
			return $this->mDescription;
		}	
	 
		/**
		 * Returns the is auto saved of the journal entry draft.
		 * 
		 * @return integer the is auto saved of the journal entry draft.
		 */
		function getIsAutoSaved(){
			return $this->mIsAutoSaved;
		}	
	 
		/**
		 * Returns the journal entry draft ID of the journal entry draft.
		 * 
		 * @return integer the journal entry draft ID of the journal entry draft.
		 */
		function getJournalEntryDraftID(){
			return $this->mJournalEntryDraftID;
		}	
	 
		/**
		 * Returns the log date of the journal entry draft.
		 * 
		 * @return string the log date of the journal entry draft.
		 */
		function getLogDate($format = "M d, Y"){
			return date($format, strtotime($this->mLogDate));
		}	
	 
		/**
		 * Returns the title of the journal entry draft.
		 * 
		 * @return string the title of the journal entry draft.
		 */
		function getTitle(){
			return $this->mTitle;
		}	
	 
		/**
		 * Returns the travel ID of the journal entry draft.
		 * 
		 * @return integer the travel ID of the journal entry draft.
		 */
		function getTravelID(){
			return $this->mTravelID;
		}	
	  	
		/**
		 * Sets the properties of the journal entry draft.
		 * 
		 * @param array $props The new values for the properties of JournalEntryDraft.
		 * 
		 * @return void
		 */
		function initialize(array $props){
			if (0 < count($props)) {
				$this->mCallout = isset($props['callout']) ? $props['callout'] : $this->mCallout;
				$this->mCityID = isset($props['cityID']) ? $props['cityID'] : $this->mCityID;
				$this->mCountryID = isset($props['countryID']) ? $props['countryID'] : $this->mCountryID;
				$this->mDescription = isset($props['description']) ? $props['description'] : $this->mDescription;
				$this->mIsAutoSaved = isset($props['isAutoSaved']) ? $props['isAutoSaved'] : $this->mIsAutoSaved;
				$this->mJournalEntryDraftID = isset($props['journalEntryDraftID']) ? $props['journalEntryDraftID'] : $this->mJournalEntryDraftID;
				$this->mLogDate = isset($props['logDate']) ? $props['logDate'] : $this->mLogDate;
				$this->mTitle = isset($props['title']) ? $props['title'] : $this->mTitle;
				$this->mTravelID = isset($props['travelID']) ? $props['travelID'] : $this->mTravelID;
				$this->mDateCreated = isset($props['datecreated']) ? $props['datecreated'] : $this->mDateCreated;
			}
		}
			
		/**
		 * Inserts/Updates a journal entry draft in the database. When the journalEntryDraftID is
		 * equal to 0 this inserts a new entry in the database, otherwise this updates
		 * an existent entry.
		 *
		 * @return void
		 */
		function save(){
			try {
				$handler = GanetDbHandler::getDbHandler();
  			
				if (0 == $this->mJournalEntryDraftID) {
					$sql = "INSERT INTO tblJournalEntryDraft (`callout`, `cityID`, `countryID`, `description`, `isAutoSaved`, `title`, `travelID`, `logDate`)".
						" VALUES (". 
						"".$handler->makeSqlSafeString($this->mCallout).",".
						"".$handler->makeSqlSafeString($this->mCityID).",".
						"".$handler->makeSqlSafeString($this->mCountryID).",".
						"".$handler->makeSqlSafeString($this->mDescription).",".
						"".$handler->makeSqlSafeString($this->mIsAutoSaved).",".
						"".$handler->makeSqlSafeString($this->mTitle).",".
						"".$handler->makeSqlSafeString($this->mTravelID).",".
						"".$handler->makeSqlSafeString($this->mLogDate)."".									
						")";
				}
				else {
					$sql = "UPDATE tblJournalEntryDraft SET ".
						" `callout` = ".$handler->makeSqlSafeString($this->mCallout).",".
						" `cityID` = ".$handler->makeSqlSafeString($this->mCityID).",".
						" `countryID` = ".$handler->makeSqlSafeString($this->mCountryID).",".
						" `description` = ".$handler->makeSqlSafeString($this->mDescription).",".
						" `isAutoSaved` = ".$handler->makeSqlSafeString($this->mIsAutoSaved).",".
						" `title` = ".$handler->makeSqlSafeString($this->mTitle).",".
						" `travelID` = ".$handler->makeSqlSafeString($this->mTravelID).",".
						" `logDate` = ".$handler->makeSqlSafeString($this->mLogDate)."".	
						" WHERE `journalEntryDraftID` = ".$handler->makeSqlSafeString($this->mJournalEntryDraftID);			
				}
				
				$handler->execute($sql);
				$this->mJournalEntryDraftID = (0 == $this->mJournalEntryDraftID) ? $handler->getLastInsertedID():$this->mJournalEntryDraftID;
			}
			catch(exception $ex){}
		}
	 
		/**
		 * Sets the callout of the journal entry draft.
		 * 
		 * @param string $callout the callout of the journal entry draft.
		 */
		function setCallout($callout){
			$this->mCallout = $callout;
		}	
	 
		/**
		 * Sets the city ID of the journal entry draft.
		 * 
		 * @param integer $cityID the city ID of the journal entry draft.
		 */
		function setCityID($cityID){
			$this->mCityID = $cityID;
		}	
	 
		/**
		 * Sets the country ID of the journal entry draft.
		 * 
		 * @param integer $countryID the country ID of the journal entry draft.
		 */
		function setCountryID($countryID){
			$this->mCountryID = $countryID;
		}	
	 
		/**
		 * Sets the description of the journal entry draft.
		 * 
		 * @param string $description the description of the journal entry draft.
		 */
		function setDescription($description){
			$this->mDescription = $description;
		}	
	 
		/**
		 * Sets the is auto saved of the journal entry draft.
		 * 
		 * @param integer $isAutoSaved the is auto saved of the journal entry draft.
		 */
		function setIsAutoSaved($isAutoSaved){
			$this->mIsAutoSaved = $isAutoSaved;
		}	
	 
		/**
		 * Sets the journal entry draft ID of the journal entry draft.
		 * 
		 * @param integer $journalEntryDraftID the journal entry draft ID of the journal entry draft.
		 */
		function setJournalEntryDraftID($journalEntryDraftID){
			$this->mJournalEntryDraftID = $journalEntryDraftID;
		}
		
		/**
		 * Sets the log date of the journal entry draft.
		 * 
		 * @param string $date the log date of the journal entry draft.
		 * 
		 * @return void
		 */
		function setLogDate($date){
			$this->mLogDate = $date;
		}	
	 
		/**
		 * Sets the title of the journal entry draft.
		 * 
		 * @param string $title the title of the journal entry draft.
		 */
		function setTitle($title){
			$this->mTitle = $title;
		}	
	 
		/**
		 * Sets the travel ID of the journal entry draft.
		 * 
		 * @param integer $travelID the travel ID of the journal entry draft.
		 */
		function setTravelID($travelID){
			$this->mTravelID = $travelID;
		}	
		
	}