<?php
	/**
	 * @(#) Class.BaseJournal.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 28, 2009
	 */
	
	require_once("travellog/model/Class.GanetDbHandler.php");
	
	/**
	 * Base class of journal.
	 */
	abstract class BaseJournal {
	 
		/**
		 * The travel ID of the journal.
		 * 
		 * @var integer 
		 */
		protected $mTravelID = "";
	 
		/**
		 * The title of the journal.
		 * 
		 * @var string 
		 */
		protected $mTitle = "";
	 
		/**
		 * The description of the journal.
		 * 
		 * @var string 
		 */
		protected $mDescription = "";
	 
		/**
		 * The traveler ID of the journal.
		 * 
		 * @var integer 
		 */
		protected $mTravelerID = 0;
	 
		/**
		 * The travellink ID of the journal.
		 * 
		 * @var integer 
		 */
		protected $mTravellinkID = 0;
			  
		/**
		 * Deletes the journal in the database.
		 *
		 * @return void
		 */
		function delete(){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "DELETE FROM tblTravel WHERE `travelID` = ".$handler->makeSqlSafeString($this->mTravelID);
				
				$handler->execute($sql);
			}
			catch(exception $ex){}
		}
		
		/**
		 * Returns the description of the journal.
		 * 
		 * @return string the description of the journal.
		 */
		function getDescription(){
			return $this->mDescription;
		}
	 
		/**
		 * Returns the travel ID of the journal.
		 * 
		 * @return integer the travel ID of the journal.
		 */
		function getTravelID(){
			return $this->mTravelID;
		}	
	 
		/**
		 * Returns the title of the journal.
		 * 
		 * @return string the title of the journal.
		 */
		function getTitle(){
			return $this->mTitle;
		}
	 
		/**
		 * Returns the traveler ID of the journal.
		 * 
		 * @return integer the traveler ID of the journal.
		 */
		function getTravelerID(){
			return $this->mTravelerID;
		}	
	 
		/**
		 * Returns the travellink ID of the journal.
		 * 
		 * @return integer the travellink ID of the journal.
		 */
		function getTravellinkID(){
			return $this->mTravellinkID;
		}	
	  	
		/**
		 * Sets the properties of the journal.
		 * 
		 * @param array $props The new values for the properties of Journal.
		 * 
		 * @return void
		 */
		function initialize(array $props){
			if (0 < count($props)) {
				$this->mTravelID = isset($props['travelID']) ? $props['travelID'] : $this->mTravelID;
				$this->mTitle = isset($props['title']) ? $props['title'] : $this->mTitle;
				$this->mDescription = isset($props['description']) ? $props['description'] : $this->mDescription;
				$this->mTravelerID = isset($props['travelerID']) ? $props['travelerID'] : $this->mTravelerID;
				$this->mTravellinkID = isset($props['travellinkID']) ? $props['travellinkID'] : $this->mTravellinkID;
			}
		}
			
		/**
		 * Inserts/Updates a journal in the database. When the travelID is
		 * equal to 0 this inserts a new entry in the database, otherwise this updates
		 * an existent entry.
		 *
		 * @return void
		 */
		function save(){
			try {
				$handler = GanetDbHandler::getDbHandler();
  			
				if (0 == $this->mTravelID) {
					$sql = "INSERT INTO tblTravel (`title`, `description`, `travelerID`, `travellinkID`)".
						" VALUES (". 
						"".$handler->makeSqlSafeString($this->mTitle).",".
						"".$handler->makeSqlSafeString($this->mDescription).",".
						"".$handler->makeSqlSafeString($this->mTravelerID).",".
						"".$handler->makeSqlSafeString($this->mTravellinkID). 											
						")";
				}
				else {
					$sql = "UPDATE tblTravel SET ".
						" `title` = ".$handler->makeSqlSafeString($this->mTitle).",".
						" `description` = ".$handler->makeSqlSafeString($this->mDescription).",".
						" `travelerID` = ".$handler->makeSqlSafeString($this->mTravelerID).",".
						" `travellinkID` = ".$handler->makeSqlSafeString($this->mTravellinkID).
						" WHERE `travelID` = ".$handler->makeSqlSafeString($this->mTravelID);			
				}
				
				$handler->execute($sql);
				$this->mTravelID = (0 == $this->mTravelID) ? $handler->getLastInsertedID():$this->mTravelID;
			}
			catch(exception $ex){}
		}
		
		/**
		 * Sets the type of owner of this journal(traveler/group).
		 * 
		 * @return void
		 */
		function setOwnerType($type){
			$this->mOwnerType = $type;
		}
	 
		/**
		 * Sets the travel ID of the journal.
		 * 
		 * @param integer $travelID the travel ID of the journal.
		 */
		function setTravelID($travelID){
			$this->mTravelID = $travelID;
		}	
	 
		/**
		 * Sets the title of the journal.
		 * 
		 * @param string $title the title of the journal.
		 */
		function setTitle($title){
			$this->mTitle = $title;
		}	
	 
		/**
		 * Sets the description of the journal.
		 * 
		 * @param string $description the description of the journal.
		 */
		function setDescription($description){
			$this->mDescription = $description;
		}	
	 
		/**
		 * Sets the traveler ID of the journal.
		 * 
		 * @param integer $travelerID the traveler ID of the journal.
		 */
		function setTravelerID($travelerID){
			$this->mTravelerID = $travelerID;
		}	
	 
		/**
		 * Sets the travellink ID of the journal.
		 * 
		 * @param integer $travellinkID the travellink ID of the journal.
		 */
		function setTravellinkID($travellinkID){
			$this->mTravellinkID = $travellinkID;
		}	
		
	}