<?php   
	/**
	 * @(#) Class.BaseDiscussionPeer.php
	 * 
	 * Autogenerated by GoAbroad Code Generator 1.0 
	 *
	 * @version 1.0 Jun 19, 2009 
	 */
	
	require_once('travellog/model/Class.DiscussionBoardDbHandler.php');
	require_once('travellog/model/Class.SqlCriteria2.php');
	require_once('travellog/model/base/Class.GanetBasePeer.php');

	/**
	 * Base class of discussion peer
	 */
	abstract class BaseDiscussionPeer extends GanetBasePeer {
		
		/** The database name of this class **/
		const DATABASE_NAME = 'DiscussionBoard';
		
		/** The table name of this class **/
		const TABLE_NAME = 'tblDiscussions';
		
		/** The primary field of this class **/
		const PRIMARY_KEY = 'ID';
	 
		/** The id of the discussion. **/
		const ID = 'tblDiscussions.ID';
	 
		/** The topic id of the discussion. **/
		const TOPIC_ID = 'tblDiscussions.topicID';
	 
		/** The title of the discussion. **/
		const TITLE = 'tblDiscussions.title';
	 
		/** The status of the discussion. **/
		const STATUS = 'tblDiscussions.status';
	 
		/** The date created of the discussion. **/
		const DATE_CREATED = 'tblDiscussions.dateCreated';
	 
		/** The date updated of the discussion. **/
		const DATE_UPDATED = 'tblDiscussions.dateUpdated';
	 
		/** The added to knowledge base of the discussion. **/
		const ADDED_TO_KNOWLEDGE_BASE = 'tblDiscussions.homepageShown';
	 
		/** The featured of the discussion. **/
		const FEATURED = 'tblDiscussions.isFeatured';
	 
		/**
		 * The instances of discussion.
		 * 
		 * @var array
		 */
		public static $sInstances = array();
		
		/**
		 * holds an array of fieldnames
		 *
		 * first dimension keys are the type constants
		 */
		private static $fieldNames = array(
			GanetBasePeer::TYPE_PEERFIELD => array(self::ID, self::TOPIC_ID, self::TITLE, self::STATUS, self::DATE_CREATED, self::DATE_UPDATED, self::ADDED_TO_KNOWLEDGE_BASE, self::FEATURED),
			GanetBasePeer::TYPE_TABLEFIELD => array('ID', 'topicID', 'title', 'status', 'dateCreated', 'dateUpdated', 'homepageShown', 'isFeatured'),
			GanetBasePeer::TYPE_NUM => array(0, 1, 2, 3, 4, 5, 6, 7)
		);	
	
		/**
		 * Returns the number of discussion.
		 * 
		 * @param <code>SqlCriteria2</code> $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return integer the number of discussion.
		 */
		static function doCount(SqlCriteria2 $criteria = null){
			try {
				$handler = DiscussionBoardDbHandler::getDbHandler();
				
				$criteria2 = (is_null($criteria)) ? new SqlCriteria2() : clone $criteria;
				$criteria2->addSelectColumn('COUNT(*)', 'count');
				$criteria2->addSelectTable(self::TABLE_NAME);
				$sql = $criteria2->createSelectStatement();
				
				$resource = $handler->execute($sql);
				$record = mysql_fetch_assoc($resource);
				
				return $record['count'];
			}
			catch (exception $ex) {}
			
			return 0;
		}

		/**
		 * Deletes the given array of discussion in the database.
		 * 
		 * @param <code>Discussion</code>[] $objects The objects to be deleted in the database.
		 * 
		 * @return boolean true on success and false on failure.
		 */
		static function doDelete(array $objects){
			if (0 < count($objects)) {
				try {
					$handler = DiscussionBoardDbHandler::getDbHandler();
					
					$sql = "DELETE FROM `".self::DATABASE_NAME."`.".self::TABLE_NAME."".
						" WHERE `".self::PRIMARY_KEY."` IN (";
					foreach ($objects as $object) {
						if ($object->exists()) {
							$sql .= $object->getPrimaryKey().", ";
						}
					}
					$sql = preg_replace('/, $/', ')', $sql);
					
					$handler->startTransaction();
					if ($handler->execute($sql)) {
						if (count($objects) == $handler->getAffectedRows()) {
							$handler->commit();
							
							foreach ($objects as $object) {
								if (isset(self::$sInstances[$object->getPrimaryKey()])) {
									unset(self::$sInstances[$object->getPrimaryKey()]);
								}
							}
							
							return true;
						}
					}
					
					$handler->rollback();
				}
				catch (exception $ex) {}
			}
			
			return false;
		}	
		
		
		/**
		* TEMPORARY FIX: use last_insert_id
		***/
		static function doInsertOne($object){
			
			try{
				$dbHandler 	= DiscussionBoardDbHandler::getDbHandler();
				$values 	= $object->toArray(GanetBasePeer::TYPE_TABLEFIELD);

				$arrVals = array();
				foreach( $values as $each ){
					$arrVals[] = $dbHandler->makeSqlSafeString($each);
				}
				$sql = "INSERT INTO `".self::DATABASE_NAME."`.`".self::TABLE_NAME."`".
					" (".implode(',', self::$fieldNames[GanetBasePeer::TYPE_TABLEFIELD]).") VALUES (".implode(", ", $arrVals).")";
				$dbHandler->startTransaction();
				if ($dbHandler->execute($sql)) {
					$dbHandler->commit();

					// get last inserted ID
					$rs = new iRecordset($dbHandler->execute("SELECT LAST_INSERT_ID() as lastInsertedID;"));
					$lastInsertedID = $rs->getlastInsertedID(0);
					
					$object->setPrimaryKey($lastInsertedID);
					self::$sInstances[$lastInsertedID] = $object;
					return true;
				}
				$dbHandler->rollback();
			}
			catch(exception $e){}
			return false;
		}
		
		
		/**
		 * Inserts the given array of discussion in the database.
		 * 
		 * @param <code>Discussion</code>[] $objects The objects to be inserted in the database.
		 * 
		 * @return true on success and false on failure.
		 */
		static function doInsert(array $objects){
			if (0 < count($objects)) {
				try {
					$handler = DiscussionBoardDbHandler::getDbHandler();
					
					// Retrieves the last increment value of the table.
					$sql = "SHOW TABLE STATUS WHERE name = '".self::TABLE_NAME."'";
					$resource = $handler->execute($sql);
					$row = mysql_fetch_assoc($resource);
					$last_increment = $row['Auto_increment'];
					
					$sql = "INSERT INTO `".self::DATABASE_NAME."`.`".self::TABLE_NAME."`".
						" (".implode(',', self::$fieldNames[GanetBasePeer::TYPE_TABLEFIELD]).") VALUES ";
					foreach ($objects as $object) {
						if ($object->exists()) {
							return false;
						}
						else {
							$values = $object->toArray(GanetBasePeer::TYPE_TABLEFIELD);
							$sql .= "(";
							foreach($values as $value){
								$sql .= $handler->makeSqlSafeString($value).', ';
							}
							$sql = preg_replace('/, $/', '', $sql);
							$sql .= "), ";
						}
					}
					$sql = preg_replace('/, $/', '', $sql);
					
					$handler->startTransaction();
					if ($handler->execute($sql)) {
						if (count($objects) == $handler->getAffectedRows()) {
							$handler->commit();
							
							// Update the primary ID of the objects
							foreach ($objects as $object) {
								$object->setPrimaryKey($last_increment);
								self::$sInstances[$last_increment] = $object;
								$last_increment++;
							}
							
							return true;
						}
					}
					
					$handler->rollback();
				}
				catch (exception $ex) {}
			}
			
			return false;
		}
		
		/**
		 * Returns all the discussion that satifies the criteria.
		 * 
		 * @param <code>SqlCriteria2</code> $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return <code>Discussion</code>[] the discussion that satifies the criteria.
		 */
		static function doSelect(SqlCriteria2 $criteria = null){
			require_once("travellog/model/Class.Discussion.php");
			
			$data = array();
			try {
				$handler = DiscussionBoardDbHandler::getDbHandler();

				$criteria2 = (is_null($criteria)) ? new SqlCriteria2() : clone $criteria;
				$criteria2->addSelectTable(self::TABLE_NAME);
				$sql = $criteria2->createSelectStatement();

				$resource = $handler->execute($sql);
				
				while ($row = mysql_fetch_assoc($resource)) {
					if (!isset(self::$sInstances[$row[self::PRIMARY_KEY]])) {
						$obj = new Discussion();
						$obj->fromArray($row);
						self::$sInstances[$obj->getPrimaryKey()] = $obj; 
					}
					$data[] = self::$sInstances[$row[self::PRIMARY_KEY]];
				}			
			}
			catch (exception $ex) {}
			
			return $data;
		}
		
		/**
		 * Returns the discussion that satifies the criteria.
		 * 
		 * @param <code>SqlCriteria2</code> $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return <code>Discussion</code> the discussion that satifies the criteria. If the data does not exists, this returns null.
		 */
		static function doSelectOne(SqlCriteria2 $criteria = null){
			$criteria2 = (is_null($criteria)) ? new SqlCriteria2() : clone $criteria;
			$criteria2->addLimit(1);
			$objects = self::doSelect($criteria);
			return (0 < count($objects)) ? $objects[0] : null;
		}
		
		/**
		 * Updates the given array of discussion in the database.
		 *
		 * @param <code>Discussion</code>[] the given array of <code>Discussion</code>
		 *
		 * @return integer the number of affected rows on success and 0 on failure.
		 */
		static function doUpdate(array $objects){
			if (0 < count($objects)) {
				try {
					$handler = DiscussionBoardDbHandler::getDbHandler();
					
					$sql = "INSERT INTO `".self::DATABASE_NAME."`.`".self::TABLE_NAME."` (".implode(',', self::$fieldNames[GanetBasePeer::TYPE_TABLEFIELD]).") VALUES ";
					foreach ($objects as $object) {
						if ($object->exists()) {
							$values = $object->toArray(GanetBasePeer::TYPE_TABLEFIELD);
							$sql .= "(";
							foreach($values as $value){
								$sql .= $handler->makeSqlSafeString($value).', ';
							}
							$sql = preg_replace('/, $/', '', $sql);
							$sql .= "), ";
						}
						else {
							return false;
						}
					}
					$sql = preg_replace('/, $/', '', $sql);
					$sql .= " ON DUPLICATE KEY UPDATE ";
					foreach (self::$fieldNames[GanetBasePeer::TYPE_TABLEFIELD] as $field) {
						if ($field != self::PRIMARY_KEY) {
							$sql .= " $field = VALUES($field), ";
						}
					}
					$sql = preg_replace('/, $/', '', $sql);
					
					if ($handler->execute($sql)) {
						return true;
					}
				}
				catch (exception $ex) {}
			}
			
			return false;				
		}
		
		/**
		 * Returns the field names of specific type.
		 * 
		 * @return array the field names of specific type.
		 * 
		 * @throws exception Throws exception if $type is not one of the class constants GanetBasePeer::TYPE_TABLEFIELD, GanetBasePeer::TYPE_PEERFIELD, GanetBasePeer::TYPE_PHPFIELD and GanetBasePeer::TYPE_NUM.
		 */
		static function getFieldNames($type = GanetBasePeer::TYPE_TABLEFIELD){
			if (!array_key_exists($type, self::$fieldNames)) {
				throw new exception('Method getFieldNames() expects the parameter $type to be one of the class constants GanetBasePeer::TYPE_TABLEFIELD, GanetBasePeer::TYPE_PEERFIELD, GanetBasePeer::TYPE_PHPFIELD, GanetBasePeer::TYPE_NUM. ' . $type . ' was given.');
			}
			
			return self::$fieldNames[$type];
		}
		
		/**
		 * Returns the discussion with ID equal to the given discussion ID.
		 * 
		 * @param integer $id The id of the discussion to be retrieved.
		 * 
		 * @return Discussion|null the discussion with ID equal to the given discussion ID; null if the ID does not exists.
		 */
		static function retrieveByPk($id){
			if (isset(self::$sInstances[$id])) {
				return self::$sInstances[$id];
			}
						
			$data = self::retrieveByPks(array($id));
			
			return (0 < count($data)) ? $data[0] : null;
		}
		
		/**
		 * Returns the Discussion instances of the given ids.
		 * 
		 * @param integer[] $ids The ids to be fetched its objects.
		 * 
		 * @return Discussion[] The Discussion instances of the given ids.
		 */
		static function retrieveByPks(array $ids){
			$objects = array();
			$to_be_queried = array();
			
			foreach ($ids as $id) {
				if (isset(self::$sInstances[$id])) {
					$objects[] = self::$sInstances[$id];
				}
				else { 
					$to_be_queried[] = $id;
				}
			}
			
			if (0 < count($to_be_queried)) {
				$criteria = new SqlCriteria2();
				$criteria->add(self::TABLE_NAME.'.'.self::PRIMARY_KEY, implode($to_be_queried, ','), SqlCriteria2::IN);
				$data = self::doSelect($criteria);
				$objects = array_merge($objects, $data);
			}
			
			return $objects;
		}
	
	}