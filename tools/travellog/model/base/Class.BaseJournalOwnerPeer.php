<?php
	/**
	 * @(#) Class.BaseJournalOwnerPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 28, 2009
	 */
	
	require_once("travellog/model/Class.GanetDbHandler.php");
	
	/**
	 * Base class of JournalOwner peer
	 */
	abstract class BaseJournalOwnerPeer {
	 
		/** The travellink ID of the journal owner. */
		const TRAVELLINK_ID = "tblTravelLink.travellinkID";
	 
		/** The ref ID of the journal owner. */
		const REF_ID = "tblTravelLink.refID";
	 
		/** The reftype of the journal owner. */
		const REFTYPE = "tblTravelLink.reftype";
		
		/**
		 * Returns the number of journal owner.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return integer the number of journal owner.
		 */
		static function doCount(Criteria2 $criteria){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "SELECT COUNT(*) as count FROM tblTravelLink ";
				$sql .= $criteria->createWhereClause();

				$resource = $handler->execute($sql);
				$record = mysql_fetch_assoc($resource);
				
				return $record['count'];
			}
			catch (exception $ex) {}
			
			return 0;
		}
		
		/**
		 * Returns all the journal owner that satifies the criteria.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return array the journal owner that satifies the criteria.
		 */
		static function doSelect(Criteria2 $criteria){
			require_once("travellog/model/Class.JournalOwner.php");
			
			$data = array();
			try {
				$handler = GanetDbHandler::getDbHandler();

				$sql = "SELECT * FROM tblTravelLink ";
				$sql .= $criteria->createSqlClause();

				$resource = $handler->execute($sql);
				
				while ($row = mysql_fetch_assoc($resource)) {
					$obj = new JournalOwner();
					$obj->initialize($row);
					$data[] = $obj;
				}			
			}
			catch (exception $ex) {}
			
			return $data;
		}
		
		/**
		 * Returns the journal owner with ID equal to the given journal owner ID.
		 * 
		 * @param integer $id The id of the journal owner to be retrieved.
		 * 
		 * @return JournalOwner|null the journal owner with ID equal to the given journal owner ID; null if the ID does not exists.
		 */
		static function retrieveByPk($id){
			require_once("Class.Criteria2.php");
			
			$criteria = new Criteria2();
			$criteria->add(self::TRAVELLINK_ID, $id);
			$data = self::doSelect($criteria);
			
			return (0 < count($data)) ? $data[0] : null;
		}
	
	}