<?php
	/**
	 * Group Peer Base class.
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 27, 2009
	 */
	
	require_once("travellog/model/Class.GanetDbHandler.php");
	require_once("Class.Criteria2.php");
	
	/**
	 * Base class of GroupPeer.
	 */
	abstract class BaseGroupPeer {

		/** The database name of this class **/
		const DATABASE_NAME = 'Travel_Logs';
		
		/** The table name of this class **/
		const TABLE_NAME = 'tblGroup';
		
		/** The primary field of this class **/
		const PRIMARY_KEY = 'groupID';
		
		const GROUP_ID = "tblGroup.groupID";
		const SENDABLE_ID = "tblGroup.sendableID";
		const NAME = "tblGroup.name";
		const DESCRIPTION = "tblGroup.description";
		const GROUP_ACCESS = "tblGroup.groupaccess";
		const IS_SUSPENDED = "tblGroup.isSuspended";
		const PARENT_ID = "tblGroup.parentID";
		const DISCRIMINATOR = "tblGroup.discriminator";
		const ADMINISTRATOR = "tblGroup.administrator";
		const GROUP_RANK = "tblGroup.inverserank";
		const FEATURED_RANK = "tblGroup.featuredsubgrouprank";
		
		/**
		 * The instances of Group
		 * 
		 * @var Group[]
		 */
		public static $sInstances = array();
	
		/**
		 * Returns the number of groups of ganet.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return array the groups of ganet.
		 */
		static function doCount(Criteria2 $criteria){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "SELECT COUNT(*) as count FROM tblGroup ";
				$sql .= $criteria->createWhereClause();

				$resource = $handler->execute($sql);
				$record = mysql_fetch_assoc($resource);
				
				return $record['count'];
			}
			catch (exception $ex) {}
			
			return 0;
		}
		
		/**
		 * Returns the groups of ganet.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return array the groups of ganet.
		 */
		static function doSelect(Criteria2 $criteria){
			static $count = 0;
			$count++;
			
			require_once("travellog/model/Class.GroupFactory.php");
			$groups = array();
			
			try {
				$handler = GanetDbHandler::getDbHandler();
				$factory = GroupFactory::instance();
				
				$sql = "SELECT * FROM tblGroup ";
				$sql .= $criteria->createSqlClause();
				$resource = $handler->execute($sql);
				
				while ($row = mysql_fetch_assoc($resource)) {
					if (!isset(self::$sInstances[$row[self::PRIMARY_KEY]])) {
						$obj = $factory->createGroupObject($row);
						self::$sInstances[$obj->getGroupID()] = $obj; 
					}			
					$groups[] = self::$sInstances[$row[self::PRIMARY_KEY]];
				}			
			}
			catch (exception $ex) {}
			
			return $groups;
		}
		
		/**
		 * Returns the group with ID equal to the given group ID.
		 * 
		 * @param integer $id The id of the group to be retrieved.
		 * 
		 * @return Group|null the Group with ID equal to the given group ID; null if the ID does not exists.
		 */
		static function retrieveByPk($id){
			if (isset(self::$sInstances[$id])) {
				return self::$sInstances[$id];
			}
						
			$data = self::retrieveByPks(array($id));
			
			return (0 < count($data)) ? $data[0] : null;
		}
		
		/**
		 * Returns the Group instances of the given ids.
		 * 
		 * @param integer[] $ids The ids to be fetched its objects.
		 * 
		 * @return Group[] The Group instances of the given ids.
		 */
		static function retrieveByPks(array $ids){
			$objects = array();
			$to_be_queried = array();
			
			foreach ($ids as $id) {
				if (isset(self::$sInstances[$id])) {
					$objects[] = self::$sInstances[$id];
				}
				else { 
					$to_be_queried[] = $id;
				}
			}
			
			if (0 < count($to_be_queried)) {
				$criteria = new Criteria2();
				$criteria->add(self::TABLE_NAME.'.'.self::PRIMARY_KEY, implode($to_be_queried, ','), Criteria2::IN);
				$data = self::doSelect($criteria);
				$objects = array_merge($objects, $data);
			}
			
			return $objects;
		}
	
	}