<?php
	/**
	 * @(#) Class.BaseJournalPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 28, 2009
	 */
	
	require_once("travellog/model/Class.GanetDbHandler.php");
	
	/**
	 * Base class of Journal peer
	 */
	abstract class BaseJournalPeer {
	 
		/** The travel ID of the journal. */
		const TRAVEL_ID = "tblTravel.travelID";
	 
		/** The title of the journal. */
		const TITLE = "tblTravel.title";
	 
		/** The description of the journal. */
		const DESCRIPTION = "tblTravel.description";
	 
		/** The traveler ID of the journal. */
		const TRAVELER_ID = "tblTravel.travelerID";
	 
		/** The travellink ID of the journal. */
		const TRAVELLINK_ID = "tblTravel.travellinkID";
		
		/**
		 * Returns the number of journal.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return integer the number of journal.
		 */
		static function doCount(Criteria2 $criteria){
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				$sql = "SELECT COUNT(*) as count FROM tblTravel ";
				$sql .= $criteria->createWhereClause();

				$resource = $handler->execute($sql);
				$record = mysql_fetch_assoc($resource);
				
				return $record['count'];
			}
			catch (exception $ex) {}
			
			return 0;
		}
		
		/**
		 * Returns all the journal that satifies the criteria.
		 * 
		 * @param Criteria2 $criteria Handles the limit, order by and where clauses.
		 * 
		 * @return array the journal that satifies the criteria.
		 */
		static function doSelect(Criteria2 $criteria){
			require_once("travellog/model/Class.Journal.php");
			
			$data = array();
			try {
				$handler = GanetDbHandler::getDbHandler();

				$sql = "SELECT * FROM tblTravel ";
				$sql .= $criteria->createSqlClause();

				$resource = $handler->execute($sql);
				
				while ($row = mysql_fetch_assoc($resource)) {
					$obj = new Journal();
					$obj->initialize($row);
					$data[] = $obj;
				}			
			}
			catch (exception $ex) {}
			
			return $data;
		}
		
		/**
		 * Returns the journal with ID equal to the given journal ID.
		 * 
		 * @param integer $id The id of the journal to be retrieved.
		 * 
		 * @return Journal|null the journal with ID equal to the given journal ID; null if the ID does not exists.
		 */
		static function retrieveByPk($id){
			require_once("Class.Criteria2.php");
			
			$criteria = new Criteria2();
			$criteria->add(self::TRAVEL_ID, $id);
			$data = self::doSelect($criteria);
			
			return (0 < count($data)) ? $data[0] : null;
		}
	
	}