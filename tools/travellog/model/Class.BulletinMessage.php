<?php
	/**
	 * Created on September 7, 2006
	 * Class.BulletinMessage.php extends Class.Message.php
	 * @author Cheryl Ivy Q. Go
	 * Last Edited on 14 May 2008 -- optimized queries
	 */

	require_once 'travellog/model/Class.Message.php';
	require_once 'travellog/model/Class.MessageSpace.php';
	require_once 'travellog/model/Class.DiscriminatorType.php';

	class BulletinMessage extends Message{		
		/**
		 * Constructor of class BulletinMessage
		 * @param integer $_attributeID
		 * @return Message
		 */

		function BulletinMessage($_attributeId = 0, $_sendableId = 0){

			$this->setSendableId($_sendableId);
			$this->setDiscriminator(DiscriminatorType::$BULLETIN);

			return $this->Message($_attributeId);			
		}
		
	 	/**
	 	 * Function name: Send
	 	 */	 	
	 	function Send(){	 		
			$msgSpace = new MessageSpace();
			$msgSpace-> Store($this);
	 	}
	 	
	 	/***************************** START: SETTERS *************************************/
		function setSource($_src = NULL){			
			$this->mSource = $_src;
		}
	 	function setDestination($_destination = array()){
	 		$this->mDestination = $_destination;
	 	}
		function setSendableId($_sendableId = 0){
			$this->mSendableID = $_sendableId;
		}
		function setBulletinRecipient(){
			$sql = "SELECT DISTINCT tblGroup.sendableID, tblGroup.discriminator, tblGroup.groupID, tblGroup.name " .
					"FROM tblMessageToAttribute, tblGroup " .
					"WHERE tblGroup.sendableID = tblMessageToAttribute.recipientID " .
					"AND tblMessageToAttribute.messageID = " . $this->getMessageId();
			$this->mRs->Execute($sql);

			if (0 < $this->mRs->Recordcount()){
				$mDiscriminator = $this->mRs->Result(0, "discriminator");

				switch($mDiscriminator){
					case GROUP::FUN :
						$groupData = array();
						$groupData["name"] = $this->mRs->Result(0, "name");
						$groupData["groupID"] = $this->mRs->Result(0, "groupID");
						$groupData["sendableID"] = $this->mRs->Result(0, "sendableID");
						$groupData["discriminator"] = $this->mRs2->Result(0, "discriminator");

						$mRecipient = new FunGroup();
						$mRecipient->setFunGroupData($groupData);

		 				break;

		 			case GROUP::ADMIN :
						$groupData = array();
						$groupData["name"] = $this->mRs->Result(0, "name");
						$groupData["groupID"] = $this->mRs->Result(0, "groupID");
						$groupData["sendableID"] = $this->mRs->Result(0, "sendableID");
						$groupData["discriminator"] = $this->mRs2->Result(0, "discriminator");

						$mRecipient = new AdminGroup();
						$mRecipient->setAdminGroupData($groupData);

		 				break;
				}
			}
			else{
				if ($this->mSource instanceof GROUP){
					$sql2 = "SELECT sendableID, discriminator, groupID, name " .
							"FROM tblGroup " .
							"WHERE sendableID = " . $this->mSource->getSendableID();
					$this->mRs2->Execute($sql2);

					$mDiscriminator = $this->mRs2->Result(0, "discriminator");

					switch($mDiscriminator){
						case GROUP::FUN :
							$groupData = array();
							$groupData["name"] = $this->mRs2->Result(0, "name");
							$groupData["groupID"] = $this->mRs2->Result(0, "groupID");
							$groupData["sendableID"] = $this->mRs2->Result(0, "sendableID");
							$groupData["discriminator"] = $this->mRs2->Result(0, "discriminator");

							$mRecipient = new FunGroup();
							$mRecipient->setFunGroupData($groupData);

			 				break;

			 			case GROUP::ADMIN :
							$groupData = array();
							$groupData["name"] = $this->mRs2->Result(0, "name");
							$groupData["groupID"] = $this->mRs2->Result(0, "groupID");
							$groupData["sendableID"] = $this->mRs2->Result(0, "sendableID");
							$groupData["discriminator"] = $this->mRs2->Result(0, "discriminator");

							$mRecipient = new AdminGroup();
							$mRecipient->setAdminGroupData($groupData);

			 				break;
					}
				}
				else{
					$mTravelerId = $this->mSource->getTravelerID();

					$travelerData = array();
					$travelerData["username"] = $this->mRs->Result(0, "username");
					$travelerData["travelerID"] = $this->mRs->Result(0, "travelerID");
					$travelerData["sendableID"] = $this->mRs->Result(0, "sendableID");

					$mRecipient = new Traveler();
					$mRecipient->setTravelerData($travelerData);
				}
			}
			$this->mDestination = array($mRecipient);
		}
		/******************************* END: SETTERS *************************************/

		public static function DeleteAllGroupBM($_sendableId = 0){
			// check if sendableID is of group
			$mFactory = MessageSendableFactory::Instance();
			$mSendable = $mFactory->Create($_sendableId);

			if ($mSendable instanceof Group){
				// If it's an Admin Group, check if it's a SG. For now, only subgroups can be deleted, therefore only PMs of SG will be removed
				// Clubs however can also be deleted, including all of its PMs

				if (($mSendable->getDiscriminator() == Group::ADMIN && $mSendable->isSubGroup()) || $mSendable->getDiscriminator() == Group::FUN){
					try{
						$mConn = new Connection();
						$mRs	= new Recordset($mConn);
						$mRs2	= new Recordset($mConn);
					}
					catch(exception $e){
						throw $e;
					}
					$arrMessageId = array();

					// get messages where sender is the group to be deleted
					$sql = "SELECT DISTINCT messageID FROM tblMessages WHERE discriminator = 2 AND senderID = " . $_sendableId;
					$mRs->Execute($sql);

					if (0 < $mRs->Recordcount()){
						while($Message = mysql_fetch_assoc($mRs->Resultset()))
							$arrMessageId[] = $Message['messageID'];
					}

					// get messages where recipient is the group to be deleted
					$sql2 = "SELECT DISTINCT tblMessages.messageID AS messageID " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 2 " .
							"AND tblMessageToAttribute.recipientID = " . $_sendableId;
					$mRs2->Execute($sql2);

					while($Message = mysql_fetch_assoc($mRs2->Resultset()))
						$arrMessageId[] = $Message['messageID'];

					if (0 < count($arrMessageId)){
						$lstMessageId = implode(",", $arrMessageId);

						// delete messages
						$sql = "DELETE FROM tblMessages WHERE messageID IN (" . $lstMessageId . ")";
						$mRs->Execute($sql);

						$sql2 = "DELETE FROM tblMessageToAttribute WHERE messageID IN (" . $lstMessageId . ")";
						$mRs2->Execute($sql2);
					}
				}
			}
		}
	}
?>