<?php
require_once("Class.Template.php");
require_once("travellog/helper/Class.HelperFactory.php");
require_once("Class.GaDateTime.php");
class EntryTableOfContents{
	private 
	
	$current_pos   = 0, 
	
	$total_records = 0,
	
	$rows_per_page = 5,
	
	$col_entry     = array(); 
		
	function getTableOfContents(){
		$arr_contents['d']   = new GaDateTime();
		$obj_helper_factory  = HelperFactory::instance();
		
		$this->total_records = count($this->col_entry);
		$total_pages         = $this->getTotalPages();
		$current_page        = $this->getCurrentPage();
		$arr_contents['obj_entry_helper']   = $obj_helper_factory->create(HelperType::$JOURNAL_ENTRY);
		$arr_contents['arr_limit']          = $this->getRowsLimit( $current_page );
		$arr_contents['next_no_of_entries'] = $this->getNextNumberOfEntries( $arr_contents['arr_limit'] ); 
		$arr_contents['col_entry']          = $this->col_entry;
		$arr_contents['current_pos']        = $this->current_pos;
		$arr_contents['obj_next_page']      = $this->getNextPage( $current_page );
		$arr_contents['obj_prev_page']      = $this->getPreviousPage( $current_page );
		$arr_contents['obj_first_entry']    = $this->getFirstEntry( $current_page );
		$arr_contents['obj_last_entry']     = $this->getLastEntry( $current_page );	
		
		return $arr_contents; 
		//$obj_template->set( "col_entry"         , $this->col_entry    );
		//$obj_template->set( "current_pos"       , $this->current_pos  );
		
		//$obj_template->set( "obj_next_page"     , $obj_next_page      );
		//$obj_template->set( "obj_prev_page"     , $obj_prev_page      );
		
		//$obj_template->set( "obj_first_entry"   , $obj_first_entry    );
		//$obj_template->set( "obj_last_entry"    , $obj_last_entry     );
		//$obj_template->set( "obj_entry_helper"  , $obj_entry_helper   );
		//$obj_template->set( "arr_limit"         , $arr_limit          );
		//$obj_template->set( "d"                 , $d                  );
		//$obj_template->set( "next_no_of_entries", $next_no_of_entries );
		
		
		//$obj_next_entry      = $this->getNextEntry();
		//$obj_prev_entry      = $this->getPreviousEntry();
		//$obj_template->set( "obj_next_entry" , $obj_next_entry   );
		//$obj_template->set( "obj_prev_entry" , $obj_prev_entry   );
		
	}
	
	private function getNextNumberOfEntries( $arr_limit ){
		if( ($arr_limit['rows']+($this->rows_per_page-1)) < $this->total_records ){
			return 5;
		}else{
			return $this->total_records-$arr_limit['rows'];
		}
	}
	
	private function getRowsLimit( $current_page ){
		$arr           = array();
		$arr['offset'] = ($current_page*$this->rows_per_page)-$this->rows_per_page;
		$arr['rows']   = $arr['offset']+($this->rows_per_page);
		if( $arr['rows'] > $this->total_records ) $arr['rows'] = $this->total_records; 
		return $arr;
	}
	
	private function getNextPage( $current_page ){
		return ( $current_page < $this->getTotalPages() )? $this->col_entry[(($current_page+1)*$this->rows_per_page)-$this->rows_per_page]: NULL;
	}
	
	private function getPreviousPage( $current_page ){
		return ( $current_page > 1 )? $this->col_entry[(($current_page-1)*$this->rows_per_page)-$this->rows_per_page]: NULL;
	}
	
	private function getTotalPages(){
		return ceil( $this->total_records/$this->rows_per_page );
	}
	
	private function getCurrentPage(){
		return ceil( ($this->current_pos+1)/$this->rows_per_page );
	}
	
	private function getFirstEntry( $current_page ){
		return ( $current_page > 1 )? $this->col_entry[0]: NULL; 
	}
	
	private function getLastEntry( $current_page ){
		return ( $current_page < $this->getTotalPages() )?$this->col_entry[count($this->col_entry)-1]: NULL; 
	}
	
	private function getNextEntry(){
		return ( ($this->current_pos+1) > 1 &&  ($this->current_pos+1) < count( $this->col_entry ) )? $this->col_entry[$this->current_pos+1]: NULL;
	}
	
	private function getPreviousEntry(){
		return ( ($this->current_pos+1) > 1 )? $this->col_entry[$this->current_pos+1]: NULL;
	} 
	
	
	
	function setEntryCollections( $col_entry ){
		$this->col_entry = $col_entry;
	}
	
	function setCurrentPosition( $current_pos ){
		$this->current_pos = $current_pos;
	}
}
?>
