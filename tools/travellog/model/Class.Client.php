<?

	/**
	*	Class.Client.php
	*	class for listing's client information
	*	@author: marc
	*	Sep 14, 2006
	*/
	
	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
	
	class Client{
	
		// db var
		private $nConn;
		private $nRecordset;
			
		// client information
		private $clientID = 0;
		private $instname = '';
		private $address = array();
		private $city = '';
		private $state = '';
		private $zip = '';
		private $countryID = 0;
		private $phone = '';
		private $fax = '';
		private $logo = '';
		private $verified = 0;
	
		
		/**
		* constructor
		*/
			function client($clientID){
			
				try{
					$this->nConn = new Connection();
					$this->nRecordset = new Recordset($this->nConn);
				}catch(Exception $e){
					die('Error in creating connection - ' . $e);
				}
				
				$this->setClientID($clientID);
				
				$sqlquery = "SELECT instname,address1,address2,address3,city,state,yearbegan,
						zip,country,phone,fax,logoimage,logoapproved,clientlevelID
						FROM GoAbroad_Main.tbcountry,GoAbroad_Main.tbclient
						WHERE GoAbroad_Main.tbcountry.countryID = GoAbroad_Main.tbclient.countryID
						AND clientID = $clientID";
				$myquery = $this->nRecordset->Execute($sqlquery);
				if( $row = mysql_fetch_array($myquery) ){
					$this->setInstName($row['instname']);
					$this->setAddress($row['address1'],$row['address2'],$row['address3']);
					$this->setCity($row['city']);
					$this->setState($row['state']);
					$this->setZip($row['zip']);
					$this->setCountry($row['country']);
					$this->setPhone($row['phone']);
					$this->setFax($row['fax']);
					$this->setVerified($row['clientlevelID']);
					$this->setLogo($row['logoapproved'],$row['logoimage']);
				}
				
				return $this;
			}
		
		/**
		*	setters
		*/		
		
		function setClientID($clientID){
			$this->clientID = $clientID;
		}
		
		function setInstName($instname){
			$this->instname = $instname;
		}
		
		function setAddress($address1,$address2,$address3){
			$arrAddress = array();
			if( strlen(trim($address1)) )
				$arrAddress[] = trim($address1);
			if( strlen(trim($address2)) )
				$arrAddress[] = trim($address2);
			if( strlen(trim($address3)) )
				$arrAddress[] = trim($address3);	
			$this->address = $arrAddress;
		}
		
		function setCity($city){
			$this->city = $city;
		}
		
		function setState($state){
			$this->state = $state;
		}
		
		function setZip($zip){
			$this->zip;
		}
		
		function setCountry($country){
			$this->country = $country;
		}
		
		
		function setPhone($phone){
			$this->phone = $phone;
		}
		
		function setFax($fax){
			$this->fax = $fax;
		}
		
		function setLogo($logoapproved,$logo){
			if( $logoapproved > 0 )
				$this->logo = $logo;
		}
		
		function setVerified($verified){
			$this->verified = $verified;	
		}
		
			
		/**
		*	getters
		*/	
		
		function getClientID(){
			return $this->clientID;
		}
		
		function getInstName(){
			return $this->instname;
		}
		
		function getAddress(){
			return $this->address;
		}
			
		function getCity(){
			return $this->city;
		}
				
		function getState(){
			return $this->state;
		}
		
		function getZip(){
			return $this->zip;
		}
		
		function getCountry(){
			return $this->country;
		}
		
		function getPhone(){
			return $this->phone;
		}
		
		function getFax(){
			return $this->fax;
		}
		
		function getLogo(){
			return $this->logo;
		}
		
		function getVerified(){
			return $this->verified;
		}

		public static function getGroupByClientID($clientID = 0){
			try {
	 			$mConn	= new Connection();
				$mRs	= new Recordset($mConn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}

			$sql = "SELECT groupID FROM tblGrouptoAdvisor " .
	 				"WHERE clientID = '$clientID' ";
	 		$mRs->Execute($sql);

	 		if (0 < $mRs->Recordcount()){
		 		while ($mRecordset = mysql_fetch_assoc($mRs->Resultset())) {
	 				try {
 						$mGroup = GroupFactory::instance()->create(array($mRecordset['groupID']));
 					}
					catch (Exception $e){
					   throw $e;
					}
 				}
				return $mGroup[0];
			}
		}		
	}
?>