<?php
/*
 * Created on Aug 7, 2006
 *  FilterCriteria2.php  - class to hold the limiting criteria of retrieving an array of objects
 */
 
 
 require_once("Class.Condition.php");
 
 class FilterCriteria2 {
 	private static $hashSeed		=	'success';		// seed to use when hashing
 	private static $hashMethod 	=	'md5';			// algorithm to use when hashing	
 	
 	private	$conditions = array();	
 	private $booleanOps = array();		
 	
 	private $orderBy = '';
 	private $order 	= 'ASC';
 	private $startRow = -1;
 	private $rowsPerPage = -1;	
 	
 	public function addCondition(Condition $condition){
 		$this->conditions[] = $condition;
 	}	 
 	
 	public function addBooleanOp($op) {
 		if ($op == "AND" || $op == "OR")			// we could add more boolean op keywords here if necessary like XOR	 		
	 		$this->booleanOps[] = $op;
 	}
 	
 	
 	public function getConditions() {return $this->conditions;}
 	
 	public function getBooleanOps() {return $this->booleanOps;}
 	
 	// returns true if the conditions and ops entered is malformed (ex. two conditions w/o an operation)
 	public function isMalformed() {
 		if (count($this->conditions) == 0 && count($this->booleanOps == 0))
 			return true;		
 		
		if (count($this->conditions) == count($this->booleanOps) - 1 )
			return false;
		else
			return true;	 			
 			
 	} 
 	
 	
 	/*****************************************
 	 * new convenience functions for 4/15/09
 	 * 	 	kgordo
 	 */
 	
 	// equal Condition
 	public function add(string $attr,$value){
 		$con = new Condition();
 		$con->setAttributeName($attr);
 		$con->setOperation(FilterOp::$EQUAL);
 		$con->setValue($value);	
 		$this->conditions[]	=	$con;
 	}
 	
 	// get hash value to use as key in caching ids related to this criteria
 	//     ex. Group->getMembers();
 	// NOTE: > the hash value will depend on the order of conditions 
 	//		 > aside from conditions , the orderby is added to the string to be hashed
 	public function getHash(){
		$strValueOfConditions = '';
 		foreach($this->conditions as $condition){
			$strValueOfConditions .= $condition->getAttributeName() . $condition->getOperation() . $condition->getValue();
		} 
		return hash('md5','FilterCriteria2' . $strValueOfConditions . $this->orderBy . $this->order . $this->rowsPerPage . $this->startRow);		
 	}
 	
 	// returns a Condition object given an attribute name, if not found returns null
 	public function getConditionWithAttribute($attr){
 		foreach ($this->conditions as $condition){
 			if ($condition->getAttributeName() == $attr){
 				return $condition;
 			}
 		}
 		return null;
 	}
 	// returns a Condition object given an operation name, if not found returns null
 	public function getConditionWithOperation($oper){
 		foreach ($this->conditions as $condition){
 			if ($condition->getOperation() == $oper){
 				return $condition;
 			}
 		}
 		return null;
 	}
 	
 	public function setOrderBy($attr,$order='ASC'){
 		$this->orderBy 	=	$attr;
 		$this->order	=	 $order;
 	}
 	
 	public function getOrderBy(){
 		//echo '<br/>order by---' . '' . $this->orderBy . ' ' . $this->order;
 		if ($this->orderBy == '')
 			return '';
 		else 
 			return '' . $this->orderBy . ' ' . $this->order; 	
 	}
 	
 	public function setStartRow($row){
 		$this->startRow = $row;
 	}
 	
 	public function getStartRow(){
 		return $this->startRow;
 	}
 	
 	public function setRowsPerPage($rows){
 		$this->rowsPerPage	=	$rows;
 	}

 	public function getRowsPerPage(){	
 		return $this->rowsPerPage;
 	}
 	
 }
 
?>
