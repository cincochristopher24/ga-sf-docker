<?php
require_once("travellog/dao/Class.TravelTips.php");
class EntryTips{
	
	function Get( $tipID ){
		require_once("travellog/vo/Class.TravelTipsVO.php");
		$obj_travel_tips    = new TravelTips;
		$obj_travel_tips_vo = new TravelTipsVO;

		$obj_travel_tips_vo->setTravelTipID( $tipID );
		$obj_travel_tips->Get( $obj_travel_tips_vo );
		
		return $obj_travel_tips_vo; 
	}
	
	function GetJSON( $tipID ){
		require_once('JSON.php');
		$json           = new Services_JSON(SERVICES_JSON_IN_ARR);
		$arr_contents   = array();
		$obj_model      = $this->Get( $tipID );
		$arr_contents[] = array( 'desc'=> $obj_model->getDescription(), 'primaryID' => $obj_model->getTravelTipID() );
		$contents       = $json->encode( $arr_contents );
		return $contents; 
	}
	
	
	function GetList( $travellogID ){
		require_once("Class.Criteria2.php");
		$obj_travel_tips = new TravelTips;
		$obj_criteria    = new Criteria2;
 		$obj_criteria->mustBeEqual( "travellogID", $travellogID );
 		$obj_criteria->setOrderBy( "travelTipID DESC" );
 		return $obj_travel_tips->GetList( $obj_criteria );
	}
	
	function GetListJSON( $travellogID ){
		require_once('JSON.php');
		$json         = new Services_JSON(SERVICES_JSON_IN_ARR);
		$arr_contents = array();
		$arr_entry    = $this->GetList( $travellogID );
		if( count($arr_entry) ){
			foreach( $arr_entry as $obj_entry ){
				$arr_contents[] = array( 'desc'=> nl2br($obj_entry->getDescription()), 'primaryID' => $obj_entry->getTravelTipID(), 'travellogID' => $obj_entry->getTravelLogID() );					
			}
		}
		$contents = $json->encode( $arr_contents );
		return $contents;
	}
	
	function Save( $arr_vars ){
		require_once("travellog/vo/Class.TravelTipsVO.php");
		$obj_travel_tips    = new TravelTips;
		$obj_travel_tips_vo = new TravelTipsVO;
		
		$arr_vars['desc'] = preg_replace("#&lt;(.|\n)*?&gt;#","",strip_tags($arr_vars['desc'],'<p><b><strong><i><em><u><span>'));
		$obj_travel_tips_vo->setTravelLogID( $arr_vars['tID']  );
		$obj_travel_tips_vo->setDescription( $arr_vars['desc'] );
		
		$obj_travel_tips->Save( $obj_travel_tips_vo );
	}
	
	function Update( $arr_vars ){
		require_once("travellog/vo/Class.TravelTipsVO.php");
		$obj_travel_tips    = new TravelTips;
		$obj_travel_tips_vo = new TravelTipsVO;
		
		$arr_vars['desc'] = preg_replace("#&lt;(.|\n)*?&gt;#","",strip_tags($arr_vars['desc'],'<p><b><strong><i><em><u><span>'));
		$obj_travel_tips_vo->setTravelTipID( $arr_vars['tID']   );
		$obj_travel_tips_vo->setDescription( $arr_vars['desc']  );
		
		$obj_travel_tips->Update( $obj_travel_tips_vo );
		
	}
	
	function Delete( $arr_vars ){
		require_once("Class.Criteria2.php");
		$obj_travel_tips = new TravelTips;
		$c               = new Criteria2;
		$c->mustBeEqual( 'travelTipID', $arr_vars['tID'] );
		
		$obj_travel_tips->Delete( $c );
	}
}
?>
