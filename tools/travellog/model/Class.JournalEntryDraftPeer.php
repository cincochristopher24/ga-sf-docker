<?php
	/**
	 * @(#) Class.JournalEntryDraftPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 24, 2009
	 */
	
	require_once('travellog/model/base/Class.BaseJournalEntryDraftPeer.php');
	require_once('Class.Criteria2.php');
	
	/**
	 * Model peer class for journal entry draft.
	 */
	class JournalEntryDraftPeer extends BaseJournalEntryDraftPeer {
		
		/**
		 * Returns the number of journal entry draft of a given owner ID.
		 * 
		 * @param integer   $refID    The ID of the traveler|group to be get the journal entry.
		 * @param integer   $refType  The type whether group/traveler the owner of the journals to be retrieved.
		 * 
		 * @return integer the number of journal entry draft of a given owner ID.
		 */
		static function retrieveJournalEntryDraftsCount($refID, $refType){
			$count = 0;
			
			if (is_numeric($refID) AND is_numeric($refType)) {
				require_once('travellog/model/Class.JournalEntryDraft.php');
				
				try {
					$handler = GanetDbHandler::getDbHandler();
					$criteria2 = new Criteria2();
					$criteria2->add('tblTravelLink.refID', $refID);
					$criteria2->add('tblTravelLink.reftype', $refType);
					$criteria2->add('tblTravel.travellinkID', 'tblTravelLink.travellinkID');
					$criteria2->add('tblTravel.travelID', 'tblJournalEntryDraft.travelID');
					$criteria2->add('tblTravel.deleted', 0);
					
					$sql = 'SELECT count(tblJournalEntryDraft.journalEntryDraftID) as count ' .
						' FROM tblJournalEntryDraft, tblTravel, tblTravelLink ' .
						''.$criteria2->createSqlClause();

					$resource = $handler->execute($sql);
					$record = mysql_fetch_assoc($resource);	
					$count = $record['count'];
				}
				catch (exception $ex) {}
			}
			
			return $count;
		}		
		
		/**
		 * Returns all the journals that has journal draft entry of a given traveler|group.
		 * 
		 * @param integer   $refID    The ID of the traveler|group to be get the journal entry.
		 * @param integer   $refType  The type whether group/traveler the owner of the journals to be retrieved.
		 * @param Criteria2  $criteria The criteria that will handle the limit, where and order by clause.
		 * 
		 * @return array all the journals that has journal draft entry.
		 */
		static function retrieveJournalsWithDraftsEntry($refID, $refType, Criteria2 $criteria){
			$journals = array();
			
			if (is_numeric($refID) AND is_numeric($refType)) {
				require_once('travellog/model/Class.Journal.php');
				require_once('travellog/model/Class.JournalEntryDraft.php');
				
				try {
					$handler = GanetDbHandler::getDbHandler();
					$criteria2 = clone $criteria;
					$criteria2->add('tblTravelLink.refID', $refID);
					$criteria2->add('tblTravelLink.reftype', $refType);
					$criteria2->add('tblTravel.travellinkID', 'tblTravelLink.travellinkID');
					$criteria2->add('tblTravel.travelID', 'tblJournalEntryDraft.travelID');
					$criteria2->add('tblTravel.deleted', 0);
					$criteria2->addOrderByColumn('tblJournalEntryDraft.datecreated', false);
					
					$sql = 'SELECT tblJournalEntryDraft.journalEntryDraftID, tblJournalEntryDraft.countryID, tblJournalEntryDraft.cityID, tblJournalEntryDraft.title, ' .
						' tblJournalEntryDraft.description, tblJournalEntryDraft.callout, tblJournalEntryDraft.logDate, tblJournalEntryDraft.isAutoSaved, tblJournalEntryDraft.datecreated, tblTravel.travelID, ' .
						' tblTravel.title as journal_title, tblTravel.lastupdated ' .
						' FROM tblJournalEntryDraft, tblTravel, tblTravelLink ' .
						''.$criteria2->createSqlClause();
					$resource = $handler->execute($sql);
					while ($record = mysql_fetch_assoc($resource)) {
						if (!isset($journals[$record['travelID']])) {
							$journal = new Journal();
							$journal->setTravelID($record['travelID']);
							$journal->setTitle($record['journal_title']);
							$journals[$journal->getTravelID()] = $journal;
						}
						
						$draft = new JournalEntryDraft();
						$draft->initialize($record);
						$journals[$record['travelID']]->addJournalEntryDraft($draft);
					}
				}
				catch (exception $ex) {}
			}
			
			return $journals;
		}
		
		/**
		 * Returns the journal entry autosave draft if it exists and null if not.
		 * 
		 * @param integer   $refID    The ID of the traveler|group to be get the journal entry.
		 * @param integer   $refType  The type whether group/traveler the owner of the journals to be retrieved.
		 * 
		 * @return JournalEntryDraft|null the journal entry autosave draft if there exists and null if not.
		 */
		static function retrieveAutoSavedDraftEntry($refID, $refType){
			$drafts = array();
			
			if (is_numeric($refID) AND is_numeric($refType)) {
				require_once('travellog/model/Class.JournalEntryDraft.php');
				
				try {
					$handler = GanetDbHandler::getDbHandler();
					$criteria2 = new Criteria2();
					$criteria2->add('tblTravelLink.refID', $refID);
					$criteria2->add('tblTravelLink.reftype', $refType);
					$criteria2->add('tblTravel.travellinkID', 'tblTravelLink.travellinkID');
					$criteria2->add('tblTravel.travelID', 'tblJournalEntryDraft.travelID');
					$criteria2->add('tblTravel.deleted', 0);
					$criteria2->add('tblJournalEntryDraft.isAutoSaved', '1');
					$criteria2->addOrderByColumn('tblJournalEntryDraft.datecreated', false);
					
					$sql = 'SELECT tblJournalEntryDraft.* ' .
						' FROM tblJournalEntryDraft, tblTravel, tblTravelLink ' .
						''.$criteria2->createSqlClause();

					$resource = $handler->execute($sql);
					while ($record = mysql_fetch_assoc($resource)) {
						$draft = new JournalEntryDraft();
						$draft->initialize($record);
						
						$drafts[] = $draft;
					}
				}
				catch (exception $ex) {}
			}
			
			return $drafts;
		}
		
		/**
		 * Returns the number of journal entry drafts of the given owner.
		 * 
		 * @param integer   $refID    The ID of the traveler|group to be get the journal entry.
		 * @param integer   $refType  The type whether group/traveler the owner of the journals to be retrieved.
		 * @param Criteria2  $criteria The criteria that will handle the limit, where and order by clause.
		 * 
		 * @return integer the number of journal entry drafts of the given owner.
		 */
		static function retrieveDraftEntryCount($refID, $refType, Criteria2 $criteria = null){
			if (is_numeric($refID) AND is_numeric($refType)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					$criteria2 = (is_null($criteria)) ? new Criteria2() : clone $criteria;
					$criteria2->add('tblTravelLink.refID', $refID);
					$criteria2->add('tblTravelLink.reftype', $refType);
					$criteria2->add('tblTravel.travellinkID', 'tblTravelLink.travellinkID');
					$criteria2->add('tblTravel.travelID', 'tblJournalEntryDraft.travelID');
					$criteria2->add('tblTravel.deleted', 0);
					$criteria2->addOrderByColumn('tblJournalEntryDraft.datecreated', false);
					
					$sql = 'SELECT COUNT(*) as count ' .
						' FROM tblJournalEntryDraft, tblTravel, tblTravelLink ' .
						''.$criteria2->createWhereClause();

					$resource = $handler->execute($sql);
					$record = mysql_fetch_assoc($resource);
					
					return $record['count'];
				}
				catch (exception $ex) {}
			}
			
			return 0;
		}
		
		/**
		 * Returns the number of server-saved drafts of a journal.
		 * 
		 * @param integer   $travelID    The ID of the traveler|group to be get the journal entry.
		 * 
		 * @return integer the number of journal entry drafts of journal.
		 * @author Augustianne Laurenne L. Barreta
		 */
		static function retrieveUnfinishedEntriesByTravelIDCount($travelID){

			$c = new Criteria2();
			$c->add(JournalEntryDraftPeer::TRAVEL_ID, $travelID);
			$c->add(JournalEntryDraftPeer::IS_AUTO_SAVED, 1);

			return JournalEntryDraftPeer::doCount($c);
		}
		
		/**
		 * Returns the number of drafts of a journal.
		 * 
		 * @param integer   $travelID    The ID of the traveler|group to be get the journal entry.
		 * 
		 * @return integer the number of journal entry drafts of journal.
		 * @author Augustianne Laurenne L. Barreta
		 */
		static function retrieveDraftsByTravelIDCount($travelID){

			$c = new Criteria2();
			$c->add(JournalEntryDraftPeer::TRAVEL_ID, $travelID);
			$c->add(JournalEntryDraftPeer::IS_AUTO_SAVED, 0);

			return JournalEntryDraftPeer::doCount($c);
		}
	}
	
