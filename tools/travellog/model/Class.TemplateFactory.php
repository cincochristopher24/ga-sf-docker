<?php
class TemplateFactory{
	static $instance = NULL;
	
	static function instance(){
		if( self::$instance == NULL ) self::$instance = new TemplateFactory;
		return self::$instance;
	}
	
	function create($template_type){
		switch( $template_type ){
			case TemplateType::$EMPHASIZE_ENTRY_PH0TO:
				require_once("travellog/views/Class.TravelPhotoView.php");
				return;
			break;
			
			case TemplateType::$EMPHASIZE_ENTRY:
				require_once("travellog/views/Class.TravelHighlightView.php");
				return new TravelHighlightView;
			break;
		}
	}
}
?>
