<?php
require_once("travellog/dao/Class.TravelHighlights.php");
class EntryHighlights{
	
	function Get( $highlightID ){
		require_once("travellog/vo/Class.TravelHighlightsVO.php");
		require_once("travellog/model/Class.TravelLog.php");
		require_once("travellog/model/Class.Photo.php");
		$obj_travel_highlights    = new TravelHighlights;
		$obj_travel_highlights_vo = new TravelHighlightsVO;
		
		$obj_travel_highlights_vo->setTravelHighlightID( $highlightID );
		$obj_travel_highlights->Get( $obj_travel_highlights_vo );
		
		$obj_entry = new TravelLog($obj_travel_highlights_vo->getTravelLogID());
		$obj_photo = new Photo( $obj_entry, $obj_travel_highlights_vo->getPhotoID() );
		
		$obj_travel_highlights_vo->setPhoto( $obj_photo->getPhotoLink('thumbnail') );
		
		return $obj_travel_highlights_vo; 
	}
	
	function GetJSON( $highlightID ){
		require_once('JSON.php');
		$json           = new Services_JSON(SERVICES_JSON_IN_ARR);
		$arr_contents   = array();
		$obj_model      = $this->Get( $highlightID );
		$arr_contents[] = array( 'desc'=> $obj_model->getDescription(), 'primaryID' => $obj_model->getTravelHighlightID(), 'photoID' => $obj_model->getPhotoID(), 'photo' => $obj_model->getPhoto());
		$contents       = $json->encode( $arr_contents );
		return $contents; 
	}
	
	function GetList( $travellogID ){
		require_once("Class.Criteria2.php");
		require_once("travellog/vo/Class.TravelHighlightsVO.php"); 
		require_once("travellog/model/Class.TravelLog.php");
		require_once("travellog/model/Class.Photo.php");
		$arr                   = array();
		$obj_travel_highlights = new TravelHighlights;
		$obj_criteria          = new Criteria2;
				
 		$obj_criteria->mustBeEqual( "travellogID", $travellogID ); 
 		$obj_criteria->setOrderBy( "travelHighlightID DESC" );
 		$rows =  $obj_travel_highlights->GetList( $obj_criteria );
 		
 		if( $rows != NULL ){
 			while( $row = mysql_fetch_assoc($rows) ){
 				$objNewMember = new	TravelHighlightsVO;
 				$obj_entry    = new TravelLog( $row["travellogID"] );
 				$obj_photo    = new Photo( $obj_entry, $row["photoID"] ); 
				$objNewMember->setTravelLogID      ( $row["travellogID"]                        );	
				$objNewMember->setDescription      ( $row["description"]                        );
				$objNewMember->setTravelHighlightID( $row["travelHighlightID"]                  ); 
				$objNewMember->setPhotoID          ( $row["photoID"]                            ); 
				$objNewMember->setPhoto            ( $obj_photo->getPhotoLink('thumbnail') );
				$arr[] = $objNewMember; 
 			}
 		}
 		
 		return $arr; 
	}
	
	function GetListJSON( $travellogID ){
		require_once('JSON.php');
		$json         = new Services_JSON(SERVICES_JSON_IN_ARR);
		$arr_contents = array();
		$arr_entry    = $this->GetList( $travellogID );
		if( count($arr_entry) ){
			foreach( $arr_entry as $obj_entry ){ 
				$arr_contents[] = array( 'desc'=> nl2br($obj_entry->getDescription()), 'primaryID' => $obj_entry->getTravelHighlightID(), 'travellogID' => $obj_entry->getTravelLogID(), 'photoID' => $obj_entry->getPhotoID(), 'photo' => $obj_entry->getPhoto());					
			}
		}
		$contents = $json->encode( $arr_contents );
		return $contents;
	}
	
	function Save( $arr_vars ){
		require_once("travellog/vo/Class.TravelHighlightsVO.php");
		$obj_travel_highlights    = new TravelHighlights;
		$obj_travel_highlights_vo = new TravelHighlightsVO;
		
		$arr_vars['desc'] = preg_replace("#&lt;(.|\n)*?&gt;#","",strip_tags($arr_vars['desc'],'<p><b><strong><i><em><u><span>'));
		$obj_travel_highlights_vo->setTravelLogID( $arr_vars['tID']     );
		$obj_travel_highlights_vo->setDescription( $arr_vars['desc']    );
		$obj_travel_highlights_vo->setPhotoID    ( $arr_vars['photoID'] );
		
		$obj_travel_highlights->Save( $obj_travel_highlights_vo );
	}
	
	function Update( $arr_vars ){
		require_once("travellog/vo/Class.TravelHighlightsVO.php");
		$obj_travel_highlights    = new TravelHighlights;
		$obj_travel_highlights_vo = new TravelHighlightsVO;
		
		$arr_vars['desc'] = preg_replace("#&lt;(.|\n)*?&gt;#","",strip_tags($arr_vars['desc'],'<p><b><strong><i><em><u><span>'));
		$obj_travel_highlights_vo->setTravelHighlightID( $arr_vars['hID']     );
		$obj_travel_highlights_vo->setDescription      ( $arr_vars['desc']    );
		$obj_travel_highlights_vo->setPhotoID          ( $arr_vars['photoID'] );
		
		$obj_travel_highlights->Update( $obj_travel_highlights_vo );
		
	}
	
	function Delete( $arr_vars ){
		require_once("travellog/vo/Class.TravelHighlightsVO.php");
		require_once("Class.Criteria2.php");
		$obj_travel_highlights = new TravelHighlights;
		$c                     = new Criteria2;
		$c->mustBeEqual( 'travelHighlightID', $arr_vars['hID'] );
		
		$obj_travel_highlights->Delete( $c );
	}
}
?>
