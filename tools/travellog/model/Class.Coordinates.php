<?php
	/**
	 * Created on Aug 3, 2006
	 * Author: Kerwin Gordo
	 * Purpose: Represents the map coordinates of a location
	 *
	 * Last Edited By	: Cheryl Ivy Q. Go
	 * Purpose			: Class Coordinates has been modified to adjust with the new requirements of GA.net.
	 * 						Users can add new Cities together with its corresponding coordinates to the 
	 * 						database which will be verified by the day crew.
	 */
	 
	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	 
	 class Coordinates {
	 	
	 	private $rs;
	 	private $conn;

	 	private $x = 0;
	 	private $y = 0;
	 	private $exists = false;
	 	private $locationId = 0;	 	
	 	
	 	function Coordinates($locationId = 0) {
	 		try{
	 			$this-> conn = new Connection();
	 			$this-> rs   = new Recordset($this->conn);
	 		}
	 		catch(exception $e){
	 			throw $e;
	 		}
			
	 		$this-> locationId = $locationId;

	 		$sql = "select * from tblCoordinate where locID = '" . $locationId . "'";
	 		$this-> rs-> Execute($sql);

			if (0 < $this->rs->Recordcount()){
		 		$this-> exists = true;
		 		$this-> x = $this->rs->Result(0, "long");
		 		$this-> y = $this->rs->Result(0, "lat");
			}
	 	}

	 	function setX($x = 0) {
	 		$this-> x = $x;
	 	}
	 	
	 	function getX() {
	 		return $this-> x;
	 	}
	 	
	 	function setY($y = 0) {
	 		$this-> y = $y;
	 	}
	 	
	 	function getY() {
	 		return $this-> y;
	 	}
	 	
	 	function isExist(){
	 		return $this-> exists;
	 	}
	 	
	 	/*
	 	 * CRUD
	 	 */
	 	
	 	function save(){
			if (!$this->exists){
	 			$sql = "insert into tblCoordinate (locID, lat, `long`) values (" . $this->locationId . ", '" . $this->y . "', '" . $this->x . "')";
	 			$this-> rs-> Execute($sql);
			}
			else
	 			$this-> update();
	 	}
	 	
	 	function update(){
			if ($this->exists){
		 		$sql = "update tblCoordinate set locID = " . $this->locationId . ", lat = '" . $this->y . "', `long` = '" . $this->x . "' where locID = " . $this->locationId;
		 		$this-> rs-> Execute($sql);
			}
	 	}
	 	
	 	function delete(){
	 		if (0 == $this->locationId)
	 			throw new Exception("cannot delete because locationid is zero");
	 		
	 		$sql = "delete from tblCoordinate where locID = " . $this->locationId;
	 		$this-> rs-> Execute($sql);	
	 	}	 
	 }
?>