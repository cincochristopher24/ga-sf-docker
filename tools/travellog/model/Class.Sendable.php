<?php

/**
 * Created on September 7, 2006
 * Class.Sendable.php 
 * @author Cheryl Ivy Q. Go
 */
	
	interface Sendable{
	
		function sendMessage(Message $_message = NULL);		
		function getName();
		function getSendableID();

	}
?>