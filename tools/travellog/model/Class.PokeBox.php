<?php
	/**
	 * Created on 07 August 2007
	 * Class.PokeBox.php
	 * @author Cheryl Ivy Q. Go
	 */

	if(!function_exists('__autoload')){
		 function __autoload($class_name){
		 	require_once "Class.". $class_name . '.php';
		 }
	}

	class PokeBox extends Poke{
		protected $mRs			= NULL;
		protected $mRs2			= NULL;
		protected $conn			= NULL;
		private $pokeCount		= 0;
		private $pokeReceived 	= array();
		
		function PokeBox($_sendableID = 0){
			try{
				$this-> conn = new Connection();
				$this-> mRs  = new Recordset($this->conn);
				$this-> mRs2 = new Recordset($this->conn);
			}
			catch(exception $e){
				throw $e;
			}
			$this-> recipientID = $_sendableID;
		}
		
		/**
		 * Getters
		 */
		function hasAddedPoke(){
			return $this-> hasAdded();
		}

		function getPokeCount(){
			return $this-> pokeCount;
		}

		function getPokesReceived($_filterCriteria2 = NULL, $_rowsLimit = NULL){
			if ($_rowsLimit){
				$row = $_rowsLimit->getRows();
				$offset = $_rowsLimit->getOffset();
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}
			else
				$queryLimit = " ";

			if ($_filterCriteria2){
				$myconditions = $_filterCriteria2-> getConditions();
				$condition = '';

				foreach($myconditions as $indCondition){
					$attname = $indCondition-> getAttributeName();
					$operation = $indCondition-> getOperation();
					$value = $indCondition-> getValue();

					$condition = $condition . " AND ";
					if ($operation == FilterOp::$EQUAL)
						$condition = $condition . $attname . " = " . " '$value' ";
					else if ($operation == FilterOp::$LIKE)
						$condition = $condition . $attname . " = " . " '$value' ";
				}
			}
			else{
				// get admin groups of traveler (if advisor) -- needs edit
				$sql = "select a.groupID from tblGrouptoAdvisor as a, tblTraveler as b where b.recipientID = " . $this->recipientID . " and a.travelerID = b.travelerID";
				$this-> rs-> Execute($sql);
				
				// get subgroups of administered group(s)

				if (0 < $this->recipientID)
					$condition = " AND recipientID = " . $this-> recipientID;
				else
					$condition = "";
			}

			$sql = "select SQL_CALC_FOUND_ROWS distinct pokeID from tblPoke where 1 = 1 " . $condition . " order by dateCreated desc " . $queryLimit;
			$this-> mRs-> Execute($sql);
			
			$sql2 = "select FOUND_ROWS() as totalRecords";
			$this-> mRs2-> Execute($sql2);

			$this-> pokeCount =  $this-> mRs2-> Result(0, "totalRecords");

			while($poke = mysql_fetch_array($this->mRs->Resultset()))
				$this-> pokeReceived[] = new Poke($poke['pokeID']);

			return $this-> pokeReceived;
		}

		/**
		 * CRUDE
		 */
		function Send(Poke $_Poke = NULL){
			if (!Poke::isExist($_Poke->pokeID)){				
				$sql = "insert into tblPoke (pokeType, senderID, recipientID, section, genID) values (" . $_Poke-> pokeType . ", " . $_Poke-> senderID . ", " . $_Poke-> recipientID .", '" . $_Poke-> section . "', " . $_Poke-> genID . ")";
				$this-> mRs-> Execute($sql);
			}
		}

		function Delete($_pokeID = 0){
			if (Poke::isExist($_pokeID)){
				$sql = "delete from tblPoke where pokeID = " . $_pokeID;
				$this-> mRs-> Execute($sql);

				$this-> pokeID = 0;
				$this-> pokeType = 0;
				$this-> senderID = 0;
				$this-> recipientID = 0;
				$this-> dateCreated = '';
				$this-> section = 0;
				$this-> genID = 0;
			}
		}
	}
?>