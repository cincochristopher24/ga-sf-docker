<?php
/**
 * Created on Jul 27, 2006
 * @package travellog.model	
 * @author Kerwin Gordo
 * Purpose: A subclass of Location and represents a Country
 */
 
 require_once("gaexception/Class.InstantiationException.php");
 require_once("Class.Connection.php");
 require_once("Class.Recordset.php");
 require_once('travellog/model/Class.LocationFactory.php');
 require_once('travellog/model/Class.Location.php');
 require_once('travellog/model/Class.MainRegion.php');
 require_once('travellog/model/Class.Region.php');
 require_once('travellog/model/Class.Continent.php');
 require_once('travellog/model/Class.CountryInfo.php');
 
 require_once('Cache/ganetCacheProvider.php');
 require_once('Class.ConnectionProvider.php');
 
 
 class Country extends Location
 {
 	protected $countryID = 0;
 	protected $possessiveName = '';
 	private $continentID = 0;
 	
 	private $continent;			// for continent object
 	protected $cities;			// array of cities
 	private $states;			// array of states
 	private $regions;			// array of regions
 	private $mainRegions;		// array of mainregions(goabroad popular regions)
 	private $otherlocs;		    // array of non-pop locations
 	private $countryInfo;		// Country Info object
 	
 	// added by Jonas Tandinco Feb/26/2008
 	private $regionID = NULL;
 	
 	private $unapprovedCities = array();
 	private $cache	=	null;
 	
 	/**
 	 * constructor for Country using the country id
 	 */
 	function Country($countryID = 0)
 	{
 		
 		if ($countryID > 0) {
	 		$rs = mysql_query("select * from GoAbroad_Main.tbcountry where countryID='" . $countryID . "' and approved=1");

	 		if (mysql_num_rows($rs) == 0) {
	 			// id not valid so throw exception
	 			throw new InstantiationException("country id " . $countryID . " is not a valid ID");	
	 		}
	 			
	 		
	 		$rsar = mysql_fetch_array($rs);
	 		$this->setName($rsar['country']);
	 		$this->setLocationID($rsar['locID']);
	 		$this->countryID = $rsar['countryID'];
	 		$this->continentID = $rsar['continentID'];
	 		$this->possessiveName = $rsar['possessive'];
 		} 		
 		
 	}
 	
 	/**
 	 * Initializes the values of the properties of the country.
 	 * 
 	 * @return void
 	 */
 	function initialize(array $props){
 		if (0 < count($props)) {
	 		$this->setName((isset($props['country'])) ? $props['country'] : "");
	 		$this->setLocationID((isset($props['locID'])) ? $props['locID'] : 0);
	 		$this->countryID = (isset($props['countryID'])) ? $props['countryID'] : 0;
	 		$this->continentID = (isset($props['continentID'])) ? $props['continentID'] : 0;
	 		$this->possessiveName = (isset($props['possessive'])) ? $props['possessive'] : "";			
 		}
 	}
 	
 	function getCountryID() {return $this->countryID;}
 	
 	function setCountryID($countryID) {$this->countryID = $countryID;}
 	
 	function getPossessiveName(){ return $this->possessiveName; }
 	
 	function getContinentID() {return $this->continentID;}
 	
 	function setContinentID($continentID) {$this->continentID = $continentID;}
 	 	
 	
 	
 	// overrides
 	
 	  	function getCountries() {return null;}
 	  	
 	  	function getStates($travelerID=0){
			if( $this->states == NULL ){
				/*
				$sql1  = '(SELECT locID,state FROM GoAbroad_Main.tbstate) ';
				$sql2  = '';
				$order = ' ORDER BY state';
				if( $travelerID ){ 
					$sql2 = 'UNION (SELECT tblTravelerToCity.locID,city as state FROM tblNewCity, tblTravelerToCity ' .
							'WHERE tblTravelerToCity.locID = tblNewCity.locID ' .
							'AND countryID                 = 91 '.
							'AND travelerID                = '.$travelerID.')';
				}
				$results = mysql_query( $sql1 . $sql2 . $order );
				$this->states = array();
				while( $row = mysql_fetch_assoc($results) ){
					$location = LocationFactory::instance();
					$this->states[] = $location->create($row['locID']); 
				}
				*/
				
				
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
		 		
		 		$sql1  = '(SELECT locID,state FROM GoAbroad_Main.tbstate) ';
				$sql2  = '';
				$order = ' ORDER BY state';
				if( $travelerID ){ 
					$sql2 = 'UNION (SELECT tblTravelerToCity.locID,city as state FROM tblNewCity, tblTravelerToCity ' .
							'WHERE tblTravelerToCity.locID = tblNewCity.locID ' .
							'AND countryID                 = 91 '.
							'AND travelerID                = '.$travelerID.')';
				}
				$rs->Execute($sql1 . $sql2 . $order );
				
	 			$this->states = array();
	 			while ($row = mysql_fetch_array($rs->Resultset())){
	 				$location = LocationFactory::instance();
	 				$this->states[] = $location-> create($row['locID']);
	 			}
				
			}
			return $this->states;
		}
 	  	
 	  	// edited by: Cheryl Ivy Q. Go  07 June 2007
 	  	// Modified By: Aldwin S. Sabornido
 		function getCities($_travID = 0, $isGetState = true) {
 			//if ($this->cities == null) {
	 			if( $this->countryID == 91 && $isGetState){
	 				$this->cities = $this->getStates($_travID);
	 			}else{
		 			// edited by : Cheryl Ivy Q. Go  4 June 2007  6:00PM
		 			/*
		 			$rs = mysql_query(
		 							  "(select a.locID, a.city from tblNewCity as a, tblTravelerToCity as b where b.locID = a.locID and " .
									  "b.travelerID = " . $_travID . " and a.countryID = " . $this->countryID . ") " .
									  "union " .
									  "(select locID, city from GoAbroad_Main.tbcity where countryID = " . $this->countryID . " and approved = 1) order by city" 
									 );
		 			$this->cities = array();
		 			while ($rsar = mysql_fetch_array($rs)){
		 				$location = LocationFactory::instance();
		 				$this->cities[] = $location-> create($rsar['locID']);
		 			}
		 			*/
		 			
					$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
			 		
			 		$sql = "(select a.locID, a.city from tblNewCity as a, tblTravelerToCity as b where b.locID = a.locID and a.locID <> 0 and " .
									  "b.travelerID = " . $_travID . " and a.countryID = " . $this->countryID . ") " .
									  "union " .
									  "(select locID, city from GoAbroad_Main.tbcity where countryID = " . $this->countryID . " and approved = 1 and locID <> 0) order by city" 
									 ;
					$rs->Execute($sql);
					
		 			$this->cities = array();
		 			while ($rsar = mysql_fetch_array($rs->Resultset())){
		 				$location = LocationFactory::instance();
		 				$this->cities[] = $location-> create($rsar['locID']);
		 			}
	 			}
 			//}	
 			return $this->cities;
 		}
 		function getRegions() {
 			if ($this->regions == null) {
	 			$rs = mysql_query("select * from tblRegion where countryID='" . $this->countryID . "' order by name");
	 			
	 			$this->regions = array();
	 			while ($rsar =  mysql_fetch_array($rs)) {
	 				$region = new Region();	
	 				$region->setName($rsar['name']);
	 				$region->setRegionID($rsar['regionID']);
	 				$region->setCountryID($rsar['countryID']);
	 				$region->setContinentID($rsar['continentID']);
	 				$this->regions[] = $region;					
	 			}
 			}	
 			return $this->regions;
 			
 		}
 		
 		// Added by Jonas Tandinco Feb/26/2008
 		public function getRegionID() {
 			if (is_null($this->regionID)) {
 				$conn = new Connection ( 'GoAbroad_Main' );
 				$rs   = new Recordset  ( $conn           );
 				
 				$query = "SELECT regionID " .
 					     "FROM tbcountry " .
						 "WHERE countryID = {$this->countryID} " .
						 "LIMIT 1";
 					     
 				$rs->Execute($query);
 				
 				if ($rs->Recordcount() > 0) {
 					$row = mysql_fetch_row($rs->Resultset());
 					
 					$this->regionID = $row[0];
 				} else {
 					throw new Exception("regionID for countryID ({$this->country}) not found");
 				}
 			}
 			
 			return $this->regionID;
 		}
 		
 		function getNonPopLocations() {
 			
 		}
 		
 	
 		function getRegion() {return null;}
 		
 		/**
 		 * returns the GoAbroad popular regions
 		 */
 		function getMainRegions() {
 			if (NULL == $this->mainRegions) {
 				$this->mainRegions = array();
 				
 				$rs = mysql_query("select mainregionID from GoAbroad_Main.tbmainregion where countryID='$this->countryID' order by mainregion");
 			
 				if (0 < mysql_num_rows($rs)) {
 					while ($rsar = mysql_fetch_array($rs)) {
 						
 						try {
 							$this->mainRegions[] = new MainRegion($rsar['mainregionID']);
 							
 						} catch (InstantiationException $ia){
 								
 						}
 							 						
 					}
 					
 				} 
 			}
 			return $this->mainRegions;
 		}
 		
 		function getCountry() {return $this;}
 		function getContinent() {
 			if ($this->continent == null) {
	 			$rs = mysql_query("select * from tblContinent where continentID='" . $this->continentID . "'");
	 			$rsar = mysql_fetch_array($rs);
	 				 			
	 			$continent = new Continent();
	 			$continent->setLocationID($rsar['locID']);
	 			$continent->setName($rsar['name']);
	 			$continent->setContinentID($rsar['continentID']);
 			}	
 			return $continent;
 		}
 		
 		
 		function getCountryInfo() {
 			 			
 			if ($this->countryInfo == null) {
 				$ci = new CountryInfo();
 				
 				$rs = mysql_query("SELECT capital,area,population,money FROM GoAbroad_Main.tbguidestocountryinfo,GoAbroad_Main.tbguides " . 
 					"WHERE GoAbroad_Main.tbguidestocountryinfo.guideID = GoAbroad_Main.tbguides.guideID " . 
 					"AND countryID ='" . $this->countryID . "' " . 
 					"AND approved = 1");
	 			$rsar = mysql_fetch_array($rs);
	 			if ( count($rsar) > 0 ){
	 				$ci->setCapital($rsar['capital']);
	 				$ci->setLandArea($rsar['area']);
	 				$ci->setPopulation($rsar['population']);
	 				$ci->setCurrency($rsar['money']);
	 			}
	 			else
	 			{	$ci->setCapital('');
	 				$ci->setLandArea('');
	 				$ci->setPopulation('');
	 				$ci->setCurrency('');
	 			}
	 			
	 			$rs = mysql_query("SELECT descr FROM GoAbroad_Main.tbguidestocategory,GoAbroad_Main.tbguides,GoAbroad_Main.tbguidestocountry " . 
	 				"WHERE GoAbroad_Main.tbguidestocategory.guideID = GoAbroad_Main.tbguides.guideID " . 
	 				"AND GoAbroad_Main.tbguidestocountry.guideID = GoAbroad_Main.tbguides.guideID " . 
	 				"AND countryID = '" . $this->countryID . "' " .
	 				"AND approved = 1 " . 
	 				"AND categoryID = 21");
	 			$rsar = mysql_fetch_array($rs);
	 			
 				if ( count($rsar) > 0 )
 					$ci->setCountryDescription($rsar['descr']);
 				else
 					$ci->setCountryDescription('');	
 				 				
 				$this->countryInfo = $ci; 			
 			} 			 		
 			return $this->countryInfo;
 		}	
 		
 		// ************************* Added By: Cheryl Ivy Q. Go   21 May 2007 ***************************** //

		static function getCountriesWithUnapprovedCity(){
			require_once 'Class.Recordset.php';

			try{
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "select a.countryID, a.country from GoAbroad_Main.tbcountry as a, tblNewCity as b where a.countryID = b.countryID and " .
				"a.countryID <> 104 and a.countryID not in (14, 27, 812, 77, 95, 65, 219) group by a.countryID order by country";
			$rs->Execute($sql);

			$arrCountry = array();
			while ($country = mysql_fetch_array($rs->Resultset())){
				$arrCountry[] = new Country($country['countryID']);
			}

			return $arrCountry;
		}
	
 		static function getUnapprovedCitiesInCountry($_locID = 0) {
 			require_once 'Class.Recordset.php';

			try{
				$rs   = new Recordset(ConnectionProvider::instance()->getConnection());
			}
			catch(exception $e){
				throw $e;
			}

 			$sql = "select a.locID, a.city from tblNewCity as a, GoAbroad_Main.tbcountry as b where a.countryID = b.countryID and b.locID = " . $_locID . " order by city";
 			$rs-> Execute($sql);

 			$arrCity = array(); 			
 			$location = LocationFactory::instance();
 			while ($city = mysql_fetch_array($rs->Resultset())){
 				$arrCity[] = $location-> create($city['locID']);
 			}

 			return $arrCity;
 		}

 		// ************************* Added By: Cheryl Ivy Q. Go   21 May 2007 ***************************** //

 		/*
 		 * CRUD + list
 		 */ 
 		/***
 		 * Edited By Naldz: Oct. 3, 2007
 		 * Added kosovo (240) in the NOT in list
		 * Edited By Ianne: Sept 17, 2009	
		 * Removed England, Wales, Scotland and N. Ireland (27,95,77,65) from NOT in list
 		 */
 		static function getCountryList()    // tested 
 		{
 			$con = ConnectionProvider::instance()->getConnection();
 			
 			$rs = mysql_query("select * from GoAbroad_Main.tbcountry where approved=1 and countryID <> 104 AND countryID NOT IN (14,812,219,240) order by country");
	 		
	 		$countries = array();
	 		
	 		
	 		while ($rsar = mysql_fetch_array($rs)) {
	 			$country = new Country();
	 			$country->setLocationID($rsar['locID']);
		 		$country->setContinentID($rsar['continentID']);
		 		$country->setCountryID($rsar['countryID']);
		 		$country->setName($rsar['country']);
		 		$countries[] = $country;
	 		}
	 		
 			
 			return $countries;
 		} 
 		
 		
 		function save()  // tested
 		{
 			if ($this->countryID > 0 || $this->getLocationID() > 0 ) {
 				throw new Exception("Cannot save!, countryID and locationID should not have been set");
 			}
 			
 			if (count($this->getName()) == 0) {
 				throw new Exception("Cannot save!, country should have a name");
 			}
 			
 			
 			// insert a row first in tblLocation
 			mysql_query("insert into tblLocation (locationtypeID) values('" . LocationType::$COUNTRY . "')" );
 			
 			$this->setLocationID(mysql_insert_id());
 			 			
 			// insert a row in tbcountry
 			mysql_query("insert into GoAbroad_Main.tbcountry (continentID,country,locID,approved) values ('" . $this->continentID . "','" . $this->getName() . "','" . $this->getLocationID() .   "',1)"  );
 			
 			$this->setCountryID(mysql_insert_id());
 			
 		}
 		
 		function update()    // tested
 		{
 			if ($this->countryID == 0 || $this->continentID == 0 ) {
 				throw new Exception("Cannot update! either countryId or continentId is not set");
 			}
 			
 			
 			
 			mysql_query("update GoAbroad_Main.tbcountry set country='" . $this->getName() . "',continentID='" . $this->continentID . "' where countryID='" . $this->countryID . "'"  );
 			 			
 			
 		}
 		
 		function delete() // tested
 		{
 			// delete record in tblLocation
 			mysql_query("delete from tblLocation where locID='" . $this->getLocationID() . "'");
 			 			
 			// delete record in tbcountry
 			mysql_query("delete from GoAbroad_Main.tbcountry where locID='" . $this->getLocationID() . "'");
 			
 			$this->setLocationID(0);
 			$this->setName("");
 			$this->setCountryID(0);
 			$this->setContinentID(0);
 		}
 		
 		/********************************************/	 	
 		
 		static function getGacomCountryList()    // tested 
 		{
 			$con = ConnectionProvider::instance()->getConnection();
 			
 			$rs = mysql_query("select * from GoAbroad_Main.tbcountry where approved=1 AND countryID NOT IN (104,14,812,219,240) order by country");
	 		
	 		$countries = array();
	 		
	 		
	 		while ($rsar = mysql_fetch_array($rs)) {
	 			$country = new Country();
	 			$country->setLocationID($rsar['locID']);
		 		$country->setContinentID($rsar['continentID']);
		 		$country->setCountryID($rsar['countryID']);
		 		$country->setName($rsar['country']);
		 		$countries[] = $country;
	 		}
 			
 			return $countries;
 		}
 }
  
?>
