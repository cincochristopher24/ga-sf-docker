<?php
	/********************************/
	/*	@author Cheryl Ivy Q. Go	*/
	/*	Created On 28 April 2008	*/
	/*	Extends class Messages		*/
	/********************************/

	require_once 'travellog/model/messageCenter/Class.gaMessage.php';
	require_once 'travellog/model/Class.DiscriminatorType.php';

	class SpamMessages extends gaMessage{
		protected $mRs	= null;
		protected $mConn	= null;

		private $mIsIncludedInNotif;

		function SpamMessages(){
			try{
 				$this->mConn  = new Connection();
				$this->mRs    = new Recordset($this->mConn);
			}
			
			catch (exception $e){				   
			   throw $e;
			}

			$this->setDiscriminator(DiscriminatorType::$PERSONAL);
	 		//return $this-> gaMessage();
		}

		/**
	 	 * Function name: Send
	 	 */
	 	
	 	function Send(){}

		/**
		 * Function name: setSource
		 * @param object $_src
		 */
		 		 
		function setSource($_src = NULL){
			$this->mSource = $_src;
		}

		/**
		 * Function name: setDestination
		 * @param array $_dest
		 */
		 
		function setDestination($_dest = array()){
			$this->mDestination = $_dest;
		}
		
		function getDestination(){
			return $this->mDestination;
		}
		
		function setIsIncludedInNotif($v = true){
			$this->mIsIncludedInNotif = $v;
		}
		
		function isIncludedInNotif(){
			return $this->mIsIncludedInNotif;
		}
		
		function getText(){
			return $this->message;
		}
		
		function getSubject(){
			return $this->title;
		}
		
		function getBody(){
			return $this->message;
		}
		
		function getSender(){
			if( !is_null($this->sender) )
				return $this->sender;
			else if (!is_null($this->getSenderID())) {
				require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
				
				return gaTravelerMapper::getTraveler($this->getSenderID());
			}
			else
				return null;
		}
		
		function getRecipient(){
			if( !is_null($this->recipient) )
				return $this->recipient;
			else if (!is_null($this->getRecipientID())) {
				require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
				
				return gaTravelerMapper::getTraveler($this->getRecipientID());
			}
			else
				return null;
		}
		
		function getRecipients(){
			if( !is_null($this->recipients) )
				return $this->recipients;
			else if (!is_null($this->getRecipientIDs())) {
				require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
				
				$arr = array();
				foreach($this->getRecipientIDs() as $id){
					$arr[] = gaTravelerMapper::getTraveler($this->getRecipientID()); 
				}
				return $arr;
			}
			else
				return null;
		}
		
		//TODO: A spam message can have lots of recipients
		function getSpamMessage(){
			$sql = "SELECT " .
					"tblPersonalMessage.dateCreated, " .
					"tblPersonalMessage.subject, " .
					"tblPersonalMessage.body, " .
					"tblPersonalMessage.isMessageIncludedInNotif " .
					"FROM " .
					"tblPersonalMessage, tblPersonalMessageRecipients " .
					"WHERE tblPersonalMessage.id = tblPersonalMessageRecipients.messageID " .
					"AND tblPersonalMessageRecipients.isSpam = 1 " .
					"AND tblPersonalMessage.isDeleted = 0 " .
					"AND tblPersonalMessage.id = " . $this->getID() . " " .
					"AND tblPersonalMessage.senderID = " . $this->getSenderID() . " " .
					"AND tblPersonalMessageRecipients.recipientID = " . $this->getRecipientID();
			
			$this->mRs->Execute($sql);
			
			$temp = null;
			while($message = mysql_fetch_assoc($this->mRs->Resultset())){
				$this->setTitle(stripslashes($message['subject']));
				$this->setMessage(stripslashes($message['body']));
				$this->setDateCreated($message['dateCreated']);
				$this->setIsIncludedInNotif($message['isMessageIncludedInNotif']);
			}
			return $this;
		}

		function Approve(){
			$sql = "UPDATE tblPersonalMessageRecipients SET isSpam = 0 WHERE messageID = " . $this->getID() .
					" AND recipientID = " . $this->getRecipientID();
			
			$this->mRs->Execute($sql);
			
			$cache = ganetCacheProvider::instance()->getCache();
			if (!is_null($cache))
				$cache->delete('ganet_pmcopy_' . $this->getSenderID());
		}

		function Delete(){
			
			gaPersonalMessageDM::deleteCopy($this->getRecipientID(), $this->getID());
			
		}
	}
?>