<?php
/*
	Filename:		Class.TravelDestinationPeer.php
	Author:			Jonas Tandinco
	Date created:	Sep/05/2007
	Purpose:		contains functions for TravelDestination CRUD
	
	TO DO:	

		2.	place all connection initializations in try blocks
		3.	handle concurrency problems that might occur in the future

	EDIT HISTORY:

	Sep/07/2007 by Jonas Tandinco
	
		1.	modified add(), edit() to make sure strings and dates are quoted, also dates with no values are given the word null in MYSQL
		2.	renamed getTravelDestination to getTravelDestinatios and modified it to retrieve the destinations for a particular travel
		3.	renamed delete to deleteDestinations and modified it to delete all the destinations from a travel
		
	Oct/01/2007 by Jonas Tandinco
	
		1.	merged startMonth, startDay, startYear to startDate
		2.	merged endMonth, endDay, endYear to endDate
		3.	applied mysql_real_escape_string() together with sprintf() to prevent SQL injections
		
	Oct/08/2007 by Aldwin Sabornido
	
		1.	added getTravelDestination() to retrieve a single travel destination
		
	Oct/08/2007 by Jonas Tandinco
	
		1.	optimized code
		
	Oct/10/2007 by Aldwin Sabornido

		1.  modified destinationExists() function because it always returns true
		
	Oct/31/2007 by Jonas Tandinco
	
		1.	modified queries to reflect to the new structure of the model
*/

require_once ('Class.Connection.php');
require_once ('Class.Recordset.php');

class TravelDestinationPeer {
	// add
	public static function add($travelDestination) {
  		$conn = new Connection('SocialApplication');
	  	$rs   = new Recordset($conn);
	  	
		$query = "INSERT INTO tbTravelWishToCountry " .
				 "(travelWishID, countryID, description, startDate, endDate) " .
				 "VALUES ({$travelDestination->getTravelWishID()},
						  {$travelDestination->getCountry()->getCountryID()},
						  '%s',
						  '{$travelDestination->getStartDate()}',
						  '{$travelDestination->getEndDate()}'
				 )";
		$query = sprintf($query, mysql_real_escape_string($travelDestination->getDescription()));

		$rs->Execute($query);
	}
	
	// edit
	public static function  edit($travelDestination) {
  		$conn = new Connection('SocialApplication');
	  	$rs   = new Recordset($conn);

		$query = "UPDATE tbTravelWishToCountry " .
				 "SET travelWishID = {$travelDestination->getTravelWishID()},
					  countryID	   = {$travelDestination->getCountry()->getCountryID()},
					  description  = '%s',
					  startDate    = '{$travelDestination->getStartDate()}',
					  endDate      = '{$travelDestination->getEndDate()}'
				 ";
		$query .= " WHERE travelWishToCountryID = {$travelDestination->getTravelWishToCountryID()}";
		$query = sprintf($query, mysql_real_escape_string($travelDestination->getDescription()));
		$rs->Execute($query);
	}
	
	// removes destinations from a travel
	public static function deleteDestinations($travelWishID) {
  		$conn = new Connection('SocialApplication');
	  	$rs   = new Recordset($conn);
	  	
	  	$query = "DELETE FROM tbTravelWishToCountry " .
	  			 "WHERE travelWishID = $travelWishID";
	  			 
	  	$rs->Execute($query);
	  	
	  	// TODO: update dates of travel plan
	}
	
	// retrieve destinations for a particular travel
	public static function getTravelDestinations($travelWishID, $travelPlan) {
  		$conn = new Connection('SocialApplication');
	  	$rs   = new Recordset($conn);

		$query = "SELECT *, country " .
				 "FROM tbTravelWishToCountry, GoAbroad_Main.tbcountry " .
				 "WHERE travelWishID = $travelWishID " .
				 "AND tbTravelWishToCountry.countryID = tbcountry.countryID " .
				 "ORDER BY travelWishToCountryID";
		
		$rs->Execute($query);
		
		$arrDestinations = array();
		
		if ($rs->Recordcount()) {
			while ($destination = mysql_fetch_assoc($rs->Resultset())) {
				$travelDestination = new TravelDestination();
				
				$travelDestination->setTravelWishToCountryID($destination['travelWishToCountryID']);
				$travelDestination->setTravelWishID($destination['travelWishID']);

				// populate country object
				$country = new Country($destination['countryID']);
				$travelDestination->setCountry($country);
				
				$travelDestination->setDescription($destination['description']);
				$travelDestination->setStartDate($destination['startDate']);
				$travelDestination->setEndDate($destination['endDate']);
				
				$travelDestination->setTravelPlan($travelPlan);
				
				$arrDestinations[] = $travelDestination;
			}
		}
		
		return $arrDestinations;
	}
	
	// check to see if travel destination exist
	public static function destinationExists($travelWishID, $countryID) {
  		$conn = new Connection('SocialApplication');
	  	$rs   = new Recordset($conn);
		 
		$sql = "SELECT travelWishID " .
			   "FROM tbTravelWishToCountry " .
			   "WHERE travelWishID = $travelWishID " .
			   "AND countryID = $countryID";
		
		$rs->Execute($sql);
		
		//$result = mysql_fetch_assoc($rs->Resultset());
		
		if ($rs->Recordcount()) {
			return true;
		}

		return false; 
	}
	
	// delete a travel destination given the countryID and travelWishID
	public static function deleteDestination($travelWishID, $countryID) {
  		$conn = new Connection('SocialApplication');
	  	$rs   = new Recordset($conn);

		$sql = "DELETE FROM tbTravelWishToCountry " .
			   "WHERE travelWishID = $travelWishID " .
			   "AND countryID = $countryID";
		
		$rs->Execute($sql);		
	}
	
	// added by Aldwin Sabornido Oct/08/2007
	// get a single travel destination based on travelwishID and countryID
	public static function getTravelDestination($travelWishID, $countryID){
		$conn = new Connection('SocialApplication');
	  	$rs   = new Recordset($conn);

		$travelDestination = new TravelDestination();

		$sql = "SELECT * FROM tbTravelWishToCountry " .
			   "WHERE travelWishID = $travelWishID " .
			   "AND countryID = $countryID";
		
		$rs->Execute($sql);
				
		if( $rs->Recordcount() ){
			
			$travelDestination->setTravelWishToCountryID( $rs->Result(0 ,'travelWishToCountryID') );
			$travelDestination->setTravelWishID         ( $rs->Result(0 ,'travelWishID')          );

			// populate country object
			$country = new Country($rs->Result(0 ,'countryID'));
			$travelDestination->setCountry($country);

			$travelDestination->setDescription          ( $rs->Result(0 ,'description')           );
			$travelDestination->setStartDate            ( $rs->Result(0 ,'startDate')             );
			$travelDestination->setEndDate              ( $rs->Result(0 ,'endDate')               );
		}

		return 	$travelDestination; 
	}
}