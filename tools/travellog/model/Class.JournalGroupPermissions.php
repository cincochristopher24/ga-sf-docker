<?php
	
	require_once('Class.DataModel.php');
	require_once('travellog/model/Class.JournalGroupPermissionsPeer.php');
	
	class JournalGroupPermissions extends DataModel{
		
		const DENY 	= 0;
		const ALLOW = 1;
		
		public function __construct($ID = null, $data = null){
			parent::initialize('Travel_Logs', 'tblJournalGroupPermissions');
			if($ID and !$this->load($ID))
				throw new exception("Invalid ID{$ID} passed to " . __CLASS__ . " constructor.");
			if(is_array($data))
				$this->setFields($data);
		}
		
		public function setData($data){
			if(is_array($data)){
				$this->setFields($data);
			}
		}
		
		public function exists(){
			return JournalGroupPermissionsPeer::entryAlreadyExists($this->travelID, $this->groupID);
		}
		
		static function applyPermissions($permitted=array(), $notpermitted=array(), $travelID=0){
			foreach($permitted as $groupID){
				$jgp = JournalGroupPermissionsPeer::retrieveByTravelAndGroup($travelID, $groupID);
				if(!$jgp){
					$jgp = new JournalGroupPermissions;
					$jgp->setTravelID($travelID);
					$jgp->setGroupID($groupID);
				}
				$jgp->setPermission(self::ALLOW);

				$jgp->save();
			}

			foreach($notpermitted as $notgroupID){
				$jgp = JournalGroupPermissionsPeer::retrieveByTravelAndGroup($travelID, $notgroupID);
				if(!$jgp){
					$jgp = new JournalGroupPermissions;
					$jgp->setTravelID($travelID);
					$jgp->setGroupID($notgroupID);
				}
				$jgp->setPermission(self::DENY);

				$jgp->save();
			}
		}
		
	}