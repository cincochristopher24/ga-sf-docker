<?php
/*
 * Created on Aug 7, 2006
 *
 */
 
 class RowsLimit {
 	
 	private $rows;
 	private $offset;
 	
 	function RowsLimit($rows,$offset = 0 ) {
 		$this->rows = $rows;
 		$this->offset = $offset;
 	}
 	
 	
 	function getRows() {return $this->rows;}
 	
 	function getOffset() {return $this->offset;}
 	
 	/**
 	 * creates the sql LIMIT clause based on the given rows and offset
 	 * 
 	 * @return string
 	 */
 	function createSqlLimit(){
 		return "LIMIT " . $this->offset . " , " . $this->rows;
 	}
 	
 }
 
?>
