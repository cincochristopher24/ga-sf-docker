<?php
class JournalPrivacyPreferenceType{
	static $PUBLIC                    = 0;
	static $PRIVATE                   = 1;
	static $FRIENDS                   = 2;
	static $GROUP_MEMBERS             = 3;
	static $GROUP_MEMBERS_AND_FRIENDS = 4;	
}
?>
