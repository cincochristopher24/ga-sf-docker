<?php
	
	/**
	 * note: requires $GLOBALS['CONFIG'] for needed variables
	 */
	 //require_once 'travellog/model/Class.Config.php';
	 //require_once('travellog/model/Class.SiteContext.php');
	 /** Added by naldz(May, 26, 2008) : Quick fix(Temporary) **/
	 require_once('Class.Connection.php');
	 require_once('Class.Recordset.php');
	 require_once('gaLogs/Class.GaLogger.php');
	 
	 class session{
		
		private	$conn;
		private $rs;
		private $userID;
		private $config;
		static	$instance = NULL; 
		
		static function getInstance( $userID ){
			
			if( NULL == self::$instance ){
				self::$instance = new session();
				self::$instance->userID = $userID;
				self::$instance->conn = new Connection();
				/***
				if(!isset($GLOBALS['CONFIG']))
					self::$instance->config = new Con_fig($_SERVER['SERVER_NAME']);
				else
					self::$instance->config = $GLOBALS['CON_FIG'];
				***/
				//self::$instance->config = SiteContext::getInstance();
			} 
			return self::$instance;	
		}
			 
	 	function generateSession(){
	 			$rs = new Recordset($this->conn);
	 			/***
				$sql = " UPDATE tblSessions " .
	 				"SET sessionTime = '" . time() . "', " .
	 				"browser = '{$_SERVER['HTTP_USER_AGENT']}' " . 
	 				"WHERE session_user_id = {$this->userID} " .
	 				"AND domain = '{$this->config->getServerName()}'";
				***/
				$sql = " UPDATE tblSessions " .
	 				"SET sessionTime = '" . time() . "', " .
	 				"browser = '{$_SERVER['HTTP_USER_AGENT']}' " . 
	 				"WHERE session_user_id = {$this->userID} " .
	 				"AND domain = '{$_SERVER['HTTP_HOST']}'";
	
				//$rs->execute($sql);
				 /** Added by naldz(May, 26, 2008) : Quick fix **/
				GaLogger::create()->beginNoTrackingSection();
				$rs->execute($sql);
				GaLogger::create()->endNoTrackingSection();
	 	}
	 	
	 	function startSession(){
 			$rs = new Recordset($this->conn);
 			/***
			$sql = "INSERT INTO tblSessions " . 
				"SET session_user_id = {$this->userID} " . 
				",sessionTime = '" . time() . "' " .  
				",loginTime = '" . time() . "' " .
				",domain = '{$this->config->getServerName()}' " .
				"ON DUPLICATE KEY UPDATE sessionTime = '" . time() . "'";
			***/
			
			$sql = "INSERT INTO tblSessions " .
				"SET session_user_id = {$this->userID} " . 
				",sessionTime = '" . time() . "' " .  
				",loginTime = '" . time() . "' " .
				",domain = '{$_SERVER['HTTP_HOST']}' " .
				"ON DUPLICATE KEY UPDATE sessionTime = '" . time() . "'";
			
			//$rs->execute($sql);
			GaLogger::create()->beginNoTrackingSection();
			$rs->execute($sql);
			GaLogger::create()->endNoTrackingSection();
	 	}
	 	
	 	function killSession(){
 			$rs = new Recordset($this->conn);
 			/***
			$sql = 	"DELETE FROM tblSessions " .
 					"WHERE session_user_id = {$this->userID} " .
 					"AND domain = '{$this->config->getServerName()}'";
			***/
			$sql = 	"DELETE FROM tblSessions " .
 					"WHERE session_user_id = {$this->userID} " .
 					"AND domain = '{$_SERVER['HTTP_HOST']}'";
			//$rs->execute($sql);
			GaLogger::create()->beginNoTrackingSection();
			$rs->execute($sql);
			GaLogger::create()->endNoTrackingSection();
	 	}	
	 	
	 }
	 
?>