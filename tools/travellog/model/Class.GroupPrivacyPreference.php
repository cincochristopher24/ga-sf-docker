<?php
/*
 * Created on 10 9, 2006
 *
 *  @author Kerwin Gordo
 * 	Purpose: class to hold a groups privacy preference  
 */
 
require_once ('Class.Connection.php');
require_once ('Class.Recordset.php');
require_once ('gaexception/Class.InstantiationException.php');
require_once 'travellog/model/Class.PrivacyPreferenceType.php';
 
 class GroupPrivacyPreference 
 {
 	
 	/******************************************************************************
 	 * edits of neri:
 	 * 		set default value of member variable mUpdates to 5:			01-09-09
 	 * 		added case for preference type "receive updates" in
 	 * 			constructor:											01-09-09 
 	 * 		added setModeOfReceivingUpdates and 
 	 * 			getModeOfReceivingUpdates functions:					01-09-09
 	 * 		modified update():											01-09-09
 	 ******************************************************************************/
 	
 	private $mGroupID = 0;
 	private $mViewAlerts = false;					// false means only viewable within the group members
 	private $mDownloadFiles = false;				//					- do -
 	private $mViewGroups = false;					//					- do -	
 	private $mViewBulletin = false;					//					- do -
 	private $mViewCalendar = false;					//					-do -	 
 	private $mUpdates = 5;							// 5 means group advisor will receive GA updates
	private $mAutoModeGroupJournals = true;		// true means all journals of group members will 
													// be automatically approved and featured for the group
 	
 	private $mCon = NULL;
	private $mRs = NULL;
 	
 	
 	function GroupPrivacyPreference($groupID = 0) {
		$this->mCon = new Connection();
		$this->mRs = new Recordset($this->mCon);

		if (0 < $groupID) {
			$this->mGroupID = $groupID;

			$qry = "select * from tblGroupPrivacyPreference where groupID='$groupID'";

			$this->mRs->Execute($qry);
			if ($this->mRs->Recordcount() == 0)
				throw new InstantiationException('Inconsistent data!, groupID' . $groupID . ' not found in group privacy preference table');

			while ($rsar = mysql_fetch_array($this->mRs->Resultset())) {
				switch ($rsar['preferencetype']) {
					case PrivacyPreferenceType :: $SHOW_GROUP_ALERTS :
						$this->mViewAlerts = $rsar['preference'];
						break;
					case PrivacyPreferenceType :: $SHOW_GROUP_SUBGROUPS :
						$this->mViewGroups = $rsar['preference'];
						break;
					case PrivacyPreferenceType :: $SHOW_GROUP_BULLETIN :
						$this->mViewBulletin = $rsar['preference'];
						break;
					case PrivacyPreferenceType :: $SHOW_GROUP_FILES :
						$this->mDownloadFiles = $rsar['preference'];
						break;	
					case PrivacyPreferenceType :: $SHOW_GROUP_CALENDAR :
						$this->mViewCalendar = $rsar['preference'];	
						break;
					case PrivacyPreferenceType :: $RECEIVE_UPDATES :
						$this->mUpdates = $rsar['preference'];
						break;
					case PrivacyPreferenceType :: $APPROVE_AND_FEATURE_GROUP_JOURNALS :
						$this->mAutoModeGroupJournals = $rsar['preference'];
						break;
				}

			}
		}	
		
			
 	}
 	
 	
 	function setGroupID($groupID) {
 		$this->mGroupID = $groupID;
 	}
 	
 	function setViewAlerts($viewAlerts) {
 		$this->mViewAlerts = $viewAlerts;
 	}
 	
 	function getViewAlerts() {
 		return $this->mViewAlerts;  
 	}
 	
 	/**
 	 * function to ask if members of the parameters group can view alerts
 	 */
 	function canViewAlerts($groupID) {
 		if ($this->mViewAlerts)
 			return true;
 		elseif (!$this->mViewAlerts && $this->mGroupID == $groupID ) 
 			return true;
 		return false;
 	}	
 	
 	function setDownloadFiles($downloadFiles) {
 		$this->mDownloadFiles = $downloadFiles;
 		
 	}
 	
 	function getDownloadFiles() {
 		return $this->mDownloadFiles;  
 	}
 	
 	
 	/**
 	 * function to ask if member of the parameters group can download files
 	 */
 	function canDownloadFiles($groupID) {
 		if ($this->mDownloadFiles )
 			return true;
 		elseif (!$this->mDownloadFiles && $this->mGroupID == $groupID)
 			return true;
 			
 		return false;
 	}
 	
 	
 	function setViewGroups($viewGroups) {
 		$this->mViewGroups = $viewGroups;
 	}
 	
 	function getViewGroups() {
 		return $this->mViewGroups;  
 	}
 	
 	/**
 	 * function to ask if member of the parameters group can view groups 
 	 */
 	function canViewGroups($groupID) {
 		if ($this->mViewGroups )
 			return true;
 		elseif (!$this->mViewGroups && $this->mGroupID == $groupID)
 			return true;
 			
 		return false;
 	}
 	
 	
 	function setViewBulletin($viewBulletin) {
 		$this->mViewBulletin = $viewBulletin;
 	}
 	
 	function getViewBulletin() {
 		return $this->mViewBulletin;  
 	}
 	
 	/**
 	 * function to ask if member of the parameters group can view bulletin
 	 */
 	
 	function canViewBulletin($groupID) {
 		if ($this->mViewBulletin )
 			return true;
 		elseif (!$this->mViewBulletin && $this->mGroupID == $groupID)
 			return true;
 			
 		return false;
 	}
 	
 	
 	function setViewCalendar($viewCalendar){
 		$this->mViewCalendar = $viewCalendar;
 	}
 	
 	function getViewCalendar() {
 		return $this->mViewCalendar;
 	}
 	
 	function canViewCalendar($groupID){
 		if ($this->mViewCalendar)
 			return true;
 		elseif (!$this->mViewCalendar && $this->mGroupID == $groupID)	
 			return true;
 			
 		return false;	 
 	}
 	
 	function setModeOfReceivingUpdates($receive){
 		$this->mUpdates = $receive;
 	}
 	
 	function getModeOfReceivingUpdates(){
 		return $this->mUpdates;
 	}

	function setAutoModeGroupJournals($autoApprove){
 		$this->mAutoModeGroupJournals = $autoApprove;
 	}
 	
 	function isAutoModeGroupJournals(){
 		return $this->mAutoModeGroupJournals;
 	}
	
	// check if group preference are still default
	function isPreferenceDefault(){
		return ( !$this->mViewAlerts && !$this->mDownloadFiles && 
				!$this->mViewGroups && !$this->mViewBulletin && 
				!$this->mViewCalendar  && $this->mUpdates == 5 && 
				$this->mAutoModeGroupJournals) ? true : false;
	}
 	
 	function update() {
 		if (0 < $this->mGroupID) {
			// delete first all the privacy preference entry
			$qry = "DELETE FROM tblGroupPrivacyPreference WHERE groupID='$this->mGroupID'";

			$this->mRs->Execute($qry);

			$showgroupalerts = PrivacyPreferenceType::$SHOW_GROUP_ALERTS;
			$showgroupbulletin = PrivacyPreferenceType::$SHOW_GROUP_BULLETIN;
			$showgroupfiles = PrivacyPreferenceType::$SHOW_GROUP_FILES;
			$showgroupsubgroups = PrivacyPreferenceType::$SHOW_GROUP_SUBGROUPS;
			$showgroupcalendar = PrivacyPreferenceType::$SHOW_GROUP_CALENDAR;
			$receiveupdates = PrivacyPreferenceType::$RECEIVE_UPDATES;
			$autogroupjournals = PrivacyPreferenceType::$APPROVE_AND_FEATURE_GROUP_JOURNALS;
						
			// then insert the new preferences
			$qry = "INSERT INTO tblGroupPrivacyPreference (groupID,preferenceType,preference) VALUES ('$this->mGroupID',$showgroupalerts,'$this->mViewAlerts')," .
			"('$this->mGroupID',$showgroupbulletin,'$this->mViewBulletin'),('$this->mGroupID',$showgroupfiles,'$this->mDownloadFiles'), " . 
			"('$this->mGroupID',$showgroupsubgroups,'$this->mViewGroups'),('$this->mGroupID',$showgroupcalendar,'$this->mViewCalendar'), " .
			"('$this->mGroupID',$receiveupdates,'$this->mUpdates'),('$this->mGroupID',$autogroupjournals,'$this->mAutoModeGroupJournals') ";

			$this->mRs->Execute($qry);

		}
 	}
 	
 	
 }
 
 
 
?>
