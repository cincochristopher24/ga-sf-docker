<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	require_once('travellog/model/Class.AddressBookEntryPreference.php');
	
	/**
	* EDITS:
	*	- Jan 26, 2009
	*		-- added ownerGroupID field - chris
	*		-- added static function - chris
	**/
	
	class AddressBookEntry{

		const IS_NOTABLE	= 1;
		const NO_STATUS		= 0;
		const PENDING		= 1;
		const ACCEPTED		= 2;
		const DENIED		= 3;
		
		private $mDb 					= null;
		private $mAddressBookEntryID 	= null;
		private $mOwnerTravelerID		= null;
		private $mEmailAddress			= null;
		private $mName 					= null;
		private $mDetails				= null;
		private $mIsNotable				= null;
		private $mStatus				= null;
		private $mGroupID				= null;
		
		function __construct($addressBookEntryID=null,$data=null){
			$this->mDb = new dbHandler();
			if(!is_null($addressBookEntryID)){
				$qry = 	'SELECT `addressBookEntryID`, `ownerTravelerID`, `emailAddress`, `name`, `details`, `isNotable`, `status`, `ownerGroupID` ' .
						'FROM tblAddressBookEntry ' .
						'WHERE addressBookEntryID = '.ToolMan::makeSqlSafeString($addressBookEntryID);
				$rs = new iRecordset($this->mDb->execQuery($qry));				
				if(0!=$rs->retrieveRecordCount()){
					$this->init($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid AddressBookEntryID '.$addressBookEntryID.' passed to object of type AddressBookEntry.');
				}
			}
			if(is_array($data)){
				$this->init($data);
			}
		}
	
		private function init($data=null){
			$this->setAddressBookEntryID($data['addressBookEntryID']);
			$this->setOwnerTravelerID($data['ownerTravelerID']);
			$this->setEmailAddress($data['emailAddress']);
			$this->setName($data['name']);
			$this->setDetails($data['details']);
			$this->setIsNotable($data['isNotable']);
			$this->setStatus($data['status']);
			$this->setOwnerGroupID($data['ownerGroupID']);
		}
		
		//SETTERS
		private function setAddressBookEntryID($id = null){
			$this->mAddressBookEntryID = $id;
		}
		
		public function setOwnerTravelerID($ownerTravelerID=null){
			$this->mOwnerTravelerID = $ownerTravelerID;			
		}
		
		public function setEmailAddress($emailAddress=null){
			$this->mEmailAddress = $emailAddress;
		}
		
		public function setName($name=null){
			$this->mName = $name;
		}
		
		public function setDetails($details=null){
			$this->mDetails = $details;			
		}

		public function setIsNotable($isNotable=null){
			$this->mIsNotable = $isNotable;
		}
		
		public function setStatus($status=null){
			$this->mStatus=$status;
		}
		
		public function setOwnerGroupID($param=null){
			$this->mOwnerGroupID = $param;
		}
		
		//GETTERS
		public function getAddressBookEntryID(){
			return $this->mAddressBookEntryID;
		}
		
		public function getOwnerTravelerID(){
			return $this->mOwnerTravelerID;
		}
		
		public function getEmailAddress(){
			return $this->mEmailAddress;
		}
		
		public function getName(){
			return $this->mName;
		}
		
		public function getDetails(){
			return $this->mDetails;
		}
		
		public function getIsNotable(){
			return $this->mIsNotable;
		}
		
		public function getStatus(){
			return $this->mStatus;
		}
		
		public function getOwnerGroupID(){
			return $this->mOwnerGroupID;
		}
		
		public function isNotable(){
			return $this->getIsNotable() == self::IS_NOTABLE;
		}
		
		public function hasStatus(){
			return $this->getStatus() != self::NO_STATUS;
		}
		
		public function isPending(){
			return $this->getStatus() == self::PENDING;
		}
		
		public function isAccepted(){
			return $this->getStatus() == self::ACCEPTED;	
		}
		
		public function isDenied(){
			return $this->getStatus() == self::DENIED;
		}	
		
		//CRUDE
		public function save(){
			if(null===$this->mAddressBookEntryID){
				$qry = 	'INSERT INTO tblAddressBookEntry(' .
							'`ownerTravelerID`, ' .
							'`emailAddress`,' .
							'`name`,' .
							'`details`,' .
							'`isNotable`,' .
							'`status`,' .
							'`ownerGroupID`'.
						') ' .
						'Values ('.
							ToolMan::makeSqlSafeString($this->getOwnerTravelerID()).','.
							ToolMan::makeSqlSafeString($this->getEmailAddress()).',' .
							ToolMan::makeSqlSafeString($this->getName()).','.
							ToolMan::makeSqlSafeString($this->getDetails()).', ' .
							ToolMan::makeSqlSafeString($this->getIsNotable()).','.
							ToolMan::makeSqlSafeString($this->getStatus()).','.
							ToolMan::makeSqlSafeString($this->getOwnerGroupID()).
							
						')';
				$this->mDb->execQuery($qry);
				$this->setAddressBookEntryID($this->mDb->getLastInsertedID());
			}
			else{
				$qry = 	'UPDATE tblAddressBookEntry SET ' .
							'`ownerTravelerID` 	= '. ToolMan::makeSqlSafeString($this->getOwnerTravelerID()) .', ' .
							'`emailAddress` 	= '.ToolMan::makeSqlSafeString($this->getEmailAddress()).', ' .
							'`name` 			= '.ToolMan::makeSqlSafeString($this->getName()).', '.
							'`details` 			= '. ToolMan::makeSqlSafeString($this->getDetails()).', ' .
							'`isNotable` 		= '. ToolMan::makeSqlSafeString($this->getIsNotable()).', ' .
							'`status` 			= '.ToolMan::makeSqlSafeString($this->getStatus()).', '.
							'`ownerGroupID`		= '.ToolMan::makeSqlSafeString($this->getOwnerGroupID()).' '.
						'WHERE `addressBookEntryID` = '.ToolMan::makeSqlSafeString($this->getAddressBookEntryID());
				$this->mDb->execQuery($qry);
			}
		}
		
		public function delete(){
			$this->clearPreferences();
			$qry = 	'DELETE FROM tblAddressBookEntry ' .
					'WHERE addressBookEntryID = '.ToolMan::makeSqlSafeString($this->getAddressBookEntryID());					
			$this->mDb->execQuery($qry);
			$this->clearFields();
			return true;
		}
		
		public function clearFields(){
			$this->setAddressBookEntryID(null);
			$this->setOwnerTravelerID(null);
			$this->setEmailAddress(null);
			$this->setName(null);
			$this->setDetails(null);
			$this->setIsNotable(null);
			$this->setStatus(null);
			return true;
		}
		
		public function getPreferences(){
			return AddressbookEntryPreference::getPreferencesByAddressBookEntryID($this->getAddressBookEntryID());
		}

		public function clearPreferences(){
			AddressbookEntryPreference::clearPreferencesByAddressBookEntryID($this->getAddressBookEntryID());
		}		
		
		/** A static function that would return a boolean value that would determine if the given keyID is valid or if it exists in the database in its particular context.
		 * This is mostly applicable in Variable Objects.
		 * @param integer keyID
		 * @return boolean
		 */
		public static function isExists($keyID=null){
			$db = new dbHandler();
			$qry = 	'SELECT addressBookEntryID ' .
					'FROM tblAddressBookEntry ' .
					'WHERE addressBookEntryID = '.ToolMan::makeSqlSafeString($keyID);
			$rs = new iRecordset($db->execQuery($qry));
			return ($rs->retrieveRecordCount() > 0);
		}
		
		public static function getAddressBookEntriesByOwnerTravelerID($ownerTravelerID=0){
			$db = new dbHandler();
			$qry = 	'SELECT `addressBookEntryID`, `ownerTravelerID`, `emailAddress`, `name`, `details`, `isNotable`, `status`, `ownerGroupID`  ' .
					'FROM tblAddressBookEntry ' .
					'WHERE ownerTravelerID = '.ToolMan::makeSqlSafeString($ownerTravelerID).' ' .
						'AND ownerGroupID = 0 ' .
					'ORDER BY name';
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();			
			while(!$rs->EOF()){
				$ar[] = new AddressBookEntry(null,$rs->retrieveRow());
				$rs->moveNext();
			}
			return $ar;
		}
		
		public static function getBulk($ownerTravelerID=0){
			return self::getAddressBookEntriesByOwnerTravelerID($ownerTravelerID);
		}
		
		public static function isUniqueEmailAddressForTraveler($travelerID,$emailAdd,$entryID=0){
			$db = new dbHandler();
			$qry =	'SELECT `addressBookEntryID` ' .
					'FROM tblAddressBookEntry ' .
					'WHERE ownerTravelerID = '.ToolMan::makeSqlSafeString( $travelerID ).' ' .
					'AND emailAddress = '.ToolMan::makeSqlSafeString( $emailAdd ).' ' .
					'AND ownerGroupID = 0 ' .
					'AND addressBookEntryID != '.ToolMan::makeSqlSafeString( $entryID );
			$rs = new iRecordset($db->execQuery($qry));
			return (0==$rs->retrieveRecordCount());
		}
		
		public static function travelerHasEntry($travelerID,$entryID){
			$db = new dbHandler();
			$qry =	'SELECT `addressBookEntryID` ' .
					'FROM tblAddressBookEntry ' .
					'WHERE ownerTravelerID = '.ToolMan::makeSqlSafeString( $travelerID ).' ' .
					'AND addressBookEntryID = '.ToolMan::makeSqlSafeString( $entryID );
			$rs = new iRecordset($db->execQuery($qry));
			return (0!=$rs->retrieveRecordCount());
		}
		
		/* added by chris: Jan. 26, 2009 */
		public static function isUniqueEmailAddressForGroup($groupID,$emailAdd,$entryID=0){
			$db = new dbHandler();
			$qry =	'SELECT `addressBookEntryID` ' .
					'FROM tblAddressBookEntry ' .
					'WHERE ownerGroupID = '.ToolMan::makeSqlSafeString( $groupID ).' ' .
					'AND emailAddress = '.ToolMan::makeSqlSafeString( $emailAdd ).' ' .
					'AND addressBookEntryID != '.ToolMan::makeSqlSafeString( $entryID );
			$rs = new iRecordset($db->execQuery($qry));
			return (0==$rs->retrieveRecordCount());
		}
		
		
		public static function getAddressBookEntriesByOwnerGroupID($groupID){
			require_once('Class.GaString.php');
			$db  		= new dbHandler;
			$groupID 	= GaString::makeSqlSafe($groupID);
			$sql 		= "SELECT * FROM Travel_Logs.tblAddressBookEntry as entry
						   WHERE entry.ownerGroupID = $groupID 
								AND entry.ownerGroupID > 0
						   ORDER BY entry.name";
			$rs 		= new iRecordset($db->execute($sql));
			$arr		= array();
			foreach( $rs as $row )
				$arr[] =  new self(null, $row);
			return $arr;
		}
		/* end add */
	}
?>