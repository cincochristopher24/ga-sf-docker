<?php
/*
 * Created on 09 26, 2006
 *  @author Kerwin Gordo
 *  Purpose: class to store the different types of comment
 */
 class CommentType {
	 public static  $PROFILE	= 1;
	 public static  $PHOTO		= 2;
	 public static  $TRAVELLOG	= 3;
	 public static  $ARTICLE    = 4;
	 public static  $VIDEO 		= 5;	
 }
 
?>
