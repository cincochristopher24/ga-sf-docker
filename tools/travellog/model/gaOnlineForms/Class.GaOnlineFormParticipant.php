<?php
	require_once('Class.DataModel.php');
	require_once('travellog/model/gaOnlineForms/Class.GaOnlineForm.php');
	require_once('travellog/model/gaOnlineForms/Class.GaOnlineFormParticipation.php');
	
	class GaOnlineFormParticipant extends DataModel
	{
		public function __construct($keyID=null,$data=null){
			parent::initialize('Travel_Logs', 'tblGaOnlineFormParticipant');
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid GaOnlineFormParticipantID');
			}
			if(is_array($data)){
				$this->setFields($data);
			}
		}
		
		public function getGaOnlineForm(){
			return GaOnlineForm::getFormByOnlineFormID($this->getGaOnlineFormID());
		}
		
		public function delete(){
			foreach($this->getParticipations() as $iParticipation){
				$iParticipation->delete();
			}
			parent::delete();
		}
		
		public function getGroup(){
			return new Group($this->getGroupID());
		}
		
		public function getParticipations(){
			return GaOnlineFormParticipation::getInstancesByParticipantID($this->getGaOnlineFormParticipantID());
		}
		
		static public function getParticipantsByOnlineFormID($onlineFormID=0){
			$db = new dbHandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblGaOnlineFormParticipant WHERE gaOnlineFormID = {$db->makeSqlSafeString($onlineFormID)}"));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new self(null,$row);
			}
			return $ar;
		}
		
		static public function getByID($id=0){
			$db = new dbHandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblGaOnlineFormParticipant WHERE gaOnlineFormParticipantID = {$db->makeSqlSafeString($id)}"));
			return $rs->retrieveRecordCount() > 0 ? new self(null,$rs->retrieveRow(0)) : null ;
		}
		
	}
?>