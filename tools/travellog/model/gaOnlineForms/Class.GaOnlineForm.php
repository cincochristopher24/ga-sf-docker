<?php
	require_once('Class.DataModel.php');
	require_once('travellog/model/gaOnlineForms/Class.GaOnlineFormParticipant.php');
	require_once('travellog/model/gaOnlineForms/Class.GaOnlineFormRestClient.php');
	
	class GaOnlineForm extends DataModel
	{
		
		const LOCKED_STATUS = 0;
		const OPEND_STATUS = 1;
		
		static private $restClient = null;
		private $mRestClient = null;
		private $mFormData = null;
		private $mJsonFormData = null;
		public function __construct($keyID=null,$data=null,$formData=null){
			parent::initialize('Travel_Logs', 'tblGaOnlineForm');
			if($keyID and !$this->load($keyID)){ throw new exception('Invalid GaOnlineFormID');	}
			if(is_array($data)){ $this->setFields($data);}
			$this->mRestClient = GaOnlineFormRestClient::create();
			$this->initFormData($formData);
		}
		
		public function getName(){
			return $this->mFormData['name'];
		}
		
		public function getCaption(){
			return $this->mFormData['caption'];
		}
		
		public function getDateCreated(){
			return $this->mFormData['dateCreated'];
		}
		
		private function initFormData($formData=null){
			if(is_null($this->mFormData)){
				if(is_null($formData)){
					$data =  (!$this->getGaFormID() ? $this->mRestClient->createNewForm() : $this->mRestClient->getFormData($this->getGaFormID()));
					$this->mJsonFormData = $data['json'];
					$this->mFormData = $data['object'];
				}
				else{
					$this->mFormData = $formData['object'];
					$this->mJsonFormData = $formData['json'];
				}
			}
		}
		
		public function getFormData(){
			return $this->mFormData;
		}
		
		public function getJsonFormData(){
			return $this->mJsonFormData;
		}
		
		/***
		public function getRenderableFormData(){
			return $this->mRestClient->getRenderableFormData($this->getGaFormID());
		}
		***/
		
		public function getStatistics(){
			return $this->mRestClient->getStatistics($this->getGaFormID());
		}
		
		public function saveFormData($formData,$newParticipants=array()){
			$gaFormID = $this->mRestClient->saveForm($formData);
			$this->setGaFormID($gaFormID);
			parent::save();
			//deal with the participants
			$extParticipants = $this->getParticipants();
			$extParticipantMap = array();
			foreach($extParticipants as $iExtParticipant){
				$extParticipantMap[$iExtParticipant->getGroupID()] = $iExtParticipant;
			}
			//check the added participants
			$stillExtParticipants = array();
			foreach($newParticipants as $iNewParticipant){
				if(array_key_exists($iNewParticipant,$extParticipantMap)){
					$stillExtParticipants[] = $iNewParticipant;
				}
				else{//this is a new participant
					$newParticipantObj = new GaOnlineFormParticipant();
					$newParticipantObj->setGroupID($iNewParticipant);
					$newParticipantObj->setGaOnlineFormID($this->getGaOnlineFormID());
					$newParticipantObj->save();
				}
			}
			//remove the deleted participants
			foreach($extParticipants as $iExtParticipant){
				if(!in_array($iExtParticipant->getGroupID(),$stillExtParticipants)){
					$iExtParticipant->delete();
				}
			}
			$this->initFormData();
		}

		public function delete(){
			$this->mRestClient->deleteForms(array($this->getGaFormID()));
			foreach($this->getParticipants() as $iParticipant){
				$iParticipant->delete();
			}
			parent::delete();
		}

		public function getParticipants(){
			return GaOnlineFormParticipant::getParticipantsByOnlineFormID($this->getGaOnlineFormID());
		}
		
		public function isOpen(){
			return $this->getStatus() == 1;
		}

		public function open(){
			$this->setStatus(1);
			parent::save();
		}
		
		public function close(){
			$this->setStatus(0);
			parent::save();
		}
		
		/***
		public function render($options=array()){
			$options['formID'] = $this->getGaFormID();
			if(array_key_exists('participation',$options) && !is_null($options['participation'])){
				$options['formValues'] = $options['participation']->getFormValue();
			}
			return $this->mRestClient->renderForm($options);
		}
		***/
		
		public function duplicate(){
			$duplicateFormData = $this->mRestClient->getDuplicateFormData($this->getGaFormID());
			$onlineFormData = $this->mFields;
			$onlineFormData['gaOnlineFormID'] = 0;
			$onlineFormData['gaFormID'] = 0;
			return new GaOnlineForm(null, $onlineFormData, $duplicateFormData);
		}

		public function getAllParticipations(){
			//get all the participations
			return GaOnlineFormParticipation::getInstancesByOnlineFormID($this->getGaOnlineFormID());
		}

		static public function getFormByGaFormID($gaFormID=0){
			$db = new dbHandler();
			$rs = new iRecordset($db->execute('SELECT * FROM tblGaOnlineForm WHERE gaFormID  = '.$db->makeSqlSafeString($gaFormID)));
			return ($rs->retrieveRecordCount() ? new self(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getFormByOnlineFormID($onlineFormID=0){
			$db = new dbHandler();
			$rs = new iRecordset($db->execute('SELECT * FROM tblGaOnlineForm WHERE gaOnlineFormID  = '.$db->makeSqlSafeString($onlineFormID)));
			return ($rs->retrieveRecordCount() ? new self(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getFormsByGroupID($groupID=0){
			$db = new dbHandler();
			$sql = 	'SELECT * '.
					'FROM tblGaOnlineForm '.
					'WHERE groupID = '.$db->makeSqlSafeString($groupID);
			$rs = new iRecordset($db->execute($sql));
			//get the formData 
			$formData = GaOnlineFormRestClient::create()->getMultipleFormData($rs->retrieveColumn('gaFormID'),false);
			$ar = array();
			foreach($rs as $row){
				$ar[$row['gaOnlineFormID']] = new self(null,$row,$formData[$row['gaFormID']]);
			}
			return $ar;
		}
		
	}
?>