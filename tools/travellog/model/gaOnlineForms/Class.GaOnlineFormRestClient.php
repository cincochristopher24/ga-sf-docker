<?php
	require_once('travellog/model/gaForms/Class.GaFormRestClient.php');
	require_once('Class.Decorator.php');
	
	class GaOnlineFormRestClient extends Decorator
	{
		static private $instance = null;
		static public function create(){
			if(is_null(self::$instance)){
				self::$instance = new self(
					GaFormRestClient::create(
						array(
							'apiKey'	=>'40038758548e9d430941c6214539086',
							'apiSecret'	=>'MyApiSecret', 
							'serverAddress' => 'http://'.$_SERVER['SERVER_NAME'].'/gaforms/serviceprovider.php'
						)
					)
				);
			}
			return self::$instance;
		}
	}
?>