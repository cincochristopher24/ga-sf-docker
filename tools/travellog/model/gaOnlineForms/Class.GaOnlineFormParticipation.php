<?php
	require_once('Class.DataModel.php');
	require_once('travellog/model/gaOnlineForms/Class.GaOnlineFormRestClient.php');
	require_once('travellog/model/gaOnlineForms/Class.GaOnlineFormParticipant.php');
	require_once('travellog/model/gaOnlineForms/Class.GaOnlineForm.php');
	
	class GaOnlineFormParticipation extends DataModel
	{
		private $mFormValue = null;
		private $mRestClient = null;
		public function __construct($keyID=null,$data=null,$formValue=null){
			parent::initialize('Travel_Logs', 'tblGaOnlineFormParticipation');
			if($keyID and !$this->load($keyID)){ throw new exception('Invalid GaOnlineFormID');	}
			if(is_array($data)){ $this->setFields($data);}
			$this->mRestClient = GaOnlineFormRestClient::create();
			$this->initFormValue($formValue);
		}
		
		public function initFormValue($formValue=null){
			if(is_null($this->mFormValue)){
				if(is_null($formValue))
					$this->mFormValue = (is_null($this->getGaOnlineFormParticipationID()) ? null : $this->mRestClient->getFormValue($this->getGaFormValueID()));
				else $this->mFormValue = $formValue;
			}
		}
		
		public function getGroupParticipant(){
			return GaOnlineFormParticipant::getByID($this->getGaOnlineFormParticipantID());
		}
		
		public function getTraveler(){
			return new Traveler($this->getTravelerID());
		}
		
		public function getDateCreated(){
			return $this->mFormValue['dateCreated'];
		}
		
		public function getFieldValues(){
			return $this->mFormValue['fieldValues'];
		}
		
		public function saveFormValue($formValues=array(),$validateOnly=false){
			$result = $this->mRestClient->saveFormValue($this->getGroupParticipant()->getGaOnlineForm()->getGaFormID(),$this->getGaFormValueID(),$formValues,$validateOnly);
			if(is_numeric($result)){
				if(!$validateOnly){
					$this->setGaFormValueID($result);
					$this->save();
				}
				return true;
			}
			return $result;
		}
		
		static public function getParticipationByParticipantIDAndTravelerID($participantID,$travelerID){
			$db = new dbHandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblGaOnlineFormParticipation WHERE gaOnlineFormParticipantID = {$db->makeSqlSafeString($participantID)} AND travelerID = {$db->makeSqlSafeString($travelerID)}"));
			return ($rs->retrieveRecordCount() ? new self(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getInstancesByParticipantID($participantID=0){
			$db = new dbHandler();
			$sql = 	'SELECT * FROM tblGaOnlineFormParticipation '.
					'WHERE gaOnlineFormParticipantID = '.$db->makeSqlSafeString($participantID);
			$rs = new iRecordset($db->execute($sql));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new self(null,$row);
			}
			return $ar;
		}
		
		static public function getInstancesByOnlineFormID($formID=0){
			$ar = array();
			$db = new dbHandler();
			$sql = 	'SELECT * FROM tblGaOnlineFormParticipation, tblGaOnlineFormParticipant '.
					'WHERE tblGaOnlineFormParticipation.gaOnlineFormParticipantID = tblGaOnlineFormParticipant.gaOnlineFormParticipantID '.
					'AND tblGaOnlineFormParticipant.gaOnlineFormID = '.$db->makeSqlSafeString($formID);
			$rs = new iRecordset($db->execute($sql));
			$formValueData = GaOnlineFormRestClient::create()->getMultipleFormValues($rs->retrieveColumn('gaFormValueID'));
			foreach($rs as $row){
				$ar[] = new self(null,$row,$formValueData[$row['gaFormValueID']]);
			}
			return $ar;
		}
	}
?>