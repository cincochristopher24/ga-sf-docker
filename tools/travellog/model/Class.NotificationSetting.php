<?php
	/***
	@author: Reynaldo Castellano III
	***/

	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.GaString.php');
	require_once('Class.DataModel.php');

	class NotificationSetting extends DataModel
	{
		
		const TRAVELER_BULLETIN_CONTEXT = 1;
		const GROUP_BULLETIN_CONTEXT	= 2;	
		const MESSAGE_CONTEXT 			= 3;
		const GROUP_ALERT_CONTEXT		= 4;
		const GROUP_EVENT_CONTEXT		= 5;
		const GROUP_RESOURCE_CONTEXT	= 6;
		const NEWS_CONTEXT				= 7;
		
		private static $CONTEXTS = array(
			self::TRAVELER_BULLETIN_CONTEXT,
			self::GROUP_BULLETIN_CONTEXT,
			self::MESSAGE_CONTEXT,
			self::GROUP_ALERT_CONTEXT,
			self::GROUP_EVENT_CONTEXT,
			self::GROUP_RESOURCE_CONTEXT,
			self::NEWS_CONTEXT
		);

		public function __construct($param=null,$data=null){
			parent::initialize('Travel_Logs','tblNotificationSetting');
			if(!is_null($param)){
				if(!$this->load($param)){
					throw new exception('Invalid notificationSettingID '.$param.' passed to object of type NotificationSetting.');
				}
			}
			if(is_array($data)){
				parent::setFields($data);
			}
		}

		//CRUDE
		public function save(){
			if(!self::isValidContext($this->getContext())){
				throw new exception('Invalid context '.$param.' passed to object of type NotificationSetting.');
			}
			parent::save();
		}

		public static function isValidContext($context){
			return in_array($context,self::$CONTEXTS);
		}

		private static function deleteNotificationSettings($context){
			$db = new dbHandler();
			$sql = 'DELETE FROM tblNotificationSetting WHERE context = '.GaString::makeSqlSafe($context);
			$db->execute($sql);
		}

		public static function deleteTravelerBulletinNotificationSettings(){
			self::deleteNotificationSettings(self::TRAVELER_BULLETIN_CONTEXT);
		}
		
		public static function deleteGroupBulletinNotificationSettings(){
			self::deleteNotificationSettings(self::GROUP_BULLETIN_CONTEXT);
		}
		
		public static function deleteMessageNotificationSettings(){
			self::deleteNotificationSettings(self::MESSAGE_CONTEXT);
		}
		
		public static function deleteGroupAlertNotificationSettings(){
			self::deleteNotificationSettings(self::GROUP_ALERT_CONTEXT);
		}
		
	}
?>