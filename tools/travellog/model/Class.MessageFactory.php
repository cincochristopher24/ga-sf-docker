<?php
	require_once 'travellog/model/Class.AlertMessage.php';
	require_once 'travellog/model/Class.BulletinMessage.php';
	require_once 'travellog/model/Class.InquiryMessage.php';
	require_once 'travellog/model/Class.PersonalMessage.php';

	class MessageFactory{
		private $mRs	= NULL;
		private $mConn	= NULL;

		private static $mInstance = NULL;

		/**
		 * Constructor of class MessageFactory
		 * @return MessageFactory
		 */

		private function MessageFactory(){			
			try{ 				
				$this->mConn = new Connection();
				$this->mRs   = new Recordset($this->mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}
		}

		/**
		 * Function name: getInstance
		 * @static
		 * @return MessageFactory
		 * Creates a single instance of this class
		 */
		static function getInstance(){
			if (self::$mInstance == NULL)
				self::$mInstance = new MessageFactory();
				
			return self::$mInstance;
		}

		/**
		 * Function name: Create
		 * @param integer $_messageID
		 * @return Message|NULL|exception
	 	 * Creates an instance of sendable based on messageID id
	 	 */
		function Create($_attributeId = 0, $_sendableId = 0){

			if (0 < $_attributeId){
				$sqlQuery = "SELECT tblMessages.discriminator " .
				 			"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblMessageToAttribute.attributeID = " . $_attributeId;
				$this->mRs->Execute($sqlQuery);

				// Message ID does not exist				
				if (0 == $this->mRs->Recordcount()){										
					throw new exception ("Attribute ID: " . $_attributeId . " is not a valid ID!");
				}

				$discriminator	= $this->mRs->Result(0, "discriminator");

				switch($discriminator){
					// ALERT
		 			case DiscriminatorType::$ALERT :
		 				$message = new AlertMessage($_attributeId, $_sendableId);
		 				break;

		 			// BULLETIN
		 			case DiscriminatorType::$BULLETIN :
		 				$message = new BulletinMessage($_attributeId, $_sendableId);
		 				break;

		 			// PERSONAL MESSAGE
		 			case DiscriminatorType::$PERSONAL :
		 				$message = new PersonalMessage($_attributeId, $_sendableId);
		 				break;

		 			// INQUIRY MESSAGE
		 			case DiscriminatorType::$INQUIRY :
		 				$message = new InquiryMessage($_attributeId, $_sendableId);
		 				break;
				}

 				return $message;
			}
		}
	}
?>