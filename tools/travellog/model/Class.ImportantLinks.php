<?php
	/************************************/
	/*	@author	Cheryl Ivy Q. Go		*/
	/*	Class.ImportantLinks.php		*/
	/*	24 April 2008					*/
	/************************************/

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';

	class ImportantLinks{

		private $mRs	= null;
		private $mConn	= null;

		private $mLinkId	= 0;
		private $mGroupId	= 0;
		private $mText		= '';
		private $mLink		= '';
		private $mDate		= '';

		function ImportantLinks($_linkId = 0){
			try{
				$this->mConn = new Connection();
				$this->mRs = new Recordset($this->mConn);
				$this->mRs2 = new Recordset($this->mConn);
			}
			catch(exception $e){
				throw $e;
			}

			if (0 < $_linkId){
				$sql = "SELECT * FROM tblAdvisorLink WHERE advisorLinkID = " . $_linkId;
				$this->mRs->Execute($sql);

				if (0 == $this->mRs->Recordcount()){
					throw new exception ("Link Id " . $_linkId . " does not exist!");
				}
				$this->mLinkId = $this->mRs->Result(0, "advisorLinkID");
				$this->mText = stripslashes($this->mRs->Result(0, "text"));
				$this->mLink = stripslashes($this->mRs->Result(0, "link"));
				$this->mGroupId = $this->mRs->Result(0, "groupID");
				$this->mDate = $this->mRs->Result(0, "dateadded");
			}
		}

		function setLinkId($_linkId = 0){
			$this->mLinkId = $_linkId;
		}
		function setGroupId($_groupId = 0){
			$this->mGroupId = $_groupId;
		}
		function setText($_text = ''){
			$this->mText = $_text;
		}
		function setLink($_link = ''){
			$this->mLink = $_link;
		}
		function setDate($_date = ''){
			$this->mDate = $_date;
		}

		function getLinkId(){
			return $this->mLinkId;
		}
		function getGroupId(){
			return $this->mGroupId;
		}
		function getText(){
			return $this->mText;
		}
		function getLink(){
			return $this->mLink;
		}
		function getDate(){
			return $this->mDate;
		}

		function Add(){
			/*if (8 <= strlen($this->mLink) && !substr_compare(trim($this->mLink), "https://", 1, 8))
				$link = trim($this->mLink);
			else if (7 <= strlen($this->mLink) && !substr_compare(trim($this->mLink), "http://", 1, 7))
				$link = trim($this->mLink);
			else
				$link = 'http://' . trim($this->mLink);*/
			$now  = date("Y-m-d H:i:s");

			$sql = "INSERT INTO tblAdvisorLink (link, text, groupID, dateadded) " .
				"VALUES ('" . addslashes(trim($this->mLink)) . "', '" . addslashes(trim($this->mText)) . "', " . $this->mGroupId . ", '$now')";
			$this->mRs2->Execute($sql);
		}
		function Edit(){
			/*$link = trim($this->mLink);
			if (!strcasecmp(substr($link, 1, 8), "https://") || !strcasecmp(substr($link, 1, 7), "http://"))
				$link = trim($this->mLink);
			else
				$link = 'http://' . trim($this->mLink);*/

			$sql = "UPDATE tblAdvisorLink " .
					"SET " .
					"link = '" . addslashes(trim($this->mLink)) . "', " .
					"text = '" . addslashes(trim($this->mText)) . "' " .
					"WHERE advisorLinkID = " . $this->mLinkId . " " .
					"AND groupID = " . $this->mGroupId;
			$this->mRs2->Execute($sql);
		}
		function Delete(){
			$sql = "DELETE FROM tblAdvisorLink WHERE advisorLinkID = " . $this->mLinkId . " AND groupID = " . $this->mGroupId;
			$this->mRs2->Execute($sql);
		}
		function getImportantLinks(){
			$sql = "SELECT * FROM tblAdvisorLink WHERE groupID = " . $this->mGroupId . " ORDER BY dateadded DESC";
			$this->mRs->Execute($sql);

			$arrLink = array();
			while($Link = mysql_fetch_assoc($this->mRs->Resultset())){
				$tempLink = new ImportantLinks();
				$tempLink->setLinkId($Link['advisorLinkID']);
				$tempLink->setGroupId($Link['groupID']);
				$tempLink->setText(stripslashes($Link['text']));
				$tempLink->setLink(stripslashes($Link['link']));
				$tempLink->setDate($Link['dateadded']);

				$arrLink[] = $tempLink;
			}

			return $arrLink;
		}
	}
?>