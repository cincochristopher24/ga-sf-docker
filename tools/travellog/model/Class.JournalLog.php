<?php
require_once('travellog/model/Class.JournalLogType.php');
class JournalLog{
	private 
	
	$logID    = 0,
	
	$travelID = 0,
	
	$linkID   = 0,
	
	$travellogID = 0,
	
	$log_type = 0,
	
	$log_date;
	
	
	
	function GetList($c){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$arr  = array();
		$conn = new Connection;
		$rs   = new Recordset($conn);
		//$sql  = sprintf('SELECT logtype,linkID,travelID,travellogID FROM tblJournalLog WHERE travelID = %d AND datediff(now(),logdate) < 7 GROUP BY logtype ORDER BY logID DESC LIMIT 4',$this->travelID);
		//$sql         = sprintf('SELECT * FROM qryJournalLog WHERE travelID = %d GROUP BY logtype ORDER BY logtype DESC LIMIT 4',$this->travelID);
		//$sql         = sprintf('SELECT * FROM qryJournalLog WHERE travelID = %d AND logtype = %d GROUP BY travellogID  DESC %s',$this->travelID,$type,$limit);
		$sql         = sprintf('SELECT * FROM qryJournalLog WHERE travelID = %d %s',$this->travelID, $c->getCriteria2('AND'));
		
		$results     = $rs->Execute($sql);
		$travellogID = 0;
		if( $rs->Recordcount() ){
			while($row = mysql_fetch_assoc($results)){
				if( $row['logtype'] == 1 || $row['logtype'] == 4 ){
					if( $travellogID != $row['travellogID'] ){
						$proceed     = 1;
						$travellogID = $row['travellogID'];
					}else $proceed = 0;
				}else $proceed = 1;
				
				if( $proceed ){
					$objNewMember = new JournalLog;
					$objNewMember->setLogType     ( $row['logtype']     );
					$objNewMember->setLinkID      ( $row['linkID']      );
					$objNewMember->setTravelID    ( $row['travelID']    );
					$objNewMember->setTravellogID ( $row['travellogID'] );
					$arr[$row['travellogID']] = $objNewMember; 
				}
			}
		}	
		return $arr;
	}
	
	function Delete(){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		if( $this->log_type ){
			$arr  = array();
			$conn = new Connection;
			$rs   = new Recordset($conn);
			switch( $this->log_type ){
				case "1":
					$sql = sprintf('DELETE FROM tblJournalLog WHERE travellogID=%d AND (logtype=%d OR logtype=%d)',$this->linkID, $this->log_type, 4);
					break;
				case "2":
					$sql = sprintf('DELETE FROM tblJournalLog WHERE linkID=%d AND logtype=%d',$this->linkID, $this->log_type);
					break;
				case "3":
					$sql = sprintf('DELETE FROM tblJournalLog WHERE travelID=%d',$this->travelID);
					break;
			}
			$rs->Execute($sql);
		}
	}
	
	function Save(){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn = new Connection;
		$rs   = new Recordset($conn);
		$sql  = sprintf('INSERT INTO tblJournalLog (travelID, logtype, linkID, travellogID) VALUES (%d, %d, %d, %d)', $this->travelID, $this->log_type, $this->linkID, $this->travellogID);
		$rs->Execute($sql);	
	}
	
	function setLogID($value){
		$this->logID = $value;
	}
	function getLogID(){
		return $this->logID;
	}
	
	function setTravelID($value){
		$this->travelID = $value;
	}
	function getTravelID(){
		return $this->travelID;
	}
	
	function setLinkID($value){
		$this->linkID = $value;
	}
	function getLinkID(){
		return $this->linkID;
	}
	
	function setTravellogID($value){
		$this->travellogID = $value;
	}
	function getTravellogID(){
		return $this->travellogID;
	}
	
	
	function setLogType($value){
		$this->log_type = $value;
	}
	function getLogType(){
		return $this->log_type;
	}
	
	function getLogTypeAsString(){
		require_once('travellog/model/Class.JournalLogType.php');
		switch($this->log_type){
			case JournalLogType::$ADDED_NEW_ENTRY:
				return 'added a new journal entry';
				break;
			case JournalLogType::$ADDED_NEW_PHOTOS:
				return 'uploaded a photo';
				break;
			case JournalLogType::$UPDATED_JOURNAL:
				return 'updated a journal';
				break;
			case JournalLogType::$UPDATED_ENTRY:
				return 'updated a journal entry';
				break;
		}
	}
	
	function getLinkAsString(){
		require_once('travellog/model/Class.JournalLogType.php');
		switch($this->log_type){
			case JournalLogType::$ADDED_NEW_ENTRY:
				return '/journal-entry.php?action=view&travellogID=' . $this->travellogID;
				break;
			case JournalLogType::$ADDED_NEW_PHOTOS:
				return '/photomanagement.php?cat=travellog&action=vfullsize&genID='.$this->travellogID.'&photoID='.$this->linkID;
				break;
			case JournalLogType::$UPDATED_JOURNAL:
				return '/journal-entry.php?action=view&travelID=' . $this->linkID;
				break;
			case JournalLogType::$UPDATED_ENTRY:
				return '/journal-entry.php?action=view&travellogID=' . $this->travellogID;
				break;
		}
	}
}
?>
