<?php
	require_once("Class.BaseFormat.php");
	require_once("travellog/model/Class.ConfirmationData.php");
	class TravelJournalEntryAdd
	{
		public function getSubject($subject=null){			
			if($subject instanceof Traveler){
				$profile = new TravelerProfile($subject->getTravelerID());
				$travelerName = $profile->getFirstName();
				return "Your friend $travelerName has added a new travel journal entry.";
			}
			elseif($subject instanceof AdminGroup){
				return $subject->getName()." has added a new travel journal entry.";
			}
			else{
				throw new exception('Invalid subject in method getSubject in TravelJournalEntryAdd.');
			}
		}
		
		public function getMessageBody($subject=null,$travellog=null){			
			if(null == $travellog){
				throw new exception('Invalid travellog passed to static function getMessageBody of class TravelJournalEntryAdd');
			}
			if($subject instanceof Traveler){
				$profile = new TravelerProfile($subject->getTravelerID());
				$travelerName = $profile->getFirstName();
				$msg = 	"Your friend $travelerName has added a new travel journal entry on\n" .
						"GoAbroad Network. To view this travel journal entry, click on\n" .
						"the link below:\n" .
						"\n" .
						"http://".$_SERVER['HTTP_HOST']."/travellog.php?action=view&travellogID=".$travellog->getTravelLogID()."\n" .
						"\n" .
						"============================================================\n";
			}
			elseif($subject instanceof AdminGroup){
				$msg = 	$subject->getName()." has added a new travel journal entry on\n" .
						"GoAbroad Network. To view this travel journal entry, click on\n" .
						"the link below:\n" .
						"\n" .
						"http://".$_SERVER['HTTP_HOST']."/travellog.php?action=view&travellogID=".$travellog->getTravelLogID()."\n" .
						"\n" .
						"============================================================\n";
			}
			else{
				throw new exception('Invalid subject in method getMessageBody in TravelJournalEntryAdd.');
			}
			return $msg;
		}
		
		public function getOptOutMessage($addressbookEntry=null){			
			if(!$addressbookEntry instanceof AddressBookEntry){
				throw new exception('Invalid $addressbookEntry passed to static function getOptOutMessage of class TravelJournalEntryAdd');
			}
			$name = BaseFormat::getSubjectName(new Traveler($addressbookEntry->getOwnerTravelerID()));
			$msg = 	"\n" .
					"You received this email because you chose to be updated\n" .
					"about $name's activities on GoAbroad Network.\n" .
					"If you no longer want to receive such updates, simply click\n" .
					"on the link below:\n" .
					"\n" .
					"http://".$_SERVER['HTTP_HOST']."/confirmnotification.php?oID=".ConfirmationData::getEncryptedTravelerID($addressbookEntry->getOwnerTravelerID())."&eID=".ConfirmationData::getEncryptedAddressBookEntryID($addressbookEntry->getAddressBookEntryID())."&cnf=0\n" .
					"\n" .
					"\n" .
					"============================================================\n";
			return $msg;
		}
	}
?>