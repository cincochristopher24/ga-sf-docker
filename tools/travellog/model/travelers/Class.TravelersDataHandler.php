<?php
	/**
	 * @(#) Class.TravelerCountHandler.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 7, 2009
	 */
	
	require_once("travellog/model/Class.GanetDbHandler.php");
	require_once("Class.ResourceIterator.php");
	
	/**
	 * Handler for counts of the travelers. This to refactor the code in traveler counts.
	 */
	class TravelersDataHandler {
		
		/**
		 * Returns an associative array of counts of the journal entries with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of counts of the journal entries with keys equal to the traveler ID.
		 */
		static function getJournalEntriesCount($traveler_ids){
			if (!empty($traveler_ids)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					$entries_count = array();
						
					$sql = "SELECT COUNT(tblTravelLog.travellogID) AS count, tblTravel.travelerID " .
						" FROM tblTravel, tblTravelLink, tblTravelLog " .
						" WHERE tblTravel.deleted = 0 " .
						" AND tblTravel.travelerID IN ($traveler_ids) " .
						" AND tblTravelLink.travellinkID = tblTravel.travellinkID " .
						" AND tblTravelLink.reftype = 1 ".
						" AND tblTravelLog.travelID = tblTravel.travelID " .
						" AND tblTravelLog.deleted = 0 " .
						" GROUP BY travelerID ";
					
					$records = new ResourceIterator($handler->execute($sql));
					foreach($records as $record) {
						$entries_count[$record['travelerID']] = $record['count'];
					}
					
					return $entries_count;
				}
				catch (exception $ex) {}
			}
		}
		
		/**
		 * Returns an associative array of counts of the journal photos with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of counts of the journal photos with keys equal to the traveler ID.
		 */
		static function getJournalPhotosCount($traveler_ids,$cobrandGroupID=NULL){
			if (!empty($traveler_ids)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					$photos_count = array();
					
					if( is_null($cobrandGroupID) ){
						$sql = "SELECT COUNT(tblTravelLogtoPhoto.photoID) AS count, tblTravel.travelerID " .
							" FROM tblTravel, tblTravelLink, tblTravelLog, tblTravelLogtoPhoto " .
							" WHERE tblTravel.deleted = 0 " .
							" AND tblTravel.travelerID IN ($traveler_ids)" .
							" AND tblTravelLink.travellinkID = tblTravel.travellinkID " .
							" AND tblTravelLink.reftype = 1 ".
							" AND tblTravel.travelID = tblTravelLog.travelID" .
							" AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID " .
							" AND tblTravelLog.deleted = 0 " .
							" GROUP BY travelerID ";
					}else{
						$sql = "SELECT COUNT(tblTravelLogtoPhoto.photoID) AS count, tblTravel.travelerID " .
							" FROM tblGroupApprovedJournals, tblTravel, tblTravelLink, tblTravelLog, tblTravelLogtoPhoto " .
							" WHERE tblGroupApprovedJournals.groupID = $cobrandGroupID ".
							" AND tblGroupApprovedJournals.approved = 1 ".
							" AND tblGroupApprovedJournals.travelID = tblTravel.travelID ".
							" AND tblTravel.deleted = 0 " .
							" AND tblTravel.travelerID IN ($traveler_ids)" .
							" AND tblTravelLink.travellinkID = tblTravel.travellinkID " .
							" AND tblTravelLink.reftype = 1 ".
							" AND tblTravel.travelID = tblTravelLog.travelID" .
							" AND tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID " .
							" AND tblTravelLog.deleted = 0 " .
							" GROUP BY travelerID ";
					}
					$records = new ResourceIterator($handler->execute($sql));
					foreach($records as $record) {
						$photos_count[$record['travelerID']] = $record['count'];
					}
					
					return $photos_count;
				}
				catch (exception $ex) {}
			}
		}	
		
		/**
		 * Returns an associative array of counts of the profile photos with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of counts of the profile photos with keys equal to the traveler ID.
		 */
		static function getProfilePhotosCount($traveler_ids){
			if (!empty($traveler_ids)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					$photos_count = array();
					
					$sql = "SELECT COUNT(tblPhoto.photoID) AS count, tblTravelertoPhoto.travelerID " .
						" FROM tblPhoto, tblTravelertoPhoto " .
						" WHERE tblTravelertoPhoto.photoID = tblPhoto.photoID" .
						" AND tblTravelertoPhoto.travelerID IN ($traveler_ids)" .
						" GROUP BY travelerID ";
						
					$records = new ResourceIterator($handler->execute($sql));
					foreach($records as $record) {
						$photos_count[$record['travelerID']] = $record['count'];
					}
					return $photos_count;
				}
				catch (exception $ex) {}
			}
		}		
		
		/**
		 * Returns an associative array of counts of the album photos with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of counts of the album photos with keys equal to the traveler ID.
		 */
		static function getAlbumPhotosCount($traveler_ids){
			if (!empty($traveler_ids)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					$photos_count = array();
					
					$sql = "SELECT COUNT(*) AS count, tblPhotoAlbum.creator " .
						" FROM tblPhotoAlbum, tblPhotoAlbumtoPhoto " .
						" WHERE tblPhotoAlbum.photoAlbumID = tblPhotoAlbumtoPhoto.photoAlbumID" .
						" AND tblPhotoAlbum.groupID = 0 " .
						" AND tblPhotoAlbum.creator IN ($traveler_ids)" .
						" GROUP BY creator ";
						
					$records = new ResourceIterator($handler->execute($sql));
					foreach($records as $record) {
						$photos_count[$record['creator']] = $record['count'];
					}
					return $photos_count;
				}
				catch (exception $ex) {}
			}
		}	
		
		/**
		 * Returns an associative array of counts of the article photos with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of counts of the article photos with keys equal to the traveler ID.
		 */
		static function getArticlePhotos($traveler_ids){
			if (!empty($traveler_ids)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					$photos_count = array();
					
					$sql = "SELECT COUNT(atp.photoID) AS count, a.authorID " .
						" FROM tblArticletoPhoto as atp, tblArticle as a " .
						" WHERE atp.articleID = a.articleID " .
						" AND a.groupID = 0 " .
						" AND a.deleted = 0 " .
						" AND a.authorID IN ($traveler_ids)" .
						" GROUP BY authorID ";
						
					$records = new ResourceIterator($handler->execute($sql));
					foreach($records as $record) {
						$photos_count[$record['authorID']] = $record['count'];
					}
					return $photos_count;
				}
				catch (exception $ex) {}
			}
		}
		
		/**
		 * Returns an associative array of the total number of photos with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of the total number of photos with keys equal to the traveler ID.
		 */
		static function getTotalCountOfPhotos($traveler_ids,$cobrandGroupID=NULL){
			if (!empty($traveler_ids)) {
				$journal_photos = self::getJournalPhotosCount($traveler_ids,$cobrandGroupID);
				$profile_photos = self::getProfilePhotosCount($traveler_ids);
				$album_photos 	= self::getAlbumPhotosCount($traveler_ids);
				$article_photos	= self::getArticlePhotos($traveler_ids);
				$photos 		= array();
				
				$travelerIDs = explode(", ", $traveler_ids);
				
				foreach ($travelerIDs as $id) {
					$count 		= 0;
					
					if (array_key_exists($id, $journal_photos))
						$count = $count + $journal_photos[$id];
					if (array_key_exists($id, $profile_photos))
						$count = $count + $profile_photos[$id];
					if (array_key_exists($id, $album_photos))
						$count = $count + $album_photos[$id];
					if (array_key_exists($id, $article_photos))
						$count = $count + $article_photos[$id];	
					
					$photos[$id] = $count; 
				}
				
				return $photos;
			}
		}
		
		/**
		 * Returns an associative array of counts of the journal videos with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of counts of the journal videos with keys equal to the traveler ID.
		 */
		static function getJournalVideosCount($traveler_ids,$cobrandGroupID=NULL){
			if (!empty($traveler_ids)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					$videos_count = array();
					
					if( is_null($cobrandGroupID) ){
						$sql = "SELECT COUNT(tblTravelLogtoVideo.videoID) AS count, tblTravel.travelerID " .
							" FROM tblTravel, tblTravelLog, tblTravelLogtoVideo, tblTravelLink " .
							" WHERE tblTravel.deleted = 0 " .
							" AND tblTravelLog.deleted = 0 " .
							" AND tblTravel.travellinkID = tblTravelLink.travellinkID " .
							" AND tblTravelLink.refID IN ($traveler_ids) " .
							" AND tblTravelLink.reftype = 1 " .
							" AND tblTravelLogtoVideo.travellogID = tblTravelLog.travellogID " .
							" AND tblTravelLog.travelID = tblTravel.travelID " .
							" GROUP BY travelerID ";
 					}else{
						$sql = "SELECT COUNT(tblTravelLogtoVideo.videoID) AS count, tblTravel.travelerID " .
							" FROM tblGroupApprovedJournals, tblTravel, tblTravelLink, tblTravelLog, tblTravelLogtoVideo " .
							" WHERE tblGroupApprovedJournals.groupID = $cobrandGroupID ".
							" AND tblGroupApprovedJournals.approved = 1 ".
							" AND tblGroupApprovedJournals.travelID = tblTravel.travelID ".
							" AND tblTravel.deleted = 0 " .
							" AND tblTravelLog.deleted = 0 " .
							" AND tblTravel.travellinkID = tblTravelLink.travellinkID " .
							" AND tblTravelLink.reftype = 1 " .
							" AND tblTravel.travelerID IN ($traveler_ids) " .
							" AND tblTravelLogtoVideo.travellogID = tblTravelLog.travellogID " .
							" AND tblTravelLog.travelID = tblTravel.travelID " .
							" GROUP BY travelerID ";
					}
					$records = new ResourceIterator($handler->execute($sql));
					foreach($records as $record) {
						$videos_count[$record['travelerID']] = $record['count'];
					}
					
					return $videos_count;
				}
				catch (exception $ex) {}
			}
		}
		
		/**
		 * Returns an associative array of counts of the album videos with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of counts of the album videos with keys equal to the traveler ID.
		 */
		static function getAlbumVideosCount($traveler_ids){
			if (!empty($traveler_ids)) {
				try {
					$handler = GanetDbHandler::getDbHandler();
					$videos_count = array();
					
					$sql = "SELECT COUNT(*) AS count, tblVideoAlbum.creatorID " .
						" FROM tblVideoAlbum, tblVideo " .
						" WHERE tblVideoAlbum.albumID = tblVideo.albumID " .
						" AND tblVideoAlbum.creatorType = 'traveler' " .
						" AND tblVideoAlbum.creatorID IN ($traveler_ids)" .
						" GROUP BY creatorID ";
 	
					$records = new ResourceIterator($handler->execute($sql));
					foreach($records as $record) {
						$videos_count[$record['creatorID']] = $record['count'];
					}
					
					return $videos_count;
				}
				catch (exception $ex) {}
			}
		}
		
		/**
		 * Returns an associative array of the total number of videos with keys equal to the traveler ID.
		 * If the traveler ID is not set that means, the count is 0.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the count.
		 * 
		 * @return array an associative array of the total number of videos with keys equal to the traveler ID.
		 */
		static function getTotalCountOfVideos($traveler_ids,$cobrandGroupID=NULL){
			if (!empty($traveler_ids)) {
				$journal_videos = self::getJournalVideosCount($traveler_ids,$cobrandGroupID);
				$album_videos 	= self::getAlbumVideosCount($traveler_ids);
				$videos 		= array();
				
				$travelerIDs = explode(", ", $traveler_ids);
				
				foreach ($travelerIDs as $id) {
					$count 		= 0;
					
					if (array_key_exists($id, $journal_videos))
						$count = $count + $journal_videos[$id];
					if (array_key_exists($id, $album_videos))
						$count = $count + $album_videos[$id];
					
					$videos[$id] = $count; 
				}
				
				return $videos;
			}
		}
		
		/**
		 * Returns an associative array of Photo info with keys equal to the traveler ID.
		 * 
		 * @param string $traveler_ids The IDs of the traveler to be get the primary photos.
		 * 
		 * @return array an associative array of Photo info with keys equal to the traveler ID.
		 */
		static function getPrimaryPhotos($traveler_ids){
			if (!empty($traveler_ids)) {
				require_once("travellog/model/Class.Photo.php");
				
				try {
					$handler = GanetDbHandler::getDbHandler();
					$photos = array();
					
					$sql = "SELECT tblPhoto.*, tblTravelertoPhoto.travelerID " .
						" FROM tblTravelertoPhoto, tblPhoto" .
						" WHERE tblTravelertoPhoto.photoID = tblPhoto.photoID" .
						" AND tblPhoto.primaryphoto = 1 " .
						" AND tblTravelertoPhoto.travelerID IN ($traveler_ids)" .
						" GROUP BY tblTravelertoPhoto.travelerID";
					
					$records = new ResourceIterator($handler->execute($sql));
					foreach($records as $record) {
						$photos[$record['travelerID']] = $record;
					}
					
					return $photos;
				}
				catch (exception $ex) {}
			}
		}	
	}
	
