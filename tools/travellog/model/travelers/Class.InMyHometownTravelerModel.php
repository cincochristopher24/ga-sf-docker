<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once("travellog/model/Class.RowsLimit.php");
require_once("travellog/model/Class.Traveler.php");
require_once('travellog/model/Class.IModel.php');
class InMyHometownTravelerModel implements IModel{
	function GetFilterLists(&$props){
		$obj_rows_limit = new RowsLimit($props['rows'], $props['offset']);
		$obj_traveler = new Traveler($props['travelerID']);
		
		$props['travelers']  = $obj_traveler->getOtherTravelersInMyHomeTown(NULL, $obj_rows_limit);
		$props['travelers_count'] = $obj_traveler->getNumberOfTravelers();
		$props['obj_paging']   = new Paging($props['travelers_count'], $props['page'],'action=InMyHometown', $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                                                  );
		$props['view_type']    = 2;
	}	
}
?>
