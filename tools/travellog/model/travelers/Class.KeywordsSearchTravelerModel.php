<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once("Class.Criteria2.php");
require_once('travellog/model/Class.IModel.php');
class KeywordsSearchTravelerModel implements IModel{
	
	/***********************************************************************
	 * edits by neri: 	
	 * 	 	if traveler is logged in, set travelerID as 2nd parameter 
	 * 				in getAllTravelers() and performSearch():		11-17-08
	 * 		excluded interest in categories of search by:			12-03-08
	 * 		added action=search in query string for paging			12-05-08
	 * 		added addslashes to keyword parameter if
	 * 				search_by = 1									12-17-08					
	 ***********************************************************************/ 
	
	function GetFilterLists(&$props){
		$obj_criteria = new Criteria2;
		$obj_criteria->setLimit($props['offset'], $props['rows']);

		if(isset($props['keywords'])) $props['keywords'] = trim($props['keywords']);

		if( $props['search_by'] == 1 ){
			$obj_criteria->setOrderBy( 'username' );
			$obj_criteria->mustBeLike( 'username', addslashes($props['keywords']) );
		}
		elseif( $props['search_by'] == 2 ){
			$obj_criteria->setOrderBy( 'email' );
			$obj_criteria->mustBeLike( 'email', $props['keywords'] );
		}
		
		if( $props['search_by'] == 4 ){
			require_once('travellog/model/Class.SearchFirstandLastnameService.php');
			$obj_traveler  = new SearchFirstandLastnameService;		
			$props['travelers'] = $obj_traveler->performSearch($props['keywords'], $props['travelerID']);	
			$props['travelers_count']  = count($props['travelers']);  	
		}
		else{
			require_once('travellog/model/Class.TravelerSearchCriteria2.php');
			$obj_traveler  = new TravelerSearchCriteria2;
			$props['travelers'] = $obj_traveler->getAllTravelers($obj_criteria, $props['travelerID']);			
			$props['travelers_count']  = $obj_traveler->getTotalRecords();  
		}
		
		$props['obj_paging']   = new Paging        ($props['travelers_count'], $props['page'],'action=search&keywords='.$props['keywords'].'&searchby='.$props['search_by'], $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                                                                    );
	}
}
?>
