<?php
class TravelersModelFactory{
	
	private static $instance = NULL;
	
	private $models = array();
	
	static function getInstance(){ 
		if( self::$instance == NULL ) self::$instance = new TravelersModelFactory; 
		return self::$instance;
	}
	
	function createModel($type){
		
		$obj_factory = FileFactory::getInstance();
		
		switch(strtolower($type)){
			case 'all':
				if( !array_key_exists('AllTravelers', $this->models) ){
					$this->models['AllTravelers'] = $obj_factory->getClass('AllTravelers');
				}
				return $this->models['AllTravelers'];
			break;
			
			case 'newest':
				if( !array_key_exists('NewestTravelers', $this->models) ){
					$this->models['NewestTravelers'] = $obj_factory->getClass('NewestTraveler');
				}
				return $this->models['NewestTravelers'];
			break;
			
			case 'mostpopular':
				if( !array_key_exists('PopularTravelers', $this->models) ){
					$this->models['PopularTravelers'] = $obj_factory->getClass('PopularTravelers');
				}
				return $this->models['PopularTravelers'];
			break;
			
			case 'search':
				if( !array_key_exists('SearchTravelers', $this->models) ){
					$this->models['SearchTravelers'] = $obj_factory->getClass('SearchTravelers');
				}
				return $this->models['SearchTravelers'];
			break;
			
			case 'lastlogin':
				if( !array_key_exists('LastLoginTravelers', $this->models) ){
					$this->models['LastLoginTravelers'] = $obj_factory->getClass('LastLoginTravelers');
				}
				return $this->models['LastLoginTravelers'];
			break;
			
			case 'inmyhometown':
				if( !array_key_exists('InMyHometownTravelers', $this->models) ){
					$this->models['InMyHometownTravelers'] = $obj_factory->getClass('InMyHometownTravelers');
				}
				return $this->models['InMyHometownTravelers'];
			break;
			
			case 'hometown':
				if( !array_key_exists('HometownTravelers', $this->models) ){
					$this->models['HometownTravelers'] = $obj_factory->getClass('HometownTravelers');
				}
				return $this->models['HometownTravelers'];
			break;
			
			case 'currentlyin': 
				if( !array_key_exists('CurrentTravelers', $this->models) ){
					$this->models['CurrentTravelers'] = $obj_factory->getClass('CurrentTravelers');
				}
				return $this->models['CurrentTravelers'];
			break;
			
			case 'planningtogo': 
				if( !array_key_exists('PlanningToGoTo', $this->models) ){
					$this->models['PlanningToGoTo'] = $obj_factory->getClass('PlanningToGoTo');
				}
				return $this->models['PlanningToGoTo'];
			break;
			
			case 'search': 
				if( !array_key_exists('KeywordsSearch', $this->models) ){
					$this->models['KeywordsSearch'] = $obj_factory->getClass('KeywordsSearchModel');
				}
				return $this->models['KeywordsSearch'];
			break;
			
			case 'locationidlist': 
				if( !array_key_exists('LocationIDList', $this->models) ){
					$this->models['LocationIDList'] = $obj_factory->getClass('LocationIDList');
				}
				return $this->models['LocationIDList']; 
			break;
			
			case 'countryphotoscount': 
				if( !array_key_exists('CountryPhotosCount', $this->models) ){
					$this->models['CountryPhotosCount'] = $obj_factory->getClass('CountryPhotosCount');
				}
				return $this->models['CountryPhotosCount']; 
			break;
			
			case 'travelerlocationcount': 
				if( !array_key_exists('TravelerLocationCount', $this->models) ){
					$this->models['TravelerLocationCount'] = $obj_factory->getClass('TravelerLocationCount');
				}
				return $this->models['TravelerLocationCount']; 
			break;
			
			case 'entrieswritencount': 
				if( !array_key_exists('EntriesWritenCount', $this->models) ){
					$this->models['EntriesWritenCount'] = $obj_factory->getClass('EntriesWritenCount');
				}
				return $this->models['EntriesWritenCount']; 
			break;
			
			case 'travelersmap': 
				if( !array_key_exists('TravelersMap', $this->models) ){
					$this->models['TravelersMap'] = $obj_factory->getClass('TravelersMap');
				}
				return $this->models['TravelersMap']; 
			break;
			
			case 'map': 
				if( !array_key_exists('Map', $this->models) ){
					$this->models['Map'] = $obj_factory->getClass('Map');
				}
				return $this->models['Map']; 
			break;
			
			case 'mapframe': 
				if( !array_key_exists('MapFrame', $this->models) ){
					$this->models['MapFrame'] = $obj_factory->getClass('MapFrame');
				}
				return $this->models['MapFrame']; 
			break;
			
			case 'havebeento': 
				if( !array_key_exists('HaveBeenTo', $this->models) ){
					$this->models['HaveBeenTo'] = $obj_factory->getClass('HaveBeenTo');
				}
				return $this->models['HaveBeenTo'];
			break;
		}
	}
}
?>