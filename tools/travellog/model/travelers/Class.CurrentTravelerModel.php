<?php
	require_once("Class.Paging.php");
	require_once("travellog/model/Class.GanetDbHandler.php");
	require_once("travellog/model/Class.IModel.php");
	require_once("Class.Criteria2.php");	

	class CurrentTravelerModel implements IModel {
		function GetFilterLists(&$props){		
			try {
				$handler = GanetDbHandler::getDbHandler();
				
				/*$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM tblTraveler " .
					" WHERE tblTraveler.isSuspended = 0 " .
					" AND tblTraveler.active = 1 " .
					" AND tblTraveler.travelerID NOT IN " .
					"	( " .
					"		SELECT travelerID FROM tblGrouptoAdvisor " .
					" ) " .
					"	AND tblTraveler.currlocationID IN ({$props['locationIDList']}) ";*/
				$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM tblTraveler " .
					" WHERE tblTraveler.isSuspended = 0 " .
					" AND tblTraveler.active = 1 " .
					"	AND tblTraveler.currlocationID IN ({$props['locationIDList']}) ";
				if($props['is_login']) {
					$sql .= " AND travelerID <> " .$handler->makeSqlSafeString($props['travelerID']).
						" AND travelerID NOT IN " .
						" ( " .
						"		SELECT userID as travelerID FROM tblBlockedUsers " .
						"		WHERE travelerID = ".$handler->makeSqlSafeString($props['travelerID'])."" .
						" ) ";
				}			
				$sql .= "	LIMIT ".$props['offset'].", ".$props['rows'];
				
				$props['travelers'] = new ResourceIterator($handler->execute($sql), "Traveler");
				
				$sql = "SELECT FOUND_ROWS() as rows";
				
				$rs = new ResourceIterator($handler->execute($sql));
				$props['travelers_count'] = $rs->getRows();
				$props['obj_paging']   = new Paging($props['travelers_count'], $props['page'],'action=CurrentlyIn&locationID='.$props['locationID'], $props['rows'] );
				$props['view_type'] = 6;
			}
			catch (exception $ex) {}
		}
		
		function GetTravelersCountryWithCurrentLocation(){
			require_once("Class.Connection.php");
			require_once("Class.Recordset.php");
			$conn          = new Connection();
			$rs            = new Recordset($conn);
			$arr_contents  = array();
			
			/*$sql = "SELECT locID AS locationID, country AS name " .
				" FROM " .
				"	(" .
				"		SELECT GoAbroad_Main.tbcountry.locID, GoAbroad_Main.tbcountry.country, tblTraveler.travelerID " .
				"		FROM tblTraveler, GoAbroad_Main.tbcountry " .
				"		WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcountry.locID " .
				"		AND GoAbroad_Main.tbcountry.locID > 0 " .
				"		UNION " .
				"		SELECT GoAbroad_Main.tbcountry.locID, GoAbroad_Main.tbcountry.country, tblTraveler.travelerID" .
				"		FROM tblTraveler, GoAbroad_Main.tbcountry, GoAbroad_Main.tbcity" .
				"		WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcity.locID" .
				"		AND GoAbroad_Main.tbcountry.countryID = GoAbroad_Main.tbcity.countryID" .
				"		AND GoAbroad_Main.tbcity.locID > 0" .
				"	) AS temp" .
				"	WHERE travelerID NOT IN " .
				"	(" .
				"		SELECT DISTINCT travelerID" .
				"		FROM tblGrouptoAdvisor" .
				"	)" .
				"	GROUP BY locationID" .
				"	ORDER BY name";*/
			$sql = "SELECT locID AS locationID, country AS name " .
				" FROM " .
				"	(" .
				"		SELECT GoAbroad_Main.tbcountry.locID, GoAbroad_Main.tbcountry.country, tblTraveler.travelerID " .
				"		FROM tblTraveler, GoAbroad_Main.tbcountry " .
				"		WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcountry.locID " .
				"		AND GoAbroad_Main.tbcountry.locID > 0 " .
				"		UNION " .
				"		SELECT GoAbroad_Main.tbcountry.locID, GoAbroad_Main.tbcountry.country, tblTraveler.travelerID" .
				"		FROM tblTraveler, GoAbroad_Main.tbcountry, GoAbroad_Main.tbcity" .
				"		WHERE tblTraveler.currlocationID = GoAbroad_Main.tbcity.locID" .
				"		AND GoAbroad_Main.tbcountry.countryID = GoAbroad_Main.tbcity.countryID" .
				"		AND GoAbroad_Main.tbcity.locID > 0" .
				"	) AS temp" .
				"	GROUP BY locationID" .
				"	ORDER BY name";		
			$rs->Execute($sql);
			if ($rs->Recordcount()){
				$arr_contents[] = array('locationID' => 0, 'name' => '- select a country -');
				while($row = mysql_fetch_assoc($rs->Resultset())){
					if( $row['locationID'] > 0){
						$arr_contents[] = $row; //array('locationID' => $row['locID'], 'name' => $row['country']); 
					}
				}
			}	
			
			return $arr_contents;
		}
		
		function GetCountriesWithCurrentLocationJSON(){
			require_once('JSON.php');
			$json     = new Services_JSON(SERVICES_JSON_IN_ARR);
			$contents = $json->encode( $this->GetTravelersCountryWithCurrentLocation() );
			return $contents;
		}
	}