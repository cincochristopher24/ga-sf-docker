<?php
require_once('travellog/repository/Class.TravelersPlanningToGo.php');
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/model/Class.IModel.php');
require_once("Class.Criteria2.php");
class TravelersPlanningToGoModel implements IModel{
	function GetFilterLists(&$props){
		$props['obj_criteria'] = new Criteria2;
		if( $props['is_login'] ) $props['obj_criteria']->mustNotBeEqual('travelerID', $props['travelerID']);
		$props['obj_criteria']->setGroupBy('travelerID');
		$props['obj_criteria']->setOrderBy('ranking, travelerID');
		$props['obj_criteria']->setLimit($props['offset'], $props['rows']);
		$obj_traveler          = new TravelersPlanningToGo;	
		$props['travelers'] = $obj_traveler->GetFilterList($props);
		$props['travelers_count'] = $obj_traveler->getTotalRecords();
		$props['obj_paging'] = new Paging($props['travelers_count'], $props['page'],'action=PlanningToGo', $props['rows'] );
		$props['view_type']    = 7;
	}
	
	function GetTravelersCountryWithFutureTravels(){
		$obj_traveler = new TravelersPlanningToGo;	
		return $obj_traveler->GetTravelersCountryWithFutureTravels();
	}
	
	function GetTravelersCountryWithFutureTravelsJSON(){
		$obj_traveler = new TravelersPlanningToGo;	
		return $obj_traveler->GetTravelersCountryWithFutureTravelsJSON();
	}
}
?>
