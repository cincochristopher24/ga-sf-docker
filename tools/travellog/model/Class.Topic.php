<?php  
	/**
	 * @(#) Class.Topic.php
	 * 
	 * Autogenerated by GoAbroad Code Generator 1.0
	 *
	 * @version 1.0 June 17, 2009 
	 */
	
	require_once('travellog/model/base/Class.BaseTopic.php');
	
	/**
	 * Model class for topic.
	 */
	class Topic extends BaseTopic {
		
		/**
		 * The related group of the topic.
		 * 
		 * @var AdminGroup
		 */
		protected $mGroup = null;
		
		/**
		 * The creator of the group.
		 * 
		 * @var Traveler
		 */
		protected $mCreator = null;
		
		/**
		 * The first discussion of the topic.
		 * 
		 * @var Discussion
		 */
		protected $mFirstDiscussion = null;
		
		/**
		 * The latest discussion of the topic.
		 * 
		 * @var Discussion
		 */
		protected $mLatestDiscussion = null;
		
		/**
		 * Returns the creator of the topic.
		 * 
		 * @return Traveler the creator of the topic
		 */
		function getCreator(){
			if (is_null($this->mCreator) OR $this->mCreator->getTravelerID() != $this->mCreatorId) {
				require_once('travellog/model/Class.TravelerPeer.php');
				
				$this->mCreator = TravelerPeer::retrieveByPk($this->mCreatorId);
			}
			
			return $this->mCreator;
		}
		
		/**
		 * Returns the first discussion of the topic.
		 * 
		 * @return Discussion the first discussion of the topic.
		 */
		function getFirstDiscussion(){
			if (is_null($this->mFirstDiscussion)) {
				require_once('travellog/model/Class.DiscussionPeer.php');
				
				$criteria = new SqlCriteria2();
				$criteria->addOrderByColumn(DiscussionPeer::DATE_UPDATED, SqlCriteria2::ASC);
				$discussions = $this->getDiscussions($criteria);
				$this->mFirstDiscussion = (0 < count($discussions)) ? $discussions[0] : null;
			} 
			
			return $this->mFirstDiscussion;
		}
		
		/**
		 * Returns the related group of the topic.
		 * 
		 * @return AdminGroup the related group of the topic
		 */
		function getGroup(){
			if (is_null($this->mGroup) OR $this->mGroup->getGroupID() != $this->mGroupId) {
				require_once('travellog/model/Class.GroupPeer.php');
				
				$this->mGroup = GroupPeer::retrieveByPk($this->mGroupId);
			}
			
			return $this->mGroup;
		}
		
		/**
		 * Returns the first discussion of the topic.
		 * 
		 * @return Discussion the first discussion of the topic.
		 */
		function getLatestDiscussion(){
			if (is_null($this->mLatestDiscussion)) {
				require_once('travellog/model/Class.DiscussionPeer.php');
				
				$criteria = new SqlCriteria2();
				$criteria->addOrderByColumn(DiscussionPeer::DATE_UPDATED, SqlCriteria2::DESC);
				$discussions = $this->getDiscussions($criteria);
				$this->mLatestDiscussion = (0 < count($discussions)) ? $discussions[0] : null;
			} 
			
			return $this->mLatestDiscussion;
		}
		
		/**
		 * Returns true if the topic is active.
		 * 
		 * @return boolean true if the topic is active. False otherwise.
		 */
		function isActive(){
			return ($this->getStatus() == TopicPeer::ACTIVE);
		}
		
		/**
		 * Returns true if the topic is archived.
		 * 
		 * @return boolean true if the topic is archived. False otherwise.
		 */
		function isArchived(){
			return ($this->getStatus() == TopicPeer::ARCHIVED);
		}
		
		/**
		 * Sets the related creator of the group.
		 * 
		 * @param Traveler $val the related creator of the group.
		 * 
		 * @return void
		 */
		function setCreator(Traveler $val){
			$this->mCreator = $val;
		}		
		
		/**
		 * Sets the related group of the topic.
		 * 
		 * @param AdminGroup $val the related group of the topic 
		 * 
		 * @return void
		 */
		function setGroup(AdminGroup $val){
			$this->mGroup = $val;
		}
		
	}