<?php

	class ObjectsRepo{
	
		const 	TRAVELER = 'traveler',
				GROUP = 'group';
		
		private static
			$traveler = array(),
			$journal = array(),
			$journalEntry = array(),
			$group = array();
		
		public static function getTraveler($travelerID){
			if( array_key_exists($travelerID, self::$traveler) )
				return self::$traveler[$travelerID];
			else
				return null;	
		}
		
		public static function getJournal($journalID){
			if( array_key_exists($journalID, self::$journal) )
				return self::$journal[$journalID];
			else
				return null;
		}
		
		public static function getJournalEntry($journalEntryID){
			if( array_key_exists($journalEntryID, self::$journalEntry) )
				return self::$journalEntry[$journalEntryID];
			else
				return null;
		} 
		
		public static function getGroup($groupID){
			if( array_key_exists($groupID, self::$group) )
				return self::$group[$groupID];
			else
				return null;
		}
		
		public static function addToRepo($type, $obj){
			switch($type){
				case self::TRAVELER:
					self::$traveler[$obj->getTravelerID()] = $obj;
					break;
				case self::GROUP:
					self::$group[$obj['groupID']] = $obj;
					break;	 
			}
		}
		
		public static function replaceFromRepo($type, $obj){
			switch($type){
				case self::TRAVELER:
					self::$traveler[$obj->getTravelerID()] = $obj;
					break;
				case self::GROUP:
					if( array_key_exists($obj->getGroupID(), self::$group) )
						self::$group[$obj->getGroupID()] = $obj;
					break;	 
			}
		}
				
	}
	
?>
