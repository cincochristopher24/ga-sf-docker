<?php
/**
* <b>SearchFirstandLastnameService</b> class
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/

require_once("travellog/model/Class.GanetDbHandler.php");
require_once("Class.ResourceIterator.php");

class SearchFirstandLastnameService{
	
	function performSearch($value, $loggedTravelerID = 0){
		try {
			$handler = GanetDbHandler::getDbHandler();
			
			$sql  = "SELECT * FROM tblTraveler WHERE active > 0 AND isSuspended = 0 " .
				" AND MATCH (`firstname`,`lastname`) AGAINST ('$value')";
			$sql .= (!is_numeric($loggedTravelerID) OR 0 == $loggedTravelerID) ? "" : " AND travelerID <> " .$loggedTravelerID.
				" AND travelerID NOT IN (SELECT userID as travelerID FROM tblBlockedUsers WHERE travelerID = {$loggedTravelerID}) ";
			//$sql .= " AND travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ";
			
			return new ResourceIterator($handler->execute($sql), "Traveler");
		}
		catch (exception $ex) {}
	}
}
?>
