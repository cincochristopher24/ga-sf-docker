<?php
/*
 * Created on Aug 7, 2006
 * Edited by Daphne on Aug 7, removed the "final" keyword after "public static", because an error is thrown:
 * Fatal error: Cannot declare property FilterOp::$EQUAL final, the final modifier is allowed only for methods in /opt/lampp/apps/travellog/model/Class.FilterOp.php on line 7
 */


// added by adelbert
// purpose: to avoid redeclaration of class. 
// Temporary remedy..
if(!class_exists('FilterOp')) {

	class FilterOp {
	 	public static  $EQUAL = 1;
	 	public static  $NOT_EQUAL = 2;
	 	public static  $GREATER_THAN = 3;
	 	public static  $GREATER_THAN_OR_EQUAL = 4;
	 	public static  $LESS_THAN = 5;
	 	public static  $LESS_THAN_OR_EQUAL = 6;
	 	public static  $ORDER_BY = 7;
	 	public static  $ORDER_BY_DESC = 8; 	
	 	public static  $LIKE = 9; 	
	 	public static  $GROUP_BY = 10;
	 	public static  $IN = 11; 	
	 	public static  $NOT_IN = 12;
	 	//public static  $START_ROW = 13;						// start row offset when accessing lists or resultset
	 	//public static  $ROWS_PER_PAGE = 14;					// no. of rows per page	
	}
	
}
 
?>
