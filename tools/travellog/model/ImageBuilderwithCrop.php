<?php


/**
 * Created on Sep 22, 2006
 * @author Joel C. LLano
 * Purpose: 
 */

require_once("Class.ResizeImage.php");
require_once("Class.CropCanvas.php");

CLass ImageBuilderwithCrop{
	
	private $imagefile;
	private $filename;	
	private $imagetype;
	private $destination;
	private $width;
	private $height;
	
	
	
	function ImageBuilderwithCrop(){
			
	}
	
		
	function setImageFile($imagefile){
		$this->imagefile = $imagefile;
	}
	
	
	function setFilename($filename){
		$this->filename = $filename;
	}
	
		
	function setImagetype($imagetype){
		$this->imagetype = $imagetype;
	}
	
	function setDestination($destination){
		$this->destination = $destination;	
	}
	
	function setArea($area){
		$this->area = $area;
	}
	
	
	
	function build($alias){
			
			
			$image = new ResizeImage();
			$image->setArea('canvas');
			
			
			copy($this->imagefile,$this->destination.$alias.$this->filename);
			
			list($width_orig, $height_orig) = getimagesize($this->destination.$alias.$this->filename);		
				
			/**#############################
			# get the bigger value 
			# of two parameter ($this->width,$this->height)
			# then add % value.
			############*/
			
			//($this->width>=$this->height)? $setnewsize = ($this->width *0.35)+$this->width : $setnewsize = ($this->height *0.35)+$this->height;
			
			($this->width>=$this->height)? $setnewsize = $this->width : $setnewsize = $this->height;
			
			/**###########
			# Resize pic to $setnewsize value
			###########*/
				
			if($width_orig >= $setnewsize || $height_orig >= $setnewsize) {						
						$image->resize($setnewsize,$width_orig,$height_orig,$this->destination.$alias.$this->filename,$this->imagetype);						
			}			
			
			/**###############
			# Then crop the resized pic 
			# to the parameter given ($this->width, $this->height).
			################*/
			
			list($width_orig, $height_orig) = getimagesize($this->destination.$alias.$this->filename);	
			
			$cropcanvass 	= new CropCanvas();					
			$cropcanvass->loadImage($this->destination.$alias.$this->filename);						
			($width_orig <= $this->width)? $cropwidth = $width_orig:$cropwidth =  $this->width;
			($height_orig <= $this->height)?	$cropheight = $height_orig: $cropheight = $this->height;																
			
			/*
			$filelocation="action_log.txt"; 
			$newfile = fopen($filelocation,"w"); 
			$add ="w=".$cropwidth."  h=".$cropheight;
			fwrite($newfile, $add); 
			fclose($newfile); 
			*/
			
			$cropcanvass->cropToSize($cropwidth, $cropheight);			
			
			/*
			if($this->width == $this->height){
				$cropcanvass->cropToSize($cropwidth, $cropheight);						
			}elseif($this->width > $this->height){
				$cropcanvass->cropToSize($cropwidth, $cropheight,ccTOP);
			}			
			*/
						
			//crop from top			
			//$cropcanvass->cropToDimensions(10, 0, $cropwidth, $cropheight);			
			
			$cropcanvass->saveImage($this->destination.$alias.$this->filename);												
			
			list($width_new, $height_new) = getimagesize($this->destination.$alias.$this->filename);		                
	        
	        ($this->width>=$this->height)? $newsize = $this->width : $newsize = $this->height;		                
			
			if($width_new >= $newsize || $height_new >= $newsize) {										
					$image->resize(($newsize),$width_new,$height_new,$this->destination.$alias.$this->filename,$this->imagetype);	
			}
			
			
						
	}
	
	
	function createFullSize($width,$height){
			$this->width 	= $width;
			$this->height = $height;
			
			$image = new ResizeImage();			
				
			copy($this->imagefile,$this->destination."fullsize".$this->filename);
			list($width_orig, $height_orig) = getimagesize($this->destination."fullsize".$this->filename);		
				
			($this->width>=$this->height)? $setnewsize = $this->width : $setnewsize = $this->height;
								
			if($width_orig >= $setnewsize || $height_orig >= $setnewsize) {						
						$image->resize($setnewsize,$width_orig,$height_orig,$this->destination."fullsize".$this->filename,$this->imagetype);						
			}
			
			
		
	}
	
	function createStandardSize($width,$height){
			$this->width 	= $width;
			$this->height = $height;
			$this->build("standard");
	}
	
	function createDefaultSize($width,$height){
			$this->width 	= $width;
			$this->height = $height;
			$this->build("default");
	}
	
	function createThumbnailSize($width,$height){
			$this->width 	= $width;
			$this->height = $height;
			$this->build("thumbnail");
	}
	
	function createFeaturedlSize($width,$height){
			$this->width 	= $width;
			$this->height = $height;
			$this->build("featured");
	}
	
		
}

?>