<?php
	/**
	 * Author: Reynaldo Castellano III
	 * Date: January 20, 2008
	 */
	require_once("travellog/model/Class.MessageSpace.php");
	class MessageBoard
	{
		private $mMessageSpace = null;
		private $mDefBulletinParams = array(
			'owner' 	=> null,
			'viewer'	=> null,
			'rowsLimit'	=> null
		);
		
		public function __construct(){
			$this->mMessageSpace = new MessageSpace();	
		}
		
		/**
		 * Params:
		 * 	- owner  	: An instance of Group or Traveler that owns the bulletin board.
		 *  - viewer	: An instance of Traveler(Administrator, Staff, Moderator or plain Traveler) or null(public) that wants to view 
		 * 					the bulletin board.
		 */
		public function getBulletins($params=array()){
			$mergedParams = array_merge($this->mDefBulletinParams,$params);
			if(is_null($mergedParams['owner'])){
				throw new exception('Parameter owner was not set.');
			}
			if($mergedParams['owner'] instanceof Traveler){
				return $this->getTravelerBulletins($mergedParams);
			}
			elseif($mergedParams['owner'] instanceof Group){
				return $this->getGroupBulletins($mergedParams);
			}
			else{
				throw new exception('Invalid parameter owner type. Owner must be of type Traveler or Group.');
			}
		}
		
		private function getTravelerBulletins($params){
			//only the owner can view his or her own bulletins.
			return $params['owner']->getSendableID() == $params['viewer']->getSendableID() 
				? $this->mMessageSpace->getBulletins($params['owner']->getSendableID(),$params['rowsLimit'])
				: array(); 
		}
		
		private function getGroupBulletins($params){
			//All members of the group can view the bulletin board of that group.
			//An outsider(someone who is not logged or not a member of a group) can view the bulletin board of the group
			//if the bulletin privacy pref of the group allows it.
			/**
			try{
				$filter = $this->getGroupFilter($params);
			}
			catch(exception $e){
				return array();
			}
			**/
			return $this->mMessageSpace->getBulletins($params['owner']->getSendableID(),$params['rowsLimit']);
		}
		
		public function getBulletinAbsulotePosition($params){
			$mergedParams = array_merge($this->mDefBulletinParams,$params); 
			if(is_null($mergedParams['owner'])){
				throw new exception('Invalid bulletin owner.');
			}
			try{
				$filter = ($params['owner'] instanceof Group)
					? $this->getGroupFilter($mergedParams)
					: $this->getTravelerFilter($mergedParams);
			}
			catch(exception $e){
				return 0;
			}
			return $this->mMessageSpace->getBulletins($params['owner']->getSendableID(),null,$params['entryID']);
		}
		
		private function getTravelerFilter($params){
			return null;
		}
		
		private function getGroupFilter($params){
			$owner = $params['owner'];
			$viewer = $params['viewer'] ;
			$rowsLimit = $params['rowsLimit'];
			require_once('travellog/model/Class.GroupFactory.php');
			require_once('travellog/model/Class.GroupPrivacyPreference.php');
			require_once('travellog/model/Class.FilterCriteria2.php');
			
			
			//regardless if the user is a member or not, if the viewbulletins pref is false, then get all the bulletins.
			//If the view bulletins pref is true, then only members of the group can view the bulletins. Also only bulletins that has been posted after the 
			//member traveler's join date will be shown. 
			$localSendableID 	= $owner->getSendableID();
			$privacypref		= new GroupPrivacyPreference($owner->getGroupID());
			$viewBulletinPref 	= $privacypref->getViewBulletin();
			
			//1. if the viewer is null(which means that that viewer is not logged), and the group privacy pref does not allow it, return an empty array.
			//2. if the viewer is not null, we have to check if that viewer is a member of the group. If not, then the rule above is in effect.
			
			$filter = null;
			if(!$viewBulletinPref && (is_null($viewer) || !$owner->isMember($viewer))){
				throw exception('Viewer is not allowed to view the bulletin board.');	
			}
			else{
				$filter = new FilterCriteria2();
				$cond = new Condition();
				$cond->setAttributeName("dateCreated");
				$cond->setOperation(FilterOp::$GREATER_THAN_OR_EQUAL);
				$joinDate = $owner->getMembershipDate($viewer);
				$cond->setValue($joinDate);
				$filter->addCondition($cond);
			}
			return $filter;
		}
		
		public function getTotalRecords(){
			return $this->mMessageSpace->getTotalRecords();
		}
		
	}
?>