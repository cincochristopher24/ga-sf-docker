<?php
	/*
	 *	Created on Jul 26, 2006
	 *	@author Czarisse Daphne P. Dolina
	 *	Last Edited:	Cheryl Ivy Q. Go		24 October 2008
	 *	Purpose:		Optimized Class.Trip.php
	 */
 
 	require_once 'Class.Connection.php';
	require_once 'Class.Recordset.php';
	require_once 'Class.Criteria2.php';
	require_once 'travellog/model/Class.Group.php';
	require_once 'Class.ConnectionProvider.php';	
	require_once 'Cache/ganetCacheProvider.php';
 
 	class Trip{
 		
 		// vars for db connection
 		private $mConn		= NULL;
 		private $mRs		= NULL;
 		
 		// vars for parent ID
 		private $travelerID		= 0;
 		private $travelID		= 0;
 		
 		//vars for parent classes
 		private $traveler    = NULL;
 		private $travel    	 = NULL;
 		
 		private $owner    = NULL;
 		
 		// vars for class attributes
 		private $tripID       	= NULL;
 		private $arrival      	= NULL;
 		private $locationID 	= NULL;
 		private $newLocationID  = 0;
 		
 		private $travellinkID  = NULL;
 		
 		// vars for other methods
 		private $location     = NULL;
 		private $arrtravellog = array();
 		private $cache	=	null;

	 	function Trip($_tripID = 0, $_data = null){
 			$this->cache = ganetCacheProvider::instance()->getCache();

 			if (0 < $_tripID){
	 			$found = false;
 				if ($this->cache != null){
	 				$trip = $this->cache->get('Trip_' . $_tripID);
	 				if ($trip){
	 					$found = true;
	 					//echo 'Found Trip ' . $_tripID . ' <br/>';
	 					$this->tripID = $_tripID;
	 					$this->arrival 		= $trip->getArrival();
	 					$this->locationID 	= $trip->getLocationID();
			 			$this->travelerID	= $trip->getTravelerID(); 
	 					$this->travelID		= $trip->getTravelID();
	 					$this->travellinkID	= $this->getTravellinkID();
	 				}
	 			}
 				
 				if (!$found){
	 				$this->mRs   = new Recordset(ConnectionProvider::instance()->getConnection());
	 				
	 				$this->tripID = $_tripID;
	
		 			$sql = "SELECT tblTrips.*, tblTravel.travelerID, tblTravel.travellinkID " .
							"FROM tblTrips, tblTravel " .
							"WHERE tblTrips.travelID = tblTravel.travelID " .
							"AND tblTravel.deleted = 0 ".  // added by ianne 
							"AND tblTrips.tripID = " . $this->tripID;
		 			$this->mRs->Execute($sql);
	
		 			if ($this->mRs->Recordcount() == 0){
	 					throw new Exception("Invalid TripID");		// ID not valid so throw exception
	 				}
	 				
	 				// set values to its attributes
	 				$this->arrival 		= $this->mRs->Result(0,"arrival");
		 			$this->locationID 	= $this->mRs->Result(0,"locID");
					
					// set values to the parent ID
		 			$this->travelerID	= $this->mRs->Result(0,"travelerID"); 
		 			$this->travelID		= $this->mRs->Result(0,"travelID");
		 			
		 			$this->travellinkID	= $this->mRs->Result(0,"travellinkID");
		 			
		 			if ($this->cache != null){
		 				$this->cache->set('Trip_' . $_tripID, $this, array('EXPIRE'=>86400));
		 			}
 				}			 			
	 		}
			elseif(is_array($_data)){
				$this->arrival			= $_data['arrival'];
				$this->travelID			= $_data['travelId'];
				$this->locationID		= $_data['locationId'];
				$this->travelerID		= $_data['travelerId'];
				$this->travellinkID		= $_data['travelLinkId'];
				$this->tripID	    	= $_data['tripId'];
				
				if ($this->cache != null) {
			 			$this->cache->set('Trip_' . $this->tripID, $this, array('EXPIRE'=>86400));
			 	}
				
			}		 				 			 			
 		}
 		
 		
 		// setters 		
	 	function setTripID($_tripID){
 			$this->tripID = $_tripID;
 		}
 		
 		function setArrival($_arrival){
 			$this->arrival = $_arrival;
 		}
 		
 		function setLocationID($_locationID){
 			$this->locationID = $_locationID;
 		}
 		
 		function setNewLocationID($_locID = 0){
 			$this-> newLocationID = $_locID;
 		}

 		//getters 		
 		function getTripID(){
 			return $this->tripID;
 		}
 		
 		function getArrival(){
 			return $this->arrival;
 		}
 		
 		function getLocationID(){
 			return $this->locationID;
 		}
 		
 		function getNewLocationID(){
 			return $this-> newLocationID;
 		}

 		function getTravellinkID(){
 			return $this->travellinkID;
 		}

 		// setter of parent ID's
	 	function setTravelerID ($_travelerID){
	 		$this->travelerID = $_travelerID;	
	 	}
	 	function setTravelID($_travelID){
	 		$this->travelID = $_travelID;
	 	}	 		
	 		
	 	// getter of parent ID's	 	
	 	function getTravelerID(){
 			return $this->travelerID;
 		}
 		
 		function getTravelID(){
 			return $this->travelID;
 		}	 		
	 		
 		// getter of parent classes	 	
	 	function getTraveler(){			
			if ($this->traveler == NULL){
	 			try {
	 				$this->traveler = new Traveler($this->travelerID);
	 			}
				catch (Exception $e) {
				   throw $e;
				}	 
	 		}			
 			return $this->traveler; 			
		}

 		// get the owner of this trip
 		function getOwner(){	 		
	 		if ($this->owner == NULL){
	 			$this->mRs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$sql = "SELECT refID, reftype FROM tblTravelLink WHERE travellinkID = " . $this->travellinkID;
 				$this->mRs->Execute($sql);

	 			try {
	 				if (1 == $this->mRs->Result(0,"reftype"))
	 					$this->owner = new Traveler($this->mRs->Result(0,"refID"));
	 				else if (2 == $this->mRs->Result(0,"reftype")){
						$mGroup = GroupFactory::instance()->create( array($this->mRs->Result(0,"refID")) );
						$this->owner = $mGroup[0];
	 				}
	 			}
				catch (Exception $e) {
				   throw $e;
				}	 
	 		}			
 			return $this->owner; 			
 		}

 		function getTravel(){
 			if ($this->travel == NULL){
	 			try {
	 				$this->travel = new Travel ($this->travelID);
	 			}
				catch (Exception $e) {
				   throw $e;
				}
	 		}
 			return $this->travel; 			
 		}
	
 		// CRUD methods 		
	 	function Create(){
 			$this->mRs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 		$sql = "INSERT into tblTrips (travelID, arrival, locID) " .
					"VALUES ('$this->travelID', '$this->arrival', '$this->locationID')";
 			$this->mRs->Execute($sql);	 	 			
 			$this->tripID = $this->mRs->GetCurrentID();	
 			$this->invalidateCacheEntry(); 		
 		} 		
 		function Update(){
 			$this->mRs =	new Recordset(ConnectionProvider::instance()->getConnection());
 			$sql = "UPDATE tblTrips " .
					"SET arrival = '$this->arrival', locID = '$this->locationID' " .
					"WHERE tripID = '$this->tripID'" ;
 			$this->mRs->Execute($sql); 

 			$this->invalidateCacheEntry();
 		}
 		/**
		* edits by ianne
		*	commented out delete queries.
		**/
 		function Delete(){ 			
 			$mytravellogs =  $this->getTravelLogs();

			for($idx_tl=0;$idx_tl<count($mytravellogs);$idx_tl++){ 								
				//$mytravellogs[$idx_tl]->Delete();	 	
				$mytravellogs[$idx_tl]->Archive();
			}	

 			$this->invalidateCacheEntry();
			//$sql = "DELETE FROM tblTrips WHERE tripID = '$this->tripID' ";		
 			//$this->mRs->Execute($sql); 				 				
 		}	 	
	 	
	 	// other methods
	 	
	 	// get location of the trip
 		function getLocation(){ 				 			
 			if ($this->location == NULL){
 				$factory = LocationFactory::instance();
 				try {
 					$this->location = $factory->create($this->locationID);
 				}
				catch (Exception $e) {
				   throw $e;
				}				
 			}

 			return $this->location; 			
 		} 		
 		
 		// get all travel logs of the specified trip
 		function getTravelLogs($criteria = null){ 			
 			$this->cache 	=	ganetCacheProvider::instance()->getCache();
 			if (null == $criteria){
 				$criteria = new Criteria2();
 				$criteria->setOrderBy("travellogID");
 				$hashKey = '';
 			} else {
 				$hashKey	=	$criteria->getHash();
 			}
			
 			$found	=	false;
 			if ($this->cache != null){
 				$jEntryIDs	=	$this->cache->get('Trip_TravelLog_Relation_' . $this->tripID . '_' . $hashKey);
 				if ($jEntryIDs){
 					//echo 'Trip_TravelLog_Relation_' . $this->tripID . '_' . $hashKey . ' >count< ' . count($jEntryIDs) . '<br/>';
 					// used loop to get travellogs from cache instead of multiget due to structure of the returned array returned by multiget
 					foreach($jEntryIDs as $jEntryID){
						$travellog = $this->cache->get($jEntryID);
						if($travellog != false && $travellog->getUrlAlias()){
							$this->arrtravellog[] = $travellog;
						}
 					}
 					
 					if (count($this->arrtravellog) == count($jEntryIDs)){
 						$found = true;
 					}	
 					
 					
 				}
 			}
 			if (!$found){
	 			if (count($this->arrtravellog) == 0){
	 				$sql = "SELECT travellogID from tblTravelLog WHERE tblTravelLog.deleted = 0 AND tripID = '$this->tripID' " . $criteria->getCriteria2('AND');
	 				$this->mRs =	new Recordset(ConnectionProvider::instance()->getConnection());
	 				$this->mRs->Execute($sql);
		 			$jEntryIDs	=	array();
	 				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
	 					try {
	 						$travellog = new TravelLog($recordset['travellogID']);
	 						$jEntryIDs[]	=	'TravelLog_' . $recordset['travellogID'];
	 					}
						catch (Exception $e) {
						   throw $e;
						}
	 					$this->arrtravellog[] = $travellog;
					}
					
					if ($this->cache != null){
						$key = 'Trip_TravelLog_Relation_' . $this->tripID . '_' . $hashKey;
						$keyForHashes	=	'Trip_TravelLog_hashes_' . $this->tripID;
						$this->cache->set($key,$jEntryIDs,array('EXPIRE' => 86400));
					
						// store hashes key for relation to be used for invalidation	
						if ($hashes = $this->cache->get($keyForHashes )){
							$hashes[]	=	$key;
							$this->cache->set($keyForHashes,$hashes,array('EXPIRE' => 86400));
						} else {
							$this->cache->set($keyForHashes,array($key),array('EXPIRE' => 86400));
						}
					}
	 			} 		
 			}	
 			return $this->arrtravellog;
 		}

	 	// Added By: Cheryl Ivy Q. Go  20 June 2007
 		function transferLocation(){
 			$sql = "UPDATE tblTrips " .
					"SET locID = " . $this->newLocationID . " " .
					"WHERE locID = " . $this-> locationID;
 			$this->mRs =	new Recordset(ConnectionProvider::instance()->getConnection());
 			$this->mRs-> Execute($sql);
 		}

 		// check if trip with same travelID and location and arrival date exists
 		public static function TripExists($_travelID, $_locationID, $_arrival){	
 			try {
				$mRs   = new Recordset(ConnectionProvider::instance()->getConnection());
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$tripID = 0;			
 			$sql = "SELECT tblTrips.tripID from tblTrips, tblTravelLog WHERE tblTrips.tripID = tblTravelLog.tripID AND tblTravelLog.deleted = 0 AND  locID = '$_locationID' AND tblTrips.travelID = '$_travelID' AND arrival =  '$_arrival'  LIMIT 1";
	 		$mRs->Execute($sql);
	 		
	 		if ($mRs->Recordcount())
	 			$tripID = $mRs->Result(0,"tripID");
	 		
	 		return $tripID;
 		}
 		
 		// invalidate entry in cache
		private function invalidateCacheEntry(){
			$this->cache 	=	ganetCacheProvider::instance()->getCache();
			if ($this->cache != null) {
				$this->cache->delete('Trip_' . $this->tripID);
				
				// invalidate cached relations with parent Travel class
				if ($relKeys	=	$this->cache->get('Travel_Trip_hashes_' . $this->travelID)){
					foreach($relKeys as $relKey){
						$this->cache->delete($relKey);
					}	
				}
				// and TravelCB class, if any
				if ($relKeys	=	$this->cache->get('TravelCB_Trip_hashes_' . $this->travelID)){
					foreach($relKeys as $relKey){
						$this->cache->delete($relKey);
					}	
				}
				
				
				
				// add more code to invalidate relations which are cached
			}
		}
 		
 	}
?>