<?php
	/**
	 * @(#) Class.TravelerPoster.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 17, 2009
	 */
	
	require_once('travellog/model/Class.PosterPeer.php');
	require_once('travellog/model/Class.Traveler.php');
	require_once('travellog/model/Class.Poster.php');
		
	/**
	 * Model class for traveler poster.
	 */
	class TravelerPoster extends Traveler implements Poster {
	 
		/**
		 * Returns the poster id of the poster.
		 * 
		 * @return int The poster id of the poster.
		 */
		function getPosterId(){
			return $this->getTravelerID();
		}	
	 
		/**
		 * Returns the poster type of the poster.
		 * 
		 * @return int The poster type of the poster. Returns PosterPeer::TRAVELER.
		 */
		function getPosterType(){
			return PosterPeer::TRAVELER;
		}

	}