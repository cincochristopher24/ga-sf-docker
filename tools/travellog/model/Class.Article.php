<?php
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");
	require_once("travellog/model/Class.Tags.php");  
	require_once("travellog/model/Class.Photo.php");  
	require_once("travellog/model/Class.Traveler.php");
	require_once("travellog/model/Class.GroupFactory.php");
	require_once("travellog/model/Class.CommentContext.php");
	require_once('Cache/ganetCacheProvider.php');
	
	class Article implements CommentContext {
		
		private $mConn = null,
				$mDB   = null,
				$mRs   = null,
				
				//vars for tbl fields
				$mArticleID 	= null,
				$mAuthorID  	= null,
				$mGroupID		= null,
				$mTitle     	= null,
				$mDescription 	= null,
				$mCallout		= null,
				$mLocID			= null,
				$mEditorsNote	= null,
				$mLogDate		= null,
				$mLastEdited	= null,
				$mIsfeatured	= null,
				$mPublish		= null,
				$mDeleted		= null,
				$mArticleDate	= null,
				
				$mCommentCount  = null,
				$mPhotoCount	= null;
		
		function Article($articleID = 0, $data = null){
			try {
	 			$this->mConn = new Connection();
				$this->mRs   = new Recordset($this->mConn);
			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$this->mArticleID = $articleID;
			
			if($this->mArticleID > 0){
				$sql = "SELECT * FROM tblArticle WHERE articleID = $this->mArticleID AND deleted = 0";
				$this->mRs->Execute($sql);
				
				if ($this->mRs->Recordcount() == 0){
 					throw new Exception("Invalid Article ID: " . $articleID);
 				}
				
				$this->mAuthorID    = $this->mRs->Result(0,"authorID");
				$this->mGroupID  	= $this->mRs->Result(0,"groupID");
				$this->mTitle		= stripslashes($this->mRs->Result(0,"title"));
				$this->mDescription	= stripslashes($this->mRs->Result(0,"description"));
				$this->mCallout		= stripslashes($this->mRs->Result(0,"callout"));
				$this->mLocID		= $this->mRs->Result(0,"locID");
				$this->mEditorsNote	= stripslashes($this->mRs->Result(0,"editorsNote"));
				$this->mArticleDate	= $this->mRs->Result(0,"articledate");
				$this->mLogDate		= $this->mRs->Result(0,"logdate");
				$this->mLastEdited	= $this->mRs->Result(0,"lastedited");
				$this->mIsfeatured	= $this->mRs->Result(0,"isfeatured");
				$this->mPublish		= $this->mRs->Result(0,"publish");
				$this->mDeleted		= $this->mRs->Result(0,"deleted");
			}else if(is_array($data)){
				$this->mArticleID	= $data['articleID'];
				$this->mGroupID		= $data['groupID'];
				$this->mAuthorID    = $data['authorID'];
				$this->mTitle		= stripslashes($data['title']);
				$this->mDescription	= stripslashes($data['description']);
				$this->mCallout		= stripslashes($data['callout']);
				$this->mLocID		= $data['locID'];
				$this->mEditorsNote	= stripslashes($data['editorsNote']);
				$this->mArticleDate	= $data['articledate'];
				$this->mLogDate		= $data['logdate'];
				$this->mLastEdited	= $data['lastedited'];
				$this->mIsfeatured	= $data['isfeatured'];
				$this->mPublish		= $data['publish'];
				$this->mDeleted		= $data['deleted'];
			}
		}
		
		function setArticleID($articleID){
			$this->mArticleID = $articleID;
		}
		
		function setAuthorID($authorID){
			$this->mAuthorID = $authorID;
		}
		
		function setGroupID($groupID){
			$this->mGroupID = $groupID;
		}
		
		function setTitle($title){
			$this->mTitle = $title;
		}
		
		function setDescription($description){
			$this->mDescription = $description;
		}
		
		function setCallout($callout){
			$this->mCallout = $callout;
		}
		
		function setLocID($locID){
			$this->mLocID = $locID;
		}		
		
		function setEditorsNote($editorsnote){
			$this->mEditorsNote = $editorsnote;
		}
		
		function setArticleDate($date){
			$this->mArticleDate = $date;
		}
		
		function setLogDate($logdate){
			$this->mLogDate = $logdate;
		}
		
		function setLastEdited($lastedited){
			$this->mLastEdited = $lastedited;
		}
		
		function setIsFeatured($isfeatured){
			$this->mIsfeatured = $isfeatured;
		}
		
		function setPublish($publish){
			$this->mPublish = $publish;
		}
		
		function setDeleted($deleted){
			$this->mDeleted = $deleted;
		}
		
		function getArticleID(){
			return $this->mArticleID;
		}
		
		function getAuthorID(){
			return $this->mAuthorID;
		}
		
		function getGroupID(){
			return $this->mGroupID;
		}
		
		function getTitle(){
			return $this->mTitle;
		}
		
		function getDescription(){
			return $this->mDescription;
		}
		
		function getCallout(){
			return $this->mCallout;
		}
		
		function getLocID(){
			return $this->mLocID;
		}
		
		function getEditorsNote(){
			return $this->mEditorsNote;
		}
		
		function getArticleDate(){
			return $this->mArticleDate;
		}
		
		function getLogDate(){
			return $this->mLogDate;
		}
		
		function getLastEdited(){
			return $this->mLastEdited;
		}
		
		function getIsFeatured(){
			return $this->mIsfeatured;
		}
		
		function getPublish(){
			return $this->mPublish;
		}
		
		function getDeleted(){
			return $this->mDeleted;
		}
		
		function Create(){
			$title 	= mysql_real_escape_string($this->mTitle);
			$desc 	= mysql_real_escape_string($this->mDescription);
			$callout = mysql_real_escape_string($this->mCallout);
			$editorsnote = mysql_real_escape_string($this->mEditorsNote);
			$locID = mysql_real_escape_string($this->mLocID);
			//$articledate = mysql_real_escape_string($this->mArticleDate);
			$now   = date('Y-m-d H:i:s');
			
 			$sql = "INSERT into tblArticle (authorID, title, description, callout, editorsNote, locID, groupID, logdate) " .
 					"VALUES ('$this->mAuthorID', '$title', '$desc', '$callout', '$editorsnote' ,'$locID' ,'$this->mGroupID', '$now')";
 			$this->mRs->Execute($sql);
 			$this->mArticleID = $this->mRs->GetCurrentID();	
			
			$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true; 
			
			$this->invalidatecacheEntry();
 		}

		function Delete(){
			$sql = "UPDATE tblArticle SET deleted = 1 WHERE articleID = $this->mArticleID";
			
			// TODO: delete links to photos, delete tags
			$this->mRs->Execute($sql);
			
			$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;
			
			$this->invalidatecacheEntry();
		}
		
		function Update(){
			$title = mysql_real_escape_string($this->mTitle);
			$desc  = mysql_real_escape_string($this->mDescription);
			$callout = mysql_real_escape_string($this->mCallout);
			$editorsnote = mysql_real_escape_string($this->mEditorsNote);
			$articledate = mysql_real_escape_string($this->mArticleDate);
			$locID = mysql_real_escape_string($this->mLocID);
			
			$now   = date('Y-m-d H:i:s');
			
			$sql = "UPDATE tblArticle 
					SET title = '$title', 
						description = '$desc', 
						lastedited = '$now', 
						editorsNote = '$editorsnote', 
						callout = '$callout', 
						locID = '$locID', 
						articledate = '$articledate' 
					WHERE articleID = ".$this->mArticleID;
			$this->mRs->Execute($sql);
			
			$_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] = true;
		}
		
		function pulldown($date = null){
			$last_year = date('Y-m-d h:i:s', strtotime("-1 year"));
			$date = (is_null($date)) ? $last_year : $date;
			$sql = "UPDATE tblArticle 
					SET lastedited = '$date' 
					WHERE articleID = ".$this->mArticleID;
			$this->mRs->Execute($sql);
			
			// flush cache
			require_once('Cache/ganetMemcache.php');
			$c = new ganetMemcache();
			$c->flush();
		}
		
		function getOtherArticlesByGroup($groupID){
			$sql = "SELECT * FROM tblArticle WHERE groupID = $groupID AND deleted = 0 AND articleID != $this->mArticleID GROUP BY articleID ORDER BY logdate DESC";
			$this->mRs->Execute($sql);
			
			$articles = array();
			while ($row = mysql_fetch_assoc($this->mRs->Resultset())) {
				try {
					$article = new Article(0,$row);
				}
				catch (Exception $e) {
				   echo $e->getMessage();
				}
				$articles[] = $article;
			}
			return $articles;
		}
		
		function getOtherArticlesByAuthor($authorID){
			$sql = "SELECT * FROM tblArticle WHERE authorID = $authorID AND deleted = 0 AND articleID != $this->mArticleID GROUP BY articleID ORDER BY logdate DESC";
			$this->mRs->Execute($sql);
			
			$articles = array();
			while ($row = mysql_fetch_assoc($this->mRs->Resultset())) {
				try {
					$article = new Article(0,$row);
				}
				catch (Exception $e) {
				   echo $e->getMessage();
				}
				$articles[] = $article;
			}
			return $articles;
		}
		
		function getRelatedArticles(){
			$sql = 'SELECT * 
					FROM tblArticle as a, tblTags as t, tblTraveler as tl
					WHERE a.articleID = t.contextID 
						AND t.context = 0 
						AND a.authorID = tl.travelerID
						AND tl.isSuspended = 0
						AND a.deleted = 0
						AND a.articleID != '.$this->mArticleID . ' ' .
						'AND t.tag IN ( ' . 
							'SELECT tag FROM tblTags WHERE contextID = ' . $this->mArticleID . ' ' . 
						 		'AND context = ' . Tags::ARTICLE . ' ' . 
						') ' . 
						'GROUP BY a.articleID ' . 
						'ORDER BY a.logdate DESC';
			
			$this->mRs->Execute($sql);
			
			$articles = array();
			while ($row = mysql_fetch_assoc($this->mRs->Resultset())) {
				try {	
					$article = new Article(0,$row);
				}
				catch (Exception $e) {
				   echo $e->getMessage();
				}
				$articles[] = $article;
			}
			
			return $articles;
		}
		
		function getRelatedArticlesByGroup($groupID){
			$sql = 'SELECT * 
					FROM tblArticle as a, tblTags as t 
					WHERE authorID IN (
							SELECT DISTINCT travelerID FROM tblGrouptoTraveler WHERE groupID = ' . $groupID .'	
						) 
						AND deleted = 0 
						AND articleID = t.contextID 
						AND t.context = 0 
						AND a.articleID != '.$this->mArticleID . ' ' .
						'AND t.tag IN ( ' . 
							'SELECT tag FROM tblTags WHERE contextID = ' . $this->mArticleID . ' ' . 
						 		'AND context = ' . Tags::ARTICLE . ' ' . 
						') ' . 
						'GROUP BY a.articleID ' . 
						'ORDER BY a.logdate DESC';
			
			$this->mRs->Execute($sql);
			
			$articles = array();
			while ($row = mysql_fetch_assoc($this->mRs->Resultset())) {
				try {	
					$article = new Article(0,$row);
				}
				catch (Exception $e) {
				   echo $e->getMessage();
				}
				$articles[] = $article;
			}
			
			return $articles;
		}
		
		function getArticles($context,$value){
			$context = ( strlen(trim($value)) > 0 ) ? $context : "default";
			
			switch($context){
				case "tag":
					$sql = "SELECT a.*
							FROM tblArticle as a, tblTags as t 
							WHERE a.articleID = t.contextID 
								AND a.deleted = 0 
								AND t.context = 0 
								AND t.tag = '".mysql_real_escape_string($value)."'".
								" GROUP BY a.articleID ORDER BY CASE WHEN lastedited = '0000-00-00 00:00:00' " . 
									"THEN logdate " . 
									"ELSE lastedited " . 
								"END DESC";
				break;
				case "author":
					$sql = "SELECT * 
							FROM tblArticle 
							WHERE authorID = $value 
								AND deleted = 0
								AND groupID = 0
								GROUP BY articleID 
								ORDER BY CASE WHEN lastedited = '0000-00-00 00:00:00' " . 
									"THEN logdate " . 
									"ELSE lastedited " . 
								"END DESC";
				break;				
				case "group":
					$sql = "SELECT * 
							FROM tblArticle 
							WHERE groupID = $value 
								AND deleted = 0 
								GROUP BY articleID 
								ORDER BY CASE WHEN lastedited = '0000-00-00 00:00:00' " . 
									"THEN logdate " . 
									"ELSE lastedited " . 
								"END DESC";
				break;
				default:
					$sql = "SELECT * 
							FROM tblArticle 
							WHERE deleted = 0 
								GROUP BY articleID 
								ORDER BY CASE WHEN lastedited = '0000-00-00 00:00:00' " . 
									"THEN logdate " . 
									"ELSE lastedited " . 
								"END DESC";
								
			}
			$this->mRs->Execute($sql);

			$articles = array();
			while ($row = mysql_fetch_assoc($this->mRs->Resultset())) {
				try {	
					$article = new Article(0,$row);
				}
				catch (Exception $e) {
				   echo $e->getMessage();
				}
				$articles[] = $article;
			}

			return $articles;
		}
		
		// photos
		function addPhoto($photo){
        	$sql = "SELECT MAX(position) as mpos from tblArticletoPhoto WHERE articleID = '$this->mArticleID'";
 			$this->mRs->Execute($sql);
        	
        	
        	if($this->mRs->Recordcount() > 0){
        		$result = mysql_fetch_assoc($this->mRs->Resultset());
        		$pos = $result['mpos']+1;           			
        	}else{
       			$pos =0;
        	}            
            
            $photoID = $photo->getPhotoID();   
            $sql = "INSERT into tblArticletoPhoto (articleID, photoID, position) VALUES ('$this->mArticleID', '$photoID', '$pos' )";
	 		$this->mRs->Execute($sql);
	
			$this->invalidateCacheEntry();
		}
		
		function removePhoto($photoID){
            $this->mRs 	=	new Recordset(ConnectionProvider::instance()->getConnection());
            $sql = "DELETE FROM tblArticletoPhoto where photoID = '$photoID' ";
	 		$this->mRs->Execute($sql);  
	
			$this->invalidateCacheEntry();
		}
		
		function getRandomPhoto(){
 				
			$sql = "SELECT count(photoID) as totalrec from tblArticletoPhoto WHERE articleID = '$this->mArticleID' ";
			$this->mRs->Execute($sql);
			
			$random = null;
			if ($this->mRs->Result(0,"totalrec") == 0){
				try {
					$random = new Photo($this);
				}
				catch (Exception $e) {						  
				   throw $e;
				}
			}else {
				
				$max = $this->mRs->Result(0,"totalrec") - 1;
			
				$randidx = mt_rand(0 , $max );	 		// the index starting from top row
			
				$sql = "SELECT photoID FROM tblArticletoPhoto WHERE articleID = '$this->mArticleID' ORDER BY photoID LIMIT $randidx , 1"  ;
				$this->mRs->Execute($sql);
				
				while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
	 				try {	
	 					$random = new Photo($this, $recordset['photoID'] );
	 				}
					catch (Exception $e) {						  
					   throw $e;
					}
				}
			}
		
			return $random;
		}
		
		function getPhotos($count = FALSE){
			$sql = "SELECT atp.photoID as photoID  
					FROM tblArticletoPhoto as atp, tblArticle as a
					WHERE atp.articleID = a.articleID
						AND a.deleted = 0
						AND atp.articleID = $this->mArticleID 
					GROUP BY atp.photoID
					ORDER BY atp.position";

			$this->mRs->Execute($sql);
			
			if($count)
				return $this->mRs->Recordcount();
				
			$photos = array();
			while ($row = mysql_fetch_assoc($this->mRs->Resultset())) {
				try {	
					$photo = new Photo($this,$row['photoID']);
				}
				catch (Exception $e) {
				   throw $e;
				}
				$photos[] = $photo;
			}
			
			$this->mPhotoCount = count($photos);
			return $photos;
		}
		
		function reArrangePhoto($marrphotoID){
        	foreach($marrphotoID as $index => $photoID){
        		$sql = "UPDATE tblArticletoPhoto SET position = '$index' WHERE photoID =  '$photoID' ";
	 			$this->mRs->Execute($sql);
        	}
        }
		
		function getPrimaryPhotoID(){
			$sql = "SELECT tblPhoto.photoID from tblArticletoPhoto, tblPhoto WHERE tblArticletoPhoto.photoID = tblPhoto.photoID AND tblPhoto.primaryphoto = 1 AND tblArticletoPhoto.articleID = '$this->mArticleID' ";
			$this->mRs->Execute($sql);
		
			$_photoID = 0;	 
			if ($this->mRs->Recordcount())	// if no photo records yet, set to default value zero
				$_photoID = $this->mRs->Result(0,"photoID");
 			
 			return $_photoID;
		}
		
		function getPrimaryPhoto(){
			$sql = "SELECT tblPhoto.photoID from tblArticletoPhoto, tblPhoto WHERE tblArticletoPhoto.photoID = tblPhoto.photoID AND tblPhoto.primaryphoto = 1 AND tblArticletoPhoto.articleID = '$this->mArticleID' ";
			$this->mRs->Execute($sql);
		
			$_photoID = 0;	 
			if ($this->mRs->Recordcount())	// if no photo records yet, set to default value zero
			$_photoID = $this->mRs->Result(0,"photoID"); 			
			
			try {	
				$photo = new Photo($this, $_photoID);							
			}
			catch (Exception $e) {
			   throw $e;
			}	
 			
 			return $photo;
		}
		
		function checkHasprimaryPhoto(){
 			$sql = "SELECT primaryphoto from tblPhoto, tblArticletoPhoto, tblArticle " .
 					"WHERE tblArticle.articleID = tblArticletoPhoto.articleID " .
					"AND tblArticletoPhoto.photoID = tblPhoto.photoID " . 
					"AND deleted = 0 " . 
 					"AND primaryphoto =1 " .
 					"AND tblArticletoPhoto.articleID ='".$this->mArticleID."'" ;
			$this->mRs->Execute($sql);
			
			
			if($this->mRs->recordcount() == 0){
				return false;	
			}else{
				return true;
			}
		}
		
		function getCountPhotos(){
			if(is_null($this->mPhotoCount))
				return $this->getPhotos($count=TRUE);
			else
				return $this->mPhotoCount;
		}
		
		function getTemplateType(){
			return 0;
		}
		
		// comments
		function addComment($comment){                
	      $comment->Create();
	      $handler = new dbHandler();

	      $sql = "INSERT INTO " .
	        " tblArticletoComment(articleID, commentID) " .
	        " VALUES (" .
	        "".$handler->makeSqlSafeString($this->mArticleID).",".
	        "".$handler->makeSqlSafeString($comment->getCommentID()).
	        ")";

	      try {
	        $handler->execute($sql);
	        $comment->setCommentType(4);
	      }
	      catch(exception $ex){
	      	throw new exception($ex->getMessage());
	      }
	    }
	
		function getComments($rowslimit = NULL, $isPassport = false, $count = FALSE){
			$handler = new dbHandler();
			$arrComment = array();

			$sql = "SELECT tc.* " .
				" FROM tblArticletoComment as talc, tblComment as tc " .
				" WHERE talc.commentID = tc.commentID ".
				" AND talc.articleID = ".$handler->makeSqlSafeString($this->mArticleID).
				" AND tc.isApproved = 1 ";
			$sql .= " ORDER BY tc.commentID DESC ";
			$sql .= (is_null($rowslimit)) ? "" : SqlUtil::createLimitClause($rowslimit);

			try {
				$rs = new iRecordset($handler->execute($sql));
				
				if($count)	return $rs->retrieveRecordCount();				
				while (!$rs->EOF()) {
					$arrComment[] = new Comment($rs->current());
					try {
						$rs->next();
					}catch (exception $ex) {
						throw new exception($ex->getMessage());
					}
				}
			}catch(exception $ex){
				throw new exception($ex->getMessage());
			}
			
			$this->mCommentCount = count($arrComment);	
			return $arrComment;              
	    }
	
		function getCommentCount(){
			if(is_null($this->mCommentCount))
				return $this->getComments(NULL, false, TRUE);
			else
				return $this->mCommentCount;
		}
		
	  /**
	   * Removes a comment from the traveler's profile.
	   * 
	   * @param Comment $comment
	   * @see Comment
	   * @throws exception
	   * @return void
	   */
	  function removeComment($comment){          
	    $handler = new dbHandler();
	    $commentID = $comment->getCommentID();
	            
	    $sql = "DELETE FROM tblArticletoComment " .
	      " WHERE `articleID` = ".$handler->makeSqlSafeString($this->mArticleID).
				" AND `commentID` = ".$handler->makeSqlSafeString($comment->getCommentID());    
	    
	    try {
	    	$handler->execute($sql);
	    } 
	    catch(exception $ex){
	    	throw new exception($ex->getMessage());
	    }                                       
	  }
		
		// tags
		function getArticleTags(){
			$tags = new Tags();
			$tags->setContext(Tags::ARTICLE);
			$tags->getTagsByContextID($this->mArticleID);
			return $tags->getTagString();
		}
		
		// others
		function getTraveler(){
			return new Traveler($this->mAuthorID);
		}
		
		function getOwner(){
			if($this->mGroupID == 0){
				$o_id = $this->mAuthorID;
				return new Traveler($o_id);
			}else{
				$group = GroupFactory::instance()->create(array($this->mGroupID));
				return $group[0];
			}
		}
		
		public static function getAllArticleTags($groupID=0){
			try {
	 			$mConn = new Connection();
				$mRs   = new Recordset($mConn);
			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			if($groupID == 0){
				$sql = "SELECT COUNT( tag ) as cnt , tag
						FROM tblTags, tblArticle, tblTraveler
						WHERE tblTags.context = 0
							AND tblTags.contextID = tblArticle.articleID
							AND tblArticle.authorID = tblTraveler.travelerID
							AND tblTraveler.isSuspended = 0
							AND tblArticle.deleted = 0 
						GROUP BY tag
						ORDER BY cnt DESC
						LIMIT 0,20";
			}else{
				$sql = "SELECT COUNT( tag ) as cnt , tag
						FROM tblTags, tblArticle
						WHERE tblTags.context = 0
							AND tblTags.contextID = tblArticle.articleID
							AND (
								tblArticle.groupID = $groupID 
								OR tblArticle.authorID IN (
									SELECT travelerID 
									FROM tblGrouptoTraveler 
									WHERE groupID IN (
										SELECT groupID 
										FROM tblGroup 
										WHERE parentID = $groupID 
										OR groupID = $groupID
									)
								)
							) 							
							AND tblArticle.deleted = 0
						GROUP BY tag
						ORDER BY cnt DESC
						LIMIT 0,20";
			}		
			$mRs->Execute($sql);		
			
			$tags = array();$max = 0;
			while($row = mysql_fetch_assoc($mRs->Resultset())){
				if($max < intval($row['cnt']))
					$max = intval($row['cnt']);
					
				$tagDef = array(
					'frequency' => $row['cnt'],
					'tag' => $row['tag']
				);				
				$tags[strtolower($row['tag'])] = $tagDef;
			}
			ksort($tags);
			
			$return = array("MAX" => $max, "ARRAY" => $tags);
			return $return;		
		}
		
		public static function getArticleCount($contextID,$context='GROUP'){
			try {
	 			$mConn = new Connection();
				$rs   = new Recordset($mConn);
			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			if($context == 'GROUP')
				$sql = "SELECT count(distinct(a.articleID)) as count FROM tblArticle as a, tblGrouptoTraveler as gt, tblTraveler as t " .
						" WHERE a.deleted = 0 " . 
						" AND a.authorID = t.travelerID " .
						" AND t.isSuspended = 0 " .						
						" AND t.deactivated = 0 " .
						" AND t.travelerID = gt.travelerID " .
						" AND (a.groupID = $contextID " .
							" OR ( gt.groupID = $contextID )" . 
						")";
			else
				$sql = "SELECT count(distinct(a.articleID)) as count FROM tblArticle as a, tblTraveler as t" .
						" WHERE a.deleted = 0 " . 
						" AND a.authorID = t.travelerID " . 
						" AND t.isSuspended = 0 " . 
						" AND t.deactivated = 0 " .
						" AND a.authorID = $contextID ";
			
			$rs->Execute($sql);
			
			$row = mysql_fetch_assoc($rs->Resultset());
			if(isset($_GET['debugArticleCount'])){
				echo $sql.'<br/>'.$row['count'];
			}
			return $row['count'];
		}
		
		function hasActionPrivilege($traveler_id){
			$owner = $this->getOwner();

			if ($owner instanceof Traveler) {
				return ($owner->getTravelerID() == $traveler_id);
			}
			else {
				return ($owner->getAdministratorID() == $traveler_id OR $owner->isStaff($traveler_id));
			}			
		}
		
		private function invalidateCacheEntry(){
			$cache	=	ganetCacheProvider::instance()->getCache();
			if ($cache != null) {
				try{
					if( !is_null($this->mGroupID) && 0 < $this->mGroupID ){
						$cache->delete('Photo_Collection_Group_Admin_'.$this->mGroupID);
						$cache->delete('Photo_Collection_Group_Public_'.$this->mGroupID);
					}else if( !is_null($this->mAuthorID) ){
						$cache->delete('Photo_Collection_Traveler_Owner_'.$this->mAuthorID);
						$cache->delete('Photo_Collection_Traveler_Public_'.$this->mAuthorID);
						$cache->delete('Photo_Collection_CBTraveler_Public_'.$this->mAuthorID);
					}
				}catch(exception $e){
				}
			}
		}
	}