<?php
/**
 * Created on Jul 26, 2006
 * Edited on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 */ 
 
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.PhotoType.php");
	require_once("Class.PathManager.php");
	
	class Photo{
				
		/**
         * Define variables for Class Member Attributes
         */
			private $mConn		     = NULL;
	 		private $mRs		 	 = NULL;
	 		
	 		private $mContext		 = NULL;
			
			private $mPhotoID        = NULL;
			private $mPhototypeID    = NULL;
			private $mCaption        = NULL;
			private $mFilename       = NULL;
			private $mThumbFileName  = NULL;
			private $mPrimaryPhoto   = NULL;
		
			private $mComments       = array();
			
		/**
         * Constructor Function for this class
         */
				
			function Photo($context, $photoID = 0){
				$this->mContext = $context;
				$this->mPhotoID = $photoID;
						
				try {
					$this->mConn = new Connection();
					$this->mRs   = new Recordset($this->mConn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				if (0 < $this->mPhotoID){  
					
					$sql = "SELECT * from tblPhoto where photoID = '$this->mPhotoID' ORDER BY photoID DESC";
		 			$this->mRs->Execute($sql);
		 			
	 				if (0 == $this->mRs->Recordcount()){
	 					throw new Exception("Invalid Photo ID");		// ID not valid so throw exception
	 				}
	 					
	 				// set values to its attributes	 		 			
		 			$this->mPhotoTypeID 	= $this->mRs->Result(0,"phototypeID"); 
		 			$this->mCaption 		= stripslashes($this->mRs->Result(0,"caption")); 
		 			$this->mFileName 		= stripslashes($this->mRs->Result(0,"filename")); 
		 			$this->mThumbFileName   = stripslashes($this->mRs->Result(0,"thumbfilename"));
					$this->mPrimaryPhoto 	= $this->mRs->Result(0,"primaryphoto");
	 			}
			}
			
			
		/**
         * Setter Functions
         */ 
		
			function setPhotoID($photoID){
				$this->mPhotoID = $photoID;
			}
			
			function setPhotoTypeID($photoTypeID){
				$this->mPhotoTypeID = $photoTypeID;
			}
			
			function setCaption($caption){
				$this->mCaption = $caption;
			}
			
			function setFileName($fileName){
				$this->mFileName = $fileName;			 
			}
			
			function setThumbFileName($thumbFileName){
				$this->mThumbFileName = $thumbFileName;			 
			}
			
			function setPrimaryPhoto($primaryPhoto){
				$this->mPrimaryPhoto = $primaryPhoto;			 
			}
			
		//getter
		
			function getPhotoID(){
				return $this->mPhotoID;
			}
			
			function getPhotoTypeID(){
				return $this->mPhotoTypeID;
			}
			
			function getCaption(){
				return $this->mCaption;
			}
			
			function getFileName(){
				return $this->mFileName;	
			}
			
			function getThumbFileName(){
				return $this->mThumbFileName;	
			}
			
			function getPrimaryPhoto(){
				return $this->mPrimaryPhoto;
			}
			
			
		/**
         * CRUD Methods (Create, Update , Delete)
         */  
		
			function Create(){ 		
				
				$Acaption       = addslashes($this->mCaption);
 				$Afilename      = addslashes($this->mFileName);
 				$Athumbfile = addslashes($this->mThumbFileName);
				
	 			$sql = "INSERT into tblPhoto (phototypeID, caption, filename, thumbfilename, primaryphoto) VALUES ('$this->mPhotoTypeID', '$Acaption', '$Afilename', '$Athumbfile', '$this->mPrimaryPhoto' )";
	 			$this->mRs->Execute($sql);
	 			
				$this->mPhotoID = $this->mRs->GetCurrentID();
				
				
				$this->mContext->addPhoto($this);
				 			 
	 		}
	 		
	 		
	 		function Update(){ 		
	 			
	 			// if selected photo is set to primary, unset other photos (of the same profile OR journal entry) from being primary
		 		if ($this->mPrimaryPhoto == 1){
		 			if ($this->mPhotoTypeID == PhotoType::$PROFILE){
			 			$sql = "UPDATE tblPhoto, tblTravelertoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelertoPhoto.photoID AND tblTravelertoPhoto.travelerID = '$this->mContext->getTravelerID()'" ;
		 				$this->mRs->Execute($sql);
		 			}
		 			else if ($this->mPhotoTypeID == PhotoType::$TRAVELLOG){
			 			$sql = "UPDATE tblPhoto,tblTravelLogtoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelLogtoPhoto.photoID AND tblTravelLogtoPhoto.travelLogID = '$this->mContext->getTravelLogID()'" ;
		 				$this->mRs->Execute($sql); 
		 			}
		 		}
		 		
		 		$Acaption  = addslashes($this->mCaption);
 				$Afilename = addslashes($this->mFileName);
 				
		 		$sql = "UPDATE tblPhoto SET caption = '$Acaption', filename = '$Afilename', primaryphoto = '$this->mPrimaryPhoto' WHERE photoID = '$this->mPhotoID'" ;
	 			$this->mRs->Execute($sql);				
	 		}
	 		
	 		
	 		function Delete(){
	 			
	 			$this->mContext->removePhoto($this->mPhotoID);
	 			
	 			// if deleted photo was a primary photo
 				if ($this->mPrimaryPhoto == 1){
 					if ($this->mContext->getRandomPhoto())
 						$this->mContext->getRandomPhoto()->setAsPrimaryPhoto();
 				}
	 			
	 			$sql = "DELETE FROM tblPhoto where photoID = '$this->mPhotoID' ";
	 			$this->mRs->Execute($sql);
	 				 			
	 		}
	 		 
			 
			function setAsPrimaryPhoto(){
				
				switch($this->mPhotoTypeID){
					
					case PhotoType::$PROFILE:					
						$sql = "UPDATE tblPhoto, tblTravelertoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelertoPhoto.photoID AND tblTravelertoPhoto.travelerID = '".$this->mContext->getTravelerID()."' " ;
		 				$this->mRs->Execute($sql);	
                        
                         // set the primary photo as avatar
                         $fullpath =  $this->getThumbnailPhotoLink();
                         $sql = "UPDATE phpbb_users SET user_avatar_type = 3, user_avatar = '$fullpath' WHERE user_id = '".$this->mContext->getTravelerID()."' " ;
                         $this->mRs->Execute($sql);                         
                        					
					break;
					
					case PhotoType::$TRAVELLOG:					
						$sql = "UPDATE tblPhoto,tblTravelLogtoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelLogtoPhoto.photoID AND tblTravelLogtoPhoto.travelLogID = '".$this->mContext->getTravelLogID()."' " ;
		 				$this->mRs->Execute($sql); 
					break;
					
					case PhotoType::$RESUME:					
						$sql = "UPDATE tblPhoto,tblResumetoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblResumetoPhoto.photoID AND tblResumetoPhoto.resumeID = '$this->mContext->gettravelerID()' " ;
		 				$this->mRs->Execute($sql); 
					break;				
				}				
				
				$sql = "UPDATE tblPhoto SET primaryphoto = 1 WHERE photoID = '$this->mPhotoID'" ;
	 			$this->mRs->Execute($sql);	
							
			}
	 		
	 	
	 	/**
         * Purpose: Returns the link for the photo image.
         * If photo is NOT found in the system, a defualt image will be displayed instead.
         */
	 		function getPhotoLink(){
		 		
		 		$showdefimg = true;			// set show default image to true
		 		
		 		if (0 < $this->mPhotoID){
		 			
		 			try {
		 				 $path = new PathManager($this->mContext);
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$fullpath =  $path->GetPathRelative() . $this->mFileName;
		 			
		 			if (TRUE == file_exists($fullpath)){			// if image to be displayed exists in URL. then proceed to display 
		 				$showdefimg = false;
		 				return $fullpath;
		 			}			 			
		 		}
		 		
		 		if (TRUE == $showdefimg){							// proceed to show default image
		 			switch (get_class($this->mContext)){
		 				case "TravelerProfile":
		 					$defaultfile = "profilepic.gif";
		 					break;
		 				case "Resume":
		 					$defaultfile = "profilepic.gif";
		 					break;
		 				case "TravelLog":
		 					$defaultfile = "thumb.gif";
		 					break;
		 			}
		 			return "http://".$_SERVER['SERVER_NAME']."/images/". $defaultfile;
		 		}
		 			 		
	 		}	
	 		
	 	/**
         * Purpose: Returns the link for the photo thumbnail image.
         * If photo is NOT found in the system, a defualt image will be displayed instead.
         */
	 		function getThumbnailPhotoLink(){
		 		
		 		$showdefimg = true;			// set show default image to true
		 		
		 		if (0 < $this->mPhotoID){
		 			try {
		 				 $path = new PathManager($this->mContext);
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$fullpath =  $path->GetPathRelative() . $this->mThumbFileName;
		 			
		 			if (TRUE == file_exists($fullpath)){				// if image to be displayed exists in URL. then proceed to display 
		 				$showdefimg = false;
		 				return $fullpath;
		 			}		 			
		 		}
		 		
		 	
		 		if (TRUE == $showdefimg){								// proceed to show default image
		 			switch (get_class($this->mContext)){
		 				case "TravelerProfile":
		 					$defaultfile = "profilepic_thumb.gif";
		 					break;
		 				case "Resume":
		 					$defaultfile = "profilepic_thumb.gif";
		 					break;
		 				case "TravelLog":
		 					$defaultfile = "thumb.gif";
		 					break;
		 			}
		 			
		 			return "http://".$_SERVER['SERVER_NAME']."/images/" . $defaultfile;
		 		}
	 		}
	 		
	 		
	 	/**
         * Purpose: Add comments for this photo.
         */
            function addComment($comment){
                
                $commentID = $comment->getCommentID();
                
                $sql = "INSERT into tblCommenttoPhoto (commentID, photoID) VALUES ('$commentID', '$this->mPhotoID')";
                $this->mRs->Execute($sql);
                
            }
            
            
        /**
         * Purpose: Remove comments for this photo.
         */
            function removeComment($comment){
                
                $commentID = $comment->getCommentID();
                
                $sql = "DELETE FROM tblCommenttoPhoto where commentID = '$commentID' AND photoID = '$this->mPhotoID'";    
                $this->mRs->Execute($sql);                                
                
            }
            
         /**
         * Purpose: Returns an array of comments for this photo.
         */
            function getComments($rowslimit = NULL){
                
                if (0 == count($this->mComments)){
                    
                    if (NULL != $rowslimit){
                        $offset = $rowslimit->getOffset();
                        $rows =  $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }
                    else
                        $limitstr  = '';  
                    
                    $sql = "SELECT commentID from tblCommenttoPhoto WHERE photoID = '$this->mPhotoID' ORDER BY commentID " . $limitstr;
                    $this->mRs->Execute($sql);
                    
                    if (0 == $this->mRs->Recordcount()){
                        return NULL;            
                    }
                    
                    else {
                        while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                            try {
                                $comment = new Comment($recordset['commentID']);
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                            $this->mComments[] = $comment;
                        }
                    }               
                }
                
                return $this->mComments;
                
            }
	 		
	}
?>