<?
/**
* <b>Inquiry</b> class with integrated CRUD methods.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/

require_once("Class.Connection.php");
require_once("Class.Recordset.php");

class InquiryBase
{
	
	protected $inquiryID;
	
	protected $travelerID;
	
	protected $listingID;
	
	protected $listingname;
	
	protected $clientname;
	
	protected $message;
	
	protected $datecreated;
	
	protected $totalrecords;
	
	function __construct()
	{
		return $this;
	}	
	
	function Get()
	{
		$conn = new Connection();
		$rs = new Recordset($conn);
		$query = "select  `inquiryID`,`travelerID`,`listingID`,`listingname`,`clientname`,`message`,DATE_FORMAT(datecreated,'%M %e, %Y') as datecreated from `tblInquiry` where `inquiryID`='".$this->inquiryID."' and travelerID='".$this->travelerID."' LIMIT 1";
		$rs->Execute($query);
		
		$this->inquiryID = $rs->Result(0,"inquiryID");
		$this->travelerID = $rs->Result(0, "travelerID");
		$this->listingID = $rs->Result(0, "listingID");
		$this->listingname = $rs->Result(0, "listingname");
		$this->clientname = $rs->Result(0, "clientname");
		$this->message = $rs->Result(0, "message");
		$this->datecreated = $rs->Result(0, "datecreated");
		$rs->Close();				
		return $this;
	}
	
	function GetList()
	{
		$conn = new Connection();
		$rs = new Recordset($conn);
		$query = "select `inquiryID`,`travelerID`,`listingID`,`listingname`,`clientname`,`message`,DATE_FORMAT(`datecreated`,'%M %e, %Y') as `datecreated` from `tblInquiry` where `travelerID`=".$this->travelerID;
		$results = $rs->Execute($query);
		$rs->Close();
		$inquiries = array();
		
		while($row=mysql_fetch_assoc($results))
		{
			$travelerID = $row['travelerID'];
			$listingID = $row['listingID'];
			$listingname = $row['listingname'];
			$clientname = $row['clientname'];
			$message = $row['message'];
			$inquiryID = $row['inquiryID'];
			$datecreated = $row['datecreated'];
			$inquiry = new Inquiry();
			$inquiry->setTravelerID($travelerID);
			$inquiry->setListingID($listingID);
			$inquiry->setListingName($listingname);
			$inquiry->setClientName($clientname);
			$inquiry->setMessage($message);
			$inquiry->setInquiryID($inquiryID);
			$inquiry->setDateCreated($datecreated);
			$inquiries[] = $inquiry; 
		}	
		return $inquiries;
	}
	
	function TotalRecords()
	{
		return $this->totalrecords; 
	}
	
	function Save()
	{
		$conn = new Connection();
		$rs = new Recordset($conn);
		$query = "select `inquiryID` from `tblInquiry` where `inquiryID`='".$this->inquiryID."' LIMIT 1";	
		$rs->Execute($query);
		$this->inquiryID = $rs->Result(0,"inquiryID");
		if ( $rs->Recordcount() )
		{
			$query = "update `tblInquiry` set 
					 `listingID` ='".$this->listingID."',
					 `listingname`='".$this->listingname."',
					 `clientname`='".$this->clientname."',
					 `message`='".$this->message."'  
					   where `inquiryID`='".intval($this->inquiryID)."' and travelerID='".$this->travelerID."'";
		}
		else
		{
			$query = "insert into `tblInquiry` (`travelerID`,`listingID`,`listingname`,`clientname`,`message`) values
					 ('".$this->travelerID."','".$this->listingID."','".$this->listingname."','".$this->clientname."','".$this->message."')"; 
		}		
		$rs->Execute($query);
		if ( $this->inquiryID == '')
		{
			$this->inquiryID = $rs->GetCurrentId();
		}
		$rs->Close();		
		return $this->inquiryID;
	}		
	
	function SaveNew()
	{
		$this->inquiryID = "";
		return $this->Save();		
	}		
	
	function Delete()
	{
		$conn = new Connection();
		$rs = new Recordset($conn);
		$query = "delete from `tblInquiry` where `inquiryID`='".intval($this->inquiryID)."' and travelerID='".$this->travelerID."'";
		$rs->Execute($query);
		$rs->Close();
	}	

}

class Inquiry extends InquiryBase 
{
		function __construct() {parent::__construct();}
		
		function Delete() {return parent::Delete();}	
	
		function SaveNew() {return parent::SaveNew();}
		
		function Save() {return parent::Save();}
		
		function GetList() {return parent::GetList();}
		
		function Get() {return parent::Get();}
			
		function setTravelerID($_travelerID)
		{
			$this->travelerID = $_travelerID;	
		}
		function setListingID($_listingID)
		{
			$this->listingID = $_listingID;	
		}
		function setListingname($_listingname)
		{
			$this->listingname = $_listingname;	
		}
		function setClientname($_clientname)
		{
			$this->clientname = $_clientname;	
		}
		function setMessage($_message)
		{
			$this->message = $_message;	
		}
		function setInquiryID($_inquiryID)
		{
			$this->inquiryID = $_inquiryID;	
		}	
		function setDateCreated($_datecreated)
		{
			$this->datecreated = $_datecreated;	
		}	
		
		
		function getTravelerID()
		{
			return $this->travelerID;	
		}
		function getListingID()
		{
			return $this->listingID;	
		}
		function getListingname()
		{
			return $this->listingname;	
		}
		function getClientname()
		{
			return $this->clientname;	
		}
		function getMessage()
		{
			return $this->message;	
		}
		function getInquiryID()
		{
			return $this->inquiryID;	
		}	
		function getDateCreated()
		{
			return $this->datecreated;	
		}	
}
?>