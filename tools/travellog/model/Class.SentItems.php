<?php
	/**
	 * Created on Jul 26, 2006
	 * Class.SentItems.php 
	 * @author Cheryl Ivy Q. Go
	 * Last Edited on 22 Jan 2008 -- refactored
	 * Last Edited on 13 May 2008
	 */
 
	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.MessageSendableFactory.php';

	class SentItems{

		private $mRs	= NULL;
		private $mRs2	= NULL;
		private $mConn	= NULL;

		private $mMessageId		= 0;
		private $mSendableId	= 0;
		private $mTotalRecords	= 0;

		const TRAVELER	= 1;
		const GROUP		= 2;

		/**
		 * Constructor of Class SentItems
		 * @param integer $_sendableID
		 * @return SentItems
		 */
			
		function SentItems($_sendableID = 0){
 			try{ 				
 				$this->mConn = new Connection();
				$this->mRs	= new Recordset($this->mConn);
				$this->mRs2	= new Recordset($this->mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			$this->mSendableId = $_sendableID;
			return $this;
		}

		/**
		 * Function name: Store
		 * @param integer $_messageID
		 * Inserts message to table tblMessages
		 */
				
		function Store($_messageID = 0, $_destination = array()){
			$counter = 0;

			if (0 < sizeof($_destination)){

				foreach ($_destination as $dest){				
					$sendableID = $dest->getSendableID();
					
					// Insert values to database table (if message was sent to group, recipient is group else, traveler)					
					$sql = "INSERT INTO tblMessageToAttribute (messageID, recipientID, messageType) " .
								"VALUES (" . $_messageID . ", " . $sendableID . ", 2)";
					$this->mRs-> Execute($sql);
				}
			}
			else{				
				// Insert values to database table (if message was sent to group, recipient is group else, traveler)					
				$sql = "INSERT INTO tblMessageToAttribute (messageID, messageType) " .
							"VALUES (" . $_messageID . ", 2)";
				$this->mRs->Execute($sql);
			}			
		}

		/**
		 * Function name: Delete
		 * @param integer $_attributeID
		 * Sets trashed of message with attributeID = $_attributeID to 1
		 */

		function Delete($_attributeID = 0){
			// get messageID
			$sql = "SELECT messageID FROM tblMessageToAttribute WHERE attributeID = " . $_attributeID;
			$this->mRs->Execute($sql);

			$messageID = $this->mRs->Result(0, "messageID");

			// Update row in table tblMessages			
			$sql2 = "UPDATE tblMessageToAttribute " .
					"SET " .
						"trashed = 1, isRead = 0 " .
					"WHERE messageType = 2 " .
					"AND messageID = " . $messageID;
			$this->mRs2->Execute($sql2);
		}

		/***************************** START: SETTERS *************************************/
		function setInstance($_instance = 0){
			$this->mInstance = $_instance;
		}
		function setTotalRecords($_totalRecords = 0){
			$this->mTotalRecords = $_totalRecords;
		}
		function setMessageId($_messageID = 0){
			$this->mMessageId = $_messageID;
		}
		function setSendableId($_sendableID = 0){
			$this->mSendableId = $_sendableID;
		}
		/******************************* END: SETTERS *************************************/

		/***************************** START: GETTERS *************************************/
		function getTotalRecords(){
			return $this->mTotalRecords;
		}
		function getMessageList($_rowsLimit = NULL){

			$arrMessages = array();
			
			if (self::TRAVELER == $this->mInstance)
				$arrMessages = $this->getTravelerSentBox($_rowsLimit);
			else
				$arrMessages = $this->getGroupSentBox($_rowsLimit);

			return $arrMessages;
		}
		function getTravelerSentBox($_rowsLimit = NULL){
			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);			
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($this->mSendableId);
				$travelerId = $Sendable->getTravelerID();
			}
			catch (exception $e){
				throw $e;
			}

			// Get SENT messages
			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ";
				$sql .= "(" .
							"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute " .
							"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.messageType = 2 " .
							"AND tblMessages.senderID = " . $this->mSendableId;

				// According to requirements, only TRAVELER has a SENT FOLDER -- NAGCHANGE NA LIWAT!

				// if user is administrator of any group, include messages
				if( $Sendable->IsAdministrator() )
					$sql .= " UNION " .
							"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute, tblGroup " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblGroup.sendableID = tblMessages.senderID " .
							"AND tblGroup.isSuspended = 0 " .
							"AND tblMessages.discriminator = 3 " .							
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.messageType = 2 " .
							"AND tblGroup.administrator = " . $travelerId;
				// if user is staff of any group, include messages
				if ( $Sendable->isStaff() )
					$sql .= " UNION " .
							"SELECT tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, tblMessages.senderID, " .
							"tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
							"FROM tblMessages, tblMessageToAttribute, tblGrouptoFacilitator " .
							"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
							"AND tblGrouptoFacilitator.sendableID = tblMessages.senderID " .
							"AND tblMessages.discriminator = 3 " .
							"AND tblMessageToAttribute.trashed = 0 " .
							"AND tblMessageToAttribute.messageType = 2 " .
							"AND tblGrouptoFacilitator.type = 1 " .
							"AND tblGrouptoFacilitator.status = 1 " .
							"AND tblGrouptoFacilitator.travelerID = " . $travelerId;
			$sql .= ") as allInbox GROUP BY messageID ORDER BY datecreated DESC  ". $queryLimit;
			$this->mRs->Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$this->mRs2->Execute($sql2);


			$this->setTotalRecords ($this->mRs2->Result(0, "totalRecords"));

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_array($this->mRs->Resultset())){
				$message = new PersonalMessage();
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setMessageTypeId($rMessage['messageType']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));
				
				$message->setIsRead($rMessage['isRead']);

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setMessageRecipient();

				$arrMessage[] = $message;
			}

			return $arrMessage;
		}
		function getGroupSentBox($_rowsLimit = NULL){
			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			else{				
				$row = $_rowsLimit->getRows();
				$offset = ($_rowsLimit->getOffset() * $row);			
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			// Get SENT messages
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblMessages.title, tblMessages.message, tblMessages.dateCreated, tblMessages.messageID, " .
			 		"tblMessages.senderID, tblMessageToAttribute.isRead, tblMessageToAttribute.attributeID, tblMessageToAttribute.messageType " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessageToAttribute.messageID = tblMessages.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.trashed = 0 " .
					"AND tblMessageToAttribute.messageType = 2 " .
					"AND tblMessages.senderID = " . $this->mSendableId . " " .
					"GROUP BY messageID " .
					"ORDER BY datecreated DESC  " . $queryLimit;
			$this->mRs->Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$this->mRs2->Execute($sql2);

			$arrMessage = array();
			$this->setTotalRecords($this->mRs2->Result(0, "totalRecords"));

			while($rMessage = mysql_fetch_array($this->mRs->Resultset())){
				$message = new PersonalMessage();
				$message->setSendableId($this->mSendableId);
				$message->setCreated($rMessage['dateCreated']);
				$message->setMessageId($rMessage['messageID']);
				$message->setAttributeId($rMessage['attributeID']);
				$message->setMessageTypeId($rMessage['messageType']);
				$message->setText(stripslashes($rMessage['message']));
				$message->setTitle(stripslashes($rMessage['title']));

				$message->setIsRead($rMessage['isRead']);

				$factory = MessageSendableFactory::Instance();
				$_source = $factory->Create($rMessage['senderID']);

				$message->setSource($_source);
				$message->setMessageRecipient();

				$arrMessage[] = $message;
			}

			return $arrMessage;
		}
		/******************************* END: GETTERS *************************************/

		static function isOwner($_attributeID = 0, $_sendableID = 0){
			try{
 				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
			}			
			catch (exception $e){				   
			   throw $e;
			}

			try{
				$mFactory = MessageSendableFactory::Instance();
				$Sendable = $mFactory->Create($_sendableID);
				$travelerId = $Sendable->getTravelerID();
			}
			catch (exception $e){
				throw $e;
			}

			$sql = "SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessages.senderID = " . $_sendableID . " " .
					"AND tblMessageToAttribute.messageType = 2 " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeID;
			// if user is administrator of any group, include messages
			$sql .= " UNION " .
					"SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.messageType = 2 " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
					"AND EXISTS " .
					"(" .
						"SELECT sendableID " .
						"FROM tblGroup " .
						"WHERE sendableID = tblMessages.senderID " .
						"AND isSuspended = 0 " .
						"AND administrator = " . $travelerId .
					")";
			// if user is staff of any group, include messages
			$sql .= " UNION " .
					"SELECT tblMessageToAttribute.attributeID " .
					"FROM tblMessages, tblMessageToAttribute " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessages.discriminator = 3 " .
					"AND tblMessageToAttribute.messageType = 2 " .
					"AND tblMessageToAttribute.attributeID = " . $_attributeID . " " .
					"AND EXISTS " .
					"(" .
						"SELECT sendableID " .
						"FROM tblGrouptoFacilitator " .
						"WHERE sendableID = tblMessages.senderID " .
						"AND type = 1 " .
						"AND status = 1 " .
						"AND travelerID = " . $travelerId .
					")";
			$mRs-> Execute($sql);

			if (0 < $mRs->Recordcount())
				return true;
			else
				return false;
		}
	}
?>