<?php
/*
 * Created on 11 17, 08
 * 
 * Author:
 * Purpose: 
 * 
 */
 
 require_once('travellog/model/Class.Traveler.php');
 require_once("Class.Connection.php");
 require_once("Class.Recordset.php");
 
 class Search {
 	private $conn		 = NULL;
 	
 	function Search(){
 		$this->conn = new Connection();
 	}
 	
 	/**
 	 * @param numeric withinGroup (groupID of the group to be searched, 0 means all users of GA.net)
 	 * @return array travelers
 	 */
 	function searchTraveler($keyword,$withinGroup = 0 ){
 		$results = array();
 		
 		if ($keyword == ''){
 			return $results;
 		}
 		
 		$kwAr = explode(' ',$keyword);
 		
 		if (count($kwAr) == 1){
 			/**
 			 *  search using 1 keyword only
 			 */
 			
 			// 1. search using LIKE in username and SOUNDS LIKE in firstname and lastname
 			if ($withinGroup == 0){
 				$query = "select * from tblTraveler as t where username like '%$keyword%' " .
 						 "union " .
 						 "select * from tblTraveler as t where firstname sounds like '$keyword' " .
 						 "union " .
 						 "select * from tblTraveler as t where lastname sounds  like '$keyword' ";
 			} else {
 				$query = "select t.* from tblTraveler as t,tblGroup as g,tblGrouptoTraveler as gt " .
 						      "where username like '%$keyword%' and t.travelerID = gt.travelerID " .
 						      "and gt.groupID = g.groupID and g.groupID='$withinGroup' "  .
 						 "union " .
 						 "select t.* from tblTraveler as t,tblGroup as g,tblGrouptoTraveler as gt " .
 						      "where username sounds like '$keyword' and t.travelerID = gt.travelerID " .
 						      "and gt.groupID = g.groupID and g.groupID='$withinGroup' "  .
 						 "union " .
 						 "select t.* from tblTraveler as t,tblGroup as g,tblGrouptoTraveler as gt " .
 						      "where firstname sounds like '$keyword' and t.travelerID = gt.travelerID " .
 						      "and gt.groupID = g.groupID and g.groupID='$withinGroup' " .
 						 "union " .
 						 "select t.* from tblTraveler as t,tblGroup as g,tblGrouptoTraveler as gt " .
 						      "where lastname sounds like '$keyword' and t.travelerID = gt.travelerID " .
 						      "and gt.groupID = g.groupID and g.groupID='$withinGroup' " ;
 			} 
 			$rsLike = new Recordset($this->conn);
 			$rsLike->Execute($query);
 			if ($rsLike->Recordcount() > 0 ){
 				while ($recordset = mysql_fetch_array($rsLike->Resultset())){
 					$travelerData = array();
					$travelerData["travelerID"] = $recordset['travelerID'];
					$travelerData["active"] = $recordset['active'];
					$travelerData["isSuspended"] = $recordset['isSuspended'];
		 			$travelerData["currlocationID"] = $recordset['currlocationID'];
		 			$travelerData["sendableID"] = $recordset['sendableID'];
		 			$travelerData["username"] = $recordset['username'];
		 			$travelerData["password"] = $recordset['password'];
		 			$travelerData["showsteps"] = $recordset['showsteps'];
											
					$traveler = new Traveler;
					$traveler-> setTravelerData($travelerData);
					
					
					// set profile data in array and set to profile; minimize DB query
					$profileData = array();								
					$profileData["travelerID"] = $recordset['travelerID'];
					$profileData["username"] = $recordset['username'];
					$profileData["firstname"] = $recordset['firstname'];
		 			$profileData["lastname"] = $recordset['lastname'];
		 			$profileData["email"] = $recordset['email'];
		 			$profileData["gender"] = $recordset['gender'];
		 			$profileData["htlocationID"] = $recordset['htlocationID'];
		 			$profileData["currlocationID"] = $recordset['currlocationID'];
		 			$profileData["address1"] = $recordset['address1'];
		 			$profileData["address2"] = $recordset['address2'];
		 			$profileData["phone"] = $recordset['phone'];
		 			$profileData["travelertypeID"] = $recordset['travelertypeID'];
		 			$profileData["birthdate"] = $recordset['birthdate'];
		 			$profileData["interests"] = $recordset['interests'];
		 			$profileData["shortbio"] = $recordset['shortbio'];
		 			$profileData["iculture"] = $recordset['iculture'];
		 			$profileData["dateregistered"] = $recordset['dateregistered'];
		 			$profileData["latestlogin"] = $recordset['latestlogin'];
		 			$profileData["views"] =  $recordset['views'];
		 			$profileData["intrnltravel"] = $recordset['intrnltravel'];
		 			$profileData["istripnexttwoyears"] = $recordset['istripnexttwoyears'];
		 			$profileData["income"] =	 $recordset['income'];
		 			$profileData["otherprefertravel"] =	 $recordset['otherprefertravel'];
		 			$profileData["ispromosend"] = $recordset['ispromosend'];
		 			$profileData["signature"] = $recordset['signature'];

					$profile = new TravelerProfile;
					$profile->setProfileData($profileData);
	
					$traveler->setTravelerProfile($profile);					
					
					$results['k' . $recordset['travelerID']] = $traveler;
 				}	
 			}
 			
 		} else if(count($kwAr) > 1) {
 			// more than 1 keyword probably client is searching for firstname and lastname
 			
 			// we just get the first two keywords
 			$key1 = $kwAr[0];
 			$key2 = $kwAr[1];
 			
 			// then search those keywords individually and combine their results
 			$srch = new Search();
 			$results = $srch->searchTraveler($key1,$withinGroup);
 			$results = array_merge($results,$srch->searchTraveler($key2,$withinGroup));
 		} else {
 			
 		}		
 		return $results;
 	}
 		
 }
 
?>
