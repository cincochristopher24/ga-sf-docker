<?php
	/***
	 * Author: Reynaldo Castellano III
	 * Version : 2.0 (Version 1 was made by Cheryl Ivy Q. Go)
	 * Date : July 05, 2007
	 */
	require_once('Class.FBini.php');
	
	class Answer
	{		
		private $mAnswerToUserID 	= 0;
		private $mSlambookUserID 	= 0;
		private $mProfileQuestionID	= 0;
		
		private $mDb = null;
		private $mValues = array();
		
		function __construct($answerToUserID=null){
			$this->mDb = FBini::createDbHandler();
			if(!is_null($answerToUserID)){
				$qry = 	'SELECT `answerToUserID`, `slambookUserID`, `profileQuestionID` ' .
						'FROM tbAnswertoUser ' .
						'WHERE answerToUserID = '.ToolMan::makeSqlSafeString($answerToUserID);						
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(0 == $rs->retrieveRecordCount()){
					throw new exception('Invalid answerToUserID '.$answerToUserID.' passed to object of type Answer.');
				}
				else{
					$this->mAnswerToUserID 		= $rs->getAnswerToUserID(0);
					$this->mSlambookUserID 		= $rs->getSlambookUserID(0);
					$this->mProfileQuestionID 	= $rs->getProfileQuestionID(0);					
				}
			}		
		}
		
		/*** SETTERS ***/		
		public function setSlamBookUserID($param=0){
			$this->mSlambookUserID = $param;
		}
		
		public function setProfileQuestionID($param=0){
			$this->mProfileQuestionID = $param;
		}
		
		public function addValue($type,$val=''){
			$this->mValues[] = array('type'=>$type,'value'=>$val);
		}
		
		/*** GETTERS ***/
		public function getAnswerToUserID(){
			return $this->mAnswerToUserID;
		}
		
		public function getSlambookUserID(){
			return $this->mSlambookUserID;
		}
		
		public function getProfileQuestionID(){
			return $this->mProfileQuestionID;
		}
		
		public function getValues(){
			require_once('Class.AnswerValue.php');
			return AnswerValue::getAnswerValuesByAnswerID($this->mAnswerToUserID); 
		}
		
		/*** CRUDE ***/
		public function save(){
			if(0==$this->mAnswerToUserID){//add
				$qry = 	'INSERT INTO tbAnswertoUser (' .
							'`slambookUserID`,' .
							'`profileQuestionID`' .
						')VALUES(' .
							ToolMan::makeSqlSafeString($this->mSlambookUserID) .',' .
							ToolMan::makeSqlSafeString($this->mProfileQuestionID).
						')';
				$this->mDb->execQuery($qry);
				$this->mAnswerToUserID = $this->mDb->getLastInsertedID();
				require_once('Class.AnswerValue.php');
				foreach($this->mValues as $iVal){
					$ansVal = new AnswerValue();
					$ansVal->setAnswerToUserID($this->mAnswerToUserID);
					$ansVal->setAnswerType($iVal['type']);
					$ansVal->setValue($iVal['value']);
					$ansVal->save();
				}
			}
			else{//udpate
				$qry =	'UPDATE tbAnswertoUser SET ' .
							'`slambookUserID` = ' 	.ToolMan::makeSqlSafeString($this->mSlambookUserID) .','.
							'`profileQuestionID` ='	.ToolMan::makeSqlSafeString($this->mProfileQuestionID).' '.
						'WHERE `answerToUserID` =' 	.ToolMan::makeSqlSafeString($this->mAnswerToUserID);
				$this->mDb->execQuery($qry);						
			}
		}
		
		public function delete(){
			$qry = 	'DELETE FROM tbAnswertoUser ' .
					'WHERE `answerToUserID` = '.ToolMan::makeSqlSafeString($this->mAnswerToUserID);
			$this->mDb->execQuery($qry);
		}
		
		public function createDuplicate(){
			$newAnswer = new Answer();
			$newAnswer->setProfileQuestionID($this->getProfileQuestionID());
			$newAnswer->setSlambookUserID($this->getSlambookUserID());
			$values = $this->getValues();
			foreach($values as $iVal){
				$newAnswer->addValue($iVal->getAnswerType(),$iVal->getValue());
			}
			return $newAnswer;
		}
		
		/*** Static Functions ***/
		public static function getAnswersByUser($userID=0){
			$db = FBini::createDbHandler();
			$qry = 	'SELECT `answerToUserID` ' .
					'FROM tbAnswertoUser ' .
					'WHERE slambookUserID = '. ToolMan::makeSqlSafeString($userID) .
					' ORDER BY answertoUserID asc';
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if(0 != $rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new Answer($rs->getAnswerToUserID());
				}
			}
			return $ar;
		}
		
		public static function clearUserAnswers($userID=0){
			$db = FBini::createDbHandler();
			$qry = 	'DELETE tbAnswer, tbAnswertoUser '.
					'FROM tbAnswer, tbAnswertoUser '.
					'WHERE tbAnswer.answertoUserID = tbAnswertoUser.answertoUserID '.
					'AND tbAnswertoUser.slambookUserID = ' .ToolMan::makeSqlSafeString($userID);
			$db->execQuery($qry);			
		}
	}
?>