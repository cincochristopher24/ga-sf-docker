<?php
/**
 * Created on Mar 22, 2007
 * @author daf
 * 
 * 
 */
 
 	class OnlineSurveyAnswers {
 	
		/**
		 * Define variables for Class Member Attributes
		 */
		     
			 private $mAnswerID		 	= 0;
			 private $mOptionID		   	= 0;
			 private $mAnswerText	   	= '';
			 
			 //parent attributes
			 private $mOnlineSurveyID  	= 0;
			 private $mQuestionID	   	= 0;
			 	
			 
		/**
		 * Constructor Function for this class
		 */
		           
		    function OnlineSurveyAnswers($answerID = 0){ 
		    	
		    	$this->mAnswerID = $answerID;
		    	
		    	
		    	try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mAnswerID){
                    
                    $sql = "SELECT * FROM tblOnlineSurveyAnswers WHERE onlineSurveyAnswerID = $this->mAnswerID ";
                    $this->mRs->Execute($sql);
                	
                    if ($this->mRs->Recordcount()){
                        
                        // Sets values to its class member attributes.
	                    $this->mOptionID         = $this->mRs->Result(0,"optionID");      
	                    $this->mAnswerText       = $this->mRs->Result(0,"answertext");     
	                    
	                    $this->mOnlineSurveyID   = $this->mRs->Result(0,"onlinesurveyID");      
	                    $this->mQuestionID       = $this->mRs->Result(0,"questionID");    
                    }
                }
		    	
		    }
		    
		/**
         * Setter Functions
         */    
            function setOptionID($_optionID){
                $this->mOptionID = $_optionID;
            }
            
            function setAnswerText($_answertext){
                $this->mAnswerText = $_answertext;
            }
            
            function setOnlineSurveyID($_surveyID){
                $this->mOnlineSurveyID = $_surveyID;
            }
            
            function setQuestionID($_answertext){
                $this->mQuestionID = $_answertext;
            }
            
           
		/**
         * Getter Functions
         */    
            function getAnswerID(){
                return $this->mAnswerID ;
            }
            
            function getOptionID(){
                return $this->mOptionID ;
            }
            
            function getAnswerText(){
                return $this->mAnswerText ;
            }
            
            function getOnlineSurvey(){
            	
            }
            
            function getQuestion () {
            	
            }
    		
    		function Save(){
    			    			
    			if (0 == $this->mAnswerID) {
    				
    				$sql = "INSERT into tblOnlineSurveyAnswers (optionID, answertext, onlineSurveyID, questionID) " .
                        "VALUES ($this->mOptionID, '$this->mAnswerText', $this->mOnlineSurveyID, $this->mQuestionID ) ";
	                $this->mRs->Execute($sql);       
	                $this->mAnswerID = $this->mRs->GetCurrentID();
    				
    			}
    			
    			else {
    				
    				$sql = "UPDATE tblOnlineSurveyAnswers SET " .
    						"optionID = $this->mOptionID, answertext =  $this->mAnswerText " .
    						"onlineSurveyID = $this->mOnlineSurveyID, questionID = $this->mQuestionID " .
    						"WHERE onlineSurveyAnswerID = $this->mAnswerID" ;
	                $this->mRs->Execute($sql);       
    				
    			}
    		}
    		
    		
    		function Delete(){
               
                $sql = "DELETE FROM tblOnlineSurveyAnswers WHERE onlineSurveyAnswerID = '$this->mAnswerID' ";     
                $this->mRs->Execute($sql);
            } 
           
    }
?>
