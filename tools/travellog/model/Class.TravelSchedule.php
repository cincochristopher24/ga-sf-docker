<?php
/*
	Filename:	Class.TravelSchedule.php
	Module:		My Travel Plans (Travel Scheduler/Travel ToDo/Go Abroad)
	Author:		Jonas Tandinco
	Created:	July 2007
	Purpose:	Travel Plan data mapper
	
	EDIT HISTORY:

	Aug/09/2007 by Jonas Tandinco
		1.	added the field "details" to all the queries and set commands

	Sep/12/2007 by Jonas Tandinco
		1.	added the userIDExists() to check if a valid userID is provided. This is used when the userID is provided through
			the URL if a user is viewing other traveler's profile
		2.	added travelerIDExists() to check that a valid traveler ID was provided

	Sep/13/2007 by Jonas Tandinco
			1.	reworked getTravelItems()

	Sep/21/2007 by Jonas Tandinco
		1.	reworked getTravelItem, addTravelItem, editTravelItem, deleteTravelItem
		2.	refactored code in addTravelItem, editTravelItem, deleteTravelItem
		
	Oct/09/2007 by Jonas Tandinco
		1.	added getOrderedTravels() as part of refactoring to transfer logic from controller to model
		
	Oct/15/2007 by Jonas Tandinco
		1.	removed all data access to tbTraveltoTravelWish table
		
	Nov/13/2007 by Jonas Tandinco
		1.	added require_once to needed classess due to the disabling of the autoload feature
		
	Nov/22/2007 by Jonas Tandinco
		1.	added static variables to represent ongoing, no dates, and past travels
*/

require_once ('Class.Connection.php');
require_once ('Class.Recordset.php');
require_once 'travellog/model/Class.TravelPlan.php';

class TravelSchedule {

	static $ONGOING = 1;
	static $PAST    = 2;
	static $NODATES = 3;
	static $PRESENT = 4;
	
	public static function getPastTravels($userID, $limit = 0) {
		return self::getTravelItems($userID, TravelSchedule::$PAST, $limit);
	}
	
	public static function getTravelItems($userID, $filter = 0, $limit = 0) {
		require_once 'Class.TravelDestinationPeer.php';
		
		$travelItems = array();
		if(empty($userID)) {
			return $travelItems;
		}

		$conn = new Connection('SocialApplication');
		$rs   = new Recordset($conn);
		//$rsJournal = new Recordset($conn);

		switch ($filter) {
		case 1:		// ongoing
			$filterString  = ' AND (dateTo >= \'' . date('Y-m-d') . '\' ' .
							 'OR dateFrom >= \'' . date('Y-m-d') . '\')';
			break;
		case 2:		// past travels
			// Prev Query --> $filterString  = ' AND NOT (( (dateTo IS NOT NULL OR dateTo != "0000-00-00") && dateTo >= \'' . date('Y-m-d') . '\') ' . 'OR dateFrom >= \'' . date('Y-m-d') . '\')';
			// Edited by: Adelbert
			// Date: July 4, 2009
			// Purpose: exclude dates '0000-00-00' 
			$filterString  = " AND ((dateTo < '".date('Y-m-d')."' AND dateTo != '0000-00-00') OR (dateFrom < '".date('Y-m-d')."' AND dateFrom != '0000-00-00') AND dateTo < '".date('Y-m-d')."') ";
//			$filterString  = " AND ((dateTo < '".date('Y-m-d')."' AND dateTo != '0000-00-00') OR (dateFrom < '".date('Y-m-d')."' AND dateFrom != '0000-00-00')) ";
			break;
		case 3:		// travels with no dates
			$filterString = " AND (dateFrom IS NULL OR dateFrom = '0000-00-00') ";

			break;
		/*case 4: // present
			$filterString = ' AND dateFrom = \'' . date('Y-m-d') . '\'';
			
			break;*/
		default:
			$filterString = '';
			
			break;
		}

		$sql = "SELECT * FROM tbTravelWish " .
			   "WHERE userID = $userID";
		$sql .= $filterString;
		$sql .= ' ORDER BY dateFrom';
		if ($limit) {
			$sql .= ' LIMIT 0, ' . $limit;
		}

		$rs->Execute($sql);
		
		require_once 'Class.TravelPlan.php';
		
	    while ($row = mysql_fetch_array($rs->Resultset())){
			$travelItem = new TravelPlan();
			
			$travelWishID = $row['travelWishID'];
			
			$travelItem->setTravelWishID($travelWishID);
			
			$travelItem->setUserID($row['userID']);
			$travelItem->setTitle($row['title']);
			$travelItem->setDetails($row['details']);
			$travelItem->setDateFrom($row['dateFrom']);
			$travelItem->setDateTo($row['dateTo']);
	      
			$travelItem->setDestinations(TravelDestinationPeer::getTravelDestinations($travelItem->getTravelWishID(), $travelItem));
	
			$travelItems[] = $travelItem;
		}

		return $travelItems;
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END GET TRAVEL ITEMS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> RETRIEVE BY PK <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
	public static function retrieveByPk($travelWishID) {
		require_once 'Class.TravelDestinationPeer.php';
		
		$conn = new Connection('SocialApplication');
		$rs   = new Recordset($conn);

		$sql = "SELECT * " .
			   "FROM tbTravelWish " .
			   "WHERE travelWishID = $travelWishID " .
			   "LIMIT 0, 1";
		
		$rs->Execute($sql);

		$travel = new TravelPlan();
    
		$row = mysql_fetch_assoc($rs->Resultset());

		// save values
		$travel->setTravelWishID($row['travelWishID']);
		$travel->setUserID($row['userID']);
		$travel->setTitle($row['title']);
		$travel->setDetails($row['details']);
		$travel->setDateFrom($row['dateFrom']);
		$travel->setDateTo($row['dateTo']);
    
		$travel->setDestinations(TravelDestinationPeer::getTravelDestinations($travel->getTravelWishID(), $travel));

		return $travel;
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END RETRIEVE BY PK <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ADD TRAVEL ITEM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
	// add a new travel
	public static function doInsert($travel) { 
		require_once 'Class.TravelDestinationPeer.php';

		if (get_magic_quotes_gpc()) {
			$travel->setTitle(stripslashes($travel->getTitle()));
			$travel->setDetails(stripslashes($travel->getDetails()));
		}

		$conn = new Connection('SocialApplication');
		$rs   = new Recordset($conn);

		// insert new travel
		$sql = "INSERT INTO tbTravelWish (userID, title, details, dateFrom, dateTo) " .
			   "VALUES ({$travel->getUserID()}, '%s', '%s'";
		$sql .= $travel->getDateFrom() ? ",'{$travel->getDateFrom()}'" : ", null";
		$sql .= $travel->getDateTo()   ? ",'{$travel->getDateTo()}'"   : ", null";
		$sql .= ")";
		$sql = sprintf($sql, mysql_real_escape_string($travel->getTitle()), mysql_real_escape_string($travel->getDetails()));

		$rs->Execute($sql);
		
		// retrieve assigned ID
		// Modified by bert: 
		// Purpose: mysql_insert_id() function is not reliable. It returns 0 value in my case. 
		// prev code $travel->setTravelWishID(mysql_insert_id());
		$travel->setTravelWishID($rs->GetCurrentId());
		
		// also save travel destinations
		foreach ($travel->getDestinations() as $destination) {
			// assign ID from the last inserted id
			$destination->setTravelWishID($travel->getTravelWishID());
			
			// attach destinations
			//TravelDestinationPeer::add($destination);
			$destination->save();
		}
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END ADD TRAVEL ITEM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> EDIT TRAVEL ITEM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
	// modify a travel
	public static function doUpdate($travel) {
		require_once 'Class.TravelDestinationPeer.php';
		
		if (get_magic_quotes_gpc()) {
			$travel->setTitle(stripslashes($travel->getTitle()));
			$travel->setDetails(stripslashes($travel->getDetails()));
		}
		
		$conn = new Connection('SocialApplication');
		$rs   = new Recordset($conn);

		$sql = "UPDATE tbTravelWish " .
			   "SET userID = {$travel->getUserID()},
					title = '%s',
					details = '%s',
					dateFrom = ";

		$sql .= $travel->getDateFrom() ? "'{$travel->getDateFrom()}'" : "null";
		$sql .= ", dateTo = ";
		
		$sql .= $travel->getDateTo()   ? "'{$travel->getDateTo()}'"   : "null";
		$sql .= " WHERE travelWishID = {$travel->getTravelWishID()}";
		$sql = sprintf($sql, mysql_real_escape_string($travel->getTitle()), mysql_real_escape_string($travel->getDetails()));

		$rs->Execute($sql);
		
		// remove previous destinations
		TravelDestinationPeer::deleteDestinations($travel->getTravelWishID());
		
		// save destinations
		foreach ($travel->getDestinations() as $destination) {
			//TravelDestinationPeer::add($destination);
			$destination->save();
		}
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END EDIT TRAVEL ITEM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DELETE TRAVEL ITEM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
	// delete a travel
	public static function doDelete($travel) {
		require_once 'Class.TravelDestinationPeer.php';		
		
		$conn = new Connection('SocialApplication');
		$rs   = new Recordset($conn);

		// delete travel
		$sql = "DELETE FROM tbTravelWish " .
			   "WHERE travelWishID = {$travel->getTravelWishID()}";

		$rs->Execute($sql);
		
		// also delete destinations for this travel
		TravelDestinationPeer::deleteDestinations($travel->getTravelWishID());
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END DELETE TRAVEL ITEM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

	public static function editCountriesToTravel($arrCountries, $travelerID) {
		$conn = new Connection('Travel_Logs');
		$rs = new Recordset($conn);
		
		// delete previous countries the user wants to travel
		$sql = "DELETE FROM " .
			   "tblTravelertoCountriesInterested " .
			   "WHERE travelerID = $travelerID";
			
		$rs->Execute($sql);
		
		// add the new countries
		foreach ($arrCountries as $countryID) {
			$sql = "INSERT INTO tblTravelertoCountriesInterested (travelerID, countryID) " .
				   "VALUES ({$travelerID}, $countryID)";
			$rs->Execute($sql);
		}
	}
	
	public static function getOrderedTravels($userID) {
		// get present and future travels
		$presentFutureTravels = self::getTravelItems($userID, self::$ONGOING);
		
		// get travels with no dates
		$noDatesTravels = self::getTravelItems($userID, self::$NODATES);
		
		// combine present and future then past travels. This is the correct ordering
		$travels = array_merge($presentFutureTravels, $noDatesTravels);
		
		return $travels;
	}

	public static function getCountriesToTravel($travelerID) {
		$conn = new Connection('Travel_Logs');
		$rs = new Recordset($conn);

		$arrCountries = array('countryIDs' => array(), 'countryNames' => array());
		
		// get countries the user is interested travelling to
		$sql = "SELECT tblTravelertoCountriesInterested.countryID, country " .
			   "FROM tblTravelertoCountriesInterested, GoAbroad_Main.tbcountry " .
			   "WHERE travelerID = $travelerID " .
			   "AND tblTravelertoCountriesInterested.countryID = tbcountry.countryID " .
			   "ORDER BY country";
			
		$rs->Execute($sql);
		
		if ($rs->Recordcount()) {
			while ($row = mysql_fetch_assoc($rs->Resultset())) {
				$arrCountries['countryIDs'][] = $row['countryID'];
				$arrCountries['countryNames'][] = $row['country'];
			}
		}

		return $arrCountries;
	}

	public static function getUserIDWithTravelerID($travelerID) {
    	$conn = new Connection();
    	$rs   = new Recordset($conn);

		$rs->Execute('SELECT userID FROM SocialApplication.tbUser WHERE travelerID=' . $travelerID);

		$row = mysql_fetch_array($rs->Resultset());
		if ($row) {
		  return $row['userID'];
		} else {
		   	// check if user is an Advisor
		   	require_once 'travellog/model/Class.Traveler.php';
   	
		   	$travelerType = new Traveler($travelerID);

		   	if (!$travelerType->isAdvisor() && self::travelerIDExists($travelerID)) {
		    	$rs->Execute("INSERT INTO SocialApplication.tbUser (travelerID) VALUES ($travelerID)");
		    	// TODO: use SELECT LAST_INSERT_ID()
				$query = "SELECT LAST_INSERT_ID()";
				$result = mysql_query($query);
				if ($result) {
					$record = mysql_fetch_row($result);
					return $record[0];
				}
				//return mysql_insert_id();
		   	} else {
		   		return 0;
		   	}
		}
	}

	//HAM 24Nov2008 
	public static function getTravelPlansUserWithTravelerID($travelerID) {
    	$conn = new Connection();
    	$rs   = new Recordset($conn);
    	
		$rs->Execute('SELECT userID, travelPlansDisplayed, travelPlansIncludePast FROM SocialApplication.tbUser WHERE travelerID=' . $travelerID);

		$row = mysql_fetch_array($rs->Resultset());
		if ($row) {
		  return array('userID' => $row['userID'], 'travelPlansDisplayed' => $row['travelPlansDisplayed'], 'travelPlansIncludePast' => $row['travelPlansIncludePast']);
		} else {
		   	// check if user is an Advisor
		   	require_once 'travellog/model/Class.Traveler.php';
   	
		   	$travelerType = new Traveler($travelerID);

		   	if (!$travelerType->isAdvisor() && self::travelerIDExists($travelerID)) {
		    	$rs->Execute("INSERT INTO SocialApplication.tbUser (travelerID) VALUES ($travelerID)");
		    	// TODO: use SELECT LAST_INSERT_ID()
				$query = "SELECT LAST_INSERT_ID()";
				$result = mysql_query($query);
				if ($result) {
					$record = mysql_fetch_row($result);
					return array('userID' => $record[0]);
				}
				//return mysql_insert_id();
		   	} else {
		   		return array();
		   	}
		}
	}	
	
	//HAM 24Nov2008
	public static function editUserSettings($settings) {
	    $conn = new Connection();
    	$rs   = new Recordset($conn);

		$sql = "UPDATE SocialApplication.tbUser SET travelPlansDisplayed={$settings['travelPlansDisplayed']}, travelPlansIncludePast={$settings['travelPlansIncludePast']} WHERE travelerID={$settings['travelerID']}";
    	$rs->Execute($sql);
	}
	
	//################################# TO BE MOVED - HELPERS ########################################################//
	// check to make sure a valid travel record exist
	public static function isValid($travelWishID, $userID) {
		$conn = new Connection();
		$rs   = new Recordset($conn);

		$query = 'SELECT travelWishID ' .
				 'FROM SocialApplication.tbTravelWish ' .
				 'WHERE travelWishID = ' . $travelWishID . ' ' .
				 'AND userID = ' . $userID;
		$rs->Execute($query);
		if (mysql_fetch_array($rs->Resultset())) {
			return true;
		}

		return false;
	}
	
	//############################################ userIDExists #################################################//
	// check if a my travel user ID exist by Jonas Tandinco Sep/12/2007
	public static function userIDExists($userID) {
		$conn = new Connection('SocialApplication');
		$rs = new Recordset($conn);
		
		$sql = "SELECT userID " .
			   "FROM tbUser " .
			   "WHERE userID = $userID";
			   
		$rs->Execute($sql);
		
		if (mysql_fetch_array($rs->Resultset())) {
			return true;
		}
		
		return false;
	}
	
	//############################################ travelerIDExists #################################################//
	// check to make sure a valid traveler id exists on the database
	public static function travelerIDExists($travelerID) {
		$conn = new Connection('Travel_Logs');
		$rs = new Recordset($conn);
		
		$sql = "SELECT travelerID " .
			   "FROM tblTraveler " .
			   "WHERE travelerID = $travelerID";
			   
		$rs->Execute($sql);
		
		if (mysql_fetch_array($rs->Resultset())) {
			return true;
		}
		
		return false;
	}
	
   /*
    * Added By: Aldwin S. Sabornido
    * Date    : August 14, 2007
    * Purpose : To determine which travel wish items to be edited.
    */
   public static function getTravelWishIDByTravelID( $travelID ){
  		//TODO: Inform Harold that I added a new function. 
  		$conn = new Connection('SocialApplication');
  		$rs   = new Recordset($conn);
  		
  		$sql  = sprintf("SELECT travelWishID FROM tbTraveltoTravelWish WHERE travelID = %d", $travelID);
  		$rs->Execute($sql);
  		 
  		return ( $rs->Recordcount() )? $rs->Result(0, "travelWishID"): 0;
   }
   
   public static function saveTraveltoTravelWish($formvars = array()){
   		//TODO: Inform Harold that I added a new function. 
   		$conn = new Connection('SocialApplication');
  		$rs   = new Recordset($conn);	
  		$sql  = sprintf("INSERT INTO tbTraveltoTravelWish (travelID, travelWishID) VALUES (%d,%d)", $formvars["travelID"],$formvars["travelWishID"]);
  		$rs->Execute($sql);
   }
   
   public static function deleteTraveltoTravelWish($formvars = array()){
   		//TODO: Inform Harold that I added a new function. 
   		$conn = new Connection('SocialApplication');
  		$rs   = new Recordset($conn);	
  		$sql  = sprintf("DELETE FROM tbTraveltoTravelWish WHERE travelID = %d", $formvars["travelID"]);
  		$rs->Execute($sql); 
   }
}

?>