<?
	
	/**
	* class.Listing.php
	* @author :	 marc
	* Sep 13, 2006
	*/
	
	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
	require_once('Class.StudentPreference.php');
	require_once('Class.Listing.php');
	require_once('Class.Country.php');
	require_once('Class.Client.php');
	require_once('Class.ListingCountry.php');
	require_once('Class.Traveler.php');
		
	/**
	 * constants
	 * VOLUNTEER_ABROAD = 1
	 * TEACH_ABROAD = 2
	 * INTERNSHIP_ABROAD = 3
	 * JOBS_ABROAD = 4
	 * STUDY_ABROAD = 5
	 * LANGUAGE_SCHOOLS = 6 
	 */ 
	define ('VOLUNTEER_ABROAD',1);
	define ('TEACH_ABROAD',2);
	define ('INTERNSHIP_ABROAD',3);
	define ('JOBS_ABROAD',4);
	define ('STUDY_ABROAD',5);
	define ('LANGUAGE_SCHOOLS',6);

	
	/**
	 * @var object nConn - see Class.Connection.php
	 * @var object nRecordset - see Class.Recordset.php
	 * 
	 * all variables below are attributes of GA listing
	 * @var object client - see Class.Client.php
	 * @var array duration
	 * @var array term 
	 * @var array language
	 * @var array languageused
	 * @var array feeincludes
	 * @var array benefits
	 * @var array teachcert
	 * @var array participant
	 * @var array living
	 * @var array application
	 * @var array postservice
	 * @var array program
	 * @var array arrProgramID
	 * @var array country
	 * @var array internjobtype
	 * @var array acaddegree
	 * @var array degprog
	 * @var string description
	 * @var string highlights
	 * @var string listingname
	 * @var string url
	 * @var string email
	 * @var string dates
	 * @var string cost
	 * @var string costincl
	 * @var string length
	 * @var string creditavail
	 * @var string experiencedesc
	 * @var string groupsize
 	 * @var string minrequirement
	 * @var string quallskills - skills
	 * @var string stipend
	 * @var string scholarshipexplain
	 * @var string typicalduration
	 * @var string agerange
	 * @var string religious
	 * @var string typicalvolunteer
	 * @var string applicationtime
	 * @var string mission
	 * @var string yearbegan
	 * @var int couples - flag | default : 0
	 * @var int faith - flag | default : 0
	 * @var int families - flag | default : 0
	 * @var int proselytizing -flag | default : 0
	 * @var int isJob - flag | default : 0
	 * @var int isIntern - flag | default : 0
	 * @var int experience - flag :: experience required | default : 0
	 * @var int indtravel - flag :: individual travel | default : 0
	 * @var int grouptravel - flag :: group travel | default : 0
	 * @var int independent - flag :: work independent | default : 0
	 * @var int listingID - default 0, listingID of listing
	 * @var int studytypeID
	*/
	class listing{
	
		// db variables
		private $nConn;
		private $nRecordset; 
		
		// class client
		private $client;
						
		// array properties
		private $duration = array();
		private $term = array();
		private $language = array();
		private $languageused = array();
		private $feeincludes = array();	
		private $benefits = array();
		private $teachcert = array();
		private $participant = array();
		private $living = array();
		private $application = array();
		private $postservice = array();
		private $program = array();
		private $arrProgramID = array();
		private $country = array();
		private $internjobtype = array();
		private $acaddegree = array();
		private $degprog = array();
		
		
		// simple properties		
		private $listingID = 0;
		private $description = '';
		private $highlights = '';
		private $listingname = '';
		private $url = '';
		private $email = '';
		private $dates = '';
		private $cost = '';
		private $costincl = '';
		private $length = '';
		private $creditavail = '';
		private $experience = 0;
		private $experiencedesc = '';
		private $indtravel = 0;
		private $grouptravel = 0;
		private $independent = 0;
		private $groupsize = '';
		private $studytypeID = 0;
		private $minrequirement = '';
		private $qualskills = '';
		private $stipend = '';
		private $scholarshipexplain = '';
		private $typicalduration = '';
		private $agerange = '';
		private $couples = 0;
		private $faith = 0;
		private $families = 0;
		private $proselytizing = 0;
		private $religious = '';
		private $typicalvolunteer = '';
		private $applicationtime = '';
		private $mission = '';
		private $yearbegan = '';
		private $isJob = 0;
		private $isIntern = 0;
		
		
		/**
		 * constructor
		 * @param int listingID
		 */
		function listing($listingID){
			
			$arrProgram = array();
			$arrLiving = array();
			$arrDuration = array();
			$arrTerm = array();
			$arrLanguage = array();
			$arrLanguageUsed = array();
			$arrFeeIncludes = array();
			$arrBenefits = array();
			$arrAcadDegree = array();
			$arrTeachCert = array();
			$arrParticipant = array();
			$arrApplication = array();
			$arrPostService = array();
			$arrInternType = array();
			$arrDegProg = array();
			
			// create database connection
			try{
				$this->nConn = new Connection();
				$this->nRecordset = new Recordset($this->nConn);
			}
			catch(Exception $e){
				die('unable to create a connection - ' . $e);
			}			
			
			$this->setListingID($listingID);
			

						
			// program
			$sqlquery = "SELECT incprogramID
					FROM GoAbroad_Main.tbprogram,Travel_Logs.tblIncProgram,GoAbroad_Main.tblistclasstocountry
					WHERE GoAbroad_Main.tbprogram.programID = Travel_Logs.tblIncProgram.programID
					AND GoAbroad_Main.tblistclasstocountry.programID = GoAbroad_Main.tbprogram.programID
					AND Travel_Logs.tblIncProgram.programID = GoAbroad_Main.tblistclasstocountry.programID
					AND GoAbroad_Main.tblistclasstocountry.listingID = $this->listingID
					GROUP BY GoAbroad_Main.tblistclasstocountry.programID";
			$myquery = $this->nRecordset->Execute($sqlquery);

			while($row = mysql_fetch_array($myquery)){ 
				$nProg = new listingProgram($row["incprogramID"]); 
				$this->arrProgramID[] = $row["incprogramID"];
				$arrProgram[] = $nProg;
			}
			$this->setProgram($arrProgram);
			
			// country
			$this->setCountry(ListingCountry::getListingCountries($this->listingID));
			
			$sqlquery = "SELECT clientID,description,highlights,listingname,url,email,dates,cost,costincl,
					length,creditavail,experience,experiencedescription,indtravel,grouptravel,independent,
					groupwork,groupsize,studytypeID,minrequirementID
					FROM GoAbroad_Main.tblisting
					WHERE listingID = $this->listingID";
			$myquery = $this->nRecordset->Execute($sqlquery);

			$listingAttributes = mysql_fetch_array($myquery);
			$this->setClient(new client($listingAttributes['clientID']));
			$this->setDescription($listingAttributes['description']);
			$this->setHighlights($listingAttributes['highlights']);
			$this->setListingname($listingAttributes['listingname']);
			$this->setUrl($listingAttributes['url']);
			$this->setEmail($listingAttributes['email']);
			$this->setDates($listingAttributes['dates']);
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) || 
				in_array(STUDY_ABROAD,$this->arrProgramID) || 
				in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID))
					$this->setCost($listingAttributes['cost']);
					$this->setCostInclude($listingAttributes['costincl']);
			if( in_array(TEACH_ABROAD,$this->arrProgramID) || 
				in_array(JOBS_ABROAD,$this->arrProgramID) )
					$this->setLength($listingAttributes['length']);
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) || 
				in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) )
					$this->setCreditAvail($listingAttributes['creditavail']);
			$this->setExperience($listingAttributes['experience']);
			$this->setExperienceDesc($listingAttributes['experiencedescription']);
			$this->setIndTravel($listingAttributes['indtravel']);
			$this->setGroupTravel($listingAttributes['grouptravel']);
			$this->setIndependent($listingAttributes['independent']);
			$this->setGroupWork($listingAttributes['groupwork'],$listingAttributes['groupsize']);
			
			if( in_array(STUDY_ABROAD,$this->arrProgramID) )
				$this->setStudyTypeID($listingAttributes['studytypeID']);
			
			if( in_array(STUDY_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(JOBS_ABROAD,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ||
				in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) ){
					$sqlquery = "SELECT acaddegree
							FROM GoAbroad_Main.tbacaddegree
							WHERE acaddegreeID = " . $listingAttributes['minrequirementID'];
					$myquery = $this->nRecordset->Execute($sqlquery);
					if( mysql_num_rows($myquery) ){
						$row = mysql_fetch_array($myquery);		
						$this->setMinRequirement($row['acaddegree']);	
					}
			}
			
			if( in_array(JOBS_ABROAD,$this->arrProgramID) )	
				$this->setIsJob(1);
				
			if( in_array(INTERNSHIP_ABROAD,$this->arrProgramID) )	
				$this->setIsInternship(1);	
			
			if( in_array(STUDY_ABROAD,$this->arrProgramID) ){
				$sqlquery = "SELECT degprog
						FROM GoAbroad_Main.tblistingtodegprog,GoAbroad_Main.tbdegprog
						WHERE GoAbroad_Main.tblistingtodegprog.degprogID = GoAbroad_Main.tbdegprog.degprogID
						AND listingID = " . $this->listingID;
				$myquery = $this->nRecordset->Execute($sqlquery);
				if( mysql_num_rows($myquery) ){
					while( $row = mysql_fetch_array($myquery) )
						$arrDegProg[] = $row['degprog'];
					$this->setSubjectArea($arrDegProg);
				}
			}
			
			// application time,yearbegan,mission
			$sqlquery = "SELECT applicationtime,mission,missionapproval,yearbegan
					FROM GoAbroad_Main.tbclient
					WHERE clientID = " . $listingAttributes['clientID'];
			$myquery = $this->nRecordset->Execute($sqlquery);
			if( mysql_num_rows($myquery) ){
				$row = mysql_fetch_array($myquery);		
				$this->setApplicationTime($row['applicationtime']);	
				$this->setMission($row['missionapproval'],$row['mission']);
				$this->setYearBegan($row['yearbegan']);
			}
			
			// intern/job type
			if( in_array(JOBS_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT interntype
							FROM GoAbroad_Main.tblistingtointerntype,GoAbroad_Main.tbinterntype
							WHERE GoAbroad_Main.tblistingtointerntype.interntypeID = GoAbroad_Main.tbinterntype.interntypeID
							AND listingID = $this->listingID";	
					$myquery = $this->nRecordset->Execute($sqlquery);
					while( $row = mysql_fetch_array($myquery) )		
						$arrInternType[] = $row['interntype'];
					$this->setInternType($arrInternType);
			}		
			
			// duration
			if ( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ){
				$sqlquery = "SELECT timespan
					FROM GoAbroad_Main.tbtimespan,GoAbroad_Main.tblistingtotimespan
					WHERE GoAbroad_Main.tbtimespan.timespanID = GoAbroad_Main.tblistingtotimespan.timespanID
					AND listingID = $this->listingID";
				$myquery = $this->nRecordset->Execute($sqlquery);
				while ( $row = mysql_fetch_array($myquery)){
					$arrDuration[] = $row['timespan'];
				}
				$this->setDuration($arrDuration);
			}

			// qualskills
			if( in_array(JOBS_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT qualskills,stipend
							FROM GoAbroad_Main.tbqualskills
							WHERE listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);
					$row = mysql_fetch_array($myquery);
					$this->setQualSkills($row['qualskills']);
					$this->setStipend($row['stipend']);
			}
			
			// term
			if( in_array(STUDY_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT term
							FROM GoAbroad_Main.tblistingtoterm,GoAbroad_Main.tbterm
							WHERE GoAbroad_Main.tblistingtoterm.termID = GoAbroad_Main.tbterm.termID
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					while ( $row = mysql_fetch_array($myquery) )
						$arrTerm[] = $row['term'];			
					$this->setTerm($arrTerm);
			}

			// language 
			if( in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT language
							FROM GoAbroad_Main.tblistingtolanguage,GoAbroad_Main.tblanguage
							WHERE GoAbroad_Main.tblistingtolanguage.languageID = GoAbroad_Main.tblanguage.languageID
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					while ( $row = mysql_fetch_array($myquery) )
						$arrLanguage[] = $row['language'];			
					$this->setLanguage($arrLanguage);
			}
			
			// language used
			if( in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT language
							FROM GoAbroad_Main.tblistingtolanguageused,GoAbroad_Main.tblanguage
							WHERE GoAbroad_Main.tblistingtolanguageused.languageID = GoAbroad_Main.tblanguage.languageID
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					while ( $row = mysql_fetch_array($myquery) )
						$arrLanguageUsed[] = $row['language'];			
					$this->setLanguageUsed($arrLanguageUsed);
			}
			
			// fee included
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT feeincludes
							FROM GoAbroad_Main.tblistingtofeeincludes,GoAbroad_Main.tbfeeincludes
							WHERE GoAbroad_Main.tblistingtofeeincludes.feeincludesID = GoAbroad_Main.tbfeeincludes.feeincludesID
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					while ( $row = mysql_fetch_array($myquery) )
						$arrFeeIncludes[] = $row['feeincludes'];			
					$this->setFeeIncludes($arrFeeIncludes);
			}
			
			// scholarship		
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(STUDY_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT scholarshipsavail,scholarshipexplain
							FROM GoAbroad_Main.tbscholarship
							WHERE listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					if( $row = mysql_fetch_array($myquery) )
						$this->setScholarship($row['scholarshipsavail'],$row['scholarshipexplain']);
			}			
					
			// benefits
			if( in_array(JOBS_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT benefits
							FROM GoAbroad_Main.tblistingtobenefits,GoAbroad_Main.tbbenefits
							WHERE GoAbroad_Main.tblistingtobenefits.benefitsID = GoAbroad_Main.tbbenefits.benefitsID 
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);
					while ( $row = mysql_fetch_array($myquery) )
						$arrBenefits[] = $row['benefits'];			
					$this->setBenefits($arrBenefits);
			}	
			
			// degrees available
			if( in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) ){
					$sqlquery = "SELECT acaddegree
							FROM GoAbroad_Main.tblistingtoacaddegree,GoAbroad_Main.tbacaddegree
							WHERE GoAbroad_Main.tblistingtoacaddegree.acaddegreeID = GoAbroad_Main.tbacaddegree.acaddegreeID
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					while ( $row = mysql_fetch_array($myquery) )
						$arrAcadDegree[] = $row['acaddegree'];			
					$this->setAcadDegree($arrAcadDegree);
			}
				
			// teach certificate
			if( in_array(TEACH_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT teachcert
							FROM GoAbroad_Main.tblistingtoteachcert,GoAbroad_Main.tbteachcert
							WHERE GoAbroad_Main.tblistingtoteachcert.teachcertID = GoAbroad_Main.tbteachcert.teachcertID
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					while ( $row = mysql_fetch_array($myquery) )
						$arrTeachCert[] = $row['teachcert'];			
					$this->setTeachCert($arrTeachCert);
			}	
			
			// participants
			$sqlquery = "SELECT participant
					FROM GoAbroad_Main.tblistingtoparticipant,GoAbroad_Main.tbparticipant
					WHERE GoAbroad_Main.tblistingtoparticipant.participantID = GoAbroad_Main.tbparticipant.participantID
					AND listingID = $this->listingID";
			$myquery = $this->nRecordset->Execute($sqlquery);		
			while ( $row = mysql_fetch_array($myquery) )
				$arrParticipant[] = $row['participant'];			
			$this->setParticipant($arrParticipant);
			
			// living arrangements
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(STUDY_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT living
							FROM GoAbroad_Main.tbliving,GoAbroad_Main.tblistingtoliving
							WHERE GoAbroad_Main.tbliving.livingID = GoAbroad_Main.tblistingtoliving.livingID 
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					while ( $row = mysql_fetch_array($myquery) )
						$arrLiving[] = $row['living'];			
					$this->setLiving($arrLiving);
			}
			
			// volunteer attributes
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ){
					$sqlquery = "SELECT timespan,agerange,couples,faith,families,
							proselytizing,religious,religioustype,typicalvolunteer
							FROM GoAbroad_Main.tbvolunteer,GoAbroad_Main.tbtimespan
							WHERE GoAbroad_Main.tbvolunteer.typicaltimespanID = GoAbroad_Main.tbtimespan.timespanID
							AND listingID = $this->listingID";
					$myquery = $this->nRecordset->Execute($sqlquery);		
					if( $row = mysql_fetch_array($myquery) ){
						$this->setTypicalTimeSpan($row['timespan']);
						$this->setAgeRange($row['agerange']);
						$this->setCouples($row['couples']);
						$this->setFaith($row['faith']);
						$this->setFamilies($row['families']);
						$this->setProselytizing($row['proselytizing']);
						$this->setReligious($row['religious'],$row['religioustype']);
						$this->setTypicalVolunteer($row['typicalvolunteer']);
					}
			}
			
			// application process
			$sqlquery = "SELECT application
					FROM GoAbroad_Main.tbapplication,GoAbroad_Main.tblistingtoapplication
					WHERE GoAbroad_Main.tbapplication.applicationID = GoAbroad_Main.tblistingtoapplication.applicationID
					AND listingID = $this->listingID";
			$myquery = $this->nRecordset->Execute($sqlquery);
			if ( mysql_num_rows($myquery) == 0 ){
				$sqlquery = "SELECT application 
						FROM GoAbroad_Main.tbclienttoapplication,GoAbroad_Main.tbapplication
						WHERE GoAbroad_Main.tbclienttoapplication.clientID = " . $listingAttributes['clientID'] .
						" AND GoAbroad_Main.tbapplication.applicationID = GoAbroad_Main.tbclienttoapplication.applicationID";
				$myquery = $this->nRecordset->Execute($sqlquery);						
			}
			while ( $row = mysql_fetch_array($myquery) )
				$arrApplication[] = $row['application'];
			$this->setApplication($arrApplication);
			
			// post services
			$sqlquery = "SELECT postservice
					FROM GoAbroad_Main.tbpostservice,GoAbroad_Main.tblistingtopostservice
					WHERE GoAbroad_Main.tbpostservice.postserviceID = GoAbroad_Main.tblistingtopostservice.postserviceID
					AND listingID = $this->listingID";
			$myquery = $this->nRecordset->Execute($sqlquery);
			while ( $row = mysql_fetch_array($myquery) )
				$arrPostService[] = $row['postservice'];			
			$this->setPostService($arrPostService);
			
			return $this;
		}
		
		/**
		* setters
		*/
		
		/**
		 * @param int listingID
		 */
		function setListingID($listingID){
			$this->listingID = $listingID;
		}
		
		/**
		 * @param object client - GA client (owner of listing), see Class.Client.php
		 */
		function setClient($client){
			$this->client = $client;
		}
		
		/**
		 * @param string applicationtime
		 */
		function setApplicationTime($applicationtime){
			$this->applicationtime = $applicationtime;
		}
		
		/**
		 * @param array program
		 */
		function setProgram($program){
			$this->program = $program;
		}
		
		/**
		 * @param array country - array of country objects
		 */
		function setCountry($country){
			$this->country = $country;
		}
		
		/**
		 * @param array degprog
		 */
		function setSubjectArea($degprog){
			if( in_array(STUDY_ABROAD,$this->arrProgramID) )
				$this->degprog = $degprog;
		}
		
		/**
		 * @param string description
		 */
		function setDescription($description){
			$this->description = $description;
		}
		
		/**
		 * @param string highlights
		 */
		function setHighlights($highlights){
			$this->highlights = $highlights;
		}
		
		/**
		 * @param string listingname
		 */
		function setListingName($listingname){
			$this->listingname = $listingname;
		}
		
		/**
		 * @param string url
		 */
		function setUrl($url){
			$this->url = $url;
		}
		
		/**
		 * @param string email
		 */
		function setEmail($email){
			$this->email = $email;
		}
		
		/**
		 * @param string dates
		 */
		function setDates($dates){
			$this->dates = $dates;
		}
		
		/**
		 * @param string cost
		 */
		function setCost($cost){
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) || 
				in_array(STUDY_ABROAD,$this->arrProgramID) || 
				in_array(LANGUAGE_SCHOOLS,$this->arrProgramID)){
					if ($cost > 0)
						$this->cost = $cost;
			}
		}
		
		/**
		 * @param string costincl
		 */
		function setCostInclude($costincl){
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) || 
				in_array(STUDY_ABROAD,$this->arrProgramID) || 
				in_array(LANGUAGE_SCHOOLS,$this->arrProgramID)){
					$this->costincl = $costincl;
			}
		}
		
		/**
		 * @param string length
		 */
		function setLength($length){
			if( in_array(TEACH_ABROAD,$this->arrProgramID) || 
				in_array(JOBS_ABROAD,$this->arrProgramID) )
					$this->length = $length;	
		}
		
		/**
		 * @param string creditavail
		 */
		function setCreditAvail($creditavail){
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) || 
				in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) )
				$this->creditavail = $creditavail;
		}
		
		/**
		 * @param int experience
		 */
		function setExperience($experience){
				$this->experience = $experience;
		}
		
		/**
		 * @param string experiencedescription
		 */
		function setExperienceDesc($experiencedescription){
			$this->experiencedesc = $experiencedescription;
		}
		
		/**
		 * @param int indtravel
		 */
		function setIndTravel($indtravel){
			$this->indtravel = $indtravel;
		}
		
		/**
		 * @param int grouptravel
		 */
		function setGroupTravel($grouptravel){
			$this->grouptravel = $grouptravel;
		}
		
		/**
		 * @param int independent
		 */
		function setIndependent($independent){
			$this->independent = $independent;
		}
		
		/**
		 * @param int groupwork
		 * @param string groupsize
		 */
		function setGroupWork($groupwork = 0,$groupsize){
			if( $groupwork > 0 )
				$this->groupsize = $groupsize;
		}
		
		/**
		 * @param int studytypeID
		 */
		function setStudyTypeID($studytypeID){
			if( in_array(STUDY_ABROAD,$this->arrProgramID) )
				$this->studytypeID = $studytypeID;
		}
		
		/**
		 * @param string minrequirement
		 */
		function setMinRequirement($minrequirement){
			if( in_array(STUDY_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(JOBS_ABROAD,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ||
				in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) )
					$this->minrequirement = $minrequirement;
		}
		
		/**
		 * @param array internjobtype
		 */
		function setInternType($internjobtype){
			if( in_array(TEACH_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) )
					$this->internjobtype = $internjobtype;
		}
		
		/**
		 * @param array duration
		 */
		function setDuration($duration){
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
				$this->duration = $duration;
		}
		
		/**
		 * @param string qualskills
		 */
		function setQualSkills($qualskills){
			if( in_array(JOBS_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ){
					$this->qualskills = $qualskills;
			}		
		}
		
		/**
		 * @param string stipend
		 */
		function setStipend($stipend){
			if( in_array(JOBS_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ){
					$this->stipend = $stipend;
			}
		}
		
		/**
		 * @param array term
		 */
		function setTerm($term){
			if( in_array(STUDY_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ){
					$this->term = $term;
			}		
		}
		
		/**
		 * @param array language
		 */
		function setLanguage($language){
			if( in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
					$this->language = $language;
		}
		
		/**
		 * @param array languageused
		 */
		function setLanguageUsed($languageused){
			if( in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ){
					$this->languageused = $languageused;
			}
		}
		
		/**
		 * @param array feeincludes
		 */
		function setFeeIncludes($feeincludes){
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ){
					$this->feeincludes = $feeincludes;
			}
		}
		
		/**
		 * @param int scholarshipsavail
		 * @param string scholarshipexplain
		 */
		function setScholarship($scholarshipsavail = 0 ,$scholarshipexplain){
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(STUDY_ABROAD,$this->arrProgramID) )
					if( $scholarshipsavail > 0 )
						$this->scholarshipexplain = $scholarshipexplain;
		}
		
		/**
		 * @param array benefits
		 */
		function setBenefits($benefits){
			if( in_array(JOBS_ABROAD,$this->arrProgramID) ||
				in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(INTERNSHIP_ABROAD,$this->arrProgramID) ||
				in_array(TEACH_ABROAD,$this->arrProgramID) ){
					$this->benefits = $benefits;
			}		
		}
		
		/**
		 * @param array acaddegree
		 */
		function setAcadDegree($acaddegree){
			if ( in_array(LANGUAGE_SCHOOLS,$this->arrProgramID) )
				$this->acaddegree = $acaddegree;
		}
		
		/**
		 * @param array teachcert
		 */
		function setTeachCert($teachcert){
			if ( in_array(TEACH_ABROAD,$this->arrProgramID) )
				$this->teachcert = $teachcert;
		}
		
		/** 
		 * @param array participant
		 */
		function setParticipant($participant){
			$this->participant = $participant;
		}
		
		/**
		 * @param array living
		 */
		function setLiving($living){
			if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) ||
				in_array(STUDY_ABROAD,$this->arrProgramID) )
					$this->living = $living;
		}
		
		/**
		 * @param string duration
		 */
		function setTypicalTimeSpan($duration){
		 	if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
		 		$this->typicalduration = $duration;
		}
		
		/**
		 * @param string agerange
		 */
		function setAgeRange($agerange){
		 	if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
		 		$this->agerange = $agerange;
		}	
		
		/**
		 * @param int couples
		 */
		function setCouples($couples){
		 	if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
		 		$this->couples = $couples;
		}
		
		/**
		 * @param int faith
		 */
		function setFaith($faith){
		 	if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
		 		$this->faith = $faith;
		}
		
		/**
		 * @param int families
		 */
		function setFamilies($families){
		 	if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
		 		$this->families = $families;
		}	
		
		/**
		 * @param int proselytizing
		 */
		function setProselytizing($proselytizing){
		 	if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
		 		$this->proselytizing = $proselytizing;
		}		
		
		/**
		 * @param int religious
		 * @param string religiuostype
		 */
		function setReligious($religious = 0,$religioustype){
		 	if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
		 		if( $religious > 0)
			 		$this->religious = $religioustype;
		}		
		
		/**
		 * @param string typicalvolunteer
		 */
		function setTypicalVolunteer($typicalvolunteer){
		 	if( in_array(VOLUNTEER_ABROAD,$this->arrProgramID) )
			 		$this->typicalvolunteer = $typicalvolunteer;
		}
		
		/**
		 * @param array application
		 */
		function setApplication($application){
			$this->application = $application;
		}
		
		/**
		 * @param array postservice
		 */
		function setPostService($postservice){
			$this->postservice = $postservice;
		}
		
		/**
		 * @param int missionapproved
		 * @param string mission
		 */
		function setMission($missionapproved = 0,$mission){
			if( $missionapproved > 0)
				$this->mission = $mission;
		}
		
		/**
		 * @param string yearbegan
		 */
		function setYearBegan($yearbegan){
			$this->yearbegan = $yearbegan;
		}
		
		/**
		 * @param int isJob
		 */
		function setIsJob($isJob){
			$this->isJob = $isJob;
		}
		
		/**
		 * @param string isInternship
		 */
		function setIsInternship($isInternship){
			$this->isInternship = $isInternship;
		}
		
		/**
		* getters
		*/
		
		/**
		 * @return int self::listingID
		 */
		function getListingID(){
			return $this->listingID;
		}
		
		/**
		 * @return object self::client - see Class.Client.php
		 */
		function getClient(){
			return $this->client;
		}
		
		/**
		 * @return string self::applicationtime
		 */
		function getApplicationTime(){
			return $this->applicationtime;
		}
		
		/**
		 * @return array self::program
		 */
		function getProgram(){
			return $this->program;
		}
		
		/**
		 * @return array self::country
		 */
		function getCountry(){
			return $this->country;
		}
		
		/**
		 * @return array self::arrProgram
		 */
		function getProgramID(){
			return $this->arrProgramID;
		}
		
		/**
		 * @return array self::degprog
		 */
		function getSubjectArea(){
			return $this->degprog;
		}
		
		/**
		 * @return string self::description
		 */
		function getDescription(){
			return $this->description;
		}
		
		/**
		 * @return string self::highlights
		 */
		function getHighlights(){
			return $this->highlights;
		}
		
		/**
		 * @return string self::listingname
		 */
		function getListingName(){
			return $this->listingname;
		}
		
		/**
		 * @return string self::url
		 */
		function getUrl(){
			return $this->url;
		}
		
		/**
		 * @return string self::email
		 */
		function getEmail(){
			return $this->email;
		}
		
		/**
		 * @return string self::dates
		 */
		function getDates(){
			return $this->dates;
		}
		
		/**
		 * @return string self::cost
		 */
		function getCost(){
			return $this->cost;
		}
		
		/**
		 * @return string self::costincl
		 */
		function getCostInclude(){
			return $this->costincl;
		}

		/**
		 * @return string self::legnth
		 */
		function getLength(){
			return $this->length;
		}
		
		/**
		 * @return int self::creditavail
		 */
		function getCreditAvail(){
			return $this->creditavail;
		}
		
		/**
		 * @return int self::experience
		 */
		function getExperience(){
			return $this->experience;
		}
		
		/**
		 * @return string self::experiencedesc
		 */
		function getExperienceDesc(){
			return $this->experiencedesc;
		}
		
		/**
		 * @return int self::indtravel
		 */
		function getIndTravel(){
			return $this->indtravel;
		}
		
		/**@return int self::grouptravel
		 * 
		 */
		function getGroupTravel(){
			return $this->grouptravel;
		}
		
		/**
		 * @return int self::independent
		 */
		function getIndependent(){
			return $this->independent;
		}
		
		/**
		 * @return int self::groupwork
		 */
		function getGroupWork(){
			return $this->groupsize;
		}
		
		/**
		 * @return int self::studytypeID
		 */
		function getStudyTypeID(){
			return $this->studytypeID;
		}
		
		/**
		 * @return string self::minrequirement
		 */
		function getMinRequirement(){
			return $this->minrequirement;
		}
		
		/**
		 * @return array self::internjobtype
		 */
		function getInternType(){
			return $this->internjobtype;
		}
		
		/**
		 * @return array self::duration
		 */
		function getDuration(){
			return $this->duration;
		}
		
		/**
		 * @return string self::qualskills
		 */
		function getQualSkills(){
			return $this->qualskills;
		}
		
		/**
		 * @return string self::stipend
		 */
		function getStipend(){
			return $this->stipend;
		}
		
		/**
		 * @return array self::term
		 */
		function getTerm(){
			return $this->term;
		}
		
		/**
		 * @return array self::language
		 */
		function getLanguage(){
			return $this->language;
		}
		
		/**
		 * @return array self::languageused
		 */
		function getLanguageUsed(){
			return $this->languageused;
		}
		
		/**
		 * @return array self::feeincludes
		 */
		function getFeeIncludes(){
			return $this->feeincludes;
		}
		
		/**
		 * @return string self::scholarshipexplain
		 */
		function getScholarship(){
			return $this->scholarshipexplain;
		}
		
		/**
		 * @return array self::benefits
		 */
		function getBenefits(){
			return $this->benefits;
		}
		
		/**
		 * @return array self::acaddegree
		 */
		function getAcadDegree(){
			return $this->acaddegree;
		}
		
		/**
		 * @return array self::teachcert
		 */
		function getTeachCert(){
			return $this->teachcert;
		}
		
		/**
		 * @return array self::participant
		 */
		function getParticipant(){
			return $this->participant;
		}
		
		/**
		 * @return array self::living
		 */
		function getLiving(){
			return $this->living;
		}
		
		/**
		 * @return string self::typicalduration
		 */
		function getTypicalDuration(){
		 	return $this->typicalduration;
		}
		
		/**
		 * @return string agerange
		 */
		function getAgeRange(){
		 	return $this->agerange;
		}
		
		/**
		 * @return int couples
		 */
		function getCouples(){
		 	return $this->couples;
		}
		
		/**
		 * @return int self::faith
		 */
		function getFaith(){
		 	return $this->faith;
		}
		
		/**
		 * @return int self::families
		 */
		function getFamilies(){
		 	return $this->families;
		}
		
		/**
		 * @return int self::proselytizing
		 */
		function getProselytizing(){
		 	return $this->proselytizing;
		}
		
		/**
		 * @return int self::religious
		 */
		function getReligious(){
		 	return $this->religious;
		}
		
		/**
		 * @return string self::typicalvolunteer
		 */
		function getTypicalVolunteer(){
		 	return $this->typicalvolunteer; 
		}
		
		/**
		 * @return array self::application
		 */
		function getApplication(){
			return $this->application;
		}
		
		/**
		 * @return array self::postservice
		 */
		function getPostService(){
			return $this->postservice;
		}
		
		/**
		 * @return string self::mission
		 */
		function getMission(){
			return $this->mission;
		}
	
		/**
		 * @return string self::yearbegan
		 */	
		function getYearBegan(){
			return $this->yearbegan;
		}
		
		/** 
		 * @return int self::isJob 
		 */
		function getIsJob(){
			return $this->isJob;
		}
	
		/**
		 * @return int self::isInternship
		 */
		function getIsInternship(){
			return $this->isInternship;
		}
		
		/**
		*	static functions
		*/
		
		/**
		 * gets referred GA Listings to student
		 * @param int travelerID
		 * @param int page
		 * @param int rowsperpage
		 * @return array arrListings - array of listing objects
		 */
		public static function getStudentReferedListings($travelerID,$page = 1,$rowsperpage = 1){
			
			$arrListings = array();
			
			try{
				$nConn = new Connection();
				$nRecordset = new Recordset($nConn);
			}
			catch(Exception $e){
				die('unable to create a connection - ' . $e);
			}
			
			$sqlquery = "SELECT listingname,instname,tblisting.listingID,tblisting.clientID
				FROM GoAbroad_Main.tblisting,GoAbroad_Main.tbclient,Travel_Logs.tblTravelertoListing
				WHERE GoAbroad_Main.tblisting.clientID = GoAbroad_Main.tbclient.clientID
				AND GoAbroad_Main.tblisting.listingID = Travel_Logs.tblTravelertoListing.listingID
				AND Travel_Logs.tblTravelertoListing.travelerID = $travelerID
				LIMIT " . (($page * $rowsperpage) - $rowsperpage) . "," . $rowsperpage;
			$myquery = $nRecordset->Execute($sqlquery);
			while( $row = mysql_fetch_array($myquery) ){
				$arrListings[] = $row;
			}
			
			return $arrListings;
		}
		
		/**
		 * gets total number of referred GA Listings to student
		 * @param int travelerID
		 * @return int number of referred GA Listings
		 */
		public static function getCountStudentReferedListings($travelerID){
			
			try{
				$nConn = new Connection();
				$nRecordset = new Recordset($nConn);
			}
			catch(Exception $e){
				die('unable to create a connection - ' . $e);
			}
			 
			$sqlquery = "SELECT COUNT(DISTINCT GoAbroad_Main.tblisting.listingID) as TotalRecords
				FROM GoAbroad_Main.tblisting,GoAbroad_Main.tbclient,Travel_Logs.tblTravelertoListing
				WHERE GoAbroad_Main.tblisting.clientID = GoAbroad_Main.tbclient.clientID
				AND GoAbroad_Main.tblisting.listingID = Travel_Logs.tblTravelertoListing.listingID
				AND Travel_Logs.tblTravelertoListing.travelerID = $travelerID";
			$myquery = $nRecordset->Execute($sqlquery);
			$row = mysql_fetch_array($myquery);
			return $row['TotalRecords'];
		}
		
		/**
		 * gets listingID of all GA Listings referred to student
		 * @param int travelerID
		 * @return array - array of listingID 
		 */
		public static function getListingIDWithStudentCommunication($travelerID){
			
			$arrListings = array();
			
			$traveler = new traveler($travelerID);
			
			try{
				$nConn = new Connection();
				$nRecordset = new Recordset($nConn);
			}
			catch(Exception $e){
				die('unable to create a connection - ' . $e);
			}
			
			$sqlquery = "SELECT listingID " .
					"FROM tblMessages,tblMessageToAttribute,tblMessageToInquiry " .
					"WHERE tblMessages.messageID = tblMessageToAttribute.messageID " .
					"AND tblMessageToAttribute.messageID = tblMessageToInquiry.messageID " .
					"AND tblMessages.messageID = tblMessageToInquiry.messageID " .
					"AND discriminator = 4 " .
					"AND recipientID = " . $traveler->getSendableID() . 
					" AND messageType = 4 " .
					"AND trashed = 0 " .
					"GROUP BY listingID";
			$myquery = $nRecordset->Execute($sqlquery);
			while( $row = mysql_fetch_array($myquery) ){
				// $nListing = new listing($row['listingID']);
				// $arrListings[] = $nListing;
				$arrListings[] = $row['listingID'];	
			}
			return $arrListings;
				
		}
		
		/**
		 * gets all listings of GA client
		 * @param int clientID
		 * @return array - array of listingIDs
		 */ 
		public static function getClientListings( $clientID ){
			$arrListings = array();
			
			try{
				$nConn = new Connection();
				$nRecordset = new Recordset($nConn);
			}
			catch(Exception $e){
				die('unable to create a connection - ' . $e);
			}
			
			$sqlquery = "SELECT tblMessageToInquiry.listingID " .
					"FROM Travel_Logs.tblMessageToInquiry, GoAbroad_Main.tblisting " .
					"WHERE tblMessageToInquiry.listingID = tblisting.listingID " .
					"AND clientID = $clientID " .
					"GROUP BY listingID";
			$nRecordset->Execute($sqlquery);
			$row = mysql_fetch_array($nRecordset->ResultSet());
			if( $row )
				for( $i=0; $i < count($row); $i++ )
					$arrListings[] = $row['listingID'];
			return $arrListings; 			
		
		} 
		 
		// get listing name only
		public static function getListingNameOnly( $listingID = 0 ){
			$nConn = new Connection();
			$nRecordset = new Recordset($nConn);
			$nListingName = NULL;
			
			if( 0 < $listingID){
				$nRecordset->Execute("SELECT listingname FROM GoAbroad_Main.tblisting WHERE listingID = $listingID");
				if( $row = mysql_fetch_array($nRecordset->ResultSet()) )
					$nListingName = $row["listingname"];
			}
			return $nListingName;
		}
			 
	}

?>