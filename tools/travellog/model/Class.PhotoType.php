<?php
/**
 * Created on Aug 3, 2006
 * @author Kerwin Gordo
 * Purpose: abstracts different types of photo
 */
 	class PhotoType { 	
 		public static  $PROFILE   		 	= 1;
 		public static  $TRAVELLOG  		= 2;
		public static  $RESUME	   		= 3;	
		public static  $ADMINGROUP	= 4;	
		public static  $FUNGROUP		= 5;	
		public static  $PROGRAM	   		= 6;
		public static  $PHOTOALBUM	= 7;
		public static  $GROUP				= 8;		
 		public static  $LOCATION			= 9; //photos by (Location) country. -> for travellog object only.
 		public static  $TRAVELERALBUM		= 10;
		public static  $CUSTOMPAGEHEADER = 12;
		public static  $ARTICLE = 11;

} 	
?>
