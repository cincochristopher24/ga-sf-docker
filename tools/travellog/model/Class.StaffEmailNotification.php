<?php
	require_once 'travellog/model/Class.MembersPanel.php';
	require_once 'travellog/model/Class.EmailNotification.php';

	class StaffEmailNotification{

		private $mTraveler		= null;
		private $mGroup			= null;
		private $mEditedMessage	= '';
		private $isNewAccount	= false;

		private $mBody			= '';
		private $mSubject		= '';
		private $mToEmail		= '';
		private $mFromName		= '';
		private $mFromEmail		= '';
		
		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setMessage($_message = ''){
			$this->mEditedMessage = $_message;
		}
		function setTraveler($_traveler = null){
			$this->mTraveler = $_traveler;
		}
		function setNewAccount($_isNew = false){
			$this->isNewAccount = $_isNew;
		}

		function retrieve(){
				$mProfile	= $this->mTraveler->getTravelerProfile();
				$this->mToEmail		= $mProfile->getEmail();
				$senderGroup		= $this->mGroup->getName();				

				if ($this->mGroup->isParent()){
					$mParent = $this->mGroup;
				}
				else{
					$mParent = $this->mGroup->getParent();
				}

				$mPassword	= $this->mTraveler->getPassword();
				$mToName	= ucfirst($mProfile->getFirstName()) . ' ' . ucfirst($mProfile->getLastName());
				$siteUrl	= "http://" . $_SERVER['SERVER_NAME'];
				$mLink		=  $siteUrl . $this->mGroup->getFriendlyURL();

				$mData		= MembersPanel::getConfigPerServer($_SERVER['SERVER_NAME']);

				$this->mFromEmail	= (0 < count($mData)) ? $mData['email'] : "admin@goabroad.net";
				$this->mFromName	= (0 < count($mData)) ? $mData['name'] : "The GoAbroad Network Team";
				$this->mSubject	= 'New Staff Invitation from the group ' . $senderGroup;

				$mSiteName = (0 < count($mData)) ? $mData['siteName'] : "GANET";

				if ($this->isNewAccount){
					if ($this->mGroup->isParent())
						$mEmailLink = $siteUrl . '/group-privacy.php?groupID=' . $mParent->getGroupID();
					else
						$mEmailLink = $siteUrl . '/privacy.php';

					$this->mBody = 'Hello ' . $mProfile->getEmail() . '!<br /><br />Greetings from GoAbroad.Net!<br /><br />' .
									$this->mEditedMessage . '<br /><br />A new GoAbroad.Net account was also created for you with the ' .
									'following information:<br /><br />Email: ' . $mProfile->getEmail() . '<br />Password: ' . $mPassword .
									'<br /><br />To visit ' . $this->mGroup->getName() . ' homepage, ' .
									'follow the link below:<br /><a href = "' . $mLink . '">' . $mLink . '</a><br /><br />' .
									'Many thanks,<br /><br />' . $this->mFromName . '<br />' .
									'<a href = "' . $siteUrl . '">' . $siteUrl . '</a><br /><br /><br />' .

									'- - - - - -<br />To manage the emails you receive from ' . $mSiteName .
									', please follow this link: <a href="' . $mEmailLink . '">' . $mEmailLink . '</a><br /><br />' .
									'This e-mail was sent from an unattended mailbox. Please do not reply. ' .
									'Send any questions to <a href="' . $siteUrl . '/feedback.php">' . $siteUrl . '/feedback.php</a>';
				}
				else{					
					if ($mParent->isSuperStaff($this->mTraveler->getTravelerID()) || $mParent->getAdministratorID() == $this->mTraveler->getTravelerID() || ($this->mGroup->isParent() && $mParent->isMember($this->mTraveler)))
						$mEmailLink = $siteUrl . '/group-privacy.php?groupID=' . $mParent->getGroupID();
					else
						$mEmailLink = $siteUrl . '/privacy.php';

					// if member of the parent group, the traveler will automatically be a staff of the subgroup
					if ($mParent->isMember($this->mTraveler)){
						$this->mBody = 'Hi ' . $mToName . ',<br /><br />' . $this->mEditedMessage . '<br /><br />' .
										'To visit ' . $this->mGroup->getName() . ' homepage, follow the link below:<br />' .
										'<a href = "' . $mLink . '">' . $mLink . '</a><br /><br />' .
										'Many thanks,<br /><br />' . $this->mFromName . '<br />' .
										'<a href = "' . $siteUrl . '">' . $siteUrl . '</a><br /><br /><br />' .

										'- - - - - -<br />You received this email because you are a member of ' . $mParent->getName() .
										' on ' . $mSiteName . '. To manage the emails you ' .
										'receive from ' . $mSiteName . ', please follow this link: <a href="' . $mEmailLink . '">' .
										$mEmailLink . '</a><br /><br />This e-mail was sent from an unattended mailbox. Please do not reply. ' .
										'Send any questions to <a href="' . $siteUrl . '/feedback.php">' . $siteUrl . '/feedback.php</a>';
					}
					// if non-member of the parent group
					else{
						$this->mBody = 'Hi ' . $mToName . ',<br /><br />' . $this->mEditedMessage . '<br /><br />' .
										'To confirm invitation as staff for the group ' . $this->mGroup->getName() .
										', follow the link below:<br />' .
										'<a href = "' . $siteUrl . '/membership.php">' . $siteUrl . '/membership.php</a><br /><br />' .
										'To visit ' . $this->mGroup->getName() . ' homepage, ' .
										'follow the link below:<br />' .
										'<a href = "' . $mLink . '">' . $mLink . '</a><br /><br />' .
										'Many thanks,<br /><br />' . $this->mFromName . '<br />' .
										'<a href = "' . $siteUrl . '">' . $siteUrl . '</a><br /><br /><br />' .

										'- - - - - -<br />You received this email because you have an account in ' . $mSiteName .
										'. To manage the emails you receive from ' . $mSiteName . ', please follow this link: <a href="' .
										$mEmailLink . '">' . $mEmailLink . '</a><br /><br />This e-mail was sent from an unattended mailbox. ' .
										'Please do not reply. Send any questions to <a href="' . $siteUrl . '/feedback.php">' .
										$siteUrl . '/feedback.php</a>';
					}
				}
		}

		function Send(){
			$mEmail = new EmailNotification;
			$mEmail->setTo($this->mToEmail);
			$mEmail->setFrom($this->mFromEmail);
			$mEmail->setFromName($this->mFromName);
			$mEmail->setSubject($this->mSubject);
			$mEmail->setMessage($this->mBody);
			$mEmail->Send();
		}
	}
?>