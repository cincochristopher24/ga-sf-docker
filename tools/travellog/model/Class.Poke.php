<?php
	/**
	 * Created on 07 August 2007
	 * Class.Poke.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'Class.Recordset.php';
	require_once 'Class.Connection.php';
	require_once 'travellog/model/Class.PokeType.php';

	class Poke{
		private $rs				= NULL;
		private $conn			= NULL;

		protected $pokeID 		= 0;
		protected $pokeType 	= 0;
		protected $message		= '';
		protected $senderID 	= 0;
		protected $recipientID 	= 0;
		protected $dateCreated 	= '';		
		
		function Poke($_pokeID = 0){
			try{
				$this-> conn = new Connection();
				$this-> rs = new Recordset($this->conn);
			}
			catch(exception $e){
				throw $e;
			}

			$this-> pokeID = $_pokeID;
			if (0 < $_pokeID){
				$sql = "select * from tblPoke where pokeID = " . $_pokeID;
				$this-> rs-> Execute($sql);
				
				if (0 == $this->rs->Recordcount())
					throw new exception("Poke ID is invalid!");

				$this-> pokeType = $this-> rs-> Result(0, "pokeType");
				$this-> senderID = $this-> rs-> Result(0, "senderID");
				$this-> recipientID = $this-> rs-> Result(0, "recipientID");
				$this-> dateCreated = $this-> rs-> Result(0, "dateCreated");
				$this-> section = $this-> rs-> Result(0, "section");
				$this-> genID = $this-> rs-> Result(0, "genID");
				$this-> setMessage();
			}
			return $this;
		}

		/**
		 * Setters
		 */
		function setPokeType($_pokeType = 0){
			$this-> pokeType = $_pokeType;
		}

		function setSenderID($_senderID = 0){
			$this-> senderID = $_senderID;
		}

		function setRecipientID($_recipientID = 0){
			$this-> recipientID = $_recipientID;
		}

		function setCreated($_dateCreated = ''){
			$this-> dateCreated = $_dateCreated;
		}

		function setSection($_section = 0){
			$this-> section = $_section;
		}

		function setGenID($_genID = 0){
			$this-> genID = $_genID;
		}

		function setMessage(){
			$Sender = SendableFactory::Instance();
			$Sender = $Sender-> Create($this->senderID);
			$link = "<a href = '".$Sender->getUsername()."'>".$Sender->getUsername()."</a>";

			switch($this->section){
				case 'profile':
					if ($this-> pokeType == PokeType::$SMILE)
						$this-> message = $link . ' smiled at you';
					else if ($this-> pokeType == PokeType::$THUMBSUP)
						$this-> message = $link .' gave you a thumbs up';
					else if ($this-> pokeType == PokeType::$CHEERSMATE)
						$this-> message = $link . ' says, "Cheers Mate!"';
					else if ($this-> pokeType == PokeType::$THAIWAI)
						$this-> message = $link . ' greeted you with a Thai wai';
					else if ($this-> pokeType == PokeType::$WAVE)
						$this-> message = $link . ' waved at you';
					else if ($this-> pokeType == PokeType::$BOWDOWN)
						$this-> message = $link . ' greeted you with a bow';
					else if ($this-> pokeType == PokeType::$CONGRATULATE)
						$this-> message = $link . ' congratulates you!';
					break;
				case 'travelerprofile':
					$Photo = new Photo('profile', $this->genID);
					$Context = $Photo-> getContext();
					$travelerID = $Context-> getTravelerID();
					$path = "photomanagement.php?action=vfullsize&cat=profile&genID=$travelerID&photoID=$this->genID";
					if ($this-> pokeType == PokeType::$SMILE)
						$this-> message = 'Your <a href="' . $path . '">photo</a> made '. $link . ' smile';
					else if ($this-> pokeType == PokeType::$THUMBSUP)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and gave it a thumbs up';
					else if ($this-> pokeType == PokeType::$CHEERSMATE)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and says, "Cheers, mate!"';
					else if ($this-> pokeType == PokeType::$THAIWAI)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and gave it a Thai Wai';
					else if ($this-> pokeType == PokeType::$WAVE)
						$this-> message = $link . ' waved at you in your <a href="' . $path . '">photo</a>';
					else if ($this-> pokeType == PokeType::$BOWDOWN)
						$this-> message = $link . ' bows down to your photographing skills in your <a href="' . $path . '">photo</a>';
					else if ($this-> pokeType == PokeType::$CONGRATULATE)
						$this-> message = $link . ' congratulates you on your awesome <a href="' . $path . '">photo</a>';
					break;
				case 'travellog':
					$Photo = new Photo('journal', $this->genID);
					$Context = $Photo-> getContext();
					$travellogID = $Context-> getTravelLogID();
					$path = "photomanagement.php?cat=travellog&action=vfullsize&genID=$travellogID&photoID=$this->genID";
					if ($this-> pokeType == PokeType::$SMILE)
						$this-> message = 'Your <a href="' . $path . '">photo</a> made ' . $link . ' smile';
					else if ($this-> pokeType == PokeType::$THUMBSUP)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and gave it a thumbs up';
					else if ($this-> pokeType == PokeType::$CHEERSMATE)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and says, "Cheers, mate!"';
					else if ($this-> pokeType == PokeType::$THAIWAI)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and gave it a Thai Wai';
					else if ($this-> pokeType == PokeType::$WAVE)
						$this-> message = $link . ' waved at you in your <a href="' . $path . '">photo</a>';
					else if ($this-> pokeType == PokeType::$BOWDOWN)
						$this-> message = $link . ' bows down to your photographing skills in your <a href="' . $path . '">photo</a>';
					else if ($this-> pokeType == PokeType::$CONGRATULATE)
						$this-> message = $link . ' congratulates you on your awesome <a href="' . $path . '">photo</a>';
					break;
				case 'fungroup':
					$Photo = new Photo('journal', $this->genID);
					$Context = $Photo-> getContext();
					$grpID = $Context-> getGroupID();
					$path = "photomanagement.php?cat=fungroup&action=vfullsize&genID=$grpID&photoID=$this->genID";
					if ($this-> pokeType == PokeType::$SMILE)
						$this-> message = 'Your <a href="' . $path . '">photo</a> made '. $link . ' smile';
					else if ($this-> pokeType == PokeType::$THUMBSUP)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and gave it a thumbs up';
					else if ($this-> pokeType == PokeType::$CHEERSMATE)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and says, "Cheers, mate!"';
					else if ($this-> pokeType == PokeType::$THAIWAI)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and gave it a Thai Wai';
					else if ($this-> pokeType == PokeType::$WAVE)
						$this-> message = $link . ' waved at you in your <a href="' . $path . '">photo</a>';
					else if ($this-> pokeType == PokeType::$BOWDOWN)
						$this-> message = $link . ' bows down to your photographing skills in your <a href="' . $path . '">photo</a>';
					else if ($this-> pokeType == PokeType::$CONGRATULATE)
						$this-> message = $link . ' congratulates you on your awesome <a href="' . $path . '">photo</a>';
					break;
				case 'photoalbum':
					$Photo = new Photo('journal', $this->genID);
					$Context = $Photo-> getContext();
					$grpID = $Context-> getGroupID();
					$albumID = $Context-> getPhotoAlbumID();
					$path = "photomanagement.php?cat=photoalbum&action=vfullsize&genID=$albumID&groupID=$grpID&photoID=$this->genID";
					if ($this-> pokeType == PokeType::$SMILE)
						$this-> message = 'Your <a href="' . $path . '">photo</a> made '. $link . ' smile';
					else if ($this-> pokeType == PokeType::$THUMBSUP)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and gave it a thumbs up';
					else if ($this-> pokeType == PokeType::$CHEERSMATE)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and says, "Cheers, mate!"';
					else if ($this-> pokeType == PokeType::$THAIWAI)
						$this-> message = $link . ' has viewed your <a href="' . $path . '">photo</a> and gave it a Thai Wai';
					else if ($this-> pokeType == PokeType::$WAVE)
						$this-> message = $link . ' waved at you in your <a href="' . $path . '">photo</a>';
					else if ($this-> pokeType == PokeType::$BOWDOWN)
						$this-> message = $link . ' bows down to your photographing skills in your <a href="' . $path . '">photo</a>';
					else if ($this-> pokeType == PokeType::$CONGRATULATE)
						$this-> message = $link . ' congratulates you on your awesome <a href="' . $path . '">photo</a>';
					break;
				case 'journal':
					$path = "journal-entry.php?action=view&travellogID=$this->genID";
					if ($this-> pokeType == PokeType::$SMILE)
						$this-> message = 'Your <a href="' . $path . '">journal</a> made '. $link . ' smile';
					else if ($this-> pokeType == PokeType::$THUMBSUP)
						$this-> message = $link . ' has read your <a href="' . $path . '">journal</a> and gave it a thumbs up';
					else if ($this-> pokeType == PokeType::$CHEERSMATE)
						$this-> message = $link . ' has read your <a href="' . $path . '">journal</a> and says, "Cheers, mate!"';
					else if ($this-> pokeType == PokeType::$THAIWAI)
						$this-> message = $link . ' has read your <a href="' . $path . '">journal</a> and gave it a Thai Wai';
					else if ($this-> pokeType == PokeType::$WAVE)
						$this-> message = $link . ' waved at you in your <a href="' . $path . '">journal</a>';
					else if ($this-> pokeType == PokeType::$BOWDOWN)
						$this-> message = $link . ' bows down to your <a href="' . $path . '">trip</a>';
					else if ($this-> pokeType == PokeType::$CONGRATULATE)
						$this-> message = $link . ' congratulates you on this <a href="' . $path . '">experience</a>';
					break; 
			}
		}

		function hasAdded(){
			$sql = "select pokeID from tblPoke where senderID = " . $this-> senderID . " and recipientID = " . $this-> recipientID . " and date(dateCreated) = curdate() and section = '" . $this-> section . "' and genID = " . $this-> genID;
			$this-> mRs-> Execute($sql);
			return $this->mRs->Recordcount();
		}

		/**
		 * Getters
		 */
		function getPokeID(){
			return $this-> pokeID;
		}

		function getPokeType(){
			return $this-> pokeType;
		}

		function getSenderID(){
			return $this-> senderID;
		}

		function getRecipientID(){
			return $this-> recipientID;
		}

		function getCreated(){
			return $this-> dateCreated;
		}

		function getSection(){
			return $this-> section;
		}

		function getGenID(){
			return $this-> genID;
		}

		function getMessage(){
			return $this-> message;
		}

		/**
		 * CRUDE
		 */
		function Save(){
			if (!Poke::isExist($this->pokeID)){				
				$sql = "insert into tblPoke (pokeType, senderID, recipientID, section, genID) values (" . $this-> pokeType . ", " . $this-> senderID . ", " . $this-> recipientID .", '" . $this-> section . "', " . $this-> genID . ")";
				$this-> rs-> Execute($sql);
			}
		}

		function Delete(){
			if (Poke::isExist($this->pokeID)){
				$sql = "delete from tblPoke where pokeID = " . $this-> pokeID;
				$this-> rs-> Execute($sql);

				$this-> pokeID = 0;
				$this-> pokeType = 0;
				$this-> senderID = 0;
				$this-> recipientID = 0;
				$this-> dateCreated = '';
				$this-> section = 0;
				$this-> genID = 0;
			}
		}

		/**
		 * Static
		 */
		static function isExist($_pokeID = 0){
			try{
				$conn = new Connection();
				$rs = new Recordset($conn);
			}
			catch(exception $e){
				throw $e;
			}

			if (0 < $_pokeID){
				$sql = "select * from tblPoke where pokeID = " . $_pokeID;
				$rs-> Execute($sql);
	
				if (0 < $rs->Recordcount())
					return true;
				else
					return false;
			}
			else
				return false;
		}
	}
?>