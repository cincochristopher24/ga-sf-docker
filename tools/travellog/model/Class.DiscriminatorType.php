<?php

/**
 * Created on October 19, 2006
 * Class.DiscriminatorType.php
 * @author Cheryl Ivy Q. Go
 */
 
	class DiscriminatorType{
	
		public static $ALERT = 1;
		public static $BULLETIN = 2;
		public static $PERSONAL = 3;
		public static $INQUIRY = 4;
	}
	
?>
