<?php
/*
 * Created on Aug 4, 2006
 *
 */
 
 class CountryInfo {
 	
 	private $description;
 	private $capital;
 	private $landarea;
 	private $population;
 	private $currency;
 	
 	function setCurrency($currency) {
 		$this->currency = $currency;
 	}	
 	
 	function getCurrency(){
 		return $this->currency;
 	}
 	
 	function setCountryDescription($description) {
 		$this->description = $description;
 	}	
 	
 	function getCountryDescription(){
 		return $this->description;
 	}
 	
 	function setCapital($capital) {
 		$this->capital = $capital;
 	}	
 	
 	function getCapital() {
 		return $this->capital;
 	}
 	
 	function setLandArea($landarea) {
 		$this->landarea = $landarea;
 	}	
 	
 	function getLandArea() {
 		return $this->landarea;
 	}		
 	
 	function setPopulation($population) {
 		$this->population = $population;
 	}
 
 	function getPopulation() {
 		return $this->population;
 	}
 
 }
 
 
?>
