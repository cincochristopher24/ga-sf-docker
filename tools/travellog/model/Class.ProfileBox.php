<?php
	require_once('travellog/model/Class.ProfileQuestion.php');
	require_once('travellog/model/Class.FBini.php');
	class ProfileBox
	{		
		//construct and FBML for the given user
		public static function generateCustomProfileFBML($fbUser,$showAll=false){
			//get the number of user that the user wants to display in his profile box
			$profileQuestions = ProfileQuestion::getQuestions();			
			if($showAll){
				$numQ = 'ALL';
			}
			else{	
				$numQ = ('ALL' == $fbUser->getNumOfQDisplayed() ? count($profileQuestions) : $fbUser->getNumOfQDisplayed() );
			}						
			$answers = $fbUser->getAnswers();
			if(0==count($answers) || $fbUser->getStatus() == 0){
				return self::generateDefaultProfileFBML($fbUser);
			}
$str = <<<STL
<style type="text/css">
#wrapper {
	width: 400px;		
	border: 1px solid black;
	font-family: "Lucida Grande", sans-serif;
	font-size: 11px;
}

#title {
	width: 100%;
	/*width: 246px;
	height: 34px;*/
}

#main {
	font-family: "Lucida Grande", sans-serif;
	font-size: 11px;
	margin-top: 20px;
	width: 500px;
	height: 300px;
}

ul {
	margin: 0;
	padding: 0;
	list-style-type: none;		
}

.question {
	
	font-family: "Lucida Grande", sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5c5c5c;	
}

.title {
	padding-top: 10px;
	padding-bottom: 10px;
}

.answer {
	margin-top: 4px;
	font-family: "Lucida Grande", Verdana, sans-serif;
	font-size: 11px;
	color: #4359a5;
}

.answer_icon {
	padding-right: 12px;
}

.viewmore {
	margin-top: 20px;
	font-weight: bold;
	color: #54632a;
}

.entry {
	width: 100%;
}
</style>	
STL;
						
			$str .= '<fb:if-is-own-profile>' .
			 			self::generateSubtitle() .
					'</fb:if-is-own-profile>';
			//get the answers for each question, display only the questions with answers
			$structAns = array();
			foreach($answers as $iAns){
				if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
					$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
				}
			}
			$ctr = 0;
			$qCtr = 0;
			$str .= '<div id="title"><img class="title" src="'.FBini::getLogoPath().'"/></div><div id="main"><ul>';
			foreach($profileQuestions as $iQuestion){
				$qCtr++;
				if(array_key_exists($iQuestion->getQuestionID(),$structAns)){	
					$str .= '<li class="entry"><h4 class="question">'.$iQuestion->getIntroduction().'</h4>';
					if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT || $iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA){
						$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
							? $structAns[$iQuestion->getQuestionID()][0]->getValue()
							: '';
						$str .='<p class="answer"><img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/>'. $val.'</p>';
					}
					elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX){
						$textVal = '';
						foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){
							if($iChoice->isTypeCheckbox() && 'Others' != $iChoice->getValue()){
								$str .='<p class="answer"><img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/>'. $iChoice->getValue().'</p>';									
							}
							elseif($iChoice->isTypeText()){
								$textVal = $iChoice->getValue(); 
							}
						}
						if(strlen($textVal)){
							$str .='<p class="answer"><img class="answer_icon" src="'.FBini::getAnswerIconPath().'"/>'. $textVal.'</p>';									
						}
					}
					$str .= '</li>';
					if($numQ == ++$ctr) break;
				}
			}
			$str .= '</ul>';
			if($numQ != 'ALL'){
				$str .= '<fb:if-is-own-profile>'.													
							'<a class="viewmore" href="'.FBini::getFBRootDirectory().'edit.php">View more</a>'.
							'<fb:else>' .
							'<a href="'.FBini::getFBRootDirectory().'view.php?uid='. $fbUser->getGenID() .'">View more</a>'.
							'</fb:else>' .
						'</fb:if-is-own-profile>';
			}
			$str .= '</div>';
			return $str;
		}
		
		public static function generateDefaultProfileFBML($fbUser){
			$str = 	'<div id="title"><img class="title" src="'.FBini::getLogoPath().'"/></div>'.
					'<fb:if-is-own-profile>' .
			 			self::generateSubtitle().
			 			'Your Travel Bio has no answers!' .
				 		'<fb:else>' .
				 			'<fb:name uid="'.$fbUser->getGenID().'" firstnameonly="true" possessive="true" /> Travel Bio has no answers.' .
				 		'</fb:else>' .
					'</fb:if-is-own-profile>';
			return $str;
		}
		
		public static function generateSubtitle(){
			return '<fb:subtitle>' .
 				'<fb:action href="'.FBini::getFBRootDirectory().'edit.php" title="Edit your Travel Bio Answers">Edit Travel Bio Answers</fb:action>' .
 				'<fb:action href="'.FBini::getFBRootDirectory().'friends.php" title="Invite your friends to have a Travel Bio">Invite Friends</fb:action>' .
 				'<fb:action href="'.FBini::getFBRootDirectory().'settings.php" title="Edit your Travel Bio Settings">Settings</fb:action>' .
 			'</fb:subtitle>';
		}
		
	}
?>