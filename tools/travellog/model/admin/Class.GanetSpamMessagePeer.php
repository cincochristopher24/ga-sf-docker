<?php

/**
 * Class for performing query and update operations.
 *
 * Fri Aug 14 18:09:00 2009
 */

require_once('travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php');
require_once('travellog/model/messageCenter/personal_message/Class.gaPersonalMessage.php');
require_once('travellog/model/Class.GanetDbHandler.php');
require_once('travellog/model/admin/Class.GanetSpamMessage.php');

class GanetSpamMessagePeer {	
	
	public static function doUpdate($messageID, $recipientIDs) {
		$handler = GanetDbHandler::getDbHandler();
		
		$sql = "UPDATE tblPersonalMessageRecipients SET isSpam = 0 WHERE messageID = " . $messageID .
				" AND recipientID IN (" . implode(",", $recipientIDs) ." )";
		
		$handler->execute($sql);
	}
	
	public static function retrieveByPk($id) {
		$handler = GanetDbHandler::getDbHandler();
		
		$sql = "SELECT " .
					"tblPersonalMessage.id, " .
					"tblPersonalMessage.dateCreated, " .
					"tblPersonalMessage.senderID, " .
					"tblPersonalMessage.subject, " .
					"tblPersonalMessage.body, " .
					"tblPersonalMessage.isMessageIncludedInNotif, " .
					"tblPersonalMessageRecipients.recipientID  " .
					"FROM " .
					"tblPersonalMessage, tblPersonalMessageRecipients " .
					"WHERE tblPersonalMessage.id = tblPersonalMessageRecipients.messageID " .
					"AND tblPersonalMessageRecipients.isSpam = 1 " .
					"AND tblPersonalMessage.isDeleted = 0 " .
					"AND tblPersonalMessageRecipients.isDeleted = 0 " .
					"AND tblPersonalMessage.id = " . $id;
		
		$resource = $handler->execute($sql);
		
		$message = null;
		$ctr = 0;
		while ($result = mysql_fetch_assoc($resource)) {
			if(0 == $ctr) {
				$message = new GanetSpamMessage();
				$message->initialize($result);
			}
			else {
				if($message instanceof GanetSpamMessage)
					$message->addRecipientID($result['recipientID']);
			}
			$ctr++;
		}
		return $message;
	}
}