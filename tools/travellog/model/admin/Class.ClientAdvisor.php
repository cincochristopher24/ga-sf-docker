<?php

	/**
	*	Class.ClientAdvisor.php
	*	class for advisor groups linked to .com client accounts
	*/
	
	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
	
	Class ClientAdvisor{
		
		// db var
		private $nConn;
		private $nRecordset;
			
		// client information
		private $clientID = 0;
		private $groupID = '';
	
		/**
		* constructor
		*/
			public function __construct($groupID){
			
				try{
					$this->nConn = new Connection();
					$this->nRecordset = new Recordset($this->nConn);
				}catch(Exception $e){
					die('Error in creating connection - ' . $e);
				}
				
				$this->setGroupID($groupID);
				
				$sqlquery = "SELECT * 
				 			FROM tblGrouptoAdvisor
							WHERE groupID = $groupID";
				$myquery = $this->nRecordset->Execute($sqlquery);
				if( $row = mysql_fetch_array($myquery) ){
					$this->setClientID($row['clientID']);
				}
				
				return $this;
			}
		
		/**
		*	setters
		*/		
		
		function setGroupID($groupID){
			$this->groupID = $groupID;
		}
		
		function setClientID($clientID){
			$this->clientID = $clientID;
		}
		
		function setInstName($instname){
			$this->instname = $instname;
		}
		
	
			
		/**
		*	getters
		*/	
		
		function getGroupID(){
			return $this->groupID;
		}
		
		function getClientID(){
			return $this->clientID;
		}
		
		function getInstName(){
			return $this->instname;
		}
		
	
		public function assignClienttoGroup(){
			
			try {
	 			$mConn	= new Connection('Travel_Logs');
				$mRs	= new Recordset($mConn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}

			$sql = "UPDATE tblGrouptoAdvisor
			 		SET clientID = $this->clientID
					WHERE groupID = $this->groupID" ;
	 		$mRs->Execute($sql);

		}
		
		public static function getAllClientAdvisors(){
			
			try {
	 			$mConn	= new Connection('Travel_Logs');
				$mRs	= new Recordset($mConn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}

			$sql = "SELECT *
					FROM tblGrouptoAdvisor, GoAbroad_Main.tbclient
					WHERE tblGrouptoAdvisor.clientID = GoAbroad_Main.tbclient.clientID
					AND tblGrouptoAdvisor.clientID > 0" ;
	 		$mRs->Execute($sql);
			
			$arr = array();
			if (0 < $mRs->Recordcount()){
		 		while ($mRecordset = mysql_fetch_assoc($mRs->Resultset())) {
					$arr[$mRecordset['groupID']] = array($mRecordset['clientID'] => $mRecordset['instname']);
 				}
			}
			return $arr;
		}
		
		public static function getAllActiveClients(){
			
			try {
	 			$mConn	= new Connection('GoAbroad_Main');
				$mRs	= new Recordset($mConn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}

			$sql = "SELECT tbclient.clientID, tbclient.instname 
					FROM tbclient
					WHERE tbclient.accesslevelID = 1 order by instname" ;
	 		$mRs->Execute($sql);
			
			$arr = array();
			if (0 < $mRs->Recordcount()){
		 		while ($mRecordset = mysql_fetch_assoc($mRs->Resultset())) {
					$clientID = $mRecordset['clientID'];
	 				$arr[$mRecordset['clientID']] = $mRecordset['instname'] ;
 				}
			}
			return $arr;
		}		
		
		public static function getClientByInstName($instname=''){
			
			try {
	 			$mConn	= new Connection('GoAbroad_Main');
				$mRs	= new Recordset($mConn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}

			$sql = "SELECT tbclient.clientID, tbclient.instname 
					FROM tbclient
					WHERE tbclient.instname LIKE '%$instname%' order by instname ";
	 		$mRs->Execute($sql);
			
			$arr = array();
			if (0 < $mRs->Recordcount()){
		 		while ($mRecordset = mysql_fetch_assoc($mRs->Resultset())) {
					$clientID = $mRecordset['clientID'];
	 				$arr[$mRecordset['clientID']] = $mRecordset['instname'] ;
 				}
				return $arr;
			}
			
		}
		
	}

?>
