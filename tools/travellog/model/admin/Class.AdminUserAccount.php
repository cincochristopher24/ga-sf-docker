<?php
	require_once('Class.DataModel.php');
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');

	class AdminUserAccount extends DataModel
	{
		const ACCESS_SALES 	= 1;
		const ACCESS_GE		 	= 2;
		const ACCESS_MODERATOR 		= 3;
		const ACCESS_ADMIN 		= 4;
		
		private $mLoggerDomain = null;
		
		private static $userAccess = array(
			self::ACCESS_SALES => 'Sales',
			self::ACCESS_GE 	 => 'GE',
			self::ACCESS_MODERATOR 	 => 'Moderator',
			self::ACCESS_ADMIN 	 => 'Admin',
		);
		
		public function __construct($keyID=null,$data=null){
			parent::initialize('Travel_Logs','tblSiteAdmin');
			if(!is_null($keyID)){
				if(!$this->load($keyID)){
					throw new exception("Invalid Admin User Account: $keyID passed to object of type AdminUserAccount.");
				}
			}
			if(is_array($data)){
				parent::setFields($data);
			}
		}
		
	
		public function getLoggerDomain(){
			if(is_null($this->mLoggerDomain)){
				$this->mLoggerDomain = ClientDomain::getClientDomainByDomainName($this->getServerName());
			}
			return $this->mLoggerDomain;
		}
		
		public function getUserAccountID(){
			return $this->getSiteadminID();
		}
		
		public function getUserAccess(){
			return self::$userAccess[$this->getAccesslevel()];
		}
		
		static public function getUserAccessTypes(){
			return self::$userAccess;
		}
		
		public function save(){
			parent::save();
			$this->mLoggerDomain = null;
		}
		
		static public function getInstanceByUserAccountID($userID=0){
			$db = new dbhandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblSiteAdmin WHERE siteadminID = {$db->makeSqlSafeString($userID)}"));
			return ($rs->retrieveRecordCount() > 0 ? new AdminUserAccount(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getInstanceByUserName($userName=''){
			$db = new dbhandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblSiteAdmin WHERE username = {$db->makeSqlSafeString($userName)}"));
			return ($rs->retrieveRecordCount() > 0 ? new AdminUserAccount(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getInstances(){
			$db = new dbhandler();
			$rs = new iRecordset($db->execute("SELECT * FROM tblSiteAdmin ORDER BY username"));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new AdminUserAccount(null,$row);
			}
			return $ar;
		}
		
		static public function createBlankInstance(){
			return new AdminUserAccount();
		}
		
		static public function validateAdminView($userAccess, $allowedViwers=array()){
			$allowedViwers = array_merge($allowedViwers, array(AdminUserAccount::ACCESS_ADMIN));
			if (!in_array($userAccess,$allowedViwers))
				return false;
			else
				return true;
		}
		
	}
?>