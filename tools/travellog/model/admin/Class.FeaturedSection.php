<?php
	/*
	 * Created on Apr 25, 2007
	 *
	 * To change the template for this generated file go to
	 * Window - Preferences - PHPeclipse - PHP - Code Templates
	 */

    require_once 'Class.Connection.php';
	require_once 'Class.Recordset.php';
	require_once 'travellog/model/Class.TravelLog.php';
	require_once 'travellog/model/Class.PhotoAlbum.php';
	require_once 'travellog/model/Class.GroupFactory.php';
	require_once 'travellog/model/Class.TravelerProfile.php';
		
	class FeaturedSection {
		
		private $mConn      = NULL;
        private $mRs        = NULL;  
            
		private $mFeaturedSectionID = 0;
		private $mFeaturedSection 	= '';
		private $mCaption			= '';
		private $mQuantity			= 0;
		private $mIsSpecialFeature  = false;
		private $arrFeaturedItems 	= array();
		
		private $table		=	'';
		private $pk			=	'';
		
		public static $GROUP				=	1;
		public static $TRAVELER				=	2;
		public static $JOURNAL				=	3;
		public static $JOURNAL_ENTRY		=	4;
		public static $PHOTO				=	5;
		public static $MOSTTRAVELED			=	6;
		public static $CHRONICLER			=	7;
		public static $PHOTOGRAPHER			=	8;
		public static $EDITORSCHOICEJOURNAL = 	9;

		private static $arrTravelLog 		= array();
		
		function FeaturedSection ($_sectionID = 0){
			
			$this->mFeaturedSectionID = $_sectionID;
			
			try {
                $this->mConn = new Connection();
                $this->mRs   = new Recordset($this->mConn);
            }
            catch (Exception $e) {                 
               throw $e;
            }
            
            if (0 < $this->mFeaturedSectionID){
                
                $sql = "SELECT * FROM tblFeaturedSection WHERE featuredsectionID = '$this->mFeaturedSectionID' ";
                $this->mRs->Execute($sql);
            
                if (0 == $this->mRs->Recordcount()){
                    throw new Exception("Invalid FeaturedSectionID");        // ID not valid so throw exception
                }
                
                // Sets values to its class member attributes.   
                $this->mFeaturedSection  = stripslashes($this->mRs->Result(0,"featuredsection"));
                $this->mCaption  = stripslashes($this->mRs->Result(0,"caption"));
                $this->mQuantity  = $this->mRs->Result(0,"quantity");
                $this->mIsSpecialFeature = $this->mRs->Result(0,"special");
                
                $this->getRelatedTable();
                                            
            }
                
		}
	
		function setCaption($_caption = ''){
			$this->mCaption = $_caption;
		}
		
		function setQuantity($_quantity = 0){
			$this->mQuantity = $_quantity;
		}
		
		function Save(){
			
			$cap = addslashes($this->mCaption);
			
			if ($this->mFeaturedSectionID){
				$sql = "UPDATE tblFeaturedSection SET caption = '$cap' , quantity = $this->mQuantity WHERE featuredsectionID = " . $this->mFeaturedSectionID ;				 
				$this->mRs->Execute($sql);
			}
		}
		
		
		function getSectionID(){
			return $this->mFeaturedSectionID ;
		}
		
		function getSection(){
			return $this->mFeaturedSection ;
		}
		
		function getCaption(){
			return $this->mCaption ;
		}
		
		function getQuantity(){
			return $this->mQuantity ;
		}
	 	
	 	
	 	function getRelatedTable(){
	 		
	 		
	 		switch ($this->mFeaturedSectionID) {
					
				case FeaturedSection::$GROUP : 
					$this->table = 'tblGroup';
					$this->pk = 'groupID';
					break;
					
				case FeaturedSection::$TRAVELER : 
					$this->table = 'tblTraveler';
					$this->pk = 'travelerID';
					break;
					
				case FeaturedSection::$JOURNAL : 
					$this->table = 'tblTravel';
					$this->pk = 'travelID';
					break;
					
				case FeaturedSection::$JOURNAL_ENTRY : 
					$this->table = 'tblTravelLog';
					$this->pk = 'travellogID';
					break;
					
				case FeaturedSection::$MOSTTRAVELED : 
					$this->table = 'tblTraveler';
					$this->pk = 'travelerID';
					break;
					
				case FeaturedSection::$CHRONICLER : 
					$this->table = 'tblTraveler';
					$this->pk = 'travelerID';
					break;
					
				case FeaturedSection::$PHOTOGRAPHER : 
					$this->table = 'tblTraveler';
					$this->pk = 'travelerID';
					break;
					
				case FeaturedSection::$EDITORSCHOICEJOURNAL : 
					$this->table = 'tblTravel';
					$this->pk = 'travelID';
					break;
				
			}
			
				
	 	}
	 	
	 	function instantiateItem($_genID = 0){
			
			 $instance = null;
			
			 switch ($this->mFeaturedSectionID) {
					
				case FeaturedSection::$GROUP :
					try{
						$mGroup		= GroupFactory::instance()->create( array($_genID) );
						$instance	= $mGroup[0];
					}
					catch(exception $e){
						return $e;
					}
					break;
					
				case FeaturedSection::$TRAVELER :
					try{
						$instance = new Traveler($_genID);
					}
					catch(exception $e){
						return $e;
					}
					break;
					
				case FeaturedSection::$JOURNAL : 
					$instance = new Travel($_genID);
					break;
					
				case FeaturedSection::$JOURNAL_ENTRY :
					try{
						$instance = new Travellog($_genID);
					}
					catch(exception $e){
						return $e;
					}
					break;
					
				case FeaturedSection::$MOSTTRAVELED : 
					$instance = new Traveler($_genID);
					break;
					
				case FeaturedSection::$CHRONICLER : 
					$instance = new Traveler($_genID);
					break;
					
				case FeaturedSection::$PHOTOGRAPHER : 
					$instance = new Traveler($_genID);
					break;
					
				case FeaturedSection::$EDITORSCHOICEJOURNAL : 
					$instance = new Travel($_genID);
					break;
				
			}
				
				return $instance;
		}
		
		
		function isItemFeatured($_genID = 0){
			
			$sql = "SELECT genID FROM tblFeaturedItems WHERE sectionID = " . $this->mFeaturedSectionID . " AND genID = " . $_genID ;
			$this->mRs->Execute($sql);
			
			if ($this->mRs->RecordCount())
				return true;
			else
				return false;
				
		}
		
		
		function setItemFeatured($_genID = 0) {
			
			if (!$this->isItemFeatured($_genID)):
				$this->resetItemRank();
				
				$newitemrank = $this->getNewItemRank();
				$sql = "INSERT INTO tblFeaturedItems (sectionID, genID, rank) VALUES ( $this->mFeaturedSectionID , $_genID , $newitemrank) " ;
				$this->mRs->Execute($sql);
			endif;
		}
		
		function unsetItemFeatured($_genID = 0){
			
			$sql = "DELETE FROM tblFeaturedItems WHERE sectionID = " . $this->mFeaturedSectionID . " AND genID = " . $_genID ;
			$this->mRs->Execute($sql);
			
			$this->resetItemRank();
		}
		
		function getNewItemRank() {
			
			$sql = "SELECT rank FROM tblFeaturedItems WHERE sectionID = " . $this->mFeaturedSectionID . " ORDER BY rank DESC LIMIT 0 , 1" ;
			$this->mRs->Execute($sql);
			
			$newrank = 1;
			if ($this->mRs->RecordCount())
				$newrank = $this->mRs->Result(0,'rank') + 1;
				
			return $newrank;			
			
		}
		
		function resetItemRank() {
			
			$sql = "SELECT featuredItemID FROM tblFeaturedItems WHERE sectionID = " . $this->mFeaturedSectionID . " ORDER BY rank" ;
			$this->mRs->Execute($sql);
			$resultset = $this->mRs->Resultset();
			$cnt = 0;
			while ($recordset = mysql_fetch_array($resultset)) {
				$cnt ++ ;
				$sql = "UPDATE tblFeaturedItems SET rank = " . $cnt . " WHERE featuredItemID = " . $recordset['featuredItemID'] ;
				$this->mRs->Execute($sql);
			}	
			
		}
		
		function arrangeItemsRank($_arrFeatItems = array()) {
			
			$sql = "SELECT featuredItemID FROM tblFeaturedItems WHERE sectionID = " . $this->mFeaturedSectionID . " ORDER BY rank" ;
			$this->mRs->Execute($sql);
			$resultset = $this->mRs->Resultset();
			$cnt = 0;
			foreach ($_arrFeatItems as $each) :
				$cnt ++ ;
				$sql = "UPDATE tblFeaturedItems SET rank = " . $cnt . " WHERE featuredItemID = " . $each ;
				$this->mRs->Execute($sql);
			endforeach;	
			
		}
		
		function getFeaturedItems(){
			
			
			if ( FeaturedSection::$PHOTO == $this->mFeaturedSectionID)
				$sql = "SELECT genID, phototypeID FROM tblFeaturedItems, tblPhoto " .
						"WHERE tblFeaturedItems.genID = tblPhoto.photoID " .
						"AND tblFeaturedItems.sectionID = " . $this->mFeaturedSectionID ;
			else
				$sql = "SELECT genID FROM tblFeaturedItems, " . $this->table . 
						" WHERE " . $this->table . "." . $this->pk . " = " . "tblFeaturedItems.genID AND sectionID = " . $this->mFeaturedSectionID ;
						
			
			$sql = 	$sql . " ORDER BY rank LIMIT 0 , " . $this->mQuantity;
			$this->mRs->Execute($sql);
			
			$this->mRs2   = new Recordset($this->mConn);
			 			
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())){
				
				if ( FeaturedSection::$PHOTO == $this->mFeaturedSectionID){
					if (1 == $recordset['phototypeID']) : //if profile photo
						$sql = "SELECT travelerID FROM tblFeaturedItems, tblTravelertoPhoto WHERE tblTravelertoPhoto.photoID = " . $recordset['genID'] ;				
						$this->mRs2->Execute($sql);		
						$context = new TravelerProfile($this->mRs2->Result(0,'travelerID'));											
					elseif (2 == $recordset['phototypeID']) :	// if entry
						$sql = "SELECT travellogID FROM tblFeaturedItems, tblTravelLogtoPhoto WHERE tblTravelLogtoPhoto.photoID = " . $recordset['genID'] ;
						$this->mRs2->Execute($sql);
						try{			
							$context = new TravelLog($this->mRs2->Result(0,'travellogID'));
							if( !$context instanceof TravelLog ){
								echo "travellogID: ".$this->mRs2->Result(0,'travellogID');
								continue;
							}								
						}catch(exception $e){
							continue;
						}
					elseif (5 == $recordset['phototypeID']) :	// if group photo
						$sql = "SELECT groupID FROM tblFeaturedItems, tblGrouptoPhoto WHERE tblGrouptoPhoto.photoID = " . $recordset['genID'] ;
						$this->mRs2->Execute($sql);
						$mGroup		= GroupFactory::instance()->create( array($this->mRs2->Result(0,'groupID')) );
						$context	= $mGroup[0];
					elseif (7 == $recordset['phototypeID']) :	// if photoalbum photo
						$sql = "SELECT photoalbumID FROM tblFeaturedItems, tblPhotoAlbumtoPhoto WHERE tblPhotoAlbumtoPhoto.photoID = " . $recordset['genID'] ;
						$this->mRs2->Execute($sql);	
						$context = new PhotoAlbum($this->mRs2->Result(0,'photoalbumID'));		
					else :
						$sql = "SELECT travellogID FROM tblFeaturedItems, tblTravelLogtoPhoto WHERE tblTravelLogtoPhoto.photoID = " . $recordset['genID'] ;
						$this->mRs2->Execute($sql);			
						$context = new TravelLog($this->mRs2->Result(0,'travellogID'));							
					endif;					

					$this->arrFeaturedItems[] = new Photo($context, $recordset['genID']);
				}
				else
					$this->arrFeaturedItems[]	=	$this->instantiateItem($recordset['genID']);
					
			} 
			
			return $this->arrFeaturedItems;	
		}

		/**
		 * STATIC FUNCTIONS
		 */	 	
	 	 
		public static function getCaptionBySection($_section = ''){
	 		try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$caption = '';
			
			$sql = "SELECT caption " .
					"FROM tblFeaturedSection " .
					"WHERE featuredsection = . $_section " ;
			$rs->Execute($sql);
	 		
	 		if ($rs->Recordcount())
				$caption = $rs->Result(0,"caption");
               
			return $caption;
		}	
		
		public static function getSpecialSections(){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$arrSpecialSection	= array();
				
			// get the special sections
			$sql = "SELECT featuredsectionID " .
					"FROM tblFeaturedSection " .
					"WHERE special = 1";
					 
			$rs->Execute($sql);

			if ($rs->Recordcount() > 0){
				while ($recordset = mysql_fetch_array($rs->Resultset())) {
					try {
		 				$arrSpecialSection[] = new FeaturedSection($recordset['featuredsectionID']);
		 			}
					catch (Exception $e) {				   
					   throw $e;
					} 	
				}
			}
						
			return $arrSpecialSection;
		}
		
		
		public static function getKeyBySectionGenID($_sectionID = 0 , $_genID = 0){
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			$featuredItemID	= 0;
				
			// get the special sections
			$sql = "SELECT featuredItemID " .
					"FROM tblFeaturedItems " .
					"WHERE sectionID = " . $_sectionID .
					" AND genID = " . $_genID;
					 
			$rs->Execute($sql);

			if ($rs->Recordcount() > 0)
				$featuredItemID = $rs->Result(0,'featuredItemID');
						
			return $featuredItemID;
		}

		/*** ##### START:: Added By: Cheryl Ivy Q. Go	1 October 2008 ##### ***/		
		public static function getUniqueDates($_filterCriteria2 = null){
			require_once('gaLogs/Reader/Class.Reader.php');
			$mReader		= new Reader();
			$mQueryStr		= '';
			$idx			= 0;
			$mDates			= array();

			try{
				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			if ($_filterCriteria2){
				$mConditions = $_filterCriteria2->getConditions();
				$mBooleanOps = $_filterCriteria2->getBooleanOps();

				foreach ($mConditions as $eachCondition){
					$mBooleanOp = $mBooleanOps[$idx];
					$mOperation = $eachCondition->getOperation();
					$mAttribute = $eachCondition->getAttributeName();
					$mValue = $eachCondition->getValue();

					switch ($mOperation){
		 				case FilterOp::$GREATER_THAN_OR_EQUAL:
		 					$mQueryStr .= $mBooleanOp . " " . $mAttribute . " >= '" . $mValue . "' ";
							break;
						case FilterOp::$LESS_THAN_OR_EQUAL:
							$mQueryStr .= $mBooleanOp . " " . $mAttribute . " <= '" . $mValue. "' ";
		 					break;
						default:
							$mQueryStr .= $mBooleanOp . " " . $mAttribute . " = '" . $mValue. "' ";
		 					break;
		 			}
					$idx +=1;
				}
			}

			$sql = 	"SELECT DISTINCT DATE_FORMAT(_EXECUTION_DATE_, '%Y-%m-%d') AS dateFeatured " .
					"FROM Travel_Logs.tblFeaturedItems " .
					"WHERE _COMMAND_ = 'INSERT' " . $mQueryStr .
					"ORDER BY dateFeatured DESC";
			$mRs = $mReader->getLogsBySql($sql);

			if(0 < $mRs->retrieveRecordCount()){
				while(!$mRs->EOF()){
					$mDates[] = $mRs->retrieveRow();
					$mRs->moveNext();
				}
			}

			return $mDates;
		}
		public static function getFeaturedOnDate($_sectionId = 0, $_date = ''){
			require_once('gaLogs/Reader/Class.Reader.php');
			$mReader		= new Reader();
			$mQueryLimit	= '';
			$mQueryString1	= '';
			$mQueryString2	= '';

			try{
				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
				$mRs2 = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			if (0 < $_sectionId){
				$mQueryString1 = "AND _NEW_sectionID = " . $_sectionId . " ";
				$mQueryString2 = "AND _OLD_sectionID = " . $_sectionId . " ";
			}

			$sql =	"SELECT DISTINCT CAST(_NEW_sectionID AS SIGNED) AS sectionID, CAST( _NEW_rank AS SIGNED ) as rank, _NEW_genID AS genID " .
					"FROM Travel_Logs.tblFeaturedItems " .
					"WHERE _COMMAND_ = 'INSERT' " .
					"AND _NEW_genID > 0 " . $mQueryString1 .
					"AND _EXECUTION_DATE_ >= '" . $_date . " 00:00:00' " .
					"AND _EXECUTION_DATE_ <= '" . $_date . " 24:00:00' " .
					"AND _NEW_genID NOT IN " .
					"(" .
						"SELECT _OLD_genID " .
						"FROM Travel_Logs.tblFeaturedItems " .
						"WHERE _COMMAND_ = 'DELETE' " . $mQueryString2 .
						"AND _EXECUTION_DATE_ >= '" . $_date . " 00:00:00' " .
						"AND _EXECUTION_DATE_ <= '" . $_date . " 24:00:00' " .
					")" .
					"GROUP BY _NEW_genID " .
					"ORDER BY sectionID ASC, rank ASC";
			$mRs = $mReader->getLogsBySql($sql);

			$mFeatured = array();
			if(0 < $mRs->retrieveRecordCount()){
				// check if all sections has a record
				while(!$mRs->EOF()){
					$mFeatured[] = $mRs->retrieveRow();
					$mRs->moveNext();
				}
				$arrSection = array();
				foreach($mFeatured as $eachFeature){
					if (!in_array($eachFeature['sectionID'], $arrSection))
						$arrSection[] = $eachFeature['sectionID'];
				}
				if ((4 > count($arrSection) && 0 == $_sectionId) || (3 > count($mFeatured) && 0 < $_sectionId)){
					// get featured on date logged before the date in the query
					$mOldDate = $_date;
					$mDate = FeaturedSection::getBeforeDate($_sectionId, $mOldDate);
					if (strlen($mDate)){
						$mFeatured = array();
						$mFeatured = FeaturedSection::getFeaturedOnDate2($_sectionId, $mOldDate, $mDate);

						$arrSection = array();
						foreach($mFeatured as $eachFeature){
							if (!in_array($eachFeature['sectionID'], $arrSection))
								$arrSection[] = $eachFeature['sectionID'];
						}
						if ((4 > count($arrSection) && 0 == $_sectionId) || (3 > count($mFeatured) && 0 < $_sectionId)){
							// get featured on date logged before the date in the query
							$mDate = FeaturedSection::getBeforeDate($_sectionId, $mDate);
							if (strlen($mDate)){
								$mFeatured = array();
								$mFeatured = FeaturedSection::getFeaturedOnDate2($_sectionId, $_date, $mDate);
							}
						}
					}
				}
			}
			else{
				// get featured on date logged before the date in the query
				$mDate = FeaturedSection::getBeforeDate($_sectionId, $_date);
				if (strlen($mDate))
					$mFeatured = FeaturedSection::getFeaturedOnDate2($_sectionId, $_date, $mDate);

				$arrSection = array();
				foreach($mFeatured as $eachFeature){
					if (!in_array($eachFeature['sectionID'], $arrSection))
						$arrSection[] = $eachFeature['sectionID'];
				}
				if ((4 > count($arrSection) && 0 == $_sectionId) || (3 > count($mFeatured) && 0 < $_sectionId)){
					// get featured on date logged before the date in the query
					$mDate = FeaturedSection::getBeforeDate($_sectionId, $mDate);
					if (strlen($mDate)){
						$mFeatured = array();
						$mFeatured = FeaturedSection::getFeaturedOnDate2($_sectionId, $_date, $mDate);

						$arrSection = array();
						foreach($mFeatured as $eachFeature){
							if (!in_array($eachFeature['sectionID'], $arrSection))
								$arrSection[] = $eachFeature['sectionID'];
						}
						if ((4 > count($arrSection) && 0 == $_sectionId) || (3 > count($mFeatured) && 0 < $_sectionId)){
							// get featured on date logged before the date in the query
							$mDate = FeaturedSection::getBeforeDate($_sectionId, $mDate);
							if (strlen($mDate)){
								$mFeatured = array();
								$mFeatured = FeaturedSection::getFeaturedOnDate2($_sectionId, $_date, $mDate);
							}
						}
					}
				}
			}	
			return $mFeatured;
		}
		public static function getBeforeDate($_sectionId = 0, $_date = ''){
			require_once('gaLogs/Reader/Class.Reader.php');
			$mReader		= new Reader();
			$mQueryString	= '';

			try{
				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
				$mRs2 = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			if (0 < $_sectionId)
				$mQueryString = "AND _NEW_sectionID = " . $_sectionId . " ";

			$sql =	"SELECT DATE_FORMAT(_EXECUTION_DATE_, '%Y-%m-%d') as dateFeatured " .
					"FROM Travel_Logs.tblFeaturedItems " .
					"WHERE _COMMAND_ = 'INSERT' " .
					"AND _NEW_genID > 0 " . $mQueryString .
					"AND _EXECUTION_DATE_ < '" . $_date . " 00:00:00' " .
					"ORDER BY _EXECUTION_DATE_ DESC " .
					"LIMIT 1";
			$mRs = $mReader->getLogsBySql($sql);

			if (0 < $mRs->retrieveRecordCount()){
				$mRecord = $mRs->retrieveRow();
				return $mRecord['dateFeatured'];
			}
		}
		public static function getFeaturedOnDate2($_sectionId = 0, $_oldDate = '', $_newDate = ''){
			require_once('gaLogs/Reader/Class.Reader.php');
			$mReader		= new Reader();
			$mQueryLimit	= '';
			$mQueryString1	= '';
			$mQueryString2	= '';

			try{
				$mConn = new Connection();
				$mRs  = new Recordset($mConn);
				$mRs2 = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			if (0 < $_sectionId){
				$mQueryString1 = "AND _NEW_sectionID = " . $_sectionId . " ";
				$mQueryString2 = "AND _OLD_sectionID = " . $_sectionId . " ";
			}

			$sql =	"SELECT DISTINCT CAST(_NEW_sectionID AS SIGNED) AS sectionID, CAST( _NEW_rank AS SIGNED ) as rank, _NEW_genID AS genID " .
					"FROM Travel_Logs.tblFeaturedItems " .
					"WHERE _COMMAND_ = 'INSERT' " .
					"AND _NEW_genID > 0 " . $mQueryString1 .
					"AND _EXECUTION_DATE_ >= '" . $_newDate . " 00:00:00' " .
					"AND _EXECUTION_DATE_ <= '" . $_oldDate . " 24:00:00' " .
					"AND _NEW_genID NOT IN " .
					"(" .
						"SELECT _OLD_genID " .
						"FROM Travel_Logs.tblFeaturedItems " .
						"WHERE _COMMAND_ = 'DELETE' " . $mQueryString2 .
						"AND _EXECUTION_DATE_ >= '" . $_newDate . " 00:00:00' " .
						"AND _EXECUTION_DATE_ <= '" . $_oldDate . " 24:00:00' " .
					")" .
					"GROUP BY _NEW_genID " .
					"ORDER BY sectionID ASC, rank ASC";
			$mRs = $mReader->getLogsBySql($sql);

			$mFeatured = array();
			if(0 < $mRs->retrieveRecordCount()){
				while(!$mRs->EOF()){
					$mFeatured[] = $mRs->retrieveRow();
					$mRs->moveNext();
				}
			}

			return $mFeatured;
		}
		/*** ##### END:: Added By: Cheryl Ivy Q. Go	1 October 2008 ##### ***/

		public static function instantiatePhotoFeature($_genID = 0){
			try{
				$mConn	= new Connection();
				$mRs	= new Recordset($mConn);
				$mRs2	= new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "SELECT DISTINCT phototypeID " .
					"FROM tblPhoto where photoID = " . $_genID;
			$mRs->Execute($sql);
			
			if (0 == $mRs->Recordcount())
				throw new exception ("Invalid photoID: ".$_genID);

			$mContext = null;
			if (0 < $mRs->Recordcount()){
				$mType = $mRs->Result(0, "phototypeID");

				switch($mType){                	
                	case PhotoType::$PROFILE:
                		$sql2 = "SELECT travelerID FROM tblTravelertoPhoto WHERE photoID = " . $_genID;
			            $mRs2->Execute($sql2);

			            if (0 < $mRs2->Recordcount())
		                    $mContext = new TravelerProfile($mRs2->Result(0, "travelerID"));
						else
							$mContext = new TravelerProfile();
                		break;

                	case PhotoType::$TRAVELLOG:
                		$sql2 = "SELECT travellogID FROM tblTravelLogtoPhoto WHERE photoID = " . $_genID;
				        $mRs2->Execute($sql2);

				        if (0 < $mRs2->Recordcount())
		                    $mContext = new TravelLog($mRs2->Result(0, "travellogID"));
						else
							$mContext = new TravelLog();
                		break;

                	case PhotoType::$GROUP:
                		$sql2 = "SELECT groupID FROM tblGroup WHERE photoID = " . $_genID;
                		$mRs2->Execute($sql2);

				        if (0 < $mRs2->Recordcount())
		                    $mContext = new FunGroup($mRs2->Result(0, "groupID"));
						else
							$mContext = new FunGroup();
			        	break;

                	case PhotoType::$FUNGROUP:
                		$sql2 = "SELECT groupID FROM tblGrouptoPhoto WHERE photoID = " . $_genID;
                		$mRs2->Execute($sql2);

				        if (0 < $mRs2->Recordcount())
		                    $mContext = new FunGroup($mRs2->Result(0, "groupID"));
						else
							$mContext = new FunGroup();
						break;

                	case PhotoType::$PHOTOALBUM:
                		$sql2 = "SELECT photoalbumID FROM tblPhotoAlbumtoPhoto WHERE photoID = " . $_genID;
	                	$mRs2->Execute($sql2);

				        if (0 < $mRs2->Recordcount())
		                    $mContext = new PhotoAlbum($mRs2->Result(0, "photoalbumID"));
						else
							$mContext = new PhotoAlbum();
                		break;

                	case PhotoType::$PROGRAM:
                		$sql2 = "SELECT programID FROM tblProgramtoPhoto WHERE photoID = " . $_genID;
		                $mRs2->Execute($sql2);

				        if (0 < $mRs2->Recordcount())
		                    $mContext = new Program($mRs2->Result(0, "programID"));
						else
							$mContext = new Program();
                		break;
                }
			}
			return new Photo($mContext, $_genID);
		}
	}
?>