<?php
/***
 * Created on Nov 3, 2008
 * @author
 * Purpose: 
 */

	require_once('Class.DataModel.php');
	require_once("travellog/model/Class.SessionManager.php");
	require_once('travellog/model2/photo/Class.ganetRotatingPhotoRepo.php');
	
	class RotatingPhoto extends DataModel
	{		
		const PHOTO_NAME 			= "photo_";
		const TRAVELER_TYPE_ID 		= 0;
		const GROUP_TYPE_ID 		= 1;
		const PHOTO_ACTIVE_STATUS	= 0;
		const PHOTO_INACTIVE_STATUS	= 1;
		
		protected static $instance	= NULL;
		public $obj_session  		= NULL;
		private $mCurrent_photoID 	= NULL;
		private $mUser              = NULL;
				
		public function __construct($param=null,$data=null) {
			parent::initialize('Travel_Logs','tblRotatingPhoto');
			if(!is_null($param)){
				if(!$this->load($param)){
					throw new exception('Invalid photoID '.$param.' passed to object of type RotatingPhoto.');
				}
			}
			if(is_array($data)){
				parent::setFields($data);
			}
		}
		
		public static function instance(){
			if (self::$instance == null){
				self::$instance	=	new RotatingPhoto();
			}
			return self::$instance;
		}
		
		public function getCurrentPhotoID() {
			return $this->mCurrent_photoID;
		}
		
		public function setCurrentPhotoID($cur_photoID) {
			$this->mCurrent_photoID = $cur_photoID;
		}
		
		/**
		 *	Purpose: Generates a name for photo using the photoID 
		 */
		public function getPhotoName() {
			$filename = $this->getNewFilename(); 
			if(empty($filename)) {
				$filename = self::PHOTO_NAME.$this->getPhotoID();
				$ext = explode(".",$this->getFilename());
				$filename .= ".".$ext[count($ext)-1];
			} 
			return $filename;
		}
		
		/**
		 *	Purpose: Save the new filename of image to newFilename in `Travel_Logs`.`tblRotatingPhoto` 
		 */
		public function savePhotoName() {
			if(($this->getNewFilename()==null)) {
				$this->setNewFilename($this->getPhotoName());
				$this->save();
			}
		}
		
		/**
		 *	Purpose: Get the url to the userID
		 */
		public function getLink() {
			$typeID = $this->getTypeID();
			$link = "#";
			if ($this->getFrom()) {
				$link = "http://".$_SERVER['SERVER_NAME'];
				if ($typeID == self::TRAVELER_TYPE_ID) 
					$link .= "/".$this->getFrom();
				else if ($typeID == self::GROUP_TYPE_ID) {
					require_once('Class.Group.php');
					$group = GroupFactory::instance()->create(array($this->getUserID()));
					$link .= $group[0]->getFriendlyUrl();
				}
			}
			return $link;
		}
		
		public function getPhotoLink() {
			return "http://".$_SERVER["SERVER_NAME"]."/images/rotating/".$this->getPhotoName();
		}
		
		/**
		 *	Purpose:  Get the user of the photo
		 */
		public function getUser() {
			if (is_null($this->mUser)) {
				$userID = $this->getUserID();
				if (!empty($userID)) {
					$typeID = $this->getTypeID();
					if ($typeID == self::TRAVELER_TYPE_ID) {
						require_once('Class.Traveler.php');
						try {
							$this->mUser = new Traveler($userID);
						}
						catch(exception $ex) {}
					}
					else if ($typeID == self::GROUP_TYPE_ID) {
						require_once('Class.Group.php');
						try {
							$group = GroupFactory::instance()->create(array($userID));
							$this->mUser = $group[0];
						}
						catch(exception $ex) {}
					}
				}
			}
			return $this->mUser;
		}
		
		/**
		 *	Purpose:  Get the name of userID
		 */
		public function getFrom() {
			$name = "";
			if ($this->getUser())
				$name = $this->getUser()->getName();
			return $name; 
		}
		
		public function isSuspendedUser() {
			if ($this->getUser())
				if($this->getUser()->isSuspended())
					return true;
			return false;
		}
		
		public function isExistingUser() {
			if (is_null($this->getUser()))
				return false;
			return true;
		}
		
		public function isAllUserSuspended($status=self::PHOTO_ACTIVE_STATUS) {
			$this->obj_session = SessionManager::getInstance();
			$sql = "SELECT DISTINCT `tblRotatingPhoto`.`photoID`";
			$sql1 = "FROM tblRotatingPhoto";
			$sql2 = "WHERE `tblRotatingPhoto`.`status` = $status";
			
			$trav_sql .= $sql. ",`tblTraveler`.`isSuspended` " . $sql1 . ",tblTraveler " . $sql2 . " AND `tblTraveler`.`isSuspended` = 0";
			$grp_sql .= ",`tblGroup`.`isSuspended` " . $sql1 . ",tblGroup " . $sql2 . " AND `tblGroup`.`isSuspended` = 0";
			
			if ($this->obj_session->get('rotatingPhotoID')) {
				$trav_sql .= " AND `tblRotatingPhoto`.`photoID` != ".$this->obj_session->get('rotatingPhotoID');
				$grp_sql .= " AND `tblRotatingPhoto`.`photoID` != ".$this->obj_session->get('rotatingPhotoID');
			}
			
			$trav_rs = new iRecordset($this->mDb->execQuery($trav_sql));
			$grp_rs = new iRecordset($this->mDb->execQuery($grp_sql));
			
			if( (0 < $trav_rs->retrieveRecordCount()) || (0 < $grp_rs->retrieveRecordCount()) ) {
				return false;
			}
			else {
				return true;
			}
		}
		
		/**
		 *	Purpose: Get a RotatingPhoto Object randomly  
		 *  deprecated
		 */
		public function getRandomPhoto($status=self::PHOTO_ACTIVE_STATUS) {
			$this->obj_session = SessionManager::getInstance();
			
			$sql = "SELECT (photoID) FROM tblRotatingPhoto WHERE status = $status";
			
			$rs = new iRecordset($this->mDb->execQuery($sql));
			
			if	(1 < $rs->retrieveRecordCount()) {
				
				if ($this->obj_session->get('rotatingPhotoID')) {
					$sql .= " AND photoID != ".$this->obj_session->get('rotatingPhotoID');
					$rs = new iRecordset($this->mDb->execQuery($sql));
				}
				$num = mt_rand(0,($rs->retrieveRecordCount()-1));
				$sql .= " LIMIT $num,1";
				$rs = new iRecordset($this->mDb->execQuery($sql));	
			}
			
			if (!$rs->retrieveRecordCount()) return false;
			
			$photo = $rs->current();
			
			$photo_obj = new RotatingPhoto($photo["photoID"]);
			
			if (!($photo_obj->isExistingUser())) {
				if (1 == $rs->retrieveRecordCount())
					return false;
				else if (2 == $rs->retrieveRecordCount()){
					$this->obj_session->unsetVar('rotatingPhotoID');
					return $this->getRandomPhoto();
				}
				else
					return $this->getRandomPhoto();
			}
			else if ($photo_obj->isSuspendedUser()) {
				if (1 == $rs->retrieveRecordCount())
					return false;
				else if (2 == $rs->retrieveRecordCount()){
					$this->obj_session->unsetVar('rotatingPhotoID');
					return $this->getRandomPhoto();
				}
				else if($photo_obj->isAllUserSuspended())
					return false;
				else
					return $this->getRandomPhoto();
			}
			else {
				$this->setCurrentPhotoID($photo["photoID"]);
				$this->obj_session->unsetVar('rotatingPhotoID');
				$this->obj_session->set('rotatingPhotoID',$this->getCurrentPhotoID());
				return $photo_obj;
			}
		}
		
		/**
		 *	Purpose: Get a RotatingPhoto Object randomly  
		 *  uses cache
		 */
		public function getRandomRotatingPhoto($status=self::PHOTO_ACTIVE_STATUS) {
			$this->obj_session = SessionManager::getInstance();
			
			$photoRepo = ganetRotatingPhotoRepo::instance();
			$vo = new ganetValueObject();
			if ($this->obj_session->get('rotatingPhotoID')) {
				$vo->set('id', $this->obj_session->get('rotatingPhotoID'));	
			}
			
			if($photo_obj = $photoRepo->retrieve($vo)) {
				$this->setCurrentPhotoID($photo_obj->getPhotoID());
				$this->obj_session->unsetVar('rotatingPhotoID');
				$this->obj_session->set('rotatingPhotoID',$this->getCurrentPhotoID());
				
				return $photo_obj;
			}
			return false;
		}
		
		public function invalidateCacheEntry() {
			$this->cache = ganetCacheProvider::instance()->getCache();
			if ($this->cache != null)
				$this->cache->delete('Rotating_Photo_Array');
		}
		
		/**
		 *	Purpose: Get all photos in `Travel_Logs`.`tblRotatingPhoto`
		 */
		public static function getAllPhotos($status=null) {
			$sql = "SELECT `photoID`,`filename`,`newFilename`,`caption`,`status`,`userID`,`typeID` FROM tblRotatingPhoto";
			if (!is_null($status))
				$sql .= " WHERE status = $status";
			$photos = array();
			$mDb = new dbHandler();;
			$rs = new iRecordset($mDb->execQuery($sql));
			if(0 < $rs->retrieveRecordCount()){
				while(!$rs->EOF()) {
					$record = $rs->current();
					$photo = new RotatingPhoto($record["photoID"]);
					$photos[] = $photo;
					$rs->next();
				}
			}
			return $photos;
		}
		
		public function getPhotos($paging=array(),$orderBy=array(),$filterBy=array()) {
			$sql = "SELECT `photoID`,`filename`,`newFilename`,`caption`,`dateUploaded`,`status`,`userID`,`typeID` FROM tblRotatingPhoto";
			if ($filterBy["filter"] !== "all") {
				if (!(is_null($filterBy["filter_by"])))
					$sql .= " WHERE ".$filterBy['filter_by']." = ".$filterBy['filter'];
			}
			if (isset($orderBy["order_by"])) {
				$sql .= " ORDER BY ".$orderBy['order_by'];
				if (!is_null($orderBy["order"]))
					$sql .= " ".$orderBy['order'];	
			}
			$from = 0; $to = 0;
			if(isset($paging["page"]) && isset($paging["rows"])) {
				$from = (($paging["page"])-1)*($paging["rows"]);
				$to = $from + $paging["rows"];
			}
			return $this->getRotatingPhotoList($sql,$from,$to);
		}
		
		public function getRotatingPhotoList($query, $from=null, $to=null) {
			try {
	 			$photos = array();
	 			$rs = new iRecordset($this->mDb->execQuery($query));
	 			if(0 < $rs->retrieveRecordCount()) {
	 				$ctr = 0;
	 				while (!$rs->EOF()) {
	 					$record = $rs->current();
	 					if (((isset($from) || (!empty($from))) && !empty($to))) {
							if ($ctr >= $from && $ctr < $to) 
								$photos[] = new RotatingPhoto($record['photoID']);
							else if ($ctr >= $to)
	 							break;
	 						$ctr++;
	 					}/*
	 					else
	 						$photos[] = new RotatingPhoto($record['photoID']);*/
						try {
							$rs->next();	
						}
						catch (exception $ex) {}
	 				}
	 			}
	 			$photos["num_records"] = $rs->retrieveRecordCount();
	 			return $photos;
	 		}
 			catch (exception $e) {
 				echo $e->getMessage();	
 			}
		}
	}	
?>