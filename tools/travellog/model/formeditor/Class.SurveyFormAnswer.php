<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.GaString.php');
	require_once('travellog/model/formeditor/Class.SurveyFormAnswerDecorator.php');
	require_once('travellog/model/formeditor/Class.SurveyFieldAnswer.php');
	
	class SurveyFormAnswer
	{
		private $mSurveyFormAnswerID;
		private $mTravelerID;
		private $mSurveyParticipantID;
		private $mDateCreated;
		
		private $mDb;
		
		public function __construct($param=null,$data=null){
			$this->mDb = new dbHandler();
			$this->clear();
			if(!is_null($param)){
				$sql = 	'SELECT surveyFormAnswerID, travelerID, surveyParticipantID, dateCreated ' .
						'FROM tblSurveyFormAnswer ' .
						'WHERE surveyFormAnswerID = '.GaString::makeSqlSafe($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount() > 0){
					$this->initialize($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid SurveyFormAnswerID '.$param.' passed to object of type SurveyFormAnswer.');
				}
			}
			if(is_array($data)){
				$this->initialize($data);
			}
		}
		
		private function clear(){
			$this->setSurveyFormAnswerID(0);
			$this->setTravelerID(0);
			$this->setSurveyParticipantID(0);
			$this->setDateCreated(null);
		}

		private function initialize($data){
			$this->setSurveyFormAnswerID($data['surveyFormAnswerID']);
			$this->setTravelerID($data['travelerID']);
			$this->setSurveyParticipantID($data['surveyParticipantID']);
			$this->setDateCreated($data['dateCreated']);
		}
		
		/*** SETTERS ***/
		
		private function setSurveyFormAnswerID($param=null){
			$this->mSurveyFormAnswerID = $param;
		}
		
		public function setTravelerID($param=null){
			$this->mTravelerID = $param;
		}

		public function setSurveyParticipantID($param=null){
			$this->mSurveyParticipantID = $param;
		}
		
		public function setDateCreated($param=null){
			$this->mDateCreated = $param;
		}
		
		/*** GETTERS ***/
		
		public function getSurveyFormAnswerID(){
			return $this->mSurveyFormAnswerID;
		}
		
		public function getTravelerID(){
			return $this->mTravelerID;
		}
		
		public function getSurveyParticipantID(){
			return $this->mSurveyParticipantID;
		}
		
		public function getDateCreated(){
			return $this->mDateCreated;
		}
		
		public function getSurveyFieldAnswers(){
			return SurveyFieldAnswer::getSurveyFieldAnswersBySurveyFormAnswerID($this->getSurveyFormAnswerID());
		}
		
		public function getSurveyFieldAnswerBySurveyFieldID($surveyFieldID=0){
			return SurveyFieldAnswer::getSurveyFieldAnswerBySurveyFormAnswerIDAndSurveyFieldID($this->getSurveyFormAnswerID(),$surveyFieldID);
		}
		
		/***
		 * CRUDE
		 */
		public function save(){
			if(!$this->getSurveyFormAnswerID()){//add
				$sql =	'INSERT INTO tblSurveyFormAnswer(' .
							'`travelerID`,' .
							'`surveyParticipantID`,' .
							'`dateCreated`' .
						')VALUES(' .
							GaString::makeSqlSafe($this->getTravelerID()).',' .
							GaString::makeSqlSafe($this->getSurveyParticipantID()).',' .
							GaString::makeSqlSafe(date('Y-m-d G:i:s')).
						')';
				$this->mDb->execQuery($sql);
				$this->setSurveyFormAnswerID($this->mDb->getLastInsertedID());
			}
			else{//update
				$sql = 	'UPDATE tblSurveyFormAnswer SET ' .
							'`travelerID` 			= '.GaString::makeSqlSafe($this->getTravelerID()).',' .
							'`surveyParticipantID` 	= '.GaString::makeSqlSafe($this->getSurveyParticipantID()).' ' .
						'WHERE `surveyFormAnswerID` = '.GaString::makeSqlSafe($this->getSurveyFormAnswerID());
				$this->mDb->execQuery($sql);
			}
		}
		
		public function delete(){
			$fldAnswers = $this->getSurveyFieldAnswers();
			foreach($fldAnswers as $iFldAnswer){
				$iFldAnswer->delete();
			}
			$sql = 	'DELETE FROM tblSurveyFormAnswer ' .
					'WHERE `surveyFormAnswerID` = '.GaString::makeSqlSafe($this->getSurveyFormAnswerID());
			$this->mDb->execQuery($sql);
			$this->clear();
		}
		
		public static function getSurveyFormAnswerByTravelerID($travelerID=0){
			$db = new dbHandler();
			$sql = 	'SELECT `surveyFormAnswerID`, `travelerID`, `tblSurveyFormAnswer`.`surveyParticipantID`, `dateCreated` ' .
					'FROM `tblSurveyFormAnswer`, `tblSurveyParticipant` ' .
					'WHERE tblSurveyFormAnswer.surveyParticipantID = tblSurveyParticipant.surveyParticipantID ' .
						'AND `travelerID` = '.GaString::makeSqlSafe($travelerID);
			$rs = new iRecordset($db->execQuery($sql));
			return ($rs->retrieveRecordCount() > 0 ? new SurveyFormAnswerDecorator(new SurveyFormAnswer(null,$rs->retrieveRow(0))) : null);
		}
		
		public static function getSurveyFormAnswersBySurveyFormID($surveyFormID=0){
			$db = new dbHandler();
			$sql = 	'SELECT `surveyFormAnswerID`, `travelerID`, `tblSurveyFormAnswer`.`surveyParticipantID`, `dateCreated` ' .
					'FROM `tblSurveyFormAnswer`, `tblSurveyParticipant` ' .
					'WHERE tblSurveyFormAnswer.surveyParticipantID = tblSurveyParticipant.surveyParticipantID ' .
						'AND `tblSurveyParticipant`.`surveyFormID` = '.GaString::makeSqlSafe($surveyFormID);
			$rs = new iRecordset($db->execQuery($sql));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyFormAnswerDecorator(new SurveyFormAnswer(null,$rs->retrieveRow()));
				} 
			}
			return $ar;
		}
		
		public static function getSurveyFormAnswerBySurveyFormIDAndTravelerID($svFormID=0,$travelerID=0){
			$db = new dbHandler();
			$sql = 	'SELECT `surveyFormAnswerID`, `travelerID`, `tblSurveyFormAnswer`.`surveyParticipantID`, `dateCreated` ' .
					'FROM `tblSurveyFormAnswer`, `tblSurveyParticipant` ' .
					'WHERE tblSurveyFormAnswer.surveyParticipantID = tblSurveyParticipant.surveyParticipantID ' .
						'AND `surveyFormID` = ' .GaString::makeSqlSafe($svFormID).' '.
						'AND `travelerID` = '.GaString::makeSqlSafe($travelerID);
			$rs = new iRecordset($db->execQuery($sql));
			return ($rs->retrieveRecordCount() > 0 ? new SurveyFormAnswerDecorator(new SurveyFormAnswer(null,$rs->retrieveRow(0))) : null);
		}
		
		public static function getSurveyFormAnswersBySurveyParticipantID($param=0){
			$db = new dbHandler();
			$sql = 	'SELECT `surveyFormAnswerID`, `travelerID`, `tblSurveyFormAnswer`.`surveyParticipantID`, `dateCreated` ' .
					'FROM `tblSurveyFormAnswer`, `tblSurveyParticipant` ' .
					'WHERE tblSurveyFormAnswer.surveyParticipantID = tblSurveyParticipant.surveyParticipantID ' .
						'AND `tblSurveyParticipant`.`surveyParticipantID` = '.GaString::makeSqlSafe($param);
			$rs = new iRecordset($db->execQuery($sql));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyFormAnswerDecorator(new SurveyFormAnswer(null,$rs->retrieveRow()));
				} 
			}
			return $ar;
		}
		
	}
?>