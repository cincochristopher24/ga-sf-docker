<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');

	class SurveyParticipant
	{
		private $mSurveyParticipantID=0;
		private $mSurveyFormID=0;
		private $mGroupID=0;
		private $mDb;
		
		function __construct($param=0){
			$this->mDb = new dbHandler();
			$this->mSurveyParticipantID = (is_numeric($param)?$param:$this->mSurveyParticipantID);
			if(0!=$this->mSurveyParticipantID){
				$qry = 	'SELECT surveyParticipantID, surveyFormID, groupID ' .
						'FROM tblSurveyParticipant ' .
						'WHERE surveyParticipantID = '.ToolMan::makeSqlSafeString( $this->mSurveyParticipantID );
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(!$rs->retrieveRecordCount()){
					$trace = debug_backtrace();
					throw new exception('Invalid Survey Participant ID ['.$this->mSurveyParticipantID.'] in object of type SurveyParicipant. <<< CALLER FILE: '. $trace[0]['file'] .' LINE: '.$trace[0]['line'].'>>>');	
				}
				$this->mSurveyParticipantID = $rs->getSurveyParticipantID(0);
				$this->mSurveyFormID		= $rs->getSurveyFormID(0);
				$this->mGroupID				= $rs->getGroupID(0);				
			}
		}
		public function init($params){
			$this->mSurveyFormID	= (array_key_exists('surveyFormID',$params))?$params['surveyFormID']:$this->mSurveFormID;
			$this->mGroupID			= (array_key_exists('groupID',$params))?$params['groupID']:$this->mGroupID;
		}
		
		/***
		 * SETTERS
		 */
		public function setSurveyFormID($param=0){
			$this->mSurveyFormID = $param;
		}
		public function setGroupID($param=0){
			$this->mGroupID = $param;
		}
		/***
		 * GETTERS
		 */
		public function getSurveyParticipantID(){
			return $this->mSurveyParticipantID;
		}
		public function getSurveyFormID(){
			return $this->mSurveyFormID;
		}
		public function getGroupID(){
			return $this->mGroupID;
		}
		/***
		 * CRUDE
		 */
		public function save(){			
			//add
			if(0==$this->mSurveyParticipantID){
				$qry = 	'INSERT INTO tblSurveyParticipant(' .
							'`surveyFormID`,' .							
							'`groupID`' .
						') ' .
						'VALUES (' .
							ToolMan::makeSqlSafeString( $this->mSurveyFormID ).', '.							
							ToolMan::makeSqlSafeString( $this->mGroupID ).
						')';				
				$this->mDb->execQuery($qry);
			}
			else{//update
				$qry =	'UPDATE tblSurveyParticipant SET ' .
							'`surveyFormID`					='.ToolMan::makeSqlSafeString( $this->mSurveyFormID ).', '.
							'`groupID`						='.ToolMan::makeSqlSafeString( $this->mGroupID ).' '.
						'WHERE surveyParticipantID ='.ToolMan::makeSqlSafeString( $this->mSurveyParticipantID );
				$this->mDb->execQuery($qry);
				$this->mSurveyParticipantID = $this->mDb->getLastInsertedID();
			}
		}
		public function delete(){
			//delete the surveyform answers associated with this participant
			require_once('travellog/model/formeditor/Class.SurveyFormAnswer.php');
			$formAnswers = SurveyFormAnswer::getSurveyFormAnswersBySurveyParticipantID($this->getSurveyParticipantID());
			foreach($formAnswers as $iFrmAns){
				$iFrmAns->delete();
			}
			$qry = 	'DELETE FROM tblSurveyParticipant ' .
					'WHERE `surveyParticipantID` ='.ToolMan::makeSqlSafeString($this->mSurveyParticipantID);
			$this->mDb->execQuery($qry);
			$this->clearValues();
		}
		
		public function clearValues(){
			$this->mSurveyParticipantID = 0;
			$this->mSurveyFormID = 0;
			$this->mGroupID =0;
		}
		
		public function getNumberOfTravelerParticipants(){
			require_once("travellog/model/Class.GroupFactory.php");
			$factory =  GroupFactory::instance();
			$grp = $factory->create( array($this->mGroupID) );

			return count($grp[0]->getMembers());
		}
		
		public function getNumberOfTravelerPaticipantsWhoParticipated(){
			
		}
		
		public function getData(){
			$par = array();
			$par['surveyParticipantID'] = $this->mSurveyParticipantID;
			$par['surveyFormID']		= $this->mSurveyFormID;
			$par['groupID']				= $this->mGroupID;
			return $par;
		}
		
		public static function getSurveyParticipants($surveyFormID=0){
			$db = new dbHandler();
			$qry = 	'SELECT surveyParticipantID ' .
					'FROM tblSurveyParticipant ' .
					'WHERE surveyFormID = '.ToolMan::makeSqlSafeString( $surveyFormID );
			$rs = new iRecordset($db->execQuery($qry));			
			$ar = array();
			if(0<$rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyParticipant($rs->getSurveyParticipantID());
				}
			}			
			return $ar;
			
		}
		
		public static function getSurveyParticipantBySurveyFormIDAndGroupID($surveyFormID,$groupID){
			$db = new dbHandler();
			$qry = 	'SELECT surveyParticipantID ' .
					'FROM tblSurveyParticipant ' .
					'WHERE surveyFormID = '.ToolMan::makeSqlSafeString( $surveyFormID ).' '.
					'AND groupID = '.ToolMan::makeSqlSafeString( $groupID );
			$rs = new iRecordset($db->execQuery($qry));
			return ( 0<$rs->retrieveRecordCount() ? new SurveyParticipant($rs->getSurveyParticipantID(0)) : null );			
		}
		
		public static function getParticipatedSurveysOfGroupByGroupID($groupID=0){
			$db = new dbHandler();
			$qry =	'SELECT surveyFormID ' .
					'FROM tblSurveyParticipant ' .
					'WHERE groupID = ' . ToolMan::makeSqlSafeString( $groupID );
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				require_once('Class.SurveyForm.php');
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyForm($rs->getSurveyFormID());
				}
			}
			return $ar;
		}
		
	}
?>
