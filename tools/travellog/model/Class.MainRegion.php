<?php
/*
 * @author Kerwin Gordo
 * Created: Sept. 19, 2006
 * Purpose: Represent popular regions seen in Goabroad.com
 * Note: this is different from Region class and this is used just for the display of listings in goabroad.net
 */
 
 require_once("gaexception/Class.InstantiationException.php");
 
 class MainRegion {
 	private $mMainRegionID;
 	private $mCountryID;
 	private $mName;
 	
 	private $mCountry;
 	private $mCities;
 	
 	
 	function MainRegion($mainRegionID = 0) { 		
 		if (0 < $mainRegionID) {
 			$rs = mysql_query("SELECT * from GoAbroad_Main.tbmainregion where mainregionID='$mainRegionID'");
 			
 			if (0 == mysql_num_rows($rs)) 
 				throw new InstantiationExcetion("mainregionID is invalid");
 			
 			
 			$rsar = mysql_fetch_array($rs);
 			
 			
 			$this->mMainRegionID = $rsar['mainregionID'];
 			$this->mCountryID = $rsar['countryID'];
 			$this->mName = $rsar['mainregion']; 	
 				 			
 		}
 	}
 	
 	function getMainRegionID() {
 		return $this->mMainRegionID;
 	}
 	
 	function getName() {
 		return $this->mName;
 	}
 	
 	
 	function getCountry() {
 		if (!isset($this->mCountry)) {
 			try {
 				$this->mCountry = new Country($this->mCountryID);
 			} catch (InstantiationException $ia) {
 				throw $ia;
 			}	 			
 		}
 		return $this->mCountry;
 	}
 	
 	function getCities($listingID = 0) {
 		if (!isset($this->mCities)) {
 			
 				$rs = mysql_query("SELECT GoAbroad_Main.tbcity.cityID
							FROM GoAbroad_Main.tbcity,GoAbroad_Main.tblistclasstocountry
							WHERE mainregionID='$this->mMainRegionID'
							AND GoAbroad_Main.tbcity.cityID = GoAbroad_Main.tblistclasstocountry.countryID
							AND locationID = 3
							AND listingID = $listingID
							GROUP BY GoAbroad_Main.tbcity.cityID
							ORDER by city");
 				
 				if (0 == mysql_num_rows($rs))
 					return array();				// return a blank array;
 					
 				while ($rsar = mysql_fetch_array($rs)){
 					try {
 						$this->mCities[] = new City($rsar['cityID']);
 					} catch (InstantiationException $ia) {}
 				} 					
  			 	
 		}
 		return $this->mCities;
 	}
 	
 	
 }
  
 
?>
