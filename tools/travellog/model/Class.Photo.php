<?php
/*
 * Created on Jul 26, 2006
 * Created by: Czarisse Daphne P. Dolina
 */
 
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.PhotoType.php");
	require_once("Class.PathManager.php");
	require_once("Class.UploadManager.php");
	require_once('travellog/model/Class.Comment.php');
	require_once 'travellog/model/Class.CommentNotification.php';
	require_once('Class.ConnectionProvider.php');
	require_once('travellog/model2/photo/Class.ganetPhotoRepo.php');
	require_once 	'travellog/vo/Class.ganetValueObject.php';
	require_once('travellog/model/Class.CommentContext.php');
	require_once("amazon/Class.GANETAmazonS3WebService.php");

	//Added by joel: 06-12-2007
	//require_once('Date.php'); //PEAR Date Package
	
	class Photo implements CommentContext {
		
		// vars for db connection
		private $conn		= NULL;
 		private $rs			= NULL;
		private $rs2		= NULL;
 		
 		// var for context parameter (parameter is an object)
 		private $context		 = NULL;
		
		// vars for class attributes
		private $photoID      = NULL;
		private $phototypeID  = NULL;
		private $caption      = NULL;
		private $filename     = NULL;
		private $thumbfilename= NULL;
		private $primaryphoto = NULL;
		private $dateuploaded = NULL;
		
		private $arrcomment	  = NULL;				// added by K. Gordo due to undefined error
		
		private $mTravelersUsername = ""; //added by Jul, used in qualified cobrand header photo selection; set when $data["username"] is defined
		
		//constructor
		
			function Photo($_context, $_photoID=0, $data=null){
				$this->context = $_context;
				$this->photoID = $_photoID;
				/*try {
					$this->conn = ConnectionProvider::instance()->getConnection();
					$this->rs   = new Recordset($this->conn);
					$this->rs2	= new Recordset($this->conn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}*/
				
				if ($this->photoID > 0){  
					
					// we retrieve a temp photo object from photo repository
					$photoRepo	=	ganetPhotoRepo::instance();
					$photoContext	=	new ganetValueObject();
					$photoContext->set('PHOTO_ID',$_photoID);
					$photoContext->set('PHOTO_CONTEXT',$_context);
					$vo	=	new ganetValueObject();
					$vo->set('CONTEXT',$photoContext);
					$tempPhoto	= $photoRepo->retrieve($vo);
					
					// then copy its contents to this photo object 
					// 		reason: not to break existing clients using photo object
					$this->context			=	$tempPhoto->getContext(); // comment this out, causes broken images // uncomment Dec-16-2010 -nash
					$this->photoID			=	$tempPhoto->getPhotoID();	
					$this->phototypeID		=	$tempPhoto->getPhotoTypeID();
		 			$this->caption	 		=  	$tempPhoto->getCaption();
		 			$this->filename 		=	$tempPhoto->getFileName();  
		 			$this->thumbfilename	=	$tempPhoto->getThumbFileName(); 
					$this->primaryphoto  	=	$tempPhoto->getPrimaryPhoto(); 
					$this->dateuploaded 	=	$tempPhoto->getDateuploaded();	
					
					
					/*$sql = "SELECT * from tblPhoto where photoID = '$this->photoID' ORDER BY photoID DESC";
		 			$this->rs->Execute($sql);
		 			
	 				if ($this->rs->Recordcount() == 0){
	 					throw new Exception("Invalid Photo ID");		// ID not valid so throw exception	 					
	 				}
	 					
	 				// set values to its attributes	 		 			
		 			$this->phototypeID 	= $this->rs->Result(0,"phototypeID"); 
		 			$this->caption 		= stripslashes($this->rs->Result(0,"caption")); 
		 			$this->filename 		= stripslashes($this->rs->Result(0,"filename")); 
		 			$this->thumbfilename = stripslashes($this->rs->Result(0,"thumbfilename"));
					$this->primaryphoto 	= $this->rs->Result(0,"primaryphoto");
					$this->dateuploaded 	= $this->rs->Result(0,"dateuploaded");
					*/

					
	 			}
				elseif(is_array($data)){
					$this->photoID 		= $data['photoID'];
					$this->phototypeID	= $data['phototypeID'];
					$this->caption 		= stripslashes($data['caption']);
					$this->filename		= stripslashes($data['filename']);
					$this->thumbfilename= stripslashes($data['thumbfilename']);
					$this->primaryphoto	= $data['primaryphoto'];
					$this->dateuploaded = $data['dateuploaded'];
					
					if( isset($data["username"]) ){
						$this->mTravelersUsername = $data["username"];
					}
				}
			}
			
			
		//setter
		
			function setPhotoID($_photoID){
				$this->photoID = $_photoID;
			}
			
			function setPhotoTypeID($_phototypeID){
				$this->phototypeID = $_phototypeID;
			}
			
			function setCaption($_caption){
				$this->caption = $_caption;
			}
			
			function setFileName($_filename){
				$this->filename = $_filename;			 
			}
			
			function setThumbFileName($_thumbfilename){
				$this->thumbfilename = $_thumbfilename;			 
			}
			
			function setPrimaryPhoto($_primaryphoto){
				$this->primaryphoto = $_primaryphoto;			 
			}
			
			function setDateuploaded($_dateuploaded){
				$this->dateuploaded = $_dateuploaded;			 
			}
			
			function setContext($_context){
				$this->context = $_context;
			}
			
		//getter
			function getID(){
				return $this->getPhotoID();
			}
			
			function getPhotoID(){
				return $this->photoID;
			}
			
			function getPhotoTypeID(){
				return $this->phototypeID;
			}
			
			function getCaption(){
				return $this->caption;
			}
			
			function getFileName(){
				return $this->filename;	
			}
			
			function getThumbFileName(){
				return $this->thumbfilename;	
			}
			
			function getPrimaryPhoto(){
				return $this->primaryphoto;
			}
			
			function getDateuploaded(){
				return $this->dateuploaded;
			}
			
			function getPathOfPhoto(){
				
				if ($this->photoID > 0){
		 			
		 			try {						
		 					$path = new PathManager($this->context,"image");						
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
				
					return $path->GetPathrelative();
				}else{
					return NULL;
				}
			}
			
			/*** Added by naldz (June 06, 2008) ***/
			public function getLastFeaturedDate(){
				require_once('gaLogs/Reader/Class.Reader.php');
				$reader = new Reader();
				$sql = 	'SELECT _EXECUTION_DATE_ as lastFeaturedDate '.
						'FROM Travel_Logs.tblFeaturedItems '.
						'WHERE _NEW_sectionID = 5 '.
						'AND _NEW_genID = '.GaString::makeSqlSafe($this->getPhotoID()).' '.
						'AND _COMMAND_ = "INSERT" '.
						'ORDER BY lastFeaturedDate DESC ';
				$rs = $reader->getLogsBySql($sql);
				if($rs->retrieveRecordCount() > 0){
					return $rs->getLastFeaturedDate(0);
				}
				return null;
			}

			/*** ##### Added By: Cheryl Ivy Q. Go	1 October 2008 ##### ***/
			function getDatesPhotoIsFeatured(){
				require_once('gaLogs/Reader/Class.Reader.php');
				$mReader = new Reader();
				$mDates = array();

				$sql = 	'SELECT _EXECUTION_DATE_ as lastFeaturedDate ' .
						'FROM Travel_Logs.tblFeaturedItems ' .
						'WHERE _NEW_sectionID = 5 ' .
						'AND _NEW_genID = ' . GaString::makeSqlSafe($this->getPhotoID()) . ' ' .
						'AND _COMMAND_ = "INSERT" ' .
						'ORDER BY lastFeaturedDate DESC ';
				$rs = $mReader->getLogsBySql($sql);

				if(0 < $rs->retrieveRecordCount())
					$mDates = $rs->retrieveColumn('lastFeaturedDate');

				return $mDates;
			}
			
		//methods
		
			function Create(){
				
				$cap  = addslashes($this->caption);
 				$file = addslashes($this->filename);
 				$thumbfile = addslashes($this->thumbfilename);
				
				//$pearDate = new Date();	
				//$dateuploaded = $pearDate->getDate(1); //return YYYY-MM-DD HH:MM:SS (1 = DATE_FORMAT_ISO)
				
				$dateuploaded = getDate(time());
				
				$dateuploaded = $dateuploaded['year']."-".$dateuploaded['mon']."-". $dateuploaded['mday']." ". $dateuploaded['hours'].":".$dateuploaded['minutes'].":".$dateuploaded['seconds'];
				
				
				if(get_class($this->context) == "Group" || get_class($this->context) == "AdminGroup" ){
					if( PhotoType::$CUSTOMPAGEHEADER == $this->phototypeID ){
						$primary = 0;
					}else{
						$primary = 1;
					}
				}elseif(get_class($this->context) == "Resume"){
					$primary = 1;
				}else{
					$hasPrimary = $this->context->checkHasprimaryphoto();
					$primary = $hasPrimary? 0 : 1;
					if( !$hasPrimary && PhotoType::$PHOTOALBUM == $this->phototypeID ){
						$this->setAsPrimaryPhoto();
					}
				}		
				
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$sql = "INSERT into tblPhoto (phototypeID, caption, filename, thumbfilename, primaryphoto, dateuploaded) VALUES ('$this->phototypeID', '$cap', '$file', '$thumbfile', '$primary', '$dateuploaded' )";
				//var_dump($sql);
	 			$this->rs->Execute($sql);
				//var_dump("passed");
	 			
				$this->photoID = $this->rs->GetCurrentID();
				
				// new code to replace existing (but now commented) code
				if( PhotoType::$CUSTOMPAGEHEADER == $this->phototypeID ){
					
				}else{
					$this->context->addPhoto($this);
				}
				
				/*
				if ($this->phototypeID == PhotoType::$PROFILE){
		 			$sql = "INSERT into tblTravelertoPhoto (travelerID, photoID) VALUES (".$this->context->getTravelerID().", '$this->photoID' )";
		 			$this->rs->Execute($sql);
	 			}
	 			else if ($this->phototypeID == PhotoType::$TRAVELLOG){
									
		 			$sql = "INSERT into tblTravelLogtoPhoto (travelLogID, photoID) VALUES (".$this->context->getTravelLogID().", '$this->photoID' )";
		 			$this->rs->Execute($sql);
	 			}
				else if ($this->phototypeID == PhotoType::$RESUME){
									
		 			$sql = "INSERT into tblResumetoPhoto (travelerID, photoID) VALUES (".$this->context->gettravelerID().", '$this->photoID' )";
		 			$this->rs->Execute($sql);
						
	 			}
	 			*/
	 			
	 			
				 			 
	 		}
	 		
	 		
	 		function Update(){ 		
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());	 			
		 		$cap  = addslashes($this->caption);
 				$file = addslashes($this->filename);
 				
 				//$pearDate = new Date();	
				//$dateuploaded = $pearDate->getDate(1);  //return YYYY-MM-DD HH:MM:SS (1 = DATE_FORMAT_ISO)
 				
 				$dateuploaded = getDate(time());
				
				$dateuploaded = $dateuploaded['year']."-".$dateuploaded['mon']."-". $dateuploaded['mday']." ". $dateuploaded['hours'].":".$dateuploaded['minutes'].":".$dateuploaded['seconds'];
				
				
 				
 				
		 		$sql = "UPDATE tblPhoto SET caption = '$cap', filename = '$file', dateuploaded = '$dateuploaded' WHERE photoID = '$this->photoID'" ;
	 			$this->rs->Execute($sql);	

	 			$this->invalidateCacheEntry();
	 		}
	 		
	 		function updateCaption(){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			$cap  = addslashes($this->caption);
				$sql = "UPDATE tblPhoto SET caption = '$cap' WHERE photoID = '$this->photoID'" ;
	 			$this->rs->Execute($sql);
	 			
	 			$this->invalidateCacheEntry();
			}
	
	
	 		function Delete(){
	 			
	 			/*
	 			if ($this->phototypeID == PhotoType::$PROFILE){
		 			$sql = "DELETE FROM tblTravelertoPhoto where photoID = '$this->photoID' ";
		 			$this->rs->Execute($sql);
		 			
		 			// if deleted photo was a primary photo
	 				if ($this->primaryphoto == 1){
	 					$newobjcontext = new TravelerProfile($this->context->getTravelerID());
	 					if ($newobjcontext->getRandomPhoto())
	 						$newobjcontext->getRandomPhoto()->setAsPrimaryPhoto();
	 				}
	 				
	 			}header("Location:".$_SERVER['HTTP_REFERER']);
									exit();
	 			else if ($this->phototypeID == PhotoType::$TRAVELLOG){
		 			$sql = "DELETE FROM tblTravelLogtoPhoto where photoID = '$this->photoID' ";
		 			$this->rs->Execute($sql); 
		 			
		 			// if deleted photo was a primary photo
	 				if ($this->primaryphoto == 1){
	 					$newobjcontext = new TravelLog($this->context->getTravelLogID());
	 					if ($newobjcontext->getRandomPhoto())
	 						$newobjcontext->getRandomPhoto()->setAsPrimaryPhoto();
	 				}
	 				
	 			}
				else if ($this->phototypeID == PhotoType::$RESUME){
		 			$sql = "DELETE FROM tblResumetoPhoto where photoID = '$this->photoID' ";
		 			$this->rs->Execute($sql); 
	 			}
	 			*/
	 			
	 			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
	 			// new code to replace above code	
	 			if( PhotoType::$CUSTOMPAGEHEADER != $this->phototypeID ){//condition added by Jul to exclude custom page header photo
	 				if( PhotoType::$PHOTOALBUM == $this->phototypeID 
						|| PhotoType::$TRAVELERALBUM == $this->phototypeID
						|| PhotoType::$TRAVELLOG == $this->phototypeID ){
						$this->context->removePhoto($this->photoID,$this->dateuploaded);
	 				}else{
						$this->context->removePhoto($this->photoID);
					}
				}
	 			
	 			
						 			
						 			/*
						 			 * newly added to delete photo from server
						 			 * Oct18-2006
						 			 * by joel 
						 			 */
						 			 			
						 			 			$path = new PathManager($this->context,'image');
									 			
									 			$uploadmanager = new UploadManager();	 			
									 			$uploadmanager->setDestination($path);
												$uploadmanager->setTypeUpload('image');											
												
												$uploadmanager->deletefile($this->getFileName());
									
									/*
									 *End newly added to delete photo 
									 */
				
				// if deleted photo was a primary photo
 				if(get_class($this->context) != "Group" && get_class($this->context) != "GroupCB"){
 					
	 				if ($this->primaryphoto == 1 || PhotoType::$PHOTOALBUM == $this->phototypeID){
						try{
	 						$rand = $this->context->getRandomPhoto();
	 						if ( $rand instanceof Photo && $rand->getPhotoTypeID() == $this->phototypeID ){
	 							$rand->setAsPrimaryPhoto();
							}
						}catch(exception $e){
						}
	 				}
 				}
	 			
				PhotoAlbum::UpdatePhotoAlbumsWhichGrabbedPhoto($this->photoID);
	
	 			$sql = "DELETE FROM tblPhoto where photoID = '$this->photoID' ";
	 			$this->rs->Execute($sql);

	 			$this->invalidateCacheEntry();
	 		}
	 		 
			 
			function setAsPrimaryPhoto(){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				switch($this->phototypeID){
					
					case PhotoType::$PROFILE:					
						$sql = "UPDATE tblPhoto, tblTravelertoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelertoPhoto.photoID AND tblTravelertoPhoto.travelerID = '".$this->context->getTravelerID()."' " ;
		 				$this->rs->Execute($sql);	
                        
                         /*  set the primary photo as avatar
                         $fullpath =  $this->getPhotoLink('thumbnail');
                         $sql = "UPDATE phpbb_users SET user_avatar_type = 3, user_avatar = '$fullpath' WHERE user_id = '".$this->context->getTravelerID()."' " ;
                         $this->rs->Execute($sql); */                         
                        $this->invalidateCacheEntry();					
					break;
					
					case PhotoType::$TRAVELLOG:					
						$sql = "UPDATE tblPhoto,tblTravelLogtoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblTravelLogtoPhoto.photoID AND tblTravelLogtoPhoto.travelLogID = '".$this->context->getTravelLogID()."' " ;
		 				$this->rs->Execute($sql); 
		 				
		 				// invalidate cache for travellog primary photo
		 				$this->invalidateCacheEntry();
					break;
					
					case PhotoType::$RESUME:					
						$sql = "UPDATE tblPhoto,tblResumetoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblResumetoPhoto.photoID AND tblResumetoPhoto.travelerID = '".$this->context->gettravelerID()."' " ;
		 				$this->rs->Execute($sql); 
					break;		
					
					case PhotoType::$PROGRAM:									
						$sql = "UPDATE tblPhoto,tblProgramtoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblProgramtoPhoto.photoID AND tblProgramtoPhoto.programID = '".$this->context->getProgramID()."' " ;
		 				$this->rs->Execute($sql); 		 				
					break;
					
					case PhotoType::$FUNGROUP:									
						$sql = "UPDATE tblPhoto,tblGrouptoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblGrouptoPhoto.photoID AND tblGrouptoPhoto.groupID = '".$this->context->getGroupID()."' " ;
		 				$this->rs->Execute($sql); 		 				
					break;
					
					case PhotoType::$PHOTOALBUM:
						//$sql = "UPDATE tblPhoto,tblPhotoAlbumtoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID AND tblPhotoAlbumtoPhoto.photoalbumID = '".$this->context->getPhotoAlbumID()."' " ;
						$photoID = $this->getPhotoID();
						$sql = "UPDATE tblPhotoAlbum
								SET primaryPhoto = $photoID
								WHERE photoalbumID = ".$this->context->getPhotoAlbumID();
						
						$this->rs->Execute($sql); 
						$this->invalidateCacheEntry();	 				
					break;
					
					case PhotoType::$ARTICLE:					
						$sql = "UPDATE tblPhoto,tblArticletoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblArticletoPhoto.photoID AND tblArticletoPhoto.articleID = '".$this->context->getArticleID()."' " ;
		 				$this->rs->Execute($sql); 
						$this->invalidateCacheEntry();
					break;	
					
					/*case PhotoType::$TRAVELERALBUM:									
						$sql = "UPDATE tblPhoto,tblPhotoAlbumtoPhoto SET primaryphoto = 0 WHERE tblPhoto.photoID = tblPhotoAlbumtoPhoto.photoID AND tblPhotoAlbumtoPhoto.photoalbumID = '".$this->context->getPhotoAlbumID()."' " ;
		 				$this->rs->Execute($sql); 		 				
					break;*/							
				}				
				
				if( PhotoType::$PHOTOALBUM == $this->phototypeID ){
					return;
				}
				
				$sql = "UPDATE tblPhoto SET primaryphoto = 1 WHERE photoID = '$this->photoID'" ;
	 			$this->rs->Execute($sql);			
			}
	 		
	 	// other methods
	 		
	 		/*
	 		
	 		// get photo link 
	 		function getPhotoLink($size='standard'){
		 		
		 		$showdefimg = true;			// set show default image to true
		 		$defaultfile = "thumb.gif";
		 		if ($this->photoID > 0){
		 			
		 			try {						
		 					$path = new PathManager($this->context,"image");						
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$size = $size == 'default'?'standard':$size;
		 			
		 			
		 			$fullpath =  $path->GetPathRelative().$size.$this->filename;
		 			
		 			if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$fullpath)){	// if image to be displayed exists in URL. then proceed to display 
		 				$showdefimg = false;
		 				
		 				switch($_SERVER['HTTP_HOST']){
							case "dev.goabroad.net":
								return "http://dev.goabroad.net/".$fullpath;
								break;
							case "www.goabroad.net":
								return "http://images.goabroad.net/".$fullpath;
								break;
							case "goabroad.net.local":
								return "http://goabroad.net.local/".$fullpath;
								break;
							default:
								return "http://" . $_SERVER['HTTP_HOST'] . "/" . $fullpath;
																					
		 				}
		 			}			 			
		 		}
		 		//return $fullpath; 
		 		
		 		
		 		if ($showdefimg){	// if value is true; show default image
			 			switch (get_class($this->context)){
			 				case "TravelerProfile":
			 					$defaultfile = "profilepic.gif";
			 					break;
			 				case "Resume":
			 					$defaultfile = "profilepic.gif";
			 					break;
			 				case "TravelLog":
			 					$defaultfile = "default_images/65_photo.jpg";
			 					break;
			 				case "Program":
			 					$defaultfile = "thumb.gif";
			 					break;
			 				case "PhotoAlbum":
			 					$defaultfile = "thumb.gif";
			 					break;
			 				case "FunGroup":
			 					$defaultfile = "club_thumb.gif";
			 					break;
			 				case "Group":		// added by daf Oct 23, 2006
			 						$factory 	=  GroupFactory::instance();
									$grp     	=  $factory->create($this->context->getGroupID());
			 						switch(get_class($grp)){
				 							case "AdminGroup":
				 								$defaultfile = "group_thumb.gif";
				 							break;
				 							case "FunGroup":
				 								$defaultfile = "club_thumb.gif";
				 							break;
			 						} 						
			 						break;
			 			}
			 			
			 			switch($_SERVER['HTTP_HOST']){
								case "dev.goabroad.net":
									return "http://dev.goabroad.net/images/". $defaultfile;
									break;
								case "www.goabroad.net":
									return "http://images.goabroad.net/images/". $defaultfile;
									break;
								case "goabroad.net.local":
									return "http://goabroad.net.local/images/". $defaultfile;
									break;
								default:
									return "http://" . $_SERVER['HTTP_HOST'] . "/images/". $defaultfile;
			 			}
		 								
		 			//return "http://".$_SERVER['SERVER_NAME']."/images/". $defaultfile;
		 		}
		 			 		
	 		}	
	 		
	 		*/
	 		
	 		
	 		function getPhotoLink($size='standard'){
		 		$showdefimg = true;			// set show default image to true
		 		$defaultfile = "default_images/164_photo.gif";
			//	var_dump($this->context);
			//	$context = $this->getContext();
				
		 		if ($this->photoID > 0){
		 			try {
		 					$path = new PathManager($this->context,"image");
		 			}
					catch (Exception $e) {
					   throw $e;
					}
					
		 			$size = $size == 'default' ? 'standard' : $size;
		 			
					$fullpath =  $path->GetPathRelative() . $size . $this->filename;
					$ganetAWS = GANETAmazonS3WebService::getInstance();
					return "http://".$ganetAWS->getDomainName()."/".$fullpath;
					/*if($_SERVER['HTTP_HOST']== "cis.dev.goabroad.net"){
							return "http://cis.goabroad.net/".$fullpath;	
					}elseif($_SERVER['HTTP_HOST']== "test.goabroad.net"){
								return "http://images.goabroad.net/".$fullpath;					
					}else{
					
								// if image to be displayed exists in URL. then proceed to display
					 			//var_dump($_SERVER['DOCUMENT_ROOT']."/".$fullpath);
								if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$fullpath)){
					 				$showdefimg = false;
					 				
					 				switch($_SERVER['HTTP_HOST']){
										case "dev.goabroad.net":
											return "http://dev.goabroad.net/".$fullpath;
											break;
										case "www.goabroad.net":
											return "http://images.goabroad.net/".$fullpath;
											break;
										case "goabroad.net.local":
											return "http://goabroad.net.local/".$fullpath;
											break;
										default:
											return "http://" . $_SERVER['HTTP_HOST'] . "/" . $fullpath;
																								
					 				}
					 			}
		 			}*/
		 			
		 		}
				
				if ($showdefimg){	// if value is true; show default image
		 			
			 			switch (get_class($this->context)){
			 				case "TravelerPoster":
			 				case "TravelerProfile":
			 					
			 					if($size =="standard")			 					
			 						$defaultfile = "default_images/profile_photo.gif";
			 					if($size =="thumbnail")
			 						$defaultfile = "default_images/user_thumb.gif";
			 					if($size =="fullsize")
			 						$defaultfile = "default_images/500_photo.gif";
			 				break;
			 				case "Resume":
			 					if($size =="standard")			 					
			 						$defaultfile = "default_images/profile_photo.jpg";
			 					if($size =="thumbnail")
			 						$defaultfile = "default_images/user_thumb.jpg";
			 					if($size =="fullsize")
			 						$defaultfile = "default_images/500_photo.gif";
			 					
			 				break;
							case "Article":
			 				case "TravelLog":
			 					if($size =="standard" || $size =="default" || $size =="featured")			 					
			 						$defaultfile = "default_images/164_photo.gif";
			 					if($size =="thumbnail")
			 						$defaultfile = "default_images/65_photo.gif";
			 					if($size =="fullsize")
			 						$defaultfile = "default_images/500_photo.gif";
			 				break;
			 				case "Program":
			 					if($size =="standard" || $size =="default" || $size =="featured")			 					
			 						$defaultfile = "default_images/164_photo.gif";
			 					if($size =="thumbnail")
			 						$defaultfile = "default_images/65_photo.gif";
			 					if($size =="fullsize")
			 						$defaultfile = "default_images/500_photo.gif";	
			 				break;
			 				case "PhotoAlbum":
			 					if($size =="standard" || $size =="default" || $size =="featured")			 					
			 						$defaultfile = "default_images/164_photo.gif";
			 					if($size =="thumbnail")
			 						$defaultfile = "default_images/65_photo.gif";
			 					if($size =="fullsize")
			 						$defaultfile = "default_images/500_photo.gif";
			 					break;
			 				case "FunGroup":
			 					if($size =="standard" || $size =="default" || $size =="featured")			 					
			 						$defaultfile = "default_images/clubs_thumb.gif";
			 					if($size =="thumbnail")
			 						$defaultfile = "default_images/clubs_thumb_small.gif";
			 					break;
			 				case "AdminGroup":
			 				case "SubGroup":
			 				case "AdvisorPoster":
			 					if($size =="standard" || $size =="default" || $size =="featured")			 					
			 						$defaultfile = "default_images/groups_thumb.gif";
			 					if($size =="thumbnail")
			 						$defaultfile = "default_images/groups_thumb_small.gif";
			 					break;
			 				case "Group":		// added by daf Oct 23, 2006
			 						//$factory 	=  GroupFactory::instance();
									//$grp     	=  $factory->create($this->context->getGroupID());
									$grp = $this->context;
			 						switch($grp->getDiscriminator()){
				 							case Group::ADMIN:
				 								if($size =="standard" || $size =="default" || $size =="featured")			 					
							 						$defaultfile = "default_images/groups_thumb.gif";
							 					if($size =="thumbnail")
							 						$defaultfile = "default_images/groups_thumb_small.gif";
				 							break;
				 							case Group::FUN:
				 								if($size =="standard" || $size =="default" || $size =="featured")			 					
							 						$defaultfile = "default_images/clubs_thumb.gif";
							 					if($size =="thumbnail")
							 						$defaultfile = "default_images/clubs_thumb_small.gif";
				 							break;
			 						} 						
			 						break;
			 			}
			 			
			 			switch($_SERVER['HTTP_HOST']){
								case "dev.goabroad.net":
									return "http://dev.goabroad.net/images/". $defaultfile;
									break;
								case "www.goabroad.net":
									return "http://images.goabroad.net/images/". $defaultfile;
									break;
								case "goabroad.net.local":
									return "http://goabroad.net.local/images/". $defaultfile;
									break;
								default:
									return "http://" . $_SERVER['HTTP_HOST'] . "/images/". $defaultfile;
			 			}
		 								
		 			//return "http://".$_SERVER['SERVER_NAME']."/images/". $defaultfile;
		 		}
	 		}
	 		
	 		/*
	 		
	 		//get photo link of thumbnail
	 		function getThumbnailPhotoLink(){
		 				 			 		
		 		
		 		$showdefimg = true;			// set show default image to true
		 		$defaultfile = "default_images/65_photo.jpg";
		 		if ($this->photoID > 0){
		 			try {
		 				 $path = new PathManager($this->context,'image');
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$fullpath =  $path->GetPathRelative()."thumbnail".$this->filename;
		 			
		 			if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$fullpath)){	// if image to be displayed exists in URL. then proceed to display 
		 				$showdefimg = false;	 				
		 				switch($_SERVER['HTTP_HOST']){
							case "dev.goabroad.net":
								return "http://dev.goabroad.net/".$fullpath;
								break;
							case "www.goabroad.net":
								return "http://images.goabroad.net/".$fullpath;
								break;
							case "goabroad.net.local":
								return "http://goabroad.net.local/".$fullpath;
								break;
							default:
								return "http://" . $_SERVER['HTTP_HOST'] . "/". $fullpath;
		 				}
		 			}			
		 		}
		 	
		 				 		
		 		if ($showdefimg){		// if value is true; show default image
		 			switch (get_class($this->context)){
		 				case "TravelerProfile":
		 					$defaultfile = "profilepic_thumb.gif";
		 					break;
		 				case "Resume":
		 					$defaultfile = "profilepic_thumb.gif";
		 					break;
		 				case "TravelLog":
		 					$defaultfile = "default_images/65_photo.jpg";
		 					break;
		 				case "Program":
		 					$defaultfile = "thumb.gif";
		 					break;
		 				case "PhotoAlbum":
		 					$defaultfile = "thumb.gif";
		 					break;
		 				case "FunGroup":
		 					$defaultfile = "club_thumb.gif";
		 					break;
		 				case "AdminGroup":		// added by daf Oct 23, 2006
		 					$defaultfile = "group_thumb.gif";
		 					break;
		 			}
		 			
		 			switch($_SERVER['HTTP_HOST']){
						case "dev.goabroad.net":
							return "http://dev.goabroad.net/images/". $defaultfile;
							break;
						case "www.goabroad.net":
							return "http://images.goabroad.net/images/". $defaultfile;
							break;
						case "goabroad.net.local":
							return "http://goabroad.net.local/images/". $defaultfile;
							break;
						default:
							return "http://" . $_SERVER['HTTP_HOST'] . "/images/". $defaultfile;
			 		}
		 			//return "http://".$_SERVER['SERVER_NAME']."/images/" . $defaultfile;
		 		}
	 		
	 		}
	 		
	 		
	 		//get photo link of thumbnail for gallery
	 		function getGalleryThumbnailPhotoLink(){
		 				 			 		
		 		
		 		$showdefimg = true;			// set show default image to true
		 		$defaultfile = "default_images/65_photo.jpg";
		 		if ($this->photoID > 0){
		 			try {
		 				 $path = new PathManager($this->context,'image');
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$fullpath =  $path->GetPathRelative()."thumbnail".$this->filename;
		 			
		 			if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$fullpath)){	// if image to be displayed exists in URL. then proceed to display 
		 				$showdefimg = false;	 				
		 				switch($_SERVER['HTTP_HOST']){
							case "dev.goabroad.net":
								return "http://dev.goabroad.net/".$fullpath;
								break;
							case "www.goabroad.net":
								return "http://images.goabroad.net/".$fullpath;
								break;
							case "goabroad.net.local":
								return "http://goabroad.net.local/".$fullpath;
								break;
							default:
								return "http://" . $_SERVER['HTTP_HOST'] . "/". $fullpath;
		 				}
		 			}			
		 		}
		 	
		 				 		
		 		if ($showdefimg){		// if value is true; show default image
		 					 			
		 			switch($_SERVER['HTTP_HOST']){
						case "dev.goabroad.net":
							return "http://dev.goabroad.net/images/". $defaultfile;
							break;
						case "www.goabroad.net":
							return "http://images.goabroad.net/images/". $defaultfile;
							break;
						case "goabroad.net.local":
							return "http://goabroad.net.local/images/". $defaultfile;
							break;
						default:
							return "http://" . $_SERVER['HTTP_HOST'] . "/images/". $defaultfile;
			 		}
		 			//return "http://".$_SERVER['SERVER_NAME']."/images/" . $defaultfile;
		 		}
	 		
	 		}
	 		
	 		function getFullsizePhotoLink(){
		 				 			 		
		 		
		 		$showdefimg = true;			// set show default image to true
		 		$defaultfile = "full_photo.jpg";
		 		if ($this->photoID > 0){
		 			try {
		 				 $path = new PathManager($this->context,'image');
		 			}
					catch (Exception $e) {				   
					   throw $e;
					}
		 			
		 			$fullpath =  $path->GetPathRelative()."fullsize".$this->filename;
		 			
		 			if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$fullpath)){	// if image to be displayed exists in URL. then proceed to display 
		 				$showdefimg = false;	 				
		 				switch($_SERVER['HTTP_HOST']){
							case "dev.goabroad.net":
								return "http://dev.goabroad.net/".$fullpath;
								break;
							case "www.goabroad.net":
								return "http://images.goabroad.net/".$fullpath;
								break;
							case "goabroad.net.local":
								return "http://goabroad.net.local/".$fullpath;
								break;
							default:
								return "http://" . $_SERVER['HTTP_HOST'] . "/". $fullpath;
		 				}
		 			}			
		 		}
		 	
		 				 		
		 		if ($showdefimg){		// if value is true; show default image
		 					 			
		 			switch($_SERVER['HTTP_HOST']){
						case "dev.goabroad.net":
							return "http://dev.goabroad.net/images/". $defaultfile;
							break;
						case "www.goabroad.net":
							return "http://images.goabroad.net/images/". $defaultfile;
							break;
						case "goabroad.net.local":
							return "http://goabroad.net.local/images/". $defaultfile;
							break;
						default:
							return "http://" . $_SERVER['HTTP_HOST'] . "/images/". $defaultfile;
			 		}
		 			//return "http://".$_SERVER['SERVER_NAME']."/images/" . $defaultfile;
		 		}
	 		
	 		}
	 		
	 		*/
	 		
	 		// newly added functions for phase 2
            
            // add comment for photo
            function addComment($comment){                
                $this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
            	$commentID = $comment->getCommentID();
                
                $sql = "INSERT into tblCommenttoPhoto (commentID, photoID) VALUES ('$commentID', '$this->photoID')";
                $this->rs->Execute($sql);

				$context = $this-> getContext();

				switch($this->getPhotoTypeID()){
                	case PhotoType::$PROFILE:
						$travelerID = $context-> getTravelerID();
                		break;
		            case PhotoType::$ARTICLE:
		 				$travelerID = $context-> getAuthorID();
                		break;
                	case PhotoType::$TRAVELLOG:
						$owner = $context-> getOwner();
                	    if (get_class($owner) == 'Traveler')
							$travelerID = $owner-> getTravelerID();
						else
							$travelerID = $owner-> getAdministrator()-> getTravelerProfile()-> getTravelerID();
                		break;
                	case PhotoType::$PHOTOALBUM:
						$context = $this->getContext();
						if( 0 < $context->getGroupID() ){
							$travelerID = $context->getOwner()->getAdministrator()->getTravelerProfile()->getTravelerID();
						}else{
							$travelerID = $context->getCreator();
						}
						//$owner = $this-> getContext()-> getGroup();
						//$travelerID = $owner-> getAdministrator()-> getTravelerProfile()-> getTravelerID();
                		break;
                }

				if ($travelerID != $comment->getAuthor()){
					
					// added by chris: use notification system to send this notification
					$comment->setCommentType(2);
					require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
					RealTimeNotifier::getInstantNotification(RealTimeNotifier::RECEIVED_GREETING, 
						array(
							'COMMENT' => $comment
						))->send();
					// end add
					
					/*
					// added by: cheryl ivy q. go  18 oct 2007 -- send email notification				
					$comment-> setCommentType(2);
					// Send Notification to owner of photo
					$eNotification = new CommentNotification();				
					$eNotification-> Create($comment);
					$eNotification-> Send();

					// Send Notification to Administrator
					$aNotification = new CommentNotification();
					$aNotification-> setIsForAdmin(true);
					$aNotification-> Create($comment);
					$aNotification-> Send();
					*/
				}
            }
            
            
            // remove comment for photo
            function removeComment($comment){
                $this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
                $commentID = $comment->getCommentID();
                
                $sql = "DELETE FROM tblCommenttoPhoto where commentID = '$commentID' AND photoID = '$this->photoID'";    
                $this->rs->Execute($sql);                                
                
            }
            
            
      /**
       * Retrieves the comment/shout-outs of this Photo.
       * 
       * @param RowsLimit $rowslimit
       * @param boolean $passport
       * @return array Array of Comment object
       * 
       * Modified by Antonio Pepito Cruda Jr due to additional fields in db.
       */
      function getComments($rowslimit = NULL, $passport = false){
		if( TRUE ){
			$this->arrcomment = array();
			return $this->arrcomment;
		}
        if ( 0 == count($this->arrcomment) ){
          $this->arrcomment = array();
		
			$context = $this->getContext();
			if( is_null($context) ){
				return $this->arrcomment;
			}
		
		  $travelerID = 0;
          switch($this->phototypeID){
            case PhotoType::$PROFILE:
              $travelerID = $context->getTravelerID();
              break;                	
            case PhotoType::$TRAVELLOG:
              $objOwner = $context->getOwner();

              if (get_class($objOwner) == 'Traveler') {
                $travelerID = $objOwner->getTravelerID();
              }
              else{
                if ($objOwner->getParentID()) {
                  $travelerID = $objOwner->getParent()->getAdministratorID();
                }
                else {
                  $travelerID = $objOwner->getAdministratorID();
                }
              }
              
              break;
            case PhotoType::$GROUP:			
              $travelerID = $context->getAdministratorID();
              break;
            case PhotoType::$FUNGROUP:			
              $travelerID = $context->getAdministratorID();
              break;                	
            case PhotoType::$PHOTOALBUM:
				if( 0 < $context->getGroupID() ){
					$travelerID = $context->getOwner()->getAdministrator()->getTravelerProfile()->getTravelerID();
				}else{
					$travelerID = $context->getCreator();
				}
              //$travelerID = $context->getGroup()->getAdministratorID();
              break;
			case PhotoType::$ARTICLE:
			  $travelerID = $context->getAuthorID();
			  break;
			/*case PhotoType::$TRAVELERALBUM:		
              $travelerID = $this->getContext()->getTravelerID();
              break;*/
          }

          $this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
          
          $sql = "SELECT tblComment.commentID, tblComment.text, tblComment.author, tblComment.adate, tblComment.isread, tblComment.pokeType " .
            "FROM tblCommenttoPhoto, tblComment " .
            "WHERE tblCommenttoPhoto.commentID = tblComment.commentID " .
            "AND tblCommenttoPhoto.photoID = " . $this->photoID;
					$sql .= ($passport) ? " AND tblComment.author <> " . $travelerID : "";

			    	if (null != $rowslimit)
			    		$sql = $sql . " ORDER BY tblComment.commentID DESC LIMIT " . $rowslimit->getOffset() . "," . $rowslimit->getRows();
			    	else
			    		$sql = $sql . " ORDER BY tblComment.commentID DESC ";

			        $this->rs->Execute($sql);

			        if (0 < $this->rs->Recordcount()){
						while ($rsar = mysql_fetch_assoc($this->rs->Resultset())){
							$Traveler = new Traveler($rsar['author']);
							if ($Traveler && !$Traveler->isSuspended()){
								if ($Traveler->isAdministrator()){
									$sql2 = "SELECT groupID FROM tblGrouptoAdvisor WHERE groupID IN " .
											"(" .
												"SELECT groupID FROM tblGroup WHERE discriminator = 2 AND isSuspended = 0" .
											")" .
											"AND travelerID = " . $rsar['author'];
									$this->rs2	=	new Recordset(ConnectionProvider::instance()->getConnection());
									$this->rs2->Execute($sql2);

									if (0 < $this->rs2->Recordcount()){
			    						$Comment = new Comment();
										$Comment->setCommentID($rsar['commentID']);
										$Comment->setTheText($rsar['text']);
										$Comment->setAuthor($rsar['author']);
										$Comment->setTheDate($rsar['adate']);
										$Comment->setIsRead($rsar['isread']);
										$Comment->setPokeType($rsar['pokeType']);
										$this->arrcomment[] = $Comment;
									}
								}
								else{
									$Comment = new Comment();
									$Comment->setCommentID($rsar['commentID']);
									$Comment->setTheText($rsar['text']);
									$Comment->setAuthor($rsar['author']);
									$Comment->setTheDate($rsar['adate']);
									$Comment->setIsRead($rsar['isread']);
									$Comment->setPokeType($rsar['pokeType']);
									$this->arrcomment[] = $Comment;
								}
							}
			    		}
			        }	                
				}

				return $this->arrcomment;
			}
	 	
		 	/**
	         * Purpose: Returns the context of this photo.
	         * A photo can be given to a FUNGROUP, or PHOTOALBUM, or PROGRAM, or TRAVELERPROFILE, or TRAVELLOG
	         */
            function getContext(){
				$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				$mContext = NULL;
				if( !is_null($this->context) ){
					return $this->context;
				}
				
                switch($this->phototypeID){
                	
                	case PhotoType::$PROFILE:	
                		
                		$sql = "SELECT travelerID FROM tblTravelertoPhoto WHERE photoID = '$this->photoID' ";      	// query if it is a traveler profile photo
			            $this->rs->Execute($sql);  
			            
			            if (0 < $this->rs->Recordcount()){
		                    $mContext = new TravelerProfile($this->rs->Result(0,"travelerID"));
		                } 
						                    
                	break;
                	
                	case PhotoType::$TRAVELLOG:			
                		
                		$sql = "SELECT travellogID FROM tblTravelLogtoPhoto WHERE photoID = '$this->photoID' ";      	// query if it is a travel log photo
				        $this->rs->Execute($sql); 
				        
				        if (0 < $this->rs->Recordcount()){
							try{
		                    	$mContext = new TravelLog($this->rs->Result(0,"travellogID"));
		                	}catch(exception $e){
								$mContext = NULL;
							}
						}   
			                
                	break;
                	
                	case PhotoType::$GROUP:			
                		
                		$sql = "SELECT groupID FROM tblGroup WHERE photoID = '$this->photoID' ";      			// query if it is a group photo
                		$this->rs->Execute($sql);       
				        
				        if (0 < $this->rs->Recordcount()){
		                    $mContext = new FunGroup($this->rs->Result(0,"groupID"));
		                }   
			        
			        break;
                	
                	case PhotoType::$FUNGROUP:			
                		
                		$sql = "SELECT groupID FROM tblGrouptoPhoto WHERE photoID = '$this->photoID' ";      			// query if it is a group photo
                		$this->rs->Execute($sql);       
				        
				        if (0 < $this->rs->Recordcount()){
		                    $mContext = new FunGroup($this->rs->Result(0,"groupID"));
		            }   
                	
                	case PhotoType::$PHOTOALBUM:			
                		
                		/*$sql = "SELECT photoalbumID FROM tblPhotoAlbumtoPhoto WHERE photoID = '$this->photoID' ";       // query if it is a photoalbum photo
	                	$this->rs->Execute($sql);  
				        
				        if (0 < $this->rs->Recordcount()){
		                    $mContext = new PhotoAlbum($this->rs->Result(0,"photoalbumID"));
		                } */
						
						$mContext = self::getPhotoAlbumContext($this->photoID);
						
                	break;
                	
                	case PhotoType::$PROGRAM:			
                		
                		$sql = "SELECT programID FROM tblProgramtoPhoto WHERE photoID = '$this->photoID' ";      	// query if it is a program photo
		                $this->rs->Execute($sql);    
				        
				        if (0 < $this->rs->Recordcount()){
		                    $mContext = new Program($this->rs->Result(0,"programID"));
		                }   
			                
                	break;

					case PhotoType::$ARTICLE:			
                		
                		$sql = "SELECT articleID FROM tblArticletoPhoto WHERE photoID = '$this->photoID' ";      	// query if it is a travel log photo
				        $this->rs->Execute($sql); 
				        
				        if (0 < $this->rs->Recordcount()){
		                    $mContext = new Article($this->rs->Result(0,"articleID"));
		                }   
			                
                	break;	
                	
					/*case PhotoType::$TRAVELERALBUM:	
                		
                		$sql = "SELECT creator
								FROM tblPhotoAlbum 
								RIGHT JOIN tblPhotoAlbumtoPhoto
									ON tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID AND
										tblPhotoAlbumtoPhoto.photoID = '$this->photoID'
								WHERE creator IS NOT NULL
								GROUP BY creator";      	// query if it is a traveler album photo
			            
						$this->rs->Execute($sql);  
			            
			            if (0 < $this->rs->Recordcount()){
		                    $traveler = new Traveler($this->rs->Result(0,"creator"));
							$mContext = $traveler->getTravelerProfile();
		                } 
						                    
                	break;*/
                	
					case PhotoType::$CUSTOMPAGEHEADER:
						$sql = "SELECT groupID FROM tblCustomPageHeaderPhoto WHERE photoID = '$this->photoID' ";      	// query if it is a travel log photo
				        $this->rs->Execute($sql); 
				        
				        if (0 < $this->rs->Recordcount()){
		                    $mContext = new AdminGroup($this->rs->Result(0,"groupID"));
		                }
					break;

                	default:
                	
                		return NULL;	
                }
                
                $this->context = $mContext;

                return $mContext;
                    
            }
            
            
			/**
			 * Get 1 random Photo for Slide.
			 * by: Joel C. Llano
			 * @access public
			 * @param NULL.
			 * @return Photo Object.
			 */
            public static function getRandomSlide($context){
				$Rrs   = $this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
				
				$Rsql = "SELECT * FROM tblPhoto ORDER BY RAND() LIMIT 1";
				$Rrs->Execute($Rsql);
				$recordset = mysql_fetch_array($Rrs->Resultset());
				
				$RPhoto = new Photo($context,$recordset['photoID']);
				
				
				
				
				
				return $RPhoto;        	
			}
            
            
        /**************************************************************
         * static functions
         */
             
         
		 	// static function to get the Photos of a given context, context object can be a traveler or group object	
	 	/*	public static function getPhotos($context, RowsLimit $rl = NULL) {
	 			if ('Traveler' == get_class($context)) {
	 				// get photos for the given traveler
	 				// get photo for traveler profile
	 				// get photo
	 			
	 				
	 			}
	 			
	 			if ('FunGroup' == get_class($context)) {
	 				// TODO - implement
	 			}
	 			
	 			if ('AdminGroup' == get_class($context)) {
	 				// TODO - implement
	 			}
	 			
	 		}
          */  	
		
		/*****
			Added By Naldz: (July 25, 2008) For optimization purpose
		*****/
		public static function getTravelLogPhotos($travelLog, $limitStr = ''){
			$cache	=	ganetCacheProvider::instance()->getCache();
			$found 	=	false;
			$arPhotos	=	array();
			if ($cache != null){
				$arPhotos	=	$cache->get('Travellog_Photos_' . $travelLog->getTravelLogID());
				if ($arPhotos){
					//echo 'Found (Photo->getTravelLogPhotos) photos ' . $travelLog->getTravelLogID() . '<br/>';
					$found	=	true;
				}
			}
			
			if (!$found){
				require_once('Class.dbHandler.php');
				require_once('Class.iRecordset.php');
				
				$db = new dbHandler();
				$sql = 	'SELECT tblPhoto.* '.
						'FROM tblPhoto, tblTravelLogtoPhoto '.
						'WHERE tblTravelLogtoPhoto.photoID = tblPhoto.photoID '.
						"AND tblTravelLogtoPhoto.travellogID = ".$travelLog->getTravelLogID().' '.
						'ORDER BY tblTravelLogtoPhoto.position ASC'.$limitStr;
				$rs = new iRecordset($db->execute($sql));
	
				$arPhotos = array();
				foreach($rs as $row){
					$arPhotos[] = new Photo($travelLog, 0, $row); 
				}
				
				// put photos in cache
				if ($cache != null){
					$cache->set('Travellog_Photos_' . $travelLog->getTravelLogID(),$arPhotos,86400);				
				}
				
			}	
			return $arPhotos;
		}
		
		//added by Jul, for logging photo processing; a consequence to adding field 'tblPhoto.lastProcessing'
		public function saveImageProcess($process="RL",$newFilename=NULL){
			$this->rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			if( "RL" == $process || "RR" == $process || "TC" == $process || "CC" == $process ){
				$sql = "UPDATE tblPhoto SET imageProcess = '$process'";
				if( !is_null($newFilename) ){
					$this->setFilename($newFilename);
					$sql .= ", filename = '$newFilename' ";
				}
				$sql .= " WHERE photoID = '$this->photoID'" ;
	 			$this->rs->Execute($sql);
				$this->invalidateCacheEntry();
			}
		}
		
		//added by Jul for cobrand custom header photos
		public function getTravelersUsername(){
			return $this->mTravelersUsername;
		}
		
		
		public function invalidateCacheEntry($params = array()){
			$this->cache = ganetCacheProvider::instance()->getCache();
			switch (get_class($this->context)) {
				case 'TravelLog':
					if ($this->cache != null) {
				 			$this->cache->delete('Travellog_PrimaryPhoto_' . $this->context->getTravelLogID() );
				 			$this->cache->delete( 'Travellog_Photos_'  . $this->context->getTravelLogID());
				 			$this->cache->delete('Photo_' . get_class($this->context) . '_' .  $this->getPhotoID());
							$this->cache->delete('Photo__' .  $this->getPhotoID());
							try{
								$journal = $this->context->getTravel();
								if( $journal instanceof Travel ){
									$owner = $journal->getOwner();
									if( $owner instanceof Group ){
										$this->cache->delete('Photo_Collection_Group_Admin_' . $owner->getGroupID());
										$this->cache->delete('Photo_Collection_Group_Public_' . $owner->getGroupID());
									}else{
										$this->cache->delete('Photo_Collection_Traveler_Owner_' . $owner->getTravelerID());
										$this->cache->delete('Photo_Collection_Traveler_Public_' . $owner->getTravelerID());
										$this->cache->delete('Photo_Collection_CBTraveler_Public_' . $owner->getTravelerID());
									}
								}
							}catch(exception $e){
							}
					}
					break;
				case 'TravelerProfile':
					if ($this->cache != null) {
				 			$this->cache->delete('Traveler_PrimaryPhoto_' . $this->context->getTravelerID() );
				 			$this->cache->delete( 'Traveler_Photos_'  . $this->context->getTravelerID());
				 			$this->cache->delete('Photo_' . get_class($this->context) . '_' .  $this->getPhotoID());
				 			$this->cache->delete('Traveler_' . $this->context->getTravelerID() );
							$this->cache->delete('TravelerProfile_Photo_Count_' . $this->context->getTravelerID());
							$this->cache->delete('Photo_Collection_Traveler_Owner_' . $this->context->getTravelerID());
							$this->cache->delete('Photo_Collection_Traveler_Public_' . $this->context->getTravelerID());
							$this->cache->delete('Photo_Collection_CBTraveler_Public_' . $this->context->getTravelerID());
							$this->cache->delete('Photo__' .  $this->getPhotoID());
					}
					break;	
				case 'Article':
					if ($this->cache != null) {
				 			$this->cache->delete('Article_PrimaryPhoto_' . $this->context->getArticleID() );
				 			$this->cache->delete( 'Article_Photos_'  . $this->context->getArticleID());
				 			$this->cache->delete('Photo_' . get_class($this->context) . '_' .  $this->getPhotoID());
							$this->cache->delete('Photo__' .  $this->getPhotoID());
							if( 0 < $this->context->getGroupID() ){
								$this->cache->delete('Photo_Collection_Group_Admin_' . $this->context->getGroupID());
								$this->cache->delete('Photo_Collection_Group_Public_' . $this->context->getGroupID());
				 			}else{
								$this->cache->delete('Photo_Collection_Traveler_Owner_' . $this->context->getAuthorID());
								$this->cache->delete('Photo_Collection_Traveler_Public_' . $this->context->getAuthorID());
								$this->cache->delete('Photo_Collection_CBTraveler_Public_' . $this->context->getAuthorID());
							}
					}
					break;	
				case 'PhotoAlbum':
					if ($this->cache != null) {
				 			$this->cache->delete('PhotoAlbum_PrimaryPhoto_' . $this->context->getPhotoAlbumID() );
				 			$this->cache->delete( 'PhotoAlbum_Photos_'  . $this->context->getPhotoAlbumID());
				 			$this->cache->delete('Photo_' . get_class($this->context) . '_' .  $this->getPhotoID());
							$this->cache->delete('Photo__' .  $this->getPhotoID());
							if( 0 < $this->context->getGroupID() ){
								$this->cache->delete('Group_PhotoAlbums_' . $this->context->getGroupID() );
								$this->cache->delete('Group_FeaturedPhotoAlbums_' . $this->context->getGroupID() );
								$this->cache->delete('Photo_Collection_Group_Admin_' . $this->context->getGroupID());
								$this->cache->delete('Photo_Collection_Group_Public_' . $this->context->getGroupID());
							}else{
								$this->cache->delete('Traveler_PhotoAlbums_' . $this->context->getCreator() );
								$this->cache->delete('Photo_Collection_Traveler_Owner_' . $this->context->getCreator());
								$this->cache->delete('Photo_Collection_Traveler_Public_' . $this->context->getCreator());
								$this->cache->delete('Photo_Collection_CBTraveler_Public_' . $this->context->getCreator());
							}
					}
					break;
				
				default:
					// we invalidate all applicable photo relation caches
					if ($this->cache != null) {
				 			
						//$this->cache->delete('Travellog_PrimaryPhoto_' . $this->context->getTravelLogID() );
						//$this->cache->delete( 'Travellog_Photos_' .  $travelLog->getTravelLogID());
						$this->cache->delete('Photo_' . get_class($this->context) . '_' . $this->getPhotoID());
						// add more code here to invalidate other photo relations in cache
				 		$this->cache->delete('Photo__' .  $this->getPhotoID());
					}
					
					break;	
			}
				
		}
		
		//added by Jul to get adjusted thumbnail dimension of a photo
		// returns Array{"width","height"}
		private $adjustedDimension = NULL;
		public function getAdjustedThumbnailDimension($descriptiveSize='featured', $targetSize=92){
			$temp = array(	"width"		=>	92,
							"height"	=>	62);			
			try{
				$path = $this->getPathOfPhoto();
				if( is_null($path) ){
					return $temp;
				}
				
				$size = list($width, $height, $type, $attr) = @getimagesize($path.$descriptiveSize.$this->getFileName());
				if ( !$size ){
					return $temp;
				}
				
				//get proper rescaling ratio to find proper adjusted dimension
				$ratio = 1;
				if ( $width > $targetSize && $width == $height ){//square
					$ratio = $targetSize / $width;
				}else if ( $width > $targetSize && $width > $height ){//landscape
					$ratio = $targetSize / $width;
				}else if ( $height > $targetSize && $height > $width ){//portrait
					$ratio = $targetSize / $height;
				}
				$height = round($height*$ratio);
				$width = round($width*$ratio);

				return array("width"=>$width, "height"=>$height);
				
			}catch(exception $e){
				return $temp;
			}
		}
		public function getAdjustedThumbnailWidth($descriptiveSize='featured', $targetSize=92){
			if( is_null($this->adjustedDimension) ){
				$this->adjustedDimension = $this->getAdjustedThumbnailDimension($descriptiveSize,$targetSize);
			}
			return $this->adjustedDimension["width"];
		}
		public function getAdjustedThumbnailHeight($descriptiveSize='featured', $targetSize=92){
			if( is_null($this->adjustedDimension) ){
				$this->adjustedDimension = $this->getAdjustedThumbnailDimension($descriptiveSize,$targetSize);
			}
			return $this->adjustedDimension["height"];
		}
		
		public function DeleteBackup($backupID=0){
			$path = new PathManager($this->context,"image");
			if(file_exists($path->getAbsolutePath()."backup-".$backupID."-".$this->filename)){
				unlink($path->getAbsolutePath()."backup-".$backupID."-".$this->filename);
			}
		}
		
		public static function TravelLogPhotoExists($photoID){
			$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			$sql = "SELECT tblTravelLogtoPhoto.photoID 
					FROM tblTravelLog, tblTravelLogtoPhoto, tblPhoto 
					WHERE tblTravelLog.travellogID = tblTravelLogtoPhoto.travellogID
						AND tblTravelLogtoPhoto.photoID = $photoID
						AND tblTravelLog.deleted = 0";
			$rs->Execute($sql);
			return ($rs->Recordcount() > 0) ? true : false;
		}
		
		function hasActionPrivilege($traveler_id){
			return false;
		}
		
		public static function getPhotoAlbumContext($photoID=0){
			$rs	=	new Recordset(ConnectionProvider::instance()->getConnection());
			
			$sql = "SELECT photoalbumID FROM tblPhotoAlbumtoPhoto WHERE photoID = $photoID";
        	$rs->Execute($sql);  
	        
			$context = NULL;
	        if ( 1 == $rs->Recordcount() ){//original upload
				$context = new PhotoAlbum($rs->Result(0,"photoalbumID"));
			}else if( 1 < $rs->Recordcount() ){//photo grabbed by group album from a traveler album
				$sql = "SELECT tblPhotoAlbum.photoalbumID AS photoalbumID
						FROM tblPhotoAlbum, tblPhotoAlbumtoPhoto
						WHERE tblPhotoAlbumtoPhoto.photoID = $photoID
							AND tblPhotoAlbumtoPhoto.photoalbumID = tblPhotoAlbum.photoalbumID
							AND tblPhotoAlbum.groupID = 0
						LIMIT 0,1";
				$rs->Execute($sql); 
				if( 0 < $rs->Recordcount() ){
					$context = new PhotoAlbum($rs->Result(0,"photoalbumID"));
				}
			}
			return $context;
		}
	}