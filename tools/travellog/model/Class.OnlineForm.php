<?php
/**
 * Created on Mar 22, 2007
 * @author daf
 * 
 * 
 */
 	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
 	require_once('travellog/model/Class.OnlineQuestions.php');
 	
 	class OnlineForm {
 	
		/**
		 * Define variables for Class Member Attributes
		 */
		     
			 private $mOnlineFormID     	= 0;
			 private $mGroupID      		= 0;
			 private $mTitle       			= '';
			 private $mDescription       	= '';
			 	
			 private $mQuestions   		= array();
		     
		/**
		 * Constructor Function for this class
		 */
		           
		    function OnlineForm($onlineFormID = 0){ 
		    	
		    	$this->mOnlineFormID = $onlineFormID;
		    	
		    	
		    	try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mOnlineFormID){
                    
                    $sql = "SELECT * FROM tblOnlineForm WHERE onlineFormID = '$this->mOnlineFormID' ";
                    $this->mRs->Execute($sql);
                	
                    if ($this->mRs->Recordcount()){
                        
                        // Sets values to its class member attributes.
	                    $this->mGroupID         = $this->mRs->Result(0,"groupID");           
	                    $this->mTitle      		= stripslashes($this->mRs->Result(0,"title"));       
	                    $this->mDescription    	= stripslashes($this->mRs->Result(0,"description"));           
	                        
                    }
                }
		    	
		    }
		    
		/**
         * Setter Functions
         */    
            function setGroupID($_groupID){
                $this->mGroupID = $_groupID;
            }
            
            function setTitle($_title){
                $this->mTitle = $_title;
            }
            
            function setDescription($_desc){
                $this->mDescription = $_desc;
            }
            
		
		/**
         * Getter Functions
         */    
            function getOnlineFormID(){
                return $this->mOnlineFormID ;
            }
            
            function getGroupID(){
                return $this->mGroupID ;
            }
            
            function getTitle(){
                return $this->mTitle ;
            }
            
            function getDescription(){
                return $this->mDescription ;
            }
          
    		
    		function Save(){
    			    			
    			$_title       	= addslashes($this->mTitle);
                $_description	= addslashes($this->mDescription);
               
    			if (0 == $this->mOnlineFormID) {
    				
    				$sql = "INSERT into tblOnlineForm (groupID, title, description) " .
                        "VALUES ($this->mGroupID, '$_title', '$_description')";
	                $this->mRs->Execute($sql);       
	                $this->mGroupID = $this->mRs->GetCurrentID();
    				
    			}
    			
    			else {
    				
    				$sql = "UPDATE tblOnlineForm SET " .
    						"groupID = $this->mGroupID, title = '$_title', description = '$_description' " .
    						"WHERE onlineFormID = $this->mOnlineFormID" ;
	                $this->mRs->Execute($sql);       
    				
    			}
    		}
    		
    		
    		function Delete(){
               
                $sql = "DELETE FROM tblOnlineForm WHERE onlineFormID = '$this->mOnlineFormID' ";     
                $this->mRs->Execute($sql);
            } 
            
            
          /**
           * Purpose: Returns an array of questions for this online form.
           */
            function getQuestions(){
                
                if (0 == count($this->mQuestions)){
                    
                    $sql = "SELECT questionID from tblOnlineFormToQuestions WHERE onlineFormID = '$this->mOnlineFormID' ORDER BY position ASC " ;
                    $this->mRs->Execute($sql);
                    
                    if (0 == $this->mRs->Recordcount()){
                        return array();            
                    }
                    else {
                        while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                            try {
                                $question = new OnlineQuestions($recordset['questionID']);
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                            $this->mQuestions[] = $question;
                        }
                    }               
                }
                
                return $this->mQuestions;
            }
            
    }
?>
