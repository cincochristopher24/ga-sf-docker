<?php
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.dbHandler.php");
	require_once("Class.iRecordset.php");
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.PhotoType.php");
	require_once("travellog/model/Class.PathManager.php");
	require_once("travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionUploadedPhotoFactory.php");
	
	class CustomPageHeaderPhoto{
		
		private $mPhotoID = 0;
		private $mGroupID = 0;
		private $mPhotoOption = 0;
		private $mTravelerID = 0;
		private $mIsActive = 0;
		private $mIsCustomizing = 0;
		private $mCaptionTemp = "";
		private $mIsDeleted = 0;
		private $mIsFiltered = 0;
		private $mIsSaved = 0;
				
		private $mFilename = "";
		private $mDescription = "";
		
		private $mConn = NULL;
		private $mRs = NULL;
		private $mDb = NULL;
		
		private $mGroup = NULL;
		private $mPhotoInstance = NULL;
		
		private $mExists = FALSE;
		
		public function __construct($photoID=NULL,$data=NULL){
			try {
	 			$this->mConn = new Connection();
				$this->mRs = new Recordset($this->mConn);
				$this->mDb = new dbHandler();
				
				if( 0 < $photoID ){
					$sql = "SELECT *
							FROM tblCustomPageHeaderPhoto
							WHERE photoID = ".$this->mDb->makeSqlSafeString($photoID);
					
					$this->mRs->Execute($sql);
					if ( 0 < $this->mRs->Recordcount() ){
	 					$this->mPhotoID = $this->mRs->Result(0,"photoID");
						$this->mGroupID = $this->mRs->Result(0,"groupID");
						$this->mPhotoOption = $this->mRs->Result(0,"photoOption");
						$this->mTravelerID = $this->mRs->Result(0,"travelerID");
						$this->mIsCustomizing = $this->mRs->Result(0,"isCustomizing");
						$this->mIsActive = $this->mRs->Result(0,"isActive");
						$this->mCaptionTemp = $this->mRs->Result(0,"captionTemp");
						$this->mIsDeleted = $this->mRs->Result(0,"isDeleted");
						$this->mIsFiltered = $this->mRs->Result(0,"isFiltered");
						$this->mIsSaved = $this->mRs->Result(0,"isSaved");
						$this->mExists = TRUE;
	 				}
				}else if( is_array($data) ){
					$this->mPhotoID = $data["photoID"];
					$this->mGroupID = $data["groupID"];
					$this->mPhotoOption = $data["photoOption"];
					$this->mTravelerID = $data["travelerID"];
					$this->mIsCustomizing = $data["isCustomizing"];
					$this->mIsActive = $data["isActive"];
					$this->mCaptionTemp = $data["captionTemp"];
					$this->mIsDeleted = $data["isDeleted"];
					$this->mIsFiltered = $data["isFiltered"];
					$this->mIsSaved = $data["isSaved"];
					$this->mExists = TRUE;
				}else if( !is_null($photoID) && !is_null($data) ){
					throw new exception("Proper usage of CustomPageHeaderPhoto constructor: new CustomPageHeaderPhoto(photoID,data); where photoID>0 or data is array with keys{'photoID','groupID','photoOption','travelerID'}");
				}
			}
			catch (Exception $e) {				   
			   throw $e;
			}
		}
		
		//setters
		public function setPhotoID($photoID=0){
			$this->mPhotoID = $photoID;
		}
		public function setGroupID($groupID=0){
			$this->mGroupID = $groupID;
		}
		public function setPhotoOption($photoOption=0){
			$this->mPhotoOption = $photoOption;
		}
		public function setTravelerID($travelerID=0){
			$this->mTravelerID = $travelerID;
		}
		public function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		public function setDescription($description=""){
			$this->mDescription = $description;
		}
		public function setIsCustomizing($customizing=0){
			$this->mIsCustomizing = $customizing;
		}
		public function setIsActive($isActive=0){
			$this->mIsActive = $isActive;
		}
		public function setCaptionTemp($caption=""){
			$this->mCaptionTemp = $caption;
		}
		public function setIsDeleted($deleted=0){
			$this->mIsDeleted = 0;
		}
		public function setIsFiltered($filtered=0){
			$this->mIsFiltered = 0;
		}
		public function setIsSaved($isSaved=0){
			$this->mIsSaved = $isSaved;
		}
		public function setPhotoInstance($photo=NULL){
			$this->mPhotoInstance = $photo;
		}
		
		//getters
		public function getPhotoID(){
			return $this->mPhotoID;
		}
		public function getGroupID(){
			return $this->mGroupID;
		}
		public function getPhotoOption(){
			return $this->mPhotoOption;
		}
		public function getTravelerID(){
			return $this->mTravelerID;
		}
		public function getDescription(){
			return $this->mDescription;
		}
		public function getIsCustomizing(){
			return $this->mIsCustomizing;
		}
		public function getIsActive(){
			return $this->mIsActive;
		}
		public function getCaptionTemp(){
			return $this->mCaptionTemp;
		}
		public function getIsDeleted(){
			return $this->mIsDeleted;
		}
		public function getIsFiltered(){
			return $this->mIsFiltered;
		}
		public function getIsSaved(){
			return $this->mIsSaved;
		}
		
		public function isCustomizing(){
			return ( 1 == $this->mIsCustomizing )? TRUE : FALSE;
		}
		public function isActive(){
			return ( 1 == $this->mIsActive )? TRUE : FALSE;
		}
		public function isDeleted(){
			return ( 1 == $this->mIsDeleted )? TRUE : FALSE;
		}
		public function isFiltered(){
			return ( 1 == $this->mIsFiltered )? TRUE : FALSE;
		}
		public function isSaved(){
			return ( 1 == $this->mIsSaved )? TRUE : FALSE;
		}
		public function getPhotoInstance(){
			if( !$this->mExists ){
				return FALSE;
			}
			if( is_null($this->mPhotoInstance) ){
				$this->mPhotoInstance = new Photo($this->mGroup,$this->mPhotoID);
			}
			return $this->mPhotoInstance;
		}
		
		public function Save($finalStep=FALSE){
			if( $this->mExists && $finalStep ){
				$photo = $this->getPhotoInstance();
				if( $photo->getCaption() != $this->mCaptionTemp ){
					$photo->setCaption($this->mCaptionTemp);
					$photo->UpdateCaption();
				}
				$sql = "UPDATE tblCustomPageHeaderPhoto
						SET isActive = 1,
							isCustomizing = 0,
							captionTemp = '',
							isSaved = 1
						WHERE photoID = ".$this->mPhotoID;
				$this->mRs->Execute($sql);
				
				$this->mIsActive = 1;
				$this->mIsCustomizing = 0;
				$this->mCaptionTemp = '';
				$this->mIsSaved = 1;
			}elseif(!$finalStep){
				
				$photoID = $this->mPhotoID = $this->mPhotoInstance->getPhotoID();
				$groupID = $this->mGroupID;
				$travelerID = $this->mTravelerID;
				$photoOption = $this->mPhotoOption;
				$isActive = $this->mIsActive;
				$isCustomizing = $this->mIsCustomizing;
				$isDeleted = $this->mIsDeleted;
				$this->mDescription = $this->mPhotoInstance->getCaption();
				$captionTemp = $this->mDb->makeSqlSafeString(strip_tags($this->mDescription,"<a>"));
				$isSaved = $this->mIsSaved;
				
				$sql = "INSERT INTO tblCustomPageHeaderPhoto(photoID,groupID,photoOption,travelerID,isCustomizing,isActive,captionTemp,isDeleted,isSaved)
						VALUES($photoID,$groupID,$photoOption,$travelerID,$isCustomizing,$isActive,$captionTemp,$isDeleted,$isSaved)";
				
				$this->mRs->Execute($sql);
				$this->mExists = TRUE;
			}
		}
		
		public function UpdateCaptionTemp(){
			$captionTemp = $this->mDb->makeSqlSafeString(strip_tags($this->mCaptionTemp,"<a>"));
			$sql = "UPDATE tblCustomPageHeaderPhoto
					SET captiontemp = $captionTemp
					WHERE photoID = ".$this->mPhotoID;
			$this->mRs->Execute($sql);
			$this->mCaptionTemp = $captionTemp;
		}
		
		public function UpdateForCustomization(){
			$this->mIsCustomizing = 1;
			$sql = "UPDATE tblCustomPageHeaderPhoto
					SET isCustomizing = 1
					WHERE photoID = ".$this->mPhotoID;
			$this->mRs->Execute($sql);
		}
		
		public function Delete($customizing=FALSE){
			if( $this->mExists ){
				if( $customizing ){
					$sql = "UPDATE tblCustomPageHeaderPhoto
							SET isDeleted = 1
							WHERE photoID = ".$this->mPhotoID;
					$this->mIsDeleted = 1;
				}else{
					//if( 0 < $this->getTravelerID() ){
						$photo = $this->getPhotoInstance();
						$photo->Delete();
					//}
					$sql = "DELETE FROM tblCustomPageHeaderPhoto WHERE photoID = ".$this->mPhotoID;
				}
				
				$this->mRs->Execute($sql);
			}
		}
		
		public function UndoChanges(){
			if( $this->mExists ){
				$this->mIsCustomizing = 0;
				$this->mIsDeleted = 0;
				$this->mCaptionTemp = "";
				$sql = "UPDATE tblCustomPageHeaderPhoto
						SET isDeleted = 0,
							isCustomizing = 0,
							captionTemp = ''
						WHERE photoID = ".$this->mPhotoID;
				$this->mRs->Execute($sql);
			}
		}
		
		public function Filter($include=FALSE){
			if( $this->mExists ){
				$isFiltered = $include ? 1 : 0;
				$sql = "UPDATE tblCustomPageHeaderPhoto
						SET isFiltered = $isFiltered
						WHERE photoID = ".$this->mPhotoID;
				$this->mRs->Execute($sql);
				$this->mIsFiltered = $isFiltered;
			}
		}
		
		public function Upload($file=NULL){
			$pathManager = new PathManager($this->mGroup,'image');
			$rel = $pathManager->GetPathrelative();
			if( !is_dir($rel) ){
				$pathManager->CreatePath();
			}
			
			$upPhoto = PhotoCollectionUploadedPhotoFactory::getInstance()->create('custompageheader');
			$upPhoto->setFile($file);
			
			$this->mPhotoInstance = $upPhoto->processFile($pathManager,$this->mGroup);
			
			if( $this->mPhotoInstance ){
				$this->setIsCustomizing(1);
				$this->Save(FALSE);//finalStep=FALSE
				return $this->mPhotoInstance;
			}else{
				return FALSE;
			}
		}
		
	}