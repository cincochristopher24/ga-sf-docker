<?

class ValidatorOutput{
	
	private $name 		= NULL;	
	private $size			= NULL;
	private $status		= false;
	private $ftype		= NULL;
	private $message	= "";
	
	private $caption		= "";
	
	private $documentTitle = "";
	
	function setName($name){
		$this->name = $name;	
	}		
	
	function setSize($size){
		$this->size = $size;	
	}
	
	function setStatus($status){
		$this->status = $status;	
	}
	
	function setFType($ftype){
		$this->ftype = $ftype;	
	}
	
	function setMessage($message){
		$this->message = $message;	
	}
	
	function setCaption($caption){
		$this->caption = $caption;	
	}
	
	function setDocumentTitle($title=""){
		$this->documentTitle = $title;
	}
	
	
	
	function getName(){
		return $this->name;	
	}
	
	function getSize(){
		return $this->size;	
	}
	
	function getStatus(){
		return $this->status;
	}
	
	function getFType(){
		return $this->ftype;	
	}
	
	function getMessage(){
		return $this->message;	
	}
	
	function getCaption(){
		return $this->caption;	
	}
	
	function getDocumentTitle(){
		return $this->documentTitle;
	}
	
}


?>