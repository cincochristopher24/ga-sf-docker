<?php
/*
	Filename:	Class.TravelPlan.php formerly Class.TravelItem.php
	Module:		My Travel Plans (Travel Scheduler/Travel ToDo/Go Abroad)
	Created:	July 2007
	Purpose:	Model for a travel plan object

	EDIT HISTORY:
	
	Aug/09/2007 by Jonas Tandinco
		1.	added the "details" field and its accessors
		
	Aug/21/2007 by Jonas Tandinco
		1.	added the journalID field and its accessors
		
	Sep/05/2007 by Jonas Tandinco
		1.	modified getDateRange() since it expects start date to be required but now it's optional
		
	Sep/20/2007 by Jonas Tandinco
		1.	reworked and optimized getDateRange() 
		
	Sep/22/2007 by Jonas Tandinco
		1.	added helpers for getDateRange()
		
	Sep/28/2007 by Jonas Tandinco
		1.	enhance display of top level dates to be more intelligent
		
	Oct/15/2007 by Jonas Tandinco
		1.	removed journalID property and its accessors
		
	Oct/22/2007 by Jonas Tandinco
		1.	fixed an issue on dateCompare() that doesn't return an accurate value if the startDate provided is null
		
	Nov/12/2007 by Jonas Tandinco
		1.	added addDestination()
		
	Nov/15/2007 by Jonas Tandinco
		1.	changed createDummyDate() to always include the whole month or the whole year if unspecified
		
	Nov/22/2007 by Jonas Tandinco
		1.	class and filename renamed to TravelPlan
*/

class TravelPlan {

	private $travelWishID;
	private $userID;
	private $title;
	private $details;

	private $dateFrom;
	private $dateTo;

	private $destinations = array();

	//Getters and setters
	public function getDateFrom() {
		return $this->dateFrom;
	}
		
	public function setDateFrom($dateFrom) {
		$this->dateFrom = $dateFrom;
	}

	public function getDateTo() {
		return $this->dateTo;
	}
	
	public function setDateTo($dateTo) {
		$this->dateTo = $dateTo;
	}
	
	function getUserID() {
		return $this->userID;
	}

	function setUserID($userID) {
		$this->userID = $userID;
	}

	function getTravelWishID() {
		return $this->travelWishID;
 	}

	function setTravelWishID($travelWishID) {
		$this->travelWishID = $travelWishID;
	}

	function getTitle() {
		return $this->title;
	}
	
	function setTitle($title) {
		$this->title = $title;
	}
  
	// accessors for details property added by Jonas Tandinco Aug/09/2007
	public function getDetails() {
		return $this->details;
	}
	
	public function setDetails($details) {
		$this->details = $details;
	}
	
	// to be worked on
	public function getDestinations() {
		return $this->destinations;
	}
	
	// add a single destinaton to destinations[]
	public function addDestination($destination) {
		$this->destinations[] = $destination;
	}
	
	public function setDestinations($destinations) {
		$this->destinations = $destinations;
	}

	// save the travel
	public function save() {
		// set date range from destination dates
		$this->setDates();

		if (!$this->getTravelWishID()) {
			TravelSchedule::doInsert($this);
		} else {
			TravelSchedule::doUpdate($this);
		}
	}

	// delete travel
	public function delete() {
		TravelSchedule::doDelete($this);
	}

	// read travel destination dates and set as appropriate
	public function setDates() {
		list($startDate, $endDate) = $this->getDates();
		
		$startDate = $this->createDummyDate($startDate[0], $startDate[1], $startDate[2]);
		$endDate   = $this->createDummyDate($endDate[0], $endDate[1], $endDate[2]);
		// CHANGED DEFAULT END AND START DATE FROM NULL TO 0000-00-00 - ianne/11-24-2009
		$this->dateFrom = $startDate ? date('Y-m-d', $startDate) : '0000-00-00';
		$this->dateTo   = $endDate   ? date('Y-m-d', $endDate)   : '0000-00-00';
	}
	
	// get the earliest start date and get the latest end date
	private function getDates() {
		$destinations = $this->getDestinations();
		
		if (!count($destinations)) {
			$startDate = array(0, 0, 0);
			$endDate   = array(0, 0, 0);
			
			return array($startDate, $endDate);
		}

		$startDate = array($destinations[0]->getStartMonth(), $destinations[0]->getStartDay(), $destinations[0]->getStartYear());
		$endDate   = array($destinations[0]->getEndMonth(),   $destinations[0]->getEndDay(),   $destinations[0]->getEndYear());
		if ($this->isEmpty($endDate)) {
			$endDate = $startDate;
		}

		if (count($destinations) > 1) {
			for ($i = 1; $i < count($destinations); $i++) {
				// check if the destination start date is earlier than our current earliest start date, and update it accordingly
				$subjectDate = array($destinations[$i]->getStartMonth(), $destinations[$i]->getStartDay(), $destinations[$i]->getStartYear());

				if (!$this->isEmpty($subjectDate) && $this->dateCompare($startDate, $subjectDate)) {
					$startDate = $subjectDate;
				}

				if (!$this->isEmpty($subjectDate) && $this->dateCompare($subjectDate, $endDate)) {
					$endDate = $subjectDate;
				}
				
				// check if the destination end date is later than our current latest end date, and update it accordingly				
				$subjectDate = array($destinations[$i]->getEndMonth(), $destinations[$i]->getEndDay(), $destinations[$i]->getEndYear());
				if (!$this->isEmpty($subjectDate) && $this->dateCompare($subjectDate, $endDate)) {
					$endDate = $subjectDate;
				}
			}
		}

		return array($startDate, $endDate);
	}

	public function getDateRange() {
		if (!count($this->getDestinations())) {
			return '';
		}

		list($startDate, $endDate) = $this->getDates();

		list($startMonth, $startDay, $startYear) = $startDate;
		list($endMonth,   $endDay,   $endYear  ) = $endDate;

		// do not display anything if there is no value for start date
		if (!$startMonth && !$startDay && !$startYear) {
			return '';
		}

		// immediately display start date if there's no end date
		if (!$endMonth && !$endDay && !$endYear) {
			return $this->formatDate($startMonth, $startDay, $startYear);
		}

		// display formatted values for start date and end date
		// TODO: to be refactored, a better algorithm could be developed
		if (!$startMonth && !$endMonth) {
			if ($startYear == $endYear) {
				return $startYear;											// 2007 - 2007 becomes 2007
			} else {
				return $startYear . ' - ' . $endYear;						// 2007 - 2008 becomes 2007 - 2008 still
			}
		} elseif ($startMonth && $endMonth) {
			if ($startYear == $endYear) {
				if ($startMonth == $endMonth) {
					return date('M Y', mktime(0, 0, 0, $startMonth, 1, $startYear));	// Feb 2007 - Feb 2007 becomes Feb 2007
				} else {
					return date('M', mktime(0, 0, 0, $startMonth, 1, $startYear)) . ' - ' .
						   date('M Y', mktime(0, 0, 0, $endMonth, 1, $endYear));	// Feb 2007 - July 2007 becomes Feb - July 2007
				}
			} else {
				return $this->formatDate($startMonth, $startDay, $startYear) . ' - ' . // Feb 2006 - Feb 2007 becomes AS IS =)
					   $this->formatDate($endMonth,   $endDay,   $endYear);
			}
		} else {
			return $this->formatDate($startMonth, $startDay, $startYear) . ' - ' . // Feb 2006 - Feb 2007 becomes AS IS =)
				   $this->formatDate($endMonth,   $endDay,   $endYear);
		}
	}

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> HELPERS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
	// returns 1 if date 1 is greater than date 2, 0 otherwise
	private function dateCompare($arrDate1, $arrDate2) {
		$date1 = $this->createDummyDate($arrDate1[0], $arrDate1[1], $arrDate1[2]);
		$date2 = $this->createDummyDate($arrDate2[0], $arrDate2[1], $arrDate2[2]);

		if (!$date1) {
			return 1;
		}
		
		if ($date1 > $date2) {
			return 1;
		}
		
		return 0;
	} 
	
	// forms a date timestamp even if dates are not all filled in as long as it's valid
	private function createDummyDate($month, $day, $year) {
		if (!$year && !$month && !$day) {
			return 0;														// no date
		} elseif ($year && !$day && !$month) {
			//return mktime(0, 0, 0, 1, 1, $year);							// 2007 becomes 01-01-2007
			return mktime(0, 0, 0, 12, 31, $year);							// 2007 becomes 12-31-2007
		} elseif ($year && $month && !$day) {
			//return mktime(0, 0, 0, $month, 1, $year);						// March 2007 becomes 03-01-2007
			$numDaysThisMonth = date('t', mktime(0, 0, 0, $month, 1, $year));
			return mktime(0, 0, 0, $month, $numDaysThisMonth, $year);		// March 2007 becomes 03-31-2007
		} elseif ($year && $month && $day) {
			return mktime(0, 0, 0, $month, $day, $year);					// April 17, 2007 becomes 04-17-2007
		}
	}
	
	// formats date properly
	private function formatDate($month, $day, $year) {
		if (!$month && !$day && !$year) {
			return '';
		} elseif ($year && !$month && !$day) {
			return $year;
		} elseif ($month && $year && !$day) {
			return date('M Y', mktime(0, 0, 0, $month, 1, $year));
		} elseif ($month && $day && $year) {
			//return date('M j Y', mktime(0, 0, 0, $month, $day, $year));
			return date('M Y', mktime(0, 0, 0, $month, $day, $year));
		}
	}
	
	// checks whether or not the date is empty
	private function isEmpty($theDate) {
		if (!$theDate[0] && !$theDate[1] && !$theDate[2]) {
			return 1;
		}
		
		return 0;
	}
}

?>