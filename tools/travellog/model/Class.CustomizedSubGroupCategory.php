<?php
  /**
   * @(#) Class.CustomizedSubGroupCategory.php
   * 
   * Customized category of subgroups.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - Dec. 19, 2008
   * @version 1.1 - Feb. 2, 2009
   */
  
  require_once("travellog/model/Class.SubGroup.php");
  require_once("travellog/model/Class.SubGroupSortType.php");
  require_once("Class.SqlResourceIterator.php");
  
  class CustomizedSubGroupCategory {
  	/**
  	 * constants
  	 */
  	const FEATURED = 1;
  	const UNFEATURED = 0;
  	const UNCATEGORIZED = 1;
  	const NOT_UNCATEGORIZED = 0;
  	
  	/**
  	 * Properties
  	 */
  	private $mName = "";
  	private $mCategoryID = 0;
  	private $mParentID = 0;
  	private $mRankSettings = 0;
  	private $mDateCreated = null;
  	private $mCategoryRank = 0;
  	private $mIsFeatured = CustomizedSubGroupCategory::UNFEATURED;
  	private $mIsUncategorized = CustomizedSubGroupCategory::NOT_UNCATEGORIZED;
  	 	
  	/**
  	 * constructor
  	 * 
  	 * @param array|integer $param The attibute data or the ID of this customized subgroup category.
  	 */
  	function __construct($param = 0){
  		if (is_numeric($param) AND 0 < $param){
  			try {
	  			$handler = new dbHandler();
	  			
	  			$sql = "SELECT * FROM tblSubGroupCategory " .
	  			  "WHERE categoryID = ".$handler->makeSqlSafeString($param);
	  			
	  			$it = new SqlResourceIterator($handler->execute($sql));
	  			if ($it->hasNext()) {
	  				$this->initialize($it->next());
	  			}
  			}
  			catch(exception $ex){}
  		}
  		else if (is_array($param)) {
  			$this->initialize($param);
  		}
  	}
  	
  	/**
  	 * Add a subgroup to this customized group section.
  	 * 
  	 * @throws exception
  	 * @param integer|AdminGroup|Group|array $group The subgroup to be added. This can be the ID of the subgroup, the ID of the subgroups to be added or it can be the AdminGroup/Group object subgroup itself.
  	 * @see Group
  	 * @see AdminGroup
  	 * @return void
  	 */
  	function addSubGroups($group){
  	  $ids = array();
      if (is_numeric($group)) {
      	$ids = array($group);
      }
      else if ($group instanceof AdminGroup || $group instanceof SubGroup) {
      	$ids = array($group->getGroupID());
      }
      else if (is_array($group)){
      	if (0 == count($group)) {
      		throw new exception("Array passed is empty.");
      	}
      	$ids = $group;
      }
      else {
      	throw new exception("Parameter is not recognized.");
      }
  	  
  	  $handler = new dbHandler();
  	  
  	  $sql = "INSERT INTO `tblGroupToSubGroupCategory`(`categoryID`, `groupID`, `categorysubgrouprank`) VALUES ";
  	  foreach($ids as $id){
  	  	$sql .= "(".$handler->makeSqlSafeString($this->mCategoryID).",".$handler->makeSqlSafeString($id).", 99999),";
  	  }
  	  $sql = preg_replace("/,$/","",$sql);
  	  
  		try {
  			$handler->execute($sql);
  		}
  		catch(exception $ex){
  			throw $ex;
  		}
  	}
  	
		/**
		 * Arranges the subgroups of this category by most recent created when the value of $category is equal to
		 * SubGroupSortType::MOST_RECENT_CREATED and alphabetically when the value of $category is SubGroupSortType::ALPHABETICAL.
		 * 
		 * @throws exception
		 * @param integer $sortType Values can be SubGroupSortType::ALPHABETICAL and SubGroupSortType::MOST_RECENT_CREATED only.
		 * @see SubGroupSortType
		 * @return void
		 */
		function autoSortGroups($sortType){
			try {
				$handler = new dbHandler();
				$sql = " SELECT DISTINCT(a.groupID) " .
					"	FROM tblGroupToSubGroupCategory as a, tblGroup as b " .
      		" WHERE a.categoryID = " . $handler->makeSqlSafeString($this->mCategoryID) . 
        	" AND b.groupID = a.groupID ";
				$rs = new iRecordset($handler->execute($sql));
				
				$sql = "SET @count = 0";
				$handler->execute($sql);
				
				$sql = "UPDATE tblGroup SET subgrouprank = (SELECT @count:= @count + 1) " .
					" WHERE groupID IN (";
				
				while(!$rs->EOF()){
					$record = $rs->current();
					$sql .= $record['groupID'].",";
					
					try {
						$rs->next();
					}
					catch(exception $ex){
						throw new exception($ex->getMessage());
					}
				}
				$sql = preg_replace("/,$/",") ",$sql);
					
				switch ($sortType) {
					case SubGroupSortType::ALPHABETICAL:
						$sql .= " ORDER BY name";
					break;
					case SubGroupSortType::MOST_RECENT_CREATED:
					  $sql .= " ORDER BY datecreated DESC ";
					break;
					default:
						throw new exception("Sort type not recognized!");
				}
				
				$handler->execute($sql);
  			
  			$this->setRankSettings($sortType);
  			$this->save();
			}
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}
		}
		
  	/**
  	 * Deletes this customized subgroup category in the database.
  	 * 
  	 * @throws exception
  	 * @return void
  	 */
  	function delete(){
  		$handler = new dbHandler();
  		
  		$sql = "DELETE FROM `tblSubGroupCategory` " .
  		  " WHERE `categoryID` = ".$handler->makeSqlSafeString($this->mCategoryID);
  		  
  		try {
  			$handler->execute($sql);
  		}
  		catch(exception $ex){
  			throw new exception($ex->getMessage());
  		}
  	}
  	
  	/**
  	 * Removes all subgroups from this category.
  	 * 
  	 * @throws exception
  	 * @return void
  	 */
  	function emptyCategory(){
  	  $handler = new dbHandler();
  	  
  	  $sql = "DELETE FROM `tblGroupToSubGroupCategory`" .
  	  		" WHERE `tblGroupToSubGroupCategory`.`categoryID` = ".$handler->makeSqlSafeString($this->mCategoryID);
  	  
  	  try {
  	  	$handler->execute($sql);
  	  }
  	  catch(exception $ex){
  	  	throw new exception($ex->getMessage());
  	  }
  	}
  	
  	/**
  	 * Gets the category ID of the customized subgroup category.
  	 * 
  	 * @return integer
  	 */
  	function getCategoryID(){
  		return $this->mCategoryID;
  	}
  	
  	/**
  	 * Gets the rank of this category in the list of categories of this category's parent group.
  	 * 
  	 * @return integer
  	 */
  	function getCategoryRank(){
  		return $this->mCategoryRank;
  	}
  	
  	/**
  	 * Gets the creation date of this subgroup category,
  	 * 
  	 * @return string  
  	 */
  	function getDateCreated(){
  		return $this->mDateCreated;
  	}
  	
		/**
		 * Returns the featured subgroups of this category.
		 * 
		 * @throws exception
		 * @see SqlResourceIterator
		 * @return SqlResourceIterator Returns the featured subgroups of this category. The SqlResourceIterator object returned is of class SubGroup.
		 */
		function getFeaturedSubGroups(){
			try {
				$handler = new dbHandler();
				$sql = "SELECT a.* " .
					" FROM tblGroup as a, tblSubGroupCategory as b, tblGroupToSubGroupCategory as c " .
					" WHERE b.categoryID = c.categoryID " .
					" AND a.groupID = c.groupID " .
					" AND a.discriminator = 2 " .
					" AND a.featuredsubgrouprank > 0 " .
					" AND b.categoryID = ".$handler->makeSqlSafeString($this->mCategoryID)."" .
					" ORDER BY featuredsubgrouprank";
				
				return new SqlResourceIterator($handler->execute($sql), "SubGroup");
			} 
			catch(exception $ex){
				throw $ex;
			}
		}
  	
  	/**
  	 * Gets the name of the customized subgroup category.
  	 * 
  	 * @return string
  	 */
  	function getName(){
  		return $this->mName;
  	}
  	
  	/**
  	 * Gets the parent admin group of this customized subgroup category.
  	 * 
  	 * @return AdminGroup
  	 */
  	function getParentGroup(){
  		require_once("travellog/model/Class.AdminGroup.php");
  		
  		return new AdminGroup($this->mParentID);
  	}
  	
  	/**
  	 * Gets the ID of the parent admin group of this customized subgroup category.
  	 * 
  	 * @return integer
  	 */
  	function getParentGroupID(){
  		return $this->mParentID;
  	}
  	
  	/**
  	 * Gets the rank settings of this category.
  	 * 
  	 * @return integer
  	 */
  	function getRankSettings(){
  		return $this->mRankSettings;
  	}
  	
  	/**
  	 * Returns the subgroups of this category.
  	 * Example of how to get active subgroups with group names that contains group.
  	 *  $category = new CustomizedSubGroupCategory(12);
  	 * 	$category->getSubGroups('group', SubGroup::ACTIVE, "subgroupcategoryranksettings DESC", new RowsLimit(10, 0));
  	 * 
  	 * @param string $searchKey All subgroups of this category that contains this word in the name of the subgroups will be retrieved.
  	 * @param integer $access The flag whether to retrieve subgroups that is active, inactive or both. The default is to retrieved all subgroups. See SubGroup constants.
  	 * @param string $order The order of the subgroups to be retrieved.  
  	 * @param RowsLimit $limit The limit of the subgroups to be retrieved.
  	 * @see RowsLimit
  	 * @return array Array of AdminGroup
  	 */
  	function getSubGroups($searchKey = null, $access = null, $order = " categorysubgrouprank", $limit = null){
			try {
				$grp_access = (SubGroup::ACTIVE == $access) ? "1,2,3" : null;
				$grp_access = (SubGroup::INACTIVE == $access) ? "4" : $grp_access;
				
				$handler = new dbHandler();
				
				$sql = "SELECT a.*, c.categorysubgrouprank " .
					" FROM tblGroup as a, tblGroupToSubGroupCategory as c " .
					" WHERE a.groupID = c.groupID " .
					" AND c.categoryID = ".$handler->makeSqlSafeString($this->mCategoryID);
				$sql .= (is_null($searchKey) OR "" == trim($searchKey)) ? "" : " AND a.name LIKE ".$handler->makeSqlSafeString($searchKey);
				$sql .= (is_null($grp_access)) ? "" : " AND a.groupaccess IN ($grp_access) ";
				$sql .= (!is_null($order) AND "" != trim($order)) ? " ORDER BY ".$order : "";
				$sql .= ($limit instanceof RowsLimit) ? " ".$limit->createSqlLimit() : "";
				
				return new SqlResourceIterator($handler->execute($sql), "SubGroup");
	 		}
	 		catch(exception $ex){
	 			throw new exception ($ex->getMessage());
	 		}
  	}
  	
  	function getSubGroupsCountForTraveler(Traveler $traveler, $searchKey = "", $access = null){
			try {
				$grp_access = (SubGroup::ACTIVE == $access) ? "1,2,3" : null;
				$grp_access = (SubGroup::INACTIVE == $access) ? "4" : $grp_access;
				
				$handler = new dbHandler();
				
				$sql = "SELECT count(a.groupID) as count " .
					" FROM tblGroup as a, tblGrouptoTraveler as b, tblGroupToSubGroupCategory as c " .
					" WHERE a.groupID = c.groupID " .
					" AND b.groupID = c.groupID " .
					" AND c.categoryID = ".$handler->makeSqlSafeString($this->mCategoryID)."".
					" AND b.travelerID = ".$traveler->getTravelerID();
				$sql .= (is_null($searchKey) OR "" == trim($searchKey)) ? "" : " AND a.name LIKE ".$handler->makeSqlSafeString($searchKey);
				$sql .= (is_null($grp_access)) ? "" : " AND a.groupaccess IN ($grp_access) ";
				
				$it = new SqlResourceIterator($handler->execute($sql));
				
				return $it->count();
	 		}
	 		catch(exception $ex){
	 			throw new exception ($ex->getMessage());
	 		}
  	}  	
  	
  	function getSubGroupsForTraveler(Traveler $traveler, $searchKey = null, $access = null, $order = " categorysubgrouprank", $limit = null){
			try {
				$grp_access = (SubGroup::ACTIVE == $access) ? "1,2,3" : null;
				$grp_access = (SubGroup::INACTIVE == $access) ? "4" : $grp_access;
				
				$handler = new dbHandler();
				
				$sql = "SELECT a.*, c.categorysubgrouprank " .
					" FROM tblGroup as a, tblGrouptoTraveler as b, tblGroupToSubGroupCategory as c " .
					" WHERE a.groupID = c.groupID " .
					" AND b.groupID = c.groupID " .
					" AND c.categoryID = ".$handler->makeSqlSafeString($this->mCategoryID)."".
					" AND b.travelerID = ".$traveler->getTravelerID();
				$sql .= (is_null($searchKey) OR "" == trim($searchKey)) ? "" : " AND a.name LIKE ".$handler->makeSqlSafeString($searchKey);
				$sql .= (is_null($grp_access)) ? "" : " AND a.groupaccess IN ($grp_access) ";
				$sql .= (!is_null($order) AND "" != trim($order)) ? " ORDER BY ".$order : "";
				$sql .= ($limit instanceof RowsLimit) ? " ".$limit->createSqlLimit() : "";

				return new SqlResourceIterator($handler->execute($sql), "SubGroup");
	 		}
	 		catch(exception $ex){
	 			throw new exception ($ex->getMessage());
	 		}
  	}
  	
  	/**
  	 * Returns the subgroups of this category.
  	 * Example of how to get active subgroups with group names that contains group.
  	 *  $category = new CustomizedSubGroupCategory(12);
  	 * 	$category->getSubGroups('group', SubGroup::ACTIVE, "subgroupcategoryranksettings DESC", new RowsLimit(10, 0));
  	 * 
  	 * @param string $searchKey All subgroups of this category that contains this word in the name of the subgroups will be retrieved.
  	 * @param integer $access The flag whether to retrieve subgroups that is active, inactive or both. The default is to retrieved all subgroups. See SubGroup constants.
  	 * @return array Array of AdminGroup
  	 */
  	function getSubGroupsCount($searchKey = "", $access = null){
			try {
				$grp_access = (SubGroup::ACTIVE == $access) ? "1,2,3" : null;
				$grp_access = (SubGroup::INACTIVE == $access) ? "4" : $grp_access;
				
				$handler = new dbHandler();
				
				$sql = "SELECT count(a.groupID) as count " .
					" FROM tblGroup as a, tblGroupToSubGroupCategory as c " .
					" WHERE a.groupID = c.groupID " .
					" AND c.categoryID = ".$handler->makeSqlSafeString($this->mCategoryID);
				$sql .= (is_null($searchKey) OR "" == trim($searchKey)) ? "" : " AND a.name LIKE ".$handler->makeSqlSafeString($searchKey);
				$sql .= (is_null($grp_access)) ? "" : " AND a.groupaccess IN ($grp_access) ";
				
				$it = new SqlResourceIterator($handler->execute($sql));
				
				return $it->count();
	 		}
	 		catch(exception $ex){
	 			throw new exception ($ex->getMessage());
	 		}
  	}
  	
  	/**
  	 * Sets the attribute data of this class.
  	 * 
  	 * @param array $data Associative array with keys equal to the fields in tblSubGroupCategory
  	 */
  	private function initialize($data){
  		if (0 < count($data)) {
  			$this->mName = (isset($data['name'])) ? $data['name'] : "";
  			$this->mCategoryID = (isset($data['categoryID'])) ? $data['categoryID'] : 0;
  			$this->mParentID = (isset($data['parentID'])) ? $data['parentID'] : 0;
  			$this->mRankSettings = (isset($data['ranksettings'])) ? $data['ranksettings'] : 0;
  			$this->mDateCreated = (isset($data['datecreated'])) ? $data['datecreated'] : null;
  			$this->mCategoryRank = (isset($data['categoryRank'])) ? $data['categoryRank'] : 0;
  			$this->mIsFeatured = (isset($data['isFeatured'])) ? $data['isFeatured'] : $this->mIsFeatured;
  			$this->mIsUncategorized = (isset($data['isUncategorized'])) ? $data['isUncategorized'] : $this->mIsUncategorized;
  		}
  	}
  	
  	/**
  	 * Returns true when this category is featured.
  	 * 
  	 * @return boolean Returns true when this category is featured.
  	 */
  	function isFeatured(){
  		return (CustomizedSubGroupCategory::FEATURED == $this->mIsFeatured) ? true : false;
  	}
  	
  	/**
  	 * Returns true when this category is an uncategorized category.
  	 * 
  	 * @return boolean Returns true when this category is an uncategorized category.
  	 */
  	function isUncategorized(){
  		return (CustomizedSubGroupCategory::UNCATEGORIZED == $this->mIsUncategorized) ? true : false;
  	}
  	
  	/**
  	 * Remove a subgroup to this customized group section.
  	 * 
  	 * @throws exception
  	 * @param integer|AdminGroup|Group|array $group The subgroup to be added. This can be the ID of the subgroup, the ID of the subgroups to be added or it can be the AdminGroup/Group object subgroup itself.
  	 * @see Group
  	 * @see AdminGroup
  	 * @return void
  	 */
  	function removeSubGroups($group){
  	  $ids = array();
  	  if (is_numeric($group)) {
  	  	$ids = array($group);
  	  }
  	  else if ($group instanceof AdminGroup || $group instanceof SubGroup) {
  	  	$ids = array($group->getGroupID());
  	  }
  	  else if (is_array($group)){
  	  	if (0 == count($group)) {
  	  		throw new exception("Array passed is empty.");
  	  	}
  	  	$ids = $group;
  	  }
  	  else {
  	  	throw new exception("Parameter is not recognized.");
  	  }
  	  
  	  $handler = new dbHandler();
  	  
  	  $sql = "DELETE FROM `tblGroupToSubGroupCategory`" .
  	  		" WHERE `groupID` IN (".implode(",",$ids).") " .
  	  		" AND `categoryID` = ".$handler->makeSqlSafeString($this->mCategoryID);
  	  
  	  try {
  	  	$handler->execute($sql);
  	  }
  	  catch(exception $ex){
  	  	throw new exception($ex->getMessage());
  	  }
  	}
  	
  	/**
  	 * Inserts/Updates a subgroup category in the database. When the categoryID is
  	 * equal to 0 this inserts a new category to the database, otherwise this updates
  	 * an existent category.
  	 * 
  	 * @throws exception
  	 * @return void
  	 */
  	function save(){
  		$handler = new dbHandler();
  		if (0 == $this->mCategoryID) {
	      $sql = "INSERT INTO `tblSubGroupCategory` (`name`, `parentID`, `ranksettings`, `categoryRank`, `datecreated`, `isUncategorized`, `isFeatured`) " .
	      	" VALUES (" .
				  "".$handler->makeSqlSafeString($this->mName).",".
					"".$handler->makeSqlSafeString($this->mParentID)."," .
					"".$handler->makeSqlSafeString($this->mRankSettings)."," .
					"".$handler->makeSqlSafeString($this->mCategoryRank).",".
					"NOW()," .
					"".$handler->makeSqlSafeString($this->mIsUncategorized).",".
					"".$handler->makeSqlSafeString($this->mIsFeatured)."".
					")";
  		}
  		else {
	      $sql = "UPDATE `tblSubGroupCategory` SET " .
				  " `name` = ".$handler->makeSqlSafeString($this->mName).",".
					" `parentID` = ".$handler->makeSqlSafeString($this->mParentID)."," .
					" `ranksettings` = ".$handler->makeSqlSafeString($this->mRankSettings)."," .
					" `categoryRank` = ".$handler->makeSqlSafeString($this->mCategoryRank)."," .
					" `isFeatured` = ".$handler->makeSqlSafeString($this->mIsFeatured)."," .
					" `isUncategorized` = ".$handler->makeSqlSafeString($this->mIsUncategorized)."" .
					" WHERE `categoryID` = ".$handler->makeSqlSafeString($this->mCategoryID);		
  		}
  		
  		try {
  			$handler->execute($sql);
  			$this->mCategoryID = (0 == $this->mCategoryID) ? $handler->getLastInsertedID() : $this->mCategoryID;
  		}
  		catch(exception $ex){
  			throw new exception($ex->getMessage());
  		}
  	}
  	
  	/**
  	 * Sets the ID of the customized subgroup category.
  	 * 
  	 * @param integer $id
  	 * @return void
  	 */
  	function setCategoryID($id){
  		$this->mCategoryID = $id;
  	}
  	
  	/**
  	 * Sets the rank of this category in the list of categories of this category's parent group.
  	 * 
  	 * @param integer $rank
  	 * @return void
  	 */
  	function setCategoryRank($rank){
  		$this->mCategoryRank = $rank;
  	}
  	
  	/**
  	 * Sets the date when this subgroup category has been created.
  	 * 
  	 * @param string $date
  	 * @return void
  	 */
  	function setDateCreated($date){
  		$this->mDateCreated = $date;
  	}
  	
  	/**
  	 * Sets this subgroup category as featured or not.
  	 * 
  	 * @param integer $isFeatured The flag whether this category is featured or not.
  	 * @return void
  	 */
  	function setIsFeatured($isFeatured){
  		$this->mIsFeatured = $isFeatured;
  	}
  	
  	/**
  	 * Sets this category as an uncategorized category.
  	 * 
  	 * @param integer $isUncategorized The flag whether this category is uncategorized or not.
  	 * @return void
  	 */
  	function setIsUncategorized($isUncategorized){
  		$this->mIsUncategorized = $isUncategorized;
  	}
  	
  	/**
  	 * Sets the name of the customized subgroup category.
  	 * 
  	 * @param string $name
  	 * @return void
  	 */
  	function setName($name){
  		$this->mName = $name;
  	}
  	
  	/**
  	 * Sets the ID of the parent admin group of the customized subgroup category.
  	 * 
  	 * @param integer $id
  	 * @return void
  	 */
  	function setParentID($id){
  		$this->mParentID = $id;
  	}
  	
  	/**
  	 * Sets the current rank settings of this custom group.
  	 * 
  	 * @param integer $val
  	 * @return void
  	 */
  	function setRankSettings($val){
  		$this->mRankSettings = $val;
  	}
  	
		/**
		 * Changes the ranking in the database of the given subgroup using the given rank in the list of subgroups of this category.
		 * 
		 * @throws exception
		 * @param AdminGroup|integer $subgroup If the given subgroup is integer this means the ID of the subgroup.
		 * @param integer $given_rank the new rank of the subgroup
		 * @param string $action one of the following (showActive or showInActive)
		 * @return void
		 */
		function updateNewRankOfSubGroup($subgroup, $given_rank, $action) {
			require_once("travellog/model/Class.CategoryRankHandler.php");
			
			$handler = new dbHandler();
			$grpAccessList = "1,2,3";
			$grpAccessList = ('showInActive' == $action) ? "4" : $grpAccessList;
      
			$sID = (is_numeric($subgroup)) ? $subgroup : $subgroup->getGroupID();
			$subgroups = array();
      
      $sql = "SELECT DISTINCT(a.groupID) FROM tblGroupToSubGroupCategory as a, tblGroup as b " .
      	" WHERE a.categoryID = " . $handler->makeSqlSafeString($this->mCategoryID) . 
        " AND b.groupaccess IN (".$grpAccessList.") " .
        " AND b.groupID = a.groupID " .
        " ORDER BY categorysubgrouprank " ;	
			
			try {
				$rs = new SqlResourceIterator($handler->execute($sql));
				$cnt = 0;
				
				while($rs->hasNext()){
					$rs->next();
					$cnt = ($cnt == ($given_rank-1) AND $sID <> $rs->groupID()) ? $cnt + 1 : $cnt;
					
					if ($sID <> $rs->groupID()) {
					  $subgroups[$cnt] = $rs->groupID();
					  $cnt++;
					}
				}
				
				$subgroups[$given_rank-1] = $sID;
				CategoryRankHandler::arrangeRankOfSubGroups($subgroups);
				
  			$this->setRankSettings(SubGroupSortType::CUSTOM);
  			$this->save();
			}
			catch(exception $ex){
				throw $ex;
			}
		}
  }
