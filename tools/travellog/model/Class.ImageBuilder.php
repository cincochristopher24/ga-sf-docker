<?php


/**
 * Created on Sep 22, 2006
 * @author Joel C. LLano
 * Purpose: TO build uploaded photo into different context.
 */

require_once("Class.ResizeImage.php");
require_once("Class.CropCanvas.php");

CLass ImageBuilder{
	
	private static $_instance = null;
	
	private $imagefile;
	private $filename;	
	private $imagetype;
	private $destination;
	private $width;
	private $height;
	
	private $mRelativePath = "";
	
	public static function getInstance(){
		if (is_null( self :: $_instance))
		    self :: $_instance = new ImageBuilder();
		 
		return self :: $_instance;
    }
    
    
    private function __construct(){
    
    }
	
	function setImageFile($imagefile){
		$this->imagefile = $imagefile;
	}
	
	function getImageFile(){
		return $this->imagefile;
	}
	
	function setFilename($filename){
		$this->filename = $filename;
	}
	
	function getFilename(){
		return $this->filename;
	}
	
	function setDestination($destination){
		$this->destination = $destination;	
	}
	
	function getDestination(){
		return $this->destination;	
	}
		
	function setImagetype($imagetype){
		$this->imagetype = $imagetype;
	}
	
	function setArea($area){
		$this->area = $area;
	}
	
	function setRelativePath($path=""){
		$this->mRelativePath = $path;
	}
	
	function getRelativePath(){
		return $this->mRelativePath;
	}
	
	function resize($alias){
		require_once 'Image/Transform.php';
		$i = Image_Transform::factory('IM');
		
		$i->load($this->imagefile);
		$i->fit($this->width,$this->height);
		$i->save($this->destination.$alias.$this->filename);
	}
	
	function crop_me($alias, $cropW, $cropH){
		require_once 'Image/Transform.php';
		$i = Image_Transform::factory('IM');
		$i->load($this->imagefile);
		
		if($i->getImageWidth() >$cropW || $i->getImageHeight() >$cropH){
			/*	
			* 	check which is GT parameter value 
			*	IF $cropW is GT than $cropH 
			*		make size of hieght to $cropW
			*	ELSE
			*	make size of hieght to $cropH
			*/
			if($cropW >= $cropH){
				$i->getImageWidth()>=$i->getImageHeight()?$i->fitY($cropW):$i->fitX($cropW);
				$i->save($this->destination.$alias.$this->filename);
				$i->load($this->destination.$alias.$this->filename);
			}else{
				$i->getImageWidth()>=$i->getImageHeight()?$i->fitY($cropH):$i->fitX($cropH);
				$i->save($this->destination.$alias.$this->filename);
				$i->load($this->destination.$alias.$this->filename);
			}
			
			$x = ($i->getImageWidth()-$cropW)/2<=0?0:($i->getImageWidth()-$cropW)/2;
			$y = ($i->getImageHeight()-$cropH)/2<=0?0:($i->getImageHeight()-$cropH)/2;
			
			$i->crop($cropW,$cropH, $x , $y);
			$i->save($this->destination.$alias.$this->filename);	
					
		}else{
			$i->save($this->destination.$alias.$this->filename);
		}
		
	}
	
	function bake(UploadManager $UMGR){
		require_once("travellog/UIComponent/Photo/factory/Class.PhotoBuilderFactory.php");
		PhotoBuilderFactory::create($UMGR->getContext(),$this);
	}
	
	function create($alias,$width,$height){
		$this->width  = $width;
		$this->height = $height;
		$this->resize($alias);
	}
	
	
	
			
}

?>