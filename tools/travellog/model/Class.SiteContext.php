<?php
	require_once('Class.Config.php');
	require_once('travellog/model/Class.DomainResolver.php');
	require_once('travellog/model/Class.GMapApiKeyHolder.php');
	
	class SiteContext
	{
		static private $instance = null;
		
		static public function getInstance(){
			if(is_null(self::$instance))self::$instance = new self;
			return self::$instance;
		}
		
		static public function getInstanceByDomainName($domain=''){
			return new self($domain);
		}
		
		private $mGroupID			= 0;
		private $mSiteName			= 'GANET';
		private $mSiteDescr			= null;
		private $mFolderName		= 'GANET';
		private $mCopyrightYear		= '2006';
		private $mAdminEmail		= 'admin@goabroad.net';
		private $mEmailName			= 'The GoAbroad Network Team';
		private $mUrl				= null;
		private $mOnlineAdvising	= 0;
		private $mAllowCustomPageHeader	= 0;
		
		private function __construct($domain=null){
			//check if we are in cobrand context.
			$cobrand = Config::findByServerName(DomainResolver::getInstance()->resolve(!is_null($domain) ? $domain : $_SERVER['HTTP_HOST']));
			if(!is_null($cobrand) && $cobrand->getSiteName() != 'GANET'){
				$this->mGroupID			= $cobrand->getGroupID();
				$this->mSiteName		= $cobrand->getSiteName();
				$this->mSiteDescr		= $cobrand->getSiteDescription();
				$this->mFolderName		= $cobrand->getFolderName();
				$this->mCopyrightYear	= $cobrand->getCopyrightYear();
				$this->mAdminEmail		= $cobrand->getAdminEmail();
				$this->mEmailName		= $cobrand->getEmailName();
				$this->mUrl				= $cobrand->getUrl();
				$this->mOnlineAdvising	= $cobrand->getOnlineAdvising();				
				$this->mAllowCustomPageHeader	= $cobrand->getAllowCustomPageHeader();
			}
		}
		
		public function isInCobrand(){
			return $this->mGroupID != 0;
		}
		
		public function getGroupID(){
			return $this->mGroupID;
		}
		
		public function getSiteName(){
			return $this->mSiteName;
		}
		
		public function getSiteDescription(){
			return $this->mSiteDescr;
		}
		
		public function getFolderName(){
			return $this->mFolderName;
		}
		
		public function getCopyrightYear(){
			return $this->mCopyrightYear;
		}
		
		public function getAdminEmail(){
			return $this->mAdminEmail;
		}
		
		public function getEmailName(){
			return $this->mEmailName;
		}
		
		public function isOnlineAdvising(){
			return $this->mOnlineAdvising == 1;
		}
		
		public function getGMapApiKey(){
			return GMapApiKeyHolder::getInstance()->getKey($_SERVER['HTTP_HOST']);
		}
		
		public function getServerName(){
			return $_SERVER['HTTP_HOST'];
		}
		
		public function allowCustomPageHeader(){
			return $this->mAllowCustomPageHeader;
		}
		
	}
?>