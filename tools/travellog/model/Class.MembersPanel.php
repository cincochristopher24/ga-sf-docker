<?php

	require_once 'travellog/model/Class.RowsLimit.php';
	require_once 'travellog/model/Class.Traveler.php';
	require_once 'travellog/model/Class.TravelerProfile.php';

	class MembersPanel{

		private $mGroup			= 0;
		private $mFirstName		= '';
		private $mLastName		= '';
		private $mEmailAdd		= '';
		private $mUserName 		= '';
		private $mPassword		= '';

		public static $SEARCH_REG_STAFF				= 1;
		public static $REMOVE_REG_STAFF				= 2;
		public static $REMOVE_SUPER_STAFF			= 3;
		public static $CANCEL_PENDING_STAFF			= 4;
		public static $INVITE_PENDING_STAFF			= 5;
		public static $STAFF_UPDATES_HISTORY		= 6;
		public static $STAFF_DENIED_INVITES			= 7;
		public static $EDIT_NOTIFICATION			= 8;
		public static $SEARCH_ALL_STAFF				= 9;
		public static $SEARCH_SUPER_STAFF			= 10;
		public static $MANAGE_STAFFED_GROUPS		= 11;
		public static $PROCEED_MANAGE_STAFF			= 12;
		public static $MANAGE_SUPER_STAFF			= 13;
		public static $PROCEED_MANAGE_SUPERSTAFF	= 14;
		public static $CHANGE_NOTIFICATION			= 15;

		public static $ADVISOR				= 1;
		public static $SUPERSTAFF			= 2;
		public static $REGULARSTAFF			= 3;
		public static $MEMBER				= 4;
		public static $NONMEMBER			= 5;

		private $mSubGroupIDs 		= array();

		function MembersPanel(){
			try{
				$this->mConn = new Connection;
				$this->mRs	= new Recordset($this->mConn);
				$this->mRs2	= new Recordset($this->mConn);
				$this->mRs3	= new Recordset($this->mConn);
			}
			catch(exception $e){
				throw $e;
			}

			return $this;
		}

		/******************** START: SETTER FUNCTIONS ***********************/
		function setGroup($_group = '',$loggedUser=NULL,$viewerType=1){
			$this->mGroup = $_group;
			
			if( 3 > $viewerType || ( 3 == $viewerType && $this->mGroup->isSubGroup() ) ){
				$this->mSubGroupIDs = array($this->mGroup->getGroupID());
			}
			
			if( 2 == $this->mGroup->getDiscriminator() ){
				$filter = NULL;
				if( 3 == $viewerType && $this->mGroup->isParent() ){
					$staffedGroups = Traveler::getStaffedGroups($loggedUser->getTravelerID(),array($this->mGroup->getGroupID()));
					$staffedIDs = array();
					foreach($staffedGroups AS $sg){
						$staffedIDs[] = $sg->getGroupID();
					}
					unset($sg);
				
					$cond = new Condition;
					$cond->setAttributeName("groupID");
					$cond->setOperation(FilterOp::$IN);
					$cond->setValue("(".implode(',', $staffedIDs).")");
					$filter = new FilterCriteria2;
					$filter->addBooleanOp("AND");
					$filter->addCondition($cond);
				}

				$sp_array = $this->mGroup->getSubGroupsAndProjects(NULL,NULL,0,$filter);
				$subgroups = $sp_array[0];
			
				foreach($subgroups AS $sb){
					$this->mSubGroupIDs[] = $sb->getGroupID();
				}
				unset($sb);
			}
		}
		function setFirstName($_firstname = ''){
			$this->mFirstName = $_firstname;
		}
		function setLastName($_lastname = ''){
			$this->mLastName = $_lastname;
		}
		function setEmailAddress($_email = ''){
			$this->mEmailAdd = $_email;
		}
		function setUserName($_userName = ''){
			$this->mUserName = $_userName;
		}
		function setPassword($_password = ''){
			$this->mPassword = $_password;
		}
		/******************** END: SETTER FUNCTIONS ***********************/

		/******************** START: GETTER FUNCTIONS ***********************/
		function getTravelersWithPendingInvitations($_isRegularStaff = false){
			$arrTraveler = array();

			if (!$_isRegularStaff){
				$sql = "SELECT DISTINCT travelerID, username, sendableID, 1 AS active " .
						"FROM " .
						"( " .
							"SELECT DISTINCT tblTraveler.travelerID, tblTraveler.username, tblTraveler.sendableID " .
							"FROM tblTraveler, tblInviteList, tblGroup " .
							"WHERE tblTraveler.travelerID = tblInviteList.travelerID " .
							"AND tblTraveler.active = 1 " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblTraveler.deactivated = 0 " .
							"AND tblInviteList.groupID = tblGroup.groupID " .
							"AND tblGroup.parentID = " . $this->mGroup->getGroupID() . " " .
							"AND tblInviteList.status = 2 " .
							" UNION " .
							"SELECT DISTINCT tblTraveler.travelerID, tblTraveler.username, tblTraveler.sendableID " .
							"FROM tblTraveler, tblInviteList " .
							"WHERE tblTraveler.travelerID = tblInviteList.travelerID " .
							"AND tblTraveler.active = 1 " .
							"AND tblTraveler.isSuspended = 0 " .
							"AND tblTraveler.deactivated = 0 " .
							"AND tblInviteList.status = 2 " .
							"AND tblInviteList.groupID =  " . $this->mGroup->getGroupID() .
						") AS tblTemp " .
						"ORDER BY username ASC";
			}
			else{
				$sql = "SELECT DISTINCT tblTraveler.travelerID, tblTraveler.username, tblTraveler.sendableID, 1 AS active " .
						"FROM tblTraveler, tblInviteList, tblGroup " .
						"WHERE tblTraveler.travelerID = tblInviteList.travelerID " .
						"AND tblTraveler.active = 1 " .
						"AND tblTraveler.isSuspended = 0 " .
						"AND tblTraveler.deactivated = 0 " .
						"AND tblInviteList.groupID = tblGroup.groupID " .
						"AND tblGroup.groupID = " . $this->mGroup->getGroupID() . " " .
						"AND tblInviteList.status = 2 " .
						"ORDER BY tblTraveler.username ASC";
			}
			$this->mRs->Execute($sql);

			while($mData = mysql_fetch_assoc($this->mRs->Resultset())){
				$mTraveler = new Traveler;
				$mTraveler->setTravelerData($mData);
				$arrTraveler[] = $mTraveler;
			}

			return $arrTraveler;
		}

		function getSubGroupIDs(){
			return $this->mSubGroupIDs;
		}
		
		function getRegisteredInvites($rowslimit=NULL){
			$limitstr = "";
			if (NULL != $rowslimit){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }
			$gIDs = "(".implode(',',$this->mSubGroupIDs).")";
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.*
					FROM tblTraveler, tblInviteList
					WHERE tblInviteList.groupID IN $gIDs
						AND tblInviteList.status=0
						AND tblTraveler.travelerID=tblInviteList.travelerID
						AND tblTraveler.active = 1 
						AND tblTraveler.isSuspended = 0
						AND tblTraveler.deactivated = 0
						AND tblTraveler.travelerID
					GROUP BY tblTraveler.travelerID
					ORDER BY tblTraveler.username
					$limitstr";
			//echo "<br /><br />".$sql."<br /><br />";
			$this->mRs->Execute($sql);

			
			$registeredInvites = array();
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
            	try {
					$travelerData = array();
					$travelerData["travelerID"] = $recordset['travelerID'];
					$travelerData["active"] = $recordset['active'];
					$travelerData["isSuspended"] = $recordset['isSuspended'];
		 			$travelerData["currlocationID"] = $recordset['currlocationID'];
		 			$travelerData["sendableID"] = $recordset['sendableID'];
		 			$travelerData["username"] = $recordset['username'];
		 			$travelerData["password"] = $recordset['password'];
		 			$travelerData["showsteps"] = $recordset['showsteps'];
					$travelerData["deactivated"] = $recordset['deactivated'];

					$traveler = new Traveler;
					$traveler-> setTravelerData($travelerData);

					// set profile data in array and set to profile; minimize DB query
					$profileData = array();								
					$profileData["travelerID"] = $recordset['travelerID'];
					$profileData["username"] = $recordset['username'];
					$profileData["firstname"] = $recordset['firstname'];
		 			$profileData["lastname"] = $recordset['lastname'];
		 			$profileData["email"] = $recordset['email'];
		 			$profileData["gender"] = $recordset['gender'];
		 			$profileData["htlocationID"] = $recordset['htlocationID'];
		 			$profileData["currlocationID"] = $recordset['currlocationID'];
		 			$profileData["address1"] = $recordset['address1'];
		 			$profileData["address2"] = $recordset['address2'];
		 			$profileData["phone"] = $recordset['phone'];
		 			$profileData["travelertypeID"] = $recordset['travelertypeID'];
		 			$profileData["birthdate"] = $recordset['birthdate'];
		 			$profileData["interests"] = $recordset['interests'];
		 			$profileData["shortbio"] = $recordset['shortbio'];
		 			$profileData["iculture"] = $recordset['iculture'];
		 			$profileData["dateregistered"] = $recordset['dateregistered'];
		 			$profileData["latestlogin"] = $recordset['latestlogin'];
		 			$profileData["views"] =  $recordset['views'];
		 			$profileData["intrnltravel"] = $recordset['intrnltravel'];
		 			$profileData["istripnexttwoyears"] = $recordset['istripnexttwoyears'];
		 			$profileData["income"] =	 $recordset['income'];
		 			$profileData["otherprefertravel"] =	 $recordset['otherprefertravel'];
		 			$profileData["ispromosend"] = $recordset['ispromosend'];
		 			$profileData["signature"] = $recordset['signature'];

					$profile = new TravelerProfile;
					$profile->setProfileData($profileData);

					$traveler->setTravelerProfile($profile);	
					
					$registeredInvites[] = $traveler;							
                }catch (Exception $e) {
                	throw $e;
               	}
        	}
			
			$sql2 = "SELECT FOUND_ROWS() AS `invites_count`";
			$this->mRs->Execute($sql2);
			
			$recordset = mysql_fetch_array($this->mRs->Resultset());
			
			return array($registeredInvites,$recordset["invites_count"]);
		}
		
		function getNonRegisteredInvites($rowslimit=NULL){
			$limitstr = "";
			if (NULL != $rowslimit){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }
			$gIDs = "(".implode(',',$this->mSubGroupIDs).")";
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblEmailInviteList.email
					FROM tblEmailInviteList
					WHERE tblEmailInviteList.groupID IN $gIDs
					GROUP BY tblEmailInviteList.email
					ORDER BY tblEmailInviteList.email
					$limitstr";
			//echo "<br /><br />".$sql."<br /><br />";
			$this->mRs->Execute($sql);
			$nonRegisteredInvites = array();
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
				$nonRegisteredInvites[] = $recordset["email"];
			}
			
			$sql2 = "SELECT FOUND_ROWS() AS `invites_count`";
			$this->mRs->Execute($sql2);
			
			$recordset = mysql_fetch_array($this->mRs->Resultset());
			
			return array($nonRegisteredInvites,$recordset["invites_count"]);
		}
		
		function getGroupNonRegisteredInvitesArray($rowslimit=NULL){
			$limitstr = "";
			if (NULL != $rowslimit){
                $offset   = $rowslimit->getOffset();
                $rows     = $rowslimit->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }
			$gIDs = "(".implode(',',$this->mSubGroupIDs).")";
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblEmailInviteList.email, tblEmailInviteList.firstname, tblEmailInviteList.lastname
					FROM tblEmailInviteList
					WHERE tblEmailInviteList.groupID IN $gIDs
					GROUP BY tblEmailInviteList.email
					ORDER BY tblEmailInviteList.email
					$limitstr";
			//echo "<br /><br />".$sql."<br /><br />";
			$this->mRs->Execute($sql);
			$nonRegisteredInvites = array();
			while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
				$nonRegisteredInvites[] = array($recordset["email"],$recordset["firstname"],$recordset["lastname"]);
			}
			
			$sql2 = "SELECT FOUND_ROWS() AS `invites_count`";
			$this->mRs->Execute($sql2);
			
			$recordset = mysql_fetch_array($this->mRs->Resultset());
			
			return array($nonRegisteredInvites,$recordset["invites_count"]);
		}

		/******************** END: GETTER FUNCTIONS ***********************/

		function addNewTraveler(){
			// insert to tblSendable
			$sql = "INSERT INTO tblSendable (sendableType) VALUES (1)";
			$this->mRs->Execute($sql);

			$sendableId = $this->mRs->GetCurrentID();
			
			// edits ianne: added dateregistered for inserting 01/28/2009
			// insert to tblTraveler
			$sql2 = "INSERT INTO tblTraveler (sendableID, username, password, firstname, lastname, email, active, dateregistered) " .
					"VALUES (" . $sendableId . ", '" . $this->mUserName . "', '" . $this->mPassword . "', '". $this->mFirstName .
					"', '" . $this->mLastName . "', '" . $this->mEmailAdd . "', 1, now())";
			$this->mRs2->Execute($sql2);

			$travelerId = $this->mRs2->getCurrentID();

			$sql = "INSERT INTO tblPrivacyPreference (travelerID, preferencetype, preference) " .
					"VALUES (" . $travelerId . ", 18, 5)";
			$this->mRs->Execute($sql);

			$sql3 = "INSERT INTO tblTravelerRanking SET travelerID = {$travelerId}";
			$this->mRs->Execute($sql3);

			return $travelerId;
		}

		/******************** START: STATIC FUNCTIONS ***********************/
		static function assignAsParentStaff($_arrGroupId = array(), $_travelerId = 0){
			try{
				$mConn 	= new Connection;
				$mRs	= new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$lstGroupId = implode(",", $_arrGroupId);
			$sql = "UPDATE tblGrouptoFacilitator " .
					"SET status = 1 " .
					"WHERE groupID IN (" . $lstGroupId . ") " .
					"AND travelerID = " . $_travelerId . " " .
					"AND type = 1";
			$mRs->Execute($sql);
		}
		static function getTravelerByEmail($_email = ''){
			try{
				$mConn 	= new Connection;
				$mRs	= new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "SELECT travelerID, sendableID, username " .
					"FROM tblTraveler " .
					"WHERE isSuspended = 0 " .
					"AND deactivated = 0 " .
					"AND email like '" . trim($_email) . "'";
			$mRs->Execute($sql);

			$mData = array();
			$mData['email']			= trim($_email);

			if (0 < $mRs->Recordcount()){				
				$mData['username']		= $mRs->Result(0, "username");
				$mData['sendableID']	= $mRs->Result(0, "sendableID");
				$mData['travelerID']	= $mRs->Result(0, "travelerID");
			}	
			$mTraveler = new Traveler;
			$mTraveler->setTravelerData($mData);

			return $mTraveler;
		}
		static function removeAsParentStaff($_arrGroupId = array(), $_travelerId = 0){
			try{
				$mConn 	= new Connection;
				$mRs	= new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$lstGroupId = implode(",", $_arrGroupId);
			$sql = "UPDATE tblGrouptoFacilitator " .
					"SET status = 0 " .
					"WHERE groupID IN (" . $lstGroupId . ") " .
					"AND travelerID = " . $_travelerId . " " .
					"AND type = 1";
			$mRs->Execute($sql);
		}
		static function getInvitationDate($_groupId = 0, $_travelerId = 0){
			try{
				$mConn = new Connection;
				$mRs = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "SELECT DISTINCT inviteDate " .
					"FROM tblInviteList " .
					"WHERE groupID = " . $_groupId . " " .
					"AND travelerID = " . $_travelerId . " " .
					"ORDER BY inviteDate DESC";
			$mRs->Execute($sql);

			if (0 < $mRs->Recordcount())
				return $mRs->Result(0, "inviteDate");
			else
				return null;
		}
		static function getDesignationDate($_travelerId = 0, $_groupId = 0){
			require_once('gaLogs/Reader/Class.Reader.php');
			$mReader		= new Reader();

			try{
				$mConn = new Connection;
				$mRs  = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$sql =	"SELECT _EXECUTION_DATE_ " .
					"FROM Travel_Logs.tblGrouptoFacilitator " .
					"WHERE _COMMAND_ = 'INSERT' " .
					"AND _NEW_travelerID = " . $_travelerId . " " .
					"AND _NEW_groupID = " . $_groupId . " " .
					"ORDER BY _EXECUTION_DATE_ DESC " .
					"LIMIT 1";
			$mRs = $mReader->getLogsBySql($sql);

			if (0 < $mRs->retrieveRecordCount()){
				$mRecord = $mRs->retrieveRow();
				return $mRecord['_EXECUTION_DATE_'];
			}
		}
		function getDeniedStaffInvitations($_group = null){
			require_once('gaLogs/Reader/Class.Reader.php');
			$mReader		= new Reader();

			try{
				$mConn = new Connection;
				$mRs  = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$arrGroupId = array();
			$arrGroupId[] = $_group->getGroupID();

			// get subgroups of parent group
			$mSubGroups = $_group->getAllSubGroups();
			foreach ($mSubGroups as $each)
				$arrGroupId[] = $each->getGroupID();
			$lstGroupId = implode(",", $arrGroupId);

			$sql =	"SELECT _OLD_groupID as groupID, _DOER_ID_ as travelerID " .
					"FROM Travel_Logs.tblInviteList " .
					"WHERE _OLD_status = 2 " .
					"AND _NEW_status = 1 " .
					"AND _COMMAND_ = 'UPDATE' " .
					"AND _OLD_groupID IN ($lstGroupId)";
			$mRs = $mReader->getLogsBySql($sql);

			$arrDenied = array();
			if(0 < $mRs->retrieveRecordCount()){
				while(!$mRs->EOF()){
					$arrDenied[] = $mRs->retrieveRow();
					$mRs->moveNext();
				}
			}

			return $arrDenied;
		}
		function getAcceptedStaffInvitations($_group = null){
			require_once('gaLogs/Reader/Class.Reader.php');
			$mReader		= new Reader();

			try{
				$mConn = new Connection;
				$mRs  = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$arrGroupId = array();
			$arrGroupId[] = $_group->getGroupID();

			// get subgroups of parent group
			if( 2 == $_group->getDiscriminator() ){
				$mSubGroups = $_group->getAllSubGroups();
				foreach ($mSubGroups as $each)
					$arrGroupId[] = $each->getGroupID();
			}
			$lstGroupId = implode(",", $arrGroupId);

			$sql =	"SELECT _NEW_groupID as groupID, _DOER_ID_ as doerID, _NEW_travelerID as travelerID " .
					"FROM Travel_Logs.tblGrouptoFacilitator " .
					"WHERE _NEW_status = 1 " .
					"AND _COMMAND_ = 'INSERT' " .
					"AND _NEW_groupID IN ($lstGroupId) " .
					" UNION " .
					"SELECT _NEW_groupID as groupID, _DOER_ID_ as doerID, _NEW_travelerID as travelerID " .
					"FROM Travel_Logs.tblGrouptoFacilitator " .
					"WHERE _NEW_status = 1 " .
					"AND _COMMAND_ = 'UPDATE' " .
					"AND _OLD_groupID IN ($lstGroupId)";
			$mRs = $mReader->getLogsBySql($sql);

			$arrDenied = array();
			if(0 < $mRs->retrieveRecordCount()){
				while(!$mRs->EOF()){
					$mData = $mRs->retrieveRow();
					if ($mData['doerID'] == $mData['travelerID'])
						$arrDenied[] = $mData;
					$mRs->moveNext();
				}
			}

			return $arrDenied;
		}

		// checks if username is already taken
		static function isUserNameTaken($_username = ''){
			try{
				$mConn = new Connection;
				$mRs  = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "SELECT travelerID " .
					"FROM tblTraveler " .
					"WHERE trim(username) LIKE '" . trim($_username) . "' AND deactivated = 0";
			$mRs->Execute($sql);

			if (0 < $mRs->Recordcount())
				return true;
			else
				return false;
		}

		// get information for the email notification
		static function getConfigPerServer($_serverName = ''){
			try{
				$mConn = new Connection;
				$mRs  = new Recordset($mConn);
			}
			catch(exception $e){
				throw $e;
			}

			$sql = "SELECT emailName, adminEmail, siteName FROM tblConfig WHERE trim(serverName) LIKE '" . $_serverName . "'";
			$mRs->Execute($sql);

			$mData = array();
			if (0 < $mRs->Recordcount()){
				$mData['name']		= $mRs->Result(0, "emailName");
				$mData['email']		= $mRs->Result(0, "adminEmail");
				$mData['siteName']	= $mRs->Result(0, "siteName");
			}

			return $mData;
		}
		/******************** END: STATIC FUNCTIONS ***********************/		
	}
?>