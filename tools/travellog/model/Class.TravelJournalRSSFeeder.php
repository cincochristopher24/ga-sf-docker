<?php
/*
 * Created on 12 7, 06
 * 
 * @author Kerwin Gordo
 * Purpose: creates rss feed for a travel journal
 * 
 */
 
 require_once('Class.Traveler.php');
 require_once('Class.Connection.php');
 
 class TravelJournalRSSFeeder {
 	
 	 	
 	/**
 	 * function to output rss content for a Journal. This will be called if a new travellog is added or
 	 * 		updated. This function will just recreate all the journal entries for the journal specified and
 	 * 		overwrites the old file.
 	 *  @param Travel journal
 	 */
 	function publish($journal) {
 		$traveler = $journal->getTraveler();
 		
 		$travellogs = array();						// the array to hold all the logs for this journal
 		
 		// get all the travellogs for a journal/trip
 		$trips = $journal->getTrips();
 		
 		if (NULL == $trips)
 			return;
 		
 		foreach($trips as $trip){
 			$triptlogs = $trip->getTravellogs();
 			if (null != $triptlogs)
 				$travellogs = array_merge($travellogs,$triptlogs);	
 		} 		
 		
 		
 		// sort the travellogs according to date (still necessary? originally sorted by travellogID)
 		
 		
 		//form the rss content
 		$travellogs = array_reverse($travellogs);
 		
  		if (0 == count($travellogs))
  			return ;
  			
 		ob_start();
 		
 		
 		header('Content-type: text/xml');
 		echo '<?xml version="1.0" encoding="utf-8"?>';
 		echo '<rss version="2.0">';
 		echo '<channel>';
 		echo '<title>' . $journal->getTitle() .  '</title>';
 		echo '<link>http://' . $_SERVER['SERVER_NAME'] .  '/journal.php</link>';
 		echo '<description>' . $journal->getDescription() . '</description>';
 		echo '<lastBuildDate>' . date('D, j M Y H:i:s',time())  . ' PST</lastBuildDate>';
        echo '<language>en-us</language>';
        
        foreach($travellogs as $tlog){
        	// count photos
        	$photos = $tlog->getPhotos();
        	$readMoreStr = 'Read More';
        	if (0 < count($photos))
        		$readMoreStr .= '/View ' . count($photos) . ' Photo';
        	
        	if (1 < count($photos))	
        	    $readMoreStr .= 's';
        	        	
        	echo '<item>'; 
        	echo '<title>' . $tlog->getTitle() . '</title>';
        	echo '<link>http://' . $_SERVER['SERVER_NAME'] . '/travellog.php?action=view&amp;travellogID=' . $tlog->getTravelLogID()  . '</link>';
        	echo '<pubDate>' .   date('D, j M Y H:i:s',strtotime($tlog->getLogDate()))  . ' PST' . '</pubDate>';
        	$location = $tlog->getTrip()->getLocation();
        	$locStr =  '<a href="http://' . $_SERVER['SERVER_NAME'] . '/travellog.php?action=view&travellogID=' . $tlog->getTravelLogID()  . '">' . $location->getName() . ', ' . $location->getCountry()->getName() . '</a>';        	
        	$dateStr = date('D, j M Y',strtotime($tlog->getLogDate()));
        	$moreStr = '<a href="http://' . $_SERVER['SERVER_NAME'] . '/travellog.php?action=view&travellogID=' . $tlog->getTravelLogID() . '">' . $readMoreStr . '</a>'; 
        	echo '<description><![CDATA[' . '<p>' . $locStr . ', ' . $dateStr . '<br />' . htmlspecialchars($this->truncateParagraph($tlog->getDescription())) .  '</p><p>' . $moreStr . '</p>]]> </description>';
        	echo '</item>';
        }  
 		 			
 		echo '</channel>';
 		echo '</rss>';
 		
 		 		
 		$output = ob_get_contents();
 		ob_end_clean();
 		 		
 		echo $output;
 		
 		/*if (!$handle = fopen('/home/kerwin/workspace/php/goabroad.net/tlog.xml','w'))
 		{
 			echo 'cannot create file';
 		}
 		fwrite($handle,$output); 		
 		fclose($handle);
 		*/
 		 		
 	}
 	
 	
 	function truncateParagraph($prgp){
 		$wordLimit = 60;
 		
 		$prgpAr = explode(" ",$prgp);
 		
 		if (60 >= count($prgpAr))
 			$newPrgp = $prgp;
 		else {
 			$prgpAr = array_slice($prgpAr,0,$wordLimit);
 			$newPrgp = implode(" ",$prgpAr);
 		}	
 		
 		return $newPrgp;
 	}
 	
 } 
  
?>
