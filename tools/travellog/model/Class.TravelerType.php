<?
	
/**
 * Created on Nov 23, 2006
 * Created by: Joel C. LLano
 */
	
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	

	class TravelerType{
		
		private $mConn         = NULL;
        private $mRs           = NULL;
        
		private $travelertypeid 		= 0;
		private $travelertype			= NULL;
		
		function TravelerType($travelertypeid = 0){
 				
 				$this->travelertypeid = $travelertypeid;
 				
 				try {
	 				$this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}

			 			if ($travelertypeid > 0){
			 				
					 				$sql = "SELECT * from tblTravelerType where travelertypeID = '$travelertypeid' ";
						 			$this->mRs->Execute($sql);
					 			
					 				if ($this->mRs->Recordcount() == 0){
					 					throw new Exception("Invalid TravelerType ID");	// ID not valid so throw exception
					 				}
					 				
					 				// set values to its attributes
									$this->travelertype 		= stripslashes($this->mRs->Result(0,"travelertype")); 
				 		}
						
						
 			}
		
		public static function getAllTravelerType(){
				try {
	 				$conn = new Connection();
					$rs   = new Recordset($conn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT * from tblTravelerType  order by travelertype ";
				$rs->Execute($sql);
				
				$travelertype_s = array();
							
				while ($rsar = mysql_fetch_array($rs->Resultset())) {
					$travelertype_s[] = new TravelerType($rsar['travelertypeID']);
				}
				return $travelertype_s;
		}
		
		
		/**
		 * Use 	__call function to dynamically call getter and setters
		 */
		
		function __call($method, $arguments) {
				//prefix ->get and set <- to lowercase
		        $prefix 		= strtolower(substr($method, 0, 3));
		        $property 	= strtolower(substr($method, 3));
		
		        if (empty($prefix) || empty($property)) {
		            return;
		        }
				
				//if client declare not in class property	
				/*
				if(!property_exists($this,$property)){					
					return "No Method";	
				}
				*/
				
		        if ($prefix == "get" && isset($this->$property)) {
		            return $this->$property;
		        }
		
		        if ($prefix == "set") {
		            $this->$property = $arguments[0];
		        }
    	}
		
		/**
         * CRUD Methods (Create, Update , Delete)
         */ 
        function Create(){
            
            $Aname  = addslashes($this->travelertype);
            
            $sql = "INSERT into tblTravelerType (travelertype) VALUES ('$Aname')";
            $this->mRs->Execute($sql);       
            $this->travelertypeID = $this->mRs->GetCurrentID();
        }
        
        function Update(){
            
            $Aname = addslashes($this->travelertype);
            
            $sql = "UPDATE tblTravelerType SET travelertype = '$Aname' WHERE travelertypeID = '$this->travelertypeid' ";
            $this->mRs->Execute($sql);
        }
        
        function Delete(){
            
            $sql = "DELETE FROM tblTravelerType WHERE travelertypeID = '$this->travelertypeid' ";        
            $this->mRs->Execute($sql);       
        }       
		
		
	}



?>