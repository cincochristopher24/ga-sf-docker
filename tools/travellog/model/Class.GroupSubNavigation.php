<?php
	require_once('Class.Decorator.php');
	class GroupSubNavigation extends Decorator
	{
		public function __construct($vars){
			//decide which model to use as core object
			if(array_key_exists('CONFIG',$GLOBALS)){
				//check if there was an assigned subnavigation for the current cobrand
				$coreObject = $this->getSubNavigationCore($GLOBALS['CONFIG']->getFolderName());
				//if none, use the cobrand class
				if(is_null($coreObject)){
					require_once('travellog/model/subnavigation/core/group/Class.COBRANDGroupSubNavigation.php');
					$coreObject = new COBRANDGroupSubNavigation($vars);
				}
			}
			else{
				//assume that we are not in a cobrand site
				//use the GANet class
				require_once('travellog/model/subnavigation/core/group/Class.GANETGroupSubNavigation.php');
				$coreObject = new GANETGroupSubNavigation($vars);
			}
			parent::__construct($coreObject);
		}
		
		private function getSubNavigationCore($folderName=''){
			$folderName = strtoupper($folderName);
			$arDir = explode('/',__FILE__);
			$strategyDir = implode('/', array_slice($arDir,0,count($arDir)-1)).'/subnavigation/core/group';
			$strategyFiles = scandir($strategyDir);
			$strategyFiles = array_slice($strategyFiles,2,count($strategyFiles));
			$strategyFileMap = array_flip($strategyFiles);
			$className = $folderName.'GroupSubNavigation'; 
			if(array_key_exists($folderName,$strategyFileMap)){
				require_once($strategyDir.'/'.$className.'php');
				return new $className();
			}
			return null;
		}
	}
?>