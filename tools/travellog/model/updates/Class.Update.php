<?php
	require_once('travellog/model/updates/Class.UpdateDoer.php');
	require_once('travellog/model/updates/Class.UpdateDoerType.php');

	class Update {
		
		private $mAction 		= null;
		private $mExecutionDate	= null;
		private $mDoer			= null;
		private $mRetrieverType	= null;
		private $mContextObject	= null;
		private $mContextLink	= null;
		private $mContextName	= null;
		private $mSubjectGroup	= null;
		private $mParentGroup	= null;
		
		private $mJournal		= null;
		
		private $mPhotoAlbum	= null;
		private $mPhotos		= null;
		private $mUaction		= null;
				
		public function Update(){}
		
		public function setAction($param=null){
			$this->mAction = $param;
		}
		
		public function setExecutionDate($param=null){
			$this->mExecutionDate = $param;
		}
		
		public function setDoer($param=null){
			$this->mDoer = $param;
		}
		
		public function setRetrieverType($param=null){
			$this->mRetrieverType = $param;
		}
		
		public function setRetriever($param=null){
			if( !is_null($param) ){
				$this->setRetrieverType(strtoupper(get_class($param)));
				$this->mRetriever = $param;
			}
		}
		
		public function setContextObject($param=null){
			$this->mContextObject = $param;
		}
		
		public function setContextLink($param=null){
			$this->mContextLink = $param;
		}
		
		public function setContextName($param=null){
			$this->mContextName = $param;
		}
		
		public function setSubjectGroup($param=null){
			$this->mSubjectGroup = $param;
		}
		
		public function setParentGroup($param=null){
			$this->mParentGroup = $param;
		}
		
		public function setJournal($journal){
			$this->mJournal = $journal;
		}
		
		public function setPhotoAlbum($photoalbum){
			$this->mPhotoAlbum = $photoalbum;
		}
		
		public function setPhotos($photos){
			$this->mPhotos = $photos;
		}
		
		public function setUAction($uaction){
			$this->mUaction = $uaction;
		}
		
		public function getAction(){
			return $this->mAction;
		}
		
		public function getExecutionDate(){
			return $this->mExecutionDate;
		}
		
		public function getDoer(){
			return $this->mDoer;
		}
		
		public function getRetrieverType(){
			return $this->mRetrieverType;
		}
		
		public function getRetriever(){
			return $this->mRetriever;
		}
		
		public function getContextObject(){
			return $this->mContextObject;
		}
		
		public function getContextLink(){
			return $this->mContextLink;
		}
		public function getContextName(){
			return $this->mContextName;
		}
		
		public function getSubjectGroup(){
			return $this->mSubjectGroup;
		}
		
		public function getParentGroup($param=null){
			return $this->mParentGroup;
		}
		
		public function getDoerType(){
			$retType = null;
			if( $this->mDoer && $this->mDoer->isTraveler() ){
				if( $this->mParentGroup->isSuperStaff($this->mDoer->getTraveler()->getTravelerID()) )
					$retType = UpdateDoerType::SUPER_STAFF;
				elseif( $this->mSubjectGroup->isRegularStaff($this->mDoer->getTraveler()->getTravelerID()))
					$retType = UpdateDoerType::REGULAR_STAFF;
				elseif( $this->mParentGroup->isMember($this->mDoer->getTraveler()) )
					$retType = UpdateDoerType::REGULAR_MEMBER;
			}
			elseif( 'group' == get_parent_class($this->mDoer) )
				$retType = UpdateDoerType::GROUP_ADMINISTRATOR;
			return $retType;
		}
		
		// journal feeds
		public function getJournal(){
			return $this->mJournal;
		}
		
		// photo updates
		public function getPhotoAlbum(){
			return $this->mPhotoAlbum;
		}
		
		public function getPhotos(){
			return $this->mPhotos;
		}
		
		public function getUAction(){
			return $this->mUaction;
		}
	}

?>