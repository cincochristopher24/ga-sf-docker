<?php
	
	require_once('Class.Template.php');
	require_once('Class.GaDateTime.php');
	require_once('Class.dbHandler.php');
	
	require_once('travellog/model/updates/Class.Update.php');
	require_once('travellog/model/updates/Class.UpdateAction.php');
	require_once('travellog/model/updates/Class.UpdateRetriever.php');

	abstract class GroupUpdates {
		
		protected $subjectGroup = null;
		protected $updates		= null;
		protected $retrievers 	= array();
		protected $parentGroup	= null;
		protected $options 		= array();
		protected $loggedTraveler = null;
		
		abstract function prepareUpdates($options = array());
		abstract function render();
		
		public function __construct($group){
			$this->subjectGroup = $group;
			$this->parentGroup	= is_null($tmp = $this->subjectGroup->getParent()) ? $this->subjectGroup : $tmp;
			$this->retrievers	= UpdateRetriever::getRetrievers();
		}
		
		public function setLoggedTraveler(Traveler $traveler){
			$this->loggedTraveler = $traveler;
		}
		
		public function getLoggedTraveler(){
			return $this->loggedTraveler;
		}
		
		protected function setOptions($options = array()){
			$defEndDate 	= GaDateTime::dbDateFormat() . ' 23:59:59';
			$defCoveredDays = isset($options['COVERED_DAYS']) ? $options['COVERED_DAYS'] : 7;
			$default = array(
				'LOGGED_TRAVELER'		=> null,
				'COVERED_DAYS'			=> $defCoveredDays,
				'MAX_UPDATES'			=> 10,
				'START_DATE'			=> !isset($options['START_DATE']) ? GaDateTime::dbDateFormat(GaDateTime::dateAdd($defEndDate,'d','-' . $defCoveredDays)) . ' 00:00:00' : $options['START_DATE'],
				'END_DATE'				=> !isset($options['END_DATE']) ? $defEndDate : $options['END_DATE'],
				'DENIED_INVITES_ONLY' 	=> false,
				'SORT_ORDER'			=> 'DESC',
				'VIEWER_TYPE'			=> 1
			);
			$this->options = array_merge($default, $options);
		}
		
		protected function getOptions(){
			return $this->options;
		}
		
		protected function getStaffedGroupIDs(){
			// looking in a sub-group's page
			if( $this->parentGroup->getGroupID() != $this->subjectGroup->getGroupID() ){
				return array($this->subjectGroup->getGroupID());
			}
			else{ // in parent group's 
				$db = new dbHandler;
				$isAdmin = $this->parentGroup->getAdministratorID() == $this->loggedTraveler->getTravelerID() || $this->parentGroup->isStaff($this->loggedTraveler->getTravelerID());
				
				$sql = 	"SELECT `tblGroup`.groupID " .
						"FROM tblGroup " . (!$isAdmin ? ', tblGrouptoFacilitator ' : '') .
						"WHERE isSuspended = 0 " .
						"AND parentID = " . $this->parentGroup->getGroupID() . " ";
				if( !$isAdmin ){
					$sql .= 'AND `tblGrouptoFacilitator`.`groupID` = `tblGroup`.`groupID` ' .
							'AND `tblGrouptoFacilitator`.`travelerID` = ' . GaString::makeSqlSafe($this->loggedTraveler->getTravelerID()) . ' ' .
							'AND `tblGrouptoFacilitator`.`status` = 1';
							
				}
				$rs = new iRecordset($db->execute($sql));
				if( !$rs->retrieveRecordcount() ) // not a member
					return array();					
				$groupIDs = $rs->retrieveColumn('groupID');
				if( $isAdmin )
					$groupIDs = array_merge($groupIDs, array($this->parentGroup->getGroupID()));
				return $groupIDs;
			}
		}
		
		protected function getInvolvedGroupIDs(){
			// looking in a sub-group's page
			if( $this->parentGroup->getGroupID() != $this->subjectGroup->getGroupID() ){
				return array($this->subjectGroup->getGroupID());
			}
			else{ // in parent group's 
				$db = new dbHandler;
				$isAdmin = $this->parentGroup->getAdministratorID() == $this->loggedTraveler->getTravelerID() || $this->parentGroup->isStaff($this->loggedTraveler->getTravelerID());
				
				$sql = 	"SELECT `tblGroup`.groupID " .
						"FROM tblGroup " . (!$isAdmin ? ', tblGrouptoTraveler ' : '') .
						"WHERE isSuspended = 0 " .
						"AND parentID = " . $this->parentGroup->getGroupID() . " ";
				if( !$isAdmin ){
					$sql .= 'AND `tblGrouptoTraveler`.`groupID` = `tblGroup`.`groupID` ' .
							'AND `tblGrouptoTraveler`.`travelerID` = ' . GaString::makeSqlSafe($this->loggedTraveler->getTravelerID()) . ' ';
				}
				
				$rs = new iRecordset($db->execute($sql));
				if( !$rs->retrieveRecordcount() ) // not a member
					return array();					
				$groupIDs = $rs->retrieveColumn('groupID');
				$groupIDs = array_merge($groupIDs, array($this->parentGroup->getGroupID()));
				return $groupIDs;
			}
		}
		
		protected function sortUpdates(){
			// sort according to dates
			$updateKeys = array_keys($this->updates);
			if( 'DESC' == $this->options['SORT_ORDER'] )
				rsort($updateKeys);
			else 
				sort($updateKeys);
			$temp = array();
			
			// limit updates
			$x = 1;
			foreach($updateKeys as $time){					
				if( $x > $this->options['MAX_UPDATES'] ){
					break;
				}
				
				if(isset($this->options['FEED_OWNER'])){
					if($this->options['FEED_OWNER'] == $this->updates[$time]->getDoer()->getTraveler()->getTravelerID()){
						$temp[$time] = $this->updates[$time];
						$x++ ;
					}	
				}else{
					$temp[$time] = $this->updates[$time];
					$x++ ;
				}			
				
			}
			$this->updates = $temp;
			return $this->updates;
		}
		
		protected function sortPhotoUpdatesFromMembers(){
			// sort according to dates
			$updateKeys = array_keys($this->updates);
			if( 'DESC' == $this->options['SORT_ORDER'] )
				rsort($updateKeys);
			else 
				sort($updateKeys);
			$temp = array();

			// limit updates
			$x = 1; $ids = array();
			foreach($updateKeys as $time){
				if( $x > $this->options['MAX_UPDATES'] ){
					break;
				}
				if( !in_array($this->updates[$time]->getDoer()->getTraveler()->getTravelerID(),$ids) ){
					$temp[$time] = $this->updates[$time];
					$ids[] = $this->updates[$time]->getDoer()->getTraveler()->getTravelerID();
					$x++ ;
				}
			}
			$this->updates = $temp;
			return $this->updates;
		}
		
		protected function sortPhotoUpdatesForGroup(){
			// sort according to dates
			$updateKeys = array_keys($this->updates);
			if( 'DESC' == $this->options['SORT_ORDER'] )
				rsort($updateKeys);
			else 
				sort($updateKeys);
			$temp = array();

			// limit updates
			$x = 1; $ids = array();
			foreach($updateKeys as $time){
				if( $x > $this->options['MAX_UPDATES'] ){
					break;
				}
				$id_action = $this->updates[$time]->getPhotoAlbum()->getPhotoAlbumID().'_'.
							 $this->updates[$time]->getUAction();
				if( !in_array($id_action,$ids) ){
					$temp[$time] = $this->updates[$time];
					$ids[] = $id_action;
					$x++ ;
				}
			}
			$this->updates = $temp;
			return $this->updates;
		}
		
		protected function sortDeletedPhotosUpdates(){
			
				// sort according to dates
				$updateKeys = array_keys($this->updates);
				if( 'DESC' == $this->options['SORT_ORDER'] )
					rsort($updateKeys);
				else 
					sort($updateKeys);
				$temp = array();

				// limit updates
				$x = 1; $ids = array();
				foreach($updateKeys as $time){
					if( $x > $this->options['MAX_UPDATES'] ){
						break;
					}
					
					$id_action_album =  $this->updates[$time]->getDoer()->getTraveler()->getTravelerID()."_".
								 		$this->updates[$time]->getUAction()."_".
								 		$this->updates[$time]->getPhotoAlbum()->getPhotoAlbumID();
								
					if( !in_array($id_action_album,$ids) ){
						$temp[$time] = $this->updates[$time];
						$ids[] = $id_action_album;
						$x++ ;
					}
				}
				$this->updates = $temp;
				return $this->updates;
		}
	}

?>