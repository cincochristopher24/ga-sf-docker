<?php
	
	class UpdateRetriever {
		
		private static $retrievers = array();
		
		private function __construct(){}
		
		public static function getRetrievers(){
			return self::$retrievers;
		}
		
		public static function registerRetrievers(){
			$arDir = explode(DIRECTORY_SEPARATOR,__FILE__);
			$strategyDir = implode(DIRECTORY_SEPARATOR, array_slice($arDir,0,count($arDir)-1)).DIRECTORY_SEPARATOR.'groupmembershipupdates';
			$strategyFiles = scandir($strategyDir);
			foreach($strategyFiles as $iFile){
				//check if it is a php file
				if('.php' == substr($iFile,strlen($iFile)-4, 4)){
					$comKey = strtoupper(str_replace(array('Class.','.php'),'',$iFile));
					$clsName = str_replace(array('Class.','.php'),'',$iFile);
					require_once('travellog/model/updates/groupmembershipupdates/'.$iFile);
					self::$retrievers[$comKey] = new $clsName();
				}
			}
		}	
	}
	UpdateRetriever::registerRetrievers();

?>