<?php

	class UpdateDoerType {
		
		const REGULAR_MEMBER 		= 1;
		const GROUP_ADMINISTRATOR	= 2;
		const REGULAR_STAFF			= 3;
		const SUPER_STAFF			= 4;
		const TRAVELER				= 5;
		
		public static function getLabel($type){
			$retVal = null;
			switch($type){
				case self::REGULAR_MEMBER :
					$retVal = 'Member';
					break;
				case self::REGULAR_STAFF :
					$retVal = 'Staff';
					break;
				case self::SUPER_STAFF :
					$retVal = 'Super Staff';
					break;
				case self::GROUP_ADMINISTRATOR :
					$retVal = 'Advisor';
					break;
				default;
			}
			return $retVal;
		}
	}

?>