<?php
	
	require_once('travellog/model/updates/Class.AbstractGroupUpdates.php');
	
	class JournalsFeed extends GroupUpdates {
		
		public function __construct($group){
			parent::__construct($group);
		}
		
		public function prepareUpdates($options = array()){
			
			$this->updates 			= array();
			$options['END_DATE'] 	= GaDateTime::dbDateTimeFormat();
			$options['START_DATE'] 	= GaDateTime::dbDateTimeFormat(strtotime('-1 day', strtotime($options['END_DATE'])));
			$this->setOptions($options);
			if( !is_null($this->options['LOGGED_TRAVELER']) )
				$this->loggedTraveler = $this->options['LOGGED_TRAVELER'];
			
			if( is_null($this->loggedTraveler) || 'Group' != get_parent_class($this->subjectGroup) ){
				return;
			}
			
			$allowedKeys = UpdateAction::getJournalsFeedActions();
			$groupIDs	= $this->getInvolvedGroupIDs();
			$start = microtime(true);
			foreach( $allowedKeys as $key ){
				if( !array_key_exists($key, $this->retrievers) )
					continue;
				$retriever = $this->retrievers[$key];
				$retrieveVars = array(
					'startDate' => $this->options['START_DATE'], 
					'endDate' 	=> $this->options['END_DATE'], 
					'group' 	=> $this->subjectGroup,
					'parentGroup' => $this->parentGroup,
					'groupIDs'	=> $groupIDs
				);
				$st = microtime(true);
				$retrievedUpdates = $retriever->retrieve($retrieveVars);
				$et = microtime(true);
				
				if( array_key_exists('dbhDebug', $_GET)){
					//echo "$key START : {$st} END: {$et} DIFF: " . ($et-$st) . '<br/>';
				}
				
				
				if(!empty($retrievedUpdates)){
					$this->updates += $retrievedUpdates;
				}
			}
			$endRetrieval = microtime(true);
			$this->sortUpdates();
			$end = microtime(true);
			if( array_key_exists('dbhDebug', $_GET)){
				//echo "RETRIEVAL START : {$start} END: {$endRetrieval} DIFF: " . ($endRetrieval-$start) . '<br/>';
				//echo "MAIN START : {$start} END: {$end} DIFF: " . ($end-$start) . '<br/>';
				//exit;
			}
			return $this->updates;
			
		} // end prepareUpdates
		
		public function render(){   
			$tpl = new Template();
			//$tpl->set("feeds",$this->updates);
			//$tpl->set("sortOrder",$this->options['SORT_ORDER']);
			//$tpl->set_path('travellog/views/');
			return $tpl->out('tpl.ViewJournalsFeed.php');
		}		
	}

?>