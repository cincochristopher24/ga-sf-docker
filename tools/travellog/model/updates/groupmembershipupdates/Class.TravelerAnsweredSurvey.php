<?php

	// the action for posting a reply in a discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');
	
	require_once('travellog/model/formeditor/Class.SurveyForm.php');

	class TravelerAnsweredSurvey implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			if( empty($groupIDs) ) 
				return array();
				
			$sql = 	'SELECT logView.* ' .
						',CAST(logView.`_EXECUTION_DATE_` AS DATE ) as eDate ' .
						' <$ ,`tblSurveyParticipant`.`surveyFormID` $> ' .
					'FROM `Travel_Logs`.`tblSurveyFormAnswer` as logView ' .
						' <$ ,`Travel_Logs`.`tblSurveyParticipant` $> ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_NEW_surveyParticipantID` = <$ `tblSurveyParticipant`.`surveyParticipantID` $> ' .
						'AND logView.`_COMMAND_` = \'INSERT\' ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
						'AND logView.`_DOER_ID_` <> ' . GaString::makeSqlSafe($this->parentGroup->getAdministratorID()) . ' ' .
						' <$ AND `tblSurveyParticipant`.`groupID` IN (\'' . join('\', \'', $groupIDs) . '\') $> '  .
					'ORDER BY `_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forGROUPRECENTACTIVITY($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$surveyForm = new SurveyForm($row['surveyFormID']);
					$traveler	= new Traveler($row['_DOER_ID_']);
					
					if( $traveler->getTravelerID() != $this->parentGroup->getAdministratorID() && $surveyForm->isParticipantVisible() ){
						$update = new Update;
						$update->setExecutionDate($row['_EXECUTION_DATE_']);
						$update->setDoer(new UpdateDoer($traveler));
						$update->setRetriever($this);
						$update->setContextLink('/surveystats.php?frmID=' .  $surveyForm->getSurveyFormID() . '&gID=' . $surveyForm->getGroupID());
						$update->setContextName('Survey Statistics');
						$update->setSubjectGroup($this->subjectGroup);
						$update->setParentGroup($this->parentGroup);
						$update->setAction(UpdatesComposer::compose(
							array(
								'action' 			=> UpdateAction::TRAVELER_ANSWERED_SURVEY,
								'surveyTitle'		=> $surveyForm->getName(),
								'groupName'			=> 'group',
								'linktofeed'		=> $update->getContextLink()
							), 'tpl.GroupMembershipUpdates.php'));
						$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;
					}
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>