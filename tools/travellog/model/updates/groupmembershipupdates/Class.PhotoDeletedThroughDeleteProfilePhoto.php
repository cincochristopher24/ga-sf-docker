<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');

	class PhotoDeletedThroughDeleteProfilePhoto implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		private $albumID        = null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			//if( empty($groupIDs) ) 
			//	return array();
			
			/**EDITED TO INCLUDE ALL PHOTOS DELETED BY TRAVELER PERMANENTLY**/
			/*$sql = 	'SELECT logView.* ' . 
					'FROM `Travel_Logs`.`tblTravelertoPhoto` as logView ' . 
					'WHERE 1=1 ' . 
						'AND logView._COMMAND_ = \'DELETE\' ';
			$rs1 = $this->reader->getLogsBySql($sql);*/
			
			$sql = 	'SELECT logView.* ' . 
					'FROM `Travel_Logs`.`tblPhoto` as logView ' . 
					'WHERE 1=1 ' . 
						'AND logView._COMMAND_ = \'DELETE\' ';
			$rs1 = $this->reader->getLogsBySql($sql);
			
			$this->albumID = $params['ALBUM_ID'];
			$sql = 	'SELECT DISTINCT(logView.`_DOER_ID_`), COUNT(logView.`_OLD_photoID`) as cnt, MAX(logView._EXECUTION_DATE_) as _EXECUTION_DATE_ ' .
					'FROM `Travel_Logs`.`tblPhotoAlbumtoPhoto` as logView ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'DELETE\' ' .
						'AND logView.`_OLD_photoID` IN (\'' . join('\', \'', $rs1->retrieveColumn('_OLD_photoID')) . '\') ' .
						'AND logView.`_OLD_photoalbumID` = \'' .$this->albumID. '\' ' .
						//'AND logView.`_EXECUTION_DATE_` IN (\'' . join('\', \'', $rs1->retrieveColumn('_EXECUTION_DATE_')) . '\') ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
					'GROUP BY `logView`.`_DOER_ID_` ORDER BY logView.`_EXECUTION_DATE_` DESC';
			
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forMEMBERDELETEDPHOTOS($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$traveler = new Traveler($row['_DOER_ID_']);
					$update = new Update;
					$update->setExecutionDate($row['_EXECUTION_DATE_']);
					$update->setDoer(new UpdateDoer($traveler));
					$update->setRetriever($this);
					$update->setSubjectGroup($this->subjectGroup);
					$update->setParentGroup($this->parentGroup);
					$update->setContextLink('/collection.php?type=group&ID='.$this->subjectGroup->getGroupID().'&context=photoalbum&genID='.$this->albumID);
					$update->setPhotoAlbum(new PhotoAlbum($this->albumID));
					$update->setUAction(UpdateAction::PHOTO_DELETED_THROUGH_DELETE_PROFILE_PHOTO);
					$update->setAction(UpdatesComposer::compose(
						array(
							'action' 	=> UpdateAction::PHOTO_DELETED_THROUGH_DELETE_PROFILE_PHOTO,
							'photocnt'	=> $row['cnt']
						), 'tpl.PhotoUpdates.php'));
					$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>