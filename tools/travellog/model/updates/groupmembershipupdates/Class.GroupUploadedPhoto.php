<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');

	class GroupUploadedPhoto implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			//$endDate 		= $params['endDate'];
			//$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			//if( empty($groupIDs) ) 
			//	return array();
			
			$albumID 	= $params['ALBUM_ID'];
			$exec_date 	= $params['EXEC_DATE']; 
			
			$startDate = GaDateTime::dbDateFormat(strtotime($exec_date));
			$endDate   = GaDateTime::dbDateFormat(strtotime('+1 day',strtotime($exec_date)));
			$groupID = $this->subjectGroup->getGroupID();
			$db  = new dbHandler('Travel_Logs');			
			$sql = "SELECT `travelerID` FROM `Travel_Logs`.`tblGrouptoTraveler` WHERE `groupID` = ".$groupID;
			$rs1 = new iRecordset($db->execute($sql));
			
			$sql = "SELECT t1.photoID 
					FROM `Travel_Logs`.`tblTravelertoPhoto` as t1, `Travel_Logs`.`tblPhotoAlbumtoPhoto` as t2 
					WHERE t1.`travelerID` IN (" . join(",", $rs1->retrieveColumn('travelerID')) . ") 
						AND t1.photoID = t2.photoID";
			$rs2 = new iRecordset($db->execute($sql));
			
			/*$sql = "SELECT t1.`photoID` 
					FROM `Travel_Logs`.`tblPhotoAlbumtoPhoto` as t1, `Travel_Logs`.`tblPhotoAlbum` as t2 
					WHERE t1.`photoalbumID` = t2.`photoalbumID` 
						AND t2.`groupID` = 0 
						AND t2.`creator` IN (" . join(",", $rs1->retrieveColumn('travelerID')) . ") ";
			$rs3 = new iRecordset($db->execute($sql));*/
			
			$sql = "SELECT t1.photoID 
					FROM `Travel_Logs`.`tblTravelLogtoPhoto` as t1, `Travel_Logs`.`tblTravelLog` as t2, `Travel_Logs`.`tblTravel` as t3, `Travel_Logs`.`tblPhotoAlbumtoPhoto` as t4   
					WHERE t1.`travellogID` = t2.`travellogID` 
						AND t1.`photoID` = t4.`photoID` 
						AND t2.`travelID` = t3.`travelID` 
						AND t3.`travelerID` IN (" . join(",", $rs1->retrieveColumn('travelerID')) . ") ";
			$rs4 = new iRecordset($db->execute($sql));
			
			$pIDs = array_merge($rs2->retrieveColumn('photoID'),$rs4->retrieveColumn('photoID'));
			
			$sql = 	'SELECT DISTINCT(logView.`_NEW_photoID`), logView.*, CAST(logView._EXECUTION_DATE_ AS DATE ) as eDate ' .
					'FROM `Travel_Logs`.`tblPhotoAlbumtoPhoto` as logView <$ ,Travel_Logs.tblPhotoAlbumtoPhoto $> ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'INSERT\' ' .
						'AND logView.`_NEW_photoalbumID` = \''.$albumID.'\' ' .
						'AND logView.`_NEW_photoID` = <$ tblPhotoAlbumtoPhoto.`photoID` $> ' . 						
						'AND logView.`_NEW_photoID` NOT IN (\'' . join('\', \'', $pIDs ). '\') ' .
						'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forGROUPRECENTPHOTOS($rs){
			$arr = array();
			$photos = array();
			$time = 0;
			
			foreach($rs as $row){
				$time = strtotime($row['_EXECUTION_DATE_']);
				$photos[] = new Photo(new PhotoAlbum($row['_NEW_photoalbumID']),$row['_NEW_photoID']);
			}
			
			$update = new Update;
			$update->setRetriever($this);
			$update->setPhotos($photos);					
			$update->setUAction(UpdateAction::GROUP_UPLOADED_PHOTO);
			$update->setAction(UpdatesComposer::compose(
				array(
					'action' 	=> UpdateAction::GROUP_UPLOADED_PHOTO,
					'photocnt'	=> count($photos)
				), 'tpl.PhotoUpdates.php'));			
			$arr[$time] = $update;
			
			return $arr;
		}
		
	}

?>