<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');

	class MemberAddedPhotoToProfile implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		// function to retrieve the NUMBER of photos uploaded by a traveler to his profile picture
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			
			//if( empty($groupIDs) ) 
			//	return array();
			
			$groupID = $this->subjectGroup->getGroupID();
			$administratorID = $this->subjectGroup->getAdministratorID();
			$db  = new dbHandler('Travel_Logs');			
			$sql = "SELECT `travelerID` 
					FROM `Travel_Logs`.`tblGrouptoTraveler` 
					WHERE `groupID` = $groupID
						AND `travelerID` != $administratorID";
			$rs1 = new iRecordset($db->execute($sql));
			
			if( !$rs1->retrieveRecordcount() ){
				return array();
			}
			
			$sql = 	'SELECT DISTINCT(logView.`_DOER_ID_`), COUNT(logView.`_NEW_travelerID`) as cnt, MAX(logView._EXECUTION_DATE_) as _EXECUTION_DATE_ ' .
					'FROM `Travel_Logs`.`tblTravelertoPhoto` as logView ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'INSERT\' ' .
						'AND logView.`_NEW_travelerID` IN (\'' . join('\', \'', $rs1->retrieveColumn('travelerID')) . '\') ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
					'GROUP BY logView.`_EXECUTION_DATE_` ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}

		private function forMEMBERRECENTPHOTOS($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$traveler = new Traveler($row['_DOER_ID_']);
					$update = new Update;
					$update->setExecutionDate($row['_EXECUTION_DATE_']);
					$update->setDoer(new UpdateDoer($traveler));
					$update->setRetriever($this);
					$update->setSubjectGroup($this->subjectGroup);
					$update->setParentGroup($this->parentGroup);
					//$update->setPhotoAlbum(new PhotoAlbum($row['_NEW_photoalbumID']));
					//$update->setPhotos($photos);
					$update->setAction(UpdatesComposer::compose(
						array(
							'action' 	=> UpdateAction::MEMBER_ADDED_PHOTO_TO_PROFILE,
							'photocnt'	=> $row['cnt']
						), 'tpl.PhotoUpdates.php'));
					$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>