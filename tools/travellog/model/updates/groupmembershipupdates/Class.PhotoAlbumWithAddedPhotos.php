<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');

	class PhotoAlbumWithAddedPhotos implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			//if( empty($groupIDs) ) 
			//	return array();
			$groupID = $this->subjectGroup->getGroupID();
			$sql = 	'SELECT DISTINCT(logView.`_NEW_photoalbumID`), logView.`_DOER_ID_`, MAX(logView.`_EXECUTION_DATE_`) AS _EXECUTION_DATE_ ' .
					'FROM `Travel_Logs`.`tblPhotoAlbumtoPhoto` as logView <$ ,Travel_Logs.tblPhotoAlbum ,Travel_Logs.tblPhotoAlbumtoPhoto $> ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'INSERT\' ' .
						'AND logView.`_NEW_photoalbumID` = <$ tblPhotoAlbum.photoalbumID $> ' .
						'AND logView.`_NEW_photoID` = <$ tblPhotoAlbumtoPhoto.photoID $> ' .
						'<$ ' .
							'AND `tblPhotoAlbum`.`groupID` = \''.$groupID.'\' ' . 
						'$> ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
					'GROUP BY `logView`.`_NEW_photoalbumID` ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forGROUPRECENTPHOTOS($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					//$photos	= $this->retrievePhotoArray($row['_NEW_photoalbumID'],$row['_EXECUTION_DATE_']);
					$traveler = new Traveler($row['_DOER_ID_']);
					$update = new Update;
					$update->setExecutionDate(GaDateTime::dbDateFormat(strtotime($row['_EXECUTION_DATE_'])));
					$update->setDoer(new UpdateDoer($traveler));
					$update->setRetriever($this);
					$update->setContextLink('/collection.php?type=group&ID='.$this->subjectGroup->getGroupID().'&context=photoalbum&genID='.$row['_NEW_photoalbumID']);
					$update->setContextName('Photo Album');
					$update->setSubjectGroup($this->subjectGroup);
					$update->setParentGroup($this->parentGroup);
					$update->setPhotoAlbum(new PhotoAlbum($row['_NEW_photoalbumID']));
					//$update->setPhotos($photos);
					$update->setAction(UpdatesComposer::compose(
						array(
							'action' 	=> UpdateAction::PHOTO_ALBUM_WITH_ADDED_PHOTOS,
							'date'		=> GaDateTime::create($update->getExecutionDate())->commonDateFormat()
						), 'tpl.PhotoUpdates.php'));
						
					$time_key = strtotime($row['_EXECUTION_DATE_']);
					
					while(array_key_exists($time_key,$arr)){
						$time_key = strtotime("+1 second",$time_key);
					}
					$arr[$time_key] = $update;
					
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>