<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');
	
	require_once('travellog/model/Class.DiscussionPeer.php');

	class PostedNewDiscussion implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			if( empty($groupIDs) ) 
				return array();
			$db  = new dbHandler('DiscussionBoard');
			$sql = 	'SELECT `tblTopics`.`ID` ' .
					'FROM `DiscussionBoard`.`tblTopics` ' . 
					'WHERE 1 ' .
						'AND `tblTopics`.`groupID` IN (\'' . join('\', \'', $groupIDs) . '\') '	;
			$rs1 = new iRecordset($db->execute($sql));
			if( !$rs1->retrieveRecordcount() ){
				return array();
			}
			$sql = 	'SELECT logView.*, CAST(logView._EXECUTION_DATE_ AS DATE ) as eDate ' .
					'FROM `DiscussionBoard`.`tblDiscussions` as logView,  `DiscussionBoard`.`tblPosts` ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'INSERT\' ' .
						'AND logView.`_LOG_ID_` = `tblPosts`.`_LOG_ID_` ' .
						'AND logView.`_NEW_ID` = `tblPosts`.`_NEW_discussionID` ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
						'AND logView.`_NEW_topicID` IN (\'' . join('\', \'', $rs1->retrieveColumn('ID')) . '\') ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		private function forGROUPRECENTACTIVITY($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$discussion = DiscussionPeer::retrieveByPK($row['_NEW_ID']);
					if($discussion instanceOf Discussion){
						$firstPost	= $discussion->getFirstPost();
						if( is_null($firstPost) ){
							continue;
						}
						$poster		= $firstPost->getPoster();
						if(PosterPeer::ADVISOR != $poster->getPosterType()){
						
							$update = new Update;
							$update->setExecutionDate($row['_EXECUTION_DATE_']);
							$update->setDoer(new UpdateDoer($poster->getAdministrator()));
							$update->setRetriever($this);
							$update->setContextLink("/discussion/posts/".$discussion->getID());
							$update->setContextName('Discussion');
							$update->setSubjectGroup($this->subjectGroup);
							$update->setParentGroup($this->parentGroup);
							$update->setAction(UpdatesComposer::compose(
								array(
									'action' 			=> UpdateAction::POSTED_NEW_DISCUSSION,
									'discussionTitle'	=> $discussion->getTitle(),
									'groupName'			=> $discussion->getTopic()->getGroup()->getName(),
									'linktofeed'		=> $update->getContextLink()
								), 'tpl.GroupMembershipUpdates.php'));
							$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;	
						}
					}	
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>