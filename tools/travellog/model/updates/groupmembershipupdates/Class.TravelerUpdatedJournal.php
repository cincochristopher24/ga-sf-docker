<?php
	
	// action for posting new discussion
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');

	class TravelerUpdatedJournal implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['parentGroup'];
			if( empty($groupIDs) ) 
				return array();
			
			$sql = 	'SELECT DISTINCT(logView.`_NEW_travelID`), logView.*, CAST(logView._EXECUTION_DATE_ AS DATE ) as eDate ' .
					'FROM `Travel_Logs`.`tblTravel` as logView <$ ,`Travel_Logs`.`tblGrouptoTraveler` $> ' .
					'WHERE 1 = 1 ' .
						'AND logView.`_COMMAND_` = \'UPDATE\' ' .
						'AND logView.`_NEW_travelerID` = <$ `tblGrouptoTraveler`.`travelerID` $> ' .
						'<$ '. 
						'AND `tblGrouptoTraveler`.`groupID` IN (\'' . join('\', \'', $groupIDs ) . '\') '  . 
						'$> '.
						'AND logView.`_OLD_deleted` = 0 AND logView.`_NEW_deleted` = 0 '  . 
						'AND logView.`_NEW_viewed` = logView.`_OLD_viewed` ' .
						//'AND logView.`_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
					'ORDER BY logView.`_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
		}
		
		/*private function forGROUPRECENTACTIVITY($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$traveler = new Traveler($row['_DOER_ID_']);
					$travel = new Travel($row['_NEW_travelID']);
					if( $traveler->getTravelerID() != $this->parentGroup->getAdministratorID() ){
						
						$update = new Update;
						$update->setExecutionDate($row['_EXECUTION_DATE_']);
						$update->setDoer(new UpdateDoer($traveler));
						$update->setRetriever($this);
						$update->setContextLink('/journal-entry.php?action=view&travellogID=' . $row['_NEW_travellogID']);
						$update->setContextName('Journal Entry');
						$update->setSubjectGroup($this->subjectGroup);
						$update->setParentGroup($this->parentGroup);
						$update->setAction(UpdatesComposer::compose(
							array(
								'action' 	=> UpdateAction::TRAVELER_ADDED_JOURNAL_ENTRY,
								'title'		=> $row['_NEW_title'],
								'groupName'	=> 'group'
							), 'tpl.GroupMembershipUpdates.php'));
						$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;
					}
				}
				catch(exception $e){}
			}
			return $arr;
		}*/
		
		private function forJOURNALSFEED($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$traveler = new Traveler($row['_DOER_ID_']);
					$travel = new Travel($row['_NEW_travelID']);
					$entries = $travel->getTravelLogsByTravelID();
					$link = (count($entries)) ? '/journal-entry.php?action=view&travellogID=' . $entries[0]->getTravelLogID() : '';
					
					if( $traveler->getTravelerID() != $this->parentGroup->getAdministratorID() ){
						
						$update = new Update;
						$update->setExecutionDate($row['_EXECUTION_DATE_']);
						$update->setDoer(new UpdateDoer($traveler));
						$update->setRetriever($this);
						$update->setContextLink($link);
						$update->setContextName('Journal');
						$update->setSubjectGroup($this->subjectGroup);
						$update->setParentGroup($this->parentGroup);
						$update->setJournal(new Travel($row['_NEW_travelID']));
						$update->setAction(UpdatesComposer::compose(
							array(
								'action' 	=> UpdateAction::TRAVELER_UPDATED_JOURNAL,
								'title'		=> $row['_NEW_title'],
								'groupName'	=> 'group',
								'pronoun'	=> $update->getDoer()->getPossessivePronoun(),
								'linktofeed'=> $update->getContextLink()
							), 'tpl.JournalsFeedUpdates.php'));
						$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;
					}
				}
				catch(exception $e){}
			}
			return $arr;
		}
		
	}

?>