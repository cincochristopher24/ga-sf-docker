<?php
	
	require_once('travellog/model/updates/interfaces/Interface.GroupMembershipUpdatesRetriever.php');
	require_once('travellog/model/updates/Class.UpdatesComposer.php');
	require_once('gaLogs/Reader/Class.Reader.php');
	
	class TravelerDeniedStaffInvitation implements GroupMembershipUpdatesRetriever {
		
		private $reader 		= null;
		private $subjectGroup 	= null;
		private $parentGroup 	= null;
		
		public function __construct(){
			$this->reader = new Reader;
		}
		
		public function retrieve($params = array()){
			$endDate 		= $params['endDate'];
			$startDate		= $params['startDate'];
			$groupIDs		= $params['groupIDs'];
			$this->subjectGroup = $params['group'];
			$this->parentGroup 	= $params['group'];
			if( empty($groupIDs) ) 
				return array();
			$sql = 	'SELECT *, CAST(`_EXECUTION_DATE_` AS DATE ) as eDate ' .
						',GROUP_CONCAT(logView.`_OLD_groupID` SEPARATOR "," ) as gIDs ' .
						',GROUP_CONCAT(logView.`_EXECUTION_DATE_` SEPARATOR "," ) as eDates ' .
					'FROM `Travel_Logs`.`tblInviteList` as logView ' .
					'WHERE 1 = 1 ' .
						'AND `_COMMAND_` = \'UPDATE\' ' .
						'AND `_OLD_groupID` IN (\'' . join('\', \'' ,$groupIDs) . '\') ' .
						'AND `_DOER_ID_` = `_OLD_travelerID` ' .
						'AND `_OLD_status` = 2 AND `_NEW_status` = 1 ' .
						'AND `_EXECUTION_DATE_` BETWEEN ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($startDate))) . ' AND ' . GaString::makeSqlSafe(GaDateTime::dbDateTimeFormat(strtotime($endDate))) . ' ' .
					'GROUP BY eDate, logView.`_OLD_travelerID` ' .
					'ORDER BY `_EXECUTION_DATE_` DESC ';
			$rs = $this->reader->getLogsBySql($sql);
			$trace = debug_backtrace();
			$callerClass = $trace[1]['class'];
			$method = "for" . strtoupper($callerClass);
			return method_exists($this, $method) ? $this->$method($rs) : array();
			
		}
		
		private function forSTAFFUPDATES($rs){
			$arr = array();
			foreach($rs as $row){
				try{
					$eDates		= explode(',', $row['eDates']);
					array_walk($eDates, create_function('&$v,$k', '$v = strtotime($v);'));
					rsort($eDates);
					array_walk($eDates, create_function('&$v,$k', '$v = GaDateTime::dbDateTimeFormat($v);'));
					$row['_EXECUTION_DATE_'] = $eDates[0];
					$traveler 	= new Traveler($row['_OLD_travelerID']);
					$groupName 	= 'group';
					if( $this->subjectGroup->getGroupID() == $this->parentGroup->getGroupID() ){
						$arrGrp		= array_unique(explode(',', $row['gIDs']));
						if( count($arrGrp) > 1){
							$groupName = count($arrGrp) . ' groups';
						}
						else{
							if( $arrGrp[0] != $this->subjectGroup->getGroupID() ){
								$group 		= GroupFactory::instance()->create(array($arrGrp[0]));
								$groupName 	= $group[0]->getName();
							}
						}
						/*$subGroups 	= array_values(array_diff($arrGrp, array($this->subjectGroup->getGroupID())));
						$temp = '';
						if( 1 < count($subGroups) )
							$temp = count($subGroups) . ' subgroups';
						elseif( 1 == count($subGroups) ){
							$group 	= GroupFactory::instance()->create(array($subGroups[0]));
							$temp 	= $group[0]->getName();
						}
						
						if( in_array($this->subjectGroup->getGroupID(), $arrGrp) ){
							$groupName = 'group ' . (count($subGroups) > 0 ? 'and ' . $temp : $temp);
						}
						else{
							$groupName = $temp;
						}*/	
					}
					
					if( $traveler->getTravelerID() != $this->parentGroup->getAdministratorID() ){
						$update = new Update;
						$update->setExecutionDate($row['_EXECUTION_DATE_']);
						$update->setDoer($traveler);
						$update->setAction(UpdatesComposer::compose(
							array(
								'traveler' 	=> $traveler, 
								'action' 	=> UpdateAction::TRAVELER_DENIED_INVITE_STAFF,
								'groupName'	=> $groupName
							), 'tpl.GroupMembershipUpdates.php'));
						$arr[strtotime($row['_EXECUTION_DATE_'])] = $update;
					}
				}
				catch(exception $e){}
			}
			return $arr;
		}
	}

?>