<?php

	class UpdateDoer {
		
		private $mCore = null;
		
		public function __construct($core){
			$this->mCore = $core;
		}
		
		public function getName(){
			if( $this->mCore instanceof Traveler )
				return $this->mCore->getUsername();
			elseif( 'Group' == get_parent_class($this->mCore) )
				return $this->mCore->getName();
			return null;
		}
		
		public function getFullName(){
			if( $this->isTraveler() ){
				$tp = $this->mCore->getTravelerProfile();
				return $tp->getFirstName() . ' ' . $tp->getLastName();
			}
		}
		
		public function getProfilePicture(){
			if( $this->mCore instanceof Traveler )
				return $this->mCore->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');
			elseif( 'Group' == get_parent_class($this->mCore) )
				return $this->mCore->getGroupPhoto()->getPhotoLink('standard');
			return null;
		}
		
		public function isTraveler(){
			return $this->mCore instanceof Traveler;
		}
		
		public function getTraveler(){
			return $this->isTraveler() ? $this->mCore : null;
		}
		
		public function getUrl(){
			if( $this->mCore instanceof Traveler )
				return '/' . $this->mCore->getUserName();
			elseif( 'Group' == get_parent_class($this->mCore) )
				return $this->mCore->getFriendlyUrl();
			return null;
		}
		
		public function getGender(){
			if( $this->mCore instanceof Traveler )
				return $this->mCore->getTravelerProfile()->getGender();
			return 0;
		}
		
		public function getPossessivePronoun(){
			switch($this->getGender()){
				case 1: return "his"; break;
				case 2: return "her"; break;
				default: return "the";
			}
		}
		
	}

?>