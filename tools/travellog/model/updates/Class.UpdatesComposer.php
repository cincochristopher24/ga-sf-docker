<?php
	require_once('Class.Template.php');
	
	class UpdatesComposer{
		
		public static function compose($vars,$tplName){
			$template = new Template();
			$template->setVars($vars);
			$template->set_path('travellog/views/updatecomposers/');
			return $template->fetch($tplName);
		}
	}
?>
