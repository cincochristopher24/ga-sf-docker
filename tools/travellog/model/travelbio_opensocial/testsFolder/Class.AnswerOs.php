<?php
	/***
	 * Author: Reynaldo Castellano III
	 * Version : 2.0 (Version 1.0 was made by Cheryl Ivy Q. Go)
	 * Date : July 05, 2007
	 */

	require_once 'travellog/model/travelbio_opensocial/testsFolder/Class.AnswerValueOs.php';
	
	class AnswerOs
	{		
		const ALL = 0;
		
		private $mAnswerID 			= 0;
		private $mTravelBioUserID 	= 0;
		private $mProfileQuestionID	= 0;
		
		private $mDb = null;
		private $mValues = array();
		
		function __construct($answerID = null){
			$this->mDb = new dbHandler("db=SocialApplication");
			if(!is_null($answerID)){
				$qry = 	'SELECT `travelBioUserAnswerID`, `travelBioUserID`, `profileQuestionID` ' .
						'FROM tblTravelBioUserAnswer ' .
						'WHERE travelBioUserAnswerID = '.ToolMan::makeSqlSafeString($answerID);						
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(0 == $rs->retrieveRecordCount()){
					throw new exception('Invalid travelBioUserAnswerID '.$answerID.' passed to object of type Answer.');
				}
				else{
					$this->setAnswerID( $rs->getTravelBioUserAnswerID(0) );
					$this->setTravelBioUserID( $rs->getTravelBioUserID(0) );
					$this->setProfileQuestionID( $rs->getProfileQuestionID(0) );					
				}
			}
		}
		
		/*** SETTERS ***/
		private function setAnswerID($param=0){
			$this->mAnswerID = $param;
		}
				
		public function setTravelBioUserID($param=0){
			$this->mTravelBioUserID = $param;
		}
		
		public function setProfileQuestionID($param=0){
			$this->mProfileQuestionID = $param;
		}
		
		public function addValue($type,$val=''){
			$this->mValues[] = array('type'=>$type,'value'=>$val);
		}
		
		/*** GETTERS ***/
		
		public function getAddedValue(){
			return $this->mValues;
		}
		
		public function getAnswerID(){
			return $this->mAnswerID;
		}
		
		public function getTravelBioUserID(){
			return $this->mTravelBioUserID;
		}
		
		public function getProfileQuestionID(){
			return $this->mProfileQuestionID;
		}
		
		public function getValues(){
			require_once('travellog/model/travelbio_opensocial/testsFolder/Class.AnswerValueOs.php');
			return AnswerValueOs::getAnswerValuesByAnswerID($this->getAnswerID());
		}
		
		/*** CRUDE ***/
		public function save(){
			//check if the travelBioUserID and the profileQuestionID has been set
			if(0==$this->getTravelBioUserID()){
				throw new exception('Cannot perform save operation. TravelBioUserID must not be equal to zero.');
			}
			if(0==$this->getProfileQuestionID()){
				throw new exception('Cannot perform save operation. ProfileQuestionID must not be equal to zero.');
			}
		//	echo "<br/>answerID = ".$this->getAnswerID();
			if(0==$this->getAnswerID()){//add
		//		echo "<br/>insert answer!";
				$qry = 	'INSERT INTO tblTravelBioUserAnswer (' .
							'`travelBioUserID`,' .
							'`profileQuestionID`' .
						')VALUES(' .
							ToolMan::makeSqlSafeString( $this->getTravelBioUserID() ) .',' .
							ToolMan::makeSqlSafeString( $this->getProfileQuestionID() ).
						')';
				$this->mDb->execQuery($qry);
				$this->setAnswerID( $this->mDb->getLastInsertedID() );
			//	echo "<br/>last insert id =".$this->mDb->getLastInsertedID();
			//	echo "<br/>new answer id =".$this->getAnswerID();
			}
			else{//udpate
			//	echo "<br/>update answer!";
				$qry =	'UPDATE tblTravelBioUserAnswer SET ' .
							'`travelBioUserID` 			= ' .ToolMan::makeSqlSafeString( $this->getTravelBioUserID() ) .','.
							'`profileQuestionID` 		= '	.ToolMan::makeSqlSafeString( $this->getProfileQuestionID() ).' '.
						'WHERE `travelBioUserAnswerID`	= '	.ToolMan::makeSqlSafeString($this->getAnswerID() );
				$this->mDb->execQuery($qry);						
			}
			require_once('travellog/model/travelbio_opensocial/testsFolder/Class.AnswerValueOs.php');
			foreach($this->mValues as $iVal){
				$ansVal = new AnswerValueOs();
			//	echo "<br/>passed answer id = ".$this->getAnswerID();
			//	echo "<br/>passed type = ".$iVal['type']." and passed value = ".$iVal['value'];
				$ansVal->setAnswerID( $this->getAnswerID() );
				$ansVal->setType($iVal['type']);
				$ansVal->setValue($iVal['value']);
				$ansVal->save();
			}
		}
		
		// $params is the travelBioId that should be replaced
		static function update($params = array()){
			if(0 < count($params)){
				$db = new dbHandler("db=SocialApplication");
				$newTravelBioId = $params['newTravelBioId'];
				$idToBeReplaced = $params['idToBeReplaced'];
				$sql = 'UPDATE tblTravelBioUserAnswer SET ' .
							'`travelBioUserID` = ' .ToolMan::makeSqlSafeString( $newTravelBioId ) .' '.
							'WHERE `travelBioUserID` = ' .ToolMan::makeSqlSafeString( $idToBeReplaced );
				$db->execQuery($sql);
			}
			
		}		
		
		public function delete(){
			$qry = 	'DELETE FROM tblTravelBioUserAnswer ' .
					'WHERE `travelBioUserAnswerID` = '.ToolMan::makeSqlSafeString( $this->getAnswerID() );
			$this->mDb->execQuery($qry);
		}
		
		public function createDuplicate(){
			$newAnswer = new AnswerOs();
			$newAnswer->setProfileQuestionID( $this->getProfileQuestionID() );
			$newAnswer->setTravelBioUserID( $this->getTravelBioUserID() );
			$values = $this->getValues();
			foreach($values as $iVal){
				$newAnswer->addValue($iVal->getType(),$iVal->getValue());
			}
			return $newAnswer;
		}
		
		/*** Static Functions ***/
		public static function getAnswersByUser($userID=0){
			$db = new dbHandler("db=SocialApplication");
			$qry = 	'SELECT `travelBioUserAnswerID` ' .
					'FROM tblTravelBioUserAnswer ' .
					'WHERE travelBioUserID = '. ToolMan::makeSqlSafeString($userID) .' '.
					'ORDER BY travelBioUserAnswerID asc';
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if(0 != $rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new AnswerOs($rs->getTravelBioUserAnswerID());
				}
			}
			return $ar;
		}

		// START -- Added BY: Cheryl Ivy Q. Go 21 February 2008
		public static function getAnswerByQuestion($_userId = 0, $_questionId = 0){
			$db = new dbHandler("db=SocialApplication");

			$sql = "SELECT travelBioUserAnswerID " .
					"FROM tblTravelBioUserAnswer " .
					"WHERE travelBioUserID = " . ToolMan::makeSqlSafeString($_userId) . " " .
					"AND profileQuestionID = " . ToolMan::makeSqlSafeString($_questionId);
			$rs = new iRecordset($db->execQuery($sql));

			if(0 != $rs->retrieveRecordCount()){
				return AnswerValueOs::getAnswerValuesByAnswerID($rs->getTravelBioUserAnswerID());
			}
			else
				return NULL;
		}
		// END -- Added BY: Cheryl Ivy Q. Go 21 February 2008

		public static function clearUserAnswers($userID=0){
//			echo "<br/>clear user answers!<br/>";
			$db = new dbHandler("db=SocialApplication");
			$qry = 	'DELETE tblTravelBioUserAnswer, tblAnswerValue '.
					'FROM tblTravelBioUserAnswer, tblAnswerValue '.
					'WHERE tblAnswerValue.travelBioUserAnswerID = tblTravelBioUserAnswer.travelBioUserAnswerID '.
					'AND tblTravelBioUserAnswer.travelBioUserID = ' .ToolMan::makeSqlSafeString($userID);
			$db->execQuery($qry);			
		}
	}
?>