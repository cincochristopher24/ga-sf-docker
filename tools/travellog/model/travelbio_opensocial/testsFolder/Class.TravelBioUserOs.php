<?php
require_once('Class.ToolMan.php');
require_once('Class.dbHandler.php');
require_once('Class.iRecordset.php');

class TravelBioUserOs{

	private $db 			  = null,
			$mOsBioUserID	  = null,
			$mTravelBioUserID = null,
			$mUserID 		  = null,
			$mDateAdded		  = null,
			$mNumOfQuestionsDisplayed = null;
	
	function __construct($param = null){
		$this->db = new dbHandler("db=SocialApplication");
		if (is_numeric($param)){				
			$qry = 	"SELECT travelBioUserID, userID, dateAdded, numQuestionsDisplayed ".
					"FROM tblTravelBioUserOS ".
					"WHERE travelBioUserID =".ToolMan::makeSqlSafeString($param);
			$rs = new iRecordset($this->db->execQuery($qry));
			while(!$rs->EOF()){
				$this->setTravelBioUserID( $rs->getTravelBioUserID() );
				$this->setUserID( $rs->getUserID() );
				$this->setDateAdded( $rs->getDateAdded() );
				$this->setNumOfQuestionsDisplayed( $rs->getNumQuestionsDisplayed() );
			$rs->moveNext();
			}
//			if (0 == $rs->retrieveRecordCount()){
//				throw new exception('Invalid travelerBioUserID '.$param.' passed to object of type TravelBioUser.');
//			}
		}
	}
	
	function delete(){
		$this->db->execQuery('DELETE FROM tblTravelBioUser WHERE travelBioUserID = ' . ToolMan::makeSqlSafeString($this->getTravelBioUserID()));
		$this->db->execQuery('DELETE FROM tblTravelBioUserOS WHERE travelBioUserID = ' . ToolMan::makeSqlSafeString($this->getTravelBioUserID()));
		//clear the answers of this TravelBioUser
		// 
//		require_once("travellog/model/travelbio_opensocial/testsFolder/Class.AnswerOs.php");
//		AnswerOs::clearUserAnswers($this->getTravelBioUserID());
	}
	
	function setTravelBioUserID($travelBioUserID){
		$this->mTravelBioUserID = $travelBioUserID;
	}
	
	function setUserID($userID){
		$this->mUserID = $userID;
	}
	
	function setDateAdded($dateAdded){
		$this->mDateAdded = $dateAdded;
	}
	
	function setNumOfQuestionsDisplayed($numOfQuestionsDisplayed){
		$this->mNumOfQuestionsDisplayed = $numOfQuestionsDisplayed;
	}
		
	function getTravelBioUserID(){
		return $this->mTravelBioUserID;
	}
	
	function getUserID(){
		return $this->mUserID;
	}
	
	function getDateAdded(){
		return $this->mDateAdded;
	}
	
	function getNumOfQuestionsDisplayed(){
		return $this->mNumOfQuestionsDisplayed;
	}

	public function getAnswers(){
		require_once("travellog/model/travelbio_opensocial/testsFolder/Class.AnswerOs.php");
		return AnswerOs::getAnswersByUser($this->mTravelBioUserID);
	}
	
	public function clearAnswers(){
		require_once("travellog/model/travelbio_opensocial/testsFolder/Class.AnswerOs.php");
		AnswerOs::clearUserAnswers($this->mTravelBioUserID);
	}
	
	public function save(){
	//	echo "<br/>numtodisplay=".$this->getNumOfQuestionsDisplayed();
	//	echo "<br/>bioUserID=".$this->getTravelBioUserID();
	//	echo "<br/>";
		$conn = new dbHandler("db=SocialApplication");
		$sql = "UPDATE tblTravelBioUser as a, tblTravelBioUserOS as b
				set a.numQuestionsDisplayed = " . $this->getNumOfQuestionsDisplayed() . ", " .
				"b.numQuestionsDisplayed = " . $this->getNumOfQuestionsDisplayed() . " " .
				"WHERE a.travelBioUserID = " . $this->getTravelBioUserID() ." AND " .
				"b.travelBioUserID = " . $this->getTravelBioUserID();
	//	echo $sql;
		$conn->execQuery($sql);
	}
	
	static function getBioUserIdByServiceUserID($serviceUserID){
		$bioUser = null;
		$conn = new dbHandler("db=SocialApplication");
		$sql = "SELECT travelBioUserID from tblTravelBioUserOS where userID = ".$serviceUserID." limit 1";
		$rs = new iRecordset($conn->execQuery($sql));
		while(!$rs->EOF()){
		//	$bioUser = $rs->getTravelBioUserID();
			$bioUser = new TravelBioUserOs($rs->getTravelBioUserID());
		$rs->moveNext();
		}
		return $bioUser;
	}
	
	static function getDotnetBioUserIdByTravelerID($travelerID){
		if(0==$travelerID){
			return null;
		}
		$db = new dbHandler("db=SocialApplication");
		$qry =	'SELECT travelBioUserID FROM tblTravelBioUser WHERE travelerID = '.ToolMan::makeSqlSafeString($travelerID);
		$rs = new iRecordset($db->execQuery($qry));
		if(0 != $rs->retrieveRecordCount()){
			return $rs->getTravelBioUserID(0);
		//	return new TravelBioUser($rs->getTravelBioUserID(0));
		}
		else{
			return null;
		}
	}
	
	static function reSyncOsUpdate($newUser = array(), $userInfo = array()){
		$newUserID  = $newUser['userID'];
		$newBioUserID = $newUser['newBioUserId'];
		$oldUserID  = $userInfo['oldUserID'];
		$conn = new dbHandler("db=SocialApplication");
		
		$sql = "INSERT INTO tblTravelBioUserOS
					SET travelBioUserID = ". $newBioUserID .", userID = " . $newUserID . ", dateAdded = now() ".
					"ON DUPLICATE KEY UPDATE travelBioUserID = ". $newBioUserID;
		$conn->execQuery($sql);
	}
	
	static function firstOsSyncUpdate($newUser = array(), $userInfo = array()){
		
		$newUserID  = $newUser['userID'];
		$newBioUserID = $newUser['newBioUserId'];
		$oldUserID  = $userInfo['oldUserID'];

		$conn = new dbHandler("db=SocialApplication");
		
		$newUserExist = self::isUserExisting($newUserID);
		//echo "<br/>exist? = ".$newUserExist;
		if($newUserExist == true){
			$sql = "DELETE FROM tblTravelBioUserOS where userID = " . $oldUserID;
			$conn->execQuery($sql);
		}
		else{
			$sql = "UPDATE tblTravelBioUserOS
			 		SET travelBioUserID = ". $newBioUserID .", userID = " . $newUserID . " ".
					"WHERE userID = ". $oldUserID;
			$conn->execQuery($sql);
		}
	}
	
	static function isUserExisting($newUserID){
		$newUserExist = false;
		$conn = new dbHandler("db=SocialApplication");
		$sql = "SELECT userID from tblTravelBioUserOS where userID = ".$newUserID;
		$rs = new iRecordset($conn->execQuery($sql));
		if(0 != $rs->retrieveRecordCount()){
			$newUserExist = true;
		}
		return $newUserExist;
	}
	
	function updateAnswers($_param = 0){
		if(0 < $_param){
			$params = array(
						"newTravelBioId" => $this->getTravelBioUserID(),
						"idToBeReplaced"=> $_param
						);
			AnswerOs::update($params);
		}
		
	}

}

?>