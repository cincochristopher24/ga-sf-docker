<?php
	
	require_once('Class.iRecordset.php');
	require_once('Class.dbHandler.php');
	require_once('Class.GaSurveyClient.php');
	
	final class GaSurveyApi
	{
		public static function create($apiKey=null,$apiSecret=null){
			$client = GaSurveyClient::createByApiKeyAndApiSecret($apiKey,$apiSecret);
			if(!is_null($client)){
				return new GaSurveyApi($client);
			}
			throw new exception('Invalid GaSurvey client.');
		}
		
		private $mGaSurveyClient = null;
		
		private function __construct($client=null){
			$this->mGaSurveyClient = $client;
		}

		public function getSurveyForm($surveyFormID=0){
			require_once('Class.SurveyForm.php');
			return SurveyForm::getByGaSurveyClientIDAndSurveyFormID($this->mGaSurveyClient->getGaSurveyClientID(),$surveyFormID);			
		}
		
		public function getSurveyFormByName($surveyFormName=null){
			require_once('Class.SurveyForm.php');
			return SurveyForm::getByGaSurveyClientIDAndSurveyFormName($this->mGaSurveyClient->getGaSurveyClientID(),$surveyFormName);			
		}
		
		public function getAllSurveyForms(){
			require_once('Class.SurveyForm.php');
			return SurveyForm::getSurveyFormsByGaSurveyClientID($this->mGaSurveyClient->getGaSurveyClientID());	
		}
		
		public function createNewSurveyForm(){
			require_once('Class.SurveyForm.php');
			$newFrm = new SurveyForm(0);
			$newFrm->setGaSurveyClientID($this->mGaSurveyClient->getGaSurveyClientID());
			return new $newFrm;
		}
		
		public function getSurveyFormAnswer($surveyFormAnswerID=0){
			require_once('Class.SurveyFormAnswer.php');
			return SurveyFormAnswer::getSurveyFormAnswerBySurveyFormAnswerID($surveyFormAnswerID);
		}
		
		public function createNewSurveyFormAnswer(){
			require_once('Class.SurveyFormAnswer.php');
			return new SurveyFormAnswer();
		}
		
		
		public function getGaSurveyClientID(){
			return $this->mGaSurveyClient->getGaSurveyClientID();
		}
		
		
	}
?>