<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	
	abstract class InputControl
	{		
		const TEXTBOX 		= 0;		
		const CHECKBOX 		= 1;
		const RADIO			= 2;
		const COMBOBOX		= 3;
		const SECTION_BREAK	= 4;		
		
		protected $mAttributes = array();
		protected $mSurveyFieldInputID;
		protected $mSurveyFieldID;
		protected $mInputType;
		protected $mPosition;
		protected $mDb;
		
		function __construct($params=0){			
			$this->mDb = new dbHandler('db=');
			$this->mSurveyFieldInputID = (is_array($params) && array_key_exists('surveyFieldInputID',$params)?$params['surveyFieldInputID']:(is_numeric($params)?$params:0));									
			if(0 != $this->mSurveyFieldInputID){
				//get data from tblSurveyFieldInput table			
				$qry = 	'SELECT surveyFieldInputID, surveyFieldID, inputType, position ' .
						'FROM GaSurvey.tblSurveyFieldInput ' .
						'WHERE surveyFieldInputID = '.$this->mSurveyFieldInputID;
				$rs = new iRecordset($this->mDb->execQuery($qry));				
				if(0==$rs->retrieveRecordCount()){
					throw new exception ("SurveyFieldInput with ID [$this->mSurveyFieldID] does not exist in database.");
				}							
				$this->mSurveyFieldInputID 	= $rs->getSurveyFieldInputID(0);
				$this->mSurveyFieldID 		= $rs->getSurveyFieldID(0);
				$this->mInputType			= $rs->getInputType(0);
				$this->mPosition 			= $rs->getPosition(0);				
				//get the attributes from tblSurveyFieldInputAttribute
				$rs = $this->getAttributeValues();											
				if(0 < $rs->retrieveRecordCount()){				
					for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){																	
						if(array_key_exists($rs->getAttribute(),$this->mAttributes)){													
							if(is_array($this->mAttributes[$rs->getAttribute()])){
								$this->mAttributes[$rs->getAttribute()][] = $rs->getValue();
							}
							else{
								$this->mAttributes[$rs->getAttribute()] = $rs->getValue();
							}
						}
					}
				}
			}
		}
		
		public function init($params){
			$this->mSurveyFieldID 	= array_key_exists('surveyFieldID',$params)?$params['surveyFieldID']:$this->mSurveyFieldID;
			$this->mInputType 		= array_key_exists('inputType',$params)?$params['inputType']:$this->mInputType;
			$this->mPosition 		= array_key_exists('position',$params)?$params['position']:$this->mPosition;
			//set the attributes
			foreach($this->mAttributes as $key => $value){
				if(array_key_exists($key,$params)){
					$this->mAttributes[$key] = $params[$key];						
				}
			}
		}
				
		public function setSurveyFieldID($param){
			$this->mSurveyFieldID = $param;
		}		
		public function setInputType($param){
			$this->mInputType = $param;
		}
		public function setPosition($param){
			$this->mPosition = $param;
		}		
		
		public function getSurveyFieldInputID(){
			return $this->mSurveyFieldInputID;
		}
		
		public function getSurveyFieldID(){
			return $this->mSurveyFieldID;			
		}
		
		public function getInputType(){
			return $this->mInputType;
		}
		public function getPosition(){
			return $this->mPosition;
		}
		
		public function setAttribute($name,$value){
			$this->mAttributes[$name] = $value;
		}
		public function getAttribute($name){
			return $this->mAttributes[$name]; 	
		}
		/*** CRUDE ***/
		public function save(){			
			if(0 != $this->mSurveyFieldInputID){//edit
				$qry =	'UPDATE GaSurvey.tblSurveyFieldInput SET ' .									
							'`surveyFieldID` 	='.ToolMan::makeSqlSafeString( $this->mSurveyFieldID ).', '.
							'`inputType` 		='.ToolMan::makeSqlSafeString( $this->mInputType ).', '.
							'`position` 		='.ToolMan::makeSqlSafeString( $this->mPosition ).
						'WHERE surveyFieldInputID ='.ToolMan::makeSqlSafeString( $this->mSurveyFieldInputID );
				$this->mDb->execQuery($qry);
				//clear the attributes in the database			
				$qry =	'DELETE FROM GaSurvey.tblSurveyFieldInputAttribute ' .
						'WHERE `surveyFieldInputID` ='.ToolMan::makeSqlSafeString($this->mSurveyFieldInputID);
				$this->mDb->execQuery($qry);				
				foreach($this->mAttributes as $key => $value){
					$arValues = array();
					if(is_array($value)){						
						$arValues = $value; 						
					}
					else{
						$arValues[] = $value;
					}
					foreach($arValues as $newVal){
						$qry =	'INSERT INTO GaSurvey.tblSurveyFieldInputAttribute(' .
									'`surveyFieldInputID`,' .
									'`attribute`,' .
									'`value`' .
								') ' .
								'VALUES (' .
									ToolMan::makeSqlSafeString( $this->mSurveyFieldInputID ).', '.
									ToolMan::makeSqlSafeString( $key ).', '.
									ToolMan::makeSqlSafeString( $newVal ).
								')';
						$this->mDb->execQuery($qry);
					}
				}						
			}
			else{//add				
				$qry = 	'INSERT INTO GaSurvey.tblSurveyFieldInput(' .							
							'`surveyFieldID`,' .
							'`inputType`,' .
							'`position`'.
						') ' .
						'VALUES (' .
							ToolMan::makeSqlSafeString( $this->mSurveyFieldID ).', '.
							ToolMan::makeSqlSafeString( $this->mInputType ).', '.
							ToolMan::makeSqlSafeString( $this->mPosition ).
						')';
				$this->mDb->execQuery($qry);
				$this->mSurveyFieldInputID = $this->mDb->getLastInsertedID();				
				//assume also that the attributes are new
				foreach($this->mAttributes as $key => $value){
					$arValues = array();
					if(is_array($value)){
						$arValues = $value;
					}
					else{
						$arValues[] = $value;
					}					
					foreach($arValues as $newVal){
						$qry =	'INSERT INTO GaSurvey.tblSurveyFieldInputAttribute(' .
									'`surveyFieldInputID`,' .
									'`attribute`,' .
									'`value`' .
								') ' .
								'VALUES (' .
									ToolMan::makeSqlSafeString( $this->mSurveyFieldInputID ).', '.
									ToolMan::makeSqlSafeString( $key ).', '.
									ToolMan::makeSqlSafeString( $newVal ).
								')';
						$this->mDb->execQuery($qry);
					}
				}
			}
		}
		
		public function getData(){			
			$ar = array();
			$ar['surveyFieldInputID'] 	= $this->mSurveyFieldInputID;
			$ar['surveyFieldID']		= $this->mSurveyFieldID;
			$ar['inputType']			= $this->mInputType;
			$ar['position']				= $this->mPosition;						
			foreach($this->mAttributes as $key => $val){
				$ar[$key] = $val;
			}
			return $ar;
		}
		
		public function delete(){
			//delete the survey Field input
			$qry = 	'DELETE FROM GaSurvey.tblSurveyFieldInput ' .
					'WHERE `surveyFieldInputID` ='.ToolMan::makeSqlSafeString($this->mSurveyFieldInputID);
			$this->mDb->execQuery($qry);
			//delete the input attributes
			$qry =	'DELETE FROM GaSurvey.tblSurveyFieldInputAttribute ' .
					'WHERE `surveyFieldInputID` ='.ToolMan::makeSqlSafeString($this->mSurveyFieldInputID);
			$this->mDb->execQuery($qry);
		}
		protected function getAttributeValues(){			
			$qry = 	'SELECT surveyFieldInputAttributeID, attribute, `value` ' .
					'FROM GaSurvey.tblSurveyFieldInputAttribute ' .
					'WHERE `surveyFieldInputID` = '. $this->mSurveyFieldInputID.' ' .
					'ORDER BY surveyFieldInputAttributeID';				
			return new iRecordset($this->mDb->execQuery($qry));
		}		
		public static function getFieldInputs($surveyFieldID){
			$db = new dbHandler('db=');
			$qry = 	'SELECT surveyFieldInputID, inputType ' .
					'FROM GaSurvey.tblSurveyFieldInput ' .
					'WHERE surveyFieldID = '.ToolMan::makeSqlSafeString($surveyFieldID).' '.
					'ORDER BY position';
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();			
			if(0<$rs->retrieveRecordCount()){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[]=self::createControl($rs->getSurveyFieldInputID(),$rs->getInputType());
				}
			}
			return $ar;
		}		
		public static function createControl($controlID, $controlType,$params=0){
			switch($controlType){
				case self::TEXTBOX:
					require_once('Class.TextBoxControl.php');
					return new TextBoxControl((0!=$params?$params:$controlID));
				break;
				case self::CHECKBOX:
					require_once('Class.CheckBoxControl.php');
					return new CheckBoxControl((0!=$params?$params:$controlID));
				break;
				case self::RADIO:
					require_once('Class.RadioControl.php');
					return new RadioControl((0!=$params?$params:$controlID));
				break;
				case self::COMBOBOX:
					require_once('Class.ComboBoxControl.php');
					return new ComboBoxControl((0!=$params?$params:$controlID));
				break;
				case self::SECTION_BREAK:
					require_once('Class.SectionBreakControl.php');
					return new SectionBreakControl((0!=$params?$params:$controlID));
				break;
				default:
					return NULL;
				break;
			}
		}	
	}
?>