<?php
	require_once('Class.InputControl.php');
	
	class RadioControl extends InputControl
	{				
		function __construct($params){
			$this->mInputType = 2;
			$this->mAttributes['isChecked'] = 0;			
			$this->mAttributes['text'] = '';			
			parent::__construct($params);
		}
	}
?>