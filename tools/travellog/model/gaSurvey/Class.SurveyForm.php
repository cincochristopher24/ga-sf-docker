<?php
	require_once('Class.dbHandler.php');
	require_once('Class.iRecordset.php');
	require_once('Class.ToolMan.php');
	require_once('Class.SurveyField.php');
	require_once('Class.GaString.php');
	
	final class SurveyForm
	{
		private $mSurveyFormID;
		private $mGaSurveyClientID;
		private $mName;		
		private $mCaption;
		private $mDateCreated;
		
		private $mFields = null;
		private $mDb = null;

		public function __construct($params=0){										
			$this->clearValues();
			$this->mSurveyFormID = is_numeric($params)?$params:$params['surveyFormID'];
			$this->mDb = new dbHandler('db=');
			if($this->mSurveyFormID != 0){
				$qry = 	'SELECT surveyFormID, gaSurveyClientID, name, caption, dateCreated ' .
						'FROM GaSurvey.tblSurveyForm ' .
						'WHERE surveyFormID = '.$this->mSurveyFormID;
				$rs = new iRecordset($this->mDb->execQuery($qry));
				if(!$rs->retrieveRecordCount()){
					$trace = debug_backtrace();
					throw new exception('Invalid Survey Form ID ['.$this->mSurveyFormID.'] in object of type Survey Form.<<< CALLER FILE: '. $trace[0]['file'] .' LINE: '.$trace[0]['line'].'>>>');
				}				
				$this->mSurveyFormID 		= $rs->getSurveyFormID(0);
				$this->mGaSurveyClientID	= $rs->getGaSurveyClientID(0);
				$this->mName				= $rs->getName(0);				
				$this->mCaption				= $rs->getCaption(0);
				$this->mDateCreated 		= $rs->getDateCreated(0);
			}
		}
		
		public function init($params){					
			$this->mGaSurveyClientID = (array_key_exists('gaSurveyClientID',$params))?$params['gaSurveyClientID']:$this->mGaSurveyClientID;
			$this->mName 	= (array_key_exists('name',$params))?$params['name']:$this->mName;
			$this->mCaption = (array_key_exists('caption',$params))?$params['caption']:$this->mCaption;
			$this->mDateCreated = (array_key_exists('dateCreated',$params))?$params['dateCreated']:$this->mDateCreated;
			//if there are fields, put them in the array of fields
			if(array_key_exists('fields',$params) && is_array($params['fields']) ){
				$this->mFields = array();
				foreach($params['fields'] as $iFld){
					if(!is_null($iFld)){
						$fld = new SurveyField($iFld['surveyFieldID']);
						$fld->init($iFld);
						$this->mFields[] = $fld;
					}
				}
			}			
		}
		
		/*** GETTERS ***/
		public function getSurveyFormID(){			
			return $this->mSurveyFormID;
		}
		public function getGaSurveyClientID(){
			return $this->mGaSurveyClientID;
		}
		public function getName(){			
			return $this->mName;
		}					
		public function getCaption(){
			return $this->mCaption;			
		}
		public function getDateCreated(){
			return $this->mDateCreated;
		}

		/*** SETTERS ***/
		public function setGaSurveyClientID($arg){
			$this->mGaSurveyClientID = $arg;
		}
		public function setName($arg){
			$this->mName = $arg;
		}
		public function setCaption($arg){
			$this->mCaption = $arg;			
		}		
				
		/*** CRUDE ***/		
		public function save(){
			//validations 
			if(0==strlen($this->mName)){
				throw new exception("Survey form Name cannot be an empty string.");
			}								
			if(0 != $this->mSurveyFormID){//edit
				$qry =	'UPDATE GaSurvey.tblSurveyForm SET ' .
							'`gaSurveyClientID`				='.ToolMan::makeSqlSafeString( $this->mGaSurveyClientID ).', '.
							'`name`							='.ToolMan::makeSqlSafeString( $this->mName ).', '.
							'`caption`						='.ToolMan::makeSqlSafeString( $this->mCaption ).' '.
						'WHERE surveyFormID ='.ToolMan::makeSqlSafeString( $this->mSurveyFormID );
				$this->mDb->execQuery($qry);
			}
			else{//add
				$this->mDateCreated = date("Y-m-d G:i:s");
				$qry = 	'INSERT INTO GaSurvey.tblSurveyForm(' .
							'`gaSurveyClientID`,'.
							'`name`,' .
							'`caption`,' .
							'`dateCreated`' .
						') ' .
						'VALUES (' .
							ToolMan::makeSqlSafeString( $this->mGaSurveyClientID ).', '.
							ToolMan::makeSqlSafeString( $this->mName ).', '.
							ToolMan::makeSqlSafeString( $this->mCaption ).', '.
							ToolMan::makeSqlSafeString( $this->mDateCreated ).
						')';
				$this->mDb->execQuery($qry);
				$this->mSurveyFormID = $this->mDb->getLastInsertedID();
			}
		}
		
		public function getData(){
			//contruct an structure for the fields and properties			
			$form = array();
			$form['surveyFormID'] = $this->mSurveyFormID;
			$form['gaSurveyClientID'] = $this->mGaSurveyClientID;
			$form['name'] = (strlen($this->mName) > 0)?$this->mName:'';
			$form['caption'] = (strlen($this->mCaption) > 0)?$this->mCaption:'';
			$form['dateCreated'] = $this->mDateCreated;
			$form['fields']  = array();
			$fields = $this->getSurveyFormFields();
			foreach($fields as $fld){
				$form['fields'][] = $fld->getData();
			}
			return $form;
		}
		
		public function delete(){
			//clear the survey form answers
			$frmAnswers = $this->getSurveyFormAnswers();
			foreach($frmAnswers as $iFrmAns){
				$iFrmAns->delete();
			}
			//delete the fields
			$fields = $this->getSurveyFormFields();
			foreach($fields as $fld){
				$fld->delete();
			}
			$qry = 	'DELETE FROM GaSurvey.tblSurveyForm ' .
					'WHERE `surveyFormID` ='.ToolMan::makeSqlSafeString($this->mSurveyFormID);
			$this->mDb->execQuery($qry);
			$this->clearValues();
		}
		
		public function getSurveyFormAnswers(){
			require_once('Class.SurveyFormAnswer.php');
			return SurveyFormAnswer::getSurveyFormAnswersBySurveyFormID($this->getSurveyFormID());
		}
		
		
		public function getExistingSurveyFields(){
			return SurveyField::getSurveyFieldsBySurveyFormID($this->getSurveyFormID());
		}
		
		public function getSurveyFieldWithSurveyFieldID($fieldID=0){
			return SurveyField::getSurveyFieldBySurveyFormIDAndSurveyFieldID($this->getSurveyFormID(),$fieldID);
		}
		
		public function createNewSurveyField(){
			$surveyField = new SurveyField();
			$surveyField->setSurveyFormID($this->getSurveyFormID());
			return $surveyField;
		}
		
		
		public function addField($fld){
			if(!is_array($this->mFields)){
				$this->mFields = array();
			}
			$this->mFields[] = $fld;
		}
		
		public function clearFields(){
			$this->mFields = null;
		}
		
		public function clearValues(){
			$this->mSurveyFormID				= 0;
			$this->mName						= 'Untitled Form';			
			$this->mCaption			 			= 'You can add your survey questions using the options on the left. To name this survey and add your instructions, click on the Form Properties tab on the left.';
			$this->mDateCreated		 			= date("Y-m-d G:i:s");
			$this->mFields 						= null;
		}
		
		public function getSurveyFormFields(){
			if(!is_array($this->mFields)){				
				return SurveyField::getSurveyFieldsBySurveyFormID($this->mSurveyFormID);
			}
			else{
				return $this->mFields;
			}
		}
		
		public function mold($frm){
			$tplData = $this->getData();
			$tplData['surveyFormID'] = 0;
			$tplData['dateCreated']  = date("Y-m-d G:i:s");
			$tplData['name']  = $tplData['name'].'(Duplicate)';
			foreach($tplData['fields'] as $key => $val){
				$tplData['fields'][$key]['surveyFieldID'] = 0;
				$tplData['fields'][$key]['parentID'] = 0;
				foreach($tplData['fields'][$key]['inputs'] as $key1 => $val1){
					$tplData['fields'][$key]['inputs'][$key1]['surveyFieldID']=0;
					$tplData['fields'][$key]['inputs'][$key1]['surveyFieldInputID']=0;
				}
			}
			$frm->init($tplData);
		}
			
		public static function isExists($id=null){
			$db = new dbHandler('db=');
			$qry = 	'SELECT surveyFormID ' .
					'FROM GaSurvey.tblSurveyForm ' .
					'WHERE surveyFormID = '.ToolMan::makeSqlSafeString( $id );						
			$rs = new iRecordset($db->execQuery($qry));
			return $rs->retrieveRecordCount() > 0;
		}
		
		public static function getByGaSurveyClientIDAndSurveyFormID($clID=0,$frmID=0){
			$db = new dbHandler('db=');
			$qry = 	'SELECT surveyFormID ' .
					'FROM GaSurvey.tblSurveyForm ' .
					'WHERE gaSurveyClientID = '.ToolMan::makeSqlSafeString($clID).' ' .
					'AND surveyFormID = '.ToolMan::makeSqlSafeString($frmID);
			$rs = new iRecordset($db->execQuery($qry));
			return $rs->retrieveRecordCount() > 0 ? new SurveyForm($rs->getSurveyFormID(0)) : null;
		}
		
		public static function getByGaSurveyClientIDAndSurveyFormName($clID=0,$frmName=null){
			$db = new dbHandler('db=');
			$qry = 	'SELECT surveyFormID ' .
					'FROM GaSurvey.tblSurveyForm ' .
					'WHERE gaSurveyClientID = '.ToolMan::makeSqlSafeString($clID).' ' .
					'AND `name` = '.ToolMan::makeSqlSafeString(trim($frmName));
			$rs = new iRecordset($db->execQuery($qry));
			return $rs->retrieveRecordCount() > 0 ? new SurveyForm($rs->getSurveyFormID(0)) : null;
		}
		
		public static function getSurveyFormsByGaSurveyClientID($clID){
			$db = new dbHandler('db=');
			$qry = 	'SELECT surveyFormID ' .
					'FROM GaSurvey.tblSurveyForm ' .
					'WHERE gaSurveyClientID = '.ToolMan::makeSqlSafeString($clID);
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new SurveyForm($rs->getSurveyFormID());
				}
			}
			return $ar;
		}
	}
?>