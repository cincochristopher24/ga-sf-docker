<?
	
/**
 * Created on Nov 23, 2006
 * Created by: Joel C. LLano
 */
	
	
	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	

	class ProgramType{
		
		private $mConn         = NULL;
        private $mRs           = NULL;
        
		private $programid 		= 0;
		private $program			= NULL;
		
		function ProgramType($programid = 0){
 				
 				$this->programid = $programid;
 				
 				try {
	 				$this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}

			 			if ($programid > 0){
			 				
					 				$sql = "SELECT * from GoAbroad_Main.tbprogram where programid = '$programid' ";
						 			$this->mRs->Execute($sql);
					 			
					 				if ($this->mRs->Recordcount() == 0){
					 					throw new Exception("Invalid Program ID");	// ID not valid so throw exception
					 				}
					 				
					 				// set values to its attributes
									$this->program 		= stripslashes($this->mRs->Result(0,"program")); 
				 		}
						
						
 			}
		
		public static function getAllProgramType(){
				try {
	 				$conn = new Connection('GoAbroad_Main');
					$rs   = new Recordset($conn);
				}
				catch (Exception $e) {				   
				   throw $e;
				}
				
				$sql = "SELECT * from GoAbroad_Main.tbprogram WHERE programid <> 1 AND programid <>10   AND programid <>13 order by programID ASC ";
				$rs->Execute($sql);
				
				$travelerprogramtype_s = array();
							
				while ($rsar = mysql_fetch_array($rs->Resultset())) {
					$travelerprogramtype_s[] = new ProgramType($rsar['programID']);
				}
				return $travelerprogramtype_s;
		}
		
		
		/**
		 * Use 	__call function to dynamically call getter and setters
		 */
		
		function __call($method, $arguments) {
				//prefix ->get and set <- to lowercase
		        $prefix = strtolower(substr($method, 0, 3));
		        $property = strtolower(substr($method, 3));
		
		        if (empty($prefix) || empty($property)) {
		            return;
		        }
		
		        if ($prefix == "get" && isset($this->$property)) {
		            return $this->$property;
		        }
		
		        if ($prefix == "set") {
		            $this->$property = $arguments[0];
		        }
    	}
		
		/**
         * CRUD Methods (Create, Update , Delete)
         */ 
        function Create(){         
        }
        
        function Update(){         
        }
        
        function Delete(){ 
        }       
		
		
	}



?>