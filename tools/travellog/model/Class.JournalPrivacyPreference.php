<?php
class JournalPrivacyPreference{
	function getPrivacyCriteria2s( $curr_travelerID, $travelerID ){
		$arr_vars = array();
		
		$arr_vars[] = JournalPrivacyPreferenceType::$PUBLIC;
		
		if( $curr_travelerID ){
			require_once("travellog/model/Class.Traveler.php");
			require_once("travellog/model/Class.JournalPrivacyPreferenceType.php");
			require_once("travellog/model/Class.AdminGroup.php");
			
			// if viewer is owner	
			if( $curr_travelerID == $travelerID ) { 
				$arr_vars[] = JournalPrivacyPreferenceType::$PRIVATE;
				$arr_vars[] = JournalPrivacyPreferenceType::$FRIENDS;	
				$arr_vars[] = JournalPrivacyPreferenceType::$GROUP_MEMBERS;
				$arr_vars[] = JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS;
			} else {
				$obj_traveler    = new Traveler( $curr_travelerID );
				$groupID         = 0; //formerly AdminGroup::get.Advisor.GroupID( $travelerID );
				if (0 < $groupID)
					$obj_admin_group = new AdminGroup( $groupID );
				else
					$obj_viewed_traveler = new Traveler( $travelerID );
			
				//check if viewer is a friend of the traveler	
				if( $obj_traveler->isFriend( $travelerID ) ){
					$arr_vars[] = JournalPrivacyPreferenceType::$FRIENDS;	
					$arr_vars[] = JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS;
				}
			
				// if traveler is an advisor, check if viewer is a member of the advisor group
				if( isset($obj_admin_group) && $obj_admin_group->isMember( $obj_traveler ) ){
					$arr_vars[] = JournalPrivacyPreferenceType::$GROUP_MEMBERS;
					$arr_vars[] = JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS;	
				}
				// check if viewer is a groupmate of the traveler
				elseif ( isset($obj_viewed_traveler)){
					$isGroupMate = false;
					$objTravelersGroups = $obj_traveler->getGroups();
					$objViewedTravelersGroups = $obj_viewed_traveler->getGroups();
					if ( $objTravelersGroups!= NULL && $objViewedTravelersGroups!= NULL){
						foreach($objTravelersGroups as $eachGroup){
							foreach($objViewedTravelersGroups as $eachViewedGroup){
								if ($eachGroup->getGroupID() == $eachViewedGroup->getGroupID()){
									$arr_vars[] = JournalPrivacyPreferenceType::$GROUP_MEMBERS;
									$arr_vars[] = JournalPrivacyPreferenceType::$GROUP_MEMBERS_AND_FRIENDS;
									break;
								}
							}
						}
					}
				}
			}
		}
		
		return implode(',', $arr_vars);
	}
}
?>