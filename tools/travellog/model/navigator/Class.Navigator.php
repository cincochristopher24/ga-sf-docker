<?

class NavigatorFactory
{

	private static 

	$DASHBOARD_HOME   = 'dashboard.php',
	
	$TRAVELER_HOME    = 'traveler.php',
	
	$TRAVEL_HOME      = 'travel.php',
	
	$GROUP_HOME       = 'group.php',
	
	$DESTINATION_HOME = 'destination.php',
	
	$_OBJECT_    = NULL,
	
	$_ISHOME_    = NULL,
	
	$_HASDATA_   = NULL;
	
	private static
	
	$arr_dashboard_link   = array(),
	
	$arr_traveler_link    = array(),
	
	$arr_travel_link      = array(),
	
	$arr_group_link       = array(),
	
	$arr_destination_link = array();
	
	
	public static function instance()
 	{
 		if (self::$_OBJECT_ == null ):
 			self::$_OBJECT_ = new Navigator();
 		endif;
 		return self::$_OBJECT_;	
 	}
	
	function CreateLinks()
	{
		if ( isset($_SERVER['HTTP_REFERER']) )
		{
			$_FILE_   = basename($_SERVER['HTTP_REFERER']);
			$_ISHOME_ = false;
			switch( $_FILE_ )
			{
				case self::$DASHBOARD_HOME:
					$_ISHOME_ = true;
					$this->DashboardNavLinks();
					break;
				case self::$TRAVELER_HOME:
					$_ISHOME_ = true;
					break;
				case self::$TRAVEL_HOME:
					$_ISHOME_ = true;
					break;
				case self::$GROUP_HOME:
					$_ISHOME_ = true;
					break;
				case self::$DESTINATION_HOME:
					$_ISHOME_ = true;
					break;
			}
		}
	}
	
}

?>
