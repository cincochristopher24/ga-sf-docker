<?php
/**
 * @(#) Class.GroupUser.php
 * 
 * @author Antonio Pepito Cruda Jr.
 * @version 1.0 - August 7, 2009
 */

/**
 * The current user of group.
 */
class GroupUser 
{
  /**
   * @var  GroupUser  The single instance of GroupUser.
   */
  private static $sInstance = null;
  
  /**
   * @var  integer  The ID of the current user.
   */
  private $mUserID = 0;
  
  /**
   * @var  boolean  The flag that holds the state whether the current user is logged or not.
   */
  private $mIsLogged = false;
  
  /**
   * Returns the single instance of GroupUser.
   * 
   * @return  GroupUser  Returns the single instance of GroupUser.
   */
  public function getInstance()
  {
    if (is_null(self::$sInstance))
    {
      self::$sInstance = new GroupUser();
    }
    
    return self::$sInstance;
  }
  
  /**
   * Constructs the current user of the group.
   */
  private function __construct()
  {
    require_once("travellog/model/Class.SessionManager.php");
    
    $session = SessionManager::getInstance();
    $this->mUserID = $session->get('travelerID');
    
  }
}
	
