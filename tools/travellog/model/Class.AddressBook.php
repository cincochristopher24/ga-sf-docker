<?php
	
	require_once('Class.AddressBookEntry.php');
			
	class AddressBook{
		private $mOwnerTravlerID = null;
		
		function __construct($ownerTravelerID=null){
			if($ownerTravelerID !== null){
				$this->setOwnerTravelerID($ownerTravelerID);
			}
			else{
				throw new exception('OwnerTravelerID is required in object of type AddressBook.');
			}
		}
		
		private function setOwnerTravelerID($param){
			$this->mOwnerTravelerID = $param;
		}
		
		public function getAddressBookEntries(){
			return AddressBookEntry::getAddressBookEntriesByOwnerTravelerID($this->getOwnerTravelerID());
		}
		
		public function getOwnerTravelerID(){
			return $this->mOwnerTravelerID;
		}
		
		public function getNotableAddressBookEntries(){
			$ar = array();
			$entries = AddressBookEntry::getAddressBookEntriesByOwnerTravelerID($this->getOwnerTravelerID());
			foreach($entries as $entry){
				if($entry->isNotable() && $entry->isAccepted()){
					$ar[] = $entry;
				}
			}
			return $ar;
		}
	}
?>