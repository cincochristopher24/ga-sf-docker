<?

/**
 * Created on Jul 31, 2006
 * @author Joel C. LLano
 * Purpose: 
 */
require_once('Class.PathManager.php');
require_once('Class.UploadValidatorFactory.php');
require_once('Class.Files.php');
require_once('Class.ImageBuilder.php');
require_once("amazon/Class.GANETAmazonS3WebService.php");

Class UploadManager extends Files {
	
	private $typeupload		= NULL;							//type of file to upload ex: image, PDF, vidoe, doc...etc....
	private $arr_showresults	= NULL;
	private $arr_valid			= array();
	private $arr_invalid		= array();
	
	private $objcontext		= NULL;
	
	private $destination		= NULL;
	
	private $arr_document_titles = array();
	
	function __autoload($class_name) {
  		 //require_once('Class.'. $class_name . '.php');
	}	
	
		
	public function setTypeUpload($typeupload){
		$this->typeupload = $typeupload;	
	}
	
	public function getTypeUpload(){
		return $this->typeupload;	
	}
	
	function setContext($classcontext){
		$this->objcontext = $classcontext;	
	}
	
	function getContext(){
		return $this->objcontext;	
	}
	
	function setDestination($destination){
		$this->destination = $destination;
	}
	
	function getDestination(){
		return $this->destination;
	}
	
	function setDocumentTitles($titles=array()){
		$this->arr_document_titles = $titles;
	}
		
	function upload() {
		
					
			$files = parent::getFiles();
			
			
			$uploadvalidatorfactory = new UploadValidatorFactory();
			$uploadvalidatorfactory->setFiles($files);
			$uploadvalidatorfactory->setDocumentTitles($this->arr_document_titles);
			$uploadvalidatorfactory->create($this->getTypeUpload());
			
			$this->arr_showresults 	= $uploadvalidatorfactory->getValidator()->getResults();			
			$this->valid 			= $uploadvalidatorfactory->getValidator()->getValidFiles();
			$this->invalid			= $uploadvalidatorfactory->getValidator()->getInValidFiles();
			
			
			//check if no folder yet then create forlder
			if(!file_exists($this->destination->getAbsolutePath())){	
				$this->destination->CreatePath();
			}	
			
			if(count($this->valid)){
				switch ($this->typeupload){ 
					case "image":
						$ImgBuilder = ImageBuilder::getInstance();	
						
						foreach($this->valid as $files){
							$ImgBuilder->setFilename($files->getName());
							$ImgBuilder->setImageFile($_SERVER['DOCUMENT_ROOT']."/users/".$files->getName());
							$ImgBuilder->setImagetype($files->getFType());
							$ImgBuilder->setDestination($this->destination->getAbsolutePath());
							$ImgBuilder->setRelativePath($this->destination->GetPathrelative());
							$ImgBuilder->bake($this);
							
						}
						
					break;					
					case "PDF":
						foreach($this->valid as $files){
							rename(PathManager::$temporarypath.$files->getName(),$this->destination->getAbsolutePath().$files->getName());
						}
					
					break;
				
				}
					
			}
			
		
			
	} 
	
	public function show(){
		return $this->arr_showresults;
	}
	
	public function getValid(){
		return $this->valid;	
	}
	
	public function getInValid(){
		return $this->invalid;	
	}
	
	public function deletefile($filename){
		
		//$path 	= new PathManager($this->objcontext,$this->typeupload);	# Delete the temporary uploaded image
		
		$this->destination->getAbsolutePath();												
		
		switch ($this->typeupload){ 
			case "image":
				$relPath = $this->destination->GetPathrelative();
				$tempRawFilesToRemove = array();
				
				if(file_exists($this->destination->getAbsolutePath()."fullsize".$filename)){
					unlink($this->destination->getAbsolutePath()."fullsize".$filename);
					$tempRawFilesToRemove[] = "fullsize".$filename;
				}
				
				if(file_exists($this->destination->getAbsolutePath()."standard".$filename)){	
					unlink($this->destination->getAbsolutePath()."standard".$filename);
					$tempRawFilesToRemove[] = "standard".$filename;
				}
				
				//unlink($_SERVER['DOCUMENT_ROOT']."/".$path->GetPathrelative()."default".$filename);
				
				if(file_exists($this->destination->getAbsolutePath()."thumbnail".$filename)){
					unlink($this->destination->getAbsolutePath()."thumbnail".$filename);
					$tempRawFilesToRemove[] = "thumbnail".$filename;
				}
				
				if(file_exists($this->destination->getAbsolutePath()."featured".$filename)){
					unlink($this->destination->getAbsolutePath()."featured".$filename);
					$tempRawFilesToRemove[] = "featured".$filename;
				}
					
				if(file_exists($this->destination->getAbsolutePath()."orig-".$filename)){
					unlink($this->destination->getAbsolutePath()."orig-".$filename);
					$tempRawFilesToRemove[] = "orig-".$filename;
				}
				
				if(file_exists($this->destination->getAbsolutePath().$filename.".txt")){
					unlink($this->destination->getAbsolutePath().$filename.".txt");
				}
				
				if(file_exists($this->destination->getAbsolutePath()."cropthumb".$filename)){
					unlink($this->destination->getAbsolutePath()."cropthumb".$filename);
					$tempRawFilesToRemove[] = "cropthumb".$filename;
				}
				
				if(file_exists($this->destination->getAbsolutePath()."jentryheader".$filename)){
					unlink($this->destination->getAbsolutePath()."jentryheader".$filename);
					$tempRawFilesToRemove[] = "jentryheader".$filename;
				}
				
				if(file_exists($this->destination->getAbsolutePath()."jentrylboxheader".$filename)){
					unlink($this->destination->getAbsolutePath()."jentrylboxheader".$filename);
					$tempRawFilesToRemove[] = "jentrylboxheader".$filename;
				}
				
				//for custom page header photos
				if(file_exists($this->destination->getAbsolutePath()."customheaderstandard".$filename)){
					unlink($this->destination->getAbsolutePath()."customheaderstandard".$filename);
					$tempRawFilesToRemove[] = "customheaderstandard".$filename;
				}
				if(file_exists($this->destination->getAbsolutePath()."customheaderfullsize".$filename)){
					unlink($this->destination->getAbsolutePath()."customheaderfullsize".$filename);
					$tempRawFilesToRemove[] = "customheaderfullsize".$filename;
				}
				
				$ganetAWS = GANETAmazonS3WebService::getInstance();
				$ganetAWS->removeRawFilesFromS3($tempRawFilesToRemove,$relPath);
			
			break;
			
			case "PDF":
				$relPath = $this->destination->GetPathrelative();
				$tempRawFilesToRemove = array();
				
				if(file_exists($this->destination->getAbsolutePath().$filename)){
					unlink($this->destination->getAbsolutePath().$filename);
					$tempRawFilesToRemove[] = $filename;
				}
				
				$ganetAWS = GANETAmazonS3WebService::getInstance();
				$ganetAWS->removeRawFilesFromS3($tempRawFilesToRemove,$relPath);
			break;
				
		}
	}
	
}


	
?>