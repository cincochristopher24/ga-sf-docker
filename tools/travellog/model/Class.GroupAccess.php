<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: abstracts different types of group access
 */
 
    class GroupAccess {  
    
        public static  $OPEN        	= 1;
        public static  $OPEN_APPROVAL   = 2;
        public static  $INVITE_ONLY 	= 3;  
 		public static  $CLOSE       	= 4;

		const OPEN_ACCESS				= 1;
		const OPEN_APPROVAL				= 2;
		const INVITE_ONLY				= 3;
		const CLOSED					= 4;
    
    }   
?>
