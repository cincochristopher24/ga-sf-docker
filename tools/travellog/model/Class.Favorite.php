<?
/**
* <b>Favorite</b> class with integrated CRUD methods.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/

require_once("Class.Connection.php");
require_once("Class.Recordset.php");

class FavoriteBase
{
	
	protected $favoriteID;
	
	protected $travelerID;
	
	protected $favoritetypeID;
	
	protected $genID;
	
	protected $title;
	
	protected $url;
		
	protected $dateadded;
	
	protected $totalrecords;
	
	function __construct()
	{
		return $this;
	}
	
	function Get()
	{
		$conn = new Connection();
		$rs = new Recordset($conn);
		$query = "select  `travelerID`, `favoritetypeID`, `genID`, `title`, `url`, `favoriteID` ,`dateadded` from `tblFavorite` where `favoriteID`='".intval($this->favoriteID)."' and travelerID='".intval($this->travelerID)."' LIMIT 1";
		$rs->Execute($query);
		$this->favoriteID = $rs->Result(0,"favoriteID");
		$this->travelerID = $rs->Result(0,"travelerID");
		$this->favoritetypeID = $rs->Result(0,"favoritetypeID");
		$this->genID = $rs->Result(0,"genID");
		$this->title = $rs->Result(0,"title");
		$this->url = $rs->Result(0,"url");
		$this->dateadded = $rs->Result(0,"dateadded");
		$rs->Close();
		return $this;
	}
	
	//function GetList($page, $rowsperpage)
	function GetList()
	{
		//$page = $page*$rowsperpage-$rowsperpage;
		/*$conn = new Connection();
		$rs = new Recordset($conn);
		$rs1 = new Recordset($conn);
		$query = "select SQL_CALC_FOUND_ROWS `travellerID`, `favoritetypeID`, `genID`, `title`, `url`, `favoriteID` ,`dateadded` from `tblFavorite` where `travellerID`='".$this->travellerID."' LIMIT ".$page.",".$rowsperpage;
		$rs->Execute($query);
		$query1 = "select FOUND_ROWS() AS totalrec ";
		$rs1->Execute($query1);
		$this->totalrecords = $rs1->Result(0, "totalrec");*/
		$conn = new Connection();
		$rs = new Recordset($conn);
		$query = "select `travelerID`, `favoritetypeID`, `genID`, `title`, `url`, `favoriteID` ,`dateadded` from `tblFavorite` where `travelerID`=".intval($this->travelerID);
		$results = $rs->Execute($query);
		$rs->Close();
		$favorites = array();
	
		while($row=mysql_fetch_assoc($results))
		{
			$favorite = new Favorite( );
			$favorite->setFavoriteID($row['favoriteID']);
			$favorite->setTravelerID($row['travelerID']);
			$favorite->setFavoritetypeID($row['favoritetypeID']);
			$favorite->setGenID($row['genID']);
			$favorite->setTitle($row['title']);
			$favorite->setUrl($row['url']);
			$favorite->setDateAdded($row['dateadded']);
			$favorites[] = $favorite; 
		}	
		return $favorites;
	}
	
	public static function GetPkByGenIDAndTraveler( $genID, $travelerID, $type )
	{
		$mConn = new Connection();
		$mRs = new Recordset( $mConn );

		$query = "SELECT favoriteID FROM tblFavorite " .
				"WHERE genID = " . $genID . " AND travelerID = " . $travelerID . " AND favoritetypeID = " . $type;
		$mRs->Execute( $query );

		if (0 < $mRs->Recordcount())
		{
			return $mRs->Result(0, "favoriteID");
		}
		else
			return 0;
	}
	
	function TotalRecords()
	{
		return $this->totalrecords; 
	}
	
	function Save()
	{
		$conn = new Connection();
		$rs = new Recordset($conn);
		$query = "select `favoriteID` from `tblFavorite` where `favoriteID`='".$this->favoriteID."' LIMIT 1";	
		$rs->Execute($query);
		$this->favoriteID = $rs->Result(0,"favoriteID");
		if ( $rs->Recordcount() )
		{
			$query = "update `tblFavorite` set 
					 `genID` ='".$this->genID."',
					 `title`='".$this->title."',
					 `url`='".$this->url."'
					   where `favoriteID`='".intval($this->favoriteID)."' and `travelerID`='".intval($this->travelerID)."'";
		}
		else
		{
			$query = "insert into `tblFavorite` (`travelerID`,`genID`,`title`,`url`,`favoritetypeID`) values
					 ('".$this->travelerID."','".$this->genID."','".$this->title."','".$this->url."','".$this->favoritetypeID."')"; 
		}		
		$rs->Execute($query);
		if ( $this->favoriteID == '')
		{
			$this->favoriteID = $rs->GetCurrentId();
		}
		$rs->Close();		
		return $this->favoriteID;
	}		
	
	function SaveNew()
	{
		$this->favoriteID = "";
		return $this->Save();		
	}		
	
	function Delete()
	{
		$conn = new Connection();
		$rs = new Recordset($conn);
		$query = "delete from `tblFavorite` where `favoriteID`='".intval($this->favoriteID)."' and `travelerID`='".intval($this->travelerID)."'";
		$rs->Execute($query);
		$rs->Close();
	}	
	
}


class Favorite extends FavoriteBase 
{
	
		function __construct() {parent::__construct();}
		
		function Delete() {return parent::Delete();}	
	
		function SaveNew() {return parent::SaveNew();}
		
		function Save() {return parent::Save();}
		
		function GetList() {return parent::GetList();}
		
		function Get() {return parent::Get();}
		
		function setFavoriteID($_favoriteID)
		{
			$this->favoriteID = $_favoriteID;	
		}
		function setTravelerID($_travelerID)
		{
			$this->travelerID = $_travelerID;	
		}
		function setFavoritetypeID($_favoritetypeID)
		{
			$this->favoritetypeID = $_favoritetypeID;	
		}
		function setGenID($_genID)
		{
			$this->genID = $_genID;	
		}
		function setTitle($_title)
		{
			$this->title = $_title;	
		}
		function setUrl($_url)
		{
			$this->url = $_url;	
		}							
		function setDateAdded($_dateadded)
		{
			$this->dateadded = $_dateadded;	
		}			
		
		
		function getFavoriteID()
		{
			return $this->favoriteID;	
		}
		function getTravelerID()
		{
			return $this->travelerID;	
		}
		function getFavoritetypeID()
		{
			return $this->favoritetypeID;	
		}
		function getGenID()
		{
			return $this->genID;	
		}
		function getTitle()
		{
			return $this->title;	
		}
		function getUrl()
		{
			return $this->url;	
		}
		function getDateAdded()
		{
			return $this->dateadded;	
		}
}	

?>