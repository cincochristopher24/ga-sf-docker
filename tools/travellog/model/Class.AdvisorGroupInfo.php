<?php
  /**
   * @(#) Class.AdvisorGroupInfo.php
   * 
   * handles the advisor group information
   * 
   * Created on Mar 22, 2007
   * @author daf
   */
 
  require_once "Class.dbHandler.php";
 
 	class AdvisorGroupInfo {
 	
		/**
		 * Define variables for Class Member Attributes
		 */
		private $mGroupID      = 0;
		private $mClientID     = 0;
		private $mWebURL       = '';
		private $mPhone       	= '';
		private $mFax       	= '';
		private $mAddress1     = '';
		private $mAddress2     = '';
		private $mAddress3     = '';
		private $mCity       	= '';
		private $mState       	= '';
		private $mZip       	= '';
		private $mCountryID    = 0;
		private $mCountry		= '';
		private $mMission      = '';
		    
		private $mIsExist     = false;
		     
		/**
		 * constructor
		 */
		function AdvisorGroupInfo($groupID = 0){ 
      $this->mGroupID = $groupID;
		  
		  try {
		  	$this->mConn = new Connection();
		    $this->mRs   = new Recordset($this->mConn);
		  }
		  catch (Exception $e) {                 
		  	throw $e;
		  }
                
      if (0 < $this->mGroupID){           
        $sql = "SELECT * FROM tblAdvisorGroupInfo WHERE groupID = '$this->mGroupID' ";
        $this->mRs->Execute($sql);
                	
        if ($this->mRs->Recordcount()){
          $this->mIsExist	  = true;
	                    
	        // Sets values to its class member attributes.
          $this->mWebURL    = $this->mRs->Result(0,"weburl");           
          $this->mPhone     = $this->mRs->Result(0,"phone");       
          $this->mFax    		= $this->mRs->Result(0,"fax");           
          $this->mAddress1  = $this->mRs->Result(0,"address1");    
          $this->mAddress2  = $this->mRs->Result(0,"address2");    
          $this->mAddress3  = $this->mRs->Result(0,"address3");    
          $this->mCity      = $this->mRs->Result(0,"city");    
          $this->mState     = $this->mRs->Result(0,"state");    
          $this->mZip      	= $this->mRs->Result(0,"zip");    
          $this->mCountryID = $this->mRs->Result(0,"countryID");
          $this->mMission   = $this->mRs->Result(0,"mission");    
        }
      }
		    	
		}
		    
		/**
     * Setter Functions
     */    
    function setClientID($_clientID){
      $this->mClientID = $_clientID;
    }
            
    function setWebURL($_webURL){
      $this->mWebURL = $_webURL;
    }
            
    function setPhone($_phone){
      $this->mPhone = $_phone;
    }
            
    function setFax($_fax){
      $this->mFax = $_fax;
    }
            
    function setAddress1($_address1){
      $this->mAddress1 = $_address1;
    }
            
    function setAddress2($_address2){
      $this->mAddress2 = $_address2;
    }
            
    function setAddress3($_address3){
      $this->mAddress3 = $_address3;
    }
            
    function setCity($_city){
      $this->mCity = $_city;
    }
            
    function setState($_state){
      $this->mState = $_state;
    }
            
    function setZip($_zip){
      $this->mZip = $_zip;
    }
            
    function setCountryID($_countryID){
      $this->mCountryID = $_countryID;
    }
            
    function setMission($_mission){
      $this->mMission = $_mission;
    }
            
		
		/**
     * Getter Functions
     */    
    
    function getGroupID(){
      return $this->mGroupID ;
    }
            
    function getClientID(){
      return $this->mClientID ;
    }
            
    function getWebURL(){
      return $this->mWebURL ;
    }
            
    function getPhone(){
      return $this->mPhone ;
    }
            
    function getFax(){
      return $this->mFax ;
    }
            
    function getAddress1(){
      return $this->mAddress1 ;
    }
            
    function getAddress2(){
      return $this->mAddress2 ;
    }
            
    function getAddress3(){
      return $this->mAddress3 ;
    }
            
    function getCity(){
      return $this->mCity ;
    }
            
    function getState(){
      return $this->mState ;
    }
            
    function getZip(){
      return $this->mZip ;
    }
            
    function getCountryID(){
      return $this->mCountryID ;
    }
            
    function getMission(){
      return $this->mMission ;
    }

    function getCountry(){
      $handler = new dbHandler();
      
      if (0 < $this->mCountryID) {
        $sql = "SELECT country FROM GoAbroad_Main.tbcountry " .
          " WHERE " .
          " countryID = ".$handler->makeSqlSafeString($this->mCountryID)."" ;                       
	          
        $this->mRs->Execute($sql);       
        $this->mCountry = $this->mRs->Result(0,"country");
      }	
      
      return $this->mCountry;
    }
    		
    function Save(){
      $handler = new dbHandler();
          
      if (0 == $this->mClientID) {
        if (!$this->mIsExist) {
          $sql = "INSERT into tblAdvisorGroupInfo(" .
            " groupID, " .
            " weburl, " .
            " phone, " .
            " fax, " .
            " address1, " .
            " address2, " .
            " address3, " .
            " city, " .
            " state, " .
            " zip, " .
            " countryID, " .
            " mission" .
            " ) VALUES (" .
            " ".$handler->makeSqlSafeString($this->mGroupID).", " .
            " ".$handler->makeSqlSafeString($this->mWebURL).", " .
            " ".$handler->makeSqlSafeString($this->mPhone).", " .
            " ".$handler->makeSqlSafeString($this->mFax).", " .
            " ".$handler->makeSqlSafeString($this->mAddress1).", " .
            " ".$handler->makeSqlSafeString($this->mAddress2).", " .
            " ".$handler->makeSqlSafeString($this->mAddress3).", " .
            " ".$handler->makeSqlSafeString($this->mCity).", " .
            " ".$handler->makeSqlSafeString($this->mState).", " .
            " ".$handler->makeSqlSafeString($this->mZip).", " .
            " ".$handler->makeSqlSafeString($this->mCountryID).", " .
            " ".$handler->makeSqlSafeString($this->mMission)." ".
            ")";
        }
        else {
          $sql = "UPDATE tblAdvisorGroupInfo SET " .
            " weburl    = ".$handler->makeSqlSafeString($this->mWebURL).", " .
            " phone     = ".$handler->makeSqlSafeString($this->mPhone).", " .
            " fax       = ".$handler->makeSqlSafeString($this->mFax).", " .
            " address1  = ".$handler->makeSqlSafeString($this->mAddress1).", " .
            " address2  = ".$handler->makeSqlSafeString($this->mAddress2).", " .
            " address3  = ".$handler->makeSqlSafeString($this->mAddress3).", " .
            " city      = ".$handler->makeSqlSafeString($this->mCity).", " .
            " state     = ".$handler->makeSqlSafeString($this->mState).", " .
            " zip       = ".$handler->makeSqlSafeString($this->mZip).", " .
            " countryID = ".$handler->makeSqlSafeString($this->mCountryID).", " .
            " mission   = ".$handler->makeSqlSafeString($this->mMission)."".
            " WHERE " .
            " groupID   = ".$handler->makeSqlSafeString($this->mGroupID)."" ;
        }
      }
      else {
        $sql = "UPDATE GoAbroad_Main.tbclient SET " .
          " weburl    = ".$handler->makeSqlSafeString($this->mWebURL).", " .
          " phone     = ".$handler->makeSqlSafeString($this->mPhone).", " .
          " fax       = ".$handler->makeSqlSafeString($this->mFax).", " .
          " address1  = ".$handler->makeSqlSafeString($this->mAddress1).", " .
          " address2  = ".$handler->makeSqlSafeString($this->mAddress2).", " .
          " address3  = ".$handler->makeSqlSafeString($this->mAddress3).", " .
          " city      = ".$handler->makeSqlSafeString($this->mCity).", " .
          " state     = ".$handler->makeSqlSafeString($this->mState).", " .
          " zip       = ".$handler->makeSqlSafeString($this->mZip).", " .
          " countryID = ".$handler->makeSqlSafeString($this->mCountryID).", " .
          " mission   = ".$handler->makeSqlSafeString($this->mMission)."".
          " WHERE " .
          " clientID  = ".$handler->makeSqlSafeString($this->mClientID)."" ;    
      }
          
      $this->mRs->Execute($sql);
          
      if (!$this->mIsExist)   
        $this->mGroupID = $this->mRs->GetCurrentID();
              
    }
  }