<?php

	/**
	 * Class.StudentProfile.php
	 * @author marc
	 * @var array countryChoices - array of student's country options
	 * @var object preference - see Class.StudentPreference.php
	 */
	
	require_once ('Class.TravelerProfile.php');
	require_once ('Class.StudentPreference.php');
	require_once ('Class.Connection.php');
	require_once ('Class.Recordset.php');
	require_once ('Class.Country.php');
	
		
	class StudentProfile{

		private $countryChoices;
		private $preference;
		private $conn;
		private $rs;
		private $travelerID;
		
		// constructor
		function __construct( $travelerID ){
			
			$this->conn = new Connection();
			$this->rs = new Recordset($this->conn);

			$this->setTravelerID( $travelerID );
			$this->getCountryChoices();
			$this->getStudentPreferences();
			
		}
		
		function setTravelerID( $travelerID ){
			$this->travelerID = $travelerID;
		}
		
		function getTravelerID(){
			return $this->travelerID;
		}
		
		/**
		 * generates student's Country/Program options
		 */
		/* function generateAllPreference(){
			$this->getCountryChoices();
			$this->getStudentPreferences();
		}*/
	
		/**
		 * @param array countryChoice
		 */
		function setCountryChoice($countryChoice){
			$this->countryChoices = $countryChoice;
		}	
	
		/**
		 * @return array student's country options
		 */
		function getCountryChoice(){
			return $this->countryChoices;
		}
	
		/**
		 * @param object preference - see Class.StudentPreference.php
		 */
		function setPreference($preference){
			$this->preference = $preference;
		}
	
		/**
		 * @return object student's program options, see Class.StudentPreference.php
		 */
		function getPreference(){
			return $this->preference;
		}
		
		/**
		 * retrieves all cuntry options of student
		 */
		function getCountryChoices(){
			
			$sqlquery = "SELECT countryID FROM tblTravelertoCountry ".
					"WHERE travelerID=" . $this->getTravelerID() .
					" GROUP BY countryID ORDER BY tbltravelertocountryID";
			$myquery = $this->rs->execute($sqlquery);
			$countryList = array();
			while ( $row = mysql_fetch_array($myquery)){
				$nCountry = new Country($row["countryID"]);
				$countryList[] = $nCountry;
			}	
			$this->setCountryChoice($countryList);
		}
		
		/**
		 * updates student's country options
		 * @param array countryChoice
		 */
		function updateCountryChoices($countryChoice){
			
			$this->deleteCountryChoices();			
			if ( count($countryChoice) )
				foreach ($countryChoice as $countryID){
					$sqlquery = "INSERT INTO tblTravelertoCountry(travelerID,countryID)" .
							"VALUES(" . $this->getTravelerID() . "," . $countryID . ")";
					$this->rs->execute($sqlquery); 
				}
		}
		
		/**
		 * deletes student's country options
		 */
		function deleteCountryChoices(){
			
			$sqlquery = "DELETE FROM tblTravelertoCountry WHERE travelerID=" . $this->getTravelerID();
			$this->rs->execute($sqlquery);
		}
		
		function getStudentPreferences(){
			
			$studPref = new studentPreference(0,$this->getTravelerID());
			$this->setPreference($studPref);
			
		}
		
		/**
		 * @param object preference - see Class.StudentPreference.php
		 */
		function updateStudentPreference($preference){
			$this->deleteStudentPreferences();
			
			$arrPreference = $preference->getPreference();
			if ( count($arrPreference) )
				foreach($arrPreference as $program){
					if($program->getProgramID() == 2){
						$sqlquery = "INSERT INTO tblTravelertoIncProgram(travelerID,incprogramID,incprogramtypeID) " .
							"VALUES(" . $this->getTravelerID() . ",2,0)";
						$myquery = $this->rs->execute($sqlquery);
					}
					else{
						$arrProgramType = $program->getProgramType();
						if (count($arrProgramType))
							foreach($arrProgramType as $programType){
								$sqlquery = "INSERT INTO tblTravelertoIncProgram(travelerID,incprogramID,incprogramtypeID) " .
								"VALUES(" . $this->getTravelerID() . "," . $program->getProgramID() . "," . $programType->getProgramTypeID() . ")";
								$myquery = $this->rs->execute($sqlquery);
							}	
					}
				}
				
		}
		
		function deleteStudentPreferences(){
			$this->conn = new Connection();
			$this->rs = new Recordset($this->conn);
			$sqlquery = "DELETE FROM tblTravelertoIncProgram WHERE travelerID=" .$this->getTravelerID();
			$this->rs->Execute($sqlquery);
		}
		
		/**
		 * checks if student's profile exist
		 * @param bigint _TravelerID
		 */
		function studentProfileExists($_TravelerID){
			$myquery = $this->rs->execute("SELECT travelerID FROM tblTravelertoIncProgram WHERE travelerID=" . $_TravelerID . 
						" UNION SELECT travelerID FROM tblTravelertoCountry WHERE travelerID=" . $_TravelerID);
			if ( mysql_num_rows($myquery) )
				return TRUE;
			else
				return FALSE;				 
		}
	}

?>