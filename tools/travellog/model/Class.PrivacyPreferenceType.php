<?php
/*
 * Created on 09 27, 2006
 * @author Kerwin Gordo
 * Purpose: 
 */
 
 class PrivacyPreferenceType {
 	
 	/***********************************************************************************
 	 * edits of neri:
 	 * 		added RECEIVE_UPDATES											01-08-09
 	 * 		added NOTIFICATION_FROM_COPARTICIPANTS_OF_DISCUSSION and 
 	 * 			NOTIFICATION_FROM_SUBGROUP_DISCUSSION_ACTIVITIES			01-26-09
 	 ***********************************************************************************/	
 	
 	// traveler privacy preference 
 	public static $SHOW_NAMES = 11;
 	public static $SHOW_EMAIL = 12; 	
 	public static $SHOW_GROUPS = 13;
 	public static $SHOW_FRIENDS = 14;
 	public static $SHOW_ONLINE_STATUS = 15;
 	public static $SHOW_CALENDAR = 16;
 	public static $SHOW_RESUME = 17;
 	public static $RECEIVE_UPDATES = 18;			
 	public static $NOTIFICATION_FROM_COPARTICIPANTS_OF_DISCUSSION = 19;
 	public static $NOTIFICATION_FROM_SUBGROUP_DISCUSSION_ACTIVITIES = 20;
 		
 	// group privacy preference categories
 	public static $SHOW_GROUP_ALERTS = 4;
 	public static $SHOW_GROUP_SUBGROUPS = 5;
 	public static $SHOW_GROUP_BULLETIN = 6;
 	public static $SHOW_GROUP_FILES = 7;
 	public static $SHOW_GROUP_CALENDAR = 8;
	public static $APPROVE_AND_FEATURE_GROUP_JOURNALS = 9;
	
	}
 
?>
