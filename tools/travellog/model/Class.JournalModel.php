<?php
class JournalModel{
	function Get( $id ){
		require_once("travellog/model/Class.Travel.php");
		require_once("travellog/vo/Class.JournalVO.php");
		$obj_travel        = new Travel( $id );
		$obj_journal_vo    = new JournalVO;
		$obj_group         = Travel::getRelatedGroup($id);
		
		if( $obj_group != false ){
			$has_more_journals = Travel::hasMoreThanOneJournal( $obj_group->getGroupID() );
			$is_group_approved = $obj_travel->isApproved($obj_group->getGroupID());
		}
		else{
			$obj_group         = NULL;
			$has_more_journals = false;
			$is_group_approved = false;
		}
		
		$obj_journal_vo->setDraftsCount($obj_travel->getJournalEntryDraftsCount());
		$obj_journal_vo->setTravelID   ( $obj_travel->getTravelID()    );
		$obj_journal_vo->setTravelerID ( $obj_travel->getTravelerID()  );
		$obj_journal_vo->setTitle      ( $obj_travel->getTitle()       );
		$obj_journal_vo->setDescription( $obj_travel->getDescription() );
		$obj_journal_vo->setTrips      ( $obj_travel->getTrips()       );
		$obj_journal_vo->setOwner      ( $obj_travel->getOwner()       );
				
		$obj_journal_vo->setGroup                ( $obj_group         );
		$obj_journal_vo->setIsGroupApproved      ( $is_group_approved );
		$obj_journal_vo->setHasMoreThanOneJournal( $has_more_journals );
		
		return $obj_journal_vo;
	}
}
?>
