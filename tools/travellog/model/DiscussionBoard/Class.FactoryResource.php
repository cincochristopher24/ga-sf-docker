<?php
	class FactoryResource
	{
		private $mRealTotal = 0;
		private $mInstances = array();
		
		private function __construct($params = array()){
			$this->mRealTotal = $params['realTotal'];
			$this->mInstances = $params['instances'];
		}
		
		public function getRealTotal(){
			return $this->mRealTotal;
		}
		
		public function getInstances(){
			return $this->mInstances;
		}
		
		public function getInstance($idx=0){
			return array_key_exists($idx,$this->mInstances) ? $this->mInstances[$idx] : null;
		}
		
		static public function create($params){
			return new self($params);
		}
		
	}
?>