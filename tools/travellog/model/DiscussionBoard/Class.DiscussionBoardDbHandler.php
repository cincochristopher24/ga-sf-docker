<?php
	/**
	 * @(#) Class.DiscussionBoardDbHandler.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 6, 2009
	 */
	 
	require_once("Class.dbHandler.php");
	
	/**
	 * Singleton dbHandler class for GANET.
	 */
	class DiscussionBoardDbHandler {
		
		/**
		 * Stores the instance of the class.
		 * 
		 * @staticvar dbHandler
		 */
		static private $sDbHandler = null;
		
		/**
		 * Returns the singleton dbHandler.
		 * 
		 * @return dbHandler the singleton dbHandler.
		 */
		function getDbHandler(){
			if (is_null(self::$sDbHandler)) {
				self::$sDbHandler = new dbHandler();
				self::$sDbHandler->setDB("DiscussionBoard");
				
				if (isset($_REQUEST['GANET_TEST']) OR !isset($_SERVER['SERVER_NAME'])) { // for phpunit testing
					self::$sDbHandler->setDB("DISCUSSION_BOARD_TEST");
				}
			}
			
			return self::$sDbHandler;
		}
		
	}
	
