<?php
	require_once('travellog/model/Class.Traveler.php');
	
	class Poster
	{
		const NOT_MEMBER	= 0;
		const MEMBER 		= 1;
		const STAFF			= 2;
		const ADMINISTRATOR	= 3;
		
		static private $DISIGNATIONS = array(
			self::NOT_MEMBER 	=> 'Not Member',
			self::MEMBER		=> 'Member',
			self::STAFF			=> 'Group Staff',
			self::ADMINISTRATOR	=> 'Group Admin'
		);
		
		private $mTraveler 		= null;
		private $mPost 			= null;
		private $mDiscussion	= null;
		private $mTopic			= null;
		private $mGroup			= null;
		private $mDesignation 	= null;
		private $mCountry = null;
		
		private function __construct($core,$post){
			//check if the traveler is the administrator of the owner group
			$this->mTraveler 	= $core;
			$this->mPost		= $post;
			$this->mDiscussion 	= $this->mPost->getParentDiscussion();
			$this->mTopic 		= $this->mDiscussion->getParentTopic();

			$this->mGroup		= $this->mTopic->getParentGroup();
			if( 0 < $this->mGroup->getParentID() ){
				$this->mGroup = $this->mGroup->getParent();
			}
		}
		
		public function getName(){
			if($this->isAdministrator()){
				return $this->mGroup->getName();
			}
			return $this->mTraveler->getUsername();
			//$profile = $this->mTraveler->getTravelerProfile();
			//return '' != trim($name = $profile->getFirstName().' '.$profile->getLastName()) ? $name : $profile->getUsername();
		}
		
		public function getFullName(){
			if($this->isAdministrator()){
				return "";
			}
			else {
				$profile = $this->mTraveler->getTravelerProfile();
				return $profile->getFirstName().' '.$profile->getLastName();
			}
		}
		
		
		public function getTraveler(){
			return $this->mTraveler;
		}
		
		public function isAdministrator(){
			return $this->mGroup->getAdministratorID() == $this->mTraveler->getTravelerID();
		}
		
		/**
		 * Returns the country of the traveler if it is a traveler. If it has no country 
		 * or the poster is an administrator, this returns false.
		 * 
		 * @return Country|boolean the country ID of the traveler if it is a traveler. Otherwise returns false.
		 */
		public function getCountry(){
			if (is_null($this->mCountry)) {
				$this->mCountry = false;
				
				if (!$this->isAdministrator()) {
					$profile = $this->mTraveler->getTravelerProfile();
					$this->mCountry = ($profile->getCity() != NULL )? $profile->getCity()->getCountry(): false;				
				}
			}
			
			return $this->mCountry;			
		}
		
		public function getProfilePicUrl(){
			if($this->isAdministrator())
				return $this->mGroup->getGroupPhoto()->getPhotoLink('thumbnail'); // administrator
			
			return $this->mTraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');
		}
		
		public function getProfileUrl(){
			if(!$this->isAdministrator())
				return $this->mTraveler->getFriendlyUrl();
			return $this->mGroup->getFriendlyUrl();
		}
		
		public function getUrl(){
			return $this->mGroup->getAdministratorID() == $this->mTraveler->getTravelerID()
				? $this->mGroup->getFriendlyUrl()
				: '/'.$this->mTraveler->getUserName();
		}
		
		public function isSuspended(){
			if(!$this->isAdministrator())
				return $this->mTraveler->isSuspended();
			return false;
		}
		
		static public function createInstance($posterID,$post){
			try{
				$traveler = new Traveler($posterID);
			}
			catch(exception $e){
				return null;
			}
			return new self($traveler,$post);
		}
		
	}
?>