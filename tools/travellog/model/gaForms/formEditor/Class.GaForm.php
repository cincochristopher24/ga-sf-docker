<?php
	require_once('Class.GaDateTime.php');
	require_once('Class.DataModel.php');
	require_once('Class.GaFormField.php');
	require_once('Interface.GaFormSerializable.php');
	require_once('travellog/model/gaForms/Class.GaFormValue.php');
	
	class GaForm extends DataModel implements GaFormSerializable
	{
		public function __construct($keyID=null,$data = null){
			parent::initialize('GaForms', 'tblGaForm');
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid GaFormID');
			}
			//set own defaults
			if(is_null($keyID)){
				$this->setName('');
				$this->setCaption('');
				$this->setGaFormClientID(0);
				$this->setDateCreated(GaDateTime::dbDateTimeFormat());
			}
			if(is_array($data)){
				$this->setFields($data);
			}
		}
		
		public function getFormFields(){
			return GaFormField::getGaFormFields($this->getGaFormID());
		}
		
		public function save(){
			if(!$this->getGaFormID()) $this->setDateCreated(GaDateTime::dbDateTimeFormat());
			parent::save();
		}
		
		public function delete(){
			//delete the answers
			$formValues = $this->getFormValues();
			foreach($formValues as $iFormValue){
				$iFormValue->delete();
			}			
			//delete the fields first
			foreach($this->getFormFields() as $iFormField){
				$iFormField->delete();
			}
			//delete the form
			parent::delete();
		}
		
		public function createNewFormField(){
			$gaFormField = new GaFormField();
			$gaFormField->setGaFormID($this->getGaFormID());
			return $gaFormField;
		}
		
		public function createNewFormValue(){
			$gaFormValue = new GaFormValue();
			$gaFormValue->setGaFormID($this->getGaFormID());
			return $gaFormValue;
		}
		
		public function getFormValues(){
			return GaFormValue::getInstancesByGaFormID($this->getGaFormID());
		}
		
		public function getFormValue($formValueID=0){
			return GaFormValue::getByGaFormIDAndFormValueID($this->getGaFormID(),$formValueID);
		}
		
		public function saveFormData($formData=array()){
			//Deal which attributes where edited.
			if(array_key_exists('name',$formData) && is_string($formData['name'])) $this->setName($formData['name']);
			if(array_key_exists('caption',$formData) && is_string($formData['caption'])) $this->setCaption($formData['caption']);
			
			$this->save();
			//deal with the fields
			$newFormFields = (array_key_exists('fields',$formData) && is_array($formData['fields']) ? $formData['fields'] : array());
			$existingFormFields = $this->getFormFields();
			$presentFormFieldIDs = array();
			foreach($newFormFields as $iNewFormField){
				if(array_key_exists('gaFormFieldID',$iNewFormField) && is_numeric($iNewFormField['gaFormFieldID'])){
					if(0==$iNewFormField['gaFormFieldID']){
						$this->createNewFormField()->saveFieldData($iNewFormField);
					}
					else{//make sure that the fieldID is in the list of existing fields
						if(array_key_exists($iNewFormField['gaFormFieldID'],$existingFormFields)){
							$existingFormFields[$iNewFormField['gaFormFieldID']]->saveFieldData($iNewFormField);
							$presentFormFieldIDs[] = $iNewFormField['gaFormFieldID'];
						}
					}
				}
			}
			
			foreach($existingFormFields as $iExtFormField){
				if(!in_array($iExtFormField->getGaFormFieldID(),$presentFormFieldIDs)){
					$iExtFormField->delete();
				}
			}
		}
		
		public function getJsonSerializableData(){
			$data = $this->mFields;
			$data['gaFormID'] = (is_null($data['gaFormID']) ? 0 : $data['gaFormID']);
			$data['fields']=array();
			$formFields = $this->getFormFields();
			foreach($formFields as $iFormField){
				$data['fields'][] = $iFormField->getJsonSerializableData();
			}
			return $data;
		}
		
		public function getJsonSerializableDuplicateData(){
			$data = $this->mFields;
			$data['gaFormID'] = 0;
			$data['name'] = 'Copy of '.$data['name'];
			$data['fields']=array();
			$formFields = $this->getFormFields();
			foreach($formFields as $iFormField){
				$data['fields'][] = $iFormField->getJsonSerializableDuplicateData();
			}
			return $data;
		}
		
		public function getStatistics(){
			$data = array(
				'totalFormValues' => count($this->getFormValues()),
				'fields' => array()
			);
			foreach($this->getFormFields() as $iFormField){
				$data['fields'][$iFormField->getGaFormFieldID()] = $iFormField->getStatistics();
			}
			return $data;
		}
		
		static public function getGaFormByFormIDAndGaFormClientID($gaFormID=0,$gaFormClientID=0){
			$db = new dbHandler('db=GaForms');
			$sql = "SELECT * FROM tblGaForm 
					WHERE 
						gaFormID = {$db->makeSqlSafeString($gaFormID)} AND  
						gaFormClientID = {$db->makeSqlSafeString($gaFormClientID)}";
			$rs = new iRecordset($db->execute($sql));
			if($rs->retrieveRecordCount() > 0){
				return new GaForm(null, $rs->retrieveRow(0));
			}
			return null;
		}
		
		static public function getMultipleGaFormsByGaFormClientID($gaFormClientID=0){
			$db = new dbHandler('db=GaForms');
			$sql = "SELECT * FROM tblGaForm 
					WHERE gaFormClientID = {$db->makeSqlSafeString($gaFormClientID)}";
			$rs = new iRecordset($db->execute($sql));
			$ar = array();
			foreach($rs as $row){
				$ar[$row['gaFormID']] = new GaForm(null,$row);
			}
			return $ar;
		}
		
		static public function getMultipleGaFormsByFormIDsAndGaFormClientID($gaFormIDs=array(),$gaFormClientID=0){
			$db = new dbHandler('db=GaForms');
			$sanitizedIDs = array(-1);
			foreach($gaFormIDs as $gaFormID){
				$sanitizedIDs[] = $db->makeSqlSafeString($gaFormID);
			}
			$idList = implode(',',$sanitizedIDs);
			$sql = "SELECT * FROM tblGaForm 
					WHERE 
						gaFormID IN ($idList) AND  
						gaFormClientID = {$db->makeSqlSafeString($gaFormClientID)}";
			$rs = new iRecordset($db->execute($sql));
			$ar = array();
			foreach($rs as $row){
				$ar[$row['gaFormID']] = new GaForm(null,$row);
			}
			return $ar;
		}
		
		static public function getByID($gaFormID=0){
			$db = new dbHandler('db=GaForms');
			$sql = "SELECT * FROM tblGaForm 
					WHERE gaFormID = {$db->makeSqlSafeString($gaFormID)}";
			$rs = new iRecordset($db->execute($sql));
			return ($rs->retrieveRecordCount() > 0 ? new GaForm(null, $rs->retrieveRow(0)) : null);
		}
	}
?>