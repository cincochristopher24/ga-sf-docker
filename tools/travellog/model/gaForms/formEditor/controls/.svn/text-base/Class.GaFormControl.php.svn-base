<?php
	require_once('Class.DataModel.php');
	require_once('travellog/model/gaForms/Class.GaFormException.php');
	require_once('travellog/model/gaForms/formEditor/Interface.GaFormSerializable.php');
	
	abstract class GaFormControl extends DataModel implements GaFormSerializable
	{		
		const TEXTBOX 		= 0;
		const CHECKBOX 		= 1;
		const RADIO			= 2;
		const COMBOBOX		= 3;
		const SECTION_BREAK	= 4;
		
		static private $controlClasses = array(
			GaFormControl::TEXTBOX 			=> 'GaFormTextBoxControl',
			GaFormControl::CHECKBOX 		=> 'GaFormCheckBoxControl',
			GaFormControl::RADIO			=> 'GaFormRadioControl',
			GaFormControl::COMBOBOX			=> 'GaFormComboBoxControl',
			GaFormControl::SECTION_BREAK	=> 'GaFormSectionBreakControl'
		);
		
		protected $mAttributes = array();
		protected function __construct($keyID=null,$data = null){
			parent::initialize('GaForms', 'tblGaFormControl');
			$this->mDb->setDB('GaForms');
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid GaFormControlID');
			}
			if(is_array($data)){$this->setFields($data);}
			$this->initAttributes();
		}
		
		protected function initAttributes(){			
			$qry = 	'SELECT `gaFormControlID`, `attribute`, `value` ' .
					'FROM tblGaFormControlAttribute ' .
					'WHERE `gaFormControlID` = '. $this->mDb->makeSqlsafeString($this->getGaFormControlID()).' ' .
					'ORDER BY `attribute`, `gaFormControlAttributeID`';
			$rs = new iRecordset($this->mDb->execQuery($qry));
			$this->mAttributes = $this->getDefAttributes();
			foreach($rs as $row){
				if(array_key_exists($row['attribute'],$this->mAttributes)){
					if(is_array($this->mAttributes[$row['attribute']])){
						$this->mAttributes[$row['attribute']][] = $row['value'];
					}
					else $this->mAttributes[$row['attribute']] = $row['value'];
				}
			}
		}
		
		final public function getAttribute($attrName){
			if(array_key_exists($attrName,$this->mAttributes)){
				return $this->mAttributes[$attrName];
			}
			throw new GaFormException(GaFormException::UNKNOWN_ATTRIBUTE_NAME);
		}
		
		final public function setAttribute($attrName,$attrValue){
			if(array_key_exists($attrName,$this->mAttributes)){
				return $this->mAttributes[$attrName] = $attrValue;
			}
			throw new GaFormException(GaFormException::UNKNOWN_ATTRIBUTE_NAME);
		}
		
		private function clearStoredAttributes(){
			$this->mDb->execute('DELETE FROM tblGaFormControlAttribute WHERE gaFormControlID = '.$this->mDb->makeSqlSafeString($this->getGaFormControlID()));
		}
		
		public function save(){
			//after save, save also the attributes
			parent::save();
			$this->clearStoredAttributes();
			$insertClause = array();
			foreach($this->mAttributes as $attrName => $attrVal){
				if(is_array($attrVal)){
					foreach($attrVal as $iAttrVal){
						$insertClause[] = "({$this->getGaFormControlID()},'$attrName',{$this->mDb->makeSqlSafeString($iAttrVal)})";
					}
				}
				else{
					$insertClause[] = "({$this->getGaFormControlID()},'$attrName',{$this->mDb->makeSqlSafeString($attrVal)})";
				}
				
			}
			if(count($insertClause)){
				$this->mDb->execute('INSERT INTO tblGaFormControlAttribute (`gaFormControlID`,`attribute`,`value`) VALUES '.implode(',', $insertClause));
			}
		}
		
		public function saveControlData($controlData){
			if(array_key_exists('inputType',$controlData) && is_numeric($controlData['inputType'])) $this->setControlType($controlData['inputType']);
			if(array_key_exists('position',$controlData) && is_numeric($controlData['position'])) $this->setPosition($controlData['position']);
			//get the default attributes
			$defAttributes = $this->getDefAttributes();
			//store the attributes
			foreach($controlData as $key => $iControlData){
				if(array_key_exists($key, $defAttributes)){
					$this->setAttribute($key,$iControlData);
				}
			}
			$this->save();
		}
		
		public function delete(){
			$this->clearStoredAttributes();
			parent::delete();
		}
		
		public function getJsonSerializableData(){
			$data = $this->mFields;
			$data['inputType'] = $data['controlType'];
			return array_merge($data,$this->mAttributes);
		}
		
		public function getJsonSerializableDuplicateData(){
			$data = $this->mFields;
			$data['gaFormControlID'] = 0;
			$data['gaFormFieldID'] = 0;
			$data['inputType'] = $data['controlType'];
			return array_merge($data,$this->mAttributes);
		}
		
		abstract public function getDefAttributes();
		abstract public function getControlType();
		
		static public function getFormFieldControls($gaFormFieldID){
			$db = new dbHandler('db=GaForms');
			$qry = 	'SELECT * ' .
					'FROM tblGaFormControl ' .
					'WHERE gaFormFieldID = '.$db->makeSqlSafeString($gaFormFieldID).' '.
					'ORDER BY position';
			$rs = new iRecordset($db->execQuery($qry));
			$ar = array();			
			foreach($rs as $row){
				$ar[$row['gaFormControlID']] = self::createControl($row);
			}
			return $ar;
		}
		
		private static function createControl($params){
			require_once('Class.'.self::$controlClasses[$params['controlType']].'.php');
			$name = self::$controlClasses[$params['controlType']];
			return new $name(null,$params);
		}
		
		public static function createControlByType($controlType){
			require_once('Class.'.self::$controlClasses[$controlType].'.php');
			$name = self::$controlClasses[$controlType];
			return new $name(null);
		}
	}
?>