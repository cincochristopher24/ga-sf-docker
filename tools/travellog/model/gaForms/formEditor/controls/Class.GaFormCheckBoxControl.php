<?php
	require_once('Class.GaFormControl.php');
	class GaFormCheckBoxControl extends GaFormControl
	{				
		public function getDefAttributes(){
			return array('size' => 0,'isChecked' => 0,'text' => '');
		}
		public function getControlType(){
			return GaFormControl::CHECKBOX;
		}
	}
?>