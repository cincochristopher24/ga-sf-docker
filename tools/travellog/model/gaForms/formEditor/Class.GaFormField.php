<?php
	require_once('Class.DataModel.php');
	require_once('controls/Class.GaFormControl.php');
	require_once('travellog/model/gaForms/Class.GaFormFieldValue.php');

	class GaFormField extends DataModel implements GaFormSerializable 
	{
		const ARRAY_TYPE = 'array';
		const STRING_TYPE = 'string';
		
		public function __construct($keyID=null,$data=null){
			parent::initialize('GaForms', 'tblGaFormField');
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid GaFormFieldID');
			}
			if(is_array($data)){
				$this->setFields($data);
			}
		}
		
		public function getControls(){
			return GaFormControl::getFormFieldControls($this->getGaFormFieldID());
		}
		
		public function createNewControl($controlType){
			$control = GaFormControl::createControlByType($controlType);
			$control->setGaFormFieldID($this->getGaFormFieldID());
			return $control;
		}
		
		public function delete(){
			//delete all the fieldanswers associated with this formfield
			$fieldValues = GaFormFieldValue::getFieldValuesByGaFormFieldID($this->getGaFormFieldID());
			foreach($fieldValues as $iFieldValue){
				$iFieldValue->delete();
			}
			//delete the controls
			foreach($this->getControls() as $iControl){
				$iControl->delete();
			}
			parent::delete();
		}
		
		public function isRequired(){
			return $this->getRequiredStatus() == 1;
		}
		
		//array or string
		public function getValueType(){
			return ($this->getFieldType() == GaFormControl::CHECKBOX || $this->getFieldType() == GaFormControl::RADIO ? self::ARRAY_TYPE : self::STRING_TYPE );
		}
		
		public function saveFieldData($fieldData=array()){
			//deal with the attributes
			if(array_key_exists('fieldType',$fieldData) && is_numeric($fieldData['fieldType'])) $this->setFieldType($fieldData['fieldType']);
			if(array_key_exists('caption',$fieldData) && is_string($fieldData['caption'])) $this->setCaption($fieldData['caption']);
			if(array_key_exists('position',$fieldData) && is_numeric($fieldData['position'])) $this->setPosition($fieldData['position']);
			$this->setRequiredStatus($fieldData['requiredStatus'] ? 1 : 0);
			$this->setOrientation($fieldData['orientation']);
			$this->save();
			//deal with controls
			$newControlData = (array_key_exists('inputs',$fieldData) && is_array($fieldData['inputs']) ? $fieldData['inputs'] : array());
			$existingControls = $this->getControls();
			$presentControls = array();
			foreach($newControlData as $iNewControlData){
				if(array_key_exists('gaFormControlID',$iNewControlData) && is_numeric($iNewControlData['gaFormControlID'])){
					if(0==$iNewControlData['gaFormControlID']){
						$this->createNewControl($iNewControlData['inputType'])->saveControlData($iNewControlData);
					}
					else{
						if(array_key_exists($iNewControlData['gaFormControlID'],$existingControls)){
							$existingControls[$iNewControlData['gaFormControlID']]->saveControlData($iNewControlData);
							$presentControls[] = $iNewControlData['gaFormControlID'];
						}
					}
				}
			}
			
			foreach($existingControls as $iExtControl){
				if(!in_array($iExtControl->getGaFormControlID(),$presentControls)){
					$iExtControl->delete();
				}
			}
		}
		
		public function getJsonSerializableData(){
			$data = $this->mFields;
			$data['inputs'] = array();
			$controls = $this->getControls();
			foreach($controls as $iControl){
				$data['inputs'][] = $iControl->getJsonSerializableData();
			}
			return $data;
		}
		
		public function getJsonSerializableDuplicateData(){
			$data = $this->mFields;
			$data['gaFormFieldID'] = 0;
			$data['gaFormID'] = 0;
			$data['inputs'] = array();
			$controls = $this->getControls();
			foreach($controls as $iControl){
				$data['inputs'][] = $iControl->getJsonSerializableDuplicateData();
			}
			return $data;
		}
		
		public function getStatistics(){
			$fieldValues = GaFormFieldValue::getFieldValuesByGaFormFieldID($this->getGaFormFieldID());
			$data = array('type' => $this->getValueType(),'data' => array());
			if($this->getValueType() == 'array'){
				foreach($this->getControls() as $iControl){
					$data['data'][$iControl->getAttribute('text')] = 0;
				}
				foreach($fieldValues as $iFieldValue){
					$dataValues = $iFieldValue->getSerialiazableFieldValueData();
					foreach($dataValues as $iDataValue){
						if(!array_key_exists($iDataValue,$data['data'])){
							$data['data'][$iDataValue] = 0;
						}
						$data['data'][$iDataValue]++;
					}
				}
			}
			else{
				foreach($fieldValues as $iFieldValue){
					$data['data'][] = $iFieldValue->getSerialiazableFieldValueData();
				}
			}
			return $data;
		}
		
		static public function getGaFormFields($gaFormID=0){
			$db = new dbHandler('db=GaForms');
			$sql = 	'SELECT * FROM tblGaFormField '.
					'WHERE gaFormID = '.$db->makeSqlSafeString($gaFormID).' '.
					'ORDER BY `position`';
			$rs = new iRecordset($db->execute($sql));
			$ar = array();
			foreach($rs as $row){
				$ar[$row['gaFormFieldID']] = new GaFormField(null,$row);
			}
			return $ar;
		}
		
		static public function getByID($gaFieldID=0){
			$db = new dbHandler('db=GaForms');
			$sql = 	'SELECT * FROM tblGaFormField '.
					'WHERE gaFormFieldID = '.$db->makeSqlSafeString($gaFieldID);
			$rs = new iRecordset($db->execute($sql));
			return $rs->retrieveRecordCount() > 0 ? new self(null,$rs->retrieveRow(0)) : null;
		}
	}
?>