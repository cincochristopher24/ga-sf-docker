<?php
	/***
	require_once('travellog/model/gaForms/Class.GaFormException.php');
	require_once('travellog/model/gaForms/Class.GaFormServiceProvider.php');
	require_once('JSON.php');
	$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
	$apiKey = array_key_exists('apiKey',$_POST) ? $_POST['apiKey'] : null;
	$apiSecret = array_key_exists('apiSecret',$_POST) ? $_POST['apiSecret'] : null;
	$method = array_key_exists('method',$_POST) ? $_POST['method'] : null;
	$clientVars = array_key_exists('clientVars',$_POST) ? $json->decode($_POST['clientVars']) : array();
	$result = array(
		'status' 	=> '1',
		'error_description'	=> '',
		'data' => null
	);
	try{
		$result['data'] = GaFormServiceProvider::create($apiKey,$apiSecret)->execute($method,$clientVars);
	}
	catch(GaFormException $e){
		$result['status'] = 'ERROR';
		$result['error_description'] = $e->getMessage();
	}
	$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
	if(!array_key_exists('noEncode',$_POST)){
		echo $json->encode($result);
	}
	else{
		echo $result;
	}
	***/
	require_once('travellog/model/gaForms/Class.GaFormException.php');
	require_once('travellog/model/gaForms/Class.GaFormServiceProvider.php');
	require_once('JSON.php');
	
	class GaFormServiceProviderProxy
	{
		static private $instance = null;
		static public function create(){
			if(is_null(self::$instance)) self::$instance = new self();
			return self::$instance;
		}
		
		private $mJson = null;
		private function __construct(){
			$this->mJson = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
		}	
		
		public function execute($vars){
			$apiKey = array_key_exists('apiKey',$vars) ? $vars['apiKey'] : null;
			$apiSecret = array_key_exists('apiSecret',$vars) ? $vars['apiSecret'] : null;
			$method = array_key_exists('method',$vars) ? $vars['method'] : null;
			$clientVars = array_key_exists('clientVars',$vars) ? $this->mJson->decode($vars['clientVars']) : array();
			$result = array(
				'status' 	=> '1',
				'error_description'	=> '',
				'data' => null
			);
			try{
				$result['data'] = GaFormServiceProvider::create($apiKey,$apiSecret)->execute($method,$clientVars);
			}
			catch(GaFormException $e){
				$result['status'] = 'ERROR';
				$result['error_description'] = $e->getMessage();
			}
			if(!array_key_exists('noEncode',$vars)){
				return $this->mJson->encode($result);
			}
			else{
				return $result;
			}
		}	
	}
?>