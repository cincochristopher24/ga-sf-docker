<?php
	require_once('Class.GaDateTime.php');
	require_once('Class.DataModel.php');
	require_once('travellog/model/gaForms/Class.GaFormFieldValue.php');
	require_once('travellog/model/gaForms/formEditor/Class.GaForm.php');
	
	class GaFormValue extends DataModel
	{
		public function __construct($keyID=null,$data=null){
			parent::initialize('GaForms', 'tblGaFormValue');
			if($keyID and !$this->load($keyID)){
				throw new exception('Invalid GaFormValueID');
			}
			if(is_array($data)){
				$this->setFields($data);
			}
		}
		
		public function clearFieldValues(){
			//delete all the fieldValues
			$fieldValues = $this->getGaFormFieldValues();
			foreach($fieldValues as $iFieldValue){
				$iFieldValue->delete();
			}
		}
		
		public function save(){
			if(is_null($this->getGaFormValueID())){
				$this->setDateCreated(GaDateTime::dbDateTimeFormat());
			}
			parent::save();
		}
		
		public function delete(){
			$fieldValues = $this->getGaFormFieldValues();
			foreach($fieldValues as $iFieldValue){
				$iFieldValue->delete();
			}
			parent::delete();
		}
		
		public function saveFieldValue($fieldID,$fieldValue){
			$formFieldValue = new GaFormFieldValue();
			$formFieldValue->setGaFormValueID($this->getGaFormValueID());
			$formFieldValue->setGaFormFieldID($fieldID);
			$formFieldValue->save($fieldValue);
		}
		
		public function getGaForm(){
			return GaForm::getByID($this->getGaFormID());
		}
		
		public function getGaFormFieldValues(){
			return GaFormFieldValue::getFieldValuesByFormValueID($this->getGaFormValueID());
		}
		
		public function getSerializableFormValueData(){
			$data = array(
				'gaFormID'	  => $this->getGaFormID(),	
				'dateCreated' => $this->getDateCreated(),
				'fieldValues' => array(),
			);
			$fieldValues = $this->getGaFormFieldValues();
			foreach($fieldValues as $iFieldValue){
				$data['fieldValues'][$iFieldValue->getGaFormFieldID()] = $iFieldValue->getSerialiazableFieldValueData();
			}
			return $data;
		}
		
		static public function getByGaFormIDAndFormValueID($formID,$formValueID){
			$db = new dbHandler();
			$sql = 	'SELECT * FROM GaForms.tblGaFormValue '.
					'WHERE gaFormValueID = '.$db->makeSqlSafeString($formValueID).' '.
					'AND gaFormID = '.$db->makeSqlSafeString($formID);
			$rs = new iRecordset($db->execute($sql));
			return ($rs->retrieveRecordCount() ? new self(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getByID($formValueID=0){
			$db = new dbHandler();
			$sql = 	'SELECT * FROM GaForms.tblGaFormValue '.
					'WHERE gaFormValueID = '.$db->makeSqlSafeString($formValueID);
			$rs = new iRecordset($db->execute($sql));
			return ($rs->retrieveRecordCount() ? new self(null,$rs->retrieveRow(0)) : null);
		}
		
		static public function getInstancesByGaFormID($formID){
			$db = new dbHandler();
			$sql = 	'SELECT * FROM GaForms.tblGaFormValue '.
					'WHERE gaFormID = '.$db->makeSqlSafeString($formID);
			$rs = new iRecordset($db->execute($sql));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new self(null,$row);
			}
			return $ar;
		}
		
		static public function getInstancesByIDs($formValueIDs=array()){
			$db = new dbHandler();
			$sanitizedIDs = array(-1);
			foreach($formValueIDs as $iFormValueID){
				$sanitizedIDs[] = $db->makeSqlSafeString($iFormValueID);
			}
			$sql = 	'SELECT * FROM GaForms.tblGaFormValue '.
					'WHERE gaFormValueID IN ('.implode(',',$sanitizedIDs).')';
			$rs = new iRecordset($db->execute($sql));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new self(null,$row);
			}
			return $ar;
		}
		
	}
?>