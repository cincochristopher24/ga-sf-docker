<?php
	require_once('JSON.php');
	class GaFormRestClient
	{
		private $mApiKey 	= null;
		private $mApiSecret = null;
		private $mServerAddress = null;
		private $mUserCurl	= false;
		private $mJson = null;
		
		private function __construct($params=array()){
			$this->mApiKey 		= array_key_exists('apiKey',$params) ? $params['apiKey']: null;
			$this->mApiSecret 	= array_key_exists('apiSecret',$params) ? $params['apiSecret']:null;
			$this->mServerAddress = array_key_exists('serverAddress',$params) ? $params['serverAddress'] : '';
			$this->mUserCurl	= array_key_exists('userCurl',$params) ? $params['useCurl'] : false;
			$this->mJson = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
		}
		
		public function saveForm($formData){
			try{
				return $this->makeRestRequest('saveForm',array('formData'=>$formData));
			}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function getFormData($formID){
			try{
				$data =$this->makeRestRequest('getFormData',array('formID'=>$formID));
				return array('json'	=> $this->mJson->encode($data),'object'	=> $data);
			}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function getMultipleFormData($formIDs=array()){
			try{
				$data =$this->makeRestRequest('getFormData',array('formID'=>$formIDs));
				$formData = array();
				foreach($data as $key => $iFormData){
					$formData[$key] = array('json' => $this->mJson->encode($iFormData),'object' => $iFormData); 
				}
				return $formData;
			}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function createNewForm(){
			try{ 
				$data = $this->mJson->encode($this->makeRestRequest('createNewForm'));
				return array('json' => $this->mJson->encode($data),	'object' => $data);
			}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function deleteForms($formIDs=array()){
			try{
				return $this->makeRestRequest('deleteForms',array('formIDs'=>$formIDs));
			}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function getDuplicateFormData($formID=0){
			try{
				$data = $this->makeRestRequest('getDuplicateFormData',array('formID'=>$formID));
				return array('json'	=> $this->mJson->encode($data),'object'	=> $data);
			}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function getFormValue($formValueID=0){
			try{ return $this->makeRestRequest('getFormValue',array('formValueID'=>$formValueID));}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function getMultipleFormValues($formValueIDs=array()){
			try{ return $this->makeRestRequest('getMultipleFormValues',array('formValueIDs'=>$formValueIDs));}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function saveFormValue($formID=null, $formValueID=null, $vars=array(), $validateOnly=false){
			try{
				$clientVars = $vars;
				$clientVars['gaFormID'] = $formID;
				$clientVars['gaFormValueID'] = $formValueID;
				$clientVars['validateOnly'] = $validateOnly;
				return $this->makeRestRequest('saveFormValue',$clientVars);
			}
			catch(exception $e){
				throw $e;
			}
		}
		
		public function getStatistics($formID=0){
			try{
				return $this->makeRestRequest('getStatistics',array('formID'=>$formID));
			}
			catch(exception $e){
				throw $e;
			}
		}
		
		private function makeRestRequest($method,$parameters=array(),$noEncode=false){
			
			$params = array();
			$params['method'] = $method;
			$params['apiKey'] = $this->mApiKey;
			$params['apiSecret'] = $this->mApiSecret;
			$params['clientVars'] = $this->mJson->encode($parameters);
			if($noEncode){ $params['noEncode'] = 1;}
			
			if($this->mUserCurl){
				/****
				 * This is an attemp  for the GaForms to become a webservice so that it can be used by sites even if it is not hosted 
				 * in the GA server, but unfurtunately, there are still some problems 
				 * like authentication in the dev server. I dont have time to think on this anymore. Later...=)
				****/
				$postVars = array();
				foreach($params as $key => $val){
					$postVars[] = $key.'='.urlencode($val);
				}
				$postString = implode('&', $postVars);
				$ch = curl_init();
		      	curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_URL, $this->mServerAddress);
		      	curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
		      	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		      	curl_setopt($ch, CURLOPT_USERAGENT, 'GaFormRestClient API 1.0 (curl) '.phpversion());
				$data = curl_exec($ch);
				$result = $this->mJson->decode($data);
		      	curl_close($ch);
				if($result['status'] == 'ERROR'){
					throw new exception($result['error_description']);
				}
			}
			else{
				require_once('travellog/model/gaForms/Class.GaFormServiceProviderProxy.php');
				$params['noEncode'] = true;
				$result = GaFormServiceProviderProxy::create()->execute($params);
				if($result['status'] == 'ERROR'){
					throw new exception($result['error_description']);
				}
			}
			return $result['data'];
		}
		
		static public function create($params=array()){
			return new GaFormRestClient($params);
		}
	}
?>