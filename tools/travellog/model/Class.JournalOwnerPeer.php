<?php
	/**
	 * @(#) Class.JournalOwnerPeer.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 April 28, 2009
	 */
	
	require_once('travellog/model/base/Class.BaseJournalOwnerPeer.php');
	
	/**
	 * Model peer class for journal owner.
	 */
	class JournalOwnerPeer extends BaseJournalOwnerPeer {
		
	}
	
