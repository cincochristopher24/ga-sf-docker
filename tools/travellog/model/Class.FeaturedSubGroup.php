<?php
/**
 * Created on Jul.11.2008
 * @author Czarisse Daphne P. Dolina
 * Purpose: domain model for a featured sub group
 */

	require_once("travellog/model/Class.AdminGroup.php");
	
	class FeaturedSubGroup extends AdminGroup {
		
		
		function isSubGroupFeatured(){
			
			if (0 < $this->mFeaturedSubGroupRank)
				return true;
			else
				return false;
				
		}
				
		function setSubGroupFeatured() {
			
			if (!$this->isSubGroupFeatured()):
				$this->resetRankOfFeaturedSubGroups();
				
				$latestrank = $this->getRankOfNewFeaturedSubGroup();
				$sql = "UPDATE tblGroup SET featuredsubgrouprank = " . $latestrank . " WHERE groupID = " . $this->mGroupID ;
				$this->mRs->Execute($sql);
			endif;
		}
		
		function unsetSubGroupFeatured(){
			
			$sql = "UPDATE tblGroup SET featuredsubgrouprank = 0 WHERE groupID =  " . $this->mGroupID ;
			$this->mRs->Execute($sql);
			
			$this->resetRankOfFeaturedSubGroups();
		}
		
		function getRankOfNewFeaturedSubGroup() {
			
			$sql = "SELECT featuredsubgrouprank FROM tblGroup WHERE parentID = " . $this->getParentID() . " ORDER BY featuredsubgrouprank DESC LIMIT 0 , 1" ;
			$this->mRs->Execute($sql);
			
			$newrank = 1;
			if ($this->mRs->RecordCount())
				$newrank = $this->mRs->Result(0,'featuredsubgrouprank') + 1;
				
			return $newrank;			
			
		}
		
		function resetRankOfFeaturedSubGroups() {
			
			$sql = "SELECT groupID FROM tblGroup WHERE parentID = " . $this->mParentID . 
					" AND featuredsubgrouprank > 0 ORDER BY featuredsubgrouprank" ;
			$this->mRs->Execute($sql);
			$resultset = $this->mRs->Resultset();
			$cnt = 0;
			while ($recordset = mysql_fetch_array($resultset)) {
				$cnt ++ ;
				$sql = "UPDATE tblGroup SET featuredsubgrouprank = " . $cnt . " WHERE groupID = " . $recordset['groupID'] ;
				$this->mRs->Execute($sql);
			}	
			
		}
		
		public static function arrangeRankOfFeaturedSubGroups($_arrFeatSubGroup = array()) {
			
			$rs = new Recordset(new Connection());
			
			$cnt = 0;
			foreach ($_arrFeatSubGroup as $each) :
				$cnt ++ ;
				$sql = "UPDATE tblGroup SET featuredsubgrouprank = " . $cnt . " WHERE groupID = " . $each ;
				$rs->Execute($sql);
			endforeach;	
			
		}
		

	}
		
?>
