<?php	

/**
 * Represents the current user of ganet. This holds the methods that are related
 * to travelers and the current user of ganet. This is implemented in singleton
 * so you should call
 * <code>
 *   $instance = TravelerUser::getInstance();
 * </code>
 * 
 * @author Antonio Pepito Cruda Jr.
 * @version 1.0  August 8, 2009
 */
class TravelerUser 
{
  /**
   * @var  TravelerUser  The single instance of this class.
   */
  private static $sInstance = null;
  
  /**
   * @var  integer  The ID of the current user.
   */
  private $mUserID = 0;
  
  /**
   * @var  boolean  The flag that holds the state whether the current user is logged or not.
   */
  private $mIsLogged = false;
  
  /**
   * Returns the single instance of this class.
   * 
   * @return  TravelerUser  The single instance of this class.
   */
  public function getInstance()
  {
    if (is_null(self::$sInstance))
    {
      self::$sInstance = new TravelerUser();
    }
    
    return self::$sInstance;
  }
  
  /**
   * Make the constructor private to apply the singleton pattern.
   */
  private function __construct()
  {
    require_once("travellog/model/Class.SessionManager.php");
    
    $session = SessionManager::getInstance();
    $this->mUserID = $session->get('travelerID');
    $this->mIsLogged = (0 < $this->mUserID);
  }
  
  /**
   * Checks whether the current user is logged in.
   * 
   * @return  boolean  true if the current user is logged in.
   */
  public function isLogged()
  {
    return $this->mIsLogged;
  }
  
  /**
   * Checks if the current user is admin(staff or administrator) of the given group.
   * 
   * @param  Group  $group  The given group
   * 
   * @return  boolean  true if the current user is admin(staff or administrator) of the given group.
   */
  public function isAdminOf(Group $group)
  {
  	if ($this->mIsLogged)
  	{
  	  // Store the checks if the user is admin of the given group
  	  // to prevent multiple query.
      static $repo = array();
    
      if (!isset($repo[$group->getGroupID()]))
      {
        $repo[$group->getGroupID()] = $group->isStaff($this->mUserID);
      }
    
      return $repo[$group->getGroupID()];
  	}
  	else
  	{
  	  return false;
  	}
  }
  
  /**
   * Returns the ID of the current logged user.
   * 
   * @return  int  The ID of the current logged user.
   */
  public function getUserID()
  {
    return $this->mUserID;
  }
}
	
