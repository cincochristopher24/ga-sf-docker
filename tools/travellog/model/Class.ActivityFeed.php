<?php

	require_once("Class.Connection.php");
	require_once("Class.Recordset.php");
	require_once("Class.dbHandler.php");
	require_once("travellog/model/Class.AdminGroup.php");
	//require_once("travellog/model/DiscussionBoard/Class.Post.php");
	require_once("travellog/model/Class.PhotoAlbum.php");
	require_once("travellog/model/Class.VideoAlbum.php");
	require_once("travellog/model/Class.Travel.php");
	require_once("travellog/model/Class.TravelLog.php");
	require_once("travellog/model/Class.Traveler.php");
	require_once("travellog/model/Class.SessionManager.php");

	class ActivityFeed{
		
		//section root id constants for new member feeds
		const MEMBER_BY_DEFAULT	=	0;
		const MEMBER_BY_INVITE	=	1;
		const MEMBER_BY_REQUEST	=	2;
		
		//section type constants
		const TRAVELER			=	0;
		const GROUP				=	1;
		
		//feed section constants
		const UNKNOWN			=	0;
		const DISCUSSION		=	1;
		const DISCUSSIONPOST	=	2;
		const PHOTOALBUM		=	3;
		const VIDEOALBUM		=	4;
		const JOURNAL			=	5;
		const JOURNALENTRY		=	6;
		const JOINGROUPREQUEST	=	7;
		const NEWMEMBER			=	8;
		const PHOTO				=	9;
		const VIDEO				=	10;
		const ENTRYPHOTO		=	11;
		const ENTRYVIDEO		=	12;
		
		private $mConn = NULL;
		private $mRs = NULL;
		private $mDb = NULL;
		
		private $mFeedID		=	0;
		private $mFeedSection	=	self::UNKNOWN;
		private $mSectionType	=	self::TRAVELER;
		private $mTravelerID	=	0;
		private $mGroupID		=	0;
		private $mParentGroupID = 	0;
		private $mSectionID		=	0;
		private $mSectionRootID	=	0;
		private $mFeedDate		=	"0000-00-00 00:00:00";
		private $mAuxDate		=	"0000-00-00 00:00:00";
		
		public function __construct($feedID=NULL, $data=NULL){
			try {
	 			$this->mConn = new Connection();
				$this->mRs = new Recordset($this->mConn);
				$this->mDb = new dbHandler();
				
				if( 0 < $feedID ){
					$sql = "SELECT *
							FROM tblActivityFeed
							WHERE feedID = ".$this->mDb->makeSqlSafeString($feedID);
					
					$this->mRs->Execute($sql);
					if ( 0 < $this->mRs->Recordcount() ){
	 					$this->mFeedID = $this->mRs->Result(0,"feedID");
						$this->mFeedSection = $this->mRs->Result(0,"feedSection");
						$this->mSectionType = $this->mRs->Result(0,"sectionType");
						$this->mTravelerID = $this->mRs->Result(0,"travelerID");
						$this->mGroupID = $this->mRs->Result(0,"groupID");
						$this->mParentGroupID = $this->mRs->Result(0,"parentGroupID");
						$this->mSectionID = $this->mRs->Result(0,"sectionID");
						$this->mSectionRootID = $this->mRs->Result(0,"sectionRootID");
						$this->mFeedDate = $this->mRs->Result(0,"feedDate");
						$this->mAuxDate = $this->mRs->Result(0,"auxDate");
	 				}
				}else if( is_array($data) ){
					$this->mFeedID = $data["feedID"];
					$this->mFeedSection = $data["feedSection"];
					$this->mSectionType = $data["sectionType"];
					$this->mTravelerID = $data["travelerID"];
					$this->mGroupID = $data["groupID"];
					$this->mParentGroupID = $data["parentGroupID"];
					$this->mSectionID = $data["sectionID"];
					$this->mSectionRootID = $data["sectionRootID"];
					$this->mFeedDate = $data["feedDate"];
					$this->mAuxDate = $data["auxDate"];
				}else if( !is_null($feedID) && !is_null($data) ){
					throw new exception("Invalid parameters passed to constructor ActivityFeed(feed,data)");
				}
			}catch(exception $e){
				throw $e;
			}
		}
		
		public function setFeedID($feedID){
			$this->mFeedID = $feedID;
		}
		public function setFeedSection($feedSection){
			$this->mFeedSection = $feedSection;
		}
		public function setSectionType($type=0){
			$this->mSectionType = $type;
		}
		public function setTravelerID($travelerID){
			$this->mTravelerID = $travelerID;
		}
		public function setGroupID($groupID=0){
			$this->mGroupID = $groupID;
		}
		public function setParentGroupID($parentGroupID=0){
			$this->mParentGroupID = $parentGroupID;
		}
		public function setSectionID($sectionID){
			$this->mSectionID = $sectionID;
		}
		public function setSectionRootID($sectionRootID){
			$this->mSectionRootID = $sectionRootID;
		}
		public function setFeedDate($feedDate){
			$this->mFeedDate = $feedDate;
		}
		public function setAuxDate($auxDate){
			$this->mAuxDate = $auxDate;
		}
		
		public function getFeedID(){
			return $this->mFeedID;
		}
		public function getFeedSection(){
			return $this->mFeedSection;
		}
		public function getSectionType(){
			return $this->mSectionType;
		}
		public function getTravelerID(){
			return $this->mTravelerID;
		}
		public function getGroupID(){
			return $this->mGroupID;
		}
		public function getParentGroupID(){
			return $this->mParentGroupID;
		}
		public function getSectionID(){
			return $this->mSectionID;
		}
		public function getSectionRootID(){
			return $this->mSectionRootID;
		}
		public function getFeedDate(){
			return $this->mFeedDate;
		}
		public function getAuxDate(){
			return $this->mAuxDate;
		}
		public function getFeedSectionLabel(){
			switch($this->mFeedSection){
				case self::DISCUSSION		:	return "DISCUSSION";
				case self::DISCUSSIONPOST	:	return "DISCUSSIONPOST";
				case self::PHOTOALBUM		:	return "PHOTOALBUM";
				case self::VIDEOALBUM		:	return "VIDEOALBUM";
				case self::JOURNAL			:	return "JOURNAL";
				case self::JOURNALENTRY		:	return "JOURNALENTRY";
				case self::JOINGROUPREQUEST	:	return "JOINGROUPREQUEST";
				case self::NEWMEMBER		:	return "NEWMEMBER";
				case self::PHOTO			:	return "PHOTO";
				case self::VIDEO			:	return "VIDEO";
				case self::ENTRYPHOTO		:	return "ENTRYPHOTO";
				case self::ENTRYVIDEO		:	return "ENTRYVIDEO";
				default						:	return "";
			}
		}
		
		public function Save(){
			$feedID = $this->mFeedID;
			$feedSection = $this->mDb->makeSqlSafeString($this->mFeedSection);
			$sectionType = $this->mDb->makeSqlSafeString($this->mSectionType);
			$travelerID = $this->mDb->makeSqlSafeString($this->mTravelerID);
			$groupID = $this->mDb->makeSqlSafeString($this->mGroupID);
			$parentGroupID = $this->mDb->makeSqlSafeString($this->mParentGroupID);
			$sectionID = $this->mDb->makeSqlSafeString($this->mSectionID);
			$sectionRootID = $this->mDb->makeSqlSafeString($this->mSectionRootID);
			$feedDate = $this->mDb->makeSqlSafeString($this->mFeedDate);
			$auxDate = $this->mDb->makeSqlSafeString($this->mAuxDate);
			if( 0 < $this->mFeedID ){
				$sql = "UPDATE tblActivityFeed
						SET feedSection = $feedSection,
							sectionType = $sectionType,
							travelerID = $travelerID,
							groupID = $groupID,
							parentGroupID = $parentGroupID,
							sectionID = $sectionID,
							sectionRootID = $sectionRootID,
							feedDate = $feedDate,
							auxDate = $auxDate
						WHERE feedID = $feedID";
				$this->mRs->Execute($sql);
			}else{
				$sql = "INSERT INTO tblActivityFeed(feedSection, sectionType, travelerID, groupID, parentGroupID, sectionID, sectionRootID, feedDate, auxDate)
						VALUES($feedSection, $sectionType, $travelerID, $groupID, $parentGroupID, $sectionID, $sectionRootID, $feedDate, $auxDate)";
				$this->mRs->Execute($sql);
				$this->mFeedID = $this->mRs->GetCurrentId();
			}
		}
		
		public function Delete(){
			$feedID = $this->mDb->makeSqlSafeString($this->mFeedID);
			$sql = "DELETE FROM tblActivityFeed WHERE feedID = $feedID";
			$this->mRs->Execute($sql);
		}
		
		public static function create($sectionObject=NULL,$rootObject=NULL,$memberBy=0,$auxDate=NULL){
			$session_mgr = SessionManager::getInstance();
			if( !$session_mgr->getLogin() ){
				throw new exception("Must be logged in to create activity feed.");
			}
			$feedSection = "";
			$sectionType = self::TRAVELER;
			$travelerID = $session_mgr->get("travelerID");
			$groupID = 0;
			$parentGroupID = 0;
			$sectionID = 0;
			$auxDate = (is_null($auxDate))? "0000-00-00 00:00:00" : $auxDate;
			$feedDate = date("Y-m-d H:i:s");
			$sectionRootID = $memberBy;
			$sameDay = FALSE;//this flag is used to disable creation of feeds for photo/videos uploaded into a particular album on the same day
			
			switch(get_class($sectionObject)){
				case "Discussion"	:	$feedSection = self::DISCUSSION;
										$sectionType = self::GROUP;
										$sectionID = $sectionObject->getID();
										$sectionRootID = $sectionObject->getTopicID();
										$topic = $sectionObject->getParentTopic();
										$groupID = $topic->getGroupID();
										$group = new AdminGroup($groupID);
										$parentGroupID = $group->getParentID();
										break;
				
				case "Post"			:	$feedSection = self::DISCUSSIONPOST;
										$sectionType = self::GROUP;
										$sectionID = $sectionObject->getID();
										$sectionRootID = $sectionObject->getDiscussionID();
										$topic = $sectionObject->getParentDiscussion()->getParentTopic();
										$groupID = $topic->getGroupID();
										$group = new AdminGroup($groupID);
										$parentGroupID = $group->getParentID();
										break;
										
				case "PhotoAlbum"	:	$feedSection = self::PHOTOALBUM;
										$sectionID = $sectionObject->getPhotoAlbumID();
										$groupID = $sectionObject->getGroupID();
										
										if( 0 < $groupID ){
											$sectionType = self::GROUP;
											$group = new AdminGroup($groupID);
											$parentGroupID = $group->getParentID();
										} 
										break;
										
				case "Photo"		:	
										switch( get_class($rootObject) ){
											case "PhotoAlbum"	:
												$feedSection = self::PHOTO;
												$sectionRootID = $rootObject->getPhotoAlbumID();
												$groupID = $rootObject->getGroupID();
												if( 0 < $groupID ){
													$group = new AdminGroup($groupID);
													$parentGroupID = $group->getParentID();
												}
												$sectionType = $groupID ? self::GROUP : self::TRAVELER;
												$today = date("Y-m-d");
												$dateAlbumCreated = $rootObject->getDateCreated();
												$sameDay = FALSE;
												if( !is_null($dateAlbumCreated) ){
													$dayCreated = explode(" ",$dateAlbumCreated);
													$sameDay = $dayCreated[0] == $today;
												}
												if( !$sameDay ){
													$params = array("FEED_SECTION"		=>	self::PHOTO,
																	"SECTION_ROOT_ID"	=>	$sectionRootID,
																	"FEED_DATE"			=>	$feedDate );
													$feeds = self::getInstances($params);
													if( 0 < $feeds["recordCount"] ){
														$sameDay = TRUE;
														$feeds["activityFeeds"][0]->setFeedDate($feedDate);
														$feeds["activityFeeds"][0]->Save();
													}
												}
												break;
												
											case "TravelLog"	:
											case "TravelLogCB"	:
												$feedSection = self::ENTRYPHOTO;
												$sectionRootID = $rootObject->getTravelLogID();
												$journal = new Travel($rootObject->getTravelID());
												$owner = $journal->getOwner();
												if( $owner instanceof Group ){
													$groupID = $owner->getGroupID();
													$sectionType = self::GROUP;
													$parentGroupID = $owner->getParentID();
												}
												//$today = date("Y-m-d");
												//$dateEntryCreated = $rootObject->getLogDate();
												$sameDay = FALSE;
												//if( !is_null($dateEntryCreated) ){
												//	$dayCreated = explode(" ",$dateEntryCreated);
												//	$sameDay = $dayCreated[0] == $today;
												//}
												//if( !$sameDay ){
													$params = array("FEED_SECTION"		=>	self::ENTRYPHOTO,
																	"SECTION_ROOT_ID"	=>	$sectionRootID,
																	"FEED_DATE"			=>	$feedDate );
													$feeds = self::getInstances($params);
													if( 0 < $feeds["recordCount"] ){
														$sameDay = TRUE;
														$feeds["activityFeeds"][0]->setFeedDate($feedDate);
														$feeds["activityFeeds"][0]->Save();
													}
												//}
												break;
										}
										
										break;
				
				case "VideoAlbum"	:	$feedSection = self::VIDEOALBUM;
										$sectionID = $sectionObject->getAlbumID();
										if( "group" == $sectionObject->getCreatorType() ){
											$groupID = $sectionObject->getCreatorID();
											$sectionType = self::GROUP;
											$group = new AdminGroup($groupID);
											$parentGroupID = $group->getParentID();
										}
										break;
				
				case "VideoAlbumToVideo":
										$feedSection = self::VIDEO;
										$sectionRootID = $rootObject->getAlbumID();
										switch( $rootObject->getCreatorType() ){
											case "traveler"	:	$sectionType = self::TRAVELER;
																break;
											case "group"	:	$sectionType = self::GROUP;
																$groupID = $rootObject->getCreatorID();
																$group = new AdminGroup($groupID);
																$parentGroupID = $group->getParentID();
																break;
										}
										$today = date("Y-m-d");
										$dateAlbumCreated = $rootObject->getDateCreated();
										$sameDay = FALSE;
										if( !is_null($dateAlbumCreated) ){
											$dayCreated = explode(" ",$dateAlbumCreated);
											$sameDay = $dayCreated[0] == $today;
										}
										if( !$sameDay ){
											$params = array("FEED_SECTION"		=>	self::VIDEO,
															"SECTION_ROOT_ID"	=>	$sectionRootID,
															"FEED_DATE"			=>	$feedDate );
											$feeds = self::getInstances($params);
											if( 0 < $feeds["recordCount"] ){
												$sameDay = TRUE;
												$feeds["activityFeeds"][0]->setFeedDate($feedDate);
												$feeds["activityFeeds"][0]->Save();
											}
										}
										break;
				case "TravelLogToVideo"	:
										$feedSection = self::ENTRYVIDEO;
										$sectionRootID = $rootObject->getTravelLogID();
										$journal = new Travel($rootObject->getTravelID());
										$owner = $journal->getOwner();
										if( $owner instanceof Group ){
											$groupID = $owner->getGroupID();
											$sectionType = self::GROUP;
											$parentGroupID = $owner->getParentID();
										}
										//$today = date("Y-m-d");
										//$dateEntryCreated = $rootObject->getLogDate();
										$sameDay = FALSE;
										//if( !is_null($dateEntryCreated) ){
										//	$dayCreated = explode(" ",$dateEntryCreated);
										//	$sameDay = $dayCreated[0] == $today;
										//}
										//if( !$sameDay ){
											$params = array("FEED_SECTION"		=>	self::ENTRYVIDEO,
															"SECTION_ROOT_ID"	=>	$sectionRootID,
															"FEED_DATE"			=>	$feedDate );
											$feeds = self::getInstances($params);
											if( 0 < $feeds["recordCount"] ){
												$sameDay = TRUE;
												$feeds["activityFeeds"][0]->setFeedDate($feedDate);
												$feeds["activityFeeds"][0]->Save();
											}
										//}
										break;
										
				case "Travel"		:	
				case "TravelCB"		:
										$feedSection = self::JOURNAL;
										$sectionID = $sectionObject->getTravelID();
										$owner = $sectionObject->getOwner();
										if( $owner instanceof Group ){
											$groupID = $owner->getGroupID();
											$sectionType = self::GROUP;
											$parentGroupID = $owner->getParentID();
										}
										if( !is_null($rootObject) && is_numeric($rootObject) && 0 < $rootObject ){
											$groupID = $rootObject;
											$sectionType = self::GROUP;
											$group = new AdminGroup($groupID);
											$parentGroupID = $group->getParentID();
										}
										break;
										
				case "TravelLog"	:	
				case "TravelLogCB"	:
										$feedSection = self::JOURNALENTRY;
										$sectionID = $sectionObject->getTravelLogID();
										$sectionRootID = $sectionObject->getTravelID();
										$journal = new Travel($sectionObject->getTravelID());
										$owner = $journal->getOwner();
										if( $owner instanceof Group ){
											$groupID = $owner->getGroupID();
											$sectionType = self::GROUP;
											$parentGroupID = $owner->getParentID();
										}
										break;
										
				case "Traveler"		:	
				case "TravelerCB"	:
										if( $rootObject instanceof AdminGroup || $rootObject instanceof Group ){
											if( $rootObject->getAdministratorID() == $sectionObject->getTravelerID() ){
												return FALSE;
											}
											if( $rootObject->isMember($sectionObject) ){
												$feedSection = self::NEWMEMBER;
											}else{
												$feedSection = self::JOINGROUPREQUEST;
											}
											$sectionType = self::GROUP;
											$travelerID = $sectionObject->getTravelerID();
											$groupID = $rootObject->getGroupID();
											$parentGroupID = $rootObject->getParentID();
										}else{
											throw new exception("groupObject must be an instance of AdminGroup or Group");
										}
										break;
										
				default				:	throw new exception("Unknown section for group activity feed {".get_class($sectionObject)."}");
										break;
			}
			
			if( !$sameDay ){
				$feed = new ActivityFeed();
				$feed->setFeedSection($feedSection);
				$feed->setSectiontype($sectionType);
				$feed->setTravelerID($travelerID);
				$feed->setGroupID($groupID);
				$feed->setParentGroupID($parentGroupID);
				$feed->setSectionID($sectionID);
				$feed->setSectionRootID($sectionRootID);
				$feed->setFeedDate($feedDate);
				$feed->setAuxDate($auxDate);
				$feed->Save();
			
				return $feed;
			}
		}
		
		public static function getInstances($params=array()){
			$db = new dbHandler();
			
			$temp = array( 	"FEED_ID"			=>	NULL,
							"FEED_SECTION"		=>	NULL,
							"SECTION_TYPE"		=>	NULL,	
							"TRAVELER_ID"		=>	NULL,
							"GROUP_ID"			=>	NULL,
							"SECTION_ID"		=>	NULL,
							"SECTION_ROOT_ID"	=>	NULL,
							"FEED_DATE"			=>	NULL,
							"ROWS_LIMIT"		=>	NULL,
							"COUNT_ONLY"		=>	NULL );
			$params = array_merge($temp,$params);
			$feedID_str = "";
			if( !is_null($params["FEED_ID"]) ){
				$feedID_str = " AND tblActivityFeed.feedID = ".$db->makeSqlSafeString($params["FEED_ID"]);
			}
			$feedSection_str = "";
			if( !is_null($params["FEED_SECTION"]) ){
				$feedSection_str = " AND tblActivityFeed.feedSection = ".$db->makeSqlSafeString($params["FEED_SECTION"]);
			}
			$sectionType_str = "";
			if( !is_null($params["SECTION_TYPE"]) ){
				$sectionType_str = " AND tblActivityFeed.sectionType = ".$db->makeSqlSafeString($params["SECTION_TYPE"]);
			}
			$travelerID_str = "";
			if( !is_null($params["TRAVELER_ID"]) ){
				$travelerID_str = " AND tblActivityFeed.travelerID = ".$db->makeSqlSafeString($params["TRAVELER_ID"]);
			}
			$groupID_str = "";
			if( !is_null($params["GROUP_ID"]) ){
				$groupID_str = " AND tblActivityFeed.groupID = ".$db->makeSqlSafeString($params["GROUP_ID"]);
			}
			$sectionID_str = "";
			if( !is_null($params["SECTION_ID"]) ){
				$sectionID_str = " AND tblActivityFeed.sectionID = ".$db->makeSqlSafeString($params["SECTION_ID"]);
			}
			$sectionRootID_str = "";
			if( !is_null($params["SECTION_ROOT_ID"]) ){
				$sectionRootID_str = " AND tblActivityFeed.sectionRootID = ".$db->makeSqlSafeString($params["SECTION_ROOT_ID"]);
			}
			$feedDate_str = "";
			if( !is_null($params["FEED_DATE"]) ){
				$feedDate_str = " AND CAST(tblActivityFeed.feedDate AS DATE) = CAST(".$db->makeSqlSafeString($params["FEED_DATE"])." AS DATE)";
			}
			$limitstr = "";
			if( !is_null($params["ROWS_LIMIT"]) ){
				$offset   = $params["ROWS_LIMIT"]->getOffset();
                $rows     = $params["ROWS_LIMIT"]->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
			}
			
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
					FROM tblActivityFeed
					WHERE 1 = 1
						$feedID_str
						$feedSection_str
						$sectionType_str
						$travelerID_str
						$groupID_str
						$sectionID_str
						$sectionRootID_str
						$feedDate_str
					GROUP BY tblActivityFeed.feedID
					$limitstr";
			
			$conn = new Connection();
			$rs = new Recordset($conn);
			$rs->Execute($sql);
			
			$sql2 = "SELECT FOUND_ROWS() AS `recordCount`";
			$rs2 = new Recordset($conn);
			$rs2->Execute($sql2);
			$data = mysql_fetch_assoc($rs2->Resultset());
			$feeds_count = $data["recordCount"];
			
			if( isset($params["COUNT_ONLY"]) && $params["COUNT_ONLY"] ){
				return $feeds_count;
			}
			
			$feeds = array();
			while( $data = mysql_fetch_assoc($rs->Resultset()) ){
				$feeds[] = new ActivityFeed(0,$data);
			}
			
			return array("activityFeeds"=>$feeds, "recordCount"=>$feeds_count);
		}
		
		public static function searchFeeds($params=array()){
			$temp = array(	"GROUP_OBJECT"				=>	NULL, //the instance of AdminGroup for the current group view
							"GET_ALL_FEEDS"				=>	NULL,
							"DISCUSSION_ONLY"			=>	NULL, 
							"JOURNALS_ONLY"				=>	NULL,
							"MULTIMEDIA_ONLY"			=>	NULL, //set to true to get feeds of photo and video albums only
							"MEMBERSHIP_ONLY"			=>	NULL,
							"GROUP_ID"					=>	NULL,
							"ROWS_LIMIT"				=>	NULL ); //if in the parent group, only get feeds for a particular subgroup
			$params = array_merge($temp,$params);
			
			if( !($params["GROUP_OBJECT"] instanceof AdminGroup) ){
				throw new exception("GROUP_OBJECT must be an instance of AdminGroup");
			}
			$subgroupOnly = FALSE;
			if( !is_null($params["GROUP_ID"]) && is_numeric($params["GROUP_ID"]) ){
				$groupID = $params["GROUP_ID"];
				$subgroupOnly = TRUE;
			}else{
				$groupID =  $params["GROUP_OBJECT"]->getGroupID();
			}
			
			$limitstr = "";
			if( !is_null($params["ROWS_LIMIT"]) ){
                $offset   = $params["ROWS_LIMIT"]->getOffset();
                $rows     = $params["ROWS_LIMIT"]->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
            }

			//variables representing constants
			$DISCUSSION 		= self::DISCUSSION;
			$DISCUSSIONPOST		= self::DISCUSSIONPOST;
			$PHOTOALBUM			= self::PHOTOALBUM;
			$VIDEOALBUM			= self::VIDEOALBUM;
			$JOURNAL			= self::JOURNAL;
			$JOURNALENTRY		= self::JOURNALENTRY;
			$JOINGROUPREQUEST	= self::JOINGROUPREQUEST;
			$NEWMEMBER			= self::NEWMEMBER;
			$PHOTO				= self::PHOTO;
			$VIDEO				= self::VIDEO;
			$ENTRYPHOTO			= self::ENTRYPHOTO;
			$ENTRYVIDEO			= self::ENTRYVIDEO;
			
			$TRAVELER			= self::TRAVELER;
			$GROUP				= self::GROUP;

			$feedSection_str = "";
			$get_all_feeds = FALSE;
			$journals_only = FALSE;
			if( !is_null($params["GET_ALL_FEEDS"]) && $params["GET_ALL_FEEDS"] ){
				$get_all_feeds = TRUE;
			}else if( !is_null($params["DISCUSSION_ONLY"]) && $params["DISCUSSION_ONLY"] ){
				$feedSection_str = " AND (tblActivityFeed.feedSection = '$DISCUSSION' OR tblActivityFeed.feedSection = '$DISCUSSIONPOST') ";
			}else if( !is_null($params["JOURNALS_ONLY"]) && $params["JOURNALS_ONLY"] ){
				$feedSection_str = " AND (tblActivityFeed.feedSection = '$JOURNAL' OR tblActivityFeed.feedSection = '$JOURNALENTRY') ";
				$journals_only = TRUE;
			}else if( !is_null($params["MULTIMEDIA_ONLY"]) && $params["MULTIMEDIA_ONLY"] ){
				$feedSection_str = " AND (tblActivityFeed.feedSection = '$PHOTOALBUM' OR tblActivityFeed.feedSection = '$PHOTO' OR tblActivityFeed.feedSection = '$VIDEOALBUM' OR tblActivityFeed.feedSection = '$VIDEO' OR tblActivityFeed.feedSection = '$ENTRYPHOTO' OR tblActivityFeed.feedSection = '$ENTRYVIDEO') ";
			}else if( !is_null($params["MEMBERSHIP_ONLY"]) && $params["MEMBERSHIP_ONLY"] ){
				$feedSection_str = " AND (tblActivityFeed.feedSection = '$JOINGROUPREQUEST' OR tblActivityFeed.feedSection = '$NEWMEMBER') ";
			}
			
			
			if( $params["GROUP_OBJECT"]->isParent() ){
				if( $get_all_feeds ){//all feeds in parent group view
					if( $subgroupOnly ){
						$sql = "(SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
								FROM tblActivityFeed, tblTraveler
								WHERE tblActivityFeed.sectionType = $GROUP
										AND tblActivityFeed.groupID = $groupID
										AND tblActivityFeed.travelerID = tblTraveler.travelerID
										AND tblTraveler.deactivated = 0
										AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								UNION ALL
								(SELECT tblActivityFeed.*
								FROM tblActivityFeed,
									tblGrouptoTraveler,
									tblTraveler
								WHERE tblActivityFeed.sectionType = $TRAVELER 
									AND tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
									AND tblGrouptoTraveler.groupID = $groupID
									AND tblActivityFeed.feedDate >= tblGrouptoTraveler.adate
									AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID
									AND tblTraveler.deactivated = 0
									AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								ORDER BY feedID DESC
								$limitstr";
					}else{
						$sql = "(SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
								FROM tblActivityFeed, tblTraveler
								WHERE tblActivityFeed.sectionType = $GROUP
									AND (tblActivityFeed.groupID = $groupID
										OR tblActivityFeed.parentGroupID = $groupID)
									AND tblActivityFeed.travelerID = tblTraveler.travelerID
									AND tblTraveler.deactivated = 0
									AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								UNION ALL
								(SELECT tblActivityFeed.*
								FROM tblActivityFeed,
									tblGrouptoTraveler,
									tblTraveler
								WHERE tblActivityFeed.sectionType = $TRAVELER
									AND tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
									AND tblActivityFeed.feedDate >= tblGrouptoTraveler.adate
									AND tblGrouptoTraveler.groupID = $groupID
									AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID
									AND tblTraveler.deactivated = 0
									AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								ORDER BY feedID DESC
								$limitstr";
					}
				}else{//specific feeds in parent group view
					if( $subgroupOnly ){
						$sql = "(SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
								FROM tblActivityFeed, tblTraveler
								WHERE 1 = 1  
									$feedSection_str
									AND tblActivityFeed.sectionType = $GROUP
									AND tblActivityFeed.groupID = $groupID
									AND tblActivityFeed.travelerID = tblTraveler.travelerID
									AND tblTraveler.deactivated = 0
									AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								UNION ALL
								(SELECT tblActivityFeed.*
								FROM tblActivityFeed,
									tblGrouptoTraveler,
									tblTraveler
								WHERE 1 = 1  
									$feedSection_str
									AND tblActivityFeed.sectionType = $TRAVELER 
									AND tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
									AND tblGrouptoTraveler.groupID = $groupID
									AND tblActivityFeed.feedDate >= tblGrouptoTraveler.adate
									AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID
									AND tblTraveler.deactivated = 0
									AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								ORDER BY feedID DESC
								$limitstr";
					}else{
						$sql = "(SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
								FROM tblActivityFeed, tblTraveler
								WHERE 1 = 1
									$feedSection_str
									AND tblActivityFeed.sectionType = $GROUP
									AND tblActivityFeed.parentGroupID = $groupID
									AND tblActivityFeed.travelerID = tblTraveler.travelerID
									AND tblTraveler.deactivated = 0
									AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								UNION ALL
								(SELECT tblActivityFeed.*
								FROM tblActivityFeed, tblTraveler
								WHERE 1 = 1
									$feedSection_str
									AND tblActivityFeed.sectionType = $GROUP
									AND tblActivityFeed.groupID = $groupID
									AND tblActivityFeed.travelerID = tblTraveler.travelerID
									AND tblTraveler.deactivated = 0
									AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								UNION ALL
								(SELECT tblActivityFeed.*
								FROM tblActivityFeed,
									tblGrouptoTraveler,
									tblTraveler
								WHERE 1 = 1
									$feedSection_str
									AND tblActivityFeed.sectionType = $TRAVELER
									AND tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
									AND tblActivityFeed.feedDate >= tblGrouptoTraveler.adate
									AND tblGrouptoTraveler.groupID = $groupID
									AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID
									AND tblTraveler.deactivated = 0
									AND tblTraveler.isSuspended = 0
								GROUP BY tblActivityFeed.feedID)
								ORDER BY feedID DESC
								$limitstr";
					}
				}
			}else{//feeds in subgroup view
				if( $get_all_feeds ){//all feeds in subgroup view
					$sql = "(SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
							FROM tblActivityFeed
							WHERE tblActivityFeed.sectionType = $GROUP
								AND tblActivityFeed.groupID = $groupID
							GROUP BY tblActivityFeed.feedID)
							UNION ALL
							(SELECT tblActivityFeed.*
							FROM tblActivityFeed,
								tblGrouptoTraveler,
								tblTraveler
							WHERE tblActivityFeed.sectionType = $TRAVELER 
								AND tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
								AND tblGrouptoTraveler.groupID = $groupID
								AND tblActivityFeed.feedDate >= tblGrouptoTraveler.adate
								AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID
								AND tblTraveler.deactivated = 0
								AND tblTraveler.isSuspended = 0
							GROUP BY tblActivityFeed.feedID)
							ORDER BY feedID DESC
							$limitstr";
				}else{//specific feeds in subgroup view
					$sql = "(SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
							FROM tblActivityFeed, tblTraveler
							WHERE 1 = 1  
								$feedSection_str
								AND tblActivityFeed.sectionType = $GROUP
								AND tblActivityFeed.groupID = $groupID
								AND tblActivityFeed.travelerID = tblTraveler.travelerID
								AND tblTraveler.deactivated = 0
								AND tblTraveler.isSuspended = 0
							GROUP BY tblActivityFeed.feedID)
							UNION ALL
							(SELECT tblActivityFeed.*
							FROM tblActivityFeed,
								tblGrouptoTraveler,
								tblTraveler
							WHERE 1 = 1  
								$feedSection_str
								AND tblActivityFeed.sectionType = $TRAVELER 
								AND tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
								AND tblGrouptoTraveler.groupID = $groupID
								AND tblActivityFeed.feedDate >= tblGrouptoTraveler.adate
								AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID
								AND tblTraveler.deactivated = 0
								AND tblTraveler.isSuspended = 0
							GROUP BY tblActivityFeed.feedID)
							ORDER BY feedID DESC
							$limitstr";
				}
			}
			
			if( $_SERVER["SERVER_NAME"] == "ga-web03.goabroad.net" ){
				echo "QUERY:  $sql<br /><br />";
			}
			
			$conn = new Connection();
			$rs = new Recordset($conn);
			$rs->Execute($sql);
			
			$rs2 = new Recordset($conn);
			$countSql = "SELECT FOUND_ROWS() AS `recordCount`";
			$rs2->Execute($countSql);
			
			$feeds = array();
			while( $data = mysql_fetch_assoc($rs->Resultset()) ){
				$feeds[] = new ActivityFeed(0,$data);
			}
			
			$feeds_count = 0;
			$data2 = mysql_fetch_assoc($rs2->Resultset());
			$feeds_count = $data2["recordCount"];
			//var_dump(count($feeds)." ".$feeds_count);
			return array("activityFeeds"=>$feeds, "recordCount"=>$feeds_count);
		}
		
		public static function getRecentMemberUpdates($params=array()){
			$temp = array(	"GROUP_OBJECT"				=>	NULL, //the instance of AdminGroup for the current group view
							"FEED_SECTION"				=>	NULL,
							"ROWS_LIMIT"				=>	NULL );
			$params = array_merge($temp,$params);

			if( !($params["GROUP_OBJECT"] instanceof AdminGroup) ){
				throw new exception("GROUP_OBJECT must be an instance of AdminGroup");
			}
			
			$groupID = $params["GROUP_OBJECT"]->getGroupID();
			
			$TRAVELER			= self::TRAVELER;
			
			$sectionType_str = " AND tblActivityFeed.sectionType = '$TRAVELER' ";
			
			$feedSection_str = "";
			$feedSection = $params["FEED_SECTION"];
			if( !is_null($params["FEED_SECTION"]) ){
				$feedSection = $params["FEED_SECTION"];
				$feedSection_str = " AND tblActivityFeed.feedSection = '$feedSection' ";
			}
			
			$limitstr = "";
			if( !is_null($params["ROWS_LIMIT"]) ){
				$offset   = $params["ROWS_LIMIT"]->getOffset();
                $rows     = $params["ROWS_LIMIT"]->getRows();
                $limitstr = " LIMIT " . $offset . " , " . $rows;
			}
			
			if( self::ENTRYPHOTO == $params["FEED_SECTION"] ){
				$sql = "SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
						FROM tblActivityFeed,
							tblGrouptoTraveler,
							tblTraveler,
							tblTravel,
							tblTravelLog,
							tblGroupApprovedJournals
						WHERE 1 = 1
							$sectionType_str
							$feedSection_str
							AND tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
							AND tblGrouptoTraveler.groupID = $groupID
							AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID
							AND tblTraveler.deactivated = 0
							AND tblTraveler.isSuspended = 0
							AND tblActivityFeed.feedDate >= tblGrouptoTraveler.adate
							AND tblTravelLog.travellogID = tblActivityFeed.sectionRootID
							AND tblTravel.travelerID = tblActivityFeed.travelerID
							AND	tblTravel.deleted = 0 
							AND tblTravel.publish = 1
							AND tblTravel.travelID = tblGroupApprovedJournals.travelID
							AND tblGroupApprovedJournals.groupID = $groupID
							AND tblGroupApprovedJournals.approved = 1 
						GROUP BY tblActivityFeed.travelerID
						ORDER BY tblActivityFeed.feedDate DESC, tblActivityFeed.feedID DESC
						$limitstr";
			}else{
				$sql = "SELECT SQL_CALC_FOUND_ROWS tblActivityFeed.*
						FROM tblActivityFeed,
							tblGrouptoTraveler,
							tblTraveler
						WHERE 1 = 1
							$sectionType_str
							$feedSection_str
							AND tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
							AND tblGrouptoTraveler.groupID = $groupID
							AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID
							AND tblTraveler.deactivated = 0
							AND tblTraveler.isSuspended = 0
							AND tblActivityFeed.feedDate >= tblGrouptoTraveler.adate
						GROUP BY tblActivityFeed.travelerID
						ORDER BY tblActivityFeed.feedDate DESC, tblActivityFeed.feedID DESC
						$limitstr";
			}
			$conn = new Connection();
			$rs = new Recordset($conn);
			$rs->Execute($sql);
			
			$rs2 = new Recordset($conn);
			$countSql = "SELECT FOUND_ROWS() AS `recordCount`";
			$rs2->Execute($countSql);

			$feeds = array();
			while( $data = mysql_fetch_assoc($rs->Resultset()) ){
				$feeds[] = new ActivityFeed(0,$data);
			}

			$feeds_count = 0;
			$data2 = mysql_fetch_assoc($rs2->Resultset());
			$feeds_count = $data2["recordCount"];
			return array("activityFeeds"=>$feeds, "recordCount"=>$feeds_count);
			
		}

		/**
		* fetch traveler network feeds
		**/
		public static function getTravelerNetworkFeeds($params=array()){
			$defaultParams = array(	"traveler"		=>	NULL,
									"cobrandGroup"	=>	NULL,
									"mode"			=>	'RECENT',
									"limitFeedID"	=>	NULL);
			$params = array_merge($defaultParams,$params);

			if( is_null($params["traveler"]) ){
				return array();
			}

			$travelerID = $params["traveler"]->getTravelerID();
			$dateJoined_str = " AND tblActivityFeed.feedDate >= '".$params["traveler"]->getTravelerProfile()->getDateJoined()."' ";

			$feedSection_str = " AND tblActivityFeed.feedSection != '".self::JOINGROUPREQUEST."' ".
								" AND tblActivityFeed.feedSection != '".self::JOURNAL."' ".
								" AND tblActivityFeed.feedSection != '".self::NEWMEMBER."' ";
			
			$mainGroupMembership_str = " AND tblActivityFeed.feedSection = '".self::NEWMEMBER."' ".
									" AND tblActivityFeed.parentGroupID = 0 ";

			$relativeFeedID_str = "";
			if( !is_null($params["limitFeedID"]) && 0 < $params["limitFeedID"] ){
				if( 'PREVIOUS' == strtoupper(trim($params["mode"])) ){
					$relativeFeedID_str = " AND tblActivityFeed.feedID < ".intval($params["limitFeedID"])." ";
				}elseif( 'RECENT' == strtoupper(trim($params["mode"])) ){
					$relativeFeedID_str = " AND tblActivityFeed.feedID > ".intval($params["limitFeedID"])." ";
				}
			}

			if( is_null($params["cobrandGroup"]) ){//main dotNet	
				$sql = "(SELECT tblActivityFeed.*
				 		FROM tblActivityFeed, tblFriend, tblTraveler
						WHERE 1=1
							$relativeFeedID_str
							$feedSection_str
							$dateJoined_str
							AND (tblFriend.friendID = $travelerID AND tblActivityFeed.travelerID = tblFriend.travelerID)
							AND (tblActivityFeed.travelerID = tblTraveler.travelerID AND tblTraveler.deactivated = 0 AND tblTraveler.isSuspended = 0)
						GROUP BY tblActivityFeed.feedID)
						UNION ALL
						(SELECT tblActivityFeed.*
						FROM tblActivityFeed
						WHERE 1=1
							$relativeFeedID_str
							$feedSection_str
							$dateJoined_str
							AND tblActivityFeed.travelerID = $travelerID
						GROUP BY tblActivityFeed.feedID)
						UNION ALL
						(SELECT tblActivityFeed.*
						FROM tblActivityFeed, tblFriend, tblTraveler
						WHERE 1=1
							$relativeFeedID_str
							$mainGroupMembership_str
							$dateJoined_str
							AND (tblFriend.friendID = $travelerID AND tblActivityFeed.travelerID = tblFriend.travelerID)
							AND (tblActivityFeed.travelerID = tblTraveler.travelerID AND tblTraveler.deactivated = 0 AND tblTraveler.isSuspended = 0)
						GROUP BY tblActivityFeed.feedID)
						UNION ALL
						(SELECT tblActivityFeed.*
						FROM tblActivityFeed
						WHERE 1=1
							$relativeFeedID_str
							$mainGroupMembership_str
							$dateJoined_str
							AND tblActivityFeed.travelerID = $travelerID
						GROUP BY tblActivityFeed.feedID)
						ORDER BY feedID DESC
						LIMIT 0,10";
			}else{//cobrand
				$groupID = $params["cobrandGroup"]->getGroupID();
				$sql = "(SELECT tblActivityFeed.*
						FROM tblActivityFeed, tblFriend, tblGrouptoTraveler, tblTraveler
						WHERE 1=1
							$relativeFeedID_str
							$feedSection_str
							$dateJoined_str
							AND (tblFriend.friendID = $travelerID AND tblActivityFeed.travelerID = tblFriend.travelerID)
							AND (tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
								AND tblGrouptoTraveler.groupID = $groupID)
							AND (tblActivityFeed.travelerID = tblTraveler.travelerID AND tblTraveler.deactivated = 0 AND tblTraveler.isSuspended = 0)
						GROUP BY tblActivityFeed.feedID)
						UNION ALL
						(SELECT tblActivityFeed.*
						FROM tblActivityFeed
						WHERE 1=1
							$relativeFeedID_str
							$feedSection_str
							$dateJoined_str
							AND tblActivityFeed.travelerID = $travelerID
						GROUP BY tblActivityFeed.feedID)
						UNION ALL
						(SELECT tblActivityFeed.*
						FROM tblActivityFeed, tblFriend, tblGrouptoTraveler, tblTraveler
						WHERE 1=1
							$relativeFeedID_str
							$mainGroupMembership_str
							$dateJoined_str
							AND (tblFriend.friendID = $travelerID AND tblActivityFeed.travelerID = tblFriend.travelerID)
							AND (tblActivityFeed.travelerID = tblGrouptoTraveler.travelerID
								AND tblGrouptoTraveler.groupID = $groupID)
							AND (tblActivityFeed.travelerID = tblTraveler.travelerID AND tblTraveler.deactivated = 0 AND tblTraveler.isSuspended = 0)
						GROUP BY tblActivityFeed.feedID)
						UNION ALL
						(SELECT tblActivityFeed.*
						FROM tblActivityFeed
						WHERE 1=1
							$relativeFeedID_str
							$mainGroupMembership_str
							$dateJoined_str
							AND tblActivityFeed.travelerID = $travelerID
						GROUP BY tblActivityFeed.feedID)
						ORDER BY feedID DESC
						LIMIT 0,10";
			}
			//echo $sql."<br />";
			$conn = new Connection();
			$rs = new Recordset($conn);
			$rs->Execute($sql);
			$feeds = array();
			while( $data = mysql_fetch_assoc($rs->Resultset()) ){
				$feeds[] = new ActivityFeed(0,$data);
			}
			
			return $feeds;
		}

	}