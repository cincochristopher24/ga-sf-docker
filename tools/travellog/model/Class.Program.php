<?php
/**
 * Created on Sep 6, 2006
 * @author Czarisse Daphne P. Dolina
 * Purpose: 
 */
     
    require_once("Class.Connection.php");
    require_once("Class.Recordset.php");
    require_once("travellog/model/Class.AdminGroup.php");
    require_once("travellog/model/Class.Photo.php");
    require_once("travellog/model/Class.Activity.php");
    
    
    class Program{
        
        
        /**
         * Define variables for Class Member Attributes
         */                              
            private $mConn      = NULL;
            private $mRs        = NULL;
           
            private $mGroupID        = NULL;
            
            private $mProgramID      = NULL;
            private $mTitle          = NULL;
            private $mStart          = NULL;
            private $mFinish         = NULL;
            private $mDescription    = NULL;
            private $mDisplayPublic  = NULL;
           
            private $mPhotos     = array();
            private $mActivities  = array();
           	
           	public  $phototype	= 6;
           	private $randphoto    = NULL;
                           
        /**
         * Constructor Function for this class
         */
            function Program($programID = 0){
                
                 $this->mProgramID = $programID;
                
                try {
                    $this->mConn = new Connection();
                    $this->mRs   = new Recordset($this->mConn);
                }
                catch (Exception $e) {                 
                   throw $e;
                }
                
                if (0 < $this->mProgramID){
                    
                    $sql = "SELECT * FROM tblProgram WHERE programID = '$this->mProgramID' ";
                    $this->mRs->Execute($sql);
                
                    if (0 == $this->mRs->Recordcount()){
                        throw new Exception("Invalid ProgramID");        // ID not valid so throw exception
                    }
                    
                    // Sets values to its class member attributes.   
                    $this->mTitle        = stripslashes($this->mRs->Result(0,"title"));           
                    $this->mStart        = $this->mRs->Result(0,"start");       
                    $this->mFinish       = $this->mRs->Result(0,"finish");   
                    $this->mDescription  = stripslashes($this->mRs->Result(0,"description"));
                    $this->mDisplayPublic  = $this->mRs->Result(0,"displaypublic");
                    
                    // Sets a value to its parent key.
                    $this->mGroupID      = $this->mRs->Result(0,"groupID");                     
                }
            }
           
           
       /**
         * Setter Functions
         */ 
            function setProgramID($programID){
               $this->mProgramID = $programID;
            }
            
            function setTitle($title){
               $this->mTitle = $title;
            }
           
            function setStart($start){
               $this->mStart = $start;
            }
           
            function setFinish($finish){
               $this->mFinish = $finish;
            }
           
            function setDescription($description){
               $this->mDescription = $description;
            }
            
            function setDisplayPublic($displaypublic){
               $this->mDisplayPublic = $displaypublic;
            }
           
           
       /**
         * Getter Functions
         */                
            function getProgramID(){
                return $this->mProgramID;
            }
            
            function getTitle(){
                return $this->mTitle;
            }
            
            function getStart(){
                return $this->mStart;
            }
            
            function getTheDate(){
                return $this->mStart;
            }
           
            function getFinish(){
                return $this->mFinish;
            }
           
            function getDescription(){
                return $this->mDescription;
            }
      
      		function getDisplayPublic(){
                return $this->mDisplayPublic;
            }
            
              
        /**
         * Setter Function for the Parent Key
         */
            function setGroupID($groupID){
                $this->mGroupID = $groupID;
            }     
            
             
        /**
         * Getter Function for the Parent Key
         */       
            function getGroupID(){
                return $this->mGroupID;
            }
            
            
            
           /**
            * added by K. Gordo
            * get subgroups associated with this program
            */ 
            function getSubgroups() {
            	$sql = "SELECT groupID FROM tblGrouptoProgram where programID='$this->mProgramID'";
            	
            	$groups = array();
            	
            	$this->mRs->Execute($sql);
            	
            	if ($this->mRs->Recordcount()){
            		while($row = mysql_fetch_assoc($this->mRs->Resultset())) {
            			$groups[] = new AdminGroup($row['groupID']);
            		}            		            		
            	}
            	
            	return $groups;
            	
            }
            
       
        /**
         * CRUD Methods (Create, Update , Delete)
         * In Create, only parent groups can create a program.
         * In Delete(), all photos and activities associated with this admin group must be deleted also.
         */
            function Create(){
                
                $grp = new AdminGroup($this->mGroupID);
               
                if ($grp->isParent()){    
                
                    $Atitle       = addslashes($this->mTitle);
                    $Adescription = addslashes($this->mDescription);
                    
                    $sql = "INSERT into tblProgram (title, start, finish, description, groupID, displaypublic) " .
                            "VALUES ('$Atitle', '$this->mStart', '$this->mFinish', '$Adescription', '$this->mGroupID', '$this->mDisplayPublic')";
                    $this->mRs->Execute($sql);       
                    $this->mProgramID = $this->mRs->GetCurrentID();
                }
            }
            
            function Update(){
                    
                $Atitle       = addslashes($this->mTitle);
                $Adescription = addslashes($this->mDescription);
                
                $sql = "UPDATE tblProgram SET title = '$Atitle', start = '$this->mStart', " .
                		"finish = '$this->mFinish', description = '$Adescription' , displaypublic = '$this->mDisplayPublic' " .
                        "WHERE programID = '$this->mProgramID' ";
                $this->mRs->Execute($sql);
            }
            
            function Delete(){
                
                $myphotos = $this->getPhotos();  
                for($idx=0;$idx<count($myphotos);$idx++){
                	$eachphoto = $myphotos[$idx];                	
                    $this->removePhoto($eachphoto->getPhotoID());           // delete photos of program
                }
                
                $myactivities = $this->getActivities();                    
                for($idx=0;$idx<count($myactivities);$idx++){
                	$eachactivity = $myactivities[$idx];      
                	$eachactivity->Delete();                                // delete activities of program
                }
                
                $sql = "DELETE FROM tblProgram WHERE programID = '$this->mProgramID' ";        // delete selected program   
                $this->mRs->Execute($sql);
                
                $sql = "DELETE FROM tblGrouptoProgram WHERE programID = '$this->mProgramID' ";        // delete selected program   
                $this->mRs->Execute($sql);
            }       
            
            
        /**
         * Purpose: Adds photo to this program.
         */
            function addPhoto($photo){
                
                //editted by: joel
            	//################################
            	$sqlq = "SELECT max(position) as mposition from tblProgramtoPhoto WHERE programID = '$this->mProgramID'";
	 			$this->mRs->Execute($sqlq);
            	
            	
            	if($this->mRs->Recordcount() > 0){
            			$rec = mysql_fetch_array($this->mRs->Resultset());
            			$pos = $rec['mposition']+1;           			
            	}else{
            			$pos =0;	
            	}
            	//################################
                
                $photoID = $photo->getPhotoID();
                $sql = "INSERT into tblProgramtoPhoto (programID, photoID, position) VALUES ('$this->mProgramID', '$photoID', '$pos')";
                $this->mRs->Execute($sql);                
            }
          
            
        /**
         * Purpose: Removes a photo from this program.
         * Deletes the photo as well as its link to this program.
         */
            function removePhoto($photoID){
                
               // $photo = new Photo($this, $photoID);                                            // delete photo
                //$photo->Delete();
                
                $sql = "DELETE FROM tblProgramtoPhoto " .
                        "WHERE  photoID = '$photoID' ";      // delete photo link to program
                $this->mRs->Execute($sql);       
            }
            
         //added by: joel
            
            function reArrangePhoto($marrphotoID){
            	
            	foreach($marrphotoID as $index => $photoID){
            			 $sql = "UPDATE tblProgramtoPhoto SET position = '$index' WHERE photoID =  '$photoID' ";
		 				$this->mRs->Execute($sql);
            	}
            }
         
         
        /**
         * Purpose: Returns an array of photos for this program.
         * Rows Limit is enabled for this function.
         */
            function getPhotos($rowslimit = NULL){
                
                if (0 == count($this->mPhotos)){
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    $sql = "SELECT photoID from tblProgramtoPhoto WHERE programID = '$this->mProgramID'  ORDER BY position DESC " . $limitstr;
                    $this->mRs->Execute($sql);
                    
                    if (0 == $this->mRs->Recordcount()){
                        return NULL;            
                    }
                    else {
                        while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                            try {
                                $photo = new Photo($this, $recordset['photoID']);
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                            $this->mPhotos[] = $photo;
                        }
                    }               
                }
                
                return $this->mPhotos;
            }
        
        
        /**
         * Purpose: Adds activity to this program.
         */
            function addActivity($activity){
                
                if (0 < $this->mProgramID){
	                $activity->setProgramID($this->mProgramID);              // set this programID for activity
	                $activity->Create();                                     // create activity 
            	}               
            }
            
            
        /**
         * Purpose: Removes activity from this program.
         */
            function removeActivity($activityID){
                
                $activity = new Activity($activityID);                   // instantiate activity
                $activity->Delete();                                     // delete activity of program                
            }
            
            
        /**
         * Purpose: Returns an array of activities for this program.
         * Rows Limit is enabled for this function.
         */
            function getActivities($rowslimit = NULL){
                
                if (0 == count($this->mActivities)){
                    
                    if (NULL != $rowslimit){
                        $offset   = $rowslimit->getOffset();
                        $rows     = $rowslimit->getRows();
                        $limitstr = " LIMIT " . $offset . " , " . $rows;
                    }  
                    else
                        $limitstr  = ''; 
                    
                    $sql = "SELECT activityID from tblActivity WHERE programID = '$this->mProgramID' ORDER BY activityID " . $limitstr;
                    $this->mRs->Execute($sql);
                    
                    if (0 == $this->mRs->Recordcount()){
                        return NULL;            
                    }
                    else {
                        while ($recordset = mysql_fetch_array($this->mRs->Resultset())) {
                            try {
                                $activity = new Activity($recordset['activityID']);
                            }
                            catch (Exception $e) {
                               throw $e;
                            }
                            $this->mActivities[] = $activity;
                        }
                    }               
                }
                
                return $this->mActivities;
            }
            
             // get random photo from the traveler profile
	 		function getRandomPhoto(){
	 			
	 			  $conn = new Connection();
				  $rs = new Recordset($conn);
	 					
	 			if ($this->randphoto == NULL){
	 				
	 				// get the number of photos to choose from
	 				$sql = "SELECT count(photoID) as totalrec from tblProgramtoPhoto WHERE programID = '$this->mProgramID' ";
	 				$rs->Execute($sql);
	 				
	 				if ($rs->Result(0,"totalrec") == 0){
	 					try {
	 						$this->randphoto = new Photo($this);
	 					}
						catch (Exception $e) {						  
						   throw $e;
						}
	 				}	 				
	 				else {
	 					
	 					$max = $rs->Result(0,"totalrec") - 1;
						
						$randidx = mt_rand(0 , $max );	 		// the index starting from top row
						
	 					$sql = "SELECT photoID FROM tblProgramtoPhoto WHERE programID = '$this->mProgramID' ORDER BY photoID LIMIT $randidx , 1"  ;
	 					$rs->Execute($sql);
	 					
	 					while ($recordset = mysql_fetch_array($rs->Resultset())) {
			 				try {	
			 					$this->randphoto = new Photo($this, $recordset['photoID'] );
			 				}
							catch (Exception $e) {						  
							   throw $e;
							}
	 					}
	 				}
	 			}
	 			
	 			return $this->randphoto;
	 		}
            
            
            /**
	 		 * Added by Joel 
	 		 * March 1 2007
	 		 * To check If Resume has primary photo
	 		 * @return Boolean 
	 		 */
            function checkHasprimaryphoto(){
	 			
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
	 			
	 			$sql = "SELECT primaryphoto from tblPhoto, tblProgramtoPhoto " .
	 					"WHERE tblProgramtoPhoto.photoID = tblPhoto.photoID " .
	 					"AND primaryphoto =1 " .
	 					"AND tblProgramtoPhoto.programID ='".$this->mProgramID."'" ;
				$rs->Execute($sql);
				
				
				if($rs->recordcount() == 0){
					return false;	
				}else{
					return true;
				}
            }
            
            /**
             * Added By: Aldwin
             * Purpose : To check if there are any photos for the specific program
             */             
            function hasPhoto(){
            	
                $sql = "SELECT photoID from tblProgramtoPhoto WHERE programID = '$this->mProgramID'  LIMIT 1";
                $this->mRs->Execute($sql);
                
                return ($this->mRs->Recordcount())? true:false;
            }                                         
    }
    
?>
