<?php

	class gaGroupMessagesManager extends gaMainMessagesManager{
		
		protected $_mGroup;
		
		public function init(){
			$this->loadPersonalMessages();
			$this->loadNews();
			$this->loadShoutOuts();
		}
		
		protected function loadNews() {
			$collNews = gaNewsDM::retrieveNewsByGroup($this->_mOwnerID, $this->_mGroup->getID());
			$this->wrapNews($collNews);

			usort($this->_mCollPrioritizedNews, array('gaMessageDM', 'orderByDate'));
			$this->_mCollPrioritizedNews = array_reverse($this->_mCollPrioritizedNews);

			usort($this->_mCollOrdinaryNews, array('gaMessageDM', 'orderByDate'));
			$this->_mCollOrdinaryNews = array_reverse($this->_mCollOrdinaryNews);
		}

		protected function loadShoutOuts() {
			$collShoutOuts = gaMessageDM::retrieveGroupShoutOuts($this->_mGroup->getID());
			foreach ($collShoutOuts as $message) {
				$this->_mCollShoutOuts[] = new gaMainShoutOutMessageAdapter($message);
			}
		}
		
		protected function loadPersonalMessages(){
			
		}
	}