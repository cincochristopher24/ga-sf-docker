<?php

abstract class gaPersonalMessage {
	
	const
		// message type
		TO_TRAVELER    = 1,
		TO_GROUP_STAFF = 2;
	
	protected $_mID;
	protected $_mDateCreated;
	protected $_mSenderID;
	protected $_mSubject;
	protected $_mBody;
	protected $_mIsMessageIncludedInNotif = 1;
	
	protected $_mSender;
	
	protected $_mCollRecipients = array();
	protected $_mRecipientIDs = array();
	
	protected $_mCollRecipientSettings;
	
	abstract public function getDisplayedRecipients();
	abstract public function send();
	
	// GETTERS
	public function getRecipientSettings() {
		return $this->_mCollRecipientSettings;
	}
	
	public function getID() {
		return $this->_mID;
	}
	
	public function getDateCreated() {
		return $this->_mDateCreated;
	}
	
	public function getSenderID() {
		return $this->_mSenderID;
	}
	
	public function getSender() {
		return $this->_mSender;
	}
	
	public function getSubject() {
		return $this->_mSubject;
	}
	
	public function getBody() {
		return $this->_mBody;
	}
	
	public function getRecipientIDs() {
		return $this->_mRecipientIDs;
	}
	
	public function getRecipients() {
		return $this->_mCollRecipients;
	}
	
	public function getIsMessageIncludedInNotif(){
		return $this->_mIsMessageIncludedInNotif;
	}

	// SETTERS
	public function setRecipientSettings(array $v) {
		$this->_mCollRecipientSettings = $v;
	}
	
	public function setID($v) {
		$this->_mID = (int) $v;
	}
	
	public function setDateCreated($v) {
		$this->_mDateCreated = $v;
	}
	
	public function setSenderID($v) {
		$this->_mSenderID = (int) $v;
	}
	
	public function setSender(gaTraveler $v) {
		$this->_mSender = $v;
	}
	
	public function setSubject($v) {
		$this->_mSubject = $v;
	}
	
	public function setBody($v) {
		$this->_mBody = $v;
	}
	
	public function setRecipientIDs(array $v) {
		$this->_mRecipientIDs = $v;
	}
	
	public function setRecipients(array $v) {
		$this->_mCollRecipients = $v;
	}
	
	public function setIsMessageIncludedInNotif($isIncluded=1){
		return $this->_mIsMessageIncludedInNotif = $isIncluded;
	}
	
}