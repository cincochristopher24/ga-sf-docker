<?php

class gaPersonalMessageCopyFactory {
	
	public static function create($type) {
		switch ($type) {
			case gaPersonalMessage::TO_TRAVELER:
				return new gaToTravelerPersonalMessageCopy;
			
			case gaPersonalMessage::TO_GROUP_STAFF:
				return new gaToGroupStaffPersonalMessageCopy;
			default:
				throw new Exception('Invalid Personal Message Type');
		}
	}
	
}