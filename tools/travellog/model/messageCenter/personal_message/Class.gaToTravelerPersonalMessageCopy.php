<?php

class gaToTravelerPersonalMessageCopy extends gaToTravelerPersonalMessage {
	
	protected $_mIsRead = false;
	protected $_mIsSpam;
	
	public function isRead() {
		return $this->_mIsRead;
	}
	
	public function isSpam() {
		return $this->_mIsSpam;
	}
	
	public function setReadStatus($v) {
		$this->_mIsRead = $v;
	}
	
	public function setSpamStatus($v) {
		$this->_mIsSpam = $v;
	}
	
}