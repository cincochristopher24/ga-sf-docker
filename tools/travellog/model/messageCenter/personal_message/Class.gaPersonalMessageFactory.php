<?php

class gaPersonalMessageFactory {
	
	public static function create($type) {
		switch ($type) {
			case gaPersonalMessage::TO_TRAVELER:
				return new gaToTravelerPersonalMessage;
			
			case gaPersonalMessage::TO_GROUP_STAFF:
				return new gaToGroupStaffPersonalMessage;
			
			default:
				throw new Exception('Invalid Personal Message Type');
		}
	}
	
}