<?php

require_once 'Class.Connection.php';
require_once 'Class.Recordset.php';

require_once 'Class.gaTraveler.php';

class gaTravelerMapper {
	
	protected static $identityMap = array();
	
	
	public static function getIDsByUserNames(array $collUserNames) {
		$rs = new Recordset(new Connection);
		
		foreach ($collUserNames as $key => $userName) {
			$collUserNames[$key] = "'$userName'";
		}
		
		$query = "SELECT t.travelerID " .
				 "FROM tblTraveler t " .
				 "WHERE t.username IN (" . join(',', $collUserNames) . ") " .
				 //"AND t.travelerID NOT IN (SELECT travelerID FROM tblBlockedUsers) " .
				 "AND t.isSuspended = 0 ".
				 "AND t.deactivated = 0";
		$rs->Execute($query);
		
		$collIDs = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_row($rs->Resultset())) {
				$collIDs[] = $row[0];
			}
		}
		
		return $collIDs;
	}
	
	public function getFriendIDs($travelerID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT f.friendID " .
				 "FROM tblFriend f " .
				 "INNER JOIN tblTraveler t " .
				 "ON t.travelerID = f.travelerID " .
				 "WHERE t.travelerID = $travelerID " .
				 "AND t.isSuspended = 0 ".
				 "AND t.deactivated = 0";
		$rs->Execute($query);
		
		$collFriendIDs = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_row($rs->Resultset())) {
				$collFriendIDs[] = $row[0];
			}
		}
		
		return $collFriendIDs;
	}
	
	public static function getFriendIDsFromUserNames($travelerID, array $collUserNames) {
		$rs = new Recordset(new Connection);
		
		foreach ($collUserNames as $key => $userName) {
			$collUserNames[$key] = "'$userName'";
		}
		
		$query = "SELECT f.friendID " .
				 "FROM tblTraveler t, tblFriend f " .
				 "WHERE t.travelerID = f.friendID " .
				 "AND t.username IN (" . join(',', $collUserNames) . ") " .
				 "AND f.travelerID = $travelerID";
		$rs->Execute($query);
		
		$collIDs = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_row($rs->Resultset())) {
				$collIDs[] = $row[0];
			}
		}
		
		return $collIDs;
	}
	
	public static function getUserIDsBlockedByTraveler($travelerID=0){
		$rs = new Recordset(new Connection);
		
		$query = "SELECT userID
				  FROM tblBlockedUsers
				  WHERE travelerID = {$travelerID}
				  GROUP BY userID";
		
		$rs->Execute($query);
		
		$blockedUserIDs = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_assoc($rs->Resultset())) {
				$blockedUserIDs[] = $row['userID'];
			}
		}

		return $blockedUserIDs;
	}
	
	public static function isUserBlockedByTraveler($userID=0,$travelerID=0){
		$rs = new Recordset(new Connection);
		
		$query = "SELECT *
				  FROM tblBlockedUsers
				  WHERE userID = {$userID}
				  	AND travelerID = {$travelerID}
				  LIMIT 0,1";
		
		$rs->Execute($query);
		
		return 0 < $rs->Recordcount();
	}
	
	public static function getFriendUserNames($travelerID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT t.username " .
				 "FROM tblFriend AS f, tblTraveler AS t " .
				 "WHERE f.friendID = t.travelerID " .
				 "AND f.travelerID = $travelerID " .
				 "AND t.isSuspended = 0";
		$rs->Execute($query);
		
		$collFriendUserNames = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_assoc($rs->Resultset())) {
				$collFriendUserNames[] = $row['username'];
			}
		}
		
		return $collFriendUserNames;
	}
	
	public static function retrieveTravelersByUserNames(array $collUserNames) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT t.* " .
				 "FROM tblTraveler as t " .
				 "WHERE t.username IN (" . join(',', $collUserNames) . ") " .
				 "AND t.isSuspended = 0"; 
		$rs->Execute($query);
		
		$collTravelers = array();
		if ($rs->Recordcount() > 0) {
			$collTravelers = self::loadFromRS($rs);
		}
		
		return $collTravelers;
	}
	
	public static function retrieveByPks(array $pks) {
		if (empty($pks))
			return array();
			
		// check identity map
		$unloadedPks = array_diff($pks, array_keys(self::$identityMap));
			
		$rs = new Recordset(new Connection);
		
		$query = "SELECT t.* " .
				 "FROM tblTraveler as t " .
				 "WHERE t.travelerID IN (" . join(',', $unloadedPks) . ") " .
				 "AND t.isSuspended = 0 " .
				 "LIMIT 1";
		$rs->Execute($query);		 
		
		self::loadFromRS($rs);
		
		$travelers = array();
		foreach ($pks as $travelerID) {
			$travelers[] = self::$identityMap[$travelerID];
		}
		
		return $travelers;
	}
	
	public static function getPrimaryPhotoLink($travelerID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT DISTINCT p.photoID " .
				 "FROM tblPhoto AS p, tblTravelertoPhoto as tp " .
				 "WHERE tp.photoID = p.photoID " .
				 "AND p.primaryphoto = 1 " .
				 "AND tp.travelerID = $travelerID " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			require_once 'travellog/model/Class.Photo.php';
			require_once 'travellog/model/Class.TravelerProfile.php';
			
			$photo = new Photo(new TravelerProfile($travelerID), $rs->Result(0, 'photoID'));
			
			return $photo->getPhotoLink(); 
		}
		
		return NULL;
	}
	
	public static function registerAccount($email, $password, $username, $firstname = '', $lastname = '') {
		$rs = new Recordset(new Connection);

    	// determine type of user
   	 	$sql = "INSERT INTO tblSendable (sendableType) " .
         	   "VALUES (1)";									// 1 traveler
    	$rs->Execute($sql);
    	
    	$query = "SELECT LAST_INSERT_ID() as id";
    	$rs->Execute($query);
    	
    	$sendableID = $rs->Result(0, 'id');

	    // create new Traveler
	    $sql = "INSERT INTO tblTraveler (sendableID, username, password, firstname, lastname, email, dateregistered, active) " .
	         "VALUES ($sendableID, '%s', '%s', '%s', '%s', " .
	         "'%s', now(), 1)";

	    $sql = sprintf($sql,
	          mysql_real_escape_string ( $username  ),
	          mysql_real_escape_string ( $password  ),
	          mysql_real_escape_string ( $firstname ),
	          mysql_real_escape_string ( $lastname  ),
	          mysql_real_escape_string ( $email     )
	         );
	
	    $rs->Execute($sql);
	    
	    $query = "SELECT LAST_INSERT_ID() as id";
    	$rs->Execute($query);
	    
	    return $rs->Result(0, 'id');
	}
	
	public static function eMailExists($email) {
		/*$rs = new Recordset(new Connection);

    	$query = "SELECT travelerID " .
    			 "FROM tblTraveler " .
    			 "WHERE email = '$email' " .
    			 "LIMIT 1";
		
    	$rs->Execute($query);
    	
    	if ($rs->Recordcount() > 0) {
    		return TRUE;
    	}
    	
    	return FALSE;*/
    	require_once 'travellog/rules/Class.TravelerAccountRules.php';
	
		return !TravelerAccountRules::validateTravelerEmail($email);
	}
	
	/**
	 * retrieves the country id of a traveler based on his hometown
	 * 
	 * @param $htlocationID => home town location ID
	 * @return country ID
	 */
	public static function getHomeTownCountryID($htlocationID) {
		if (0 == $htlocationID)
			return 0;
		
		$rs = new Recordset(new Connection());
		
		$query = "SELECT stateID " .
				 "FROM GoAbroad_Main.tbstate " .
				 "WHERE locID = $htlocationID";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			return 91; //United States
		}
		
		$query = "SELECT countryID " .
				 "FROM GoAbroad_Main.tbcity " .
				 "WHERE locID = $htlocationID " .
				 "UNION " .
				 "SELECT countryID " .
				 "FROM GoAbroad_Main.tbcountry " .
				 "WHERE locID = $htlocationID ";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			return $rs->Result(0, 'countryID');
		}
		
		return 0;
	}
	
	public static function getGroupStaffs($group) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT t.* " .
				 "FROM tblGroup as g, tblGrouptoFacilitator as f, tblTraveler as t " .
				 "WHERE g.groupID = f.groupID " .
				 "AND f.travelerID = t.travelerID " .
				 "AND g.groupID = {$group->getID()} " .
				 "AND f.status = 1 ".
				 "AND t.isSuspended = 0 ".
				 "AND t.deactivated = 0";
		$rs->Execute($query);
		
		if ($rs->Recordcount() <= 0) {
			return array();
		}
		
		return self::loadFromRS($rs);
	}
	
	protected static function doLoadOneFromRS($rs) {
		$result = self::loadFromRS($rs);
		
		return $result[0];
	}
	
	public static function getTravelerWithSendableID($sendableID) {
		$rs = new Recordset(new Connection());
		
		$query = "SELECT t.* " .
				 "FROM tblTraveler AS t " .
				 "WHERE t.sendableID = $sendableID " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			$traveler = self::doLoadOneFromRS($rs);
			
			return $traveler;
		} else {
			throw new Exception("SendableID: $sendableID doesn't match any record in tblTraveler");
		}
	}
	
	public static function getTravelersBySendableIDs(array $keys) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT t.* " .
				 "FROM tblTraveler as t " .
				 "WHERE t.sendableID IN (" . join(',', $keys) . ")";
		$rs->Execute($query);
		
		return self::loadFromRS($rs);
	}
	
	public static function getTravelersByPrimaryKeys(array $keys) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT t.* " .
				 "FROM tblTraveler as t " .
				 "WHERE t.travelerID IN (" . join(',', $keys) . ")";
		$rs->Execute($query);
		
		return self::loadFromRS($rs);
	}
	
	/**
	 * same as getTraveler only that it requires the e-mail address
	 * and password of the user
	 * @param $eMail
	 * @param $password
	 * 
	 * @return gaTraveler object if found
	 * @return NULL otherwise
	 */
	public static function getTravelerByCredentials($eMail, $password) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT t.* " .
				 "FROM tblTraveler AS t " .
				 "WHERE t.email = '%s' " .
				 "AND t.password = '%s' " .
				 "AND t.isSuspended = 0 ".
				 "AND t.deactivated = 0 ".
				 "LIMIT 1";
		$query = sprintf($query, mysql_real_escape_string($eMail), mysql_real_escape_string($password));
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			$traveler = self::doLoadOneFromRS($rs);
			
			return $traveler;
		}
		
		return NULL;
	}
	
	public static function getTraveler($travelerID) {
		// check identity map
		if (array_key_exists($travelerID, self::$identityMap)) {
			return self::$identityMap[$travelerID];
		}
		
		$rs = new Recordset(new Connection());
		
		// TODO: make sure the traveler is active
		$query = "SELECT t.* " .
				 "FROM tblTraveler as t " .
				 "WHERE t.travelerID = $travelerID " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			$traveler = self::doLoadOneFromRS($rs);
			
			return $traveler;
		}
		
		return NULL;
	}
	
	public static function getTravelerIDWithUserName($userName) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT travelerID " .
				 "FROM tblTraveler " .
				 "WHERE username = '$username'" .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			$row = mysql_fetch_row($rs->Resultset());
			
			return $row[0];
		}
		
		return NULL;
	}
	
	public static function getSendableIDsWithUserNames(array $userNames) {
		$rs = new Recordset(new Connection);
		
		$pool = array();
		foreach ($userNames as $userName) {
			$pool[] = '"' . $userName . '"';
		}
		
		$query = "SELECT sendableID " .
				 "FROM tblTraveler " .
				 "WHERE username IN (" . join(',', $pool) . ") " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		$travelerIDs = array();
		if ($rs->Recordcount() > 0) {
			while ($row = mysql_fetch_row($rs->Resultset())) {
				$travelerIDs[] = $row[0];
			}
		}
		
		return $travelerIDs;
	}
	
	public static function getSendableIDByTravelerID($travelerID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT sendableID " .
				 "FROM tblTraveler " .
				 "WHERE travelerID = $travelerID " .
				 "LIMIT 1";
		$rs->Execute($query);
		
		if ($rs->Recordcount()) {
			$row = mysql_fetch_row($rs->Resultset());
			
			return $row[0];
		} else {
			throw new Exception ("travelerID $travelerID not found in tblTraveler");
		}
	}
	
	public static function loadFromRS($rs) {
		$travelers = array();
		while ($row = mysql_fetch_assoc($rs->Resultset())) {
			if (array_key_exists($row['travelerID'], self::$identityMap)) {
				$traveler = self::$identityMap[$row['travelerID']];
			} else {
				$traveler = new gaTraveler;
			
				$traveler->setID           ( $row['travelerID']   );
				$traveler->setSendableID   ( $row['sendableID']   );
				$traveler->setUserName     ( $row['username']     );
				
				$traveler->setFirstName    ( $row['firstname']    );
				$traveler->setLastName     ( $row['lastname']     );
				$traveler->setEMailAddress ( $row['email']        );
				$traveler->setAddress      ( trim($row['address1'] . ' ' . $row['address2']) );
				$traveler->setPhone        ( $row['phone']        );

				$traveler->setHtLocationID ( $row['htlocationID'] );
				$traveler->setSignature    ( $row['signature']    );
				
				//$traveler->setFriendIDs    ( self::getFriendIDs($row['travelerID']) );
				
				self::$identityMap[$traveler->getID()] = $traveler;
			}
	
			$travelers[] = $traveler;
		}
		
		return $travelers;
	}
	
	public static function getMessageRecipients($messageID) {
		$rs = new Recordset(new Connection);
		
		$query = "SELECT DISTINCT t.* " .
				 "FROM tblMessageToAttribute AS a, tblTraveler AS t " .
				 "WHERE a.recipientID = t.sendableID " .
				 "AND a.messageType = " . gaMessage::OUTBOX . " " .
				 "AND a.messageID = $messageID";
		$rs->Execute($query);
		
		if ($rs->Recordcount() > 0) {
			return self::loadFromRS($rs);
		}
		
		return array();
	}

	/**
	* rude workaround to aid traveler search criteria ranking
	* so as to make use of the light gaTraveler class
	**/
	public static function getAllTravelers(){
		$rs = new Recordset(new Connection);
		$query = "SELECT t.*
				FROM tblTraveler AS t
				WHERE t.active = 1 
					AND t.isSuspended = 0 
					AND t.deactivated = 0";
		$rs->Execute($query);
		if ($rs->Recordcount() > 0) {
			return self::loadFromRS($rs);
		}
		
		return array();
	}
	
	/**
	* rude workaround to aid traveler search criteria ranking
	* so as to make use of the light gaTraveler class
	**/
	public static function getTravelerByEmail($email, $groupID){
		
		if(self::eMailExists($email)){
			$rs = new Recordset(new Connection);
			$query = "SELECT t.*
					FROM tblTraveler AS t, tblGrouptoTraveler AS gt
					WHERE t.travelerID = gt.travelerID
						AND gt.groupID = '$groupID'
						AND t.active = 1 
						AND t.isSuspended = 0 
						AND t.deactivated = 0
						AND t.email = '".mysql_real_escape_string($email)."'";
			$rs->Execute($query);

			if ($rs->Recordcount() > 0) {
				$traveler = self::doLoadOneFromRS($rs);
				return $traveler;
			}
		}
		return NULL;
	}
}