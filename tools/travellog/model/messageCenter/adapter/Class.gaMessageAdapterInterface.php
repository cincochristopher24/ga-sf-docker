<?php

interface gaMessageAdapterInterface {
	
	function getID();
	
	function getDateCreated();
	function getTitle();
	function getContent();
	function hasContent();
	function isReplyAllowed();
	function getContentType();
	function getSender();
	function getSenderID();
	
	function getSenderRealName();
	
	// can be transferred to presentation
	function getCssClass();
	function getSenderLink();
	function getRecipientsLink();
	function getRecipientsRealNameLink();
	
	function isSenderGAMember();
	
}