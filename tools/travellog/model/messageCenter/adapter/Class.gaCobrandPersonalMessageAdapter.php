<?php

class gaCobrandPersonalMessageAdapter extends gaPersonalMessageAdapter {
	
	protected $_mGroup;
	
	public function __construct(gaPersonalMessage $message) {
		require_once('travellog/model/Class.SiteContext.php');
		
		$siteContext = SiteContext::getInstance();
		
		$this->_mGroup = gaGroupMapper::getGroup($siteContext->getGroupID());
		parent::__construct($message);
	}
	
	public function getSenderLink() {
		if (in_array($this->_mOriginalMessage->getSender()->getID(), $this->_mGroup->getMemberIDs())) {
			return '<a href="/' . $this->_mOriginalMessage->getSender()->getUserName() . '">' . $this->_mOriginalMessage->getSender()->getUserName() . '</a>';
		} else {
			return $this->_mOriginalMessage->getSender()->getUserName();
		}
	}
	
	public function getRecipientsLink($showRealName = false) {
		$links = array();
		foreach ($this->_mOriginalMessage->getDisplayedRecipients() as $recipient) {
			if (get_class($recipient) == 'gaTraveler' && in_array($recipient->getID(), $this->_mGroup->getMemberIDs())) {
				$link = '<a href="' . $recipient->getFriendlyURL() . '">' . $recipient->getDisplayName() . '</a>';
			} elseif (get_class($recipient) == 'gaGroup' && in_array($recipient->getID(), array_merge(array($this->_mGroup->getID()), gaGroupMapper::retrieveSubGroupIDs($this->_mGroup->getID())))) {
				$link = '<a href="' . $recipient->getFriendlyURL() . '">' . $recipient->getDisplayName() . '</a>';
			} else {
				$link = $recipient->getDisplayName();
			}
			
			if ($showRealName) {
				$link .= ' <span style="font-color:gray"><em>' . $recipient->getName() . '</em></span>';
			}
			
			$links[] = $link;
		}

		return join(', ', $links);
	}
	
}