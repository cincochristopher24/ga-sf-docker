<?php

abstract class gaShoutOutMessageAdapter implements gaMessageAdapterInterface {
	
	protected $_mOriginalMessage;
	
	protected $_mPokesView;
	
	public function __construct($message) {
		$this->_mOriginalMessage = $message;
		$this->initShoutOuts($message);
	}
	
	abstract protected function getPokesView();
	
	protected function initShoutOuts($message) {
		require_once 'travellog/model/Class.PokeType.php';
		
		$Poke      = new PokeType($message->getPokeType());
		$this->_mPokesView = $this->getPokesView();

		// creator of the shout out
		$this->_mPokesView->setAuthorId     ( $message->getAuthor()       );
		$this->_mPokesView->setAuthorObject ( $message->getAuthorObject() );
		
		// target of the shout out
		$this->_mPokesView->setOwnerId      ( $message->getTravelerRecipientID() );
		
		$this->_mPokesView->setPokeTypeId   ( $message->getPokeTypeID()   );
		$this->_mPokesView->setMessage      ( $message->getTheText()      );

		$messageContext = $message->getContext();

		if (!strcasecmp("TravelLog", get_class($messageContext))){
			$this->_mPokesView-> setContextId($messageContext->getTravelLogID());
			$this->_mPokesView-> setContext(3);
		}
		elseif (!strcasecmp("Photo", get_class($messageContext))){
			$this->_mPokesView-> setContextId($messageContext->getPhotoID());
			$this->_mPokesView-> setContext(2);
		}
		elseif(!strcasecmp("Article", get_class($messageContext))){
			$this->_mPokesView-> setContextId($messageContext->getArticleID());
			$this->_mPokesView-> setContext(4);
		}
		else{
			$this->_mPokesView-> setOwnerID($messageContext->getTravelerID());
			$this->_mPokesView-> setContextId($messageContext->getTravelerID());
			$this->_mPokesView-> setContext(1);
		}

		if (0 < strlen($message-> getTheText()))
			$this->_mPokesView-> setHasMessage(true);
		else
			$this->_mPokesView-> setHasMessage(false);
		
		$this->_mPokesView->setAuthor();
		$this->_mPokesView->setOwner();
	}
	
	public function isRead() {
		return $this->_mOriginalMessage->isRead();
	}
	
	public function getID() {
		return $this->_mOriginalMessage->getCommentID() . '-' . $this->getContentType() . '-' . $this->isRead();
	}
	
	public function getCssClass() {
		return 'shoutout';
	}
	
	public function getSenderLink() {
		return $this->_mPokesView->getSenderPath();
	}
	
	public function getDateCreated() {
		return $this->_mOriginalMessage->getCreated();
	}
	
	public function getTitle() {
		return $this->_mPokesView->getPassportMessage();
	}
	
	public function getContent() {
		return $this->_mOriginalMessage->getTheText();
		//return '';
	}
	
	public function hasContent() {
		return "" != trim($this->_mOriginalMessage->getTheText());
		//return false;
	}
	
	public function isReplyAllowed() {
		return false;
	}
	
	public function getContentType() {
		return 5;	// gaMessage::SHOUTOUT
	}
	
	public function getSender() {
		return '';
	}
	
	public function getSenderID() {
		return 0;
	}
	
	public function getRecipientsLink() {
		return '';
	}
	
	public function getSenderRealName() {
		return '';
	}
	
	public function getRecipientsRealNameLink() {
		return array();
	}
	
	public function isSenderGAMember(){
		return 0 < $this->_mOriginalMessage->getAuthor();
	}
}