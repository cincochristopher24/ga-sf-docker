<?php

class gaMainNewsMessageAdapter extends gaNewsMessageAdapter {
	
	public function getSenderLink() {
		return '<a href="' . $this->_mOriginalMessage->getSender()->getFriendlyURL() . '">' . $this->_mOriginalMessage->getSender()->getName() . '</a>';
	}
	
}