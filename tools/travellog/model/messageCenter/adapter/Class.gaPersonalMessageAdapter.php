<?php

class gaPersonalMessageAdapter implements gaMessageAdapterInterface {
	
	protected $_mOriginalMessage;
	
	public function __construct($message) {
		$this->_mOriginalMessage = $message;
	}
	
	public function getID() {
		return $this->_mOriginalMessage->getID();
	}
	
	public function getCssClass() {
		return 'message';
	}
	
	public function getSenderLink() {
		return '<a href="/' . $this->_mOriginalMessage->getSender()->getUserName() . '">' . $this->_mOriginalMessage->getSender()->getUserName() . '</a>';
	}
	
	public function getDateCreated() {
		return $this->_mOriginalMessage->getDateCreated();
	}
	
	public function getTitle() {
		return $this->_mOriginalMessage->getSubject();
	}
	
	public function getContent() {
		return $this->_mOriginalMessage->getBody();
	}
	
	public function hasContent() {
		return true;
	}
	
	public function isReplyAllowed() {
		return true;
	}
	
	public function getContentType() {
		return 3;	// gaMessage::PERSONAL
	}
	
	public function getSender() {
		return $this->_mOriginalMessage->getSender()->getUserName();
	}
	
	public function getSenderID() {
		return $this->_mOriginalMessage->getSender()->getID();
	}
	
	public function getRecipientsLink($showRealName = false) {
		$links = array();
		foreach ($this->_mOriginalMessage->getDisplayedRecipients() as $recipient) {
			$link = '<a href="' . $recipient->getFriendlyURL() . '">' . $recipient->getDisplayName() . '</a>';
			if ($showRealName) {
				$link .= ' <span style="font-color:gray"><em>' . $recipient->getRealName() . '</em></span>';
			}
			
			$links[] = $link;
		}

		return join(', ', $links);
	}
	
	public function getRecipientsRealNameLink() {
		return $this->getRecipientsLink(true);
	}
	
	public function getSenderRealName() {
		return $this->_mOriginalMessage->getSender()->getName();
	}
	
	public function isSenderGAMember(){
		TRUE;
	}
	
}