<?php

class gaMessage {
    
    const 
    	
    	// tblMessages discriminator
    	ALERT             =  1,
    	NEWS              =  2,
    	PERSONAL          =  3,
    	INQUIRY           =  4,
    	
    	
    	// tblComment
    	SHOUTOUT          =  5,
    	
    	// tblMessagesToAttribute messageType
    	INBOX             =  1,
    	OUTBOX            =  2,
    	SPAM              =  5,
    	
    	// tblMessageToAttribute isRead
    	READ              =  1,
    	UNREAD            =  0,
    	NA                =  9,
    	
    	// alerts
    	ALL_ALERTS        =  1,
    	EXPIRED_ALERT     =  2,
    	UNEXPIRED_ALERT   =  3;
    
    protected $id             =  NULL;
    protected $senderID       =  NULL;
    protected $title          =  NULL;
    protected $message        =  NULL;
    protected $dateCreated    =  NULL;
    protected $isRead         =  NULL;
    protected $sender         =  NULL;
    protected $discriminator  =  NULL;
    protected $recipientID    =  NULL;
    
    // to be deleted
    protected $recipient      =  NULL;
    
    protected $recipients     =  NULL;
	protected $attributeID	  =	 NULL;
    
    // GETTERS
    public function getDisplayedRecipients() {
    	return $this->getRecipients();
    }
    
    public function getRecipients() {
    	return $this->recipients;
    }
    
    public function getRecipient() {
    	return $this->recipient;
    }
    
    public function getRecipientID() {
    	return $this->recipientID;
    }
    
    public function getDiscriminator() {
    	return $this->discriminator;
    }
    
    public function isRead() {
    	return $this->isRead;
    }
    
    public function getSender() {
    	return $this->sender;
    }
    
    public function getID() {
        return $this->id;
    }
    
    public function getSenderID() {
        return $this->senderID;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getMessage() {
    	return $this->message;
    }
    
    public function getDateCreated() {
    	return $this->dateCreated;
    }

	public function getAttributeID(){
		return $this->attributeID;
	}
    
    // SETTERS
    public function setRecipients($v) {
    	$this->recipients = $v;
    }
    
    public function setRecipient($v) {
    	$this->recipient = $v;
    }
    
    public function setRecipientID($v) {
    	$this->recipientID = $v;
    }
    
    public function setDiscriminator($v) {
    	$this->discriminator = $v;
    }
    
    public function setIsRead($v) {
    	$this->isRead = $v;
    }
    
    public function setSender($v) {
    	$this->sender = $v;
    }
    
    public function setID($v) {
    	$this->id = $v;
    }
    
    public function setSenderID($v) {
    	$this->senderID = $v;
    }
    
    public function setTitle($v) {
    	$this->title = $v;
    }
    
    public function setMessage($v) {
    	$this->message = $v;
    }
    
    public function setDateCreated($v) {
    	$this->dateCreated = $v;
    }

	public function setAttributeID($v){
		$this->attributeID = $v;
	}
    
    // alias
    public function getCreated() {
    	return $this->getDateCreated();
    }
    
}