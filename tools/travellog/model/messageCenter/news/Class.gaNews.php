<?php

class gaNews {
	
	protected $id          	=  null;
    protected $groupID	   	=  null;
    protected $title       	=  null;
    protected $content     	=  null;
    protected $dateCreated 	=  null;
    protected $expiration  	=  null;
    
	protected $_mAuthorID;
	
    protected $_mSender;
        
    // GETTERS
    public function getAuthorID() {
    	return $this->_mAuthorID;
    }
    
    public function getSender() {
    	return $this->_mSender;
    }
    
    public function getGroupID() {
    	return $this->groupID;
    }
    
    public function getID() {
        return $this->id;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getContent() {
    	return $this->content;
    }
    
    public function getDateCreated() {
    	return $this->dateCreated;
    }
    
    public function getExpiration() {
    	return $this->expiration;
    }
    
    // SETTERS
    public function setAuthorID($v) {
    	$this->_mAuthorID = (int) $v;
    }
    
    public function setSender(gaGroup $v) {
    	$this->_mSender = $v;
    }
    
    public function setID($v) {
    	$this->id = $v;
    }
    
    public function setGroupID($v) {
    	$this->groupID = $v;
    }
    
    public function setTitle($v) {
    	$this->title = $v;
    }
    
    public function setContent($v) {
    	$this->content = $v;
    }
    
    public function setDateCreated($v) {
    	$this->dateCreated = $v;
    }
    
    public function setExpiration($v) {
    	$this->expiration = $v;
    }
    
    // alias
    public function getCreated() {
    	return $this->getDateCreated();
    }
    
    // CRUD
    public function save() {
    	return gaNewsDM::save($this, $this->_mAuthorID);
    }
    
	public function delete() {
		gaNewsDM::delete($this->getID());
	}
	

    // CONVENIENCE
    public function isPrioritized() {
		$expiration = explode(' ',$this->getExpiration());
		
    	$expireTimeStamp = strtotime($expiration[0] . ' 23:59:59');
		
    	if (!$this->getExpiration() == NULL && $expireTimeStamp > time()) {
    		return TRUE;
    	}
    	
    	return FALSE;
    } 
	
}