<?php

class gaNewsCopy extends gaNews {
	
	protected $isRead;
	
	public function isRead() {
		return $this->isRead;
	}
	
	public function setReadStatus($v) {
		$this->isRead = $v;
	}
	
}