<?php

class gaGroup {
	
	const
		FUN    =  1,
		ADMIN  =  2,
		
		PARENT =  0;
	
	protected $id              = NULL;
	protected $name            = NULL;
	protected $isParent        = NULL;
	protected $sendalbeID      = NULL;
	protected $administratorID = NULL;
	protected $discriminator   = NULL;
	protected $_mCollMemberIDs;
	
	protected $advisor         = NULL;
	protected $collStaffs      = NULL;
	
	protected $parentGroup     = NULL;
	
	// GETTERS
	public function getMemberIDs() {
		return $this->_mCollMemberIDs;
	}
	
	public function getAdministratorID() {
		return $this->administratorID;
	}
	
	public function getParentGroup() {
		if ($this->isParent()) {
			// TODO: consider returning null
			return $this;
		}
		
		return $this->parentGroup;
	}
	
	public function getAdvisor() {
		return $this->advisor;
	}
	
	public function getStaffs() {
		return $this->collStaffs;
	}
	
	public function getSuperStaffs() {
		if ($this->isParent()) {
			return $this->getStaffs();
		} else {
			return $this->getParentGroup()->getStaffs();
		}
	}
	
	public function getSendableID() {
		return $this->sendableID;
	}
	
	public function getFriendlyURL() {
		$url = str_replace('-', '_', $this->getName());
		$url = str_replace(' ', '-', $url);
		
		if ($this->isParent()) {
			$url = '/groups/' . $url;
		} else {
			$parentURL = str_replace('-', '_', $this->getParentGroup()->getName());
			$parentURL = str_replace(' ', '-', $parentURL);
			
			$url = '/groups/' . $parentURL . '/subgroup/' . $url;
		}
		
		return $url;
	}
	
	public function isParent() {
		return $this->isParent;
	}
	
	public function isSubGroup() {
		if ($this->isParent()) {
			return FALSE;
		}
		
		return TRUE;
	}
	
	public function isClub(){
		return self::FUN == $this->discriminator;
	}
	
	public function getID() {
		return $this->id;
	}
	
	public function getGroupID(){
		return $this->getID();
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getDiscriminator(){
		return $this->discriminator;
	}
	
	// SETTERS
	public function setMemberIDs(array $v) {
		$this->_mCollMemberIDs = $v;
	}
	
	public function setAdministratorID($v) {
		$this->administratorID = $v;
	}
	
	public function setParentGroup($v) {
		$this->parentGroup = $v;
	}
	
	public function setAdvisor($v) {
		$this->advisor = $v;
	}
	
	public function setStaffs($v) {
		$this->collStaffs = $v;
	}
	
	public function setSendableID($v) {
		$this->sendableID = $v;
	}
	
	public function setIsParent($v) {
		$this->isParent = $v;
	}
	
	public function setID($v) {
		$this->id = $v;
	}
	
	public function setName($v) {
		$this->name = $v;
	}
	
	public function setDiscriminator($v){
		$this->discriminator = $v;
	}
	
	// CONVENIENCE
	/**
	 * returns the travelers with admin priveleges in the group
	 * PARENT GROUP = the advisor and super staff
	 * SUB GROUP = staff
	 */
	public function getOverSeerIDs() {
		return array_merge(array($this->getAdministratorID()), $this->getSuperStaffIDs(), $this->getStaffIDs());
	}
	
	public function getRealName() {
		return '';
	}
	
	public function getDisplayName() {
		return $this->getName();
	}
	
	public function getStaffIDs() {
		$staffIDs = array();
		
		foreach ($this->getStaffs() as $staff) {
			$staffIDs[] = $staff->getID();
		}
		
		return $staffIDs;
	}
	
	public function getSuperStaffIDs() {
		$superStaffIDs = array();
		
		foreach ($this->getSuperStaffs() as $superStaff) {
			$superStaffIDs[] = $superStaff->getID();
		}
		
		return $superStaffIDs;
	}
	
}