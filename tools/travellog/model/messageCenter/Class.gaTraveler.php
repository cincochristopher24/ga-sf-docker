<?php

class gaTraveler {

	const
		ANONYMOUS  = 1,
		OWNER      = 2,
		FRIEND     = 3,
		NON_FRIEND = 4;
	
	
	protected $id           =  NULL;
	protected $username     =  NULL;
	protected $sendableID   =  NULL;
	protected $_mCignature;
	
	protected $firstName;
	protected $lastName;
	protected $eMail;
	protected $address;
	protected $phone;
	
	protected $htLocationID;
	
	protected $_mCollFriendIDs = NULL;
	
	protected $administeredGroups = NULL;
	protected $mainGroupsJoined = NULL;

	protected $countTravelJournals = NULL;
	protected $isProfileCompleted = NULL;
	
	// GETTERS
	public function getFriendIDs() {
		if( is_null($this->_mCollFriendIDs) ){
			$this->_mCollFriendIDs = gaTravelerMapper::getFriendIDs($this->getID());
		}
		return $this->_mCollFriendIDs;
	}
	
	public function getHtLocationID() {
		return $this->htLocationID;
	}
	
	public function getPhone() {
		return $this->phone;
	}
	
	public function getSignature() {
		return $this->_mSignature;
	}
	
	public function getSendableID() {
		return $this->sendableID;
	}
	
	public function getID() {
		return $this->id;
	}
	
	public function getUserName() {
		return $this->username;
	}
	
	public function getFirstName() {
		return $this->firstName;
	}
	
	public function getLastName() {
		return $this->lastName;
	}
	
	public function getEMailAddress() {
		return $this->eMail;
	}
	
	public function getAddress() {
		return $this->address;
	}
	
	// SETTERS
	public function setFriendIDs(array $v) {
		$this->_mCollFriendIDs = $v;
	}
	
	public function setHtLocationID($v) {
		$this->htLocationID = $v;
	}
	
	public function setPhone($v) {
		$this->phone = $v;
	}
	
	public function setSignature($v) {
		$this->_mSignature = $v;
	}
	
	public function setSendableID($v) {
		$this->sendableID = $v;
	}
	
	public function setID($v) {
		$this->id = $v;
	}
	
	public function setUserName($v) {
		$this->username = $v;
	}
	
	public function setFirstName($v) {
		$this->firstName = $v;
	}
	
	public function setLastName($v) {
		$this->lastName = $v;
	}
	
	public function setEMailAddress($v) {
		$this->eMail = $v;
	}
	
	public function setAddress($v) {
		$this->address = $v;
	}
	
	// CONVENIENCE
	public function getName() {
		$name = $this->firstName . ' ' . $this->lastName;
		if( "" == trim($name) ){
			$name = $this->getUserName();
		}
		return $name;
	}
	
	public function getDisplayName() {
		return $this->getUserName();
	}
	
	public function getFriendlyURL() {
		return '/' . $this->getUserName();
	}
	
	public function getRealName() {
		return $this->getName();
	}
	
	public function getAdministeredGroups(){
		if( is_null($this->administeredGroups) ){
			$this->administeredGroups = gaGroupMapper::getAdministeredGroups($this);
		}
		return $this->administeredGroups;
	}
	
	public function getMainGroupsJoined(){
		if( is_null($this->mainGroupsJoined) ){
			$this->mainGroupsJoined = gaGroupMapper::getMainGroupsJoined($this);
		}
		return $this->mainGroupsJoined;
	}
	
	public function getCountTravelJournals(){
		return $this->countTravelJournals;
	}
	public function setCountTravelJournals($count){
		$this->countTravelJournals = $count;
	}
	public function getIsProfileCompleted(){
		return $this->isProfileCompleted;
	}
	public function setIsProfileCompleted($completed){
		$this->isProfileCompleted = $completed;
	}
}