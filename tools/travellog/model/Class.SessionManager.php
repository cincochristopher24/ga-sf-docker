<?php
if(!isset($_SESSION)){
session_start(); 
}
require_once 'travellog/model/Class.Session.php';
require_once 'travellog/model/Class.TravelerSessionManager.php';
require_once 'Class.Constants.php';
class SessionManager{
	
	protected static $instance = null; 
	
	static function getInstance(){
		if( self::$instance == null ) self::$instance = new SessionManager;
		return self::$instance; 
	}

	function set($name, $value){
		$_SESSION[$name] = $value; 	
	}	
	
	function get($name){
		if( array_key_exists($name, $_SESSION) ){
			//quick patch for kicking out unwanted users. 
			if( 'travelerid' ==  strtolower($name) && 0 != $_SESSION[$name] && constants::ADMIN_TRAVELER_ID != $_SESSION[$name] ){
				//$travelerSession = TravelerSession::getInstance($_SESSION[$name], $_SERVER["HTTP_HOST"]);
				if(!TravelerSessionManager::getInstance()->hasSession($_SESSION['travelerID'], $_SERVER["HTTP_HOST"])){
					unset($_SESSION['travelerID']);
					return 0;
				}
			}
			return $_SESSION[$name];
		}  
		return 0;
	}
	
	function unsetVar($name){
		if( array_key_exists($name, $_SESSION) ){ 
			unset($_SESSION[$name]);	
		}  
	}
	
	function getLogin(){
		$isLogin = false;

		//if( 0 == strcmp(crypt($this->get('travelerID'),CRYPT_BLOWFISH),$this->get("login")) && 0 == strcmp($_SERVER["HTTP_HOST"],$this->get("domain")) ){
		if( $this->get('travelerID') && (0 == strcmp($_SERVER["HTTP_HOST"], $this->get("domain"))) ){
			$session = session::getInstance($this->get('travelerID'));
			$session->generateSession();
			$isLogin = true;
		} 

		return $isLogin;
	}
	
	function isSiteAdmin(){
		$admin = false;
		
		//quick fix, replace function call to getLogin because of inconsistent php versions in prod servers
		// use compare of domain/http_host instead
		if( $this->get("siteAdminID") && 0 == strcmp($_SERVER["HTTP_HOST"],$this->get("domain")) ) $admin = true;
		return $admin;
	}
} 
?>