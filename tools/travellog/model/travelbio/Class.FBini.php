<?php
	require_once ('Class.ToolMan.php');
	require_once ('Class.dbHandler.php');
	require_once ('Class.iRecordset.php');

	class FBIni{
		public static function createDBHandler(){
			/***
			if('DEV'==$server || 'PROD' == $server){
				$host = ($server == 'DEV')?'192.168.3.9':'eth0.ga_database01';
				$db = new dbHandler('NONE');
				$db->setHost($host);
				$db->setUser('goabroad');
				$db->setPassword('g4pt4lip4p4');
				$db->connect();	
			}
			***/
			$db = new dbHandler();
			$db->setDB('SocialApplication');
			return $db;
		}
		
		public static function getApiKey(){
			return 'a2a4d53f9caee9c6e2d57b850b7ab8dc';//oldtravelbio
			//return '88ff42c1a72109296384a3c2dd4fde3a';//newtravelbio
		}

		public static function getApiSecret(){
			return '3cdf2b9b5590f130331684dde485a6ae';//oldtravelbio
			//return '53f09d80c98c7072ff9697e0695a3367';//newtravelbio
		}
		
		public static function getImagePath(){
			return 'http://images.goabroad.net/images/facebook/';
		}
		
		public static function getLogoPath(){
			return self::getImagePath().'mytravelbioicon.gif';
		}
		
		public static function getAnswerIconPath(){
			return self::getImagePath().'question.jpg';	
		}
			
		public static function getCallbackDirectory(){
			return 'http://www.goabroad.net/travelbio/';//oldtravelbio
			//return 'http://www.goabroad.net/travelbio2/';//newtravelbio			
		}
		
		public static function getFBRootDirectory(){
			//return 'http://apps.facebook.com/newtravelbio/';
			return 'http://apps.facebook.com/travelbio/';
		}
		
		public static function getAdminFbUserID(){
			return '507323382';
			//return '505580161';
		}
		
		public static function getCallbackHost(){
			return 'http://www.goabroad.net/';
		}
				 
		public static function getInfiniteSessionKey(){
			return 'dbc3fa48b3edf8acbc813260-507323382';//oldtravelbio
			//return 'a70d3ce2c4e1ea05cc9c5e4a-507323382';//newtravelbio						
		}
	}
?>