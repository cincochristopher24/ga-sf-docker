<?php
	class TravelBioDashBoard	
	{
		
		const EDIT_TAB 		= 0;
		const FRIENDS_TAB 	= 1;
		const SYNC_TAB 		= 2;
		const SETTINGS_TAB	= 3;
		const VIEW_TAB		= 4;		
		public static function generate($selected){
			ob_start();	
			?>
			<fb:tabs>
			    <fb:tab_item href="<?= FBini::getFBRootDirectory() ?>myTravelBio.php" title="My Travel Bio" selected="<?= self::VIEW_TAB == $selected ?"true":"false" ?>"/>
			    <fb:tab_item href="<?= FBini::getFBRootDirectory() ?>edit.php" title="Edit My Travel Bio" selected="<?= self::EDIT_TAB == $selected ?"true":"false" ?>"/>
				<fb:tab_item href="<?= FBini::getFBRootDirectory() ?>friends.php" title="Invite Friends" selected="<?= self::FRIENDS_TAB==$selected?"true":"false" ?>"/>
				<fb:tab_item href="<?= FBini::getFBRootDirectory() ?>fbsync.php" title="Synchronize" selected="<?= self ::SYNC_TAB==$selected?"true":"false" ?>"/>
				<fb:tab_item href="<?= FBini::getFBRootDirectory() ?>settings.php" title="Settings" align="right"  selected="<?= self::SETTINGS_TAB==$selected?"true":"false" ?>"  />
			</fb:tabs>
			<?php
			$contents = ob_get_contents();
			ob_end_clean();
			return $contents;
		}
	}
?>