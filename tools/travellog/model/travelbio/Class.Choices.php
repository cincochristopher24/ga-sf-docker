<?php
	/**
	 * Created on 03 July 2007  6:55 PM
	 * Class.Choices.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once('Class.FBini.php');

	class Choices{
		private $db			= null;
		private $qID			= 0;
		private $choice 		= '';
		private $choiceID 	= 0;
		private $exists		= false;

		function Choices($_choiceID = 0){
			try{
				$this-> db = FBIni::createDBHandler();
			}
			catch(exception $e){
				throw $e;
			}

			$this-> choiceID = $_choiceID;
			if (0 < $this->choiceID){
				$rs = new iRecordset($this->db->execQuery("select * from tbChoice where choiceID = " . ToolMan::makeSqlSafeString($this->choiceID)));				

				if (0 < $rs->retrieveRecordCount()){
					$this-> exists = true;
					$this-> setChoice ($rs->getChoice(0));
					$this-> setQuestionID ($rs->getProfileQuestionID(0));
				}
				else
					throw new exception('Invalid choiceID '.$_choiceID.' passed to object of type Choices.');
			}

			return $this;
		}

		// Setters
		function setChoice($_choice = ''){
			$this-> choice = $_choice;
		}

		function setQuestionID($_qID = 0){
			$this-> qID = $_qID;
		}

		// Getters
		function getChoiceID(){
			return $this-> choiceID;
		}

		function getChoice(){
			return $this-> choice;
		}

		function getQuestionID(){
			return $this-> qID;
		}

		// Boolean
		function isExist(){
			return $this-> exists;
		}

		// CRUD
		function Save(){
			if (!$this->isExist()){
				$sql = "insert into tbChoice (" .
							"profileQuestionID, choice" .
					  ") values (" .
					  		ToolMan::makeSqlSafeString($this->qID) . ", " .
					  		ToolMan::makeSqlSafeString($this->choice) .
					  ")";
				$this->db->execQuery($sql);
				
			}
			else
				$this-> Update();
		}
		
		protected function Update(){
			if (!$this->isExist()){
				$sql = "update tbChoice set profileQuestionid = " . ToolMan::makeSqlSafeString($this->qID) . ", " .
							"choice = " . ToolMan::makeSqlSafeString($this->choice) . 
					   " where choiceID = " . ToolMan::makeSqlSafeString($this->choiceID);
				$this->db->execQuery($sql);
			}		
		}

		function Delete(){
			if ($this->isExist()){
				$this->db->execQuery("delete from tbChoice where choiceID = " . $this->choiceID);
			}			
		}
		
		// Static
		static function getChoices($_qID = 0){
			try{
				$db = FBIni::createDBHandler();
			}
			catch(exception $e){
				throw $e;
			}
			$arrChoices = array();
			if (0 < $_qID){
				$rs = new iRecordset($db->execQuery("select choiceID from tbChoice where profileQuestionID = " . ToolMan::makeSqlSafeString($_qID) . " ORDER BY choiceID"));
				if($rs->retrieveRecordCount() > 0){
					for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){						
						$arrChoices[] = new Choices($rs->getChoiceID());
					}
				}
			}
			return $arrChoices;
		}
	}
?>