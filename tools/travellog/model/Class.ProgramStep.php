<?php

	/**
	*	Created by: ZarisDafni
	*	Oct 16, 2007
	*/

require_once("Class.Connection.php");
require_once("Class.Recordset.php");

	class ProgramStep {
		
		
		private $mConn     		 = null;
	    private $mRs       		 = null;
	
		private $mProgramStepID  = 0;
		private $mProgramStepInfo  = '';
		
		private static $conn = null;
		private static $rs = null;
		
		function __construct($_programStepID){
			try {
	            $this->mConn = new Connection();
                $this->mRs   = new Recordset($this->mConn);

				if (0 < $_programStepID):
					
					$sql = "SELECT * FROM tblProgramStep WHERE programstepID = " . $_programStepID;
					$this->mRs->Execute($sql);
					
					if ($this->mRs->Recordcount()) {
						$this->mProgramStepID = $this->mRs->Result(0,'programstepID');
						$this->mProgramStepInfo = $this->mRs->Result(0,'stepinfo');
					}
				endif;
            }
            catch (Exception $e) {                 
               throw $e;
            }
			
		}
		
		function getProgramStepID(){
			return $this->mProgramStepID;
		}
		
		function getProgramStepInfo(){
			return $this->mProgramStepInfo;
		}
		
		public static function initConn(){

			if (self::$conn == null):
				self::$conn = new Connection();
				self::$rs = new Recordset(self::$conn);
			endif;

		}
		public static function getAllProgramSteps(){

			$sql = "SELECT programstepID FROM tblProgramStep";
			self::$rs->Execute($sql);

			$arrProgramSteps = array();
			while ($recordset = mysql_fetch_array(self::$rs->Resultset())){
				$arrProgramSteps[] = new ProgramStep($recordset['programstepID']);
			}

			return $arrProgramSteps;


		}
		
	}
?>