<?php
/*
 * Created on Jul 31, 2006
 * Author: A subclass of Location and represents a place that is not officially identified as a region or city.
 * 			This might be a town, barangay or an out-of-town area
 * 			
 */
 
 require_once("gaexception/Class.InstantiationException.php");
 
 class NonPopLocation extends Location {
 	
 	private $nonPopLocationID;
 	private $regionID;
 	private $countryID;
 	private $continentID;
 	
  	private $region;
  	private $country;
  	private $continent;
 	
 	function setNonPopLocationID($nonPopLocationID) {$this->nonPopLocationID = $nonPopLocationID;}
 	function getNonPopLocationID() {return $this->nonPopLocationID;}
 	
 	function setRegionID($regionID) {$this->regionID = $regionID; }
 	
 	function setCountryID($countryID) {$this->countryID = $countryID;}
 	
 	function setContinentID($continentID) {$this->continentID = $continentID;}
 		
 		 	
 	
 	// overrides
 	
 	  	function getCountries() {return null;}	
 		function getCities() { return null; } 			 			
 		function getRegions() { return null; } 		
 		function getNonPopLocations() {return null; }
 		 	
 		function getRegion() {
 			if ($this->region == null) {
 				$rs = mysql_query("select * from tblRegion where regionID = '" . $this->regionID . "'" );
 				
 				if (!$rs)
 					return null;
 					
 				$rsar = mysql_fetch_array($rs);
 					 				
 				$this->region = new Region();
 				$this->region->setLocationID($rsar['locationID']);
 				$this->region->setName($rsar['name']);
 				$this->region->setRegionID($rsar['regionID']);
 				$this->region->setCountryID($rsar['countryID']);
 				$this->region->setContinentID($rsar['continentID']); 				 				
 			}
 			return $this->region; 	
 		}
 		function getCountry() {
 			if ($this->country == null) {
 				$rs = mysql_query("select * from GoAbroad_Main.tbcountry where countryID = '" . $this->countryID . "'");
 				
 				$rsar = mysql_fetch_array($rs);
 				
 				$this->country = new Country();
 				$this->setLocationID($rsar2['locationID']);
 				$this->setContinentID($rsar2['continentID']);
 				$this->setCountryID($rsar2['countryID']);
 				$this->setName($rsar2['country']); 					
 			} 	
 			return $this->country;		
 		}
 		function getContinent() {
 			if ($this->continent == null) {
	 			$rs = mysql_query("select * from tblContinent where continentID='" . $this->continentID . "'");
	 			$rsar = mysql_fetch_array($rs);
	 			
	 			//print_r($rsar);
	 			//exit();
	 			
	 			$continent = new Continent();
	 			$continent->setLocationID($rsar['locationID']);
	 			$continent->setName($rsar['name']);
	 			$continent->setContinentID($rsar['continentID']);
 			}	
 			return $continent;
 			
 		}	
 	
 }
 
?>
