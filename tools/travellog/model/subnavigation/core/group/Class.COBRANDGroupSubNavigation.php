<?php
	//hide subnav if viewer is not advisor
	//if the someone is viewing a subgroup, make the subgroup subnav the main nave.
	
	require_once('travellog/model/subnavigation/ABSTRACT/Class.AbstractGroupSubNavigation.php');
	class COBRANDGroupSubNavigation extends AbstractGroupSubNavigation
	{
		public function __construct($vars=array()){
			$this->mClasses['TRAVELER']['file'] = 'travellog/custom/COBRAND/model/Class.TravelerCB.php';
			$this->mClasses['TRAVELER']['className'] = 'TravelerCB';
			parent::__construct($vars);
		}
		
		protected function isInCobrand(){
			return true;
		}
		
		protected function createParentGroupNameLink(){
			return null;
		}
		
		/***
		protected function createGroupNameLink($group,$privacyPref,$options=array()){
			//$this->alterLinkUrl('GROUP_NAME','/');
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus || $group->isSubGroup()){
				return parent::createGroupNameLink($group,$privacyPref,$options);
			}
			return null;
		}
		
		protected function createMembersLink($group,$privacyPref,$options=array()){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus || $group->isSubGroup()){
				//return parent::createMembersLink();
				//$grp = (!is_null($this->mParentGroup) ? $this->mParentGroup : $this->mSubNavGroup);
				//return parent::createMembersLink($grp,$this->mPrivacyPref,array('IS_HIGHLIGHTED'=>('MEMBERS'==$this->mLinkToHighlight && !$this->mSubNavGroup->isSubGroup())));
				return parent::createMembersLink($group, $privacyPref, $options);
			}
			return null;
		}
		
		protected function createSubGroupsLink($group,$privacyPref,$options=array()){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus){
				$this->mAlteredLinks['SUBGROUPS'] = 'Groups';
				$isGroupLinkHighlighed = (!is_null($this->mCustomGroupSection) && $this->mSubNavGroup->isSubGroup() && !$this->mCustomGroupSection->isSubGroupInSection($this->mSubNavGroup->getGroupID())) || 'SUBGROUPS'==$this->mLinkToHighlight;
				return parent::createSubGroupsLink($group,$privacyPref,$options);
				//return parent::createSubGroupsLink();
			}
			return null;
		}
		***/
		
		/***
		protected function createCustomGroupsLink($group,$privacyPref,$options=array()){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus){
				return parent::createCustomGroupsLink($group,$privacyPref,$options);
			}
			return null;
		}
		***/
		/***
		protected function createJournalsLink($group,$privacyPref,$options=array()){
			//if(AbstractGroupSubNavigation::NOT_MEMBER == $this->mTravelerStatus){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus || $group->isSubGroup()){
				//$this->mAlteredUrls['MY_JOURNALS'] = '/journal.php';
				return parent::createJournalsLink($group,$privacyPref,$options);
			}
			//return parent::createJournalsLink();
			return null;
		}
		
		
		protected function createPhotoLink($group,$privacyPref,$options=array()){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus){
				return parent::createPhotoLink($group,$privacyPref,$options);
			}
			return null;
		}
		
		protected function createEventsLink($group,$privacyPref,$options=array()){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus  || $group->isSubGroup()){
				return parent::createEventsLink($group,$privacyPref,$options);
			}
			return null;
		}
		
		protected function createMessageCenterLink($group,$privacyPref,$options=array()){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus  || $group->isSubGroup()){
				return parent::createMessageCenterLink($group,$privacyPref,$options);
			}
			return null;
		}

		protected function createResourceFilesLink($group,$privacyPref,$options=array()){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus || $group->isSubGroup()){
				return parent::createResourceFilesLink($group,$privacyPref,$options);
			}
			return null;
		}
		***/
		/*protected function createDiscussionBoardLink($group,$privacyPref,$options=array()){
			if(AbstractGroupSubNavigation::ADMINISTRATOR == $this->mTravelerStatus){
				return parent::createDiscussionBoardLink($group,$privacyPref,$options);
			}
			return null;
		}*/
	}
?>