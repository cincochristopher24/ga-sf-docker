<?php
	require_once('travellog/model/subnavigation/ABSTRACT/Class.AbstractTravelerSubNavigation.php');
	
	class COBRANDTravelerSubNavigation extends AbstractTravelerSubNavigation
	{
		protected $mCobrandGroupID = 0;
		public function __construct($vars=array()){
			$this->mClasses['TRAVELER']['file'] = 'travellog/custom/COBRAND/model/Class.TravelerCB.php';
			$this->mClasses['TRAVELER']['className'] = 'TravelerCB';
			$this->mCobrandGroupID = $vars['cobrandGroupID'];
			parent::__construct($vars);
		}
		
		protected function createOnlineAdvisingLink(){
			return null;
		}
		
		protected function createResumeLink(){
			return null;
		}
		
		protected function createParticipantInquiriesLink(){
			return null;
		}
		
		protected function createGroupsLink(){
			if($this->mViewedTraveler->getPrivacyPreference()->canViewGroups($this->mLoggedTraveler->getTravelerID()) || $this->mIsOwn){
				$linkConst = 'MY_GROUPS';
				$tempUrl = array_key_exists($linkConst,$this->mAlteredUrls)
					?$this->mAlteredUrls[$linkConst]
					:($this->mIsOwn ? "/group.php?mode=mygroups" : "/group.php?travelerID=".$this->mContextID);
				$tempIsHighlighted = ($linkConst==$this->mLinkToHighlight )?true:false;
				$tempIsHidden = (in_array($linkConst,$this->mHiddenLinks))?true:false;
				$tempLinkCaption = (array_key_exists($linkConst,$this->mAlteredLinks))?$this->mAlteredLinks[$linkConst]:$this->mLinkLabels[$linkConst];
				$groups = $this->mViewedTraveler->getGroups();
				if(is_null($groups)){
					$groups = array();
				}
				if(count($groups)){
					for($i = 0; $i < count($groups);$i++ ){
						if ($groups[$i] != null  && $groups[$i]->getGroupID() == $this->mCobrandGroupID ){
							unset($groups[$i]);					// remove the parent group whos id is the same as cobrands group id
							break;	
						}
					}		
				}
				if(!$this->mIsOwn && 5 >= count($groups) ){
					return null;
				}
				return $this->createNewLink($tempLinkCaption,$tempUrl,$tempIsHighlighted,$tempIsHidden);
			}
			return null;
		}	
	}
?>