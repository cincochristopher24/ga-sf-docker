<?php
require_once("Class.Connection.php");
require_once("Class.Recordset.php");
class GroupApprovedJournals{
	private 
	
	$mConn    = NULL,
	
	$mRs      = NULL,
	
	$mTravelID = 0,
	
	$mApproved = 0,
	
	$mGroupID  = 0;
	
	function __construct($travelID=0){
		$this->mConn = new Connection;
		$this->mRs   = new Recordset($this->mConn);
		if( $travelID ){ 
			$this->mTravelID = $travelID;
			$this->Get();	
		}	
	}
	
	function Save(){
		$sql = sprintf
				(
					'INSERT INTO tblGroupApprovedJournals (travelID, groupID, approved) VALUES (%d, %d, %d)'
					,$this->mTravelID
					,$this->mGroupID
					,$this->mApproved
				);
		$this->mRs->Execute($sql);
	}
	
	function Get(){
		$sql = sprintf
				(
					'SELECT gaj.* FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.travelID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 LIMIT 1'
					,$this->mTravelID
				);
		$this->mRs->Execute($sql);
		if( $this->mRs->Recordcount() ){
			$this->mTravelID = $this->mRs->Result(0, 'travelID');
			$this->mGroupID  = $this->mRs->Result(0, 'groupID' );
			$this->mApproved = $this->mRs->Result(0, 'approved');
		}
	}
	
	function GetFiltered(){
		$sql = sprintf
				(
					'SELECT gaj.* FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.travelID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 AND gaj.groupID = %d LIMIT 1' 
					,$this->mTravelID
					,$this->mGroupID 
				);
		
		$this->mRs->Execute($sql);
		if( $this->mRs->Recordcount() ){
			$this->mTravelID = $this->mRs->Result(0, 'travelID');
			$this->mGroupID  = $this->mRs->Result(0, 'groupID' );
			$this->mApproved = $this->mRs->Result(0, 'approved');
		}
	}
	
	function Update(){
		if( $this->mTravelID && $this->mGroupID ){
			$sql = sprintf
					(
						'UPDATE tblGroupApprovedJournals SET approved = %d WHERE travelID = %d AND groupID = %d'  
						,$this->mApproved
						,$this->mTravelID
						,$this->mGroupID 
					);
	
			$this->mRs->Execute($sql);
			
			// added by K. Gordo, unfeature a journal if it is unapproved
			if ($this->mApproved == 0){
				require_once('travellog/model/Class.Featured.php');
				Featured::unsetFeaturedTravelInGroup($this->mGroupID,$this->mTravelID);
			}
			
		}
		else
			throw new UpdateGroupApprovedJournalsException('TravelID and GroupID must not be zero!');
	}
	
	static function isExistsJournal($travelID, $groupID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.travelID FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.travelID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 AND gaj.groupID = %d'  
					,$travelID
					,$groupID       
				);
		$mRs->Execute($sql);
		return ( $mRs->Recordcount() )? true : false;
	}
	
	static function isApproved($travelID, $groupID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.approved FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.travelID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 AND gaj.groupID = %d'  
					,$travelID
					,$groupID       
				);
		$mRs->Execute($sql);
		return $mRs->Result(0, 'approved');  
	}
	
	/**
	 * @param numeric groupID the groupID where the approved journals belong
	 * @return array of travel IDs
	 */
	static function getApprovedTravelIDsInGroup($groupID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.travelID FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.groupID = %d AND gaj.approved = 1 AND gaj.travelID = t.travelID AND t.deleted = 0'  
					,$groupID       
				);
		$mRs->Execute($sql);
		$jIDs = array();
		if ($mRs->Recordcount() > 0){
			while ($travelIDAr = mysql_fetch_array($mRs->Resultset())){
				$jIDs[$travelIDAr['travelID']] = $travelIDAr['travelID']; 	
			}			
			
		}
		return $jIDs;  
	}
	
	static function getUnapprovedTravelIDsInGroup($groupID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.travelID FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.groupID = %d AND gaj.approved = 0 AND gaj.travelID = t.travelID AND t.deleted = 0'  
					,$groupID       
				);
		$mRs->Execute($sql);
		$jIDs = array();
		if ($mRs->Recordcount() > 0){
			while ($travelIDAr = mysql_fetch_array($mRs->Resultset())){
				$jIDs[$travelIDAr['travelID']] = $travelIDAr['travelID']; 	
			}			
			
		}
		return $jIDs;
	}
	
	/**
	 * retrieves all travel ids related to a group whether approved or unapproved
	 * @param numeric groupID the groupID where the approved journals belong
	 * @return array of travel IDs
	 */
	static function getAllTravelIDsInGroup($groupID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.travelID FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.groupID = %d AND gaj.travelID = t.travelID AND t.deleted = 0'  
					,$groupID       
				);
		$mRs->Execute($sql);
		$jIDs = array();
		if ($mRs->Recordcount() > 0){
			while ($travelIDAr = mysql_fetch_array($mRs->Resultset())){
				$jIDs[$travelIDAr['travelID']] = $travelIDAr['travelID']; 	
			}			
			
		}
		return $jIDs;  
	}
	
	
	static function hasMoreThanOneJournal($groupID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.travelID FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.groupID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 LIMIT 2' 
					,$groupID 
				);

		$mRs->Execute($sql);
		return $mRs->Recordcount() > 1 ? true : false;  
	} 
	
	static function getRelatedGroup($travelID){
		require_once('travellog/model/Class.GroupFactory.php');
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.groupID FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.travelID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 LIMIT 1' 
					,$travelID

				);
		$mRs->Execute($sql);
		
		if( $mRs->Recordcount() ){
			$obj_group	= GroupFactory::instance()->create( array($mRs->Result(0, 'groupID')) );

			return $obj_group[0];
		}
		else return false;
		  
	}
	
	static function getRelatedGroups($travelID){
		require_once('travellog/model/Class.GroupFactory.php');
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.groupID, gaj.travelID FROM tblGroupApprovedJournals as gaj, tblTravel as t, tblGroup as g WHERE gaj.travelID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 AND gaj.approved = 1 AND gaj.groupID = g.groupID AND g.isSuspended = 0' 
					,$travelID

				);
		$mRs->Execute($sql);
		
		$groups = array();
		if ($mRs->Recordcount() > 0){
			while ($row = mysql_fetch_array($mRs->Resultset())){
				$obj_group	= GroupFactory::instance()->create( array($row['groupID']) );
				if(count($obj_group)){
					$rGroup = $obj_group[0];
					if (!$rGroup->isParent()){
						$rGroup = $rGroup->getParent();						
					}	
				
					$groups[$rGroup->getGroupID()] = $rGroup;
				}
			}
		}
		
		return $groups;		  
	}
	
	static function getApprovedTravelPhotosInGroup($groupID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.travelID,gaj.isphotorelate FROM tblGroupApprovedJournals as gaj, tblTravel as t WHERE gaj.groupID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 AND gaj.approved = 1'  
					,$groupID       
				);
		$mRs->Execute($sql);
		$jIDs = array();
		if ($mRs->Recordcount() > 0){
			while ($travelIDAr = mysql_fetch_array($mRs->Resultset())){
				$jIDs[] = array('travelID'=>$travelIDAr['travelID'],'isphotorelate'=>$travelIDAr['isphotorelate']); 	
			}			
			
		}
		return $jIDs;  
	}
	
	static function unapproveJournalInSubgroups($travelID,$parentID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		
		$sql = "UPDATE tblGroupApprovedJournals 
				SET approved = 0 
				WHERE travelID = $travelID 
					AND groupID IN (SELECT groupID FROM tblGroup WHERE parentID = $parentID)";
		$mRs->Execute($sql);
	}
	
	//function to remove group-related journals of a traveler
	//staff journals of this traveler in that group aren't included
	static function removeGroupRelatedJournalsOfTraveler($travelerID,$groupID){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		
		$sql = "DELETE FROM tblGroupApprovedJournals
				WHERE tblGroupApprovedJournals.groupID = {$groupID}
				 	AND tblGroupApprovedJournals.travelID IN (
						SELECT tblTravel.travelID FROM tblTravel,tblTravelLink
						WHERE tblTravel.travelerID = {$travelerID}
							AND tblTravel.travellinkID = tblTravelLink.travellinkID
							AND tblTravelLink.reftype = 1
							AND tblTravelLink.refID = {$travelerID});";
		$mRs->Execute($sql);
		
		$mConn2 = new Connection;
		$mRs2   = new Recordset($mConn2);
		
		$sql2 = "DELETE FROM tblGroupFeaturedJournals
				WHERE tblGroupFeaturedJournals.groupID = {$groupID}
					AND tblGroupFeaturedJournals.travelID IN (
						SELECT tblTravel.travelID FROM tblTravel,tblTravelLink
						WHERE tblTravel.travelerID = {$travelerID}
							AND tblTravel.travellinkID = tblTravelLink.travellinkID
							AND tblTravelLink.reftype = 1
							AND tblTravelLink.refID = {$travelerID});";
		$mRs2->Execute($sql2);
	}
	
	/**
	* retrieves all related groups including subgroups
	* use instead of getRelatedGroups where only parent groups are returned
	*/
	static function getGroupsWhereTravelIsApproved($travelID){
		require_once('travellog/model/Class.GroupFactory.php');
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		$sql = sprintf
				(
					'SELECT gaj.groupID, gaj.travelID FROM tblGroupApprovedJournals as gaj, tblTravel as t, tblGroup as g WHERE gaj.travelID = %d AND gaj.travelID = t.travelID AND t.deleted = 0 AND gaj.approved = 1 AND gaj.groupID = g.groupID AND g.isSuspended = 0' 
					,$travelID

				);
		$mRs->Execute($sql);
		
		$groups = array();
		if ($mRs->Recordcount() > 0){
			while ($row = mysql_fetch_array($mRs->Resultset())){
				$obj_group	= GroupFactory::instance()->create( array($row['groupID']) );
				if(count($obj_group)){
					$rGroup = $obj_group[0];
					$groups[$rGroup->getGroupID()] = $rGroup;
				}
			}
		}
		
		return $groups;		  
	}
	
	static function countGroupRelatedEntries($groupID=0){
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		
		$sql = "SELECT tblTravelLog.travellogID
				FROM tblGroupApprovedJournals, tblTravelLog
				WHERE tblGroupApprovedJournals.groupID = {$groupID}
					AND tblGroupApprovedJournals.approved = 1 
					AND tblGroupApprovedJournals.travelID = tblTravelLog.travelID
					AND tblTravelLog.deleted = 0
				GROUP BY tblTravelLog.travellogID";
		
		$mRs->Execute($sql);
		
		return $mRs->Recordcount();
	}
	
	function setTravelID($travelID){
		$this->mTravelID = $travelID;
	}
	
	function setApproved($approved){
		$this->mApproved = $approved;
	}
	
	function setGroupID($groupID){
		$this->mGroupID = $groupID;
	}
	
	function getApproved(){
		return $this->mApproved; 
	}
}

class GroupApprovedJournalsException extends Exception {}
class UpdateGroupApprovedJournalsException extends GroupApprovedJournalsException {}
?>
