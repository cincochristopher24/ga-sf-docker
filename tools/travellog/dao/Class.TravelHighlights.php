<?php
/**
* <b>TravelHighlights</b> class with integrated CRUD methods.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2007-2008
*/

require_once("Class.Connection.php");
require_once("Class.Recordset.php");
class TravelHighlights{
	
	private 
	
	$mConn = NULL,
	
	$mRs   = NULL;
	
	function __construct(){
		$this->mConn = new Connection;
		$this->mRs   = new Recordset($this->mConn);	
	}
		
	function Get( TravelHighlightsVO &$obj_travel_highlight ){
		$sql = sprintf(
						"SELECT travellogID, description, photoID FROM tblTravelHighlights WHERE travelHighlightID = %d"
						,$obj_travel_highlight->getTravelHighlightID()
					  );
		$this->mRs->Execute($sql);
		if( $this->mRs->Recordcount() ){
			$obj_travel_highlight->setTravelLogID( $this->mRs->Result(0, "travellogID") );
			$obj_travel_highlight->setDescription( $this->mRs->Result(0, "description") );
			$obj_travel_highlight->setPhotoID    ( $this->mRs->Result(0, "photoID")     );
		} 
	}
	 
	/*function GetList( Criteria2 $c ){
		$arr = array();
		$sql = sprintf(
 					    "SELECT travelHighlightID, travellogID, description, photoID FROM tblTravelHighlights WHERE 1 = 1 %s"
 					    ,$c->getCriteria2('AND')
					  );
		
		$this->mRs->Execute($sql);
		
		if( $this->mRs->Recordcount() ){
			require_once("travellog/vo/Class.TravelHighlightsVO.php");
			while( $row = mysql_fetch_assoc( $this->mRs->Resultset() ) ){
				$objNewMember = new	TravelHighlightsVO;
				$objNewMember->setTravelLogID      ( $row["travellogID"]       );	
				$objNewMember->setDescription      ( $row["description"]       );
				$objNewMember->setTravelHighlightID( $row["travelHighlightID"] );
				$objNewMember->setPhotoID          ( $row["photoID"]           ); 
				$arr[] = $objNewMember; 
			}
		}
		return $arr;
	}*/
	
	function GetList( Criteria2 $c ){
		$sql = sprintf(
 					    "SELECT travelHighlightID, travellogID, description, photoID FROM tblTravelHighlights WHERE 1 = 1 %s"
 					    ,$c->getCriteria2('AND')
					  );
		
		$this->mRs->Execute($sql);
		
		return ( $this->mRs->Recordcount() )? $this->mRs->Resultset(): NULL; 
	}
	
	function Save( TravelHighlightsVO &$obj_travel_highlight ){
		$sql = sprintf(
 					    "INSERT INTO tblTravelHighlights (travellogID, description, photoID) VALUES (%d, '%s', %d)"
 					    ,$obj_travel_highlight->getTravelLogID()
 					    ,addslashes($obj_travel_highlight->getDescription())
 					    ,$obj_travel_highlight->getPhotoID()
					  );
		$this->mRs->Execute($sql); 
	}
	
	function Update( TravelHighlightsVO &$obj_travel_highlight ){
		$sql = sprintf(
 					    "UPDATE tblTravelHighlights SET description = '%s', photoID = %d WHERE travelHighlightID = %d"
 					    ,addslashes($obj_travel_highlight->getDescription())
 					    ,$obj_travel_highlight->getPhotoID()
 					    ,$obj_travel_highlight->getTravelHighlightID() 
					  );
		$this->mRs->Execute($sql);
	}
	
	function Delete( Criteria2 $c ){
		if( get_class($c) == 'Criteria2' ){ 
			$sql = sprintf(
	 					    "DELETE FROM tblTravelHighlights WHERE %s"
	 					    ,$c->getCriteria2()
						  );
			$this->mRs->Execute($sql);
		}
		else
			throw new DeleteTravelHighlightsException('Invalid criteria object!');	
	}   
}

class TravelHighlightsException extends Exception {}
class DeleteTravelHighlightsException extends TravelHighlightsException {}
class UpdateTravelHighlightsException extends TravelHighlightsException {}

?>

