<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyCenterController.php');
class DSESurveyCenterController extends COBRANDSurveyCenterController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/DSE/views'));
		$this->file_factory->setPath('CSS', '/custom/DSE/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/DSE/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>