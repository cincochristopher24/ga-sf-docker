<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDActivityController.php');
class UCDENVERActivityController extends COBRANDActivityController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/UCDENVER/views'));
		$this->file_factory->setPath('CSS', '/custom/UCDENVER/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/UCDENVER/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>