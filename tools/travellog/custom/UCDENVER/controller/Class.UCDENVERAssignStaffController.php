<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class UCDENVERAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/UCDENVER/views'));
			$this->mFactory->setPath('CSS', '/custom/UCDENVER/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/UCDENVER/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>