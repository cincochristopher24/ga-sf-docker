<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
class UCDENVERPagesController extends COBRANDPagesController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/UCDENVER/views'));
		$this->file_factory->registerTemplate('PrivacyPolicy',array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsDefault', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsAdvisor', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/UCDENVER/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/UCDENVER/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>