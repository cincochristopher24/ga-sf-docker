<?php

	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionBoardHomeController.php');

	class QUINNIPIACDiscussionBoardHomeController extends COBRANDDiscussionBoardHomeController {

		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/QUINNIPIAC/views/'));
			$this->file_factory->setPath('CSS', '/custom/QUINNIPIAC/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/QUINNIPIAC/views/');
		}
	}
?>