<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	QUINNIPIACRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for QUINNIPIAC	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/QUINNIPIAC/views'));
		$file_factory->setPath('CSS', '/custom/QUINNIPIAC/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/QUINNIPIAC/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}