<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class QUINNIPIACAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/QUINNIPIAC/views'));
			$this->mFactory->setPath('CSS', '/custom/QUINNIPIAC/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/QUINNIPIAC/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>