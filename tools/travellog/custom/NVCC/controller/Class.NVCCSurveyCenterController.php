<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyCenterController.php');
class NVCCSurveyCenterController extends COBRANDSurveyCenterController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/NVCC/views'));
		$this->file_factory->setPath('CSS', '/custom/NVCC/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/NVCC/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>