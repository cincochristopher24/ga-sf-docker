<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class NVCCAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/NVCC/views'));
			$this->mFactory->setPath('CSS', '/custom/NVCC/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/NVCC/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>