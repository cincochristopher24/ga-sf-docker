<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTopicController.php');

	class NVCCTopicController extends COBRANDTopicController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/NVCC/views/'));
			$file_factory->setPath('CSS', '/custom/NVCC/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/NVCC/views/');
		}
	}