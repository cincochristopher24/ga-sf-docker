<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionController.php');

	class ISVDiscussionController extends COBRANDDiscussionController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ISV/views/'));
			$file_factory->setPath('CSS', '/custom/ISV/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISV/views/');
		}
	}