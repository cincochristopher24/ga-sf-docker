<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class ISVSubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new ISV subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ISV/views"));
			$this->file_factory->setPath("CSS", "/custom/ISV/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ISV/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}