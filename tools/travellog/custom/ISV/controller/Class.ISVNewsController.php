<?php
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class ISVNewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISV/views'));
			$this->file_factory->setPath('CSS', '/custom/ISV/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISV/views/');
		}
	 	
	 }