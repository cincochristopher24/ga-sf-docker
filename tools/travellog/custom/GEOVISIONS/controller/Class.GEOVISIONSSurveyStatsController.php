<?php 
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyStatsController.php');
	
 	class GEOVISIONSSurveyStatsController extends COBRANDSurveyStatsController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GEOVISIONS/views'));
			$this->file_factory->setPath('CSS', '/custom/GEOVISIONS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GEOVISIONS/views/');
 		}
 	}