<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class GEOVISIONSSubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new GEOVISIONS subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/GEOVISIONS/views"));
			$this->file_factory->setPath("CSS", "/custom/GEOVISIONS/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/GEOVISIONS/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}