<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');
	
	class GEOVISIONSAssignStaffController extends COBRANDAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GEOVISIONS/views'));
			$this->mFactory->setPath('CSS', '/custom/GEOVISIONS/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/GEOVISIONS/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}