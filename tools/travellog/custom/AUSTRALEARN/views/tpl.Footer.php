<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	<div id="footer_inner_wrap">
	
		<div id="footer_links">
			<div>
			<ul>
				<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>">AustraLearn</a></li>
				<?= NavigationHelper::displayGlobalNavigation() ?>
			</ul>

			<ul>
				<li class="first"><a href="/passport.php">My Passport</a></li>
				<li><a href="/travelers.php">Students</a></li>
				<li><a href="/travel.php">Journals</a></li>
				<li><a href="/subgroups.php">Groups</a></li>
				<li class="last">
					<?= HelperGlobal::displayLoginLink(); ?>
				</li>
			</ul>
			<p id="footlet">Copyright &copy; <? echo date("Y"); ?> My AustraLearn and GoAbroad.net&reg;</p>
			</div>
		</div>
	</div>
</div>
	
