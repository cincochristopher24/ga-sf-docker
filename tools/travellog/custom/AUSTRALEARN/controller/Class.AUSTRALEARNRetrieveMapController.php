<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	AUSTRALEARNRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for AUSTRALEARN	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/AUSTRALEARN/views'));
		$file_factory->setPath('CSS', '/custom/AUSTRALEARN/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AUSTRALEARN/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}