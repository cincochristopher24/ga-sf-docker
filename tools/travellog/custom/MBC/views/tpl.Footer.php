<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	<div id="footer_inner_wrap">
	
		<div id="footer_links">
			<div>
			<ul>
				<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>"> MBC </a></li>
				<?= NavigationHelper::displayGlobalNavigation() ?>
			</ul>


			<ul id="footer_global_navigation">
				<?= NavigationHelper::displayMainNavigation($page_location); ?>
				<li class="last">
					<?= HelperGlobal::displayLoginLink(); ?>
				</li>
			</ul>
			<p id="footlet">Copyright &copy; <? echo date("Y"); ?> Mary Baldwin College and GoAbroad.net&reg;</p>
			</div>
		</div>
	</div>
</div>
	
