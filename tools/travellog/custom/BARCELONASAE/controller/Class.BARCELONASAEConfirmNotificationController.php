<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDConfirmNotificationController.php');
	
 	class BARCELONASAEConfirmNotificationController extends COBRANDConfirmNotificationController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/BARCELONASAE/views'));
			$this->file_factory->setPath('CSS', '/custom/BARCELONASAE/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/BARCELONASAE/views/');
 		}
 	}