<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelPlansController.php');

	class BARCELONASAETravelPlansController extends COBRANDTravelPlansController{
		
		function __construct(){
			parent::__construct();
			
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/BARCELONASAE/views'));
			$this->file_factory->setPath('CSS', '/custom/BARCELONASAE/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/BARCELONASAE/views/');   
		}
		
		function performAction(){ 
			parent::performAction();
		}
		
	}