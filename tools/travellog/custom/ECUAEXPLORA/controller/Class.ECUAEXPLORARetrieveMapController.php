<?php

require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');
	
class ECUAEXPLORARetrieveMapController extends COBRANDRetrieveMapController {

	/**
	 * Initializes controller for ECUAEXPLORA */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ECUAEXPLORA/views'));
		$file_factory->setPath('CSS', '/custom/ECUAEXPLORA/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ECUAEXPLORA/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}

}