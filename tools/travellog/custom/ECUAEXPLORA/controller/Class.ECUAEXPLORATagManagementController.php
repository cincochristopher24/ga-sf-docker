<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTagManagementController.php');
	
 	class ECUAEXPLORATagManagementController extends COBRANDTagManagementController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ECUAEXPLORA/views'));
			$this->file_factory->setPath('CSS', '/custom/ECUAEXPLORA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ECUAEXPLORA/views/');
 		}
 		
 		function performAction(){ 
			parent::performAction();
		}
 	}