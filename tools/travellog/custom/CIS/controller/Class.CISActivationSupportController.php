<?php
	/*
	 * Class.CISActivationSupportController.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDActivationSupportController.php");
	
	class CISActivationSupportController extends COBRANDActivationSupportController{
		
		function __construct(){
			
			parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views'));
			$this->file_factory->setPath('CSS', '/custom/CIS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/');
				
		}
		
	}  
?>
