<?php
/*
	Filename:		Class.CISTravelMapsController.php
	Author:			Jonas Tandinco
	Date Created:	Jan/03/2008
	Putpose:		controller implementation for CIS co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');

class CISTravelMapsController extends COBRANDTravelMapsController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views'));
		$this->file_factory->setPath('CSS', '/custom/CIS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/');   
	}
	
	public function performAction(){ 
		parent::performAction();
	}
}
?>
