<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 26 November 2007
	 * Class.CISCommentRequestController.php
	 */

	require_once 'travellog/custom/COBRAND/controller/Class.COBRANDCommentRequestController.php';

	 class CISCommentRequestController extends COBRANDCommentRequestController {

	 	function __construct(){

			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views'));
			$this->file_factory->setPath('CSS', '/custom/CIS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/');
		}

	 	function performAction() {
	 		parent::performAction();
	 	}
	 }
?>