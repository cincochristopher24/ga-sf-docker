<?php
	/*
	 * Class.AIFSLoginController.php
	 * Created on Nov 23, 2007
	 * created by marc
	 */
 
 	require_once("travellog/custom/COBRAND/controller/Class.COBRANDLoginController.php");
 	
 	class GLOBALSCHOLARLoginController extends COBRANDLoginController{
 		
 		function __construct(){
 			
 			parent::__construct();
 			
 			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GLOBALSCHOLAR/views'));
			$this->file_factory->setPath('CSS', '/custom/GLOBALSCHOLAR/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSCHOLAR/views/');
 		}
 		
 	}
?>
