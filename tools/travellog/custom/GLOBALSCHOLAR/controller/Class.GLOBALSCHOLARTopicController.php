<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTopicController.php');

	class GLOBALSCHOLARTopicController extends COBRANDTopicController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/GLOBALSCHOLAR/views/'));
			$file_factory->setPath('CSS', '/custom/GLOBALSCHOLAR/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSCHOLAR/views/');
		}
	}