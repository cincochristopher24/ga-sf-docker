<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDWidgetController.php');
class GLOBALSCHOLARWidgetController extends COBRANDWidgetController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GLOBALSCHOLAR/views'));
		$this->file_factory->setPath('CSS', '/custom/GLOBALSCHOLAR/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSCHOLAR/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>