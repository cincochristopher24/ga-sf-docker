<?php

	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
	Template::setMainTemplateVar('layoutID', 'admin_group');
	Template::setMainTemplateVar('page_location', 'Home');


$code = <<<BOF
<script type="text/javascript"> 

function toggleBox(szDivID, iState) // 1 visible, 0 hidden 
{ 
	if(document.layers) //NN4+ 
	{ 
	document.layers[szDivID].visibility = iState? "show" : "hide"; 
	} 
	else if(document.getElementById) //gecko(NN6) + IE 5+ 
	{ 
	var obj = document.getElementById(szDivID); 
	obj.style.visibility = iState? "visible" : "hidden"; 
	} 
	else if(document.all)// IE 4 
	{ 
	document.all[szDivID].style.visibility = iState? "visible" : "hidden"; 
	} 
} 
 
</script>
BOF;

$code2 = <<<BOF
<script type="text/javascript">
var manager = new homeStayManager();
tool.AddEvent(window,'load',function(){	manager._useLoadingMessage(); 
});

function toggleAction(viewtype, param){

	if (viewtype==1){
		document.getElementById('divActionCmbStudent').innerHTML = 
				'<a href="javascript:void(0)" onClick="manager._saveCmbStudent(\''+param+'\');toggleAction(2,\''+param+'\');">Save Changes</a>';
		
	}
	else {
		document.getElementById('divActionCmbStudent').innerHTML = 
				'<a href="javascript:void(0)" onClick="manager._viewCmbStudent(\''+param+'\')">Change</a>';
	}
	
}
</script>		 
BOF;

Template::includeDependentJs('/min/f=js/prototype.js');
Template::includeDependentJs('/js/moo1.ajax.js');
Template::includeDependentJs('/js/homeStayManager.js');
Template::includeDependentJs('/js/tools/tools.js');
Template::includeDependent($code2);

?>
<div align="right"><a href="homestay.php?mode=add">Add new HomeStay</a></div>
	<div class="pagination">
		<? if ($paging->getTotalPages() > 1 ): ?>		
			<div class="page_markers">
				<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec ?>
			</div>	
			<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
		<? endif; ?>	
	</div>

	<table width="560" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#D8E0EC" bgcolor="#E8EBF4" class="Arial12px" >
	 


	
<?  $iterator->rewind();

	while($iterator->valid()):
		$homestay = $iterator->current();	
		$user = null;
		if (null != $homestay->getAssignedTraveler())
			$user = $homestay->getAssignedTraveler();
?>	
	<tr>
	    <td align="left"><?= $homestay->getName() ?></td>
	</tr>
	<tr>
	    <td align="left">
			<div id="divCmbStudent" class="divCmbStudent"> 
				<? if (null != $user) : ?>
					Assigned to : <a href="/<?=$user->getUserName()?>"><?=$user->getUserName()?></a> 
				<? else: ?>
					No assigned traveler
				<? endif; ?>
			</div>	
			<div id="divActionCmbStudent" class="divCmbStudent"> 
				<a href="javascript:void(0)" onClick="manager._viewCmbStudent('hsID=<?=$homestay->getHomeStayID()?>&amp;userID=<?=(null!=$user) ? $user->getTravelerID() : 0 ?>');" >Change</a>
			</div>	
		</td>
	</tr> 
	<tr>
	    <td align="right" >
			<a href="homestay.php?hsID=<?=$homestay->getHomeStayID()?>">View Details</a>  
			|
			<a href="homestay.php?mode=edit&amp;hsID=<?=$homestay->getHomeStayID()?>">Edit</a>  
			| 
			<? if (null==$user) : ?>
				<a href="homestay.php?mode=delete&amp;hsID=<?=$homestay->getHomeStayID()?>" onClick="return confirm('Delete HomeStay?')">Delete</a>
			<? else : ?>
				<a href="homestay.php?mode=delete&amp;hsID=<?=$homestay->getHomeStayID()?>" onClick="return confirm('Homestay has assigned traveler. Proceed?')">Delete</a>
			<? endif; ?>
		</td>
	</tr>
<?	$iterator->next();
endwhile;
?>

	</table>
	
<div class="pagination">
	<? if ($paging->getTotalPages() > 1 ): ?>		
		<div class="page_markers">
			<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec ?>
		</div>
		<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
	<? endif; ?>	
</div>