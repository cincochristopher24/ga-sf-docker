<?php
	/*
	 * Class.COBRANDMessagesController.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class ISINewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISI/views'));
			$this->file_factory->setPath('CSS', '/custom/ISI/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISI/views/');
		}
	 	
	 }
?>