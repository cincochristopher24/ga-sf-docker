<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDLoginController.php');
require_once 'Class.Template.php';
class ISILoginController extends COBRANDLoginController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISI/views'));
		$this->file_factory->setPath('CSS', '/custom/ISI/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISI/views/');
		Template::setMainTemplateVar('layoutID', 'page_default');
	}

	function readyLoginForm($loginData = array()){
		$content = parent::readyLoginForm($loginData);
 		$content['displayRegisterLink'] = true;
 		return $content;
 	}

}
?>