<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewMemberController.php');
	
	class ISINewMemberController extends COBRANDNewMemberController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISI/views'));
			$this->file_factory->setPath('CSS', '/custom/ISI/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISI/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
