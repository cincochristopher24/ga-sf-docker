<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	<div id="footer_inner_wrap">
	
		<div id="footer_links">
			<div>
			<ul>
				<li class="first"><a href="http://www.internationalstudent.com/" target="_blank"> International Student </a></li>
				<?= NavigationHelper::displayGlobalNavigation() ?>
			</ul>

			<ul>
        		<?= NavigationHelper::displayMainNavigation($page_location); ?>
				<li class="last">
					<?= HelperGlobal::displayLoginLink(); ?>
				</li>
			</ul>
			<p id="footlet">Copyright &copy; <? echo date("Y"); ?> My International Student and GoAbroad.net&reg;</p>
			</div>
		</div>
	</div>
</div>
	
