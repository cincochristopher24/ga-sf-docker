<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewCollectionController.php");
	
	/**
	 * The new collection controller of a cobrand.
	 */
	class ITOINewCollectionController extends COBRANDNewCollectionController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ITOI/views"));
			$this->file_factory->setPath("CSS", "/custom/ITOI/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ITOI/views/");
		}
		
	}
	
