<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDActivationSupportController.php");
	
	/**
	 * The activation support controller of a cobrand.
	 */
	class ITOIActivationSupportController extends COBRANDActivationSupportController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ITOI/views"));
			$this->file_factory->setPath("CSS", "/custom/ITOI/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ITOI/views/");
		}
		
	}
	
