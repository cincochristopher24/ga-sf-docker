<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTopicController.php');

	class ITOITopicController extends COBRANDTopicController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ITOI/views/'));
			$file_factory->setPath('CSS', '/custom/ITOI/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ITOI/views/');
		}
	}