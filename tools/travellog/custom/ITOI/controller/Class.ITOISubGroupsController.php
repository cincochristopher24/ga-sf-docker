<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class ITOISubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new ITOI subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ITOI/views"));
			$this->file_factory->setPath("CSS", "/custom/ITOI/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ITOI/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}