<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDPhotoAlbumController.php');
		
	class UFIBSPhotoAlbumController extends COBRANDPhotoAlbumController {
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/UFIBS/views'));
			$this->file_factory->setPath('CSS', '/custom/UFIBS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/UFIBS/views/'); 
		}
		
		function performAction() {
			parent::performAction();
		}
	}