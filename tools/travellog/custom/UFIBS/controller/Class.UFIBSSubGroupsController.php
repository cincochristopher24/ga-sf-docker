<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class UFIBSSubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new UFIBS subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/UFIBS/views"));
			$this->file_factory->setPath("CSS", "/custom/UFIBS/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/UFIBS/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}