<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDProfileController.php');
class MYISICProfileController extends COBRANDProfileController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/MYISIC/views'));
		$this->file_factory->setPath('CSS', '/custom/MYISIC/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/MYISIC/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>