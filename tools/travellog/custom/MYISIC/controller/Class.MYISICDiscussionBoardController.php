<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionBoardController.php');

	class MYISICDiscussionBoardController extends COBRANDDiscussionBoardController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/MYISIC/views/'));
			$file_factory->setPath('CSS', '/custom/MYISIC/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/MYISIC/views/');
		}
	}