<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class TEFLINSTITUTESubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new TEFLINSTITUTE subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/TEFLINSTITUTE/views"));
			$this->file_factory->setPath("CSS", "/custom/TEFLINSTITUTE/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/TEFLINSTITUTE/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}