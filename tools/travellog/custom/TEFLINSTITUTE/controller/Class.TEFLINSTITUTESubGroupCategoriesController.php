<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSubGroupCategoriesController.php');
	
 	class TEFLINSTITUTESubGroupCategoriesController extends COBRANDSubGroupCategoriesController {
 		
 		function __construct()
 		{
	 		parent::__construct();
	 		
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/TEFLINSTITUTE/views'));
			$file_factory->setPath('CSS', '/custom/TEFLINSTITUTE/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/TEFLINSTITUTE/views/');
 		
			Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));	
 		}
 		
 	}