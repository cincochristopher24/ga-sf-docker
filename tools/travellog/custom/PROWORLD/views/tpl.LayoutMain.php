<?php
$js_track = <<<JSTRACK
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
    var pageTracker = _gat._getTracker("UA-1198286-2");
    pageTracker._setLocalRemoteServerMode();
    pageTracker._initData();
    pageTracker._trackPageview();
</script>
JSTRACK;
Template::includeDependent($js_track, array('bottom'=>true));

require_once('travellog/custom/COBRAND/views/tpl.LayoutMain.php');
?>
