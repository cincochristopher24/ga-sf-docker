<?php
	/*
	 * Class.PROWORLDActivationSupportController.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
	
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDActivationSupportController.php");
	
	class PROWORLDActivationSupportController extends COBRANDActivationSupportController{
		
		function __construct(){
			
			parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
			$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/');
				
		}
		
	} 
	 
?>
