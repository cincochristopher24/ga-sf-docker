<?php
/*
	Filename:		Class.PROWORLDTravelPlansController.php
	Author:			Jonas Tandinco
	Date Created:	Nov/12/2007
	Putpose:		controller implementation for PROWORLD co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelPlansController.php');

class PROWORLDTravelPlansController extends COBRANDTravelPlansController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
		$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/');   
	}
	
	public function performAction(){ 
		parent::performAction();
	}
}
?>
