<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDCustomPageHeaderController.php');
class PROWORLDCustomPageHeaderController extends COBRANDCustomPageHeaderController{
	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
		$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/'); 
	}
	 
	function performAction(){ 
		
		parent::performAction(); 
		
	}
}