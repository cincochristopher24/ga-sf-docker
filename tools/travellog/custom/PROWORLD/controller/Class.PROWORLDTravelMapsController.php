<?php
/*
	Filename:		Class.PROWORLDTravelMapsController.php
	Author:			Jonas Tandinco
	Date Created:	Jan/03/2008
	Putpose:		controller implementation for PROWORLD co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');

class PROWORLDTravelMapsController extends COBRANDTravelMapsController{
	public function __construct() {
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
		$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/');   
	}
	
	public function performAction(){ 
		parent::performAction();
	}
}
?>
