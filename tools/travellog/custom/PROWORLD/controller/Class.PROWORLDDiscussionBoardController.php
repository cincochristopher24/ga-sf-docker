<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionBoardController.php');

	class PROWORLDDiscussionBoardController extends COBRANDDiscussionBoardController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/PROWORLD/views/'));
			$file_factory->setPath('CSS', '/custom/PROWORLD/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/');
		}
	}