<?php
	/*
	 * Class.PROWORLDLoginController.php
	 * Created on Nov 23, 2007
	 * created by marc
	 */
	
 	require_once("travellog/custom/COBRAND/controller/Class.COBRANDLoginController.php");
 	
 	class PROWORLDLoginController extends COBRANDLoginController{
 		
 		
 		function __construct(){
 			
 			parent::__construct();
 			
 			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/PROWORLD/views'));
			$this->file_factory->setPath('CSS', '/custom/PROWORLD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/PROWORLD/views/');
 		}
 		
 	}

?>
