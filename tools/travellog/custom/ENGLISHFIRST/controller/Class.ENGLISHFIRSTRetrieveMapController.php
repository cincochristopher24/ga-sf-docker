<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	ENGLISHFIRSTRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for ENGLISHFIRST	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ENGLISHFIRST/views'));
		$file_factory->setPath('CSS', '/custom/ENGLISHFIRST/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ENGLISHFIRST/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}