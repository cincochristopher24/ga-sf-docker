<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewMemberController.php');
	
	class ENGLISHFIRSTNewMemberController extends COBRANDNewMemberController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ENGLISHFIRST/views'));
			$this->file_factory->setPath('CSS', '/custom/ENGLISHFIRST/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ENGLISHFIRST/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
