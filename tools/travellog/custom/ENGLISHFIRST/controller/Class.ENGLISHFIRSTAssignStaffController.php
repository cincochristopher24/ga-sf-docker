<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class ENGLISHFIRSTAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ENGLISHFIRST/views'));
			$this->mFactory->setPath('CSS', '/custom/ENGLISHFIRST/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/ENGLISHFIRST/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>