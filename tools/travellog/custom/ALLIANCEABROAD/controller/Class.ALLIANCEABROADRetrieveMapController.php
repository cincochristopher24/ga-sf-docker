<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	ALLIANCEABROADRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for ALLIANCEABROAD	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ALLIANCEABROAD/views'));
		$file_factory->setPath('CSS', '/custom/ALLIANCEABROAD/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ALLIANCEABROAD/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}