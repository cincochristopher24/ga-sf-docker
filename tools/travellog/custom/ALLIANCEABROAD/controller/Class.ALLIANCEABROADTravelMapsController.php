<?php
/*
	Filename:		Class.ALLIANCEABROADTravelMapsController.php
	Author:			Jonas Tandinco
	Date Created:	Jan/03/2007
	Putpose:		controller implementation for ALLIANCEABROAD co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');

class ALLIANCEABROADTravelMapsController extends COBRANDTravelMapsController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ALLIANCEABROAD/views'));
		$this->file_factory->setPath('CSS', '/custom/ALLIANCEABROAD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ALLIANCEABROAD/views/'); 
	}
	
	public function performAction() {
		parent::performAction();
	}
}
?>