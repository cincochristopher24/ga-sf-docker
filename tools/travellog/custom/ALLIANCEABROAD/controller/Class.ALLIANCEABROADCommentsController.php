<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 14 Dec 2007
	 * Class.ALLIANCEABROADCommentsController.php
	 */

	require_once('travellog/custom/COBRAND/controller/Class.COBRANDCommentsController.php');

	class ALLIANCEABROADCommentsController extends COBRANDCommentsController {
 		function __construct(){		
			parent::__construct();	
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ALLIANCEABROAD/views'));
			$this->file_factory->setPath('CSS', '/custom/ALLIANCEABROAD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ALLIANCEABROAD/views/'); 	
 		}

 		function performAction() {
 			parent::performAction();
 		} 	
 	}
?>