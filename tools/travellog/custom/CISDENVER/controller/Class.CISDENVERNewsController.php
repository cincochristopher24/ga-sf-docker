<?php
	/*
	 * Class.COBRANDMessagesController.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	 
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class CISDENVERNewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CISDENVER/views'));
			$this->file_factory->setPath('CSS', '/custom/CISDENVER/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CISDENVER/views/');
		}
	 	
	 }
?>