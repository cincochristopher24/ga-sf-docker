<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelersController.php');
	
	class VFCTravelersController extends COBRANDTravelersController {
	
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/VFC/views'));
			$this->file_factory->setPath('CSS', '/custom/VFC/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VFC/views/');
		}
	
		function performAction(){
			parent::performAction();
		}
	
	}