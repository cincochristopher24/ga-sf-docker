<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDGroupMembersController.php');
	
	class ATHENAGroupMembersController extends COBRANDGroupMembersController{
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ATHENA/views'));
			$this->file_factory->setPath('CSS', '/custom/ATHENA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ATHENA/views/'); 
		}
		 
		function performAction(){ 
			
			parent::performAction(); 
			
		}
	}