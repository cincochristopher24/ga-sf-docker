<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDActivationSupportController.php");
	
	/**
	 * The activation support controller of a cobrand.
	 */
	class ATHENAActivationSupportController extends COBRANDActivationSupportController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ATHENA/views"));
			$this->file_factory->setPath("CSS", "/custom/ATHENA/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ATHENA/views/");
		}
		
	}
	
