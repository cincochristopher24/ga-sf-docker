<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	GVIRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for GVI	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/GVI/views'));
		$file_factory->setPath('CSS', '/custom/GVI/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GVI/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}