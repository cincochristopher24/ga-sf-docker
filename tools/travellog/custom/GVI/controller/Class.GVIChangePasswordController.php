<?php
/*
 * Created on 12 23, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

	require_once('travellog/custom/COBRAND/controller/Class.COBRANDChangePasswordController.php');
	
 	class GVIChangePasswordController extends COBRANDChangePasswordController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GVI/views'));
			$this->file_factory->setPath('CSS', '/custom/GVI/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GVI/views/');
 		}
 		
 		function performAction(){ 
			parent::performAction();
		}
 	}