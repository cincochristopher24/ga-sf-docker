<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewCollectionController.php');
class GVINewCollectionController extends COBRANDNewCollectionController{
	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GVI/views'));
		$this->file_factory->setPath('CSS', '/custom/GVI/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GVI/views/'); 
	}
	 
	function performAction(){ 
		
		parent::performAction(); 
		
	}
}