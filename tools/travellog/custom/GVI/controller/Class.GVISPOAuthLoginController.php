<?php 
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSPOAuthLoginController.php");
	
	/**
	 * The SP OAuth login controller of a cobrand.
	 */
	class GVISPOAuthLoginController extends COBRANDSPOAuthLoginController{
		
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/GVI/views"));
			$this->file_factory->setPath("CSS", "/custom/GVI/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/GVI/views/");
		}
		
	}
	
