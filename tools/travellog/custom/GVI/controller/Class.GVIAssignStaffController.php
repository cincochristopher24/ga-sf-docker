<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class GVIAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GVI/views'));
			$this->mFactory->setPath('CSS', '/custom/GVI/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/GVI/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>