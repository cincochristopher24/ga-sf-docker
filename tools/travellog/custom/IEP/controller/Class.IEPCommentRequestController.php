<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 26 November 2007
	 * Class.IEPCommentRequestController.php
	 */

	require_once 'travellog/custom/COBRAND/controller/Class.COBRANDCommentRequestController.php';

	 class IEPCommentRequestController extends COBRANDCommentRequestController {

	 	function __construct(){

			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/IEP/views'));
			$this->file_factory->setPath('CSS', '/custom/IEP/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/IEP/views/');
		}

	 	function performAction() {
	 		parent::performAction();
	 	}
	 }
?>