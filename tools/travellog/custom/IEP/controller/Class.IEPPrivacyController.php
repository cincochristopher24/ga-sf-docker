<?php
/*
 * Created on 16-Nov-07
 *
 *  Author: K. Gordo
 */
 
 require_once('travellog/custom/COBRAND/controller/Class.COBRANDPrivacyController.php');
 
 class IEPPrivacyController extends COBRANDPrivacyController {
 	function __construct(){
 		parent::__construct();
 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/IEP/views'));
		$this->file_factory->setPath('CSS', '/custom/IEP/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/IEP/views/'); 
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 }
  
?>