<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php');
class IEPSubGroupsController extends COBRANDSubGroupsController{
	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/IEP/views'));
		$this->file_factory->setPath('CSS', '/custom/IEP/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/IEP/views/'); 
	}
	 
	function performAction(){ 
	
		parent::performAction(); 
		
	}
}
?>