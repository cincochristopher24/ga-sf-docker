<?php
/*
	Filename:		Class.AIFSTravelMapsController.php
	Author:			Jonas Tandinco
	Date Created:	Jan/03/2007
	Putpose:		controller implementation for AIFS co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');

class AIFSTravelMapsController extends COBRANDTravelMapsController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AIFS/views'));
		$this->file_factory->setPath('CSS', '/custom/AIFS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/'); 
	}
	
	public function performAction() {
		parent::performAction();
	}
}
?>