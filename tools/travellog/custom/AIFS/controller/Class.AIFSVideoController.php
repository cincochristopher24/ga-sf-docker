<?php
/*
 * Created on 11 27, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

 	require_once('travellog/custom/COBRAND/controller/Class.COBRANDVideoController.php');
	
 	class AIFSVideoController extends COBRANDVideoController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AIFS/views'));
			$this->file_factory->setPath('CSS', '/custom/AIFS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/');
 		}
 		
 		function performAction(){ 
			parent::performAction();
		}
 	}