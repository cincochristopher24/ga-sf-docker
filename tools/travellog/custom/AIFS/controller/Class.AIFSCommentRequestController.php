<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 26 November 2007
	 * Class.AIFSCommentRequestController.php
	 */

	require_once 'travellog/custom/COBRAND/controller/Class.COBRANDCommentRequestController.php';

	 class AIFSCommentRequestController extends COBRANDCommentRequestController {

	 	function __construct(){

			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AIFS/views'));
			$this->file_factory->setPath('CSS', '/custom/AIFS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/');
		}

	 	function performAction() {
	 		parent::performAction();
	 	}
	 }
?>