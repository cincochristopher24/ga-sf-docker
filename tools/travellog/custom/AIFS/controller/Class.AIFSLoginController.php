<?php
	/*
	 * Class.AIFSLoginController.php
	 * Created on Nov 23, 2007
	 * created by marc
	 */
 
 	require_once("travellog/custom/COBRAND/controller/Class.COBRANDLoginController.php");
 	
 	class AIFSLoginController extends COBRANDLoginController{
 		
 		
 		function __construct(){
 			
 			parent::__construct();
 			
 			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AIFS/views'));
			$this->file_factory->setPath('CSS', '/custom/AIFS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/');
 		}
 		
 	}
?>
