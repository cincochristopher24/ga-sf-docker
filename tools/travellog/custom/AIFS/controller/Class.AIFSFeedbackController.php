<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDFeedbackController.php');
 	class AIFSFeedbackController extends COBRANDFeedbackController 
 	{
		function __construct(){
			parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/AIFS/views'));
			$this->file_factory->setPath('CSS', '/custom/AIFS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/AIFS/views/');
		}
 	}
?>