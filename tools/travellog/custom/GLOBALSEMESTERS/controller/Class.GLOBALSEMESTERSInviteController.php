<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDInviteController.php');
class GLOBALSEMESTERSInviteController extends COBRANDInviteController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GLOBALSEMESTERS/views'));
		$this->file_factory->setPath('CSS', '/custom/GLOBALSEMESTERS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSEMESTERS/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>