<?php

	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionBoardHomeController.php');

	class GLOBALSEMESTERSDiscussionBoardHomeController extends COBRANDDiscussionBoardHomeController {

		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/GLOBALSEMESTERS/views/'));
			$this->file_factory->setPath('CSS', '/custom/GLOBALSEMESTERS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSEMESTERS/views/');
		}
	}
?>