<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTopicController.php');

	class GLOBALSEMESTERSTopicController extends COBRANDTopicController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/GLOBALSEMESTERS/views/'));
			$file_factory->setPath('CSS', '/custom/GLOBALSEMESTERS/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSEMESTERS/views/');
		}
	}