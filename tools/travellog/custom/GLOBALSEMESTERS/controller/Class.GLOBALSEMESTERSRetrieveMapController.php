<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	GLOBALSEMESTERSRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for GLOBALSEMESTERS	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/GLOBALSEMESTERS/views'));
		$file_factory->setPath('CSS', '/custom/GLOBALSEMESTERS/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSEMESTERS/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}