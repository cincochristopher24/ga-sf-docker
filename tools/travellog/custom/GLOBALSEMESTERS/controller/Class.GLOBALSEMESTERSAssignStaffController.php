<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class GLOBALSEMESTERSAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/GLOBALSEMESTERS/views'));
			$this->mFactory->setPath('CSS', '/custom/GLOBALSEMESTERS/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/GLOBALSEMESTERS/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>