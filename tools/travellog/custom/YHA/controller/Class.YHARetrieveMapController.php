<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	YHARetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for YHA	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/YHA/views'));
		$file_factory->setPath('CSS', '/custom/YHA/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/YHA/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}