<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDConfirmNotificationController.php');
	
 	class FEAConfirmNotificationController extends COBRANDConfirmNotificationController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/FEA/views'));
			$this->file_factory->setPath('CSS', '/custom/FEA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/FEA/views/');
 		}
 	}