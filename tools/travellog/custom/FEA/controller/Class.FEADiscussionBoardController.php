<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionBoardController.php');

	class FEADiscussionBoardController extends COBRANDDiscussionBoardController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/FEA/views/'));
			$file_factory->setPath('CSS', '/custom/FEA/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/FEA/views/');
		}
	}