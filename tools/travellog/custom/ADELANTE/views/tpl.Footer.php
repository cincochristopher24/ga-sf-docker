<?php
require_once('Class.HelperGlobal.php');
?>

<div id="footer">
	<div id="footer_inner_wrap">
		<div id="footer_links">
			<div>
				<ul id="footer_ul1">
					<li class="first"><a href="<?= $GLOBALS['CONFIG']->getUrl() ?>">Adelante Abroad</a></li>
					<li><a href="/privacy-policy.php">Privacy Policy</a></li>
					<li><a href="/terms.php">Terms of use</a></li>
				</ul>

				<ul id="footer_ul2">
					<li class="first"><a href="/passport.php">My Passport</a></li>
					<li><a href="/travelers.php">Students</a></li>
					<li><a href="/travel.php">Journals</a></li>
					<li><a href="/subgroups.php">Groups</a></li>
					<li class="last">
						<?= HelperGlobal::displayLoginLink(); ?>
					</li>
				</ul>
				<p id="footlet">Copyright &copy; <? echo date("Y"); ?> Adelante Abroad and GoAbroad.net&reg;</p>
			</div>		
		</div>
	</div>
</div>
	
