<?php
/*
Variables passed to this template:
    $isPublic       :   set if public view
	$isAdvisor		: 	holds a boolean value. TRU if the traveler logged in is an advisor. FALSE otherwise.
	$gID            :   The group ID if $isAdvisor is true
	$loggedUser		: 	an instance(object) of class Traveler
	 					$loggedUser is passed just in case $isAdvisor is insufficient
						to decide on which features to display and what location/url to associate with its links.
*/	
if ($loggedUser instanceof Traveler) {
    $username = $loggedUser->getUserName();
    $id = $loggedUser->getTravelerId();
}


$add_journal_link        = '/journal.php?action=add';
$my_travel_journals_link = '/travel.php?action=myjournals';
$advisor_journals_link   = '/login.php?redirect=travel.php?action=groupJournals';
$widgets_link            = '/widget.php';
$photos_link             = '/login.php';
$group_home_link         = '/login.php';
$groups_link             = '/group.php?mode=mygroups';
$dboard_link             = '/login.php?redirect=discussionboard/home.php';
$showTravelers = true;
$showAdvisors  = true;

if ($isPublic) {
    $add_journal_link = '/login.php?redirect=journal.php?action=add';
    $my_travel_journals_link = '/login.php?redirect=travel.php?action=myjournals';
    $widgets_link = '/login.php?redirect=widget.php';
    $photos_link = '/login.php';
    $groups_link = '/login.php?redirect=group.php?mode=mygroups';
} elseif (!$isAdvisor) {
    $photos_link = '/photos.php?type=traveler&ID=' . $id;
    $showAdvisors  = false;
} elseif ($isAdvisor) {
    $photos_link = '/photos.php?type=group&ID=' . $gID;
    $advisor_journals_link = '/travel.php?action=groupJournals&gID=' .$gID;
    $group_home_link = '/group.php?gID=' . $gID;
    $dboard_link             = '/discussionboard/home.php?gID=' . $gID;
    $showTravelers = false;
}

Template::setMainTemplateVar('page_location', 'What\'s New?');
?>

<div id="intro_container" class="feature">
	<div id="intro">
		<h1>Welcome to the all-new <?php echo $GLOBALS['CONFIG']->getSiteName(); ?> Network!</h1>
		<div id="content">
			<p>
			    We have updated our look and have added cool new features to <?php echo $GLOBALS['CONFIG']->getSiteName(); ?> Network. 
			</p>
		</div>
	</div>
</div>


<div class="layout_2" id="content_wrapper">	
	<div id="full_column">
	    <?if ($showTravelers): ?>
	        <?if ($isPublic):?><h2>For Travelers</h2><?endif;?>
    	    <ul class="new_features">
    		    <li>
    				<h4>New Login, Customizable Profile Name!</h4>
    				<p>
    				    So, why are we now asking for your email address instead of your username? It's because we now allow you to edit your Profile Name - the name that appears on your traveler page and is your unique name on the site. You used to type this in as your username when logging in. You may now edit this name to anything you please, at any time. We want to make logging in easier for you in case you are unable to keep track of your Profile Name changes - and we understand that it's generally easier for you to remember your email address.
    				</p>
    				<p>
    				    <a href="/edittravelerprofile.php"><strong>Review your email address or customize your Profile Name&nbsp;&raquo;</strong></a>
    				</p>
    				<p>
    				  <img src="images/feature/customname1.jpg" />
    				</p>
    			</li>			
    			<li>
    				<h4>Add Videos to your Journals!</h4>
    				<p>
    				    You may now embed your Google Video or YouTube videos in your journal entries. Simply
    				    create a journal entry, or go to one of your existing journals, and click on <strong>Add a Video</strong>.
    				</p>
    				<p>
    				    <a href="<?=$add_journal_link?>"><strong>Write a journal entry</strong></a> or  
    				    <a href="<?=$my_travel_journals_link?>"><strong>pick an entry from your journals</strong></a>
    				    to <strong>Add a Video</strong> now!
    				</p>
    				<p>
    				  <br /><br />
    				  <img src="images/feature/video2.jpg" />
    				</p>
    			</li>
    			<li class="clear">
    				<h4>Send your Journal Entries as Postcards!</h4>
    				<p>
    				    Let everyone know that you have a new journal entry by sending an e-postcard! After you're done with your new entry, we let you pick a photo, write a message, and we will then send it along to your family and friends.  
    			  </p>
    			  <p>
    			        <a href="<?=$my_travel_journals_link?>"><strong>Send a journal entry as a postcard&nbsp;&raquo;</strong></a>
    			  </p>
		        <p>
    				  <img src="images/feature/journal3.jpg" />
    				</p>
    			</li>
    			<li>
    				<h4>Bring your Journals and Traveler Profile to your Facebook Profile!</h4>
    				<p>
    				    We have four Facebook applications that let you bring the traveler in you to your Facebook profile. These apps let you sync your journals, travel map and travel plans with your Facebook page. All you need to do is add our apps on Facebook and sync them with your <?php echo $GLOBALS['CONFIG']->getSiteName(); ?> Network profile. Whenever you make a change on one site or the other, the change reflects on both websites.
    				</p>
    				<p>
    				    <a href="<?=$widgets_link?>"><strong>Update your Apps&nbsp;&raquo;</strong></a>
    				</p>
    				<p>
    				  <img src="images/feature/apps4.jpg" />
    				</p>
    			</li>
    			<li class="clear">
    				<h4>Photos Section</h4>
    				<p>
    				    You can now showcase all of your photos on one page. 
    				</p>
    				<p>
    				    <a href="<?=$photos_link?>"><strong>View all your photos&nbsp;&raquo;</strong></a>
    				</p>
    				<p>
    				  <img src="images/feature/photos5.jpg" />
    				</p>
    			</li>
    			<li>
    				<h4>Discussion Board</h4>
    				<p>
    				    Interact with the travelers on your groups or clubs! Post topics for discussion or join discussions about your common interests.
    				</p>
    				<p>
    				    <a href="<?= $groups_link ?>"><strong>Go to your groups and join a discussion&nbsp;&raquo;</strong></a>
    				</p>
    				<p>
    				  <img src="images/feature/discussion6.jpg" />
    				</p>
    			</li>
    	    </ul>
	    <?endif;?>
	    
	    
	    <div id="border">&nbsp</div>
	    <?if ($showAdvisors): ?>
	        <?if ($isPublic):?><h2>For Advisors</h2><?endif;?>
    		<ul class="new_features">
    		        <li>
        				<h4>New Login, Customizable Profile Name!</h4>
        				<p>
        				    So, why are we now asking for your email address instead of your username? It's because we now allow members to edit their Profile Name - the name that appears on their traveler page and is their unique name on the site. If you have a regular traveler account, you used to type this in as your username when logging in. You and your members may now edit this name to anything you please, at any time. We want to make logging in easier for you and your members in case you are unable to keep track of your Profile Name changes - and we understand that it's generally easier for you to remember your email address.
        				</p>
						<?if ($isPublic):?>	
        				<p>
        				    <a href="<?=$group_home_link?>"><strong>Review your email address&nbsp;&raquo;</strong></a>
        				</p>
						<?endif;?>	
        				<p>
    				    	<img src="images/feature/customname1.jpg" />
    				    </p>
        			</li>
        			<?if (false): //hide this for now?>		
        			    <li>
            				<img src="/images/features/2.gif" />
            				<h4>Add Videos to your Journals!</h4>
            				<p>
            				    You may now embed your Google Video or YouTube videos in your journal entries. Simply
            				    create a journal entry, or go to one of your existing journals, and click on <strong>Add a Video</strong>.
            				    <br /><br />
            				    <a href="<?=$add_journal_link?>"><strong>Write a journal entry</strong></a> or  
            				    <a href="<?=$advisor_journals_link?>"><strong>pick an entry from your journals</strong></a>
            				    to <strong>Add a Video</strong> now!
            				</p>
            			</li>
            	<?endif;?>
        			<li>
        				<h4>Send your Journal Entries as Postcards!</h4>
        				<p>
        				    Let everyone know that your group has a new journal entry by sending an e-postcard! After you're done with your new entry, we let you pick a photo, write a message, and we will then send it along to your and your members' family and friends. This is also a nice way to send updates to the parents of your program participants.
        			  </p>
					<?if ($isPublic):?>	
        			  <p>
        			        <a href="<?=$advisor_journals_link?>"><strong>Send a journal entry as a postcard&nbsp;&raquo;</strong></a>
        			  </p>
					<?endif;?>		
        			  <p>
        				  <img src="images/feature/journal3.jpg" />
        				</p>
			    
        			</li>
        			
        			<li class="clear">
        				<h4>Photos Section</h4>
        				<p>
        				    You can now showcase all of your group's photos on one page.
        				</p>
						<?if ($isPublic):?>
        				<p>
        				    <a href="<?=$photos_link?>"><strong>View all your photos&nbsp;&raquo;</strong></a>
        				</p>
						<?endif;?>		
        				<p>
        				  <img src="images/feature/photos5.jpg" />
        				</p>
        			</li>
        			
        			<li>
        				<h4>Discussion Board</h4>
        				<p>
        				    Let your members reach you while encouraging interaction among your members! Post topics for discussion or join discussions about your group's interests and activities
        				</p>
						<?if ($isPublic):?>
        				<p>
        				    <a href="<?=$dboard_link?>"><strong>Go to your discussion board&nbsp;&raquo;</strong></a>
        				</p>
						<?endif;?>		
        				<p>
        				  <img src="images/feature/discussion6.jpg" />
        				</p>
        			</li>
        			
        			<li class="clear">
        				<h4>Let your members bring their Journals and Traveler Profile to Facebook!</h4>
        				<p>
        				    We have four Facebook applications that let your members show off the traveler in them on their Facebook profiles. These apps let them bring their journals, travel map and travel plans to Facebook. All they have to do is add our app on Facebook and sync them to their <?php echo $GLOBALS['CONFIG']->getSiteName(); ?> Network profile. Whenever they make a change on one site or the other, the change reflects on both websites.
                </p>
                <p>
                            These apps give value to what they put up on <?php echo $GLOBALS['CONFIG']->getSiteName(); ?> Network, knowing they can show off their travel journals and photos even to their non-traveler friends. You and your students benefit from this: your student can have beautiful travel journals and easily link these journals to Facebook -- and you can keep these great testimonials on a site with your look and feel, and your entire control.        				   
        				</p>
        				<p>
        				  <img src="images/feature/apps4.jpg" />
        				</p>
        			</li>
        	</ul>
    	<?endif;?>
	</div>
</div>
