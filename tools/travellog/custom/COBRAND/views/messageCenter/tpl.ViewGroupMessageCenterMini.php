<?php require_once 'travellog/helper/gaTextHelper.php' ?>
<?php require_once 'travellog/helper/messageCenter/messageCenterHelper.php' ?>
<div id="messageNotification"></div>
<div id="main_content">
	<div id="inboxContainer">
		<table id="inbox" class="message_table">
			<tbody>
				<?php if (count($messages) > 0) : ?>
					<?php foreach ($messages as $message) : ?>
						<?php
							includeView('singleMessage', array(
															'message'        => $message,
															'snippetLimit'   => $snippetLimit,
															'canBeSelected'  => $canBeSelected,
															'canBeDeleted'   => $canBeDeleted,
															'showRealName'   => $showRealName,
															'showReadStatus' => $showReadStatus,
														))
						?>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="help_text" style="text-align:center">
						<span>You don't have messages yet<span>. <a href="/messages.php?compose" title="Compose a message">Send a message</a> <?if($thisIsAClub):?>to your friends<?else:?>to your groups<?endif;?>.
					</div>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	var isPowerful = <?php echo $isPowerful ? 1 : 0 ?>;
	var groupID = <?php echo isset($_GET['gID']) ? $_GET['gID'] : 0 ?>;
</script>