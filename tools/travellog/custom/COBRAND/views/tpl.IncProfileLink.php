<?php
/*
 * Created on Aug 10, 2006
 * Tpl.ProfileLink.php - a photo, add/edit profile link
 */
?>

<div class="section" id="profile">

	<h2><span>Profile</span></h2>

	<div class="content">
		<? if ($age): ?>
			<div id="traveler_info">
				<a class="user_thumb" href="<?= htmlspecialchars($viewProfileLink) ?>" title="View Profile" ><img src="<?= $profileImage ?>" alt="profile photo" width="37" height="37" /></a>
				<p><span>Age:</span> <?=$age?></p>
				<p><span>Hometown:</span> <?=$hometown?></p>
				<p><span>Currently in:</span>
					<? if (NULL != $location ) :?>
						<?=$location->getName() ?>
					<? endif; ?>
				</p>	
				<p class="actions">
					<a href="<?= htmlspecialchars($editProfileLink) ?>" >Edit Profile</a> | <a href="<?= htmlspecialchars($viewProfileLink) ?>" >View Profile</a>
				</p>
			</div>
		<? else: ?>
			<p class="help highlight">
				<strong>Tell us about yourself!</strong> Create your Profile and be on your way to meeting other Travelers. 
			</p>
			<div class="actions">
				<a href="<?= htmlspecialchars($editProfileLink) ?>" class="button">Create Profile</a>
				<div class="clear"></div>
			</div>
		<? endif; ?>

			<? if ( ($messagesLink !== '') || isset($frLink) || isset($griLink) || isset($grrLink) || isset($dgrrLink) || (isset($commentsLink) && trim($commentsLink) !== '') || isset($surveyFormsLink)): ?>
			<div id="notifications">	
				<div class="newalertshead"></div>
				<ul>
					<? if ($messagesLink !== ''): ?>
						<li><?= $messagesLink ?></li>
					<? endif; ?>
					<?if (isset($frLink)): ?>
						<li><?=$frLink?></li>
					<?endif;?>	
					<?if (isset($griLink)): ?>
						<li><?=$griLink?></li>
					<?endif;?>
					<?if (isset($grrLink)): ?>
						<li><?=$grrLink?></li>
					<?endif;?>
					<?if (isset($dgrrLink)): ?>
						<li><?=$dgrrLink?></li>
					<?endif;?>
					<? if (isset($commentsLink) && $commentsLink !== ''): ?>
						<li><?=$commentsLink?></li>
					<?endif;?>
					<?if (isset($surveyFormsLink)): ?>
						<li><?=$surveyFormsLink?></li>
					<?endif;?>
				</ul>
			</div>
			<? endif; ?>
		<div class="clear"></div>		
	</div>
	
	<div id="links">
		<ul>
			<li><a href="<?= htmlspecialchars($privacyPreferencesLink) ?>" >Privacy Preferences</a></li>
			
				<?/* if (isset($edit_resume_link)) : ?>	
						<!--<strong>Resume Views:</strong> <?=$resume_views ?> <br />-->
						<li><a href="<?=$edit_resume_link?>">Edit Resume</a></li>
						<li><a href="<?=$view_resume_link?>">View Resume</a></li>	
				<? else:  ?>	
						<li><a href="resume.php?action=add">Create your resume &raquo;</a></li>			
				<? endif; */?>
			 
			<li><a href="<?=$inviteFriendsLink ?>">Tell everyone about the GoAbroad Network!</a></li>			
		</ul>
	</div>
	
	<div class="foot"></div>
</div>

