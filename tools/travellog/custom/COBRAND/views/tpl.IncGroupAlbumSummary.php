<?php
require_once('Class.GaDateTime.php');
$d = new GaDateTime();
?>
<?php if (count($albums)):?>
	<div class="section photos" id="featured_photos">
		<h2><span>Photo Albums</span></h2>
		<div class="content">
				<?php foreach ($albums as $album): ?>
					<!--<div class="photo">
						<a href="#" title="Featured Photo Album">
							<img src="<?= $album->getPrimaryPhoto()->getPhotoLink('default') ?>" alt="Featured "/>
						</a> 
						<div class="caption">
							Photo Album 1
						</div>	
							<a href="#" title="Travel Community - ...">Photo Album 1</a>
					</div>-->
					
					
					<div class="photo_box">
								<div class="photo_cont">
									<a class="user_thumb" href="<?='/photomanagement.php?cat=photoalbum&action=view&groupID='.$album->getGroupID()."&genID=".$album->getPhotoAlbumID();  ?>" class="user_thumb" > 																												
										<img class="image_bg" src="<? echo $album->getPrimaryPhoto()->getPhotoLink('default'); ?>" />																										
									</a>
								</div>																																					
								<div class="text_cont">
									<div class="photo_caption meta"> 
										

										<? echo $album->getTitle(); ?>
									</div>																
									<p class="lst_update">Last Update: <?= $d->set($album->getLastUpdate())->friendlyFormat(); ?></p>
									<p class="photo_items">contains: <? echo count($album->getPhotos())." item", (count($album->getPhotos())>1)? "s":"" ?></p> 
									<p><a href="<?='/photomanagement.php?cat=photoalbum&amp;action=view&amp;groupID='.$album->getGroupID()."&amp;genID=".$album->getPhotoAlbumID();  ?>">View Album</a></p>									
								</div>				
					</div>
					
					
				<?php endforeach;?>
			
			
			
				<div class="clear"></div>
				
				<div class="section_foot">
					<a href="/photoalbum.php?action=view&amp;groupID=<?=$groupID?>" class="more">
						<?php if ($owner): ?>
							Manage Photo Albums
						<?php else: ?>
							View Photo Albums
						<?php endif;?>
					</a>
				</div>
		</div>
		<div class="foot"></div>
	</div>
<?php endif; ?>
