<ul class="toc">
	<li><a href="#eligibility">Eligibility</a></li>
	<li><a href="#term">Term</a> </li>
	<li><a href="#conduct">Member Conduct </a></li>
	<li><a href="#rightscontent"><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net Proprietary Rights on Website Content</a></li>
	<li><a href="#rightsmember">Proprietary Rights on Member Posted Content on the Website</a></li>
	<li><a href="#copyright">Copyright Policy</a></li>
	<li><a href="#privacy">Privacy</a></li>
	<li><a href="#disclaimers">Disclaimers</a></li>
	<li><a href="#limitation">Limitation on Liability</a></li>
	<li><a href="#indemnity">Indemnity</a></li>
	<li><a href="#other">Other</a>
</ul>
<p>
	 Welcome to <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad Network  -  the ultimate site for travelers. The <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net service and network are operated by GoAbroad.com  ("Company"). By using the <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net website ("Website") as a member or nonmember you agree to be bound by these Terms of Service ("Terms"). If you wish to become a <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net member ("Member") and take advantage of our services ("Service"), you must read these Terms and indicate your acceptance by following the instructions in the <a href="/register.php">Registration</a> page. We reserve the right to change, modify, add or delete portions of these terms at any time without further notice. <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net may also offer services of our partners that are governed by different Terms of Use and you may be required to agree to their terms as well.
</p>

<a name="eligibility"></a><h2>Eligibility</h2>		
 <ul>
 <li> You must be at least thirteen (13) years of age to register as a Member of <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net or use the Website. </li>
 <li>By accessing and using the Website, you are certifying that you are 13 years of age or older and you agree to abide by all of the Terms and conditions of this Website. </li>
 <li>If you do not agree to the Terms, do not use the Website.</li>
 </ul>
 	
<a name="term"></a><h2>Term</h2>
<ul>
 <li>These Terms will remain in effect while you use the Website and/or a Member.</li>
 <li>You may terminate your membership at any time for any reason.</li>
 <li>We reserve the right to terminate our Service at any time for any reason.</li>
 <li><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net reserves the right to refuse Service to anyone at any time.</li>
</ul>

<a name="conduct"></a><h2>Member Conduct</h2>
<ul>
<li><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net is for personal use only of Members and may not be used in any commercial endeavor without our express written consent in each case.</li>
<li>Members are responsible for any activity that occur under their account and confidentiality of their password.</li>
<li> Members warrant and agree that no materials of any kind submitted through their account will violate or infringe upon the rights of any third party such as copyright, trademark, privacy, personal and proprietary rights.</li>
<li>Members may not copy, transfer or use any content belonging to or posted by <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net or other Members for the purpose of selling or advertising.</li>
<li>Members may not collect usernames, email addresses or other personally identifiable information of other Members for the purpose of sending unsolicited emails and messages.</li>
<li>Members are not allowed to transmit any material that contains software viruses or any other malicious computer code, files or programs.</li>
<li>Members are not allowed to post, upload, email or transmit any content that is pornographic, obscene, vulgar, libelous, threatening, abusive, harmful, hateful or offensive.</li>
<li>Members are not allowed to use the Website to engage in any illegal activities deemed illegal in teh United States or in the host country of the members countries of origin this includes organizing any activities illegal, or conducting any business which is illegal.</li>
</ul>

<a name="rightscontent"></a><h2><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net  Proprietary Rights on Website Content</h2>
<ul>
<li>All content on the Website such as logos, design, text, graphics, other files ("Content") are the proprietary property of the Company. All  rights reserved.</li>
<li>No Content may be copied, modified, published, transmitted, distributed, displayed or sold without our express written consent in each case.</li>
</ul>

<a name="rightsmember"></a><h2>Proprietary Rights on Member Posted Content on the Website</h2>
<ul>
 <li>Members are solely responsible for photos, messages and other Content that are posted on our Website through their account.</li>
 <li>By posting on our Website, you automatically grant the Company an irrevocable, perpetual, non-exclusive, fully paid, transferrable, worldwide license to use, copy, perform, display and distribute such Content.</li>
 <li>The Company may review and remove any Member Content that violate these Terms.</li>
 <li>Members may remove Content they posted from the Website at anytime and if such an action is taken, the license mentioned above will automatically expire.</li>
 </ul>

<a name="copyright"></a><h2>Copyright Policy</h2>
<ul>
<li>Member may not post, distribute or reproduce in any way any copyrighted material, trademarks and/or other proprietary information without obtaining the prior written consent of the owner.</li>
<li>If you believe your work's copyright has been infringed on this Website, please <a href="feedback.php">contact us</a>.</li>
</ul>

<a name="disputes"></a><h2>Member Disputes</h2>
<ul>
<li>Members are solely responsible for their interactions with other <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net Members. <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net reserves the right (but has no obligation) to monitor disputes between you and other Members.</li>
</ul>

<a name="privacy"></a><h2>Privacy</h2>
<ul>
<li>Use of this Website and/or Service is governed by our <a href="privacy-policy.php">Privacy Policy</a>.</li>
</ul>
 
<a name="disclaimers"></a><h2>Disclaimers</h2>
<ul>
<li>The Company is not responsible for any incorrect or inaccurate Content posted on the Website or in connection with the Service, whether caused by users of the Website, Members, our advertisers or corporate partners, or by any of the equipment or programming associated with or utilized inthe operation of the Website or the Service.</li>
<li>The Company is not responsible for the conduct, whether online or offline, of any user of the Website or Member of the Service.</li>
<li><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net assumes no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorized access to, or alteration of, user or Member communications.</li>
<li><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net is not responsible for any problems or technical malfunction of any telephone network or lines, computer online systems, servers or providers, computer equipment, software, failure of email or players on account of technical problems or traffic congestion on the Internet or at any website or combination thereof, including injury or damage to users and/or Members or to any other person's computer related to or resulting from participating or downloading materials in connection with the Web and/or in connection with the Service.</li>
<li>Under no circumstances will the Company be responsible for any loss or damage, including personal injury or death, resulting from anyone's use of the Website or the Service, any Content posted on or through the Website or the Service or transmitted to Members, or any interactions between users of the Website or Members, whether online or offline.</li>
<li>The Website and the Service are provided "AS-IS" and, except as otherwise prohibited by applicable law, we expressly disclaim any warranty of any kind, including, but not limited to warranties of merchantability, fitness for a particular purpose, and non-infringement.</li>
<li><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net cannot guarantee and do not promise any specific results from use of the Website or the Service. No advice or information, whether oral or written, obtained by you from us, or from or through the Website or the Service shall create any warranty not expressly stated herein.</li>
<li><?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net encourages parents of minors to monitor their childrenwhenever they are active on the internet.  Like every networking site a minor is at risk whenever revealing private information to unknown users onthe internet.  Minors should be aware of the risks of internet usage.</li>
</ul>
			
<a name="limitation"></a><h2>Limitation on Liability</h2>
<p>Except in jurisdictions where such provisions are restricted, in no event will GoAbroad Network be liable to you or any third person for any indirect, consequential, exemplary, incidental, special or punitive damages, including also lost profits arising from your use of the Website or the Service, even if <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net has been advised of the possibility of such
 damages. Notwithstanding anything to the contrary contained herein, <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net's liability to you for any cause whatsoever, and regardless of the form of the action, will at all times be limited to the amount paid, if any, by you to <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net for the Service during the term of membership.
 </p>

<a name="indemnity"></a><h2>Indemnity</h2>
<p>You agree to indemnify and hold <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net, its subsidiaries, affiliates, officers, agents, and other partners and employees, harmless from any loss, liability, claim, or demand, including reasonable attorney's fees, made by any third party due to or arising out of your use of the Service in violation of this Agreement and/or arising from a breach of this Agreement and/or any breach of your representations and warranties set forth above.
 </p>

<a name="other"></a><h2>Other</h2>
<p>You will be deemed to have accepted the Terms upon use of the Website or by becoming a Member. This Agreement, together with our <a href="privacy-policy.php">Privacy Policy</a>, and any notices regarding the Website or the Service sent to you or posted on the Website, which are incorporated herein by this reference, contains the entire agreement between you and <?=$GLOBALS['CONFIG']->getSiteName()?> and GoAbroad.net regarding the use of the Website or the Service. Our failure to exercise or enforce any right or provision of these Terms shall not constitute a waiver of such right or provision. If any provision of these Terms are held invalid or unlawful, the remainder of these Terms shall continue in full force and effect.
</p>
