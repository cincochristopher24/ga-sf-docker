<?php
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	Template::includeDependentJs('/js/subGroupsManager.js');
	if ($isAdminLogged AND false == $is_searching) {
		Template::includeDependentJs('/js/scriptaculous/effects.js');
		Template::includeDependentJs('/js/scriptaculous/dragdrop.js');
	}
	
	$tab_action = (isset($tab_action)) ? $tab_action : "showActive";
?>
  <?= $profile->render()?>
  
  <?=$subNavigation->show();?>

	<div class="layout_2" id="content_wrapper">
		<div id="wide_column">
			<? if ($isAdminLogged): ?>
				<div id="featured_groups" class="section">
					<h2><span>Featured Groups</span></h2>
					<div class="section_small_details">
						Below are the groups featured in your group home page.
					</div>
					<div class="content" id="featuredGroupsContent">
						<?=	$featuredSubGroupsTemplate ?>
					</div>
				</div>
			<? endif; ?>
			
			<div id="groups_category_mode" class="section">	
				<h2>
					<span>Groups</span>
					<span class="header_actions">						
					  <a href="javascript:void(0)" style="<?=("showActive" === $tab_action) ? "color:gray;" : "";?>" onclick="manager._viewActive('gID=<?=$grpID?>&amp;mode=ajax_category&amp;tabAction=showActive&amp;searchKey=<?=$searchKey?>');" id="viewActive">
					    Active (<strong id="active_id"><?=$cnt_active?></strong>)
					  </a> 
					  |
					  <a href="javascript:void(0)" style="<?=("showInActive" === $tab_action) ? "color:gray;" : "";?>"  onclick="manager._viewInActive('gID=<?=$grpID?>&amp;mode=ajax_category&amp;tabAction=showInActive&amp;searchKey=<?=$searchKey?>');" id="viewInActive">
					    Inactive (<strong id="inactive_id"><?=$cnt_inactive?></strong>) 
					  </a>
					</span>
				</h2>
				
				<div class="section_small_details">					
						<? if ($isAdminLogged AND false == $is_searching) : ?>
							You can arrange <strong>groups</strong> and <strong>categories</strong> by dragging them.					
						<? else: ?>
							&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
						<? endif; ?>
						
						<div class="section_filter">
							<label>View Mode:</label>
							<select onchange="window.location.href = this.value;">
								<option value="/subgroups.php?gID=<?= $grpID ?>&amp;txtGrpName=<?=$searchKey?>"> Category View </option>
								<option value="/subgroups.php?gID=<?= $grpID ?>&amp;mode=list&amp;txtGrpName=<?=$searchKey?>"> List View </option>
							</select>
						</div>						
				</div>

				<div id="loader" class="widePanel content" style="display: none;">
					<p class="loading_message">
						<span id="imgLoading">
							<img alt="Loading" src="/images/loading.gif"/>
						</span>
						<em id="statusCaption">Loading data please wait...</em>
					</p>
				</div>
				
				<div id="content_to_hide">
					<form action="/subgroups.php" method="post">
						<? if ($isAdminLogged AND false == $is_searching) : ?>
							<input type="submit" value="Save" />
						<? endif; ?>
						
						<div class="content" id="subGroupsContent">			
								<?=	$subGroupsTemplate ?>			
						</div>
						
						<input type="hidden" name="mode" value="view" />
						<input type="hidden" name="gID" value="<?=$grpID?>" />
						<input type="hidden" id="tabAction" name="tabAction" value="<?=$tab_action?>" />
						<input type="hidden" name="action" value="custom_sort_groups_and_categories">
					</form>
				</div>
				
			</div>
		</div>
		
		<div id="narrow_column">
			<? if ((20 < $cnt_active OR 20 < $cnt_inactive OR $is_searching)): ?>
				<div class="section" id="quick_tasks2">
					<h2><span>Search</span></h2>
					<div class="content">
						<ul class="actions">
							<span>
								<input type="text" class="text" id="txtGrpName" name="txtGrpName" value="<?=$searchKey?>" />				
							</span>
							<span>		
								<input type="button" value="Search group" onclick="manager.search('/subgroups.php?gID=<?=$grpID?>&amp;mode=view')" />
							</span>
							<script type="text/javascript"><!--
								document.getElementById('txtGrpName').onkeypress = function(e){
									if(13 == e.keyCode) {
										manager.search('/subgroups.php?gID=<?=$grpID?>&amp;mode=view')
									}
								}
							//-->
							</script>
						</ul>
					</div>				
				</div>
			<? endif; ?>
			
			<? if ($isAdminLogged): ?>
								
				<div class="section" id="quick_tasks">
					
					<label>Group Tasks</label>
					
					<div class="content">
						
						<ul class="actions">
							<li>
								<a href="/group.php?mode=add&amp;pID=<?=$grpID?> " class="button" >
								  Add Group
								</a>
							</li>
							<li>
								<a href="/subgroupcategories.php?gID=<?=$grpID?>&amp;mode=viewAddCategory" class="button">
								  Add Category
								</a>
							</li>
						</ul>
					
					</div>	
				</div>
			<? endif; ?>
		</div>
</div>

<!--added by ianne - 11/20/2008 success message after confirm-->

<script type="text/javascript">
	<?if(isset($success)):?>
		CustomPopup.initPrompt("<?=$success?>");
		window.onload = function(){
			CustomPopup.createPopup();
		}
	<?endif;?>
</script>