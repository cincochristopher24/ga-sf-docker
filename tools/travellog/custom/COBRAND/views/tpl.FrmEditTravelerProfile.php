
<?php //SubNavigation::displayLinks(); 
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::includeDependentJs('/js/interactive.form.js');
	Template::includeDependentJs('/js/form.edit.profile.js');
	Template::includeDependentCss('/css/form.edit.profile.css');
	
	
		//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
		//Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");	
		//Template::includeDependentJs("/js/cpaint/lib/cpaint2.inc.compressed.js"); 
		//Template::includeDependentJs("/js/prototype.lite.js"); 
		Template::includeDependentJs("/min/f=js/prototype.js");
		Template::includeDependentJs("/js/profile.js"); 
	

//Template::includeDependentJs("/js/scriptaculous/scriptaculous.js?load=effects,builder,dragdrop");
/*Template::includeDependentJs (
	array(
		'/js/scriptaculous/scriptaculous.js',
		'/js/scriptaculous/effects.js',
		'/js/scriptaculous/builder.js',
		'/js/scriptaculous/dragdrop.js'
	)
);

//thumbnail cropper
Template::includeDependentJs("/js/imgcropper/lib/cropper.js"); 
Template::includeDependentCss('/js/imgcropper/css/cropper.css');
//



Template::includeDependentJs ( "/js/jquery-1.1.4.pack.js"           );
		
Template::includeDependentCss( "/min/f=css/Thickbox.css"                  );
Template::includeDependentCss( "/lytebox/lytebox.css"               );

Template::includeDependentJs (
				array(
					"/lytebox/lytebox.js",
					"/js/interface/interface.js",
					"/js/interface/source/idrag.js",
					"/js/interface/source/idrop.js",
					"/js/interface/source/isortables.js",
					"/js/ajaxfileupload.js",
					"/js/jeditable.js"
				));

Template::includeDependentJs ( "/js/yahooUI/carousel/yahoo-dom-event.js"           );
Template::includeDependentJs ( "/js/yahooUI/carousel/utilities.js"           );
Template::includeDependentJs ( "/js/yahooUI/carousel/dragdrop-min.js"           );
Template::includeDependentJs ( "/js/yahooUI/carousel/container_core-min.js"           );
Template::includeDependentJs ( "/js/yahooUI/carousel/carousel.js"           );

Template::includeDependentCss( "/js/yahooUI/carousel/carousel.css"               );
			

Template::includeDependentJs (array("/js/thickbox.js","/js/PhotoUILayout.js"));



$photo =<<<BOF
	<script type = "text/javascript">
		jQuery.noConflict();
		PhotoService = new mPhotoService('profile', $travelerID, $travelerID);
		PhotoService.setLayout(1); 
		//PhotoService.setisOwner(1); 
			
	</script>
BOF;

Template::includeDependent($photo);*/
	
	echo $profile->render(); 
	$subNavigation->show();
	
	 
	
	require_once('Class.FormHelpers.php');
?>


<div id="content_wrapper" class="layout_2">


	<div id="wide_column">
	
		<div id="edit_view" class="section">
			<h2><span>Edit Profile</span></h2>
			<div class="content">				
				<form action="" method="post" id="form" name="form" class="interactive_form" >
				<p class="help_text">
					Fields with  <span class="required">*</span> indicate required fields.
				</p>
				
				<!--
				<p class="help_text">
					Fields with  <span style="color:blue;">*</span> indicate required fields
					to qualify for  the contest.				
				</p>
				-->

				<?php if ( $errors ) : ?>
					<div class="error_notice">						
						<ul>
							<?php foreach ($errors as $error) : ?>
								<li><?= $error ?></li>
							<?php endforeach; ?>
						</ul>
					</div>
				<?php endif; ?>
				
				<ul class="form">
					<!--username field added by ianne 11/25/2008-->
					<li><a href="javascript:void(0)" onclick="window.open('/changepassword.php?action=entry','ChangePassword','width=500,height=650,navigation toolbar=disabled,left=200,top=200')">Change Account Settings</a></li>
					<li style="overflow:hidden;">
						<label for="txt_username"> Username: <?=$username?> </label><!--span class="feature_helper">Username: </span> <span class="required feature_helper">*</span><a class="featureMrk_sp feature_usertxt" href="javascript:void(0)" onmouseout="CustomPopup.removeBubble();" onmouseover="CustomPopup.fetchNewBubble('Customize your profile name!',this);"><span>New</span></a></label-->
						<!--<input type="text" id="txt_username" name="txt_username" value="<?//= $username  ?>" class="text feature_clr" />-->
					</li>
					
					<li>
						<label for="txt_email">Email: <?=$email?><!--span class="required">*</span--></label-->
						<!--input type="text" id="txt_email" name="txt_email" value="<?//= $email  ?>" class="text" /-->
					</li>					
					<li>&nbsp;</li>
					<li class="labeled">
						<fieldset>
							<legend><span>Name <span class="required">*</span></span></legend>
							<span class="labeled_input_wrapper">
								<input type="text" id="txt_firstname" name="txt_firstname" value="<?= $firstname  ?>" class="text" />
								<label for="txt_firstname">First Name </label>
							</span>
							<span class="labeled_input_wrapper">
								<input type="text" id="txt_lastname" name="txt_lastname" value="<?= $lastname  ?>" class="text" />
								<label for="txt_lastname">Last Name </label>
							</span>
							<div class="clear"></div>
						</fieldset>
					</li>
					
					<!--username field added by ianne 12/2/2008-->
					
					<!--li>
						<label for="txt_username">Username: <span class="required">*</span><a class="featureMrk_sp feature_usertxt" href="javascript:void(0)" onmouseout="CustomPopup.removeBubble();" onmouseover="CustomPopup.fetchNewBubble('Customize your profile name!',this);"><span>New</span></a></label>
						<input type="text" id="txt_username" name="txt_username" value="<?//= $username  ?>" class="text" />
					</li-->
					
					<!--li>
						<label for="txt_email">Email: <span class="required">*</span></label>
						<input type="text" id="txt_email" name="txt_email" value="<?//= $email  ?>" class="text" />
					</li-->
					
					<li class="labeled">
						<fieldset id="fset_birthday">
							<legend><span>Birthday:</span></legend>
					<? $now = getdate(); ?>	
							<span class="labeled_input_wrapper">							
								
								<input class="text" name="txtbday" id="txtbday" value="<? if($birthday	!= "0000-00-00") echo $bday['mday'];  ?>" size="2" maxlength="2" />
								<label for="txtbday">DD</label>
							</span>		
							
							<span class="labeled_input_wrapper">
								<select  name="txtbmonth" id="txtbmonth">
									<option value="0">[Select a Month]</option>
										<? for($month=1; $month<=12; $month++): ?>
											<option value="<?=$month?>"
											
											<? if($birthday	!= "0000-00-00" && $bday['mon'] ==$month)://=month is valid ?> 
												SELECTED
											<? endif; ?>
											>
											
											<?   
												 $today = getdate(); 
												 $m = getdate(mktime( 0, 0, 0, $month, 1, $today['year'] ));
											?>
											<?= $m['month']; ?>					
											</option>
										<? endfor; ?> 
								</select>
								<label for="txtbmonth">Month</label>
							</span>
							
							<span class="labeled_input_wrapper">						
								<input class="text" name="txtbyear" id="txtbyear" value="<? if($birthday	!= "0000-00-00") echo $bday['year'];  ?>" size="4" maxlength="4" />
								<label for="txtbyear">YYYY</label>
							</span>
						</fieldset>
						
					</li>
					
					
					
					<li>
					<label for="txt_gender">Gender:  <!--<span style="color:blue;">*</span>--></label>
					<select name="gender">
					
						<option value="0" <? if($gender == 0): ?>selected="selected"<? endif; ?>>-Select-</option>
						<option value="1" <? if($gender == 1): ?>selected="selected"<? endif; ?>>Male</option>
						<option value="2" <? if($gender == 2): ?>selected="selected"<? endif; ?>>Female</option>
					
					</select>
					
					
					</li>
					
					<!--li>
						<label for="travelertype">Traveler Type:  <span style="color:blue;">*</span>></label>
						
							<select name="travelertype">
									<option value="0">-Select Type-</option>
									<?/* foreach($travtype as $traveltype): ?>									
											<option value="<?= $traveltype->gettravelertypeid(); ?>" <? if($travelertypeid == $traveltype->gettravelertypeid()): ?>selected="selected"<? endif; ?>><?= $traveltype->gettravelertype(); ?></option>
									<? endforeach; */?>		
							</select>
							
					</li-->
					
					
					<li class="labeled">
							<fieldset>
						
									<legend><label for="countryID">Where are you originally from (hometown): </label></legend>

									<span class="labeled_input_wrapper">
										<select name="countryID" id="countryID" onchange="Profile.getCurrentCity(this.options[this.selectedIndex].value, <?=$travelerID?>,'cityID','subselecthomecity','homecity');">
											<option value="0">-Select Country-</option>
											<? foreach($countrylist as $htcountry): ?>
											<option value="<?= $htcountry->getcountryid() ?>" 
												<? if($countryID == $htcountry->getcountryid()): ?>SELECTED<? endif; ?>>
												<?= htmlspecialchars($htcountry->getName()) ?>
											</option>
											<? endforeach; ?>
										</select>
										<label for="countryID">Country</label>
									</span>
									
									<span class="labeled_input_wrapper">
										
										<!-- ajax sub select city container-->
										<div id="subselecthomecity">
										
										<select name="cityID" id="cityID">
											<option value="0"><?if($countryID ==91):?>-Select State-<?else:?>-Select City-<?endif;?></option>
											<?php foreach($htCities as $htCitys): ?>
												<option value="<?= $htCitys->getlocationID()?>" 
													<?php if($tmpcity): ?>							
														<? if($cityID == $htCitys->getlocationID()): ?>SELECTED <? endif; ?>
													<?php else: ?>
														<? if($cityID == $htCitys->getlocationID()): ?>SELECTED <? endif; ?>
													<?php endif; ?>
													>
													<?= $htCitys->getName() ?>
												</option>
											<? endforeach; ?>
										</select>
										</div>
										
										<label for="cityID" id="homecity"><?if($countryID ==91):?>State<?else:?>City<?endif;?></label>
									
									
									</span>
									
									<div class="clear"></div>
									
							</fieldset>
												
					</li>
					
					
					<li class="labeled">
						<fieldset>
							<legend><label for="currcountryID">Where are you currently living: </label></legend>
							<span class="labeled_input_wrapper">
								<select name="currcountryID" id="currcountryID" onchange="Profile.getCurrentCity(this.options[this.selectedIndex].value, <?=$travelerID?>,'currcityID','subselectcurrentcity','currlivingcity');">
									<option value="0">-Select Country-</option>
									<? foreach($countrylist as $currlivcountry): ?>
									<option value="<?= $currlivcountry->getcountryID()?>" 
										<? if($currcountryID == $currlivcountry->getcountryID()): ?>SELECTED<? endif; ?>>
										<?= htmlspecialchars($currlivcountry->getName()) ?>
									</option>
									<? endforeach; ?>
								</select>
								<label for="currcountryID">Country</label>
							</span>
							
							<span class="labeled_input_wrapper">
								
								<!-- ajax sub select city container-->
								<div id="subselectcurrentcity">
								
								<select name="currcityID" id="currcityID">
									<option value="0"><?if($currcountryID ==91):?>-Select State-<?else:?>-Select City-<?endif;?></option>
									
									<?php foreach($currlivCities as $currlivCitys): ?>
									<option value="<?= $currlivCitys->getlocationID()?>" 
										
										<?php if($tmpcity): ?>							
											<? if($currcityID == $currlivCitys->getlocationID()): ?>SELECTED <? endif; ?>
										<?php else: ?>
											<? if($currcityID == $currlivCitys->getlocationID()): ?>SELECTED <? endif; ?>
																			
										<?php endif; ?>
										 >
									<?= $currlivCitys->getName() ?>
									</option>
									<? endforeach; ?>
								</select>
								</div>
								
								<label for="currcityID" id="currlivingcity"><?if($currcountryID ==91):?>State<?else:?>City<?endif;?></label>
							</span>
							<div class="clear"></div>
						</fieldset>
					</li>
					
					<!--
					<li>
					<label for="txt_phone">Phone:</label> 
					<p class="supplement">(countrycode+areacode+phonenumber)</p>
					<input type="text" id="txt_phone" name="txt_phone" value="<?//= $phone  ?>" class="text" />
					</li>
					
					
					<li>
						<fieldset>
							<legend><label for="txtarea_address1">Address: </label> </legend>
							<ul><li>
							<textarea name="txtarea_address1" cols="50" rows="2"><?//= $address1 ?></textarea></li><li>						
							<textarea name="txtarea_address2" cols="50" rows="2"><?//= $address2 ?></textarea></li></ul>
						</fieldset>
					</li>
					-->
				</li>
				<li>
				<label for="txtarea_signature">Signature:</label>
				<textarea name="txtarea_signature" cols="50" rows="3"><?= $signature ?></textarea>
				</li>	
				</ul>
				
				
					
				<h2 <?if($personalquestionsanswered): ?> style="display:none;" <?endif;?> >Personal</h2>
					
					
				<ul <?if($personalquestionsanswered): ?> style="display:none;" <?endif;?> class="form">
					<?php
						// Edited BY: Cheryl Ivy Q. Go  22 February 2008
						/*
						<li>
						<label for="txtarea_interests">What is your traveling philosophy? <!--<span style="color:blue;">*</span>--></label>
						<textarea name="txtarea_interests" cols="50" rows="3"><?= $interests ?></textarea>
						</li>
						<!--
						<li>
						<label for="txtarea_shortbio">Would you rather travel to metro cities or third world rural areas? Please explain.<span style="color:blue;">*</span></label>
						<textarea name="txtarea_shortbio" cols="50" rows="6"><?= $shortbio  ?></textarea>
						</li>
						-->
						<li>
						<label for="txtarea_iculture">What is the most interesting culture you've experienced? <!--<span style="color:blue;">*</span>--></label>
						<textarea name="txtarea_iculture" cols="50" rows="6"><?= $iculture  ?></textarea>
						</li>
						*/
					?>
				
					<li>
						<fieldset>
						<legend><span>Have you travelled internationally?</span> </legend>

							<input name="rdoTraveled" id="rdoTraveled" value="1" type="radio" <? if($intrnltravel):?> checked="checked" <?endif; ?> /> Yes
							<br />
							<input name="rdoTraveled" id="rdoTraveled" value="0" type="radio" <? if(!$intrnltravel):?> checked="checked" <?endif; ?> /> No
							
						</fieldset>
					</li>
																						
					<li>					
							<fieldset class="choices" id="purpose" style="display:<?if($intrnltravel):?>block;<?else:?>none;<?endif; ?>">
									<legend><span>Specify the purpose of your international travel.</span></legend>
									<ul>
										<?foreach($purposetypes->getAllPurposeType() as $x => $tptype):?>											
												<li>
														<input 	<? $tmpptypes = in_array($tptype->gettpurposetypeid(),$travelerpurposetypes,FALSE)?>																		
																	 		<?if($tmpptypes):?>
																							checked="checked"																					
																			<?endif; ?> 
																	
																			name="chkPurpose[]" id="chkPurpose<?=$x; ?>" value="<?=$tptype->gettpurposetypeid(); ?>" 
																			type="checkbox" /> 
														
														<label for="chkPurpose<?=$x; ?>"><?=$tptype->gettptype(); ?></label>
												
												</li>
										<?endforeach;?>
									</ul>
									<div class="clear"></div>
							</fieldset>
					</li>
					
					<li>
						<fieldset class="choices" >
						<legend><span>Which of the following programs are you interested in? <!--<strong style="color:blue;">*</strong>--></span> </legend>
							<ul>
								<?foreach($programtypes->getAllProgramType() as $p => $pptype):?>	
									<li>
											<input 	<? $tmp_ptypes = in_array($pptype->getprogramid(),$tpprograminterested,FALSE)?>																		
																	 		<?if($tmp_ptypes):?>
																							checked="checked"																					
																			<?endif; ?> 
																	
																			name="chkProgram[]" id="chkProgram<?=$p; ?>" value="<?=$pptype->getprogramid(); ?>" 
																			type="checkbox" /> 
														
														<label for="chkProgram<?=$p; ?>"><?=$pptype->getprogram(); ?></label>
									</li>	
								<?endforeach;?>
								
							</ul>
						</fieldset>
					</li>
					
					<li>
						<fieldset class="choices">
							<legend><span>Do you expect to make an international trip in the next 2 years?</span></legend>
							<ul>
								<li><input name="rdotripnexttwoyears" id="rdotripnexttwoyears_1" value="1" type="radio" <? if($istripnexttwoyears):?> checked="checked" <?endif; ?>  /><label for="rdotripnexttwoyears_1">Yes</label></li>
								<li><input name="rdotripnexttwoyears" id="rdotripnexttwoyears_0" value="0" type="radio" <? if(!$istripnexttwoyears):?> checked="checked" <?endif; ?> /><label for="rdotripnexttwoyears_0">No</label></li>
							</ul>
						</fieldset>
					</li>					
					
					<li>
						<fieldset class="choices" >
						<legend><span>Countries I've Travelled To: </span> </legend>
							<ul class="many">
								<?foreach($countrylist as $p => $crlist):?>	
									<li>
											<input 	<? $tmp_countrytravelled = in_array($crlist->getcountryid(),$tpcountrytravelled,FALSE)?>																		
																	 		<?if($tmp_countrytravelled):?>
																							checked="checked"																					
																			<?endif; ?> 
																	
																			name="chkcountrytravelled[]" id="chkcountrytravelled<?=$p; ?>" value="<?=$crlist->getcountryid(); ?>_<?=$crlist->getLocationID(); ?>" 
																			type="checkbox" /> 
														
											<label for="chkcountrytravelled<?=$p; ?>"><?= htmlspecialchars($crlist->getname()); ?></label>
									</li>	
								<?endforeach;?>
								
							</ul>
						</fieldset>
					</li>				
					
					
					<li>
						<fieldset class="choices" >
						<legend><span>What countries are you interested in traveling to? <!--<strong style="color:blue;">*</strong>--></span> </legend>
							<ul class="many">
								<?foreach($countrylist as $p => $crlist):?>	
									<li>
											<input 	<? $tmp_countryinterested = in_array($crlist->getcountryid(),$tpcountry,FALSE)?>																		
																	 		<?if($tmp_countryinterested):?>
																							checked="checked"																					
																			<?endif; ?> 
																	
																			name="chkcountryinrested[]" id="chkcountryinrested<?=$p; ?>" value="<?=$crlist->getcountryid(); ?>" 
																			type="checkbox" /> 
											<label for="chkcountryinrested<?=$p; ?>">			
												<?= htmlspecialchars($crlist->getname()); ?>
											</label>
									</li>	
								<?endforeach;?>
								
							</ul>
						</fieldset>
					</li>				
					
					
					<li>
						<fieldset class="choices" >
						<legend><span>What do you prefer to do when you travel? <!--<strong style="color:blue;">*</strong>--></span> </legend>
							<ul>
								<?foreach($prefferedtypes->getAllPreferredTravelType() as $p => $preftype):?>	
									<li>
											<input 	<? $tmp_preftypes = in_array($preftype->getpreferredtypeid(),$tppreffered,FALSE)?>																		
																	 		<?if($tmp_preftypes):?>
																							checked="checked"																					
																			<?endif; ?> 
																	
																			name="chkPreferred[]" id="chkPreferred<?=$p?>" value="<?=$preftype->getpreferredtypeid(); ?>" 
																			type="checkbox"/> 
														
														<label for="chkPreferred<?=$p?>"><?=$preftype->getpreferredtype(); ?></label>
									</li>	
								<?endforeach;?>
									
									<li>
										<input type="checkbox" name="chkpreferother" <?if($chkpreferother):?>checked="checked" <?endif; ?> id="chkpreferother" onClick="Profile.uncheck(this.form.elements['chkPreferred[]'])" />
										<label for="chkpreferother">Other</label> 
										
											&nbsp;<input type="text" name="txtprefer" id="txtprefer" value="<?=$otherprefertravel;?>" size="35" style="display:<?if($chkpreferother):?>block;<?else:?>none;<?endif;?>"/> 										
									</li>
									
									
									
							</ul>
						</fieldset>
					</li>
					<li>
									
							<legend><span>Annual Income  <!--<strong style="color:blue;">*</strong>--></span></legend>
							<select name="cboIncome" id="cboIncome">
							
							<option value="0" selected="true">- Choose -</option>	
							<option value="1" <?if($income == 1): ?>selected<?endif; ?>>$5,000 below</option>	
							<option value="2" <?if($income == 2): ?>selected<?endif; ?>>$6,000 - $10,000</option>	
							<option value="3" <?if($income == 3): ?>selected<?endif; ?>>$11,000 - $15,000</option>	
							<option value="4" <?if($income == 4): ?>selected<?endif; ?>>$16,000 - $20,000</option>	
							<option value="5" <?if($income == 5): ?>selected<?endif; ?>>$21,000 - $25,000</option>	
							<option value="6" <?if($income == 6): ?>selected<?endif; ?>>$26,000 - $30,000</option>	
							<option value="7" <?if($income == 7): ?>selected<?endif; ?>>$31,000 - $40,000</option>	
							<option value="8" <?if($income == 8): ?>selected<?endif; ?>>$41,000 - $50,000</option>							
							<option value="9" <?if($income == 9): ?>selected<?endif; ?>>$51,000 - $75,000</option>	
							<option value="10" <?if($income == 10): ?>selected<?endif; ?>>$76,000 - $100,000</option>	
							<option value="11" <?if($income == 11): ?>selected<?endif; ?>>$100,000 or above</option>	
							
							</select>
				</ul>								
				<ul>
					<li class="actions">
						<input type="submit" class="submit" name="save" value="Save Profile" />
					</li>
				</ul>				
				<div class="clear"></div>
				
				
				
				
				
				
				</form>
			</div>
		
			<div class="foot"></div>
			
		</div>
	</div>
	
	<!-- edits by neri so as to avoid redundancy: 12-02-08 -->
	<!--div id="narrow_column">
	
		<div id="quick_tasks" class="section" >
			<h2><span>Profile Tasks</span></h2>
			<div class="content">
				<ul class="actions">
					<li>
						<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(1,1,'gallery')" class="button">Manage Profile Photos</a> 					
					</li>
					
					<li>
						<a href="javascript:void(0)" class="button" onclick="window.open('changepassword.php?action=entry','ChangePassword','width=500,height=355,toolbar=0,left=200,top=200')" >Change Password</a>
					</li>
					<li>
						<a href="privacy.php" class="button">Privacy Preferences</a>
					</li>
					<?/*if (false):?>
					<li>
						<a href="student-profile.php"class="button">	Virtual Advisor</a>					
					</li>
					<li>
						<a href="resume.php?action=view"class="button">	Edit/View Resume</a>					
					</li>
					<?endif;?>
					<li>
						<a href="/editgatravelbio.php"class="button"> <?=$fbAction*/?> My Travel Bio </a>
					</li>

				</ul>
			</div>
		
		<div class="foot"></div>
		
		</div>
		
	</div-->
	


	
	<div class="clear"></div>
</div>
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$('input[@name=rdoTraveled]').bind('change',function(){
				if($(this).val() == 1)
					$('#purpose').slideDown('slow');
				else
					$('#purpose').slideUp('slow');
			});
		});
	})(jQuery);
</script>