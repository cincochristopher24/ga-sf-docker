<?php
	ob_start();

	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');
	Template::setMainTemplateVar('layoutID', 'members');

	Template::includeDependentJs("/js/utils/Class.Request.js");
	Template::includeDependentCss('/css/assign_subgroup.css');

	$subNavigation->show();	

	ob_end_clean();
?>

<div class="area_wrapper" id="content_wrapper">
	<div class="area" id="top">
	<div id="wide_column" style = "float: none">
		<div class="member_box section" id="members_list" style = "width: 738px">
			<h1 class="big_header">
				<span>Assign Staff to <?=ucfirst($groupName)?> Subgroups</span>
			</h1>
			<ul class="tabs">
				<li class = "active">
					<a id = "search" onclick = "assignStaffManager._viewSearch('gId=<?=$gId?>')" href = "javascript:void(0)">Search</a>
				</li>
				<li>
					<a id = "nonmembers" onclick = "assignStaffManager._viewNonMembers('gId=<?=$gId?>')" href = "javascript:void(0)">Non GoAbroad.net Members</a>
				</li>
				<li>
					<a id = "pending" onclick = "assignStaffManager._viewPendingInvitations('gId=<?=$gId?>')" href = "javascript:void(0)">Pending Invitations</a>
				</li>
				</ul>
			<div id="statusPanel1" class="widePanel" style="display: none;">
				<p class="loading_message">
					<span id="imgLoading">
					<img alt="Loading" src="/images/loading.gif"/>
					</span>
					<em id="statusCaption">Loading data please wait...</em>
				</p>
			</div>
			<div class="member_box_content content"  id="member_box_content" >
				<?=$viewsearchstaff->render()?>
			</div>
		</div>
		
</div>
	<div class="clear"></div>
	</div>
</div>