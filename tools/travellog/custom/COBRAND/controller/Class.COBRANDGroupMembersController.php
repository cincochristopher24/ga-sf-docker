<?php
require_once('travellog/controller/Class.AbstractGroupMembersController.php');
class COBRANDGroupMembersController extends AbstractGroupMembersController{
	
	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		$this->file_factory->registerClass('GroupController' , array('path' => 'travellog/custom/COBRAND/controller/', 'file'=>'COBRANDGroupViewController'));
		
		$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
		
		/**
		 * @todo The following configurations should be placed in a different part since this is (almost) universal for all of COBRAND Controllers
		 */
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');
		
	}
	
	function performAction(){
		
		// set the config groupID as get variable, if not groupID is defined
		$gID = (isset($_GET['gID'])) ? $_GET['gID'] : 0;
		$gID = (isset($_POST['gID'])) ? $_POST['gID'] : $gID;
		$_POST['gID'] = (0 < $gID) ? $gID : $GLOBALS['CONFIG']->getGroupID() ;
		
		parent::performAction();
	}
}
?>