<?php
	require_once('travellog/controller/Class.AbstractAddressbookManagementController.php');
 	class COBRANDAddressbookManagementController extends AbstractAddressbookManagementController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
			$this->pageTitle = $GLOBALS['CONFIG']->getSiteName().' Addressbook';
			$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));		
 		}
 		
 		public function performAction(){
 			$_GET['groupID'] = $GLOBALS['CONFIG']->getGroupID();
 			parent::performAction();
 		}
 		
 	}
?>