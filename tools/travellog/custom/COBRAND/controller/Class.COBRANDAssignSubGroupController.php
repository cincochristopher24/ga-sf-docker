<?php
	require_once 'travellog/controller/Class.AbstractAssignSubGroupController.php';
	
	class COBRANDAssignSubGroupController extends AbstractAssignSubGroupController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerClass('Config');
	 		$CONFIG            = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $CONFIG;
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
