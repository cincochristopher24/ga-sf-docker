<?php
/*
 * Created on 11 12, 07
 * 
 * Author: J. LLano
 * Purpose: the default controller for "Photo" module for ganet cobrand sites
 * 
 */
 
 require_once('travellog/controller/Class.AbstractResourceFileController.php');
 
 class COBRANDResourceFileController extends AbstractResourceFileController {
 	
 	function __construct(){
 		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		$this->file_factory->registerClass('SubNavigation',array('path'=>'travellog/custom/COBRAND/model/','file'=>'SubNavigationCB'));
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');
	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 	
 	
 }
 
?>
