<?php
	 
require_once("travellog/controller/Class.AbstractSPSyncAccountController.php");
require_once("travellog/model/Class.Config.php");
 
class COBRANDSPSyncAccountController extends AbstractSPSyncAccountController{

	protected $config;
	protected $adminGroup;
	
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$this->file_factory->registerClass('TravelerCB', array("path" => "travellog/custom/COBRAND/model"));
		$this->file_factory->registerClass("AdminGroup", array("path" => "travellog/model/"));
		
 		$this->config = $this->file_factory->getClass('Config');
		$this->adminGroup = $this->file_factory->getClass("AdminGroup", array($this->config->getGroupID()));
	}
	
	protected function autoLogin($travelerID=0){
		if( !$travelerID ){
			return FALSE;
		}
		
		$traveler = $this->file_factory->getClass("TravelerCB",array($travelerID));
		$this->adminGroup = $this->file_factory->getClass("AdminGroup", array($this->config->getGroupID()));
		
		$this->file_factory->registerClass('COBRANDSPOAuthLoginController', array('path' => 'travellog/custom/COBRAND/controller/'));
		$controller = $this->file_factory->getClass("COBRANDSPOAuthLoginController");
		
		if( $this->adminGroup->isMember($traveler) ){
			$this->redirectAfterLogin = "TRAVELER_DASHBOARD";
			return $controller->startTravelerSession($travelerID);
		}elseif( $this->adminGroup->isInInviteList($traveler) ){
			$this->redirectAfterLogin = "/membership.php?gID=".$this->adminGroup->getID()."&mode=acceptinvite";
			return $controller->startTravelerSession($travelerID);
		}else{
			/*$groupAccess = $this->adminGroup->getGroupAccess();
			$directSignupAllowed = $this->adminGroup->directSignupAllowed();
			
			if( !$directSignupAllowed				//direct signup not allowed
				|| 4 == $groupAccess 				//group access CLOSE
				|| 3 == $groupAccess 				//group access INVITE ONLY
				|| 2 == $groupAccess ){				//group access OPEN_APPROVAL
				$this->redirectAfterLogin = "RESTRICTED";
				return FALSE;
			}else{
				$this->redirectAfterLogin = "RESTRICTED";
				return FALSE;
			}*/
			$this->redirectAfterLogin = "RESTRICTED";
			return FALSE;
		}
	}
}