<?php
require_once('travellog/controller/Class.AbstractArticleController.php');
class COBRANDArticleController extends AbstractArticleController{  
	
	function __construct(){ 
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'SubNavigationCB'));
		$this->file_factory->registerClass('CommentsView' , array('path' => 'travellog/custom/COBRAND/views/', 'file' => 'CommentsViewCB'));
		$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
		$this->file_factory->registerClass('Article'       , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'ArticleCB'));
	}
	
	function performAction(){
		try{
			$this->data         = array_merge($_GET, $_POST);
			$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'view';
			$this->__applyRules(); 
			parent::performAction();
		}catch(exception $e){
			header('location: /index.php');
		}
	}   
	
	protected function __applyRules(){
		global $CONFIG; 
		if( strtolower($this->data['action']) == 'view' ){
			if(!isset($this->data['articleID']) || !is_numeric($this->data['articleID'])){
				ToolMan::redirect('/journal.php'); 
				exit;
			}
			
			if( isset($this->data['articleID']) && $this->data['articleID'] ){
				$obj_article = $this->file_factory->getClass('Article', array($this->data['articleID']));
				$this->data['tID'] = $obj_article->getAuthorID();
			}else{
				ToolMan::redirect('/journal.php'); exit;
			}
			$this->data['gID'] = $CONFIG->getGroupID();  
			$obj_group         = $this->file_factory->getClass('AdminGroup');
			$obj_group->setGroupID($this->data['gID']);
			
			$travelerID = isset($_SESSION["travelerID"]) ? $_SESSION["travelerID"] : 0;
			$isStaffLogged = $obj_group->isStaffInNetwork($travelerID);
			$isAdminLogged = $CONFIG->isAdministrator($travelerID);
			
			if( $isStaffLogged || $isAdminLogged ){

			}else if( !$obj_group->isMemberOfGroupNetwork($this->data) 
					&& !(!$obj_group->isPendingMemberOfGroupNetwork($this->data['tID']) && !$isStaffLogged && !$isAdminLogged) ){
				$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['tID']));
				if( $obj_traveler->isAdvisor() && $this->obj_session->getLogin() )
					ToolMan::redirect('/journal.php?action=GroupJournals');
				elseif( !$obj_traveler->isAdvisor() && $this->obj_session->getLogin() )
					ToolMan::redirect('/journal.php?action=MyJournals');
				else
					ToolMan::redirect('/journal.php');
				exit; 
			}    
		} 
		else
			parent::__applyRules();		
	}
}
?>
