<?php
	require_once('travellog/controller/Class.AbstractPostController.php');
	
	class COBRANDPostController extends AbstractPostController {
	
 		function __construct() {
 			parent::__construct();
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerClass('Config');
			$file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
 			$GLOBALS['CONFIG'] = $file_factory->getClass('Config');
 		}
	
	}
	
