<?php
require_once('travellog/controller/Class.AbstractGroupTemplateController.php');

class COBRANDGroupTemplateController extends AbstractGroupTemplateController {
	public function __construct() {
		parent::__construct();
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
	}
}