<?php
/*
 * Created on 11 12, 07
 * 
 * Author: J. LLano
 * Purpose: the default controller for "Photo" module for ganet cobrand sites
 * 
 */
 
 require_once('travellog/controller/Class.AbstractPhotoAlbumController.php');
 
 
 class COBRANDPhotoAlbumController extends AbstractPhotoAlbumController {
 	
 	function __construct(){
 		
		parent::__construct();
			
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		$this->file_factory->registerClass('SubNavigation',array('path'=>'travellog/custom/COBRAND/model/','file'=>'SubNavigationCB'));
	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 	
 	
 }
 
?>
