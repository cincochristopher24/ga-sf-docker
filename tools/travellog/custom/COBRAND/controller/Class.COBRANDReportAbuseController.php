<?php
require_once('travellog/controller/Class.AbstractReportAbuseController.php');
class COBRANDReportAbuseController extends AbstractReportAbuseController{
	
	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		$this->file_factory->registerClass('Traveler' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'TravelerCB'));
		
	}
	
	function performAction(){
		
		parent::performAction();
	}
	
	function _sendAbuseReport($_params = array() ) {
		
		/*********************************************************************
		 * edits by neri: 11-04-08
		 * checks if server name is not local to avoid the error with smtp
		 * also added a referer once the message has been successfully sent
		 *********************************************************************/
		
		if (0 < $_params['tID']) {
			$traveler = $this->file_factory->getClass ('Traveler',array($_params['tID']));
			$referer = $traveler->getUserName()."'s Profile";
			
			// modified by neri to implement the friendly url: 11-07-08 
			$refererLink = '/'.$traveler->getUserName();
		}
		
		else if (0 < $_params['jeID']) {
			$journalEntry = $this->file_factory->getClass ('TravelLog',array($_params['jeID']));
			$referer = $journalEntry->getOwner()->getUserName()."'s Journal Entry";
			$refererLink = '/journal-entry.php?action=view&travellogID='.$journalEntry->getTravelLogID();
		}
		
		else if (0 < $_params['msgID']) {
			$message = $this->file_factory->getClass ('PersonalMessage',array($_params['msgID']));
			$referer = 'your messages.';
			$refererLink = '/messages.php?view&active=inbox&messageID='.$_params['msgID'];
		}
		
		$mail = new PHPMailer(); 
		$mail->IsSMTP(); 
		$mail->IsHTML(true);
		
		if (isset($this->loggedUser)):
			$mail->FromName = $this->loggedUser->getTravelerProfile()->getUserName(); 
			$mail->From 	= $this->loggedUser->getTravelerProfile()->getEmail();
		elseif(strlen($_POST['email'])):
			$mail->FromName = $_POST['email'];
			$mail->From 	= $_POST['email'];
		else:
			$mail->FromName = "Report Abuse";
			$mail->From 	= "abuse@goabroad.net";
		endif;	
		
		if (isset($_SERVER['REMOTE_ADDR']))
			$this->messageBody['details'] .= "<br /><br /> IP Address: " . $_SERVER['REMOTE_ADDR'] ;
		
		if (isset($_SERVER['HTTP_USER_AGENT']))
			$this->messageBody['details'] .= "<br /><br /> User Agent: " . $_SERVER['HTTP_USER_AGENT'] ;
			
			
		$mail->Subject = "Report Abuse";
		
		$mail->Body = $this->messageBody['txtAbuse'] . "<br /><br />" .  $this->messageBody['txtAbuseQ&A'] . $this->messageBody['details'] ;
		
		$mail->AddAddress("abuse@goabroad.net"); 
		$mail->AddBCC("gapdaphne.notifs@gmail.com"); 
		$mail->Send();
		
		$msgSent = 'Your report has successfully been submitted. We will review your report and take the ' .
				'necessary actions should we find a violation of the ' . $GLOBALS['CONFIG']->getSiteName() . ' Terms of Use. <br /> ' .
				'Thank you for helping us maintain the quality of the site.';
		
		$this->tpl->set('action', $_params['action']);
		$this->tpl->set('Msg', $msgSent);
		$this->tpl->set('referer', $referer);
		$this->tpl->set('refererLink', $refererLink);
		$this->tpl->out('tpl.ReportAbuse.php');
	}
	
}
?>
