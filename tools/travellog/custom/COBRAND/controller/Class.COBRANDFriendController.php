<?php
require_once('travellog/controller/Class.AbstractFriendController.php');
class COBRANDFriendController extends AbstractFriendController{
	
	function __construct(){
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		
		$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'SubNavigationCB'));
		$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
		$this->file_factory->registerClass('AdminGroup');
	}
	
	function performAction(){
		global $CONFIG;
		$this->data           = array_merge($_GET, $_POST);
		$this->data['gID']    = ( isset($this->data['gID']) )? $this->data['gID'] : $CONFIG->getGroupID();
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'View';
		$this->__applyRules();
		parent::performAction(); 
	}
		
	protected function __applyRules(){ 
		global $CONFIG;
		if( array_key_exists('tID', $this->data) ){
			$obj_group         = $this->file_factory->getClass('AdminGroup');
			$obj_group->setGroupID($CONFIG->getGroupID());
			
			$travelerID = isset($_SESSION["travelerID"]) ? $_SESSION["travelerID"] : 0;
			$isStaffLogged = $obj_group->isStaffInNetwork($travelerID);
			$isAdminLogged = $CONFIG->isAdministrator($travelerID);
			
			if( $isStaffLogged || $isAdminLogged ){

			}else if( !$obj_group->isMemberOfGroupNetwork($this->data) 
					&& !(!$obj_group->isPendingMemberOfGroupNetwork($this->data['tID']) && !$isStaffLogged && !$isAdminLogged) ){
				if( $this->obj_session->getLogin() )
					ToolMan::redirect('/friend.php');
				else
					ToolMan::redirect('/travelers.php');
				exit; 
			} 
		} 
		parent::__applyRules();   
	}
}
?>