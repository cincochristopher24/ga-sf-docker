<?php 

	require_once('travellog/controller/Class.AbstractDiscussionController.php');

	class COBRANDDiscussionController extends AbstractDiscussionController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('Config');
			$config = $file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $config;
		}
	}
?>