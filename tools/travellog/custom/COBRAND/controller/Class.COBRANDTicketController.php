<?php

	require_once('travellog/controller/Class.AbstractTicketController.php');
	
 	class COBRANDTicketController extends AbstractTicketController {
 		
 		function __construct() {
 			parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
			$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
 		}
 		
 		function performAction() {
 			parent::performAction();
 		}
 	}