<?php
/*
 * Created on 16-Nov-07
 *
 */
 
 require_once('travellog/controller/Class.AbstractPrivacyController.php');
  
 class COBRANDPrivacyController extends AbstractPrivacyController {
 	
 	function __construct(){
 	    
		parent::__construct();
 		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
 		$this->file_factory->registerClass('SubNavigation',array('path'=>'travellog/custom/COBRAND/model/','file'=>'SubNavigationCB'));
 		$this->cobrand = true;		// aded by neri: 01-15-09	
 	}
 	
 	function performAction(){
 		parent::performAction();
 	}
 }
?>
