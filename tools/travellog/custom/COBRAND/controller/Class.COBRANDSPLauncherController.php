<?php
	 
require_once("travellog/controller/Class.AbstractSPLauncherController.php");
require_once("travellog/model/Class.Config.php");
 
class COBRANDSPLauncherController extends AbstractSPLauncherController{

	private $config = NULL;
 	
	public function __construct(){
		$GLOBALS['CONFIG'] = new Config();
		$this->config = $GLOBALS['CONFIG'];
		parent::__construct();
	}
	
	protected function getDestinationServer(){
		return FALSE === strpos($_SERVER["SERVER_NAME"],".local") ? $this->config->getServerName() : $this->config->getServerName().".local";
	}
}