<?php
require_once('travellog/controller/Class.AbstractHomeStayController.php');
class COBRANDHomeStayController extends AbstractHomeStayController{
	
	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		$this->file_factory->registerClass('Traveler' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'TravelerCB'));
		
	}
	
	function performAction(){
		
		// set the config groupID as get variable, if not groupID is defined
		$gID = (isset($_GET['gID'])) ? $_GET['gID'] : 0;
		$gID = (isset($_POST['gID'])) ? $_POST['gID'] : $gID;
		$_POST['gID'] = (0 < $gID) ? $gID : $GLOBALS['CONFIG']->getGroupID() ;
		
		parent::performAction();
	}
}
?>
