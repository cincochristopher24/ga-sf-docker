<?php
/*
 * Created on 12 03, 08
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

	require_once('travellog/controller/Class.AbstractPostCardController.php');
	
 	class COBRANDPostCardController extends AbstractPostCardController {
 		
 		function __construct() {
 			parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
 		}
 		
 		function performAction(){
			$this->data           = array_merge($_GET, $_POST);
			$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'view'; 
			parent::__applyRules();
			parent::performAction(); 
		}
 	}