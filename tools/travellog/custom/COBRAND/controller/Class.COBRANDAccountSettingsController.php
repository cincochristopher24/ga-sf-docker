<?php

	require_once('travellog/controller/Class.AbstractAccountSettingsController.php');
	
	class COBRANDAccountSettingsController extends AbstractAccountSettingsController{

		function __construct(){

			parent::__construct();
			
			$this->file_factory->registerClass('Config');
			$this->config = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] 	=	$this->config;

			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));

			/**
			 * @todo The following configurations should be placed in a different part since this is (almost) universal for all of COBRAND Controllers
			 */
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');

		}

		function performAction(){
			parent::performAction();
		}
	}