<?php
 require_once('travellog/controller/Class.AbstractPassportController.php');
 
 class COBRANDPassportController extends AbstractPassportController {
 	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		
		// class overrides
		$this->file_factory->registerClass('Traveler',array('path'=>'travellog/custom/COBRAND/model/','file'=>'TravelerCB'));
		$this->file_factory->registerClass('SubNavigation',array('path'=>'travellog/custom/COBRAND/model/','file'=>'SubNavigationCB'));
		$this->file_factory->registerClass('MessagesPanel',array('path'=>'travellog/custom/COBRAND/views/','file'=>'MessagesPanelCB'));
		$this->file_factory->registerClassInitCall('MessagesPanel',array('func'=>'setGroupID','params' => array('groupID'=>$GLOBALS['CONFIG']->getGroupID())));
		// template overrides
		$this->file_factory->registerTemplate('IncMyGroups',array('path'=>'travellog/custom/COBRAND/views/','file'=>'IncMyGroups'));
		$this->file_factory->registerTemplate('IncProfileLink',array('path'=>'travellog/custom/COBRAND/views/','file'=>'IncProfileLink'));
		$this->file_factory->registerTemplate('IncInquirySummary',array('path'=>'travellog/custom/COBRAND/views/','file'=>'IncInquirySummary'));
		$this->file_factory->registerTemplate('IncCreateClientVars',array('path'=>'travellog/custom/COBRAND/views/','file'=>'IncCreateClientVars'));
		
 	}
 	
 	function performAction() {
 		parent::performAction();
 	}
 	
 	
 }
 
?>
