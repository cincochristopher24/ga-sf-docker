<?php
  require_once('travellog/controller/Class.AbstractSubGroupsController.php');
  require_once('travellog/model/Class.SubGroupFactory.php');
  require_once("travellog/model/Class.HelpText.php");

  class COBRANDSubGroupsController extends AbstractSubGroupsController{
		
	  function __construct(){
		
		  parent::__construct();
		
		  $this->file_factory->registerClass('Config');
		  $GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		  $this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
		  $this->tpl = $this->file_factory->getClass('Template');
		  $this->tpl->set_path("travellog/custom/COBRAND/views/");
	  }
	
	  function performAction(){
		
		  // set the config groupID as get variable, if not groupID is defined
		  $gID = (isset($_GET['gID'])) ? $_GET['gID'] : 0;
		  $gID = (isset($_POST['gID'])) ? $_POST['gID'] : $gID;
		  $_POST['gID'] = (0 < $gID) ? $gID : $GLOBALS['CONFIG']->getGroupID() ;
		
		  $params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'view';
		  $params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
		
			parent::performAction();
	  }
	  
	  /**
	   * Searches for a group.
	   * 
	   * @param string $keyword 	The keyword to be searched
	   * 
	   * @return void
	   */
	  function categorySearch($keyword = ''){
	  	require_once('travellog/action/subgroups/Class.CategorySearch.php');
	  	$params = array();
	  	$params['keyword'] = $keyword;
	  	$params['parent_group_id'] = $GLOBALS['CONFIG']->getGroupID();
	  	$action = new CategorySearch($params);
	  	$action->execute();
	  }
	  
	  /**
	   * Searches for a group.
	   * 
	   * @param string $keyword 	The keyword to be searched
	   * @param int 	 $page			The current page
	   * 
	   * @return void
	   */
	  function groupSearch($keyword = '', $page = 1){
	  	require_once('travellog/action/subgroups/Class.GroupSearch.php');
	  	$params = array();
	  	$params['keyword'] = $keyword;
	  	$params['page'] = $page;
	  	$params['parent_group_id'] = $GLOBALS['CONFIG']->getGroupID();
	  	$action = new GroupSearch($params);
	  	$action->execute();
	  }  
	  
	  /**
	   * Shows the subgroup index page.
	   * 
	   * @param string $category_id The ID of the category
	   * @param string $keyword 		The keyword to be searched
	   * @param int 	 $page 				The page number 
	   * 
	   * @return void
	   */
	  function ajaxSearch($category_id = 0, $keyword = '', $page = 1){
	  	require_once('travellog/action/subgroups/Class.AjaxSearch.php');
	  	
	  	$params = array();
	  	$params['category_id'] = $category_id;
	  	$params['keyword'] = $keyword;
	  	$params['page'] = $page;
	  	$action = new AjaxSearch($params);
	  	$action->execute();
	  }
	  
  }
?>