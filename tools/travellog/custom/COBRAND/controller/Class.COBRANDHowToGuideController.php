<?php

	require_once('travellog/controller/Class.AbstractHowToGuideController.php');
	
 	class COBRANDHowToGuideController extends AbstractHowToGuideController {
 		
 		function __construct() {
 			parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
 		}
 		
 		function performAction() {
 			parent::performAction();
 		}
 	}