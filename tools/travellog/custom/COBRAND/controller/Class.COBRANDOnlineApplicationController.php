<?php
	require_once('travellog/model/Class.SiteContext.php');
	$siteContext = SiteContext::getInstance();
	$name = $siteContext->getFolderName();

	require_once('travellog/controller/onlineApplications/'.$name.'_onlineApplication/Class.AbstractOnlineApplicationController.php');
 	class COBRANDOnlineApplicationController extends AbstractOnlineApplicationController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
		 	$this->pageTitle = $GLOBALS['CONFIG']->getSiteName().' Online Application';
			$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));		
 		}
 		
 		public function performAction(){
 			$_GET['groupID'] = $GLOBALS['CONFIG']->getGroupID();
 			parent::performAction();
 		}
 		
 	}
?>