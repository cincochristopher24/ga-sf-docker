<?php
require_once('travellog/controller/Class.AbstractGroupsController.php');
class COBRANDIndexController extends AbstractGroupsController{
	
	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		$this->file_factory->registerClass('GroupController' , array('path' => 'travellog/custom/COBRAND/controller/', 'file'=>'COBRANDGroupViewController'));
		
		$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));
		
		/**
		 * @todo The following configurations should be placed in a different part since this is (almost) universal for all of COBRAND Controllers
		 */
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');
	}
	
	function performAction(){
		
		// set the config groupID as get variable, if not groupID is defined
		$gID = (isset($_GET['gID'])) ? $_GET['gID'] : 0;
		$gID = (isset($_POST['gID'])) ? $_POST['gID'] : $gID;
		$_POST['gID'] = (0 < $gID) ? $gID : $GLOBALS['CONFIG']->getGroupID() ;
		
		if( $this->obj_session->getLogin() ){
			$_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($GLOBALS['CONFIG']->getGroupID()));
			$group = $_group[0];
			if( !$group->isMember(new Traveler($this->obj_session->get("travelerID"))) ){
				header("location: /membership.php?gID=".$GLOBALS['CONFIG']->getGroupID()."&mode=acceptinvite");
			}
		}
		
		parent::performAction();
	}
	
	function _viewGroup($_params){
		$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
		
		$this->_defineCommonAttributes($_params);
		
		// if this group is the group based on config, or a subgroup of that group
		$this->_allowAdminPrivileges($this->group->getGroupID() == $GLOBALS['CONFIG']->getGroupID() ||  ( $this->group->isSubGroup() && $this->group->getParent()->getGroupID() == $GLOBALS['CONFIG']->getGroupID() ) );		
		
		$obj_controller = $this->file_factory->getClass('GroupController');
	
		$obj_controller->setGroupDetails($this->group);
		$obj_controller->setIsLogged($this->isLogged);
		$obj_controller->setIsMemberLogged($this->isMemberLogged);
		$obj_controller->setIsAdminLogged($this->isAdminLogged);
		
		if (NULL!= $this->loggedUser)
			$obj_controller->setLoggedUser($this->loggedUser);
		
		$obj_controller->preparePreferences();
		$obj_controller->prepareGroupInfo();
		$obj_controller->prepareGroupMembers();	
		$obj_controller->prepareMainGroupInfo();
		$obj_controller->prepareGroupCalendar();
		$obj_controller->prepareMessageCenter();  // commented by nash - has an error in my local
		$obj_controller->prepareGroupDiscussions(); // added by chris
		$obj_controller->prepareLogin();
		
		$obj_controller->prepareGroupTags();
		
		$obj_controller->prepareGroupSurveys();
		$obj_controller->prepareGroupFiles();
		$obj_controller->prepareSubGroups();
		//$obj_controller->prepareCustomGroupSection();
		$obj_controller->prepareSubNavigation();
		$obj_controller->prepareGroupJournals();	
		$obj_controller->prepareGroupAlbums();	
		$obj_controller->prepareImportantLinks();			
		$obj_controller->prepareGroupArticles();
		$obj_controller->prepareGroupVideos();	// added by neri: 06-10-09		
		$obj_controller->prepareGroupSteps();	
		
		$obj_controller->viewGroup();
		
	}
	
}
?>
