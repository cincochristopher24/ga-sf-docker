<?php

require_once('travellog/controller/Class.AbstractTravelerEventController.php');
class COBRANDTravelerEventController extends AbstractTravelerEventController{
	
	function __construct(){
		
		parent::__construct();
		
		$this->file_factory->registerClass('Config');
 		$CONFIG            = $this->file_factory->getClass('Config');
		$GLOBALS['CONFIG'] = $CONFIG;
		
		$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'SubNavigationCB'));
		$this->file_factory->registerClass('Travel'       , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelCB'));
		$this->file_factory->registerClass('Traveler'     , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelerCB'));
		$this->file_factory->registerClass('TravelLog'    , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelLogCB'));
	}
	 
	function performAction(){
		global $CONFIG;
		$this->data            = array_merge($_GET, $_POST);
		$this->data['gID']     = ( isset($this->data['gID']) )? $this->data['gID'] : $CONFIG->getGroupID();
		$this->data['action']  = ( isset($this->data['action']))? $this->data['action'] : 'View';
		$this->data['context'] = ( isset($this->data['context']))? $this->data['context'] : 2;
		switch( $this->data['context'] ){
			case "0":
				$event_controller = $this->file_factory->getClass('ActivityController'); 
				$event_controller->performAction();	 
			break;
			case "1":
				$event_controller = $this->file_factory->getClass('GroupEventController');
				$event_controller->performAction();	 
			break;
			default:
				$this->__applyRules(); 		
				parent::performAction();  
		}
	}
		
	protected function __applyRules(){ 
		global $CONFIG;

		if( $this->data['gID'] != $CONFIG->getGroupID() ){
			$obj_group_factory = $this->file_factory->invokeStaticClass('GroupFactory');
			$obj_group         = $obj_group_factory->create( array($this->data['gID']) );
			if( get_class($obj_group[0]) == 'AdminGroup' && $obj_group[0]->getParentID() != $CONFIG->getGroupID() ){
				ToolMan::redirect('/calendar-event.php');  
				exit;
			}  
		}	
		parent::__applyRules();   
	}
}
?>
