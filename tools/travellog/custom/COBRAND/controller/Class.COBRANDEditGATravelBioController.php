<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 27 November 2007
	 * Class.COBRANDEditGATravelBioController.php
	 */

	require_once 'travellog/controller/Class.AbstractEditGATravelBioController.php';

	class COBRANDEditGATravelBioController extends AbstractEditGATravelBioController{
		function __construct(){
			parent::__construct();

			$this->file_factory->registerClass('Config');
	 		$CONFIG            = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $CONFIG;
		}

		function performAction(){
			parent::performAction();
		}		
	}
?>