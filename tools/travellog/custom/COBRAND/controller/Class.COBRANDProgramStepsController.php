<?php
require_once('travellog/controller/Class.AbstractProgramStepsController.php');

class COBRANDProgramStepsController extends AbstractProgramStepsController{
	
	function __construct(){
		
		parent::__construct();
	
		$this->file_factory->registerClass('Config');
		$GLOBALS['CONFIG'] 	=	$this->file_factory->getClass('Config');
		
		$this->file_factory->registerClass('Traveler', array('path' => 'travellog/custom/COBRAND/model', 'file'=>'TravelerCB'));
		
	}
	
	function performAction(){
		
		parent::performAction();
	}
}
?>