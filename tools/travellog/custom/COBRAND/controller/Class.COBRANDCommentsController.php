<?php
/*
 * Created on 11 15, 07
 * 
 * Author:K. Gordo
 * Purpose: Default Cobrand controller for comments 'module'
 * 
 */
 
 	require_once('travellog/controller/Class.AbstractCommentsController.php');
 
 	class COBRANDCommentsController extends AbstractCommentsController {
 		function __construct(){
		
 			parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$CONFIG            = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $CONFIG;
		
	 		$this->file_factory->registerClass('CommentsView', array('path'=>'travellog/custom/COBRAND/views/','file'=>'CommentsViewCB'));
	 		$this->file_factory->registerClassInitCall('CommentsView', array('func'=>'setGroupId',array($CONFIG->getGroupID()))); 
	 	}

 		function performAction(){
	 		parent::performAction();
	 	}
 	} 
?>