<?php
	/*
	 * Class.COBRANDActivationSupportController.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
	
	require_once("travellog/controller/Class.AbstractActivationSupportController.php");
	
	class COBRANDActivationSupportController extends AbstractActivationSupportController{
		
		function __construct(){
			
			parent::__construct();
			$this->file_factory->registerClass('Config');
			$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass("GroupFactory", array("path" => "travellog/model"));
			
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');
				
		}
		
		function validate(){
			
			$validator = $this->file_factory->getClass("Validator");
			$asData = $this->asHelper->readASData();

			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create($GLOBALS['CONFIG']->getGroupID())->isInEmailInviteList($asData["email"]);

			if( !strlen($asData["email"]) )
				$asData["arrError"][] = constants::EMPTY_EMAIL_ADDRESS;
			elseif( !$validator->isEmail($asData["email"]) )	
				$asData["arrError"][] = constants::INVALID_EMAIL_FORMAT;
			elseif( !$asData["travelerID"] = $mGroup[0] )
				$asData["arrError"][] = constants::TRAVELER_DOES_NOT_EXIST;	
			if( !strlen($asData["message"]) && constants::HELP_ACTIVATE == $this->asHelper->getAction() )	
				$asData["arrError"][] = constants::EMPTY_MESSAGE;
			if( !($this->file_factory->invokeStaticClass("Captcha","validate",array($asData["secCode"],$asData["code"])) ))
				$asData["arrError"][] = constants::SECURITY_CODE_MISMATCH;
			return $asData;			
		}		
	}  
?>