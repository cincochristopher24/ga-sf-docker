<?php 
	
require_once('travellog/controller/Class.AbstractRetrieveMapController.php');

/**
 * RetrieveMap cobrand class
 * 
 * @version 1.0 September 7, 2009 
 * @package apps.travellog.custom.COBRAND.controller
 */
class COBRANDRetrieveMapController extends AbstractRetrieveMapController {
	
	/**
	 * Constructs a new controller for cobrand.
	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
	    $file_factory->registerClass('Config');
		$file_factory->registerClass('TravelLog'    , array('path' => 'travellog/custom/COBRAND/model/', 'file' => 'TravelLogCB'));		
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/COBRAND/views'));
		$file_factory->setPath('CSS', '/custom/COBRAND/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');
		
	  $GLOBALS['CONFIG'] 	=	$file_factory->getClass('Config');
	  
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		  
	}
	
	function performAction(){
		$this->data         = array_merge($_GET, $_POST);
		parent::performAction();
	}
	
}