<?php
	/*
	 * Class.COBRANDForgotPasswordController.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	
	require_once("travellog/controller/Class.AbstractForgotPasswordController.php");
	
	
	class COBRANDForgotPasswordController extends AbstractForgotPasswordController{
		
		/*********************************************************************************
		 * edits of neri:
		 * 		replaced cobrandName with cobrandEmailName and cobrandSiteName:	01-07-09
		 * 		set cobrand to true in constructor:								01-06-09
		 * 		get cobrand group name and added it to fpData in validate():	12-22-08
		 *********************************************************************************/
		
		function __construct(){
			
			parent::__construct();
			$this->file_factory->registerClass('Config');
			$this->file_factory->registerClass("TravelerCB", array("path" => "travellog/custom/COBRAND/model"));
			$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');
			
			$this->cobrand = true;
		}
		
		function validate(){
			$fpData = $this->fpHelper->readForgotpasswordData();
			
			if( !strlen($fpData["email"]) )
				$fpData["arrError"][] = constants::EMPTY_EMAIL_ADDRESS;
			elseif( !$fpData["travelerID"] = $this->file_factory->invokeStaticClass("TravelerCB","exist",array($fpData["email"])) )
				$fpData["arrError"][] = constants::TRAVELER_DOES_NOT_EXIST;
			if( !$this->file_factory->invokeStaticClass("Captcha","validate",array($fpData["code"],$fpData["secCode"])))
				$fpData["arrError"][] = constants::SECURITY_CODE_MISMATCH;
			
			$fpData['cobrandEmailName'] = $GLOBALS['CONFIG']->getEmailName();
			$fpData['cobrandSiteName'] = $GLOBALS['CONFIG']->getSiteName();		
			return $fpData;
		}
		
	}   
?>
