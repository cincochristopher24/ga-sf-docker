<?php
require_once('travellog/controller/Class.AbstractSubGroupCategoriesController.php');
 
 	class COBRANDSubGroupCategoriesController extends AbstractSubGroupCategoriesController {
 		function __construct(){
		
 			parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$CONFIG            = $this->file_factory->getClass('Config');
			$GLOBALS['CONFIG'] = $CONFIG;
		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/COBRAND/views'));
			$this->file_factory->setPath('CSS', '/custom/COBRAND/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/COBRAND/views/');
	 	}

 		function performAction(){
	 		parent::performAction();
	 	}
 	} 
?>