<?php
	require_once('travellog/controller/Class.AbstractSurveyFormController.php');
 	class COBRANDSurveyFormController extends AbstractSurveyFormController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerClass('Config');
	 		$GLOBALS['CONFIG'] = $this->file_factory->getClass('Config');
			$this->file_factory->registerClass('SubNavigation' , array('path' => 'travellog/custom/COBRAND/model/', 'file'=>'SubNavigationCB'));			
 		}
 	}
?>