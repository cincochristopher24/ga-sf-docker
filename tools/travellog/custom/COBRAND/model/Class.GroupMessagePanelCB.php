<?php
	require_once('travellog/model/Class.GroupMessagePanel.php');
	require_once('travellog/custom/COBRAND/views/Class.PokesViewCB.php');

	class GroupMessagePanelCB extends GroupMessagePanel{
	
		/**
	 	 * display all messages side by side sorted by date
	 	 * 	 Bulletin: show from current date, Personal: show unread, Inquiries: showall 
	 	 */
	 	protected function renderAll(){
	 		/****************************************************/
			/* Last Edited By: Cheryl Ivy Q. Go		28 May 2008	*/
			/* Purpose: Filter new messages -- display only new */
			/*			messages in NEW Tab of Message Center	*/
			/****************************************************/

			require_once('travellog/model/Class.MessageBoard.php');
			require_once('travellog/model/Class.SessionManager.php');

	 		$filter = null;
			$messages = array();
	 		$ms = new MessageSpace();

			$messageBoard = new MessageBoard();
			$obj_session = SessionManager::getInstance();

			// START: get bulletins
			if (TRUE == $this->mPrivacypref->getViewBulletin()){
		 		$msgs = $messageBoard->getBulletins(array(
		 			'owner' => $this->context,
		 			'viewer'=> new Traveler($obj_session->get('travelerID'))
		 		));

				foreach ($msgs as $indMessage){
					if ($this->isAdminLogged){
						if (!$indMessage->isRead())
							$messages[] = $indMessage;
					}
					else
						$messages[] = $indMessage;
				}
			}	
			// END: get bulletins
 		
	 		// START: get alerts
	 		if (TRUE == $this->isMemberLogged || TRUE == $this->mPrivacypref->getViewAlerts() ) { 			
		 		$alerts = $ms->getAlerts($this->context->getSendableID());
	 			$messages = array_merge($messages, $alerts);
	 		}
			// END: get alerts
		
			/****************** START: GET GREETINGS, SALUTATIONS AND COMMENTS ********************/
			if ($this->isAdminLogged){
				$tempMessage = array();

				if (!strcasecmp('AdminGroup', get_class($this->context))){				
					$tlogs = Travellog::getTravellogs($this->context);

					// get journal-entry comments
					if (null != $tlogs) {
				 	 	foreach ($tlogs as $tlog){

							$tLogComments = $tlog->getComments(null, true);
							if ($tLogComments != null)
				 	 			if (0 < count($tLogComments))
				 	 				$tempMessage = (0 < count($tempMessage)) ? array_merge($tempMessage, $tLogComments) : $tLogComments;

							$photos = $tlog->getPhotos();
							// get comments for journal-entry photos
							if (null != $photos){
								foreach ($photos as $photo){
									$photoComments = $photo->getComments(null, true);
									if (0 < count($photoComments))
										$tempMessage = (0 < count($tempMessage)) ? array_merge($tempMessage, $photoComments) : $photoComments;
								}
							}
				 	 	}
			 	 	}

					// get comments for photos in photo album
					$album = $this-> context-> getPhotoAlbums();
					if (null != $album){
						foreach ($album as $indAlbum){
							$photos = $indAlbum->getPhotos();
							if (null != $photos){
								foreach ($photos as $photo){
									$albumPhotoComments = $photo->getComments(null, true);
									if (0 < count($albumPhotoComments))
										$tempMessage = (0 < count($tempMessage)) ? array_merge($tempMessage, $albumPhotoComments) : $albumPhotoComments;
								}
							}
						}
					}
				}
				else{
					// get comments for club photos
					$photos = $this->context->getPhotos();
			 	 	if (null != $photos) {
				 	 	foreach ($photos as $photo){
				 	 		$clubPhotoComments = $photo->getComments(null, true);
							if (0 < count($clubPhotoComments))
					 			$tempMessage = (0 < count($tempMessage)) ? array_merge($tempMessage, $clubPhotoComments) : $clubPhotoComments;
				 	 	}
			 	 	}
				}			
				foreach ($tempMessage as $indMessage){
					if (!$indMessage->getIsRead())
						$messages[] = $indMessage;
				}
			}
			/****************** END: GET GREETINGS, SALUTATIONS AND COMMENTS **********************/

	 		if (count($messages)){
	 			// sort the messages by date
		 		usort($messages,array('Message','orderByDate'));
		 		// reverse it for descending order
	 			$messages = array_reverse($messages);
	 		}

	 		if (count($messages) > $this->maxMessages)
	 			$messages = array_slice($messages,0,$this->maxMessages);

			if (0 < $this->context->getGroupID()){
		 		$grpMembers = $this-> context-> getMembers();
		 		foreach ($grpMembers as $member){
		 			$members[] = $member->getTravelerID();	
		 		}
			}

	 		$template = new Template();
	 		$template->set('messages', $messages);
	 		$template->set('messType', MessagesPanel::$ALL);		
	 		$template->set('context', $this->context);
	 		$template->set('groupId', $this->context->getGroupID());
			$template->set('members', (isset($members)) ? $members : array());
			$template->set('ht',new HelpText() );		
	 		$template->out('travellog/custom/COBRAND/views/tpl.GroupMessagesPanel.php'); 				
	 	}
 	
	
		/**
	 	 * display all News
	 	 */
		/**
		// remove news
	 	protected function renderNews(){
 		
	 		$group = $this->context;
	 		$filter = null;
 		
			if ( FALSE == $this->isMemberLogged ){
		
				$cond = new Condition;
				$cond->setAttributeName("displaypublic");
				$cond->setOperation(FilterOp::$EQUAL);
				$cond->setValue(1);
			
				$filter = new FilterCriteria2();
				$filter->addCondition($cond);	
				$filter->addBooleanOp("AND");									
			}	
	
			$messages = $group->getNews($filter);
		
			if (isset($_SESSION['travelerID']))
	 			// if member is logged in, and alerts privacy is shown to members only
	 			$messages = $this->removeMessageOlderThanMe($messages);
 				
			if (count($messages) && ( count($messages) > $this->maxMessages || $this->isAdminLogged) ) 
				$messages = array_slice($messages,0,$this->maxMessages); 	
		
			if (count($messages)){
				// sort the messages by date
		 		usort($messages,array('Message','orderByDate'));
		 		// reverse it for descending order
		 		$messages = array_reverse($messages);
			}
		
			if (Group::ADMIN == $group->getDiscriminator()) {	
		
				$template = new Template();
		 		$template->set('messages',$messages);
		 		$template->set('grpID',$this->context->getGroupID());
		 		$template->set('messType',self::$NEWS);
		 		$template->set('context',$this->context);
				$template->set('ht',new HelpText() );		
		 		$template->out('travellog/custom/COBRAND/views/tpl.GroupMessagesPanel.php');
			}
 		
	 		$this->msgs = 	$messages;		
	 	}
	 	*/
 	
	 	/**
	 	 * display all Bulletin message type
	 	 */
	 	protected function renderBulletin(){
	 		$ms = new MessageSpace();
	 		// get bulletins
	 		$messages = $ms->getBulletins($this->context->getSendableID()); 
 		
	 		if (isset($_SESSION['travelerID']) && FALSE == $this->mPrivacypref->getViewBulletin()) 
				// if member is logged in, and bulletin privacy is shown to members only, strip off old bulletins
				$messages = $this->removeMessageOlderThanMe($messages);
 			
	 		if (count($messages) > $this->maxMessages)
	 			$messages = array_slice($messages,0,$this->maxMessages); 	
 		
	 		if (count($messages)){
		 		// sort the messages by date
		 		usort($messages,array('Message','orderByDate'));
		 		// reverse it for descending order
		 		$messages = array_reverse($messages);
	 		}
 	
	 		$template = new Template();
	 		$template->set('messages',$messages);
	 		$template->set('grpID',$this->context->getGroupID());
	 		$template->set('context',$this->context);
	 		$template->set('messType',MessagesPanel::$BULLETIN);
	 		$template->set('ht',new HelpText() );		
	 		$template->out('travellog/custom/COBRAND/views/tpl.GroupMessagesPanel.php');
 		
	 		$this->msgs = 	$messages;	
	 	}
 	
 	
	 	/**
	 	 * display all Alerts message type
	 	 */
		/**
		 //remove alerts
	 	protected function renderAlerts(){
	 		$ms = new MessageSpace();
	 		// get bulletins
	 		$messages = $ms->getAlerts($this->context->getSendableID()); 
 		
	 		if (isset($_SESSION['travelerID']) && FALSE == $this->mPrivacypref->getViewAlerts() )
	 			// if member is logged in, and alerts privacy is shown to members only
	 			$messages = $this->removeMessageOlderThanMe($messages);
 			
	 		if ( count($messages) && ( count($messages) > $this->maxMessages || $this->isAdminLogged) )
	 			$messages = array_slice($messages,0,$this->maxMessages); 	
 		
	 		if (count($messages)){
		 		// sort the messages by date
		 		usort($messages,array('Message','orderByDate'));
		 		// reverse it for descending order
		 		$messages = array_reverse($messages);
	 		}
 	
	 		$template = new Template();
	 		$template->set('messages',$messages);
	 		$template->set('grpID',$this->context->getGroupID());
	 		$template->set('messType',self::$ALERTS);
	 		$template->set('context',$this->context);
			$template->set('ht',new HelpText() );		
	 		$template->out('travellog/custom/COBRAND/views/tpl.GroupMessagesPanel.php');
 		
	 		$this->msgs = 	$messages;		
	 	}
		*/
	
	 	protected function renderGreetings(){
			$template = new Template();
			$messages = null;
			$members = array();

			if ($this->isAdminLogged){
				if (!strcasecmp('AdminGroup', get_class($this->context))){
					$tlogs = Travellog::getTravellogs($this->context);

					// get journal-entry comments
					if (null != $tlogs) {
				 	 	foreach ($tlogs as $tlog){
				 	 		$tLogComments = $tlog->getComments(null, true);
							if ($tLogComments != null)
								if (0 < count($tLogComments))
									$messages = (0 < count($messages)) ? array_merge($messages, $tLogComments) : $tLogComments;

							$photos = $tlog->getPhotos();
							// get comments for journal-entry photos
							if (null != $photos){
								foreach ($photos as $photo){
									$photoComments = $photo->getComments(null, true);
									if (0 < count($photoComments))
											$messages = (0 < count($messages)) ? array_merge($messages, $photoComments) : $photoComments;
								}
							}
				 	 	}
			 	 	}

					// get comments for photos in photo album
					$album = $this->context->getPhotoAlbums();
					if (null != $album){
						foreach ($album as $indAlbum){
							$photos = $indAlbum->getPhotos();
							if (null != $photos){
								foreach ($photos as $photo){
									$albumPhotoComments = $photo->getComments(null, true);
									if (0 < count($albumPhotoComments))
										$messages = (0 < count($messages)) ? array_merge($messages, $albumPhotoComments) : $albumPhotoComments;
								}
							}
						}
					}
				}
				else{
					// get comments for club photos
					$photos = $this->context->getPhotos();
			 	 	if (null != $photos) {
				 	 	foreach ($photos as $photo){
				 	 		$clubPhotoComments = $photo->getComments(null, true);
							if (0 < count($clubPhotoComments))
								$messages = (0 < count($messages)) ? array_merge($messages, $clubPhotoComments) : $clubPhotoComments;
				 	 	}
			 	 	}
				}

				if (0 < count($messages)){
					// sort the messages by date
			 		usort($messages, array('Comment','orderByDate'));
			 		// reverse it for descending order
			 		$messages = array_reverse($messages);
				}
			}

			if (0 < $this->context->getGroupID()){
		 		$grpMembers = $this-> context-> getMembers();
		 		foreach ($grpMembers as $member){
		 			$members[] = $member->getTravelerID();	
		 		}
			}

	 		$template->set('messages', $messages);
	 		$template->set('messType', MessagesPanel::$GREET);
			$template->set('groupId', $this->context->getGroupID());
			$template->set('members', (0 < count($members)) ? $members : array());
			$template->set('ht',new HelpText() );		
	 		$template->out('travellog/custom/COBRAND/views/tpl.GroupMessagesPanel.php');
		}

	 	/**
	 	  * function called if Context is roup object wherein old messages (bulletins, alerts) older than the date he joined group
	 	  * 	will not be displayed 
	 	  *  @param array $messages		array of Bulletin / AlertMessages
	 	  *  note: messages must be ordered by date descending so that the array will not be looped to the end
	 	  */ 
	 	 protected function removeMessageOlderThanMe($messages) {
 	 	
	 	 	if ('AdminGroup' == get_class($this->context) || 'FunGroup' == get_class($this->context)) {
 	 		
	 	 		$traveler = new Traveler($_SESSION['travelerID']);
	 	 		$dateJoined = $this->context->getMembershipDate($traveler);
 	 	   
	 	 		$offset = 0;
	 	 		$size = count($messages);
 	 		
	 	 		if ($size){
		 	 		// sort the messages by date
			 		usort($messages,array('Message','orderByDate'));
			 		// reverse it for descending order
			 		$messages = array_reverse($messages);
	 		
		 			$newmessages = array();
		 			$isNews = false;
	 			
		 	 		foreach($messages as $message){ 
			 			if ('News' == get_class($message)){ 
		 	 				if ($dateJoined < $message->getCreated() || 1 == $message->getDisplayPublic()){
		 	 					$newmessages[] = $message;
		 	 				}
		 	 				$isNews = true;			
			 			}
		 			
			 			else {
			 				if ($dateJoined > $message->getCreated()) {
			 	 				$messages = array_slice($messages,0,$offset);	 
			 					break;		 					
			 	 			} 	 
			 			}
		 			
		 	 			$offset++;
		 	 		}	
 	 		
		 	 		if ($isNews) 
		 	 			$messages = $newmessages;
	 	 	
	 	 		}
 	 				
	 	 		return $messages;
	 	 	} 	 	
		}
	}
?>