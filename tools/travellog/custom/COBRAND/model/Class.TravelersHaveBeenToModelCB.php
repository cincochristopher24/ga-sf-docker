<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/custom/COBRAND/model/Class.TravelersHaveBeenToCB.php');
require_once('travellog/model/Class.IModel.php');
require_once("Class.Criteria2.php");
class TravelersHaveBeenToModelCB implements IModel{
	
	function GetFilterLists(&$props){
		$props['obj_criteria'] = new Criteria2;
		if( $props['is_login'] ) $props['obj_criteria']->mustNotBeEqual('travelerID', $props['travelerID']);
		
		$props['obj_criteria']->setGroupBy('travelerID');
		$props['obj_criteria']->setOrderBy('ranking, travelerID');
		$props['obj_criteria']->setLimit($props['offset'], $props['rows']); 
		$obj_traveler = new TravelersHaveBeenToCB;	
		$props['travelers'] = $obj_traveler->GetFilterList($props);
		$props['travelers_count'] = $obj_traveler->getTotalRecords();
		// neri added locationID as parameter for querystring in Paging class: 11-28-08 
		$props['obj_paging']   = new Paging( $obj_traveler->getTotalRecords(), $props['page'],'action=HaveBeenTo&locationID='.$props['locationID'], $props['rows'] );
		
		//$props['obj_paging']   = new Paging        ( $obj_traveler->getTotalRecords(), $props['page'],'action=HaveBeenTo', $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                                    );
		$props['view_type']    = 8;
	}
	
	function GetTravelersCountryWithPastTravels(){
		$obj_traveler = new TravelersHaveBeenToCB;
		return $obj_traveler->GetTravelersCountryWithPastTravels();	
	}
	
	function GetTravelersCountryWithPastTravelsJSON(){
		$obj_traveler = new TravelersHaveBeenToCB;
		return $obj_traveler->GetTravelersCountryWithPastTravelsJSON();	
	}
}
?>
