<?php
class TravelerLocationCountCB{
	function GetFilterList(&$props){
		global $CONFIG;
		
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn = new Connection;
		$rs1  = new Recordset( $conn );
		$rs2  = clone($rs1);
			
		/* neri modified query so as not to display users who have blocked 
				the logged traveler or those who are suspended: 11-28-08 */
		
		/*
		 	$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS tblTraveler.travelerID ' .
						'FROM tblTraveler, tblGrouptoTraveler ' .
						'WHERE ' .
						'active > 0 ' .
						'AND tblTraveler.travelerID      = tblGrouptoTraveler.travelerID ' .
						'AND tblGrouptoTraveler.groupID  = %d ' .
						'AND tblTraveler.travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
						'AND currlocationID IN (%s) ' .
						'LIMIT 1'
						,$CONFIG->getGroupID()
						,$props['locationIDList'] 
					);
		 */
			
		/*$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS tblTraveler.travelerID ' .
						'FROM tblTraveler, tblGrouptoTraveler ' .
						'WHERE ' .
						'active > 0 ' .
						'AND tblTraveler.isSuspended 	 = 0 ' .
						'AND tblTraveler.travelerID      = tblGrouptoTraveler.travelerID ' .
						'AND tblGrouptoTraveler.groupID  = %d ' .
						'AND tblTraveler.travelerID NOT IN (SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) ' .
						'AND tblTraveler.travelerID NOT IN (SELECT travelerID FROM tblBlockedUsers WHERE tblBlockedUsers.userID = '.$props['travelerID'].') ' .
						'AND currlocationID IN (%s) ' .
						'LIMIT 1'
						,$CONFIG->getGroupID()
						,$props['locationIDList'] 
					);*/
		$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS tblTraveler.travelerID ' .
						'FROM tblTraveler, tblGrouptoTraveler ' .
						'WHERE ' .
						'active > 0 ' .
						'AND tblTraveler.isSuspended 	 = 0 ' .
						'AND tblTraveler.travelerID      = tblGrouptoTraveler.travelerID ' .
						'AND tblGrouptoTraveler.groupID  = %d ' .
						'AND tblTraveler.travelerID NOT IN (SELECT travelerID FROM tblBlockedUsers WHERE tblBlockedUsers.userID = '.$props['travelerID'].') ' .
						'AND currlocationID IN (%s) ' .
						'LIMIT 1'
						,$CONFIG->getGroupID()
						,$props['locationIDList'] 
					);
		
		$rs1->Execute($sql1);
		
		$sql2 = 'SELECT FOUND_ROWS() as totalrecords';
		$rs2->Execute($sql2);
				
		$props['current_location_count'] = $rs2->Result(0, 'totalrecords'); 
	}
}
?>
