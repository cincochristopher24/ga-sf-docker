<?php
require_once('travellog/model/Class.TravelerSearchCriteria2.php');
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/model/Class.IModel.php');
require_once("Class.Criteria2.php");
class AllTravelersModelCB implements IModel{
	function GetFilterLists(&$props){
		global $CONFIG;
		
		$obj_traveler = new TravelerSearchCriteria2CB;	
		$obj_criteria = new Criteria2;
		
		if( $props['is_login'] ){
			$obj_criteria->mustNotBeEqual( 'travelerID', $props['travelerID'] );	
		}
		
		$obj_criteria->mustBeEqual('tblGrouptoTraveler.groupID' , $CONFIG->getGroupID());
		
		$obj_criteria->setOrderBy( 'ranking DESC'                );
		$obj_criteria->setLimit  ( $props['offset'].','.$props['rows'] );
		
		$col_travelers         = $obj_traveler->getAllTravelers($obj_criteria);
		$props['obj_paging']   = new Paging        ( $obj_traveler->getTotalRecords(), $props['page'],'action=All', $props['rows'] );
		$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                                    );
		$props['view_type']    = 1;
	}
}
?>
