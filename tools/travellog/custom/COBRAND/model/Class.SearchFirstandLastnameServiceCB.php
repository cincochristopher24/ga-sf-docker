<?php
/**
* <b>SearchFirstandLastnameService</b> class
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/
require_once("travellog/model/Class.Traveler.php");
require_once('travellog/model/Class.SearchFirstandLastnameService.php');
require_once("Class.Connection.php");
require_once("Class.Recordset.php");

class SearchFirstandLastnameServiceCB extends SearchFirstandLastnameService{
	
	/********************************************************************************
	 * edits of neri to performSearch():
	 *		added the logged travelerID as parameter so as not to display 
	 *			certain travelers to block users: 							11-28-08
	 *		checked if traveler is suspended:								12-17-08 		
	 ********************************************************************************/
	
	function performSearch($value, $loggedTravelerID = 0){
		global $CONFIG;
		
		$conn = new Connection();
		$rs   = new Recordset($conn);
		$gID  = $CONFIG->getGroupID();
		$arr  = array();
		/*$sql  = "SELECT DISTINCT t.travelerID from tblTraveler as t,tblGrouptoTraveler as gt,tblGroup as g   WHERE 1 = 1 AND active > 0 and gt.travelerID = t.travelerID and gt.groupID = g.groupID and (g.groupID='$gID' or g.parentID='$gID')  AND t.travelerID NOT IN " .
			    "(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor) " . " AND MATCH (`firstname`,`lastname`) AGAINST ('$value')";*/
		$sql  = "SELECT DISTINCT t.travelerID 
				from tblTraveler as t,tblGrouptoTraveler as gt,tblGroup as g   
				WHERE 1 = 1 AND active > 0 
					and gt.travelerID = t.travelerID 
					and gt.groupID = g.groupID 
					and (g.groupID='$gID' or g.parentID='$gID')  
					AND MATCH (`firstname`,`lastname`) AGAINST ('$value')";
		
		$results = $rs->Execute($sql);
		
		if ($rs->Recordcount()){
			while($row = mysql_fetch_assoc($results)){
				//$arr[] = new Traveler($row["travelerID"]);				// editted by neri: 11-28-08
				$traveler = new Traveler($row['travelerID']);
				
				if (!$traveler->isSuspended()) {
					if (0 < $loggedTravelerID && !$traveler->isBlocked($loggedTravelerID))
						$arr[] = $traveler;		
					else if (0 == $loggedTravelerID)
						$arr[] = $traveler;
				}						
			}
		}
		return $arr;	   
	}
}
?>
