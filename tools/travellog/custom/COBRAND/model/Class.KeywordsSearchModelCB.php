<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once('travellog/model/Class.IModel.php');
class KeywordsSearchModelCB implements IModel{
	function GetFilterLists(&$props){
		global $CONFIG;
		
		if( $props['search_by'] == 4 ){
			require_once('travellog/model/Class.SearchFirstandLastnameServiceCB.php');
			$obj_traveler  = new SearchFirstandLastnameServiceCB(); 	
			$props['travelers'] = $obj_search->performSearch($props['keywords']);
			$props['travelers_count'] = count($props['travelers']);
			$optimize      = false;
		}
		else{
			require_once('travellog/model/Class.TravelerSearchCriteria2CB.php');
			require_once("Class.Criteria2.php");
			$obj_criteria = new Criteria2;
			
			$obj_criteria->mustBeEqual('tblTraveler.travelerID'    , 'tblGrouptoTraveler.travelerID');
			$obj_criteria->mustBeEqual('tblGrouptoTraveler.groupID' , $CONFIG->getGroupID());
			
			if ( $this->searchby == 1 ){
				$obj_criteria->setOrderBy('username');
				$obj_criteria->mustBeLike('username', $props['keywords']);
			}elseif ( $this->searchby == 2 ){
				$obj_criteria->setOrderBy('email');
				$obj_criteria->mustBeLike('email', $props['keywords']);
			}elseif ( $this->searchby == 3 ){
				$obj_criteria->setOrderBy('interests');
				$obj_criteria->mustBeLike('interests', $props['keywords']);
			}
			
			$obj_traveler  = new TravelerSearchCriteria2CB;
			$props['travelers'] = $myobj->getAllTravelers($obj_criteria);
			$props['travelers_count'] = $obj_traveler->getTotalRecords();
			$optimize      = true;
		}
		
		$props['obj_paging']   = new Paging($props['travelers_count'], $this->page, 'action=search&keywords='.$props['keywords'].'&searchby='.$props['search_by'], $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], $optimize                  );
		$props['view_type']    = 1;
		
	}
}
?>
