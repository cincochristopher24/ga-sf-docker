<?php
require_once('Class.Paging.php');
require_once('Class.ObjectIterator.php');
require_once("travellog/model/Class.RowsLimit.php");
require_once('travellog/custom/COBRAND/model/Class.TravelerCB.php');
require_once('travellog/model/Class.IModel.php');
class PopularTravelerModelCB implements IModel{
	function GetFilterLists(&$props){
		$obj_rows_limit        = new RowsLimit($props['rows'], $props['offset']);
		
		if ($props['is_login'])
			$props['travelers']          = TravelerCB::getMostPopular($obj_rows_limit, $props['travelerID']);				// neri added travelerID as parameter if traveler is logged in: 11-28-08
		else
			$props['travelers']           = TravelerCB::getMostPopular($obj_rows_limit);
		
		$props['travelers_count'] = TravelerCB::$statvar;
		$props['obj_paging']   = new Paging(TravelerCB::$statvar, $props['page'],'action=MostPopular', $props['rows'] );
		//$props['obj_iterator'] = new ObjectIterator( $col_travelers, $props['obj_paging'], true                             );
		$props['view_type']    = 4;
	}
}
?>
