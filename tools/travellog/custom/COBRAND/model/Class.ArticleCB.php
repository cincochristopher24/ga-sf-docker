<?php
require_once('travellog/model/Class.TravelLog.php');
require_once('travellog/model/Class.Article.php');

class ArticleCB extends Article{
	static function getCountriesWithArticles( $groupID ){
		$gIDs    = self::getAdminSubGroups  ( $groupID );	
		$travIDs = self::getGroupsToTraveler( $gIDs    );
		$arr     = array();
		$conn    = new Connection();
		$rs      = new Recordset($conn);		
		$sql     = sprintf(
							'SELECT gat.countryID as countryID FROM tblArticle as a, GoAbroad_Main.tbcountry as gat
							 WHERE a.deleted = 0 
							 	AND a.authorID IN ( %s ) 
								AND a.locID =  gat.locID 
							 GROUP BY countryID
							 UNION         
							 SELECT gact.countryID as countryID FROM tblArticle as a, GoAbroad_Main.tbcity as gact
							 WHERE a.deleted = 0 
							 	AND a.authorID IN ( %s ) 
								AND a.locID =  gact.locID 
							 GROUP BY countryID '
							 ,$travIDs
							 ,$travIDs
						  ); 
		$rs->Execute( $sql );
		
		if ($rs->Recordcount() == 0){
			return NULL;	 	
		}else {
			require_once("travellog/model/Class.Country.php");
			while ($row = mysql_fetch_array($rs->Resultset())) {
				try {
					$obj_country = new Country($row['countryID']);
					$arr[] = $obj_country;
				} catch (Exception $e) { }
			}
		}
		
		usort($arr,array("TravelLog","orderByCountryName"));
				
		return $arr;
	}
	
	private static function getAdminSubGroups( $groupID ){
		require_once("travellog/model/Class.AdminGroup.php");
		$obj_admin_group   = new AdminGroup( $groupID );
		$col_groups        = $obj_admin_group->getSubGroups();
		$col_sub_groupID   = array();
		$col_sub_groupID[] = $groupID;
		if( count($col_groups) ){
			foreach( $col_groups as $obj_group ){
				$col_sub_groupID[] = $obj_group->getGroupID();		
			}
		}
		return implode(',', $col_sub_groupID); 
	}
	
	private static function getGroupsToTraveler( $groupIDs ){
		$conn = new Connection();
		$rs   = new Recordset($conn);
		$arr  = array();
		$sql  = sprintf('SELECT travelerID FROM tblGrouptoTraveler WHERE groupID IN (%s)', $groupIDs);
		$rs->Execute( $sql );
		if( count( $rs->Resultset() ) ){
			while( $row = mysql_fetch_assoc( $rs->Resultset() ) ){
				$arr[] = $row['travelerID'];	
			}
		}
		return implode(',', $arr);
	}
}
?>
