<?php
	/**
	 *	Created on June 25, 2007
	 *	Class.TravelerCB.php - subclass of Traveler for Goabroad.net cobranding
 	 *	@author Kerwin Gordo
	 *	Last Edited By: CheWyl		31 Oct 2008
	 */

	require_once('travellog/model/Class.Traveler.php');
	require_once('travellog/model/Class.ProgramStep.php');

	class TravelerCB extends Traveler implements Sendable{
		private $config;

		public function setConfig($config){
			$this->config = $config;
		}
		
		/**
		 * overrides
		 */
		
		function isOnline(){			
			$sql = "SELECT session_user_id " .
			 		"FROM tblSessions " .
			 		"WHERE domain = '{$GLOBALS["CONFIG"]->getServerName()}' " .
					"AND session_user_id = " . $this->travID;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			if ($this->rs->Recordcount())
				return true;
			else
				return false;		
		}
		
    /**
     * Returns the categories of subgroups of this parent group.
     * 
     * @param string $keyword The category name to be searched.
     * @param boolean $isUncategorizedIncluded The flag whether to include the uncategorized category.
     * @param RowsLimit $limit The LIMIT of the categories to be retrieved.
     * @see RowsLimit
     * @throws exception
     * @see SqlResourceIterator
     * @return SqlResourceIterator Returns the categories of subgroups of this parent group. Returns SqlResourceIterator of class CustomizedSubGroupCategory.
     */
    function getSubGroupCategories($keyword = null, $limit = null){
		  try {
			  $handler = new dbHandler();
			  
			  $sql = "SELECT DISTINCT c.* " .
			    " FROM " .
			    "   `tblGroupToSubGroupCategory` as a," .
			    "   `tblGrouptoTraveler` as b," .
			    "   `tblSubGroupCategory` as c " .
			    " WHERE " .
			    "   a.groupID = b.groupID " .
			    " AND " .
			    "   c.categoryID = a.categoryID " .
			    " AND " .
			    "   c.parentID = " .$GLOBALS["CONFIG"]->getGroupID().
			    " AND " .
			    "   b.travelerID = ".$handler->makeSqlSafeString($this->getTravelerID())."";
			  $sql .= (is_null($keyword) OR "" == trim($keyword)) ? "" : " AND c.name LIKE ".$handler->makeSqlSafeString('%'.$keyword.'%');
			  $sql .= " ORDER BY categoryRank";
			  $sql .= (is_null($limit)) ? "" : SqlUtil::createLimitClause($limit)." ";

			  return new SqlResourceIterator($handler->execute($sql), "CustomizedSubGroupCategory");
		  }
		  catch(exception $ex){
			  throw $ex;
		  }
    }
    
    /**
     * Returns the number of subgroup category of this admin group.
     * 
     * @param string $keyword The category name to be searched.
     * @param boolean $isUncategorizedIncluded The flag whether to include the uncategorized category in counting.
     * @throws exception
     * @return integer Returns the number of subgroup category of this admin group.
     */
    function getSubGroupCategoriesCount($keyword = null){	  
		  try {
			  $sql = "SELECT COUNT(DISTINCT c.categoryID) " .
			    " FROM " .
			    "   `tblGroupToSubGroupCategory` as a," .
			    "   `tblGrouptoTraveler` as b," .
			    "   `tblSubGroupCategory` as c " .
			    " WHERE " .
			    "   a.groupID = b.groupID " .
			    " AND " .
			    "   c.categoryID = a.categoryID " .
			    " AND " .
			    "   c.parentID = " .$GLOBALS["CONFIG"]->getGroupID().
			    " AND " .
			    "   b.travelerID = ".$handler->makeSqlSafeString($this->getTravelerID())."";
			  $sql .= (is_null($keyword) OR "" == trim($keyword)) ? "" : " AND c.name LIKE ".$handler->makeSqlSafeString('%'.$keyword.'%');

			  
			  $rs = new SqlResourceIterator($handler->execute($sql));
			  return $rs->count();		  
		  }
		  catch(exception $ex){
			  throw $ex;
		  }
    }
		
		/**
		 * Retrieves the number of groups of this traveler
		 * 
		 * @param string $groupName limits the group to be retrieve so that only group that has name LIKE %$groupName% will be retrieve
		 * @return integer
		 */
		public function getGroupsCount($groupName = ""){
			$handler = new dbHandler();
			$arrGroup = array();
			$gID = $GLOBALS['CONFIG']->getGroupID();

			/*$sql = "SELECT count(DISTINCT tblGroup.groupID) as count " .
			 		" FROM tblGroup, tblGrouptoTraveler " .
					" WHERE tblGroup.groupID = tblGrouptoTraveler.groupID " .
					" AND tblGroup.isSuspended = 0 " .
					"AND tblGroup.name LIKE " .
					"".$handler->makeSqlSafeString("%".$groupName."%")."".
					" AND tblGroup.parentID = " . $gID .
					" AND tblGrouptoTraveler.travelerID = ".$this->travID;*/
					
			$sql = "SELECT count(DISTINCT tblGroup.groupID) as count " .
			 		" FROM tblGroup, tblGrouptoTraveler " .
					" WHERE tblGroup.isSuspended = 0 " .
					" AND ( tblGroup.parentID = $gID AND tblGroup.administrator = ".$this->travID." ) OR ".
					" (tblGroup.groupID = tblGrouptoTraveler.groupID " .
					"AND tblGroup.name LIKE " .
					"".$handler->makeSqlSafeString("%".$groupName."%")."".
					" AND tblGroup.parentID = " . $gID .
					" AND tblGrouptoTraveler.travelerID = ".$this->travID.")";
			
			$result = $handler->execute($sql);
			$rs = new iRecordset($result);
			$rec = $rs->current();
			
			return $rec['count'];	
		}
		
		/**
		 * Retrieves the groups of a traveler that is also a subgroup of the current
		 * COBRAND group.
		 * 
		 * @param FilterCriteria2 $_filterCriteria2 used in ORDER BY in sql
		 * @param RowsLimit $_rowslimit used in LIMIT in sql
		 * @param string $groupName limits the group to be retrieve so that only group that has name LIKE %$groupName% will be retrieve
		 * @return array array of Group object
		 */	 
		function getGroups($_filterCriteria2 = NULL, $_rowslimit = NULL, $groupName = ""){
			$handler = new dbHandler();
			$mCondition = "";
			
			if ($_filterCriteria2){				
				$myConditions = $_filterCriteria2->getConditions();
	 			$mAttname = $myConditions[0]->getAttributeName();
				$mOperation = $myConditions[0]->getOperation();

	 			switch ($mOperation){
	 				case FilterOp::$ORDER_BY :
	 					$mCondition = $mCondition . " ORDER BY " . $mAttname;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$mCondition = $mCondition . " ORDER BY " . $mAttname . " DESC " ;
	 					break;
	 			}
			}
			else
				$mCondition = $mCondition . " ORDER BY `name` ASC ";
			
			if ($_rowslimit){	 			
	 			$mOffset = $_rowslimit->getOffset();
	 			$mRows =  $_rowslimit->getRows();
	 			$mLimitStr = " 	LIMIT " . $mOffset . " , " . $mRows;
	 		}	
	 		else
	 			$mLimitStr = "";
			
			$arrGroup = array();
			$gID = $GLOBALS['CONFIG']->getGroupID();

			/*$sql = "SELECT DISTINCT tblGroup.groupID " .
			 		" FROM tblGroup, tblGrouptoTraveler " .
					" WHERE tblGroup.groupID = tblGrouptoTraveler.groupID " .
					" AND tblGroup.isSuspended = 0 " .
					"AND tblGroup.name LIKE " .
					"".$handler->makeSqlSafeString("%".$groupName."%")."".
					" AND tblGroup.parentID = " . $gID .
					" AND tblGrouptoTraveler.travelerID = ".$this->travID.$mCondition.$mLimitStr;*/
					
			$sql = "SELECT tblGroup.groupID " .
			 		" FROM tblGroup, tblGrouptoTraveler " .
					" WHERE tblGroup.isSuspended = 0 " .
					" AND ( tblGroup.parentID = $gID AND tblGroup.administrator = ".$this->travID." ) OR ".
					" (tblGroup.groupID = tblGrouptoTraveler.groupID " .
					"AND tblGroup.name LIKE " .
					"".$handler->makeSqlSafeString("%".$groupName."%")."".
					" AND tblGroup.parentID = " . $gID .
					" AND tblGrouptoTraveler.travelerID = ".$this->travID.") GROUP BY tblGroup.groupID ".$mCondition.$mLimitStr;
			
			$this->rs->Execute($sql);
			
			if (0 < $this->rs->Recordcount()){
				while($group = mysql_fetch_array($this->rs->Resultset()))
					$arrGroupId[] = $group['groupID'];

				$arrGroup = GroupFactory::instance()->create($arrGroupId);
			}

			return $arrGroup;
		}

		/*
		 * function name: exists
		 * by: chewyl
		 * 28 November 2007
		 * checks if traveler exists based on email
		 * arguments: 
		 * 		strFind => string to find
		 * returns travelerID if match has been found, 0 otherwise
		 */
		 
		/*********************************************************************************
		 * edits of neri to exist():
		 * 		returned travelerID if account exists, otherwise returned 0:	12-22-08
		 *********************************************************************************/
		
		public static function exist($strFind){			
			$mConn	= new Connection();
			$mRs	= new Recordset($mConn);
			$CONFIG = $GLOBALS['CONFIG'];			
			$gID = $CONFIG->getGroupID();

			$sql = "SELECT DISTINCT tblTraveler.travelerID " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGroup.groupID = tblGrouptoTraveler.groupID " .
					"AND tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblTraveler.email = '" . trim($strFind) . "' " .
					"AND tblTraveler.deactivated = 0 ".
					"AND (tblGroup.parentID = " . $gID . " OR tblGroup.groupID = " . $gID . ") ";
			$mRs->Execute($sql);
			
			/*if( 0 < $mRs->Recordcount())
				return true;
			else
				return false;*/
				
			if( 0 == $mRs->Recordcount())
				return 0;
			else{
				$tmpTrav = mysql_fetch_array($mRs->ResultSet());
				return $tmpTrav["travelerID"];
			}
		}

		function getFriends($_filtercriteria = NULL, $_rowslimit = NULL){
			$this->rs =	ConnectionProvider::instance()->getRecordset();
			$CONFIG = $GLOBALS['CONFIG'];			
			$gID = $CONFIG->getGroupID();
			
			$sql = "SELECT tblTraveler.* " .
					"FROM tblFriend, tblGrouptoTraveler, tblGroup, tblTraveler " .
					"WHERE tblFriend.friendID = tblGrouptoTraveler.travelerID " .
					"AND tblFriend.friendID = tblTraveler.travelerID " .
			        "AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
			        "AND " .
						"(" .
							"tblGroup.parentID = " . $gID . " OR tblGroup.groupID = " . $gID .
						")" .
					"AND tblFriend.travelerID = " . $this->travID . " " .
					"AND tblTraveler.deactivated = 0 ".
			        "GROUP BY tblTraveler.travelerID ";			  
			$this->rs->Execute($sql);

			$arrFriend = array();			
			while ($Friend = mysql_fetch_array($this->rs->Resultset())){
				try{
					$mTraveler = new Traveler();
					$mTraveler->setTravelerData($Friend);	
					$arrFriend[$Friend ['travelerID']] = $mTraveler;			
				} 	
				catch (exception $e) {		
					throw $e;
				}
			}

			return $arrFriend;         
		}		

		function getOtherTravelersInPresentLocation($_filtercriteria = null, $_rowslimit = null, $_random = false){			
			$limitstr = "";
			$arrTraveler = array();
			$CONFIG = $GLOBALS['CONFIG'];
			$gID	= $CONFIG->getGroupID();
						
			if ($_filtercriteria){			
				$myconditions = $_filtercriteria-> getConditions();
	 			$attname = $myconditions[0]-> getAttributeName();
				$operation = $myconditions[0]-> getOperation();

	 			switch ($operation){
	 				case FilterOp::$ORDER_BY :
	 					$condition = " ORDER BY " . $attname ;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$condition = " ORDER BY " . $attname . " DESC " ;
	 					break;
	 			}
			}
			else{
				if ($_random)
					$condition = " ORDER BY RAND() ";
				else
					$condition = " ORDER BY latestlogin DESC";
			}

		 	if ($_rowslimit){
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$limitstr = " 	LIMIT " . $offset . " , " . $rows;
	 		}	 		

			/*$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblTraveler.currlocationID = " . $this->locationID . " " .
					"AND tblTraveler.travelerID <> " . $this->travID . " " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						")" .
					"AND tblTraveler.travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor" .
						") " .
					"GROUP BY tblTraveler.travelerID " .
					$condition . $limitstr;*/
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblTraveler.currlocationID = " . $this->locationID . " " .
					"AND tblTraveler.travelerID <> " . $this->travID . " " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						")" .
					" AND tblTraveler.deactivated = 0 ".
					"GROUP BY tblTraveler.travelerID " .
					$condition . $limitstr;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			$sql2 = "select FOUND_ROWS() as totalRecords";
			$this->rs2	=	ConnectionProvider::instance()->getRecordset();
			$this->rs2->Execute($sql2);
			if ($this->rs2->Result(0, "totalRecords")){

				$this->setNumberOfTravelers($this->rs2->Result(0, "totalRecords"));
				while ($Traveler = mysql_fetch_array($this->rs->Resultset())){
					$mTraveler = new TravelerCB();
					$mTraveler->setTravelerData($Traveler);
					$arrTraveler[] = $mTraveler;
				}
			}

			return $arrTraveler;			
		}
		
		/*************************************************************************
		 * edits of neri to getOtherTravelersInMyHomeTown():
		 *		added loggedTravelerID so as not to display certain 
		 *			travelers' profiles to blocked users: 				11-28-08 	
		 *		filtered data and checked if traveler is suspended:		12-17-08						
		 *************************************************************************/
		
		function getOtherTravelersInMyHomeTown($_filtercriteria = NULL, $_rowslimit = NULL, $loggedTravelerID = 0){	
			$limitstr = "";
			$arrTraveler = array();
			$CONFIG = $GLOBALS['CONFIG'];			
			$gID	= $CONFIG->getGroupID();
			$htLocation = $this->getTravelerProfile()->getHTLocationID();
			
			if ($_filtercriteria){
				$myconditions = $_filtercriteria-> getConditions();
	 			$attname = $myconditions[0]-> getAttributeName();
				$operation = $myconditions[0]-> getOperation();
				
	 			switch ($operation){
	 				case FilterOp::$ORDER_BY :
	 					$condition = " ORDER BY " . $attname ;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$condition = " ORDER BY " . $attname . " DESC " ;
	 					break;
	 			}
			}
			else
				$condition = " ORDER BY latestlogin DESC";
		 	
		 	if ($_rowslimit){
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$limitstr = " 	LIMIT " . $offset . " , " . $rows;
	 		}
	 			
			/*$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* " .
			 		"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblTraveler.htlocationID = " . $htLocation . " " .
					"AND tblTraveler.travelerID <> " . $this->travID . " " .
					"AND tblTraveler.active > 0 " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						")" .
					"AND tblTraveler.travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor" .
						") " .
					"GROUP BY tblTraveler.travelerID " .
					$condition . $limitstr;*/
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* " .
			 		"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblTraveler.htlocationID = " . $htLocation . " " .
					"AND tblTraveler.travelerID <> " . $this->travID . " " .
					"AND tblTraveler.active > 0 " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						")" .
					" AND tblTraveler.deactivated = 0 ".
					"GROUP BY tblTraveler.travelerID " .
					$condition . $limitstr;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);
			
			$sql2 = "select FOUND_ROWS() as totalRecords";
			$this->rs2	=	ConnectionProvider::instance()->getRecordset();
			$this->rs2->Execute($sql2);

			$this->setNumberOfTravelers($this->rs2->Result(0, "totalRecords"));
			
			if ($this->rs2->Result(0, "totalRecords")){
				while ($Traveler = mysql_fetch_assoc($this->rs->Resultset())){
					$mTraveler = new TravelerCB();
					$mTraveler->setTravelerData($Traveler);
					
					if (!$mTraveler->isSuspended()) {
						if (0 < $loggedTravelerID && !$mTraveler->isBlocked($loggedTravelerID))
							$arrTraveler[] = $mTraveler;
						else if (0 == $loggedTravelerID)
							$arrTraveler[] = $mTraveler;
					}
				}
			}

			return $arrTraveler;					
		}

		function getFriendRequests(){
			$arrRequest = array();
			$CONFIG	= $GLOBALS['CONFIG'];			
			$gID	= $CONFIG->getGroupID();

			/*$sql = "SELECT tblTraveler.* " .
					"FROM tblFriendRequest, tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblTraveler.travelerID = tblFriendRequest.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblGrouptoTraveler.travelerID = tblFriendRequest.travelerID " .
					"AND tblFriendRequest.friendID = " . $this->travID . " " .
					"AND tblTraveler.active = 1 " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						") " .
					"AND tblTraveler.travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor" .
						") " .
					"GROUP BY tblTraveler.travelerID";*/
			$sql = "SELECT tblTraveler.* " .
					"FROM tblFriendRequest, tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblTraveler.travelerID = tblFriendRequest.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblGrouptoTraveler.travelerID = tblFriendRequest.travelerID " .
					"AND tblFriendRequest.friendID = " . $this->travID . " " .
					"AND tblTraveler.active = 1 " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						") " .
					" AND tblTraveler.deactivated = 0 ".
					"GROUP BY tblTraveler.travelerID";
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			$this->setNumberOfFriendRequests($this->rs->Recordcount());

			if (0 < $this->rs->Recordcount()){
				while ($Friend = mysql_fetch_assoc($this->rs->Resultset())){
					try{
						$mTraveler = new TravelerCB();
						$mTraveler->setTravelerData($Friend);
						$arrRequest[] = $mTraveler;
					}					
					catch (exception $e){
						throw $e;
					}
				}
			}

			return $arrRequest;
		}		

		function getPendingRequests(){
			$pendingRequests = array();
			$CONFIG = $GLOBALS['CONFIG'];			
			$gID = $CONFIG->getGroupID();

			/*$sql = "SELECT tblTraveler.* " .
					"FROM tblFriendRequest, tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblTraveler.travelerID = tblFriendRequest.friendID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblGrouptoTraveler.travelerID = tblFriendRequest.friendID " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND NOT EXISTS " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblFriendRequest.friendID) " .
					"AND tblTraveler.active = 1 " .
					"AND tblFriendRequest.travelerID = " . $this->travID . " " .
					"GROUP BY tblTraveler.travelerID";*/
			$sql = "SELECT tblTraveler.* " .
					"FROM tblFriendRequest, tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblTraveler.travelerID = tblFriendRequest.friendID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblGrouptoTraveler.travelerID = tblFriendRequest.friendID " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND tblTraveler.active = 1 " .
					"AND tblFriendRequest.travelerID = " . $this->travID . " " .
					"AND tblTraveler.deactivated = 0 ".
					"GROUP BY tblTraveler.travelerID";
			
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			$this-> setNumberOfPendingFriends($this->rs->Recordcount());

			if (0 < $this->rs->Recordcount()){
				while($Friend = mysql_fetch_array($this->rs->Resultset())){				
					try{
						$mTraveler = new TravelerCB();
						$mTraveler->setTravelerData($Friend);
						$pendingRequests[] = $mTraveler;
					}
					catch (exception $e){
						throw $e;
					}
				}
			}

			return $pendingRequests;
		}		
		
		function getBlockedUsers(){
			$blockedUsers = array();
			$CONFIG = $GLOBALS['CONFIG'];	
			$gID = $CONFIG->getGroupID();
			
			/*$sql = "SELECT tblTraveler.* " .
					"FROM tblBlockedUsers, tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblTraveler.travelerID = tblBlockedUsers.userID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblGrouptoTraveler.travelerID = tblBlockedUsers.userID " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND NOT EXISTS " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblBlockedUsers.userID) " .
					"AND tblTraveler.active = 1 " .
					"AND tblBlockedUsers.travelerID = " . $this->travID . " " .
					"GROUP BY tblTraveler.travelerID";*/
			$sql = "SELECT tblTraveler.* " .
					"FROM tblBlockedUsers, tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblTraveler.travelerID = tblBlockedUsers.userID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblGrouptoTraveler.travelerID = tblBlockedUsers.userID " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND tblTraveler.active = 1 " .
					"AND tblTraveler.deactivated = 0 ".
					"AND tblBlockedUsers.travelerID = " . $this->travID . " " .
					"GROUP BY tblTraveler.travelerID";
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			$this-> setNumberOfBlockedUsers($this->rs->Recordcount());

			if (0 < $this->rs->Recordcount()){
				while ($Blocked = mysql_fetch_array($this->rs->Resultset())){					
					try{
						$mTraveler = new TravelerCB();
						$mTraveler->setTravelerData($Blocked);
						$blockedUsers[] = $mTraveler;
					}					
					catch (exception $e){
						throw $e;											
					}					
				}
			}

			return $blockedUsers;
		}
		
		
		function getGroupRequests($_filterCriteria2 = NULL){
			$condition	= "";
			$arrGroupId	= array();
			$groupRequests = array();
			$CONFIG = $GLOBALS['CONFIG'];
			$gID = $CONFIG->getGroupID();
			
			if (NULL != $_filterCriteria2){			
				$filterCondition = $_filterCriteria2-> getConditions();
				$attribute = $filterCondition[0]-> getAttributeName();
				$value     = $filterCondition[0]-> getValue();

				switch($filterCondition[0]->getOperation()){    				
    				case FilterOp::$EQUAL:
    					$operator	= " = ";
    					break;
    				case FilterOp::$NOT_EQUAL:
    					$operator   = " <> ";
    					break;
    				default:
    					$operator   = " = ";
    					break;
    			}

				$condition = " AND " . $attribute . $operator . $value;
			}
			/*$sql = "SELECT DISTINCT tblRequestList.groupID " .
			 		"FROM tblRequestList, tblGroup " .
					"WHERE tblRequestList.groupID = tblGroup.groupID " .
					"AND tblRequestList.travelerID = " . $this->travID . " " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND NOT EXISTS " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblRequestList.travelerID) " .
					"AND tblGroup.isSuspended = 0 " .
					"AND tblRequestList.status = 0 " . $condition;*/
			$sql = "SELECT DISTINCT tblRequestList.groupID " .
			 		"FROM tblRequestList, tblGroup " .
					"WHERE tblRequestList.groupID = tblGroup.groupID " .
					"AND tblRequestList.travelerID = " . $this->travID . " " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND tblGroup.isSuspended = 0 " .
					"AND tblRequestList.status = 0 " . $condition;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			$this->setNumberOfGroupRequests ($this->rs->Recordcount());

			if (0 < $this->rs->Recordcount()){				
				while ($groupRequest = mysql_fetch_array ($this->rs->Resultset()))
					$arrGroupId[] = $groupRequest['groupID'];
				$groupRequests = GroupFactory::instance()->create($arrGroupId);
			}

			return $groupRequests;
		}
		
		 
		function getGroupInvites(){			
			$arrGroupId = array();
			$groupInvites = array();
			$CONFIG = $GLOBALS['CONFIG'];
			$gID = $CONFIG->getGroupID();

			/*$sql = "SELECT DISTINCT tblInviteList.groupID " .
					"FROM tblInviteList, tblGroup " .
					"WHERE tblInviteList.groupID = tblGroup.groupID " .
					"AND tblInviteList.travelerID = " . $this->travID . " " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND NOT EXISTS " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblInviteList.travelerID) " .
					"AND tblInviteList.status = 0 " .
					"AND tblGroup.isSuspended = 0 ";*/
			$sql = "SELECT DISTINCT tblInviteList.groupID " .
					"FROM tblInviteList, tblGroup " .
					"WHERE tblInviteList.groupID = tblGroup.groupID " .
					"AND tblInviteList.travelerID = " . $this->travID . " " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND tblInviteList.status = 0 " .
					"AND tblGroup.isSuspended = 0 ";
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			$this->setNumberOfGroupInvites($this->rs->Recordcount());

			//if (0 == $this->rs->Recordcount()){
			if($this->rs->Recordcount()){
				while ($groupInvite = mysql_fetch_array ($this->rs->Resultset()))
					$arrGroupId[] = $groupInvite['groupID'];
				$groupInvites = GroupFactory::instance()->create($arrGroupId);
			}

			return $groupInvites;
		}
		
		function getNumberOfGroupInvites(){			
			$CONFIG = $GLOBALS['CONFIG'];	
			$gID = $CONFIG->getGroupID();
			
			if (0 == $this->numberOfGroupInvites) {
				/*$sql = "SELECT count(tblInviteList.groupID) AS cnt " .
				 		"FROM tblInviteList, tblGroup " .
						"WHERE tblInviteList.groupID = tblGroup.groupID " .
						"AND tblInviteList.travelerID = " . $this->travID . " " .
						"AND " .
							"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
						"AND NOT EXISTS " .
							"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblInviteList.travelerID) " .
						"AND tblInviteList.status = 0 " .
						"AND tblGroup.isSuspended = 0 ";*/
				$sql = "SELECT count(tblInviteList.groupID) AS cnt " .
				 		"FROM tblInviteList, tblGroup " .
						"WHERE tblInviteList.groupID = tblGroup.groupID " .
						"AND tblInviteList.travelerID = " . $this->travID . " " .
						"AND " .
							"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
						"AND tblInviteList.status = 0 " .
						"AND tblGroup.isSuspended = 0 ";
			
				$this->rs	=	ConnectionProvider::instance()->getRecordset();
				$this->rs->Execute($sql);
				
				$rsar = mysql_fetch_array($this->rs->ResultSet());
				$this->numberOfGroupInvites = $rsar['cnt'];
			}	

			return $this->numberOfGroupInvites;
		}
		
		/** Function name: getNumberOfGroupRequests
		 * @return integer
		 */		 
		function getNumberOfGroupRequests (){			
			$CONFIG = $GLOBALS['CONFIG'];
			$gID = $CONFIG->getGroupID();

			if (0 == $this->numberOfGroupRequests){
				/*$sql = "SELECT count(tblRequestList.groupID) AS cnt " .
				 		"FROM tblRequestList, tblGroup " .
						"WHERE tblRequestList.groupID = tblGroup.groupID " .
						"AND tblRequestList.travelerID = " . $this->travID . " " .
						"AND " .
							"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
						"AND NOT EXISTS " .
							"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblRequestList.travelerID) " .
						"AND tblGroup.isSuspended = 0 " .
						"AND tblRequestList.status = 0";*/
				$sql = "SELECT count(tblRequestList.groupID) AS cnt " .
				 		"FROM tblRequestList, tblGroup " .
						"WHERE tblRequestList.groupID = tblGroup.groupID " .
						"AND tblRequestList.travelerID = " . $this->travID . " " .
						"AND " .
							"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
						"AND tblGroup.isSuspended = 0 " .
						"AND tblRequestList.status = 0";
				
				$this->rs	=	ConnectionProvider::instance()->getRecordset();
				$this->rs->Execute($sql);

				$rsar = mysql_fetch_array($this->rs->ResultSet());
				$this->numberOfGroupRequests = $rsar['cnt'];
			}

			return $this-> numberOfGroupRequests;
		}
		
		/** Function name: getNumberOfDeniedGroupRequests (added by K. Gordo)
		 * @return integer
		 */
		function getNumberOfDeniedGroupRequests (){
			$CONFIG = $GLOBALS['CONFIG'];	
			$gID = $CONFIG->getGroupID();

			/*$sql = "SELECT count(tblRequestList.groupID) AS cnt " .
					"FROM tblRequestList, tblGroup " .
					"WHERE tblRequestList.groupID = tblGroup.groupID " .
					"AND tblRequestList.travelerID = " . $this->travID . " " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND NOT EXISTS " .
						"(SELECT DISTINCT travelerID FROM tblGrouptoAdvisor WHERE tblGrouptoAdvisor.travelerID = tblRequestList.travelerID) " .
					"AND tblGroup.isSuspended = 0 " .
					"AND tblRequestList.status = 1";*/
			$sql = "SELECT count(tblRequestList.groupID) AS cnt " .
					"FROM tblRequestList, tblGroup " .
					"WHERE tblRequestList.groupID = tblGroup.groupID " .
					"AND tblRequestList.travelerID = " . $this->travID . " " .
					"AND " .
						"(tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID . ") " .
					"AND tblGroup.isSuspended = 0 " .
					"AND tblRequestList.status = 1";
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			$rsar = mysql_fetch_array($this->rs->ResultSet());
			return $rsar['cnt'];			
		}

		function getGroups2($_filterCriteria2 = NULL, $_rowslimit = NULL){
			$limitstr	= "";
			$arrGroups	= array();
			$CONFIG	= $GLOBALS['CONFIG'];
			$gID	= $CONFIG->getGroupID();

			if (!$this->isSubGroup())
				$condition = " and b.parentID = 0";
			else
				$condition = " and b.parentID = " . $gID;

			if ($_filterCriteria2){		
				$myconditions = $_filterCriteria2-> getConditions();
	 			$attname = $myconditions[0]-> getAttributeName();
				$operation = $myconditions[0]-> getOperation();
				
	 			switch ($operation){
	 				case FilterOp::$ORDER_BY :
	 					$condition = $condition . " ORDER BY " . $attname ;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$condition = $condition . " ORDER BY " . $attname . " DESC " ;
	 					break;
	 			}
			}
			else
				$condition = $condition . " ORDER BY `name` ASC ";				

			if ($_rowslimit){	 			
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$limitstr = " 	LIMIT " . $offset . " , " . $rows;
	 		}		

			// Get subgroups a traveler belongs to in cobrand main group			
			$sql = "SELECT DISTINCT tblGroup.groupID " .
					"FROM tblGrouptoTraveler, tblGroup " .
					"WHERE a.`travelerID` = $this->travID " .
					"and b.isSuspended = 0 and b.`groupID` = a.`groupID` ". $condition . $limitstr;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs-> Execute($sqlQuery);

			$arrGroupId = array();
			$groups = array();
			
			if ($this->rs->Recordcount()){
				while($group = mysql_fetch_array($this->rs->Resultset()))
					$arrGroupId[] = $group['groupID'];

				$groups = GroupFactory::instance()->create($arrGroupId);
			}

			return $groups;
		}
		
		// new functions for HomeStay and ProgramSteps
		
		public function getHomeStay(){
			$sql = "SELECT homestayID FROM tblTravelertoHomeStay WHERE travelerID = " . $this->travID ; 
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			if ($this->rs->Recordcount())
				return new HomeStay($this->rs->Result(0,'homestayID'));
		}

		public function addHomeStay($_homestayID){
			$sql = "INSERT INTO tblTravelertoHomeStay (travelerID, homestayID) VALUES (" . $this->travID . " , " . $_homestayID . ")" ;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);  
		}

		public function removeHomeStay($_homestayID){
			$sql = "DELETE FROM tblTravelertoHomeStay WHERE homestayID = " . $_homestayID ;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);  
		}
		
		public function addProgramStep($_pStepID){		
			if (!$this->hasAddedProgramStep($_pStepID)):
				$sql = "INSERT INTO tblTravelertoProgramStep ( travelerID, programstepID ) VALUES ($this->travID, $_pStepID)" ;
				$this->rs	=	ConnectionProvider::instance()->getRecordset();
				$this->rs->Execute($sql);				
			endif;			
		}
		
		public function removeProgramStep($_pStepID) {			
			$sql = "DELETE FROM tblTravelertoProgramStep WHERE programstepID = " . $_pStepID;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);			
		}
		
		public function getAllAddedProgramSteps(){
			$sql = "SELECT programstepID FROM tblTravelertoProgramStep WHERE travelerID = " . $this->travID;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			$pSteps = array();
			while ($rset = mysql_fetch_array($this->rs->Resultset())){
				$pSteps[] = new ProgramStep ($rset['programstepID']);				
			}

			return $pSteps;			
		}

		public function hasAddedProgramStep($_pStepID){
			$sql = "SELECT programstepID " .
					"FROM tblTravelertoProgramStep " .
					"WHERE travelerID = " . $this->travID . " " .
					"AND programstepID = " . $_pStepID;
			$this->rs	=	ConnectionProvider::instance()->getRecordset();
			$this->rs->Execute($sql);

			if ($this->rs->Recordcount())
				return true;
			else
				return false;
		}

		/*
		 * STATIC FUNCTIONS
		*/
		
		/********************************************************************
		 * edits by neri to getNewestMembers():
		 * 		added loggedTravelerID so as not to display certain 
		 * 			travelers' profiles to blocked users: 			11-28-08
		 * 		checked if traveler is suspended:					12-17-08	
		 ********************************************************************/
		
		public static function getNewestMembers($_rowslimit = NULL, $loggedTravelerID = 0){
			$arrTraveler = array();
			$CONFIG = $GLOBALS['CONFIG'];			
			$gID	= $CONFIG->getGroupID();		
			$mRs	= $CONFIG->getResultset();
			
			if ($_rowslimit){
				$offset = $_rowslimit-> getOffset();
				$rows = $_rowslimit-> getRows();
				$limitstr = " LIMIT " . $offset . " , " . $rows;				
			}			
			else
				$limitstr = " LIMIT 0, 6";
			
			/*$sql = "SELECT tblTraveler.* " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblTraveler.active > 0 " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						")" .
					"AND tblTraveler.travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor" .
						") " .
					"GROUP BY tblTraveler.travelerID " .
					"ORDER BY dateregistered DESC" . $limitstr;*/
			$sql = "SELECT tblTraveler.* " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblTraveler.active > 0 " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						")" .
					" AND tblTraveler.deactivated = 0 ".
					"GROUP BY tblTraveler.travelerID " .
					"ORDER BY dateregistered DESC" . $limitstr;
			$mRs->Execute($sql);
			
			while ($Traveler = mysql_fetch_assoc($mRs->Resultset())){
				$mTraveler = new TravelerCB();
				$mTraveler->setTravelerData($Traveler);
				//$arrTraveler[] = $mTraveler;								// edits by neri: 11-28-08
				
				if (!$mTraveler->isSuspended()) {
					if (0 < $loggedTravelerID && !$mTraveler->isBlocked($loggedTravelerID))
						$arrTraveler[] = $mTraveler;
					else if (0 == $loggedTravelerID)
						$arrTraveler[] = $mTraveler;
				}
			}

			return $arrTraveler;			
		}
		
		/*********************************************************************************
		 * edits of neri to getMostPopular():
		 * 		added loggedTravelerID so as not to display certain travelers' 
		 * 			profiles to blocked users: 									11-28-08
		 * 		checked if traveler is suspended: 								12-17-08
		 *********************************************************************************/
		
		public static function getMostPopular($_rowslimit = NULL, $loggedTravelerID = 0){
			$arrTraveler = array();
			$CONFIG = $GLOBALS['CONFIG'];			
			$gID	= $CONFIG->getGroupID();		
			$mRs	= $CONFIG->getResultset();
			$mRs2	= $CONFIG->getResultset();
			
			if ($_rowslimit){
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$limitstr = " 	LIMIT " . $offset . " , " . $rows;
	 		}	 		
	 		else
	 			$limitstr = " LIMIT 0, 6";
			
			/*$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* " .
			 		"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
		 			"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						") " .
					"AND tblTraveler.active > 0 " .
					"AND tblTraveler.travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor ".
						") " .
					"GROUP BY tblTraveler.travelerID " .
					"ORDER BY views DESC" . $limitstr;*/
			$sql = "SELECT SQL_CALC_FOUND_ROWS tblTraveler.* " .
			 		"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
		 			"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						") " .
					"AND tblTraveler.active > 0 " .
					"AND tblTraveler.deactivated = 0 ".
					"GROUP BY tblTraveler.travelerID " .
					"ORDER BY views DESC" . $limitstr;
			$mRs->Execute($sql);			
			
			if (0 < $mRs->RecordCount()){
				while ($Traveler = mysql_fetch_assoc($mRs->Resultset())){
					$mTraveler = new TravelerCB();
					$mTraveler->setTravelerData($Traveler);
					
					if (!$mTraveler->isSuspended()) {
						if (0 < $loggedTravelerID && !$mTraveler->isBlocked($loggedTravelerID))				// editted by neri: 11-28-08
							$arrTraveler[] = $mTraveler;
						else if (0 == $loggedTravelerID)
							$arrTraveler[] = $mTraveler;
					}			
				}
				
				$sql2 = "SELECT FOUND_ROWS() as totalRecords";
				$mRs2->Execute($sql2);

				Traveler::$statvar = $mRs2->Result(0, "totalRecords");
			}

			return $arrTraveler;	
		}

		public static function getTravelersInLocation($_locID = 0){
			$CONFIG = $GLOBALS['CONFIG'];
			$gID	= $CONFIG->getGroupID();			
			$mRs	= $CONFIG->getResultset();
			$mRs2	= $CONFIG->getResultset();
			 	
			/*$sql = "SELECT tblTraveler.* " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						")" .
					"AND currlocationID = " . $_locID . " " .
					"AND tblTraveler.travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor" .
						") " .
					"GROUP BY tblTraveler.travelerID";*/
			$sql = "SELECT tblTraveler.* " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup " .
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						")" .
					"AND currlocationID = " . $_locID . " " .
					"AND tblTraveler.deactivated = 0 ".
					"GROUP BY tblTraveler.travelerID";
			$mRs->Execute($sql);

			$arrTraveler = array();
			if (0 < $mRs->Recordcount()){
				while ($Traveler = mysql_fetch_assoc($mRs->Resultset())){
					$mTraveler = new TravelerCB();
					$mTraveler->setTravelerData($Traveler);
					$arrTraveler[] = $mTraveler;
				}
			}

			return $arrTraveler;			
		}
		
		/*********************************************************************************
		 * edits of neri to getAllTravelers():
		 * 		added loggedTravelerID so as not to display certain travelers' 
		 * 			profiles to blocked users: 									11-28-08
		 * 		checked if traveler is suspended: 								12-17-08
		 *********************************************************************************/

		public static function getAllTravelers($_filtercriteria = NULL, $_rowslimit = NULL, $loggedTravelerID = 0){
			$mLimitStr = "";
			$arrTraveler = array();
			$CONFIG	= $GLOBALS['CONFIG'];
			$gID	= $CONFIG->getGroupID();
			$mRs	= $CONFIG->getResultset();
			$mRs2	= $CONFIG->getResultset();
			
			if ($_filtercriteria){				
				$myconditions = $_filtercriteria-> getConditions();
	 			$attname = $myconditions[0]-> getAttributeName();
				$operation = $myconditions[0]-> getOperation();
				
	 			switch ($operation){
	 				case FilterOp::$ORDER_BY :
	 					$condition = " ORDER BY " . $attname ;
	 					break;
	 				case FilterOp::$ORDER_BY_DESC :
	 					$condition = " ORDER BY " . $attname . " DESC " ;
	 					break;
	 				case FilterOp::$LIKE :
	 					$myboolean = $_filtercriteria->getBooleanOps();
	 					$value = $myconditions[0]->getValue();
	 					$condition = $myboolean[0]. ' ' . $attname . " LIKE '%$value%'";
	 					break;
	 				case FilterOp::$IN:
	 					$myboolean = $_filtercriteria->getBooleanOps();
	 					$value = $myconditions[0]->getValue();
	 					$condition = $myboolean[0]. ' ' . $attname .' in ('.$value.')';
	 					break;
	 			}
			}
			else
				$condition = " ORDER BY `latestlogin` DESC";
		 	
		 	if ($_rowslimit){
	 			
	 			$offset = $_rowslimit-> getOffset();
	 			$rows =  $_rowslimit-> getRows();
	 			$mLimitStr = " 	LIMIT " . $offset . " , " . $rows;
	 		}
		 			
			/*$sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT tblTraveler.* " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup ".
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblTraveler.active > 0 " .
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						") " .
					"AND tblTraveler.travelerID NOT IN " .
						"(" .
							"SELECT DISTINCT travelerID FROM tblGrouptoAdvisor " .
						") " .
					$condition . $mLimitStr;*/
			$sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT tblTraveler.* " .
					"FROM tblTraveler, tblGrouptoTraveler, tblGroup ".
					"WHERE tblGrouptoTraveler.travelerID = tblTraveler.travelerID " .
					"AND tblGrouptoTraveler.groupID = tblGroup.groupID " .
					"AND tblTraveler.active > 0 " .
					"AND tblTraveler.deactivated = 0 ".
					"AND " .
						"(" .
							"tblGroup.groupID = " . $gID . " OR tblGroup.parentID = " . $gID .
						") " .
					$condition . $mLimitStr;
			
			$mRs->Execute($sql);
			
			if (0 < $mRs->RecordCount()){
				
				while ($Traveler = mysql_fetch_assoc($mRs->Resultset())){
					$mTraveler = new TravelerCB();
					$mTraveler->setTravelerData($Traveler);
					
					if (!$mTraveler->isSuspended()) {
						if (0 < $loggedTravelerID && !$mTraveler->isBlocked($loggedTravelerID) /*&& $mTraveler->getTravelerID() != $loggedTravelerID*/)				// added by neri: 11-28-08
							$arrTraveler[] = $mTraveler;
						else if (0 == $loggedTravelerID)
							$arrTraveler[] = $mTraveler;
					}	 
				}
				
				$sql2 = "SELECT FOUND_ROWS() AS totalRecords";
				$mRs2->Execute($sql2);

				Traveler::$statvar = $mRs2->Result(0, "totalRecords");	
			}
			return $arrTraveler;			
		}		
	}
?>