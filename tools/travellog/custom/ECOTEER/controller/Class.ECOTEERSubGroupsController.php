<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class ECOTEERSubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new ECOTEER subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/ECOTEER/views"));
			$this->file_factory->setPath("CSS", "/custom/ECOTEER/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/ECOTEER/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}