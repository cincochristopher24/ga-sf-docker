<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDEntryController.php');
	
	class ECOTEEREntryController extends COBRANDEntryController {
	
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ECOTEER/views'));
			$this->file_factory->setPath('CSS', '/custom/ECOTEER/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ECOTEER/views/');
		}
	
		function performAction(){
			parent::performAction();
		}
	
	}