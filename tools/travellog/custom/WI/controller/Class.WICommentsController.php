<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDCommentsController.php');
 
	class WICommentsController extends COBRANDCommentsController {
		function __construct(){		
			parent::__construct();	
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/WI/views'));
			$this->file_factory->setPath('CSS', '/custom/WI/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/WI/views/');
		}
	 	
		function performAction() {
			parent::performAction();
		} 	
	}