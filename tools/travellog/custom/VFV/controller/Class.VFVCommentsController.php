<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDCommentsController.php');
 
	class VFVCommentsController extends COBRANDCommentsController {
		function __construct(){		
			parent::__construct();	
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/VFV/views'));
			$this->file_factory->setPath('CSS', '/custom/VFV/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VFV/views/');
		}
	 	
		function performAction() {
			parent::performAction();
		} 	
	}