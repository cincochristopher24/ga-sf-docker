<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionController.php');

	class VFVDiscussionController extends COBRANDDiscussionController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/VFV/views/'));
			$file_factory->setPath('CSS', '/custom/VFV/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VFV/views/');
		}
	}