<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDIndexController.php');
	
	class VFVIndexController extends COBRANDIndexController{
		function __construct(){
	
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/VFV/views'));
			$this->file_factory->setPath('CSS', '/custom/VFV/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VFV/views/'); 
		}
		 
		function performAction(){ 
		
			parent::performAction();
			
		}
	}