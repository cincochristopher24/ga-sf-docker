<?php
	/*
	 * Class.AIFSLoginController.php
	 * Created on Nov 23, 2007
	 * created by marc
	 */
 
 	require_once("travellog/custom/COBRAND/controller/Class.COBRANDLoginController.php");
 	
 	class CEALoginController extends COBRANDLoginController{
 		
 		function __construct(){
 			
 			parent::__construct();
 			
 			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CEA/views'));
			$this->file_factory->setPath('CSS', '/custom/CEA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CEA/views/');
 		}
 		
 	}
?>
