<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 26 November 2007
	 * Class.CEACommentRequestController.php
	 */

	require_once 'travellog/custom/COBRAND/controller/Class.COBRANDCommentRequestController.php';

	 class CEACommentRequestController extends COBRANDCommentRequestController {

	 	function __construct(){

			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CEA/views'));
			$this->file_factory->setPath('CSS', '/custom/CEA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CEA/views/');
		}

	 	function performAction() {
	 		parent::performAction();
	 	}
	 }
?>