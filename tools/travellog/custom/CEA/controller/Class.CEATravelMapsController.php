<?php
/*
	Filename:		Class.CEATravelMapsController.php
	Author:			Jonas Tandinco
	Date Created:	Jan/03/2008
	Putpose:		controller implementation for CEA co-brand site

	EDIT HISTORY:
*/

require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');

class CEATravelMapsController extends COBRANDTravelMapsController{
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CEA/views'));
		$this->file_factory->setPath('CSS', '/custom/CEA/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CEA/views/');   
	}
	
	public function performAction(){ 
		parent::performAction();
	}
}
?>
