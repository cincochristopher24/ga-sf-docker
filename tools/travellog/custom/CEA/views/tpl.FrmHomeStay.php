<?php

Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
Template::setMainTemplateVar('layoutID', 'admin_group');
Template::setMainTemplateVar('page_location', 'Home');

?>

<div class="area" id="main">	
	<div class="section">
		<div class="content">
			<form action="homestay.php?mode=save" method="POST">
				<li>
				<label>Name:</label>
				<input type="text" name="txtName" size="30" value="<?= isset($txtName) ? $txtName : '' ?>"> 
				<?= isset($errormsg) && array_key_exists('txtName' , $errormsg) ? $errormsg['txtName'] : '' ?>
				</li>
				<li>
				<label>Address:</label>
				<textarea name="txaAddress" cols="30" rows="10"><?= isset($txaAddress) ? $txaAddress : '' ?></textarea>
				</li>
				<li>
				<label>Email:</label>
				<input type="textbox" name="txtEmail" size="30" value="<?= isset($txtEmail) ? $txtEmail : '' ?>"> 
				</li>
				<li>
				<label>Phone:</label>
				<input type="textbox" name="txtPhone" size="30" value="<?= isset($txtPhone) ? $txtPhone : '' ?>"> 
				</li>
				<li>
			 	<label>Family Names:</label>
				<textarea name="txaFamNames" cols="30" rows="10"><?= isset($txaFamNames) ? $txaFamNames : '' ?></textarea>
				</li>
				<li>
				<label>Description:</label>
				<textarea name="txaDesc" cols="30" rows="10"><?= isset($txaDesc) ? $txaDesc : '' ?></textarea>
				</li>
				<li>
				<label>Assign to Available Student:</label>
				<select name="cmbStudent">
					<option value="0">-- SELECT --
					<? foreach ($users as $eachuser) : ?>
						<option value="<?=$eachuser->getTravelerID()?>" <?= (isset($assignedStudentID) && $assignedStudentID == $eachuser->getTravelerID()) ? 'selected' : '' ?> > <?=$eachuser->getUserName()?>
					<? endforeach; ?>
				</select>
				</li>
				<li>
				<input type="submit" name="submit" value="Save">
				</li>
				<input type="hidden" name="hsID" value="<?= isset($hsID) ? $hsID : 0 ?>">						
			</form>
		</div>
	</div>
</div>