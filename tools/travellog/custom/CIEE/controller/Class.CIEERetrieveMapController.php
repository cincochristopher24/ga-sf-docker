<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	CIEERetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for CIEE	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/CIEE/views'));
		$file_factory->setPath('CSS', '/custom/CIEE/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIEE/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}