<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDFeedbackController.php');
class CIEEFeedbackController extends COBRANDFeedbackController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIEE/views'));
		$this->file_factory->setPath('CSS', '/custom/CIEE/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIEE/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>