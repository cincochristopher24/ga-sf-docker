<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewMemberController.php');
	
	class CIEENewMemberController extends COBRANDNewMemberController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIEE/views'));
			$this->file_factory->setPath('CSS', '/custom/CIEE/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIEE/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
