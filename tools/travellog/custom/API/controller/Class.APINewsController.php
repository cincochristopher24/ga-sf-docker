<?php
	 require_once("travellog/custom/COBRAND/controller/Class.COBRANDNewsController.php");
	 
	 class APINewsController extends COBRANDNewsController{
	 	
	 	private $config = NULL;
	 	
	 	function __construct(){
	 		parent::__construct();
	 		
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/API/views'));
			$this->file_factory->setPath('CSS', '/custom/API/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/API/views/');
		}
	 	
	 }