<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDFacilitatorController.php');
	
 	class CULTURALEMBRACEFacilitatorController extends COBRANDFacilitatorController 
 	{
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CULTURALEMBRACE/views'));
			$this->file_factory->setPath('CSS', '/custom/CULTURALEMBRACE/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CULTURALEMBRACE/views/');
 		}
 	}
?>