<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDFeedbackController.php');
class CULTURALEMBRACEFeedbackController extends COBRANDFeedbackController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CULTURALEMBRACE/views'));
		$this->file_factory->setPath('CSS', '/custom/CULTURALEMBRACE/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CULTURALEMBRACE/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>