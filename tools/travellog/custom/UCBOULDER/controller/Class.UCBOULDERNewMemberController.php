<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewMemberController.php');
	
	class UCBOULDERNewMemberController extends COBRANDNewMemberController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/UCBOULDER/views'));
			$this->file_factory->setPath('CSS', '/custom/UCBOULDER/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/UCBOULDER/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
