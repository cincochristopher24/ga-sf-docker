<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDTravelMapsController.php');
class LIVINGROUTESTravelMapsController extends COBRANDTravelMapsController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/LIVINGROUTES/views'));
		$this->file_factory->setPath('CSS', '/custom/LIVINGROUTES/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/LIVINGROUTES/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>