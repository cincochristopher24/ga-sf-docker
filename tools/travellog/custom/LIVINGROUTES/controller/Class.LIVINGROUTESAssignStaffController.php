<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class LIVINGROUTESAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/LIVINGROUTES/views'));
			$this->mFactory->setPath('CSS', '/custom/LIVINGROUTES/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/LIVINGROUTES/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>