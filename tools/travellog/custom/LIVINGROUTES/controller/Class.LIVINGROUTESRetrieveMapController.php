<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	LIVINGROUTESRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for LIVINGROUTES	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/LIVINGROUTES/views'));
		$file_factory->setPath('CSS', '/custom/LIVINGROUTES/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/LIVINGROUTES/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}