<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDRegistrationController.php");
	
	class ISLARegistrationController extends COBRANDRegistrationController{
		
		function __construct(){
			
			parent::__construct();
						
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISLA/views'));
			$this->file_factory->setPath('CSS', '/custom/ISLA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISLA/views/');
		}
		
	} 