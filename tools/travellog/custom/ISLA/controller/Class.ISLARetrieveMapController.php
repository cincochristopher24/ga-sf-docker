<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	ISLARetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for ISLA	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ISLA/views'));
		$file_factory->setPath('CSS', '/custom/ISLA/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ISLA/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}