<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');
	
	class ISLAAssignStaffController extends COBRANDAssignStaffController{
		
		public function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ISLA/views'));
			$this->mFactory->setPath('CSS', '/custom/ISLA/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/ISLA/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}