<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDFeedbackController.php');
class SOLABROADFeedbackController extends COBRANDFeedbackController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SOLABROAD/views'));
		$this->file_factory->setPath('CSS', '/custom/SOLABROAD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SOLABROAD/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>