<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewMemberController.php');
	
	class SOLABROADNewMemberController extends COBRANDNewMemberController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SOLABROAD/views'));
			$this->file_factory->setPath('CSS', '/custom/SOLABROAD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SOLABROAD/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
