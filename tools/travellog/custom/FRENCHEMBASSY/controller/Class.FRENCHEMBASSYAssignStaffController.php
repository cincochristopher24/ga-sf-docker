<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDAssignStaffController.php');

	class FRENCHEMBASSYAssignStaffController extends COBRANDAssignStaffController {

		function __construct(){
			parent::__construct();
			$this->mFactory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/FRENCHEMBASSY/views'));
			$this->mFactory->setPath('CSS', '/custom/FRENCHEMBASSY/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/custom/FRENCHEMBASSY/views/');
		}

		function performAction(){
			parent::performAction();
		}
	}
?>