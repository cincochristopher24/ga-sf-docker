<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSurveyFormController.php');
	
 	class ITEFLASurveyFormController extends COBRANDSurveyFormController {
 		function __construct(){
	 		parent::__construct();
	 		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ITEFLA/views'));
			$this->file_factory->setPath('CSS', '/custom/ITEFLA/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ITEFLA/views/');
 		}
 	}