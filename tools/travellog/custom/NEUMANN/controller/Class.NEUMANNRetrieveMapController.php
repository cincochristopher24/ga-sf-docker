<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	NEUMANNRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for NEUMANN	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/NEUMANN/views'));
		$file_factory->setPath('CSS', '/custom/NEUMANN/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/NEUMANN/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}