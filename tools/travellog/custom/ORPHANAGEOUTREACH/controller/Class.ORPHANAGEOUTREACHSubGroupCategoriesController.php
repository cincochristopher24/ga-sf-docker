<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDSubGroupCategoriesController.php');
	
 	class ORPHANAGEOUTREACHSubGroupCategoriesController extends COBRANDSubGroupCategoriesController {
 		
 		function __construct()
 		{
	 		parent::__construct();
	 		
	 		$file_factory = FileFactory::getInstance();
	 		$file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ORPHANAGEOUTREACH/views'));
			$file_factory->setPath('CSS', '/custom/ORPHANAGEOUTREACH/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ORPHANAGEOUTREACH/views/');
 		
			Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));	
 		}
 		
 	}