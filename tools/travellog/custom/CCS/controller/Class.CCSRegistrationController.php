<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDRegistrationController.php");
	
	class CCSRegistrationController extends COBRANDRegistrationController{
		
		function __construct(){
			
			parent::__construct();
						
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CCS/views'));
			$this->file_factory->setPath('CSS', '/custom/CCS/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CCS/views/');
		}
		
	} 