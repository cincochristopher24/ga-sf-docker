<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDNewMemberController.php');
	
	class ELIABROADNewMemberController extends COBRANDNewMemberController{
		
		public function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/ELIABROAD/views'));
			$this->file_factory->setPath('CSS', '/custom/ELIABROAD/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ELIABROAD/views/');
		}
		
		public function performAction(){
			parent::performAction();
		}
	}
?>
