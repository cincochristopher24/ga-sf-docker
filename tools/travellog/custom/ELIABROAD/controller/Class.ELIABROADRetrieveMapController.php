<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	ELIABROADRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for ELIABROAD	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/ELIABROAD/views'));
		$file_factory->setPath('CSS', '/custom/ELIABROAD/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/ELIABROAD/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}