<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	CENTRALCOLLEGERetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for CENTRALCOLLEGE	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/CENTRALCOLLEGE/views'));
		$file_factory->setPath('CSS', '/custom/CENTRALCOLLEGE/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CENTRALCOLLEGE/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}