<?php
	require_once("travellog/custom/COBRAND/controller/Class.COBRANDSubGroupsController.php");
	
	/**
	 * The subgroups controller of a cobrand.
	 */
	class HUMANITYEXCHANGESubGroupsController extends COBRANDSubGroupsController {
		
		/**
		 * Constructs a new HUMANITYEXCHANGE subgroups controller.
		 */
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate("LayoutMain", array('path' => "travellog/custom/HUMANITYEXCHANGE/views"));
			$this->file_factory->setPath("CSS", "/custom/HUMANITYEXCHANGE/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/custom/HUMANITYEXCHANGE/views/"); 
		}
		
		/**
		 * Performs the necessary action.
		 * 
		 * @return void
		 */
		function performAction(){ 
			parent::performAction();
		}
	}