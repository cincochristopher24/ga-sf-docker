<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	VCURetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for VCU	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/VCU/views'));
		$file_factory->setPath('CSS', '/custom/VCU/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VCU/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}