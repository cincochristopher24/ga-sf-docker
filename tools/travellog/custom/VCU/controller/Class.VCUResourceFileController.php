<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDResourceFileController.php');
class VCUResourceFileController extends COBRANDResourceFileController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/VCU/views'));
		$this->file_factory->setPath('CSS', '/custom/VCU/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VCU/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>