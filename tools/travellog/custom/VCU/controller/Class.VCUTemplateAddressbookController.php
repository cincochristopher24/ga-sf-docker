<?php  
	/**
	 * @(#) Class.VCUTemplateAddressbookController.php
	 * 
	 * Autogenerated by GoAbroad Code Generator 1.0
	 *
	 * @version 1.0 Jun 06, 2009 
	 */	
	
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDTemplateAddressbookController.php');
	
	/**
	 * TemplateAddressbook cobrand controller class for VCU.
	 */
	class VCUTemplateAddressbookController extends COBRANDTemplateAddressbookController {
		
		function __construct(){
			parent::__construct();
			$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/VCU/views'));
			$this->file_factory->setPath('CSS', '/custom/VCU/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/VCU/views/'); 
		}
		
	}