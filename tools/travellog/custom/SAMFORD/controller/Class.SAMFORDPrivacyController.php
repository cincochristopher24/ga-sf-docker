<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDPrivacyController.php');
class SAMFORDPrivacyController extends COBRANDPrivacyController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SAMFORD/views'));
		$this->file_factory->setPath('CSS', '/custom/SAMFORD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SAMFORD/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>