<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDAccountDeactivationController.php');

class SAMFORDAccountDeactivationController extends COBRANDAccountDeactivationController
{
	public function __construct()
	{
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/SAMFORD/views'));
		$this->file_factory->setPath('CSS', '/custom/SAMFORD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SAMFORD/views/'); 
	}
}