<?php   
	
require_once('travellog/custom/COBRAND/controller/Class.COBRANDRetrieveMapController.php');

class	SAMFORDRetrieveMapController extends COBRANDRetrieveMapController {
	
	/**
	 * Initializes controller for SAMFORD	 */
	public function __construct() {
		parent::__construct();
		
		$file_factory = FileFactory::getInstance();
		$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/SAMFORD/views'));
		$file_factory->setPath('CSS', '/custom/SAMFORD/css/');
		$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SAMFORD/views/'); 
		
		Template::setMainTemplate($file_factory->getTemplate('LayoutMain'));		
	}
	
}