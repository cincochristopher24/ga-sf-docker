<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDActivationSupportController.php');
class SAMFORDActivationSupportController extends COBRANDActivationSupportController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SAMFORD/views'));
		$this->file_factory->setPath('CSS', '/custom/SAMFORD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SAMFORD/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>