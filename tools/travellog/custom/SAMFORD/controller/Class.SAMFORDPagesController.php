<?php
require_once('travellog/custom/COBRAND/controller/Class.COBRANDPagesController.php');
class SAMFORDPagesController extends COBRANDPagesController {

	function __construct(){
		parent::__construct();
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/SAMFORD/views'));
		$this->file_factory->registerTemplate('TermsDefault', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('TermsAdvisor', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->registerTemplate('PrivacyPolicy', array('path' => 'travellog/custom/COBRAND/views'));
		$this->file_factory->setPath('CSS', '/custom/SAMFORD/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/SAMFORD/views/');
	}

	function performAction(){
		parent::performAction();
	}

}
?>