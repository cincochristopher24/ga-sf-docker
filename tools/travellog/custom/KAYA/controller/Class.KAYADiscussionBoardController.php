<?php
	require_once('travellog/custom/COBRAND/controller/Class.COBRANDDiscussionBoardController.php');

	class KAYADiscussionBoardController extends COBRANDDiscussionBoardController {

		public function __construct(){
			parent::__construct();
			$file_factory = FileFactory::getInstance();
			$file_factory->registerTemplate('LayoutMain', array('path' => 'travellog/custom/KAYA/views/'));
			$file_factory->setPath('CSS', '/custom/KAYA/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/custom/KAYA/views/');
		}
	}