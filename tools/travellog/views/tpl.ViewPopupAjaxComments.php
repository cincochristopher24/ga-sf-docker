<?php
/*
 * Created on 09 25, 2006
 * Purpose: template to list comments
 */

require_once('Class.HtmlHelpers.php');
require_once('Class.GaDateTime.php');
$d = new GaDateTime();

?>
<div id="comments2" class="section">

  <div class="content" style="background-color:#DDDDDD;font-size:10px;font-family:verdana">
  
  <h2>Photo Comments</h2>
    <? if (0 < count($comments)) : ?>  	  	  	
  		
  		<? foreach($comments as $comment ) : 
	  			try {
	  		    	$tp = new TravelerProfile($comment->getAuthor());
	  			}	
	  		    catch (Exception $exc) {}	 ?>
			
				<div class="author">
					<? $gID = 0;?>										
	 				<?if ($gID > 0 ):?>
	 					<?$ag = new AdminGroup($gID);?>
	 					<a href="/group.php?gID=<?=$gID?>"  class="user_thumb"><img src="<?=$ag->getGroupPhoto()->getPhotoLink() ?>" height="37" width="37" alt="User Profile Photo" /></a>					
				 		<a href="/group.php?gID=<?=$gID?>">
				 			<?if (strlen($ag->getName()) <= 10):?>
				 				<?=$ag->getName()?>
				 			<?else:?>
				 				<?=substr($ag->getName(),0,8) . '...'  ?>
				 			<?endif;?>	
				 		</a>
	 				<?else:?>					
						<a href="/profile.php?action=view&amp;travelerID=<?=$tp->getTravelerID()?>" class="user_thumb"><img src="<?=$tp->getPrimaryPhoto()->getThumbnailPhotoLink() ?>" height="37" width="37" alt="User Profile Photo" /></a>					
				 		<a href="/profile.php?action=view&amp;travelerID=<?=$tp->getTravelerID()?>">				 		
				 		<?if (strlen($tp->getUserName()) <= 10):?>
				 			<?=$tp->getUserName()?>
				 		<?else:?>
				 			<?=substr($tp->getUserName(),0,8) . '...'  ?>
				 		<?endif;?>	
				 		</a>
				 	<?endif;?>					 	
			 	</div>
			 	<div class="comment_content">	
				 	<div class="meta date">
				 			<?= $d->set($comment->getTheDate())->friendlyFormat() ?>
				 	</div>
				 	
				 			<div id="thecommenttext<?=$comment->getCommentID()?>">	
								<?php echo preg_replace('/\n/', '<br />', $comment->getTheText()); ?>
							</div>
							
							<div id="actions<?=$comment->getCommentID()?>" class="actions">
						 			<? if ($travelerID == $tp->getTravelerID()) : ?>
						 						<a href="javascript:void(0)" onclick="PhotoService.editComents(<?=$comment->getCommentID()?>,<?=$contextID?>,1)">Edit</a>|
												<a href="javascript:void(0)" onclick="PhotoService.deletecomment(<?=$comment->getCommentID()?>,<?=$contextID?>,1)">Delete</a>
						 			<? elseif (isset($okToDelete)): ?>
						 						<a href="javascript:void(0)" onclick="PhotoService.deletecomment(<?=$comment->getCommentID()?>,<?=$contextID?>,1)">Delete</a>
						 					 	
						 			<? endif; ?>
						 	</div>
				 	
				 	<div id="commenteditloading<?=$comment->getCommentID()?>" style="display:none;color:red;padding-top:10px;">
				 		<img src="/images/loadingAnimation.gif"/>
				 	</div>
				 	
				 	<div class="clear"></div>
				 </div>
				
				 <div style="background-color:#fff;height:2px;margin-bottom:8px;"></div> 	
			
  		<? endforeach; ?>
  
  <?else:?>		
  		
  		There's no comment yet on this Photo!
  
  		  			
  <? endif; ?>
  
  
  
  <br/>
  
  <? if (isset($context) && isset($contextID) && $travelerID > 0 && $canAddComment):?>
  	<h3><a name="/comment_form" id="comment_form"></a>Add Comment</h3>	
  	<form action="/comment.php?action=savenew" method="post" id="frmcomment" name="frmcomment">
  		<ul class="form">
  			<li>  				
				<textarea name="comment" id="comment" cols="50" rows="6"></textarea>
			</li>
			<li class="actions">
					<input type="hidden" name="context" 	id= "context" 	value="<?=$context?>" />
					<input type="hidden" name="contextID" 	id= "contextID" value="<?=$contextID?>" />
					<input type="hidden" name="photocat" 	id= "photocat" 	value="<?=$photoCat?>" />	<!--- specifically used for photos -->
					<input type="hidden" name="genID" 		id= "genID" 	value="<?=$genID?>" />
					<input type="hidden" name="loginid"   	id= "loginid"   value="<?=$travelerID?>"/>
					<input type="hidden" name="popup" id= "popup"   value="1"/>
					<input type="hidden" name="ajaxaction"   	id= "ajaxaction"   value="savenewcomments"/>
						
					<div id="addcommentloading" style="display:none;">
						<img src="/images/loadingAnimation.gif"/>
					</div>
					
					<div>
						
						<input type="button" onclick="<?=$jsName?>" class="submit" value="Save" />
					</div>
				
			</li>
		</ul>
		<div class="clear"></div>
	</form>
  <? elseif ($travelerID == 0): ?>
  	<h3><a name="comment_form" id="comment_form"></a>Add Comment</h3>
  	<p>You have to be <a href="/login.php?redirect=<?=$envURL?>">logged in</a> to leave comments.<br />
	  Not yet a member? <a href="/register.php">Register</a> now&#8212;it&#8217;s fast, easy and totally free.
	</p>
  <? endif;?>	
  </div>
</div>
