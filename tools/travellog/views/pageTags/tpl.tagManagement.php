<?php
	$tagLabels = json_encode($groupTagData);
	Template::includeDependent("<script>var GroupTagLabels = $tagLabels;</script>");
?>
<p class="confirmation" id="confirmation" style="display:none;"></p>
<p class="notice" style="display:none;"></p>

<p class="error_notice" id="error_notice" style="display:none;"></p>								

<div class="section" style="">
	<h2>
		<?php $tagCounterString = (0 < $tagCount) ? 'You have <strong id="tag_counter">'.$tagCount.'</strong> tag(s)' : 'You have no tag'; ?>
		<span class="toLower" id="span_tag_counter"><?=$tagCounterString?></span>
		<?php if($searchTagKeyword != ""):?>
			<span class="header_actions" id="back_to_view_all_tags"><a href="/tagmanagement.php" >Back to View All Tags</a></span>
		<?php endif;?>
	</h2>
	
	<div class="listControls">
		<div class="left">					 
			<button class="alpha omega" name="buttonNewTag" id="<?=$gID?>">
				<span>New Tag</span>
			</button>
			<img id="loading_image" src="/images/loading_small.gif" alt="loading..." width="16" height="16" style="display:none; vertical-align: middle; margin: 0 5px;"/>
		</div>
		<div class="right">
			<label for="sortOptions">Sort Tags</label>
			<select id="sortOptions">
				<option value='weight DESC'>No. Of Groups Descending</option>
				<option value='weight ASC'>No. Of Groups Ascending</option>
				<option value='name ASC'>Tag Name Ascending</option>
				<option value='name DESC'>Tag Name Descending</option>								
			</select>
		</div>				
	</div><!-- end listControls -->				
	<div class="content" id="content">
		<?php echo $tagsList ?>
	</div><!-- end content -->
</div>
<script type="text/javascript">
	tagManagementConfig = {
		'gID' : <?= $gID ?>
	}
</script>