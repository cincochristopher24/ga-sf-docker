<?php echo $profile->render(); ?>
<?php echo $subNavigation->show(); ?>
<div class="layout_2" id="content_wrapper">
		<div class="mainSectionHead">		
			<h2 class="title">Tag Manager</h2>	
			<p class="details">
				<?php if(isset($GLOBALS['CONFIG'])):?>
					<?php $formAction = '/group-pages.php'?>
				<?php else: ?>
					<?php $formAction = '/group-pages.php?gID='.$gID?>
				<?php endif; ?>
				In here you can manage your tags. <a href="<?=$formAction?>" class="strong">Go back to the list of groups &raquo;</a>
			</p>
		</div>
		
	<div id="wide_column">
		<?// for the tag management?>
		<?php if( isset($tagManagement) ): ?>
			<?php echo $tagManagement; ?>
		<?php endif; ?>
	</div>
	
	<div id="narrow_column">
		<?php echo $tplSearchTag ?>
	</div>
</div>