<div>
	<table class="tagMgrList">
		<thead>
			<tr>
				<th class="tagCol">
					Tag
				</th>
				<th class="tagWeight">
					No. of groups
				</th>
				<th>
					Actions
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($tags as $tag):?>
				<tr>
					<td >
						<a href="#" class="strong" id="tagName_link_<?=$tag->getID()?>"><?=$tag->getName()?></a>
						<div style="display:none;">
							<input type="text" class="tagTextBox" name="fieldName" id="tag_<?=$tag->getID()?>" value="<?=$tag->getName()?>" />
						</div>
					<?/*	<input class="tagTextBox" type="hidden" id="tag_<?=$tag->getID()?>" readonly="yes" disabled="yes" style="border: 0px solid #000000;" value="<?=$tag->getName()?>" /> */?>
					</td>
					<td>
						<span class="strong" id="number_of_groups_<?=$tag->getID()?>"><?=$tag->getWeight()?></span>
					</td>
					<td>
						<p class="actions" id="manageTagActions" name='manageTagActions'>
							<a class="alpha" name='manageTagAction' id="edit_<?=$tag->getID()?>">Rename</a>
							<a class="omega" name='manageTagAction' id="delete_<?=$tag->getID()?>">Delete</a> 
						</p>															
					</td>
				</tr>
			<?php endforeach; ?>
			
		</tbody>	
	</table>	

</div>
<script type="text/javascript">
	tagManagement.init();
</script>