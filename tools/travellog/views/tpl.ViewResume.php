<?php 
	
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'My Passport');
	// Template::includeDependentJs('/js/teachjs.js');
	
	/**
	 * Added by naldz (Nov 20,2006)
	 * This is for the subnavigation links
	 */	
	
	$subNavigation->show();
?>

<div class="layout_2" id="content_wrapper">
	
	<div id="wide_column">
		<div class="section" id="resume">
			<div class="head_left"><div class="head_right"></div></div>
			<div class="content">
				<div id="resume_photo">
					<img src ="<?= $tmpphoto ?>" class="resume_photo" alt="resume photo" />
				</div>
				<h1><?= $fname ?> <?= $lname ?></h1>
				<div id="resume_details">
				<p>
					<?= $email ?><br />
					<?php if($gender != ""): ?>
						<?= $gender ?>, 
					<?php endif; ?>
					
					<?= $age ?> years old
					
					<br />
				
				<?php if($address1 != null): ?>
					<?= $address1 ?><br />
				<?php endif; ?>
				
				<?php if($address2 != null): ?>
					<?= $address2 ?><br />
				<?php endif; ?>
				
				<?php if($phone != null): ?>
					Phone
					<?= $phone ?>
				<?php endif; ?>
				</p>
				
				
				
				<h2>Educational Background</h2>
				<h3>Highest Education</h3>
				<dl class="resume_info">
				<dt>Level</dt>
					<dd>	 
				        <?= htmlspecialchars($acadDegree) ?>
				    </dd>
				
				<dt>Major</dt>
					<dd>  
				            <?= htmlspecialchars($interntype) ?> 
				    </dd>   
				<dt>University</dt>
					<dd>
						<?= htmlspecialchars($univName) ?>
					</dd>
				<dt>Location</dt>
					<dd>
						<?= htmlspecialchars($univlocation) ?>
					</dd>
				</dl>
				<?php if($acadDegree2 != null): ?>
					
					<h3>2nd Highest Educational Level</h3>
					<dl class="resume_info">
					<dt>Level</dt>
						<dd>
							<?= htmlspecialchars($acadDegree2) ?>
					    </dd>
					<?php if($interntype2 != null): ?>
					<dt>Major</dt>
						<dd>  
					            <?= htmlspecialchars($interntype2) ?> 
					    </dd> 
					<?php endif; ?>
					<?php if($univName2 != null): ?>
					<dt>University</dt>
						<dd>
							<?= htmlspecialchars($univName2) ?>
						</dd>
					<?php endif; ?>
					<?php if($univlocation2 != null): ?>
					<dt>Location</dt>
						<dd>
							<?= htmlspecialchars($univlocation2) ?>
						</dd>
					<?php endif; ?>
					</dl>
				<?php endif; ?>
				
				<?php if(count($certnames)): ?>
				<h2>Certifications</h2>
				<table class="resume_info" cellspacing="1">
					<tr>
						<th>Certification</th>
						<th>Year Obtained</th>	
					</tr>
					<?php for($i=0;$i<count($certnames);$i+=2): ?>					
					<tr>
						<td>
							<?= $certnames[$i] ?>
						</td>
						<td>
							<?= $certnames[$i+1] ?>
						</td>
					</tr>	
					
					<?php endfor; ?>
				
				</table>
				<?php endif; ?>
				
				<?php if ($viewemploy == 1): ?>
				<h2>Professional Details</h2>
				<dl class="resume_info">
				<dt>Are you currently employed?</dt>
					<dd> 
						<?php if ($viewemploy == 1): ?>
							Yes
						<?php else : ?>
							No
						<?php endif; ?>
					</dd>
				<?php if($viewemploy == 1 && $viewteach == 1): ?>
				<dt>Are you a teacher?</dt>
					<dd>
						<?php if($viewteach == 1): ?>
							Yes
						<?php else : ?>
							No
						<?php endif; ?>
					</dd>
				<?php endif; ?>
				<?php if($currJob != null): ?>
				<dt>Current Job</dt>
					<dd>
						<?= htmlspecialchars($currJob) ?>
					</dd>
				<?php endif; ?>
				</dl>
				<?php endif; ?>
				
				<?php if ($viewteach == 1): ?>
				 <h2>Teacher&#8217;s Profile</h2>
				<dl class="resume_info">
					<dt>Current Teaching Position</dt>
						<dd> 
							
					      		<?= htmlspecialchars($teachingposition) ?>
					      				      
						</dd>
					<dt>English Proficiency</dt>
						<dd> 
							<?= htmlspecialchars($englishproficiency) ?>				      		
						</dd>
					<dt>Years of Experience</dt>
						<dd>
							<?= htmlspecialchars($teachingyears) ?>
						</dd>
					<dt>Preferred Teaching Level</dt>
						<dd>
							<?= htmlspecialchars($preferredteachinglevel) ?>
						</dd>
				</dl>
				<?php endif; ?>
				  
				<h2>Job Preferences</h2>
				<dl class="resume_info">
				<dt>Preferred Employment</dt>
					<dd> 
				        <?php if ($prefEmployment == 1): ?>
				          Part Time 
				        <?php elseif ($prefEmployment == 2): ?>
				          Full Time 
				        <?php elseif ($prefEmployment == 3): ?>
				          Part Time or Full Time 
				        <?php endif; ?>
				    </dd>
				<dt>Job Type</dt>
					<dd>
						<?= htmlspecialchars($jobtype) ?>
					</dd>
				<dt>Preferred Country</dt>
					<dd> 
				        <?= htmlspecialchars($prefCountry) ?>
					</dd>
				<?php if($prefCity != null): ?>
				<dt>Preferred City</dt>
					<dd>
						<?= htmlspecialchars($prefCity) ?>
					</dd>
				<?php endif; ?>
				<dt>When can you begin working?</dt>
					<dd>
						<?= htmlspecialchars($workWhen) ?>
					</dd>
				</dl>
				
				<?php if ($workexpcount): ?>
				<h2>Work Experience</h2>
				<table width="100%" border="0" cellspacing="1" class="resume_info2">
				<tr>
					<th>Position</th>
					<th>Employer</th>
					<th>Job Description</th>
					<th>Length of Service</th>
				</tr>
				    
				    <?php foreach($qworkexp as $value): ?>
					    <tr>
					      <td><?= $value->getJobPosition() ?></td>
					      <td><?= $value->getEmployer() ?></td>
					      <td><?= $value->getJobDescription() ?></td>
					      <td><?= $value->getLenServ() ?></td>
					    </tr>
				    <?php endforeach; ?>
				    
				</table>  
				<?php endif; ?>
				
				<?php if($moreDetails != null): ?> 
				<h2>Additional Details</h2>
				<!--<textarea readonly cols="50" rows="10" id="resume_addinfo">-->
					<?= HtmlHelpers::Textile($moreDetails) ?>
				<!--</textarea>-->
				<?php endif; ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="foot"></div>
		</div>
	</div>
	
	<div id="narrow_column">
		<div class="section" id="quick_tasks">
			<div class="head_left"><div class="head_right"></div></div>
			<div class="content">
				<ul class="actions">
					<?php if($resourcefilecount): ?>
						<li><a href="resourcefiles.php?action=view&amp;cat=resume&amp;genID=<?= $travelerID ?>" class="button">Remove / Replace Uploaded Resume</a></li>
					<?php elseif (0 == $resourcefilecount): ?>
						<li><a href="resourcefiles.php?action=add&amp;cat=resume&amp;genID=<?= $travelerID ?>" class="button">Upload Resume</a></li>
					<?php endif; ?>
					<li><a href="resume.php?action=edit" class="button">Edit Resume</a></li>
				</ul>
			</div>
			<div class="foot"></div>
		</div>
	</div>
	
	
	<div class="clear"></div>
</div>