<div class="section" id="sub_groups">

	<h2>
	  <span><?= $label ?></span>
	  <br />
	  <span class="header_actions long_links">
	    <? if($isAdminLogged): // show add link if the admin is the current logged in user ?>
	      <a href="/group.php?mode=add&amp;pID=<?=$grpID?>&amp;addtoPNP=1">Add Group</a> |
	    <? endif; ?>
	    
	    <a href="/customgroupsection.php?gID=<?=$grpID?>" ><?= $link_label ?></a>
	  </span>
	
	</h2>
	
	
	<div class="content">		
		<? if (count($grpSubGroups)) { ?>
			<ul class="groups">
					<?  for($idx=0;$idx<count($grpSubGroups);$idx++){
							$eachsubgroup = $grpSubGroups[$idx];
							
							// photo tag
							$cntPhotos = $eachsubgroup->getPhotos(true);
							$albums = $eachsubgroup->getPhotoAlbums();
							if ($albums != NULL){
								foreach($albums as $each)
								  $cntPhotos = $cntPhotos + $each->getCountPhotos();
							}
								  
							$photo_tag_cnt = (1 < $cntPhotos)  ? $cntPhotos.' Photos' : $cntPhotos.' Photo';
							$photo_tag_cnt = (0 == $cntPhotos) ? "" : $photo_tag_cnt;
							// end of photo tag
							
							// journal tag
							$cntJournals = count($eachsubgroup->getTravels());
						  
						  $journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journals' : $cntJournals.' Journal';
							$journal_tag_cnt = (0 === $cntJournals) ? "" : $journal_tag_cnt;
							// end of journal tag
					    
					    // members tag
					    $cntMembers = $eachsubgroup->getMembers(NULL, NULL, NULL, true);
							$members_tag_cnt = (1 < $cntMembers) ? $cntMembers.' Members' : $cntMembers.' Member';
							$members_tag_cnt = (0 === $cntMembers) ? '' : $members_tag_cnt;
							// end of members tag
							
							$group_name = (20 < strlen($eachsubgroup->getName()) && !strpos($eachsubgroup->getName()," ")) ? substr($eachsubgroup->getName(),0,20) . "..." : $eachsubgroup->getName();
					?>			

					<li>

						<a title="View Group Profile" class="thumb" href="<?=$eachsubgroup->getFriendlyURL()?>"><img class="pic" width="65px" height="65px" alt="Group Emblem" src="<?=$eachsubgroup->getGroupPhoto()->getphotolink('thumbnail') ?>"  /></a>
							
						<div class="details">
						<a class="groupname" href="<?=$eachsubgroup->getFriendlyURL()?>" title="<?=$eachsubgroup->getName()?>"><?=$group_name?></a>

						<p>
						<?= $photo_tag_cnt ?>   <? if ($cntPhotos) { ?> <br /> <? } ?>
						<?= $members_tag_cnt ?> <? if ($cntMembers) { ?> <br /> <? } ?>
						<?= $journal_tag_cnt ?>
						
						<?
							if (0 == $cntMembers) {
								/** edited by ianne - 11/13/2008
								 *  added prompt and referer page to join group link
								 *  hide join link if logged user has pending request to join subgroup
								 */
								if (NULL != $loggedUser && FALSE == $loggedUser->isAdvisor()):
									$validGrpAccess = array(GroupAccess::$OPEN, GroupAccess::$OPEN_APPROVAL);
									if ( $isLogged && !$isAdminLogged  && in_array($eachsubgroup->getGroupAccess(), $validGrpAccess) && in_array ($eachsubgroup->getParent()->getGroupAccess(), $validGrpAccess ) ):
										if(!$eachsubgroup->isInRequestList($loggedUser)):
											$message = 'You are about to join the '.strtolower($eachsubgroup->getLabel()).' '.$eachsubgroup->getName().'. Your request is subject to the administrator&lsquo;s approval and will be sent after clicking Join. You will be able to interact with this group once your request has been approved.';
											$refLink = '&amp;ref=groupsubgroup';
											
											if($eachsubgroup->getGroupAccess() == GroupAccess::$OPEN):
												$message = 'You are about to join ' . $eachsubgroup->getName().'. You will automatically be a member of the group after clicking Join.';
											endif;
											
											$strlinkjoin = ' Do you want to <a href="javascript:void(0)" onclick="CustomPopup.initialize(\'Join '.$eachsubgroup->getName().' ?\',\'Are you sure you want to join the '.strtolower($eachsubgroup->getLabel()).' '.$eachsubgroup->getName().'?\',\'/membership.php?mode=join&amp;gID='. $eachsubgroup->getGroupID().$refLink.'&confirm\',\'Join\',\'0\',\''.$message.'\');CustomPopup.createPopup();">join</a> this group?';
										endif;				
									endif;
								endif;
					
								echo 'Group has no members yet.' ; echo (isset($strlinkjoin)) ? $strlinkjoin : '';				
							}
						?>						
						</p>
					
						<? if ($isAdminLogged && $cntMembers): ?>
						<p>
							<a href="/messages.php?act=messageGroupStaff&gID=<?= $eachsubgroup->getGroupID() ?>">Message Staff</a>
						</p>
						<? endif; ?>					
						</div>
					</li>		
					<? } ?>
			
					
			
				</ul>
				<? } ?>
			
					<? if ($isAdminLogged): ?>
				
						
						<? if (0 == $origCountSubGroups): ?>
						<p class="side_help_text">	No groups yet.</p>
						
						<? elseif (0 == count($grpSubGroups)): ?>
						<p class="side_help_text">	No featured groups. </p>
							
							<? if (0 < $origCountSubGroups): ?>
								<a href="/customgroupsection.php?gID=<?=$grpID?>" >View all <?=$origCountSubGroups?> groups.</a>
							<? endif; ?>
						
						<? endif; ?>
						
					
				
					<? endif; ?>
						
		</div>
		
</div>
