<?php 
	
Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('page_location', 'My Passport');
Template::includeDependentCss('/css/form.resume.css');
Template::includeDependentJs ( '/min/f=js/prototype.js');
Template::includeDependentJs('/js/interactive.form.js');
Template::includeDependentJs('/js/form.teachjs.js');
Template::includeDependentJs('/js/tools/tools.js');
Template::includeDependentJs("/js/moo.ajax.js");   
Template::includeDependentJs("/js/moo.fx.js");   
Template::includeDependentJs("/js/moo.fx.pack.js");
	
	
$code = <<<BOF
<script type="text/javascript">
//<![CDATA[
var hide = function() {
	document.getElementById('accountadd').style.display='none';
}
//]]>

function createWorkExp(){
	
	//get the number of created forms, count the txtElPosition#
	var workFrame = document.getElementById('workExp'); 	
	var workExpEl = workFrame.getElementsByTagName('input');
	var workExpEltxt = workFrame.getElementsByTagName('textarea');
	var elCtrPos = 1;
	var elCtrEmp = 1;
	var elCtrJob = 1;
	var elCtrLen = 1;
	for(var i=0;i<workExpEl.length;i++){		
		if(workExpEl[i].name.substr(0,14)=='txtWE1Position'){
			elCtrPos++;
		}
		else if(workExpEl[i].name.substr(0,14)=='txtWE1Employer'){
			elCtrEmp++;	
		}
		else if(workExpEl[i].name.substr(0,13)=='txtWE1LenServ'){
			elCtrLen++;
		}
		
	}	
	for(var i=0;i<workExpEltxt.length;i++){
		if(workExpEltxt[i].name.substr(0,13)=='txtWE1JobDesc'){
			elCtrJob++;
		}
	}
	var newHtml = '';
	newHtml += '<h2><a name="work'+elCtrPos+'" id="work'+elCtrPos+'"></a>Work Experience '+elCtrPos+'</h2>';
	newHtml += '<li class="form_set">';
	newHtml += '<label for="txtWE1Position'+elCtrPos+'">Position</label>';
	newHtml += '<input name="txtWE1Position'+elCtrPos+'" value="" class="text ga_interactive_form_field" size="50" maxlength="50" id="txtWE1Position'+elCtrPos+'" type="text">';
	newHtml += '</li>';
	newHtml += '<li class="form_set">';
	newHtml += '<label for="txtWE1Employer'+elCtrEmp+'">Employer</label>';
	newHtml += '<input name="txtWE1Employer'+elCtrEmp+'" value="" class="text ga_interactive_form_field" size="50" maxlength="50" id="txtWE1Employer'+elCtrEmp+'" type="text">';
	newHtml += '</li>';
	newHtml += '<li class="form_set">';
	newHtml += '<label for="txtWE1JobDesc'+elCtrJob+'">Job Description</label>';
	newHtml += '<textarea class="ga_interactive_form_field" name="txtWE1JobDesc'+elCtrJob+'" cols="48" rows="5" id="txtWE1JobDesc'+elCtrJob+'"></textarea>';
	newHtml += '</li>';
	newHtml += '<li class="form_set">';
	newHtml += '<label for="txtWE1LenServ'+elCtrLen+'">Length of Service</label>';
	newHtml += '<input name="txtWE1LenServ'+elCtrLen+'" value="" class="text ga_interactive_form_field" size="50" maxlength="50" id="txtWE1LenServ'+elCtrLen+'" type="text">';
	newHtml += '</li>';
	workFrame.innerHTML += newHtml;	 
	document.getElementById("hidwecount").value++;	
	document.location.href = '#work'+elCtrPos+'';
 }
 
 
function delworkexp(divID,travelerID,workexperienceID){
	
	new ajax('resume.php?action=delete', {postBody: 'travelerID='+travelerID+'&workexperienceID='+workexperienceID, update: $(divID) } );
	//get the div to be removed
	var remDiv = $(divID);
	remDiv.parentNode.removeChild(remDiv);
	var tmpCtr = 0;
	for(var i=0;i<=document.getElementById("hidwecount").value;i++){
		if( document.getElementById("labelid"+i) ){
			tmpCtr++;
			document.getElementById("labelid"+i).innerHTML = '<h2><a name="work'+tmpCtr+'" id="work'+tmpCtr+'"></a>Work Experience '+tmpCtr+'</h2>';
			$("labelid"+i).id = "labelid"+tmpCtr;			
			//update the names of all the elements related to the current label
			document.frm_AddEditResume['hidwID'+i].name = 'hidwID'+tmpCtr;
			document.frm_AddEditResume['txtWE1Position'+i].name = 'txtWE1Position'+tmpCtr;
			document.frm_AddEditResume['txtWE1Employer'+i].name = 'txtWE1Employer'+tmpCtr;
			document.frm_AddEditResume['txtWE1JobDesc'+i].name = 'txtWE1JobDesc'+tmpCtr;
			document.frm_AddEditResume['txtWE1LenServ'+i].name = 'txtWE1LenServ'+tmpCtr;
		}
	}
	//update the counter element	
	document.getElementById("hidwecount").value = --document.getElementById("hidwecount").value;
}
 
</script>
BOF;

$incjs = <<<BOF
<script type="text/javascript">

 window.onload = function(){
 		toggleOptions();
 }

</script>
BOF;

Template::includeDependent($code);

if($radIsTeacher == 2)
Template::includeDependent($incjs);
?>


<?php 
	/**
	 * Added by naldz (Nov 20,2006)
	 * This is for the subnavigation links
	 */	
	$subNavigation->show();
?>
<div class="layout_2" id="content_wrapper">
	
	<div id="wide_column">
		
		<div class="section" id="resume">
			<h1><span><?= $labelaction ?></span></h1>
			<div class="content">
		
				<form action="resume.php?action=<?= $act ?>" method="post" enctype="multipart/form-data" id="frm_AddEditResume" name="frm_AddEditResume" class="interactive">
			
					<?php if ($errorcode != 0): ?>
						<div class="errors">
							<p>There are some problems that need some attention before proceeding. </p>
					
							<ul class="error_list">			
								<?php foreach($errors as $error): ?>
										<li><?= $error ?></li>
								<?php endforeach; ?>
							</ul>
						
						</div>
					<?php endif; ?>
			
			
					<?php if($labelaction == "Edit Resume"): ?>
						<div id="resume_photo">
							<img src ="<?= $tmpphoto ?>" class="resume_photo" alt="resume photo"/>
							<ul class="actions">
								<?php if ($photocount != null): ?>
									<li>
										<a href="photomanagement.php?cat=resume&amp;action=edit&amp;genID=<?= $travelerID ?>&amp;photoID=<?=$photocount[0]->getPhotoID()?>">Edit Photo</a>
										|
										<a href="photomanagement.php?cat=resume&amp;action=delete&amp;genID=<?= $travelerID ?>&amp;photoID=<?=$photocount[0]->getPhotoID()?>" onclick="javascript:return confirm('Do you want to REMOVE this Photo?');">Delete Photo</a>
									</li>
								<?php else: ?>
									<li><a href="photomanagement.php?cat=resume&amp;action=add&amp;genID=<?= $travelerID ?>">Upload Photo</a></li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endif; ?>
				
					<p class="help_text">
						Fields with  <span class="required">*</span> indicate required fields.
					</p>
				
				
					<h2>Personal Details</h2>
						<ul class="form">
				
						<li>
							<fieldset>
								<legend><span>Name <span class="required">*</span></span></legend>
								<span>
						
									<?php echo  FormHelpers::CreateTextBox('txtFirstName', $txtFirstName,
											array(
												   'class'     => 'text',
												   'size'      => 50,
												   'maxlength' => 50,
												   'id'		   => 'txtFirstName'
												) 
											); ?>						
									<label for="txtFirstName">First Name <span class="required">*</span> </label>
								</span>
								<span>
									<?php echo  FormHelpers::CreateTextBox('txtLastName', $txtLastName,
											array(
												   'class'     => 'text',
												   'size'      => 50,
												   'maxlength' => 50,
												   'id'		   => 'txtLastName'
												) 
											); ?>
									<label for="txtLastName">Last Name <span class="required">*</span></label>
								</span>
							</fieldset>		
						</li>
				
						<li>
						
							<label for="txtEmail">Email</label>
							<?php echo  FormHelpers::CreateTextBox('txtEmail', $txtEmail,
											array(
												   'class'     => 'text',
												   'size'      => 50,
												   'maxlength' => 50,
												   'id'		   => 'txtEmail',
												   'readonly'  => 'true'
												) 
											); ?>						
						</li>
					
						<li class="frm_date">	
							<fieldset>
								<legend><label for="monthOptions">Birth Date <span class="required">*</span></label></legend>
								<span>
									<select name="monthOptions" id="monthOptions">
										<option value="0">-Month-</option>
											<?php for($month=1; $month<=12; $month++): ?>
												<option value="<?=$month?>"
											
												<? if($monthOptions == $month) print('selected'); ?>>
											
												<?   
													 $today = getdate(); 
													 $m = getdate(mktime( 0, 0, 0, $month, 1, $today['year'] ));
												?>
												<?= $m['month']; ?>					
												</option>
											<? endfor; ?> 
									</select>	
								</span>
							
								<span>
									<select name="lstDay" id="lstDay">
											<option value="0">-Day-</option>
											<?php for($i=1; $i<=31; $i++): ?>
							           
								              <option value="<?= $i ?>" <?php if($lstDay == $i) print('selected'); ?>><?= $i ?></option>
							           
								            <?php endfor; ?>							
									</select>		
								</span>
							
								<span>
									<select name="lstYear" id="lstYear">
										<option value="0">-Year-</option>
									
										<?php 
										$syear = date("Y");
									
										for($w=$syear; $w>=1930; $w--): ?>
							            
								              <option value="<?= $w ?>" <?php if($lstYear == $w) print('selected'); ?>><?= $w ?></option>
							           
								        <?php endfor; ?>
									</select>		
								</span>
								<div class="clear"></div>
							</fieldset>
						</li>
					
						<li>	
							<label for="lstGender">Gender <span class="required">*</span></label>
						
								<select name="lstGender" id="lstGender">
									<option value="0" <? if($lstGender == 0): ?>selected="selected"<? endif; ?>>- Select -</option>
									<option value="1" <? if($lstGender == 1): ?>selected="selected"<? endif; ?>>Male</option>
									<option value="2" <? if($lstGender == 2): ?>selected="selected"<? endif; ?>>Female</option>
								</select>
						</li>
					
						<li>
						
							<label for="txtPhone">Phone</label>
						
							<?php echo  FormHelpers::CreateTextBox('txtPhone', $txtPhone,
											array(
												   'class'     => 'text',
												   'size'      => 50,
												   'maxlength' => 50,
												   'id'		   => 'txtPhone'
												) 
											); ?>						
						</li>
						<li>
							<label for="txaAddress1">Address</label>
						
							<textarea name="txaAddress1" cols="48" rows="3" id="txaAddress1"><?= $txaAddress1 ?></textarea><br>	
							<textarea name="txaAddress2" cols="48" rows="3" id="txaAddress2"><?= $txaAddress2 ?></textarea>
						</li>		
					</ul>
				
					<h2>Educational Background</h2>
				
					<ul class="form">
						<li>	
							<fieldset class="column">
								<legend><label for="lstSelAcadDegree">Highest Educational Level <span class="required">*</span></label></legend>
								<div>
									<span>
										<select name="lstSelAcadDegree" id="lstSelAcadDegree">
											<option value = "0">-SELECT-</option>
											<?= $acaddegreeOptions ?>
										</select>
										<label for="lstSelAcadDegree">Educational Level <span class="required">*</span></label>
									</span>
									<span>							
										<select name= "lstMajor" id="lstMajor">
											<option value = "0">-SELECT-</option>
											<?= $majorOptions ?>
										</select>
										<label for="lstMajor">Major <span class="required">*</span></label>
									</span>
									<div class="clear"></div>
								</div>
								<div>
								<span>
									<?php echo  FormHelpers::CreateTextBox('txtUnivName', $txtUnivName,
													array(
														   'class'     => 'text',
														   'size'      => 50,
														   'maxlength' => 50,
														   'id'		   => 'txtUnivName'
														) 
													); ?>
									<label for="txtUnivName">University <span class="required">*</span></label>
								</span>	
								<span>
									<select name = "lstUnivCountry" id="lstUnivCountry">
										<option value = "0" >-SELECT-</option>
										<?= $univlocation ?>
									</select>
									<label for="lstUnivCountry">Location <span class="required">*</span></label>
								</span>					
								</div>
							</fieldset>
						</li>
					</ul>
					
					<ul class="form">
						<li>
							<fieldset class="column">
								<legend><label for="lstSelAcadDegree2">2nd Highest Educational Level</label></legend>
								<div>
									<span>
										<select name="lstSelAcadDegree2" id="lstSelAcadDegree2">
											<option value   = "0">-SELECT-</option>
											<?= $acaddegreeOptions2 ?>
										</select>
										<label for="lstSelAcadDegree2">Educational Level</label>
									</span>
									<span>								
										<select name= "lstMajor2" id="lstMajor">
											<option value = "0">-SELECT-</option>
											<?= $majorOptions2 ?>
										</select>
										<label for="lstMajor2">Major</label>
									</span>
								</div>
							
								<div>
									<span>
										<?php echo  FormHelpers::CreateTextBox('txtUnivName2', $txtUnivName2,
													array(
														   'class'     => 'text',
														   'size'      => 50,
														   'maxlength' => 50,
														   'id'		   => 'txtUnivName2'
														) 
													); ?>
										<label for="txtUnivName2">University</label>
									</span>
									<span>
										<select name = "lstUnivCountry2" id="lstUnivCountry2">
											<option value = "0" >-SELECT-</option>
											<?= $univlocation2 ?>
										</select>
										<label for="lstUnivCountry2">Location</label>
									</span>
								</div>
							</fieldset>
						</li>
					</ul>
				
					<h2>Professional Certification</h2>
					<p class="form_help">				
						e.g. TEFL or TESOL Certified, Coldfusion Certified Programmer, CCNA, etc..
					</p>
				
					<ul class="form">				
						<li>
							<fieldset>
								<legend><label for="txtcertname">Certificate 1</label></legend>
								<span>
									<?php echo  FormHelpers::CreateTextBox('txtcertname', $txtcertname,
												array(
													   'class'     => 'text',
													   'size'      => 50,
													   'maxlength' => 50,
													   'id' 	   => 'txtcertname'
													) 
												); ?>
									<label for="txtcertname">Certificate Name</label>
								</span>
								
								<span>
									<?php echo  FormHelpers::CreateTextBox('txtyearobtained', $txtyearobtained,
												array(
													   'class'     => 'text',
													   'size'      => 10,
													   'maxlength' => 10,
													   'id'		   => 'txtyearobtained'
													) 
												); ?>
									<label for="txtyearobtained">Year</label>
								</span>
							</fieldset>
						</li>
					
						<li>
							<fieldset>
								<legend><label for="txtcertname2">Certificate 2</label></legend>
								<span>
									<?php echo  FormHelpers::CreateTextBox('txtcertname2', $txtcertname2,
											array(
												   'class'     => 'text',
												   'size'      => 50,
												   'maxlength' => 50,
												   'id'		   => 'txtcertname2'
												) 
											); ?>
									<label for="txtcertname2">Certificate Name</label>
								</span>		
								<span>
									<?php echo  FormHelpers::CreateTextBox('txtyearobtained2', $txtyearobtained2,
											array(
												   'class'     => 'text',
												   'size'      => 10,
												   'maxlength' => 10,
												   'id' 	   => 'txtyearobtained2'
												) 
											); ?>
									<label for="txtyearobtained2">Year</label>
							
								</span>
							</fieldset>
						</li>
					
						<li>
							<fieldset>
								<legend><label for="txtcertname3">Certificate 3</label></legend>
								<span>
									<?php echo  FormHelpers::CreateTextBox('txtcertname3', $txtcertname3,
											array(
												   'class'     => 'text',
												   'size'      => 50,
												   'maxlength' => 50,
												   'id'		   => 'txtcertname3'
												) 
											); ?>
									<label for="txtcertname3">Certificate Name</label>
								</span>
								<span>
									<?php echo  FormHelpers::CreateTextBox('txtyearobtained3', $txtyearobtained3,
											array(
												   'class'     => 'text',
												   'size'      => 10,
												   'maxlength' => 10,
												   'id'		   => 'txtyearobtained3'
												) 
											); ?>
									<label for="txtyearobtained3">Year</label>
								</span>
							</fieldset>
						</li>
					</ul>
					
					
					<h2>Professional Details</h2>
				
					<ul class="form">	
						<li>
						
							<fieldset class="choices">
								<legend><span>Are you currently employed?</span></legend>	
								<ul>
						<!--
							onclick="employed(false);toggleOptions(); //dispprof();"
							onclick="employed(true);teach(false);"
						-->	
								<li>
								   <input type  = "radio"
								   	   id	 = "radio_1"
									   name  = "radIsCurrEmployed" 
									   value = "1" 
									   onclick="toggleOptions();"
									   <?php if ($radIsCurrEmployed == 1): ?>
									   checked="checked"
									    <?php endif; ?> 
								    />
									<label for="radio_1">Yes</label>
								</li>
								<li>
									<input type  = "radio"
									   id	 = "radio_2"
								       name  = "radIsCurrEmployed"
									   value = "2" 
									   onclick="toggleOptions();"
									   <?php if ($radIsCurrEmployed != 1): 
									   ?>
									   checked="checked"
						    			<?php endif; 
						    			?> 
						    		/>
									<label for="radio_2">No</label>
								</li>
								</ul>
							</fieldset>
						</li>
						<li id="fset_isTeacher">
					
							<fieldset class="choices">
								<legend><span>Are you a teacher?</span></legend>	
								<ul>						
								<!-- 
									onclick="teach(true)"
									onclick="teach(false)" 
								 -->
								 	<li>
										<input type="radio" 
						    				name="radIsTeacher" 
						    				value="1" 
						    				id="isTeacher1" 	    				
						    				onclick="toggleOptions();" 
											<?php if ($radIsTeacher == 1): ?>
											checked = "checked"
											<?php endif; ?>
											<?php if ($radIsCurrEmployed != 1): ?>
											disabled = "true"
											<?php endif; ?>
										/>
										<label for="isTeacher1">Yes</label>
									</li>
									<li>
										<input type="radio" 
						        			name="radIsTeacher" 
						        			value="2" 
						        			id="isTeacher2" 
						        			onclick="toggleOptions();"	        			
									        <?php if ($radIsTeacher == 2): ?>
									        checked = "checked"
									        <?php endif; ?>
									        <?php if ($radIsCurrEmployed != 1):  ?>
									        disabled = "disabled"
									        <?php endif; ?>
									    />
										<label for="isTeacher2">No</label>
									</li>
								</ul>
							</fieldset>
						</li>
						
					
						<li id="fset_currentJob">
						
							<label for="isTeacher3">Please specify your current job</label>
						
							<?php echo  FormHelpers::CreateTextBox('txtCurrJob', $txtCurrJob,
											   array(
												'id'        => 'isTeacher3',
												'class'     => 'text',	
												'size'      => 50,
												'maxlength' => 50,
												'disabled' => ($radIsTeacher == 1 || $radIsTeacher != 1)				   
											  )
										);?>
						
						</li>
				
					</ul>
				
					<?=  $teacherProfile ?>
				
					<?php if (strlen($radIsTeacher) && $radIsTeacher): ?>
						<script type="text/javascript">teachenable(); </script>
					<?php endif; ?>
				
				
					<h2>Job Preferences</h2>
				
					<ul class="form" id="workExp">
						<li>
							
								<label for="lstPrefEmployment">Preferred Employment <span class="required">*</span></label>
							
									<select name = "lstPrefEmployment" id="lstPrefEmployment">
										<option value = "0">-SELECT-</option>
										<?= $prefemployment ?>
									</select>
						</li>
					
						<li>
							
								<label for="lstJobType">Job Type <span class="required">*</span></label>
							
									<select name="lstJobType" id="lstJobType">
										<option value = "0" >-SELECT-</option>
										<?= $jobtypeOptions ?>
									</select>
						</li>
					
						<li>
					
								<label for="lstPrefCountry">Preferred Country <span class="required">*</span></label>
							
									<select name = "lstPrefCountry" id="lstPrefCountry">
										<option value = "0" >-SELECT-</option>
										<?= $prefcountryOptions ?>
									</select>
						</li>
					
						<li>
							
								<label for="txtPrefCity">Preferred City</label>
							
								<?php echo  FormHelpers::CreateTextBox('txtPrefCity', $txtPrefCity,
												array(
								   				   'class'     => 'text',
												   'size'      => 50,		
									 		       'maxlength' => 50,
									 		       'id'		   => 'txtPrefCity'
												 )
											 );
										?>
						</li>
					
						<li>
					
								<label for="txtWorkWhen">When can you begin working? <span class="required">*</span></label>
							

								<?php echo  FormHelpers::CreateTextBox('txtWorkWhen', $txtWorkWhen,
												array(
												   'class'     => 'text',
												   'size'      => 50,		 
									 		       'maxlength' => 50,
									 		       'id'		   => 'txtWorkWhen'
												  )
											 );
										?>
						</li>
													
					
						<li>
							<label for="txaMoreDetails">Additional Details (includes your skills, references, publications, languages, etc.)</label>
						
						
								<textarea name="txaMoreDetails" cols="48" rows="10" id="txaMoreDetails"><?= $txaMoreDetails ?></textarea>
						</li>		
					
						<a href="javascript:void(0)" onclick="createWorkExp()">
						<?php if("Edit Resume" == $labelaction): ?>
							<?php if($workexpcount): ?>
								Add Another Work Experience		
							<?php else: ?>
								Add Work Experience
							<?php endif; ?>
						<?php endif; ?>
						</a>
					
						<?php if("Edit Resume" == $labelaction): ?>
						<?php for($i=0;$i<count($worklist);$i++): ?>
							<?php $value = $worklist[$i]; ?>
							<?php $varwID = $value->getworkexperienceID(); ?>
						
						<div id="divworkexp<?= $i + 1 ?>">	
						<div id="labelid<?= $i + 1 ?>"><h2><a name="work<?= $i + 1 ?>" id="work<?= $i + 1 ?>"></a>Work Experience <?= $i + 1 ?></h2></div>
						<div align="right">
							<?php if(strlen(trim($varwID))): ?>
								<a href="javascript:void(0)" onclick="if(confirm('Are you sure you want to delete?')) delworkexp('divworkexp<?= $i + 1 ?>',<?= $travelerID ?>,<?= $value->getworkexperienceID() ?>);">Delete Work Experience</a>
							<?php endif; ?>
						</div>
						<li>
							
								<label for="txtWE1Position<?= $i + 1 ?>">Position</label>
							
								<?php echo  FormHelpers::CreateTextBox('txtWE1Position'.($i + 1), $value->getJobPosition(),
												array(
								   				   'class'     => 'text',
												   'size'      => 50,		
									 		       'maxlength' => 50,
									 		       'id'		   => 'txtWE1Position'.($i + 1)
												 )
											 );
										?>
						</li>
					
						<li>
							
								<label for="txtWE1Employer<?= $i + 1 ?>">Employer</label>
							
								<?php echo  FormHelpers::CreateTextBox('txtWE1Employer'.($i + 1), $value->getEmployer(),
												array(
								   				   'class'     => 'text',
												   'size'      => 50,		
									 		       'maxlength' => 50,
									 		       'id'		   => 'txtWE1Employer'.($i + 1)
												 )
											 );
										?>
						</li>
					
						<li>
							<label for="txtWE1JobDesc<?= $i + 1 ?>">Job Description</label>
						
							<textarea name="txtWE1JobDesc<?= $i + 1 ?>" cols="48" rows="5" id="txtWE1JobDesc<?= $i + 1 ?>"><?= $value->getJobDescription() ?></textarea>
						</li>
					
						<li>
							
								<label for="txtWE1LenServ<?= $i + 1 ?>">Length of Service</label>
							
								<?php echo  FormHelpers::CreateTextBox('txtWE1LenServ'.($i + 1), $value->getLenServ(),
												array(
								   				   'class'     => 'text',
												   'size'      => 50,		
									 		       'maxlength' => 50,
									 		       'id'		   => 'txtWE1LenServ'.($i + 1)
												 )
											 );
										?>
						</li><br>
						<input type="hidden" name="hidwID<?= ($i + 1) ?>" value="<?= $value->getworkexperienceID() ?>">
						</div>
						<?php endfor; ?>
						<input type="hidden" name="hidwecount" value="<?= $i ?>" id="hidwecount">
						<?php endif; ?>
					</ul>
					<ul class="form">
						<?= $newsletterSubscribe ?>
						<li class="actions">
								
							<input name  = "btnSubmit" class = "submit" type  = "submit" value = "<?= $btnSubmit ?>" align="middle" />
						
						</li>
					</ul>
					<div class="clear"></div>
				</form>
			
				<script type="text/javascript">teachpos();teachinglevel();</script>
			
				<?php if("Edit Resume" == $labelaction && $resourcefilecount): ?>
					<div class="action_group">
						<p><strong>Do you want to <a href="resourcefiles.php?action=view&amp;cat=resume&amp;genID=<?= $travelerID ?>">remove</a> or <a href="resourcefiles.php?action=view&amp;cat=resume&amp;genID=<?= $travelerID ?>">replace</a> your uploaded PDF resume? </strong></p>
					</div>
				<?php elseif ("Edit Resume" == $labelaction && 0 == $resourcefilecount): ?>
					<div class="action_group">
						<p><strong>Do you have a personalized resume? Click <a href="resourcefiles.php?action=add&amp;cat=resume&amp;genID=<?= $travelerID ?>">here</a> to upload &raquo;</strong></p>
					</div>
				<?php endif; ?>
			
				<?php //if($labelaction == "Edit Resume"):
				 ?>
					<?php //$worketc 
					?>
				<?php //endif; 
				?>
			
				<div class="clear"></div>
			</div>
			<div class="foot"></div>
		</div>
	</div>
	
	<div id="narrow_column">
		
		<div class="section" id="quick_tasks">
			<div class="head_left"><div class="head_right"></div></div>
			<div class="content">
				<p class="help_text">
					<?= $helptext ?>
				</p>
				<?php if("Edit Resume" == $labelaction && $resourcefilecount): ?>
					<ul class="actions">
						<li><a href="resourcefiles.php?action=view&amp;cat=resume&amp;genID=<?= $travelerID ?>" class="button">Remove / Replace Resume</a></li>
					</ul>
				<?php elseif ("Edit Resume" == $labelaction && 0 == $resourcefilecount): ?>
					<ul class="actions">
						<li><a href="resourcefiles.php?action=add&amp;cat=resume&amp;genID=<?= $travelerID ?>" class="button">Upload Resume</a></li>
					</ul>
				<?php endif; ?>
			</div>
			<div class="foot"></div>
		</div>
		
	</div>
	
	<div class="clear"></div>
</div>

