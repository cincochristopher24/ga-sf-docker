<div id="narrow_column">		
		
	<div id ="featured_photos" class="photos section">
		
		<h2><span>Gallery View
			<? if($owner): ?>	
					<span class="photo_controls">
					<!--
						<a href="<?= $uploadphoto ?>" class="upload_pic line">												
							<img class="upload_ico" src="/images/upload_ico.gif" />
							<? if($reccount): ?>
								Upload More Photos													
							<? else:?>
								Upload Photo 
							<? endif;?>
						</a>
					-->
					<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(1,1,'gallery',<?= $genid?>,'<?= $context?>')"  class="upload_pic line">
						<img class="upload_ico" src="/images/upload_ico.gif" /> Manage/Upload Photos	
					</a>
					</span>
					
			<? endif; ?>
			</span>
		</h2>
		<div class="content">	
			<? for($x=$start; $x<$endrow; $x++): ?>
				
					<div class="thumb_box">
				
						<a href="javascript:void(0)" onclick="photo.fullSizeView(<?=$x ?>,<?=$endrow ?>,<?=$pagenum ?>,'piccontainer')">		
							<div id="previewWrap<?=$photo[$x]->getPhotoId();?>" class="noclass" > 
								<img src="<? echo $photo[$x]->getPhotoLink('thumbnail'); ?>" class="photo_thumb" >	
							</div>
						</a>
					
					</div>
				
																			
			<? endfor; ?>	
			
			<div class="clear"></div>
			<? if($owner && $context == 'photoalbum'): ?>
				<div id="photo_group_controls">
					<a href="javascript:void(0)" onclick='GROUPPhotoAlbum.showedit_title("<?= $headercaption;?>",<?= $genid?>)'>Edit Photo Album</a> | 
					<a href="javascript:void(0)" onclick="GROUPPhotoAlbum.deleteAlbum(<?= $genid?>,<?= $album->getGroupID()  ?>)">Delete Photo Album</a>
				</div>
			<? endif; ?>
			<? if ($page->getNumberOfPages() > 1): ?>
				<div class="pagination">
					<div class="pagination_pages">			
							
						<?= $page->getPrevious(true);	?>				
				
						<?= $page->getPagelink() ?>
											
						<?= $page->getNext(true); ?>						
					</div>
				</div>
			<? endif; ?>
		</div>			
		<div class="foot"></div>
	</div>
		
</div>	


