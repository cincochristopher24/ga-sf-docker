<div class="noticeBox">

	<div id="noticeHeader">
		<form class="jLeft" id="frmNotice" name="frmNotice" method="post" action="travelerdashboard.php">
			<input class="jLeft" style="margin-top:0px;" type="checkbox" id="chkHideNextTime1" name="ADVISOR_ACCOUNT_NOTICE" onclick="document.getElementById('chkHideNextTime2').checked = this.checked;" value="0" />
			<label class="jLeft" for="chkHideNextTime1">Don&rsquo;t show this notice again in the future</label>
		</form>
		<a href="javascript: document.getElementById('frmNotice').submit()" class="jRight link">Proceed to your account &#187;</a>
	</div>

	<p class="clear"><strong>Hi <?=$vars["group"]->getName()?> administrator,</strong></p>

	<ul>
  	<?php if( isset($vars["siteContext"]) ): ?>
	  <li>
	    Thanks for logging in to <?=$vars["siteContext"]->getSiteName()?> Network today! Please take a moment to
	    know about some important changes we&rsquo;ve made.
	  </li>
  	<?php else: ?>
		<li>
		    Thanks for logging in to GoAbroad Network today! Please take a moment to
		    know about some important changes we&rsquo;ve made.
		</li>
	<?php endif; ?>
		<li>
		 We&rsquo;ve seen some of you have had to manage your advisor groups with two 
		 accounts - an advisor account and your own member account. We know your 
		 time is precious so to save you from having to maintain two different 
		 accounts, we have merged the two account types together.
		</li>

		<li>
		  Now, you can access and manage advisor groups from your member account. 
		  And in connection with this, you will see a brand new Passport page when 
		  you log in.
		</li>
	
		<li>
		  Please be assured that you have not lost any content in this process. You 
		  still have your Advisor Group and everything it contained before the 
		  change - you have simply been converted into an individual with admin 
		  privileges for the group <strong><?=$vars["group"]->getName()?></strong>. It's 
		  like putting a face and name behind the posts that you make for 
		  <strong><?=$vars["group"]->getName()?></strong>.
		</li>

		<li><img src="/images/noticeImage3.jpg" border="0" align="middle"></li>

		<li>
		  Your username is currently: <strong><?=$vars["traveler"]->getUserName()?></strong>
		</li>

		<li>
		  If your username is your company or school name, you might want to change 
		  it to the name you prefer to have on the site.
		</li>

		<li><img src="/images/noticeImage2.jpg" border="0" align="middle"></li>

		<li>
		  You can view and manage your group by going to your Groups tab, and 
		  clicking on your group name. You will find that none of your group 
		  management capabilities have changed.
		</li>

		<li><img src="/images/noticeImage1.jpg" border="0" align="middle"></li>

		<li>To know more about the change, <a href="/faq.php">please visit our illustrated guide &#187;</a></li>
	</ul>

	<form class="jLeft" id="frmNotice2" name="frmNotice2" method="post" action="travelerdashboard.php">
		<input class="jLeft" style="margin-top:0px;" type="checkbox" id="chkHideNextTime2" name="ADVISOR_ACCOUNT_NOTICE" value="0" onclick="document.getElementById('chkHideNextTime1').checked = this.checked;"/>
		<label class="jLeft" for="chkHideNextTime2">Don&rsquo;t show this notice again in the future</label>
	</form>
	<a href="javascript: document.getElementById('frmNotice2').submit()" class="jRight button">Proceed to your account &#187;</a>

</div>