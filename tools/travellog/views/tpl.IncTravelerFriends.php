<?php
/*
 * Created on 10 24, 2006
 *
 * @author  Kerwin Gordo
 * Purpose : subtemplate for profile (showing a travelers friend)
 */
require_once("Class.StringFormattingHelper.php");
$ltID = 0;	
if (isset($loggedUser)) {
	$ltID = $loggedUser->getTravelerID();			  
}
?>

<div class="section" id="featured_travelers">
	<h2>
		<span>Friends <?php if ($showFriendsCount) echo '('.$count.')'; ?></span>
		<?php if ($count> 12) : ?>
			<span class="header_actions">
	            <a class="more" title="View all friends of <?=$traveler->getUserName()?>" href="<?php echo $viewAllFriendsLink; ?>">View All Friends</a>
			</span>
        <?php endif; ?>
	</h2>
	<div class="content">
	<!-- Changed classname from "users" to "usrs2 bubbled" -->
    	<ul class="users2 bubbled">
			<?php foreach($friends as $friend): ?>
				<?php $profile = $friend->getTravelerProfile();	?>
				<li> 
					<a href="/<?php echo $profile->getUserName();?>" class="thumb bubble_control">
						<img class="pic" src="<?php echo $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width="65" height="65" alt="User Profile Pic" title="View Profile" />
					</a>
					<div class="details bubble">
						<?php if ($profile->getHTLocationID()): ?>
							<?php if ($profile->getCity()): ?>
								<?php $cID = $profile->getCity()->getCountry()->getCountryID()  ?>
								<img class="flag" src="http://images.goabroad.com/images/flags/flag<?php echo $cID; ?>.gif" width="22" height="11" alt="Travel blog from <?php echo $profile->getCity()->getCountry()->getName(); ?>" />
							<?php endif; ?>
						<?php endif; ?>
						<strong><a href="/<?php echo $profile->getUserName();?>" title="<?php echo $profile->getUserName();?>" class="username"> <?php echo stripslashes($profile->getUserName()); ?> </a></strong>
						<?php if ($isowner || $friend->getPrivacyPreference()->canViewNames($ltID)): ?>
							<em><?php echo stripslashes($profile->getFirstName()); ?> <?php echo stripslashes($profile->getLastName()); ?></em>
						<?php endif; ?>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
		<div class="clear"></div>
	</div>
	<div class="foot"></div>
</div>
