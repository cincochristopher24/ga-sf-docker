<?php
	/*
	 * Class.ViewStudentsLists.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
	
	require_once("travellog/views/Class.AbstractView.php");

	class ViewStudentsList extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewStudentsList.php');
		}
		
	} 
?>
