<?php
require_once("travellog/views/Class.AbstractView.php");
class EntryTitleCalloutPhoto extends AbstractView{
	function render(){
		return ( $this->contents['obj_entry']->getTravelLogID() )? $this->obj_template->fetch('tpl.EntryTitleCalloutPhoto.php'): NULL;
	}
}
?>
