<?php
	/**
	 * Created on 09 25, 2006
	 * Purpose: template to list comments
	 * Last edited by: Cheryl Ivy Q. Go  27 September 2007
	 * For Greetings, Salutations and Comments
	 */

	require_once 'Class.HtmlHelpers.php';
	require_once 'Class.GaDateTime.php';

	$d = new GaDateTime();

	// profile
	if (1 == $context)
		$contextName = 'profile';
	elseif (2 == $context)
		$contextName = 'photo';
	// journal-entry
	elseif (3 == $context)
		$contextName = 'journal-entry';

	$t = 1;
	$ictr = 0;
	$default = 4;
	$isViewAll = false;
?>

<h2>
	GREETINGS, SALUTATIONS AND COMMENTS
	<? if (isset($context) && isset($contextID) && $travelerID > 0 && $canAddComment): ?>
		<? if (1 == $context) : ?>
			<span class = "actions">
				<a class = "add" href = "javascript:void(0)" onclick = "Comment.showCommentBox();Comment.setDivId('comments');Comment.setContextId(<?=$contextID?>);Comment.setContext(<?=$context?>);">Add Your Own</a>
			</span>
		<? elseif (3 == $context) : ?>
			<span style = "padding-left: 390px; text-transform: none;">
				<a class = "add" href = "javascript:void(0)" onclick = "Comment.showCommentBox();Comment.setDivId('comments');Comment.setContextId(<?=$contextID?>);Comment.setContext(<?=$context?>);">Add Your Own</a>
			</span>
		<? endif; ?>
	<? endif; ?>
</h2>
<div class="content" style = "padding: 0px;">		
    <? if (0 < count($comments)) :?>  	  	  	
  		<ul>
	  		<? foreach($comments as $comment ) : 
		  			try {
		  		    	$tp = new TravelerProfile($comment->getAuthor());
		  			}	
		  		    catch (Exception $exc) {}

		  		if($ictr >= $default){
						echo '<li id="ans'.$t.'" style="display: none;">';
						$t +=1;
						$isViewAll = true;
				}
				else
					echo '<li>';
			?>
					<div class="author">
						<? $gID = 0;?>										
		 				<?if ($gID > 0 ):?>
		 					<?$ag = new AdminGroup($gID);?>
		 					<a href="/group.php?gID=<?=$gID?>"  class="user_thumb"><img src="<?=$ag->getGroupPhoto()->getPhotoLink() ?>" height="37" width="37" alt="User Profile Photo" /></a>					
		 				<?else:?>					
							<a href="<?=$tp->getUserName()?>" class="user_thumb"><img src="<?=$tp->getPrimaryPhoto()->getThumbnailPhotoLink() ?>" height="37" width="37" alt="User Profile Photo" /></a>					
					 		<? if (strlen($tp->getUserName()) <= 10): ?>
					 			<? $name = $tp->getUserName() ?>
					 		<? else: ?>
					 			<? $name = substr($tp->getUserName(),0,8) . '...' ?>
					 		<?endif;?>
					 	<?endif;?>
					 	<? $authorId = $tp->getTravelerID(); ?>
				 	</div>
				 	<div class="comment_content">
				 		<div class = "greeting_date"> <?= $d->set($comment->getCreated())->friendlyFormat() ?> </div>
				 		<div class = "poke_content">
				 			<?
				 				$Poke = new PokeType($comment->getPokeType());

				 				$pokesview = new PokesView();
				 				$pokesview-> setVisitorId($travelerID);		// travelerId of logged-in user
				 				$pokesview-> setAuthorId($authorId);		// travelerId or groupId of the greeting owner
				 				$pokesview-> setOwnerId($ownerID);			// travelerId or groupId of profile/photo/journal-entry
				 				$pokesview-> setPokeTypeId($Poke->getPokeTypeID());
				 				$pokesview-> setContextId($contextID);
				 				$pokesview-> setContext($context);
				 			?>
				 			<? if (0 == $comment->getPokeType()) : ?>
				 				<div style = "font-weight: bold; padding-bottom: 5px;"> <?=$pokesview->getMessage()?> </div>
				 			<? else : ?>
				 				<img src = <?=$Poke->getPath().$Poke->getPokeImage()?> />
				 				<div class = "poke_message"> <?=$pokesview->getMessage()?> </div>
				 			<? endif; ?>
				 		</div>
			 			<div id="thecommenttext<?=$comment->getCommentID()?>">	
							<?= HtmlHelpers::Textile($comment->getTheText()) ?>
						</div>
						
						<div id="actions<?=$comment->getCommentID()?>" class="actions">
					 			<? if ($travelerID == $tp->getTravelerID()) : ?>
					 					<a href="javascript:void(0)" onclick="Comment.setDivId('comments');Comment.showedit(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>)">Edit</a>|
										<a href="javascript:void(0)" onclick="Comment.setDivId('comments');Comment.deletecomments(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>)">Delete</a>
					 			<? elseif (isset($okToDelete)): ?>
					 					<a href="javascript:void(0)" onclick="Comment.setDivId('comments');Comment.deletecomments(<?=$context?>,<?=$contextID?>,<?=$comment->getCommentID()?>,'<?=$photoCat?>',<?=$genID?>)">Delete</a>
					 			<? endif; ?>
					 	</div>
					 	
					 	<div id="commenteditloading<?=$comment->getCommentID()?>" style="display:none;color:red;padding-top:10px;">
					 		<img src="/images/loadingAnimation.gif"/>
					 	</div>

					 	<div class="clear"></div>
					 </div>				 	
				</li>
				<? $ictr += 1; ?>
	  		<? endforeach; ?>
		</ul>	  		
	<?else:?>
		<div class = "no_greeting">
  			There's no greeting yet on this <?=$contextName?>!
  		</div>
  	<? endif; ?>
  	<? if (!isset($_SESSION['travelerID'])) : ?>
		<h3 style = "margin: 10px 5px 0px;"> Add Comment </h3>
		<p>
			You have to be <a href="/login.php?redirect=<?=$envURL?>">logged in</a> to leave comments.<br />
			Not yet a member? <a href="/register.php">Register</a> now&#8212;it&#8217;s fast, easy and totally free.
		</p>
	<? endif; ?>
	<? if(isset($isViewAll) && $isViewAll) : ?>
		<div style = "float: right; padding-right: 10px;"> <a id = 'col_layer' href = 'javascript:void(0)' onclick = 'ecollapse(<?=$t?>)'> View all </a> </div>
	<? endif; ?>

	<input type="hidden" name="server"		id = "server" value = "http://<?=$_SERVER['HTTP_HOST']?>" />
</div>
