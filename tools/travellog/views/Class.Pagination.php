<?php
  /**
   * @(#) Class.Pagination.php
   * class for pagination
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 11 5, 08
   */
   
  require_once 'Class.Template.php';
  
  class Pagination {
  	/**
  	 * the current page number
  	 * @var integer
  	 */
  	protected $mCurPage = 1;
  	
  	/**
  	 * the events with the javascript callback to use
  	 * ex: $event = array("onclick"=>"callback()","onmouse"=>"callback2()");
  	 * @var array
  	 */
  	protected $mEvents = array();
  	
  	/**
  	 * for anchors href value (default: javascript:void(0))
  	 * for set href value it interprets [label] to the correct page assigned
  	 * (ex. if href for first [label] will be converted to 1)
  	 * @var string
  	 */
  	protected $mHref = "javascript:void(0)";
  	
  	/**
  	 * array keys were
  	 *   - "path"=> template path
  	 *   - "main_template"=> the main paging template
  	 *   - "active_quick_template"=> the active paging template for quick paging like (prev, next, last and first)
  	 *   - "inactive_quick_template"=> the inactive paging template for quick paging
  	 *   - "active_number_template"=> the active paging template for numbers
  	 *   - "inactive_number_template"=> the inactive paging template for numbers
  	 * @var string
  	 */
  	protected $mTemplates = array(
  	  "path"=>"travellog/views/",
			"main_template"=>"tpl.Pagination.php",
			"active_quick_template"=>"tpl.IncActivePagingQuickPage.php",
			"inactive_quick_template"=>"tpl.IncInactivePagingQuickPage.php",
			"active_number_template"=>"tpl.IncActivePagingNumberPage.php",
			"inactive_number_template"=>"tpl.IncInactivePagingNumberPage.php"
  	  );
  	  
  	/**
  	 * the number of records
  	 * @var integer
  	 */
  	protected $mNumRecords = 0;
  	
  	/**
  	 * the number of rows per page
  	 * @var integer
  	 */
  	protected $mRowsPerPage = 10;
  	
  	/**
  	 * the number of pages
  	 * @var integer
  	 */
  	protected $mNumPages = 0;
  	
  	/**
  	 * the number of pages to show
  	 * @var integer
  	 */
  	protected $mNumPageToShow = 11;
  	
  	
  	/**
  	 * @constructor
  	 * @param integer $numRecord the number of records
  	 * @param integer $curPage the current page
  	 * @param integer $numRowsPerPage the number of rows per page
  	 * @param integer $numPageToShow the number of page numbers to show
  	 */
  	public function __construct($numRecord, $curPage = 1, $numRowsPerPage = 10, $numPageToShow = 11){
  		$this->mNumRecords = $numRecord;
  		$this->mRowsPerPage = $numRowsPerPage;
  		$this->mCurPage = $curPage;
  		$this->mNumPages = ceil($this->mNumRecords/$this->mRowsPerPage);
  		$this->mNumPageToShow = $numPageToShow;
  	}
  	
  	/**
  	 * Method to create the page numbers
  	 * @return array array of numbers (the page numbers that will be shown)
  	 */
  	public function createPageNumbers() {
			$cnt = 0;
			$num_pages = ceil($this->mNumRecords/$this->mRowsPerPage);
			$half_num_pages = floor($this->mNumPageToShow/2);
			
			if ($half_num_pages >= $this->mCurPage OR $this->mNumPageToShow >= $num_pages) {
				$cnt = 1;	
			}
			else if (($num_pages - $half_num_pages) < $this->mCurPage AND ($this->mNumPageToShow-1) < $num_pages) {
				$cnt = $num_pages - ($this->mNumPageToShow-1);
			}
			else {
				$cnt = 	$this->mCurPage - $half_num_pages;
			}
			
			$page_numbers = array();
			
			for ($count = 1; $cnt <= $num_pages AND 11 >= $count ; $cnt++, $count++) {
				$page_numbers[] = $cnt; 
			}			
			
			return $page_numbers;
		}
  	 
  	
  	/**
  	 * Method to fetch the template of events with data included
  	 * @return string the template in string format
  	 */
  	public function fetchEventsTemplate(){
  		$tpl = "";
  		
  		foreach($this->mEvents as $event=>$callback){
  			$tpl = $tpl." ".htmlentities($event)." = \"".htmlentities($callback)."\" ";
  		}
  		
  		return $tpl;
  	}
  	
  	/**
  	 * Method to fetch the template of pagination
  	 * @return string
  	 */
  	public function fetchTemplate(){
  	  $events = $this->fetchEventsTemplate(); 
  	  
  	  $first = new Template();
  	  $first->set_path($this->mTemplates['path']);
  	  $first->set("label", "First");
  	  $first->set("href", str_replace("[page]",1,$this->mHref));
  	  $first->set("class", "first");
  	  $first->set("events", str_replace("[page]",1,$events));
  	  $first_tpl_used = (1 == $this->mCurPage)?"inactive_quick_template":"active_quick_template";
  	  
  	  $prev_number = (0 < ($this->mCurPage-1)) ? ($this->mCurPage-1) : 1;
  	  $prev = new Template();
  	  $prev->set_path($this->mTemplates['path']); 	
  	  $prev->set("label", "Prev");
  	  $prev->set("href", str_replace("[page]",$prev_number,$this->mHref));
  	  $prev->set("class", "previous");
  	  $prev->set("events", str_replace("[page]",$prev_number,$events));
  	  $prev_tpl_used = (1 == $this->mCurPage)?"inactive_quick_template":"active_quick_template";
  	  
  	  $next_number = ($this->mNumPages > ($this->mCurPage + 1)) ? ($this->mCurPage + 1) : $this->mNumPages;
  	  $next = new Template();
  	  $next->set_path($this->mTemplates['path']);  
  	  $next->set("label", "Next");
  	  $next->set("href", str_replace("[page]",$next_number,$this->mHref));
  	  $next->set("class", "next");
  	  $next->set("events", str_replace("[page]",$next_number,$events));
  	  $next_tpl_used = ($this->mNumPages == $this->mCurPage)?"inactive_quick_template":"active_quick_template";
  	  
  	  $last = new Template();
  	  $last->set_path($this->mTemplates['path']);
  	  $last->set("label", "Last");
  	  $last->set("href", str_replace("[page]",$this->mNumPages,$this->mHref));
  	  $last->set("class", "last");
  	  $last->set("events", str_replace("[page]",$this->mNumPages,$events));
  	  $last_tpl_used = ($this->mNumPages == $this->mCurPage)?"inactive_quick_template":"active_quick_template";  	  
  	  
  	  $number_pages = "";
  		$page_numbers = $this->createPageNumbers();
  	  $number = new Template();
  	  $number->set_path($this->mTemplates['path']);
  	  
  	  foreach($page_numbers as $page){
  	  	$number->set("label", $page);
  	  	$number->set("href", str_replace("[page]",$page,$this->mHref));
  	  	$number->set("events", str_replace("[page]",$page,$events));
  	  	$page_tpl = ($this->mCurPage == $page)?"inactive_number_template":"active_number_template";
  	    $number_pages = $number_pages.$number->fetch($this->mTemplates[$page_tpl]);
  	  }
  	  
  	  $main = new Template();
  	  $main->set_path($this->mTemplates['path']);
  	  $main->set("first_tag",$first->fetch($this->mTemplates[$first_tpl_used]));
  	  $main->set("prev_tag",$prev->fetch($this->mTemplates[$prev_tpl_used]));
  	  $main->set("next_tag",$next->fetch($this->mTemplates[$next_tpl_used]));
  	  $main->set("last_tag",$last->fetch($this->mTemplates[$last_tpl_used]));
  	  $main->set("number_pages_tag", $number_pages);
  	  
  	  return $main->fetch($this->mTemplates['main_template']);
  	}
  	
  	/**
  	 * Method to get the current page number
  	 * @return integer the current page number
  	 */
  	public function getCurrentPage(){
  		return $this->mCurPage;
  	}
  	
  	/**
  	 * Method to get the events associated with their callback function
  	 * @return array
  	 */
  	public function getEvents(){
  		return $this->mEvents;
  	}
  	
  	/**
  	 * Method to get the anchor href value
  	 * @return string
  	 */
  	public function getHrefValue(){
  		return $this->mHref;
  	}
  	
  	/**
  	 * Method to get the number of pages
  	 * @return integer
  	 */
  	public function getNumberOfPages(){
  		return $this->mNumPages;
  	}
  	
  	/**
  	 * Method to get the number of pages to show
  	 * @return integer
  	 */
  	public function getNumberOfPagesToShow(){
  		return $this->mNumPageToShow;
  	}
  	
  	/**
  	 * Method to get the number of records
  	 * @return integer
  	 */
  	public function getNumberOfRecords(){
  		return $this->mNumRecords;
  	}
  	
  	/**
  	 * Method to get the number of rows per page
  	 * @return integer
  	 */
  	public function getNumberOfRowsPerPage(){
  		return $this->mRowsPerPage;
  	}
  	
  	/**
  	 * Method to get the templates used
  	 * @return array
  	 */
  	public function getTemplates(){
  		return $this->mTemplates;
  	}
  	
  	/**
  	 * Method to set the current page number
  	 * @param integer $curPage
  	 * @return void
  	 */
  	public function setCurrentPage($curPage){
  		$this->mCurPage = $curPage;
  	}
  	
  	/**
  	 * Method to set the events associated with their callback function
  	 * @param array $events the associated array of events to its callback function
  	 * @return void
  	 */
  	public function setEvents($events){
  		$this->mEvents = $events;
  	}
  	
  	/**
  	 * Method to set the anchor href value
  	 * @param string $href
  	 * @return void
  	 */
  	public function setHrefValue($href){
  		$this->mHref = $href;
  	}
  	
  	/**
  	 * Method to set the number of pages to show
  	 * @param integer $numPageToShow
  	 */
  	public function setNumberOfPagesToShow($numPageToShow){
  		$this->mNumPageToShow = $numPageToShow;
  	}   	
  	
  	/**
  	 * Method to set the number of records
  	 * @param integer $record
  	 * @return void
  	 */
  	public function setNumberOfRecords($record){
  		$this->mNumRecords = $record;
  	}
  	
  	/**
  	 * Method to set the number of rows per page
  	 * @param integer $rowsPerPage
  	 * @return void
  	 */
  	public function setNumberOfRowsPerPage($rowsPerPage){
  		$this->mRowsPerPage = $rowsPerPage;
  	} 	
  	
  	/**
  	 * Method to set the templates
  	 * @param string $templates
  	 * @return void
  	 */
  	public function setTemplates($templates){
  		foreach ($templates as $key=>$tpl) {
  			$this->mTemplates[$key] = $tpl; 
  		}
  	}  	
  }
