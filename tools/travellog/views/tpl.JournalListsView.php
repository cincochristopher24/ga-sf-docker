<?php
/* NOTE: PLEASE OBSERVE PROPER INDENTATION WHEN MODIFYING THIS TEMPLATE.. :)
 * BY: ALDWIN SABORNIDO
 */

$objObjectIterator = $contents['obj_journal_iterator'];
$props             = $contents['props'];
$javascript        = '';
$show_authors      = ( isset($props['show_author']) )? $props['show_author'] : false;


$objObjectIterator->rewind(); 
$ctr     = 1;
$display = 5; 
while( $objObjectIterator->valid() ):
	$ok                     = true;
	$text_publish           = ( $objObjectIterator->current()->getPublish() )? 'Unpublish': 'Publish';
	$j                      = 0;
	$latest_entry_title     = '';
	$latest_entry_desc      = '';
	$latest_entry_date      = '';
	$latest_entry_location  = '';
	$latest_primary_photo   = '';
	$temp_obj_entry         = array();     	
	$obj_traveler           = $objObjectIterator->current()->getTraveler();
	$obj_traveler_profile   = $obj_traveler->getTravelerProfile();
	$col_trips              = $objObjectIterator->current()->getTrips();
	$id                     = $objObjectIterator->current()->getTravelID();
	$journal_title          = ( strlen(trim($objObjectIterator->current()->getTitle())) > 40)? substr($objObjectIterator->current()->getTitle(), 0, 40) . '...' : $objObjectIterator->current()->getTitle();
	$journal_desc           = ( strlen(trim($objObjectIterator->current()->getDescription())) > 250 )? strip_tags(nl2br(substr($objObjectIterator->current()->getDescription(),0,250))): strip_tags(nl2br($objObjectIterator->current()->getDescription())); 
	$journal_views          = $objObjectIterator->current()->getViews();
    $is_group               = false;
    
    $journal_owner          = $objObjectIterator->current()->getOwner();     
    $there_are_entries      = false;     
    
    // added by chris 
    if((isset($props['is_login']) && $props['is_login'] && isset($props['travelerID'])) && 0 < ($tempGID = AdminGroup::getAdvisorGroupID($props['travelerID']))){
    	$tempAdminGroup = new AdminGroup($tempGID);
    }   
    
	if( $obj_traveler->isAdvisor() ){
		$groupID        = AdminGroup::getAdvisorGroupID( $obj_traveler->getTravelerID() );
		$objAdminGroup  = new AdminGroup($groupID);
		$username       = $objAdminGroup->getName();
		$is_group       = true; 
		$traveler_type  = '';
		$primary_photo  = $objAdminGroup->getGroupPhoto()->getPhotoLink('thumbnail');  
	}	
	else{
		$username      = $obj_traveler->getUsername();
		$traveler_type = $obj_traveler->getTravelerType();
		$primary_photo = $obj_traveler_profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
	}
	if( count($col_trips) ){
		foreach( $col_trips as $obj_trip ){
			$col_entries = $obj_trip->getTravelLogs();
			foreach( $col_entries as $obj_entry ){
				if( $props['is_login'] && $props['travelerID'] == $obj_entry->getTravel()->getTravelerID() )
					$temp_obj_entry[] = $obj_entry;
				elseif( $obj_entry->getPublish() == 1 )
					$temp_obj_entry[] = $obj_entry;
				$there_are_entries = true; 
			}
		}
	}

	$total_entries = count($temp_obj_entry);
	if( $total_entries ){
		$obj_entry              = $temp_obj_entry[$total_entries-1];
		$entry_owner            = $obj_entry->getOwner();
		$link                   = '';
		$obj_trip               = $obj_entry->getTrip();
		$city                   = $obj_trip->getLocation()->getName();
		$country                = $obj_trip->getLocation()->getCountry()->getName();
		$latest_entry_title     = $obj_entry->getTitle();
		$entry_title_strip      = str_replace('-','_',$latest_entry_title);
		$entry_title_strip      = str_replace(' ','-',$entry_title_strip); 
		$latest_entry_desc      = ( strlen(trim($obj_entry->getDescription())) > 250 )? strip_tags(nl2br(substr($obj_entry->getDescription(),0,250))): strip_tags(nl2br($obj_entry->getDescription()));
		$latest_entry_date      = $contents['date_time']->set($obj_trip->getArrival())->friendlyFormat(); 
		$latest_entry_location  = ( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country;
		
		if( get_class($entry_owner) == 'Traveler' ){
			$journal_link       = $username . '/journals/' . $id;
			$link               = $username . '/journals/' . $id . '/' . $entry_title_strip;
		}
		elseif( is_object($entry_owner) ){
			$username = str_replace(' ','-',$username);  
			if( $entry_owner->getParentID() ){
				$subgroup_name  = str_replace(' ','-',$entry_owner->getName());
				$journal_link   = 'groups/'. $username . '/subgroup/' . $subgroup_name . '/journals/' . $id;
				$link           = 'groups/'. $username . '/subgroup/' . $subgroup_name . '/journals/' . $id . '/' . $entry_title_strip;
			}
			else{
				$link           = 'groups/'. $username . '/journals/' . $id . '/' . $entry_title_strip;
				$journal_link   = 'groups/'. $username . '/journals/' . $id ;  
			} 
		}
		$j = $total_entries-1;
		while( $ok ){
			$latest_primary_photo = $temp_obj_entry[$j]->getPrimaryPhoto();
			//if( $latest_primary_photo->getPhotoID() || $j == ($total_entries-1) ) $ok = false;
			//$j++; 
			if( $latest_primary_photo->getPhotoID() || $j == 0 ) $ok = false; 
			$j--;  
		}
	}
	else
		$link = $username . '/journals/' . $id; 
?>
	
	
<div class="container <?php if( get_class($journal_owner) != 'Traveler'  ):?>admin<?php endif;?>">     
	<div class="journal_box_wrap">
		<div class="header">
		
			<h2>
				<!-- edited by daf; show clickable link only if there are entries-->	
				<? if ($objObjectIterator->current()->hasAnyEntry()) : ?>
					<a href="/<?php echo $link?>" title="<?php echo $journal_title?>" alt="<?php echo $journal_title?>">
						<?= HtmlHelpers::truncateWord($journal_title, 45); ?>
						<!--<?php echo $journal_title?>	-->					
					</a>
				<? else: ?>
					<?= HtmlHelpers::truncateWord($journal_title, 45); ?>
				<? endif; ?>
				
				
			</h2>

		    <?php if( $show_authors ):?>       
				<div class="author_info">
					    <?if( $is_group ):?>
					    	<a href="/group.php?gID=<?php echo $groupID?>" title="Read more of <?php echo (strtolower(substr($username, -1)) == 's')? $username . "'s": $username;  ?> travel experiences and adventure!"><img src="<?php echo $primary_photo?>" width="37px" height="37px" alt="Group: <?php echo $username?>" /></a>  
							<p class="author_username"><a href="/group.php?gID=<?php echo $groupID?>" title="Read more of <?php echo (strtolower(substr($username, -1)) == 's')? $username . "'s": $username;  ?> travel experiences and adventure!"><?php echo $username?></a></p>						    
					    <?else:?>
					    	<a href="/<?php echo $username?>" title="Read more of <?php echo (strtolower(substr($username, -1)) == 's')? $username . "'": $username. "'s";  ?> travel experiences and adventure!"><img src="<?php echo $primary_photo?>" width="37px" height="37px" alt="Traveler: <?php echo $username?>" /></a> 
							<p class="author_username"><a href="/<?php echo $username?>" title="Read more of <?php echo (strtolower(substr($username, -1)) == 's')? $username . "'": $username. "'s";  ?> travel experiences and adventure!"><?php echo $username?></a></p> 
							<p><?php echo $traveler_type?></p>
						<?endif;?>   
				</div>		
			<?php endif;?>

			
		</div>
		<div class="content">
			<?php if( is_object($latest_primary_photo) && $latest_primary_photo->getPhotoID() && $total_entries ):?> 
				<div class="photo">
					<a href="/<?php echo $link?>"> 
						<img src="<?php echo $latest_primary_photo->getPhotoLink()?>" alt="journal photo" />
					</a>
				</div>
			<?php endif;?>
				
			<?php if($total_entries):?>
				<div class="entry_title"><a href="/<?php echo $link?>"><?php echo $latest_entry_title?></a></div>
			<?php endif;?>	
				<div class="entry_detail"><span class="location_info"><?php if($total_entries){ echo $latest_entry_location . '&nbsp;|&nbsp;' . $latest_entry_date . '&nbsp;|&nbsp;'; }?>Views: <?php echo $journal_views?></span>
			
				<?php 
					if($total_entries)
						echo $latest_entry_desc . ' ...';
					else 
						echo $journal_desc . ' ...';
				?>
		
			</div>
		
			<?php if($props['is_login']):?><!-- if logged in -->
					<? if (isset($props['action']) && ( strtolower($props['action']) == 'groupjournals' || strtolower($props['action']) == 'groupmembersjournal' ) ) : ?><!--if view is group journals / member journals -->
						<? if (isset($tempAdminGroup) && $props['travelerID'] == $tempAdminGroup->getAdministratorID() && $props['travelerID'] != $objObjectIterator->current()->getTravelerID()) : ?> <!-- if advisor of this group and not owner of journal | show Approve / Unapprove Links-->
							<?if( $objObjectIterator->current()->isApproved($props['gID']) ):?> 
								<a href="/journal.php?action=Unpublish&amp;travelID=<?=$objObjectIterator->current()->getTravelID()?>">Unapprove Member Journal</a>
							<?else:?>
								<a href="/journal.php?action=Publish&amp;travelID=<?=$objObjectIterator->current()->getTravelID()?>">Approve Member Journal</a>
							<?endif;?>
							<?$hasApprovalLink = true;?>
						<?endif; ?>
					<?endif; ?>
			<? endif;?>
			 		
				<?php if( isset($props['action']) && strtolower($props['action']) != 'all' && $props['is_login'] && $props['travelerID'] == $objObjectIterator->current()->getTravelerID() ):?>
						<? (isset($hasApprovalLink) && $hasApprovalLink) ? ' | ' : ''?>
			    		<a href="/journal-entry.php?action=add&amp;travelID=<?php echo $id?>">Add Entry</a> | <a href="/journal.php?action=PublishUnpublish&amp;travelID=<?php echo $id?>"><?=$text_publish?></a> | <a href="/journal.php?action=Edit&amp;travelID=<?php echo $id?>">Edit</a> <?php if(!$objObjectIterator->current()->hasAnyEntry()):?>| <a href="/journal.php?action=Delete&amp;travelID=<?php echo $id?>" onclick="return confirm('Are you sure you want to delete your journal, <?=$entry_title_strip?>?\n\nAll of your entries and photos linked to this journal and comments from other travelers will also be deleted.\nThis cannot be undone.');">Delete</a><?php endif;?>
				<?php endif;?>
				
				<!-- <?if( $is_group ):?>
					<div class="author_info">
						<a href="/group.php?gID=<?php echo $groupID?>" title="Read more of <?php echo (strtolower(substr($username, -1)) == 's')? $username . "'s": $username;  ?> travel experiences and adventure!"><img src="<?php echo $primary_photo?>" width="37px" height="37px" alt="Traveler: <?php echo $username?>" /></a>  
						<a class="author_username" href="/group.php?gID=<?php echo $groupID?>" title="Read more of <?php echo (strtolower(substr($username, -1)) == 's')? $username . "'s": $username;  ?> travel experiences and adventure!"><?php echo $username?>'s</a><br/>
						<span>Advisor Group Journal</span>
					</div>	
				<?endif;?>-->
				
			
		</div>
	
		<div class="foot"></div>
	</div>
	<div class="entry_box">
	
		<a id="up-arrow<?php if( $total_entries > $display) echo $ctr?>" class="<?php if($total_entries > $display):?>journal_button_top<?php else:?>jbuttontop_disable<?php endif;?>"></a>
		<div id="mycarousel<?php if( $total_entries > $display) echo $ctr;?>" class="inside_entry">
			<div class="carousel-clip-region">
		        <ul class="carousel-list">
		        	<?php
		        		$i=1;
		        		foreach( $temp_obj_entry as $obj_entry2 ):
		        			$entry_title       = $obj_entry2->getTitle();
		        			$entry_date        = date('d F',strtotime($obj_entry2->getTrip()->getArrival()));
		        			$entry_photos      = $obj_entry2->countPhotos();
		        			$entry_owner2      = $obj_entry2->getOwner();
		        			$toc_journal_link  = '';
		        			$entry_title_strip = str_replace('-','_',$entry_title);
							$entry_title_strip = str_replace(' ','-',$entry_title_strip); 
							if( get_class($entry_owner2) == 'Traveler' ){
								$toc_journal_link   = $username . '/journals/' . $id;
								$toc_link           = $username . '/journals/' . $id . '/' . $entry_title_strip;
							}
							elseif( is_object($entry_owner2) ){
								$username = str_replace(' ','-',$username);  
								if( $entry_owner2->getParentID() ){
									$subgroup_name  = str_replace(' ','-',$entry_owner2->getName());
									$toc_link       = 'groups/'. $username . '/subgroup/' . $subgroup_name . '/journals/' . $id . '/' . $entry_title_strip;
								}
								else{
									$toc_link       = 'groups/'. $username . '/journals/' . $id . '/' . $entry_title_strip;
								} 
							}
		        	?>
							<li id="mycarousel<?php echo $ctr;?>-item-<?php echo $i;?>" <?php if( $i== count($temp_obj_entry) ):?>class="active"<?php endif;?>> 
								<a href="/<?php echo $toc_link?>"> 
									<strong>
										 <?php 
										 	echo substr($entry_title,0,20);
										 	if( strlen(trim($entry_title)) > 20 ) echo '...'; 
										 ?>
									</strong>
									<span class="entry_date_photo">
										<?php echo $entry_date?> <?php if( $entry_photos ):?>| <?php echo $entry_photos ?> Photo<?php if( $entry_photos != 1 ):?>s<?php endif;?> <span></span><?php endif;?>
									</span>
								</a>
							</li>
					<?php
							$i++;
						endforeach; 
					?> 
				</ul>	 
			</div>
		</div>			
		<a id="down-arrow<?php if( $total_entries > $display ) echo $ctr?>" class="<?php if($total_entries > $display):?>journal_button_bottom<?php else:?>jbuttonbot_disable<?php endif;?>"></a>
	
	</div>

	<div class="clear"></div>
</div>
	

	
<?php 
	if( $total_entries > $display ){  
			ob_start();
?>
				carousel<?php echo $ctr?> = new Carousel("mycarousel<?php echo $ctr?>",  
			        { prevElement:"up-arrow<?php echo $ctr?>", nextElement:"down-arrow<?php echo $ctr?>", size: <?php echo $total_entries?>, firstVisible: <?php echo $total_entries-($display-1)?>});        
<?php	
				$javascript .= ob_get_contents();    
			ob_end_clean();  
			$ctr++;   
		}
		$objObjectIterator->next(); 
	endwhile;
?>


<script type="text/javascript">
//<![CDATA[
	var pageLoad = function() 
	{
		<?php echo $javascript;?>  
	};
	YAHOO.util.Event.addListener(window, 'load', pageLoad); 
//]]> 
</script>
