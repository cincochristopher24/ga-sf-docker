<? for ($i=0; $i<count($videos); $i++): ?>
	<? if (0 == $i % 4): ?>
		<li class = "first_of_four <? if ($videos[$i]->getVideoID() == $videoID): ?>active<?endif;?>">
	<? else: ?>
		<li <? if ($videos[$i]->getVideoID() == $videoID): ?> class = "active" <? endif; ?>>	
	<? endif; ?>
			<? if (isset($travelerID)): ?>
				<? if ('album' == $collectionType): ?>
					<span class="thumbnail">
						<a href="/video.php?action=getTravelerVideos&amp;travelerID=<?= $travelerID ?>&amp;albumID=<?= $videos[$i]->getParentAlbum()->getAlbumID() ?>&amp;videoID=<?= $videos[$i]->getVideoID() ?>&amp;type=album" title="View Video">
							<img src = "<?= $videos[$i]->getVideoImageUrl() ?>" />
						</a>
					</span>
					
				<? else: ?>	
					<span class="thumbnail">
						<a href="/video.php?action=getTravelerVideos&amp;travelerID=<?= $travelerID ?>&amp;travelID=<?= $videos[$i]->getParentTravelLog()->getTravelID() ?>&amp;travelLogID=<?= $videos[$i]->getParentTravelLog()->getTravelLogID() ?>&amp;videoID=<?= $videos[$i]->getVideoID() ?>&amp;type=journal<?php if($jeVideos): ?>&amp;jeVideos=1<?php endif; ?>" title="View Video">
							<img src = "<?= $videos[$i]->getVideoImageUrl() ?>" />
						</a>
					</span>
					
					<a href="/video.php?action=getTravelerVideos&amp;travelerID=<?= $travelerID ?>&amp;travelID=<?= $videos[$i]->getParentTravelLog()->getTravelID() ?>&amp;travelLogID=<?= $videos[$i]->getParentTravelLog()->getTravelLogID() ?>&amp;videoID=<?= $videos[$i]->getVideoID() ?>&amp;type=journal<?php if($jeVideos): ?>&amp;jeVideos=1<?php endif; ?>">
						<strong> <?= $videos[$i]->getParentTravelLog()->getTitle() ?> </strong>  
					</a>
				<? endif; ?>
			<? else: ?>
				<? if ('album' == $collectionType): ?>
					<span class="thumbnail">
						<a href="/video.php?action=getGroupVideos&amp;gID=<?= $groupID ?>&amp;albumID=<?= $videos[$i]->getParentAlbum()->getAlbumID() ?>&amp;videoID=<?= $videos[$i]->getVideoID() ?>&amp;type=album" title="View Video">
							<img src = "<?= $videos[$i]->getVideoImageUrl() ?>" />
						</a>
					</span>	
				<? else: ?>
					<span class="thumbnail">	
						<a href="/video.php?action=getGroupVideos&amp;gID=<?= $groupID ?>&amp;travelID=<?= $videos[$i]->getParentTravelLog()->getTravelID() ?>&amp;travelLogID=<?= $videos[$i]->getParentTravelLog()->getTravelLogID() ?>&amp;videoID=<?= $videos[$i]->getVideoID() ?>&amp;type=journal<?php if($jeVideos): ?>&amp;jeVideos=1<?php endif; ?>" title="View Video">
							<span> <img src = "<?= $videos[$i]->getVideoImageUrl() ?>" /> </span>
						</a>
					</span>
					
					<a href="/video.php?action=getGroupVideos&amp;gID=<?= $groupID ?>&amp;travelID=<?= $videos[$i]->getParentTravelLog()->getTravelID() ?>&amp;travelLogID=<?= $videos[$i]->getParentTravelLog()->getTravelLogID() ?>&amp;videoID=<?= $videos[$i]->getVideoID() ?>&amp;type=journal<?php if($jeVideos): ?>&amp;jeVideos=1<?php endif; ?>">
						<strong> <?= $videos[$i]->getParentTravelLog()->getTitle() ?> </strong> 
					</a>
				<? endif; ?>
			<? endif; ?>
			
			<p><? if (strlen($videos[$i]->getTitle())) echo htmlspecialchars($videos[$i]->getTitle()) ?></p>
			<? if ('00:00' != $videos[$i]->getDuration()): ?> <span class="duration"> <?= $videos[$i]->getDuration() ?> </span> <br/> <? endif; ?> 
			<?php if($isOwner): ?>
				<a href="javascript:void(0)" onclick="Video.showEditCustomPopUp(<?php echo $videos[$i]->getVideoID() ?>, '<?php echo $collectionType ?>')">Edit</a> |
				<a href="javascript:void(0)" onclick="Video.showDeleteCustomPopUp(<?php echo $videos[$i]->getVideoID() ?>, '<?php echo $collectionType ?>')">Delete</a>
			<?php endif; ?>
		</li>
<? endfor; ?>