<?php
/*
 * Created on 10 5, 2006
 * @author Kerwin Gordo
 */
  
?>

<div class="section" id="bulletin">
	<h2>Cork Board 
		<span class="actions"><a href="bulletinManagement.php?method=compose" class="add">New Post</a></span>
	</h2>
 	<div class="content">
		<?if (0 < count($bulletins)) : ?>
		 		<ul>
		 		<?foreach($bulletins as $bulletin) :?>
		 			<li>
			 			<div class="meta date"><?=date('M j, Y',strtotime($bulletin->getCreated()))?></div>
			 			<a class="title" title="View Post" href="bulletinManagement.php?hlEntryID=<?=$bulletin->getAttributeID()?>"><?=$bulletin->getTitle()?></a><br /> 
			 			<div class="meta">
			 				<a class="username" href="/<?=$bulletin->getSource()->getUserName()?>" ><?=$bulletin->getSource()->getUserName() ?></a>
				 			<?$groupLinks = array() ?>
				 			<?$destinations = $bulletin->getDestination() ?>
				 			<?php if(count($destinations)> 0) echo "|"; ?>			 			
				 			<?	 			
				 			foreach($destinations as $dest) {
				 				if ("Group" == get_parent_class($dest)) 
				 					$groupLinks[] = '<a href="group.php?gID=' . $dest->getGroupID() . '">' . $dest->getName() . '</a>';     	 				
				 			}
				 			
				 			$gstr = implode(", ",$groupLinks);
				 			?>
				 			<?=$gstr?>
			 			</div>
					</li>
		 		<?endforeach;?>
		 		</ul>
		 	 
		<? else : ?>
		  <p class="help_text">
		  	 <? $ht = new HelpText();
			    echo nl2br($ht->getHelpText('dashboard-corkboard'));
			 ?>
		  </p>
	 	<? endif; ?>
	 	<?if (10 < $bulletinCount) : ?>
		 	<div class="section_foot">
			 	 <a class="more" href="bulletinManagement.php">View All</a>
			</div>
		<?endif;?>
	</div>
	<div class="foot"></div>
</div>
