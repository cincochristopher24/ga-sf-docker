<?php
	/*
	 * Class.ViewStudentPreferenceForm.php
	 * Created on Nov 30, 2007
	 * created by marc
	 */
	
	require_once("travellog/views/Class.AbstractView.php");

	class ViewStudentPreferenceForm extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.FrmAllPreferences.php');
		}
		
	} 
?>
