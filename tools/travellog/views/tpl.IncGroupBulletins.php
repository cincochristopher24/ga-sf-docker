<?php
/*
 * Created on Oct 20, 2006
 * Author: Czarisse Daphne P. Dolina
 * Tpl.IncGroupBulletin.php - displays group bulletins
 */
?>

<div class="section" id="bulletin">	
		<h2>Bulletin 
			<span class="actions">
				<? if (TRUE == $isMemberLogged) { ?>
					<a href="/bulletinmanagement.php?method=compose&amp;groupID=<?= $grpID ?>" class="add" >New Post </a>	
					<? if (isset($grpBulletinsViewLink) ) { ?>
					
					<? } ?>	 
				<? } ?>
			</span>
		</h2>
		<div class="content">									
			<ul class="bulletin_list">
			<?   for($idx=0;$idx<count($grpBulletins);$idx++){
					$eachbulletin = $grpBulletins[$idx];
			?>
			
				<li>				
					<div class="meta date"><?=date('M j, Y',strtotime($eachbulletin->getCreated()))?></div>
							<a href="/bulletinmanagement.php?hlEntryID=<?=$eachbulletin->getAttributeID()?>&amp;groupID=<?= $grpID ?>"><?=$eachbulletin->getTitle()?></a>
							
							<div class="meta">
							<a href="/<?=$eachbulletin->getSource()->getUsername() ?>" >
								<?=$eachbulletin->getSource()->getUsername() ?>
							</a> |
							
							<?$destinations = $eachbulletin->getDestination() ?>	 			
							
							
							<?	$start = false;
								foreach($destinations as $dest) { 
									if ("Group" == get_parent_class($dest)) { 
										if ($start == true){?>
											,
										<? } ?>
										
										<a href="<?=$dest->getFriendlyURL()?>"><?=$dest->getName() ?></a>
												
							<? 			$start = true;	 
							   		} 
								} 
							?>
							</div>
					
				</li>
			
			<? } ?>
			</ul>
			<? if (0 == count($grpBulletins)) { ?>
				<p>There are no bulletins added to this group.</p>
			<? } ?> 
				
			<? if (isset($grpBulletinsViewLink) ) { ?>	
				<div class="section_foot">
					<a href="bulletinmanagement.php?groupID=<?= $grpID ?>" >View All Posts</a>				
				</div>	
			<? } ?>
		</div>
	
		<div class="clear"></div>
</div>	