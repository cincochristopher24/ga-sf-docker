<?php
	/*
	 * tpl.ViewActivationSupportConfirmationPage.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
?>
<div class="area" id="main">
	<div id="activation_title">
		<h1>Account Activation Support</h1>	
	</div>
	<div id="activation_content">
		<?= $contents["msg"]; ?>
	</div>
	<div class="clear"></div>
</div>		
