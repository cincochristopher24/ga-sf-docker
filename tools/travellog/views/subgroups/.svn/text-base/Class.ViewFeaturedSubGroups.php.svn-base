<?php
  /**
   * @(#) Class.ViewFeaturedSubGroups.php
   * Handles the viewing of the featured subgroups.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - Dec 3, 2008
   */
  
	require_once 'Class.Template.php';
	
	class ViewFeaturedSubGroups {
		private $mTemplate = "tpl.IncViewModeFeaturedSubGroupList.php";
		private $mAlertMsg = '';
		private $mMode = 'view';
		private $mFeaturedGroups = NULL;
		private $mParentGroup = NULL;
		private $mTabAction = "showActive";
		private $mPage = 1;
		private $mCategory = "";
		private $mCategories = null;
    
    /**
     * Sets the categories of the current admin group.
     * 
     * @param SqlResourceIterator $categories The categories of the current admin group.
     * @return void
     */
    function setCategories($categories){
    	$this->mCategories = $categories;
    }    
    
    /**
     * Sets the parent group of the featured groups.
     * 
     * @param AdminGroup $group
     * @return void
     */
    function setParentGroup($group){
    	$this->mParentGroup = $group;
    }
    
    /**
     * Sets the current tab action.
     * 
     * @param string $action values are inactive and active only
     * @return void
     */
    function setTabAction($action){
    	$this->mTabAction = $action;
    }
    
    /**
     * Sets the current page of the subgroup list.
     * 
     * @param integer $page
     * @return void
     */
    function setPage($page){
    	$this->mPage = $page;
    }
    
		/**
		 * Sets the featured subgroups to be shown.
		 * 
		 * @param array $groupList
		 * @return void
		 */
		function setSubGroupList($groupList){
			$this->mFeaturedGroups = $groupList;
		}
		
		/**
		 * Sets the alert message to be shown in the featured groups section.
		 * 
		 * @param string $_alertMsg
		 * @return void
		 */
		function setAlertMessage($_alertMsg){
			$this->mAlertMsg = $_alertMsg;
		}
		
		/**
		 * Sets the category tto be shown.
		 * 
		 * @param string|integer $category
		 */		
		function setCategory($category){
			$this->mCategory = $category;
		}
		
		/**
		 * Sets the view mode whether it is in arrange mode or in view mode.
		 * 
		 * @param string $mode one of the two values (arrange or view)
		 * @return void
		 */
		function setMode($_mode){
			$this->mMode = $_mode;
			$this->mTemplate = "tpl.IncViewModeFeaturedSubGroupList.php";
		}	
    
    /**
     * Fetches the template of featured subgroups. 
     * Values on the template will be based on the properties set.
     * 
     * @throws exception throws exception when $this->mFeaturedGroups is not set or set to null
     * @return string
     */
		function fetchTemplate(){
			if (is_null($this->mFeaturedGroups))
			  throw new exception("Set first the featured subgroups to be shown.");
			$tpl = new Template();
			$tpl->set_path("travellog/views/subgroups/");
			$tpl->set("iterator", $this->mFeaturedGroups);
			$tpl->set("alertMsg", $this->mAlertMsg);
			$tpl->set("mode", $this->mMode);
			$tpl->set("action", $this->mTabAction);
			$tpl->set("grpID", $this->mParentGroup->getGroupID());
			$tpl->set("parent", $this->mParentGroup);
			$tpl->set("page", $this->mPage);
			$tpl->set("category", $this->mCategory);
			
			return $tpl->fetch("tpl.IncViewModeFeaturedSubGroupList.php");
		}

    /**
     * Shows the featured subgroups template. Values will be based on the properties set.
     * 
     * @throws exception throws exception when $this->mFeaturedGroups is not set or set to null
     * @return void
     */
		function render(){
			try {
			  echo $this->fetchTemplate();
			}
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}
		}	
	}