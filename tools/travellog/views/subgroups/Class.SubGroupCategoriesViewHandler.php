<?php
  /**
   * @(#) Class.SubGroupCategoriesViewHandler.php
   * 
   * A view handler of the subgroup categories.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 01 6, 09
   */
  
  class SubGroupCategoriesViewHandler {
  	protected $parentID = 0;  // the ID of the parent group
  	protected $categories = array(); // the categories of the parent group
  	protected $message = ""; // the message that shows after deletion or update
  	protected $categoriesCount = 0; // the number of categories of the parent group. The number of categories shown may not 
  																	// be equal to the $categoriesCount because its possible that the categories are being under a limit.
  	
  	/**
  	 * Renders the list of categories of the given parent group.
  	 * 
  	 * @return void
  	 */
  	function renderListOfCategories(){
			echo $this->fetchListOfCategories();
  	}
  	
  	/**
  	 * Assign variables and creates the template for viewing the list of 
  	 * categories of the given parent group.
  	 * 
  	 * @return string
  	 */
  	function fetchListOfCategories(){
			$tpl = new Template();
			$tpl->set_path("travellog/views/subgroups/");
			$tpl->set("categories",$this->categories);
			$tpl->set("categoryCount",$this->categoriesCount);
			$tpl->set("message", $this->message);
			$tpl->set("grpID", $this->parentID );
			
			return preg_replace("/\s+/", " ", $tpl->fetch('tpl.IncSubGroupCategories.php'));
  	}
  	
  	/**
  	 * Sets the categories to be shown.
  	 * 
  	 * @param array $categories
  	 * @return void
  	 */
  	function setCategories($categories){
  		$this->categories = $categories;
  	}
  	
  	/**
  	 * Sets the number of categories of the parent group to be shown.
  	 * 
  	 * @param integer $count
  	 * @return void
  	 */
  	function setCategoriesCount($count){
  		$this->categoriesCount = $count;
  	}
  	
  	/**
  	 * Sets the confirmation message to be shown.
  	 * 
  	 * @param string $message
  	 * @return void
  	 */
  	function setMessage($message){
  		$this->message = $message;
  	}
  
  	/**
  	 * Sets the parent group ID that is to be shown.
  	 * 
  	 * @param integer $id
  	 * @return void
  	 */
  	function setParentGroupID($id){
  		$this->parentID = $id;
  	}
  }
