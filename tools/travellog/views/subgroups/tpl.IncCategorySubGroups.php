<ul class="groups" id="groups_list<?=$categoryID?>">
	<input type="hidden" id="subgroups_page<?=$categoryID?>" name="subgroups_page<?=$categoryID?>" value="<?= $page ?>" />
	
	<? while($iterator->hasNext()): ?>
	<?php
		$group = $iterator->next();
		$subgroup_name = str_replace('-', '_', $group->getName());
		$subgroup_name = str_replace(' ', '-', $subgroup_name);
		$group_name = str_replace('-', '_', $parent->getName());
		$group_name = str_replace(' ', '-', $group_name);
			
		$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;
		
		$members_count = $group->getMembersCount();
		$staff_count = $group->getStaffCount();
		// $journals_count = $group->getJournalsCount();
		$journals_count = $group->getJournalEntriesCount();
		$photos_count = ($group->getAlbumPhotosCount() + ((0 == $group->getGroupPhotoID()) ? 0 : 1 ));
		$videos_count = $group->getVideosCount();
		$articles_count = $group->getArticlesCount();
	?>
		<!-- start looping group -->
		<li> 
			<a href="<?= $friendlyURL ?>" class="thumb" title="View Group Profile">
				<img src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>" alt="Group Logo" />
			</a>
			<div class="details"> 
				<a class="groupname" href="<?= $friendlyURL ?>"><?= $group->getName() ?></a> 
				
				<? if ($isAdminLogged): ?>
					<span id="featured_label<?=$group->getGroupID()?>" <? if (!$group->isFeatured()){ ?>style="display:none;"<? } ?> class="featured_label">Featured</span>
				<? endif; ?>
				
				<div class="group_stats"> 
					<span><?php echo (0 == $members_count) ? "No" : $members_count; ?> Member<?php if (1 != $members_count) {?>s<? } ?></span>|
					<span><?php echo (0 == $staff_count) ? "No" : $staff_count;  ?> Staff</span> 
				</div>
				
				<div class="group_stats"> 
					<span><?php echo (0 == $journals_count) ? "No" : $journals_count; ?> Journal Entr<?php if (1 != $journals_count) {?>ies<? }else{?>y<?php } ?></span>|
					<span><?php echo (0 == $articles_count) ? "No" : $articles_count; ?> Article<?php if (1 != $articles_count) {?>s<?php }?></span>|
					<span><?php echo (0 == $photos_count) ? "No" : $photos_count; ?> Photo<?php if (1 != $photos_count) {?>s<? } ?></span>| 
					<span><?php echo (0 == $videos_count) ? "No" : $videos_count; ?> Video<?php if (1 != $videos_count) {?>s<? } ?></span> 
				</div>
				
				<div class="group_actions">
				  <? if ($isAdminLogged) : ?>
					  <a href="javascript:void(0)" id="unfeature<?=$group->getGroupID()?>" <? if (!$group->isFeatured()){?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Remove <?=addslashes($group->getName())?> from Featured Groups ?','Are you sure you want to remove <?= addslashes($group->getName()) ?> from Featured Groups?',function(){manager.unfeature('action=unsetFeatured&amp;mode=list&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;show=true', <?=$group->getGroupID()?>);},'Remove','1');CustomPopup.setJS();CustomPopup.createPopup();" >
					  	Remove in Features
					  </a>
					  <a href="javascript:void(0)" id="feature<?=$group->getGroupID()?>" <? if ($group->isFeatured()){?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Feature <?= addslashes($group->getName()) ?> ?','Are you sure you want to feature <?= addslashes($group->getName()) ?>?',function(){manager.feature('action=setFeatured&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;show=true', <?=$group->getGroupID()?>);},'Feature','1');CustomPopup.setJS();CustomPopup.createPopup();">
					    Feature this Subgroup
					  </a>
					  
					  <? if (0 < $categoryCount): ?>
					  	<a href="javascript:void(0)" onclick = "manager.showCategoryAssigner('Move <?=addslashes($group->getName())?> To Another Category?', <?= $categoryID ?>, '?action=assign_category&amp;gID=<?=$parent->getGroupID()?>&amp;mode=view&amp;sgID=<?=$group->getGroupID()?>')" >Move to another category</a>
						<? endif; ?>
						
						<a href="/messages.php?act=messageGroupStaff&gID=<?= $group->getGroupID() ?>">Message Staff</a>
						<? $popup_link = "/group.php?mode=delete&amp;confirm&amp;gID=".$group->getGroupID() ?>
						<a href="javascript:void(0)" onclick = "CustomPopup.initialize('Delete <?= addslashes($group->getName()) ?> ?','Are you sure you want to delete <?= addslashes($group->getName()) ?>?','<?=$popup_link?>','Delete','0');CustomPopup.createPopup();return false;">	
							Delete
						</a>
						
						<? if (1 < ceil($groups_count/$numPerPage)): ?>
							<a href="javascript:void(0)" onclick="manager.showPageAssigner('Move <?= addslashes($group->getName()) ?> to other page?', <?= $page ?>, <?=ceil($groups_count/$numPerPage)?>, 'action=transfer_page_category&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;tabAction=<?=$tabAction?>&amp;page=<?= $page?>&amp;numPerPage=<?=$numPerPage?>', <?=$categoryID?>)">
								Transfer Page
							</a>
						<? endif; ?>			
					<? endif; ?>
				</div>
			</div>
			<input type="hidden" name="sort_subgroup_ids<?=$categoryID?>[]" value="<?= $group->getGroupID() ?>">								
		</li>
		<!-- end looping group -->
	<? endwhile; ?>
	<? if (1 < ceil($groups_count/$numPerPage)): ?>
		<?=$paging->showPagination();?>
	<? endif; ?>	
</ul>	
<? if ($isAdminLogged AND "" == trim($keyword) AND 1 < $categoryCount) : ?>
	<script type="text/javascript">
		jQuery('#groups_list<?=$categoryID?>').sortable({ items: 'li', axis: 'y' });
	</script>
<? endif; ?>