<?php
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	Template::includeDependentJs('http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js');
	Template::includeDependentJs('http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js');
	Template::includeDependentJs('/js/subGroupsManager.js');
	Template::includeDependentJs('/js/GroupTemplateManager.js');
?>
  <?= $profile->render()?>
  
  <?=$subNavigation->show();?>

	<div class="layout_2" id="content_wrapper">
		<div id="wide_column">
			<div id="groups_category_mode" class="section">	
				<h2>
					<span>Subgroups</span>
					<span class="header_actions">						
					<?php	if( $isAdminLogged ): ?>  
					  <a href="javascript:void(0)" style="<?=("showActive" === $tabAction) ? "color:gray;" : "";?>" onclick="manager._viewActive('gID=<?=$grpID?>&amp;mode=ajax_list&amp;tabAction=showActive&amp;txtGrpName=<?=$searchKey?>');" id="viewActive">
					    Active (<strong id="active_id"><?=$cnt_active?></strong>)
					  </a> 
					  |
					  <a href="javascript:void(0)" style="<?=("showInActive" === $tabAction) ? "color:gray;" : "";?>"  onclick="manager._viewInActive('gID=<?=$grpID?>&amp;mode=ajax_list&amp;tabAction=showInActive&amp;txtGrpName=<?=$searchKey?>');" id="viewInActive">
					    Inactive (<strong id="inactive_id"><?=$cnt_inactive?></strong>) 
					  </a>
					<?php endif; ?>
					  <input type="hidden" id="tabAction" value="<?=$tabAction?>" />
					</span>
				</h2>

				<div id="loader" class="widePanel content" style="display: none;">
					<p class="loading_message">
						<span id="imgLoading">
							<img alt="Loading" src="/images/loading.gif"/>
						</span>
						<em id="statusCaption">Loading data please wait...</em>
					</p>
				</div>
				
				<div id="content_to_hide">
					<div class="content" id="subGroupsContent">
						<?= $subgroups ?>
					</div>
				</div>
				
				<?= $assigner ?>
			</div>
		</div>
		
		<div id="narrow_column">
			<? if (20 < $cnt_active OR 20 < $cnt_inactive OR $is_searching): ?>
				<div class="section" id="quick_tasks2">
					<h2><span>Search</span></h2>
					<div class="content">
						<ul class="actions">
							<span>
								<input type="text" class="text" id="txtGrpName" name="txtGrpName" value="<?=$searchKey?>" />										
							</span>
							<span>		
								Search By:
								<select id="search_by" name="selSearchBy">
									<option value="group" <?php if ("group" == $search_by) {?>selected="selected"<?}?> > Group Name </option>
									<option value="category" <?php if ("category" == $search_by) {?>selected="selected"<?}?> > Category Name </option>
								</select>
							</span>
							<span>	
								<br />	
								<input type="button" value="Search" onclick="manager.search('/subgroups.php?gID=<?=$grpID?>&amp;action=search&amp;search_by='+jQuery('#search_by').val())" />
							</span>
					</div>				
				</div>
			<? endif; ?>
			
			<? if ($isAdminLogged): ?>
								
				<div class="section" id="quick_tasks">
					
					<label>Group Tasks</label>
					
					<div class="content">
						
						<ul class="actions">
							<li>
								<a href="/group.php?mode=add&amp;pID=<?=$grpID?> " class="button" >
									+ Add Subgroup
								</a>
							</li>
							<li>
								<a href="/subgroupcategories.php?gID=<?=$grpID?>&amp;mode=viewAddCategory&amp;fromMode=subgroups" class="button">
								  + Add Category
								</a>
							</li>
							<li>
								<a href="/group_template.php?action=view_template_manager&amp;group_id=<?php echo $grpID; ?>" class="button">
								 View Template Manager
								</a>
							</li>
						</ul>
					
					</div>	
				</div>
			<? endif; ?>
			
		</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#txtGrpName').keypress(function(e){
			if(13 == e.keyCode) {
				manager.search('/subgroups.php?gID=<?php echo $grpID; ?>&action=search&search_by='+jQuery('#search_by').val());
			}
		});
	});
	<? if(isset($success)): ?>
		CustomPopup.initPrompt("<?=$success?>");
		window.onload = function(){
			CustomPopup.createPopup();
		}
	<? endif; ?>
</script>