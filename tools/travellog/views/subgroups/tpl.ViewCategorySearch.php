<?php
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	Template::includeDependentJs('/js/subgroup.js');
	
	$data = array();
	$data['num_per_page'] = 6;
	$has_groups_shown = false;
?>
  <div id="intro_container">
	  <div id="intro">
		  <h1>Get expert travel advice.</h1>
		  <div class="content">
		  	Join Groups and get reliable expert tips and advice from savvy travelers and professionals.
		  </div>
	  </div>
  </div>

	<div class="layout_2" id="content_wrapper">
		<div id="wide_column">
			
			<div id="groups_category_mode" class="section">	
				<h2>
					<span>Groups</span>
				</h2>
				
				<div id="content_to_hide">
					<div class="content" id="subGroupsContent">
						<? if (0 < $categories->size()): ?>
							<?php $categories->rewind() ?>
							<ul id="category_list" class="groups">
								<? while($categories->hasNext()): ?>
									<?php 
										$data['category'] = $categories->next();
										$data['groups_count'] = $data['category']->getSubGroupsCount(); 
										$data['groups'] = $data['category']->getSubGroups(null, null, " categorysubgrouprank", new RowsLimit($data['num_per_page'], 0)); 
										$data['parent_group'] = $parent_group;
										$data['paging'] = new Paging($data['groups_count'], 1, $data['category']->getCategoryID()."','/subgroups/ajax_search/".$data['category']->getCategoryID().'/'.$keyword, $data['num_per_page'], true, true);
										$data['paging']->setOnclick("SubGroupManager.paging");
										$data['page'] = 1;
										
										$tpl = new Template();
										$tpl->setVars($data);									
									?>
									<?php if (0 < $data['groups_count'] || (0 < strlen($keyword) AND $is_admin_logged)): ?>
										<?php $has_groups_shown = true; ?>
										<li class="category_box" id="li_category_id_<?= $data['category']->getCategoryID() ?>">
											<div class="category_header">
												<h3 id="cat_head<?=$data['category']->getCategoryID()?>" > 
														<?= $data['category']->getName() ?>
												</h3>
												<? if (0 < $data['groups_count'] ): ?>
													<a class="clipper collapse" href="javascript:void(0)" title="Collapse/Clip this category" onclick="SubGroupManager.slide(<?= $data['category']->getCategoryID() ?>, this)">&raquo;</a>	
													<input type="hidden" id="slider<?= $data['category']->getCategoryID() ?>_slide" value="0" />		
												<? endif; ?>
																	
											</div>
											<div id="loader<?=$data['category']->getCategoryID()?>" class="widePanel content" style="display: none;">
												<p class="loading_message">
													<span id="imgLoading">
														<img alt="Loading" src="/images/loading.gif"/>
													</span>
													<em id="statusCaption">Loading data please wait...</em>
												</p>
											</div>
											
											<div id="slider<?=$data['category']->getCategoryID()?>" >				
												<?php echo preg_replace('/\s+/', ' ', $tpl->fetch('travellog/views/subgroups/tpl.ViewAjaxSearch.php')); ?>
											</div>
										</li>
									<?php endif; ?>
								<? endwhile; ?>
							</ul>
						<? endif; ?>
						
						<?php if (!$has_groups_shown): ?>
							<? if (0 < strlen($keyword)): ?>
								<p class="help_text"> <span>No matching results found for search of category name '<?php echo $keyword; ?>'<span>.</p>
							<? else: ?>
								<p class="help_text"> <span>There are no categories</span>.</p>
							<? endif; ?>
						<?php endif; ?>
					</div>
				</div>
				
			</div>
		</div>
		
		<div id="narrow_column">
			<div class="section" id="quick_tasks2">
				<h2><span>Search</span></h2>
				<div class="content">
					<ul class="actions">
						<span>
							<input type="text" style="width: 100%" class="text" id="txtKeyword" name="txtKeyword" value="<?php echo $keyword; ?>" />				
						</span>
						<span>		
							Search By:
							<select id="selSearchBy" name="selSearchBy">
								<?php if (strlen($keyword)): ?>
									<option value="group" > Group Name </option>
									<option value="category" selected="selected" > Category Name </option>
								<?php else: ?>
									<option value="group" selected="selected" > Group Name </option>
									<option value="category" > Category Name </option>								
								<?php endif; ?>
							</select>
						</span>
						<span>	
							<br />	
							<input type="button" value="Search" onclick="SubGroupManager.search(jQuery('#selSearchBy').val(), jQuery('#txtKeyword').val());" />
						</span>
					</ul>
				</div>				
			</div>
		</div>
</div>

<!--added by ianne - 11/20/2008 success message after confirm-->

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#txtKeyword').keypress(function(e){
			if(13 == e.keyCode) {
				SubGroupManager.search(jQuery('#selSearchBy').val(), jQuery('#txtKeyword').val());
			}
		});
	});
	<?php if(isset($success)): ?>
		CustomPopup.initPrompt("<?=$success?>");
		window.onload = function(){
			CustomPopup.createPopup();
		}
	<?endif;?>
</script>