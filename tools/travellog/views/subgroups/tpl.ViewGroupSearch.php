<?php
	Template::setMainTemplateVar('layoutID', 'main_pages');
	Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	Template::includeDependentJs('/js/subgroup.js');
?>

<div id="intro_container">
  <div id="intro">
	  <h1>Get expert travel advice.</h1>
	  <div class="content">
	  	Join Groups and get reliable expert tips and advice from savvy travelers and professionals.
	  </div>
  </div>
</div>

<div class="layout_2" id="content_wrapper">
	<div id="wide_column">
		<div id="groups_and_clubs" class="section">	
    <h2><span>Groups</span></h2>
    
    <div class="content" id="subGroupsContent">
      <?php if (0 < $groups->size()): ?>
	      <ul id="groups_list_main">
	        <? while($groups->hasNext()): ?>
	  				<?php
	        		$group = $groups->next(); 
							$subgroup_name = str_replace('-', '_', $group->getName());
							$subgroup_name = str_replace(' ', '-', $subgroup_name);
							$group_name = str_replace('-', '_', $parent_group->getName());
							$group_name = str_replace(' ', '-', $group_name);
								
							$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;	            	
	        	?>
	          
	          <li itemid="<?= $group->getGroupID() ?>">	
	            <a class="group_thumb" href="<?= $friendlyURL ?>">
	              <img src="<?= $group->getGroupPhoto()->getPhotoLink('standard'); ?>" alt="<?= $group->getName(); ?>" >
	            </a>					
				
			          <div class="groups_info">
					        <strong>
					          <a href="<?= $friendlyURL ?>">
					            <?= $group->getName(); ?>
					          </a>
					        </strong>
					
					        <p>
						        <?= StringFormattingHelper::wordTrim($group->getDescription(), 40)?>
						
						        <? if (40 < count(explode(' ', strip_tags($group->getDescription()) ) ) ) : ?>
						          <?='&#8230;' ?>
						        <? endif ; ?> 
						
						        <a href="<?= $friendlyURL ?>" title="Read more about the <?= $group->getName() ?> travel community">
						          Read More
						        </a>
					        </p>
					
					        <? if (trim($group->getGroupMembershipLink('cobrand_search', $page)) !== ''):?>
				          <p class="action">
				            <?=$group->getGroupMembershipLink('cobrand_search', $page)?>
				          </p>
	              <? endif; ?>
				        </div>
			        </li>		  
		        <? endwhile; ?>
	        </ul>
	       
			    <?php $paging->showPagination(); ?>
		    <?php else: ?>
					<? if (strlen($keyword)): ?>
						<p class="help_text"> <span>No matching results found for search of group name '<?php echo $keyword; ?>'<span>.</p>
					<? else: ?>
						<p class="help_text"> <span>There are no groups</span>.</p>
					<? endif; ?>
				<?php endif; ?>
    
		  </div>
		</div>
	</div>

	<div id="narrow_column">
		<div class="section" id="quick_tasks2">
			<h2><span>Search</span></h2>
			<div class="content">
				<ul class="actions">
					<span>
						<input type="text" style="width: 100%" class="text" id="txtKeyword" name="txtKeyword" value="<?php echo $keyword; ?>" />				
					</span>
					<span <?php if (0 == $categories_count) { ?>style="display:none;"<? } ?>>		
						Search By:
						<select id="selSearchBy" name="selSearchBy">
							<option value="group" selected="selected" > Group Name </option>
							<option value="category"  > Category Name </option>
						</select>
					</span>
					<span>	
						<br />	
						<input type="button" value="Search" onclick="SubGroupManager.search(jQuery('#selSearchBy').val(), jQuery('#txtKeyword').val());" />
					</span>
				</ul>
			</div>				
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#txtKeyword').keypress(function(e){
			if(13 == e.keyCode) {
				SubGroupManager.search(jQuery('#selSearchBy').val(), jQuery('#txtKeyword').val());
			}
		});
	});
	<?php if(isset($success)): ?>
		CustomPopup.initPrompt("<?php echo $success; ?>");
		window.onload = function(){
			CustomPopup.createPopup();
		}
	<?php endif; ?>
</script>