<div class="section" id="sub_groups">
	<h2>
	  	<span>
			<?= $category->getName() ?>
		</span>
		
		<span class="header_actions">
			<a href='/subgroups.php?gID=<?php echo $category->getParentGroupID() ?>'>View All</a>
		</a>
		
	</h2>
	
	<div class="content">
		<? if (empty($subgroups)): ?>
			<p class="side_help_text"> Category has no featured subgroups.</p>
		<? else: ?>
			<ul class="groups">
				<?= $subgroups ?>
			</ul>
		<? endif; ?>
	</div>
</div>
	
