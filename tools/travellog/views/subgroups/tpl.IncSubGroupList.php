<?php $tabAction = (SubGroup::ACTIVE == $access) ? "showActive" : "showInActive" ?>

<div class="section_small_details">					
		<? if ($isAdminLogged AND !$is_searching AND 1 < $subgroupsCount): ?>
			You can arrange <strong>subgroups</strong> by dragging them.	
		<? else: ?>
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		<? endif; ?>				
		<div class="section_filter">
			<label>View Mode:</label>
			<select onchange="window.location.href = this.value;">
				<option value="/subgroups.php?gID=<?= $parent->getGroupID() ?>&amp;txtGrpName=<?=$keyword?>&amp;tabAction=<?=$tabAction?>"> Category View </option>
				<option value="/subgroups.php?gID=<?= $parent->getGroupID() ?>&amp;mode=list&amp;txtGrpName=<?=$keyword?>&amp;tabAction=<?=$tabAction?>" selected> List View </option>
			</select>
		</div>						
</div>

<? if ($isAdminLogged AND !$is_searching AND 1 < $subgroupsCount): ?>
	<input type="button" value="Save" id="save_but" onclick="manager.saveCustomSort('page=<?=$page?>&gID=<?= $parent->getGroupID() ?>&tabAction=<?=$tabAction?>&action=customsortgroups');" />
	<p id="orderLoader" style="display:none;"> Saving order... </p>
<? endif; ?>

<? if (0 < $iterator->size()): ?>
<ul id="group_list" class="groups" >							
	<? while($iterator->hasNext()): ?>
		<?php
			$group = $iterator->next();
			$subgroup_name = str_replace('-', '_', $group->getName());
			$subgroup_name = str_replace(' ', '-', $subgroup_name);
			$group_name = str_replace('-', '_', $parent->getName());
			$group_name = str_replace(' ', '-', $group_name);
				
			$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;			
			
			$category = $group->getCategory();
			$category_name = (is_null($category)) ? "None" : $category->getName();
			
			$members_count = $group->getMembersCount();
			$staff_count = $group->getStaffCount();
			// $journals_count = $group->getJournalsCount();
			$journals_count = $group->getJournalEntriesCount();
			$photos_count = ($group->getAlbumPhotosCount() + ((0 == $group->getGroupPhotoID()) ? 0 : 1 ));
			$videos_count = $group->getVideosCount();
			$articles_count = $group->getArticlesCount();
		?>
		<!-- start looping group -->
		<li class="list_box groups"> 
			<a href="<?= $friendlyURL ?>" class="thumb" title="View Group Profile">
				<img src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>" alt="Group Logo" />
			</a>
			<div class="details"> 
				<a class="groupname" href="<?= $friendlyURL ?>"><?= $group->getName() ?></a>
				
				<? if ($isAdminLogged): ?>
					<span id="featured_label<?=$group->getGroupID()?>" <? if (!$group->isFeatured()){ ?>style="display:none;"<? } ?> class="featured_label">Featured</span>
				<? endif; ?>
				
				<div class="group_stats"> 
					<span><?php echo (0 == $members_count) ? "No" : $members_count; ?> Member<?php if (1 != $members_count) {?>s<? } ?></span>|
					<span><?php echo (0 == $staff_count) ? "No" : $staff_count;  ?> Staff</span> 
				</div>
				
				<div class="group_stats"> 
					<span><?php echo (0 == $journals_count) ? "No" : $journals_count; ?> Journal Entr<?php if (1 != $journals_count) {?>ies<?php }else{ ?>y<?php } ?></span>|
					<span><?php echo (0 == $articles_count) ? "No" : $articles_count; ?> Article<?php if (1 != $articles_count) {?>s<?php }?></span>|
					<span><?php echo (0 == $photos_count) ? "No" : $photos_count; ?> Photo<?php if (1 != $photos_count) {?>s<? } ?></span>| 
					<span><?php echo (0 == $videos_count) ? "No" : $videos_count; ?> Video<?php if (1 != $videos_count) {?>s<? } ?></span> 
				</div>	
				
				<div class="group_category_label"> 
					<strong> Category:&nbsp;<?= $category_name ?></strong>
				</div>										
				
				<div class="group_actions">
				  <? if ($isAdminLogged): ?>
					  
					  <a href="javascript:void(0)" id="unfeature<?=$group->getGroupID()?>" <? if (!$group->isFeatured()){?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Remove <?=addslashes($group->getName())?> from Featured Groups ?','Are you sure you want to remove <?= addslashes($group->getName()) ?> from Featured Groups?',function(){manager.unfeature('action=unsetFeatured&amp;mode=list&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>', <?=$group->getGroupID()?>);},'Remove','1');CustomPopup.setJS();CustomPopup.createPopup();" >
					  	Remove in Features
					  </a>
					  <a href="javascript:void(0)" id="feature<?=$group->getGroupID()?>" <? if ($group->isFeatured()){?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Feature <?= addslashes($group->getName()) ?> ?','Are you sure you want to feature <?= addslashes($group->getName()) ?>?',function(){manager.feature('action=setFeatured&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>', <?=$group->getGroupID()?>);},'Feature','1');CustomPopup.setJS();CustomPopup.createPopup();">
					    Feature this Subgroup
					  </a>
						
						<? if (0 < $categoryCount): ?> 
						  <? if (is_null($category)): ?>
						  	<a href="javascript:void(0)" onclick = "manager.showCategoryAssigner('Assign <?=addslashes($group->getName())?> To A Category?', 0)" >Assign to a category</a>
						  <? else:?>
						  	<a href="javascript:void(0)" onclick = "manager.showCategoryAssigner('Move <?=addslashes($group->getName())?> To Another Category?', <?= $category->getCategoryID() ?>, '?action=assign_category&amp;gID=<?=$parent->getGroupID()?>&amp;mode=list&amp;sgID=<?=$group->getGroupID()?>')" >Move to another category</a>
						  <? endif; ?>
					  <? endif; ?>
					  
						<a href="/messages.php?act=messageGroupStaff&gID=<?= $group->getGroupID() ?>">Message Staff</a>
						<? $popup_link = "/group.php?mode=delete&amp;confirm&amp;gID=".$group->getGroupID() ?>
						<a href="javascript:void(0)" onclick = "CustomPopup.initialize('Delete <?= addslashes($group->getName()) ?> ?','Are you sure you want to delete <?= addslashes($group->getName()) ?>?','<?=$popup_link?>','Delete','0');CustomPopup.createPopup();return false;">	
							Delete
						</a>
						
						<? if (1 < $numPages): ?>
							<a href="javascript:void(0)" onclick="manager.showPageAssigner('Move <?= addslashes($group->getName()) ?> to other page?', <?=$page?>, <?=$numPages?>, 'action=transfer_page_list&amp;gID=<?=$parent->getGroupID()?>&amp;sgID=<?=$group->getGroupID()?>&amp;tabAction=<?=$tabAction?>&amp;page=<?=$page?>')">
								Transfer Page
							</a>
						<? endif; ?>
						
						<a href="javascript:GroupTemplateManager.showCreateForm(<?php echo $group->getGroupID(); ?>);" title="">Save as template</a>
						
					<? endif; ?>
				</div>
			</div>	
			<input type="hidden" name="sort_subgroup_ids[]" value="<?= $group->getGroupID() ?>">								
		</li>
		<!-- start looping group -->
	<? endwhile; ?>
</ul>

<? if (1 < $numPages): ?>
	<?=$paging->showPagination();?>
<? endif; ?>

<script type="text/javascript">
	<?php if ($isAdminLogged AND "" == trim($keyword) AND 1 < $subgroupsCount): ?>
		jQuery('#group_list').sortable({ items: 'li', axis: 'y' });
	<?php endif; ?>
</script>

<? else: ?>
	<?php if (!$is_searching): ?>
		<p class="help_text"><span>There are no <?php if ($isAdminLogged) { echo ($access = SubGroup::ACTIVE) ? "active" : "inactive";} ?> subgroups</span>.</p>
	<?php else: ?>
		<p class="help_text"> <span>No matching results found for search of group name '<?php echo $keyword; ?>'<span>.</p>
	<?php endif; ?>
<? endif; ?>