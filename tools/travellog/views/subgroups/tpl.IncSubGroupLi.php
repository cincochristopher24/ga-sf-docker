<?	
	$subgroup_name = str_replace('-', '_', $group->getName());
	$subgroup_name = str_replace(' ', '-', $subgroup_name);
	$group_name = str_replace('-', '_', $parent->getName());
	$group_name = str_replace(' ', '-', $group_name);
		
	$friendlyURL = "/groups/". str_replace(' ','-', $group_name) . "/subgroup/" .str_replace(' ','-',$subgroup_name) ;
	
	// photo tag
	$cntPhotos = (0 < $group->getGroupPhotoID()) ? 1 : 0;
	$cntPhotos = $cntPhotos + $group->getAlbumPhotosCount();
	 
	$photo_tag_cnt = (1 < $cntPhotos)  ? $cntPhotos.' Photos' : $cntPhotos.' Photo';
	$photo_tag_cnt = (0 == $cntPhotos) ? "" : $photo_tag_cnt;
	// end of photo tag
	
	// journal tag
	// $cntJournals = $group->getJournalsCount();
	$cntJournals = $group->getJournalEntriesCount();
	
	// article tag
	$cntArticles = $group->getArticlesCount();
  
	if($cntJournals > 0 && $cntArticles > 0){
		$journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journal Entries' : $cntJournals.' Journal Entry';
		$journal_tag_cnt .= (1 < $cntArticles) ? ' | '.$cntArticles.' Articles' : ' | '.$cntArticles.' Article';
	}else if($cntJournals <= 0 && $cntArticles > 0)
		$journal_tag_cnt = (1 < $cntArticles) ? $cntArticles.' Articles' : $cntArticles.' Article';
	else if($cntJournals > 0 && $cntArticles <= 0)
		$journal_tag_cnt = (1 < $cntJournals) ? $cntJournals.' Journal Entries' : $cntJournals.' Journal Entry';
	else
		$journal_tag_cnt = "";
	// end of journal tag
  
	// members tag
	$cntMembers = $group->getMembersCount();
	$members_tag_cnt = (1 < $cntMembers) ? $cntMembers.' Members' : $cntMembers.' Member';
	$members_tag_cnt = (0 == $cntMembers) ? '' : $members_tag_cnt;
	// end of members tag
	
	$group_name = (20 < strlen($group->getName()) && !strpos($group->getName()," ")) ? substr($group->getName(),0,20) . "..." : $group->getName();
?>			

<li>
	<a title="View Group Profile" class="thumb" href="<?=$friendlyURL?>"><img class="pic" width="65px" height="65px" alt="Group Emblem" src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>"  /></a>
		
	<div class="details">
		<a class="groupname" href="<?=$friendlyURL?>" title="<?=$group->getName()?>"><?=$group_name?></a>
			
		<p>
			<?= $photo_tag_cnt ?>   <? if ($cntPhotos) { ?> <br /> <? } ?>
			<?= $members_tag_cnt ?> <? if ($cntMembers) { ?> <br /> <? } ?>
			<?= $journal_tag_cnt ?> <? if ($cntJournals || $cntArticles) { ?> <br /> <? } ?>
			
			<? if (0 == $cntMembers): ?>
				<? if (isset($loggedUser) && FALSE == $loggedUser->isAdvisor()):
						$validGrpAccess = array(GroupAccess::$OPEN, GroupAccess::$OPEN_APPROVAL);
						if ( $isLogged && !$isAdminLogged  && in_array($group->getGroupAccess(), $validGrpAccess) && in_array ($parent->getGroupAccess(), $validGrpAccess ) ):
							if(!$group->isInRequestList($loggedUser)):
								$message = 'You are about to join the '.strtolower($group->getLabel()).' '.$group->getName().'. Your request is subject to the administrator&lsquo;s approval and will be sent after clicking Join. You will be able to interact with this group once your request has been approved.';
								$refLink = '&amp;ref=groupsubgroup';
								
								if($group->getGroupAccess() == GroupAccess::$OPEN):
									$message = 'You are about to join ' . $group->getName().'. You will automatically be a member of the group after clicking Join.';
								endif;
								
								$strlinkjoin = ' Do you want to <a href="javascript:void(0)" onclick="CustomPopup.initialize(\'Join '.$group->getName().' ?\',\'Are you sure you want to join the '.strtolower($eachsubgroup->getLabel()).' '.$eachsubgroup->getName().'?\',\'/membership.php?mode=join&amp;gID='. $group->getGroupID().$refLink.'&confirm\',\'Join\',\'0\',\''.$message.'\');CustomPopup.createPopup();">join</a> this group?';
							endif;				
						endif;
					endif;
		
					echo  'Group has no members yet.' ; echo (isset($strlinkjoin)) ? $strlinkjoin : '';				
				endif; ?>						
		</p>				
	</div>
</li>	
	
