<div id="assigner_content" style="display:none;">
	<table class="table_popbox">
		<tr>
			<td class="popbox_topLeft"></td>
			<td class="popbox_border"></td> 
			<td class="popbox_topRight"></td>
		</tr>
		<tr>
			<td class="popbox_border"></td>
			<td class="popbox_content">
				<h4 class="header">
					<strong id="reassign_category_title">Remove Proworld Test Group ?</strong>
				</h4>
				<div class="confirm">
					<strong>Assign To Category</strong> &nbsp;
					<select %id% = "_assign_category">
						<option %id%="category0" value="0">Select Category</option>
						<? while($iterator->hasNext()): ?>
							<? $category = $iterator->next() ?>
							<option %id%="category<?= $category->getCategoryID() ?>" value="<?= $category->getCategoryID() ?>"><?= $category->getName() ?></option>
						<? endwhile; ?>
					</select>
				</div>
				<div class="buttons_box" id="popup_buttons">
					<p>
						<input type="button" id="input_submit" name="btnConfirm" onclick="manager.reAssignCategory()" value="Assign" class="prompt_button"/>
						<input type="button" id="input_cancel" onclick="manager.hideCategoryAssigner()" value="Cancel" class="prompt_button"/>
					</p>
				</div>
			</td>
			<td class="popbox_border"></td>
		</tr>
		<tr>
			<td class="popbox_bottomLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_bottomRight"></td>
		</tr>
	</table>
</div>
	
