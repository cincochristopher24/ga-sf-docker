<?php
  /**
   * @(#) Class.SubGroupCategoriesViewHandler.php
   * 
   * A view handler of the subgroup categories.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - Feb. 02, 2009
   */
  
  class CategoriesWithSubGroupsViewHandler {
  	protected $parent = null;  // the ID of the parent group
  	protected $categories = array(); // the categories of the parent group
  	protected $message = ""; // the message that shows after deletion or update
  	protected $group_access_type = 1;
  	protected $isAdminLogged = false;
  	protected $categoriesCount = 0;
  	protected $keyword = "";
  	
  	/**
  	 * Constructs the view handler for subgroups.
  	 * 
  	 * @param SqlResourceIterator $iterator The subgroups to be shown.
  	 * @param integer $subgroups_count The number of subgroups to be used in paging.
  	 * @param integer $page The current page number to show.
  	 * @param integer $numRows The number of rows to show.
  	 */
  	function __construct(SqlResourceIterator $iterator){
  		$this->categories = $iterator;
  	}  	
  	
  	/**
  	 * Assign variables and creates the template for viewing the list of 
  	 * categories of the given parent group.
  	 * 
  	 * @return string
  	 */
  	function fetch(){
			$tpl1 = new GAHtmlTemplate();
			$tpl1->setTemplate("travellog/views/subgroups/tpl.IncSubGroupToCategoryAssigner.php");
			$tpl1->set("iterator", $this->categories);
			
			$tpl = new Template();
			$tpl->set_path("travellog/views/subgroups/");
			$tpl->set("prompt", $tpl1->fetch());
			$tpl->set("iterator", $this->categories);
			$tpl->set("message", $this->message);
			$tpl->set("parent", $this->parent);
			$tpl->set("access", $this->group_access_type);
			$tpl->set("isAdminLogged", $this->isAdminLogged);
			$tpl->set("keyword", $this->keyword);
			
			return $tpl->fetch("tpl.IncCategoriesWithSubGroups.php");
  	}
  	
  	/**
  	 * Renders the list of categories of the given parent group.
  	 * 
  	 * @return void
  	 */
  	function render(){
			echo $this->fetch();
  	}
  	
  	/**
  	 * Sets the number of categories of the parent group to be shown.
  	 * 
  	 * @param integer $count
  	 * @return void
  	 */
  	function setCategoriesCount($count){
  		$this->categoriesCount = $count;
  	}
  	
		/**
		 * Sets the flag whether the current logged user is admin or not.
		 * 
		 * @param boolean $isAdminLogged The flag whether the current logged user is admin or not.
		 * @return void
		 */
		function setIsAdminLogged($isAdminLogged){
			$this->isAdminLogged = $isAdminLogged;
		}
		
		/**
		 * Sets the keyword to filter the groups that shows.
		 * 
		 * @param boolean $keyword The keyword to filter the groups that shows.
		 * @return void
		 */
		function setKeyword($keyword){
			$this->keyword = $keyword;
		}
  	
  	/**
  	 * Sets the access type of categories' subgroups.
  	 * 
  	 * @param integer $access The access type of categories' subgroups.
  	 * @return void
  	 */
  	function setSubGroupAccess($access){
  		$this->group_access_type = $access;
  	}
  	
  	/**
  	 * Sets the confirmation message to be shown.
  	 * 
  	 * @param string $message
  	 * @return void
  	 */
  	function setMessage($message){
  		$this->message = $message;
  	}
  
  	/**
  	 * Sets the parent group to be used in templates.
  	 * 
  	 * @param AdminGroup $parent The parent group to be used in templates.
  	 * @return void
  	 */
  	function setParentGroup($parent){
  		$this->parent = $parent;
  	}
  }