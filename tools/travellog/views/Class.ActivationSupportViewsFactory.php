<?php
	/*
	 * Class.ActivationSupportViewsFactory.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
	 
	class ActivationSupportViewsFactory{
		
		static $instance = NULL;
		private $views = array();
		
		static function getInstance(){ 
			if( self::$instance == NULL ) self::$instance = new ActivationSupportViewsFactory; 
			return self::$instance;
		}
		
		function createView( $view = constants::VIEW_ACTIVATION_SUPPORT_FORM_PAGE){
			
			switch($view){
				
				case constants::VIEW_ACTIVATION_SUPPORT_CONFIRMATION_PAGE:
					if( !array_key_exists("ViewActivationSupportConfirmationPage", $this->views) ){
						require_once("travellog/views/Class.ViewActivationSupportConfirmationPage.php");
						$this->views["ViewActivationSupportConfirmationPage"] = new ViewActivationSupportConfirmationPage;
					}
					return $this->views["ViewActivationSupportConfirmationPage"];
				
				default:
					if( !array_key_exists("ViewActivationSupportFormPage", $this->views) ){
						require_once("travellog/views/Class.ViewActivationSupportFormPage.php");
						$this->views["ViewActivationSupportFormPage"] = new ViewActivationSupportFormPage;
					}
					return $this->views["ViewActivationSupportFormPage"];		
			}
		}
		
	}  
?>
