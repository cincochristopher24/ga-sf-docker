<?php echo $profileComponent->render() ?>
<?php $subnavigation->show() ?>

<div id="content_wrapper" class="layout_2">
	<div id="message_center" class="section">
		<h2><span>Compose Message</span></h2>
		<?	if( !isset($_GET["gID"]) ): ?>

			<ul id="nav">
				<li><a href="<?php echo $inboxLink ?>"><span>Inbox</span></a></li>
				<li><a href="<?php echo $outboxLink ?>"><span>Outbox</span></a></li>
			</ul>
		<?	endif; ?>
		<div class="section" id="section_messages">
		
		
		<div class="new_message content">
			<?php if (trim($helpText) != '') : ?>
				<p class="help_text">
					<?php echo $helpText ?>
				</p>
			<?php endif; ?>
			<?php if (count($errors) > 0) : ?>
				<ul class="error_notice">
				<?php foreach ($errors as $error) : ?>
					<li><?php echo $error ?></li>
				<?php endforeach; ?>
				</ul>
			<?php endif; ?>

			<form name="frmComposeMessage" action=<?php echo $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING'] ?> method="post" onsubmit="document.getElementById('btnSend').disabled=true; return true;">
				<table class="message" width="100%" cellspacing="0">
					<tbody class="message_info">
						<tr>
							<th scope="row">To:</th>
							<td>
								<span class="message_input_container">
									<?php if ($recipientsListLocked) : ?>
										<?php echo $recipientDisplayName ?>
										<?php echo ' '.$recipientType ?>
									<?php else : ?>
										<input type="text" class="text" name="txtDestination" id="txtDestination" value="<?php echo $recipientDisplayName ?>" />
									<?php endif; ?>
								</span>
								<?php if ($autoSuggestSetting == 'on') : ?>
								<script type="text/javascript">
									var obj = actb(document.getElementById('txtDestination'),[<?php echo join(',', $suggestions) ?>]);
								</script>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<th scope="row">Subject:</th>
							<td>
								<span class="message_input_container">
									<?php
										if( 150 < strlen($subject) ){
											$subject = substr($subject,0,150);
										}
									?>
									<input type="text" class="text" name="txtSubject" id="txtSubject" value="<?php echo $subject ?>" onkeyup="if(150>=this.value.length){document.getElementById('characters_left').innerHTML=150-this.value.length;}else{this.value=this.value.substring(0,150);}"/>
									<span id="characters_left"><?=150-strlen($subject)?></span> characters left
								</span>
							</td>
						</tr>
						<tr>
							<th class="message" scope="row" valign="top">Message:</th>
							<td>
								<textarea name="txaMsgText" id="txaMsgText"  cols="70" rows="10"><?php echo $body ?></textarea>
							</td>
						</tr>
						
						<tr class="actions">
							<td>&nbsp;</td>
							<td>
							<a class="read_first" href="message-rules.php" onclick="window.open('message-rules.php', 'messageRules', 'resizable=no,width=490,height=520,top=' + ((screen.width/2)- 1000) + ',left=20'); return false;">Please read the <?php echo $siteDisplayName ?> Rules of Conduct before sending your message.</a>
								<br/>
								<input type='checkbox' name='chkSendNotification' id='chkSendNotification' <?php echo $notifyByEmail ?> value="1"/>
								<label for='chkSendNotification'>Include your message in the notification?</label>
								<p class="supplement">
									We send an email notification to the recipient to inform them that you've sent a message.<br /> Checking the box will include your message in that notification.
								</p>
								<input type="submit" name="btnSend" id="btnSend" value="Send" class="submit highlight" />
								</td>
						</tr>
					</tbody>
				</table>	
			</form>
			</div>			
		</div>

</div>