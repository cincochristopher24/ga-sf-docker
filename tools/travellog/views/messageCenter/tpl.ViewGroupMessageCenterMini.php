<?php require_once 'travellog/helper/gaTextHelper.php' ?>
<?php require_once 'travellog/helper/messageCenter/messageCenterHelper.php' ?>
<div id="messageNotification"></div>
<div id="main_content">
	<div id="inboxContainer">
		<table id="inbox" class="message_table">
			<tbody>
				<?php if (count($messages) > 0) : ?>
					<?php foreach ($messages as $message) : ?>
						<?php
							includeView('singleMessage', array(
															'message'        => $message,
															'snippetLimit'   => $snippetLimit,
															'canBeSelected'  => $canBeSelected,
															'canBeDeleted'   => $canBeDeleted,
															'showRealName'   => $showRealName,
															'showReadStatus' => $showReadStatus,
														))
						?>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="gline">
						<div class="gunit image">
							<img src="/images/g_news.jpg" alt="News" width="167" height="143" />
						</div>
						<div class="glastUnit description">
							<h3>Post announcements and reminders.</h3>
							<p>
								Publish important News and Alerts here with options for private viewing by members only or announcing to the public.	
							</p>
							<a class="button_v3 goback_button jLeft" href="/news.php?gID=<?=$group->getID()?>"><strong>Post News</strong></a>
						</div>
					</div>
				<?/*	<div class="help_text" style="text-align:center">
						<?	if( $thisIsAClub ): ?>
							<span>You don't have messages yet<span>. <a href="/messages.php?act=messageMembers&amp;gID=<?=$group->getID()?>" title="Compose a message">Send a message</a> to your friends.
						<?	else: ?>
							<?	if( $group->isParent() ): ?>
								<span>You don't have messages yet<span>. <a href="/messages.php?act=messageStaffOfManyGroups&amp;gID=<?=$group->getID()?>" title="Compose a message">Send a message</a> to your groups.
							<?	else: ?>
								<span>You don't have messages yet<span>. <a href="/messages.php?act=messageGroupStaff&amp;gID=<?=$group->getID()?>" title="Compose a message">Send a message</a> to your subgroup.
							<?	endif; ?>
						<?	endif; ?>
						<?/*<span>You don't have messages yet<span>. <a href="/messages.php?compose" title="Compose a message">Send a message</a> <?if($thisIsAClub):?>to your friends<?else:?>to your groups<?endif;?>.?>
					</div>  */?>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	var isPowerful = <?php echo $isPowerful ? 1 : 0 ?>;
	var groupID = <?php echo isset($_GET['gID']) ? $_GET['gID'] : 0 ?>;
</script>