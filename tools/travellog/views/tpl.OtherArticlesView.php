<?
$authors_articles = $contents['authors_articles'];
$group_articles = $contents['group_articles'];
$related_articles = $contents['related_articles'];
$limit = $contents['limit'];
?>
<?if(count($authors_articles) || count($group_articles) || count($related_articles)):?>

		<?if(count($authors_articles)):?>
		<div class="articleviewer">
			<div class="articleBox">
			<h1>Other Articles From This Author</h1>
			<ul>
				<?$cnt=1;?>
				<?foreach($authors_articles as $authors_article):?>
					<? $gID = ($authors_article->getGroupID() != 0) ? "&gID=".$authors_article->getGroupID() : ""; ?>
					<?if($cnt == ($limit+1)):?>
						</ul><ul id="more_author" style="display:none;">
					<?endif;?>				
					<li><a href="/article.php?action=view&articleID=<?=$authors_article->getArticleID()?><?=$gID?>"><?=$authors_article->getTitle()?></a></li>
					<?$cnt++;?>
				<?endforeach;?>		
			</ul>
			</div>
			<div class="journalsmallcontrol">	
				<?if(count($authors_articles) > $limit):?>	
					<a href="javascript:void(0)" id="expand_author" class="expand rightside">view more +</a>
				<?endif;?>
			</div>
		</div>	
		<?endif;?>
	
		<?if(count($group_articles)):?>
		<div class="articleviewer">
			<div class="articleBox">		
			<h1>Other Articles From This Group</h1>
			<ul>
				<?$cnt=1;?>
				<?foreach($group_articles as $group_article):?>
					<? $gID = ($group_article->getGroupID() != 0) ? "&gID=".$group_article->getGroupID() : ""; ?>
					<?if($cnt == ($limit+1)):?>
						</ul><ul id="more_group" style="display:none;">
					<?endif;?>
					<li><a href="/article.php?action=view&articleID=<?=$group_article->getArticleID()?><?=$gID?>"><?=$group_article->getTitle()?></a></li>
					<?$cnt++;?>
				<?endforeach;?>
			</ul>	
			</div>			
			<div class="journalsmallcontrol">	
				<?if(count($group_articles) > $limit):?>	
					<a href="javascript:void(0)" id="expand_group" class="expand rightside">view more +</a>
				<?endif;?>
			</div>
		</div>
		<?endif;?>

		<?if(count($related_articles)):?>
		<div class="articleviewer">	
			<div class="articleBox">			
			<h1>Related Articles</h1>
			<ul>
				<?$cnt=1;?>
				<?foreach($related_articles as $related_article):?>
					<? $gID = ($related_article->getGroupID() != 0) ? "&gID=".$related_article->getGroupID() : ""; ?>
					<?if($cnt == ($limit+1)):?>
						</ul><ul id="more_related" style="display:none;">
					<?endif;?>
					<li><a href="/article.php?action=view&articleID=<?=$related_article->getArticleID()?><?=$gID?>"><?=$related_article->getTitle()?></a></li>
					<?$cnt++;?>
				<?endforeach;?>
			</ul>
			</div>			
			<div class="journalsmallcontrol">	
				<?if(count($related_articles) > $limit):?>	
					<a href="javascript:void(0)" id="expand_related" class="expand rightside">view more +</a>
				<?endif;?>
			</div>
		</div>	
		<?endif;?>	
<?endif;?>	