<?php
/*
 * Purpose: template used instead of tpl.JournalsComp.php when ajax call is used in changing tabs/pages 
 * 				for JournalsComp. This is also included by tpl.JournalsComp.php for regular loading 
 */
?>

<?
	$country = '';
	$city = '';
	$jeCtr = 1;					// journal entry counter used for identifying yahoo carousel object
	$carJsCode = '';			// yahoo carousel code
?>
	
<ul class="journals">
	<?foreach($journals as $journal):
		$jID = $journal->getTravelID();
		$jeCount = count($journalsAndEntries[$jID]);
		$jEntriesAr = $journalsAndEntries[$jID];
		
		if ($journal->getOwner() == null)
			echo 'travelid:' . $journal->getTravelID();
						
		$jah = new JournalAuthorUIHelper($journal->getOwner());
		
		if (isset($jEntriesAr[$jeCount-1])){
			$trip = $jEntriesAr[$jeCount-1]->getTrip();
			try{
				$location = $trip->getLocation();
				if(is_object($location)){
					$country = $location->getCountry()->getName();
					$city = $location->getName();
				}
			}catch(Exception $e){
			}
		} 
		?>
		
		 <li id="container<?=$jID?>">     
							
			<h3>						
				<?if (isset($jEntriesAr[$jeCount-1])): ?>									
					<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" title="<?=$journal->getTitle()?>"><?=$journal->getTitle()?></a>
				<?else:?>
					<?=$journal->getTitle()?>
				<?endif;?>	
			</h3>
								
			<div class="content" id="content<?=$journal->getTravelID()?>">					 
    			   <?if (isset($journalPhotos[$jID])):?>
				 <div class="photo">
					<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>"> 
						<img src="<?=$journalPhotos[$jID]->getPhotoLink('standard')?>" alt="journal photo" />
					</a>							
				 </div>	
			   <?endif;?>
				
				<?if (isset($jEntriesAr[$jeCount-1])):?>								
					<h4>
						<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>">
							<?=$jEntriesAr[$jeCount-1]->getTitle()?>
						</a>
					</h4>
					<p class="meta">						
						<?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?> | <?=date('D M d, Y',strtotime($trip->getArrival()))?> | Views: <?=$journal->getViews()?>
					</p>
					<p class="entry_preview">
						<?if ($jeCount):?>							
							<?=(strlen(trim($jEntriesAr[$jeCount-1]->getDescription())) > 175 )? strip_tags(nl2br(substr($jEntriesAr[$jeCount-1]->getDescription(),0,175))) . '...' : strip_tags(nl2br($jEntriesAr[$jeCount-1]->getDescription()))?>
						<?endif;?>	
					</p>
				<?else:?>
					<div class="no_entry_detail">There are no entries for this journal. <br />
						Document those great travel moments and share them with family and friends! A journal book is a set of journal entries about the same trip. You may write about your experiences and add your travel photos to your entries. You may also do a photo journal.
					</div>
				<?endif;?>
				
				
				<ul class="actions" id="actions<?=$journal->getTravelID()?>">
					<li>
						<a href="/journal-entry.php?action=add&amp;travelID=<?=$jID?>" class="msg edit_entry_content" title="Add journal entry"><span id="add<?=$journal->getTravelID()?>">Add Entry</span></a><!--<span id="span_add<?=$journal->getTravelID()?>">&nbsp;|&nbsp;</span>-->
					</li>
						<!--<a href="javascript:void(0)" onclick="publishJournal(<?=$journal->getTravelID()?>)" class="msg edit_entry_content" title="Publish/Unpublish this journal"><span id="unpublish<?=$journal->getTravelID()?>">Unpublish</span></a>-->
					<li>
						<a href="/travel.php?action=edit&amp;travelID=<?=$jID?>" class="msg edit_entry_content" title="Edit this journal"><span id="edit_journal<?=$journal->getTravelID()?>">Edit Journal</span></a><!--<span id="span_edit_journal<?=$journal->getTravelID()?>">&nbsp;|&nbsp;</span>-->
					</li>
					<li>
						<a href="javascript:void(0)" onclick="deleteJournal(<?=$journal->getTravelID()?>)" class="msg delete_entry_title" title="Delete this journal"><span id="delete<?=$journal->getTravelID()?>">Delete</span></a><!--<span id="span_delete<?=$journal->getTravelID()?>">&nbsp;|&nbsp;</span>-->
					</li>
					<li>
						<a href="javascript:void(0)" onclick="approveJournal(<?=$journal->getTravelID()?>,<?=$subjectID?>)" class="msg edit_entry_content" title="Approve/Unapprove this journal to be viewed in group"><span id="approve<?=$journal->getTravelID()?>" >Approve</span></a><!--<span id="span_approve<?=$journal->getTravelID()?>">&nbsp;|&nbsp;</span>-->
					</li>
					<li>
						<a href="javascript:void(0)" onclick="featureJournal(<?=$journal->getTravelID()?>,<?=$subjectID?>)" class="msg edit_entry_content" title="Move this journal into the featured journals"><span id="feature<?=$journal->getTravelID()?>">Feature</span></a>
					</li>
					<li>
						<a href="javascript:void(0)" onclick="forwardToUnapproveJournal(<?=$journal->getTravelID()?>,<?=$subjectID?>)" class="msg edit_entry_content" title="Move this journal into group unapproved journals"><span id="forwardToUnapprove<?=$journal->getTravelID()?>">To Unapproved</span></a>
					</li>	
				</ul>	
				
					<div class="entry_group_tag">
						<?if (isset($relatedGroupsAr[$jID])):
							 $group = $relatedGroupsAr[$jID];
							 $site = $group->getServerName(); 	
						?>
							<?if($site != false):?>	
								<a href="http://<?=$group->getServerName()?>" title="<?= $group->getName()?>">	
									<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
								</a>
								<a href="http://<?=$group->getServerName()?>" alt="" class="author_username">
								<?=$group->getName()?>
								</a>
								<br/>
								<span>Advisor Group Journal</span>			

							<?else:?>
								<a href="/group.php?gID=<?=$group->getGroupID()?>" class="<?= $group->getName()?>">
									<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/>
								</a>
								<a href="/group.php?gID=<?=$group->getGroupID()?>" class="author_username">
								<?=$group->getName()?>	
								</a>
								<br/>
								<span>Advisor Group Journal</span>	

							<?endif;?>															
						<?endif;?>
					</div>	
				
				<?if ($showAuthor):?>			 			
					<div class="author_info">				    
						<a href="<?=$jah->getString('OWNER_URL')?>" title="Read more of <?=$jah->getString('OWNER_URL')?> travel experiences and adventure!"><img src="<?=$jah->getString('PHOTO_THUMBNAIL_URL')?>" width="37px" height="37px"  /></a>  
						<p class="author_username"><a href="<?=$jah->getString('OWNER_URL')?>" title="Read more of <?=$jah->getString('OWNER_URL')?> travel experiences and adventure!"><?=HtmlHelpers::truncateWord($jah->getString('OWNER'), 13)?></a></p>						    				       
					</div>
				<?endif;?>						
			</div>
					
			<div class="entry_box">					
					
			</div>
		</li>
				
		<?if (isset($journalControlsJsCode[$jID])) echo $journalControlsJsCode[$jID]?>		
		
		
	<?endforeach;?>
	</ul>
	<!-- echo javascript code for journal controls -->
	<?foreach($journalControlsJsCode as $travelID => $jsCode){
		echo $jsCode; 
	  }?>
	
	
	<? if (isset($pagingComp)):?>	
	    	<? $pagingComp->showPagination() ?>
	<? endif; ?>
	
	<script type="text/javascript">
	var pageLoad = function() 
		{			
			<?php //echo $carJsCode;?>  
		};
	</script>
