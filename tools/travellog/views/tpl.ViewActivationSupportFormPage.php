<?php
	/*
	 * tpl.ViewActivationSupportFormPage.php
	 * Created on Nov 26, 2007
	 * created by marc
	 */
?>

<div id="content_wrapper">
		<div id="page_form" class="section">		
					<h2 class="header_activation">Account Activation Support</h2>	
					
						<div class="help_text">
							<p class="disc_par_one">
								You have successfully created an account, but it has not yet been activated.
							</p>
							<p>
								We are sorry for the inconvenience. If you did not receive the email with
								the activation link, it is likely that:
							</p>
							<ul>
								<li>The email ended up in your bulk or spam folder. Please check.</li>
								<li>You may have mistyped your email address, thus the email could not be sent</li>
							</ul>


							<p class="disc_par_two">
								<strong>If the email is not in your bulk folder</strong>, please provide us with your email address using 
								the form on the right, and click on the button <strong>"Send me the activation email"</strong>. 
							</p>
							<p class="disc_par_two">
								<strong>If you got the email but the links won't work</strong>, please fill out the form on the right and
								 click on the button <strong>"Help me activate my account"</strong>. We will get back to you as soon as we can.
							</p>	 
						</div>			
					
					
					
					
					
					
					
					
					
								
					<form class="interactive_form" action="activation-support.php" method="post">
					    <? if (count($contents["arrError"])): ?>
					        <div class="error_notice">
					            <p>There are problems with your submission. Please correct them before you continue.</p>
					        </div>
					    <? endif; ?>
						<ul class="form">	
							<li>
								<label for="txtEmail">Email: <span class="required">*</span></label>
								<? if( in_array(constants::EMPTY_EMAIL_ADDRESS,$contents["arrError"]) ): ?>
									<p class="error"><strong>Please provide your email address</strong></p>
								<? elseif( in_array(constants::INVALID_EMAIL_FORMAT,$contents["arrError"]) ): ?>
									<p class="error"><strong>The email you provided is invalid</strong></p>
								<? elseif( in_array(constants::TRAVELER_DOES_NOT_EXIST,$contents["arrError"]) ): ?>
									<p class="error"><strong>You may have mistyped your email.</strong></p>	
								<? endif; ?>
								<input class="text ga_interactive_form_field" type="text" name="txtEmail" style="width:300px" id="txtEmail" value="<?= $contents["tmpEmail"]; ?>" />
							</li>
							<li>	
								<label for="txaMessage">Message:</label>
								<? if( in_array(constants::EMPTY_MESSAGE,$contents["arrError"]) ): ?>
									<p class="error"><strong>Please provide us information on how we could be of help.</em></p>
								<? endif; ?>
								<p class="supplement">Note: If you got your activation email and the links won't work, please copy and paste the email as part of your message below. This will help us get a better understanding of the issue. Thank you for your patience.</p>
								<textarea class="text ga_interactive_form_field" name="txaMessage" id="txaMessage"><?= $contents["tmpMessage"]; ?></textarea>
							</li>
							<li class="captcha">
								<label for="securityCode"><strong>Security Code:</strong></label>
								<? if(in_array(constants::SECURITY_CODE_MISMATCH,$contents["arrError"])): ?>
									<p class="error"><strong>Security code mismatch.</strong></p>
								<? endif; ?>
								<img  id="captchaImg" src="<?= $contents["src"]; ?>" alt="Captcha" class="captcha_image"  />
								<p>
								<a href="javascript:void(0)" onclick="Captcha.load()" style="color:red; font-weight:normal">[Refresh Security Code Image]</a>
								
								<a href="http://www.goabroad.com/captchainfo.cfm" title="Why do you need to enter this code?" onclick="window.open('captcha-info.php?location=activation-support', 'captchainfo', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable,width=500,height=235, top=' + ((screen.width/2)- 1000) + ', left=20'); return false;">
								<img src="http://images.goabroad.com/images/icons/help.gif" border="0" alt="help" /> What is this?</a>			
								<input type="hidden" id="encSecCode" name="encSecCode" value="<?= $contents["code"]; ?>" />
								</p>
								<p class="supplement">
								Please type the code that you see in the image above:<span class="required">*</span></p>
								<input type="text" id="securityCode" name="securityCode" style="width:300px;margin-top:5px;" value="" class="text" />
							</li>	
							<li class="actions">
								<input class="submit button ga_interactive_form_field" type="submit" value="Send me the activation mail" name="btnActivate" id="btnActivate" />&nbsp;<span>or</span>&nbsp;<input class="submit button ga_interactive_form_field" type="submit" value="Help me activate my account" name="btnHelp" id="btnHelp" />
							</li>
						</ul>
					</form>
					</div>

	
</div>