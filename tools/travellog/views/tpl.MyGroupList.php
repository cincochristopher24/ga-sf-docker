<?php
	// Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	//Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('page_location', $page_location);
?>

		
<div id="intro_container">
  <div id="intro">
		<?= $profile->render()?>
	</div>
</div>

<? $subNavigation->show(); ?>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
		<div id="groups_and_clubs" class="section">			
			<h2> <span><?= $maintitle ?></span> </h2>				
			<div id="my_groups" class="content">
				<div id="groups_list_main">
				  <? // tpl..php ?>
				  <?= $group_list_tpl ?>
				</div>
			</div>
		</div>		
	</div>
	<?if( isset($showBox) && $showBox ): ?>
	<div id="narrow_column">
		<div class="section">
			<div class="content">
			  <? // tpl.FrmSearchGrpName.php ?>
				<?= $search_template ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div>
<!--added by ianne - 11/20/2008 success message after confirm-->
<?if(isset($success)):?>
	<script type="text/javascript">
		window.onload = function(){
			CustomPopup.initPrompt("<?=$success?>");
			CustomPopup.createPopup();
		}
	</script>
<?endif;?>