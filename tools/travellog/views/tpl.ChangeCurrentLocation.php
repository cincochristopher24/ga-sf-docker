<?php
Template::setMainTemplate("travellog/views/tpl.LayoutPopup.php");
Template::setMainTemplateVar('layoutID', 'main_pages');
?>
<script type="text/javascript" >
	//<!--
	function countrySelect() {
		document.forms[0].action = '/setlocation.php?action=selectcountry';
		document.forms[0].submit();
	}
	
	function clickSave() {
		document.forms[0].action = '/setlocation.php?action=save';
		document.forms[0].submit();
	}
	
	function clickCancel() {
		window.close();		
	}
	//-->
</script>
<div id="popup_content">
	<div class="section" >
		<h1 id="pop_header"><span>Change Current Location:</span></h1>
		<div class="content">
			<form method="post" >	
			  <select name="countryLocID" onchange="countrySelect()" >  	 
			     <?echo $fh->CreateOptions($cAr,$countryLocID)?>
			  </select>

			  <?if (isset($location)):?>	 
			  <select name="cityLocID" >	  
			   	<?echo $fh->CreateOptions($cityAr,$location->getLocationID())?>	  	
			  </select>
			  <?else:?>
			  <select name="cityLocID" >	  
			   	<?echo $fh->CreateOptions($cityAr)?>	  	
			  </select>	
			  <?endif;?>


			  <input type="hidden" name="travelerID" value="<?=$travelerID?>">
			  <br/><br/>
			  <input type="submit" value="Save" onclick="clickSave()" >
			  <input type="submit" value="Cancel" onclick="clickCancel()" >
			</form>
		</div>
		<div class="foot"></div>
	</div>
</div>