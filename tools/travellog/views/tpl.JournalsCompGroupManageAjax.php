<?php
/*
 * Purpose: template used instead of tpl.JournalsComp.php when ajax call is used in changing tabs/pages 
 * 				for JournalsComp. This is also included by tpl.JournalsComp.php for regular loading 
 */
?>

<?
	$country = '';
	$city = '';
	$jeCtr = 1;					// journal entry counter used for identifying yahoo carousel object
	$carJsCode = '';			// yahoo carousel code
	
	//temporary code:
	require_once('Class.dbHandler.php');
	require_once('travellog/model/Class.GroupFactory.php');
	$dbh = new dbHandler();
	$grpFactory =  GroupFactory::instance();
?>

<ul class="journals">
	<?foreach($journals as $journal):
		$jID = $journal->getTravelID();
		$jeCount = count($journalsAndEntries[$jID]);
		$jEntriesAr = $journalsAndEntries[$jID];
		$journalOwner = $journal->getOwner();
		if ($journalOwner == null){
			echo 'travelid:' . $journal->getTravelID();
			continue;
		}
						
		$jah = new JournalAuthorUIHelper($journalOwner);
		$locationID = 0;
		if (isset($jEntriesAr[$jeCount-1])){
			$trip = $jEntriesAr[$jeCount-1]->getTrip();
			$country = $trip->getLocation()->getCountry()->getName();
			$city = $trip->getLocation()->getName();
			$locationID = $trip->getLocation()->getCountry()->getCountryID();
		} 
		?>
		 		<li class="container ajax_journal" id="container<?=$jID?>">     
					<div class="journal_box_wrap">
				
						<div class="header">							
								<h3 class="group_h">						
									<?if (isset($jEntriesAr[$jeCount-1])): ?>									
										<a href="/journal-entry.php?action=view&travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" alt="<?=$journal->getTitle()?>"><?=HtmlHelpers::truncateWord($journal->getTitle(), 50)?></a>
									<?else:?>
										<?=HtmlHelpers::truncateWord($journal->getTitle(), 50)?>
									<?endif;?>	
								</h3>				 		    									
						</div>				
						<div class="content" id="content<?=$journal->getTravelID()?>">					      			   
							<?if (isset($journalPhotos[$jID])):?>
								 <div class="photo">
									<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>"> 
										<img class="float_right" src="<?=$journalPhotos[$jID]->getPhotoLink('featured')?>" alt="journal photo" />
									</a>							
								 </div>	
							   <?endif;?>
							<?if (isset($jEntriesAr[$jeCount-1])):?>								
								<div class="entry_title"><a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>">
								<?=HtmlHelpers::truncateWord($jEntriesAr[$jeCount-1]->getTitle(), 50)?>
								</a></div>										
								<div class="entry_detail">	
									<span class="location_info">
									<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" alt="Travel Journals from <?=$country?>" style="margin: 0 0 0 0;" />						
									<?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?> | <?=(strtotime($trip->getArrival())) ? date('D M d, Y',strtotime($trip->getArrival())).' | ' : ''?>Views: <?=$journal->getViews()?>
									</span>
									<?if ($jeCount):?>	
									<p>						
										<?=(strlen(trim($jEntriesAr[$jeCount-1]->getDescription())) > 175 )? strip_tags(nl2br(substr($jEntriesAr[$jeCount-1]->getDescription(),0,175))) . '...' : strip_tags(nl2br($jEntriesAr[$jeCount-1]->getDescription()))?>
									</p>
									<?endif;?>	
								</div>
								<?if('APPROVED'==$viewTab):?>
									<?php
									 	$iSql = 'SELECT tblGroup.name, tblGroup.groupID FROM tblGroupApprovedJournals, tblGroup '.
												'WHERE tblGroup.groupID = tblGroupApprovedJournals.groupID '.
												'AND tblGroupApprovedJournals.travelID = '.$dbh->makeSqlSafeString($journal->getTravelID()).' '.
												"AND (tblGroup.groupID = {$dbh->makeSqlSafeString($subjectID)} OR tblGroup.parentID = {$dbh->makeSqlSafeString($subjectID)})";
										$rsGroupRelatedToJournal = new iRecordset($dbh->execute($iSql)); 
										$arGroupLinks = array();
										foreach($rsGroupRelatedToJournal as $row){
											$relatedGroup = $grpFactory->create(array($row['groupID']));
											$arGroupLinks[] = "<a href='{$relatedGroup[0]->getFriendlyURL()}'>{$row['name']}</a>";
										}
									?>
									<?= implode(', ',array_slice($arGroupLinks,0,3)) ?>
									<?php if(count($arGroupLinks) > 3): ?>
										<?php
											$content = addslashes(implode('<br /> ',$arGroupLinks));
											//echo $content;exit;
											$jsf = 	'(function(){'.
														'CustomPopup.initPrompt(\''.$content.'\',\'Related Groups\');'.
														'CustomPopup.setHTML(true);'.
														'CustomPopup.setJS(true);'.
														'CustomPopup.setActionButton(\'Ok\'); '.
														'CustomPopup.createPopup();'.
													'})();';
											echo " ...<a href='javascript:void(0)' onclick=\"$jsf\">[View All]</a>";
										?>
									<?endif;?>
								<?endif;?>
							<?else:?>
								<div class="no_entry_detail">There are no entries for this journal. <br />
										Document those great travel moments and share them with family and friends! A journal book is a set of journal entries about the same trip. You may write about your experiences and add your travel photos to your entries. You may also do a photo journal.
								</div>
							<?endif;?>
					
							<?if ($showAuthor):?>
							
								<? $owner = $journal->getTraveler(); ?>
								<? if( !$owner->isAdvisor() && !isset($relatedGroupsAr[$jID]) ): ?>
									<p class="entry_group_tag">
										<span>
											<a class="username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$owner->getUserName()?>">
												<img class="pic" width="37" height="37" src="<?=$owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" title="Read more of <?=$owner->getUserName()?> travel experiences and adventure!" />
											</a>
										</span>					
											<a class="author_username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$owner->getUserName()?>">
												<strong><?=$owner->getUserName()?></strong>
											</a>
									</p>
								<? elseif(!$owner->isAdvisor()): ?>
									<p class="meta">
										traveler: 					
											<a class="author_username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$owner->getUserName()?>"><strong><?=$owner->getUserName()?></strong></a>
											<?if($canAddJournal):?>
												<strong class="meta"><em> <?=$owner->getFullName()?> </em></strong>
											<?endif;?>
									</p>
								<? endif; ?>
										 			
								<?/*<div class="author_info">				    
							    	<!--<a href="<?=$jah->getString('OWNER_URL')?>" title="Read more of <?=$jah->getString('OWNER_URL')?> travel experiences and adventure!"><img src="<?=$jah->getString('PHOTO_THUMBNAIL_URL')?>" width="37px" height="37px"  /></a> --> 
									<p class="author_username">author: <a href="<?=$jah->getString('OWNER_URL')?>" title="Read more of <?=$jah->getString('OWNER')?> travel experiences and adventure!"><?=HtmlHelpers::truncateWord($jah->getString('OWNER'), 13)?></a></p>																											
								</div>*/?>
							<?endif;?>														
					
							<div class="entry_group_tag">
									<?if (isset($relatedGroupsAr[$jID][$subjectID])):
										 $group = $relatedGroupsAr[$jID][$subjectID];
										 $site = $group->getServerName(); 	
									?>
										<?if($site != false):?>						
											<span>
											<a href="http://<?=$group->getServerName()?>" alt="<?= $group->getName()?>">	
											<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/></a>
											</span>	
											<a href="http://<?=$group->getServerName()?>" alt="" class="author_username">
											<?=HtmlHelpers::truncateWord($group->getName(), 13)?>
											</a>
											<br/>
											<span>Advisor Group Journal</span>			

										<?else:?>
											<span>
											<a href="/group.php?gID=<?=$group->getGroupID()?>" class="<?= $group->getName()?>">
											<img src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37"/></a>
											</span>					
											<a href="/group.php?gID=<?=$group->getGroupID()?>" class="author_username">
											<?=HtmlHelpers::truncateWord($group->getName(), 13)?>	
											</a>
											<br/>
											<span>Advisor Group Journal</span>	

										<?endif;?>															
									<?endif;?>
								</div>	
						</div>
						<div class="actions" id="actions<?=$journal->getTravelID()?>">
							<? $group = new AdminGroup($subjectID); ?>
							<div>
								<?if( 'STAFF_ADMIN' == $viewTab ):?>
									<?if($journal->getTraveler()->getTravelerID() == $_SESSION['travelerID']):?>
										<a href="/journal-entry.php?action=add&amp;travelID=<?=$jID?>" class="edit_entry_content" title="Add journal entry"><span id="add<?=$journal->getTravelID()?>">Add Entry</span></a><!--span id="span_add<?=$journal->getTravelID()?>">&nbsp;|&nbsp;</span-->
										<?if (isset($jEntriesAr[$jeCount-1])): ?>									
											<a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>">Edit Journal</a>
										<?else:?>
											<a href="javascript:void(0)" onclick="CustomPopup.initialize('Delete Journal ?','Are you sure you want to delete this journal?','/travel.php?action=delete&amp;travelID=<?=$jID ?>','Delete','1','All entries, photos and other traveler comments linked to this journal will also be deleted. This action cannot be undone.');CustomPopup.createPopup();" >Delete</a>
										<?endif;?>
									<?endif;?>	
								<?endif;?>
								<!--<a href="/journal-entry.php?action=add&amp;travelID=<?=$jID?>" class="edit_entry_content" title="Add journal entry"><span id="add<?=$journal->getTravelID()?>">Add Entry</span></a>--><!--<span id="span_add<?=$journal->getTravelID()?>">&nbsp;|&nbsp;</span>-->
								<!--<a href="javascript:void(0)" onclick="publishJournal(<?=$journal->getTravelID()?>)" class="msg edit_entry_content" title="Publish/Unpublish this journal"><span id="unpublish<?=$journal->getTravelID()?>">Unpublish</span></a>-->
								<!--<a href="/travel.php?action=edit&amp;travelID=<?=$jID?>" title="Edit this journal"><span id="edit_journal<?=$journal->getTravelID()?>">Edit Journal</span></a>-->
								<!--<a href="javascript:void(0)" onclick="deleteJournal(<?=$journal->getTravelID()?>)" class="msg" title="Delete this journal"><span id="delete<?=$journal->getTravelID()?>">Delete | </span></a>--><!--<span id="span_delete<?=$journal->getTravelID()?>">&nbsp;|&nbsp;</span>-->
								<a href="javascript:void(0)" onclick="approveJournal(<?=$journal->getTravelID()?>,<?=$subjectID?>)" title="Approve/Unapprove this journal to be viewed in group"><span id="approve<?=$journal->getTravelID()?>">Approve | </span></a><!--<span id="span_approve<?=$journal->getTravelID()?>">&nbsp;|&nbsp;</span>-->
								<a href="javascript:void(0)" onclick="featureJournal(<?=$journal->getTravelID()?>,<?=$subjectID?>)" title="Move this journal into the featured journals"><span id="feature<?=$journal->getTravelID()?>">Feature | </span></a>
								<a href="javascript:void(0)" onclick="forwardToUnapproveJournal(<?=$journal->getTravelID()?>,<?=$subjectID?>)" class="edit_entry_content" title="Move this journal into group unapproved journals"><span id="forwardToUnapprove<?=$journal->getTravelID()?>">To Unapproved</span></a>
								<?php
									// quick fix only
									$arr = JournalsComp::convertURLParamToSetupArray();
									if( !isset($arr['VIEW_TAB']) ){
										$arr['VIEW_TAB'] = 'NEW';
									}
									if( $arr['VIEW_TAB'] != 'NEW' && $arr['VIEW_TAB'] != 'UNAPPROVED' ):
										require_once('Class.dbHandler.php');
										require_once('travellog/model/Class.AdminGroup.php');
									 	require_once('travellog/model/Class.Travel.php');
										$db = new dbHandler;
									 	$group = new AdminGroup($subjectID);
									 	$travel = $journal;
										$owner = $travel->getOwner();
										if( $owner instanceof Group ){
											$owner = $owner->getAdministrator();
										}
										$sql = ' select distinct gID,gname from ( SELECT tblGroup.groupID AS gID, tblGroup.name AS gname ' .
							 												' FROM tblGroup, tblGrouptoTraveler ' .
																			' WHERE tblGroup.groupID = tblGrouptoTraveler.groupID ' .
																			' AND tblGroup.parentID = ' . $group->getGroupID() .
																			' AND tblGrouptoTraveler.travelerID=' . $owner->getTravelerID() . ') as g ' .
							 			       ' left join tblGroupApprovedJournals as gaj on (gID = gaj.groupID and gaj.travelID=' . $travel->getTravelID() . ' ) where gaj.groupID is null or gaj.travelID is null' .
							 			       ' UNION ' .
							 			       ' select g.groupID,name from tblGroup as g,tblGrouptoTraveler as gtt,tblGroupApprovedJournals as gaj ' .
											   ' where g.parentID = ' . $group->getGroupID() . ' and g.groupID = gtt.groupID and gtt.travelerID=' . $owner->getTravelerID() . 
											   ' and g.groupID = gaj.groupID and gaj.approved=0';
										$rs = new iRecordset($db->execute($sql));
								?>
									<?php if($rs->retrieveRecordcount()):?>
											<a href="javascript:void(0)" onclick="relateJournal(<?=$journal->getTravelID()?>,<?=$subjectID?>)">Relate</a>
									<?php endif;?>		
								<?php
									endif;
								?>
								<input type="hidden" value="<?=$group->getParentID()?>" id="hidParentID"/>		
							</div>	
						</div>
					</div>
					<?if (isset($journalControlsJsCode[$jID])) echo $journalControlsJsCode[$jID]?>
				</li>
			<?endforeach;?>
	</ul>
			<!-- echo javascript code for journal controls -->
			<?foreach($journalControlsJsCode as $travelID => $jsCode){
				echo $jsCode; 
			  }?>
	
	
			<? if (isset($pagingComp)):?>	
			    	<? $pagingComp->showPagination() ?>
			<? endif; ?>
