<? $showListView = (preg_match("/proworld/",$_SERVER['SERVER_NAME'])) ? true : false; ?>

<? if ($customGroupSection->getCustomGroupSectionID()): ?>
	<div class="layout_2" id="content_wrapper">
		<div id="wide_column">
			<? if ($isAdminLogged): ?>
				<div id="groups_and_clubs" class="section">
					<h2>
					  <span>Featured Group</span>
					</h2>

					<div id="statusPanel1" class="widePanel content" style="display: none;margin-top: 10px">
						<p class="loading_message">
							<span id="imgLoading">
								<img alt="Loading" src="/images/loading.gif"/>
							</span>
							<em id="statusCaption">Loading data please wait...</em>
						</p>
					</div>

					<div class="content" id="featuredGroupsContent">
						<?=	$featSubGroupsPanel ?>
					</div>
				</div>			
			<? endif; ?>
			
			
			<div id="groups_and_clubs" class="section">	
				<h2>
					<span>
					  <?=$customGroupSection->getSectionLabel()?>
					</span>
					
					<span class="header_edit_actions">
					  <? if ($isAdminLogged): ?>
						  <a href="customgroupsection.php?gID=<?=$grpID?>&amp;mode=edit" >
						    Edit
						  </a>
						<? endif; ?>
					</span>	
					
					<span class="header_actions">						
					  <a href="javascript:void(0)" <?= ("showActive" === $tabAction) ? "style='color:gray;'" : "" ?> onclick="manager._viewActive('grpID=<?=$grpID?>&amp;mode=<?= $mode?>&amp;action=showActive&amp;isAdminLogged=<?=$isAdminLogged?>&amp;searchKey=<?=$searchKey?>');" id="viewActive">
					    Active (<?=$active_cnt?>)
					  </a> 
					  |
					  <a href="javascript:void(0)" <?= ("showInActive" === $tabAction) ? "style='color:gray;'" : "" ?> onclick="manager._viewInActive('grpID=<?=$grpID?>&amp;mode=<?= $mode?>&amp;action=showInActive&amp;isAdminLogged=<?=$isAdminLogged?>&amp;searchKey=<?=$searchKey?>');" id="viewInActive">
					    Inactive (<?=$inactive_cnt?>)
					  </a>
					</span>
				</h2>
				
				<div id="statusPanel2" class="widePanel content" style="display: none;margin-top:10px;">
					<p class="loading_message">
						<span id="imgLoading">
							<img alt="Loading" src="/images/loading.gif"/>
						</span>
						<em id="statusCaption">Loading data please wait...</em>
					</p>
				</div>
				
				<div class="content" id="subGroupsContent">
					<?=	$subGroupsPanel->render() ?>				
				</div>
			</div>
		</div>
		
		
    <div id="narrow_column">			
			<? if ($isAdminLogged): ?>
			  <? if (0 < count($allSubGroups)): ?>
			    <div class="section" id="group_project_side">
				    <form action="" method="POST">
				      <h2>
				        <span>Assign Groups</span>
				      </h2>
						
					    <div class="content">
						    <div class="group_help">
						   	  <p>
							      Add groups to <?=$customGroupSection->getSectionLabel()?>
							    </p>
						    </div>
						
						    <ul class="project_assign">
							    <? foreach($allSubGroups as $each) :?>
								    <li>
								      <input type="checkbox" name="chkSubGroupID[]" value="<?=$each->getGroupID()?>">
								      <input type="hidden" name="chkSubGroupAccess[<?=$each->getGroupID()?>]" value="<?=$each->getGroupAccess()?>">
								      <span><?=$each->getTrimmedGroupName(25);?></span>
								    </li>
							    <? endforeach; ?>
						    </ul>
					    </div>
						  <div>
						    <input type="submit" name="Add" value="Add">
						  </div>
						  <input type="hidden" name="action" value="addtosection">
				    </form>
			    </div>
			  <? endif; ?>
			<? endif; ?>
			
			<? if ("arrange" != $mode AND (1 < ($active_cnt/$rows_per_page) OR 1 < ($inactive_cnt/$rows_per_page) OR $is_searching)): ?>
        <div id="quick_tasks2" class="section">
					<h2><span>Search</span></h2>
					
					<div class="content">
						<ul class="actions">
							<form id="searchGroupForm" method="post" action="customgroupsection.php">
								<span>
									<input type="text" value="<?=$searchKey?>" name="txtGrpName" id="txtGrpName" class="text"/>
									<input type="hidden" value="<?=$mode?>" name="mode"/>
									<input type="hidden" value="<?=$grpID?>" name="gID"/>
									<input type="hidden" value="<?=$tabAction?>" name="tabAction" id="tabAction1"/>					
								</span>
								<span>		
									<input type="submit" id="btnSearchName" name="btnSearchName" value="Search group" title="Search" alt="search button"/>
								</span>
							</form>
						</ul>
					</div>				
				</div>
			<? endif; ?>
		  
		  <? if ($isAdminLogged): ?>
				<? if (1 < $active_cnt OR 1 < $inactive_cnt OR $is_searching): ?>
			    <div class="section" id="quick_tasks3">
				    <label>Group Sort Order</label>				  
				    <div class="content">
					  
						  <? $ranksettings = $customGroupSection->getRankSettings()?>
						  <form name="sortGroup" action="#" method="POST" >							
							  <ul id="sort_but" class="actions">
							    <li><input type="radio" name="radSort" value="2" <?=(2 == $ranksettings) ? 'checked' : '' ?> />Alphabetical</li>
							    <li><input type="radio" name="radSort" value="3" <?=(3 == $ranksettings) ? 'checked' : '' ?> />By Most Recent Created</li>
							    <li><input type="radio" name="radSort" value="1" <?=(1 == $ranksettings) ? 'checked' : '' ?> />Custom</li>
							    <li><input type="submit" name="btnSubmit" value="Save" /></li>
						  	</ul>
						  	
						  	<input type="hidden" name="action" value="sortgroups" />
							  <input type="hidden" name="mode" value="<?=$mode?>" />
							  <input type="hidden" name="gID" value="<?=$grpID?>" />
							  <input type="hidden" name="tabAction" id="tabAction2" value="<?=$tabAction?>" />
						  </form>
				    </div>
			    </div>
			  <? endif; ?>
			  
			  
			  <div class="section" id="quick_tasks2">
				  <h2>
				   <span>Group Tasks</span>
				  </h2>
				  
				  <div class="content">
					  <ul class="actions">
						  <li>
							  <a href="group.php?mode=add&amp;pID=<?=$grpID?>&amp;addtoPNP=1 " class="button" >
							    Add Group
							  </a>
						  </li>
						  <li>
						    <? if (1 < $active_cnt OR 1 < $inactive_cnt OR $is_searching): // show arrange groups when there are more than one subgroups in custom group ?>
							    <? if ('arrange' == $mode): ?>
								    <a href="customgroupsection.php?gID=<?=$grpID?> " class="button" >
								      View Groups
								    </a>
							    <? else: ?>
								    <a href="customgroupsection.php?gID=<?=$grpID?>&amp;mode=arrange " class="button" >
								      Arrange Groups
								    </a>
							    <? endif; ?>
							  <? endif; ?>
						  </li>
						  
							<li>
								<a href="customgroupsection.php?gID=<?=$grpID?>&amp;mode=list" class="button" >List View</a>
							</li>					  
						  						
					  </ul>
				  </div>
			  </div>
			
			<? endif; ?>
		
		</div>	
  </div>
  
<? else: ?>

	<div class="layout_2" id="content_wrapper">
		<div id="wide_column">
			<div id="groups_and_clubs" class="section" align="center">
				<p class="help_text">
					<span><?=$group->getName()?> does not have a customized Group Section yet</span>. 
					<br />
					<a href="customgroupsection.php?gID=<?=$grpID?>&amp;mode=add" >
					  Click here to add one now.
					</a>
				</p>
			</div>
		</div>
	</div>
	
<? endif; ?>