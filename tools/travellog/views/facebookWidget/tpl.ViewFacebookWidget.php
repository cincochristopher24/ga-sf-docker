<script type="text/javascript">
	var fb_travelerID = <?php echo $travelerID ?>;
	var fb_oauthURL = '<?php echo $authorizeUrl?>';
	var fb_widgetStatus = <?php echo $widgetStatus ?>;
	
	function fb_connect(){
		// twitterWidgetStatus = 2 means that the user has no record yet and thus must be redirected to the oauthURL
		if(fb_widgetStatus == 2){
			window.location = fb_oauthURL;
		}
		else if(fb_widgetStatus == 0){
			jQuery.ajax({
				url: 'ajaxpages/facebookWidget.php',
				data: { action: 'connect', travelerID: widgetTravelerID},
				beforeSend: function(){
					jQuery('#fb_loading_img').html("<img src='images/loading.gif' />")
					},
				success: function(response){
					jQuery('#fb_loading_img').empty();
					jQuery('#fb_div_connect').hide();
					jQuery('#fb_div_connected').show();
					jQuery('#fb_disconnectWrapper').show();
				}
				});
		}

	}
	function fb_disconnect(){
		jQuery.ajax({
			url: 'ajaxpages/facebookWidget.php',
			data: { action: 'disconnect', travelerID: widgetTravelerID},
			beforeSend: function(){
				jQuery('#fb_setting_loading_img').html("<img src='images/loading_small.gif' />");
				},
			success: function(response){
				fb_widgetStatus = 0;
				jQuery('#fb_setting_loading_img').empty();
				self.parent.tb_remove(this);
				jQuery('#fb_div_connected').hide();
			//	jQuery('#widgetWrapper').css('background-color', '#FFFFFF');
				jQuery('#fb_disconnectWrapper').hide();
				jQuery('#fb_div_connect').show();
				
			}
			});
	}
	function fb_saveSettings(saveButton){
		var text = jQuery('#fb_defaultTextSetting').val();
		var origText = jQuery('#fb_origTextSetting').html();
		if(text != origText){
	//	if(text.length > 0){
			jQuery.ajax({
				url: 'ajaxpages/facebookWidget.php',
				data: { action: 'saveSettings', travelerID: widgetTravelerID, defaultText: text},
				beforeSend: function(){
					jQuery('#fb_setting_loading_img').html("<img src='images/loading_small.gif' />");
					},
				success: function(response){
					jQuery('#fb_setting_loading_img').empty();
					jQuery('#fb_origTextSetting').html(text);
				//	jQuery('#defaultTextSetting').val('');
					self.parent.tb_remove(this);
				}
				});
		}
		
	}
	
	function fb_cancelSetting(){
		if(jQuery('#fb_defaultTextSetting').val() != jQuery('#fb_origTextSetting').html()){
			jQuery('#fb_defaultTextSetting').val(jQuery('#fb_origTextSetting').html());
		}
		self.parent.tb_remove(this);
	}
	
	function fb_limitText(textarea, limitNum) {
		var limitCount = jQuery('#fb_characterCounter');
		var limitVal = limitCount.html();
		if(textarea.value.length > limitNum){
			textarea.value = textarea.value.substring(0, limitNum);
		}
		else {
			var limitVal = limitNum - textarea.value.length;
			if(limitVal == 0 || limitVal == ""){
				limitVal = '0';
			}
			limitCount.html(limitVal);
		}
	}
	
	function fb_positionThickBox(){
		window.scroll(0,0);
	}
	
	
</script>

<?php $style = ("" == $errorMessage) ? 'display:none' : '' ?>
<?php $defaultText = (!is_null($widgetSettings) ? $widgetSettings->getMessage() : '' )?>


<div class="unit size1of2"> <span id="facebookLogo" href="#">facebook</span> </div>

<div class="unit lastUnit size1of2 buttons standard rounded">

	<div id="fb_div_connected" style="display:none;">
		<p>Your <?=$siteName?> Network profile is now synchronized with your Facebook account. You may now share your travel journals, photos and videos as feeds.</p>
		<span id="widgetCheckMark">Connected</span>
		<a id="fb_widgetOptionsBtn" class="thickbox" style="text-decoration: none;" href="#TB_inline?height=600&width=550&inlineId=fb_modal&modal=true" onmouseup="fb_positionThickBox();" title="Change post text">Options</a>
	</div>

	<div id="fb_div_connect" style="display:none;">
		<p>Facebook is a social networking website that allows users to maintain personal profiles, add friends and send them messages. Click Connect to start sharing.</p>
		<button class="" id="widgetConnectBtn" type="button" onclick="fb_connect();" title="Connect to Facebook">Connect</button>
		<p class="errorMessage"><?php echo $errorMessage?></p>
	</div>
		
	<div id='fb_loading_img'></div>
	<div id="fb_disconnectWrapper" style="display:none;" >
		<button onclick="fb_disconnect()" title="Disconnect Facebook Widget">Disconnect</button>
	</div>

</div>
	
<div id="fb_modal" style="display:none;">
	
	<a class="modalClose" onclick="fb_cancelSetting();">X</a>
	<div class="modalBody" id="widgetOptionsWrapper">
		<ul>	
			<li class="modalHeader">
				<h4>Change Post Text</h4>
			</li>
			<li>
				<form class="socialMediaForm">
					<textarea class="postTextbox" name="limitedtextarea" id="fb_defaultTextSetting" onkeydown="return fb_limitText(this,117);" onkeyup="return fb_limitText(this,117);"><?php echo trim($defaultText); ?></textarea>
					<span id='fb_saveTextSetting_error'></span>
				</form>
			</li>
			
			<ul class="line">
				<li class="unit size1of2" id="facebookOptionsLeft">
					<p id="fb_textAreaHelpText">You may customize the message you wish to appear on Facebook whenever you share your <?=$siteName?> Network articles, photos and videos.</p>
					<div id="fb_origTextSetting" style="display:none;"><?php echo trim($defaultText); ?></div>
				</li>
				
				<li class="unit size1of2" id="facebookOptionsRight">
					<?php $limitCount = 117 - strlen($defaultText); ?>
					<p id="fb_characterCounter"><?php echo $limitCount?></p>
				</li>
			
			<li class="buttons standard rounded">
				<span id='fb_setting_loading_img'></span>
				<button id="fb_widgetOptionsSaveBtn" type="button" onclick="fb_saveSettings(this);" title="Save post text">Save</button>
			</li>
			</ul>
		</ul>
	</div>

</div>
<script type="text/javascript">
	if(fb_widgetStatus === 1){
		jQuery('#fb_div_connected').show();
	}
	else {
		jQuery('#fb_div_connect').show();
		jQuery('#fb_disconnectWrapper').hide();
	}

	jQuery('#facebookWidgetWrapper').hover(
		function(){
			if(jQuery('#fb_div_connect').css('display') == 'none'){
				jQuery('#fb_disconnectWrapper').show();
			}
		},
		function(){
			if(jQuery('#fb_div_connect').css('display') == 'none'){
				jQuery('#fb_disconnectWrapper').hide();
			}
		}
	);
</script>