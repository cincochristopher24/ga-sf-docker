<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs('/js/utils/json.js');
	Template::includeDependentJs('/js/tools/common.js');
	Template::includeDependentJs('/js/textloader.js');
	
	Template::includeDependentJs( '/js/jquery-1.1.4.pack.js'     );
	Template::includeDependentJs( '/js/date.js'                  );
	Template::includeDependentJs( '/js/jquery.datePicker.js'     );
	
	$jscode = <<<JSCODE
<script type="text/javascript">
	jQuery(function()
    {
		var d = new Date();
		jQuery('.date-pick').datePicker();
		
		if (jQuery('.date-pick').val() == '' ) {
			jQuery('.date-pick').dpSetStartDate(d.addDays(0).asString());
		} else {
			jQuery('.date-pick').dpSetStartDate(jQuery('.date-pick').val());
		}
    });
</script>
JSCODE;
	Template::includeDependent( $jscode );
	ob_start();
	$subNavigation->show();	
?>	

<div class="layout_2" id="content_wrapper">
	
<?php if("viewAll" == $method): ?>						
	<div class="alerts" id="wide_column">
		<div class="section">
			<h2>Alert Details</h2>
			<ul class="content">
				<?php
					$msgIterator->rewind();
					while( $msgIterator->valid() ):
						$attributeID = $msgIterator->current()->getAttributeID();					
				?>
						<li id="alert_entry">
							<p class="date dark_gray"><?= date("M d, Y",strtotime($msgIterator->current()->getExpiry())) ?></p>
							<div class="details" id="cont_<?= $msgIterator->current()->getAttributeID() ?>">
								<?php if(strlen($msgIterator->current()->getText()) > 500): ?>						
									<?php $shortMsg = substr($msgIterator->current()->getText(), 0, 499); ?>
									<?= HtmlHelpers::Textile($shortMsg.'... <a href="javascript:void(0)" onclick="TextLoader.load('.$msgIterator->current()->getAttributeID().');"><strong>[View&nbsp;More]</strong></a>',array('stripTags'=>false)) ?>																
								<?php else: ?>													
									<?= HtmlHelpers::Textile($msgIterator->current()->getText()) ?>
								<?php endif;?>
							</div>
							<a class="delete" onclick="return confirm('Are you sure you want to delete this alert message?')" href="alertmanagement.php?method=delete&amp;attributeID=<?= $attributeID ?>&amp;groupID=<?= $groupID ?>&amp;filter=<?= $filter ?>"><span>[Delete]</span></a>
						</li>
				<?php
						$msgIterator->next(); 
					endwhile;
				?>	
				<?php if($numRec < 1): ?>					
					<p class="help_text">
						<span>No Alert Messages</span>. <br />
						<a href="/alertmanagement.php?method=compose&amp;groupID=<?= $groupID ?>&amp;filter=<?= $filter ?>" >Create an Alert Message Now &raquo;</a>
					</p>					
				<?php endif;?>
			</ul>
						<?php if($numRec > 1 && $paging->getTotalPages() > 1): ?>
								<div class="pagination"><? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?></div>
						<?php endif; ?>

		</div>
	</div>
<?php elseif("compose" == $method): ?>
			
				<div id="wide_column">					
					<div class="section">
						<h2><span>Alert Details</span></h2>
						<div class="content">
							<form name="frmAddAlert" action="alertmanagement.php?method=save" method="post">
								<ul class="form">	
									<li>
										<?php if(isset($errCodes) && in_array(1,$errCodes)): ?>
											<p class="error message"><strong>Please enter a valid date.</strong></p>
										<?php endif; ?>
										<label for="txtExpirationDate">Expiration Date:</label>
										<input name="txtExpirationDate" id="txtExpirationDate" class="text date-pick" value="<?=$expirationDate?>" />
										<p class="supplement">(yyyy-mm-dd)</p>
									</li>									 																
									<li>
										<?php if(isset($errCodes) && in_array(2,$errCodes)): ?>
											<p class="error message"><strong>Please enter a message alert.</strong></p>
										<?php elseif(isset($errCodes) && in_array(3,$errCodes)): ?>		
											<p class="error message"><strong>Alert message cannot exceed more than 500 characters.</strong></p>									
										<?php endif; ?>
										<label for="txaAlertMessage">Message: </label>									
										<textarea name="txaAlertMessage" id="txaAlertMessage" cols="50" rows="15"><?=$alertMessage?></textarea>
									</li>
									<li class="checkbox">
										<input type="checkbox" name="chkIncludeMsg" id="chkIncludeMsg" />
										<label for="chkIncludeMsg">
											Include alert message in the notification.
										</label>
									</li>
									<li class="checkbox">
										<input type="checkbox" name="chkNotifyGroupMembers" id="chkNotifyGroupMembers" />
										<label for="chkNotifyGroupMembers">
											Send a notification email to all members of this group.
										</label>
									</li>
									<li class="actions">
										<input type="submit" value="Add" name="submit" class="submit" />
										<input type="hidden" name="groupID" value="<?= $groupID ?>" />
										<input type="hidden" name="filter" value="<?= $filter ?>" />	
									</li>
								</ul>
							</form>
						</div>
					</div>
				</div>
<?php endif; ?>
	
	<div id="narrow_column">
		<div id="alerts_actions" class="section">
			<?php if("viewAll" == $method): ?>
				<div class="content filter">	
							<form nbame="frmView" method="post" action="/alertmanagement.php">
								<ul class="form">
									<li>
										<label for="txtfilter" class="before"><strong>Show :</strong></label>
										<span>
											<select name="filter" class="text">
												<option value="all" <?= ($filter=="all")?" selected":"" ?> >All</option>
												<option value="active" <?= ($filter=="active")?"selected":"" ?>>Active</option>
												<option value="expired" <?= ($filter=="expired")?"selected":"" ?>>Expired</option>
											</select>
										</span>
										<span>
											<input type="submit" class="button" name="submit" value="Filter" />
											<input type="hidden" name="groupID" value="<?= $groupID  ?>" />	
										</span>
									</li>
								</ul>
							</form>								
				</div>
			<?php endif; ?>			
			<div class="content">								
				<ul class="actions">
					<?php if("viewAll" == $method): ?>
						<li><a href="/alertmanagement.php?method=compose&amp;groupID=<?= $groupID ?>&amp;filter=<?= $filter ?>" class="button">Compose an Alert</a> </li>
					<?php else: ?>
						<li><a href="/alertmanagement.php?groupID=<?= $groupID ?>&filter=<?= $filter ?>" class="button">Back to Alert Messages</a></li>
					<?php endif; ?>
				</ul>									
			</div>
		</div>
	</div>
</div>
