<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');
	Template::includeDependentJs('/js/interactive.form2.js');				// changed from interactive.form.js
	Template::includeDependentJs('/js/groupProfile.js');
	Template::includeDependentCss('/min/f=css/member_staff.css');

	Template::includeDependentCss("/min/g=GroupPagesCss");
	Template::includeDependentJs("/min/g=GroupPagesJs");
	
	if( isset($groupTagData) ){
		$tagLabels = json_encode($groupTagData);
		Template::includeDependent("<script>var GroupTagLabels = $tagLabels;</script>");
	}
	
	$showAssignAdminStaff = (isset($showAssignAdminStaff)) ? $showAssignAdminStaff : false;
	$showCategoriesList = (isset($showCategoriesList)) ? $showCategoriesList : false;
?>

<?= $profile->render()?>

<?	
	if (isset($subNavigation))
		$subNavigation->show();
?>

<div id="content_wrapper" class="layout_2">


		<div id="wide_column">
			<div class="section">
					<h2><span><?=$grpProfileLabel?></span></h2>
					<div class="content">
						
							<form name="frmGroupProfile" action="group.php" method="post" class="interactive_form">
								<p class="help_text">
									Fields with  <span class="required">*</span> indicate required fields.
								</p>
								<? if (isset($errorcodes)) { ?>
									<div>
										<ul class="errors">
										<?=$errormsg?>
										</ul>
									</div>								
								<? } ?>
								<ul class="form">
									<? if (isset($txtGroupEmail ) && isset($isAdvisor) && $isAdvisor ) { ?>
										<!-- <li><a href="javascript:void(0)" onclick="window.open('/changepassword.php?action=entry&amp;group=<?//=$gID?>','ChangePassword','width=500,height=650,navigation toolbar=disabled,left=200,top=200')">Change Administrator Accounts Settings</a></li> -->
										<li>
											<label for="txtGroupEmail">Email: <?= stripslashes($txtGroupEmail) ?></label>							 
										</li>
									<? } ?>
									<li>
										<label for="txtGroupName">Name <span class="required">*</span></label>
											<p class="supplement"> Please use only letters, numbers, dash or apostrophe. Use only single space between words. </p>
							
											 <input type="text" name="txtGroupName" value="<?= stripslashes($txtGroupName) ?>" class="text" size="50" maxlength="100" />
								 
									</li>
									
									<? if (isset($allGroupCat)){ ?>
										<li>
											<label for="f">Type <span class="required">*</span></label>
											<div id="group_type">
												<select multiple name="selGroupCatList" size="5" style="width:250px">
													
													<?php
														foreach($allGroupCat as $key => $value){ 
															if (!array_key_exists($key,$selGroupCat)) {
															?>
															<option value="<?=$key?>"><?=$value?>
													<? } }
													?>
												</select>
								
												<ul id="transfer_buttons">
													<li><input type="button" name="moveleft" id="moveleft"  
													onClick="move(this, this.form.selGroupCatChoice,this.form.selGroupCatList);  " value="<<"></li>
													<li><input type="button" name="moveright" id="moveright#"  
													onClick="move(this, this.form.selGroupCatList,this.form.selGroupCatChoice) ;" value=">>"></li>
												</ul>				
												<select multiple name="selGroupCatChoice" size="5" style="width:250px">
													<?php
														$grpcatlist = '';
														foreach($selGroupCat as $id=>$category):?>
															<option value="<?=$id?>"><?=$category?></option>
														<?  $grpcatlist = $grpcatlist . ',' . $id;  ?>
														<? endforeach; ?>
												</select>
											</div>
											<input type="hidden" name="selGroupCatKey" id="selGroupCatKey" 
												value="<?=isset($grpcatlist)? $grpcatlist : ''?>">
										</li>
									<? } ?>
						
									<li>
										<label for="txaDescription">Description <span class="required">*</span></label>
											<!-- stripslashes added by neri: 11-07-08 -->
											<textarea name="txaDescription" id="txaDescription"><? if (strlen($txaDescription) ) echo stripslashes($txaDescription) ?></textarea>
								 
									</li>
						
									<li>
										<label>Membership Options <span class="required">* </span><?=(isset($allGroupCat) && $grpDiscrim == 2) ? '(Group access of groups will be adjusted, if necessary)' : '' ?></label>
										<fieldset class="choices radio"> 											
											<ul>
												<? if (in_array(1,$arrAllowedRadAccess)) { ?>
													<li>
														<input type="radio" name="radAccess" id="radAccess_1" value="1" <? if ($radAccess == 1): ?> checked="checked"<? endif; ?> />
														<label for="radAccess_1">Open</label>
													</li>
												<? } ?>
												<? if (in_array(2,$arrAllowedRadAccess)) { ?>
													<li>
														<input type="radio" name="radAccess" id="radAccess_2"value="2" <? if ($radAccess == 2): ?> checked="checked"<? endif; ?> />
														<label for="radAccess_2">Open but requires moderator approval</label>														
													</li>
												<? } ?>
												
												<? if (in_array(3,$arrAllowedRadAccess)) { ?>
													<li>
														<input type="radio" name="radAccess" value="3" id="radAccess_3" <? if ($radAccess == 3): ?> checked="checked"<? endif; ?> />
														<label for="radAccess_3">Invite Only</label>
													</li>
												<? } ?>
												
											</ul>
										</fieldset>
									</li>
									
									<?//INSERT OPTION TO ASSIGN TO A GROUP PAGE TAG ?>
									<?php if( isset($tags) ): ?>
										<li>
											<label for="tags">Assign tags to this group: </label>
											<p class="supplement">To add <span class="strong">multiple tags</span> at once separate each tag with an empty space  e.g <span class="em">Volunteer</span> <span class="em">2009</span> <span class="em">Brazil</span></p>
											<input type="text" class="text" name="tags" id="tags" value="<?php echo $tags;?>" size="50" />											
											<script>
												if( "undefined" != typeof(GroupTagLabels) ){
													jQuery("#tags").autocomplete(
														GroupTagLabels,
														{
															multiple:true
														}
													);
												}
											</script>
										</li>
									<?php endif; ?>
									
									<? if ($showAssignAdminStaff) : ?>
									  <li onmouseover="this.className=''">
									    <label for="groupStaff"> Admin Staff: </label>
									    <p class="supplement">The following Super Staff will be assigned to this new group: </p>
									      <ul class="users member_list">
									        <? foreach($staff as $traveler): ?>
									          <? $profile = $traveler->getTravelerProfile(); ?>
				                    <? $username = $profile->getUserName(); ?>
				                    <? $thumbNail	= $profile->getPrimaryPhoto()->getPhotoLink('thumbnail'); ?>
				                    <? $fullname = $profile->getFirstName() . " " . $profile->getLastName(); ?>
				                  
				                    <li class = "staff">
				                      <a class = "thumb" href = "/<?=$username?>"> 
					                      <img src = "<?=$thumbNail?>" width = "65" height = "65" alt = "Member Thumbnail"/>
				                      </a>				
				                      <div class = "details overflow">
					                      <a href = "/<?=$username?>" class = "username" title = "<?=$username?>">
						                      <?=$username?>
						                      <span class = "full_name"><?=$fullname?></span>
					                      </a>	
				                      </div>
				                      <div class = "designation_group">
						                    <span>Assigned to: <span>All Groups</span> </span>
					                    </div>
				                    </li>
									      
									        <? endforeach; ?>
									      </ul>
									    <input type="hidden" name="assignStaff" value="1">
									  </li>
									<? endif; ?>
						
									<? if (isset($tplAdvisorGroupInfo)) { ?> 
										<?= $tplAdvisorGroupInfo ; ?>
									<? } ?>

									<li class="actions">
							
										<input type="hidden" name="gID" value="<?=$gID ?>" />
										<input type="hidden" name="pID" value="<?= (isset($pID)) ? $pID :  0 ?>" />
										<input type="hidden" name="addtoPNP" value="<?= (isset($addtoPNP)) ? $addtoPNP :  0 ?>" />
										
										<input type="hidden" name="grpDiscrim" value="<?=(isset($grpDiscrim)) ? $grpDiscrim :  '0' ?>" />					
										<input type="hidden" name="grpProfileLabel" value="<?=(isset($grpProfileLabel)) ? $grpProfileLabel :  '' ?>" />
										<input type="hidden" name="mode" value="save" />
										<input type="hidden" name="arrAllowedRadAccess" value="<?=implode(",",$arrAllowedRadAccess)?>" />
										<input type="submit" name="btnSubmit" value="Save" class="submit" />
										
										<? $backLink->getButton() ?>
							
									</li>
						
								</ul>
							</form>
					</div>
			</div>
		</div>

</div>