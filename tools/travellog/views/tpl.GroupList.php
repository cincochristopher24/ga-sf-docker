<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
  
  Template::setMainTemplateVar('title', 'Groups and Clubs - GoAbroad Network: Online Community for Travelers');
  Template::setMainTemplateVar('metaDescription', 'Connect with the travelers on GoAbroad Network by joining or creating online social circles.'); 
  Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
  Template::setMainTemplateVar('layoutID', 'main_pages');
  Template::setMainTemplateVar('page_location', 'Groups / Clubs');
?>

<div id="intro_container">
	<div id="intro">
		<?/* $ht = new HelpText();?>
		<h1><?= $ht->getHelpText('GROUP-TAB-SUBHEAD')?></h1>
		<div class="content"><?= HtmlHelpers::Textile($ht->getHelpText('groups/clubs-tab')); ?>			
		</div */?>
		<h1>Get expert travel advice.</h1>
		<div class="content">Join Groups and get reliable expert tips and advice from savvy travelers and professionals.</div>
	</div>
</div>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
		<div id="groups_and_clubs" class="section">
			<h2><span>Travel Groups</span></h2>
			<div class="content">
				<div id="groups_list_main">
				  <? //travellog/views/tpl.ViewGroupList.php?>
				  <?=$group_list_tpl?>
				</div>
			</div>
		</div>
	</div>
	<div id="narrow_column">
		<div id="search" class="section">
				<? // travellog/views/tpl.FrmGroupFilters.php ?>
				<?= $filter_template ?>
				
				<? // travellog/views/tpl.FrmSearchGrpName.php ?>
				<?= $search_template ?>
		</div>
		<?php if(!isset($GLOBALS['CONFIG'])): ?>
		<div class="section">
			<script type="text/javascript"><!--
				google_ad_client = "ca-pub-0455507834956932";
				/* .NET Sidebar Ad */
				google_ad_slot = "3558779789";
				google_ad_width = 250;
				google_ad_height = 250;
				//-->
			</script>
			<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>		
		</div>      
		<?php endif; ?>
	</div>
</div>
<!--added by ianne - 11/20/2008 success message after confirm-->
<?if(isset($success)):?>
	<script type="text/javascript">
		window.onload = function(){
			CustomPopup.initPrompt("<?=$success?>");
			CustomPopup.createPopup();
		}
	</script>
<?endif;?>