
<script language="JavaScript" type="text/javascript" src="/js/custompopup-1.2.js"></script>

<div id="journals_summary" class="section" >	
	<a name="anchor"></a>
	<h2><span><?=$compTitle?></span></h2>	
    <div class="content action_container">
        <?if ($canAddJournal):?>

			<!-- commented by Clyde -->
        	<!--<div class="header_actions">        	
        		<a href="<?=$addJournalBookLink?>">Add a Journal Book</a>        	
        	</div>-->

        <?endif;?>	
    </div>
	<div class="content action_container" >		
		<div id="content">
			<?include('tpl.JournalsCompGroupManageAjax.php');?>		
			
			<?if (count($journals) == 0){
				 echo '<p class="help_text"><span>' . $noJournalMessage  . '</span></p>';						
			}?>

		</div>
	</div>
</div>
<div id="dlg_container">
</div>

<!--
	* ianne - 11/29/2008
	*	added custompopups confirmation and success messages to journal-related functionalities
	*	(publish/unpublish, delete, feature/unfeature, approve/unapprove, to unapproved)
	* neri - 01/20/2009
	* 	changed "Unfeature" text to "Remove from Featured List" in featureJournal()
-->
<!-- javascript code for publish/unpublish journal -->
<script type="text/javascript">		
	var viewTab = "";
	//<![CDATA[						
	function publishJournal(travelID) {  	  	
		var cur_text = jQuery("#unpublish" + travelID).text();
	  	var publish_journal = function(){
			jQuery.ajax({
		  		url			:	"/journal.php",
		  		type		:	"GET",		
		  		data		:	"action=publishunpublish&travelID=" + travelID,
		  		success		:	function(response){
		  							var currentText = jQuery("#unpublish" + travelID).text();
									cur_text = currentText;
								  	if (currentText == "Publish")
								  		currentText = "Unpublish";
								  	else
								  		currentText = "Publish";
							  		
								  	jQuery("#unpublish" + travelID).text(currentText);			
		  						},
		  		beforeSend: function(xmlhttp) {  							 	
									jQuery('<p id="loading_image">Processing please wait...</p>').appendTo("#actions" + travelID);
							 	},
					 
				complete: function(xmlhttp,status) { 
					jQuery("#loading_image").remove(); 
					CustomPopup.initPrompt("This journal has been successfully "+cur_text.toLowerCase()+"ed.");
					CustomPopup.createPopup();
				}				
		  	});
		}
	  	CustomPopup.initialize(cur_text+" Journal ?","Are you sure you want to "+cur_text.toLowerCase()+" this journal?",publish_journal,cur_text);
		CustomPopup.setJS(true); // set that submit will be implemented using js
		CustomPopup.createPopup();  		
	  }
	  
	  function deleteJournal(travelID){
	  	//if (confirm('Are you sure you want to delete this journal?')){
	  		// make ajax call to delete journal
	  		//jQuery.get("journal.php?action=delete&travelID=" + travelID );
	  		
	  		var delete_journal = function(){
				jQuery.ajax({
		  			url			:	"/ajaxpages/journals-comp.php",
		  			type		:	"GET",
		  			data		:	"action=delete_journal&travelID=" + travelID,
		  			success		:	function(response) {
		  								//alert('journal deleted');
		  								window.location.reload();	  								
		  							},
		  			beforeSend	:	function(xmlhttp) {  							 	
										jQuery('<p id="loading_image">Deleting journal please wait...</p>').appendTo("#actions" + travelID);
							 		},						
					complete: function(xmlhttp,status) { 
								CustomPopup.initPrompt("This journal has been successfully deleted.");
								CustomPopup.createPopup();
								jQuery("#loading_image").remove(); 
							},
					error	: function (XMLHttpRequest, textStatus, errorThrown) {
	  								alert('error deleting journal');
							} 		 		
						 		
		  		});
			}
			CustomPopup.initialize("Delete Journal ?","Are you sure you want to delete this journal?",delete_journal,"Delete");
			CustomPopup.setJS(true); // set that submit will be implemented using js
			CustomPopup.createPopup(); 
	  			
	  	//}		  	
	  }	
	  
	
		// this is a quick add edit this later
	
	  function relateJournal(travelID,groupID){
		
		// dialog box to get the groups  this journal will relate to		
	 	var show_dialog = function () { var loginDialog = new YAHOO.widget.Dialog("dlg_container", 
				{ 
				    width : "340px",				    
				    visible : false, 
				    constraintoviewport : true,
				    draggable : true,	
				    context:	["approve" + travelID ,"tl","bl"],
				    buttons : [ { text:"Submit", handler:function(){submit_handler(loginDialog)}, isDefault:true }, { text:"Cancel", handler:function(){ this.hide();} } ]  

				} );

		 	loginDialog.setHeader("Check groups to relate this journal");									
			loginDialog.setBody("<div id=\"dlg_content\"></div>");			
			loginDialog.render();
			loginDialog.show();

		};	

		// edited by ianne - 11/26/2008
		var submit_handler = function(){
			var groupIDs = "";
			jQuery(".approve_selection").each(function(){
				if (this.checked)
					groupIDs += this.value + ",";  									
			});
			if (groupIDs.length > 0){
				groupIDs = groupIDs.substr(0,groupIDs.length -1 );
				jQuery.ajax({
						url 		:	"/ajaxpages/journals-comp.php",	
						type		:	"GET",
		  				data		:	"action=relate_journal_to_groups&travelID=" + travelID + "&groupID=" + groupID + "&groupIDs=" + groupIDs,
		  				success		:	function(response) {		  											  									
		  									if (response == 'SUCCESS'){
		  										
		  									}
		  									/*else
		  										loginDialog.setBody("Error:" + response);

		  									setTimeout(function(){loginDialog.hide();},1500);*/

		  								},
		  				beforeSend: function(xmlhttp) {  							 	
									jQuery('<p id="loading_image">Processing please wait...</p>').appendTo("#actions" + travelID);
							 	},

						complete: function(xmlhttp,status) { 
							jQuery("#loading_image").remove(); 
							// added success message -- ianne 11/28
							CustomPopup.initPrompt("This journal has been successfully related to groups.");
							CustomPopup.createPopup();
						}				 		  								
				});				
			}		

		};

	  	
		 	// get applicable groups 
		 	jQuery.ajax({
		 				url			:	"/ajaxpages/journals-comp.php",
		 				type		:	"GET",
		 				data		: 	"action=get_app_groups&travelID=" + travelID + "&groupID=" + groupID + "&approve=1",
		 				success		: 	function(response) {

		 										if (response == ""){	 											
													CustomPopup.initPrompt("No groups to relate.");
													CustomPopup.createPopup();
													//CustomPopup.initialize("Approve Journal ?","Are you sure you want to approve this journal?",tog_approve_this_group,"Approve");
													//CustomPopup.setJS(true); // set that submit will be implemented using js
													//CustomPopup.createPopup();
		 											//tog_approve_this_group();						// do the approval on a single group only
		 										} else {
		 											// parse groupID,name string
		 											var groupInfoAr = response.split(",");
		 											var htmlCode = "<ul>"; 
		 											for (var i = 0 ; i < groupInfoAr.length; i=i+2){
		 												htmlCode += '<li><input type="checkbox" class="approve_selection" name="groupID[]" style="width:12px"  value="' + groupInfoAr[i] + '"/>' + groupInfoAr[i+1] + '</li>'; 
		 											}
													htmlCode += "</ul>";
		 											//show_dialog();
		 											//jQuery("#dlg_content").attr("innerHTML","<form action>" + htmlCode + "</form>");

													// custompopup implementation added by ianne 11/26/2008
													CustomPopup.initialize("Check groups to relate this journal",htmlCode,submit_handler,"Submit");
													CustomPopup.setJS(true); // set that submit will be implemented using js
													CustomPopup.setHTML(true);
													CustomPopup.createPopup();
		 										}
		 									},
		 				complete	:	function() {}					
		 			});
	  }
	  // end quicky
	
	  function approveJournal(travelID,groupID){
		// check if approve or disapprove through the button text
		var linkText = jQuery("#approve" + travelID).attr("innerHTML");	
		
		
	 	
	 	// dialog box to get the groups  this journal will relate to		
	 	var show_dialog = function () { var loginDialog = new YAHOO.widget.Dialog("dlg_container", 
				{ 
				    width : "340px",				    
				    visible : false, 
				    constraintoviewport : true,
				    draggable : true,	
				    context:	["approve" + travelID ,"tl","bl"],
				    buttons : [ { text:"Submit", handler:function(){submit_handler(loginDialog)}, isDefault:true }, { text:"Cancel", handler:function(){ this.hide();} } ]  
	                          		    			   
				} );
		 	 
		 	loginDialog.setHeader("Check groups to relate this journal");									
			loginDialog.setBody("<div id=\"dlg_content\"></div>");			
			loginDialog.render();
			loginDialog.show();
		
		};	
		
		// edited by ianne - 11/26/2008
		var submit_handler = function(){
			var groupIDs = "";
			jQuery(".approve_selection").each(function(){
				if (this.checked)
					groupIDs += this.value + ",";  									
			});
			if (groupIDs.length > 0){
				groupIDs = groupIDs.substr(0,groupIDs.length -1 );
				jQuery.ajax({
						url 		:	"/ajaxpages/journals-comp.php",	
						type		:	"GET",
		  				data		:	"action=approve_journal_to_groups&travelID=" + travelID + "&groupID=" + groupID + "&groupIDs=" + groupIDs,
		  				success		:	function(response) {		  											  									
		  									if (response == 'SUCCESS'){
		  										//loginDialog.setBody("Journal approved!");
		  										// we toggle the text from between Approve/Unapprove
				  								var linkText = jQuery("#approve" + travelID).attr("innerHTML");
				  								var newText = "Approve";
				  								if (linkText == "Approve")
				  									newText = "Unapprove";
				  								jQuery("#approve" + travelID).attr("innerHTML",newText);
		  									}
		  									/*else
		  										loginDialog.setBody("Error:" + response);
		  											  									
		  									setTimeout(function(){loginDialog.hide();},1500);*/
		  											  											  										  												  											  								
		  								},
		  				beforeSend: function(xmlhttp) {  							 	
									jQuery('<p id="loading_image">Processing please wait...</p>').appendTo("#actions" + travelID);
							 	},
					  							 
						complete: function(xmlhttp,status) { 
							jQuery("#loading_image").remove(); 
							// added success message -- ianne 11/28
							CustomPopup.initPrompt("This journal has been successfully approved.");
							CustomPopup.createPopup();
						}				 		  								
				});				
			}		
						
		};
			
	 	 	 		 	
	 	var tog_approve_this_group = function() {	 		
			var link_text = jQuery("#approve" + travelID).attr("innerHTML");
		  	jQuery.ajax({
		  		url			:	"/ajaxpages/journals-comp.php",
		  		type		:	"GET",
		  		data		:	"action=tog_approve_journal&travelID=" + travelID + "&groupID=" + groupID,
		  		success		:	function(response) {
		  							if (response == "SUCCESS"){
		  								// we toggle the text from between Approve/Unapprove
		  								var linkText = jQuery("#approve" + travelID).attr("innerHTML");
										link_text = linkText;
		  								var newText = "Approve";
		  								if (linkText == "Approve")
		  									newText = "Unapprove";
		  								jQuery("#approve" + travelID).attr("innerHTML",newText);	
		  							} else {
		  								alert(response);
		  							}
		  						},
		  		beforeSend: function(xmlhttp) {  							 	
									jQuery('<p id="loading_image">Processing please wait...</p>').appendTo("#actions" + travelID);
							 	},
					  							 
				complete: function(xmlhttp,status) { 
					jQuery("#loading_image").remove(); 
					CustomPopup.initPrompt("This journal has been successfully "+link_text.toLowerCase()+"d.");
					CustomPopup.createPopup();
				}  		  							 					 				
		  	}); 		  	
	  	};
	  	
	  	
	  	if (linkText == "Approve") {				  									
		 	// get applicable groups 
		 	jQuery.ajax({
		 				url			:	"/ajaxpages/journals-comp.php",
		 				type		:	"GET",
		 				data		: 	"action=get_app_groups&travelID=" + travelID + "&groupID=" + groupID + "&approve=1",
		 				success		: 	function(response) {
		 										
		 										if (response == ""){	 											
													CustomPopup.initialize("Approve Journal ?","Are you sure you want to approve this journal?",tog_approve_this_group,"Approve");
													CustomPopup.setJS(true); // set that submit will be implemented using js
													if(jQuery('#hidParentID') != null && jQuery('#hidParentID').val() > 0)
														CustomPopup.setMessage("This journal will be approved for this subgroup and the main group.");													
													CustomPopup.createPopup();
		 											//tog_approve_this_group();						// do the approval on a single group only
		 										} else {
		 											// parse groupID,name string
		 											var groupInfoAr = response.split(",");
		 											var htmlCode = "<ul>"; 
		 											for (var i = 0 ; i < groupInfoAr.length; i=i+2){
		 												htmlCode += '<li><input type="checkbox" class="approve_selection" name="groupID[]" style="width:12px"  value="' + groupInfoAr[i] + '"/>' + groupInfoAr[i+1] + '</li>'; 
		 											}
													htmlCode += "</ul>";
		 											//show_dialog();
		 											//jQuery("#dlg_content").attr("innerHTML","<form action>" + htmlCode + "</form>");
	
													// custompopup implementation added by ianne 11/26/2008
													CustomPopup.initialize("Check groups to relate this journal",htmlCode,submit_handler,"Submit");
													CustomPopup.setJS(true); // set that submit will be implemented using js
													CustomPopup.setHTML(true);
													CustomPopup.createPopup();
		 										}
		 									},
		 				complete	:	function() {}					
		 			});
	 	} else if (linkText == "Unapprove")  {
			CustomPopup.initialize("Unapprove Journal ?","Are you sure you want to unapprove this journal?",tog_approve_this_group,"Unapprove");
			CustomPopup.setJS(true); // set that submit will be implemented using js
			if(jQuery('#hidParentID') != null && jQuery('#hidParentID').val() > 0)
				CustomPopup.setMessage("This action will remove the journal from the subgroup approved section and NOT from the main group.");
			else
				CustomPopup.setMessage("This action will remove the journal from the main group approved section as well as from the related subgroups.");
			CustomPopup.createPopup(); 		
	 		//tog_approve_this_group();
	 	} 		
	  	
	  	
	  }
	  
	  function featureJournal(travelID,groupID){		  		
			var link_text = jQuery("#feature" + travelID).attr("innerHTML");
	  		var feature_journal = function(){
				jQuery.ajax({
		  			url			:	"/ajaxpages/journals-comp.php",
		  			type		:	"GET",
		  			data		:	"action=tog_feature_journal&travelID=" + travelID + "&groupID=" + groupID,
		  			success		:	function(response) {
		  							if (response == "SUCCESS"){
		  								// we toggle the text from between Feature/Unfeature
		  								var linkText = jQuery("#feature" + travelID).attr("innerHTML");
										link_text = linkText;
		  								var newText = "Feature";
		  								if (linkText == "Feature")
		  									newText = "Remove from Featured List";
		  								jQuery("#feature" + travelID).attr("innerHTML",newText);	
		  								jQuery("#" + viewTab).click();
		  							} else {
		  								alert(response);
		  							}
		  						},
		  			beforeSend: function(xmlhttp) {  							 	
									jQuery('<p id="loading_image">Loading please wait...</p>').appendTo("#content" + travelID);
							 	},
				  							 
					complete: function(xmlhttp,status) { 
						jQuery("#loading_image").remove();
						msg = "featured.";
						if("Feature" != link_text)
							msg = "removed from the featured list.";
						CustomPopup.initPrompt("This journal has been successfully "+msg);
						CustomPopup.createPopup();
					}				
		  		});
			}
			conf_msg = "Are you sure you want to "+link_text.toLowerCase()+" this journal?";
			action_path = link_text;
			if("Feature" != link_text){
				conf_msg = "Are you sure you want to remove this journal from the featured list?";
				action_path = "Remove";
			}
			CustomPopup.initialize(action_path+" Journal ?",conf_msg,feature_journal,action_path);
			CustomPopup.setJS(true); // set that submit will be implemented using js
			CustomPopup.createPopup();
	  	}
	  	
	  function forwardToUnapproveJournal(travelID,groupID){
	  		
			var forward_to_unapproved_journal = function(){
				jQuery.ajax({
			  		url			:	"/ajaxpages/journals-comp.php",
			  		type		:	"GET",
			  		data		:	"action=forward_to_unapprove_journal&travelID=" + travelID + "&groupID=" + groupID,
			  		success		:	function(response) {
			  							if (response == "SUCCESS"){	  								
			  								//alert("Journal moved to unapproved section!");
			  								jQuery("#unapproved").click();	
			  							} else {
			  								alert(response);
			  							}
			  						},
			  		beforeSend: function(xmlhttp) {  							 	
										jQuery('<p id="loading_image">Processing please wait...</p>').appendTo("#actions" + travelID);
								 	},
				  							 
					complete: function(xmlhttp,status) { 
						jQuery("#loading_image").remove(); 
						CustomPopup.initPrompt("This journal has been successfully moved to the unapproved section");
						CustomPopup.createPopup();
						window.location.reload();
					}  		  							 					 				
			  	});		  	
			}
			CustomPopup.initialize("Move Journal to Unapproved Section ?","Are you sure you want to move this journal to the unapproved Section?",forward_to_unapproved_journal,"Move");
			CustomPopup.setJS(true); // set that submit will be implemented using js
			CustomPopup.createPopup();
	  }	
	  	
	  	
	  
	  function changePage(params){	  		  		  	
	  	params += "&action=change_page";
	  	journalCompAjaxRequest(params,"content","changePage");	  	  	
	  }	
	  	  
	  function changeTab(viewTab){	  		
	  		var params = "viewTab=" + viewTab + "&viewMode=" + "<?=$viewMode?>" + "&subjectID=" + "<?=$subjectID?>" + "&action=change_view_tab";	  		
	  		
	  		jQuery('<div id="loading"></div>').prependTo("#content");
	  		
	  		journalCompAjaxRequest(params,"loading");  
	  		
	  		// change active
	  		jQuery(".tabs li").attr("class","inactive");
	  			  		
	  		jQuery("#" + viewTab.toLowerCase()).parent().attr("class","active");
	  }   
	  	
	  function journalCompAjaxRequest(params,messCont,actionType){
	  	
	  	messCont = typeof(messCont) != 'undefined' ? messCont : 'content';
	  	
	  	jQuery.ajax({
	  		url				:	"/ajaxpages/journals-comp.php",
	  		type			:	"GET",
	  		data			:	params,
	  		success			:	function (response){	  								
	  								jQuery("#content").empty();
	  								jQuery("#content").append(response);
	  								jQuery(document).ready(function() { pageLoad(); });						 	  								
	  								if (actionType == "changePage")
	  									document.location.href = "#anchor";
	  							},
	  		beforeSend		: function(xmlhttp) {  							 	
								jQuery('<p id="loading_image">Loading please wait...</p>').appendTo("#" + messCont);
						 	},
			complete		: function(xmlhttp,status) { jQuery("#loading_image").remove();	  	/* function to run yahoo carousel*/	  },
			
			error			: 	function (XMLHttpRequest, textStatus, errorThrown){alert(errorThrown)} 			 	
	  	});		
	  }
	  
	//]]> 
</script>	
<div id="dlg"></div>






