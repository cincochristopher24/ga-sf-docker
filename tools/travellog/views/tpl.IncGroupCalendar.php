<?php
/*
 * Created on Oct 20, 2006
 * Author: Czarisse Daphne P. Dolina
 * Tpl.IncGroupCalendar.php - displays group calendar events; 
 * if fun group, display all group events;
 * if admin group, display both group events and activities for its program
 */
require_once('Class.GaDateTime.php');
require_once('Class.DateTimeHolder.php');
$d = new GaDateTime();

	/***********************************************************************************************
	 * edit of neri:	01-20-09
	 * 		changed "View Past Events" link text to "Manage Events" - if logged  in as admin/staff
	 ***********************************************************************************************/
?>


<div class="section" id="events_calendar">


	<h2>
		<span>Events</span>

		<span class="header_actions">
		<? if (TRUE == $isAdminLogged) { ?>
			<a href="/event.php?action=add&amp;gID=<?= $grpID ?>" class="add">Add Event</a> |
		<? } ?>
	
		<? if (5 < $countAllEvents || $hasRetriedGetEvents || $isAdminLogged) { ?>	
			<?php
				if ($isAdminLogged) $calstr = "Manage Events";
				elseif (5 < $countAllEvents || $hasRetriedGetEvents  )
					$calstr = "View All Events";
			?>
			<a href="/event.php?action=viewGroupEvent&amp;gID=<?= $grpID ?>" ><?=$calstr?></a>
		<? } ?>
		</span>		
	</h2>
	
		<div class="content" style="overflow: auto;">
			
			
			

			<? if (false == $hasRetriedGetEvents) { ?>
				<ul class="elements events" id="events_list">
					<? $cnt = 0; ?>					
					<?  foreach($grpCalendar as $k => $eachcalendar) { ?>
						<? if($cnt >= 5) break; ?>
						<li>
								<?php
									// we will now load meta date through ajax since we will still convert it to local time
									$containerClass = $eachcalendar->getEventID().'_'.strtotime($eachcalendar->getTheDate()).'_'.$eachcalendar->getTimeZoneID().'_'.$k;
									$link_to_event = urlencode("/event.php?action=view&amp;gID=".$grpID."&amp;eID=".$eachcalendar->getEventID()."&amp;context=".$eachcalendar->getContext());
									DateTimeHolder::getInstance()->addProfileEventDetails($eachcalendar->getEventID(),$eachcalendar->getTitle(),$eachcalendar->getContext(),$link_to_event,$isAdminLogged);
									DateTimeHolder::getInstance()->addDateTime($eachcalendar->getTheDate(), $containerClass, $eachcalendar->getTimeZoneID());
								?>
								
								<span class="meta date" id="<?php echo $containerClass ?>"></span>
								<?/*
								<span class="meta date">
									<?= $d->set($eachcalendar->getTheDate())->htmlEventDateFormat(); ?>
									<span id="events_time" ><?php echo date('h:i A', strtotime($eachcalendar->getTheDate())) ?></span>
									<?php if(function_exists('date_create') && $eachcalendar->getTimeZoneID() ):?>
									<span id="events_zone" onmouseover="jQuery().showEquivalentLocalTime('<?=$eachcalendar->getEventID()?>','groupevent', this)" onmouseout="CustomPopup.removeBubble();">
										<?php echo 'GMT '. substr(date_create($eachcalendar->getTheDate(), new DateTimeZone($eachcalendar->getTimeZone()))->format('O'),0,3); ?>
									</span>
									<?php endif; ?>
								</span>
								*/?>
								<h4><a href="/event.php?action=view&amp;gID=<?= $grpID ?>&amp;eID=<?= $eachcalendar->getEventID() ?>&amp;context=<?= $eachcalendar->getContext() ?>" ><?= $eachcalendar->getTitle();	?></a></h4>
								<div class="details">							
									<? if (TRUE == $isAdminLogged) { ?>								
										<? if (0 == $eachcalendar->getContext()) {
										   		$urledit = "activity.php?action=edit&aID=" . $eachcalendar->getEventID().'&context=0';
										   		$urldelete = "activity.php?action=remove&aID=" . $eachcalendar->getEventID().'&context=0';
										   }
										   else {
										   		$urledit = "event.php?action=edit&eID=" . $eachcalendar->getEventID().'&context=1';
										   		$urldelete = "event.php?action=remove&eID=" . $eachcalendar->getEventID().'&context=1';
										   }
										?>								
										<a id="edit" href="/<?= $urledit ?>" >Edit</a> | <a id="delete" href="javascript:void(0);" onclick="CustomPopup.initialize('Delete Event ?','Are you sure you want to delete this event?','/<?= $urldelete ?>','Delete Event','1');CustomPopup.createPopup();" >Delete</a>
									<? } ?>

								</div>
						</li>						
						<? $cnt++; ?>
					<? } ?>				
				</ul>

			<? } ?>	
			<? if (0 == count($grpCalendar) && $isAdminLogged) { ?>
				<? /*if ( $is_parent_group ) : ?>
					<p class="side_help_text">There are no calendar events added to this group.</p>
				<? else : ?>
					<p class="side_help_text">There are no calendar events added to this subgroup.</p>
				<? endif */?>
				<div class="sg_description">
					<h3></h3>
					<p>
						Create a public or private calendar/itinerary of events.
					</p>
					<a class="button_v3 goback_button jLeft" href="/event.php?action=add&amp;gID=<?= $grpID ?>"><strong>Add an Event</strong></a>
				</div>
			<? } else if (count($grpCalendar) && $hasRetriedGetEvents) { // if there are events, but are not current ?>  
				<?/*<p class="side_help_text">There are no current calendar events.</p> */ ?>
				<div class="sg_description">
					<h3></h3>
					<p>
						Create a public or private calendar/itinerary of events.
					</p>
					<a class="button_v3 goback_button jLeft" href="/event.php?action=add&amp;gID=<?= $grpID ?>"><strong>Add an Event</strong></a>
				</div>
			<? } ?>					
		</div>			
</div>
<!--?php Template::includeDependentJs('/min/f=js/jquery.json-1.3.min.js',array('bottom'=>true));?-->
<!--?php Template::includeDependentJs('/min/f=js/localtime.js');?-->

<script type="text/javascript" charset="utf-8">
	b = '<?php echo DateTimeHolder::getInstance()->toJsonData()?>';
	LocalTimeConverter.convertAndSortEventDates(b,2);
</script>
