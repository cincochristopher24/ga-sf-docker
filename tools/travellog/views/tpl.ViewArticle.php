<?=$contents['profile_view']->render()?>
<?=$contents['sub_navigation']->show()?>
<div id="content_wrapper" class="control_divider articleMark">
	<div id="wide_column">	
		<h1  id="ENTRYTITLE" class="journal_header clear">
			<?=$contents['article']->getTitle()?>
		</h1>
		<div style="clear:both;"></div>
		<?if( $contents['enable_ctrls'] ):?>
			<a class="entry_links bottom_shift" href="javascript:void(0)" id="ArticleTitleLink" rev="<?=$contents['article']->getArticleID()?>"><span>Edit Article Title</span></a>
		<?endif;?>
	
		<div id="popup-entry-location">
			<?
				$location = LocationFactory::instance()->create($contents['article']->getLocID());
				$country = $location->getCountry()->getName();
				$city = $location->getName();
				$locationID = $location->getCountry()->getCountryID();
			?>
			<span style="display:inline" id="location-part">	
				<img id="journal_entry_flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" alt="Articles from <?=$country?>" />						
				<?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?>
			</span>
			<?if(strtotime($contents['article']->getLogDate())):?>
				| <span style="display:inline"><?= date('M d, Y', strtotime($contents['article']->getLogDate())); ?></span>
			<?endif;?>
		</div>			
		<span id="popup-location" style="display:none"></span>
		<div id="add_city_box" class="dialog">
			<?php 
				if( isset($arr_vars['isAdmin']) )
					include '../addNewCity.php';
				else
					include 'addNewCity.php';
			?>
		</div>
		<?if( $contents['enable_ctrls'] ):?> 
			<a class="entry_links bottom_shift" id="edit-article-location" href="javascript:void(0)"><span>Edit Article Location</span></a>
		<?endif;?>
		
		<div class="socialMediaBox">
			<?=$sub_view['fb_like_control']->render()?>
			<?=$sub_view['bookmark_controls']->render()?>
		</div>

	<?=$contents['comments_view']->fetchShoutOutPopUp() ?>
		<div class="entry_content">	
			<div class="entrymiddleline">
				<div id="ENTRYDESCRIPTION" class="entrydescription">
					<?=$sub_view['photo_desc_callout']->render()?>
				</div>
			</div>
			
			<?if(trim($contents['article']->getEditorsNote()) != "" || $contents['enable_ctrls']):?>
				<p class="articlenotes clear"><strong>Editor's Note:</strong> 
					<span id="EDITORSNOTE">
						<?$note = preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/","<a href=\"\\0\">\\0</a>", $contents['article']->getEditorsNote());?>
						<?=$note?>
					</span>
					<?if( $contents['enable_ctrls'] ):?>
						<a class="entry_links bottom_shift" href="javascript:void(0)" id="ArticleEditorsNoteLink" rev="<?=$contents['article']->getArticleID()?>"><span>Edit</span></a>
					<?endif;?>
				</p>
			<?endif;?>
			<?if(count($contents['tags']) || $contents['enable_ctrls']):?>
				<p class="articlenotes clear"><strong>Tags:</strong>
					<span id="ARTICLESTAG">
						<?$cnt=0;?>
						<?foreach( $contents['tags'] as $tag):?>
							<a href="/journal.php?action=All&amp;tag=<?=trim($tag)?>"><?=trim($tag)?></a>
							<?$cnt++;?>
							<?if($cnt < count($contents['tags'])):?>,<?endif;?>
						<?endforeach;?>
					</span>	
					<?if( $contents['enable_ctrls'] ):?>
						<a class="entry_links bottom_shift" href="javascript:void(0)" id="ArticleTagsLink" rev="<?=$contents['article']->getArticleID()?>"><span>Edit</span></a>
					<?endif;?>
				</p>
			<?endif;?>				

			<?/*?><p class="articleviewer bookmark_controls"><strong>Bookmark this Article:</strong>
				<?=$sub_view['bookmark_controls']->render()?>
			</p>*/?>
		</div>
		
		<?if(!$contents['enable_ctrls']):?>
			<a id="report" href="/reportabuse.php?aID=<?=$contents['article']->getArticleID()?>">Report inappropriate article</a>
		<?endif;?>	

		<div id="comments" class="section">
			<?=$contents['comments_view']->renderAjax()?>
		</div>
	</div>	
		
	<div id="narrow_column">
		<?if( $contents['enable_ctrls'] ):?>
			<?$param = ($contents['article']->getGroupID() != 0) ? "&gID=".$contents['article']->getGroupID() : "";?>
			<a class="entry_links bottom_shift deletentry" href="javascript:void(0)" onclick="CustomPopup.initialize('Delete Article ?','Are you sure you want to delete this article?','/article.php?action=delete&articleID=<?=$contents['article']->getArticleID()?><?=$param?>','Delete','1');CustomPopup.createPopup();" ><span>Delete</span></a>
		<?endif;?>		
		<div id="travel_map" style="width:203px; height:203px"></div>	
    	<?//= $sub_view['article_map']->render()?><br/>
		<div id="gallerycontainer">
			<?=$contents['thumbnail_view']->render()?>
		</div>
		<?php if($contents['enable_ctrls']): ?>
		<div class="create">	
			<ul class="actions">
				<li>
					<a class="button" href="/article.php?action=add">+ Add a new Article</a>
				</li>
			</ul>	
		</div>
		<?php endif; ?>
		<?=$sub_view['other_articles']->render()?>
		<div id="gaothers">
			<?=$sub_view['ads_view']->render()?>
		</div>
	</div>
</div>
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$.ajax({
				url: "/retrieve-map.php",
				data: "context=3&articleID=<?php echo $contents['article']->getArticleID()?>",
				type: "GET",
				success: function(res){
					$('#travel_map').attr('class','map');
					eval(res);
				}
			});
		});
	})(jQuery);
	
	<?php if($contents['enable_ctrls']): ?>
		window.onbeforeunload = function(e){
			if( null != document.getElementById("content_tbl") ){
				return "You might lose some unsaved text if you do so. You may want to save your article properly before navigating away from this page.";
			}
		}
	<?php endif; ?>
</script>