<?php
require_once("travellog/views/Class.AbstractView.php");
class EntryHighlightsView extends AbstractView{
	function render(){
		return ( $this->contents['travellogID'] )? $this->obj_template->fetch('tpl.EntryHighlights.php'): NULL;
	}	
}
?>
