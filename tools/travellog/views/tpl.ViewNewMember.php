<?php
	ob_start();

	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'page_home');
	Template::setMainTemplateVar('page_location', 'Home');

	Template::includeDependentJs('/js/assignstaffmanager.js');
	Template::includeDependentJs("/js/utils/Class.Request.js");
	Template::includeDependentCss('/css/assign_subgroup.css');

	ob_end_clean();
?>

<div class="area" id="top_wrap">
	<div style = "width: 80%">
		<div>
			<h1>
				Welcome! You are now part of the GoAbroad.net Family!
			</h1>
		</div>
		<div style = "padding: 30px;">
			<div style = "border: 1px solid grey;width: 500px;height:300px;">				
				<div id="statusPanel1" class="widePanel" style="display: none;">
					<p class="loading_message">
						<span id="imgLoading">
						<img alt="Loading" src="/images/loading.gif"/>
						</span>
						<em id="statusCaption">Loading data please wait...</em>
					</p>
				</div>
				<div id = "member_box_content">
					<?=$viewchange->renderChangeUserInfo()?>
				</div>
			</div>
		</div>
		
		<div class="clear"/>
		<div style = "padding: 30px;">
			<div style = "border: 0px solid grey;width: 800px;">
				<div class = "notice" style = "font-size: 12px;">
						<?php
							if (1 < sizeof($staffedGroups)) :
								echo "<strong>You</strong> are now a staff in the following groups: <br />";
								foreach ($staffedGroups as $subgroup)
									echo "<br />" . $subgroup->getName();
								echo ".";
							else :
								$subGroup = $staffedGroups[0]->getName();
								echo "<strong>You </strong> are now a staff of group " . $subGroup . ".";
							endif;
						?>
				</div>
			</div>
		</div>
	</div>
</div>