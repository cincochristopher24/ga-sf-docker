<?php
/**
 * Created on Aug 10, 2006
 * Purpose : template for displaying travels summary for an AdminGroup
 */
 
 
?>


<?
require_once("Class.Criteria2.php");
require_once('Class.GaDateTime.php');
require_once('travellog/model/Class.HelpText.php');

$criteria  = new Criteria2();
$criteria1 = new Criteria2();
$criteria->setOrderBy('travellogID DESC');
$criteria1->setOrderBy('arrival, tripID');
$d = new GaDateTime();
?>

<h2><span>Travel Journals</span></h2>	
<div class="content">
	
	<?if (null != $travels_array && isset($canEdit) ): ?><div class="header_actions"><a href="/<?=$addTravelLink ?>">Create a Travel Journal</a></div><?endif;?>
	
	<?php if ($travels_array != null) : ?>	
		
		<ul class="travel_journals_container">
			<?php foreach($travels_array as $travel) : ?>	
				<li>
					

          			<img class="arrow" src="images/sections/uncollapsed_arrow.gif" alt="expand" />	
					<div class="details">
						
					<h3><a class="journal_title" href="/journal-entry.php?action=view&amp;travelID=<?=$travel->getTravelID()?>" ><?= $travel->getTitle()  ?></a></h3>	
					
					<?  
						$entry_lists       = '';
						$ctr               = 0;
						$exceedFromTotal   = false;
						$arrTrips	= $travel->getTrips($criteria1);
						$latestDescription = NULL;
						$arr_journals      = array();
						$collectionLogs    = $travel->getJournalLogs();
						$jID               = array();      
						$objJournalEntry   = NULL;
						
						if (count($arrTrips)){
							for($i = (count($arrTrips)-1); $i >= 0; $i--){	
					   			$arrJournalEntry = $arrTrips[$i]->getTravelLogs($criteria);
					   			foreach( $arrJournalEntry as $objJournalEntry ){
					   				$arr_journals[$objJournalEntry->getTravelLogID()] = $objJournalEntry;
					   				if( $ctr < 2 ){
					   					$entry_lists .= '<div class="entry_list"><a href="/journal-entry.php?action=view&travellogID='.$objJournalEntry->getTravelLogID().'">'.$objJournalEntry->getTitle().'&nbsp;&nbsp;</a><p class="entry_date_photo">'.date('d F',strtotime($objJournalEntry->getTrip()->getArrival())).' &nbsp;|&nbsp;'.$objJournalEntry->countPhotos();
					   					if( $objJournalEntry->countPhotos() > 1 ){
					   						if( array_key_exists($objJournalEntry->getTravelLogID(), $collectionLogs) ){
					   							$entry_lists .= ' Photos<span class="span_new"><img src="/images/icons_new.gif"/></span>';
					   							$jID[]        = $objJournalEntry->getTravelLogID();
					   						}else
					   							$entry_lists .= ' Photos';
					   					}else
					   						if( array_key_exists($objJournalEntry->getTravelLogID(), $collectionLogs) ){
					   							$entry_lists .= ' Photo<span class="span_new"><img src="/images/icons_new.gif"/></span>';
					   							$jID[]        = $objJournalEntry->getTravelLogID();
					   						}else
					   							$entry_lists .= ' Photo';
					   					$entry_lists .=	' </p></div>';
					   				}else
					   					$exceedFromTotal = true;
					   				$ctr++;
					   			}					   			
					   		}
					   		
					   		$objTrips = $arrTrips[count($arrTrips)-1];
							$arrJournalEntry = $objTrips->getTravelLogs($criteria);
							$objJournalEntry = $arrJournalEntry[0];
							if( strlen(trim($objJournalEntry->getDescription())) ) 
								$latestDescription = $objJournalEntry->getDescription();
						
						}  		
						?>
						

							<? echo  $entry_lists ;    	
					    		$ctr1 = 0;
					  	    	foreach( $collectionLogs as $objLogs ){
						    		if( array_key_exists( $objLogs->getTravellogID(), $arr_journals) && !in_array($objLogs->getTravellogID(), $jID) ){
						    			$obj_journal_entry = $arr_journals[$objLogs->getTravellogID()];
						    ?>			
						    			<div class="entry_list">
							<a href="/journal-entry.php?action=view&amp;travellogID=<?=$obj_journal_entry->getTravelLogID()?>"><?=$obj_journal_entry->getTitle()?></a>
						    			<p class="entry_date_photo"><?=date('d F',strtotime($obj_journal_entry->getTrip()->getArrival()))?>&nbsp;|&nbsp; <?=$obj_journal_entry->countPhotos()?> Photo
						    			
					    				<? if( $obj_journal_entry->countPhotos() > 1 ): ?>
					    					'<span class="span_new"><img src="/images/icons_new.gif"/></span>
					    				<? else : ?>
					    			 		'<span class="span_new"><img src="/images/icons_new.gif"/></span>
					    			 	<? endif ; ?>
					    			 		</p></div>
					    			 	<?
					    			 	$ctr1++;
						    		}
						    		
						    	}
					  		?>
					  		
					      	<?if( $exceedFromTotal ):
				      			$more_entry = NULL;
				      			if( ($ctr+$ctr1) > (2+$ctr1) ) $more_entry = $ctr - ($ctr1+2);
					      	?>
					  			<p class="morelink"><a  align="right" href="/journal-entry.php?action=view&travelID=<?=$objJournalEntry->getTravelID()?>"><?=$more_entry?> More...</a></p>					  			
					  		<?endif;?>     
					    

						<? if ($objJournalEntry != NULL) : ?>
						   		
                           	<div class="entry_content">	
							<!--><p class="entry_summary"><a href="/journal-entry.php?action=view&travellogID=<?=$objJournalEntry->getTravelLogID()?>"><?=$objJournalEntry->getTitle()?></a></p>
							<p class="entry_summary"><?=$d->set($objJournalEntry->getTrip()->getArrival())->friendlyFormat()?> | Photo: <?=count($objJournalEntry->getPhotos())?></p>-->					
							<p class="entry_summary"> <?= nl2br(substr($latestDescription, 0, 250));
											if( strlen(trim($latestDescription)) > 250 ) echo '...'; ?> 
							</p>
							</div>
						<? else : ?>
							<p>No Entries Available</p>
						<? endif; ?>
					</div>
					
					<? if ($objJournalEntry != NULL) : ?>
						<div class="photo">
							<?if( $travel->getRandomPhoto() ):?>
								<a href="/journal-entry.php?action=view&amp;travelID=<?=$travel->getTravelID()?>">
									<img src="<?=$travel->getRandomPhoto()->getPhotoLink()?>" alt="journal photo" width="100px">
								</a>
							<? endif; ?>
					 	</div>
					<? endif; ?>
					
					
					<div class="clear"></div>	

				</li>
			<?php endforeach; ?>
		</ul>

	<?php else: ?>
		<p class="help_text"><span>
			<? if (isset($canEdit) && $canEdit){
				   //$ht = new HelpText();
				   //echo nl2br($ht->getHelpText('group-traveljournals'));		// key for travel journals in dashboard part
				   echo $helptext;
				} else {
				   echo 'No travel journals.';
				}
			?>
			</span>
		</p>
		
		<div class="header_actions">
			<?if (isset($canEdit)) :?>
				<a class="button" href="/<?=$addTravelLink ?>" id="abtn_create_first_journal" >Create Group's First Travel Journal &raquo; </a>
			<?endif;?>		
		</div>
			
	<?php endif; ?>	

	<div class="clear"></div>
	
	<?if ($hasMoreJournals):?>
		<div class="foot_actions">
			<a href="/<?=$moreJournalsLink?>">More Journals ...</a>
		</div>
	<?endif;?>
	
</div>
<div class="foot"></div>

	
