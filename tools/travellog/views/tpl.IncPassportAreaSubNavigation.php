

<ul id="sub_nav" class="sub_nav_small">		
	<?php 
		$firstIsTaken = false;
		foreach($mainLinks as $navLink):
			if(!$navLink->getIsHidden()):
	?>
			<li <?php if(!$firstIsTaken): $firstIsTaken = true;?>class="first"<?php endif; ?> <?php if($navLink->getIsHighlighted()): ?>id="active"<?php endif;?>>
				
				<?if($navLink->getHasNewFeatures()):?>
					<a class="featuremarker" href="javascript:void(0)" onmouseout="CustomPopup.removeBubble();" onmouseover="CustomPopup.fetchNewBubble('<?= $navLink->getNewLink($navLink->getLinkText()) ?>',this);"><span>New</span></a>
				<?endif;?>	
							
				<a href="<?= htmlentities($navLink->getUrl()) ?>" <?if(!$navLink->getHasNewFeatures()):?>class="subnav_controller"<?endif;?>>
				<?php if($navLink->getIsHighlighted()): ?>
					<span><strong><?= $navLink->getLinkText() ?></strong></span>
				<?php else: ?>
					<span><?= $navLink->getLinkText() ?></span>
				<?php endif; ?>
				</a>
			</li>
	<?php 
			endif;
		endforeach; 
	?>
</ul>
<?php if(count($subLinks)): ?>
	<ul id="subgroup_nav">		
		<?php 
			$firstIsTaken = false;
			foreach($subLinks as $navLink):
				if(!$navLink->getIsHidden()):
		?>
				<li <?php if(!$firstIsTaken): $firstIsTaken = true;?>class="first"<?php endif; ?> <?php if($navLink->getIsHighlighted()): ?>id="active"<?php endif;?>>
					<a href="<?= htmlentities($navLink->getUrl()) ?>">
					<?php
						$navlinktext = $navLink->getLinkText();
						if (strlen($navlinktext) > 22) {
							$hovernavlinktext = $navlinktext;
							$navlinktext = substr($navlinktext, 0, 22) . '...';
						}
					?>
					<?php if($navLink->getIsHighlighted()): ?>
						<span><strong <?php if (isset($hovernavlinktext)) echo "title='$hovernavlinktext'"; ?>><?= $navlinktext ?></strong></span>
					<?php else: ?>
						<span <?php if (isset($hovernavlinktext)) echo "title='$hovernavlinktext'"; ?>><?= $navlinktext ?></span>
					<?php endif; ?>
					</a>
				</li>
		<?php 
				endif;
			endforeach; 
		?>
	</ul>
<?php endif; ?>

