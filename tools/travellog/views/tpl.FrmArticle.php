<div id="content_wrapper" class="layout_2">
   <div id="wide_column">
        <div class="section">
                      
              <h2><span><?=( $props['mode']== 'add' )? 'Create ': 'Edit '?> an Article </em></span></h2>
			<div class="content">
					<? if (count($props['errors'])):?>
						<div class="error_notice">
							<ul>
							<?foreach ($props['errors'] as $error) :?>
									<li><?= $error ?></li>
							<? endforeach; ?>
							</ul>
						</div>
					<? endif; ?>
					
					<form name="sform" action="/article.php<?=$props['gID']?>" method="post" class="interactive_form" id="sform">
						<ul class="form">	
							<li>	
								<label for="title">Title of Article<span class="required">*</span></label>
								<input type="text" name="title" id="title" size="40" value="<?=$props['title']?>" class="text big charcount" />
								<br/><input type="text" disabled="disabled" id="title_counter" size="3" value="" /> characters remaining
							</li>
							
							<li class="labeled">
								<fieldset>
									<legend><span>Location of Article</span></legend>
									<span class="labeled_input_wrapper">
									    <select name="countryID" id="countryID">
									    	<?foreach( $col_countries as $obj_country ):?>
									    		
									    		<option value="<?=$obj_country->getLocationID()?>" <?if(isset($props['countryID']) && $props['countryID'] == $obj_country->getLocationID()):?>selected="true"<?endif;?>><?=$obj_country->getName()?></option>
									    	<?endforeach;?>	
									    </select>
										<label for="countryID">Country</label>
								    </span>
									<span class="labeled_input_wrapper">
										<span id="citylist">
											<select name="cityID" id="cityID" onchange="jQuery.triggerLocation(this)"> 
												<option value="0">None</option>    
											</select>
										</span>
										<label for="citylist" id="lblCityName">City</label>
									</span>
								</fieldset>
							</li>
														
							<li>	
								<label for="callout">Callout:</label>
								<p class="supplement">
									A Callout is the highlight sentence(s) of your article. You may  
									select a sentence or two from your article and copy them here.  
									This callout may be the highlight of your experience, or it could be  
									your entry in a nutshell. The callout will be  
									displayed in bigger text and will be placed in quotes.
								</p>
								<input type="text" name="callout" id="callout" size="55" maxlength="250" value="<?=$props['callout']?>" class="text big charcount" />
								<br/><input type="text" disabled="disabled" id="callout_counter" size="3" value="" /> characters remaining
							</li>
							<li style="padding-right: 12px">
								<label for="description">Please write your Article here<span class="required">*</span></label>
								<p class="supplement">
									To prevent losing your content, we encourage you to write from a text editor then copy your text here.
								</p>
								<textarea name="description" id="description" rows="25" cols="75"><?=$props['description'] ?></textarea>
							</li>
							<li>
								<label for="editorsnote">Editor's Note:</label>
								<input type="text" name="editorsnote" id="editorsnote" size="55" maxlength="250" value="<?=$props['editorsnote']?>" class="text big" />
							</li>							
							<li>	
								<label for="tags">Tags:</label>
								<p class="supplement">
									Separate multiple tags with comma.
								</p>
								<input type="text" name="tags" id="tags" size="55" maxlength="250" value="<?=$props['tags']?>" class="text big" />
							</li>	
							<? if(isset($props['isAdmin']) && $props['isAdmin']):?>
								<li>
									<label class="inpt_box">Last Updated</label>
									<p class="supplement">
										example: &#8220;yyyy-mm-dd&#8221;.
									</p>
									<input type="text" name="lastupdate" id="lastupdate" size="20" maxlength="10" value="<?=$arr_vars['lastupdate']?>" class="text small" />
								</li>
							<?endif;?>
							<?if($props['mode']=='add'):?>
								<li>
									<?/*if( $props['is_administrator'] || $props['is_advisor'] ):?>
									<label for="arrivaldate">Send notice to</label>
									<?//*if( $props['is_administrator'] || $props['is_advisor'] ):/?>
										<input type="checkbox" name="chkGroup"            id="chkGroup"            value="1" <?if($props['chkGroup']):?>checked<?endif;?> />Group Members<br />
										<input type="checkbox" name="chkGroupAddressBook" id="chkGroupAddressBook" value="1" <?if($props['chkGroupAddressBook']):?>checked<?endif;?> />Group Members Address Book entries<br />
									<? endif;*/ ?>
									<!--input type="checkbox" name="chkAddressBook"          id="chkAddressBook"      value="1" <?/*if($props['chkAddressBook']):?>checked<?endif;*/?> />Address Book entries -->
								</li> 
							<? endif; ?>
							<li id="regbutton" class="actions">	
								<input type="submit" value="Save" class="submit" onclick="saveClicked = true;" />
								<input type="hidden" name="action" id="action" value="save" />
								<input type="hidden" name="articleID" id="articleID" value="<?=$props['articleID']?>" />
							</li>
                 </ul>
                  <div class="clear"></div>
                </form>

             
          
            </div>
          
		  <div class="foot"></div>

          </div>
         
   
       
     

  </div>

	<!-- ##### Added By: Cheryl Ivy Q. Go		23 May 2008 #####-->
	<!-- ######### Purpose: Modal box to add new city ########## -->
	<!-- ########### Comment: PLEASE DON'T REMOVE! ############# -->
	<div id = "add_city_box" class="dialog">
		<?php 
			if( isset($arr_vars['isAdmin']) )
				include '../addNewCity.php';
			else
				include 'addNewCity.php';
		?>
	</div>
	<!-- ##### END #####-->

	<div id="narrow_column">
		<div class="helpextra"></div>
		<div class="helpbox">
			<h2>What is an Article?</h2>
			<p>Do you want to share interesting travel stories but you don't really need several journal entries to do it? Now you can write these stories as travel articles, then publish and share them on GoAbroad Network. Through the new Travel Articles section, you can write about places, activities and news that are relevant to the travel community.
			</p>
		</div>
	</div>

</div>
<script type="text/javascript" src="/js/jquery.characterCount.js"></script>	
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.charcount').characterCount(250);
		jQuery('.charcount').keyup();
	});
	
	var saveClicked = false;
	window.onbeforeunload = function(e){
		if( null != document.getElementById("description_tbl") && !saveClicked ){
			return "You might lose some unsaved text if you do so. You may want to save your article properly before navigating away from this page.";
		}
	}
</script>
