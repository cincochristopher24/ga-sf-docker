<?php 
	echo $contents['pFactory']->get_view()->render();
	$contents['subNavigation']->show(); 
?>
<div id="content_wrapper">
    <div id="composePanel" class="widePanel">
    	<form action="news.php?gID=<?php echo $_GET['gID'] ?>" method="post">
    		<ul class="form">	
    			<li>
    				<?php if(in_array(1,$contents['errCodes'])): ?>
    					<p class="error message"><strong>Please enter a title.</strong></p>
    				<?php endif;?>	
    				<label for="composeTitle">Title:</label>
    				<p class="error message" id="composeTitleErr"></p>
    				<input type="text" class="text" name="txtTitle" id="composeTitle" style="width:400px" value="<?php echo $contents['title'];?>" />
    			</li>								 																
    			<li>
    				<?php if(in_array(2,$contents['errCodes'])): ?>
    					<p class="error message"><strong>Please enter content.</strong></p>
    				<?php endif; ?>
    				<label for="txaAlertMessage">Content: </label>									
    				<textarea name="txaAlertMessage" id="txaAlertMessage" cols="50" rows="15"><?php echo $contents['content'];?></textarea>
    			</li>
    			<li class="checkbox">
    				<input type="checkbox" name="chkNotifyGroupMembers" id="chkNotifyGroupMembers" />
    				<label for="chkNotifyGroupMembers">
    					Notify Members?
    				</label>
    			</li>
    			<li class="checkbox">
    				<input type="checkbox" name="chkIncludeMsg" id="chkIncludeMsg" />
    				<label for="chkIncludeMsg">
    					Include news content in notification.
    				</label>
    			</li>
    			<li class="checkbox">
    				<input type="checkbox" name="chkEmphasize" id="chkEmphasize" />
    				<label for="chkEmphasize">
    					Emphasize this News.
    				</label>
    				<p class="supplement"></p>
    				<div id="expirationDateDiv" style="display: none">
    					<label for="txtExpirationDate">Expiration Date:</label><br />
    					<input type="text" name="txtExpirationDate" id="txtExpirationDate" class="text date-pick" value="<?php echo $contents['expiration'];?>" />
    					<p class="supplement">(yyyy-mm-dd)</p>
    				</div>	
    			</li>	
    			<li class="actions">
    				<input type="submit" value="Save" name="btnSaveNews" class="submit" />
    				<input type="hidden" name="hdnGroupID" value="<?php echo $contents['groupID']; ?>" />
    				<input type="hidden" name="hdnNewsID" value="<?php echo $contents['newsID']; ?>"
    			</li>
    		</ul>
    	</form>
    </div>
</div>