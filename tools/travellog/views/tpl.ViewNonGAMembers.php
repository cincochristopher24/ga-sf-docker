<div id="statusPanel2" class="widePanel" style="display: none;">
	<p class="loading_message">
		<span id="imgLoading">
		<img alt="Loading" src="/images/loading.gif"/>
		</span>
		<em id="statusCaption">Loading data please wait...</em>
	</p>
</div>

<div id = "invite_staff_form">
	<?php if ( sizeof($errors) ) : ?>
		<div class="errors">					
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= $error ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>

	<label style = "font-weight: bold; font-size: 11px;">Assign as Staff in</span> <span class = "required">*</span></label><strong></strong>
	<div id="scrolllst2">
		<ul class="form">
			<li>
				<?
					$arrCheckBox = array();
					foreach($subGroupList as $indList) :
				?>
				<div>
					<input
						type = "checkbox"
						name = "chk<?=$indList->getGroupID()?>"
						id = "chk<?=$indList->getGroupID()?>"
						value = "<?=$indList->getGroupID()?>" />
					<? $sgName = stripslashes($indList->getName()) ?>
					<?= (strlen($sgName) > 24) ? substr($sgName, 0, 24).'...' : $sgName ?>
				</div>
				<?
					$arrCheckBox[] = $indList->getGroupID();
					endforeach;
				?>
			</li>
			<li style = "margin-top: 30px">
				<? $lstCheckBox = implode(",", $arrCheckBox) ?>
				<form name = "frmNonMember" id = "frmNonMember">
					<input type = "hidden" name = "firstName" 	id = "firstName" 	value = "<?=$firstName?>" />
					<input type = "hidden" name = "lastName" 	id = "lastName" 	value = "<?=$lastName?>" />
					<input type = "hidden" name = "userName" 	id = "userName" 	value = "<?=$userName?>" />
					<input type = "hidden" name = "password" 	id = "password"	 	value = "<?=$password?>" />
					<input type = "hidden" name = "emailAdd" 	id = "emailAdd"	 	value = "<?=$emailAdd?>" />
					<input type = "hidden" name = "lstCheckBox" id = "lstCheckBox" 	value = "<?=$lstCheckBox?>" />
				</form>
			</li>
		</ul>
	</div>
	<div class="button">
	<input
			type = "button"
			name = "btnAssign"
			id = "btnAssign"
			value = "Assign"
			onclick = "AssignStaffManager._assignNonMember('gId=<?=$group->getGroupID()?>&action=AssignNonGA&email=<?=$emailAdd?>')" />
	</div>
	
	<div class="clear"> </div>
</div>