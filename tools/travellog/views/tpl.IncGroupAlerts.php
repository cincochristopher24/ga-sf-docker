<?php
/*
 * Created on Oct 20, 2006
 * Author: Czarisse Daphne P. Dolina
 * Tpl.IncGroupAlert.php - displays group alerts
 */
?>

<div class="section" id="alerts">
	<h2>Alerts	
		<span class="actions">	<? if (TRUE == $isAdminLogged) { ?><a href="/alertManagement.php?method=compose&amp;groupID=<?= $grpID ?>" class="add" >Add New Alert</a><? } ?></span>
	</h2>
	<div class="content">
		<ul>
			<?  for($idx=0;$idx<count($grpAlerts);$idx++){
					$eachalert = $grpAlerts[$idx];
			?>
				<li>
				<span class="destinations"><?= $eachalert->getCreated(); ?></span>
				<br />
				<?= $eachalert->getText(); ?>								
				</li>
					
			<? } ?>
			<? if (0 == count($grpAlerts) && $isAdminLogged) { ?>
				<li>There are no alerts added to this group.</li>
			<? } ?>
		</ul>
	</div>
</div>
