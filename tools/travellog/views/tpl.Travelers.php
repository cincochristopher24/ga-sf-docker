
<?php

/**
 * TODO: Ask Aldwin to add extra information to "Travelers in my Location" section
 */
// Added custom title & meta description - icasimpan Apr 24, 2007 
Template::setMainTemplateVar('title', 'Meet Interesting Travelers on the GoAbroad Network');
Template::setMainTemplateVar('metaDescription','Travelers with various interests constantly add their journeys to the Network. Browse the growing travelers list and meet interesting people.');
Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('layoutID', 'page_travelers');
Template::setMainTemplateVar('page_location', 'Travelers');
?>
<div class="area" id="intro">
	<div class="section">
		<h1>Travelers</h1>
		<div class="content">
			<? if(isset($obj_help)): ?>
				<p class="subheader"><?= $obj_help->getHelpText('TRAVELER-TAB-SUBHEAD')?></p>
				<?= HtmlHelpers::Textile($obj_help->getHelpText('TRAVELER-TAB')); ?>
			<? endif; ?>
		</div>
	</div>
</div>

<div class="area" id="top">
	<div id="search_and_results_area">
		<div class="wrapper_3">
			<div class="wrapper_2">
				<div class="wrapper">
					
					<div class="section" id="top_controls">
						<div class="content">
								<ul class="form" id="top_controls_filter">
									<li>
										<fieldset>
											<span>
												<label for="traveler">Show</label>
											</span>
											<span>
												<select name="action" id="action"onchange="redirect();">
													<option value="all" <?if( $viewType == 1 ){?>selected<?}?>>All Travelers</option>
													<? if($hometown_travelers_count && $isLogin): ?>
														<option value="inmyhometown" <?if( $viewType == 2 ){?>selected<?}?>>Travelers From My Hometown</option>
													<? endif; ?>
													<? if($newest_traveler_count): ?>
														<option value="newest" <?if( $viewType == 3 ){?>selected<?}?>>Most Recent Travelers</option>
													<? endif; ?>
													<option value="popular" <?if( $viewType == 4 ){?>selected<?}?>>Most Popular/Viewed Profile</option>
													<option value="lastlogin" <?if( $viewType == 5 ){?>selected<?}?>>By Last Login</option>
													<option value="location" <?if( $viewType == 6 ){?>selected<?}?>>By Location</option>
												</select>
											</span>
											<span id="COUNTRYSEARCH" <?if( $viewType != 6 ){?>style="display:none"<?}?>><?=$incLocation?></span>
										</fieldset>
									</li>
								</ul>
								<div class="clear"></div>
						</div>
					</div>
			
					<?=$mainContent?>
				</div>
			</div>
		</div>
	</div>
	
	<? if( count($obj_present_locations) ): ?>
		<div id="travelers_in_my_location">
			<div class="wrapper_3">
				<div class="wrapper_2">
					<div class="wrapper">
			
			
					<h1>Travelers in my location </h1>
					<div class="travelers_container">
						<? foreach($obj_present_locations as $obj_present_location): 
							$currTravelerID = $obj_present_location->getTravelerID(); 
							if ( $currTravelerID != $travelerID):
						?>
							<div class="traveler section">
								<div class="content">
									<div class="wrapper">
										<? if ( $isLogin ): ?>
											<div class="user_stat">
												<?
												$obj_pref = new PrivacyPreference( $currTravelerID ); 
												if ( $obj_present_location->isOnline() && $obj_pref->canViewOnlineStatus($travelerID) ):
												?>
												<img src="/images/orb_online.gif" border="0" alt="" title="online" />
												<? else: ?>
												<img src="/images/orb_offline.gif" border="0" alt="" title="offline" />
												<? endif; ?>
											</div>
										<? endif; ?>
										<a href="/profile.php?action=view&amp;travelerID=<?= $obj_present_location->getTravelerID() ?>"  title="View Profile" class="user_thumb"><img src="<?= HelperTraveler::displayTravelerPhotoSrc($obj_present_location) ?>" class="profile_photo_thumb" alt="<?= $obj_present_location->getUsername() ?>" height="37" width="37" /></a>
										<h2>
											<? if (strlen($obj_present_location->getUsername()) > 24): ?>
												<a href="/profile.php?action=view&amp;travelerID=<?=$obj_present_location->getTravelerID()?>" title="<?=$obj_present_location->getTravelerID()?>"><?= substr($obj_present_location->getUsername(),0, 23).'...' ?></a>
											<? else: ?>
												<a href="/profile.php?action=view&amp;travelerID=<?=$obj_present_location->getTravelerID()?>"><?=$obj_present_location->getUsername()?></a>
											<? endif; ?>
										</h2>
										<?php $travtype = $obj_present_location->getTravelerType() ?>
										<?php if ($travtype): ?>
											<p class="traveler_type">
												<?= $travtype ?> | 
												<?php
													$count_journals        = $obj_present_location->getCountTravelJournals();
													$count_journal_entries = $obj_present_location->getCountTravelLogs();
													$journals              = ( $count_journals > 1)? 'Journals':'Journal';
													$entries               = ( $count_journal_entries > 1)? 'Entries':'Entry';
												?>
												<?=$count_journals?>  <?=$journals ?> : <?=$count_journal_entries?> <?=$entries ?>
											</p>
										<?php endif; ?>
									</div>
									<div class="profile_info">
										<?= HtmlHelpers::truncateText($obj_present_location->getTravelerProfile()->getInterests(), 25) ?>
									</div>
									
									<div class="actions">
										<a href="/profile.php?action=view&amp;travelerID=<?=$currTravelerID?>" title="<?=$obj_present_location->getUsername()?>">view profile</a>
										<? if($isLogin && !$isAdvisor): ?>
											&nbsp;|&nbsp;<a href="#">send message</a>
											<? if (!$obj_friend->isFriend($currTravelerID) && !$obj_friend->isBlocked($currTravelerID) ):?>
											&nbsp;|&nbsp;<a href="/friend.php?action=request&amp;friendID=<?=$currTravelerID?>">request as friend</a>
											<?endif; ?>
										<? endif; ?>	
									</div>
									<div class="clear"></div>
								</div>
							</div>
						<?   endif;
							endforeach; ?>
						<div class="clear"></div>
					</div>
					
					</div>
				</div>
			</div>
		</div>
	<? endif; ?>

</div>

<div class="area" id="travelers_spotlight">
	
	<div id="search_travelers" class="section">
		<div class="wrapper">
			<h2><label for="keywords">Search</label></h2>
			<div class="content">
				<?= $search_template ?>
			</div>
		</div>
	</div>

	<div id="featured_and_most_popular" class="section">
		<div class="wrapper">
			<h2>Featured Travelers</h2>
			<div class="content">
				<ul class="users">
					<?if( count($arr_featured_travelers) ): ?>
						<? foreach( $arr_featured_travelers as $obj_featured_traveler ):?>
						<li> 
							<a class="user_thumb" href="profile.php?action=view&amp;travelerID=<?=$obj_featured_traveler->getTravelerID()?>"> 
							<img alt="User Profile Photo" src="<?= HelperTraveler::displayTravelerPhotoSrc($obj_featured_traveler) ?>" height="37" width="37" /> </a>
							
							<a class="username" href="profile.php?action=view&amp;travelerID=<?=$obj_featured_traveler->getTravelerID()?>"> 
								<? if (strlen($obj_featured_traveler->getUsername()) > 19): ?>
									<?= substr($obj_featured_traveler->getUsername(),0, 18).'...' ?>
								<? else: ?>
									<?=$obj_featured_traveler->getUsername()?>
								<? endif; ?>
							</a> <br/>
							<?if( $obj_featured_traveler->getTravelerProfile()->getHTLocationID() ):?> 
								<img src="http://images.goabroad.com/images/flags/flag<?=$obj_featured_traveler->getTravelerProfile()->getCity()->getCountry()->getCountryID()?>.gif" alt="<?=$obj_featured_traveler->getTravelerProfile()->getCity()->getCountry()->getName()?> flag" height="11" width="22"/>
							<?endif;?> 
						</li>
						<?endforeach;?>
					<?endif;?>
				</ul>
			</div>
			
			
			
			<h2 class="pop">Popular Travelers</h2>
			<div class="content">
				<ul class="users">
					<?if( count($arr_popular_travelers) ):?>
						<? foreach( $arr_popular_travelers as $obj_popular_travelers ):?>
							<li> 
								<a class="user_thumb" href="profile.php?action=view&amp;travelerID=<?=$obj_popular_travelers->getTravelerID()?>"> 
								<img alt="User Profile Photo" src="<?= HelperTraveler::displayTravelerPhotoSrc($obj_popular_travelers) ?>" height="37" width="37" /> </a>
								
								<a class="username" href="profile.php?action=view&amp;travelerID=<?=$obj_popular_travelers->getTravelerID()?>">
									<? if (strlen($obj_popular_travelers->getUsername()) > 19): ?>
										<?= substr($obj_popular_travelers->getUsername(),0, 18).'...' ?>
									<? else: ?>
										<?=$obj_popular_travelers->getUsername()?>
									<? endif; ?>
								</a> <br/>
								<?if( $obj_popular_travelers->getTravelerProfile()->getHTLocationID() ):?>
									<img src="http://images.goabroad.com/images/flags/flag<?=$obj_popular_travelers->getTravelerProfile()->getCity()->getCountry()->getCountryID()?>.gif" alt="<?=$obj_popular_travelers->getTravelerProfile()->getCity()->getCountry()->getName()?> flag" height="11" width="22"/>
								<?endif;?> 
							</li>
						<?endforeach;?>
					<?endif;?>
				</ul>
			</div>
		</div>
	</div>
	
	
</div>


<div class="clear"></div>
