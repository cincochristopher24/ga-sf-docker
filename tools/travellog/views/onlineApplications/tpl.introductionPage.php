<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if($groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
	//	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
?>





<div class="area" id="intro">
	<h2 class="border_bottom_solid">Study Abroad Program Information</h2>
	<div class="section_detail">
		<div class="content paddedOnTop20">
			<h3>Here’s how the CIS On-line Application works:</h3>
			<label><strong style="font-size:15px;">Step 1:</strong> Create an account - if you are reading this you have already done that!</label>
			<ul class="disc">
			</ul>
			<label><strong style="font-size:15px;">Step 2:</strong> Complete the online application</label>
			<ul class="disc">
				<li>Personal Information - all about you</li>
				<li>Emergency Contact Information - who to call</li>
				<li>Program Information - when/where you're going</li>
				<li>Personal Statement - tell us about your goals</li>
				<li>Agreement & Waiver - read, sign and date</li>
			</ul>
			<label><strong style="font-size:15px;">Step 3:</strong> Pay the online application fee</label>
			<ul class="disc">
			</ul>
			<label><strong style="font-size:15px;">Step 4:</strong> Submit additional documents to complete your application</label>
			<ul class="disc">
				<li>Course Selection Worksheet</li>
				<li>Academic Reference</li>
				<li>Official Transcript</li>
				<li>CIS Scholarship Application (optional)</li>
			</ul>
			<h3>What you will need:</h3>
			<ul class="disc">
				<li>Credit Card</li>
				<li>Current GPA</li>
				<li>Name, term, and year for the program to which you are applying</li>
				<li>Passport (if you have one)</li>
			</ul>
			<h3>Helpful Hints</h3>
			<p>
				<strong>Be sure to consult with your study abroad office and/or academic advisor before applying.</strong> They may have their own application process or requirements that you need to follow. They can also provide guidance on getting your chosen study abroad program approved for credit transfer and financial aid, as well as other helpful information about working with your home university or college.
			</p>
			<p>
				<strong>Research your program.</strong> Be sure to research the program you’d like to attend at www.studyabroad-cis.com to learn about the destination, application deadlines, eligibility requirements, dates, and more. Not sure which program is a good fit for you? Call us toll-free at 877-617-9090 or email us at info@cisabroad.com. We’re happy to help!
			</p>
			<p>
				<strong>Find out what happens next!</strong> Read the Study Abroad Checklist (<a href="/cis/documents/study_abroad_checklist.pdf">download pdf</a>) to get an overview of what happens between when you apply and when you get on the plane.
			</p>
			<p>
				<strong>Familiarize yourself with the nitty-gritty details.</strong> Read up on CIS Policies and Procedures (download <a href="/cis/documents/CIS_semester_abroad_policies_procedures.pdf">Semester Abroad Policies pdf</a>, 
				<a href="/cis/documents/CIS_short_term_program_policies_procedures.pdf">Short Term Policies pdf</a>) so that you understand our refund policy, deadlines, procedures, expectations, etc.
			</p>
			<p>
				<strong>The importance of saving your application.</strong> Don’t forget to save your application periodically so that you can return to it at any time.
			</p>
			
			
			<div class="wrap paddedOnTop20 right border_top_dotted">				
				<a class="button_v3" href="?step=1&action=view">
					<strong>Continue your Application</strong>
				</a>
			<?/*	<a class="button_v3" href="?action=viewChecklist">
					<strong>View Checklist</strong>
				</a> */ ?>
			</div>
		</div>
	</div>
</div>