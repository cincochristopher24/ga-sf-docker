<?php
require_once('Class.HtmlHelpers.php');
require_once('travellog/model/Class.Country.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');

$country = new Country($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getCountryBirthPlace());
$yrInSchool = array('Freshman', 'Sophomore', 'Junior', 'Senior', 'College/University Graduate', 'High School Graduate');
$priorityTxt = array('','Program 1 is my first choice, and Program 2 is my second choice.', 'I am equally interested in both Program 1 and Program 2.');



$name         = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getName();
$fname        = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName();
$lname        = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName();
$preferred    = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getPreferredName();
$gender       = (1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getGender()) ? 'Male' : 'Female';
$bday         = Date("F d, Y", strtotime($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBirthday()));
$citizenship  = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getCitizenship();
$bplace       = $country->getName();
$ss           = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getSecNumber();
$email        = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail();
$email2       = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getAltEmail();
$street       = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1();
$city         = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1();
$state        = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1();
$zip          = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1();
$permanent    = (1 == $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPermanentAddress()) ? 'Yes': 'No';
$pstreet       = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet2();
$pcity         = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity2();
$pstate        = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState2();
$pzip          = ('Yes' == $permanent) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip2();
$phone        = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1();
$phone2        = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber2();
$cellPhone    = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCellphone();
$passport     = isset($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]->getPassportNumber() : '';
$expiry       = isset($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]) ? Date('Y-m',strtotime($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]->getExpiryDate())) : '';
$eName        = $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getName();
$efName        = $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getFirstName();
$elName        = $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getLastName();
$eRel         = $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getRelationship();
$eStreet      = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getStreet1();
$eCity        = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getCity1();
$eState       = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getState1();
$eZip         = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getZip1();
$ePhone       = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getPhoneNumber1();
$needs        = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getSpecialNeeds();
$sName        = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName();
$sMajor       = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMajor();
$sMinor       = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMinor();
$yrInSchool   = $yrInSchool[$objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getYearInSchool()];
$average      = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getGenAverage();
$courses      = $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getCurrentCourses();
$learn        = (0 < $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom() ? CISOnlineAppConst::$addInfoOptKeys[$objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom()] : "");
$learnOther   = $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers();
$term         = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTermName();
$yr           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear();
$p1           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramName();
$o1           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionName();
$p2           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondProgramName();
$o2           = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondOptionName();
$priority     = $priorityTxt[$objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getPriorityOption()];
$fullYr       = $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getInterest();
$volunteer    = (2 == $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getVolunteering() ? 'Yes' : 'No');
$statement1   = addslashes($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement1());
$statement2   = addslashes($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement2());
$statement3   = addslashes($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement3());

$payingPersons = (isset($objArr[CISOnlineAppFieldsMapper::BILLING_INFO]) ? $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getPayingPersons() : 0); 

$yourSelf = (($yourSelf = $payingPersons & 1) == 1) ? 'You' : '';
$parents  = (($parents  = $payingPersons & 2) == 2) ? 'Parent' : '';
$aid      = (($aid      = $payingPersons & 4) == 4) ? 'Aid' : '';

$pay       = $yourSelf.' '.$parents.' '.$aid;
$bName     = $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getMainBillingContact();
$bfName    = $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getFirstName();
$blName    = $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getLastName();

$bRel      = $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getRelationship();
$bStreet   = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getStreet1();
$bCity     = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getCity1();
$bState    = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getState1();
$bZip      = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getZip1();
$bPhone    = $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getPhoneNumber1();
$bEmail    = $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getEmail();

$dateApplied = Date("Y-M-d", strtotime($objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getApplicationDate()));
$linebreak = chr(13).chr(10);
?>
"CIS Type","Contact Type","Contact Sub Type","Status","Status Programs","Date of Application","First Name","Last Name","Preferred Name","Sex","Birthdate","Citizenship","Country of Birth","SS#","Email","Email #2","Current Address1","Current City","Current State","Current Zip","Current Phone","School/Company ","First Name Repeat","Last Name Repeat","Permanent Street Address","Permanent City","Permanent State","Permanent Zip","Permanent Phone","Cellphone","PassportNumber","Passport Expiration Date","Emergency First Name","Emergency Last Name","Emergency Contact Relationship","Emergency Address","Emergency City","Emergency State","Emergency Zip","Emergency Phone #1","Special needs","School/Company","Field of study","Minor","Current year in school","Current GPA","Courses not on Transcript","Inquiry Source","Other","First Name Repeat","Last Name Repeat","School/Company Repeat","Email Repeat","Program Term","Program Year","Contact Year","Contact Term","Programs","Program Option","Program2","Option2","Program Priority","Interested in Full Year","Interested in Green Volunteer","Academic/Career Goals","Cultural Understanding","Personal Growth","Paying Bill","Billing First Name","Billing Last Name","Billing Contact Relationship","Billing Address","Billing City","Billing State","Billing Zip","Billing Phone #1","Billing Email","Date of Application","First Name Repeat","Last Name Reppeat","Email Repeat","School/Company Repeat"
"CIS","Student","Study Abroad","Online Applicant","Online Applicant","<?=$dateApplied?>","<?=$fname?>","<?=$lname?>","<?=$preferred?>","<?=$gender?>","<?=$bday?>","<?=$citizenship?>","<?=$bplace?>","<?=$ss?>","<?=$email?>","<?=$email2?>","<?=$street?>","<?=$city?>","<?=$state?>","<?=$zip?>","<?=$phone?>","<?=$sName?>","<?=$fname?>","<?=$lname?>","<?=$pstreet?>","<?=$pcity?>","<?=$pstate?>","<?=$pzip?>","<?=$phone2?>","<?=$cellPhone?>","<?=$passport?>","<?=$expiry?>","<?=$efName?>","<?=$elName?>","<?=$eRel?>","<?=$eStreet?>","<?=$eCity?>","<?=$eState?>","<?=$eZip?>","<?=$ePhone?>","<?=$needs?>","<?=$sName?>","<?=$sMajor?>","<?=$sMinor?>","<?=$yrInSchool?>","<?=$average?>","<?=$courses?>","<?=$learn?>","<?=$learnOther?>","<?=$fname?>","<?=$lname?>","<?=$sName?>","<?=$email?>","<?=$term?>","<?=$yr?>","<?=$yr?>","<?=$term?>","<?=$p1?>","<?=$o1?>","<?=$p2?>","<?=$o2?>","<?=$priority?>","<?=$fullYr?>","<?=$volunteer?>","<?=$statement1?>","<?=$statement2?>","<?=$statement3?>","<?=$pay?>","<?=$bfName?>","<?=$blName?>","<?=$bRel?>","<?=$bStreet?>","<?=$bCity?>","<?=$bState?>","<?=$bZip?>","<?=$bPhone?>","<?=$bEmail?>","<?=$dateApplied?>","<?=$fname?>","<?=$lname?>","<?=$email?>","<?=$sName?>"
