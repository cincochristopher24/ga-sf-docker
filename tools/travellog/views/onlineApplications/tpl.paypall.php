<?php
	$sess = SessionManager::getInstance();
	$sess->unsetVar('onlineAppUserID');
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if($groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
	//	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
?>


<div class="area" id="intro">
	<h2 class="border_bottom_solid"></h2>
	<div class="section_detail">
		<div class="content paddedOnTop20">
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="8552568">
			<table>
			<tr><td><input type="hidden" name="on0" value="Application Fees">Application Fees</td></tr><tr><td><select name="os0">
			                <option value="Semester Program Application Fee">Semester Program Application Fee $50.00
			                <option value="Short-term Program Application Fee">Short-term Program Application Fee $50.00
			                <option value="Summer Internship Application Fee (non CPA schools)">Summer Internship Application Fee (non CPA schools) $150.00
			                <option value="Summer Internship Application Fee for CPA School">Summer Internship Application Fee for CPA School $100.00
			</select> </td></tr>
			</table>
			<input type="hidden" name="currency_code" value="USD">
			<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
		</div>
	</div>
</div>