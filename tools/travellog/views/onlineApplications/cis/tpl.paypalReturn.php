<?php
	//$sess = SessionManager::getInstance();
	//$sess->unsetVar('onlineAppUserID');

	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	//ob_start();
?>

<div class="area" id="intro">
	<h2 class="border_bottom_solid">Thanks for starting your application with CISabroad! </h2>
	<div class="section_detail">
		<div class="content paddedOnTop20">
				<p> If there’s anything we can do to help you complete your application, we are eager to be of assistance. Feel free to email us at <a href="mailto: info@cisabroad.com"> <strong> info@cisabroad.com</strong></a> or give us a call at 877-617-9090! </p>
                <p> To pick up where you left off and complete your application at a later time, please go to the <a href ="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=V7QF7GGUDH34Y" target ="_blank"><strong>Deposit Payment Page</strong></a> and bookmark it in your browser </p>
                <p> In the mean time, if you would like to work on the other parts of your application, please download the application instructions at the <a href ="http://www.cisabroad.com/apply-now"><strong> CIS Apply Now</strong></a>  page. </p>
                <!--
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target ="_blank">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="V7QF7GGUDH34Y">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
                -->
                
                <p> Please get your credit card out and click on <a href ="https://www.paypal.com/us/cgi-bin/webscr?cmd=_flow&SESSION=UAE7a7E82wtc8SQynZBlN7_BIHVB9A7t0O9sv2bp_pf9gNgc_729HWHf4lW&dispatch=50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b081988562bf19d61623c669b34e5cd175ba4a" target ="_blank" > Pay Now</a> to complete your online application. </p>
        </div>
	</div>
    
</div>



