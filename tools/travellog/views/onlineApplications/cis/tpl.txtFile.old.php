<?php
	require_once('Class.HtmlHelpers.php');
	require_once('travellog/model/Class.Country.php');
	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');

	$sp = " ";
	$spFeed1 = 40;
	$spFeed2 = 8;
	$linebreak = chr(13).chr(10);
	//$linebreak = "\n";
	//var_dump($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getCountryBirthPlace());
	$county = new Country($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getCountryBirthPlace());
//	$yrInSchool = array('Freshman', 'Sophomore', 'Junior', 'Senior');
	$yrInSchool = array('1' => 'Freshman', '2' => 'Sophomore', '3' => 'Junior', '4' => 'Senior', '5' => 'College/University Graduate', '6' => 'High School Graduate');
	$priorityTxt = array('','Program 1 is my first choice, and Program 2 is my second choice.', 'I am equally interested in both Program 1 and Program 2.');
?>
<?=$linebreak?>
<?=$linebreak?>
----------------------------------------------<?=$linebreak?>
Personal Information<?=$linebreak?>
----------------------------------------------<?=$linebreak?>
<?=$linebreak?>
Name<?=str_repeat($sp,$spFeed1-strlen('Name'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getName(); ?> <?=$linebreak?>
Preferred Name<?=str_repeat($sp,$spFeed1-strlen('Preferred Name'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getPreferredName(); ?> <?=$linebreak?>
Gender<?=str_repeat($sp,$spFeed1-strlen('Gender'))?>:<?=str_repeat($sp,$spFeed2)?><?=$gender = (1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getGender()) ? 'Male' : 'Female'; ?> <?=$linebreak?>
Birthdate<?=str_repeat($sp,$spFeed1-strlen('Birthdate'))?>:<?=str_repeat($sp,$spFeed2)?><?=Date("F d, Y", strtotime($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBirthday())); ?> <?=$linebreak?>
Citizenship<?=str_repeat($sp,$spFeed1-strlen('Citizenship'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getCitizenship(); ?> <?=$linebreak?>
Country of Birth<?=str_repeat($sp,$spFeed1-strlen('Country of Birth'))?>:<?=str_repeat($sp,$spFeed2)?><?=$county->getName()?> <?=$linebreak?>
Social Security Number<?=str_repeat($sp,$spFeed1-strlen('Social Security Number'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getSecNumber(); ?> <?=$linebreak?>
Email Address<?=str_repeat($sp,$spFeed1-strlen('Email Address'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail(); ?> <?=$linebreak?>
Alternate Email<?=str_repeat($sp,$spFeed1-strlen('Alternate Email'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getAltEmail(); ?> <?=$linebreak?>
---Address---<?=$linebreak?>
Street Address<?=str_repeat($sp,$spFeed1-strlen('Street Address'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1(); ?> <?=$linebreak?>
City<?=str_repeat($sp,$spFeed1-strlen('City'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1(); ?> <?=$linebreak?>
State<?=str_repeat($sp,$spFeed1-strlen('State'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1(); ?> <?=$linebreak?>
Zip<?=str_repeat($sp,$spFeed1-strlen('Zip'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1(); ?> <?=$linebreak?>
Phone<?=str_repeat($sp,$spFeed1-strlen('Phone'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1(); ?> <?=$linebreak?>
Cellphone<?=str_repeat($sp,$spFeed1-strlen('Cellphone'))?>:<?=str_repeat($sp,$spFeed2)?><?=$objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCellphone();?><?=$linebreak?>
Permanent Address<?=str_repeat($sp,$spFeed1-strlen('Permanent Address'))?>:<?=str_repeat($sp,$spFeed2)?><?=$permanent = (1 == $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPermanentAddress()) ? 'Yes': 'No'; ?> <?=$linebreak?>
Permanent Street<?=str_repeat($sp,$spFeed1-strlen('Permanent Street'))?>:<?=str_repeat($sp,$spFeed2)?><?=  ($permanent == 'Yes') ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet2(); ?> <?=$linebreak?>
Permanent City<?=str_repeat($sp,$spFeed1-strlen('Permanent City'))?>:<?=str_repeat($sp,$spFeed2)?><?= ($permanent == 'Yes') ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity2(); ?> <?=$linebreak?>
Permanent State<?=str_repeat($sp,$spFeed1-strlen('Permanent State'))?>:<?=str_repeat($sp,$spFeed2)?><?= ($permanent == 'Yes') ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState2(); ?> <?=$linebreak?>
Permanent Zip<?=str_repeat($sp,$spFeed1-strlen('Permanent Zip'))?>:<?=str_repeat($sp,$spFeed2)?><?= ($permanent == 'Yes') ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip2(); ?> <?=$linebreak?>
Permanent Phone<?=str_repeat($sp,$spFeed1-strlen('Permanent Phone'))?>:<?=str_repeat($sp,$spFeed2)?><?= ($permanent == 'Yes') ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1() : $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber2(); ?> <?=$linebreak?>
---Passport Details---<?=$linebreak?>
Passport<?=str_repeat($sp,$spFeed1-strlen('Passport'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]->getPassportNumber() : 'n/a' ?> <?=$linebreak?>
Passport Expiry<?=str_repeat($sp,$spFeed1-strlen('Passport Expiry'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]) ? Date('Y-m',strtotime($objArr[CISOnlineAppFieldsMapper::PASSPORT_INFO]->getExpiryDate())) : 'n/a' ?> <?=$linebreak?>
---Emergency Contact Information--- <?=$linebreak?>
Name<?=str_repeat($sp,$spFeed1-strlen('Name'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]) ? $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getName() : 'n/a' ?><?=$linebreak?>
Relation<?=str_repeat($sp,$spFeed1-strlen('Relation'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]) ? $objArr[CISOnlineAppFieldsMapper::EMERGENCY_CONTACT]->getRelationship() : 'n/a' ?><?=$linebreak?>
Street Address<?=str_repeat($sp,$spFeed1-strlen('Street Address'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getStreet1() : 'n/a' ?><?=$linebreak?>
City<?=str_repeat($sp,$spFeed1-strlen('City'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getCity1() : 'n/a' ?><?=$linebreak?>
State<?=str_repeat($sp,$spFeed1-strlen('State'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getState1() : 'n/a' ?><?=$linebreak?>
Zip<?=str_repeat($sp,$spFeed1-strlen('Zip'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getZip1() : 'n/a' ?><?=$linebreak?>
Phone<?=str_repeat($sp,$spFeed1-strlen('Phone'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][1]->getPhoneNumber1() : 'n/a' ?><?=$linebreak?>
<?=$linebreak?>
Do you have any special needs that CIS should
be aware of when placing you in a program?<?=$linebreak?>
<?=$linebreak?>
<?=isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getSpecialNeeds() : 'n/a' ?><?=$linebreak?>
<?=$linebreak?>
----------------------------------------------<?=$linebreak?>
School Information<?=$linebreak?>
----------------------------------------------<?=$linebreak?>
<?=$linebreak?>
Name of school you currently attend<?=str_repeat($sp,$spFeed1-strlen('Name of school you currently attend'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName() : 'n/a' ?> <?=$linebreak?>
School State<?=str_repeat($sp,$spFeed1-strlen('School State'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getStateAbbr() : 'n/a' ?> <?=$linebreak?>
Major<?=str_repeat($sp,$spFeed1-strlen('Major'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMajor() : 'n/a' ?> <?=$linebreak?>
Minor<?=str_repeat($sp,$spFeed1-strlen('Minor'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMinor() : 'n/a' ?> <?=$linebreak?>
Year in School<?=str_repeat($sp,$spFeed1-strlen('Year in School'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $yrInSchool[$objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getYearInSchool()] : 'n/a' ?> <?=$linebreak?>
Current Cumulative GPA<?=str_repeat($sp,$spFeed1-strlen('Current Cumulative GPA'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getGenAverage() : 'n/a' ?> <?=$linebreak?>
Courses from current semester<?=str_repeat($sp,$spFeed1-strlen('Courses from current semester'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getCurrentCourses() : 'n/a' ?> <?=$linebreak?>
<?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
Additional Information <?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
<?=$linebreak?>
How did you learn about the CIS Program you're applying for?<?=$linebreak?>
<?=$linebreak?>
<?=isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? 
		($learn = (0 < $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom() 
			? CISOnlineAppConst::$addInfoOptKeys[$objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom()] 
			: $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers())) 
		: 'n/a' 
?><?=$linebreak?>
<?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
Study Abroad Program Information <?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
<?=$linebreak?>
Term Applying for<?=str_repeat($sp,$spFeed1-strlen('Term Applying for'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTermName() : 'n/a' ?> <?=$linebreak?>
Year Applying for<?=str_repeat($sp,$spFeed1-strlen('Year Applying for'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear() : 'n/a' ?> <?=$linebreak?>
Program 1<?=str_repeat($sp,$spFeed1-strlen('Program 1'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramName() : 'n/a'; ?> <?=$linebreak?>
Option 1<?=str_repeat($sp,$spFeed1-strlen('Option 1'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionName() : 'n/a' ?> <?=$linebreak?>
Program 2<?=str_repeat($sp,$spFeed1-strlen('Program 2'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondProgramName() : 'n/a';?> <?=$linebreak?>
Option 2<?=str_repeat($sp,$spFeed1-strlen('Option 2'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondOptionName() : 'n/a' ?> <?=$linebreak?>
Priority<?=str_repeat($sp,$spFeed1-strlen('Priority'))?>:<?=str_repeat($sp,$spFeed2)?><?=$priorityTxt[$objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getPriorityOption()];?><?=$linebreak?>
<?=$linebreak?>
Are you interested in studying abroad for a full academic year?<?=$linebreak?>
<?=isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getInterest() : 'n/a' ?><?=$linebreak?>
<?=$linebreak?>
Are you interested in volunteering while abroad? <?=(isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $volunteer = (2 == $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getVolunteering() ? 'Yes' : 'No') : 'n/a' )?> <?=$linebreak?>
<?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
Personal Statement <?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
<?=$linebreak?>

Submit Personal Statement Later : <?=isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]) ? (1 == $objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getLateOption() ? 'Yes' : '') : 'n/a' ?> <?=$linebreak?>
<?=$linebreak?>
---How does the content of the study abroad program you are applying to relate
to your present and future academic and career goals?<?=$linebreak?>
<?=$linebreak?>
<?=isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]) ? addslashes($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement1()) : 'n/a' ?> <?=$linebreak?>
<?=$linebreak?>
---List and explain four goals relating to academics and/or cultural understanding you hope to achieve during your study abroad experience.<?=$linebreak?>
<?=isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]) ? addslashes($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement2()) : 'n/a' ?> <?=$linebreak?>
<?=$linebreak?>
---List and explain four expectations for your own personal growth/change you hope to experience as a result of your participation in this study abroad program.<?=$linebreak?>
<?=isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]) ? addslashes($objArr[CISOnlineAppFieldsMapper::PERSONAL_STATEMENT]->getStatement3()) : 'n/a' ?> <?=$linebreak?>
<?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
Billing and Payment <?=$linebreak?>
---------------------------------------------- <?=$linebreak?>
<?=$linebreak?>
<?php 
	$payingPersons = (isset($objArr[CISOnlineAppFieldsMapper::BILLING_INFO]) ? $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getPayingPersons() : 0); 
	/***
	$yourSelf = (($yourSelf = $payingPersons & 1) == 1) ? 'You' : '';
	$parents  = (($parents  = $payingPersons & 2) == 2) ? 'Parent' : '';
	$aid      = (($aid      = $payingPersons & 4) == 4) ? 'Aid' : '';
	***/
	$payers = array();
	if(($yourSelf = $payingPersons & 1) == 1) $payers[] = 'You';
	if(($yourSelf = $payingPersons & 2) == 2) $payers[] = 'Parent';
	if(($yourSelf = $payingPersons & 4) == 4) $payers[] = 'Aid';
?>
Who will be paying your fees?<?=str_repeat($sp,($spFeed1-strlen('Who will be paying your fees?'))-1)?> <?php echo implode(', ',$payers) ?>
<?=$linebreak?>
<?=$linebreak?>
Name to whom invoices should be sent/billing contact<?=$linebreak?>
<?=$linebreak?>
<?=$objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getFirstName() .' '. $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getLastName();?>
<?=$linebreak?>
Relation<?=str_repeat($sp,$spFeed1-strlen('Relation'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::BILLING_INFO]) ? $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getRelationship() : 'n/a' ?> <?=$linebreak?>
Street Address<?=str_repeat($sp,$spFeed1-strlen('Street Address'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getStreet1() : 'n/a' ?> <?=$linebreak?>
City<?=str_repeat($sp,$spFeed1-strlen('City'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getCity1() : 'n/a' ?> <?=$linebreak?>
State<?=str_repeat($sp,$spFeed1-strlen('State'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getState1() : 'n/a' ?> <?=$linebreak?>
Zip<?=str_repeat($sp,$spFeed1-strlen('Zip'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getZip1() : 'n/a' ?> <?=$linebreak?>
Phone<?=str_repeat($sp,$spFeed1-strlen('Phone'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][2]->getPhoneNumber1() : 'n/a' ?> <?=$linebreak?>
Email<?=str_repeat($sp,$spFeed1-strlen('Email'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::BILLING_INFO]) ? $objArr[CISOnlineAppFieldsMapper::BILLING_INFO]->getEmail() : 'n/a' ?> <?=$linebreak?>
<?=$linebreak?>
Date of Application<?=str_repeat($sp,$spFeed1-strlen('Date of Application'))?>:<?=str_repeat($sp,$spFeed2)?><?=isset($objArr[CISOnlineAppFieldsMapper::AGREEMENT]) ? Date("Y-M-d", strtotime($objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getApplicationDate())) : 'n/a' ?> <?=$linebreak?>
<?=$linebreak?>
<?=$linebreak?>
Additional Comments:
<?=$linebreak?>
<?=addslashes($comment)?><?=$linebreak?>
<?=$linebreak?>
<?=$linebreak?>
Last Change Date<?=str_repeat($sp,$spFeed1-strlen('Last Change Date'))?>:<?=str_repeat($sp,$spFeed2)?><?=Date('d M Y')?>