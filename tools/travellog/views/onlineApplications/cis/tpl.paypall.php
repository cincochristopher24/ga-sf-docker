<?php
	//$sess = SessionManager::getInstance();
	//$sess->unsetVar('onlineAppUserID');

	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	//ob_start();
?>


<div class="area" id="intro">


	<h3 class="border_bottom_solid">$200 Application Deposit</h2>
	<div class="section_detail">
            
		<div class="content paddedOnTop20">
            
            
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target ="_blank">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="V7QF7GGUDH34Y">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>
            
            <!-- 
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target = "_blank">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="V7QF7GGUDH34Y">
			<table>
			<tr><td><input type="hidden" name="on0" value="Student Name (Required)">Student Name (Required)</td></tr><tr><td><input type="text" name="os0" maxlength="60"></td></tr>
			</table>
			<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>	
            -->

			<p>When you Pay your application deposit, your application will automatically be submitted. </p>
			<p>You will hear from a CIS staff member soon regarding next steps. If you would like to begin working immediately on these steps,  please pay the deposit now, and then follow the application instructions to complete and submit your remaining application materials.</p>

			<p>Read up on CIS Policies and Procedures (download <a href="http://www.cisabroad.com/files/cis_semester_abroad_policies_procedures.pdf">Semester Abroad Policies pdf</a>,
				<a href="http://www.cisabroad.com/files/cis_short_term_program_policies_procedures.pdf">Short Term Policies pdf</a>) so that you understand our refund policy, deadlines, procedures, expectations, etc.
			</p>
		</div>
	</div>
</div>