<?php
	Template::setMainTemplateVar('title', $pageTitle);

	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
	Template::includeDependentCss('/min/f=css/jquery.autocomplete.css');
	Template::includeDependent('<style type="text/css">.form li {margin-bottom:0}</style>');
	Template::includeDependentJs('/js/prototype.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator-1.1.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');

	Template::includeDependentJs('/js/jquery.autocomplete.js');
	ob_start();
	$remove_fields = true;

$incjs = <<<BOF
<script type="text/javascript">
	//<![CDATA[
	function showPAddressForm(){

		var divStyle = $('permanentAddress').style;

		if(divStyle.display == 'none' && $('n').checked == true){
			jQuery('#permanentAddress').slideDown("slow");

		}
		else if(divStyle.display == 'block' && $('y').checked == true) {
			var parent = $('permanentAddress');
			var inputChildren = parent.getElementsByTagName("input");
			var txtareaChild = parent.getElementsByTagName("textarea");
			var valIsEmpty = true;
			var a = 0;
			for(var i = 0; i < inputChildren.length; i++){
				if(inputChildren[i].getAttribute('value') != ''){
					a++;
					if(a==1){
						var ans = confirm('This will erase all your entries in the permanent address section. Click Ok to continue.');
						if(!ans){
							$('n').checked = true;
							valIsEmpty = false;
							break;
						}
					}

					inputChildren[i].setAttribute('value', '');
					txtareaChild[0].innerHTML = '';
				}

			}

		//	jQuery('#permanentAddress input').each(function(){
		//		var child = $(this);
		//		var val = $(this).val();
		//		alert(val);
		//	});
			if(valIsEmpty == true){
				jQuery('#permanentAddress').slideUp("slow");
			}

		}

	}

	function showSpecifyField(){
		var refererOpt = $('referer');
		var selectedRef = refererOpt.options[refererOpt.options.selectedIndex].value;
		if(selectedRef == '' || selectedRef == 0){
			jQuery('#specifyField').slideDown("slow");
		}
		else {
			jQuery('#specifyField').slideUp("slow");
		}
	}
	/***
	function activateOtherSchool(){
		var schoolOpt = $('school');
		var selectedSchoolVal = schoolOpt.options[schoolOpt.options.selectedIndex].value;
		if(selectedSchoolVal == '0'){
			$('otherSchools').style.display = '';
		}
		else {
			$('school1').value = '';
			$('otherSchools').style.display = 'none';
		}
	}
	***/

	function disableHighSchoolCheckbox(){
		var chckHighschool = $('chckHighSchool');

		if(chckHighschool.checked == true){
			chckHighschool.checked = false;
		}
	}

	function enableOtherSchoolCheckbox(){

		var chckOtherSchool = $('chckOtherSchool');
		if(chckOtherSchool.checked == false){
			chckOtherSchool.checked = true;
		}
	}

	function disableOtherSchool(){


		var chckHighschool = $('chckHighSchool');
		var chckOtherSchool = $('chckOtherSchool');

		var txtOtherSchool = $('school1');
		//var optSchool = $('school');
		var otherSchools = $('otherSchools');

		var yearOpt = $('year');

		var defaulYearInSchool = parseInt(jQuery('#yearInSchool').val()) - 1;


		if(chckHighschool.checked == true || chckOtherSchool.checked == true){
			if(otherSchools.style.display == 'none'){
				jQuery('#otherSchools').slideDown("slow");
			}
			$('schoolX').disabled = true;

			yearOpt.options.length = 0;

			if(chckHighschool.checked == false){
				//alert('other school');
				jQuery('#majorAndMinor').slideDown('slow');

				yearOpt.options[yearOpt.options.length] = new Option('Freshman','1');
				yearOpt.options[yearOpt.options.length] = new Option('Sophomore','2');
				yearOpt.options[yearOpt.options.length] = new Option('Junior','3');
				yearOpt.options[yearOpt.options.length] = new Option('Senior','4');
				yearOpt.options[yearOpt.options.length] = new Option('College/University Graduate','5');
				yearOpt.options[yearOpt.options.length] = new Option('High School Graduate','6');

				yearOpt.options[defaulYearInSchool].selected = true;
			}
			else {

			//	chckOtherSchool.checked = true;



				jQuery('#majorAndMinor').slideUp('slow');
				yearOpt.options[yearOpt.options.length] = new Option('Freshman','1');
				yearOpt.options[yearOpt.options.length] = new Option('Sophomore','2');
				yearOpt.options[yearOpt.options.length] = new Option('Junior','3');
				yearOpt.options[yearOpt.options.length] = new Option('Senior','4');


				if(defaulYearInSchool < 5){
					yearOpt.options[defaulYearInSchool].selected = true;
				}
			}

		//	checkCheckedSchoolOption();


			txtOtherSchool.disabled = false;
		}
	/*	else if(chckOtherSchool.checked == true){
			if(otherSchools.style.display == 'none'){
				jQuery('#otherSchools').slideDown("slow");
			}
			$('schoolX').disabled = true;
			txtOtherSchool.disabled = false;
			yearOpt.options.length = 0;
			jQuery('#majorAndMinor').slideDown('slow');

			yearOpt.options[yearOpt.options.length] = new Option('Freshman','1');
			yearOpt.options[yearOpt.options.length] = new Option('Sophomore','2');
			yearOpt.options[yearOpt.options.length] = new Option('Junior','3');
			yearOpt.options[yearOpt.options.length] = new Option('Senior','4');
			yearOpt.options[yearOpt.options.length] = new Option('College/University Graduate','5');
			yearOpt.options[yearOpt.options.length] = new Option('High School Graduate','6');

			yearOpt.options[defaulYearInSchool].selected = true;

		}*/

		else {
			jQuery('#otherSchools').slideUp("slow");
			$('schoolX').disabled = false;
			jQuery('#majorAndMinor').slideDown('slow');

			yearOpt.options.length = 0;
			yearOpt.options[yearOpt.options.length] = new Option('Freshman','1');
			yearOpt.options[yearOpt.options.length] = new Option('Sophomore','2');
			yearOpt.options[yearOpt.options.length] = new Option('Junior','3');
			yearOpt.options[yearOpt.options.length] = new Option('Senior','4');
			yearOpt.options[yearOpt.options.length] = new Option('College/University Graduate','5');
			yearOpt.options[yearOpt.options.length] = new Option('High School Graduate','6');

			yearOpt.options[defaulYearInSchool].selected = true;
			//optSchool.disabled = false;
			txtOtherSchool.value = "";
		}
	}

	function showSpecialNeeds(){
		var spDiv = $('specialNeedsDiv');

		var y = $('spYes');
		var n = $('spNo');

		if(spDiv.style.display == 'none'){
			y.checked = true;
			n.checked = false;
			jQuery('#specialNeedsDiv').slideDown("slow");
		}
		else {
			y.checked = false;
			n.checked = true;
			jQuery('#specialNeedsDiv').slideUp("slow");
		}
	}

	Event.observe(window,'load',function(){
		//activateOtherSchool();
		disableOtherSchool();
	//	showSpecifyField();
		(function($){
			var schools = {};
			$("#schoolX").autocomplete('/ajaxpages/searchCISUniversity.php');
			$('#schoolX').result(
				function(event, data, formatted) {
					$('#schoolXVal').val(data ? data[2]+'_'+data[1] : 0);
				}
			).keyup(function(){

				$(this).search();
			});
		})(jQuery);
	});
	//]]>
</script>
BOF;
	Template::includeDependent($incjs);



$incjs2 = <<<BOF
<script type="text/javascript">
//<![CDATA[

	function showSecondProg(){
		alert('showSecondProg');
		var chkAddProg = $('chkAddProgram').checked;
		var divStyle = $('program2').style;

		if(chkAddProg==true){
			jQuery('#program2').slideDown("slow");
		}
		else {
			var program_2 = $('p2');
			var mOption2 = $('option2');
			program_2.options[0].selected = true;
			mOption2.options[0].selected = true;
			$('options_2').style.display = 'none';

			$('program1choice').checked = false;
			$('equally').checked = false;

			jQuery('#program2').slideUp("slow");
		}
	}

	function getProgramDuration(e){
		var programID = e.options[e.selectedIndex].value;
		var term = $('term');
		var termID = term.options[term.selectedIndex].value;
		ajaxGetDurationsByProgramID(programID,termID);
    
    if(document.getElementById('p1').value == ''){
        $('startDateDiv').style.display = "none";
    	}
   }
	function ajaxGetDurationsByProgramID(progID, termID){
		var durationOptions = $('durationOption');
		var defaultVal = $('durationOption_default').value;

		jQuery.ajax({
			url: '/ajaxpages/CIS_onlineApplication/programDuration.php',
			data: { programID : progID, termID : termID },
			success: function(jsonResponse){

				if("NO_DURATION" == jsonResponse){
					durationOptions.options.length = 0;
					durationOptions.options[durationOptions.options.length] = new Option('No Option', '');
					$('durationOptionDiv').style.display = 'none';

				}
				else {
					$('durationOptionDiv').style.display = '';
					var durations = eval("(" + jsonResponse + ")");

					durationOptions.options.length = 0;

					durationOptions.options[durationOptions.options.length] = new Option('Choose duration', '');
					for(var i in durations){
						if(parseInt(i) >= 0){
							var optLength = durationOptions.options.length;
							durationOptions.options[durationOptions.options.length] = new Option(durations[i]['name'], durations[i]['id']);

							if(defaultVal == durations[i]['id']){
								durationOptions.options[optLength].selected = true;
							}
						}
					}
				}

			}
			});
	}

	function filterDuration(){

		var prog = $('p1');
		var term = $('term');
		var programID = prog.options[prog.selectedIndex].value;
		var termID = term.options[term.selectedIndex].value;
		ajaxGetDurationsByProgramID(programID, termID);
	}

	function updateStartDay(e){
		var dYear = $('year');
		var startDay = $('startDay');

		var startMonth = e.options[e.selectedIndex].value;
		var yearApplying = dYear.options[dYear.selectedIndex].value;

		var month=new Array(12);
		month[0]="January";
		month[1]="February";
		month[2]="March";
		month[3]="April";
		month[4]="May";
		month[5]="June";
		month[6]="July";
		month[7]="August";
		month[8]="September";
		month[9]="October";
		month[10]="November";
		month[11]="December";

		var dStartMonth = 0;
		for(var x in month){
			if(parseInt(x) >= 0){
				if(startMonth == month[x]){
					dStartMonth = x;
				}
			}
		}

		var days = daysInMonth(dStartMonth, yearApplying);

		startDay.options.length = 0;
		startDay.options[startDay.options.length] = new Option(" ","");
		for (var i = 1; i <= days; i++){
			startDay.options[startDay.options.length] = new Option(i,i);
		}
	}

	function daysInMonth(month,year) {
		//return 32 - new Date(year, month, 32).getDate();
	    return new Date(year, month, 0).getDate();
	}

	function filterAll(){
		var prog1 = $('p1');
		if(0 < prog1.options[prog1.options.selectedIndex].value){
			filterProgramsByTerm();
			filterDuration();
		}
		else {
			$('housingOptionDiv').style.display = "none";
			$('durationOptionDiv').style.display = "none";
			$('startDateDiv').style.display = "none";
		}
	}

	function showSpecifyField(){
		var refererOpt = $('referer');
		var selectedRef = refererOpt.options[refererOpt.options.selectedIndex].value;
		if(selectedRef == '' || selectedRef == 0){
			jQuery('#specifyField').slideDown("slow");
		}
		else {
			jQuery('#specifyField').slideUp("slow");
		}
	}

	function showCampusRep(){
		var CampusRepYes = $('CampusRepYes').checked;
		var CampusRepNo = $('CampusRepNo').checked;
		if(CampusRepYes == true){
			$('campus_rep_name').style.display = '';
		}
		else if(CampusRepNo == true) {
			$('campus_rep_name').style.display = 'none';
		}
	}

	function showFriendGrant(){
		var FriendGrantYes = $('FriendGrantYes').checked;
		var FriendGrantNo = $('FriendGrantNo').checked;
		if(FriendGrantYes == true){
			$('friend_grant_name').style.display = '';
		}
		else if(FriendGrantNo == true) {
			$('friend_grant_name').style.display = 'none';
		}
	}

Event.observe(window,'load',function(){
//	var prog1 = $('p1');
//	alert(prog1.options[prog1.options.selectedIndex].value);
//	if(prog1.options[prog1.options.selectedIndex].value == ''){
		filterAll();
		showSpecifyField();
		showCampusRep();
		showFriendGrant();

		//filterProgramsByTerm();
		//filterOption(1);
		//filterOption(2);
		//filterDuration();
//	}
});

//]]>
</script>
BOF;
		Template::includeDependent($incjs2);
?>


<div id="content_wrapper" class="noBottomPadding">
<div id="wide_column" class="layout2">
	<div class="section_detail">
		<div class="content">
		<h3>WHAT YOU WILL NEED TO GET STARTED:</h3>
			<ul class="disc">
				<li>Your credit card (please have your card ready in order to pay the $200 application deposit *).</li>
				<li>Your current GPA</li>
				<li>Name, term, and year for the program to which you are applying</li>
				<!--li>Passport (if you have one)</li-->
			</ul>
			<p>
               	*Please note: Your $200 application deposit is NOT an extra fee, but is a deposit applied towards your total program fee. This deposit is nonrefundable if you choose to withdraw from the program.
            </p>    
			<p>
				<strong>Be sure to consult with your study abroad office and/or academic advisor before applying.</strong> They may have their own application process or requirements that you need to follow. They can also provide guidance on getting your chosen program approved for credit and financial aid, as well as other helpful information about working with your home university or college.
			</p>
            
	<script type="text/javascript">
             
            var DP = (typeof DP == "object") ? DP : {};   
            (function($) {    
            	DP.validateForm = function(){
               	var error_handler = [];
                //generic check value method
                var valueCheck = function(elmId, wrongValue, messageText){
                    if($("#" + elmId).val() == wrongValue){
                        createAlert(elmId, messageText);
                    }else {
                        $('#'+elmId).css('background-color', '');
                    }
                }
                var errorReset = function(){
                    error_handler = [];
                }

               	var createAlert = function(elmId, messageText){
                	$('#'+elmId).css('background-color', '#FFCC66');
                  	error_handler.push(elmId);
                }
               	var validateSch = function (){
                	var schoolId = $('#schoolXVal').val();
                    if(schoolId == 'usu_0' || schoolId ==0) {
                        return false;
                    }
                    return true;
               	}
               
               	var checkSchool  = function(elmId,wrongValue){
                   	switch(elmId){
                       	case  'schoolX' :
                           	schoolVal = validateSch() == true ? 'ok' : $('#'+elmId).val();
                           	!$('#chckOtherSchool').is(':checked') ? valueCheck(elmId,schoolVal,'') : valueCheck(elmId,'rem_clr','') ;
                           	break;
                           
                      	case 'school1':
                           	$('#chckOtherSchool').is(':checked') ? valueCheck(elmId,wrongValue,'') : valueCheck(elmId,'rem_clr','');
                           	break;
                     
					/* not a validation */     
                    	case 'chckHighSchool' :
                           	if($('#chckHighSchool').is(':checked')){
                             	$('#major').val('');
                           	}
                           	break;
                      	case 'CampusRepNo':
                          	if($('#CampusRepNo').is(':checked')){
                             	$('#campusRep').val('');
                           	}
                           	break;
                      	case 'FriendGrantNo':
                          	if($('#FriendGrantNo').is(':checked')){
                             	$('#friendGr').val('');
                           	}
                           	break;      
                      	/* end */     
					}
				}
                
                var checkEmail = function (elmId){
                   	var emailvalue = $('#'+elmId).val();
					emailVal =  validateEmail(document.getElementById(elmId)) == true ? 'meada' : $('#'+elmId).val();
                   	if(elmId == 'email'){
                       	valueCheck(elmId, emailVal,'');  
                   	}else {
                       	emailvalue != '' ? valueCheck(elmId,emailVal,'') : valueCheck(elmId,'original','');
                   	}
                }

                //public method checks fieds
                //requires 'valSetting' setting object
                this.runCheck = function(){
                    errorReset(); 
                    for (i=0;i<this.valSetting.fileds.length;i++){    
                    	if($('#'+this.valSetting.fileds[i].container).is(':visible') ) {        
							switch (this.valSetting.fileds[i].type){
        						case 'email':
        							checkEmail(this.valSetting.fileds[i].id);
     								break;
								case 'checkbox':
        							checkSchool(this.valSetting.fileds[i].id, this.valSetting.fileds[i].val);
    								break;
								default :
        							valueCheck(this.valSetting.fileds[i].id, this.valSetting.fileds[i].val, this.valSetting.fileds[i].msg);   
							}
						}
                    }
                    
                    if(error_handler.length > 0){
                    	alert('Please fill out the required fields (highlighted).');
                        return false;
                    }
                    // agreement
                    if(!$('#signature').is(':checked')){
                        alert('Please agree to the Agreement and Waiver');
                        return false;
                    }
				}
            } 
            })(jQuery);
            
            //creates instance of the validation class
            var vl = new DP.validateForm();

            //passes validation settings
            vl.valSetting = {fileds : [
                {id : "p1", val : '' , msg : "Program Name", container : "progInfoDiv", type : '' } ,
                {id : "option1", val : '', msg : "Program Option", container : "options_1", type : '' },
                {id : "housingOption", val : '', msg : "Housing Option", container : "housingOptionDiv", type : '' },
                {id : "durationOption", val : '', msg : "Duration Option", container : "durationOptionDiv", type : '' },
                {id : "startMonth", val : '', msg : "Start Month", container : "startDateDiv", type : '' }, 
              	{id : "fn", val : '', msg : "", container : "visibleDiv", type : '' }, 
                {id : "ln", val : '', msg : "", container : "visibleDiv", type : '' }, 
                {id : "email", val : '', msg : "", container : "visibleDiv", type : 'email' }, 
                {id : "email2", val : '', msg : "", container : "visibleDiv", type : 'email' }, 
                {id : "email3", val : '', msg : "", container : "visibleDiv", type : 'email' }, 
                
                {id : "address", val : '', msg : "", container : "visibleDiv", type : '' }, 
                {id : "city", val : '', msg : "", container : "visibleDiv", type : '' }, 
                {id : "zip", val : '', msg : "", container : "visibleDiv", type : '' },
                {id : "phone", val : '', msg : "", container : "visibleDiv", type : '' },
                {id : "schoolX", val : '', msg : "", container : "visibleDiv", type : 'checkbox' },
                {id : "school1", val : '', msg : "", container : "otherSchools", type : 'checkbox' },
                {id : "school_email", val : '', msg : "", container : "visibleDiv", type : 'email' },
                {id : "major", val : '', msg : "", container : "majorAndMinor", type : '' },
              /* not a validation functions */  
                {id : "chckHighSchool", val : '', msg : "", container : "visibleDiv", type : 'checkbox' },
                {id : "CampusRepNo", val : '', msg : "", container : "visibleDiv", type : 'checkbox' },
                {id : "FriendGrantNo", val : '', msg : "", container : "visibleDiv", type : 'checkbox' },
              /* end */    
              
                {id : "gpa", val : '', msg : "", container : "visibleDiv", type : '' },
                {id : "term", val : '', msg : "", container : "visibleDiv", type : '' },
                {id : "year2", val : '', msg : "", container : "visibleDiv", type : '' },
                {id : "website", val : '', msg : "", container : "specifyField", type : '' },
                {id : "campusRep", val : '', msg : "", container : "campus_rep_name", type : '' },
                {id : "friendGr", val : '', msg : "", container : "friend_grant_name", type : '' }
                
                ]};
    </script>    

    <div id ="visibleDiv">
    </div>
		<h2>Personal Information</h2>
			<form method="post" action="OnlineApplication.php?step=1&amp;action=save" id="formPersonalInfo" name="formPersonalInfo" onsubmit ="/*FormValidate.program_info(); */return vl.runCheck();">

					<fieldset class="contentBlock first">
						<legend class="display_none">Profile Information</legend>
						<p class="title setSmallText dottedTopBorder">Fields with <span class="required">*</span> indicate required fields. </p>
						<ul class="form paddingof10 border_bottom_dotted">
								<li class="line paddedOnTop size50">
									<div class="line wrap">
										<label for="fn" class="setMediumText strong">Name <span class="required">*</span></label>
										<div class="unit size50">
											<?php $firstName = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getFirstName() : '');?>
											<input name="PersonalInfo[FirstName]" class="text size90" id="fn" type="text" value="<?php echo $firstName ?>">
											<label class="fcGray text setSmallText" for="fn">First Name <span class="required">*</span></label>
										</div>
										<div class="unit size50 lastUnit">
											<?php $lastName = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getLastName() : '');?>
											<input name="PersonalInfo[LastName]" class="text size90" id="ln" type="text" value="<?php echo $lastName ?>">
											<label class="fcGray text setSmallText" for="ln">Last Name <span class="required">*</span></label>
										</div>
									</div>
								</li>
								<li class="line wrap size50">
									<div class="unit size50">
										<label for="PersonalInfo[Gender]" class="setMediumText strong">Gender <span class="required">*</span></label>
										<ul class="checkbox_radio_list">
											<?php $gender = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getGender() : 1);?>
											<li><input type="radio" class="checkbox_radio_list paddedSOnTop" id="m" value="1" name="PersonalInfo[Gender]" <?php if($gender==1): ?>checked<?php endif;?>/> <label for="m" class="font-normal">Male</label></li>
											<li><input type="radio" class="checkbox_radio_list paddedSOnTop" id="f" value="2" name="PersonalInfo[Gender]" <?php if($gender==2): ?>checked<?php endif;?>/> <label for="f" class="font-normal">Female</label></li>
										</ul>
									</div>
								</li>

								<li class="line size70">
									<div class="line size50 wrap">
										<label for="mm" class="setMediumText strong">Birthdate <span class="required">*</span></label>
										<?php
											$month = '';
											$day   = '';
											$year  = '';
											if(isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO])):
												$birthdate = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBirthday();
												$birthdate = strtotime($birthdate);
												$month = date('n',$birthdate);
												$day   = date('j',$birthdate);
												$year  = date('Y',$birthdate);
											endif;
										?>
										<div class="unit size50">
											<div class="unit size50">
												<select name="PersonalInfo[mm]" id="mm" > <!--onChange="return newDateLimiter(this, event);">-->
													<?php for($i = 1; $i <=12; $i++): ?>
														<option value='<?php echo $i ?>' <?php if($month==$i): ?>selected<?php endif; ?> ><?php echo $i ?></option>
													<?php endfor; ?>
												</select>
											<?php /*	<input name="PersonalInfo[mm]" id="mm" type="text" value="<?=$month?>" onKeypress="return numbersonly(this, event);" onChange="return dateLimiter(this, event);"> */ ?>
												<label class="fcGray text setSmallText" for="mm">MM</label>
											</div>
											<div class="unit size50 lastUnit">

												<select name="PersonalInfo[dd]" class="" id="dd">
													<?php for($i = 1; $i <=31; $i++): ?>
														<option value='<?php echo $i ?>' <?php if($day==$i): ?>selected<?php endif; ?> ><?php echo $i ?></option>
													<?php endfor; ?>
												</select>
											<?php /*	<input name="PersonalInfo[dd]" class="text size70" id="dd" type="text" value="<?=$day?>" onKeypress="return numbersonly(this, event);" onChange="return dateLimiter(this, event);"> */ ?>
												<label class="fcGray text setSmallText" for="dd">DD</label>
											</div>
										</div>
										<div class="unit lastUnit size50">
											<div class="unit lastUnit">
												<select name="PersonalInfo[yyyy]" class="" id="yyyy" >
													<?php for($i = 1970; $i <=2000; $i++): ?>
														<option value='<?php echo $i ?>' <?php if($year==$i): ?>selected<?php endif; ?> ><?php echo $i ?></option>
													<?php endfor; ?>
												</select>
												<label class="fcGray text setSmallText" for="yyyy">YYYY</label>
											</div>
										</div>
									</div>
								</li>

								<li class="line wrap paddedOnBottom">
									<div class="unit size50">
										<?php $email = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getEmail() : '');?>
										<label for="email" class="setMediumText strong">Email <span class="required">*</span></label>
										<input type="text" id="email" class="text size95" size="23" name="PersonalInfo[Email]" value="<?php echo $email ?>"/>

									</div>
									<div class="unit lastUnit formhelptext size40">
										 Please check to make sure your email address is correct. We will send all relevant information regarding your application to this address.
									</div>
								</li>

								<li class="line wrap paddedOnBottom">
									<div class="unit size50 paddedOnBottom">
										<?php $email2 = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getAltEmail() : '');?>
										<label for="email2" class="setMediumText strong">Alternate Email</label>
										<input type="text" id="email2" class="text size95" size="23" name="PersonalInfo[AltEmail]" value="<?php echo $email2 ?>"/>

									</div>
									<div class="unit lastUnit formhelptext size40">
										 In case your first email address is unavailable, we may be able to send you emails to this account.
									</div>
								</li>
								<li class="line wrap paddedOnBottom">
									<div class="unit size50 paddedOnBottom">
										<?php $email3 = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getBillingEmail() : '');?>
										<label for="email3" class="setMediumText strong">Billing Email</label>
										<input type="text" id="email3" class="text size95" size="23" name="PersonalInfo[BillingEmail]" value="<?php echo $email3 ?>"/>

									</div>
									<div class="unit lastUnit formhelptext size40">
										 Please provide the email address where invoices should be sent.
									</div>
								</li>
						</ul>
					</fieldset>

					<fieldset class="contentBlock first">
						<legend class="paddedOnBottom20 setMediumText paddedOnTop20">Address</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line wrap paddedOnBottom">
								<div class="unit size50">
									<?php $street1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getStreet1() : ''); ?>
									<label for="address" class="setMediumText strong">Street Address <span class="required">*</span></label>
									<input type="text" id="address" class="text size95" size="23" name="AddressInfo[Street1]" value="<?php echo $street1 ?>"/>
								</div>
							</li>
							<li class="line wrap paddedOnBottom">
								<div class="unit size50">
									<?php $city1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCity1() : ''); ?>
									<label for="city" class="setMediumText strong">City <span class="required">*</span></label>
									<input type="text" id="city" class="text size95" size="23" name="AddressInfo[City1]" value="<?php echo $city1 ?>"/>
								</div>
							</li>

							<li class="line paddedOnTop size50 paddedOnBottom">
								<div class="line wrap">
									<div class="unit size50">
										<?php $state1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getState1() : ''); ?>
										<label for="state" class="setSMediumText strong">State </label>
										<select id="state" name="AddressInfo[State1]">
											<option value="">Choose State</option>
											<?php foreach($states as $state):?>
												<option value="<?php echo $state['abbr'] ?>" <?php if($state1==$state['abbr']):?>selected<?php endif; ?> ><?php echo $state['abbr'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="unit size50 lastUnit">
										<?php $zip1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getZip1() : ''); ?>
										<label for="zip" class="setSMediumText strong">Zip <span class="required">*</span></label>
										<input type="text" id="zip" class="text size90" size="25" name="AddressInfo[Zip1]" value="<?php echo $zip1 ?>" onKeyPress="return numbersonly(this, event)"/>
									</div>
								</div>
							</li>
							<li class="line wrap paddedOnBottom">
								<div class="unit size50">
									<?php $phone1 = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getPhoneNumber1() : ''); ?>
									<label for="phone" class="setMediumText strong">Phone <span class="required">*</span></label>
									<input type="text" id="phone" class="text size95" size="23" name="AddressInfo[PhoneNumber1]" value="<?=$phone1?>" onKeyPress="return numbersonly(this, event)"/>
								</div>
								<div class="unit lastUnit formhelptext size40">
									Example: 555-555-5555
								</div>
							</li>
							<li class="line wrap paddedOnBottom">
								<div class="unit size50">
									<?php $cellphone = (isset($objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDRESS_INFO][0]->getCellphone() : ''); ?>
									<label for="cphone" class="setMediumText strong">Cell Phone</label>
									<input type="text" id="cphone" class="text size95" size="23" name="AddressInfo[Cellphone]" value="<?php echo $cellphone ?>" onKeyPress="return numbersonly(this, event)"/>
								</div>
							</li>

						</ul>
					</fieldset>


					<fieldset class="contentBlock first">
						<legend class="paddedOnBottom20 setMediumText paddedOnTop20">School Information</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line wrap paddedOnBottom">
								<div class="paddedOnBottom unit size50">
									<?php $schoolID = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolID() : '0'); ?>
									<?php $type = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getType() : ''); ?>
									<?php
										if($type == 0){ $schoolID = 'usu_'.$schoolID; }
										else if($type == 1){ $schoolID = 'partner_'.$schoolID; }
									?>

									<label for="schoolX" class="setMediumText strong">Name of School you currently attend <span class="required">*</span></label>
									<?php
										$schoolXname = (
											isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) && $type != 2
												? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getSchoolName()
												: ''
										);
									?>
									<input onkeyup ="test(), changeSchool()" onfocus="changeSchool()" onblur="changeSchool()" type="text" id="schoolX" name="SchoolInfo[OtherSchoolName1]" class="text size95" value="<?php echo $schoolXname ?>" />
									<input type="hidden" id="schoolXVal" name="SchoolInfo[SchoolID]" value="<?php echo $schoolID?>" />

<script type="text/javascript">
	function changeSchool() {
        var orig = jQuery("#schoolXVal").val();
        var partnerSchool = orig.slice(0, 7);
		if (partnerSchool == "partner") {
			jQuery("#discountedSchoolWarning").show();
			jQuery('#defaultSchoolWarning').hide();
		}
		else {
			jQuery("#discountedSchoolWarning").hide();
			jQuery('#defaultSchoolWarning').show();
		}
	}
	
	function test() {
		var timeout = setTimeout(function() {
			var schoolId = document.getElementById('schoolXVal').value;
			var waiveme = document.getElementById('waiveme');
			var submit = document.getElementById('submit');
			var waiveFeeCheckbox = document.getElementById('waiveFee');
		 	if (schoolId.indexOf('partner') == 0) {
		 		waiveme.checked = true;
		 		submit.setAttribute("value","Submit");
		 	} else {
		 		waiveme.checked = false;
		 		submit.setAttribute("value","Continue");

		 	}
		}, 1);
	}

	function resetPartner(element){
		var submit = document.getElementById('submit');
		var schoolId = document.getElementById('schoolXVal').value;
		var waiveFeeCheckbox = document.getElementById('waiveFee');
		var waiveme = document.getElementById('waiveme');
		if(element.checked == true){
			submit.setAttribute("value","Continue");
			waiveme.checked = false;
		}else{
			if (schoolId.indexOf('partner') == 0) {
				waiveme.checked = true;
				submit.setAttribute("value","Submit");
		 	} else {
		 		submit.setAttribute("value","Continue");
		 		waiveme.checked = false;
		 	}
		}
	}
</script>


									<div>
										<label style="position: relative; width: 40%; float: left;"><input type="checkbox" id="chckOtherSchool" name="SchoolInfo[Other]" onclick="disableHighSchoolCheckbox(); disableOtherSchool(); resetPartner(this);" <?php if($type==3):?>checked<?php endif; ?> /> Other School</label>
										<label style="position: relative; width: 40%; float: left;"><input type="checkbox" id="chckHighSchool" name="SchoolInfo[Hs]" onclick="enableOtherSchoolCheckbox(); disableOtherSchool(); resetPartner(this);" <?php if($type==2):?>checked<?php endif; ?> /> High School</label>
									</div>
								</div>
								<div id="defaultSchoolWarning" class="unit lastUnit formhelptext size40">
									Begin typing the official name of your home university
									in this box, and then select your school from the drop-down menu.
								</div>
								<div id="discountedSchoolWarning" class="unit lastUnit size40" style="display:none">
									<font color="green">Your school will pay the application deposit on your behalf. Please complete and submit the application.</font>
								</div>
							</li>
							<li class="line wrap paddedOnBottom">
								<div id='otherSchools' class="lastUnit size50" >
									<?php $sName = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getOtherSchoolName() : ''); ?>

									<label for="school1" class="setMediumText strong">Specify Name of School <span class="required">*</span></label>
									<input type="text" id="school1" class="text size95" size="23" name="SchoolInfo[OtherSchoolName]" value="<?php echo $sName ?>" />
								</div>
							</li>
							<li class="line wrap paddedOnBottom">
								<div class="paddedOnBottom unit size50" id="majorAndMinor">
									<ul>
										<li class="line wrap paddedOnBottom">
												<?php $major = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getMajor() : ''); ?>
												<label for="major" class="setMediumText strong">Major <span class="required">*</span></label>
												<input type="text" id="major" class="text size95" size="23" name="SchoolInfo[Major]" value="<?php echo $major ?>"/>
										</li>

									</ul>
								</div>
							</li>

							<li class="line wrap">
								<div class="paddedOnBottom unit size50">
									<?php $yearInSchool = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getYearInSchool() : 1); ?>
									<input type="hidden" id="yearInSchool" value="<?php echo $yearInSchool ?>">
									<label for="year" class="setMediumText strong">Year in School <span class="required">*</span></label>
									<select id="year" name="SchoolInfo[YearInSchool]" class="size50">
										<option value="1" <?php if($yearInSchool==1):?>selected<?php endif; ?> >Freshman</option>
										<option value="2" <?php if($yearInSchool==2):?>selected<?php endif; ?> >Sophomore</option>
										<option value="3" <?php if($yearInSchool==3):?>selected<?php endif; ?> >Junior</option>
										<option value="4" <?php if($yearInSchool==4):?>selected<?php endif; ?> >Senior</option>
										<option value="5" <?php if($yearInSchool==5):?>selected<?php endif; ?> >College/University Graduate</option>
										<option value="6" <?php if($yearInSchool==6):?>selected<?php endif; ?> >High School Graduate</option>
									</select>

								</div>
							</li>

							<li class="line size50 paddedOnBottom">
								<div class="line wrap">
									<div class="unit">
										<?php $genAverage = (isset($objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::SCHOOL_INFO]->getGenAverage() : ''); ?>
										<label for="gpa" class="setSMediumText strong">Current cumulative GPA <span class="required">*</span></label>
										<input type="text" id="gpa" class="text size50" size="25" name="SchoolInfo[GenAverage]" value="<?php echo $genAverage ?>" onKeyPress="return numbersonly(this, event)"/>
									</div>
								</div>
							</li>

						</ul>
					</fieldset>

<!-- SCHOOL INFORMATION   -->
					<h2>Program Information</h2>
					<fieldset class="contentBlock first">
						<legend class="display_none">Program Date</legend>
						<p class="title setSmallText dottedTopBorder">Fields with <span class="required">*</span> indicate required fields. </p>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="paddedOnTop line wrap">
								<?php $selectedTerm = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTerm() : ''); ?>
								<div class="unit size50">
									<label for="term" class="setMediumText strong">Term Applying for <span class="required">*</span></label>
									<select id="term" name="ProgramInfo[Term]" class="size50" onchange="filterProgramsByTerm()">
										<option value="">Choose a Term</option>
										<?php foreach($terms as $term): ?>
											<option value="<?php echo $term->getID()?>" <?php if($selectedTerm==$term->getID()):?>selected<?php endif; ?> ><?php echo $term->getName()?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</li>

							<li class="line wrap paddedOnBottom">
								<?php $year = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear() : ''); ?>
								<div class="unit size50">
									<label for="year" class="setMediumText strong">Year Applying for <span class="required">*</span></label>
									<select id="year2" name="ProgramInfo[Year]" class="size50" onchange="showShirtField()">
										<option value="">Choose a Year</option>
										<option value="2012" <?php if($year==2012):?>selected<?php endif;?> >2012</option>
										<option value="2013" <?php if($year==2013):?>selected<?php endif;?> >2013</option>
										<option value="2014" <?php if($year==2014):?>selected<?php endif;?> >2014</option>
									</select>
								</div>
							</li>
						</ul>
					</fieldset>


	<script type = "text/javascript">
		var jsPrograms = <?php echo json_encode($jsPrograms); ?>;
		var jsOptions = <?php echo json_encode($jsOptions); ?>;
		var jsHousingOptions = <?php echo json_encode($jsHousingOptions); ?>
		
		function showShirtField(){
			var term = $('term').value;
			var year = $('year2').value;
			if(((term == 1 || term == 3) && (year == 2013 || year == 2014)) || ((term == 4 || term == 7) && (year == 2014))){
				$('shirtOptionDiv').style.display = '';
			}
			else {
				$('shirtOptionDiv').style.display = 'none';
			}
		}

		function filterProgramsByTerm(){
			var termOptions = $('term');
			var program_1 = $('p1');
			var programToTerm = [];

			var selectedTermID = termOptions.options[termOptions.options.selectedIndex].value;

			clearOptions();

			if(0 < selectedTermID){
				program_1.disabled = false;
				for(var i in jsPrograms){
					var terms = jsPrograms[i]['termID'];
					for(var x in terms){
						if(terms[x] == selectedTermID){
							programToTerm[i] = jsPrograms[i];
						}
					}

				}
			}
			else {
				program_1.disabled = true;
			}

			var selectedProg1 = program_1.options[program_1.options.selectedIndex].value;

				program_1.options.length = 0;

				program_1.options[program_1.options.length] = new Option('Choose Program...','');

				for(var i in programToTerm){
					if(parseInt(i) >= 0){
						var optProg1_length = program_1.options.length;

						program_1.options[program_1.options.length] = new Option(programToTerm[i]['name'],programToTerm[i]['id']);

						if(selectedProg1 == programToTerm[i]['id']){
							program_1.options[optProg1_length].selected = true;
							filterOption(1);
						}

					}
				}
		
			showShirtField();

		}

		function clearOptions(){
			var option1Div = $('options_1');
			var housingOptionDiv = $('housingOptionDiv');
			var durationOptionDiv = $('durationOptionDiv');
			var startDateDiv = $('startDateDiv');

			var progOption1 = $('option1');
			var housingOption = $('housingOption');
			var durationOption = $('durationOption');
			progOption1.options[0] = new Option('Choose Your Options', '');
			housingOption.options[0] = new Option('Choose housing option', '');
			durationOption.options[0] = new Option('Choose duration', '');

			option1Div.style.display = "none";
			housingOptionDiv.style.display = "none";
			durationOptionDiv.style.display = "none";
			startDateDiv.style.display = "none";

		}
		function filterOption(optNumber){
			var termOptions = $('term');
			var program = $('p'+optNumber);
			var school = $('schoolX').value;

			var selectedProgramID = program.options[program.options.selectedIndex].value;
			var selectedTermID = termOptions.options[termOptions.options.selectedIndex].value;

			var programOptions = [];
			var housingOptions = [];
			if(0 < selectedProgramID){

				for(var i in jsOptions){
					var termID = jsOptions[i]['termID'];
					var programID = jsOptions[i]['programID'];

					if(selectedTermID == termID && selectedProgramID == programID){
						programOptions[i] = jsOptions[i];
					}
				}

				for(var a in jsPrograms){
					if(parseInt(a) >= 0){
						if(selectedProgramID == jsPrograms[a]['id']){
							if(jsPrograms[a]['hasStartDate'] == 1){
								$('startDateDiv').style.display = '';
							}
							else {
								var s_month = $('startMonth');
								var s_day = $('startDay');
								s_month.options[0].selected = true;
								s_day.options[0].selected = true;
								$('startDateDiv').style.display = 'none';
							}
						}
					}
				}
                
                if(selectedProgramID == 233 && selectedTermID == 1){
                    $('startDateDiv').style.display = 'none';
                }
                if(selectedProgramID == 253 && selectedTermID == 1){
                    $('startDateDiv').style.display = 'none';
                }

				for(var x in jsHousingOptions){
					var programID = jsHousingOptions[x]['programID'];
					var houseTermID = jsHousingOptions[x]['termID'];
					if(selectedProgramID == programID && selectedTermID == houseTermID){
						housingOptions[x] = jsHousingOptions[x];
					}
				}
			}
			if(selectedProgramID == 91 && school == 'Drexel University'){
				distributeForAustralia();
			}
			else if(selectedProgramID == 101 && school == 'Drexel University'){
				distributeForChina();
			}
			else {
				distributeHousingOptions(housingOptions);
				distributeOptions(programOptions, optNumber);
				filterDuration();
			}
		}

		function distributeOptions(xProgramOptions, optNumber){
			var optionDiv = $('options_'+optNumber);
			var progOption = $('option'+optNumber);

			var p1 = $('option1');
			if(0 < xProgramOptions.length){

				optionDiv.style.display = "";

				var defaultVal = $('option'+optNumber+'_default').value;
				progOption.options.length = 0;

				progOption.options[progOption.options.length] = new Option('Choose Your Options', '');
				for(var i in xProgramOptions){
					if(parseInt(i) >= 0){
						var optLength = progOption.options.length;
						progOption.options[progOption.options.length] = new Option(xProgramOptions[i]['name'], xProgramOptions[i]['id']);
						if(defaultVal == xProgramOptions[i]['id']){
							progOption.options[optLength].selected = true;
						}
					}
				}

			}
			else {
				progOption.options.length = 0;

				progOption.options[progOption.options.length] = new Option('Choose Your Options', '');
				optionDiv.style.display = "none";
			}

		}
		
		function distributeForChina(){
			var hOptions = $('housingOption');
			var progOption = $('option1');
			
			$('options_1').style.display = "";
			$('housingOptionDiv').style.display = "";
			optionslength = 0;
			hOptions.options[optionslength] = new Option('Choose housing option', '');
			progOption.options[optionslength] = new Option('Choose Your Options', '');
			
			var defaultVal = $('housing_default').value;
			var defaultOptionVal = $('option1_default').value;
			
			progOption.options[1] = new Option('Independent Internship 6 months', 464);
			if(defaultOptionVal == 1){
				progOption.options[464].selected = true;
			}
			
			hOptions.options[1] = new Option('Housing', 27);
			hOptions.options[2] = new Option('No Housing', 26);
			if(defaultVal == 2){
				hOptions.options[26].selected = true;
			}
			else if(defaultVal == 1){
				hOptions.options[27].selected = true;
			}
		}
		
		function distributeForAustralia(){
			var hOptions = $('housingOption');
			var progOption = $('option1');
			
			$('options_1').style.display = "";
			$('housingOptionDiv').style.display = "";
			optionslength = 0;
			hOptions.options[optionslength] = new Option('Choose housing option', '');
			progOption.options[optionslength] = new Option('Choose Your Options', '');
			
			var defaultVal = $('housing_default').value;
			var defaultOptionVal = $('option1_default').value;
			
			progOption.options[1] = new Option('Independent Internship 6 months', 464);
			if(defaultOptionVal == 1){
				progOption.options[464].selected = true;
			}
			
			hOptions.options[1] = new Option('Homestay', 13);
			hOptions.options[2] = new Option('Apartment (Sydney Only)', 17);
			hOptions.options[3] = new Option('No Housing', 26);
			if(defaultVal == 1){
				hOptions.options[13].selected = true;
			}
			else if(defaultVal == 2){
				hOptions.options[17].selected = true;
			}
			else if(defaultVal == 3){
				hOptions.options[26].selected = true;
			}
		}

		function distributeHousingOptions(housingOptions){
			var hOptions = $('housingOption');
			if(0 < housingOptions.length){
				$('housingOptionDiv').style.display = "";
				hOptions.options.length = 0;
				hOptions.options[hOptions.options.length] = new Option('Choose housing option', '');

				var defaultVal = $('housing_default').value;

				for(var i in housingOptions){
					if(parseInt(i) >= 0){
						var optLength = hOptions.options.length;
						hOptions.options[hOptions.options.length] = new Option(housingOptions[i]['name'], housingOptions[i]['id']);
						if(defaultVal == housingOptions[i]['id']){
							hOptions.options[optLength].selected = true;
						}
					}
				}
			}
			else {
				hOptions.options.length = 0;
				hOptions.options[hOptions.options.length] = new Option('Choose housing option', '');
				$('housingOptionDiv').style.display = "none";
			}
		}
	</script>


					<fieldset class="contentBlock first">
						<legend class="display_none">Program Chosen</legend>
						<ul class="form paddingof10 border_bottom_dotted">
								<li class="line wrap paddedOnBottom">
									<?php $prog1 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramID() : ''); ?>
									<?php $prog2 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondProgramID() : ''); ?>
									<div id = "progInfoDiv" class="paddedOnTop unit size50">
										<label for="p1" class="setMediumText strong">Program Applying For <span class="required">*</span></label>
										<select id="p1" name="ProgramInfo[FirstProgramID]" class="" onchange="filterOption(1); getProgramDuration(this)" <?php if($prog1==""):?>  disabled  <?php endif; ?> >
											<option value="">Choose Program...</option>
											<?php foreach($programs as $program): ?>
												<option value="<?php echo $program->getID() ?>" <?php if($prog1==$program->getID()):?>selected<?php endif;?> ><?php echo $program->getName()?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</li>
								<li class="line wrap paddedOnBottom">
									<?php $opt1 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionID() : ''); ?>
										<ul>
											<li>
												<div id="options_1" class="" style='display:none' >
													<label for="option1" class="setMediumText strong">Program Options<span class="required">*</span></label>
													<input type="hidden" id="option1_default" value="<?php echo $opt1 ?>">
													<select id="option1" name="ProgramInfo[FirstOptionID]" class="">
														<option value="<?php echo $opt1 ?>" <?php if($opt1!=''):?>selected<?php endif;?> ></option>
													</select>
												</div>
											</li>
											<li>
												<div class="unit size50" id="housingOptionDiv" style='display:none'>
													<?php $housingID = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getHousingOptionID() : ''); ?>
													<label for="option1" class="setMediumText strong">Housing Options<span class="required">*</span></label>
													<input type="hidden" id="housing_default" value="<?php echo $housingID ?>">
													<select id="housingOption" name="ProgramInfo[HousingOptionID]" class="">
															<option value=""></option>
													</select>
												</div>
												<div class="lastUnit size50" id="durationOptionDiv" style='display:none'>
													<?php $durationID = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getDurationOptionID() : ''); ?>
													<label for="option1" class="setMediumText strong">Duration of Internship<span class="required">*</span></label>
													<input type="hidden" id="durationOption_default" value="<?php echo $durationID ?>">
													<select id="durationOption" name="ProgramInfo[DurationOptionID]" class="">
															<option value=""></option>
													</select>
												</div>

											</li>
											<li>
												<div id="startDateDiv" style='display:none'>
													<div class="unit size50">
														<?php $startMonth = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartMonth() : ''); ?>
														<label for="option1" class="setMediumText strong">Month you would like to<br/> start your internship? <span class="required">*</span></label>
														<input type="hidden" id="startMonth_default" value="<?php echo $startMonth ?>">
														<select id="startMonth" name="ProgramInfo[StartMonth]" class="" onchange="updateStartDay(this)">
															<option value="" ></option?>
															<?php foreach($startMonths as $month ):?>
																<option value="<?php echo $month ?>" <?php if($startMonth == $month):?>selected<?php endif;?> ><?php echo $month ?></option>
															<?php endforeach;?>
														</select>
													</div>
													<div class="lastUnit size50">
														<?php $startDay = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getStartDay() : ''); ?>
														<label for="option1" class="setMediumText strong">Day of month you would like to<br/> start your internship? <span class="required">*</span></label>
														<input type="hidden" id="startDay_default" value="<?php echo $startDay?>">
														<select id="startDay" name="ProgramInfo[StartDay]" class="">
															<option value="" ></option?>
															<?php for($day = 1; $day <= 31 ; $day++):?>
																<option value="<?php echo $day ?>" <?php if($startDay == $day):?>selected<?php endif;?> ><?php echo $day ?></option>
															<?php endfor; ?>
														</select>
													</div>
												</div>
											</li>
										</ul>
								</li>
							</ul>
					</fieldset>

					<fieldset class="contentBlock first">
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size50">
									<?php $learnFrom = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getLearnProgramFrom() : '-1'); ?>
									<label for="interested" class="setMediumText strong">How did you learn about this program? <span class="required">*</span></label>
									<select id="referer" name="AdditionalInfo[LearnProgramFrom]" class="size50" onchange="showSpecifyField()">
										<option value="" <?php if($learnFrom == '-1'):?>selected<?php endif;?> ></option>
										<?php foreach(CISOnlineAppConst::getAddInfoOptions() as $key => $option):?>
											<option value="<?php echo $key ?>" <?php if((int)$learnFrom === $key):?>selected<?php endif;?> ><?php echo $option ?></option>
										<?php endforeach;?>
									</select>
								</div>
								<div id="specifyField" class="unit lastUnit size40">
									<?php $others = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getOthers() : ''); ?>
									<label for="website" class="setMediumText strong"<span class="fcGray">Please Specify: </span></label>
									<input type="text" id="website" class="text size95" size="23" name="AdditionalInfo[Others]" value="<?php echo $others ?>"/>
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size50">
									<?php $financialAid = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFinancialAid() : 0); ?>
									<label for="interested" class="setMediumText strong">Are you planning on using financial aid to pay for this program? <span class="required">*</span></label>
									<input type="radio" value="1" name="AdditionalInfo[FinancialAid]" <?php if($financialAid == 1): ?>checked<?php endif;?> >Yes <input type="radio" value="0" name="AdditionalInfo[FinancialAid]" <?php if($financialAid == 0): ?>checked<?php endif;?>>No
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size50">
									<label for="interested" class="setMediumText strong">Enter Coupon code if you have one: <br/><span class="setSmallText required">Warning: Case sensitive!</span></label>
									<?php if($couponCode):?><p class="setSmallText required">For security reasons, you have to re-enter the code.</p><?php endif;?>
									<input type="text" id="coupon" class="text size50" size="23" name="CouponCode" value=""/>
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size90">
									<?php $campusRep = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getCampusRep() : ""); ?>
									<label for="interested" class="setMediumText strong">Were you referred by an Alumni/CISabroad Campus Representative? If yes, please name the alumni who referred you.</label>
									<input type="radio" value="1" id="CampusRepYes" name="CampusRep" <?php if($campusRep != ""): ?>checked<?php endif;?> onclick="showCampusRep()" >Yes <input type="radio" value="0" id="CampusRepNo" name="CampusRep" <?php if($campusRep == ""): ?>checked<?php endif;?> onclick="showCampusRep()" >No
									<div id="campus_rep_name">
										<input type="text" id = "campusRep" class="text size20" size="40" name="AdditionalInfo[CampusRep]" value="<?php echo $campusRep?>"/>
									</div>
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size90">
									<?php $friendGrant = (isset($objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::ADDITIONAL_INFO]->getFriendGrant() : ""); ?>
									<label for="interested" class="setMediumText strong">Are you applying for a Bring a Friend Grant? If yes, please name your friend.</label>
									<input type="radio" value="1" id="FriendGrantYes" name="FriendGrant" <?php if($friendGrant != ""): ?>checked<?php endif;?> onclick="showFriendGrant()" >Yes <input type="radio" value="0" id="FriendGrantNo" name="FriendGrant" <?php if($friendGrant == ""): ?>checked<?php endif;?> onclick="showFriendGrant()" >No
									<div id='friend_grant_name'>
										<input type="text" id ="friendGr" class="text size20" size="40" name="AdditionalInfo[FriendGrant]" value="<?php echo $friendGrant ?>"/>
									</div>
								</div>
							</li>
						</ul>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<div class="unit size90">
									<label for="interested" class="setMediumText strong">Are you applying for a Go Again grant? (you are an alum going on another CISabroad program)</label>
									<input type="radio" value="1"  name="goAgainGrant">Yes
									<input type="radio" value="0"  name="goAgainGrant" checked = "checked">No
								</div>
							</li>
						</ul>
						
						<div id='shirtOptionDiv' style='display:none'>
							<ul class="form paddingof10 border_bottom_dotted">
								<li class="line wrap paddedOnBottom">
									<?php $shirtSize = (isset($objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getShirtSize() : ''); ?>
									<div class="unit size50">
										<label for="shirtSize" class="setMediumText strong">T Shirt Size</label>
										<select id="shirtSize" name="PersonalInfo[ShirtSize]" class="size50">
											<option value="">Choose a Size</option>
											<option value='Female Small'  <?php if($shirtSize=='Female Small'):?> selected<?php endif;?> >Female Small</option>
											<option value='Female Medium' <?php if($shirtSize=='Female Medium'):?>selected<?php endif;?> >Female Medium</option>
											<option value='Female Large'  <?php if($shirtSize=='Female Large'):?> selected<?php endif;?> >Female Large</option>
											<option value='Female XL'     <?php if($shirtSize=='Female XL'):?>    selected<?php endif;?> >Female XL</option>
											<option value='Female XXL' 	  <?php if($shirtSize=='Female XXL'):?>	  selected<?php endif;?> >Female XXL</option>
											<option value='Male Small' 	  <?php if($shirtSize=='Male Small'):?>   selected<?php endif;?> >Male Small</option>
											<option value='Male Medium'   <?php if($shirtSize=='Male Medium'):?>  selected<?php endif;?> >Male Medium</option>
											<option value='Male Large'    <?php if($shirtSize=='Male Large'):?>	  selected<?php endif;?> >Male Large</option>
											<option value='Male XL' 	  <?php if($shirtSize=='Male XL'):?>	  selected<?php endif;?> >Male XL</option>
											<option value='Male XXL' 	  <?php if($shirtSize=='Male XXL'):?>	  selected<?php endif;?> >Male XXL</option>
										</select>
									</div>
									<div class="unit lastUnit formhelptext size40">
										 Please note: t-shirts run small, so please select one size larger than you normally would.
									</div>
								</li>
							</ul>
						</div>

						<legend class="display_none">Comments</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">
								<?php $com = ($comment == null) ? '' : $comment; ?>
								<label for="comments" class="setMediumText strong">Additional comments about your application:</label>
								<textarea rows="5" cols="53" name="txtAcomment"><?php echo $com ?></textarea>
							</li>
						</ul>
					</fieldset>
	<!-- +++++++++++++++++++++++++++++++++++  waive here  +++++++++++++++++++++++++++++++++++  -->

	<script type="text/javascript">
	function visibleterm(){
		document.getElementById('hideterm').style.display = 'block';
		document.getElementById('termConditionLink').style.display = 'none';
		document.getElementById('visibleAgWaive').innerHTML = "I understand and agree to the above statements.";
		}

	</script>

			<div id = "hideterm" style = "display:none;">

			 <h2>Agreement and Waiver</h2>

			<p class="paddedOnBottom">
			</p>

			<ul class="disc">
				<li>
					I certify that all of the above information is correct, and I agree to stand by the financial, academic, and conduct policies and procedures set forth by the Center for International Studies (CIS) and partner institutions.
				</li>
				<li >
					I authorize the appropriate officials of my overseas host institution(s) to forward official transcripts of the academic work I complete while abroad to CIS. They (CIS) will then release this information to the appropriate officials at my home institution.
				</li>

				<li>
				Furthermore, I understand that CIS and its affiliated institutions, in arranging these programs, act only as agents. As such, neither CIS nor any of its employees, or persons, parties, organizations, or agencies collaborating with them, is or shall be responsible or liable for injury, loss, damage, deviation, delay, curtailment, however caused, or the consequences thereof which may occur during any travel or program. CIS and the sponsoring institution reserve the right to cancel or alter any program or course for any reason.
				</li>

			</ul>
			</div>
				<fieldset class="contentBlock first">
					<legend class="display_none">siganture and date</legend>
					<ul class="form paddingof10 border_bottom_dotted">
							<li class="paddedOnTop line wrap paddedOnBottom">
								<?php $signature = (isset($objArr[CISOnlineAppFieldsMapper::AGREEMENT]) ? $objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getSignature() : ''); ?>
								<label><a onclick = "visibleterm()" id = "termConditionLink" >Agreement and Waiver</a></label>
								<label class="setMediumText strong"><input type="checkbox" id="signature" name="Agreement[Signature]" <?php if($signature!=''):?>checked<?php endif; ?> >
								<strong id = "visibleAgWaive">
								 I understand and agree to the Agreement and Waiver.
								 </strong>
								</label>
							</li>
							<li class="paddedOnTop line wrap paddedOnBottom20">
								<?php
									$dateApplied = Date("l F d, Y");
									if(isset($objArr[CISOnlineAppFieldsMapper::AGREEMENT])):
										$dateApplied = $objArr[CISOnlineAppFieldsMapper::AGREEMENT]->getApplicationDate();
										$dateApplied = strtotime($dateApplied);
										$dateApplied = Date("l F d, Y", $dateApplied);
									endif;
								?>
								<div class="unit size50">
									<label for="date" class="setMediumText strong">Date of application   <span class="required">*</span></label>
									<input type="text" id="date" class="text size95" size="23" name="Agreement[ApplicationDate]" value="<?php echo $dateApplied ?> " disabled="disabled"/>
								</div>
							</li>

					</ul>
				</fieldset>
				<div class="wrap paddedOnTop20 right">

				</div>

					<div id="waiveFee" style="display: none;">
						<input type="checkbox" value="1" name = "waiveme" id = "waiveme" />
					</div>

					<input type='hidden' name='pID' value='<?php echo $pID = (isset($_GET['pID']) ? $_GET['pID'] : '')?>'>
					<div class="wrap paddedOnTop20 right">
						<input type="submit" value="Continue" name="Submit" id='submit' class="submit button_v3 ga_interactive_form_field smallorange" onclick="return validatePersonalInfoForm()"  />
					</div>

				</form>

			</div>

	</div>

	</div>
	<div id="narrow_column">
	</div>
	</div>