<?php
if($groupID){
	require_once('travellog/helper/Class.NavigationHelper.php');
	if(NavigationHelper::isCobrand())
		Template::setMainTemplateVar('page_location', 'Home');
	else
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
}
else
//	Template::setMainTemplateVar('page_location', 'My Passport');
Template::setMainTemplateVar('title', $pageTitle);
Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
Template::includeDependentJs('/js/prototype.js');
Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
//Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
ob_start();
?>
<div id="body">
	<div id="content_wrapper" class="yui-skin-sam ltr">
		<div id="wide_column" class="layout2">
			<div class="section_detail">
				<div class="content">
					To register, please fill out the following form:
					<form action="?action=processRegistration" method="POST">
					<ul class="form paddingof10">
						<li class="line paddedOnTop size50">
							<label for="txtEmail" class="setMediumText strong">Email <span class="required">*</span><span id="errMsg1" class="required"><?php echo $existingUser = (isset($errMsg['existingUser']) ? $errMsg['existingUser'] : '');?></span></label>
							<input name="txtEmail" class="text size90" id="txtEmail1" type="text" >
							
						</li>
						<li class="line paddedOnTop size50">
							<label for="psword" class="setMediumText strong">Password <span class="required">*</span><span id="errMsg2" class="required"></span></label>
							<input name="psword" class="text size90" id="psword1" type="password"  >
						</li>
						<li class="line paddedOnTop size50">
							<label for="repsword" class="setMediumText strong">Re-type Password <span class="required">*</span></label>
							<input name="repsword" class="text size90" id="repsword" type="password" >
						</li>
					</ul>
					<div class="wrap paddedOnTop20">
						<input type="submit" value="Register" name="Submit" id='submit' class="submit button_v3 ga_interactive_form_field" onclick="return validateRegistration()" />
						
					</div>
					</form>
				</div>
			</div>
		</div>
		
	</div>
</div>

