<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if($groupID){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	else
	//	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentCss('/min/f=css/onlineApplications/online_application.css');
	Template::includeDependentJs('/js/prototype.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineAppFormValidator.js');
	Template::includeDependentJs('/js/onlineApplications/cisOnlineApp.js');
	ob_start();
	
	
$incjs = <<<BOF
<script type="text/javascript">
//<![CDATA[

	function showSecondProg(){
		var chkAddProg = $('chkAddProgram').checked;
		var divStyle = $('program2').style;
		
		if(chkAddProg==true){
			jQuery('#program2').slideDown("slow");
		}
		else {
			var program_2 = $('p2');
			var mOption2 = $('option2');
			program_2.options[0].selected = true;
			mOption2.options[0].selected = true;
			$('options_2').style.display = 'none';
			
			$('program1choice').checked = false;
			$('equally').checked = false;
			
			jQuery('#program2').slideUp("slow");
		}
	}


Event.observe(window,'load',function(){
//	var prog1 = $('p1');
//	alert(prog1.options[prog1.options.selectedIndex].value);
//	if(prog1.options[prog1.options.selectedIndex].value == ''){
		filterProgramsByTerm();
		filterOption(1);
		filterOption(2);	
//	}
	
	}
	);

//]]>
</script>
BOF;

		Template::includeDependent($incjs);
?>			





<!-- displays the profile component: added by neri 11-04-08 -->
<? //$profile->render()?>
<!-- end -->
	
<?  //$subNavigation->show(); ?>
		
<? /*
<script type="text/javascript">
		jQuery.noConflict();
</script>
*/ ?>



<!-- Rotating Photo -->
<div id="content_wrapper" class="yui-skin-sam ltr noTopPadding">
	<div id="wide_column" class="layout2">
		<div class="section_detail">
			<div class="content">

				<h2>Program Information</h2>
				<form method="post" action="OnlineApplication.php?step=2&action=save&pID=<?=$pID?>" id="programInformationForm">

					<fieldset class="contentBlock first">
						<legend class="display_none">Program Date</legend>
						<p class="title setSmallText dottedTopBorder">Fields with <span class="required">*</span> indicate required fields. </p>
						<ul class="form paddingof10 border_bottom_dotted">
								<li class="paddedOnTop line wrap">
									<?php $selectedTerm = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getTerm() : ''); ?>
									<div class="unit size50">
										<label for="term" class="setMediumText strong">Term Applying for <span class="required">*</span></label>
										<select id="term" name="ProgramInfo[Term]" class="size50" onchange="filterProgramsByTerm()">
											<option value="">Choose a Term</option>
											<?php foreach($terms as $term): ?>
												<option value="<?=$term->getID()?>" <?php if($selectedTerm==$term->getID()):?>selected<?endif;?> ><?=$term->getName()?></option>
											<?php endforeach; ?>
											
										</select>																							
									</div>
								</li>
								
								<li class="line wrap paddedOnBottom">
									<?php $year = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getYear() : ''); ?>
									<div class="unit size50">
										<label for="year" class="setMediumText strong">Year Applying for <span class="required">*</span></label>
										<select id="year" name="ProgramInfo[Year]" class="size50">
											<option value="">Choose a Year</option>
											<option value="2010" <?php if($year==2010):?>selected<?endif;?> >2010</option>
											<option value="2011" <?php if($year==2011):?>selected<?endif;?> >2011</option>
										</select>																							
									</div>
								</li>						
						</ul>
					</fieldset>
					
					<script type="text/javascript" >
						
						var jsPrograms = <?= json_encode($jsPrograms); ?>;
						var jsOptions = <?= json_encode($jsOptions); ?>;
						
						function filterProgramsByTerm(){
							var termOptions = $('term');
							var program_1 = $('p1');
							var program_2 = $('p2');
							var programToTerm = [];	
							
							var selectedTermID = termOptions.options[termOptions.options.selectedIndex].value;
							
							clearOptions();
							
							if(0 < selectedTermID){
								program_1.disabled = false;
								program_2.disabled = false;
								for(var i in jsPrograms){
								//	alert(jsPrograms[i]['termID']);
									var terms = jsPrograms[i]['termID'];
									for(var x in terms){
									//	alert(terms[x]);
										if(terms[x] == selectedTermID){
										//	alert('added '+jsPrograms[i]['name']);
											programToTerm[i] = jsPrograms[i];
										}
									}
									
								}
							}
							else {
								program_1.disabled = true;
								program_2.disabled = true;
							}
							
							var selectedProg1 = program_1.options[program_1.options.selectedIndex].value;
							var selectedProg2 = program_2.options[program_2.options.selectedIndex].value;
							
							
						//	if(program_1.options[program_1.options.selectedIndex].value == ''){
								program_1.options.length = 0;
								program_2.options.length = 0;

								program_1.options[program_1.options.length] = new Option('Choose Program...','');
								program_2.options[program_2.options.length] = new Option('Choose Program...','');

								for(var i in programToTerm){
									if(parseInt(i) >= 0){
										var optProg1_length = program_1.options.length;
										var optProg2_length = program_2.options.length;
										
										program_1.options[program_1.options.length] = new Option(programToTerm[i]['name'],programToTerm[i]['id']);
										program_2.options[program_2.options.length] = new Option(programToTerm[i]['name'],programToTerm[i]['id']);
										
										if(selectedProg1 == programToTerm[i]['id']){
											program_1.options[optProg1_length].selected = true;
											filterOption(1);
										}
										
										if(selectedProg2 == programToTerm[i]['id']){
											program_2.options[optProg2_length].selected = true;
											filterOption(2);
										}
									}
								}
								
								
						//	}
							
							
						}
						
						function filterOption(optNumber){
							var termOptions = $('term');
							var program = $('p'+optNumber);
							
							var selectedProgramID = program.options[program.options.selectedIndex].value;
							var selectedTermID = termOptions.options[termOptions.options.selectedIndex].value;
							
							var programOptions = [];
							
							if(0 < selectedProgramID){
								$('chkAddProgram').disabled = false;
								
								for(var i in jsOptions){
									var termID = jsOptions[i]['termID'];
									var programID = jsOptions[i]['programID'];
									
									if(selectedTermID == termID && selectedProgramID == programID){
									//	alert(jsOptions[i]['name']);
										programOptions[i] = jsOptions[i];
									}
								}
							}
							
							distributeOptions(programOptions, optNumber);
							
						}
						
						function distributeOptions(programOptions, optNumber){
							var optionDiv = $('options_'+optNumber);
							var progOption = $('option'+optNumber);
							
							var p1 = $('option1');
							
							//optionDiv.style.display = "none";
							
							if(0 < programOptions.length){
								
								optionDiv.style.display = "";
								
								var defaultVal = $('option'+optNumber+'_default').value;
							//	alert(progOption.name+' default value '+defaultVal);
								progOption.options.length = 0;
								
								progOption.options[progOption.options.length] = new Option('Choose Your Options', '');
								for(var i in programOptions){
									if(parseInt(i) >= 0){
										var optLength = progOption.options.length;
										progOption.options[progOption.options.length] = new Option(programOptions[i]['name'], programOptions[i]['id']);
									//	alert(programOptions[i]['id']);
										if(defaultVal == programOptions[i]['id']){
											progOption.options[optLength].selected = true;
										}
									}
								}
								
							}
							else {
								progOption.options.length = 0;
								
								progOption.options[progOption.options.length] = new Option('Choose Your Options', '');
								optionDiv.style.display = "none";
							}
							
						}
						
						function clearOptions(){
							var option1Div = $('options_1');
							var option2Div = $('options_2');
							var progOption1 = $('option1');
							var progOption2 = $('option2');
							
							progOption1.options[0] = new Option('Choose Your Options', '');
							progOption2.options[0] = new Option('Choose Your Options', '');
							
							option1Div.style.display = "none";
							option2Div.style.display = "none";
							
						}
					</script>
					
					<fieldset class="contentBlock first">
						<legend class="display_none">Program Chosen</legend>
						<ul class="form paddingof10 border_bottom_dotted">
								<li class="line wrap paddedOnBottom">
									<?php $prog1 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstProgramID() : ''); ?>
									<?php $prog2 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondProgramID() : ''); ?>
									<div class="paddedOnTop unit size50">
										<label for="p1" class="setMediumText strong">Program 1 <span class="required">*</span></label>
										<select id="p1" name="ProgramInfo[FirstProgramID]" class="" onchange="filterOption(1)" <?php if($prog1==""):?> disabled <?php endif; ?> >
											<option value="">Choose Program...</option>
											<?php foreach($programs as $program): ?>
												<option value="<?=$program->getID()?>" <?php if($prog1==$program->getID()):?>selected<?php endif;?> ><?=$program->getName()?></option>
											<?php endforeach; ?>
										</select>
									</div>
									
								</li>
								<li class="line wrap paddedOnBottom">
									<?php $opt1 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getFirstOptionID() : ''); ?>
									<div id="options_1" class="unit size50" style='display:none' >
										<label for="option1" class="setMediumText strong">Program 1 Options<span class="required">*</span></label>
										<input type="hidden" id="option1_default" value="<?=$opt1?>">
										<select id="option1" name="ProgramInfo[FirstOptionID]" class="">
											<option value="<?=$opt1?>" <?php if($opt1!=''):?>selected<?php endif;?> ></option>
										</select>
									</div>
								</li>
								<div class="line">
									<input type='checkbox' id='chkAddProgram' onclick="showSecondProg()" <?php if($prog2!=''):?>  checked <?php else: ?> disabled <?php endif; ?> class="checkbox_radio_list" /><label for="chkAddProgram" class="font-normal inline">I wish to apply to an additional program.</label>
								</div>	
							</ul>
								
							<div id='program2' style=<?php  echo $display = ($prog2=='') ? 'display:none;' : 'display:block;' ?>>
								<ul class="border_bottom_dotted">
									<li class="paddedOnTop line wrap">
										<div class="unit size50">
											<label for="p2" class="setMediumText strong">Program 2 <span class="required">*</span></label>
											<select id="p2" name="ProgramInfo[SecondProgramID]" class="" onchange="filterOption(2)" <?php if($prog2==""):?> disabled <? endif; ?> >
												<option value="">Choose Program...</option>
												<?php foreach($programs as $program): ?>
													<option value="<?=$program->getID()?>" <?php if($prog2==$program->getID()):?>selected<?endif;?> ><?=$program->getName()?></option>
												<?php endforeach; ?>
											</select>	
										</div>
										
										
									</li>
									<li class="line wrap paddedOnBottom">
										<?php $opt2 = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getSecondOptionID() : ''); ?>
										<div id="options_2" class="unit size50" style='display:none'>
											<label for="option2" class="setMediumText strong">Program 2 Options <span class="required">*</span></label>
											<input type="hidden" id="option2_default" value="<?=$opt2?>">
											<select id="option2" name="ProgramInfo[SecondOptionID]" class="" >
												<option value="<?=$opt2?>" ></option>
											</select>
										</div>
									</li>
									<li class="paddedOnBottom20">
										<div id="priority" class="" style=''>
											<label for="priority" class="setMediumText strong">Priority Option <span class="required">*</span></label>
											<?php $priorityOpt = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getPriorityOption() : 0); ?>
											<label id="priorityOptLabel" class="font-normal inline"></label>
											<ul>
												<li><input type='radio' value="1" id="program1choice" name="ProgramInfo[PriorityOption]" <?php if($priorityOpt==1):?>checked<?php endif; ?> class="checkbox_radio_list"><label for="program1choice" class="font-normal inline"> Program 1 is my first choice, and Program 2 is my second choice.</label></li>
												<li><input type='radio' value="2" id="equally" name="ProgramInfo[PriorityOption]" <?php if($priorityOpt==2):?>checked<?php endif; ?> class="checkbox_radio_list"><label for="equally" class="font-normal inline"> I am equally interested in both Program 1 and Program 2.</label></li>
											</ul>
										</div>
									</li>	
								</ul>
							</div>
					</fieldset>
					
					<fieldset class="contentBlock first">
						<legend class="display_none">Interested in</legend>
						<ul class="form paddingof10 border_bottom_dotted">
							<li class="line paddedOnTop wrap paddedOnBottom">	
								<div class="unit size50">
									<?php $interest = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getInterest() : ''); ?>
									<label for="interested" class="setMediumText strong">Are you interested in studying abroad for a full academic year?</label>
									<input type="text" id="interested" class="text size95" size="23" name="ProgramInfo[Interest]" value="<?=$interest?>"/>																								
								</div>
								<div class="unit lastUnit formhelptext size40">
									
								</div>
							</li>
								
							<li class="line wrap size50">	
								<label for="volunteer" class="setMediumText paddedOnTop strong">Are you interested in volunteering while abroad?</label>
								<div class="unit size50">
									<?php $volunteering = (isset($objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]) ? $objArr[CISOnlineAppFieldsMapper::PROGRAM_INFO]->getVolunteering() : 2); ?>
									<ul class="checkbox_radio_list">
										<li><input type="radio" class="checkbox_radio_list paddedSOnTop" id="y" value="2" name="ProgramInfo[Volunteering]" <?php if($volunteering==2): ?>checked<?endif;?> /> <label for="y" class="font-normal">Yes</label></li>
										<li><input type="radio" class="checkbox_radio_list paddedSOnTop" id="n" value="3" name="ProgramInfo[Volunteering]" <?php if($volunteering==3): ?>checked<?endif;?> /> <label for="n" class="font-normal">No</label></li>
									</ul>
								</div>
							</li>		
						</ul>
					</fieldset>
					
					<div class="wrap paddedOnTop20 right">
					<? /*<a class="button_v3" href="OnlineApplication.php?step=1&action=view&pID=<?=$pID?>&backTrack"  > */ ?>
						<a class="button_v3" onclick="return backTrackAction('programInformationForm')" >
							<strong>Back</strong>
						</a>
						
						<input type="submit" value="Next/Save" id="submit" name="<?php echo $x = (isset($buttonName) ? $buttonName : 'Submit');?>" class="submit button_v3 ga_interactive_form_field" onClick="return validateProgramInfoForm()"/>
						
						<a class="button_v3" href="javascript:void(0);" onclick="return saveAndExit('programInformationForm')">
							<strong>Save and Quit</strong>
						</a>											
					</div>
				</form>

			</div>

		</div>		

	</div>
	<div id="narrow_column">
			<?php echo $applicationProgressMenu?>
	</div>
</div>
		