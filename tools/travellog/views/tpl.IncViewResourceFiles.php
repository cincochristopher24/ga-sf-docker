<div id="content" >

<? if($reccount): ?>	
	<table cellspacing="0" cellpadding="0">															
			<tr>																	
				<th id="resource_head_filename">Filename</th>
				<th id="resource_head_caption">Caption</th>																		
				<? if($owner): ?>
						<th>Actions</th>
				<? endif; ?>																		
			</tr>
			
			<?for($x=$start; $x<$endrow; $x++): ?>																	
					<tr>	
						<td>	
							<img src="images/pdf.gif" alt="" />
							<a href="<? echo $resourcefiles[$x]->getResourcefileLink(); ?>">										
								<?  echo $resourcefiles[$x]->getFileName();?>
							</a>		
						</td>
						<td>																					
							<?  echo $resourcefiles[$x]->getCaption();?>&nbsp;
						</td>
						
						<? if($owner): ?>																				
							<td class="actions">								
								<ul class="mini_actions">									
									<li>
										<?if(trim($resourcefiles[$x]->getCaption())): ?>
											<a href="javascript:void(0)" onclick="ResourceFile.showedit(<?=$resourcefiles[$x]->getResourceFileID();?>)"> 
												<span>	Edit Caption </span>
											</a>
										<?else:?>
											<a href="javascript:void(0)" onclick="ResourceFile.showedit(<?=$resourcefiles[$x]->getResourceFileID();?>)"> 
												<span>	Add Caption </span>
											</a>
										<?endif; ?>																								
									</li>
									
									<li>
										<a href="<? echo $edit_resourcefilelink.$resourcefiles[$x]->getResourceFileID();  ?>">
											<span>Change</span>
										</a>
									</li>
									
									<li>
										<a class="delete" href="javascript:void(0)" onclick="ResourceFile.Delete(<?= $resourcefiles[$x]->getResourceFileID(); ?>)">
											<span> Delete </span>
										</a>	
									</li>																											
								</ul>
								
								<?if($context == 'admingroup'):?>
									<br /><a href="javascript:void(0)" onclick="ResourceFile.showprivacypreference(<?= $resourcefiles[$x]->getResourceFileID();?>)"> 
										Set Privacy Preference
									</a>
									(
									<?if($resourcefiles[$x]->getPrivacypreference() ==0):?>not show<?endif;?>
									<?if($resourcefiles[$x]->getPrivacypreference() ==1):?>show to members<?endif;?>
									<?if($resourcefiles[$x]->getPrivacypreference() ==2):?>show all<?endif;?>
									)
									
								<?endif; ?>															
							</td>																			
						<? endif; ?>																				
					</tr>
					
			<? endfor; ?>							
	
	</table>
	
	
		
		<? $page->showPagination(array('label' => 'Files')); ?>
		
<? else: ?>
		<div class="help_text">
			<p>
				<?=$norecfound; ?>	
																	
				<? if($owner): ?>
					Do you want to <a href="<?= $uploadrfiles ?>" 
						class="up_photo">upload</a> a file?
					<? endif; ?>		
			</p>
												
		</div>
<? endif; ?>


</div>


											
									
								
						
					
						