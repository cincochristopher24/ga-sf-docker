<?php
require_once('serverConfig/Class.ServerConfig.php');
$showClickTale = strtolower(ServerConfig::getInstance()->getServerType()) == 'production' && 'www.goabroad.net' == $_SERVER['SERVER_NAME'] && (array_key_exists('REMOTE_ADDR', $_SERVER) && '121.96.35.154' == $_SERVER['REMOTE_ADDR']);
require_once('travellog/factory/Class.FileFactory.php');

$obj_factory = FileFactory::getInstance();

// DEFAULTS
if (!isset($title)) 		  $title 			= 'GoAbroad Network: Online Community for Travelers';
if (!isset($metaDescription)) $metaDescription  = '';
if (!isset($metaKeywords))    $metaKeywords     = 'travel community, travel network, travel online, travel website, travel web site, travel blog, travel note, free travel blogs, travel blogs, free travel blog, personal travel blogs, travel notes, travel journal, travel community website, travel notes blog, free travel journal, free online travel journal, travel journal online, blogs, journals, travel diaries, travel journals, travel diary, traveler, travelers community, travelers community, traveler community, free travel journals, free online travel journals, online travelers community, travel review, 
travel comments, travel observations, trip journals, travelogues, travel reviews, travel guide, travel advice, travel photos, travel maps';
if (!isset($layoutID)) 		  $layoutID 		= 'basic';
if (!isset($page_location))	  $page_location 	= '';

if (!isset($isInHomepage))    $isInHomepage     = false;

//header('Content-Type: text/html, charset=utf-8');


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<title><?=$title?></title>
<meta name="google-site-verification" content="89qZN71flfxjWt0hVSCKEeqgAAN0dTNcPOhrJT-zCSg" />
<meta name="description" content="<?=$metaDescription ?>" />
<meta name="keywords" content="<?=$metaKeywords?>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<?php
	/** Additional meta tags needed only in /index.php - icasimpan Apr 25,2007
	 *  y_key     => Yahoo Site Explorer
	 *  verify_v1 => Google Sitemaps
	 *  NOODP     => Dmoz title not to be used in search engines results pages
	 *  NOYDIR    => Yahoo directory title not to be used in search engines results pages
	 */ 
	if ($_SERVER['SCRIPT_NAME'] == '/index.php'):
		echo '<meta name="y_key" content="9f51662a4b1147d3" />' . "\n";
		echo '<meta name="verify-v1" content="t7aF38KKCgN0Y6Z6vX+t5jnjJ+8J1oR/rCT1mQz1EOs=" />' . "\n";
		echo '<meta name="msvalidate.01" content="3666641D4D3CF0046645F59413DE3E59" />' . "\n";
		echo '<meta name="robots" content="NOODP" />'  . "\n";
		echo '<meta name="robots" content="NOYDIR" />' . "\n";
		
	endif;
        // NOTE: __utm.js =>  GoAbroad in-house Urchin Web Analytics - iaacasimpan Nov 7,2007
?>

<?php Template::renderFBRequiredMetaTags(); ?>

<!-- <link type="text/css" rel="stylesheet" href="/min/b=css&amp;f=reset.css,type.css,layout.css,widget.css,section.css,forms.css" /> -->
<link type="text/css" rel="stylesheet" href="/min/g=LayoutMainCss" />
<!-- <link href="/css/base.css" rel="stylesheet" type="text/css" /> -->

<style type="text/css">
	@import url("/min/f=css/<?=$layoutID?>.css");
</style>
<?php if ('/css/' != $obj_factory->getPath('CSS')): ?>
<link href="<?=$obj_factory->getPath('CSS')?>layout.css" rel="stylesheet" type="text/css" />
<?php endif; ?>
<?php 
 Template::includeDependentJs('/min/g=LayoutMainJs', array('include_here' => true)); // minified by lance
?> 

<!-- compliance patch for microsoft browsers -->
<!--[if lt IE 8]>
<link href="/css/main.ie.fix.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
//Fix image cache problem with IE 6
try {
  document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
</script> 

<![endif]-->
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-467553-56']);
	_gaq.push(['_setDomainName', '.goabroad.net']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
</head>
<body id="<?=$layoutID?>">
<!--script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript"> </script--> 
<?php if($showClickTale): ?>
	<!-- ClickTale Top part -->
	<script type="text/javascript">
		var WRInitTime=(new Date()).getTime();
	</script>
	<!-- ClickTale end of Top part -->
<?php endif;?>

<div id="fb-root"></div>
<?php 
	$fbAppID = (false === stripos($_SERVER['SERVER_NAME'], 'staging.goabroad.net')) ? '122355307794078' : '119585118064120';
	$fbScript = <<<FBSCRIPT
<script>
  window.fbAsyncInit = function() {
    FB.init({appId: '$fbAppID', status: true, cookie: true,
             xfbml: true});
  };
  (function() {
    var e = document.createElement('script'); e.async = true;
    e.src = document.location.protocol +
      '//connect.facebook.net/en_US/all.js';
    document.getElementById('fb-root').appendChild(e);
  }());
</script>
FBSCRIPT;
	Template::includeDependent($fbScript, array('include_here' => true));
?>	
<div id="wrapper">
	<div id="inner_wrapper">
	<?php include($obj_factory->getPath('HEADER_FOOTER').'tpl.IncHeader.php'); ?>
	
	<div id="body">		
		<?=$contents?>
	</div>
	  
	
		<?php include($obj_factory->getPath('HEADER_FOOTER').'tpl.Footer.php'); ?>
	</div>
</div>

<?php if($showClickTale): ?>
	<!-- ClickTale Bottom part -->
	<div id="ClickTaleDiv" style="display: none;"></div>
	<script type="text/javascript">
		if(document.location.protocol!='https:') {
	  		document.write(unescape("%3Cscript src='http://images.goabroad.com/js/WRb.js' type='text/javascript'%3E%3C/script%3E"));			
		}

		if(typeof ClickTale=='function') {
			ClickTale(12317,0.518,"www02");
		}
				
	</script>
	<!-- ClickTale end of Bottom part -->
	
	<?// added woopra tracker for goabroad.net hompepage only, per request of Joel Tan?>
	<?php if ($isInHomepage):?>
		
		<!-- Woopra Code Start -->
		<script type="text/javascript"
		src="//static.woopra.com/js/woopra.v2.js"></script>
		<script type="text/javascript">
		woopraTracker.track();
		</script>
		<!-- Woopra Code End -->

		
	<?php endif; ?>
	
<?php endif;?>

</body>
</html>
<?php flush(); // TEMPORARY FIX ?>