<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	require_once('travellog/helper/Class.NavigationHelper.php');
	if (NavigationHelper::isCobrand()) {
		Template::setMainTemplateVar('page_location', 'Home');
	}else{
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}		
	Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
	Template::includeDependentJs('/js/interactive.form.js');	
	Template::setMainTemplateVar('layoutID', 'survey');	
	//Template::includeDependentCss('/css/formeditor/survey.css');
	echo $profile->render();
	$subNavigation->show();
?>
<div id="content_wrapper">
	<?php if('SHOW_FORM'==$action): ?>
		<div class="section" id="survey_preview">
			<h1><span><?= $surveyForm->getName() ?></span></h1>
			<div class="content">
				<?php if($isAdvisor): ?>
			      	<p>NOTE:  Since you are administering this survey, you are not a qualified participant. You can answer this form for testing purposes only, your answers will not be saved.<?php if($surveyForm->isLocked()): ?> Also, this survey is marked as closed. No traveler can participate in this survey.<?php endif; ?></p>			
			    <?php endif;?>
				<p><?= nl2br($surveyForm->getCaption()) ?></p>
				<form name="frmSurvey" action="/surveyform.php?frmID=<?=$surveyForm->getSurveyFormID()?>&gID=<?= $gID ?>" method="post" class="interactive_form">
					<?php if($isAdvisor): ?>
					<input name="isAdvisor" type="hidden" value="1" />
					<?php endif;?>
					<input name="frmID" type="hidden" value="<?=$surveyForm->getSurveyFormID()?>" />
					<input name="gID" type="hidden" value="<?=$gID?>" />
					<ul class="form">			
						<?php $fields = $surveyForm->getSurveyFormFields();	foreach($fields as $iFld):?>
							<li>
									<?php if(in_array($iFld->getSurveyFieldID(),$errFields)) :?>
									<div class="errors">
										<strong>This field is required!</strong>								
									<?php endif;?>
									<label for="fld_<?=$iFld->getSurveyFieldID()?>" <?php if(SurveyField::SECTION_BREAK == $iFld->getFieldType()){echo 'class="sectionBreak"';} ?>>

										<?= nl2br($iFld->getCaption()) ?>
										<?php if($iFld->isRequired()):?>
											<span class="required">*</span>
										<?php endif; ?>
									</label>

									<ul>
										<?php 
											$inputs = $iFld->getFieldInputs();
											foreach($inputs as $iInput):
										 ?>
										 <li class="<?= $iFld->isOrientationHorizontal() ? 'horizontalOriented' : 'verticalOriented'?>">
											 <?php 	if($iInput->getInputType() == InputControl::TEXTBOX): ?>
											 	<?php 
											 		$class = (($iInput->getAttribute('size')==0)?'small_text':(($iInput->getAttribute('size')==1)?'medium_text':'large_text'));						 		
													$txtVal = count($formFieldValues[$iFld->getSurveyFieldID()])
														? $formFieldValues[$iFld->getSurveyFieldID()][0]
														: null;
											 		if($iInput->getAttribute('isMultiline')):
											 	?>
											 		<textarea name="fld_<?=$iFld->getSurveyFieldID() ?>" class="<?= $class ?>" ><?= $txtVal ?></textarea>
											 	<?php else:?>
											 		<input name="fld_<?=$iFld->getSurveyFieldID() ?>" type="text" class="<?= $class ?> text" value="<?= $txtVal ?>" />
											 	<?php endif;?>
											<?php
											 	elseif($iInput->getInputType() == InputControl::CHECKBOX):
											 		$isChecked = (in_array($iInput->getAttribute('text'),$formFieldValues[$iFld->getSurveyFieldID()]));
										 	?>
											 	<input type="checkbox" name="fld_<?=$iFld->getSurveyFieldID() ?>[]" <?= ($isChecked)?'checked':''?> value="<?= $iInput->getAttribute('text') ?>" />
												<? if (strlen($iInput->getAttribute('text')) > 100): ?>
													<?= substr($iInput->getAttribute('text'), 0, 100); ?>...
												<? else: ?>
													<?= $iInput->getAttribute('text'); ?>
												<? endif;?> 						 	
											 <?php 	
											 	elseif($iInput->getInputType() == InputControl::RADIO): 
											 		$isChecked = (in_array($iInput->getAttribute('text'),$formFieldValues[$iFld->getSurveyFieldID()]));
											 ?>						 
											 	<input type="radio" name="fld_<?=$iFld->getSurveyFieldID() ?>[]" <?= ($isChecked)?'checked':''?> value="<?= $iInput->getAttribute('text') ?>" />
												<? if (strlen($iInput->getAttribute('text')) > 100): ?>	
													<?= substr($iInput->getAttribute('text'), 0, 100); ?>...
												<? else: ?>
													<?= $iInput->getAttribute('text'); ?>
												<? endif; ?>
											 <?php 	
											 	elseif($iInput->getInputType() == InputControl::COMBOBOX): 
											 	$class = (($iInput->getAttribute('size')==0)?'small_select':(($iInput->getAttribute('size')==1)?'medium_select':'large_select'));
											 ?>
											 	<select name="fld_<?= $iFld->getSurveyFieldID(); ?>" class="<?=$class?>" >
											 		<?php $opts = $iInput->getAttribute('options');?>
											 		<?php 
											 			$i = 0;						 			
											 			foreach($opts as $iOpt):
											 				$isSelected = (in_array($iOpt,$formFieldValues[$iFld->getSurveyFieldID()]));
											 		?>
											 			<option value="<?= $iOpt?>" <?= ($isSelected) ?'selected':''?> ><?= $iOpt ?></option>
											 		<?php
											 			endforeach;
											 		?>
											 	</select>								 
											 <?php endif; ?>
										 </li>
										 <?php endforeach; ?>
									</ul>
									<?php if($iFld->isOrientationHorizontal()): ?>
									 	<div class="clear" ></div>
									<?php endif; ?>
									<?php if(in_array($iFld->getSurveyFieldID(),$errFields)) :?>
										</div>
									<?php endif;?>
							</li>
						<?php endforeach; ?>	
						<li class="actions">
							<input type="submit" value="Submit" class="submit ga_interactive_form_field" />
							<input type="reset" value="Reset" class="button ga_interactive_form_field" />
						</li>
					</ul>
				</form>
				<div class="clear"></div>
			</div>
			<div class="foot"></div>
		</div>
	<?php elseif('SHOW_MESSAGE'==$action):?>
		<div class="section" id="survey_preview">
			<h1><span>Survey</span></h1>
			<div class="content">
				<p><?= $message ?></p>								
			</div>
			<div class="foot"></div>
		</div>
	<?php endif;?>
</div>




