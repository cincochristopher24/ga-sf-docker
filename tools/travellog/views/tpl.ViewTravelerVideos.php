<?
	echo $profile->render();
	echo $subNavigation->show();
?>

<div id="rundown">
	<!-- Begin Content Wrapper -->
	<div id="content_wrapper">
		<?php 
			if( !is_null($commentsview) ){
				echo $commentsview->fetchShoutOutPopUp();
			}
		?>
		
		<!-- Begin Wide Column -->
		<div id="wide_column" class="layout2">
			<? if ('journal' == $type): ?>
				<div class = "media_control" align = "right">
					<? if ($hasVideos && $isOwner): ?> 
						<input class="button_v3" type="button" value="Add Videos" onclick = "Video.showAddCustomPopUp('journal', <?php echo $travelLogID ?>, <?php echo $travelerID ?>, 'Traveler')"/>
					<? endif; ?>
					<? if (1 < count($journalEntries)): ?>
						<div>
							<strong> Select an Entry: </strong>
							<select id = "cmbEntries" onchange = "VideoAlbum.setCurrentJournalEntry(this.options[this.selectedIndex].value, <?= $travelerID ?>, 'Traveler', <?= $travelID ?>); return false;">
								<option value = "0" <? if (!$travelLogID): ?> selected = "selected" <? endif; ?>>--Displaying all entries--</option>
								<? foreach ($journalEntries as $entry): ?>
									<option value = "<?= $entry->getTravelLogID() ?>" <? if ($travelLogID == $entry->getTravelLogID()): ?> selected = "selected" <? endif; ?>><?= $entry->getTitle() ?></option>
								<? endforeach; ?>
							</select>
						</div>
					<? endif; ?>
				</div>
			<? endif; ?>
			
			<div id="video_main_view" class="section">
				<? if ($isOwner): ?>
					<? if (!$travelID && !isset($travelLogID) && !$albumID): ?>
						<div class="notification text_content">
							<?php if(!$cntCollections): ?>
								<h2> You have not yet added videos. </h2>
								<p> To get started, you can add a video album below. To make your video appear on a journal entry, you first need to <a href="/journal.php?action=add">write a journal</a> and add your video to a journal entry. </p>
								<p> We currently only support Google Video, YouTube, and Vimeo videos. If you don't have a Google Video, YouTube, or Vimeo account, you will need to create one separately. </p>
							<?php else: ?>
								<h1>You have <?= $cntCollections ?> Video Album<? if(1 < $cntCollections): ?>s<? endif; ?>.</h1>
								<p> 
									To add a video to an existing album, select the album on the right.<br/>
									To add a new album, just type in the album name below and click <strong>Add New Album</strong>. 
								</p>
							<?php endif; ?>
						</div>
						
						<div class="display">
							<a href="http://www.youtube.com/" target="_blank" title="Add videos from YouTube">
								<img src="/images/youtube_logo.gif" alt="YouTube: Broadcast Yourself (TM)" width="124" height="52"/>
							</a>
							<a href="http://video.google.com/" target="_blank" title="Add videos from Google Video">
								<img src="/images/google_logo.gif" alt="Google Video (TM) BETA" width="145" height="52"/>
							</a>
							<a href="http://www.vimeo.com/" target="_blank" title="Add videos from Vimeo">
								<img src="/images/vimeo_logo.gif" alt="Vimeo" width="138" height="52"/>
							</a>
						</div>	
						
						<div class="quick_create">
							<h2> Add a Video Album </h2>
							<p> Title </p>
							<div>
								<form action = "/videoAlbum.php?action=saveAlbum&amp;travelerID=<?= $travelerID ?>&amp;albumID=0" method = "post" onSubmit = "return VideoAlbum.validateAlbum()">
									<input type="text" size="40" id = "txtAlbumTitle" name = "txtAlbumTitle" value="Type Album Name Here" onFocus = "this.value = ''">
									<input type="submit" value="Add New Album" class="button_v3" onclick = "return true;">
								</form>
							</div>
						</div>
					<? else: ?>
						<? if ($hasCreatedAnAlbum || !$hasVideos): ?>
							<h1> <span id="collectionTitle"> <?= htmlspecialchars($collectionTitle) ?> <em>Videos</em> </span> </h1>
							<div class="content">
								<div class="notification middle">
									<h2>You have not yet added videos to this album.</h2>
									<?php if('album' == $type): ?>
										<input class="button_v3" type="button" value="Add Videos" onclick = "Video.showAddCustomPopUp('album', <?php echo $albumID ?>, <?php echo $travelerID ?>, 'Traveler')"/>
									<?php else: ?>
										<input class="button_v3" type="button" value="Add Videos" onclick = "Video.showAddCustomPopUp('journal', <?php echo $travelLogID ?>, <?php echo $travelerID ?>, 'Traveler')"/>
									<?php endif; ?>
								</div>			
							</div>
						<? elseif ($hasVideos): ?> 
							<h1>
								<span id="collectionTitle"> <?= htmlspecialchars($collectionTitle) ?> <em>Videos</em></span>
								<span class="header_actions"> <? if ('album' == $type): ?> <input class="button_v3" type="button" value="Add Videos" onclick = "Video.showAddCustomPopUp('album', <?php echo $albumID ?>, <?php echo $travelerID ?>, 'Traveler')" /> <? endif; ?> </span>	
							</h1>
							<div class="content">
								<? if ($videoID): ?>
									<h3>
										<? if ($video->getTitle()): ?> <span id="videoTitle"> <?= htmlspecialchars($video->getTitle()) ?> </span> <? endif; ?>
										<? if ('journal' == $type): ?>
											<span id = "travelLogTitle"><a href="/journal-entry.php?action=view&amp;travellogID=<?= $video->getParentTravelLog()->getTravelLogID() ?>"> <?= htmlspecialchars($video->getParentTravelLog()->getTitle()) ?> </a></span>
										<? endif; ?>
									</h3> 
									<div id="video_viewer">
										<?= $playVideo ?>						
									</div>
									<? if ($video->getCaption()): ?> 
										<div id="caption" class="videoCaption clear"> <?= htmlspecialchars($video->getCaption()) ?> </div> 
									<? endif; ?>
								<? endif; ?>
								
								

								<div class="section comments" id="comments">
									<?php
									 	if( !is_null($commentsview) ){
											echo $commentsview->renderAjax();
										}
									?>
								</div>
								
								<ul id="videosList" class="video_lists">
									<?= $videosList ?>
								</ul>
							</div>
						<? endif; ?>
					<? endif; ?>
				<? else: ?>
					<h1> <span id="collectionTitle"> <?= htmlspecialchars($collectionTitle) ?> <em>Videos</em> </span> </h1>
					<div class="content">
						<? if ($videoID): ?>
							<h3>
								<? if ($video->getTitle()): ?> 
									<span id="videoTitle"> <?= htmlspecialchars($video->getTitle()) ?> </span> 
								<? endif; ?>
								<? if ('journal' == $type): ?>
									<span id = "travelLogTitle"> <a href="/journal-entry.php?action=view&amp;travellogID=<?= $video->getParentTravelLog()->getTravelLogID() ?>"> <?= htmlspecialchars($video->getParentTravelLog()->getTitle()) ?> </a> </span>
								<? endif; ?>
							</h3> 
							<div id="video_viewer">
								<?= $playVideo ?>						
							</div>
							
							<? if ($video->getCaption()): ?> 
								<div id="caption" class="videoCaption clear"> <?= htmlspecialchars($video->getCaption()) ?> </div> 
							<? endif; ?>
						<? endif; ?>
						
						
						<div class="section comments" id="comments">
							<?php
							 	if( !is_null($commentsview) ){
									echo $commentsview->renderAjax();
								}
							?>
						</div>
						
						<ul id="videosList" class="video_lists">
							<?= $videosList ?>
						</ul>
					</div>
				<? endif; ?>
			</div>
			
			<div class="jRight">
				<?php if( isset($fbLike) ): ?>
					<?php echo $fbLike->render(); ?>
				<?php endif; ?>
			
				<?php if( isset($sharebar) ): ?>
					<?php echo $sharebar->render(); ?>
				<?php endif; ?>
			</div>
		</div>
		<!-- End Wide Column -->
		
		<? if ($cntCollections): ?>
			<!-- Begin Narrow Section -->
			<div id="narrow_column">
				<div class="section nbsection">
					<h2 style="margin-bottom:10px;border:none;"> 
						<span>Collections</span> 
						<? if ($isOwner): ?>
							<span class="header_actions"><a href="/video.php?action=getTravelerVideos&amp;travelerID=<?= $travelerID ?>">+Add Video Album</a></span>
						<? endif; ?>
					</h2>
					<div class="content">
						<?= $collections ?>
					</div>
				</div>
			</div>
			<!-- End Narrow Section -->
		<? endif; ?>	
	</div>
	<!-- End Content Wrapper -->
</div>

<script type = "text/javascript">
	window.onload = function() {
		<? if ($fail): ?>
			CustomPopup.initPrompt("Sorry but a problem was encountered while retrieving the attributes of your video.");
			CustomPopup.createPopup();
		<? elseif ($hasCreatedAnAlbum): ?>
			CustomPopup.initPrompt("You have successfully added an album.");
			CustomPopup.createPopup();
		<? elseif ($hasEditedAnAlbum): ?>
			CustomPopup.initPrompt("You have successfully edited an album.");
			CustomPopup.createPopup(); 
		<? elseif ($hasDeletedAnAlbum): ?>
			CustomPopup.initPrompt("You have successfully deleted an album.");
			CustomPopup.createPopup();
		<? elseif ($hasFailedSavingAnAlbum): ?>
			CustomPopup.initPrompt("Sorry but this album cannot be saved since it is already existing in your collection of video albums.");
			CustomPopup.createPopup();
		<? elseif ($hasCreatedAVideo): ?>
			CustomPopup.initPrompt("You have successfully added a video.");
			CustomPopup.createPopup();
		<? elseif ($hasEditedAVideo): ?>
			CustomPopup.initPrompt("You have successfully edited a video.");
			CustomPopup.createPopup(); 
		<? elseif ($hasDeletedAVideo): ?>
			CustomPopup.initPrompt("You have successfully deleted a video.");
			CustomPopup.createPopup();
		<? endif; ?>
	}
</script>