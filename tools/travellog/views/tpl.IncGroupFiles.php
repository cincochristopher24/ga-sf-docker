<?php
/*
 * Created on Oct 20, 2006
 * Author: Czarisse Daphne P. Dolina
 * Tpl.IncGroupFiles.php - displays group uploaded resource files
 */
?>
<div class="section" id="files">
	<h2>
		<span>Files</span>
		<? if (TRUE == $isAdminLogged) { ?>
			<span class="header_actions">
				<a href="/resourcefiles.php?action=upload&amp;cat=admingroup&amp;genID=<?= $grpID ?>" class="add">Upload File</a> |
				<a href="/resourcefiles.php?cat=admingroup&amp;action=view&amp;genID=<?= $grpID ?>" class="add">Manage Files</a>
			</span>
		<? } ?>
	</h2>
	<div class="content" style="overflow: auto;">				
			<ul class="elements">	
			<?  for($idx=0;$idx<count($grpResourceFiles);$idx++){
					$eachresourcefile = $grpResourceFiles[$idx];
			?>
			
				<li>	
						<a class="file_title" href="<?= "http://".$_SERVER['SERVER_NAME']."/". $eachresourcefile->getResourcefileLink(); ?>" target="_blank"> 
							<? if ( 0 < strlen($eachresourcefile->getCaption()))
								echo $eachresourcefile->getCaption(); 
							else
								echo $eachresourcefile->getFileName(); ?>
						</a><br />
							<div class="control">
							<? if (TRUE == $isAdminLogged) { ?>
								<a href="/resourcefiles.php?cat=admingroup&amp;action=delete&amp;genID=<?= $grpID ?>&amp;rfileID=<?= $eachresourcefile->getResourceFileID() ?>" onclick="javascript:return confirm('Do you want to DELETE this Resource File?');" >Delete</a>
							<? } ?>
							</div>
				</li>				
			<? } ?>
			</ul>					
			<? if (0 == count($grpResourceFiles) && $isAdminLogged) { ?>
				<? /* if ( $is_parent_group ) : ?>
					<p class="side_help_text">There are no files added to this group.</p>
				<? else : ?>
					<p class="side_help_text">There are no files added to this subgroup.</p>
				<? endif */?>
				<div class="sg_description">
					<h3></h3>
					<p>
						Upload private or public PDF documents.
					</p>
					<a class="button_v3 goback_button jLeft" href="/resourcefiles.php?action=upload&amp;cat=admingroup&amp;genID=<?= $grpID ?>"><strong>Upload File</strong></a>
				</div>
			<? } ?>			
		</div>		
</div>
