<?php
/*
	Filename: 	  tpl.IncTravelPlanMain.php
	Author:		  Jonas Tandinco
	Purpose:  	  partial for displaying the list of travels
	Date created: Oct/09/2007

	EDIT HISTORY:
	
	Oct/09/2007 by Jonas Tandinco
		1.	removed link to journals
		
	Oct/18/2007 by Jonas Tandinco
		1.	removed "View Less" and "View More"
		
	Oct/22/2007 by Jonas Tandinco
		1.	added "See who else is going" to non-past travels
		
	Oct/31/2007 by Jonas Tandinco
		1.	changed method calls for traveldestination object for its country field
		2.	enabled "See who else is going"
		
	Nov/14/2007 by Jonas Tandinco
		1.	removed links to travelscheduler.php and pointed to the new travelplans.php
		
	Feb/22/2008 by Jonas Tandinco
		1.	see who else is going only enabled if logged on user is viewing his own travel plans
		
	Feb/26/2008 by Jonas Tandinco
		1.	separated design if only one country is selected for a travel plan
*/
?>	
<?php foreach ($travels as $travel) : ?>
	<div id="travel_entry_wrapper">
		<?php if (count($travel->getDestinations()) == 1) : ?>
			<?php
				$destination = $travel->getDestinations();
				$destination = $destination[0];
			?>
			<div id="top_level">
				<div class="date_section">
					<h4 class="top_level_date">
						<?php echo $destination->getDateRange() ?>
					</h4>
					<?php if ($owner) : ?>
						<div class="actions">
							<a href="travelplans.php?action=edit&travelwishid=<?php echo $travel->getTravelWishID() ?>">
								<img src="images/goabroad_app/edit.gif"/>
							</a>
							<a href="travelplans.php?action=delete&travelwishid=<?php echo $travel->getTravelWishID() ?>" onclick="return confirm('Are you sure you want to delete this travel?')">
								<img src="images/goabroad_app/delete.gif"/>
							</a>
							</div>
						<div class="clear"></div>
					<?php endif; ?>
				</div>
				<div class="mainTravel_content">
					<h3>
						<?php echo htmlentities($travel->getTitle()) ?>
					</h3>
					<p><?php echo htmlentities($travel->getDetails()) ?></p>
					<h4 class="dest_country"><!--<img class="destination_bullet" src="/images/goabroad_app/bullet.gif"/>-->
						<?php echo $destination->getCountry()->getName() ?>
					</h4>
					<p class="destination_desc"><?php echo htmlentities($destination->getDescription()) ?></p>
					<?php if (!$isPast && strtotime($destination->getStartDate()) > time() && $owner) : ?>
						<a href="/travelers.php?action=PlanningToGo&locationID=<?php echo $destination->getCountry()->getLocationID() ?>"><img src="images/img/seewhoelse_icon.gif"></a>
					<?php endif; ?>										
				</div>
				<div class="clear"></div>
			</div>
		<?php else : ?>
			<div id="top_level">
				<div class="date_section">
					<h4 class="top_level_date">
						<?php echo $travel->getDateRange() ?>
					</h4>
					<div class="clear"></div>
					<?php if ($owner) : ?>
						<div class="actions">
							<a href="travelplans.php?action=edit&travelwishid=<?php echo $travel->getTravelWishID() ?>">
								<img src="images/goabroad_app/edit.gif"/>
							</a>
							<a href="travelplans.php?action=delete&travelwishid=<?php echo $travel->getTravelWishID() ?>" onclick="return confirm('Are you sure you want to delete this travel?')">
								<img src="images/goabroad_app/delete.gif"/>
							</a>
							</div>
						<div class="clear"></div>
					<?php endif; ?>
				</div>
				<div class="mainTravel_content">
					<h3>
						<?php echo htmlentities($travel->getTitle()) ?>
					</h3>
					<p><?php echo htmlentities($travel->getDetails()) ?></p>				
				</div>
				<div class="clear"></div>
			</div>
			<div>
				<?php foreach ($travel->getDestinations() as $destination) : ?>
					<div id="destinations_container">
						<div class="destination_header">					
							<h4 class="dest_country"><img class="destination_bullet" src="/images/goabroad_app/bullet.gif"/>
								<?php echo $destination->getCountry()->getName() ?>
							</h4>
							<h4 class="sub_date">
								<?php if ($destination->getDateRange() != $destination->getTravelPlan()->getDateRange()) : ?>
									<?php echo $destination->getDateRange() ?>
								<?php endif; ?>
							</h4>
							<div class="clear"></div>
						</div>
						<p class="destination_desc"><?php echo htmlentities($destination->getDescription()) ?></p>
						<?php if ($owner && count($travel->getDestinations()) > 1) : ?>
							<a href="travelplans.php?action=deldest&travelwishid=<?php echo $travel->getTravelWishID() ?>&countryid=<?php echo $destination->getCountry()->getCountryID() ?>" onclick="return confirm('Are you sure you want to delete this destination?')">
								<img class="delete_desc" src="images/goabroad_app/delete_dest.jpg"/>
							</a>
						<?php endif; ?>
	
						<?php if (!$isPast && strtotime($destination->getStartDate()) > time() && $owner) : ?>
							<a href="/travelers.php?action=PlanningToGo&locationID=<?php echo $destination->getCountry()->getLocationID() ?>"><img src="images/img/seewhoelse_icon.gif"></a>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="clear"></div>		
		<?php endif; ?>
	</div>
<?php endforeach; ?>