<?php
	$code = <<<BOF
<script type="text/javascript">
tinyMCE_GZ.init({
	plugins : 'style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,'+ 
        'searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras',
	themes : 'simple,advanced',
	languages : 'en',
	disk_cache : true,
	debug : false
});
</script>
	
<script type="text/javascript">   
//<![CDATA[
	tinyMCE.init({
		mode: "none",
	  	theme: "advanced",
	  	theme_advanced_toolbar_location: "top",
	  	theme_advanced_toolbar_align: "left",
	  	theme_advanced_buttons1: "bold,italic,underline",
	  	theme_advanced_buttons2: "", 
	  	theme_advanced_buttons3: "",
		valid_elements: "b,strong,i,em,u,p,-p,-br,style",
		inline_styles: false,
		cleanup_on_startup: true,
		force_p_newlines: true,
		force_br_newlines: false,
		cleanup_callback: "custom_callback",
		plugins: "safari"
	});
	
	function custom_callback(type, value){
		switch (type) {
			case "get_from_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s)*<\/p>)/gi,'');
				break;
			case "insert_to_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s)*<\/p>)/gi,'');
				break;
			case "submit_content":
				break;
			case "get_from_editor_dom":
				break;
			case "insert_to_editor_dom":
				break;
			case "setup_content_dom":
				break;
			case "submit_content_dom":
				break;
		}
		
		return value;
	}

	jQuery(document).ready(function(){
		jQuery.initDate();
		EntryManager.cityID = $cityID;
		EntryManager.draftID = 0;
		EntryManager.resetCities();
		tinyMCE.execCommand( 'mceAddControl', false, 'description' );
		jQuery('.charcount').characterCount(250);
		jQuery('.charcount').keyup();
	});

	var saveClicked = false;
	window.onbeforeunload = function(e){
		if( null != document.getElementById("description_tbl") && !saveClicked ){
			return "You might lose some unsaved text if you do so. You may want to save your entry properly before navigating away from this page.";
		}
	}
//]]>  
</script>
BOF;
	
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('layoutID', 'journals');	
	
	if(!Template::isExistDependent('/min/f=js/prototype.js'))
		Template::includeDependentJs('/min/f=/js/prototype.js', array('top'=>true));
	// Template::includeDependentJs('/js/moo1.ajax.js', array('bottom'=>true));
	// Template::includeDependentJs('/js/travellogManager.js', array('bottom'=>true));
	// Template::includeDependentJs('/js/utils/Class.Request.js', array('bottom'=>true));
	// Template::includeDependentJs('/js/modaldbox.js', array('bottom'=>true));
	// Template::includeDependentJs('/js/minified_date1.0.js', array('bottom'=>true));     
	// Template::includeDependentJs('/js/jquery.entry.js', array('bottom'=>true));
	Template::includeDependentJs('/js/tiny_mce/tiny_mce_gzip.js', array('bottom'=>true));
	// Template::includeDependentJs('/js/interactive.form.js', array('bottom'=>true));
	Template::includeDependent($code, array('bottom'=>true));
	Template::includeDependent($obj_map->getJsToInclude(), array('bottom'=>true));
	Template::includeDependentJs('/min/g=AddEntryJs', array('bottom'=>true));
	// Template::includeDependentJs('/js/addnewcity.js', array('bottom'=>true));
	// Template::includeDependentJs('/min/f=js/jquery.characterCount.js', array('bottom'=>true));
	// Template::includeDependentJs('/min/f=js/jquery.popup/jquery.popup.js', array('bottom'=>true));
?>

<?php echo $profile->render(); ?>

<?php echo $sub_navigation->show(); ?>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
	
		<?php if ($props['show_auto_save_alert']): ?>
			<div class="infoBox">
				<p>		
					<strong>
						There <?php echo (1 < $props['unfinished_entries_count']) ? 'are '.$props['unfinished_entries_count'] : 'is an ';  ?> 
						unfinished entr<?php echo (1 < $props['unfinished_entries_count']) ? 'ies' : 'y'; ?>
						<a href="<?php echo $props['view_drafts_link']; ?>">click here to view.</a>
					</strong>
				</p>
			</div>
		<?php endif; ?> 
			
    	<div id="loader" class="jRight" style="display:none;">
    		<img src="/images/loading_small.gif" border="0" align="left"><span id="auto_message">Autosaving in Progress</span>
    	</div>
        
        <div class="formbox">
        	<h1>Create Journal Entry for <em>&#8220;<? if(isset($props['journal_title'])) echo $props['journal_title']; ?>&#8221;</em></h1>

					<? if (count($props['errors'])):?>
						<div class="error_notice">
							<ul>
							<?foreach ($props['errors'] as $error) :?>
									<li><?= $error ?></li>
							<? endforeach; ?>
							</ul>
						</div>
					<? endif; ?>
								
					<form name="sform" action="/journal-entry.php" method="post" class="interactive_form" id="sform">
						<ul class="form">	
							<li>	
								<label for="title">Title of Journal Entry<span class="required">*</span></label>
								<input type="text" name="title" id="title" size="40" value="<?=$props['title']?>" class="text big charcount" />
								<br/><input type="text" disabled="disabled" id="title_counter" size="3" value="" /> characters remaining
							</li>
							
							<li class="labeled">
								<fieldset>
									<legend><span>Location of Journal Entry</span></legend>
									<span class="labeled_input_wrapper">
									    <select name="countryID" id="countryID" onchange="EntryManager.resetCities();" >
									    	<?foreach( $col_countries as $obj_country ):?>	
									    		<option value="<?=$obj_country->getLocationID()?>" <?if(isset($props['countryID']) && $props['countryID'] == $obj_country->getLocationID()):?>selected="true"<?endif;?>><?=$obj_country->getName()?></option>
									    	<?endforeach;?>
									    </select>
										<label for="countryID">Country</label>
								    </span>
									<span class="labeled_input_wrapper">
										<span id="citylist">
											<select name="cityID" id="cityID" onchange="jQuery.triggerLocation(this);"> 
												<option value="0">None</option>    
											</select>
										</span>
										<label for="citylist" id="lblCityName">City</label>
									</span>
								</fieldset>
							</li>
							<li>
								<input type="checkbox" name="current_location" id="current_location" 
									<?if($props['current_location']):?>  
										checked="checked"
									<?endif;?>
								class="checkbox" />This is my current location.
							</li>
							
							<li>	
								<label for="arrivaldate">Date of Journal Entry</label>
								<input style="width:109px;" type="text" readonly="true" class="date-pick" name="arrival" id="arrival" value="<?=$props['arrival']?>"/>
							</li>
							<li>	
								<label for="callout">Callout:</label>
								<p class="supplement">
									A Callout is the highlight sentence(s) of your journal entry. You may  
									select a sentence or two from your journal entry and copy them here.  
									This callout may be the highlight of your experience, or it could be  
									your entry in a nutshell. On the journal page, the callout will be  
									displayed in bigger text and will be placed in quotes.
								</p>
								<input type="text" name="callout" id="callout" size="55" maxlength="250" value="<?=$props['callout']?>" class="text big charcount" />
								<br/><input type="text" disabled="disabled" id="callout_counter" size="3" value="" /> characters remaining
							</li>
							<li>
								<label for="description">Please write your Journal Entry here<span class="required">*</span></label>
								<p class="supplement">
									To prevent losing your entry, we encourage you to write from a text editor then copy your text here.
								</p>
								<textarea name="description" id="description" rows="25" cols="75"><?=$props['description'] ?></textarea>
							</li>
							
							<? if(isset($props['isAdmin']) && $props['isAdmin']):?>
								<li>
									<label class="inpt_box">Last Updated</label>
									<p class="supplement">
										example: &#8220;yyyy-mm-dd&#8221;.
									</p>
									<input type="text" name="lastupdate" id="lastupdate" size="20" maxlength="10" value="<?=$arr_vars['lastupdate']?>" class="text small" />
								</li>
							<?endif;?>
							
							<li class="jLeft">
									<input type="button" name="action" id="action" value="Save as Draft" class="button_v3" onclick="EntryManager.saveAsDraft();" />
							</li> 
							
							<li id="regbutton" class="actions jRight">
								<?php $allow = ($props['groupIDsCount'] == 0) ? 'false' : 'true'; ?>
								<?php $allow = (!empty($props['groupIDStr']) || !empty($props['notGroupIDStr'])) ? 'false' : $allow;?>
								<a href="/journal.php?action=<?if($props['isGroupJournal']):?>groupJournals&gID=<?else:?>myjournals&travelerID=<?endif;?><?=$journalOwnerID?>" class="jLeft button_v3" style="line-height:17px;margin-right:10px;" onclick="saveClicked=true;"><strong>Go Back</strong></a>	
								<input type="submit" id="createentry_button" value="Publish Entry" class="button_v3" onclick="saveClicked=true;EntryManager.saveEntry(<?php echo $props['travelerID'];?>,<?php echo $allow; ?>); return false;"/>
								<input type="hidden" name="action" id="action" value="publish entry" onclick="saveClicked=true;" />
								<input type="hidden" name="travelID"    id="travelID"    value="<?=$props['travelID']   ?>" />
								<input type="hidden" name="travellogID" id="travellogID" value="<?=$props['travellogID']?>" />
								<input type="hidden" name="draftID" id="draftID" value="0" />
								<input type="hidden" name="groupIDStr" id="groupIDStr" value="<?=$props['groupIDStr']?>" />
								<input type="hidden" name="notGroupIDStr" id="notGroupIDStr" value="<?=$props['notGroupIDStr']?>" />
								<input type="hidden" name="allowAutoApprove" id="allowAutoApprove" value="0" />
							</li>
					</ul>
				<div class="clear"></div>
			</form>
		</div>
  </div>
  
	<div id="narrow_column">
		<div class="helpextra"></div>
		<div class="helpbox">
			<h2>What is a Journal?</h2>
			<p>Your Journal is like a book where you record your experiences and trip photos, and the journal entries
are like pages of this book. You may use one Journal for your entire trip and add a series of journal
entries for all its details.
			</p>

			<h2>What is Callout</h2>
			<p>May be the highlights of your experience, or it could be your entry in a nutshell. It will be displayed in bigger text and will be placed in quotes.</p>
			
			<h2>What is Autosaving?</h2>
			<p>Most travelers who have used internet cafes or public Wifi on the road know how unpredictable that can be. This feature will save your work every 5 minutes when you're writing journal entries.</p>
		</div>
	</div>
 

	<!-- ##### Added By: Cheryl Ivy Q. Go		23 May 2008 #####-->
	<!-- ######### Purpose: Modal box to add new city ########## -->
	<!-- ########### Comment: PLEASE DON'T REMOVE! ############# -->
	<div id = "add_city_box" class="dialog">
		<?php 
			if( isset($arr_vars['isAdmin']) )
				include '../addNewCity.php';
			else
				include 'addNewCity.php';
		?>
	</div>
	<!-- ##### END #####-->

<div class="clear"></div>

</div>
