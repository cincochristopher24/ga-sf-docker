<div id="map_canvas"></div>

<div id="country_info"></div>

<div id="show_countries">
    <p id="title">Travelled to <?php echo $travelMapCount;?> <?php echo $travelMapCount == 1 ? 'country' : 'countries'; ?> </p>
	<p id="countries_list"><?php echo $userMap->getCountryNamesList() ?></p>
		
	<?php if ($owner) : ?>
	<div class="widget_actions">
        <a href="widget.php?travelerID=<?php echo $travelerID?>&action=editMTM">Edit Countries</a>
	</div>	
	<?php endif; ?>
</div>
<!-- show google maps using ajax -->
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$.ajax({
				url: "/retrieve-map.php",
				data: "context=0&travelerID=<?php echo $travelerID?>",
				type: "GET",
				success: function(res){
					eval(res);
				}
			});	
		});
	})(jQuery);
</script>

<!-- <script type="text/javascript">
//<![CDATA[
	jQuery(document).ready(function () {initialize();});
  
	function initialize() {
		if (GBrowserIsCompatible()) {
	    	var map = new GMap2(document.getElementById("map_canvas"));

			map.setCenter(new GLatLng(0, 0), 1);
			map.addControl(new GSmallMapControl());
			map.setZoom(0);
	
			// Create a base icon for all of our markers that specifies the
	        // shadow, icon dimensions, etc.
	        var baseIcon = new GIcon();
	        
	        baseIcon.image = "/images/ga_markerTransparent.png"
	        baseIcon.shadow = "/images/ga_marker_shadowTransparent.png";
	        baseIcon.iconSize = new GSize(12, 20);
	        baseIcon.shadowSize = new GSize(22, 20);
	        baseIcon.iconAnchor = new GPoint(6, 20);
	        baseIcon.infoWindowAnchor = new GPoint(6, 2);
	        baseIcon.infoShadowAnchor = new GPoint(18, 25);

		    // Creates a marker at the given point
	    	// Clicking the marker will hide it
	    	function createMarker(latlng, myHtml, countryName, isClickable) {
	      		var marker = new GMarker(latlng, {title:countryName, icon:baseIcon, clickable:isClickable});

	      		GEvent.addListener(marker, 'click', function() {
	      			document.getElementById('country_info').innerHTML = myHtml;
	      		});

	      		return marker;
			}
	        // add countries traveled markers	
	      	<?/*php foreach($userMap->getCountriesTraveled() as $country) : ?>	
	      		var latlng = new GLatLng(<?php echo $country->getLatitude() ?>,
	                              <?php echo $country->getLongitude() ?>);
	            
	            // compose html display for info window
				<?php if (!$country->getNumPhotos() && !$country->getNumJournalEntries() && !$country->getNumTravelTips()) : ?>
					var myHtml = '';
					var isClickable = false;
				<?php else: ?>
	        		var myHtml = '<div id="bubble_details_container" <?php if ($country->getNumPhotos() > 0):?> <?php endif; ?> >';
					var isClickable = true;

	        		<?php if ($country->getNumPhotos() > 0) : ?>
	        			myHtml += '<img id="photo" src="<?php echo $country->getRandomPhoto() ?>" />';
	        		<?php endif; ?>
	        		myHtml += '<div id="bubble_details">';
					myHtml += "<h4><?php echo $country->getCountryName() ?></h4>";
					myHtml += '<div id="container">';
	        		<?php if ($country->getNumJournalEntries() == 1) : ?>
	        			myHtml += "<p><span>1</span> Journal Entry<p>";
	        		<?php elseif ($country->getNumJournalEntries() > 1) : ?>
	        			myHtml += "<p><span><?php echo $country->getNumJournalEntries() ?></span> Journal Entries</p>";
	        		<?php endif; ?> 

	        		<?php if ($country->getNumPhotos() == 1) : ?>
	        			myHtml += "<p><span>1</span> Travel Photo</p>";
	        		<?php elseif ($country->getNumPhotos() > 1) : ?>
	        			myHtml += "<p><span><?php echo $country->getNumPhotos() ?></span> Travel Photos</p>";
	        		<?php endif; ?>

	        		<?php if ($country->getNumTravelTips() == 1) : ?>
	    				myHtml += "<p><span>1</span> Travel Tip</p>";
	    			<?php elseif ($country->getNumTravelTips() > 1) : ?>
	    				myHtml += "<p><span><?php echo $country->getNumTravelTips() ?></span> Travel Tips</p>";
	        		<?php endif; ?>
	        		myHtml += '<div style="clear: both;"></div></div></div><div class="clear"></div></div>';
				<?php endif; ?>
				
				map.addOverlay(createMarker(latlng, myHtml, '<?php echo $country->getCountryName() ?>', isClickable));
			<?php endforeach; */?>
		}
    }

//]]>
</script> -->