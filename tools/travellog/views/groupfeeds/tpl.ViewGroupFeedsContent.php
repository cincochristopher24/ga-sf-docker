<div id="content_wrapper" class="layout_2">
	<div id="feeds">

		<div id="journals_summary" class="section" >	
			<a name="anchor"></a>
			<h2>
				<span><?=($is_parent_group)?'Group Activity Feed':'Subgroup Activity Feed'?></span>
				<span class="header_actions rss"><a href="/groupfeedsrss.php?gID=<?=$group->getGroupID()?>">Subscribe to RSS Feed</a></span>
			</h2>	
			<div id="content">
				<div id="GROUP_FEEDS_TAB">
					<?=$groupFeedsTab->render();?>
				</div>
				<div id="GROUP_FEEDS_CONTENT">
					<?//=$groupFeedsContent->render();?>
				</div>
			</div>
		</div>

	</div>
</div>