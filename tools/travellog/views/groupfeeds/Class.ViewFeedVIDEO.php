<?php
	require_once("travellog/views/groupfeeds/Class.AbstractViewFeed.php");

	class ViewFeedVIDEO extends AbstractViewFeed{
		public function prepare(){
			require_once("travellog/model/Class.Traveler.php");
			require_once("travellog/model/Class.VideoAlbum.php");
			try{
				$traveler = new Traveler($this->mFeed->getTravelerID());
				$videoAlbum = new VideoAlbum($this->mFeed->getSectionRootID());
				
				if( !($videoAlbum instanceof VideoAlbum) ){
					throw new exception("Invalid VideoAlbumID: ".$this->mFeed->getSectionRootID()." FeedID: ".$this->mFeed->getFeedID());
				}
				if( $traveler->isSuspended() ){
					throw new Exception("Suspended travelerID: ".$this->mFeed->getTravelerID()." FeedID: ".$this->mFeed->getFeedID());
				}
				
				$this->mTemplate->set("videoAlbum",$videoAlbum);
				$this->mTemplate->set("traveler",$traveler);
				$this->mTemplate->set("feed",$this->mFeed);
				$this->mTemplate->set("group",$this->mGroup);
			}catch(exception $e){
				throw $e;
			}
		}
		public function render(){
			parent::render();
		}
	}