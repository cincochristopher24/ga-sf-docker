<?php
	$albumDate = GaDateTime::create($feed->getFeedDate());
	
	$rowsLimit = new RowsLimit(10,0);
	$videosArray = $videoAlbum->getVideosUploadedByDay($rowsLimit,$feed->getFeedDate());
	$videos = $videosArray["videos"];
	if( !$videosArray["recordCount"] ){
		throw new exception("videos uploaded not found; possible undeleted video upload feed");
	}
		
	$owner = $traveler;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$owner = new AdminGroup($feed->getGroupID());
		$profileLink = $owner->getFriendlyURL();
		$albumLink = "/video.php?action=getGroupVideos&gID=".$videoAlbum->getCreatorID()."&albumID=".$videoAlbum->getAlbumID()."&type=album";
		$videoCountLabel = 1 < $videosArray["recordCount"] ? $videosArray["recordCount"]." Videos were" : $videosArray["recordCount"]." Video was";
	}else{
		$profileLink = $owner->getFriendlyURL();
		if( 0 < count($videos) ){
				$albumLink = "action=getTravelerVideos&travelerID=".$videoAlbum->getCreatorID()."&albumID=".$videoAlbum->getAlbumID()."&type=album";
		}else{
			$albumLink = "/video.php?travelerID=".$videoAlbum->getCreatorID();
		}
		$videoCountLabel = 1 < $videosArray["recordCount"] ? $videosArray["recordCount"]." Videos" : $videosArray["recordCount"]." Video";
	}
	
	$isAdvisorAlbum = FALSE;
	$creator = $traveler;
	$profileLink = $creator->getFriendlyUrl();
	/*if( $group->getAdministratorID() == $traveler->getTravelerID() ){
		$isAdvisorAlbum = TRUE;	
		$creator = new AdminGroup($traveler->getAdvisorGroup());
		$profileLink = $creator->getFriendlyURL();
	}*/
	
	$isStaffAlbum = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP && !$isAdvisorAlbum && $owner->isStaff($feed->getTravelerID()) ){
		$isStaffAlbum = TRUE;
		$creator = new AdminGroup($feed->getGroupID());
		$profileLink = $creator->getFriendlyURL();
	}
	
	$sameGroup = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$sameGroup = $group->getGroupID() == $feed->getGroupID();
	}
?>

<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>">
	<?	if( $isAdvisorAlbum || $isStaffAlbum ): ?> 
		<a href="<?=$profileLink?>" class="">
			<img src="<?=$creator->getGroupPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>   
	<?	else: ?>
		<a href="<?=$profileLink?>">
			<img class="jLeft" src="<?=$creator->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>
	<?	endif; ?>
	<span class="box">
		<p class="highlights feedVideo">
			<?	if( $feed->getSectionType() == ActivityFeed::TRAVELER ): ?>		
				<a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a> <strong><?=stripslashes($owner->getFullName())?></strong> added <?=$videoCountLabel?> to album <a href="<?=$albumLink?>"><?=$videoAlbum->getTitle()?></a>
			<?	else: ?>
				<?=$videoCountLabel?> added to album <a href="<?=$albumLink?>"><?=$videoAlbum->getTitle()?></a>  
				<?	if( !$sameGroup && $feed->getSectionType() == ActivityFeed::GROUP ): ?>
					in <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	endif;?>	
		</p>
			<?	foreach($videos AS $video):
					if( $feed->getSectionType() == ActivityFeed::TRAVELER ){
						$videoLink = "/video.php?action=getTravelerVideos&travelerID=".$owner->getTravelerID()."&albumID=".$videoAlbum->getAlbumID()."&videoID=".$video->getVideoID()."&type=album";
					}else{
						$videoLink = "/video.php?action=getGroupVideos&gID=".$owner->getGroupID()."&albumID=".$videoAlbum->getAlbumID()."&videoID=".$video->getVideoID()."&type=album";
					}
			?><div class="video jLeft">
			<div class="video jLeft">
				<div class="videomarker">
					<img src="/images/v3/discussions/videosign.gif" border="0" align="left"/>
				</div>
				<img width="92" height="62" border="0" src="<?= $video->getVideoImageUrl() ?>"/>
				<a href="<?=$videoLink?>"><strong><?=$video->getTitle()?></strong></a><br>
				<span><?=$video->getDuration()?></span>
			</div>	

			<?	endforeach; ?>
		
		<p class="jRight eventime"><?=$albumDate->htmlDateFormat()?></p>
	</span>
</li>