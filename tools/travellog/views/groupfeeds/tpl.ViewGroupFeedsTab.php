<ul id="nav" class="sub_nav_small">
	<li id="ALL_FTAB" name="FTAB" class="first <?=('ALL'==$activeTab)? 'active' : '';?>">
		<a href="javascript:void(0)" onclick="groupFeeds.loadFeedsTab('ALL'); return false;"><span>All</span></a>
	</li>
	<li id="JOURNALS_FTAB" name="FTAB" class="<?=('JOURNALS'==$activeTab)? 'active' : '';?>">
		<a href="javascript:void(0)" onclick="groupFeeds.loadFeedsTab('JOURNALS'); return false;"><span>Journals</span></a>
	</li>
	<li id="MULTIMEDIA_FTAB" name="FTAB" class="<?=('MULTIMEDIA'==$activeTab)? 'active' : '';?>">
		<a href="javascript:void(0)" onclick="groupFeeds.loadFeedsTab('MULTIMEDIA'); return false;"><span>Multimedia</span></a>
	</li>
	<li id="DISCUSSION_FTAB" name="FTAB" class="<?=('DISCUSSION'==$activeTab)? 'active' : '';?>">
		<a href="javascript:void(0)" onclick="groupFeeds.loadFeedsTab('DISCUSSION'); return false;"><span>Discussion</span></a>
	</li>
	<li id="MEMBERSHIP_FTAB" name="FTAB" class="<?=('MEMBERSHIP'==$activeTab)? 'active' : '';?>">
		<a href="javascript:void(0)" onclick="groupFeeds.loadFeedsTab('MEMBERSHIP'); return false;"><span>Membership</span></a>
	</li>
	<?	if( $group->getDiscriminator() == GROUP::ADMIN && $group->isParent() ): ?>
		<?php
			$subgroups = $group->getSubGroups();
			if( 0 < count($subgroups) ):
		?>
		<li class="actions">
			<strong>Group:</strong>
			<select id="cboSubgroup" onchange="groupFeeds.loadSubgroupFeeds(this.options[this.selectedIndex].value);">
				<option value="<?=$group->getGroupID()?>"></option>
				<?	foreach($subgroups AS $subgroup): 
						$name = $subgroup->getName();
						$name = (50 >= strlen($name)) ? $name : substr($name,0,50)."...";
				?>
					<option value="<?=$subgroup->getGroupID()?>"><?=$name?></option>
				<?	endforeach; ?>
			</select>
		</li>
		<?	endif; ?>
	<?	endif; ?>
	<li id="LOADING_FTAB" style="display:none;">
		<img src="/images/loading_small.gif" />
	</li>
</ul>
<input type="hidden" id="FEEDS_GROUP_ID" value="<?=$group->getGroupID()?>" />