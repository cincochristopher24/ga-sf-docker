<?php
	$albumDate = GaDateTime::create($feed->getFeedDate());
	
	$rowsLimit = new RowsLimit(10,0);
	$photosArray = $photoAlbum->getPhotosUploadedByDay($rowsLimit,$feed->getFeedDate());
	$photos = $photosArray["photos"];
	$photoCount = $photosArray["recordCount"];
	
	if( !$photoCount ){
		throw new exception("photo album is empty {feedID: ".$feed->getFeedID()."}");
	}
	
	$owner = $traveler;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$owner = new AdminGroup($feed->getGroupID());
		$profileLink = $owner->getFriendlyURL();
		$albumLink = "/collection.php?type=group&ID=".$owner->getGroupID()."&context=photoalbum&genID=".$photoAlbum->getPhotoAlbumID();
	}else{
		$profileLink = $owner->getFriendlyUrl();
		$albumLink = "/collection.php?type=traveler&ID=".$photoAlbum->getCreator()."&context=traveleralbum&genID=".$photoAlbum->getPhotoAlbumID();
	}
	
	$isAdvisorAlbum = FALSE;
	$creator = $traveler;
	/*if( $group->getAdministratorID() == $traveler->getTravelerID() ){
		$isAdvisorAlbum = TRUE;	
		$creator = new AdminGroup($traveler->getAdvisorGroup());
		$profileLink = $creator->getFriendlyURL();
	}*/
	
	$isStaffAlbum = 0 < $photoAlbum->getGroupID() ? TRUE : FALSE;
	
	/*$isStaffAlbum = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP && !$isAdvisorAlbum && $owner->isStaff($feed->getTravelerID()) ){
		$isStaffAlbum = TRUE;
	}*/
	
	$sameGroup = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$sameGroup = $group->getGroupID() == $feed->getGroupID();
	}
?>

<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>"> 
	<?	if( $isAdvisorAlbum ): ?> 
		<a href="<?=$profileLink?>" class="">
			<img src="<?=$creator->getGroupPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>   
	<?	else: ?>
		<a href="<?=$profileLink?>">
			<img class="jLeft" src="<?=$creator->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>
	<?	endif; ?>
	<span class="box">
		<p class="highlights feedPhoto">
			<?	if( $isAdvisorAlbum ): ?>
				<a href="<?=$profileLink?>" class=""><strong><?=$creator->getName()?></strong></a> created a new PHOTO ALBUM <a href="<?=$albumLink?>"><?=$photoAlbum->getTitle()?></a>
				<?	if( !$sameGroup ): ?>
					for <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	elseif( $isStaffAlbum ): ?>
				<a href="<?=$profileLink?>"><strong><?=$creator->getName()?></strong></a> <strong><?=stripslashes($creator->getFullName())?></strong> created a new PHOTO ALBUM <a href="<?=$albumLink?>"><?=$photoAlbum->getTitle()?></a> 
				<?	if( !$sameGroup ): ?>
					for <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	else: ?>
				<a href="<?=$profileLink?>"><strong><?=$creator->getName()?></strong></a> <strong><?=stripslashes($creator->getFullName())?></strong> created a new PHOTO ALBUM <a href="<?=$albumLink?>"><?=$photoAlbum->getTitle()?></a> 
			<?	endif; ?>
		</p>
		<div class="photo jLeft">
			<?	if( 1 > $photoCount ): ?>
				No photos were uploaded when the album was created.
			<?	else: ?>
				<?	foreach($photos AS $photo): ?>
					<img width="<?=$photo->getAdjustedThumbnailWidth()?>" height="<?=$photo->getAdjustedThumbnailHeight()?>" border="0" src="<?=$photo->getPhotoLink('featured')?>"/>
				<?	endforeach; ?>
			<?	endif; ?>
		</div>	
		<p class="jRight eventime"><?=$albumDate->htmlDateFormat()?></p>
	</span>
</li>
