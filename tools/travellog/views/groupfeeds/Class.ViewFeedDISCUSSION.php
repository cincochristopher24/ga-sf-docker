<?php
	require_once("travellog/views/groupfeeds/Class.AbstractViewFeed.php");

	class ViewFeedDISCUSSION extends AbstractViewFeed{
		public function prepare(){
			require_once("travellog/model/DiscussionBoard/Class.Discussion.php");
			require_once("travellog/model/Class.Traveler.php");
			try{
				$discussionID = $this->mFeed->getSectionID();
				$topicID = $this->mFeed->getSectionRootID();
				
				$discussion = Discussion::createInstance($discussionID);
				if( is_null($discussion) ){
					throw new exception("Discussion { $discussionID } does not exist.");
				}
				$topic = $discussion->getParentTopic();
				
				$creator = new Traveler($this->mFeed->getTravelerID());
				
				if( $creator->isSuspended() ){
					throw new Exception("Suspended travelerID: ".$this->mFeed->getTravelerID()." FeedID: ".$this->mFeed->getFeedID());
				}
				
				$this->mTemplate->set("discussion",$discussion);
				$this->mTemplate->set("topic",$topic);
				$this->mTemplate->set("creator",$creator);
				$this->mTemplate->set("feed",$this->mFeed);
				$this->mTemplate->set("group",$this->mGroup);
				$this->mTemplate->set("loggedUser",$this->mLoggedUser);
			}catch(exception $e){
				throw $e;
			}
		}
		public function render(){
			parent::render();
		}
	}