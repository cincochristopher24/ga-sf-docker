<?php
	require_once("travellog/views/groupfeeds/Class.AbstractViewFeed.php");

	class ViewFeedJOURNALENTRY extends AbstractViewFeed{
		public function prepare(){
			require_once("travellog/model/Class.Traveler.php");
			require_once("travellog/model/Class.TravelLog.php");
			
			try{
				$traveler = new Traveler($this->mFeed->getTravelerID());
				$travel = new Travel($this->mFeed->getSectionRootID());
				$travellog = new TravelLog($this->mFeed->getSectionID());
				
				if( !($travellog instanceof TravelLog) ){
					throw new Exception("Invalid TravelLogID: ".$this->mFeed->getSectionID()." FeedID: ".$this->mFeed->getFeedID());
				}
				if( !($travel instanceof Travel) ){
					throw new Exception("Invalid TravelID: ".$this->mFeed->getSectionRootID()." FeedID: ".$this->mFeed->getFeedID());
				}
				if( $traveler->isSuspended() ){
					throw new Exception("Suspended travelerID: ".$this->mFeed->getTravelerID()." FeedID: ".$this->mFeed->getFeedID());
				}
				
				$this->mTemplate->set("journal",$travel);
				$this->mTemplate->set("journalentry",$travellog);
				$this->mTemplate->set("traveler",$traveler);
				$this->mTemplate->set("feed",$this->mFeed);
				$this->mTemplate->set("group",$this->mGroup);
			}catch(exception $e){
				throw $e;
			}
		}
		public function render(){
			parent::render();
		}
	}