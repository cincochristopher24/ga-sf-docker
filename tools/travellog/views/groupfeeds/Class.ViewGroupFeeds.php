<?php
	
	require_once("Class.Template.php");

	class ViewGroupFeeds{
		
		private $mParams	=	array(	"action"	=>	NULL,
		  								"mode"		=>	NULL,
		 								"sgID"		=>	NULL	);
		private $mGroup	=	NULL;
		
		public function setParams($params=array()){
			$this->mParams = array_merge($this->mParams,$params);
		}
		public function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		
		public function render(){
			require_once("travellog/views/groupfeeds/Class.ViewGroupFeedsTab.php");
			$feedsTab = new ViewGroupFeedsTab();
			$feedsTab->setActiveTab($this->mParams["mode"]);
			$feedsTab->setGroup($this->mGroup);
			
			$feedsTpl = new Template();
			$feedsTpl->set("groupFeedsTab",$feedsTab);
			$feedsTpl->set("group",$this->mGroup);
			$feedsTpl->set("is_parent_group",$this->mGroup->isParent());
			$feedsTpl->out("travellog/views/groupfeeds/tpl.ViewGroupFeedsContent.php");
		}
	}