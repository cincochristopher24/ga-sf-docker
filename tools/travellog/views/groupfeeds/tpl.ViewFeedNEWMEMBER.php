<?php
	$profileLink = "/".$traveler->getName();
	$feedDate = GaDateTime::create($feed->getFeedDate());
	$auxDate = GaDateTime::create($feed->getAuxDate());
?>
<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>">     
	<a href="<?=$profileLink?>">
		<img class="jLeft" src="<?=$traveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
	</a>
	<span class="box">
		<p class="highlights feedMember"><a href="<?=$profileLink?>"><strong><?=$traveler->getName()?></strong></a> <strong><?=stripslashes($traveler->getFullName())?></strong> became a member of <a href="<?=$group->getFriendlyURL()?>"><?=$group->getName()?></a></p>
		<div class="discuss jLeft">
			<p>
			<?php
				switch($feed->getSectionRootID()){
					case ActivityFeed::MEMBER_BY_DEFAULT	:	$dateLabel = trim($feedDate->htmlDateFormat());
																break;
					case ActivityFeed::MEMBER_BY_INVITE		: 	$dateLabel = "Date invited: ".trim($auxDate->htmlDateFormat())."<br />Date invitation was accepted: ".trim($feedDate->htmlDateFormat());					
																break;
					case ActivityFeed::MEMBER_BY_REQUEST	:	$dateLabel = "Date requested to join group: ".trim($auxDate->htmlDateFormat())."<br />Date request was approved: ".trim($feedDate->htmlDateFormat());
																break;
				}
			?>
			</p>
		</div>	
		<p class="jRight eventime"><?=$dateLabel?></p>
	</span>
</li>