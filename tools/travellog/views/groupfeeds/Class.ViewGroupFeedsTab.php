<?php
	
	require_once("Class.Template.php");

	class ViewGroupFeedsTab{
		
		private $mGroup		=	NULL;
		private $mActiveTab =	"ALL";
		
		function setGroup($groupObj=NULL){
			$this->mGroup = $groupObj;
		}
		function setActiveTab($tab="ALL"){
			$this->mActiveTab = $tab;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("group",$this->mGroup);
			$tpl->set("activeTab",$this->mActiveTab);
			$tpl->out("travellog/views/groupfeeds/tpl.ViewGroupFeedsTab.php");
		}
	}