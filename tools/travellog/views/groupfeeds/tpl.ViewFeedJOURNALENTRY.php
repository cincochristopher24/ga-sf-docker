<?php
	try{
		$owner = $journal->getOwner();
		if( !($owner instanceof Group) && !($owner instanceof Traveler) ){
			throw new exception("Owner must be Group or Traveler");
		}
	}catch(exception $e){
		throw $e;
	}
	$entryHelper = new EntryHelper();
	$journalLink = $entryHelper->getLink($journalentry);
	$entryDate = GaDateTime::create($feed->getFeedDate());
	
	$profileLink = "/".$traveler->getName();
	$isAdvisorJournal = FALSE;
	/*if( $group->getAdministratorID() == $traveler->getTravelerID() ){
		$isAdvisorJournal = TRUE;
		$profileLink = $group->getFriendlyURL();
	}*/
	/*if( $journal->isGroupJournal() ){
		$isAdvisorJournal = TRUE;
		$profileLink = $group->getFriendlyURL();
		$journalLink = $entryHelper->getLink($journalentry);
	}*/
	$isStaffJournal = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP && !$isAdvisorJournal && $owner->isStaff($feed->getTravelerID()) ){
		$isStaffJournal = TRUE;
	}
	
	$sameGroup = FALSE;
	if( $feed->getSectionType() == ActivityFeed::GROUP ){
		$sameGroup = $group->getGroupID() == $feed->getGroupID();
	}	
?>
<li class="container ajax_journal" id="feed_<?=$feed->getFeedID()?>">     
	<?	if( $isAdvisorJournal ): ?> 
		<a href="<?=$profileLink?>" class="">
			<img src="<?=$owner->getGroupPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>   
	<?	else: ?>
		<a href="<?=$profileLink?>">
			<img class="jLeft" src="<?=$traveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" border="" align="left" alt=""/>
		</a>
	<?	endif; ?>
	<span class="box">
		<p class="highlights feedJournal">
			<?	if( $isAdvisorJournal ): ?>
				<a href="<?=$profileLink?>" class=""><strong><?=$owner->getName()?></strong></a> created a new admin JOURNAL ENTRY <a href="<?=$journalLink?>"><?=$journalentry->getTitle()?></a>
				<?	if( !$sameGroup ): ?>
					for <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	elseif( $isStaffJournal ):  ?>
				<a href="<?=$profileLink?>"><strong><?=$traveler->getName()?></strong></a> <strong><?=stripslashes($traveler->getFullName())?></strong> created a new staff JOURNAL ENTRY <a href="<?=$journalLink?>"><?=$journalentry->getTitle()?></a>
				<?	if( !$sameGroup ): ?>
					for <a href="<?=$owner->getFriendlyURL()?>" class=""><strong><?=$owner->getName()?></strong></a>
				<?	endif; ?>
			<?	else: ?>
				<a href="<?=$profileLink?>"><strong><?=$traveler->getName()?></strong></a> <strong><?=stripslashes($traveler->getFullName())?></strong> created a new JOURNAL ENTRY <a href="<?=$journalLink?>"><?=$journalentry->getTitle()?></a>
			<?	endif; ?>
		</p>
		<div class="discuss jLeft">
			<p><?=nl2br(HtmlHelpers::TruncateText(strip_tags($journalentry->getDescription()), 50))?></p>	
		</div>	
		<p class="jRight eventime"><?=$entryDate->htmlDateFormat()?></p>
	</span>
</li>