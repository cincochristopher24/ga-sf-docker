<?php
	require_once("Class.Template.php");
	require_once("travellog/model/Class.ActivityFeed.php");

	abstract class AbstractViewFeed{
		
		protected	$mFeed = NULL;
		protected	$mGroup = NULL;
		protected	$mTemplate = NULL; 
		protected	$mLoggedUser = NULL;
		private 	$mValidFeed = FALSE;	

		public function create($feed=NULL,$group=NULL){
			if( $feed instanceof ActivityFeed && $group instanceof AdminGroup ){
				$this->mFeed = $feed;
				$this->mGroup = $group;
				$this->mValidFeed = TRUE;
				$this->mTemplate = new Template();
			}else{
				throw new exception("Argument must be an instance of ActivityFeed!");
			}
		}
		public function setLoggedUser($user=NULL){
			$this->mLoggedUser = $user;
		}
		protected function render(){
			if( !$this->mValidFeed ){
				throw new exception("Feed must be set and must be an instance of ActivityFeed!");
			}
			$this->mTemplate->set("feed",$this->mFeed);
			$this->mTemplate->out("travellog/views/groupfeeds/tpl.ViewFeed".$this->mFeed->getFeedSectionLabel().".php");
		}
	}