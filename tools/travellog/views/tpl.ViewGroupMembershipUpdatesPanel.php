<?$dateTime = new GaDateTime;
$shownUpdates = 0;
?>
<div class = "content">

	<?php if (0 < count($groupMembershipUpdates)) : ?>
		<ul class = "users">
			
			<?$grpDate = ''?>
			<?php foreach($groupMembershipUpdates as $executionDate => $update):?>
				
				<? // if current doer is not a traveler or not active or suspended proceed to next update ?>
				<?php if( !$update->getDoer() instanceof Traveler || !$update->getDoer()->isActive() || $update->getDoer()->isSuspended()){ continue ;}?>
				
				<?php if($grpDate != date("Y-m-d",$executionDate)):?>
					<?php $grpDate = date("Y-m-d",$executionDate)?>
					<li class="date_separator">
						<?=$dateTime->friendlyFormat($grpDate) ?>
					</li>
				<?php endif;?>
				
					<li>
						<a class = "thumb" title = "<?=$update->getDoer()->getUsername()?>" href = "/<?=$update->getDoer()->getUsername()?>"> 
							<img src = "<?=$update->getDoer()->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" width = "65" height = "65" alt = "Member Thumbnail"/>
						</a>
						<div class = "details overflow">
							<a class = "username" title = "<?=$update->getDoer()->getUsername()?>" href = "/<?=$update->getDoer()->getUsername()?>"><?=HtmlHelpers::truncateWord($update->getDoer()->getUsername(), 15)?></a>
							<strong><?=$update->getDoer()->getFullName()?></strong>
							<?=$update->getAction()?>
							<?php if( UpdateAction::GROUP_INVITED_NON_GA_STAFF == $update->getRetrieverType() ):?>
								<strong> <?=$update->getDoer()->getTravelerProfile()->getEmail()?></strong>
							<?php endif; ?>
						</div>
					</li>
				<?$shownUpdates++?>
			<?php endforeach;?>
			
		</ul>
	<?php endif; ?>
	
	<?php if( !$shownUpdates ):?><p class="help_text"> <span>No record found</span>.</p><?php endif; ?>
</div>
