<?php
/*
	Filename:	tpl.IncTravelPlanMini
	Author:		Jonas Tandinco
	Created:	October 2007
	Purpose:	include file for displaying travel plans in profile and passport pages
	
	EDIT HISTORY:
	
	Oct/15/2007 by Jonas Tandinco
		1.	removed link to journals
		2.	removed "more" and "less"
		
	Oct/31/2007 by Jonas Tandinco
		1.	changed method calls of traveldestination object for its country field
		2.	enabled "See who else is going"
		
	Nov/14/2007 by Jonas Tandinco
		1.	removed links to travelscheduler.php and pointed to the new travelplans.php
		
	Feb/22/2008 by Jonas Tandinco
		1.	see who else is going only enabled if logged on user is viewing his own travel plans
		
	Feb/25/2008 by Jonas Tandinco
		1.	separated design if only one country is select
*/
?>
<?php foreach ($travels as $travel) : ?>
	<li id="travel_entry_wrapper" class="travel_entries">
		<div id="top_level">			
			<?php if (count($travel->getDestinations()) == 1) : ?>
				<?php
					$destination = $travel->getDestinations();
					$destination = $destination[0];
				?>
				<span class="top_level_date">
					<?php echo $destination->getDateRange() ?>
				</span>

				<div class="top_level_details">
					<h4>
						<?php echo htmlentities($travel->getTitle()) ?>
					</h4>
					<p><?php echo htmlentities($travel->getDetails()) ?></p>																													
				</div>
				
				<ul class="destinations_wrapper">
						<li id="destinations_container" class="destinations_instance">
							
							<?php if (strlen($destination->getDateRange()) != 0) : ?>
							<div class="sub_date">
								<?php if ($destination->getDateRange() != $destination->getTravelPlan()->getDateRange()) : ?>
									<?php echo $destination->getDateRange() ?>
								<?php endif; ?> 
							</div>
							<?php endif; ?>		
							
							<div class="destination_details">
								<?php echo $destination->getCountry()->getName() ?>:
								<?php echo $destination->getDescription() ?>
							</div>								
														
							<?php if (strtotime($destination->getStartDate()) > time() && $owner) : ?>
								<a href="travelers.php?action=PlanningToGo&locationID=<?php echo $destination->getCountry()->getLocationID() ?>"><img src="images/img/seewhoelse_icon.gif"></a>
							<?php endif; ?>							
						</li>							
				</ul>	
				
				<?php if ($owner) : ?>
					<div class="widget_actions">
						<a href="widget.php?travelerID=<?php echo $travelerID; ?>&action=editMTP&do=edit&travelWishID=<?php echo $travel->getTravelWishID() ?>">
							Edit Itinerary
						</a>
					</div>
				<?php endif; ?>												
				
			<?php else : ?>
				<span class="top_level_date">
					<?php echo $travel->getDateRange() ?>
				</span>

				<div class="top_level_details">
					<h4><?php echo htmlentities($travel->getTitle()) ?></h4>
					<p><?php echo htmlentities($travel->getDetails()) ?></p>
				</div>
				
				<ul class="destinations_wrapper">
					<?php foreach($travel->getDestinations() as $destination) : ?>
						<li id="destinations_container" class="destinations_instance">					
							
							<?php if (strlen($destination->getDateRange()) != 0) : ?>
							<div class="sub_date">
								<?php if ($destination->getDateRange() != $destination->getTravelPlan()->getDateRange()) : ?>
									<?php echo $destination->getDateRange() ?>
								<?php endif; ?>
							</div>
							<?php endif; ?>		
											
							<div class="destination_details">
								<strong><?php echo $destination->getCountry()->getName() ?></strong>: <br />
								<?php echo htmlentities($destination->getDescription()) ?>
								
								<?php if (strtotime($destination->getStartDate()) > time() && $owner) : ?>								
									<a class="seeWhoElse" href="travelers.php?action=PlanningToGo&locationID=<?php echo $destination->getCountry()->getLocationID() ?>"><img src="images/img/seewhoelse_icon.gif"></a>
								<?php endif; ?>
																
							</div>																															
						</li>
					<?php endforeach; ?>
				</ul>		

				<?php if ($owner) : ?>
					<div class="widget_actions">
						<a href="widget.php?travelerID=<?php echo $travelerID; ?>&action=editMTP&do=edit&travelWishID=<?php echo $travel->getTravelWishID() ?>">
							Edit Itinerary
						</a>
              		</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</li>
<?php endforeach; ?>