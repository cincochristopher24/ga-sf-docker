<div style="overflow:auto;">
	<table style="width:400px;background-color:#ececec">
	<?php if('DISPLAY_ADDRESSBOOK_ENTRIES' == $method):?>			
		<?php if(!count($entries)):?>
			<tr style="border: 1px solid black;">
				<td style="background-color:#ececec"><strong>You have no entries in your addressbook.</strong></td>							
			</tr>
		<?php else:?>
			<tr style="border: 1px solid black;">
				<td style="background-color:#ececec"><strong>Name</strong></td>
				<td style="background-color:#ececec"><strong>Email Address</strong></td>
			</tr>
			<?php foreach($entries as $entry): ?>
				<tr style="cursor:pointer; border: 1px solid black;" onclick="AddressBook.addAddressToContainer('<?= $entry->getEmailAddress() ?>')" onmouseover="var tds = this.getElementsByTagName('td'); for(var i=0;i&lt;tds.length;i++){tds[i].style.backgroundColor='#ececec'}" onmouseout="var tds = this.getElementsByTagName('td'); for(var i=0;i&lt;tds.length;i++){tds[i].style.backgroundColor=''}">
					<td><?= $entry->getFirstName().' '.$entry->getLastName() ?></td>
					<td><?= $entry->getEmailAddress() ?></td>
				</tr>
			<?php endforeach; ?>
		<?php endif;?>
	<?php elseif('DISPLAY_FRIENDS_LIST' == $method || 'DISPLAY_GROUP_LIST' == $method):?>	
		<?php if(!count($entries)):?>
			<tr style="border: 1px solid black;">
				<td class="help_text"><span>You have no friends yet.</span></td>							
			</tr>
		<?php else:?>
			<thead>
				<tr style="border: 1px solid black">
					<td style="background-color:#ececec">
						<strong>
							<?php if('DISPLAY_FRIENDS_LIST' == $method):?>User Name<?php else: ?>Group Name<?php endif; ?>
						</strong>
					</td>
				</tr>
			</thead>
			<tbody style="cursor:pointer;">
				<?php 
					$methodName = ('DISPLAY_FRIENDS_LIST' == $method)?'getUserName':'getName';
					foreach($entries as $entry):
				?>
					<tr style="cursor:pointer; border: 1px solid black"  onclick="AddressBook.addAddressToContainer('<?= $entry->$methodName() ?>')" onmouseover="var tds = this.getElementsByTagName('td'); for(var i=0;i&lt;tds.length;i++){tds[i].style.backgroundColor='#ececec'}" onmouseout="var tds = this.getElementsByTagName('td'); for(var i=0;i&lt;tds.length;i++){tds[i].style.backgroundColor=''}">
						<td>
							<?= $entry->$methodName() ?>
							<?php 
								if('DISPLAY_FRIENDS_LIST' == $method):								
									$pref = new PrivacyPreference($entry->getTravelerID());
									$canViewOnlineStatus = $pref->canViewOnlineStatus($_SESSION['travelerID']);									 
									if($canViewOnlineStatus && $entry->isOnLine()):
										echo '&nbsp; - Online';
									endif;										
								endif; 
							?>							
						</td>						
					</tr>
				<?php endforeach; ?>
			</tbody>
		<?php endif;?>
	<?php endif; ?>
	</table>
</div>
<div width="100%" align="center" style="padding:5px;">
	<input type="button" name="btnClose" value="Close" onclick="AddressBook.hide()">
</div>