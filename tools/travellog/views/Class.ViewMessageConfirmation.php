<?php
	/*
	 * Class.ViewMessageConfirmation.php
	 * Created on Nov 16, 2007
	 * created by marc
	 */
	 
	require_once("travellog/views/Class.AbstractView.php");

	class ViewMessageConfirmation extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewMessageConfirmation.php');
		}
		
	}
?>
