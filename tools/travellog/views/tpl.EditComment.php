<?php
/*
 * Created on 09 26, 2006
 * @author Kerwin Gordo
 * Purpose: template for editing a comment
 */
?>


<?php 
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	/**
	 * Added by naldz (Nov 20,2006)
	 * This is for the subnavigation links
	 */	
	$snav->show();
?>

<h2>Edit Comment</h2>
<form action="comment.php?action=saveedit" method="post">
		<textarea name="comment" cols="40" rows="6"><?=$comment?>
		</textarea><br />				
		<input type="hidden" name="contextID" value="<?=$contextID?>" />
		<input type="hidden" name="context" value="<?=$context?>" />
		<input type="hidden" name="referer" value="<?=$referer?>" />
		<input type="SUBMIT" value="Update" />	
</form>

