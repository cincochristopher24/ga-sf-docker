<?php

	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.Traveler.php';

	class ViewFeature{
		private $mIsAdvisor		= FALSE;
		private $mIsLogged		= FALSE;
		private $mLoggedUser	= NULL;
		//private $mAdvisorGroupID	=	NULL;

		function setLoggedUser($_loggedUser = NULL){
			$this->mLoggedUser = $_loggedUser;
			$this->mAdvisorGroupID = NULL;
			if( $this->mLoggedUser instanceof Traveler && 0 < $this->mLoggedUser->getTravelerID() ){
				$this->mIsLogged = TRUE;
				$this->mIsAdvisor = $this->mLoggedUser->isAdvisor();
				/**if ($this->mIsAdvisor) {
				    $file_factory = FileFactory::getInstance();
        			$file_factory->registerClass('SessionManager');
        	 		$file_factory->registerClass("AdminGroup", array("path" => "travellog/model/"));
				    $this->mAdvisorGroupID =  $file_factory->invokeStaticClass("AdminGroup","getAdvisorGroupID",array($this->mLoggedUser->getTravelerID()));
				}*/
			}
		}

		function render(){
			$file_factory = FileFactory::getInstance();
			$tpl = new Template();
			if( !$this->mIsLogged ){
				$tpl->set("isPublic", true);
			} else {
			    $tpl->set("isPublic", false);
			}
			$tpl->set("isAdvisor",	$this->mIsAdvisor);
			//$tpl->set("gID", $this->mAdvisorGroupID);
			$tpl->set("loggedUser", $this->mLoggedUser);
			$tpl->set("siteName", isset($GLOBALS['CONFIG']) ? $GLOBALS['CONFIG']->getSiteName() : 'GoAbroad');
			$tpl->out($file_factory->getTemplate("ViewFeature"));
		}
	}