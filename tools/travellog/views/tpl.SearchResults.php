<?if ($displayBy == 'TRAVELER'):?>
	<h2>Search Results</h2>
	<?if (count($results) > 0):?>
		<ul>
		<?if ($viewBy == 'TRAVELER'):
			 foreach($results as $t):?>		
				<?php if($t->isSuspended()) continue; ?>
				<li><a href="/journal.php?action=myjournals&amp;travelerID=<?=$t->getTravelerID()?>"><?=$t->getUserName() . ' (' . $t->getTravelerProfile()->getFirstname() . ' ' . $t->getTravelerProfile()->getLastname() .')' ?></a></li>
			<?endforeach;
		endif;?>	
		
		<?if ($viewBy == 'GROUP'):
			 foreach($results as $t):?>		
				<?php if($t->isSuspended()) continue; ?>
				<li><a href="/journal.php?<?=$origParam?>&amp;tID=<?=$t->getTravelerID()?>&amp;jcp=<?=$jcp?>"><?=$t->getUserName() . ' (' . $t->getTravelerProfile()->getFirstname() . ' ' . $t->getTravelerProfile()->getLastname() .')' ?></a></li>
			<?endforeach;
		endif;?>
		
		</ul>
	<?else:?>
		<p>No results found.</p>
		
	<?endif;?>	
<?endif;?>
<br/>
<a href="<?=$_SERVER['HTTP_REFERER']?>">Go back<a>		