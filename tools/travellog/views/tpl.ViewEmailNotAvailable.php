<div id="statusPanel2" class="widePanel" style="display: none;">
	<p class="loading_message">
		<span id="imgLoading">
		<img alt="Loading" src="/images/loading.gif"/>
		</span>
		<em id="statusCaption">Loading data please wait...</em>
	</p>
</div>

<div id ="invite_staff_form">
	<?php if ( sizeof($errors) ) : ?>
		<div class="errors">					
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= $error ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
	
	<div class="help_text" style="font-size:9px;">
		<span style="font-size:9px;">
			The Email address <strong><?=$emailAdd?> </strong> is already in use. <br />
		</span>
			If you want to assign this user to your subgroups, select the available subgroups below and proceed.
	</div>
	
	<div id="scrolllst1">
		<ul class="form">		

			<li>
				<?
					$arrCheckBox = array();
					foreach($subGroupList as $indList) :
				?>
				<div>
					<input
							type = "checkbox"
							name = "chk<?=$indList->getGroupID()?>"
							id = "chk<?=$indList->getGroupID()?>"
							value = "<?=$indList->getGroupID()?>"
							<?if($indList->isStaff($travelerId) || $indList->isPendingStaffToSubGroup($travelerId)) :?> disabled <?endif;?> style = "float:left" />
					<label style="font-size:10px; font-weight: normal;">
						<? $sgName = stripslashes($indList->getName()) ?>
						<?= (strlen($sgName) > 20) ? substr($sgName, 0, 20).'...' : $sgName ?>
						<? if ($indList->isStaff($travelerId)) :?>
							<i style = "color:red;font-size:10px;">(STAFF)</i>
						<? elseif ($indList->isPendingStaffToSubGroup($travelerId)) : ?>
							<i style = "color:red;font-size:10px;">(SENT INVITE)</i>
						<?endif;?>
					</label>
				</div>
				<?
					$arrCheckBox[] = $indList->getGroupID();
					endforeach;
				?>
			</li>
		</ul>
	</div>
	
	<div class="button">
		<? $lstCheckBox = implode(",", $arrCheckBox) ?>
		<input type = "hidden" name = "lstCheckBox" id = "lstCheckBox" value = "<?=$lstCheckBox?>" />
		<input
				type = "button"
				name = "btnProceed"
				id = "btnProceed"
				value = "Proceed"
				onclick = "AssignStaffManager._proceedAssignExistingEmail('gId=<?=$gId?>&action=Proceed&email=<?=$emailAdd?>')" />
		<input
				type = "button"
				name = "btnCancel"
				id = "btnCancel"
				value = "Cancel"
				onclick = "AssignStaffManager._viewInvite('gId=<?=$gId?>')" />
	</div>
	
	<div class="clear"> </div>
</div>

