<?php
/*
 * Created on Oct 20, 2006
 * Author: Czarisse Daphne P. Dolina
 * Tpl.IncGroupMap.php - displays map with pins representing the current location 
 * of each group member, each pin is linked to he corresponding destination
 */
?>
<div class="section map" id="map">
	<h2>Current Member Locations
		<!-- <?= $grpName ?> -->
	</h2>
		<div class="content">

			<?
				echo $map->addMapFrame($mFrame); 
				echo $map->plotLocations($arrLocationMarker); 
			?>
		
		</div>
		<div class="clear"></div>
</div>