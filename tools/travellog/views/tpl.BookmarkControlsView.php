<!-- AddThis Button BEGIN -->
<div class="jRight addthis_toolbox addthis_default_style">
<a href="http://addthis.com/bookmark.php?v=250&amp;username=goabroadllc" class="addthis_button_compact" addthis:url="<?=$contents['permalink']?>" addthis:title="<?=$contents['title']?>">Share</a>
<span class="addthis_separator">|</span>
<a class="addthis_button_facebook" addthis:url="<?=$contents['permalink']?>" addthis:title="<?=$contents['title']?>"></a>
<a class="addthis_button_myspace" addthis:url="<?=$contents['permalink']?>" addthis:title="<?=$contents['title']?>"></a>
<a class="addthis_button_google" addthis:url="<?=$contents['permalink']?>" addthis:title="<?=$contents['title']?>"></a>
<a class="addthis_button_twitter" addthis:url="<?=$contents['permalink']?>" addthis:title="<?=$contents['title']?>"></a>
</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=goabroadllc"></script>
<script type="text/javascript">
	var addthis_config = { "data_track_clickback" : true };
	var addthis_share = { 
		templates: {
			twitter: 'Check out {{title}} ({{url}}) via @GoAbroad'
		}
	};
</script>
<!-- AddThis Button END -->