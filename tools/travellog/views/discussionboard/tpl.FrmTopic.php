<?php echo $profile->render()?>
<?php $sub_navigation->show(); ?>


<div class="layout_2" id="content_wrapper">

	<ul class="breadcrumbs">
		<li class="root">
			<a href="/discussion_board/home/<?php echo $group_id; ?>"><span>Knowledge Base Home</span></a>
		</li>
		<?php if(0 < $topic_id): ?>
			<li>
				<a href="/topic/discussions/<?php echo $topic_id; ?>"><span><?php echo $title; ?></span></a>
			</li>
			<li>
				<strong><span>Edit</span></strong>
			</li>
		<?php else: ?>
			<li>
				<strong><span>Add New Topic</span></strong>
			</li>
		<?php endif; ?>
	</ul>	
	
	<div id="wide_column">			
		<div class="section" id="dboard" >
			<h2>
				<?php if(0 < $topic_id): ?>
					Edit Topic <span><?php echo $title; ?></span>
				<?php else: ?>
					Add New Topic
				<?php endif; ?>	
			</h2>
			
			<?php if(0 < $topic_id): ?>
			<?php else: ?>
				<div class="section_small_details"> 
					Start a topic for discussion. You will be able to start the discussion on this topic by writing the first post on the next step. 
				</div>
			<?php endif; ?>
			
			<div class="content">
				<div id="dboard_body">
					<form action="/topic/save/" method="post" accept-charset="utf-8" class="interactive_form post">
						<ul class="form">
							<li>
								<label for="txtTitle">Topic</label>
								<?php if (isset($errors['title'])): ?>
									<p class="error"><?php echo $errors['title']; ?></p>
								<?php endif; ?>
								<input name="txtTitle" type="text" value="<?php echo $title; ?>" class="text big" />
							</li>
							<li>
								<label for="txaDescription">Description</label>
								<?php if(isset($errors['description'])): ?>
									<p class="error"><?php echo $errors['description']; ?></p>
								<?php endif; ?>
								<textarea name="txaDescription" id="txaDescription" style="width:45%; height:2.6em"><?php echo $description; ?></textarea>
							</li>
							<li>
								<fieldset class="choices">
									<legend><span>Privacy Settings</legend>
									<?php if (isset($errors['privacy_setting'])): ?>
										<p class="error"><?php echo $errors['privacy_setting']; ?></p>
									<? endif; ?>
									</span>
									<ul>
										<li>
											<input type="radio" <?php echo ($privacy_setting == TopicPeer::PUBLIC_SETTING) ? 'checked="checked"' : '' ?> value="<?php echo TopicPeer::PUBLIC_SETTING; ?>" name="rdoPrivacy"  />
											<label for="rdoprivacy_<?php echo TopicPeer::PUBLIC_SETTING; ?>">Posts for this topic can be viewed by anyone but only members can add posts</label>
										</li>
										<li>
											<input type="radio" <?php echo ($privacy_setting == TopicPeer::MEMBERS_ONLY_SETTING) ? 'checked="checked"' : '' ?> value="<?php echo TopicPeer::MEMBERS_ONLY_SETTING; ?>" name="rdoPrivacy"  />
											<label for="rdoprivacy_<?php echo TopicPeer::MEMBERS_ONLY_SETTING; ?>">Only members can add and view posts for this topic</label>
										</li>
									</ul>
								</fieldset>
							</li>
							<li class="actions">
								<input type="submit" value="Save Topic" class="submit" />
								&nbsp; or  &nbsp;
								<a class="emphasize_link" href="/discussion_board/home/<?php echo $group_id; ?>">&larr; Return to Discussion Board Home</a>
							</li>
						</ul>
						<input type="hidden" name="hidTopicId" value="<?php echo $topic_id; ?>" />
						<input type="hidden" name="hidGroupId" value="<?php echo $group_id; ?>" />
					</form>
				</div>
			</div>
			<div class="foot"></div>
		</div>			
	</div>
	<!-- End Wide Column	 -->
	
	<!-- <div id="narrow_column">
			<div class="section">
				
				<h2> <span> Some Help Text </span> </h2>	
				
				<div class="content">
					<p class="help_text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>					
				</div>
			
				<div class="foot"> </div>
			</div>
		</div> -->


</div>