<?php if ($discussion_helper->hasFeaturedDiscussions()): ?>	
	<table cellspacing="0">
		<thead>
			<tr>
				<th class="first" scope="col">
					<h2>
						Pinned Discussions
					</h2>
				</th>
				<th scope="col"/>
				<th scope="col"/>
				<th class="last" scope="col"/>
			</tr>
		</thead>
		<tbody>												
			<?php $discussion_helper->renderFeaturedDiscussions(); ?>
		</tbody>		
	</table>
<?php endif; ?>

<?php if ($discussion_helper->hasUnFeaturedDiscussions()): ?>
	<table cellspacing="0">
		<?php if ($discussion_helper->hasFeaturedDiscussions()): ?>	
			<thead>
				<tr>
					<th class="first" scope="col">
						<h2>Other Discussions</h2>
					</th>
					<th scope="col"/>
					<th scope="col"/>
					<th class="last" scope="col"/>
				</tr>
			</thead>
		<?php endif; ?>
		<tbody >
			<?php $discussion_helper->renderUnFeaturedDiscussions(); ?>
		</tbody>		
	</table>				
<?php endif; ?>