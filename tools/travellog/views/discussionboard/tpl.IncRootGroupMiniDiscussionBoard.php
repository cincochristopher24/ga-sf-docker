<!--?php
	Template::includeDependentCss('/min/f=css/discussion_board-1.2.css');
	Template::includeDependentJs('/js/DiscussionBoard.js', array('bottom' => true));
?-->
<div id="discuss_board" class="section">
	<h2>
		<span>Knowledge Base </span>
		<span class="header_actions">
			<a href="/discussion_board/home/<?php echo $group_id; ?>">Go to the Knowledge Base</a>
		</span>
	</h2>
	<div class="content">
		<?php if (0 < count($topics)): ?>
			<ul id="dboard_body" class="discuss_content">
				<?php if (0 < count($added_discussions)): ?>
					<input type="hidden" id="hidAddedDiscussionsCount" value="<?php echo count($added_discussions); ?>" >
					<table cellspacing="0" id="addedTable" >
						<thead>
							<tr>
								<th class="first" scope="col">
									<h2>Added Discussions</h2>
								</th>
								<th scope="col"/>
								<th scope="col"/>
								<th class="last" scope="col"/>
							</tr>
							<tr>
								<td colspan="4" class="description">
									<p>These are the discussions added to the knowledge base.</p>	
								</td>
							</tr>
						</thead>
						<tbody>	
							<?php foreach ($added_discussions as $discussion): ?>
								<?php $first_post = $discussion->getFirstPost(); ?>
								<?php $first_poster = $first_post->getPoster(); ?>
								<?php $post_count = $discussion->getPostsCount(); ?>
								<?php if (1 < $post_count): ?>
									<?php $latest_post = $discussion->getLatestPost(); ?>
									<?php $latest_poster = $latest_post->getPoster(); ?>
								<?php endif; ?>
								<tr id="discussion_<?php echo $discussion->getId(); ?>" >
									<td scope="row">
										<h3 >
											<a href="/discussion/posts/<?php echo $discussion->getId(); ?>">
												<?php echo $discussion->getTitle(); ?>
											</a>
										</h3>
											<p class="meta">
												Started by 
												<?php if (PosterPeer::TRAVELER == $first_poster->getPosterType()): ?>
													<a href="<?php echo $first_poster->getFriendlyUrl(); ?>"><?php echo $first_poster->getUserName(); ?></a><?php if (!$is_admin_logged){ ?>,<? } ?>
													<?php if ($is_admin_logged): ?>
														<em><?php echo $first_poster->getFullName(); ?></em>,
													<?php endif; ?>
												<?php else: ?>
													<a href="<?php echo $first_poster->getFriendlyUrl(); ?>"><?php echo $first_poster->getName(); ?></a>,								
												<?php endif; ?>
												
												<?php if (isset($latest_post)): ?>
													<?php echo ($post_count - 1); ?> <?php if (1 < ($post_count - 1)) echo 'Replies'; else echo 'Reply'; ?>,
													last reply <a href="/discussion/posts/<?php echo $discussion->getId(); ?>#<?php echo $latest_post->getId(); ?>"><?php echo GaDateTime::descriptiveDifference($latest_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> Ago</a> 
													by 
													<?php echo $latest_poster->getName(); ?>					
													<?php if ($is_admin_logged AND PosterPeer::TRAVELER == $latest_poster->getPosterType()): ?>
														<em><?php echo $latest_poster->getFullName(); ?></em>
													<?php endif; ?>	
												<?php else: ?>
													<?php echo ($post_count - 1); ?> <?php if (1 < ($post_count - 1)) echo 'Replies'; else echo 'Reply'; ?>
												<?php endif; ?>
											</p>
											<p><?php echo preg_replace('/\n/', '<br />', $first_post->getMessage()); ?></p>										
											<?php if ($is_admin_logged): ?>
												<div class="actions">
													Actions &nbsp;
													<a href="javascript:void(0)" id="archive_<?php echo $discussion->getId(); ?>" <?php if (!$discussion->isActive()) { ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Archive Discussion ?','Are you sure you want to archive this discussion?', function(){ Discussion.archive(<?php echo $discussion->getId(); ?>); },'Archive','1');CustomPopup.setJS();CustomPopup.createPopup();" >
														<span>Archive</span>
													</a> 										
												
													<a href="javascript:void(0)" id="activate_<?php echo $discussion->getId(); ?>" <?php if($discussion->isActive()){ ?>style="display:none;"<? } ?> onclick="CustomPopup.initialize('Activate Discussion ?','Are you sure you want to activate this discussion?', function(){ Discussion.activate(<?php echo $discussion->getId(); ?>); },'Activate','1');CustomPopup.setJS();CustomPopup.createPopup();">
														<span>Activate</span>
													</a>								
													|
													<a id="remove_knowledge_action_<?php echo $discussion->getId(); ?>" onclick="CustomPopup.initialize('Knowledge Base','Are you sure you want to remove this discussion from the knowledge base?', function(){ Discussion.removeFromKnowledgeBase(<?php echo $discussion->getID(); ?>, 0, function(){ var addedCount = parseInt(jQuery('#hidAddedDiscussionsCount').val()); jQuery('#hidAddedDiscussionsCount').val(addedCount-1); if (1 == addedCount) { jQuery('#addedTable').css('display', 'none'); } else { jQuery('#discussion_<?php echo $discussion->getId(); ?>').css('display', 'none'); } }); },'OK','1');CustomPopup.setJS();CustomPopup.createPopup();" href="javascript:void(0);" title="Remove from knowledge base">Remove from Knowledge Base</a>						
												</div>
											<?php endif; ?>
									 </td>																							
								</tr>
							<?php endforeach; ?>																					
						</tbody>		
					</table>	
				<?php endif; ?>
				
				<?php foreach ($topics as $topic): ?>
					<table cellspacing="0">
						<thead>
							<tr>
								<th class="first" scope="col">
									<h2>
										<a title="" href="/topic/discussions/<?php echo $topic->getId(); ?>" class="topic_link"><?php echo $topic->getTitle(); ?></a>
									</h2>
								</th>
								<th scope="col"/>
								<th scope="col"/>
								<th class="last" scope="col"/>
							</tr>
							<tr>
								<td colspan="4" class="description">
									<p><?php echo $topic->getDescription(); ?></p>	
								</td>
							</tr>
						</thead>
						<tbody id="discussions_<?php echo $topic->getId(); ?>" >	
							<?php if ($discussion_helpers[$topic->getId()]->hasFeaturedDiscussions()): ?>	
								<tr> 
									<td class="divider" colspan="4">
										<div>Pinned Discussions</div>
									</td>
								</tr>														
								<?php $discussion_helpers[$topic->getId()]->renderFeaturedDiscussions(); ?>
							<?php endif; ?>
							<?php if ($discussion_helpers[$topic->getId()]->hasUnFeaturedDiscussions()): ?>
								<?php if ($discussion_helpers[$topic->getId()]->hasFeaturedDiscussions()): ?>	
									<tr> 
										<td class="divider" colspan="4">
											<div>Other Discussions</div>
										</td>
									</tr>	
								<?php endif; ?>
								
								<?php $discussion_helpers[$topic->getId()]->renderUnFeaturedDiscussions(); ?>
							<?php endif; ?>																					
						</tbody>		
					</table>			
				<?php endforeach; ?>
	
				<?php if (0 < count($recent_discussions)): ?>
					
					<?// we will not show recently discussed section if we only have 1 recent discussion and that discussion only has 1 post ?>
					<?php if(! (1 == count($recent_discussions) && 1 == $recent_discussions[0]->getPostsCount()) ): ?>
					
					<table cellspacing="0">
						<thead>
							<tr>
								<th class="first">
									<h2>Recently Discussed</h2>
								</th>
								<th>  </th>
								<th>  </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="recent-column" colspan="3">
									<ul class="users recent-list">						
										<?php $cnt = 0; ?>
										<?php foreach($recent_discussions as $discussion): ?>
											<?php $latest_post = $discussion->getLatestPost(); ?>
											
											<?php if( is_null($latest_post) ): continue; endif; ?>
											
											<?php $poster = $latest_post->getPoster(); ?>									
											<li>
											
												<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
													<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
														<img src="<?php echo $poster->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');; ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
													</a>
												<?php else: ?>
													<a href="<?php echo $poster->getFriendlyUrl(); ?>" class="thumb">
														<img src="<?php echo $poster->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" alt="<?php echo $poster->getName(); ?>'s Profile Pic" class="pic"/>
													</a>								
												<?php endif; ?>								
												
												<div class="details">											
											    <strong>
											    	<img title="" alt="pinned" src="/images/v3/discussions/reply-to-discussion.gif" class="pin_img"/>
											    	
														<?php if (PosterPeer::TRAVELER == $poster->getPosterType()): ?>
															<a class="username" href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getUserName(); ?>'s travel blogs and travel notes"><?php echo $poster->getUserName(); ?></a>
												    	<?php if ($is_admin_logged): ?>
												    		<em><?php echo $poster->getFullName(); ?></em>
												    	<?php endif; ?>														
														<?php else: ?>
															<a class="username" href="<?php echo $poster->getFriendlyUrl(); ?>" title="Read <?php echo $poster->getName(); ?>'s travel blogs and travel notes"><?php echo $poster->getName(); ?></a>								
														<?php endif; ?>	
											    </strong>    									    		
													<?php if (1 == $discussion->getPostsCount()): ?>    									    		
														started a new discussion
													<?php else: ?>	
														replied to
													<?php endif; ?>
										    	<a href="/discussion/posts/<?php echo $discussion->getId(); ?>" class="highlight" title=""><?php echo $discussion->getTitle(); ?></a>	
										  		<em><?php echo GaDateTime::descriptiveDifference($latest_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> ago</em>	
										  		<p> <?php echo preg_replace('/\n/', '<br />', $latest_post->getMessage()); ?> </p> 	
											  	
											  	<?php if ($discussion->isActive() && $can_reply): ?>
														<div class="actions">
															Actions  
															<a href="/post/reply/<?php echo $discussion->getId(); ?>">
																<span>Reply</span>
															</a>																																			
														</div>							
													<?php endif; ?>
												</div>
											</li>
											<?php $cnt++; ?>
											<?php if (3 == $cnt): ?>
												<?php break; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									</ul>								
								</td>
							</tr>			
						</tbody>
					</table>
					
					<?php endif; ?>
					
				<?php endif; ?>
			</ul>
		<?php else: ?>
		<?/*	<div class="help_text" style="text-align: center;">
				<span>There are no discussions added to this group.
					<a href="/topic/add/<?php echo $group_id; ?>">Add a Topic?</a> or <a href="/discussionboard/home.php?gID=<?php echo $group_id; ?>">Manage Knowledge Base</a>.
				</span>	
			</div> */ ?>
			<div class="gline">
				<div class="gunit image">
					<img src="/images/g_knowledge.jpg" alt="News" width="180" height="136" />
				</div>
				<div class="glastUnit description">
					<h3>Share and discuss information.</h3>
					<p>
						The Knowledge Base, as its name suggests, is a place that you and your students can use to share useful and official information with your ENTIRE group. It is recommended that you start by adding frequently asked questions and give your answers and recommendations. As the Knowledge Base expands with member contributions over time, this feature will be a useful resource that your members can refer to for any questions before, during and after their trips.
					</p>
					<a class="button_v3 goback_button jLeft" href="/topic/add/<?=$group_id?>"><strong>Add a Topic</strong></a>
				</div>
			</div>
		<?php endif; ?>		
	</div>
</div>