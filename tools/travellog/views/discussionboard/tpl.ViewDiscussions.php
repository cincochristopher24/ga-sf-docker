<?php Template::includeDependentJs('/js/DiscussionBoard.js', array('bottom'=>true)); ?>
<?php $gaDateTime = new GaDateTime(); ?>

<?php echo $profile->render(); ?>
<?php $sub_navigation->show(); ?>

<div class="layout_2" id="content_wrapper">
	<ul class="breadcrumbs">
		<li class="root">
			<a href="/discussion_board/home/<?php echo $group->getGroupID(); ?>"><span>Knowledge Base Home</span></a>
		</li>
		<li>
			<strong><span><?php echo $topic->getTitle(); ?></span></strong>
		</li>
	</ul>
		
	<div id="dboard">			
		<div class="content">												
			<div id="dboard_head">													
				<h2><?php echo $topic->getTitle(); ?></h2>	
				<ul class="meta">
					<li class="first"><?php echo (0 < $discussions_count) ? $discussions_count : 'No'; ?> Discussion<?php if (1 <> $discussions_count) { ?>s<? } ?></li>
					<li><?php echo (0 < $posts_count) ? $posts_count : 'No'; ?> Post<?php if (1 < $posts_count OR 0 == $posts_count) {?>s<? } ?></li>
					<li>Started <?php echo GaDateTime::descriptiveDifference($topic->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> Ago</li>
					<?php if (isset($last_post)): ?>	
						<li>Last Post Created <?php echo GaDateTime::descriptiveDifference($last_post->getDateCreated(), GaDateTime::dbDateTimeFormat()); ?> Ago</li>
					<?php endif; ?>
				</ul>
				<p><?php echo $topic->getDescription(); ?></p>						
			</div>				
				
			<div id="dboard_body">	
				<ul class="post_actions">
					<?php if (($is_member_logged AND !$topic->isArchived()) OR $is_admin_logged): ?>
						<li>
							<a class="button" href="/discussion/add/<?php echo $topic->getId(); ?>" title="">New Discussion</a>
						</li>
					<?php endif; ?>
					<?php if ($is_admin_logged): ?>
						<?php if (0 == $discussions_count): ?>
							<li>
								<a class="button" onclick="CustomPopup.initialize('Delete Topic ?','Are you sure you want to delete the topic? ','/topic/delete/<?php echo $topic->getId(); ?>','Delete','1');CustomPopup.createPopup();" href="javascript:void(0)">Delete Topic</a>
							</li>
						<?php endif; ?>
						<li>
							<a class="button" href="/topic/edit/<?php echo $topic->getId(); ?>" title="Edit Topic">Edit Topic</a>
						</li>
					<?php endif; ?>									
				</ul>
				<table cellspacing="0">
					<thead>
						<tr>
							<th scope="col" class="first">
								<h2>Discussion</h2>
							</th>
							<?php if (0 < $discussions_count): ?>
								<th scope="col">Replies</th>
								<th scope="col">Started by</th>
								<th scope="col" class="last">Last Reply</th>
							<?php endif; ?>
						</tr>
					</thead>
					<tbody id="discussions_<?php echo $topic->getID(); ?>" >
						<?php if (0 < $discussions_count): ?>
							<?php if ($discussion_helper->hasFeaturedDiscussions()): ?>	
								<tr> 
									<td class="divider" colspan="4">
										<div>Pinned Discussions</div>
									</td>
								</tr>														
								<?php $discussion_helper->renderFeaturedDiscussions(); ?>
							<?php endif; ?>
							<?php if ($discussion_helper->hasUnFeaturedDiscussions()): ?>
								<?php if ($discussion_helper->hasFeaturedDiscussions()): ?>	
									<tr> 
										<td class="divider" colspan="4">
											<div>Other Discussions</div>
										</td>
									</tr>	
								<?php endif; ?>
								<?php $discussion_helper->renderUnFeaturedDiscussions(); ?>
							<?php endif; ?>
						<?php else: ?>
							<tr>
								<td >
									<div>The topic has no discussions yet. Click <a href="/discussion/add/<?php echo $topic->getID() ?>">here</a> to add a new discussion.</div>
								</td>
							</tr>	
						<?php endif; ?>																																																										
					</tbody>
				</table>																	
			</div>	
		</div>
		<div class="foot"></div>
	</div>
	<div class="clear"></div>
</div>