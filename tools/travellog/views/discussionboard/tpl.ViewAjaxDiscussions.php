<?php if ($discussion_helper->hasFeaturedDiscussions()): ?>	
	<tr> 
		<td class="divider" colspan="4">
			<div>Pinned Discussions</div>
		</td>
	</tr>														
	<?php $discussion_helper->renderFeaturedDiscussions(); ?>
<?php endif; ?>
<?php if ($discussion_helper->hasUnFeaturedDiscussions()): ?>
	<?php if ($discussion_helper->hasFeaturedDiscussions()): ?>	
		<tr> 
			<td class="divider" colspan="4">
				<div>Other Discussions</div>
			</td>
		</tr>	
	<?php endif; ?>
	<?php $discussion_helper->renderUnFeaturedDiscussions(); ?>
<?php endif; ?>
	
