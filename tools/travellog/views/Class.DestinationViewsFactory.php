<?php
	/*
	 * Class.DestinationViewsFactory.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	 
	 require_once("Class.Constants.php");
	 
	 class DestinationViewsFactory{
	 	
	 	static $instance = NULL;
	
		private $views = array();
		
		static function getInstance(){ 
			if( self::$instance == NULL ) self::$instance = new DestinationViewsFactory; 
			return self::$instance;
		}
	
		function createView( $view = constants::VIEW_ALL_COUNTRIES_PAGE ){
			
			switch( $view ){
				
				case constants::VIEW_COUNTRY_PAGE:
					if( !array_key_exists("ViewIndividualCountry", $this->views) ){
						require_once("travellog/views/Class.ViewIndividualCountry.php");
						$this->views["ViewIndividualCountry"] = new ViewIndividualCountry;
					}
					return $this->views["ViewIndividualCountry"];
					break;
					
				case constants::VIEW_TRAVELERS_CURRENTLY_IN_COUNTRY:
					if( !array_key_exists("ViewTravelersCurrentlyInCountry", $this->views) ){
						require_once("travellog/views/Class.ViewTravelersCurrentlyInCountry.php");
						$this->views["ViewTravelersCurrentlyInCountry"] = new ViewTravelersCurrentlyInCountry;
					}
					return $this->views["ViewTravelersCurrentlyInCountry"];
					break;
				
				case constants::VIEW_TRAVELERS_LIVING_IN_COUNTRY:
					if( !array_key_exists("ViewTravelersLivingInCountry", $this->views) ){
						require_once("travellog/views/Class.ViewTravelersLivingInCountry.php");
						$this->views["ViewTravelersLivingInCountry"] = new ViewTravelersLivingInCountry;
					}
					return $this->views["ViewTravelersLivingInCountry"];
					break;	
				
				case constants::VIEW_JOURNALS_IN_COUNTRY:
					if( !array_key_exists("ViewJournalsInCountry", $this->views) ){
						require_once("travellog/views/Class.ViewJournalsInCountry.php");
						$this->views["ViewJournalsInCountry"] = new ViewJournalsInCountry;
					}
					return $this->views["ViewJournalsInCountry"];
					break;		
					
				case constants::VIEW_COUNTRY_LIST:
					if( !array_key_exists("ViewCountryList", $this->views) ){
						require_once("travellog/views/Class.ViewCountryList.php");
						$this->views["ViewCountryList"] = new ViewCountryList;
					}
					return $this->views["ViewCountryList"];
					break;	
					
				default:
					if( !array_key_exists("ViewAllCountries", $this->views) ){
						require_once("travellog/views/Class.ViewAllCountries.php");
						$this->views["ViewAllCountries"] = new ViewAllCountries;
					}
					return $this->views["ViewAllCountries"];				
			}
			
		}
	 
	 } 
?>
