<div style="margin:30px">
	<h2><?=$site?> Error Report Form</h2>
	
	<br/>
	<div class="ticket_information" style=<?if(isset($success)):?>"display:none"<?endif;?>>
		<p>If you're having problems with our site, please let us know.</p>
		<p>Please complete the following form to submit errors in our site to our development team.</p>
	</div>
	<br/>	
	<?if(isset($success)):?>
		<div id="ticket_confirmation" class="confirmation">
			<?=$success?>
		</div>
	<?endif;?>
	<?if(isset($errors)):?>
		<div id="ticket_errors" class="error_notice">
			<ul>
			<? foreach($errors as $error): ?>
				<li><?=$error?></li>
			<? endforeach; ?>
			</ul>
		</div>
	<?endif;?>
	<br/>
	<div id="ticket_form" style=<?if(isset($success)):?>"display:none"<?endif;?>>
		<form action="ticket.php" method="post" accept-charset="utf-8">
			<ul class="form">
				<li class="labeled">						
					<fieldset>		
						<label>Name </label>					
						<span>
							<input type="text" id="txt_firstname" name="txt_firstname" value="<?=$firstname?>" class="text" />
							<label for="txt_firstname">First Name <span class="required">*</span></label>
						</span>
						<span>
							<input type="text" id="txt_lastname" name="txt_lastname" value="<?=$lastname?>" class="text" />
							<label for="txt_lastname">Last Name </label>
						</span>
					</fieldset>
				</li>
				<li>
					<label>Email Address <span class="required">*</span></label>					
					<input type="text" name="txt_email" value="<?=$email?>" id="txt_email">
				</li>
				<li>
					<label>Subject <span class="required">*</span></label>					
					<input type="text" name="txt_subject" value="<?=$subject?>" id="txt_subject">
				</li>
				<li>
					<label> Describe the problem you encountered. <span class="required">*</span></label>
					<textarea name="txa_message" rows="8" cols="40"><?=$message?></textarea>						
				</li>
			<ul>	
			<p>
				<input type="button" value="cancel" onclick="javascript:history.back();">
				<input type="submit" value="Send &rarr;">
			</p>
		</form>
	</div>
</div>
