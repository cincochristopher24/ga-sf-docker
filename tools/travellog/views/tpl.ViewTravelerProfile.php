

<!-- Begin Top Area -->
<?php echo $profile_comp->render()?>
<!-- End Top Area -->

<?php $subNavigation->show(); ?>


<div id="rundown">
	
	<!-- Begin Content Wrapper -->
	<div id="content_wrapper">
	  
	  <? // shout-out pop-up ?>
		<?= $commentsview->fetchShoutOutPopUp() ?>
		<? // end of shout-out pop-up ?>
    
    <!-- Begin Steps -->
    <?php if (isset($steps)) echo $steps; ?>
    <!-- Begin Steps -->
        
		<!-- Begin Wide Column -->
		<div id="wide_column" class="layout2">			
			
			<?=$featuredAlbumsView->render()?>
			
			<?php if(isset($videos)) echo $videos; ?>
			
		     <!-- BEGIN TRAVEL JOURNALS -->
		     <div id="travel_journals">
		       <div class="content">
		         <?=$journalsComp->render() ?>
		       </div>
		     </div>
			 <!-- END TRAVEL JOURNALS -->
			
			 <!-- BEGIN TRAVEL ARTICLES -->	
			 <div id="travel_articles">
		       <div class="content">
		       		<?=$articleComp->render() ?>
		       </div>
		     </div>
		     <!-- END TRAVEL ARTICLES -->    
	        							
	
	         <!-- BEGIN MESSAGE CENTER -->
	         <?php if (isset($messageCenterView)) : ?>
	         <div id="message_center" class="section">
	         	<h2>
	         		<span>Message Center</span>
					
	         	</h2>
				<div class="content">
			 		<?php echo $messageCenterView ?>
				</div>
	         </div>
	    	<?php endif; ?>
	         <!-- END MESSAGE CENTER -->           

              
	        <!-- Comments Section -->
			<div id="comments" class="section comments">
				  <?php $start = microtime(true);?>
	    		  <?= $commentsview->renderAjax()?>
				  <?php 
				  $end	= 	microtime(true);
				  //echo '(tpl.ViewTravelerProfile.php) commentsview->renderAjax() took ' . ($end - $start) . ' sec <br/>';
				  ?>
			</div>		
			
		</div>	
		<!-- End Wide Column -->
	
		<!-- Begin Narrow Column -->
		<div id="narrow_column">
			
			<!-- Begin Travel Map Section -->
			<?php if (count($userMap->getCountriesTraveled()) > 0 || $owner) :?>				
				<div id="travelmap" class="section" >
					<h2>
						<span id="linkicon">
							<a href="/faq.php#bring_2_facebook"><span>Facebook</span></a>	
						</span>
						<span>MyTravelMap</span>		
					</h2>
					<div class="content">
						<?php echo $travelMap ?>			
					</div>
				</div>
			<?php endif;?>
			<!-- End Travel Map Section -->
	
			
			<!-- Begin Travel Plans Section -->
			<?php if ($showMyTravelPlans) : ?>
			<div id="mytravelplans" class="section">
				<h2>
					<span id="linkicon">
						<a href="/faq.php#bring_2_facebook"><span>Facebook</span></a>	
					</span>
					<span>MyTravelPlans</span>							
				</h2>
				<div class="content">
					<?php echo $incTravelplans;	?>
				</div>
			</div>
			<?php endif; ?>
			<!-- End Travel Plans Section -->			
	
			<!-- Begin Personals Section (Friends, Groups, Calendar)-->			
			<?php if(isset($friends)) echo $friends;?>			
			<?php if(isset($groups)) echo $groups; ?>
			<?php if(isset($calendar)) if($eventCount || $owner) echo $calendar; ?>		
			<!-- End Personals Section -->	
			
			<?php if(!isset($GLOBALS['CONFIG'])): ?>
			<br>
				<div class="section">
					<script type="text/javascript"><!--
						google_ad_client = "ca-pub-0455507834956932";
						/* .NET Sidebar Ad */
						google_ad_slot = "3558779789";
						google_ad_width = 250;
						google_ad_height = 250;
						//-->
					</script>
					<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>		
				</div>      
			<?php endif; ?>
		
			<!-- Begin Advertisement -->
			<? if ($this['advertisement']): ?>
				<div id="gaothers" class="section">

					<div class="content">
						<p id="gaothers_image">
							<a href="<?= $advertisement->getUrl()?>" target="_blank">
								<img src="<?= htmlspecialchars($advertisement->getImagePath())?>" height="90" width="120" alt="<?= htmlspecialchars($advertisement->getAltText())?>" />
							</a>
						</p>
						<p id="gaothers_statement">
							<?= $advertisement->getAdText()?>
						</p>
					</div>
				</div>
			<? endif;?>
			<!-- End Advertisement -->

		</div>
		<!-- End Narrow Section -->

	</div>
	<!-- End Content Wrapper -->

</div>


