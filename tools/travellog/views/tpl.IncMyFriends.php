<?php
/*
 * Created on 09 22, 2006
 * @author Kerwin Gordo
 * Purpose: subtemplate for dashboard(passport)
 */
?>

<div class="section" id="friends">
    <h2><span>Friends (<?=$count ?>)</span>
        <span class="header_actions">
    		<a href="<?= $addFriendsLink ?>">Add Friends</a> 
    	</span>
	</h2>
	<div class="content travelers">
    
	    <!--<div class="meta">
	    	<?if (0 < $count) {?>
		    	<?=$traveler->getUserName()?> has <?=$count ?>
		    		 <? if (1 < $count): ?>
			    	 		friends 
			    	 <? else: ?>
			    	 		friend 
			    	 <? endif;  ?>			    	
		    <? } ?>
		 </div>-->	
		
		
		
		<?php if (isset($friends)):?>		
			<ul class="users elements">
		<?php foreach($friends as $friend) : ?>

			<li <?= ($friend->isOnline()) ? 'class="online"' : 'class="offline"'?>>
				<a title="View Profile" class="thumb" href="/<?=$friend->getUserName()?>" >
					<img class="pic" alt="User Profile Photo" src="<?=$friend->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" />
				</a>
				<div class="details">										
					<strong><a class="username" href="/<?=$friend->getUserName()?>">  <?= $friend->getUserName()?> </a></strong>
					
					<a href="/messages.php?act=messageTraveler&id=<?=$friend->getTravelerID()?>">Send a Message</a> | 
					<a href="/friend.php?action=delete&amp;fID=<?=$friend->getTravelerID()?>" onclick="return confirm('Are you sure you want to remove your friend?')">Remove</a> 
					<br />
					<a class="negative_action" href="/friend.php?action=block&amp;fID=<?=$friend->getTravelerID()?>" onclick="return confirm('Are you sure you want to block your friend?')">Block User</a>
					
				</div>
			</li>
		<?php endforeach; ?>	
		</ul>
		<?php else: ?>    
    		<p class="help_text">
    			<?=$helpText?>	    					
    		</p>    	
    	<?php endif; ?>		
 	</div>
	<div class="foot"></div>	 	
</div>


