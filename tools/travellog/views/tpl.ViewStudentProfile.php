<?
	/**
	 * tpl.ViewStudentProfile.php
	 * @author - marc
	 */
	
	require_once('Class.HtmlHelpers.php');
	$contents["subNavigation"]->show();
?>
<div class="layout_2" id="content_wrapper">
	
	<div class="area" id="wide_column">
		<div class="section" id="student_profile">
			<div class="head_left"><div class="head_right"></div></div>
			<div class="content">
				<? if( strlen($contents["helpText"]) ): ?>
					<h1>Profile Information
					</h1>
					<div class="help_text">
						<?= HtmlHelpers::Textile($contents["helpText"]); ?> 
						<div class="actions">
							<a href="edittravelerprofile.php"><strong>Edit Profile</strong></a>
						</div>	
					</div>
				<? else : ?>		
					
						<h1><?= $contents["firstName"]; ?>&#8217;<? if( !preg_match("/[zs]$/i",$contents["firstName"]) ): ?>s<? endif; ?> Profile Information</h1>
						
						<div id="student_prefered_activities">
							<h2>What I would like to do</h2>		
							<?= $sub_views['STUDENT_PREFERENCE']->render(); ?>
						</div>
						
						<? if ( $contents["countCountry"] ) : ?>
							<div id="student_prefered_countries">
								<h2>&#8230;in <? if( 1 < $contents["countCountry"] ): ?>these countries<? else: ?>this country<? endif; ?> :</h2>
								<?= $sub_views['COUNTRY_PREFERENCE']->render(); ?>
							</div>
						<? endif; ?>
						<div class="clear"></div>
						<div id="listings">
							<?= $sub_views['REFERED_LISTINGS']->render(); ?>	
						</div>
				<? endif; ?>	
				<div class="actions">
					<a href="passport.php"><strong>Return to My Passport</strong></a>
				</div>
			</div>
		
		<div class="foot"></div>
		
		</div>
	</div>
	
	<div class="area" id="narrow_column">
		<div class="section" id="quick_tasks">
			<div class="head_left"><div class="head_right"></div></div>
			<? if($contents["allowed"]) :?>	
				<div class="content">
					<ul class="actions">
						<li><a href="student-profile.php?qry" class="button">Create&nbsp;/&nbsp;Edit Student Profile Information</a></li>
					</ul>
				</div>
			<? endif; ?>	
			
			<div class="foot"></div>
			
		</div>
	</div>
	
	<div class="clear"></div>
</div>
