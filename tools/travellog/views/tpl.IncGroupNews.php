<?php
/*
 * Created on Oct 20, 2006
 * Author: Czarisse Daphne P. Dolina
 * Tpl.IncGroupNews.php - displays group news
 */
?>
<div class="section" id="news">		
	<h2>News
		<span class="actions"><? if (TRUE == $isAdminLogged) { ?><a href="/newsmanagement.php?method=compose&amp;groupID=<?= $grpID ?>" class="add">Post a News</a><? } ?></span>		
	</h2>	
			<div class="content">		
				
				<ul>
					<?  for($idx=0;$idx<count($grpNews);$idx++){
							$eachnews = $grpNews[$idx];
					?>
					<li>
						<div class="meta"><?= $eachnews->getTheDate();?></div>
						<h2><a href="/newsmanagement.php?groupID=<?= $grpID ?>&amp;hlEntryID=<?= $eachnews->getNewsID() ?>"><?= $eachnews->getTitle();?></a></h2>	
					</li>		
					<? } ?>
					<? if (0 == count($grpNews) && $isAdminLogged) { ?>
					<li>
						There are no news added to this group.
					</li>
					<? } ?>
				</ul>	
				<div class="section_foot">
				<? if (isset($grpNewsViewLink) ) { ?><a href="/newsmanagement.php?groupID=<?= $grpID ?>" >View All</a>	<? } ?>	
				</div>
			
			</div>
</div>