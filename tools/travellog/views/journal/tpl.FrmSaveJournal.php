<?php
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::includeDependentJs('/js/interactive.form.js');
	Template::setMainTemplateVar('layoutID', 'journals');
?>

<?php echo $profile->render(); ?>
<?php echo $sub_navigation->show(); ?>

<div id="content_wrapper">
	<div id="wide_column" class="layout_2">
	
		<?php if (0 < count($unfinished_entries)): ?>
			<?php foreach($unfinished_entries as $unfinished_entry): ?>
				<div class="infoBox">
					<p>
						<strong>You have an unfinished entry</strong> 
							" <a href="/journal_draft/view_draft/<?php echo $unfinished_entry->getJournalEntryDraftID(); ?>"><?php echo $unfinished_entry->getTitle(); ?></a> " 
						<strong><a href="/journal_draft/view_draft/<?php echo $unfinished_entry->getJournalEntryDraftID(); ?>">continue here</a></strong>
						
						<span> or </span>
						<strong><a href="/journal_draft/view_saved_drafts/">view other saved draft</a></strong>
					</p>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
				
		<div class="formbox">        
			<h1>Create a Journal</h1>
			<?php if (isset($errors) AND 0 < count($errors)): ?>
				<div class="errors">
					<ul>
						<?php foreach($errors as $error): ?>
							<li><?php echo $error; ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
			
			<form name="sform" action="/journal.php" method="post" class="interactive_form">
				<ul class="form add_journal">
					<li>
						<label class="inpt_box">Journal Title <span class="required">*</span></label>
					
						<p class="supplement">
							Name your journal book. You will be able to add individual titles for each of your entries later.<br />
							Some examples: &#8220;My First Trip to Europe&#8221;, &#8220;Vacation in Jamaica&#8221;.
						</p>
							
							<input type="text" name="title" id="title" size="40" value="<?php echo $title; ?>" class="text big" />
					</li>		
					<li>
						<label class="inpt_box">Description <span class="required">*</span></label>

						<p class="supplement">
							Please write a few words (2-3 sentences) describing what your entries for this journal will be about. <br />
							This is <strong>NOT</strong> your journal entry yet. You will be able to add your entries in the next step.
						</p>
						
						<textarea name="description" id="description"><?php echo $description; ?></textarea>		
					</li>
					<li id="regbutton" class="actions jRight">
						<input type="submit" name="action" value="Next" class="button_v2" />
					</li>
				</ul>
			</form>
		</div>
	</div>

	<div id="narrow_column">
		<div class="helpextra"></div>
		<div class="helpbox">
			<h2>What is a Journal?</h2>
			<p>Your Journal is like a book where you record your experiences and trip photos, and the journal entries
are like pages of this book. You may use one Journal for your entire trip and add a series of journal
entries for all its details.
			</p>
		</div>
	</div>
</div>
	</div>