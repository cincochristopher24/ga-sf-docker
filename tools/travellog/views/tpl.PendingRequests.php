<? if( isset($pending_requests) && count($pending_requests) ): ?>
	<div class="section">
		<h2>
			<span>Pending Requests</span>
			<span id="pr_loader" style="display:none;">
				<img alt="Loading" src="/images/loading_small.gif" width="15" height="15" />
			</span>
		</h2>
		<div class="content">
			<ul class="users">
				<? $i = 0;
				   foreach ( $pending_requests as $pending_request): ?>
					<?php if(!$showAll && ($i == $showN)): ?>
						</ul><ul class="users pendingRequests" style="display:none;">
					<?php elseif($showAll && ($i == $showN)): ?>		
						</ul><ul class="users pendingRequests">
					<?php endif; ?>		
				
					<li id="pr_<?=$pending_request->getTravelerID()?>" <? if($i === 0):?>class="first"<?endif;?>>																
						<a title="View Profile" class="thumb" href="profile.php?action=view&amp;travelerID=<?=$pending_request->getTravelerID()?>">
							<img alt="User Profile Photo" src="<?=$pending_request->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>"  width="37" height="37" />
						</a>
						<!--edited by nick: 10/22/2009-->
						<div class="details request">								
						<strong><a class="username" href="/<?php echo $pending_request->getUserName()?>">
							<?= $pending_request->getUserName()?>
						</a></strong>
						<!--edited by ianne - 11/11/2008-->
						<p>
							<a href="javascript:void(0)" onclick="cancelFriend({username:'<?=$pending_request->getUserName()?>',friendID:'<?=$pending_request->getTravelerID()?>', n:'<?=$showN?>'});"><span>Cancel</span></a>
						</p>
						</div>
					</li>							
				<? $i++;
				   endforeach; ?>
			</ul>
			<div style="margin-top:2px;font-size:10px;padding-bottom:5px;">
				<?if(!$showAll && count($pending_requests) > $showN):?>	
					<a href="javascript:void(0)" class="pendingRequests more" style="float:right;text-decoration:none;">+ view more</a>
				<?elseif($showAll && count($pending_requests) > $showN):?>
					<a href="javascript:void(0)" class="pendingRequests" style="float:right;text-decoration:none;">- view less</a>
				<?endif;?>
			</div>
		</div>
	</div>
<? endif; ?>
<script type="text/javascript">
	(function($){
		$('a.pendingRequests').click(function(){
			if($(this).is('.more')){
				$('ul.pendingRequests').slideDown('slow');
				$(this).html('- view less');
			}else{
				$('ul.pendingRequests').slideUp('slow');
				$(this).html('+ view more');
			}
			$(this).toggleClass('more');
			$('#pending_requests').myScroll();
		});
	})(jQuery);
</script>