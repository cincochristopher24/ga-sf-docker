<?php Template::setMainTemplate('travellog/views/tpl.LayoutMain.php'); ?>
<div id="wrapper1">
	<table>	
		<tr>
			<td>
			<form name="frmFilter" method="post">
				<strong>Filter:</strong>
				<select name="filter" style="margin:2px;">
					<option value="<?= TravelBioManager::ALL_ACCOUNT ?>" <?= (TravelBioManager::ALL_ACCOUNT == $filter)?"selected":"" ?> >All Users</option>
					<option value="<?= TravelBioManager::FB_ACCOUNT ?>" <?= (TravelBioManager::FB_ACCOUNT == $filter)?"selected":"" ?>>Facebook Users Only</option>
					<option value="<?= TravelBioManager::GA_ACCOUNT ?>" <?= (TravelBioManager::GA_ACCOUNT == $filter)?"selected":"" ?>>GoAbroad User Only</option>
					<option value="<?= TravelBioManager::SYNC_ACCOUNT ?>" <?= (TravelBioManager::SYNC_ACCOUNT == $filter)?"selected":"" ?>>Synchronized Users Only</option>
				</select>&nbsp;
				<input type="submit" name="submit" value="Apply Filter" />
			</form>
			</td>
			<td>
				<form name="frmSearch" method="post">
					<strong>Search</strong>
					<input type="text" name="searchText" value="<?=$searchText?>" />
					<strong>by</strong>
					<select name="searchBy" style="margin:2px;">
						<option value="lastName" <?= ("lastName" == $searchBy)?"selected":"" ?> >Last Name</option>
						<option value="firstName" <?= ("firstName" == $searchBy)?"selected":"" ?> >First Name</option>
						<option value="facebookID" <?= ("facebookID" == $searchBy)?"selected":"" ?> >Facebook ID</option>
						<option value="travelerID" <?= ("travelerID" == $searchBy)?"selected":"" ?> >Traveler ID</option>
					</select>
					<input type="submit" name="submit" value="Go&gt;&gt;" />
				</form>				
			</td>
		</tr>
	</table>
	<br /><br />
	
	<? if ( $paging->getTotalPages() > 1 ):?>
		<? $start = $paging->getStartRow() + 1; 
		   $end = $paging->getEndRow(); 
		?>
		<div class="pagination">
			<div class="page_markers">
			<?= $start . " - " . $end . " of " . $totalrec ?>
			</div>	
			<? $paging->getFirst(); ?> | <? $paging->getPrevious(); ?> | <? $paging->getLinks(); ?> | <? $paging->getNext(); ?> | <? $paging->getLast(); ?>
		</div>
	<? endif; ?>
	
	<table>
		<tr>
			<td><strong>#</strong></td>
			<td><strong>TravelBioUserID</strong></td>
			<td><strong>Name</strong></td>
			<td><strong>Sex</strong></td>
			<td><strong>UserType</strong></td>
			<td><strong>TravelerID</strong></td>
			<td><strong>FacebookID</strong></td>
		</tr>
		<?php
			$iterator->rewind();
			while($iterator->valid()):
				$iUser = $iterator->current();	
				//foreach($travelBioUsers as $iUser):
		?>		
			<tr>
				<td>#</td>
				<?php if(!is_null($iUser)): ?>
				<td><?= $iUser->getTravelBioUserID(); ?></td>
				<td><?= $iUser->getName() ?></td>
				<td><?= $iUser->getSex() ?></strong></td>
				<td>					
					<?= ($iUser->isSynchronized()?'Sync':($iUser->isFbUserOnly()?'Facebook':'GoAbroad') ) ?>
				</td>
				<td><?= $iUser->getTravelerID() ?></td>
				<td><?= $iUser->getFbUserID() ?></td>
				<?php else: ?>
				<td colspan="6" align="center">--- Non Existing Traveler ---</td>
				<?php endif; ?>
			</tr>
		<?php 
			$iterator->next();
			endwhile;
			//endforeach;
		?>
	</table>
	
	<div class="clear"></div>
	
	
	<? if ( $paging->getTotalPages() > 1 ):?>
		<div class="pagination">
			<div class="page_markers">
			<?= $start . " - " . $end . " of " . $totalrec ?>
			</div>	
			<? $paging->getFirst(); ?> | <? $paging->getPrevious(); ?> | <? $paging->getLinks(); ?> | <? $paging->getNext(); ?> | <? $paging->getLast(); ?>
		</div>
	<? endif; ?>
	
</div>