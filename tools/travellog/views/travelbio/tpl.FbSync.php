<fb:dashboard></fb:dashboard>
<?= $dashboard ?>

<fb:editor-custom label="">
<!-- this form is from tpl.GaSync.php. added by Adelbert S.-->
<div id="wrapper">
	<div id="top_section"></div>
	<div id="banner"></div>
	<div id="gray_filler"></div>
	<div id="content">			

		<?php if('SHOW_APP_ERROR'==$tplAction): ?>
			<p id="para1" style="color:#dd0000;font-weight:bold;margin-top:20px;margin-bottom:15px">
				Error! Only facebook users can use this application.
			</p>
		<?php elseif('SHOW_FORM'==$tplAction): ?>				
			<p class="para" id="para1">
				If you have a GoAbroad Network account, you can synchronize your Facebook Travel Bio with your GoAbroad Network profile. Synchronizing means that both your Facebook and GoAbroad Network profiles will show the same Travel Bio answers no matter where you edit them. To synchronize, type in your GoAbroad Network email address and password.
			</p>
			<?php if($fbUser->isSynchronized()): ?>
			<p class="para">
				<strong>NOTE: </strong>Your Travel Bio app is currently synchronized with your GoAbroad Network account <?= $fbUser->getUserName() ?>. 
				To synchronize your Facebook app with another GoAbroad Network account, please fill out the form below. If already synchronized, click on the 
				Unsynchronize button to remove sychronization.
				<fb:editor action="fbsync.php" labelwidth="1" width='500'>	
				<ul class="forms_section">						
						<input class="inputsubmit" type="submit" name="submit" value="Unsynchronize" style="width:100px;"/>
				</ul>
				</fb:editor>
			</p>           			

			<!-- added by Adelbert S. -->
			<?php else : ?>

			<p class="para">
				Not yet a GoAbroad Network member? Sign up for your FREE account now! <a href="http://www.goabroad.net/register.php" class="link">Find out how GoAbroad Network can change the way you travel.</a> 
			</p>

			<fb:editor action="fbsync.php" labelwidth="1" width='500'>	
			<p id="para3" style="font-weight:bold">
				Login details of the GoAbroad.net account that you want your Facebook Travel Bio to be synchronized with:
			</p>
			<ul class="forms_section">
				<?php if(!is_null($notice)): ?>

					<p style="color:#dd0000;font-weight:bold;margin-top:20px;margin-bottom:15px">
						<?= $notice ?>
					</p>

				<?php endif; ?>
					<label id="username_label"><strong>Email:</strong></label> </br>
					<input type="text" name="username" value="" style="width: 150px; margin-bottom: 10px;"/> </br>


					<label id="password_label"><strong>Password:</strong></label> </br>
					<input type="password" name="password" value="" style="width: 150px; margin-bottom: 10px;"/>

					</br></br>
					<input class="inputsubmit" type="submit" name="submit" value="Synchronize" style="width:100px;" />
					<input class="inputsubmit" type="submit" name="submit" value="Skip" style="width:60px;" />

			</ul>
			<?php if(isset( $travelerID )):	 ?>
				<input type="hidden" name="travelerID" value="<?=$travelerID?>" />
			<?php endif; ?>												
			<input type="hidden" name="facebookID" value="<?= $fbUser->getFbUserID() ?>" />
			</fb:editor>

			<!-- added by Adelbert S. -->
			<?php endif; ?>

		<?php elseif('SHOW_ERROR_DUAL_ACCOUNT'==$tplAction): ?>			

			<fb:editor action="fbsync.php" labelwidth="1" width='500'>	
			<p class="para">
				We have detected that you already have answers to your Travel Bio on GoAbroad Network.
				Please choose which account has the answers that you wish to keep.<br />
				<br />
				Note: Choosing cannot be undone. If you choose your GoAbroad.net answers, this will delete
				your answers on Facebook and change them to your GoAbroad.net answers. Likewise, if you choose your
				Facebook answers, your GoAbroad.net profile will then change to reflect your choice.<br />					
				<br />
				If you wish to keep your profiles as they are, just click on <strong>Skip</strong>.
				<input type="hidden" name="travelerID" value="<?=$travelerID?>" />
			</p>				
			<ul id="forms_section">

					<input type="submit" name="submit" value="Keep my GoAbroad.net TravelBio" />
					<input type="submit" name="submit" value="Keep my Facebook TravelBio" />
					<input type="submit" name="submit" value="Skip" />

			</ul>
			<input type="hidden" name="facebookID" value="<?= $fbUser->getFbUserID() ?>" />
			<input type="hidden" name="travelerID" value="<?=$travelerID?>" />
			</fb:editor>

		<?php elseif('SHOW_SKIP'==$tplAction): ?>
			<p id="para1" style="color:#dd0000;font-weight:bold;margin-top:20px;margin-bottom:15px">
				Synchronization process has been cancelled. No changes have been made.<br />
			</p>
			<p>
				You can choose to go back to your <a href="http://www.facebook.com/profile.php?id=<?= $fbUser->getFbUserID() ?>" class="link">Facebook Profile</a> or you can proceed to <a href="http://www.goabroad.net/login.php" class="link">login to your GoAbroad.net account</a>.
            </p>
			<p class="para">
				Not yet a GoAbroad Network member? Sign up for your FREE account now! <a href="http://www.goabroad.net/register.php" class="link">Find out how GoAbroad Network can change the way you travel.</a> 
			</p>
			<ul id="forms_section"></ul>
		<?php elseif('SHOW_SUCCESS'==$tplAction): ?>
			<p id="para1" style="color:#008800;font-weight:bold;margin-top:20px;">  
				Synchronization process was successful! 
			</p>
			
			<p id="para2" style="font-weight:bold;">
				Your Travelbio in Facebook is now synchronized with your GoAbroad.net account <?= $syncedTraveler->getUserName() ?>
			</p>
			
			<p id="para3">
				You can choose to go back to your <a href="http://www.facebook.com/profile.php?id=<?= $fbUser->getFbUserID() ?>" class="link">Facebook Profile</a> or you can proceed to <a href="http://www.goabroad.net/login.php" class="link">login to your GoAbroad.net account</a>.
			</p>

			<p class="para">
				Not yet a GoAbroad Network member? Sign up for your FREE account now! <a href="http://www.goabroad.net/register.php" class="link">Find out how GoAbroad Network can change the way you travel.</a> 
			</p>
			<ul id="forms_section"></ul>
		<?php elseif('SHOW_ERROR_ALREADY_SYNCED'==$tplAction): ?>
			<p id="para1" style="color:#dd0000;font-weight:bold;margin-top:20px;margin-bottom:15px">
				Sorry, we are unable to proceed with the synchronization process.
			</p>
			<p id="para1" style="font-weight:bold">
				Your facebook account has already been synchronized with the GoAbroad Network
				account that you have entered.
			</p>

			<ul id="forms_section"></ul>			
		<?php endif;?>
	</div>
</div>		
<!-- this form is from tpl.GaSync.php. added by Adelbert S.-->		
	</fb:editor-custom>

