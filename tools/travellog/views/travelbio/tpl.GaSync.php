<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="Cache-Control" content="NO-CACHE" />
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<title>GoAbroad - Facebook Synchronize</title>
<link type="text/css" rel="stylesheet" href="/css/travelbio/gasync.css">
</head>
<body>
	<div id="wrapper">
		<div id="top_section"></div>
		<div id="banner"></div>
		<div id="gray_filler"></div>
		<div id="content">			
			<?php if('SHOW_APP_ERROR'==$tplAction): ?>
				<p class="para" id="para1">
					Error! Only facebook users can use this application.
				</p>
			<?php elseif('SHOW_FORM'==$tplAction): ?>				
				<p class="para" id="para1">
					If you have a GoAbroad Network account, you can synchronize your Facebook Travel Bio with your GoAbroad Network profile. Synchronizing means that both your Facebook and GoAbroad Network profiles will show the same Travel Bio answers no matter where you edit them. To synchronize, type in your GoAbroad Network email address and password.
				</p>
				<p class="para">
					Not yet a GoAbroad Network member? Sign up for your FREE account now! <a href="/register.php" class="link">Find out how GoAbroad Network can change the way you travel.</a> 
				</p>
				<?php if($fbUser->isSynchronized()): ?>
				<p class="para">
					<strong>NOTE: </strong>Your Travel Bio app is currently synchronized with your GoAbroad Network account <?= $fbUser->getUserName() ?>. 
					To synchronize your Facebook app with another GoAbroad Network account, please fill out the form below. If already synchronized, click on the 
					Unsynchronize button to remove sychronization.
					<form action="gasync.php" method="post">
					<ul class="forms_section">						
						<li class = "sync_labels">
							<input class="editorkit_button" type="submit" name="submit" value="Unsynchronize" />
						</li>						
					</ul>
					</form>
				</p>
				<?php endif; ?>
				<form action="gasync.php" method="post">
				<p class="para" id="para3">
					Login details of the GoAbroad.net account that you want your Facebook Travel Bio to be synchronized with:
				</p>
				<ul class="forms_section">
					<?php if(!is_null($notice)): ?>
					<li class = "sync_labels">
						<p class="highlight">
							<?= $notice ?>
						</p>
					</li>
					<?php endif; ?>
					<li class = "sync_labels">
						<label id="username_label">Email :</label>
						<input class="input_text" type="text" name="username" value="" />
					</li>
					<li class = "sync_labels">
						<label id="password_label">Password :</label>
						<input class="input_text" type="password" name="password" value="" />
					</li>
					<!--
					<li class = "sync_labels" id="buttons">
						<input type="checkbox" name="log_me" />
						<label id="log_me_label">Log me in to GoAbroad Network</label>
					</li>
					-->
					<li class = "sync_labels" id="buttons">
						<input class="editorkit_button" type="submit" name="submit" value="Synchronize" />
						<input class="editorkit_button" type="submit" name="submit" value="Skip" />
					</li>
				</ul>
				<?php if(isset( $travelerID )):	 ?>
					<input type="hidden" name="travelerID" value="<?=$travelerID?>" />
				<?php endif; ?>												
				<input type="hidden" name="fbUserID" value="<?= $fbUser->getFbUserID() ?>" />
				</form>
			<?php elseif('SHOW_ERROR_DUAL_ACCOUNT'==$tplAction): ?>			
				<form action="gasync.php" method="post">
				<p class="para">
					We have detected that you already have answers to your Travel Bio on GoAbroad Network.
					Please choose which account has the answers that you wish to keep.<br />
					<br />
					Note: Choosing cannot be undone. If you choose your GoAbroad.net answers, this will delete
					your answers on Facebook and change them to your GoAbroad.net answers. Likewise, if you choose your
					Facebook answers, your GoAbroad.net profile will then change to reflect your choice.<br />					
					<br />
					If you wish to keep your profiles as they are, just click on <strong>Skip</strong>.
				</p>				
				<ul id="forms_section">
					<li class = "sync_labels">
						<input class="editorkit_button" type="submit" name="submit" value="Keep my GoAbroad.net TravelBio" />
						<input class="editorkit_button" type="submit" name="submit" value="Keep my Facebook TravelBio" />
						<input class="editorkit_button" type="submit" name="submit" value="Skip" />
					</li>
				</ul>
				<input type="hidden" name="fbUserID" value="<?= $fbUser->getFbUserID() ?>" />
				<input type="hidden" name="travelerID" value="<?=$travelerID?>" />
				</form>
			<?php elseif('SHOW_SKIP'==$tplAction): ?>
				<p class="para" id="para1">
					Synchronization process has been cancelled. No changes have been made.<br />
					You can choose to go back to your <a href="http://www.facebook.com/profile.php?id=<?= $fbUser->getFbUserID() ?>" class="link">Facebook Profile</a> or you can proceed to <a href="/login.php" class="link">login to your GoAbroad.net account</a>.
				</p>
				<p class="para">
					Not yet a GoAbroad Network member? Sign up for your FREE account now! <a href="/register.php" class="link">Find out how GoAbroad Network can change the way you travel.</a> 
				</p>
				<ul id="forms_section"></ul>
			<?php elseif('SHOW_SUCCESS'==$tplAction): ?>
				<p class="para" id="para1">
					Synchronization process was successful. Your Travelbio in Facebook is now sychronized with your GoAbroad.net account <?= $syncedTraveler->getUserName() ?>.<br />
					You can choose to go back to your <a href="http://www.facebook.com/profile.php?id=<?= $fbUser->getFbUserID() ?>" class="link">Facebook Profile</a> or you can proceed to <a href="/login.php" class="link">login to your GoAbroad.net account</a>.
				</p>
				<p class="para">
					Not yet a GoAbroad Network member? Sign up for your FREE account now! <a href="/register.php" class="link">Find out how GoAbroad Network can change the way you travel.</a> 
				</p>
				<ul id="forms_section"></ul>
			<?php elseif('SHOW_ERROR_TRAVELER_ALREADY_SYNCED'== $tplAction): ?>
				<p class="para" id="para1">
					Sorry, we are unable to proceed with the synchronization process. The email address and password you
					entered points to a GoAbroad Network account that has already been synchronized with
					another Facebook account.<br />
					<br />					
					You can choose to go back to your <a href="http://www.facebook.com/profile.php?id=<?= $fbUser->getFbUserID() ?>" class="link">Facebook Profile</a> or you can try to 
					<a href="http://<?= $_SERVER['HTTP_HOST'] ?>./travelbio/gasync.php" class="link">synchronize with your other facebook accounts.</a>
				</p>
				<ul id="forms_section">				
					
				</ul>
			<?php elseif('SHOW_ERROR_ALREADY_SYNCED'==$tplAction): ?>
				<p class="para" id="para1">
					Sorry, we are unable to proceed with the synchronization process. Your facebook account has already been synchronized with the GoAbroad Network
					account that you have entered.
				</p>
				<ul id="forms_section"></ul>			
			<?php endif;?>
		</div>
		<div id="footer">
			<p id="copyright" style = "color: white;">Copyright 2007 GoAbroad.net</p>
		</div>
	</div>
</body>
</html>