<fb:dashboard></fb:dashboard>
<?= $dashboard ?>

<style type="text/css"><?= $css ?></style>
<fb:editor action="edit.php" labelwidth="6" width='500'>
	<?php 
		$ctr = 0;
		foreach($profileQuestions as $iQuestion):
	?>	
	 	<fb:editor-custom label="<?= ++$ctr ?>">
			<div><?= $iQuestion->getQuestion() ?></div>
			<br />
			<?php 
				if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT): 
					$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
						? $structAns[$iQuestion->getQuestionID()][0]->getValue()
						:'';
			?>
				<input type="text" name="txt_<?= $iQuestion->getQuestionID() ?>" value="<?= htmlentities($val) ?>" />
			<?php 
				elseif($iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA): 
					$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
						? $structAns[$iQuestion->getQuestionID()][0]->getValue()
						:'';
			?>
				<textarea cols="30" rows="5" name="txa_<?= $iQuestion->getQuestionID() ?>"><?= htmlentities($val) ?></textarea>
			<?php elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX): ?>				
				<ul class="checkbox_list">					
					<?php 						
						$choices = $iQuestion->getChoices();
						$selections = array();
						$textVal = '';						
						if(array_key_exists($iQuestion->getQuestionID(),$structAns)){
							foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){							
								if($iChoice->isTypeCheckbox()){
									$selections[] = htmlentities($iChoice->getValue()); 
								}
								elseif($iChoice->isTypeText()){
									$textVal = htmlentities($iChoice->getValue()); 
								}
							}
						}
						$i=0;
						foreach($choices as $iChoice):
					?>
						<li class="checkbox_list_item">
							<input id="ans_<?=$i ?>" type="checkbox" name="chk_<?= $iQuestion->getQuestionID() ?>[]" style="width:auto" value="<?= htmlentities($iChoice->getChoice()) ?>" <?= in_array($iChoice->getChoice(),$selections)?'checked="true"':'' ?>  />&nbsp; <label for="ans_<?=$i ?>"><?= $iChoice->getChoice() ?> </label>
						</li>
					<?php $i++; endforeach;?>					
					<li class="checkbox_list_item checkbox_list_item_text">
						<input type="text" name="txtchk_<?= $iQuestion->getQuestionID() ?>" value="<?= htmlentities($textVal) ?>" />	
					</li>
				</ul>
			<?php endif; ?>
		</fb:editor-custom>
	<?php endforeach; ?>
	<fb:editor-custom label="">
		Unanswered questions will not show up on your profile, and by default, ALL of your answers will be shown. 
		To set the amount of space that MyTravelBio will take up on your profile, please go to <a href="<?= FBini::getFBRootDirectory() ?>settings.php" title="Go to settings">Settings</a>.
	</fb:editor-custom>
	<fb:editor-buttonset>
   		<fb:editor-button name="submit" value="Save" />
   		<fb:editor-button name="submit" value="Cancel" />   		
 	</fb:editor-buttonset>
</fb:editor>