<fb:dashboard></fb:dashboard>
<?= $dashboard ?>
<fb:editor action="settings.php" labelwidth="1" width='500'>	
	<fb:editor-custom label="">
		<div>
			All of your answers will be shown on your profile. To show fewer answers, please set a number below:
		</div>
		
		<?php if(isset($_POST['submit'])) : ?>
		 	<p style='color:#008800;font-weight:bold;margin: 20px 0 30px 0'> Your Settings changes has been successfully saved! </p>
		<?php endif; ?>
		
		Number of questions to show on your profile: (maximum is 7)
		
		<select name="number_question">			
			<?php // for($i=3;$i<count($profileQuestions)-1;$i++):
			 	for($i=3;$i<8;$i++): ?>
				<option value="<?=$i?>" <?php if($user->getNumOfQuestionsDisplayed() == $i) echo "selected" ?>><?=$i?></option>
			<?php endfor; ?>
			<option value="ALL" <?php if($user->getNumOfQuestionsDisplayed() == 'ALL') echo "selected" ?>>All</option>
		</select>
	</fb:editor-custom>
	<fb:editor-buttonset>	
   		<fb:editor-button name="submit" value="Save Settings" />
   		<fb:editor-button name="submit" value="Cancel" />   		
 	</fb:editor-buttonset>
</fb:editor>