<?php
Template::setMainTemplate("travellog/views/tpl.LayoutPopup.php");
Template::setMainTemplateVar('layoutID', 'main_pages');
?>
<div id="popup_content">
	<div class="section" >
		<h1 id="pop_header"><span>What is Synchronize?</span></h1>
		<div class="content">
			<p>
				If you have a Facebook account, you can synchronize your 
				GoAbroad Network Travel Bio with your Facebook profile app. 
				Synchronizing means that both your Facebook and GoAbroad 
				Network profiles will show the same Travel Bio answers no 
				matter where you edit them. To synchronize, login to your 
				Facebook account. Once logged in, just add the Travel Bio 
				app, and choose Synchronize from the settings.
			</p>
		</div>
		<div class="foot"></div>
	</div>
</div>