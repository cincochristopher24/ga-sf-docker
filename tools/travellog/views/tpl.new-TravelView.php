<?php
$file_factory  = FileFactory::getInstance();
require_once('Class.HtmlHelpers.php');
?>

<?php if( !$objSubNavigation->getDoNotShow() ) echo $objSubNavigation->show();?>

<?php if( $props['is_login'] && isset($props['is_owner']) ):?>

<?elseif( strtolower($props['action']) != 'myjournals' ):?> 

<div id="intro_container">
	<div id="intro">
		<h1>Share Travel Journals and Photos and Stay Connected.</h1>	
		<div class="content">
			<p>
				<?php
					$vartitle = 'GoAbroad Network helps you keep memoirs of your travel
					while keeping your friends and family posted.';
					echo (isset($GLOBALS['CONFIG'])) ? str_replace('GoAbroad Network' , $GLOBALS['CONFIG']->getSiteName(), $vartitle) : $vartitle ;					
				?>
			</p>
		</div>
	</div>
</div>

<?php endif;?>
<div id="content_wrapper" class="layout_2 custom">
	<div id="wide_column">
				
  				<?php echo $journalsComp->render();?>
 	
	</div>
	<div id="narrow_column">
		<?php if( isset($props['is_owner']) && $props['is_owner'] && isset($props['action']) && (strtolower($props['action']) == 'myjournals' || strtolower($props['action']) == 'groupjournals')):?>
			<div id="quick_tasks" class="section"> 
				<h2>
				<span>Quick Task</span>
				</h2>
				<div class="content">
              		<ul class="actions">
              			<?if( isset($props['is_owner']) && $props['is_owner'] ):?>
	              			<?if( $objPaging != NULL ):?>
	              				<?if( strtolower($props['action']) != 'myjournals'):?> 
	                				<li <?if( strtolower($props['action']) != 'groupmembersjournal' ):?>id="sel"<?endif;?>><a class="button" href="/journal.php?action=MyJournals<?=$props['query_string']?>">Manage Journals</a></li>
	                			<?else:?>
	                				<!--li <?if( strtolower($props['action']) != 'groupmembersjournal' ):?>id="sel"<?endif;?>><a class="button" href="/journal.php?action=GroupJournals<?=$props['query_string']?>">Manage Journals</a></li-->
	                			<?endif;?> 
	                		<?endif;?> 
                			<?if( $props['is_login'] && isset($props['is_parent']) && $props['is_parent'] ):?>
                				<li <?if( strtolower($props['action']) == 'groupmembersjournal' ):?>id="sel"<?endif;?>><a class="button" href="/journal.php?action=GroupMembersJournal">Members Journals</a></li>
                			<?endif;?> 
		                	<li><a class="button" href="/travel.php?action=add">Create a Travel Journal</a></li> 
			            <?endif;?>

        		    </ul>
              		<div class="clear"></div>
			    </div>
				<div class="foot"></div>
		  	</div> 
		<?php else:?>
		    <?php if(isset($collectionCountry) && count($collectionCountry) ):?>
				<div id="search" class="section"  style="padding-bottom: 0px;">
					<h2><span>Search by</span></h2>
		            <div class="content" >
						<form action="journal.php" method="get" onsubmit="return false;">
							<fieldset class="fieldset1">
		              	        <p class="row_wrap"> 
					                <select name="countryID" id="countryID" class="countries">
										<option value="0" <?php if( $props['locationID'] == 0 ): ?>selected<?php endif;?>>Choose a Country </option>
					                	<?php foreach( $collectionCountry as $obj_country ):?>
											<?php if($obj_country instanceOf Country): ?>
						                		<option value="<?php echo $obj_country->getLocationID()?>" <?php if( $props['locationID'] == $obj_country->getLocationID() ): ?>selected<?php endif;?>><?php echo $obj_country->getName()?></option>
											<?php endif; ?>
										<?endforeach;?>
					                </select>
									<span id="search_quicksearch" class="actions"><input id="btnFilter" name="btnFilter" onclick="search()" value="Show Journals" class="submit" type="submit" /></span>
				                </p> 
			                </fieldset>
		                </form>
					              
			        </div>
				</div>			
				<?php if(!isset($GLOBALS['CONFIG'])): ?>
				<div class="section">
					<script type="text/javascript"><!--
						google_ad_client = "ca-pub-0455507834956932";
						/* .NET Sidebar Ad */
						google_ad_slot = "3558779789";
						google_ad_width = 250;
						google_ad_height = 250;
						//-->
					</script>
					<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>		
				</div>      
				<?php endif; ?>

				<?php
					$fbRecommendationsTemplate = new Template();
					$fbRecommendationsTemplate->out("travellog/views/tpl.fbRecommendationsBox.php");
				?>
				<!-- Map for Journals -->
				<?php if(isset($collectionCountry) && count($collectionCountry) ):?>
					<?php
						require_once('travellog/model/Class.SiteContext.php');
						$map_key = SiteContext::getInstance()->getGMapApiKey();
						Template::includeDependentJs("http://maps.google.com/maps?file=api&v=2&key=$map_key", array('bottom' => true,'minify' => false));
						Template::includeDependentJs('/js/GMAPS/markermanager.js', array('bottom' => true));
					?>	
					<div id="travel_destinations" class="section">
						<h2> 
							<span>Map of Travel Journals</span>
						</h2>
					    <div class="content row_wrap">
							<div id="map" class="img_border" style="width: <?php echo null?>px; height: 300px;"></div> 
							<p class="dark_gray">Click the balloon to view the travel journals in that location</p>
					    </div>
					</div>
				<?php endif;?> 
				<?//php if( isset($obj_map_view)) echo $obj_map_view->render()?>
				
				<!--     End         -->	
			<?php endif;?>	
		<?php endif;?>	
	
	    <?php if( $props['is_login']): ?>
	        <div id="journal_actions" class="section">
	            <div class="content">
	                <a href="/journal.php?action=add" class="button"><span>+ Add a new Journal</span></a>
					<a href="/article.php?action=add" class="button"><span>+ Add a new Article</span></a>
                </div>
            </div>
	    <?php endif;?>
		<!--div id="tag_cloud_container" class="tagcloud section">
		</div-->
		
	</div>
	<div class="clear"></div>
 </div><!-- <script src="/js/tagcloud.js"></script> -->

<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$.ajax({
				url: "/retrieve-map.php",
				data: "context=2",
				type: "GET",
				success: function(res){
					eval(res);
				}
			});	
		});
	})(jQuery);
</script>
