<?php

//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('layoutID', 'page_photos');
	
	
//Template::includeDependentJs ( "/js/jquery-1.2.6.js" ,array('bottom'=> true) );
//Template::includeDependentJs ( "/js/jquery-1.1.4.pack.js"         ,array('bottom'=> true) );

Template::includeDependentCss( "/min/f=css/Thickbox.css");
Template::includeDependentJs ( "/min/f=js/prototype.js");

if( isset($_REQUEST["type"]) && isset($_REQUEST["ID"]) && isset($_REQUEST["context"]) ){
	Template::includeDependentJs ( "/js/yahooUI/carousel/yahoo-dom-event.js"  );
	Template::includeDependentJs ( "/js/yahooUI/carousel/utilities.js");
	//Template::includeDependentJs ( "/js/yahooUI/carousel/dragdrop-min.js"  );
	Template::includeDependentJs ( "/js/yahooUI/carousel/container_core-min.js"  );
	Template::includeDependentJs ( "/js/yahooUI/carousel/carousel.js"   );
	Template::includeDependentCss( "/js/yahooUI/carousel/carousel.css"               );

	Template::includeDependentJs (array('/js/scriptaculous/scriptaculous.js','/js/scriptaculous/effects.js',	'/js/scriptaculous/builder.js',	'/js/scriptaculous/dragdrop.js'),array('bottom'=> true) );
	Template::includeDependentCss( "/lytebox/lytebox.css");
	Template::includeDependentJs (array("/lytebox/lytebox.js",	"/js/ajaxfileupload.js"),array('bottom'=> true) );	
	Template::includeDependentJs("/js/imgcropper/lib/cropper.js",array('bottom'=> true) ); 
	Template::includeDependentCss('/js/imgcropper/min/f=css/cropper.css');
}else{
	Template::includeDependentJs("/js/jcarousel-lite.js",array('bottom'=> true));
}
			

Template::includeDependentJs (array("/js/thickbox.js","/js/PhotoUILayout.js"),array('bottom'=> true) );

$context 	= $ArrData['context'];
$loginid 	= $ArrData['loginid'];
$genid 		= $ArrData['genid'];

$photo =<<<BOF
	<script type = "text/javascript">
	if( 'undefined' == typeof(PhotoService) ){//condition added by jul
		jQuery.noConflict();
		PhotoService = new mPhotoService('$context', $loginid, $genid);
		PhotoService.setLayout(1); 
		//jQuery("#photogallery").ready(function(){    
		//	PhotoService.loadgallery(null,0);
		//});
	}
	</script>
BOF;

Template::includeDependent($photo,array('bottom'=> true) );

?>

			
<? echo $profile_header->render(); ?>
			
<?= $subNavigation->show();  ?>

<div id="rundown">
	<!-- Begin Content Wrapper -->
	<div id="content_wrapper">

						<!-- Begin Wide Column -->
						<div id="wide_column" class="layout2">
							<div class="section" id="photo_main_view">
								<h1>
									<span id="albumheader">
										<span><? echo $Title ;?></span>
										<? if( "travellog" == $context ): ?>
											<span class="header_actions"><a href="<?=$BackLink?>">Go to Journal</a></span>
										<? endif; ?>
									</span>
								</h1>
								<div class="content">
									<div id="gallerycontainer">
											
											<? echo $galleryPhotos;?>
											
									</div>
									
								</div>
								<!-- End Wide Column -->
								<!-- Begin Narrow Column -->
							</div>
						</div>
						<!-- end Wide Column -->
						
						
		
				
	</div>
	<!-- End Content Wrapper -->
</div>
<!--  End Rundown -->





	
	