<?php
	/*
	 * Class.ForgotPasswordViewsFactory.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	
	 require_once("Class.Constants.php");
	 
	 class ForgotPasswordViewsFactory{
	 	
	 	static $instance = NULL;
	
		private $views = array();
		
		static function getInstance(){ 
			if( self::$instance == NULL ) self::$instance = new ForgotPasswordViewsFactory; 
			return self::$instance;
		}
	
		function createView( $view = constants::VIEW_FORGOT_PASSWORD_FORM_PAGE ){
			
			switch( $view ){
				
				case constants::VIEW_FORGOT_PASSWORD_CONFIRMATION_PAGE:
					if( !array_key_exists("ViewForgotPasswordConfirmation", $this->views) ){
						require_once("travellog/views/Class.ViewForgotPasswordConfirmation.php");
						$this->views["ViewForgotPasswordConfirmation"] = new ViewForgotPasswordConfirmation;
					}
					return $this->views["ViewForgotPasswordConfirmation"];
				
				default:
					if( !array_key_exists("ViewForgotPasswordForm", $this->views) ){
						require_once("travellog/views/Class.ViewForgotPasswordForm.php");
						$this->views["ViewForgotPasswordForm"] = new ViewForgotPasswordForm;
					}
					return $this->views["ViewForgotPasswordForm"];
					break;	
					
			}
			
		}
	 
	 } 
?>