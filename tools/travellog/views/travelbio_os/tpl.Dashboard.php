<body class="yui-skin-sam">
<div id="tabs" class="yui-navset">
	<ul class="yui-nav">
		<li <?if($selected_tab == 1):?>class="selected" <?endif;?>><a onclick="bioTabs('showBio')" href="javascript:void(0)"><em>My Travel Bio</em></a></li>
	    <li <?if($selected_tab == 2):?>class="selected" <?endif;?>><a onclick="bioTabs('editBio')" href="javascript:void(0)"><em>Update Bio</em></a></li>
	    <li <?if($selected_tab == 3):?>class="selected" <?endif;?>><a onclick="bioTabs('TraveBioSync')" href="javascript:void(0)"><em>Synchronize</em></a></li>
		<li <?if($selected_tab == 4):?>class="selected" <?endif;?>><a onclick="bioTabs('settings')" href="javascript:void(0)"><em>Settings</em></a></li>
	</ul>
</div>
