<?php if (isset($profView))
		  echo $profView->render();
?>
<?php if( !$objSubNavigation->getDoNotShow() ) echo $objSubNavigation->show();?>

<div id="content_wrapper" class="layout_2 custom">
  		<?php
  				echo $journalsComp->render();
  		?>
  		<?php
  				echo $articlesComp->render();
  		?>
</div>

