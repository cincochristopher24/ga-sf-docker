<?php
/**
 * Created on Aug 10, 2006
 * Purpose : template for displaying travels summary for a traveler
 */
 ?>  


<div id="journals_summary" class="sub_section">
	<!--<h2>All Travel Journals <span class="actions"><?if (null != $travels_array  ): ?><a class="add" href=<?=$addTravelLink ?> >Add a Travel Journal</a><?endif;?></span></h2>-->	
	<?php if ($travels_array != null) : ?>
		<ul class="travel_journals_container">
			<?$index = 0; // travels_array index tracker?>			
			<?php foreach($travels_array as $travel) : ?>
				<li>
					<div class="travel_entry">
						<a name="anchor<?=$travel->getTravelID()?>"></a>
						<img class="arrow" id="arrow<?=$travel->getTravelID()?>" title="Show Entries" src="/images/sections/uncollapsed_arrow.gif" alt="expand" /><div id="arrow_status<?=$travel->getTravelID()?>" style="display:none">uncollapsed</div>
						<div class="details">
							<h3 id="title_<?=$travel->getTravelID()?>">
								<!-- edited by daf; show clickable link only if there are entries-->	
								<? if ($travel->hasAnyEntry()) : ?>
									<a class="journal_title" href="/journal-entry.php?action=view&amp;travelID=<?=$travel->getTravelID()?>" ><?= $travel->getTitle()  ?></a>
								<? else: ?>
									<?= $travel->getTitle()  ?>
								<? endif; ?>
							</h3>
							<p class="views">
								Views: <?= $travel->getViews()  ?>						
							</p>							
							<p class="text"><?= HtmlHelpers::truncateText($travel->getDescription(), 6); ?></p>
							
							<?	//$hasTravelEntries = false;
								//if (array_key_exists(''.$travel->getTravelID(),$hasTravelEntriesAr))
							  	//	$hasTravelEntries = true;						
							?>
							
							
							<?if (array_key_exists($travel->getTravelID(),$canEditJournalAr) && $canEditJournalAr[$travel->getTravelID()]):?>
								<div class="actions" id="<?=$travel->getTravelID()?>">								
									<div>
										<a href="/travel.php?action=edit&amp;travelID=<?=$travel->getTravelID() ?>">Edit</a>&nbsp;|&nbsp;
										<span id="del_link_<?=$travel->getTravelID()?>" <?if ($travel->getEntryCount() > 0):?>style="display:none"<?endif;?>>
											<a href="javascript:void(0)" onclick="confirmDelete('/travel.php?action=delete&amp;travelID=<?=$travel->getTravelID() ?>','<?=$travel->getTitle()?>')">Delete</a>&nbsp;|&nbsp;
										</span>
										<a href="/journal-entry.php?action=add&travelID=<?=$travel->getTravelID()?>" title="">Add Entry</a>&nbsp;|&nbsp;<a href="javascript:void(0)" class="show_entry" id="<?=$travel->getTravelID()?>">Show Entries</a><a href="javascript:void(0)" class="hide_link" id="link<?=$travel->getTravelID()?>" style="display:none" >Hide Entries</a>
										<?if($travel->getEntryCount() > 0 && !$travel->getPublish()):?>
											&nbsp;|&nbsp;
											<a href="javascript:void(0)" id="pubj<?=$travel->getTravelID()?>" onclick="publishJournal(<?=$travel->getTravelID()?>)" class="anchor_publish"><?if ($travel->getPublish()):?><span class="span_unpublish">Unpublish</span><?else:?><span class="span_publish">Publish</span<?endif;?></a>
										<?endif;?>
									</div>
								</div>
							<?else:?>
								<div class="actions" id="<?=$travel->getTravelID()?>">								
									<div><a href="javascript:void(0)" class="show_entry" id="<?=$travel->getTravelID()?>">Show Entries</a><a href="javascript:void(0)" class="hide_link" id="link<?=$travel->getTravelID()?>" style="display:none" >Hide Entries</a>
										</div>
								</div>
							<?endif;?>															
						</div>
						<?$photo = $travel->getRandomPhoto()?>
						<img class="journal_image" width="64px" height="64px" src="<?=$photo->getPhotoLink('thumbnail')?>">
						<div class="clear"></div>
					</div>
					<div class="journal_entries" id="journal_entries<?=$travel->getTravelID()?>">
												
						<div class="container_entries" id="response<?=$travel->getTravelID()?>"  style="display:none" >
							<?	$t = new Template();
								$t->set('entries',$tlogs);
								$t->set('travelId',$travel->getTravelID());
								$t->set('canAdd',$canAdd);
								$t->set('canEditJournalAr',$canEditJournalAr);
								//$t->set('traveler',$travel->getOwner());								
								echo $t->fetch('travellog/views/tpl.AjTravelEntries.php');
							?>							
						</div>
						
						
						<div class="hide_link" id="link<?=$travel->getTravelID()?>" style="display:none">
							<a href="#anchor<?=$travel->getTravelID()?>" class="hide_entry_link" id="link<?=$travel->getTravelID()?>">Hide Entries</a>
						</div>
						
					</div>	
					<?if($index == 0 && $travel->getEntryCount() > 0 ):?>
						<script language="JavaScript" type="text/javascript">
							// simulate click if first journal has journal entries
							window.onload = function() {jQuery("#arrow<?=$travel->getTravelID()?>").click(); };							
						</script>  
					<?endif;?>				
				</li>
				<?$index++;?>
			<?php endforeach; ?>
		</ul>
		
	<?php else: ?>
		

		<?if($canAdd):?>
			<p class="help fixhelp">			 
				<?= $helptext ?>	   			
			</p>
		
			<div class="actions">			
				<a class="button" href="<?=$addTravelLink ?>" id="abtn_create_first_journal" >Create Your First Travel Journal &raquo; </a>					
			</div>
		<?else:?>
			<p class="help_text">			 
				<span>No journals for this group</span>.	   			
			</p>
		<?endif;?>	
			
	<?php endif; ?>	

</div>