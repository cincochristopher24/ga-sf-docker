<?php

require_once('Class.StringFormattingHelper.php');
require_once('Class.HtmlHelpers.php');
require_once('Class.GaDateTime.php');
$d = new GaDateTime();

Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('layoutID', 'page_journal');

if($obj_subnavigation->getDoNotShow())
	Template::setMainTemplateVar('page_location', 'Journals');
else
	Template::setMainTemplateVar('page_location', 'My Passport');

ob_start();
$obj_map->includeJs();
$google_map_include = ob_get_contents();
ob_end_clean();

$code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	var manager = new TravellogManager(); 
	window.onload = function(){ 
		manager.useLoadingMessage();
	}
//]]>
</script>
BOF;

Template::includeDependentJs("/js/prototype.js");
Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");
Template::includeDependentJs("/js/moo1.ajax.js");
Template::includeDependentJs("/js/travellogManager.js");
Template::includeDependent($google_map_include);
Template::includeDependent($code);

if ($context == 1) echo $obj_subnavigation->show();


require_once('travellog/model/Class.Advertisement.php');

$advertisement = Advertisement::getAd();

?>
		<div class="area<? if(!$obj_subnavigation->getDoNotShow()) : ?> mine<? endif; ?>" id="intro" >
			<div class="section" id="journal_title">				
				<a href="/journalfeed.php?jid=<?= $obj_travel->getTravelID() ?>" title="RSS Feed" style="display:block;float:right;"><span style="position:relative;top: -5px;">Subscribe <?if( $isAdvisor  ) echo $obj_profile->getName(); else echo $obj_profile->getUserName(); ?>'s Journal</span>
				  <img src="/images/rss_journals.gif" title="Subscribe to this journal" alt="rss feed icon" style="margin-left:5px;"  />
				</a>
				<h1><?= $obj_travel->getTitle() ?></h1>
				<div class="content">
					
					<div class="meta">
						<?= HtmlHelpers::Textile($obj_travel->getDescription()) ?>
					</div>
					<? if($fromAddEdit): ?>
						<p class="help_text">
							<strong>Journal entry has successfully been <?= ($fromAddEdit == 1)? 'added': 'updated' ?>.</strong>
							<?if( $fromAddEdit == 1 ):?>
								<br />Do you want to add photos? <a href="/photomanagement.php?action=add&amp;travelerID=<?= $arr_travellog[0]->getTravelerID() ?>&amp;cat=travellog&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>">Click here to add.</a>
							<? endif; ?>
						</p>
					<? endif; ?>
					
					<? if ($showControls): ?>
						<div class="actions">
							<strong>
								  <a href="/journal.php?action=showedit&amp;travelID=<?= $travelID ?>">Edit</a>
								| <a href="/journal-entry.php?action=add&amp;travelID=<?= $travelID ?>" class="add">Add Entry</a>
							</strong>
						</div>		
					<? endif; ?>
				</div>
			</div>
		</div>
		<?php 
			if ( count($arr_travellog) ) {
				$is_has_journal = TRUE;
				$obj_comment->setContext(3,$arr_travellog[0]->getTravelLogID());
				$obj_comment->readComments();
				
				$arr_obj_photo = $arr_travellog[0]->getSecondaryPhotos($obj_rowslimit);
				
			} else {
				$is_has_journal = FALSE;
				$arr_obj_photo = array();
			}
		?>
		<div class="area" id="main">
			<div id="main_entry_contents" <?php if( count($arr_obj_photo) < 1 ) echo 'class="has_no_photos"'; ?>>
				<div id="main_entry_contents_wrapper">
					<div id="main_entry_contents_wrapper2">
						<div class="area" id="journal_and_comments">
						
							<div class="section journal" id="journal_entry">							
								<?php if ($is_has_journal) : ?>
						 			<img src="http://images.goabroad.com/images/flags/flag<?= $arr_travellog[0]->getTrip()->getLocation()->getCountry()->getCountryID(); ?>.gif" title="<?= $arr_travellog[0]->getTrip()->getLocation()->getCountry()->getName(); ?>" alt="<?= $arr_travellog[0]->getTrip()->getLocation()->getCountry()->getName(); ?>" align="right" id="journal_entry_flag" />
						  			<h1><?=$arr_travellog[0]->getTitle()?></h1>
						  			<div class="meta">
										<? 
											$cityName    = $arr_travellog[0]->getTrip()->getLocation()->getName(); 
											$countryName = $arr_travellog[0]->getTrip()->getLocation()->getCountry()->getName();
											if( strcasecmp($cityName, $countryName) == 0 )
												echo $countryName;
											else
												echo $cityName . ', ' . $countryName;  
										?> | 
										<span class="date"><?= $d->set($arr_travellog[0]->getTrip()->getArrival())->friendlyFormat()?></span>
							  		</div>
							  		
							  			<? if ($showControls): ?>
										<div class="nav_actions">
										
										<?php if (count($arr_obj_photo) < 1 ) :?>
							  				<a class="add_pics1" href="/photomanagement.php?action=add&amp;travelerID=<?= $arr_travellog[0]->getTravelerID() ?>&amp;cat=travellog&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>"><span>Add Your<br/>Photos Here</span></a>
							  			<?php else: ?>	
							  				<a class="add_pics2" href="/photomanagement.php?action=add&amp;travelerID=<?= $arr_travellog[0]->getTravelerID() ?>&amp;cat=travellog&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>"><span>Add Photos</span></a>
							  			<?php endif; ?>
										
											<a class="edit_msg" href="/journal-entry.php?action=showedit&amp;travellogID=<?= $arr_travellog[0]->getTravellogID() ?>"><span>Edit Entry</span></a>
											<a class="del_msg" href="/journal-entry.php?action=delete&amp;travelID=<?= $travelID ?>&amp;travellogID=<?= $arr_travellog[0]->getTravelLogID() ?>" onclick="return confirm('Are you sure want to delete this entry?');"><span>Delete Entry</span></a>
									     </div>
										<div class="clear"></div>
									    <? endif; ?>
							  		<?if( strlen(trim($arr_travellog[0]->getCallOut())) ):?>
							  			<div class="pull_quote"><?=$arr_travellog[0]->getCallOut()?> <img src="images/c_quote.jpg" alt="" class="c_quote"></div>
							  		<?endif;?>
										  		
						  			<div class="content">
										<div class="section map" id="mappads">
											<h2>Map</h2>
											<div class="content">
												<? 
													$obj_mFrame->setWidth(164);
													$obj_mFrame->setHeight(164);
													$obj_map->addMapFrame($obj_mFrame);
												?>
												<?= $obj_map->plotLocations($arr_location_marker) ?>
											</div>
										</div>
							  			<?php if ($arr_travellog[0]->getPrimaryPhoto()->getPhotoID()): ?>
							  				<div class="photo">
								  				<a href="/photomanagement.php?car=travellog&amp;action=vfullsize&amp;cat=travellog&amp;photoID=<?= $arr_travellog[0]->getPrimaryPhoto()->getPhotoID() ?>&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>">
								  					<img src="<?=$arr_travellog[0]->getPrimaryPhoto()->getPhotoLink('default')?>" alt="Journal Entry Photo" />
								  				</a><br />
								  				<span id="photocaption" style="font-size:10px"><?$arr_travellog[0]->getPrimaryPhoto()->getCaption()?></span>
								  			</div>
							  			<?php endif; ?>
							  			<?= HtmlHelpers::Textile($arr_travellog[0]->getDescription())?>

										<div class="actions pagination">
											<? if ($has_prev || $has_next): ?>
												<ul>
													<? if ($has_prev): ?>
														<li class="previous">
															<a href="/journal-entry.php?action=view&amp;travellogID=<?= $has_prev ?>" title="Go to the previous journal entry"><span>&laquo;</span> <?= HtmlHelpers::TruncateText($prev_title, 3) ?></a>
														</li>
													<? endif; ?>
													<? if ($has_next): ?>								
														<li class="next">
															<a href="/journal-entry.php?action=view&amp;travellogID=<?= $has_next ?>" title="Go to the next journal entry"><?= HtmlHelpers::TruncateText($next_title, 3) ?> <span>&raquo;</span></a>
														</li>
													<? endif; ?>
												</ul>
											<? endif; ?>
												
											<ul>
												<? if ( !isset($_SESSION['travelerID']) || ($_SESSION['travelerID'] != $obj_travel->getTravelerID()) ): ?> 
													<? if (isset($_GET['travellogID'])) : ?>
														<li class="next"><span style="font-size:10px"><a href="/reportabuse.php?jeID=<?= $_GET['travellogID'] ?>">Report inappropriate journal entry</a></span></li>
													<? elseif (isset($_GET['travelID'])) : ?>
														<li class="next"><span style="font-size:10px"><a href="/reportabuse.php?jID=<?= $_GET['travelID'] ?>">Report inappropriate journal</a></span></li>
													<? endif ; ?>
												<? endif; ?>
											</ul>
										</div>
									</div>

								<?php else: ?>
									<p class="help_text"><strong>No entries for this Journal!</strong></p>		
								<?php endif; ?>
								
								<? if ($showControls && $is_has_journal): ?>
										<div class="nav_actions">
										<?php if (count($arr_obj_photo) < 1 ) :?>
							  				<a class="add_pics1" href="/photomanagement.php?action=add&amp;travelerID=<?= $arr_travellog[0]->getTravelerID() ?>&amp;cat=travellog&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>"><span>Add Your<br/>Photos Here</span></a>
							  			<?php else: ?>	
							  				<a class="add_pics2" href="/photomanagement.php?action=add&amp;travelerID=<?= $arr_travellog[0]->getTravelerID() ?>&amp;cat=travellog&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>"><span>Add Photos</span></a>
							  			<?php endif; ?>
										
											<a class="edit_msg" href="/journal-entry.php?action=showedit&amp;travellogID=<?= $arr_travellog[0]->getTravellogID() ?>"><span>Edit Entry</span></a>
											<a class="del_msg" href="/journal-entry.php?action=delete&amp;travelID=<?= $travelID ?>&amp;travellogID=<?= $arr_travellog[0]->getTravelLogID() ?>" onclick="return confirm('Are you sure want to delete this entry?');"><span>Delete Entry</span></a>
									     </div>
										<div class="clear"></div>
							    <? endif;?>
							</div>
								
							<?php if ($is_has_journal) : ?>
									<?=$obj_comment->renderAjax()?>
							<?php endif; ?>
						</div>
					
						<div class="area" id="photos_area">
							<div class="section photos">
								<h2>Photographs</h2>
								<div class="content">
									<?php if ($is_has_journal):?>
										<?php if( count($arr_obj_photo) ):?>
											<? if ($showControls): ?>
												<a href="/photomanagement.php?action=view&amp;cat=travellog&amp;travelerID=<?= $arr_travellog[0]->getTravelerID() ?>&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>">Manage photos and captions</a><br />
											<?endif;?>
											<?foreach($arr_obj_photo as $obj_photo):?>
												<div class="photo">
													<a href="/photomanagement.php?cat=travellog&amp;action=vfullsize&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>&amp;photoID=<?= $obj_photo->getPhotoID() ?>">
														<img src="<?=$obj_photo->getPhotoLink('default')?>" alt="<?= ($obj_photo->getCaption()) ? $obj_photo->getCaption() : 'Photo for Journal Entry' ?> " />
													</a><br />
													<span id="photocaption<?=$obj_photo->getPhotoID()?>" style="font-size:10px"><?=$obj_photo->getCaption()?></span><br />
													<? if ($showControls): ?>
														<a href="javascript:void(0)" onclick="manager.editPhotoCaption('mode=editPhotoCaption&photoID=<?=$obj_photo->getPhotoID()?>&element=photocaption<?=$obj_photo->getPhotoID()?>')">[edit caption]</a>
													<?endif;?>
												</div>
											<?endforeach;?>
											<div class="clear"></div>
											<div class="section_foot">
												<a href="/photomanagement.php?action=view&amp;cat=travellog&amp;travelerID=<?= $arr_travellog[0]->getTravelerID() ?>&amp;genID=<?= $arr_travellog[0]->getTravelLogID() ?>" class="more">More Photos</a>
											</div>
										 <?php endif;?>
									<?php endif;?>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="area" id="profile_and_entries">
				<div class="section" id="profile">
					<h2><? if($isAdvisor): ?>
							Group Info
						<? else: ?>
							Author Info
						<? endif; ?>
					</h2>
					<div class="content">
						<? if($isAdvisor): ?>
							<h3 class="advisor"><a href="/group.php?gID=<?= $obj_profile->getGroupID() ?>" title="View this Group's Profile"><?= $obj_profile->getName() ?></a></h3>
							<a href="/group.php?gID=<?= $obj_profile->getGroupID() ?>" class="group_emblem" title="View Profile">
							<? if ($obj_profile->getGroupPhoto()): ?>
								<img src="<?= $obj_profile->getGroupPhoto()->getPhotoLink() ?>" alt="Emblem" />
							<? endif; ?>
							</a>
						<? else: ?>
							<a href="/profile.php?action=view&amp;travelerID=<?= $obj_profile->getTravelerID() ?>" class="user_thumb" title="View Profile">
							<? if ($obj_profile->getPrimaryPhoto()): ?>
									<img src="<?= $obj_profile->getPrimaryPhoto()->getThumbnailPhotoLink() ?>" alt="User profile photo thumbnail" />
								<? endif; ?>
							</a>
							<h3><a href="/profile.php?action=view&amp;travelerID=<?= $obj_profile->getTravelerID() ?>" title="View Profile of <?= $obj_profile->getUserName() ?>"><?= HtmlHelpers::truncateWord($obj_profile->getUserName(), 14) ?></a></h3>
						<? endif; ?>
						<div class="profile_info meta">
							<? if($isAdvisor): ?>
								<? if ( count($obj_profile->getMembers()) ) : ?>
									<div class="members"><strong>Members:</strong> <?= count($obj_profile->getMembers()) ?></div>
								<? endif; ?>
								<? if ( $obj_profile->getInstitutionInfo()->getMission() != NULL ):?>
									<div class="mission_statement">
										<strong>Mission Statement:</strong><br />
										<?= HtmlHelpers::TruncateText($obj_profile->getInstitutionInfo()->getMission(), 30) ?>
									</div>
								<? endif; ?>
							<? else: ?>
								<? if ( $obj_profile->getAge() ) : ?>
									<div class="age">
										<strong>Age: </strong>
										<?=$obj_profile->getAge()?>
									</div>
								<? endif; ?>
								<? if ( $obj_profile->getCurrLocationID() ): ?>
									<div class="location">
										<strong>Currently in:</strong>
										<? 
											$location = $obj_profile->getCurrLocation();
											if (strcasecmp(get_class($location),"country") == 0)
												echo $location->getName();
											else
												echo $location->getName().', '.$location->getCountry()->getName();
										?>	
									</div>
								<? endif; ?>
								<? if ( $obj_profile->getInterests() != NULL ):?>
									<div class="philosophy"><strong>Traveling Philosophy :</strong><br /><?= $obj_profile->getInterests() ?></div>
								<? endif; ?>
								<? if ( $showControls ): ?> 
									<div class="actions">
										<a href="/edittravelerprofile.php?action=edit&amp;travelerID=<?= $obj_profile->getTravelerID() ?>">Edit Profile</a>
									</div>
								<? endif; ?>
								
							<? endif; ?>	
						</div>
						<div class="clear"></div>
					</div>
				</div>
						
				<?php if ($is_has_journal) : ?>
					<div class="section" id="entry_list">											
						<div class="content">
							<? if($total_records): ?>
								<? if ( $start_row != 0 ): ?>
									<div class="tpagination" >
										<div style="font-size:10px;"><a href="/journal-entry.php?action=view&amp;travellogID=<?= $first_entry ?>">Read first entry</a></div>
										<div class="previous"><a style="padding:10px 0 5px 10px;" href="/journal-entry.php?action=view&amp;page=<?= $page-1 ?>&amp;travelID=<?= $travelID ?>">Previous 5 entries</a></div>
										<div class="clear"></div>
									</div>
								<? endif; ?>
								<ul>
									<? for ($i = $start_row; $i < $end_row; $i++): ?>
										<li <? if ($current_travellogID==$arr_tableofcontents[$i]->getTravelLogID()) echo 'class="active"' ?>>
										<a href="/journal-entry.php?action=view&amp;travellogID=<?= $arr_tableofcontents[$i]->getTravelLogID().htmlspecialchars($query_string); ?>" title="<?= $arr_tableofcontents[$i]->getTitle() ?>">
											<strong><?=$arr_tableofcontents[$i]->getTitle() ?></strong>
											<span class="meta"><?= $arr_other_info[$arr_tableofcontents[$i]->getTravelLogID()] ?></span>
										</a>
									<? endfor; ?>
								</ul>
								<? if ( $total_records != $end_row ): 
									if( $total_records > $end_row+4 )
										$entries = 5;
									else
										$entries = $total_records-$end_row;
									$label   = ( $entries > 1 )? 'entries': 'entry';
								?>
									<div class="pagination">
										<div class="next"><a style="padding:3px 0 12px 10px;margin:0;"  href="/journal-entry.php?action=view&amp;page=<?= $page+1 ?>&amp;travelID=<?= $travelID ?>" style="padding:2px 0 8px 10px;margin:10px;">Next <?=$entries . ' ' .$label?></a></div>
										<div style="font-size:10px;"><a href="/journal-entry.php?action=view&amp;travellogID=<?= $recent_entry ?>">Read most recent entry</a></div>	
										<div class="clear"></div>
									</div>
								<? endif; ?>
							<? endif; ?>
						</div>
				    </div>
				<?php else: ?>
					
				<?php endif; ?>
					
				<div id="gaothers">
					<p id="gaothers_image">
						<a href="<?= $advertisement->getUrl()?>" target="_blank">
							<img src="<?= htmlspecialchars($advertisement->getImagePath())?>" height="90" width="120" alt="<?= htmlspecialchars($advertisement->getAltText())?>" />
						</a>
					</p>
					<p id="gaothers_statement">
						<?= $advertisement->getAdText()?>
					</p> 
				</div>				
			</div>
			<div class="clear"></div>
		</div>
		<div id="showedit" style="background-color:#EEEEEE;border: solid #455D8B;position:absolute;display:none;width:285px;height:130px;z-index:9">
			<div style="background-color:#455D8B;color:white;padding:2px;clear:both;"><strong>Caption</strong></div>	
			<div id="editcaption"></div>
		</div>
	    <script>
	    //<![CDATA[
	    	new Draggable('showedit',{});
	    //]]>	
	    </script>
				
				
				
