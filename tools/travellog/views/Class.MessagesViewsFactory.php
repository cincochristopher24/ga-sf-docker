<?php
	/*
	 * Class.MessagesViewFactory.php
	 * Created on Nov 12, 2007
	 * created by marc
	 */
	 
	 require_once("Class.Constants.php");
	 
	 class MessagesViewsFactory{
	 	
	 	static $instance = NULL;
	
		private $views = array();
		
		static function getInstance(){ 
			if( self::$instance == NULL ) self::$instance = new MessagesViewsFactory; 
			return self::$instance;
		}
	
		function createView( $view = constants::MESSAGE_LIST ){
			
			switch( $view ){
				
				case constants::MESSAGE_FORM:
					if( !array_key_exists("MessageForm", $this->views) ){
						require_once("travellog/views/Class.FrmMessage.php");
						$this->views["MessageForm"] = new FrmMessage;
					}
					return $this->views["MessageForm"];
					break;
					
				case constants::VIEW_MESSAGE:
					if( !array_key_exists("ViewMessage", $this->views) ){
						require_once("travellog/views/Class.ViewMessage.php");
						$this->views["ViewMessage"] = new ViewMessage;
					}
					return $this->views["ViewMessage"];
					break;
						
				case constants::MESSAGE_NAVIGATION:
					if( !array_key_exists("MessageNavigation", $this->views) ){
						require_once("travellog/views/Class.MessageNavigation.php");
						$this->views["MessageNavigation"] = new MessageNavigation;
					}
					return $this->views["MessageNavigation"];
					break;
					
				case constants::MESSAGE_PAGING:	
					if( !array_key_exists("MessagePaging", $this->views) ){
						require_once("travellog/views/travelers/Class.PagingListsView.php");
						$this->views["MessagePaging"] = new PagingListsView;
					}
					return $this->views["MessagePaging"];
					break;
				
				case constants::MESSAGE_ERRORS:	
					if( !array_key_exists("MessageErrors", $this->views) ){
						require_once("travellog/views/Class.ViewMessageErrors.php");
						$this->views["MessageErrors"] = new ViewMessageErrors;
					}
					return $this->views["MessageErrors"];
					break;
					
				case constants::MESSAGE_CONFIRMATION:
					if( !array_key_exists("MessageConfirmation", $this->views) ){
						require_once("travellog/views/Class.ViewMessageConfirmation.php");
						$this->views["MessageConfirmation"] = new ViewMessageConfirmation;
					}
					return $this->views["MessageConfirmation"];
					break;
				
				case constants::MESSAGE_ACTIONS:
					if( !array_key_exists("ViewMessageActions", $this->views) ){
						require_once("travellog/views/Class.ViewMessageActions.php");
						$this->views["ViewMessageActions"] = new ViewMessageActions;
					}
					return $this->views["ViewMessageActions"];
					break;	
					
				case constants::SPAM_LIST:	
					if( !array_key_exists("ViewSpamList", $this->views) ){
						require_once("travellog/views/admin/Class.ViewSpamList.php");
						$this->views["ViewSpamList"] = new ViewSpamList;
					}
					return $this->views["ViewSpamList"];
					break;
					
				default:
					if( !array_key_exists("ViewMessageList", $this->views) ){
						require_once("travellog/views/Class.ViewMessageList.php");
						$this->views["ViewMessageList"] = new ViewMessageList;
					}
					return $this->views["ViewMessageList"];				
			}
			
		}
	 
	 }
?>
