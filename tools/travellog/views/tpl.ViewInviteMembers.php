<?php
/**
 * Created on Apr.17.07
 *
 * @author Daphne 
 * Purpose:  template for View Search / Invite Members Panel (Search, Invite)
 */
  
?>

<div class="wrap">
<? if (count($invites)) : ?>
	
	<h1>Invitation(s) to join group were sent to the following GoAbroad Network Travelers: </h1>
	<? foreach($invites as $each) : ?>
		<? $eachprofile = $each->getTravelerProfile() ?>
		<div class="traveler pending">
			<a href="/<?=$eachprofile->getUserName()?>" title="View Profile" class="user_thumb">
				<img src="<?=$eachprofile->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" height="37" width="37" />
			</a>
			<h2><a href="/<?=$eachprofile->getUserName()?>"><?=$eachprofile->getUserName()?></a></h2>
			<div class="profile_info meta"> 
		  		<div class="mem_status">(pending member approval)</div>
				<div class="mem_option"></div>
		  	</div>
		</div>	
	<? endforeach; ?>	
	<? //$flag = GroupMembersPanel::$ALLMEMBERS ?>
	<div class="clear"></div>
	
<? endif; ?>

<? if (count($emailInvites)) : ?>
	
		<h1>Invitations to join group were sent to the following emails: </h1>
		<? foreach($emailInvites as $each) : ?>
			<?= $each ?>	
			<br />	
		<? endforeach; ?>
		<? //$flag = GroupMembersPanel::$PENDINGEMAILINVITES ?>

<? endif; ?>

</div>
<div class="clear"></div>
<!--<input type="hidden" value="<?=$flag?>" id="hidflag">-->