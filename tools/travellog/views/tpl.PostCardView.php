<?php
/**
 * PLEASE OBSERVE PROPER INDENTATION WHEN MODIFYING THIS TEMPLATE
 */
?>

<?php
	$props        = $contents['props'];
	$title        = $contents['obj_entry']->getTitle();
	$callout      = $contents['obj_entry']->getCallout();
	$obj_location = $contents['obj_entry']->getTrip()->getLocation();
	$date         = $contents['obj_entry']->getTrip()->getArrival();
	$photo_id     = $contents['obj_entry']->getPrimaryPhoto()->getPhotoID();
	$arr_photos   = ( is_array($contents['photos']) )? $contents['photos']: array();
	$photo        = '';
	if( $photo_id )
		$photo    = $contents['obj_entry']->getPrimaryPhoto()->getPhotoLink('jentryheader');
	if( get_class($obj_location) == 'Country' ){
		$country_id = $obj_location->getCountryID();
		$country    = $obj_location->getName();
		$city       = '';
	}
	else{
		$country_id = $obj_location->getCountry()->getCountryID();
		$country    = $obj_location->getCountry()->getName();
		$city       = ( $obj_location->getName() != $country )? ' ' . $obj_location->getName() : '';
	}
?>

<?php echo $contents['obj_profile_view']->render()?>
<?php $contents['obj_navigation']->show()?>
<form method="post" action="/postcards.php?action=send" onsubmit="disable_buttons()">
	<div id="content_wrapper" class="control_divider">
	
		<div class="send_postcard_header">
			<h1>Send Postcards</h1>
			<p>
				Make an e-postcard out of this journal entry! Pick a photo, write a message, then we'll send it along to your family and friends.
			</p>
		</div>	
		
				
		<div class="postcard_title"> 
			
			<h1 class="postcard_header">
				To: <span>
					(separate email addresses with a comma, e.g. mom@aol.com, dad@verizon.net) <br/>
					We will send the postcard individually to protect your recipient list.
					</span>
			</h1>
				
			<div class="left_box">			
				<textarea id="mytextarea" name="to" autocomplete="off" class="text" style="width:385px;height:140px;margin:0;"/><?php echo $props['to']?></textarea>
			</div>
			
			
			<div class="right_box">
				<?php if( count($contents['col_address_book'])):?>		
					<div class="recipient_list" id="adbook_list">
						<strong class="jLeft"> Address Book </strong>		
						
						<?php if(count($contents['col_address_book']) > 1) : ?>		
							<div class="jRight"> 
								<input type="checkbox" name="#" onclick="toggleCheckbox(this.checked, 'adbook_item')" class="jLeft"/>
								<strong style="padding: 3px 0; color:#666;line-height:normal;" class="jLeft"> Check All </strong>
							</div>
						<?php endif; ?>										
						
						<ul>						
							<?php foreach( $contents['col_address_book'] as $obj_addressbook ):?>
								<li>
									<span width="10px">
										<input class="adbook_item" type="checkbox" id="addresses" name="addresses[]" value="<?php echo $obj_addressbook->getName() . '^' .$obj_addressbook->getEmailAddress()?>" />
									</span>
									<span>
										<?php echo $obj_addressbook->getName()?>
									</span>
								</li>
							<?php endforeach;?>				
							
						</ul>
						
					</div>			
				<?php endif;?>
				<?php if( count($contents['col_friends'])):?>	
					<div class="recipient_list" id="friends_list">
						<strong class="jLeft"> Friends </strong>		

						<?php if(count($contents['col_friends']) > 1) : ?>				
							<div class="jRight"> 
								
									<input type="checkbox" name="#" onclick="toggleCheckbox(this.checked, 'friend_item')" class="jLeft"/>
									<strong style="padding: 3px 0; color:#666;line-height:normal;" class="jLeft"> Check All </strong>
								
							</div>
						<?php endif; ?>																
						<ul>	
							<?php foreach( $contents['col_friends'] as $obj_traveler ):?>					
								<li>
									<span width="10px">
										<input class="friend_item" type="checkbox" id="addresses" name="addresses[]" value="<?php echo $obj_traveler->getUsername() . '^' .$obj_traveler->getEmail()?>" />
									</span>
									<span>
										<?php echo $obj_traveler->getUsername()?>
									</span>
								</li>
							<?php endforeach;?>			
							
						</ul>
					</div>			
				<?php endif;?>
			</div>			
		</div>

		<div id="wide_column">
		    		<?php if($photo_id):?>
    				        <img id="primaryheader_photo" class="header_photo" src="<?php echo $photo;?>" border="0"/>
    				<?php endif;?>
					<div>		
				    	<div class="postcardhelp">
							<h2>Note</h2> 
							<p>The edits you make here will be applied to your journal entry and not just this postcard.</p>
						</div>
				    
    					<h1>
    						<div id="ENTRYTITLE"><?php echo $title?></div>
    					</h1>
    					<a href="javascript:void(0)"id="EntryTitleLink" class="entry_links bottom_shift"><span>Edit Entry Title</span></a>
    					<p id="popup-entry-location">                                                      
   						    <img src="http://images.goabroad.com/images/flags/flag<?php echo $country_id?>.gif" title="<?php echo $country?>" alt="<?php echo $country?>" id="journal_entry_flag" />
    						<span style="display:inline;"><?php if( strlen(trim($city)) ) echo $city . ', '?><?php echo $country?> | </span>
    						<span style="display:inline;" class="date"><?php echo date('M d, Y', strtotime($date))?></span>
    					</p>
    					<a class="entry_links bottom_shift" id="edit-entry-location" href="javascript:void(0)"><span>Edit Entry Location</span></a>
    					<p class="entrycallout">&ldquo;<span id="ENTRYCALLOUTCONTENT"><?php echo $callout;?></span>&rdquo;</p>
    					<a href="javascript:void(0)"id="EntryCalloutLink" class="entry_links bottom_shift"><span>Edit Entry Callout</span></a>
    				</div>
		
			<div class="content1 postcardbox clear">	
				<div class="postcardhelp">
					<h2>Write your postcard message</h2>
					<p>If you typed more than one recipient email, they will all get the same message. We will send the postcard individually to protect your recipient list. Maximum of 255 words only.</p>
				</div>   
				<h1 class="postcard_header">Message:</h1>
				<textarea
							id			= "limitedtextarea"
							name		= "message"
							class		= "text"
							onKeyup		= "countWords('limitedtextarea', 'countdown', 255)"
							onKeydown	= "countWords('limitedtextarea', 'countdown', 255)"
							style		= "width: 385px; height: 200px; margin: 0; overflow-y: auto"><?=$props['message']?></textarea>
				<div style = "width: 395px; padding-bottom: 40px">
					<span style = "float: right">
						<input type = "textbox" id = "countdown" readonly = "yes" size = "3" value = "255" /> word(s) left
					</span>
				</div>
				<div class="postcard_control">
	<input type="submit" class="submit highlight button_v3" id="action" value="Send" name="action" />
							<input type="button" class="button_v3" name="goback" id="goback" value="Go back to journal entry" onclick="CustomPopup.initialize('Go Back to Journal Entry?','Are you sure you don\'t want to continue sending your postcard?','/journal-entry.php?action=view&travellogID=<?php echo $props['travellogID']?>','Go Back');CustomPopup.createPopup();" />
							<!--a href="/journal-entry.php?action=view&travellogID=<?php echo $props['travellogID']?>"><span>Go back to journal entry</span></a-->
				</div>		
			</div>
		</div>
		<div id="narrow_column">
			<?php if( count($arr_photos) ):?>
				<div id="postcard_images">
					<ul id="mycarousel" class="jcarousel jcarousel-skin-tango">
			        	<?php 
			        		$i = 1;
			        		foreach($arr_photos as $obj_photo):
			        	?>
		      					<li>
		      						<a href="javascript:void(0)" onclick="changePhoto('<?php echo $obj_photo->getPhotoLink('jentryheader')?>')">
		      							<img src="<?php echo $obj_photo->getPhotoLink('default')?>" alt="" />
		      						</a>
								</li>
						<?php 
								$i++;
							endforeach;
						?> 
					</ul>
				</div>
			<?php endif;?>
 	 	</div>
	</div>
  	<input type="hidden" name="callout"     id="NEWENTRYCALLOUTCONTENT" value="<?php echo $callout?>"  />
  	<input type="hidden" name="title"       id="NEWENTRYTITLE"   value="<?php echo $title?>"    />
  	<input type="hidden" name="travellogID" value="<?php echo $props['travellogID']?>"  /> 
  	<input type="hidden" name="photolink"   id="photolink" value="<?php echo $photo;?>" /> 
</form>
<script type="text/javascript">
//<![CDATA[
	var changePhoto = function(photo){
		document.getElementById('primaryheader_photo').src = photo;
		document.getElementById('photolink').value         = photo;   
	}
	
	function cancel_postcard(url){
		if( confirm('Are you sure you don\'t want to continue sending your postcard?') ){
			window.location.href = url;
		}
	}
	
	function disable_buttons(){
		jQuery('input[type="submit"]').attr('disabled', 'true');
		jQuery('input[type="button"]').attr('disabled', 'true'); 
	}
	
	function toggleCheckbox(b, cls){
		var ar = jQuery('.' + cls);
		for( x = 0; x<ar.length; ++x){
			ar[x].checked = b;
		}
	}

	var holdText = '';
	var disabledBox = false;

	function countWords(_txaMessage, _txtCount, _maximum){
		var text1 = jQuery('#'+_txaMessage).val();
		var numOfWords = doCount(text1);

		if (numOfWords <= _maximum)
			holdText = text1;

		if(numOfWords > _maximum){
			disabledBox = true;
			reset(_txaMessage, _maximum);
			jQuery('#'+_txtCount).val(0);
		}
		else{
			disabledBox = false;
			jQuery('#'+_txtCount).val(_maximum - numOfWords);
		}
	}

	function doCount(_textParam){
		if (0 < _textParam.length){
			var iwhitespace = /^[^A-Za-z0-9]+/gi;							// remove initial whitespace
	        var left_trimmedStr = _textParam.replace(iwhitespace, "");
	        var na = rExp = /[^A-Za-z0-9]+/gi;								// non alphanumeric characters
	        var cleanedStr = left_trimmedStr.replace(na, " ");
	        var splitString = cleanedStr.split(" ");

			return splitString.length;
		}
		else
			return 0;
	}

	function reset(_txaMessage, _maximum){
		if(disabledBox){
			var currText = $(_txaMessage).value;
			var newLength = doCount(currText);

			//prevent user from adding words, but not taking them away
			if(newLength > _maximum)
				$(_txaMessage).value = holdText;
		}
	}
//]]> 
</script>
<span id="popup-location" style="display:none"></span>
