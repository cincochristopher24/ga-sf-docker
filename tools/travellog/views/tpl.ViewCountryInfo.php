<?
	/** 
	* tpl.ViewCountryInfo.php
	* @author marc
	* aug 2006
	*/
?>


<div id="content_wrapper">
	<div id="wide_column">
		<div class="section" id="profile">
			<h2><span>Country Info</span></h2>
				<div class="content">
					<div id="profile_container">
						<h1><img src="http://images.goabroad.com/images/flags/flag<?= $contents["country"]->getCountryID(); ?>.gif" alt="<?= $contents["country"]->getName() . ' Flag'; ?>" width="46" height="23" /> <?= $contents["country"]->getName(); ?></h1>
						
						<ul class="profile">
							<li></li>
							<?php if( strlen($contents["country"]->getCountryInfo()->getCountryDescription()) > 0 ): ?>
								<li><strong>Description:</strong> <?= $contents["country"]->getCountryInfo()->getCountryDescription();  ?></li>
							<?php endif; ?>	
							<?php if( strlen($contents["country"]->getCountryInfo()->getCapital()) > 0 ): ?>
								<li><strong>Capital:</strong> <?=  $contents["country"]->getCountryInfo()->getCapital();  ?></li>
							<?php endif; ?>
							<?php if( strlen($contents["country"]->getCountryInfo()->getCurrency()) > 0 ): ?>	
								<li><strong>Currency:</strong> <?= $contents["country"]->getCountryInfo()->getCurrency();  ?></li>
							<?php endif; ?>
							<?php if( strlen($contents["country"]->getCountryInfo()->getPopulation()) > 0 ): ?>
								<li><strong>Population:</strong> <?= $contents["country"]->getCountryInfo()->getPopulation();  ?></li>
							<?php endif; ?>
							<?php if( strlen($contents["country"]->getCountryInfo()->getLandArea()) > 0 ): ?>
								<li><strong>Land Area:</strong> <?= $contents["country"]->getCountryInfo()->getLandArea();  ?></li>
							<?php endif; ?>
						</ul>											
					</div>	
				</div>
				<div class="foot"></div>
		</div>
		<div id="latest_journal" class="section journal">
			<?= $sub_views['JOURNALS_IN_COUNTRY']->render(); ?>
		</div>
	</div>
	<div id="narrow_column">
		<div class="section map">
			<h2><span>Map</span></h2>
				<div class="content">
					<?
						$contents["map"]->addMapFrame($contents["mFrame"]); 
						$contents["map"]->plotLocations($contents["arrLocationMarker"]); 
					?>
				</div>
				<div class="foot"></div>	
		</div>
		<div id="featured_photos" class="section photos">
			<?php
				if ( count($contents["Photos"]) ):
					echo "<h2><span>Photo";
							if( 1 < count($contents["Photos"]) )
								echo "s";
							echo "</span></h2>"; 
			?>
			<div class="content">					
			<?php						
				$photoCtr = 0;
				foreach($contents["Photos"] as $Photo): 
			?>
			
				<div class="photo">
					<a href="../photomanagement.php?cat=location&action=vfullsize&genID=<?= $contents["country"]->getLocationID(); ?>&photoID=<?= $Photo->getPhotoID() ?>"><img src=<?= "\"" . $Photo->getThumbnailPhotoLink() . "\""; ?>></a>
					<div class="caption">
					<? if ( strlen(trim($Photo->getCaption())))
						echo $Photo->getCaption()
					?>	
					</div>
					<p>Photo by</p> <a href="<?= $contents["PhotoLogs"][$photoCtr]->getTraveler()->getTravelerProfile()->getUsername(); ?>" ><?= $contents["PhotoLogs"][$photoCtr]->getTraveler()->getUsername(); ?></a><br />
					<a href=<?= "\"../travel.php?action=view&travelID=" . $contents["PhotoLogs"][$photoCtr]->getTravel()->getTravelID() . "&travelerID=" . $contents["PhotoLogs"][$photoCtr]->getTraveler()->getTravelerID() . "\"" ?> ><?= $contents["PhotoLogs"][$photoCtr]->getTravel()->getTitle(); ?></a>
				</div>
				
					<?php $photoCtr++; endforeach; ?>
					<div class="clear"></div>
					<? if ( 2 < $contents["PhotoCount"] ) :?>
							<div class="section_foot">
								<a href='../photomanagement.php?cat=location&action=view&genID=<?= $contents["country"]->getLocationID(); ?>'>view all <?= $contents["country"]->getName(); ?> photos</a>
							</div>
					<? endif; ?>
							
				<?endif; ?>
			</div>	
			<div class="foot"></div>
		</div>	
				
			<div class="section" id="travelers">
				<? if ( 0 < $contents["TravelerCount"] ): ?>
					<h2><span>Traveler<? if( 1 < $contents["TravelerCount"] ) : ?>s<? endif; ?> in <?= $contents["country"]->getName(); ?></span></h2> 
					<div class="content travelers">
						<?= $sub_views['TRAVELERS_CURRENTLY_IN_COUNTRY']->render(); ?>
						<? if( 3 < $contents["TravelerCount"] ) : ?>
							<div class="section_foot">
								<a href="travelers.php?action=current&locationID=<?= $contents["country"]->getLocationID(); ?>">view all travelers in <?= $contents["country"]->getName(); ?></a>
							</div>		
						<? endif; ?>
					</div>	
					<div class="foot"></div>		
			   <? endif; ?>
			</div>
				  
			<div id="travelers_living" class="section">   
			<? if ( 0 < $contents["TravelersLivingCount"] ): ?>
					<h2><span>Traveler<? if( 1 < $contents["TravelersLivingCount"] ) : ?>s<? endif; ?> Living in <?= $contents["country"]->getName(); ?></span></h2> 
						<div class="content travelers">
							<?= $sub_views['TRAVELERS_LIVING_IN_COUNTRY']->render(); ?>
							<? if( 3 < $contents["TravelersLivingCount"] ) : ?>
								<div class="section_foot">
									<a href="travelers.php?action=living&locationID=<?= $contents["country"]->getLocationID(); ?>">view all travelers living in <?= $contents["country"]->getName(); ?></a>
								</div>
							<? endif; ?>
						</div>
						<div class="foot"></div>	
			 <?  endif; ?>
		</div>
			<div class="clear"></div>
	
	</div>
	</div>
	<div class="clear"></div>
</div>



		