<div id="journals_summary" class="section traveler_context" >	
	<a name="anchor"></a>
	<h2><span><?=$compTitle?></span>
		<span id="linkicon">
			<a href="/faq.php#bring_2_facebook"><span>Facebook</span></a>
		</span>
		<?/*php if( count($journals) ):?>
			<span class="header_actions">
				&nbsp;|&nbsp;<a href="/journal_draft/view_saved_drafts">View All Drafts</a>
			</span> 
		<?php endif; ?>	
		<?php if( count($journals) ):?>
			<span class="header_actions">
				<a href="/journal.php?action=myjournals&amp;travelerID=<?=$subjectID?>">View All Journals</a>
			</span>
		<?php endif; */?>
	</h2>
	<?
		$country = '';
		$city = '';
		$jeCtr = 1;					// journal entry counter used for identifying yahoo carousel object
		$carJsCode = '';			// yahoo carousel code
	?>
	<div id="mytraveljournal">		
		<?foreach($journals as $journal):
			$jID = $journal->getTravelID();
			$jeCount = count($journalsAndEntries[$jID]);
			$jEntriesAr = $journalsAndEntries[$jID];
			$updates = $journalsAndUpdates[$jID];
			if ($journal->getOwner() == null){
				echo 'travelid:' . $journal->getTravelID();
				continue;
			}
			
			$unfinished = JournalEntryDraftPeer::retrieveUnfinishedEntriesByTravelIDCount($jID);
			$drafts = JournalEntryDraftPeer::retrieveDraftsByTravelIDCount($jID);
							
			if (isset($jEntriesAr[$jeCount-1])){
				$trip = $jEntriesAr[$jeCount-1]->getTrip();
				try{
					$location = $trip->getLocation();
					if(is_object($location)){
						$country = $location->getCountry()->getName();
						$city = $location->getName();
					}
				}catch(exception $e){
				}
			} 
		?>
		<?if( 0 < count($jEntriesAr) || $canAddJournal):?>
			<div class="journalgroup">
				<div class="header">
					<?if (isset($jEntriesAr[$jeCount-1])): ?>									
						<h3><a href="/journal-entry.php?action=view&amp;travellogID=<?=$jEntriesAr[$jeCount-1]->getTravelLogID()?>" title="<?=$journal->getTitle()?>"><?=$journal->getTitle()?></a></h3>
					<?else:?>
						<h3><?=$journal->getTitle()?></h3>
					<?endif;?>
				</div>

				<?if(count($jEntriesAr) == 0):?>
					<div class="help_text">
						<? if($unfinished > 0 && $drafts > 0 && $canAddJournal): ?>
							<span>This journal has 
								<a href="/journal_draft/view_saved_drafts" ><?=$drafts?> draft<?if($drafts > 1):?>s<?endif;?></a> and 
								<a href="/journal_draft/view_saved_drafts" ><?=$unfinished?> auto-saved unfinished entr<?if($unfinished > 1):?>ies<?else:?>y<?endif;?></a>.
							</span><br />
						<? elseif($unfinished == 0 && $drafts > 0 && $canAddJournal): ?>
							<span>This journal has <a href="/journal_draft/view_saved_drafts" ><?=$drafts?> draft<?if($drafts > 1):?>s<?endif;?></a>.</span><br />
						<? elseif($unfinished > 0 && $drafts == 0 && $canAddJournal): ?>
							<span>This journal has <a href="/journal_draft/view_saved_drafts" ><?=$unfinished?> auto-saved unfinished entr<?if($unfinished > 1):?>ies<?else:?>y<?endif;?></a>.</span><br />
						<? else: ?>
							<span>There are no entries for this journal.</span><br />
						<? endif; ?>
						Document great travel moments and share them with family and friends! A journal book is a set of journal entries about the same trip. You may write about your experiences and add your travel photos to your entries or to a photo album.

					</div>
					<?if($canAddJournal):?>
						<p  class="action_links" id="controls<?=$journal->getTravelID()?>">
							<a href="/journal-entry.php?action=add&amp;travelID=<?=$journal->getTravelID()?>">Add an Entry</a>
							<?php if(!$journal->hasJournalEntryDrafts()): ?>
							 | <a href="javascript:void(0)" onclick="CustomPopup.initialize('Delete Journal ?','Are you sure you want to delete this journal?','/travel.php?action=delete&amp;travelID=<?=$journal->getTravelID() ?>','Delete','1','All entries, photos and other traveler comments linked to this journal will also be deleted. This action cannot be undone.');CustomPopup.createPopup();">Delete</a>
							<?php endif; ?>
						</p>
					<?endif;?>	
				<?else:?>
					<? if($unfinished > 0 && $drafts > 0 && $canAddJournal): ?>
						<span>This journal has <?=$jeCount?> <?if($jeCount > 1):?>entries<?else:?>entry<?endif;?>, 
							<a href="/journal_draft/view_saved_drafts" ><?=$drafts?> draft<?if($drafts > 1):?>s<?endif;?></a> and 
							<a href="/journal_draft/view_saved_drafts" ><?=$unfinished?> auto-saved unfinished entr<?if($unfinished > 1):?>ies<?else:?>y<?endif;?></a>.
						</span><br />
					<? elseif($unfinished == 0 && $drafts > 0 && $canAddJournal): ?>
						<span>This journal has <?=$jeCount?> <?if($jeCount > 1):?>entries<?else:?>entry<?endif;?> and <a href="/journal_draft/view_saved_drafts" ><?=$drafts?> draft<?if($drafts > 1):?>s<?endif;?></a>.</span><br />
					<? elseif($unfinished > 0 && $drafts == 0 && $canAddJournal): ?>
						<span>This journal has <?=$jeCount?> <?if($jeCount > 1):?>entries<?else:?>entry<?endif;?> and <a href="/journal_draft/view_saved_drafts" ><?=$unfinished?> auto-saved unfinished entr<?if($unfinished > 1):?>ies<?else:?>y<?endif;?></a>.</span><br />
					<? else: ?>
						<span>
							This journal has <?=$jeCount?> <?if($jeCount > 1):?>entries<?else:?>entry<?endif;?>.
						</span>
					<? endif; ?>						
					<p><?= HtmlHelpers::truncateWord($journal->getDescription(),'100', '...') ?></p>
					<?if($canAddJournal):?>
						<p  class="action_links" id="controls<?=$jID?>">
							<a href="/journal-entry.php?action=add&amp;travelID=<?=$jID?>">Add an Entry</a>
						</p>
					<?endif;?>
				<?endif;?>				
				<ul>
					<?foreach($jEntriesAr as $entry):?>
					<li class="list">
						<?if($entry->getPrimaryPhoto()->getPrimaryPhoto() > 0):?>
							<img src="<?=$entry->getPrimaryPhoto()->getPhotoLink('featured')?>" border="0" width="<?=$entry->getPrimaryPhoto()->getAdjustedThumbnailWidth()?>" height="<?=$entry->getPrimaryPhoto()->getAdjustedThumbnailHeight()?>">
						<?endif;?>	
						<h4>
							<a href="/journal-entry.php?action=view&amp;travellogID=<?=$entry->getTravelLogID()?>">	
							<!-- trimmed by lance -->
							<?=(70 < strlen($entry->getTitle()))? substr($entry->getTitle(), 0, 70).'..': $entry->getTitle()?>
							</a>
							<? if(isset($updates['LAST_ENTRY_UPDATED']) && ($updates['LAST_ENTRY_UPDATED']->getTravelLogID() == $entry->getTravelLogID())): ?>
								<img src="/images/v3/update_badge.gif" border="0" class="new">
							<? endif; ?>	
							<? if(isset($updates['RECENTLY_ADDED_ENTRY']) && ($updates['RECENTLY_ADDED_ENTRY']->getTravelLogID() == $entry->getTravelLogID())): ?>
								<img src="/images/v3/new_badge2.gif" border="0" class="new"></h4>
							<? endif; ?>
						</h4>
						
						<p class="meta">
							<?php
								$ecountry = "";
								$ecity = "";
								try{
									$location = $entry->getTrip()->getLocation();
									if(is_object($location)){
										$ecountry = $location->getCountry()->getName();
										$ecity    = $location->getName();
									}
								}catch(Exception $_e){
								}
							?>
							<?= ( strtolower($ecity) == strtolower($ecountry) )? $ecountry : $ecity . ', ' . $ecountry ?>
							&nbsp;
							<?= (strtotime($entry->getTrip()->getArrival())) ? '|&nbsp;'.date('D M d, Y',strtotime($entry->getTrip()->getArrival())) : '' ?><br />
						</p>
						<p class="meta">
							<? 
								// added by neri to display also the number of videos and photos for each journal entry in the profile page: 12-15-08
									
								$cntPhotos = count($entry->getPhotos());
								$cntVideos = count($entry->getTravelLogVideos());
								
								if (0 < $cntPhotos) {
									echo $cntPhotos.' Photo';
									
									if (1 < $cntPhotos)
										echo 's';
								}	
							?>
							
							<? if (0 < $cntPhotos && 0 < $cntVideos): ?> | <? endif; ?>
							
							<? if (0 < $cntVideos) {
									echo $cntVideos.' Video';
									
									if (1 < $cntVideos)
										echo 's';
							   }
							?>	
						</p>
					</li>
					<?endforeach;?>					
				</ul>				
			</div>
		<? endif; ?>
		<?endforeach;?>
		
		<?if (count($journals) == 0){
			 echo '<div class="help_text">' . $noJournalMessage  . '</div>';						
		}?>
	</div>
	<? if (isset($pagingComp)):?>	
	    	<? $pagingComp->showPagination() ?>
	<? endif; ?>
	  <?if ($canAddJournal):?>
      	<ul class="actions">        	
      		<li><a class="button" href="<?=$addJournalBookLink?>">+ Add a new Journal</a></li>
      		<li><a class="button" href="/journal_draft/view_saved_drafts">View All Drafts</a></li>
      	</ul>
      <?endif;?>	
</div>