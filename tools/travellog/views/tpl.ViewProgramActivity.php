<?php
$file_factory = FileFactory::getInstance();
$file_factory->getClass('GaDateTime');
?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" id="it">
	<?if( $props['is_owner'] ):?>
		<tr>
			<td colspan="<?=$props['is_owner'] ? 4 : 3;?>" class="top_header" valign="center">
				<h2>Manage Activities</h2>
				<a href="javascript:void(0)" onclick="jQuery().addForm('action=Create&pID=<?=$props['pID']?>&context=0');"  class="add"><strong>Add Activity</strong></a>
				<div class="clear"></div>
			</td>
		</tr>
	<?endif;?>
	<?if( count($col_activities) ):?>
	<tr>
		<th width="150">Date / Time</th>
		<th>Activity</th>
		<th>Description</th>
		<?if( $props['is_owner'] ):?>
			<th width="80" align="center">Controls</th>
		<?endif;?>
	</tr>
	<?endif;?> 
	<?if( count($col_activities) ): 
		foreach( $col_activities as $obj_activity ):?>
			<tr>
				<td valign="center"><?= GaDateTime::friendlyFormat($obj_activity->getTheDate()) ?></td>
				<td valign="center"><strong><?= $obj_activity->getTitle() ?></strong></td>
				<td valign="center"><?=nl2br($obj_activity->getDescription())?></td>
				<?if( $props['is_owner'] ):?>
					<td valign="center" align="center">
						<a href="javascript:void(0)" onclick="jQuery().addForm('action=Edit&eID=<?=$obj_activity->getActivityID()?>&context=0');">Edit</a>&nbsp;|&nbsp;
						<a href="javascript:void(0)" onclick="jQuery().deleteEvent('action=Delete&eID=<?=$obj_activity->getActivityID()?>&context=0')">Delete
					</td>
				<? endif; ?>
			</tr>
	<?  endforeach;
	  endif;?>
</table>
