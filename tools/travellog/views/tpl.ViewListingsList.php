<?php
	/*
	 * tpl.ViewListingsList.php
	 * Created on Dec 13, 2007
	 * created by marc
	 */
?>
<div id="admin_messages">
	<p>View : <b>Listing</b> | <a onclick="javascript: SLController.changeView('viewByStudent');">Student</a></p>
	<?= $sub_views['PAGING']->render(); ?> 
	<? 
		foreach( $contents['listListingID'] as $listingID ) :
	    	$nListing = new Listing($listingID);  
	?>
			<div class="fl_left header_mess" id="msgPack_<?= $listingID; ?>">
				<? if( $contents['inquiryBox']->getNumberOfNewMessagesPerListing($listingID) > 0 ) : ?> 
					&nbsp;<?= $contents['inquiryBox']->getNumberOfNewMessagesPerListing($listingID); ?>&nbsp;
				<? endif; ?>
			</div>	
			<div class="stretcher header_mess title"><?= $nListing->getListingName(); ?></div>
			<div class="msgPack">
			  	<? 
					foreach($contents['inquiryBox']->getStudents($listingID) as $student) : 
						$objStud = new Traveler($student);
			  	?>	
			  			<div class="fl_left header_mess" id="grpMsg_<?= $listingID; ?>_<?= $objStud->getSendableID(); ?>">
				  			<? if( $contents['inquiryBox']->getNumberOfNewAdminMessagesPerStudentAndListing($objStud->getSendableID(),$listingID) > 0 ) : ?> 
				  				&nbsp;<?= $contents['inquiryBox']->getNumberOfNewAdminMessagesPerStudentAndListing($objStud->getSendableID(),$listingID); ?> &nbsp;
				  			<? endif; ?>
						</div>
			  			<div class="header_mess" onclick="javascript: SLController.displayMessages('all_msg_<?= $objStud->getTravelerID(); ?>_<?= $listingID; ?>',<?= $listingID; ?>,<?= $student; ?>,'listView');"><?= $objStud->getTravelerProfile()->getLastName(); ?>, <?= $objStud->getTravelerProfile()->getFirstName(); ?></div>
			  			<div class="display" id="all_msg_<?= $objStud->getTravelerID(); ?>_<?= $listingID; ?>" style="display: none">
						</div>
				<? endforeach; ?>
			</div> 
		<? endforeach; ?>					
	<?= $sub_views['PAGING']->render(); ?>

</div>
