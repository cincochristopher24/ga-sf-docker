<?php
/*
 * Created on Aug 10, 2006
 */
?>

<div id="inquiries" class="section">
	<h2><span>Free Online Advising</span></h2>
	
	<div class="content">
		<!-- edited by marc, dec 22 -->
		<? if( count($arrReferredListings) ): ?>
	 		<ul>
	 			<? foreach( $arrReferredListings as $nListing ) : ?>
	 					<li>
	 						<strong><?= $nListing["listingname"] ?></strong><br />
	 						Listing ID: <?= $nListing["listingID"]?><br />
	 						<?= $nListing["instname"]; ?>
	 					</li>
	 			<? endforeach; ?>
	 		</ul>
 		
 			<div class="controls">
				<?= $view_inquiries_link ?>
			</div>
	 
	 	<?php else:  ?>
		
			<p class="help_text">
				 <?=$helpText?>				 
			</p>
			
		<?php endif; ?>	
		
		
		
		<div class="actions">
			<?if (isset($profile_link)) :?>
				<?=$profile_link?>
			<? endif; ?>
		</div>
	</div>
	
	<div class="foot"></div>
</div>
