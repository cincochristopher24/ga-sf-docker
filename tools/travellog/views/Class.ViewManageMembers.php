<?php
	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.Condition.php';
	require_once 'travellog/model/Class.FilterCriteria2.php';
	
	class ViewManageMembers{

		private $mGroup		= null;
		private $mObjTraveler	= null;
		private $mAction	= 'view';
		private $mArrTravSubGroupList = array();
		private $mArrAssignedTravSubGroupList = array();
		
		function setGroup($_group){
			$this->mGroup = $_group;
		}
		
		function setTraveler($_objTraveler){
			$this->mObjTraveler = $_objTraveler;
		}
		
		function setAction($_action){
			$this->mAction = $_action;
		}
		
		function setArrTravSubGroupList($_arrTravSubGroupList){
			$this->mArrTravSubGroupList = $_arrTravSubGroupList;
		}
		
		function setArrAssignedTravSubGroupList($_arrAssignedTravSubGroupList){
			$this->mArrAssignedTravSubGroupList = $_arrAssignedTravSubGroupList;
		}
		
		function render(){
			
			switch($this->mAction){
				
				case 'showGroups':

					$this->showSubGroups();

					break;

				case 'save':

					$this->manageSubGroups();

					break;

				default:
				
					$this->manageSubGroups();
					
			}
		}
		
		function manageSubGroups(){
		
			if (count($this->mArrTravSubGroupList) > 0) :
				echo (count($this->mArrTravSubGroupList) > 1 ) ? 'Groups: ' : 'Group: ' ;
				echo implode(", ",$this->mArrTravSubGroupList);											
			endif;
			
			if (count($this->mArrAssignedTravSubGroupList) > 0) :
				echo (count($this->mArrAssignedTravSubGroupList) > 1 ) ? 'Group Invites: ' : 'Group Invite: ' ;
				echo implode(", ",$this->mArrAssignedTravSubGroupList);											
			endif;
			
		}
		
		function showSubGroups(){

			$condorder = new Condition;
			$condorder->setAttributeName("name");

			$order = new FilterCriteria2();
			$order->addCondition($condorder);

			$tpl = new Template();
			$tpl->set("arrSubGroup",	$this->mGroup->getSubGroups(null, $order));
			$tpl->set("member",	$this->mObjTraveler);
			$tpl->set("travelerType", $this->mGroup->isMember($this->mObjTraveler) ? 'member' : 'invite');

			$tpl->out('travellog/views/tpl.ViewSubGroupList.php');

		}
		
		function renderPopUp(){
			$tpl = new Template();

			$tpl->out('travellog/views/tpl.ViewAddImportantLink.php');
		}
	}
?>