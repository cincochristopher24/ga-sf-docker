<div class="layout_2" id="content_wrapper">
	<div id="wide_column">
		<div id="groups_and_clubs" class="section">
			<h1 class="big_header"><span><?= (0 < $group->getCustomGroupSection()->getCustomGroupSectionID() ) ? $group->getCustomGroupSection()->getSectionLabel() : 'Projects and Programs' ?></span></h1>
			<div class="content" id="formGroupsContent" align="center">
				<form action="customgroupsection.php" method="POST">
					<strong>Edit Label:</strong>
					<input type="textbox" name="txtSectionLabel" size="30" value="<?= (0 <  $group->getCustomGroupSection()->getCustomGroupSectionID() ) ? $group->getCustomGroupSection()->getSectionLabel() : 'Projects and Programs' ?>">
					<input type="hidden" name="gID" value="<?=$group->getGroupID()?>">
					<input type="hidden" name="action" value="save">
					<input type="submit" name="submit" value="Save">
				</form>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
