<?php
  /**
   * @(#) Class.ViewFeaturedSubGroups.php
   * Handles the viewing of the featured subgroups.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 12 3, 08
   */
  
	require_once 'Class.Template.php';
	
	class ViewFeaturedSubGroups {
		private $mTemplate = "tpl.IncViewModeFeaturedSubGroupList.php";
		private $mAlertMsg = '';
		private $mMode = 'view';
		private $mFeaturedGroups = NULL;
		private $mParentGroup = NULL;
		private $mTabAction = "showActive";
		private $mPage = 1;
		private $mCategory = "";
		private $mCategories = null;
    
    /**
     * Sets the categories of the current admin group.
     * 
     * @param SqlResourceIterator $categories The categories of the current admin group.
     * @return void
     */
    function setCategories($categories){
    	$this->mCategories = $categories;
    }    
    
    /**
     * Sets the parent group of the featured groups.
     * 
     * @param AdminGroup $group
     * @return void
     */
    function setParentGroup($group){
    	$this->mParentGroup = $group;
    }
    
    /**
     * Sets the current tab action.
     * 
     * @param string $action values are inactive and active only
     * @return void
     */
    function setTabAction($action){
    	$this->mTabAction = $action;
    }
    
    /**
     * Sets the current page of the subgroup list.
     * 
     * @param integer $page
     * @return void
     */
    function setPage($page){
    	$this->mPage = $page;
    }
    
		/**
		 * Sets the featured subgroups to be shown.
		 * 
		 * @param array $groupList
		 * @return void
		 */
		function setSubGroupList($groupList){
			$this->mFeaturedGroups = $groupList;
		}
		
		/**
		 * Sets the alert message to be shown in the featured groups section.
		 * 
		 * @param string $_alertMsg
		 * @return void
		 */
		function setAlertMessage($_alertMsg){
			$this->mAlertMsg = $_alertMsg;
		}
		
		/**
		 * Sets the category tto be shown.
		 * 
		 * @param string|integer $category
		 */		
		function setCategory($category){
			$this->mCategory = $category;
		}
		
		/**
		 * Sets the view mode whether it is in arrange mode or in view mode.
		 * 
		 * @param string $mode one of the two values (arrange or view)
		 * @return void
		 */
		function setMode($_mode){
			$this->mMode = $_mode;
			if ("arrange" == $_mode) {
			  $this->mTemplate = "tpl.IncArrangeModeFeaturedSubGroupList.php";
			}
			else {
			  $this->mTemplate = "tpl.IncViewModeFeaturedSubGroupList.php";
			}
		}	
    
    /**
     * Fetches the template of featured subgroups. 
     * Values on the template will be based on the properties set.
     * 
     * @throws exception throws exception when $this->mFeaturedGroups is not set or set to null
     * @return string
     */
		function fetchTemplate(){
			if (is_null($this->mFeaturedGroups))
			  throw new exception("Set first the featured subgroups to be shown.");
			$this->tplSubGroups = new Template();
			$this->tplSubGroups->set_path("travellog/views/subgroups/");
			$this->tplSubGroups->set("iterator", $this->mFeaturedGroups);
			$this->tplSubGroups->set("alertMsg", $this->mAlertMsg);
			$this->tplSubGroups->set("mode", $this->mMode);
			$this->tplSubGroups->set("action", $this->mTabAction);
			$this->tplSubGroups->set("grpID", $this->mParentGroup->getGroupID());
			$this->tplSubGroups->set("parent", $this->mParentGroup);
			$this->tplSubGroups->set("page", $this->mPage);
			$this->tplSubGroups->set("category", $this->mCategory);
			
			return $this->tplSubGroups->fetch($this->mTemplate);
			//return preg_replace("/(\s)+/"," ",$this->tplSubGroups->fetch($this->mTemplate));
		}

    /**
     * Shows the featured subgroups template. Values will be based on the properties set.
     * 
     * @throws exception throws exception when $this->mFeaturedGroups is not set or set to null
     * @return void
     */
		function render(){
			try {
			  echo $this->fetchTemplate();
			}
			catch(exception $ex){
				throw new exception($ex->getMessage());
			}
		}	
	}