<?php
//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('layoutID', 'page_default');
Template::setMainTemplateVar('page_location', '');
?>

<div class="area" id="intro">
	<h1><?= $title ?></h1>
	<div class="section_detail">
		<div class="content">
			<?=$content ?>
		</div>
	</div>
</div>
			