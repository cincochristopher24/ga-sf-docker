<?php
// DEFAULTS
if (!isset($title)) 		  $title 			= 'GoAbroad Network: Online Community for Travelers';
if (!isset($metaDescription)) $metaDescription  = '';
if (!isset($metaKeywords))    $metaKeywords     = 'travel community, travel network, travel online, travel website, travel web site, travel blog, travel note, free travel blogs, travel blogs, free travel blog, personal travel blogs, travel notes, travel journal, travel community website, travel notes blog, free travel journal, free online travel journal, travel journal online, blogs, journals, travel diaries, travel journals, travel diary, traveler, travelers community, travelers community, traveler community, free travel journals, free online travel journals, online travelers community, travel review, 
travel comments, travel observations, trip journals, travelogues, travel reviews, travel guide, travel advice, travel photos, travel maps';
if (!isset($layoutID)) 		  $layoutID 		= 'basic';
if (!isset($page_location))	  $page_location 	= '';

//header('Content-Type: text/html, charset=utf-8');


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=$title?></title>
<meta name="description" content="<?=$metaDescription ?>" />
<meta name="keywords" content="<?=$metaKeywords?>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/css/base.css" rel="stylesheet" type="text/css" />
<style type="text/css">
@import url("/css/<?=$layoutID?>.css");
#wrapper {
    min-width:200px;
}
</style>

<?php Template::includeDependentJs('/js/prototype.js', array('include_here' => true))?>
<!-- compliance patch for microsoft browsers -->
<!--[if lt IE 8]>
<link href="/css/main.ie.fix.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
//Fix image cache problem with IE 6
try {
  document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
</script>
<![endif]-->
</head>
<body id="<?=$layoutID?>" class="popup">
<div id="wrapper">
	<div id="inner_wrapper">
	
	<div id="body">
		
		<?=$contents?>
		
	</div>
	</div>
</div>
</body>
</html>
