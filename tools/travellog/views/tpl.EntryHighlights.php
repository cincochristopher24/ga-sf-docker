<? if( (count($contents['col_highlights']) || $contents['show_admin_controls'])  ): ?> 
<div id="main-highlights">
	
	<div id="gallery" style="display: none">  
		<h2>Click an image to select.</h2>
		<div id="image-gallery" class="jcarousel-skin-ie7"></div>      
	</div>
	
		<h2><span>Highlights</span> 
			<?if( $contents['show_admin_controls'] ):?>
				<a href="javascript:void(0)" id="EntryHighlightsLink" class="entry_links top_shift"><span>Add a Highlight</span></a>
				<span id="hl_loading" style="display:none"><img src="/images/loading.gif" /></span>
		 	<?endif;?>
		</h2>	
	
	<?if( $contents['show_admin_controls'] ):?>	
		<p class="help_text" id="HighlightsHelpText" <?php if( count($contents['col_highlights']) ):?>style="display:none"<?php endif;?>>Highlight the most interesting or unique event that happened on your trip!</p>
	<?endif;?>
		
	<ul id="ENTRYHIGHLIGHTSLIST" class="content">
		<?if( count($contents['col_highlights']) ):?> 
			<? 
				$slen = count( $contents['col_highlights'] )-1;
				$ctr  = 0;
				foreach( $contents['col_highlights'] as $obj_highlights ):
			?>
			<li id="contentcontainer<?=$obj_highlights->getTravelHighlightID()?>" class="contentholder">
					<?if($obj_highlights->getPhotoID()):?>
						<img class="form_picture" alt="" src="<?=$obj_highlights->getPhoto()?>"/>
					<?endif;?>
					<?=nl2br($obj_highlights->getDescription())?> 
		<?if( $contents['show_admin_controls'] ):?>
				<span>
					<!--a href="javascript:void(0)" id="edithighlight-<?=$obj_highlights->getTravelHighlightID()?>" rel="gahighlight">Edit</a> | 
					<a href="javascript:void(0)" id="deletehighlight-<?=$obj_highlights->getTravelHighlightID()?>" rel="gahighlight">Delete</a-->
					<a href="javascript:void(0)" onclick="jQuery.edit_highlight(<?php echo $obj_highlights->getTravelHighlightID()?>)">Edit</a> | 
					<a href="javascript:void(0)" onclick="jQuery.remove_highlight(<?php echo $obj_highlights->getTravelHighlightID()?>)">Delete</a>
				</span>
		<?endif;?>
			</li>
				<?	
					$ctr++;
					endforeach;
				?>
			<?endif;?>	
		</ul>	
</div>
<? endif; ?>


