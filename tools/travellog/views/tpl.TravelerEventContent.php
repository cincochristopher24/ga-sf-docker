<?

$file_factory = FileFactory::getInstance(); 
$d = $file_factory->getClass('GaDateTime');

?>

<div id="view">
	<?if( $props['obj_event']->getDisplayPublic() || ( $props['is_login'] && $props['is_owner'] ) ):?>
		
		<div class="date meta" id="event_details_date_display">
			<?php echo $d->set($props['local_datetime'])->htmlEventDateFormat() ?>
			<span id="events_time" ><?php echo date('h:i A', strtotime($props['local_datetime'])) ?></span>
			
			<span id="events_zone" >
				<?php echo 'GMT '. substr(date_create($props['local_datetime'], new DateTimeZone($props['local_timezone']))->format('O'), 0, 3); ?>
			</span>
			
		</div>
		
		<?/*
		<div class="date meta" id="event_details_date_display">
			<?= $d->set($props['obj_event']->getTheDate())->htmlEventDateFormat() ?>
			<span id="events_time" ><?php echo date('h:i A', strtotime($props['obj_event']->getTheDate())) ?></span>
			<?php if(function_exists('date_create') && $props['obj_event']->getTimeZoneID() ):?>
			<span id="events_zone" onmouseover="jQuery().showEquivalentLocalTime('<?=$props['obj_event']->getTravelerEventID()?>','travelerevent', this)" onmouseout="CustomPopup.removeBubble();">
				<?php echo 'GMT '. substr(date_create($props['obj_event']->getTheDate(), new DateTimeZone($props['obj_event']->getTimeZone()))->format('O'), 0, 3); ?>
			</span>
			<?php endif;?>
		</div>
		*/?>
			<h4><?=$props['obj_event']->getTitle()?></h4>
		<div class="event_details"><?= HtmlHelpers::Textile($props['obj_event']->getDescription()); ?></div>
		<div class="clear"></div>
		<?if ( $props['is_login'] && $props['is_owner'] ):?> 
			<ul class="actions">
				<li><a href="javascript:void(0)" onclick="jQuery().addForm('action=modify&amp;eID=<?=$props['obj_event']->getTravelerEventID()?>&amp;context=2')" class="button">Edit</a></li><li><a href="javascript:void(0)" onclick="CustomPopup.initialize('Delete Event ?','Are you sure you want to delete this event?', function(){ jQuery().deleteEvent('action=delete&eID=<?=$props['obj_event']->getTravelerEventID()?>&amp;context=2') },'Delete Event','1');CustomPopup.setJS();CustomPopup.createPopup();" class="button">Delete</a></li>
			</ul>
		<? endif; ?> 
	<?else:?>
		<p class="help_text"> <span>Sorry, you are not allowed to view this event<span>.</p>
	<? endif; ?>
</div>
