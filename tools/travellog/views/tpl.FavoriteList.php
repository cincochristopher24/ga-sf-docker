<?
/**
* <b>Template Favorite List</b>
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/
?>


<table cellpadding="0" cellspacing="15" border="1" width="80%">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="1" width="100%">
				<tr><td width="40%">Title</td><td width="60%">Url</td></tr>
				<? foreach($favorites as $favorite): ?>
					<tr>
						<td><?= $favorite->getTitle() ?></td>
						<td><?= $favorite->getUrl() ?></td>
					</tr>
				<? endforeach; ?>
			</table>
		</td>
	</tr>
</table>
<a href="/dashboard.php">Return to Dashboard</a>
