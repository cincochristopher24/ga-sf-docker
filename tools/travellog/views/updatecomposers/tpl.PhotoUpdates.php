<?	require_once('travellog/model/updates/Class.UpdateAction.php'); 

	switch($action){
		case UpdateAction::GROUP_UPLOADED_PHOTO :
			$photocnt = ($photocnt > 1) ? $photocnt.' photos were' : $photocnt.' photo was'; 
			echo $photocnt.' uploaded by staff/admin';
			break;
		case UpdateAction::GROUP_UPLOADED_PHOTO_FROM_MEMBER :
			$photocnt = ($photocnt > 1) ? $photocnt.' photos were' : $photocnt.' photo was'; 
			echo $photocnt.' grabbed from members';
			break;	
		case UpdateAction::MEMBER_DELETED_PHOTO :			
			$photocnt = ($photocnt > 1) ? $photocnt.' photos' : $photocnt.' photo';
			echo 'deleted '.$photocnt.' from your album';
			break;
		case UpdateAction::MEMBER_ADDED_PHOTO_TO_PROFILE:
		case UpdateAction::MEMBER_ADDED_PHOTO_TO_ALBUM:
		case UpdateAction::MEMBER_ADDED_PHOTO_TO_ENTRY:			
			$photocnt = ($photocnt > 1) ? $photocnt.' photos' : $photocnt.' photo';
			echo 'just added '.$photocnt.'.';
			break;	
		case UpdateAction::PHOTO_DELETED_THROUGH_DELETE_ENTRY:
			$photocnt = ($photocnt > 1) ? $photocnt.' photos' : $photocnt.' photo';
			echo 'deleted a journal entry containing '.$photocnt.' from this album';
			break;
		case UpdateAction::PHOTO_DELETED_THROUGH_DELETE_JOURNAL:
			$photocnt = ($photocnt > 1) ? $photocnt.' photos' : $photocnt.' photo';
			echo 'deleted a journal containing '.$photocnt.' from this album';
			break;
		case UpdateAction::PHOTO_DELETED_THROUGH_UNAPPROVE_JOURNAL:
			$photocnt = ($photocnt > 1) ? $photocnt.' photos' : $photocnt.' photo';
			echo 'disapproved a journal containing '.$photocnt.' from this album';
			break;
		case UpdateAction::PHOTO_DELETED_THROUGH_DELETE_PROFILE_PHOTO:
			$photocnt = ($photocnt > 1) ? $photocnt.' photos' : $photocnt.' photo';
			echo 'deleted '.$photocnt.' contained in this album';
			break;
		case UpdateAction::PHOTO_ALBUM_WITH_ADDED_PHOTOS:
			echo 'has recently added photos on '.$date;
			break;	
		default;
	}

?>
