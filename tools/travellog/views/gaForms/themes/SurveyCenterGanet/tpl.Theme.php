<form name="frmSurvey" action="<?= $actionUrl ?>" method="post" class="interactive_form">
	<input name="frmID" type="hidden" value="<?= $formData['gaFormID'] ?>" />
	<ul class="form">
		<?php foreach($formData['fields'] as $iFld):?>
			<li>
					<?php if(in_array($iFld['gaFormFieldID'],$errFields)) :?>
					<div class="errors">
						<strong>This field is required!</strong>								
					<?php endif;?>
					<label for="fld_<?= $iFld['gaFormFieldID'] ?>" <?php if(GaFormControl::SECTION_BREAK == $iFld['fieldType']){echo 'class="sectionBreak"';} ?>>
						<?= nl2br($iFld['caption']) ?>
						<?php if($iFld['requiredStatus']):?>
							<span class="required">*</span>
						<?php endif; ?>
					</label>
					<ul>
						<?php 
							$inputs = $iFld['inputs'];
							foreach($inputs as $iInput):
						 ?>
						 <li class="<?= $iFld['orientation'] ? 'horizontalOriented' : 'verticalOriented'?>">
							 <?php 	if($iInput['inputType'] == GaFormControl::TEXTBOX): ?>
							 	<?php 
									$class = (($iInput['size']==0)?'small_text':(($iInput['size']==1)?'medium_text':'large_text'));						 		
									/***
									$txtVal = count($formFieldValues[$iFld->getSurveyFieldID()])
										? $formFieldValues[$iFld->getSurveyFieldID()][0]
										: null;
							 		***/
									if($iInput['isMultiline']):
							 	?>
							 		<textarea name="fld_<?=$iFld['gaFormFieldID'] ?>" class="<?= $class ?>" ><?/**= $txtVal **/?></textarea>
							 	<?php else:?>
							 		<input name="fld_<?=$iFld['gaFormFieldID'] ?>" type="text" class="<?= $class ?> text" value="<? /**=  $txtVal **/?>" />
							 	<?php endif;?>
							<?php 	elseif($iInput['inputType'] == GaFormControl::CHECKBOX):
							 		//$isChecked = (in_array($iInput['text'],$formFieldValues[$iFld->getSurveyFieldID()]));
									$isChecked = true;
						 	?>
							 	<input type="checkbox" name="fld_<?=$iFld['gaFormFieldID'] ?>[]" <?= ($isChecked)?'checked':''?> value="<?= $iInput['text'] ?>" />
								<? if (strlen($iInput['text']) > 100): ?>
									<?= substr($iInput['text'], 0, 100); ?>...
								<? else: ?>
									<?= $iInput['text'] ?>
								<? endif;?> 						 	
							 <?php 	
							 	elseif($iInput['inputType'] == GaFormControl::RADIO): 
							 		//$isChecked = (in_array($iInput['text'],$formFieldValues[$iFld['gaFormFieldID']]));
									$isChecked = true;
							 ?>						 
							 	<input type="radio" name="fld_<?=$iFld['gaFormFieldID'] ?>[]" <?= ($isChecked)?'checked':''?> value="<?= $iInput['text'] ?>" />
								<? if (strlen($iInput['text']) > 100): ?>	
									<?= substr($iInput['text'], 0, 100); ?>...
								<? else: ?>
									<?= $iInput['text']; ?>
								<? endif; ?>
							 <?php 	
							 	elseif($iInput['inputType'] == GaFormControl::COMBOBOX): 
							 		$class = ($iInput['size'] == 0 ?'small_select':($iInput['size']==1 ?'medium_select':'large_select'));
							 ?>
							 	<select name="fld_<?= $iFld['gaFormFieldID']; ?>" class="<?=$class?>" >
							 		<?php $opts = $iInput['options'];?>
							 		<?php 
							 			$i = 0;						 			
							 			foreach($opts as $iOpt):
							 				$isSelected = (in_array($iOpt,$formFieldValues[$iFld['gaFormFieldID']]));
							 		?>
							 			<option value="<?= $iOpt ?>" <?= ($isSelected) ?'selected':''?> ><?= $iOpt ?></option>
							 		<?php
							 			endforeach;
							 		?>
							 	</select>								 
							 <?php endif; ?>
						 </li>
						 <?php endforeach; ?>
					</ul>
					<?php if($iFld['orientation']): ?>
					 	<div class="clear" ></div>
					<?php endif; ?>
					<?php if(in_array($iFld['gaFormFieldID'],$errFields)) :?>
						</div>
					<?php endif;?>
			</li>
		<?php endforeach; ?>	
		<li class="actions">
			<input type="submit" value="Submit" class="submit ga_interactive_form_field" />
			<input type="reset" value="Reset" class="button ga_interactive_form_field" />
		</li>
	</ul>
</form>