<?php echo $profile->render(); ?>
<?php echo $subNavigation->show(); ?>

<script type="text/javascript">
//<[!CDATA[

	function doSubmitOnce(currentForm){
		// Run through elements and disable submit buttons
		for(i=0;i<currentForm.length;i++)
		{
			var currentElement = currentForm.elements[i];
			if(currentElement.type.toLowerCase() == "submit")
			currentElement.disabled = true;
		}
	}

	// We need to register our submit once function on all forms
	if(document.all||document.getElementById) 
	{	
		// Run through all forms on page
		for(i=0;i<document.forms.length;i++)
		{
			var currentForm = document.forms[i];
			// Register event handler
			// Use quirksmode idea for flexible registration by copying existing events
			var oldEventCode = (currentForm.onsubmit) ? currentForm.onsubmit : function () {};
			currentForm.onsubmit = function () {oldEventCode(); doSubmitOnce(currentForm)};
		}
	}
	
	var Captcha = new CaptchaLoader({					
		imageTagID	: 'captchaImg',
		onLoad		: function(params){			
			$('encSecCode').value  = params['code'];
		},
		imgAttributes : {
			width : '200',
			height: '50'	
		}
	});
				
	//]]>
</script>

<div class="layout_2" id="content_wrapper">
	<div id="wide_column">
		<div class="section">
			<h2><span>Deactivate Account</span></h2>
			<div class="content">
				<form name="frmDeactivate" id="frmDeactivate" action="/account-deactivation.php" method="post">
					<p class="help_text">
						Please tell us why you're deactivating your account.	
					</p>
									
					<ul class="form">
						<li>
							<input type="radio" name="radReason" value="1" /> I don't find GoAbroad.net useful.
						</li>
						<li>
							<input type="radio" name="radReason" value="2" /> I don't know how to use GoAbroad.net.
						</li>	
						<li>
							<input type="radio" name="radReason" value="3" /> I am switching to another travel networking site.
						</li>
						<li>
							<input type="radio" name="radReason" value="4" checked="checked"/> Others
						</li>
						<li>
							Please explain further:
							<textarea name="txtExplanation" rows="7"></textarea>
						</li>
			
						<hr style="clear: both; border: 1px solid #dfdfdf;"/>
						
						<li>
							<label for="txtFindString">Security Code:</label>
							<p class="supplement">Please type the code that you see in the image below:</p>
							<input type="text" id="securityCode" name="securityCode" value="" class="text" /><br />
							<img  id="captchaImg" src="<?php echo $secSrc; ?>" alt="Captcha" /><br />
							<a id="refresh" href="javascript:void(0)" onclick="Captcha.load()" style="color:red; font-weight:normal">[Refresh Image]</a>
							<a href="http://www.goabroad.com/captchainfo.cfm" title="Why do you need to enter this code?" onclick="window.open('captcha-info.php?location=deactivation-support&amp;network=<?= $contents['network'] ?>', 'captchainfo', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable,width=500,height=235, top=' + ((screen.width/2)- 1000) + ', left=20'); return false;">
							<img src="http://images.goabroad.com/images/icons/help.gif" border="none"> What is this?</a>
							<input type="hidden" id="encSecCode" name="encSecCode" value="<?php echo $secCode; ?>" />							
						</li>
						
						<hr style="clear: both; border: 1px solid #dfdfdf;"/>
						
						<li class="actions" id="pop_button">
							<input type="hidden" id="action" name="action" value="deactivate" />
							<input type="submit" id = "btnSubmit" name="submit" value="Deactivate" class="submit button_v3" /> or <a href="/account-settings.php">Cancel</a>
						</li>
					</ul>
				</form>
			</div>
		</div>	
	</div>
</div>