<?=$sub_view['entry_sub_navigation_view']->render()?>

    <div class="layout_4" id="content_wrapper">
		
		<div class="journal_section" id="journal_title" >
			  <?=$sub_view['journal_view']->render()?>	
		</div>

      <div id="wide_column">
	
        <div id="journal_entry" class="section">

                <div class="title_container">
	
					<div class="curve_left"><div class="curve_right"></div></div>

	                 <div id="titleprimaryphoto">
	                 	<?=$sub_view['entry_primary_photo_view']->render()?>
	                 </div>

					<div class="title_info">
                  		<?=$sub_view['title_callout_photo_view']->render()?>
					</div>
                    
                	</div>
            
                <div class="content content_container">
   
                  <div class="area" id="photos_area">
                    <div class="photos">
                      <!-- This area is for the highlight text start-->
                      
                      <?=$sub_view['highlights_view']->render()?>

                    </div>
                  </div>

                  <div class="photobox">
                    <div id="gallerycontainer">
                    	  <?//=$sub_view['gallery']->render()?>
                    </div>
                    
                  </div>
                  <!-- This area is for the highlight Photos end-->
                  
                     <div class="entry_container">
                    <?=$sub_view['desc_view']->render()?>
                    
                    
                    <div class="area" id="photos_area2">
                      <div class="photos">
                        <!-- This area is for the traveltip text start-->
                        
                        <?=$sub_view['tips_view']->render()?> 
                      
                        <!-- This area is for the traveltip text end-->
                      </div>
                    </div>
               
					<!-- Added By: Cheryl Ivy Q. Go  28 September 2007 -->
					<div id="comments" class="journal_section comments">						
						<?=$contents['comments_view']->renderAjax()?>
					</div>
					<!-- End -->
					<div class="clear"></div>
                 
                 </div>
            
            </div>
            <div class="clear"></div>
		<div class="foot"></div>
          </div>


      </div>

<div id="narrow_column">

    <?=$sub_view['profile_view']->render()?>

    <?= $sub_view['map_view']->render()?>

	<!-- new Update Postcard Link Start-->

				<div id="postcard" class="section">
				<div class="head_left">
					<div class="head_right"/></div>
				</div>
				<div class="postcard_link content">
					<h3><a href="#">Send as a Postcard</a></h3>
					<p>mail this journal entry<br/> as a postcard to any<br/>of your friends</p>
					<img src="/images/postcard_link.gif">
				</div>
				<div class="foot"/></div>
				</div>

	<!-- new Update Postcard Link End-->

	<?=$sub_view['toc_view']->render()?>

	<div class="clear"></div>	
		
    <div id="gaothers" class="section">
      <?=$sub_view['ads_view']->render()?>
    </div>
	
</div>	


      <div class="clear"></div>

    </div>