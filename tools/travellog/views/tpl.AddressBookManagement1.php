<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
	ob_start();	
?>				
<?php 
	/**
	 * Added by naldz (Nov 20,2006)
	 * This is for the subnavigation links
	 */
	 SubNavigation::displayLinks();
?>
<?php if ($method=="view"){	?>
	<form name="frmAddressBook" action="addressBookManagement.php" method="POST" class="interactive_form">				
		<strong>Address Book Management</strong>&nbsp;&nbsp;<a href="addressBookManagement.php?action=add">[Add Address Book Entry]</a><br /><br />
		<table cellpadding="3px">
			<?php if(!count($members)): ?>
				<tr>
					<td colspan="6">GoAbroad.net members (No Entries)</td>
				</tr>
			<?php else: ?>	
				<tr>
					<td colspan="6">GoAbroad.net members</td>
				</tr>
				<tr>
					<td></td>
					<td><strong>UserName</strong></td>
					<td><strong>Name</strong></td>
					<td><strong>Email Address</strong></td>
					<td><strong>Notify</strong></td>
					<td><strong>Actions</strong></td>
				</tr>						
				<?php foreach($members as $entry){ ?>
					<tr>
						<td><input type='checkbox' name="chkAddressBookEntry[]" value='<?= $entry->getAddressBookEntryId()?>' /></td>
						<td><?= $entry->getUserName() ?></td>							
						<td><?= $entry->getFirstName() ?> <?= $entry->getLastName() ?></td>
						<td><?= $entry->getEmail() ?></td>
						<td align="center"><?= $entry->getIsNotable()?"Yes":"No" ?></td>
						<td>
							<a href="addressBookManagement.php?action=edit&eId=<?=$entry->getAddressBookEntryId() ?>">[Edit]</a>|								
							<a href="addressBookManagement.php?action=delete&eId=<?=$entry->getAddressBookEntryId() ?>" onclick="return confirm('Are you sure you want to delete this address book entry?')">[Delete]</a>
						</td>
					</tr>
				<?php } ?>
			<?php endif; ?>
		</table>
		<br /><br />
		<table cellpadding="3px">
			<?php if(!count($nonMembers)): ?>
				<tr>
					<td colspan="5">Non-members (No Entries)</td>
				</tr>
			<?php else: ?>
				<tr>
					<td colspan="5">Non-members</td>
				</tr>
				<tr>
					<td></td>
					<td><strong>Name</strong></td>
					<td><strong>Email Address</strong></td>
					<td><strong>Notify</strong></td>
					<td><strong>Actions</strong></td>
				</tr>
				<?php foreach($nonMembers as $entry){ ?>
					<tr>
						<td><input type='checkbox' name='chkAddressBookEntry[]' value='<?= $entry->getAddressBookEntryId()?>' /></td>
						<td><?= $entry->getFirstName() ?> <?= $entry->getLastName() ?></td>
						<td><?= $entry->getEmail() ?></td>
						<td align="center"><?= $entry->getIsNotable()?"Yes":"No" ?></td>
						<td>
							<a href="addressBookManagement.php?action=edit&eId=<?=$entry->getAddressBookEntryId() ?>">[Edit]</a>|								
							<a href="addressBookManagement.php?action=delete&eId=<?=$entry->getAddressBookEntryId() ?>" onclick="return confirm('Are you sure you want to delete this address book entry?')">[Delete]</a>
							<!--|<a href="addressBookManagement.php?action=invite&eId=<?=$entry->getAddressBookEntryId() ?>">[Invite]</a>-->
						</td>
					</tr>
				<?php }	?>
			<?php endif; ?>			
		</table>
		<?php if(count($members) || count($nonMembers)): ?>
			<table cellpadding="3px">
				<tr>
					<td colspan="4" align="center">
						With Check:&nbsp;
						<!--<input type="submit" name="method" value="Invite">&nbsp;-->
						<input type="submit" name="method" value="Delete" />
					</td>
				</tr>
			</table>
		<?php endif; ?>
	</form>
	<br /><br />
	<div>
		The information in your addressbook is solely the property of you and the people you have added. 
		We will never contact, sell, rent or share any of the this information at any time. You may invite 
		your contacts to become GoAbroad.net members but it is not required for them to be a member to receive 
		your travel logs and updates.					
	</div>
<?php 
	}
	elseif($method=="add" || $method=="edit"){																						
		?>
		<form name="frmAddressBook" action="addressBookManagement.php?action=save&prevaction=<?=$method?>" method="POST">
			<table>
				<tr>
					<td>Email:</td>
					<td>
						<?php if(!$addressBookEntry->isGaNetMember()): ?>
							<input type="text" name="txtEmail" value="<?= $addressBookEntry->getEmail()?>" />
							<?php if(isset($msg)) echo $msg; ?>
						<?php else: ?>
							<?= $addressBookEntry->getEmail()?>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td>First Name:</td>
					<td><input type="text" name="txtFirstName" value="<?= $addressBookEntry->getFirstName()?>" /></td>
				</tr>
				<tr>
					<td>Last Name:</td>
					<td><input type="text" name="txtLastName" value="<?= $addressBookEntry->getLastName()?>" /></td>
				</tr>
				<tr>
					<td>Details:</td>
					<td><textarea name="txaDetails"><?= $addressBookEntry->getDetails()?></textarea>
				</tr>
				<tr>
					<td colspan="2">						
						Send Notification whenever you update your information?<br />
						<input type="radio" name="radIsNotable" value="0" <?= (!$addressBookEntry->getIsNotable())?"checked":""?> /> No						
						<input type="radio" name="radIsNotable" value="1" <?= ($addressBookEntry->getIsNotable())?"checked":""?> /> Yes
					</td>
				</tr>
				<tr>
					<td colspan="2"><input name="submit" type="submit" value="Save" />&nbsp;<input name="submit" type="submit" value="Cancel" /></td>
				</tr>				
			</table>
			<input type="hidden" name="hidAddressBookEntryId" value="<?=$addressBookEntry->getAddressBookEntryId() ?>" />
		</form>
		<?php
		}
?>