<?php
	Template::setMainTemplateVar('layoutID', 'survey');	
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	require_once('travellog/helper/Class.NavigationHelper.php');
	if (NavigationHelper::isCobrand()) {
		Template::setMainTemplateVar('page_location', 'Home');
	}
	else {
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentJs('/min/g=InteractiveJs', array('top' => true));
	//Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
	//Template::includeDependentJs('/js/interactive.form.js');
	//Template::includeDependentCss('/css/formeditor/survey.css');
?>

<!-- displays the profile component: added by neri 11-06-08 -->
<?= $profile->render()?>
<!-- end -->

<?
	$subNavigation->show();	
?>

<script>
	(function($){
		$.fn.deleteSurvey = function(surveyID, grpID, name) {
			var path = "/surveycenter.php?method=delete&frmID="+surveyID+"&gID="+grpID;
			CustomPopup.initialize("Delete Survey Form?","Are you sure you want to delete survey form '"+ name +"'?<br/>All of its data including the answers of the participants will also be deleted.",path,"Delete");
			CustomPopup.createPopup();
		}
	})(jQuery);
</script>

<div id="content_wrapper">

		<div id="container">
			<div id="container_inner_wrapper">
				<div id="survey_options">
	    	<ul id="top_tabs">
	    		<li id="sel" class="first_tab"><a href="/surveycenter.php?gID=<?=$oGroup->getGroupID()?>" class="first_tab">Manage Surveys</a></li>
	      		<li><a href="/surveyformeditor.php?frmID=0&gID=<?=$oGroup->getGroupID()?>">Add a New Survey</a></li>      		
	    	</ul>
	  	</div>	
				<ul id="content">
			<?php 
				$withRecord = false;
				$iterator->rewind();
				while( $iterator->valid() ):
					$withRecord = true;
			?>
			<li class="survey_box">
				<div class="survey_actions">
	            	<ul>
	              		<li><a href="/surveyformeditor.php?frmID=<?= $iterator->current()->getSurveyFormID() ?>&amp;gID=<?=$oGroup->getGroupID() ?>" class="edit" title="Edit this Survey">Edit</a></li>
	              		<!--li><a href="/surveycenter.php?method=delete&amp;frmID=<?= $iterator->current()->getSurveyFormID() ?>&amp;gID=<?=$oGroup->getGroupID() ?>" class="delete" title="Delete this Survey" onclick="return confirm('Are you sure you wan to delete survey form \'<?= strip_tags($iterator->current()->getName()) ?>\'?\nAll of its data including the answers of the participants will also be deleted.');">Delete</a></li-->
	              		<li><a href="javascript:void(0)" class="delete" id="deleteSurvey" title="Delete this Survey" onclick="jQuery.fn.deleteSurvey(<?=$iterator->current()->getSurveyFormID() ?>,<?=$oGroup->getGroupID() ?>,'<?=strip_tags($iterator->current()->getName()) ?>');">Delete</a></li>
	              		<li><a href="/surveycenter.php?method=<?=$iterator->current()->isLocked()?'unlock':'lock'?>&amp;frmID=<?= $iterator->current()->getSurveyFormID() ?>&amp;gID=<?=$oGroup->getGroupID() ?>" class="<?= $iterator->current()->isLocked()?'unlock':'lock' ?>" title="<?= $iterator->current()->isLocked()?'Open':'Close' ?> this survey"><?= $iterator->current()->isLocked()?'Open':'Close' ?></a></li>              		
	              		<li><a href="/surveystats.php?frmID=<?= $iterator->current()->getSurveyFormID() ?>&amp;gID=<?=$oGroup->getGroupID() ?>" class="stats">Stats</a></li>
	              		<li><a href="/surveyformeditor.php?tplID=<?= $iterator->current()->getSurveyFormID() ?>&amp;gID=<?=$oGroup->getGroupID() ?>" class="template">Duplicate</a></li>
	           	 	</ul>
	          	</div>
	        	<h2><a href="/surveyform.php?frmID=<?= $iterator->current()->getSurveyFormID() ?>&amp;gID=<?=$oGroup->getGroupID() ?>" title="View this survey"><?= strip_tags($iterator->current()->getName()) ?></a></h2>
		      		<p class="gray"><strong class="dark_gray">Created on:</strong> <?= date("M d, Y",strtotime($iterator->current()->getDateCreated())) ?></p>
		        	<p><strong class="dark_gray">Participants:</strong>
		        	<?php
		        		$pars = $iterator->current()->getParticipants();
		        		$ctr = 1;
		        		foreach($pars as $iPar):
		        			try{
								$mGroup = $groupFactory->create( array($iPar->getGroupID()) );
								$grp = $mGroup[0];
							}
							catch(exception $e){
								$grp = null;
							}
		        	?>
						<?if(!is_null($grp)):?>
							<a href="/group.php?gID=<?=$iPar->getGroupID()?>"><?=$grp->getName().'('.count($grp->getMembers()).')';?></a><?= ($ctr++ < count($pars))?',':'' ?>
						<?endif;?>
					<?php endforeach; ?></p>
					<?php
						$totalParticipants = $iterator->current()->getTotalNumberOfUniqueTravelerParticipants();
						$participated = $iterator->current()->getNumberOfTravelerParticipantsWhoParticipated();
					?>
					<p class="gray"><strong class="dark_gray">Total No. of Participants: </strong> <?= $totalParticipants ?></p>
		        	<p class="gray"><strong class="dark_gray">No. who participated:</strong> <?= $participated ?></p>
		        	<p class="gray"><strong class="dark_gray">No. who didn't participate:</strong> <?= ($totalParticipants-$participated < 0 ? 0 : $totalParticipants-$participated) ?></p>	         
	        </li>                
	        <?php
	        	$iterator->next();
	        	endwhile; 
	        ?>
	        <?php if(!$withRecord):?>
				<li class="survey_box">
					<h3 class="help_text"> <span>You have no survey forms yet</span>. Click on the <a href="/surveyformeditor.php?frmID=0&amp;gID=<?=$oGroup->getGroupID() ?>">Add a New Survey</a> link to create your first survey.</h3>
				</li>		
			<?php endif;?>
			<?php if($withRecord): ?>
	        	<li class="pagination"> <?= $paging->showPagination() ?> </li>
	       	<?php endif;?>        
	    </ul>
	    	</div>
		</div>
		<div class="clear"></div>


</div>