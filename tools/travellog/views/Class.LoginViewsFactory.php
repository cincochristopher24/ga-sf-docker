<?php
	/*
	 * Class.LoginViewsFactory.php
	 * Created on Nov 22, 2007
	 * created by marc
	 */
	 
	class LoginViewsFactory{
		
		static $instance = NULL;
		private $views = array();
		
		static function getInstance(){ 
			if( self::$instance == NULL ) self::$instance = new LoginViewsFactory; 
			return self::$instance;
		}
		
		function createView( $view = constants::VIEW_LOGIN_FORM){
			
			switch($view){
				
				case constants::VIEW_LOGIN_PAGE:
					if( !array_key_exists("ViewLoginPage", $this->views) ){
						require_once("travellog/views/Class.ViewLoginPage.php");
						$this->views["ViewLoginPage"] = new ViewLoginPage;
					}
					return $this->views["ViewLoginPage"];
				
				default:
					if( !array_key_exists("ViewLoginForm", $this->views) ){
						require_once("travellog/views/Class.ViewLoginForm.php");
						$this->views["ViewLoginForm"] = new ViewLoginForm;
					}
					return $this->views["ViewLoginForm"];		
			}
		}
		
	} 
?>
