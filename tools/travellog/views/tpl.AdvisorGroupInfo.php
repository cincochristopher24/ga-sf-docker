<?php
/**
 * Created on Mar 23, 2007
 * @author daf
 * 
 * 
 */
?>
<li>	
<label for="txtAddress1">Street Address<span class="required">*</span>:</label>
<input value="<?= stripslashes($txtAddress1) ?>" name="txtAddress1" maxlength="100" size="60" class="text" type="text" >
</li>
<input value="<?= stripslashes($txtAddress2) ?>" name="txtAddress2" maxlength="100" size="60" class="text" type="text">
</li>
<li>
<input value="<?= stripslashes($txtAddress3) ?>" name="txtAddress3" maxlength="100" size="60" class="text" type="text">
</li>
<li>
<label for="txtCity">City<span class="required">*</span>:</label>
<input value="<?= stripslashes($txtCity) ?>"  name="txtCity" maxlength="50" size="20" class="text" type="text">
</li>
<li>
<label for="txtState">State/Province:</label>
<input value="<?= stripslashes($txtState) ?>" name="txtState" maxlength="50" size="25" class="text" type="text">
</li>
<li>
<label for="txtZip">Zip/Postal Code:</label>
<input value="<?= stripslashes($txtZip) ?>" name="txtZip" id="zip" maxlength="50" size="20" class="text" type="text">
</li>
<li>
<label for="selGroupCountry">Country<span class="required">*</span>:</label>
<select name="selGroupCountry"  class="fieldboxes" id="selCountry">
	<option value  = "0">-SELECT-</option>
	<?=$selGroupCountry?>
</select>	
</li>
<li>
<label for="txtPhone">Phone:</label>
<input value="<?= stripslashes($txtPhone) ?>" name="txtPhone" id="phone" maxlength="50" size="20" class="text" type="text">
</li>
<li>
<label for="txtFax">Fax:</label>
<input value="<?= stripslashes($txtFax) ?>" name="txtFax" id="fax" maxlength="50" size="25" class="text" type="text">
</li>
<li>
<label for="txtWeburl">Organizational Homepage URL :</label>
http://
<input value="<?= stripslashes($txtWeburl) ?>" name="txtWeburl" id="weburl" maxlength="150" size="60" class="text" type="text">
</li>
<li>
<label for="mission">Mission Statement:</label>
<em class="required">please do not include email or web site addresses</em><br>
<textarea name="txaMission" cols="70" rows="15"><? if (strlen($txaMission) ) echo $txaMission ?></textarea>


