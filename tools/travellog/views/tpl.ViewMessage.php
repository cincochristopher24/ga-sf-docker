<?
	/**
	* tpl.ViewMessage.php
	* @author marc
	* aug 2006
	*/
	
	require_once('Class.GaDateTime.php');
	require_once('Class.Constants.php');
	$d = new GaDateTime();

	$contents["subNavigation"]->show(); 
?>
	<div class="layout_2" id="content_wrapper">
			<div id="wide_column">
			
				<div class="section" id="section_messages">
					<h2><span>Message</span></h2>
					<div class="content">
					<form action="messages.php" method="post" onsubmit="doSubmitOnce(this)">
						<!--<h1></h1>-->
						<div class="view_message">
							<table width="100%" cellspacing="0">
								<? if( strlen($contents["errString"]) ) : ?>
									<tr>
										<td> <?= $contents["errString"]; ?> </td>
									</tr>
								<? else: ?>
									<tbody class="message_info">
										<? if( in_array($contents["active"],array(constants::SENT_ITEMS,constants::TRASH,constants::DRAFTS)) ): ?>
											<tr>
												<th scope="row" width="80">To:</th>
												<td><?= implode(", ",$contents["destination"]); ?></td>
											</tr>	 
										<? endif; ?>	
										<? if( !in_array($contents["active"],array(constants::SENT,constants::DRAFTS)) ): ?>	
											<tr>
												<th scope="row" width="80">From:</th>
												<td><?= $contents["source"]; ?></td>
											</tr>		
										<? endif; ?>	
									
										<tr>
											<th scope="row" width="80">Subject:</th>
											<td><p><?php echo stripslashes($contents["message"]->getTitle()); ?></p></td>
										</tr>
										<tr class="border_line">
											<th scope="row" width="80">Date:</th>
											<td><?php echo $d->set($contents["message"]-> getCreated())->standardFormat(); ?></td>
										</tr>
									</tbody>
									<tbody>
									<tr class="message_cont">
										<td colspan="2"> <?php echo NL2BR(stripslashes($contents["message"]->getText())); ?> </td>
									</tr>
									
									<tr class="actions">
										<td colspan="2">
											<?= $sub_views['MESSAGE_ACTIONS']->render(); ?>
											<input type="hidden" name="hdnReferer" value="<?php echo htmlspecialchars($contents["referer"]) ?>" />
											<input type="hidden" name="hdnActive" value="<?php echo $contents["active"] ?>" />
											<input type="hidden" name="hdnMessageID" value=<?php echo "\"" . $contents["message"]->getAttributeID() . "\"" ?> />
											<input type="hidden" name="hdnAction" id="hdnAction" value="" type="hidden" />
											<div>
												<a href="reportabuse.php?msgID=<?= $contents["message"]->getAttributeID()?>">Report inappropriate message</a>
											</div>
										</td>
									</tr>
									</tbody>
								<? endif; ?>			 
							</table>
						</div>						
					</form>
					</div>
				</div>
			</div>
			
			<div class="area" id="narrow_column">
				<?= $sub_views['MESSAGE_NAVIGATION']->render(); ?>
			</div>
</div>

<?   
	if( strlen($contents["errString"]) ){
	 	header("Refresh: 3; URL=http://" . $_SERVER['SERVER_NAME'] . "/messages.php?" . $active);	
	}
?>	
