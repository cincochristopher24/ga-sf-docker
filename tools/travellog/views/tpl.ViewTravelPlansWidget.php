<div id="eventsContainer">
  <p id="mtpLoadingIndicator" style="display: none">Please wait...</p>
  <!-- Begin Itineraries -->
  <dl id="iteneraries">
    <a class="first" href="#addLink" id="add"> Add a New Itinerary </a>  
	<?php foreach ($travelItems as $travel) : ?>
    <div id="itineraryContainer<?php echo $travel->getTravelWishID(); ?>">
      <dt class="itenerary_main_details">
        <span>
      	  <?php
      	  $destinations = $travel->getDestinations();
          if (count($destinations) == 1) echo $destinations[0]->getDateRange();      	
      	  else echo $travel->getDateRange();	 
          ?>
        </span>
        <p id="title"><?php echo htmlentities($travel->getTitle()) ?></p>
        
        <ul class="actions">
          <li class="first">
            <a href="#editLink" id="edit_<?php echo $travel->getTravelWishID(); ?>"> Edit </a>
          </li>
          <li>
            <a href="#deleteLink" id="delete_<?php echo $travel->getTravelWishID(); ?>"> Delete </a>
          </li>  
        </ul>  
      </dt>

      <dd class="itenerary_secodary_details first"> 
        <p id="details"><?php echo htmlentities($travel->getDetails()) ?></p>

        <dl class="itenerary_destinations">
        <?php 
	    foreach ($destinations as $destination) { ?>
          <dt><?php echo $destination->getDateRange(); ?></dt>
          <dd><?php echo $destination->getCountry()->getName() ?>: <?php echo $destination->getDescription() ?>
          </dd>
	    <?php
	    } ?>
        </dl>	  
      </dd>
    </div>
	<?php endforeach; ?>

    <div class="widget_help">
	  <strong> You currently have <span id="travelItemCount"><?php echo $totalNumItems; ?></span> Travel <?php echo ($totalNumItems == 1) ? 'Plan' : 'Plans';?></strong> <?php if ($pastTravelsCount) :?>(<?php echo $includePastTravelsSetting ? 'with' : 'and'; ?> <span id="pastTravelsCount"><?php echo $pastTravelsCount; ?> Past <?php echo $pastTravelsCount == 1 ? 'Travel' : 'Travels'; ?></span>) <?php endif;?>
	  <p class="help_text"> 
	    You have currently set to display <span id="settingsCount"><?php echo $itemsToDisplaySetting; ?> travel <?php echo ($itemsToDisplaySetting == 1) ? 'plan' : 'plans';?></span> above as default, please note that they are ordered in terms of time proximity. 
	    <a id="settingsLink" href="javascript:void(0);" style="display: block;">Change your settings.</a>
	  </p>
    
      <form id="mtpSettingsForm" name="mtpSettingsForm" action="widget.php" method="post">
        <div id="mtpSettings" style="display: none;">
	      Display:<input id="inputSettings" name="inputSettings" type="text" value="<?php echo $itemsToDisplaySetting; ?>" style="width: 30px;" />
          <span id="FixThisHack">
	        <input id="inputPast" name="inputPast" class="input_box" type="checkbox" <?php if ($includePastTravelsSetting) echo 'checked="true"' ?> />Include past travels&nbsp;
          </span>
	      <a id="saveSettings" href="javascript:void(0);">Apply</a> <a id="cancelSettings" href="javascript:void(0);">Cancel</a>
        </div>  
      </form>
    </div>  
	
    <div class="actions">
	    <a class="last" id="list" href="#viewAllLink" style="display: <?php echo ($totalNumItems > count($travelItems) && (count($travelItems) > 0)) ? 'block' : 'none'; ?>">View All</a>
    </div>
  </dl> 
  <!-- End Itineraries -->
  
  <!-- The contents of the following div is loaded on-demand. -->
  <div id="travelPlansForm" style="display: none"></div>
</div><!-- End Events container -->

<div id="savedMtp" class="a_notice" style="display: none">Your changes have been saved.</div>
<!-- TODO: look for an alternative method -->
<?php 
if (isset($doAction)) {
  if ($doAction == 'add') { ?>
  
<script>
(function () {
  
  LIB.toggleVisibility('mtpLoadingIndicator', 'iteneraries'); 
      
  new LIB.Ajax('POST', 'widget.php?app=mtp&action=showAddForm', {
    onSuccess: function(o) {
      //API.setElementHtml('travelPlansForm', o.responseText);
      LIB.getEBI('travelPlansForm').innerHTML = o.responseText;
      LIB.toggleVisibility('mtpLoadingIndicator', 'travelPlansForm');
    }
  });
})();
</script>

<?php 
  } else if($doAction == 'edit') { ?>

<script>
(function () {
  
  LIB.toggleVisibility('mtpLoadingIndicator', 'iteneraries'); 
  
  new LIB.Ajax('POST', 'widget.php?app=mtp&action=showEditForm', {
    onSuccess: function(o) {
    
      API.setElementHtml(API.getEBI('travelPlansForm'), o.responseText);
      API.getEBI('addDestination').innerHTML = 'Add ' + LIB.numToWords(API.getEBCN('destinations', API.getEBI('theCountries')).length + 1) + 'Destination';
      
      LIB.toggleVisibility('mtpLoadingIndicator', 'travelPlansForm');
    },
    
    body: {
      travelWishID: '<?php echo $twID; ?>' 
    }
  });

})();
</script>

<?php 
  } else if($doAction == 'viewAll') { ?>

<script>
(function () {
  
  LIB.toggleVisibility('iteneraries', 'mtpLoadingIndicator');
  
  new LIB.Ajax('POST', 'widget.php?app=mtp&action=list', {
  	
    onSuccess: function(o) {
      //API.getEBTN('div', API.getEBI('travelplans_passport'))[0].innerHTML = o.responseText;
      //API.getEBI('list').innerHTML = "Less";
      API.setElementHtml(API.getEBTN('div', API.getEBI('travelplans_passport'))[0], o.responseText);
      API.setElementHtml(API.getEBI('list'), "Less");	          
    },
    
    body: {
    	numItemsToDisplay : 'all'
    }	
  });
})();
</script>

<?php 
  }
}?>