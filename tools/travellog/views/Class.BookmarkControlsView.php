<?php
require_once("travellog/views/Class.AbstractView.php");
class BookmarkControlsView extends AbstractView{

	function prepareURLParameters(){
		/*try{
			$shortURL = BitLyApi::getInstance()->shortenUrl($this->contents['permalink']);
		}catch(Exception $e){
			$shortURL = $this->contents['permalink'];
		}*/
		$shortURL = $this->contents['permalink'];
		$this->contents['permalink'] = urlencode($shortURL);
		$this->contents['title'] = urlencode($this->contents['title']);
	}	
	
	function render(){
		$this->obj_template->set( 'contents', $this->contents );
		return $this->obj_template->fetch('tpl.BookmarkControlsView.php');
	}
}
?>
