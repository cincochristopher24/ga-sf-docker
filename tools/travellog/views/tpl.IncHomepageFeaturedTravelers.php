<div class="section" id="featured_travelers">
	<h2>
		<span><?=$featTravelerSection->getCaption()?></span>
		<span class="header_actions">
			<a href="/travelers.php" class="more" title="Learn more from the travel experiences of other GoAbroad network travelers. Read their travel reviews and notes.">More Travelers</a>
		</span>
	</h2>
	<div class="content">
	<!-- Changed classname from "users" to "usrs2 bubbled" -->
    	<ul class="users2 bubbled">
			<?php for($idxtravprof=0; $idxtravprof<count($featTraveler); $idxtravprof++){ 
					$featTravProfile = $featTraveler[$idxtravprof]->getTravelerProfile();
					$_tmpUsernamePossessive = StringFormattingHelper::possessive($featTravProfile->getUserName());					
					$currLocation = "";
					if( $featTravProfile->getCurrLocationID() ){
						$location = $featTravProfile->getCurrLocation();
						$country = (!is_null($location)) ? $location->getCountry() : null;
						if(!is_null($location) && !is_null($country)){	
							$this->props['curr_hometown']         = $location->getName();
							$this->props['curr_country']  	     = $country->getName(); 
							if( $location->getName() != $country->getName() ){
								$currLocation =  $location->getName() . ", ";
							}
							$currLocation .=  $country->getName();
						}	
					}
			?>
			<li>
				<a class="thumb bubble_control" href="/<?php echo $featTravProfile->getUserName();?>" >
					<img class="pic" alt="<?php echo $_tmpUsernamePossessive;?> Travel Journals" title="Read <?php echo $_tmpUsernamePossessive;?> travel experiences and travel notes" src="<?php echo $featTravProfile->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width="65" height="65"/>
				</a>
				<div class="details bubble">
					<?php if ($featTravProfile->getHTLocationID()) { ?>
							<?php $cID = $featTravProfile->getCity()->getCountry()->getCountryID(); ?>
							<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="Travel notes from <?php $featTravProfile->getCity()->getCountry()->getName();?>" />
					<?php } ?>
					<strong><a class="username" href="/<?php echo $featTravProfile->getUserName();?>" title="Read <?php echo $_tmpUsernamePossessive;?> travel blogs and travel notes"><?php echo $featTravProfile->getUserName();?></a></strong> 
					<?php if( "" != $currLocation ): ?>
						<span class="current_loc">Currently in: <?php echo $currLocation;?></span>
					<?php endif; ?>
				</div>
			</li>
			<?php } ?>
		</ul>
		<div class="clear"></div>
	</div>
	<div class="foot"></div>
</div>