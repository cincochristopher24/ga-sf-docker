<!--div id="advisor_how_to">
	<h2>For Advisors</h2>
	<p><a href="/downloads/AdvisorHowTo.pdf">Download the GoAbroad Network Advisor Tutorial</a></p>
</div-->
<div id="faq_wrapper">

<div id="toc_wrapper" name="toc_wrapper">
<h2>Traveler Help</h2>


<ul class="toc">
	<li><a href="#profile">How do I add/change my profile photo?</a></li>
	<li><a href="#travel">What are Travel Journals and Travel Journal Entries? How do I create one?</a></li>
	<li><a href="#video">How do I add pictures or videos to my journals?</a></li>
	<li><a href="#postcard_rss">How do I share my journals with family and friends back home?</a></li>
	<li><a href="#facebook">How do I make my journals appear on my Facebook page?</a></li>
	<li><a href="#bring_2_facebook">How do I bring my traveler profile to Facebook, MySpace, Orkut or hi5?</a></li>
	<li><a href="#messages">How do I send messages to other Travelers?</a></li>
	<?php if("GoAbroad Network" == $site): ?><li><a href="#groups">What are groups?</a></li><?php endif; ?>
	<li><a href="#subgroups">What are SubGroups?</a></li>
	<li><a href="#tags">What are Tags?</a></li>	
	<li><a href="#cantsee">I can't see the photo upload tool.</a></li>
	<li><a href="#cantselect">I can't select my photos on the upload tool.</a></li>	
	<li><a href="#cantfind">I am unable to find my iPhoto or Aperture images.</a></li>	
	<li><a href="#cantfind">I am unable to see the tool or my photo previews.</a></li>
</ul>
</div>


<div class="sections_wrapper" id="profile_wrapper">
<h2><a name="profile" id="profile" class="content_text toc_anchor"></a>How do I add/change my profile photo?</h2>
<img class="content_image" src="images/FAQ/1_prof_photo.jpg" alt="Profile Photo"/>

<p>
	To personalize and enhance your profile, you can upload a photo to appear along with your 
	profile information. To add or change your photo, you will find the option to 
	<strong>Manage Profile Photos</strong> at the top of your page, right below your profile 
	name and information. 
</p>

<p class="bottom_p"></p>

</div>




<div class="sections_wrapper">
<h2><a name="travel" id="travel" class="toc_anchor"></a>What are Travel Journals and Travel Journal Entries? How do I create one?</h2>
<img class="content_image nofloat" src="images/FAQ/2travel.jpg" alt="Journal Entries"/>

<p>
	Travel Journals are like diaries detailing a summary of experiences from a trip. Your 
	Journal is like a book where you record your experiences and trip photos, and the blog 
	entries are like pages of this book. You may add any number of blog entries to every 
	Journal, and add any number of photos to each entry. Your blog entries are sorted by 
	date; this will allow readers to read about your trip - from when you start it until you 
	end it.
</p>


<p class="position_relative" id="travel_p2">
	To create a Travel Journal, just click on <strong>+Create a New Journal.</strong> This 
	button can be found on your Passport Page, under your Profile and Journals 	tabs. If you 
	already have journals, you can find this button after the list of all your journals.
</p>
<p class="position_relative" id="travel_p3">
	<strong>Travel Journal Entries</strong> are like journal sections or installments 
	describing a certain leg of a trip. These are like pages of your Travel Journal. 
	To create a travel journal entry, you first need to add a Travel Journal (see 
	<strong>What are Travel Journals?</strong> in the previous section). After adding a 
	Travel Journal book, you will get a page that lets you add your first entry. To add your
	succeeding entries, you will find the button to <strong>Add an Entry</strong> right above
	the list of your entries for that journal. If you are viewing a journal entry, you can
	Add a Journal Entry to the journal you 	are viewing by clicking on the button right
	above the list of your entries.
</p>

<p class="bottom_p"></p>

<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>





<div class="sections_wrapper">
<h2><a name="video" id="video" class="toc_anchor"></a>How do I add pictures or videos to my journals?</h2>

<img class="content_image" src="images/FAQ/3_video1.jpg"/>
<img class="content_image" id="video2" src="images/FAQ/3_video2.jpg" alt="Add Video"/>
<p>
	After writing your journal entry and saving it, you will see your saved entry as it would 
	appear on your <?php echo $site?> page. There is a link to <strong>Upload
	Photos</strong> right beside your entry title and details. You may add captions and rotate your photos 
	after they have been uploaded. To add more photos, click <strong>Manage entry photos</strong>, which is 
	right below your photos for that journal.</p>
	
<p> <strong>To add videos to your journal</strong>, you will find the link to 
	<strong>Add	a Video</strong> on the	right side of your journal entry, after the Photos and Highlights. 
	You may embed your Google Video or YouTube videos in your journal entries. These are the two
	video accounts we support at the moment and if you don't have a Google Video
	or YouTube account, you will need to create one separately. </p>
	

<p class="bottom_p" id="video_bottom_p"></p>

<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>

<div class="sections_wrapper">
<h2><a name="postcard_rss" id="postcard_rss" class="toc_anchor"></a>How do I share my journals with family and friends back home?</h2>

There are two ways to make sure your family and friends do not miss out on any of your posts: </p>

	<ol>
		<li> Send them <strong>E-postcards</strong> or	</li>
		<li> Have them subscribe to your <strong>RSS feeds</strong>. </li>
	</ol>
<img class="content_image" id="postcard1" src="images/FAQ/4_postcard.jpg" alt="E-Postcard"/>
	
<p class="p_spacing" id="postcard_p3"><em>Sending Journals as E-postcards</em><br />
	It is easy and simple to send every new entry as a <strong>Postcard</strong> to your family
	and friends. When you finish your journal entry, you will find the link to
	<strong>Send as a Postcard</strong> on the right side of your journal page, right below the map.
	Creating your postcard is simple:</p>

	<ul>
		<li>Add recipients</li>
			<p id="postcard_tip1">TIP: Adding the email addresses of your family and friends to your Address Book saves you from having to type each address every time you send a postcard. It's also a nifty way to make sure you don't miss to send your update to anyone! </li>
		<li>Select your postcard photo</li>
		<li>Write your postcard message</li>
		<li>Click Send</li>
	</ul>

	<p class="p_spacing">The recipients will get your postcard via email. You may send postcards to
	anyone - your friends on <?php echo $site?>, your Address Book contacts or
	you can type the email addresses of the people you want to send it to. We
	will send the postcard individually to protect your recipient list. They
	do not need to have <?php echo $site?> accounts to get your postcards or view
	your journals and photos.</p>

	<p>Also, anyone can leave shout-outs on your journals and let you know they
	are following your journey. (View the <a href="javascript:void(0)" onclick="window.open('/shout-out-policy.php?network=<?php echo $site?>', 'shout-out-policy', 'resizable=no,width=490,height=300,top=' + ((screen.width/2)- 1000) + ',left=20'); return false;"><?php echo $site?> Shout-Out Policy</a>)</p>
	
	<img class="content_image" id="postcard2" src="images/FAQ/4_postcard2.jpg" alt="RSS Feeds"/>
	<p class="p_spacing" id="postcard_p4"> <em>RSS Feeds</em><br />
	Your family and friends can subscribe to RSS feeds that will keep
	them posted with your new journal entries. The RSS icon to subscribe to your
	journals can be found at the top of your page, right along with your profile
	information. </p>
	
	<p class="bottom_p"></p>
	
<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>	
	
	
	
	
<div class="sections_wrapper">	
<h2><a name="facebook" id="facebook"  class="toc_anchor"></a>How do I make my journals appear on my Facebook page?</h2>

	<img class="content_image" src="images/FAQ/5_facebook.jpg" alt="Sync with Facebook"/>
	<p  class="bottom_p">On your My Passport page, go to the Journals tab; you will find the button <strong>Sync
	with Facebook</strong>. Clicking on this will take you to the 
	MyTravelJournals application page on Facebook. You may be asked to log in to your
	Facebook account. You will need to add the application if you have not previously 
	done so. Afterwards you will be able to sync your <?php echo $site?> account with your 
	MyTravelJournals application on Facebook. 	The application shows your journals and 
	links to these travel journals on <?php echo $site?>. You can add MyTravelJournals to 
	your Wall, to your Boxes tab, or as another tab on your Profile page.</p>

<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg"></a>
</div>	
	
	
	
	
<div class="sections_wrapper">	
<h2><a name="bring_2_facebook" id="bring_2_facebook" class="toc_anchor"></a>How do I bring my traveler profile to Facebook, MySpace, Orkut or hi5?</h2>
	<p><?php echo $site?> has four applications that you can add to your profile on Facebook, MySpace, Orkut and hi5. We know most of your friends are on these social networks, but <?php echo $site?> has the cool tools that let you share your travel experiences best. By adding our applications on your Facebook, MySpace, Orkut or hi5 profile, you can keep journalling on <?php echo $site?> and be confident that your journals are instantly shared with your friends on these sites.</p>
	
	
	<img class="content_image" id="bring_facebook" src="images/FAQ/6_bring_facebook.jpg" alt="Open Social Applications"/>
	<p class="p_spacing" id="bring_facebook_p2">
		
	<strong>MyTravelBio</strong><br />
	This application lets you share your most memorable travel experiences. Share the highlights of your travels and show your traveler personality by synchronizing the application with your Facebook, MySpace, Orkut or hi5 profile.</p>
	
	<ul class="widget_live widget_live_extend">
	<?php 
	foreach ($widgets['My Travel Bio'] as $container => $url): ?>

       <li class="<?php echo $containerIconClassName[$container]; ?>">
           <a href="<?php echo $url; ?>" title="Install this App in <?php echo $container;?>" target="_blank">
 				<?php echo $container;?>
           </a>
       </li>

	<?php
	endforeach;
	?>
	</ul>
	
	<p><strong>MyTravelMap</strong><br />
	Mark the countries you've been to and show your travel map on your profile. If you have travel journals, this app will show how many journals, photos and tips you have for every country. Bring this map to Facebook, MySpace, Orkut or hi5 by adding the application and synchronizing it with your <?php echo $site?> account.</p>
	
	<ul class="widget_live widget_live_extend">
	<?php 
	foreach ($widgets['My Travel Map'] as $container => $url): ?>

       <li class="<?php echo $containerIconClassName[$container]; ?>">
           <a href="<?php echo $url; ?>" title="Install this App in <?php echo $container;?>" target="_blank">
 				<?php echo $container;?>
           </a>
       </li>

	<?php
	endforeach;
	?>
	</ul>
	
	<p><strong>MyTravelPlans</strong><br />
	Connect with people traveling to the same places as you are. Share your travel plans and find out who could be traveling your way! Bring this app to Facebook, MySpace, Orkut or hi5 and synchronize to update on either site and have your changes show on both.</p>
	
	<ul class="widget_live widget_live_extend">
	<?php 
	foreach ($widgets['My Travel Plans'] as $container => $url): ?>

       <li class="<?php echo $containerIconClassName[$container]; ?>">
           <a href="<?php echo $url; ?>" title="Install this App in <?php echo $container;?>" target="_blank">
 				<?php echo $container;?>
           </a>
       </li>

	<?php
	endforeach;
	?>
	</ul>
	
	<p><strong>MyTravelJournals</strong><br />
	This application lets you bring your travel journals to Facebook, MySpace, Orkut or hi5. Share your travels with your friends - now they don't have to remember your traveler page to follow your journeys. Simply synchronize and refresh to keep the links current on your profile on these sites.</p>	
	
	<ul class="widget_live widget_live_extend">
	<?php 
	foreach ($widgets['My Travel Journals'] as $container => $url): ?>

       <li class="<?php echo $containerIconClassName[$container]; ?>">
           <a href="<?php echo $url; ?>" title="Install this App in <?php echo $container;?>" target="_blank">
 				<?php echo $container;?>
           </a>
       </li>

	<?php
	endforeach;
	?>
	</ul>
	
	<p><strong>Facebook</strong><br />	
	To synchronize, click on the icon Sync with Facebook. Clicking on this will take you to the application page on Facebook. You may be asked to log in to your Facebook account. You will need to add the applications if you have not previously done so. Afterwards you will be able to sync your <?php echo $site?> account with the applications on Facebook. You can add these applications to your Wall, to your Boxes tab, or as another tab on your Profile page.</p>	
	
	<p class="bottom_p"></p>
	
<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a></div>	





<div class="sections_wrapper">
<h2><a name="messages" id="messages" class="toc_anchor"></a>How do I send messages to other Travelers?</h2>
	
	<img class="content_image" src="images/FAQ/7_message.jpg" alt="Send a message"/>	
	<p>To send a message to another traveler, simply go to their page, where you will find 
	the link <strong>Send Message</strong> right below their profile information. You 
	will be directed to a form that will let you compose and send your message. The 
	messages you sent will be stored in your Outbox.</p>
	
	<p class="bottom_p"></p>
	
<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>


<?php if("GoAbroad Network" == $site): ?>
<div class="sections_wrapper">
<h2><a name="groups" id="groups" class="toc_anchor"></a>What are groups? </h2>
	<p>	Creating Groups lets you form online social circles with other travelers who share your interests. Anyone can create a Group and it may be for just	about any interest under the sun. You may create and/or join as many Groups as you please.</p>
	
	<p> Group features are specially tailored for travel abroad Advisors, organizations and institutions. The features were designed for ease of communication between program providers or organizers and the participants of a travel abroad program.</p>
	
	<img class="content_image" id="groups" src="images/FAQ/8_group.jpg" alt="Groups and Clubs"/>	
	<p class="p_spacing" id="groups_p4"><strong>To browse the Groups</strong> on <?php echo $site?>, simply go to
	the <a href="/group.php">GROUPS</a> tab. You may also use the keyword search to find a group.</p>
	
	<p><strong>To join Groups</strong>, you will find the link <strong>Join
	this Group</strong> below the group details. You need to be logged in to see these links.</p>
		
	<p><strong>To create Groups</strong>, go to My Passport &raquo; Groups tab. You will find the link to Create Group on the right side of the page. </p>
	
	<p class="bottom_p" id="groups_bottom_p"></p>
	
<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>	
<?php endif; ?>


<div class="sections_wrapper">
	<h2><a name="subgroups" id="subgroups" class="toc_anchor"></a>What are SubGroups? </h2>
	<p>
		SubGroups are useful for creating smaller and thus more manageable groups. Each SubGroup functions as a complete independent group with it's own messaging functions, discussion boards, and management tools. 
	</p>
	
	<p>
		We recommend creating a unique SubGroup for every physical group of participants you manage. For instance, "Study in Barcelona - Spring 2010".
	</p>	
	
	<p>
		You can group your members into smaller, more manageable subgroups. Each subgroup can be its own community within your group, with their own private messaging and discussion boards. 	
	</p>
	
	<p>Not sure you need subgroups? Consider the guide below: </p>
	
	<ul class="disc">
		<li>
			Will you be sending out targeted and specific communication to batches of members at a time? 	
		</li>
		<li>
			Will some of your members share a common experience and feel the need to connect exclusively with a group? 
		</li>
	</ul>
	
	<p>
		If you answered Yes to any of the questions above, you may need to create subgroups for more efficient management of your members. We recommend that you build these groups first before adding or inviting members.
	</p>
			
	<p class="bottom_p" id="groups_bottom_p"></p>
	
	<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>	


<div class="sections_wrapper">
	<h2><a name="tags" id="tags" class="toc_anchor"></a>What are Tags? </h2>
	
	<p>
		Assigning Tags to your SubGroups will assist in two ways. First, is  
		to organize the SubGroups into meaningful categories and second to  
		create easy navigation for your users to find the groups on the web  
		site.			
	</p>	
	
	<p>
		Start by creating a Tag simply by typing a keyword in the Tag  
		Manager. Tags should be logical single keywords that apply to your  
		types of programs such as "Study", "Volunteer", "Spain", "Summer",  
		"Peru". Notice that Tags are single words. Phrases like "Summer  
		Study in Barcelona 2010" will be broken down into 5 separate tags  
		"Summer", "Study", "in","Barcelona" and "2010". The word "in"  
		however is not a useful tag and should be removed. Once tags have  
		been created, you may then assign multiple tags to your SubGroups  
		or batches of SubGroups all at once.	
	</p>		
	
	<p>
		The list of all the tags on your web site is called the 'tag  
		cloud'. A bigger font size indicates that more subgroups are tagged  
		with that word. When a visitor clicks on a tag in the tag cloud the  
		page will display all SubGroups that are tagged with the word  
		selected. The tag cloud then narrows to show a list of all the  
		secondary tags within the group of selected SubGroups. Clicking  
		further on the tags that remain will further narrow down the search  
		to the SubGroups that have both tags selected and so on.	
	</p>
	
	<p class="bottom_p" id="groups_bottom_p"></p>
	
	<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>	





<div class="sections_wrapper">	
<h2><a name="facebook" id="cantsee"  class="toc_anchor"></a>I can't see the photo upload tool.</h2>
	<img class="content_image position_relative" id="img_cantsee" src="images/FAQ/trust.jpg" alt="Certificate"/>
	<p  class="bottom_p">The photo uploader may take a few moments to load. A prompt will pop up. Depending on what computer you are on, you may see the buttons Trust, Run or OK, please click on it to enable the uploader to work. This popup may no longer appear unless you quit and restart your browser.</p>

<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>

<div class="sections_wrapper" id="cantselect_wrapper">	
<h2><a name="facebook" id="cantselect"  class="toc_anchor"></a>I can't select my photos on the upload tool.</h2>
	<img class="content_image position_relative" id="img_cantselect" src="images/FAQ/img_cantselect.jpg" alt="export from iPhoto"/>
	<p>Your photos may be too large. We currently have a 2MB/photo limit. To reduce the size of your photos, you can export them from your photo managing application and set new dimensions during the export process in order to reduce the file size.</p>
	To export from iPhoto or Aperture, please follow these steps:
	
	<ul>		
		<li><p>On your iPhoto or Aperture gallery, select all the photos that you want to upload by holding down Ctrl or Apple ⌘ while clicking on each photo.</p></li>
		<li><p>When you have selected all the photos that you want to upload, go to File » Export.</p></li>
		<li>
			On the pop-up:
			<ol>
				<li>Click on the File Export tab</li>
				<li>Under Kind, select JPEG</li>
				<li>Under JPEG Quality, select High</li>
				<li>Under Size. select, Large</li>
				<li>Click Export.</li>
			</ol>
		<li>You will be asked to select the location that you want to save the photo to. After selecting a folder, click OK.</li>
		<li>Go back to your <?php echo $site?> page and open the photo uploader again. Select the folder that you saved your exported images to and select the images you want to upload.</li>
		</li>
	</ul>
	<p>&nbsp;</p>

<a class="top_button" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
</div>

<div class="sections_wrapper">	
<h2><a name="facebook" id="cantfind"  class="toc_anchor"></a>I am unable to find my iPhoto or Aperture images.</h2>
	<img class="content_image position_relative" id="img_cantfind" src="images/FAQ/img_cantfind.jpg" alt="drag images from iPhoto"/>
	<p class="margin-top-small">Just drag and drop!</p>
	<ul>
		
		<li>Simply drag your photos from your photo managing application and drop them onto the lower right box of the uploader - we strongly recommend this for iPhoto and Aperture users.</li>
		<li>You can also drag your photos from your Windows Explorer or Finder window.</li>
		<li>If you are unable to drop a photo, please check if the photo is within the 2 MB limit per image.</li>
	</ul>
	
	<p>Add photos to your album.</p>
	<ul>
		
		<li>On the right, select your Pictures or Photos folder, or the folder that contains the photos that you want to upload.</li>
		<li>On the photo preview, click on the green plus or drag the image to the lower right box to select it.</li>
		<li>If you are unable to drop a photo, or you do not see the green plus icon, please check if the photo is within the 2 MB limit per image.</li>
		<li>After selecting photos, click on the green arrow button to start uploading.</li>
	</ul>
	<p>&nbsp;</p>
	
	<p class="margin_bottom_large">Need more help? <a href="/feedback.php">Email us</a></p>
	<a class="top_button margin-top-small" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
		
</div>
	
<div class="sections_wrapper">	
<h2><a name="facebook" id="cantfind"  class="toc_anchor"></a>I am unable to see the tool or my photo previews.</h2>
	<img class="content_image position_relative" id="img_install" src="images/FAQ/img_install.jpg" alt="Java installation complete"/>
	<p class="bottom_p">You will need Java&trade; for the uploader to work. <a href="http://www.java.com/en/download/index.jsp">You can get a quick and free Java download here</a>. If you are on Windows and you see a Java&trade; icon on the lower right corner of your screen, simply click it and it will guide you through the installation in a few clicks.</p>
	
	<a class="top_button margin-top-large" href="#toc_wrapper"><img src="images/FAQ/top.jpg" alt="top button"/></a>
		
</div>
	
<div id="contactus_wrapper">
	<p id="contact_us">Need more help? <a href="/feedback.php">Contact us</a></p>
</div>	
	
</div>