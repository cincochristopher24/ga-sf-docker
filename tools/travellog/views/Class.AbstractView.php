<?php
require_once("Class.Template.php");
abstract class AbstractView{
	
	protected
	
	$sub_views    = array(),
	
	$contents     = NULL,
		
	$obj_template = NULL,
	
	$path         = 'travellog/views/';
		
	function setContents( $contents ){
		$this->obj_template = new Template;
		$this->contents = $contents;
		$this->obj_template->set( "contents", $contents );
		$this->obj_template->set_path( $this->path );	
	}
	
	function addSubView( AbstractView $sub_view, $name ){
		if( !array_key_exists($name,$this->sub_views) )
			$this->sub_views[$name] = $sub_view;
		else
			throw new Exception($name . ' already exists in array.');
	}
	
	function setPath($path){
		$this->path = $path;
	}
	
	abstract function render();
}
?>
