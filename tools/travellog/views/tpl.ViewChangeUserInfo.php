<? if (!$changedUserInfo) : ?>
	<div id = "change-login-info" style = "padding: 10px;">
		<h3>
			You can change your username and password here. You are only given this 
			chance to do this. So if you want to change your log-in information, 
			please do it now.
		</h3>
		<div>
			<?php if ( sizeof($changeErrors) ) : ?>
				<div class="errors">					
					<ul>
						<?php foreach ($changeErrors as $error) : ?>
							<li><?= $error ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
			<form method = "post" class = "interactive_form" name = "frmChange" onSubmit = "AssignStaffManager._change('lstTravelerId=<?=$travelerId?>&action=Change');return false;">
				<ul class="form">		
					<li>
						<label for = "txtUsername">Username <span class = "required">*</span></label>
						<input type = "text" name = "txtUsername" id = "txtUsername" size = "30" class = "text" maxlength = "50" value = "<?=$txtUsername?>" />
					<li>
					<li>
						<label for = "txtPassword">Password <span class = "required">*</span></label>
						<input type = "password" name = "txtPassword" id = "txtPassword" size = "30" class = "text" maxlength = "50" value = "<?=$txtPassword?>" />
					</li>
					<li>
						<label for = "txtConfirm">Confirm Password <span class = "required">*</span></label>
						<input type = "password" name = "txtConfirm" id = "txtConfirm" size = "30" class = "text" maxlength = "50" value = "<?=$txtConfirm?>" />
					</li>
					<li style = "padding-top: 20px;">
						<input type = "button" name = "btnChange" id = "btnChange" value = "Change" onclick = "AssignStaffManager._change('lstTravelerId=<?=$travelerId?>&action=Change')" />
						<input type = "button" name = "btnSkip" id = "btnChange" value = "Skip" onclick = "AssignStaffManager._skipEditTravelerInfo('lstTravelerId=<?=$travelerId?>&action=Skip')" />
					</li>
				</ul>
			</form>
		</div>
	</div>
<? else : ?>
	<div id = "changed_username_password" style = "padding: 10px;">
		<h3> You have changed your username and password! </h3>
	</div>
<? endif; ?>