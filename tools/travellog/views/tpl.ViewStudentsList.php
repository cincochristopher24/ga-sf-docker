<?php
	/*
	 * tpl.ViewStudentsList.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
	require_once('travellog/model/Class.Listing.php'); 
?>
<div id="admin_messages">
		<p>View : <a onclick="javascript: SLController.changeView('viewByListing');">Listing</a> | <b>Student</b></p>
	<?= $sub_views['PAGING']->render(); ?>
	<? 
		foreach( $contents['listStudentID'] as $studentID ) :
	    	$objStud = new Traveler($studentID);  
	?>
			<div class="fl_left header_mess" id="msgPack_<?= $objStud->getSendableID(); ?>">
				<? if( $contents['inquiryBox']->getNumberOfNewMessagesPerStudent($objStud->getSendableID()) > 0 ) : ?> 
					&nbsp;<?= $contents['inquiryBox']->getNumberOfNewMessagesPerStudent($objStud->getSendableID()); ?>&nbsp;
				<? endif; ?>
			</div>
			<div class="stretcher header_mess title"><?= $objStud->getTravelerProfile()->getLastName(); ?>,<?= $objStud->getTravelerProfile()->getFirstName(); ?></div>
	 	  	<?  
				foreach($contents['inquiryBox']->getListings1($objStud->getSendableID()) as $listingID) : 
					$objListing = new Listing($listingID);
		  	?>	
		  			<div class="fl_left header_mess" id="grpMsg_<?= $listingID; ?>_<?= $objStud->getSendableID(); ?>">
			  			<? if( $contents['inquiryBox']->getNumberOfNewAdminMessagesPerStudentAndListing($objStud->getSendableID(),$listingID) > 0 ) : ?> 
			  				&nbsp;<?= $contents['inquiryBox']->getNumberOfNewAdminMessagesPerStudentAndListing($objStud->getSendableID(),$listingID); ?> &nbsp;
			  			<? endif; ?>
					</div>
		  			<div class="header_mess" onclick="javascript: SLController.displayMessages('all_msg_<?= $objStud->getTravelerID()."_".$listingID; ?>',<?= $listingID; ?>,<?= $studentID; ?>,'studentView');"><?= $objListing->getListingName(); ?></div>
		  			<div id="all_msg_<?= $objStud->getTravelerID(); ?>_<?= $listingID; ?>" style="display: none" class="display"></div> 
	<?
				endforeach;
		endforeach;		
	?>					
	<?= $sub_views['PAGING']->render(); ?>

</div>