
				
		<?  
			if( $iterator->count() ):
				$iterator->rewind();
		?>	
		<ul class="users">	
			<? while( $iterator->valid() ):?>
				<li id="f_<?=$iterator->current()->getTravelerID()?>">
							<a class="thumb" href="/profile.php?action=view&amp;travelerID=<?=$iterator->current()->getTravelerID()?>" title="View Profile" class="user_thumb">
								<img alt="User Profile Photo" src="<?=$iterator->current()->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" />
							</a>
							<div class="details">	
								<strong>
									<?/*if( strlen($iterator->current()->getUserName()) > 11):?>
										<a class="username" href="/<?=$iterator->current()->getUserName()?>" title="<?= $iterator->current()->getUserName()?>"><?=substr($iterator->current()->getUserName(),0, 11) ?>...</a>
									<?else:*/?>	
										<a class="username" href="/<?=$iterator->current()->getUserName()?>" title="View Profile">
											<?= $iterator->current()->getUserName()?>
										</a>
									<?//endif;?>	
								</strong>							
								<!--span class="dark_gray"><?/*=$iterator->current()->getTravelerProfile()->getTravType()->getTravelerType()*/?></span-->
								<?if( $isOwner ):?>
									<p>
										<!--edited by ianne - 11/11/2008-->
										<a href="messages.php?act=messageTraveler&id=<?=$iterator->current()->getTravelerID()?>" class="message"><span>Send Message</span></a> |
										<a href="javascript:void(0)" onclick="removeFriend({username: '<?=$iterator->current()->getUserName()?>',friendID: '<?=$iterator->current()->getTravelerID()?>', page: <?=$current_page?>});" class="remove"><span>Remove</span></a> |
										<a href="javascript:void(0)" onclick="blockFriend({username: '<?=$iterator->current()->getUserName()?>',friendID: '<?=$iterator->current()->getTravelerID()?>', page: <?=$current_page?>});" class="block"><span>Block</span></a>
									</p>
								<?endif;?>
							</div>
						<div class="travbox_info">							
							<?php
							$journalCount = $iterator->current()->getCountTravelJournals();
							$journalEntryCount = $iterator->current()->getCountTravelLogs();

							$journals = ( $journalCount > 1)? 'Journals':'Journal';
							$entries  = ($journalEntryCount > 1)? 'Entries':'Entry';
							?>
							<p class="gray"><span><?=$journalCount?></span>  <?=$journals ?></p>
							<p class="gray"><span><?=$journalEntryCount?></span> <?=$entries ?></p>								  
						</div>
				</li>	
				<? $iterator->next(); ?>
			<? endwhile; ?>
		</ul>
		<? else: ?>
			<p class="help_text">
				<?	$site = (isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad Network"; ?>
				<span><? if (!$isOwner): ?> <?= $trav_name ?> has not added any friends yet.<? else: ?>You don't have any friends yet.<? endif; ?></span><br />
				<? if ($isOwner): ?>
					<? if (!isset($GLOBALS['CONFIG'])): ?>
						<a href="/invite.php">Send an invitation</a> to your friends to join you or 
					<?endif;?>				
					<a href="/travelers.php">Find and meet</a> other travelers who are already here.<?//=$site?></a> 
				<? endif; ?>	
			</p>
		<? endif; ?>		
		<?php
		$paging->showPagination(array('label'=>' Friends'));
		?>