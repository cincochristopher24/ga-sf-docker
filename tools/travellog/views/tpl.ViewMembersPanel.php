<?php
/**
 * Created on Apr.16.07
 *
 * @author Daphne 
 * Purpose:  template for View Members Panel (All Members, Official Members, , Pending Invites, Pending Email Invites, Pending Requests)
 */
  
?>
<? if (GroupMembersPanel::$STAFF ==  $memberTabType || GroupMembersPanel::$MODERATORS ==  $memberTabType ) : ?>	
	<?$facilitatorType = (GroupMembersPanel::$STAFF ==  $memberTabType) ? 'Staff' : 'Moderator' ?>
	<div id="group_moderator_control" ><a href="/facilitator.php?gID=<?=$group->getGroupID()?>&amp;type=<?=$facilitatorType?>">Manage <?=$facilitatorType?></a></div>
<?endif; ?>
<?if (0 < count($allMembers)) : ?>
 		<?$gID = $group->getGroupID(); ?>
		<? foreach($allMembers as $eachmember ): ?>
 			<?	if ( $eachmember instanceof Traveler) 
 					$eachprofile = $eachmember->getTravelerProfile();
				$facilitator = ''; 
				if ($group->isMember($eachmember)):
					$travelerType = 'member';
					if ($group->isStaff($eachmember->getTravelerID())): 
						$facilitator = 'Staff'; 
					elseif ($group->isModerator($eachmember->getTravelerID())): 
						$facilitator = 'Moderator';
					endif;
				elseif ($group->isInInviteList($eachmember)):
				 	$travelerType = 'invite';
				elseif ($group->isInRequestList($eachmember)):
				 	$travelerType = 'request';
				elseif ($group->isInDeniedInviteList($eachmember)):
				 	$travelerType = 'deniedinvite';
				else:
					$travelerType = 'traveler';				
				endif;
 			?>
 		
 			
	 			
	 			<?
			 	$entryJournal = array();
			 	if( $eachmember->getCountTravelJournals() != NULL && 0 < $eachmember->getCountTravelJournals() ){
		   			( 1 < $eachmember->getCountTravelJournals() ) ? $str = " Journals" : $str = " Journal";
		   			array_push($entryJournal,$eachmember->getCountTravelJournals() . $str );		
		   		}
			 	if( $eachmember->getCountTravelLogs() != NULL && 0 < $eachmember->getCountTravelLogs() ){
		   			( 1 < $eachmember->getCountTravelLogs() ) ? $str = " Entries" : $str = " Entry";
		   			array_push($entryJournal,$eachmember->getCountTravelLogs() . $str );		
		   		}
				$mySubGroupList = array();
				if (isset($subGroupList))
					$mySubGroupList = $subGroupList[$eachmember->getTravelerID()];
				$myAssignedSubGroupList = array();
				if (isset($assignedSubGroupList))
					$myAssignedSubGroupList = $assignedSubGroupList[$eachmember->getTravelerID()];
				?>			 
				
					<li class="traveler<?if ('member' != $travelerType):?> pending<?endif;?>">
						<a href="/<?= $eachprofile->getUserName() ?>" title="View Profile" class="user_thumb">
							<img src="<?=$eachprofile->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" height="37" width="37" alt="traveler's photo"  />
						</a>
						
						<div class="traveler_info">
							<h2> <!-- traveler username -->
								<a href="/<?= $eachprofile->getUserName() ?>" title="<?= $eachprofile->getUserName() ?>">
									<?= (strlen($eachprofile->getUserName()) > 24) ? substr($eachprofile->getUserName(),0, 24).'...' : $eachprofile->getUserName() ?>
									<? if (isset($isAdminLogged) && $isAdminLogged && ( strlen($eachprofile->getFirstName()) || strlen($eachprofile->getLastName()))) { 
										echo "( " . $eachprofile->getFirstName() . " " . $eachprofile->getLastName() .  " )";
									}?>
								</a>
							</h2>
							
							<? if ('member' == $travelerType || 'invite' == $travelerType || 'request' == $travelerType )  : ?>
									<? if ( isset($_SESSION['travelerID']) ): ?>
										<span class="user_stat">
										    <?
											$obj_pref = new PrivacyPreference( $eachmember->getTravelerProfile()->getTravelerID() ); 
											// if pref allows or the loggeduser is a member
											( ( $eachmember->getTravelerID() == $_SESSION['travelerID']) || ( $eachmember->isOnline() && $obj_pref->canViewOnlineStatus($_SESSION['travelerID']) ) ) ? $imgfile = 'orb_online.gif' : $imgfile = 'orb_offline.gif';
											$stat_alt = ('orb_online.gif' == $imgfile) ? 'online' : 'offline';
											?>
											<img src="/images/<?=$imgfile?>" alt="<?=$stat_alt?>" title="<?=$stat_alt?>" />
										</span>
									<? endif; ?>


									<span class="flags">
										<? if ($eachprofile->getHTLocationID()): ?>
										 <? $cID = $eachprofile->getCity()->getCountry()->getCountryID()  ?>
											<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="Travel blog from <?= $eachprofile->getCity()->getCountry()->getName(); ?>" />												
										<? endif; ?>
									</span>
							<?endif;?>													
							<div class="_" style="clear: right;"> </div>
							
							<? if (GroupMembersPanel::$ALLMEMBERS ==  $memberTabType) : ?>	
								<span class="status">
									<?php
										if ('member' == $travelerType && (isset($isAdminLogged) && $isAdminLogged ) ):
											echo 'Member';
										elseif ('invite' == $travelerType):
											echo 'Invited';
										elseif ('request' == $travelerType):
											echo 'Requesting';
										endif;							
									?>					
									<?= $facilitator ?>													
								</span>
							<?endif;?>							
													
							<!--
							<?// if (count($entryJournal)) {?>
								<span class="mem_statsprev">							
									<?//= implode(" | ",$entryJournal) ;	?>
								</span>
							<?// } ?>
						-->																	
						</div> <!-- end traveler_info -->				
						
						<? if ('member' == $travelerType || 'invite' == $travelerType) : ?>
							<div class="mem_status">
								<? if (GroupMembersPanel::$ALLMEMBERS ==  $memberTabType || GroupMembersPanel::$MEMBERS ==  $memberTabType || GroupMembersPanel::$PENDINGINVITES = $memberTabType) : ?>	
					  				<? if (isset($isAdminLogged) && $isAdminLogged) : ?>
					  						<?= ($eachmember->getTravelerID() == $group->getAdministratorID()) ? '(admin)' : '' ?>
									<?  endif; ?>
									<div id = "statusPanel_member_group_list_<?=$eachprofile->getTravelerID()?>" class = "widePanel" style = "display: none;">
										<p class = "loading_message">
											<span id = "imgLoading">
												<img alt = "Loading" src = "/images/loading_small.gif"/>
											</span>
											<em id = "statusCaption">Loading please wait...</em>
										</p>
									</div>

									<div class="member_grouplist" id="display_member_group_list_<?=$eachprofile->getTravelerID()?>">
										<?php
											// if admin is logged in and group is ProWorld or ProWorld's sub group, show password
											// $eachmember->getPassword() returns null... so $eachprofile->getTraveler()->getPassword() was used =(
											if( 'member' == $travelerType && isset($isAdminLogged) && $isAdminLogged && (245 == $gID || (!is_null($parentGroup = $group->getParent() ) && 245 == $parentGroup->getGroupID())) && (GroupMembersPanel::$ALLMEMBERS ==  $memberTabType || GroupMembersPanel::$MEMBERS ==  $memberTabType) ){
												echo '<div>Password: ' . $eachprofile->getTraveler()->getPassword() . '</div>';
											}
										?>
										<p>
											<?	if (count($mySubGroupList) > 0) :
													echo ('<strong>Group Affiliation:</strong> ');
													echo implode(", ",$mySubGroupList);											
												endif;
											?>
											<?	if (count($myAssignedSubGroupList) > 0) :
													echo (count($myAssignedSubGroupList) > 1 ) ? ' <strong>Group Invites:</strong> ' : ' <strong>Group Invite:</strong> ' ;	
													echo implode(", ",$myAssignedSubGroupList);
												endif;
											?>
										</p>
									</div>

								<? endif; ?>
							</div> <!-- end mem_status -->							
						<? endif; ?>						
						
						<div class="clear" > </div>
						
						<div class="options_box">						
								<? if (isset($isAdminLogged) && $isAdminLogged ) { ?>
									<span class="mem_option">	
								
										<? if ('member' == $travelerType && $eachmember->getTravelerID() != $group->getAdministrator()->getTravelerID()) : ?>
											<a href="members.php?mode=remove&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$gID?>">Remove</a>
											<?if (GroupMembersPanel::$STAFF ==  $memberTabType || GroupMembersPanel::$MODERATORS ==  $memberTabType ) :?>
												| <a href="facilitator.php?mode=remove<?=$facilitatorType?>&amp;tID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$gID?>"> Remove as <?=$facilitatorType?></a>
											<? endif; ?>
										<? elseif ('invite' == $travelerType) : ?>
											<a href="javascript:void(0)" onclick="manager._setPanelNum(1);manager._cancelName('mode=cancelName&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;grpID=<?=$gID?>&amp;viewType=8');">Cancel Invitation</a>
										<? elseif ('request' == $travelerType): ?>
											<a href="members.php?mode=accept&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$gID?>" >Accept</a> |
											<a href="members.php?mode=deny&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$gID?>">Deny</a>
										<? elseif ('deniedinvite' == $travelerType): ?>
											<a href="members.php?mode=invite&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$gID?>&amp;confirm" >Invite Again</a>
											<a href="members.php?mode=delete&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$gID?>" >| Cancel</a>
										<? endif; ?>
									</span>
														
									<span class="mem_option">
										<? if (isset($isAdminLogged) && $isAdminLogged && !$group->isSubGroup() && 0 < $countSubGroups) {?>

											<a onclick="manageMembers.setAction('showGroups'); manageMembers.setTravelerId(<?=$eachmember->getTravelerID()?>); manageMembers.showSubGroupsBox()" href="javascript:void(0)">Manage</a>

										<? } ?>
									</span>														
								<? } ?>	
								<div class="clear" > </div>
						</div>							
								<div class="clear" > </div>											
					</li>
			 	  	
			 	
	 	<? endforeach; ?>
		
 		<? if ( $paging->getTotalPages() > 1 ):?>
			<? $start = $paging->getStartRow() + 1; 
			   $end = $paging->getEndRow(); 
			?>
			<? $paging->showPagination() ?>
		<? endif; ?>
			 		 
<? else : ?>
  <p class="help_text"><span>  	
  	 <? if (GroupMembersPanel::$ALLMEMBERS ==  $memberTabType){ 		  	   
	  	   echo 'No Members yet.';
	 	} 
	 	
	 	if (GroupMembersPanel::$MEMBERS == $memberTabType) {
	 	  echo 'No Members yet.';
	 	} 
	
		if (GroupMembersPanel::$STAFF == $memberTabType) {
	 	    echo 'No Assigned Staff yet.'; 
	 	}
	
	 	if (GroupMembersPanel::$PENDINGINVITES == $memberTabType && !$hasEmailInviteList) {
		    echo 'No Pending Invites.';
	 	} 
	 	
	 	if (GroupMembersPanel::$PENDINGREQUESTS == $memberTabType) {
		    echo 'No Pending Requests.';
	 	} 
	 
	 	if (GroupMembersPanel::$MEMUPDATES == $memberTabType) {
		    echo 'No Membership Updates.';
	 	} 
	 
	 	if (GroupMembersPanel::$DENIEDINVITES == $memberTabType) {
		    echo 'No Denied Invites.';
	 	} 
	 	
	  ?>
	</span>	 
  </p>
<? endif; ?>

<?if (isset($hasEmailInviteList) && $hasEmailInviteList):
	echo $emailInviteList;
endif;?>