<?php
/*
 * Created on 09 27, 2006
 * @author Kerwin Gordo
 * Purpose: template for privacy settings of a traveler
 */
 
	Template::setMainTemplateVar('layoutID', 'page_editprof');

 require_once('travellog/model/Class.PrivacyPreferenceType.php');
 require_once('travellog/model/Class.PrivacyPreference.php');
 require_once('travellog/model/Class.Traveler.php');

/**
Template::includeDependentJs ( "/js/jquery-1.1.4.pack.js"           );

Template::includeDependentCss( "/min/f=css/Thickbox.css"                  );
Template::includeDependentCss( "/lytebox/lytebox.css"               );

Template::includeDependentJs (
				array(
					"/lytebox/lytebox.js",
					"/js/interface/interface.js",
					"/js/interface/source/idrag.js",
					"/js/interface/source/idrop.js",
					"/js/interface/source/isortables.js",
					"/js/ajaxfileupload.js",
					"/js/jeditable.js"
				));
Template::includeDependentJs ( "/js/yahooUI/carousel/yahoo-dom-event.js"           );
Template::includeDependentJs ( "/js/yahooUI/carousel/utilities.js"           );
Template::includeDependentJs ( "/js/yahooUI/carousel/dragdrop-min.js"           );
Template::includeDependentJs ( "/js/yahooUI/carousel/container_core-min.js"           );
Template::includeDependentJs ( "/js/yahooUI/carousel/carousel.js"           );

Template::includeDependentCss( "/js/yahooUI/carousel/carousel.css"               );

Template::includeDependentJs (array("/js/thickbox.js","/js/PhotoUILayout.js"));



$photo_script =<<<BOF
	<script type = "text/javascript">
		jQuery.noConflict();
		PhotoService = new mPhotoService('profile', $travelerID, $travelerID);
		PhotoService.setLayout(1); 
		//PhotoService.setisOwner(1); 

	</script>
BOF;

Template::includeDependent($photo_script); 
*/
?>

<? 
	
	/****************************************************************************
	 * edits of neri:
	 * 		displays the profile component: 						11-06-08 
	 * 		disregarded displaying of resume and online status:		11-18-08
	 * 		added li for receiving GA updates:						01-08-09 
	 * 		added li for receiving notifications from group
	 * 			and subgroup discussions							01-26-09 
	 ****************************************************************************/
	
	echo $profile->render(); 
	$snav->show(); 
?>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
			<h1>Edit Privacy Preferences</h1>
			<div class="section_small_details">
				You can control what information shows up on your profile and set who will be able to see them.			
			</div>
			<br />
			
			<div class="content">
				
				<? if (isset($message)) : ?>
					<div class="confirmation">
					    <p><?=$message ?></p>
					</div>
				<? endif; ?>
				
				<form name="privacyform" action="privacy.php?action=updateprivacy" method="post" class="interactive_form">
					
				<ul class="form">
					
					<li>
						<fieldset class="choices">
							<legend>Name</legend>
							<ul>
								<li><input type="radio" name="names" value="1" <? if ($pp->showNamesTo() == PrivacyPreference::$SHOW_EVERYONE) echo 'checked="checked"'  ?> id="names_1" /><label for="names_1">Show to all</label></li>
								<li><input type="radio" name="names" value="2" <? if ($pp->showNamesTo() == PrivacyPreference::$FRIENDS) echo 'checked="checked"'  ?> id="names_2"  /><label for="names_2">Show to my friends</label></li>
								<li><input type="radio" name="names" value="3" <? if ($pp->showNamesTo() == PrivacyPreference::$GROUPMATES) echo 'checked="checked"'  ?> id="names_3"  /><label for="names_3">Show to my groupmates</label></li>
								<li><input type="radio" name="names" value="4" <? if ($pp->showNamesTo() == PrivacyPreference::$FRIENDS_AND_GROUPMATES) echo 'checked="checked"'  ?> id="names_4"  /><label for="names_4">Show to my friends and groupmates</label></li>
								<li><input type="radio" name="names" value="0" <? if ($pp->showNamesTo() == PrivacyPreference::$NO_ONE) echo 'checked="checked"'  ?> id="names_5" /><label for="names_5">Don't show</label></li>
							</ul>
						</fieldset>
					</li>
					
					<li>
						
						<fieldset class="choices">
							<legend>Email</legend>
							<ul>
								<li><input type="radio" name="email" value="1" <? if ($pp->showEmailTo() == PrivacyPreference::$SHOW_EVERYONE) echo 'checked="checked"'  ?> id="email_1" /><label for="email_1">Show to all</label></li>
								<li><input type="radio" name="email" value="2" <? if ($pp->showEmailTo() == PrivacyPreference::$FRIENDS) echo 'checked="checked"'  ?> id="email_2"   /><label for="email_2">Show to my friends</label></li>
								<li><input type="radio" name="email" value="3" <? if ($pp->showEmailTo() == PrivacyPreference::$GROUPMATES) echo 'checked="checked"'  ?> id="email_3"  /><label for="email_3">Show to my groupmates</label></li>
								<li><input type="radio" name="email" value="4" <? if ($pp->showEmailTo() == PrivacyPreference::$FRIENDS_AND_GROUPMATES) echo 'checked="checked"'  ?> id="email_4"  /><label for="email_4">Show to my friends and groupmates</label></li>
								<li><input type="radio" name="email" value="0" <? if ($pp->showEmailTo() == PrivacyPreference::$NO_ONE) echo 'checked="checked"'  ?> id="email_5" /><label for="email_5">Don't show</label></li>
							</ul>
						</fieldset>
					</li>
					
					
					<li>
						<fieldset class="choices">
							<legend>My Groups</legend>
							<ul>
								<li><input type="radio" name="groups" value="1" <? if ($pp->showGroupsTo() == PrivacyPreference::$SHOW_EVERYONE) echo 'checked="checked"'  ?> id="groups_1"  /><label for="groups_1">Show to all</label></li>
								<li><input type="radio" name="groups" value="2" <? if ($pp->showGroupsTo() == PrivacyPreference::$FRIENDS) echo 'checked="checked"'  ?> id="groups_2"  /><label for="groups_2">Show to my friends</label></li>
								<li><input type="radio" name="groups" value="3" <? if ($pp->showGroupsTo() == PrivacyPreference::$GROUPMATES) echo 'checked="checked"'  ?> id="groups_3"  /><label for="groups_3">Show to my groupmates</label></li>
								<li><input type="radio" name="groups" value="4" <? if ($pp->showGroupsTo() == PrivacyPreference::$FRIENDS_AND_GROUPMATES) echo 'checked="checked"'  ?> id="groups_4"  /><label for="groups_4">Show to my friends and groupmates</label></li>
								<li><input type="radio" name="groups" value="0" <? if ($pp->showGroupsTo() == PrivacyPreference::$NO_ONE) echo 'checked="checked"'  ?> id="groups_5"  /><label for="groups_5">Don't show</label></li>			
							</ul>
						</fieldset>
					</li>
					
					<li>
						<fieldset class="choices">
							<legend>My Friends</legend>
							<ul>
								<li><input type="radio" name="friends" value="1" <? if ($pp->showFriendsTo() == PrivacyPreference::$SHOW_EVERYONE) echo 'checked="checked"'  ?> id="friends_1"  /><label for="friends_1">Show to all</label></li>
								<li><input type="radio" name="friends" value="2" <? if ($pp->showFriendsTo() == PrivacyPreference::$FRIENDS) echo 'checked="checked"'  ?> id="friends_2"  /><label for="friends_2">Show to my friends</label></li>
								<li><input type="radio" name="friends" value="3" <? if ($pp->showFriendsTo() == PrivacyPreference::$GROUPMATES) echo 'checked="checked"'  ?> id="friends_3"  /><label for="friends_3">Show to my groupmates</label></li>
								<li><input type="radio" name="friends" value="4" <? if ($pp->showFriendsTo() == PrivacyPreference::$FRIENDS_AND_GROUPMATES) echo 'checked="checked"'  ?> id="friends_4"  /><label for="friends_4">Show to my friends and groupmates</label></li>
								<li><input type="radio" name="friends" value="0" <? if ($pp->showFriendsTo() == PrivacyPreference::$NO_ONE) echo 'checked="checked"'  ?> id="friends_5"  /><label for="friends_5">Don't show</label></li>			
							</ul>
						</fieldset>
					</li>
					<li>
						<fieldset class="choices">
							<legend>Calendar</legend>
							<ul>
								<li><input type="radio" name="calendar" value="1" <? if ($pp->showCalendarTo() == PrivacyPreference::$SHOW_EVERYONE) echo 'checked="checked"'  ?> id="calendar_1"  /><label for="calendar_1">Show to all</label></li>
								<li><input type="radio" name="calendar" value="2" <? if ($pp->showCalendarTo() == PrivacyPreference::$FRIENDS) echo 'checked="checked"'  ?> id="calendar_2"  /><label for="calendar_2">Show to my friends</label></li>
								<li><input type="radio" name="calendar" value="3" <? if ($pp->showCalendarTo() == PrivacyPreference::$GROUPMATES) echo 'checked="checked"'  ?> id="calendar_3"  /><label for="calendar_3">Show to my groupmates</label></li>
								<li><input type="radio" name="calendar" value="4" <? if ($pp->showCalendarTo() == PrivacyPreference::$FRIENDS_AND_GROUPMATES) echo 'checked="checked"'  ?> id="calendar_4"  /><label for="calendar_4">Show to my friends and groupmates</label></li>
								<li><input type="radio" name="calendar" value="0" <? if ($pp->showCalendarTo() == PrivacyPreference::$NO_ONE) echo 'checked="checked"'  ?> id="calendar_5"  /><label for="calendar_5">Don't show</label></li>						
							</ul>
						</fieldset>
					</li>
					
					
					<h1 class="clear">Email Notifications</h1>
					<div class="section_small_details">
						You will get notified via email of any event, post or activity on the site that may involve you or need your attention. You can control the notifications that you receive below.
					</div>											
				
					
					<li class="full_block">
						<fieldset class="choices">
							<legend style="margin:0;"> <?= $network ?> Updates</legend>
							<div class="item_details">We send important communications about new features, promotions, contests or changes on the site. </div>
							<ul>
								<li><input type="radio" name="updates" value="5" <? if ($pp->getModeOfReceivingUpdates() == PrivacyPreference::$RECEIVE) echo 'checked="checked"' ?> id="updates_1" /><label for="updates_1">Yes, send me an email</label></li>
								<li><input type="radio" name="updates" value="6" <? if ($pp->getModeOfReceivingUpdates() == PrivacyPreference::$DONT_RECEIVE) echo 'checked="checked"' ?> id="updates_2" /><label for="updates_2">No, thank you.</label></li>
							</ul>
						</fieldset>
					</li>
					
					<? if ($displayDiscussionThreadNotification): ?> 
						<li class="full_block">
							<fieldset class="choices">
								<legend> Main Group Discussion Board</legend>
								<div class="item_details">We will notify you of any new post in a thread that you participated in.</div>
								<ul>	
									<li><input type="radio" name="coparticipants_notification" value="7" <? if ($pp->getModeOfReceivingNotificationFromCoParticipants() == PrivacyPreference::$NOTIFIED_BY_CO_PARTICIPANTS) echo 'checked="checked"' ?> id="coparticipants_notification_1" /><label for="coparticipants_notification_1">Yes, send me an email</label></li>
									<li><input type="radio" name="coparticipants_notification" value="8" <? if ($pp->getModeOfReceivingNotificationFromCoParticipants() == PrivacyPreference::$NOT_NOTIFIED_BY_CO_PARTICIPANTS) echo 'checked="checked"' ?> id="coparticipants_notification_2" /><label for="coparticipants_notification_2">No, thank you.</label></li>
								</ul>
							</fieldset>
						</li>
					<? endif; ?>
					
					<? if ($displayActivityNotification): ?>
						<li class="full_block">
							<fieldset class="choices">
								<legend> Sub Group Discussion Board Activities </legend>
								<div class="item_details">We will notify you of all activity on your group discussion board.</div>
								<ul>	
									<li><input type="radio" name="activity_notification" value="9" <? if ($pp->getModeOfReceivingNotificationFromSubgroupDiscussionActivities() == PrivacyPreference::$NOTIFIED_BY_ALL_ACTIVITIES) echo 'checked="checked"' ?> id="activity_notification_1" /><label for="activity_notification_1">Yes, send me an email</label></li>
									<li><input type="radio" name="activity_notification" value="10" <? if ($pp->getModeOfReceivingNotificationFromSubgroupDiscussionActivities() == PrivacyPreference::$NOT_NOTIFIED_BY_ALL_ACTIVITIES) echo 'checked="checked"' ?> id="activity_notification_2" /><label for="activity_notification_2">No, thank you.</label></li>
								</ul>
							</fieldset>
						</li>
					<? endif; ?>
					
					<!--li>
						<fieldset class="choices">
							<legend>Mail Notifications  (to entries in my Address Book)</legend>
							<ul>
								<li><input type="checkbox" name="notifynewjournalentry"	 <?/* if ($pp->getNotifyOnNewJE()) echo 'checked="checked"' ?> id="notifynewjournalentry"  /><label for="notifynewjournalentry">Notify on new journal entry</label></li>
								<li><input type="checkbox" name="notifyeditjournalentry" <? if ($pp->getNotifyOnEditJE()) echo 'checked="checked"' ?> id="notifyeditjournalentry"	 /><label for="notifyeditjournalentry">Notify on journal entry edits</label></li>
								<li><input type="checkbox" name="notifynewphoto"		 <? if ($pp->getNotifyOnNewPhoto()) echo 'checked="checked"' */?>	 id="notifynewphoto" /><label for="notifynewphoto">Notify on new photo</label></li>
							</ul>
						</fieldset>
					</li-->
					<!--li>
						<fieldset class="choices">
							<legend>Resume</legend>
							<ul>
								<li><input type="radio" name="resume" value="1" <? /*if ($pp->showResumeTo() == PrivacyPreference::$SHOW_EVERYONE) echo 'checked="checked"'  ?> id="resume_1"  /><label for="resume_1">Show to all</label></li>
								<li><input type="radio" name="resume" value="2" <? if ($pp->showResumeTo() == PrivacyPreference::$FRIENDS) echo 'checked="checked"'  ?> id="resume_2"  /><label for="resume_2">Show to my friends</label></li>
								<li><input type="radio" name="resume" value="3" <? if ($pp->showResumeTo() == PrivacyPreference::$GROUPMATES) echo 'checked="checked"'  ?> id="resume_3"  /><label for="resume_3">Show to my groupmates</label></li>
								<li><input type="radio" name="resume" value="4" <? if ($pp->showResumeTo() == PrivacyPreference::$FRIENDS_AND_GROUPMATES) echo 'checked="checked"'  ?> id="resume_4"  /><label for="resume_4">Show to my friends and groupmates</label></li>
								<li><input type="radio" name="resume" value="0" <? if ($pp->showResumeTo() == PrivacyPreference::$NO_ONE) echo 'checked="checked"'  ?> id="resume_5"  /><label for="resume_5">Don't show</label></li>						
							</ul>
						</fieldset>
					</li>
					
					<li>
						<fieldset class="choices">
							<legend>Online Status</legend>
							<ul>
								<li><input type="radio" name="online" value="1" <? if ($pp->showOnlineTo() == PrivacyPreference::$SHOW_EVERYONE) echo 'checked="checked"'  ?> id="online_1"  /><label for="online_1">Show to all</label></li>
								<li><input type="radio" name="online" value="2" <? if ($pp->showOnlineTo() == PrivacyPreference::$FRIENDS) echo 'checked="checked"'  ?> id="online_2"  /><label for="online_2">Show to my friends</label></li>
								<li><input type="radio" name="online" value="3" <? if ($pp->showOnlineTo() == PrivacyPreference::$GROUPMATES) echo 'checked="checked"'  ?> id="online_3"  /><label for="online_3">Show to my groupmates</label></li>
								<li><input type="radio" name="online" value="4" <? if ($pp->showOnlineTo() == PrivacyPreference::$FRIENDS_AND_GROUPMATES) echo 'checked="checked"'  ?> id="online_4"  /><label for="online_4">Show to my friends and groupmates</label></li>
								<li><input type="radio" name="online" value="0" <? if ($pp->showOnlineTo() == PrivacyPreference::$NO_ONE) echo 'checked="checked"'*/  ?> id="online_5"  /><label for="online_5">Don't show</label></li>			
							</ul>
						</fieldset>
					</li-->					
				</ul>

					<input type="submit" value="Update Privacy Settings" class="submit button_v3" />

				</form>
			</div>
	</div>
	<div id="narrow_column">
		<div class="helpextra"></div>
		<div class="helpbox" id="quick_tasks">
				<div id="block_users">
						<h2>Block Unwanted Users</h2>
						<p>
							<?=$privacy_blockedusers?>											
						</p>
						
						<? if (isset($error)) : ?>
							<div class="error_notice">
								<span><?=$error ?></span>
							</div>
						<? endif; ?>
						
						<form id="frmBlock" action="privacy.php?action=block" method="post" >
							<label for="username">Username:</label>
							<input type="text" name="username" class="text" />
							<span class="actions"><input id="blockUserForm" type="submit" class="submit" value="Block" onclick="return false;" /> </span>
						</form><br/>				
						
						<? if (isset($blockedUsers)) : ?>
							<h3>
								<strong>Blocked Users</strong>
								<span id="fr_loader" style="display:none;">
									<img alt="Loading" src="/images/loading_small.gif" width="15" height="15" />
								</span>
							</h3>
							<ul id="blocked_users" style="padding: 0px;">
								<? $i = 0;
								   foreach ( $blockedUsers as $blocked_user ): ?>
									<?php if($i == 5): ?>
										</ul><ul class="hid_blocked_users" style="display:none;">	
									<?php endif; ?>	
									<li id="li_<?=$blocked_user->getUserName()?>" style="overflow:hidden;" >																
										<a style="float:left;width:50px;" title="View Profile" href="/<?php echo $blocked_user->getUserName()?>">
											<img style="padding-top:4px;" alt="User Profile Photo" src="<?=$blocked_user->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" width="37" height="37" />
										</a>
										<div>								
											<strong><a style="font-size: 12px;" title="View Profile" class="username"  href="/<?php echo $blocked_user->getUserName()?>">
												<?=$blocked_user->getUserName()?>
											</a></strong>
											<p>
												<a style="font-size: 11px;" href = "javascript:void(0)" onclick="unblockUser('<?=$blocked_user->getUserName()?>');return false;"><span>Unblock</span></a>
											</p>
										</div>	
									</li>
								<? $i++;
								   endforeach; ?>
							</ul>
							<div style="margin-top:2px;font-size:10px;padding-bottom:5px;overflow:auto;width:100%;">	
								<?if(count($blockedUsers) > 5):?>
									<a href="javascript:void(0)" class="blockedUsers more" style="float:right;text-decoration:none;">+ view more</a>
								<?endif;?>
							</div>
						<? endif; ?>	
				</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	(function($){
		var use_this = $('ul.hid_blocked_users').height()/2;
		unblockUser = function(username){
			var doUnblock = function(){
				$.ajax({
					url: 'privacy.php?action=unblock&username='+username,
					type: 'POST',
					beforeSend: function(){
						$('#fr_loader').show();
					},
					success: function(html){
						if(html == ""){
							$('#li_'+username).remove();
							if($('.hid_blocked_users li').length <= 0){
								$('a.blockedUsers').hide();
							}
							CustomPopup.initPrompt('You have successfully removed '+username+' from your blocked users list.');
						}else{
							CustomPopup.initPrompt(html, 'Error!', 'OK');
						}	
						CustomPopup.createPopup();
					},
					complete: function(){
						$('#fr_loader').hide();
					}
				});
			}
			
			CustomPopup.initialize('Unblock '+username+'?','Are you sure you want to remove '+username+' from your blocked users list?',doUnblock,'Yes','1');
			CustomPopup.setJS(true);
			CustomPopup.createPopup();
		}
		
		$(document).ready(function(){
			$('#blockUserForm').click(function(){
				var doBlock = function(){
					$('#frmBlock').submit();
				};
				CustomPopup.initialize('Block User?','Are you sure you want to block this user?',doBlock,'Yes','1');
				CustomPopup.setJS(true);
				CustomPopup.createPopup();
			});
			$('a.blockedUsers').click(function(){
				$('ul.hid_blocked_users').css({'height': use_this});
				if($(this).is('.more')){
					$('ul.hid_blocked_users').slideDown('slow');
					$(this).html('- view less');
				}else{
					$('ul.hid_blocked_users').slideUp('slow');
					$(this).html('+ view more');
				}
				$(this).toggleClass('more');
			});
		});
	})(jQuery);
</script>