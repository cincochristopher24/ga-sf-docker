<?php
/*
	Filename: 	 tpl.ViewTravelPlansMain.php formerly tplViewTravelSchedule.php
	Author:		 Jonas Tandinco
	Purpose:  	 view for Travel Scheduler application
	Date created: Jul/11/2007

	EDIT HISTORY:
 
	Jul/17/2007 by Jonas Tandinco
		1.	changed the order of status. before => (wish, ongoing, done, planned)
			now => (ongoing, planned, done, wish)

	Jul/23/2007 by Jonas Tandinco
		1.	programmed the order of countries to be in the sequence the user
			chose them.

	Aug/08/2007 by Jonas Tandinco
		1.	removed unnecesarry fields according to new specs

	Aug/09/2007 by Jonas Tandinco
		1.	temporarily disabled previous css
		2.	removed link for editing a travel

	Sep/10/2007 by Jonas Tandinco
		1.	removed status of a travel
		2.	removed destinations from the top-level
		3.	added the destination, description, and dates under a travel

	Sep/18/2007 by Jonas Tandinco
			1.	made sure start date is displayed only when the start date is specified for a travel
 	
	Sep/20/2007 by Jonas Tandinco
		1.	renamed hide destinations to less and show destinations to more
		2.	reprogrammed behavior of more and less to always display the first 2 travels
		
	Sep/21/2007 by Jonas Tandinco
		1.	added htmlentities() to input text boxes and text area to prevent XSS

	Sep/25/2007 by Jonas Tandinco
		1.	only allowed delete of travel destinations if there are more than 1 set destinations
		
	Oct/09/2007 by Jonas Tandinco
		1.	separated past travels so a separate design for it could be implemented
		
	Oct/18/2007 by Jonas Tandinco
		1.	renamed "Countries I want to travel" to Travel Wishlist
		2.	removed "View More" and "View Less"
		
	Oct/22/2007 by Jonas Tandinco
		1.	added isPast variable to show "See who else is going" for non-past travels
		
	Nov/13/2007 by Jonas Tandinco
		1.	renamed filename
		2.	modified links to point to travelplans.php
*/
?>

<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	if (isset($_GET['travelerID'])) {
		Template::setMainTemplateVar('page_location', 'Travelers');
	} else {
		Template::setMainTemplateVar('page_location', 'My Passport');
	}
	Template::includeDependentCss('/css/travel_todo_style.css');
?>

<?php $subNavigation->show() ?>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
		<div class="section">
			<div class="head_left">
				<p class="head_right"></p>			
			</div>
			<div class="content">
				<div id="ganet_app_container_list">
					<?php if (count($travel_wishes) + count($pastTravels)) : ?>
						<?php if (count($travel_wishes)) : ?>
							<div id="normal_travels">
								<?php $isPast  = FALSE ?>
								<?php $travels = $travel_wishes ?>
								<?php require 'tpl.IncTravelPlanMain.php' ?>
							</div>
						<?php endif; ?>
						<?php if (count($pastTravels)) : ?>
							<div id="past_travels">
								<p style="border-bottom: 1px solid rgb(204, 204, 204); font-weight: bold; margin-top: 10px; padding-bottom: 3px;">PAST TRAVELS</p>
								<?php $isPast  = TRUE ?>
								<?php $travels = $pastTravels ?>
								<?php require 'tpl.IncTravelPlanMain.php' ?>
							</div>
						<?php endif; ?>
					<?php elseif ($owner) : ?>
						<p><?php echo "You haven't listed your travels yet." ?></p>
						<br />
						<p><?php echo "List down your travel plans and find out who could be traveling your way!" ?></p>
						<br />
						<p><?php echo "List down your past travels-- show how well traveled you are and help make the GoAbroad Network community more useful to travelers like you. You can help travelers who need tips about traveling to the places you've been to!" ?></p>
					<?php endif; ?>
				</div>
				<div class="clear"></div>
				<div>
					<!-- Countries I want to travel to section -->
					<?php if ($owner || count($countriesInterested['countryIDs'])) : ?>
					<div>
						<div style="border: 1px solid #e6e6e6; background-color: #fafafa; padding: 10px;">
							<p style="color: #777777">
								<span style="font-weight: bold; color: #60718B;">Travel Wishlist(<?php echo count($countriesInterested['countryIDs']) ?>):</span>
								<?php echo join(', ', $countriesInterested['countryNames']) ?>
							</p>
						</div>
						<?php if ($owner) : ?>
							<div id="show_countries" style="padding: 5px 0 0 10px">
								<a href="javascript:void(0)" onclick="showCountries()">Edit Country List</a>
							</div>

							<div id="country_list" style="display:none; padding: 10px 0 0 0;">
								<form method="post" action="/travelplans.php?action=setcountries">
									<p style="padding: 0 0 5px 10px">What countries are you interested in traveling to?</p>
									<div style="overflow: auto; height: 400px; border: 1px solid #e6e6e6; padding: 0 0 0 5px">
										<?php foreach($countryList as $country) : ?>
											<input name="sel_countries[]" type="checkbox" value="<?php echo $country->getCountryID() ?>" 
												<?php if (in_array($country->getCountryID(), $countriesInterested['countryIDs'])) : ?>
													checked="checked"
												<?php endif; ?>
											/>
											<?php echo $country->getName() ?>
											<br />
										<?php endforeach; ?>
									</div>
									<div style="padding: 10px 0 0 0">
										<input type="submit" value="Save changes" />
										<input type="button" value="Cancel" onclick="hideCountries()">
									</div>
								</form>
							</div>
						<?php endif; ?>
					</div>
					<?php endif; ?>
					<!-- Countries I want to travel to section ends here! -->
				</div>
			</div>
			<div class="foot"></div>
		</div>
	</div>
	<div id="narrow_column">
		<div class="section" id="quick_tasks">
			<?php if (isset($_SESSION['travelerID'])) : ?>
				<h2 id="trav_heading"><span>MY TRAVELS</span></h2>
				<div class="content">				
					<div class="widget_actions">
						<?php if (!$owner) : ?>
								<a href="/travelplans.php" class="button">View My Travels</a>
						<?php else : ?>
								<a href="/travelplans.php?action=add" class="button">Add a Travel</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="foot"></div>
			<?php endif; ?>
		</div>
	</div>
</div>

<script type="text/javascript">
/////////////////////////////////////// SHOW HIDE DESTINATIONS //////////////////////////////////////////////////
function showCountries() {
	Element.show($('country_list'));
	Element.hide($('show_countries'));
}

function hideCountries() {
	Element.hide($('country_list'));
	Element.show($('show_countries'));
}
</script>