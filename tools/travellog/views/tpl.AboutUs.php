<!-- dominique aguilos tuesday jan 20 -->

<div class="image_container" id="collage">
	<img src="/images/jpg/collage2.jpg" alt="images from travelers" />	
</div>


<div id="desc_container">
<p id="p1">GoAbroad Network is a travel community that lets you connect with family, friends and travelers worldwide <br /> through travel journals, photos, maps and other applications.
</p>

<p id="p2"> Whether you are on a study abroad trip, a volunteer stint, a business trip or a tour - GoAbroad Network will make it easy for you to share your journeys through unlimited, FREE travel journal space. Add unlimited photos and videos to your journals and send them off to family and friends as e-postcards. Your family and friends can also sign up for RSS feeds and read your journals every time you post. You can even take your journals to your Facebook profile with our Facebook Apps. Meet and make friends with other travelers on the site: send messages, share travel tips and form clubs with those who share your interests. The diverse journals on the site are valuable help on your future travel planning, and the community is a great resource for practical and experienced travel advice.
</p>
</div>

<div id="infobox">

	<h2 id="infobox_h2">Learn more about:</h2>

	
	<ul id="infobox_list">
		<li><a href="http://www.goabroad.net/feature.php">What's new on GoAbroad.net</a></li>
		<li><a href="http://www.goabroad.net/faq.php">What you can do on GoAbroad.net</a></li>
		<li><a href="http://www.goabroad.net/faq.php#bring_2_facebook">Our Facebook applications</a></li>
	</ul>
	
	<img id="ganet_logo" src="images/jpg/ganetlogo.png" alt="GoAbroad.net logo" />
	<p>Got questions? Visit our <a href="http://www.goabroad.net/faq.php">FAQ page</a></p>
	
</div>	


<div id="owner_container">

	<h2 style="margin-bottom:20px; margin-top:10px;">The Founder</h2>
	
	<div id="troy">
		<div class="owner_image">
			<img src="images/jpg/troy.jpg" alt="Troy Peden" />
		</div>

	<p class="owner_description">
		<em>Troy</em> is a co-founder of <a href="http://www.goabroad.com/">GoAbroad.com</a>
		and is the current editor.
		In addition to being the former study abroad coordinator at the
		University of Colorado at Denver, Troy has directed several non-profit
		international organizations. His extensive travel has included study,
		work and volunteering abroad throughout Europe and Russia,
		Latin America, South 	Asia and Southeast Asia. Currently, Troy resides
		in Jacksonville, Florida where he is the study abroad director at Jacksonville
		University. Troy still works very closely with the NGO he helped found in
		Southeast Asia.
	</p>
	</div>
</div>



