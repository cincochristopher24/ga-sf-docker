 	<?
 	header("ETag: PUB" . time());
	header("Last-Modified: " . gmdate("D, d M Y H:i:s", time()-10) . " GMT");
	header("Expires: " . gmdate("D, d M Y H:i:s", time() + 5) . " GMT");
	header("Pragma: no-cache");
	header("Cache-Control: max-age=1, s-maxage=1, no-cache, must-revalidate");
	session_cache_limiter("nocache");
 	
 	clearstatcache();
 	$tmp_sid = md5(uniqid(mt_rand(), true));
 	
 	?>
	
	<?php
	ob_start();
	
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'page_photos');
	$style = "<style>	
					#AjaxLoading{margin:15px 0 20px 5px;padding:3px;font-size:14px;width:150px;text-align:center;position:absolute;background-color:#707070;}
			  </style>";
	
	Template::includeDependent($style); 
	
	Template::includeDependentJs("/js/prototype.js");
	Template::includeDependentJs("/js/scriptaculous/scriptaculous.js");
	//Template::includeDependentJs("/js/cpaint/lib/cpaint2.inc.js");   
	Template::includeDependentJs("/js/Upload_Progress.js"); 
	
	$code =<<<BOF
	<script type = "text/javascript">
		var UploadMeter = new UploadProgress("$tmp_sid",$genID,"$cat",$loginID);	
		/*	
		var cp = new cpaint();
		cp.set_transfer_mode('get');
		cp.set_response_type('text');
		cp.set_persistent_connection(true);
		cp.set_async(true);
		//cp.set_proxy_url('/js/cpaint/lib/cpaint2.proxy.php');

		function upload(){
			cp.call('/ajaxpages/cp_progress_bar_status.php', 'GetBytesRead', startUpload_callback, '$tmp_sid');			
		}
		
		function startUpload(){
			document.uploader.submit();
			Effect.BlindDown('progress_bar_notice');
			
			self.setInterval('upload()', 2000)  
			
		}	
		
		function startUpload_callback(elem){		
			$("progress_bar").innerHTML = elem;	
		}
		
		*/
	
	
	</script>
BOF;

		Template::includeDependent($code);
	
	
	ob_end_clean();
	?>

	
		
		<? $subNavigation->show(); ?>
		
		
		<div id="content_wrapper">		
				<div id="wide_column">
					<div id="photo_box" class="section">
					   	<h2><span>Add Photos</span></h2>
						   <div class="content"> 	
							
								<?if($photocount && ($_GET['cat'] !='resume' && $_GET['cat'] !='group')):?>
									<div class="top_controls">
											<p class="control_text">
												 <a href="<?= $backlink; ?>" class="">Back to <?= $backcaption; ?></a> |
												 <a href="photomanagement.php?action=view&cat=<?=$_GET['cat']?>&genID=<?=$_GET['genID']?>" class="">View all photos</a>
											</p>
									</div>
								<?else:?>
									<div class="top_controls">
											<p class="control_text">
												 <a href="<?= $backlink; ?>" class="">Back to <?= $backcaption; ?></a>
											</p>
									</div>
								<?endif;?>			
							
							
							
								<div class="upload_cont">									
										
									<?php if ( $error ) : ?>
										<div class="errors">	
										
											<ul>
												<? foreach($error as $index => $err ): ?> 
													<li><? echo ($index+1).". ". $err->getMessage()." ".$err->getName()." (".round($err->getsize()/1024,2)." kb)" ?></li>
												<? endforeach; ?>												
											</ul>
										
										</div>
									<?php endif; ?>
								
								
												<form name= "uploader" action="/cgi-bin/uu_upload.pl?tmp_sid=<?=$tmp_sid?>" method="post" enctype="multipart/form-data">
																			
													<div class="photo_help">
													<span>Select image files from your computer</span><br/ > 
											
													-  You can upload up to a total size of 2MB/image.
													<br/>
													-  The following image file formats are allowed: jpg, gif, png. 
													
													
														<div class="inpt_box" style="padding-left:10px;">

																		<? for($x=0; $x<$defaultnumberofpic; $x++): ?>					
																			<div style="padding:5px;">
																				<?=$x+1; ?>.<input class="pic_browse" type="file" name="upfile_<?=$x?>" size="40" />
																			</div>
																		<? endfor; ?>
														</div>
													
													
													</div>
												
												
													
												
												
													<div style="margin:0;">
				
														<?if($cat !== 'resume' && $cat !== 'group'):?>	
															<div style="color:#4f4f4f;float:none;clear:both;margin:0;padding:0 0 10px 10px;">
														
																<b>Send notice to:</b><br/>
																<?if($cat !== 'profile'):?>
																		<? if ($isAdministrator || $isAdvisor): ?>
																			<input type="checkbox" name="chkGroup" id="chkGroup" value="1" checked />Group Members<br />
																		<? endif; ?>
																<? endif; ?>
																<input type="checkbox" name="chkAddressBook" id="chkAddressBook" value="1" checked />Address Book entries
														
															</div>
													
													
														<?endif;?>
													</div>
												
												
												
												
												
												
												
											
												
													<div id="upload_container" style="margin:0;border-top:1px solid #c3c3c3;padding-top:10px;">
												
														<input type="button" align="right" name="upload" value="Upload" onclick="UploadMeter.startUpload()"/>

														<? if($newgroup): ?>										
														
																<input type="button" align="right" name="skip" value="Skip &raquo;" onClick="window.location='<? echo "http://".$_SERVER['SERVER_NAME']."/group.php?gID=".$_GET['genID']  ?>';return false;" /> 	
												
														<? endif; ?>
												
													</div>
													<!-- Start Progress Bar -->
				    								<div id="progress_bar_notice" style="color:#878787;display:none;padding-left:20px;float:none;clear:both;">
			    									
				    									<p>
				    										<b> Uploading photos, please wait...</b>
				    									</p>
					    								<!--
					    								<p>
					    									Depending on the size of the photos and the speed of your connection, 
					    									this could take a few minutes. 
					    									Please do not close this window before the upload is complete. 
					    									This page will update automatically when your upload is finished.
														</p>
														-->					
				    								</div>
			    								
												    <div id="progress_bar" style="display:block;padding-left:20px;">
											    
												    </div>
											    
												    <!-- End Progress Bar -->
												
													<input type="hidden" name="txtcat" value="<?=$cat;?>">
													<input type="hidden" name="txtaction" value="<?=$action;?>">
													<input type="hidden" name="txtgenid" value="<?=$genID;?>">
													<input type="hidden" name="txtid" value="0">
													<input type="hidden" name="txtscriptname" value="/photomanagement.php">
												
												</form>
												<div class="clear"></div>
										
								
								
								</div>	
								<div class="clear"></div>	
							</div>	
						 	<div class="foot"></div>
   					</div>
				</div>
				<div class="clear"></div>
		</div>				
							
				
	
		
