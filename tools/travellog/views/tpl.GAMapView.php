<?php 
	require_once('travellog/model/Class.LocationFactory.php');
	$file_factory      = FileFactory::getInstance(); 
	$obj_factory       = LocationFactory::instance(); 
	$obj_map_frame     = $file_factory->getClass('MapFrame');
	$obj_map           = $file_factory->getClass('Map');
	$obj_map->includeJs();
	$width             = ( isset( $contents['width']  ) )? $contents['width']: 204; 
	$height            = ( isset( $contents['height'] ) )? $contents['height']: 300;
	$title             = ( isset( $contents['title']) )? $contents['title'] : 'Map of Travel Journals';
	$querystring       = ( isset( $contents['querystring'] ) )? $contents['querystring'] . '&': 'action=All&';
	$col_markers       = array();

	$col_locationIDs = $contents['col_locations'];
	if( count($col_locationIDs) ){
		foreach($col_locationIDs as $obj){
			$obj_marker   = $file_factory->getClass('LocationMarker');
			$obj_location = $obj_factory->create($obj->getLocationID());
			$obj_marker->setLocation($obj_location);
			$obj_marker->setLink('/journal.php?'.$querystring.'locationID='.$obj->getLocationID());
			$col_markers[] = $obj_marker; 
		} 
	}
?>

<?php if( count($col_locationIDs) ):?>
<div id="map" class="section">
	<h2>
		<span><?php echo $title?></span>
	</h2>
    <div class="content">
		<div id="map_box">
			<? 
				$obj_map_frame->setWidth($width);
				$obj_map_frame->setHeight($height);
				$obj_map_frame->setConnectMarkers(false);
				$obj_map->addMapFrame($obj_map_frame);
				echo $obj_map->plotLocations($col_markers, '', true);
			?>
		</div>
		
		<p id="trav_cont_helptxt">Click the balloon to view the travel journals in that location</p>
		
    </div>
	<div class="foot"></div>
</div>
<?php endif;?>
