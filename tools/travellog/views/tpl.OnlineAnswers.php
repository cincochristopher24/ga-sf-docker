<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');	
?>

<div class="area" id="intro">
	<div class="section" align="right">
		<h2><a href="onlineform.php?formID=<?=$question->getForm()->getOnlineFormID()?>">Survey Form</a>
			| <a href="onlinesurvey.php?formID=<?=$question->getForm()->getOnlineFormID()?>">Survey Results</a>
			| <a href="surveylist.php?formID=<?=$question->getForm()->getOnlineFormID()?>">Participant List</a>
		</h2>
	</div>	
</div>

<div class="area" id="main">
	<div class="section">
		<h2><?=$question->getQuestion()?></h2>
		<div class="content">
			
			<ul class="form">
				<? for($i=0; $i<count($arrAnswers);$i++) { ?>
					<? $each = $arrAnswers[$i]; ?>
					<li>
						<label>&raquo;&nbsp;<?=$each->getAnswerText()?></label>										 
					</li>					
				<? } ?>	
				<li>
					
				</li>
			</ul>
	
			<div class="clear"></div>
		</div>
	</div>
</div>