<?php
require_once("travellog/views/Class.AbstractView.php");
class ArticleView extends AbstractView{
	function render(){
		$this->obj_template->set( 'sub_view', $this->sub_views );
		$this->obj_template->set( 'contents', $this->contents );
		return $this->obj_template->fetch('tpl.ViewArticle.php');
	}
}
?>
