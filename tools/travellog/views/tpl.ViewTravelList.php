<?
	
	/**
	 * tpl.ViewTravelList.php
	 * @author marc
	 * Dec 6, 2006
	 * @param object Travels - array of Travels/Journals
	 * @param int displayHeader - flag to display header
	 * @param int displayAll - flag to display "view all" link
	 * @param object country
	 */
	 
	 if( !isset($contents["displayHeader"]) )
	 	$contents["displayHeader"] = 0;
	 if( !isset($contents["displayAll"]) )
	 	$contents["displayAll"] = 0;	
	 if( !isset($contents["displayDescriptionTeaser"]) )
	 	$contents["displayDescriptionTeaser"] = 0;	
	 if( !isset($contents["displayAllCities"]) )
	 	$contents["displayAllCities"] = 0;	
	 if( !isset($contents["displayCount"]) )
	 	$contents["displayCount"] = 3;	
	 	
?>	
	<? if( count($contents["Travels"]) ): ?>
		
			<? if( $contents["displayHeader"] ): ?>
				<h2><span>Journal<? if( 1 < count($contents["Travels"]) ): ?>s<? endif; ?></span></h2>
			<? endif; ?>
			<div class="content">
			<? 	foreach($contents["Travels"] as $travelLogID => $Travel):	?>
				<div class="travel">
					
						<? if( $Travel->getRandomPhoto() != NULL ) : ?>
							<div class="photo">
							<img src="<?= $Travel->getRandomPhoto()->getThumbnailPhotoLink(); ?>" />
							</div>
						<? endif; ?>
						<h3><a href="../travellog.php?action=view&travelID=<?= $Travel->getTravelID(); ?>&travelerID=<?= $Travel->getTravelerID(); ?>&travellogID=<?= $travelLogID?>"><?= $Travel->getTitle(); ?></a></h3>
						<div class="meta">
							<?= implode(' to ',$Travel->getDateRange()); ?>
						</div> 	 
						<div class="description">
							<p>
							<? if( $contents["displayDescriptionTeaser"] ): ?>
								<?= substr($Travel->getDescription(),0,100) ?> ...&nbsp; <a href="../travel.php?action=view&travelID=<?= $Travel->getTravelID(); ?>&travelerID=<?= $Travel->getTravelerID(); ?>">read more</a>
							<? else: ?>
								<?= nl2br($Travel->getDescription()); ?>
							<? endif; ?>		
						</div>	
						<div class="info">	
							<? if ( count($Travel->getTrips()) && $contents["displayAllCities"]):
									$arrCity = array();
							 		foreach($Travel->getTrips() as $trip):	
										$arrCity[] = $trip->getLocation()->getName();
									endforeach;
									echo implode(", ",array_unique($arrCity)) . "<br />";		
								endif;	 
							?>
							Author: <a href="<?= $Travel->getTraveler()->getTravelerProfile()->getUsername(); ?>"><?= $Travel->getTraveler()->getTravelerProfile()->getUsername() ?></a><br>
							Viewed <?= $Travel->getViews();?> time<? if( 1 < $Travel->getViews() ): ?>s<? endif; ?>
						</div>
					
				</div>	
			<? endforeach; ?>
			
			<? if( $contents["displayAll"] > $contents["displayCount"] ): ?>
				<div class="section_foot">
					<a href="../travel.php<? if( isset($contents["country"]) ): ?>?countryID=<?= $contents["country"]->getLocationID(); ?><? endif; ?>">view all <? if( isset($contents["country"]) ): ?><?= $contents["country"]->getName(); ?><? endif; ?> journals</a>
				</div>
			<? endif; ?>
		</div>
		<div class="clear"></div>
		<div class="foot"></div>		
	<? endif; ?>
	
