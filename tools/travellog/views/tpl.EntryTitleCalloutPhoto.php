<?php 
require_once('Class.GaDateTime.php');
$d = new GaDateTime();
$callout = trim($contents['obj_entry']->getCallout());
?> 

			<?php if( $contents['show_admin_controls'] ):?>
				<div id="postcard">
					<a class="featureMrk_sp feature_postcard" href="javascript:void(0)" onmouseout="CustomPopup.removeBubble();" onmouseover="CustomPopup.fetchNewBubble('Send your journals as e-postcards!',this);"><span>New</span></a>
					<a class="sendusapostcard" href="/postcards.php?travellogID=<?= $contents['obj_entry']->getTravelLogID() ?>">Send as a <span>Postcard</span></a>
				</div>
			<?php endif;?> 

<h1 class="journal_header">
	<span id="ENTRYTITLE"><?=$contents['obj_entry']->getTitle()?></span>
</h1>
	
	<?if( $contents['show_admin_controls'] ):?>
		<a class="entry_links bottom_shift" href="javascript:void(0)" id="EntryTitleLink" rev="<?=$contents['obj_entry']->getTravelLogID()?>"><span>Edit Entry Title</span></a>
	<?endif;?>

	<div id="popup-entry-location"> 
		<img src="http://images.goabroad.com/images/flags/flag<?=$contents['obj_entry']->getCountryID()?>.gif" title="<?=$contents['obj_entry']->getCountry()?>" alt="<?=$contents['obj_entry']->getCountry()?>" id="journal_entry_flag" />
		<span style="display:inline">
			<?if( strtolower($contents['obj_entry']->getCity()) != strtolower($contents['obj_entry']->getCountry()) ) echo $contents['obj_entry']->getCity() .', ';?> 
			<?=$contents['obj_entry']->getCountry()?>
		</span>
		<?if(strtotime($contents['obj_entry']->getArrival())):?>
		&nbsp;|&nbsp;
			<span style="display:inline">
				<?//= $d->set($contents['obj_entry']->getArrival())->friendlyFormat(); ?>
				<?= date('M d, Y', strtotime($contents['obj_entry']->getArrival())); ?>
			</span>
		<?endif;?>
	</div>
		<div class="socialMediaBox">
			<?=$contents['fb_like_control']->render()?>
			<?=$contents['g_plusone_control']->render()?>
			<?=$contents['bookmark_controls']->render()?>
		</div>

	<?if( $contents['show_admin_controls'] ):?> 
		<a class="entry_links bottom_shift" id="edit-entry-location" href="javascript:void(0)"><span>Edit Entry Location</span></a>
	<?endif;?>
 

<span id="popup-location" style="display:none"></span>			

<div id="add_city_box" class="dialog">
	<?php 
		if( isset($arr_vars['isAdmin']) )
			include '../addNewCity.php';
		else
			include 'addNewCity.php';
	?>
</div>
