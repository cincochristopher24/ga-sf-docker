<?php $arrMonths = array('-Month-','January','February','March','April','May','June','July','August','September','October','November','December')?>

  <!-- Begin ADD/EDIT Travel Itinerary --> 
    <form id="mtpForm" name="mtpForm" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>?action=save&amp;src=<?php echo isset($_GET['src']) ? $_GET['src'] : 1 ?>">
      <input type="hidden" name="hdnTravelWishID" value="<?php echo $travel->getTravelWishID() ?>" />    
      <div id="ganet_goabroad_wrapper">
      
        <h2>
          <span><?php if (!$travel->getTravelWishID()) : ?>Add a New Travel Itinerary<?php else: ?>Modify Travel Itinerary<?php endif; ?></span>
        </h2>

        <h4>Give your itinerary a name:</h4>
        <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>        
        <input id="title" name="title" class="input_box" type="text" value="<?php echo htmlentities($travel->getTitle());?>" />
        <p>Ex. My World Tour in 2007; Exploring Greece; My Year Out</p>
        
        <h4>Itinerary Description:</h4>
        <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>        
        <textarea id="travel_details" class="input_box" name="details"><?php echo htmlentities($travel->getDetails()); ?></textarea>
        <p>Please write a few details about your trip, ex. [My World Tour in 2007] My quest to set foot on every continent</p>
        
        <div id="theCountries">
          <?php $destinations = $travel->getDestinations(); $numDestinations = count($destinations);?>
          <?php foreach($destinations as $i => $destination) : ?><!-- when adding a dummy destination is created -->
          <div class="destinations" id="destination_<?php echo $i ?>">
            <h4 id="lblDestination">Select Destination Country:</h4>
            <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>            
            <select name="selCountries[]" class="destination_select_countries" style="margin: 2px 0 0 0">
              <option value="0">-- Select Country --</option>
              <?php foreach ($countryList as $country) : ?>
              <option id="<?php echo $country->getCountryID() ?>" value="<?php echo $country->getCountryID() ?>" <?php if ($destination->getCountry()->getCountryID() == $country->getCountryID()) echo 'selected="selected"';?>>
                <?php echo $country->getName() ?>
              </option>
              <?php endforeach; ?>
            </select>
            <a id="deleteDestination" class="delete_entry" style="font-weight: bold; font-size: 12px; <?php if ($numDestinations < 2): ?>display: none;<?php endif;?>" href="#delDestination">delete this destination</a>
            
            <h4>Destination Details:</h4>
            <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>
            <textarea name="dest_descriptions[]" class="input_box"><?php echo $destination->getDescription();?></textarea>
            <p>Please write your specific destination, i.e. the cities, states, provinces, etc.</p>

            <h4>Travel Start Date:</h4>
            <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>            
            <select style="margin: 0" name="start_months[]">
              <?php for ($j = 0; $j < count($arrMonths); $j++) : ?>
              <option value="<?php echo $j ?>" <?php if ($destination->getStartMonth() == $j) echo 'selected="selected"'; ?>>
              	<?php echo $arrMonths[$j] ?>
              </option>
              <?php endfor; ?>
            </select>
            
            <select style="margin: 0" name="start_days[]">
              <option value="0">-Day-</option>
              <?php for ($j = 1; $j <= 31; $j++) : ?>
              <option value="<?php echo $j ?>" <?php if ($destination->getStartDay() == $j) echo 'selected="selected"'; ?>><?php echo $j ?></option>
              <?php endfor; ?>
            </select>

            <select style="margin: 0" name="start_years[]">
              <option value="0">-Year-</option>
              <?php for ($j = 2000; $j <= 2030; $j++) : ?>
              <option value="<?php echo $j ?>" <?php if ($destination->getStartYear() == $j) echo 'selected="selected"'; ?>><?php echo $j ?></option>
              <?php endfor; ?>
            </select>
            
            <p>Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</p>

            <h4>Travel End Date:</h4>
            <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>
            <select style="margin: 0" name="end_months[]">
              <?php for ($j = 0; $j < count($arrMonths); $j++) : ?>
              <option value="<?php echo $j ?>" <?php if ($destination->getEndMonth() == $j) echo 'selected="selected"'; ?>><?php echo $arrMonths[$j] ?></option>
              <?php endfor; ?>
            </select>
            
            <select style="margin: 0" name="end_days[]">
              <option value="0">-Day-</option>
              <?php for ($j = 1; $j <= 31; $j++) : ?>
              <option value="<?php echo $j ?>" <?php if ($destination->getEndDay() == $j) echo 'selected="selected"'; ?>><?php echo $j ?></option>
              <?php endfor; ?>
            </select>
            
            <select style="margin: 0" name="end_years[]">
              <option value="0">-Year-</option>
              <?php for ($j = 2000; $j <= 2030; $j++) : ?>
              <option value="<?php echo $j ?>" <?php if ($destination->getEndYear() == $j) echo 'selected="selected"'; ?>><?php echo $j ?></option>
              <?php endfor; ?>
            </select>
            
            <p>Optional</p>
            
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </form>
          
    <div class="actions">
		<a href="#saveNewLink" id="save"> Save </a> |
        <a href="#addDestinationLink" id="addDestination"> Add Second Destination </a> |
	  	<a href="#cancelNewLink" id="cancelNew"> Cancel </a>
    </div>
              

          
<!-- </div>  -->
  <!-- End ADD Travel Itinerary -->

  <div id="hiddenDestination" style="display: none">   
    <div class="destinations">
    
      <h4 id="lblDestination">Select Destination Country:</h4>
      <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>      
      <select name="selCountries[]" class="destination_select_countries" style="margin: 2px 0 0 0">
        <option value="0">-- Select Country --</option>
        <?php foreach ($countryList as $country) : ?>
        <option id="<?php echo $country->getCountryID() ?>" value="<?php echo $country->getCountryID() ?>">
          <?php echo $country->getName() ?>
        </option>
        <?php endforeach; ?>
      </select>
      <a id="deleteDestination" class="delete_entry" style="font-weight: bold; font-size: 12px;" href="#delDestination">delete this destination</a>
      
      <h4>Destination Details:</h4>
      <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>
      <textarea name="dest_descriptions[]" class="input_box"></textarea>
      <p>Please write your specific destination, i.e. the cities, states, provinces, etc.</p>
      
      <h4>Travel Start Date:</h4>
      <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>
      <select style="margin: 0" name="start_months[]">
        <?php for ($j = 0; $j < count($arrMonths); $j++) : ?>
        <option value="<?php echo $j ?>"><?php echo $arrMonths[$j] ?></option>
        <?php endfor; ?>
      </select>
      
      <select style="margin: 0" name="start_days[]">
        <option value="0">-Day-</option>
        <?php for ($j = 1; $j <= 31; $j++) : ?>
        <option value="<?php echo $j ?>"><?php echo $j ?></option>
        <?php endfor; ?>
      </select>
      
      <select style="margin: 0" name="start_years[]">
        <option value="0">-Year-</option>
        <?php for ($j = 2000; $j <= 2030; $j++) : ?>
        <option value="<?php echo $j ?>"><?php echo $j ?></option>
        <?php endfor; ?>
      </select>
      
      <p>Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</p>

      <h4>Travel End Date:</h4>
      <div class="errorMsg" style="color: brown; display: none">Placeholder for error message</div>      
      <select style="margin: 0" name="end_months[]">
        <?php for ($j = 0; $j < count($arrMonths); $j++) : ?>
        <option value="<?php echo $j ?>"><?php echo $arrMonths[$j] ?></option>
        <?php endfor; ?>
      </select>
      
      <select style="margin: 0" name="end_days[]">
        <option value="0">-Day-</option>
        <?php for ($j = 1; $j <= 31; $j++) : ?>
        <option value="<?php echo $j ?>"><?php echo $j ?></option>
        <?php endfor; ?>
      </select>
      
      <select style="margin: 0" name="end_years[]">
        <option value="0">-Year-</option>
        <?php for ($j = 2000; $j <= 2030; $j++) : ?>
        <option value="<?php echo $j ?>"><?php echo $j ?></option>
        <?php endfor; ?>
      </select>
      
      <p>Optional</p>
      
    </div>   
  </div><!-- End hiddenDestination -->