<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');
	Template::includeDependentJs('/js/interactive.form.js');
	$subNavigation->show();
?>

<div class="area" id="top">
	<div class="section" id="add_members">
			<h1>Add New Member to <?=$grpName?></h1>
			<div class="content">
				<div class="form_division">
					<form name="frmAddMember" action="members.php?mode=add&amp;gID=<?=$grpID?>" class="interactive_form" method="post">
						<h2>Search by Email</h2>
						<ul class="form">
							<li>
								<fieldset>
								<span>
									<?= FormHelpers::CreateTextBox('txtEmail', $txtEmail,
										array(
											   'class'     => 'text',
											   'maxlength' => 50
											) 
										); 											 
									?>
									<label for="txtEmail">Email Address</label>
								</span>
								<span class="actions"><input type="submit" name="btnSearchEmail" value="Search" class="submit" /></span>
								</fieldset>
							</li>
						</ul>
					</form>
					<div class="clear"></div>
				</div>
				
				<div class="form_division" id="add_members_search">
					<form name="frmAddMember" action="members.php?mode=add&amp;gID=<?=$grpID?>" method="post" class="interactive_form" >
						<h2>Search by Name</h2>
						<ul class="form">
							<li>
								<fieldset>
									<span>
										<?= FormHelpers::CreateTextBox('txtFirstName', $txtFirstName,
											array(
												   'class'     => 'text',
												   //'size'      => 50,
												   'maxlength' => 50
												) 
											); 
										 
										?>
										<label for="txtFirstName">First Name</label>
									</span>
									<span>
										<?= FormHelpers::CreateTextBox('txtLastName', $txtLastName,
											array(
												   'class'     => 'text',
												   //'size'      => 50,
												   'maxlength' => 50
												) 
											); 
										 
										 ?> 
										<label for="txtLastName">Last Name</label>
									</span>
									<span class="actions">
										<input type="submit" name="btnSearchName" value="Search" class="submit" />
									</span>
								</fieldset>
							</li>
						</ul>
					</form>
					<div class="clear"></div>
				</div>
				
				<div class="form_division" id="add_members_search">
					<form name="frmAddMember" action="members.php?mode=invite_multiple&amp;gID=<?=$grpID?>" method="post" class="interactive_form" >
						<h2>Multiple Invitation By Email Add</h2>
						<ul class="form">
							<li>
								<fieldset>
									<span>
										<?= FormHelpers::CreateTextBox('txtMemail', $txtMemail,
											array(
												   'class'     => 'text',
												   'size'     => '60',
												   'maxlength' => 500
												) 
											); 
										 
										?>
										<label for="txtMemail">Email Address (separated by a comma)</label>
									</span>										
									<span class="actions">
										<input type="submit" name="btnInvitezMultiple" value="Invite" class="submit" />
									</span>
								</fieldset>
							</li>
						</ul>
					</form>
					<div class="clear"></div>
				</div>
				
				<div class="clear"></div>
				<? if (0 < strlen($txtUserName) ||  0 < strlen($txtFirstName) || 0 < strlen($txtLastName) || 0 <  strlen(($txtEmail))) { ?>
						<form name="frmMulti_invite" action="members.php?mode=invite_multiple&amp;gID=<?=$grpID?>" method="post" class="interactive_form" >
							<div id="user_search_results">
								<?  if (isset($arrtravID) || isset($emailaddsearch)) : ?>
									<h2>Showing Results for <em><?=$searchkey?></em>.</h2>	
								<? else : ?>
									<h2>No matches for <em><?=$searchkey?></em>were found.</h2>
								<? endif; ?>	
								<?  if (isset($arrtravID)) : ?>
									<? if (count($arrtravID)) : ?>
										<p>The following matches were found. Please click confirm to add all checked matches to the group.</p>
									<? endif;?>
									<ul class="users">
									<? for($idx=0;$idx<count($arrtravID);$idx++) :
										$traveler = new Traveler($arrtravID[$idx]);										
										$eachprofile = $traveler->getTravelerProfile();
									?>								
										<li>
											<a href="/<?=$eachprofile->getUserName()?>" class="user_thumb">	
												<img src="<? echo $eachprofile->getPrimaryPhoto()->getThumbnailPhotoLink() ?>" />																
											</a>
											<a href="/<?=$eachprofile->getUserName()?>" class="username">	
												<?=$eachprofile->getUserName()?>		
											</a>
											<? if (NULL!=$eachprofile->getFirstName() || NULL!=$eachprofile->getLastName() ) { ?>
												(
												<? if (NULL!=$eachprofile->getFirstName() ) { ?>
													<?= $eachprofile->getFirstName() ?>
												<? } ?>
												<? if (NULL!=$eachprofile->getLastName() ) { ?>
													<?= $eachprofile->getLastName() ?>
												<? } ?>
												)
											<? } ?>
											<? if (NULL!=$eachprofile->getEmail()) { ?>
												<?= $eachprofile->getEmail()?>
											<? } ?>
											
											
											<br />
										
											<? if ($grp->isMember($traveler)) : ?>											
												Member
											<? elseif ($grp->isInInviteList($traveler)) : ?>
												Pending Approval |
												<a href="members.php?mode=cancel&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$grpID?>" >Cancel Invitation</a>
											<? else : ?>												
												<a href="members.php?mode=invite&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$grpID?>&amp;confirm" >	
													Invite to Join Group
												</a>	
												| <input type="checkbox" name="multi_inviteID[]" value="<?=$eachprofile->getTravelerID()?>" checked>	Invite											
											<? endif; ?>
																		
										</li>
									<? endfor;?>
									</ul>
								<?  elseif (isset($emailaddsearch)) : ?>
									<p>The following email addresses were not found in our member list YET. This could simply be because your group members have not yet joined the GoAbroad Network. 
									We suggest you double check the email addresses and select the ones you want us to store for future use. 
									Once a new member joins using one of these email addresses, a match will automatically be made and the new member will immediately be prompted to instantly join your group. 
									In addition, an email will be sent to each of these email addresses requesting membership with instructions to make it easy to join along with a request from <strong><em><?= $grp->getName()?></em></strong>.</p>
									<ul class="users">
										<li>
											<?= $emailaddsearch	?>		
											|											
											<? if ($grp->isInEmailInviteList($emailaddsearch)) : ?>
												Pending Invitation Approval |
												<a href="members.php?mode=cancel_email&amp;email=<?=$emailaddsearch?>&amp;gID=<?=$grpID?>" >Cancel Invitation</a>
											<? else : ?>
												<a href="members.php?mode=invite_email&amp;email=<?=$emailaddsearch?>&amp;gID=<?=$grpID?>" >Invite to Join Group</a>
												| <input type="checkbox" name="multi_inviteemail[]" value="<?=$emailaddsearch?>" checked>	Invite												
											<? endif; ?>									
											
										</li>
									</ul>								
								<? endif; ?>
							</div>
							<input type="hidden" name="multiemailsearch" value="1">
							<input type="submit" name="btnConfirmMultiple" value="confirm">
						</form>
				<? } ?>
				<div class="actions">
					<a href="<?$backLink->getLink(true)?>" class="button">Back</a>
				</div>
			</div>
	</div>
</div>