<?
	/*************************************
	 * edit of neri:	01-20-09
	 * 		added Manage Survey link
	 *************************************/
?>

<div class="section" id="survey">
	<h2>
		<span>Surveys</span>
		<?php if ($isAdminLogged) : ?>
			<span class="header_actions">
				<a href="/surveyformeditor.php?frmID=0&amp;gID=<?= $group->getGroupID() ?>" class="add" >Add Survey</a>
				| <a href="/surveycenter.php?gID=<?= $group->getGroupID() ?>">Manage Survey</a>
			</span> 
		<?php endif; ?>
	</h2>
	<div class="content" style="overflow: auto;">
		<ul class="elements">					
		<?php  foreach($surveys as $iSurvey):?>
			<li>	
				<a class="survey_title" href="/surveyform.php?frmID=<?= $iSurvey->getSurveyFormID() ?>&gID=<?= $group->getGroupID() ?>"> 
					<?= $iSurvey->getName() ?>
				</a>
				<br />
				<?php 
					$participants = $iSurvey->getParticipants();
					$participantLinks = array();
					foreach($participants as $iParticipant){
						try{
							$groupParticipant = new AdminGroup($iParticipant->getGroupID());
						}
						catch(exception $e){
							$groupParticipant = null;
						}
						if(!is_null($groupParticipant)){
							$url = $groupParticipant->getFriendlyURL();
							$groupName = $groupParticipant->getName();
							$participantLinks[] = "<a href='$url' class='group_name'>$groupName</a>";
						}
					}
				?>
				<?= implode(', ',$participantLinks) ?>
			</li>
		<?php endforeach; ?>
		</ul>				
		<? if (0 == count($surveys) && $isAdminLogged) : ?>
			<? /*if ( $is_parent_group ) : ?>
				<p class="side_help_text">This group has no surveys.</p>
			<? else : ?>
				<p class="side_help_text">This subgroup has no surveys.</p>
			<? endif */?>
			
			<div class="sg_description">
				<h3></h3>
				<p>
					This is a very useful tool for creating fully customized online forms to help you acquire  feedback for evaluations or just create fun surveys to keep your members active.
				</p>
				<p>
					Responses are anonymous and presented in stastical reports, saving you time and effort in collating and calculating responses.
				</p>
				<a class="button_v3 goback_button jLeft" href="/surveyformeditor.php?frmID=0&amp;gID=<?= $group->getGroupID() ?>"><strong>Add Survey</strong></a>
			</div>
			
			
		<? endif; ?>
	</div>	
</div>