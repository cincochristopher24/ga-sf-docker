<?$this->doNotUseMainTemplate();?>
<div class="interactive_form">
<ul class="form">
	<li>
		<label for="txtTitle">Event Title</label>
		<div id="errTitle" style="color:red"></div>
		<input type="text" name="title" id="title" value="<?=$props['title']?>" size="40" class="text big" />
	</li>
	<li>
		<label for="txtDate">Date</label>
		<div id="errDate" style="color:red"></div>
		<input style="width:109px;" type="text" readonly="true" class="date-pick text" name="event_date" id="event_date" value="<?=$props['event_date']?>"/>
		<label for="event_time">Time</label>
		<input style="width:109px;" type="text" class="text" name="event_time" id="event_time" value="<?php echo $props['event_time']?>"/>
	</li>
	<li>
		<label for="timezone">Time Zone</label>
		<select name="timezone" id="timezone" >
			<option value="0" selected >-- Select Time Zone --</option>
			
			<?php foreach( $props['groupedTimezones'] as $id => $each ):?>
				<option value="<?php echo $id?>" <?php if($props['timezone'] == $id) echo 'selected'?> ><?php echo $each?></option>
			<?php endforeach; ?>
			
			<?/*php foreach( $props['groupedTimezones'] as $continent => $v ):?>
				<optgroup label="<?php echo $continent ?>">
				<?php foreach( $v as $id => $iZone ):?>
					<option value="<?php echo $id?>" <?php if($props['timezone'] == $id) echo 'selected'?> ><?php echo $iZone?></option>
				<?php endforeach;?>
				</optgroup>
			<?php endforeach; */?>
		</select>
	</li>
	<li>
		<label for="txaDescription">Event Details</label>
		<div id="errDescription" style="color:red"></div>
		<textarea name="description" id="description" rows="10" cols="80"><?=$props['description']?></textarea>
	<li>
		<fieldset class="choices">
			<legend><span>Show this Event to</span></legend>
			<ul>
				<li>
					<input type="radio" name="display[]" id="display1" value="1" <?if( $props['display'] == 1 ):?>checked="true"<?endif;?> /><label for="display1">Public</label>
				</li>
				<li>
					<input type="radio" name="display[]" id="display2" value="2" <?if( $props['display'] == 2 ):?>checked="true"<?endif;?> /><label for="display2">Friends</label> 
				</li>
			</ul>
		</fieldset>
	</li>
</ul>
	<div class="actions" style="margin-left:0px;">
		<input type="hidden" name="mode"      id="mode"      value="<?=$props['mode']?>" />
		<input type="hidden" name="tID"       id="tID"       value="<?=$props['tID']?>"  /> 
		<input type="hidden" name="eID"       id="eID"       value="<?=$props['eID']?>"  />  
		<input type="hidden" name="context"   id="context"   value="2"                 />   
		<input type="button" name="action"    id="action"    value="Save"   class="submit" onclick="jQuery('input').save()" /><input type="button" name="btnCancel" id="btnCancel" value="Cancel" class="submit" onclick="jQuery().cancel()" /> 
	</div>

</div>
  