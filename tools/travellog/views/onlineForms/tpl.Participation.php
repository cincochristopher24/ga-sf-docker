<?php	
	Template::setMainTemplateVar('layoutID', 'survey');
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');		
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs('/js/interactive.form.js');
	Template::includeDependentJs('/js/scriptaculous/scriptaculous.js');
	//Template::includeDependentCss('/css/formeditor/surveystatsentry.css');
	require_once('Class.GaDateTime.php');
	
	$d = new GaDateTime();
	
	$subNavigation->show();
	ob_start();	
?>
<script type="text/javascript">
//<![CDATA[
	var containers = new Array();
	function showDetail(divCont){
		for(var i=0;i<containers.length;i++){
			$(containers[i]).style.display = 'none';
			$('link_'+containers[i]).removeClassName('active');
		}
		new Effect.Appear(divCont, { 
			duration: .30
		});
		//$(divCont).style.display = 'block';
		$('link_'+divCont).addClassName('active');
		
	}
	function showFirstEntry(){
		showDetail(containers[0]);
	}
//]]>
</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);
?>

<div class="area mine" id="intro">
	<div class="section">
    	<h1>Survey Center</h1>
  	</div>
</div>
	<div id="container">
		<div id="container_inner_wrapper">
			<div id="survey_options">
		    	<ul id="top_tabs">
		    		<li id="sel" class="first_tab"><a href="manage.php" class="first_tab">Manage Online Forms</a></li>
		      		<li><a href="formeditor.php?frmID=0">Create a New Online Form</a></li>
		    	</ul>
		    	<div class="clear"></div>
		  	</div>
			<div id="content">
				<div id="stats_content" class="section">
					<h2><?= $onlineForm->getName() ?> Stats (<?= count($participations) ?> participant<? if (count($participations) > 1) echo 's'; ?>) </h2>
					<p id="survey_caption"><?= $onlineForm->getCaption() ?></p>
					<ul class="tabs">
						<li><strong><a href="stats.php?frmID=<?= $onlineForm->getGaOnlineFormID() ?>">Overview</a></strong></li>
						<li class="active">
							<? if( 0 < count($participations)): ?>
								<a href="stats.php?frmID=<?= $onlineForm->getGaOnlineFormID() ?>&view=entries">Entries</a>
							<? else: ?>
								Entries
							<? endif; ?>
						</li>
					</ul>
					<div id="stats_entries">
						<div id="navPanel">
							<ul>
							<!-- The next li element is a temporary fix for an as yet undocumented
								problem with IE7. Margin settings does not seem to apply properly on
								this first child element when its className changes dynamically through
								javascript. Weird...
							<li class="walalang" style="display:none"></li>-->
							<?php foreach($participations as $iParticipation): ?>
								<li id="link_ans_<?= $iParticipation->getGaOnlineFormParticipationID() ?>">
									<p>
										<strong>
											<a href="javascript:void(0)" onclick="showDetail('ans_<?=  $iParticipation->getGaOnlineFormParticipationID() ?>')">
												<?= $iParticipation->getTraveler()->getUserName() ?>
											</a>
										</strong>
										<br />
										<span class="info meta date">
											<strong>Date: </strong>
											<?= $d->set($iParticipation->getDateCreated())->friendlyFormat() ?>
										</span>
										<br />
										<span class="info meta"><strong>Group:</strong> <?= $iParticipation->getGroupParticipant()->getGroup()->getName() ?></span>
									</p>
									<script type="text/javascript">
										containers[containers.length] = 'ans_<?= $iParticipation->getGaOnlineFormParticipationID() ?>';
									</script>
								</li>
							<?php endforeach; ?>
						</div>
						<div id="detailPanel">
							<?php foreach($participations as $iParticipation): ?>
								<div id="ans_<?= $iParticipation->getGaOnlineFormParticipationID() ?>" style="" >
									<?php $fieldValues = $iParticipation->getFieldValues() ?>
									<ul>
										<?php foreach($formData['fields'] as $iFld): ?>
											<li>
												<h3 <?php if($iFld['fieldType']=='4'){echo 'class="sectionBreak"';} ?>>
													<?= nl2br($iFld['caption']) ?>
													<?php if($iFld['requiredStatus']):?>
														<span class="required">*</span>
													<?php endif; ?>
												</h3>
												<?php if(array_key_exists($iFld['gaFormFieldID'], $fieldValues)): ?>
														<?php if(count($iFld['inputs'])): ?>
																<?php if(is_array($fieldValues[$iFld['gaFormFieldID']])): ?>
																	<div>
																		<ul>
																			<?php foreach($fieldValues[$iFld['gaFormFieldID']] as $iVal): ?>
																	 			<li><?= $iVal ?></li>
																	 		<?php endforeach; ?>
																		</ul>
																	</div>
																<?php else: ?>
																 	<div>
															 			<?= $fieldValues[$iFld['gaFormFieldID']] ?>
																 	</div>
																<?php endif; ?>				 	
														<?php endif; ?>
												<?php endif; ?>
											</li>
										<?php endforeach; ?>	
									</ul>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<div class="clear"></div>
<script type="text/javascript">
	showFirstEntry();
</script>
