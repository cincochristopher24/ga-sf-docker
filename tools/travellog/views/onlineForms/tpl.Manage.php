<?php
	Template::setMainTemplateVar('layoutID', 'survey');	
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('title', $pageTitle);
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs('/js/interactive.form.js');
	$subNavigation->show();	
?>

<div id="content_wrapper">

		<div id="container">
			<div id="container_inner_wrapper">
				<div id="survey_options">
	    	<ul id="top_tabs">
	    		<li id="sel" class="first_tab"><a href="manage.php" class="first_tab">Manage Online Forms</a></li>
	      		<li><a href="formeditor.php?frmID=0">Create New Online Form</a></li>      		
	    	</ul>
	    	<div class="clear"></div>
	  	</div>	
				<div id="content">
			<?php 
				$withRecord = false;
				$iterator->rewind();
				while( $iterator->valid() ):
					$withRecord = true;
			?>
			<div class="survey_box">
	        	<h2><a href="participate.php?frmID=<?= $iterator->current()->getGaOnlineFormID() ?>&gID=<?= $group->getGroupID() ?>" title="View this Online"><?= strip_tags($iterator->current()->getName()) ?></a></h2>
	      		<p> 
		      		<b>Created on:</b> <?= date("M d, Y",strtotime($iterator->current()->getDateCreated())) ?><br />
		        	<b>Participants:</b>
		        	<?php
		        		$pars = $iterator->current()->getParticipants();
		        		$ctr = 1;
		        		foreach($pars as $iPar):
		        			try{
								$grp = $groupFactory->create($iPar->getGroupID());
							}
							catch(exception $e){
								$grp = null;
							}
		        	?>
						<?if(!is_null($grp)):?>
							<a href="/group.php?gID=<?=$iPar->getGroupID()?>"><?=$grp->getName().'('.count($grp->getMembers()).')';?></a><?= ($ctr++ < count($pars))?',':'' ?>
						<?endif;?>
					<?php endforeach; ?><br />
					<?php 
					/***
						<?php
							$totalParticipants = $iterator->current()->getTotalNumberOfUniqueTravelerParticipants();
							$participated = $iterator->current()->getNumberOfTravelerParticipantsWhoParticipated();
						?>
						<b>Total No. of Participants: </b> <?= $totalParticipants ?> <br />
			        	<b>No. who participated:</b> <?= $participated ?><br />
			        	<b>No. who didn't participate:</b> <?= ($totalParticipants-$participated < 0 ? 0 : $totalParticipants-$participated) ?><br />
					***/ 
					?>
	      		</p>
	          	<div class="survey_actions">
	            	<ul>
	              		<li><a href="formeditor.php?frmID=<?= $iterator->current()->getGaOnlineFormID() ?>" class="edit" title="Edit this Online Form">Edit</a></li>
	              		<li><a href="manage.php?method=delete&amp;frmID=<?= $iterator->current()->getGaOnlineFormID() ?>" class="delete" title="Delete this Online Form" onclick="return confirm('Are you sure you wan to delete Online Form \'<?= strip_tags($iterator->current()->getName()) ?>\'?\nAll of its data including the answers of the participants will also be deleted.');">Delete</a></li>
	              		<li><a href="manage.php?method=<?=$iterator->current()->isOpen()?'close':'open'?>&amp;frmID=<?= $iterator->current()->getGaOnlineFormID() ?>" class="<?= $iterator->current()->isOpen()?'lock':'unlock' ?>" title="<?= $iterator->current()->isOpen()?'Close':'Open' ?> this Online Form"><?= $iterator->current()->isOpen()?'Close':'Open' ?></a></li>
	              		<li><a href="stats.php?frmID=<?= $iterator->current()->getGaOnlineFormID() ?>" class="stats">Stats</a></li>
	              		<li><a href="formeditor.php?frmID=<?= $iterator->current()->getGaOnlineFormID() ?>&duplicate" class="template">Duplicate</a></li>
	           	 	</ul>
	          	</div>
	        </div>
	        <?php
	        	$iterator->next();
	        	endwhile; 
	        ?>
	        <?php if(!$withRecord):?>
				<div class="survey_box">
					<h3 class="initial">You have no online forms yet. Click on the <a href="formeditor.php">Create a New Online Form</a> link to create your first online form.</h3>
				</div>		
			<?php endif;?>
			<div class="clear"></div>
			<?php if($withRecord): ?>
	        	<div class="pagination"> <? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?> </div>
	       	<?php endif;?>        
	    </div>
	    	</div>
		</div>
		<div class="clear"></div>


</div>