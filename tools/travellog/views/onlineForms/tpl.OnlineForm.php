<?php
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs('/js/interactive.form.js');
	Template::setMainTemplateVar('layoutID', 'survey');	
	$subNavigation->show();
?>
<div id="content_wrapper">
	<?php if('SHOW_FORM'==$action): ?>
		<div class="section" id="survey_preview">
			<h1><span><?= $onlineForm->getName() ?></span></h1>
			<div id="formStage" class="content">
				<?php if($isAdvisor): ?>
			      	<p>NOTE:  Since you are administering this survey, you are not a qualified participant. You can answer this form for testing purposes only, your answers will not be saved.<?php if(!$onlineForm->isOpen()): ?> Also, this survey is marked as closed. No traveler can participate in this survey.<?php endif; ?></p>
			    <?php endif;?>
				<p><?= nl2br($onlineForm->getCaption()) ?></p>
				<form action="/onlineforms/participate.php?frmID=<?= $onlineForm->getGaOnlineFormID() ?>&gID=<?= $gID ?>" method="post" class="interactive_form">
					<ul class="form">
						<?php foreach($formData['fields'] as $iFld):?>
							<li>
									<?php if(array_key_exists($iFld['gaFormFieldID'],$errFields)) :?>
										<div class="errors"><strong><?= $errFields[$iFld['gaFormFieldID']] ?></strong>
									<?php endif;?>
											<label for="<?= $gaFormHelper->getFieldName($iFld) ?>" class="<?= $gaFormHelper->getFieldLabelClassName($iFld) ?>">
												<?= nl2br($iFld['caption']) ?>
												<? if($iFld['requiredStatus']):?>
													<span class="required">*</span>
												<? endif; ?>
											</label>
											<ul>
												<? $inputs = $iFld['inputs']; ?>
												<? foreach($inputs as $iInput): ?>
												 	<li>
														<?
															$options = array();
															if(array_key_exists($iFld['gaFormFieldID'],$prevPostValues)){
																$options['value'] = $prevPostValues[$iFld['gaFormFieldID']];
															}
														?>
														<?= $gaFormHelper->renderControl($iInput,$options) ?>
												 	</li>
												<? endforeach; ?>
											</ul>
									<?php if(in_array($iFld['gaFormFieldID'],$errFields)) :?>
										</div>
									<?php endif;?>
							</li>
						<?php endforeach; ?>
						<li class="actions">
							<input type="submit" value="Submit" class="submit ga_interactive_form_field" />
							<input type="reset" value="Reset" class="button ga_interactive_form_field" />
						</li>
					</ul>
				</form>
				<div class="clear"></div>
			</div>
			<div class="foot"></div>
		</div>
	<?php elseif('SHOW_MESSAGE'==$action):?>
		<div class="section" id="survey_preview">
			<h1><span>Online Form</span></h1>
			<div class="content">
				<p><?= $message ?></p>
			</div>
			<div class="foot"></div>
		</div>
	<?php endif;?>
</div>