<?php
	require_once 'Class.Template.php';
	require_once "Class.Paging.php";
	require_once "travellog/model/Class.RowsLimit.php";
	
	class ViewSubGroupsCategory{
		private $mParentID;
		private $mCategoryID;
		private $mCategoryName;
		private $mNumberOfRowsToRetrieve;
		private $mCurrentPage;
		
		private $mSubGroupNonCategoryList;
		private $mSubGroupCategoryList;
		
		function setParentID($id){
			$this->mParentID = $id;
		}
		
		function setCategoryID($id){
			$this->mCategoryID = $id;
		}
		
		function setCategoryName($name) {
			$this->mCategoryName = $name;
		}
		
		function setNumberOfRowsToRetrieve($num) {
			$this->mNumberOfRowsToRetrieve($num);
		}
		
		function setCurrentPage($page){
			$this->mCurrentPage = $page;
		}
		
		function setSubGroupNonCategoryList($list) {
			$this->mSubGroupNonCategoryList = $list;
		}
		
		function setSubGroupCategoryList($list) {
			$this->mSubGroupCategoryList = $list;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->setVars( array(
				"id" => $this->mCategoryID,
				"name" => $this->mCategoryName,
				"parentID" => $this->mParentID,
				"category_groups" => $this->mSubGroupCategoryList,
				"non_category_groups" => $this->mSubGroupNonCategoryList
			));
			
			$tpl->setTemplate('travellog/views/tpl.FrmCustomizedSubGroupCategory.php');
			$tpl->out(); 	
		}
	}
