<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');	
	Template::includeDependentJs("/addressBook/addressBook.js");
	//Template::setMainTemplateVar('page_location', 'My Passport');
	ob_start();	
?>	
	<script type="text/javascript">
		AddressBook.init({
			ownerID:<?= $trvID ?>,
			addressContainerID:"address",
			valueToPass:"Email",
			entriesPerPage:"10"
		});
	</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);
?>		

<?php 
	/**
	 * Added by naldz (Nov 20,2006)
	 * This is for the subnavigation links
	 */
	 //SubNavigation::displayLinks();
	 //$subNavigation->show(); 
?>


<div id="content_wrapper">
	<div id="wide_column">	
		<div class="formbox" id="compose">
			<h1>Compose</h1>
			<div class="content">
			<?php if($method == "compose"): ?>
				<!--<p class="help_text">
					Fields with  <span class="required">*</span> indicate required fields.
				</p>-->
				<form name="frmInvite" action="invite.php?method=send" method="post">
					<ul class="form">
						<li>
								<label for="subject">Subject:</label>
								<?php if(isset($errCodes) && in_array(0,$errCodes)): ?>
									<p class="errors">Please enter a subject.</p>
								<?php endif; ?>										
								<input id="subject" name="txtSubject" size="74" value="<?= $subject ?>" class="text" />
							
						</li>
						<li>
								<label for="message">Message:</label>
								<?php if(isset($errCodes) && in_array(1,$errCodes)): ?>
									<p class="errors">Please enter a message.</p>
								<?php endif; ?>
								<textarea name="txaMessage" id="message" cols="70" rows="20"><?= $message ?></textarea><br />
						</li>
						<li>
								<label for="address">Recipients' Email Addresses (Separated by a comma)&nbsp;<!--<a href="javascript:void(0)" onclick="AddressBook.show()">[Show address book]</a>--></label>
								
								<?php if(isset($errCodes) && in_array(2,$errCodes)): ?>
									<p class="errors">At least one email address of your recipient is required.<p class="error">
								<?php endif; ?>							
								<textarea name="txaAddress" id="address" cols="70" rows="5"><?= $address ?></textarea><br />
						</li>
						<li class="actions">
							<input type="submit" name="btnSend" value="Send Invitation" class="button" />
						</li>
					</ul>
					<div class="clear"></div>
				</form>	
			<?php else: ?>
				<p><?php echo $msgSummary ?></p>
				<p>
					<a href="/invite.php">Invite more friends</a> or <a href="/passport.php">return to your passport page</a>.
				</p>
			<?php endif; ?>
			<div class="clear"></div>
			</div>
		</div>
	</div>	
	<div id="narrow_column">
		<div class="helpextra"></div>
		<div class="helpbox">
			<h2>Invite</h2>
        	<p><strong>Tell everyone about the GoAbroad Network!</strong></p>
			
			<p>Share your travel experiences, tips and photos with your friends, your family and with all the
			other travelers and travel enthusiasts you know. Invite them to GoAbroad Network today and be updated with
			their travel plans, see what they've been up to and maybe even plan your next trip together!</p>
        </div>

		
	</div>
</div>