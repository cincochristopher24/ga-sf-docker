<?php
	/*
	 *	@author CheWyl
	 *	12 November 2008
	 */

	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.MembersPanel.php';
	require_once 'travellog/views/member_staff/Class.ViewNotification.php';

	class InviteStaff{

		private $mType				= 0;
		private $mCounter			= 0;
		private $mSubGroupId		= 0;
		private $mViewerType		= 0;
		private $mIsLogged			= false;
		private $isProceedInvite	= false;
		private $mGroup				= null;
		private $mLoggedUser		= null;
		private $mNotification		= null;
		private $arrError			= array();
		private $arrEmail			= array();
		private $arrNMEmail			= array();
		private $arrSubGroups		= array();
		private $arrTraveler		= array();
		private $arrNonMember		= array();
		private $arrTravelerId		= array();

		function setType($_type = 0){
			$this->mType = $_type;
		}
		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setIsLogged($_isLogged = false){
			$this->mIsLogged = $_isLogged;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}
		function setViewerType($_viewerType = 0){
			$this->mViewerType = $_viewerType;
		}
		function setErrors($_errors = array()){
			$this->arrError = $_errors;
		}
		function setSubGroupId($_subGroupId = 0){
			$this->mSubGroupId = $_subGroupId;
		}
		function setArrEmails($_emails = array()){
			$this->arrEmail = $_emails;
		}
		function setArrTravelerId($_arrTravelerId = array()){
			$this->arrTravelerId = $_arrTravelerId;
		}
		function setMessages(){
			$combinedStaff = array_merge($this->arrTraveler, $this->arrNonMember);

			$this->mNotification = new ViewNotification;
			$this->mNotification->setGroup($this->mGroup);

			if (1 == count($combinedStaff)){
				$mDetail	= $combinedStaff[0];
				$mGroupId	= $mDetail['subgroupId'];

				$mGroup		= GroupFactory::instance()->create( array($mGroupId) );
				$mName		= $mGroup[0]->getName();

				if (isset($mDetail['traveler'])){
					if ($mGroup[0]->isParent()){
						// if member of the group (parent group)
						if ($mGroup[0]->isMember($mDetail['traveler'])){
							$default = "The admin for your group " . $mGroup[0]->getName() .
										" has assigned you to be a staff of " . $mGroup[0]->getName() . ".";
						}
						else{
							$default = "The administrator of the group " . $mGroup[0]->getName() .
										" has invited you to join and be a staff of their group.";
						}
					}
					else{
						// if member of the parent group
						if ($mGroup[0]->getParent()->isMember($mDetail['traveler'])){
							$default = "The admin for your group " . $mGroup[0]->getParent()->getName() .
										" has assigned you to be a staff of " . $mName . ".";
						}
						else{
							$default = "The administrator of the group " . $mGroup[0]->getParent()->getName() .
										" has invited you to join and be a staff of " . $mName . ".";
						}
					}
				}
				elseif (isset($mDetail['email'])){
					if ($mGroup[0]->isParent()){
						$default = "The admin for your group " . $mGroup[0]->getName() .
									" has assigned you to be a staff of " . $mGroup[0]->getName() . ".";
					}
					else{
						$default = "The admin for your group " . $mGroup[0]->getParent()->getName() .
									" has assigned you to be a staff of " . $mName . ".";
					}
				}				

				$this->mNotification->setSubGroup($mGroup[0]);
			}
			else{
				// check if all checkboxes are checked, if not, customize message
				
				if ($this->mGroup->isParent())
					$default = "The administrator of the group " . $this->mGroup->getName() . " has invited you to be a staff of their group.";
				else
					$default = "The administrator of the group " . $this->mGroup->getParent()->getName() .
								" has invited you to be a staff of their group " . $this->mGroup->getName() . ".";
			}

			$this->mNotification->setEditedMessage($default);
			$this->mNotification->setInvitedStaff($combinedStaff);
			$this->mNotification->retrieve();
		}

		function prepareUnstaffedSubGroups($_tId = 0){
			$arrSubGroup = array();
			$groupList = $this->arrSubGroups;

			// check if traveler is staff of subgroup
			foreach($groupList as $eachGroup){
				if ( (!$eachGroup->isStaff($_tId)) && (!$eachGroup->isPendingStaffToGroup($_tId)) )
					$arrSubGroup[] = $eachGroup;
			}

			return $arrSubGroup;
		}

		function retrieve(){
			if (1 == $this->mViewerType || 2 == $this->mViewerType){
				if ($this->mGroup->isParent()){
					// get all sub groups
					$this->arrSubGroups = $this->mGroup->getAllSubGroups();
				}
				else
					$this->arrSubGroups = array($this->mGroup);
			}
			else{
				$this->mType = 2;
				if ($this->mGroup->isParent()){
					// get all staffed sub groups of regular staff
					$this->arrSubGroups = Traveler::getStaffedGroups($this->mLoggedUser->getTravelerID(), array($this->mGroup->getGroupID()));
				}
				else
					$this->arrSubGroups = array($this->mGroup);
			}

		 	if (0 < count($this->arrEmail)) {
				foreach($this->arrEmail as $each){
					if (strlen(trim($each))){
						// check if email address exists
						$mTraveler = MembersPanel::getTravelerByEmail($each);

						// goabroad.net user and not the advisor of the parent group
						if ( $mTraveler && !$mTraveler->isAdvisor($this->mGroup->getGroupID()) ){
							if (0 < $mTraveler->getTravelerID()){
								$mTravelerId = $mTraveler->getTravelerID();
								$mIsSuperStaff = $this->mGroup->isStaff($mTravelerId);
								$mIsPendingSS = $this->mGroup->isPendingStaffToGroup($mTravelerId);

								$mData = array();
								$mData['type']			= $this->mType;
								$mData['traveler']		= $mTraveler;
								$mData['isPendingSS'] 	= $mIsPendingSS;
								$mData['isSuperStaff']	= $mIsSuperStaff;

								// check if not superstaff or no sent invitation as superstaff
								if (!$mIsSuperStaff && !$mIsPendingSS){
									$subGroups = $this->prepareUnstaffedSubGroups($mTraveler->getTravelerID());

									if (0 < count($subGroups))
										$mData['subGroupList'] = $subGroups;
									else
										$mData['subGroupList'] = array();

									$mSubGroup = new AdminGroup();
									$mSubGroup->setGroupID($this->mSubGroupId);

									if (!$mSubGroup->isStaff($mTravelerId) && !$mSubGroup->isPendingStaffToGroup($mTravelerId)){
										$mData['subgroupId'] = $this->mSubGroupId;
										$this->isProceedInvite = true;
								
										$this->arrTraveler[] = $mData;
										if (!in_array($mTravelerId, $this->arrTravelerId))
											$this->arrTravelerId[] = $mTravelerId;

										// set notification message
										$this->setMessages();
									}
									else{
										if (0 < count($subGroups)){
											$mData['subgroupId'] = $subGroups[0]->getGroupID();
											$this->isProceedInvite = true;
											
											$this->arrTraveler[] = $mData;
											if (!in_array($mTravelerId, $this->arrTravelerId))
												$this->arrTravelerId[] = $mTravelerId;

											// set notification message
											$this->setMessages();
										}
										else{
											$mData['subgroupId'] = 0;

											$this->arrTraveler[] = $mData;
											if (!in_array($mTravelerId, $this->arrTravelerId))
												$this->arrTravelerId[] = $mTravelerId;
										}
									}
								}
								else{
									$subGroups = array();
									$mData['subgroupId'] = 0;
									$mData['subGroupList'] = array();

									$this->arrTraveler[] = $mData;
									if (!in_array($mTravelerId, $this->arrTravelerId))
										$this->arrTravelerId[] = $mTravelerId;
								}
							}
							// non goabroad.net user
							else{
								$mData = array();
								$mData['type'] = $this->mType;
								$mData['subgroupId'] = $this->mSubGroupId;
								$mData['email'] = $each;
								$mData['isPendingSS'] = false;
								$mData['isSuperStaff'] = false;

								$this->arrNonMember[] = $mData;

								if (!in_array($each, $this->arrNMEmail))
									$this->arrNMEmail[] = $each;
								$this->isProceedInvite = true;

								// set notification message
								$this->setMessages();
							}
						}
					}
				}
			}
		}

		function retrieveInvite(){
			if (1 == $this->mViewerType || 2 == $this->mViewerType){
				if ($this->mGroup->isParent()){
					// get all sub groups
					$this->arrSubGroups = $this->mGroup->getAllSubGroups();
				}
				else
					$this->arrSubGroups = array($this->mGroup);
			}
			else{
				$this->mType = 2;
				if ($this->mGroup->isParent()){
					// get all staffed sub groups of regular staff
					$this->arrSubGroups = Traveler::getStaffedGroups($this->mLoggedUser->getTravelerID(), array($this->mGroup->getGroupID()));
				}
				else
					$this->arrSubGroups = array($this->mGroup);
			}

			// get traveler
			$mTravelerId = $this->arrTravelerId[0];
			
			$mIsSuperStaff = $this->mGroup->isStaff($mTravelerId);
			$mIsPendingSS = $this->mGroup->isPendingStaffToGroup($mTravelerId);
			$mData = array();
			$mData['type'] = 0;
			$mData['subgroupId'] = 0;
			$mData['isPendingSS'] = $mIsPendingSS;
			$mData['isSuperStaff'] = $mIsSuperStaff;
			$mData['traveler'] = new Traveler($mTravelerId);

			// check if not superstaff or no sent invitation as superstaff
			if (!$mIsSuperStaff && !$mIsPendingSS){
				$subGroups = $this->prepareUnstaffedSubGroups($mTravelerId);
				$this->isProceedInvite = true;

				if (0 < count($subGroups))
					$mData['subGroupList'] = $subGroups;
				else
					$mData['subGroupList'] = array();
			}
			else{
				$subGroups = array();
				$mData['subGroupList'] = array();
			}

			if (0 < $this->mSubGroupId){
				$mSubGroup = new AdminGroup();
				$mSubGroup->setGroupID($this->mSubGroupId);

				if (!$mSubGroup->isStaff($mTravelerId) && !$mSubGroup->isPendingStaffToGroup($mTravelerId))
					$mData['subgroupId'] = $this->mSubGroupId;
				else{
					if (0 < count($subGroups))
						$mData['subgroupId'] = $subGroups[0]->getGroupID();
					else
						$mData['subgroupId'] = 0;
				}
			}
			else{
				$mSubGroup = $subGroups[0];
				if (!$this->mGroup->isStaff($mTravelerId) && !$this->mGroup->isPendingStaffToGroup($mTravelerId))
					$mData['subgroupId'] = $this->mGroup->getGroupID();
				else{
					if (0 < count($subGroups))
						$mData['subgroupId'] = $subGroups[0]->getGroupID();
					else
						$mData['subgroupId'] = 0;
				}
			}
			$this->arrTraveler[] = $mData;

			// set notification message
			$this->setMessages();
		}

		function retrieveAdd($_param = array()){
			$arrEmail = isset($_param['hdnEmailAdd']) ? explode(",", trim($_param['hdnEmailAdd'])) : array();
			$arrTravelerId = isset($_param['hdnTravelerId']) ? explode(",", trim($_param['hdnTravelerId'])) : array();
			
			if (1 == $this->mViewerType || 2 == $this->mViewerType){
				if ($this->mGroup->isParent()){
					// get all sub groups
					$this->arrSubGroups = $this->mGroup->getAllSubGroups();
				}
				else
					$this->arrSubGroups = array($this->mGroup);
			}
			else{
				$this->mType = 2;
				if ($this->mGroup->isParent()){
					// get all staffed sub groups of regular staff
					$this->arrSubGroups = Traveler::getStaffedGroups($this->mLoggedUser->getTravelerID(), array($this->mGroup->getGroupID()));
				}
				else
					$this->arrSubGroups = array($this->mGroup);
			}

			if (0 < count($arrTravelerId)){
				// loop through members
				foreach ($arrTravelerId as $tID){
					$mIsSuperStaff = $this->mGroup->isStaff($tID);
					$mIsPendingSS = $this->mGroup->isPendingStaffToGroup($tID);
					$mTraveler = new Traveler($tID);
					$mData = array();
					$mData['type'] = isset($_param['cmbStaffType_'.$tID]) ? $_param['cmbStaffType_'.$tID] : 0;
					$mData['subgroupId'] = isset($_param['cmbGroup_'.$tID]) ? $_param['cmbGroup_'.$tID] : 0;
					$mData['traveler'] = $mTraveler;
					$mData['isPendingSS'] = $mIsPendingSS;
					$mData['isSuperStaff'] = $mIsSuperStaff;

					// check if not superstaff or no sent invitation as superstaff
					if (!$mIsSuperStaff && !$mIsPendingSS){
						$subGroups = $this->prepareUnstaffedSubGroups($mTraveler->getTravelerID());
						$this->isProceedInvite = true;

						if (0 < count($subGroups))
							$mData['subGroupList']	= $subGroups;
						else
							$mData['subGroupList']	= array();
					}
					else
						$mData['subGroupList']	= array();

					$this->arrTraveler[] = $mData;

					if (!in_array($tID, $this->arrTravelerId))
						$this->arrTravelerId[] = $tID;

					if (isset($_param['chk_'.$tID]))
						$this->mCounter += 1;
				}
			}

			if (0 < count($arrEmail)){
				// loop through non-members
				for ($ctr = 1; $ctr <= count($arrEmail); $ctr++){
					$mData = array();
					$email = $arrEmail[$ctr-1];

					$mData['type'] = $_param['cmbStaffType_temp'.$ctr];
					$mData['subgroupId'] = $_param['cmbGroup_temp'.$ctr];
					$mData['email'] = $email;
					$mData['firstname'] = $_param['txtFirstName_temp'.$ctr];
					$mData['lastname'] = $_param['txtLastName_temp'.$ctr];
					$mData['username'] = $_param['txtUserName_temp'.$ctr];
					$mData['password'] = $_param['txtPassword_temp'.$ctr];
					$mData['isPendingSS'] = false;
					$mData['isSuperStaff'] = false;
					$this->arrNonMember[] = $mData;
					if (!in_array($email, $this->arrNMEmail))
						$this->arrNMEmail[] = $email;
					$this->isProceedInvite = true;

					if (isset($_param['chk_temp'.$ctr]))
						$this->mCounter += 1;
				}
			}

			// set notification message
			$this->setMessages();
		}

		function render(){

			$tpl = new Template();
			
			$tpl->set("group",				$this->mGroup);
			$tpl->set("errors",				$this->arrError);
			$tpl->set("counter",			$this->mCounter);
			$tpl->set("arrEmail",			$this->arrNMEmail);
			$tpl->set("members",			$this->arrTraveler);
			$tpl->set("subGroups",			$this->arrSubGroups);
			$tpl->set("viewerType",			$this->mViewerType);
			$tpl->set("loggedUser",			$this->mLoggedUser);
			$tpl->set("nonMembers",			$this->arrNonMember);
			$tpl->set("groupId",			$this->mGroup->getGroupID());
			$tpl->set("notification",		$this->mNotification);
			$tpl->set("arrTravelerId",		$this->arrTravelerId);
			$tpl->set("isProceedInvite",	$this->isProceedInvite);

			$combinedInvite = count($this->arrTraveler) + count($this->arrNonMember);
			if (1 < $combinedInvite || (0 == $combinedInvite && 1 < count($this->arrEmail)))
				$tpl->out("travellog/views/member_staff/tpl.ViewInviteStaff.php");
			else
				$tpl->out("travellog/views/member_staff/tpl.ViewInviteSingleStaff.php");
		}
	}
?>