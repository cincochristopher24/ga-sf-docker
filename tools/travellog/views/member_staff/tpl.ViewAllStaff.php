<? if (0 < count($arrGroups)) : ?>
	<div class = "section_small_details" style = "border-bottom: none">
		<label for = "other_group_selector"> In Group</label>
		<select name = "other_group_selector" id = "groupselect_regular_staff">
			<? if (1 < count($arrGroups)) : ?>
				<option value = "<?=$groupId?>" <?if($groupId==$chosenId):?>selected<?endif;?>> -- All Groups -- </option>
			<? endif; ?>
			<? foreach($arrGroups as $indSubGroup) : ?>
				<option
						value = "<?=$indSubGroup->getGroupID()?>"
						<?if($chosenId==$indSubGroup->getGroupID()):?>selected<?endif;?>>
						<?=$indSubGroup->getName()?>
				</option>
			<? endforeach; ?>
		</select>
		<input
				type	= "button"
				name	= "btnRefreshList"
				id		= "btnRefreshList"
				value	= "Refresh List" />
		<? if (in_array($viewerType, array(1, 2, 3)) && ($groupId!=$chosenId) && ($loggedUser->isGroupStaff($chosenId))) : ?>
			<span id = "regularstaff_actions" class="actions">
				<a href = "members.php?gID=<?=$groupId?>&mode=4&type=2&cID=<?=$chosenId?>">+ Add Staff</a>
			</span>
		<? endif; ?>
	</div>
	<div class="content">
		<ul id = "regularstaff_list" class = "users member_list">
			<? foreach($arrStaff as $indStaff) : ?>
				<?
					$profile	= $indStaff->getTravelerProfile();
					$username	= $indStaff->getUsername();
					$travelerId	= $indStaff->getTravelerID();
					$email		= $profile->getEmail();
					$thumbNail	= $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$fullname	= $profile->getFirstName() . " " . $profile->getLastName();

					if (isset($staffedGroups)){
						$myStaffedSubGroupList = implode(", ", $staffedGroups[$travelerId]);
						$groupName = $group->getName();
					}
					if (isset($chosenGroupName)){
						$myStaffedSubGroupList = $chosenGroupName;
						$designation_date = MembersPanel::getDesignationDate($travelerId, $chosenId);
						$groupName = $chosenGroupName;
					}

					// check if fullname can be displayed
					$canView = $indStaff->getPrivacyPreference()->canViewNames($travelerId);

					// get travelerID of logged user, if not logged, 0
					$loggedId = ($isLogged) ? $loggedUser->getTravelerID() : 0;
				?>
				<li class = "staff">
					<?php if($travelerId == $administratorID):?>
						<span class = "staff_type"> 
							Owner/Admin
						</span>
					<?php endif ?>
					<a class = "thumb" href = "/<?=$username?>"> 
						<img src = "<?=$thumbNail?>" width = "65" height = "65" alt = "Member Thumbnail"/>
					</a>				
					<div class = "details overflow">
						<a href = "/<?=$username?>" class = "username" title = "<?=$username?>">
							<?=$username?>
							<? if (in_array($viewerType, array(1, 2)) || (3 == $viewerType && $loggedUser->isGroupStaff($chosenId))) : ?>
								<span class = "full_name"><?=$fullname?></span>
							<? else :?>
								<? if ($canView) : ?>
									<span class = "full_name"><?=$fullname?></span>
								<? endif; ?>
							<? endif; ?>
						</a>
						<? if ($chosenId != $groupId) : ?>
							<div class = "designation_date">
								<span>
									Designation Date: 
									<span>
										<? if (0 < strlen($designation_date)) : ?>
											<?=$d->set($designation_date)->friendlyFormat()?>
										<? else : ?>
											<i>[Not Recorded]</i>
										<? endif; ?>
									</span>
								</span>
							</div>
						<? endif; ?>

						<div class = "designation_group">
							<span>
								Assigned to: 
								<span>
									<?=
										(280 < strlen($myStaffedSubGroupList))
										? substr($myStaffedSubGroupList, 0, 280) . '..'
										: $myStaffedSubGroupList
									?>
								</span>
							</span>
						</div>
						<? if ( $isLogged ) : ?>
							<? if ( $loggedId != $travelerId ) : ?>
								<div class = "actions">
									<? if (!$indStaff->isBlocked($loggedId)) : ?>
										<a href = "messages.php?act=messageTraveler&id=<?=$travelerId?>"> Send Message &rarr; </a>
									<? endif; ?>
									<?
										// check remove staff privilege
										if ($group->isParent())
											$isSuperStaff = $group->isSuperStaff($travelerId);
										else
											$isSuperStaff = $group->getParent()->isSuperStaff($travelerId);								
									?>
									<? if (!$isSuperStaff) : ?>
										<? if (in_array($viewerType, array(1, 2, 3)) && ($chosenId != $groupId) && ($loggedUser->isGroupStaff($chosenId))) : ?>
											&nbsp;|
											<a class="remove_staff" href="javascript: void(0)" id="remove_staff_<?=$travelerId?>">Remove Staff</a>
											<input type="hidden" class="remove_staff_name_holder" id="remove_staff_name_holder_<?=$travelerId?>" value="<?=$username?>" />
										<? endif; ?>
									<? endif; ?>
								</div>
							<? else : ?>
								<div class="actions">This is YOU.</div>
							<? endif; ?>
						<? endif; ?>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
		<? if ($paging->getTotalPages() > 1) : ?>
			<div class = "pagination">
				<div class = "page_markers">
					<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalCount?>
				</div>	
				<?$paging->getFirst()?>
				&nbsp;|&nbsp;
				<?$paging->getPrevious()?>
				&nbsp;&nbsp;
				<?$paging->getLinks()?>
				&nbsp;&nbsp;
				<?$paging->getNext()?>
				&nbsp;|&nbsp;
				<?$paging->getLast()?>
			</div>
		<? endif; ?>
	</div>
<? else : ?>
	<div class = "section_small_details" style = "border-bottom: none" style = "text-align: center">
		There are no assigned staff in <?=$group->getName()?> group(s).
	</div>
<? endif; ?>

<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$("#btnRefreshList").click(function(){
				var gId = $('option:selected', $('#groupselect_regular_staff')).val();
				StaffManager.setViewerType(<?=$viewerType?>)
				StaffManager.searchAllStaff( gId );
			});
			$(".remove_staff").click(function(){
				var elId	= this.id;
				var elName	= this.className;
				var travelerId = elId.substr(elName.length+1);
				var username = $("#remove_staff_name_holder_"+travelerId).val();

				StaffManager.removeStaff(travelerId, username, <?='"'.str_replace("'", "\&rsquot;", $groupName).'"'?>);
			});
		});
	})(jQuery);
</script>