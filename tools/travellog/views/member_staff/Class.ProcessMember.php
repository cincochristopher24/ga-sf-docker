<?php

	require_once("Class.Template.php");
	require_once("travellog/model/Class.Traveler.php");
	require_once("travellog/model/Class.AdminGroup.php");
	require_once("travellog/model/Class.Group.php");
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterOp.php');
	require_once('travellog/model/Class.FilterCriteria2.php');

	class ProcessMember{
		
		const REMOVE_MEMBER				=	1;
		const GET_NON_AFFILIATED_GROUPS	=	2;
		const ASSIGN_TO_GROUP			=	3;
		const GET_GROUPS_TO_MANAGE		=	4;
		const UPDATE_GROUP_AFFILIATION	=	5;
		
		private $mGroup		=	NULL;
		private $mMember	=	NULL;
		private $mParams	=	array();
		
		public function __construct(){
		}
		
		public function setParams($params = array() ){
			$default = array(	"action"	=>	NULL,
			 					"tID"		=>	0);
			$this->mParams = array_merge($default,$params);
		}
		
		public function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		
		public function setMember($member=NULL){
			$this->mMember = $member;
		}
		
		public function performAction(){
			switch($this->mParams["action"]){
				case self::REMOVE_MEMBER				:	$this->removeMember();
															break;
				case self::GET_NON_AFFILIATED_GROUPS	:	$this->setupGroupsToManage();
															break;
				case self::ASSIGN_TO_GROUP				:	$this->assignToGroup();
															break;
				case self::GET_GROUPS_TO_MANAGE			:	$this->setupGroupsToManage();
															break;
				case self::UPDATE_GROUP_AFFILIATION		:	$this->updateGroupAffiliation();
															break;
			}
		}
		
		private function removeMember(){
			$this->mMember = new Traveler($this->mParams["tID"]);
			if( ($this->mGroup instanceof Group || $this->mGroup instanceof AdminGroup) && $this->mMember instanceof Traveler ){
				$this->mGroup->removeMember($this->mMember->getTravelerID());
				echo "You have just removed the membership of <strong>".$this->mMember->getFullName()."</strong> in <strong>".$this->mGroup->getName()."</strong>";
			}
		}
		
		private function setupGroupsToManage(){
			$traveler = new Traveler($this->mParams["tID"]);
			
			$cond = new Condition;
			$cond->setAttributeName("name");
			$cond->setOperation(FilterOp::$ORDER_BY);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			$sg_array = $this->mGroup->getSubGroupsAndProjects(NULL,$order);
			$subgroups = $sg_array[0];
			
			$tpl = new Template();
			$tpl->set("traveler",$traveler);
			$tpl->set("subgroups",$subgroups);
			$tpl->set("action",$this->mParams["action"]);
			$tpl->out("travellog/views/member_staff/tpl.ViewGroupAffiliations.php");
		}
		
		private function assignToGroup(){
			$traveler = new Traveler($this->mParams["tID"]);
			$ids = explode(",",$this->mParams["assignTo"]);
			$subgroups = array();
			foreach($ids AS $id){
				$sg = new AdminGroup($id);
				if( $sg instanceof AdminGroup ){
					$sg->addMember($traveler);
					$subgroups[] = $sg;
				}
			}
			unset($id);

			$tpl = new Template();
			$tpl->set("traveler",$traveler);
			$tpl->set("subgroups",$subgroups);
			$tpl->set("action",$this->mParams["action"]);
			$tpl->out("travellog/views/member_staff/tpl.ViewGroupAffiliations.php");
		}
		
		private function updateGroupAffiliation(){
			$removed_from = array();
			$assigned_to = array();
			
			$traveler = new Traveler($this->mParams["tID"]);
			$affiliated = $this->mGroup->getSubGroupsByTravelerID($traveler->getTravelerID(),false);
			$affiliated_ids = array();
			foreach($affiliated AS $group){
				$affiliated_ids[] = $group->getGroupID();
			}
			unset($group);
			
			$ids = explode(",",$this->mParams["assignTo"]);
			
			if(!$ids){
				$removed_from = $affiliated;
				foreach($affiliated AS $group){
					$group->removeMember($traveler->getTravelerID());
				}
				unset($group);
			}else{
				foreach($ids AS $id){
					if( !in_array($id,$affiliated_ids) && "" != $id ){
						$grp = new AdminGroup($id);
						$grp->addMember($traveler);
						$assigned_to[] = $grp;
					}
				}
				unset($id);
				foreach($affiliated_ids AS $id){
					if( !in_array($id,$ids) && "" != $id ){
						$grp = new AdminGroup($id);
						$grp->removeMember($traveler->getTravelerID());
						$removed_from[] = $grp;
					}
				}
				unset($id);
			}
			
			$tpl = new Template();
			$tpl->set("traveler",$traveler);
			$tpl->set("removed_from",$removed_from);
			$tpl->set("assigned_to",$assigned_to);
			$tpl->set("action",$this->mParams["action"]);
			$tpl->out("travellog/views/member_staff/tpl.ViewGroupAffiliations.php");
		}
	}