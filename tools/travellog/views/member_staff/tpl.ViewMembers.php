<?	
	if (isset($group)){
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
		Template::setMainTemplateVar('group', $group);
	}

	if (isset($successfulAdd) && $successfulAdd) : ?>
		<script type = "text/javascript">
			StaffManager.promptSuccessAdd();
		</script>
<? endif; ?>

<?=$mainGroupInfo->render()?>		
<?=$subNavigation->show()?>

<div id = "content_wrapper">
	<div id = "members_staff_rundown">
		<?php if ($this->hasVar('copying_notice_component')): ?>
			<?php $this->getVar('copying_notice_component')->render(); ?>
		<?php endif; ?>
		
		<div id = "wide_column" class = "layout2">
			<p id = "ms_statusPanel" style = "display: none">
	        	Please wait...
	    	</p>
			<div id = "main_content">
				<? if( isset($main_content) ): ?>
					<?=$main_content->render()?>
				<? endif; ?>
				<? if ( isset($pending_member) ): ?>
					<?=$pending_member->render()?>
				<? endif; ?>
			</div>
		</div>
		
		<div id = "narrow_column">
			<div id = "member_staff_nav" class = "section">
				<?	if( !is_null($accordion) ): ?>
					<?=$accordion->render()?>
				<?	endif; ?>
			</div>		
			<?	if( $showHistory ): ?>
				<div id="memberStaffHistoryPanel">
					<div id = "staff_history" class = "section">
						<h2>
							<span>
								<? if( in_array($mode, array(2, 4)) ): ?>
									Staff History
								<? else : ?> 
									Membership History 
								<? endif; ?>
							</span>
						</h2>
						<div class="notification">
							Loading... <img src="/images/load_gray.gif" />
						</div>
						<div class ="foot"></div>
					</div>
					<script type="text/javascript">
						jQuery(document).ready(function(){
							var _timer_ = setTimeout(function(){
				          	  	memberStaffHistoryManager.setMode(<?=$mode?>);
								memberStaffHistoryManager.setGroupID(<?=$groupId?>);
								memberStaffHistoryManager.loadHistory();
				          	  clearTimeout(_timer_);
				            },500);
							/*memberStaffHistoryManager.setMode(<?=$mode?>);
							memberStaffHistoryManager.setGroupID(<?=$groupId?>);
							memberStaffHistoryManager.loadHistory();*/
						});
					</script>
				</div>
			<?	endif; ?>
		</div>

	</div>
</div>