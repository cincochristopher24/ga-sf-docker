<h2><span>Navigation</span></h2>
<div class ="content">
	<dl class = "nav_accord_list">
		<dt>
			<? $isSubGroup = $group->isSubGroup(); ?>
			<a href = "<?=$location . '&mode=1'?>" class = "nav_accord <?if(1==$mode):?>active<?endif?>">
				<div class="arrow"> &larr; </div>
				<? if( 3 > $viewerType || (4 > $viewerType && $isSubGroup) ): ?>
					Your Members
				<? else: ?>
					Members
				<? endif; ?>
			</a>
		</dt>
		<dd id = "desc1" style = "display: <?=(1==$mode)?'block':'none'?>">
			<? if( 3 > $viewerType && !$isSubGroup ): ?>
				Manage your members here. Use the filters to sort your members for easy management.
			<? else: ?>
				<? if( 3 > $viewerType || (4 > $viewerType && $isSubGroup) ): ?>
					Manage your members in <?=stripslashes($group->getName())?>. Use the search box for easy management.
				<? elseif( 4 == $viewerType ): ?>
					Connect with your co-members in <?=stripslashes($group->getName())?>. Use the search box to find people you may know.
				<? else: ?>
					Connect with the members of <?=stripslashes($group->getName())?>. Use the search box to find people you may know.
				<? endif; ?>
			<? endif; ?>
		</dd>
		<? if( 2 == $discriminator ): ?>
			<dt>
				<a href	= "<?=$location . '&mode=2'?>" class = "nav_accord <?if(2==$mode):?>active<?endif?>">
					<div class="arrow"> &larr; </div>
					<? if( 3 > $viewerType): ?>
						Your Staff
					<? else : ?>
						View Staff
					<? endif ?>
				</a>
			</dt>
			<dd id = "desc2" style = "display: <?=(2==$mode)?'block':'none'?>">
				<? if( 3 > $viewerType ): ?>
						Manage your staff here. Administrator can manage all subgroups, while Staff can only manage the subgroups they are assigned to.
				<? elseif( 3 == $viewerType && $isSubGroup ): ?>
						Manage your staff here. Staff can manage the subgroups they are assigned to.
				<? else: ?>
						Connect with the staff of <?=stripslashes($group->getName())?>. 
				<? endif; ?>
			</dd>
		<? endif; ?>
		<? if( $viewerType  < 4 || ( 2 == $discriminator && $group->isSubGroup() && $viewerType < 4 ) ): ?>
			<dt>
				<a href	= "<?=$location . '&mode=3'?>" class = "nav_accord <?if(3==$mode):?>active<?endif?>">
					<div class="arrow"> &larr; </div>
					Invite Members
				</a>
			</dt>	
			<dd id = "desc3" style = "display: <?=(3==$mode)?'block':'none'?>">
					Invite program participants, alumni, co-workers and friends to join your group. Just type in their email addresses and we will send them your invitation.
			</dd>
			<? if( 2 == $discriminator ): ?>
				<dt>
					<a href	= "<?=$location . '&mode=4'?>" class = "nav_accord <?if(4==$mode):?>active<?endif?>">
						<div class="arrow"> &larr; </div>
						Add Staff
					</a>
				</dt>
				<dd id = "desc4" style = "display: <?=(4==$mode)?'block':'none'?>">
						Invite your staff to help you manage your group.
				</dd>
			<? endif; ?>
			<dt>
				<a href	= "<?=$location . '&mode=5'?>" class = "nav_accord <?if(5==$mode):?>active<?endif?>">
					<div class="arrow"> &larr; </div>
					Pending Invitations
				</a>
			</dt>
			<dd id = "desc5" style = "display: <?=(5==$mode)?'block':'none'?>">
					These are the people you have already invited to join your group. You may still cancel the invitation or invite them to another group. You may also follow up on your previous invitation if the person has not yet responded.
			</dd>
			<dt>
				<a href	= "<?=$location . '&mode=6'?>" class = "nav_accord <?if(6==$mode):?>active<?endif?>">
					<div class="arrow"> &larr; </div>
					Requests to Join
				</a>
			</dt>			
			<dd id = "desc6" style = "display: <?=(6==$mode)?'block':'none'?>">
					The profiles that you find here are of GoAbroad Network members who are interested to join your group. You can approve or deny their requests here.
			</dd>
		<? endif; ?>
		<?/*if( 4 > $viewerType ):?>
			<dt>
				<a href	= "<?=$location . '&mode=8'?>" class = "nav_accord <?if(8==$mode):?>active<?endif?>">
					<div class="arrow"> &larr; </div>
					Members Activity Feed <img style="margin-bottom: -3px;" alt="New" src="images/v3/new_badge.gif"/>
				</a>
			</dt>			
			<dd id = "desc6" style = "display: <?=(8==$mode)?'block':'none'?>">
					These are the recent activities of your members.
			</dd>
		<?endif;*/?>
	</dl>
</div>