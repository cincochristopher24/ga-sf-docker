<? $param = 'gID=' . $groupId . '&mode=' . $mode . '&type=' .$type ?>
<div id = "staff_history" class = "section">
	<h2><span><? if ($type==1): ?> Membership History <? else : ?> Staff History <? endif; ?></span></h2>
	<ul id = "nav">
		<li id = "update_history" class = "active">
			<a href = "javascript:void(0)" onclick = "StaffManager.viewStaffMemberUpdates('<?=$param?>')">
				<span>Updates</span>
			</a>
		</li>
		<li id = "denied_history">
			<a href = "javascript:void(0)" onclick = "StaffManager.viewStaffMemberDeniedInvites('<?=$param?>')">
				<span>Denied Invitations</span>
			</a>
		</li>
	</ul>
	<p id = "staff_history_statusPanel" style = "display: none">
        Please wait...
    </p>
	<div id = "staff_history_content"><?=$history->render()?></div>
	<div class = "foot"></div>
</div>