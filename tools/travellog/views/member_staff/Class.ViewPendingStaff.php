<?php
	require_once 'Class.Template.php';
	require_once 'travellog/views/member_staff/Class.PendingStaff.php';

	class ViewPendingStaff{

		private $mGroup			= null;
		private $mPending		= null;
		private $mLoggedUser	= null;
		private $mViewerType	= 0;

		function setGroup($_group = ''){
			$this->mGroup = $_group;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}
		function setViewerType($_viewerType = 0){
			$this->mViewerType = $_viewerType;
		}

		function retrieve(){
			if( 1 == $this->mGroup->getDiscriminator() ){
				return;
			}
			$mGroupId = $this->mGroup->getGroupID();

			$this->mPending = new PendingStaff;
			$this->mPending->setGroup($this->mGroup);
			$this->mPending->setLoggedUser($this->mLoggedUser);
			$this->mPending->setViewerType($this->mViewerType);
			$this->mPending->retrieve();
		}

		function render(){
			if( 1 == $this->mGroup->getDiscriminator() ){
				return;
			}
			$tpl = new Template();

			$tpl->set("group", 			$this->mGroup);
			$tpl->set("pending",		$this->mPending);
			$tpl->set("groupId",		$this->mGroup->getGroupID());

			$tpl->out('travellog/views/member_staff/tpl.ViewPendingInvitations.php');
		}
	}
?>