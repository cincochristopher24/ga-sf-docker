<?php
	/*
	 *	@author CheWyl
	 *	12 November 2008
	 */

	require_once 'Class.Paging.php';
	require_once 'Class.Template.php';

	class ViewAllStaff{

		private $mPage				= 1;
		private $mTotalCount		= 0;
		private $mViewerType		= 0;
		private $mRowsPerPage		= 10;
		private $mChosenGroupId		= 0;
		private $mPaging			= null;
		private $mGroup				= null;
		private $mChosenGroup		= null;
		private $mLoggedUser		= null;
		private $mIsLogged			= false;
		private $isRemove			= false;
		private $arrAllStaff		= array();
		private $mStaffedGroups		= array();

		function setPage($_page = 1){
			$this->mPage = $_page;
		}
		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setChosenGroup($_group = null){
			$this->mChosenGroup = $_group;
		}
		function setLoggedUser($_logged = null){
			$this->mLoggedUser = $_logged;
		}
		function setIsLogged($_isLogged = false){
			$this->mIsLogged = $_isLogged;
		}
		function setViewerType($_viewerType = 0){
			$this->mViewerType = $_viewerType;
		}
		function setRowsPerPage($_rows = 10){
			$this->mRowsPerPage = $_rows;
		}

		function getAllStaffCount(){
			return $this->mAllStaffCount;
		}

		function removeAsStaff($_travelerId = 0){
			if ($this->mChosenGroup)
				$this->mChosenGroup->removeStaff($_travelerId);
		}
		function prepareStaffedGroups(){
			$mySubGroupList = array();

			foreach ($this->arrAllStaff as $eachStaff){
				$arrSubGroup = array();
				$travelerId = $eachStaff->getTravelerID();
				// if traveler is the advisor/administrator of the group, get all subgroups
				if ( $travelerId == $this->mGroup->getAdministratorID() )
					$tempSubGroup = $this->mGroup->getSubGroups();
				else
					$tempSubGroup = Traveler::getAllStaffedSubGroups($travelerId, $this->mGroup->getGroupID());

				foreach ($tempSubGroup as $subGroup)
					$arrSubGroup[] = $subGroup->getName();
				$mySubGroupList[$travelerId] = $arrSubGroup;
			}

			return $mySubGroupList;
		}

		function retrieveAll(){
			// get all staffed sub groups
			$this->arrSubGroups = $this->mGroup->getStaffedSubGroups( false );

			// set rowslimit per page
			$mRowsLimit = new RowsLimit($this->mRowsPerPage, ($this->mPage-1));

			if ($this->mChosenGroup && $this->mChosenGroup->isSubGroup()){
				// get all regular staff
				$this->arrAllStaff = $this->mChosenGroup->getAllGroupStaff($mRowsLimit);
				// merge administrator to the staff list
				$this->arrAllStaff = array_merge(array($this->mGroup->getAdministrator()), $this->arrAllStaff);
				$this->mTotalCount = $this->mChosenGroup->getTotalRecords() + 1;
				$mQueryString = "gID=" . $this->mGroup->getGroupID() . "&mode=2&cID=" . $this->mChosenGroup->getGroupID();
			}
			else{
				$this->arrAllStaff = $this->mGroup->getAllStaff($mRowsLimit);
				// merge administrator to the staff list
				$this->arrAllStaff = array_merge(array($this->mGroup->getAdministrator()), $this->arrAllStaff);
				$this->mTotalCount = $this->mGroup->getTotalRecords() + 1;
				$this->mStaffedGroups = $this->prepareStaffedGroups();		// all staffed groups per traveler
				$mQueryString = "gID=" . $this->mGroup->getGroupID() . "&mode=2";
				$this->mChosenGroup = $this->mGroup;
			}
			$this->mPaging = new Paging( $this->mTotalCount, $this->mPage, $mQueryString, $this->mRowsPerPage );
			$this->mPaging->setScriptName('/members.php');
		}
		function retrieve(){
			// get all staffed sub groups
			$this->arrSubGroups = $this->mGroup->getStaffedSubGroups( false );
			// set rowslimit per page
			$mRowsLimit = new RowsLimit($this->mRowsPerPage, ($this->mPage-1));

			// if in parent group
			if ($this->mChosenGroup->getGroupID() == $this->mGroup->getGroupID()){
				$this->arrAllStaff = $this->mGroup->getAllStaff($mRowsLimit);
				// merge administrator to the staff list
				$this->arrAllStaff = array_merge(array($this->mGroup->getAdministrator()), $this->arrAllStaff);
				$this->mTotalCount = $this->mGroup->getTotalRecords() + 1;
				$this->mStaffedGroups = $this->prepareStaffedGroups();		// all staffed groups per traveler
				$mQueryString = "gID=" . $this->mGroup->getGroupID() . "&mode=2";
			}
			else{
				if ($this->isRemove)
					$this->mChosenGroup = $this->arrSubGroups[0];

				// get all regular staff
				$this->arrAllStaff = $this->mChosenGroup->getAllGroupStaff($mRowsLimit);
				// merge administrator to the staff list
				$this->arrAllStaff = array_merge(array($this->mGroup->getAdministrator()), $this->arrAllStaff);
				$this->mTotalCount = $this->mChosenGroup->getTotalRecords() + 1;
				$mQueryString = "gID=" . $this->mGroup->getGroupID() . "&mode=2&cID=" . $this->mChosenGroup->getGroupID();
			}
			$this->mPaging = new Paging( $this->mTotalCount, $this->mPage, $mQueryString, $this->mRowsPerPage );
			$this->mPaging->setScriptName('/members.php');
		}

		function renderAll(){
			$tpl = new Template();

			$tpl->set("d",					new GaDateTime);
			$tpl->set("group",				$this->mGroup);
			$tpl->set("paging",				$this->mPaging);
			$tpl->set("totalCount",			$this->mTotalCount);
			$tpl->set("arrGroups",			$this->arrSubGroups);
			$tpl->set("viewerType",			$this->mViewerType);
			$tpl->set("arrStaff",			$this->arrAllStaff);
			$tpl->set("isLogged",			$this->mIsLogged);
			$tpl->set("loggedUser",			$this->mLoggedUser);
			$tpl->set("groupId",			$this->mGroup->getGroupID());
			$tpl->set("chosenId",			(!is_null($this->mChosenGroup))?$this->mChosenGroup->getGroupID():$this->mGroup->getGroupID());
			if (count($this->mStaffedGroups))
				$tpl->set("staffedGroups",	$this->mStaffedGroups);
			else
				$tpl->set("chosenGroupName",	$this->mChosenGroup->getName());
			$tpl->set("administratorID",	$this->mGroup->getAdministratorID());

			$tpl->out("travellog/views/member_staff/tpl.ViewAllStaff.php");
		}
		function render(){
			$tpl = new Template();

			$tpl->set("d",					new GaDateTime);
			$tpl->set("group",				$this->mGroup);
			$tpl->set("paging",				$this->mPaging);
			$tpl->set("isLogged",			$this->mIsLogged);
			$tpl->set("totalCount",			$this->mTotalCount);
			$tpl->set("loggedUser",			$this->mLoggedUser);
			$tpl->set("arrStaff",			$this->arrAllStaff);
			$tpl->set("viewerType",			$this->mViewerType);
			$tpl->set("arrGroups",			$this->arrSubGroups);
			$tpl->set("groupId",			$this->mGroup->getGroupID());
			$tpl->set("chosenId",			$this->mChosenGroup->getGroupID());
			if (count($this->mStaffedGroups))
				$tpl->set("staffedGroups",	$this->mStaffedGroups);
			else
				$tpl->set("chosenGroupName",	$this->mChosenGroup->getName());
			$tpl->set("administratorID",	$this->mGroup->getAdministratorID());

			$tpl->out("travellog/views/member_staff/tpl.ViewAllStaff.php");
		}
	}
?>