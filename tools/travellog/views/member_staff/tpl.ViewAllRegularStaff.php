<? if (0 < count($arrStaff)) : ?>
	<div class = "section_small_details" style = "border-bottom: none">
		<? if ((in_array($viewerType, array(1, 2)) || (3 == $viewerType && $loggedUser->isGroupStaff($groupId)))) : ?>
			<span id = "regularstaff_actions" class="actions">
				<a href = "members.php?gID=<?=$groupId?>&mode=4&type=2">+ Add Staff</a>
			</span>
		<? endif; ?>
	</div>
	<div class="content">
		<ul id = "regularstaff_list" class = "users member_list">
			<? foreach($arrStaff as $indStaff) : ?>
				<?
					$profile	= $indStaff->getTravelerProfile();
					$username	= $indStaff->getUsername();
					$travelerId	= $indStaff->getTravelerID();
					$email		= $profile->getEmail();
					$thumbNail	= $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
					$fullname	= $profile->getFirstName() . " " . $profile->getLastName();

					$designation_date = MembersPanel::getDesignationDate($travelerId, $groupId);

					// check if fullname can be displayed
					$canView = $indStaff->getPrivacyPreference()->canViewNames($travelerId);

					// get travelerID of logged user, if not logged, 0
					$loggedId = ($isLogged) ? $loggedUser->getTravelerID() : 0;
				?>
				<li class = "staff">
					<?php if($travelerId == $administratorID):?>
						<span class = "staff_type"> 
							Owner/Admin
						</span>
					<?php endif ?>
					<a class = "thumb" href = "/<?=$username?>"> 
						<img src = "<?=$thumbNail?>" width = "65" height = "65" alt = "Member Thumbnail"/>
					</a>				
					<div class = "details overflow">
						<a href = "/<?=$username?>" class = "username" title = "<?=$username?>">
							<?=$username?>
							<? if (in_array($viewerType, array(1, 2)) || (3 == $viewerType && $loggedUser->isGroupStaff($groupId))) : ?>
								<span class = "full_name"><?=$fullname?></span>
							<? else : ?>
								<? if ($canView || ($loggedId == $travelerId)) : ?>
									<span class = "full_name"><?=$fullname?></span>
								<? endif; ?>
							<? endif; ?>
						</a>
						<div class = "designation_date">
							<span>
								Designation Date: 
								<span>
									<? if (0 < strlen($designation_date)) : ?>
										<?=$d->set($designation_date)->friendlyFormat();?>
									<? else : ?>
										<i>[Not Recorded]</i>
									<? endif; ?>
								</span>
							</span>
						</div>
						<div class = "designation_group">
							<span>
								Assigned to: 
								<span>
									<?=
										(280 < strlen($groupName))
										? substr($groupName, 0, 280) . '..'
										: $groupName
									?>
								</span>
							</span>
						</div>
						<? if ($isLogged && ($loggedId != $travelerId)) : ?>
							<div class = "actions">
								<? if (!$indStaff->isBlocked($loggedId)) : ?>
									<a href = "messages.php?act=messageTraveler&id==<?=$travelerId?>"> Send Message &rarr; </a>
								<? endif; ?>
								<?
									// check remove staff privilege
									if ($group->isParent())
										$isSuperStaff = $group->isSuperStaff($travelerId);
									else
										$isSuperStaff = $group->getParent()->isSuperStaff($travelerId);								
								?>
								<? if (!$isSuperStaff) : ?>
									<? if (in_array($viewerType, array(1, 2)) || (3 == $viewerType && $loggedUser->isGroupStaff($groupId))) : ?>
										&nbsp;|
										<a
											class	= "negative_action"
											href	= "javascript: void(0)"
											onclick = "StaffManager.removeStaff('gID=<?=$groupId?>&mode=2&tID=<?=$travelerId?>&viewerType=<?=$viewerType?>', '<?=$username?>', '<?=str_replace("'", "&rsquot;", $groupName)?>')">
											Remove Staff
										</a>
									<? endif; ?>
								<? endif; ?>
							</div>
						<? elseif ( $loggedId == $travelerId ) : ?>
							<div class="actions">This is YOU.</div>
						<? endif; ?>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
		<? if ($paging->getTotalPages() > 1) : ?>
			<div class = "pagination">
				<div class = "page_markers">
					<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalCount?>
				</div>	
				<?$paging->getFirst()?>
				&nbsp;|&nbsp;
				<?$paging->getPrevious()?>
				&nbsp;&nbsp;
				<?$paging->getLinks()?>
				&nbsp;&nbsp;
				<?$paging->getNext()?>
				&nbsp;|&nbsp;
				<?$paging->getLast()?>
			</div>
		<? endif; ?>
	</div>
<? else : ?>
	<div class = "section_small_details">
		<? if (in_array($viewerType, array(1, 2)) || (3 == $viewerType && $loggedUser->isGroupStaff($groupId))) : ?>
			<p class="help_text"> <span>You have not yet assigned staff for <?=$groupName?></span>.</p>
			<span id = "regularstaff_actions" class="actions">
				<a href = "members.php?gID=<?=$groupId?>&mode=4&type=2">+ Add Staff</a>
			</span>
		<? else : ?>
			<p class="help_text"> <span>There are no staff assigned to <?=$groupName?></span>.</p>
		<? endif; ?>
	</div>
<? endif; ?>