<? if( 0 < $registeredInvitesCount ): ?>
	<div id = "pending_members" class = "section">
		<h2><span>Pending Member Invitations (GA Members)</span></h2>
		<div class="section_small_details">
			Travelers with invitations: <strong><?=$registeredInvitesCount?></strong>
		</div>	
		<div id = "pending_registered_invites" class="content">
			<? if( 0 < $registeredInvitesCount ): ?>
				<?=$pending->renderRegisteredInvitesList()?>
			<? endif; ?>
		</div>
		<div class="foot"></div>
	</div>
<? endif; ?>