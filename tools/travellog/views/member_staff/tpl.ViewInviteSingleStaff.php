<div id="add_staff" class="section">
	<h2>
		<span>Add Staff</span>
	</h2>
	<? if (0 < count($members) || 0 < count($nonMembers)) : ?>
		<div class="content">
			<form name="frmReview" id="frmReview" action="members.php?gID=<?=$groupId?>&mode=4" method="POST">
				<table id="staff_review_form">
					<tbody>
						<tr colspan="2" id="review_tr">
							<td class="checkbox"></td>
							<th>Review Invitation Details</th>
						</tr>
						<? if (0 < count($members)) : ?>
							<? foreach ($members as $indMember) : ?>
								<?
									$mType = $indMember['type'];
									$mSubGroupId = $indMember['subgroupId'];
									$mTraveler = $indMember['traveler'];
									$mSubGroupList = $indMember['subGroupList'];
									$mIsPendingSS = $indMember['isPendingSS'];
									$mIsSuperStaff = $indMember['isSuperStaff'];

									$mProfile = $mTraveler->getTravelerProfile();
									$mTravelerId = $mTraveler->getTravelerID();
									$mUserName = $mTraveler->getUsername();
									$truncate_username = HtmlHelpers::truncateWord($mUserName, 15);
									$thumbNail = $mProfile->getPrimaryPhoto()->getPhotoLink('thumbnail');
									$mFullName = $mProfile->getFirstName() . ' ' . $mProfile->getLastName();
									$mEmail = $mProfile->getEmail();

									if ($group->isParent())
										$mParent = $group;
									else
										$mParent = $group->getParent();

									if ($mTraveler->isStaff() && $mParent->isMember($mTraveler))
										$mDesignation = $mParent->getName() . ' Staff';
									else if ($mParent->isMember($mTraveler))
										$mDesignation = $mParent->getName() . ' Member';
									else
										$mDesignation = 'Existing GoAbroad.Net User';
								?>
								<!-- START registered member -->
								<tr class="registered">
									<td class="checkbox"></td>
									<td class="member_details">
										<div class="users overflow">
											<a href="/<?=$mUserName?>" title="View <?=$mUserName?>'s Pictures" class="thumb">
												<img class="pic" src="<?=$thumbNail?>" alt="<?=$mUserName?>'s thumbnail" />
											</a>
											<div class="details overflow">
												<a href="/<?=$mUserName?>" class="username" title="View <?=$mUserName?>'s Profile">
													<?=stripslashes($mUserName)?>
													<span class="full_name"><?=$mFullName?></span>
												</a>
												<div class="member_email"><?=$mEmail?></div>													
												<div class="hint"><?=$mDesignation?></div>																											
											</div>
											<? if (!$mIsPendingSS && !$mIsSuperStaff) : ?>
												<div class="generic_form_input notopborder" id="staff_type_designation">
													<? if ($group->isParent()) : ?>
														<? if (1 == $viewerType || 2 == $viewerType) : ?>
															<label for="staff_type_select"> Assign As:</label>
															<? if (0 < count($mSubGroupList)) : ?>
																<select name="cmbStaffType_<?=$mTravelerId?>" id="cmbStaffType_<?=$mTravelerId?>" class="cmbStaffType">
																	<option value="1" <?if(1==$mType):?>selected<?endif;?>>Admin</option>
																	<option value="2" <?if(2==$mType):?>selected<?endif;?>>Staff</option>
																</select>
															<? else : ?>
																<select name="cmbStaffType_<?=$mTravelerId?>" id="cmbStaffType_<?=$mTravelerId?>">
																	<option value="1">Admin</option>
																</select>
															<? endif; ?>
														<? else : ?>
															<? if (0 < count($mSubGroupList)) : ?>
																<select name="cmbStaffType_<?=$mTravelerId?>" id="cmbStaffType_<?=$mTravelerId?>">
																	<option value="2">Staff</option>
																</select>
															<? else : ?>
																<div align="center">No more unassigned groups for <?=$mUserName?></div>
															<? endif ?>
														<? endif; ?>
													<? else : ?>
														<? if (0 < count($mSubGroupList)) : ?>
															<select name="cmbStaffType_<?=$mTravelerId?>" id="cmbStaffType_<?=$mTravelerId?>">
																<option value="2">Staff</option>
															</select>
														<? else : ?>
															<? if ($loggedUser->getTravelerID() != $mTravelerId) : ?>
																<div align="center"><?=stripslashes($mUserName)?> is already a staff of <?=stripslashes($group->getName())?></div>
															<? else : ?>
																<div align="center">You are already a staff of <?=stripslashes($group->getName())?></div>
															<? endif; ?>
														<? endif ?>
													<? endif; ?>
												</div>
												<div class="generic_form_input" id="group_designation">
													<? if ($group->isParent()) : ?>
														<? if (1 == $viewerType || 2 == $viewerType) : ?>
															<label for="group_select"> Assign To:</label>
															<select name="cmbGroup_<?=$mTravelerId?>" id="cmbGroup_<?=$mTravelerId?>" class="cmbGroup">
																<option value="<?=$groupId?>" <?if(1==$mType):?>selected<?endif;?>>Main Group</option>
																<? foreach($mSubGroupList as $indGroup) : ?>
																	<option value="<?=$indGroup->getGroupID()?>" <?if(($mSubGroupId==$indGroup->getGroupID())&&(2==$mType)):?>selected<?endif;?>><?=stripslashes($indGroup->getName())?></option>
																<? endforeach; ?>
															</select>
														<? else : ?>
															<? if (0 < count($mSubGroupList)) : ?>
																<select name="cmbGroup_<?=$mTravelerId?>" id="cmbGroup_<?=$mTravelerId?>" class="cmbGroup">
																	<? foreach($mSubGroupList as $indGroup) : ?>
																		<option value="<?=$indGroup->getGroupID()?>" <?if(($mSubGroupId==$indGroup->getGroupID())&&(2==$mType)):?>selected<?endif;?>><?=stripslashes($indGroup->getName())?></option>
																	<? endforeach; ?>
																</select>
															<? endif ?>
														<? endif ?>
													<? else : ?>
														<? if (0 < count($mSubGroupList)) : ?>
															<select name="cmbGroup_<?=$mTravelerId?>" id="cmbGroup_<?=$mTravelerId?>" class="cmbGroup">
																<option value="<?=$groupId?>"><?=stripslashes($group->getName())?></option>
															</select>
														<? endif ?>
													<? endif; ?>
												</div>
											<? else : ?>
												<div class="generic_form_input notopborder" id="staff_type_designation" style="text-align: center">
													<? if ($group->isParent()) : ?>
														No more unassigned groups for <?=$mUserName?>
													<? else : ?>
														<? if ($loggedUser->getTravelerID() != $mTravelerId) : ?>
															<?=stripslashes($mUserName)?> is already a staff of <?=stripslashes($group->getName())?>
														<? else : ?>
															You are already a staff of <?=stripslashes($group->getName())?>
														<? endif; ?>
													<? endif ?>
												</div>
											<? endif; ?>
										</div>
								</tr>
								<!-- END registered member -->
							<? endforeach; ?>
						<?endif; ?>

						<? if (0 < count($nonMembers)) : ?>
							<? $ctr = 1 ?>
							<? foreach ($nonMembers as $indNonMember) : ?>
								<?
									$mType			= $indNonMember['type'];
									$mEmail			= $indNonMember['email'];
									$mSubGroupId	= $indNonMember['subgroupId'];
									$mLastName		= isset($indNonMember['lastname']) ? $indNonMember['lastname'] : '';
									$mUserName		= isset($indNonMember['username']) ? $indNonMember['username'] : '';
									$mPassword		= isset($indNonMember['password']) ? $indNonMember['password'] : '';
									$mFirstName		= isset($indNonMember['firstname']) ? $indNonMember['firstname'] : '';
								?>
								<!-- START non registered user -->
								<tr class="non_registered">
									<td class="checkbox"></td>
									<td class="member_details">
										<div class="users overflow">
											<a class="thumb" title="View Picture" href="#">
												<img class="pic" alt="non-member" src="/images/default_images/user_thumb.jpg" />
											</a>
											<div class="details overflow">																										
												<div class="nonregistered_email"><?=$mEmail?></div>
												<div class="hint">Non-registered GoAbroad.net User</div>																																							
											</div>
											<div class="generic_form_input notopborder" id="staff_type_designation">
												<label for="staff_type_select">Assign As:</label>
												<? if ($group->isParent()) : ?>
													<? if (1 == $viewerType || 2 == $viewerType) : ?>
														<select name="cmbStaffType_temp<?=$ctr?>" id="cmbStaffType_temp<?=$ctr?>" class="cmbStaffType_temp">
															<option value="1" <?if(1==$mType):?>selected<?endif;?>>Admin</option>
															<option value="2" <?if(2==$mType):?>selected<?endif;?>>Staff</option>
														</select>
													<? else : ?>
														<select name="cmbStaffType_temp<?=$ctr?>" id="cmbStaffType_temp<?=$ctr?>">
															<option value="2">Staff</option>
														</select>
													<? endif; ?>
												<? else : ?>
														<select name="cmbStaffType_temp<?=$ctr?>" id="cmbStaffType_temp<?=$ctr?>">
															<option value="2">Staff</option>
														</select>
												<? endif; ?>
											</div>					
											<div class="generic_form_input" id="group_designation">
												<label for="group_select">Assign To:</label>
												<? if ($group->isParent()) : ?>
													<select name="cmbGroup_temp<?=$ctr?>" id="cmbGroup_temp<?=$ctr?>" class="cmbGroup_temp">
														<? if (1 == $viewerType || 2 == $viewerType) : ?>
															<option value="<?=$groupId?>" <?if(1==$mType):?>selected<?endif;?>>Main Group</option>
														<? endif; ?>
														<? foreach($subGroups as $indGroup) : ?>
																<option
																		value="<?=$indGroup->getGroupID()?>"
																		<?if(($mSubGroupId==$indGroup->getGroupID())&&(2==$mType)):?>selected<?endif;?>>
																		<?=stripslashes($indGroup->getName())?>
																</option>
														<? endforeach; ?>
													</select>
												<? else : ?>
													<select name="cmbGroup_temp<?=$ctr?>" id="cmbGroup_temp<?=$ctr?>" class="cmbGroup_temp">
														<option value="<?=$groupId?>"><?=stripslashes($group->getName())?></option>
													</select>
												<? endif; ?>
											</div>
											<div class="generic_form_input" id="name_nonregistered">
												<label>First Name:</label> 
												<input
														type	= "text"
														class	= "textbox"
														id		= "txtFirstName_temp<?=$ctr?>"
														name	= "txtFirstName_temp<?=$ctr?>"
														value	= "<?=$mFirstName?>" />
												<? if (0 < count($errors) && isset($errors['txtFirstName_temp'.$ctr])) : ?>
													<div class="errors">
														<span><?=$errors['txtFirstName_temp'.$ctr]?></span>
													</div>
												<? endif; ?>
											</div>
											<div class="generic_form_input" id="name_nonregistered">
												<label>Last Name:</label> 
												<input
														type	= "text"
														class	= "textbox"
														id		= "txtLastName_temp<?=$ctr?>"
														name	= "txtLastName_temp<?=$ctr?>"
														value	= "<?=$mLastName?>" />
												<? if (0 < count($errors) && isset($errors['txtLastName_temp'.$ctr])) : ?>
													<div class="errors">
														<span><?=$errors['txtLastName_temp'.$ctr]?></span>
													</div>
												<? endif; ?>
											</div>
											<div class="generic_form_input" id="username_nonregistered">
												<label>Username:</label>
												<input
														type	= "text"
														class	= "textbox"
														id		= "txtUserName_temp<?=$ctr?>"
														name	= "txtUserName_temp<?=$ctr?>"
														value	= "<?=$mUserName?>" 
														maxlength = "25" />
												<? if (0 < count($errors) && isset($errors['txtUserName_temp'.$ctr])) : ?>
													<div class="errors">
														<span><?=$errors['txtUserName_temp'.$ctr]?></span>
													</div>
												<? endif; ?>
												<span class="label_small_details">Please use only letters, numbers or underscore. Username must be one word only.</span>
											</div>
											<div class="generic_form_input" id="password_nonregistered">
												<label>Password:</label>
												<input
														type	= "text"
														class	= "textbox"
														id		= "txtPassword_temp<?=$ctr?>"
														name	= "txtPassword_temp<?=$ctr?>"
														value	= "<?=$mPassword?>" />
												<? if (0 < count($errors) && isset($errors['txtPassword_temp'.$ctr])) : ?>
													<div class="errors">
														<span><?=$errors['txtPassword_temp'.$ctr]?></span>
													</div>
												<? endif; ?>
											</div>
										</div>
									</td>
								</tr>
								<!-- END non registered user -->
								<? $ctr+=1 ?>
							<? endforeach; ?>
						<? endif; ?>
					</tbody>
				</table>

				<? if ($isProceedInvite) : ?>
					<div id="msg_preview" class="section">
						<h2>
							<span>Message Preview</span>
							<a href="javascript:void(0)" onclick="StaffManager.clickEditLink()">
								<span class="actions" id="action_message">Edit Message</span>
							</a>
						</h2>
						<div class="content">
							<p id="notification_LoadingIndicator" style="display: none; margin-left: 230px; margin-top: 50px;">
						        Please wait...
						    </p>
							<div id="display_notification_message"><?=$notification->render()?></div>
							<p id="action_add_staff" style="margin-left:230px">
								<input type="submit" name="btnAdd" id="btnAdd" value="Add Staff" />
								<input type="submit" name="btnCancel" id="btnCancel" value="Cancel" />
								<? if (0 < count($arrTravelerId)) : ?>
									<input type="hidden" name="hdnTravelerId" id="hdnTravelerId" value="<?=implode(",", $arrTravelerId)?>">
								<? endif; ?>
								<? if (0 < count($arrEmail)) : ?>
									<input type="hidden" name="hdnEmailAdd" id="hdnEmailAdd" value="<?=implode(",", $arrEmail)?>">
								<? endif; ?>
							</p>
						</div>
					</div>
				<? endif; ?>
			</form>
		</div>
	<? else : ?>
		<div class="content">
			<table id="staff_review_form">
				<tbody>
					<tr colspan="2" id="review_tr">
						<th style="text-align: center">You can't add a group administrator as a staff.</th>
					</tr>
				</tbody>
			</table>
		</div>
	<? endif; ?>
</div>

<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$("#chkToggleAll").click(function(){
				StaffManager.toggleAll();
			});
		});
		$(".cmbStaffType").change(function(){
			var elId	= this.id;
			var elName	= this.className;
			var travelerId = elId.substr(elName.length+1);

			var iType = $('option:selected', this).val();
			StaffManager.changeGroup( $("#cmbGroup_"+travelerId), iType, 1 );
		});
		$(".cmbStaffType_temp").change(function(){
			var elId	= this.id;
			var elName	= this.className;
			var counter	= elId.substr(elName.length+1);

			var iType = $('option:selected', this).val();
			StaffManager.changeGroup( $("#cmbGroup_temp"+counter), iType, 1 );
		});
		$(".cmbGroup").change(function(){
			var elId	= this.id;
			var elName	= this.className;
			var travelerId = elId.substr(elName.length+1);

			var gId	= $('option:selected', this).val();
			StaffManager.changeStaffType( $("#cmbStaffType_"+travelerId), gId, 1 );
		});
		$(".cmbGroup_temp").change(function(){
			var elId	= this.id;
			var elName	= this.className;
			var counter	= elId.substr(elName.length+1);

			var gId	= $('option:selected', this).val();
			StaffManager.changeStaffType( $("#cmbStaffType_temp"+counter), gId, 1 );
		});
	})(jQuery);
</script>