<?php
	require_once 'Class.Template.php';
	require_once 'travellog/views/member_staff/Class.JoinRequest.php';

	class ViewJoinRequest{

		private $mGroup			= NULL;
		private $mJoinRequest	= NULL;
		private $mLoggedUser	= NULL;
		private $mViewerType	= 1;

		function setGroup($_group = ''){
			$this->mGroup = $_group;
		}
		function setLoggedUser($user=NULL){
			$this->mLoggedUser = $user;
		}
		function setViewerType($viewerType=1){
			$this->mViewerType = $viewerType;
		}

		function retrieve(){
			$this->mJoinRequest = new JoinRequest;
			$this->mJoinRequest->setViewerType($this->mViewerType);
			$this->mJoinRequest->setLoggedUser($this->mLoggedUser);
			$this->mJoinRequest->setGroup($this->mGroup);
		}

		function render(){
			$this->mJoinRequest->performAction();
		}
	}