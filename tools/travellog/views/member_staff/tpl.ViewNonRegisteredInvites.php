<? if( 0 < $nonRegisteredInvitesCount): ?>
	<div id = "pending_members" class = "section">
		<h2><span>Pending Member Invitations (Non-GA Members)</span></h2>
		<div class="section_small_details">
			Non-GA Members sent with membership invitations: <strong><?=$nonRegisteredInvitesCount?></strong>
		</div>	
		<div id = "pending_nonregistered_invites" class="content">
			<? if( 0 < $nonRegisteredInvitesCount ): ?>
				<?=$pending->renderNonRegisteredInvitesList()?>
			<? endif; ?>
		</div>
		<div class="foot"></div>
	</div>
<? endif; ?>