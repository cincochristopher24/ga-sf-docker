<div id="add_staff" class="section">
	<h2>
		<span>Add Staff</span>
	</h2>
	<!--div class="section_small_details"> 
		Invite your staff to help you manage your group. 
	</div-->
	<div class="content">
		<table id="staff_add_form">
			<tbody>
				<form name="frmInvite" id="frmInvite" action="members.php?gID=<?=$groupId?>&mode=4" method="POST">
					<tr colspan="2">
						<td class="left_side">
							<label for="email_textarea">Email Address</label> <span class="required">*</span>
							<span class="label_small_details">
								Please enter email addresses separated by commas.
							</span>
						</td>
						<td class="right_side"> 
							<textarea name="email_textarea" class="members_invite_textarea"><?=$emails?></textarea>
							<? if (0 < count($errors) && isset($errors['email'])) : ?>
								<div class="errors" style="_">
									<span><?=$errors['email']?></span>
								</div>
							<? endif; ?>							
						</td>
					</tr>		
					<tr colspan="2">
						<td class="left_side">
							<label for="cmbStaffType">Assign As</label> <span class="required">*</span>
							<span class="label_small_details">
								What type of staff?
								<br />
								<!--a href="#"> Learn more about Staff </a-->
							</span>											
						</td>
						<td class="right_side">
							<? if ($group->isParent()) : ?>
								<? if (1 == $viewerType || 2 == $viewerType) : ?>
									<? if (0 < count($subGroups)) : ?>
										<select name="cmbStaffType" id="cmbStaffType" class="_">
											<option value="0">Select Staff Type</option>
											<option value="1" <?if(1==$type):?>selected<?endif;?>>Admin</option>
											<option value="2" <?if(2==$type):?>selected<?endif;?>>Staff</option>
										</select>
									<? else : ?>
										<select name="cmbStaffType" id="cmbStaffType" class="_">
											<option value="1">Admin</option>
										</select>
									<? endif; ?>
								<? else : ?>
									<select name="cmbStaffType" id="cmbStaffType" class="_">
										<option value="2">Staff</option>
									</select>
								<? endif; ?>
							<? else : ?>
								<select name="cmbStaffType" id="cmbStaffType" class="_">
									<option value="2">Staff</option>
								</select>
							<? endif; ?>
							<? if (0 < count($errors) && isset($errors['staff_type'])) : ?>
									<div class="errors" style="_">
										<span><?=$errors['staff_type']?></span>
									</div>
							<? endif; ?>
						</td>
					</tr>
					<tr colspan="2">
						<td class="left_side">
							<label for="cmbGroup">Assign to Group</label> <span class="required">*</span>
							<span class="label_small_details">
								Where do you want to assign them?
								<br />												
								<!--a href="#"> Learn more about Groups </a-->
							</span>											
						</td>
						<td class="right_side">
							<? if ($group->isParent()) : ?>
								<? if (1 == $viewerType || 2 == $viewerType) : ?>
									<? if (0 < count($subGroups)) : ?>
										<select name="cmbGroup" id="cmbGroup" class="_">
											<option value="0">Select Group</option>
											<option value="<?=$group->getGroupID()?>" <?if(1==$type):?>selected<?endif;?>>Main Group</option>
											<? foreach($subGroups as $indGroup) : ?>
												<option
														value="<?=$indGroup->getGroupID()?>"
														<?if(($subGroupId==$indGroup->getGroupID())&&(2==$type)):?>selected<?endif;?>>
														<?=$indGroup->getName()?>
												</option>
											<? endforeach; ?>
										</select>
									<? else : ?>
										<select name="cmbGroup" id="cmbGroup" class="_">
											<option value="<?=$groupId?>">Main Group</option>
										</select>
									<? endif; ?>
								<? else : ?>
									<select name="cmbGroup" id="cmbGroup" class="_">
										<? if (1 < count($subGroups)) : ?>
											<option value="0">Select Group</option>
										<? endif; ?>
										<? foreach($subGroups as $indGroup) : ?>
											<option
													value="<?=$indGroup->getGroupID()?>"
													<?if(($subGroupId==$indGroup->getGroupID())&&(2==$type)):?>selected<?endif;?>>
													<?=$indGroup->getName()?>
											</option>
										<? endforeach; ?>
									</select>
								<? endif; ?>
							<? else : ?>
								<select name="cmbGroup" id="cmbGroup" class="_">
									<option value="<?=$groupId?>"><?=$group->getName()?></option>
								</select>
							<? endif; ?>
							<? if (0 < count($errors) && isset($errors['group'])) : ?>
									<div class="errors" style="_">
										<span><?=$errors['group']?></span>
									</div>
							<? endif; ?>
						</td>
					</tr>
					<tr colspan="2">
						<td class="left_side"></td>
						<td class="right_side">
							<input type="submit" name="btnInvite" id="btnInvite" value="Invite" />
						</td>
					</tr>
				</form>
			</tbody>	
		</table>
	</div>
</div>

<? if (1 == $viewerType || 2 == $viewerType) : ?>
	<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				$("#cmbStaffType").change(function(){
					var iType = $('option:selected', this).val();
					StaffManager.changeGroup( $("#cmbGroup"), iType, 0 );
				});
				$("#cmbGroup").change(function(){
					var gId	= $('option:selected', this).val();
					StaffManager.changeStaffType( $("#cmbStaffType"), gId, 0 );
				});
			});
		})(jQuery);
	</script>
<? endif ?>