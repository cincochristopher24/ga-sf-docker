<ul id="search_result_list" class="users member_list">
	
<? if( $isAdminGroup ): ?>
	<?php
		$hasSubGroups = FALSE;
		if( $group->isParent() ){
			try{
				$hasSubGroups = 0 < $group->getSubGroupsCount() ? TRUE : FALSE;
			}catch(exception $e){
			}
		}
	?>
	<?	$pgroup_name = str_replace('-', '_', $group->getName());
		$pgroup_name = str_replace(' ', '-', $pgroup_name);
		
		for ($i=0; $i<sizeof($members); $i++ ){
			$member = $members[$i];
			if($member->isDeactivated()){
				continue;
			}
			$isSuperStaff = FALSE;
			$isStaff = FALSE;
			$staffType = "";
			$class = "member";
			
			$isPendingMember = FALSE;
			if( $group->isMember($member) ){
				if( $group->isParent() ){
					$isStaff = $group->isRegularStaff($member->getTravelerID());
					$isSuperStaff = $group->isSuperStaff($member->getTravelerID());
				}elseif( $group->getAdministratorID() == $member->getTravelerID() ){
					$isStaff = TRUE;
					$isSuperStaff = TRUE;
				}else{
					$isStaff = $group->isStaff($member->getTravelerID());
					$isSuperStaff = $parent->isSuperStaff($member->getTravelerID());
				}
			}else{
				$isPendingMember = TRUE;
			}
			
			$class = ($isStaff)? "staff" : "member";
			$class = ($isSuperStaff)? "staff" : $class;
			
			if( $isPendingMember ){
				$class = "staff";
			}
			if ( 0 == $i ){
				$class .= " first";
			}
			
			if( $isPendingMember ){
				$staffType = "Pending";
			}elseif( $group->getAdministratorID() == $member->getTravelerID() ){
				if( 1 == $viewerType ){
					$class = "staff";
					$staffType = "Owner/Admin";
				}else{
					$class = "member";
				}
			}else{
				$staffType = $isStaff ? "Staff" : "";
				$staffType = $isSuperStaff ? "Admin" : $staffType;
			}
			if( 2 < $viewerType && "" != $staffType && !$isPendingMember ){
				$staffType = "Staff";
			}
			
			$joinedGroups = array();
			if( !$isPendingMember && $group->isParent() && !$isSuperStaff ){
				//$joinedGroups = $group->getSubGroupsByTravelerID($member->getTravelerID(), TRUE);
				$joinedGroups = $group->getSubgroupAffiliations($member->getTravelerID());
			}
			$membership_date = "";
			if( $isNewMemberDisplay || !$group->isParent() ){
				try{
					$memberDateAdded = $group->getMembershipDate($member);
					if( is_null($memberDateAdded) || "0000-00-00 00:00:00" == $memberDateAdded ){
						$membership_date = "";
					}else{
						$tokens = explode(" ",$memberDateAdded);
						$dt = explode("-",$tokens[0]);
						$tt = explode(":",$tokens[1]);
						$date = getdate(mktime($tt[0],$tt[1],$tt[2],$dt[1],$dt[2],$dt[0]));
						$membership_date = $date["month"]." ".$date["mday"].", ".$date["year"];
					}
				}catch(exception $e){
					$membership_date = $e->getMessage();
				}
			}

			$canView = $member->getPrivacyPreference()->canViewNames($member->getTravelerID());
	?>
	
	<li class="<?=$class?> <?if($isPendingMember && 2 >= $viewerType):?>pending_member<?endif?>"  >
	
		<span class="staff_type"> 
			<?=$staffType?>
		</span>
		<? $server = $_SERVER["SERVER_NAME"]; ?>
		<a class="thumb" href="http://<?=$server.'/'.$member->getUserName()?>"> 
			<img src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width="65" height="65" alt="<?=$member->getUserName()?>"/>
		</a>										
		<div class="details overflow">
			<?	if( !$isPendingMember ): ?>
				<a href="http://<?=$server.'/'.$member->getUserName()?>" class="username" title="<?=$member->getUserName()?>" >
					<?=$member->getUserName()?>
					<? if( 4 > $viewerType || ( 3 < $viewerType && $canView ) ): ?>
						&nbsp;&nbsp;<span class="full_name"><?=stripslashes($member->getFullName())?></span>
					<? endif; ?>
				</a>			
				<div class="group_affiliations">
					<? if( "" != $membership_date && ($isNewMemberDisplay || !$group->isParent()) && (3 > $viewerType || (3 == $viewerType && $group->isSubGroup())) ): ?>
							<div>
								<? if( $accessedGroup->isParent() ): ?>
									<span> Member of <?=$group->getName()?> Since:&nbsp;&nbsp;<?=$membership_date?></span>
								<? else: ?>
									<span> Member Since:&nbsp;&nbsp;<?=$membership_date?></span>
								<? endif; ?>
							</div>
					<? endif; ?>
					<? if ( 0 < sizeof($joinedGroups) ): ?>
						<? if( $group->isParent() && !$isSuperStaff ): ?>
							<span> Group Affiliations: (<?=sizeof($joinedGroups)?>)</span>
							<ul id="affiliation_summary_<?=$member->getTravelerID()?>">
								<? for($j=0; $j<sizeof($joinedGroups) && 10 > $j; $j++ ){
										$grp_affiliation = $joinedGroups[$j];
										$sgroup_name = str_replace('-', '_', $grp_affiliation);
										$sgroup_name = str_replace(' ', '-', $sgroup_name);
																		
										$affiliationURL = "/groups/". str_replace(' ','-', $pgroup_name) . "/subgroup/" .str_replace(' ','-',$sgroup_name) ;
								?>
								<li> <a href="<?=$affiliationURL?>"><?=stripslashes($grp_affiliation)?></a>
									<? if( $j < 9 && $j < sizeof($joinedGroups)-1 ): ?>
									,&nbsp;
									<? endif; ?>
									<? if( $j == 9 && 10 < sizeof($joinedGroups) ): ?>
									&nbsp;
									<i>(<a href="" onclick="gmsManager.hide('affiliation_summary_<?=$member->getTravelerID()?>'); gmsManager.show('affiliation_all_<?=$member->getTravelerID()?>'); return false;">
									more &darr; 
									</a>)</i>
									<? endif; ?>
								</li>
				
								<? } ?>													
							</ul>
							<? if( 10 < sizeof($joinedGroups) ): ?>
								<ul id="affiliation_all_<?=$member->getTravelerID()?>" style="display:none;">
									<? for($j=0; $j<sizeof($joinedGroups); $j++ ){
											$grp_affiliation = $joinedGroups[$j];
											$sgroup_name = str_replace('-', '_', $grp_affiliation);
											$sgroup_name = str_replace(' ', '-', $sgroup_name);
											
											$affiliationURL = "/groups/". str_replace(' ','-', $pgroup_name) . "/subgroup/" .str_replace(' ','-',$sgroup_name) ;	
									?>
									<li> <a href="<?=$affiliationURL?>"><?=stripslashes($grp_affiliation)?></a>
										<? if( $j < sizeof($joinedGroups)-1 ): ?>
										,&nbsp;
										<? endif; ?>
										<? if( $j == sizeof($joinedGroups)-1 ): ?>
										&nbsp;
										<i>(<a href="" onclick="gmsManager.hide('affiliation_all_<?=$member->getTravelerID()?>'); gmsManager.show('affiliation_summary_<?=$member->getTravelerID()?>'); return false;">
										hide &uarr;
										</a>)</i>
										<? endif; ?>
									</li>
									<? } ?>
								</ul>
							<? endif; ?>
						<? endif; ?>
					<? elseif( $hasSubGroups ): ?>
						<? if( $group->isParent() && !$isSuperStaff ): ?>
							<span> Group Affiliations other than the Main Group: </span>
							<ul id="affiliation_summary_<?=$member->getTravelerID()?>">
								<li> None </li>
							</ul>
						<? endif; ?>
						<? if( $group->isParent() && $isSuperStaff ): ?>
							<span> Group Affiliations: All groups</span>
						<? endif; ?>
					<? endif; ?>
				</div>
			<?	else: ?><?/* if member has pending invitation */?>
				<a href="http://<?=$server.'/'.$member->getUserName()?>" class="username" title="<?=$member->getUserName()?>" >
					<?=$member->getUserName()?>
					<? if( 4 > $viewerType || ( 3 < $viewerType && $canView ) ): ?>
						&nbsp;&nbsp;<span class="full_name"><?=stripslashes($member->getFullName())?></span>
					<? endif; ?>
				</a>
				<div class="group_affiliations">
					<div>
						<span>Member has yet to confirm the group invitation.</span>
					</div>
				</div>
			<?	endif; ?>
			<!-- END group_affiliations -->
			<? if( $viewerType < 6 && !is_null($loggedUser) && $loggedUser->getTravelerID() != $member->getTravelerID() ): ?>
				<?php
					// removed condition for showing assign to group link if group has no subgroup
					$hasAssignToGroupLink = !$isPendingMember && $accessedGroup->isParent() && $viewerType < 3 && $group->getAdministratorID() != $member->getTravelerID() ? TRUE : FALSE;
					// $hasAssignToGroupLink = !$isPendingMember && $accessedGroup->isParent() && $viewerType < 3 && $group->getAdministratorID() != $member->getTravelerID() && $hasSubGroups ? TRUE : FALSE;
					if( $group->isParent() ){
						$hasSendMessageLink = !$isPendingMember && $viewerType < 3 && !$loggedUser->isBlocked($member->getTravelerID()) ? TRUE : FALSE;
					}else{
						$hasSendMessageLink = !$isPendingMember && $viewerType < 4 && !$loggedUser->isBlocked($member->getTravelerID()) ? TRUE : FALSE;
					}
					$hasRemoveMemberLink = !$isPendingMember && ( 1 == $viewerType || ( 2 == $viewerType && !$isSuperStaff ) || ( !$accessedGroup->isParent() && $accessedGroup->isStaff($loggedUser->getTravelerID()) && !$isSuperStaff ) ) && $group->getAdministratorID() != $member->getTravelerID() ? TRUE : FALSE;
				?>
				<?php if( $hasAssignToGroupLink || $hasSendMessageLink || $hasRemoveMemberLink ): ?>
					<div class="actions">
						<?php $hasActionLinks = FALSE; ?>
						
						<? if( $hasAssignToGroupLink ): ?>
							<? if( (0 < sizeof($joinedGroups) || !$group->isParent()) && !$isSuperStaff ): ?>
								<a href="javascript:void(0)" onclick="gmsManager.getGroupsToManage(<?=$member->getTravelerID()?>); return false;">Manage Group Affiliations</a> | 
							<? else:  ?>
								<? if( !$isSuperStaff ): ?>
									<a href="javascript:void(0)" onclick="gmsManager.getAssignToGroups(<?=$member->getTravelerID()?>); return false;">Assign to Group</a> | 
								<? endif; ?>
							<? endif; ?>
							<?php $hasActionLinks = TRUE; ?>
						<? endif; ?>
						<? if( $hasSendMessageLink ): ?>
							<a href="http://<?=$server?>/messages.php?act=messageTraveler&id=<?=$member->getTravelerID()?>" >Send Message &rarr;</a>
							<?php $hasActionLinks = TRUE; ?>
						<? endif; ?>
						<? if( $hasRemoveMemberLink ): ?>
							<?php if( $hasActionLinks ): ?> | <?php endif; ?>
							<a class="negative_action" href="javascript:void(0)" onclick="gmsManager.removeMember(<?=$member->getTravelerID()?>,'<?=$member->getFullName()?>','<?=str_replace("'","&rsquot;",$accessedGroup->getName())?>'); return false;">Remove Membership</a>
						<? endif; ?>
						
					</div><!-- END actions -->
				<?php endif; ?>
			<? endif; ?>
			<? if($viewerType < 6 && !is_null($loggedUser) && $loggedUser->getTravelerID() == $member->getTravelerID() ): ?>
				<div class="actions">
					This is YOU.
				</div>
			<? endif; ?>																										
		</div>
		<!-- END details overflow -->
	</li>
	<!-- END staff -->

	<? } ?>
	
<? else: ?>

	<?for ($i=0; $i<sizeof($members); $i++ ){
		$member = $members[$i];
		if($member->isDeactivated()){
			continue;
		}
		$staffType = "";
		$class = "member";
		if ( 0 == $i ){
			$class .= " first";
		}
		$membership_date = "";
		try{
			$memberDateAdded = $group->getMembershipDate($member);
			if( is_null($memberDateAdded) || "0000-00-00 00:00:00" == $memberDateAdded ){
				$membership_date = "";
			}else{
				$tokens = explode(" ",$memberDateAdded);
				$dt = explode("-",$tokens[0]);
				$tt = explode(":",$tokens[1]);
				$date = getdate(mktime($tt[0],$tt[1],$tt[2],$dt[1],$dt[2],$dt[0]));
				$membership_date = $date["month"]." ".$date["mday"].", ".$date["year"];
			}
		}catch(exception $e){
			$membership_date = $e->getMessage();
		}
		
		$canView = $member->getPrivacyPreference()->canViewNames($member->getTravelerID());
	?>
	
	<li class="<?=$class?>">
	
		<span class="staff_type"> 
			<?=$staffType?>
		</span>
			<? $server = $_SERVER["SERVER_NAME"]; ?>
		<a class="thumb" href="http://<?=$server.'/'.$member->getUserName()?>"> 
			<img src="<?=$member->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width="65" height="65" alt="Member Thumbnail"/>
		</a>												
	
		<div class="details overflow">
				<a href="http://<?=$server.'/'.$member->getUserName()?>" class="username" title="<?=$member->getUserName()?>" >
					<?=$member->getUserName()?>
					<? if( 4 > $viewerType || $canView ): ?>
						&nbsp;&nbsp;<span class="full_name"><?=$member->getFullName()?></span>
					<? endif; ?>
				</a>
			<? if( 3 > $viewerType && "" != $membership_date ) : ?>			
				<div class="group_affiliations">
					<div><span> Member Since:&nbsp;&nbsp;<?=$membership_date?></span></div>
				</div>
			<? endif; ?>
			<!-- END group_affiliations -->
			<? if( $viewerType < 6 && !is_null($loggedUser) && $loggedUser->getTravelerID() != $member->getTravelerID() ): ?>
				<?php
					$hasSendMessageLink = 1 == $viewerType && !is_null($loggedUser) && $loggedUser->getTravelerID() != $member->getTravelerID() ? TRUE : FALSE;
					$hasRemoveMemberLink = 1 == $viewerType && $member->getTravelerID() != $group->getAdministratorID() ? TRUE : FALSE;
				?>
				<?php if( $hasSendMessageLink || $hasRemoveMemberLink ): ?>
					<div class="actions">
						<? if( $hasSendMessageLink ): ?>
						<a href="http://<?=$server?>/messages.php?act=messageTraveler&ig=<?=$member->getTravelerID()?>" >Send Message &rarr;</a>
						<? endif; ?>
						<? if( $hasRemoveMemberLink ): ?>
						 | <a class="negative_action" href="javascript:void(0)" onclick="gmsManager.removeMember(<?=$member->getTravelerID()?>,'<?=$member->getFullName()?>','<?=str_replace("'","&rsquot;",$accessedGroup->getName())?>'); return false;">Remove Membership</a>
						<? endif; ?>																												
					</div><!-- END actions -->
				<?php endif; ?>	
			<? endif; ?>
			<? if( $viewerType < 6 && !is_null($loggedUser) && $loggedUser->getTravelerID() == $member->getTravelerID() ): ?>
				<div class="actions">
					This is YOU.																												
				</div><!-- END actions -->	
			<? endif; ?>																									
		</div>
		<!-- END details overflow -->
	</li>
	
	
	<? } ?>

<? endif; ?>	
	
</ul>


<img id="loading_page" src="/images/loading_small.gif" style="display:none;"/>&nbsp;
<? if ( $paging->getTotalPages() > 1 ):?>
	<? $start = $paging->getStartRow() + 1; 
	   $end = $paging->getEndRow(); 
	?>
	<? $paging->showPagination() ?>
<? endif; ?>