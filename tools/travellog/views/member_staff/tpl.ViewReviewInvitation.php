<div id="invite_members" class="section">
	<h2>
		<span>Finalize and Review Invitation</span>			
	</h2> 								
	
	<div class="content">
		<form id="review_invitation_form" name="review_invitation_form" action="/members.php?gID=<?=$group_object->getGroupID()?>&mode=3" method="post">
		<table id="members_review_form">
			<tbody>
			<?	if( $error != InviteMember::NO_ERROR ): ?>
				<tr>
					<td colspan="2">
						<div class="errors" style="margin-bottom: 15px;">
							<?	if( 1 < count($invalidEmails) ): ?>
							There are invalid email addresses from the data you provided.
							<?	else: ?>
							There is an invalid email address from the data you provided.
							<?	endif;?>
							<ul>
								<?	foreach($invalidEmails as $email): ?>
								<li>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$email?> is invalid.
								</li>
								<?	endforeach; ?>
							</ul>
						</div>
					</td>
				</tr>
			<?	endif; ?>
			<?
				$server = $_SERVER["SERVER_NAME"];
				$valid_invites = 0; 
				$valid_registered = 0;
				$valid_nonRegistered = 0;
				$style = 'display:none';
				if( $isAdminGroup && !$group_object->isSubGroup() ){
					$style = '';
				}
				//$style = ( $isAdminGroup )? "" : "display:none;";
				$style_bottom = ( $isAdminGroup )? "" : "border-bottom:none;";
				$isSubGroup = $group_object->isSubGroup();
			?>
				
			<? foreach( $travelers["registered"] AS $traveler ):
				$isMember = FALSE;
				
				$group_name = "";
				if( GROUP::ADMIN == $group_object->getDiscriminator() && !$isSubGroup ){
					$selected_group = new AdminGroup($assignToGroup);
					if( $selected_group->isMember($traveler) ){
						$isMember = TRUE;
					}
					$group_name = $selected_group->getName();
				}else{
					if( $group_object->isMember($traveler) ){
						$isMember = TRUE;
					}
					$group_name = stripslashes($group_object->getName());
				}
			?>
				<tr class="registered">
					<td class="checkbox">
						<? if( 0 == $traveler->isSuspended() ): ?>
							<? if( $isMember ): ?>
								<input type="checkbox" id="inviteID_<?=$traveler->getTravelerID()?>" name="inviteID[]"value="<?=$traveler->getTravelerID()?>"/>
							<? else: ?>
								<input type="checkbox" id="inviteID_<?=$traveler->getTravelerID()?>" name="inviteID[]"value="<?=$traveler->getTravelerID()?>" checked="checked"/>
							<? endif; ?>
						<? endif; ?>
					</td>
					<td class="member_details">
						<div class="users overflow">
							<a href="http://<?=$server?>/<?=$traveler->getUsername()?>" title="View <?=$traveler->getUsername()?>'s Pictures" class="thumb" target="_blank">
								<img class="pic" src="<?=$traveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" alt="<?=$traveler->getUsername()?>" />													
							</a>	
							<div class="details overflow" style="<?=$style_bottom?>">
								<a href="http://<?=$server?>/<?=$traveler->getUsername()?>" class="username" title="View <?=$traveler->getUsername()?>'s Profile" target="_blank"> 
									<?=$traveler->getUsername()?> 
									<span class="full_name"> <?=stripslashes($traveler->getFullName())?></span>
								</a>
								<div class="member_email">
									<?=$traveler->getEmail()?>
								</div>													
								<div class="hint">
									Existing GoAbroad.Net User
								</div>																											
							</div>																								
							<!-- END details overflow -->												
							
							<div class="generic_form_input notopborder" id="group_designation">
							<? 	if( 0 == $traveler->isSuspended() ):// && !$traveler->isAdvisor() ):
									$valid_registered++;
							?>
								<div id="select_box_<?=$traveler->getTravelerID()?>">
									<label for="group_select" style="<?=$style?>"> Assign To:</label>
									<select name="group_select_<?=$traveler->getTravelerID()?>" style="<?=$style?>">
										<? 	$valid_groups = 0;
											foreach($groups AS $group){ ?>
											<? if($group->getGroupID() == $assignToGroup): ?>
											<option value="<?=$group->getGroupID()?>" selected="selected"><?=stripslashes($group->getName())?> <? if( 2 == $group->getDiscriminator() && $group->isParent() ): ?>(Main Group)<?endif;?></option>
											<? else: ?>
												<? if( !$group->isMember($traveler) ): 
													$valid_groups++;
												?>
													<option value="<?=$group->getGroupID()?>" ><?=stripslashes($group->getName())?> <? if( 2 == $group->getDiscriminator() && $group->isParent() ): ?>(Main Group)<?endif;?></option>
												<? endif; ?>
											<? endif; ?>
										<? } unset($group); ?>														
									</select>
								</div>
								<br />
								<div>
								<? 	if( $isMember ): ?>
									<span class="hint">
										<p>
										<? 	if( !$isSubGroup ): 
												if( 0 < $valid_groups ):
													echo "This traveler is already a member of $group_name. You may select another group instead.";
												else:
													$valid_registered--;
													if( GROUP::ADMIN == $group_object->getDiscriminator() ){
														echo "No more groups available for this traveler. This traveler is already a member of all your groups.";
													}else{
														echo "This traveler is already a member of your club.";
													}
													hideCheckbox($traveler->getTravelerID());
												endif;    
											else:
												$valid_registered--;
												echo "This traveler is already a member of $group_name";
												hideCheckbox($traveler->getTravelerID());
										 	endif; ?>
										</p>
									</span>
								<? 	else:
										if( GROUP::ADMIN == $group_object->getDiscriminator() && $isSubGroup ){
											$parent = $group_object->getParent();
											if( $parent->isMember($traveler) ){
												?>
												<br />
												<span class="hint">
													<p>
													This traveler is already a member of your main group <?=stripslashes($parent->getName())?>. This traveler will be automatically added as a member of <?=stripslashes($group_object->getName())?> when you hit on the Send Invitation button.
													</p>
												</span>
												<?				
											}
										}else if(GROUP::ADMIN == $group_object->getDiscriminator() && $group_object->isParent() ){
											if( $group_object->isMember($traveler) ){
												?>
												<br />
												<span class="hint">
													<p>
													This traveler is already a member of your main group <?=stripslashes($group_object->getName())?>. This traveler will be automatically added as a member of the selected group when you hit on the Send Invitation button.
													</p>
												</span>
												<?
											}
										}?>
								<? 	endif; ?>
								</div>
							<? 	else: 
									hideCheckbox($traveler->getTravelerID());
							?>
								<div style="border-top: none;">
									<? if( $traveler->isAdvisor() ): ?>
										<span class="hint"><p><strong><i><?=$traveler->getEmail()?></i></strong> is used by an advisor group. It can no longer be invited as a member.</p></span>
									<? else: ?>
										<span class="hint"><p>This traveler is currently suspended and can't be invited as a member.</p></span>
									<? endif; ?>
								</div>
								<br />
							<? 	endif; ?>									
							</div>
																			
							<!-- END group_designation -->												
							
						</div>
						<!-- END users -->
					</td>
				</tr>
				<!-- END registered member -->
								
			<? endforeach; unset($traveler); ?>						
				<!-- End non registered user -->
			<? if( 0 < $valid_registered ): ?>
				<tr>
					<td colspan="2">
						<div id="msg_preview" class="section">
							<h2>
								<!--span> Message Preview</span--> 
								<span id="message_action" class="actions">
									This is the message that will be sent in the invitation. Click on <a href="" onclick="imManager.editMessage(1); return false;">Edit Message</a> to modify your message.
								</span>
							</h2>

							<div class="content">
								<textarea id="default_message" readonly="true" style="display:none;"><?=$message?></textarea>
								<textarea id="message" name="message" readonly="true" style="overflow: scroll;overflow-y:scroll;overflow-x: hidden; overflow:-moz-scrollbars-vertical;"><?=$message?></textarea>
								<div id="edit_msg_action" style="display:none;">
									<input type="button" name="name" value="Save Changes" onclick="imManager.saveEditMessage(1); return false;"/>
									<input type="button" name="name" value="Cancel" onclick="imManager.cancelEditMessage(1); return false;"/>										
								</div><!-- End edit_msg_action -->
							</div>
							<!-- END content -->
						</div><!-- END msg_preview -->
					</td>
				</tr>
			<? endif; ?>
			
			<? 	$ctr = -1;
				foreach($travelers["nonRegistered"] AS $traveler): ?>	
				<?
					$travelername = $travelers["nonRegisteredNames"][++$ctr];
					$valid_nonRegistered++;
				?>
				
				<tr class="non_registered">
					<td class="checkbox">
						<input type="checkbox" name="invite_email[]" value="<?=trim($traveler)?>" checked="checked"/>
					</td>
					<td class="member_details">
						<div class="users overflow">	
							<a class="thumb" href="" onclick="return false;">
								<img class="pic" alt="thumbnail" src="/images/default_images/user_thumb.gif"/>
							</a>												
							<div class="details overflow">																										
								<div class="nonregistered_email">	<?=$traveler?> 	</div>																										
								<div class="hint"> Not yet registered	</div>																																							
							</div>
							<!-- END details overflow -->							
							
							<div  class="generic_form_input notopborder" id="group_designation" style="<?=$style?>">
								<label for="group_select"> Assign To:</label>
								<select name="group_select_<?=trim(str_replace('.','_',$traveler))?>">
									<? if($group_object->getGroupID() == $assignToGroup): ?>
									<option value="<?=$group_object->getGroupID()?>" selected="selected"><?=stripslashes($group_object->getName())?> (Main Group)</option>
									<? else: ?>
									<option value="<?=$group_object->getGroupID()?>" ><?=stripslashes($group_object->getName())?> (Main Group)</option>
									<? endif; ?>
									
									<? foreach($groups AS $group){ ?>
										<? if($group->getGroupID() == $assignToGroup): ?>
										<option value="<?=$group->getGroupID()?>" selected="selected"><?=stripslashes($group->getName())?></option>
										<? else: ?>
										<option value="<?=$group->getGroupID()?>" ><?=stripslashes($group->getName())?></option>
										<? endif; ?>
									<? } unset($group); ?>														
								</select>														
							</div>
							<!-- END group_designation -->	
							
							<div class="generic_form_input" id="name_nonregistered" style="border-top: none;">
								<label for="name_text"> Name:</label> 
								<input class="textbox" type="text" name="name_text_<?=trim(str_replace('.','_',$traveler))?>" value="<?=$travelername?>" id="name_text_<?=trim($traveler)?>" /> <span class="hint"> (e.g Last Name, First Name)
							</div>
							<!-- END name_nonregistered -->																	
						</div>
						<!-- END users -->											
						
					</td>
				</tr>				
			<? endforeach; unset($traveler)?>	
			<? if( 0 < $valid_nonRegistered ): ?>
				<tr>
					<td colspan="2">
						<div id="msg_preview" class="section">
							<h2>
								<!--span> Message Preview</span--> 
								<span id="message_action2" class="actions">
									This is the message that will be sent in the invitation. Click on <a href="" onclick="imManager.editMessage(2); return false;">Edit Message</a> to modify your message.
								</span>
							</h2>

							<div class="content">
								<textarea id="default_message2" readonly="true" style="display:none;"><?=$message2?></textarea>
								<div id="reg_link_missing" class="errors" style="display: none;">
									<ul>
										<li>It seems that the text [registration_link] was removed from the message. However, the text is necessary so the recipient can view the link. It is recommended to copy the text below into the message:<br /><br /></li>
										<li>To join [your_group_name], please follow the link below:<br />[registration_link]</li>
									</ul>
									<div class="foot"></div>
								</div>
								<textarea id="message2" name="message2" readonly="true" style="overflow: scroll;overflow-y:scroll;overflow-x: hidden; overflow:-moz-scrollbars-vertical;"><?=$message2?></textarea>
								<div id="edit_msg_action2" style="display:none;">
									<input type="button" name="name" value="Save Changes" onclick="imManager.saveEditMessage(2); return false;"/>
									<input type="button" name="name" value="Cancel" onclick="imManager.cancelEditMessage(2); return false;"/>										
								</div><!-- End edit_msg_action -->
							</div>
							<!-- END content -->
						</div><!-- END msg_preview -->	
					</td>
				</tr>
			<? endif; ?>
			</tbody>	
		</table>
		
		<? if( 0 < $valid_registered+$valid_nonRegistered): ?>	
			<div>
				<input type="button" id="send_invite_button" value="Send Invitation" onclick="imManager.continueInvitation();" />
				&nbsp;&nbsp;<span class="actions"><strong><a href="http://<?=$server?>/members.php?gID=<?=$group_object->getGroupID()?>&mode=3">Back to Invites Form</a></strong></span>
				<input type="hidden" id="action" name="action" value="" />
			</div>
		<? else: ?>
		<div id="msg_preview" class="section">
			<h2>
				<span>Notice</span>
				<span class="actions"><a href="http://<?=$server?>/members.php?gID=<?=$group_object->getGroupID()?>&mode=3">Back to Invites Form</a></span>
			</h2>
			<div id="msg_preview_text" class="content">
				<p>Cannot continue sending invitation. No one to be invited.</p>
			</div>
		</div>
		<? endif;?>
		
		</form>												
	</div>
	<!-- END content -->
</div>
<!-- END invite_members -->

<?php
	function hideCheckbox($id){
?>
	<script type="text/javascript">
		try{
			document.getElementById("inviteID_<?=$id?>").style.display = "none";
		}catch(e){}
		try{
			document.getElementById("select_box_<?=$id?>").style.display = "none";
		}catch(e){}
	</script>
<?		
	}
?>