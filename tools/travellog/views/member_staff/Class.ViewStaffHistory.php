<?php
	/*
	 *	@author CheWyl
	 *	18 November 2008
	 */

	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.MembersPanel.php';

	class ViewStaffHistory{

		private $mType		= 1;
		private $mGroup		= null;
		private $arrHistory	= array();

		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setType($_type = 1){
			$this->mType = $_type;
		}

		function retrieve(){
			if (2 == $this->mType)
				$this->renderDenied();
			else
				$this->renderUpdates();
		}

		function renderDenied(){
			$arrDenied = MembersPanel::getDeniedStaffInvitations($this->mGroup);

			if (0 < count($arrDenied)){
				foreach($arrDenied as $each){
					try{
						$mGroup = GroupFactory::instance()->create( array($each['groupID']) );
					}
					catch(exception $e){
						$mGroup = null;
					}
					try{
						$mTraveler = new Traveler( $each['travelerID'] );
					}
					catch(exception $e){
						$mTraveler = null;
					}
					

					if ($mGroup && $mTraveler){
						$mData = array();
						$mData['traveler'] = $mTraveler;
						$mData['group'] = $mGroup[0];
						$this->arrHistory[] = $mData;
					}
				}
			}
		}

		function renderUpdates(){
			$arrAccepted = MembersPanel::getAcceptedStaffInvitations($this->mGroup);

			if (0 < count($arrAccepted)){
				foreach($arrAccepted as $each){
					try{
						$mGroup = GroupFactory::instance()->create( array($each['groupID']) );
					}
					catch(exception $e){
						$mGroup = null;
					}
					try{
						$mTraveler = new Traveler( $each['travelerID'] );
					}
					catch(exception $e){
						$mTraveler = null;
					}

					if ($mGroup && $mTraveler){
						$mData = array();
						$mData['traveler'] = $mTraveler;
						$mData['group'] = $mGroup[0];
						$this->arrHistory[] = $mData;
					}
				}
			}
		}

		function render(){
			$tpl = new Template;

			$tpl->set("arrHistory",		$this->arrHistory);

			if (2 == $this->mType)
				$tpl->out("travellog/views/member_staff/tpl.ViewDeniedStaffHistory.php");
			else
				$tpl->out("travellog/views/member_staff/tpl.ViewStaffUpdateHistory.php");
		}
	}
?>