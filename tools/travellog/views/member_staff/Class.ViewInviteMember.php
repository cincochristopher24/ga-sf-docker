<?php
	require_once 'Class.Template.php';
	require_once 'travellog/views/member_staff/Class.InviteMember.php';

	class ViewInviteMember{

		private $mGroup			= null;
		private $mInviteMember	= null;
		private $mLoggedUser	= null;
		private $mViewerType	=	1;

		function setGroup($_group = ''){
			$this->mGroup = $_group;
		}

		function setLoggedUser($loggedUser=NULL){
			$this->mLoggedUser = $loggedUser;
		}
		
		function setViewerType($viewerType=1){
			$this->mViewerType = $viewerType;
		}

		function retrieve(){
			$this->mInviteMember = new InviteMember();
			$this->mInviteMember->setViewerType($this->mViewerType);
			$this->mInviteMember->setLoggedUser($this->mLoggedUser);//always invoke setLoggedUser before setGroup
			$this->mInviteMember->setGroup($this->mGroup);
		}

		function render(){
			$this->mInviteMember->performAction();
		}
	}