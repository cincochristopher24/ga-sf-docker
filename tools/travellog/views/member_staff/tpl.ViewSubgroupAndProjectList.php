<ul id="other_group_project_list">
	<?php for ($i=0; $i<sizeof($subGroupsAndProjects); $i++ ) {
			$subgroup = $subGroupsAndProjects[$i];
			$privileged = (!is_null($loggedUser) && $subgroup->isStaff($loggedUser->getTravelerID())) || $viewerType < 3;
			$staff = count($subgroup->getStaff($list=TRUE));
			if( $privileged ){
				$members = $subgroup->getGroupMembers($rowslimit = NULL, $filtercriteria = NULL, $order = NULL, $unassigned=FALSE,$includePending=TRUE,$countOnly=TRUE) - $staff;
			}else{
				$members = $subgroup->getMembers($rowslimit = NULL, $filtercriteria = NULL, $order = NULL, $count = TRUE);				
			}
			$memberLabel = 1 > $members ? "" : 1 < $members ? "$members Members" : "$members Member";
			$staffLabel = 1 > $staff ? "" : "$staff Staff";
			$memberStaffLabel = "No Members";
			if( $members > 0 && $staff > 0 ){
				$memberStaffLabel = $memberLabel." and ".$staffLabel;
			}else if( $members < 1 && $staff > 0 ){
				$memberStaffLabel = $staffLabel;
			}else if( $members > 0 && $staff < 1 ){
				$memberStaffLabel = $memberLabel;
			}
	?>
		<?php if( 0 == $i ):  ?>
			<li id="first">
		<?php else: ?>
			<li>
		<?php endif; ?>

		<div class="group_name"> 
			<?=stripslashes($subgroup->getName())?> 
			<span class="member_size"> (<?=$memberStaffLabel?>)</span> 
		</div>
		<?php $server = $_SERVER["SERVER_NAME"]; ?>
		<?php if( !is_null($loggedUser) && $privileged ): ?>
			<div class="actions">
				<a href="http://<?=$server?>/members.php?gID=<?=$subgroup->getGroupID()?>">Manage Members</a>
				<?php if( 0 < $staff ): ?>
					|
					<a href="http://<?=$server?>/messages.php?act=messageGroupStaff&gID=<?=$subgroup->getGroupID()?>" >Message Staff &rarr;</a>
				<?php endif; ?>
			</div>
		<?php else: ?>
			<?php if (0 < ($members+$staff) ) : ?>
				<div class="actions">
					<a href="http://<?=$server?>/members.php?gID=<?=$subgroup->getGroupID()?>">View Members/Staff</a>
				</div>
			<?php endif ?>
		<?php endif; ?>																											
		</li>
	<?php } ?>																																	
</ul>

<img id="loading_page" src="/images/loading_small.gif" style="display:none;"/>
<?php if ( $paging->getTotalPages() > 1 ):?>
	<?php
		$start = $paging->getStartRow() + 1; 
		$end = $paging->getEndRow(); 
	?>
	<?php $paging->showPagination() ?>
<?php endif; ?>
