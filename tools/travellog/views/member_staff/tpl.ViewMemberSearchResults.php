<? if ( 14 != $action && 15 != $action ): ?>
	<div id="memberstaff_search_results" class="section">
		<?// if( $isAjaxRequest && 13 != $action ): ?><!-- from search -->
		<? if( 13 != $action ): ?>
			<?	if( isset($_GET["action"]) ): ?>
				<h2> <span> Search Results</span> </h2>
				<div class="section_small_details">
					Your Search 
					<? if ( "" != $name_filter ): ?> 
						for "<strong><?=stripslashes($name_filter)?></strong>"
					<? endif; ?>
			
					<?	$grp_label = "";
						if( 0 == $scope ){
							$grp_label = "MAIN GROUP";
						}else if( 1 == $scope || 2 == $scope){
							$grp_label = $group->getName();
						}
					?>
					in "<strong><?=stripslashes($group->getName())?></strong>"
					returned <strong><?=$totalrec?></strong> <?if(1<$totalrec):?>results<?else:?>result<?endif;?>
				</div>
			<?	else: 
					$totalrec = $totalrec - ($grpStaffCount+$grpSuperStaffCount);
					$members_label = 0 == $totalrec ? "" : 1 < $totalrec ? $totalrec." Members" : $totalrec." Member";
					$staff_label = ($grpStaffCount+$grpSuperStaffCount)." Staff";
					
					$memberStaffLabel = "No Members";
					if( 0 < $totalrec && 0 < ($grpStaffCount+$grpSuperStaffCount) ){
						$memberStaffLabel = $members_label." and ".$staff_label;
					}else if( 0 == $totalrec && 0 < ($grpStaffCount+$grpSuperStaffCount) ){
						$memberStaffLabel = $staff_label;
					}else if( 0 < $totalrec && 0 == ($grpStaffCount+$grpSuperStaffCount) ){
						$memberStaffLabel = $members_label;
					}
			?>
				<h2> <span> Members of <?=$group->getName()?></span></h2>
				<div class="section_small_details">
					"<strong><?=$group->getName()?></strong>" has <?=$memberStaffLabel?>
				</div>
			<?	endif; ?>
			<!-- END section_small_details -->
		<? endif; ?>
		<?// if( $isAjaxRequest && 13 == $action ): ?>
		<? if( 13 == $action ): ?>
			<h2> <span> Members of the Main Group</span> </h2>
			<div class="section_small_details">
				<?
					$label = '"<strong>'.$group->getName().'</strong>" has no Member yet';
					
					$totalrec = $totalrec - ($grpSuperStaffCount+$grpStaffCount);
					
					if( 1 == $totalrec ){
						$label = '"<strong>'.$group->getName().'</strong>" has 1 Member';
					}else if( 1 < $totalrec){
						$label = '"<strong>'.$group->getName().'</strong>" has '.$totalrec.' Members';
					}
					/*if( 0 <  $grpSuperStaffCount ){
						$label .= " and ".$grpSuperStaffCount." Staff ";
					}*/
					if( 3 > $viewerType ){
						if( 0 < $grpSuperStaffCount && 0 < $grpStaffCount ){
							$label .= ", $grpSuperStaffCount Admin and $grpStaffCount Staff";
						}else if( 0 < $grpSuperStaffCount ){
							$label .= " and $grpSuperStaffCount Admin";
						}else if( 0 < $grpStaffCount ){
							$label .= " and $grpStaffCount Staff";
						}
					}else if( 0 < $grpStaffCount ){
						$label .= " and $grpStaffCount Staff";
					}
					
				?>
				<?=$label?>
			</div>
		<? endif; ?>
		<div id="search_result_content" class="content">
			<? echo $controller->renderMemberSearchResultList(); ?>
		</div>
		<!-- END content -->
	</div>
	<!-- END search_results -->
<? endif; ?>

<? if( 14 == $action || 15 == $action ): //for unassigned members ?>
	<div id="unassigned_members" class="section">
		<h2> <span> Showing Unassigned Members</span>  </h2>
	
		<div class="section_small_details">
			<form onsubmit="gmsManager.refreshUnassignedMembers(); return false;">
				<ul class="list_small_details">											
					<li>
						<label for="sortname_filter"> Sort Name</label>	&nbsp;											
						<select name="sortname_filter" id="sortname_filter">
							<option value="0" <?=( 0 == $sortByFilter)? 'selected="selected"' : ''?> >From A to Z</option>
							<option value="1" <?=( 1 == $sortByFilter)? 'selected="selected"' : ''?> >From Z to A</option>
						</select>																								
					</li>
					<li>
						<input type="submit" name="name" value="Refresh List" id="refresh_list"/>
					</li>
					<li>
						<img id="loading_content" src="/images/loading_small.gif" style="display:none;" />
					</li>
				</ul>
			</form>								
		</div>
		<!-- END section_small_details -->
	
		<div id="search_result_content" class="content">
			<? echo $controller->renderMemberSearchResultList(); ?>						
		</div>
		<!-- END content -->
	
	</div>
	<!-- END unassigned_members -->
<? endif; ?>