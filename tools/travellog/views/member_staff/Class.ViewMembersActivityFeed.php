<?php
/***
 * Created on 11 23, 08
 *
 * * @author 
 * Purpose: 
 */
 
 	require_once 'Class.Template.php';
	require_once 'travellog/factory/Class.FileFactory.php';
	
 	class ViewMembersActivityFeed {
		private $mGroupID		= 0;
		private $mMode			= null;
		private $mLoggedUser	= null;
		private $mFeeds			= null;
		private $mShowIndiFeeds = false;
		private $mViewerType    = null;
		private $mMember 		= null;
		private $mAffiliations  = array();
		
		const MEMBER_TYPE	= 1;
		const STAFF_TYPE	= 2;

		function setGroupId($_groupID = 0){
			$this->mGroupID = $_groupID;
		}
		
		function setMode($_mode = 1){
			$this->mMode = $_mode;
		}
		
		function setLoggedUser($_loggedUser = null){
			$this->mLoggedUser = $_loggedUser;
		}
		
		function setFeeds($feeds){
			$this->mFeeds = $feeds;
		}
		
		function setShowIndiFeeds($bool){
			$this->mShowIndiFeeds = $bool;
		}
		
		function setMember($member){
			$this->mMember = $member;
		}
		
		function setAffiliations($affiliates){
			$this->mAffiliations = $affiliates;
		}
		
		function setViewerType($type){
			$this->mViewerType = $type;
		}
		
		function getGroup(){
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('GroupFactory');
			$group = $file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($this->mGroupID));
			return $group[0];
		}
		
		function render() {
			$tpl = new Template;
			$tpl->set("groupID",$this->mGroupID);
			$tpl->set("group",$this->getGroup());
			$tpl->set("feeds",$this->mFeeds);
			$tpl->set("showIndiFeed",$this->mShowIndiFeeds);
			$tpl->set("member",$this->mMember);
			$tpl->set("viewerType",$this->mViewerType);
			$tpl->set("affiliations",$this->mAffiliations);
			$tpl->set("loggedUser",$this->mLoggedUser);
			$tpl->setTemplate("travellog/views/member_staff/tpl.ViewMembersActivityFeed.php");
			return $tpl;
		}
 	}
?>