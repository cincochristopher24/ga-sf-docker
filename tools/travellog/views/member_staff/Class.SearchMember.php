<?php
	/*
	 *	@author Jul
	 *	2008-11-12
	 */

	require_once ('Class.Template.php');
	require_once ('Class.Paging.php');
	require_once ('Class.ObjectIterator.php');
	require_once ('travellog/model/Class.Condition.php');
	require_once ('travellog/model/Class.FilterOp.php');
	require_once ('travellog/model/Class.FilterCriteria2.php');
	require_once ('travellog/model/Class.AdminGroup.php');
	require_once ('Class.dbHandler.php');
	require_once('Cache/ganetCacheProvider.php');

	class SearchMember{
		
		//action constants
		const DISPLAY_ALL_GROUPS 	=	10;
		const DISPLAY_GROUP_LIST	=	11;
		const SEARCH_MEMBERS		=	12;
		const SHOW_MAIN_GROUP_MEMBERS	=	13;
		const SHOW_UNASSIGNED_MEMBERS	=	14;
		const FETCH_UNASSIGNED_MEMBERS	=	15;
		const SHOW_NEW_MEMBERS			=	16;
		const FETCH_NEW_MEMBERS			=	17;
		const SHOW_RECENT_ACTIVITY		=	18;
		const FETCH_RECENT_ACTIVITY		=	19;
		
		//displayModeFilter constants with action DISPLAY_ALL_GROUPS
		const DISPLAY_ALL			=	0;
		const DISPLAY_GROUP			=	1;
		const DISPLAY_PROJECT		=	2;
		
		//sortByFilter constants with action DISPLAY_ALL_GROUPS
		const RANK_ASC				=	0;
		const RANK_DESC				=	1;
		const NAME_ASC				=	2;
		const NAME_DESC				=	3;
		
		//scope of member search
		const MAIN_GROUP			=	0;
		const GROUP					=	1;
		const PROJECT				=	2;
		
		//date range, for new member display
		const DAY					=	0;
		const WEEK					=	1;
		const MONTH					=	2;
		const YEAR					=	3;
		
		private $mAccessedGroup	=	NULL;
		
		private $mMainGroupObject = NULL;
		private $mGroupObject = NULL;
		private $mIsSubGroup = FALSE;
		private $mGroupSubGroups = array();
		private $mGroupSubGroupsCount = 0;
		private $mGroupProjects = array();
		private $mGroupProjectsCount = 0;
		private $mSubGroupsAndProjects = array();
		private $mSubGroupsAndProjectsCount = 0;
		private $mGroupMembers = array();
		private $mGroupMembersCount = 0;
		private $mGroupSuperStaff = array();
		private $mGroupStaff = array();
		private $mIsAdminGroup = TRUE;
		
		private $mRpp = 25; //rows per page
		private $mRowsLimit = NULL;
		
		/* to hold parameters from the browser */
		private $mParams = array(	"action"	=>	self::DISPLAY_ALL_GROUPS,
									"page"		=>	1,
									"username"	=>	"",
									"gID_filter"=>	0,
									"scope"		=>	self::GROUP,
									"sortByFilter"	=>	0,
									"displayModeFilter"	=>	self::DISPLAY_ALL,
									"qCount"	=>	1,
									"qRange"	=>	2 );
		private $mAction = self::DISPLAY_ALL_GROUPS;
		private $mScope = self::GROUP;
		private $mPage = 1;
		
		private $mIsAjaxRequest = FALSE;
		private $mPageClickAction = "";
		private $mIsNewMemberDisplay = FALSE;
		
		private $mViewerType 	=	6;
		private $mLoggedUser	=	NULL;
		private $mIsPrivileged	=	FALSE;
		
		function setGroup($group=NULL){
			$this->mGroupObject = $group;
			$this->setAccessedGroup($group);
			$this->mIsAdminGroup = (2 == $this->mGroupObject->getDiscriminator())? TRUE : FALSE;
			if (  $this->mIsAdminGroup && 0 < $this->mGroupObject->getParentID() ){
				$this->mMainGroupObject = new AdminGroup($this->mGroupObject->getParentID());
				if ( !($this->mMainGroupObject instanceof AdminGroup) ){
					$this->mMainGroupObject = NULL;
				}
			}
		}
		
		function setAccessedGroup($group=NULL){
			$this->mAccessedGroup = $group;
		}
		
		function setParams($params=array()){
			$default = array(	"action"	=>	self::DISPLAY_ALL_GROUPS,
								"page"		=>	1,
								"username"	=>	"",
								"gID_filter"=>	0,
								"scope"		=>	self::GROUP,
								"sortByFilter"	=>	0,
								"displayModeFilter"	=>	self::DISPLAY_ALL,
								"qCount"	=>	1,
								"qRange"	=>	2 );
			$this->mParams = array_merge($default,$params);
			$this->mPage = $this->mParams["page"];
			$this->mScope = $this->mParams["scope"];
			$this->mAction = $this->mParams["action"];
		}
		
		function setViewerType($viewer_type=6){
			$this->mViewerType = $viewer_type;
			if( 1 == $this->mViewerType || 
				($this->mIsAdminGroup && 3>$this->mViewerType) ||
				($this->mIsAdminGroup && 0 < $this->mGroupObject->getParentID() && 4>$this->mViewerType) ){
				$this->mIsPrivileged = TRUE;
			}
		}
		
		function setLoggedUser($user=NULL){
			$this->mLoggedUser = $user;
		}
		
		function setIsAjaxRequest($isAjax=FALSE){
			$this->mIsAjaxRequest = ( is_bool($isAjax) ) ? $isAjax : FALSE;
		}
		
		function performAction(){
			switch($this->mAction){
				case self::DISPLAY_ALL_GROUPS	:	$this->displayAllSubGroupsAndProjects(); break;
				case self::DISPLAY_GROUP_LIST	:	$this->displayAllSubGroupsAndProjects(); break;//$this->searchSubGroupsAndProjects(); break;
				case self::SEARCH_MEMBERS		:	$this->searchMembers(); break;
				case self::SHOW_MAIN_GROUP_MEMBERS	:	$this->showMainGroupMembers(); break;
				case self::SHOW_UNASSIGNED_MEMBERS	:	$this->showUnassignedMembers(); break;
				case self::FETCH_UNASSIGNED_MEMBERS	:	$this->fetchUnassignedMembers(); break;
				case self::SHOW_NEW_MEMBERS		:	$this->showNewMembers(); break;
				case self::FETCH_NEW_MEMBERS	:	$this->fetchNewMembers(); break;
				case self::SHOW_RECENT_ACTIVITY	:	$this->showRecentActivity(); break;
				case self::FETCH_RECENT_ACTIVITY:	$this->fetchRecentActivity(); break;
			}
		}
		
		function renderSearchForm(){
			try{
				$this->mParams["gID"] = $this->mGroupObject->getGroupID();
				$this->mIsSubGroup = $this->mGroupObject->isSubGroup();
				$this->mRowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
				
				$scope = ( $this->mIsSubGroup )? self::GROUP : self::MAIN_GROUP;
				$gID = $this->mGroupObject->getGroupID();
				$searchMode = 1;
				if( 1==$this->mGroupObject->getDiscriminator() || $this->mIsSubGroup ){
					$searchMode = 2;
				}
				
				$cache = ganetCacheProvider::instance()->getCache();
				$sp_array = FALSE;
				if ($cache != NULL){
					$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"members")));
				}
				if( !$sp_array ){
					$cond = new Condition;
					$cond->setAttributeName("username");
					$cond->setOperation(FilterOp::$ORDER_BY);
					$order = new FilterCriteria2();
					$order->addCondition($cond);
					$sp_array = $this->mGroupObject->getGroupMembers($this->mRowsLimit,NULL,$order,FALSE,$this->mIsPrivileged);
					
					$this->setCacheKey($cache,$sp_array,$type="members");	
				}
				$this->mGroupMembers = $sp_array[0];
				$this->mGroupMembersCount = $sp_array[1];
				
				if( $this->mIsAdminGroup ){
				
					$cond = new Condition;
					$cond->setAttributeName("name");
					$cond->setOperation(FilterOp::$ORDER_BY);
					$order = new FilterCriteria2();
					$order->addCondition($cond);
					
					$sp_array = FALSE;
					//if( $cache != NULL ){
					//	$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"groups")));
					//}
					//if( !$sp_array ){
						$sp_array = $this->mGroupObject->getSubGroupsAndProjects(NULL,$order,self::DISPLAY_GROUP);
						
					//	$this->setCacheKey($cache,$sp_array,$type="groups");
					//}
					$this->mGroupSubGroups = $sp_array[0];
					$this->mGroupSubGroupsCount = $sp_array[1];
					
					$sp_array = FALSE;
					//if( $cache != NULL ){
					//	$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"projects")));
					//}
					//if( !$sp_array ){
						$sp_array = $this->mGroupObject->getSubGroupsAndProjects(NULL,$order,self::DISPLAY_PROJECT);
						
					//	$this->setCacheKey($cache,$sp_array,$type="projects");
					//}
					$this->mGroupProjects = $sp_array[0];
					$this->mGroupProjectsCount = $sp_array[1];			
					
					$staff_array = FALSE;
					if( $cache != NULL ){
						$staff_array = $cache->get($this->generateCacheKey("URL",array("type"=>"staff")));
					}
					if( !$staff_array ){
						if ( $this->mIsSubGroup ){
							$this->mGroupStaff = $this->mGroupObject->getStaff();
						}else{
							$this->mScope = self::MAIN_GROUP;
							$this->mGroupStaff = $this->mGroupObject->getStaffofSubGroups();
							$this->mGroupSuperStaff = $this->mGroupObject->getStaff();
						}
						$this->setCacheKey($cache,array("staff"=>$this->mGroupStaff, "superstaff"=>$this->mGroupSuperStaff),$type="staff");
					}else{
						$this->mGroupStaff = $staff_array["staff"];
						$this->mGroupSuperStaff = $staff_array["superstaff"];
					}
					
					$sp_array = FALSE;
					if( $cache != NULL ){
						$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"subgroupsprojects")));
					}
					if( !$sp_array ){
						$cond = new Condition;
						$cond->setAttributeName("subgrouprank");
						$cond->setOperation(FilterOp::$ORDER_BY);
						$order = new FilterCriteria2();
						$order->addCondition($cond);
						$sp_array = $this->mGroupObject->getSubGroupsAndProjects($this->mRowsLimit,$order,self::DISPLAY_ALL);
						$this->setCacheKey($cache,$sp_array,$type="subgroupsprojects");
					}
					$this->mSubGroupsAndProjects = $sp_array[0];
					$this->mSubGroupsAndProjectsCount = $sp_array[1];
				
				}
				
				if(0 == $this->mGroupSubGroupsCount+$this->mGroupProjectsCount){
					$searchMode = 2;
				}else{
					$searchMode = 1;
				}
				if( 0 == $this->mParams["gID_filter"] ){
					$this->mParams["gID_filter"] = $this->mParams["gID"];
				}
				$gID_filter = $this->mParams["gID_filter"];
				$lastAction = $this->mAction;
				$qCount = $this->mParams["qCount"];
				$qRange = $this->mParams["qRange"];
				$sortByFilter = $this->mParams["sortByFilter"];
				$displayModeFilter = $this->mParams["displayModeFilter"];
				$page = $this->mPage;
				$username = str_replace("'","&rsquot;",$this->mParams["username"]);
				$code = <<<BOF
				<script type="text/javascript"> 
				//<![CDATA[
					jQuery(document).ready(function(){
						gmsManager.setGroupID($gID);
						gmsManager.setSearchMode($searchMode);
						gmsManager.setLastAction($lastAction);
						gmsManager.getSearchMemberFilter().setUsername('$username');
						gmsManager.getSearchMemberFilter().setScope($scope);
						gmsManager.getSearchMemberFilter().setGID($gID_filter);
						gmsManager.getSearchMemberFilter().setPage($page);
						gmsManager.getDisplayAllFilter().setQCount($qCount);
						gmsManager.getDisplayAllFilter().setQRange($qRange);
						gmsManager.getDisplayAllFilter().setSortByFilter($sortByFilter);
						gmsManager.getDisplayAllFilter().setDisplayModeFilter($displayModeFilter);
						gmsManager.getDisplayAllFilter().setPage($page);
					});
				//]]>
				</script>
BOF;
				Template::includeDependent($code);
				
				$group_type = "";
				if( 2 == $this->mGroupObject->getDiscriminator() && $this->mGroupObject->isParent() ){
					$group_type = "MAIN GROUP";
				}
				
				if ( $this->mGroupObject instanceof AdminGroup ){
					$tpl = new Template();
					$tpl->set("grpID",$this->mGroupObject->getGroupID());
					$tpl->set("isAdminGroup",$this->mIsAdminGroup);
					$tpl->set("grpName",$this->mGroupObject->getName());
					$tpl->set("isSubGroup",$this->mIsSubGroup);
					$tpl->set("grpType",$group_type);
					$tpl->set("grpMemberCount",$this->mGroupMembersCount);
					$tpl->set("grpSubGroups",$this->mGroupSubGroups);
					$tpl->set("grpSubGroupCount",$this->mGroupSubGroupsCount);
					$tpl->set("grpProjects",$this->mGroupProjects);
					$tpl->set("grpProjectCount",$this->mGroupProjectsCount);
					if( 3 > $this->mViewerType ){
						$tpl->set("grpSuperStaffCount",sizeof($this->mGroupSuperStaff));
						$tpl->set("grpStaffCount",sizeof($this->mGroupStaff)-sizeof($this->mGroupSuperStaff));
					}else{
						$tpl->set("grpSuperStaffCount",0);
						$tpl->set("grpStaffCount",sizeof($this->mGroupStaff));
					}
					$tpl->set("controller",$this);
					$tpl->set("viewerType",$this->mViewerType);
					$tpl->set("username",$username);
					$tpl->out("travellog/views/member_staff/tpl.FrmGroupMemberSearch.php");
				}else if( $this->mGroupObject instanceof Group ){
					$tpl = new Template();
					$tpl->set("group",$this->mGroupObject);
					$tpl->set("grpMemberCount",$this->mGroupMembersCount);
					$tpl->set("isAdminGroup",$this->mIsAdminGroup);
					$tpl->set("controller",$this);
					$tpl->set("viewerType",$this->mViewerType);
					$tpl->set("username",$username);
					$tpl->out("travellog/views/member_staff/tpl.FrmGroupMemberSearch.php");
				}else{
					$this->renderError("Expected instance of Group or AdminGroup");
				}

			}catch(exception $e){
				$this->renderError($e->getMessage());
			}
		}
		
		function renderDefaultResults(){
			
			try{
				if( 50 <= $this->mGroupMembersCount && !$this->mIsSubGroup && !isset($_GET["action"]) ){
					//ob_end_clean();
					//header("location: /members.php?action=13&gID=".$this->mGroupObject->getGroupID()."&cID=0&mode=1&sMode=13");
					$this->mAction = self::SEARCH_MEMBERS;
					$this->mPageClickAction = "gmsManager.updateMemberSearchResultList";
					$this->searchMembers();//renderMemberSearchResults();
				}else if ( 0 < sizeof($this->mSubGroupsAndProjects) ){
					$tpl = new Template();
					$tpl->set("group",$this->mGroupObject);
					$tpl->set("grpMemberCount",$this->mGroupMembersCount);
					$tpl->set("subGroupsAndProjects",$this->mSubGroupsAndProjects);
					$tpl->set("controller",$this);
					$tpl->set("loggedUser",$this->mLoggedUser);
					$tpl->set("displayMode",$this->mParams["displayModeFilter"]);
					$tpl->set("sortByFilter",$this->mParams["sortByFilter"]);
					$tpl->set("totalrec",$this->mSubGroupsAndProjectsCount);
				
					$tpl->set("isSubGroup",$this->mIsSubGroup);
					if( 3 > $this->mViewerType ){
						$tpl->set("grpSuperStaffCount",sizeof($this->mGroupSuperStaff));
						$tpl->set("grpStaffCount",sizeof($this->mGroupStaff)-sizeof($this->mGroupSuperStaff));
					}else{
						$tpl->set("grpSuperStaffCount",0);
						$tpl->set("grpStaffCount",sizeof($this->mGroupStaff));
					}
					$tpl->set("viewerType",$this->mViewerType);
				
					$tpl->out("travellog/views/member_staff/tpl.ViewDisplayAllSubGroups.php");
				}else{
					$this->mAction = self::SEARCH_MEMBERS;
					$this->mPageClickAction = "gmsManager.updateMemberSearchResultList";
					$this->renderMemberSearchResults();
				}
			}catch(exception $e){
				$this->renderError($e->getMessage());
			}
		}
		
		// for DISPLAY_ALL_GROUPS
		function displayAllSubGroupsAndProjects(){
			$this->mGroupObject = new AdminGroup($this->mParams["gID"]);
			
			$cache = ganetCacheProvider::instance()->getCache();
			$sp_array = FALSE;
			if ($cache != NULL){
				$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"members")));
			}
			if( !$sp_array ){
				$sp_array = $this->mGroupObject->getGroupMembers(NULL,NULL,NULL,FALSE,$this->mIsPrivileged);
				
				$this->setCacheKey($cache,$sp_array,$type="members");
			}
			$this->mGroupMembers = $sp_array[0];
			$this->mGroupMembersCount = $sp_array[1];

			$this->searchSubGroupsAndProjects();
			$this->renderDefaultResults();
		}
		
		// for DISPLAY_GROUP_LIST together with function renderSubGroupAndProjectList()
		function searchSubGroupsAndProjects(){
			$this->mGroupObject = new AdminGroup($this->mParams["gID"]);
			$this->mRowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			
			$orderBy = FilterOp::$ORDER_BY;
			$orderField = "subgrouprank";
			if ( isset($this->mParams["sortByFilter"]) ){
				switch($this->mParams["sortByFilter"]){
					case self::RANK_ASC		:	$orderField = "subgrouprank";
												$orderBy = FilterOp::$ORDER_BY;
												break;
					case self::RANK_DESC	:	$orderField = "subgrouprank";
												$orderBy = FilterOp::$ORDER_BY_DESC;
												break;
					case self::NAME_ASC		:	$orderField = "name";
												$orderBy = FilterOp::$ORDER_BY;
												break;
					case self::NAME_DESC	:	$orderField = "name";
												$orderBy = FilterOp::$ORDER_BY_DESC;
												break;
				}
			}
			$cond = new Condition;
			$cond->setAttributeName($orderField);
			$cond->setOperation($orderBy);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			if( isset($this->mParams["displayModeFilter"]) ){
				if( self::DISPLAY_ALL == $this->mParams["displayModeFilter"] ||
					self::DISPLAY_GROUP == $this->mParams["displayModeFilter"] ||
					self::DISPLAY_PROJECT == $this->mParams["displayModeFilter"] ){
						
					$cache = ganetCacheProvider::instance()->getCache();
					$sp_array = FALSE;
					if ($cache != NULL){
						$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"searchsubgroups")));
					}
					if( !$sp_array ){	
						$sp_array = $this->mGroupObject->getSubGroupsAndProjects($this->mRowsLimit,$order,$this->mParams["displayModeFilter"]);
					
						$this->setCacheKey($cache,$sp_array,$type="searchsubgroups");
					}
					$this->mSubGroupsAndProjects = $sp_array[0];
					$this->mSubGroupsAndProjectsCount = $sp_array[1];
					//$this->renderSubGroupAndProjectList();
				}else{
					$this->renderError("Unknown displayModeFilter [ ".$this->mParams["displayModeFilter"]." ]");
				}
			}
		}
		
		function renderSubGroupAndProjectList(){
			try{
				$paging        = new Paging( $this->mSubGroupsAndProjectsCount, $this->mPage, 'grpID=' . $this->mGroupObject->getGroupID(), $this->mRpp );
				$paging->setOnclick('gmsManager.fetchAllGroupsAndProjects');
				$iterator      = new ObjectIterator( $this->mSubGroupsAndProjects, $paging );
					
				$tpl = new Template();
				$tpl->set("group",$this->mGroupObject);
				$tpl->set("displayMode",$this->mParams["displayModeFilter"]);
				$tpl->set("subGroupsAndProjects",$this->mSubGroupsAndProjects);
				$tpl->set("paging",$paging);
				$tpl->set("iterator",$iterator);
				$tpl->set("totalrec",$this->mSubGroupsAndProjectsCount);
				$tpl->set("viewerType",$this->mViewerType);
				$tpl->set("loggedUser",$this->mLoggedUser);
				$tpl->out("travellog/views/member_staff/tpl.ViewSubgroupAndProjectList.php");
			}catch(exception $e){
				$this->renderError($e->getMessage());
			}
		}
		
		function searchMembers(){		
			$this->mGroupObject = new AdminGroup($this->mParams["gID_filter"]);
			$this->mIsAdminGroup = ( 2 == $this->mGroupObject->getDiscriminator() )? TRUE : FALSE;
			
			$sp_array = FALSE;
			$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null){
				$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"search")));
			}
			
			if( !$sp_array ){		
				$where = NULL;
				if( "" != $this->mParams["username"] ){
					$db = new dbHandler();
					$username = $db->makeSqlSafeString($this->mParams["username"]);
					$username_like = $db->makeSqlSafeString($this->mParams["username"]."%");
					$cond1 = new Condition;
					$cond1->setAttributeName(" ( tblTraveler.username");
					$cond1->setOperation(FilterOp::$LIKE);
					$cond1->setValue("$username_like OR tblTraveler.email = $username OR CONCAT(tblTraveler.firstname,' ',tblTraveler.lastname) LIKE $username_like )");		
					$where = new FilterCriteria2();
					$where->addCondition($cond1);
				}
				$cond4 = new Condition;
				$cond4->setAttributeName("username");
				$cond4->setOperation(FilterOp::$ORDER_BY);
				$order = new FilterCriteria2();
				$order->addCondition($cond4);
			
				$this->mRowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));			
				$sp_array = $this->mGroupObject->getGroupMembers($this->mRowsLimit,$where,$order,FALSE,$this->mIsPrivileged);
			
				$this->setCacheKey($cache,$sp_array,$type="search");
			}
			
			$this->mGroupMembers = $sp_array[0];
			$this->mGroupMembersCount = $sp_array[1];
			
			if ( 0 < $this->mGroupObject->getParentID() ){
				$this->mMainGroupObject = new AdminGroup($this->mGroupObject->getParentID());
				if ( !($this->mMainGroupObject instanceof AdminGroup) ){
					$this->mMainGroupObject = NULL;
				}
			}
			$this->mPageClickAction = "gmsManager.updateMemberSearchResultList";
			
			$this->renderMemberSearchResults();
		}
		
		function renderMemberSearchResults(){
			try{
				/*$cache = ganetCacheProvider::instance()->getCache();

				if ($cache != null && $content = $cache->get($this->generateCacheKey("URL",array("type"=>"results")))){
					echo $content;		// display the cached content
					return;
				}*/
				
				$tpl = new Template();
				$tpl->set("controller",$this);
				$tpl->set("isAjaxRequest",$this->mIsAjaxRequest);
				$tpl->set("action",$this->mAction);
				$tpl->set("group",$this->mGroupObject);
				$tpl->set("viewed_gID",$this->mParams["gID"]);
				$tpl->set("main_group",$this->mMainGroupObject);
				$tpl->set("scope",$this->mScope);
				$tpl->set("action",$this->mAction);
				$tpl->set("name_filter",$this->mParams["username"]);
				$tpl->set("sortByFilter",$this->mParams["sortByFilter"]);
				$tpl->set("totalrec",$this->mGroupMembersCount);
				
				$tpl->set("isSubGroup",$this->mIsSubGroup);
				$tpl->set("viewerType",$this->mViewerType);
				if( 3 > $this->mViewerType ){
					$tpl->set("grpSuperStaffCount",sizeof($this->mGroupSuperStaff));
					$tpl->set("grpStaffCount",sizeof($this->mGroupStaff)-sizeof($this->mGroupSuperStaff));
				}else{
					$tpl->set("grpSuperStaffCount",0);
					$tpl->set("grpStaffCount",sizeof($this->mGroupStaff));
				}
				$tpl->out("travellog/views/member_staff/tpl.ViewMemberSearchResults.php");
				
				/*$content = $tpl->fetch("travellog/views/member_staff/tpl.ViewMemberSearchResults.php");
				echo $content;
				
				$this->setCacheKey($cache,$content,$type="results");*/
			}catch(exception $e){
				$this->renderError($e->getMessage());
			}
		}
		
		function renderMemberSearchResultList(){
			/*$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null && $content = $cache->get($this->generateCacheKey("URL",array("type"=>"resultlist")))){
				echo $content;		// display the cached content
				return;
			}*/
			
			$paging = new Paging( $this->mGroupMembersCount, $this->mPage, 'grpID=' . $this->mGroupObject->getGroupID(), $this->mRpp, false );
			$paging->setOnclick($this->mPageClickAction);
			$iterator = new ObjectIterator( $this->mGroupMembers, $paging );
			
			$tpl = new Template();
			$tpl->set("members",$this->mGroupMembers);
			$tpl->set("accessedGroup",$this->mAccessedGroup);
			$tpl->set("parent",$this->mMainGroupObject);
			$tpl->set("group",$this->mGroupObject);
			$tpl->set("isAdminGroup",$this->mIsAdminGroup);
			$tpl->set("isNewMemberDisplay",$this->mIsNewMemberDisplay);
			$tpl->set("main_group",$this->mMainGroupObject);
			$tpl->set("paging",$paging);
			$tpl->set("iterator",$iterator);
			$tpl->set("viewerType",$this->mViewerType);
			$tpl->set("loggedUser",$this->mLoggedUser);
			$tpl->out("travellog/views/member_staff/tpl.ViewMemberSearchResultList.php");
			
			/*$content = $tpl->fetch("travellog/views/member_staff/tpl.ViewMemberSearchResultList.php");
			echo $content;
			
			$this->setCacheKey($cache,$content,$type="resultlist");*/
		}
		
		function showMainGroupMembers(){
			$this->mGroupObject = new AdminGroup($this->mParams["gID"]);
			$this->mIsAdminGroup = ( 2 == $this->mGroupObject->getDiscriminator() )? TRUE : FALSE;
			
			$cond = new Condition;
			$cond->setAttributeName("username");
			$cond->setOperation(FilterOp::$ORDER_BY);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			$this->mRowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			
			$sp_array = FALSE;
			$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null){
				$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"members")));
			}
			if( !$sp_array ){
				$sp_array = $this->mGroupObject->getGroupMembers($this->mRowsLimit,NULL,$order,FALSE,$this->mIsPrivileged);
				$this->setCacheKey($cache,$sp_array,$type="members");
			}		
			
			$this->mGroupMembers = $sp_array[0];
			$this->mGroupMembersCount = $sp_array[1];
			
			$this->mPageClickAction = "gmsManager.updateMainGroupMembers";
			$this->renderMemberSearchResults();
		}
		
		function showUnassignedMembers(){
			$this->mGroupObject = new AdminGroup($this->mParams["gID"]);
			$this->mIsAdminGroup = ( 2 == $this->mGroupObject->getDiscriminator() )? TRUE : FALSE;

			$cond = new Condition;
			$cond->setAttributeName("username");
			$cond->setOperation(FilterOp::$ORDER_BY);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			$this->mRowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			
			$sp_array = FALSE;
			$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null){
				$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"unassignedmembers")));
			}
			if( !$sp_array ){
				$sp_array = $this->mGroupObject->getGroupMembers($this->mRowsLimit,NULL,$order,TRUE);
				$this->setCacheKey($cache,$sp_array,$type="unassignedmembers");
			}
			$this->mGroupMembers = $sp_array[0];
			$this->mGroupMembersCount = $sp_array[1];
			
			$this->mPageClickAction = "gmsManager.fetchUnassignedMembers";
			$this->renderMemberSearchResults();
		}
		
		function fetchUnassignedMembers(){
			$this->mGroupObject = new AdminGroup($this->mParams["gID"]);
			$this->mIsAdminGroup = ( 2 == $this->mGroupObject->getDiscriminator() )? TRUE : FALSE;
			
			$orderBy = FilterOp::$ORDER_BY;
			if( 1 == $this->mParams["sortByFilter"]){
				$orderBy = FilterOp::$ORDER_BY_DESC;
			}
			$cond = new Condition;
			$cond->setAttributeName("username");
			$cond->setOperation($orderBy);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			$this->mRowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			
			$sp_array = FALSE;
			$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null){
				$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"unassignedmembers")));
			}
			if( !$sp_array ){
				$sp_array = $this->mGroupObject->getGroupMembers($this->mRowsLimit,NULL,$order,TRUE);
				$this->setCacheKey($cache,$sp_array,$type="unassignedmembers");
			}
			$this->mGroupMembers = $sp_array[0];
			$this->mGroupMembersCount = $sp_array[1];
			
			$this->mPageClickAction = "gmsManager.fetchUnassignedMembers";
			$this->renderMemberSearchResults();
		}
		
		function showNewMembers(){
			$this->mGroupObject = new AdminGroup($this->mParams["gID"]);
			$this->mIsAdminGroup = ( 2 == $this->mGroupObject->getDiscriminator() )? TRUE : FALSE;
			
			if ( $this->mIsAdminGroup && 0 < $this->mGroupObject->getParentID() ){
				$this->mMainGroupObject = new AdminGroup($this->mGroupObject->getParentID());
				if ( !($this->mMainGroupObject instanceof AdminGroup) ){
					$this->mMainGroupObject = NULL;
				}
			}
		
			$orderBy = FilterOp::$ORDER_BY_DESC;
			$cond = new Condition;
			$cond->setAttributeName("tblGrouptoTraveler.adate");
			$cond->setOperation($orderBy);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			$cond1 = new Condition;
			$cond1->setAttributeName("CAST(tblGrouptoTraveler.adate AS DATE)");
			$cond1->setOperation(FilterOp::$GREATER_THAN_OR_EQUAL);
			$cond1->setValue("DATE_SUB(CURDATE(), INTERVAL 1 MONTH)");
			$where = new FilterCriteria2();
			$where->addCondition($cond1);
			
			$this->mRowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			
			$sp_array = FALSE;
			$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null){
				$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"newmembers")));
			}
			if( !$sp_array ){
				$sp_array = $this->mGroupObject->getGroupMembers($this->mRowsLimit,$where,$order);
				$this->setCacheKey($cache,$sp_array,$type="newmembers");
			}
			$this->mGroupMembers = $sp_array[0];
			$this->mGroupMembersCount = $sp_array[1];
			
			$this->mPageClickAction = "gmsManager.fetchNewMembers";
			$this->mIsNewMemberDisplay = TRUE;
			$tpl = new Template();
			$tpl->set("controller",$this);
			$tpl->set("qCount",$this->mParams["qCount"]);
			$tpl->set("qRange",$this->mParams["qRange"]);
			$tpl->set("sortByFilter",$this->mParams["sortByFilter"]);
			$tpl->out("travellog/views/member_staff/tpl.ViewNewMembers.php");
		}
		
		function fetchNewMembers(){
			$this->mGroupObject = new AdminGroup($this->mParams["gID"]);
			$this->mIsAdminGroup = ( 2 == $this->mGroupObject->getDiscriminator() )? TRUE : FALSE;
			
			if ( $this->mIsAdminGroup && 0 < $this->mGroupObject->getParentID() ){
				$this->mMainGroupObject = new AdminGroup($this->mGroupObject->getParentID());
				if ( !($this->mMainGroupObject instanceof AdminGroup) ){
					$this->mMainGroupObject = NULL;
				}
			}
			
			$orderBy = ( self::RANK_ASC == $this->mParams["sortByFilter"] )? FilterOp::$ORDER_BY : FilterOp::$ORDER_BY_DESC;
			$cond = new Condition;
			$cond->setAttributeName("tblGrouptoTraveler.adate");
			$cond->setOperation($orderBy);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			
			$count = ( strval(intval($this->mParams["qCount"])) == $this->mParams["qCount"] ) ? $this->mParams["qCount"] : 1 ;
			
			switch($this->mParams["qRange"]){
				case self::DAY	:	$range = "DAY"; break;
				case self::WEEK	:	$range = "WEEK"; break;
				case self::MONTH:	$range = "MONTH"; break;
				case self::YEAR	:	$range = "YEAR"; break;
				default			:	$range = "MONTH"; break;
			}
			
			$cond1 = new Condition;
			$cond1->setAttributeName("CAST(tblGrouptoTraveler.adate AS DATE)");
			$cond1->setOperation(FilterOp::$GREATER_THAN_OR_EQUAL);
			$cond1->setValue("DATE_SUB(CURDATE(), INTERVAL $count $range)");
			$where = new FilterCriteria2();
			$where->addCondition($cond1);
			
			$this->mRowsLimit = new RowsLimit($this->mRpp, $this->calcOffset($this->mPage));
			
			$sp_array = FALSE;
			$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null){
				$sp_array = $cache->get($this->generateCacheKey("URL",array("type"=>"newmembers")));
			}
			if( !$sp_array ){
				$sp_array = $this->mGroupObject->getGroupMembers($this->mRowsLimit,$where,$order);
				$this->setCacheKey($cache,$sp_array,$type="newmembers");
			}
			$this->mGroupMembers = $sp_array[0];
			$this->mGroupMembersCount = $sp_array[1];
			
			$this->mPageClickAction = "gmsManager.fetchNewMembers";
			$this->mIsNewMemberDisplay = TRUE;
			$tpl = new Template();
			$tpl->set("controller",$this);
			$tpl->set("qCount",$this->mParams["qCount"]);
			$tpl->set("qRange",$this->mParams["qRange"]);
			$tpl->set("sortByFilter",$this->mParams["sortByFilter"]);
			$tpl->out("travellog/views/member_staff/tpl.ViewNewMembers.php");
			
			//$this->renderMemberSearchResultList();
		}
		
		private function renderError($message=""){
			$tpl = new Template();
			$tpl->set("error","Message from ViewGroupMemberSearch: { ".$message." }");
			$tpl->out("travellog/views/member_staff/tpl.MemberStaffError.php");
		}

		private function calcOffset($page=1){
			return ($page-1) * $this->mRpp;
		}
		
		private function showRecentActivity(){
			$content = FALSE;
			$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null){
				$content = $cache->get($this->generateCacheKey("URL",array("type"=>"recentactivity")));
			}
			if( !$content ){
				$sort = ( 0 == $this->mParams["sortByFilter"] )? "DESC" : "ASC";
				require_once('travellog/model/updates/Class.GroupRecentActivity.php');
				$recentActivity = new GroupRecentActivity($this->mGroupObject);
				$recentActivity->prepareUpdates(
					array(
						'LOGGED_TRAVELER'	=> $this->mLoggedUser,
						'SORT_ORDER'		=>	$sort,
						'VIEWER_TYPE'		=>	$this->mViewerType 
				));
				
				$content = $recentActivity->render();
				$this->setCacheKey($cache,$content,$type="recentactivity");
			}
			echo $content;
		}
		
		private function fetchRecentActivity(){
			$content = FALSE;
			$cache = ganetCacheProvider::instance()->getCache();
			if ($cache != null){
				$content = $cache->get($this->generateCacheKey("URL",array("type"=>"recentactivity")));
			}
			if( !$content ){
				$sort = ( 0 == $this->mParams["sortByFilter"] )? "DESC" : "ASC";
				require_once('travellog/model/updates/Class.GroupRecentActivity.php');
				$recentActivity = new GroupRecentActivity($this->mGroupObject);
				$recentActivity->prepareUpdates(
					array(
						'LOGGED_TRAVELER'	=> $this->mLoggedUser,
						'SORT_ORDER'		=>	$sort,
						'VIEWER_TYPE'		=>	$this->mViewerType 
				));
				
				$content = $recentActivity->render();
				$this->setCacheKey($cache,$content,$type="recentactivity");
			}
			echo $content;
		}
		
		
		/**
		 * 
		 * @param $keySource	where the cache key is derived
		 * @param $params
		 * @return string
		 */
		protected function generateCacheKey($keySource = 'URL',$params = array()){
			$key = '';
			if ($keySource == 'URL'){
				$key	=	'Group_SearchMembers_' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . $_SERVER['QUERY_STRING'];
			}
			if( isset($params["type"]) ){
				$key .= "_".$params["type"];
			}
			if( $this->mIsPrivileged ){
				$key .= "_adminview";
			}
			return $key;
		} 
		
		private function setCacheKey($cache=NULL,$value=NULL,$type){
			if( $cache != NULL ){
				$key = $this->generateCacheKey("URL",array("type"=>$type));
				$cache->set($key,$value,array('EXPIRE'=>3600));
				// store keys for invalidation
				$hashes = $cache->get('Group_SearchMembers_hashes_' . $this->mParams["gID"]);
				
				if ($hashes) {
					$hashes[] =	$key;
					$cache->set('Group_SearchMembers_hashes_' . $this->mParams["gID"],$hashes,array('EXPIRE'=>3600));
				} else {
					$cache->set('Group_SearchMembers_hashes_' . $this->mParams["gID"],array($key),array('EXPIRE'=>3600));
				}
			}
		}
		
	}