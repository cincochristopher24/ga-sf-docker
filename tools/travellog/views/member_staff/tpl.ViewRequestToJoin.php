<div id="requesting_users" class="section">
	<? if( 0 < $request_count ): ?>
		<h2> <span> Showing Requests to Join</span>  </h2>
		<div class="section_small_details">
			Travelers with join requests: <strong><?=$request_count?></strong>
		</div>
		<!-- END section_small_details -->
		<div id="join_requests" class="content">
			<? echo $controller->renderRequestList(); ?>								
		</div>
	<!-- END content -->
	<? else: ?>
		<h2> <span> Requests to Join</span>  </h2>
		<div class="section_small_details">
			There are no requests so far.
		</div>
	<? endif; ?>						
</div>
<!-- END requesting_users -->