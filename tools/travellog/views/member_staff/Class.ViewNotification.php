<?php
	/*
	 *	@author CheWyl
	 *	19 November 2008
	 */

	require_once 'Class.Template.php';

	class ViewNotification{

		private $mGroup				= null;
		private $mParent			= null;
		private $mSubGroup			= null;
		private $arrStaff			= array();
		private $mPreviewMessage	= '';
		private $mEditableMessage	= '';

		function setGroup($_group = null){
			$this->mGroup = $_group;
		}
		function setSubGroup($_group = null){
			$this->mSubGroup = $_group;
		}
		function setParent($_group = null){
			$this->mParent = $_group;
		}
		function setInvitedStaff($_staff = array()){
			$this->arrStaff = $_staff;
		}
		function setEditedMessage($_message = ''){
			$this->mEditableMessage = stripslashes($_message);
		}
		function getEditedMessage(){
			return $this->mEditableMessage;
		}

		function retrieve(){
			$mData		= MembersPanel::getConfigPerServer($_SERVER['SERVER_NAME']);
			$mSiteName	= (0 < count($mData)) ? $mData['siteName'] : "GANET";
			$mEmailName	= (0 < count($mData)) ? $mData['name'] : "The GoAbroad Network Team";

			if (1 == count($this->arrStaff)){
				$mTemp		= $this->arrStaff;
				$mDetail	= $mTemp[0];
				$siteUrl	= "http://" . $_SERVER['SERVER_NAME'];
				$mLink		=  $siteUrl . $this->mSubGroup->getFriendlyURL();

				if (isset($mDetail['traveler'])){
					$mTraveler	= $mDetail['traveler'];
					$mFullName	= ucfirst($mTraveler->getTravelerProfile()->getFirstName()) . ' ' .
				 				ucfirst($mTraveler->getTravelerProfile()->getLastName());

					// check if group is parent group
					if ($this->mGroup->isParent())
						$mParent = $this->mGroup;
					else
						$mParent = $this->mGroup->getParent();

					// if superstaff | administrator | invited as superstaff and member of parent group
					if ($mParent->isSuperStaff($mTraveler->getTravelerID()) || ($mParent->getAdministratorID() == $mTraveler->getTravelerID()) || ($this->mSubGroup->isParent() && $mParent->isMember($mTraveler)))
						$mEmailLink = $siteUrl . '/group-privacy.php?groupID=' . $mParent->getGroupID();
					else
						$mEmailLink = $siteUrl . '/privacy.php';

					// if member of the parent group, the traveler will automatically be a staff of the subgroup
					if ($mParent->isMember($mTraveler)){
						$this->mPreviewMessage = 'Hi ' . $mFullName . ',<br /><br />' . $this->mEditableMessage . '<br /><br />' .
												'To visit ' . $this->mSubGroup->getName() . ' homepage, follow the link below:<br />' .
												'<a href = "' . $mLink . '">' . $mLink . '</a><br /><br />' .
												'Many thanks,<br /><br />' . $mEmailName . '<br />' .
												'<a href = "' . $siteUrl . '">' . $siteUrl . '</a><br /><br /><br />' .

												'- - - - - -<br />You received this email because you are a member of ' .
												$mParent->getName() . ' on ' . $mSiteName . '. To manage the emails you ' .
												'receive from ' . $mSiteName . ', please follow this link: <a href="' . $mEmailLink .
												'">' . $mEmailLink . '</a><br />' .
												'<br />This e-mail was sent from an unattended mailbox. Please do not reply. ' .
												'Send any questions to <a href="' . $siteUrl . '/feedback.php">' . $siteUrl . '/feedback.php</a>';
					}
					// if non-member of the parent group
					else{
						$this->mPreviewMessage = 'Hi ' . $mFullName . ',<br /><br />' . $this->mEditableMessage . '<br /><br />' .
												'To confirm invitation as staff for the group ' . $this->mSubGroup->getName() .
												', follow the link below:<br />' .
												'<a href = "' . $siteUrl . '/membership.php">' . $siteUrl . '/membership.php</a><br /><br />' .
												'To visit ' . $this->mSubGroup->getName() . ' homepage, ' .
												'follow the link below:<br />' .
												'<a href = "' . $mLink . '">' . $mLink . '</a><br /><br />' .
												'Many thanks,<br /><br />' . $mEmailName . '<br />' .
												'<a href = "' . $siteUrl . '">' . $siteUrl . '</a><br /><br /><br />' .

												'- - - - - -<br />You received this email because you have an account in ' . $mSiteName .
												'. To manage the emails you ' .
												'receive from ' . $mSiteName . ', please follow this link: <a href="' . $mEmailLink .
												'">' . $mEmailLink . '</a><br />' .
												'<br />This e-mail was sent from an unattended mailbox. Please do not reply. ' .
												'Send any questions to <a href="' . $siteUrl . '/feedback.php">' . $siteUrl . '/feedback.php</a>';
					}
				}
				elseif (isset($mDetail['email'])){
					// check if group is parent group
					if ($this->mGroup->isParent())
						$mParent = $this->mGroup;
					else
						$mParent = $this->mGroup->getParent();

					// if invited as superstaff
					if ($this->mSubGroup->isParent())
						$mEmailLink = $siteUrl . '/group-privacy.php?groupID=' . $mParent->getGroupID();
					else
						$mEmailLink = $siteUrl . '/privacy.php';

					$this->mPreviewMessage = 'Hello ' . $mDetail['email'] . '!<br /><br />Greetings from GoAbroad.Net!<br /><br />' .
											$this->mEditableMessage . '<br /><br />A new GoAbroad.Net account was also created for you with the ' .
											'following information:<br /><br />Email: <i>[Email]</i><br />Password: <i>[Password]</i>' .
											'<br /><br />To visit ' . $this->mSubGroup->getName() . ' homepage, ' .
											'follow the link below:<br /><a href = "' . $mLink . '">' . $mLink . '</a><br /><br />' .
											'Many thanks,<br /><br />' . $mEmailName . '<br />' .
											'<a href = "' . $siteUrl . '">' . $siteUrl . '</a><br /><br /><br />' .

											'- - - - - -<br />To manage the emails you receive from ' . $mSiteName .
											', please follow this link: <a href="' . $mEmailLink . '">' . $mEmailLink . '</a><br />' .
											'<br />This e-mail was sent from an unattended mailbox. Please do not reply. ' .
											'Send any questions to <a href="' . $siteUrl . '/feedback.php">' . $siteUrl . '/feedback.php</a>';
				}
			}
			else{
				$siteUrl	= "http://" . $_SERVER['SERVER_NAME'];

				if (!$this->mGroup->isParent()){
					$mLink =  $siteUrl . $this->mGroup->getFriendlyURL();
					$this->mPreviewMessage = 'Hi <i>[Name]</i>,<br /><br />' . $this->mEditableMessage . '<br /><br />' .
											'To visit ' . $this->mGroup->getName() . ' homepage, follow the link below:<br />' .
											'<a href = "' . $mLink . '">' . $mLink . '</a><br /><br />' .
											'Many thanks,<br /><br />' . $mEmailName . '<br />' .
											'<a href = "' . $siteUrl . '">' . $siteUrl . '</a><br /><br /><br />' .

											'- - - - - -<br />You received this email because <i>[you have an account in / you are a member of ' .
											']</i> ' . $mSiteName . '. To manage the emails you ' .
											'receive from ' . $mSiteName . ', please follow this link: <i>[Privacy Preference]</i><br />' .
											'<br />This e-mail was sent from an unattended mailbox. Please do not reply. ' .
											'Send any questions to <a href="' . $siteUrl . '/feedback.php">' . $siteUrl . '/feedback.php</a>';
				}
				else
					$this->mPreviewMessage = 'Hi <i>[Name]</i>,<br /><br />' . $this->mEditableMessage . '<br /><br />' .
											'To visit <i>[group name]</i> homepage, follow the link below:<br />' .
											'<i>[url site]</i><br /><br />' .
											'Many thanks,<br /><br />' . $mEmailName . '<br />' .
											'<a href = "' . $siteUrl . '">' . $siteUrl . '</a><br /><br /><br />' .

											'- - - - - -<br />You received this email because <i>[you have an account in / you are a member of ' .
											']</i> ' . $mSiteName . '. To manage the emails you ' .
											'receive from ' . $mSiteName . ', please follow this link: <i>[Privacy Preference]</i><br />' .
											'<br />This e-mail was sent from an unattended mailbox. Please do not reply. ' .
											'Send any questions to <a href="' . $siteUrl . '/feedback.php">' . $siteUrl . '/feedback.php</a>';
			}
		}

		function render(){
			$tpl = new Template;

			$tpl->set("arrStaff",			$this->arrStaff);
			$tpl->set("groupId",			$this->mGroup->getGroupID());
			$tpl->set("actual_message",		$this->mEditableMessage);
			$tpl->set("preview_message",	$this->mPreviewMessage);

			$tpl->out("travellog/views/member_staff/tpl.ViewNotification.php");
		}
	}
?>