<?php
	/*
	 *	@author CheWyl
	 *	22 December 2008
	 */

	$arrGroupId		= array();
	$arrCheckBox	= array();
	$mTitle = "Manage Staffed Groups of <strong>" . $traveler->getUserName() . "</strong>";

	foreach($arrStaffedSGs as $subgroup)
		$arrGroupId[] = $subgroup->getGroupID();
	if ($isSuperStaff)
		$mAction = 'gID=' . $group->getGroupID() . '&mode=2&tID=' . $traveler->getTravelerID();
	else{
		if ( $chosenGroup ){
			$mAction = 'gID=' . $group->getGroupID() . '&mode=2&tID=' . $traveler->getTravelerID() . '&cID=' . $chosenGroup->getGroupID();
		}
		else{
			$mAction = 'gID=' . $group->getGroupID() . '&mode=2&tID=' . $traveler->getTravelerID();
		}
	}
?>

<table class="table_popbox">
	<tbody>
		<tr>
			<td class="popbox_topLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_topRight"></td>
		</tr>
		<tr>
			<td class="popbox_border"></td>
			<td class="popbox_content">
				<h4 id="popup_header" class="header"><?=$mTitle?></h4>
				<div id="popup_confirm" class="confirm">
					<div style="display: none;" id="loading_image">
						<span> <center> <img src="/images/loading.gif"/> </center> </span>
					</div>
				
					<div class="group_checkboxes">
						<ul>
							<? foreach($arrSubGroups as $subgroup) : ?>
							<?
								$checked		= "";
								$chkDisabled	= "";
								if (in_array($subgroup->getGroupID(), $arrGroupId)){
									$checked = "checked='checked'";
								if ($isSuperStaff)
									$chkDisabled = "disabled='true'";
								}
							?>
								<li>
									<input type="checkbox" id="chk<?=$subgroup->getGroupID()?>" class="manage_group_checkbox" name="group_checkbox" value="<?=$subgroup->getGroupID()?>" <?=$checked?> <?=$chkDisabled?> />&nbsp;<?=stripslashes($subgroup->getName())?>
								</li>
								<? $arrCheckBox[] = $subgroup->getGroupID() ?>
								<? endforeach; ?>
								<? unset($subgroup); ?>
					    </ul>
					</div>
				</div>	
				<div id="popup_buttons" class="buttons_box">
					<? if ($isSuperStaff) : ?>
						<input type="button" id="btnManageAdmin" value="OK" disabled="true" class="prompt_button" />
					<? else : ?>
						<input type="button" id="btnManageStaff" value="OK" disabled="true" class="prompt_button" />
					<? endif; ?>
					<input type="button" id="btnManageCancel" value="Cancel" class="prompt_button"/>
					<input type="hidden" id="lstCheckBox" value="<?=implode(",", $arrCheckBox)?>" class="prompt_button"/>
				</div>
			</td>
			<td class="popbox_border"></td>
		</tr>
		<tr>
			<td class="popbox_bottomLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_bottomRight"></td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$(".manage_group_checkbox").click(function(){
				var gId = $('option:selected', $('#groupselect_regular_staff')).val();
				StaffManager.enableButton( gId );
			});
			$("#btnManageAdmin").click(function(){
				StaffManager.manageSuperStaffAssignment('<?=$mAction?>');
			});
			$("#btnManageStaff").click(function(){
				StaffManager.manageStaffAssignment('<?=$mAction?>');
			});
			$("#btnManageCancel").click(function(){
				StaffManager.removeThickbox();
			});
		});
	})(jQuery);
</script>