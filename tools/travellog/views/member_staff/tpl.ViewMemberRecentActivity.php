<?$dateTime = new GaDateTime;?>

<div id="all_groups_programs" class="section">
	<h2> <span>SHOWING RECENT MEMBER ACTIVITIES</span> </h2>
	<div class="section_small_details"> 
		<form onsubmit="gmsManager.updateRecentActivity(); return false;"> 
			<ul class="list_small_details">											
				<li>
					Displaying Recent Activities in the last 24 hours
				</li>										

				<li>
					<label for="sort_by_filter">Order</label>
					<select id="sort_by_filter" name="sort_by_filter">
						<option value="0" <?=("DESC" == $sortOrder)? 'selected="selected"' : ''?> >Descending</option>
						<option value="1" <?=("ASC" == $sortOrder)? 'selected="selected"' : ''?> >Ascending</option>
					</select>								
				</li>
			
				<li>
					<input type="submit" name="refresh_list" value="Refresh List" id="refresh_list"/>
				</li>
			
				<li>
					<img id="loading_content" src="/images/loading_small.gif" style="display:none;" />
				</li>										
			</ul>
		</form>
	</div>
	<div class="content" id="recent_activity_content" >
		<?=$GroupRecentActivityObject->renderRecentActivityList()?>
	</div>
</div>