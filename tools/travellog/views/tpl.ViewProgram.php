<?=$obj_navigation->show();?>
<div id="content_wrapper" class="layout_2">
	
	<div id="wide_column">

			<div class="section" id="program_list">
				<h1 class="big_header"><span>Itinerary</span></h1>
				<div class="content">
					<p class="loading_message" style="display:none">
					    <span id="imgLoading">
					        <img alt="Loading" src="/images/loading.gif"/>
					    </span>
					    <em id="statusCaption">Please wait...</em>
					</p>
					<div id="content_details"></div> 
					
					<div id="activity_lists"></div>
				</div>
				<div class="foot"></div>
			</div>

	</div>
	
	<div id="narrow_column">
		<div class="section" id="quick_tasks">
			<h2><span>Itineraries</span></h2>
			<div class="content">
				<?=$contents?>
			</div>
			<div class="foot"></div>
		</div>
	</div>
	
	<div class="clear"></div>
</div>

