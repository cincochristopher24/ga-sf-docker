<?php
/**
 * <b>View event</b> contents template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */

Template::setMainTemplateVar('page_location', 'My Passport');

require_once('Class.HtmlHelpers.php');
require_once('Class.GaDateTime.php');
$d = new GaDateTime();
?>

<div class="area" id="area_left">
	<div class="section" id="quick_tasks">
		<ul class="actions">
			<li><a href="javascript:void(0)" onclick="manager.displayForm('mode=add')" class="button"><span class="rc_top_left"></span><span class="rc_top_right"></span>Create Event<span class="rc_bot_left"></span><span class="rc_bot_right"></span></a></li>
		</ul>
		<h1>Calendar</h1>
		<div class="content" id="events_calendar">
			<ul class="events">
				<?
					$obj_iterator->rewind();
					while( $obj_iterator->valid() ):?>
						<li id="title<?=$obj_iterator->current()->getEventID() ?>" class="viewevents">
							<div class="meta date">
								<?= $d->set($obj_iterator->current()->getTheDate())->htmlDateFormat();?>
							</div>
							<h3><a href="javascript:void(0)" onclick="manager.toggleEvent(<?= $obj_iterator->current()->getEventID()?> ,<?= $obj_iterator->current()->getContext()?> );"><?= HtmlHelpers::WrapText($obj_iterator->current()->getTitle(), 16, "<br />");?></a></h3>
							<div class="clear"></div>
						</li>
						<? $obj_iterator->next();?>
				<?	endwhile;?>
				
			</ul>
		</div>
	</div>	
</div>


	<div class="section">
		<div class="content">
			<div id="view"></div>
			<? if ( $obj_paging->getTotalPages() > 1 ): ?>
			<div class="section_foot paging">
				<? $obj_paging->getPrevious() ?> |
				<? $obj_paging->getNext() ?>
			</div>
			<? endif; ?>
			<div id="eventID" style="display: none;"><?= $primaryID ?></div>
			<div id="context" style="display: none;"><?= $context   ?></div>
		</div>
	</div>
