<?php
	Template::includeDependentJs('/min/g=TravelerProfile');
	Template::includeDependentCss('/css/sharingAndConnectionsWidget.css');
	Template::includeDependentCss('/css/vsgStandardized.css');
	
?>
<!-- Begin Top Area -->
<?php echo $profile_comp->render()?>
<!-- End Top Area -->

<?php $subNavigation->show(); ?>

<div id="rundown">
    <!-- Begin Content Wrapper -->
    <div id="content_wrapper">

        <!-- Begin Wide Column -->
        <div id="wide_column" class="layout2">                      
      		
			<!-- added by bert on May 11, 2009 -->
			<!-- Applications Help texts -->
			<p id="apps_help_text">
				We now let you share your travels even with your non-traveler friends - 
				just add our app on Facebook, MySpace, Orkut and hi5 to bring the traveler 
				in you to your pages on any of these sites!				
			</p>
			<!-- End of Applications Help texts -->
			<div class="section widget_app" id="sharingAndConnectionsWrapper">
				<h2>Sharing & Connections</h2>
				<ul>
					<li>
						<p><span class="bold1">It's easy and takes only a few seconds!</span> Connect your <?=$siteName?> Network profile to these applications and be able to update your social media accounts whenever you post journals, articles, photos, and videos.</p>
					</li>

					<li>
						<ul id="widgetWrapper">
							<li class="line" id="twitterWidgetWrapper">
								<?php echo $twitterWidget?>
							</li>
							<li class="line" id="facebookWidgetWrapper">
								<?php echo $facebookWidget ?>														
							</li>
						</ul>
					</li>
				</ul>
			</div>
			
            <!-- Begin My Travel Map Widget-->
            <div class="section widget_app" id="travelmap_passport">
                <h2><span>My Travel Map</span></h2>
                <div class="content"><?php echo $travelMap; ?></div>
            </div>      
            <!-- End My Travel Map Widget-->
        
            <!-- Begin My Travel Bio Widget-->
            <div class="section widget_app" id="travelbio_passport">
                <h2><span>My Travel Bio</span></h2>
                <div class="content"><?php echo $travelBio; ?></div>
            </div>      
            <!-- End My Travel Bio Widget-->    
        
            <!-- Begin My Travel Plans Widget-->
            <div class="section widget_app" id="travelplans_passport">
                <h2><span>My Travel Plans</span></h2>
                <div class="content"><?php echo $incTravelplans; ?></div>
            </div>      
            <!-- End My Travel Plans Widget-->                      
        </div>  
        <!-- End Wide Column -->
  
        <!-- Begin Narrow Column -->
        <div id="narrow_column">
      
            <!-- Begin What Are Widgets? -->                
            <div class="helpextra"></div>
          	<div class="helpbox">
					<!-- added by bert on May 11, 2009 -->
					<!-- Applications Help texts -->
					<h2>What are Apps?</h2>
                    <p><?=$siteName?> Network has four applications that you can add to your profile on Facebook, MySpace, Orkut and hi5.
					<p>We know most of your friends are on these social networks, but GoAbroad Network has the cool tools 
					   that let you share your travel experiences best. By adding our applications on your Facebook, MySpace, 
					   Orkut or hi5 profile, you can keep journalling on GoAbroad Network and be confident that your journals 
					   are instantly shared with your friends on these sites.</p>
					<!-- Applications Help texts -->
			</div>	
			<div class="widgets_description">	
				<ul>
                	<?php
                	$widgetDescriptions = array(
                		'My Travel Journals' => array(
                			'labelImageId' => 'desc_mtjournals',
                			'description'  => 'Post your journals and photos and let your friends and family in on your experience!'
                		),
                		'My Travel Map' => array(
                			'labelImageId' => 'desc_mtmap',
                			'description'  => 'Point out the countries you\'ve been to and show your travel map on your profile. If
                                               you have travel journals, this app will show how many journals, photos and tips you
                                               have for every country.'
						),
						'My Travel Bio' => array(
							'labelImageId' => 'desc_mtbio',
                			'description'  => 'Share your most memorable travel experiences! Let others know how interesting your 
                                               travels have been, and see what your friends have to say about their travels.'
                		),						
                		'My Travel Plans' => array(
							'labelImageId' => 'desc_mtplans',
							'description'  => 'Share your travel plans and find out who could be traveling your way! '
						)	
                	);
                
                	$containerIconClassName = array(
                		'Facebook'   => 'facebook_direct',
                		'hi5!'       => 'hi5_direct',
                		'MySpace'    => 'myspace_direct',
                		'Friendster' => 'friendster_direct',
                		'Bebo'       => 'bebo_direct',
                		'Orkut'      => 'orkut_direct'
                	);
                
                	foreach ($widgets as $widgetName => $externalLinks) { ?>
                	<li class="section">
                        <h3 id="<?php echo $widgetDescriptions[$widgetName]['labelImageId']; ?>">
                			<?php echo $widgetName; ?>
                        </h3>
                        <p><?php echo $widgetDescriptions[$widgetName]['description']; ?></p>

                            <ul class="widget_live">
                                <li class="live_text">Access this app in</li>
                        		<?php 
                      			foreach ($externalLinks as $container => $url) { ?>
                      
                                <li class="<?php echo $containerIconClassName[$container]; ?>">
                                    <a href="<?php echo $url; ?>" title="Install this App in <?php echo $container;?>" target="_blank">
                          				<?php echo $container;?>
                                    </a>
                                </li>
                      
                      			<?php    
                      			}//end inner foreach
                      			?>
                            </ul>                     
                    </li>  
                	<?php 
                	}//end outer foreach
                	?>
                    </ul> 
            </div>        
                <!-- End What are Widgets? -->
  
        </div>
        <!-- End Narrow Section --> 
        
    </div>
    <!-- End Content Wrapper -->

</div>    
<!--  End Rundown -->

<?php if (isset($editModeMTM) && $editModeMTM) : ?>
<script type="text/javascript">
(function () {
    var target = LIB.getEBI('travelmap_passport');
    window.scroll(0, target.offsetTop);
    LIB.colorFade(target.getElementsByTagName('div')[0], {color: 'blue'});
})();    
</script>    
<?php elseif (isset($editModeMTB) && $editModeMTB) : ?>
<script type="text/javascript">
(function () {
    var target = LIB.getEBI('travelbio_passport');
    window.scroll(0, target.offsetTop);
    LIB.colorFade(target.getElementsByTagName('div')[0], {color: 'blue'});
})();    
</script>    
<?php elseif (isset($editModeMTP) && $editModeMTP) : ?>
<script type="text/javascript">
(function () {
    var target = LIB.getEBI('travelplans_passport');
    window.scroll(0, target.offsetTop);
    LIB.colorFade(target.getElementsByTagName('div')[0], {color: 'blue'});
})();    
</script>    
<?php endif; ?>