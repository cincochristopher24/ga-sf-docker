<div class="pagination">
  <div class="pagination_pages">
    <?= $first_tag ?>
    <?= $prev_tag ?> 
		<?= $number_pages_tag ?>
    <?= $next_tag ?>
    <?= $last_tag ?>
  </div>
  <div class="clear"></div>
</div>
