<?php
	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.SessionManager.php';
	require_once 'travellog/model/travelbio/Class.TravelBioUser.php';
	require_once 'travellog/model/travelbio/Class.ProfileQuestion.php';

	class RandomTravelBioView{
		private $numQ				= 0;
		private $ownerId			= 0;
		private $questionId			= 0;
		private $profileAnswer		= '';
		private $isOwner			= false;
		private $hideDiv			= false;
		private $showAction			= false;
		private $profileQuestion	= '';

		function setHideDiv($_hide = false){
			$this->hideDiv = $_hide;
		}
		function setOwnerId($_ownerId = 0){
			$this->ownerId = $_ownerId;
		}
		function setQuestionId($_qId = 0){
			$this->questionId = $_qId;
		}
		function setShowAction($_show = false){
			$this->showAction = $_show;
		}
		function getProfileQuestion(){
			return $this->profileQuestion;
		}

		function readTravelBio(){
			$qCtr = 0;
			$User = TravelBioUser::getTravelBioUserByTravelerID($this->ownerId);

			if(is_null($User))
		  		$User = new TravelBioUser();
			
			$this->numQ = ProfileQuestion::getNumberofQuestionsWithAnswer($this->ownerId);
			$this->profileQuestion = ProfileQuestion::getRandomQuestionByUser($this->ownerId, $this->questionId);
			if ($this->profileQuestion){
				$Answer = $User->getAnswerByQuestion($this->profileQuestion->getQuestionID());

				if($this->profileQuestion->getQuestionType() == ProfileQuestion::$TEXT || $this->profileQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA){
					$this->profileAnswer = $Answer[0]->getValue();
				}
				else{
					$textVal = '';
					$ictr = 0;
					$tempAnswer = '';

					foreach($Answer as $iChoice){
						$tempAnswer = (0 == $ictr) ? $iChoice->getValue() : ', ' . $iChoice->getValue();
						if($iChoice->isTypeCheckbox() && 'Others' != $iChoice->getValue())
							$this->profileAnswer .= $tempAnswer;
						elseif($iChoice->isTypeText()){
							$textVal = $iChoice->getValue();
						}
						$ictr += 1;
					}
					if(strlen($textVal))
						$this->profileAnswer .= (0 == $ictr) ? $textVal : ', ' . $textVal;
					$qCtr+=1;
				}
			}
			// check ownership
			$mSession = SessionManager::getInstance();
			$mLoggedId = (0 < $mSession->get('travelerID')) ? $mSession->get('travelerID') : 0;
			$this->isOwner = ($mLoggedId == $this->ownerId) ? true : false;
		}

		function render() {
			if (0 == $this->numQ){
				$mQuestion = 'What is your travelling philosophy?';
		 		$mAnswer = "I haven't really thought about it...";
		 		$mQuestionId = 15;
			}
			else{
				$mQuestion = $this->profileQuestion->getQuestion();
		 		$mAnswer = $this->profileAnswer;
		 		$mQuestionId = $this->profileQuestion->getQuestionID();
			}

	 		$template = new Template();

			$template->set("profileQuestionId",	$mQuestionId);
			$template->set("profileQuestion",	$mQuestion);
	 		$template->set("profileAnswer",		$mAnswer);
			$template->set("showAction",		$this->showAction);
			$template->set("ownerId",			$this->ownerId);
			$template->set("hideDiv",			$this->hideDiv);
			$template->set("isOwner",			$this->isOwner);
			$template->set("numQ",				$this->numQ);

	 		return $template->fetch('travellog/views/tpl.ViewRandomTravelBio.php');
		}
	 	
		
		//TODO: refactor HAM
	 	function setupWidget($template = null) {
	 		//It would be better if get this value from db but this way its one
	 		//less query (actually at least $totalNumQ number of queries). 
	 		//This will need periodic checking though
	 		$totalNumQ = 16;
	 		//$totalNumQ = count($this->profileQuestion->getQuestions());

	 		//Defaults when all questions are unanswered
	 		$q = 'What is your travelling philosophy?';
	 		$a = "I haven't really thought about it...";
	 		$qID = 15;

	 		if ($this->numQ) {
	 			$qID = $this->profileQuestion->getQuestionID();
	 			$q = $this->profileQuestion->getQuestion();
	 			$a = $this->profileAnswer;
	 		}

	 		if (is_null($template)) {
	 			//for ajax responses
	 			return array(
	 				'questionID' => $qID, 
	 			    'question' => $q, 
	 			    'answer' => $a,
	 				'numQ' => $this->numQ,
	 			    'percentDone' => round($this->numQ * 100 / $totalNumQ)
	 			);
	 		} else {	
				$template->set("profileQuestionID", $qID);
		 		$template->set("profileQuestion", $q);
		 		$template->set("profileAnswer", $a);
				$template->set("ownerId", $this->ownerId);
				$template->set("numQ", $this->numQ);
				$template->set("totalNumQ", $totalNumQ);
				$template->set("percentDone", round($this->numQ * 100 / $totalNumQ));
	 		}	
		}

		function loadJsonValues(){
			if (0 == $this->numQ){
				$mQuestion = 'What is your travelling philosophy?';
		 		$mAnswer = "I haven't really thought about it...";
		 		$mQuestionId = 15;
			}
			else{
				$mQuestion = $this->profileQuestion->getQuestion();
		 		$mAnswer = $this->profileAnswer;
		 		$mQuestionId = $this->profileQuestion->getQuestionID();
			}

			return array(
					'numQ'			=> $this->numQ,
					'ownerId'		=> $this->ownerId,
					'answer'		=> $mAnswer,
					'question'		=> $mQuestion,
					'questionId'	=> $mQuestionId
				);
		}

		function renderTBAction($_numQ = 0){
			$template = new Template();
			$template->set("numQ", $_numQ);

			return $template->fetch('travellog/views/tpl.ViewRandomTravelBioAction.php');
		}

		function parseToXML($htmlStr) {
	  		$xmlStr=str_replace("&",'&amp;',$htmlStr);
		  	$xmlStr=str_replace('<','&lt;',$xmlStr);
		  	$xmlStr=str_replace('>','&gt;',$xmlStr);
		  	$xmlStr=str_replace('"','&quot;',$xmlStr);
		  	$xmlStr=str_replace("'",'&#39;',$xmlStr);
		  	return $xmlStr;
		}
	}
?>