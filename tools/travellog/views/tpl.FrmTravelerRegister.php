	<?php
	/*
	 * tpl.FrmTravelerRegister.php
	 * Created on Mar 22, 2007
	 * created by marc
	 */

	require_once("Class.Constants.php");
	$siteName = isset($contents['siteName']) && '' != $contents['siteName'] ? $contents['siteName'] : 'GoAbroad Network';
	$redirect = ( isset($contents['redirect']) && 0 < trim(strlen($contents['redirect']))) ? '?redirect='.$contents['redirect'] : '';
	
	$email_error = "";
	if( array_key_exists(constants::EMPTY_EMAIL_ADDRESS,$contents["arrErrorMessages"]) ):
		$email_error =  "Email address required!";
	elseif( array_key_exists(constants::INVALID_EMAIL_FORMAT,$contents["arrErrorMessages"]) ):
		$email_error =  "Invalid email address format!";
	elseif( array_key_exists(constants::EMAIL_ALREADY_USED,$contents["arrErrorMessages"]) ):
		$email_error =  "Email address already used!";
	endif;
	
	$username_error = "";
	if( array_key_exists(constants::EMPTY_USERNAME,$contents["arrErrorMessages"]) ):
		$username_error = "Screen name required!";
	elseif( array_key_exists(constants::INVALID_USERNAME_FORMAT,$contents["arrErrorMessages"]) ):
		$username_error = "Invalid screen name format!";
	elseif( array_key_exists(constants::USERNAME_ALREADY_USED,$contents["arrErrorMessages"]) ):
		$username_error = "Screen name already taken!";
	endif;
	
	$firstname_error = "";
	if( array_key_exists(constants::EMPTY_FIRSTNAME,$contents["arrErrorMessages"]) ):
		$firstname_error .= $contents["arrErrorMessages"][constants::EMPTY_FIRSTNAME];
	endif;
	$lastname_error = "";
	if( array_key_exists(constants::EMPTY_LASTNAME,$contents["arrErrorMessages"]) ):
		$lastname_error .= $contents["arrErrorMessages"][constants::EMPTY_LASTNAME];
	endif;
	
	$captcha_error = "";
	if( array_key_exists(constants::SECURITY_CODE_MISMATCH,$contents["arrErrorMessages"]) ):
		$captcha_error = $contents["arrErrorMessages"][constants::SECURITY_CODE_MISMATCH];
	endif;
	
	$agreement_error = "";
	if( array_key_exists(constants::DISAGREE_TERMS,$contents["arrErrorMessages"]) ):
		$agreement_error = $contents["arrErrorMessages"][constants::DISAGREE_TERMS];
	endif;
	
	$authorized_sp = is_null($contents["service_provider"]) ? FALSE : strtolower($contents["service_provider"]); 
	
	$password_error = ""; 
	if( array_key_exists(constants::INVALID_PASSWORD_FORMAT,$contents["arrErrorMessages"]) && !$authorized_sp ):
		$password_error = "Invalid password format!";
	endif;
	if( array_key_exists(constants::PASSWORD_CONFIRMATION_FAILED,$contents["arrErrorMessages"]) ):
		$password_error = "Passwords don't match!";
	endif;
?>
<script type="text/javascript">
	var authorized_sp = <?php if($authorized_sp):?>true<?php else:?>false<?php endif;?>;
</script>
	<div id="content_wrapper" class="registration">		
		<div id="wide_column">
			<div id="page_form" class="formbox">
				<div class="title">
					<h1>Sign Up</h1>
					<p class="register_text">You are one step away from sharing your travels and connecting with the <br><?php echo $siteName;?> Network community.</p>
				</div>
				
				<?php if($authorized_sp): ?>
					<div id="syncMessageBox">
						<ul>
							<li class="buttons standard rounded">
								<!--img src="/images/syncNotification.jpg" alt="SYNC" /-->
								<a href="#TB_inline?dummy=1&width=490&height=400&inlineId=syncUIContent" class="thickbox">
									<!--img src="images/syncNotification.jpg" /-->
								</a>
							</li>
							<li class="content">
								<p>If you already have a <?=isset($GLOBALS["CONFIG"]) ? $siteName." Network" : $siteName;?> account, you may synchronize your profile with your <?php echo ucfirst($authorized_sp);?> account by clicking on the sync button.</p>
							</li>
						</ul>
					</div>
					
					<div id="syncUIContent" style="display:none">
						<div class="modalBody"  id="syncWrapper">
							<a class="modalClose" href="" onclick="jQuery('#txtSyncEmail').val(''); jQuery('#pwdSyncPassword').val(''); jQuery('#syncResult').attr('style','visibility: hidden;'); self.parent.tb_remove(this); return false;">X</a>
							<ul>
								<li class="modalHeader">
									<span id="syncLogoWrapper">SYNC</span>
								</li>
								<li>
									<p>Synchronize your <?php echo ucfirst($authorized_sp);?> account to your profile. Please enter the email and password you are currently using to login to your <?=isset($GLOBALS["CONFIG"]) ? $siteName." Network" : $siteName;?> account.</p>
								</li>
								<li id="syncForm">
									<form id="frmSyncForm">
										<ul>
											<li>
												<label>Email</label>
												<input type="text" id="txtSyncEmail" name="syncfield"/>						
											</li>
											<li>
												<label>Password</label>
												<input type="password" id="pwdSyncPassword" name="syncfield"/>				
											</li>
											<li id="syncStatus" style="visibility: hidden;">
												<img src="/images/loading_small.gif" />  Requesting. Please wait...
											</li>
											<li class="buttons standard rounded" id="syncActions">
												<button type="submit" id="btnSync">Synchronize</button>
												<p id="syncResultMessage" class="errorMessage" style="visibility: hidden;"></p>
											</li>		
										</ul>
									</form>
								</li>
							</ul>
						</div>
					</div>
				<?php elseif(!$contents["fromEmailInvite"]): ?>
					<div id="registerPageSocialMediaConnect">
						<p>
							You may use your account from the following social media networks to sign up for an account. Simply click on any of the icons.
						</p>
						<ul class="socialMediaBar" id="registerPageSocialMedia">
							<li><a class="socialMediaIcon32" id="twitterIcon32" href="/splauncher.php?action=login&context=twitter" title="Register with your Twitter account">twitter</a></li>
							<li><a class="socialMediaIcon32" id="googleIcon32" href="/splauncher.php?action=login&context=google" title="Register with your Google account">google</a></li>
							<li><a class="socialMediaIcon32" id="facebookIcon32" href="/splauncher.php?action=login&context=facebook" title="Register with your Facebook account">facebook</a></li>
						</ul>
					</div>
				<?php endif; ?>
				
        	<?php if( "" != $firstname_error || "" != $lastname_error || "" != $captcha_error || "" != $agreement_error || "" != $email_error || array_key_exists(constants::EMAIL_BLACKLISTED,$contents["arrErrorMessages"])): ?>
	  			<p class="error_notice">
					<?php if( "" != $firstname_error ): ?>
	  					<strong><?php echo $firstname_error;?></strong><br />
	            	<?php endif; ?>
					<?php if( "" != $lastname_error ): ?>
	  					<strong><?php echo $lastname_error;?></strong><br />
	            	<?php endif; ?>
					<?php if( "" != $captcha_error ): ?>
	  					<strong><?php echo $captcha_error;?></strong><br />
	            	<?php endif; ?>
					<?php if( "" != $agreement_error ): ?>
						<strong><?php echo $agreement_error;?></strong>
	          		<?php endif; ?>
					<?php if( array_key_exists(constants::EMAIL_BLACKLISTED,$contents["arrErrorMessages"]) ): ?>
						<strong><?php echo $contents["arrErrorMessages"][constants::EMAIL_BLACKLISTED]; ?></strong>
	          		<?php endif; ?>
				</p>
	        <?php endif; ?>

				
				<form action="register.php<?php echo $redirect;?>" method="post" onsubmit="jQuery('#registerBtn').addClass('disabled'); jQuery('#registerBtn').attr('disabled','true');" class="clear" id="traveler_registration_form">
					<ul class="form">
						<?php $notice_color = "" == $email_error ? "" : "red"; ?>
						<li id="email_field" class="form_set <?php echo $notice_color;?>">
							<div class="line">
								<div class="unit size65 positionRelative">
									<label for="txtEmail" class="inpt_box">Email address: <span class="required">*</span></label>
									<input class="text bigTextBox" name="txtEmail" id="txtEmail" type="text" value="<?php echo $contents["emailAdd"]; ?>" onkeyup="document.getElementById('hdnTempEmail').value=this.value;" onblur="_emailLostFocus();"/>
									<p class="supplement">This email address must be valid and working. We will send a confirmation email to this address for you to complete your registration.</p>
									<span id="email_field_arrow" class="arrow"></span>
								</div>
								<div class="notification lastUnit">
									<img id="checking_email_status"src="images/loading_small.gif" class="loading" style="visibility:hidden;"/>
									<strong id="email_error_text"><?php echo $email_error;?></strong>
								</div>
							</div>
						</li>
						
						<?php if( !$authorized_sp ): ?>
							<?php $notice_color = "" == $password_error ? "" : "red"; ?>
							<li id="password_fields" class="form_set <?php echo $notice_color;?>">
								<div class="line">
									<div class="unit size65 positionRelative">
										<div class="unit size50">
											<label for="pwdPasWrd" class="inpt_box">Password: <?php if(!$authorized_sp):?><span class="required">*</span><?php endif;?></label>
											<input class="text smallTextBox" name="pwdPasWrd" id="pwdPasWrd" onkeyup="document.getElementById('hdnTempPass').value=this.value;" value="" type="password" onblur="_passwordLostFocus();" onfocus="_passwordFieldsGainedFocus();" />
										</div>
										<div class="lastUnit size50">
											<label for="pwdConfirmPasWrd" class="inpt_box">Confirm password: <?php if(!$authorized_sp):?><span class="required">*</span><?php endif;?></label>
											<input class="text smallTextBox" name="pwdConfirmPasWrd" id="pwdConfirmPasWrd" value="" type="password" onfocus="_passwordFieldsGainedFocus();" />
										</div>
										<?php if($authorized_sp ): ?>
											<p class="supplement">Although not required, registering with a password will give you added flexibility in accessing your new <?=isset($GLOBALS["CONFIG"]) ? $siteName." Network" : $siteName;?> account. The password must be at least 6 characters long.</p>
										<?php else: ?>
											<p class="supplement">Password must be at least 6 characters long.</p>
										<?php endif; ?>
										<span id="password_fields_arrow" class="arrow"></span>
									</div>
									<div class="notification lastUnit">
										<img id="checking_password_status"src="images/loading_small.gif" class="loading" style="visibility:hidden;"/>
										<strong id="password_error_text"><?php echo $password_error;?></strong>
									</div>
								</div>
							</li>
						<?php endif; ?>

						<?php $notice_color = "" == $username_error ? "" : "red"; ?>
						<li id="username_field" class="form_set <?php echo $notice_color;?>">
							<div class="line">
								<div class="unit size65 positionRelative">
									<label for="txtUsrName" class="inpt_box">
										Screen Name: <span class="required">*</span>
										
										[ <a href="" onclick="showScreenNameInfo(); return false;">?</a> ]
									</label>
									<input class="text bigTextBox" name="txtUsrName" id="txtUsrName" type="text" value="<?php echo $contents["usrname"]; ?>" maxlength="25" />
									<p class="supplement">
									<strong>http://<?php echo $_SERVER["SERVER_NAME"]?>/<span id="userScreenName"><?php echo $contents["usrname"]; ?></span></strong><br />
									This is the name that will appear on your profile. Please use only letters, numbers or underscore.
									Username must be one word only.
									</p>
									<span id="username_field_arrow" class="arrow"></span>
								</div>
								<div class="notification lastUnit">
									<img id="checking_username_status"src="images/loading_small.gif" class="loading" style="visibility:hidden;"/>
									<strong id="username_error_text"><?php echo $username_error;?></strong>
								</div>
							</div>
						</li>
						
						<li id="name_fields" class="form_set">
							<div class="line">
								<div class="unit size65 positionRelative">
									<div class="unit size50">
										<label for="txtFirstName" class="inpt_box">First Name: <span class="required">*</span></label>
										<input class="text smallTextBox" name="txtFirstName" id="txtFirstName" value="<?php echo stripslashes($contents["firstname"]); ?>" type="text" onfocus="_namefieldsGainedFocus();" />
									</div>
									<div class="lastUnit size50">
										<label for="txtLastName" class="inpt_box">Last Name: <span class="required">*</span></label>
										<input class="text smallTextBox" name="txtLastName" id="txtLastName" value="<?php echo stripslashes($contents["lastname"]); ?>" type="text" onfocus="_namefieldsGainedFocus();" />
									</div>
									<p class="supplement"></p>
									<span id="name_fields_arrow" class="arrow"></span>
								</div>
								<div class="notification lastUnit">
									<img id="checking_name_status"src="images/loading_small.gif" class="loading" style="visibility:hidden;"/>
									<strong id="name_fields_error_text"></strong>
								</div>
							</div>
						</li>

						<?php if( !$authorized_sp ): ?>
							<li id="captcha_field" class="captcha form_set">		
								<div class="line">
									<div class="unit size65 positionRelative">
										<div class="unit size50">
											<label for="securityCode">Security Code : <span class="required">*</span></label>
											<input id="securityCode" name="securityCode" class="text bigTextBox" type="text" />
											<p class="supplement">Please type the code that you see in the image below:</p>
											<img class="captcha_image" id="captchaImg" src="<?php echo htmlentities($contents["security"]["src"]); ?>" alt="Captcha" />
											<p>		    
												<a href="javascript:void(0)" onclick="Captcha.load()" style="color: red; font-weight: normal;"> [Refresh Image] </a> 
												<a href="" title="Why do you need to enter this code?" onclick="showCaptchaInfo(); return false;">
													<img src="http://images.goabroad.com/images/icons/help.gif" alt="help" border="0"> 
													What's this?
												</a>
												<input class="ga_interactive_form_field" id="encSecCode" name="encSecCode" value="<?php echo $contents["security"]["code"]; ?>" type="hidden" />
											</p>
										</div>
										<div class="lastUnit size50">
										</div>														
										<p class="supplement"></p>
										<span id="captcha_field_arrow" class="arrow"></span>
									</div>
									<div class="notification lastUnit">
										<img id="checking_captcha_status"src="images/loading_small.gif" class="loading" style="visibility:hidden;"/>
										<strong id="captcha_field_error_text"></strong>
									</div>
								</div>
							</li>
						<?php endif; ?>
						
						<?php if( !(isset($GLOBALS['CONFIG']) && $GLOBALS['CONFIG']->getGroupID() > 0 )):?>
							<li class="paddingOf10Top confirm form_set">
								<input class="" name="chkSubscribe" id="chkSubscribe" type="checkbox" <?php if($contents["subscribeNL"]): ?>checked<?php endif; ?>/>
								<label for="chkSubscribe" class="checkbox subscribes inline">Subscribe me to the GoAbroad.com Newsletter.</label>
								<p class="supplement">
									We never sell, lease nor rent our list to ANY outside companies.<br />
									Please read our <a href="http://<?php echo $_SERVER['SERVER_NAME']?>/privacy-policy.php" rel="popup">privacy policy</a>.
								</p>
							</li>
						<?php endif; ?>

						<li class="actions form_set">
							<input type="hidden" name="chkAgree" id="chkAgree" value="1" />
							<input type="hidden" id="hdnTempPass" value="" />
							<input type="hidden" id="hdnTempEmail" value="<?php echo $contents["emailAdd"]; ?>" />
							<input type="hidden" id="hdnTempEmail2" value="<?php echo $contents["emailAdd"]; ?>" />
							<input type="hidden" id="hdnTempUsername" value="<?php echo $contents["usrname"]; ?>" />
							<?php if("GoAbroad Network" == $siteName): ?>
								<input type="hidden" id="hdnNetwork" value="<?php echo $siteName;?>" />
							<?php else: ?>
								<input type="hidden" id="hdnNetwork" value="<?php echo $siteName.' Network';?>" />
							<?php endif; ?>
							
							<ul>
								<li class="buttons standard rounded green">
									<button id="registerBtn" type="submit">Register</button>
								</li>
								<li>
									By clicking on Register you are agreeing <br/> to the <a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/terms.php" target="_blank">Terms of Use</a> of the <strong><?=isset($GLOBALS["CONFIG"]) ? $siteName." Network" : $siteName;?></strong>.
								</li>
							</ul>
							
							<input type="hidden" name="btnRegister" value="Register" />

							<input class="ga_interactive_form_field" type="hidden" name="hdnRegKey" value="<?= $contents["regKey"]; ?>" />
							<input class="ga_interactive_form_field" type="hidden" name="hdnRegID" value="<?= $contents["regID"]; ?>" />
							
							<?php if($authorized_sp): ?>
								<input type="hidden" id="hdnAuthorizedSP" name="hdnAuthorizedSP" value="<?php echo $authorized_sp;?>" />
							<?php endif; ?>
							<?php if( isset($contents["fromEmailInvite"]) ): ?>
								<input type="hidden" id="hdnFromEmailInvite" name="hdnFromEmailInvite" value="<?=$contents['fromEmailInvite'] ? 1 : 0;?>" />
							<?php endif; ?>
						</li>

					</ul>
				</form>
			</div>
		</div>
		<div id="narrow_column">
			<div class="helpextra"></div>
			<?php
				$tpl = new Template();
				$tpl->set("siteName",$siteName);
				$tpl->out("travellog/views/tpl.IncRegistrationSidebar.php");
			?>
		</div>				
	</div>