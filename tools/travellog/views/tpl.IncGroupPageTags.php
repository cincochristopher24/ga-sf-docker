<div id="tagCloud" class="content section">
	<h2><span>Explore subgroups</span></h2>
    <div class="content tagcloud">
		<div id='tagCloudStage'></div>
	</div>	
	<script type="text/javascript">
		var cloudConfigData = {
			'groupID' : <?= $groupID ?>,
			'tagContexts' : [],
			'qryString' : '?gID=<?= $groupID ?>',
			'destPath' : '/group-pages.php',
			'mostPopular' : 20,
		};
		(function($){
			$("#tagCloudStage").refreshTagCloud({});
		})(jQuery);
	</script>
</div><!-- end content -->