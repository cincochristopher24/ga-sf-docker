<?php
/*
 * Created on 08 30, 07
 *
 * Template to view the steps to do for those who are new to Goabroad.net
 */
 
 $jscode = <<<BOF
 <script type="text/javascript">
	//<![CDATA[
		function removeSteps(travelerID){
			
			new Ajax.Request('/ajaxpages/unshowSteps.php',{method : 'get',parameters: {travelerid : travelerID}  });
			
			var parent = document.getElementById('top');
			var stepsArea = document.getElementById('steps_wrapper1');
			
			parent.removeChild(stepsArea);
		}
	//]]>	
 </script>
BOF;
Template::includeDependent($jscode); 
 
?>



<div id="steps_wrapper1">
	<div class="box2">
		<p class="title">Thank's for registering on <span style="color:#0596c7;">GoAbroad.net</span></p>
		<p class="info">Now the fun begins! Here is how we suggest you proceed with your new Travel Journal adventures.</p>
	</div>
	<img style="float: left; padding: 70px 0 0 5px;" src="/images/steps_arrowleft.gif" />
	<div class="box" id="step1">
		<p>Complete Your Profile</p>
		<p class="step_detail">Give your Journal Entries a personality and open up opportunities to meet like minded travelers!</p>
	</div>
	<div class="box" id="step2">
		<p>Create Your Address Book</p>
		<p class="step_detail">Create your address book. Adding contact info of your friends and family will keep them updated whenever you post new Journal Entries and photos.</p>
	</div>
	<div class="box" id="step3">
		<p>Create Your Journals</p>
		<p class="step_detail">Create a Journal Title and a short Description, Add a Journal Entry and upload Travel Photos.</p>
	</div>
	<div class="box" id="step4">
		<p>Join and Create Groups</p>
		<p class="step_detail">Join a group, start your own club or share your travel experiences while searching for answers to your questions. Get involved in the network!</p>
	</div>
	<img style="float: left; padding: 70px 0 0 5px;" src="/images/steps_arrowright.gif" />
	<div class="box" id="step5" style="display: none">
		<p></p>
		<p class="step_detail"></p>
	</div>
	<div class="box" id="step6" style="display: none">
		<p></p>
		<p class="step_detail"></p>
	</div>
	<img id="steps_exitBtn" src="/images/exit_button.gif" onclick="removeSteps(<?=$travelerID?>)" />
	<div class="clear"></div>
	
</div>
