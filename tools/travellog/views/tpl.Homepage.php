<?php
/*
 * Created on Aug 8, 2006
 * Created by: Czarisse Daphne P. Dolina
 */
require_once('Class.StringFormattingHelper.php');
require_once('Class.GaDateTime.php');
require_once('travellog/model/Class.Advisor.php');
$d = new GaDateTime();
Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('layoutID', 'page_home');
Template::setMainTemplateVar('page_location', 'Home');

Template::includeDependentCss("/css/cobrand_customhome.css");
Template::includeDependentCss("/css/vsgStandardized.css");
Template::includeDependentCss("/css/modalBox.css");
Template::includeDependentCss("/css/Thickbox.css");

?>

	
	<!-- added by marc, dec 20 -->
	<!--
	<? if( $this['logoutMe'] ) : ?>		
		<iframe src="http://<?= $serve_dir; ?>.jobsabroad.com/createClientVars.cfm" width="0" height="0" frameborder="0" style="display:block"></iframe>
		<iframe src="http://<?= $serve_dir; ?>.languageschoolsguide.com/createClientVars.cfm" width="0" height="0" frameborder="0" style="display:block"></iframe>
		<iframe src="http://<?= $serve_dir; ?>.studyabroaddirectory.com/createClientVars.cfm" width="0" height="0" frameborder="0" style="display:block"></iframe>
		<iframe src="http://<?= $serve_dir; ?>.internabroad.com/createClientVars.cfm" width="0" height="0" frameborder="0" style="display:block"></iframe>
		<iframe src="http://<?= $serve_dir; ?>.teachabroad.com/createClientVars.cfm" width="0" height="0" frameborder="0" style="display:block"></iframe>
		<iframe src="http://<?= $serve_dir; ?>.volunteerabroad.com/createClientVars.cfm" width="0" height="0" frameborder="0" style="display:block"></iframe>
	<? endif; ?>	
-->

<div id="content_wrapper">
	
	<div id="top_wrap" class="area">
				
		
		<? if( $this['logoutMe'] ): ?>
			<div  id="logout_information" class="confirmation">
				<div class="content">
					<p>You have been logged out successfully. See you next time!</p>
				</div>
			</div>
		<? endif; ?>	

		<div id="intro">
			<!-- Start of Rotating Photo -->
			<? if($rotatingPhoto):?>
			<img id="rotating_photo" src="<?=$rotatingPhoto->getPhotoLink()?>" title="GoAbroad.net Photos" alt="GoAbroad.net Photos" height="324" width="980" />
			<div id="extra_image_overlay"></div>
			<div id="top_content">
				<h1><span>Share your journeys with the world!</span></h1>
				<p id="photo_caption"><?=$rotatingPhoto->getCaption()?></p>
				<p id="photo_credit">
					<? if($rotatingPhoto->getFrom()):?>Photo by: <a href="<?=$rotatingPhoto->getLink()?>"><?=$rotatingPhoto->getFrom()?></a><?endif;?>
				</p>
			</div>			
			<? else:?>
			<img id="rotating_photo" src="/images/rotating/isv_2.jpg" title="GoAbroad.net Photos" alt="GoAbroad.net Photos" height="324" width="980" />
			<div id="extra_image_overlay"></div>
			<div id="top_content">
				<h1><span>Share your journeys with the world!</span></h1>
				<p id="photo_caption">Volunteers helping build a school.</p>
				<p id="photo_credit">
					Photo by: <a href="http://www.goabroad.net/group.php?gID=1760">International Student Volunteers</a>
				</p>
			</div>
			<? endif;?>
			<!-- End of Rotating Photo -->
			
			<?php if(!$loggedIn):?>
				<ul>
  				<li class="first">
  					<div>
  						<p>
							<strong>Keep memoirs of your travel</strong>
							while keeping your friends and family posted with Travel Journals and Travel Photos.
						</p>
  					</div>
  				</li>
  				<li>
  					<div>
  						<p>
							<strong>Meet interesting travelers</strong>
							Connect with like-minded travelers before, during and after your travels.
						</p>
					</div>
  				</li>
  				<li>
			  		<div>
  						<p>
  							<strong>Share journals on Facebook!</strong>
							Add our apps on Facebook, MySpace, Orkut and hi5 to bring your travels to your profile!
						</p>
					</div>
  				</li>
  				<li id="signup">
  					<div>
  						<p>Register, <span>it's free</span></p>
  			  			<a href="/register.php" class="sign_text" title="Record your travel experiences now! Join GoAbroad Network. It's FREE."><span>Sign-up now</span></a>
		  			</div>
				</li>
				</ul>
			<?php endif; ?>	
		</div>			
				
	</div>
	
	
				
	<div id="rundown">				
						
				<div id="wide_column">
        			
					<div class="section" id="journals" >
							<? if (count($latestLog)) { ?>
								<h2>
								    <span>
    									<? if (strlen($featEntrySection->getCaption())) : ?>
    										<?=$featEntrySection->getCaption()?>
    									<? else : ?>
    										<? $uname = $latestLog[0]->getTraveler()->getUserName() ?>
    										<?=strlen($uname)-1 == strripos($uname,'s')?$uname."'":$uname."'s"; ?> Journal Entry From
    											<?= $latestLog[0]->getTrip()->getLocation()->getCountry()->getName(); ?>											
    									<? endif;?>						
    								</span>
    								<span class="header_actions">
        								<a href="journal.php" class="more" title="Read more travel experiences from other GoAbroad Network travelers.">More Travel Journals</a>	
        							</span>
								</h2>								
							<? } ?>	
							<div class="content">
								<ul class="journals">
								    
								<? 
								for ($idxlog=0; $idxlog<count($latestLog); $idxlog++) {
									
									$idxlatestLog = 	$latestLog[$idxlog];
									$owner = $idxlatestLog->getOwner();
									$exclude = $idxlatestLog->getTraveler()->isSuspended();
									$exclude = ($owner instanceOf Group) ? $owner->IsSuspended() : $exclude;
									if($exclude) continue;
								?>
									<li class="container">
										<h3>
											<a class="journal_title" href="<?php echo $idxlatestLog->getFriendlyUrl(); ?>" title="Travel blog - <?= $idxlatestLog->getTitle(); ?>">
												<?= $idxlatestLog->getTitle() ?>
											</a>
										</h3>
										
										<?php // Added to give meaningful alt text for journal entry photos(Ismael Casimpan Sept 11, 2007)
											    if (strlen($idxlatestLog->getTitle()) > 21):
												    $_altTextFeaturedJournalEntryPhoto = substr($idxlatestLog->getTitle(),0, 21).'...';
											    else:
												    $_altTextFeaturedJournalEntryPhoto = $idxlatestLog->getTitle();
											    endif;   
										    ?>
										
										    <?php if( $idxlatestLog->checkHasprimaryphoto() &&  strlen(trim($idxlatestLog->getRandomPhoto()->getPhotoLink("default"))) && substr($idxlatestLog->getRandomPhoto()->getPhotoLink("default"), -13) != '164_photo.jpg' ):?>
											<a href="/<?php echo $idxlatestLog->getFriendlyURL(); ?>">
												<img class="jImage" alt="Travel journals and travel notes - <?= $_altTextFeaturedJournalEntryPhoto ?>" title="Travel experiences - <?= $idxlatestLog->getTitle(); ?>" src="<?= $idxlatestLog->getRandomPhoto()->getPhotoLink("featured") ?>"  />
											</a>									
											<?php endif;?>		
											
											<div class="authorInfo">
												<?php if(!is_null($idxlatestLog->getTrip()->getLocation())): ?>
												
														<span>
															<p><? $cID = $idxlatestLog->getTrip()->getLocation()->getCountry()->getCountryID()  ?>
														<?= $d->set($idxlatestLog->getTrip()->getArrival())->friendlyFormat()?>	
														|
														<?
														$cityName    = $idxlatestLog->getTrip()->getLocation()->getName(); 
														$countryName = $idxlatestLog->getTrip()->getLocation()->getCountry()->getName();
														if( strcasecmp($cityName, $countryName) == 0 )
															echo $countryName;
														else
															echo $cityName . ', ' . $countryName;
													?> <img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="Travel journals from <?= $idxlatestLog->getTrip()->getLocation()->getCountry()->getName(); ?>" />
															</p>
														</span>	
														
												<?php endif; ?>
											</div>	
										
										<p><?= StringFormattingHelper::wordTrim($idxlatestLog->getDescription(), 70) . '&#8230;' ?></p>
										<div class="authorInfo">
											<?php
												$_tmpUsernamePossessive = StringFormattingHelper::possessive($idxlatestLog->getTraveler()->getUserName());
													
											?>										
											<? if($owner instanceOf Group) { ?>
												<?php
													// $advisor = new Advisor($idxlatestLog-> getTravelerID());	
													// $advisorgroup = $advisor->getAdvisorGroup();
													$advisorgroup = $owner;									
												?>
												
												<a class="username" href="<?= $advisorgroup->getFriendlyURL()?>">
													<img class="jLeft" alt="<?=$advisorgroup->getName()?>" src="<?= $advisorgroup->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="" width="37" height="37" title="Read <?= $advisorgroup->getName() ?> travel blog and travel notes"/>
												</a>
												<span>
													<p>by: <a class="author_username" href="<?= $advisorgroup->getFriendlyURL()?>" title="<?=$advisorgroup->getName()?>"><?= $advisorgroup->getName() ?></p>
													</p>		

											<? } else { ?>
												<a class="username" href="/<?= $idxlatestLog->getTraveler()->getUserName() ?>">
													<img class="jLeft" alt="<?= $_tmpUsernamePossessive ?> Profile" width="37" height="37" title="Read <?= $_tmpUsernamePossessive ?> travel experiences and travel notes" src="<?= $idxlatestLog->getTraveler()->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')  ?>" />
												</a>
												<span>
													<p>by: <a class="author_username" href="/<?= $idxlatestLog->getTraveler()->getUserName() ?>" title="Read <?= $_tmpUsernamePossessive ?> travel blog and travel notes">	
															<strong><?= $idxlatestLog->getTraveler()->getUserName() ?></strong></a>
													</p>

											<? } ?>
												
													<p><img class="journalIcon" src="/images/journal_icon2.gif" />
														<strong><a class="journal_title" href="/<?php echo $idxlatestLog->getFriendlyURL(); ?>" title="<?= $_tmpUsernamePossessive ?> travel experience: <?= $idxlatestLog->getTravel()->getTitle() ?>" >
												<?= $idxlatestLog->getTravel()->getTitle() ?></a></strong>
													</p>
											</span>		
										</div>
																			</li>
								<? } ?>
								</ul>
							</div>
					</div>
					
					
					
					<div class="section photos" id="featured_photos">		

						<h2><span><?=$featPhotoSection->getCaption()?></span></h2>
						
						<div class="content">
							
									
									<? for ($idxphoto=0; $idxphoto<count($featPhoto); $idxphoto++) { 
										$idxfeatPhoto = $featPhoto[$idxphoto];
										//$idxtravel_photo  =	$arrtravel_photo[$idxphoto];
										$_context = $idxfeatPhoto->getContext();
										
										$photocount = count($_context->getPhotos());
										$TP_Offset = 1;
										
										if($photocount > 0 ){
											foreach($_context->getPhotos() as $off => $tp_photo){
												if($idxfeatPhoto->getPhotoID() == $tp_photo->getPhotoID()){
														$TP_Offset = $off + 1;
													break;
												}
												
											}
											
										}
										
										$type = ''; $owner_ID = 0;
										switch(get_class($_context)) :					
											case 'TravelLog':						
												//$viewphotolink = "../photomanagement.php?cat=travellog&action=vfullsize&genID=" .$_context->getTravelLogID(). "&photoID=".$idxfeatPhoto->getPhotoID();
												$context = "travellog";
												$gen_ID = $_context->getTravelLogID();
												$owner = $_context->getOwner();
												if ($owner){
													$type = get_class($owner) == 'Traveler' ? 'traveler' : 'group';
													$owner_ID = $type == 'traveler' ? $owner->getTravelerID() : $owner->getID();
												}
												break;
											case 'TravelerProfile':						
												//$viewphotolink = "../photomanagement.php?cat=profile&action=view&genID=" .$_context->getTravelerID(). "&photoID=".$idxfeatPhoto->getPhotoID();
												$context = "profile";
												$gen_ID = $_context->getTravelerID();
												$owner_ID = $_context->getTravelerID();
												$type = 'traveler';
												break;
											case 'FunGroup':						
												//$viewphotolink = "../photomanagement.php?cat=fungroup&action=view&genID=" .$_context->getGroupID(). "&photoID=".$idxfeatPhoto->getPhotoID();
												$context = "fungroup";
												$gen_ID = $_context->getGroupID();
												break;
											case 'PhotoAlbum':						
												//$viewphotolink = "../photomanagement.php?cat=photoalbum&action=view&groupID=" .$_context->getGroupID()."&genID=" .$_context->getPhotoAlbumID(). "&photoID=".$idxfeatPhoto->getPhotoID();
												$context = "photoalbum";
												$gen_ID = $_context->getPhotoAlbumID();
												$owner_ID = $_context->getGroupID() ? $_context->getGroupID() : $_context->getCreator();
												$type = $_context->getGroupID() ? 'group' : 'traveler';
												break;
											default:
												//$viewphotolink = '';
												$context = NULL;
												break;	
										endswitch;
									
									?>
									
										<div class="photo">
											
											<? if($context != NULL):?>
												<!--a class="thumbnail" href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup('<?= $idxfeatPhoto->getPhotoID(); ?>','gallery','<?= $gen_ID ?>','<?= $context ?>')"     title="Featured Travel Photo <?php if ($idxfeatPhoto->getCaption()) echo ' - ' . $idxfeatPhoto->getCaption(); ?>">
													<img class="photo_bg" src="<?= $idxfeatPhoto->getPhotoLink('featured') ?>"  alt="Featured Travel Photo - <?php if ($idxfeatPhoto->getCaption()) echo $idxfeatPhoto->getCaption(); ?>" />
												</a-->
												
												<a class="thumbnail" href="javascript:void(0)" onclick="collectionPopup.setParams('<?= $type; ?>','<?= $owner_ID; ?>');collectionPopup.loadGallery('<?= $context ?>','<?= $gen_ID ?>','<?= $idxfeatPhoto->getPhotoID(); ?>');"     title="Featured Travel Photo <?php if ($idxfeatPhoto->getCaption()) echo ' - ' . $idxfeatPhoto->getCaption(); ?>">
													<img class="photo_bg" src="<?= $idxfeatPhoto->getPhotoLink('featured') ?>" alt="Featured Travel Photo - <?php if ($idxfeatPhoto->getCaption()) echo $idxfeatPhoto->getCaption(); ?>" />
												</a>
											<? else: ?>
											    <span class="thumbnail">
											        <img class="photo_bg" src="<?= $idxfeatPhoto->getPhotoLink('featured') ?>" alt="Featured Travel Photo - <?php if ($idxfeatPhoto->getCaption()) echo $idxfeatPhoto->getCaption(); ?>" />
											    </span>
											<? endif; ?>
											<div class="info">
											    <div class="caption">
    												<?	if ($idxfeatPhoto->getCaption())
    													 echo $idxfeatPhoto->getCaption();
    												?>
    											</div>
											
												<?
												switch(get_class($_context)){
													
													case 'TravelLog':
													try{
														$_context->getOwner();
													}catch(exception $e){
														echo $e->getMessage();
														continue;
													}
												?>	
													<? if (get_class($_context->getOwner()) == 'Traveler') : ?>
														traveler: <a class="username" href="/<?= $_context->getTraveler()->getUserName() ?>" title="Travel notes of <?= $_context->getTraveler()->getUserName() ?>" > <?= $_context->getTraveler()->getUserName() ?> </a>														
													<? elseif (get_class($_context->getOwner()) == 'AdminGroup') : ?>
														<a href="<?=$_context->getOwner()->getFriendlyURL()?>" title="Learn more about the <?=$_context->getOwner()->getName()?> travel community"><?=$_context->getOwner()->getName()?></a>
													<? endif;?>
													<br />
													<a class="journal_title" href="journal-entry.php?action=view&amp;travelID=<?= $_context->getTravel()->getTravelID() ?>" title="Travel notes - <?= $_context->getTravel()->getTitle()?>"><?= $_context->getTravel()->getTitle() ?></a>
												<? 		break;
													case 'TravelerProfile':
												?>
													traveler: <a class="username" href="/<?= $_context->getTraveler()->getUserName() ?>" title="Travel notes of <?= $_context->getTraveler()->getUserName() ?>" > <?= $_context->getTraveler()->getUserName() ?> </a>	
												<?		break;
													case 'FunGroup':
												?>
														<a href="<?=$_context->getFriendlyURL()?>" title="Learn more about the <?=$_context->getName()?> travel community"><?=$_context->getName()?></a>
												<?		break;
													case 'PhotoAlbum':
												?>
													advisor:
													<?
														$mGroup		=  GroupFactory::instance()->create( array($_context->getGroupID()) );
    													$relGroup   =  $mGroup[0];
													?>
    												<a href="<?=$relGroup->getFriendlyURL()?>" title="Learn more about the <?=$relGroup->getName()?> travel community"><?=$relGroup->getName()?></a>
												<?		break;														
												}
												
												?>
											
    										</div>
										</div>
										
										<? if (0 == (($idxphoto + 1) % 3)) :?>
											<div class="clear"></div>
										<? endif; ?>
										
									<? } ?>
						</div>
					
						<div class="foot"></div>
						
					</div>
			   		
							
				</div>				
				
				<div id="narrow_column">
				
                    <?php
                    	$featuredTravelersTemplate = new Template();
						$featuredTravelersTemplate->set("featTravelerSection",$featTravelerSection);
						$featuredTravelersTemplate->set("featTraveler",$featTraveler);
						$featuredTravelersTemplate->out("travellog/views/tpl.IncHomepageFeaturedTravelers.php");
                    ?>                    
					
					<?php //if (false): // This is no longer used?>
					<!-- Browse By Destinations -->
					<?//=$obj_map_view->render();?>	
					<?//endif;?>
					<div class="section" style="padding-bottom: 0px;">
						<!--iframe src="http://www.facebook.com/plugins/likebox.php?id=192597394475&amp;height=64&amp;stream=false&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:292px; height:64px;" allowTransparency="true"></iframe-->
						<fb:like-box profile_id="192597394475" width="292" connections="0" stream="false" header="false"></fb:like-box>
					</div>
					<?php
                    	$fbRecommendationsTemplate = new Template();
						$fbRecommendationsTemplate->out("travellog/views/tpl.fbRecommendationsBox.php");
                    ?> 
					
					<div class="section" id="featured_group">
						<h2><span><?=$featGroupSection->getCaption()?></span></h2>
						<div class="content">
						    <ul class="groups">
							<? for ($idxgrp=0; $idxgrp<count($featGroup); $idxgrp++) { 
								$idxfeatGroup = $featGroup[$idxgrp];								
							?>
							    <li>
    								<a href="<?= $idxfeatGroup->getFriendlyURL() ?>" class="group_photo">
										<img alt="Featured Travel Group - <?= $idxfeatGroup->getName() ?>" title="Read more about the <?= $idxfeatGroup->getName() ?> travel community" src="<?= $idxfeatGroup->getGroupPhoto()->getPhotoLink('standard') ?>" />
									</a>
    								<h3>
    								    <a href="<?= $idxfeatGroup->getFriendlyURL() ?>" title="Check out the <?= $idxfeatGroup->getName() ?> travel community" > 
    										<?= $idxfeatGroup->getName() ?> 
    									</a>
    								</h3> 
    								<p>
    									<?= StringFormattingHelper::wordTrim($idxfeatGroup->getDescription(), 40)?><? if (40 < count(explode(' ', strip_tags($idxfeatGroup->getDescription()) ) ) ) : ?><?='&#8230;' ?><? endif ; ?> <br /> <a href="<?= $idxfeatGroup->getFriendlyURL() ?>" title="Read more about the <?= $idxfeatGroup->getName() ?> travel community">Read More...</a>
    								</p>
    							</li>		
							<? } ?>
							</ul>
								<div class="section_foot">
									<a href="/group.php" class="more" title="Search for other travel communities and groups.">More Travel Groups</a>
								</div>
						</div>
					<div class="foot"></div>
					</div>				
						
				</div>
				
			
	</div>


</div>