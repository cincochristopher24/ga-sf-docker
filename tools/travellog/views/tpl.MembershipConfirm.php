<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	
	// show profile - ianne 11/14/02008
	if (isset($subNavigation)):
		echo $profile->render();
		$subNavigation->show();
	endif;	
?>

<div id="intro_container">
	<div id="intro" class="custom">
		<h1>Group Management</h1>
	</div>
</div>

<div id="content_wrapper">
	<div id="wide_column">
		<div id="pending_invites" class="section">
			<h2><span>PENDING INVITATIONS SENT BY GROUP</span></h2>
			<div class="content">			
			<? if (NULL != $grpInvites) { ?>							
							<p class="help_text"><?=$msgGroupInvite ?></p>
							<ul class="users">
								
										<?  if (NULL != $grpInvites){  
											foreach ($grpInvites as $eachgroupinvite) {
												$group_name = str_replace("'","&lsquo;",$eachgroupinvite->getName()); 
										?>
									<li>
										<a title="View Group Profile" class="group_thumb" href="<?=$eachgroupinvite->getFriendlyURL()?>" >	
											<img class="subgroup_img" alt="Group Emblem" src="<?=$eachgroupinvite->getGroupPhoto()->getPhotoLink('standard') ?>" />											
										</a>
									<div class="groups_info">
										<strong><a href="group.php?gID=<?=$eachgroupinvite->getGroupID() ?>" class="groupname"><?=$eachgroupinvite->getName();	?></a></strong>
										<?if ($eachgroupinvite->getParent() != NULL) : ?>
											<?$parent = $eachgroupinvite->getParent()?>																				
											<p>	
												<?=$parent->getName()?>		
											</p>										
										<?endif;?>
										<?if ($eachgroupinvite->isStaffInvitation($travID)):?>
											<p>Staff Invitation</p>
										<?endif;?>			
										<p class="actions">
											<!-- editeed by ianne - 11/14/2008	-->
											<a href="javascript:void(0)" onclick="CustomPopup.initialize('Accept Invitation ?','Are you sure you want to accept <?=$group_name?>&lsquo;s invitation to join the <?=strtolower($eachgroupinvite->getLabel())?>?','membership.php?gID=<?=$eachgroupinvite->getGroupID() ?>&amp;mode=accept&amp;confirm','Accept','0');CustomPopup.createPopup();">Accept</a>
											| <a href="javascript:void(0)" onclick="CustomPopup.initialize('Deny Invitation ?','Are you sure you want to deny <?=$group_name?>&lsquo;s invitation to join the <?=strtolower($eachgroupinvite->getLabel())?>?','membership.php?gID=<?=$eachgroupinvite->getGroupID() ?>&amp;mode=deny&amp;confirm','Deny','0');CustomPopup.createPopup();" >Deny</a>
										</p>
									</div>
									</li>
										<? } } ?>
							</ul>
			<? } 
			else { ?>
				<p class="help_text">
					<span>There are no invitations sent by group</span>.
				</p>
			<? } ?>
			</div>

		</div>
		<div id="pending_requests" class="section">
			<h2><span>PENDING REQUESTS TO JOIN GROUP</span></h2>
			<div class="content">
				<? if (NULL != $grpRequests) { ?>
					<ul class="users">
						<p class="help_text"><? echo $msgGroupRequest ?></p>
							<?  if (NULL != $grpRequests){  ?>
								<? foreach ($grpRequests as $eachgrouprequest) { ?>
									<?$group_name = str_replace("'","&lsquo;",$eachgrouprequest->getName());?>
									<li>
										<a title="View Group Profile" class="group_thumb" href="<?=$eachgrouprequest->getFriendlyURL()?>" >	
											<img class="subgroup_img" alt="Group Emblem" src="<?=$eachgrouprequest->getGroupPhoto()->getPhotoLink('standard') ?>" />											
										</a>

										<div class="groups_info">
									<strong><a class="groupname" href="group.php?gID=<?=$eachgrouprequest->getGroupID()?>" >	
										<?=$eachgrouprequest->getName()?>		
									</a></strong>
									<?if ($eachgrouprequest->getParent() != NULL) : ?>
										<?$parent = $eachgrouprequest->getParent()?>											
										<p>
											<?=$parent->getName()?>		
										</p>
									<?endif;?>
									<p class="actions">
									<a href="javascript:void(0)" onclick="CustomPopup.initialize('Cancel Request ?','Are you sure you want to cancel your request to join the <?=strtolower($eachgrouprequest->getLabel())?> <?=$group_name?>?','membership.php?mode=cancel&amp;gID=<?=$eachgrouprequest->getGroupID()?>&amp;confirm','Yes','0');CustomPopup.createPopup();" >	
										Cancel Request	
									</a>	
									</p>
								</div>
									</li>
								<? } ?>
							<? } ?>
					</ul>
				<? } 
				else { ?>
						<p class="help_text"> <span>There are no requests to join group</span>.</p>
				<? } ?>
			</div>
		</div>		
	</div>
<?/*	<div id="narrow_column">
		<div id="denied_requests" class="section">
			<h2><span>DENIED REQUESTS TO JOIN GROUP</span></h2>
			<div class="content">
				<? if (NULL != $grpDeniedRequests) { ?>
					<p class="help_text"><? echo $msgDeniedGroupRequest ?></p>
				<ul class="users">
					

					<?  if (NULL != $grpDeniedRequests){  
						foreach ($grpDeniedRequests as $eachdeniedrequest) { 
							$group_name = str_replace("'","&lsquo;",$eachdeniedrequest->getName());
					?>
					<li>
						<a title="View Group Profile" class="thumb" href="<?=$eachdeniedrequest->getFriendlyURL()?>" >	
							<img class="subgroup_img" alt="Group Emblem" src="<?=$eachdeniedrequest->getGroupPhoto()->getPhotoLink('standard') ?>" />											
						</a>
						<div class="groups_info">
						<strong><a class="groupname" href="group.php?gID=<?=$eachdeniedrequest->getGroupID()?>" >	
							<?=$eachdeniedrequest->getName()?>		
						</a></strong>
						<?if ($eachdeniedrequest->getParent() != NULL) : ?>
							<?$parent = $eachdeniedrequest->getParent()?>						
							<p>
								<?=$parent->getName()?>		
							</p>
						<?endif;?>
						<p class="actions">
							
							<a href="javascript:void(0)" onclick="CustomPopup.initialize('Cancel Request ?','Are you sure you want to cancel your request to join the <?=strtolower($eachdeniedrequest->getLabel())?> <?=$group_name?>?','membership.php?mode=delete&amp;gID=<?=$eachdeniedrequest->getGroupID()?>&amp;confirm','Yes','0');CustomPopup.createPopup();" >	
								Cancel Request		
							</a>
						</p>
						</div>
					</li>
					<? } } ?>
				</ul>
				<? } 
				else { ?>
					<p class="help_text"> <span>There are no denied requests to join group</span>.</p>
				<? } ?>						
			</div>
		 	<?/*= $backLink->getLink(); ?>
		</div>
	</div>
</div>*/?>
<!--added by ianne - 11/20/2008 success message after confirm-->
<?if(isset($success)):?>
	<script type="text/javascript">
		window.onload = function(){
			CustomPopup.initPrompt("<?=$success?>");
			CustomPopup.createPopup();
		}
	</script>
<?endif;?>