<?php
/**
 * <b>View Program</b> template.
 * @author Aldwin S. Sabornido
 * @version 1.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */

$load = '';

if(strcasecmp($mode,'view')==0 && $has_programs):
	$load = 'manager.refreshDetail();';
elseif(strcasecmp($mode,'add')==0):
	$load = "manager.displayForm('mode=add&gID=".$groupID."');";
elseif(strcasecmp($mode,'edit')==0):
	$load = "manager.displayForm('mode=edit&pID=".$programID."');";
endif;


$code = <<<BOF
<script type = "text/javascript">
//<![CDATA[
	new ajaxUtils();
//]]>
</script>
<script type="text/javascript" src="/js/programManager.js"></script>
<script type = "text/javascript">
//<![CDATA[
	var manager = new ProgramManager();
	window.onload = function(){
		manager.useLoadingMessage();
		$load	
	}
//]]>
</script>
BOF;

	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::includeDependentJs("/js/prototype1.lite.js");
	Template::includeDependentJs("/js/ajaxUtils.js"); 
	Template::includeDependentJs("/js/Validation.js");
	Template::setMainTemplateVar('page_location', 'Groups');
	Template::includeDependent($code); 

	echo $subNavigation->show(); 
?>
<span id="gID" style="display:none"><?= $groupID ?></span>
<div id="programcontent"><?= $content ?></div>


