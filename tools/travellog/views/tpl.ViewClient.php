<?php
	/*
	 * tpl.ViewClient.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
?>
<? if(strlen(trim($contents['client']->getLogo()))): ?>
	 <img src="http://images.goabroad.com/images/clients/logos/<?= $contents['client']->getLogo(); ?>" alt="<?= $contents['client']->getInstName(); ?>"><br>
<? endif; ?>
<? $arrAddress = $contents['client']->getAddress(); 
	  foreach( $arrAddress as $address)	{
	  	if( strlen(trim($address)))
	  		echo $address . "<br>";
	  }
	?>
<? echo $contents['client']->getCity();
	  if( strlen(trim($contents['client']->getState())))
	  	echo ", " . $contents['client']->getState() . "<br>"; 
	  else
	  	echo "<br>"	;
	?>	
<? echo $contents['client']->getCountry();
	  if( strlen(trim($contents['client']->getZip()))) 
	  	echo " " . $contents['client']->getZip() . "<br>"; 
	  else
	  	echo "<br>"	;
	?>
<? if( strlen(trim($contents['client']->getPhone()))): ?>
	Phone : <?= $contents['client']->getPhone() . "<br>"; ?>
   <? endif; ?>	
<? if( strlen(trim($contents['client']->getFax()))): ?>
	Fax : <?= $contents['client']->getFax() . "<br>"; ?>
<? endif; ?>	