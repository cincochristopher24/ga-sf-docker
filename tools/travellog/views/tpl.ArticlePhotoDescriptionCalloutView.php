<?=$sub_view['primary_photo']->render()?>
<?if(count($contents['desc'])):?>
	<div class="right">
		<?if( $contents['enable_ctrls'] ):?>
			<a class="entry_links bottom_shift" href="javascript:void(0)" id="ArticleDescriptionLink" rev="<?=$contents['article']->getArticleID()?>"><span>Edit Article Description</span></a>
		<?endif;?>
		<?=$contents['desc']['col1']?>
	</div>

	<div class="left">
		<?if(strlen(trim($contents['article']->getCallout()))):?>
			<p class="entrycallout"> 
				<span class="leftquote">&nbsp;</span>
				<span id="ENTRYCALLOUT"><?=$contents['article']->getCallout()?></span>
				<span class="rightquote">&nbsp;</span>
			</p>
			<?if( $contents['enable_ctrls'] ):?>
				<a class="entry_links bottom_shift" href="javascript:void(0)" id="ArticleCalloutLink" rev="<?=$contents['article']->getArticleID()?>"><span>Edit Article Callout</span></a>
			<?endif; ?>
		<?endif;?>
		<?=$contents['desc']['col0']?>
	</div>
<?else:?>	
	<?if(strlen(trim($contents['article']->getCallout()))):?>
		<p class="entrycallout"> 
			<span class="leftquote">&nbsp;</span>
			<span id="ENTRYCALLOUT"><?=$contents['article']->getCallout()?></span>
			<span class="rightquote">&nbsp;</span>
		</p>
		<?if( $contents['enable_ctrls'] ):?>
			<a class="entry_links bottom_shift" href="javascript:void(0)" id="ArticleCalloutLink" rev="<?=$contents['article']->getArticleID()?>"><span>Edit Article Callout</span></a>
		<?endif;?>
	<?endif;?>
	<div class="entryholder">
	<?if( $contents['enable_ctrls'] ):?>
		<div class="entrydesc_control">
			<a class="entry_links top_shift" href="javascript:void(0)" id="ArticleDescriptionLink" rev="<?=$contents['article']->getArticleID()?>">
				<span>Edit Article Description</span>
			</a>
		</div>	
	<?endif;?>
	<?
	$desc = $contents['article']->getDescription();
	if(!preg_match('/<p[^>]*>/',$desc))
		//$desc = nl2br(ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<a href=\"\\0\">\\0</a>", $desc));
		$desc = nl2br(preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/","<a href=\"\\0\">\\0</a>", $desc));
	else
		//$desc = ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<a href=\"\\0\">\\0</a>", $desc);
		$desc = preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/","<a href=\"\\0\">\\0</a>", $desc);
	?>
	
	<?=$desc?>
	</div>
<?endif;?>