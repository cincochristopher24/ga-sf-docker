<?php
	require_once("travellog/views/Class.AbstractView.php");
	
	class ViewNewsMessage extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewNewsMessage.php');
		}
		
	}
?>
