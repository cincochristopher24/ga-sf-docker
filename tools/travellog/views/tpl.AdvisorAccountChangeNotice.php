<div class="noticeBox">

	<div id="noticeHeader">
		<form class="jLeft" id="frmNotice" name="frmNotice" method="post" action="travelerdashboard.php">
			<input class="jLeft" style="margin-top:0px;" type="checkbox" id="chkHideNextTime1" name="ADVISOR_ACCOUNT_NOTICE" onclick="document.getElementById('chkHideNextTime2').checked = this.checked;" value="0" />
			<label class="jLeft" for="chkHideNextTime1">Don&rsquo;t show this notice again in the future</label>
		</form>
		<a href="javascript: document.getElementById('frmNotice').submit()" class="jRight link">Proceed to your account &#187;</a>
	</div>

	<p class="clear"><strong>Hi <?=$vars["group"]->getName()?> administrator,</strong></p>

	<ul>
  	<?php if( isset($vars["siteContext"]) ): ?>
	  <li>
	    Thanks for logging in to <?=$vars["siteContext"]->getSiteName()?> Network today! Please take a moment to
	    know about some important changes we&rsquo;ve made.
	  </li>
  	<?php else: ?>
		<li>
		    Thanks for logging in to GoAbroad Network today! Please take a moment to
		    know about some important changes we&rsquo;ve made.
		</li>
	<?php endif; ?>
		<li>
		  We at GoAbroad Network continue to think of ways to make your group more
		  convenient to manage and easier for your members to use.
		</li>

    <li>
      We have added two important new features to Groups. <em>1.</em> 
      Customizable Sub Group Templates make it easy to add new Sub Groups. 
      <em>2.</em> Assigning key-word Tags allow you to easily organize your 
      Sub Groups and give your users an interface to find specific groups. 
      Below are more details about these features.
		</li>
	
		<li>
      <h2>What are Templates?</h2>
		</li>
		
    <li>
      Templates allow for easy duplication of similar Sub Groups. This is 
      especially useful in creating Sub Groups for repetitive programs 
      such as study or volunteer placements that start anew every semester 
      or month. You can create a new template or save a template from an 
      existing Sub Group. Sub Groups created from a Template contain the 
      basic information, security settings, assigned staff, photos, videos 
      and files that you need to jump start your new Sub Group effectively. 
      All that's left to do is invite the new members and help get them 
      started with adding their journals and photos.
    </li>

		<li><img src="/images/traveler_updates/200902_group_template.gif" border="0" align="middle"></li>

		<li>
		  Now that it's so easy to create Sub Groups for all of your specific 
		  travel groups we give you a way to organize and manage all of those 
		  new Sub Groups with <strong><em>Tags...</em></strong>
		</li>

		<li>
			<h2>What are Tags?</h2>
		</li>
		<li>
		  Assigning Tags to your Sub Groups will assist in two ways.
		</li>
		</li>
			<ol style="padding-left:2em;list-style-type:decimal;">
				<li style="margin-bottom:0">Is to organize the SubGroups into meaningful categories and</li> 
				<li style="margin-bottom:0">to create easy navigation for your users to find the groups on the web site.</li>
		  	</ol>
		<li>

    <li>
		Start by creating a Tag simply by typing a keyword in the Tag 
		Manager. Tags should be logical single keywords that apply to your 
		types of programs such as "Study", "Volunteer", "Spain", "Summer". 
		Notice that Tags are single words. Phrases like "Summer Study in 
		Barcelona 2010" will be broken down into 5 separate tags "Summer", 
		"Study", "in","Barcelona" and "2010". The word "in" however is not 
		a useful tag and should be removed. Once tags have been created, 
		you may then assign multiple tags to your Sub Groups or batches of 
		Sub Groups all at once.
    </li>
    
	<li>
		The list of all the tags on your web site is called the 'tag cloud'. 
		A bigger font size indicates that more subgroups are tagged with 
		that word. When a visitor clicks on a tag in the tag cloud the page 
		will display all Sub Groups that are tagged with the word selected. 
		The tag cloud then narrows to show a list of all the secondary 
		tags within the group of selected SubGroups. Clicking further on 
		the tags that remain will further narrow down the search to the Sub 
		Groups that have both tags selected and so on.
	</li>

		<li>To know more about the change, <a href="/faq.php">please visit our illustrated guide &#187;</a></li>

		<li><img src="/images/traveler_updates/200902_group_tags.gif" border="0" align="middle"></li>
	</ul>

	<form class="jLeft" id="frmNotice2" name="frmNotice2" method="post" action="travelerdashboard.php">
		<input class="jLeft" style="margin-top:0px;" type="checkbox" id="chkHideNextTime2" name="ADVISOR_ACCOUNT_NOTICE" value="0" onclick="document.getElementById('chkHideNextTime1').checked = this.checked;"/>
		<label class="jLeft" for="chkHideNextTime2">Don&rsquo;t show this notice again in the future</label>
	</form>
	<a href="javascript: document.getElementById('frmNotice2').submit()" class="jRight button">Proceed to your account &#187;</a>

</div>