<?php
/*
 * Class.ViewStudentCommunications.php
 * Created on Dec 7, 2007
 * created by marc
 */
 
	require_once("travellog/views/Class.AbstractView.php");

	class ViewStudentCommunications extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewPageStudentCommunication.php');
		}
		
	}
?>
