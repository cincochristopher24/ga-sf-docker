<!-- Begin Top Area -->
		<!-- BEGIN PROFILE -->
		<?= $profile ?>		
		<!-- END PROFILE -->
<!-- End Top Area -->

<!-- SUBNAVIGATION -->
<?php $snav->show(); ?>
<?=$createClientVars?>

	
<!-- BEGIN CONTENT WRAPPERS	 -->
<div id="content_wrapper">
	
	<!-- STEPS -->
	<?=$steps?>
	
	<!-- BEGIN WIDE COLUMN -->
	<div id="wide_column">						
				<!-- BEGIN TRAVEL JOURNALS -->
				<div class="section" id="travel_journals">
					<h2><span>All Travel Journals</span></h2> 
					<div class="content">
						<? //$latest_travel_log ?>
				
						<div class="header_actions">
							<a href="/journal.php?action=add">Add a Journal Book</a>
						</div>
						<?=$travelView->render() ?>
				
					</div>
					<div class="foot"></div>
				</div>
				<!-- END TRAVEL JOURNALS -->
		
				<!-- BEGIN MESSAGE CENTER -->
				<div class="section" id="msg_center">
					<?if (isset($messages)):?>
					<?php
					 	ob_start();
						$messages->render(MessagesPanel::$ALL);
						$msg =  ob_get_contents();
						ob_end_clean(); 
					?>
					<?endif;?>
					<h2><span>Message Center</span></h2>
					<ul class="tabs">
						<li class="active"><a href="javascript:void(0)" id="viewAll">New (<span id="msgTotal"><?= isset($messages)?$messages->getTotalNumberOfNewMessages():'0' ?></span>)</a></li>
						<li><a href="javascript:void(0)"  id="viewPersonal">Messages</a></li>
						<li><a href="javascript:void(0)"  id="viewBulletin">Bulletin</a></li>
						<li><a href="javascript:void(0)"  id="viewGreetings">Greetings</a></li>
						<li><a href="javascript:void(0)"  id="viewSurveys">Surveys</a></li>
					</ul>
					<div class="clear"></div>
					<div class="content" >
						<?= $msg ?>
					</div>
					<div class="foot"></div>		
				</div>
				<!-- END MESSAGE CENTER -->						
				
				<?//=$surveys ?>
				<?//=$alerts ?>
				<?//=$inquiries ?>
				
	</div>
	<!-- END WIDE COLUMN -->
				
	
	<!-- NARROW COLUMN -->
	<div id="narrow_column">
		
		
				<!-- BEGIN TRAVEL MAP -->
				<div class="section" id="travelmap">
					<h2>
						<span>
							<span>MyTravelMap</span>
							<!--<img class="facebook" src="/images/facebook_logo.gif" />-->
						</span>
					</h2>
					<div class="content">
						<?php echo $travelMap ?>
					</div>			
					<div class="foot"></div>
				</div>
				<!-- END TRAVEL MAP -->		
				
				<!-- BEGIN TRAVEL PLANS -->
		        <div id="mytravelplans" class="section">
					<h2>
						<span>
							<span>MyTravelPlans</span>
							<a href="http://www.facebook.com/apps/application.php?id=4150427774"><!--<img class="facebook" src="/images/facebook_logo.gif" />--><span class="facebook_link">facebook</span></a>
							<div class="clear"></div>
						</span>
					</h2>
					<div class="content">
						<?php echo $myTravels ?>
					</div>
					<div class="foot"></div>
				</div>
				<!-- END TRAVEL PLANS -->
									
				<?=$friends?>
		
				<?=$other_travelers ?>												
		
				<?=$groups?>
		
				<?=$calendar ?>		
				
	</div>
	<!-- END NARROW COLUMN -->	
		
</div>
<!-- END CONTENT WRAPPER -->
	
	


