	<div class="section" id="newsfeed">
		<a name="my_newsfeed"><h2>Newsfeed</h2></a>
		<div class="content">
			<? if(0 < count($updates)):?>
			<? $feedCtr = 0?>
				<?foreach($updates as $date => $items):?>
					<? $objdate = new GaDateTime(strtotime($date))?>
					<h2><?=$objdate->friendlyFormat()?></h2>
					<ul>
						<?foreach($items as $time => $item):?>
			    			<li><?=$item['actionString'] ?>
			    			<? if(isset($item['detail'])):?>
			    				<div> [<a id = "<?='btnDetail'.$feedCtr?>" href = 'javascript:void(0)' onClick = '$("detail"+<?=$feedCtr?>).toggle()'>Toggle Detail</a>] 
			    				<div id="<?= 'detail'.$feedCtr?>"><?=$item['detail']?></div> </div>
			    			<? endif;?>
			    			</li>
			    			<? ++$feedCtr?>	
						<?endforeach;?> 
					</ul>
				<?endforeach;?>
			<? else:?>
					No updates!
			<? endif;?>
		</div>
	</div>
