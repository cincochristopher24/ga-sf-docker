
 <div class="frm_subsection">
 		<h2 class="frm_header">Work Experience</h2>
				    
				<?php if($workexpcount): ?>
				<table id="work_experience_table" cellspacing="1">
					<tr>
						<th width="18%">Position</th>
						<th width="18%">Employer</th>
						<th>Job Description</th>
						<th width="100">Length of Service</th>
						<th width="80">Actions</th>
					</tr>
				<?php foreach($qworkexp as $value): ?>
				
				   <tr>
				      <td valign="top"><?= $value->getJobPosition() ?></td>
				      <td valign="top"><?= $value->getEmployer() ?></td>
				      <td valign="top"><?= $value->getJobDescription() ?></td>
				      <td valign="top"><?= $value->getLenServ() ?></td>
				      <td align="center" valign="top">
				      	<a href="resume.php?weID=<?= $value->getworkexperienceID() ?>&amp;action=editworkform">Edit</a> | 
				      	<a href="resume.php?weID=<?= $value->getworkexperienceID() ?>&amp;action=delete" onclick="return confirm('Are you sure you want to delete?')">Delete</a></td>
				   </tr>
				   
				<?php endforeach; ?>
				</table>
					
				
				<div id="add_work_experience_form">
					<form name="workloop" method="post" action="resume.php?action=<?= $act ?>">
								<h3>Add work experience?</h3>
								<ul class="form">
									<li class="actions2">
										<label for="txtWorkCount">Type number of work experience(s):</label>
										<input type="text" name="txtWorkCount" id="txtWorkCount" value="1" size="2" class="text big" />
									      			<input type="submit" name="btnSubmit" value="Submit" class="submit" />
									</li>
								</ul>
					</form>
				</div>
					
			<?php endif; ?>
			
			<?php if($workexpcount == 0): ?>
				<div id="add_work_experience_form" class="action_group">
							<form name="workloop" method="post" action="resume.php?action=<?= $act ?>">
								<h3>Add work experience?</h3>
								<ul class="form">
									<li class="actions2">
										<label for="txtWorkCount">Type number of work experience(s):</label>
										<input type="text" name="txtWorkCount" value="1" size="2" class="text big" />
							      		<input type="submit" name="btnSubmit" value="Submit" />
							     </ul>
								
							</form>
				</div>
			<?php endif; ?>
			<div class="clear"></div>
</div>	
			<?php if($resourcefilecount): ?>
				<div class="action_group">
					<p><strong>Do you want to <a href="resourcefiles.php?action=view&amp;cat=resume&amp;genID=<?= $travID ?>">remove</a> or <a href="resourcefiles.php?action=view&amp;cat=resume&amp;genID=<?= $travID ?>">replace</a> your uploaded PDF resume? </strong></p>
				</div>
			<?php else: ?>
				<div class="action_group">
					<p><strong>Do you have a personalized resume? Click <a href="resourcefiles.php?action=add&amp;cat=resume&amp;genID=<?= $travID ?>">here</a> to upload &raquo;</strong></p>
				</div>
			<?php endif; ?>
			
		