<?php
	/*
	 * tpl.ViewChooseRegister.php
	 * Created on Apr 12, 2007
	 * created by marc
	 */
	 
?>

<div id="content_wrapper" class="registration">
	<div id="wide_column">
				<div class="travcol_reg">
					<h2><a href="register.php?traveler" class="head_link">Traveler&#8217;s Registration</a></h2>
					<h3>Why Register as a Traveler?</h3>
					<p>GoAbroad Network lets you create your profile, chronicle your
					 travels, and upload unlimited photos to enhance your profile and chronicles. Become
					  friends with other travelers on the site, post messages for each other, join 
					  interesting forums and form clubs with those who share your interests. You also 
					  have your own space for inquiries, where you can search for programs for your travels
					  abroad, and you can bookmark the programs for future reference in your very own online
					  scrapbook. Plus, you can post your r&eacute;sum&eacute; and we will make it available to our large
					  database of clients who might give you valuable opportunities abroad! To find out more
					  about what you can do on GoAbroad Network, view the <a href="/faq.php">FAQs</a>.</p>
					  <p class="regis_but"><a href="register.php?traveler">Click here to Register!</a></p>
				</div>
				<div class="adviscol_reg">
					<h2><a href="register.php?advisor" class="head_link">Advisor's Registration</a></h2>
					<h3><?= $contents["advisorQuestion"]; ?></h3>
					<?= HtmlHelpers::Textile($contents["advisorAnswer"]); ?>
					<p class="regis_but"><a href="register.php?advisor">Click here to Register!</a></p>
				</div>
	</div>
	
	<div id="narrow_column">
		<div class="help_reg">
				<h2><a href="activation-support.php">Having Trouble Activating<br/> Your Account?</a></h2>
		</div>

	</div>

</div>			

