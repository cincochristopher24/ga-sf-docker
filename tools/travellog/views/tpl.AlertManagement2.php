<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />				
		<title>News Management</title>
		<link rel="stylesheet" type="text/css" href="/travellog/css/adhoc.css" media="screen" />
		<style rel="stylesheet" type="text/css">
			.selEntry{
				background-color:yellow	
			}
			.selTab{
				border-width:3px;
				border-style:solid;	
				background-color:#cdcdcd;
			}
			.unselTab{
				background-color:#cdcdcd
			}
			.widePanel{
				width:600px;
				border-width:3px;
				border-style:solid;
				/*border-color:#cdcdcd;*/
				background-color:#cdcdcd;
				padding:10px;
				overflow: auto; 
				height: 400px
			}
			.errCaption{
				color: red;				
			}
		</style>
		<script type="text/javascript" src="/js/newsManager.js"></script>		
		<script type="text/javascript">
			NewsManager.init({
				groupID : <?= $groupID?>,
				hlEntryID : <?= $hlEntryID?>,
				method : "<?= $method?>",
				itemsPerPage: <?= $itemsPerPage?>,
				isGroupMember : <?= $isGroupMember?>,
				pageNavPerPage : <?= $pageNavPerPage?>,
				mode : '<?= $mode ?>'
				//navPanelID : "navPanel",
				//detailPanelID : "detailPanel"
			});
		</script>
	</head>
	<body>
		<div id="wrapper1">
			<table cellpadding="10px">
				<tr>
					<td valign="top">
						<!--navigation panel-->
						<?php if($mode == "READ_WRITE"): ?>
						<p align="center">
							<a href="javascript:void(0)" onclick="NewsManager.createEntry()">Compose</a>
						</p>
						<?php endif; ?>	
						<div id="navPanel">
							<table style="width:200px">
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table>							
						</div>
						<table style="width:200px">
							<tr>
								<td>
									<div id="pageNavPanel">Page navigation</div>
								</td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<div id="tabPanel">
							<table cellpadding="6px">
								<tr>
									<td align="center" class="" id="detailTab"><strong><a href="javascript:void(0)" onclick="NewsManager.displayDetailPanel()">Details</a></trong></td>
									<?php if($mode == "READ_WRITE"): ?>
									<td align="center" class="" id="composeTab"><strong><a href="javascript:void(0)" onclick="NewsManager.displayComposePanel()">Compose/Edit</a></strong></td>
									<?php endif; ?>									
								</tr>
							</table>
						</div>
						<!-- detail panel -->
						<div id="detailPanel" class="widePanel">													
							<font id="detailTitle" style="font-size:14px">Title</font>&nbsp;&nbsp;&nbsp;&nbsp;(<span id="detailDate">date</span>)
							<?php if($mode == "READ_WRITE"): ?>
							<br /><a href="javascript: void(0)" id="editEntryLink" onclick="NewsManager.editEntry(0)">[Edit]</a>&nbsp;<a href="javascript: void(0)" id="deleteEntryLink" onclick="NewsManager.deleteEntry(0)">[Delete]</a>
							<?php endif; ?>
							<br /><br />
							<strong>Details: </strong><br />
							<div id="detailContent"></div>
						</div>
						<!-- compose panel -->
						<?php if($mode == "READ_WRITE"): ?>
						<div id="composePanel" class="widePanel" style="display:none">
							<h3 id="transMethod">Compose</h3>
							<table cellpadding="3px">
								<tr>
									<td><strong>Title: </strong></td>
									<td><span class="errCaption" id="composeTitleErr"></span><input type="text" id="composeTitle" style="width:400px"></td>
								</tr>
								<tr>
									<td valign="top"><strong>Details: </strong></td>
									<td><span class="errCaption" id="composeDetailsErr"></span><textarea  id="composeDetails" style="width:400px;height:200px"></textarea></td>
								</tr>
								<tr>
									<td valign="top">&nbsp;</td>
									<td><input type="checkbox" id="composeDisplayPublic">Show Public</td>
								</tr>
								<tr>
									<td colspan="2" align="right">
										<input type="button" value="Cancel" onclick="NewsManager.cancelEntry()">&nbsp;<input type="button" value="Save" onclick="NewsManager.saveEntry()">
										<input type="hidden" value="0" id="composeID">
									</td>
								</tr>
							</table>
						</div>
						<?php endif; ?>
						<div id="statusPanel" class="widePanel" style="display:none">
							<table cellpadding="3px" width="100%" height="100%">
								<tr>
									<td width="100%" height="100%" align="center"><h3 id="statusCaption">Hoy</h3></td>									
								</tr>
							</table>																	
						</div>
						<!--<div id="logPanel" class="widePanel"></div>-->
						
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>