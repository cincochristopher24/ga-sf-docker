<div id="content_wrapper" class="layout_2">
	<div id="wide_column">	
		<? if ( isset($all_friend_requests) && count($all_friend_requests)): ?>
			<div id="friend_requests" class="confirmation">
				<?if( count($all_friend_requests) ):?>
					<h2>
						<span>New Friend Requests</span>
						<span id="fr_loader" style="display:none;">
							<img alt="Loading" src="/images/loading_small.gif" width="15" height="15" />
						</span>
					</h2>
				<? endif;?>
				<div class="content">
					<?if( count($all_friend_requests) ):?>
						<!--edited by nick: 10/22/2009-->
						<ul class="users request">
							<? $i = 0;
							   foreach ( $all_friend_requests as $friends_request): ?>
								<li id="fr_<?=$friends_request->getTravelerID()?>" <? if($i === 0):?>class="first"<?endif;?>>																		
										<a title="View Profile" class="thumb" <?php if(!$friends_request->isSuspended()): ?> href="/<?php echo $friends_request->getUserName()?>" <?php endif; ?>>
											<img alt="User Profile Photo" src="<?=$friends_request->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" width="37" height="37" />
										</a>
									<div>
									<strong>
									 <?php if(!$friends_request->isSuspended()): ?> <a class="username" 	 href="/<?php echo $friends_request->getUserName()?>" 	> <?php endif; ?>
										<?= $friends_request->getUserName()?>
									 <?php if(!$friends_request->isSuspended()): ?> </a> <?php endif; ?>
									</strong>
									<?php if($friends_request->isSuspended()): ?>
									<p>The profile you are trying to view is no longer available.</p>
									<?php endif; ?>
									<? if ( !$friends_request->isSuspended() && $IsLogin && $isOwner ): ?>
										<p>
											<a href="javascript:void(0)" onclick="acceptFriend({username:'<?=$friends_request->getUserName()?>',friendID:'<?=$friends_request->getTravelerID()?>'});" class="button"><span>Accept</span></a>
											<a href="javascript:void(0)" onclick="denyFriend({username:'<?=$friends_request->getUserName()?>',friendID:'<?=$friends_request->getTravelerID()?>'});" class="button"><span>Deny</span></a>
										</p>	
									<? endif; ?>
									</div>
								</li>
							<? $i++;
							   endforeach; ?>
						</ul>
					<? endif; ?>
				</div>			
			</div>
		<? endif; ?>
		
		<div id="friends" class="section">
		
		    	<h2><span>
					<? 
						if( (!$isOwner && $count_friends) || (!$isOwner && $count_friends == 0) ){ 
							echo $trav_name."'";
							if (strcasecmp(substr($trav_name,-1),'s') != 0) echo 's';	
						}
						else echo 'My '; 
					?> Friends   
				</span>
				<span id="f_loader" style="display:none;">
					<img alt="Loading" src="/images/loading_small.gif" width="15" height="15" />
				</span>
				</h2>		
				<div id="friends_list" class="content">
					<?php echo $friends_list?>				
				</div>
		</div>
	</div>
	
	<? if($IsLogin): ?>
	<div id="narrow_column">
		<? if ( $IsLogin && $isOwner ): ?>
			<div class="section">
				<div class="content">
					<ul class="actions">
						<li>
							<a href="/travelers.php" title="Add Friends" class="button">+ Add Friends</a>
						</li>
					</ul>
					<div id="PR_help_text">
						<?php if(!$hasPendingRequests): ?>
							<p class="help_text">
								You can be friends with other travelers on the GoAbroad Network and share your travel experiences, tips and photos with them.
							</p>
						<?php endif; ?>		
					</div>
				</div>
			</div>
		<? endif; ?> 
		<!-- transferred pending requests and blocked travelers to separate templates - 10/21/2009 ianne -->
		<?php if(isset($pending_requests)): ?><div id="pending_requests"><?php echo $pending_requests; ?></div><?php endif; ?>
		<?php if(isset($blocked_users)): ?><div id="blocked_users"><?php echo $blocked_users; ?></div><?php endif; ?>
	</div>
	<? endif; ?>		
</div>

<script src="/min/f=js/jquery.manageFriendsTab.js"></script>
<script type="text/javascript">
	(function($){
		$.fn.myScroll = function(){
			var target = this;
			if (target.length) {
			    		var targetOffset = target.offset().top;
		        $('html,body').animate({scrollTop: targetOffset}, 1000);
			}
		}
	})(jQuery);
</script>