<?php
class VideoTemplateFactory{

	static $instance = null;
	
	static function getInstance(){
		if( self::$instance == null ) self::$instance = new VideoTemplateFactory;
		return self::$instance;
	}
	
	function create($url){
		// neri changed $matches[1] to $matches to solve notice undefined offset: 11-24-08
		if( preg_match("/http:\/\/video.google.com\/([0-9a-zA-Z-_]*)(.*)/i", $url, $matches) )
			return array('travellog/views/videos/tpl.google.com.php', $matches);
		elseif( preg_match("/http:\/\/www.youtube.com\/v\/([0-9a-zA-Z-_]*)(.*)&hl=en&fs=1/i", $url, $matches) )
			return array('travellog/views/videos/tpl.youtube.com.php', $matches);
		elseif( preg_match("/http:\/\/youtube.com\/v\/([0-9a-zA-Z-_]*)(.*)&hl=en&fs=1/i", $url, $matches) )
			return array('travellog/views/videos/tpl.youtube.com.php', $matches);
	}
}
?>