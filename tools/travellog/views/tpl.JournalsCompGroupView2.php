<?
require_once 'Class.StringFormattingHelper.php';
$currentGroupID = 0;
if( isset($_GET["gID"]) ){
	$currentGroupID = intval($_GET["gID"]);
}else if( isset($GLOBALS["CONFIG"]) ){
	$currentGroupID = $GLOBALS["CONFIG"]->getGroupID();
}
?>
<div id="journals_summary" class="section" >
	<a name="anchor"></a>
	<h2><span><?=$compTitle?></span></h2>	
	
	<div class="content" >								
			<?$ti = microtime(true);?>
			
			<ul class="journals">
				<?foreach($journals as $journal):?>
				<?php 
					$jID = $journal->getTravelID();
					$jeCount = 0; $jEntriesAr = array();
					if(isset($journalsAndEntries[$jID])){
						$jeCount = count($journalsAndEntries[$jID]);
						$jEntriesAr = $journalsAndEntries[$jID];
					}
					if ($journal->getOwner() == null){
						echo 'travelid:' . $journal->getTravelID();
						continue;
					}
									
					//$jah = new JournalAuthorUIHelper($journal->getOwner());
					$city = ''; $country = ''; $locationID = 0;$link = '';					
					if ($journal->getType() == "JOURNAL" && isset($jEntriesAr[$jeCount-1])){
						$trip = $jEntriesAr[$jeCount-1]->getTrip();
						$country = $trip->getLocation()->getCountry()->getName();
						$city = $trip->getLocation()->getName();
						$locationID = $trip->getLocation()->getCountry()->getCountryID();
						$link = '/journal-entry.php?action=view&amp;travellogID='.$jEntriesAr[$jeCount-1]->getTravellogID();
						$date = $trip->getArrival();
					}else if($journal->getType() == "ARTICLE"){
						$location = LocationFactory::instance()->create($journal->getLocID());
						$country = $location->getCountry()->getName();
						$city = $location->getName();
						$locationID = $location->getCountry()->getCountryID();
						$link = '/article.php?action=view&amp;articleID='.$journal->getTravelID();
						$date = $journal->getDate();
					}					
				?>
					<li class="container" id="container<?=$jID?>">     
						<h3>
							<a class="journal_title" href="<?=$link?>">
								<?if ($journal->getType() == "JOURNAL" && isset($jEntriesAr[$jeCount-1])):?>
									<?=$jEntriesAr[$jeCount-1]->getTitle()?>
								<?elseif($journal->getType() == "ARTICLE"):?>		
									<?=$journal->getTitle()?>
								<?endif;?>
							</a>
						</h3>
						<?if ($journal->getType() == "JOURNAL" && isset($jEntriesAr[$jeCount-1])):?>
							<?if (isset($journalPhotos[$jID])):?>
								<a href="<?=$link?>"> 
									<img class="jImage float_right" src="<?=$journalPhotos[$jID]->getPhotoLink('featured')?>" alt="journal photo" />
								</a>
						 	<?endif;?>
						<?elseif($journal->getType() == "ARTICLE"):?>		
							<?if (isset($journalPhotos['ARTICLE'][$jID])):?>
								<a href="<?=$link?>"> 
									<img class="jImage float_right" src="<?=$journalPhotos['ARTICLE'][$jID]->getPhotoLink('featured')?>" alt="journal photo" />
								</a>
						 	<?endif;?>
						<?endif;?>	
						<div class="authorInfo">
							<?$owner = $journal->getTraveler();?>
							<?//if(!$owner->isAdvisor()):?>
							<?if( !$journal->isGroupJournal() ):?>
								<a class="username" href="/<?=$owner->getUserName()?>">
									<img class="jLeft" width="37" height="37" src="<?=$owner->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$owner->getUserName()?>" />
								</a>
								<span>		
									<p>by: <a class="author_username" href="/<?=$owner->getUserName()?>"><strong><?=$owner->getUserName()?></strong></a>
										<?if($canAddJournal):?>	
											<strong class="meta"><em> <?=$owner->getFullName()?> </em></strong>
										<?endif;?>
									</p>	
									<p>created on: <?= (strtotime($date)) ? date('M d, Y',strtotime($date)).' | ' : '' ?><?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?>
										<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" style="margin: 0 0 0 0;" />	
									</p>
								</span>	
							<?else:?>	
								<? 	//$group = new AdminGroup($owner->getAdvisorGroup());
									$group = new AdminGroup($currentGroupID);
									if( 0 < $group->getParentID() ){
										$group = $group->getParent();
									}
									$site = $group->getServerName();
									$local = "";
									if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
										$local = ".local";
									}
									
									$grouplink = ($site != false) ? 'http://'.$group->getServerName().$local : '/group?gID='.$group->getGroupID(); 
								?>
								<a class="username" href="<?=$grouplink?>">
									<img class="jLeft" width="37" height="37" src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$group->getName()?>" />
								</a>
								<span>		
									<p>by: <a class="author_username" href="<?=$grouplink?>">
										<strong><?=$group->getName()?></strong>
											</a>
									</p>	
									<p>created on: <?= (strtotime($journal->getDate())) ? date('M d, Y',strtotime($journal->getDate())).' | ' : '' ?><?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?>
										<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" style="margin: 0 0 0 0;" />	
									</p>
								</span>	
							<?endif;?>
						</div>
						<p>
							<?if ($journal->getType() == "JOURNAL" && isset($jEntriesAr[$jeCount-1])):?>
								<?= StringFormattingHelper::wordTrim(strip_tags($jEntriesAr[$jeCount-1]->getDescription()), 70, '&#8230;')?>
							<?elseif($journal->getType() == "ARTICLE"):?>
								<?= StringFormattingHelper::wordTrim(strip_tags($journal->getDescription()), 70, '&#8230;')?>
							<?endif;?>
						</p>
						<? if( !$owner->isAdvisor() ): ?>
							<?if (isset($relatedGroupsAr[$jID])):
								if (count($relatedGroupsAr[$jID]) == 1): 
									 $group = $relatedGroupsAr[$jID];
									 $group = array_pop($group);
									 $site = $group->getServerName(); 
										$local = "";
										if( 0 < strpos($_SERVER["SERVER_NAME"],".local") ){
											$local = ".local";
										}
									$grouplink = ($site != false) ? 'http://'.$group->getServerName().$local : '/group?gID='.$group->getGroupID(); 	
								?>	<div class="groupBug">
										<a class="group_advisor" href="<?=$grouplink?>">
											<img class="jLeft" width="37" height="37" src="<?= $group->getGroupPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$group->getName()?>" />
										</a>
										<span>
											<a href=""><strong><?=$group->getName()?></strong></a>
											<p><strong><em>Advisor Group Journal</em></strong></p>
										</span>
									</div>
								<?endif;?>	
							<?endif;?>
						<?endif;?>	
					</li>				
				<?endforeach;?>
			</ul>
				
			<? if (isset($pagingComp)):?>	
		    	<? $pagingComp->showPagination() ?>
			<? endif; ?>
					
			<?if (count($journals) == 0){
				 echo '<p class="help_text">' . $noJournalMessage  . '</p>';						
			}?>
	</div>
	
	<div class="foot"></div>  
	<div class="yui-skin-sam">
	<div id="dlg">
	</div>
</div>

</div>
<div id="dlg_container">
</div>