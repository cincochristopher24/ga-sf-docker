<?php if( count($items) ): ?>
	<div id="random_<?php echo $section;?>_section" class="section">
		
		<h2><?php echo $label;?></h2>
		
		<div id="random_<?php echo $section;?>_content" class="content">		
		
			<div id="random_<?php echo $section;?>_content_body">
				<?php
					$template = new Template();
					$template->set("items",$items);
					echo $template->fetch("travellog/views/group-pages/tpl.GroupPagesRandom{$section}.php");
				?>
			</div>
		</div>			
	</div>
<?php endif; ?>