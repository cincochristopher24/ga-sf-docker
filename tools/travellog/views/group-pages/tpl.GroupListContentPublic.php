<table class="groupsList">
	<tbody id="groupListItems_<?php echo $category;?>">
		<?php
			foreach($groups AS $index => $group):
				$tpl = new Template();
				$tpl->set("loggedUser",$loggedUser);
				$tpl->set("isAdminLogged",$isAdminLogged);
				$tpl->set("isCobrandSearchPage",$isCobrandSearchPage);
				$tpl->set("category",$category);
				$tpl->set("group",$group);
				$tpl->set("index",$index);
				echo $tpl->fetch("travellog/views/group-pages/tpl.GroupListItemPublic.php");
			endforeach;
		?>
	</tbody>
</table>

<?php if( isset($paging) ): ?>
 	<?php if( $paging->getTotalPages() > 1 ): ?>
		<?php $prefix = '<img id="page_loading_'.$category.'" src="/images/loading_small.gif" alt="loading" width="16" height="16" style="display:none;"/>'; ?>
		<?php $paging->showPagination(array("show_details"=>TRUE, "details_prefix"=>$prefix)); ?>
		<input type="hidden" id="<?php echo $category;?>_total_pages" value="<?php echo $paging->getTotalPages();?>" />
	<?php else: ?>
		<input type="hidden" id="<?php echo $category;?>_total_pages" value="1" />
		<img id="page_loading_<?php echo $category;?>" src="/images/loading_small.gif" alt="loading" width="16" height="16" style="display:none;"/>
	<?php endif; ?>
<?php else: ?>
	<input type="hidden" id="<?php echo $category;?>_total_pages" value="1" />
	<img id="page_loading_<?php echo $category;?>" src="/images/loading_small.gif" alt="loading" width="16" height="16" style="display:none;"/>
<?php endif; ?>