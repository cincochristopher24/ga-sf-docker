<div id="tagCloud" class="section tagcloud">
	<?php if( $isAdminLogged && !isset($_GET["search"]) ): ?>
		<h2>
			<span>Related Tags</span>
			<span class="header_actions">
				<a href="faq.php#tags" target="_blank">What are Tags?</a> 
			</span>				
		</h2>
	<?php endif; ?>		
		<div class="section_small_details">
			Can't find what you're looking for? Go back to the list of all tags.
			<a href="<?php echo GroupPagesHelper::getInstance()->getDefaultPageUrl($groupID); ?>" class="strong">Start Over &raquo;</a>
		</div><!-- end listControls -->	
	<?php if(count($relatedTags)): ?>					
		<div class="content">
			<div id='tagCloudStage'></div>
			<script type="text/javascript">
				var cloudConfigData = {
					'groupID' : <?= $groupID ?>,
					'tagContexts' : <?= json_encode($tagContexts) ?>,
					'qryString' : '<?= GroupPagesHelper::getInstance()->getDefaultPageQueryString($groupID) ?>'
				};
				(function($){
					$("#tagCloudStage").refreshTagCloud({});
				})(jQuery);
			</script>
		</div><!-- end content -->			
		<div class="foot">
			<?/*
			<p>
				<?php if( $isAdminLogged ): ?>
					Manage your tags with <a href="/tagmanagement.php?gID=<?php echo $groupID;?>" class="strong">Template Manager</a>
				<?php endif; ?>
			</p>
			*/?>
		</div>
	<?php else:?>	
		<p class="notice">No related tags for this page.</p>	
	<?php endif; ?>
	
</div><!-- end section -->