<?php
	$tag_ids = array();
	$tag_names = array();
	foreach($tags AS $tag){
		$tag_ids[] = $tag->getID();
		$tag_names[] = htmlspecialchars(stripslashes($tag->getName()),ENT_QUOTES);
	}
	$tagIDs = json_encode($tag_ids);
	$tagNames = json_encode($tag_names);
	
	$all_tag_ids = array();
	$all_tag_names = array();
	foreach($allTags AS $tag){
		$all_tag_ids[] = $tag->getID();
		$all_tag_names[] = htmlspecialchars(stripslashes($tag->getName()),ENT_QUOTES);
	}
	$allTagIDs = json_encode($all_tag_ids);
	$allTagNames = json_encode($all_tag_names);
?>
<script type="text/javascript">
	var groupPagesConfig = {
		'gID' : <?php echo $group->getGroupID();?>,
		'actionUrl' : '<?php echo $actionUrl; ?>',
		'tagIDs' : <?php echo $tagIDs;?>,
		'tagNames' : <?php echo $tagNames; ?>,
		'allTagIDs' : <?php echo $allTagIDs;?>,
		'allTagNames' : <?php echo $allTagNames; ?>,
		'keyword': '<?php echo htmlspecialchars(stripslashes($keyword),ENT_QUOTES);?>',
		'rpp' : 10
	}
</script>