<div id="tagCloud" class="section" <?php if(!count($relatedTags)):?>style="display:none"<?php endif;?> >
	<?php if( $isAdminLogged && !isset($_GET["search"]) ): ?>
		<h2>
			<span>Related Tags</span>
			<span class="header_actions">
				<a href="/faq.php#tags" target="_blank">What are Tags?</a> 
			</span>
		</h2>
	<?php endif; ?>
	
 	<div class="section_small_details">
		<p>
			Click on the tags below to search for specific subgroups. Narrow down your search by further clicking on the tags that remain on the list below.
		</p>	
	</div>		
	
	
	<div class="content tagcloud">
		<div id='tagCloudStage'></div>
		<script type="text/javascript">
			var cloudConfigData = {
				'groupID' : <?= $groupID ?>,
				'tagContexts' : <?= json_encode($tagContexts) ?>,
				'qryString' : '<?= GroupPagesHelper::getInstance()->getDefaultPageQueryString($groupID) ?>'
			};
			(function($){
				$("#tagCloudStage").refreshTagCloud({});
			})(jQuery);
		</script>
	</div><!-- end content -->
	<div class="foot">
	</div>
</div><!-- end section -->