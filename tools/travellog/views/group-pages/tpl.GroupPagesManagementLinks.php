<div id="mygroups_action" class="section">
	<h2><span>You might want to...</span></h2>
	<div class="content">
		<br />
		<?php if(isset($GLOBALS['CONFIG'])):?>
			<?php $formAction = '/tagmanagement.php'?>
		<?php else: ?>
			<?php $formAction = '/tagmanagement.php?gID='.$group->getGroupID()?>
		<?php endif; ?>
		<ul class="actions">
			<li><a class="button" href="/group.php?mode=add&pID=<?php echo $group->getGroupID();?>">Create a New SubGroup</a>	</li>
			<li><a class="button" href='<?php echo $formAction ?>'>Manage Your Tags</a></li>
			<li><a class="button" href='/group_template.php?action=view_template_manager&group_id=<?= $group->getGroupID() ?>'>View Template Manager</a></li>
        </ul>										
	</div>
</div>