<?php
	if(isset($groupTagData)){
		$tagLabels = json_encode($groupTagData);
		Template::includeDependent("<script>var GroupTagLabels = $tagLabels;</script>");
	}
?>
<?php echo $profile->render(); ?>
<?php echo $subNavigation->show(); ?>
<div class="layout_2" id="content_wrapper">
	<div id="wide_column">	
		<?php if( isset($breadcrumbs) ): ?>
			<?php echo $breadcrumbs; ?>
		<?php endif; ?>

		<?php if( isset($searchResultsHeader) ): ?>
			<?php echo $searchResultsHeader; ?>
		<?php endif; ?>
		
		<?php if( isset($wideSubgroupsInfo) ): ?>
			<?php echo $wideSubgroupsInfo; ?>
		<?php endif; ?>

		<?php if(isset($wideTagCloud)): ?>
			<?= $wideTagCloud ?>
		<?php endif;?>
		<?// render wide column components ?>
		<?php if( isset($defaultGroups) ): ?>
			<?php echo $defaultGroups; ?>
		<?php endif; ?>
		<?// for the tag management?>
		<?php if( isset($tagManagement) ): ?>
			<?php echo $tagManagement; ?>
		<?php endif; ?>
		<?php if( isset($randomSections) ): ?>
			<?php foreach($randomSections AS $section): ?>
				<?php echo $section;?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	
	<div id="narrow_column">
		<?//render narrow-column components ?>
		<?php if(isset($narrowTagCloud)): ?>
			<?= $narrowTagCloud ?>
		<?php endif;?>
		<?php if( isset($narrowSubgroupsInfo) ): ?>
			<?php echo $narrowSubgroupsInfo; ?>
		<?php endif; ?>		
		<?php if( isset($searchForm) ): ?>
			<?php echo $searchForm; ?>
		<?php endif; ?>
		<?php if( isset($pageManagementLinks) ):  ?>
			<?php echo $pageManagementLinks; ?>
		<?php endif; ?>
	</div>
</div>

<?php if( isset($configScript) ): ?>
	<?php echo $configScript; ?>
<?php endif; ?>