<div id="<?php echo $category?>_controls" class="listControls" style="<?php echo $showCategoryStyle?>">
	<div class="left">					 
		<button id="lnkBatchSelectAll_<?php echo $category?>" class="alpha omega">
			<span>Select All</span>
		</button>
		<button id="lnkBatchSelectNone_<?php echo $category?>" class="omega">
			<span>Select None</span>
		</button>						
	</div>
	<div class="right">
		<img id="batch_loading_<?php echo $category;?>" src="/images/loading_small.gif" alt="loading" width="16" height="16" style="display:none; vertical-align: middle; margin: 0 5px;"/>
		<?php if( "TAGGED" == $category || "SEARCH" == $category ): ?>
			<button class="alpha " onclick="jQuery.fn.batchFeature('<?php echo $category;?>')">
				<span>Feature</span>
			</button>				 
			<button class="alpha" onclick="jQuery.fn.batchUnfeature('<?php echo $category;?>')">
				<span>Unfeature</span>
			</button>
		<?php else: ?>
			<?php if( "FEATURED" == $category ): ?>
				<button class="alpha" onclick="jQuery.fn.batchUnfeature('<?php echo $category;?>'); return false;">
					<span>Unfeature</span>
				</button>
			<?php else: ?>
				<button class="omega" onclick="jQuery.fn.batchFeature('<?php echo $category;?>'); return false;">
					<span>Feature</span>
				</button>
			<?php endif; ?>
		<?php endif; ?>
		<?php if( "TAGGED" != $category ): ?>					
			<button class="alpha omega" name="batch_assign_tags" id="<?=$category?>">
				<span>Assign tags</span>
			</button>
		<?php else: ?>
			<button class="alpha omega" name="batch_assign_tags" id="<?=$category?>">
				<span>Assign tags</span>
			</button>
		<?php endif; ?>
		<?php if( "TAGGED" == $category ): ?>
			<button class="omega" onclick="jQuery.fn.batchRemoveFromPage(); return false;">
				<span>Remove from Page</span>
			</button>
		<?php endif; ?>		
	</div>
</div><!-- end listControls -->

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#lnkBatchSelectAll_FEATURED").bind("click",function(){
			jQuery("input[name='chkGroup_FEATURED']").attr("checked",true);
			jQuery("tr[name='FEATURED_item']").addClass("selected");
		});
		jQuery("#lnkBatchSelectNone_FEATURED").bind("click",function(){
			jQuery("input[name='chkGroup_FEATURED']").attr("checked",false);
				jQuery("tr[name='FEATURED_item']").removeClass("selected");
		});
		jQuery("#lnkBatchSelectAll_RECENT").bind("click",function(){
			jQuery("input[name='chkGroup_RECENT']").attr("checked",true);
			jQuery("tr[name='RECENT_item']").addClass("selected");
		});
		jQuery("#lnkBatchSelectNone_RECENT").bind("click",function(){
			jQuery("input[name='chkGroup_RECENT']").attr("checked",false);
			jQuery("tr[name='RECENT_item']").removeClass("selected");
		});
		jQuery("#lnkBatchSelectAll_TAGGED").bind("click",function(){
			jQuery("input[name='chkGroup_TAGGED']").attr("checked",true);
			jQuery("tr[name='TAGGED_item']").addClass("selected");
		});
		jQuery("#lnkBatchSelectNone_TAGGED").bind("click",function(){
			jQuery("input[name='chkGroup_TAGGED']").attr("checked",false);
			jQuery("tr[name='TAGGED_item']").removeClass("selected");
		});
		jQuery("#lnkBatchSelectAll_SEARCH").bind("click",function(){
			jQuery("input[name='chkGroup_SEARCH']").attr("checked",true);
			jQuery("tr[name='SEARCH_item']").addClass("selected");
		});
		jQuery("#lnkBatchSelectNone_SEARCH").bind("click",function(){
			jQuery("input[name='chkGroup_SEARCH']").attr("checked",false);
			jQuery("tr[name='SEARCH_item']").removeClass("selected");
		});
	});
</script>