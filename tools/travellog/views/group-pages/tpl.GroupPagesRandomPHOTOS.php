<div id="group_photos">
	<div class="content">												
		<?php foreach($items AS $item): ?>
			<?php
				$tpl = new Template();
				$tpl->set("photo",$item);
				$tpl->set("context",$item->getContext());
				echo $tpl->fetch("travellog/views/group-pages/tpl.GroupPagesRandomPHOTOItem.php");
			?>
		<?php endforeach; ?>        						
	</div><!-- end content -->
</div><!-- end group_photos -->