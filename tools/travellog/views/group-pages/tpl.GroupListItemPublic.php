<?php
	$members = $group->getMembers($rowslimit = NULL, $filtercriteria = NULL, $order = NULL, $count = TRUE);				
	
	$memberLabel = 1 > $members ? "" : 1 < $members ? "$members Members" : "$members Member";
	
	require_once("travellog/dao/Class.GroupApprovedJournals.php");
	$relatedEntries = GroupApprovedJournals::countGroupRelatedEntries($group->getGroupID());
	$relatedEntriesLabel = 1 > $relatedEntries ? "" : 1 < $relatedEntries ? "$relatedEntries Journal Entries" : "$relatedEntries Journal Entry";
	
	$counterLabel = "No Members / No Journal Entries";
	if( $members > 0 && $relatedEntries > 0 ){
		$counterLabel = $memberLabel." / ".$relatedEntriesLabel;
	}else if( $members < 1 && $relatedEntries > 0 ){
		$counterLabel = $relatedEntriesLabel;
	}else if( $members > 0 && $relatedEntries < 1 ){
		$counterLabel = $memberLabel;
	}
?>

<?php if( 0 == $index%2 ): ?>
	<tr>
<?php endif; ?>
		<td class="detailsCol publicCol">
			<a title="View Group Profile" class="thumb" href="<?php echo $group->getFriendlyURL();?>">
				<img class="pic" width="65px" height="65px" alt="Group Emblem" src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail') ?>"  />
			</a>
			<div class="details">
				<p class="text">
					<a class="groupName" href="<?php echo $group->getFriendlyURL();?>" title="<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?>">
						<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES); ?>
					</a>
					<input type="hidden" id="groupName_<?php echo $group->getGroupID();?>" value="<?php echo htmlspecialchars(stripslashes($group->getName()),ENT_QUOTES);?>" />
					<span class="groupMeta">
						<?php echo $counterLabel; ?>
					</span>								
				</p>																						
			</div> <!-- end details -->
		</td>
<?php if( 1 == $index%2 ): ?>
	</tr>
<?php endif; ?>