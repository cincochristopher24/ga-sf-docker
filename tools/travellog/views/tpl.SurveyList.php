<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');	
?>

<div class="area" id="intro">
	<div class="section" align="right">
		<h2><a href="onlineform.php?formID=<?=$form->getOnlineFormID()?>">Survey Form</a>
			| <a href="onlinesurvey.php?formID=<?=$form->getOnlineFormID()?>">Survey Results</a>
		</h2>
	</div>	
</div>

<div class="area" id="main">
	<div class="section">
		<h2><?=$form->getTitle()?></h2>
		
		<div class="content">
			<? if (count($arrParticipants)) { ?>
				<h2>Participants:</h2>
				
				<? for($i=0; $i<count($arrParticipants);$i++) { ?>
					<? $eachtraveler = $arrParticipants[$i]; 
						$photolink = $eachtraveler->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink();
						if (0 != strncmp("http://", $photolink , "7"))
							$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
					?>		 
					<div id="group_administrator" class="users">
						
						<a href="/<?=$eachtraveler->getUsername()?>" class="user_thumb" >
							<img src="<?=$photolink?>" alt="<?=$eachtraveler->getUsername()?>"  width="37" height="37" alt="User Profile Pic" title="View Profile" />						
						</a>							 
						<a href="/<?=$eachtraveler->getUserName() ?>" class="username">
							 <?=$eachtraveler->getUserName() ?>
						</a>
						<div style="clear: left;"></div>
					</div>							
				<? } ?>	
			<? } ?>			
			
			<? if (count($arrNonParticipants)) { ?>
				<h2><?=$grpName?> Members who have not participated in survey:</h2>
				
				<? for($i=0; $i<count($arrNonParticipants);$i++) { ?>
					<? $eachtraveler = $arrNonParticipants[$i]; 
						$photolink = $eachtraveler->getTravelerProfile()->getPrimaryPhoto()->getThumbnailPhotoLink();
						if (0 != strncmp("http://", $photolink , "7"))
							$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
					?>		 
					<div id="group_administrator" class="users">
						
						<a href="profile.php?action=view&amp;travelerID=<?= $eachtraveler->getTravelerID() ?>" class="user_thumb" >
							<img src="<?=$photolink?>" alt="<?=$eachtraveler->getUsername()?>"  width="37" height="37" alt="User Profile Pic" title="View Profile" />						
						</a>							 
						<a href="profile.php?action=view&amp;travelerID=<?= $eachtraveler->getTravelerID() ?>" class="username">
							 <?=$eachtraveler->getUserName() ?>
						</a>
						<div style="clear: left;"></div>
					</div>							
				<? } ?>	
			<? } ?>
	
			<div class="clear"></div>
		</div>
	</div>
</div>