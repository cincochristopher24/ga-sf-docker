<?php
/*
 * Created on 09 25, 2006
 * @author Kerwin Gordo
 * Purpose: template for adding a comment
 */
?>

<?php 
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	$snav->show();
?>
<h2>Add Comment</h2>
<form action="comment.php?action=savenew" method="post">
		<textarea name="comment" cols="40" rows="6"></textarea><br />		
		<input type="hidden" name="context" value="<?=$context?>" />
		<input type="hidden" name="contextID" value="<?=$contextID?>" />
		<input type="hidden" name="referer" value="<?=$referer?>" />
		<input type="SUBMIT" value="Save" />	
</form>

