<?php
/*
 * Created on 09 30, 08
 * 
 * file: Class.JournalsComp2.php
 * Purpose: New version of Class.JournalsComp.php
 * 
 */
 
 	require_once('travellog/model/Class.Traveler.php');
	require_once('travellog/model/Class.Travel.php');
	require_once('travellog/dao/Class.GroupApprovedJournals.php');
	require_once('Class.Template.php');
	require_once('Class.JournalsCompFUrlPaging.php');
	require_once('Class.JournalsCompPaging.php');
	require_once('travellog/vo/Class.JournalsArticlesVO.php');
	require_once('Cache/ganetCacheProvider.php');
	
	/**
	* EDIT:
	* 	- Dec. 4, 2008 - chris
	*		- no longer check if entry is published or not
	* 	- Dec. 11, 2008 - chris
	*		- reverted changes made in checking for the published field
	*	- Jan. 20, 2009 - neri
	*		- changed "Unfeature" text to "Remove from Featured List" in configureJournalControls()
	**/
	
	class JournalsComp {
		/****
		 * Context vars in $setupAr variable passed in constructor
		 * 		VIEWMODE			=>	GROUP(journals for group) -> other values -> (GROUP.VIEW, GROUP.MANAGE) , PROFILE(journals for Traveler) , ALL (all journals)
		 * 		SECURITY_CONTEXT 	=> 	PUBLIC (public view) , LOGIN (a viewing user has logged in) 
		 * 		VIEW_TAB			=>  the selected tab , NEW(Untagged Journals of group members), FEATURED,  STAFF_ADMIN, UNAPPROVED, APPROVED, STAFF_ADMIN_APPROVED (admin journals plus approved journals of members)
		 * 		CURRENT_PAGE		=>	the current page to display. This is numeric.
		 * 		ROWS_PER_PAGE		=>  the no. of rows to display per page.  
		 * 		RENDERING			=>  page rendering (NORMAL,AJAX)
		 * 		SITE				=>	site where journals are displayed (GOABROAD,COBRAND)
		 */
		
		protected $viewMode = 'PROFILE';				// default, other values ALL, PROFILE.ALL, GROUP, GROUP.VIEW  , GROUP.MANAGE 		
		protected $securityContext = 'PUBLIC';     		// default
		protected $site	= 'GOABROAD';					// default
		protected $viewTab = '';						// default, other values are FEATURED, STAFF_ADMIN, APPROVED, UNAPPROVED, NEW
		protected $loggedUserId = 0;
		protected $subjectID = 0;
								
		protected $journals = array();
		protected $journalsAndEntries = array();		// a travelID => journal_entries(object) array
		protected $journalPhotos = array();				// a travelID => photo url array	
		protected $subject = null;
		protected $tabsScript = '';						// contains the javascript code to control display of tab
		protected $groupMembers = array();				// contains an array of traveler ids of the members if subject is a group
		protected $journalControlsJsCode = array();		// an assoc array travelID => string of javascript code to manipulate the ui controls of each journal
		protected $approvedJournalsAr = array();		// an assoc array travelID => travelID of travelIDs of approved journals
		protected $featuredJournalsAr = array();		// an assoc array travelID => travelID of travelIDs of featured journals
		protected $relatedGroupsAr = array();			// an assoc array travelID => group  object of the journal related group			
		protected $htmlTemplate;						// the filename of html template to use during rendering
		
		protected $mJournalDraftsCount = 0;
		
		// context vars 
		protected $isAdmin = false;						// logged user is administrator of the group in case this is in group context
		protected $isStaff = false;						// logged user is staff of the group in case this is in group context

		// other filter vars (to limit the journals displayed to a particular criteria in addition to the basic criteria)
		protected $locationID = 0;						// used if we search journals in a particular location
		protected $travelerID = 0;						// search journals by author; this has bearing if the subject is an admin group
		
		// caching vars
		protected $enableCaching =	0;
		protected $cacheKeySource =	'URL';				// source of the key for cache to use, other values-> 'PROFILE_CONTEXT_VARS'
		protected $cacheExpire	  = 3600;				// expiration of cache item in seconds	
		
		// other vars
		protected $rpp = 5; 							// default rows per page
		//protected $sr = 1;								// default start row
		protected $cp = 1;								// default current page  
		protected $jEntryInViewCount = 5;  				// no. of journal entries visible per journal (in carousel display)
		protected $pagingComp;
		protected $scriptName = '';
		protected $criAr = array();						// assoc array for additional criteria used in retrieving journals. Typically used to filter by country
														// 'LOCATIONID' => locationID 
		protected $origParams = '';						// the original url params of used to access the containing page of this comp.This does not include the params used by this component
		protected $username = '';						// username of traveler to be searched																						
														 
		protected $isAjax = false;						// rendering of the page if traditional or ajax
		protected $addJournalBookLink = '/journal.php?action=add';
		protected $canAddJournal = false;
		protected $hideButton = false;
		protected $showAuthor = true;					// show author name and image in journal
		protected $compTitle  = 'Travel Journals';
		protected $cache	=	null;	
		protected $journalsAndUpdates = array();
		protected $cobrandProfile = null;
		protected $journalsAndArticles = false;
		protected $subFeatured = false;

		/**
		 * @param array setupAr assoc array to setup the context
		 * @param numeric subjectID id of the owner of the journals to be displayed
		 * @param numeric loggedUserId user id of the viewing user
		 */		
		function JournalsComp($setupAr = null,$subjectID,$loggedUserId = 0) {
			
			if ($setupAr != null){
				if (isset($setupAr['VIEWMODE']))
					$this->viewMode 		= $setupAr['VIEWMODE'];
										
				if (isset($setupAr['SECURITY_CONTEXT']))	
					$this->securityContext 	= $setupAr['SECURITY_CONTEXT'];				
					
				if (isset($setupAr['VIEW_TAB']))	
					$this->viewTab 			= $setupAr['VIEW_TAB'];
									
				if (isset($setupAr['CURRENT_PAGE']))	
					$this->cp 				= $setupAr['CURRENT_PAGE'];					
				if (isset($setupAr['START_ROW']))	
					$this->sr 				= $setupAr['START_ROW'];
				if (isset($setupAr['ROWS_PER_PAGE']) )	
					$this->rpp 				= $setupAr['ROWS_PER_PAGE'];
				if (isset($setupAr['RENDERING'])){	
					if ($setupAr['RENDERING'] == 'AJAX')
						$this->isAjax = true;
				}
				if (isset($setupAr['SITE'])){
					$this->site 			= $setupAr['SITE'];
				}
				if (isset($setupAr['COBRAND_PROFILE'])){
					$this->cobrandProfile 			= $setupAr['COBRAND_PROFILE'];
				}
				if(isset($setupAr['JOURNALS_ARTICLES'])){
					$this->journalsAndArticles = true;
					$this->compTitle = "Travel Journals And Articles";
				}
				// we set the default viewtab to FEATURED if view mode is GROUP and VIEW_TAB is not explicitly set
				if (!isset($setupAr['VIEW_TAB']) && $this->viewMode == 'GROUP' &&  $this->site == 'GOABROAD' ){
					$this->viewTab = 'FEATURED';					
				} 
				
				if ($this->viewTab == 'FEATURED') 
					$this->setCompTitle('Featured Journals');
				if(isset($setupAr['SUB_FEATURED'])){
					$this->subFeatured = $setupAr['SUB_FEATURED'];
				}	
												
			} 
			
			
			$this->subjectID = $subjectID;
			$this->loggedUserId = $loggedUserId;
			
			$ar = explode('.',$this->viewMode);
			$secAr = explode('.',$this->securityContext);
			
			if (in_array('PROFILE',$ar)){			
				$this->subject = new Traveler($this->subjectID);	
				$this->subjectID = $this->subject->getTravelerID();		
				if ($loggedUserId == $this->subjectID)
					$this->canAddJournal = true;
					$this->hideButton = true;		
				
				$this->showAuthor = false;												
			}
			
			
			
			if (in_array('GROUP',$ar)){				
				$this->subject = new AdminGroup($this->subjectID);	
				$this->subjectID = $this->subject->getGroupID();
				if ($loggedUserId == $this->subject->getAdministrator()->getTravelerID()){
					$this->isAdmin = true;
				}
				
				if ($this->subject->isStaff($loggedUserId)){
					$this->isStaff = true;
				}		
				
				if ($this->isAdmin || $this->isStaff){
					$this->canAddJournal = true;
					$this->hideButton = true;	
					if ($this->viewTab == '')
						$this->viewTab = 'NEW';				
				}
				
				// modify add journal link
				$this->addJournalBookLink .= '&contextGroupID=' . $this->subjectID; 																																	
			}
			
			// viewTabs Staff/Admin and Unapproved are only for staff/administrators so we will enforce it here
			if ($this->viewTab == 'STAFF_ADMIN' || $this->viewTab == 'UNAPPROVED' ){
				if (!$this->isAdmin && !$this->isStaff){
					// we default to 'ALL'
					$this->viewTab = '';
				}
			}
			
			$this->scriptName = $_SERVER['SCRIPT_NAME'];
			
			$this->generateOrigUrlParam();
			// $this->decideToUseCaching();				// function to decide whether to use caching or not 		
			$this->selectHtmlTemplate();				// 						
		}
		
		
		// url param is used to instantiate this component in the appropriate context
		static function convertURLParamToSetupArray(){
			// we expect the parameter to be base64 encoded and held by jcp url param (this url param is created in "configurePaging" method)
			$setupAr = array();
			
			if (isset($_GET['jcp'])){
				$paramStr = base64_decode($_GET['jcp']);
				
				$paramTokens = explode('&',$paramStr);				// divide param string by & delimeter
				foreach($paramTokens as $token){
					$ppAr = explode('=',$token);					// divide token by = delimeter to get variable name -> value pair
					if (isset($ppAr[1]) && $ppAr[1] != ''){
						$setupAr[$ppAr[0]] = $ppAr[1];
					}
				}
			}
			
			
			// we also need to get the page param and include it in setupAr
			if (isset($_GET['page'])){
				$setupAr['CURRENT_PAGE'] =	$_GET['page']; 
			}
			
			return $setupAr;
		}
		
		protected function decideToUseCaching(){
			$ar = explode('.',$this->viewMode);
			$this->cache	=	ganetCacheProvider::instance()->getCache();
			
			if ($ar[0] == 'ALL'){
				if ($this->cache != null) {	
					$this->enableCaching = 1;
				}
			}
			
			// cache showing of group in public 
			if ($ar[0] == 'GROUP' && count($ar) == 1 ) {
				
				if ($this->cache != null) {	
					$this->enableCaching = 1;
					$this->cacheKeySource =	'GROUP_CONTEXT_VARS';
				}
			}
			
			// cache showing of journals in traveler profile as long as the logged user  
			//		is not the owner of the journals
			if ($ar[0] == 'PROFILE' && $this->loggedUserId != $this->subjectID  ) {
				if ($this->cache != null) {	
					
					if (!in_array('ALL',$ar)){
						$this->cacheKeySource =	'PROFILE_CONTEXT_VARS';
					}
					
					$this->enableCaching = 1;
				}
			}
			
		}
		
		protected function selectHtmlTemplate(){
			$ar = explode('.',$this->viewMode);
			
			if ($ar[0] == 'ALL'){
				if($this->journalsAndArticles)
					$this->htmlTemplate = 'tpl.JournalsCompAll2.php';
				else	
					$this->htmlTemplate = 'tpl.JournalsCompAll.php';
			}
			
			if ($ar[0] == 'PROFILE') {
				if (in_array('ALL',$ar))
					$this->htmlTemplate = 'tpl.JournalsCompTravelerView2.php';
				else
					$this->htmlTemplate = 'tpl.JournalsComp.php';
			}  
			
			if ($ar[0] == 'GROUP') {
				if (in_array('VIEW',$ar)){
					$this->htmlTemplate = 'tpl.JournalsCompGroupView.php';
				}
				elseif (in_array('MANAGE',$ar)){
					if (!$this->isAjax){
						if($this->journalsAndArticles)
							$this->htmlTemplate = 'tpl.JournalsCompGroupManage2.php';
						else
							$this->htmlTemplate = 'tpl.JournalsCompGroupManage.php';
					}else{
						if($this->journalsAndArticles)
							$this->htmlTemplate = 'tpl.JournalsCompGroupManageAjax2.php';	
						else
							$this->htmlTemplate = 'tpl.JournalsCompGroupManageAjax.php';	
					}	
				} else {
					if($this->journalsAndArticles)
						$this->htmlTemplate = 'tpl.JournalsCompGroupView2.php';
					else
						$this->htmlTemplate = 'tpl.JournalsCompGroupView.php';
				}
			}
		}
		
		// extract the 'original' url param of containing page
		public function generateOrigURLParam(){			
			$this->origParams = preg_replace('/(&)?page=[0-9]+/','',$_SERVER['QUERY_STRING'],-1);		// remove page param 
 			$this->origParams = preg_replace('/&jcp=(.)+/','',$this->origParams,-1);							// remove jcp (this is the one generated by this component) param
			return $this->origParams;
		}
		/**
		 * @return array of strings, which are the params used for tabs
		 */
	  function generateViewTabURLParams(){
			$tabs = array('FEATURED','NEW','JOURNALS_FEED','STAFF_ADMIN','APPROVED','UNAPPROVED');
				
			$qryStr = 'VIEWMODE=' . $this->viewMode . '&SECURITY_CONTEXT=' . $this->securityContext . 
 				'&SITE=' . $this->site . '&ROWS_PER_PAGE=' . $this->rpp ;
 			  								
			
			if (count($this->criAr) > 0){
				foreach($this->criAr as $k => $v){					
					$qryStr .= '&' . strtolower($k) . '=' . $v;
				}
			}
			
			$tabsParam = array();
			foreach($tabs as $tab){
				$tabsParam[$tab] = 'gID=' . $this->subjectID .'&jcp=' . base64_encode($qryStr . '&VIEW_TAB=' . $tab);
				//$tabsParam[$tab] = 'jcp=' . $qryStr . '&VIEW_TAB=' . $tab;
			}
			
			//$jcParam = 'jcp=' . base64_encode($qryStr);				// journal component param to be passed during submit
			return $tabsParam;
		}
		
		/**
		 *  sets the criteria for retrieving journals
		 * 	@param array $criAr	(criteria1  => value1, criteria2 => value2,...)
		 */
		function setJournalCriteria2($criAr){			
			$this->criAr = $criAr;
			if (array_key_exists('LOCATIONID',$this->criAr)){
				$this->locationID = $this->criAr['LOCATIONID'];
			} 
			if (array_key_exists('TRAVELERID',$this->criAr)){
				$this->travelerID = $this->criAr['TRAVELERID'];
			}
			
		}
		
		function setCompTitle($title){
			$this->compTitle = $title;
		}
		
		// added by neri to get staff and admin journals only: 12-12-08
		
		function getStaffAdminJournalsOnlyForVideos() {
			$this->retrieveJournals();
			$this->filterJournals();
			return $this->journals; 
		}
		
		/**
		 * Retrieves and set the journal drafts count.
		 * 
		 * @return void
		 */
		function retrieveJournalDraftsCount(){
			require_once('travellog/model/Class.JournalEntryDraftPeer.php');
			
			$this->mJournalDraftsCount = JournalEntryDraftPeer::retrieveDraftEntryCount($this->loggedUserId, 1);
		}
		
		function render(){		
			$ti = microtime(true);
			$this->invalidateCacheEntry();
			if ($this->enableCaching && $this->renderCachedContent()){
				// caching is enabled and was able to find and output cached content so we..
				return;
			}
			
			$this->retrieveJournals();							// retrieve all journals for group,traveler
			ob_end_flush(); 	// TEMPORARY FIX FOR WHITE PAGE IN PROFILE					
			$this->filterJournals();
			$this->retrieveJournalDraftsCount();
			$this->retrieveApprovedJournals();					// get approved journal ids																 			
			$this->retrieveFeaturedJournals();					// get featured journal ids															
			$this->configurePaging();							// create paging comp and remove journals not to be displayed in a page
			$this->configureTabs();	
					
			$this->retrieveRelatedGroups();						// retrieve the related group of all journals
			
			$this->configureJournalControls();
			
			$this->retrieveJournalEntries();
			
			$this->filterJournalEntries();
			
			$this->retrieveJournalPhotos();
			
			// if(strpos($_SERVER['HTTP_HOST'],'dev.') >= 0 || strpos($_SERVER['HTTP_HOST'],'.local') >= 0)
			$this->retrieveJournalUpdates();				// retrieve updates for journal
			
			$this->setupTemplate();							// setup template to display the component
			
								
		}
		
		protected function renderCachedContent(){
			$renderStat = 0;
			$key	=	$this->generateCacheKey($this->cacheKeySource);
			$content	=	$this->cache->get($key);
			if ($content) {
				echo $content;					// cached content is found so we out the data;
				$renderStat	=	1;				// we set renderStat to true to tell the caller that cache was found and rendered
			}
			
			return $renderStat;
		}
		
		protected function generateCacheKey($keySource,$params = array()){
			require_once('travellog/model/Class.SiteContext.php');
			$siteID = SiteContext::getInstance()->getGroupID();
			$key = '';
			if ($this->cacheKeySource == 'URL'){
				$key	=	'Journal_URL_' . $siteID . $_SERVER['SCRIPT_NAME'] . $_SERVER['QUERY_STRING'];
				
			} else if ($this->cacheKeySource ==	'PROFILE_CONTEXT_VARS'){
				$key 	=	'Journal_Profile_Context_' . $siteID . '_' . $this->subjectID;
			} else if ($this->cacheKeySource ==	'GROUP_CONTEXT_VARS'){
				$key 	=	'Journal_Group_Context_' . $this->securityContext . '_' . $this->subjectID . '_' .
								$siteID . $_SERVER['SCRIPT_NAME'] . $_SERVER['QUERY_STRING'];
			}
			$_SESSION['journal_keys'][] = $key;
			return $key;
		} 
		
		public function invalidateCacheEntry(){
			if(isset($_SESSION['TRAVEL_CACHE_ENTRY_UPDATED']) && $_SESSION['TRAVEL_CACHE_ENTRY_UPDATED'] == true){
				$this->cache = ganetCacheProvider::instance()->getCache();
				if ($this->cache != null){
					$keys = isset($_SESSION['journal_keys']) ? $_SESSION['journal_keys'] : array();
					foreach($keys as $key){
						$this->cache->delete($key);
					}
				}
				unset($_SESSION['TRAVEL_CACHE_ENTRY_UPDATED']);
				unset($_SESSION['journal_keys']);
			}
		}
		
		function renderJournalsFeed(){			
			require_once("travellog/model/Class.ActivityFeed.php");
			require_once("travellog/components/activity_feed/factory/Class.ActivityFeedAdapterFactory.php");
			$feeds = ActivityFeed::searchFeeds(array(
				'GROUP_OBJECT' => $this->subject,
				'JOURNALS_ONLY' => TRUE
			));
			
			$tFeeds = array();
			foreach($feeds['activityFeeds'] as $feed){
				$feed = ActivityFeedAdapterFactory::create($feed); 
				$feed->setCurrentGroup($this->subject); 
				$feed->showActionLinks();
				$feed->setViewMode("GROUP_JOURNALS");
				if($feed->itemExists()){
					$feed->setOnClickActions(array(
						'FEATURE_ACTION' => 'featureJournal('.$feed->getItem()->getTravelID().','.$this->subjectID.')',
						'APPROVE_ACTION' => 'approveJournal('.$feed->getItem()->getTravelID().','.$this->subjectID.')',
						'TO_UNAPPROVE_ACTION' => 'forwardToUnapproveJournal('.$feed->getItem()->getTravelID().','.$this->subjectID.')'
					));
					$tFeeds[] = $feed;
				}
			}		
			
			$this->journals = $tFeeds;
			$this->rpp = 10;
			$this->configurePaging();
			
			$startrow = $this->rpp * ($this->cp -1);
			$tFeeds = array_slice($tFeeds,$startrow,$this->rpp);
			
			$t = new Template();
			$t->set('feeds',$tFeeds);
			$t->set('pagingComp',$this->pagingComp);			
			$t->set('subjectID',$this->subjectID);
			$t->set('viewMode',$this->viewMode);
			$t->set('viewTab',$this->viewTab);
			$t->out('travellog/views/tpl.ViewJournalsFeed.php');
		}
		
		private function setupTemplate(){
			$tf = microtime(true);	 
			$t = new Template();
			$t->set('subjectID',$this->subjectID);
			$t->set('viewMode',$this->viewMode);
			$t->set('viewTab',$this->viewTab);	
			$t->set('currentPage',$this->cp);		
			$t->set('jEntryInViewCount',$this->jEntryInViewCount);
			$t->set('rowsPerPage',$this->rpp);
			$t->set('locationID',$this->locationID);				
			$t->set('journal_drafts_count', $this->mJournalDraftsCount);
			$t->set('journals',$this->journals);
			$t->set('journalsAndEntries',$this->journalsAndEntries);
			$t->set('journalPhotos',$this->journalPhotos);
			$t->set('tabsScript',$this->tabsScript);
			$t->set('journalControlsJsCode',$this->journalControlsJsCode);
			$t->set('pagingComp',$this->pagingComp);	
			$t->set('scriptName',$this->scriptName);	
			$t->set('addJournalBookLink',$this->addJournalBookLink);
			$t->set('securityContext',$this->securityContext);
			$t->set('canAddJournal',$this->canAddJournal);
			$t->set('hideButton',$this->hideButton);
			$t->set('relatedGroupsAr',$this->relatedGroupsAr);
			$t->set('showAuthor',$this->showAuthor);

			$this->compTitle = ($this->subFeatured) ? 'Travel Journals' : $this->compTitle;
			$this->compTitle = ($this->subject instanceOf AdminGroup && $this->subject->getParentID() > 0) ? $this->subject->getName().' '.$this->compTitle : $this->compTitle;
			$t->set('compTitle',$this->compTitle);
			$t->set('subFeatured', $this->subFeatured);
			// updates
			$t->set('journalsAndUpdates',$this->journalsAndUpdates);
			
			//$t->set('viewTabs',$this->generateViewTabURLParams());
			$t->set('origParams',$this->origParams);
			$t->set('canManage',($this->isAdmin || $this->isStaff)?true:false);
			$t->set('subject',$this->subject);
			
			// set message if there is no journal
			$noJournalMessage = 'No travel journals yet.'; 

			if (count($this->journals) == 0){			  
			
			  if ($this->subject != null) {
					if ($this->subject instanceof Traveler) {
						// if it's a traveler
						
						if ($this->subject->getTravelerID() == $this->loggedUserId){
							$noJournalMessage = '<span>Document those great travel moments and share them with family and friends!</span> <br /> A journal book is a set of journal entries about the same trip. You may write about your experiences and add your ' .
									'travel photos to your entries. You may also do a photo journal. <a href="/journal.php?action=add"><strong>Create your first Travel Journal Book</strong></a>';
						} else {
							$noJournalMessage = $this->subject->getUsername() . ' has no travel journals yet. Please check back later.';
						}	
					} else if ($this->subject instanceof Group) {
						// it's a group or group admin?
						$noJournalMessage = $this->subject->getName() . ' has no travel journals yet.';
					}
			  }	  
			  
			
			  //-- message when logged as admin
			  switch ($this->viewTab){
			  	case 'NEW':	
			  		$noJournalMessage = 'No new travel journals from group members.';
			  		break;
			  	case 'FEATURED':	
			  		$noJournalMessage = 'No featured journals.';
			  		break;	
			  	case 'STAFF_ADMIN':
			  		$noJournalMessage = 'No journals created from this group.';
			  		break;
			  	case 'APPROVED':
			  		$noJournalMessage = 'No approved journals.';
			  		break;	   		
			  	case 'UNAPPROVED':
			  		$noJournalMessage = 'No unapproved journals.';
			  		break;	 
			  	case 'STAFF_ADMIN_APPROVED':
			  		$noJournalMessage = 'There are no journals.';
			  		break;	   			  	
			  }	
			  //--
			  
			  //-- message when there is no search result
			  if ($this->travelerID > 0){
			  	$tr = new Traveler($this->travelerID);
			  	$noJournalMessage = 'No journals found written by ' . $tr->getUsername() . ' in this area. Please try searching from other tabs. ' ;
			  }
			  //--
					
			}
			$t->set('noJournalMessage',$noJournalMessage);

			if ($this->enableCaching){
				$content	=		$t->fetch('travellog/views/'.$this->htmlTemplate);
				echo $content;
				// set the $content to cache
				$key	=	$this->generateCacheKey($this->cacheKeySource);
				$this->cache->set($key,$content,array('EXPIRE'=>$this->cacheExpire)); 
			} else {
				$t->out('travellog/views/'.$this->htmlTemplate);
			}
			
		}
		
		
		
		
		
		
		//setup hierarchical mode string ex. profile, group.all, group.featured ;
		function addViewModeSetup($setupString){
			$this->viewModeSetup = $setupString;
		}
									
		// setup hierarchical security context string ex. public, login.personal, login.group.admin
		function addSecurityContextSetup($setupString) {
			$this->securityContext = $setupString;
		}
		
		
		/*function setLoggedUserId($id){
			$this->loggedUserId = $id;
		}
		
		// the id of the viewed object like (traveler, group, subgroup)
		function setSubjectId($id){
			$this->subjectID = $id;
		}*/
		
		
		protected function retrieveJournals(){
			require_once('travellog/model/Class.JournalPrivacyPreference.php');
			
			$ar = explode('.',$this->viewMode);
			$secAr = explode('.',$this->securityContext);						
			// view journals for Travels of AdminGroup
			if ($this->subject != null){			
				if ($this->subject instanceof Traveler){							
					$ownerTravID = $this->subject->getTravelerID();												
				}
				
				if ($this->subject instanceof AdminGroup){																
					$ownerTravID = $this->subject->getAdministrator()->getTravelerID();																										
				}
				$jpp = new JournalPrivacyPreference();
				$privacyCriteria2 = $jpp->getPrivacyCriteria2s($this->loggedUserId , $ownerTravID );
								
				if ($this->subject instanceof Traveler ){
					if($this->cobrandProfile){
						// LOGGED IN AND OWNER
						if (in_array('PROFILE',$ar) && in_array('LOGIN',$secAr) && $this->loggedUserId > 0 && $this->loggedUserId == $this->subject->getTravelerID()  )
							$this->journals = $this->subject->getTravels($privacyCriteria2, 1);
						else // NOT OWNER
							$this->journals = $this->subject->getCBTravels($privacyCriteria2, 0, $GLOBALS['CONFIG']->getGroupID());
					}else{
						if($this->loggedUserId > 0 && $this->loggedUserId == $this->subject->getTravelerID()){ // owner
							$this->journals = $this->subject->getTravels($privacyCriteria2, 1);
						}else{ // not owner
							$this->journals = $this->subject->getTravels($privacyCriteria2, 0);
						}
					}
						
				}
				else {
					
					if ($this->locationID > 0){		
						// with a location id search criteria
						require_once('travellog/custom/COBRAND/model/Class.TravelCB.php');
						if($this->journalsAndArticles){
							$arr_vars = array();
							$arr_vars['locationID'] = $this->locationID;
							$arr_vars['groupID'] = $this->subject->getGroupID();
							$arr_vars['privacyCriteria2'] = $privacyCriteria2;
							$this->journals = TravelCB::getAllGroupTravelsAndArticlesByCountryID($arr_vars);				
							$this->journals = array_merge($this->journals,TravelCB::getAllGroupMemberTravelsAndArticlesByLocationID($arr_vars));
						}else{
							$arr_vars = array();
							$arr_vars['locationID'] = $this->locationID;
							$arr_vars['groupID'] = $this->subject->getGroupID();
							$arr_vars['privacyCriteria2'] = $privacyCriteria2;
							$this->journals = TravelCB::getAllGroupTravelsByCountryID($arr_vars);				
							$this->journals = array_merge($this->journals,TravelCB::getAllGroupMemberTravelsByLocationID($arr_vars));
						}
						
					}else if(array_key_exists('TAG',$this->criAr)){
						require_once('travellog/custom/COBRAND/model/Class.TravelCB.php');
						$arr_vars = array();
						$arr_vars['groupID'] = $this->subject->getGroupID();
						$arr_vars['tag'] = $this->criAr['TAG'];
						$this->journals = TravelCB::getAllGroupTravelsAndArticlesByTag($arr_vars);
					}else {
						if($this->journalsAndArticles){
							if ($this->travelerID == 0) {
								$this->journals = $this->subject->getTravelsAndArticles($privacyCriteria2);     
							}
							
							$this->journals = array_merge($this->journals,$this->loadTravelsOfMembers());
							
						}else{
							if ($this->travelerID == 0) {
								$this->journals = $this->subject->getTravels($privacyCriteria2);     
							}

							$this->journals = array_merge($this->journals,$this->loadTravelsOfMembers());
						}
					}
				}		
			}	
			
			// view all journals in (Goabroad.net site)
			if (in_array('ALL',$ar) && $this->subject == null){
				require_once('travellog/model/Class.Travel.php');
				require_once('travellog/model/Class.Condition.php');
				require_once('travellog/model/Class.FilterCriteria2.php');
				require_once('travellog/model/Class.FilterOp.php');
				require_once('Class.Criteria2.php');
				
				$filterCri = new FilterCriteria2();
				$condition = new Condition();
				$condition->setAttributeName('lastupdated');
				$condition->setOperation(FilterOp::$ORDER_BY_DESC);
				$filterCri->addCondition($condition);
								
				$criteria = new Criteria2();				
				$criteria->mustBeEqual('tblTravel.publish', 1);
				$criteria->mustBeGreaterThan('tblTravel.entryCount',0);
				$criteria->mustBeEqual('tblTravelLog.publish', 1);
				
				if (array_key_exists('LOCATIONID',$this->criAr)){										
					if($this->journalsAndArticles)
						$this->journals = Travel::getAllTravelsAndArticlesByCountryID($this->criAr['LOCATIONID']);	
					else
						$this->journals = Travel::getAllTravelsByCountryID($this->criAr['LOCATIONID']);	
				}elseif(array_key_exists('TAG',$this->criAr)){
					$this->journals = Travel::getAllTravelsAndArticlesByTag($this->criAr['TAG']);	
				}else{
					if($this->journalsAndArticles)
						$this->journals = Travel::getFilteredTravelsAndArticles($filterCri,null,$criteria);
					else
						$this->journals = Travel::getFilteredTravels($filterCri,null,$criteria);
				}					 							
			}
		
			if ($this->journals == null)
				$this->journals = array();	
											
			// recreate travels array to be an assoc array to be used for searching
			$newJournals = array();
			foreach($this->journals as $journal){
				$newJournals[$journal->getTravelID()] = $journal;
			}
			
			$this->journals = $newJournals;
		}
		
		protected function filterJournals(){				
			//echo 'VIEW TAB::'	. $this->viewTab;
			switch ($this->viewTab){
				case 'STAFF_ADMIN':
					$this->filterJournalsForStaffAdmin();					
					return;
				case 'FEATURED':
					$this->filterFeaturedJournals();
					return;
				case 'NEW':
					$this->filterNewGroupMemberJournals();
					return;	
				case 'APPROVED':					
					$this->filterApprovedJournals();
					return;
				case 'UNAPPROVED':
					$this->filterUnapprovedJournals();
					return;
				case 'STAFF_ADMIN_APPROVED':
					$this->filterStaffAdminApprovedJournals();
					return;	
				default:
							
			}
			
			/**
			 * the code below is filtering journals for 'ALL' viewMode case. In this case there is no regard for viewTab
			 */
			$ar = explode('.',$this->viewMode);	
			$secAr = explode('.',$this->securityContext);
			
			
			$appTIDs = array();							// approved travel ids of members in a group
			if (in_array('GROUP',$ar)){
				$appTIDs = GroupApprovedJournals::getApprovedTravelIDsInGroup($this->subjectID);
			}
																				
			foreach($this->journals as $journal){			
				// condition (filter by travelerID)				
				if ($this->travelerID > 0) {
					if (!$journal->getTravelerID() == $this->travelerID){						
						unset($this->journals[$journal->getTravelID()]);							
					}					
					continue;
				}
				
				// condition (view traveler in public)				
				if (in_array('PROFILE',$ar) && in_array('PUBLIC',$secAr) && $this->loggedUserId == 0){															
					// so we remove unpublished journals and journals with no entries
					if (!$journal->getPublish() || $journal->getEntryCount() == 0){
						unset($this->journals[$journal->getTravelID()]);							
					}					
					continue;
				} 
				
				// condition (view traveler, logged in but is not the subject )
				if (in_array('PROFILE',$ar) && in_array('LOGIN',$secAr) && $this->loggedUserId > 0 && $this->loggedUserId != $this->subject->getTravelerID()  ){					
					// so we remove unpublished journals and journals with no entries
					if (!$journal->getPublish() || $journal->getEntryCount() == 0){
						unset($this->journals[$journal->getTravelID()]);	
					}
					continue;
				}
				
				// condition (view group in public)
				if (in_array('GROUP',$ar) && in_array('PUBLIC',$secAr) && $this->loggedUserId == 0 ){																				
					if ($journal->getOwner() instanceof AdminGroup) {						
						// we remove unpublished journals and those with no entries
						if (!$journal->getPublish() || $journal->getEntryCount() == 0 ){																										
							unset($this->journals[$journal->getTravelID()]);	
						}
					} else {
						
						// we remove unapproved and unpublished journals
						if (!$journal->isApproved($this->subject->getGroupID()) || !$journal->getPublish() || $journal->getEntryCount() == 0){
							//echo $journal->getPublish() . '<br/>';
							unset($this->journals[$journal->getTravelID()]);	
						} elseif (!array_key_exists($journal->getTravelID(),$appTIDs)){
							
							unset($this->journals[$journal->getTravelID()]);
						}												
					}
					continue;
				}
				
				// condition (view group, logged in, but is not admin/staff of the group)	
				if (in_array('GROUP',$ar) && in_array('LOGIN',$secAr) && !$this->isAdmin && !$this->isStaff){
									
					if ($journal->getOwner() instanceof AdminGroup ){
						if (!$journal->getPublish() || $journal->getEntryCount() == 0 )
							unset($this->journals[$journal->getTravelID()]);
					} else {
						if (!$journal->isApproved($this->subject->getGroupID()) || $journal->getEntryCount() == 0)							
							unset($this->journals[$journal->getTravelID()]);
						elseif (!array_key_exists($journal->getTravelID(),$appTIDs)){
							unset($this->journals[$journal->getTravelID()]);
						}													
					}
										
					continue;
				}
				
				// condition (view group, logged in, but is not admin/staff and not the owner of the journal )
				if (in_array('GROUP',$ar) && in_array('LOGIN',$secAr) && !$this->isAdmin && !$this->isStaff && $journal->getOwner() instanceof Traveler && $journal->getOwner()->getTravelerID() != $this->loggedUserId ){					
					
					// so we remove unapproved journals, unpublished journals and journals with no entries
					if (!$journal->isApproved($this->subject->getGroupID()) || !$journal->getPublish() || $journal->getEntryCount() == 0 ){
						unset($this->journals[$journal->getTravelID()]);	
					} elseif (!array_key_exists($journal->getTravelID(),$appTIDs)){
							unset($this->journals[$journal->getTravelID()]);
						}												
					continue;
				}
				
				// condition (view journals in public) , we remove those with no journal entries				
				if (in_array('PUBLIC',$secAr) && $this->loggedUserId == 0 ){										
					if (!$journal->getPublish() || $journal->getEntryCount() == 0){												
						unset($this->journals[$journal->getTravelID()]);						
					}					
				}
			}
		}
		
		private function filterNewGroupMemberJournals(){
			require_once('travellog/dao/Class.GroupApprovedJournals.php');
			$jIDs = GroupApprovedJournals::getAllTravelIDsInGroup($this->subjectID);
			foreach($this->journals as $journal) {
				if($journal->getType() == "ARTICLE"){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof Traveler && $journal->getOwner()->isSuspended()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof Traveler && $journal->getOwner()->isDeactivated()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if($journal->getEntryCount() <= 0){
					unset($this->journals[$journal->getTravelID()]);
				}
				if (!$journal->getPublish()){
					unset($this->journals[$journal->getTravelID()]);
				} 
				elseif ($journal->getOwner() instanceof AdminGroup) {
					unset($this->journals[$journal->getTravelID()]);
				} elseif (array_key_exists($journal->getTravelID(),$jIDs)){
					unset($this->journals[$journal->getTravelID()]);
					//echo 'found<br/>';
				} elseif ($this->travelerID > 0 && $this->travelerID != $journal->getTravelerID()) {
					unset($this->journals[$journal->getTravelID()]);
				}
			} 			
		}
		
		private function filterJournalsForStaffAdmin(){		    
			foreach ($this->journals as $journal){
				if ($journal->getOwner() instanceof Group && $journal->getOwner()->IsSuspended()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof Traveler)
					unset($this->journals[$journal->getTravelID()]);
			}
			
		}
		
		private function filterFeaturedJournals(){
			require_once('travellog/model/Class.Featured.php');
			$tIDs = Featured::getFeaturedTravelIDsInGroup($this->subjectID);
			
			foreach ($this->journals as $journal){
				if ($journal->getOwner() instanceof Traveler && $journal->getOwner()->isSuspended()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof Traveler && $journal->getOwner()->isDeactivated()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof Group && $journal->getOwner()->IsSuspended()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if (!array_key_exists($journal->getTravelID(),$tIDs) || !$journal->getPublish())
					unset($this->journals[$journal->getTravelID()]);
					
				elseif ($this->travelerID > 0 && $this->travelerID != $journal->getTravelerID()) {
					unset($this->journals[$journal->getTravelID()]);
				}
				
				elseif ($journal->getEntryCount() == 0) {
					unset($this->journals[$journal->getTravelID()]);
				}	
			}
		}
		
		private function filterApprovedJournals(){
			foreach ($this->journals as $journal){
				if ($journal->getOwner() instanceof Traveler && $journal->getOwner()->isSuspended()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof Traveler && $journal->getOwner()->isDeactivated()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof AdminGroup || !$journal->isApproved($this->subject->getGroupID())){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				elseif ($this->travelerID > 0 && $this->travelerID != $journal->getTravelerID()) {
					unset($this->journals[$journal->getTravelID()]);
				}
				
				elseif($journal->getEntryCount() == 0) {
					unset($this->journals[$journal->getTravelID()]);
				}
				
			}
		}
		
		// staff/admin journals and approved group members journals
		private function filterStaffAdminApprovedJournals(){
			$jIDs = GroupApprovedJournals::getApprovedTravelIDsInGroup($this->subjectID);
			foreach ($this->journals as $journal){
				if($journal instanceOf Travel || (($journal instanceOf JournalsArticlesVO) && $journal->getType() == "JOURNAL" )){
					if (!array_key_exists($journal->getTravelID(),$jIDs) && !($journal->getOwner() instanceof AdminGroup)){
						unset($this->journals[$journal->getTravelID()]);
					} elseif ($journal->getEntryCount() == 0 ){
						unset($this->journals[$journal->getTravelID()]);
					}
				}	
			}
		}

		private function filterUnapprovedJournals(){
			require_once('travellog/dao/Class.GroupApprovedJournals.php');
			$jIDs = GroupApprovedJournals::getUnapprovedTravelIDsInGroup($this->subjectID);
			foreach ($this->journals as $journal){
				if ($journal->getOwner() instanceof Traveler && $journal->getOwner()->isSuspended()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof Traveler && $journal->getOwner()->isDeactivated()){
					unset($this->journals[$journal->getTravelID()]);
				}
				
				if ($journal->getOwner() instanceof AdminGroup || !array_key_exists($journal->getTravelID(),$jIDs) ){
					unset($this->journals[$journal->getTravelID()]);
				}
				elseif ($this->travelerID > 0 && $this->travelerID != $journal->getTravelerID()) {
					unset($this->journals[$journal->getTravelID()]);
				}
			}
		}
		
		
		protected function retrieveRelatedGroups(){
			require_once('travellog/dao/Class.GroupApprovedJournals.php');
			if ($this->viewMode != 'GROUP' ){
				foreach ($this->journals as $journal){
					/*if ($rGroup = GroupApprovedJournals::getRelatedGroup($journal->getTravelID())){
						//$approved2 = false;
						
						$approved1 = GroupApprovedJournals::isApproved($journal->getTravelID(),$rGroup->getGroupID());
						
						if (!$rGroup->isParent()){
							$rGroup = $rGroup->getParent();						
						}	
							
						if ($approved1){						
							$this->relatedGroupsAr[$journal->getTravelID()] = $rGroup ;						
						}							
					}*/
					if($journal instanceOf Travel || (($journal instanceOf JournalsArticlesVO) && $journal->getType() == "JOURNAL" )){
						$rGroups = GroupApprovedJournals::getRelatedGroups($journal->getTravelID());
						$this->relatedGroupsAr[$journal->getTravelID()] = $rGroups ;						
					}
				}
			}				
		}
		
		
		/**
		 * this will put journal entries array into an assoc array with journal id as the key
		 */
		protected function retrieveJournalEntries() {
			require_once('travellog/model/Class.TravelLog.php');			
			if ($this->subject != null) {
				
				// temporarily implemented in tpl.JournalsComp.php for now
				$orderByArrival = $this->htmlTemplate == 'tpl.JournalsComp.php' || $this->htmlTemplate == 'tpl.JournalsCompTravelerView2.php';

				$jEntries = TravelLog::getTravellogs($this->subject, null, $orderByArrival);
				
				if ($this->subject instanceof AdminGroup)		
					$jEntries = array_merge($jEntries,$this->loadTravellogsOfMembers($this->journals));
				
				// loop through journals and find corresponding journal entries	
				$newJEntries = array();
				$this->journalsAndEntries = array();
				$found = false;		
				foreach ($this->journals as $journal){
					if($journal instanceOf Travel || (($journal instanceOf JournalsArticlesVO) && $journal->getType() == "JOURNAL" )){
						foreach ($jEntries as $jEntry){						
							if ($jEntry->getTravelID() == $journal->getTravelID()){							
								$found = true;
								$newJEntries[] = $jEntry;
							} 
						}
						$this->journalsAndEntries[$journal->getTravelID()] = $newJEntries;
						$newJEntries = array();			//reset the array for new entries				
					}
				}														
			} else {				
				
				// subject is null so we retrieve the journal entries for those existing in this->journals array
				$newJEntries = array();
				$this->journalsAndEntries = array();				
				foreach($this->journals as $journal){
					if($journal instanceOf Travel || (($journal instanceOf JournalsArticlesVO) && $journal->getType() == "JOURNAL" )){
						$trips = $journal->getTrips();
						foreach($trips as $trip){
							$tLogs = $trip->getTravelLogs();
							foreach($tLogs as $tLog){
								$newJEntries[] =	$tLog;  
							}						
						}
						$this->journalsAndEntries[$journal->getTravelID()] = $newJEntries;
						$newJEntries = array(); 
					}
				}				
			}
		}
		
		protected function filterJournalEntries(){			
			$ar = explode('.',$this->viewMode);	
			$secAr = explode('.',$this->securityContext);
			
												
			foreach ($this->journalsAndEntries as $jID => $jeLines) {					
				if (!array_key_exists($jID,$this->journals)){			
					continue;
				}
				
				$i = -1;
				$tempArray = array();										// an array to hold the filtered journal entries
				foreach($jeLines as $jEntry){	
					$i++;
					// condition (view traveler in public)				
					if (in_array('PROFILE',$ar) && in_array('PUBLIC',$secAr) && $this->loggedUserId == 0){																					
						
						// we include published journal entries
						if ($jEntry->getPublish()){																			
							$tempArray[] = $jEntry;											
						}							
						continue;
					} 
					
					// condition (view traveler, logged in but is not the subject )
					if (in_array('PROFILE',$ar) && in_array('LOGIN',$secAr) && $this->loggedUserId > 0 && $this->loggedUserId != $this->subject->getTravelerID()  ){					
				
						if ($jEntry->getPublish()){							
							$tempArray[] = $jEntry;	
						}
						continue;
					}
					
					// condition (view group in public)
					if (in_array('GROUP',$ar) && in_array('PUBLIC',$secAr) && $this->loggedUserId == 0 ){						
						// we include published journal entries
						if ($jEntry->getPublish()){							
							$tempArray[] = $jEntry;	
						}
						continue;
					}
					
					// condition (view group, logged in, and user is admin/staff)
					if (in_array('GROUP',$ar) && in_array('LOGIN',$secAr) && ($this->isAdmin || $this->isStaff)){
						if (($jEntry->getOwner() instanceof Traveler && $jEntry->getPublish()) || $jEntry->getOwner() instanceof AdminGroup){
							$tempArray[] = $jEntry;	
						}
						continue;
					}
					 
					// condition (view group, logged in, but is not admin/staff of the group)	
					if (in_array('GROUP',$ar) && in_array('LOGIN',$secAr) && $this->loggedUserId > 0 && !$this->isStaff && !$this->isAdmin  ){						
						if ($jEntry->getPublish()){
							$tempArray[] = $jEntry;	
						} elseif ($jEntry->getOwner() instanceof Traveler && $jEntry->getOwner()->getTravelerID() == $this->loggedUserId) {
							$tempArray[] = $jEntry;
						}						
						continue;
					}
					
					// condition (view group, logged in, but is not admin/staff and not the owner of the journal )
					if (in_array('GROUP',$ar) && in_array('LOGIN',$secAr) && $jEntry->getOwner() instanceof Traveler && $jEntry->getOwner()->getTravelerID() != $this->loggedUserId ){
						// so we remove unapproved and unpublished journals
						if ($jEntry->getPublish()){
							//unset($this->journalsAndEntries[$jEntry->getTravelID()]);
							$tempArray[] = $jEntry;	
						}
						continue;
					}
					
					if (in_array('PUBLIC',$secAr) && $this->loggedUserId == 0){																											
						// we include published journal entries
						if ($jEntry->getPublish()){
							$tempArray[] = $jEntry;											
						}							
						continue;
					} 	
					
					$tempArray[] = $jEntry;									
				}
				
				$this->journalsAndEntries[$jID] = $tempArray;
								
			}
									
		}
		
		protected function retrieveJournalUpdates(){
			$this->journalsAndUpdates = array();
			$jUpdates = array();
			foreach($this->journals as $journal){
				if($journal instanceOf Travel){
					$jUpdates['LAST_ENTRY_UPDATED'] = $journal->getLastEntryUpdated();
					$jUpdates['RECENTLY_ADDED_ENTRY'] = $journal->getRecentlyAddedEntry();
					//$jUpdates['PHOTOS_AND_VIDEOS_COUNT'] = $this->filterJournalUpdates($journal->getLatestPhotoAndVideoCountPerEntry());
					$this->journalsAndUpdates[$journal->getTravelID()] = $jUpdates;
					$jUpdates = array();
				}
			}
		}
		
		protected function filterJournalUpdates($jUpdates){
			$cnt = 0;
			$updates = array();
			foreach($jUpdates as $key => $update){
				if($cnt == 2) break;

				$juVo = new JournalUpdatesVO;
				
				$action = "";
				if(isset($update['PHOTO']) && isset($update['VIDEO'])){
					if($update['PHOTO']['cnt'] > 1)
						$action = "Added ".$update['PHOTO']['cnt']." photos and ";
					else
						$action = "Added ".$update['PHOTO']['cnt']." photo and ";
					
					if($update['VIDEO']['cnt'] > 1)	
						$action .= $update['VIDEO']['cnt']." videos to"; 
					else
						$action .= $update['VIDEO']['cnt']." video to"; 
					
					$juVo->setTitle($update['PHOTO']['title']);	
					$juVo->setDate($update['PHOTO']['maxdate']);	
				}else if(isset($update['PHOTO']) && !isset($update['VIDEO'])){	
					if($update['PHOTO']['cnt'] > 1)
						$action = "Added ".$update['PHOTO']['cnt']." photos to"; 
					else
						$action = "Added ".$update['PHOTO']['cnt']." photo to";
					
					$juVo->setTitle($update['PHOTO']['title']);	
					$juVo->setDate($update['PHOTO']['maxdate']);	
				}else if(!isset($update['PHOTO']) && isset($update['VIDEO'])){
					if($update['VIDEO']['cnt'] > 1)	
						$action = "Added ".$update['VIDEO']['cnt']." videos to"; 
					else
						$action = "Added ".$update['VIDEO']['cnt']." video to"; 
					
					$juVo->setTitle($update['VIDEO']['title']);	
					$juVo->setDate($update['VIDEO']['maxdate']);	
				}
				
				$juVo->setTravelLogID($key);	
				$juVo->setAction($action);	
			
				$updates[] = $juVo;	
				
				$cnt++;
			}
			return $updates;
		}
		
		/*protected function filterJournalsWithoutEntries(){						
			$secAr = explode('.',$this->securityContext);
			// we remove the journals without journal entries from public view
			if (in_array('PUBLIC',$secAr) && $this->loggedUserId == 0 ){
				foreach($this->journals as $journal){					
					if (count($this->journalsAndEntries[$journal->getTravelID()]) == 0){						
						//echo 'jid:' . $journal->getTravelID() . '<br/>';
						unset($this->journals[$journal->getTravelID()]);						
					}
				}
			}			
			
		}*/
		

		
		/**
		 * retrieve the photo that will be displayed on the journal box
		 * 
		 * 		Rule : 
		 *           1.  If journal owner selected a primary photo for journal then this will be permanent for that journal
		 * 					Travel->getPrimaryphoto() > 0 
		 * 			 2.  If owner did not select a primary photo, then a random photo will be selected from the latest journal entry
		 * 			 3.  If the latest journal entry has no photos, then we will display nothing		
		 * 
		 * Note: This function should be called after retrieveJournalEntries since this will use journalsAndEntries array	
		 */
		protected function retrieveJournalPhotos() {
			foreach ($this->journals as $journal){
				if($journal instanceOf Travel || (($journal instanceOf JournalsArticlesVO) && $journal->getType() == "JOURNAL" )){
					if (count($this->journalsAndEntries[$journal->getTravelID()]) > 0) {				
						// we have journal entries for this journal
						$tlObjAr = $this->journalsAndEntries[$journal->getTravelID()];
						if ($journal->getPrimaryphoto() > 0){
							// user selected a primary photo						
							$this->journalPhotos[$journal->getTravelID()] = new Photo($tlObjAr[0],$journal->getPrimaryphoto());
						} else {
							// we get the latest journal entry
							$jEntry = $tlObjAr[count($tlObjAr)-1];
							$photos = $jEntry->getPhotos();
							if ($photos != null){
								// we get a random no. from 0 to count of photos -1 
								$rn = rand(0,count($photos)-1);
								// we select our journal photo
								$this->journalPhotos[$journal->getTravelID()] = $photos[$rn];
							}
						}
					}				
				}
				
				if(($journal instanceOf JournalsArticlesVO) && $journal->getType() == "ARTICLE" ){
					$article =  new Article($journal->getTravelID());
					if($article->checkHasprimaryPhoto()){
						$this->journalPhotos['ARTICLE'][$journal->getTravelID()] = $article->getPrimaryPhoto();
					}else{	
						$randPhoto = $article->getRandomPhoto();
						if($randPhoto->getPhotoID() > 0)
							$this->journalPhotos['ARTICLE'][$journal->getTravelID()] = $randPhoto;
					}	
				}	
			}
						
		}
		
		
		/**
		 * create an array of javascript code to configure the action controls of each journal
		 * 		- this is done in order not to clutter the template with logic code and html code will still be editable by designers
		 *		- the compromise is during rendering of page each journal will have an accompanying javascript code   
		 */
		protected function configureJournalControls(){
			$ar = explode('.',$this->viewMode);	
			$secAr = explode('.',$this->securityContext);	
			
			$journalControlsJsCode = array();
			
			foreach ($this->journals as $journal){
				// if the context is public view there should be no controls available
				if (in_array('PUBLIC',$secAr)){
					$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
																	  '    jQuery(".actions").remove();  ' .
																	  '    jQuery(".action_links").remove();  ' .
																	  '</script>';
					// Note: this is just one script for the entire journals but it is enough since it just hides all the controls												  											  
					break;												  	
				}
				
				// user is logged in and viewing his own profile
				if (in_array('PROFILE',$ar) && in_array('LOGIN',$secAr) && $this->loggedUserId == $this->subject->getTravelerID()){
					//script to remove "approve" and "feature" links
					$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
																	  '   jQuery("#approve' . $journal->getTravelID() .  '").remove();'  .
																	  '   jQuery("#span_approve' . $journal->getTravelID() .  '").remove();'  .
																	  '   jQuery("#span_delete' . $journal->getTravelID() .  '").remove();'  .
																	  '   jQuery("#feature' . $journal->getTravelID() .  '").remove();' .
																	  '   jQuery("#forwardToUnapprove' . $journal->getTravelID() .  '").remove();' ;			
																	  
																	  
					if (!$journal->getPublish())
						$ap = '  jQuery("#unpublish' . $journal->getTravelID() . '").attr("innerHTML","Publish");';												
					else
						$ap = '';										
							
												
					// if there is a journal entry we will also remove the delete link
					if ($journal->getEntryCount() > 0){							
						$ap .= '  jQuery("#delete'. $journal->getTravelID() .'").remove();';
						$ap .= '  jQuery("#span_delete'. $journal->getTravelID() .'").remove();';
						$ap .= '  jQuery("#span_edit_journal'. $journal->getTravelID() .'").remove();';
					} else {
						// no journal entry so we remove the publish link
						$ap .= '	jQuery("#unpublish' . $journal->getTravelID() . '").remove();';
						$ap .= '	jQuery("#span_unpublish' . $journal->getTravelID() . '").remove();';
					}	
							
					$journalControlsJsCode[$journal->getTravelID()] .= $ap . '</script>';													  
																	  
																	  
					continue;									  	
				}
				
				// user is logged in and viewing others profile, this is same as PUBLIC view where there are no controls available
				if (in_array('PROFILE',$ar) && in_array('LOGIN',$secAr) && $this->loggedUserId != $this->subject->getTravelerID()){
					$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
																	  '    jQuery(".actions").remove();  ' .
																	  '    jQuery(".action_links").remove();  ' .
																	  '</script>';
					// Note: this is just one script for the entire journals but it is enough since it just hides all the controls												  											  
					break;												  	
				}
				
				// viewing group, and journal is owned by a member of the Group
				if (in_array('GROUP',$ar) && $journal->getOwner() instanceof Traveler ){					
						if ($journal->getOwner()->getTravelerID() == $this->loggedUserId) {
							// logged user is owner of journal
							if (!$this->isStaff) {
								// logged user is owner of the journal and is not a staff of the group, so we remove "approve" and "feature" links						 
                                if ($this->viewTab != 'UNAPPROVED') {
    								$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
    																			  '   jQuery("#approve' . $journal->getTravelID() .  '").remove();'  .
    																			  '   jQuery("#span_approve' . $journal->getTravelID() .  '").remove();'  .
    																			  '   jQuery("#span_delete' . $journal->getTravelID() .  '").remove();'  .
    																			  '   jQuery("#feature' . $journal->getTravelID() .  '").remove();'  .			
    																			  '</script>';
                                } else {
    								$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
    																			  '   jQuery("#span_delete' . $journal->getTravelID() .  '").remove();'  .
    																			  '   jQuery("#feature' . $journal->getTravelID() .  '").remove();'  .	
                                                                                  '   jQuery("#forwardToUnapprove'.$journal->getTravelID().'").remove();'.		
    																			  '</script>';
                                }
							} else {
								// logged user is staff and at the same time the owner of the journal																								
								
								$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">'; 
																																
								$ap = '';
								
									
								if (!$journal->getPublish()){									
									$ap = '  jQuery("#unpublish' . $journal->getTravelID() . '").attr("innerHTML","Publish");';
									
								}												
								else
									$ap = '  jQuery("#unpublish' . $journal->getTravelID() . '").attr("innerHTML","Unpublish");';		
																			  
								// if journal is featured we replace the text with unfeature text ---
								if (array_key_exists($journal->getTravelID(),$this->featuredJournalsAr)){								
									$ap .= '  jQuery("#feature'. $journal->getTravelID() .'").attr("innerHTML","Remove from Featured List");'; 
								} else {
									$ap .= '  jQuery("#feature'. $journal->getTravelID() .'").attr("innerHTML","Feature");';
								}												  				
								
								// if journal is approved we replace unapprove we replace the text with 'unapprove' text ---
								if (array_key_exists($journal->getTravelID(),$this->approvedJournalsAr)){								
									$ap .= '  jQuery("#approve'. $journal->getTravelID() .'").attr("innerHTML","Unapprove");'; 
								} else {
									$ap .= '  jQuery("#approve'. $journal->getTravelID() .'").attr("innerHTML","Approve");';
								}		
								
								// if journal has no journal entries we remove the publish, approve, feature links
								if ($journal->getEntryCount() < 1){
									$ap .= '  jQuery("#unpublish' . $journal->getTravelID() . '").remove();';
									$ap .= '  jQuery("#span_unpublish' . $journal->getTravelID() . '").remove();';
									$ap .= '  jQuery("#approve' . $journal->getTravelID() . '").remove();';
									$ap .= '  jQuery("#span_approve' . $journal->getTravelID() . '").remove();';
									$ap .= '  jQuery("#feature' . $journal->getTravelID() . '").remove();';
								}
								
								// if viewtab is not in NEW, we remove the To Unapprove link
								if ($this->viewTab != 'NEW') {
									$ap .= '  jQuery("#forwardToUnapprove' . $journal->getTravelID() . '").remove();';
								}
								
								
								$ap .= '</script>';
								
								$journalControlsJsCode[$journal->getTravelID()] .= $ap;
																								
							} 									  
						} elseif (($this->isAdmin || $this->isStaff) ) {
							// logged user is admin or staff but not the owner of the journal, so we remove the edit controls and leave approve and feature links
							$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
																			  '   jQuery("#add' . $journal->getTravelID() .  '").remove();'  .
																			  '   jQuery("#span_add' . $journal->getTravelID() .  '").remove();'  .
																			  '   jQuery("#unpublish' . $journal->getTravelID() .  '").remove();'  .
																			  '   jQuery("#span_unpublish' . $journal->getTravelID() .  '").remove();'  .
																			  '   jQuery("#edit_journal' . $journal->getTravelID() .  '").remove();'  .
																			  '   jQuery("#span_edit_journal' . $journal->getTravelID() .  '").remove();'  .
																			  '   jQuery("#delete' . $journal->getTravelID() .  '").remove();'  .
																			  '   jQuery("#span_delete' . $journal->getTravelID() .  '").remove();'  ;			
																			  
							
							$ap = '';												  
							// if journal is featured we replace the text with unfeature text ---
							if (array_key_exists($journal->getTravelID(),$this->featuredJournalsAr)){								
								$ap .= '  jQuery("#feature'. $journal->getTravelID() .'").attr("innerHTML","Remove from Featured List");'; 
							} else {
								$ap .= '  jQuery("#feature'. $journal->getTravelID() .'").attr("innerHTML","Feature");';
							}												  				
							
							// if journal is approved we replace unapprove we replace the text with 'unapprove' text ---
							if (array_key_exists($journal->getTravelID(),$this->approvedJournalsAr)){								
								$ap .= '  jQuery("#approve'. $journal->getTravelID() .'").attr("innerHTML","Unapprove");'; 
							} else {
								$ap .= '  jQuery("#approve'. $journal->getTravelID() .'").attr("innerHTML","Approve");';
							}	
														
							// if journal has no journal entries we remove the publish, approve, feature links
							if ($journal->getEntryCount() < 1){
								$ap .= '  jQuery("#unpublish' . $journal->getTravelID() . '").remove();';
								$ap .= '  jQuery("#span_unpublish' . $journal->getTravelID() . '").remove();';
								$ap .= '  jQuery("#approve' . $journal->getTravelID() . '").remove();';
								$ap .= '  jQuery("#span_approve' . $journal->getTravelID() . '").remove();';
								$ap .= '  jQuery("#feature' . $journal->getTravelID() . '").remove();';
							}
							
							// if viewtab is not in NEW, we remove the To Unapprove link
							if ($this->viewTab != 'NEW') {
								$ap .= '  jQuery("#forwardToUnapprove' . $journal->getTravelID() . '").remove();';
							}
															
							$ap .= '</script>';
							
							$journalControlsJsCode[$journal->getTravelID()] .= $ap;
						}else {
							//logged user is not owner of the journal and is not admin/staff, so we remove all controls for the journal
							$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
																	  		   '    jQuery("#actions'. $journal->getTravelID()  .'").remove();  ' .
																	  		  '</script>';
						}											  						
					 
				} 
				
				// viewing group and journal is owned by the group
				if (in_array('GROUP',$ar) && $journal->getOwner() instanceof AdminGroup ){
					// viewer/user is not admin or staff
					if (!$this->isAdmin && !$this->isStaff) {				
						// remove all controls
						$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
																	  		   '    jQuery("#actions'. $journal->getTravelID()  .'").remove();  ' .
																	  		  '</script>';
					} else {
						// viewer is admin /staff, we just remove the approve button, we also need to change unpublish/publish link if need be
						$journalControlsJsCode[$journal->getTravelID()] = '<script type="text/javascript">' .
																	  		  '   jQuery("#approve' . $journal->getTravelID() .  '").remove();'  .
																			  '   jQuery("#span_approve' . $journal->getTravelID() .  '").remove();' .
																			  '   jQuery("#forwardToUnapprove' . $journal->getTravelID() .  '").remove();';
																			  
																	  	  						
						if (!$journal->getPublish()) {
							$ap = '  jQuery("#unpublish' . $journal->getTravelID() . '").attr("innerHTML","Publish");';
							$ap .= ' jQuery("#feature'. $journal->getTravelID() .'").remove();';
							$ap .= ' jQuery("#span_delete'. $journal->getTravelID() .'").remove();';
						}												
						else
							$ap = '';										
							
												
						// if there is a journal entry we will also remove the delete link
						if ($journal->getEntryCount() > 0){							
							$ap .= '  jQuery("#delete'. $journal->getTravelID() .'").remove();';
							$ap .= '  jQuery("#span_delete'. $journal->getTravelID() .'").remove();';
						}	
											
						// if journal is featured we replace the text with unfeature text ---
						if (array_key_exists($journal->getTravelID(),$this->featuredJournalsAr)){								
							$ap .= '  jQuery("#feature'. $journal->getTravelID() .'").attr("innerHTML","Remove from Featured List");'; 
						} else {
							$ap .= '  jQuery("#feature'. $journal->getTravelID() .'").attr("innerHTML","Feature");';
						}
						
						// if journal has no journal entries we remove the publish, approve, feature links
						if ($journal->getEntryCount() < 1){
							$ap .= '  jQuery("#unpublish' . $journal->getTravelID() . '").remove();';
							$ap .= '  jQuery("#span_unpublish' . $journal->getTravelID() . '").remove();';
							$ap .= '  jQuery("#approve' . $journal->getTravelID() . '").remove();';
							$ap .= '  jQuery("#span_approve' . $journal->getTravelID() . '").remove();';
							$ap .= '  jQuery("#feature' . $journal->getTravelID() . '").remove();';
						}
													
						$journalControlsJsCode[$journal->getTravelID()] .= $ap . '</script>';		  	  
																	  	  
					}
				}				
			}
			
			$this->journalControlsJsCode = $journalControlsJsCode;
		}
		
		/**
		 * function to show the actual state of journal controls (approve/unaprove, feature/unfeature....) 
		 */
		private function changeStateOfJournalControl($journal){
			
		}
		
		
		/**
		 * create a javascript code to control navigation tab
		 * 		plus other javascript codes
		 */
		protected function configureTabs(){
			$ar = explode('.',$this->viewMode);	
			$secAr = explode('.',$this->securityContext);	
			
			/**
			 * viewing a traveler profile has no tabs regardless of security context
			 */
			if (in_array('PROFILE',$ar)){
				$this->tabsScript = '<script type="text/javascript">' .
								 '   jQuery(".tabs").remove();' .								 
								 '</script>';
								
				return;
			}
			
			/**
			 * viewing a group and user is not logged in
			 */
			if (in_array('GROUP',$ar) && (in_array('PUBLIC',$secAr) || $this->loggedUserId == 0) ){
				
				if (count($this->journals) == 0){
					$this->tabsScript = '<script type="text/javascript">' .
										'	jQuery("#journals_summary").remove();' .
										'</script>';
				} else {	
				
					$this->tabsScript = '<script type="text/javascript">' .
										 '   jQuery(".tabs").remove();' .														
										 '</script>';
				}					 
								 	
				return;					
			}
			
			/**
			 * viewing a group and user is either admin/staff of the group
			 */
			 if (in_array('GROUP',$ar) && ($this->isStaff || $this->isAdmin )) {
			 	$this->tabsScript = '<script type="text/javascript">' .
			 						'    jQuery(".tabs li").attr("class",""); '; 				 	
			 	$this->tabsScript .= '</script>';
			 	 			
				$this->addSelectTabJsScript();				 
			 	return;
			 }
			 
			
			//............. more
			
			
			// default
			$this->tabsScript = '<script type="text/javascript">' .
								 '   jQuery(".tabs").remove();' .								 
								 '</script>';								 						 
											
		}
		
		// helper function for configureTabs() to select the appropriate view tab
		private function addSelectTabJsScript(){
			$this->tabsScript .= '<script type="text/javascript">' ;
						
		 	switch ($this->viewTab){
		 		case 'STAFF_ADMIN':
					$this->tabsScript .= ' jQuery("#staff_admin").parent().attr("class","active"); ' ;			 		
		 			break;
		 		case 'FEATURED':			 			
		 			$this->tabsScript .= ' jQuery("#featured").parent().attr("class","active"); ' ;			 					 		
		 			break;
		 		case 'NEW':	
		 			$this->tabsScript .= ' jQuery("#new").parent().attr("class","active"); ' ;			 		
		 			break;	
		 		case 'APPROVED':	
		 			$this->tabsScript .= ' jQuery("#approved").parent().attr("class","active"); ' ;			 		
		 			break;	
		 		case 'UNAPPROVED':	
		 			$this->tabsScript .= ' jQuery("#unapproved").parent().attr("class","active"); ' ;			 		
		 			break;				 			
		 	}
			$this->tabsScript .= '</script>';				
		}
		
		// this should be called after filtering journals
		protected function configurePaging() {
			$qryStr = '';
			
			// if we are in profile mode we will not enable paging
			if($this->viewMode == 'PROFILE')
				return;
			
 			
 			/****
		 * Context vars in $setupAr variable passed in constructor
		 * 		VIEWMODE			=>	GROUP(journals for group), PROFILE(journals for Traveler) , ALL (all journals)
		 * 		SECURITY_CONTEXT 	=> 	PUBLIC (public view) , LOGIN (a viewing user has logged in) 
		 * 		VIEW_TAB			=>  the selected tab , NEW(Untagged Journals of group members), FEATURED,  STAFF_ADMIN, UNAPPROVED, APPROVED
		 * 		CURRENT_PAGE		=>	the current page to display. This is numeric.
		 * 		ROWS_PER_PAGE		=>  the no. of rows to display per page.  
		 * 		RENDERING			=>  page rendering (NORMAL,AJAX)
		 * 		SITE				=>	site where journals are displayed (GOABROAD,COBRAND)
		 */
 			
 			$qryStr = 'VIEWMODE=' . $this->viewMode . '&VIEW_TAB=' . $this->viewTab .'&SECURITY_CONTEXT=' . $this->securityContext . 
 				'&SITE=' . $this->site . '&ROWS_PER_PAGE=' . $this->rpp ;
 			  								
			
			if (count($this->criAr) > 0){
				foreach($this->criAr as $k => $v){					
					$qryStr .= '&' . strtolower($k) . '=' . $v;
				}
			}
			
			
			if (isset($_GET['friendly_url']) && $_GET['friendly_url'] ){
				$jcParam = 'jcp/' . base64_encode($qryStr);				// journal component param to be passed during paging
				$this->pagingComp = new JournalsCompFUrlPaging(count($this->journals),$this->cp,$jcParam,$this->rpp);
			}	
			else{
				$jcParam = 'jcp=' . base64_encode($qryStr);				// journal component param to be passed during paging
				
				if( "/group.php" != $_SERVER["SCRIPT_NAME"] && "/index.php" != $_SERVER["SCRIPT_NAME"] ){
					$jcParam = "gID=".$this->subjectID."&".$jcParam;
				}elseif( isset($_GET["gID"]) ){
					$_SERVER["QUERY_STRING"] = str_replace("gID=".$this->subjectID."&","",$_SERVER["QUERY_STRING"]);
					$_SERVER["QUERY_STRING"] = "gID=".$this->subjectID.$_SERVER["QUERY_STRING"];
					$_SERVER["QUERY_STRING"] = str_replace("gID=".$this->subjectID."gID=".$this->subjectID,"gID=".$this->subjectID,$_SERVER["QUERY_STRING"]);
				}
				
				$this->pagingComp = new JournalsCompPaging(count($this->journals),$this->cp,$jcParam,$this->rpp);
				
				if( "/common_controller.php" == $this->pagingComp->getScriptName() ){
					$this->pagingComp->setScriptName("/group.php");
				}
				
			}

			//$this->pagingComp->setOnclick('changePage');			// set js function name when changing pages	
			
			$startrow = $this->rpp * ($this->cp -1);
			// get the journals that are only part of the current page
			$this->journals = array_slice($this->journals,$startrow,$this->rpp);
		}
		
		
		
		
		protected function loadTravellogsOfMembers($travels){
	 		$members = array();
	 		foreach ($travels as $travel ){
				if($travel instanceOf Travel || (($travel instanceOf JournalsArticlesVO) && $travel->getType() == "JOURNAL" )){
		 			if ($travel->getOwner() instanceof Traveler){
		 				$members[$travel->getOwner()->getUserName()] = $travel->getOwner();
		 			}
				}
	 		} 
	 		
	 		$memTLogs = array();
	 		foreach($members as $member){
	 			$memTLogs = array_merge($memTLogs,TravelLog::getTravellogs($member));
	 		}
	 		
	 		return $memTLogs;	
 		}
 		
 		
 		// load travels(journals) of members of a group
 		protected function loadTravelsOfMembers() {
 			$members = array();
 			$mt = array();
 			
 			if ($this->subject instanceof AdminGroup){
 				/*$members = $this->subject->getMembers();
 				$jpp = new JournalPrivacyPreference();
 				foreach ($members as $member){ 					
					$privacyCriteria2 = $jpp->getPrivacyCriteria2s($this->loggedUserId , $member->getTravelerID() ); 					
 					$mt = array_merge($mt,$member->getTravels($privacyCriteria2,1));
 				}*/
 				if($this->journalsAndArticles)
	 				$mt = $this->subject->getTravelsAndArticlesOfMembers();
				else
 					$mt = $this->subject->getTravelsOfMembers();
 			}
 			 			 			
 			return $mt;
 		}
 		
 		// callback function to sort group members journals by last updated
 		private static function compareTravelLastUpdate($a,$b){ 			
 			if ($a->getLastUpdated() == $b->getLastUpdated()) {
		        return 0;
		    }
		    return ($a->getLastUpdated() < $b->getLastUpdated()) ? 1 : -1;
 		}
 		
 		
 		// retrieve approved traveler journals who are member of a group
 		protected function retrieveApprovedJournals(){
 			require_once('travellog/dao/Class.GroupApprovedJournals.php');
 			// this is only needed if logged user is admin or staff
 			if ($this->isAdmin || $this->isStaff){ 				
 				$this->approvedJournalsAr = GroupApprovedJournals::getApprovedTravelIDsInGroup($this->subjectID); 				
 			}
 		}	
 		
 		protected function retrieveFeaturedJournals(){
 			
 			require_once('travellog/model/Class.Featured.php');
 			// this is only needed if logged user is admin or staff
 			if ($this->isAdmin || $this->isStaff){ 				
 				$this->featuredJournalsAr = Featured::getFeaturedTravelIDsInGroup($this->subjectID); 				
 			}
 		}		
 		
 		function getResultsCount() {
 			// NOTE: This is a Hack. If caching is enabled it will always return 1 which might produce errors if outside code depends on the actual no. of journals  
 			if (!$this->enableCaching)
 				return count($this->journals);
 			else {
 				return 1;
 			}	
 		}	
		
	}	
	
		
	/**
	 * JournalAuthorUIHelper
	 * 		- a utility class that wraps the author of a Journal(or Travel) and makes it easy to extract its information
	 * 			without doing ifs
	 * 		- this is useful in putting information in the templates for journals where we need the names, photos
	 * 			links etc about an author of a journal
	 */
	class JournalAuthorUIHelper {
		protected $author = null;
		
		protected $stringInfo = array('OWNER'=>'','OWNER_URL'=>'','PHOTO_THUMBNAIL_URL'=>'');
		protected $objectInfo = array();
		
		function JournalAuthorUIHelper($author){
			// $author should be of type Traveler or AdminGroup
			if (!$author instanceof Traveler && !$author instanceof AdminGroup) {
				throw new Exception('constructor argument should be an instance of Travel or AdminGroup');
			}	
								
			$this->author = $author;
			
			$this->extractInfo();
		}
		
		// get string output given a key name ex. getString('OWNER'), getString('OWNER_URL') etc.
		function getString($name){
			if (array_key_exists($name,$this->stringInfo))
				return $this->stringInfo[$name];
			else
				return 'does not exist';	
		}
		
		// get a object given a key name
		function getObject($name){
			if (array_key_exists($name,$this->objectInfo))
				return $this->objectInfo[$name];
			else
				return 'does not exist';
		}
		
		
		
		/**
		 * worker functions
		 * 
		 */
		 
		 	// extract info router	
			 private function extractInfo() {
				switch (get_class($this->author)){
					case 'Traveler':
						$this->extractTravelerInfo();
						break;
					case 'AdminGroup':
						$this->extractAdminGroupInfo();
						break;	
				}			 	
			 }
			 
			 // extract traveler info
			 private function extractTravelerInfo(){
			 	$a = $this->author;
			 	$this->stringInfo['OWNER'] = $a->getUserName(); 
			 	$this->stringInfo['OWNER_URL'] = '/' . $a->getUserName();
			 	$this->stringInfo['PHOTO_THUMBNAIL_URL'] = $a->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail');
			 }
			 
			 // extract admingroup info
			 private function extractAdminGroupInfo(){
			 	$a = $this->author;
			 	$this->stringInfo['OWNER'] = $a->getName(); 
			 	$this->stringInfo['OWNER_URL'] = '/group.php?gID=' .  $a->getGroupID();
			 	$this->stringInfo['PHOTO_THUMBNAIL_URL'] = $a->getGroupPhoto()->getPhotoLink('thumbnail');
			 }
		 
		/**
		 * end - worker functions
		 */	
		
	}
	
	
	/****************************************
	 * Interface ProcessStepper
	 * 		- convenience interface for this component to encapsulate the sequence of steps of processing the journals
	 * 				ex. $this->retrieveJournals(),$this->filterJournals(),$this->retrieveApprovedJournals()					
	 */
	 
	 interface ProcessStepperT {
	 	function performSteps();
	 }
	 
	 
	/*************
	 * class JournalAuthorProcessStepper
	 * 		- holds the steps of processing the journals for Traveler or Group context
	 * 
	 */
	 class JournalAuthorProcessStepper implements ProcessStepperT {
	 	private $jComp;			// the journals component
	 	
	 	function JournalAuthorProcessStepper(JournalsComp $jComp){
	 		$this->jComp = $jComp;
	 	}
	 	
	 	function performSteps() {
	 		$this->retrieveJournals();							// retrieve all journals for group,traveler
			$this->filterJournals();
			$this->retrieveApprovedJournals();					// get approved journal ids							
			$this->retrieveJournalEntries();			
			$this->filterJournalsWithoutEntries();			
			$this->retrieveFeaturedJournals();					// get featured journal ids						
			$this->filterJournalEntries();			
			$this->retrieveJournalPhotos();						
			$this->configureTabs();			
			$this->configureJournalControls();						
			$this->configurePaging();							// create paging comp and remove journals not to be displayed in a page
	 	}
	 } 
	
	/*********************
	 * class ViewAllJournalsProcessStepper
	 * 		- holds the steps of processing the journals for all travelers or group
	 * 
	 */	
	class ViewAllJournalsProcessStepper implements ProcessStepperT {
		private $jComp;			// the journals component
		
		function ViewAllJournalsProcessStepper(JournalsComp $jComp){
			$this->jComp = $jComp;
		}
		
		function performSteps(){
			$this->retrieveJournals();							// retrieve all journals for group,traveler
			$this->filterJournals();
			$this->retrieveApprovedJournals();					// get approved journal ids							
			$this->retrieveJournalEntries();			
			$this->filterJournalsWithoutEntries();
			$this->configurePaging();							// create paging comp and remove journals not to be displayed in a page
						
			$this->retrieveFeaturedJournals();					// get featured journal ids						
			$this->filterJournalEntries();			
			$this->retrieveJournalPhotos();						
			$this->configureTabs();			
			$this->configureJournalControls();						
			
		}
		
	}	
 
 
 
 
 
?>
