<?php
/*
 * Created on 10 9, 2006
 * @author Kerwin Gordo
 * Purpose: template for group privacy preference
 */
 
  //Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
  //Template::includeDependentJs("/js/jquery-1.1.4.pack.js");	
  Template::includeDependentJs('/min/f=js/prototype.js');
  Template::includeDependentJs('/js/interactive.form.js'); 
  
  /********************************************************************
   * edits of neri:
   * 	added radio buttons for receive GA updates:		01-09-09
   * 	displayed the profile component:				11-06-08
   ********************************************************************/
  
  echo $profile->render();
  $snav->show();
?>
 
 <div id="content_wrapper" class="layout_2">


	<div id="wide_column">
		<div class="section">
			<h2><span><?=$group->getName()?>&nbsp;Privacy Preferences</span></h2>
			
			<? if (isset($message)) : ?>
				<div class="confirmation">
				    <p><?=$message ?></p>
				</div>
			<? endif; ?>
			
			<div class="content">
			<form name="privacyform" action="group-privacy.php?action=updateprivacy" method="post" class="interactive_form">
					<ul class="form" style="clear:none">
					
				
					<li>
						<fieldset class="choices">
							<legend><span>News</span></legend>
							<ul>
							<li><input type="radio" name="bulletin" value="public" <?if ($gpp->getViewBulletin()) echo 'CHECKED';  ?>  >Show to public</input></li>
							<li><input type="radio" name="bulletin" value="members" <?if (!$gpp->getViewBulletin()) echo 'CHECKED';  ?>  >Show only to members</input></li>
							</ul>
						</fieldset>
					</li>
		
					<li>
						<fieldset class="choices">
							<legend><span>Calendar</span></legend>
							<ul>
							<li><input type="radio" name="calendar" value="public" <?if ($gpp->getViewCalendar()) echo 'CHECKED';  ?>  >Show to public</input></li>
							<li><input type="radio" name="calendar" value="members" <?if (!$gpp->getViewCalendar()) echo 'CHECKED';  ?>  >Show only to members</input></li>		
							</ul>
						</fieldset>
					</li>
		<? if ('AdminGroup' ==  get_class($group)) :?>
					<li>
						<fieldset class="choices">
							<legend><span>Downloadable Files</span></legend>
							<ul>
							<li><input type="radio" name="files" value="public" <?if ($gpp->getDownloadFiles()) echo 'CHECKED';  ?> >Show to public</input></li>
							<li><input type="radio" name="files" value="members" <?if (!$gpp->getDownloadFiles()) echo 'CHECKED';  ?> >Show only to members</input></li>
							</ul>
						</fieldset>
					</li>
			<? if (!$group->isSubGroup()) :?>		
					<li>
						<fieldset class="choices">
							<legend><span>Groups</span></legend>
							<ul>
							<li><input type="radio" name="groups" value="public" <?if ($gpp->getViewGroups()) echo 'CHECKED';  ?>  >Show to public</input></li>
							<li><input type="radio" name="groups" value="members" <?if (!$gpp->getViewGroups()) echo 'CHECKED';  ?>  >Show only to members</input></li>
							</ul>
						</fieldset>
					</li>
					<li>
						<fieldset class="choices">
							<legend><span>Member's Journals</span></legend>
							<ul>
							<li><input type="radio" name="journals" value="automatic" <?if ($gpp->isAutoModeGroupJournals()) echo 'CHECKED';  ?> >Automatic approve and feature</input></li>
							<li><input type="radio" name="journals" value="manual" <?if (!$gpp->isAutoModeGroupJournals()) echo 'CHECKED';  ?> >Manual approve and feature</input></li>
							</ul>
						</fieldset>
					</li>
			<? endif;?>	
					
					<!--li>
						<fieldset class="choices">
							<legend><span>Receive GoAbroad Network Updates</span></legend>
							<ul>
							<li><input type="radio" name="updates" value="receive" <?/*if (5 == $gpp->getModeOfReceivingUpdates()) echo 'CHECKED';  ?> >Yes</input></li>
							<li><input type="radio" name="updates" value="dont_receive" <?if (6 == $gpp->getModeOfReceivingUpdates()) echo 'CHECKED';*/  ?> >No</input></li>		
							</ul>
						</fieldset>
					</li-->
		<? endif;?>	
					
		<li class="actions">
		<input type="hidden" name="groupID" value="<?=$group->getGroupID()?>" >   <input type="submit" value="Update group privacy settings" class="submit" ></li>
	</ul>
				<div class="clear"></div>
				</form>
			</div>
			<div class="foot"></div>
		</div>
	</div>
	
	<div class="clear"></div>
</div>