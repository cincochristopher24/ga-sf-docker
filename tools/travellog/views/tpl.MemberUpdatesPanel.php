<?php
/**
 * Created on Apr.16.07
 *
 * @author Daphne 
 * Purpose:  template for View Members Panel (Members, Pending Requests, Pending Invites)
 */
  
?>


<? if (GroupMembersPanel::$MEMUPDATES == $memberTabType ) : ?>
		
 		<? $factory    =  SendableFactory::instance(); ?>
 		<ul>
			<?  foreach ($grpMembershipUpdates as $eachupdate) : 
					$eachmember =  $eachupdate['traveler']->getTravelerProfile();
					$sendable   =  $factory->create($eachupdate['source']);
			?>
				<li>
						<? if ('Traveler' == get_class($sendable) ) : ?> 
							<a href="/<?=$eachmember->getUserName()?>" class="username" >	
								<?=$eachmember->getUserName()?>		
							</a>
							has 
							
							<? if (1 == $eachupdate['status']){ ?>
								joined
							<? }  elseif (0 == $eachupdate['status']){ ?>
								left
							<? } ?>
						
						<? elseif ('Group' == get_class($sendable) ) : ?>    
							Group Admin has removed 
							<a href="/<?=$eachmember->getUserName()?>" >	
								<?=$eachmember->getUserName()?>		
							</a>
							from
						<? endif; ?>
							
						this group on 
							
						<?=date("Y-m-d h:m:s", strtotime($eachupdate['date']) )?>
						
					
				</li>
			<? endforeach; ?>
		</ul>

<? elseif (GroupMembersPanel::$DENIEDINVITES == $memberTabType ) : ?>
		<ul>
			
			<?  for($idx=0;$idx<count($deniedInviteList);$idx++) :
					$eachdeniedinvite = $deniedInviteList[$idx];
			?>
			<li >
				<a href="/<?=$eachdeniedinvite->getUserName()?>" class="username"><?=$eachdeniedinvite->getUserName()?></a>
				-
				<a href="members.php?mode=invite&amp;travelerID=<?=$eachdeniedinvite->getTravelerID()?>&amp;gID=<?=$grpID?>&amp;confirm" >Invite</a>
				<a href="members.php?mode=delete&travelerID=<?=$eachdeniedinvite->getTravelerID()?>&gID=<?=$grpID?>">| Cancel Invitation</a>
			</li>
			<?endfor; ?>					
		</ul>
<? endif; ?>

  <p class="help_text"><span>  	
  	 <? if (self::$ALLMEMBERS ==  $memberTabType){ 		  	   
	  	   $ht = new HelpText();
		   echo nl2br($ht->getHelpText('group-messagecenter'));
	 	} 
	 	
	 	if (self::$MEMBERS == $memberTabType) {
	 		$ht = new HelpText();
		   echo nl2br($ht->getHelpText('group-bulletin'));
	 	} 
	 	
	 	if (self::$PENDINGINVITES == $memberTabType) {
	 		$ht = new HelpText();
		   echo nl2br($ht->getHelpText('group-alerts'));
	 	} 
	 	
	 	if (self::$PENDINGREQUESTS == $memberTabType) {
	 		$ht = new HelpText();
		   echo nl2br($ht->getHelpText('group-news'));
	 	} 
	 	  	 	
	  ?>
	</span>
  </p>
  