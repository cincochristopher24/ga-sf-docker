<?php
	$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	tinyMCE.init({
		mode: "none",
	  	theme: "advanced",
	  	theme_advanced_toolbar_location: "top",
	  	theme_advanced_toolbar_align: "left",
	  	theme_advanced_buttons1: "bold,italic,underline",
	  	theme_advanced_buttons2: "", 
	  	theme_advanced_buttons3: "",
		valid_elements : "b[],strong[],i[],em[],u[],p[]",
		cleanup_on_startup: true,
		force_p_newlines: true,
		force_br_newlines: false,
		cleanup_callback: "custom_callback"
	});
	
	function custom_callback(type, value){
		switch (type) {
			case "get_from_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s)*<\/p>)/gi,'');
				break;
			case "insert_to_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s)*<\/p>)/gi,'');
				break;
			case "submit_content":
				break;
			case "get_from_editor_dom":
				break;
			case "insert_to_editor_dom":
				break;
			case "setup_content_dom":
				break;
			case "submit_content_dom":
				break;
		}
	
		return value;
	}

	jQuery(document).ready(function(){
		jQuery.initDate();
		EntryManager.cityID = $cityID;
		EntryManager.draftID = $draftID;
		EntryManager.resetCities();
		tinyMCE.execCommand( 'mceAddControl', false, 'description' );
	});
//]]>  
</script>
BOF;
	
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::setMainTemplateVar('layoutID', 'journals');	
	
	Template::includeDependentJs('/min/f=js/prototype.js', array('bottom'=>true));
	Template::includeDependentJs('/js/moo1.ajax.js', array('bottom'=>true));
	Template::includeDependentJs('/js/travellogManager.js', array('bottom'=>true));
	Template::includeDependentJs('/js/utils/Class.Request.js', array('bottom'=>true));
	Template::includeDependentJs('/js/modaldbox.js', array('bottom'=>true));
	Template::includeDependentJs('/js/minified_date1.0.js', array('bottom'=>true));    
	Template::includeDependentJs('/js/minified_jquery.entry-1.2.js', array('bottom'=>true));
	Template::includeDependentJs('/js/tiny_mce/tiny_mce.js', array('bottom'=>true));
	Template::includeDependentJs('/js/interactive.form.js', array('bottom'=>true));
	Template::includeDependent($code, array('bottom'=>true));
	Template::includeDependent($props['obj_map']->getJsToInclude(), array('bottom'=>true));
	Template::includeDependentJs('/js/addnewcity.js', array('bottom'=>true));
?>

<?php echo $props['profile']->render(); ?>

<?php echo $props['sub_nav']->show(); ?>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">
		<?php if ($props['show_auto_save_alert']): ?>
			<div class="infoBox">
				<p>		
					<strong>
						There <?php echo (1 < $props['unfinished_entries_count']) ? 'are '.$props['unfinished_entries_count'] : 'is an ';  ?> 
						unfinished entr<?php echo (1 < $props['unfinished_entries_count']) ? 'ies' : 'y'; ?>
						<a href="<?php echo $props['view_drafts_link']; ?>">click here to view.</a>
					</strong>
				</p>
			</div>
		<?php endif; ?>
    
    	<div id="loader" class="jRight" style="display:none;">
    		<img src="/images/loading_small.gif" border="0" align="left"><span id="auto_message">Autosaving in Progress</span>
    	</div>
        
        <div class="formbox">
        	<h1>Create Journal Entry for <em>&#8220;<?php echo $props['journal']->getTitle(); ?>&#8221;</em></h1>
								
					<form name="sform" action="/journal-entry.php" method="post" class="interactive_form" id="sform">
						<ul class="form">	
							<li>	
								<label for="title">Title of Journal Entry<span class="required">*</span></label>
								<input type="text" name="title" id="title" size="40" value="<?php echo $props['draft']->getTitle(); ?>" class="text big" />
							</li>
							
							<li class="labeled">
								<fieldset>
									<legend><span>Location of Journal Entry</span></legend>
									<span class="labeled_input_wrapper">
									    <select name="countryID" id="countryID" onchange="EntryManager.resetCities();" >
									    	<?foreach( $col_countries as $obj_country ):?>	
									    		<option value="<?=$obj_country->getLocationID()?>" <?php if ($props['draft']->getCountryID() == $obj_country->getLocationID()):?>selected="true"<?endif;?>><?=$obj_country->getName()?></option>
									    	<?endforeach;?>	
									    </select>
										<label for="countryID">Country</label>
								    </span>
									<span class="labeled_input_wrapper">
										<span id="citylist">
											<select name="cityID" id="cityID" onchange="jQuery.triggerLocation(this);"> 
												<option value="0">None</option>    
											</select>
										</span>
										<label for="citylist" id="lblCityName">City</label>
									</span>
								</fieldset>
							</li>
							<li>
								<input type="checkbox" name="current_location" id="current_location" class="checkbox" />This is my current location.
							</li>
							
							<li>	
								<label for="arrivaldate">Date of Journal Entry</label>
								<input style="width:109px;" type="text" readonly="true" class="date-pick" name="arrival" id="arrival" value="<?php echo $props['draft']->getLogDate("Y-m-d"); ?>"/>
							</li>
							<li>	
								<label for="callout">Callout:</label>
								<p class="supplement">
									A Callout is the highlight sentence(s) of your journal entry. You may  
									select a sentence or two from your journal entry and copy them here.  
									This callout may be the highlight of your experience, or it could be  
									your entry in a nutshell. On the journal page, the callout will be  
									displayed in bigger text and will be placed in quotes.
								</p>
								<input type="text" name="callout" id="callout" size="55" maxlength="250" value="<?php echo $props['draft']->getCallout(); ?>" class="text big" />
							</li>
							<li style="padding-right: 12px">
								<label for="description">Please write your Journal Entry here<span class="required">*</span></label>
								<p class="supplement">
									To prevent losing your entry, we encourage you to write from a text editor then copy your text here.
								</p>
								<textarea name="description" id="description" rows="25" cols="75"><?php echo $props['draft']->getDescription(); ?></textarea>
							</li>
							
							<li class="jLeft">
								<input type="button" name="action" id="action" value="Save as Draft" class="button_v3" onclick="EntryManager.saveAsDraft();" />
							</li> 
							
							<li id="regbutton" class="actions jRight">	
								<a href="<?php echo $back; ?>" class="jLeft button_v3" style="margin-right:10px;"><strong>Go Back</strong></a>
								<input type="submit" value="Create Entry" class="button_v3"  />
								<input type="hidden" name="action" id="action" value="publish entry" />
								<input type="hidden" name="travelID"    id="travelID"    value="<?php echo $props['draft']->getTravelID(); ?>" />
								<input type="hidden" name="travellogID" id="travellogID" value="0" />
								<input type="hidden" name="draftID" id="draftID" value="<?php echo $props['draft']->getJournalEntryDraftID(); ?>" />
							</li>
					</ul>
				<div class="clear"></div>
			</form>
		</div>
  </div>
  
	<div id="narrow_column">
		<div class="helpextra"></div>
		<div class="helpbox">
			<h2>What is a Journal?</h2>
			<p>Your Journal is like a book where you record your experiences and trip photos, and the journal entries
are like pages of this book. You may use one Journal for your entire trip and add a series of journal
entries for all its details.
			</p>

			<h2>What is Callout</h2>
			<p>May be the highlights of your experience, or it could be your entry in a nutshell. It will be displayed in bigger text and will be placed in quotes.</p>
			
			<h2>What is Autosaving?</h2>
			<p>Most travelers who have used internet cafes or public Wifi on the road know how unpredictable that can be. This feature will save your work every 5 minutes when you're writing journal entries.</p>		
		</div>
	</div>
 
	<div id = "add_city_box" class="dialog">
		<?php 
				include 'addNewCity.php';
		?>
	</div>

<div class="clear"></div>

</div>