<?php
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::includeDependentJs('/js/journalEntryDraftManager.js');
	Template::setMainTemplateVar('layoutID', 'journals');
?>

<?php echo $profile->render(); ?>
<?php echo $sub_navigation->show(); ?>

<div id="content_wrapper">
	
	<div id="wide_column" class="layout_2">
		
		<div class="formbox">
			<h1>Drafts</h1>
			<?php foreach ($journals as $journal): ?>
				<div class="draftBox">
					<div class="draftJournals" id="view_journal_<?php echo $journal->getTravelID(); ?>" >Drafts under "<span><?php echo $journal->getTitle(); ?></span>"</div>	
					<?php $drafts = $journal->getJournalEntryDrafts(); ?>
					<input type="hidden" id="journal_count<?php echo $journal->getTravelID(); ?>" value="<?php echo count($drafts); ?>" />
					<ul>
						<?php foreach ($drafts as $draft): ?>
							<?php $action = "CustomPopup.initialize('Delete Journal Entry Draft ?','Are you sure you want to delete journal entry draft {$draft->getTitle()} ?', function(){ manager.deleteEntryDraft({$draft->getJournalEntryDraftID()}, {$journal->getTravelID()}) },'Remove','1');CustomPopup.setJS();CustomPopup.createPopup();"; ?>
							<li class="draftEntry" id="draftEntry<?php echo $draft->getJournalEntryDraftID(); ?>" >
								<a href="javascript:void(0);" class="delete" onclick="<?php echo $action; ?>">
									<img src="/images/s_close.gif" border="0" align="left">
								</a>
								<a class="jLeft" href="/journal_draft/view_draft/<?php echo $draft->getJournalEntryDraftID(); ?>">
									<?php echo $draft->getTitle(); ?>
									<?php if (1 == $draft->getIsAutoSaved()): ?>
										(unfinished entry)
									<?php endif; ?>
								</a>
								<strong class="jRight">
									<a href="/journal_draft/view_draft/<?php echo $draft->getJournalEntryDraftID(); ?>">edit draft</a>
								</strong>
								
								<div class="draftDate jRight">
									<?php echo $draft->getDateCreated("M d, Y / h:i a"); ?>
								</div>  
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endforeach; ?>
			
			<div class="jRight">
				<input type="button" id="action" value="Go Back" class="button_v3" onclick="window.location.href = '<?=$back?>'; " />
			</div>
			
		</div>
		
	</div>
	
	<div id="narrow_column">
		<div class="helpextra"></div>
		
		<div class="helpbox">
			<h2>What is a Journal?</h2>
			<p>Your Journal is like a book where you record your experiences and trip photos, and the journal entries
are like pages of this book. You may use one Journal for your entire trip and add a series of journal
entries for all its details.</p>
			<h2>What is Autosaving?</h2>
			<p>Most travelers who have used internet cafes or public Wifi on the road know how unpredictable that can be. This feature will save your work every 5 minutes when you're writing journal entries.</p>
		</div>
	
	</div>
	
</div>