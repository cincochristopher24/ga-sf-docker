<?php
/*
 * Created on Aug 10, 2006
 * Tpl.ProfileLink.php - a photo, add/edit profile link
 */

 /* Edited on Oct 16 2008 
	- Redesign Markup Alignment
 */
	
?>

<!-- Begin intro: Serves as wrapper of Traveler Profile -->
<div id="intro">
	
					
	<div id="photo">
		<a class="profile_photo" href="<?= htmlspecialchars($viewProfileLink) ?>" title="View Profile" >									
				<img src="<?= $profileImage ?>" alt="profile photo" width="150" height="200" class="profile_photo" />				
		</a>
	</div>				
	
	<h1 id="traveler_name">
		<? if (strlen($username) > 8): ?>
				<?= substr($username,0, 8) ?>...
		<? else: ?>	
				<?=$username?>
		<?endif; ?>										
		<span> Full Name </span>																									
	</h1>							
	
	<!-- Begin Details	 -->
		<div id="details">																					

				 <!-- Begin Profile -->
				<ul class="profile">													
						<li>
							<strong>Age:</strong> 
							<?=$age?>
						</li>																		
						<li>
							<strong>Traveler Type:</strong>
							Artist-Traveler	
						</li>
						<li>
							<strong>Hometown:</strong>
							<?=$hometown?>	
						</li>																	
						<li>
							<strong>Currently in: </strong>										
							<? if (NULL != $location ) :?>
								<?=$location->getName() ?>
							<? endif; ?>
						</li>																							
				</ul>
				<!-- End Profile -->

				<!-- Begin Profile Actions -->
				<div id="profile_actions">
													
						<ul id="actions" class="actions"> 
							<li class="first">
								<a href="<?= htmlspecialchars($editProfileLink) ?>">Edit Profile</a>
							</li>
							<li class="last">
								<a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(1,1,'gallery')">Manage Profile Photos</a>
							</li>										
							<li class="first last" id="action_privacy_pref">
								<a href="<?= htmlspecialchars($privacyPreferencesLink) ?>"> Privacy Preferences</a>
							</li>
							
							<li class="first last" id="action_tell_others">
								<a href="<?=$inviteFriendsLink ?>"> Tell everyone about the GoAbroad Network!</a>
							</li>																																																												
						</ul> 																																
				</div>
				<!-- End Profile Actions -->	
				
				<?/* TODO: NOTIFICATIONS ON PROFILE 
					if ( ($messagesLink !== '') || isset($frLink) || isset($griLink) || isset($grrLink) || isset($dgrrLink) || (isset($commentsLink) && trim($commentsLink) !== '') || isset($surveyFormsLink)): 
				*/?>											
			
	   </div>
	   <!-- End Details -->				
	

		<!-- Begin Travel Bio -->
		<div id="bubble_philosophy">
				<strong> What do you want to do the next time you travel?</strong>
				<?php /*
				<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
					laboris nisi ut aliquip ex ea commodo consequat" 
				</p>
				*/ ?>
				<a href="#" id="travelbio_link"> More from </a>								
		</div>							
		<!-- End Travel Bio -->										
</div>
<!-- End intro -->					

	
