<?php
	/**
	 * Created on 16 August 2007
	 * Class.PokesView.php
	 * @author Cheryl Ivy Q. Go
	 */

	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.Poke.php';
	require_once 'travellog/model/Class.PokeType.php';
	require_once 'travellog/model/Class.FilterCriteria2.php';

	class PokesView{

		protected $mContext		= 0;
		protected $mContextId	= 0;
		protected $mVisitorId	= 0;
		protected $mAuthorId	= 0;
		protected $mOwnerId		= 0;
		protected $mPokeTypeId	= 0;
		protected $mOwner		= '';
		protected $mAuthor	= '';
		protected $mAuthorObj = null;
		protected $mPath		= '';
		protected $mOwnerPath	= '';
		protected $mAuthorPath	= '';
		protected $mUserType	= 0;
		protected $mComment		= '';
		protected $mMessage		= '';
		protected $mHasMessage	= false;

		function PokesView(){}

		/**
		 * Setters
		 */

		function setVisitorId($_visitorId = 0){
			$this->mVisitorId = $_visitorId;
		}

		function setAuthorId($_authorId = 0){
			$this->mAuthorId = $_authorId;
		}

		function setOwnerId($_ownerId = 0){
			$this->mOwnerId = $_ownerId;
		}

		function setPokeTypeId($_pId = 0){
			$this->mPokeTypeId = $_pId;
		}

		function setContextId($_contextId = 0){
			$this->mContextId = $_contextId;
		}

		function setMessage($_message = ''){
			$this->mMessage = $_message;
		}
		
		/**
		 * Sets the author object of this poke and its corresponding comment.
		 * 
		 * @param CommentAuthor $author
		 */
		function setAuthorObject($author){
			$this->mAuthorObj = $author;
		}
		
		/*
		* EDITS: transferres TravelerProfile instantiation since its only used if comment type is not equal to article/travellog
		*/
		function setOwner(){
			if(CommentType::$TRAVELLOG == $this->mContext){
				$entry = new Travellog($this->mContextId);
				if($entry->getTravel()->isGroupJournal()){
					$ag = $entry->getTravel()->getOwner();
					$this->mOwner = $ag->getName();
					$this-> mOwnerPath = "<a href='".$ag->getFriendlyURL()."'>".$this->mOwner."</a>";
					return;
				}
			}
			if(CommentType::$ARTICLE == $this->mContext){
				$article = new Article($this->mContextId);
				if( 0 < $article->getGroupID() ){
					$ag = new AdminGroup($article->getGroupID());
					$this->mOwner = $ag->getName();
					$this-> mOwnerPath = "<a href='".$ag->getFriendlyURL()."'>".$this->mOwner."</a>";
					return;
				}
			}
			
			try{
				$tp = new TravelerProfile($this->mOwnerId);
				$this->mOwner = $tp->getUserName();
			}catch(exception $e){}
			
			
			if (0 < $this->mVisitorId AND Traveler::isMemberOfAnyOfMyGroup($this->mVisitorId, $this->mAuthorId)) {
				$this->mOwnerPath = '<a href="http://'.$_SERVER['HTTP_HOST'].'/'.$this->mOwner.'">'.$this->mOwner.'</a> <em>'.$this->mOwner.'</em>';
			}
			else {
				$this->mOwnerPath = '<a href="http://'.$_SERVER['HTTP_HOST'].'/'.$this->mOwner.'">'.$this->mOwner.'</a>';
			}
			
		}
		
    /**
     * Sets the author path and the author name attributes of this class.
     * 
     * Note: modified so that it can handle comments of non-GANET users.
     * Modified by: Antonio Pepito Cruda Jr. - Dec. 17, 2008
     */
		function setAuthor(){
			$this->mAuthor = $this->mAuthorObj->getName();
			$this->mAuthorPath = "<span style = 'color:#1387C4;'>".$this->mAuthor."</span>";
			
			$this->mAuthorId = $this->mAuthorObj->getAuthorID();
			
			switch($this->mAuthorObj->getAuthorType()){
		  	    case CommentAuthor::TRAVELER:
		  	    	$traveler = new TravelerProfile($this->mAuthorId);
					if (0 < $this->mVisitorId AND Traveler::isMemberOfAnyOfMyGroup($this->mVisitorId, $this->mAuthorId)) {
						$this->mAuthorPath = "<a href='/".$this->mAuthor."'>".$this->mAuthor."</a> <em>".$traveler->getName()."</em>";
					}
					else {
						$this->mAuthorPath = "<a href='/".$this->mAuthor."'>".$this->mAuthor."</a>";
					}  
		  	    	break;
		  	    case CommentAuthor::ADMIN_GROUP:
					/*$ag = $this->mAuthorObj->getAuthorObject();
					if(CommentType::$TRAVELLOG == $this->mContext){
						$entry = new Travellog($this->mContextId);
						if($entry->getTravel()->isGroupJournal()){
							$gID = $ag->getGroupID();
							if ($gID > 0 ){
								$this->mAuthor = $ag->getName();
								$this-> mAuthorPath = "<a href='".$ag->getFriendlyURL()."'>".$this->mAuthor."</a>";
								return;
							}
						}
					}*/
					if( $this->mAuthorId ){
						$this-> mAuthorPath = "<a href='/".$this->mAuthor."'>".$this->mAuthor."</a>";     
		  	    	}
					break;
				default:
					if( $this->mAuthorId ){
						$this-> mAuthorPath = "<a href='/".$this->mAuthor."'>".$this->mAuthor."</a>";     
		  	    	}
					break;
			}
		}

		function setContext($_context = 0){
			$this-> mContext = $_context;
			$Photo = new Photo(null, $this->mContextId);
			switch ($_context) {
				case CommentType::$PROFILE:
					$owner = new Traveler($this->mOwnerId );
					$link = $owner->getUserName();
					$this-> mPath = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $link;
					break;
				case CommentType::$PHOTO:					
					if ($Photo->getPhotoTypeID() == PhotoType::$PHOTOALBUM){
						$grpId = $Photo->getContext()->getGroupID();
						$albumId = $Photo->getContext()->getPhotoAlbumID();
						$link = 'photomanagement.php?action=vfullsize&cat=photoalbum&groupID=' . $grpId . '&genID=' . $albumId . '&photoID=' . $this->mContextId;
					}
					elseif ($Photo->getPhotoTypeID() == PhotoType::$PROFILE){
						$ownerId = $Photo->getContext()->getTravelerID();
						$link = 'photomanagement.php?action=vfullsize&cat=profile&genID=' . $ownerId . '&photoID=' . $this-> mContextId;
					}
					elseif ($Photo->getPhotoTypeID() == PhotoType::$TRAVELLOG){
						$travellog 		= new TravelLog($Photo->getContext()->getTravelLogID());	
						$link = 'journal-entry.php?action=view&travellogID=' . $travellog->getTravelLogID();
					}
					$this-> mPath = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $link;
					break;				
				case CommentType::$TRAVELLOG:
					$travellog 		= new TravelLog($this->mContextId);
					$link = 'journal-entry.php?action=view&travellogID=' . $travellog->getTravelLogID();

					$this-> mPath = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $link;
					break;
				case CommentType::$ARTICLE:
					$article 		= new Article($this->mContextId);
					$link = 'article.php?action=view&articleID=' . $article->getArticleID();

					$this-> mPath = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $link;
					break;	
 			}
		}

		function setHasMessage($_bool = false){
			$this-> mHasMessage = $_bool;
		}

		/**
		 * Getters
		 */

		public function getAuthorName() {
			return $this->mAuthor;
		}
		
		public function getAuthorFullName() {
			return $this->mAuthorObj->getName();
		}
		
		function getMessage(){
			$this->setAuthor();
			$this->setOwner();

			switch($this->mContext){
				case CommentType::$PROFILE:		// PROFILE
					$this-> getProfileMessage();
					break;
				case CommentType::$PHOTO:		// PHOTO
					$this-> getPhotoMessage();
					break;
				case CommentType::$TRAVELLOG: 	// JOURNAL ENTRY
					$this-> getTravellogMessage();
					break;
				case CommentType::$ARTICLE: 	// JOURNAL ENTRY
					$this-> getArticleMessage();
					break;	
			}
			return $this-> mComment;
		}

		function getProfileMessage(){
			$this->mAuthorPath = "<span style = 'color:#1387C4;'>".$this->mAuthorPath."</span>";
			switch($this->mPokeTypeId){
				case PokeType::$SMILE:
					if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you smiled';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// author but not owner
	 					$this-> mComment =  'you smiled at ' . $this->mOwnerPath;
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' smiled at you';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' smiled';
	 					else
	 						$this-> mComment = $this->mAuthorPath . ' smiled at ' . $this->mOwnerPath;
	 				}
	 				break;
	 			case PokeType::$THUMBSUP:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you gave a thumbs up';
	 				elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// author but not owner
	 					$this-> mComment =  'you gave ' . $this->mOwnerPath . ' a thumbs up';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' gave you a thumbs up';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this-> mAuthorPath . ' gave a thumbs up';
	 					else
	 						$this-> mComment = $this->mAuthorPath . ' gave ' . $this->mOwnerPath . ' a thumbs up';
	 				}
	 				break;
	 			case PokeType::$CHEERSMATE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId) && $this->mAuthorId <> 0)			// author and owner
	 					$this-> mComment =  'you said, "Cheers, mate!"';
	 				elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId) && $this->mAuthorId <> 0)			// author but not owner
	 					$this-> mComment =  'you said, "Cheers, mate!"';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId) && $this->mAuthorId <> 0)		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' says, "Cheers, mate!"';
	 				else																						// neither author nor owner	 						
	 					$this-> mComment = $this->mAuthorPath . ' says, "Cheers, mate!"';
	 				break;
	 			case PokeType::$THAIWAI:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you gave a Thai wai';
	 				elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you greeted ' . $this->mOwnerPath . ' with a Thai wai';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' greeted you with a Thai wai';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' gave a Thai wai';
	 					else
	 						$this-> mComment = $this->mAuthorPath . ' greeted ' . $this->mOwnerPath . ' with a Thai wai';
	 				}
	 				break;
	 			case PokeType::$WAVE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you waved';
	 				elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you waved at ' . $this->mOwnerPath;
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' waved at you';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' waved';
	 					else
	 						$this-> mComment = $this->mAuthorPath . ' waved at ' . $this->mOwnerPath;
	 				}
	 				break;
	 			case PokeType::$BOWDOWN:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you bowed down';
	 				elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you greeted ' . $this->mOwnerPath . ' with a bow';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' greeted you with a bow';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' bowed down';
	 					$this-> mComment = $this->mAuthorPath . ' greeted with a bow';
	 				}
	 				break;
	 			case PokeType::$CONGRATULATE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you said, "Congratulations!"';
	 				elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you congratulated ' . $this->mOwnerPath . '!';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' congratulates you!';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' said, "Congratulations!"';
	 					else
	 						$this-> mComment = $this->mAuthorPath . ' congratulates ' . $this->mOwnerPath;
	 				}
	 				break;
	 			default:
	 				if ($this->mAuthorId == $this->mVisitorId)
	 					$this-> mComment = 'you wrote:';
	 				else
	 					$this-> mComment = $this->mAuthorPath . ' wrote:';
			}
		}

		function getTravellogMessage(){
			$this->mAuthorPath = "<span style = 'color:#1387C4;'>".$this->mAuthorPath."</span>";
			switch($this->mPokeTypeId){
				case PokeType::$SMILE:
					if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you smiled';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'this journal made you smile';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  'your journal made ' . $this->mAuthorPath . ' smile';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' smiled';
	 					else {
	 						if ("s" == substr($this->mOwner, -1)) {
	 							$this-> mComment = $this->mOwnerPath . "' journal made " . $this->mAuthorPath . " smile";
	 						}
	 						else {
	 							$this-> mComment = $this->mOwnerPath . "'s journal made " . $this->mAuthorPath . " smile";
	 						}
	 					}
	 				}
	 				break;
	 			case PokeType::$THUMBSUP:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you gave a thumbs up';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId)) {	// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you have read " . $this->mOwnerPath . "' journal and gave it a thumbs up";
						}	
						else {
							$this-> mComment =  "you have read " . $this->mOwnerPath . "'s journal and gave it a thumbs up";
						}				
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' has read your journal and gave it a thumbs up';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' gave a thumbs up';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " has read " . $this->mOwnerPath . "' journal and gave it a thumbs up";
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " has read " . $this->mOwnerPath . "'s journal and gave it a thumbs up";
							} 					
	 					}
	 				}
	 				break;
	 			case PokeType::$CHEERSMATE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you said, "Cheers, mate!"';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))	{		// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you have read " . $this->mOwnerPath . "'". ' journal and said, "Cheers, mate!"';
						}	
						else {
							$this-> mComment =  "you have read " . $this->mOwnerPath . "'s" . ' journal and said, "Cheers, mate!"';
						}						
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this-> mAuthorPath . ' has read your journal and says, "Cheers, mate!"';
	 				else {																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' says, "Cheers, mate!"';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " has read " . $this->mOwnerPath . "'". ' journal and says, "Cheers, mate!"';
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " has read " . $this->mOwnerPath . "'s" . ' journal and says, "Cheers, mate!"';
							}	 					
	 					}
	 				}
	 				break;
	 			case PokeType::$THAIWAI:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you gave a Thai wai';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))	{		// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you gave " . $this->mOwnerPath . "' journal a Thai wai";
						}	
						else {
							$this-> mComment =  "you gave " . $this->mOwnerPath . "'s journal a Thai wai";
						}					
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' gave your journal a Thai wai';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' gave a Thai wai';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " gave " . $this->mOwnerPath . "'". ' journal a Thai wai';
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " gave " . $this->mOwnerPath . "'s" . ' journal a Thai wai';
							}
	 					}
	 				}
	 				break;
	 			case PokeType::$WAVE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you waved';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))	{		// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you waved at " . $this->mOwnerPath . "' journal";
						}	
						else {
							$this-> mComment =  "you waved at " . $this->mOwnerPath . "'s journal";
						}					
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' waved at your journal';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' waved';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " waved at " . $this->mOwnerPath . "'" . ' journal';
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " waved at " . $this->mOwnerPath . "'s" . ' journal';
							}		 					
	 					}
	 				}
	 				break;
	 			case PokeType::$BOWDOWN:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you bowed down';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))	{		// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you bowed down to " . $this->mOwnerPath . "' trip";
						}	
						else {
							$this-> mComment =  "you bowed down to " . $this->mOwnerPath . "'s trip";
						}					
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' bows down to your trip';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' bowed down';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " bows down to " . $this->mOwnerPath . "' trip";
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " bows down to " . $this->mOwnerPath . "'s trip";
							}
	 					}
	 				}
	 				break;
	 			case PokeType::$CONGRATULATE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you said, "Congratulations!"';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you congratulated ' . $this->mOwnerPath . ' on this experience';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' congratulates you on this experience';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' said, "Congratulations!"';
	 					else
	 						$this-> mComment =$this->mAuthorPath . ' congratulates ' . $this->mOwnerPath . ' on this experience';
	 				}
	 				break;
	 			default:
	 				if ($this->mAuthorId == $this->mVisitorId)
	 					$this-> mComment = 'you wrote:';
	 				else
	 					$this-> mComment = $this->mAuthorPath . ' wrote:';
			}
		}

		function getPhotoMessage(){
			switch($this->mPokeTypeId){
				case PokeType::$SMILE:
					if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you smiled';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'this photo made you smile';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  'your photo made ' . $this->mAuthorPath . ' smile';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' smiled';
	 					else
	 						$this-> mComment = $this->mOwnerPath . "'s photo made " . $this->mAuthorPath . " smile";
	 				}
	 				break;
	 			case PokeType::$THUMBSUP:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you gave a thumbs up';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you gave this photo a thumbs up';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' has viewed your photo and gave it a thumbs up';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' gave a thumbs up';
	 					else
	 						$this-> mComment = $this->mAuthorPath . " has viewed " . $this->mOwnerPath . "'s photo and gave it a thumbs up";
	 				}
	 				break;
	 			case PokeType::$CHEERSMATE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you said, "Cheers, mate!"';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  "you have viewed " . $this->mOwnerPath . "'s" . ' photo and said, "Cheers, mate!"';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' has viewed your photo and says, "Cheers, mate!"';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' says, "Cheers, mate!"';
	 					else
	 						$this-> mComment = $this->mAuthorPath . " has viewed " . $this->mOwnerPath . "'s" . ' photo and says, "Cheers, mate!"';
	 				}
	 				break;
	 			case PokeType::$THAIWAI:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you gave a Thai wai';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  "you gave this photo a Thai wai";
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' gave your photo a Thai wai';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' gave a Thai wai';
	 					else
	 						$this-> mComment = $this->mAuthorPath . " gave " . $this->mOwnerPath . "'s" . ' photo a Thai wai';
	 				}
	 				break;
	 			case PokeType::$WAVE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you waved';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you waved at ' . $this->mOwnerPath . ' on his/her photo';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' waved at you on your photo';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' waved';
	 					else
	 						$this-> mComment = $this->mAuthorPath . ' waved at ' . $this->mOwnerPath . ' on his/her photo';
	 				}
	 				break;
	 			case PokeType::$BOWDOWN:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you bowed down';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  "you bowed down to " . $this->mOwnerPath . "'s photographing skills";
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' bows down to your photographing skills';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' bowed down';
	 					else
	 						$this-> mComment = $this->mAuthorPath . " bows down to " . $this->mOwnerPath . "'s photographing skills";
	 				}
	 				break;
	 			case PokeType::$CONGRATULATE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you said, "Congratulations!"';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you congratulated ' . $this->mOwnerPath . ' on his/her awesome photo';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' congratulates you on your awesome photo';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' said, "Congratulations!"';
	 					else
	 						$this-> mComment = $this->mAuthorPath . ' congratulates ' . $this->mOwnerPath . ' his/her awesome photo';
	 				}
	 				break;
	 			default:
	 				if ($this->mAuthorId == $this->mVisitorId)
	 					$this-> mComment = 'you wrote:';
	 				else
	 					$this-> mComment = $this->mAuthorPath . ' wrote:';
			}
		}

		function getPassportMessage(){
			switch($this->mContext){
				case 1:			// profile
					if (PokeType::$SMILE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this->mComment = $this->mAuthorPath . ' smiled at you on your <a href = "' . $this-> mPath . '">profile</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' smiled at you';
					}
					elseif (PokeType::$THUMBSUP == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' gave you a thumbs up on your <a href = "' . $this-> mPath . '">profile</a> saying: ';
						else
							$this-> mComment = $this->mAuthorPath . ' gave you a thumbs up';
					}
					elseif (PokeType::$CHEERSMATE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' says, "Cheers, mate!" on your <a href = "' . $this-> mPath . '">profile</a> and: ';
						else
							$this-> mComment = $this->mAuthorPath . ' says, "Cheers, mate!"';
					}
					elseif (PokeType::$THAIWAI == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' greeted you with a Thai wai on your <a href = "' . $this-> mPath . '">profile</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' greeted you with a Thai wai';
					}
					elseif (PokeType::$WAVE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' waved at you on your <a href = "' . $this-> mPath . '">profile</a> saying: ';
						else
							$this-> mComment = $this->mAuthorPath . ' waved at you';
					}
					elseif (PokeType::$BOWDOWN == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' greeted you with a bow and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' greeted you with a bow';
					}
					elseif (PokeType::$CONGRATULATE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' congratulates you in your <a href = "' . $this-> mPath . '">profile</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' congratulates you!';
					}
					else
						$this-> mComment = $this->mAuthorPath . ' posted a shout-out on your <a href = "' . $this-> mPath . '">profile</a> saying: ';
					break;
				case 2:			// photo
					if (PokeType::$SMILE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = 'your <a href = "' . $this-> mPath . '">photo</a> made ' . $this->mAuthorPath . ' smile. ' . $this-> mAuthor . ' also said:<div style = "margin-left: 5px;">" ' . $this->mMessage . ' "</div>';
						else
							$this-> mComment = 'your <a href = "' . $this-> mPath . '">photo</a> made ' . $this->mAuthorPath . ' smile';
					}
					elseif (PokeType::$THUMBSUP == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has viewed your <a href = "' . $this-> mPath . '">photo</a> and said:<div style = "margin-left: 5px;">" ' . $this->mMessage . ' "</div>with thumbs up';
						else
							$this-> mComment = $this->mAuthorPath . ' has viewed your <a href = "' . $this-> mPath . '">photo</a> and gave it a thumbs up';
					}
					elseif (PokeType::$CHEERSMATE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has viewed your <a href="' . $this-> mPath . '">photo</a>says, "Cheers, mate!"<div style = "margin-left: 5px;">" ' . $this->mMessage . ' "</div>';
						else
							$this-> mComment = $this->mAuthorPath . ' has viewed your <a href="' . $this-> mPath . '">photo</a> and says, "Cheers, mate!"';
					}
					elseif (PokeType::$THAIWAI == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has viewed your <a href="' . $this-> mPath . '">photo</a>, gave it a Thai Wai and said:<div style = "margin-left: 5px;">" ' . $this->mMessage . ' "</div>';
						else
							$this-> mComment = $this->mAuthorPath . ' has viewed your <a href="' . $this-> mPath . '">photo</a> and gave it a Thai Wai';
					}
					elseif (PokeType::$WAVE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' waved at you on your <a href="' . $this-> mPath . '">photo</a> and said:<div style = "margin-left: 5px;">" ' . $this->mMessage . ' "</div>';
						else
							$this-> mComment = $this->mAuthorPath . ' waved at you in your <a href="' . $this-> mPath . '">photo</a>';
					}
					elseif (PokeType::$BOWDOWN == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' bows down to your photographing skills in your <a href="' . $this-> mPath . '">photo</a> and said:<div style = "margin-left: 5px;">" ' . $this->mMessage . ' "</div>';
						else
							$this-> mComment = $this->mAuthorPath . ' bows down to your photographing skills in your <a href="' . $this-> mPath . '">photo</a>';
					}
					elseif (PokeType::$CONGRATULATE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' said:<div style = "margin-left: 5px;">" ' . $this->mMessage . ' "</div>as he/she congratulates you on your awesome <a href="' . $this-> mPath . '">photo</a>';
						else
							$this-> mComment = $this->mAuthorPath . ' congratulates you on your awesome <a href="' . $this-> mPath . '">photo</a>';
					}
					else
						$this-> mComment = $this->mAuthorPath . ' said:<div style = "margin-left: 5px;">" ' . $this->mMessage . ' "</div>on your <a href="' . $this-> mPath . '">photo</a>';
					break;
				case 3:			// journal-entry
					if (PokeType::$SMILE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = 'your <a href = "' . $this-> mPath . '">journal</a> made ' . $this->mAuthorPath . ' smile. ' . $this-> mAuthor . ' also said: ';
						else
							$this-> mComment = 'your <a href = "' . $this-> mPath . '">journal</a> made ' . $this->mAuthorPath . ' smile';
					}
					elseif (PokeType::$THUMBSUP == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">journal</a>, gave it a thumbs up and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">journal</a> and gave it a thumbs up';
					}
					elseif (PokeType::$CHEERSMATE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">journal</a>, says "Cheers, mate!" and: ';
						else
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">journal</a> and says, "Cheers, mate!"';
					}
					elseif (PokeType::$THAIWAI == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">journal</a>, gave it a Thai Wai and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">journal</a> and gave it a Thai Wai';
					}
					elseif (PokeType::$WAVE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . '  waved at you on your <a href="' . $this-> mPath . '">journal</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . '  waved at you in your <a href="' . $this-> mPath . '">journal</a>';
					}
					elseif (PokeType::$BOWDOWN == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' bows down to your <a href="' . $this-> mPath . '">trip</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' bows down to your <a href="' . $this-> mPath . '">trip</a>';
					}
					elseif (PokeType::$CONGRATULATE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' congratulates you on this <a href="' . $this-> mPath . '">experience</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' congratulates you on this <a href="' . $this-> mPath . '">experience</a>';
					}
					else
						$this-> mComment = $this->mAuthorPath . ' has read your <a href = "' . $this-> mPath . '">journal</a> and said: ';
					break;
				case 4:			// articles
					if (PokeType::$SMILE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = 'your <a href = "' . $this-> mPath . '">article</a> made ' . $this->mAuthorPath . ' smile. ' . $this-> mAuthor . ' also said: ';
						else
							$this-> mComment = 'your <a href = "' . $this-> mPath . '">article</a> made ' . $this->mAuthorPath . ' smile';
					}
					elseif (PokeType::$THUMBSUP == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">article</a>, gave it a thumbs up and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">article</a> and gave it a thumbs up';
					}
					elseif (PokeType::$CHEERSMATE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">article</a>, says "Cheers, mate!" and: ';
						else
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">article</a> and says, "Cheers, mate!"';
					}
					elseif (PokeType::$THAIWAI == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">article</a>, gave it a Thai Wai and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' has read your <a href="' . $this-> mPath . '">article</a> and gave it a Thai Wai';
					}
					elseif (PokeType::$WAVE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . '  waved at you on your <a href="' . $this-> mPath . '">article</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . '  waved at you in your <a href="' . $this-> mPath . '">article</a>';
					}
					elseif (PokeType::$BOWDOWN == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' bows down to your <a href="' . $this-> mPath . '">trip</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' bows down to your <a href="' . $this-> mPath . '">trip</a>';
					}
					elseif (PokeType::$CONGRATULATE == $this->mPokeTypeId){
						if (true == $this->mHasMessage)
							$this-> mComment = $this->mAuthorPath . ' congratulates you on this <a href="' . $this-> mPath . '">experience</a> and said: ';
						else
							$this-> mComment = $this->mAuthorPath . ' congratulates you on this <a href="' . $this-> mPath . '">experience</a>';
					}
					else
						$this-> mComment = $this->mAuthorPath . ' has read your <a href = "' . $this-> mPath . '">article</a> and said: ';
					break;
			}
			return $this-> mComment;
		}
		
		function getArticleMessage(){
			switch($this->mPokeTypeId){
				case PokeType::$SMILE:
					if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you smiled';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'this article made you smile';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  'your article made ' . $this->mAuthorPath . ' smile';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' smiled';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mOwnerPath . "' article made " . $this->mAuthorPath . " smile";
							}	
							else {
								$this-> mComment = $this->mOwnerPath . "'s article made " . $this->mAuthorPath . " smile";
							}	 					
	 					}
	 				}
	 				break;
	 			case PokeType::$THUMBSUP:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you gave a thumbs up';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))	{		// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you have read " . $this->mOwnerPath . "' article and gave it a thumbs up";
						}	
						else {
							$this-> mComment =  "you have read " . $this->mOwnerPath . "'s article and gave it a thumbs up";
						}
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' has read your article and gave it a thumbs up';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' gave a thumbs up';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " has read " . $this->mOwnerPath . "' article and gave it a thumbs up";
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " has read " . $this->mOwnerPath . "'s article and gave it a thumbs up";
							} 					
	 					}
	 				}
	 				break;
	 			case PokeType::$CHEERSMATE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you said, "Cheers, mate!"';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId)){		// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you have read " . $this->mOwnerPath . "'" . ' article and said, "Cheers, mate!"';
						}	
						else {
							$this-> mComment =  "you have read " . $this->mOwnerPath . "'s" . ' article and said, "Cheers, mate!"';
						}					
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this-> mAuthorPath . ' has read your article and says, "Cheers, mate!"';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' says, "Cheers, mate!"';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " has read " . $this->mOwnerPath . "'" . ' article and says, "Cheers, mate!"';
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " has read " . $this->mOwnerPath . "'s" . ' article and says, "Cheers, mate!"';
							}		 					
	 					}
	 				}
	 				break;
	 			case PokeType::$THAIWAI:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you gave a Thai wai';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId)) {			// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you gave " . $this->mOwnerPath . "' article a Thai wai";
						}	
						else {
							$this-> mComment =  "you gave " . $this->mOwnerPath . "'s article a Thai wai";
						}					
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' gave your article a Thai wai';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' gave a Thai wai';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " gave " . $this->mOwnerPath . "'" . ' article a Thai wai';
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " gave " . $this->mOwnerPath . "'s" . ' article a Thai wai';
							}		 					
	 					}
	 				}
	 				break;
	 			case PokeType::$WAVE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you waved';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId)) {			// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you waved at " . $this->mOwnerPath . "' article";
						}	
						else {
							$this-> mComment =  "you waved at " . $this->mOwnerPath . "'s article";
						}					
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' waved at your article';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' waved';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " waved at " . $this->mOwnerPath . "'" . ' article';
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " waved at " . $this->mOwnerPath . "'s" . ' article';
							}		 						
	 					}
	 				}
	 				break;
	 			case PokeType::$BOWDOWN:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you bowed down';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId)) {			// author but not owner
						if ("s" == substr($this->mOwner, -1)) {	
							$this-> mComment =  "you bowed down to " . $this->mOwnerPath . "' article";
						}	
						else {
							$this-> mComment =  "you bowed down to " . $this->mOwnerPath . "'s article";
						}						
					}
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' bows down to your article';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' bowed down';
	 					else {
							if ("s" == substr($this->mOwner, -1)) {	
								$this-> mComment = $this->mAuthorPath . " bows down to " . $this->mOwnerPath . "' article";
							}	
							else {
								$this-> mComment = $this->mAuthorPath . " bows down to " . $this->mOwnerPath . "'s article";
							}	 					
	 					}
	 				}
	 				break;
	 			case PokeType::$CONGRATULATE:
	 				if (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId == $this->mOwnerId))			// author and owner
	 					$this-> mComment =  'you said, "Congratulations!"';
					elseif (($this->mAuthorId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))			// author but not owner
	 					$this-> mComment =  'you congratulated ' . $this->mOwnerPath . ' on this experience';
	 				elseif (($this->mOwnerId == $this->mVisitorId) && ($this->mAuthorId != $this->mOwnerId))		// owner but not author
	 					$this-> mComment =  $this->mAuthorPath . ' congratulates you on this experience';
	 				else{																						// neither author nor owner
	 					if ($this->mOwnerId == $this->mAuthorId)
	 						$this-> mComment = $this->mAuthorPath . ' said, "Congratulations!"';
	 					else
	 						$this-> mComment =$this->mAuthorPath . ' congratulates ' . $this->mOwnerPath . ' on this experience';
	 				}
	 				break;
	 			default:
	 				if ($this->mAuthorId == $this->mVisitorId)
	 					$this-> mComment = 'you wrote:';
	 				else
	 					$this-> mComment = $this->mAuthorPath . ' wrote:';
			}
		}
		
		function getSenderPath(){
			return $this-> mAuthorPath;
		}
	}
?>