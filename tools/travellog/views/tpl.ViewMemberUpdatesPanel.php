<?php
/**
 * Created on Apr.17.07
 *
 * @author Daphne 
 * Purpose:  template for View Membership Updates / Denied Invitations Panel (Updates, Denied Invitations)
 */
  
  require_once('Class.GaDateTime.php');
  $d = new GaDateTime();
?>


<div id="mem_updates_content">
<?  if (NULL != $memUpdates) : ?> 
	<? $grpDate = '' ?>
	
	
	<?  foreach ($memUpdates as $eachupdate) : 
			$eachmember =  $eachupdate['traveler']->getTravelerProfile();
			$factory    =  SendableFactory::instance();
			$sendable   =  $factory->create($eachupdate['source']);	
			$source     =  SendableFactory::$SendableType;			
	?> 
		<? if ($grpDate != date("Y-m-d", strtotime($eachupdate['date'])) ) { ?>
			<? $grpDate = date("Y-m-d", strtotime($eachupdate['date']) ) ?>
			</ul>
			<h2><?=$d->friendlyFormat($grpDate) ?></h2>
			<ul>
		<? } ?>
			
		<li>
				<a href="/<?=$eachmember->getUserName()?>" class="username" ><?=$eachmember->getUserName()?></a>
				
				<? switch($eachupdate['actiontype']) { 
					
					case 1: echo 'joined this group'; break;
					case 2: echo 'left this group'; break;
					case 3: echo 'requested to join group'; break;
					case 4: echo 'cancelled request to join group'; break;
					case 5: echo 'accepted group invitation'; break;
					case 6: echo 'denied group invitation'; break;					
					case 7: echo 'was invited to join group'; break;
					case 8: echo '\'s invitation was cancelled by group'; break;
					case 9: echo '\'s request to join was accepted'; break;
					case 10: echo '\'s request to join was denied'; break;
					case 11: echo 'was removed from group'; break;
				
				} ?>	
			
		</li>
		<? if ($grpDate != date("Y-m-d", strtotime($eachupdate['date'])) ) { ?>
			</ul>
		<? } ?>
	<? endforeach; ?>
	</ul>
<? endif; ?>

</div>
