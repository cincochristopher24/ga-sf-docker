<?php
/**
 * <b>Activity</b> template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
//SubNavigation::displayLinks() ;
?>
<table border="1" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<?
				if ( count($props['errors']) ):
					foreach($props['errors'] as $error):
						echo $error.'<br />';	
					endforeach;
				endif; 
				echo $form;  
			?>
		</td>
	</tr>
</table>
