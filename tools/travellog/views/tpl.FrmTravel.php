<?php
$file_factory = FileFactory::getInstance();
$file_factory->getClass('HtmlHelpers');
$ctr = 0;
?>      
<div id="content_wrapper">
	<div id="wide_column" class="layout_2">
		<?php if ($props['show_auto_save_alert']): ?>
			<div class="infoBox">
				<p>		
					<strong>
						There <?php echo (1 < $props['unfinished_entries_count']) ? 'are '.$props['unfinished_entries_count'] : 'is an ';  ?> 
						unfinished entr<?php echo (1 < $props['unfinished_entries_count']) ? 'ies' : 'y'; ?>
						<a href="<?php echo $props['view_drafts_link']; ?>">click here to view.</a>
					</strong>
				</p>
			</div>
		<?php endif; ?>
				
		<div class="formbox">        
			<h1>
				<? if('add' == $props['mode']) : ?>
					Create a Journal
				<? else: ?>
					Edit Journal for <?=HtmlHelpers::truncateText($props['title'], 20)?>
				<? endif; ?>
			</h1>
			<?php if (isset($props['errors']) AND 0 < count($props['errors'])): ?>
				<div class="errors">
					<ul>
						<?php foreach($props['errors'] as $error): ?>
							<li><?php echo $error; ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
			<form name="sform" action="<?=$props['action']?>" method="post" class="interactive_form">
				<ul class="form add_journal">
								<?php if( array_key_exists('contextGroup',$props) && count($props['col_subgroups']) > 0): ?>
									<li>
										<fieldset class="choices">
										    <legend class="inpt_box">Sub-Groups: <span class="required">*</span></legend>
    										<?php 
    										    if (8 < count($props['col_subgroups'])) {
    										        $class_many = ' class="many"';
    										    } else {
    										        $class_many = '';
    										    }
    										?>
    										<ul <?=$class_many?>>
										    
    										<?
    										    $i = 0;
    											foreach($props['col_subgroups'] as $sub_groups):
    											    $i++;
    												if( count($props['gIDs']) && in_array($sub_groups->getGroupID(), $props['gIDs']) )
    													$selected = 'checked=true';
    												else
    													$selected = '';
    										?>
    											<li>
    											    <input type="checkbox" name="gIDs[]" id="gIDs_<?=$i?>" value="<?= $sub_groups->getGroupID() ?>" <?= $selected ?> />
    											    <label for="gIDs_<?=$i?>"><?= $sub_groups->getName() ?></label>
    											</li>
    										<? endforeach; ?>
    										</ul>
										</fieldset>
									</li>
								<?php elseif(array_key_exists('contextGroup',$props) && $props['contextGroup']->getGroupID() == $props['gID'] && !count($props['col_subgroups'])):?>
									<li>
										<label class="inpt_box">Group: <?= $props['contextGroup']->getName() ?></label>
									</li>
								<?php endif; ?>
								<li>
									<label class="inpt_box">Journal Title <span class="required">*</span></label>
									<p class="supplement">
										Name your journal book. You will be able to add individual titles for each of your entries later.<br />
										Some examples: &#8220;My First Trip to Europe&#8221;, &#8220;Vacation in Jamaica&#8221;.
									</p>
									<input type="text" name="title" id="title" size="40" value="<?=$props['title']?>" class="text big charcount250" />
									<br/><input type="text" disabled="disabled" id="title_counter" size="3" value="" /> characters remaining
								</li>		
								<li>
									<label class="inpt_box">Description <span class="required">*</span></label>
									<p class="supplement">
										Please write a few words (2-3 sentences) describing what your entries for this journal will be about. <br />
										This is <strong>NOT</strong> your journal entry yet. You will be able to add your entries in the next step.
									</p>
									<textarea name="description" id="description" class="charcount150"><?=$props['description']?></textarea>
									<br/><input type="text" disabled="disabled" id="description_counter" size="3" value="" /> words remaining
								</li>
						
						
								<? if( isset($props['isAdmin']) ): ?>
									<li>
										<label class="inpt_box">Last Updated</label>
										<p class="supplement">
											example: &#8220;yyyy-mm-dd&#8221;.
										</p>
										<input type="text" name="lastupdate" id="lastupdate" size="20" maxlength="10" value="<?$props['lastupdated']?>" class="text small" />
									</li>
								<? endif;?>
								<li id="regbutton" class="actions jRight">
									<?php if(isset($_SERVER['HTTP_REFERER'])):?>
										<a href="<?=$props['obj_back_link']->getLink(true,true)?>" class="button_v3 goback_button jLeft" style="margin-right:10px;"><strong>Go Back</strong></a>
									<?php endif;?>
									<input type="submit" name="action" id="action" value="Next" class="button_v3" />
									<input type="hidden" name="travelID" id="travelID" value="<?=$props['travelID']?>" />
									<input type="hidden" name="context"  id="context"  value="<?=$props['context']?>"  />
									<? if($props['context'] == 2): ?>
										<input type="hidden" name="gID" id="gID" value="<?=$props['gID']?>" />
									<? endif; ?>								
								</li>
							</ul>
			</form>
		</div>
	</div>
	<div id="narrow_column">
		<div class="helpextra"></div>
		<div class="helpbox">
			<h2>What is a Journal?</h2>
			<p>Your Journal is like a book where you record your experiences and trip photos, and the journal entries
are like pages of this book. You may use one Journal for your entire trip and add a series of journal
entries for all its details.
			</p>
		</div>
	</div>
</div>
	</div>
<script type="text/javascript" src="/js/jquery.characterCount.js"></script>	
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.charcount250').characterCount(250);
		jQuery('.charcount250').keyup();
		jQuery('.charcount150').wordCount(150);
		jQuery('.charcount150').keyup();
	});
</script>			