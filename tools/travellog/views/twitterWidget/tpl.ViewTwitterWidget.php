<script type="text/javascript">
	var widgetTravelerID = <?= $travelerID ?>;
	var oauthURL = '<?=$authorizeUrl?>';
	var twitterWidgetStatus = <?= $widgetStatus ?>;
	
	function connect(){
		// twitterWidgetStatus = 2 means that the user has no record yet and thus must be redirected to the oauthURL
		if(twitterWidgetStatus == 2){
			window.location = oauthURL;
		}
		else if(twitterWidgetStatus == 0){
			jQuery.ajax({
				url: 'ajaxpages/twitterWidget.php',
				data: { action: 'connect', travelerID: widgetTravelerID},
				beforeSend: function(){
					jQuery('#loading_img').html("<img src='images/loading.gif' />")
					},
				success: function(response){
					jQuery('#loading_img').empty();
					jQuery('#div_connect').hide();
					jQuery('#div_connected').show();
					jQuery('#disconnectWrapper').show();
				}
				});
		}
	}
	function disconnect(){
		jQuery.ajax({
			url: 'ajaxpages/twitterWidget.php',
			data: { action: 'disconnect', travelerID: widgetTravelerID},
			beforeSend: function(){
				jQuery('#setting_loading_img').html("<img src='images/loading_small.gif' />");
				},
			success: function(response){
				twitterWidgetStatus = 0;
				jQuery('#setting_loading_img').empty();
				self.parent.tb_remove(this);
				jQuery('#div_connected').hide();
			//	jQuery('#widgetWrapper').css('background-color', '#FFFFFF');
				jQuery('#disconnectWrapper').hide();
				jQuery('#div_connect').show();
				
			}
			});
	}
	function saveTwitterWidgetSettings(saveButton){
		var text = jQuery('#defaultTextSetting').val();
		var origText = jQuery('#origTextSetting').html();
		if(text != origText){
	//	if(text.length > 0){
			jQuery.ajax({
				url: 'ajaxpages/twitterWidget.php',
				data: { action: 'saveSettings', travelerID: widgetTravelerID, defaultText: text},
				beforeSend: function(){
					jQuery('#setting_loading_img').html("<img src='images/loading_small.gif' />");
					},
				success: function(response){
					jQuery('#setting_loading_img').empty();
					jQuery('#origTextSetting').html(text);
				//	jQuery('#defaultTextSetting').val('');
					self.parent.tb_remove(this);
				}
				});
		}
		
	}
	
	function cancelSetting(){
		if(jQuery('#defaultTextSetting').val() != jQuery('#origTextSetting').html()){
			jQuery('#defaultTextSetting').val(jQuery('#origTextSetting').html());
		}
		self.parent.tb_remove(this);
	}
	
	function limitText(textarea, limitNum) {
		var limitCount = jQuery('#characterCounter');
		var limitVal = limitCount.html();
		if(textarea.value.length > limitNum){
			textarea.value = textarea.value.substring(0, limitNum);
		}
		else {
			var limitVal = limitNum - textarea.value.length;
			if(limitVal == 0 || limitVal == ""){
				limitVal = '0';
			}
			limitCount.html(limitVal);
		}
	}
	
	function positionThickBox(){
		window.scroll(0,0);
	}
	
	
</script>

<?php $style = ("" == $errorMessage) ? 'display:none' : '' ?>
<?php $defaultText = (!is_null($widgetSettings) ? $widgetSettings->getMessage() : '' )?>


<div class="unit size1of2">
	<span id="twitterLogo" href="#">twitter</span>
</div>
<div class="unit size1of2 buttons standard rounded">
	<? /*php if($widgetStatus === 1):*/?>
		<div id="div_connected" style="display:none;">
		<p>Your <?=$siteName?> Network profile is now synchronized with your Twitter account. You may now share your travel journals, photos and videos as tweets.</p>
		<span id="widgetCheckMark">Connected</span>
		<a id="widgetOptionsBtn" class="thickbox" style="text-decoration: none;" href="#TB_inline?height=600&width=550&inlineId=modal&modal=true" onmouseup="positionThickBox();" title="Change post text">Options</a>
		</div>
	<? /*php else: */?>
		<div id="div_connect" style="display:none;">
		<p>Twitter is a social networking service that enables users to send and read short messages known as tweets. Click Connect to start tweeting.</p>
		<button class="" id="widgetConnectBtn" type="button" onclick="connect();" title="Connect to Twitter">Connect</button>
		<p class="errorMessage"><?php echo $errorMessage?></p>
		</div>
		
	<? /*php endif;*/?>
	<div id='loading_img'></div>
	<div id="disconnectWrapper" style="display:none;">
		<button onclick="disconnect()" title="Disconnect Twitter Widget">Disconnect</button>
	</div>
</div>
	
<div id="modal" style="display:none;">
	
	<a class="modalClose" onclick="cancelSetting();">X</a>
	<div class="modalBody" id="widgetOptionsWrapper">
		<ul>	
			<li class="modalHeader">
				<h4>Change Post Text</h4>
			</li>
			<li>
				<form class="socialMediaForm">
					<textarea class="postTextbox" name="limitedtextarea" id="defaultTextSetting" onkeydown="return limitText(this,117);" onkeyup="return limitText(this,117);"><?php echo trim($defaultText); ?></textarea>
					<span id='saveTextSetting_error'></span>
				</form>
			</li>
			
			<ul class="line">
				<li class="unit size1of2" id="twitterOptionsLeft">
					<p id="textAreaHelpText">You may customize the message you wish to appear on Twitter whenever you share your <?=$siteName?> Network articles, photos and videos.</p>
					<div id="origTextSetting" style="display:none;"><?php echo trim($defaultText); ?></div>
				</li>
				
				<li class="unit size1of2" id="twitterOptionsRight">
					<?php $limitCount = 117 - strlen($defaultText); ?>
					<p id="characterCounter"><?php echo $limitCount?></p>
				</li>

			<li class="buttons standard rounded">
				<span id='setting_loading_img'></span>
			<!--	<button id="widgetOptionsSaveBtn" type="button" onclick="cancelSetting();">Cancel</button> -->
				<button id="widgetOptionsSaveBtn" type="button" onclick="saveTwitterWidgetSettings(this);" title="Save post text">Save</button>
			</li>
			</ul>
		</ul>
	</div>

</div>
<script type="text/javascript">
	if(twitterWidgetStatus === 1){
		jQuery('#div_connected').show();
	}
	else {
		jQuery('#div_connect').show();
		jQuery('#disconnectWrapper').hide();
	}

//	alert('test');
//	jQuery('#disconnectWrapper').css('top', '25px');
//	jQuery('#disconnectWrapper').hide();
	jQuery('#twitterWidgetWrapper').hover(
		function(){
			if(jQuery('#div_connect').css('display') == 'none'){
				jQuery('#disconnectWrapper').show('');
			}
		/*	if(jQuery('#disconnectWrapper').css('display') == 'block'){
				jQuery('#widgetWrapper').css('background-color', '#FAFAFA');
				jQuery('#disconnectWrapper').stop().animate({top: '0px'}, 500, "easein");
			//	alert(jQuery('#twitterWidgetWrapper').css('background-color'));
			}*/
		},
		function(){
			if(jQuery('#div_connect').css('display') == 'none'){
				jQuery('#disconnectWrapper').hide();
			}
		/*	if(jQuery('#disconnectWrapper').css('display') == 'block'){
				jQuery('#widgetWrapper').css('background-color', '#FFFFFF');
				jQuery('#disconnectWrapper').stop().animate({top: '25px'}, 500, "easein");
			//	alert(jQuery('#twitterWidgetWrapper').css('background-color'));
			}*/
		}
	);
</script>