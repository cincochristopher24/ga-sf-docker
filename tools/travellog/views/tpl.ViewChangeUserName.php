<div style="background-color:#CCCCCC;width:395px;overflow:hidden;padding:15px;height:125px;">
	<form name="frmChange" action="/changeUserName.php?username=<?=$username?>&amp;ID=<?=$travelerID?>" method="post">
		<div style="color:red;font-size:12px;margin-bottom:10px;text-align:center;"><?=$error?></div>
		<div style="overflow:hidden;margin-top:3px;">
			<div style="width:180px;float:left;text-align:right;margin-right:2px;">Old Username:</div>
			<div style="width:200px;float:left">
				<input type="textbox" name="txtUserName" id="txtUserName" value="<?=$username?>" readonly="true" size="25" />
			</div>
		</div>
		<div style="overflow:hidden;margin-top:3px;">
			<div style="width:180px;float:left;text-align:right;margin-right:2px;">
				New Username: <span style="color:red">*</span>
			</div>
			<div style="width:200px;float:left">
				<input type="textbox" name="txtNewUserName1" id="txtNewUserName1" value="<?=$newUserName1?>" size="25" />
			</div>
		</div>
		<div style="overflow:hidden;margin-top:3px;">
			<div style="width:180px;float:left;text-align:right;margin-right:2px;">
				Retype New Username: <span style="color:red">*</span>
			</div>
			<div style="width:200px;float:left">
				<input type="textbox" name="txtNewUserName2" id="txtNewUserName2" value="<?=$newUserName2?>" size="25" />
			</div>
		</div>
		<div align="center" style="margin-top:20px;">
			<input type="submit" name="btnChangeUserName" id="btnChangeUserName" value="Change Username" />
			&nbsp;&nbsp;
			<input type="button" name="btnCancel" id="btnCancel" value="Cancel" onclick="javascript:self.close()"/>
		</div>
	</form>
</div>