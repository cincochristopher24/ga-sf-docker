<?php	
	Template::setMainTemplateVar('layoutID', 'survey');
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');		
	require_once('travellog/helper/Class.NavigationHelper.php');
	if (NavigationHelper::isCobrand()) {
		Template::setMainTemplateVar('page_location', 'Home');
	}else{
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
	Template::includeDependentJs('/js/interactive.form.js');
	Template::includeDependentJs('/js/scriptaculous/scriptaculous.js');
	//Template::includeDependentCss('/css/formeditor/surveystatsentry.css');
	require_once('Class.GaDateTime.php');
	
	$d = new GaDateTime(); ?>
	
<!-- displays the profile component: added by neri 11-06-08 -->
<?= $profile->render()?>
<!-- end -->	
	
<?	$subNavigation->show();
	ob_start();	
?>
<script type="text/javascript">
//<![CDATA[
	var containers = new Array();
	function showDetail(divCont){
		for(var i=0;i<containers.length;i++){
			$(containers[i]).style.display = 'none';
			$('link_'+containers[i]).removeClassName('active');
		}
		new Effect.Appear(divCont, { 
			duration: .30
		});
		//$(divCont).style.display = 'block';
		$('link_'+divCont).addClassName('active');
		
	}
	function showFirstEntry(){
		showDetail(containers[0]);
	}
//]]>
</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);
?>

<div class="area mine" id="intro">
	<div class="section">
    	<h1>Survey Center</h1>
  	</div>
</div>
	<div id="container">
	
	
		<div id="container_inner_wrapper">
			<div id="survey_options">
		    	<ul id="top_tabs">
		    		<li id="sel" class="first_tab"><a href="/surveycenter.php?gID=<?= $oGroup->getGroupID() ?>" class="first_tab">Manage Surveys</a></li>
		      		<li><a href="/surveyformeditor.php?frmID=0&gID=<?= $oGroup->getGroupID() ?>">Create a New Survey</a></li>
		    	</ul>
		    	<div class="clear"></div>
		  	</div>
			<div id="content">
				<div id="stats_content" class="section">
					<h2><?= $surveyForm->getName() ?> Stats (<?= $totalParticipants ?> participant<? if ($totalParticipants > 1) echo 's'; ?>) </h2>
					<p id="survey_caption"><?= $surveyForm->getCaption() ?></p>
					<ul class="tabs">
						<li><a href="/surveystats.php?frmID=<?= $surveyForm->getSurveyFormID() ?>&gID=<?= $oGroup->getGroupID() ?>">Overview</a></strong></li>
						<li class="active"><strong><a href="/surveystats.php?frmID=<?= $surveyForm->getSurveyFormID() ?>&view=entries&gID=<?= $oGroup->getGroupID() ?>">Entries</a></strong></li>
					</ul>
					<div id="stats_entries">
						
						<div id="navPanel">
							<ul>
							<!-- The next li element is a temporary fix for an as yet undocumented
								problem with IE7. Margin settings does not seem to apply properly on
								this first child element when its className changes dynamically through
								javascript. Weird...
							<li class="walalang" style="display:none"></li>-->
							<?php foreach($participantInfo as $iInfo): ?>
								<li id="link_ans_<?= $iInfo['formAnswer']->getSurveyFormAnswerID() ?>">
									<p>
										<strong><a href="javascript:void(0)" onclick="showDetail('ans_<?= $iInfo['formAnswer']->getSurveyFormAnswerID()?>')"><?= $surveyForm->isParticipantVisible() ? $iInfo['travelerProfile']->getUserName() : 'Anonymous' ?></a></strong><br />
										<span class="info meta date"><strong>Date: </strong><?= $d->set($iInfo['formAnswer']->getDateCreated())->friendlyFormat() ?></span><br />
										<?php if($surveyForm->isParticipantVisible()): ?>
											<span class="info meta"><strong>Group:</strong> <?= $iInfo['group']->getName() ?></span>
										<?php endif; ?>
									</p>
									<script type="text/javascript">
										containers[containers.length] = 'ans_<?= $iInfo['formAnswer']->getSurveyFormAnswerID()?>';
									</script>
								</li>
							<?php endforeach; ?>
						</div>
				
						<div id="detailPanel">
							<?php foreach($participantInfo as $iInfo): ?>
								<div id="ans_<?= $iInfo['formAnswer']->getSurveyFormAnswerID()?>" style="display:none" >
									<?= $iInfo['formTpl'] ?>
								</div>
							<?php endforeach; ?>
						</div>
											
						<div class="clear"></div>
					
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	
	</div>


<div class="clear"></div>
<script type="text/javascript">
	showFirstEntry();
</script>
