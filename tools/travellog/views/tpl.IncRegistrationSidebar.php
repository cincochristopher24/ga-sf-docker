<?php if( isset($GLOBALS["CONFIG"]) ): ?>
	<div class="helpbox">
		<h2>Why sign up on <?php echo $siteName;?> Network?</h2>
		<p>As a <?php echo $siteName;?> Network Traveler, you will be able to</p>
		<ul>
			<li><p><strong>meet and be friends with travelers</strong> from all over the world</p></li>
			<li><p><strong>blog</strong> about your travels in your very own travel journals</p></li>
			<li><p><strong>post and share</strong> travel photos</p></li>
			<li><p><strong>join groups</strong> of like minded travelers</p></li>
			<li><p><strong>post messages</strong> for your friends and groupmates</p></li>
			<li><p><strong>search for programs</strong> for your travels abroad</p></li>
			<li><p>and <strong>lots of other features</strong> rolling out in the following weeks!</p></li>
		</ul>
	</div>
<?php else: ?>
	<div class="helpbox">
		<h2>Why sign up on GoAbroad Network?</h2>
		<p>As a GoAbroad Network Traveler, you will be able to</p>
		<ul>
			<li><p><strong>connect with travelers</strong> from all over the world</p></li>
			<li><p><strong>blog</strong> about your travels in your very own travel journals</p></li>
			<li><p>post and share <strong>travel photos and videos</strong></p></li>
			<li><p><strong>join/create groups</strong> for your Universities, study abroad offices, travel agents, tour operators or just about any group of travel enthusiasts</p></li>
			<li><p><strong>share on Facebook</strong> - bring travel journals and travel map to Facebook, MySpace, Orkut and hi5</p></li>
		</ul>
	</div>
<?php endif; ?>

