<li>
	<a title="View Group Profile" class="group_thumb" style="float: left;" href="" onclick="return false;" >	
		<img class="subgroup_img" alt="Group Emblem" src="<?=$group->getGroupPhoto()->getPhotoLink('standard') ?>" />											
	</a>
	<div class="groups_info">
		<strong><a href="" class="groupname" onclick="return false;"><?=$group->getName()?></a></strong>
		<?	if( 0 < $group->getParentID() ): ?>
			<?	$parent = $group->getParent(); ?>																				
			<p>	
				<?=$parent->getName()?>		
			</p>										
		<?endif;?>
		<?if ($group->isStaffInvitation($travelerID)):?>
			<p>Staff Invitation</p>
		<?endif;?>			
		<p class="actions">
			<a href="<?=$linkConfirm?>">Accept</a>
			&nbsp;|&nbsp;
			<a href="<?=$linkCancel?>">Deny</a>
		</p>
	</div>
</li>