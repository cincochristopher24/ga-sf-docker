<?php
/**
 * <b>View event</b> contents template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
?>

<table border="1" cellpadding="4" cellspacing="4" width="100%">
	<tr>
		<td valign="top" width="30%">
			<table border="1" cellpadding="4" cellspacing="4" width="100%">
				<?
					$obj_iterator->rewind();
					while( $obj_iterator->valid() ):
						echo '<tr id="'.$obj_iterator->current()->getTravelerEventID().'" class="viewevents"><td>';
						echo $obj_iterator->current()->getTheDate().'<br /><a href="javascript:void(0)" onclick="toggleEvent('.$obj_iterator->current()->getTravelerEventID().','.$context.');" >'.$obj_iterator->current()->getTitle().'</a>';
						echo '</td></tr>';
						$obj_iterator->next();
					endwhile;
				?>
			</table>
			
		</td>
		<td valign="top" width="70%">
			<div id="view"></div>
		</td>
	</tr>
	<tr>
		<td valign="top" width="30%">
			<? if ( $obj_paging->getTotalPages() > 1 ): ?>
				<table border="0" cellpadding="1" cellspacing="1" width="100%">
					<tr>
						<td align="left"><? $obj_paging->getPrevious() ?></td>
						<td align="right"><? $obj_paging->getNext() ?></td>
					</tr>
				</table>
			<? endif; ?>
			<div id="eventID" style="display: none;"><?= $primaryID ?></div>
			<div id="context" style="display: none;"><?= $context   ?></div>
		</td>
	</tr>
</table>
