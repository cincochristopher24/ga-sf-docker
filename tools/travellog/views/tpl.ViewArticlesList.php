<div id="journals_summary" class="section" >
	<a name="anchor"></a>
	<h2><span>Articles</span></h2>
	
	<div class="content" >								
		<ul class="journals">
			<?foreach($articles as $article):?>
				<?
					$location = LocationFactory::instance()->create($article->getLocID());
					$country = $location->getCountry()->getName();
					$city = $location->getName();
					$locationID = $location->getCountry()->getCountryID();
				?>
			 	<li class="container">
					<div class="content">
						<a href="/article.php?action=view&amp;articleID=<?=$article->getArticleID()?>"> 
							<img class="float_right" src="<?=$article->getPrimaryPhoto()->getPhotoLink('standard')?>" alt="journal photo" />
						</a>										
						<h3>
						<a class="journal_title" href="/article.php?action=view&articleID=<?=$article->getArticleID()?>">
							<?=$article->getTitle()?>
						</a>
						</h3>							
						<p class="meta">
							<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$locationID?>.gif" width="22" height="11" alt="Articles from <?=$country?>" style="margin: 0 0 0 0;" />						
							<?=( strtolower($city) == strtolower($country) )? $country : $city . ', ' . $country?> | <?=date('D M d, Y',strtotime($article->getLogDate()))?> | Views: 0<?//=$journal->getViews()?>
						</p>
						<p>
							<?= StringFormattingHelper::wordTrim($article->getDescription(), 70, '&#8230;')?>
						</p>
						<p class="entry_group_tag" id="override">
							<span>
								<a class="username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$article->getTraveler()->getUserName()?>">
									<img class="pic" width="37" height="37" src="<?=$article->getTraveler()->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" alt="<?=$article->getTraveler()->getUserName()?>" title="Read more of <?=$article->getTraveler()->getUserName()?> articles!" />
								</a>
							</span>					
							<a class="author_username" href="http://<?=$_SERVER["SERVER_NAME"].'/'.$article->getTraveler()->getUserName()?>">
								<strong><?=$article->getTraveler()->getUserName()?></strong>
							</a>									
						</p>
					</div>
				</li>		
			<?endforeach;?>	
		</ul>
	</div>		
</div>	
<? if (isset($pagingComp)):?>	
    	<? $pagingComp->showPagination() ?>
<? endif; ?>
<div id="dlg"></div>
