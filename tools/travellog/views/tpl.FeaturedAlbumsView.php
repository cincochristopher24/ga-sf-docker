<?/*php
	$albumTemplates = array();
	$albumCount = 0;
	foreach($featuredAlbums AS $album){
		$photoCount = $album->getCountPhotos();
		if( 0 < $photoCount ){
			$albumTpl = new Template();
			$albumTpl->set("album",$album);
			$albumTpl->set("context", "traveler"==$type ? "traveleralbum" : "photoalbum");
			$albumTpl->set("photoCount",$photoCount);
			$albumTemplates[] = $albumTpl;
			$albumCount++;
		}
		if( 3 == $albumCount ){
			break;
		}
	}
?>
<?	if( 0 < count($albumTemplates) ): ?>
	<div class="section newMedia">
		<h2>
			<span>Photos</span>
			<span class="header_actions">
				<div style="clear: both;">
					<a href="/collection.php?type=<?=$type?>&amp;ID=<?=$ID?>">
						View All Photos
					</a>
				</div>
			</span>
		</h2> 
		<div class="content">												
			<?php	
				foreach($albumTemplates AS $albumTpl):
					$albumTpl->out("travellog/views/tpl.IncFeaturedAlbumView.php");
				endforeach;
			?>
		</div>

	</div>
<?	endif; */?>

<?	if( 0 < count($recentUploads) ): ?>
	<div class="section newMedia">
		<h2>
			<span>Photos</span>
			<span class="header_actions">
				<div style="clear: both;">
					<a href="/collection.php?type=<?=$type?>&amp;ID=<?=$ID?>">
						View All Photos
					</a>
				</div>
			</span>
		</h2> 
		<div class="content">												
			<?php
				require_once("Class.GaDateTime.php");
				foreach($recentUploads AS $recentUpload):
					$dateuploaded = '';
					try{ 
						// handle this in advance since PHP 5.3 a valid date here, possibly 1969-12-31
						if($recentUpload->getDateuploaded() != '0000-00-00 00:00:00'){
							$dateuploaded = strtolower(GaDateTime::descriptiveDifference($recentUpload->getDateuploaded(), date("Y-m-d H:i:s"))); 
						}	
					}catch(exception $e){ $dateuploaded = ''; }
					
					// fix for photos with no upload dates; if not caught by exception
					$dateuploaded = ($dateuploaded == '0000-00-00 00:00:00') ? '' : $dateuploaded;

					$recentUploadTpl = new Template();
					$recentUploadTpl->set("photo",$recentUpload);
					$recentUploadTpl->set("type",$type);
					$recentUploadTpl->set("ID",$ID);
					$recentUploadTpl->set("date",$dateuploaded);
					$recentUploadTpl->out("travellog/views/tpl.IncFeaturedAlbumView.php");
				endforeach;
			?>
		</div>

	</div>
<?	endif; ?>