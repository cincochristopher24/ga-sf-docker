



<p class="photo_controls">
	<img src="/images/edit_photo.gif" class="edit_ico"/><a href="" alt="Edit Title/Photo" title="Edit Title/Photo" class="edit_text">Edit caption</a>
	<img src="/images/delete_ico.gif"class="delete_ico"/><a href="" alt="Delete Photo" title="Delete Photo">Delete photo</a>
</p>

<div class="white_box">		
	<div class="bg_gray">
		<img src="<?= urldecode($photo[$elementID]->getPhotoLink('fullsize')); ?>" class="pic_borders" />	
	</div>
</div>
			
	
<div class="text_nav">			
			<?if($prev): ?>
					<a href="javascript:void(<?=$pgprev ?>)" onclick="<?=$pgprev ?>" class="prev_but">prev</a>
			<?else:?>
					prev
			<? endif; ?>
			
			<span class="full_caption"><?= $photo[$elementID]->getCaption(); ?></span>
			
			<?if($next): ?>
					<a href="javascript:void(<?=$pgnext ?>)" onclick="<?=$pgnext ?>" class="next_but">next</a>
			<?else:?>
					next
			<? endif; ?>
				
</div>				


<div class="comment_box" id="id">					
	<?=$commentsview->render(); ?>		
</div>
<div class="clear"></div>	