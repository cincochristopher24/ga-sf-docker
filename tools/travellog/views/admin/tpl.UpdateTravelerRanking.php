<h2>Travelers(<?=($confirm ? count($insertedTravelers) : $data->retrieveRecordcount())?>) <?=($confirm ? 'inserted' : 'to be  inserted')?> to tblTravelerRanking:</h2>

<?php if( !$confirm ) :?>
	<form action="" method="post" >
		<input type="submit" value="GO"  onclick ="return confirm('Are you sure?')"/>
		<input type="hidden" name="action" value="updateTravelerRanking" />
		<input type="hidden" name="confirm" value="1" />
	</form>
<?php endif; ?>

<table width="50%">
	<tr>
		<th>Traveler ID</th>
		<th>Username</th>
		<th>Password</th>
		<th>isAutomatedTraveler</th>
	</tr>

<?php if( $confirm ): ?>
	<?php foreach( $insertedTravelers as $v ) : ?>
		<tr>
			<td align="center"><?=$v['travelerID']?></td>
			<td align="center"><?=$v['username']?></td>
			<td align="center"><?=$v['password']?></td>
			<td align="center"><?=$v['isAutomatedTraveler']?></td>
		</tr>
	<?php endforeach;?>
	
<?php else : ?>
	<?php foreach( $data as $rs ) : ?>
		<tr>
			<td align="center"><?=$rs['travelerID']?></td>
			<td align="center"><?=$rs['username']?></td>
			<td align="center"><?=$rs['password']?></td>
			<td align="center"><?=$rs['isAutomatedTraveler']?></td>
		</tr>
	<?php endforeach;?>
<?php endif; ?>
</table>

<?php if($confirm):?><div><b>Running from <?=$_SERVER['SERVER_NAME']?></b></div><?php endif;?>