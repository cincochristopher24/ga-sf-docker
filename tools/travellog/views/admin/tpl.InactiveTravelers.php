<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
?>

<div id = "intro_container">
	<div id = "intro" align = "center">
		<h1> INACTIVE TRAVELERS</h1>
		<h2> QUANTITY : <?=$cntTraveler?> </h2>
	</div>
</div>

<div id = "content_wrapper" class = "layout_2">
	<div class = "section">
		<div class = "content">
			<form name = "frmSend" id = "frmSend" action = "act_SendEmailConfirmation.php" method = "post">
				<ul class = "users">
					<li>
						<input type = "submit" name = "btnSubmit" id = "btnSubmit" value = "Send Email Confirmation" />
					</li>
					<?
						$lstTravelerID = '';
						$iterator->rewind();
						while($iterator->valid()):
							$indTraveler =$iterator->current();
							$photolink = $indTraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink();
							if (0 != strncmp("http://", $photolink , "7"))
								$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
							$travelerID = $indTraveler-> getTravelerID();
							$lstTravelerID = $lstTravelerID . ', ' . $travelerID;
						?>
							<li>
								<a class = "thumb" href="/<?=$indTraveler->getUsername()?>">
									<img src = "<?=$photolink?>" alt = "<?=$indTraveler->getUsername()?>" style = "width: 110px; height: 120px" />
								</a>
								<div class = "details" style = "margin-left: 50px">
									<strong>
										<a href="/<?=$indTraveler->getUsername()?>"><?=$indTraveler->getUsername()?></a>
									</strong>
									<hr width = "95%" align = "left" />
									<p class = "gray">
										<strong>Name: </strong>
										<?=$indTraveler->getTravelerProfile()->getFirstName().' '.$indTraveler->getTravelerProfile()->getLastName()?>
									</p>
									<p class = "gray">
										<strong>Email Address: </strong>
										<?=$indTraveler->getTravelerProfile()->getEmail();?>
									</p>
									<p class = "gray">
										<strong>Member Since: </strong>
										<?= $d->set($indTraveler->getTravelerProfile()->getDateJoined())->friendlyFormat()?>							
									</p>
									<p>
										<input type = "checkbox" name = "<?=$travelerID?>" id = "<?=$travelerID?>" value = "1"/>
										&nbsp;
										<a
											href = "javascript: void(0)"
											onclick = "window.open('act_SendEmailConfirmation.php?action=view&travelerID=<?=$travelerID?>',
																	'View Email Confirmation',
																	'resizable,width=710,height=495, top=' + ((screen.width/2)- 500) + ', left=300'); return false">
											view email
										</a>
									</p>
								</div>
							</li>
							<? $iterator->next(); ?>
						<? endwhile; ?>
				</ul>
				<input type = "hidden" name = "page" id = "page" value = "<?=$page?>" />
				<input type = "hidden" name = "lstTravelerID" id = "lstTravelerID" value = "<?=$lstTravelerID?>" />
			</form>

   			<div class="pagination">
				<? if ($paging->getTotalPages() > 1 ): ?>		
					<div class="page_markers">
						<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $cntTraveler ?>
					</div>
					<? $paging->getFirst(); ?>
					&nbsp;|&nbsp;
					<? $paging->getPrevious(); ?>
					&nbsp;&nbsp;
					<? $paging->getLinks(); ?>
					&nbsp;&nbsp;
					<? $paging->getNext(); ?>
					&nbsp;|&nbsp;
					<? $paging->getLast(); ?>
				<? endif; ?>	
			</div>
		</div>
	</div>
</div>