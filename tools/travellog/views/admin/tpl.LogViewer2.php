<?
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	$dateobj = new GaDateTime();
?>

<script type="text/javascript">
	function funcDisable(radio){
		// if by keyword 

		if(radio == 0)
		{
			document.frmSearch.txtFromDate.disabled = true;
			document.frmSearch.txtToDate.disabled = true;
			document.frmSearch.txtKeyword.disabled = false;
		}
		
		//if by date range
		else if(radio == 1)
		{
			document.frmSearch.txtKeyword.disabled = true;
			document.frmSearch.txtFromDate.disabled = false;
			document.frmSearch.txtToDate.disabled = false;
		}
	}
</script>

<?	if ($radButton == 0): ?> <body onload = "funcDisable(0)"> <? endif;
	if ($radButton == 1): ?> <body onload = "funcDisable(1)"> <? endif;?>
	<div id="wrapper1">
		<div id="wrapper2">
			<div id="maincol">
				<div id="leftcol"></div> 
				<div id="centercol">
					<div id="log_list" style = "padding:20px;">
						<div style = "padding:15px">
							<form name = "frmSearch" method = "POST">
								<table align = "center" border = "0" width = "45%" style = "background-color: grey;">
									<tr>
										<td colspan = "2"> <b> Search: </b> </td>
									</tr>
									<tr>										
										<td width = "50%"> <input type = "radio" name = "radButton" id = "radButton" onclick = "javascript:funcDisable(0);" value = "0" <? if ($radButton == 0): ?> checked <? endif; ?>/> by Keyword </td>
										<td width = "50%"> <input type = "radio" name = "radButton" id = "radButton" onclick = "javascript:funcDisable(1);" value = "1" <? if ($radButton == 1): ?> checked <? endif; ?>/> by Date Range </td>
									</tr>
									<tr>								
										<td> Keyword: <input type = "text" name = "txtKeyword" value = "<?= $txtKeyword ?>" size = "23 "/> </td>
										<td> 
											From: <input type = "text" name = "txtFromDate" id = "txtFromDate" size = "12" value = "<?= $txtFromDate ?>" /> <font style = "font-size:10px"> (yyyy-mm-dd) </font> <br />
											To: &nbsp;&nbsp;&nbsp; <input type = "text" name = "txtToDate" id = "txtToDate" size = "12" value = "<?= $txtToDate ?>" /> <font style = "font-size:10px"> (yyyy-mm-dd) </font> 
										</td>
									</tr>
									<tr>
										<td align = "right" colspan = "2"> <input type = "submit" name = "btnSubmit" value = "Search" /> </td>
									</tr>
								</table>
							</form>						
						</div>
						<?
							if (1 == $dispPaging){
								?>
								<div class="section_top paging" style = "padding-top:15px; padding-left:20px; padding-bottom:15px;">
									<? $paging->getFirst(); ?> |
									<? $paging->getPrevious() ; ?> |
									<? $paging->getLinks(); ?> |
									<? $paging->getNext(); ?> |
									<? $paging->getlast(); ?>
								</div>
								<?
							}
							
							if (0 < sizeOf($handle)){								
								foreach($handle as $logSite){
								?>
									<div align = "right" style = "padding-right:20px;"> <b> <?= $dateobj-> set($logSite->getErrorDate())->friendlyFormat(); ?> </b> </div> 
									<div align = "left" style = "padding-left:20px;">
										<b> <?= $logSite-> getErrorType() . "<br />" ?> </b>
										<?= $logSite-> getMessage() . "<br />" ?>
										<?= "Error occured at line " . $logSite-> getLineNum() . " in "; ?> <b> <?= $logSite-> getScriptName() . "<br />" ?> </b>
										<b> Referrer: </b> <?= $logSite-> getReferer() . "<br />" ?>
										<b> Remote Address </b> <?= $logSite-> getRemoteAddress() . "<br />" ?>
										<b> Browser: </b> <?= $logSite-> getBrowser() . "<br />" ?>
										<b> Method: </b> <?= $logSite-> getMethod() . "<br />" ?>
									</div>
									<?
								}
							}
							else{								
								?>
								<div align = "center" style = "font-weight:bold; color:red; font-size:15px; padding-top:20px;"> No Results Found </div>
								<div id = "main">
									<div class = "actions pagination">
										<ul>
											<li class = "previous">
												<a href = "logindex2.php" title = "Go back to Log Files" class = "button"> Previous </a>
											</li>
										</ul>
									</div>
								</div>
								<?
							}
						if (1 == $dispPaging){
						?>
							<div class="section_foot paging" style = "padding-right:20px;">
								<? $paging->getFirst(); ?> |
								<? $paging->getPrevious() ; ?> |
								<? $paging->getLinks(); ?> |
								<? $paging->getNext(); ?> |
								<? $paging->getlast(); ?>
							</div>
							<?
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
