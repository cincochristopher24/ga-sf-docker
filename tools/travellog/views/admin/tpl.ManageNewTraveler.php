<div align="center">
	<form action="" method="post">
		<b>Keyword:</b>
		<input type="text" name="txtKeyword" value="<?=$keyword?>" />
		<b>Search By:</b>
		<select name="searchBy">
			<option value="username" <?=('username' == $searchBy ? 'selected' : '')?> >username</option>
			<option value="email" <?=('email' == $searchBy ? 'selected' : '')?> >email</option>
		</select>
		<input type="submit" value="Search" />
		<input type="hidden" name="action" value="searchTravelers" />
		<input type="hidden" name="gID" value="<?=$gID?>" />
	</form>
	<hr />
	<table cellpadding="4" cellspacing="2" width = "50%">
		<tr>
			<td align="center">
				<label>Username</label>
			</td>
			<td align="center">
				<label>Email</label>
			</td>
			<td align="center">
				<label>Action</label>
			</td>
		</tr>
		<?php if( !is_null($data) && $data->retrieveRecordcount()): ?>
			<?php foreach( $data as $row ):?>
				<?php
					$traveler 	= new Traveler;
					$traveler->setTravelerData($row);
					$isMember	= false;
					$groupName	= null;
					if( $gID ){
						$isMember = $group->isMember($traveler);
						$group->getName();
					}
				?>
				<tr>
					<td align="center"><a href="<?=$traveler->getFriendlyUrl()?>"> <?=$row['username']?> </a></td>
					<td align="center">
						<form action="" method="post">
							<input type="text" name="txtEmail" id="txtEmail" value="<?=$row['email']?>" />
							<input type="submit" value="Update Email" />
							<input type="hidden" name="tID" value="<?=$traveler->getTravelerID()?>" />
							<input type="hidden" name="gID" value="<?=$gID?>" />
							<input type="hidden" name="action" value="updateEmail" />
						</form>
					</td>
					<td align="center">
						<?php if($isMember): ?>
							<a href="manageNewTraveler.php?action=emailNotification&amp;tID=<?=$row['travelerID']?>&amp;gID=<?=$gID?>" onclick="return confirm('Are you sure you want to send a notification to <?=$row['email']?>?')">Email Notification</a><br />
						<?php else: ?>Not A Member
						<?php endif; ?>	
					</td>
				</tr>
			<?php endforeach;?>
		<?php else: ?>
			<tr>
				<td colspan="3" align="center"><label><font color="red">No Records Found!</font></label></td>
			</tr>
		<?php endif;?>
	</table>
	<?php if(!empty($errors)) : ?>
		<div class="errors"><ul><li><?=join('</li><li>', $errors)?></li></ul></div>
	<?php endif;?>
</div>