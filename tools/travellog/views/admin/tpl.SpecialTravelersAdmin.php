<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();
?>

<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>
<div class="area" id="intro">
	<div class="section" align="center">
		<h1>SPECIAL FEATURES</h1>		
	</div>
</div>		

<div class="area" id="intro">
	<div class="section">
		<h2>CURRENTLY FEATURED</h2>		
	</div>
</div>		

<div class="area" id="top">
	<? foreach($specialsections as $eachsection) : ?>
		<? if (count($currFeat[$eachsection->getSectionID()])) : ?>
			<? $idxCurrFeat = $currFeat[$eachsection->getSectionID()] ?>
			<div class="section"><h2><?=$eachsection->getCaption()?></h2></div>
			<div id="journals_list" class="list">
				<?	foreach ($idxCurrFeat as $eachtraveler) :	
					 $photolink = $eachtraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink();
						if (0 != strncmp("http://", $photolink , "7"))
							$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;		    
				?>
					<div class="journal section">
						<div class="content">
							<div class="wrapper">
								<h2>
									<a href="/<?=$eachtraveler->getUsername()?>"><?=$eachtraveler->getUsername()?></a>
								</h2>
								<div class="description">
									<hr width="25%" align="left">
									&raquo;<a href="travelers.php?specialfeature=<?=$eachtraveler->getTravelerID()?>&amp;specialsectionID=<?=$eachsection->getSectionID()?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">Remove Feature</a>
									<br />
									<? if (TRUE == $eachtraveler->isSuspended()) { ?>									
										&raquo;<a href="travelers.php?travID=<?= $eachtraveler->getTravelerID()?>&amp;activate" onclick="javascript:return confirm('Do you want to ACTIVATE this Traveler?');">Activate Traveler</a>
										<br />									
									<? } else { ?>								
										&raquo;<a href="travelers.php?travID=<?= $eachtraveler->getTravelerID()?>&amp;suspend" onclick="javascript:return confirm('Do you want to SUSPEND this Traveler?');">Suspend Traveler</a>
										<br />								
									<? } ?>
								</div>
							</div>
							<div class="photo">
								<a href="/<?=$eachtraveler->getUsername()?>">
									<img src="<?=$photolink?>" alt="<?=$eachtraveler->getUsername()?>" width="123" height="90" />
								</a>								
							</div>
							<div class="info meta">
								<?=$eachtraveler->getCountTravelJournals()?> Journals | <?=$eachtraveler->getCountTravelLogs()?> Entries
								<? $numcountries = $eachtraveler->getTravelerProfile()->getTravelerCountriesTravelled() ?>
								<br />Visited: <?=count($numcountries)?> <?=count($numcountries) > 1 ? ' Countries' : ' Country'; ?>
								<? $numphotos = Traveler::getMostPhotographsByTravelerID($eachtraveler->getTravelerID()) ?>
								<br />Uploaded: <?=$numphotos?> <?=$numphotos > 1 ? ' Photos' : ' Photo'; ?>
								<br />Member Since: <?= $d->set($eachtraveler->getTravelerProfile()->getDateJoined())->friendlyFormat()?>
								<br />Last Login: <?= $d->set($eachtraveler->getTravelerProfile()->getLastLogin())->friendlyFormat()?>						
							</div>
							<div class="clear"></div>
						</div>
					</div>		
				<? endforeach; ?>		
			</div>
		<? endif ;?>
	<? endforeach; ?>
</div>

<div class="area" id="intro">
	<div class="section">
		<h2><?= $title ?></h2>
	</div>
</div>	

<div class="area" id="top">
	<div id="top_controls">						
		<?= $search_template ?><br />
		<?= $mosttraveled_link ?>&nbsp;|&nbsp;<?= $chronicler_link ?>&nbsp;|&nbsp;<?= $photographer_link ?>
	</div>
	
	<? if ($currSection->getSectionID() > 0 ) : ?>
	<div class="section"><h2>CHOOSE <em><?=$currSection->getCaption()?></em> (<?=$currSection->getQuantity()?>)
	<a href="javascript: void(0)"  onClick="window.open('caption.php?sectionID=<?=$currSection->getSectionID()?>','PopUp','left=300,height=200,width=400,status=yes')">change</a>
	</h2></div>		
	<div id="journals_list" class="list">				
		
		<div class="pagination">
			<? if ($paging->getTotalPages() > 1 ): ?>		
				<div class="page_markers">
					<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . count($obj_travelers) ?>
				</div>
				<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
			<? endif; ?>	
		</div>
		
		<?	
			$iterator->rewind();
			while($iterator->valid()):
			$eachtraveler =$iterator->current();
			$photolink = $eachtraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink();
				if (0 != strncmp("http://", $photolink , "7"))
					$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;		    					    
		?>
			<div class="journal section">
				<div class="content">
					<div class="wrapper">
						<h2>
							<a href="/<?=$eachtraveler->getUsername()?>"><?=$eachtraveler->getUsername()?></a>
						</h2>
						<div class="description">
							<hr width="25%" align="left">
							<? if ($currSection->isItemFeatured($eachtraveler->getTravelerID())){ ?>
								<strong>[Featured]</strong>
								<br />
								&raquo;<a href="travelers.php?specialfeature=<?=$eachtraveler->getTravelerID()?>&amp;specialsectionID=<?=$currSection->getSectionID()?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">Remove Feature</a>
								<br />
							<? } else { ?>
								&raquo;<a href="travelers.php?specialfeature=<?=$eachtraveler->getTravelerID()?>&amp;specialsectionID=<?=$currSection->getSectionID()?>">Feature This!</a>
								<br />
							<? } ?>
							<? if (TRUE == $eachtraveler->isSuspended()) { ?>									
								&raquo;<a href="travelers.php?travID=<?= $eachtraveler->getTravelerID()?>&amp;activate" onclick="javascript:return confirm('Do you want to ACTIVATE this Traveler?');">Activate Traveler</a>
								<br />									
							<? } else { ?>								
								&raquo;<a href="travelers.php?travID=<?= $eachtraveler->getTravelerID()?>&amp;suspend" onclick="javascript:return confirm('Do you want to SUSPEND this Traveler?');">Suspend Traveler</a>
								<br />								
							<? } ?>
						</div>
					</div>
					<div class="photo">
						<a href="/<?=$eachtraveler->getUsername()?>">
							<img src="<?=$photolink?>" alt="<?=$eachtraveler->getUsername()?>" width="123" height="90" />
						</a>								
					</div>
					<div class="info meta">
						<?=$eachtraveler->getCountTravelJournals()?> Journals | <?=$eachtraveler->getCountTravelLogs()?> Entries
						<? $numcountries = $eachtraveler->getTravelerProfile()->getTravelerCountriesTravelled() ?>
						<br />Visited: <?=count($numcountries)?> <?=count($numcountries) > 1 ? ' Countries' : ' Country'; ?>
						<? $numphotos = Traveler::getMostPhotographsByTravelerID($eachtraveler->getTravelerID()) ?>
						<br />Uploaded: <?=$numphotos?> <?=$numphotos > 1 ? ' Photos' : ' Photo'; ?>
						<br />Member Since: <?= $d->set($eachtraveler->getTravelerProfile()->getDateJoined())->friendlyFormat()?>
						<br />Last Login: <?= $d->set($eachtraveler->getTravelerProfile()->getLastLogin())->friendlyFormat()?>
					</div>
					<div class="clear"></div>
				</div>
			</div>		
		<?  $iterator->next();
			endwhile;
	    ?>		   
	    
	    <div class="pagination">
			<? if ($paging->getTotalPages() > 1 ): ?>		
				<div class="page_markers">
					<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . count($obj_travelers) ?>
				</div>
				<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
			<? endif; ?>	
		</div>		
		
	</div>
	<? endif; ?>
	
</div>
