<?
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
?>
<div id="wrapper1">
	<div id="wrapper2">
		<div id="maincol">
			<div id="leftcol"></div>
			<div id="centercol">
				<br />
				<table border="1" align="center">
					<tr>
						<td colspan="8">
							<form name="frmFilter" method="post" action="/admin/facebook/index.php">
								Show : <select name="cmbShow">
									<option value="all">All</option>
									<option value="facebook_users_only">Facebook Users Only</option>
									<option value="goabroad.net_users_only">GoAbroad.net Users Only</option>
									<option value="synchronized_users">Synchronized Users</option>
									<option value="users_with_facebook_account">Users with Facebook Account</option>
								</select>
								Sort :<select name="cmbSort">
									<option value="dateAdded" <?= ($sortField=='dateCreate')?'selected':'' ?>>Date Added</option>
									<option value="slambookUserID" <?= ($sortField=='slambookUserID')?'selected':'' ?>>SlambookUserID</option>
									<option value="genID" <?= ($sortField=='genID')?'selected':'' ?>>GenID(FacebookID/TravelerID)</option>
								</select>
								Sort Type: <select name="cmbSortType">
									<option value="asc" <?= ($sortType=='asc')?'selected':'' ?>>Ascending</option>
									<option value="desc" <?= ($sortType=='desc')?'selected':'' ?>>Descending</option>
								</select>
								&nbsp;<input type="submit" name="submit" value="<< Go >>">
							</form>
						</td>
					</tr>
					<tr align="center">
						<td><strong>SlambookUserID</strong></td>
						<td><strong>Date Added</strong></td>
						<td><strong>Type</strong></td>
						<td><strong>FacebookID/TravelerID</strong></td>
						<td><strong>Name</strong></td>
						<td><strong>Gender</strong></td>
						<td><strong>Profile Box</strong></td>
						<td><strong>Actions</strong></td>
					</tr>
					<?php foreach($users as $iUser):?>
						<tr>
							<td align="center"><?= $iUser->getSlambookUserID() ?></td>
							<td align="center"><?= $iUser->getDateAdded() ?></td>
							<td align="center"><?= $userTypes[$iUser->getUserType()] ?></td>
							<td align="center"><?= $iUser->getGenID() ?></td>
							<td><?= $iUser->getName() ?></td>
							<td align="center"><?= $iUser->getSex() ?></td>
							<td align="center">
								<a href="#">Generate Default</a><br />
								<a href="#">Generate Custom </a>
							</td>
							<td align="center">
								<a href="#">Show Answers</a><br />
								<a href="#">Delete</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
				<br />
			</div>
		</div>
	</div>
</div>