<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::includeDependentJs('/min/f=js/prototype.js');
	Template::includeDependentJs('/js/interactive.form.js');
?>
<?php if ( strlen($msg) ): ?>
	<div class="help_text">
			<?php echo $msg; ?>
	</div>
<?php elseif ( $isRegister ): ?>
	<p class="help_text">
		<strong>Welcome to the GoAbroad Network!</strong>. You may now login with 
		your registered username and password. 
	</p>
<?php endif; ?>	
<form name="test" action=<?= "\"". $action . "\"" ?> method="post" class="interactive_form">
	<ul class="form<?php if($isRegister): ?> highlight<?php endif;?>">
		<li>
			<label for="txtUsrName">Username:</label>
			<input type="text" name="txtUsrName" id="txtUsrName" value="" size="15" class="text" />
		</li>
		<li>
			<label for="pwdPaswrd">Password:</label>
			<input name="pwdPasWrd" id="pwdPasWrd" type="password" size="15" class="text"  />
			<p><a href="forgot-password.php" title="Unable to login to your travel blog? Request to change your password.">Forgot password?</a></p>
		</li>
		<li class="actions">
				<input name="btnSubmit" type="submit" class="submit" value="Login" align="middle" />
				<!--<button name="btnSubmit" type="submit" class="submit" value="Login">Login</button>-->
				<input name="login" value="Log In" type="hidden" />
				<input type="hidden" name="hdnLoginType" value="<?= $hdnLoginType; ?>" />
				<input type="hidden" name="hdnReferer" value="<?= $hdnReferer; ?>" />
		</li>
	</ul>
</form>

