<?php
	/********************************************************/
	/*	@author:		Cheryl Ivy Q. Go					*/
	/*	Date Created:	30 September 2008					*/
	/*	Purpose:		template for viewFeaturedDates.php	*/
	/********************************************************/

	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');

	Template::includeDependentJs("/js/jquery-1.1.4.pack.js");
	Template::includeDependentJs("/js/date.js");
	Template::includeDependentJs("/js/jquery.datePicker.js");

	$code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	jQuery(function(){
		var d = new Date();
		jQuery('.date-pick').datePicker();

		//var today = jQuery('.date-pick').val();
		//alert(jQuery('.date-pick').dpGetSelected());

		jQuery('.date-pick').dpSetStartDate('2008-01-01');						// set start date as January 1, 2008
		jQuery('.date-pick').dpSetEndDate(d.addDays(0).asString());				// set end date as date today
    });
//]]>
</script>
BOF;
	Template::includeDependent($code);
	$d = new GaDateTime();

	include_once('travellog/views/admin/tpl.AdminHeader.php');
	require_once 'travellog/model/admin/Class.FeaturedSection.php';
	require_once 'travellog/UIComponent/Photo/facade/PhotoUIFacade.php';

	if ($sectionID == FeaturedSection::$JOURNAL_ENTRY)
		$mSection = 'Journal Entries';
	else if ($sectionID == FeaturedSection::$PHOTO)
		$mSection = 'Photos';
	else if ($sectionID == FeaturedSection::$TRAVELER)
		$mSection = 'Travelers';
	else if ($sectionID == FeaturedSection::$GROUP)
		$mSection = 'Groups';
	else
		$mSection = 'All';

	$dateFeatured = '';

	$Photo = new PhotoUIFacade();
	$Photo->setContext('home');
	$Photo->setGenID(0);
	$Photo->setLoginID(0);
	$Photo->build();
?>

<div id = "content_wrapper" class = "layout_2">
	<div id = "wide_column">
		<h2>
			<?=$mSection . ' Featured (' . count($featured) . ')'?>
		</h2>
		<div class = "section">
			<div class = "content">
				<? if (0 < count($featured)) : ?>
					<div id = "groups_list_main">
						<ul>						
							<? foreach ($featured as $eachFeatured) : ?>
									<li>
										<? if (FeaturedSection::$GROUP == $eachFeatured['sectionID']) : ?>
											<? $mGroup = $eachFeatured['feature'] ?>
											<? if (is_numeric($mGroup)) : ?>
												<p>
													<h4 style = "text-align: center; font-size: 12px; padding-top: 30px;">
														Group with groupID <?=$mGroup?> no longer exists!
													</h4>
												</p>
											<? else : ?>
												<?
													$mPhotolink = $mGroup->getGroupPhoto()->getPhotoLink();
													if (0 != strncmp("http://", $mPhotolink, "7"))
														$mPhotolink = "http://" . $_SERVER['SERVER_NAME'] . "/" . $mPhotolink;
												?>
												<a class = "group_thumb" href = "/group.php?gID=<?=$mGroup->getGroupID()?>">
													<img src = "<?=$mPhotolink?>" alt = "<?= $mGroup->getName() ?>" />
												</a>
												<div class = "groups_info">
													<strong>
														<a href = "/group.php?gID=<?=$mGroup->getGroupID()?>">
															<?=$mGroup->getName()?>
														</a>
													</strong>
													<hr width = "95%" align = "left" />
													<p class = "gray">
														<span style = "font-weight:bold">Group Section</span>&nbsp;&nbsp;
														<? if (ViewFeaturedDatesController::displayViewLink($mGroup->getGroupID(), 1)) : ?>
															<a
																href	= "javascript: void(0)"
																class	= "edit"
																onClick = "window.open('viewFeaturedDates.php?sectionID=1&type=1&genID=<?=$mGroup->getGroupID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
																[view dates]
															</a>
														<? else : ?>
															[featured once]
														<? endif; ?>
													</p>
													<p class = "gray">
														<span style = "font-weight:bold">Last Login: </span>
														<?=$d->set($mGroup->getAdministrator()->getTravelerProfile()->getLastLogin())->friendlyFormat()?>
													</p>													
													<?
														$grpCategoryList = $mGroup->getGroupCategory(true);
														$arrType = array();

														if (count($grpCategoryList)) :
															foreach($grpCategoryList as $key => $value)
																$arrType[] = $value;
															$lstType = implode(', ', $arrType);
													?>
														<p class = "gray">
														 	<span style = "font-weight:bold">Type: </span>
															<?=$lstType?>
														</p>
													<? endif; ?>
													<p style = "margin-top:10px">
														<? if (TRUE == $mGroup->isSuspended()) : ?>
															&raquo;<strong>[Suspended]</strong>
															<a href="groups.php?groupID=<?=$mGroup->getGroupID()?>&amp;activate" onclick="javascript:return confirm('Do you want to ACTIVATE this Group?');">Activate Group</a>
														<? else : ?>								
															&raquo;<a href="groups.php?groupID=<?=$mGroup->getGroupID()?>&amp;suspend" onclick="javascript:return confirm('Do you want to SUSPEND this Group?');">Suspend Group</a>
														<? endif; ?>
													</p>
												</div>
												<div class = "travbox_info">
													<p class = "gray">
														<? if (Group::FUN ==  $mGroup->getDiscriminator() ) : ?>
															<span style = "font-weight:bold">Administrator:</span>
															<div style = "padding-top: 5px">
												            	<?
																	try {
											                    		$mAdminProfile = $mGroup->getAdministrator()->getTravelerProfile();
																	}
																	catch (Exception $e) {}
										                        ?>
																<?
																	$mPhotolink = $mGroup->getGroupPhoto()->getPhotoLink();
																	if (0 != strncmp("http://", $mPhotolink, "7"))
																		$mPhotolink = "http://" . $_SERVER['SERVER_NAME'] . "/" . $mPhotolink;
																?>
																<a href = "/<?=$mAdminProfile->getUserName() ?>" class = "user_thumb">
																	<img src = "<?=$mAdminProfile->getPrimaryPhoto()->getPhotoLink('thumbnail')?>" width = "37" height = "37" alt = "User Profile Pic" title = "View Profile" />
																</a>
																<a href="/<?=$mAdminProfile->getUserName() ?>" class="username">
																	<?
																		$mUserName = (15 < strlen($mAdminProfile->getUserName()))
																						? substr($mAdminProfile->getUserName(), 0, 15) . '..'
																						: $mAdminProfile->getUserName();
																		echo $mUserName;
																	?>
																</a>
															</div>
														<? endif; ?>
													</p>
												</div>
											<? endif; ?>

										<? elseif (FeaturedSection::$TRAVELER == $eachFeatured['sectionID']) : ?>
											<? $mTraveler = $eachFeatured['feature'] ?>
											<? if (is_numeric($mTraveler)) : ?>
												<p>
													<h4 style = "text-align: center; font-size: 12px; padding-top: 30px;">
														Traveler with travelerID <?=$mTraveler?> no longer exists!
													</h4>
												</p>
											<? else : ?>
												<?
													$mPhotolink = $mTraveler->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink();
													if (0 != strncmp("http://", $mPhotolink, "7"))
														$mPhotolink = "http://".$_SERVER['SERVER_NAME'] . "/" . $mPhotolink;
												?>
												<a class = "thumb" href = "/<?=$mTraveler->getUsername()?>">
													<img src = "<?=$mPhotolink?>" alt = "<?=$mTraveler->getUsername()?>" style = "width: 95px; height: 105px; float: left" />
												</a>
												<div class = "groups_info" style = "padding-left: 40px;">
													<h3>
														<a href = "/<?=$mTraveler->getUsername()?>"><?=$mTraveler->getUsername()?></a>
													</h3>
													<hr width = "95%" align = "left" />
													<p class = "gray">
														<strong>Traveler Section</strong>&nbsp;&nbsp;
														<? if (ViewFeaturedDatesController::displayViewLink($mTraveler->getTravelerID(), 2)) : ?>
															<a
																href	= "javascript: void(0)"
																class	= "edit"
																onClick = "window.open('viewFeaturedDates.php?sectionID=2&type=1&genID=<?=$mTraveler->getTravelerID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
																[view dates]
															</a>
														<? else : ?>
															[featured once]
														<? endif; ?>
													</p>
													<p>
														<? if (TRUE == $mTraveler->isSuspended()) : ?>
															&raquo;<a href="travelers.php?travID=<?=$mTraveler->getTravelerID()?>&amp;activate" onclick="javascript:return confirm('Do you want to ACTIVATE this Traveler?');">Activate Traveler</a>
														<? else : ?>
															&raquo;<a href="travelers.php?travID=<?=$mTraveler->getTravelerID()?>&amp;suspend" onclick="javascript:return confirm('Do you want to SUSPEND this Traveler?');">Suspend Traveler</a>
														<? endif; ?>
													</p>
													<p>
														&raquo;<a href="messages.php?compose&travTo=<?=$mTraveler->getTravelerID()?>">Send Message</a>
													</p>
												</div>
												<div class = "travbox_info">
													<p class = "gray">
														<?=$mTraveler->getCountTravelJournals()?>
														<?=(1 < $mTraveler->getCountTravelJournals()) ? ' Journals' : ' Journal'?>
														&nbsp;|&nbsp;
														<?=$mTraveler->getCountTravelLogs()?>
														<?=(1 < $mTraveler->getCountTravelLogs()) ? ' Entries' : ' Entry'?>
													</p>
													<p class = "gray">
														Member Since: 
														<?= $d->set($mTraveler->getTravelerProfile()->getDateJoined())->friendlyFormat()?>
													</p>
													<p class = "gray">
														Last Login: 
														<?= $d->set($mTraveler->getTravelerProfile()->getLastLogin())->friendlyFormat()?>
													</p>
												</div>
											<? endif; ?>

										<? elseif (FeaturedSection::$JOURNAL_ENTRY == $eachFeatured['sectionID']) : ?>
											<? $mEntry = $eachFeatured['feature'] ?>
											<? if (is_numeric($mEntry)) : ?>
												<p>
													<h4 style = "text-align: center; font-size: 12px; padding-top: 30px;">
														Journal entry with entryID <?=$mEntry?> has been deleted!
													</h4>
												</p>
											<? else : ?>
												<a class = "thumb" href = "../journal-entry.php?action=view&amp;travelerID=<?=$mEntry->getTravelerID()?>&amp;travellogID=<?=$mEntry->getTravelLogID()?>" style = "float: left">
													<img alt = "Journal Entry Photo" src="<?=$mEntry->getRandomPhoto()->getPhotoLink()?>" style = "height: 100px; width: 150px" />
												</a>
												<div class = "groups_info" style = "margin-left: 15px; width: 300px">
													<h3>
														<a href = "../journal-entry.php?action=view&amp;travelerID=<?=$mEntry->getTravelerID()?>&amp;travellogID=<?=$mEntry->getTravelLogID()?>">
															<?
																$mTitle = (50 < strlen($mEntry->getTitle()))
																			? substr($mEntry->getTitle(), 0, 50) . '..'
																			: $mEntry->getTitle();
																echo $mTitle;
															?>
														</a>
													</h3>
													<hr width = "95%" align = "left">
													<p>
														<strong>Journal Entry Section</strong>&nbsp;&nbsp;
														<? if (ViewFeaturedDatesController::displayViewLink($mEntry->getTravelLogID(), 4)) : ?>
															<a
																href	= "javascript: void(0)"
																class	= "edit"
																onClick = "window.open('viewFeaturedDates.php?sectionID=4&type=1&genID=<?=$mEntry->getTravelLogID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
																[view dates]
															</a>
														<? else : ?>
															[featured once]
														<? endif; ?>
													</p>
													<p>
														<? $cID = $mEntry->getTrip()->getLocation()->getCountry()->getCountryID()  ?>
														<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" />
														<?=$mEntry->getTrip()->getLocation()->getName()?>, 
														<?=$mEntry->getTrip()->getLocation()->getCountry()->getName()?>
													</p>
												</div>
												<div class = "travbox_info">
													<p class = "gray">
														<span><?=$d->set($mEntry->getTrip()->getArrival())->friendlyFormat()?></span>
													</p>
													<p>
														<a class = "username" href = "/<?=$mEntry->getTraveler()->getUserName()?>" >
															<?
																$mName = (35 < strlen($mEntry->getTraveler()->getUserName()))
																		? substr($mEntry->getTraveler()->getUserName(), 0, 35) . '..'
																		: $mEntry->getTraveler()->getUserName();
																echo $mName;
															?>
														</a>
													</p>
													<p>
														<a class = "journal_title" href = "/journal-entry.php?action=view&amp;travelID=<?=$mEntry->getTravelID()?>&amp;travelerID=<?=$mEntry->getTravelerID()?>">
															<?
																$mTitle = (35 < strlen($mEntry->getTravel()->getTitle()))
																			? substr($mEntry->getTravel()->getTitle(), 0, 35) . '..'
																			: $mEntry->getTravel()->getTitle();
																echo $mTitle;
															?>
														</a>
													</p>
												</div>
											<? endif; ?>

										<? elseif (FeaturedSection::$PHOTO == $eachFeatured['sectionID']) : ?>
											<? $mPhoto = $eachFeatured['feature'] ?>
											<? if (is_numeric($mPhoto)) : ?>
												<p>
													<h4 style = "text-align: center; font-size: 12px; padding-top: 30px;">
														Photo with photoID <?=$mPhoto?> has been deleted!
													</h4>
												</p>
											<? else : ?>
												<?
													$_context	= $mPhoto->getContext();
													$mCaption	= $mPhoto->getCaption();
													$mPhotolink	= $mPhoto->getPhotoLink();

													if (0 != strncmp("http://", $mPhotolink, "7"))
														$mPhotolink = "http://".$_SERVER['SERVER_NAME']. "/" . $mPhotolink;

													switch (get_class($_context)) :					
														case 'TravelLog':															
															$mGenId = $_context->getTravelID();
															$mContext = 'Journal';
															if (0 == strlen($mCaption))
																$mCaption = "Journal Entry Photo";
															$mSectionCaption = "Journal Entry Photo Section";
															$mStyle = "height: 100px; width: 150px";
															break;
														case 'TravelerProfile':
															$mGenId = $_context->getTravelerID();
															$mContext = 'Profile';
															if (0 == strlen($mCaption))
																$mCaption = "Traveler Profile Photo";
															$mSectionCaption = "Traveler Profile Photo Section";
															$mStyle = "width: 95px; height: 105px; float: left";
															break;
														case 'FunGroup':
															$mGenId = $_context->getGroupID();
															$mContext = 'FunGroup';
															if (0 == strlen($mCaption))
																$mCaption = "Group Photo";
															$mSectionCaption = "Group Photo Section";
															$mStyle = "width: 95px; height: 105px; float: left";
															break;
														case 'PhotoAlbum':
															$mGenId = $_context->getPhotoAlbumID();
															$mContext = 'Photoalbum';
															if (0 == strlen($mCaption))
																$mCaption = "Group's Photo Album Photo";
															$mSectionCaption = "Group's Photo Album Photo Section";
															$mStyle = "height: 100px; width: 150px";
															break;
														default:
															$mContext = '';
															break;	
													endswitch;
												?>
												<a class = "thumb" href = "javascript:void(0)" onclick = "PhotoService.loadPhotoPopup('<?=$mPhoto->getPhotoID()?>', 'gallery', '<?=$mGenId?>', '<?=$mContext?>')" style = "float: left">
													<img src = "<?=$mPhotolink?>" alt = "<?=$mPhoto->getCaption()?>" style = "<?=$mStyle?>" />
												</a>
												<? if ($mContext == 'Journal' || $mContext == 'Photoalbum'): ?>
													<div class = "groups_info" style = "padding-left: 15px; width: 300px">
												<? else : ?>
													<div class = "groups_info" style = "padding-left: 40px; width: 300px">
												<? endif; ?>
													<?=(50 < strlen($mCaption)) ? substr($mCaption, 0, 50) . '..' : $mCaption ?>
													<hr width = "95%" align = "left" />
													<?=$mSectionCaption?>
													<? if (ViewFeaturedDatesController::displayViewLink($mPhoto->getPhotoID(), 5)) : ?>
														<a
															href	= "javascript: void(0)"
															class	= "edit"
															onClick = "window.open('viewFeaturedDates.php?sectionID=5&type=1&genID=<?=$mPhoto->getPhotoID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
															[view dates]
														</a>
													<? else : ?>
														[featured once]
													<? endif; ?>
													<p>
														<? if (get_class($_context) == 'TravelLog') : ?>
															<? $cID = $_context->getTrip()->getLocation()->getCountry()->getCountryID()  ?>
															<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" />
															<?=$_context->getTrip()->getLocation()->getName()?>, 
															<?=$_context->getTrip()->getLocation()->getCountry()->getName()?>
														<? endif; ?>
													</p>
												</div>
												<div class = "travbox_info">
													<? if (get_class($_context) == 'TravelLog' || get_class($_context) == 'TravelerProfile') : ?>
														<p>
							 								<a
																class = "username"
																href = "/<?=$_context->getTraveler()->getUserName()?>">
																<?
																	$mName = (35 < strlen($_context->getTraveler()->getUserName()))
																			? substr($_context->getTraveler()->getUserName(), 0, 35) . '..'
																			: $_context->getTraveler()->getUserName();
																	echo $mName;
																?>
															</a>
														</p>
														<? if (get_class($_context) == 'TravelLog') : ?>
															<p>
																<a
																	class	= "journal_title"
																	href	= "/journal-entry.php?action=view&amp;travelID=<?=$_context->getTravel()->getTravelID()?>">
																		<?
																			$mTitle = (35 < strlen($_context->getTravel()->getTitle()))
																						? substr($_context->getTravel()->getTitle(), 0, 35) . '..'
																						: $_context->getTravel()->getTitle();
																			echo $mTitle;
																		?>
																</a>
															</p>
														<? endif; ?>						
													<? elseif (get_class($_context) == 'FunGroup') : ?>
														<p>
															<a href="/group.php?gID=<?=$_context->getGroupID()?>">
																<?
																	$mName = (35 < strlen($_context->getName()))
																			? substr($_context->getName(), 0, 35) . '..'
																			: $_context->getName();
																	echo $mName;
																?>
															</a>
														</p>
													<? elseif (get_class($_context) == 'PhotoAlbum') : ?>
														<?
															$mGroup		=  GroupFactory::instance()->create( array($_context->getGroupID()) );
															$relGroup	=  $mGroup[0];
														?>
														<p>
															<a href="/group.php?gID=<?=$relGroup->getGroupID()?>">
																<?
																	$mName = (35 < strlen($relGroup->getName()))
																			? substr($relGroup->getName(), 0, 35) . '..'
																			: $relGroup->getName();
																	echo $mName;
																?>
															</a>
														</p>
													<? endif; ?>
												</div>
											<? endif; ?>
										<? endif; ?>
									</li>
							<? endforeach; ?>
						</ul>
					</div>
				<? else : ?>
					<h4 style = "text-align: center; text-transform: uppercase; font-weight: bold">Your search has returned 0 rows.</h4>
				<? endif; ?>
			</div>
		</div>
	</div>

	<div id = "narrow_column">
		<div id = "search" class = "section">			
			<h2>
				<span style = "text-transform: none">Search for Travelers, Photos, Journal Entries, Groups Featured on GoAbroad.net</span>
			</h2>
			<div class = "content" style = "padding-top: 20px">
				<form name = "frmSearch" id = "frmSearch" action = "viewFeaturedDates.php?type=2" method = "POST">
					<fieldset class = "fieldset1">
						<p class = "row_wrap">
							<strong>Date: </strong>
							<input
								readonly	= "true"
								style		= "width: 109px"
								class		= "date-pick dp-applied"
								name		= "date"
								id			= "date"
								value		= "<?=$date?>"
								type		= "text" />
							<span style = "font-size: 10px; margin-left: 30px; margin-top: -10px;">&nbsp;&nbsp;(yyyy-mm-dd)</span>
						</p>
						<p class = "row_wrap">
							<strong>Section: </strong>
							<select name = "sectionID" id = "sectionID">
								<option value = "0" <?if($sectionID == 0):?>selected="true"<?endif;?>>All</option>
								<option value = "1" <?if($sectionID == 1):?>selected="true"<?endif;?>>Group</option>
								<option value = "2" <?if($sectionID == 2):?>selected="true"<?endif;?>>Traveler</option>
								<option value = "4" <?if($sectionID == 4):?>selected="true"<?endif;?>>Journal-entry</option>
								<option value = "5" <?if($sectionID == 5):?>selected="true"<?endif;?>>Photo</option>
							</select>
						</p>
						<p class = "gray">
							<input type = "submit" name = "btnSearch" id = "btnSearch" value = "Search" />
						</p>
					</fieldset>
				</form>
			</div>
			<div class = "foot"></div>
		</div>
	</div>
</div>