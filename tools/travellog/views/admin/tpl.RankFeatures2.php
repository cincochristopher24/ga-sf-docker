<?php

	require_once('Class.StringFormattingHelper.php');
	$d = new GaDateTime();
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Home');
	Template::setMainTemplateVar('layoutID', 'page_home');
	
	$style = "
			<style>									
				ul.boxy {
				width: 10em;				 
				display:block;
			}
			ul.boxy li {
				cursor:move;
				border: 1px solid #ccc;
				position: relative;		
			}
			</style>					
				";
	Template::includeDependent($style);
	Template::includeDependentJs('/js/prototype.js');
	Template::includeDependentJs('/js/scriptaculous/effects.js');
	Template::includeDependentJs('/js/scriptaculous/dragdrop.js');
	Template::includeDependentJs('/js/arrangefeatitems.js');
	
	include_once('travellog/views/admin/tpl.AdminHeader.php');
?>
	
	<div id="content_wrapper">
		
		<div id="top_wrap" class="area">
	
			<div id="intro">
				<!-- Start of Rotating Photo -->
				<? if($rotatingPhoto):?>
				<img id="rotating_photo" src="<?=$rotatingPhoto->getPhotoLink()?>" title="GoAbroad.net Photos" alt="GoAbroad.net Photos" height="324" width="980" />
				<div id="extra_image_overlay"></div>
				<div id="top_content">
					<h1><span>Share your journeys with the world!</span></h1>
					<p id="photo_caption"><?=$rotatingPhoto->getCaption()?></p>
					<p id="photo_credit">
						<? if($rotatingPhoto->getFrom()):?>Photo by: <a href="<?=$rotatingPhoto->getLink()?>"><?=$rotatingPhoto->getFrom()?></a><?endif;?>
					</p>
				</div>			
				<? else:?>
				<img id="rotating_photo" src="/images/rotating/isv_2.jpg" title="GoAbroad.net Photos" alt="GoAbroad.net Photos" height="324" width="980" />
				<div id="extra_image_overlay"></div>
				<div id="top_content">
					<h1><span>Share your journeys with the world!</span></h1>
					<p id="photo_caption">Volunteers helping build a school.</p>
					<p id="photo_credit">
						Photo by: <a href="http://www.goabroad.net/group.php?gID=1760">International Student Volunteers</a>
					</p>
				</div>
				<? endif;?>
				<!-- End of Rotating Photo -->
				<ul>
  				<li class="first">
  					<div>
  						<p>
							<strong>Keep memoirs of your travel</strong>
							while keeping your friends and family posted with Travel Journals and Travel Photos.
						</p>
  					</div>
  				</li>
  				<li>
  					<div>
  						<p>
							<strong>Meet interesting travelers</strong>
							Connect with like-minded travelers before, during and after your travels.
						</p>
					</div>
  				</li>
  				<li>
			  		<div>
  						<p>
  							<strong>Share journals on Facebook!</strong>
							Add our apps on Facebook, MySpace, Orkut and hi5 to bring your travels to your profile!
						</p>
					</div>
  				</li>
  				<li id="signup">
  					<div>
  						<p>Register, <span>it's free</span></p>
  			  			<a href="/register.php" class="sign_text" title="Record your travel experiences now! Join GoAbroad Network. It's FREE."><span>Sign-up now</span></a>
		  			</div>
				</li>
				</ul>
			</div>			
					
		</div> <!--  End id="top_wrap" class="area"  -->
							
		<div id="rundown">				
							
			<div id="wide_column">
							
				<div class="section journal" id="journals">
					<? if (count($latestLog)) { ?>
						<h2>
							<span>
							<? if (strlen($featEntrySection->getCaption())) : ?>
								<?=$featEntrySection->getCaption()?>
							<? else : ?>
								<? $uname = $latestLog[0]->getTraveler()->getUserName() ?>
								<?=strlen($uname)-1 == strripos($uname,'s')?$uname."'":$uname."'s"; ?> Journal Entry From 
									<?= $latestLog[0]->getTrip()->getLocation()->getCountry()->getName(); ?>											
							<? endif; ?>	
							</span>
							<span class="header_actions">
								<a href="/journal.php" class="more" title="Read more travel experiences from other GoAbroad Network travelers.">More Travel Journals</a>	
							</span>								
						</h2>								
					<? } ?>	
					<div class="content">
						<ul id="entryID" class="sortable boxy" style="width:646px;">
						<? 
						for ($idxlog=0; $idxlog<count($latestLog); $idxlog++) {
							$idxlatestLog = 	$latestLog[$idxlog];
							$owner = $idxlatestLog->getOwner();
							$exclude = $idxlatestLog->getTraveler()->isSuspended();
							$exclude = ($owner instanceOf Group) ? $owner->IsSuspended() : $exclude;
							if($exclude) continue;						
						?>
							<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$JOURNAL_ENTRY, $idxlatestLog->getTravelLogID())?>" style="padding:9px 0;">
							<h3 style="font-size:16px;font-weight:bold;margin-top:5px;">
								<a href="/journal-entry.php?action=view&amp;travelerID=<?= $idxlatestLog->getTravelerID() ?>&amp;travellogID=<?= $idxlatestLog->getTravelLogID() ?>" title="Travel blog - <?= $idxlatestLog->getTitle(); ?>">
									<?= $idxlatestLog->getTitle() ?>
								</a>
							</h3>
					
							<div class="meta">
								<? $cID = $idxlatestLog->getTrip()->getLocation()->getCountry()->getCountryID()  ?>
								<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="Travel journals from <?= $idxlatestLog->getTrip()->getLocation()->getCountry()->getName(); ?>" />
								<span><?
								$cityName    = $idxlatestLog->getTrip()->getLocation()->getName(); 
								$countryName = $idxlatestLog->getTrip()->getLocation()->getCountry()->getName();
								if( strcasecmp($cityName, $countryName) == 0 )
									echo $countryName;
								else
									echo $cityName . ', ' . $countryName;
								?></span>
								|
								<?= $d->set($idxlatestLog->getTrip()->getArrival())->friendlyFormat()?>						 
							</div>
					        
					        <div class="photo">
								<?php // Added to give meaningful alt text for journal entry photos(Ismael Casimpan Sept 11, 2007)
								    if (strlen($idxlatestLog->getTitle()) > 21):
									    $_altTextFeaturedJournalEntryPhoto = substr($idxlatestLog->getTitle(),0, 21).'...';
								    else:
									    $_altTextFeaturedJournalEntryPhoto = $idxlatestLog->getTitle();
								    endif;   
							    ?>
							    <?php if( strlen(trim($idxlatestLog->getRandomPhoto()->getPhotoLink("default"))) && substr($idxlatestLog->getRandomPhoto()->getPhotoLink("default"), -13) != '164_photo.jpg' ):?>
								<a href="/journal-entry.php?action=view&amp;travelerID=<?= $idxlatestLog->getTravelerID() ?>&amp;travellogID=<?= $idxlatestLog->getTravelLogID() ?>">
									<img alt="Travel journals and travel notes - <?= $_altTextFeaturedJournalEntryPhoto ?>" title="Travel experiences - <?= $idxlatestLog->getTitle(); ?>" src="<?= $idxlatestLog->getRandomPhoto()->getPhotoLink("default") ?>"  />
								</a>									
								<?php endif;?>								 				 
							</div>
					        
							<p><?= StringFormattingHelper::wordTrim($idxlatestLog->getDescription(), 70) . '&#8230;' ?></p>
					
							<div class="author" style="color:#888888;font-weight:bold;">
								<?php
									$_tmpUsernamePossessive = StringFormattingHelper::possessive($idxlatestLog->getTraveler()->getUserName());
								?>
								<? if($idxlatestLog->getTraveler()->isAdvisor()) { ?>
									<?php
										//$advisor = new Advisor($idxlatestLog-> getTravelerID());	
										//$advisorgroup = $advisor->getAdvisorGroup();											
										$advisorgroup = $owner;
									?>
									
									advisor:
									
									<a class="username" href="<?= $advisorgroup->getFriendlyURL()?>" title="Read <?= $advisorgroup->getName() ?> travel blog and travel notes">	
										<?= $advisorgroup->getName() ?>						 
									</a>
									
								<? } else { ?>
									
									
									<img class="pic" alt="<?= $_tmpUsernamePossessive ?> Profile" width="37" height="37" title="Read <?= $_tmpUsernamePossessive ?> travel experiences and travel notes" src="<?= $idxlatestLog->getTraveler()->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail')  ?>" />
									<a class="username" href="/<?= $idxlatestLog->getTraveler()->getUserName() ?>" title="Read <?= $_tmpUsernamePossessive ?> travel blog and travel notes">	
										<?= $idxlatestLog->getTraveler()->getUserName() ?>
									</a>
									
								<? } ?>
								<br />
								journal:
								<a class="journal_title" href="/journal-entry.php?action=view&amp;travelID=<?= $idxlatestLog->getTravelID() ?>&amp;travelerID=<?= $idxlatestLog->getTravelerID() ?>" title="<?= $_tmpUsernamePossessive ?> travel experience: <?= $idxlatestLog->getTravel()->getTitle() ?>" >
									<?= $idxlatestLog->getTravel()->getTitle() ?> 
								</a>						 
							</div>
						</li>
						<? } ?>
					</ul>
					<div class="section_foot">
						<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$JOURNAL_ENTRY?>)">
							<input type="hidden" name="featItemsID" id="featentryID" value="">	
							<input type="hidden" name="sectionID" value="<?=FeaturedSection::$JOURNAL_ENTRY?>">
							<input  type="submit" value="Save Changes" name="rearrange"/>
						</form>
					</div>
					<div class="foot"></div>
						
				</div>	 <!-- End content -->	
			</div><!-- End section journal journals -->
			
			<div class="section photos" id="featured_photos">
			
				<h2><span><?=$featPhotoSection->getCaption()?></span></h2>
				
					<div class="content">
								
						<ul id="photoID" class="sortable boxy" width="164px" style="display:inline;">
						
						<? for ($idxphoto=0; $idxphoto<count($featPhoto); $idxphoto++) { 
							$idxfeatPhoto = $featPhoto[$idxphoto];
							$_context = $idxfeatPhoto->getContext();
							
							$photocount = count($_context->getPhotos());
							$TP_Offset = 1;
							
							if($photocount > 0 ){
								foreach($_context->getPhotos() as $off => $tp_photo){
									if($idxfeatPhoto->getPhotoID() == $tp_photo->getPhotoID()){
											$TP_Offset = $off + 1;
										break;
									}									
								}								
							}

							switch(get_class($_context)) :					
								case 'TravelLog':
									$mGenId = $_context->getTravelID();
									$mContext = 'Journal';
									break;
								case 'TravelerProfile':
									$mGenId = $_context->getTravelerID();
									$mContext = 'Profile';
									break;
								case 'FunGroup':
									$mGenId = $_context->getGroupID();
									$mContext = 'Fungroup';
									break;
								case 'PhotoAlbum':
									$mGenId = $_context->getPhotoAlbumID();
									$mContext = 'Photoalbum';
									break;
								default:
									$mContext = '';
									break;	
							endswitch;
						?>
							<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$PHOTO, $idxfeatPhoto->getPhotoID())?>" style="display:inline;float:left;" >
							<div class="photo"  style="float:left;">
								<span class="thumbnail">
							        <img class="photo_bg" src="<?= $idxfeatPhoto->getPhotoLink('featured') ?>"  alt="Featured Travel Photo - <?php if ($idxfeatPhoto->getCaption()) echo $idxfeatPhoto->getCaption(); ?>" />
							    </span>
								<div class="info">
								    <div class="caption">
										<?	if ($idxfeatPhoto->getCaption())
											 echo $idxfeatPhoto->getCaption();
										?>
									</div>
								
									<?
									switch(get_class($_context)){
										
										case 'TravelLog':
									?>	
										<? if (get_class($_context->getOwner()) == 'Traveler') : ?>
											traveler: <a class="username" href="/<?= $_context->getTraveler()->getUserName() ?>" title="Travel notes of <?= $_context->getTraveler()->getUserName() ?>" > <?= $_context->getTraveler()->getUserName() ?> </a>														
										<? elseif (get_class($_context->getOwner()) == 'AdminGroup') : ?>
											<a href="/<?=$_context->getOwner()->getFriendlyURL()?>" title="Learn more about the <?=$_context->getOwner()->getName()?> travel community"><?=$_context->getOwner()->getName()?></a>
										<? endif;?>
										<br />
										<a class="journal_title" href="/journal-entry.php?action=view&amp;travelID=<?= $_context->getTravel()->getTravelID() ?>" title="Travel notes - <?= $_context->getTravel()->getTitle()?>"><?= $_context->getTravel()->getTitle() ?></a>
									<? 		break;
										case 'TravelerProfile':
									?>
										traveler: <a class="username" href="/<?= $_context->getTraveler()->getUserName() ?>" title="Travel notes of <?= $_context->getTraveler()->getUserName() ?>" > <?= $_context->getTraveler()->getUserName() ?> </a>	
									<?		break;
										case 'FunGroup':
									?>
											<a href="<?=$_context->getFriendlyURL()?>" title="Learn more about the <?=$_context->getName()?> travel community"><?=$_context->getName()?></a>
									<?		break;
										case 'PhotoAlbum':
									?>
										advisor:
										<?
											$mGroup		=  GroupFactory::instance()->create( array($_context->getGroupID()) );
											$relGroup   =  $mGroup[0];
										?>
										<a href="<?=$relGroup->getFriendlyURL()?>" title="Learn more about the <?=$relGroup->getName()?> travel community"><?=$relGroup->getName()?></a>
									<?		break;	
									}
									?>
									<p>
									<? if(0 < strlen($mContext)) : ?>
										<a href = "javascript:void(0)" onclick = "PhotoService.loadPhotoPopup('<?=$idxfeatPhoto->getPhotoID()?>', 'gallery', '<?=$mGenId?>', '<?=$mContext?>');">[view photos]</a>
									<? endif; ?>
									</p>
								</div><!-- End info -->
							
							
							</div> <!-- End photo -->
							</li>
							<? if (0 == (($idxphoto + 1) % 3)) :?>
							<div class="clear"></div>
							<? endif; ?>
						<? } ?>
						</ul>
						
						<div class="foot"></div>
					
					</div> <!-- End content -->
					<div class="clear"></div>
						<div class="section_foot">
							<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$PHOTO?>)">
								<input type="hidden" name="featItemsID" id="featphotoID" value="">	
								<input type="hidden" name="sectionID" value="<?=FeaturedSection::$PHOTO?>">
								<input  type="submit" value="Save Changes" name="rearrange"/>
							</form>
						</div>
				<div class="foot"></div>
					
				</div> <!-- End section photos featured_photos -->
					
			</div> <!--  End wide_column  -->
			
			<div id="narrow_column">
					
		        <div class="section" id="featured_travelers">
		
					<h2>
					    <span><?=$featTravelerSection->getCaption()?></span>
					    <span class="header_actions">
							<a href="/travelers.php" class="more" title="Learn more from the travel experiences of other GoAbroad network travelers. Read their travel reviews and notes.">More Travelers</a>
						</span>
					</h2>
					<div class="content">
						<ul id="travelerID" class="sortable boxy users" style="width:268px; ">
						<? 
						for ($idxtravprof=0; $idxtravprof<count($featTraveler); $idxtravprof++) { 
							$featTravProfile = $featTraveler[$idxtravprof]->getTravelerProfile();					
						?>
							<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$TRAVELER, $featTravProfile->getTravelerID() )?>">
								<?php
									$_tmpUsernamePossessive = StringFormattingHelper::possessive($featTravProfile->getUserName());
								?>
								<a class="thumb" href="/<?= $featTravProfile->getUserName() ?>" >
									<img class="pic" alt="<?= $_tmpUsernamePossessive ?> Travel Journals" title="Read <?= $_tmpUsernamePossessive ?> travel experiences and travel notes" src="<?= $featTravProfile->getPrimaryPhoto()->getPhotoLink('thumbnail')  ?>" />
								</a>
								<div class="details" style="width:150px;">
								    <strong><a class="username" href="/<?= $featTravProfile->getUserName() ?>" title="Read <?= $_tmpUsernamePossessive ?> travel blogs and travel notes"><?= $featTravProfile->getUserName() ?></a></strong>
									<? if ($featTravProfile->getHTLocationID()) { ?>
										<? $cID = $featTravProfile->getCity()->getCountry()->getCountryID()  ?>
										<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="Travel notes from <?= $featTravProfile->getCity()->getCountry()->getName(); ?>" />
									<? } ?>
								</div>	
								
							</li>
							
						<? } ?>
						</ul>
					</div> <!--  End content  -->
					<div class="section_foot">
						<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$TRAVELER?>)">
							<input type="hidden" name="featItemsID" id="feattravelerID" value="">	
							<input type="hidden" name="sectionID" value="<?=FeaturedSection::$TRAVELER?>">
							<input  type="submit" value="Save Changes" name="rearrange"/>
						</form>
					</div> <!-- End section_foot -->
					<div class="foot"></div>
	
				</div>  <!--  End section featured_travelers  -->
				
				<div class="section" id="featured_group">
					<h2><span><?=$featGroupSection->getCaption()?></span></h2>
					<div class="content">
						<ul id="groupID" class="sortable boxy" style="width:268px; ">
						<? for ($idxgrp=0; $idxgrp<count($featGroup); $idxgrp++) { 
							$idxfeatGroup = $featGroup[$idxgrp];								
						?>
						<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$GROUP, $idxfeatGroup->getGroupID())?>">
							<div class="photo">
								<a class="thumb" href="/group.php?gID=<?= $idxfeatGroup->getGroupID() ?>"   style="float:right;margin-left:5px;">
									<img alt="Featured Group Photo" src="<?= $idxfeatGroup->getGroupPhoto()->getPhotoLink('standard') ?>" />
								</a>
							</div>
							<h3><a href="/group.php?gID=<?= $idxfeatGroup->getGroupID() ?>" > 
									<?= $idxfeatGroup->getName() ?> 
								</a>
							</h3>
							<p style="color:#999999;font-size:11px;line-height:16px;margin:5px 0;">
								<?= StringFormattingHelper::wordTrim($idxfeatGroup->getDescription(), 40)?><? if (40 < count(explode(' ', strip_tags($idxfeatGroup->getDescription()) ) ) ) : ?><?='&#8230;' ?><? endif ; ?> <br /> <a href="<?= $idxfeatGroup->getFriendlyURL() ?>" title="Read more about the <?= $idxfeatGroup->getName() ?> travel community">Read More...</a>
							</p>
							<div class="clear"></div>	
						</li>
						<? } ?>
						</ul>
						<div class="section_foot">
							<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$GROUP?>)">
								<input type="hidden" name="featItemsID" id="featgroupID" value="">	
								<input type="hidden" name="sectionID" value="<?=FeaturedSection::$GROUP?>">
								<input  type="submit" value="Save Changes" name="rearrange"/>
							</form>
							<a href="/group.php" class="more"  title="Search for other travel communities and groups.">More Travel Groups</a>
						</div> <!-- End section_foot -->
					</div> <!-- End content -->
				</div>  <!-- End section featured_group -->		
				<div class="foot"></div>		
			</div> <!-- End narrow_column -->
		</div> <!-- End rundown -->
	</div> <!-- End content_wrapper -->
	
	
	<script type="text/javascript">
	   Sortable.create("travelerID",{dropOnEmpty:true,containment:["travelerID"],constraint:false});	
	   Sortable.create("groupID",{dropOnEmpty:true,containment:["groupID"],constraint:false});
	   Sortable.create("photoID",{dropOnEmpty:true,containment:["photoID"],constraint:false});
	   Sortable.create("entryID",{dropOnEmpty:true,containment:["entryID"],constraint:false});		   
	</script>