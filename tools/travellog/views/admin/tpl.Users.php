<?php
	/**
	 * @author Cheryl Ivy Q. Go
	 * tpl.Users.php
	 * 12 September 2007  5:15 PM
	 */

	Template::setMainTemplateVar('title', 'View GoAbroad.net Travelers and Advisors');
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'page_travelers');
	Template::includeDependentJs('/js/calendar.js');
	Template::includeDependentCss('/css/calendar_default.css');
	Template::includeDependentCss('/css/calendar.css');
	$code =<<<BOF
		<style type="text/css">
			div.markerTooltip, div.markerDetail {
		        	color: black;
					font-weight: bold;
					background-color: white;
					white-space: nowrap;
					margin: 0;
					padding: 2px 3px;
					border: 1px solid;
		     }
		</style>  
BOF;
	Template::includeDependent($code);
	$d = new GaDateTime();

	if (0 == $cmbStatus)
		$section = 'INACTIVE ';
	else if (1 == $cmbStatus)
		$section = 'ACTIVE ';
	else
		$section = 'ALL ';
	if (0 == $cmbType)
		$section = $section . 'ADVISORS';
	else if (1 == $cmbType)
		$section = $section . 'TRAVELERS';
	else
		$section = $section . 'TRAVELERS AND ADVISORS';
?>

<div class = "area" id = "intro">
	<div align = "right" class = "section">
		<?include_once 'travellog/views/admin/tpl.AdminHeader.php';?>
	</div>
	<div class = "section" align = "center" style = "margin-left:auto; margin-right: auto; width: 990px;">
		<h1> <?=$section?> </h1>
		<h2> QUANTITY : <?=$cntTraveler?> </h2>
	</div>
</div>

<div id = "top" class="area" style = "float:none;">
	<div id="search_and_results_area" style = "margin-left:auto;margin-right:auto;width: 1000px;">
		<div class="wrapper_3">
			<div class="wrapper_2">
				<div class="wrapper">
					
					<div class="section" id="top_controls">
						<div class="content">
							<form action = "users.php" id = "frmSearch" name = "frmSearch" method = "post">
								<ul class="form" id="top_controls_filter">
									<li>
										<fieldset>
											<span>
												<label for = "cmbType">Account Type</label>
											</span>
											<span>
												<select name = "cmbType" id = "cmbType">
													<option value = "0" <?if(0 == $cmbType):?> selected = "yes" <?endif;?>> Advisor </option>
													<option value = "1" <?if(1 == $cmbType):?> selected = "yes" <?endif;?>> Traveler </option>
													<option value = "2" <?if(2 == $cmbType):?> selected = "yes" <?endif;?>> All </option>
												</select>
											</span>
											<span style = "padding-left: 50px;">
												<label for = "cmbStatus">Status</label>
											</span>
											<span>
												<select name = "cmbStatus" id = "cmbStatus">
													<option value = "0" <?if(0 == $cmbStatus):?> selected = "yes" <?endif;?>> Inactive </option>
													<option value = "1" <?if(1 == $cmbStatus):?> selected = "yes" <?endif;?>> Active </option>
													<option value = "2" <?if(2 == $cmbStatus):?> selected = "yes" <?endif;?>> All </option>
												</select>
											</span>
											<span style = "padding-left: 50px;">
												<label>Date Range</label>
											</span>
											<span>
												<input type = "text" name = "txtFromDate" id = "txtFromDate" size = "12" value = "<?=$txtFromDate?>" onclick = "displayCalendar(document.frmSearch.txtFromDate,'yyyy/mm/dd',this)" readonly />
												<span class = "user"> to </span>
												<input type = "text" name = "txtToDate" id = "txtToDate" size = "12" value = "<?=$txtToDate?>" onclick = "displayCalendar(document.frmSearch.txtToDate,'yyyy/mm/dd',this)" readonly />
											</span>
											<span style = "padding-left: 50px;">
												<input type = "submit" name = "btnSearch" id = "btnSearch" value = "Search" />
											</span>
										</fieldset>
									</li>
								</ul>
								<div class="clear"></div>
							</form>
						</div>
					</div>

					<? if ( $paging->getTotalPages() > 1 ): ?>
						<div class="pagination">
							<? $paging->getFirst() ?>
							|
							<? $paging->getPrevious() ?>
							|
							<? $paging->getLinks() ?>
							|
							<? $paging->getNext() ?>
							|
							<? $paging->getLast(); ?>
						</div>
					<? endif; ?>
					
					<div class="journal_header"><span>&nbsp;</span></div> 
					<div class="content" id="travelers_list_main">
					
						<?
							$iterator->rewind();
							while($iterator->valid()):
								?>	
									<div class="section traveler" style = "width: 487px;">
										<div class="content">
											<div class="wrapper">
												<? if (!strcasecmp("traveler", get_class($iterator->current()))) : ?>
													<a href = "/<?=$iterator->current()->getUsername()?>"  title="View Profile" class="user_thumb">
														<img src="<?=$iterator->current()->getTravelerProfile()->getPrimaryPhoto()->getPhotoLink('thumbnail') ?>" class="profile_photo_thumb" alt="<?=$iterator->current()->getUsername()?>" height="37" width="37" />
													</a>
													<h2>
														<? if (strlen($iterator->current()->getUsername()) > 24): ?>
															<a href="/<?=$iterator->current()->getUsername()?>" title="<?=$iterator->current()->getUsername()?>"><?= substr($iterator->current()->getUsername(),0, 23).'...' ?></a>
														<? else: ?>
															<a href="/<?=$iterator->current()->getUsername()?>"><?=$iterator->current()->getUsername()?></a>
														<? endif; ?>
													</h2>
													<?
														$type 					= $iterator-> current()-> getTravelerType();
														$count_journals       	= $iterator-> current()-> getCountTravelJournals();
														$count_journal_entries 	= $iterator-> current()-> getCountTravelLogs();
														$journals              	= ( $count_journals > 1) ? 'Journals' : 'Journal';
														$entries               	= ( $count_journal_entries > 1) ? 'Entries' : 'Entry';

														$name = $iterator->current()->getTravelerProfile()->getFirstName().' '.$iterator->current()->getTravelerProfile()->getLastName();
														$eAdd = $iterator->current()->getTravelerProfile()->getEmail();
														$since = $d->set($iterator->current()->getTravelerProfile()->getDateJoined())->friendlyFormat();
												else : ?>
													<a href = "group.php?gID=<?=$iterator->current()->getGroupID()?>" title="View Profile" class="user_thumb">
														<img src="<?= $iterator->current()->getGroupPhoto()->getPhotoLink(); ?>" class = "group_photo_thumb" alt="<?= $iterator->current()->getName(); ?>" height = "39" width = "39" />
													</a>
													<h2>
														<? if (strlen($iterator->current()->getName()) > 24): ?>
															<a href="/group.php?gID=<?=$iterator->current()->getGroupID()?>" title="<?=$iterator->current()->getName()?>"><?= substr($iterator->current()->getName(),0, 23).'...' ?></a>
														<? else: ?>
															<a href="/group.php?gID=<?=$iterator->current()->getGroupID()?>"><?=$iterator->current()->getName()?></a>
														<? endif; ?>
													</h2>
													<?
														$grpCategoryList = $iterator->current()->getGroupCategory(true);
														$type = "";	
														if (count($grpCategoryList)){															
											 				foreach($grpCategoryList as $cat)
																  $type .= $cat.", ";
															$type = preg_replace('/, $/','',$type);	
														}
														
														//$type 					= $iterator-> current()-> getGroupCategory()-> getName();
														$count_journals        	= 0;
														$count_journal_entries 	= 0;
														$journals              	= ( $count_journals > 1) ? 'Journals' : 'Journal';
														$entries               	= ( $count_journal_entries > 1) ? 'Entries' : 'Entry';

														$name = $iterator->current()->getAdministrator()->getTravelerProfile()->getFirstName().' '.$iterator->current()->getAdministrator()->getTravelerProfile()->getLastName();
														$eAdd = $iterator->current()->getAdministrator()->getTravelerProfile()->getEmail();
														$since = $d->set($iterator->current()->getAdministrator()->getTravelerProfile()->getDateJoined())->friendlyFormat();
												endif; ?>
												<p class="traveler_type">
													<?=$type?> | <?=$count_journals?>  <?=$journals ?> : <?=$count_journal_entries?> <?=$entries ?>
												</p>
											</div>
											<div class="profile_info"> 
												<p> <?if(!strcasecmp("traveler", get_class($iterator->current()))):?> Name: <?else:?> Advisor Name: <?endif;?> <?=$name?> </p>
												<p> Email Address: <?=$eAdd?> </p>
												<p> Member Since: <?=$since?> </p>
											</div>
											<div class="clear"></div>
										</div>
									</div>									
								<?
								$iterator->next();
							endwhile;
						?>
						
						<? if ( $paging->getTotalPages() == 0): ?>
							<p align = "center"><strong>No records found!</strong></p>
						<? endif; ?>
						<div class="clear"></div>					
					</div>
					<div class="journal_header2"><span>&nbsp;</span></div>
					<? if ( $paging->getTotalPages() > 1 ): ?>
						<div class="pagination">
							<? $paging->getFirst() ?>
							|
							<? $paging->getPrevious() ?>
							|
							<? $paging->getLinks() ?>
							|
							<? $paging->getNext() ?>
							|
							<? $paging->getLast(); ?>
						</div>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
