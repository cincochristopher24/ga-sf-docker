<?php
	/**
	 * tpl.ViewSpamList.php
	 * created on 04 28, 08
	 * edits: added check/uncheck all functionality -- 03/20/2009 -- ianne
	 */
	 
	require_once 'travellog/helper/DisplayHelper.php'; 
	require_once('Class.GaDateTime.php');
	$d = new GaDateTime();
	
	include_once('travellog/views/admin/tpl.AdminHeader.php');
	
?>

<? if(isset($sub_views['MESSAGE_PAGING'])) echo $sub_views['MESSAGE_PAGING']->render(); ?>
<form action="/admin/spam-messages.php" method="GET">
	<input type="text" name="keyword" value="" />
	<input type="hidden" name="action" value="search" />
	<input type="submit" name="submit" value="Search Username" />
</form>
<a href="/admin/spam-messages.php">View All Spam Messages</a>
<?php if(0 < count($contents['spamList'])): ?>
<table>
	<form action="spam-messages.php" method="post">
	<tr><td colspan="2"><a href="javascript:void(0)" class="checked">Check All</td></tr>	
<? foreach( $contents['spamList'] as $spam): ?>
	<tr>
		<td>
			<input type="checkbox" class="chkbox" name="chkMsgId[]" id="chkMsg_<?= $spam->getID(); ?>" value="<?= $spam->getID(); ?>" />
		</td>
		<td>
			<div class="view_message">
				<table width="100%" cellspacing="0">
					<tbody class="message_info">
						<tr>
							<th scope="row" width="80">To:</th>
							<td>
								<?/* foreach( $spam->getDestination() as $dest ): ?>
									<?= displayLink($dest,$contents['ownerID'],constants::PENDING_MESSAGES); ?> 
								<? endforeach; */?>
								<?php if($spam->isForGroupStaff()): ?>
									<?php foreach( $spam->getGroups() as $iGroup ): ?>
										<?= displayMessageStaffLink($spam->getRecipients() ,$iGroup, $contents['ownerID']); ?> 
									<?php endforeach; ?>
								<?php else: ?>
									<?php foreach( $spam->getRecipients() as $iRecipient ): ?>
										<?= displayLink($iRecipient, $contents['ownerID'], constants::PENDING_MESSAGES); ?> 
									<?php endforeach; ?>
								<?php endif; ?>
							</td>
						</tr>	 
						<tr>
							<th scope="row" width="80">From:</th>
							<td><?= displayLink($spam->getSender(),$contents['ownerID']	,constants::PENDING_MESSAGES); ?></td>
						</tr>		
						<tr>
							<th scope="row">Subject:</th>
							<td><h3><?/*php echo stripslashes($spam->getTitle()); */?><?php echo stripslashes($spam->getSubject()); ?></h3></td>
						</tr>
						<tr class="border_line">
							<th scope="row">Date:</th>
							<td><?php echo $d->set($spam->getDateCreated())->standardFormat(); ?></td>
						</tr>
					</tbody>
					<tbody>
					<tr class="message_cont">
						<th scope="row" valign="top">Message:</th>
						<td><?/*php echo NL2BR(stripslashes($spam->getMessage())); */?><?php echo NL2BR(stripslashes($spam->getBody())); ?> </td>
					</tr>
					</tbody>
				</table>
			</div>
		</td>	
	</tr>
<? endforeach; ?>
	<tr><td colspan="2"><a href="javascript:void(0)" class="checked">Check All</td></tr>
	<tr>
		<td colspan='2'>
			<input type="submit" name="btnSend" value="Send" />
			<input type="submit" name="btnDelete" value="Delete" onclick="return confirm('Are you sure you want to delete the checked messages?'); " />
  		</td>
	</tr>
	</form>
</table>
<?php else: ?>
	<div><strong>No Spam Messages Found.</strong></div>
<?php endif; ?>
<? if(isset($sub_views['MESSAGE_PAGING'])) echo $sub_views['MESSAGE_PAGING']->render(); ?>