<?php

	require_once('Class.StringFormattingHelper.php');
	$d = new GaDateTime();
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'home');
	Template::setMainTemplateVar('page_location', 'Home');
	
	$style = "
			<style>									
				ul.boxy {
				width: 10em;				 
				display:block;
			}
			ul.boxy li {
				cursor:move;
				border: 1px solid #ccc;
				position: relative;		
			}
			</style>					
				";
				
				
	Template::includeDependent($style); 
	
	
	//Template::includeDependentCss('../css/lists.css');
	 
	
	
	Template::includeDependentJs('/js/prototype.js');
	Template::includeDependentJs('/js/scriptaculous/effects.js');
	Template::includeDependentJs('/js/scriptaculous/dragdrop.js');
	Template::includeDependentJs('/js/arrangefeatitems.js');
	
	
	
	/**
	Template::includeDependentJs('dd_files/coordinates.js');
	Template::includeDependentJs('dd_files/drag.js');
	Template::includeDependentJs('dd_files/dragdrop.js');
	Template::includeDependentJs('dd_files/onload.js');
	
	 */
	
	//Template::includeDependent($script); 
	
?>

<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>
	
	
				<div class="area" id="top">
				
				
				<div class="main_content_wrapper">
					<div class="section" id="intro">
						<div class="content">
							<p id="intro_p"><strong>GoAbroad Network</strong> is the newest social 
networking site for travelers. <a href="/about-us.php">Read More&#8230;</a></p>
							<div>&nbsp;</div>
							
							<div id="intro_callouts">
								<div class="callout" id="travelers_callout">
									<h2>Travelers</h2>
									<ul>
										<li>Share Travel Journals</li>
										<li>Share Travel Photos</li>
										<li>Stay Connected</li>
										<li>Meet Interesting Travelers </li>
									</ul>
									<a href="/register.php?traveler" class="callout_link"><img src="/images/traveler_link_img.gif" /></a>
								</div>
								
								<div class="callout" id="advisors_callout">
									<h2>Advisors/Planners</h2>
									<ul>
										<li>Manage Groups</li>
										<li>Send Email Broadcasts</li>
										<li>Keep Families Informed  </li>
									</ul>
									<a href="/register.php?advisor" class="callout_link">Register</a>
								</div>
							
								
								<div id="join_now">
									<a href="/register.php?traveler"><strong>Join Now. It&#8217;s Free!</strong></a>	
								</div>
								
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			
					
						
						<div class="section" id="newest_travelers">
							<h2><span>Newest Members</span></h2>
							<div class="content">
								<ul class="users">						
								<? 
								for ($idxnewmem=0; $idxnewmem<count($newestMembers); $idxnewmem++) { 
									$newMemProfile = $newestMembers[$idxnewmem]->getTravelerProfile();					
								?>
									<li>
										<a class="user_thumb" href="/<?= $newMemProfile->getUserName() ?>" >	
											<img alt="User Profile Photo" src="<?= $newMemProfile->getPrimaryPhoto()->getPhotoLink('thumbnail') ?>" width="37" height="37" />
										</a>	
											<? if (strlen($newMemProfile->getUserName()) > 12): ?>
												<a class="username" href="/<?= $newMemProfile->getUserName() ?>" title="<?=$newMemProfile->getUserName()?>"><?= substr($newMemProfile->getUserName(),0, 12).'...' ?></a>
											<? else: ?>
												<a class="username" href="/<?= $newMemProfile->getUserName() ?>" > <?= $newMemProfile->getUserName() ?> </a>
											<? endif; ?>						 
										
										<br />
										
										<? if ($newMemProfile->getHTLocationID()) { ?>
											<? $cID = $newMemProfile->getCity()->getCountry()->getCountryID()  ?>
											<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="<?= $newMemProfile->getCity()->getCountry()->getName(); ?> Flag" />												
										<? } ?>						
									</li>
									
								<? } ?>
								</ul>								
							</div>
						</div>
						
					
						<div class="section" id="featured_travelers">
																
							<h2><?=$featTravelerSection->getCaption()?></h2>
							<div class="content">
								<ul id="travelerID" class="sortable boxy users" style="width:170px; ">
								<? 
								for ($idxtravprof=0; $idxtravprof<count($featTraveler); $idxtravprof++) { 
									$featTravProfile = $featTraveler[$idxtravprof]->getTravelerProfile();					
								?>
									<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$TRAVELER, $featTravProfile->getTravelerID() )?>" >
										<a class="user_thumb" href="/<?= $featTravProfile->getUserName() ?>" >	
											<img alt="User Profile Photo" src="<?= $featTravProfile->getPrimaryPhoto()->getPhotoLink('thumbnail')  ?>" width="37" height="37" />
										</a>	
										
										<? if (strlen($featTravProfile->getUserName()) > 12): ?>
													<a class="username" href="/<?= $featTravProfile->getUserName() ?>" title="<?= $featTravProfile->getUserName() ?>"><?= substr($featTravProfile->getUserName(),0, 12).'...' ?></a>
												<? else: ?>
													<a class="username" href="/<?= $featTravProfile->getUserName() ?>" > <?= $featTravProfile->getUserName() ?> </a>
												<? endif; ?>	
																 
										<br />
										<? if ($featTravProfile->getHTLocationID()) { ?>
											<? $cID = $featTravProfile->getCity()->getCountry()->getCountryID()  ?>
											<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="<?= $featTravProfile->getCity()->getCountry()->getName(); ?> Flag" />
										<? } ?>		
										
									</li>
									
								<? } ?>
								</ul>
								<div class="section_foot">
									<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$TRAVELER?>)">
										<input type="hidden" name="featItemsID" id="feattravelerID" value="">	
										<input type="hidden" name="sectionID" value="<?=FeaturedSection::$TRAVELER?>">
										<input  type="submit" value="Save Changes" name="rearrange"/>
									</form>
									<a href="/travelers.php" class="more">More Travelers </a>
								</div>
							</div>
														
						</div>
					
					
					
					<div class="clear"></div>
					
					
				</div>
				
				<div class="area" id="rundown">
											
						<div class="main_content_wrapper">
						<div class="section journal" id="journals">
								<? if (count($latestLog)) { ?>
									<h2>
										<? if (strlen($featEntrySection->getCaption())) : ?>
											<?=$featEntrySection->getCaption()?>
										<? else : ?>
											<? $uname = $latestLog[0]->getTraveler()->getUserName() ?>
											<?=strlen($uname)-1 == strripos($uname,'s')?$uname."'":$uname."'s"; ?> Journal Entry From 
												<?= $latestLog[0]->getTrip()->getLocation()->getCountry()->getName(); ?>											
										<? endif; ?>									
									</h2>								
								<? } ?>	
								<div class="content">
									<ul id="entryID" class="sortable boxy" style="width:700px;">
									<? 
									for ($idxlog=0; $idxlog<count($latestLog); $idxlog++) {
										$idxlatestLog = 	$latestLog[$idxlog];									
									?>
										<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$JOURNAL_ENTRY, $idxlatestLog->getTravelLogID())?>">
										<div class="journal_summary">
											<div class="photo">
												<a href="/journal-entry.php?action=view&amp;travelerID=<?= $idxlatestLog->getTravelerID() ?>&amp;travellogID=<?= $idxlatestLog->getTravelLogID() ?>" >
													<img alt="Journal Entry Photo" src="<?= $idxlatestLog->getRandomPhoto()->getPhotoLink("default") ?>"  />
												</a>																	 				 
											</div>
											
											<h3>
												<a href="/journal-entry.php?action=view&amp;travelerID=<?= $idxlatestLog->getTravelerID() ?>&amp;travellogID=<?= $idxlatestLog->getTravelLogID() ?>" >
													<?= $idxlatestLog->getTitle() ?>
												</a>
											</h3>
											
											<div class="meta">
												<? $cID = $idxlatestLog->getTrip()->getLocation()->getCountry()->getCountryID()  ?>
												<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="<?= $idxlatestLog->getTrip()->getLocation()->getCountry()->getName(); ?> Flag" />
												<?= $idxlatestLog->getTrip()->getLocation()->getName(); ?>, <?= $idxlatestLog->getTrip()->getLocation()->getCountry()->getName(); ?>
												<br />
												<?= $d->set($idxlatestLog->getTrip()->getArrival())->friendlyFormat()?>						 
											</div>
											
											<p><?= StringFormattingHelper::wordTrim($idxlatestLog->getDescription(), 100) . '&#8230;' ?></p>
											
											<div class="author">
												<a class="username" href="/<?= $idxlatestLog->getTraveler()->getUserName() ?>" >	
													<?= $idxlatestLog->getTraveler()->getUserName() ?>						 
												</a><br />
												<a class="journal_title" href="/journal-entry.php?action=view&amp;travelID=<?= $idxlatestLog->getTravelID() ?>&amp;travelerID=<?= $idxlatestLog->getTravelerID() ?>" >
													<?= $idxlatestLog->getTravel()->getTitle() ?> 
												</a>						 
											</div>
											
											<div class="clear"></div>											
										</div>
										</li>
									<? } ?>
									</ul>
									<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$JOURNAL_ENTRY?>)">
										<input type="hidden" name="featItemsID" id="featentryID" value="">	
										<input type="hidden" name="sectionID" value="<?=FeaturedSection::$JOURNAL_ENTRY?>">
										<input  type="submit" value="Save Changes" name="rearrange"/>
									</form>
									<div class="sub_section">
										<h2><?=$featTravelSection->getCaption()?></h2>
										
										<ul id="travelID" class="sortable boxy journals" style="width:800px;">
										<? for ($idxtrav=0; $idxtrav<count($featTravel); $idxtrav++) { 
											$idxfeatTravel = $featTravel[$idxtrav];
										?>
											<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$JOURNAL, $idxfeatTravel->getTravelID())?>">
												<a href="/journal-entry.php?action=view&amp;travelID=<?= $idxfeatTravel->getTravelID() ?>" class="title" >
												
													<img align="left" style="padding:3px;" alt="Journal Entry Photo" src="<?= $idxfeatTravel->getRandomPhoto()->getPhotoLink('thumbnail') ?>"  />
												
													<? if (strlen($idxfeatTravel->getTitle()) > 21): ?>
														<?= substr($idxfeatTravel->getTitle(),0, 21).'...' ?>
													<? else: ?>
														<?= $idxfeatTravel->getTitle() ?>
													<? endif; ?>
												</a>
												<? $travelLoc = array_unique($featTravelLoc[$idxtrav]);
													if (count($travelLoc)) :
														$idxLoc = 0;
														$limit = 2;
														if (2 > count($travelLoc))
															$limit = count($travelLoc);
														echo '<br />in: ' ;
														foreach($travelLoc as $each) {
															if (0 < $idxLoc)
																echo ", ";
															echo $each;
															$idxLoc++;
															if ($idxLoc == $limit)
																break;														
														}	
													endif;						   
												?>
												<br />
												by:
												<a class="username" href="/<?= $idxfeatTravel->getTraveler()->getUserName() ?>" >	
													<?= $idxfeatTravel->getTraveler()->getUserName() ?>						 
												</a><br />
												
											</li>
										<? } ?>
										</ul>										
										<div class="clear"></div>
										<div class="section_foot">
											<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$JOURNAL?>)">
												<input type="hidden" name="featItemsID" id="feattravelID" value="">	
												<input type="hidden" name="sectionID" value="<?=FeaturedSection::$JOURNAL?>">
												<input  type="submit" value="Save Changes" name="rearrange"/>
											</form>
											
											<a href="/journal.php" class="more">More Journals </a>	
										</div>
										
									</div>
							</div>		
						</div>
				</div>
							
								
				
				<div class="area column2">
				
				
					<div class="section photos" id="featured_photos">		

						<h2><?=$featPhotoSection->getCaption()?></h2>
						
						<div class="content">
							
							<ul id="photoID" class="sortable boxy" style="width:190px;">
							<? for ($idxphoto=0; $idxphoto<count($featPhoto); $idxphoto++) { 
								$idxfeatPhoto = $featPhoto[$idxphoto];
								//$idxtravel_photo  =	$arrtravel_photo[$idxphoto];
								$_context = $idxfeatPhoto->getContext();
								switch(get_class($_context)) :					
									case 'TravelLog':						
										$viewphotolink = "../photomanagement.php?cat=travellog&amp;action=vfullsize&amp;genID=" .$_context->getTravelLogID(). "&amp;photoID=".$idxfeatPhoto->getPhotoID();
										break;
									case 'TravelerProfile':						
										$viewphotolink = "../photomanagement.php?cat=profile&amp;action=view&amp;genID=" .$_context->getTravelerID(). "&amp;photoID=".$idxfeatPhoto->getPhotoID();
										break;
									case 'FunGroup':						
										$viewphotolink = "../photomanagement.php?cat=fungroup&amp;action=view&amp;genID=" .$_context->getGroupID(). "&amp;photoID=".$idxfeatPhoto->getPhotoID();
										break;
									case 'PhotoAlbum':						
										$viewphotolink = "../photomanagement.php?cat=photoalbum&amp;action=view&amp;groupID=" .$_context->getGroupID()."&amp;genID=" .$_context->getPhotoAlbumID(). "&amp;photoID=".$idxfeatPhoto->getPhotoID();
										break;
									default:
										$viewphotolink = '';
										break;	
								endswitch;								
							?>
								<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$PHOTO, $idxfeatPhoto->getPhotoID())?>">
								<div class="photo">
									<a href="<?=$viewphotolink?>">
										<img src="<?= $idxfeatPhoto->getPhotoLink('featured') ?>"  alt="Featured Photo" />
									</a> 
									<div class="caption">
										<?	if ($idxfeatPhoto->getCaption())
											 echo $idxfeatPhoto->getCaption();
										?>
									</div>
									<? if (get_class($_context) == 'TravelLog' || get_class($_context)  == 'TravelerProfile') : ?>
										<a class="username" href="profile.php?action=view&amp;travelerID=<?= $_context->getTravelerID() ?>" > <?= $_context->getTraveler()->getUserName() ?> </a>
										<br />
										<? if (get_class($_context) == 'TravelLog') : ?>
											<a class="journal_title" href="journal-entry.php?action=view&amp;travelID=<?= $_context->getTravel()->getTravelID() ?>" title="<?= $_context->getTravel()->getTitle()?>"><?= $_context->getTravel()->getTitle() ?></a>
										<? endif;?>
									<? elseif (get_class($_context) == 'FunGroup') : ?>
										<a href="../group.php?gID=<?=$_context->getGroupID()?>"><?=$_context->getName()?></a>
									<? elseif (get_class($_context) == 'PhotoAlbum') : ?>
										<?$factory =  GroupFactory::instance();
										  $relGroup   =  $factory->create( array($_context->getGroupID()) );?>
										<a href="../group.php?gID=<?=$relGroup[0]->getGroupID()?>"><?=$relGroup[0]->getName()?></a>
									<? endif;?>
								</div>
								
								<div class="clear"></div>
								<?if (0 != ($idxphoto % 2)) :?>
									<!--<div class="clear"></div>-->
								<? endif; ?>
								</li>
							<? } ?>
							</ul>
							
						</div>
						<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$PHOTO?>)">
							<input type="hidden" name="featItemsID" id="featphotoID" value="">	
							<input type="hidden" name="sectionID" value="<?=FeaturedSection::$PHOTO?>">
							<input  type="submit" value="Save Changes" name="rearrange"/>
						</form>
					</div>
							
					
					<div class="section" id="featured_group">
						<h2><?=$featGroupSection->getCaption()?></h2>
						<div class="content">
							<ul id="groupID" class="sortable boxy" style="width:350px; ">
							<? for ($idxgrp=0; $idxgrp<count($featGroup); $idxgrp++) { 
								$idxfeatGroup = $featGroup[$idxgrp];								
							?>
							<li itemID="<?=FeaturedSection::getKeyBySectionGenID(FeaturedSection::$GROUP, $idxfeatGroup->getGroupID())?>">
								<div class="photo">
									<a class="thumb" href="/group.php?gID=<?= $idxfeatGroup->getGroupID() ?>" >
										<img alt="Featured Group Photo" src="<?= $idxfeatGroup->getGroupPhoto()->getPhotoLink('standard') ?>" />
									</a>
								</div>
								<h3><a href="/group.php?gID=<?= $idxfeatGroup->getGroupID() ?>" > 
										<?= $idxfeatGroup->getName() ?> 
									</a>
								</h3>
								<p>
									<?= StringFormattingHelper::wordTrim($idxfeatGroup->getDescription(), 50)?><? if (50 < count(explode(' ', strip_tags($idxfeatGroup->getDescription()) ) ) ) : ?><?='&#8230;' ?><? endif ; ?>
								</p>
								
								<div class="clear"></div>	
							</li>
							<? } ?>
							</ul>
							<div class="section_foot">
								<form name="frmposition" action="" method="POST" onsubmit="return setPosition(<?=FeaturedSection::$GROUP?>)">
									<input type="hidden" name="featItemsID" id="featgroupID" value="">	
									<input type="hidden" name="sectionID" value="<?=FeaturedSection::$GROUP?>">
									<input  type="submit" value="Save Changes" name="rearrange"/>
								</form>
								<a href="/group.php" class="more">More Groups</a>
							</div>
						</div>
					</div>				
				</div>
					
				
					<div class="clear"></div>
			</div>
			
			<script type="text/javascript">
		   Sortable.create("travelID",{dropOnEmpty:true,containment:["travelID"],constraint:false});	
		   Sortable.create("travelerID",{dropOnEmpty:true,containment:["travelerID"],constraint:false});	
		   Sortable.create("groupID",{dropOnEmpty:true,containment:["groupID"],constraint:false});
		   Sortable.create("photoID",{dropOnEmpty:true,containment:["photoID"],constraint:false});
		   Sortable.create("entryID",{dropOnEmpty:true,containment:["entryID"],constraint:false});		   
			</script>

		
