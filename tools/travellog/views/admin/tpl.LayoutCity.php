<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns = "http://www.w3.org/1999/xhtml">
	<head>
		<title> Approve City Tool </title>
		<link href = "/css/forms.css"			rel = "stylesheet"	type = "text/css" />
		<link href = "/css/default.css" 		rel = "stylesheet"	type = "text/css" />
		<link href = "/css/jt_DialogBox.css"	rel = "stylesheet" 	type = "text/css" />
		<link href = "/css/veil.css"			rel = "stylesheet" 	type = "text/css" />

		<?php
			Template::includeDependentJs(
				array(
					'/js/dom-drag.js',
					'/js/jt_utils.js',
					'/js/jt_DialogBox.js',
					'/js/jt_AppDialogs.js'
				),
				array('include_here' => true)
			);
		?>
		<?php Template::includeDependentJs('/js/prototype.js', array('include_here' => true))?>
		<?php
			Template::includeDependentJs(
				array(
					'/js/utils/Class.Request.js',
					'/js/behaviour.js',
					'/js/GA_layout.js'
				),
				array('include_here' => true)
			);
		?>
	</head>
	<body>
		<div id = "wrapper">
			<?=$contents?>
		</div>
	</body>
</html>