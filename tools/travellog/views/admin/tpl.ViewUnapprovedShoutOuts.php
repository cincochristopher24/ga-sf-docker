<?php
/***
 * Created on 12 17, 08
 *
 * * @author 
 * Purpose: 
 */
?>

<?	include_once('travellog/views/admin/tpl.AdminHeader.php');
	$d = new GaDateTime();
	//$travelerID = (0 == $travelerID) ? -1 : $travelerID;
?>

<div id="content_wrapper" class='layout_2'>
		<div id="message_center" class="section">
		
			<h2><span>
				UnApproved Shout Outs
			</span></h2>
		
			<div id="comments" class="content">
			
			<form action="approveShoutOut.php" method="post" id = "form">
				<input type="checkbox" class="shoutout_list_batch_checkbox" onclick="return checkAll(this);"/>Check/Uncheck All
				<ul id = "display_comments_content">
				<?	$display = 0; 
					foreach($shoutOutList as $comment):
						try{
					 		$author = $comment->getAuthorObject();
					 		$context = $comment->getContext();
					 		$Poke = new PokeType($comment->getPokeType());
							$pokesview = new PokesView();
							$pokesview->setVisitorId(-1);		// travelerId of logged-in user
							$pokesview->setOwnerId($comment->getContextOwnerID());
							$pokesview->setPokeTypeId($Poke->getPokeTypeID());
						 	$pokesview->setContextId($comment->getContextId());
						 	if($context instanceof TravelerProfile)
						 		$pokesview->setContext(CommentType::$PROFILE);
						 	if($context instanceof Photo)
						 		$pokesview->setContext(CommentType::$PHOTO);
						 	if($context instanceof TravelLog)
						 		$pokesview->setContext(CommentType::$TRAVELLOG);
						 	if($context instanceof Article)
						 		$pokesview->setContext(CommentType::$ARTICLE);
						 	$pokesview->setAuthorObject($author);
						$display++;
						}catch(exception $e){
							continue;
						}
				?>
					
					<li>
						<input type="checkbox" class="shoutout_batch_checkbox" name="chkCommentID[]" value="<?=$comment->getCommentID()?>"/>
						<?$comment->getContextOwnerID()?>
						<? if($context instanceof TravelerProfile ):?>
							<a href="/<?=$comment->getContext()->getUserName()?>"><?=$comment->getContext()->getUserName()?></a> 
						<? elseif($context instanceof TravelLog):?>
							<a href="/journal-entry.php?action=view&amp;travellogID=<?=$comment->getContext()->getTravelLogID()?>"><?=$comment->getContext()->getTitle()?></a>
						<? elseif($context instanceof Article):?>
							<a href="/article.php?action=view&articleID=<?=$comment->getContext()->getArticleID()?>"><?=$comment->getContext()->getTitle()?></a>	
						<? elseif($context instanceof Photo):?>
							Photo
						<? endif;?>
						<a href="#"  class="thumb">
							<img src="<?=$author->getPhotoSource()?>" height="37" width="37" alt="User Profile Photo" />
 						</a>
					<div class="comments_content">
						<div class="comment_info"> 
  							<strong>
								<?=$pokesview->getMessage()?>
							</strong> 
  							<span>
								<?= $d->set($comment->getCreated())->friendlyFormat() ?>
  							</span>					 							
						</div>
						<? if (0 == $comment->getPokeType()) : ?>
						<div>	
							<?= HtmlHelpers::Textile($comment->getTheText()) ?>
						</div>
						<? else : ?>
						<? $class_name = preg_replace('/.png$/','',$Poke->getPokeImage())?>
						<div class="shout do_<?=$class_name?>">do <?= $class_name ?></div>
						<div>	
							<?= HtmlHelpers::Textile($comment->getTheText()) ?>
						</div>
						<? endif; ?>
					</div>
					</li>
					<? endforeach;?>
				</ul>		

				<?php if (0 == $display) : ?>
					<p> There are no unapproved shout-outs at this time. </p>
				<?php endif;?>
				
				<input type="checkbox" class="shoutout_list_batch_checkbox" onclick="return checkAll(this);"/>Check/Uncheck All	
				
				<div class="actions">
					<input type="hidden" name="hdnReferer" value="<?= $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>" />
					<input type="hidden" name="hdnActive" value="<?= isset($contents['active']) ? $contents['active'] : ''; ?>" />
					<input type="hidden" name="hdnPage" value="<?= isset($contents['page']) ? $contents['page'] : '';?>" />
					<input type="button" name="btnSend" id='btnSend' value="Approve" onclick="var con = confirm('Are you sure you want to submit the selected Shout Out?'); if (con) { this.disabled = true; document.getElementById('btnDelete').disabled='true'; document.getElementById('action').value='send';document.getElementById('form').submit(); } else { return con; }" class="submit" <?if(count($shoutOutList)==0):?>disabled<?endif;?>/>
					<input type="button" name="btnDelete" id='btnDelete' value="Delete" onclick="var con = confirm('Are you sure you want to delete the selected Shout Out?'); if (con) { this.disabled = true; document.getElementById('btnSend').disabled='true'; document.getElementById('action').value='delete';document.getElementById('form').submit(); } else { return con; }" class="submit" <?if(count($shoutOutList)==0):?>disabled<?endif;?>/>
					<input type="hidden" name="action" id="action">
				</div>
			</form>	
			
			</div>
		
			<div class="pagination">
			<? if(isset($paging)):?>
				<? if ($paging->getTotalPages() > 1 ): ?>
				<div class="page_markers">
					Showing <?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $count?> Unapproved Shout Outs.
				</div>			
				<? $paging->getFirst(); ?>&nbsp;|&nbsp;<? $paging->getPrevious(); ?>&nbsp;&nbsp;<? $paging->getLinks(); ?>&nbsp;&nbsp;<? $paging->getNext(); ?>&nbsp;|&nbsp;<? $paging->getLast(); ?>
				<? endif; ?>
			<? endif; ?>		
			</div>
			
			<div class="content">
				<p class="help_text"></p>
			</div>
		</div>
</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll(chk)
{
  jQuery('.shoutout_list_batch_checkbox').attr('checked',(chk.checked));
  var boxes = document.getElementsByTagName('input'); 
  for(var index = 0; index < boxes.length; index++) { 
  	box = boxes[index]; 
  	if (box.type == 'checkbox' && box.className == 'shoutout_batch_checkbox')
  		box.checked = chk.checked 
  } 
  return true;
}
/* ]]> */
</script>