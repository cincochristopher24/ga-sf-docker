<?php $header = array('username', 'firstname', 'lastname', 'email')?>
<form action="" method="post" enctype="multipart/form-data" >
	<label for='file'>Choose Excel files or .CSV files</label>
		<input type="file" name="file" id="file" />
	<br />
	<input type = "checkbox" name="generateUsername" value = "1" <?=($generateUsername ? 'checked' : '') ?> >Auto-generate username from first and second columns
	<br /><br/>
	<input type="submit" name="btnLoad" value="Load File" />
</form>
<p><a href="manageNewTraveler.php?gID=<?=$gID?>">Manage New Travelers</a></p>
<hr />
<div >
	<?//php if(isset($travelersToBeAdded) AND 0 < count($travelersToBeAdded)):?>
	<?php if(isset($loadfile) and $loadfile):?>
			<h2>Below are the information read from file: (<?=count($travelersToBeAdded)?>)</h2><br />
 			<a href ="javascript:void(0)" onclick = "selectboxes('traveleridtobeadded',1,'frmTravelersToBeAdded')">Select All</a>&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href ="javascript:void(0)" onclick = "selectboxes('traveleridtobeadded',0,'frmTravelersToBeAdded')">Unselect All</a>
			<form id = "frmTravelersToBeAdded" method = "post">
			
			<label for="txtCustomPassword">Custom password that will be used by all created travelers. If empty, system will generate random password to each traveler.</label>	
			<br />
			<input type="text" name="txtCustomPassword" id="txtCustomPassword" value="" size="20"/>
			
			<table cellpadding="4" cellspacing="2" width = "50%">
				<th bgcolor = "#ccc"></th><th bgcolor = "#ccc"><?php echo implode('</th><th bgcolor = "#ccc">', $header)?></th>
			<?php foreach($travelersToBeAdded as $k => $iTraveler):?>
					<tr bgcolor = "#33CCCC">
						<td align="center"><input type="checkbox" name="traveleridtobeadded[]" value ="<?=$k?>" ></td>
						<?php foreach($iTraveler as $k => $value):?>
							<td align="center"><?= $value?></td>
						<?php endforeach; ?>
					</tr>
			<?php endforeach;?>
			</table>

			<!-- existing travelers-->
			<?php if(isset($existingTravelers) AND 0 < count($existingTravelers)):?>
				<h2>Existing Travelers based on emails in file(<?=count($existingTravelers)?>):</h2>
				<table cellpadding="4" cellspacing="2" width = "50%">
					<th bgcolor = "#ccc"><?php echo implode('</th><th bgcolor = "#ccc">', $header)?></th>
					<?php foreach($existingTravelers as $iExisting):?>
							<tr bgcolor = "#33CCCC">
								<?php $existingProfile = $iExisting->getTravelerProfile()?>
								<td align = "center"><?=$iExisting->getUserName()?></td>
								<td align = "center"><?=$existingProfile->getFirstName()?></td>
								<td align = "center"><?=$existingProfile->getLastName()?></td>
								<td align = "center"><?= $existingProfile->getEmail()?></td>
							</tr>
					<?php endforeach;?>
				</table>
				<input type = "hidden" name = "existingTravelerIDs" value = "<?=implode(',',$existingTravelerIDs)?>">
			<?php endif;?>
			<!-- end existing travelers-->
			
			<?php if($generateUsername):?>
				<input type = 'hidden' name = 'generateUsername' value = '1'>
			<?php endif;?>
			<input type = 'hidden' name = 'uploaded_filename' value = '<?=$uploaded_filename?>'>
			<input type = 'hidden' name = 'gID' value = '<?=$gID?>'>
			<input type = 'submit' name = 'btnCreate' value = 'Create New Travelers'>
			<a href="">Cancel</a>
			<div><b><br>Create travelers who will be members of a same group. <br>You can still load the same file later to join travelers to other groups</b></div>
			</form>	
	
	<?php elseif(isset($addedTravelers) AND 0 < $addedTravelers): ?>
		<h2>Below are the newly registered travelers: (<?=count($addedTravelers)?>)</h2>
		<a href ="javascript:void(0)" onclick = "selectboxes('travelerids',1,'frmAddedTravelers')">Select All Travelers</a>&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href ="javascript:void(0)" onclick = "selectboxes('travelerids',0,'frmAddedTravelers')">Unselect All Travelers</a>
		<form id = "frmAddedTravelers" method = 'post'>
			<table cellpadding="4" cellspacing="2" width = "50%">
				<?php if(isset($groups) and 0 < count($groups)):?><th bgcolor = "#ccc"></th><?php endif;?>
				<th bgcolor = "#ccc">Username</th>
				<th bgcolor = "#ccc">Password</th>
				<th bgcolor = "#ccc">Firstname</th>
				<th bgcolor = "#ccc">Lastname</th>
				<th bgcolor = "#ccc">Email Address</th>
		<?php foreach($addedTravelers as $iTraveler):?>
				<tr bgcolor = "#33CCCC">
					<?php $profile = $iTraveler->getTravelerProfile()?>
					<?php if(isset($groups) and 0 < count($groups)):?><td align="center"><input type='checkbox' value = '<?=$iTraveler->getTravelerID()?>' name = 'travelerids[]' /></td><?php endif;?>
					<td align="center"><a href="../<?=$iTraveler->getUserName()?>"><?= $iTraveler->getUserName()?></a></td>
					<td align="center"><?= $iTraveler->getPassword()?></td>
					<td align="center"><?= $profile->getFirstName()?></td>
					<td align="center"><?= $profile->getLastName()?></td>
					<td align="center"><?= $profile->getEmail()?></td>
				</tr>
		<?php endforeach;?>
			</table>
			
			<!-- existing travelers-->
			<?php if(isset($existingTravelers) AND 0 < count($existingTravelers)):?>
				<h2>Below are existing Travelers based on emails in file(<?=count($existingTravelers)?>):</h2>
				<table cellpadding="4" cellspacing="2" width = "50%">
					<?php if(isset($groups) and 0 < count($groups)):?><th bgcolor = "#ccc"></th><?php endif;?>
					<th bgcolor = "#ccc">Username</th>
					<th bgcolor = "#ccc">Firstname</th>
					<th bgcolor = "#ccc">Lastname</th>
					<th bgcolor = "#ccc">Email Address</th>
					<?php foreach($existingTravelers as $iExisting):?>
							<tr bgcolor = "#33CCCC">
								<?php $existingProfile = $iExisting->getTravelerProfile()?>
								<?php if(isset($groups) and 0 < count($groups)):?><td align="center"><input type='checkbox' value = '<?=$iExisting->getTravelerID()?>' name = 'travelerids[]' /></td><?php endif;?>
								<td align = "center"><a href ="../<?=$iExisting->getUserName()?>"><?=$iExisting->getUserName()?></a></td>
								<td align = "center"><?=$existingProfile->getFirstName()?></td>
								<td align = "center"><?=$existingProfile->getLastName()?></td>
								<td align = "center"><?= $existingProfile->getEmail()?></td>
							</tr>
					<?php endforeach;?>
				</table>
				<input type = "hidden" name = "existingTravelerIDs" value = "<?=implode(',',$existingTravelerIDs)?>">
			<?php endif;?>
			<!-- end existing travelers-->
		<br/>
		<input type = 'submit' name = 'btnDelete' value = 'Delete Travelers' onclick = "return confirm('Delete traveler?')"/> Delete Traveler
		
		
		<?php if(isset($groups) and 0 < count($groups)):?>
			<h2>Groups:</h2>
			<a href ="javascript:void(0)" onclick = "selectboxes('groupids',1,'frmAddedTravelers')">Select All</a>&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href ="javascript:void(0)" onclick = "selectboxes('groupids',0,'frmAddedTravelers')">Unselect All</a>
				<table  cellpadding="4" cellspacing="2">
					<tr >
					<?$ctr = 0?>
					<?php foreach($groups as $iGroup):?>
						<?$ctr++?>
						<td><input type = 'checkbox' value = '<?=$iGroup->getGroupID()?>' name = 'groupids[]' />
						<?= $iGroup->getName()?></td>
						<?if($ctr > 2):?></tr><tr><?$ctr=0; endif;?>
					<?php endforeach;?>
				</table>
			<div class = 'clear'></div>
			<h3>Add selected travelers as members for selected groups<input type = 'submit' name = 'btnJoinGroups' value = 'Go' onclick = "return confirm('Continue?')"/></h3>
		<?php endif;?>
		</form><?php if ($duplicateUsername OR $duplicateEmail ):?>
				<?php $duplicates = $duplicateUsername+$duplicateEmail; ?>
			<h2>Edit Duplicate Username/Email Address: </h2>
			<form name="duplicateForm" method="post" >	
			    <table cellpadding="4" cellspacing="2" width = "50%">
			    	<th bgcolor = "#ccc">Username</th>
			    	<th bgcolor = "#ccc">Password</th>
			    	<th bgcolor = "#ccc">Firstname</th>
			    	<th bgcolor = "#ccc">Lastname</th>
			    	<th bgcolor = "#ccc">Email Address</th>
			    <?php foreach($duplicates as $iDuplicateTraveler):?>
			    	<tr bgcolor = "#33CCCC">
			    		<td align="center"><input name="duplicateUsername[]" type="text" value="<?= $iDuplicateTraveler['username']?>"/></td>
			    		<td align="center"><input name="duplicatePassword[]" type="text" value=""/></td>
			    		<td align="center"><input name="duplicateFirstname[]" type="text" value="<?= $iDuplicateTraveler['firstname']?>"/></td>
			    		<td align="center"><input name="duplicateLastname[]" type="text" value="<?= $iDuplicateTraveler['lastname']?>"/></td>
			    		<td align="center"><input name="duplicateEmail[]" type="text" value="<?= $iDuplicateTraveler['email']?>"/></td>
			    	</tr>
			    <?php endforeach;?>
			    </table>
			    <?php if($generateUsername):?>
				<input type="hidden" name="generateUsername" value="1"/>
				<?php endif;?>
				<input type="hidden" name="hdnDuplicate" value="1"/>
			    <input type="submit" name="btnCreate" value="Submit"/>
			</form>	
			<br/>
		<?php endif;?>
	<?php endif;?>
</div>

<?// details for joined groups?>
<div>
	<?php if(isset($joinedgroups) and isset($joinedtravelers) and 0 < count($joinedgroups) and $joinedtravelers):?>
		<?php $arr = array()?>
		<?php foreach($joinedtravelers as $iTraveler):?>
			<?php $arr[] = '<a href="../' . $iTraveler->getUserName() . '">' . $iTraveler->getUserName() . '</a>'?>
		<?php endforeach;?>
		<?php echo implode(', ', $arr)?> has been added as members for the following groups:<br />
		<ul>
		<?php foreach($joinedgroups as $iGroup):?>
			<li><a href='../group.php?gID=<?=$iGroup->getGroupID()?>'><?=$iGroup->getName()?></a></li>
		<?php endforeach;?>
		</ul>
	<?php endif;?>
</div>

<div><h2><?= isset($notice) ? $notice : ''?></h2></div>

<br /><br />
<hr />
<b>
<p>This tool will create travelers based on information listed in an excel file or csv.</p>
<br />
<p>Both Excel or CSV files must follow this column ordering:</p>
<p>Username -- First Name -- Last Name -- Email</p>
<br />
<p>First Name, Last Name and Email are not required. Passwords are autogenerated.</p>
<p>Check "Auto-generate username from first and second columns" if username will be generated from firstname and lastname, assuming that firstname and lastname are the 1st and 2nd column of your file.</p>
<br>
For CSV files, the field separtor is a comma(,)
<br>
Example:
<br>
<i>myUsername,myFirstName,myLastName,myEmail@goabroad.com</i>
<br>
A new traveler will be created using this line of information with username "myUsername", firstname "myFirstName", lastname "myLastName" and with email "myEmail@goabroad.com".
<br><br>
The same is true with Excel files except that fields are separated by columns.
</b>

