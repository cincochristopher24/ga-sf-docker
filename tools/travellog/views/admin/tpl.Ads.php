<?php
/**
 * Created on May 10, 2007
 *
 * @author daf 
 */
 
 	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');

?>
	<div class="area" id="intro">
		<div class="section" align="right">
			<h2><a href="index.php">GoAbroad Network Admin</a></h2>
		</div>
		<div class="section" align="center">
			<h1>ADS</h1>		
		</div>
	</div>		
	
	<div class="area" id="intro">
		<div class="section">
			<h2><a href="ads.php?mode=add">Add New</a></h2>
		</div>
	</div>	
	
	<div class="area" id="top">
		<div id="journals_list" class="list">	
			<? foreach($arrAds as $each):?>
				<div class="journal section">
					<div class="content">
						<div class="wrapper">
							<div class="description">
								<br />
								<strong>GA.com ClientID :</strong> <?= $each->getClientID()?><br />
								<strong>GA.com BannerID :</strong> <?= $each->getBannerID()?><br />
								<strong>Alt Text :</strong> <?= $each->getAltText()?><br />		
								<strong>Website :</strong> <?= $each->getSiteLink()?>				
							</div>
						</div>
						<div class="photo">
							<a href="<?= $each->getUrl()?>">
									<img src="<?= htmlspecialchars($each->getImagePath())?>" height="90" width="120" alt="<?= htmlspecialchars($each->getAltText())?>" />
							</a>
							<p id="advertisement_statement">
								<?= $each->getAdText()?>
							</p>								
						</div>
						<div class="info meta">
							<a href="ads.php?adID=<?=$each->getAdvertisementID()?>&amp;mode=edit">Edit</a>  |
							<a onclick="return confirm('Delete?')" href="ads.php?adID=<?=$each->getAdvertisementID()?>&amp;mode=delete">Delete</a>
						</div>
						<div class="clear"></div>
					</div>
				</div>		
			<? endforeach; ?>		
		</div>
	</div>