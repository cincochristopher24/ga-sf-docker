<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	$d = new GaDateTime();

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
	require_once 'travellog/controller/admin/Class.ViewFeaturedDatesController.php';
?>

<div id = "intro_container">
	<div id = "intro" align = "center">
		<h1>GROUPS/CLUBS</h1>
	</div>
</div>

<div id = "content_wrapper" class = "layout_2">
	<div id = "wide_column">
		<h2>
			<span><?=$title?></span>
		</h2>
		<div id = "groups_and_clubs" class = "section">
			<div class = "content">
				<div style = "border: 1px solid grey; margin-left: 10px; padding-left: 20px; padding-top: 10px; width: 88%">
					<?=$filter_template?>
					<hr width = "96%" align = "left" />
					<?=$search_template?>
				</div>
				<div id = "groups_list_main" style = "padding-top: 20px;">
					<? if ($paging->getTotalPages() > 1) : ?>
						<div class = "pagination">
							<div class = "page_markers">
								<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec?>
							</div>	
							<?$paging->getFirst()?>
							&nbsp;|&nbsp;
							<?$paging->getPrevious()?>
							&nbsp;&nbsp;
							<?$paging->getLinks()?>
							&nbsp;&nbsp;
							<?$paging->getNext()?>
							&nbsp;|&nbsp;
							<?$paging->getLast()?>
						</div>
					<? endif; ?>

					<?
						$iterator->rewind();
						$grpRank = $paging->getStartRow();
						$frmGroupID = array();
					?>
					<? if ($iterator->valid()) : ?>						
						<ul>
							<?
								while($iterator->valid()):
									$eachgroup = $iterator->current();	
									$frmGroupID[] = $eachgroup->getGroupID();
									$grpRank++;
										if (Group::ADMIN ==  $eachgroup->getDiscriminator())
											if (TRUE == $eachgroup->isSubGroup())			
												$eachgroup = $eachgroup->getParent();	

									$photolink = $eachgroup->getGroupPhoto()->getPhotoLink();
									if (0 != strncmp("http://", $photolink , "7"))
										$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
							?>
									<li>
										<a class = "group_thumb" href="http://<?=$_SERVER['SERVER_NAME']?>/group.php?gID=<?= $eachgroup->getGroupID() ?>">
											<img src="<?= $photolink ?>" alt="<?= $eachgroup->getName() ?>" />
										</a>
										<div class = "groups_info">
											<strong>
												<a href = "http://<?=$_SERVER['SERVER_NAME']?>/group.php?gID=<?= $eachgroup->getGroupID() ?>">
													<?=$eachgroup->getName()?>
												</a>
											</strong>
											
											<?php if ( AdminUserAccount::ACCESS_ADMIN == $_SESSION['userAccess']) :?>
												<hr width = "75%" align = "left" />
												<p>
													<strong>Last Login: </strong>
													<?=$d->set($eachgroup->getAdministrator()->getTravelerProfile()->getLastLogin())->friendlyFormat()?>
												</p>
												<p>
													<strong>Date Created: </strong>
													<?=$d->set($eachgroup->getDateCreated())->friendlyFormat()?>
												</p>
												<p>
													<? $lastFeaturedDate = $eachgroup->getLastFeaturedDate() ?>
													<?= strlen($lastFeaturedDate) ? '<strong>Date Last Featured: </strong>' . $d->set($lastFeaturedDate)->friendlyFormat() : '<strong>Not Featured yet!</strong>' ?>
													<? if (ViewFeaturedDatesController::displayViewLink($eachgroup->getGroupID(), 1)) : ?>
														&nbsp; &nbsp;
														<a
															href	= "javascript: void(0)"
															class	= "edit"
															onClick = "window.open('viewFeaturedDates.php?sectionID=1&type=1&genID=<?=$eachgroup->getGroupID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
															[view dates]
														</a>
													<? endif; ?>
												</p>
												<p>
												 	<? if ($section->isItemFeatured($eachgroup->getGroupID())) : ?>
													 		&raquo;<strong>[Featured]</strong>
															<a href="groups.php?feature=<?= $eachgroup->getGroupID()?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">Remove Feature</a>
													<? else : ?>
															&raquo;<a href="groups.php?feature=<?= $eachgroup->getGroupID() ?>">Feature This!</a>
													<? endif; ?>
												</p>
											<? endif; ?>
											
											<p>
												<? if (TRUE == $eachgroup->isSuspended()) : ?>
														&raquo;<strong>[Suspended]</strong>
														<a href="groups.php?groupID=<?= $eachgroup->getGroupID()?>&amp;activate" onclick="javascript:return confirm('Do you want to ACTIVATE this Group?');">Activate Group</a>
												<? else : ?>
													&raquo;<a href="groups.php?groupID=<?= $eachgroup->getGroupID()?>&amp;suspend" onclick="javascript:return confirm('Do you want to SUSPEND this Group?');">Suspend Group</a>
												<? endif; ?>
											</p>
											
											<?php if ( AdminUserAccount::ACCESS_ADMIN == $_SESSION['userAccess']) :?>
												<p>
													<strong>Move to Page: </strong>
													<select name="cmbPage" onChange="document.getElementById('hidgrpID').value = <?=$eachgroup->getGroupID()?>;document.getElementById('hidPage').value = this.value;document.getElementById('hidoldabsRank').value = <?= $eachgroup->getGroupRank() ?>;document.formmove.submit()">
														<?for($i=1;$i<=$paging->getTotalPages();$i++):?>
															<option value="<?=$i?>" <? if ($page==$i):?>selected <?endif;?>><?=$i?>
														<?endfor;?>
													</select>
												</p>
												<p>
													<? if (Group::FUN ==  $eachgroup->getDiscriminator() ) : ?>
														<strong>Administrator:</strong>
														<div style = "padding-top: 5px">
											            	<? $adminprofile = $eachgroup->getAdministrator()->getTravelerProfile(); ?>
											                <a href="/<?=$adminprofile->getUserName() ?>" class="user_thumb" >
																<img src="<?=$adminprofile->getPrimaryPhoto()->getPhotoLink('thumbnail')  ?>" width="37" height="40" alt="User Profile Pic" title="View Profile" />
															</a>							 
															<a href="/<?=$adminprofile->getUserName() ?>" class="username">
																<?
																	$mUserName = (15 < strlen($adminprofile->getUserName()))
																					? substr($adminprofile->getUserName(), 0, 15)
																					: $adminprofile->getUserName();
																	echo $mUserName;
																?>
															</a>
														</div>
													<? endif; ?>
												</p>
												<p>
													<? $grpCategoryList = $eachgroup->getGroupCategory(true) ?>
													<? if (count($grpCategoryList)) : ?>
															<strong>Type: </strong>
															<? $lstType = implode(", ", $grpCategoryList) ?>
															<?=$lstType?>
															<br />
													<? endif; ?>
													<? if (Group::ADMIN ==  $eachgroup->getDiscriminator()) : ?>
														<?
															$arrSubGroup = array();
															$mSubGroups = $eachgroup->getSubGroups();

															foreach($mSubGroups as $subgroup)
																$arrSubGroup[] = $subgroup->getName();
															$lstSubGroups = (0 < count($arrSubGroup)) ? implode(", ", $arrSubGroup) : '';
														?>
														<? if (count($arrSubGroup)) : ?>
															<strong>Sub Groups: </strong>
															<?=(180 < strlen($lstSubGroups)) ? substr($lstSubGroups, 0, 180) . '...' : $lstSubGroups?>
														<? else : ?>
															<strong>No Sub Groups Yet</strong>
														<? endif; ?>
													<? endif; ?>
												</p>
											<? endif; ?>
											
										</div>
									</li>
								<? $iterator->next() ?>
							<? endwhile; ?>		
						</ul>
						<div class="section_foot">
							<? $qrystr = (strlen($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '' )?>
							<form name="frmposition" action="groups.php<?=$qrystr?>" method="POST" onsubmit="return setGroupPosition()">
								<input type="hidden" name="frmGroupID" id="frmGroupID" value="<?=implode(",",$frmGroupID)?>">
								<input type="hidden" name="rankGroupID" id="rankGroupID" value="">					
								<input  type="submit" value="Save Changes" name="rearrange"/>
							</form>
							<form name="formmove" action="groups.php<?=$qrystr?>" method="POST">
								<input type="hidden" id="hidPage" name="hidPage">
								<input type="hidden" id="hidoldabsRank" name="hidoldabsRank">
								<input type="hidden" id="hidgrpID" name="hidgrpID">
								<input type="hidden" id="hidcntpage" name="hidcntpage" value="<?=$iterator->count()?>">
							</form>
						</div>
					<? else : ?>
						<h4 style = "text-align: center; text-transform: uppercase; font-weight: bold">Your search has returned 0 rows.</h4>
					<? endif; ?>

					<? if ($paging->getTotalPages() > 1) : ?>
						<div class = "pagination">
							<div class = "page_markers">
								<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec ?>
							</div>
							<?$paging->getFirst()?>
							&nbsp;|&nbsp;
							<?$paging->getPrevious()?>
							&nbsp;&nbsp;
							<?$paging->getLinks()?>
							&nbsp;&nbsp;
							<?$paging->getNext()?>
							&nbsp;|&nbsp;
							<?$paging->getLast();?>
						</div>		
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>

	<?php if ( AdminUserAccount::ACCESS_ADMIN == $_SESSION['userAccess']) :?>
	<div id = "narrow_column">
		<div id = "sub_groups" class = "section">
			<h2 style = "text-align: center">
				<? if (count($currFeat)) : ?>
					<? if (1 < count($currFeat)) : ?>
					 	CURRENTLY FEATURED GROUPS
					<? else : ?>
						CURRENTLY FEATURED GROUP
					<? endif; ?>
				<? endif; ?>
				<div style = "font-weight: normal; font-size: 12px; padding-top: 5px; text-transform: none">
					<strong>Caption: </strong>
					<?=$section->getCaption()?>
					<br />
					<strong>Quantity: </strong>
					<?=$section->getQuantity()?>
					<br />
					<a
						href	= "javascript: void(0)"
						class	= "edit"
						onClick	= "window.open('caption.php?sectionID=1','PopUp','left=300,height=200,width=400,status=yes')">
						change
					</a>
				</div>
			</h2>			
			<div class = "content">
				<ul class = "groups">
					<?
						foreach ($currFeat as $eachgroup) :
							if (Group::ADMIN ==  $eachgroup->getDiscriminator())
								if (TRUE == $eachgroup->isSubGroup())			
									$eachgroup = $eachgroup->getParent();	

							$photolink = $eachgroup->getGroupPhoto()->getPhotoLink();
							if (0 != strncmp("http://", $photolink , "7"))
								$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;
					?>
							<li class="sidecol_listimg">							
								<h4>
									<a href="http://<?=$_SERVER['SERVER_NAME']?>/group.php?gID=<?= $eachgroup->getGroupID() ?>">
										<?=$eachgroup->getName() ?>
									</a>
								</h4>
								<a class = "group_thumb" href="http://<?=$_SERVER['SERVER_NAME']?>/group.php?gID=<?= $eachgroup->getGroupID() ?>">
									<img src="<?= $photolink ?>" alt="<?= $eachgroup->getName() ?>" />
								</a>
								<div class = "groups_info">
									<p>
										&raquo;
										<a href="groups.php?feature=<?= $eachgroup->getGroupID()?>&amp;remove" onclick="javascript:return confirm('Do you want to REMOVE this Feature?');">Remove Feature</a>
										<br />
										<? if (TRUE == $eachgroup->isSuspended()) : ?>
											&raquo;<strong>[Suspended]</strong>
											<a href="groups.php?groupID=<?= $eachgroup->getGroupID()?>&amp;activate" onclick="javascript:return confirm('Do you want to ACTIVATE this Group?');">Activate Group</a>
										<? else : ?>
											&raquo;
											<a href="groups.php?groupID=<?= $eachgroup->getGroupID()?>&amp;suspend" onclick="javascript:return confirm('Do you want to SUSPEND this Group?');">Suspend Group</a>
										<? endif; ?>
									</p>
								
									<? if (Group::FUN ==  $eachgroup->getDiscriminator() ) : ?>
										<p id = "group_administrator" class = "users">
											<span style = "font-weight:bold">Administrator:</span>
											<div>
												<?
													try {
							                        	$adminprofile = $eachgroup->getAdministrator()->getTravelerProfile(); ?>
								                        <a href="/<?=$adminprofile->getUserName() ?>" class="user_thumb" >
															<img src="<?=$adminprofile->getPrimaryPhoto()->getPhotoLink() ?>" width="37" height="40" alt="User Profile Pic" title="View Profile" />
														</a>							 
														<a href="/<?=$adminprofile->getUserName() ?>" class="username">
															 <?=$adminprofile->getUserName() ?>
														</a>
							                    <?
													}
													catch (Exception $e) {}
						                        ?>
											</div>
										</p>
									<? elseif (Group::ADMIN ==  $eachgroup->getDiscriminator()) : ?>
										<p>
											<?
												$arrSubGroup = array();
												$mSubGroups = $eachgroup->getSubGroups();

												foreach($mSubGroups as $subgroup)
													$arrSubGroup[] = $subgroup->getName();
												$lstSubGroups = (0 < count($arrSubGroup)) ? implode(", ", $arrSubGroup) : '';
											?>
											<? if (count($arrSubGroup)) : ?>
												<span style = "font-weight:bold">Sub Groups:</span>
												<?=(65 < strlen($lstSubGroups)) ? substr($lstSubGroups, 0, 65) . '...' : $lstSubGroups?>
											<? else : ?>
												<span style = "font-weight:bold">No Sub Groups Yet</span>
											<? endif; ?>
										</p>
										<? $grpCategoryList = $eachgroup->getGroupCategory(true) ?>
										<? if (0 < count($grpCategoryList)) : ?>
											<? $lstCategory = implode(", ", $grpCategoryList) ?>
											<p>
												<span style = "font-weight:bold">Type: </span>
												<?=$lstCategory?>
											</p>
										<? endif; ?>
									<? endif; ?>
								</div>
								<p>
									<span style = "font-weight:bold">Last Login:</span>
									<?=$d->set($eachgroup->getAdministrator()->getTravelerProfile()->getLastLogin())->friendlyFormat()?>
								</p>
								<?php $lastFeaturedDate = $eachgroup->getLastFeaturedDate() ?>
								<? if (strlen($lastFeaturedDate)) : ?>
									<p>
										<span style = "font-weight:bold">Date Featured:</span>
										<?=$d->set($lastFeaturedDate)->friendlyFormat()?>
								<? endif; ?>
								<? if (ViewFeaturedDatesController::displayViewLink($eachgroup->getGroupID(), 1)) : ?>
									&nbsp; &nbsp;
									<a
										href	= "javascript: void(0)"
										class	= "edit"
										onClick = "window.open('viewFeaturedDates.php?sectionID=1&type=1&genID=<?=$eachgroup->getGroupID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;"
										style	= "font-size: 10px">
										[view dates]
									</a>
								<? endif; ?>
								</p>
							</li>
						<? endforeach; ?>		
					</ul>
				</div>
			</div>
			<div class = "foot"></div>
		</div>
		<? endif; ?>
	</div>
</div>