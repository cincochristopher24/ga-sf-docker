<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
?>

	<div id="wrapper1">
	<? if (isset($grpCat)) { ?>
		<table border="0" width="75%" align="center">	
			<tr>
				<form name="frmEdit" action="groupcategory.php?mode=save&amp;catID=<?=$grpCat->getCategoryID()?>" method="post">
					<td>
						<input type="text" name="txtCategory" value="<?=$grpCat->getName()?>" />
					</td>
					<td>
						<select name="grpDiscrim">
							<option value="1" <? if (GROUP::FUN   == $grpCat->getDiscriminator()){ ?> selected <? }?> >Club</option>
							<option value="2" <? if (GROUP::ADMIN == $grpCat->getDiscriminator()){ ?> selected <? }?> >Group</option>
						</select>
					</td>
					<td>
						<input type="submit" name="Save" value="Save" />
					</td>
				</form>
				<form name="frmCancelEdit" action="groupcategory.php" method="post">
					<td>
						<input type="submit" name="Cancel" value="Cancel" />
					</td> 
				</form>
			</tr>
		</table>
	<? } else { ?>
					
		<table border="0" width="75%" align="center">				
			<form name="frmQuickAdd" action="groupcategory.php?mode=save" method="post">
				<tr height="50">
					<td colspan="3"> <b>Quick Add Group Category</b>
					</td>
				</tr>
				<tr>
					<td>
						Group Category: <input type="text" name="txtCategory" value="" />
					</td>
					<td>
						<select name="grpDiscrim">
							<option value="1">Club</option>
							<option value="2">Group</option>
						</select>
					</td>
					
					<td>
						<input type="submit" name="Add" value="Add Group Category" />
					</td>
				</tr>
			</form>
		</table>
		
		<table border="0" width="75%" align="center">				
			<tr>
				<td colspan="3"><b>Categories for Clubs</b>
				</td>
			</tr>
			<? if (0 < count($grpFunCategories)) { ?>
				<? foreach ($grpFunCategories as $eachgrpfuncategory) { ?>
					<tr>
						<td>
							<?= $eachgrpfuncategory->getName() ?>
						</td>
						<td>
							<a href="groupcategory.php?mode=edit&amp;catID=<?= $eachgrpfuncategory->getCategoryID() ?>">
							Edit
							</a>	
						</td>
						<td>
							<a onclick="javascript:return confirm('Do you want to DELETE this Group Category?')" href="groupcategory.php?mode=delete&amp;catID=<?= $eachgrpfuncategory->getCategoryID() ?>">
							Delete
							</a>
						</td> 
					</tr>
				<? } ?>		
			
			<? } else { ?>
				<tr>
					<td colspan="3">There are no Categories for Clubs yet.
					</td>
				</tr>
			<? } ?>
			
			<tr>
				<td colspan="3"><b>Categories for Groups</b>
				</td>
			</tr>
			
			<? if (0 < count($grpAdminCategories)) { ?>
				<? foreach ($grpAdminCategories as $eachgrpadmincategory) { ?>
					<tr>
						<td>
							<?= $eachgrpadmincategory->getName() ?>
						</td>
						<td>
							<a href="groupcategory.php?mode=edit&amp;catID=<?= $eachgrpadmincategory->getCategoryID() ?>">
							Edit
							</a>	
						</td>
						<td>
							<a onclick="javascript:return confirm('Do you want to DELETE this Group Category?')" href="groupcategory.php?mode=delete&amp;catID=<?= $eachgrpadmincategory->getCategoryID() ?>">
							Delete
							</a>
						</td> 
					</tr>
				<? } ?>		
			
			<? } else { ?>
				<tr>
					<td colspan="3">There are no Categories for Groups yet.
					</td>
				</tr>
			<? } ?>
					
		</table>
		
		

	<? } ?>
		
	</div>			
	