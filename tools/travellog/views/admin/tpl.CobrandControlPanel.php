<div>
	<a href="/admin/cobrandControlPanel.php">View List</a>&nbsp; | &nbsp;<a href="/admin/cobrandControlPanel.php?action=add">Add Cobrand Domain</a>
</div>
<?php if('showForm'==$templateMethod): ?>
	<h2>Add/Edit Cobrand Domain</h2>
	<form action="/admin/cobrandControlPanel.php?action=save" method="post">
		<table>
			<tr>
				<td>ClientDomainID:<td>
				<td>
					<?= ($cobrandDomain->getCobrandDomainID() ? $cobrandDomain->getCobrandDomainID() : 'New Cobrand' ) ?>
				</td>
			</tr>
			<tr>
				<td>Cobrand URL:<td>
				<td>
					<?php if(array_key_exists('serverName',$errorStack)):?><?= $errorStack['serverName'] ?><br /><?php endif; ?>
					<input type="text" name="serverName" value="<?= $cobrandDomain->getServerName() ?>" /> 
				</td>
			</tr>
			<tr>
				<td>Site Name:<td>
				<td>
					<?php if(array_key_exists('siteName',$errorStack)):?><?= $errorStack['siteName'] ?><br /><?php endif; ?>
					<input type="text" name="siteName" value="<?= $cobrandDomain->getSiteName() ?>" />
				</td>
			</tr>
			<tr>
				<td>Group ID:<td>
				<td>
					<?php if(array_key_exists('groupID',$errorStack)):?><?= $errorStack['groupID'] ?><br /><?php endif; ?>
					<input type="text" name="groupID" value="<?= $cobrandDomain->getGroupID() ?>" />
				</td>
			</tr>
			<tr>
				<td>Site Description:<td>
				<td>
					<?php if(array_key_exists('siteDescr',$errorStack)):?><?= $errorStack['siteDescr'] ?><br /><?php endif; ?>
					<textarea name="siteDescr" cols="30" rows="10"><?= $cobrandDomain->getSiteDescr() ?></textarea>
				</td>
			</tr>	
			<tr>
				<td>Folder Name:<td>
				<td>
					<?php if(array_key_exists('folderName',$errorStack)):?><?= $errorStack['folderName'] ?><br /><?php endif; ?>
					<input type="text" name="folderName" value="<?= $cobrandDomain->getFolderName() ?>" />
				</td>
			</tr>
			<tr>
				<td>Copyright Year:<td>
				<td>
					<?php if(array_key_exists('copyrightYear',$errorStack)):?><?= $errorStack['copyrightYear'] ?><br /><?php endif; ?>
					<select name="copyrightYear">
						<option value="0" selected="true">Select Year</option>
						<?php for($i=2007;$i<2016;$i++): ?>
						<option value="<?= $i ?>" <?php if($i==$cobrandDomain->getCopyrightYear()): ?>selected="true"<?php endif; ?>><?= $i ?></option>
						<?php endfor; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Admin Emailaddress:<td>
				<td>
					<?php if(array_key_exists('adminEmail',$errorStack)):?><?= $errorStack['adminEmail'] ?><br /><?php endif; ?>
					<input type="text" name="adminEmail" value="<?= $cobrandDomain->getAdminEmail() ?>" />
				</td>
			</tr>
			<tr>
				<td>Email Sender Name:<td>
				<td>
					<?php if(array_key_exists('emailName',$errorStack)):?><?= $errorStack['emailName'] ?><br /><?php endif; ?>
					<input type="text" name="emailName" value="<?= $cobrandDomain->getEmailName() ?>" />
				</td>
			</tr>
			<tr>
				<td>Client URL:<td>
				<td>
					<?php if(array_key_exists('url',$errorStack)):?><?= $errorStack['url'] ?><br /><?php endif; ?>
					<input type="text" name="url" value="<?= $cobrandDomain->getUrl() ?>" />
				</td>
			</tr>
			<tr>
				<td>Online Advising:<td>
				<td><input type="checkbox" name="onlineAdvising" checked="<?= $cobrandDomain->getOnlineAdvising() ? 'true'  : 'false' ?>" /></td>
			</tr>
			<tr>
				<td>Allow Customized Header:<td>
				<td><input type="checkbox" name="allowCustomPageHeader" <?php if($cobrandDomain->getAllowCustomPageHeader()): ?>checked<?php endif; ?> /></td>
			</tr>
				<tr>
					<td colspan="2"><input type="submit"><td>
				</tr>
		</table>
		<input type="hidden" name="configID" value="<?= $cobrandDomain->getCobrandDomainID() ?>" />
	</form>
<?php else: ?>
	<h2>Cobrand Domain List</h2>
	<table border="1" cellpadding="10">
		<tr>
			<th width="250px">Cobrand URL</th>
			<th width="80px">Site Name</th>
			<th width="30px">CP Year</th>
			<th width="150px">Group</th>
			<th width="150px">Institution</th>
			<th width="200px">Admin Email Address</th>
			<th width="150px">Sender Name</th>
			<th width="30px">OLA</th>
			<th width="130px">Custom Page Header</th>
			<th widht="70px">Actions</th>
		</tr>
		<?php foreach($cobrandDomains as $iCobrandDomain):?>
		<tr>
			<td>
				<a href="http://<?= $iCobrandDomain->getServerName() ?>"><?= $iCobrandDomain->getServerName() ?></a><br />
			</td>
			<td>
				<?= $iCobrandDomain->getSiteName() ?><br />
				Client URL:
				<?php if(strlen($iCobrandDomain->getUrl())): ?>
					<a href="<?= $iCobrandDomain->getUrl() ?>"><?= $iCobrandDomain->getUrl() ?></a>
				<?php endif; ?><br />
				Folder Name: <?=$iCobrandDomain->getFolderName()?>
			</td>
			<td><?= $iCobrandDomain->getCopyrightYear() ?></td>
			<td>
				<?php if($iCobrandDomain->getGroupID() > 0):?>
					#<?= $iCobrandDomain->getGroup()->getGroupID() ?> -(<a href="<?= $iCobrandDomain->getGroup()->getFriendlyURL() ?>"><?= $iCobrandDomain->getGroup()->getName() ?></a>)<br />
					<strong>Username: </strong><?= $iCobrandDomain->getGroup()->getAdministrator()->getUserName() ?><br />
					<strong>EmailAddress: </strong><?= $iCobrandDomain->getGroup()->getAdministrator()->getEmail() ?><br />
					<strong>Password: </strong><?= $iCobrandDomain->getGroup()->getAdministrator()->getPassword() ?><br />
				<?php endif; ?>
			</td>
			<td>
				<?php if (array_key_exists($iCobrandDomain->getGroupID(), $clientAdvisors)) {
					$clientAccount = $clientAdvisors[$iCobrandDomain->getGroupID()];
				?>
					<a href="http://www.goabroad.com/admin/new_clients.cfm?clientID=<?=key($clientAccount)?>"><?=$clientAccount[key($clientAccount)]?></a>
					<br />
					<a href="/admin/assignClient.php?gID=<?=$iCobrandDomain->getGroupID()?>">
						<img src="http://images.goabroad.net/images/edit_caption.gif">						
					</a>
					<a href="/admin/assignClient.php?gID=<?=$iCobrandDomain->getGroupID()?>&amp;remove" onClick="return confirm('Remove Client Assignment?')">
						<img src="http://images.goabroad.net/images/delete_ico.gif">
					</a>
				<? } else { ?>
					<a href="/admin/assignClient.php?gID=<?=$iCobrandDomain->getGroupID()?>">
						<img src="http://images.goabroad.net/images/aadd_bg.gif">Add
					</a>
				<? } ?>
			</td>
			<td><?= $iCobrandDomain->getAdminEmail() ?></td>
			<td><?= $iCobrandDomain->getEmailName() ?></td>
			<td><?= $iCobrandDomain->getOnlineAdvising() ? 'Yes' : 'No' ?></td>
			<td><?= $iCobrandDomain->getAllowCustomPageHeader() ? 'Yes' : 'No' ?></td>
			<td>
				<a href="cobrandControlPanel.php?action=edit&cbID=<?= $iCobrandDomain->getConfigID() ?>">Edit</a>
				|
				<a href="cobrandControlPanel.php?action=delete&cbID=<?= $iCobrandDomain->getConfigID() ?>" onclick="return confirm('Are you sure you want to delete this cobrand?')">Delete</a>
			</td>
		</tr>
		<?php endforeach;?>
	</table>
<?php endif; ?>