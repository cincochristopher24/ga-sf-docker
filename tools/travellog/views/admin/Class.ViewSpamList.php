<?php
	/**
	 * Class.ViewSpamList.php
	 * created on 04 28, 08
	 */
	
	require_once("travellog/views/Class.AbstractView.php");

	class ViewSpamList extends AbstractView{
		
		function render(){
			$this->obj_template->set('sub_views', $this->sub_views);
			return $this->obj_template->fetch('tpl.ViewSpamList.php');
		}
		
	} 
?>
