<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>

<div class="area" id="intro">
	<div class="section" align="center">
		<h1>ROTATING PHOTOS</h1>		
	</div>
</div>
<a href="uploadPhoto.php?action=view">View All Uploaded Photos</a>
<br/> 

<form action="uploadPhoto.php" method="POST" enctype="multipart/form-data">
	Upload Photos: (Up to 2MB) <br/>
	to <?=$_SERVER["SERVER_NAME"]?>/images/rotating/<br />
	<input type="hidden" name="action" value="upload">
	<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="2000000" />
	<span id="add_more_upload-1" name="add_more_upload-1">
		<input id="uploadPhoto1" name="photos[]" type="file" />
		<input type="hidden" id="hidNum" name="hidNum" value="1">
	</span><br/>
	<input type="button" name="btnAddMorePhoto" id="btnAddMorePhoto" value="ADD MORE" onclick="RotatingPhotoJS.addMoreUpload();"/>
	<input type="submit" value="UPLOAD" />
</form>