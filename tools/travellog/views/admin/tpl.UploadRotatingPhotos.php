<? include_once('travellog/views/admin/tpl.AdminHeader.php');?>

<div class="area" id="intro">
	<div class="section" align="center">
		<h1>ROTATING PHOTOS</h1>		
	</div>
</div>

<div id = "top" class="area" style = "float:none;">
	<div style = "margin-left:auto;margin-right:auto;width: 980px;">
		<div class="wrapper">
			<a href="rotatingPhoto.php?action=view">View Rotating Photos</a>
			<form action="rotatingPhoto.php?action=upload" method="POST" enctype="multipart/form-data">
				Upload Photos: (Up to 2MB) <br/>
				to <?=$_SERVER["SERVER_NAME"]?>/images/rotating/<br />
				<input type="hidden" name="action" value="upload">
				<input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="2000000" />
				<span id="add_more_upload-1" name="add_more_upload-1">
					<input id="uploadPhoto1" name="photos[]" type="file" />
					<input type="hidden" id="hidNum" name="hidNum" value="1">
				</span><br/>
				<input type="button" name="btnAddMorePhoto" id="btnAddMorePhoto" value="ADD MORE" onclick="RotatingPhotoJS.addMoreUpload();"/>
				<input type="submit" value="UPLOAD" />
			</form>
		</div>
	</div>
</div>