<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('layoutID', 'main_pages');
	
	$d = new GaDateTime();

	include_once 'travellog/views/admin/tpl.AdminHeader.php';
	require_once 'travellog/controller/admin/Class.ViewFeaturedDatesController.php';
	require_once 'travellog/UIComponent/Photo/facade/PhotoUIFacade.php';

	$Photo = new PhotoUIFacade();
	$Photo->setContext('home');
	$Photo->setGenID(0);
	$Photo->setLoginID(0);
	$Photo->build();
?>

<div id = "intro_container">
	<div id = "intro" align = "center">
		<h1>PHOTOS</h1>
	</div>
</div>

<div id = "content_wrapper" class = "layout_2">
	<div id = "wide_column">
		<h2>
			<span><?=$title?></span>
		</h2>
		<div id = "groups_and_clubs" class = "section">
			<div class = "content">
				<div style = "padding-top: 10px;">
					<?=$search_template?>
					<span style = "float: right">
						<? if ($viewtype == 'entry') : ?>
							<strong>Journal Entry</strong> &nbsp;&nbsp;| &nbsp;&nbsp;							
							<a href="photos.php?viewtype=profile">Profile</a> &nbsp;&nbsp;| &nbsp;&nbsp;
							<a href="photos.php?viewtype=group">Group</a>
						<? endif; ?>
						<? if ($viewtype == 'profile') : ?>
							<a href="photos.php?viewtype=entry">Journal Entry</a> &nbsp;&nbsp;| &nbsp;&nbsp;
							<strong>Profile</strong> &nbsp;&nbsp;| &nbsp;&nbsp;
							<a href="photos.php?viewtype=group">Group</a>
						<? endif; ?>
						<? if ($viewtype == 'group') : ?>
							<a href="photos.php?viewtype=entry">Journal Entry</a> &nbsp;&nbsp;| &nbsp;&nbsp;
							<a href="photos.php?viewtype=profile">Profile</a> &nbsp;&nbsp;| &nbsp;&nbsp;
							<strong>Group</strong>
						<? endif; ?>
					</span>
				</div>
				<div id = "groups_list_main" style = "padding-top: 20px;">
					<? if (0 < count($photos)) : ?>
						<? if ($paging->getTotalPages() > $rowsperpage) : ?>
							<div class = "pagination">
								<div class = "page_markers">
									<?=$paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec?>
								</div>	
								<?$paging->getFirst()?>
								&nbsp;|&nbsp;
								<?$paging->getPrevious()?>
								&nbsp;&nbsp;
								<?$paging->getLinks()?>
								&nbsp;&nbsp;
								<?$paging->getNext()?>
								&nbsp;|&nbsp;
								<?$paging->getLast()?>
							</div>
						<? endif; ?>
						<ul class = "users">
							<?
								$mFactory =  GroupFactory::instance();

								foreach($photos as $eachphoto) :
									$_context  = $eachphoto->getContext();
									$photolink = $eachphoto->getPhotoLink();

									if (0 != strncmp("http://", $photolink , "7"))
										$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;

									switch (get_class($_context)) :					
										case 'TravelLog':
											$mGenId = $_context->getTravelID();
											$mContext = 'Journal';
											break;
										case 'TravelerProfile':
											$mGenId = $_context->getTravelerID();
											$mContext = 'Profile';
											break;
										case 'FunGroup':
											$mGenId = $_context->getGroupID();
											$mContext = 'Fungroup';
											break;
										case 'PhotoAlbum':
											$mGenId = $_context->getPhotoAlbumID();
											$mContext = 'Photoalbum';
											break;
									endswitch;
							?>
									<li>										
										<a class = "thumb" href = "javascript:void(0)" onclick = "PhotoService.loadPhotoPopup('<?=$eachphoto->getPhotoID()?>', 'gallery', '<?=$mGenId?>', '<?=$mContext?>')">
											<img src="<?=$photolink?>" alt="<?=$eachphoto->getCaption()?>" 
											<? if ($viewtype == 'profile'): ?>
												style = "height: 100px; width: 100px"
											<? else : ?>
												style = "height: 100px; width: 150px"
											<? endif; ?>
											/>
										</a>
										<div class = "details"
											<? if ($viewtype == 'profile'): ?>
												style = "margin-left: 45px; width: 480px;"
											<? else : ?>
												style = "margin-left: 85px; width: 480px;"
											<?endif;?>>
											<? if (strlen($eachphoto->getCaption())) : ?>
												<?
													$mCaption = (75 < strlen($eachphoto->getCaption()))
																? substr($eachphoto->getCaption(), 0, 75) . '...'
																: $eachphoto->getCaption();
												?>
												<p class = "gray"><?=$mCaption?></p>
											<? endif; ?>
											<hr width = "25%" align = "left" />
											<? if (get_class($_context) == 'TravelLog' || get_class($_context) == 'TravelerProfile') : ?>
												<p class = "gray">
					 								<a
														class = "username"
														href = "/<?=$_context->getTraveler()->getUserName()?>">
														<?=$_context->getTraveler()->getUserName()?>
													</a>
												</p>
												<? if (get_class($_context) == 'TravelLog') : ?>
													<p class = "gray">
														<a
															class="journal_title"
															href = "/journal-entry.php?action=view&amp;travelID=<?=$_context->getTravel()->getTravelID()?>">
															<?=$_context->getTravel()->getTitle()?>
														</a>
													</p>
												<? endif; ?>						
											<? elseif (get_class($_context) == 'FunGroup') : ?>
												<p class = "gray">
													<a href="/group.php?gID=<?=$_context->getGroupID()?>"><?=$_context->getName()?></a>
												</p>
											<? elseif (get_class($_context) == 'PhotoAlbum') : ?>
												<p class = "gray">
													<? $relGroup	=  $mFactory->create( array($_context->getGroupID()) );?>
													<a href="/group.php?gID=<?=$relGroup[0]->getGroupID()?>"><?=$relGroup[0]->getName()?></a>
												</p>
											<? endif; ?>
											<? /*$lastFeaturedDate = $eachphoto->getLastFeaturedDate()*/ ?>
											<p class = "gray">
												<?/*=strlen($lastFeaturedDate) ? '<strong>Date Last Featured: </strong>' . $d->set($lastFeaturedDate)->friendlyFormat() : ''*/ ?>
												<? /*if (ViewFeaturedDatesController::displayViewLink($eachphoto->getPhotoID(), 5)) :*/ ?>
													<a
														href	= "javascript: void(0)"
														class	= "edit"
														onClick = "window.open('viewFeaturedDates.php?sectionID=5&type=1&genID=<?=$eachphoto->getPhotoID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
														[view dates]
													</a>
												<? /*endif*/ ?>
											</p>
											<? if ($section->isItemFeatured($eachphoto->getPhotoID())) : ?>
												<p class = "gray">
													<strong>[Featured]</strong>
												</p>
												<p class = "gray">
													&raquo;
													<a
														href = "photos.php?feature=<?=$eachphoto->getPhotoID()?>&amp;remove"
														onclick = "javascript:return confirm('Do you want to REMOVE this Feature?');">
														Remove Feature
													</a>
												</p>
											<? else : ?>
												<p class = "gray">
													&raquo;
													<a href = "photos.php?feature=<?=$eachphoto->getPhotoID()?>">Feature This!</a>
												</p>
											<? endif; ?>
										</div>
									</li>
								<? endforeach; ?>
						</ul>

						<? if ($paging->getTotalPages() > $rowsperpage) : ?>
							<div class = "pagination">
								<div class = "page_markers">
									<?= $paging->getStartRow() + 1 . " - " . $paging->getEndRow() . " of " . $totalrec ?>
								</div>
								<?$paging->getFirst()?>
								&nbsp;|&nbsp;
								<?$paging->getPrevious()?>
								&nbsp;&nbsp;
								<?$paging->getLinks()?>
								&nbsp;&nbsp;
								<?$paging->getNext()?>
								&nbsp;|&nbsp;
								<?$paging->getLast();?>
							</div>
						<? endif; ?>
					<? else : ?>
						<h4 style = "text-align: center; text-transform: uppercase; font-weight: bold">Your search has returned 0 rows.</h4>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>

	<div id = "narrow_column">
		<div class = "section">
			<h2 style = "text-align: center">
				<? if (count($currFeat)) : ?>
					<? if (1 < count($currFeat)) : ?>
					 	CURRENTLY FEATURED PHOTOS
					<? else : ?>
						CURRENTLY FEATURED PHOTO
					<? endif; ?>
				<? endif; ?>
				<div style = "font-weight: normal; font-size: 12px; padding-top: 5px; text-transform: none">
					<strong>Caption: </strong>
					<?=$section->getCaption()?>
					<br />
					<strong>Quantity: </strong>
					<?=$section->getQuantity()?>
					<br />
					<a
						href	= "javascript: void(0)"
						class	= "edit"
				 		onClick	= "window.open('caption.php?sectionID=5','PopUp','left=300,height=200,width=400,status=yes')">
						change
					</a>
				</div>
			</h2>
			<div class = "content">
				<ul class = "users">
					<? $mFactory =  GroupFactory::instance(); ?>
					<?
						foreach ($currFeat as $eachphoto) :
						 	$_context  = $eachphoto->getContext();
							$photolink = $eachphoto->getPhotoLink();

							if (0 != strncmp("http://", $photolink , "7"))
								$photolink = "http://".$_SERVER['SERVER_NAME']. "/" . $photolink;	

							switch(get_class($_context)) :
								case 'TravelLog':
									$mGenId = $_context->getTravelID();
									$mContext = 'Journal';
									$mStyle = "width: 123px; height: 90px;";
									break;
								case 'TravelerProfile':
									$mGenId = $_context->getTravelerID();
									$mContext = 'Profile';
									$mStyle = "width: 90px; height: 90px;";
									break;
								case 'FunGroup':
									$mGenId = $_context->getGroupID();
									$mContext = 'Fungroup';
									$mStyle = "width: 123px; height: 90px;";
									break;
								case 'PhotoAlbum':
									$mGenId = $_context->getPhotoAlbumID();
									$mContext = 'Photoalbum';
									$mStyle = "width: 123px; height: 90px;";
									break;
								default:
									$mContext = "";
									$mStyle = "width: 123px; height: 90px;";
							endswitch;
					?>
							<li>
								<? if (0 < strlen($mContext)) : ?>
									<a class = "thumb" href = "javascript:void(0)" onclick = "PhotoService.loadPhotoPopup('<?=$eachphoto->getPhotoID()?>', 'gallery', '<?=$mGenId?>', '<?=$mContext?>')">
										<img src="<?=$photolink?>" alt="<?=$eachphoto->getCaption()?>" style = "<?=$mStyle?>" />
									</a>
								<? else : ?>
									<span class = "thumb">
										<img src="<?=$photolink?>" alt="<?=$eachphoto->getCaption()?>" style = "<?=$mStyle?>" />
									</span>
								<? endif; ?>
								<div class = "details" style = "width: 140px; margin-left: 53px">									
									<? if (get_class($_context) == 'TravelLog' || get_class($_context) == 'TravelerProfile') : ?>
			 							<h4 style = "padding:0px">
											<?
												$mName = (18 < strlen($_context->getTraveler()->getUserName()))
														? substr($_context->getTraveler()->getUserName(), 0, 18) . '..'
														: $_context->getTraveler()->getUserName();
											?>
											<a href="/<?=$_context->getTraveler()->getUserName()?>"><?=$mName?></a>
										</h4>
										<hr width = "98%" align = "left">
										<? if (get_class($_context) == 'TravelLog') : ?>
											<?
												$mTitle = (22 < (strlen($_context->getTravel()->getTitle())))
															? substr($_context->getTravel()->getTitle(), 0, 22) . '..'
															: $_context->getTravel()->getTitle();
											?>
											<a class="journal_title" href="/journal-entry.php?action=view&amp;travelID=<?=$_context->getTravel()->getTravelID()?>">
												<?=$mTitle?>
											</a>
										<? endif;?>						
									<? elseif (get_class($_context) == 'FunGroup') : ?>
										<?
											$mName = (18 < strlen($_context->getName()))
													? substr($_context->getName(), 0, 18) . '..'
													: $_context->getName();
										?>
										<h4 style = "padding:0px"><a href="/group.php?gID=<?=$_context->getGroupID()?>"><?=$mName?></a></h4>
										<hr width = "98%" align = "left">
									<? elseif (get_class($_context) == 'PhotoAlbum') : ?>
										<? $relGroup   =  $mFactory->create( array($_context->getGroupID()) );?>
										<?
											$mName = (18 < strlen($relGroup[0]->getName()))
													? substr($relGroup[0]->getName(), 0, 18) . '..'
													: $relGroup[0]->getName();
										?>
										<h4 style = "padding:0px"><a href="/group.php?gID=<?=$relGroup[0]->getGroupID()?>"><?=$mName?></a></h4>
										<hr width = "98%" align = "left">
									<? endif; ?>
									<p class = "gray">
										&raquo;
										<a
											href = "photos.php?feature=<?=$eachphoto->getPhotoID()?>&amp;remove"
											onclick = "javascript:return confirm('Do you want to REMOVE this Feature?');">
											Remove Feature
										</a>
									</p>
									<?php $lastFeaturedDate = $eachphoto->getLastFeaturedDate() ?>
									<p class = "gray" style = "font-size: 10px">
										<?= strlen($lastFeaturedDate) ? 'Featured: ' . $d->set($lastFeaturedDate)->friendlyFormat() : '' ?>
									</p>
									<? if (ViewFeaturedDatesController::displayViewLink($eachphoto->getPhotoID(), 5)) : ?>
										<p class = "gray">
											<a
												href	= "javascript: void(0)"
												class	= "edit"
												onClick = "window.open('viewFeaturedDates.php?sectionID=5&type=1&genID=<?=$eachphoto->getPhotoID()?>', 'windowname','menubar=no,toolbar=no,location=no,directories=no,status,scrollbars,resizable=no,width=450,height=300, top=' + ((screen.width/2)- 320) + ', left=400'); return false;">
												[view dates]
											</a>
										</p>
									<? endif; ?>
								</div>
							</li>
						<? endforeach; ?>		
					</ul>
				</div>
			</div>
			<div class = "foot"></div>
		</div>
	</div>
</div>