<?php
	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
?>

	<div id="wrapper1">
	<? if (isset($objTravType)) { ?>
		<table border="0" width="75%" align="center">	
			<tr>
				<form name="frmEdit" action="travelertype.php?mode=save&amp;travTypeID=<?=$objTravType->getTravelerTypeID()?>" method="post">
					<td>
						<input type="text" name="txtTravType" value="<?=$objTravType->getTravelerType()?>" />
					</td>
					<td>
						<input type="submit" name="Save" value="Save" />
					</td>
				</form>
				<form name="frmCancelEdit" action="travelertype.php" method="post">
					<td>
						<input type="submit" name="Cancel" value="Cancel" />
					</td> 
				</form>
			</tr>
		</table>
	<? } else { ?>
					
		<table border="0" width="75%" align="center">				
			<form name="frmQuickAdd" action="travelertype.php?mode=save" method="post">
				<tr height="50">
					<td> <b>Quick Add Traveler Type</b>
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" name="txtTravType" value="" />
					</td>
					<td>
						<input type="submit" name="Add" value="Add Traveler Type" />
					</td>
				</tr>
			</form>
		</table>
		
		
		<table border="0" width="75%" align="center">				
			<? if (0 < count($allTravType)) { ?>
				<tr>
					<td><b>All Traveler Types</b>
					</td>
				</tr>
				<? foreach ($allTravType as $eachtravtype) { ?>
					<tr>
						<td>
							<?= $eachtravtype->getTravelerType() ?>
						</td>
						<td>
							<a href="travelertype.php?mode=edit&amp;travTypeID=<?= $eachtravtype->getTravelerTypeID() ?>">
							Edit
							</a>
						</td>
						<td>
							<a onclick="javascript:return confirm('Do you want to DELETE this Traveler Type?')" href="travelertype.php?mode=delete&amp;travTypeID=<?= $eachtravtype->getTravelerTypeID() ?>">
							Delete
							</a>
						</td> 
					</tr>
				<? } ?>		
			
			<? } else { ?>
				<tr>
					<td>There are no Traveler Types yet.
					</td>
				</tr>
			<? } ?>
					
		</table>
		
		

	<? } ?>
		
	</div>			
	