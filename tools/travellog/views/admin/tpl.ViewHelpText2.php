<?php
/*
 * Created on 12 03, 07
 *
 */
 
 Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
  			   	
?>
  <script type="text/javascript" src="/js/jquery-1.1.4.pack.js"></script>	
  <script type="text/javascript">
		jQuery.noConflict();
		jQuery(document).ready(function() {
			jQuery("#index").change(function(){
					jQuery("#sitename").empty();
					jQuery.get("/admin/helptext2.php",{action : "getsites_aj", hkey : jQuery("#index option:selected").text()}, function(data) { jQuery("#sitename").append(data) } );	
			});
		});
		
		
		function updateHelp(){
			hform = document.getElementById("helpform");
			hform.action = 'helptext2.php?action=update&hkey=' + hkey + '&index=' + index + '&sitename=' + siteName;			
			hform.submit();
		} 	
		
		function deleteHelp() {
			if (!confirm('Are you sure you want to delete this Help Text?'))
				return;
			
			hform = document.getElementById("helpform");
			hform.action = 'helptext2.php?action=delete&hkey=' + hkey + '&index=' + index + '&sitename=' + siteName;			
			hform.submit();
		} 	  	
  </script> 	

  <h2>Help Text Management</h2>		

  <br />
  <a href="helptext2.php?action=add">Add New Helptext</a> 
  <br />  
  
  <?if (isset($message)):?>
  	<br />
  	<ul><li><?=$message?></li></ul>	
  	<br />
  <?endif;?> 

  <h3>Help Keys / Site</h3>
  <form name="keysform"  method="post" action="helptext2.php" >	
	  <select name="index" id="index" >
	  	<?$ctr=0;?>
	  	<?foreach($rsar as $row) :?>
	     
	    	<option value="<?=$ctr?>" <?if ($ctr==$index) echo ' selected ' ?>    ><?=$row['hkey']?></option>
	    	<? if ($ctr == $index) : ?>
		    	<script type="text/javascript">		    
		    		var hkey = '<?=$row['hkey']?>';
		    		var index = <?=$index?>
		    	</script> 
	    	<? endif; ?>
	    	<?$ctr++?>
	    <?endforeach;?>
	  </select>
	 
	 
		  <select name="sitename" id="sitename" >
	  			<?foreach($sitesAr as $site) :?>
	  				<option value="<?=$site?>" <?if ($siteName==$site) echo ' selected ' ?> ><?=$site?></option>
	  				<? if ($siteName == $site) : ?>
				    	<script type="text/javascript">		    			    		
				    		var siteName = '<?=$siteName?>';
				    	</script> 
			    	<? endif; ?>  				
	  			<?endforeach;?>
	  	  </select>	
  	 
  	  
  	  <input type="submit" value="Search">
  </form>
  
  
  
  <br /><br />
  <form name="helpform" id="helpform" method="post" >
	  <div>
	  	  <h3>Help Text Description</h3>
	  	  <textarea id="helpdesc" name="helpdesc" rows="3" cols="80" wrap="on"><?=$description?></textarea>			 
	  </div>
	  
	  <br /><br />
	  <?if (isset($helptext)) :?>
	  	<div>  	
	  	  <h3>Help Text</h3>		
		  <textarea name="helptext" rows="10" cols="80" wrap="on"><?=$helptext?></textarea>  
	  	</div>
	  <?endif; ?>  	    	      
  </form>
  <input type="submit" name="name" value="Update" title="Update edits on Help Text and Description" onclick="updateHelp()"/>
  <input type="submit" name="name" value="Delete" title="Delete this Help Text" onclick="deleteHelp()"/>