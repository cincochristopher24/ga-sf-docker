<table class="table_popbox">
	<tbody>
		<tr>
			<td class="popbox_topLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_topRight"></td>
		</tr>
		<tr>
			<td class="popbox_border"></td>
			<td class="popbox_content">
				<?php if( count($surveys) ): ?>
					<h4 class="header" id="popup_header"><strong>CHOOSE A FORM</strong></h4>
					<div class="confirm" id="popup_confirm">
						<div class = "formcont" id="formcont">
							<ul id="imageList">
								<?	foreach($surveys as $i => $survey): ?>
									<li>
										<a href="/surveyform.php?frmID=<?=$survey->getSurveyFormID()?>&amp;gID=<?=$groups[$i]->getGroupID()?>">
											<strong><?=htmlspecialchars(stripslashes(trim($survey->getName())),ENT_QUOTES)?></strong>
										</a>&nbsp;
										<i>(<?=$groups[$i]->getName()?>)</i>
									</li>
								<?	endforeach; ?>
							</ul>
						</div>
					</div>
				<?php else: ?>
					<h4 class="header" id="popup_header"><strong>NO FORM FOUND</strong></h4>
					<div class="confirm" id="popup_confirm">
						Sorry but you don't have any more form to fill out.
					<div>
				<?php endif; ?>
				<div class="buttons_box" id="popup_buttons">
					<input type="button" class="prompt_button" value="Close" onclick="travelerDashboard.closeSurveyList(); return false;" />
				</div>
			</td>
			<td class="popbox_border"></td>
		</tr>
		<tr>
			<td class="popbox_bottomLeft"></td>
			<td class="popbox_border"></td>
			<td class="popbox_bottomRight"></td>
		</tr>
	</tbody>
</table>