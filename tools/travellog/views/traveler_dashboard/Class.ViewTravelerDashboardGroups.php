<?php
	
	require_once("Class.Template.php");

	class ViewTravelerDashboardGroups{
		
		private $mTraveler	=	NULL;
		private $mGroups = array();
		
		private $mCobrandGroup = NULL;
		private $mShowAddGroupLink = TRUE;
		private $mAddGroupLink = "/group.php?mode=add&advisor";
		
		public function setTraveler($traveler=NULL){
			$this->mTraveler = $traveler;
			
			if( SiteContext::getInstance()->isInCobrand() ){
				$this->mCobrandGroup = new AdminGroup(SiteContext::getInstance()->getGroupID());
				$this->mGroups = $this->mTraveler->getTravelerCBSubGroups($this->mCobrandGroup);
				if( $this->mCobrandGroup->getAdministratorID() != $this->mTraveler->getTravelerID() 
					&& !$this->mCobrandGroup->isStaff($this->mTraveler->getTravelerID()) ){
					$this->mShowAddGroupLink = FALSE;
				}else{
					$this->mAddGroupLink = "/group.php?mode=add&pID=".$this->mCobrandGroup->getGroupID();
				}
			}else{
				$this->mTraveler->setIsSubGroup(FALSE);
				$cond = new Condition;
				$cond->setAttributeName("name");
				$cond->setOperation(FilterOp::$ORDER_BY);
				$order = new FilterCriteria2();
				$order->addCondition($cond);

				$this->mGroups = $this->mTraveler->getGroups($order);
			}
			
			ViewTravelerDashboard::setTravelerGroupCount(count($this->mGroups));
		}
		
		public function render(){
			$owned = array();
			$administered = array();
			$staffed = array();
			$other = array();
			
			foreach($this->mGroups AS $group){
				if( $this->mTraveler->getTravelerID() == $group->getAdministratorID() ){
					$owned[] = $group;
				}elseif( $group->isStaff($this->mTraveler->getTravelerID()) ){
					if( 0 == $group->getParentID() ){
						$administered[] = $group;
					}else{
						$staffed[] = $group;
					}
				}else{
					$other[] = $group;
				}
			}
			$this->mGroups = NULL;
			unset($this->mGroups);
			
			$tpl = new Template();
			$tpl->set("travelerID",$this->mTraveler->getTravelerID());
			$tpl->set("owned",$owned);
			$tpl->set("administered",$administered);
			$tpl->set("staffed",$staffed);
			$tpl->set("other",$other);
			$tpl->set("isInCobrand",SiteContext::getInstance()->isInCobrand());
			$tpl->set("showAddGroupLink",$this->mShowAddGroupLink);
			$tpl->set("addGroupLink",$this->mAddGroupLink);
			$tpl->out("travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardGroups.php");
		}
	}