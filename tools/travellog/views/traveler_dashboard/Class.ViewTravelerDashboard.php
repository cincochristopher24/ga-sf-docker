<?php

	require_once("Class.Template.php");
	require_once("travellog/views/traveler_dashboard/Class.ViewTravelerDashboardSteps.php");
	require_once("travellog/views/traveler_dashboard/Class.ViewTravelerDashboardNetworkFeeds.php");
	require_once("travellog/views/traveler_dashboard/Class.ViewTravelerDashboardMessageCenter.php");
	require_once("travellog/views/traveler_dashboard/Class.ViewTravelerDashboardNotifications.php");
	require_once("travellog/views/traveler_dashboard/Class.ViewTravelerDashboardShortcuts.php");
	require_once("travellog/views/traveler_dashboard/Class.ViewTravelerDashboardGroups.php");
	
	class ViewTravelerDashboard{
		
		private $mParams	=	array(	"action"	=>	NULL,
		  								"mode"		=>	NULL,
		 								"sgID"		=>	NULL	);
		private $mTraveler	=	NULL;
		private static $mTravelerGroupCount = 0;
		
		public function setParams($params=array()){
			$this->mParams = array_merge($this->mParams,$params);
		}
		public function setTraveler($traveler=NULL){
			$this->mTraveler = $traveler;
		}
		
		public static function setTravelerGroupCount($groupCount=0){
			self::$mTravelerGroupCount = $groupCount;
		}
		public static function getTravelerGroupCount(){
			return self::$mTravelerGroupCount;
		}
		
		public function render(){
			$networkFeedsView = new ViewTravelerDashboardNetworkFeeds();
			$networkFeedsView->setTraveler($this->mTraveler);
			
			$messageCenterView = new ViewTravelerDashboardMessageCenter();
			$messageCenterView->setTraveler($this->mTraveler);
			
			$notificationsView = new ViewTravelerDashboardNotifications();
			$notificationsView->setTraveler($this->mTraveler);
		
			$shortcutsView = new ViewTravelerDashboardShortcuts();
			$shortcutsView->setTraveler($this->mTraveler);
		
			$groupsView = new ViewTravelerDashboardGroups();
			$groupsView->setTraveler($this->mTraveler);
		
			$stepsView = new ViewTravelerDashboardSteps();
			$stepsView->setTraveler($this->mTraveler);
		
			$tpl = new Template();
			$tpl->set("stepsView",$stepsView);
			$tpl->set("networkFeedsView",$networkFeedsView);
			$tpl->set("messageCenterView",$messageCenterView);
			$tpl->set("notificationsView",$notificationsView);
			$tpl->set("shortcutsView",$shortcutsView);
			$tpl->set("groupsView",$groupsView);
			$tpl->out("travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardContent.php");
		}
	}