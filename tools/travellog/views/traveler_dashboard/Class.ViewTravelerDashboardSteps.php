<?php
	
	require_once("Class.Template.php");
	require_once("travellog/factory/Class.FileFactory.php");

	class ViewTravelerDashboardSteps{
		
		private $mTraveler	=	NULL;
		
		public function setTraveler($traveler=NULL){
			$this->mTraveler = $traveler;
		}
		
		public function render(){
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('TravelMapPeer', array('path' => 'travellog/model/socialApps/mytravelmap/'));
			$file_factory->registerClass('TravelSchedule');
			$file_factory->registerClass('ProfileQuestion', array('path' => 'travellog/model/travelbio/'));
			$file_factory->registerClass('AddressBookEntry');	
			
			$userMap = $file_factory->invokeStaticClass('TravelMapPeer', 'getTravelMap', array($this->mTraveler->getTravelerID()));						
			
			$travelPlansUser = $file_factory->invokeStaticClass('TravelSchedule',	'getTravelPlansUserWithTravelerID', array($this->mTraveler->getTravelerID()));
			$travelItems = $file_factory->invokeStaticClass('TravelSchedule', 'getOrderedTravels', array($travelPlansUser['userID']));
			
			$hasPhotos = $this->mTraveler->hasPhotos(); 
			$hasVideos = $this->mTraveler->hasVideos();
			
			// changed steps implementation
			// removed extra js and css (carousel, ready , etc.)
			$step1Done = $this->mTraveler->isProfileCompleted($includePersonals=false);
			$step2Done = count($file_factory->invokeStaticClass('AddressBookEntry', 'getAddressBookEntriesByOwnerTravelerID', array($this->mTraveler->getTravelerID())));
			$step3Done = $this->mTraveler->getCountTravelJournals(true);
			$step4Done = ViewTravelerDashboard::getTravelerGroupCount();
			$step5Done = ($hasPhotos && $hasVideos);
			$step6Done = $file_factory->invokeStaticClass('ProfileQuestion', 'getNumberofQuestionsWithAnswer', array($this->mTraveler->getTravelerID()));
			$step7Done = count($travelItems);
			$step8Done = count($userMap->getCountriesTraveled());
			$step9Done = $this->mTraveler->hasSyncedApplications();
			$stepsComplete = ($step1Done && $step2Done && $step3Done && $step4Done && $step5Done && $step6Done && $step7Done && $step8Done && $step9Done);
			
			if(!$stepsComplete || $this->mTraveler->getShowSteps()){
	 			$jscode = <<<BOF
	 				<script type="text/javascript">
						var initiallyHidden = true;
						var hideShowClicked = function(){
							jQuery('#stepOptionText').text('');
							jQuery('#hideShowLoader').show();
							jQuery('.stepOption').unbind();
							if(jQuery('.stepOption').is('.stepOptionShow')){
								hideSteps({id: {$this->mTraveler->getTravelerID()}, showSteps: 1, mode: 'temp_show'});
							}else{
								hideSteps({id: {$this->mTraveler->getTravelerID()}, showSteps: 0, mode: 'temp_hide'});
							}
							return false;
						}
						var hideSteps = function(params){
							jQuery.ajax({
								url: '/ajaxpages/unshowSteps.php',
								data: 'travelerid='+params.id+'&showSteps='+params.showSteps,
								type: 'POST',
								success: function(html){
									if(params.mode == 'permanent'){
										jQuery('.stepContainer').hide();
									}else if(params.mode == 'temp_show'){
										jQuery('#stepOptionText').text('Hide Steps');
										jQuery("#thisCont").show();
										jQuery("#stepText").show();
										jQuery('.stepOption').toggleClass('stepOptionShow');
									}else{
										jQuery('#stepOptionText').text('Show Steps');
										jQuery("#thisCont").hide();
										jQuery("#stepText").hide();
										jQuery('.stepOption').toggleClass('stepOptionShow');
									}
								},
								complete: function(){
									jQuery('#hideShowLoader').hide();
									jQuery('.stepOption').bind('click', hideShowClicked);
									if(params.mode == 'temp_show' && initiallyHidden){
										jQuery("#steps_carousel").show();
										jQuery("#steps_carousel").jcarousel({
											visible: 7
										});
										initiallyHidden = false;
									}
								}
							});
						};

						jQuery(document).ready(function(){
							if(jQuery("#steps_carousel").is(':visible')){
								initiallyHidden = false;
								jQuery("#steps_carousel").jcarousel({
									visible: 7
								});
							}
							if(jQuery('a.stepDone').length >= 9){
								hideSteps({id: {$this->mTraveler->getTravelerID()}, showSteps: 0, mode: 'permanent'});
							}
							
							jQuery('.stepOption').bind('click', hideShowClicked);
						});
					</script>
BOF;
				
				Template::includeDependentJs("/min/f=js/jquery.jcarousel.js");
				Template::includeDependent($jscode);
				
				$tpl = new Template();
				$tpl->set("travelerID",$this->mTraveler->getTravelerID());
				$tpl->set("showSteps", $this->mTraveler->getShowSteps());
				$tpl->set("step1Done", $step1Done);
				$tpl->set("step2Done", $step2Done);
				$tpl->set("step3Done", $step3Done);
				$tpl->set("step4Done", $step4Done);
				$tpl->set("step5Done", $step5Done);
				$tpl->set("step6Done", $step6Done);
				$tpl->set("step7Done", $step7Done);
				$tpl->set("step8Done", $step8Done);
				$tpl->set("step9Done", $step9Done);
				$tpl->set("redirectToPhoto", ($hasPhotos) ? false : true);
				if( isset($GLOBALS['CONFIG']) ){
					$tpl->set("siteName", $GLOBALS['CONFIG']->getSiteName());
				}else{
					$tpl->set("siteName", 'GoAbroad.net');
				}	
				$tpl->out("travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardSteps.php");
			}
		}
	}