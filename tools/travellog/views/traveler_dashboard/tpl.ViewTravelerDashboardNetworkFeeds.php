<?php
//expected variable: travelerID
?>
<div class="section" id="feeds">
	<h2>
		<span>Updates</span>&nbsp;<img src="/images/loading_small.gif" id="load_recent_feeds_status" style="display:none;"/>
	</h2>
	<div class="content">
		<div>
			<ul id="traveler_feed_list" class="feedlist">
			</ul>
			<span id="load_previous_feeds" class="jRight" style="display: none;">
				<a href="javascript:void(0)" onclick="travelerDashboard.fetchPreviousFeeds()" class="curveLink bottom_shift"><span><img src="/images/loading_small.gif" id="load_previous_feeds_status" style="display:none;margin-bottom:-3px" width="16" height="16"/> Load Previous</span></a>
			</span>
		</div>
	</div>
</div>
<script type="text/javascript">
	travelerDashboard.travelerID = <?=$travelerID?>;
	var localTime = new Date(); 
	var unixTime = Math.floor(localTime.getTime() / 1000);
	var clientTimeZone = (unixTime - <?=mktime()?>);
	travelerDashboard.timezone = Math.round(clientTimeZone / 3600);
	travelerDashboard.loadInitialFeeds();
</script>
