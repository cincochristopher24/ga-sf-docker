<?php
//expected variables: isNotificationShown, notifications
?>
<?php if( $isNotificationShown ): ?>
	<div class="section">
		<h2>
			<span>Notifications</span>
		</h2>
		<div class="content">
			<!-- Begin Notifications -->
			<div style="" id="notifications">
				<strong>You have </strong>
				<?=implode($notifications,",&nbsp;")?>
			</div>
			<!-- End Notifications -->
		</div>
		<div class="foot"></div>
	</div>
	
	<?php if( 1 < count($surveys) ): ?>
		<div id="survey_selection" style="display:none;">
			<ul>
				<?	foreach($surveys as $i => $survey): ?>
					<li>
						<a href="/surveyform.php?frmID=<?=$survey->getSurveyFormID()?>&amp;gID=<?=$groups[$i]->getGroupID()?>" onclick="CustomPopup.removePopup();" title="Survey from <?=$groups[$i]->getName()?>">
							<strong><?=htmlspecialchars(stripslashes(trim($survey->getName())),ENT_QUOTES)?></strong>
						</a>
					</li>
				<?	endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
	
<?php endif; ?>