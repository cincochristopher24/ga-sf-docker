<div id="rundown">
	
	<div id="content_wrapper" class="yui-skin-sam">
		<?php if(isset($stepsView)): ?>
		<?php $stepsView->render()?>
		<?php endif; ?>
		<div id="wide_column" class="layout2">
			
			<!-- Network feed component -->
			<?php if(isset($networkFeedsView)): ?>
			<?php $networkFeedsView->render()?>
			<?php endif; ?>
			
			<!-- Message center component -->
			<?php if(isset($messageCenterView)): ?>
			<?php $messageCenterView->render()?>
			<?php endif; ?>
		</div><!-- END layout2-->
		
		<div id="narrow_column">
			<!-- Dashboard notifications component -->
			<?php if(isset($notificationsView)): ?>
			<?php $notificationsView->render()?>
			<?php endif; ?>
			
			<!-- Dashboard shortcuts component -->
			<?php if(isset($shortcutsView)): ?>
			<?php $shortcutsView->render()?>
			<?php endif; ?>
			
			<!-- Dashboard fb activity -->
			<?php
            	$fbRecommendationsTemplate = new Template();
				$fbRecommendationsTemplate->out("travellog/views/tpl.fbRecommendationsBox.php");
            ?> 
			
			<!-- Dashboard groups component -->
			<?php if(isset($groupsView)): ?>
			<?php $groupsView->render()?>
			<?php endif; ?>
			
		</div><!-- end narrow_column -->
		
	</div><!-- end content_wrapper -->
	
</div>