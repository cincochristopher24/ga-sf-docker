<?php
	require_once 'Class.Template.php';
	require_once 'travellog/model/Class.ImportantLinks.php';

	class ViewImportantLinks{

		private $mLinkId	= 0;
		private $mGroupId	= 0;
		private $mLoggedId	= 0;
		private $mText		= '';
		private $mLink		= '';
		private $mGroup		= null;
		private $mIsAdmin	= false;
		private $mArrLink	= array();

		function setGroupId($_groupId = 0){
			$this->mGroupId = $_groupId;
		}
		function setLinkId($_linkId = 0){
			$this->mLinkId = $_linkId;
		}
		function setText($_text = ''){
			$this->mText = $_text;
		}
		function setLink($_link = ''){
			$this->mLink = $_link;
		}
		function setLoggedUserId($_userId = 0){
			$this->mLoggedId = $_userId;
		}
		function setIsAdminLogged($_isAdmin = false){
			$this->mIsAdmin = $_isAdmin;
		}

		function getArrLink(){
			return $this->mArrLink;
		}

		function add(){
			$importantLink = new ImportantLinks();
			$importantLink->setGroupId($this->mGroupId);
			$importantLink->setText($this->mText);
			$importantLink->setLink($this->mLink);
			$importantLink->Add();
		}

		function edit(){
			$importantLink = new ImportantLinks();
			$importantLink->setLinkId($this->mLinkId);
			$importantLink->setGroupId($this->mGroupId);
			$importantLink->setText($this->mText);
			$importantLink->setLink($this->mLink);
			$importantLink->Edit();
		}

		function delete(){
			$importantLink = new ImportantLinks();
			$importantLink->setLinkId($this->mLinkId);
			$importantLink->setGroupId($this->mGroupId);
			$importantLink->Delete();
		}

		function retrieve(){
			$importantLink = new ImportantLinks();
			$importantLink->setGroupId($this->mGroupId);
			$this->mArrLink = $importantLink->getImportantLinks();
		}

		function render(){
			$tpl = new Template();

			$tpl->set("groupId",	$this->mGroupId);
			$tpl->set("arrLink",	$this->mArrLink);
			$tpl->set("isAdmin",	$this->mIsAdmin);
			$tpl->out('travellog/views/tpl.IncGroupImportantLinks.php');
		}

		function renderPopUp(){
			$tpl = new Template();

			$tpl->out('travellog/views/tpl.ViewAddImportantLink.php');
		}
	}
?>