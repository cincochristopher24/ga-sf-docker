<?php
	/*************************************************************/
	/**	Created By: Cheryl Ivy Q. Go							**/
	/**	Created On: 24 April 2008  6:00 PM						**/
	/**	Purpose:	Display important links on group's homepage	**/
	/*************************************************************/
?>


<div id = "display_important_links" style="overflow: auto;">
	
	<? if (0 < count($arrLink)) : ?>
		<ul class="elements">
			<? foreach ($arrLink as $indLink) : ?>
			<li class="link_element">
				<?
					$mLink = $indLink->getLink();
					if (strcasecmp(substr($mLink, 0, 4), "http"))
						$mLink = "http://" . $mLink;
				?>
				<a class="link_title" href = "javascript:void(0)" class="linkname" onclick = "window.open('<?=$mLink?>','windowname2','menubar,toolbar,location,directories,status,scrollbars,resizable'); return false;"><?=$indLink->getText()?></a>

				<? if ($isAdmin) : ?>
							<em><?=$indLink->getLink()?></em>
							<p class="admin_links">
								<a class="edit" href = "javascript:void(0)" onclick = "ImportantLinks.setAction('edit'); ImportantLinks.setText('<?=htmlspecialchars(addslashes($indLink->getText()),ENT_QUOTES)?>'); ImportantLinks.setLink('<?=$indLink->getLink()?>');  ImportantLinks.setLinkId(<?=$indLink->getLinkId()?>); ImportantLinks.showEditBox()"><span>Edit</span></a> | 
								<a class="delete" href = "javascript:void(0)" onclick = "ImportantLinks.setAction('delete'); ImportantLinks.setLinkId(<?=$indLink->getLinkId()?>); ImportantLinks.deleteLink()"><span>Delete</span></a>
							</p>					
				<? endif; ?>
			</li>
			<? endforeach; ?>
		</ul>
	<? else : ?>
		<?/*<p class="side_help_text"> There are no important links added yet. </p> */ ?>
		<div class="sg_description">
			<h3></h3>
			<p>
				List URLs to your website, payments systems or reference sites.
			</p>
			<a class="button_v3 goback_button jLeft" href = "javascript:void(0)" onclick = "ImportantLinks.setAction('add'); ImportantLinks.showAddBox()"><strong>Add Link</strong></a>
		</div>
	<? endif; ?>
</div>