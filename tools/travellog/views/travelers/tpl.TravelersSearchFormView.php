<form> 
 	<div id="show_trav">
		<label for="traveler" style="">Show</label>
		<ul>
 			<li>
				<select name="action" id="ga_traveler_form_search">
					<!--option value="All" <?if( $contents['view_type'] == 1 ){?>selected<?}?>>All Travelers</option-->
					<option value="LastLogin"      <?if( $contents['view_type'] == 5 ){?>selected<?}?>>By Last Login</option>
					<? if($contents['is_login']): ?>
						<option value="InMyHometown" <?if( $contents['view_type'] == 2 ){?>selected<?}?>>Travelers From My Hometown</option>
					<? endif; ?>
					<option value="Newest" <?if( $contents['view_type'] == 3 ){?>selected<?}?>>Most Recent Travelers</option>
					<option value="MostPopular"    <?if( $contents['view_type'] == 4 ){?>selected<?}?>>Most Popular/Viewed Profile</option>
					<option value="CurrentlyIn"    <?if( $contents['view_type'] == 6 ){?>selected<?}?>>Currently In</option>
					<option value="PlanningToGo"   <?if( $contents['view_type'] == 7 ){?>selected<?}?>>Planning To Go To</option>
					<option value="HaveBeenTo"     <?if( $contents['view_type'] == 8 ){?>selected<?}?>>Have Been To</option>
				</select>
				<span id="COUNTRYSEARCH" style="display:none"></span>
  			</li>
  		</ul>
	</div>
</form>
<form onsubmit="return false;">	
	<div id="search_trav">
		<label for="traveler">Search</label>
		<ul>
			<li><input id="keywords" type="text" value="<?=$contents['keywords']?>" size="30" name="keywords"/></li> 
	  		<li>
	  			<select id="searchby" name="searchby">
	    			<option value="1" <?if($contents['search_by']==1){?>selected="true"<?}?>>Username</option>
					<option value="2" <?if($contents['search_by']==2){?>selected="true"<?}?>>Email</option>
					<option value="3" <?if($contents['search_by']==3){?>selected="true"<?}?>>By Interest</option>
					<option value="4" <?if($contents['search_by']==4){?>selected="true"<?}?>>By Name</option>
	  			</select>
	  		</li>
			<li><input class="search_but" type="image" src="/images/travelers/search_but.gif" value="Search" name="search" id="keywords_search"/></li>
		</ul>
 	</div>
 </form>