<div id="intro_container">
	<div id="intro">
		<h1>Meet Interesting Travelers.</h1>
		<div class="content">Connect with like-minded travelers before, during and after your travels.</div>	
	</div>
</div>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">		
		<div class="section">
			<div class="content">
				<?=$sub_views['LISTS_VIEW']->render()?>
			</div>
		</div>
	</div>
	<div id="narrow_column">										
		<div id="search" class="section" style="padding-bottom: 0px;">
			<div class="searchfield">
				<h2><span>Show</span></h2>
				<div class="content">
					<form> 
					 	<fieldset id="search_form" class="fieldset1">
				 			<p class="row_wrap">
								<select name="action" id="ga_traveler_form_search" style="margin-right:0;">
									<option value="LastLogin"      <?if( $contents['view_type'] == 5 ){?>selected<?}?>>By Last Login</option>

									<? if($contents['is_login'] AND !$contents['is_advisor'] AND 0 < $contents['obj_traveler']->getTravelerProfile()->getHTLocationID()): ?>
									<option value="InMyHometown" <?if( $contents['view_type'] == 2 ){?>selected<?}?>>Travelers From My Hometown</option>
									<? endif; ?>
									
									<option value="Newest" <?if( $contents['view_type'] == 3 ){?>selected<?}?>>Most Recent Travelers</option>
									<option value="MostPopular"    <?if( $contents['view_type'] == 4 ){?>selected<?}?>>Most Popular/Viewed Profile</option>
									
									<?php if($contents['numTravelersCurrentlyIn'] > 0): ?>
										<option value="CurrentlyIn"    <?if( $contents['view_type'] == 6 ){?>selected<?}?>>Currently In</option>
									<?php endif; ?>
									
									<?php if($contents['numTravelersPlanningToGoTo'] > 0): ?>
										<option value="PlanningToGo"   <?if( $contents['view_type'] == 7 ){?>selected<?}?>>Planning To Go To</option>
									<?php endif; ?>
									
									<?php if($contents['numTravelersHaveBeenTo'] > 0): ?>
										<option value="HaveBeenTo"     <?if( $contents['view_type'] == 8 ){?>selected<?}?>>Have Been To</option>
									<?php endif; ?>
								</select>
								<span id="COUNTRYSEARCH" style="display:none"></span>
							</p>
						</fieldset>
					</form>
				</div>
			</div>
			<div class="searchfield">
				<h2><span>Find Travelers</span></h2>
				<div class="content">
					<form onsubmit="return false;">	
						<fieldset id="search_trav" class="fieldset1">
							<p class="traveler_search_cont row_wrap">
								<input id="keywords" class="text" type="text" value="<?=$contents['keywords']?>" size="30" name="keywords"/>					  				<select id="searchby" name="searchby">
									<option value="1" <?if($contents['search_by']==1){?>selected="true"<?}?>>Username</option>
									<option value="2" <?if($contents['search_by']==2){?>selected="true"<?}?>>Email</option>
									<option value="4" <?if($contents['search_by']==4){?>selected="true"<?}?>>By Name</option>
						  		</select>
								<span id="search_btn" class="actions"><input class="search_but submit" type="submit" value="Search" name="search" id="keywords_search"/></span>
							</p>
					 	</fieldset>
				 	</form>
				</div>
			</div>	
		</div>
		<?php if(!isset($GLOBALS['CONFIG'])): ?>
		<div class="section">
			<script type="text/javascript"><!--
				google_ad_client = "ca-pub-0455507834956932";
				/* .NET Sidebar Ad */
				google_ad_slot = "3558779789";
				google_ad_width = 250;
				google_ad_height = 250;
				//-->
			</script>
			<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>		
		</div>      
		<?php endif; ?>
		<?php
			$fbRecommendationsTemplate = new Template();
			$fbRecommendationsTemplate->out("travellog/views/tpl.fbRecommendationsBox.php");
		?>
		<div class="section">
			<div id="travel_destinations">
				<h2><span>Map of travelers</span></h2>
				<div class="content row_wrap">
					<div id="map" class="img_border" style="">
						<div id="_map0" style="width:264px; height:300px"></div>
					</div>
					<p class="dark_gray">Click the balloon to view the travelers in that location</p>
				</div>
			</div>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery.ajax({
					url: "/ajaxpages/traveler-map.php",
					data: "view_type=<?php echo $contents['view_type']; ?>&amp;action=<?php echo $contents['type']; ?>",
					type: "GET",
					success: function(res){
						document.getElementById('_map0').className = "map";
						eval(res);
					}
				});	
			});
		</script> 	 
		</div>
	</div>
</div>
