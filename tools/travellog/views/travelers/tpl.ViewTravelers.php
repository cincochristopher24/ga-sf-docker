<?php 
		Template::setMainTemplateVar("title", "Meet Interesting Travelers on the ".((isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad"). " Network"); 
		if( isset($GLOBALS['CONFIG'])) {  
			     
		}
		else {
			Template::setMainTemplateVar( 'title'          , 'Meet Interesting Travelers on the GoAbroad Network');
		}
		Template::setMainTemplateVar( 'metaDescription', 'Travelers with various interests constantly add their journeys to the Network. Browse the growing travelers list and meet interesting people.');
		Template::setMainTemplateVar( 'layoutID'       , 'travelers/travelers'       );
		Template::setMainTemplateVar( 'page_location'  , 'Travelers'                 );
?>
<div id="intro_container">
	<div id="intro">
		<?/*h1><?=$contents['obj_help_text']->getHelpText('TRAVELER-TAB-SUBHEAD')?></h1>
		<div class="content">
			<?=HtmlHelpers::Textile($contents['obj_help_text']->getHelpText('TRAVELER-TAB'));?>
	  </div */?>
		<h1>Meet Interesting Travelers.</h1>
		<div class="content">Connect with like-minded travelers before, during and after your travels.</div>
	</div>
</div>

<div id="content_wrapper" class="layout_2">
	<div id="wide_column">		
		<div class="section">
			<div class="content">
				<?/*=$sub_views['LISTS_VIEW']->render()*/?>
			</div>
		</div>
	</div>
	<div id="narrow_column">										
		<div class="section">
			<div class="content">
				<form> 
				 	<div id="show_trav">
						<label for="ga_traveler_form_search">Show</label>
						<ul>
				 			<li id="search_method">
								<select name="action" id="ga_traveler_form_search">
									<option value="LastLogin"      <?if( $contents['view_type'] == 5 ){?>selected<?}?>>By Last Login</option>
									<? if($contents['is_login']): ?>
										<option value="InMyHometown" <?if( $contents['view_type'] == 2 ){?>selected<?}?>>Travelers From My Hometown</option>
									<? endif; ?>
									<option value="Newest" <?if( $contents['view_type'] == 3 ){?>selected<?}?>>Most Recent Travelers</option>
									<option value="MostPopular"    <?if( $contents['view_type'] == 4 ){?>selected<?}?>>Most Popular/Viewed Profile</option>
									<option value="CurrentlyIn"    <?if( $contents['view_type'] == 6 ){?>selected<?}?>>Currently In</option>
									<option value="PlanningToGo"   <?if( $contents['view_type'] == 7 ){?>selected<?}?>>Planning To Go To</option>
									<option value="HaveBeenTo"     <?if( $contents['view_type'] == 8 ){?>selected<?}?>>Have Been To</option>
								</select>
								<span id="COUNTRYSEARCH" style="display:none"></span>
				  			</li>
				  		</ul>
					</div>
				</form>
				
				<form onsubmit="return false;">	
					<div id="search_trav">
						<label for="traveler">Find Travelers</label>
						<ul class="traveler_search_cont">
							<li>
								<input id="keywords" class="text" type="text" value="<?=$contents['keywords']?>" size="30" name="keywords"/>					  		
					  			<select id="searchby" name="searchby">
					    			<option value="1" <?if($contents['search_by']==1){?>selected="true"<?}?>>Username</option>
									<option value="2" <?if($contents['search_by']==2){?>selected="true"<?}?>>Email</option>
									<option value="4" <?if($contents['search_by']==4){?>selected="true"<?}?>>By Name</option>
					  			</select>
					  		</li>
							<li id="search_btn"><input class="search_but" type="submit" value="Search" name="search" id="keywords_search"/></li>
						</ul>
				 	</div>
				 </form>
			</div>
		</div>
	  	<?/*=$sub_views['MAP_VIEW']->render()*/?>		 	 
		</div>
</div>
	
