<?php
require_once("travellog/views/Class.AbstractView.php");
class TravelersListsView extends AbstractView{
	function render(){
		$this->obj_template->set('sub_views', $this->sub_views);
		
		$obj_session = SessionManager::getInstance();
		$success_message = ($obj_session->get('custompopup_message')) ? $obj_session->get('custompopup_message') : NULL;
		$obj_session->unsetVar('custompopup_message');
		
		$this->obj_template->set('success', $success_message);
		
		return $this->obj_template->fetch('tpl.TravelersListsView.php');
	}
}
?>

