<div id="travel_destinations" class="section">
	<h2><span>Map of travelers</span></h2>
	<div class="content row_wrap">
		<div id="map" class="img_border" style="width:264px; height:300px;">
			<?
				$contents['obj_map_frame']->setWidth(264);
				$contents['obj_map_frame']->setHeight(300);
				$contents['obj_map_frame']->setConnectMarkers(false);
				$contents['obj_map']->addMapFrame($contents['obj_map_frame']);
				echo $contents['obj_map']->plotLocations($contents['arr_marker'], '', true);
			?>
		</div>
		<p class="dark_gray">Click the balloon to view the travelers in that location</p>
	</div>
</div>