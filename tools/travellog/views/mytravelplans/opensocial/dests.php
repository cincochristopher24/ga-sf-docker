<style>
.lists th {
     text-align: left;
     padding-left: 10px;
     background: #6d84b4;
}
.lists .spacer {
     background: none;
     border: none;
     padding: 0px;
     margin: 0px;
     width: 10px;
}
.lists th h4 { float: left; color: white; margin:5px 0; }
.lists th a { float: right; font-weight: normal; color: #d9dfea; }
.lists th a:hover { color: white; }
.lists td {
     margin:0px 10px;
     padding:0px;
     vertical-align:top;
     width:306px;
}
.lists .list {
     background: white none repeat scroll 0%;
     border-color: #BBBBBB;
     border-style:none solid;
     border-width:medium 1px;
}
.lists .list .list_item { border-top:1px solid #E5E5E5; padding: 10px; }
.lists .list .list_item.first { border-top: none; }
.lists .see_all {
     background: white none repeat scroll 0%;
     border-color: #BBBBBB;
     border-style:none solid solid;
     border-width:medium 1px 1px;
     text-align:left;
}
.lists .see_all div { border-top:1px solid #E5E5E5; padding:5px 10px; }

img{
	float: left; 
	width: 50px;
	height: 50px;
	margin: 0 10px 0 0;
	border: 1px solid #DDDDDD;
}
.destinationContainer{
	width: 260px;
	display: none; 
	font-size: 11px;	
	padding: 10px 10px; 
	position: absolute;	
	border: 1px solid #DDDDDD;
	background-color: #F5F5F5; 
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
}

.itineraryHeader{
	display: block;	
	font-weight: bold; 
	padding-bottom: 3px;
	border-bottom: 1px solid rgb(204, 204, 204); 
}

.closeButton {
	margin: 0px;
	width: 12px;
	height: 12px;
	float: right;
	cursor: pointer;
}

.destinationTitle{
	padding-top: 7px; 
	font-weight: bold; 
	color: rgb(68, 130, 190);
}

.aDestinationTitle{
	display: block; 
	font-size: 12px;
	font-weight: bold;
	cursor:pointer;
}
.dateRange{
	font-size: 11px; 
	color: rgb(136, 136, 136);
}

p{
	padding-bottom:2px;
}

</style>

<div id="destinationContainer" class="destinationContainer">
	<img src="http://www.goabroad.net/facebook/mytravelplans/images/delete.jpg" onclick="$('destinationContainer').style.display='none'" class="closeButton"/>
	<span class="itineraryHeader">Complete Itinerary</span>
	<p class="destinationTitle" id="title"></p>
	<p id="details"></p>
	<p id="country" style='font-weight:bold;font-size:12px'></p>
	<p id="dateRange" class="dateRange"></p>
	<p id="description"/></p>
</div>

<div style="padding: 0 10px 10px 10px;">
  <table class="lists" cellspacing="0" border="0"> 
    <tr>
      <th id="friendsDestinations">
        <h4 id="countryName"><?php echo $destinations[0]->getCountry()->getName(); ?></h4>
      </th>
      <th class="spacer"></th>
      <th id="myDestinations">
        <h4>My Travel Plans - <?php echo $destinations[0]->getCountry()->getName(); ?></h4>
      </th>
    </tr>
    <tr>
      <td class="list">
        <div class="list_item clearfix">
        <?php $i=0; foreach($destinations as $d) : $i++;  ?>
		  	<?php if($d->getContainerID() != $containerID) : ?>
	          <div style="margin: 0 0 5px 0">
				<img src='<?php echo $persons[$d->getContainerID()]['photo']; ?>' />
	            <div style="float: left;">
	              <a id='title_<?php echo $i ?>' class="aDestinationTitle" title="View Itinerary" onclick="viewDestination(this,'<?php echo $d->getDetails() ?>','<?php echo $d->getDescription() ?>','<?php echo $d->getDateRange() ?>')" >
	                <?php echo $d->getTitle(); ?>
	              </a>

	              <?php echo $d->getDateRange() ? $d->getDateRange() : 'No date specified.'; ?>
	              <div>
					<?php if($persons[$d->getContainerID()]['url']) : ?>
						<a style="display: block; font-size: 12px;" href="<?php echo $persons[$d->getContainerID()]['url']; ?>" title="View Profile" target="_blank">
							<?php echo $persons[$d->getContainerID()]['name']; ?>
						</a>
					<?php else: ?>  
						<span style="color:#999999"><?php echo $persons[$d->getContainerID()]['name'] ?> </span>
					<?php endif; ?> 
				  </div>
	            </div>
	            <div style="clear: both"></div>
	          </div>
          	<?php else: $myDestinations[] = $d; endif; ?>
        <?php endforeach; ?>
        </div>
      </td>
      <td class="spacer"></td>
      <td class="list">
        <div class="list_item clearfix">
        <?php foreach($myDestinations as $myD) : $i++; ?>
          <div style="margin: 0 0 5px 0">
			<img src='<?php echo $ownPhoto ?>' />
            <div style="float: left;">
              <a id='title_<?php echo $i ?>' class="aDestinationTitle" title="View Itinerary" onclick="viewDestination(this,'<?php echo $myD->getDetails() ?>','<?php echo $myD->getDescription() ?>','<?php echo $myD->getDateRange() ?>')" style="padding:5px 0px;">
                <?php echo $myD->getTitle(); ?>
              </a>
              <?php echo $myD->getDateRange() ? $myD->getDateRange() : 'No date specified.'; ?>
            </div>
            <div style="clear: both"></div>
          </div>
        <?php endforeach; ?>
        </div>
      </td>
    </tr>
    <tr>
      <td class="see_all">&nbsp;</td>
      <td class="spacer"></td>
      <td class="see_all">&nbsp;</td>
    </tr>
  </table>
</div>