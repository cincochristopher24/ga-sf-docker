<form id="travelForm" name="travelForm" action="index.php?action=save" method="post">
  <div id="ganet_goabroad_wrapper" class="box">

    <h2 class="fontColor2">
      	<?php if ($action == 'add') : ?> Add a New Travel Itinerary
      	<?php else: ?>  Modify Travel Itinerary
      	<?php endif; ?>
		<span style='padding-left:20px;font-size:13px;font-weight:normal'>* fields are required<span>
    </h2>

    <!-- itinerary name: -->
    <h4>* Give your itinerary a name:</h4>
    <input id="title" name="title" type="text" value="<?php echo htmlentities($title, ENT_QUOTES); ?>" class="size fontMed"/>
    <div id="titleError" style="display:none;"></div>
    <div class="helpText">Please name your trip, e.g. My World Tour in 2007, Exploring Greece, My Year Out</div>

    <!-- Itinerary Description: -->
    <h4>* Itinerary Description:</h4>
    <textarea id="details" name="details" value="<?php echo htmlentities($details, ENT_QUOTES); ?>" class="size sizeV fontMed"><?php echo htmlentities($details, ENT_QUOTES); ?></textarea>
    <div id="detailsError" style="display:none;"></div>
    <div class="helpText">Please write a few details about your trip, e.g. [My World Tour in 2007] My quest to set foot on every continent, [Exploring Greece] Satisfy my fascination with ancient temples, [My Year Out] I spent a fun year out before I started college.</div>

    <!-- DESTINATIONS -->
    <?php $divCount = 0; $currentDate = getDate(); ?>
    <?php if (empty($destinations)) : ?>
    <!-- ACTION = ADD -->
    <div id="destinations">

      <div id="<?php echo $divCount; ?>">

        <!-- Destination Country: -->
        <h4>* Select Destination Country:</h4>
        <select id=country name="countries" class="size fontMed">
          <option value=""></option>
        <?php foreach ($ctryList as $ctry) : ?>
          <option value="<?php echo $ctry->getCountryID(); ?>">
            <?php echo $ctry->getName(); ?>
          </option>
        <?php endforeach; ?>
        </select>                    
    	<div id="destinationsError" style="display:none;"></div>

        <!-- Destination Details: -->
        <h4>Destination Details:</h4>
        <textarea id="description" name="description" class="size sizeV fontMed"></textarea>

        <!-- Travel Start Date: -->
        <h4>Travel Start Date:</h4>
        <select id="fromMonth" name="fromMonth" onchange="updateEndDate(this);" class="fontMed">
        <?php foreach ($monthArray as $index => $month) : ?>
          <option value="<?php echo $index ?>" <?php if ($index == $currentDate['mon']): ?> selected="selected"<?php endif; ?>>
            <?php echo $month; ?>
          </option>
        <?php endforeach; ?>
        </select>
        <select id="fromDay" name="fromDay" onchange="updateEndDate(this);" class="fontMed">
          <option value="00"></option>
          <?php for($i=1; $i<32; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if ($i == $currentDate['mday']): ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <select id="fromYear" name="fromYear" onchange="updateEndDate(this);" class="fontMed">
          <option value="0000"></option>
          <?php for($i=2000; $i<2031; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if ($i == $currentDate['year']): ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <div id="fromDateError" style="display:none;"></div>
        <div class="helpText">Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</div>


        <!-- Travel End Date: -->
        <h4>Travel End Date:</h4>
        <select id="toMonth" name="toMonth" class="fontMed">
        <?php foreach ($monthArray as $index => $month) : ?>
          <option value="<?php echo $index ?>">
            <?php echo $month; ?>
          </option>
        <?php endforeach; ?>
        </select>
        <select id="toDay" name="toDay" class="fontMed">
          <option value="00"></option>
          <?php for($i=1; $i<32; $i++): ?>
          <option value="<?php echo $i; ?>">
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <select id="toYear" name="toYear" class="fontMed">
          <option value="0000"></option>
          <?php for($i=2000; $i<2031; $i++): ?>
          <option value="<?php echo $i; ?>">
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <div id="toDateError" style="display:none;"></div>
        <div class="helpText">Optional</div>

        <?php $divCount++; ?>
      </div><!-- end divCount -->
    </div><!-- end id="destinations" -->
    <?php else : ?>
    <!-- ACTION = EDIT -->
    <div id="destinations">
    <?php
      foreach ($destinations as $d) {
        $travelWishToCountryID = $d->getTravelWishToCountryID();
        $destTravItemID = $d->getTravelWishID();
        $destCountryID = $d->getCountry()->getCountryID();
        $destDescription = htmlentities($d->getDescription(), ENT_QUOTES);

        list($destFromYear, $destFromMonth, $destFromDay) = explode('-', $d->getStartDate());
        list($destToYear, $destToMonth, $destToDay) = explode('-', $d->getEndDate());
    ?>
      <div id="<?php echo $divCount; ?>">
        <?php if ($divCount == 0) { ?>
        <h4>Destination:</h4>
        <?php } else { ?>
        <h4 class="multiple">Destination:</h4>
        <?php } ?>
        <select id="country" name="countries" class="size fontMed">
          <option value=""></option>
        <?php foreach ($ctryList as $ctry) : ?>
          <option value="<?php echo $ctry->getCountryID(); ?>" <?php if ($ctry->getCountryID() == $destCountryID): ?> selected="selected"<?php endif; ?>>
            <?php echo $ctry->getName(); ?>
          </option>
        <?php endforeach; ?>
        </select>
    	<div id="destinationsError" style="display:none;"></div>

        <?php if ($divCount > 0) { ?>
        <a class="rmvDest" href="javascript:void(0);" onclick="rmvDest(this); return false;">Delete This Destination</a>
        <?php } ?>
        <!-- DESCRIPTION -->
        <h4>Description:</h4>
        <textarea id="description" name="description" value="<?php echo $destDescription; ?>" class="size sizeV fontMed"><?php echo $destDescription; ?></textarea>
        <!-- FROM DATE -->
        <h4>Travel Start Date:</h4>
        <select id="fromMonth" name="destFromMonth" class="fontMed">
        <?php foreach ($monthArray as $index => $month) : ?>
          <option value="<?php echo $index ?>" <?php if ($index == $destFromMonth): ?> selected="selected"<?php endif; ?>>
            <?php echo $month; ?>
          </option>
        <?php endforeach; ?>
        </select>
        <select id="fromDay" name="destFromDay" class="fontMed">
          <option value="00"></option>
          <?php for($i=1; $i<32; $i++): ?>
          <option value="<?php echo $i ?>" <?php if ($i == $destFromDay): ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <select id="fromYear" name="destFromYear" class="fontMed">
          <option value="0000"></option>
          <?php for($i=2000; $i<2031; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if($i == $destFromYear) : ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <div id="fromDateError" style="display:none;"></div>
        <div class="helpText">Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</div>
        <!-- TO DATE -->
        <h4>Travel End Date:</h4>
        <select id="toMonth" name="destToMonth" class="fontMed">
        <?php foreach ($monthArray as $index => $month) : ?>
          <option value="<?php echo $index ?>" <?php if ($index == $destToMonth): ?> selected="selected"<?php endif; ?>>
            <?php echo $month; ?>
          </option>
        <?php endforeach; ?>
        </select>
        <select id="toDay" name="destToDay" class="fontMed">
          <option value="00"></option>
          <?php for($i=1; $i<32; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if ($i == $destToDay): ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <select id="toYear" name="destToYear" class="fontMed">
          <option value="0000"></option>
          <?php for($i=2000; $i<2031; $i++): ?>
          <option value="<?php echo $i; ?>" <?php if($i == $destToYear) : ?> selected="selected"<?php endif; ?>>
            <?php echo $i; ?>
          </option>
          <?php endfor; ?>
        </select>
        <div id="toDateError" style="display:none;"></div>
        <div class="helpText">Optional</div>
        <?php $divCount++; ?>
      </div><!-- end divCount -->
      <?php }//end foreach ?>
    </div><!-- end destinations -->
    <?php endif; ?>

	<div style='padding-top:20px' id='buttons'>
	    <?php if ($action == 'add') : ?>
	      <a href="#headerLogo" onclick="validateForm(this);" class="button fontMed">Save</a>
	    <?php elseif ($action == 'edit') : ?>
	      <a href="#headerLogo" onclick="validateForm(this);" class="button fontMed">Save</a>
	      <input id="travelItemID" type="hidden" name="travelItemID" value="<?php echo $travelItemID; ?>" />
	    <?php endif; ?>
	      <a href="javascript: void(0);" id="linkAddDest" onclick="addDest()" class="button fontMed">Add Destination</a>
	      <a href="#headerLogo" id="linkTabHome" onclick="handleTabHome()" class="button fontMed">Back to Dashboard</a>
	</div>
  </div>
</form>

<div id="hiddenDiv" style="display: none">
  <h4 id="lblDestination">Destination:</h4>
  <select id="country" name="countries" class="size fontMed">
    <option value=""></option>
    <?php foreach ($ctryList as $ctry) : ?>
    <option value="<?php echo $ctry->getCountryID(); ?>">
      <?php echo $ctry->getName(); ?>
    </option>
    <?php endforeach; ?>
  </select>
  <a class="rmvDest" href="javascript: void(0);" onclick="rmvDest(this); return false;">Delete This Destination</a>
  <!-- DESCRIPTION -->
  <h4>Destination Details:</h4>
  <textarea id="description" name="description" value="" class="size sizeV fontMed"></textarea>
  <!-- FROM DATE -->
  <h4>Travel Start Date:</h4>
  <select id="fromMonth" name="fromMonth" onchange="updateEndDate(this);" class="fontMed">
  <?php foreach ($monthArray as $index => $month) : ?>
    <option value="<?php echo $index ?>" <?php if ($index == $currentDate['mon']): ?> selected="selected"<?php endif; ?>>
      <?php echo $month; ?>
    </option>
  <?php endforeach; ?>
  </select>
  <select id="fromDay" name="fromDay" onchange="updateEndDate(this);" class="fontMed">
    <option value="00"></option>
    <?php for($i=1; $i<32; $i++): ?>
    <option value="<?php echo $i; ?>" <?php if ($i == $currentDate['mday']): ?> selected="selected"<?php endif; ?>>
      <?php echo $i; ?>
    </option>
    <?php endfor; ?>
  </select>
  <select id="fromYear" name="fromYear" onchange="updateEndDate(this);" class="fontMed">
    <option value="0000"></option>
    <?php for($i=2000; $i<2031; $i++): ?>
    <option value="<?php echo $i; ?>" <?php if ($i == $currentDate['year']): ?> selected="selected"<?php endif; ?>>
      <?php echo $i; ?>
    </option>
    <?php endfor; ?>
  </select>
  <div id="fromDateError" style="display:none;"></div>
  <div class="helpText">Select either a specific date, month and year, year only or leave unanswered signifying a wishlist itinerary</div>
  <!-- TO DATE -->
  <h4>Travel End Date:</h4>
  <select id="toMonth" name="toMonth" class="fontMed">
  <?php foreach ($monthArray as $index => $month) : ?>
    <option value="<?php echo $index ?>">
      <?php echo $month; ?>
    </option>
  <?php endforeach; ?>
  </select>
  <select id="toDay" name="toDay" class="fontMed">
    <option value="00"></option>
    <?php for($i=1; $i<32; $i++): ?>
    <option value="<?php echo $i; ?>">
      <?php echo $i; ?>
    </option>
    <?php endfor; ?>
  </select>
  <select id="toYear" name="toYear" class="fontMed">
    <option value="0000"></option>
    <?php for($i=2000; $i<2031; $i++): ?>
    <option value="<?php echo $i; ?>">
      <?php echo $i; ?>
    </option>
    <?php endfor; ?>
  </select>
  <div id="toDateError" style="display:none;"></div>
  <div class="helpText">Optional</div>
</div>