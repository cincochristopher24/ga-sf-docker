<div id="ganet_app_container" class="box"> 
  	<input type="hidden" name="user" value="<?php echo $user; ?>"/>
	
	<div id="top_level">

    	<?php if ($ganetUserName) : ?>  	    		

			<p class="box fontMed fontColor1"><strong>You are currently using <?php echo $ganetUserName ?>.</strong></p>

    		<p id="syncAlert" class="paddingBottom10">
				This App is currently synchronized with 
				<?php echo "<a href='http://www.goabroad.net/".urlencode($ganetUserName)."' target='_blank'>".$ganetUserName."'s </a>";?> GoAbroad Network profile. 
				If you don't have any GoAbroad Network account with an email "<?php echo $ganetUserEmail;?>" 
				<a href="http://www.goabroad.net/feedback.php">please send us an email.</a>
			</p>
		<?php else: ?>
		    <p class="paddingBottom10">
				If you have a GoAbroad Network account, you can synchronize your <?php echo $container[0]; ?> My Travel Plans 
				application with your GoAbroad Network profile. Synchronizing means that both your <?php echo $container[0]; ?> 
				and GoAbroad Network profiles will show the same travel items no matter where you edit them. 
				To synchronize, type in your email for GoAbroad Network and password.
			</p>				
    	<?php endif; ?>
 				
 				<div class="padding10 borderGray bColor1">
  					<div class="line paddingBottom10 clear">
  						<div class="left size1of4">
      					<strong>*Email:</strong>
     					</div>
			<div class="left size3of4">
   					 <input type="text" name="email" id="email"/>
 						</div>

 					</div>
  					<div class="line paddingBottom10 clear">
  						<div class="left size1of4">        
     					<strong>*Password:</strong>
			</div>
 						<div class="left size3of4">	
		        <input type="password" name="password" id="password" onkeypress="pressSync(event)"/>
			</div>
	    </div>
	    
		<input type="button" value="Synchronize" id="linkDoSync" class="button fontMed" onclick="handleTabDoSync()"/>
		</div>

    </div>
</div>