<?php require_once('travellog/views/mytravelplans/facebook/tpl.menu.php'); ?>

<fb:request-form type="My Travel Plans" action="<?php echo $action; ?>" invite="true"
  content="
	You've been invited to add the My Travel Plans application!
	<fb:name uid="<?php echo $user ?>" firstnameonly="true" shownetwork="false"/> 
	wants you to add My Travel Plans so that you can share and compare your travel plans!
	<fb:req-choice url="http://apps.facebook.com/travel-todo/index.php" label="Add My Travel Plans!" />
  ">
  <fb:multi-friend-selector max="20" actiontext="Here are your friends who don't have My Travel Plans.  Invite them to share their travel plans with you!" showborder="true" rows="5" exclude_ids="<?=$arFriends?>">
</fb:request-form>