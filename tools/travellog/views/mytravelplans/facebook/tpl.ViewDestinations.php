
<style>
.lists th {
     text-align: left;
     padding: 5px 10px;
     background: #6d84b4;
}

.lists .spacer {
     background: none;
     border: none;
     padding: 0px;
     margin: 0px;
     width: 10px;
}

.lists th h4 { float: left; color: white; }
.lists th a { float: right; font-weight: normal; color: #d9dfea; }
.lists th a:hover { color: white; }

.lists td {
     margin:0px 10px;
     padding:0px;
     vertical-align:top;
     width:306px;
}

.lists .list {
     background:white none repeat scroll 0%;
     border-color:-moz-use-text-color #BBBBBB;
     border-style:none solid;
     border-width:medium 1px;
}

.lists .list .list_item { border-top:1px solid #E5E5E5; padding: 10px; }
.lists .list .list_item.first { border-top: none; }

.lists .see_all {
     background:white none repeat scroll 0%;
     border-color:-moz-use-text-color #BBBBBB rgb(187, 187, 187);
     border-style:none solid solid;
     border-width:medium 1px 1px;
     text-align:left;
}

.lists .see_all div { border-top:1px solid #E5E5E5; padding:5px 10px; }

</style>

<?php require_once('tpl.menu.php'); $myDestinations = array(); ?>

<div style="padding: 0 10px 10px 10px;">
  <table class="lists" cellspacing="0" border="0">
    <tr>
      <th>
        <h4><?php echo $destinations[0]->getCountry()->getName(); ?></h4>
        <!-- <a href="#">Hide</a> -->
      </th>
      <th class="spacer"></th>
      <th>
        <h4>My Travel Plans</h4>
        <!--<a href="#">Hide</a> -->
      </th>
    </tr>
    <tr>
      <td class="list">
        <div class="list_item clearfix">
        <?php foreach($destinations as $d) : ?>
		  	<?php if($d->getContainerID() != $containerID) : ?>
	          <div style="margin: 0 0 5px 0">
	            <fb:profile-pic uid="<?php echo $d->getContainerID(); ?>" size='square' linked="true" style="float: left; margin: 0 10px 0 0"/>
	            <div style="float: left;">
	              <a style="display: block; font-size: 12px; font-weight: bold;" href="#" title="View Itinerary" onclick="viewDestination(this, <?php echo $d->getTravelWishID(); ?>); return false;">
	                <?php echo $d->getTitle(); ?>
	              </a>
	              <?php echo $d->getCountry()->getName(); ?> - <?php echo $d->getDateRange() ? $d->getDateRange() : 'No date specified.'; ?>
	              <div><fb:name uid="<?php echo $d->getContainerID(); ?>" linked="true"/></div>
	            </div>
	            <div style="clear: both"></div>
	          </div>
          	<?php else: $myDestinations[] = $d; endif; ?>
        <?php endforeach; ?>
        </div>
      </td>
      <td class="spacer"></td>
      <td class="list">
        <div class="list_item clearfix">
        <?php foreach($myDestinations as $myD) { ?>
          <div style="margin: 0 0 5px 0">
            <fb:profile-pic uid="<?php echo $myD->getContainerID(); ?>" size='square' linked="true" style="float: left; margin: 0 10px 0 0"/>
            <div style="float: left;">
              <a style="display: block; font-size: 12px; font-weight: bold;" href="#" title="View Itinerary" onclick="viewDestination(this, <?php echo $myD->getTravelWishID(); ?>); return false;">
                <?php echo $myD->getTitle(); ?>
              </a>
              <?php echo $myD->getCountry()->getName(); ?> - <?php echo $myD->getDateRange() ? $myD->getDateRange() : 'No date specified.'; ?>
              <div><fb:name uid="<?php echo $myD->getContainerID(); ?>" linked="true" capitalize="true" /></div>
            </div>
            <div style="clear: both"></div>
          </div>
        <?php } ?>
        </div>
      </td>
    </tr>
    <tr>
      <td class="see_all"></td>
      <td class="spacer"></td>
      <td class="see_all"></td>
    </tr>

  </table>
<script>

function viewDestination(obj, travelItemID) {
  var ajax = new Ajax();
  ajax.responseType = Ajax.FBML;

  ajax.ondone = function(data) {
    new Dialog(Dialog.DIALOG_CONTEXTUAL).setContext(obj).showMessage('Complete Itinerary', data, 'Close');
  }
  ajax.onerror = function() {
    //TODO:
  }

  ajax.requireLogin = 1;
  var queryParams = { "ajaxAction":"viewDestination", "travelItemID":travelItemID };

  ajax.post("http://www.goabroad.net/facebook/mytravelplans/ajax.php", queryParams);
}
</script>