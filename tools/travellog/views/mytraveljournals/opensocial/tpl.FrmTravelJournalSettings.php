<?php if(count($args['userJournals'])): ?>

	<?php if(trim($appViewType) == 'home') : ?>
			
		<!-- Start Scene 1 :: When Traveler successfully synced his journals :: Note: Traveler has a journal -->
		
		<div style="display:block;">
		
			<p>The following are the currently shown journals in your public profile:</p>		
			
			<ul class="home_journals_list">
			   	<?php $i=0; foreach( $args['userJournals'] as $journal ) :
					$journalLink = osMytravelJournalsController::getLink(array_shift($journal->getEntries())); ?>

					<li>
						<a class="journal_title" href="<?php echo $journalLink ?>" target="_blank">						
							<?php echo $journal->getTitle() ?>
						</a>
					</li>
				<?php $i++; endforeach; ?>				            
			</ul>
			
			<p> If you wish to edit the list of journals to be shown in your profile, please go to your <a href="http://profile.myspace.com/Modules/Applications/Pages/Canvas.aspx?appId=127873" title="App Dashboard" target="_blank">App Dashboard</a></p>
		</div>	
		<!-- End Scene 1 -->
					

		<!-- Start Scene 3	:: Traveler installed the app and has not synced his account yet -->
		<div style="display:none;">
			<p><strong class="info">You haven't synchronized your account yet.</strong></p>			
			<br />
			
			<p> <a href="#" title="Sync your account"> Synchronize</a> your account now, to begin using the app or Go to your <a href="#" title="Go to your App Dashboard">App Dashboard</a>.</p>	
		</div>				
		<!-- End Scene 3 -->
		
		
	<?php else : ?>				
		<div id="frmsettings" class="frm_settings">
		<h2 class="header"> Settings </h2>
		<span class="sub_header"> Configure how this app would display your journals.</span>
										
		
		<p class="info"> Please select your application preference.</p>
		
		<div class="panel">
			<div class="<?=$panelHeader0?>" id="ph0">
				<input onclick="setShowJournalType0()" type="radio" name="viewType" class="radio_btn" <?=$viewType0?> value="0" id="viewType0"/>
				<label onclick="setShowJournalType0()" for="viewType0" ><strong> Update my journals automatically</strong></label>
			</div>
			<div class="panel_content">
				Checking this option will display the latest journal entries from the journal that you recently updated.
			</div>
		</div>
		<br />
		<div class="panel">
			<div class="<?=$panelHeader1?>" id="ph1">
				<input onclick="setShowJournalType1()" type="radio" name="viewType" class="radio_btn" <?=$viewType1?> value = "1" id="viewType1"/>
				<label onclick="setShowJournalType1()" for="viewType1"><strong> I want to customize how MyTravelJournals displays my journals </strong></label>
			</div>
			<div class="panel_content">
				<div>
					Below are your journals in GoAbroad Network. Please check the ones you want to be displayed on your profile.
					You can select a maximum of 5 journals to be displayed.
				</div>
				<ul class="setting_journal_list">

					<!-- start of loop -->
					<?php $i=0; foreach( $args['userJournals'] as $journal ) :
						$journalLink = osMytravelJournalsController::getLink(array_shift($journal->getEntries()));
					?>
						
						<li id="list_journal<?=$i?>" class="list_journal_name">

							<?php foreach($args['selectedJournal'] as $selectedjournal){
									if($selectedjournal->getID() == $journal->getID()){
										$checkedStatus = "checked='checked'"; break;
									}
									else { $checkedStatus = ""; }
							 } ?>
								
							<input type="checkbox" name="travelID[]" id="jcheckbox<?=$i?>" value="<?=$journal->getID()?>" class="check_box" <?=$checkedStatus?> <?=$disabledStatus?> />
							<a href="<?=$journalLink ?>" target="_blank"><?php echo $journal->getTitle(); ?></a>
						</li>

					<?php $i++; endforeach; ?>				            

					<input type='hidden' value='<?=$i?>' id='totalJournal'>
					<!-- end of loop -->								
				</ul>		
				<p id="errMsgHolder" class="error"> </p>
			</div>
		</div>																								
		<br/>
		<input type="button" name="btnSave" value="Save Settings" onclick="return myTravelJournalsController.changeSettings(<?=$i?>);" />

	<?php endif; ?>
<!-- //////////// end of all journals with checkbox  -->				

<? else:  ?>	
	<p class="info">
		You have not yet written any journals. Why not create one and start sharing your travels?
		Go to <a href="http://www.goabroad.net/register.php" target="_blank">http://www.goabroad.net</a> and create a journal.
	</p>
	
	<p>
		When you have finally created a journal come back here and complete your settings. Then that's it! Share your memorable travels with the world!
	</p>
	
<? endif; ?>			
