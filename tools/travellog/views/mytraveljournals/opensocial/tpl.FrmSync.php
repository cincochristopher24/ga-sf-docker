<?php if($appViewType == 'profile' || $appViewType == 'profile.left' || $appViewType == 'profile.right' || (!$args['viewerIsOwner'] && $appViewType == 'canvas') ) : ?>
	<p style="padding: 10px 0 5px 15px; font-weight: bold; font-size: 13px; color: #B22222"> No journals to be displayed yet.</p>
	<p style="padding: 0pt 0pt 10pt 15px; font-size: 11px; color: #B22222">Account is not yet synchonized to GoAbroad Network.</p>

	<?php if ($args['viewerIsOwner']): ?>  
		<p align="center" style="padding: 0pt 0pt 25pt 15px;"><a onclick="gotoCanvas()" style="cursor:pointer;font-size:12px;font-weight:bold;">Synchronize your account now!</a> </p>
    <?php else : ?>
		<p style="padding: 0pt 0pt 15pt 15px; font-size: 12px;font-weight:bold">
			<a onclick="gotoCanvas()" style="cursor:pointer">Add your own MyTravelJournals and start sharing your travels now!</a>
		</p>
	<?php endif; ?>

		
<?php elseif($appViewType == 'preview') : ?>	
	<p style ="padding:10px">
		Bring your <a href="http://www.goabroad.net" target="_blank">GoAbroad Network</a> travel journals to <a>Hi5</a>!
		<br />
		Post your journals, photos and let your friends and family know 
		about your experience! 
	</p>												

	<p style ="padding:10px">
		GoAbroad Network is a social networking website for travelers all over 
		the world. It is an online travel community where members can share 
		travel journals and photos with families and friends, while connecting
		with a growing network of travelers.													
	</p>	
	<div style ="background: #fff url(http://www.goabroad.net/opensocial/myTravelJournals/images/myspace_about_bg.jpg) no-repeat scroll -200px 0px;height: 298px;padding: 0 !important;">&nbsp;<div/>

<!--	<div class="feature">					
		<p class="info">Synchronize your GoAbroad and <?=ucwords($args['wsName'][0])?> accounts.</p>
		<p class="">
			Bring your <strong>GoAbroad Network</strong> travel journals to <strong><?=ucwords($args['wsName'][0])?></strong>! 
			<br /> 
			Post your journals, photos and let your friends and family know 
			about your experience!
		</p>
		<p class="">
			<strong>Not yet travel-blogging?</strong>
			<br />				
			Get a free traveler profile and cool travel blog on <a href="http://www.goabroad.net/register.php" target="_blank"> 
			www.GoAbroad.Net!</a>
		</p>	
	</div>
-->
	
<?php else : ?>

	<div id="frmSync">	
		<h1 class="header">Synchronize your Journals</h1>
		<span class="sub_header"> Update your journals with ease.</span>

		<?php if($args['sync']): ?>


			<p class="info"> You are currently using <?=$args['ownerName']?>. </p>

			<p style="padding-bottom: 10px;">
				This App is currently synchronized with <a href="http://www.goabroad.net/<?=urlencode($args['ownerName'])?>" target="_blank"><?=$args['ownerName']?>'s</a> GoAbroad Network account.<br/>

<!--				<strong>GoAbroad Network Account Email used by this App:</strong> <?=$args['dotNetEmail']?> -->
				If you don't have any GoAbroad Network account with an email "</strong><?php echo $args['dotNetEmail'] ?></strong>" 
				<a href='http://www.goabroad.net/feedback.php' target='_blank'>please send us an email.</a>
			</p>	

<!--			<p style="padding-bottom: 10px;"> If you wish to synchronize a different GoAbroad Network account, click <strong>Unsynchronize</strong> then sync again. </p>
			<input type="button" value="Unsynchronize" onclick="showLoading();myTravelJournalsController.unsynchronize()" />
-->
		<?php else: ?>
	
			<p class="first_p">
				If you have a GoAbroad Network account, you can synchronize your <?=ucwords($args['wsName'][0])?> Travel Journals with
				your GoAbroad Network travel Journals. Synchronizing means that both your <?=ucwords($args['wsName'][0]) ?> and GoAbroad
				Network profiles will show the same Travel Journals. 	
			</p>

			<p class="info" id="syncMsgs">
				To synchronize, type in your GoAbroad Network email and	password.
			</p>

		<?php endif; ?>
			
			<p>
				<label for="txtUsername"><strong>Email</strong>:</label> 
				<br />
				<input type="text" name="txtUsername" id="txtUsername" value="<?= $args['username']; ?>" />
				<br />
				<label for="txtPassword"><strong>Password</strong>:</label> 
				<br />
				<input type="password" name="txtPassword" id="txtPassword" onkeypress="return pressSync(event)" />
			</p>
	
			<p class="error" id="divErrorMsg">
				<?= $args['errorMsg']; ?>
			</p>

			<p> 
				<input type="button" value="Synchronize" onclick="return myTravelJournalsController.synchronize();" /> 
			</p>	
	
			<br />

		<?php if(!$args['sync']): ?>
			<p>
				Not yet a GoAbroad Network member?  <a href="http://www.goabroad.net/register.php" target="_blank">Sign up</a> for your <strong>FREE</strong> account now! 
				<br />
				Find out how GoAbroad Network can change the way you travel.
			</p>
		<?php endif; ?>

	</div>   

<?php endif; ?>
