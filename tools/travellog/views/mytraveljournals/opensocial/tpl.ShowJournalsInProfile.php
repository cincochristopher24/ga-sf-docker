<div id='wrapper' class="wrapper">
<?php if( is_array($args['journals']) || is_object($args['journals']) ): $i=0; ?>
		<ul id="journals_list">
	
		<?php foreach( $args['journals'] as $journal ):
	
			$entries = $journal->getEntries();
			$FriendlyURL = osMytravelJournalsController::getLink($entries[0]);
			$totalEntries = count($entries);		
			$buttonClassName = $totalEntries == 1 ? 'buttonDisabled' : ''; 
			$liClass = $i == count($args['journals'])-1 ? 'journal no_bborder' : 'journal with_bborder';  ?>
					
			<li class="<?=$liClass?>">
				<input type ="hidden" value="<?=count($entries)?>" id="totalEntries<?=$i?>"/>
				<div class="journal_box">
										
					<h1>
						<?php if( ($appviewType == 'profile' || $appviewType == 'preview') && $args['domain'] == 'hi5.com') : ?>
							<div style="font-size:16px;color:0055aa;font-weight:bold"><?=$journal->getTitle(); ?></div>
						<?php else : ?>
							<a href="<?=$FriendlyURL?>" class="jtitle" alt="<?= $journal->getTitle(); ?>" id="journal<?=$i?>" target="_blank"><?=$journal->getTitle(); ?></a>
						<?php endif; ?>
					</h1>
	<!--					<div class="clear"> </div> -->
					<div class="journal_details"> 
						This journal has <?php echo $totalEntries; echo $totalEntries > 1 ? ' entries' : ' entry'; ?>, Last Entry Created on <?=$dateTimeTool->friendlyFormat($journal->getLastUpdated())?>
					</div>
	
					<div class="journal_description"> 
						<?=$journal->getDescription(); ?>
					</div>										
	
					<?php $ii = 0; foreach( $entries as $entry ):
	
						$primaryPhoto = $entry->getPrimaryPhoto()->getPhotoLink('standard');
						$FriendlyURL = osMytravelJournalsController::getLink($entry);
						list($width, $height) = osMytravelJournalsController::getNewImageSize($primaryPhoto);	?>

						<div class="entry_box">
		                	<div class="jcontent" id="divContent<?=$ii?>" style="display:block">
	
								<!-- Begin Entry Thumb -->
								<div class="photobox">
									<!-- images should be resized to (WxH) 90px X 60px for landscapes, 60px X 90px for portraits -->
									<?php if( ($appviewType == 'profile' || $appviewType == 'preview') && $args['domain'] == 'hi5.com') : ?>
										<img id="primaryPhoto<?=$i?>" class="jphoto" src="<?=$primaryPhoto?>" width="<?=$width?>" height="<?=$height?>"/>	
									<?php else : ?>
										<a href="<?=$FriendlyURL ?>" target="_blank" id="photoBox<?=$ii?>" > <img id="primaryPhoto<?=$i?>" class="jphoto" src="<?=$primaryPhoto?>" width="<?=$width?>" height="<?=$height?>"/> </a>
									<?php endif; ?>										
								</div>
								<!-- End Entry Thumb -->
	
								<!-- Begin Entry Title -->								
		                		<div class="entry_title">
									<?php if( ($appviewType == 'profile' || $appviewType == 'preview') && $args['domain'] == 'hi5.com') : ?>
										<div style="font-size:12px;color:#3B5998;"><?=$entry->getTitle(); ?></div>
									<?php else : ?>
		                    			<a href="<?=$FriendlyURL ?>" target="_blank" id="title<?=$i?>"> <?=$entry->getTitle(); ?></a>
									<?php endif; ?>
		                    	</div>
		                    	<!-- End Entry Title -->
	
								<!-- Begin Entry Details-->
								<div class="entry_detail" id="details<?=$ii?>">
									<div><?= $dateTimeTool->friendlyFormat($entry->getTrip()->getArrival()); ?></div>
									<div> <?= implode(', ', array_unique(array($entry->getTrip()->getLocation()->getName(),$entry->getTrip()->getLocation()->getCountry()->getName()))); ?></div>
									<div> <?php echo $entry->countPhotos(); echo $entry->countPhotos() > 1 ? ' Photos' : ' Photo' ?></div>
								</div>					
								<!-- End Entry Details -->																					          							
		                	</div>
		                	<!-- end jcontent--> 

	                	</div>					
	
						<?php if($appviewType == 'profile' || $appviewType == 'profile.left' || $appviewType == 'profile.right' || $appviewType == 'preview') break; ?>
						
					<? $ii++; endforeach; ?>	    
	
					<? if( $tmpAdminGroup = $journal->getAdvisorGroup() ): ?>
						
						<div class="advisorbox">			
		
							<img alt="Advisor Logo" src="<?= $tmpAdminGroup->getGroupPhoto()->getPhotoLink('thumbnail'); ?>" />

							<?php if( ($appviewType == 'profile' || $appviewType == 'preview') && $args['domain'] == 'hi5.com') : ?>
								<?=$tmpAdminGroup->getName(); ?>
								<span class="advDetails">Advisor Group Journal</span>
							<?php else : ?>
								<a href="<?=$tmpAdminGroup->getServerName(); ?>" target="_blank">
									<?=$tmpAdminGroup->getName(); ?>
									<span class="advDetails">Advisor Group Journal</span>
								</a>
							<?php endif; ?>										
	


	
					       <div class="clear"> </div>			
					       
						</div>
						
					<? endif; ?>         			
	       		   		
	   		   		<div class="clear"> </div>       		   		
	
		       </div>    <!-- end div journal box -->		       
	
			</li> <!-- end li journal -->
					
		<? 	$i++; endforeach; ?>
		
		</ul>
		<!-- end journals_list -->
</div>

<? else: ?>

	<?php if ($appviewType == 'preview') : $container = explode('.',$args['domain']); ?>
		<p style ="padding:10px">
			Bring your <a href="http://www.goabroad.net" target="_blank">GoAbroad Network</a> travel journals to <a><?php echo $container[0] ?></a>!
			<br />
			Post your journals, photos and let your friends and family know 
			about your experience! 
		</p>												

		<p style ="padding:10px">
			GoAbroad Network is a social networking website for travelers all over 
			the world. It is an online travel community where members can share 
			travel journals and photos with families and friends, while connecting
			with a growing network of travelers.													
		</p>	
		<div style ="background: #fff url(http://www.goabroad.net/opensocial/myTravelJournals/images/myspace_about_bg.jpg) no-repeat scroll -200px 0px;height: 298px;padding: 0 !important;">&nbsp;<div/>

	<?php else : ?>
		<?php if ($args['viewerIsOwner']): ?>
			<p style="padding: 10px 0 5px 15px; font-weight: bold; font-size: 13px; color: #B22222">You have not created any journal yet.</p>
<!--			<p style="padding: 0pt 0pt 15pt 15px; font-size: 11px; color: #B22222">Start sharing your travels! Go to <b>http://www.goabroad.net</b> and create a journal now!</a></p> -->

			<?php if($appviewType == 'canvas') : ?>
				<p style="padding: 0pt 0pt 15pt 15px; font-size: 11px; color: #B22222">Start sharing your travels! Go to <b><a href="http://www.goabroad.net" target="_blank">http://www.goabroad.net</a></b> and create a journal now!</a></p>
			<?php else : ?>			
				<p align="left;" style="padding: 0pt 0pt 25pt 15px;"><a onclick="gotoCanvas()" style="cursor:pointer;font-size:12px;font-weight:bold;">Manage your MyTravelJournals now!</a> </p>
			<?php endif; ?>

		<? else : ?>
			<p style="padding: 10px 0 5px 15px; font-weight: bold; font-size: 13px; color: #B22222">
				<?=$args['name']; ?> has not created any journal yet.
			</p>
			<p style="padding: 0pt 0pt 15pt 15px; font-size: 12px;font-weight:bold">
				<a onclick="gotoCanvas()" style="cursor:pointer">Add your own MyTravelJournals and start sharing your travels now!</a>
			</p>

		<? endif; ?>

	<?php endif; ?> 

<? endif; ?>