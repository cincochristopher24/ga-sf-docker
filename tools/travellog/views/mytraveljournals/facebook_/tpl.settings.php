<script>
function checkJournals(obj) {
	var x = 0;                                                    
	for(var i=0; i<document.getElementById('totalJournal').getValue(); i++) {
		if(document.getElementById('jcheckbox'+i).getChecked() == true) {
			x++;				
		}		
	}
	if(x == 0 && document.getElementById('viewType1').getChecked() == true){
		document.getElementById('errMsgHolder').setTextValue('You have to select atleast one(1) of your journals.');
		return false;		

	}
	else if ( x > 5 && document.getElementById('viewType1').getChecked() == true) {
		document.getElementById('errMsgHolder').setTextValue('You just have maximum of 5 journals to be displayed.');	
		return false;		
	}			 
}

function setShowJournalType0(){
	document.getElementById('ph0').setClassName('panel_header active');
	document.getElementById('ph1').setClassName('panel_header');
	for(var i=0; i<document.getElementById('totalJournal').getValue(); i++) {
		document.getElementById('jcheckbox'+i).setDisabled(true);
	}	

}

function setShowJournalType1(){
	document.getElementById('ph0').setClassName('panel_header');
	document.getElementById('ph1').setClassName('panel_header active');
	for(var i=0; i<document.getElementById('totalJournal').getValue(); i++) {
		document.getElementById('jcheckbox'+i).setDisabled(false);
	}	
}

</script>

<div id="inner_wrapper" >
	
		<?php if(count($args['userJournals'])) {?>	
			<div class="frm_settings">	
				<form action="settings.php?action=changeSettings" method="post">				
					<h2 class="header"> Settings </h2>
					<span class="sub_header"> Configure how this app would display your journals.</span>
													
					
					<p class="info"> Please select your application preference.</p>
					
					<div class="panel">
						<div class="<?=$panelHeader0?>" id="ph0">
							<input onclick="setShowJournalType0()" type="radio" name="viewType" class="radio_btn" <?=$viewType0?> value = "0" id="viewType0"/>
							<label onclick="setShowJournalType0()" for="viewType0" ><strong> Update my journals automatically</strong></label>
						</div>
						<div class="panel_content">
							Checking this option will display the latest journal entries from the journal that you recently updated.
						</div>
					</div>
					<br />
					<div class="panel">
						<div class="<?=$panelHeader1?>" id="ph1">
							<input onclick="setShowJournalType1()" type="radio" name="viewType" class="radio_btn" <?=$viewType1?> value = "1" id="viewType1"/>
							<label onclick="setShowJournalType1()" for="viewType1"><strong> I want to customize how MyTravelJournals displays my journals </strong></label>
						</div>
						<div class="panel_content">
							<div>
								Below are your journals in GoAbroad Network. Please check the ones you want to be displayed on your profile.
								You can select a maximum of 5 journals to be displayed.
							</div>
							<ul class="setting_journal_list">

<!-- start of loop -->			<?php 
								$i = 0;
								foreach( $args['userJournals'] as $journal ) :
									$journalLink = fbMytravelJournalsController::getLink(array_shift($journal->getEntries())); ?>

									<li id="list_journal<?=$i?>" class="list_journal_name">
										<?php
											foreach($args['selectedJournal'] as $selectedjournal){
												if($selectedjournal->getID() == $journal->getID()){
													$checkedStatus = "checked='checked'"; break;
												}
												else {
													$checkedStatus = "";
												}
											}
										?>
										<input type="checkbox" name="travelID[]" id="jcheckbox<?=$i?>" value="<?=$journal->getID()?>" class="check_box" <?=$checkedStatus?> <?=$disabledStatus?> >
										<a href="<?=$journalLink?>" target="_blank"><?php echo $journal->getTitle(); ?></a>
<!--											<div class="setting_journal_lastentry"> Last Entry on Mar 2, 2008</div>  -->
									</li>
<!-- end of loop -->			<?php $i++; endforeach; ?>	

								<input type='hidden' value='<?=$args['id']?>' name='userID' >
								<input type='hidden' value='<?=$i?>' id='totalJournal' name='myI'>
								<input type='hidden' value='<?=$args['numberOfTravelJournals']?>' id='selectedJournal' name='numJournals'>

							</ul>		
							<div id="errMsgHolder" style="padding:10px;color:#dd2200;font-weight:bold"></div>
						</div>
					</div>																								
					<br />				
					<input id='save' class='inputsubmit' type='submit' name='submit' value='Save Settings' onclick='return checkJournals(this)'>
				</form>								    
			</div>	
			
		<?php } else {?>
			<div class="feature">
				<p class="info">You have not yet written any journals. </p>
				
				<p>
					You've successfuly synchronized your GoAbroad Network account here in Facebook, but it seems you haven't written
					any journals yet. Why not create one and start sharing your travels?				
				</p>
				
				<p>
					<a href='http://www.goabroad.net/login.php' target='_blank'> Visit</a> your GoAbroad Network account  and create a journal now!
				</p>
			</div>
		<?php }?>
	
	<?php require_once('footer.php');?>	

</div>