<div id="inner_wrapper">

<?php if(0!=$args['friendsDiff']) : ?>

	<fb:fbml>
		<fb:request-form action="<?=$args['isSync'] ? 'index.php' : 'fbSync.php' ?>" method="POST" invite="true" type="Journal" 
		content="You have been invited by <?=htmlentities('<fb:name uid="'.$args['fbUserID'].'" firstnameonly="true" />') ?> to add the My Travel Journal Application! Add this application if you want to share your travel journals with <?= htmlentities('<fb:name uid="'.$fbUserID.'" firstnameonly="true" />') ?>. <?=htmlentities("<fb:req-choice url=\"http://apps.facebook.com/mytraveljournals/\" label=\"Add Application\" />") ?>">
		<div class="form_invites"			
			<h2 class="header"> Invite friends </h2>
			<span class="sub_header"> Sharing makes it more meaningful.</span>
			<p class="first_p">
				Inviting your friends is easy, just click on their names below and click the <strong> Send Journal Invitation</strong> button.
				You can also filter your friends by network making it easy for you to find, plus you can send a custom message in your invitation.				
			</p>								
		</div>				
		<fb:multi-friend-selector showborder="false" actiontext="&nbsp;" exclude_ids="<?= $args['excludedUIDList'] ?>">
		</fb:request-form showborder="false">
	</fb:fbml>

<?php else : ?>

	<div class="form_invites"			
		<h2 class="header"> Invite friends </h2>
		<span class="sub_header"> Sharing makes it more meaningful.</span>		
		<p class="info"> Either all of your friends have this application already or you don't have any friends in facebook yet.</p>				
		<p class="first_p">
			If you don't have any friends in facebook yet, why not invite them here and share the fun? 
			Got their emails? Then here's the link to <a href='http://www.facebook.com/invite.php?ref=sb'>invite your friends</a>.
		</p>
	</div>	
	
<?php endif; require_once('footer.php'); ?>

</div>