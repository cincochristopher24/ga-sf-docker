<?php
/**
 * <b>Program</b> template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<?
				if ( count($errors) ):
					foreach($errors as $error):
						echo $error.'<br />';	
					endforeach;
				endif; 
				echo $form_program_management; 
			?>
		</td>
	</tr>
</table>
