<?php
//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
Template::setMainTemplateVar('layoutID', 'page_default');
Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
Template::includeDependentJs('js/interactive.form.js');
Template::includeDependentJs('js/tools/common.js');
Template::includeDependentJs('js/utils/json.js');
//Template::includeDependentJs('captcha/captcha.js');

Template::includeDependentJs('gaCaptcha/gaCaptcha.js');

$style = <<<STYLE
<style type="text/css">
/*<!-- <[!CDATA[ -->*/
#main div.help {
	margin: 10px 30px 20px 410px;
	font-size: 14px;
	line-height: 1.8em;
	padding: 0 0 0 25px;
	max-width: 35em;
    background: url(/images/dotted.gif) left top repeat-y;	
}
/*<!--]]> -->*/
</style>
STYLE;
Template::includeDependent($style);
?>
<script type="text/javascript">
	var captcha = new GaCaptcha({
		'imageTagID' 	: 'captchaImg',
		'handler'		: '<?= GaCaptcha::getImageSource() ?>'
	});
</script>
	<div id="content_wrapper" class="feedback">
		<div id="page_form" class="section">
				<h2>Send Us Your Feedback</h2>
				<div class="help_text">
					<?php if(	isset($GLOBALS['CONFIG']) ):?>
						<?= HtmlHelpers::Textile(str_replace('GoAbroad Network', $GLOBALS['CONFIG']->getSiteName(), $helpText->getHelpText('FEEDBACK_HELP_TEXT'))); ?>
					<?php else:?>
						<?= HtmlHelpers::Textile($helpText->getHelpText('FEEDBACK_HELP_TEXT')); ?>
					<?php endif;?>
				</div>
				<?php if ('showForm' == $method): ?>
				<form name="frmFeedback" method="post" action="feedback.php" class="interactive_form">
					<ul class="form">
						<li>
							<?= $msgComm ?>
							<label>Enter your feedback: </label>
							<textarea id="msg" name="msg"><?= $msg ?></textarea>
						</li>
						<li>
							<?= $emailComm ?>
							<label>Email Address (optional):</label>
							<input type="text" id="email" name="email" value="<?= $email ?>" class="text" />
							<p class="supplement">Please include your email address if you wish to receive a response.
							Your personal information will remain strictly confidential and never
							sold, rented, leased nor shared with any person nor company.</p>
						</li>
						<li>
							<?= $captchaComm ?>
							<label>Enter the code shown: </label>
							<input type="text" id="securityCode" name="securityCode" value="" class="text" />	
								<div id="captcha">							
									<img  id="captchaImg" src="<?= GaCaptcha::getImageSource() ?>" alt="Captcha" border="1" /><br />
									<a href="/feedback.php?dt=<?=time()?>" onclick="captcha.load();return false;">Refresh Image</a>
									<input type="hidden" id="encSecCode" name="encSecCode" value="<?=$secCode?>" />
								</div>													
						</li>
					
						<li class="actions">							
							<input type="submit" class="button submit" name="send" value="Send" />
						</li>
					</ul>
				</form>				
				<?php else: ?>
					<div>
						<?php echo isset($GLOBALS['CONFIG']) ? str_replace('GoAbroad.net', $GLOBALS['CONFIG']->getSiteName(), $comm) : $comm; ?>
						<?// echo $comm ?>
					</div>
				<?php endif; ?>
			</div>
	</div>