<?php
/*
 * Created on 09 22, 2006
 * @author Kerwin Gordo
 * Purpose: subtemplate for groups of a traveler (used in passport)
 */
?>

<?
	$accountType = '';								// GA.net account types 'REGULAR','UPGRADED','ADVISOR'
	$hasAdvisorGroup = false;
?>
<div class="section" id="groups">
	<h2><span>
		Groups
    	<?if (0 < $count) : ?>
    		(<?=$count ?>)		
	    <? endif; ?>
	</span></h2>
	<div class="content">
		<div class="header_actions">	 
			<a href="/group.php?mode=add">Create Club</a>|<a href="/group.php">Join Group/Club</a>			
		</div>
	
	
	<!--<div class="content">-->
		<? if (isset($groups) && $groups != null) : ?>
			<ul id="groups_container" <?php if ($count > 5): ?> class="elements scroll" <?php else: ?> class="elements" <?php endif; ?>>
			<? foreach($groups as $group) : ?>
				<li>
					
					<a title="View Group Profile" class="thumb" href="/group.php?gID=<?=$group->getGroupID()?>"><img class="pic" alt="Group Emblem" src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail')?>" height="37" width="37" /></a>
					
					<div class="details">						
						
						<? if (strlen($group->getName()) > 30): ?>
							<a href="/group.php?gID=<?=$group->getGroupID()?>" title="<?= $group->getName()?>">  <?= substr($group->getName(),0, 30) ?>...</a>
						<? else: ?>	
							<a href="/group.php?gID=<?=$group->getGroupID()?>">  <?= $group->getName()?> </a>
						<?endif; ?>
						
						<?if ($group->getAdministrator()->getTravelerID() == $traveler->getTravelerID()):?>
							<span  class="meta">(&nbsp;<strong>Admin</strong>&nbsp;)</span>
						<?elseif (get_class($group) == 'AdminGroup' && $group->getParentID() > 0 ):?>	
							<span  class="meta">(&nbsp;<strong>Moderator</strong>&nbsp;)</span>							
						<?endif;?>																		
												
					</div>
					<div class="clear"></div>									
				</li>
			<? endforeach; ?>
			</ul>
			<!--
			<?if ($logged):?>
			    <div id="groups_actions">
			    	<a id="join_group" href="/group.php" class="button" id="link_join_group">Join a Group / Club</a></li>
			    	<a id="create_club" class="button" href="/group.php?mode=add">Create a Club</a>	    
			    </div>
		    <?endif;?>
			-->
	    <? else : ?>
	    	<p class="help_text">
	    		 <?=isset($helpTextprofile)?$traveler->getUsername()." ". $helpText:$helpText?>		    
	    	</p>    	
	    <? endif; ?>
	    
	
   <!--</div>-->
	</div>
	<div class="foot"></div>
	
</div>
