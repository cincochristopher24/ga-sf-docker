<?php
/**
 * <b>View Confirmation</b> template.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
 
 Template::setMainTemplateVar('layoutID', 'main_pages');
?>


<div id="content_wrapper">
	<!--<div id="div_friend_one"></div>-->
	<div class="section">
		<h1 class="big_header"><span>Friend Request</span></h1>
		<div class="content">
			<div class="frConfrmMsg">
				<?
					$username = $friend;
					if( substr($friend, -1) == 's' ) $friend .= "'";
					else $friend .= "'s";

					if( $isFriend )
						echo '<h3>' . $username . ' is already on your friends list. <a href="/'.$username.'">Go back to '. $friend .' profile.</h3><br />';
					else	
						echo '<h3>Your friend request has been sent. <a href="/'.$username.'">View '. $friend .' profile.</h3><br />';
				?>
				<a href="/passport.php">Return to My Passport</a>
			</div>
		</div>
		<div class="foot"></div>
	</div>
	<!--<div id="div_friend_two"></div>-->
</div>


<!--
<div id="div_friend_one"></div>
	<div class="area" id="main">
	<div class="section" id="confirmation">
			<h1>Friend Request</h1>

			<div class="content">
			<div class="frConfrmMsg">
	<?
		$username = $friend;
		if( substr($friend, -1) == 's' ) $friend .= "'";
		else $friend .= "'s";
		
		if( $isFriend )
			echo '<h3>' . $username . ' is already on your friends list. <a href="/'.$username.'">Go back to '. $friend .' profile.</h3><br />';
		else	
			echo '<h3>Your friend request has been sent. <a href="/'.$username.'">View '. $friend .' profile.</h3><br />';
	?>
	<a href="/passport.php">Return to My Passport</a>
</div>
	</div>
</div>	
</div>


<div id="div_friend_two"></div>-->