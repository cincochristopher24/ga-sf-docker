<?
$file_factory = FileFactory::getInstance(); 
$d = $file_factory->getClass('GaDateTime');
?>
<h1 class="big_header"><span>Event Details</span></h1>
<div class="content">
	<div id="view">
		<?if( $props['obj_event']->getDisplayPublic() || ( $props['is_login'] && $props['is_owner'] ) ):?>
			<div class="date meta" id="event_details_date_display"><?= $d->set($props['obj_event']->getTheDate())->htmlDateFormat() ?></div>
			<h4><?=$props['obj_event']->getTitle()?></h4>
			<div class="event_details"><?= HtmlHelpers::Textile($props['obj_event']->getDescription()); ?></div>
			<div class="clear"></div>
			<?if ( $props['is_login'] && $props['is_owner'] ):?>
				<ul class="actions">
					<li><a href="javascript:void(0)" onclick="jQuery().addForm('action=edit&amp;eID=<?=$props['obj_event']->getActivityID()?>&amp;context=0')" class="button">Edit</a></li><li><a href="javascript:void(0)" onclick="jQuery().deleteEvent('action=delete&amp;eID=<?=$props['obj_event']->getActivityID()?>&amp;context=0')" class="button">Delete</a></li>
				</ul>
			<? endif; ?> 
		<?else:?>
			<p class="event">Sorry, you are not allowed to view this event.</p>     
		<? endif; ?>
	</div> 
	<div class="clear"></div>
</div>
