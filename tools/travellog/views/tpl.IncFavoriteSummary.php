<?php
/*
 * Created on Aug 10, 2006
 */
?>
<div id="scrapbook" class="section">
	<h2>My Scrapbook</h2>
	<div class="content">
	<? if (isset($favorites)) : ?>
				<ul id="scrapbook_items">
					<?php	foreach ($favorites as $favorite) : ?>
							<li>
								<?= $favorite->getGenID()  ?>
								<a href="<?= $favorite->getURL()?>">
									<?= $favorite->getTitle() ?>
								</a>
							</li>
					<?php endforeach; ?>
				</ul>
	<?  else :?>
		<p class="help_text">			
			   <?= $helpText ?>			
		</p>
	<?  endif; ?>
	
	<form name="form1" >
	  <select name="program" onchange="openNewWindow()" id="program" >
	  	<option value="http://www.goabroad.com">--- Search For A Program ---</option>
	    <option value="http://www.studyabroaddirectory.com">Study Abroad</option>	    
	    <option value="http://www.internabroad.com">Intern Abroad</option>
	    <option value="http://www.languageschoolsguide.com">Language Schools</option>
	    <option value="http://www.volunteerabroad.com">Volunteer Abroad</option>
	    <option value="http://www.jobsabroad.com">Jobs Abroad</option>
	    <option value="http://www.teachabroad.com">Teach Abroad</option>
	   
	  </select>
    </form> 
    <script type="text/javascript">
    	function openNewWindow(){
  			
  			
  			var listBox = document.getElementById('program');
  			var selOption = listBox.options[listBox.selectedIndex];
  			
  			window.open(selOption.value,'_blank');
    	}
	</script>
  
	</div>
</div>