<?php
require_once("travellog/views/Class.AbstractView.php");
class EntryTipsView extends AbstractView{
	function render(){
		return ( $this->contents['travellogID'] )? $this->obj_template->fetch('tpl.EntryTips.php'): NULL;
	}	
}
?>
