<?php
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'Groups');
	Template::includeDependentJs('/js/groupProfile.js');
	
	if (isset($subNavigation))
		$subNavigation->show();
?>

<div id="content_wrapper">

	<div id="wide_column">
	<div id="subgroup_facilitator" class="section">
			<h2><span>ASSIGN</span></h2>
			<div class="content">
					<form name="frmFacilitator" action="facilitator.php" method="post" class="interactive_form">
					
							<? if ($type == 'Facilitator' || $type== 'Staff') { ?>
								<div>
									<label for="groupStaff">Staff for Sub Group:</label>
									<select multiple name="selGroupTravelersForStaff" size="5" style="width:234px">
										<?php 
											$allMembersKeyList = '';
											foreach($allMembers as $key => $value){ 
												if (!array_key_exists($key,$selGroupStaff)) {
												?>
												<option value="<?=$key?>"><?=$value?>
										<? 	$allMembersKeyList = $allMembersKeyList . ',' . $key  ; 	
											} }
										?>
									</select>
									<input type="hidden" name="allMembersKeyList" id="allMembersKeyList" 
											value="<?=$allMembersKeyList?>">
								
									<input style="margin:30px 0;" type="button" name="moveleftstaff" id="moveleftstaff"  
										onClick="move(this, this.form.selGroupStaff,this.form.selGroupTravelersForStaff);  " value="<<">
									<input style="margin:30px 0;" type="button" name="moverightstaff" id="moverightstaff"  
										onClick="move(this, this.form.selGroupTravelersForStaff,this.form.selGroupStaff) ;" value=">>">
																		
									<select multiple name="selGroupStaff" size="5" style="width:234px">
										<?php
											$grpStaffKeyList = '';
											foreach($selGroupStaff as $key => $value){ ?>
												<option value="<?=$key?>"><?=$value?>
										<?  $grpStaffKeyList = $grpStaffKeyList . ',' . $key  ; 	
										    }
										?>
									</select>
									<input type="hidden" name="grpStaffKeyList" id="grpStaffKeyList" 
										value="<?=isset($grpStaffKeyList)? $grpStaffKeyList : ''?>">	
								</div>
							<? } ?>
							
							<div class="actions">
								<input type="hidden" name="gID" value="<?=$gID ?>" />
								<input type="hidden" name="type" value="<?=$type ?>" />
								<input type="hidden" name="mode" value="save" />
								<input type="submit" name="btnSubmit" value="Save" class="submit" />
							</div>
							
						</ul>
					</form>
				</div>
				<div class="foot"></div>				
	</div>
	</div><div class="clear"/></div>
</div>