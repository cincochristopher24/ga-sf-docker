<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />				
		<title>News Management</title>
		<link rel="stylesheet" type="text/css" href="/travellog/css/adhoc.css" media="screen" />		
	</head>
	<body>
		<div id="wrapper1">
			<?php
				if($method=="view"):									
			?>
				<div>
					<table cellpadding="7px">
						<tr>
							<td valign="top">
								<!-- panel -->
								<div>									
									<table cellpadding="3px">
										<tr>
											<td align="center">
												<a href="/newsManagement.php?method=compose&groupID=<?= $groupID ?>&hlEntryID=<?= $hlEntryID?>">Compose News</a>
											</td>
										</tr>
										<tr>
											<td>												
												<table border="1" cellpadding="4px" style="width:200px">													
													<?php 														
														foreach($news as $eachNews){ 												
													?>
														<tr <?php if($eachNews->getNewsID() == $hlEntryID){ echo "bgcolor='yellow'"; $hlEntry = $eachNews;} ?>>
															<td>																
																<?= date('M d, Y',strtotime($eachNews->getTheDate())) ?><br />
																<a href="/newsManagement.php?groupID=<?= $groupID ?>&hlEntryID=<?=$eachNews->getNewsID()?>"><?= $eachNews->getTitle() ?></a><br />																 																
															</td>
														</tr>
													<?php 													
														} 
													?>
												</table>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<td valign="top">
								<!-- info -->
								<table border="0" cellpadding="1px" style="width:400px">
									<?php if (isset($hlEntry)):  ?>
										<tr>
											<td align="right">
												<?= $hlEntry->getTheDate() ?>
											</td>
										</tr>
										<tr>
											<td>
												<h3><?= $hlEntry->getTitle() ?></h3>
												<a href="/newsManagement.php?newsID=<?= $hlEntry->getNewsID() ?>&groupID=<?= $groupID ?>&method=edit&hlEntryID=<?= $hlEntryID?>">Edit</a> | 
												<a href="/newsManagement.php?newsID=<?= $hlEntry->getNewsID() ?>&groupID=<?= $groupID ?>&method=delete&hlEntryID=<?= $hlEntryID?>">Delete</a><br /><br />
												<strong>Details: </strong><br />
												<?= $hlEntry->getContent() ?>
											</td>
										</tr>
									<?php else: ?>
										<tr>
											<td>No bulletins.</td>
										</tr>
									<?php endif; ?>
								</table>
							</td>
						</tr>
					</table>
				</div>
			<?php 				
				elseif($method=="edit" || $method=="compose"):
			?>	
				<form name="frmNews" method="post" action="newsManagement.php?groupID=<?= $groupID?>&method=save">
					<table>						
						<tr>
							<td valign="top">Title:</td>
							<td>
								<?php if(isset($errCodes) && in_array(1,$errCodes)): ?>
									<font color="red">Please enter a title for the news.</font><br />
								<?php endif; ?>
								<input name="txtTitle" value="<?=$news->getTitle() ?>" type="text" style="width:350px">
							</td>
						</tr>
						<tr>
							<td valign="top">Details:</td>
							<td>
								<?php if(isset($errCodes) && in_array(2,$errCodes)): ?>
									<font color="red">Please enter the details for the news.</font><br />
								<?php endif; ?>
								<textarea name="txaDetails" cols="50" rows="10"><?=$news->getContent() ?></textarea><br /><br />
								<input type="checkbox" name="chkDisplayPublic[]" <?= ($news->getDisplayPublic())?"checked":"" ?> >Display in Public 
							</td>
						</tr>						
						<tr>
							<td colspan="2" align="right">
								<input type="submit" name="btnSubmit" value="Cancel">&nbsp;&nbsp;
								<input type="submit" name="btnSubmit" value="Submit">								
							</td>
						</tr>
					</table>					
					<input type="hidden" name="hlEntryID" value="<?=$hlEntryID?>">
					<input type="hidden" name="hidNewsID" value="<?=$news->getNewsID()?>">
				</form>
			<?php  
				endif;
			?>
		</div>					
	</body>
</html>