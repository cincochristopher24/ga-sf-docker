<?php echo $profile->render(); ?>
<?php echo $subNavigation->show(); ?>

<div class="layout_2" id="content_wrapper">
	<div id="wide_column">
		<div class="section">
			<h2><span>Change Account Settings</span></h2>
			<div class="content">
				<form name="passform" id="passform" action="/account-settings.php" method="post">
					<p class="help_text">
						You can change your username and other log-in details, like email and password, for your <?php if(is_null($config)): ?>GoAbroad<?php else: ?><?php echo $config->getSiteName();?><?php endif; ?> Network account here. If you want to change other settings, such as your full name and birthday, click on <a href="/edittravelerprofile.php">Edit Profile</a>. You may also modify privacy settings by clicking on <a href="/privacy.php">Privacy Preferences</a>.	
					</p>
					<?php if ( isset($success_message) && "" != $success_message ): ?>
						<p class="confirmation"><?php echo $success_message;?></p>
					<?php endif; ?>
				
					<?php if ( isset($errors) && count($errors) ): ?>
						<div class='error'>
							<ul>
							<?php foreach($errors as $error):?>
								<li><?php echo $error;?></li>
							<?php endforeach;?>
							</ul>
						</div>
					<?php endif; ?>
				
					<ul class="form">
						<li>
							<label class="inpt_box"> <span class="feature_helper">Username:</span> <span class="required feature_helper">*</span></label>
							<br />
							<input type="text" id="username" name="username" value="<?php echo $traveler->getUsername();?>" class="text" maxlength="25"/>
							<input type="hidden" id="hdnusername" name="hdnusername" value="<?php echo $traveler->getUsername();?>"/>	
							<br />
							<span class="supplement">Please use only letters, numbers or underscore. Username must be one word only.</span>	
						</li>									
			  		
						<hr style="clear: both; border: 1px solid #dfdfdf;"/>
					
						<li>
							<label for="inpt_box">Email:</label>  <span id = "currentemail"><?php echo $traveler->getEmail();?></span>
						</li>
						<li>
							<label for="inpt_box">New Email:</label>
							<input type="text" id="newemail" name="newemail" value="" class="text" />
						</li>
						<li>
							<label for="inpt_box">Retype New Email:</label>
							<input type="text" id="newemail2" name="newemail2" value="" class="text" />
						</li>							
				
						<hr style="clear: both; border: 1px solid #dfdfdf;"/>
				
						<li>
							<label class="inpt_box">Current password:</label>
							<input type="password" name="currentPassword" id = "currentPassword" class="text" />			
						</li>
				
						<li>
							<label class="inpt_box">New password:</label>
							<input type="password" name="newPassword" id="newPassword" class="text" />
							<br />
							<span class="supplement">Passwords must be 6 characters or more.</span>								
						</li>
			
						<li>
							<label class="inpt_box">Retype new password:</label>
							<input type="password" name="newPassword2" id="newPassword2" class="text" />
						</li>	
			
						<hr style="clear: both; border: 1px solid #dfdfdf;"/>
						
						<li class="actions" id="pop_button">
							<input type="hidden" id="action" name="action" value="update" />
							<input type="hidden" id="hidHasref" name="hidHasref" value="" />
							<input type="submit" id = "btnSubmit" name="submit" value="Update" class="submit button_v3" />
						</li>
					</ul>
				</form>
			</div>
		</div>	
	</div>
	
	<div id="narrow_column">	
		<div class="section">
			<h2>
				<span>Link Profile</span>
				<!--span class="header_actions">
					<a href="" target="_blank"></a> 
				</span-->				
			</h2>
			<div class="section_small_details">
				You may now use your Twitter, Google or Facebook account to log in to your <?php if(is_null($config)): ?>GoAbroad<?php else: ?><?php echo $config->getSiteName();?><?php endif; ?> Network profile. Simply click on any of the buttons below to begin the process of linking your accounts.
			</div>	
			<div class="content">
				<h3></h3>
				<ul>
					<li>
						<h4>
							Twitter
							<?php if($twitter_linked): ?>
								<button id="btnLinkTwitter" name="btnLink" type="button" class="submit button_v3">UNLINK</button>
							<?php else: ?>
								<button id="btnLinkTwitter" name="btnLink" type="button" class="submit button_v3" value="/splauncher.php?action=link&context=twitter">LINK</button>
							<?php endif;?>
							<span id="twitter_request_status" style="visibility: hidden;">
								<img src="/images/loading_small.gif" /> Requesting...
							</span>
						</h4>
					</li>
					<li>
						<h4>
							Google
							<?php if($google_linked): ?>
								<button id="btnLinkGoogle" name="btnLink" type="button" class="submit button_v3">UNLINK</button>
							<?php else: ?>
								<button id="btnLinkGoogle" name="btnLink" type="button" class="submit button_v3" value="/splauncher.php?action=link&context=google">LINK</button>
							<?php endif;?>
							<span id="google_request_status" style="visibility: hidden;">
								<img src="/images/loading_small.gif" /> Requesting...
							</span>
						</h4>
					</li>
					<li>
						<h4>
							Facebook
							<?php if($facebook_linked): ?>
								<button id="btnLinkFacebook" name="btnLink" type="button" class="submit button_v3">UNLINK</button>
							<?php else: ?>
								<button id="btnLinkFacebook" name="btnLink" type="button" class="submit button_v3" value="/splauncher.php?action=link&context=facebook">LINK</button>
							<?php endif;?>
							<span id="facebook_request_status" style="visibility: hidden;">
								<img src="/images/loading_small.gif" /> Requesting...
							</span>
						</h4>
					</li>
				</ul>
			</div><!-- end content -->			
			<div class="foot">
			</div>
		</div><!-- end section -->
		
		<div class="section">
			<h2>
				<span>Deactivate Account</span>				
			</h2>
			<div class="section_small_details">
				<a href="/account-deactivation.php">Deactivate my account</a>.
			</div>	
			<div class="content">	
			</div>
			<div class="foot">
			</div>
		</div>
		
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		function unlink(context){
			var _context = context.toLowerCase();
			
			jQuery("#btnLink"+context).attr("disabled","disabled");
			jQuery("#"+_context+"_request_status").attr("style","visibility: normal;");
			
			var urlParams = [];
			urlParams.push('action=unlink');
			urlParams.push('context='+_context);
			urlParams.push('sid='+Math.random());
			jQuery.ajax({
				type: 'POST',
				data: urlParams.join('&'),
				url: '/ajaxpages/account-settings.php',
				cache: false,
				error: function(obj,error,exception){
					jQuery("#btnLink"+context).attr("disabled","");
					jQuery("#"+_context+"_request_status").attr("style","visibility: hidden;");
				},
				success: function(data, status){
					jQuery("#btnLink"+context).html("LINK");
					jQuery("#btnLink"+context).val("/splauncher.php?action=link&context="+_context);
					jQuery("#btnLink"+context).attr("disabled","");
					jQuery("#"+_context+"_request_status").attr("style","visibility: hidden;");
				}
			});
		}
		jQuery("button[name='btnLink']").click(function(){
			if( "" != jQuery(this).val() ){
				window.location.replace(jQuery(this).val());
				return;
			}
			var id = jQuery(this).attr("id");
			if( "btnLinkTwitter" == id ){
				unlink("Twitter");
			}else if( "btnLinkGoogle" == id ){
				unlink("Google");
			}else if( "btnLinkFacebook" == id ){
				unlink("Facebook");
			}
		});
	});
</script>