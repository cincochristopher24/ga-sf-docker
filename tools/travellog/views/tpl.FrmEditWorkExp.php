
<?php 

Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');

	/**
	 * Added by naldz (Nov 20,2006)
	 * This is for the subnavigation links
	 */	
	$subNavigation->show();
?>

<div class="area" id="top">
		<div class="section">
			<h1>Edit Work Experience</h1>
			<div class="content">
			
			
			<form name="workexp" method="post" action="resume.php?action=<?= $act ?>&amp;weID=<?= $workexperienceID ?>" class="interactive">
			
				<?php if ($errorcode != 0): ?>
					<div class="errors">
						<p>There are some problems that need some attention before proceeding. </p>
					
						<ul class="error_list">			
							<?php foreach($errors as $errorlist): ?>
									<li><?= $errorlist ?></li>
							<?php endforeach; ?>
						</ul>
						
					</div>
				<?php endif; ?>
				
			<ul class="form">
				
						<li>
							<label for="txtWE1Position">Position<span class="required">*</span></label>
						
							<?php echo  FormHelpers::CreateTextBox('txtWE1Position', $txtWE1Position,
				    						array(
						        				'class' => 'text', 
						        				'size' => 50, 
						        				'maxlength' => 50,
						        				'id' => 'txtWE1Position'
						        			) 
				    			); ?> 
				    	</li>
				    			
						<li>
						
							<label for="txtWE1Employer">Employer<span class="required">*</span></label>
							
							<?php echo  FormHelpers::CreateTextBox('txtWE1Employer', $txtWE1Employer,
				    						array(
				    							'class' => 'text',
				    							'size' => 50, 
				    							'maxlength' => 50,
				    							'id' => 'txtWE1Employer'
				    						) 
				    			); ?> 
				    	</li>
				    	
						<li>
							
							<label for="txtWE1JobDesc">Job Description<span class="required">*</span></label>
							
							<textarea name="txtWE1JobDesc" cols="47" rows="5" id="txtWE1JobDesc"><?= $txtWE1JobDesc ?></textarea>
							
				    	</li>
				    	
						<li>
							
							<label for="txtWE1LenServ">Length of Service<span class="required">*</span></label>
							
							<?php echo  FormHelpers::CreateTextBox('txtWE1LenServ', $txtWE1LenServ,
				    						array(
						        				'class' => 'text', 
						        				'size' => 50, 
						        				'maxlength' => 50,
						        				'id' => 'txtWE1LenServ'
						        			) 
				    			); ?> 
						</li>
						
						 <li class="action">
							
								<input name  = "btnSubmit" type  = "submit" value = "Submit" class = "submit" />
							
						</li>
				</ul>
			</form>
			</div>
		</div>
	</div>


