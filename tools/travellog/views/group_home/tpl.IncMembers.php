<div class="section" id="members_box">
	<h2>
		<span>Members</span>
		<?php if ($is_admin_logged): ?>
			<span class="header_actions long_links">
			  <a href="/members.php?gID=<?php echo $gID; ?>&mode=3" >Add Member</a> |
			  <a href="/members.php?gID=<?php echo $gID; ?>" >Manage Members</a>
			</span>
		<?php elseif ($show_members_count) : ?>
			<span class="header_actions">
			  <a href="/members.php?gID=<?php echo $gID; ?>" >View All <?php echo $members_count; ?> Members</a>
			</span>
		<?php endif; ?>
	</h2>
	
	<div id="statusPanel1" class="widePanel" style="display: none;">
		<p class="loading_message">
			<span id="imgLoading">
			  <img alt="Loading" src="/images/loading.gif"/>
			</span>
			<em id="statusCaption">Loading data please wait...</em>
		</p>
	</div>
	
	<div id="member_box_content">
		<?php if (0 < $members_count): ?>
			<ul id="members_container" class="users2 bubbled">
				<?php foreach($members as $member): ?>
					<?php $profile = $member->getTravelerProfile();	?>
					<li> 
						<a href="/<?php echo $profile->getUserName();?>" class="thumb bubble_control">
							<img class="pic" src="<?php echo $profile->getPrimaryPhoto()->getPhotoLink('thumbnail');?>" width="65" height="65" alt="User Profile Pic" title="View Profile" />
						</a>
						<div class="details bubble">
							<?php if ($profile->getHTLocationID()): ?>
								<?php if ($profile->getCity()): ?>
									<?php $cID = $profile->getCity()->getCountry()->getCountryID()  ?>
									<img class="flag" src="http://images.goabroad.com/images/flags/flag<?php echo $cID; ?>.gif" width="22" height="11" alt="Travel blog from <?php echo $profile->getCity()->getCountry()->getName(); ?>" />
								<?php endif; ?>
							<?php endif; ?>
							<strong><a href="/<?php echo $profile->getUserName();?>" title="<?php echo $profile->getUserName();?>" class="username"> <?php echo stripslashes($profile->getUserName()); ?> </a></strong>
							<?php if ($is_admin_logged || $member->getPrivacyPreference()->canViewNames($logged_user_id)): ?>
								<em><?php echo stripslashes($profile->getFirstName()); ?> <?php echo stripslashes($profile->getLastName()); ?></em>
							<?php endif; ?>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
			<div class="clear"></div>
			<?php if ($is_admin_logged): ?>
				<div class="header_actions">
					<a href="/messages.php?act=messageMembers&gID=<?php echo $gID; ?>">Send Message</a>
				</div>
			<?php endif; ?>
		<?php else: ?>
			<?php if($is_parent_group): ?>
				<p class="side_help_text"> There are no members of this group. </p>
			<?php else: ?>
				<p class="side_help_text"> There are no members of this subgroup. </p>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	
</div>