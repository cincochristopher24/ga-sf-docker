<?php
if($owner) {
	Template::includeDependentJs ( "/js/scriptaculous/prototype.js", array('minify' => false,'bottom'=> TRUE)          );
	Template::includeDependentJs ( "/js/scriptaculous/scriptaculous.js?load=effects,builder,dragdrop", array('minify' => false,'bottom'=> TRUE)          );
	
	Template::includeDependentJs ( "/js/yahooUI/build/yahoo-dom-event/yahoo-dom-event.js",array("bottom"=>true)           );
	Template::includeDependentJs ( "/js/yahooUI/build/utilities/utilities.js",array("bottom"=>true)           );
	Template::includeDependentJs ( "/js/yahooUI/build/container/container_core-min.js",array("bottom"=>true)           );	
	Template::includeDependentJs ( "/js/yahooUI/build/connection/connection.js",array("bottom"=>true) );
	
	Template::includeDependentJs ( "/js/yahooUI/carousel/carousel.js",array("bottom"=>true)           );
	Template::includeDependentCss( "/js/yahooUI/carousel/carousel_photoalbum.css"               );	
	//Template::includeDependentCss( "/js/yahooUI/build/fonts/fonts-min.css");
	Template::includeDependentCss( "/js/yahooUI/build/container/assets/skins/sam/container.css");
	Template::includeDependentJs ( "/js/yahooUI/build/container/container.js",array("bottom"=>true)           );	
	Template::includeDependentCss( "/min/f=css/Thickbox.css" );
	Template::includeDependentJs("/js/jquery-1.1.4.pack.js");
	Template::includeDependentJs("/js/imgcropper/lib/cropper.js",array("bottom"=>true)); 
	Template::includeDependentCss('/js/imgcropper/css/cropper.css');
	Template::includeDependentCss( "/lytebox/lytebox.css"               );
	Template::includeDependentJs (
					array(
						"/lytebox/lytebox.js",
						"/js/interface/interface.js",
						"/js/interface/source/idrag.js",
						"/js/interface/source/idrop.js",
						"/js/interface/source/isortables.js",
						"/js/ajaxfileupload.js",
						"/js/jeditable.js"
					),array("bottom"=>true));
	Template::includeDependentJs (array("/js/thickbox.js","/js/PhotoUILayout.js","/js/GROUPphotoalbum.js"),array("bottom"=>true));

	$photo_service_javascript =<<<BOF
<script type = "text/javascript">
	jQuery.noConflict();
	PhotoService = new mPhotoService('photoalbum', 6400, 0);
	PhotoService.setLayout(1); 
	//PhotoService.setisOwner(1); 
	
</script>
BOF;
	
	Template::includeDependent($photo_service_javascript,array("bottom"=>true));
	Template::includeDependentCss('/js/yahooUI/carousel/carousel.css');
}
?>
<div id="wide_column">
			<div id="featured_photos" class="section photos">
				<div class="head_left"><div class="head_right"></div></div>
				<div class="content">
							<?php 
								$mleft = (548 - ($imgwidth + 20)) / 2;
							?>
							<div class="white_box"  style="clear:both;width:<?=$imgwidth+10;?>px;height:<?=$imgheight+10;?>px;margin:0 auto;">	
								
									<div id="piccontainer" class="bg_gray">								
										<img id="imgbox<?=$photo[$tmpprev?$tmpprev-1:$start]->getPhotoID();?>" src="<?= urldecode($imgpath); ?>" class="pic_borders" />	
									</div>
								
							</div>
							<div class="clear"></div>
							<? if (false): ?>
								<div style="text-align:center;background-color:#aaa;padding:10px 0;margin:0 0 10px 0;display:none;" id="cropcontrols">
							
									<input type="button" name="btn_save_crop" value="Save thumbnail version" onclick="photo.savethumbnailversion()">
									<input type="button" name="btn_save_crop" value="Cancel" onclick="photo.cancelCROPPING(<?=$photo[$tmpprev?$tmpprev-1:$start]->getPhotoID();?>,'<?=$img_thumb_path?>')" >
									
								</div>
							<? endif; ?>
							<div style="text-align:center;background-color:#aaa;padding:10px 0;margin: 0 0 10px 0; font-size: 14px; font-weight: bold;display:none;" id="cropsuccess">
									
							</div>
							
							<div class="cap_controls meta" style="margin:0;padding:20px 0 0;text-align:center;">
									
									<?if($owner):?>
									<div class="photo_controls"> 		 	
									 	
											<p class="photo_but">
												<a class="changephot_ico" href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(<?=$start+1 ?>,1,'gallery',<?=$genid ?>,'<?= $context?>')">
													Edit Photo
												</a>
											</p>
											<? if (false): ?>
												<p class="photo_but">
										 		 		<a class="changephot_ico" href="<?= $edit_photolink.$photo[$tmpprev?$tmpprev-1:$start]->getPhotoID(); ?>" >												
															Change Photo
														</a>	
								 		 			</p>
												
							 					
								 					<?if($isfound):?>
														<p class="photo_but">
															<a class="rotphot_ico" href="<?=$rotate_link.$photo[$tmpprev?$tmpprev-1:$start]->getPhotoID();?>">
																Rotate Photo
															</a>
														</p>
													<?endif;?>
							 					
							 					
								 					<p class="photo_but">
														<a class="delphot_ico" href="javascript:void(0)" onclick="photo.Delete(<? echo $photo[$tmpprev?$tmpprev-1:$start]->getPhotoID();?>);" alt="Delete Photo" title="Delete Photo">
															Delete photo
														</a>
													</p>
													<?if($isfound):?>
														<?$dim = $imgwidth>$imgheight?$imgheight:$imgwidth ?>
														<p class="photo_but">
															<a class="delphot_ico" href="javascript:void(0)" onclick="photo.execthumbnailversion(<? echo $photo[$tmpprev?$tmpprev-1:$start]->getPhotoID() ?>,<?=$dim ?>);" alt="Set Thumbnail version" title="Set Thumbnail version">
																thumb version
															</a>
														</p>
													<?endif;?>
												
												
													<form id="frmthumbversion" name="frmthumbversion" class="frmCrop">
										
														<input id="txtcropcount" 	name="txtcropcount" 	type="hidden" 		value="1">
														<input type="hidden"  	 	name="cropX" 			id="cropX" 			value="0" />
												        <input type="hidden"  	 	name="cropY" 			id="cropY" 			value="0" />
												        <input type="hidden"  	 	name="cropWidth" 		id="cropWidth" 		value="<?=$imgwidth?>" />
												        <input type="hidden"  	 	name="cropHeight" 		id="cropHeight" 	value="<?=$imgheight?>" />
														<input type="hidden" 		name="photoid" 			id= "photoid"   	value="<?=$photo[$tmpprev?$tmpprev-1:$start]->getPhotoID()?>"/>
														<input type="hidden"		name="tofullview" 		type="tofullview" 	value="1">
														<input type="hidden"  	name="action"  			id= "action"   		value="savethumbversion"/>
													
														<input type="hidden" name="LASTPic" id= "LASTPic" value="1" />
													
														<input type="hidden"  	name="cat" 			id= "cat" 		value="<?=$context;?>" />
														<input type="hidden"  	name="genID" 		id= "genID" 	value="<?=$genid;?>" />
													
													
													
													
													
													</form> 	
												<?endif;?>
												
												
													
							 		</div>
										<? if (false): ?>
											<div class="primphot_controls">
											 		 	<? if(!$photo[$tmpprev?$tmpprev-1:$start]->getPrimaryPhoto()): ?>	
											 		 			<?if($owner):?>
									 							<?endif;?> 			
																<p class="prim_but">
																<a href="javascript:void(0)" 
																onclick="photo.setAsPrimary(<? echo $photo[$tmpprev?$tmpprev-1:$start]->getPhotoID();?>,<?=$tmpprev?$tmpprev-1:$start;?>,<?=$endrow;?>,<?=$pagenum;?>)">Set as Primary Photo <?if($context =='travellog'):?>for this journal entry<?endif;?></a>
																</p>
														<? endif; ?>
												
														<?if($context =="travellog" && !$travelprimaryphoto):?>
															<p class="prim_but">
															<a href="javascript:void(0)" 
															onclick="photo.setAsTravelPrimary(<? echo $photo[$tmpprev?$tmpprev-1:$start]->getPhotoID();?>,<?=$tmpprev?$tmpprev-1:$start;?>,<?=$endrow;?>,<?=$pagenum;?>)">Set as Primary Photo for <span><?=$traveltitle;?></span></a>
															</p>
														<?endif;?>
												
												
											</div>
										<?endif;?>
									<?endif;?>
									
									<? if (false): ?>
										<?if(trim($photo[$tmpprev?$tmpprev-1:$start]->getCaption())):?>		 		 	
									 		 	<?if($owner):?>
											 		<p class="caption_but">
														<a href="javascript:void(0)" onclick="photo.showedit(<? echo $photo[$tmpprev?$tmpprev-1:$start]->getPhotoID() ?>,<?=$tmpprev?$tmpprev-1:$start;?>,<?=$endrow;?>,<?=$pagenum;?>)" alt="Edit Title/Photo" title="Edit Title/Photo" class="edit_text">
															Edit Caption
														</a>
													</p>
											 	<? endif;?> 	
										 	
								 		<?else:?>
												<?if($owner):?>
													<p class="caption_but">
														<a href="javascript:void(0)" onclick="photo.showedit(<? echo $photo[$tmpprev?$tmpprev-1:$start]->getPhotoID() ?>,<?=$tmpprev?$tmpprev-1:$start;?>,<?=$endrow;?>,<?=$pagenum;?>)" alt="Edit Title/Photo" title="Edit Title/Photo" class="edit_text">
															Add Caption
														</a>
													</p>
												<? endif;?> 
								 		<? endif;?>
									<? endif; ?>
							 		
							 		<?if(trim($photo[$tmpprev?$tmpprev-1:$start]->getCaption())):?>

							 			<span class="photo_caption2">
							 				<div style="width:440px;">	
							 					<? echo $photo[$tmpprev?$tmpprev-1:$start]->getCaption(); ?>
							 				</div>	
							 			</span>
							 		<?endif;?>
							 		
							 		
				 					<?php
									/****************************** start: display pokes for photo ***************************/
										/*if (isset($_SESSION['travelerID']) && $owner)
											$pokesview-> render();*/
									/******************************** end: display pokes for photo ***************************/

									/**************************** start: link to add poke ***********************************/									
										/*if (isset($_SESSION['travelerID']) && !$owner) {
											$pokesview-> show();
										}*/
									/****************************** end: link to add poke ***********************************/
								?>
							</div>

							<div class="text_nav meta" style="margin:0 auto;width:300px">
									<? if($tmpprev?$tmpprev-1:$start != 0): ?>
										<a href="javascript:void(0)" onclick="<?=$prevpage?>" class="prevnav_but"><span>prev</span></a>
										
									<? else :?>
										<p class="prevnavdis_but"><span>prev</span></p>
									<? endif; ?>
										
									<? if($nextpage):?>
										
										<a href="javascript:void(0)" onclick="<?=$nextpage?>"  class="nextnav_but"><span>next</span></a>
									<? else :?>
										<p class="nextnavdis_but"><span>next</span></p> 		
									<? endif; ?>						
							</div>	
			
							<div class="clear"></div>	

							<!--<a name="comment_form"></a>-->
						
				
				
				</div>
				<?if(!$owner):?>
					<ul id="actions">
						<li><a href="<?= $backlink; ?>" class="">Back to <?= $backcaption; ?></a></li> 
						<li id="offensive">
						<? $_context = $photo[$tmpprev?$tmpprev-1:$start]->getContext();
						    if ("TravelLog" == get_class($_context)): 				$genID = $_context->getTravelLogID();		$cat = 'travellog';								
							elseif ("TravelerProfile" == get_class($_context)):		$genID = $_context->getTravelerID();			$cat = 'profile';							
							elseif ("PhotoAlbum" == get_class($_context)):			$genID = $_context->getPhotoAlbumID();	$cat = 'photoalbum';
							elseif ("FunGroup" == get_class($_context)):			$genID = $_context->getGroupID();		$cat = 'fungroup';
							else:													$genID = 0;										$cat = '';
							endif;
						 ?>
						<a href="/reportabuse.php?pID=<?= $photo[$tmpprev?$tmpprev-1:$start]->getPhotoID() ?>&amp;cat=<?=$cat?>&amp;genID=<?=$genID?>">Report inappropriate photos please.</a>
						</li>
					</ul>
				<?endif;?>
			<div class="foot"></div>
		</div>

<!-- Added By: Cheryl Ivy Q. Go  28 September 2007 -->
<div id = "comments" class = "section comments">
	<?=$commentsview->renderAjax()?>
</div>
<!-- End -->
</div>