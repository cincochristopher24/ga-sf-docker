<? if ( $paging->getTotalPages() == 0): ?>	
  
  <? if (isset($search) && TRUE == $search): ?>
		<div class="containerbox">Your search has yielded no results.</div>
  <? elseif (TRUE == $mygroups): ?>
		<p class="help_text">
   		<span>You are not a member of any group yet</span>.<br /> Check out the <a href="/group.php"><strong>groups page</strong></a> or <a href="/group.php?mode=add"><strong>create your own</strong></a>!
    </p>
	<? endif; ?>	

<? endif; ?>

  <ul>
    <? foreach($group_list as $eachgroup): ?>
			<? $grpCategoryList = $eachgroup->getGroupCategory(true);
				 $type = "";
				 foreach($grpCategoryList as $key => $value)
					 $type .= $value.", ";
					  
				 $type = preg_replace('/, $/','',$type);		
			?>
			
			<li>	
				<a class="group_thumb" href="<?= $eachgroup->getFriendlyURL()?>">
				  <img src="<?= $eachgroup->getGroupPhoto()->getPhotoLink('standard'); ?>" alt="<?= $eachgroup->getName(); ?>" >
				</a>					
				
				<div class="groups_info">
					<strong>
					  <a href="<?= $eachgroup->getFriendlyURL(); ?>">
					    <?= $eachgroup->getName(); ?>
					  </a>
					</strong>
					
					<p>
						<?= StringFormattingHelper::wordTrim($eachgroup->getDescription(), 40)?>
						  <? if (40 < count(explode(' ', strip_tags($eachgroup->getDescription()) ) ) ) : ?>
						    <?='&#8230;' ?>
						  <? endif ; ?> 
						  <a href="<?= $eachgroup->getFriendlyURL() ?>" title="Read more about the <?= $eachgroup->getName() ?> travel community">
						    Read More
						  </a>
					</p>
					
					<? if (count($grpCategoryList)) : ?>
						<p>
						  <span class="gray">
						    <strong>Type :</strong>
						    <?= $type ?>
						  </span>
						</p>
				  <? endif; ?>
				  
					<? if (Group::ADMIN ==  $eachgroup->getDiscriminator()) : ?>
					  <? $gpp = $eachgroup->getPrivacyPreference(); ?>
					  <? if (NULL != $eachgroup->getSubGroups() && ( (NULL != $loggedUser && $eachgroup->isMember($loggedUser)) || $gpp->getViewGroups()) ): ?>
						  <? $cntr = 0; ?>
							<? $start = TRUE; ?>		
							<? $showsub = TRUE; ?>
							<p>
							  
							  <? foreach ($eachgroup->getSubGroups() as $eachsub ) : ?>
							    <? if ( (!$mygroups) || (NULL != $loggedUser && $mygroups && TRUE == $eachsub->isMember($loggedUser) )) : ?> 
							      <? if ($start) : ?>
							        <strong>You are a member of the following Sub Group:</strong><br />
							      <? else : ?>,
							      <? endif; ?>
					              
					          <a href="<?= $eachsub->getFriendlyURL()?>">
						          <?= $eachsub->getName(); ?>
							      </a>
		                    
						        <? $cntr = $cntr + 1 ;?>
					          <? if (2 == $cntr && 4 <= count($eachgroup->getSubGroups())): ?>
					            <?= ' ... ' ; ?>
				              <? break; ?>
			              <? elseif (3 == $cntr && 3 == count($eachgroup->getSubGroups())): ?>
					            <? break; ?>
								    <? endif; ?>								    
								    <? $start = FALSE; ?>
							    <? endif; ?>
							  <? endforeach; ?>
							</p>
					  <? endif; ?> 
	 				<? endif; ?>
				  		 															
					<? if (trim($eachgroup->getGroupMembershipLink('mygroups')) !== ''):?>
   					<p class="action"><?=$eachgroup->getGroupMembershipLink('mygroups',$current_page)?></p>
   				<? endif; ?>
				</div>
  
			</li>	     
    <? endforeach;?>
	</ul>
	
  <? $paging->showPagination(array('label'=>' Groups')); ?>
