<?php $this->doNotUseMainTemplate(); ?>
	<? if (GroupMembersBoxView::$MEMBERS == $memberTabType && $isAdminLogged && count($grpMembers)) { ?>
		<div class="header_actions">
			<a href="/messages.php?act=messageGroupStaff&gID=<?= $gID ?>">Message Staff</a>
		</div>
	<? } ?>
	     
	<ul id="members_container" class="elements users">	

		<? for($idx=0;$idx<count($grpMembers);$idx++){
			$eachtraveler = $grpMembers[$idx];	
			$eachprofile = $eachtraveler->getTravelerProfile();	
			try {
				$pref = new PrivacyPreference($eachprofile->getTravelerID());							
			}
			catch (Exception $e) {}	
			
			$canViewOnlineStatus = FALSE;		
			// if logged in, check privacy pref of travelers and show own status
			if (isset($pref) && $loggeduserID > 0 && ($pref->canViewOnlineStatus($loggeduserID) || $loggeduserID == $eachprofile->getTravelerID()) )
				$canViewOnlineStatus = true;
				
		?>	
				<li>
					
				<a href="/<?= $eachprofile->getUserName() ?>" class="thumb"><img class="pic" src="<?=$eachprofile->getPrimaryPhoto()->getPhotoLink('thumbnail') ?>" width="65px" height="65px" alt="User Profile Pic" title="View Profile" /></a>
				<div class="details">										 										
	

						<? if (strlen($eachprofile->getUserName()) > 12): ?>
							<a href="/<?= $eachprofile->getUserName() ?>" title="<?= $eachprofile->getUserName() ?>" class="username"><?= $eachprofile->getUserName() ?></a>
						<? else: ?>	
							<a href="/<?= $eachprofile->getUserName() ?>" class="username"><?= $eachprofile->getUserName() ?></a>
						<?endif; ?>
								
						<? if ($eachprofile->getHTLocationID()) { ?>
						 <? $cID = $eachprofile->getCity()->getCountry()->getCountryID()  ?>
							<img class="flag" src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="Travel blog from <?= $eachprofile->getCity()->getCountry()->getName(); ?>" />												
						<? } ?>						
				</div>

				</li>						
			<? } ?>					
	</ul>
	