<? 	
	$videos 		= $contents['obj_entry']->getVideos();
	$primary_video 	= $contents['obj_entry']->getPrimaryVideo();
	$cntVideos		= count($videos);
	$owner			= $contents['obj_entry']->getOwner();
	$travelID 		= $contents['obj_entry']->getTravelID();
	$travelLogID 	= $contents['obj_entry']->getTravelLogID();
?>

<? if ($videos): ?>
	<h2 class="addavideo"><span>Videos</span>
		<? if($contents['show_admin_controls']): ?>
			<? if ($owner instanceof Traveler): ?>
				<a class="entry_links top_shift" href="javascript:void(0)" onclick = "tb_open_new('','/video.php?action=showPrompt&travelerID=<?= $owner->getTravelerID() ?>&travelID=<?= $travelID ?>&travelLogID=<?= $travelLogID ?>&redirect=journal-entry.php?action=view@travellogID=<?= $contents['obj_entry']->getTravelLogID() ?>&height=380&width=400&modal=true')"><span>Add a Video</span></a>
			<? else: ?>
				<a class="entry_links top_shift" href="javascript:void(0)" onclick = "tb_open_new('','/video.php?action=showPrompt&gID=<?= $owner->getGroupID() ?>&travelID=<?= $travelID ?>&travelLogID=<?= $travelLogID ?>&redirect=journal-entry.php?action=view@travellogID=<?= $contents['obj_entry']->getTravelLogID() ?>&height=380&width=400&modal=true')"><span>Add a Video</span></a>
			<? endif; ?>
		<? endif; ?>
	</h2>
	
	<div id = "videos" class="journalviewer">	
		<? if (!$cntVideos): ?>
			<? if ($contents['show_admin_controls']): ?>
				<p id = "primary_video_upload_interface"> There are no videos for this journal entry. <br /> </p> <br/>
			<? endif; ?>
		<? else: ?>
			<ul class="video_lists">
				<li>
					<a class="vidtitle" href="javascript:void(0)" onclick = "tb_open_new('','/video.php?action=playEntryVideo&videoID=<?= $primary_video->getVideoID() ?>&height=360&width=510')">
						<strong><?= $primary_video->getTitle() ?></strong>
					</a> 
					<a class="vidimage" href="javascript:void(0)" onclick = "tb_open_new('','/video.php?action=playEntryVideo&videoID=<?= $primary_video->getVideoID() ?>&height=380&width=510')">
						<span>
							<? if($primary_video->getVideoImageUrl()): ?>
								<img src="<?= $primary_video->getVideoImageUrl() ?>" width = "150" height = "100"/>
							<? else: ?>
								<img src="/images/default_images/120_video.gif" width = "150" height = "100"/>
							<? endif; ?>
						</span>						
						<? if ('00:00' != $primary_video->getDuration()): ?> <span class="duration"><?= $primary_video->getDuration() ?></span> <? endif; ?>
					</a>
				</li>
			</ul>
			
			<? if($contents['show_admin_controls']): ?>
				<div class="journalsmallcontrol">
					<? if ($owner instanceof Traveler): ?>
						<a href="/video.php?action=getTravelerVideos&amp;travelerID=<?= $owner->getTravelerID() ?>&amp;travelID=<?= $contents['obj_entry']->getTravelID() ?>&amp;travelLogID=<?= $contents['obj_entry']->getTravelLogID() ?>&amp;type=journal&amp;jeVideos=1" class="leftside"> manage videos </a>
					<? else: ?>
						<a href="/video.php?action=getGroupVideos&amp;gID=<?= $owner->getGroupID() ?>&amp;travelID=<?= $contents['obj_entry']->getTravelID() ?>&amp;travelLogID=<?= $contents['obj_entry']->getTravelLogID() ?>&amp;type=journal&amp;jeVideos=1" class="leftside"> manage videos </a>
					<? endif; ?>	
				</div>
			<? else: ?>
				<?php if (1 < $cntVideos): ?> 
					<div class="journalsmallcontrol">
						<? if ($owner instanceof Traveler): ?>
							<a href="/video.php?action=getTravelerVideos&amp;travelerID=<?= $owner->getTravelerID() ?>&amp;travelID=<?= $contents['obj_entry']->getTravelID() ?>&amp;travelLogID=<?= $contents['obj_entry']->getTravelLogID() ?>&amp;type=journal&amp;jeVideos=1" class="leftside"> go to videos </a>
						<? else: ?>
							<a href="/video.php?action=getGroupVideos&amp;gID=<?= $owner->getGroupID() ?>&amp;travelID=<?= $contents['obj_entry']->getTravelID() ?>&amp;travelLogID=<?= $contents['obj_entry']->getTravelLogID() ?>&amp;type=journal&amp;jeVideos=1" class="leftside"> go to videos </a>
						<? endif; ?>
					</div>
				<? endif; ?>
			<? endif; ?>
		<? endif; ?>
	</div>
	
<? else: ?>
	<? if($contents['show_admin_controls']): ?>
		<h2 class="addavideo"><span>Videos</span>
			<? if ($owner instanceof Traveler): ?>
				<a class="entry_links top_shift" href="javascript:void(0)" onclick = "tb_open_new('','/video.php?action=showPrompt&travelerID=<?= $owner->getTravelerID() ?>&travelID=<?= $travelID ?>&travelLogID=<?= $travelLogID ?>&redirect=journal-entry.php?action=view@travellogID=<?= $contents['obj_entry']->getTravelLogID() ?>&height=380&width=400&modal=true')"><span>Add a Video</span></a>
			<? else: ?>
				<a class="entry_links top_shift" href="javascript:void(0)" onclick = "tb_open_new('','/video.php?action=showPrompt&gID=<?= $owner->getGroupID() ?>&travelID=<?= $travelID ?>&travelLogID=<?= $travelLogID ?>&redirect=journal-entry.php?action=view@travellogID=<?= $contents['obj_entry']->getTravelLogID() ?>&height=380&width=400&modal=true')"><span>Add a Video</span></a>
			<? endif; ?>
		</h2>
		<div id = "videos" class="journalviewer">
			<p class="help_text">There are no videos for this journal entry.</p>
		</div>	
	<? endif; ?>
<? endif; ?>