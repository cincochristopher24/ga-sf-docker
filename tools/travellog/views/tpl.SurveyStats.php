<?php	
	require_once ('Class.HtmlHelpers.php');
	Template::setMainTemplateVar('layoutID', 'survey');	
	//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');		
	require_once('travellog/helper/Class.NavigationHelper.php');
	if (NavigationHelper::isCobrand()) {
		Template::setMainTemplateVar('page_location', 'Home');
	}else{
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
	Template::includeDependentJs('/js/interactive.form.js');
	Template::includeDependentJs('/js/scriptaculous/scriptaculous.js');
	//Template::includeDependentJs('/js/formeditor/surveystatsviewer.js');
	//Template::includeDependentCss('/css/formeditor/survey.css'); ?>

<!-- displays the profile component: added by neri 11-06-08 -->
<?= $profile->render()?>
<!-- end -->

<?	$subNavigation->show();	
	ob_start();	
?>
<script type="text/javascript">
//<![CDATA[	
	function showDetail(divCont){
		if($('link_'+divCont).innerHTML == 'Show'){
			new Effect.SlideDown('cont_'+divCont, { 
				duration: .10
			});
			$('link_'+divCont).innerHTML = 'Hide';
		}
		else{
			new Effect.SlideUp('cont_'+divCont, { 
				duration: .10
			});
			$('link_'+divCont).innerHTML = 'Show';
		}	
	}
//]]>
</script>
<?php
	$script = ob_get_contents();
	ob_end_clean();
	Template::includeDependent($script);
?>

	<div id="content_wrapper">
	
		<div id="container_inner_wrapper">
			<div id="survey_options">
		    	<ul id="top_tabs">
		    		<li id="sel" class="first_tab"><a href="/surveycenter.php?gID=<?= $oGroup->getGroupID() ?>" class="first_tab">Manage Surveys</a></li>
		      		<li><a href="/surveyformeditor.php?frmID=0&gID=<?= $oGroup->getGroupID() ?>">Create a New Survey</a></li>      		
		    	</ul>
		    	<div class="clear"></div>
		  	</div>
			<div id="content">
				<div id="stats_content" class="section">
					<h2><?= $surveyForm->getName() ?> Stats (<?= $totalParticipants ?> participant<? if ($totalParticipants > 1) echo 's'; ?>)</h2>
					<p id="survey_caption"><?= $surveyForm->getCaption() ?></p>
					<ul class="tabs">
						<li class="active"><strong><a href="/surveystats.php?frmID=<?= $surveyForm->getSurveyFormID() ?>&gID=<?= $oGroup->getGroupID() ?>">Overview</a></strong></li>
						<li><?php if(0<$totalParticipants):?><a href="/surveystats.php?frmID=<?= $surveyForm->getSurveyFormID() ?>&view=entries&gID=<?= $oGroup->getGroupID() ?>"><?php endif; ?>Entries<?php if(0<$totalParticipants):?></a><?php endif; ?></li>
					</ul>
					<div id="stats_basic">
						<ul>
						<?php
							$fields = $surveyForm->getSurveyFormFields();
							foreach($fields as $iFld):
						?>
							<li>
								<h3>
									<?php if ( $iFld->isAnswerable() ): ?>
										<?= $iFld->getCaption() ?><?php if($iFld->isRequired()):?> <span class="required">*</span><?php endif; ?>
									<?php endif; ?>
								</h3>
								<?php 
									if ( $iFld->isAnswerable() ):
										$fieldAnswer = $surveyFieldAnswers[$iFld->getSurveyFieldID()];
								?>
									<div class="field_results">
											<?php if ( $iFld->isWithOptions() ): ?>
												<table border="0" cellpadding="0" width="100%" cellspacing="0" class="choice_stats">
												<?php 
													foreach($fieldAnswer as $fieldCaption => $totalAns):
														$percent = $percent = ($totalAns > 0 ? round(($totalAns/$totalParticipants)*100, 1) : 0);		
												?>
														<tr>
															<td class="choice_label"><?= $fieldCaption ?></td>
															<td class="choice_results stat_numbers"><?= $totalAns ?></td>
															<td class="choice_percentage"><?= $percent ?>%</td>
															<td class="choice_percentage_figure"><span><span style="width:<?= $percent ?>%"></span></span></td>
														</tr>
												
												<?php endforeach; ?>
												</table>	
											<?php else: ?>
												<p class="field_info">
													<span class="stat_numbers">
														<?php $responses = count($fieldAnswer) ?>
														<?= $responses ?>
													</span>
													<?php if(0 < $responses): ?>
														 Responses | <a href="javascript:void(0)" onclick="showDetail(<?=$iFld->getSurveyFieldID()?>)" id="link_<?=$iFld->getSurveyFieldID()?>">Show</a>
													<?php else: ?>
														 Response
													<?php endif; ?> 
												</p>
												<div id="cont_<?=$iFld->getSurveyFieldID()?>" style="display:none">
													<ul class="stat_texts">
													<?php foreach($fieldAnswer as $iAns):  ?>
														<li>
															<?=HtmlHelpers::Textile($iAns)?>
														</li>
													<?php endforeach;?>
													</ul>
												</div>
											<?php endif; ?>
									</div>
								<?php endif; ?>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>	
	

	
		<div class="clear"></div>
		
	</div>