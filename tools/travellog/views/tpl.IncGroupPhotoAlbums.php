
<div class="section" id="group_photos">
	<h2>
		<span>Photo Albums</span>
		<? if ($isAdminLogged || (is_array($grpPhotoAlbums) && count($grpPhotoAlbums) >3) ) : ?>
				<span class="header_actions">
							<div style="clear:both">
									<? if ($isAdminLogged) : ?>
											<a href="/collection.php?type=group&amp;ID=<? echo $grpID ?>">Manage Album</a> 
									<? else: ?>
											
											<? //check if there are more than 3 albums with uploaded photos ?>
											<? $album_counter = 0; ?>
											<? foreach($grpPhotoAlbums as $k =>$album): ?>
													<?
														$uploadedphotos			= $album["numphotos"];
													?>
													<? if($uploadedphotos >0): ?>
															<? $album_counter += 1; ?>
													<? endif; ?>
											<? endforeach; ?>
											<? if($album_counter >3):?>
													<a href="/collection.php?type=group&amp;ID=<? echo $grpID ?>">View More Albums</a>
											<? endif; ?>
									<? endif; ?>
							</div>
				</span>
		<? endif;?>
		
	</h2> 
	<div class="content">												
		
		 <? $cnt = 0;?>
		
		<?if( is_array($grpPhotoAlbums) && count($grpPhotoAlbums)): ?>
				
					<? foreach($grpPhotoAlbums as $k =>$palbum): ?>
						<?
							$creator 				= $palbum["creator"];
							$context 				= $palbum["context"];
							$genID					= $palbum["genID"];
							$title					= stripslashes($palbum["title"]);
							$numphotos			= $palbum["numphotos"];
							$publish				= $palbum["publish"];
							$primaryphoto		= $palbum["primaryphoto"];
						?>
						
						
						<? if($isAdminLogged || $numphotos>0): ?>
						
								   <div class="photo"> 
									   		<div id="peralbum_<?= $genID?>">	
													
													<? if($numphotos >0): ?>
															<!--a href="javascript:void(0)" onclick="PhotoService.loadPhotoPopup(<?= $primaryphoto->getPhotoid(); ?>,'gallery',<?= $genID?>,'<?= $context?>')">
																<img id="listen_img_album_<?= $genID?>" src="<?=$primaryphoto->getPhotoLink('default')?>" class="photo_bg"/>
															</a-->
															<a href="javascript:void(0)" onclick="collectionPopup.setParams('group','<?= $grpID; ?>');collectionPopup.loadGallery('<?= $context?>',<?= $genID?>,<?= $primaryphoto->getPhotoid(); ?>);">
																<img id="listen_img_album_<?= $genID?>" src="<?=$primaryphoto->getPhotoLink('featured')?>" class="photo_bg"/>
															</a>
													<? else:?>
															<img id="listen_img_album_<?= $genID?>" src="<?=$primaryphoto->getPhotoLink('featured')?>" class="photo_bg"/>
													<? endif; ?>
													
													<!-- album title container- for ajax callback request-->
													<div id="ptitle_<?= $genID?>">
														<p class="photo_caption">
															<?= $title?>
															<br>
															<span id="listen_caption_album_<?= $genID?>" style="font-size:10px;text-align:right">
																<?= $numphotos?> <?= $numphotos>1?"photos":"photo" ?>
															</span>
														</p>			
													</div>
													
													
											</div> 
											
						            </div>
									
													
									<? $cnt = $cnt + 1;?>	
								
									<? 
										if($cnt >2)break;
									?>
						
						<? endif;?>
						
					<? endforeach; ?>	
					
					
					
		<? endif;?>
		
		<? if(is_array($grpPhotoAlbums) && count($grpPhotoAlbums) == 0 || !is_array($grpPhotoAlbums) ): ?>
		<?/*	<div class="help_text" style="text-align:center">
				<span>
				<? if ( isset($is_parent_group) && $is_parent_group ) : ?>
					There are no Photo albums added to this group.
				<? else : ?>
					There are no Photo albums added to this subgroup.
				<? endif ?>
				<? if ($isAdminLogged): ?>	
						 	<a href="/collection.php?type=group&amp;ID=<? echo $grpID ?>">
						 		Add Photo Album?
						 	</a>
				<? endif; ?>
				</span>
			</div> */ ?>
			
				<div class="gline">
					<div class="gunit image">
						<img src="/images/g_album.jpg" alt="Photo Album" width="192" height="131" />
					</div>
					<div class="glastUnit description">
						<h3>Give your community a glimpse into your programs.</h3>
						<p>
							Feature the best photos from your program's stock photos to give a glimpse of the programs activities or create an album collection from the members contributions.
						</p>
						<a class="button_v3 goback_button jLeft" href="/collection.php?type=group&amp;ID=<?=$grpID ?>"><strong>Add Photo Album</strong></a>
					</div>
				</div>
		<? endif; ?>
		
	</div>
	<div class="foot"></div>
</div>