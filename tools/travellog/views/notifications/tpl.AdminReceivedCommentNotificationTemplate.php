<?php // TEMPLATE FOR RECEIVED COMMENT INSTANT NOTIFICATION TEMPLATE ?>
Hi <?=$recipientName?>,


<?=$message?>. The message was sent on
<?=$gaDateTime->set($comment->getCreated())->standardFormat()?>.

<?php if( '' != $comment->getTheText() ):?>
=========================================

Comment ID: <?=$comment->getCommentID() ?>

Recipient: <?=$recipient ?>

Message:

<?=$comment->getTheText()?>


=========================================

<?php endif; ?>

To view this message please follow the link below:
http://<?=$siteUrl?>/<?=$link?>


To approve please follow the link below:
http://<?=$approveUrl?>/admin/approveShoutOut.php


Many thanks,

<?php echo $emailName?>

http://<?=$siteUrl?>