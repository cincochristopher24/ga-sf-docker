<?php
	require_once('Class.Crypt.php');
	$crypt = new Crypt;
	$k=urlencode($crypt->encrypt('travelID='.$travel->getTravelID().'&groupID='.$group->getGroupID()));
?>
Hi <?=ucfirst($recipient)?>,


<?=ucfirst($owner->getUsername())?> has created a new journal '<?=$travel->getTitle()?>' on <?=preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName)?>.


To view this journal, follow this link:
http://<?=$siteUrl?>/journal-entry.php?action=view&travelID=<?=$travel->getTravelID()?>


To approve this journal follow this link:
http://<?=$siteUrl?>/compjournal.php?action=approveJournal&k=<?=$k?>


To unapprove this journal follow this link:
http://<?=$siteUrl?>/compjournal.php?action=unapproveJournal&k=<?=$k?>


To feature this journal follow this link:
http://<?=$siteUrl?>/compjournal.php?action=featureJournal&k=<?=$k?>



Many thanks,

<?=$siteName?>

http://<?=$siteUrl?>