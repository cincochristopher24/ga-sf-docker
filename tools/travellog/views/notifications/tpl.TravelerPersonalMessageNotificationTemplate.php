<?
	if($source->postedAsAdministrator()) :
		$extra = 'Your administrator for the group ' . $source->getName();
	elseif($source->postedAsStaff()):
		$extra = 'Your staff, ' . $source->getName();
	else:
		$extra = ucfirst($source->getName());
	endif;
	$extra .= ' sent you ' . (count($messages) > 1? 'messages':'a message') . ' on ' . preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$siteName);
?>
<?/*HTML?>
Hi <?= ucfirst($recipient)?>,
<BR /><BR />
	<? if($source->postedAsAdministrator()) : ?>
			Your administrator for the group <?= $source->getName()?>
	<? elseif($source->postedAsStaff()):?>
			Your staff, <?= $source->getName()?> 
	<? else:?>
			<?= ucfirst($source->getName())?> 
	<? endif;?>
		sent you <?=count($messages) > 1? 'messages':'a message'?> on <?=$siteName?>. 
		The latest message was sent on <?= $time ?>.
<? foreach($messages as $message) :?>
		<br/><br/>
		==================================
		<BR />
		 Subject: <?= $message->getTitle() ?>
		<BR />
		 Message:
		<BR />
		 <?= nl2br($message->getText())?>
		 <BR />
		==================================
		<BR /><a href="http://<?= $siteUrl . '/messages.php?reply&active=inbox&messageID=' . $message->getAttributeID()?>">Reply to this message</a>
<? endforeach; ?>
<BR><BR>
Many thanks, <BR><BR>
<strong>The <?= $siteName ?> Team </strong>
<br /> <a href = 'http://<?=$siteUrl?>'> <?=$siteUrl?> </a>
<?/*end HTML*/?>
Hi <?= ucfirst($recipient)?>,


<?=$extra?>.
<?$links = array()?>

<?foreach($messages as $iMessages):?>
<?if($iMessages['isMessageIncluded']):?>

======================================================
Subject: <?=$iMessages['message']->getTitle()?>

Message:
<?=$iMessages['message']->getText()?>

======================================================
To reply to this message, follow the link below:
http://<?=$siteUrl . '/messages.php?reply&active=inbox&messageID=' . $iMessages['message']->getAttributeID()?>


<?else:?>
<?$links[] = "http://$siteUrl/messages.php?view&active=inbox&messageID=" . $iMessages['message']->getAttributeID()?>
<?endif;?>
<?endforeach;?>
<?if(count($links) > 0):?>
<?= (count($links) == count($messages) ? (count($links) > 1 ? 'To view these' : 'To view this' ) : chr(10) . 'To view other')?> <?=count($links) > 1 ? 'messages, follow the links':'message, follow the link'?> below:

<?=implode(chr(10),$links)?>
<?/*http://<?=$siteUrl?>/bulletinmanagement.php?hlEntryID=<?=?>*/?>


<?endif;?>
<?/*foreach($messages as $message):?>


======================================================
Subject: <?=$message->getTitle()?>

Message:
<?= $message->getText()?>

======================================================

To reply to this message, follow the link below:
http://<?=$siteUrl . '/messages.php?reply&active=inbox&messageID=' . $message->getAttributeID()?>

<?endforeach;*/?>


Many thanks,


<?=$siteName?>

http://<?=$siteUrl?>
