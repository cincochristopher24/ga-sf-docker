Hi <?=ucwords($profile->getFirstName())?>,

Congratulations on your up-coming participation on a GeoVisions program! We look forward to having you on our programs and becoming part of our growing GeoVisions network. We would like to ask you to join our online social network, which will become one of your primary tools before, during and after your program.
 
1. Logon to your personalized GeoVisions webpage by going to http://geovisions.goabroad.net
 
                * Logon = <?=$traveler->getTravelerProfile()->getEmail()?>

                * Password = <?=$traveler->getPassword()?>

 
2. Learn more and have fun
 
                * Create your profile for other GeoVisions participants to see
                * Connect with other GeoVisions participants
                * Post your stories and photos
                * Once the program begins, post your stories and photos, get program updates, follow the other participants' adventures and much more.

 
The GeoVisions online social network has four applications that you can add on Facebook, MySpace, Orkut and hi5. We know most of your friends are on these social networks, and we want you to be able to share your travel experiences with them. By adding our applications, you can keep journalling onGeoVisions and be confident that your journals are instantly shared with your friends on these sites.


Once again, congratulations and welcome to GeoVisions!

Alexandra LeGrant
Social Media

63 Whitfield St.
Guilford, CT 06437
Tel: 1.203.453.5838
Tel Toll Free: 1-877-949-9998
Fax: 1.203-738-1024
alexandra@geovisions.org
