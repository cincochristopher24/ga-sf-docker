<?$extra = "to join your " . (($group instanceof AdminGroup) ? "group ":"club ") . $group->getName()?>
<?$arr = array()?>
<?foreach($travelers as $traveler):?>
<?$arr[] = ucfirst($traveler->getUsername())?>
<?endforeach;?>
Hi <?=$recipient?>,

<?=((count($arr)<=2) ? implode(' and ', $arr) : implode(', ',array_slice($arr,0,count($arr)-1)) . ' and ' . $arr[count($arr)-1])?> want<?php if(count($arr)==1): echo 's'; endif;?> to join your group <?php echo $group->getName()?>.
<?/*There <?= (count($travelers) > 1 ? "are new requests $extra":"is a new request $extra")?> from the following <?=(count($travelers) > 1 ? "travelers":"traveler")?>:*/?>

<?foreach($travelers as $traveler):?>

======================================================
To accept request from <?=$traveler->getUsername()?>, follow the link below:
http://<?=$siteUrl?>/members.php?mode=accept&travelerID=<?=$traveler->getTravelerID()?>&gID=<?=$group->getGroupID()?>


To deny request from <?=$traveler->getUsername()?>, follow the link below:
http://<?=$siteUrl?>/members.php?mode=deny&travelerID=<?=$traveler->getTravelerID()?>&gID=<?=$group->getGroupID()?>


*Your privacy is protected and <?=$traveler->getUsername()?> will not be notified if you choose to deny this request.
======================================================

<?endforeach;?>


To manage your members<?/*view pending <?= (count($travelers) > 1 ? "requests":"request")?>*/?>, please follow the link below:
http://<?=$siteUrl?>/members.php?gID=<?=$group->getGroupID()?>



Many thanks,


<?=$emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>