<?php
	$groupNamePossessive = GaString::makePossessive($group->getName());
	
	$message = <<<EOF
Hi $recipient,
EOF;
	foreach($data as $dID => $travData){
		$discussion = DiscussionPeer::retrieveByPK($dID);
		
		if(!$discussion) continue;
		$numUsers = count($travData);
		
		$pluralForm = ($numUsers > 1) ? 's' : '';
	$message .= <<<EOF
	
________________________________
	
The following user$pluralForm added a post to the discussion '{$discussion->getTitle()}' in {$groupNamePossessive} Discussion Board.
EOF;

		foreach($travData['travelers'] as $travelerID){
			try{
				$traveler = new Traveler($travelerID);	
			}
			catch(exception $e){
				throw new NotificationException($notificationError, $trace[0], '[CLASS]Traveler: ' . $e->getMessage());
			}
	$message .= <<<EOF

	- {$traveler->getUsername()}
EOF;
		}
		
	$message .= <<<EOF
	
	
	
To view posts from the discussion '{$discussion->getTitle()}', please follow this link:
http://$siteUrl/discussion/posts/{$discussion->getID()}
	
EOF;
		
	}

	$message .= <<<EOF

________________________________




Many thanks,

$emailName

http://$siteUrl


$notificationFooter

EOF;


echo $message;
?>