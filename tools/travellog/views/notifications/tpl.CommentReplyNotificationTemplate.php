<?php // TEMPLATE FOR NOTIFICATION OF REPLIES TO COMMENT TO BE SENT TO OTHER RELATED COMMENT POSTERS ?>
Hi <?=$recipientName?>,


<?=$message?>
The message was sent on <?=$gaDateTime->set($comment->getCreated())->standardFormat()?>.

<?php if( '' != $comment->getTheText() && $showComment ):?>
=========================================
Message:

<?=$comment->getTheText()?>

=========================================

<?php endif; ?>

To view this message please follow the link below:
http://<?=$siteUrl?>/<?=$link?>

<?=$endMessage?>.


Many thanks,

<?php echo $emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>