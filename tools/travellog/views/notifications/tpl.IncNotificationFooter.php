<?php 
	
	if( isset($group) ):
		$s = (isset($isAdvisor) && $isAdvisor) ? 'you are an administrator of a group':'you are a member of '.$group->getName();
	else:
		$s = 'you have an account';
	endif;
	
	if (isset($isAdvisor)) {
		if ($isAdvisor && isset($group) && ($group instanceof Group || $group instanceof gaGroup) )
			$link = 'http://'.$siteUrl.'/group-privacy.php?groupID='.$group->getGroupID();
		else
			$link = 'http://'.$siteUrl.'/privacy.php';	
	}
	
	else 
		$link = 'http://'.$siteUrl.'/privacy.php';
		
	if( isset($isStaff) && $isStaff ){
		$s = ($isSubgroup)? 'you are a staff of group '.$group->getName() : 'you are a staff of '.$group->getName();
		$link = 'http://'.$siteUrl.'/group-privacy.php?groupID='.$group->getGroupID();
	}
?>
- - - - -

You received this email because <?php echo $s .' on '.$siteName?>. To manage the emails you receive from <?php echo $siteName?>, please follow this LINK. <?php echo $link; ?>


This e-mail was sent from an unattended mailbox. Please do not reply. Send any questions to http://<?php echo $siteUrl?>/feedback.php