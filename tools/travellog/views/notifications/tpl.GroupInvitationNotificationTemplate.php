Hi<?=('' != trim($recipient) ? " $recipient" : "")?>,


You have an invitation to join the group <?=$group->getName()?>.


To find out more about <?php echo $group->getName()?>, visit their page by following the link below:
http://<?php echo $siteUrl . $group->getFriendlyUrl()?>



To accept this invitation, please follow this link:
http://<?=$siteUrl?>/membership.php?gID=<?=$group->getGroupID()?>&mode=accept


To view your other invites, please follow this link:
http://<?=$siteUrl?>/membership.php?gID=<?=$group->getGroupID()?>




Many thanks,

<?php echo $emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>