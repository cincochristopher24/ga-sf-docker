Hi <?=$recipient?>,

Your group administrator for <?php echo $group->getName()?> has posted news for the group.

<?php if( $includeMessage ): ?>
======================================================
Title: <?=$news->getTitle() . chr(10)?>
Content:
<?=$news->getContent() . chr(10)?>
======================================================

<?php endif;?>
To read this news, please follow the link below:
http://<?=$siteUrl?>/news.php?gID=<?=$group->getGroupID()?>
<?php if( $news->isPrioritized() ):?>



The posted news is time-sensitive and will expire on <?php echo date('F d, Y', strtotime($news->getExpiration()))?>. Please view the post before this date.
<?php endif;?>



Many thanks,

<?php echo $emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>