<?php
	require_once('Class.GaDateTime.php');
	$dateTime = new GaDateTime;
?>

Hi <?= ucfirst($recipientName[$mode])?>,


<?= $message[$mode]?>. The message was sent on <?= $dateTime->set($comment->getCreated())->standardFormat()?>.

To view this message please follow the link below:

http://<?=$siteUrl?>/<?=$link?>



Many thanks,

<?=$siteName?>

http://<?=$siteUrl?>