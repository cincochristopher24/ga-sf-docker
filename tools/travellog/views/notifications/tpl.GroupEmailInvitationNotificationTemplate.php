Hi<?=('' != trim($recipient) ? " $recipient" : "")?>,


Your friends at <?php echo $group->getName()?> have invited you to join their group on the <?php echo $siteName?> online community.


By joining their group, you will be able to:

- Find out what's new through News and <?php echo $group->getName()?> calendar  events that will show up on your page. 
- Have access to <?php echo GaString::makePossessive($group->getName())?> resource files, like program information, itineraries, forms and others
- Receive email broadcasts from <?php echo $group->getName()?>

- Join discussions with members through their Discussion Boards
- Participate in surveys and evaluations conducted by <?php echo $group->getName()?>



To join <?php echo $group->getName()?>, please follow the link below:

http://<?php echo $siteUrl?>/register.php?<?=$urlParam?>




Many thanks,

<?php echo $emailName?>

http://<?=$siteUrl?>




- - - - -

You received this email because you have an invitation from <?php echo $group->getName()?>. We haven't added your email address to any lists, nor will we share it with anyone at any time.

This e-mail was sent from an unattended mailbox. Please do not reply. Send any questions to http://<?php echo $siteUrl?>/feedback.php