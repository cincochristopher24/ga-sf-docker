Hi [#recipient_name#],

Invitation to be a staff of group [#group_name#] has been cancelled by [#group_name#]'s administrator.



Many thanks,


[#site_name#]
[#site_url#]