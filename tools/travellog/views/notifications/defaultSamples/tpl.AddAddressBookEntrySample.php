Dear [#recipient_name#],


Thank you for confirming your consent. You have agreed to receive updates about [#doer_name#]'s activities on [site_name].
[#He/She#] will be able to send you messages. You will also find out if [#doer_name#]:

	* wrote new travel journals
	* edited [his/her] travel journals
	* and uploaded new photos.

To view [#doer_name#]'s profile, please click the following link:
[#link_to_profile#]


If you're not yet registered on the [#site_name#], you may sign up by clicking on the following link:
[#link_to_register#]


Have a great day!


Many thanks,

[#site_name#]
[#site_url#]