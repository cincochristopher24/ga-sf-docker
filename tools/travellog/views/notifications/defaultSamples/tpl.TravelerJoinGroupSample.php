Hi [#recipient)name#],


[#doer_name#] has joined your group [#group_name#].


To view [#doer_names#]'s profile, please follow the link below:
[#link_to_profile#]


To manage your members, follow the link below:

[#link_to_manage#]


Many thanks,

[#site_name#]
[#site_url#]