Hi [#recipient_name#],

The administrator of the group [#name_of_parent_group#] has invited you to join and be a staff of their group.


To confirm invitation as staff for the group [#name_of_subgroup#], follow the link below:

[#link_to_membership#]


To visit [#name_of_parent_group#]' Homepage, please follow the link below:

[#link_to_parent_group#]



Many thanks,

[#site_name#]
[#site_url#]