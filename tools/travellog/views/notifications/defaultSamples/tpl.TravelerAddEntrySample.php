Hi [#recipient_name#],

[#doer_name#] has added a new entry for his/her journal '[#journal_title#]' on [#site_name#].


==========================================================
Title: [#journal_entry_title#]

To view this journal entry, follow this link:
[#link_to_journal_entry#]

==========================================================


To approve this journal, follow this link:
[#link_to_approve_journal#]

To unapprove this journal, follow this link:
[#link_to_unapprove_journal#]

To feature this journal, follow this link:
[#link_to_feature_journal#]



Many thanks,

[#site_name#]
[#site_url#]