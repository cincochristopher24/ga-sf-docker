Hi [#recipient_name#],


You have an invitation to join the group [#group_name#].


To accept this group invitation, please follow the link below:
[#link_to_accept_invitation#]


To deny this group invitation, please follow the link below:
[#link_to_deny_invitation#]


To view your other invites, follow the link below:
[#link_to_other_invites#]


Many thanks,

[#site_name#]
[#site_url#]