Hi [#recipient_name#],

Your administrator has posted a news for the group [#group_name#].

======================================================
Title: [#news_title#]
Content:
[#news_content#]
======================================================

To read this news, please follow the link below:
[#link_to_news#]


Many thanks,

[#site_name#]
[#site_url#]