Hi [#recipient_name#],

Greetings from [#site_name#]!

Your friends at [#group_name#] have invited you to join their group on the [#site_name#] online community.

Find out what's new with [#group_name#]'s members. By joining their group, you will be able to:

- check out travel journals and photos from other members
- share your own experiences with your own travel journals and photos
- check alerts for group members
- read bulletins from [#group_name#]

- access [#group_name#]'s resource files, like program information, itineraries, updates and forms
- receive email broadcasts for members
- discuss experiences with members through private messages
- participate in surveys and evaluations for [#group_name#]'s members
- be updated with what's new about [#group_name#] through calendar events that will automatically appear on your calendar.

To join [#group_name#], please follow the link below:
[#link_to_registration#]


Happy travels!

[#site_name#]
[#site_url#]