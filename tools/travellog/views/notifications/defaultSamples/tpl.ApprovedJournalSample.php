Hi [#recipient_name#],

Your travel journal entitled [#title_of_journal#] has been included
in [#group_name#] group. This means that your travel journal will also appear in 
the groups journals.


Many thanks,


[#site_name#]
[#site_url#]
