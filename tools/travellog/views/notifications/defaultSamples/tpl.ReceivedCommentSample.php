Hi [#recipient_name#],


You received a new greeting from [#doer_name#]. The message was sent on
[#Tuesday, January 27, 2009 3:09 AM#].

=========================================
Message:

[#comment_message#]

=========================================


To view this message please follow the link below:
[#link_to_comment#]



Many thanks,

[#site_name#]
[#site_url#]