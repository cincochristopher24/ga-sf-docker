Hi [#recipient_name#],

[#doer_name#] sent you a message on [#site_name#].

======================================================
Subject: [#message_title#]
Message: 
[#message#]
======================================================

To reply to this message, follow the link below:
[#link_to_messages#]



Many thanks,


[#site_name#]
[#site_url#]