Hi <?=ucfirst($recipient->getUserName())?>,


<?=($doer->getTravelerID() == $group->getAdministratorID() ? 'Your administrator for the group ' . $group->getName() : ucfirst($doer->getUserName()))?> created new <?=(1 < count($events) ? 'events' : 'event')?> for the group <?=$group->getName()?>. To view <?=(1 < count($events) ? 'these events, please follow the links below' : 'this event, please follow the link below')?>:


<?foreach($events as $event):?>
http://<?=$siteUrl?>/event.php?gID=<?=$group->getGroupID()?>&eID=<?=$event->getGroupEventID()?>&context=1

<?endforeach;?>


Many thanks,

<?=$siteName?>

http://<?=$siteUrl?>