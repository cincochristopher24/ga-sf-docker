Hi <?=ucwords($profile->getFirstName())?>,

Congratulations on your acceptance to participate on an IEP program!  We look forward to having you on our programs and becoming part of our growing IEP network. We would like to ask you to join our online network, which will become one of your primary tools before, during and after your program.
 
1. Logon to your personalized IEP webpage by going to http://iep.goabroad.net
 
                * Logon = <?=$traveler->getTravelerProfile()->getEmail()?>

                * Password = <?=$traveler->getPassword()?>

 
2. Learn more and have fun
 
                * Create your profile for other IEP participants to see
                * Connect with other IEP participants
                * Post your stories and photos
                * Learn about your upcoming program

 
Once again, congratulations and welcome to IEP!
 
The IEP Team
International Education Programs
Jacksonville University G-105
2800 University Blvd North
Jacksonville, FL 32211
ph: 904.256.7295
fx: 904.256.7187
info@iepabroad.org