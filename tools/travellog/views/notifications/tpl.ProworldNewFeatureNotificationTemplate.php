Hi <?=$member->getUsername()?>,


Greetings from ProWorld Service Corps!

Check out the new and improved MyProWorld.net! The redesigned MyProWorld.net gives you new features and an enhanced user
experience as you share your travel journals, learn about other ProWorld travelers' experiences and connect with ProWorld staff.

We have also improved the logon system.  Your username is now your email address.

Happy travels!

ProWorld Service Corps
http://myproworld.goabroad.net

<?/** ORIGINAL TEXT FROM russell
Hi <username>,
>
>
> Greetings from ProWorld Service Corps!
>
> Check out the new and improved MyProWorld.net! The redesigned  
> MyProWorld.net gives you new features and an enhanced user  
> experience as you share your travel journals, learn about other  
> ProWorld travelers' experiences and connect with ProWorld staff.
>
> We have also improved the logon system.  Your username is now your  
> email address.
>
> Happy travels!
>
> ProWorld Service Corps
> http://myproworld.goabroad.net
>
**/ ?>