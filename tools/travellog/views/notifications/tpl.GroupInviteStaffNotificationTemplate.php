Hi <?php echo $recipient?>,


The administrator of the group <?=$parent->getName()?> has invited you to join and be a staff of their group.


To visit <?=GaString::makePossessive($parent->getName())?> Homepage, please follow the link below:

http://<?=$siteUrl?>/group.php?gID=<?=$parent->getGroupID()?>




To accept this invitation, please follow the link below:

http://<?=$siteUrl?>/membership.php



Many thanks,

<?php echo $emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>