Dear <?=$addressBookEntry->getName()?>,

Greetings from <?=($siteName == $ownerName ? 'GoAbroad Network' : $siteName)?> and <?=$ownerName?>!

You received this email because <?=$ownerName?> added you to <?=(is_null($gender) ? 'his/her' : ('his' == $gender ? 'his' : $gender)) . ' ' . $siteName?> Address Book, and would like you to receive updates about <?=(is_null($gender) ? 'his/her' : ('his' == $gender ? 'his' : $gender))?> travel journals and photos on <?=$siteName?>.

<?=$siteName?> is a social networking site dedicated to travelers worldwide. <?=$username?> shares <?=(is_null($gender) ? 'his/her' : ('his' == $gender ? 'his' : $gender)) . ' ' . ($isModerator ? 'organization\'s travel experiences' : 'travels')?> on this website by writing travel journals <?=($isModerator ? 'or uploading photos.' : ', uploading photos and sending messages to friends like you.' )?> To view <?=GaString::makePossessive($username) . ($siteName == $username ? '' : " $siteName")?> profile, please click on the link below:
 					
<?=$profileLink . chr(10)?>

We would then like to ask for your permission about receiving these updates. If you click on the link below, you AGREE to be notified if <?=$username?> wrote new travel journals<?=($isModerator ? ' or uploaded new photos.' : ', uploaded new photos or to receive messages from ' . (is_null($gender) ? 'him/her' : ('his' == $gender ? 'him' : $gender)) )?>.

<?=$confirmationLink?>


If you prefer NOT to be notified of such updates, please click on the link below or do nothing and this request for confirmation will expire in 2 months

<?=$denyLink?>


============================================================
To create your own account on <?=$siteName?>, please visit

http://<?=$siteUrl?>
	
============================================================


Thank you and have a great day!

The <?=$siteName?> Team