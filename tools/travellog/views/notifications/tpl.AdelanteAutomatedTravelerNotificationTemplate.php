Hi <?=ucwords($profile->getFirstName())?>,

Congratulations on your up-coming participation on the Adelante Abroad program! We look forward to having you on our programs and becoming part of our growing Adelante network. We would like to ask you to join our online social network, which will become one of your primary tools before, during and after your program.
 
1. Logon to your personalized Adelante webpage by going to http://adelante.goabroad.net
 
                * Logon = <?=$traveler->getTravelerProfile()->getEmail()?>

                * Password = <?=$traveler->getPassword()?>

 
2. Learn more and have fun
 
                * Create your profile for other Adelante participants to see
                * Connect with other Adelante participants
                * Post your stories and photos
                * Once the program begins, post your stories and photos, get program updates, follow the other participants' adventures and much more.

 
Once again, congratulations and welcome to Adelante!

The Adelante Team
http://adelante.goabroad.net
