<?php
	require_once('Class.Crypt.php');
	$crypt = new Crypt;
	$k=urlencode($crypt->encrypt('travelID='.$travel->getTravelID().'&groupID='.$group->getGroupID()));
	
	/**
	* EDITS: check if journal is approved or featured already 
	* (for approve, unapprove, feature links in email notification)
	**/
	require_once('travellog/dao/Class.GroupApprovedJournals.php');
	$relatedGroups = GroupApprovedJournals::getGroupsWhereTravelIsApproved($travel->getTravelID());
	$alreadyApproved = in_array($group->getGroupID(),array_keys($relatedGroups));
	
	require_once('travellog/model/Class.Featured.php');
	$featuredGroups = Featured::getGroupsWhereTravelIsFeatured($travel->getTravelID());
	$alreadyFeatured = in_array($group->getGroupID(),array_keys($featuredGroups));
	
	$automode = $group->getPrivacyPreference()->isAutoModeGroupJournals();
?>
Hi <?php echo $recipient?>,

<?=$owner->getUsername()?> has added <?=(count($entries)>1 ? 'new entries' : 'a new entry')?> for the journal '<?=$travel->getTitle()?>' on <?php echo $siteName?>.


<?php foreach($entries as $entry):?>
==========================================================
Title: <?=$entry->getTitle() . chr(10)?>

To view this journal entry, follow this link:
http://<?=$siteUrl?>/journal-entry.php?action=view&travelID=<?=$travel->getTravelID()?>&travellogID=<?=$entry->getTravellogID()?>


==========================================================

<?php endforeach;?>

<?php if($automode): ?>
<?php echo $travel->getTitle()?> has been automatically approved and featured based on your group's privacy preference. 

To change this setting follow this link: http://<?php echo $siteUrl?>/group-privacy.php?groupID=<?php echo $group->getGroupID()?>
<?php endif;?>	


<?php if(!$alreadyApproved): ?>
To approve this journal, follow this link:
http://<?=$siteUrl?>/compjournal.php?action=approveJournal&k=<?=$k?>
<?php endif; ?>


<?php if($alreadyApproved): ?>
To unapprove this journal, follow this link:
http://<?=$siteUrl?>/compjournal.php?action=unapproveJournal&k=<?=$k?>
<?php endif; ?>


<?php if(!$alreadyFeatured): ?>
To feature this journal, follow this link:
http://<?=$siteUrl?>/compjournal.php?action=featureJournal&k=<?=$k?>
<?php endif; ?>




Many thanks,

<?=$emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>