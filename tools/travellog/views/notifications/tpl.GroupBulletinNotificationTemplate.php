<?php
	if($source->postedAsStaff()){
		$extra = 'Your staff, ' . $source->getName() . ' posted';
	}
	else if($source->postedAsAdministrator()){
		$extra = 'Your administrator has posted';
	}
	else{
		$extra = ucfirst($source->getName()) . ' posted';
	}	
	$extra .= (count($messages) > 1 ? ' new bulletin messages ' : ' a new bulletin message ') . 'for the ' . (($group instanceof AdminGroup) ? 'group ':'club ') . $group->getName();
?>
<? /* HTML  ?>
Hi <?= ucfirst($recipient) ?>,<BR><BR>

<?= $extra ?>. The latest message was posted <?= $time ?>. <BR>
To read the post, please follow the <?= (count($messages) > 1 ? "links" : "link") ?> below:<BR><BR> 
<? 	foreach($messages as $message):?>
		<a href="http://<?=$siteUrl?>/bulletinmanagement.php?groupID=<?=$group->getGroupID()?>&hlEntryID=<?=$message->getAttributeID()?>"> <?=$message->getTitle()?> </a><br/>
<?	endforeach;?>

<BR><BR>
Many thanks, <BR><BR>
<strong>The <?=$siteName?> Team </strong>
<br /> <a href = 'http://<?=$siteUrl?>'> http://<?=$siteUrl?> </a> 
<? /* end HTML */?>
<? /* Text */?>
Hi <?= ucfirst($recipient)?>,


<?=$extra?>.
<?/*To read the post, please follow the <?= (count($messages) > 1 ? "links" : "link") ?> below:*/?>
<?$links = array()?>

<?foreach($messages as $iMessages):?>
<?if($iMessages['isMessageIncluded']):?>

======================================================
Title: <?=$iMessages['message']->getTitle()?>

Message:
<?=$iMessages['message']->getText()?>

======================================================
To view this post, follow the link below:
http://<?=$siteUrl?>/bulletinmanagement.php?groupID=<?=$group->getGroupID()?>&hlEntryID=<?=$iMessages['message']->getAttributeID()?>


<?else:?>
<?$links[] = "http://$siteUrl/bulletinmanagement.php?groupID=" . $group->getGroupID() . "&hlEntryID=" . $iMessages['message']->getAttributeID()?>
<?endif;?>
<?endforeach;?>
<?if(count($links) > 0):?>
<?= (count($links) == count($messages) ? (count($links) > 1 ? 'To read these' : 'To read this' ) : chr(10) . 'To read other')?> <?=count($links) > 1 ? 'posts, follow the links':'post, follow the link'?> below:

<?=implode(chr(10),$links)?>
<?/*http://<?=$siteUrl?>/bulletinmanagement.php?hlEntryID=<?=?>*/?>


<?endif;?>

<?/*To post a new bulletin message for the <?=(($group instanceof AdminGroup) ? 'group ':'club ') . $group->getName()?>, please follow the link below:
http://<?=$siteUrl?>/bulletinmanagement.php?method=compose&groupID=<?=$group->getGroupID()?>*/?>

Many thanks,


<?=$siteName?>

http://<?=$siteUrl?>
<? /* end Text*/?>
