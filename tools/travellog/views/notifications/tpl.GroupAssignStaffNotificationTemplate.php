Hi <?=$recipient?>,


The admin for your group <?=$parent->getName()?> has assigned you to be a staff of <?=$subgroup->getName()?>. 


To visit <?=GaString::makePossessive($subgroup->getName())?> homepage, please follow the link below:
http://<?=$siteUrl?>/group.php?gID=<?=$subgroup->getGroupID()?>



Many thanks,


<?php echo $emailName?>

http://<?=$siteUrl?>




<?php echo $notificationFooter ?>