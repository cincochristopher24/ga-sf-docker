Hi <?= $recipientName ?>,

<?php if(count($journalEntries) && count($journalPhotos)): ?>
Your friend has added a <?php if(1==count($journalEntries)): ?> a <?php endif; ?>new journal <?php if(1==count($journalEntries)): ?>entry<?php else: ?>entries<?php endif; ?>
and has uploaded <?php if(1==count($journalPhotos)): ?> a <?php endif; ?> new <?php if(1==count($journalEntries)): ?> photo <?php else: ?> photos <?php endif; ?> on GoAbroad.net
<?php elseif(count($journalEntries)): ?>
Your friend has added a <?php if(1==count($journalEntries)): ?> a <?php endif; ?>new journal <?php if(1==count($journalEntries)): ?>entry<?php else: ?>entries<?php endif; ?> on GoAbroad.net
<?php elseif(count($journalPhotos)): ?>
Your friend has uploaded <?php if(1==count($journalPhotos)): ?> a <?php endif; ?> new <?php if(1==count($journalEntries)): ?> photo <?php else: ?> photos <?php endif; ?> on GoAbroad.net
<?php endif;?>


===============================================

<?php if(count($journalPhotos)): ?>
New Journal <?php if(1==count($journalEntries)): ?>Photo<?php else: ?>Photos<?php endif; ?> of <?= $subjectName ?>:  

<?php foreach($journalPhotos as $iJournalPhoto): ?>
<?= $iJournalPhoto ?>
<?php endforeach;?>
<?php endif;?>

<?php if(count($journalEntries)): ?>
New Travel Journal <?php if(1==count($journalEntries)): ?>Entry<?php else: ?>Entries<?php endif; ?> of <?= $subjectName ?>:  

<?php foreach($journalEntries as $iJournalEntry): ?>
<?= $iJournalEntry ?>
<?php endforeach;?>
<?php endif;?>