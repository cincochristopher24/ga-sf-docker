<h2><span>Step 2:</span><span class="subtle">&nbsp;Process Photos</span></h2>
<div class="content">
	<form name="frmCustomPageHeaderUpload" id="frmCustomPageHeaderUpload" method="post" action="/custom_pageheader.php" >
		<ul class="form" id="custom_home_form">
			<li class="form_item">
				<label for="home_photos">Upload your Photos:</label>
				<p class="supplement">
					Upload your preferred photos from your computer. In case you want to add more photo just click "Add Another Photo". Ideally 3-10 photos would be good.
				</p>
				<ul class="indented_form" id="up_photo">
					<?	
						$rand_id = uniqid();
						if( 0 < count($customPhotos) ): ?>
						<?	
							require_once("travellog/views/custom_pageheader/Class.ViewUploadedCustomPageHeader.php");
							foreach($customPhotos AS $custom):
								$customTpl = new ViewUploadedCustomPageHeader($custom);
								$customTpl->render();
							endforeach;
						?>
					<?	else: ?>	
						<li id="uploaded_item_<?=$rand_id?>">
							<div class="preview_thumb">
								<img id="uploaded_img_<?=$rand_id?>" src="/images/v3/cobrand/small-preview.jpg" alt="Preview Thumb Will Be Displayed Here"/>
							</div>
							<form id="frmUpload_<?=$rand_id?>" method="post" enctype="multipart/form-data" name="frmUpload_<?=$rand_id?>" >
								<span id="upload_area_<?=$rand_id?>">
									<span id="loading_anim_<?=$rand_id?>" style="display:none;"><img src="/images/loading_small.gif" /><em>Uploading image...</em></span>
									<span id="file_element_<?=$rand_id?>">
										<input id="home_photos_" name="home_photos_<?=$rand_id?>" type="file" onchange="customPageHeader.validateImageFile(this,'<?=$rand_id?>'); return false;" />
									</span>
									<input type="button" id="upload_file_<?=$rand_id?>" value="Upload" style="display:none;" onclick="customPageHeader.ajaxFileUpload(this); return false;"/>
									<span id="file_error_notice_<?=$rand_id?>" style="display:none;"></span>
								</span>
							</form>
							<div class="photodesc_label">Photo Description:</div>
							<input class="text wide" id="home_photo_description_<?=$rand_id?>" name="home_photo_description_<?=$rand_id?>" type="text" />
							<div id="photo_control_<?=$rand_id?>" class="photo_crop" style="display: none;"> 
								<a id="crop_<?=$rand_id?>" href="" title="" onclick="return false;">Crop</a> or <a id="delete_<?=$rand_id?>" href="" title="" onclick="return false;">Delete</a>
								<img id="requesting_delete_<?=$rand_id?>" src="/images/loading_small.gif" style="display:none;" />
							</div>
							<div class="photo_crop" name="empty_box" id="empty_box_<?=$rand_id?>" style="display:none;">
								<input type="hidden" id="uploading_<?=$rand_id?>" value="0" />
								<a id="lnkDeleteEmptyBox_<?=$rand_id?>" href="" onclick="customPageHeader.deleteEmptyBox(this); return false;">Delete</a>
							</div>
						</li>
					<?	endif; ?>											
				</ul>
			
				<li class="form_item" id="upload_file_control">
					<a id="add_uploadbox" href="javascript:void(0)" title="" onclick="customPageHeader.addUploadBox(); return false;">
						<span>Add Another Photo</span>
					</a>																	
				</li>
			</li>				
		
		
			<li class="form_item actions">
				<input type="hidden" name="step" value="2" />
				<input type="hidden" id="action" name="action"	value="PROCEED_UPLOAD_STEP3" />
				<input id="btnProceedToStep3" type="submit" value="Proceed to Step 3: Preview and Save" disabled="true"/>
				<span id="btnPrevStep">
					or &nbsp; <a href="/custom_pageheader.php?action=VIEW&step=1" onclick="jQuery('#btnProceedToStep3').attr('disabled',true); jQuery('#btnPrevStep').hide(); jQuery('#btnCancel').hide();" title="">&larr; Go Back to the Previous Step</a>						
				</span>

				<p id="btnCancel" style="border-top: 1px solid #dfdfdf; padding:10px 0; margin-top: 5px;">
					If you dont want to save any of your changes, click <strong> <a href="javascript:void(0)" title="" onclick="jQuery('#btnPrevStep').hide(); jQuery('#btnCancel').hide(); customPageHeader.cancelHandpicked(); return false;">Cancel</a> </strong>
				</p>
			</li>
		
		</ul>					
	</form>
	<?/*
		IDs:
		upload_template
		uploaded_item_X
		uploaded_img_X
		home_photos_X
		upload_file_X
		photo_control_X
		file_error_notice_X
		frmUpload_X
	*/?>
	<!--div id="upload_template" style="display:none;"-->
		<ul id="upload_template" style="display:none;">
			<li id="uploaded_item_">
				<div class="preview_thumb">
					<img id="uploaded_img_" src="/images/v3/cobrand/small-preview.jpg" alt="Preview Thumb Will Be Displayed Here"/>
				</div>
				<form id="frmUpload_" method="post" enctype="multipart/form-data" name="frmUpload_" >
					<span id="upload_area_">
						<span id="loading_anim_" style="display:none;"><img src="/images/loading_small.gif" /><em>Uploading image...</em></span>
						<span id="file_element_">
							<input id="home_photos_" name="home_photos_" type="file" onchange="customPageHeader.validateImageFile(this,'param_id'); return false;" />
						</span>
						<input type="button" id="upload_file_" value="Upload" style="display:none;" onclick="customPageHeader.ajaxFileUpload(this); return false;"/>
						<span id="file_error_notice_" style="display:none;"></span>
					</span>
				</form>
				<div class="photodesc_label">Photo Description:</div>
				<input class="text wide" id="home_photo_description_" name="home_photo_description_" type="text" />
				<div id="photo_control_" class="photo_crop" style="display: none;"> 
					<a id="crop_" href="" title="" onclick="return false;">Crop</a> or <a id="delete_" href="" title="" onclick="return false;">Delete</a>
					<img id="requesting_delete_" src="/images/loading_small.gif" style="display:none;" />
				</div>
				<div class="photo_crop" name="empty_box" id="empty_box_" >
					<input type="hidden" id="uploading_" value="0" />
					<a id="lnkDeleteEmptyBox_" href="" onclick="customPageHeader.deleteEmptyBox(this); return false;">Delete</a>
				</div>
			</li>
		</ul>
	<!--/div-->
	
</div>
<!-- end for content -->
<script type="text/javascript">
	customPageHeader.setPhotoCount(<?=count($customPhotos)?>);
	customPageHeader.emptyBoxes = <?=(0 < count($customPhotos))? 0 : 1?>;
</script>