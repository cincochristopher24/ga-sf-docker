<?php

	require_once("travellog/model/Class.CustomPageHeader.php");
	require_once("Class.Template.php");
	
	class ViewRotatingPhoto{
		
		private $mGroup = NULL;
		
		function setGroup($group=NULL){
			$this->mGroup = $group;
		}
		
		function render($isAdminLogged=FALSE){
			
			$tpl = new Template();
			
			$customPageHeader = new CustomPageHeader($this->mGroup->getGroupID());
			$photo = FALSE;
			if( $customPageHeader->exists() && $customPageHeader->isActive() ){
				$customPageHeader->setGroup($this->mGroup);
				$rotatingPhoto = $customPageHeader->getRandomPhoto();
				if( $rotatingPhoto ){
					$photo = $rotatingPhoto->getPhotoInstance();

					$traveler = FALSE;
					if( 0 != $rotatingPhoto->getTravelerID() ){
						if( $rotatingPhoto->getTravelerID() != $this->mGroup->getAdministratorID() ){
							require_once("travellog/model/Class.Traveler.php");
							$traveler = new Traveler($rotatingPhoto->getTravelerID());
							if( !$this->mGroup->isMember($traveler) ){
								$traveler = FALSE;
							}
						}
					}
					$tpl->set("traveler",$traveler);
				}
			}
			$tpl->set("adminLogged",$isAdminLogged);
			$tpl->set("customPageHeader",$customPageHeader);
			$tpl->set("photo",$photo);
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewRotatingPhoto.php");
		}
	}