<?php
	
	require_once("Class.Template.php");
	require_once('Class.Paging.php');
	require_once('Class.ObjectIterator.php');
	require_once('travellog/model/Class.AdminGroup.php');
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterOp.php');
	require_once('travellog/model/Class.FilterCriteria2.php');
	require_once("Class.dbHandler.php");
	
	class ViewCustomPageHeaderStep_FETCH_2{
		
		private $mRpp	=	15;
		private $mPage	=	1;
		private $mFirst	=	0;
		private $mLast	= 	0;
		private $mMode	=	"ALL";//valid values: ALL,RECENT_UPDATE,SELECTED
		private $mSelectedJournals = array();
		private $mTitle = '';
		
		private $mCustomPageHeader = NULL;
		
		private $mJournalsCount = 0;
		private $mJournals = array();
		
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
		}
		function setPage($page=1){
			$this->mPage = $page;
		}
		function setDisplayMode($mode="ALL"){
			$this->mMode = $mode;
		}
		function setSelectedJournals($journals=array()){
			$this->mSelectedJournals = $journals;
		}
		function setTitle($title=""){
			$this->mTitle = $title;
		}
		
		function render(){
			$this->searchJournals();

			$tpl = new Template();
			$tpl->set("journalsCount",$this->mJournalsCount);
			$tpl->set("controller",$this);
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewCustomPageHeaderStep_FETCH_2.php");
		}
		
		function renderJournalsList(){
			$paging = new Paging( $this->mJournalsCount, $this->mPage, '' , $this->mRpp );
			$paging->setOnclick("customPageHeader.fetchJournalsWithQualifiedPhotos");
			$iterator = new ObjectIterator( $this->mJournals, $paging );
			
			$tpl = new Template();
			$tpl->set("journals",$this->mJournals);
			$tpl->set("journalsCount",$this->mJournalsCount);
			$tpl->set("paging",$paging);
			$tpl->set("first",$this->mFirst);
			$tpl->set("last",$this->mLast);
			$tpl->set("mode",$this->mMode);
			$tpl->set("iterator",$iterator);
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewQualifiedJournalsList.php");
		}
		
		function searchJournals(){
			$orderBy = FilterOp::$ORDER_BY;
			$orderField = "tblTravel.title";
			if ( 'RECENT_UPDATE' == $this->mMode  ){
				$orderField = "tblTravel.lastupdated";
			}
			
			$filter = NULL;
			if( "" != $this->mTitle ){
				$db = new dbHandler();
				$title_like = $db->makeSqlSafeString("%".$this->mTitle."%");
				$cond = new Condition;
				$cond->setAttributeName("tblTravel.title");
				$cond->setOperation(FilterOp::$LIKE);
				$cond->setValue("$title_like");		
				$filter = new FilterCriteria2();
				$filter->addCondition($cond);
			}
			
			$cond = new Condition;
			$cond->setAttributeName($orderField);
			$cond->setOperation($orderBy);
			$order = new FilterCriteria2();
			$order->addCondition($cond);
			$journals = $this->mCustomPageHeader->getGroup()->getJournalsWithQualifiedPhotos($order,$filter,$this->mSelectedJournals);
			$this->mJournalsCount = count($journals);
			$this->mJournals = $this->limitJournals($journals,$this->mPage,$this->mRpp);
			
			if( 0 == count($this->mSelectedJournals) && "SELECTED" == $this->mMode ){
				$this->mMode = "ALL";
			}
		}
		
		function limitJournals($journals=array(), $page=1, $rpp=15){
			$temp = array();
			$count = count($journals);
			$offset = $this->calcOffset($page);
			for($i=$offset, $ctr=1; $i<$count && $ctr <= $rpp; $i++,$ctr++ ){
				$this->mFirst = $offset+1;
				$this->mLast = 1 + $i;
				$temp[] = $journals[$i];
			}
			return $temp;
		}
		
		private function calcOffset($page=1){
			return ($page-1) * $this->mRpp;
		}
		
	}