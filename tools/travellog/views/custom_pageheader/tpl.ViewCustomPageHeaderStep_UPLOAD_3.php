<h2><span>Step 3:</span><span class="subtle">&nbsp;Preview and Save</span></h2>
<div class="content">
	<div id="gallery_view" class="cobrand_feat">
		<div id="intro">
			<!-- start rotating photo -->
			<img id="rotating_custom_header_photo" src="<?=$customPhoto->getPhotoInstance()->getPhotoLink('customheaderstandard')?>" alt="Featured Photos" title="Featured Photo" height="324" width="980" />
			
			<div class="simulate_refresh">
				<div id="btnRefreshRandom">
					<input type="button" value="Refresh" name="simulate_refresh" onclick="customPageHeader.fetchRandomPhoto(); return false;" />
					<p>Click to simulate homepage refresh</p>
				</div>
				<img id="refreshingRandom" src="/images/load_smallBgBlack.gif" style="display: none;"/>
			</div>
			
			<div id="extra_image_overlay" class="featured_photo">
				<h1 class="ganet_slogan">Share your journeys with the world!</h1>	
				<p id="photo_caption_area" class="photo_caption" <?if(""==$customPhoto->getCaptionTemp()):?>style="display:none;"<?endif;?> >
					<img src="/images/v3/cobrand/quot.png" alt="" title="" />
					<span id="rotating_custom_header_photo_caption">
						<?=htmlspecialchars(stripslashes(strip_tags($customPhoto->getCaptionTemp())),ENT_QUOTES)?>
					</span>
					<img src="/images/v3/cobrand/quot-close.png" alt="" title="" />
				</p>				
			</div>			
		</div>
		
		<div class="cobrand_welcome">
		
			<div class="wide">
				<h1><?=htmlspecialchars(stripslashes(strip_tags($customPageHeader->getWelcomeTitleTemp())),ENT_QUOTES)?></h1>
				<p> 
					<?=htmlspecialchars(stripslashes(strip_tags($customPageHeader->getWelcomeTextTemp())),ENT_QUOTES)?> 
				</p>
			</div>
			
			<?	if( $customPageHeader->getDirectSignupTemp() == 0 ): ?>
				<div class="narrow" id="feature_ganet" style="display: block;">
					<a href="http://goabroad.net" title="GoAbroad Network - The network for travellers!" id="cobrand_signup">GoAbroad Network</a>
					<p>This site is proudly powered by the GoAbroad Network.</p>				
				</div>
			<?	else: ?>						
				<div class="narrow" id="signup_ON" style="display: block;">
					<a href="#" title="Sign-Up Fast and Easy!" >Sign-Up</a>
					<strong> Absolutely Free!</strong>
					<p>Sign Up fast and easy.</p>				
				</div>									
			<?	endif; ?>							
		</div>		
		
		<div class="form_item actions">
			<form id="frmHomepageTexts" method="post" action="/custom_pageheader.php">
				<div class="actions">
					<input type="hidden" id="action" name="action" value="SAVE_CUSTOMIZATION" />
					<input id="btnSave" type="submit" value="Save Homepage Customization" />
					<span id="btnPrevStep">
						or &nbsp; <a href="/custom_pageheader.php?action=VIEW&step=2" onclick="jQuery('#btnSave').attr('disabled',true); jQuery('#btnPrevStep').hide(); jQuery('#btnCancel').hide();" title="">&larr; Go Back to the Previous Step</a>						
					</span>
				</div>
			</form>
			
			<p id="btnCancel" style="border-top: 1px solid #dfdfdf; padding:10px 0; margin-top: 5px;">
				If you dont want to save any of your changes, click <strong> <a href="" title="" onclick="customPageHeader.cancelCustomization(); return false;">Cancel</a> </strong>
			</p>
			
		</div>							
		
		
	</div>								
</div>