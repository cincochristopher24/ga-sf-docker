<?php
	require_once("Class.Template.php");
	require_once("travellog/views/custom_pageheader/Class.ViewCustomPageHeaderStepsNav.php");
	require_once("travellog/views/custom_pageheader/Class.ViewCustomPageHeaderSection.php");
	
	class ViewCustomPageHeader{
		
		private $mParams	=	array( 	"action"	=>	"VIEW",
										"step"		=>	1);

		private $mCustomPageHeader	=	NULL;
		private $mCustomPageHeaderPhotos	=	array();
		
		function setParams($params=array()){
			$this->mParams = array_merge($this->mParams,$params);
		}
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
		}
		
		function render(){
			$section = new ViewCustomPageHeaderSection();
			$section->setParams($this->mParams);
			$section->setCustomPageHeader($this->mCustomPageHeader);
			$section->createSection();
			
			$steps_nav = new ViewCustomPageHeaderStepsNav();
			$steps_nav->setStep($this->mParams["step"]);
			
			$tpl = new Template();
			$tpl->set("step",$this->mParams["step"]);
			$tpl->set("steps_nav",$steps_nav);
			$tpl->set("section",$section);
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewCustomPageHeaderContent.php");
		}
	}