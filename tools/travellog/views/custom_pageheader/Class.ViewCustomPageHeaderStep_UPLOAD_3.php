<?php
	
	require_once("Class.Template.php");
	
	class ViewCustomPageHeaderStep_UPLOAD_3{
		
		private $mCustomPageHeader = NULL;
		
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
			$params = array("deleted"	=>	FALSE,
							"photoOption" => $this->mCustomPageHeader->getPhotoOptionTemp() );
			if( 0 == count($this->mCustomPageHeader->getCustomPageHeaderPhotos($params)) ){
				header("Location: /custom_pageheader.php?action=VIEW&step=2");
			}
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("customPageHeader",$this->mCustomPageHeader);
			$tpl->set("customPhoto",$this->mCustomPageHeader->getRandomPhoto(TRUE));
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewCustomPageHeaderStep_UPLOAD_3.php");
		}
		
	}