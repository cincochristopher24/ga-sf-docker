<?php
	
	require_once("Class.Template.php");
	
	class ViewCustomPageHeaderStep_FILTERUSED_2{
		
		private $mCustomPageHeader = NULL;
		
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
		}
		
		function render(){
			$tpl = new Template();
			$params = array("deleted"			=>	FALSE,
							"ignorePhotoOption"	=>	TRUE);

			$tpl->set("customPageHeader",$this->mCustomPageHeader);
			$tpl->set("customPhotos",$this->mCustomPageHeader->getCustomPageHeaderPhotos($params));
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewCustomPageHeaderStep_FILTERUSED_2.php");
		}
		
	}