<?php
	
	require_once("Class.Template.php");
	
	class ViewCustomPageHeaderStep_FILTERUSED_3{
		
		private $mCustomPageHeader = NULL;
		
		function setCustomPageHeader($pageHeader=NULL){
			$this->mCustomPageHeader = $pageHeader;
			$params = array("deleted"		=>	FALSE,
							"isFiltered"	=>	TRUE,
							"ignorePhotoOption"	=>	TRUE,
							"isSaved"		=>	TRUE);
			if( 0 == count($this->mCustomPageHeader->getCustomPageHeaderPhotos($params)) ){
				header("Location: /custom_pageheader.php?action=VIEW&step=1");
			}
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("customPageHeader",$this->mCustomPageHeader);
			$tpl->set("customPhoto",$this->mCustomPageHeader->getRandomPhoto($customizing=NULL,$filtered=TRUE));
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewCustomPageHeaderStep_FILTERUSED_3.php");
		}
		
	}