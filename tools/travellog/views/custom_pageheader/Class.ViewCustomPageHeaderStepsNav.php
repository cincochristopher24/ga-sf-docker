<?php
	require_once("Class.Template.php");
	
	class ViewCustomPageHeaderStepsNav{
		
		private $mStep = 1;
		
		function setStep($step=1){
			$this->mStep = $step;
		}
		
		function render(){
			$tpl = new Template();
			$tpl->set("step",$this->mStep);
			$tpl->out("travellog/views/custom_pageheader/tpl.ViewCustomPageHeaderStepsNav.php");
		}
	}