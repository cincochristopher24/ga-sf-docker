<h2><span>Step 2:</span><span class="subtle">&nbsp;Process Photos</span></h2>
<div class="content">

	<div class="form" id="custom_home_form">
	
		<div class="block_header">
			<div class="text">
				<strong>Select the journals for your homepage photo	</strong>
				<p class="sub_text">Add photos to your pool by selecting any journals below.</p>						
			</div>
			
			<div class="search_box">						
				<input id="journal_search_tbox" type="text" value="" />							
				<a id="journal_search_btn" href="" title="" <?if(0==$journalsCount):?>onclick="return false;"<?else:?>onclick="customPageHeader.searchJournalsByTitle(); return false;"<?endif;?> >Search</a>
			</div>
		</div>
		
		<div id="journals_list">
			<?=$controller->renderJournalsList()?>
		</div>				
	
		<div class="form_item actions">
			<form name="frmJournalsSelection" id="frmJournalsSelection" method="post" action="/custom_pageheader.php" onsubmit="customPageHeader.prepareSelection();" >
				<input type="hidden" id="selection" name="selection" value="" />
				<input type="hidden" name="action" value="VIEW_PHOTOS_TO_FETCH" />
				<input type="hidden" name="step" value="3" />
				<input id="btnSubmitSelection" type="submit" value="Proceed to Step 3: Preview and Save" />
				or &nbsp; 
				<a href="/custom_pageheader.php?action=VIEW&step=1" title="">&larr; Go Back to the Previous Step</a>						
			</form>
		</div>										
	
	</div>					
	<script type="text/javascript">
		customPageHeader.updateSelectedJournalsCount();
	</script>
</div>