<?	if( $privileged ): ?>
	<div class="prompt crop_box" id="popup" style="top: 276px; left: -103px;">
		<table class="table_popbox">
			<tbody>
				<tr>
					<td class="popbox_topLeft"/>
						<td class="popbox_border"/>
						<td class="popbox_topRight"/>
				</tr>
				<tr>
					<td class="popbox_border"/>
					
						<td class="popbox_content">
							<h4 class="header" id="popup_header">
								<strong style="color: #fff;">Crop Photo</strong>
							</h4>
						
							<div class="crop_box_content">
								<div id="crop_view">
									<img id="cropbox" src="<?=$source?>" width="<?=$fullWidth?>" height="<?=$fullHeight?>"/> 
								</div>
								<!--div id="crop_area">
									<img alt="Preview Thumb" height="312" width="500" src="http://docs.goabroad.com/goabroad.net/Mockups/Cobrand%20Custom%20Home/scratch%20files/cropper500x312.png"/>						
								</div-->
							
								<div id="preview">
								
								</div>
								
								<div> </div>
								<div id="preview_text">
									<p>Your cropped photo will look like this</p>
								
									<br />
									<div>
										<strong> Cropping Dimension</strong>
									</div>
									<br />									
									<span class="box_w"><?//=$minWidth?>980px</span>&nbsp;x&nbsp;<span class="box_h"><?//=$minHeight?>324px</span>								
									<br />
									<img id="crop_requesting" src="/images/load_gray.gif" style="display:none;" />
								</div>								
																						
							</div>						
						
							<div class="buttons_box" id="popup_buttons">
								<input type="button" value="Crop Photo" class="prompt_button" onclick="customPageHeader.cropPhoto(<?=$photoID?>); return false;" id="a_submit"/>
								<input type="button" value="Cancel" onclick="customPageHeader.closeCropper(); return false;" class="prompt_button" id="a_cancel"/>
							</div>
						</td>
					
						<td class="popbox_border"/>
				</tr>
				<tr>
					<td class="popbox_bottomLeft"/>
					<td class="popbox_border"/>
					<td class="popbox_bottomRight"/>
				</tr>
			</tbody>
		</table>
		<form id="frmCropper">
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="width" name="width" />
			<input type="hidden" id="height" name="height" />
		</form>
	</div>
	<script type="text/javascript">
		customPageHeader.initCropper(<?=$minWidth?>,<?=$minHeight?>);
	</script>
<?	else: ?>
	<script type="text/javascript">
		self.parent.tb_remove(this);
	</script>
<?	endif; ?>