<?php
require_once('Class.HtmlHelpers.php');
?>
<div id="profile_info" class="section">
<h2><span>About the Group</span></h2>
<div class="content">
	<a href="<?=$obj_helper->getLink($contents['obj_profile'])?>" class="group_emblem" title="View Profile">
		<?if( strlen($contents['obj_profile']->getPhoto()) ): ?>
			<img src="<?=$contents['obj_profile']->getPhoto()?>" alt="Emblem" />
		<? endif; ?>
	</a>

	<h3 class="advisor"><a href="<?=$obj_helper->getLink($contents['obj_profile'])?>" title="View this Group's Profile"><?= HtmlHelpers::truncateWord($contents['obj_profile']->getName(), 14); ?></a></h3>

	<div class="profile_info meta">
		<?if( $contents['obj_profile']->getMembers() ) : ?>
			<div class="members"><strong>Members:</strong> <?=$contents['obj_profile']->getMembers()?></div>
		<? endif; ?>
		<?if( strlen($contents['obj_profile']->getMission()) ):?>
			<div class="mission_statement">
				<strong>Mission Statement:</strong> <br /> 
				<?= HtmlHelpers::TruncateText($contents['obj_profile']->getMission(), 20) ?>
			</div>
		<? endif; ?>
	</div>
	<div class="clear"></div>	
</div>
<div class="foot"></div>
</div>
 
