<?php
/*
 * Created on 10 24, 2006
 *
 * @author Kerwin Gordo
 * Purpose: subtemplate for groups of a traveler (used in profile.php)
 */


?>

<div class="section" id="groups">
	<h2>
	  <span>Groups<?php if ($showGroupCount) echo '('.$groupCount.')'; ?></span>
	  <? if (5 < $groupCount): ?>
	    <span class="header_actions">
	      <a href="/group.php?travelerID=<?=$traveler->getTravelerID()?>">View All Groups</a>
	    </span>
	  <? endif; ?>
	</h2>
	
	<div class="content">

		<? if ($groups)   : ?>
			<ul id="groups_container" class="groups">
					<? foreach($groups as $group) : ?>
						<li>
							<a class="thumb" href="/group.php?gID=<?=$group->getGroupID()?>" title="Visit the travel community <?=$group->getName() ?>">
									<img  class="pic" src="<?=$group->getGroupPhoto()->getPhotoLink('thumbnail')?>" height="65" width="65" alt="Travel community <?=$group->getName() ?>" />
							</a>
							
							<div class="details">
								<strong><a class="groupname" href="/group.php?gID=<?=$group->getGroupID()?>">  <?= $group->getName()?> </a></strong>
								
								<?/* if ($group->getAdministrator()->getTravelerID() == $traveler->getTravelerID()):?>
									<span class="meta"><strong>(&nbsp;Admin&nbsp;)</strong></span>	
								<? endif;*/?>
								
							</div>
							
							<div class="clear"></div>	
						</li>
					<? endforeach; ?>						    
			</ul>
		
		<? else : ?>
	    	<p class="side_help_text">
	    		<?if($isowner):?>
	    			It appears that you are not yet a member of any group.
	    			<br />
		    		Why not <a href="/group.php"><strong>Join</strong></a> one or <a href="/group.php?mode=add"><strong>Create</strong></a> your own?
		    	<? else : ?>
		    		<?=$traveler->getUserName()?> is not yet a member of any group.
	    		<? endif; ?>
	    	</p>			
		<? endif; ?>

	</div>
	
	<div class="foot"></div>

</div>
