<?php
	/*
	 * tpl.ViewAdvisorCommunications.php
	 * Created on Dec 12, 2007
	 * created by marc
	 */
	$contents['subNavigation']->show();  
?>
	<div id="admin_message_view" class="header">
		<div id="main" class="area">
			<div class="section">
				<div class="content">
					<h1>Participant Inquiries</h1>
					<div id="comm_page">
						<?	 
							if( strlen($contents['errString']) ) 
								echo $contents['errString'];
							else 
								echo $sub_views['MESSAGE_LIST']->render();
						?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>			
	</div>	
<?   
	if( strlen($contents['errString']) ){
		header("Refresh: 3; URL=http://" . $_SERVER['SERVER_NAME'] . "/passport.php");	
	}
?>