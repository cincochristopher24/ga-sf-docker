<?if (count($arrResultEmail)) : ?>
	<p>The following email addresses were not found in our member list YET. This could simply be because your group members have not yet joined the GoAbroad Network. 
	We suggest you double check the email addresses and select the ones you want us to store for future use. 
	Once a new member joins using one of these email addresses, a match will automatically be made and the new member will immediately be prompted to instantly join your group. 
	In addition, an email will be sent to each of these email addresses requesting membership with instructions to make it easy to join along with a request from <strong><em><?= $group->getName()?></em></strong>.</p>
	<ul class="users">
		<? foreach($arrResultEmail as $eachEmail) : ?>
		<li>	
			<?= $eachEmail	?>		
			|											
			<? if ($group->isInEmailInviteList($eachEmail)) : ?>
				Pending Invitation Approval |
				<a href="javascript:void(0)" onclick="manager._setPanelNum(2);manager._cancelEmail('&amp;searchType=<?=$searchType?>&amp;searchKey=<?=$searchKey?>&amp;searchKey2=<?=$searchKey2?>&amp;mode=cancelEmail&amp;email=<?=$eachEmail?>&amp;grpID=<?=$group->getGroupID()?>');">Cancel Invitation</a>
			<? else : ?>
				<a href="javascript:void(0)" onclick="manager._setPanelNum(2);manager._inviteEmail('&amp;searchType=<?=$searchType?>&amp;searchKey=<?=$searchKey?>&amp;searchKey2=<?=$searchKey2?>&amp;mode=inviteEmail&amp;email=<?=$eachEmail?>&amp;grpID=<?=$group->getGroupID()?>');">Invite to Join Group</a>									
				<? if (1 < count($arrResultEmail)) : ?>
					| <input type="checkbox" name="multiEmail[]" value="<?=$eachEmail?>" checked>	Invite				
					<? $showConfirm1 = true ?>	
				<? endif; ?>
			<? endif; ?>									
			
		</li>
		<?endforeach; ?>
	</ul>	
	<? if ($showConfirm1) : ?>
		<a href="javascript:void(0)" onClick="manager._setPanelNum(2);manager._inviteEmailMultiple('&amp;searchType=<?=$searchType?>&amp;searchKey=<?=$searchKey?>&amp;searchKey2=<?=$searchKey2?>&amp;mode=inviteEmailMultiple&amp;grpID=<?=$group->getGroupID()?>')">Confirm Selected </a>
	<? endif ; ?>

<?endif;?>

<? if (count($arrResultID)) : ?>
	<p>The following matches were found. Please click confirm to add all checked matches to the group.</p>
	

	<? foreach($arrResultID as $eachTravID) : ?>
		<?	$traveler = new Traveler($eachTravID);							
			$eachprofile = $traveler->getTravelerProfile(); 
			
			if ($group->isMember($traveler)):
				$travelerType = 'member';
			elseif ($group->isInInviteList($traveler)):
			 	$travelerType = 'invite';
			elseif ($group->isInRequestList($traveler)):
			 	$travelerType = 'request';
			elseif ($group->isInDeniedInviteList($traveler)):
			 	$travelerType = 'deniedinvite';
			else:
				$travelerType = 'traveler';				
			endif;
			
			$entryJournal = array();
		 	if( 0 < $traveler->getCountTravelJournals() ){
	   			( 1 < $traveler->getCountTravelJournals() ) ? $str = " Journals" : $str = " Journal";
	   			array_push($entryJournal,$traveler->getCountTravelJournals() . $str );		
	   		}
		 	if(  0 < $traveler->getCountTravelLogs() ){
	   			( 1 < $traveler->getCountTravelLogs() ) ? $str = " Entries" : $str = " Entry";
	   			array_push($entryJournal,$traveler->getCountTravelLogs() . $str );		
	   		}
			$mySubGroupList = array();
			if (isset($subGroupList))
				$mySubGroupList = $subGroupList[$traveler->getTravelerID()];
				
			//set privacy for profile (email);
			$canviewemail	= $traveler->getPrivacyPreference()->canViewEmail($group->getAdministratorID()); 
			
			?>
		 	<div class="traveler">
				
				<? if ( isset($_SESSION['travelerID']) ): ?>
					<div class="user_stat">
					    <?
						$obj_pref = new PrivacyPreference( $eachTravID); 
						// if pref allows or the loggeduser is a member
						( ( $eachTravID == $_SESSION['travelerID']) || ( $traveler->isOnline() && $obj_pref->canViewOnlineStatus($_SESSION['travelerID']) ) ) ? $imgfile = 'orb_online.gif' : $imgfile = 'orb_offline.gif';
						$stat_alt = ('orb_online.gif' == $imgfile) ? 'online' : 'offline';
						?>
						<img src="/images/<?=$imgfile?>" alt="<?=$stat_alt?>" title="<?=$stat_alt?>" />
					</div>
				<? endif; ?>
				
				<div class="mem_stats">
					<div class="flags">
						<? if ($eachprofile->getHTLocationID()): ?>
						 <? $cID = $eachprofile->getCity()->getCountry()->getCountryID()  ?>
							<img src="http://images.goabroad.com/images/flags/flag<?=$cID?>.gif" width="22" height="11" alt="Travel blog from <?= $eachprofile->getCity()->getCountry()->getName(); ?>" style="margin-bottom:1px;"/>												
						<? endif; ?>
					</div>
						
					<div class="status">
						<?php
							if ('member' == $travelerType ):
								echo '(member)';
							elseif ('invite' == $travelerType):
								echo '(invites)';
							elseif ('request' == $travelerType):
								echo '(requests)';
							endif;								
						?>
					</div>
				
				</div>
				
				<a href="/<?= $eachprofile->getUserName()?>" class="user_thumb">	
					<img src="<?= $eachprofile->getPrimaryPhoto()->getPhotoLink('thumbnail') ?>" height="37" width="37" />																
				</a>
			
				<h2>
					<a href="/<?= $eachprofile->getUserName() ?>" title="<?= $eachprofile->getUserName() ?>">
						<?= (strlen($eachprofile->getUserName()) > 24) ? substr($eachprofile->getUserName(),0, 24).'...' : $eachprofile->getUserName() ?>
						<? if ( 'member' == $travelerType && (strlen($eachprofile->getFirstName()) || strlen($eachprofile->getLastName())) ) { 
							echo "( " . $eachprofile->getFirstName() . " " . $eachprofile->getLastName() .  " )";
						}?>						
					</a>
				</h2>
				
				<div class="profile_info meta"> 
					<div class="mem_statsprev">							
						<?= implode(" | ",$entryJournal) ;	?>
					</div>
					<div class="mem_option">
						<? if ('invite' == $travelerType) : ?>
							<a href="javascript:void(0)" onclick="manager._setPanelNum(2);manager._cancelName('&amp;searchType=<?=$searchType?>&amp;searchKey=<?=$searchKey?>&amp;searchKey2=<?=$searchKey2?>&amp;mode=cancelName&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;grpID=<?=$group->getGroupID()?>');">Cancel Invitation</a>
						<? elseif ('request' == $travelerType) : ?>
							<a href="/members.php?mode=accept&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$group->getGroupID()?>">Accept </a>|
							<a href="/members.php?mode=deny&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;gID=<?=$group->getGroupID()?>">Deny</a>
						<? elseif ('member' != $travelerType) : ?>												
							<? if (1 < count($arrResultID)) : ?>
								<input type="checkbox" name="multiID[]" value="<?=$eachprofile->getTravelerID()?>" checked>	Check to invite				
								<? $showConfirm2 = true ?>	
							<? endif; ?>
							<br />						
							<a href="javascript:void(0)" onclick="manager._setPanelNum(2);manager._inviteName('&amp;searchType=<?=$searchType?>&amp;searchKey=<?=$searchKey?>&amp;searchKey2=<?=$searchKey2?>&amp;mode=inviteName&amp;travelerID=<?=$eachprofile->getTravelerID()?>&amp;grpID=<?=$group->getGroupID()?>');">
								Invite this
							</a>							
						<? endif; ?>						
					</div>
				</div>
				<div class="mem_status">
					<br />
					<strong>
					<?	if (count($mySubGroupList) > 0) :
							echo (count($mySubGroupList) > 1 ) ? 'Groups: ' : 'Group: ' ;
							echo implode(", ",$mySubGroupList);
							echo "<br />";
						endif;
					?>	
					</strong>
				</div>			
			   </div>
			</div>
	<? endforeach; ?>
	

			
	<? if ($showConfirm2) : ?>
			<a href="javascript:void(0)" onclick="manager._toggleCheckBox()" class="button" >Check / Uncheck all</a>
			<a href="javascript:void(0)" onclick="manager._setPanelNum(2);manager._inviteNameMultiple('&amp;searchType=<?=$searchType?>&amp;searchKey=<?=$searchKey?>&amp;searchKey2=<?=$searchKey2?>&amp;mode=inviteNameMultiple&amp;grpID=<?=$group->getGroupID()?>')" class="button">Invite Selected </a>
	<? endif ; ?>
		
	
<? endif ; ?>