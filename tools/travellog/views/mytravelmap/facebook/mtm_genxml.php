<?php
function parseToXML($htmlStr) {
  $xmlStr=str_replace('<','&lt;',$htmlStr);
  $xmlStr=str_replace('>','&gt;',$xmlStr);
  $xmlStr=str_replace('"','&quot;',$xmlStr);
  $xmlStr=str_replace("'",'&#39;',$xmlStr);
  $xmlStr=str_replace("&",'&amp;',$xmlStr);
  return $xmlStr;
}

$mapEntries = $model['mapEntries'];

$xml = "<markers>";
foreach ($mapEntries as $entry) {
  $country = parseToXML($entry->getCountry()->getName());
  $lat = $entry->getCountry()->getLatitude();
  $lng = $entry->getCountry()->getLongitude();
  $pUrl = $entry->getPhoto();
  $jCnt = $entry->getJournalEntriesCount();
  if (!$jCnt) $jCnt = 0;
  $pCnt = $entry->getPhotosCount();
  if (!$pCnt) $pCnt = 0;
  $tCnt = $entry->getTipsCount();
  if (!$tCnt) $tCnt = 0;
  $withJournal = 'withJournal';

  $xml .= <<< Xml
    <marker ctry="$country" lat="$lat" lng="$lng" pUrl="$pUrl" jCnt="$jCnt" pCnt="$pCnt" tCnt="$tCnt" type="$withJournal" />
Xml;
}

$xml .= "</markers>";

header("Content-type: text/xml");

echo $xml;
?>