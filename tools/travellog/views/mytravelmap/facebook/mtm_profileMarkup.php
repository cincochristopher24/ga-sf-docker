<?php
$fbml ='<fb:wide>';

// CSS STYLE -->
$fbml .= <<< Style
<style>
  	#container {
		background: url(http://www.goabroad.net/facebook/mytravelmap/images/world_map_facebook.gif) #ffffff no-repeat;
	    color: #ffffff;
	    position: relative;
	    height: 226px;
	    width: 380px;
		overflow: hidden;
  	}

  	#container div {
    	position: absolute;
  	}

  	#container div img {
    	border-style: none;
  	}

  	#top_links {
	  	width: 368px;
	  	border: 1px solid #EFEFEF;
	  	margin: 5px 0;
	  	padding: 5px;
	  	text-align: right;
	  	font-weight: bold;
  	}
  
  	#wish {
		color: #777777;
		margin: 0 0 5px 0;
		border: 1px solid #e6e6e6;
		padding: 5px;
		font-size: 11px;
		margin-top: 5px;
		background-color: #fafafa;
  	}

  	#wish span {
		color: #60718B; 
		font-weight: bold;	
  	}

  	#ctryInfoDiv {
		color: #777777;
		margin: 5px 0 5px 0;
		border: 1px solid #e6e6e6;
		padding: 10px;
		background-color: #fafafa;
  	}

  	.imgTrans{
		filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
  	}

  	.blueMarker {
		width:12px;
		height:20px;
		filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinBlue');
  	}

  	.greenMarker {
		width: 12px;
		height: 20px;
		filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinGreen');
  	}

  	.yellowMarker {
		width: 12px;
		height: 20px;
		filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinYellow');
  	}

  	.shadowMarker {
		width: 10px;
		height: 17px;
		filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinShadow');
  	}     

  	.fLeft {
  		float:left;
  		padding: 0 5px;
  	}
Style;

// set the individual countries' markers position in our CSS
$map = new Map(array('x'=>380, 'y'=>380));
$entryCount = 0;
$countries = '';
$preselectedCtry = '';

// IF HAS MAP ENTRIES -->
if ($mapEntries) :
	
	// SET CSS FOR MAP ENTRIES -->
  	foreach ($mapEntries as $entry) :
	    $shadowDivID = 's'.$entryCount;
	    $markerDivID = 'c'.$entryCount;
	    $altMarkerDivID = 'ac'.$entryCount;

	    $coordinates = $map->lngLatToXYCoords($entry->getCountry()->getLongitude(), $entry->getCountry()->getLatitude());

	    $y = $coordinates['y'];
	    $x = $coordinates['x'] - 2;

		$showdowY = $y-1;
		$showdowX = $x+1;
	    $fbml .= '#'.$shadowDivID. '{
	      	top: '.$showdowY.'px;
	      	left: '.$showdowX.'px;
	      	z-index: 0
	    }';

	    $x++;
	    $fbml .= '#'.$markerDivID. '{
	      	top: '.$y.'px;
	      	left: '.$x.'px;
	      	z-index: 5
	    }';

	    $y++;
	    $fbml .= '#'.$altMarkerDivID. '{
	      	top: '.$y.'px;
	      	left: '.$x.'px;
	      	z-index: 5
	    }';
	    $entryCount++;
	
  	endforeach;
	// END OF SET CSS FOR MAP ENTRIES -->


	$fbml .='</style>';
	// END OF CSS STYLE -->


	$a = $_SERVER['REQUEST_URI'];
	// TOP LINKS -->
	$fbml .= <<< Links
	<input type='hidden' value='$a' name='test'>
	<fb:visible-to-owner>
  		<div style="text-align:right" id="top_links">
    		<a href="http://apps.facebook.com/my-travel-map/index.php?action=refreshProfile&box=1" id="refresh">Refresh</a>
			<span id="links_pipe">|</span>
    		<a href="http://apps.facebook.com/my-travel-map/index.php?id=$userID">Edit Country Lists</a>
  		</div>
    </fb:visible-to-owner>
Links;
	// END OF TOP LINKS -->



	// LEGEND ICONS FOR SYNCHRONIZED -->
	if(isset($isSynchronized) && $isSynchronized) {
		$fbml .= <<< Legend
		<ul style="margin:10px 0 35px 0;list-style:none; padding:0px;">
			<li class="fLeft">
				<div class="blueMarker fLeft"><img src="$pinBlue" class="imgTrans"/></div>Default
			</li>    
			<li class="fLeft">
				<div class="greenMarker fLeft"><img src="$pinGreen" class="imgTrans"/></div>Journals
			</li>
			<li class="fLeft">
				<div class="yellowMarker fLeft"><img src="$pinYellow" class="imgTrans"/></div>Current selection
			</li>
		</ul>
Legend;
	}
	// END OF LEGEND ICONS FOR SYNCHRONIZED -->



	// MAP DIV CONTAINER -->
	$fbml .= '<div id="container" onclick="dummyFunction()">';

  	$isCtrySelected = false; //watch out for $entryCount
	$entryHelper = new EntryHelper;	

	// $entryCount FOR LOOP -->
  	for ($i = 0; $i<$entryCount; $i++) :
		$e = $mapEntries[$i];
		$ctryName = str_replace("&",'&amp;',$e->getCountry()->getName());
		$js = $e->getJournalEntriesCount();
		$ps = $e->getPhotosCount();
		$ts = $e->getTipsCount();
    		$image = $e->getPhoto() ? "<img style='float: left; margin-right: 10px; border: 2px solid #e6e6e6' src='{$e->getPhoto()}'>" : '';
		if($e->getTravelLogID()){
			$travelLog = new TravelLog($e->getTravelLogID());
			$link = self::GOABROAD_DOTNET_URL.$entryHelper->getLink($travelLog);
			$image = "<a href='$link' target='_blank'>$image</a>";
			$countryName = "<a href='$link' target='_blank'>
				<span style='color: #60718B; font-weight: bold; padding: 4px 0 10px 0'>$ctryName</span>
			</a>";

		} else {
			$countryName = "<span style='color: #60718B; font-weight: bold; padding: 4px 0 10px 0'>$ctryName</span>";
		}
		$ctryMarkup = ''; 
    	if ($js + $ps + $ts) :
      		$ctryMarkup .= <<< CtryMarkup
       		$image <div id="country_details" style="float: left">$countryName
CtryMarkup;

	      	if ($js) $ctryMarkup .= ($js==1) ? '<p style="margin: 0">' . $js . ' Journal Entry</p>' : '<p style="margin: 0">' . $js . ' Journal Entries</p>';
	      	if ($ps) $ctryMarkup .= ($ps==1) ? '<p style="margin: 0">' . $ps . ' Travel Photo</p>' : '<p style="margin: 0">' . $ps . ' Travel Photos</p>';
	      	if ($ts) $ctryMarkup .= ($ts==1) ? '<p style="margin: 0">' . $ts . ' Travel Tip</p>' : '<p style="margin: 0">' . $ts . ' Travel Tips</p>';

      		$ctryMarkup .= '</div><div style="clear: both"></div>';

	      	$fbml .= <<< FbjsString
	        <fb:js-string var="entries.ctry$i">
	            $ctryMarkup
	        </fb:js-string>
FbjsString;

      		if (!$isCtrySelected) { $preselectedCtry = $ctryMarkup; }


	        // MARKER'S SHADOW -->
	      	$fbml .= <<< Marker
	        <div id="s$i" class="shadowMarker">
	          	<img width="22" src="$pinShadow" class="imgTrans"/>
	        </div>
Marker;
			// END OF MARKER'S SHADOW -->


        	// MAIN MARKERS -->
	      	if (!$isCtrySelected) :
				$isCtrySelected = true;
	        	$fbml .= <<< Marker
	          	<div id="c$i" class="greenMarker" style="display:none" onclick="showCountryInfo($i)" onmouseover="accentMarker(this)" >
	            	<img class="imgTrans" title="{$e->getCountry()->getName()}" alt="{$e->getCountry()->getName()}" src="$pinGreen" />
	          	</div>
	          	<div id="ac$i" onmouseover="accentMarker(this)" class="yellowMarker">
	            	<img class="imgTrans" title="{$e->getCountry()->getName()}" alt="{$e->getCountry()->getName()}" src="$pinYellow" />
	          	</div>
Marker;
	     	else :

	        	$fbml .= <<< Marker
	          	<div id="c$i" onclick="showCountryInfo($i)" onmouseover="accentMarker(this)" class="greenMarker">
	            	<img class="imgTrans" title="{$e->getCountry()->getName()}" alt="{$e->getCountry()->getName()}" src="$pinGreen" />
	          	</div>
	          	<div id="ac$i" onmouseover="accentMarker(this)" style="display:none" class="yellowMarker">
	            	<img class="imgTrans" title="{$e->getCountry()->getName()}" alt="{$e->getCountry()->getName()}" src="$pinYellow" />
	          	</div>
Marker;
      		endif;
			// END OF MAIN MARKERS -->


    	else :
      		// MAIN MARKERS AND SHADOW -->
    		$fbml .= <<< Marker
			<div id="s$i" class="shadowMarker">
				<img width="22" src="$pinShadow" class="imgTrans"/>
			</div>
      		<div id="c$i" class="blueMarker">
        		<img class="imgTrans" width="12" height="20" title="{$e->getCountry()->getName()}" alt="{$e->getCountry()->getName()}" src="$pinBlue" />
      		</div>
Marker;
      		// MAIN MARKERS AND SHADOW -->

    	endif;
    	$countries .= $countries ? ', '.$e->getCountry()->getName() : $e->getCountry()->getName();

  	endfor;
	// END OF $entryCount FOR LOOP -->
	

	$fbml .= '</div>';
	// END OF MAP DIV CONTAINER -->


	// COUNTRY JOURNALS INFO -->
	$fbml .= $preselectedCtry 
		? "<div id='ctryInfoDiv'>$preselectedCtry</div>" 
		: "<div id='ctryInfoDiv' style='display: none'></div>";	
	// END OF COUNTRY JOURNALS INFO -->



	// COUNTRYLIST -->
	if($entryCount) :
		$fbml .= <<< CountryList
		<div id="wish">
		    <span>
			  	<fb:fbml version="1.1">
					<fb:name uid='$userID' firstnameonly='true' useyou='false'/>
			  	</fb:fbml>
			  	has traveled to ($entryCount): 
			</span> 
			$countries
		    <div id="indicator" style="display: none">
		      	<img style="margin: 5px 0 0 0; float: left;" src="http://www.goabroad.com/facebook/traveltodo/images/loading.gif">
				<p style="float: left; margin: 10px 0 0 2px;">Loading...</p>
		      	<div style="clear: both;"></div>
		    </div>
		</div>
CountryList;
	endif;
	// END OF COUNTRYLIST -->


// ELSE HAS NO MAP ENTRIES -->
else :
  	$fbml .='</style>';	
  	$fbml .= <<< Links
  	<fb:visible-to-owner>
	  	<div id="top_links">
	    	<a href="http://apps.facebook.com/my-travel-map/index.php?action=refreshProfile" id="refresh">Refresh</a>
			<span id="links_pipe">|</span>
	    	<a href="http://apps.facebook.com/my-travel-map/">Edit Country Lists</a>
	  	</div>
  	</fb:visible-to-owner>
	<div style='font-weight:bold;padding:10px'>No Countries traveled yet!</div>
Links;

endif;
// END IF HAS MAP ENTRIES -->


// SRCIPTS -->
$fbml .= <<< Script
<script>
  	function dummyFunction() {}

  	function showCountryInfo(id) {
    	for (var i=0; i < $entryCount; i++) {
      		if (pin = document.getElementById('ac' + i)) pin.setStyle('display', 'none');
      		document.getElementById('c' + i).setStyle('display', 'block');
    	}
    	document.getElementById('ac' + id).setStyle('display', 'block');
    	document.getElementById('c' + id).setStyle('display', 'none');
    	document.getElementById('ctryInfoDiv').setInnerFBML(entries["ctry"+id]).setStyle('display', 'block');
  	}

  	function accentMarker(obj) {
    	for (var i=0; i < $entryCount; i++) {
      		if (mainPin = document.getElementById('c' + i)) {
        		mainPin.setStyle({zIndex: '4'});
      		}
      		if (altPin = document.getElementById('ac' + i)) {
        		altPin.setStyle({zIndex: '4'});
      		}
    	}
    	obj.setStyle({display: 'block', zIndex: '5'});
  	}

  	function restoreMarker(obj) {
    	obj.setStyle('display', 'block');
  	}
</script>
Script;
// END OF SRCIPTS -->

$fbml .='</fb:wide>';
?>
