<?php
/**
 * Default page of the My Travel Map application.
 */

$countryList = $model['countryList'];
$mapEntries = $model['mapEntries'];
$isSynced = $model['isSynced'];
$entriesCount = count($mapEntries);
$user = $model['user'];
$isOwnerUser =$model['isOwnerUser'];
$fbFirstName =$model['fbFirstName'];
$iframeHeight = $isSynced ? 600 : 500;

$traveledName = $isOwnerUser ? "$fbFirstName have" : "$fbFirstName has";

if($isOwnerUser) : 
	require_once 'mtm_dashboard.php'; ?>
<?php else : ?>
 	<fb:dashboard></fb:dashboard>
	<h5 style="margin: 0 0 20px 45px;"><fb:name uid="<?=$user ?>" possessive="true" /> travel map</h5>
<?php endif; ?>

<link type="text/css" rel="Stylesheet" media="screen" href="http://www.goabroad.net/facebook/mytravelmap/css/mytravelmap.css"/>
<fb:iframe
	src="http://www.goabroad.net/facebook/mytravelmap/index.php?action=genmap&id=<?=$user?>&fbFirstName=<?=$fbFirstName?>"
	width="100%"
        height="<?php echo $iframeHeight ?>"
        scrolling="no"
	style="border:none"
/>
<div style="padding: 0 0 15px 0;width: 100%">
  <div id="rewriteDiv" style="border: 1px solid #E6E6E6; padding:8px; margin: 0pt auto 10px; width: 486px;">


    <span><?=$traveledName ?> traveled to:</span>(<?php echo $entriesCount; ?>): <?php $cnt=0; foreach($mapEntries as $entry) { $cnt++; echo $entriesCount == $cnt ? $entry->getCountry()->getName() : $entry->getCountry()->getName().', '; } ?>
    <div id="indicator" style="display: none">
      <img src="http://www.goabroad.com/facebook/traveltodo/images/loading.gif" /><p>Loading...</p>
      <div class="clear"></div>
    </div>

	<?php if($isOwnerUser) : ?>
	  <a style="cursor: pointer; display: block; padding-top:10px" clicktoshow="editForm" clicktohide="showEditForm">Edit Country List</a>
	<?php endif; ?>

  </div>


  <form style="display: none" id="editForm" action="index.php?action=saveCountryList&id=<?php echo $user ?>" method="post">
    <div id="countrylist_container" style="border: 1px solid rgb(230, 230, 230); margin: 0pt auto 5px; padding: 8px; width: 486px; height:<?=$isSynced?'560px':'455px'?> ">
    <?php
    $uneditableMapEntries = array();
    if ($isSynced) {
      $uneditableMapEntries = $model['uneditableMapEntries'];
      if (count($uneditableMapEntries)) { ?>
        <div style="background-color:#FAFAFA; color:#777777; margin:10px auto; padding: 0 0 0 10px; width:460px;">
			Countries marked with an asterisk cannot be unchecked here because you may have written:
		    <ul style="padding-bottom:10px">
			    <li>journals on GoAbroad Network  for that country, or </li>
			    <li>past travel plans for that country on the application MyTravelPlans</li>
			</ul>
        </div>
    <?php }
 	} ?>
	<div style="overflow: auto;height: 400px;">
<?php 
    foreach ($countryList as $ctry) {

	  // added by Adelbert S.
	  // purpose: to avoid warning error (undefined variable: disabled)
	  $disabled = false;

      $id=$ctry->getCountryID();
    ?>
      <input type="checkbox"
             name="wishList[]"
             value="<?php echo $id.'_'.$ctry->getLocationID(); ?>"
             id="c<?php echo $id; ?>"
         <?php
         foreach ($uneditableMapEntries as $disabledEntry) {
           if ($id == $disabledEntry->getCountry()->getCountryID()) { ?>
         	   disabled="disabled"
         <?php
             $disabled = true;
             break;
           } else {
           	 $disabled = false;
           }//endif
         }//endforeach
         ?>
         <?php
         foreach ($mapEntries as $entry) {
           if ($id == $entry->getCountry()->getCountryID()) { ?>
             checked="checked"
         <?php
             break;
           }//end if
         }//end foreach
         ?>
      >
      <label for="c<?php echo $id; ?>">
        <?php
          echo $ctry->getName();
          if ($disabled) { ?>
            *
        <?php } ?>
      </label><br/>
    <?php } ?>                              
	</div>
	    <div id="actions" style= "padding: 10px 0pt 0pt; margin:10px 10px 5px 5px; border-top:1px solid #efefef">
	      <!--<input type="submit" value="Save"/>-->
		  <button onclick="return show()" class="inputbutton" style="padding: 4px; float: left; margin-right: 5px; line-height: 130%; cursor: pointer" type="submit">Save</button>
	      <a class="inputbutton" style="padding: 4px; margin-right:5px; float: left; display: block; line-height: 133%; text-decoration: none" clicktohide="editForm" clicktoshow="showEditForm" href="http://apps.facebook.com/my-travel-map">Cancel</a>
		  <div style="clear: both"></div>
	    </div>
    </div>
  </form>
</div>

<script>
function show(){
//	new Dialog().showMessage('Dialog', 'Continue');
//	var user_message_prompt = "What do you think of this book?"; 
//	var user_message = {value: "write your review here"}; 
//	var body_general = 'test body';
//	var template_data = {
//		"country": "Iceland", 
//		"images" : [{
//			"src":"http://www.goabroad.net/facebook/mytravelmap/images/wallTabMap.png",
//			"href":"http://www.facebook.com/apps/application.php?id=7425188942"
//		}]
//	};                                                                  
//   	var continuation = function() {document.setLocation('http://www.facebook.com'); };
//	Facebook.showFeedDialog(170317608942, template_data, body_general, null, continuation, user_message_prompt, user_message);
//	return false;
//}

</script>
