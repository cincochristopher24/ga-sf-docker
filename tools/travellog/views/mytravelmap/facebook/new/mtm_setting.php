<?php
$user        = $model['user'];
$isOwnerUser = $model['isOwnerUser'];
$fbFirstName = $model['fbFirstName'];
$pinSetting  = $model['pinSetting'];
$selected    = 'setting';
?>

<div id="tab_canvas" class=" ">
	<div>
		<div class="app_tab " id="">
			<div>
				<div class="appContainer" id="appContainer">
				
					<?php require_once 'mtm_dashboard.php';?>
				
					<div id="inner_wrapper">
						<div class="frm_settings">
							
							<form action="index.php?action=doChangeSetting" method="post">
  								<input type="hidden" name="user" value="<?php echo $user; ?>"/>
  								
  								<h2 class="header"> Settings </h2>
                			    <span class="sub_header">
									These settings only affect the "Large Box" view in your boxes tab and the MyTravelMap tab. The wall and "Small Box" view shows only countries.
                			    </span>
							    
                			    <p class="info"> Please select your application preference.</p>
  								
								<?php if ($_REQUEST['action']=='doChangeSetting') : ?>
  								
  									<div id="status">
  								
  									<?php if ($model['errors']): ?>
  										<fb:success decoration="shorten">
  											<fb:message>Success!</fb:message>
  										</fb:success>
  									<?php else: ?>
  										<fb:error decoration="shorten">
  								 			<fb:message>Error!</fb:message>
  										</fb:error>
  									<?php endif; ?>
  								
  									</div>
  									<br/>
  								
								<?php endif; ?>
                			    <div class="panel">
                			    	<div id="ph2" class="panel_header active">
                			    	  <input type="radio" id="viewType2" value="2" <?php echo (2==$pinSetting) ? 'checked="checked"' : ''; ?> class="radio_btn" name="pinSetting" />
                			    	  <label for="viewType2"><strong> Show <em>all</em> pins</strong></label>
									
                			    	</div>
                			    	<div class="panel_content">This is the default. If you think your map is too crowded, change the settings to show only country or city pins.</div>
                			    </div>
                			    <br />
  								
                			    <div class="panel">
                			    	<div id="ph1" class="panel_header active">
                			    	  <input type="radio" id="viewType1" value="1" <?php echo (1==$pinSetting) ? 'checked="checked"' : ''; ?> class="radio_btn" name="pinSetting" />
                			    	  <label for="viewType1"><strong> Show only <em>city</em> pins</strong></label>
									
                			    	</div>
                			    	<div class="panel_content">If you have changed your settings from Show all pins and you feel your map is still cluttered, turn it down another notch.</div>
                			    </div>
                			    <br />

  								<div class="panel">
                			    	<div id="ph0" class="panel_header active">
                			    	  <input type="radio" id="viewType0" value="0" <?php echo !$pinSetting ? 'checked="checked"' : ''; ?> class="radio_btn" name="pinSetting" />
                			    	  <label for="viewType0"><strong> Show only <em>country</em> pins</strong></label>
									
                			    	</div>
                			    	<div class="panel_content">Select this setting if you want to show only the countries you have visited.</div>
                			    </div>
                			    <br />
                			    
                			    <input type="submit" clicktoshow="loadingIndicator" value="Save Setting" clickthrough="true" name="submit" class="inputbutton" id="save" />
  								
							</form>

						</div>

						
						<div class="foot"> My Travel Map
							<span> &copy; <a target="_blank" href="http://goabroad.net">GoAbroad
							Network</a>/ <a target="_blank" href="http://goabroad.com">GoAbroad.com</a>.
							<span> All Rights Reserved. </span>
							</span>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
		<div style="position: absolute; top: -10000px; width: 0px; height: 0px;" id="FB_HiddenContainer"></div>
	</div>
</div>