<?php
$user = $model['user']; 
$isOwnerUser = $model['isOwnerUser'];
$selected = 'synchronize';
require_once 'mtm_dashboard.php'; ?>

<div id="inner_wrapper" style="overflow:hidden">
	
	<form action="index.php?action=doSync" method="post" id="syncForm" onsubmit="return syncNoticeMessage()">
	  <input type="hidden" name="user" value="<?php echo $user; ?>"/>

	<?php if ($_REQUEST['action']=='doSync') : ?>

		<div id="status" style="margin:0 85px">

		<?php if (is_object($model['message'])): ?>
			<fb:success decoration="shorten">
	  			<fb:message>Success!</fb:message><?php echo $model['message']; ?>
			</fb:success>
		<?php else: ?>
			<fb:error decoration="shorten">
	   			<fb:message>Error!</fb:message><?php echo $model['message']; ?>
	  		</fb:error>
		<?php endif; ?>

		</div>

	<?php endif; ?>

	  <div style="padding: 0 0 20px 85px">
	<?php if($model['isSynchronized']) { ?>
	    <p style="width: 500px;">
			This account is currently synchronized with <b><?=$model['ganetUsername'] ?>'s</b> GoAbroad Network profile. If you don't have any GoAbroad Network account with an email "<b><?=$model['ganetEmail'] ?></b>" <a href='http://www.goabroad.net/feedback.php'>please send us an email</a>
	    </p>
	<?php }else{  ?>
	    <p style="width: 500px;">
			Did you know that you can get this map to show your travel blogs for each country? All you need to do is synchronize this application with your GoAbroad Network travel profile.
	    </p>
	    <p style="width: 490px;">
			<b>If you don't have a GoAbroad Network account yet, <a href="http://www.goabroad.net/register.php?traveler" target="_blank">click here</a> to know how easily you can get.</b>
	    </p>
	    <p style="width: 500px;">
			For existing members, simply type in your GoAbroad Network email and password:	
	    </p>
	<?php } ?>	
	    <div style="padding: 15px 0 0 0;">
	      <label style="padding: 0 20px 0 0;">Email:</label><br/>
	      <input style="border: 1px solid #ebebeb" type="text" name="email" value="<?php echo $_REQUEST['email'] ? $_REQUEST['email'] : ''; ?>" />
	    </div>
	    <div style="padding: 5px 0 15px 0;">
	      <label style="padding: 0 22px 0 0;">Password:</label><br/>
	      <input style="border: 1px solid #ebebeb" type="password" name="password"/>
	    </div>
	    <div style="padding-top:10px">
	      <input type="submit" class="inputbutton" value="Synchronize" onclick="return syncNoticeMessage()"/>
	    </div>

	    <div id="loadingIndicator" style="display:none;">
	      <img src="http://www.goabroad.com/facebook/traveltodo/images/loading.gif" style="margin: 5px 0 0 0; float: left;"><p style="float: left; margin: 10px 0 0 2px;">Please wait...</p>
	      <div style="clear: both;"></div>
	    </div>
	  </div>
	</form>

	<div class="foot"> My Travel Map
		<span> &copy; <a target="_blank" href="http://goabroad.net">GoAbroad
		Network</a>/ <a target="_blank" href="http://goabroad.com">GoAbroad.com</a>.
		<span> All Rights Reserved. </span>
		</span>
	</div>
</div>

<script type="text/javascript">
    	
	function syncNoticeMessage() {
		var showMsg = <?php echo isset($model['message']) ? 'false' : 'true' ?>;
		if(showMsg == true ){
			var dialog_title = 'Synchronize';
			var dialog_message = 'Please be reminded that synchronizing your account will pull all your map entries in goabroad.net and will drop all your entries here in facebook.\n\nClick yes to continue.';
			var dialog = new Dialog().showChoice(dialog_title, dialog_message, 'Yes', 'Cancel'); 
			dialog.onconfirm = function() { 
				document.getElementById('loadingIndicator').setStyle('display','block');
				document.getElementById('syncForm').submit();
			}; 
		} else {
			document.getElementById('loadingIndicator').setStyle('display','block');
			document.getElementById('syncForm').submit();			
		}

		return false;
	}
</script>