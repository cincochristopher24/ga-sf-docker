<?php
    $vars = $model['vars'];
    $locID = isset($vars['locationID']) ? $vars['locationID'] : 0;
    $countryID = isset($vars['countryID']) ? $vars['countryID'] : 0;
    $lat = isset($vars['latitude']) ? $vars['latitude'] : 0;
    $long = isset($vars['longitude']) ? $vars['longitude'] : 0;
    $isNewCountry = isset($vars['isNewCountry']) ? $vars['isNewCountry'] : 0;
    $parms='';
    foreach ($_REQUEST as $name=>$val) {
      if (substr($name,0,6)!='fb_sig') continue;
      if ($parms!='') $parms.='&';
      $parms.=$name.'='.$val;
    }
    
?>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>My Travel Map</title>

    <link type="text/css" rel="Stylesheet" media="screen" href="http://www.goabroad.net/facebook/mytravelmap/css/travelMapBubble.css"/>
	<link type="text/css" rel="Stylesheet" media="screen" href="http://www.goabroad.net/facebook/mytravelmap/css/mytravelmap_v2.css"/>

	<style type="text/css">
		.lightbox{
			height: 100%;
			left:0;
			position:fixed;
			text-align:center;
			top:9;
			width:100%;
		}
		
		.promptBox {
			background-color: #FFFFFF;
			border: 3px solid gray;
			font-weight:bold;
			margin:0 auto;
			padding:20px;
			position:relative;
			top:150px;
			width:370px;
		}
	
	</style>
	
	
  </head>
<body onload="load()" id="mapBody" style="baclground-color:#efefef">
	<div>
        <div style = "height: 525px; background-color: #FFFFFF; border: 4px solid #525252;">
          <form name = "frmAddCity" id = "frmAddCity" method = "POST">
            <h4 class="area" style="padding:10px 15px;font-size:16px;font-weight:bold;margin:10px 0 0 0;color:#333"> Add New Location </h4>
            <div style = "padding-left: 15px; padding-top: 5px;">
              <div class="content">
	
	
                <ul class="form">
	
                  <li style="padding-bottom: 10px;">
                    <label for="txtCity" style="font-size:12px;color:#333;"> What's the name of this place? <span style="color:#f00"> * </span> </label><br />
                    <input type="text" name="txtCity" id="txtCity" value="" style="font-size:12px;width:250px;margin-top:3px;color:#444" />
                    <span id="txtErrorCity" style="size: 9px; font-weight: normal; color: #f00;"> </span>
				  </li>

                  <li>
                    <label style="font-size:12px;color:#333;margin-bottom:2px"> Where is it? <span style="color:#f00"> * </span>
						<span id="txtErrorLocation" style="font-weight: normal; size: 9px; color: #FF0000;"> </span>
					</label>
					
                    <div>
                      <fieldset>


                      <div id="viewallcountries_map" style="border:solid 1px #79b;width:460px;float:left;margin-top:3px;">
   					
                        <div id="map" class="map" style="width:460px; height:300px"></div>
                      </div>

                      <div style = " font-size: 10.5px; padding-left: 13px; width: 250px; margin-left: 460px">
                        <div>
                          <h4 style = "font-size: 11px; font-weight: bold; padding-bottom: 5px;"> Tips: </h4>

                        </div>
                        <div style = "padding-left: 15px;"> <img src = "/images/zoomIn.jpg" width = "13" height = "13" /> <font valign = "middle" style = "padding-left: 10px;"> zoom in </font> </div>
                        <div style = "padding-left: 15px; padding-top: 5px;"> <img src = "/images/zoomOut.jpg" width = "13" height = "13" /> <font valign = "middle" style = "padding-left: 10px;"> zoom out </font> </div>
                        <div style = "padding-left: 15px; padding-top: 5px;"> <img src = "/images/panUp.jpg" width = "13" height = "13" /> <font valign = "middle" style = "padding-left: 10px;"> pan map upward </font> </div>
                        <div style = "padding-left: 15px; padding-top: 5px;"> <img src = "/images/panLeft.jpg" width = "13" height = "13" /> <font valign = "middle" style = "padding-left: 10px;"> pan map to the left </font> </div>
                        <div style = "padding-left: 15px; padding-top: 5px;"> <img src = "/images/panRight.jpg" width = "13" height = "13" /> <font valign = "middle" style = "padding-left: 10px;"> pan map to the right </font> </div>
                        <div style = "padding-left: 15px; padding-top: 5px;"> <img src = "/images/panDown.jpg" width = "13" height = "13" /> <font valign = "middle" style = "padding-left: 10px;"> pan map downward </font> </div>

						<ul style="padding:10px 15px 0 35px;margin:0;font-size:11px;list-style-type:disc">
						  <li>drag the map to move around</li>
						  <li>click on the map where the city that you want to add is located</li>
						  <li>click on the marker to remove it</li>
						</ul>
                      </div>

                      </fieldset>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li style = "padding-bottom: 23px;padding-top:5px;">
                    <div style = "width: 250px; float: left;">
						Longitude: <span id = "txtLong"></span>
					</div>
                    <div> Latitude: <span id = "txtLat"></span> </div>
                  </li>
                  <li style = "padding-bottom: 10px;">
					<div class="clear"></div>
                    <fieldset>
                    <span id="addLocation">
                    	<input type="button" name="btnAdd" id="btnAdd" value="Add Location" onclick="checkForms();" style="font-size:12px;" />
                    </span>
                    
					<input type = "hidden" name = "countryLocID" id= "countryLocID" value="<?php echo $locID?>">
					<input type = "hidden" name = "countryID" id= "countryID" value="<?php echo $countryID?>">
                    </fieldset>
                  </li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
          </form>
        </div>
      </div>
    </div>	

	<div style="display: none;" id="lightboxContainer" class="lightbox">
		<div id="promptBox" class="promptBox">
		    <span id="success"></span>
			<span id="promptMsg">New Location has been added.</span>
			<div stylee="margin-top:20px"> </div>
			<input type="button" value="OK" id="yesButton" class="promptButton" onclick="closeCustomMapAction();"/>
		</div>
	</div>
	<iframe id="helpframe" src='' height='300' width='300' frameborder='0'></iframe>
    <script type="text/javascript" src="http://www.goabroad.net/js/prototype.js"></script>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAADN6t8sswaCRGbhKv8-BC0xTfWdpSMA2WXJD7WFd_XyKADP-mYxSQwLFLL4KN4RJUZRLKVgX7vxapkg" type="text/javascript"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">

    //<![CDATA[
    var blueImage = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_tinymarkerBlueTransparent.png';	
	var postURL = 'http://www.goabroad.net/facebook/mytravelmap.new/index.php';

    var iconBlue = new GIcon(); 
    iconBlue.image = blueImage;
//    iconBlue.shadow = shadowImage;
//   iconBlue.iconSize = new GSize(12, 20);
//    iconBlue.shadowSize = new GSize(22, 20);
    iconBlue.iconAnchor = new GPoint(6, 20);
    iconBlue.infoWindowAnchor = new GPoint(5, 1);

	var map;
    function load() {
      	if (GBrowserIsCompatible()) {
   			map = new GMap2($("map"));
        	map.addControl(new GSmallMapControl());
        	map.setCenter(new GLatLng(<?php echo $lat?>, <?php echo $long?>), 5);
        	
        	geocoder = new GClientGeocoder();
        	GEvent.addListener(map, 'click', function(overlay, latlng) {
        	    clearMarkers();
        	    var lat = latlng.lat();
            	var lon = latlng.lng();
            	cLat =lat;
            	cLng = lon;
            	var latOffset = 0.01;
            	var lonOffset = 0.01;
            	var point = new GLatLng(lat, lon); 
                marker = new GMarker(point,iconBlue);
                      
                map.addOverlay(new GMarker(point,iconBlue));

                geocoder.getLocations(latlng, showAddress);
            });
            
      	}
    }
	
    function showAddress(response) {
		if (200 == response.Status.code){ // valid location
        	place = response.Placemark[0];
			arrAddress = place.address.split(",");
			$('txtCity').value=arrAddress[0];
			$('txtLong').innerHTML=place.Point.coordinates[0];
			$('txtLat').innerHTML=place.Point.coordinates[1];
		}
		else if (602 == response.Status.code){ // null
			arrCoordinates = response.name.split(",");
			$('txtCity').value="";
			$('txtLat').innerHTML=arrCoordinates[0];
			$('txtLong').innerHTML=arrCoordinates[1];
		}
    }
    
    function clearMarkers() {
        map.clearOverlays();
    }

	function checkForms(){
		if (document.frmAddCity.txtCity.value.length == 0){
			$('txtErrorCity').innerHTML = '(what is the name of the city that you wish to add?)';
			if (($('txtLat').innerHTML.length == 0) && ($('txtLong').innerHTML.length == 0)){
				$('txtErrorLocation').innerHTML = '(please click on the map for the coordinates of your city)';
			}
			else{
				$('txtErrorLocation').innerHTML = '';
			}
		} 
		else{
			if (($('txtLat').innerHTML.length == 0) && ($('txtLong').innerHTML.length == 0)){
				$('txtErrorLocation').innerHTML = '(please click on the map for the coordinates of your city)';
				$('txtErrorCity').innerHTML = '';
			}
			else{
				$('addLocation').innerHTML = 'Adding Location...';
				addLocation();
			}
		}
	}
	
	function addLocation() {
		var name = document.frmAddCity.txtCity.value;
		var lat = $('txtLat').innerHTML;
		var long = $('txtLong').innerHTML;
		var locID = $('countryLocID').value;
		var countryID = $('countryID').value;
		var isNewCountry = <?php echo $isNewCountry?>;
		var url = postURL+'?action=addLocation&latitude='+lat+'&longitude='+long+'&locID='+locID+'&countryID='+countryID+'&name='+name+'&isNewCountry='+isNewCountry+'&<?php echo $parms?>';
        
		new Ajax.Request(url, {
			onSuccess:function(transport){
				$('lightboxContainer').style.display = 'block';
			}
		});
	}
	
	function closeCustomMapAction() {
	    //$('addLocation').innerHTML = '<input type = "button" name = "btnAdd" id = "btnAdd" value = "Add Location" onclick = "checkForms();" />';
	    //$('lightboxContainer').style.display='none';
        // Going to 'pipe' the data to the parent through the helpframe..
        //var helpmapframe = $('helpframe');
        //alert($('mapBody').parentNode.id);
        //helpmapframe.src = 'http://apps.facebook.com/mytravelmap-dev/index.php?action=mapHelper';
        $('promptMsg').innerHTML = 'Updating Map changes...';
        $('yesButton').disabled = true;
        parent.location.href = "http://apps.facebook.com/my-travel-map/index.php";
	}

	function closeMap() {
		alert($('mapBody').parentNode.id);
	}
	
	function $(elemId) {
		return document.getElementById(elemId);
	}
	
   //]]>
    </script>
</body>

</html>
