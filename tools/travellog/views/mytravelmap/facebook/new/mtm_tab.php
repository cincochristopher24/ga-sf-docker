<?php
$pinBlueCity = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_tinymarkerBlueTransparent.png';
$pinYellowCity = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_tinymarkerYellowTransparent.png';
$map = new Map(array('x'=>380, 'y'=>380));
$mapCity = new Map(array('x'=>378, 'y'=>403));
$i=0;
$countries = '';
$preselectedCtry = '';
$countriesWithCities = '';
$defaultPointerLeft = null;
$showCityPins = $pinSetting != 0;
$showCountryPins = $pinSetting != 1;
$firstLoc = 0;
$countriesCount = count($mapEntries);
?>
<style type="text/css">

<?php

	$style='';
	// SET CSS FOR MAP ENTRIES -->
  	foreach ($mapEntries as $entry) :

		if($showCountryPins) {
		    $shadowDivID = 's'.$i;
		    $markerDivID = 'pin1_'.$i;
		    $altMarkerDivID = 'pin2_'.$i;

		    $coordinates = $map->lngLatToXYCoords($entry->getCountry()->getLongitude(), $entry->getCountry()->getLatitude());

		    $y = $coordinates['y'];
		    $x = $coordinates['x'] - 2;

			if($entry->getCountry()->getCountryID() == 1322) {
				$x += 5;
			}

			$showdowY = $y-1;
			$showdowX = $x+1;
		    $style .= '#'.$shadowDivID. '{
		      	top: '.$showdowY.'px;
		      	left: '.$showdowX.'px;
		      	z-index: 0;
		    }';
                                 
		    $x++;
		    $style .= '#'.$markerDivID. '{
		      	top: '.$y.'px;
		      	left: '.$x.'px;
		      	z-index: 5;
		    }';

		    $y++;
		    $style .= '#'.$altMarkerDivID. '{
		      	top: '.$y.'px;
		      	left: '.$x.'px;
		      	z-index: 5;
		    }';
			if($i == 0)	$defaultPointerLeft = $x.'px';
		    $i++;
		}
	
		if($showCityPins) :
			foreach ($entry->getCountry()->getCityLocationList() as $cityEntry) :
			    $shadowDivID = 's'.$i;
			    $markerDivID = 'pin1_'.$i;
			    $altMarkerDivID = 'pin2_'.$i;

			    $coordinates = $mapCity->lngLatToXYCoords($cityEntry->getLongitude(), $cityEntry->getLatitude());

			    $y = $coordinates['y'];
			    $x = $coordinates['x'] - 2;

				if($cityEntry->getCountryID() == 1322) {
					$x += 5;
				}

			    $style .= '#'.$shadowDivID. '{
			      	display: none;
			    }';

			    $x++;

			    $style .= '#'.$markerDivID. '{
			      	top: '.$y.'px;
			      	left: '.$x.'px;
			      	z-index: 5; 
				 }';

			    $y++;
			    $style .= '#'.$altMarkerDivID. '{
			      	top: '.$y.'px;
			      	left: '.$x.'px;
			      	z-index: 5;
				 }';
				if($i == 0)	$defaultPointerLeft = $x.'px';
			    $i++;
			endforeach;
		endif;
		
  	 endforeach;
	echo $style;
?>
</style>
<div class="right_column" id="right_column" style="float:left">
	
	<div id="tab_canvas" class=" ">
		<div>
			<div class="app_tab " id="">
				<div>
					<div class="appContainer" id="appContainer">
						<?php $selected = 'dashboard'; require_once 'mtm_dashboard.php' ?>						
						
						<div id="inner_wrapper">
							<div class="wrapper2" id="wrapper" style="position:relative;padding: 70px 20px 0;">
								
								<?php $i = 0; $entryHelper = new EntryHelper; $isCtrySelected = false; //watch out for $entryCount ?>
									<!-- MAP DIV CONTAINER -->
								<div class="map_container_tab">
									<div id="container">

									<!-- $mapEntries FOR LOOP -->
									<?php
									foreach ($mapEntries as $mapEntry) :
										$countryLocationID = $mapEntry->getCountry()->getID();
										if($i == 0) $firstLoc = $mapEntry->getCountry()->getID();
										$flag = $mapEntry->getCountry()->getFlag();
										$countryName = str_replace("&",'&amp;',$mapEntry->getCountry()->getName());
										$image = $journalEntries = $photoEntries = $travelTips = '';
										if($showCountryPins) { 
											if($travellogID = $mapEntry->getCountry()->getTravelLogID()) {
									  			$travelLog = new TravelLog($travellogID);
												$entryTips = new EntryTips;
												$ts = count($entryTips->getList($travellogID));
												$js = $mapEntry->getCountry()->getJournalEntriesCount();
												$photos = $travelLog->getPhotos();
												$ps = count($photos);
												$link = self::GOABROAD_DOTNET_URL.$entryHelper->getLink($travelLog);

									        	$entries = $js < 2 ? "$js Journal Entry" : "$js Journal Entries";
												$journalEntries = "$entries <span class='mute'>|</span> <a href='$link' target='_blank'>read latest &raquo;</a><br />";	

												if($ps) {
									        		$photoEntries = $ps < 2 ? "$ps Photo" : "$ps Photos";
		                                            $photo =  $photos[rand(0,$ps-1)]->getPhotoLink('thumbnail');
											   		$image = "<img class='journal_photo' src='$photo'>";
													$photosLink = self::GOABROAD_DOTNET_URL."/collection.php?type=traveler&ID=".$travelLog->getTravelerID()."&context=travellog&genID=".$travellogID;
													$photoEntries = "$photoEntries <span class='mute'>|</span> <a href='$photosLink' target='_blank'>browse photos &raquo;</a><br />";
												}

												if($ts) {
									            	$travelTips = $ts < 2 ? "$ts Travel Tip"   : "$ts Travel Tips";													
												}
											}
								      		$ctryMarkup = "<div>
											  		$image
											  		<img class='location_flag' src='$flag' alt='' />
											  		<p class='country_name'><strong>$countryName</strong></p>";
											if($travellogID)  {
												 $ctryMarkup .= "<p class='location_details'>
												    $journalEntries 
												    $photoEntries
												    $travelTips
												  </p>";
											}
								      		$ctryMarkup .= '<div style="clear: both"/></div>'; ?>

									        <fb:js-string var="entries.ctry<?php echo $i?>">
									            <?php echo $ctryMarkup; ?>
									        </fb:js-string>
								      		<?php if (!$isCtrySelected) { $preselectedCtry = $ctryMarkup; } ?>


									        <!-- MARKER'S SHADOW -->
									        <div id="s<?php echo $i?>" class="shadowMarker">
									          	<img width="22" src="<?php echo $pinShadow ?>" class="imgTrans"/>
									        </div>
									        <!-- END OF MARKER'S SHADOW -->


								        	<!-- MAIN MARKERS -->
											<?php if (!$isCtrySelected) : $isCtrySelected = true; ?>
									          	<div id="pin1_<?php echo $i ?>" class="blueMarker" style="display:none" onclick="showCountryInfo('<?php echo $i ?>',this)" onmouseover="accentMarker(this)" >
									            	<img class="imgTrans" title="<?php echo $countryName ?>" alt="" src="<?php echo $pinBlue ?>" />
									          	</div>
									          	<div id="pin2_<?php echo $i ?>" onmouseover="accentMarker(this)" class="yellowMarker">
									            	<img class="imgTrans" title="<?php echo $countryName ?>" alt="" src="<?php echo $pinYellow ?>" />
									          	</div>

									     	<?php else : ?>
									          	<div id="pin1_<?php echo $i ?>" onclick="showCountryInfo('<?php echo $i ?>',this)" onmouseover="accentMarker(this)" class="blueMarker">
									            	<img class="imgTrans" title="<?php echo $countryName ?>" alt="" src="<?php echo $pinBlue ?>" />
									          	</div>
									          	<div id="pin2_<?php echo $i ?>" onmouseover="accentMarker(this)" style="display:none" class="yellowMarker">
									            	<img class="imgTrans" title="<?php echo $countryName ?>" alt="" src="<?php echo $pinYellow ?>" />
									          	</div>
									     	<?php endif; ?>
						   					<input id="locationID_<?php echo $i ?>" type="hidden" value="<?php echo $countryLocationID ?>"/>											
										<?php $i++; }
										
										 	$cities = array();
										
											foreach ($mapEntry->getCountry()->getCityLocationList() as $cityEntry) :
												$cityLocationID = $cityEntry->getID();
												$cityName = str_replace("&",'&amp;',$cityEntry->getName()). ', '. $countryName;
								            	$cities[] = $cityEntry->getName();
												$cityImage = $cityJournalEntries = $cityPhotoCount = $cityTipsCount = '';
												if($showCityPins) :
													if($cityTravellogID = $cityEntry->getTravelLogID()) {
											  			$cityTravelLog = new TravelLog($cityTravellogID);
														$cityEntryTips = new EntryTips;
														$cityTipsCount = count($cityEntryTips->getList($cityTravellogID));
														$cityEntryCount = $cityEntry->getJournalEntriesCount();
														$cityPhotos = $cityTravelLog->getPhotos();
														$cityPhotoCount = count($cityPhotos);
														$cityLink = self::GOABROAD_DOTNET_URL.$entryHelper->getLink($cityTravelLog);

											        	$cityEntries = $js < 2 ? "$cityEntryCount Journal Entry" : "$cityEntryCount Journal Entries";
														$cityJournalEntries = "$cityEntries <span class='mute'>|</span> <a href='$cityLink' target='_blank'>read latest &raquo;</a><br/>";

														if($cityPhotoCount) {
											        		$cityPhotoEntries = $cityPhotoCount < 2 ? "$cityPhotoCount Photo" : "$cityPhotoCount Photos";
				                                            $cityPhoto =  $cityPhotos[rand(0,$cityPhotoCount-1)]->getPhotoLink('thumbnail');
													   		$cityImage = "<img class='journal_photo' src='$cityPhoto'>";
															$cityPhotosLink = self::GOABROAD_DOTNET_URL."/collection.php?type=traveler&ID=".$cityTravelLog->getTravelerID()."&context=travellog&genID=".$cityTravellogID;
															$cityPhotoEntries = "$cityPhotoEntries <span class='mute'>|</span> <a href='$cityPhotosLink' target='_blank'>browse photos &raquo;</a><br/>";
														}

														if($cityTipsCount) {
											            	$cityTravelTips = $cityTipsCount < 2 ? "$cityTipsCount Travel Tip"   : "$cityTipsCount Travel Tips";													
														}

													}
										      		$cityMarkup = "<div>
													  		$cityImage
													  		<img class='location_flag' src='$flag' alt='' />
													  		<p class='country_name'><strong>$cityName</strong></p>";
													
													if($cityTravellogID)  {
														 $cityMarkup .= "<p class='location_details'>
														    $cityJournalEntries 
														    $cityPhotoEntries
														    $cityTravelTips
														  </p>";
													}
										      		$cityMarkup .= '<div style="clear: both"/></div>'; 

													if($pinSetting == 1 && !$isCtrySelected) { $preselectedCtry = $cityMarkup; } ?>
													
											        <fb:js-string var="entries.ctry<?php echo $i ?>">
														<?php echo $cityMarkup ?>
											        </fb:js-string>


										        	<!-- MAIN MARKERS -->
											      	<?php if (!$isCtrySelected) : $isCtrySelected = true; ?>
											          	<div id="pin1_<?php echo $i ?>" class="blueSmallMarker" style="display:none" onclick="showCountryInfo('<?php echo $i ?>',this)" onmouseover="accentMarker(this)" >
											            	<img class="imgTrans" title="<?php echo $cityName ?>" alt="" src="<?php echo $pinBlueCity ?>" />
											          	</div>
											          	<div id="pin2_<?php echo $i ?>" class="yellowSmallMarker" onmouseover="accentMarker(this)">
											            	<img class="imgTrans" title="<?php echo $cityName ?>" alt="" src="<?php echo $pinYellowCity ?>" />
											          	</div>
													<?php else: ?>
											          	<div id="pin1_<?php echo $i ?>" class="blueSmallMarker" onclick="showCountryInfo('<?php echo $i ?>',this)" onmouseover="accentMarker(this)">
											            	<img class="imgTrans" title="<?php echo $cityName ?>" alt="" src="<?php echo $pinBlueCity ?>" />
											          	</div>
											          	<div id="pin2_<?php echo $i ?>" class="yellowSmallMarker" onmouseover="accentMarker(this)" style="display:none">
											            	<img class="imgTrans" title="<?php echo $cityName ?>" alt="" src="<?php echo $pinYellowCity ?>" />
											          	</div>
													<?php endif; ?>
								   					<input id="locationID_<?php echo $i ?>" type="hidden" value="<?php echo $cityLocationID ?>"/>
													
												<?php $i++; endif; // end if of IF city pins 
												
									  		endforeach; // END OF $cityEntry FOR LOOP 
										$stringCities = !empty($cities) ? implode(', ', $cities) : '';
										$countriesWithCities .= '<li><strong>'.$countryName.'</strong><br>'. implode(', ', $cities).'</li>';
										$citiesCount += count($cities);
										
								  	endforeach; // END OF $entryCount FOR LOOP --> ?>

									<?php $hasCities  = ($citiesCount > 0) ? true : false; ?>
									
									<?php if(!is_null($defaultPointerLeft)) : ?>
										<div id='pinPointer' class='markerPointer' style='left:<?php echo $defaultPointerLeft ?>'>
											<img src='http://www.goabroad.net/facebook/mytravelmap/images/arrow-up.gif'/>
										</div>
									<?php endif; ?>
									<?php if($countriesCount) : ?>
										<div class="click_invitation" id="click_invitation">
											<img src="http://www.goabroad.net/facebook/mytravelmap/images/click_invite_illustration.jpg" />
											<p>
												<strong>Tip:</strong><br />
												Click on the pins to browse through the locations.
											</p>
											<p>
												<a onclick="this.getParentNode().getParentNode().setStyle('display','none')">close</a>
											</p>
										</div>
									<?php endif ?>
									</div> <!-- close div container -->

									<?php if($countriesCount) : ?>
										
									   	<fb:js-string var='countriesWithCities'><?php echo $countriesWithCities ?><br/><a href='#' onclick='hideCities()'>hide cities</a></fb:js-string>
									   	<fb:js-string var='countriesOnly'><?php echo $countries ?><br/><a href='#' onclick='showCities()'>show cities</a></fb:js-string>

										<!-- COUNTRY JOURNALS INFO -->
										<div class="location_info">
											<ul class="location_navigation">
										    	<li class="prev"><a class="prev_link" href="#" title="Previous" onclick="mapEntriesPrevNextControl('prev')"><span>Prev</span></a></li>
										    	<li class="next"><a class="next_link" href="#" title="Next" onclick="mapEntriesPrevNextControl('next')"><span>Next</span></a></li>
										  	</ul>
											<?php 
											echo $preselectedCtry 
												? "<div id='locationInfo'>$preselectedCtry</div>" 
												: "<div id='locationInfo' style='display: none'> </div>";	
											// END OF COUNTRY JOURNALS INFO -->
											?>
										</div>
										<?php
										$countriesCount = $countriesCount < 2 ? "$countriesCount country" : "$countriesCount countries";
										$citiesCount = $citiesCount < 2 ? "$citiesCount city" : "$citiesCount cities";
										// COUNTRYLIST -->  ?>
										
											<div id="wish" class="wish" style="display:none">
												<span> 
													<fb:fbml version="1.1">
														<fb:name uid='<?php echo $userID ?>' firstnameonly='true' useyou='false'/>
												  	</fb:fbml>
													<?/* dont include city count if there are no cities  */?>
													has been to <?php if($hasCities): echo $citiesCount; ?> in <?php endif; ?><?php echo $countriesCount ?>: 
												</span> <br />
												<div id="locationList">
													<?php echo $countries ?> 
													<a href="#" onclick="showCities()">show cities</a>
												</div>
											</div>
										
											
										<fb:visible-to-owner>
											<div class="friends_been_there" id="friends">
												<h3 class="friends_list_header" id="friends_header" style="<?php if(count($friendsBeenThere[$firstLoc]) <= 0): echo 'display:none;'; endif; ?>">Friends who've been there:</h3>
												<?php $friendsBeenThere = is_array($friendsBeenThere) ? $friendsBeenThere : array(); ?>	
												<?php foreach($friendsBeenThere as $loc => $friends): ?>
													<ul class="friends_list" id="friendlist_<?=$loc?>" style="<?php if($loc != $firstLoc): echo 'display:none;'; endif; ?>">
														<?php 
															$friends = array_flip($friends);
															$friends = array_unique($friends);
															$limit = (count($friends) > 6) ? 6 : count($friends);
															$friends = (count($friends) > 0) ? array_rand($friends, $limit) : array();
															$friends = (is_array($friends)) ? $friends : array($friends); // array_rand returns the key if the limit is set to 1 else array of keys
														?>
														<?php foreach($friends as $friend): ?>
															<li>
																<div style="width:57px;">
																	<a href="http://apps.facebook.com/my-travel-map/index.php?id=<?=$friend?>" target="_parent"><fb:profile-pic uid="<?=$friend?>" linked="false" size="square"/></a>
																	<div style="width:57px;word-wrap:break-word;">
																		<a href="http://apps.facebook.com/my-travel-map/index.php?id=<?=$friend?>" target="_parent"><fb:name uid="<?=$friend?>" linked="false" /></a>
																	</div>	
																</div>
															</li>
														<?php endforeach; ?>
													</ul>
												<?php endforeach; ?>
												<br/>
											  	<p class="clear"><a href="http://apps.facebook.com/my-travel-map/index.php?action=invite" target="_parent" class="fb_button">Invite Friends</a></p>										
											</div>
										</fb:visible-to-owner>
									<?php endif; ?>
								</div>
								
								<div class="wish2">

									<?php if ($countriesCount) : ?>
								  	<div class="top_links">
										<fb:visible-to-owner>
		  									<a href="http://apps.facebook.com/my-travel-map/?edit" class="round_button">
		  									  	<span>Edit Country Lists</span>
		  									</a>
									    </fb:visible-to-owner>
  								  	</div>

									<h2 class="location_list_heading"> 
										<fb:name uid="<?php echo $facebookID?>" linked="false" capitalize="true" firstnameonly="true" useyou="false"/> 
										has travelled to: 
									</h2>
									<ul class="location_list">
									    <?php echo $countriesWithCities ?> 
									</ul>
									<?php else : ?>
										<strong>
										No Countries and cities traveled yet.
										<fb:visible-to-owner>
		  									<a href="http://apps.facebook.com/my-travel-map/">
		  									  	<span style="color:#3B5998">Click here to add!</span>
		  									</a>
									    </fb:visible-to-owner>
										<strong>
									<?php endif ?>
								</div>

								<h1 class="map_heading" style="	position:absolute;width:700px;top:0px;left:20px;display:block;">
									<fb:name uid="<?php echo $facebookID?>" linked="false" capitalize="true" firstnameonly="true" possessive="true"/> Travel Map 
									<?php if($countriesCount) : ?><strong class="map_summary"> <?php if($hasCities): echo $citiesCount; ?> in <?php endif; ?><?php echo $countriesCount; endif ?> 
								</h1>
							</div>                                                                                                              							
							<div class="foot"> My Travel Map
								<span> &copy; <a target="_blank" href="http://goabroad.net">GoAbroad
								Network</a>/ <a target="_blank" href="http://goabroad.com">GoAbroad.com</a>.
								<span> All Rights Reserved. </span>
								</span>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
			<div style="position: absolute; top: -10000px; width: 0px; height: 0px;" id="FB_HiddenContainer"></div>
		</div>
	</div>
</div>

<script>

	var currentValue = 0;
	var totalCount = <?php echo $i ?> - 1;

	function $(elemId) {
	   return document.getElementById(elemId);
	}

  	function showCountryInfo(id,elem) {

		var divElem = $('container').getChildNodes();
    	for (var i=0; i < divElem.length; i++) {
			if(divElem[i].getClassName() == 'yellowMarker' || divElem[i].getClassName() == 'yellowSmallMarker') {
				divElem[i].setStyle('display', 'none');
				divElem[i].getPreviousSibling().setStyle('display', 'block');
			}			
    	}
		currentValue = id;
    	elem.setStyle('display', 'none');
    	elem.getNextSibling().setStyle('display', 'block');
    	$('pinPointer').setStyle('left', elem.getNextSibling().getAbsoluteLeft()-183 + 'px');
    	$('locationInfo').setInnerFBML(entries["ctry"+id]).setStyle('display', 'block');
		getFriends($('locationID_'+id).getValue());
  	}

	
	function mapEntriesPrevNextControl(action) {
		
		var i = parseInt(currentValue);
		if(action == 'next') {
			i = i == totalCount ? 0 : i+1;
		} else {
			i = i == 0 ? totalCount : i-1;
		}
		currentValue = i;
		
		showCountryInfo(i,$('pin1_'+i));
		accentMarker($('pin2_'+i));
	}

	function getFriends(locationID) {
		var friendlist = $('friends').getElementsByTagName('ul');
		for(i=0; i<friendlist.length;i++){
			if(friendlist[i].getClassName() == 'friends_list'){
				friendlist[i].setStyle('display', 'none');
			}
		}
		// checked if friends_list is empty
		if($('friendlist_'+locationID) != null){
			if($('friendlist_'+locationID).getElementsByTagName('li').length > 0){	
				$('friends_header').setStyle('display', 'block');
				$('friendlist_'+locationID).setStyle('display', 'block');
			}else{
				$('friends_header').setStyle('display', 'none');
			}	
		}else{
			$('friends_header').setStyle('display', 'none');
		}		
	}

  	function accentMarker(obj) {
    	for (var i=0; i < <?php echo $i ?>; i++) {
      		if (mainPin = $('pin1_' + i)) {
        		mainPin.setStyle({zIndex: '4'});
      		}
      		if (altPin = $('pin2_' + i)) {
        		altPin.setStyle({zIndex: '4'});
      		}
    	}
    	obj.setStyle({display: 'block', zIndex: '5'});
  	}

	function showCities() {
		$('locationList').setInnerFBML(countriesWithCities);
	}

	function hideCities() {
		$('locationList').setInnerFBML(countriesOnly);		
	}

  	function restoreMarker(obj) {
    	obj.setStyle('display', 'block');
  	}
</script>
